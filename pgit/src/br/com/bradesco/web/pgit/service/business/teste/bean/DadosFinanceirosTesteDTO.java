/*
 * Nome: br.com.bradesco.web.pgit.service.business.teste.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.teste.bean;

import java.math.BigDecimal;

/**
 * Nome: DadosFinanceirosTesteDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class DadosFinanceirosTesteDTO {

	/** Atributo qtdeDoc. */
	private Long qtdeDoc;
	
	/** Atributo valorDoc. */
	private BigDecimal valorDoc;
	
	/** Atributo qtdeTed. */
	private Long qtdeTed;
	
	/** Atributo valorTed. */
	private BigDecimal valorTed;
	
	/** Atributo ordemPgto. */
	private Long ordemPgto;
	
	/** Atributo valorOrdemPgto. */
	private BigDecimal valorOrdemPgto;
	
	/** Atributo qtdeCredConta. */
	private Long qtdeCredConta;
	
	/** Atributo valorCredConta. */
	private BigDecimal valorCredConta;
	
	/** Atributo qtdeDebConta. */
	private Long qtdeDebConta;
	
	/** Atributo valorDebConta. */
	private BigDecimal valorDebConta;
	
	/** Atributo qtdeTitBradesco. */
	private Long qtdeTitBradesco;
	
	/** Atributo valorTitBradesco. */
	private BigDecimal valorTitBradesco;
	
	/** Atributo qtdeTitOutroBc. */
	private Long qtdeTitOutroBc;
	
	/** Atributo valorTitOutroBc. */
	private BigDecimal valorTitOutroBc;
	
	/** Atributo qtdeDeposIdent. */
	private Long qtdeDeposIdent;
	
	/** Atributo valorDeposIdent. */
	private BigDecimal valorDeposIdent;
	
	/** Atributo qtdeOrdemCred. */
	private Long qtdeOrdemCred;
	
	/** Atributo valorOrdemCred. */
	private BigDecimal valorOrdemCred;
	
	/** Atributo qtdePgtoFornecedor. */
	private Long qtdePgtoFornecedor;
	
	/** Atributo valorPgtoFornecedor. */
	private BigDecimal valorPgtoFornecedor;
	
	/** Atributo qtdeDocFolhaPgto. */
	private Long qtdeDocFolhaPgto;
	
	/** Atributo valorDocFolhaPgto. */
	private BigDecimal valorDocFolhaPgto;
	
	/** Atributo qtdeTedFolhaPgto. */
	private Long qtdeTedFolhaPgto;
	
	/** Atributo valorTedFolhaPgto. */
	private BigDecimal valorTedFolhaPgto;
	
	/** Atributo qtdeOrdemPgtoFolhaPgto. */
	private Long qtdeOrdemPgtoFolhaPgto;
	
	/** Atributo valorOrdemPgtoFolhaPgto. */
	private BigDecimal valorOrdemPgtoFolhaPgto;
	
	/** Atributo qtdeCredContaFolhaPgto. */
	private Long qtdeCredContaFolhaPgto;
	
	/** Atributo valorCredContaFolhaPgto. */
	private BigDecimal valorCredContaFolhaPgto;
	
	/** Atributo qtdeFolhaPgto. */
	private Long qtdeFolhaPgto;
	
	/** Atributo valorFolhaPgto. */
	private BigDecimal valorFolhaPgto;
	
	/** Atributo qtdeDarf. */
	private Long qtdeDarf;
	
	/** Atributo valorDarf. */
	private BigDecimal valorDarf;
	
	/** Atributo qtdeGare. */
	private Long qtdeGare;
	
	/** Atributo valorGare. */
	private BigDecimal valorGare;
	
	/** Atributo qtdeGps. */
	private Long qtdeGps;
	
	/** Atributo valorGps. */
	private BigDecimal valorGps;
	
	/** Atributo qtdeDebVeiculo. */
	private Long qtdeDebVeiculo;
	
	/** Atributo valorDebVeiculo. */
	private BigDecimal valorDebVeiculo;
	
	/** Atributo qtdeCodBarra. */
	private Long qtdeCodBarra;
	
	/** Atributo valorCodBarra. */
	private BigDecimal valorCodBarra;
	
	/** Atributo qtdePgtoEletronico. */
	private Long qtdePgtoEletronico;
	
	/** Atributo valorPgtoEletronico. */
	private BigDecimal valorPgtoEletronico;


	/**
	 * Get: ordemPgto.
	 *
	 * @return ordemPgto
	 */
	public Long getOrdemPgto() {
		return ordemPgto;
	}
	
	/**
	 * Set: ordemPgto.
	 *
	 * @param ordemPgto the ordem pgto
	 */
	public void setOrdemPgto(Long ordemPgto) {
		this.ordemPgto = ordemPgto;
	}
	
	/**
	 * Get: qtdeCodBarra.
	 *
	 * @return qtdeCodBarra
	 */
	public Long getQtdeCodBarra() {
		return qtdeCodBarra;
	}
	
	/**
	 * Set: qtdeCodBarra.
	 *
	 * @param qtdeCodBarra the qtde cod barra
	 */
	public void setQtdeCodBarra(Long qtdeCodBarra) {
		this.qtdeCodBarra = qtdeCodBarra;
	}
	
	/**
	 * Get: qtdeCredConta.
	 *
	 * @return qtdeCredConta
	 */
	public Long getQtdeCredConta() {
		return qtdeCredConta;
	}
	
	/**
	 * Set: qtdeCredConta.
	 *
	 * @param qtdeCredConta the qtde cred conta
	 */
	public void setQtdeCredConta(Long qtdeCredConta) {
		this.qtdeCredConta = qtdeCredConta;
	}
	
	/**
	 * Get: qtdeCredContaFolhaPgto.
	 *
	 * @return qtdeCredContaFolhaPgto
	 */
	public Long getQtdeCredContaFolhaPgto() {
		return qtdeCredContaFolhaPgto;
	}
	
	/**
	 * Set: qtdeCredContaFolhaPgto.
	 *
	 * @param qtdeCredContaFolhaPgto the qtde cred conta folha pgto
	 */
	public void setQtdeCredContaFolhaPgto(Long qtdeCredContaFolhaPgto) {
		this.qtdeCredContaFolhaPgto = qtdeCredContaFolhaPgto;
	}
	
	/**
	 * Get: qtdeDarf.
	 *
	 * @return qtdeDarf
	 */
	public Long getQtdeDarf() {
		return qtdeDarf;
	}
	
	/**
	 * Set: qtdeDarf.
	 *
	 * @param qtdeDarf the qtde darf
	 */
	public void setQtdeDarf(Long qtdeDarf) {
		this.qtdeDarf = qtdeDarf;
	}
	
	/**
	 * Get: qtdeDebConta.
	 *
	 * @return qtdeDebConta
	 */
	public Long getQtdeDebConta() {
		return qtdeDebConta;
	}
	
	/**
	 * Set: qtdeDebConta.
	 *
	 * @param qtdeDebConta the qtde deb conta
	 */
	public void setQtdeDebConta(Long qtdeDebConta) {
		this.qtdeDebConta = qtdeDebConta;
	}
	
	/**
	 * Get: qtdeDebVeiculo.
	 *
	 * @return qtdeDebVeiculo
	 */
	public Long getQtdeDebVeiculo() {
		return qtdeDebVeiculo;
	}
	
	/**
	 * Set: qtdeDebVeiculo.
	 *
	 * @param qtdeDebVeiculo the qtde deb veiculo
	 */
	public void setQtdeDebVeiculo(Long qtdeDebVeiculo) {
		this.qtdeDebVeiculo = qtdeDebVeiculo;
	}
	
	/**
	 * Get: qtdeDeposIdent.
	 *
	 * @return qtdeDeposIdent
	 */
	public Long getQtdeDeposIdent() {
		return qtdeDeposIdent;
	}
	
	/**
	 * Set: qtdeDeposIdent.
	 *
	 * @param qtdeDeposIdent the qtde depos ident
	 */
	public void setQtdeDeposIdent(Long qtdeDeposIdent) {
		this.qtdeDeposIdent = qtdeDeposIdent;
	}
	
	/**
	 * Get: qtdeDoc.
	 *
	 * @return qtdeDoc
	 */
	public Long getQtdeDoc() {
		return qtdeDoc;
	}
	
	/**
	 * Set: qtdeDoc.
	 *
	 * @param qtdeDoc the qtde doc
	 */
	public void setQtdeDoc(Long qtdeDoc) {
		this.qtdeDoc = qtdeDoc;
	}
	
	/**
	 * Get: qtdeDocFolhaPgto.
	 *
	 * @return qtdeDocFolhaPgto
	 */
	public Long getQtdeDocFolhaPgto() {
		return qtdeDocFolhaPgto;
	}
	
	/**
	 * Set: qtdeDocFolhaPgto.
	 *
	 * @param qtdeDocFolhaPgto the qtde doc folha pgto
	 */
	public void setQtdeDocFolhaPgto(Long qtdeDocFolhaPgto) {
		this.qtdeDocFolhaPgto = qtdeDocFolhaPgto;
	}
	
	/**
	 * Get: qtdeFolhaPgto.
	 *
	 * @return qtdeFolhaPgto
	 */
	public Long getQtdeFolhaPgto() {
		return qtdeFolhaPgto;
	}
	
	/**
	 * Set: qtdeFolhaPgto.
	 *
	 * @param qtdeFolhaPgto the qtde folha pgto
	 */
	public void setQtdeFolhaPgto(Long qtdeFolhaPgto) {
		this.qtdeFolhaPgto = qtdeFolhaPgto;
	}
	
	/**
	 * Get: qtdeGare.
	 *
	 * @return qtdeGare
	 */
	public Long getQtdeGare() {
		return qtdeGare;
	}
	
	/**
	 * Set: qtdeGare.
	 *
	 * @param qtdeGare the qtde gare
	 */
	public void setQtdeGare(Long qtdeGare) {
		this.qtdeGare = qtdeGare;
	}
	
	/**
	 * Get: qtdeGps.
	 *
	 * @return qtdeGps
	 */
	public Long getQtdeGps() {
		return qtdeGps;
	}
	
	/**
	 * Set: qtdeGps.
	 *
	 * @param qtdeGps the qtde gps
	 */
	public void setQtdeGps(Long qtdeGps) {
		this.qtdeGps = qtdeGps;
	}
	
	/**
	 * Get: qtdeOrdemCred.
	 *
	 * @return qtdeOrdemCred
	 */
	public Long getQtdeOrdemCred() {
		return qtdeOrdemCred;
	}
	
	/**
	 * Set: qtdeOrdemCred.
	 *
	 * @param qtdeOrdemCred the qtde ordem cred
	 */
	public void setQtdeOrdemCred(Long qtdeOrdemCred) {
		this.qtdeOrdemCred = qtdeOrdemCred;
	}
	
	/**
	 * Get: qtdeOrdemPgtoFolhaPgto.
	 *
	 * @return qtdeOrdemPgtoFolhaPgto
	 */
	public Long getQtdeOrdemPgtoFolhaPgto() {
		return qtdeOrdemPgtoFolhaPgto;
	}
	
	/**
	 * Set: qtdeOrdemPgtoFolhaPgto.
	 *
	 * @param qtdeOrdemPgtoFolhaPgto the qtde ordem pgto folha pgto
	 */
	public void setQtdeOrdemPgtoFolhaPgto(Long qtdeOrdemPgtoFolhaPgto) {
		this.qtdeOrdemPgtoFolhaPgto = qtdeOrdemPgtoFolhaPgto;
	}
	
	/**
	 * Get: qtdePgtoEletronico.
	 *
	 * @return qtdePgtoEletronico
	 */
	public Long getQtdePgtoEletronico() {
		return qtdePgtoEletronico;
	}
	
	/**
	 * Set: qtdePgtoEletronico.
	 *
	 * @param qtdePgtoEletronico the qtde pgto eletronico
	 */
	public void setQtdePgtoEletronico(Long qtdePgtoEletronico) {
		this.qtdePgtoEletronico = qtdePgtoEletronico;
	}
	
	/**
	 * Get: qtdePgtoFornecedor.
	 *
	 * @return qtdePgtoFornecedor
	 */
	public Long getQtdePgtoFornecedor() {
		return qtdePgtoFornecedor;
	}
	
	/**
	 * Set: qtdePgtoFornecedor.
	 *
	 * @param qtdePgtoFornecedor the qtde pgto fornecedor
	 */
	public void setQtdePgtoFornecedor(Long qtdePgtoFornecedor) {
		this.qtdePgtoFornecedor = qtdePgtoFornecedor;
	}
	
	/**
	 * Get: qtdeTed.
	 *
	 * @return qtdeTed
	 */
	public Long getQtdeTed() {
		return qtdeTed;
	}
	
	/**
	 * Set: qtdeTed.
	 *
	 * @param qtdeTed the qtde ted
	 */
	public void setQtdeTed(Long qtdeTed) {
		this.qtdeTed = qtdeTed;
	}
	
	/**
	 * Get: qtdeTedFolhaPgto.
	 *
	 * @return qtdeTedFolhaPgto
	 */
	public Long getQtdeTedFolhaPgto() {
		return qtdeTedFolhaPgto;
	}
	
	/**
	 * Set: qtdeTedFolhaPgto.
	 *
	 * @param qtdeTedFolhaPgto the qtde ted folha pgto
	 */
	public void setQtdeTedFolhaPgto(Long qtdeTedFolhaPgto) {
		this.qtdeTedFolhaPgto = qtdeTedFolhaPgto;
	}
	
	/**
	 * Get: qtdeTitBradesco.
	 *
	 * @return qtdeTitBradesco
	 */
	public Long getQtdeTitBradesco() {
		return qtdeTitBradesco;
	}
	
	/**
	 * Set: qtdeTitBradesco.
	 *
	 * @param qtdeTitBradesco the qtde tit bradesco
	 */
	public void setQtdeTitBradesco(Long qtdeTitBradesco) {
		this.qtdeTitBradesco = qtdeTitBradesco;
	}
	
	/**
	 * Get: qtdeTitOutroBc.
	 *
	 * @return qtdeTitOutroBc
	 */
	public Long getQtdeTitOutroBc() {
		return qtdeTitOutroBc;
	}
	
	/**
	 * Set: qtdeTitOutroBc.
	 *
	 * @param qtdeTitOutroBc the qtde tit outro bc
	 */
	public void setQtdeTitOutroBc(Long qtdeTitOutroBc) {
		this.qtdeTitOutroBc = qtdeTitOutroBc;
	}
	
	/**
	 * Get: valorCodBarra.
	 *
	 * @return valorCodBarra
	 */
	public BigDecimal getValorCodBarra() {
		return valorCodBarra;
	}
	
	/**
	 * Set: valorCodBarra.
	 *
	 * @param valorCodBarra the valor cod barra
	 */
	public void setValorCodBarra(BigDecimal valorCodBarra) {
		this.valorCodBarra = valorCodBarra;
	}
	
	/**
	 * Get: valorCredConta.
	 *
	 * @return valorCredConta
	 */
	public BigDecimal getValorCredConta() {
		return valorCredConta;
	}
	
	/**
	 * Set: valorCredConta.
	 *
	 * @param valorCredConta the valor cred conta
	 */
	public void setValorCredConta(BigDecimal valorCredConta) {
		this.valorCredConta = valorCredConta;
	}
	
	/**
	 * Get: valorCredContaFolhaPgto.
	 *
	 * @return valorCredContaFolhaPgto
	 */
	public BigDecimal getValorCredContaFolhaPgto() {
		return valorCredContaFolhaPgto;
	}
	
	/**
	 * Set: valorCredContaFolhaPgto.
	 *
	 * @param valorCredContaFolhaPgto the valor cred conta folha pgto
	 */
	public void setValorCredContaFolhaPgto(BigDecimal valorCredContaFolhaPgto) {
		this.valorCredContaFolhaPgto = valorCredContaFolhaPgto;
	}
	
	/**
	 * Get: valorDarf.
	 *
	 * @return valorDarf
	 */
	public BigDecimal getValorDarf() {
		return valorDarf;
	}
	
	/**
	 * Set: valorDarf.
	 *
	 * @param valorDarf the valor darf
	 */
	public void setValorDarf(BigDecimal valorDarf) {
		this.valorDarf = valorDarf;
	}
	
	/**
	 * Get: valorDebConta.
	 *
	 * @return valorDebConta
	 */
	public BigDecimal getValorDebConta() {
		return valorDebConta;
	}
	
	/**
	 * Set: valorDebConta.
	 *
	 * @param valorDebConta the valor deb conta
	 */
	public void setValorDebConta(BigDecimal valorDebConta) {
		this.valorDebConta = valorDebConta;
	}
	
	/**
	 * Get: valorDebVeiculo.
	 *
	 * @return valorDebVeiculo
	 */
	public BigDecimal getValorDebVeiculo() {
		return valorDebVeiculo;
	}
	
	/**
	 * Set: valorDebVeiculo.
	 *
	 * @param valorDebVeiculo the valor deb veiculo
	 */
	public void setValorDebVeiculo(BigDecimal valorDebVeiculo) {
		this.valorDebVeiculo = valorDebVeiculo;
	}
	
	/**
	 * Get: valorDeposIdent.
	 *
	 * @return valorDeposIdent
	 */
	public BigDecimal getValorDeposIdent() {
		return valorDeposIdent;
	}
	
	/**
	 * Set: valorDeposIdent.
	 *
	 * @param valorDeposIdent the valor depos ident
	 */
	public void setValorDeposIdent(BigDecimal valorDeposIdent) {
		this.valorDeposIdent = valorDeposIdent;
	}
	
	/**
	 * Get: valorDoc.
	 *
	 * @return valorDoc
	 */
	public BigDecimal getValorDoc() {
		return valorDoc;
	}
	
	/**
	 * Set: valorDoc.
	 *
	 * @param valorDoc the valor doc
	 */
	public void setValorDoc(BigDecimal valorDoc) {
		this.valorDoc = valorDoc;
	}
	
	/**
	 * Get: valorDocFolhaPgto.
	 *
	 * @return valorDocFolhaPgto
	 */
	public BigDecimal getValorDocFolhaPgto() {
		return valorDocFolhaPgto;
	}
	
	/**
	 * Set: valorDocFolhaPgto.
	 *
	 * @param valorDocFolhaPgto the valor doc folha pgto
	 */
	public void setValorDocFolhaPgto(BigDecimal valorDocFolhaPgto) {
		this.valorDocFolhaPgto = valorDocFolhaPgto;
	}
	
	/**
	 * Get: valorFolhaPgto.
	 *
	 * @return valorFolhaPgto
	 */
	public BigDecimal getValorFolhaPgto() {
		return valorFolhaPgto;
	}
	
	/**
	 * Set: valorFolhaPgto.
	 *
	 * @param valorFolhaPgto the valor folha pgto
	 */
	public void setValorFolhaPgto(BigDecimal valorFolhaPgto) {
		this.valorFolhaPgto = valorFolhaPgto;
	}
	
	/**
	 * Get: valorGare.
	 *
	 * @return valorGare
	 */
	public BigDecimal getValorGare() {
		return valorGare;
	}
	
	/**
	 * Set: valorGare.
	 *
	 * @param valorGare the valor gare
	 */
	public void setValorGare(BigDecimal valorGare) {
		this.valorGare = valorGare;
	}
	
	/**
	 * Get: valorGps.
	 *
	 * @return valorGps
	 */
	public BigDecimal getValorGps() {
		return valorGps;
	}
	
	/**
	 * Set: valorGps.
	 *
	 * @param valorGps the valor gps
	 */
	public void setValorGps(BigDecimal valorGps) {
		this.valorGps = valorGps;
	}
	
	/**
	 * Get: valorOrdemCred.
	 *
	 * @return valorOrdemCred
	 */
	public BigDecimal getValorOrdemCred() {
		return valorOrdemCred;
	}
	
	/**
	 * Set: valorOrdemCred.
	 *
	 * @param valorOrdemCred the valor ordem cred
	 */
	public void setValorOrdemCred(BigDecimal valorOrdemCred) {
		this.valorOrdemCred = valorOrdemCred;
	}
	
	/**
	 * Get: valorOrdemPgto.
	 *
	 * @return valorOrdemPgto
	 */
	public BigDecimal getValorOrdemPgto() {
		return valorOrdemPgto;
	}
	
	/**
	 * Set: valorOrdemPgto.
	 *
	 * @param valorOrdemPgto the valor ordem pgto
	 */
	public void setValorOrdemPgto(BigDecimal valorOrdemPgto) {
		this.valorOrdemPgto = valorOrdemPgto;
	}
	
	/**
	 * Get: valorOrdemPgtoFolhaPgto.
	 *
	 * @return valorOrdemPgtoFolhaPgto
	 */
	public BigDecimal getValorOrdemPgtoFolhaPgto() {
		return valorOrdemPgtoFolhaPgto;
	}
	
	/**
	 * Set: valorOrdemPgtoFolhaPgto.
	 *
	 * @param valorOrdemPgtoFolhaPgto the valor ordem pgto folha pgto
	 */
	public void setValorOrdemPgtoFolhaPgto(BigDecimal valorOrdemPgtoFolhaPgto) {
		this.valorOrdemPgtoFolhaPgto = valorOrdemPgtoFolhaPgto;
	}
	
	/**
	 * Get: valorPgtoEletronico.
	 *
	 * @return valorPgtoEletronico
	 */
	public BigDecimal getValorPgtoEletronico() {
		return valorPgtoEletronico;
	}
	
	/**
	 * Set: valorPgtoEletronico.
	 *
	 * @param valorPgtoEletronico the valor pgto eletronico
	 */
	public void setValorPgtoEletronico(BigDecimal valorPgtoEletronico) {
		this.valorPgtoEletronico = valorPgtoEletronico;
	}
	
	/**
	 * Get: valorPgtoFornecedor.
	 *
	 * @return valorPgtoFornecedor
	 */
	public BigDecimal getValorPgtoFornecedor() {
		return valorPgtoFornecedor;
	}
	
	/**
	 * Set: valorPgtoFornecedor.
	 *
	 * @param valorPgtoFornecedor the valor pgto fornecedor
	 */
	public void setValorPgtoFornecedor(BigDecimal valorPgtoFornecedor) {
		this.valorPgtoFornecedor = valorPgtoFornecedor;
	}
	
	/**
	 * Get: valorTed.
	 *
	 * @return valorTed
	 */
	public BigDecimal getValorTed() {
		return valorTed;
	}
	
	/**
	 * Set: valorTed.
	 *
	 * @param valorTed the valor ted
	 */
	public void setValorTed(BigDecimal valorTed) {
		this.valorTed = valorTed;
	}
	
	/**
	 * Get: valorTedFolhaPgto.
	 *
	 * @return valorTedFolhaPgto
	 */
	public BigDecimal getValorTedFolhaPgto() {
		return valorTedFolhaPgto;
	}
	
	/**
	 * Set: valorTedFolhaPgto.
	 *
	 * @param valorTedFolhaPgto the valor ted folha pgto
	 */
	public void setValorTedFolhaPgto(BigDecimal valorTedFolhaPgto) {
		this.valorTedFolhaPgto = valorTedFolhaPgto;
	}
	
	/**
	 * Get: valorTitBradesco.
	 *
	 * @return valorTitBradesco
	 */
	public BigDecimal getValorTitBradesco() {
		return valorTitBradesco;
	}
	
	/**
	 * Set: valorTitBradesco.
	 *
	 * @param valorTitBradesco the valor tit bradesco
	 */
	public void setValorTitBradesco(BigDecimal valorTitBradesco) {
		this.valorTitBradesco = valorTitBradesco;
	}
	
	/**
	 * Get: valorTitOutroBc.
	 *
	 * @return valorTitOutroBc
	 */
	public BigDecimal getValorTitOutroBc() {
		return valorTitOutroBc;
	}
	
	/**
	 * Set: valorTitOutroBc.
	 *
	 * @param valorTitOutroBc the valor tit outro bc
	 */
	public void setValorTitOutroBc(BigDecimal valorTitOutroBc) {
		this.valorTitOutroBc = valorTitOutroBc;
	}
}