/*
 * Nome: br.com.bradesco.web.pgit.service.business.regsegregacessodados.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.regsegregacessodados.bean;

/**
 * Nome: ListarExcRegraSegregacaoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarExcRegraSegregacaoSaidaDTO {
	
	/** Atributo codMensagem. */
	private String codMensagem;
	
	/** Atributo mensagem. */
	private String mensagem;
	
	/** Atributo cdRegra. */
	private int cdRegra;
	
	/** Atributo cdExcecao. */
	private int cdExcecao;
	
	/** Atributo cdTipoUnidadeOrganizacionalUsuario. */
	private int cdTipoUnidadeOrganizacionalUsuario;
	
	/** Atributo dsTipoUnidadeOrganizacionalUsuario. */
	private String dsTipoUnidadeOrganizacionalUsuario;
	
	/** Atributo empresa. */
	private long empresa;
	
	/** Atributo cdUnidadeOrganizacionalUsuario. */
	private int cdUnidadeOrganizacionalUsuario;
	
	/** Atributo dsUnidadeOrganizacionalUsuario. */
	private String dsUnidadeOrganizacionalUsuario;
	
	/** Atributo tipoExcecao. */
	private int tipoExcecao;
	
	/** Atributo tipoAbrangenciaExcecao. */
	private int tipoAbrangenciaExcecao;
	
	/**
	 * Get: empresa.
	 *
	 * @return empresa
	 */
	public long getEmpresa() {
		return empresa;
	}
	
	/**
	 * Set: empresa.
	 *
	 * @param cdEmpresa the empresa
	 */
	public void setEmpresa(long cdEmpresa) {
		this.empresa = cdEmpresa;
	}
	
	/**
	 * Get: cdExcecao.
	 *
	 * @return cdExcecao
	 */
	public int getCdExcecao() {
		return cdExcecao;
	}
	
	/**
	 * Set: cdExcecao.
	 *
	 * @param cdExcecao the cd excecao
	 */
	public void setCdExcecao(int cdExcecao) {
		this.cdExcecao = cdExcecao;
	}
	
	/**
	 * Get: cdRegra.
	 *
	 * @return cdRegra
	 */
	public int getCdRegra() {
		return cdRegra;
	}
	
	/**
	 * Set: cdRegra.
	 *
	 * @param cdRegra the cd regra
	 */
	public void setCdRegra(int cdRegra) {
		this.cdRegra = cdRegra;
	}
	
	/**
	 * Get: tipoAbrangenciaExcecao.
	 *
	 * @return tipoAbrangenciaExcecao
	 */
	public int getTipoAbrangenciaExcecao() {
		return tipoAbrangenciaExcecao;
	}
	
	/**
	 * Set: tipoAbrangenciaExcecao.
	 *
	 * @param cdTipoAbrangenciaExcecao the tipo abrangencia excecao
	 */
	public void setTipoAbrangenciaExcecao(int cdTipoAbrangenciaExcecao) {
		this.tipoAbrangenciaExcecao = cdTipoAbrangenciaExcecao;
	}
	
	/**
	 * Get: tipoExcecao.
	 *
	 * @return tipoExcecao
	 */
	public int getTipoExcecao() {
		return tipoExcecao;
	}
	
	/**
	 * Set: tipoExcecao.
	 *
	 * @param cdTipoExcecao the tipo excecao
	 */
	public void setTipoExcecao(int cdTipoExcecao) {
		this.tipoExcecao = cdTipoExcecao;
	}
	
	/**
	 * Get: cdTipoUnidadeOrganizacionalUsuario.
	 *
	 * @return cdTipoUnidadeOrganizacionalUsuario
	 */
	public int getCdTipoUnidadeOrganizacionalUsuario() {
		return cdTipoUnidadeOrganizacionalUsuario;
	}
	
	/**
	 * Set: cdTipoUnidadeOrganizacionalUsuario.
	 *
	 * @param cdTipoUnidadeOrganizacionalUsuario the cd tipo unidade organizacional usuario
	 */
	public void setCdTipoUnidadeOrganizacionalUsuario(
			int cdTipoUnidadeOrganizacionalUsuario) {
		this.cdTipoUnidadeOrganizacionalUsuario = cdTipoUnidadeOrganizacionalUsuario;
	}
	
	/**
	 * Get: cdUnidadeOrganizacionalUsuario.
	 *
	 * @return cdUnidadeOrganizacionalUsuario
	 */
	public int getCdUnidadeOrganizacionalUsuario() {
		return cdUnidadeOrganizacionalUsuario;
	}
	
	/**
	 * Set: cdUnidadeOrganizacionalUsuario.
	 *
	 * @param cdUnidadeOrganizacionalUsuario the cd unidade organizacional usuario
	 */
	public void setCdUnidadeOrganizacionalUsuario(int cdUnidadeOrganizacionalUsuario) {
		this.cdUnidadeOrganizacionalUsuario = cdUnidadeOrganizacionalUsuario;
	}
	
	/**
	 * Get: dsTipoUnidadeOrganizacionalUsuario.
	 *
	 * @return dsTipoUnidadeOrganizacionalUsuario
	 */
	public String getDsTipoUnidadeOrganizacionalUsuario() {
		return dsTipoUnidadeOrganizacionalUsuario;
	}
	
	/**
	 * Set: dsTipoUnidadeOrganizacionalUsuario.
	 *
	 * @param dsTipoUnidadeOrganizacionalUsuario the ds tipo unidade organizacional usuario
	 */
	public void setDsTipoUnidadeOrganizacionalUsuario(
			String dsTipoUnidadeOrganizacionalUsuario) {
		this.dsTipoUnidadeOrganizacionalUsuario = dsTipoUnidadeOrganizacionalUsuario;
	}
	
	/**
	 * Get: dsUnidadeOrganizacionalUsuario.
	 *
	 * @return dsUnidadeOrganizacionalUsuario
	 */
	public String getDsUnidadeOrganizacionalUsuario() {
		return dsUnidadeOrganizacionalUsuario;
	}
	
	/**
	 * Set: dsUnidadeOrganizacionalUsuario.
	 *
	 * @param dsUnidadeOrganizacionalUsuario the ds unidade organizacional usuario
	 */
	public void setDsUnidadeOrganizacionalUsuario(
			String dsUnidadeOrganizacionalUsuario) {
		this.dsUnidadeOrganizacionalUsuario = dsUnidadeOrganizacionalUsuario;
	}
	
	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}
	
	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	
	

}
