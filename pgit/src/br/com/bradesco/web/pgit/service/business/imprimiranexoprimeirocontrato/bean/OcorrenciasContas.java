/*
 * Nome: br.com.bradesco.web.pgit.service.business.imprimiranexoprimeirocontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.imprimiranexoprimeirocontrato.bean;

import br.com.bradesco.web.pgit.utils.PgitUtil;

/**
 * Nome: OcorrenciasContas
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class OcorrenciasContas {
	
	/** Atributo cdCpfCnpjConta. */
	private String cdCpfCnpjConta;
    
    /** Atributo cdNomeParticipanteConta. */
    private String cdNomeParticipanteConta;
    
    /** Atributo cdBancoConta. */
    private Integer cdBancoConta;
    
    /** Atributo cdAgenciaConta. */
    private Integer cdAgenciaConta;
    
    /** Atributo digitoAgenciaConta. */
    private String digitoAgenciaConta;
    
    /** Atributo cdContaConta. */
    private Long cdContaConta;
    
    /** Atributo digitoContaConta. */
    private String digitoContaConta;
    
    /** Atributo dsTipoConta. */
    private String dsTipoConta;
    
    /** Atributo agencia. */
    private String agencia;    
    
    /** Atributo conta. */
    private String conta;
    
    public String getAgenciaFormatada(){
        return getCdAgenciaConta() + " - " + getDigitoAgenciaConta();
    }
    
    public String getContaFormatada(){
        return getCdContaConta() + " - " + getDigitoContaConta();
    }
    
	/**
	 * Get: cdCpfCnpjConta.
	 *
	 * @return cdCpfCnpjConta
	 */
	public String getCdCpfCnpjConta() {
		return cdCpfCnpjConta;
	}
	
	/**
	 * Set: cdCpfCnpjConta.
	 *
	 * @param cdCpfCnpjConta the cd cpf cnpj conta
	 */
	public void setCdCpfCnpjConta(String cdCpfCnpjConta) {
		this.cdCpfCnpjConta = cdCpfCnpjConta;
	}
	
	/**
	 * Get: cdNomeParticipanteConta.
	 *
	 * @return cdNomeParticipanteConta
	 */
	public String getCdNomeParticipanteConta() {
		return cdNomeParticipanteConta;
	}
	
	/**
	 * Set: cdNomeParticipanteConta.
	 *
	 * @param cdNomeParticipanteConta the cd nome participante conta
	 */
	public void setCdNomeParticipanteConta(String cdNomeParticipanteConta) {
		this.cdNomeParticipanteConta = cdNomeParticipanteConta;
	}
	
	/**
	 * Get: cdBancoConta.
	 *
	 * @return cdBancoConta
	 */
	public Integer getCdBancoConta() {
		return cdBancoConta;
	}
	
	/**
	 * Set: cdBancoConta.
	 *
	 * @param cdBancoConta the cd banco conta
	 */
	public void setCdBancoConta(Integer cdBancoConta) {
		this.cdBancoConta = cdBancoConta;
	}
	
	/**
	 * Get: cdAgenciaConta.
	 *
	 * @return cdAgenciaConta
	 */
	public Integer getCdAgenciaConta() {
		return cdAgenciaConta;
	}
	
	/**
	 * Set: cdAgenciaConta.
	 *
	 * @param cdAgenciaConta the cd agencia conta
	 */
	public void setCdAgenciaConta(Integer cdAgenciaConta) {
		this.cdAgenciaConta = cdAgenciaConta;
	}
	
	/**
	 * Get: digitoAgenciaConta.
	 *
	 * @return digitoAgenciaConta
	 */
	public String getDigitoAgenciaConta() {
		return digitoAgenciaConta;
	}
	
	/**
	 * Set: digitoAgenciaConta.
	 *
	 * @param digitoAgenciaConta the digito agencia conta
	 */
	public void setDigitoAgenciaConta(String digitoAgenciaConta) {
		this.digitoAgenciaConta = digitoAgenciaConta;
	}
	
	/**
	 * Get: cdContaConta.
	 *
	 * @return cdContaConta
	 */
	public Long getCdContaConta() {
		return cdContaConta;
	}
	
	/**
	 * Set: cdContaConta.
	 *
	 * @param cdContaConta the cd conta conta
	 */
	public void setCdContaConta(Long cdContaConta) {
		this.cdContaConta = cdContaConta;
	}
	
	/**
	 * Get: digitoContaConta.
	 *
	 * @return digitoContaConta
	 */
	public String getDigitoContaConta() {
		return digitoContaConta;
	}
	
	/**
	 * Set: digitoContaConta.
	 *
	 * @param digitoContaConta the digito conta conta
	 */
	public void setDigitoContaConta(String digitoContaConta) {
		this.digitoContaConta = digitoContaConta;
	}
	
	/**
	 * Get: dsTipoConta.
	 *
	 * @return dsTipoConta
	 */
	public String getDsTipoConta() {
		return dsTipoConta;
	}
	
	/**
	 * Set: dsTipoConta.
	 *
	 * @param dsTipoConta the ds tipo conta
	 */
	public void setDsTipoConta(String dsTipoConta) {
		this.dsTipoConta = dsTipoConta;
	}

	public String getAgencia() {
		return getDigitoAgenciaConta();
	}

	public String getConta() {
		return getDigitoContaConta();
	}	
}