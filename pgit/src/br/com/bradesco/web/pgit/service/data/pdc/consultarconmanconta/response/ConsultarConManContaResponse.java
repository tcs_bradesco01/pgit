/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultarconmanconta.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.math.BigDecimal;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ConsultarConManContaResponse.
 * 
 * @version $Revision$ $Date$
 */
public class ConsultarConManContaResponse implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _codMensagem
     */
    private java.lang.String _codMensagem;

    /**
     * Field _mensagem
     */
    private java.lang.String _mensagem;

    /**
     * Field _cdBanco
     */
    private int _cdBanco = 0;

    /**
     * keeps track of state for field: _cdBanco
     */
    private boolean _has_cdBanco;

    /**
     * Field _dsBanco
     */
    private java.lang.String _dsBanco;

    /**
     * Field _cdComercialAgenciaContabil
     */
    private int _cdComercialAgenciaContabil = 0;

    /**
     * keeps track of state for field: _cdComercialAgenciaContabil
     */
    private boolean _has_cdComercialAgenciaContabil;

    /**
     * Field _dsComercialAgenciaContabil
     */
    private java.lang.String _dsComercialAgenciaContabil;

    /**
     * Field _cdNumeroContaBancaria
     */
    private long _cdNumeroContaBancaria = 0;

    /**
     * keeps track of state for field: _cdNumeroContaBancaria
     */
    private boolean _has_cdNumeroContaBancaria;

    /**
     * Field _cdDigitoContaBancaria
     */
    private int _cdDigitoContaBancaria = 0;

    /**
     * keeps track of state for field: _cdDigitoContaBancaria
     */
    private boolean _has_cdDigitoContaBancaria;

    /**
     * Field _cdTipoConta
     */
    private int _cdTipoConta = 0;

    /**
     * keeps track of state for field: _cdTipoConta
     */
    private boolean _has_cdTipoConta;

    /**
     * Field _dsTipoConta
     */
    private java.lang.String _dsTipoConta;

    /**
     * Field _cdSituacaoVinculo
     */
    private int _cdSituacaoVinculo = 0;

    /**
     * keeps track of state for field: _cdSituacaoVinculo
     */
    private boolean _has_cdSituacaoVinculo;

    /**
     * Field _dsSituacaoVinculo
     */
    private java.lang.String _dsSituacaoVinculo;

    /**
     * Field _dtVinculoInicial
     */
    private java.lang.String _dtVinculoInicial;

    /**
     * Field _dtVinculoFinal
     */
    private java.lang.String _dtVinculoFinal;

    /**
     * Field _cdMunicipioFeri
     */
    private int _cdMunicipioFeri = 0;

    /**
     * keeps track of state for field: _cdMunicipioFeri
     */
    private boolean _has_cdMunicipioFeri;

    /**
     * Field _dsMunicipioFeri
     */
    private java.lang.String _dsMunicipioFeri;

    /**
     * Field _cdMunicipio
     */
    private int _cdMunicipio = 0;

    /**
     * keeps track of state for field: _cdMunicipio
     */
    private boolean _has_cdMunicipio;

    /**
     * Field _dsMunicipio
     */
    private java.lang.String _dsMunicipio;

    /**
     * Field _cdUnidadeFederativa
     */
    private int _cdUnidadeFederativa = 0;

    /**
     * keeps track of state for field: _cdUnidadeFederativa
     */
    private boolean _has_cdUnidadeFederativa;

    /**
     * Field _dsUnidadeFederativa
     */
    private java.lang.String _dsUnidadeFederativa;

    /**
     * Field _hrInclusaoRegistro
     */
    private java.lang.String _hrInclusaoRegistro;

    /**
     * Field _hrManutencaoRegistro
     */
    private java.lang.String _hrManutencaoRegistro;

    /**
     * Field _cdUsuarioInclusao
     */
    private java.lang.String _cdUsuarioInclusao;

    /**
     * Field _cdUsuarioInclusaoExterno
     */
    private java.lang.String _cdUsuarioInclusaoExterno;

    /**
     * Field _cdOperacaoCanalInclusao
     */
    private java.lang.String _cdOperacaoCanalInclusao;

    /**
     * Field _cdTipoCanalInclusao
     */
    private int _cdTipoCanalInclusao = 0;

    /**
     * keeps track of state for field: _cdTipoCanalInclusao
     */
    private boolean _has_cdTipoCanalInclusao;

    /**
     * Field _dsCanalInclusao
     */
    private java.lang.String _dsCanalInclusao;

    /**
     * Field _cdUsuarioManutencao
     */
    private java.lang.String _cdUsuarioManutencao;

    /**
     * Field _cdUsuarioManutencaoExterno
     */
    private java.lang.String _cdUsuarioManutencaoExterno;

    /**
     * Field _cdOperacaoCanalManutencao
     */
    private java.lang.String _cdOperacaoCanalManutencao;

    /**
     * Field _cdTipoCanalManutencao
     */
    private int _cdTipoCanalManutencao = 0;

    /**
     * keeps track of state for field: _cdTipoCanalManutencao
     */
    private boolean _has_cdTipoCanalManutencao;

    /**
     * Field _dsCanalManutencao
     */
    private java.lang.String _dsCanalManutencao;

    /**
     * Field _cdMotivoSituacaoConta
     */
    private int _cdMotivoSituacaoConta = 0;

    /**
     * keeps track of state for field: _cdMotivoSituacaoConta
     */
    private boolean _has_cdMotivoSituacaoConta;

    /**
     * Field _dsMotivoSituacaoConta
     */
    private java.lang.String _dsMotivoSituacaoConta;

    /**
     * Field _valorAdicionadoContratoConta
     */
    private java.math.BigDecimal _valorAdicionadoContratoConta = new java.math.BigDecimal("0");

    /**
     * Field _dtInicialValorAdicionado
     */
    private java.lang.String _dtInicialValorAdicionado;

    /**
     * Field _dtFinalValorAdicionado
     */
    private java.lang.String _dtFinalValorAdicionado;

    /**
     * Field _valorAdicionadoContratoTed
     */
    private java.math.BigDecimal _valorAdicionadoContratoTed = new java.math.BigDecimal("0");

    /**
     * Field _dtInicioValidadeTed
     */
    private java.lang.String _dtInicioValidadeTed;

    /**
     * Field _dtFimValidadeTed
     */
    private java.lang.String _dtFimValidadeTed;

    /**
     * Field _cdIndicadorTipoManutencao
     */
    private int _cdIndicadorTipoManutencao = 0;

    /**
     * keeps track of state for field: _cdIndicadorTipoManutencao
     */
    private boolean _has_cdIndicadorTipoManutencao;

    /**
     * Field _dsIndicadorTipoManutencao
     */
    private java.lang.String _dsIndicadorTipoManutencao;

    /**
     * Field _dsEmpresaSegLinha
     */
    private java.lang.String _dsEmpresaSegLinha;


      //----------------/
     //- Constructors -/
    //----------------/

    public ConsultarConManContaResponse() 
     {
        super();
        setValorAdicionadoContratoConta(new java.math.BigDecimal("0"));
        setValorAdicionadoContratoTed(new java.math.BigDecimal("0"));
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarconmanconta.response.ConsultarConManContaResponse()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdBanco
     * 
     */
    public void deleteCdBanco()
    {
        this._has_cdBanco= false;
    } //-- void deleteCdBanco() 

    /**
     * Method deleteCdComercialAgenciaContabil
     * 
     */
    public void deleteCdComercialAgenciaContabil()
    {
        this._has_cdComercialAgenciaContabil= false;
    } //-- void deleteCdComercialAgenciaContabil() 

    /**
     * Method deleteCdDigitoContaBancaria
     * 
     */
    public void deleteCdDigitoContaBancaria()
    {
        this._has_cdDigitoContaBancaria= false;
    } //-- void deleteCdDigitoContaBancaria() 

    /**
     * Method deleteCdIndicadorTipoManutencao
     * 
     */
    public void deleteCdIndicadorTipoManutencao()
    {
        this._has_cdIndicadorTipoManutencao= false;
    } //-- void deleteCdIndicadorTipoManutencao() 

    /**
     * Method deleteCdMotivoSituacaoConta
     * 
     */
    public void deleteCdMotivoSituacaoConta()
    {
        this._has_cdMotivoSituacaoConta= false;
    } //-- void deleteCdMotivoSituacaoConta() 

    /**
     * Method deleteCdMunicipio
     * 
     */
    public void deleteCdMunicipio()
    {
        this._has_cdMunicipio= false;
    } //-- void deleteCdMunicipio() 

    /**
     * Method deleteCdMunicipioFeri
     * 
     */
    public void deleteCdMunicipioFeri()
    {
        this._has_cdMunicipioFeri= false;
    } //-- void deleteCdMunicipioFeri() 

    /**
     * Method deleteCdNumeroContaBancaria
     * 
     */
    public void deleteCdNumeroContaBancaria()
    {
        this._has_cdNumeroContaBancaria= false;
    } //-- void deleteCdNumeroContaBancaria() 

    /**
     * Method deleteCdSituacaoVinculo
     * 
     */
    public void deleteCdSituacaoVinculo()
    {
        this._has_cdSituacaoVinculo= false;
    } //-- void deleteCdSituacaoVinculo() 

    /**
     * Method deleteCdTipoCanalInclusao
     * 
     */
    public void deleteCdTipoCanalInclusao()
    {
        this._has_cdTipoCanalInclusao= false;
    } //-- void deleteCdTipoCanalInclusao() 

    /**
     * Method deleteCdTipoCanalManutencao
     * 
     */
    public void deleteCdTipoCanalManutencao()
    {
        this._has_cdTipoCanalManutencao= false;
    } //-- void deleteCdTipoCanalManutencao() 

    /**
     * Method deleteCdTipoConta
     * 
     */
    public void deleteCdTipoConta()
    {
        this._has_cdTipoConta= false;
    } //-- void deleteCdTipoConta() 

    /**
     * Method deleteCdUnidadeFederativa
     * 
     */
    public void deleteCdUnidadeFederativa()
    {
        this._has_cdUnidadeFederativa= false;
    } //-- void deleteCdUnidadeFederativa() 

    /**
     * Returns the value of field 'cdBanco'.
     * 
     * @return int
     * @return the value of field 'cdBanco'.
     */
    public int getCdBanco()
    {
        return this._cdBanco;
    } //-- int getCdBanco() 

    /**
     * Returns the value of field 'cdComercialAgenciaContabil'.
     * 
     * @return int
     * @return the value of field 'cdComercialAgenciaContabil'.
     */
    public int getCdComercialAgenciaContabil()
    {
        return this._cdComercialAgenciaContabil;
    } //-- int getCdComercialAgenciaContabil() 

    /**
     * Returns the value of field 'cdDigitoContaBancaria'.
     * 
     * @return int
     * @return the value of field 'cdDigitoContaBancaria'.
     */
    public int getCdDigitoContaBancaria()
    {
        return this._cdDigitoContaBancaria;
    } //-- int getCdDigitoContaBancaria() 

    /**
     * Returns the value of field 'cdIndicadorTipoManutencao'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorTipoManutencao'.
     */
    public int getCdIndicadorTipoManutencao()
    {
        return this._cdIndicadorTipoManutencao;
    } //-- int getCdIndicadorTipoManutencao() 

    /**
     * Returns the value of field 'cdMotivoSituacaoConta'.
     * 
     * @return int
     * @return the value of field 'cdMotivoSituacaoConta'.
     */
    public int getCdMotivoSituacaoConta()
    {
        return this._cdMotivoSituacaoConta;
    } //-- int getCdMotivoSituacaoConta() 

    /**
     * Returns the value of field 'cdMunicipio'.
     * 
     * @return int
     * @return the value of field 'cdMunicipio'.
     */
    public int getCdMunicipio()
    {
        return this._cdMunicipio;
    } //-- int getCdMunicipio() 

    /**
     * Returns the value of field 'cdMunicipioFeri'.
     * 
     * @return int
     * @return the value of field 'cdMunicipioFeri'.
     */
    public int getCdMunicipioFeri()
    {
        return this._cdMunicipioFeri;
    } //-- int getCdMunicipioFeri() 

    /**
     * Returns the value of field 'cdNumeroContaBancaria'.
     * 
     * @return long
     * @return the value of field 'cdNumeroContaBancaria'.
     */
    public long getCdNumeroContaBancaria()
    {
        return this._cdNumeroContaBancaria;
    } //-- long getCdNumeroContaBancaria() 

    /**
     * Returns the value of field 'cdOperacaoCanalInclusao'.
     * 
     * @return String
     * @return the value of field 'cdOperacaoCanalInclusao'.
     */
    public java.lang.String getCdOperacaoCanalInclusao()
    {
        return this._cdOperacaoCanalInclusao;
    } //-- java.lang.String getCdOperacaoCanalInclusao() 

    /**
     * Returns the value of field 'cdOperacaoCanalManutencao'.
     * 
     * @return String
     * @return the value of field 'cdOperacaoCanalManutencao'.
     */
    public java.lang.String getCdOperacaoCanalManutencao()
    {
        return this._cdOperacaoCanalManutencao;
    } //-- java.lang.String getCdOperacaoCanalManutencao() 

    /**
     * Returns the value of field 'cdSituacaoVinculo'.
     * 
     * @return int
     * @return the value of field 'cdSituacaoVinculo'.
     */
    public int getCdSituacaoVinculo()
    {
        return this._cdSituacaoVinculo;
    } //-- int getCdSituacaoVinculo() 

    /**
     * Returns the value of field 'cdTipoCanalInclusao'.
     * 
     * @return int
     * @return the value of field 'cdTipoCanalInclusao'.
     */
    public int getCdTipoCanalInclusao()
    {
        return this._cdTipoCanalInclusao;
    } //-- int getCdTipoCanalInclusao() 

    /**
     * Returns the value of field 'cdTipoCanalManutencao'.
     * 
     * @return int
     * @return the value of field 'cdTipoCanalManutencao'.
     */
    public int getCdTipoCanalManutencao()
    {
        return this._cdTipoCanalManutencao;
    } //-- int getCdTipoCanalManutencao() 

    /**
     * Returns the value of field 'cdTipoConta'.
     * 
     * @return int
     * @return the value of field 'cdTipoConta'.
     */
    public int getCdTipoConta()
    {
        return this._cdTipoConta;
    } //-- int getCdTipoConta() 

    /**
     * Returns the value of field 'cdUnidadeFederativa'.
     * 
     * @return int
     * @return the value of field 'cdUnidadeFederativa'.
     */
    public int getCdUnidadeFederativa()
    {
        return this._cdUnidadeFederativa;
    } //-- int getCdUnidadeFederativa() 

    /**
     * Returns the value of field 'cdUsuarioInclusao'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioInclusao'.
     */
    public java.lang.String getCdUsuarioInclusao()
    {
        return this._cdUsuarioInclusao;
    } //-- java.lang.String getCdUsuarioInclusao() 

    /**
     * Returns the value of field 'cdUsuarioInclusaoExterno'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioInclusaoExterno'.
     */
    public java.lang.String getCdUsuarioInclusaoExterno()
    {
        return this._cdUsuarioInclusaoExterno;
    } //-- java.lang.String getCdUsuarioInclusaoExterno() 

    /**
     * Returns the value of field 'cdUsuarioManutencao'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioManutencao'.
     */
    public java.lang.String getCdUsuarioManutencao()
    {
        return this._cdUsuarioManutencao;
    } //-- java.lang.String getCdUsuarioManutencao() 

    /**
     * Returns the value of field 'cdUsuarioManutencaoExterno'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioManutencaoExterno'.
     */
    public java.lang.String getCdUsuarioManutencaoExterno()
    {
        return this._cdUsuarioManutencaoExterno;
    } //-- java.lang.String getCdUsuarioManutencaoExterno() 

    /**
     * Returns the value of field 'codMensagem'.
     * 
     * @return String
     * @return the value of field 'codMensagem'.
     */
    public java.lang.String getCodMensagem()
    {
        return this._codMensagem;
    } //-- java.lang.String getCodMensagem() 

    /**
     * Returns the value of field 'dsBanco'.
     * 
     * @return String
     * @return the value of field 'dsBanco'.
     */
    public java.lang.String getDsBanco()
    {
        return this._dsBanco;
    } //-- java.lang.String getDsBanco() 

    /**
     * Returns the value of field 'dsCanalInclusao'.
     * 
     * @return String
     * @return the value of field 'dsCanalInclusao'.
     */
    public java.lang.String getDsCanalInclusao()
    {
        return this._dsCanalInclusao;
    } //-- java.lang.String getDsCanalInclusao() 

    /**
     * Returns the value of field 'dsCanalManutencao'.
     * 
     * @return String
     * @return the value of field 'dsCanalManutencao'.
     */
    public java.lang.String getDsCanalManutencao()
    {
        return this._dsCanalManutencao;
    } //-- java.lang.String getDsCanalManutencao() 

    /**
     * Returns the value of field 'dsComercialAgenciaContabil'.
     * 
     * @return String
     * @return the value of field 'dsComercialAgenciaContabil'.
     */
    public java.lang.String getDsComercialAgenciaContabil()
    {
        return this._dsComercialAgenciaContabil;
    } //-- java.lang.String getDsComercialAgenciaContabil() 

    /**
     * Returns the value of field 'dsEmpresaSegLinha'.
     * 
     * @return String
     * @return the value of field 'dsEmpresaSegLinha'.
     */
    public java.lang.String getDsEmpresaSegLinha()
    {
        return this._dsEmpresaSegLinha;
    } //-- java.lang.String getDsEmpresaSegLinha() 

    /**
     * Returns the value of field 'dsIndicadorTipoManutencao'.
     * 
     * @return String
     * @return the value of field 'dsIndicadorTipoManutencao'.
     */
    public java.lang.String getDsIndicadorTipoManutencao()
    {
        return this._dsIndicadorTipoManutencao;
    } //-- java.lang.String getDsIndicadorTipoManutencao() 

    /**
     * Returns the value of field 'dsMotivoSituacaoConta'.
     * 
     * @return String
     * @return the value of field 'dsMotivoSituacaoConta'.
     */
    public java.lang.String getDsMotivoSituacaoConta()
    {
        return this._dsMotivoSituacaoConta;
    } //-- java.lang.String getDsMotivoSituacaoConta() 

    /**
     * Returns the value of field 'dsMunicipio'.
     * 
     * @return String
     * @return the value of field 'dsMunicipio'.
     */
    public java.lang.String getDsMunicipio()
    {
        return this._dsMunicipio;
    } //-- java.lang.String getDsMunicipio() 

    /**
     * Returns the value of field 'dsMunicipioFeri'.
     * 
     * @return String
     * @return the value of field 'dsMunicipioFeri'.
     */
    public java.lang.String getDsMunicipioFeri()
    {
        return this._dsMunicipioFeri;
    } //-- java.lang.String getDsMunicipioFeri() 

    /**
     * Returns the value of field 'dsSituacaoVinculo'.
     * 
     * @return String
     * @return the value of field 'dsSituacaoVinculo'.
     */
    public java.lang.String getDsSituacaoVinculo()
    {
        return this._dsSituacaoVinculo;
    } //-- java.lang.String getDsSituacaoVinculo() 

    /**
     * Returns the value of field 'dsTipoConta'.
     * 
     * @return String
     * @return the value of field 'dsTipoConta'.
     */
    public java.lang.String getDsTipoConta()
    {
        return this._dsTipoConta;
    } //-- java.lang.String getDsTipoConta() 

    /**
     * Returns the value of field 'dsUnidadeFederativa'.
     * 
     * @return String
     * @return the value of field 'dsUnidadeFederativa'.
     */
    public java.lang.String getDsUnidadeFederativa()
    {
        return this._dsUnidadeFederativa;
    } //-- java.lang.String getDsUnidadeFederativa() 

    /**
     * Returns the value of field 'dtFimValidadeTed'.
     * 
     * @return String
     * @return the value of field 'dtFimValidadeTed'.
     */
    public java.lang.String getDtFimValidadeTed()
    {
        return this._dtFimValidadeTed;
    } //-- java.lang.String getDtFimValidadeTed() 

    /**
     * Returns the value of field 'dtFinalValorAdicionado'.
     * 
     * @return String
     * @return the value of field 'dtFinalValorAdicionado'.
     */
    public java.lang.String getDtFinalValorAdicionado()
    {
        return this._dtFinalValorAdicionado;
    } //-- java.lang.String getDtFinalValorAdicionado() 

    /**
     * Returns the value of field 'dtInicialValorAdicionado'.
     * 
     * @return String
     * @return the value of field 'dtInicialValorAdicionado'.
     */
    public java.lang.String getDtInicialValorAdicionado()
    {
        return this._dtInicialValorAdicionado;
    } //-- java.lang.String getDtInicialValorAdicionado() 

    /**
     * Returns the value of field 'dtInicioValidadeTed'.
     * 
     * @return String
     * @return the value of field 'dtInicioValidadeTed'.
     */
    public java.lang.String getDtInicioValidadeTed()
    {
        return this._dtInicioValidadeTed;
    } //-- java.lang.String getDtInicioValidadeTed() 

    /**
     * Returns the value of field 'dtVinculoFinal'.
     * 
     * @return String
     * @return the value of field 'dtVinculoFinal'.
     */
    public java.lang.String getDtVinculoFinal()
    {
        return this._dtVinculoFinal;
    } //-- java.lang.String getDtVinculoFinal() 

    /**
     * Returns the value of field 'dtVinculoInicial'.
     * 
     * @return String
     * @return the value of field 'dtVinculoInicial'.
     */
    public java.lang.String getDtVinculoInicial()
    {
        return this._dtVinculoInicial;
    } //-- java.lang.String getDtVinculoInicial() 

    /**
     * Returns the value of field 'hrInclusaoRegistro'.
     * 
     * @return String
     * @return the value of field 'hrInclusaoRegistro'.
     */
    public java.lang.String getHrInclusaoRegistro()
    {
        return this._hrInclusaoRegistro;
    } //-- java.lang.String getHrInclusaoRegistro() 

    /**
     * Returns the value of field 'hrManutencaoRegistro'.
     * 
     * @return String
     * @return the value of field 'hrManutencaoRegistro'.
     */
    public java.lang.String getHrManutencaoRegistro()
    {
        return this._hrManutencaoRegistro;
    } //-- java.lang.String getHrManutencaoRegistro() 

    /**
     * Returns the value of field 'mensagem'.
     * 
     * @return String
     * @return the value of field 'mensagem'.
     */
    public java.lang.String getMensagem()
    {
        return this._mensagem;
    } //-- java.lang.String getMensagem() 

    /**
     * Returns the value of field 'valorAdicionadoContratoConta'.
     * 
     * @return BigDecimal
     * @return the value of field 'valorAdicionadoContratoConta'.
     */
    public java.math.BigDecimal getValorAdicionadoContratoConta()
    {
        return this._valorAdicionadoContratoConta;
    } //-- java.math.BigDecimal getValorAdicionadoContratoConta() 

    /**
     * Returns the value of field 'valorAdicionadoContratoTed'.
     * 
     * @return BigDecimal
     * @return the value of field 'valorAdicionadoContratoTed'.
     */
    public java.math.BigDecimal getValorAdicionadoContratoTed()
    {
        return this._valorAdicionadoContratoTed;
    } //-- java.math.BigDecimal getValorAdicionadoContratoTed() 

    /**
     * Method hasCdBanco
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdBanco()
    {
        return this._has_cdBanco;
    } //-- boolean hasCdBanco() 

    /**
     * Method hasCdComercialAgenciaContabil
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdComercialAgenciaContabil()
    {
        return this._has_cdComercialAgenciaContabil;
    } //-- boolean hasCdComercialAgenciaContabil() 

    /**
     * Method hasCdDigitoContaBancaria
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdDigitoContaBancaria()
    {
        return this._has_cdDigitoContaBancaria;
    } //-- boolean hasCdDigitoContaBancaria() 

    /**
     * Method hasCdIndicadorTipoManutencao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorTipoManutencao()
    {
        return this._has_cdIndicadorTipoManutencao;
    } //-- boolean hasCdIndicadorTipoManutencao() 

    /**
     * Method hasCdMotivoSituacaoConta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMotivoSituacaoConta()
    {
        return this._has_cdMotivoSituacaoConta;
    } //-- boolean hasCdMotivoSituacaoConta() 

    /**
     * Method hasCdMunicipio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMunicipio()
    {
        return this._has_cdMunicipio;
    } //-- boolean hasCdMunicipio() 

    /**
     * Method hasCdMunicipioFeri
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMunicipioFeri()
    {
        return this._has_cdMunicipioFeri;
    } //-- boolean hasCdMunicipioFeri() 

    /**
     * Method hasCdNumeroContaBancaria
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdNumeroContaBancaria()
    {
        return this._has_cdNumeroContaBancaria;
    } //-- boolean hasCdNumeroContaBancaria() 

    /**
     * Method hasCdSituacaoVinculo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSituacaoVinculo()
    {
        return this._has_cdSituacaoVinculo;
    } //-- boolean hasCdSituacaoVinculo() 

    /**
     * Method hasCdTipoCanalInclusao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoCanalInclusao()
    {
        return this._has_cdTipoCanalInclusao;
    } //-- boolean hasCdTipoCanalInclusao() 

    /**
     * Method hasCdTipoCanalManutencao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoCanalManutencao()
    {
        return this._has_cdTipoCanalManutencao;
    } //-- boolean hasCdTipoCanalManutencao() 

    /**
     * Method hasCdTipoConta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoConta()
    {
        return this._has_cdTipoConta;
    } //-- boolean hasCdTipoConta() 

    /**
     * Method hasCdUnidadeFederativa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdUnidadeFederativa()
    {
        return this._has_cdUnidadeFederativa;
    } //-- boolean hasCdUnidadeFederativa() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdBanco'.
     * 
     * @param cdBanco the value of field 'cdBanco'.
     */
    public void setCdBanco(int cdBanco)
    {
        this._cdBanco = cdBanco;
        this._has_cdBanco = true;
    } //-- void setCdBanco(int) 

    /**
     * Sets the value of field 'cdComercialAgenciaContabil'.
     * 
     * @param cdComercialAgenciaContabil the value of field
     * 'cdComercialAgenciaContabil'.
     */
    public void setCdComercialAgenciaContabil(int cdComercialAgenciaContabil)
    {
        this._cdComercialAgenciaContabil = cdComercialAgenciaContabil;
        this._has_cdComercialAgenciaContabil = true;
    } //-- void setCdComercialAgenciaContabil(int) 

    /**
     * Sets the value of field 'cdDigitoContaBancaria'.
     * 
     * @param cdDigitoContaBancaria the value of field
     * 'cdDigitoContaBancaria'.
     */
    public void setCdDigitoContaBancaria(int cdDigitoContaBancaria)
    {
        this._cdDigitoContaBancaria = cdDigitoContaBancaria;
        this._has_cdDigitoContaBancaria = true;
    } //-- void setCdDigitoContaBancaria(int) 

    /**
     * Sets the value of field 'cdIndicadorTipoManutencao'.
     * 
     * @param cdIndicadorTipoManutencao the value of field
     * 'cdIndicadorTipoManutencao'.
     */
    public void setCdIndicadorTipoManutencao(int cdIndicadorTipoManutencao)
    {
        this._cdIndicadorTipoManutencao = cdIndicadorTipoManutencao;
        this._has_cdIndicadorTipoManutencao = true;
    } //-- void setCdIndicadorTipoManutencao(int) 

    /**
     * Sets the value of field 'cdMotivoSituacaoConta'.
     * 
     * @param cdMotivoSituacaoConta the value of field
     * 'cdMotivoSituacaoConta'.
     */
    public void setCdMotivoSituacaoConta(int cdMotivoSituacaoConta)
    {
        this._cdMotivoSituacaoConta = cdMotivoSituacaoConta;
        this._has_cdMotivoSituacaoConta = true;
    } //-- void setCdMotivoSituacaoConta(int) 

    /**
     * Sets the value of field 'cdMunicipio'.
     * 
     * @param cdMunicipio the value of field 'cdMunicipio'.
     */
    public void setCdMunicipio(int cdMunicipio)
    {
        this._cdMunicipio = cdMunicipio;
        this._has_cdMunicipio = true;
    } //-- void setCdMunicipio(int) 

    /**
     * Sets the value of field 'cdMunicipioFeri'.
     * 
     * @param cdMunicipioFeri the value of field 'cdMunicipioFeri'.
     */
    public void setCdMunicipioFeri(int cdMunicipioFeri)
    {
        this._cdMunicipioFeri = cdMunicipioFeri;
        this._has_cdMunicipioFeri = true;
    } //-- void setCdMunicipioFeri(int) 

    /**
     * Sets the value of field 'cdNumeroContaBancaria'.
     * 
     * @param cdNumeroContaBancaria the value of field
     * 'cdNumeroContaBancaria'.
     */
    public void setCdNumeroContaBancaria(long cdNumeroContaBancaria)
    {
        this._cdNumeroContaBancaria = cdNumeroContaBancaria;
        this._has_cdNumeroContaBancaria = true;
    } //-- void setCdNumeroContaBancaria(long) 

    /**
     * Sets the value of field 'cdOperacaoCanalInclusao'.
     * 
     * @param cdOperacaoCanalInclusao the value of field
     * 'cdOperacaoCanalInclusao'.
     */
    public void setCdOperacaoCanalInclusao(java.lang.String cdOperacaoCanalInclusao)
    {
        this._cdOperacaoCanalInclusao = cdOperacaoCanalInclusao;
    } //-- void setCdOperacaoCanalInclusao(java.lang.String) 

    /**
     * Sets the value of field 'cdOperacaoCanalManutencao'.
     * 
     * @param cdOperacaoCanalManutencao the value of field
     * 'cdOperacaoCanalManutencao'.
     */
    public void setCdOperacaoCanalManutencao(java.lang.String cdOperacaoCanalManutencao)
    {
        this._cdOperacaoCanalManutencao = cdOperacaoCanalManutencao;
    } //-- void setCdOperacaoCanalManutencao(java.lang.String) 

    /**
     * Sets the value of field 'cdSituacaoVinculo'.
     * 
     * @param cdSituacaoVinculo the value of field
     * 'cdSituacaoVinculo'.
     */
    public void setCdSituacaoVinculo(int cdSituacaoVinculo)
    {
        this._cdSituacaoVinculo = cdSituacaoVinculo;
        this._has_cdSituacaoVinculo = true;
    } //-- void setCdSituacaoVinculo(int) 

    /**
     * Sets the value of field 'cdTipoCanalInclusao'.
     * 
     * @param cdTipoCanalInclusao the value of field
     * 'cdTipoCanalInclusao'.
     */
    public void setCdTipoCanalInclusao(int cdTipoCanalInclusao)
    {
        this._cdTipoCanalInclusao = cdTipoCanalInclusao;
        this._has_cdTipoCanalInclusao = true;
    } //-- void setCdTipoCanalInclusao(int) 

    /**
     * Sets the value of field 'cdTipoCanalManutencao'.
     * 
     * @param cdTipoCanalManutencao the value of field
     * 'cdTipoCanalManutencao'.
     */
    public void setCdTipoCanalManutencao(int cdTipoCanalManutencao)
    {
        this._cdTipoCanalManutencao = cdTipoCanalManutencao;
        this._has_cdTipoCanalManutencao = true;
    } //-- void setCdTipoCanalManutencao(int) 

    /**
     * Sets the value of field 'cdTipoConta'.
     * 
     * @param cdTipoConta the value of field 'cdTipoConta'.
     */
    public void setCdTipoConta(int cdTipoConta)
    {
        this._cdTipoConta = cdTipoConta;
        this._has_cdTipoConta = true;
    } //-- void setCdTipoConta(int) 

    /**
     * Sets the value of field 'cdUnidadeFederativa'.
     * 
     * @param cdUnidadeFederativa the value of field
     * 'cdUnidadeFederativa'.
     */
    public void setCdUnidadeFederativa(int cdUnidadeFederativa)
    {
        this._cdUnidadeFederativa = cdUnidadeFederativa;
        this._has_cdUnidadeFederativa = true;
    } //-- void setCdUnidadeFederativa(int) 

    /**
     * Sets the value of field 'cdUsuarioInclusao'.
     * 
     * @param cdUsuarioInclusao the value of field
     * 'cdUsuarioInclusao'.
     */
    public void setCdUsuarioInclusao(java.lang.String cdUsuarioInclusao)
    {
        this._cdUsuarioInclusao = cdUsuarioInclusao;
    } //-- void setCdUsuarioInclusao(java.lang.String) 

    /**
     * Sets the value of field 'cdUsuarioInclusaoExterno'.
     * 
     * @param cdUsuarioInclusaoExterno the value of field
     * 'cdUsuarioInclusaoExterno'.
     */
    public void setCdUsuarioInclusaoExterno(java.lang.String cdUsuarioInclusaoExterno)
    {
        this._cdUsuarioInclusaoExterno = cdUsuarioInclusaoExterno;
    } //-- void setCdUsuarioInclusaoExterno(java.lang.String) 

    /**
     * Sets the value of field 'cdUsuarioManutencao'.
     * 
     * @param cdUsuarioManutencao the value of field
     * 'cdUsuarioManutencao'.
     */
    public void setCdUsuarioManutencao(java.lang.String cdUsuarioManutencao)
    {
        this._cdUsuarioManutencao = cdUsuarioManutencao;
    } //-- void setCdUsuarioManutencao(java.lang.String) 

    /**
     * Sets the value of field 'cdUsuarioManutencaoExterno'.
     * 
     * @param cdUsuarioManutencaoExterno the value of field
     * 'cdUsuarioManutencaoExterno'.
     */
    public void setCdUsuarioManutencaoExterno(java.lang.String cdUsuarioManutencaoExterno)
    {
        this._cdUsuarioManutencaoExterno = cdUsuarioManutencaoExterno;
    } //-- void setCdUsuarioManutencaoExterno(java.lang.String) 

    /**
     * Sets the value of field 'codMensagem'.
     * 
     * @param codMensagem the value of field 'codMensagem'.
     */
    public void setCodMensagem(java.lang.String codMensagem)
    {
        this._codMensagem = codMensagem;
    } //-- void setCodMensagem(java.lang.String) 

    /**
     * Sets the value of field 'dsBanco'.
     * 
     * @param dsBanco the value of field 'dsBanco'.
     */
    public void setDsBanco(java.lang.String dsBanco)
    {
        this._dsBanco = dsBanco;
    } //-- void setDsBanco(java.lang.String) 

    /**
     * Sets the value of field 'dsCanalInclusao'.
     * 
     * @param dsCanalInclusao the value of field 'dsCanalInclusao'.
     */
    public void setDsCanalInclusao(java.lang.String dsCanalInclusao)
    {
        this._dsCanalInclusao = dsCanalInclusao;
    } //-- void setDsCanalInclusao(java.lang.String) 

    /**
     * Sets the value of field 'dsCanalManutencao'.
     * 
     * @param dsCanalManutencao the value of field
     * 'dsCanalManutencao'.
     */
    public void setDsCanalManutencao(java.lang.String dsCanalManutencao)
    {
        this._dsCanalManutencao = dsCanalManutencao;
    } //-- void setDsCanalManutencao(java.lang.String) 

    /**
     * Sets the value of field 'dsComercialAgenciaContabil'.
     * 
     * @param dsComercialAgenciaContabil the value of field
     * 'dsComercialAgenciaContabil'.
     */
    public void setDsComercialAgenciaContabil(java.lang.String dsComercialAgenciaContabil)
    {
        this._dsComercialAgenciaContabil = dsComercialAgenciaContabil;
    } //-- void setDsComercialAgenciaContabil(java.lang.String) 

    /**
     * Sets the value of field 'dsEmpresaSegLinha'.
     * 
     * @param dsEmpresaSegLinha the value of field
     * 'dsEmpresaSegLinha'.
     */
    public void setDsEmpresaSegLinha(java.lang.String dsEmpresaSegLinha)
    {
        this._dsEmpresaSegLinha = dsEmpresaSegLinha;
    } //-- void setDsEmpresaSegLinha(java.lang.String) 

    /**
     * Sets the value of field 'dsIndicadorTipoManutencao'.
     * 
     * @param dsIndicadorTipoManutencao the value of field
     * 'dsIndicadorTipoManutencao'.
     */
    public void setDsIndicadorTipoManutencao(java.lang.String dsIndicadorTipoManutencao)
    {
        this._dsIndicadorTipoManutencao = dsIndicadorTipoManutencao;
    } //-- void setDsIndicadorTipoManutencao(java.lang.String) 

    /**
     * Sets the value of field 'dsMotivoSituacaoConta'.
     * 
     * @param dsMotivoSituacaoConta the value of field
     * 'dsMotivoSituacaoConta'.
     */
    public void setDsMotivoSituacaoConta(java.lang.String dsMotivoSituacaoConta)
    {
        this._dsMotivoSituacaoConta = dsMotivoSituacaoConta;
    } //-- void setDsMotivoSituacaoConta(java.lang.String) 

    /**
     * Sets the value of field 'dsMunicipio'.
     * 
     * @param dsMunicipio the value of field 'dsMunicipio'.
     */
    public void setDsMunicipio(java.lang.String dsMunicipio)
    {
        this._dsMunicipio = dsMunicipio;
    } //-- void setDsMunicipio(java.lang.String) 

    /**
     * Sets the value of field 'dsMunicipioFeri'.
     * 
     * @param dsMunicipioFeri the value of field 'dsMunicipioFeri'.
     */
    public void setDsMunicipioFeri(java.lang.String dsMunicipioFeri)
    {
        this._dsMunicipioFeri = dsMunicipioFeri;
    } //-- void setDsMunicipioFeri(java.lang.String) 

    /**
     * Sets the value of field 'dsSituacaoVinculo'.
     * 
     * @param dsSituacaoVinculo the value of field
     * 'dsSituacaoVinculo'.
     */
    public void setDsSituacaoVinculo(java.lang.String dsSituacaoVinculo)
    {
        this._dsSituacaoVinculo = dsSituacaoVinculo;
    } //-- void setDsSituacaoVinculo(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoConta'.
     * 
     * @param dsTipoConta the value of field 'dsTipoConta'.
     */
    public void setDsTipoConta(java.lang.String dsTipoConta)
    {
        this._dsTipoConta = dsTipoConta;
    } //-- void setDsTipoConta(java.lang.String) 

    /**
     * Sets the value of field 'dsUnidadeFederativa'.
     * 
     * @param dsUnidadeFederativa the value of field
     * 'dsUnidadeFederativa'.
     */
    public void setDsUnidadeFederativa(java.lang.String dsUnidadeFederativa)
    {
        this._dsUnidadeFederativa = dsUnidadeFederativa;
    } //-- void setDsUnidadeFederativa(java.lang.String) 

    /**
     * Sets the value of field 'dtFimValidadeTed'.
     * 
     * @param dtFimValidadeTed the value of field 'dtFimValidadeTed'
     */
    public void setDtFimValidadeTed(java.lang.String dtFimValidadeTed)
    {
        this._dtFimValidadeTed = dtFimValidadeTed;
    } //-- void setDtFimValidadeTed(java.lang.String) 

    /**
     * Sets the value of field 'dtFinalValorAdicionado'.
     * 
     * @param dtFinalValorAdicionado the value of field
     * 'dtFinalValorAdicionado'.
     */
    public void setDtFinalValorAdicionado(java.lang.String dtFinalValorAdicionado)
    {
        this._dtFinalValorAdicionado = dtFinalValorAdicionado;
    } //-- void setDtFinalValorAdicionado(java.lang.String) 

    /**
     * Sets the value of field 'dtInicialValorAdicionado'.
     * 
     * @param dtInicialValorAdicionado the value of field
     * 'dtInicialValorAdicionado'.
     */
    public void setDtInicialValorAdicionado(java.lang.String dtInicialValorAdicionado)
    {
        this._dtInicialValorAdicionado = dtInicialValorAdicionado;
    } //-- void setDtInicialValorAdicionado(java.lang.String) 

    /**
     * Sets the value of field 'dtInicioValidadeTed'.
     * 
     * @param dtInicioValidadeTed the value of field
     * 'dtInicioValidadeTed'.
     */
    public void setDtInicioValidadeTed(java.lang.String dtInicioValidadeTed)
    {
        this._dtInicioValidadeTed = dtInicioValidadeTed;
    } //-- void setDtInicioValidadeTed(java.lang.String) 

    /**
     * Sets the value of field 'dtVinculoFinal'.
     * 
     * @param dtVinculoFinal the value of field 'dtVinculoFinal'.
     */
    public void setDtVinculoFinal(java.lang.String dtVinculoFinal)
    {
        this._dtVinculoFinal = dtVinculoFinal;
    } //-- void setDtVinculoFinal(java.lang.String) 

    /**
     * Sets the value of field 'dtVinculoInicial'.
     * 
     * @param dtVinculoInicial the value of field 'dtVinculoInicial'
     */
    public void setDtVinculoInicial(java.lang.String dtVinculoInicial)
    {
        this._dtVinculoInicial = dtVinculoInicial;
    } //-- void setDtVinculoInicial(java.lang.String) 

    /**
     * Sets the value of field 'hrInclusaoRegistro'.
     * 
     * @param hrInclusaoRegistro the value of field
     * 'hrInclusaoRegistro'.
     */
    public void setHrInclusaoRegistro(java.lang.String hrInclusaoRegistro)
    {
        this._hrInclusaoRegistro = hrInclusaoRegistro;
    } //-- void setHrInclusaoRegistro(java.lang.String) 

    /**
     * Sets the value of field 'hrManutencaoRegistro'.
     * 
     * @param hrManutencaoRegistro the value of field
     * 'hrManutencaoRegistro'.
     */
    public void setHrManutencaoRegistro(java.lang.String hrManutencaoRegistro)
    {
        this._hrManutencaoRegistro = hrManutencaoRegistro;
    } //-- void setHrManutencaoRegistro(java.lang.String) 

    /**
     * Sets the value of field 'mensagem'.
     * 
     * @param mensagem the value of field 'mensagem'.
     */
    public void setMensagem(java.lang.String mensagem)
    {
        this._mensagem = mensagem;
    } //-- void setMensagem(java.lang.String) 

    /**
     * Sets the value of field 'valorAdicionadoContratoConta'.
     * 
     * @param valorAdicionadoContratoConta the value of field
     * 'valorAdicionadoContratoConta'.
     */
    public void setValorAdicionadoContratoConta(java.math.BigDecimal valorAdicionadoContratoConta)
    {
        this._valorAdicionadoContratoConta = valorAdicionadoContratoConta;
    } //-- void setValorAdicionadoContratoConta(java.math.BigDecimal) 

    /**
     * Sets the value of field 'valorAdicionadoContratoTed'.
     * 
     * @param valorAdicionadoContratoTed the value of field
     * 'valorAdicionadoContratoTed'.
     */
    public void setValorAdicionadoContratoTed(java.math.BigDecimal valorAdicionadoContratoTed)
    {
        this._valorAdicionadoContratoTed = valorAdicionadoContratoTed;
    } //-- void setValorAdicionadoContratoTed(java.math.BigDecimal) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ConsultarConManContaResponse
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultarconmanconta.response.ConsultarConManContaResponse unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultarconmanconta.response.ConsultarConManContaResponse) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultarconmanconta.response.ConsultarConManContaResponse.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarconmanconta.response.ConsultarConManContaResponse unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
