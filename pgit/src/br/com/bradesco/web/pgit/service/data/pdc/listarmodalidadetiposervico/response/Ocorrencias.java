/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listarmodalidadetiposervico.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdParametroTela
     */
    private int _cdParametroTela = 0;

    /**
     * keeps track of state for field: _cdParametroTela
     */
    private boolean _has_cdParametroTela;

    /**
     * Field _cdProdutoServicoOperacao
     */
    private int _cdProdutoServicoOperacao = 0;

    /**
     * keeps track of state for field: _cdProdutoServicoOperacao
     */
    private boolean _has_cdProdutoServicoOperacao;

    /**
     * Field _dsProdutoServicoOperacao
     */
    private java.lang.String _dsProdutoServicoOperacao;

    /**
     * Field _cdProdutoOperacaoRelacionado
     */
    private int _cdProdutoOperacaoRelacionado = 0;

    /**
     * keeps track of state for field: _cdProdutoOperacaoRelacionado
     */
    private boolean _has_cdProdutoOperacaoRelacionado;

    /**
     * Field _dsProdutoOperacaoRelacionado
     */
    private java.lang.String _dsProdutoOperacaoRelacionado;

    /**
     * Field _cdRelacionamentoProdutoProduto
     */
    private int _cdRelacionamentoProdutoProduto = 0;

    /**
     * keeps track of state for field:
     * _cdRelacionamentoProdutoProduto
     */
    private boolean _has_cdRelacionamentoProdutoProduto;

    /**
     * Field _dtInclusaoServico
     */
    private java.lang.String _dtInclusaoServico;

    /**
     * Field _cdSituacaoServicoRelacionado
     */
    private int _cdSituacaoServicoRelacionado = 0;

    /**
     * keeps track of state for field: _cdSituacaoServicoRelacionado
     */
    private boolean _has_cdSituacaoServicoRelacionado;

    /**
     * Field _dsSituacaoServicoRelacionado
     */
    private java.lang.String _dsSituacaoServicoRelacionado;

    /**
     * Field _qtdeModalidade
     */
    private int _qtdeModalidade = 0;

    /**
     * keeps track of state for field: _qtdeModalidade
     */
    private boolean _has_qtdeModalidade;

    /**
     * Field _cdAmbienteServicoContrato
     */
    private java.lang.String _cdAmbienteServicoContrato;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarmodalidadetiposervico.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdParametroTela
     * 
     */
    public void deleteCdParametroTela()
    {
        this._has_cdParametroTela= false;
    } //-- void deleteCdParametroTela() 

    /**
     * Method deleteCdProdutoOperacaoRelacionado
     * 
     */
    public void deleteCdProdutoOperacaoRelacionado()
    {
        this._has_cdProdutoOperacaoRelacionado= false;
    } //-- void deleteCdProdutoOperacaoRelacionado() 

    /**
     * Method deleteCdProdutoServicoOperacao
     * 
     */
    public void deleteCdProdutoServicoOperacao()
    {
        this._has_cdProdutoServicoOperacao= false;
    } //-- void deleteCdProdutoServicoOperacao() 

    /**
     * Method deleteCdRelacionamentoProdutoProduto
     * 
     */
    public void deleteCdRelacionamentoProdutoProduto()
    {
        this._has_cdRelacionamentoProdutoProduto= false;
    } //-- void deleteCdRelacionamentoProdutoProduto() 

    /**
     * Method deleteCdSituacaoServicoRelacionado
     * 
     */
    public void deleteCdSituacaoServicoRelacionado()
    {
        this._has_cdSituacaoServicoRelacionado= false;
    } //-- void deleteCdSituacaoServicoRelacionado() 

    /**
     * Method deleteQtdeModalidade
     * 
     */
    public void deleteQtdeModalidade()
    {
        this._has_qtdeModalidade= false;
    } //-- void deleteQtdeModalidade() 

    /**
     * Returns the value of field 'cdAmbienteServicoContrato'.
     * 
     * @return String
     * @return the value of field 'cdAmbienteServicoContrato'.
     */
    public java.lang.String getCdAmbienteServicoContrato()
    {
        return this._cdAmbienteServicoContrato;
    } //-- java.lang.String getCdAmbienteServicoContrato() 

    /**
     * Returns the value of field 'cdParametroTela'.
     * 
     * @return int
     * @return the value of field 'cdParametroTela'.
     */
    public int getCdParametroTela()
    {
        return this._cdParametroTela;
    } //-- int getCdParametroTela() 

    /**
     * Returns the value of field 'cdProdutoOperacaoRelacionado'.
     * 
     * @return int
     * @return the value of field 'cdProdutoOperacaoRelacionado'.
     */
    public int getCdProdutoOperacaoRelacionado()
    {
        return this._cdProdutoOperacaoRelacionado;
    } //-- int getCdProdutoOperacaoRelacionado() 

    /**
     * Returns the value of field 'cdProdutoServicoOperacao'.
     * 
     * @return int
     * @return the value of field 'cdProdutoServicoOperacao'.
     */
    public int getCdProdutoServicoOperacao()
    {
        return this._cdProdutoServicoOperacao;
    } //-- int getCdProdutoServicoOperacao() 

    /**
     * Returns the value of field 'cdRelacionamentoProdutoProduto'.
     * 
     * @return int
     * @return the value of field 'cdRelacionamentoProdutoProduto'.
     */
    public int getCdRelacionamentoProdutoProduto()
    {
        return this._cdRelacionamentoProdutoProduto;
    } //-- int getCdRelacionamentoProdutoProduto() 

    /**
     * Returns the value of field 'cdSituacaoServicoRelacionado'.
     * 
     * @return int
     * @return the value of field 'cdSituacaoServicoRelacionado'.
     */
    public int getCdSituacaoServicoRelacionado()
    {
        return this._cdSituacaoServicoRelacionado;
    } //-- int getCdSituacaoServicoRelacionado() 

    /**
     * Returns the value of field 'dsProdutoOperacaoRelacionado'.
     * 
     * @return String
     * @return the value of field 'dsProdutoOperacaoRelacionado'.
     */
    public java.lang.String getDsProdutoOperacaoRelacionado()
    {
        return this._dsProdutoOperacaoRelacionado;
    } //-- java.lang.String getDsProdutoOperacaoRelacionado() 

    /**
     * Returns the value of field 'dsProdutoServicoOperacao'.
     * 
     * @return String
     * @return the value of field 'dsProdutoServicoOperacao'.
     */
    public java.lang.String getDsProdutoServicoOperacao()
    {
        return this._dsProdutoServicoOperacao;
    } //-- java.lang.String getDsProdutoServicoOperacao() 

    /**
     * Returns the value of field 'dsSituacaoServicoRelacionado'.
     * 
     * @return String
     * @return the value of field 'dsSituacaoServicoRelacionado'.
     */
    public java.lang.String getDsSituacaoServicoRelacionado()
    {
        return this._dsSituacaoServicoRelacionado;
    } //-- java.lang.String getDsSituacaoServicoRelacionado() 

    /**
     * Returns the value of field 'dtInclusaoServico'.
     * 
     * @return String
     * @return the value of field 'dtInclusaoServico'.
     */
    public java.lang.String getDtInclusaoServico()
    {
        return this._dtInclusaoServico;
    } //-- java.lang.String getDtInclusaoServico() 

    /**
     * Returns the value of field 'qtdeModalidade'.
     * 
     * @return int
     * @return the value of field 'qtdeModalidade'.
     */
    public int getQtdeModalidade()
    {
        return this._qtdeModalidade;
    } //-- int getQtdeModalidade() 

    /**
     * Method hasCdParametroTela
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdParametroTela()
    {
        return this._has_cdParametroTela;
    } //-- boolean hasCdParametroTela() 

    /**
     * Method hasCdProdutoOperacaoRelacionado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdutoOperacaoRelacionado()
    {
        return this._has_cdProdutoOperacaoRelacionado;
    } //-- boolean hasCdProdutoOperacaoRelacionado() 

    /**
     * Method hasCdProdutoServicoOperacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdutoServicoOperacao()
    {
        return this._has_cdProdutoServicoOperacao;
    } //-- boolean hasCdProdutoServicoOperacao() 

    /**
     * Method hasCdRelacionamentoProdutoProduto
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdRelacionamentoProdutoProduto()
    {
        return this._has_cdRelacionamentoProdutoProduto;
    } //-- boolean hasCdRelacionamentoProdutoProduto() 

    /**
     * Method hasCdSituacaoServicoRelacionado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSituacaoServicoRelacionado()
    {
        return this._has_cdSituacaoServicoRelacionado;
    } //-- boolean hasCdSituacaoServicoRelacionado() 

    /**
     * Method hasQtdeModalidade
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtdeModalidade()
    {
        return this._has_qtdeModalidade;
    } //-- boolean hasQtdeModalidade() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdAmbienteServicoContrato'.
     * 
     * @param cdAmbienteServicoContrato the value of field
     * 'cdAmbienteServicoContrato'.
     */
    public void setCdAmbienteServicoContrato(java.lang.String cdAmbienteServicoContrato)
    {
        this._cdAmbienteServicoContrato = cdAmbienteServicoContrato;
    } //-- void setCdAmbienteServicoContrato(java.lang.String) 

    /**
     * Sets the value of field 'cdParametroTela'.
     * 
     * @param cdParametroTela the value of field 'cdParametroTela'.
     */
    public void setCdParametroTela(int cdParametroTela)
    {
        this._cdParametroTela = cdParametroTela;
        this._has_cdParametroTela = true;
    } //-- void setCdParametroTela(int) 

    /**
     * Sets the value of field 'cdProdutoOperacaoRelacionado'.
     * 
     * @param cdProdutoOperacaoRelacionado the value of field
     * 'cdProdutoOperacaoRelacionado'.
     */
    public void setCdProdutoOperacaoRelacionado(int cdProdutoOperacaoRelacionado)
    {
        this._cdProdutoOperacaoRelacionado = cdProdutoOperacaoRelacionado;
        this._has_cdProdutoOperacaoRelacionado = true;
    } //-- void setCdProdutoOperacaoRelacionado(int) 

    /**
     * Sets the value of field 'cdProdutoServicoOperacao'.
     * 
     * @param cdProdutoServicoOperacao the value of field
     * 'cdProdutoServicoOperacao'.
     */
    public void setCdProdutoServicoOperacao(int cdProdutoServicoOperacao)
    {
        this._cdProdutoServicoOperacao = cdProdutoServicoOperacao;
        this._has_cdProdutoServicoOperacao = true;
    } //-- void setCdProdutoServicoOperacao(int) 

    /**
     * Sets the value of field 'cdRelacionamentoProdutoProduto'.
     * 
     * @param cdRelacionamentoProdutoProduto the value of field
     * 'cdRelacionamentoProdutoProduto'.
     */
    public void setCdRelacionamentoProdutoProduto(int cdRelacionamentoProdutoProduto)
    {
        this._cdRelacionamentoProdutoProduto = cdRelacionamentoProdutoProduto;
        this._has_cdRelacionamentoProdutoProduto = true;
    } //-- void setCdRelacionamentoProdutoProduto(int) 

    /**
     * Sets the value of field 'cdSituacaoServicoRelacionado'.
     * 
     * @param cdSituacaoServicoRelacionado the value of field
     * 'cdSituacaoServicoRelacionado'.
     */
    public void setCdSituacaoServicoRelacionado(int cdSituacaoServicoRelacionado)
    {
        this._cdSituacaoServicoRelacionado = cdSituacaoServicoRelacionado;
        this._has_cdSituacaoServicoRelacionado = true;
    } //-- void setCdSituacaoServicoRelacionado(int) 

    /**
     * Sets the value of field 'dsProdutoOperacaoRelacionado'.
     * 
     * @param dsProdutoOperacaoRelacionado the value of field
     * 'dsProdutoOperacaoRelacionado'.
     */
    public void setDsProdutoOperacaoRelacionado(java.lang.String dsProdutoOperacaoRelacionado)
    {
        this._dsProdutoOperacaoRelacionado = dsProdutoOperacaoRelacionado;
    } //-- void setDsProdutoOperacaoRelacionado(java.lang.String) 

    /**
     * Sets the value of field 'dsProdutoServicoOperacao'.
     * 
     * @param dsProdutoServicoOperacao the value of field
     * 'dsProdutoServicoOperacao'.
     */
    public void setDsProdutoServicoOperacao(java.lang.String dsProdutoServicoOperacao)
    {
        this._dsProdutoServicoOperacao = dsProdutoServicoOperacao;
    } //-- void setDsProdutoServicoOperacao(java.lang.String) 

    /**
     * Sets the value of field 'dsSituacaoServicoRelacionado'.
     * 
     * @param dsSituacaoServicoRelacionado the value of field
     * 'dsSituacaoServicoRelacionado'.
     */
    public void setDsSituacaoServicoRelacionado(java.lang.String dsSituacaoServicoRelacionado)
    {
        this._dsSituacaoServicoRelacionado = dsSituacaoServicoRelacionado;
    } //-- void setDsSituacaoServicoRelacionado(java.lang.String) 

    /**
     * Sets the value of field 'dtInclusaoServico'.
     * 
     * @param dtInclusaoServico the value of field
     * 'dtInclusaoServico'.
     */
    public void setDtInclusaoServico(java.lang.String dtInclusaoServico)
    {
        this._dtInclusaoServico = dtInclusaoServico;
    } //-- void setDtInclusaoServico(java.lang.String) 

    /**
     * Sets the value of field 'qtdeModalidade'.
     * 
     * @param qtdeModalidade the value of field 'qtdeModalidade'.
     */
    public void setQtdeModalidade(int qtdeModalidade)
    {
        this._qtdeModalidade = qtdeModalidade;
        this._has_qtdeModalidade = true;
    } //-- void setQtdeModalidade(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listarmodalidadetiposervico.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listarmodalidadetiposervico.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listarmodalidadetiposervico.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarmodalidadetiposervico.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
