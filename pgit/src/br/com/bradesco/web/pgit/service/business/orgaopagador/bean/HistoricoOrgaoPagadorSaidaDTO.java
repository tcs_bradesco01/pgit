/*
 * Nome: br.com.bradesco.web.pgit.service.business.orgaopagador.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.orgaopagador.bean;

import java.io.Serializable;

/**
 * Nome: HistoricoOrgaoPagadorSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class HistoricoOrgaoPagadorSaidaDTO implements Serializable {

	/** Atributo serialVersionUID. */
	private static final long serialVersionUID = 4199951423061579779L;

	/** Atributo cdOrgaoPagador. */
	private Integer cdOrgaoPagador;

    /** Atributo horario. */
    private String horario;

    /** Atributo cdTipoUnidadeOrganizacional. */
    private Integer cdTipoUnidadeOrganizacional;

    /** Atributo dsTipoUnidadeOrganizacional. */
    private String dsTipoUnidadeOrganizacional;

    /** Atributo cdPessoaJuridica. */
    private long cdPessoaJuridica;

    /** Atributo dsPessoJuridica. */
    private String dsPessoJuridica;

    /** Atributo nrSeqUnidadeOrganizacional. */
    private Integer nrSeqUnidadeOrganizacional;

    /** Atributo dsSeqUnidadeOrganizacional. */
    private String dsSeqUnidadeOrganizacional;

    /** Atributo dsSituacao. */
    private Integer dsSituacao;
    
    /** Atributo dsUsuarioResponsavel. */
    private String dsUsuarioResponsavel;

    /** Atributo cdIndicadorTipoManutencao. */
    private Integer cdIndicadorTipoManutencao;

    /** Atributo cdTarifOrgaoPagador. */
    private Integer cdTarifOrgaoPagador;
    
    /** Atributo cdCanalInclusao. */
    private Integer cdCanalInclusao;

    /** Atributo dsCanalInclusao. */
    private String dsCanalInclusao;
    
    /** Atributo cdAutenSegrcInclusao. */
    private String cdAutenSegrcInclusao;

    /** Atributo nmOperFluxoInclusao. */
    private String nmOperFluxoInclusao;

    /** Atributo hrInclusaoRegistro. */
    private String hrInclusaoRegistro;

    /** Atributo cdCanalManutencao. */
    private Integer cdCanalManutencao;

    /** Atributo dsCanalManutencao. */
    private String dsCanalManutencao;

    /** Atributo cdAutenSegrcManutencao. */
    private String cdAutenSegrcManutencao;

    /** Atributo nmOperFluxoManutencao. */
    private String nmOperFluxoManutencao;

    /** Atributo hrManutencaoRegistro. */
    private String hrManutencaoRegistro;

    /** Atributo dsMotivoBloqueio. */
    private String dsMotivoBloqueio;

	/**
	 * Get: cdIndicadorTipoManutencao.
	 *
	 * @return cdIndicadorTipoManutencao
	 */
	public Integer getCdIndicadorTipoManutencao() {
		return cdIndicadorTipoManutencao;
	}

	/**
	 * Set: cdIndicadorTipoManutencao.
	 *
	 * @param cdIndicadorTipoManutencao the cd indicador tipo manutencao
	 */
	public void setCdIndicadorTipoManutencao(Integer cdIndicadorTipoManutencao) {
		this.cdIndicadorTipoManutencao = cdIndicadorTipoManutencao;
	}

	/**
	 * Get: cdOrgaoPagador.
	 *
	 * @return cdOrgaoPagador
	 */
	public Integer getCdOrgaoPagador() {
		return cdOrgaoPagador;
	}

	/**
	 * Set: cdOrgaoPagador.
	 *
	 * @param cdOrgaoPagador the cd orgao pagador
	 */
	public void setCdOrgaoPagador(Integer cdOrgaoPagador) {
		this.cdOrgaoPagador = cdOrgaoPagador;
	}

	/**
	 * Get: cdPessoaJuridica.
	 *
	 * @return cdPessoaJuridica
	 */
	public long getCdPessoaJuridica() {
		return cdPessoaJuridica;
	}

	/**
	 * Set: cdPessoaJuridica.
	 *
	 * @param cdPessoaJuridica the cd pessoa juridica
	 */
	public void setCdPessoaJuridica(long cdPessoaJuridica) {
		this.cdPessoaJuridica = cdPessoaJuridica;
	}

	/**
	 * Get: cdTipoUnidadeOrganizacional.
	 *
	 * @return cdTipoUnidadeOrganizacional
	 */
	public Integer getCdTipoUnidadeOrganizacional() {
		return cdTipoUnidadeOrganizacional;
	}

	/**
	 * Set: cdTipoUnidadeOrganizacional.
	 *
	 * @param cdTipoUnidadeOrganizacional the cd tipo unidade organizacional
	 */
	public void setCdTipoUnidadeOrganizacional(Integer cdTipoUnidadeOrganizacional) {
		this.cdTipoUnidadeOrganizacional = cdTipoUnidadeOrganizacional;
	}

	/**
	 * Get: dsPessoJuridica.
	 *
	 * @return dsPessoJuridica
	 */
	public String getDsPessoJuridica() {
		return dsPessoJuridica;
	}

	/**
	 * Set: dsPessoJuridica.
	 *
	 * @param dsPessoJuridica the ds pesso juridica
	 */
	public void setDsPessoJuridica(String dsPessoJuridica) {
		this.dsPessoJuridica = dsPessoJuridica;
	}

	/**
	 * Get: dsSeqUnidadeOrganizacional.
	 *
	 * @return dsSeqUnidadeOrganizacional
	 */
	public String getDsSeqUnidadeOrganizacional() {
		return dsSeqUnidadeOrganizacional;
	}

	/**
	 * Set: dsSeqUnidadeOrganizacional.
	 *
	 * @param dsSeqUnidadeOrganizacional the ds seq unidade organizacional
	 */
	public void setDsSeqUnidadeOrganizacional(String dsSeqUnidadeOrganizacional) {
		this.dsSeqUnidadeOrganizacional = dsSeqUnidadeOrganizacional;
	}

	/**
	 * Get: dsSituacao.
	 *
	 * @return dsSituacao
	 */
	public Integer getDsSituacao() {
		return dsSituacao;
	}

	/**
	 * Set: dsSituacao.
	 *
	 * @param dsSituacao the ds situacao
	 */
	public void setDsSituacao(Integer dsSituacao) {
		this.dsSituacao = dsSituacao;
	}

	/**
	 * Get: dsTipoUnidadeOrganizacional.
	 *
	 * @return dsTipoUnidadeOrganizacional
	 */
	public String getDsTipoUnidadeOrganizacional() {
		return dsTipoUnidadeOrganizacional;
	}

	/**
	 * Set: dsTipoUnidadeOrganizacional.
	 *
	 * @param dsTipoUnidadeOrganizacional the ds tipo unidade organizacional
	 */
	public void setDsTipoUnidadeOrganizacional(String dsTipoUnidadeOrganizacional) {
		this.dsTipoUnidadeOrganizacional = dsTipoUnidadeOrganizacional;
	}

	/**
	 * Get: dsUsuarioResponsavel.
	 *
	 * @return dsUsuarioResponsavel
	 */
	public String getDsUsuarioResponsavel() {
		return dsUsuarioResponsavel;
	}

	/**
	 * Set: dsUsuarioResponsavel.
	 *
	 * @param dsUsuarioResponsavel the ds usuario responsavel
	 */
	public void setDsUsuarioResponsavel(String dsUsuarioResponsavel) {
		this.dsUsuarioResponsavel = dsUsuarioResponsavel;
	}

	/**
	 * Get: horario.
	 *
	 * @return horario
	 */
	public String getHorario() {
		return horario;
	}

	/**
	 * Set: horario.
	 *
	 * @param horario the horario
	 */
	public void setHorario(String horario) {
		this.horario = horario;
	}

	/**
	 * Get: nrSeqUnidadeOrganizacional.
	 *
	 * @return nrSeqUnidadeOrganizacional
	 */
	public Integer getNrSeqUnidadeOrganizacional() {
		return nrSeqUnidadeOrganizacional;
	}

	/**
	 * Set: nrSeqUnidadeOrganizacional.
	 *
	 * @param nrSeqUnidadeOrganizacional the nr seq unidade organizacional
	 */
	public void setNrSeqUnidadeOrganizacional(Integer nrSeqUnidadeOrganizacional) {
		this.nrSeqUnidadeOrganizacional = nrSeqUnidadeOrganizacional;
	}

	/**
	 * Get: cdAutenSegrcInclusao.
	 *
	 * @return cdAutenSegrcInclusao
	 */
	public String getCdAutenSegrcInclusao() {
		return cdAutenSegrcInclusao;
	}

	/**
	 * Set: cdAutenSegrcInclusao.
	 *
	 * @param cdAutenSegrcInclusao the cd auten segrc inclusao
	 */
	public void setCdAutenSegrcInclusao(String cdAutenSegrcInclusao) {
		this.cdAutenSegrcInclusao = cdAutenSegrcInclusao;
	}

	/**
	 * Get: cdAutenSegrcManutencao.
	 *
	 * @return cdAutenSegrcManutencao
	 */
	public String getCdAutenSegrcManutencao() {
		return cdAutenSegrcManutencao;
	}

	/**
	 * Set: cdAutenSegrcManutencao.
	 *
	 * @param cdAutenSegrcManutencao the cd auten segrc manutencao
	 */
	public void setCdAutenSegrcManutencao(String cdAutenSegrcManutencao) {
		this.cdAutenSegrcManutencao = cdAutenSegrcManutencao;
	}

	/**
	 * Get: cdCanalInclusao.
	 *
	 * @return cdCanalInclusao
	 */
	public Integer getCdCanalInclusao() {
		return cdCanalInclusao;
	}

	/**
	 * Set: cdCanalInclusao.
	 *
	 * @param cdCanalInclusao the cd canal inclusao
	 */
	public void setCdCanalInclusao(Integer cdCanalInclusao) {
		this.cdCanalInclusao = cdCanalInclusao;
	}

	/**
	 * Get: cdCanalManutencao.
	 *
	 * @return cdCanalManutencao
	 */
	public Integer getCdCanalManutencao() {
		return cdCanalManutencao;
	}

	/**
	 * Set: cdCanalManutencao.
	 *
	 * @param cdCanalManutencao the cd canal manutencao
	 */
	public void setCdCanalManutencao(Integer cdCanalManutencao) {
		this.cdCanalManutencao = cdCanalManutencao;
	}

	/**
	 * Get: cdTarifOrgaoPagador.
	 *
	 * @return cdTarifOrgaoPagador
	 */
	public Integer getCdTarifOrgaoPagador() {
		return cdTarifOrgaoPagador;
	}

	/**
	 * Set: cdTarifOrgaoPagador.
	 *
	 * @param cdTarifOrgaoPagador the cd tarif orgao pagador
	 */
	public void setCdTarifOrgaoPagador(Integer cdTarifOrgaoPagador) {
		this.cdTarifOrgaoPagador = cdTarifOrgaoPagador;
	}

	/**
	 * Get: dsCanalInclusao.
	 *
	 * @return dsCanalInclusao
	 */
	public String getDsCanalInclusao() {
		return dsCanalInclusao;
	}

	/**
	 * Set: dsCanalInclusao.
	 *
	 * @param dsCanalInclusao the ds canal inclusao
	 */
	public void setDsCanalInclusao(String dsCanalInclusao) {
		this.dsCanalInclusao = dsCanalInclusao;
	}

	/**
	 * Get: dsCanalManutencao.
	 *
	 * @return dsCanalManutencao
	 */
	public String getDsCanalManutencao() {
		return dsCanalManutencao;
	}

	/**
	 * Set: dsCanalManutencao.
	 *
	 * @param dsCanalManutencao the ds canal manutencao
	 */
	public void setDsCanalManutencao(String dsCanalManutencao) {
		this.dsCanalManutencao = dsCanalManutencao;
	}

	/**
	 * Get: dsMotivoBloqueio.
	 *
	 * @return dsMotivoBloqueio
	 */
	public String getDsMotivoBloqueio() {
		return dsMotivoBloqueio;
	}

	/**
	 * Set: dsMotivoBloqueio.
	 *
	 * @param dsMotivoBloqueio the ds motivo bloqueio
	 */
	public void setDsMotivoBloqueio(String dsMotivoBloqueio) {
		this.dsMotivoBloqueio = dsMotivoBloqueio;
	}

	/**
	 * Get: hrInclusaoRegistro.
	 *
	 * @return hrInclusaoRegistro
	 */
	public String getHrInclusaoRegistro() {
		return hrInclusaoRegistro;
	}

	/**
	 * Set: hrInclusaoRegistro.
	 *
	 * @param hrInclusaoRegistro the hr inclusao registro
	 */
	public void setHrInclusaoRegistro(String hrInclusaoRegistro) {
		this.hrInclusaoRegistro = hrInclusaoRegistro;
	}

	/**
	 * Get: hrManutencaoRegistro.
	 *
	 * @return hrManutencaoRegistro
	 */
	public String getHrManutencaoRegistro() {
		return hrManutencaoRegistro;
	}

	/**
	 * Set: hrManutencaoRegistro.
	 *
	 * @param hrManutencaoRegistro the hr manutencao registro
	 */
	public void setHrManutencaoRegistro(String hrManutencaoRegistro) {
		this.hrManutencaoRegistro = hrManutencaoRegistro;
	}

	/**
	 * Get: nmOperFluxoInclusao.
	 *
	 * @return nmOperFluxoInclusao
	 */
	public String getNmOperFluxoInclusao() {
		return nmOperFluxoInclusao;
	}

	/**
	 * Set: nmOperFluxoInclusao.
	 *
	 * @param nmOperFluxoInclusao the nm oper fluxo inclusao
	 */
	public void setNmOperFluxoInclusao(String nmOperFluxoInclusao) {
		this.nmOperFluxoInclusao = nmOperFluxoInclusao;
	}

	/**
	 * Get: nmOperFluxoManutencao.
	 *
	 * @return nmOperFluxoManutencao
	 */
	public String getNmOperFluxoManutencao() {
		return nmOperFluxoManutencao;
	}

	/**
	 * Set: nmOperFluxoManutencao.
	 *
	 * @param nmOperFluxoManutencao the nm oper fluxo manutencao
	 */
	public void setNmOperFluxoManutencao(String nmOperFluxoManutencao) {
		this.nmOperFluxoManutencao = nmOperFluxoManutencao;
	}
	
	/**
	 * Get: canalInclusao.
	 *
	 * @return canalInclusao
	 */
	public String getCanalInclusao() {
		if (getCdCanalInclusao() != null && getCdCanalInclusao() != 0) {
			return getCdCanalInclusao() + " - " + getDsCanalInclusao();
		}
		return "";
	}
	
	/**
	 * Get: canalManutencao.
	 *
	 * @return canalManutencao
	 */
	public String getCanalManutencao() {
		if (getCdCanalManutencao() != null && getCdCanalManutencao() != 0) {
			return getCdCanalManutencao() + " - " + getDsCanalManutencao();
		}
		return "";
	}

	/**
	 * (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof HistoricoOrgaoPagadorSaidaDTO)) {
			return false;
		}

		HistoricoOrgaoPagadorSaidaDTO other = (HistoricoOrgaoPagadorSaidaDTO) obj;
		return this.getCdOrgaoPagador().equals(other.getCdOrgaoPagador())
					&& this.getHorario().equals(other.getHorario());
	}

	/**
	 * (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		int hashCode = 7;
		hashCode = hashCode + getCdOrgaoPagador().hashCode();
		hashCode = hashCode + getHorario().hashCode();
		return hashCode;
	}
}
