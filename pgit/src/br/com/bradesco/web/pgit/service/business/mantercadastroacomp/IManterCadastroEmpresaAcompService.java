/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantercadastroacomp
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantercadastroacomp;

import br.com.bradesco.web.pgit.service.business.mantercadastroacomp.bean.ConsultarEmpresaAcompanhadaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadastroacomp.bean.ConsultarEmpresaAcompanhadaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadastroacomp.bean.ManterEmpresaAcompanhadaEntradaDTO;

/**
 * Nome: IManterCadastroEmpresaAcompService
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public interface IManterCadastroEmpresaAcompService {
	
	/**
	 * Consultar empresa acompanhada.
	 *
	 * @param entradaDTO the entrada dto
	 * @return the consultar empresa acompanhada saida dto
	 */
	ConsultarEmpresaAcompanhadaSaidaDTO consultarEmpresaAcompanhada(ConsultarEmpresaAcompanhadaEntradaDTO entradaDTO);
	
	/**
	 * Consistir dados logicos.
	 *
	 * @param entrada the entrada
	 * @return the string
	 */
	String consistirDadosLogicos(ManterEmpresaAcompanhadaEntradaDTO entrada);
	
	/**
	 * Incluir empresa acompanhada.
	 *
	 * @param entradaDTO the entrada dto
	 * @return the string
	 */
	String incluirEmpresaAcompanhada(ManterEmpresaAcompanhadaEntradaDTO entradaDTO);
	
	/**
	 * Alterar empresa acompanhada.
	 *
	 * @param entradaDTO the entrada dto
	 * @return the string
	 */
	String alterarEmpresaAcompanhada(ManterEmpresaAcompanhadaEntradaDTO entradaDTO);
	
	/**
	 * Excluir empresa acompanhada.
	 *
	 * @param entradaDTO the entrada dto
	 * @return the string
	 */
	String excluirEmpresaAcompanhada(ManterEmpresaAcompanhadaEntradaDTO entradaDTO);
}

