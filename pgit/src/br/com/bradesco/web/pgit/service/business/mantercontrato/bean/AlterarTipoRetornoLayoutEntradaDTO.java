/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantercontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantercontrato.bean;

/**
 * Nome: AlterarTipoRetornoLayoutEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class AlterarTipoRetornoLayoutEntradaDTO {
	
	/** Atributo cdPessoaJuridicaContrato. */
	private Long cdPessoaJuridicaContrato;
	
	/** Atributo cdTipoContratoNegocio. */
	private Integer cdTipoContratoNegocio;
	
	/** Atributo nrSequenciaContratoNegocio. */
	private Long nrSequenciaContratoNegocio;
	
	/** Atributo cdTipoLayoutArquivo. */
	private Integer cdTipoLayoutArquivo;
	
	/** Atributo cdArquivoRetornoPagamento. */
	private Integer cdArquivoRetornoPagamento;
	
	/** Atributo cdTipoGeracaoRetorno. */
	private Integer cdTipoGeracaoRetorno;
	
	/** Atributo cdTipoMontaRetorno. */
	private Integer cdTipoMontaRetorno;
	
	/** Atributo cdTipoOcorrenciaRetorno. */
	private Integer cdTipoOcorrenciaRetorno;
	
	/** Atributo cdTipoClassificacaoRetorno. */
	private Integer cdTipoClassificacaoRetorno;
	
	/** Atributo cdTipoOrdemRegistroRetorno. */
	private Integer cdTipoOrdemRegistroRetorno;
	
	/**
	 * Get: cdArquivoRetornoPagamento.
	 *
	 * @return cdArquivoRetornoPagamento
	 */
	public Integer getCdArquivoRetornoPagamento() {
		return cdArquivoRetornoPagamento;
	}
	
	/**
	 * Set: cdArquivoRetornoPagamento.
	 *
	 * @param cdArquivoRetornoPagamento the cd arquivo retorno pagamento
	 */
	public void setCdArquivoRetornoPagamento(Integer cdArquivoRetornoPagamento) {
		this.cdArquivoRetornoPagamento = cdArquivoRetornoPagamento;
	}
	
	/**
	 * Get: cdPessoaJuridicaContrato.
	 *
	 * @return cdPessoaJuridicaContrato
	 */
	public Long getCdPessoaJuridicaContrato() {
		return cdPessoaJuridicaContrato;
	}
	
	/**
	 * Set: cdPessoaJuridicaContrato.
	 *
	 * @param cdPessoaJuridicaContrato the cd pessoa juridica contrato
	 */
	public void setCdPessoaJuridicaContrato(Long cdPessoaJuridicaContrato) {
		this.cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
	}
	
	/**
	 * Get: cdTipoClassificacaoRetorno.
	 *
	 * @return cdTipoClassificacaoRetorno
	 */
	public Integer getCdTipoClassificacaoRetorno() {
		return cdTipoClassificacaoRetorno;
	}
	
	/**
	 * Set: cdTipoClassificacaoRetorno.
	 *
	 * @param cdTipoClassificacaoRetorno the cd tipo classificacao retorno
	 */
	public void setCdTipoClassificacaoRetorno(Integer cdTipoClassificacaoRetorno) {
		this.cdTipoClassificacaoRetorno = cdTipoClassificacaoRetorno;
	}
	
	/**
	 * Get: cdTipoContratoNegocio.
	 *
	 * @return cdTipoContratoNegocio
	 */
	public Integer getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}
	
	/**
	 * Set: cdTipoContratoNegocio.
	 *
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}
	
	/**
	 * Get: cdTipoGeracaoRetorno.
	 *
	 * @return cdTipoGeracaoRetorno
	 */
	public Integer getCdTipoGeracaoRetorno() {
		return cdTipoGeracaoRetorno;
	}
	
	/**
	 * Set: cdTipoGeracaoRetorno.
	 *
	 * @param cdTipoGeracaoRetorno the cd tipo geracao retorno
	 */
	public void setCdTipoGeracaoRetorno(Integer cdTipoGeracaoRetorno) {
		this.cdTipoGeracaoRetorno = cdTipoGeracaoRetorno;
	}
	
	/**
	 * Get: cdTipoLayoutArquivo.
	 *
	 * @return cdTipoLayoutArquivo
	 */
	public Integer getCdTipoLayoutArquivo() {
		return cdTipoLayoutArquivo;
	}
	
	/**
	 * Set: cdTipoLayoutArquivo.
	 *
	 * @param cdTipoLayoutArquivo the cd tipo layout arquivo
	 */
	public void setCdTipoLayoutArquivo(Integer cdTipoLayoutArquivo) {
		this.cdTipoLayoutArquivo = cdTipoLayoutArquivo;
	}
	
	/**
	 * Get: cdTipoMontaRetorno.
	 *
	 * @return cdTipoMontaRetorno
	 */
	public Integer getCdTipoMontaRetorno() {
		return cdTipoMontaRetorno;
	}
	
	/**
	 * Set: cdTipoMontaRetorno.
	 *
	 * @param cdTipoMontaRetorno the cd tipo monta retorno
	 */
	public void setCdTipoMontaRetorno(Integer cdTipoMontaRetorno) {
		this.cdTipoMontaRetorno = cdTipoMontaRetorno;
	}
	
	/**
	 * Get: cdTipoOcorrenciaRetorno.
	 *
	 * @return cdTipoOcorrenciaRetorno
	 */
	public Integer getCdTipoOcorrenciaRetorno() {
		return cdTipoOcorrenciaRetorno;
	}
	
	/**
	 * Set: cdTipoOcorrenciaRetorno.
	 *
	 * @param cdTipoOcorrenciaRetorno the cd tipo ocorrencia retorno
	 */
	public void setCdTipoOcorrenciaRetorno(Integer cdTipoOcorrenciaRetorno) {
		this.cdTipoOcorrenciaRetorno = cdTipoOcorrenciaRetorno;
	}
	
	/**
	 * Get: cdTipoOrdemRegistroRetorno.
	 *
	 * @return cdTipoOrdemRegistroRetorno
	 */
	public Integer getCdTipoOrdemRegistroRetorno() {
		return cdTipoOrdemRegistroRetorno;
	}
	
	/**
	 * Set: cdTipoOrdemRegistroRetorno.
	 *
	 * @param cdTipoOrdemRegistroRetorno the cd tipo ordem registro retorno
	 */
	public void setCdTipoOrdemRegistroRetorno(Integer cdTipoOrdemRegistroRetorno) {
		this.cdTipoOrdemRegistroRetorno = cdTipoOrdemRegistroRetorno;
	}
	
	/**
	 * Get: nrSequenciaContratoNegocio.
	 *
	 * @return nrSequenciaContratoNegocio
	 */
	public Long getNrSequenciaContratoNegocio() {
		return nrSequenciaContratoNegocio;
	}
	
	/**
	 * Set: nrSequenciaContratoNegocio.
	 *
	 * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
	 */
	public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}
	
	
}
