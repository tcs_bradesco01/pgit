/*
 * Nome: br.com.bradesco.web.pgit.service.business.funcoesalcada.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantermodalidadesnetempresa.bean;

// TODO: Auto-generated Javadoc
/**
 * Nome: AlterarFuncoesAlcadaEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class AlterarHorarioLimiteEntradaDTO {
	
	/** The cd modalidade. */
	public int cdModalidade;
	
	/** The ds modalidade. */
	public String dsModalidade;
	
	/** The hr limite. */
	public String hrLimiteAutorizacao;
	
	/** The hr limite transm. */
	public String hrLimiteTransmissao;
	
	/** The cd sequencia tipo mod. */
	public int cdSequenciaTipoModalidade;
	
	/** The cd situacao tipo mod. */
	public int cdSituacaoTipoModalidade; 
	
	/**
     * Field _hrLimiteManutencao
     */
	private String hrLimiteManutencao;

    /**
     * Field _cdIndetificadorManutencao
     */
    private int cdIndetificadorManutencao = 0;

	/**
	 * Gets the cd modalidade.
	 *
	 * @return the cdModalidade
	 */
	public int getCdModalidade() {
		return cdModalidade;
	}

	/**
	 * Sets the cd modalidade.
	 *
	 * @param cdModalidade the cdModalidade to set
	 */
	public void setCdModalidade(int cdModalidade) {
		this.cdModalidade = cdModalidade;
	}

	/**
	 * Gets the ds modalidade.
	 *
	 * @return the dsModalidade
	 */
	public String getDsModalidade() {
		return dsModalidade;
	}

	/**
	 * Sets the ds modalidade.
	 *
	 * @param dsModalidade the dsModalidade to set
	 */
	public void setDsModalidade(String dsModalidade) {
		this.dsModalidade = dsModalidade;
	}

	/**
	 * Gets the hr limite.
	 *
	 * @return the hrLimite
	 */
	public String getHrLimiteAutorizacao() {
		return hrLimiteAutorizacao;
	}

	/**
	 * Sets the hr limite.
	 *
	 * @param hrLimite the hrLimite to set
	 */
	public void setHrLimiteAutorizacao(String hrLimiteAutorizacao) {
		this.hrLimiteAutorizacao = hrLimiteAutorizacao;
	}

	/**
	 * Gets the hr limite transm.
	 *
	 * @return the hrLimiteTransm
	 */
	public String getHrLimiteTransmissao() {
		return hrLimiteTransmissao;
	}

	/**
	 * Sets the hr limite transm.
	 *
	 * @param hrLimiteTransm the hrLimiteTransm to set
	 */
	public void setHrLimiteTransmissao(String hrLimiteTransmissao) {
		this.hrLimiteTransmissao = hrLimiteTransmissao;
	}

	/**
	 * Gets the cd sequencia tipo mod.
	 *
	 * @return the cdSequenciaTipoMod
	 */
	public int getCdSequenciaTipoModalidade() {
		return cdSequenciaTipoModalidade;
	}

	/**
	 * Sets the cd sequencia tipo mod.
	 *
	 * @param cdSequenciaTipoMod the cdSequenciaTipoMod to set
	 */
	public void setCdSequenciaTipoModalidade(int cdSequenciaTipoModalidade) {
		this.cdSequenciaTipoModalidade = cdSequenciaTipoModalidade;
	}

	/**
	 * Gets the cd situacao tipo mod.
	 *
	 * @return the cdSituacaoTipoMod
	 */
	public int getCdSituacaoTipoModalidade() {
		return cdSituacaoTipoModalidade;
	}

	/**
	 * Sets the cd situacao tipo mod.
	 *
	 * @param cdSituacaoTipoMod the cdSituacaoTipoMod to set
	 */
	public void setCdSituacaoTipoModalidade(int cdSituacaoTipoModalidade) {
		this.cdSituacaoTipoModalidade = cdSituacaoTipoModalidade;
	}

	public String getHrLimiteManutencao() {
		return hrLimiteManutencao;
	}

	public void setHrLimiteManutencao(String hrLimiteManutencao) {
		this.hrLimiteManutencao = hrLimiteManutencao;
	}

	public int getCdIndetificadorManutencao() {
		return cdIndetificadorManutencao;
	}

	public void setCdIndetificadorManutencao(int cdIndetificadorManutencao) {
		this.cdIndetificadorManutencao = cdIndetificadorManutencao;
	}
	
}
