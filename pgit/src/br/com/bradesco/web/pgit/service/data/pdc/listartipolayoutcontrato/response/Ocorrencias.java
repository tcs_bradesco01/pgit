/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listartipolayoutcontrato.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdTipoServico
     */
    private int _cdTipoServico = 0;

    /**
     * keeps track of state for field: _cdTipoServico
     */
    private boolean _has_cdTipoServico;

    /**
     * Field _dsTipoServico
     */
    private java.lang.String _dsTipoServico;

    /**
     * Field _cdRelacionamentoProdutoProduto
     */
    private int _cdRelacionamentoProdutoProduto = 0;

    /**
     * keeps track of state for field:
     * _cdRelacionamentoProdutoProduto
     */
    private boolean _has_cdRelacionamentoProdutoProduto;

    /**
     * Field _cdTipoLayout
     */
    private int _cdTipoLayout = 0;

    /**
     * keeps track of state for field: _cdTipoLayout
     */
    private boolean _has_cdTipoLayout;

    /**
     * Field _dsTipoLayout
     */
    private java.lang.String _dsTipoLayout;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listartipolayoutcontrato.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdRelacionamentoProdutoProduto
     * 
     */
    public void deleteCdRelacionamentoProdutoProduto()
    {
        this._has_cdRelacionamentoProdutoProduto= false;
    } //-- void deleteCdRelacionamentoProdutoProduto() 

    /**
     * Method deleteCdTipoLayout
     * 
     */
    public void deleteCdTipoLayout()
    {
        this._has_cdTipoLayout= false;
    } //-- void deleteCdTipoLayout() 

    /**
     * Method deleteCdTipoServico
     * 
     */
    public void deleteCdTipoServico()
    {
        this._has_cdTipoServico= false;
    } //-- void deleteCdTipoServico() 

    /**
     * Returns the value of field 'cdRelacionamentoProdutoProduto'.
     * 
     * @return int
     * @return the value of field 'cdRelacionamentoProdutoProduto'.
     */
    public int getCdRelacionamentoProdutoProduto()
    {
        return this._cdRelacionamentoProdutoProduto;
    } //-- int getCdRelacionamentoProdutoProduto() 

    /**
     * Returns the value of field 'cdTipoLayout'.
     * 
     * @return int
     * @return the value of field 'cdTipoLayout'.
     */
    public int getCdTipoLayout()
    {
        return this._cdTipoLayout;
    } //-- int getCdTipoLayout() 

    /**
     * Returns the value of field 'cdTipoServico'.
     * 
     * @return int
     * @return the value of field 'cdTipoServico'.
     */
    public int getCdTipoServico()
    {
        return this._cdTipoServico;
    } //-- int getCdTipoServico() 

    /**
     * Returns the value of field 'dsTipoLayout'.
     * 
     * @return String
     * @return the value of field 'dsTipoLayout'.
     */
    public java.lang.String getDsTipoLayout()
    {
        return this._dsTipoLayout;
    } //-- java.lang.String getDsTipoLayout() 

    /**
     * Returns the value of field 'dsTipoServico'.
     * 
     * @return String
     * @return the value of field 'dsTipoServico'.
     */
    public java.lang.String getDsTipoServico()
    {
        return this._dsTipoServico;
    } //-- java.lang.String getDsTipoServico() 

    /**
     * Method hasCdRelacionamentoProdutoProduto
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdRelacionamentoProdutoProduto()
    {
        return this._has_cdRelacionamentoProdutoProduto;
    } //-- boolean hasCdRelacionamentoProdutoProduto() 

    /**
     * Method hasCdTipoLayout
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoLayout()
    {
        return this._has_cdTipoLayout;
    } //-- boolean hasCdTipoLayout() 

    /**
     * Method hasCdTipoServico
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoServico()
    {
        return this._has_cdTipoServico;
    } //-- boolean hasCdTipoServico() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdRelacionamentoProdutoProduto'.
     * 
     * @param cdRelacionamentoProdutoProduto the value of field
     * 'cdRelacionamentoProdutoProduto'.
     */
    public void setCdRelacionamentoProdutoProduto(int cdRelacionamentoProdutoProduto)
    {
        this._cdRelacionamentoProdutoProduto = cdRelacionamentoProdutoProduto;
        this._has_cdRelacionamentoProdutoProduto = true;
    } //-- void setCdRelacionamentoProdutoProduto(int) 

    /**
     * Sets the value of field 'cdTipoLayout'.
     * 
     * @param cdTipoLayout the value of field 'cdTipoLayout'.
     */
    public void setCdTipoLayout(int cdTipoLayout)
    {
        this._cdTipoLayout = cdTipoLayout;
        this._has_cdTipoLayout = true;
    } //-- void setCdTipoLayout(int) 

    /**
     * Sets the value of field 'cdTipoServico'.
     * 
     * @param cdTipoServico the value of field 'cdTipoServico'.
     */
    public void setCdTipoServico(int cdTipoServico)
    {
        this._cdTipoServico = cdTipoServico;
        this._has_cdTipoServico = true;
    } //-- void setCdTipoServico(int) 

    /**
     * Sets the value of field 'dsTipoLayout'.
     * 
     * @param dsTipoLayout the value of field 'dsTipoLayout'.
     */
    public void setDsTipoLayout(java.lang.String dsTipoLayout)
    {
        this._dsTipoLayout = dsTipoLayout;
    } //-- void setDsTipoLayout(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoServico'.
     * 
     * @param dsTipoServico the value of field 'dsTipoServico'.
     */
    public void setDsTipoServico(java.lang.String dsTipoServico)
    {
        this._dsTipoServico = dsTipoServico;
    } //-- void setDsTipoServico(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listartipolayoutcontrato.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listartipolayoutcontrato.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listartipolayoutcontrato.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listartipolayoutcontrato.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
