/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listarcomprovantesalarialintranet.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.util.Enumeration;
import java.util.Vector;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ListarComprovanteSalarialIntranetResponse.
 * 
 * @version $Revision$ $Date$
 */
public class ListarComprovanteSalarialIntranetResponse implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _codMensagem
     */
    private java.lang.String _codMensagem;

    /**
     * Field _mensagem
     */
    private java.lang.String _mensagem;

    /**
     * Field _qtdeMaximaDetalheComprovante
     */
    private int _qtdeMaximaDetalheComprovante = 0;

    /**
     * keeps track of state for field: _qtdeMaximaDetalheComprovante
     */
    private boolean _has_qtdeMaximaDetalheComprovante;

    /**
     * Field _qtdeEmissaoComprovantePagamento
     */
    private int _qtdeEmissaoComprovantePagamento = 0;

    /**
     * keeps track of state for field:
     * _qtdeEmissaoComprovantePagamento
     */
    private boolean _has_qtdeEmissaoComprovantePagamento;

    /**
     * Field _cdMidiaComprovanteSalarial
     */
    private java.lang.String _cdMidiaComprovanteSalarial;

    /**
     * Field _nrComprovantes
     */
    private int _nrComprovantes = 0;

    /**
     * keeps track of state for field: _nrComprovantes
     */
    private boolean _has_nrComprovantes;

    /**
     * Field _ocorrenciasList
     */
    private java.util.Vector _ocorrenciasList;


      //----------------/
     //- Constructors -/
    //----------------/

    public ListarComprovanteSalarialIntranetResponse() 
     {
        super();
        _ocorrenciasList = new Vector();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarcomprovantesalarialintranet.response.ListarComprovanteSalarialIntranetResponse()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method addOcorrencias
     * 
     * 
     * 
     * @param vOcorrencias
     */
    public void addOcorrencias(br.com.bradesco.web.pgit.service.data.pdc.listarcomprovantesalarialintranet.response.Ocorrencias vOcorrencias)
        throws java.lang.IndexOutOfBoundsException
    {
        if (!(_ocorrenciasList.size() < 150)) {
            throw new IndexOutOfBoundsException("addOcorrencias has a maximum of 150");
        }
        _ocorrenciasList.addElement(vOcorrencias);
    } //-- void addOcorrencias(br.com.bradesco.web.pgit.service.data.pdc.listarcomprovantesalarialintranet.response.Ocorrencias) 

    /**
     * Method addOcorrencias
     * 
     * 
     * 
     * @param index
     * @param vOcorrencias
     */
    public void addOcorrencias(int index, br.com.bradesco.web.pgit.service.data.pdc.listarcomprovantesalarialintranet.response.Ocorrencias vOcorrencias)
        throws java.lang.IndexOutOfBoundsException
    {
        if (!(_ocorrenciasList.size() < 150)) {
            throw new IndexOutOfBoundsException("addOcorrencias has a maximum of 150");
        }
        _ocorrenciasList.insertElementAt(vOcorrencias, index);
    } //-- void addOcorrencias(int, br.com.bradesco.web.pgit.service.data.pdc.listarcomprovantesalarialintranet.response.Ocorrencias) 

    /**
     * Method deleteNrComprovantes
     * 
     */
    public void deleteNrComprovantes()
    {
        this._has_nrComprovantes= false;
    } //-- void deleteNrComprovantes() 

    /**
     * Method deleteQtdeEmissaoComprovantePagamento
     * 
     */
    public void deleteQtdeEmissaoComprovantePagamento()
    {
        this._has_qtdeEmissaoComprovantePagamento= false;
    } //-- void deleteQtdeEmissaoComprovantePagamento() 

    /**
     * Method deleteQtdeMaximaDetalheComprovante
     * 
     */
    public void deleteQtdeMaximaDetalheComprovante()
    {
        this._has_qtdeMaximaDetalheComprovante= false;
    } //-- void deleteQtdeMaximaDetalheComprovante() 

    /**
     * Method enumerateOcorrencias
     * 
     * 
     * 
     * @return Enumeration
     */
    public java.util.Enumeration enumerateOcorrencias()
    {
        return _ocorrenciasList.elements();
    } //-- java.util.Enumeration enumerateOcorrencias() 

    /**
     * Returns the value of field 'cdMidiaComprovanteSalarial'.
     * 
     * @return String
     * @return the value of field 'cdMidiaComprovanteSalarial'.
     */
    public java.lang.String getCdMidiaComprovanteSalarial()
    {
        return this._cdMidiaComprovanteSalarial;
    } //-- java.lang.String getCdMidiaComprovanteSalarial() 

    /**
     * Returns the value of field 'codMensagem'.
     * 
     * @return String
     * @return the value of field 'codMensagem'.
     */
    public java.lang.String getCodMensagem()
    {
        return this._codMensagem;
    } //-- java.lang.String getCodMensagem() 

    /**
     * Returns the value of field 'mensagem'.
     * 
     * @return String
     * @return the value of field 'mensagem'.
     */
    public java.lang.String getMensagem()
    {
        return this._mensagem;
    } //-- java.lang.String getMensagem() 

    /**
     * Returns the value of field 'nrComprovantes'.
     * 
     * @return int
     * @return the value of field 'nrComprovantes'.
     */
    public int getNrComprovantes()
    {
        return this._nrComprovantes;
    } //-- int getNrComprovantes() 

    /**
     * Method getOcorrencias
     * 
     * 
     * 
     * @param index
     * @return Ocorrencias
     */
    public br.com.bradesco.web.pgit.service.data.pdc.listarcomprovantesalarialintranet.response.Ocorrencias getOcorrencias(int index)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index >= _ocorrenciasList.size())) {
            throw new IndexOutOfBoundsException("getOcorrencias: Index value '"+index+"' not in range [0.."+(_ocorrenciasList.size() - 1) + "]");
        }
        
        return (br.com.bradesco.web.pgit.service.data.pdc.listarcomprovantesalarialintranet.response.Ocorrencias) _ocorrenciasList.elementAt(index);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarcomprovantesalarialintranet.response.Ocorrencias getOcorrencias(int) 

    /**
     * Method getOcorrencias
     * 
     * 
     * 
     * @return Ocorrencias
     */
    public br.com.bradesco.web.pgit.service.data.pdc.listarcomprovantesalarialintranet.response.Ocorrencias[] getOcorrencias()
    {
        int size = _ocorrenciasList.size();
        br.com.bradesco.web.pgit.service.data.pdc.listarcomprovantesalarialintranet.response.Ocorrencias[] mArray = new br.com.bradesco.web.pgit.service.data.pdc.listarcomprovantesalarialintranet.response.Ocorrencias[size];
        for (int index = 0; index < size; index++) {
            mArray[index] = (br.com.bradesco.web.pgit.service.data.pdc.listarcomprovantesalarialintranet.response.Ocorrencias) _ocorrenciasList.elementAt(index);
        }
        return mArray;
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarcomprovantesalarialintranet.response.Ocorrencias[] getOcorrencias() 

    /**
     * Method getOcorrenciasCount
     * 
     * 
     * 
     * @return int
     */
    public int getOcorrenciasCount()
    {
        return _ocorrenciasList.size();
    } //-- int getOcorrenciasCount() 

    /**
     * Returns the value of field
     * 'qtdeEmissaoComprovantePagamento'.
     * 
     * @return int
     * @return the value of field 'qtdeEmissaoComprovantePagamento'.
     */
    public int getQtdeEmissaoComprovantePagamento()
    {
        return this._qtdeEmissaoComprovantePagamento;
    } //-- int getQtdeEmissaoComprovantePagamento() 

    /**
     * Returns the value of field 'qtdeMaximaDetalheComprovante'.
     * 
     * @return int
     * @return the value of field 'qtdeMaximaDetalheComprovante'.
     */
    public int getQtdeMaximaDetalheComprovante()
    {
        return this._qtdeMaximaDetalheComprovante;
    } //-- int getQtdeMaximaDetalheComprovante() 

    /**
     * Method hasNrComprovantes
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrComprovantes()
    {
        return this._has_nrComprovantes;
    } //-- boolean hasNrComprovantes() 

    /**
     * Method hasQtdeEmissaoComprovantePagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtdeEmissaoComprovantePagamento()
    {
        return this._has_qtdeEmissaoComprovantePagamento;
    } //-- boolean hasQtdeEmissaoComprovantePagamento() 

    /**
     * Method hasQtdeMaximaDetalheComprovante
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtdeMaximaDetalheComprovante()
    {
        return this._has_qtdeMaximaDetalheComprovante;
    } //-- boolean hasQtdeMaximaDetalheComprovante() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Method removeAllOcorrencias
     * 
     */
    public void removeAllOcorrencias()
    {
        _ocorrenciasList.removeAllElements();
    } //-- void removeAllOcorrencias() 

    /**
     * Method removeOcorrencias
     * 
     * 
     * 
     * @param index
     * @return Ocorrencias
     */
    public br.com.bradesco.web.pgit.service.data.pdc.listarcomprovantesalarialintranet.response.Ocorrencias removeOcorrencias(int index)
    {
        java.lang.Object obj = _ocorrenciasList.elementAt(index);
        _ocorrenciasList.removeElementAt(index);
        return (br.com.bradesco.web.pgit.service.data.pdc.listarcomprovantesalarialintranet.response.Ocorrencias) obj;
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarcomprovantesalarialintranet.response.Ocorrencias removeOcorrencias(int) 

    /**
     * Sets the value of field 'cdMidiaComprovanteSalarial'.
     * 
     * @param cdMidiaComprovanteSalarial the value of field
     * 'cdMidiaComprovanteSalarial'.
     */
    public void setCdMidiaComprovanteSalarial(java.lang.String cdMidiaComprovanteSalarial)
    {
        this._cdMidiaComprovanteSalarial = cdMidiaComprovanteSalarial;
    } //-- void setCdMidiaComprovanteSalarial(java.lang.String) 

    /**
     * Sets the value of field 'codMensagem'.
     * 
     * @param codMensagem the value of field 'codMensagem'.
     */
    public void setCodMensagem(java.lang.String codMensagem)
    {
        this._codMensagem = codMensagem;
    } //-- void setCodMensagem(java.lang.String) 

    /**
     * Sets the value of field 'mensagem'.
     * 
     * @param mensagem the value of field 'mensagem'.
     */
    public void setMensagem(java.lang.String mensagem)
    {
        this._mensagem = mensagem;
    } //-- void setMensagem(java.lang.String) 

    /**
     * Sets the value of field 'nrComprovantes'.
     * 
     * @param nrComprovantes the value of field 'nrComprovantes'.
     */
    public void setNrComprovantes(int nrComprovantes)
    {
        this._nrComprovantes = nrComprovantes;
        this._has_nrComprovantes = true;
    } //-- void setNrComprovantes(int) 

    /**
     * Method setOcorrencias
     * 
     * 
     * 
     * @param index
     * @param vOcorrencias
     */
    public void setOcorrencias(int index, br.com.bradesco.web.pgit.service.data.pdc.listarcomprovantesalarialintranet.response.Ocorrencias vOcorrencias)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index >= _ocorrenciasList.size())) {
            throw new IndexOutOfBoundsException("setOcorrencias: Index value '"+index+"' not in range [0.." + (_ocorrenciasList.size() - 1) + "]");
        }
        if (!(index < 150)) {
            throw new IndexOutOfBoundsException("setOcorrencias has a maximum of 150");
        }
        _ocorrenciasList.setElementAt(vOcorrencias, index);
    } //-- void setOcorrencias(int, br.com.bradesco.web.pgit.service.data.pdc.listarcomprovantesalarialintranet.response.Ocorrencias) 

    /**
     * Method setOcorrencias
     * 
     * 
     * 
     * @param ocorrenciasArray
     */
    public void setOcorrencias(br.com.bradesco.web.pgit.service.data.pdc.listarcomprovantesalarialintranet.response.Ocorrencias[] ocorrenciasArray)
    {
        //-- copy array
        _ocorrenciasList.removeAllElements();
        for (int i = 0; i < ocorrenciasArray.length; i++) {
            _ocorrenciasList.addElement(ocorrenciasArray[i]);
        }
    } //-- void setOcorrencias(br.com.bradesco.web.pgit.service.data.pdc.listarcomprovantesalarialintranet.response.Ocorrencias) 

    /**
     * Sets the value of field 'qtdeEmissaoComprovantePagamento'.
     * 
     * @param qtdeEmissaoComprovantePagamento the value of field
     * 'qtdeEmissaoComprovantePagamento'.
     */
    public void setQtdeEmissaoComprovantePagamento(int qtdeEmissaoComprovantePagamento)
    {
        this._qtdeEmissaoComprovantePagamento = qtdeEmissaoComprovantePagamento;
        this._has_qtdeEmissaoComprovantePagamento = true;
    } //-- void setQtdeEmissaoComprovantePagamento(int) 

    /**
     * Sets the value of field 'qtdeMaximaDetalheComprovante'.
     * 
     * @param qtdeMaximaDetalheComprovante the value of field
     * 'qtdeMaximaDetalheComprovante'.
     */
    public void setQtdeMaximaDetalheComprovante(int qtdeMaximaDetalheComprovante)
    {
        this._qtdeMaximaDetalheComprovante = qtdeMaximaDetalheComprovante;
        this._has_qtdeMaximaDetalheComprovante = true;
    } //-- void setQtdeMaximaDetalheComprovante(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ListarComprovanteSalarialIntranetResponse
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listarcomprovantesalarialintranet.response.ListarComprovanteSalarialIntranetResponse unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listarcomprovantesalarialintranet.response.ListarComprovanteSalarialIntranetResponse) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listarcomprovantesalarialintranet.response.ListarComprovanteSalarialIntranetResponse.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarcomprovantesalarialintranet.response.ListarComprovanteSalarialIntranetResponse unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
