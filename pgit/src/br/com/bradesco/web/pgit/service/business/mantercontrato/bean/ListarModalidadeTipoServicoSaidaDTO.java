/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantercontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantercontrato.bean;

/**
 * Nome: ListarModalidadeTipoServicoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarModalidadeTipoServicoSaidaDTO {

	/** Atributo codMensagem. */
	private String codMensagem;

	/** Atributo mensagem. */
	private String mensagem;

	/** Atributo cdParametroTela. */
	private Integer cdParametroTela;

	/** Atributo cdServico. */
	private Integer cdServico;

	/** Atributo dsServico. */
	private String dsServico;

	/** Atributo cdModalidade. */
	private Integer cdModalidade;

	/** Atributo dsModalidade. */
	private String dsModalidade;

	/** Atributo cdRelacionamentoProduto. */
	private Integer cdRelacionamentoProduto;

	/** Atributo dtInicialServico. */
	private String dtInicialServico;

	/** Atributo cdSituacaoServicoRelacionado. */
	private Integer cdSituacaoServicoRelacionado;

	/** Atributo dsSituacaoServicoRelacionado. */
	private String dsSituacaoServicoRelacionado;

	/** Atributo qtdeModalidade. */
	private Integer qtdeModalidade;

	/** Atributo cdAmbienteServicoContrato. */
	private String cdAmbienteServicoContrato;
	
	/** Atributo cdProdutoOperacaoRelacionado. */
	private Integer cdProdutoOperacaoRelacionado;
    
	/** Atributo dsProdutoOperacaoRelacionado. */
	private String dsProdutoOperacaoRelacionado;

	/** Atributo check. */
	private boolean check;

	/**
	 * Get: cdModalidade.
	 *
	 * @return cdModalidade
	 */
	public Integer getCdModalidade() {
		return cdModalidade;
	}

	/**
	 * Set: cdModalidade.
	 *
	 * @param cdModalidade the cd modalidade
	 */
	public void setCdModalidade(Integer cdModalidade) {
		this.cdModalidade = cdModalidade;
	}

	/**
	 * Get: cdRelacionamentoProduto.
	 *
	 * @return cdRelacionamentoProduto
	 */
	public Integer getCdRelacionamentoProduto() {
		return cdRelacionamentoProduto;
	}

	/**
	 * Set: cdRelacionamentoProduto.
	 *
	 * @param cdRelacionamentoProduto the cd relacionamento produto
	 */
	public void setCdRelacionamentoProduto(Integer cdRelacionamentoProduto) {
		this.cdRelacionamentoProduto = cdRelacionamentoProduto;
	}

	/**
	 * Get: cdServico.
	 *
	 * @return cdServico
	 */
	public Integer getCdServico() {
		return cdServico;
	}

	/**
	 * Set: cdServico.
	 *
	 * @param cdServico the cd servico
	 */
	public void setCdServico(Integer cdServico) {
		this.cdServico = cdServico;
	}

	/**
	 * Get: cdSituacaoServicoRelacionado.
	 *
	 * @return cdSituacaoServicoRelacionado
	 */
	public Integer getCdSituacaoServicoRelacionado() {
		return cdSituacaoServicoRelacionado;
	}

	/**
	 * Set: cdSituacaoServicoRelacionado.
	 *
	 * @param cdSituacaoServicoRelacionado the cd situacao servico relacionado
	 */
	public void setCdSituacaoServicoRelacionado(Integer cdSituacaoServicoRelacionado) {
		this.cdSituacaoServicoRelacionado = cdSituacaoServicoRelacionado;
	}

	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}

	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}

	/**
	 * Get: dsModalidade.
	 *
	 * @return dsModalidade
	 */
	public String getDsModalidade() {
		return dsModalidade;
	}

	/**
	 * Set: dsModalidade.
	 *
	 * @param dsModalidade the ds modalidade
	 */
	public void setDsModalidade(String dsModalidade) {
		this.dsModalidade = dsModalidade;
	}

	/**
	 * Get: dsServico.
	 *
	 * @return dsServico
	 */
	public String getDsServico() {
		return dsServico;
	}

	/**
	 * Set: dsServico.
	 *
	 * @param dsServico the ds servico
	 */
	public void setDsServico(String dsServico) {
		this.dsServico = dsServico;
	}

	/**
	 * Get: dsSituacaoServicoRelacionado.
	 *
	 * @return dsSituacaoServicoRelacionado
	 */
	public String getDsSituacaoServicoRelacionado() {
		return dsSituacaoServicoRelacionado;
	}

	/**
	 * Set: dsSituacaoServicoRelacionado.
	 *
	 * @param dsSituacaoServicoRelacionado the ds situacao servico relacionado
	 */
	public void setDsSituacaoServicoRelacionado(String dsSituacaoServicoRelacionado) {
		this.dsSituacaoServicoRelacionado = dsSituacaoServicoRelacionado;
	}

	/**
	 * Get: dtInicialServico.
	 *
	 * @return dtInicialServico
	 */
	public String getDtInicialServico() {
		return dtInicialServico;
	}

	/**
	 * Set: dtInicialServico.
	 *
	 * @param dtInicialServico the dt inicial servico
	 */
	public void setDtInicialServico(String dtInicialServico) {
		this.dtInicialServico = dtInicialServico;
	}

	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}

	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	/**
	 * Get: cdParametroTela.
	 *
	 * @return cdParametroTela
	 */
	public Integer getCdParametroTela() {
		return cdParametroTela;
	}

	/**
	 * Set: cdParametroTela.
	 *
	 * @param cdParametroTela the cd parametro tela
	 */
	public void setCdParametroTela(Integer cdParametroTela) {
		this.cdParametroTela = cdParametroTela;
	}

	/**
	 * Is check.
	 *
	 * @return true, if is check
	 */
	public boolean isCheck() {
		return check;
	}

	/**
	 * Set: check.
	 *
	 * @param check the check
	 */
	public void setCheck(boolean check) {
		this.check = check;
	}

	/**
	 * Get: qtdeModalidade.
	 *
	 * @return qtdeModalidade
	 */
	public Integer getQtdeModalidade() {
		return qtdeModalidade;
	}

	/**
	 * Set: qtdeModalidade.
	 *
	 * @param qtdeModalidade the qtde modalidade
	 */
	public void setQtdeModalidade(Integer qtdeModalidade) {
		this.qtdeModalidade = qtdeModalidade;
	}

	/**
	 * Get: cdAmbienteServicoContrato.
	 *
	 * @return cdAmbienteServicoContrato
	 */
	public String getCdAmbienteServicoContrato() {
		return cdAmbienteServicoContrato;
	}

	/**
	 * Set: cdAmbienteServicoContrato.
	 *
	 * @param cdAmbienteServicoContrato the cd ambiente servico contrato
	 */
	public void setCdAmbienteServicoContrato(String cdAmbienteServicoContrato) {
		this.cdAmbienteServicoContrato = cdAmbienteServicoContrato;
	}

	/**
	 * Get: cdProdutoOperacaoRelacionado.
	 *
	 * @return cdProdutoOperacaoRelacionado
	 */
	public Integer getCdProdutoOperacaoRelacionado() {
		return cdProdutoOperacaoRelacionado;
	}

	/**
	 * Set: cdProdutoOperacaoRelacionado.
	 *
	 * @param cdProdutoOperacaoRelacionado the cd produto operacao relacionado
	 */
	public void setCdProdutoOperacaoRelacionado(Integer cdProdutoOperacaoRelacionado) {
		this.cdProdutoOperacaoRelacionado = cdProdutoOperacaoRelacionado;
	}

	/**
	 * Get: dsProdutoOperacaoRelacionado.
	 *
	 * @return dsProdutoOperacaoRelacionado
	 */
	public String getDsProdutoOperacaoRelacionado() {
		return dsProdutoOperacaoRelacionado;
	}

	/**
	 * Set: dsProdutoOperacaoRelacionado.
	 *
	 * @param dsProdutoOperacaoRelacionado the ds produto operacao relacionado
	 */
	public void setDsProdutoOperacaoRelacionado(String dsProdutoOperacaoRelacionado) {
		this.dsProdutoOperacaoRelacionado = dsProdutoOperacaoRelacionado;
	}

}