/*
 * Nome: br.com.bradesco.web.pgit.service.business.combo.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.combo.bean;

/**
 * Nome: ConsultarMotivoSituacaoPagPendenteEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarMotivoSituacaoPagPendenteEntradaDTO{
	
	/** Atributo nrOcorrencias. */
	private Integer nrOcorrencias;
	
	/** Atributo cdSituacaoPagamento. */
	private Integer cdSituacaoPagamento;

	/**
	 * Get: nrOcorrencias.
	 *
	 * @return nrOcorrencias
	 */
	public Integer getNrOcorrencias(){
		return nrOcorrencias;
	}

	/**
	 * Set: nrOcorrencias.
	 *
	 * @param nrOcorrencias the nr ocorrencias
	 */
	public void setNrOcorrencias(Integer nrOcorrencias){
		this.nrOcorrencias = nrOcorrencias;
	}

	/**
	 * Get: cdSituacaoPagamento.
	 *
	 * @return cdSituacaoPagamento
	 */
	public Integer getCdSituacaoPagamento(){
		return cdSituacaoPagamento;
	}

	/**
	 * Set: cdSituacaoPagamento.
	 *
	 * @param cdSituacaoPagamento the cd situacao pagamento
	 */
	public void setCdSituacaoPagamento(Integer cdSituacaoPagamento){
		this.cdSituacaoPagamento = cdSituacaoPagamento;
	}
}