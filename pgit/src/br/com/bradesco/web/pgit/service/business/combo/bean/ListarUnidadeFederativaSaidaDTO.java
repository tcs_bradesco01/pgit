/*
 * Nome: br.com.bradesco.web.pgit.service.business.combo.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.combo.bean;

/**
 * Nome: ListarUnidadeFederativaSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarUnidadeFederativaSaidaDTO {

	/** Atributo cdUnidadeFederativa. */
	private int cdUnidadeFederativa;
	
	/** Atributo dsUnidadeFederativa. */
	private String dsUnidadeFederativa;
	
	/**
	 * Listar unidade federativa saida dto.
	 *
	 * @param cdUnidadeFederativa the cd unidade federativa
	 * @param dsUnidadeFederativa the ds unidade federativa
	 */
	public ListarUnidadeFederativaSaidaDTO(int cdUnidadeFederativa, String dsUnidadeFederativa){
		this.cdUnidadeFederativa = cdUnidadeFederativa;
		this.dsUnidadeFederativa = dsUnidadeFederativa;
		
	}

	/**
	 * Get: cdUnidadeFederativa.
	 *
	 * @return cdUnidadeFederativa
	 */
	public int getCdUnidadeFederativa() {
		return cdUnidadeFederativa;
	}

	/**
	 * Set: cdUnidadeFederativa.
	 *
	 * @param cdUnidadeFederativa the cd unidade federativa
	 */
	public void setCdUnidadeFederativa(int cdUnidadeFederativa) {
		this.cdUnidadeFederativa = cdUnidadeFederativa;
	}

	/**
	 * Get: dsUnidadeFederativa.
	 *
	 * @return dsUnidadeFederativa
	 */
	public String getDsUnidadeFederativa() {
		return dsUnidadeFederativa;
	}

	/**
	 * Set: dsUnidadeFederativa.
	 *
	 * @param dsUnidadeFederativa the ds unidade federativa
	 */
	public void setDsUnidadeFederativa(String dsUnidadeFederativa) {
		this.dsUnidadeFederativa = dsUnidadeFederativa;
	}
	
	
}
