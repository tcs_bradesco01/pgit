/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.incluirsolbasepagto.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.math.BigDecimal;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class IncluirSolBasePagtoRequest.
 * 
 * @version $Revision$ $Date$
 */
public class IncluirSolBasePagtoRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdPessoaJuridicaContrato
     */
    private long _cdPessoaJuridicaContrato = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridicaContrato
     */
    private boolean _has_cdPessoaJuridicaContrato;

    /**
     * Field _cdTipoContratoNegocio
     */
    private int _cdTipoContratoNegocio = 0;

    /**
     * keeps track of state for field: _cdTipoContratoNegocio
     */
    private boolean _has_cdTipoContratoNegocio;

    /**
     * Field _nrSequenciaContratoNegocio
     */
    private long _nrSequenciaContratoNegocio = 0;

    /**
     * keeps track of state for field: _nrSequenciaContratoNegocio
     */
    private boolean _has_nrSequenciaContratoNegocio;

    /**
     * Field _dtInicioPagamento
     */
    private java.lang.String _dtInicioPagamento;

    /**
     * Field _dtFimPagamento
     */
    private java.lang.String _dtFimPagamento;

    /**
     * Field _cdServico
     */
    private int _cdServico = 0;

    /**
     * keeps track of state for field: _cdServico
     */
    private boolean _has_cdServico;

    /**
     * Field _cdServicoRelacionado
     */
    private int _cdServicoRelacionado = 0;

    /**
     * keeps track of state for field: _cdServicoRelacionado
     */
    private boolean _has_cdServicoRelacionado;

    /**
     * Field _cdBancoDebito
     */
    private int _cdBancoDebito = 0;

    /**
     * keeps track of state for field: _cdBancoDebito
     */
    private boolean _has_cdBancoDebito;

    /**
     * Field _cdAgenciaDebito
     */
    private int _cdAgenciaDebito = 0;

    /**
     * keeps track of state for field: _cdAgenciaDebito
     */
    private boolean _has_cdAgenciaDebito;

    /**
     * Field _cdContaDebito
     */
    private long _cdContaDebito = 0;

    /**
     * keeps track of state for field: _cdContaDebito
     */
    private boolean _has_cdContaDebito;

    /**
     * Field _digitoContaDebito
     */
    private java.lang.String _digitoContaDebito;

    /**
     * Field _cdPessoaJuridicaConta
     */
    private long _cdPessoaJuridicaConta = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridicaConta
     */
    private boolean _has_cdPessoaJuridicaConta;

    /**
     * Field _cdTipoContratoConta
     */
    private int _cdTipoContratoConta = 0;

    /**
     * keeps track of state for field: _cdTipoContratoConta
     */
    private boolean _has_cdTipoContratoConta;

    /**
     * Field _nrSequenciaContratoConta
     */
    private long _nrSequenciaContratoConta = 0;

    /**
     * keeps track of state for field: _nrSequenciaContratoConta
     */
    private boolean _has_nrSequenciaContratoConta;

    /**
     * Field _cdIndicadorPagos
     */
    private java.lang.String _cdIndicadorPagos;

    /**
     * Field _cdIndicadorNaoPagos
     */
    private java.lang.String _cdIndicadorNaoPagos;

    /**
     * Field _cdIndicadorAgendados
     */
    private java.lang.String _cdIndicadorAgendados;

    /**
     * Field _cdFavorecido
     */
    private long _cdFavorecido = 0;

    /**
     * keeps track of state for field: _cdFavorecido
     */
    private boolean _has_cdFavorecido;

    /**
     * Field _cdInscricaoFavorecido
     */
    private long _cdInscricaoFavorecido = 0;

    /**
     * keeps track of state for field: _cdInscricaoFavorecido
     */
    private boolean _has_cdInscricaoFavorecido;

    /**
     * Field _cdTipoInscricaoFavorecido
     */
    private int _cdTipoInscricaoFavorecido = 0;

    /**
     * keeps track of state for field: _cdTipoInscricaoFavorecido
     */
    private boolean _has_cdTipoInscricaoFavorecido;

    /**
     * Field _nmMinemonicoFavorecido
     */
    private java.lang.String _nmMinemonicoFavorecido;

    /**
     * Field _cdOrigem
     */
    private int _cdOrigem = 0;

    /**
     * keeps track of state for field: _cdOrigem
     */
    private boolean _has_cdOrigem;

    /**
     * Field _cdDestino
     */
    private int _cdDestino = 0;

    /**
     * keeps track of state for field: _cdDestino
     */
    private boolean _has_cdDestino;

    /**
     * Field _vlTarifaPadrao
     */
    private java.math.BigDecimal _vlTarifaPadrao = new java.math.BigDecimal("0");

    /**
     * Field _numPercentualDescTarifa
     */
    private java.math.BigDecimal _numPercentualDescTarifa = new java.math.BigDecimal("0");

    /**
     * Field _vlrTarifaAtual
     */
    private java.math.BigDecimal _vlrTarifaAtual = new java.math.BigDecimal("0");


      //----------------/
     //- Constructors -/
    //----------------/

    public IncluirSolBasePagtoRequest() 
     {
        super();
        setVlTarifaPadrao(new java.math.BigDecimal("0"));
        setNumPercentualDescTarifa(new java.math.BigDecimal("0"));
        setVlrTarifaAtual(new java.math.BigDecimal("0"));
    } //-- br.com.bradesco.web.pgit.service.data.pdc.incluirsolbasepagto.request.IncluirSolBasePagtoRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdAgenciaDebito
     * 
     */
    public void deleteCdAgenciaDebito()
    {
        this._has_cdAgenciaDebito= false;
    } //-- void deleteCdAgenciaDebito() 

    /**
     * Method deleteCdBancoDebito
     * 
     */
    public void deleteCdBancoDebito()
    {
        this._has_cdBancoDebito= false;
    } //-- void deleteCdBancoDebito() 

    /**
     * Method deleteCdContaDebito
     * 
     */
    public void deleteCdContaDebito()
    {
        this._has_cdContaDebito= false;
    } //-- void deleteCdContaDebito() 

    /**
     * Method deleteCdDestino
     * 
     */
    public void deleteCdDestino()
    {
        this._has_cdDestino= false;
    } //-- void deleteCdDestino() 

    /**
     * Method deleteCdFavorecido
     * 
     */
    public void deleteCdFavorecido()
    {
        this._has_cdFavorecido= false;
    } //-- void deleteCdFavorecido() 

    /**
     * Method deleteCdInscricaoFavorecido
     * 
     */
    public void deleteCdInscricaoFavorecido()
    {
        this._has_cdInscricaoFavorecido= false;
    } //-- void deleteCdInscricaoFavorecido() 

    /**
     * Method deleteCdOrigem
     * 
     */
    public void deleteCdOrigem()
    {
        this._has_cdOrigem= false;
    } //-- void deleteCdOrigem() 

    /**
     * Method deleteCdPessoaJuridicaConta
     * 
     */
    public void deleteCdPessoaJuridicaConta()
    {
        this._has_cdPessoaJuridicaConta= false;
    } //-- void deleteCdPessoaJuridicaConta() 

    /**
     * Method deleteCdPessoaJuridicaContrato
     * 
     */
    public void deleteCdPessoaJuridicaContrato()
    {
        this._has_cdPessoaJuridicaContrato= false;
    } //-- void deleteCdPessoaJuridicaContrato() 

    /**
     * Method deleteCdServico
     * 
     */
    public void deleteCdServico()
    {
        this._has_cdServico= false;
    } //-- void deleteCdServico() 

    /**
     * Method deleteCdServicoRelacionado
     * 
     */
    public void deleteCdServicoRelacionado()
    {
        this._has_cdServicoRelacionado= false;
    } //-- void deleteCdServicoRelacionado() 

    /**
     * Method deleteCdTipoContratoConta
     * 
     */
    public void deleteCdTipoContratoConta()
    {
        this._has_cdTipoContratoConta= false;
    } //-- void deleteCdTipoContratoConta() 

    /**
     * Method deleteCdTipoContratoNegocio
     * 
     */
    public void deleteCdTipoContratoNegocio()
    {
        this._has_cdTipoContratoNegocio= false;
    } //-- void deleteCdTipoContratoNegocio() 

    /**
     * Method deleteCdTipoInscricaoFavorecido
     * 
     */
    public void deleteCdTipoInscricaoFavorecido()
    {
        this._has_cdTipoInscricaoFavorecido= false;
    } //-- void deleteCdTipoInscricaoFavorecido() 

    /**
     * Method deleteNrSequenciaContratoConta
     * 
     */
    public void deleteNrSequenciaContratoConta()
    {
        this._has_nrSequenciaContratoConta= false;
    } //-- void deleteNrSequenciaContratoConta() 

    /**
     * Method deleteNrSequenciaContratoNegocio
     * 
     */
    public void deleteNrSequenciaContratoNegocio()
    {
        this._has_nrSequenciaContratoNegocio= false;
    } //-- void deleteNrSequenciaContratoNegocio() 

    /**
     * Returns the value of field 'cdAgenciaDebito'.
     * 
     * @return int
     * @return the value of field 'cdAgenciaDebito'.
     */
    public int getCdAgenciaDebito()
    {
        return this._cdAgenciaDebito;
    } //-- int getCdAgenciaDebito() 

    /**
     * Returns the value of field 'cdBancoDebito'.
     * 
     * @return int
     * @return the value of field 'cdBancoDebito'.
     */
    public int getCdBancoDebito()
    {
        return this._cdBancoDebito;
    } //-- int getCdBancoDebito() 

    /**
     * Returns the value of field 'cdContaDebito'.
     * 
     * @return long
     * @return the value of field 'cdContaDebito'.
     */
    public long getCdContaDebito()
    {
        return this._cdContaDebito;
    } //-- long getCdContaDebito() 

    /**
     * Returns the value of field 'cdDestino'.
     * 
     * @return int
     * @return the value of field 'cdDestino'.
     */
    public int getCdDestino()
    {
        return this._cdDestino;
    } //-- int getCdDestino() 

    /**
     * Returns the value of field 'cdFavorecido'.
     * 
     * @return long
     * @return the value of field 'cdFavorecido'.
     */
    public long getCdFavorecido()
    {
        return this._cdFavorecido;
    } //-- long getCdFavorecido() 

    /**
     * Returns the value of field 'cdIndicadorAgendados'.
     * 
     * @return String
     * @return the value of field 'cdIndicadorAgendados'.
     */
    public java.lang.String getCdIndicadorAgendados()
    {
        return this._cdIndicadorAgendados;
    } //-- java.lang.String getCdIndicadorAgendados() 

    /**
     * Returns the value of field 'cdIndicadorNaoPagos'.
     * 
     * @return String
     * @return the value of field 'cdIndicadorNaoPagos'.
     */
    public java.lang.String getCdIndicadorNaoPagos()
    {
        return this._cdIndicadorNaoPagos;
    } //-- java.lang.String getCdIndicadorNaoPagos() 

    /**
     * Returns the value of field 'cdIndicadorPagos'.
     * 
     * @return String
     * @return the value of field 'cdIndicadorPagos'.
     */
    public java.lang.String getCdIndicadorPagos()
    {
        return this._cdIndicadorPagos;
    } //-- java.lang.String getCdIndicadorPagos() 

    /**
     * Returns the value of field 'cdInscricaoFavorecido'.
     * 
     * @return long
     * @return the value of field 'cdInscricaoFavorecido'.
     */
    public long getCdInscricaoFavorecido()
    {
        return this._cdInscricaoFavorecido;
    } //-- long getCdInscricaoFavorecido() 

    /**
     * Returns the value of field 'cdOrigem'.
     * 
     * @return int
     * @return the value of field 'cdOrigem'.
     */
    public int getCdOrigem()
    {
        return this._cdOrigem;
    } //-- int getCdOrigem() 

    /**
     * Returns the value of field 'cdPessoaJuridicaConta'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridicaConta'.
     */
    public long getCdPessoaJuridicaConta()
    {
        return this._cdPessoaJuridicaConta;
    } //-- long getCdPessoaJuridicaConta() 

    /**
     * Returns the value of field 'cdPessoaJuridicaContrato'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridicaContrato'.
     */
    public long getCdPessoaJuridicaContrato()
    {
        return this._cdPessoaJuridicaContrato;
    } //-- long getCdPessoaJuridicaContrato() 

    /**
     * Returns the value of field 'cdServico'.
     * 
     * @return int
     * @return the value of field 'cdServico'.
     */
    public int getCdServico()
    {
        return this._cdServico;
    } //-- int getCdServico() 

    /**
     * Returns the value of field 'cdServicoRelacionado'.
     * 
     * @return int
     * @return the value of field 'cdServicoRelacionado'.
     */
    public int getCdServicoRelacionado()
    {
        return this._cdServicoRelacionado;
    } //-- int getCdServicoRelacionado() 

    /**
     * Returns the value of field 'cdTipoContratoConta'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoConta'.
     */
    public int getCdTipoContratoConta()
    {
        return this._cdTipoContratoConta;
    } //-- int getCdTipoContratoConta() 

    /**
     * Returns the value of field 'cdTipoContratoNegocio'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoNegocio'.
     */
    public int getCdTipoContratoNegocio()
    {
        return this._cdTipoContratoNegocio;
    } //-- int getCdTipoContratoNegocio() 

    /**
     * Returns the value of field 'cdTipoInscricaoFavorecido'.
     * 
     * @return int
     * @return the value of field 'cdTipoInscricaoFavorecido'.
     */
    public int getCdTipoInscricaoFavorecido()
    {
        return this._cdTipoInscricaoFavorecido;
    } //-- int getCdTipoInscricaoFavorecido() 

    /**
     * Returns the value of field 'digitoContaDebito'.
     * 
     * @return String
     * @return the value of field 'digitoContaDebito'.
     */
    public java.lang.String getDigitoContaDebito()
    {
        return this._digitoContaDebito;
    } //-- java.lang.String getDigitoContaDebito() 

    /**
     * Returns the value of field 'dtFimPagamento'.
     * 
     * @return String
     * @return the value of field 'dtFimPagamento'.
     */
    public java.lang.String getDtFimPagamento()
    {
        return this._dtFimPagamento;
    } //-- java.lang.String getDtFimPagamento() 

    /**
     * Returns the value of field 'dtInicioPagamento'.
     * 
     * @return String
     * @return the value of field 'dtInicioPagamento'.
     */
    public java.lang.String getDtInicioPagamento()
    {
        return this._dtInicioPagamento;
    } //-- java.lang.String getDtInicioPagamento() 

    /**
     * Returns the value of field 'nmMinemonicoFavorecido'.
     * 
     * @return String
     * @return the value of field 'nmMinemonicoFavorecido'.
     */
    public java.lang.String getNmMinemonicoFavorecido()
    {
        return this._nmMinemonicoFavorecido;
    } //-- java.lang.String getNmMinemonicoFavorecido() 

    /**
     * Returns the value of field 'nrSequenciaContratoConta'.
     * 
     * @return long
     * @return the value of field 'nrSequenciaContratoConta'.
     */
    public long getNrSequenciaContratoConta()
    {
        return this._nrSequenciaContratoConta;
    } //-- long getNrSequenciaContratoConta() 

    /**
     * Returns the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @return long
     * @return the value of field 'nrSequenciaContratoNegocio'.
     */
    public long getNrSequenciaContratoNegocio()
    {
        return this._nrSequenciaContratoNegocio;
    } //-- long getNrSequenciaContratoNegocio() 

    /**
     * Returns the value of field 'numPercentualDescTarifa'.
     * 
     * @return BigDecimal
     * @return the value of field 'numPercentualDescTarifa'.
     */
    public java.math.BigDecimal getNumPercentualDescTarifa()
    {
        return this._numPercentualDescTarifa;
    } //-- java.math.BigDecimal getNumPercentualDescTarifa() 

    /**
     * Returns the value of field 'vlTarifaPadrao'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlTarifaPadrao'.
     */
    public java.math.BigDecimal getVlTarifaPadrao()
    {
        return this._vlTarifaPadrao;
    } //-- java.math.BigDecimal getVlTarifaPadrao() 

    /**
     * Returns the value of field 'vlrTarifaAtual'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlrTarifaAtual'.
     */
    public java.math.BigDecimal getVlrTarifaAtual()
    {
        return this._vlrTarifaAtual;
    } //-- java.math.BigDecimal getVlrTarifaAtual() 

    /**
     * Method hasCdAgenciaDebito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgenciaDebito()
    {
        return this._has_cdAgenciaDebito;
    } //-- boolean hasCdAgenciaDebito() 

    /**
     * Method hasCdBancoDebito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdBancoDebito()
    {
        return this._has_cdBancoDebito;
    } //-- boolean hasCdBancoDebito() 

    /**
     * Method hasCdContaDebito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdContaDebito()
    {
        return this._has_cdContaDebito;
    } //-- boolean hasCdContaDebito() 

    /**
     * Method hasCdDestino
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdDestino()
    {
        return this._has_cdDestino;
    } //-- boolean hasCdDestino() 

    /**
     * Method hasCdFavorecido
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFavorecido()
    {
        return this._has_cdFavorecido;
    } //-- boolean hasCdFavorecido() 

    /**
     * Method hasCdInscricaoFavorecido
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdInscricaoFavorecido()
    {
        return this._has_cdInscricaoFavorecido;
    } //-- boolean hasCdInscricaoFavorecido() 

    /**
     * Method hasCdOrigem
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdOrigem()
    {
        return this._has_cdOrigem;
    } //-- boolean hasCdOrigem() 

    /**
     * Method hasCdPessoaJuridicaConta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridicaConta()
    {
        return this._has_cdPessoaJuridicaConta;
    } //-- boolean hasCdPessoaJuridicaConta() 

    /**
     * Method hasCdPessoaJuridicaContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridicaContrato()
    {
        return this._has_cdPessoaJuridicaContrato;
    } //-- boolean hasCdPessoaJuridicaContrato() 

    /**
     * Method hasCdServico
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdServico()
    {
        return this._has_cdServico;
    } //-- boolean hasCdServico() 

    /**
     * Method hasCdServicoRelacionado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdServicoRelacionado()
    {
        return this._has_cdServicoRelacionado;
    } //-- boolean hasCdServicoRelacionado() 

    /**
     * Method hasCdTipoContratoConta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoConta()
    {
        return this._has_cdTipoContratoConta;
    } //-- boolean hasCdTipoContratoConta() 

    /**
     * Method hasCdTipoContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoNegocio()
    {
        return this._has_cdTipoContratoNegocio;
    } //-- boolean hasCdTipoContratoNegocio() 

    /**
     * Method hasCdTipoInscricaoFavorecido
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoInscricaoFavorecido()
    {
        return this._has_cdTipoInscricaoFavorecido;
    } //-- boolean hasCdTipoInscricaoFavorecido() 

    /**
     * Method hasNrSequenciaContratoConta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaContratoConta()
    {
        return this._has_nrSequenciaContratoConta;
    } //-- boolean hasNrSequenciaContratoConta() 

    /**
     * Method hasNrSequenciaContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaContratoNegocio()
    {
        return this._has_nrSequenciaContratoNegocio;
    } //-- boolean hasNrSequenciaContratoNegocio() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdAgenciaDebito'.
     * 
     * @param cdAgenciaDebito the value of field 'cdAgenciaDebito'.
     */
    public void setCdAgenciaDebito(int cdAgenciaDebito)
    {
        this._cdAgenciaDebito = cdAgenciaDebito;
        this._has_cdAgenciaDebito = true;
    } //-- void setCdAgenciaDebito(int) 

    /**
     * Sets the value of field 'cdBancoDebito'.
     * 
     * @param cdBancoDebito the value of field 'cdBancoDebito'.
     */
    public void setCdBancoDebito(int cdBancoDebito)
    {
        this._cdBancoDebito = cdBancoDebito;
        this._has_cdBancoDebito = true;
    } //-- void setCdBancoDebito(int) 

    /**
     * Sets the value of field 'cdContaDebito'.
     * 
     * @param cdContaDebito the value of field 'cdContaDebito'.
     */
    public void setCdContaDebito(long cdContaDebito)
    {
        this._cdContaDebito = cdContaDebito;
        this._has_cdContaDebito = true;
    } //-- void setCdContaDebito(long) 

    /**
     * Sets the value of field 'cdDestino'.
     * 
     * @param cdDestino the value of field 'cdDestino'.
     */
    public void setCdDestino(int cdDestino)
    {
        this._cdDestino = cdDestino;
        this._has_cdDestino = true;
    } //-- void setCdDestino(int) 

    /**
     * Sets the value of field 'cdFavorecido'.
     * 
     * @param cdFavorecido the value of field 'cdFavorecido'.
     */
    public void setCdFavorecido(long cdFavorecido)
    {
        this._cdFavorecido = cdFavorecido;
        this._has_cdFavorecido = true;
    } //-- void setCdFavorecido(long) 

    /**
     * Sets the value of field 'cdIndicadorAgendados'.
     * 
     * @param cdIndicadorAgendados the value of field
     * 'cdIndicadorAgendados'.
     */
    public void setCdIndicadorAgendados(java.lang.String cdIndicadorAgendados)
    {
        this._cdIndicadorAgendados = cdIndicadorAgendados;
    } //-- void setCdIndicadorAgendados(java.lang.String) 

    /**
     * Sets the value of field 'cdIndicadorNaoPagos'.
     * 
     * @param cdIndicadorNaoPagos the value of field
     * 'cdIndicadorNaoPagos'.
     */
    public void setCdIndicadorNaoPagos(java.lang.String cdIndicadorNaoPagos)
    {
        this._cdIndicadorNaoPagos = cdIndicadorNaoPagos;
    } //-- void setCdIndicadorNaoPagos(java.lang.String) 

    /**
     * Sets the value of field 'cdIndicadorPagos'.
     * 
     * @param cdIndicadorPagos the value of field 'cdIndicadorPagos'
     */
    public void setCdIndicadorPagos(java.lang.String cdIndicadorPagos)
    {
        this._cdIndicadorPagos = cdIndicadorPagos;
    } //-- void setCdIndicadorPagos(java.lang.String) 

    /**
     * Sets the value of field 'cdInscricaoFavorecido'.
     * 
     * @param cdInscricaoFavorecido the value of field
     * 'cdInscricaoFavorecido'.
     */
    public void setCdInscricaoFavorecido(long cdInscricaoFavorecido)
    {
        this._cdInscricaoFavorecido = cdInscricaoFavorecido;
        this._has_cdInscricaoFavorecido = true;
    } //-- void setCdInscricaoFavorecido(long) 

    /**
     * Sets the value of field 'cdOrigem'.
     * 
     * @param cdOrigem the value of field 'cdOrigem'.
     */
    public void setCdOrigem(int cdOrigem)
    {
        this._cdOrigem = cdOrigem;
        this._has_cdOrigem = true;
    } //-- void setCdOrigem(int) 

    /**
     * Sets the value of field 'cdPessoaJuridicaConta'.
     * 
     * @param cdPessoaJuridicaConta the value of field
     * 'cdPessoaJuridicaConta'.
     */
    public void setCdPessoaJuridicaConta(long cdPessoaJuridicaConta)
    {
        this._cdPessoaJuridicaConta = cdPessoaJuridicaConta;
        this._has_cdPessoaJuridicaConta = true;
    } //-- void setCdPessoaJuridicaConta(long) 

    /**
     * Sets the value of field 'cdPessoaJuridicaContrato'.
     * 
     * @param cdPessoaJuridicaContrato the value of field
     * 'cdPessoaJuridicaContrato'.
     */
    public void setCdPessoaJuridicaContrato(long cdPessoaJuridicaContrato)
    {
        this._cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
        this._has_cdPessoaJuridicaContrato = true;
    } //-- void setCdPessoaJuridicaContrato(long) 

    /**
     * Sets the value of field 'cdServico'.
     * 
     * @param cdServico the value of field 'cdServico'.
     */
    public void setCdServico(int cdServico)
    {
        this._cdServico = cdServico;
        this._has_cdServico = true;
    } //-- void setCdServico(int) 

    /**
     * Sets the value of field 'cdServicoRelacionado'.
     * 
     * @param cdServicoRelacionado the value of field
     * 'cdServicoRelacionado'.
     */
    public void setCdServicoRelacionado(int cdServicoRelacionado)
    {
        this._cdServicoRelacionado = cdServicoRelacionado;
        this._has_cdServicoRelacionado = true;
    } //-- void setCdServicoRelacionado(int) 

    /**
     * Sets the value of field 'cdTipoContratoConta'.
     * 
     * @param cdTipoContratoConta the value of field
     * 'cdTipoContratoConta'.
     */
    public void setCdTipoContratoConta(int cdTipoContratoConta)
    {
        this._cdTipoContratoConta = cdTipoContratoConta;
        this._has_cdTipoContratoConta = true;
    } //-- void setCdTipoContratoConta(int) 

    /**
     * Sets the value of field 'cdTipoContratoNegocio'.
     * 
     * @param cdTipoContratoNegocio the value of field
     * 'cdTipoContratoNegocio'.
     */
    public void setCdTipoContratoNegocio(int cdTipoContratoNegocio)
    {
        this._cdTipoContratoNegocio = cdTipoContratoNegocio;
        this._has_cdTipoContratoNegocio = true;
    } //-- void setCdTipoContratoNegocio(int) 

    /**
     * Sets the value of field 'cdTipoInscricaoFavorecido'.
     * 
     * @param cdTipoInscricaoFavorecido the value of field
     * 'cdTipoInscricaoFavorecido'.
     */
    public void setCdTipoInscricaoFavorecido(int cdTipoInscricaoFavorecido)
    {
        this._cdTipoInscricaoFavorecido = cdTipoInscricaoFavorecido;
        this._has_cdTipoInscricaoFavorecido = true;
    } //-- void setCdTipoInscricaoFavorecido(int) 

    /**
     * Sets the value of field 'digitoContaDebito'.
     * 
     * @param digitoContaDebito the value of field
     * 'digitoContaDebito'.
     */
    public void setDigitoContaDebito(java.lang.String digitoContaDebito)
    {
        this._digitoContaDebito = digitoContaDebito;
    } //-- void setDigitoContaDebito(java.lang.String) 

    /**
     * Sets the value of field 'dtFimPagamento'.
     * 
     * @param dtFimPagamento the value of field 'dtFimPagamento'.
     */
    public void setDtFimPagamento(java.lang.String dtFimPagamento)
    {
        this._dtFimPagamento = dtFimPagamento;
    } //-- void setDtFimPagamento(java.lang.String) 

    /**
     * Sets the value of field 'dtInicioPagamento'.
     * 
     * @param dtInicioPagamento the value of field
     * 'dtInicioPagamento'.
     */
    public void setDtInicioPagamento(java.lang.String dtInicioPagamento)
    {
        this._dtInicioPagamento = dtInicioPagamento;
    } //-- void setDtInicioPagamento(java.lang.String) 

    /**
     * Sets the value of field 'nmMinemonicoFavorecido'.
     * 
     * @param nmMinemonicoFavorecido the value of field
     * 'nmMinemonicoFavorecido'.
     */
    public void setNmMinemonicoFavorecido(java.lang.String nmMinemonicoFavorecido)
    {
        this._nmMinemonicoFavorecido = nmMinemonicoFavorecido;
    } //-- void setNmMinemonicoFavorecido(java.lang.String) 

    /**
     * Sets the value of field 'nrSequenciaContratoConta'.
     * 
     * @param nrSequenciaContratoConta the value of field
     * 'nrSequenciaContratoConta'.
     */
    public void setNrSequenciaContratoConta(long nrSequenciaContratoConta)
    {
        this._nrSequenciaContratoConta = nrSequenciaContratoConta;
        this._has_nrSequenciaContratoConta = true;
    } //-- void setNrSequenciaContratoConta(long) 

    /**
     * Sets the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @param nrSequenciaContratoNegocio the value of field
     * 'nrSequenciaContratoNegocio'.
     */
    public void setNrSequenciaContratoNegocio(long nrSequenciaContratoNegocio)
    {
        this._nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
        this._has_nrSequenciaContratoNegocio = true;
    } //-- void setNrSequenciaContratoNegocio(long) 

    /**
     * Sets the value of field 'numPercentualDescTarifa'.
     * 
     * @param numPercentualDescTarifa the value of field
     * 'numPercentualDescTarifa'.
     */
    public void setNumPercentualDescTarifa(java.math.BigDecimal numPercentualDescTarifa)
    {
        this._numPercentualDescTarifa = numPercentualDescTarifa;
    } //-- void setNumPercentualDescTarifa(java.math.BigDecimal) 

    /**
     * Sets the value of field 'vlTarifaPadrao'.
     * 
     * @param vlTarifaPadrao the value of field 'vlTarifaPadrao'.
     */
    public void setVlTarifaPadrao(java.math.BigDecimal vlTarifaPadrao)
    {
        this._vlTarifaPadrao = vlTarifaPadrao;
    } //-- void setVlTarifaPadrao(java.math.BigDecimal) 

    /**
     * Sets the value of field 'vlrTarifaAtual'.
     * 
     * @param vlrTarifaAtual the value of field 'vlrTarifaAtual'.
     */
    public void setVlrTarifaAtual(java.math.BigDecimal vlrTarifaAtual)
    {
        this._vlrTarifaAtual = vlrTarifaAtual;
    } //-- void setVlrTarifaAtual(java.math.BigDecimal) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return IncluirSolBasePagtoRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.incluirsolbasepagto.request.IncluirSolBasePagtoRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.incluirsolbasepagto.request.IncluirSolBasePagtoRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.incluirsolbasepagto.request.IncluirSolBasePagtoRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.incluirsolbasepagto.request.IncluirSolBasePagtoRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
