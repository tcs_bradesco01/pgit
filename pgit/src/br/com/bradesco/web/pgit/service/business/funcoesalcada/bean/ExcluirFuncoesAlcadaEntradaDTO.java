/*
 * Nome: br.com.bradesco.web.pgit.service.business.funcoesalcada.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.funcoesalcada.bean;

/**
 * Nome: ExcluirFuncoesAlcadaEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ExcluirFuncoesAlcadaEntradaDTO {
	
	/** Atributo centroCusto. */
	private String centroCusto;
	
	/** Atributo codigoAplicacao. */
	private String codigoAplicacao;
	
	/** Atributo acao. */
	private int acao;
	
	/** Atributo indicadorAcesso. */
	private int indicadorAcesso;
	
	/**
	 * Get: acao.
	 *
	 * @return acao
	 */
	public int getAcao() {
		return acao;
	}
	
	/**
	 * Set: acao.
	 *
	 * @param acao the acao
	 */
	public void setAcao(int acao) {
		this.acao = acao;
	}
	
	/**
	 * Get: centroCusto.
	 *
	 * @return centroCusto
	 */
	public String getCentroCusto() {
		return centroCusto;
	}
	
	/**
	 * Set: centroCusto.
	 *
	 * @param centroCusto the centro custo
	 */
	public void setCentroCusto(String centroCusto) {
		this.centroCusto = centroCusto;
	}
	
	/**
	 * Get: codigoAplicacao.
	 *
	 * @return codigoAplicacao
	 */
	public String getCodigoAplicacao() {
		return codigoAplicacao;
	}
	
	/**
	 * Set: codigoAplicacao.
	 *
	 * @param codigoAplicacao the codigo aplicacao
	 */
	public void setCodigoAplicacao(String codigoAplicacao) {
		this.codigoAplicacao = codigoAplicacao;
	}
	
	/**
	 * Get: indicadorAcesso.
	 *
	 * @return indicadorAcesso
	 */
	public int getIndicadorAcesso() {
		return indicadorAcesso;
	}
	
	/**
	 * Set: indicadorAcesso.
	 *
	 * @param indicadorAcesso the indicador acesso
	 */
	public void setIndicadorAcesso(int indicadorAcesso) {
		this.indicadorAcesso = indicadorAcesso;
	}
}
