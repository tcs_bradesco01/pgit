/*
 * Nome: br.com.bradesco.web.pgit.service.business.anteciparpostergarpagtosagecon.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.anteciparpostergarpagtosagecon.bean;

import java.math.BigDecimal;

/**
 * Nome: ConsultarPagtosConsAntPostergacaoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarPagtosConsAntPostergacaoSaidaDTO{
    
    /** Atributo codMensagem. */
    private String codMensagem;
    
    /** Atributo mensagem. */
    private String mensagem;
    
    /** Atributo nrConsultas. */
    private Integer nrConsultas;
    
    /** Atributo cdPessoaJuridica. */
    private Long cdPessoaJuridica;
    
    /** Atributo dsRazaoSocial. */
    private String dsRazaoSocial;
    
    /** Atributo cdTipoContrato. */
    private Integer cdTipoContrato;
    
    /** Atributo nrContrato. */
    private Long nrContrato;
    
    /** Atributo nrCnpjCpf. */
    private Long nrCnpjCpf;
    
    /** Atributo nrFilialCnpjCpf. */
    private Integer nrFilialCnpjCpf;
    
    /** Atributo cdDigitoCnpjCpf. */
    private Integer cdDigitoCnpjCpf;
    
    /** Atributo nrRemessa. */
    private Long nrRemessa;
    
    /** Atributo dtPagamento. */
    private String dtPagamento;
    
    /** Atributo cdBanco. */
    private Integer cdBanco;
    
    /** Atributo cdAgencia. */
    private Integer cdAgencia;
    
    /** Atributo cdDigitoAgencia. */
    private Integer cdDigitoAgencia;
    
    /** Atributo cdConta. */
    private Long cdConta;
    
    /** Atributo cdDigitoConta. */
    private String cdDigitoConta;
    
    /** Atributo cdProdutoServicoOperacao. */
    private Integer cdProdutoServicoOperacao;
    
    /** Atributo dsResumoProdutoServico. */
    private String dsResumoProdutoServico;
    
    /** Atributo cdProdutoServicoRelacionado. */
    private Integer cdProdutoServicoRelacionado;
    
    /** Atributo dsOperacaoProdutoServico. */
    private String dsOperacaoProdutoServico;
    
    /** Atributo cdSituacaoOperacaoPagamento. */
    private Integer cdSituacaoOperacaoPagamento;
    
    /** Atributo dsSituacaoOperacaoPagamento. */
    private String dsSituacaoOperacaoPagamento;
    
    /** Atributo qtPagamento. */
    private Long qtPagamento;
    
    /** Atributo qtPagamentoFormatado. */
    private String qtPagamentoFormatado;
    
    /** Atributo vlPagamento. */
    private BigDecimal vlPagamento;
    
    /** Atributo cpfCnpjFormatado. */
    private String cpfCnpjFormatado;
    
    /** Atributo contaFormatada. */
    private String contaFormatada;
    
    /** Atributo cdPessoaContratoDebito. */
    private Long cdPessoaContratoDebito;
    
    /** Atributo cdTipoContratoDebito. */
    private Integer cdTipoContratoDebito;
    
    /** Atributo nrSequenciaContratoDebito. */
    private Long nrSequenciaContratoDebito;    
    
	/**
	 * Get: cdAgencia.
	 *
	 * @return cdAgencia
	 */
	public Integer getCdAgencia() {
		return cdAgencia;
	}
	
	/**
	 * Set: cdAgencia.
	 *
	 * @param cdAgencia the cd agencia
	 */
	public void setCdAgencia(Integer cdAgencia) {
		this.cdAgencia = cdAgencia;
	}
	
	/**
	 * Get: cdBanco.
	 *
	 * @return cdBanco
	 */
	public Integer getCdBanco() {
		return cdBanco;
	}
	
	/**
	 * Set: cdBanco.
	 *
	 * @param cdBanco the cd banco
	 */
	public void setCdBanco(Integer cdBanco) {
		this.cdBanco = cdBanco;
	}
	
	/**
	 * Get: cdConta.
	 *
	 * @return cdConta
	 */
	public Long getCdConta() {
		return cdConta;
	}
	
	/**
	 * Set: cdConta.
	 *
	 * @param cdConta the cd conta
	 */
	public void setCdConta(Long cdConta) {
		this.cdConta = cdConta;
	}
	
	/**
	 * Get: cdDigitoAgencia.
	 *
	 * @return cdDigitoAgencia
	 */
	public Integer getCdDigitoAgencia() {
		return cdDigitoAgencia;
	}
	
	/**
	 * Set: cdDigitoAgencia.
	 *
	 * @param cdDigitoAgencia the cd digito agencia
	 */
	public void setCdDigitoAgencia(Integer cdDigitoAgencia) {
		this.cdDigitoAgencia = cdDigitoAgencia;
	}
	
	/**
	 * Get: cdDigitoCnpjCpf.
	 *
	 * @return cdDigitoCnpjCpf
	 */
	public Integer getCdDigitoCnpjCpf() {
		return cdDigitoCnpjCpf;
	}
	
	/**
	 * Set: cdDigitoCnpjCpf.
	 *
	 * @param cdDigitoCnpjCpf the cd digito cnpj cpf
	 */
	public void setCdDigitoCnpjCpf(Integer cdDigitoCnpjCpf) {
		this.cdDigitoCnpjCpf = cdDigitoCnpjCpf;
	}
	
	/**
	 * Get: cdDigitoConta.
	 *
	 * @return cdDigitoConta
	 */
	public String getCdDigitoConta() {
		return cdDigitoConta;
	}
	
	/**
	 * Set: cdDigitoConta.
	 *
	 * @param cdDigitoConta the cd digito conta
	 */
	public void setCdDigitoConta(String cdDigitoConta) {
		this.cdDigitoConta = cdDigitoConta;
	}
	
	/**
	 * Get: cdPessoaJuridica.
	 *
	 * @return cdPessoaJuridica
	 */
	public Long getCdPessoaJuridica() {
		return cdPessoaJuridica;
	}
	
	/**
	 * Set: cdPessoaJuridica.
	 *
	 * @param cdPessoaJuridica the cd pessoa juridica
	 */
	public void setCdPessoaJuridica(Long cdPessoaJuridica) {
		this.cdPessoaJuridica = cdPessoaJuridica;
	}
	
	/**
	 * Get: cdProdutoServicoOperacao.
	 *
	 * @return cdProdutoServicoOperacao
	 */
	public Integer getCdProdutoServicoOperacao() {
		return cdProdutoServicoOperacao;
	}
	
	/**
	 * Set: cdProdutoServicoOperacao.
	 *
	 * @param cdProdutoServicoOperacao the cd produto servico operacao
	 */
	public void setCdProdutoServicoOperacao(Integer cdProdutoServicoOperacao) {
		this.cdProdutoServicoOperacao = cdProdutoServicoOperacao;
	}
	
	/**
	 * Get: cdProdutoServicoRelacionado.
	 *
	 * @return cdProdutoServicoRelacionado
	 */
	public Integer getCdProdutoServicoRelacionado() {
		return cdProdutoServicoRelacionado;
	}
	
	/**
	 * Set: cdProdutoServicoRelacionado.
	 *
	 * @param cdProdutoServicoRelacionado the cd produto servico relacionado
	 */
	public void setCdProdutoServicoRelacionado(Integer cdProdutoServicoRelacionado) {
		this.cdProdutoServicoRelacionado = cdProdutoServicoRelacionado;
	}
	
	/**
	 * Get: cdSituacaoOperacaoPagamento.
	 *
	 * @return cdSituacaoOperacaoPagamento
	 */
	public Integer getCdSituacaoOperacaoPagamento() {
		return cdSituacaoOperacaoPagamento;
	}
	
	/**
	 * Set: cdSituacaoOperacaoPagamento.
	 *
	 * @param cdSituacaoOperacaoPagamento the cd situacao operacao pagamento
	 */
	public void setCdSituacaoOperacaoPagamento(Integer cdSituacaoOperacaoPagamento) {
		this.cdSituacaoOperacaoPagamento = cdSituacaoOperacaoPagamento;
	}
	
	/**
	 * Get: cdTipoContrato.
	 *
	 * @return cdTipoContrato
	 */
	public Integer getCdTipoContrato() {
		return cdTipoContrato;
	}
	
	/**
	 * Set: cdTipoContrato.
	 *
	 * @param cdTipoContrato the cd tipo contrato
	 */
	public void setCdTipoContrato(Integer cdTipoContrato) {
		this.cdTipoContrato = cdTipoContrato;
	}
	
	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}
	
	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}
	
	/**
	 * Get: dsOperacaoProdutoServico.
	 *
	 * @return dsOperacaoProdutoServico
	 */
	public String getDsOperacaoProdutoServico() {
		return dsOperacaoProdutoServico;
	}
	
	/**
	 * Set: dsOperacaoProdutoServico.
	 *
	 * @param dsOperacaoProdutoServico the ds operacao produto servico
	 */
	public void setDsOperacaoProdutoServico(String dsOperacaoProdutoServico) {
		this.dsOperacaoProdutoServico = dsOperacaoProdutoServico;
	}
	
	/**
	 * Get: dsRazaoSocial.
	 *
	 * @return dsRazaoSocial
	 */
	public String getDsRazaoSocial() {
		return dsRazaoSocial;
	}
	
	/**
	 * Set: dsRazaoSocial.
	 *
	 * @param dsRazaoSocial the ds razao social
	 */
	public void setDsRazaoSocial(String dsRazaoSocial) {
		this.dsRazaoSocial = dsRazaoSocial;
	}
	
	/**
	 * Get: dsResumoProdutoServico.
	 *
	 * @return dsResumoProdutoServico
	 */
	public String getDsResumoProdutoServico() {
		return dsResumoProdutoServico;
	}
	
	/**
	 * Set: dsResumoProdutoServico.
	 *
	 * @param dsResumoProdutoServico the ds resumo produto servico
	 */
	public void setDsResumoProdutoServico(String dsResumoProdutoServico) {
		this.dsResumoProdutoServico = dsResumoProdutoServico;
	}
	
	/**
	 * Get: dtPagamento.
	 *
	 * @return dtPagamento
	 */
	public String getDtPagamento() {
		return dtPagamento;
	}
	
	/**
	 * Set: dtPagamento.
	 *
	 * @param dtPagamento the dt pagamento
	 */
	public void setDtPagamento(String dtPagamento) {
		this.dtPagamento = dtPagamento;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	/**
	 * Get: nrCnpjCpf.
	 *
	 * @return nrCnpjCpf
	 */
	public Long getNrCnpjCpf() {
		return nrCnpjCpf;
	}
	
	/**
	 * Set: nrCnpjCpf.
	 *
	 * @param nrCnpjCpf the nr cnpj cpf
	 */
	public void setNrCnpjCpf(Long nrCnpjCpf) {
		this.nrCnpjCpf = nrCnpjCpf;
	}
	
	/**
	 * Get: nrConsultas.
	 *
	 * @return nrConsultas
	 */
	public Integer getNrConsultas() {
		return nrConsultas;
	}
	
	/**
	 * Set: nrConsultas.
	 *
	 * @param nrConsultas the nr consultas
	 */
	public void setNrConsultas(Integer nrConsultas) {
		this.nrConsultas = nrConsultas;
	}
	
	/**
	 * Get: nrContrato.
	 *
	 * @return nrContrato
	 */
	public Long getNrContrato() {
		return nrContrato;
	}
	
	/**
	 * Set: nrContrato.
	 *
	 * @param nrContrato the nr contrato
	 */
	public void setNrContrato(Long nrContrato) {
		this.nrContrato = nrContrato;
	}
	
	/**
	 * Get: nrFilialCnpjCpf.
	 *
	 * @return nrFilialCnpjCpf
	 */
	public Integer getNrFilialCnpjCpf() {
		return nrFilialCnpjCpf;
	}
	
	/**
	 * Set: nrFilialCnpjCpf.
	 *
	 * @param nrFilialCnpjCpf the nr filial cnpj cpf
	 */
	public void setNrFilialCnpjCpf(Integer nrFilialCnpjCpf) {
		this.nrFilialCnpjCpf = nrFilialCnpjCpf;
	}
	
	/**
	 * Get: nrRemessa.
	 *
	 * @return nrRemessa
	 */
	public Long getNrRemessa() {
		return nrRemessa;
	}
	
	/**
	 * Set: nrRemessa.
	 *
	 * @param nrRemessa the nr remessa
	 */
	public void setNrRemessa(Long nrRemessa) {
		this.nrRemessa = nrRemessa;
	}
	
	/**
	 * Get: qtPagamento.
	 *
	 * @return qtPagamento
	 */
	public Long getQtPagamento() {
		return qtPagamento;
	}
	
	/**
	 * Set: qtPagamento.
	 *
	 * @param qtPagamento the qt pagamento
	 */
	public void setQtPagamento(Long qtPagamento) {
		this.qtPagamento = qtPagamento;
	}
	
	/**
	 * Get: vlPagamento.
	 *
	 * @return vlPagamento
	 */
	public BigDecimal getVlPagamento() {
		return vlPagamento;
	}
	
	/**
	 * Set: vlPagamento.
	 *
	 * @param vlPagamento the vl pagamento
	 */
	public void setVlPagamento(BigDecimal vlPagamento) {
		this.vlPagamento = vlPagamento;
	}
	
	/**
	 * Get: cpfCnpjFormatado.
	 *
	 * @return cpfCnpjFormatado
	 */
	public String getCpfCnpjFormatado() {
		return cpfCnpjFormatado;
	}
	
	/**
	 * Set: cpfCnpjFormatado.
	 *
	 * @param cpfCnpjFormatado the cpf cnpj formatado
	 */
	public void setCpfCnpjFormatado(String cpfCnpjFormatado) {
		this.cpfCnpjFormatado = cpfCnpjFormatado;
	}
	
	/**
	 * Get: contaFormatada.
	 *
	 * @return contaFormatada
	 */
	public String getContaFormatada() {
		return contaFormatada;
	}
	
	/**
	 * Set: contaFormatada.
	 *
	 * @param contaFormatada the conta formatada
	 */
	public void setContaFormatada(String contaFormatada) {
		this.contaFormatada = contaFormatada;
	}
	
	/**
	 * Get: dsSituacaoOperacaoPagamento.
	 *
	 * @return dsSituacaoOperacaoPagamento
	 */
	public String getDsSituacaoOperacaoPagamento() {
		return dsSituacaoOperacaoPagamento;
	}
	
	/**
	 * Set: dsSituacaoOperacaoPagamento.
	 *
	 * @param dsSituacaoOperacaoPagamento the ds situacao operacao pagamento
	 */
	public void setDsSituacaoOperacaoPagamento(String dsSituacaoOperacaoPagamento) {
		this.dsSituacaoOperacaoPagamento = dsSituacaoOperacaoPagamento;
	}
	
	/**
	 * Get: cdPessoaContratoDebito.
	 *
	 * @return cdPessoaContratoDebito
	 */
	public Long getCdPessoaContratoDebito() {
		return cdPessoaContratoDebito;
	}
	
	/**
	 * Set: cdPessoaContratoDebito.
	 *
	 * @param cdPessoaContratoDebito the cd pessoa contrato debito
	 */
	public void setCdPessoaContratoDebito(Long cdPessoaContratoDebito) {
		this.cdPessoaContratoDebito = cdPessoaContratoDebito;
	}
	
	/**
	 * Get: cdTipoContratoDebito.
	 *
	 * @return cdTipoContratoDebito
	 */
	public Integer getCdTipoContratoDebito() {
		return cdTipoContratoDebito;
	}
	
	/**
	 * Set: cdTipoContratoDebito.
	 *
	 * @param cdTipoContratoDebito the cd tipo contrato debito
	 */
	public void setCdTipoContratoDebito(Integer cdTipoContratoDebito) {
		this.cdTipoContratoDebito = cdTipoContratoDebito;
	}
	
	/**
	 * Get: nrSequenciaContratoDebito.
	 *
	 * @return nrSequenciaContratoDebito
	 */
	public Long getNrSequenciaContratoDebito() {
		return nrSequenciaContratoDebito;
	}
	
	/**
	 * Set: nrSequenciaContratoDebito.
	 *
	 * @param nrSequenciaContratoDebito the nr sequencia contrato debito
	 */
	public void setNrSequenciaContratoDebito(Long nrSequenciaContratoDebito) {
		this.nrSequenciaContratoDebito = nrSequenciaContratoDebito;
	}
	
	/**
	 * Get: qtPagamentoFormatado.
	 *
	 * @return qtPagamentoFormatado
	 */
	public String getQtPagamentoFormatado() {
		return qtPagamentoFormatado;
	}
	
	/**
	 * Set: qtPagamentoFormatado.
	 *
	 * @param qtPagamentoFormatado the qt pagamento formatado
	 */
	public void setQtPagamentoFormatado(String qtPagamentoFormatado) {
		this.qtPagamentoFormatado = qtPagamentoFormatado;
	}
}