/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.alterarmsgcomprovantesalrl.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;

/**
 * Class AlterarMsgComprovanteSalrlRequest.
 * 
 * @version $Revision$ $Date$
 */
public class AlterarMsgComprovanteSalrlRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdControleMensagemSalarial
     */
    private int _cdControleMensagemSalarial = 0;

    /**
     * keeps track of state for field: _cdControleMensagemSalarial
     */
    private boolean _has_cdControleMensagemSalarial;

    /**
     * Field _cdTipoComprovanteSalarial
     */
    private int _cdTipoComprovanteSalarial = 0;

    /**
     * keeps track of state for field: _cdTipoComprovanteSalarial
     */
    private boolean _has_cdTipoComprovanteSalarial;

    /**
     * Field _cdTipoMensagemSalarial
     */
    private int _cdTipoMensagemSalarial = 0;

    /**
     * keeps track of state for field: _cdTipoMensagemSalarial
     */
    private boolean _has_cdTipoMensagemSalarial;

    /**
     * Field _dsControleMensagemSalarial
     */
    private java.lang.String _dsControleMensagemSalarial;

    /**
     * Field _cdRestMensagemComprovante
     */
    private int _cdRestMensagemComprovante = 0;

    /**
     * keeps track of state for field: _cdRestMensagemComprovante
     */
    private boolean _has_cdRestMensagemComprovante;


      //----------------/
     //- Constructors -/
    //----------------/

    public AlterarMsgComprovanteSalrlRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.alterarmsgcomprovantesalrl.request.AlterarMsgComprovanteSalrlRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdControleMensagemSalarial
     * 
     */
    public void deleteCdControleMensagemSalarial()
    {
        this._has_cdControleMensagemSalarial= false;
    } //-- void deleteCdControleMensagemSalarial() 

    /**
     * Method deleteCdRestMensagemComprovante
     * 
     */
    public void deleteCdRestMensagemComprovante()
    {
        this._has_cdRestMensagemComprovante= false;
    } //-- void deleteCdRestMensagemComprovante() 

    /**
     * Method deleteCdTipoComprovanteSalarial
     * 
     */
    public void deleteCdTipoComprovanteSalarial()
    {
        this._has_cdTipoComprovanteSalarial= false;
    } //-- void deleteCdTipoComprovanteSalarial() 

    /**
     * Method deleteCdTipoMensagemSalarial
     * 
     */
    public void deleteCdTipoMensagemSalarial()
    {
        this._has_cdTipoMensagemSalarial= false;
    } //-- void deleteCdTipoMensagemSalarial() 

    /**
     * Returns the value of field 'cdControleMensagemSalarial'.
     * 
     * @return int
     * @return the value of field 'cdControleMensagemSalarial'.
     */
    public int getCdControleMensagemSalarial()
    {
        return this._cdControleMensagemSalarial;
    } //-- int getCdControleMensagemSalarial() 

    /**
     * Returns the value of field 'cdRestMensagemComprovante'.
     * 
     * @return int
     * @return the value of field 'cdRestMensagemComprovante'.
     */
    public int getCdRestMensagemComprovante()
    {
        return this._cdRestMensagemComprovante;
    } //-- int getCdRestMensagemComprovante() 

    /**
     * Returns the value of field 'cdTipoComprovanteSalarial'.
     * 
     * @return int
     * @return the value of field 'cdTipoComprovanteSalarial'.
     */
    public int getCdTipoComprovanteSalarial()
    {
        return this._cdTipoComprovanteSalarial;
    } //-- int getCdTipoComprovanteSalarial() 

    /**
     * Returns the value of field 'cdTipoMensagemSalarial'.
     * 
     * @return int
     * @return the value of field 'cdTipoMensagemSalarial'.
     */
    public int getCdTipoMensagemSalarial()
    {
        return this._cdTipoMensagemSalarial;
    } //-- int getCdTipoMensagemSalarial() 

    /**
     * Returns the value of field 'dsControleMensagemSalarial'.
     * 
     * @return String
     * @return the value of field 'dsControleMensagemSalarial'.
     */
    public java.lang.String getDsControleMensagemSalarial()
    {
        return this._dsControleMensagemSalarial;
    } //-- java.lang.String getDsControleMensagemSalarial() 

    /**
     * Method hasCdControleMensagemSalarial
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdControleMensagemSalarial()
    {
        return this._has_cdControleMensagemSalarial;
    } //-- boolean hasCdControleMensagemSalarial() 

    /**
     * Method hasCdRestMensagemComprovante
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdRestMensagemComprovante()
    {
        return this._has_cdRestMensagemComprovante;
    } //-- boolean hasCdRestMensagemComprovante() 

    /**
     * Method hasCdTipoComprovanteSalarial
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoComprovanteSalarial()
    {
        return this._has_cdTipoComprovanteSalarial;
    } //-- boolean hasCdTipoComprovanteSalarial() 

    /**
     * Method hasCdTipoMensagemSalarial
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoMensagemSalarial()
    {
        return this._has_cdTipoMensagemSalarial;
    } //-- boolean hasCdTipoMensagemSalarial() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdControleMensagemSalarial'.
     * 
     * @param cdControleMensagemSalarial the value of field
     * 'cdControleMensagemSalarial'.
     */
    public void setCdControleMensagemSalarial(int cdControleMensagemSalarial)
    {
        this._cdControleMensagemSalarial = cdControleMensagemSalarial;
        this._has_cdControleMensagemSalarial = true;
    } //-- void setCdControleMensagemSalarial(int) 

    /**
     * Sets the value of field 'cdRestMensagemComprovante'.
     * 
     * @param cdRestMensagemComprovante the value of field
     * 'cdRestMensagemComprovante'.
     */
    public void setCdRestMensagemComprovante(int cdRestMensagemComprovante)
    {
        this._cdRestMensagemComprovante = cdRestMensagemComprovante;
        this._has_cdRestMensagemComprovante = true;
    } //-- void setCdRestMensagemComprovante(int) 

    /**
     * Sets the value of field 'cdTipoComprovanteSalarial'.
     * 
     * @param cdTipoComprovanteSalarial the value of field
     * 'cdTipoComprovanteSalarial'.
     */
    public void setCdTipoComprovanteSalarial(int cdTipoComprovanteSalarial)
    {
        this._cdTipoComprovanteSalarial = cdTipoComprovanteSalarial;
        this._has_cdTipoComprovanteSalarial = true;
    } //-- void setCdTipoComprovanteSalarial(int) 

    /**
     * Sets the value of field 'cdTipoMensagemSalarial'.
     * 
     * @param cdTipoMensagemSalarial the value of field
     * 'cdTipoMensagemSalarial'.
     */
    public void setCdTipoMensagemSalarial(int cdTipoMensagemSalarial)
    {
        this._cdTipoMensagemSalarial = cdTipoMensagemSalarial;
        this._has_cdTipoMensagemSalarial = true;
    } //-- void setCdTipoMensagemSalarial(int) 

    /**
     * Sets the value of field 'dsControleMensagemSalarial'.
     * 
     * @param dsControleMensagemSalarial the value of field
     * 'dsControleMensagemSalarial'.
     */
    public void setDsControleMensagemSalarial(java.lang.String dsControleMensagemSalarial)
    {
        this._dsControleMensagemSalarial = dsControleMensagemSalarial;
    } //-- void setDsControleMensagemSalarial(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return AlterarMsgComprovanteSalrlRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.alterarmsgcomprovantesalrl.request.AlterarMsgComprovanteSalrlRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.alterarmsgcomprovantesalrl.request.AlterarMsgComprovanteSalrlRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.alterarmsgcomprovantesalrl.request.AlterarMsgComprovanteSalrlRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.alterarmsgcomprovantesalrl.request.AlterarMsgComprovanteSalrlRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
