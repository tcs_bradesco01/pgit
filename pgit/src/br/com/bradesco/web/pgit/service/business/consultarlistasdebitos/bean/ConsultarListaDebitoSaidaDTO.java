/*
 * Nome: br.com.bradesco.web.pgit.service.business.consultarlistasdebitos.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.consultarlistasdebitos.bean;

import java.math.BigDecimal;

/**
 * Nome: ConsultarListaDebitoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarListaDebitoSaidaDTO {
    
    /** Atributo codMensagem. */
    private String codMensagem;
    
    /** Atributo mensagem. */
    private String mensagem;
    
    /** Atributo numeroCpfCnpj. */
    private String numeroCpfCnpj;
    
    /** Atributo dsPessoaEmpresa. */
    private String dsPessoaEmpresa;
    
    /** Atributo cdPessoaJuridicaContrato. */
    private Long cdPessoaJuridicaContrato;
    
    /** Atributo cdTipoContratoNegocio. */
    private Integer cdTipoContratoNegocio;
    
    /** Atributo nrSequenciaContratoNegocio. */
    private Long nrSequenciaContratoNegocio;
    
    /** Atributo dsTipoContratoNegocio. */
    private String dsTipoContratoNegocio;
  
    /** Atributo cdListaDebitoPagamento. */
    private Long cdListaDebitoPagamento;
    
    /** Atributo cdSituacaoListaDebito. */
    private Integer cdSituacaoListaDebito;
    
    /** Atributo dsSituacaoListaDebito. */
    private String dsSituacaoListaDebito;
    
    /** Atributo dtPrevistaPagamento. */
    private String dtPrevistaPagamento;
    
    /** Atributo dtPrevistaPagamentoFormatada. */
    private String dtPrevistaPagamentoFormatada;
    
    /** Atributo cdProdutoServicoOperacao. */
    private Integer cdProdutoServicoOperacao;
    
    /** Atributo dsResumoProdutoServico. */
    private String dsResumoProdutoServico;
    
    /** Atributo cdProdutoServicoRelacionado. */
    private Integer cdProdutoServicoRelacionado;
    
    /** Atributo dsProdutoServicoRelacionado. */
    private String dsProdutoServicoRelacionado;
    
    /** Atributo cdBanco. */
    private Integer cdBanco;
    
    /** Atributo dsBanco. */
    private String dsBanco;
    
    /** Atributo cdAgencia. */
    private Integer cdAgencia;
    
    /** Atributo cdDigitoAgencia. */
    private Integer cdDigitoAgencia;
    
    /** Atributo dsAgencia. */
    private String dsAgencia;
    
    /** Atributo cdConta. */
    private Long cdConta;
    
    /** Atributo cdDigitoConta. */
    private String cdDigitoConta;
    
    /** Atributo cdTipoConta. */
    private Integer cdTipoConta;
    
    /** Atributo dsTipoConta. */
    private String dsTipoConta;
    
    /** Atributo qtPagamentoAutorizado. */
    private Long qtPagamentoAutorizado;
    
    /** Atributo vlPagamentoAutorizado. */
    private BigDecimal vlPagamentoAutorizado;
    
    /** Atributo qtPagamentoDesautorizado. */
    private Long qtPagamentoDesautorizado;
    
    /** Atributo vlPagamentoDesautorizado. */
    private BigDecimal vlPagamentoDesautorizado;
    
    /** Atributo qtPagamendoEfetivado. */
    private Long qtPagamendoEfetivado;
    
    /** Atributo vlPagamentoEfetivado. */
    private BigDecimal vlPagamentoEfetivado;
    
    /** Atributo qtPagamentoNaoEfetivado. */
    private Long qtPagamentoNaoEfetivado;
    
    /** Atributo vlPagamentoNaoEfetivado. */
    private BigDecimal vlPagamentoNaoEfetivado;
    
    /** Atributo qtTotalPagamento. */
    private Long qtTotalPagamento;
    
    /** Atributo vlTotalPagamentos. */
    private BigDecimal vlTotalPagamentos;
    
    /** Atributo contaDebitoFormatado. */
    private String contaDebitoFormatado;
    
    /** Atributo numeroCpfCnpjFormatado. */
    private String numeroCpfCnpjFormatado;
    
    /** Atributo dsEmpresa. */
    private String dsEmpresa;
    
    /** Atributo cdServicoCompostoPagamento. */
    private Long cdServicoCompostoPagamento;
    
	/**
	 * Get: dsEmpresa.
	 *
	 * @return dsEmpresa
	 */
	public String getDsEmpresa() {
		return dsEmpresa;
	}
	
	/**
	 * Set: dsEmpresa.
	 *
	 * @param dsEmpresa the ds empresa
	 */
	public void setDsEmpresa(String dsEmpresa) {
		this.dsEmpresa = dsEmpresa;
	}
	
	/**
	 * Get: cdAgencia.
	 *
	 * @return cdAgencia
	 */
	public Integer getCdAgencia() {
		return cdAgencia;
	}
	
	/**
	 * Set: cdAgencia.
	 *
	 * @param cdAgencia the cd agencia
	 */
	public void setCdAgencia(Integer cdAgencia) {
		this.cdAgencia = cdAgencia;
	}
	
	/**
	 * Get: cdBanco.
	 *
	 * @return cdBanco
	 */
	public Integer getCdBanco() {
		return cdBanco;
	}
	
	/**
	 * Set: cdBanco.
	 *
	 * @param cdBanco the cd banco
	 */
	public void setCdBanco(Integer cdBanco) {
		this.cdBanco = cdBanco;
	}
	
	/**
	 * Get: cdConta.
	 *
	 * @return cdConta
	 */
	public Long getCdConta() {
		return cdConta;
	}
	
	/**
	 * Set: cdConta.
	 *
	 * @param cdConta the cd conta
	 */
	public void setCdConta(Long cdConta) {
		this.cdConta = cdConta;
	}
	
	/**
	 * Get: cdDigitoAgencia.
	 *
	 * @return cdDigitoAgencia
	 */
	public Integer getCdDigitoAgencia() {
		return cdDigitoAgencia;
	}
	
	/**
	 * Set: cdDigitoAgencia.
	 *
	 * @param cdDigitoAgencia the cd digito agencia
	 */
	public void setCdDigitoAgencia(Integer cdDigitoAgencia) {
		this.cdDigitoAgencia = cdDigitoAgencia;
	}
	
	/**
	 * Get: cdDigitoConta.
	 *
	 * @return cdDigitoConta
	 */
	public String getCdDigitoConta() {
		return cdDigitoConta;
	}
	
	/**
	 * Set: cdDigitoConta.
	 *
	 * @param cdDigitoConta the cd digito conta
	 */
	public void setCdDigitoConta(String cdDigitoConta) {
		this.cdDigitoConta = cdDigitoConta;
	}
	
	/**
	 * Get: cdListaDebitoPagamento.
	 *
	 * @return cdListaDebitoPagamento
	 */
	public Long getCdListaDebitoPagamento() {
		return cdListaDebitoPagamento;
	}
	
	/**
	 * Set: cdListaDebitoPagamento.
	 *
	 * @param cdListaDebitoPagamento the cd lista debito pagamento
	 */
	public void setCdListaDebitoPagamento(Long cdListaDebitoPagamento) {
		this.cdListaDebitoPagamento = cdListaDebitoPagamento;
	}
	
	/**
	 * Get: cdPessoaJuridicaContrato.
	 *
	 * @return cdPessoaJuridicaContrato
	 */
	public Long getCdPessoaJuridicaContrato() {
		return cdPessoaJuridicaContrato;
	}
	
	/**
	 * Set: cdPessoaJuridicaContrato.
	 *
	 * @param cdPessoaJuridicaContrato the cd pessoa juridica contrato
	 */
	public void setCdPessoaJuridicaContrato(Long cdPessoaJuridicaContrato) {
		this.cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
	}
	
	/**
	 * Get: cdProdutoServicoOperacao.
	 *
	 * @return cdProdutoServicoOperacao
	 */
	public Integer getCdProdutoServicoOperacao() {
		return cdProdutoServicoOperacao;
	}
	
	/**
	 * Set: cdProdutoServicoOperacao.
	 *
	 * @param cdProdutoServicoOperacao the cd produto servico operacao
	 */
	public void setCdProdutoServicoOperacao(Integer cdProdutoServicoOperacao) {
		this.cdProdutoServicoOperacao = cdProdutoServicoOperacao;
	}
	
	/**
	 * Get: cdProdutoServicoRelacionado.
	 *
	 * @return cdProdutoServicoRelacionado
	 */
	public Integer getCdProdutoServicoRelacionado() {
		return cdProdutoServicoRelacionado;
	}
	
	/**
	 * Set: cdProdutoServicoRelacionado.
	 *
	 * @param cdProdutoServicoRelacionado the cd produto servico relacionado
	 */
	public void setCdProdutoServicoRelacionado(Integer cdProdutoServicoRelacionado) {
		this.cdProdutoServicoRelacionado = cdProdutoServicoRelacionado;
	}
	
	/**
	 * Get: cdSituacaoListaDebito.
	 *
	 * @return cdSituacaoListaDebito
	 */
	public Integer getCdSituacaoListaDebito() {
		return cdSituacaoListaDebito;
	}
	
	/**
	 * Set: cdSituacaoListaDebito.
	 *
	 * @param cdSituacaoListaDebito the cd situacao lista debito
	 */
	public void setCdSituacaoListaDebito(Integer cdSituacaoListaDebito) {
		this.cdSituacaoListaDebito = cdSituacaoListaDebito;
	}
	
	/**
	 * Get: cdTipoConta.
	 *
	 * @return cdTipoConta
	 */
	public Integer getCdTipoConta() {
		return cdTipoConta;
	}
	
	/**
	 * Set: cdTipoConta.
	 *
	 * @param cdTipoConta the cd tipo conta
	 */
	public void setCdTipoConta(Integer cdTipoConta) {
		this.cdTipoConta = cdTipoConta;
	}
	
	/**
	 * Get: cdTipoContratoNegocio.
	 *
	 * @return cdTipoContratoNegocio
	 */
	public Integer getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}
	
	/**
	 * Set: cdTipoContratoNegocio.
	 *
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}
	
	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}
	
	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}
	
	/**
	 * Get: dsAgencia.
	 *
	 * @return dsAgencia
	 */
	public String getDsAgencia() {
		return dsAgencia;
	}
	
	/**
	 * Set: dsAgencia.
	 *
	 * @param dsAgencia the ds agencia
	 */
	public void setDsAgencia(String dsAgencia) {
		this.dsAgencia = dsAgencia;
	}
	
	/**
	 * Get: dsBanco.
	 *
	 * @return dsBanco
	 */
	public String getDsBanco() {
		return dsBanco;
	}
	
	/**
	 * Set: dsBanco.
	 *
	 * @param dsBanco the ds banco
	 */
	public void setDsBanco(String dsBanco) {
		this.dsBanco = dsBanco;
	}
	
	/**
	 * Get: dsPessoaEmpresa.
	 *
	 * @return dsPessoaEmpresa
	 */
	public String getDsPessoaEmpresa() {
		return dsPessoaEmpresa;
	}
	
	/**
	 * Set: dsPessoaEmpresa.
	 *
	 * @param dsPessoaEmpresa the ds pessoa empresa
	 */
	public void setDsPessoaEmpresa(String dsPessoaEmpresa) {
		this.dsPessoaEmpresa = dsPessoaEmpresa;
	}
	
	/**
	 * Get: dsProdutoServicoRelacionado.
	 *
	 * @return dsProdutoServicoRelacionado
	 */
	public String getDsProdutoServicoRelacionado() {
		return dsProdutoServicoRelacionado;
	}
	
	/**
	 * Set: dsProdutoServicoRelacionado.
	 *
	 * @param dsProdutoServicoRelacionado the ds produto servico relacionado
	 */
	public void setDsProdutoServicoRelacionado(String dsProdutoServicoRelacionado) {
		this.dsProdutoServicoRelacionado = dsProdutoServicoRelacionado;
	}
	
	/**
	 * Get: dsResumoProdutoServico.
	 *
	 * @return dsResumoProdutoServico
	 */
	public String getDsResumoProdutoServico() {
		return dsResumoProdutoServico;
	}
	
	/**
	 * Set: dsResumoProdutoServico.
	 *
	 * @param dsResumoProdutoServico the ds resumo produto servico
	 */
	public void setDsResumoProdutoServico(String dsResumoProdutoServico) {
		this.dsResumoProdutoServico = dsResumoProdutoServico;
	}
	
	/**
	 * Get: dsSituacaoListaDebito.
	 *
	 * @return dsSituacaoListaDebito
	 */
	public String getDsSituacaoListaDebito() {
		return dsSituacaoListaDebito;
	}
	
	/**
	 * Set: dsSituacaoListaDebito.
	 *
	 * @param dsSituacaoListaDebito the ds situacao lista debito
	 */
	public void setDsSituacaoListaDebito(String dsSituacaoListaDebito) {
		this.dsSituacaoListaDebito = dsSituacaoListaDebito;
	}
	
	/**
	 * Get: dsTipoConta.
	 *
	 * @return dsTipoConta
	 */
	public String getDsTipoConta() {
		return dsTipoConta;
	}
	
	/**
	 * Set: dsTipoConta.
	 *
	 * @param dsTipoConta the ds tipo conta
	 */
	public void setDsTipoConta(String dsTipoConta) {
		this.dsTipoConta = dsTipoConta;
	}
	
	/**
	 * Get: dsTipoContratoNegocio.
	 *
	 * @return dsTipoContratoNegocio
	 */
	public String getDsTipoContratoNegocio() {
		return dsTipoContratoNegocio;
	}
	
	/**
	 * Set: dsTipoContratoNegocio.
	 *
	 * @param dsTipoContratoNegocio the ds tipo contrato negocio
	 */
	public void setDsTipoContratoNegocio(String dsTipoContratoNegocio) {
		this.dsTipoContratoNegocio = dsTipoContratoNegocio;
	}
	
	/**
	 * Get: dtPrevistaPagamento.
	 *
	 * @return dtPrevistaPagamento
	 */
	public String getDtPrevistaPagamento() {
		return dtPrevistaPagamento;
	}
	
	/**
	 * Set: dtPrevistaPagamento.
	 *
	 * @param dtPrevistaPagamento the dt prevista pagamento
	 */
	public void setDtPrevistaPagamento(String dtPrevistaPagamento) {
		this.dtPrevistaPagamento = dtPrevistaPagamento;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	/**
	 * Get: nrSequenciaContratoNegocio.
	 *
	 * @return nrSequenciaContratoNegocio
	 */
	public Long getNrSequenciaContratoNegocio() {
		return nrSequenciaContratoNegocio;
	}
	
	/**
	 * Set: nrSequenciaContratoNegocio.
	 *
	 * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
	 */
	public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}
	
	/**
	 * Get: numeroCpfCnpj.
	 *
	 * @return numeroCpfCnpj
	 */
	public String getNumeroCpfCnpj() {
		return numeroCpfCnpj;
	}
	
	/**
	 * Set: numeroCpfCnpj.
	 *
	 * @param numeroCpfCnpj the numero cpf cnpj
	 */
	public void setNumeroCpfCnpj(String numeroCpfCnpj) {
		this.numeroCpfCnpj = numeroCpfCnpj;
	}
	
	/**
	 * Get: qtPagamendoEfetivado.
	 *
	 * @return qtPagamendoEfetivado
	 */
	public Long getQtPagamendoEfetivado() {
		return qtPagamendoEfetivado;
	}
	
	/**
	 * Set: qtPagamendoEfetivado.
	 *
	 * @param qtPagamendoEfetivado the qt pagamendo efetivado
	 */
	public void setQtPagamendoEfetivado(Long qtPagamendoEfetivado) {
		this.qtPagamendoEfetivado = qtPagamendoEfetivado;
	}
	
	/**
	 * Get: qtPagamentoAutorizado.
	 *
	 * @return qtPagamentoAutorizado
	 */
	public Long getQtPagamentoAutorizado() {
		return qtPagamentoAutorizado;
	}
	
	/**
	 * Set: qtPagamentoAutorizado.
	 *
	 * @param qtPagamentoAutorizado the qt pagamento autorizado
	 */
	public void setQtPagamentoAutorizado(Long qtPagamentoAutorizado) {
		this.qtPagamentoAutorizado = qtPagamentoAutorizado;
	}
	
	/**
	 * Get: qtPagamentoDesautorizado.
	 *
	 * @return qtPagamentoDesautorizado
	 */
	public Long getQtPagamentoDesautorizado() {
		return qtPagamentoDesautorizado;
	}
	
	/**
	 * Set: qtPagamentoDesautorizado.
	 *
	 * @param qtPagamentoDesautorizado the qt pagamento desautorizado
	 */
	public void setQtPagamentoDesautorizado(Long qtPagamentoDesautorizado) {
		this.qtPagamentoDesautorizado = qtPagamentoDesautorizado;
	}
	
	/**
	 * Get: qtPagamentoNaoEfetivado.
	 *
	 * @return qtPagamentoNaoEfetivado
	 */
	public Long getQtPagamentoNaoEfetivado() {
		return qtPagamentoNaoEfetivado;
	}
	
	/**
	 * Set: qtPagamentoNaoEfetivado.
	 *
	 * @param qtPagamentoNaoEfetivado the qt pagamento nao efetivado
	 */
	public void setQtPagamentoNaoEfetivado(Long qtPagamentoNaoEfetivado) {
		this.qtPagamentoNaoEfetivado = qtPagamentoNaoEfetivado;
	}
	
	/**
	 * Get: qtTotalPagamento.
	 *
	 * @return qtTotalPagamento
	 */
	public Long getQtTotalPagamento() {
		return qtTotalPagamento;
	}
	
	/**
	 * Set: qtTotalPagamento.
	 *
	 * @param qtTotalPagamento the qt total pagamento
	 */
	public void setQtTotalPagamento(Long qtTotalPagamento) {
		this.qtTotalPagamento = qtTotalPagamento;
	}
	
	/**
	 * Get: vlPagamentoAutorizado.
	 *
	 * @return vlPagamentoAutorizado
	 */
	public BigDecimal getVlPagamentoAutorizado() {
		return vlPagamentoAutorizado;
	}
	
	/**
	 * Set: vlPagamentoAutorizado.
	 *
	 * @param vlPagamentoAutorizado the vl pagamento autorizado
	 */
	public void setVlPagamentoAutorizado(BigDecimal vlPagamentoAutorizado) {
		this.vlPagamentoAutorizado = vlPagamentoAutorizado;
	}
	
	/**
	 * Get: vlPagamentoDesautorizado.
	 *
	 * @return vlPagamentoDesautorizado
	 */
	public BigDecimal getVlPagamentoDesautorizado() {
		return vlPagamentoDesautorizado;
	}
	
	/**
	 * Set: vlPagamentoDesautorizado.
	 *
	 * @param vlPagamentoDesautorizado the vl pagamento desautorizado
	 */
	public void setVlPagamentoDesautorizado(BigDecimal vlPagamentoDesautorizado) {
		this.vlPagamentoDesautorizado = vlPagamentoDesautorizado;
	}
	
	/**
	 * Get: vlPagamentoEfetivado.
	 *
	 * @return vlPagamentoEfetivado
	 */
	public BigDecimal getVlPagamentoEfetivado() {
		return vlPagamentoEfetivado;
	}
	
	/**
	 * Set: vlPagamentoEfetivado.
	 *
	 * @param vlPagamentoEfetivado the vl pagamento efetivado
	 */
	public void setVlPagamentoEfetivado(BigDecimal vlPagamentoEfetivado) {
		this.vlPagamentoEfetivado = vlPagamentoEfetivado;
	}
	
	/**
	 * Get: vlPagamentoNaoEfetivado.
	 *
	 * @return vlPagamentoNaoEfetivado
	 */
	public BigDecimal getVlPagamentoNaoEfetivado() {
		return vlPagamentoNaoEfetivado;
	}
	
	/**
	 * Set: vlPagamentoNaoEfetivado.
	 *
	 * @param vlPagamentoNaoEfetivado the vl pagamento nao efetivado
	 */
	public void setVlPagamentoNaoEfetivado(BigDecimal vlPagamentoNaoEfetivado) {
		this.vlPagamentoNaoEfetivado = vlPagamentoNaoEfetivado;
	}
	
	/**
	 * Get: vlTotalPagamentos.
	 *
	 * @return vlTotalPagamentos
	 */
	public BigDecimal getVlTotalPagamentos() {
		return vlTotalPagamentos;
	}
	
	/**
	 * Set: vlTotalPagamentos.
	 *
	 * @param vlTotalPagamentos the vl total pagamentos
	 */
	public void setVlTotalPagamentos(BigDecimal vlTotalPagamentos) {
		this.vlTotalPagamentos = vlTotalPagamentos;
	}
	
	/**
	 * Get: contaDebitoFormatado.
	 *
	 * @return contaDebitoFormatado
	 */
	public String getContaDebitoFormatado() {
		return contaDebitoFormatado;
	}
	
	/**
	 * Set: contaDebitoFormatado.
	 *
	 * @param contaDebitoFormatado the conta debito formatado
	 */
	public void setContaDebitoFormatado(String contaDebitoFormatado) {
		this.contaDebitoFormatado = contaDebitoFormatado;
	}
	
	/**
	 * Get: numeroCpfCnpjFormatado.
	 *
	 * @return numeroCpfCnpjFormatado
	 */
	public String getNumeroCpfCnpjFormatado() {
		return numeroCpfCnpjFormatado;
	}
	
	/**
	 * Set: numeroCpfCnpjFormatado.
	 *
	 * @param numeroCpfCnpjFormatado the numero cpf cnpj formatado
	 */
	public void setNumeroCpfCnpjFormatado(String numeroCpfCnpjFormatado) {
		this.numeroCpfCnpjFormatado = numeroCpfCnpjFormatado;
	}
	
	/**
	 * Get: dtPrevistaPagamentoFormatada.
	 *
	 * @return dtPrevistaPagamentoFormatada
	 */
	public String getDtPrevistaPagamentoFormatada() {
		return dtPrevistaPagamentoFormatada;
	}
	
	/**
	 * Set: dtPrevistaPagamentoFormatada.
	 *
	 * @param dtPrevistaPagamentoFormatada the dt prevista pagamento formatada
	 */
	public void setDtPrevistaPagamentoFormatada(String dtPrevistaPagamentoFormatada) {
		this.dtPrevistaPagamentoFormatada = dtPrevistaPagamentoFormatada;
	}
	
	/**
	 * Get: cdServicoCompostoPagamento.
	 *
	 * @return cdServicoCompostoPagamento
	 */
	public Long getCdServicoCompostoPagamento() {
		return cdServicoCompostoPagamento;
	}
	
	/**
	 * Set: cdServicoCompostoPagamento.
	 *
	 * @param cdServicoCompostoPagamento the cd servico composto pagamento
	 */
	public void setCdServicoCompostoPagamento(Long cdServicoCompostoPagamento) {
		this.cdServicoCompostoPagamento = cdServicoCompostoPagamento;
	} 
	
	
}