/*
 * Nome: br.com.bradesco.web.pgit.service.business.imprimiranexoprimeirocontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.imprimiranexoprimeirocontrato.bean;

import java.util.List;

import br.com.bradesco.web.pgit.service.business.confcestastarifas.dto.OcorrenciasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.imprimiranexosegundocontrato.bean.OcorrenciasAvisoFormularios;
import br.com.bradesco.web.pgit.service.business.imprimiranexosegundocontrato.bean.OcorrenciasAvisos;
import br.com.bradesco.web.pgit.service.business.imprimiranexosegundocontrato.bean.OcorrenciasComprovanteSalariosDiversos;
import br.com.bradesco.web.pgit.service.business.imprimiranexosegundocontrato.bean.OcorrenciasComprovantes;
import br.com.bradesco.web.pgit.service.business.imprimiranexosegundocontrato.bean.OcorrenciasCreditos;
import br.com.bradesco.web.pgit.service.business.imprimiranexosegundocontrato.bean.OcorrenciasModBeneficios;
import br.com.bradesco.web.pgit.service.business.imprimiranexosegundocontrato.bean.OcorrenciasModFornecedores;
import br.com.bradesco.web.pgit.service.business.imprimiranexosegundocontrato.bean.OcorrenciasModRecadastro;
import br.com.bradesco.web.pgit.service.business.imprimiranexosegundocontrato.bean.OcorrenciasModSalarial;
import br.com.bradesco.web.pgit.service.business.imprimiranexosegundocontrato.bean.OcorrenciasModTributos;
import br.com.bradesco.web.pgit.service.business.imprimiranexosegundocontrato.bean.OcorrenciasModularidades;
import br.com.bradesco.web.pgit.service.business.imprimiranexosegundocontrato.bean.OcorrenciasOperacoes;
import br.com.bradesco.web.pgit.service.business.imprimiranexosegundocontrato.bean.OcorrenciasServicoPagamentos;
import br.com.bradesco.web.pgit.service.business.imprimiranexosegundocontrato.bean.OcorrenciasTitulos;
import br.com.bradesco.web.pgit.service.business.imprimiranexosegundocontrato.bean.OcorrenciasTriTributo;

/**
 * Nome: AnexoContratoDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class AnexoContratoDTO {
	//primeiro contrato
	/** Atributo codMensagem. */
	private String codMensagem;
    
    /** Atributo mensagem. */
    private String mensagem;
    
    /** Atributo nrSequenciaContrato. */
    private Long nrSequenciaContrato;
    
    /** Atributo dtInclusaoContrato. */
    private String dtInclusaoContrato;
    
    /** Atributo dtInicioVigencia. */
    private String dtInicioVigencia;
    
    /** Atributo dtFinalVigencia. */
    private String dtFinalVigencia;
    
    /** Atributo cdCpfCnpjRepresentante. */
    private String cdCpfCnpjRepresentante;
    
    /** Atributo dsNomeRepresentante. */
    private String dsNomeRepresentante;
    
    /** Atributo cdGrupoEconomico. */
    private Long cdGrupoEconomico;
    
    /** Atributo dsGrupoEconomico. */
    private String dsGrupoEconomico;
    
    /** Atributo cdAtividadeEconomica. */
    private Integer cdAtividadeEconomica;
    
    /** Atributo dsAtividadeEconomica. */
    private String dsAtividadeEconomica;
    
    /** Atributo cdSegmentoEconomico. */
    private Integer cdSegmentoEconomico;
    
    /** Atributo dsSegmentoEconomico. */
    private String dsSegmentoEconomico;
    
    /** Atributo cdSubSegmento. */
    private Integer cdSubSegmento;
    
    /** Atributo dsSubSegmento. */
    private String dsSubSegmento;
    
    /** Atributo dsContrato. */
    private String dsContrato;
    
    /** Atributo cdAgenciaGestoraContrato. */
    private Integer cdAgenciaGestoraContrato;
    
    /** Atributo dsAgenciaGestoraContrato. */
    private String dsAgenciaGestoraContrato;
    
    /** Atributo qtdeParticipante. */
    private Integer qtdeParticipante;
    
    /** Atributo listaParticipantes. */
    private List<OcorrenciasParticipantes> listaParticipantes;    
    
    /** Atributo qtdeConta. */
    private Integer qtdeConta;    
    
    /** Atributo listaContas. */
    private List<OcorrenciasContas> listaContas;    
    
    /** Atributo qtLayout. */
    private Integer qtLayout;    
    
    /** Atributo listaLayouts. */
    private List<OcorrenciasLayout> listaLayouts;
    
    /** Atributo qtCestasTarifas. */
    private Integer qtCestasTarifas;
    
    /** Atributo listaCestasTarifas. */
    private List<OcorrenciasSaidaDTO> listaCestasTarifas;
    
    //segundo contrato    
	/** Atributo cdServicoFornecedor. */
    private String cdServicoFornecedor;
	
	/** Atributo qtModalidadeFornecedor. */
	private Integer qtModalidadeFornecedor;        
	
	/** Atributo listModFornecedores. */
	private List<OcorrenciasModFornecedores> listModFornecedores;        
	
	/** Atributo cdServicoTributos. */
	private String cdServicoTributos;
	
	/** Atributo qtModalidadeTributos. */
	private Integer qtModalidadeTributos;        
	
	/** Atributo listaModTributos. */
	private List<OcorrenciasModTributos> listaModTributos;        
	
	/** Atributo cdServicoSalarial. */
	private String cdServicoSalarial;
	
	/** Atributo qtModalidadeSalario. */
	private Integer qtModalidadeSalario;
	
	/** Atributo listaModSalario. */
	private List<OcorrenciasModSalarial> listaModSalario;        
	
	/** Atributo cdServicoBeneficio. */
	private String cdServicoBeneficio;        
	
	/** Atributo qtModalidadeBeneficio. */
	private Integer qtModalidadeBeneficio;
	
	/** Atributo listaModBeneficio. */
	private List<OcorrenciasModBeneficios> listaModBeneficio;        
	
	/** Atributo cdIndicadorAviso. */
	private String cdIndicadorAviso;
	
	/** Atributo cdIndicadorAvisoPagador. */
	private String cdIndicadorAvisoPagador;
	
	/** Atributo cdIndicadorAvisoFavorecido. */
	private String cdIndicadorAvisoFavorecido;
	
	/** Atributo cdIndicadorComplemento. */
	private String cdIndicadorComplemento;
	
	/** Atributo cdIndicadorComplementoPagador. */
	private String cdIndicadorComplementoPagador;
	
	/** Atributo cdIndicadorComplementoFavorecido. */
	private String cdIndicadorComplementoFavorecido;
	
	/** Atributo cdIndicadorComplementoSalarial. */
	private String cdIndicadorComplementoSalarial;
	
	/** Atributo cdIndicadorComplementoDivergente. */
	private String cdIndicadorComplementoDivergente;
	
	/** Atributo cdIndcadorCadastroFavorecido. */
	private String cdIndcadorCadastroFavorecido;
	
	/** Atributo dsServicoRecadastro. */
	private String dsServicoRecadastro;
	
	/** Atributo qtModalidadeRecadastro. */
	private Integer qtModalidadeRecadastro;        
	
	/** Atributo listaModRecadastro. */
	private List<OcorrenciasModRecadastro> listaModRecadastro;        
	
	/** Atributo dsSalarioEmissaoAntecipadoCartao. */
	private String dsSalarioEmissaoAntecipadoCartao;
	
	/** Atributo cdSalarioQuantidadeLimiteSolicitacaoCartao. */
	private Integer cdSalarioQuantidadeLimiteSolicitacaoCartao;
	
	/** Atributo dsSalarioAberturaContaExpressa. */
	private String dsSalarioAberturaContaExpressa;
	
	/** Atributo dsTibutoAgendadoDebitoVeicular. */
	private String dsTibutoAgendadoDebitoVeicular;
	
	/** Atributo dsBeneficioInformadoAgenciaConta. */
	private String dsBeneficioInformadoAgenciaConta;
	
	/** Atributo dsBeneficioTipoIdentificacaoBeneficio. */
	private String dsBeneficioTipoIdentificacaoBeneficio;
	
	/** Atributo dsBeneficioUtilizacaoCadastroOrganizacao. */
	private String dsBeneficioUtilizacaoCadastroOrganizacao;
	
	/** Atributo dsBeneficioUtilizacaoCadastroProcuradores. */
	private String dsBeneficioUtilizacaoCadastroProcuradores;
	
	/** Atributo dsBeneficioPossuiExpiracaoCredito. */
	private String dsBeneficioPossuiExpiracaoCredito;
	
	/** Atributo cdBeneficioQuantidadeDiasExpiracaoCredito. */
	private Integer cdBeneficioQuantidadeDiasExpiracaoCredito;
	
	/** Atributo cdServicoQuantidadeServicoPagamento. */
	private Integer cdServicoQuantidadeServicoPagamento;        
	
	/** Atributo listaServicoPagamento. */
	private List<OcorrenciasServicoPagamentos> listaServicoPagamento;        
	
	/** Atributo qtAviso. */
	private Integer qtAviso;
	
	/** Atributo listaAvisos. */
	private List<OcorrenciasAvisos> listaAvisos;        
	
	/** Atributo qtComprovante. */
	private Integer qtComprovante;
	
	/** Atributo listaComprovantes. */
	private List<OcorrenciasComprovantes> listaComprovantes;        
	
	/** Atributo qtComprovanteSalarioDiversos. */
	private Integer qtComprovanteSalarioDiversos;
	
	/** Atributo listaComprovanteSalarioDiversos. */
	private List<OcorrenciasComprovanteSalariosDiversos> listaComprovanteSalarioDiversos;        
	
	/** Atributo dsCadastroFormaManutencao. */
	private String dsCadastroFormaManutencao;
	
	/** Atributo cdCadastroQuantidadeDiasInativos. */
	private Integer cdCadastroQuantidadeDiasInativos;
	
	/** Atributo dsCadastroTipoConsistenciaInscricao. */
	private String dsCadastroTipoConsistenciaInscricao;
	
	/** Atributo dsRecTipoIdentificacaoBeneficio. */
	private String dsRecTipoIdentificacaoBeneficio;
	
	/** Atributo dsRecTipoConsistenciaIdentificacao. */
	private String dsRecTipoConsistenciaIdentificacao;
	
	/** Atributo dsRecCondicaoEnquadramento. */
	private String dsRecCondicaoEnquadramento;
	
	/** Atributo dsRecCriterioPrincipalEnquadramento. */
	private String dsRecCriterioPrincipalEnquadramento;
	
	/** Atributo dsRecTipoCriterioCompostoEnquadramento. */
	private String dsRecTipoCriterioCompostoEnquadramento;
	
	/** Atributo qtRecEtapaRecadastramento. */
	private Integer qtRecEtapaRecadastramento;
	
	/** Atributo qtRecFaseEtapaRecadastramento. */
	private Integer qtRecFaseEtapaRecadastramento;
	
	/** Atributo dtRecInicioRecadastramento. */
	private String dtRecInicioRecadastramento;
	
	/** Atributo dtRecFinalRecadastramento. */
	private String dtRecFinalRecadastramento;
	
	/** Atributo dsRecGeradorRetornoInternet. */
	private String dsRecGeradorRetornoInternet;
	
	/** Atributo qtForTitulo. */
	private Integer qtForTitulo;        		
	
	/** Atributo listaTitulos. */
	private List<OcorrenciasTitulos> listaTitulos;        
	
	/** Atributo qtCredito. */
	private Integer qtCredito;
	
	/** Atributo listaCreditos. */
	private List<OcorrenciasCreditos> listaCreditos;        
	
	/** Atributo qtOperacao. */
	private Integer qtOperacao;
	
	/** Atributo listaOperacoes. */
	private List<OcorrenciasOperacoes> listaOperacoes;        
	
	/** Atributo qtModularidade. */
	private Integer qtModularidade;
	
	/** Atributo listaModularidades. */
	private List<OcorrenciasModularidades> listaModularidades;        
	
	/** Atributo dsFraTipoRastreabilidade. */
	private String dsFraTipoRastreabilidade;
	
	/** Atributo dsFraRastreabilidadeTerceiro. */
	private String dsFraRastreabilidadeTerceiro;
	
	/** Atributo dsFraRastreabilidadeNota. */
	private String dsFraRastreabilidadeNota;
	
	/** Atributo dsFraCaptacaoTitulo. */
	private String dsFraCaptacaoTitulo;
	
	/** Atributo dsFraAgendaCliente. */
	private String dsFraAgendaCliente;
	
	/** Atributo dsFraAgendaFilial. */
	private String dsFraAgendaFilial;
	
	/** Atributo dsFraBloqueioEmissao. */
	private String dsFraBloqueioEmissao;
	
	/** Atributo dtFraInicioBloqueio. */
	private String dtFraInicioBloqueio;
	
	/** Atributo dtFraInicioRastreabilidade. */
	private String dtFraInicioRastreabilidade;
	
	/** Atributo dsFraAdesaoSacado. */
	private String dsFraAdesaoSacado;
	
	/** Atributo dsTdvTipoConsultaProposta. */
	private String dsTdvTipoConsultaProposta;
	
	/** Atributo dsTdvTipoTratamentoValor. */
	private String dsTdvTipoTratamentoValor;
	
	/** Atributo qtTriTributo. */
	private Integer qtTriTributo;        
	
	/** Atributo listaTriTributos. */
	private List<OcorrenciasTriTributo> listaTriTributos;        
	
	/** Atributo dsBprTipoAcao. */
	private String dsBprTipoAcao;
	
	/** Atributo qtBprMesProvisorio. */
	private Integer qtBprMesProvisorio;
	
	/** Atributo cdBprIndicadorEmissaoAviso. */
	private String cdBprIndicadorEmissaoAviso;
	
	/** Atributo qtBprDiaAnteriorAviso. */
	private Integer qtBprDiaAnteriorAviso;
	
	/** Atributo qtBprDiaAnteriorVencimento. */
	private Integer qtBprDiaAnteriorVencimento;
	
	/** Atributo cdBprUtilizadoMensagemPersonalizada. */
	private String cdBprUtilizadoMensagemPersonalizada;
	
	/** Atributo dsBprDestino. */
	private String dsBprDestino;
	
	/** Atributo dsBprCadastroEnderecoUtilizado. */
	private String dsBprCadastroEnderecoUtilizado;
	
	/** Atributo qtAvisoFormulario. */
	private Integer qtAvisoFormulario;        
	
	/** Atributo listaAvisoFormularios. */
	private List<OcorrenciasAvisoFormularios> listaAvisoFormularios;
	
	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}
	
	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	/**
	 * Get: nrSequenciaContrato.
	 *
	 * @return nrSequenciaContrato
	 */
	public Long getNrSequenciaContrato() {
		return nrSequenciaContrato;
	}
	
	/**
	 * Set: nrSequenciaContrato.
	 *
	 * @param nrSequenciaContrato the nr sequencia contrato
	 */
	public void setNrSequenciaContrato(Long nrSequenciaContrato) {
		this.nrSequenciaContrato = nrSequenciaContrato;
	}
	
	/**
	 * Get: dtInclusaoContrato.
	 *
	 * @return dtInclusaoContrato
	 */
	public String getDtInclusaoContrato() {
		return dtInclusaoContrato;
	}
	
	/**
	 * Set: dtInclusaoContrato.
	 *
	 * @param dtInclusaoContrato the dt inclusao contrato
	 */
	public void setDtInclusaoContrato(String dtInclusaoContrato) {
		this.dtInclusaoContrato = dtInclusaoContrato;
	}
	
	/**
	 * Get: dtInicioVigencia.
	 *
	 * @return dtInicioVigencia
	 */
	public String getDtInicioVigencia() {
		return dtInicioVigencia;
	}
	
	/**
	 * Set: dtInicioVigencia.
	 *
	 * @param dtInicioVigencia the dt inicio vigencia
	 */
	public void setDtInicioVigencia(String dtInicioVigencia) {
		this.dtInicioVigencia = dtInicioVigencia;
	}
	
	/**
	 * Get: dtFinalVigencia.
	 *
	 * @return dtFinalVigencia
	 */
	public String getDtFinalVigencia() {
		return dtFinalVigencia;
	}
	
	/**
	 * Set: dtFinalVigencia.
	 *
	 * @param dtFinalVigencia the dt final vigencia
	 */
	public void setDtFinalVigencia(String dtFinalVigencia) {
		this.dtFinalVigencia = dtFinalVigencia;
	}
	
	/**
	 * Get: cdCpfCnpjRepresentante.
	 *
	 * @return cdCpfCnpjRepresentante
	 */
	public String getCdCpfCnpjRepresentante() {
		return cdCpfCnpjRepresentante;
	}
	
	/**
	 * Set: cdCpfCnpjRepresentante.
	 *
	 * @param cdCpfCnpjRepresentante the cd cpf cnpj representante
	 */
	public void setCdCpfCnpjRepresentante(String cdCpfCnpjRepresentante) {
		this.cdCpfCnpjRepresentante = cdCpfCnpjRepresentante;
	}
	
	/**
	 * Get: dsNomeRepresentante.
	 *
	 * @return dsNomeRepresentante
	 */
	public String getDsNomeRepresentante() {
		return dsNomeRepresentante;
	}
	
	/**
	 * Set: dsNomeRepresentante.
	 *
	 * @param dsNomeRepresentante the ds nome representante
	 */
	public void setDsNomeRepresentante(String dsNomeRepresentante) {
		this.dsNomeRepresentante = dsNomeRepresentante;
	}
	
	/**
	 * Get: cdGrupoEconomico.
	 *
	 * @return cdGrupoEconomico
	 */
	public Long getCdGrupoEconomico() {
		return cdGrupoEconomico;
	}
	
	/**
	 * Set: cdGrupoEconomico.
	 *
	 * @param cdGrupoEconomico the cd grupo economico
	 */
	public void setCdGrupoEconomico(Long cdGrupoEconomico) {
		this.cdGrupoEconomico = cdGrupoEconomico;
	}
	
	/**
	 * Get: dsGrupoEconomico.
	 *
	 * @return dsGrupoEconomico
	 */
	public String getDsGrupoEconomico() {
		return dsGrupoEconomico;
	}
	
	/**
	 * Set: dsGrupoEconomico.
	 *
	 * @param dsGrupoEconomico the ds grupo economico
	 */
	public void setDsGrupoEconomico(String dsGrupoEconomico) {
		this.dsGrupoEconomico = dsGrupoEconomico;
	}
	
	/**
	 * Get: cdAtividadeEconomica.
	 *
	 * @return cdAtividadeEconomica
	 */
	public Integer getCdAtividadeEconomica() {
		return cdAtividadeEconomica;
	}
	
	/**
	 * Set: cdAtividadeEconomica.
	 *
	 * @param cdAtividadeEconomica the cd atividade economica
	 */
	public void setCdAtividadeEconomica(Integer cdAtividadeEconomica) {
		this.cdAtividadeEconomica = cdAtividadeEconomica;
	}
	
	/**
	 * Get: dsAtividadeEconomica.
	 *
	 * @return dsAtividadeEconomica
	 */
	public String getDsAtividadeEconomica() {
		return dsAtividadeEconomica;
	}
	
	/**
	 * Set: dsAtividadeEconomica.
	 *
	 * @param dsAtividadeEconomica the ds atividade economica
	 */
	public void setDsAtividadeEconomica(String dsAtividadeEconomica) {
		this.dsAtividadeEconomica = dsAtividadeEconomica;
	}
	
	/**
	 * Get: cdSegmentoEconomico.
	 *
	 * @return cdSegmentoEconomico
	 */
	public Integer getCdSegmentoEconomico() {
		return cdSegmentoEconomico;
	}
	
	/**
	 * Set: cdSegmentoEconomico.
	 *
	 * @param cdSegmentoEconomico the cd segmento economico
	 */
	public void setCdSegmentoEconomico(Integer cdSegmentoEconomico) {
		this.cdSegmentoEconomico = cdSegmentoEconomico;
	}
	
	/**
	 * Get: dsSegmentoEconomico.
	 *
	 * @return dsSegmentoEconomico
	 */
	public String getDsSegmentoEconomico() {
		return dsSegmentoEconomico;
	}
	
	/**
	 * Set: dsSegmentoEconomico.
	 *
	 * @param dsSegmentoEconomico the ds segmento economico
	 */
	public void setDsSegmentoEconomico(String dsSegmentoEconomico) {
		this.dsSegmentoEconomico = dsSegmentoEconomico;
	}
	
	/**
	 * Get: cdSubSegmento.
	 *
	 * @return cdSubSegmento
	 */
	public Integer getCdSubSegmento() {
		return cdSubSegmento;
	}
	
	/**
	 * Set: cdSubSegmento.
	 *
	 * @param cdSubSegmento the cd sub segmento
	 */
	public void setCdSubSegmento(Integer cdSubSegmento) {
		this.cdSubSegmento = cdSubSegmento;
	}
	
	/**
	 * Get: dsSubSegmento.
	 *
	 * @return dsSubSegmento
	 */
	public String getDsSubSegmento() {
		return dsSubSegmento;
	}
	
	/**
	 * Set: dsSubSegmento.
	 *
	 * @param dsSubSegmento the ds sub segmento
	 */
	public void setDsSubSegmento(String dsSubSegmento) {
		this.dsSubSegmento = dsSubSegmento;
	}
	
	/**
	 * Get: dsContrato.
	 *
	 * @return dsContrato
	 */
	public String getDsContrato() {
		return dsContrato;
	}
	
	/**
	 * Set: dsContrato.
	 *
	 * @param dsContrato the ds contrato
	 */
	public void setDsContrato(String dsContrato) {
		this.dsContrato = dsContrato;
	}
	
	/**
	 * Get: cdAgenciaGestoraContrato.
	 *
	 * @return cdAgenciaGestoraContrato
	 */
	public Integer getCdAgenciaGestoraContrato() {
		return cdAgenciaGestoraContrato;
	}
	
	/**
	 * Set: cdAgenciaGestoraContrato.
	 *
	 * @param cdAgenciaGestoraContrato the cd agencia gestora contrato
	 */
	public void setCdAgenciaGestoraContrato(Integer cdAgenciaGestoraContrato) {
		this.cdAgenciaGestoraContrato = cdAgenciaGestoraContrato;
	}
	
	/**
	 * Get: dsAgenciaGestoraContrato.
	 *
	 * @return dsAgenciaGestoraContrato
	 */
	public String getDsAgenciaGestoraContrato() {
		return dsAgenciaGestoraContrato;
	}
	
	/**
	 * Set: dsAgenciaGestoraContrato.
	 *
	 * @param dsAgenciaGestoraContrato the ds agencia gestora contrato
	 */
	public void setDsAgenciaGestoraContrato(String dsAgenciaGestoraContrato) {
		this.dsAgenciaGestoraContrato = dsAgenciaGestoraContrato;
	}
	
	/**
	 * Get: qtdeParticipante.
	 *
	 * @return qtdeParticipante
	 */
	public Integer getQtdeParticipante() {
		return qtdeParticipante;
	}
	
	/**
	 * Set: qtdeParticipante.
	 *
	 * @param qtdeParticipante the qtde participante
	 */
	public void setQtdeParticipante(Integer qtdeParticipante) {
		this.qtdeParticipante = qtdeParticipante;
	}
	
	/**
	 * Get: listaParticipantes.
	 *
	 * @return listaParticipantes
	 */
	public List<OcorrenciasParticipantes> getListaParticipantes() {
		return listaParticipantes;
	}
	
	/**
	 * Set: listaParticipantes.
	 *
	 * @param listaParticipantes the lista participantes
	 */
	public void setListaParticipantes(List<OcorrenciasParticipantes> listaParticipantes) {
		this.listaParticipantes = listaParticipantes;
	}
	
	/**
	 * Get: qtdeConta.
	 *
	 * @return qtdeConta
	 */
	public Integer getQtdeConta() {
		return qtdeConta;
	}
	
	/**
	 * Set: qtdeConta.
	 *
	 * @param qtdeConta the qtde conta
	 */
	public void setQtdeConta(Integer qtdeConta) {
		this.qtdeConta = qtdeConta;
	}
	
	/**
	 * Get: listaContas.
	 *
	 * @return listaContas
	 */
	public List<OcorrenciasContas> getListaContas() {
		return listaContas;
	}
	
	/**
	 * Set: listaContas.
	 *
	 * @param listaContas the lista contas
	 */
	public void setListaContas(List<OcorrenciasContas> listaContas) {
		this.listaContas = listaContas;
	}
	
	/**
	 * Get: qtLayout.
	 *
	 * @return qtLayout
	 */
	public Integer getQtLayout() {
		return qtLayout;
	}
	
	/**
	 * Set: qtLayout.
	 *
	 * @param qtLayout the qt layout
	 */
	public void setQtLayout(Integer qtLayout) {
		this.qtLayout = qtLayout;
	}
	
	/**
	 * Get: listaLayouts.
	 *
	 * @return listaLayouts
	 */
	public List<OcorrenciasLayout> getListaLayouts() {
		return listaLayouts;
	}
	
	/**
	 * Set: listaLayouts.
	 *
	 * @param listaLayouts the lista layouts
	 */
	public void setListaLayouts(List<OcorrenciasLayout> listaLayouts) {
		this.listaLayouts = listaLayouts;
	}
	
	/**
	 * Get: cdServicoFornecedor.
	 *
	 * @return cdServicoFornecedor
	 */
	public String getCdServicoFornecedor() {
		return cdServicoFornecedor;
	}
	
	/**
	 * Set: cdServicoFornecedor.
	 *
	 * @param cdServicoFornecedor the cd servico fornecedor
	 */
	public void setCdServicoFornecedor(String cdServicoFornecedor) {
		this.cdServicoFornecedor = cdServicoFornecedor;
	}
	
	/**
	 * Get: qtModalidadeFornecedor.
	 *
	 * @return qtModalidadeFornecedor
	 */
	public Integer getQtModalidadeFornecedor() {
		return qtModalidadeFornecedor;
	}
	
	/**
	 * Set: qtModalidadeFornecedor.
	 *
	 * @param qtModalidadeFornecedor the qt modalidade fornecedor
	 */
	public void setQtModalidadeFornecedor(Integer qtModalidadeFornecedor) {
		this.qtModalidadeFornecedor = qtModalidadeFornecedor;
	}
	
	/**
	 * Get: listModFornecedores.
	 *
	 * @return listModFornecedores
	 */
	public List<OcorrenciasModFornecedores> getListModFornecedores() {
		return listModFornecedores;
	}
	
	/**
	 * Set: listModFornecedores.
	 *
	 * @param listModFornecedores the list mod fornecedores
	 */
	public void setListModFornecedores(List<OcorrenciasModFornecedores> listModFornecedores) {
		this.listModFornecedores = listModFornecedores;
	}
	
	/**
	 * Get: cdServicoTributos.
	 *
	 * @return cdServicoTributos
	 */
	public String getCdServicoTributos() {
		return cdServicoTributos;
	}
	
	/**
	 * Set: cdServicoTributos.
	 *
	 * @param cdServicoTributos the cd servico tributos
	 */
	public void setCdServicoTributos(String cdServicoTributos) {
		this.cdServicoTributos = cdServicoTributos;
	}
	
	/**
	 * Get: qtModalidadeTributos.
	 *
	 * @return qtModalidadeTributos
	 */
	public Integer getQtModalidadeTributos() {
		return qtModalidadeTributos;
	}
	
	/**
	 * Set: qtModalidadeTributos.
	 *
	 * @param qtModalidadeTributos the qt modalidade tributos
	 */
	public void setQtModalidadeTributos(Integer qtModalidadeTributos) {
		this.qtModalidadeTributos = qtModalidadeTributos;
	}
	
	/**
	 * Get: listaModTributos.
	 *
	 * @return listaModTributos
	 */
	public List<OcorrenciasModTributos> getListaModTributos() {
		return listaModTributos;
	}
	
	/**
	 * Set: listaModTributos.
	 *
	 * @param listaModTributos the lista mod tributos
	 */
	public void setListaModTributos(List<OcorrenciasModTributos> listaModTributos) {
		this.listaModTributos = listaModTributos;
	}
	
	/**
	 * Get: cdServicoSalarial.
	 *
	 * @return cdServicoSalarial
	 */
	public String getCdServicoSalarial() {
		return cdServicoSalarial;
	}
	
	/**
	 * Set: cdServicoSalarial.
	 *
	 * @param cdServicoSalarial the cd servico salarial
	 */
	public void setCdServicoSalarial(String cdServicoSalarial) {
		this.cdServicoSalarial = cdServicoSalarial;
	}
	
	/**
	 * Get: qtModalidadeSalario.
	 *
	 * @return qtModalidadeSalario
	 */
	public Integer getQtModalidadeSalario() {
		return qtModalidadeSalario;
	}
	
	/**
	 * Set: qtModalidadeSalario.
	 *
	 * @param qtModalidadeSalario the qt modalidade salario
	 */
	public void setQtModalidadeSalario(Integer qtModalidadeSalario) {
		this.qtModalidadeSalario = qtModalidadeSalario;
	}
	
	/**
	 * Get: listaModSalario.
	 *
	 * @return listaModSalario
	 */
	public List<OcorrenciasModSalarial> getListaModSalario() {
		return listaModSalario;
	}
	
	/**
	 * Set: listaModSalario.
	 *
	 * @param listaModSalario the lista mod salario
	 */
	public void setListaModSalario(List<OcorrenciasModSalarial> listaModSalario) {
		this.listaModSalario = listaModSalario;
	}
	
	/**
	 * Get: cdServicoBeneficio.
	 *
	 * @return cdServicoBeneficio
	 */
	public String getCdServicoBeneficio() {
		return cdServicoBeneficio;
	}
	
	/**
	 * Set: cdServicoBeneficio.
	 *
	 * @param cdServicoBeneficio the cd servico beneficio
	 */
	public void setCdServicoBeneficio(String cdServicoBeneficio) {
		this.cdServicoBeneficio = cdServicoBeneficio;
	}
	
	/**
	 * Get: qtModalidadeBeneficio.
	 *
	 * @return qtModalidadeBeneficio
	 */
	public Integer getQtModalidadeBeneficio() {
		return qtModalidadeBeneficio;
	}
	
	/**
	 * Set: qtModalidadeBeneficio.
	 *
	 * @param qtModalidadeBeneficio the qt modalidade beneficio
	 */
	public void setQtModalidadeBeneficio(Integer qtModalidadeBeneficio) {
		this.qtModalidadeBeneficio = qtModalidadeBeneficio;
	}
	
	/**
	 * Get: listaModBeneficio.
	 *
	 * @return listaModBeneficio
	 */
	public List<OcorrenciasModBeneficios> getListaModBeneficio() {
		return listaModBeneficio;
	}
	
	/**
	 * Set: listaModBeneficio.
	 *
	 * @param listaModBeneficio the lista mod beneficio
	 */
	public void setListaModBeneficio(List<OcorrenciasModBeneficios> listaModBeneficio) {
		this.listaModBeneficio = listaModBeneficio;
	}
	
	/**
	 * Get: cdIndicadorAviso.
	 *
	 * @return cdIndicadorAviso
	 */
	public String getCdIndicadorAviso() {
		return cdIndicadorAviso;
	}
	
	/**
	 * Set: cdIndicadorAviso.
	 *
	 * @param cdIndicadorAviso the cd indicador aviso
	 */
	public void setCdIndicadorAviso(String cdIndicadorAviso) {
		this.cdIndicadorAviso = cdIndicadorAviso;
	}
	
	/**
	 * Get: cdIndicadorAvisoPagador.
	 *
	 * @return cdIndicadorAvisoPagador
	 */
	public String getCdIndicadorAvisoPagador() {
		return cdIndicadorAvisoPagador;
	}
	
	/**
	 * Set: cdIndicadorAvisoPagador.
	 *
	 * @param cdIndicadorAvisoPagador the cd indicador aviso pagador
	 */
	public void setCdIndicadorAvisoPagador(String cdIndicadorAvisoPagador) {
		this.cdIndicadorAvisoPagador = cdIndicadorAvisoPagador;
	}
	
	/**
	 * Get: cdIndicadorAvisoFavorecido.
	 *
	 * @return cdIndicadorAvisoFavorecido
	 */
	public String getCdIndicadorAvisoFavorecido() {
		return cdIndicadorAvisoFavorecido;
	}
	
	/**
	 * Set: cdIndicadorAvisoFavorecido.
	 *
	 * @param cdIndicadorAvisoFavorecido the cd indicador aviso favorecido
	 */
	public void setCdIndicadorAvisoFavorecido(String cdIndicadorAvisoFavorecido) {
		this.cdIndicadorAvisoFavorecido = cdIndicadorAvisoFavorecido;
	}
	
	/**
	 * Get: cdIndicadorComplemento.
	 *
	 * @return cdIndicadorComplemento
	 */
	public String getCdIndicadorComplemento() {
		return cdIndicadorComplemento;
	}
	
	/**
	 * Set: cdIndicadorComplemento.
	 *
	 * @param cdIndicadorComplemento the cd indicador complemento
	 */
	public void setCdIndicadorComplemento(String cdIndicadorComplemento) {
		this.cdIndicadorComplemento = cdIndicadorComplemento;
	}
	
	/**
	 * Get: cdIndicadorComplementoPagador.
	 *
	 * @return cdIndicadorComplementoPagador
	 */
	public String getCdIndicadorComplementoPagador() {
		return cdIndicadorComplementoPagador;
	}
	
	/**
	 * Set: cdIndicadorComplementoPagador.
	 *
	 * @param cdIndicadorComplementoPagador the cd indicador complemento pagador
	 */
	public void setCdIndicadorComplementoPagador(String cdIndicadorComplementoPagador) {
		this.cdIndicadorComplementoPagador = cdIndicadorComplementoPagador;
	}
	
	/**
	 * Get: cdIndicadorComplementoFavorecido.
	 *
	 * @return cdIndicadorComplementoFavorecido
	 */
	public String getCdIndicadorComplementoFavorecido() {
		return cdIndicadorComplementoFavorecido;
	}
	
	/**
	 * Set: cdIndicadorComplementoFavorecido.
	 *
	 * @param cdIndicadorComplementoFavorecido the cd indicador complemento favorecido
	 */
	public void setCdIndicadorComplementoFavorecido(String cdIndicadorComplementoFavorecido) {
		this.cdIndicadorComplementoFavorecido = cdIndicadorComplementoFavorecido;
	}
	
	/**
	 * Get: cdIndicadorComplementoSalarial.
	 *
	 * @return cdIndicadorComplementoSalarial
	 */
	public String getCdIndicadorComplementoSalarial() {
		return cdIndicadorComplementoSalarial;
	}
	
	/**
	 * Set: cdIndicadorComplementoSalarial.
	 *
	 * @param cdIndicadorComplementoSalarial the cd indicador complemento salarial
	 */
	public void setCdIndicadorComplementoSalarial(String cdIndicadorComplementoSalarial) {
		this.cdIndicadorComplementoSalarial = cdIndicadorComplementoSalarial;
	}
	
	/**
	 * Get: cdIndicadorComplementoDivergente.
	 *
	 * @return cdIndicadorComplementoDivergente
	 */
	public String getCdIndicadorComplementoDivergente() {
		return cdIndicadorComplementoDivergente;
	}
	
	/**
	 * Set: cdIndicadorComplementoDivergente.
	 *
	 * @param cdIndicadorComplementoDivergente the cd indicador complemento divergente
	 */
	public void setCdIndicadorComplementoDivergente(String cdIndicadorComplementoDivergente) {
		this.cdIndicadorComplementoDivergente = cdIndicadorComplementoDivergente;
	}
	
	/**
	 * Get: cdIndcadorCadastroFavorecido.
	 *
	 * @return cdIndcadorCadastroFavorecido
	 */
	public String getCdIndcadorCadastroFavorecido() {
		return cdIndcadorCadastroFavorecido;
	}
	
	/**
	 * Set: cdIndcadorCadastroFavorecido.
	 *
	 * @param cdIndcadorCadastroFavorecido the cd indcador cadastro favorecido
	 */
	public void setCdIndcadorCadastroFavorecido(String cdIndcadorCadastroFavorecido) {
		this.cdIndcadorCadastroFavorecido = cdIndcadorCadastroFavorecido;
	}
	
	/**
	 * Get: dsServicoRecadastro.
	 *
	 * @return dsServicoRecadastro
	 */
	public String getDsServicoRecadastro() {
		return dsServicoRecadastro;
	}
	
	/**
	 * Set: dsServicoRecadastro.
	 *
	 * @param dsServicoRecadastro the ds servico recadastro
	 */
	public void setDsServicoRecadastro(String dsServicoRecadastro) {
		this.dsServicoRecadastro = dsServicoRecadastro;
	}
	
	/**
	 * Get: qtModalidadeRecadastro.
	 *
	 * @return qtModalidadeRecadastro
	 */
	public Integer getQtModalidadeRecadastro() {
		return qtModalidadeRecadastro;
	}
	
	/**
	 * Set: qtModalidadeRecadastro.
	 *
	 * @param qtModalidadeRecadastro the qt modalidade recadastro
	 */
	public void setQtModalidadeRecadastro(Integer qtModalidadeRecadastro) {
		this.qtModalidadeRecadastro = qtModalidadeRecadastro;
	}
	
	/**
	 * Get: listaModRecadastro.
	 *
	 * @return listaModRecadastro
	 */
	public List<OcorrenciasModRecadastro> getListaModRecadastro() {
		return listaModRecadastro;
	}
	
	/**
	 * Set: listaModRecadastro.
	 *
	 * @param listaModRecadastro the lista mod recadastro
	 */
	public void setListaModRecadastro(List<OcorrenciasModRecadastro> listaModRecadastro) {
		this.listaModRecadastro = listaModRecadastro;
	}
	
	/**
	 * Get: dsSalarioEmissaoAntecipadoCartao.
	 *
	 * @return dsSalarioEmissaoAntecipadoCartao
	 */
	public String getDsSalarioEmissaoAntecipadoCartao() {
		return dsSalarioEmissaoAntecipadoCartao;
	}
	
	/**
	 * Set: dsSalarioEmissaoAntecipadoCartao.
	 *
	 * @param dsSalarioEmissaoAntecipadoCartao the ds salario emissao antecipado cartao
	 */
	public void setDsSalarioEmissaoAntecipadoCartao(String dsSalarioEmissaoAntecipadoCartao) {
		this.dsSalarioEmissaoAntecipadoCartao = dsSalarioEmissaoAntecipadoCartao;
	}
	
	/**
	 * Get: cdSalarioQuantidadeLimiteSolicitacaoCartao.
	 *
	 * @return cdSalarioQuantidadeLimiteSolicitacaoCartao
	 */
	public Integer getCdSalarioQuantidadeLimiteSolicitacaoCartao() {
		return cdSalarioQuantidadeLimiteSolicitacaoCartao;
	}
	
	/**
	 * Set: cdSalarioQuantidadeLimiteSolicitacaoCartao.
	 *
	 * @param cdSalarioQuantidadeLimiteSolicitacaoCartao the cd salario quantidade limite solicitacao cartao
	 */
	public void setCdSalarioQuantidadeLimiteSolicitacaoCartao(Integer cdSalarioQuantidadeLimiteSolicitacaoCartao) {
		this.cdSalarioQuantidadeLimiteSolicitacaoCartao = cdSalarioQuantidadeLimiteSolicitacaoCartao;
	}
	
	/**
	 * Get: dsSalarioAberturaContaExpressa.
	 *
	 * @return dsSalarioAberturaContaExpressa
	 */
	public String getDsSalarioAberturaContaExpressa() {
		return dsSalarioAberturaContaExpressa;
	}
	
	/**
	 * Set: dsSalarioAberturaContaExpressa.
	 *
	 * @param dsSalarioAberturaContaExpressa the ds salario abertura conta expressa
	 */
	public void setDsSalarioAberturaContaExpressa(String dsSalarioAberturaContaExpressa) {
		this.dsSalarioAberturaContaExpressa = dsSalarioAberturaContaExpressa;
	}
	
	/**
	 * Get: dsTibutoAgendadoDebitoVeicular.
	 *
	 * @return dsTibutoAgendadoDebitoVeicular
	 */
	public String getDsTibutoAgendadoDebitoVeicular() {
		return dsTibutoAgendadoDebitoVeicular;
	}
	
	/**
	 * Set: dsTibutoAgendadoDebitoVeicular.
	 *
	 * @param dsTibutoAgendadoDebitoVeicular the ds tibuto agendado debito veicular
	 */
	public void setDsTibutoAgendadoDebitoVeicular(String dsTibutoAgendadoDebitoVeicular) {
		this.dsTibutoAgendadoDebitoVeicular = dsTibutoAgendadoDebitoVeicular;
	}
	
	/**
	 * Get: dsBeneficioInformadoAgenciaConta.
	 *
	 * @return dsBeneficioInformadoAgenciaConta
	 */
	public String getDsBeneficioInformadoAgenciaConta() {
		return dsBeneficioInformadoAgenciaConta;
	}
	
	/**
	 * Set: dsBeneficioInformadoAgenciaConta.
	 *
	 * @param dsBeneficioInformadoAgenciaConta the ds beneficio informado agencia conta
	 */
	public void setDsBeneficioInformadoAgenciaConta(String dsBeneficioInformadoAgenciaConta) {
		this.dsBeneficioInformadoAgenciaConta = dsBeneficioInformadoAgenciaConta;
	}
	
	/**
	 * Get: dsBeneficioTipoIdentificacaoBeneficio.
	 *
	 * @return dsBeneficioTipoIdentificacaoBeneficio
	 */
	public String getDsBeneficioTipoIdentificacaoBeneficio() {
		return dsBeneficioTipoIdentificacaoBeneficio;
	}
	
	/**
	 * Set: dsBeneficioTipoIdentificacaoBeneficio.
	 *
	 * @param dsBeneficioTipoIdentificacaoBeneficio the ds beneficio tipo identificacao beneficio
	 */
	public void setDsBeneficioTipoIdentificacaoBeneficio(String dsBeneficioTipoIdentificacaoBeneficio) {
		this.dsBeneficioTipoIdentificacaoBeneficio = dsBeneficioTipoIdentificacaoBeneficio;
	}
	
	/**
	 * Get: dsBeneficioUtilizacaoCadastroOrganizacao.
	 *
	 * @return dsBeneficioUtilizacaoCadastroOrganizacao
	 */
	public String getDsBeneficioUtilizacaoCadastroOrganizacao() {
		return dsBeneficioUtilizacaoCadastroOrganizacao;
	}
	
	/**
	 * Set: dsBeneficioUtilizacaoCadastroOrganizacao.
	 *
	 * @param dsBeneficioUtilizacaoCadastroOrganizacao the ds beneficio utilizacao cadastro organizacao
	 */
	public void setDsBeneficioUtilizacaoCadastroOrganizacao(String dsBeneficioUtilizacaoCadastroOrganizacao) {
		this.dsBeneficioUtilizacaoCadastroOrganizacao = dsBeneficioUtilizacaoCadastroOrganizacao;
	}
	
	/**
	 * Get: dsBeneficioUtilizacaoCadastroProcuradores.
	 *
	 * @return dsBeneficioUtilizacaoCadastroProcuradores
	 */
	public String getDsBeneficioUtilizacaoCadastroProcuradores() {
		return dsBeneficioUtilizacaoCadastroProcuradores;
	}
	
	/**
	 * Set: dsBeneficioUtilizacaoCadastroProcuradores.
	 *
	 * @param dsBeneficioUtilizacaoCadastroProcuradores the ds beneficio utilizacao cadastro procuradores
	 */
	public void setDsBeneficioUtilizacaoCadastroProcuradores(String dsBeneficioUtilizacaoCadastroProcuradores) {
		this.dsBeneficioUtilizacaoCadastroProcuradores = dsBeneficioUtilizacaoCadastroProcuradores;
	}
	
	/**
	 * Get: dsBeneficioPossuiExpiracaoCredito.
	 *
	 * @return dsBeneficioPossuiExpiracaoCredito
	 */
	public String getDsBeneficioPossuiExpiracaoCredito() {
		return dsBeneficioPossuiExpiracaoCredito;
	}
	
	/**
	 * Set: dsBeneficioPossuiExpiracaoCredito.
	 *
	 * @param dsBeneficioPossuiExpiracaoCredito the ds beneficio possui expiracao credito
	 */
	public void setDsBeneficioPossuiExpiracaoCredito(String dsBeneficioPossuiExpiracaoCredito) {
		this.dsBeneficioPossuiExpiracaoCredito = dsBeneficioPossuiExpiracaoCredito;
	}
	
	/**
	 * Get: cdBeneficioQuantidadeDiasExpiracaoCredito.
	 *
	 * @return cdBeneficioQuantidadeDiasExpiracaoCredito
	 */
	public Integer getCdBeneficioQuantidadeDiasExpiracaoCredito() {
		return cdBeneficioQuantidadeDiasExpiracaoCredito;
	}
	
	/**
	 * Set: cdBeneficioQuantidadeDiasExpiracaoCredito.
	 *
	 * @param cdBeneficioQuantidadeDiasExpiracaoCredito the cd beneficio quantidade dias expiracao credito
	 */
	public void setCdBeneficioQuantidadeDiasExpiracaoCredito(Integer cdBeneficioQuantidadeDiasExpiracaoCredito) {
		this.cdBeneficioQuantidadeDiasExpiracaoCredito = cdBeneficioQuantidadeDiasExpiracaoCredito;
	}
	
	/**
	 * Get: cdServicoQuantidadeServicoPagamento.
	 *
	 * @return cdServicoQuantidadeServicoPagamento
	 */
	public Integer getCdServicoQuantidadeServicoPagamento() {
		return cdServicoQuantidadeServicoPagamento;
	}
	
	/**
	 * Set: cdServicoQuantidadeServicoPagamento.
	 *
	 * @param cdServicoQuantidadeServicoPagamento the cd servico quantidade servico pagamento
	 */
	public void setCdServicoQuantidadeServicoPagamento(Integer cdServicoQuantidadeServicoPagamento) {
		this.cdServicoQuantidadeServicoPagamento = cdServicoQuantidadeServicoPagamento;
	}
	
	/**
	 * Get: listaServicoPagamento.
	 *
	 * @return listaServicoPagamento
	 */
	public List<OcorrenciasServicoPagamentos> getListaServicoPagamento() {
		return listaServicoPagamento;
	}
	
	/**
	 * Set: listaServicoPagamento.
	 *
	 * @param listaServicoPagamento the lista servico pagamento
	 */
	public void setListaServicoPagamento(List<OcorrenciasServicoPagamentos> listaServicoPagamento) {
		this.listaServicoPagamento = listaServicoPagamento;
	}
	
	/**
	 * Get: qtAviso.
	 *
	 * @return qtAviso
	 */
	public Integer getQtAviso() {
		return qtAviso;
	}
	
	/**
	 * Set: qtAviso.
	 *
	 * @param qtAviso the qt aviso
	 */
	public void setQtAviso(Integer qtAviso) {
		this.qtAviso = qtAviso;
	}
	
	/**
	 * Get: listaAvisos.
	 *
	 * @return listaAvisos
	 */
	public List<OcorrenciasAvisos> getListaAvisos() {
		return listaAvisos;
	}
	
	/**
	 * Set: listaAvisos.
	 *
	 * @param listaAvisos the lista avisos
	 */
	public void setListaAvisos(List<OcorrenciasAvisos> listaAvisos) {
		this.listaAvisos = listaAvisos;
	}
	
	/**
	 * Get: qtComprovante.
	 *
	 * @return qtComprovante
	 */
	public Integer getQtComprovante() {
		return qtComprovante;
	}
	
	/**
	 * Set: qtComprovante.
	 *
	 * @param qtComprovante the qt comprovante
	 */
	public void setQtComprovante(Integer qtComprovante) {
		this.qtComprovante = qtComprovante;
	}
	
	/**
	 * Get: listaComprovantes.
	 *
	 * @return listaComprovantes
	 */
	public List<OcorrenciasComprovantes> getListaComprovantes() {
		return listaComprovantes;
	}
	
	/**
	 * Set: listaComprovantes.
	 *
	 * @param listaComprovantes the lista comprovantes
	 */
	public void setListaComprovantes(List<OcorrenciasComprovantes> listaComprovantes) {
		this.listaComprovantes = listaComprovantes;
	}
	
	/**
	 * Get: qtComprovanteSalarioDiversos.
	 *
	 * @return qtComprovanteSalarioDiversos
	 */
	public Integer getQtComprovanteSalarioDiversos() {
		return qtComprovanteSalarioDiversos;
	}
	
	/**
	 * Set: qtComprovanteSalarioDiversos.
	 *
	 * @param qtComprovanteSalarioDiversos the qt comprovante salario diversos
	 */
	public void setQtComprovanteSalarioDiversos(Integer qtComprovanteSalarioDiversos) {
		this.qtComprovanteSalarioDiversos = qtComprovanteSalarioDiversos;
	}
	
	/**
	 * Get: listaComprovanteSalarioDiversos.
	 *
	 * @return listaComprovanteSalarioDiversos
	 */
	public List<OcorrenciasComprovanteSalariosDiversos> getListaComprovanteSalarioDiversos() {
		return listaComprovanteSalarioDiversos;
	}
	
	/**
	 * Set: listaComprovanteSalarioDiversos.
	 *
	 * @param listaComprovanteSalarioDiversos the lista comprovante salario diversos
	 */
	public void setListaComprovanteSalarioDiversos(
					List<OcorrenciasComprovanteSalariosDiversos> listaComprovanteSalarioDiversos) {
		this.listaComprovanteSalarioDiversos = listaComprovanteSalarioDiversos;
	}
	
	/**
	 * Get: dsCadastroFormaManutencao.
	 *
	 * @return dsCadastroFormaManutencao
	 */
	public String getDsCadastroFormaManutencao() {
		return dsCadastroFormaManutencao;
	}
	
	/**
	 * Set: dsCadastroFormaManutencao.
	 *
	 * @param dsCadastroFormaManutencao the ds cadastro forma manutencao
	 */
	public void setDsCadastroFormaManutencao(String dsCadastroFormaManutencao) {
		this.dsCadastroFormaManutencao = dsCadastroFormaManutencao;
	}
	
	/**
	 * Get: cdCadastroQuantidadeDiasInativos.
	 *
	 * @return cdCadastroQuantidadeDiasInativos
	 */
	public Integer getCdCadastroQuantidadeDiasInativos() {
		return cdCadastroQuantidadeDiasInativos;
	}
	
	/**
	 * Set: cdCadastroQuantidadeDiasInativos.
	 *
	 * @param cdCadastroQuantidadeDiasInativos the cd cadastro quantidade dias inativos
	 */
	public void setCdCadastroQuantidadeDiasInativos(Integer cdCadastroQuantidadeDiasInativos) {
		this.cdCadastroQuantidadeDiasInativos = cdCadastroQuantidadeDiasInativos;
	}
	
	/**
	 * Get: dsCadastroTipoConsistenciaInscricao.
	 *
	 * @return dsCadastroTipoConsistenciaInscricao
	 */
	public String getDsCadastroTipoConsistenciaInscricao() {
		return dsCadastroTipoConsistenciaInscricao;
	}
	
	/**
	 * Set: dsCadastroTipoConsistenciaInscricao.
	 *
	 * @param dsCadastroTipoConsistenciaInscricao the ds cadastro tipo consistencia inscricao
	 */
	public void setDsCadastroTipoConsistenciaInscricao(String dsCadastroTipoConsistenciaInscricao) {
		this.dsCadastroTipoConsistenciaInscricao = dsCadastroTipoConsistenciaInscricao;
	}
	
	/**
	 * Get: dsRecTipoIdentificacaoBeneficio.
	 *
	 * @return dsRecTipoIdentificacaoBeneficio
	 */
	public String getDsRecTipoIdentificacaoBeneficio() {
		return dsRecTipoIdentificacaoBeneficio;
	}
	
	/**
	 * Set: dsRecTipoIdentificacaoBeneficio.
	 *
	 * @param dsRecTipoIdentificacaoBeneficio the ds rec tipo identificacao beneficio
	 */
	public void setDsRecTipoIdentificacaoBeneficio(String dsRecTipoIdentificacaoBeneficio) {
		this.dsRecTipoIdentificacaoBeneficio = dsRecTipoIdentificacaoBeneficio;
	}
	
	/**
	 * Get: dsRecTipoConsistenciaIdentificacao.
	 *
	 * @return dsRecTipoConsistenciaIdentificacao
	 */
	public String getDsRecTipoConsistenciaIdentificacao() {
		return dsRecTipoConsistenciaIdentificacao;
	}
	
	/**
	 * Set: dsRecTipoConsistenciaIdentificacao.
	 *
	 * @param dsRecTipoConsistenciaIdentificacao the ds rec tipo consistencia identificacao
	 */
	public void setDsRecTipoConsistenciaIdentificacao(String dsRecTipoConsistenciaIdentificacao) {
		this.dsRecTipoConsistenciaIdentificacao = dsRecTipoConsistenciaIdentificacao;
	}
	
	/**
	 * Get: dsRecCondicaoEnquadramento.
	 *
	 * @return dsRecCondicaoEnquadramento
	 */
	public String getDsRecCondicaoEnquadramento() {
		return dsRecCondicaoEnquadramento;
	}
	
	/**
	 * Set: dsRecCondicaoEnquadramento.
	 *
	 * @param dsRecCondicaoEnquadramento the ds rec condicao enquadramento
	 */
	public void setDsRecCondicaoEnquadramento(String dsRecCondicaoEnquadramento) {
		this.dsRecCondicaoEnquadramento = dsRecCondicaoEnquadramento;
	}
	
	/**
	 * Get: dsRecCriterioPrincipalEnquadramento.
	 *
	 * @return dsRecCriterioPrincipalEnquadramento
	 */
	public String getDsRecCriterioPrincipalEnquadramento() {
		return dsRecCriterioPrincipalEnquadramento;
	}
	
	/**
	 * Set: dsRecCriterioPrincipalEnquadramento.
	 *
	 * @param dsRecCriterioPrincipalEnquadramento the ds rec criterio principal enquadramento
	 */
	public void setDsRecCriterioPrincipalEnquadramento(String dsRecCriterioPrincipalEnquadramento) {
		this.dsRecCriterioPrincipalEnquadramento = dsRecCriterioPrincipalEnquadramento;
	}
	
	/**
	 * Get: dsRecTipoCriterioCompostoEnquadramento.
	 *
	 * @return dsRecTipoCriterioCompostoEnquadramento
	 */
	public String getDsRecTipoCriterioCompostoEnquadramento() {
		return dsRecTipoCriterioCompostoEnquadramento;
	}
	
	/**
	 * Set: dsRecTipoCriterioCompostoEnquadramento.
	 *
	 * @param dsRecTipoCriterioCompostoEnquadramento the ds rec tipo criterio composto enquadramento
	 */
	public void setDsRecTipoCriterioCompostoEnquadramento(String dsRecTipoCriterioCompostoEnquadramento) {
		this.dsRecTipoCriterioCompostoEnquadramento = dsRecTipoCriterioCompostoEnquadramento;
	}
	
	/**
	 * Get: qtRecEtapaRecadastramento.
	 *
	 * @return qtRecEtapaRecadastramento
	 */
	public Integer getQtRecEtapaRecadastramento() {
		return qtRecEtapaRecadastramento;
	}
	
	/**
	 * Set: qtRecEtapaRecadastramento.
	 *
	 * @param qtRecEtapaRecadastramento the qt rec etapa recadastramento
	 */
	public void setQtRecEtapaRecadastramento(Integer qtRecEtapaRecadastramento) {
		this.qtRecEtapaRecadastramento = qtRecEtapaRecadastramento;
	}
	
	/**
	 * Get: qtRecFaseEtapaRecadastramento.
	 *
	 * @return qtRecFaseEtapaRecadastramento
	 */
	public Integer getQtRecFaseEtapaRecadastramento() {
		return qtRecFaseEtapaRecadastramento;
	}
	
	/**
	 * Set: qtRecFaseEtapaRecadastramento.
	 *
	 * @param qtRecFaseEtapaRecadastramento the qt rec fase etapa recadastramento
	 */
	public void setQtRecFaseEtapaRecadastramento(Integer qtRecFaseEtapaRecadastramento) {
		this.qtRecFaseEtapaRecadastramento = qtRecFaseEtapaRecadastramento;
	}
	
	/**
	 * Get: dtRecInicioRecadastramento.
	 *
	 * @return dtRecInicioRecadastramento
	 */
	public String getDtRecInicioRecadastramento() {
		return dtRecInicioRecadastramento;
	}
	
	/**
	 * Set: dtRecInicioRecadastramento.
	 *
	 * @param dtRecInicioRecadastramento the dt rec inicio recadastramento
	 */
	public void setDtRecInicioRecadastramento(String dtRecInicioRecadastramento) {
		this.dtRecInicioRecadastramento = dtRecInicioRecadastramento;
	}
	
	/**
	 * Get: dtRecFinalRecadastramento.
	 *
	 * @return dtRecFinalRecadastramento
	 */
	public String getDtRecFinalRecadastramento() {
		return dtRecFinalRecadastramento;
	}
	
	/**
	 * Set: dtRecFinalRecadastramento.
	 *
	 * @param dtRecFinalRecadastramento the dt rec final recadastramento
	 */
	public void setDtRecFinalRecadastramento(String dtRecFinalRecadastramento) {
		this.dtRecFinalRecadastramento = dtRecFinalRecadastramento;
	}
	
	/**
	 * Get: dsRecGeradorRetornoInternet.
	 *
	 * @return dsRecGeradorRetornoInternet
	 */
	public String getDsRecGeradorRetornoInternet() {
		return dsRecGeradorRetornoInternet;
	}
	
	/**
	 * Set: dsRecGeradorRetornoInternet.
	 *
	 * @param dsRecGeradorRetornoInternet the ds rec gerador retorno internet
	 */
	public void setDsRecGeradorRetornoInternet(String dsRecGeradorRetornoInternet) {
		this.dsRecGeradorRetornoInternet = dsRecGeradorRetornoInternet;
	}
	
	/**
	 * Get: qtForTitulo.
	 *
	 * @return qtForTitulo
	 */
	public Integer getQtForTitulo() {
		return qtForTitulo;
	}
	
	/**
	 * Set: qtForTitulo.
	 *
	 * @param qtForTitulo the qt for titulo
	 */
	public void setQtForTitulo(Integer qtForTitulo) {
		this.qtForTitulo = qtForTitulo;
	}
	
	/**
	 * Get: listaTitulos.
	 *
	 * @return listaTitulos
	 */
	public List<OcorrenciasTitulos> getListaTitulos() {
		return listaTitulos;
	}
	
	/**
	 * Set: listaTitulos.
	 *
	 * @param listaTitulos the lista titulos
	 */
	public void setListaTitulos(List<OcorrenciasTitulos> listaTitulos) {
		this.listaTitulos = listaTitulos;
	}
	
	/**
	 * Get: qtCredito.
	 *
	 * @return qtCredito
	 */
	public Integer getQtCredito() {
		return qtCredito;
	}
	
	/**
	 * Set: qtCredito.
	 *
	 * @param qtCredito the qt credito
	 */
	public void setQtCredito(Integer qtCredito) {
		this.qtCredito = qtCredito;
	}
	
	/**
	 * Get: listaCreditos.
	 *
	 * @return listaCreditos
	 */
	public List<OcorrenciasCreditos> getListaCreditos() {
		return listaCreditos;
	}
	
	/**
	 * Set: listaCreditos.
	 *
	 * @param listaCreditos the lista creditos
	 */
	public void setListaCreditos(List<OcorrenciasCreditos> listaCreditos) {
		this.listaCreditos = listaCreditos;
	}
	
	/**
	 * Get: qtOperacao.
	 *
	 * @return qtOperacao
	 */
	public Integer getQtOperacao() {
		return qtOperacao;
	}
	
	/**
	 * Set: qtOperacao.
	 *
	 * @param qtOperacao the qt operacao
	 */
	public void setQtOperacao(Integer qtOperacao) {
		this.qtOperacao = qtOperacao;
	}
	
	/**
	 * Get: listaOperacoes.
	 *
	 * @return listaOperacoes
	 */
	public List<OcorrenciasOperacoes> getListaOperacoes() {
		return listaOperacoes;
	}
	
	/**
	 * Set: listaOperacoes.
	 *
	 * @param listaOperacoes the lista operacoes
	 */
	public void setListaOperacoes(List<OcorrenciasOperacoes> listaOperacoes) {
		this.listaOperacoes = listaOperacoes;
	}
	
	/**
	 * Get: qtModularidade.
	 *
	 * @return qtModularidade
	 */
	public Integer getQtModularidade() {
		return qtModularidade;
	}
	
	/**
	 * Set: qtModularidade.
	 *
	 * @param qtModularidade the qt modularidade
	 */
	public void setQtModularidade(Integer qtModularidade) {
		this.qtModularidade = qtModularidade;
	}
	
	/**
	 * Get: listaModularidades.
	 *
	 * @return listaModularidades
	 */
	public List<OcorrenciasModularidades> getListaModularidades() {
		return listaModularidades;
	}
	
	/**
	 * Set: listaModularidades.
	 *
	 * @param listaModularidades the lista modularidades
	 */
	public void setListaModularidades(List<OcorrenciasModularidades> listaModularidades) {
		this.listaModularidades = listaModularidades;
	}
	
	/**
	 * Get: dsFraTipoRastreabilidade.
	 *
	 * @return dsFraTipoRastreabilidade
	 */
	public String getDsFraTipoRastreabilidade() {
		return dsFraTipoRastreabilidade;
	}
	
	/**
	 * Set: dsFraTipoRastreabilidade.
	 *
	 * @param dsFraTipoRastreabilidade the ds fra tipo rastreabilidade
	 */
	public void setDsFraTipoRastreabilidade(String dsFraTipoRastreabilidade) {
		this.dsFraTipoRastreabilidade = dsFraTipoRastreabilidade;
	}
	
	/**
	 * Get: dsFraRastreabilidadeTerceiro.
	 *
	 * @return dsFraRastreabilidadeTerceiro
	 */
	public String getDsFraRastreabilidadeTerceiro() {
		return dsFraRastreabilidadeTerceiro;
	}
	
	/**
	 * Set: dsFraRastreabilidadeTerceiro.
	 *
	 * @param dsFraRastreabilidadeTerceiro the ds fra rastreabilidade terceiro
	 */
	public void setDsFraRastreabilidadeTerceiro(String dsFraRastreabilidadeTerceiro) {
		this.dsFraRastreabilidadeTerceiro = dsFraRastreabilidadeTerceiro;
	}
	
	/**
	 * Get: dsFraRastreabilidadeNota.
	 *
	 * @return dsFraRastreabilidadeNota
	 */
	public String getDsFraRastreabilidadeNota() {
		return dsFraRastreabilidadeNota;
	}
	
	/**
	 * Set: dsFraRastreabilidadeNota.
	 *
	 * @param dsFraRastreabilidadeNota the ds fra rastreabilidade nota
	 */
	public void setDsFraRastreabilidadeNota(String dsFraRastreabilidadeNota) {
		this.dsFraRastreabilidadeNota = dsFraRastreabilidadeNota;
	}
	
	/**
	 * Get: dsFraCaptacaoTitulo.
	 *
	 * @return dsFraCaptacaoTitulo
	 */
	public String getDsFraCaptacaoTitulo() {
		return dsFraCaptacaoTitulo;
	}
	
	/**
	 * Set: dsFraCaptacaoTitulo.
	 *
	 * @param dsFraCaptacaoTitulo the ds fra captacao titulo
	 */
	public void setDsFraCaptacaoTitulo(String dsFraCaptacaoTitulo) {
		this.dsFraCaptacaoTitulo = dsFraCaptacaoTitulo;
	}
	
	/**
	 * Get: dsFraAgendaCliente.
	 *
	 * @return dsFraAgendaCliente
	 */
	public String getDsFraAgendaCliente() {
		return dsFraAgendaCliente;
	}
	
	/**
	 * Set: dsFraAgendaCliente.
	 *
	 * @param dsFraAgendaCliente the ds fra agenda cliente
	 */
	public void setDsFraAgendaCliente(String dsFraAgendaCliente) {
		this.dsFraAgendaCliente = dsFraAgendaCliente;
	}
	
	/**
	 * Get: dsFraAgendaFilial.
	 *
	 * @return dsFraAgendaFilial
	 */
	public String getDsFraAgendaFilial() {
		return dsFraAgendaFilial;
	}
	
	/**
	 * Set: dsFraAgendaFilial.
	 *
	 * @param dsFraAgendaFilial the ds fra agenda filial
	 */
	public void setDsFraAgendaFilial(String dsFraAgendaFilial) {
		this.dsFraAgendaFilial = dsFraAgendaFilial;
	}
	
	/**
	 * Get: dsFraBloqueioEmissao.
	 *
	 * @return dsFraBloqueioEmissao
	 */
	public String getDsFraBloqueioEmissao() {
		return dsFraBloqueioEmissao;
	}
	
	/**
	 * Set: dsFraBloqueioEmissao.
	 *
	 * @param dsFraBloqueioEmissao the ds fra bloqueio emissao
	 */
	public void setDsFraBloqueioEmissao(String dsFraBloqueioEmissao) {
		this.dsFraBloqueioEmissao = dsFraBloqueioEmissao;
	}
	
	/**
	 * Get: dtFraInicioBloqueio.
	 *
	 * @return dtFraInicioBloqueio
	 */
	public String getDtFraInicioBloqueio() {
		return dtFraInicioBloqueio;
	}
	
	/**
	 * Set: dtFraInicioBloqueio.
	 *
	 * @param dtFraInicioBloqueio the dt fra inicio bloqueio
	 */
	public void setDtFraInicioBloqueio(String dtFraInicioBloqueio) {
		this.dtFraInicioBloqueio = dtFraInicioBloqueio;
	}
	
	/**
	 * Get: dtFraInicioRastreabilidade.
	 *
	 * @return dtFraInicioRastreabilidade
	 */
	public String getDtFraInicioRastreabilidade() {
		return dtFraInicioRastreabilidade;
	}
	
	/**
	 * Set: dtFraInicioRastreabilidade.
	 *
	 * @param dtFraInicioRastreabilidade the dt fra inicio rastreabilidade
	 */
	public void setDtFraInicioRastreabilidade(String dtFraInicioRastreabilidade) {
		this.dtFraInicioRastreabilidade = dtFraInicioRastreabilidade;
	}
	
	/**
	 * Get: dsFraAdesaoSacado.
	 *
	 * @return dsFraAdesaoSacado
	 */
	public String getDsFraAdesaoSacado() {
		return dsFraAdesaoSacado;
	}
	
	/**
	 * Set: dsFraAdesaoSacado.
	 *
	 * @param dsFraAdesaoSacado the ds fra adesao sacado
	 */
	public void setDsFraAdesaoSacado(String dsFraAdesaoSacado) {
		this.dsFraAdesaoSacado = dsFraAdesaoSacado;
	}
	
	/**
	 * Get: dsTdvTipoConsultaProposta.
	 *
	 * @return dsTdvTipoConsultaProposta
	 */
	public String getDsTdvTipoConsultaProposta() {
		return dsTdvTipoConsultaProposta;
	}
	
	/**
	 * Set: dsTdvTipoConsultaProposta.
	 *
	 * @param dsTdvTipoConsultaProposta the ds tdv tipo consulta proposta
	 */
	public void setDsTdvTipoConsultaProposta(String dsTdvTipoConsultaProposta) {
		this.dsTdvTipoConsultaProposta = dsTdvTipoConsultaProposta;
	}
	
	/**
	 * Get: dsTdvTipoTratamentoValor.
	 *
	 * @return dsTdvTipoTratamentoValor
	 */
	public String getDsTdvTipoTratamentoValor() {
		return dsTdvTipoTratamentoValor;
	}
	
	/**
	 * Set: dsTdvTipoTratamentoValor.
	 *
	 * @param dsTdvTipoTratamentoValor the ds tdv tipo tratamento valor
	 */
	public void setDsTdvTipoTratamentoValor(String dsTdvTipoTratamentoValor) {
		this.dsTdvTipoTratamentoValor = dsTdvTipoTratamentoValor;
	}
	
	/**
	 * Get: qtTriTributo.
	 *
	 * @return qtTriTributo
	 */
	public Integer getQtTriTributo() {
		return qtTriTributo;
	}
	
	/**
	 * Set: qtTriTributo.
	 *
	 * @param qtTriTributo the qt tri tributo
	 */
	public void setQtTriTributo(Integer qtTriTributo) {
		this.qtTriTributo = qtTriTributo;
	}
	
	/**
	 * Get: listaTriTributos.
	 *
	 * @return listaTriTributos
	 */
	public List<OcorrenciasTriTributo> getListaTriTributos() {
		return listaTriTributos;
	}
	
	/**
	 * Set: listaTriTributos.
	 *
	 * @param listaTriTributos the lista tri tributos
	 */
	public void setListaTriTributos(List<OcorrenciasTriTributo> listaTriTributos) {
		this.listaTriTributos = listaTriTributos;
	}
	
	/**
	 * Get: dsBprTipoAcao.
	 *
	 * @return dsBprTipoAcao
	 */
	public String getDsBprTipoAcao() {
		return dsBprTipoAcao;
	}
	
	/**
	 * Set: dsBprTipoAcao.
	 *
	 * @param dsBprTipoAcao the ds bpr tipo acao
	 */
	public void setDsBprTipoAcao(String dsBprTipoAcao) {
		this.dsBprTipoAcao = dsBprTipoAcao;
	}
	
	/**
	 * Get: qtBprMesProvisorio.
	 *
	 * @return qtBprMesProvisorio
	 */
	public Integer getQtBprMesProvisorio() {
		return qtBprMesProvisorio;
	}
	
	/**
	 * Set: qtBprMesProvisorio.
	 *
	 * @param qtBprMesProvisorio the qt bpr mes provisorio
	 */
	public void setQtBprMesProvisorio(Integer qtBprMesProvisorio) {
		this.qtBprMesProvisorio = qtBprMesProvisorio;
	}
	
	/**
	 * Get: cdBprIndicadorEmissaoAviso.
	 *
	 * @return cdBprIndicadorEmissaoAviso
	 */
	public String getCdBprIndicadorEmissaoAviso() {
		return cdBprIndicadorEmissaoAviso;
	}
	
	/**
	 * Set: cdBprIndicadorEmissaoAviso.
	 *
	 * @param cdBprIndicadorEmissaoAviso the cd bpr indicador emissao aviso
	 */
	public void setCdBprIndicadorEmissaoAviso(String cdBprIndicadorEmissaoAviso) {
		this.cdBprIndicadorEmissaoAviso = cdBprIndicadorEmissaoAviso;
	}
	
	/**
	 * Get: qtBprDiaAnteriorAviso.
	 *
	 * @return qtBprDiaAnteriorAviso
	 */
	public Integer getQtBprDiaAnteriorAviso() {
		return qtBprDiaAnteriorAviso;
	}
	
	/**
	 * Set: qtBprDiaAnteriorAviso.
	 *
	 * @param qtBprDiaAnteriorAviso the qt bpr dia anterior aviso
	 */
	public void setQtBprDiaAnteriorAviso(Integer qtBprDiaAnteriorAviso) {
		this.qtBprDiaAnteriorAviso = qtBprDiaAnteriorAviso;
	}
	
	/**
	 * Get: qtBprDiaAnteriorVencimento.
	 *
	 * @return qtBprDiaAnteriorVencimento
	 */
	public Integer getQtBprDiaAnteriorVencimento() {
		return qtBprDiaAnteriorVencimento;
	}
	
	/**
	 * Set: qtBprDiaAnteriorVencimento.
	 *
	 * @param qtBprDiaAnteriorVencimento the qt bpr dia anterior vencimento
	 */
	public void setQtBprDiaAnteriorVencimento(Integer qtBprDiaAnteriorVencimento) {
		this.qtBprDiaAnteriorVencimento = qtBprDiaAnteriorVencimento;
	}
	
	/**
	 * Get: cdBprUtilizadoMensagemPersonalizada.
	 *
	 * @return cdBprUtilizadoMensagemPersonalizada
	 */
	public String getCdBprUtilizadoMensagemPersonalizada() {
		return cdBprUtilizadoMensagemPersonalizada;
	}
	
	/**
	 * Set: cdBprUtilizadoMensagemPersonalizada.
	 *
	 * @param cdBprUtilizadoMensagemPersonalizada the cd bpr utilizado mensagem personalizada
	 */
	public void setCdBprUtilizadoMensagemPersonalizada(String cdBprUtilizadoMensagemPersonalizada) {
		this.cdBprUtilizadoMensagemPersonalizada = cdBprUtilizadoMensagemPersonalizada;
	}
	
	/**
	 * Get: dsBprDestino.
	 *
	 * @return dsBprDestino
	 */
	public String getDsBprDestino() {
		return dsBprDestino;
	}
	
	/**
	 * Set: dsBprDestino.
	 *
	 * @param dsBprDestino the ds bpr destino
	 */
	public void setDsBprDestino(String dsBprDestino) {
		this.dsBprDestino = dsBprDestino;
	}
	
	/**
	 * Get: dsBprCadastroEnderecoUtilizado.
	 *
	 * @return dsBprCadastroEnderecoUtilizado
	 */
	public String getDsBprCadastroEnderecoUtilizado() {
		return dsBprCadastroEnderecoUtilizado;
	}
	
	/**
	 * Set: dsBprCadastroEnderecoUtilizado.
	 *
	 * @param dsBprCadastroEnderecoUtilizado the ds bpr cadastro endereco utilizado
	 */
	public void setDsBprCadastroEnderecoUtilizado(String dsBprCadastroEnderecoUtilizado) {
		this.dsBprCadastroEnderecoUtilizado = dsBprCadastroEnderecoUtilizado;
	}
	
	/**
	 * Get: qtAvisoFormulario.
	 *
	 * @return qtAvisoFormulario
	 */
	public Integer getQtAvisoFormulario() {
		return qtAvisoFormulario;
	}
	
	/**
	 * Set: qtAvisoFormulario.
	 *
	 * @param qtAvisoFormulario the qt aviso formulario
	 */
	public void setQtAvisoFormulario(Integer qtAvisoFormulario) {
		this.qtAvisoFormulario = qtAvisoFormulario;
	}
	
	/**
	 * Get: listaAvisoFormularios.
	 *
	 * @return listaAvisoFormularios
	 */
	public List<OcorrenciasAvisoFormularios> getListaAvisoFormularios() {
		return listaAvisoFormularios;
	}
	
	/**
	 * Set: listaAvisoFormularios.
	 *
	 * @param listaAvisoFormularios the lista aviso formularios
	 */
	public void setListaAvisoFormularios(List<OcorrenciasAvisoFormularios> listaAvisoFormularios) {
		this.listaAvisoFormularios = listaAvisoFormularios;
	}

	public Integer getQtCestasTarifas() {
		return qtCestasTarifas;
	}

	public void setQtCestasTarifas(Integer qtCestasTarifas) {
		this.qtCestasTarifas = qtCestasTarifas;
	}

	public List<OcorrenciasSaidaDTO> getListaCestasTarifas() {
		return listaCestasTarifas;
	}

	public void setListaCestasTarifas(List<OcorrenciasSaidaDTO> listaCestasTarifas) {
		this.listaCestasTarifas = listaCestasTarifas;
	}
	
}
