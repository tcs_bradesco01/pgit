/**
 * Nome: br.com.bradesco.web.pgit.service.business.mantersolicitacaoexclusaolotepagamentosservice
 * Compilador: JDK 1.5
 * Prop�sito: INSERIR O PROP�SITO DAS CLASSES DO PACOTE
 * Data da cria��o: <dd/MM/yyyy>
 * Par�metros de compila��o: -d
 */
package br.com.bradesco.web.pgit.service.business.mantersolicitacaoexclusaolotepagamentosservice;

import br.com.bradesco.web.pgit.service.business.lotepagamentos.bean.ConsultarLotePagamentosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.lotepagamentos.bean.ConsultarLotePagamentosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.lotepagamentos.bean.DetalharLotePagamentosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.lotepagamentos.bean.DetalharLotePagamentosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.lotepagamentos.bean.ExcluirLotePagamentosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.lotepagamentos.bean.ExcluirLotePagamentosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.lotepagamentos.bean.IncluirLotePagamentosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.lotepagamentos.bean.IncluirLotePagamentosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicitacaoexclusaolotepagamentosservice.bean.ConsultarOcorrenciasSolicLoteEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicitacaoexclusaolotepagamentosservice.bean.ConsultarOcorrenciasSolicLoteSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicitacaoexclusaolotepagamentosservice.bean.ConsultarQtdeValorPgtoPrevistoSolicEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicitacaoexclusaolotepagamentosservice.bean.ConsultarQtdeValorPgtoPrevistoSolicSaidaDTO;

/**
 * Nome: IManterSolicitacaoExclusaoLotePagamentosServiceService
 * <p>
 * Prop�sito: Interface do adaptador
 * ManterSolicitacaoExclusaoLotePagamentosService
 * </p>
 * 
 * @author CPM Braxis / TI Melhorias - Arquitetura
 * @version 1.0
 */
public interface IManterSolicitacaoExclusaoLotePagamentosService {

	public ConsultarLotePagamentosSaidaDTO consultarSolicitacaoExclusaoLotePagamentos(
			ConsultarLotePagamentosEntradaDTO entrada);

	public DetalharLotePagamentosSaidaDTO detalharSolicitacaoExclusaoLotePagamentos(
			DetalharLotePagamentosEntradaDTO entrada);

	public ConsultarOcorrenciasSolicLoteSaidaDTO consultarOcorrenciasSolicLote(
			ConsultarOcorrenciasSolicLoteEntradaDTO entrada);

	public IncluirLotePagamentosSaidaDTO incluirSolicitacaoExclusaoLotePagamentos(
			IncluirLotePagamentosEntradaDTO entrada);

	public ExcluirLotePagamentosSaidaDTO excluirSolicitacaoExclusaoLotePagamentos(
			ExcluirLotePagamentosEntradaDTO entrada);

	public ConsultarQtdeValorPgtoPrevistoSolicSaidaDTO consultarQtdeValorPgtoPrevistoSolic(
			ConsultarQtdeValorPgtoPrevistoSolicEntradaDTO entrada);
}