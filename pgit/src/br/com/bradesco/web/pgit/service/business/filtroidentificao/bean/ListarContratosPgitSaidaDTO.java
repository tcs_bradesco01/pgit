/*
 * Nome: br.com.bradesco.web.pgit.service.business.filtroidentificao.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.filtroidentificao.bean;


/**
 * Nome: ListarContratosPgitSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarContratosPgitSaidaDTO {
	
	/** Atributo cdAditivo. */
	private String cdAditivo;
	
	/** Atributo cdAgenciaOperadora. */
	private Integer cdAgenciaOperadora;
	
	/** Atributo cdAtividadeEconomica. */
	private Integer cdAtividadeEconomica;
	
	/** Atributo cdClubRepresentante. */
	private Long cdClubRepresentante;
	
	/** Atributo cdControleCpfCnpjRepresentante. */
	private Integer cdControleCpfCnpjRepresentante;
	
	/** Atributo cdCpfCnpjRepresentante. */
	private Long cdCpfCnpjRepresentante;
	
	/** Atributo cdDigitoAgenciaOperadora. */
	private String cdDigitoAgenciaOperadora;
	
	/** Atributo cdFilialCpfCnpjRepresentante. */
	private Integer cdFilialCpfCnpjRepresentante;
	
	/** Atributo cdGrupoEconomico. */
	private Long cdGrupoEconomico;
	
	/** Atributo cdMotivoSituacao. */
	private Integer cdMotivoSituacao;
	
	/** Atributo cdPessoaJuridica. */
	private Long cdPessoaJuridica;
	
	/** Atributo cdSegmentoCliente. */
	private Integer cdSegmentoCliente;
	
	/** Atributo cdSituacaoContrato. */
	private Integer cdSituacaoContrato;
	
	/** Atributo cdSubSegmentoCliente. */
	private Integer cdSubSegmentoCliente;
	
	/** Atributo cdTipoContrato. */
	private Integer cdTipoContrato;
	
	/** Atributo cdTipoParticipacao. */
	private String cdTipoParticipacao;
	
	/** Atributo dsAgenciaOperadora. */
	private String dsAgenciaOperadora;
	
	/** Atributo dsGerenteResponsavel. */
	private String dsGerenteResponsavel;
	
	/** Atributo dsAtividadeEconomica. */
	private String dsAtividadeEconomica;
	
	/** Atributo dsContrato. */
	private String dsContrato;
	
	/** Atributo dsGrupoEconomico. */
	private String dsGrupoEconomico;
	
	/** Atributo dsMotivoSituacao. */
	private String dsMotivoSituacao;
	
	/** Atributo dsSegmentoCliente. */
	private String dsSegmentoCliente;
	
	/** Atributo dsSituacaoContrato. */
	private String dsSituacaoContrato;
	
	/** Atributo dsSubSegmentoCliente. */
	private String dsSubSegmentoCliente;
	
	/** Atributo dsTipoContrato. */
	private String dsTipoContrato;
	
	/** Atributo nmRazaoSocialRepresentante. */
	private String nmRazaoSocialRepresentante;
	
	/** Atributo nrSequenciaContrato. */
	private Long nrSequenciaContrato;
	
	/** Atributo cnpjOuCpfFormatado. */
	private String cnpjOuCpfFormatado;
	
	/** Atributo dsPessoaJuridica. */
	private String dsPessoaJuridica;
	
	/** Atributo codMensagem. */
	private String codMensagem;
	
	/** Atributo mensagem. */
	private String mensagem;
	
	/** Atributo cdFuncionarioBradesco. */
	private Long cdFuncionarioBradesco;
	
	/** Atributo nmFuncionarioBradesco. */
	private String nmFuncionarioBradesco;
	
	/** Atributo dtAbertura. */
	private String dtAbertura;
	
	/** Atributo dtEncerramento. */
	private String dtEncerramento;
	
	/** Atributo dtInclusao. */
	private String dtInclusao;
	
	/**
	 * Listar contratos pgit saida dto.
	 */
	public ListarContratosPgitSaidaDTO() {
		super();
	}

	/**
	 * Get: cdAditivo.
	 *
	 * @return cdAditivo
	 */
	public String getCdAditivo() {
		return cdAditivo;
	}
	
	/**
	 * Set: cdAditivo.
	 *
	 * @param cdAditivo the cd aditivo
	 */
	public void setCdAditivo(String cdAditivo) {
		this.cdAditivo = cdAditivo;
	}
	
	/**
	 * Get: cdAgenciaOperadora.
	 *
	 * @return cdAgenciaOperadora
	 */
	public Integer getCdAgenciaOperadora() {
		return cdAgenciaOperadora;
	}
	
	/**
	 * Set: cdAgenciaOperadora.
	 *
	 * @param cdAgenciaOperadora the cd agencia operadora
	 */
	public void setCdAgenciaOperadora(Integer cdAgenciaOperadora) {
		this.cdAgenciaOperadora = cdAgenciaOperadora;
	}
	
	/**
	 * Get: cdAtividadeEconomica.
	 *
	 * @return cdAtividadeEconomica
	 */
	public Integer getCdAtividadeEconomica() {
		return cdAtividadeEconomica;
	}
	
	/**
	 * Set: cdAtividadeEconomica.
	 *
	 * @param cdAtividadeEconomica the cd atividade economica
	 */
	public void setCdAtividadeEconomica(Integer cdAtividadeEconomica) {
		this.cdAtividadeEconomica = cdAtividadeEconomica;
	}
	
	/**
	 * Get: cdClubRepresentante.
	 *
	 * @return cdClubRepresentante
	 */
	public Long getCdClubRepresentante() {
		return cdClubRepresentante;
	}
	
	/**
	 * Set: cdClubRepresentante.
	 *
	 * @param cdClubRepresentante the cd club representante
	 */
	public void setCdClubRepresentante(Long cdClubRepresentante) {
		this.cdClubRepresentante = cdClubRepresentante;
	}
	
	/**
	 * Get: cdControleCpfCnpjRepresentante.
	 *
	 * @return cdControleCpfCnpjRepresentante
	 */
	public Integer getCdControleCpfCnpjRepresentante() {
		return cdControleCpfCnpjRepresentante;
	}
	
	/**
	 * Set: cdControleCpfCnpjRepresentante.
	 *
	 * @param cdControleCpfCnpjRepresentante the cd controle cpf cnpj representante
	 */
	public void setCdControleCpfCnpjRepresentante(
			Integer cdControleCpfCnpjRepresentante) {
		this.cdControleCpfCnpjRepresentante = cdControleCpfCnpjRepresentante;
	}
	
	/**
	 * Get: cdCpfCnpjRepresentante.
	 *
	 * @return cdCpfCnpjRepresentante
	 */
	public Long getCdCpfCnpjRepresentante() {
		return cdCpfCnpjRepresentante;
	}
	
	/**
	 * Set: cdCpfCnpjRepresentante.
	 *
	 * @param cdCpfCnpjRepresentante the cd cpf cnpj representante
	 */
	public void setCdCpfCnpjRepresentante(Long cdCpfCnpjRepresentante) {
		this.cdCpfCnpjRepresentante = cdCpfCnpjRepresentante;
	}
	
	/**
	 * Get: cdDigitoAgenciaOperadora.
	 *
	 * @return cdDigitoAgenciaOperadora
	 */
	public String getCdDigitoAgenciaOperadora() {
		return cdDigitoAgenciaOperadora;
	}
	
	/**
	 * Set: cdDigitoAgenciaOperadora.
	 *
	 * @param cdDigitoAgenciaOperadora the cd digito agencia operadora
	 */
	public void setCdDigitoAgenciaOperadora(String cdDigitoAgenciaOperadora) {
		this.cdDigitoAgenciaOperadora = cdDigitoAgenciaOperadora;
	}
	
	/**
	 * Get: cdFilialCpfCnpjRepresentante.
	 *
	 * @return cdFilialCpfCnpjRepresentante
	 */
	public Integer getCdFilialCpfCnpjRepresentante() {
		return cdFilialCpfCnpjRepresentante;
	}
	
	/**
	 * Set: cdFilialCpfCnpjRepresentante.
	 *
	 * @param cdFilialCpfCnpjRepresentante the cd filial cpf cnpj representante
	 */
	public void setCdFilialCpfCnpjRepresentante(Integer cdFilialCpfCnpjRepresentante) {
		this.cdFilialCpfCnpjRepresentante = cdFilialCpfCnpjRepresentante;
	}
	
	/**
	 * Get: cdGrupoEconomico.
	 *
	 * @return cdGrupoEconomico
	 */
	public Long getCdGrupoEconomico() {
		return cdGrupoEconomico;
	}
	
	/**
	 * Set: cdGrupoEconomico.
	 *
	 * @param cdGrupoEconomico the cd grupo economico
	 */
	public void setCdGrupoEconomico(Long cdGrupoEconomico) {
		this.cdGrupoEconomico = cdGrupoEconomico;
	}
	
	/**
	 * Get: cdMotivoSituacao.
	 *
	 * @return cdMotivoSituacao
	 */
	public Integer getCdMotivoSituacao() {
		return cdMotivoSituacao;
	}
	
	/**
	 * Set: cdMotivoSituacao.
	 *
	 * @param cdMotivoSituacao the cd motivo situacao
	 */
	public void setCdMotivoSituacao(Integer cdMotivoSituacao) {
		this.cdMotivoSituacao = cdMotivoSituacao;
	}
	
	/**
	 * Get: cdPessoaJuridica.
	 *
	 * @return cdPessoaJuridica
	 */
	public Long getCdPessoaJuridica() {
		return cdPessoaJuridica;
	}
	
	/**
	 * Set: cdPessoaJuridica.
	 *
	 * @param cdPessoaJuridica the cd pessoa juridica
	 */
	public void setCdPessoaJuridica(Long cdPessoaJuridica) {
		this.cdPessoaJuridica = cdPessoaJuridica;
	}
	
	/**
	 * Get: cdSegmentoCliente.
	 *
	 * @return cdSegmentoCliente
	 */
	public Integer getCdSegmentoCliente() {
		return cdSegmentoCliente;
	}
	
	/**
	 * Set: cdSegmentoCliente.
	 *
	 * @param cdSegmentoCliente the cd segmento cliente
	 */
	public void setCdSegmentoCliente(Integer cdSegmentoCliente) {
		this.cdSegmentoCliente = cdSegmentoCliente;
	}
	
	/**
	 * Get: cdSituacaoContrato.
	 *
	 * @return cdSituacaoContrato
	 */
	public Integer getCdSituacaoContrato() {
		return cdSituacaoContrato;
	}
	
	/**
	 * Set: cdSituacaoContrato.
	 *
	 * @param cdSituacaoContrato the cd situacao contrato
	 */
	public void setCdSituacaoContrato(Integer cdSituacaoContrato) {
		this.cdSituacaoContrato = cdSituacaoContrato;
	}
	
	/**
	 * Get: cdSubSegmentoCliente.
	 *
	 * @return cdSubSegmentoCliente
	 */
	public Integer getCdSubSegmentoCliente() {
		return cdSubSegmentoCliente;
	}
	
	/**
	 * Set: cdSubSegmentoCliente.
	 *
	 * @param cdSubSegmentoCliente the cd sub segmento cliente
	 */
	public void setCdSubSegmentoCliente(Integer cdSubSegmentoCliente) {
		this.cdSubSegmentoCliente = cdSubSegmentoCliente;
	}
	
	/**
	 * Get: cdTipoContrato.
	 *
	 * @return cdTipoContrato
	 */
	public Integer getCdTipoContrato() {
		return cdTipoContrato;
	}
	
	/**
	 * Set: cdTipoContrato.
	 *
	 * @param cdTipoContrato the cd tipo contrato
	 */
	public void setCdTipoContrato(Integer cdTipoContrato) {
		this.cdTipoContrato = cdTipoContrato;
	}
	
	/**
	 * Get: cdTipoParticipacao.
	 *
	 * @return cdTipoParticipacao
	 */
	public String getCdTipoParticipacao() {
		return cdTipoParticipacao;
	}
	
	/**
	 * Set: cdTipoParticipacao.
	 *
	 * @param cdTipoParticipacao the cd tipo participacao
	 */
	public void setCdTipoParticipacao(String cdTipoParticipacao) {
		this.cdTipoParticipacao = cdTipoParticipacao;
	}
	
	/**
	 * Get: dsAgenciaOperadora.
	 *
	 * @return dsAgenciaOperadora
	 */
	public String getDsAgenciaOperadora() {
		return dsAgenciaOperadora;
	}
	
	/**
	 * Set: dsAgenciaOperadora.
	 *
	 * @param dsAgenciaOperadora the ds agencia operadora
	 */
	public void setDsAgenciaOperadora(String dsAgenciaOperadora) {
		this.dsAgenciaOperadora = dsAgenciaOperadora;
	}
	
	/**
	 * Get: dsAtividadeEconomica.
	 *
	 * @return dsAtividadeEconomica
	 */
	public String getDsAtividadeEconomica() {
		return dsAtividadeEconomica;
	}
	
	/**
	 * Set: dsAtividadeEconomica.
	 *
	 * @param dsAtividadeEconomica the ds atividade economica
	 */
	public void setDsAtividadeEconomica(String dsAtividadeEconomica) {
		this.dsAtividadeEconomica = dsAtividadeEconomica;
	}
	
	/**
	 * Get: dsContrato.
	 *
	 * @return dsContrato
	 */
	public String getDsContrato() {
		return dsContrato;
	}
	
	/**
	 * Set: dsContrato.
	 *
	 * @param dsContrato the ds contrato
	 */
	public void setDsContrato(String dsContrato) {
		this.dsContrato = dsContrato;
	}
	
	/**
	 * Get: dsGrupoEconomico.
	 *
	 * @return dsGrupoEconomico
	 */
	public String getDsGrupoEconomico() {
		return dsGrupoEconomico;
	}
	
	/**
	 * Set: dsGrupoEconomico.
	 *
	 * @param dsGrupoEconomico the ds grupo economico
	 */
	public void setDsGrupoEconomico(String dsGrupoEconomico) {
		this.dsGrupoEconomico = dsGrupoEconomico;
	}
	
	/**
	 * Get: dsMotivoSituacao.
	 *
	 * @return dsMotivoSituacao
	 */
	public String getDsMotivoSituacao() {
		return dsMotivoSituacao;
	}
	
	/**
	 * Set: dsMotivoSituacao.
	 *
	 * @param dsMotivoSituacao the ds motivo situacao
	 */
	public void setDsMotivoSituacao(String dsMotivoSituacao) {
		this.dsMotivoSituacao = dsMotivoSituacao;
	}
	
	/**
	 * Get: dsSegmentoCliente.
	 *
	 * @return dsSegmentoCliente
	 */
	public String getDsSegmentoCliente() {
		return dsSegmentoCliente;
	}
	
	/**
	 * Set: dsSegmentoCliente.
	 *
	 * @param dsSegmentoCliente the ds segmento cliente
	 */
	public void setDsSegmentoCliente(String dsSegmentoCliente) {
		this.dsSegmentoCliente = dsSegmentoCliente;
	}
	
	/**
	 * Get: dsSituacaoContrato.
	 *
	 * @return dsSituacaoContrato
	 */
	public String getDsSituacaoContrato() {
		return dsSituacaoContrato;
	}
	
	/**
	 * Set: dsSituacaoContrato.
	 *
	 * @param dsSituacaoContrato the ds situacao contrato
	 */
	public void setDsSituacaoContrato(String dsSituacaoContrato) {
		this.dsSituacaoContrato = dsSituacaoContrato;
	}
	
	/**
	 * Get: dsSubSegmentoCliente.
	 *
	 * @return dsSubSegmentoCliente
	 */
	public String getDsSubSegmentoCliente() {
		return dsSubSegmentoCliente;
	}
	
	/**
	 * Set: dsSubSegmentoCliente.
	 *
	 * @param dsSubSegmentoCliente the ds sub segmento cliente
	 */
	public void setDsSubSegmentoCliente(String dsSubSegmentoCliente) {
		this.dsSubSegmentoCliente = dsSubSegmentoCliente;
	}
	
	/**
	 * Get: dsTipoContrato.
	 *
	 * @return dsTipoContrato
	 */
	public String getDsTipoContrato() {
		return dsTipoContrato;
	}
	
	/**
	 * Set: dsTipoContrato.
	 *
	 * @param dsTipoContrato the ds tipo contrato
	 */
	public void setDsTipoContrato(String dsTipoContrato) {
		this.dsTipoContrato = dsTipoContrato;
	}
	
	/**
	 * Get: nmRazaoSocialRepresentante.
	 *
	 * @return nmRazaoSocialRepresentante
	 */
	public String getNmRazaoSocialRepresentante() {
		return nmRazaoSocialRepresentante;
	}
	
	/**
	 * Set: nmRazaoSocialRepresentante.
	 *
	 * @param nmRazaoSocialRepresentante the nm razao social representante
	 */
	public void setNmRazaoSocialRepresentante(String nmRazaoSocialRepresentante) {
		this.nmRazaoSocialRepresentante = nmRazaoSocialRepresentante;
	}
	
	/**
	 * Get: nrSequenciaContrato.
	 *
	 * @return nrSequenciaContrato
	 */
	public Long getNrSequenciaContrato() {
		return nrSequenciaContrato;
	}
	
	/**
	 * Set: nrSequenciaContrato.
	 *
	 * @param nrSequenciaContrato the nr sequencia contrato
	 */
	public void setNrSequenciaContrato(Long nrSequenciaContrato) {
		this.nrSequenciaContrato = nrSequenciaContrato;
	}
	
	/**
	 * Get: cnpjOuCpfFormatado.
	 *
	 * @return cnpjOuCpfFormatado
	 */
	public String getCnpjOuCpfFormatado() {
		return cnpjOuCpfFormatado;
	}

	/**
	 * Set: cnpjOuCpfFormatado.
	 *
	 * @param cnpjOuCpfFormatado the cnpj ou cpf formatado
	 */
	public void setCnpjOuCpfFormatado(String cnpjOuCpfFormatado) {
		this.cnpjOuCpfFormatado = cnpjOuCpfFormatado;
	}

	/**
	 * Get: dsPessoaJuridica.
	 *
	 * @return dsPessoaJuridica
	 */
	public String getDsPessoaJuridica() {
		return dsPessoaJuridica;
	}

	/**
	 * Set: dsPessoaJuridica.
	 *
	 * @param dsPessoaJuridica the ds pessoa juridica
	 */
	public void setDsPessoaJuridica(String dsPessoaJuridica) {
		this.dsPessoaJuridica = dsPessoaJuridica;
	}

	/**
	 * Get: cdFuncionarioBradesco.
	 *
	 * @return cdFuncionarioBradesco
	 */
	public Long getCdFuncionarioBradesco() {
		return cdFuncionarioBradesco;
	}

	/**
	 * Set: cdFuncionarioBradesco.
	 *
	 * @param cdFuncionarioBradesco the cd funcionario bradesco
	 */
	public void setCdFuncionarioBradesco(Long cdFuncionarioBradesco) {
		this.cdFuncionarioBradesco = cdFuncionarioBradesco;
	}

	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}

	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}

	/**
	 * Get: dtAbertura.
	 *
	 * @return dtAbertura
	 */
	public String getDtAbertura() {
		return dtAbertura;
	}

	/**
	 * Set: dtAbertura.
	 *
	 * @param dtAbertura the dt abertura
	 */
	public void setDtAbertura(String dtAbertura) {
		this.dtAbertura = dtAbertura;
	}

	/**
	 * Get: dtEncerramento.
	 *
	 * @return dtEncerramento
	 */
	public String getDtEncerramento() {
		return dtEncerramento;
	}

	/**
	 * Set: dtEncerramento.
	 *
	 * @param dtEncerramento the dt encerramento
	 */
	public void setDtEncerramento(String dtEncerramento) {
		this.dtEncerramento = dtEncerramento;
	}

	/**
	 * Get: dtInclusao.
	 *
	 * @return dtInclusao
	 */
	public String getDtInclusao() {
		return dtInclusao;
	}

	/**
	 * Set: dtInclusao.
	 *
	 * @param dtInclusao the dt inclusao
	 */
	public void setDtInclusao(String dtInclusao) {
		this.dtInclusao = dtInclusao;
	}

	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}

	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	/**
	 * Get: nmFuncionarioBradesco.
	 *
	 * @return nmFuncionarioBradesco
	 */
	public String getNmFuncionarioBradesco() {
		return nmFuncionarioBradesco;
	}

	/**
	 * Set: nmFuncionarioBradesco.
	 *
	 * @param nmFuncionarioBradesco the nm funcionario bradesco
	 */
	public void setNmFuncionarioBradesco(String nmFuncionarioBradesco) {
		this.nmFuncionarioBradesco = nmFuncionarioBradesco;
	}
	
	/**
	 * Get: descFuncionarioBradesco.
	 *
	 * @return descFuncionarioBradesco
	 */
	public String getDescFuncionarioBradesco() {
		return nmFuncionarioBradesco;
	}

	/**
	 * Set: dsGerenteResponsavel.
	 *
	 * @param dsGerenteResponsavel the ds gerente responsavel
	 */
	public void setDsGerenteResponsavel(String dsGerenteResponsavel) {
		this.dsGerenteResponsavel = dsGerenteResponsavel;
	}

	/**
	 * Get: dsGerenteResponsavel.
	 *
	 * @return dsGerenteResponsavel
	 */
	public String getDsGerenteResponsavel() {
		return dsGerenteResponsavel;
	}
}