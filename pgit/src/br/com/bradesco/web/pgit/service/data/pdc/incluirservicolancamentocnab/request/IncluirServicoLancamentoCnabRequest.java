/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.incluirservicolancamentocnab.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class IncluirServicoLancamentoCnabRequest.
 * 
 * @version $Revision$ $Date$
 */
public class IncluirServicoLancamentoCnabRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdFormaLancamentoCnab
     */
    private int _cdFormaLancamentoCnab = 0;

    /**
     * keeps track of state for field: _cdFormaLancamentoCnab
     */
    private boolean _has_cdFormaLancamentoCnab;

    /**
     * Field _cdTipoServicoCnab
     */
    private int _cdTipoServicoCnab = 0;

    /**
     * keeps track of state for field: _cdTipoServicoCnab
     */
    private boolean _has_cdTipoServicoCnab;

    /**
     * Field _cdFormaLiquidacao
     */
    private int _cdFormaLiquidacao = 0;

    /**
     * keeps track of state for field: _cdFormaLiquidacao
     */
    private boolean _has_cdFormaLiquidacao;

    /**
     * Field _cdProdutoServicoOperacao
     */
    private int _cdProdutoServicoOperacao = 0;

    /**
     * keeps track of state for field: _cdProdutoServicoOperacao
     */
    private boolean _has_cdProdutoServicoOperacao;

    /**
     * Field _cdProdutoOperacaoRelacionado
     */
    private int _cdProdutoOperacaoRelacionado = 0;

    /**
     * keeps track of state for field: _cdProdutoOperacaoRelacionado
     */
    private boolean _has_cdProdutoOperacaoRelacionado;


      //----------------/
     //- Constructors -/
    //----------------/

    public IncluirServicoLancamentoCnabRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.incluirservicolancamentocnab.request.IncluirServicoLancamentoCnabRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdFormaLancamentoCnab
     * 
     */
    public void deleteCdFormaLancamentoCnab()
    {
        this._has_cdFormaLancamentoCnab= false;
    } //-- void deleteCdFormaLancamentoCnab() 

    /**
     * Method deleteCdFormaLiquidacao
     * 
     */
    public void deleteCdFormaLiquidacao()
    {
        this._has_cdFormaLiquidacao= false;
    } //-- void deleteCdFormaLiquidacao() 

    /**
     * Method deleteCdProdutoOperacaoRelacionado
     * 
     */
    public void deleteCdProdutoOperacaoRelacionado()
    {
        this._has_cdProdutoOperacaoRelacionado= false;
    } //-- void deleteCdProdutoOperacaoRelacionado() 

    /**
     * Method deleteCdProdutoServicoOperacao
     * 
     */
    public void deleteCdProdutoServicoOperacao()
    {
        this._has_cdProdutoServicoOperacao= false;
    } //-- void deleteCdProdutoServicoOperacao() 

    /**
     * Method deleteCdTipoServicoCnab
     * 
     */
    public void deleteCdTipoServicoCnab()
    {
        this._has_cdTipoServicoCnab= false;
    } //-- void deleteCdTipoServicoCnab() 

    /**
     * Returns the value of field 'cdFormaLancamentoCnab'.
     * 
     * @return int
     * @return the value of field 'cdFormaLancamentoCnab'.
     */
    public int getCdFormaLancamentoCnab()
    {
        return this._cdFormaLancamentoCnab;
    } //-- int getCdFormaLancamentoCnab() 

    /**
     * Returns the value of field 'cdFormaLiquidacao'.
     * 
     * @return int
     * @return the value of field 'cdFormaLiquidacao'.
     */
    public int getCdFormaLiquidacao()
    {
        return this._cdFormaLiquidacao;
    } //-- int getCdFormaLiquidacao() 

    /**
     * Returns the value of field 'cdProdutoOperacaoRelacionado'.
     * 
     * @return int
     * @return the value of field 'cdProdutoOperacaoRelacionado'.
     */
    public int getCdProdutoOperacaoRelacionado()
    {
        return this._cdProdutoOperacaoRelacionado;
    } //-- int getCdProdutoOperacaoRelacionado() 

    /**
     * Returns the value of field 'cdProdutoServicoOperacao'.
     * 
     * @return int
     * @return the value of field 'cdProdutoServicoOperacao'.
     */
    public int getCdProdutoServicoOperacao()
    {
        return this._cdProdutoServicoOperacao;
    } //-- int getCdProdutoServicoOperacao() 

    /**
     * Returns the value of field 'cdTipoServicoCnab'.
     * 
     * @return int
     * @return the value of field 'cdTipoServicoCnab'.
     */
    public int getCdTipoServicoCnab()
    {
        return this._cdTipoServicoCnab;
    } //-- int getCdTipoServicoCnab() 

    /**
     * Method hasCdFormaLancamentoCnab
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFormaLancamentoCnab()
    {
        return this._has_cdFormaLancamentoCnab;
    } //-- boolean hasCdFormaLancamentoCnab() 

    /**
     * Method hasCdFormaLiquidacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFormaLiquidacao()
    {
        return this._has_cdFormaLiquidacao;
    } //-- boolean hasCdFormaLiquidacao() 

    /**
     * Method hasCdProdutoOperacaoRelacionado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdutoOperacaoRelacionado()
    {
        return this._has_cdProdutoOperacaoRelacionado;
    } //-- boolean hasCdProdutoOperacaoRelacionado() 

    /**
     * Method hasCdProdutoServicoOperacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdutoServicoOperacao()
    {
        return this._has_cdProdutoServicoOperacao;
    } //-- boolean hasCdProdutoServicoOperacao() 

    /**
     * Method hasCdTipoServicoCnab
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoServicoCnab()
    {
        return this._has_cdTipoServicoCnab;
    } //-- boolean hasCdTipoServicoCnab() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdFormaLancamentoCnab'.
     * 
     * @param cdFormaLancamentoCnab the value of field
     * 'cdFormaLancamentoCnab'.
     */
    public void setCdFormaLancamentoCnab(int cdFormaLancamentoCnab)
    {
        this._cdFormaLancamentoCnab = cdFormaLancamentoCnab;
        this._has_cdFormaLancamentoCnab = true;
    } //-- void setCdFormaLancamentoCnab(int) 

    /**
     * Sets the value of field 'cdFormaLiquidacao'.
     * 
     * @param cdFormaLiquidacao the value of field
     * 'cdFormaLiquidacao'.
     */
    public void setCdFormaLiquidacao(int cdFormaLiquidacao)
    {
        this._cdFormaLiquidacao = cdFormaLiquidacao;
        this._has_cdFormaLiquidacao = true;
    } //-- void setCdFormaLiquidacao(int) 

    /**
     * Sets the value of field 'cdProdutoOperacaoRelacionado'.
     * 
     * @param cdProdutoOperacaoRelacionado the value of field
     * 'cdProdutoOperacaoRelacionado'.
     */
    public void setCdProdutoOperacaoRelacionado(int cdProdutoOperacaoRelacionado)
    {
        this._cdProdutoOperacaoRelacionado = cdProdutoOperacaoRelacionado;
        this._has_cdProdutoOperacaoRelacionado = true;
    } //-- void setCdProdutoOperacaoRelacionado(int) 

    /**
     * Sets the value of field 'cdProdutoServicoOperacao'.
     * 
     * @param cdProdutoServicoOperacao the value of field
     * 'cdProdutoServicoOperacao'.
     */
    public void setCdProdutoServicoOperacao(int cdProdutoServicoOperacao)
    {
        this._cdProdutoServicoOperacao = cdProdutoServicoOperacao;
        this._has_cdProdutoServicoOperacao = true;
    } //-- void setCdProdutoServicoOperacao(int) 

    /**
     * Sets the value of field 'cdTipoServicoCnab'.
     * 
     * @param cdTipoServicoCnab the value of field
     * 'cdTipoServicoCnab'.
     */
    public void setCdTipoServicoCnab(int cdTipoServicoCnab)
    {
        this._cdTipoServicoCnab = cdTipoServicoCnab;
        this._has_cdTipoServicoCnab = true;
    } //-- void setCdTipoServicoCnab(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return IncluirServicoLancamentoCnabRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.incluirservicolancamentocnab.request.IncluirServicoLancamentoCnabRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.incluirservicolancamentocnab.request.IncluirServicoLancamentoCnabRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.incluirservicolancamentocnab.request.IncluirServicoLancamentoCnabRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.incluirservicolancamentocnab.request.IncluirServicoLancamentoCnabRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
