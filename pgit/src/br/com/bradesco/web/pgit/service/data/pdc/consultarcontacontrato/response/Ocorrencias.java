/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultarcontacontrato.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdFinalidadeContaPagamento
     */
    private int _cdFinalidadeContaPagamento = 0;

    /**
     * keeps track of state for field: _cdFinalidadeContaPagamento
     */
    private boolean _has_cdFinalidadeContaPagamento;

    /**
     * Field _dsFinalidadeContaPagamento
     */
    private java.lang.String _dsFinalidadeContaPagamento;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarcontacontrato.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdFinalidadeContaPagamento
     * 
     */
    public void deleteCdFinalidadeContaPagamento()
    {
        this._has_cdFinalidadeContaPagamento= false;
    } //-- void deleteCdFinalidadeContaPagamento() 

    /**
     * Returns the value of field 'cdFinalidadeContaPagamento'.
     * 
     * @return int
     * @return the value of field 'cdFinalidadeContaPagamento'.
     */
    public int getCdFinalidadeContaPagamento()
    {
        return this._cdFinalidadeContaPagamento;
    } //-- int getCdFinalidadeContaPagamento() 

    /**
     * Returns the value of field 'dsFinalidadeContaPagamento'.
     * 
     * @return String
     * @return the value of field 'dsFinalidadeContaPagamento'.
     */
    public java.lang.String getDsFinalidadeContaPagamento()
    {
        return this._dsFinalidadeContaPagamento;
    } //-- java.lang.String getDsFinalidadeContaPagamento() 

    /**
     * Method hasCdFinalidadeContaPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFinalidadeContaPagamento()
    {
        return this._has_cdFinalidadeContaPagamento;
    } //-- boolean hasCdFinalidadeContaPagamento() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdFinalidadeContaPagamento'.
     * 
     * @param cdFinalidadeContaPagamento the value of field
     * 'cdFinalidadeContaPagamento'.
     */
    public void setCdFinalidadeContaPagamento(int cdFinalidadeContaPagamento)
    {
        this._cdFinalidadeContaPagamento = cdFinalidadeContaPagamento;
        this._has_cdFinalidadeContaPagamento = true;
    } //-- void setCdFinalidadeContaPagamento(int) 

    /**
     * Sets the value of field 'dsFinalidadeContaPagamento'.
     * 
     * @param dsFinalidadeContaPagamento the value of field
     * 'dsFinalidadeContaPagamento'.
     */
    public void setDsFinalidadeContaPagamento(java.lang.String dsFinalidadeContaPagamento)
    {
        this._dsFinalidadeContaPagamento = dsFinalidadeContaPagamento;
    } //-- void setDsFinalidadeContaPagamento(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultarcontacontrato.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultarcontacontrato.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultarcontacontrato.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarcontacontrato.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
