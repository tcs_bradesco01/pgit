/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listarcontacomplementar.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdContaComplementar
     */
    private int _cdContaComplementar = 0;

    /**
     * keeps track of state for field: _cdContaComplementar
     */
    private boolean _has_cdContaComplementar;

    /**
     * Field _cdCorpoCnpjCpf
     */
    private long _cdCorpoCnpjCpf = 0;

    /**
     * keeps track of state for field: _cdCorpoCnpjCpf
     */
    private boolean _has_cdCorpoCnpjCpf;

    /**
     * Field _cdFilialCpfCnpj
     */
    private int _cdFilialCpfCnpj = 0;

    /**
     * keeps track of state for field: _cdFilialCpfCnpj
     */
    private boolean _has_cdFilialCpfCnpj;

    /**
     * Field _cdCtrlCpfCnpj
     */
    private int _cdCtrlCpfCnpj = 0;

    /**
     * keeps track of state for field: _cdCtrlCpfCnpj
     */
    private boolean _has_cdCtrlCpfCnpj;

    /**
     * Field _dsNomeRazaoSocial
     */
    private java.lang.String _dsNomeRazaoSocial;

    /**
     * Field _cdPessoaJuridicaContratoConta
     */
    private long _cdPessoaJuridicaContratoConta = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridicaContratoCont
     */
    private boolean _has_cdPessoaJuridicaContratoConta;

    /**
     * Field _cdTipoContratoConta
     */
    private int _cdTipoContratoConta = 0;

    /**
     * keeps track of state for field: _cdTipoContratoConta
     */
    private boolean _has_cdTipoContratoConta;

    /**
     * Field _nrSequenciaContratoConta
     */
    private long _nrSequenciaContratoConta = 0;

    /**
     * keeps track of state for field: _nrSequenciaContratoConta
     */
    private boolean _has_nrSequenciaContratoConta;

    /**
     * Field _cdBanco
     */
    private int _cdBanco = 0;

    /**
     * keeps track of state for field: _cdBanco
     */
    private boolean _has_cdBanco;

    /**
     * Field _dsBanco
     */
    private java.lang.String _dsBanco;

    /**
     * Field _cdAgencia
     */
    private int _cdAgencia = 0;

    /**
     * keeps track of state for field: _cdAgencia
     */
    private boolean _has_cdAgencia;

    /**
     * Field _dsAgencia
     */
    private java.lang.String _dsAgencia;

    /**
     * Field _cdConta
     */
    private long _cdConta = 0;

    /**
     * keeps track of state for field: _cdConta
     */
    private boolean _has_cdConta;

    /**
     * Field _cdDigitoConta
     */
    private java.lang.String _cdDigitoConta;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarcontacomplementar.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdAgencia
     * 
     */
    public void deleteCdAgencia()
    {
        this._has_cdAgencia= false;
    } //-- void deleteCdAgencia() 

    /**
     * Method deleteCdBanco
     * 
     */
    public void deleteCdBanco()
    {
        this._has_cdBanco= false;
    } //-- void deleteCdBanco() 

    /**
     * Method deleteCdConta
     * 
     */
    public void deleteCdConta()
    {
        this._has_cdConta= false;
    } //-- void deleteCdConta() 

    /**
     * Method deleteCdContaComplementar
     * 
     */
    public void deleteCdContaComplementar()
    {
        this._has_cdContaComplementar= false;
    } //-- void deleteCdContaComplementar() 

    /**
     * Method deleteCdCorpoCnpjCpf
     * 
     */
    public void deleteCdCorpoCnpjCpf()
    {
        this._has_cdCorpoCnpjCpf= false;
    } //-- void deleteCdCorpoCnpjCpf() 

    /**
     * Method deleteCdCtrlCpfCnpj
     * 
     */
    public void deleteCdCtrlCpfCnpj()
    {
        this._has_cdCtrlCpfCnpj= false;
    } //-- void deleteCdCtrlCpfCnpj() 

    /**
     * Method deleteCdFilialCpfCnpj
     * 
     */
    public void deleteCdFilialCpfCnpj()
    {
        this._has_cdFilialCpfCnpj= false;
    } //-- void deleteCdFilialCpfCnpj() 

    /**
     * Method deleteCdPessoaJuridicaContratoConta
     * 
     */
    public void deleteCdPessoaJuridicaContratoConta()
    {
        this._has_cdPessoaJuridicaContratoConta= false;
    } //-- void deleteCdPessoaJuridicaContratoConta() 

    /**
     * Method deleteCdTipoContratoConta
     * 
     */
    public void deleteCdTipoContratoConta()
    {
        this._has_cdTipoContratoConta= false;
    } //-- void deleteCdTipoContratoConta() 

    /**
     * Method deleteNrSequenciaContratoConta
     * 
     */
    public void deleteNrSequenciaContratoConta()
    {
        this._has_nrSequenciaContratoConta= false;
    } //-- void deleteNrSequenciaContratoConta() 

    /**
     * Returns the value of field 'cdAgencia'.
     * 
     * @return int
     * @return the value of field 'cdAgencia'.
     */
    public int getCdAgencia()
    {
        return this._cdAgencia;
    } //-- int getCdAgencia() 

    /**
     * Returns the value of field 'cdBanco'.
     * 
     * @return int
     * @return the value of field 'cdBanco'.
     */
    public int getCdBanco()
    {
        return this._cdBanco;
    } //-- int getCdBanco() 

    /**
     * Returns the value of field 'cdConta'.
     * 
     * @return long
     * @return the value of field 'cdConta'.
     */
    public long getCdConta()
    {
        return this._cdConta;
    } //-- long getCdConta() 

    /**
     * Returns the value of field 'cdContaComplementar'.
     * 
     * @return int
     * @return the value of field 'cdContaComplementar'.
     */
    public int getCdContaComplementar()
    {
        return this._cdContaComplementar;
    } //-- int getCdContaComplementar() 

    /**
     * Returns the value of field 'cdCorpoCnpjCpf'.
     * 
     * @return long
     * @return the value of field 'cdCorpoCnpjCpf'.
     */
    public long getCdCorpoCnpjCpf()
    {
        return this._cdCorpoCnpjCpf;
    } //-- long getCdCorpoCnpjCpf() 

    /**
     * Returns the value of field 'cdCtrlCpfCnpj'.
     * 
     * @return int
     * @return the value of field 'cdCtrlCpfCnpj'.
     */
    public int getCdCtrlCpfCnpj()
    {
        return this._cdCtrlCpfCnpj;
    } //-- int getCdCtrlCpfCnpj() 

    /**
     * Returns the value of field 'cdDigitoConta'.
     * 
     * @return String
     * @return the value of field 'cdDigitoConta'.
     */
    public java.lang.String getCdDigitoConta()
    {
        return this._cdDigitoConta;
    } //-- java.lang.String getCdDigitoConta() 

    /**
     * Returns the value of field 'cdFilialCpfCnpj'.
     * 
     * @return int
     * @return the value of field 'cdFilialCpfCnpj'.
     */
    public int getCdFilialCpfCnpj()
    {
        return this._cdFilialCpfCnpj;
    } //-- int getCdFilialCpfCnpj() 

    /**
     * Returns the value of field 'cdPessoaJuridicaContratoConta'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridicaContratoConta'.
     */
    public long getCdPessoaJuridicaContratoConta()
    {
        return this._cdPessoaJuridicaContratoConta;
    } //-- long getCdPessoaJuridicaContratoConta() 

    /**
     * Returns the value of field 'cdTipoContratoConta'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoConta'.
     */
    public int getCdTipoContratoConta()
    {
        return this._cdTipoContratoConta;
    } //-- int getCdTipoContratoConta() 

    /**
     * Returns the value of field 'dsAgencia'.
     * 
     * @return String
     * @return the value of field 'dsAgencia'.
     */
    public java.lang.String getDsAgencia()
    {
        return this._dsAgencia;
    } //-- java.lang.String getDsAgencia() 

    /**
     * Returns the value of field 'dsBanco'.
     * 
     * @return String
     * @return the value of field 'dsBanco'.
     */
    public java.lang.String getDsBanco()
    {
        return this._dsBanco;
    } //-- java.lang.String getDsBanco() 

    /**
     * Returns the value of field 'dsNomeRazaoSocial'.
     * 
     * @return String
     * @return the value of field 'dsNomeRazaoSocial'.
     */
    public java.lang.String getDsNomeRazaoSocial()
    {
        return this._dsNomeRazaoSocial;
    } //-- java.lang.String getDsNomeRazaoSocial() 

    /**
     * Returns the value of field 'nrSequenciaContratoConta'.
     * 
     * @return long
     * @return the value of field 'nrSequenciaContratoConta'.
     */
    public long getNrSequenciaContratoConta()
    {
        return this._nrSequenciaContratoConta;
    } //-- long getNrSequenciaContratoConta() 

    /**
     * Method hasCdAgencia
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgencia()
    {
        return this._has_cdAgencia;
    } //-- boolean hasCdAgencia() 

    /**
     * Method hasCdBanco
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdBanco()
    {
        return this._has_cdBanco;
    } //-- boolean hasCdBanco() 

    /**
     * Method hasCdConta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdConta()
    {
        return this._has_cdConta;
    } //-- boolean hasCdConta() 

    /**
     * Method hasCdContaComplementar
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdContaComplementar()
    {
        return this._has_cdContaComplementar;
    } //-- boolean hasCdContaComplementar() 

    /**
     * Method hasCdCorpoCnpjCpf
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCorpoCnpjCpf()
    {
        return this._has_cdCorpoCnpjCpf;
    } //-- boolean hasCdCorpoCnpjCpf() 

    /**
     * Method hasCdCtrlCpfCnpj
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCtrlCpfCnpj()
    {
        return this._has_cdCtrlCpfCnpj;
    } //-- boolean hasCdCtrlCpfCnpj() 

    /**
     * Method hasCdFilialCpfCnpj
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFilialCpfCnpj()
    {
        return this._has_cdFilialCpfCnpj;
    } //-- boolean hasCdFilialCpfCnpj() 

    /**
     * Method hasCdPessoaJuridicaContratoConta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridicaContratoConta()
    {
        return this._has_cdPessoaJuridicaContratoConta;
    } //-- boolean hasCdPessoaJuridicaContratoConta() 

    /**
     * Method hasCdTipoContratoConta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoConta()
    {
        return this._has_cdTipoContratoConta;
    } //-- boolean hasCdTipoContratoConta() 

    /**
     * Method hasNrSequenciaContratoConta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaContratoConta()
    {
        return this._has_nrSequenciaContratoConta;
    } //-- boolean hasNrSequenciaContratoConta() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdAgencia'.
     * 
     * @param cdAgencia the value of field 'cdAgencia'.
     */
    public void setCdAgencia(int cdAgencia)
    {
        this._cdAgencia = cdAgencia;
        this._has_cdAgencia = true;
    } //-- void setCdAgencia(int) 

    /**
     * Sets the value of field 'cdBanco'.
     * 
     * @param cdBanco the value of field 'cdBanco'.
     */
    public void setCdBanco(int cdBanco)
    {
        this._cdBanco = cdBanco;
        this._has_cdBanco = true;
    } //-- void setCdBanco(int) 

    /**
     * Sets the value of field 'cdConta'.
     * 
     * @param cdConta the value of field 'cdConta'.
     */
    public void setCdConta(long cdConta)
    {
        this._cdConta = cdConta;
        this._has_cdConta = true;
    } //-- void setCdConta(long) 

    /**
     * Sets the value of field 'cdContaComplementar'.
     * 
     * @param cdContaComplementar the value of field
     * 'cdContaComplementar'.
     */
    public void setCdContaComplementar(int cdContaComplementar)
    {
        this._cdContaComplementar = cdContaComplementar;
        this._has_cdContaComplementar = true;
    } //-- void setCdContaComplementar(int) 

    /**
     * Sets the value of field 'cdCorpoCnpjCpf'.
     * 
     * @param cdCorpoCnpjCpf the value of field 'cdCorpoCnpjCpf'.
     */
    public void setCdCorpoCnpjCpf(long cdCorpoCnpjCpf)
    {
        this._cdCorpoCnpjCpf = cdCorpoCnpjCpf;
        this._has_cdCorpoCnpjCpf = true;
    } //-- void setCdCorpoCnpjCpf(long) 

    /**
     * Sets the value of field 'cdCtrlCpfCnpj'.
     * 
     * @param cdCtrlCpfCnpj the value of field 'cdCtrlCpfCnpj'.
     */
    public void setCdCtrlCpfCnpj(int cdCtrlCpfCnpj)
    {
        this._cdCtrlCpfCnpj = cdCtrlCpfCnpj;
        this._has_cdCtrlCpfCnpj = true;
    } //-- void setCdCtrlCpfCnpj(int) 

    /**
     * Sets the value of field 'cdDigitoConta'.
     * 
     * @param cdDigitoConta the value of field 'cdDigitoConta'.
     */
    public void setCdDigitoConta(java.lang.String cdDigitoConta)
    {
        this._cdDigitoConta = cdDigitoConta;
    } //-- void setCdDigitoConta(java.lang.String) 

    /**
     * Sets the value of field 'cdFilialCpfCnpj'.
     * 
     * @param cdFilialCpfCnpj the value of field 'cdFilialCpfCnpj'.
     */
    public void setCdFilialCpfCnpj(int cdFilialCpfCnpj)
    {
        this._cdFilialCpfCnpj = cdFilialCpfCnpj;
        this._has_cdFilialCpfCnpj = true;
    } //-- void setCdFilialCpfCnpj(int) 

    /**
     * Sets the value of field 'cdPessoaJuridicaContratoConta'.
     * 
     * @param cdPessoaJuridicaContratoConta the value of field
     * 'cdPessoaJuridicaContratoConta'.
     */
    public void setCdPessoaJuridicaContratoConta(long cdPessoaJuridicaContratoConta)
    {
        this._cdPessoaJuridicaContratoConta = cdPessoaJuridicaContratoConta;
        this._has_cdPessoaJuridicaContratoConta = true;
    } //-- void setCdPessoaJuridicaContratoConta(long) 

    /**
     * Sets the value of field 'cdTipoContratoConta'.
     * 
     * @param cdTipoContratoConta the value of field
     * 'cdTipoContratoConta'.
     */
    public void setCdTipoContratoConta(int cdTipoContratoConta)
    {
        this._cdTipoContratoConta = cdTipoContratoConta;
        this._has_cdTipoContratoConta = true;
    } //-- void setCdTipoContratoConta(int) 

    /**
     * Sets the value of field 'dsAgencia'.
     * 
     * @param dsAgencia the value of field 'dsAgencia'.
     */
    public void setDsAgencia(java.lang.String dsAgencia)
    {
        this._dsAgencia = dsAgencia;
    } //-- void setDsAgencia(java.lang.String) 

    /**
     * Sets the value of field 'dsBanco'.
     * 
     * @param dsBanco the value of field 'dsBanco'.
     */
    public void setDsBanco(java.lang.String dsBanco)
    {
        this._dsBanco = dsBanco;
    } //-- void setDsBanco(java.lang.String) 

    /**
     * Sets the value of field 'dsNomeRazaoSocial'.
     * 
     * @param dsNomeRazaoSocial the value of field
     * 'dsNomeRazaoSocial'.
     */
    public void setDsNomeRazaoSocial(java.lang.String dsNomeRazaoSocial)
    {
        this._dsNomeRazaoSocial = dsNomeRazaoSocial;
    } //-- void setDsNomeRazaoSocial(java.lang.String) 

    /**
     * Sets the value of field 'nrSequenciaContratoConta'.
     * 
     * @param nrSequenciaContratoConta the value of field
     * 'nrSequenciaContratoConta'.
     */
    public void setNrSequenciaContratoConta(long nrSequenciaContratoConta)
    {
        this._nrSequenciaContratoConta = nrSequenciaContratoConta;
        this._has_nrSequenciaContratoConta = true;
    } //-- void setNrSequenciaContratoConta(long) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listarcontacomplementar.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listarcontacomplementar.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listarcontacomplementar.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarcontacomplementar.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
