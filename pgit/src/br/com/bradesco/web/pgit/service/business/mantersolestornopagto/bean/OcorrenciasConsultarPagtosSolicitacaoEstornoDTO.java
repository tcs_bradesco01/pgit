/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantersolestornopagto.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantersolestornopagto.bean;

import java.math.BigDecimal;

import br.com.bradesco.web.pgit.utils.CpfCnpjUtils;

/**
 * Nome: OcorrenciasConsultarPagtosSolicitacaoEstornoDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class OcorrenciasConsultarPagtosSolicitacaoEstornoDTO {
	
	/** Atributo cdCorpoCpfCnpj. */
	private Long cdCorpoCpfCnpj;
	
	/** Atributo cdFilialCpfCnpj. */
	private Integer cdFilialCpfCnpj;
	
	/** Atributo cdDigitoCpfCnpj. */
	private Integer cdDigitoCpfCnpj;
	
	/** Atributo cdPessoaJuridicaContrato. */
	private Long cdPessoaJuridicaContrato;
	
	/** Atributo dsRazaoSocial. */
	private String dsRazaoSocial;
	
	/** Atributo cdTipoContratoNegocio. */
	private Integer cdTipoContratoNegocio;
	
	/** Atributo nrSequenciaContratoNegocio. */
	private Long nrSequenciaContratoNegocio;
	
	/** Atributo cdprodutoServicoOperacao. */
	private Integer cdprodutoServicoOperacao;
	
	/** Atributo dsOperacaoProdutoServico. */
	private String dsOperacaoProdutoServico;
	
	/** Atributo cdProdutoServicoRelacionado. */
	private Integer cdProdutoServicoRelacionado;
	
	/** Atributo dsOperacaoProdutoRelacionado. */
	private String dsOperacaoProdutoRelacionado;
	
	/** Atributo cdControlePagamento. */
	private String cdControlePagamento;
	
	/** Atributo dtCredito. */
	private String dtCredito;
	
	/** Atributo vlrEfetivacaoPagamento. */
	private BigDecimal vlrEfetivacaoPagamento;
	
	/** Atributo cdInscricaoFavorecido. */
	private Long cdInscricaoFavorecido;
	
	/** Atributo dsFavorecidoCliente. */
	private String dsFavorecidoCliente;
	
	/** Atributo cdBancoDebito. */
	private Integer cdBancoDebito;
	
	/** Atributo cdAgenciaDebito. */
	private Integer cdAgenciaDebito;
	
	/** Atributo cdDigitoAgenciaDebito. */
	private Integer cdDigitoAgenciaDebito;
	
	/** Atributo cdContaDebito. */
	private Long cdContaDebito;
	
	/** Atributo cdDigitoContaDebito. */
	private String cdDigitoContaDebito;
	
	/** Atributo bancoCredito. */
	private Integer bancoCredito;
	
	/** Atributo agenciaCredito. */
	private Integer agenciaCredito;
	
	/** Atributo digitoAgenciaCredito. */
	private Integer digitoAgenciaCredito;
	
	/** Atributo contaCredito. */
	private Long contaCredito;
	
	/** Atributo digitoContaCredito. */
	private String digitoContaCredito;
	
	/** Atributo cdSituacaoOperacaoPagamento. */
	private Integer cdSituacaoOperacaoPagamento;
	
	/** Atributo dsSituacaoOperacaoPagamento. */
	private String dsSituacaoOperacaoPagamento;
	
	/** Atributo dtEfetivacao. */
	private String dtEfetivacao;
	
	/** Atributo cpfCnpjRecebedor. */
	private Long cpfCnpjRecebedor;
	
	/** Atributo filialCpfCnpjRecebedor. */
	private Integer filialCpfCnpjRecebedor;
	
	/** Atributo digitoCpfCnpjRecebedor. */
	private Integer digitoCpfCnpjRecebedor;
	
	/** Atributo cdTipoTela. */
	private Integer cdTipoTela;
	
	/** Atributo cdAgendadosPagosNaoPagos. */
	private Integer cdAgendadosPagosNaoPagos;
	
	/** Atributo dsMotivoRecusaEstorno. */
	private String dsMotivoRecusaEstorno;

	/** Atributo bancoAgenciaContaDebitoFormatado. */
	private String bancoAgenciaContaDebitoFormatado;
	
	/** Atributo bancoAgenciaContaCreditoFormatado. */
	private String bancoAgenciaContaCreditoFormatado;
	
	/** Atributo agenciaCreditoFormatada. */
	private String agenciaCreditoFormatada;
	
	/** Atributo contaCreditoFormatada. */
	private String contaCreditoFormatada;
	
	/** Atributo favorecidoBeneficiarioFormatado. */
	private String favorecidoBeneficiarioFormatado;
	
	/** Atributo cpfCnpjFormatado. */
	private String cpfCnpjFormatado;
	
	/** Atributo bancoAgenciaContaFavorecidoFormatada. */
	private String bancoAgenciaContaFavorecidoFormatada;

	/**
	 * Get: cdCorpoCpfCnpj.
	 *
	 * @return cdCorpoCpfCnpj
	 */
	public Long getCdCorpoCpfCnpj() {
		return cdCorpoCpfCnpj;
	}

	/**
	 * Set: cdCorpoCpfCnpj.
	 *
	 * @param cdCorpoCpfCnpj the cd corpo cpf cnpj
	 */
	public void setCdCorpoCpfCnpj(Long cdCorpoCpfCnpj) {
		this.cdCorpoCpfCnpj = cdCorpoCpfCnpj;
	}

	/**
	 * Get: cdFilialCpfCnpj.
	 *
	 * @return cdFilialCpfCnpj
	 */
	public Integer getCdFilialCpfCnpj() {
		return cdFilialCpfCnpj;
	}

	/**
	 * Set: cdFilialCpfCnpj.
	 *
	 * @param cdFilialCpfCnpj the cd filial cpf cnpj
	 */
	public void setCdFilialCpfCnpj(Integer cdFilialCpfCnpj) {
		this.cdFilialCpfCnpj = cdFilialCpfCnpj;
	}

	/**
	 * Get: cdDigitoCpfCnpj.
	 *
	 * @return cdDigitoCpfCnpj
	 */
	public Integer getCdDigitoCpfCnpj() {
		return cdDigitoCpfCnpj;
	}

	/**
	 * Set: cdDigitoCpfCnpj.
	 *
	 * @param cdDigitoCpfCnpj the cd digito cpf cnpj
	 */
	public void setCdDigitoCpfCnpj(Integer cdDigitoCpfCnpj) {
		this.cdDigitoCpfCnpj = cdDigitoCpfCnpj;
	}

	/**
	 * Get: cdPessoaJuridicaContrato.
	 *
	 * @return cdPessoaJuridicaContrato
	 */
	public Long getCdPessoaJuridicaContrato() {
		return cdPessoaJuridicaContrato;
	}

	/**
	 * Set: cdPessoaJuridicaContrato.
	 *
	 * @param cdPessoaJuridicaContrato the cd pessoa juridica contrato
	 */
	public void setCdPessoaJuridicaContrato(Long cdPessoaJuridicaContrato) {
		this.cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
	}

	/**
	 * Get: dsRazaoSocial.
	 *
	 * @return dsRazaoSocial
	 */
	public String getDsRazaoSocial() {
		return dsRazaoSocial;
	}

	/**
	 * Set: dsRazaoSocial.
	 *
	 * @param dsRazaoSocial the ds razao social
	 */
	public void setDsRazaoSocial(String dsRazaoSocial) {
		this.dsRazaoSocial = dsRazaoSocial;
	}

	/**
	 * Get: cdTipoContratoNegocio.
	 *
	 * @return cdTipoContratoNegocio
	 */
	public Integer getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}

	/**
	 * Set: cdTipoContratoNegocio.
	 *
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}

	/**
	 * Get: nrSequenciaContratoNegocio.
	 *
	 * @return nrSequenciaContratoNegocio
	 */
	public Long getNrSequenciaContratoNegocio() {
		return nrSequenciaContratoNegocio;
	}

	/**
	 * Set: nrSequenciaContratoNegocio.
	 *
	 * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
	 */
	public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}

	/**
	 * Get: cdprodutoServicoOperacao.
	 *
	 * @return cdprodutoServicoOperacao
	 */
	public Integer getCdprodutoServicoOperacao() {
		return cdprodutoServicoOperacao;
	}

	/**
	 * Set: cdprodutoServicoOperacao.
	 *
	 * @param cdprodutoServicoOperacao the cdproduto servico operacao
	 */
	public void setCdprodutoServicoOperacao(Integer cdprodutoServicoOperacao) {
		this.cdprodutoServicoOperacao = cdprodutoServicoOperacao;
	}

	/**
	 * Get: dsOperacaoProdutoServico.
	 *
	 * @return dsOperacaoProdutoServico
	 */
	public String getDsOperacaoProdutoServico() {
		return dsOperacaoProdutoServico;
	}

	/**
	 * Set: dsOperacaoProdutoServico.
	 *
	 * @param dsOperacaoProdutoServico the ds operacao produto servico
	 */
	public void setDsOperacaoProdutoServico(String dsOperacaoProdutoServico) {
		this.dsOperacaoProdutoServico = dsOperacaoProdutoServico;
	}

	/**
	 * Get: cdProdutoServicoRelacionado.
	 *
	 * @return cdProdutoServicoRelacionado
	 */
	public Integer getCdProdutoServicoRelacionado() {
		return cdProdutoServicoRelacionado;
	}

	/**
	 * Set: cdProdutoServicoRelacionado.
	 *
	 * @param cdProdutoServicoRelacionado the cd produto servico relacionado
	 */
	public void setCdProdutoServicoRelacionado(
			Integer cdProdutoServicoRelacionado) {
		this.cdProdutoServicoRelacionado = cdProdutoServicoRelacionado;
	}

	/**
	 * Get: dsOperacaoProdutoRelacionado.
	 *
	 * @return dsOperacaoProdutoRelacionado
	 */
	public String getDsOperacaoProdutoRelacionado() {
		return dsOperacaoProdutoRelacionado;
	}

	/**
	 * Set: dsOperacaoProdutoRelacionado.
	 *
	 * @param dsOperacaoProdutoRelacionado the ds operacao produto relacionado
	 */
	public void setDsOperacaoProdutoRelacionado(
			String dsOperacaoProdutoRelacionado) {
		this.dsOperacaoProdutoRelacionado = dsOperacaoProdutoRelacionado;
	}

	/**
	 * Get: cdControlePagamento.
	 *
	 * @return cdControlePagamento
	 */
	public String getCdControlePagamento() {
		return cdControlePagamento;
	}

	/**
	 * Set: cdControlePagamento.
	 *
	 * @param cdControlePagamento the cd controle pagamento
	 */
	public void setCdControlePagamento(String cdControlePagamento) {
		this.cdControlePagamento = cdControlePagamento;
	}

	/**
	 * Get: dtCredito.
	 *
	 * @return dtCredito
	 */
	public String getDtCredito() {
		return dtCredito;
	}

	/**
	 * Set: dtCredito.
	 *
	 * @param dtCredito the dt credito
	 */
	public void setDtCredito(String dtCredito) {
		this.dtCredito = dtCredito;
	}

	/**
	 * Get: vlrEfetivacaoPagamento.
	 *
	 * @return vlrEfetivacaoPagamento
	 */
	public BigDecimal getVlrEfetivacaoPagamento() {
		return vlrEfetivacaoPagamento;
	}

	/**
	 * Set: vlrEfetivacaoPagamento.
	 *
	 * @param vlrEfetivacaoPagamento the vlr efetivacao pagamento
	 */
	public void setVlrEfetivacaoPagamento(BigDecimal vlrEfetivacaoPagamento) {
		this.vlrEfetivacaoPagamento = vlrEfetivacaoPagamento;
	}

	/**
	 * Get: cdInscricaoFavorecido.
	 *
	 * @return cdInscricaoFavorecido
	 */
	public Long getCdInscricaoFavorecido() {
		return cdInscricaoFavorecido;
	}

	/**
	 * Set: cdInscricaoFavorecido.
	 *
	 * @param cdInscricaoFavorecido the cd inscricao favorecido
	 */
	public void setCdInscricaoFavorecido(Long cdInscricaoFavorecido) {
		this.cdInscricaoFavorecido = cdInscricaoFavorecido;
	}

	/**
	 * Get: dsFavorecidoCliente.
	 *
	 * @return dsFavorecidoCliente
	 */
	public String getDsFavorecidoCliente() {
		return dsFavorecidoCliente;
	}

	/**
	 * Set: dsFavorecidoCliente.
	 *
	 * @param dsFavorecidoCliente the ds favorecido cliente
	 */
	public void setDsFavorecidoCliente(String dsFavorecidoCliente) {
		this.dsFavorecidoCliente = dsFavorecidoCliente;
	}

	/**
	 * Get: cdBancoDebito.
	 *
	 * @return cdBancoDebito
	 */
	public Integer getCdBancoDebito() {
		return cdBancoDebito;
	}

	/**
	 * Set: cdBancoDebito.
	 *
	 * @param cdBancoDebito the cd banco debito
	 */
	public void setCdBancoDebito(Integer cdBancoDebito) {
		this.cdBancoDebito = cdBancoDebito;
	}

	/**
	 * Get: cdAgenciaDebito.
	 *
	 * @return cdAgenciaDebito
	 */
	public Integer getCdAgenciaDebito() {
		return cdAgenciaDebito;
	}

	/**
	 * Set: cdAgenciaDebito.
	 *
	 * @param cdAgenciaDebito the cd agencia debito
	 */
	public void setCdAgenciaDebito(Integer cdAgenciaDebito) {
		this.cdAgenciaDebito = cdAgenciaDebito;
	}

	/**
	 * Get: cdDigitoAgenciaDebito.
	 *
	 * @return cdDigitoAgenciaDebito
	 */
	public Integer getCdDigitoAgenciaDebito() {
		return cdDigitoAgenciaDebito;
	}

	/**
	 * Set: cdDigitoAgenciaDebito.
	 *
	 * @param cdDigitoAgenciaDebito the cd digito agencia debito
	 */
	public void setCdDigitoAgenciaDebito(Integer cdDigitoAgenciaDebito) {
		this.cdDigitoAgenciaDebito = cdDigitoAgenciaDebito;
	}

	/**
	 * Get: cdContaDebito.
	 *
	 * @return cdContaDebito
	 */
	public Long getCdContaDebito() {
		return cdContaDebito;
	}

	/**
	 * Set: cdContaDebito.
	 *
	 * @param cdContaDebito the cd conta debito
	 */
	public void setCdContaDebito(Long cdContaDebito) {
		this.cdContaDebito = cdContaDebito;
	}

	/**
	 * Get: cdDigitoContaDebito.
	 *
	 * @return cdDigitoContaDebito
	 */
	public String getCdDigitoContaDebito() {
		return cdDigitoContaDebito;
	}

	/**
	 * Set: cdDigitoContaDebito.
	 *
	 * @param cdDigitoContaDebito the cd digito conta debito
	 */
	public void setCdDigitoContaDebito(String cdDigitoContaDebito) {
		this.cdDigitoContaDebito = cdDigitoContaDebito;
	}

	/**
	 * Get: bancoCredito.
	 *
	 * @return bancoCredito
	 */
	public Integer getBancoCredito() {
		return bancoCredito;
	}

	/**
	 * Set: bancoCredito.
	 *
	 * @param bancoCredito the banco credito
	 */
	public void setBancoCredito(Integer bancoCredito) {
		this.bancoCredito = bancoCredito;
	}

	/**
	 * Get: agenciaCredito.
	 *
	 * @return agenciaCredito
	 */
	public Integer getAgenciaCredito() {
		return agenciaCredito;
	}

	/**
	 * Set: agenciaCredito.
	 *
	 * @param agenciaCredito the agencia credito
	 */
	public void setAgenciaCredito(Integer agenciaCredito) {
		this.agenciaCredito = agenciaCredito;
	}

	/**
	 * Get: digitoAgenciaCredito.
	 *
	 * @return digitoAgenciaCredito
	 */
	public Integer getDigitoAgenciaCredito() {
		return digitoAgenciaCredito;
	}

	/**
	 * Set: digitoAgenciaCredito.
	 *
	 * @param digitoAgenciaCredito the digito agencia credito
	 */
	public void setDigitoAgenciaCredito(Integer digitoAgenciaCredito) {
		this.digitoAgenciaCredito = digitoAgenciaCredito;
	}

	/**
	 * Get: contaCredito.
	 *
	 * @return contaCredito
	 */
	public Long getContaCredito() {
		return contaCredito;
	}

	/**
	 * Set: contaCredito.
	 *
	 * @param contaCredito the conta credito
	 */
	public void setContaCredito(Long contaCredito) {
		this.contaCredito = contaCredito;
	}

	/**
	 * Get: digitoContaCredito.
	 *
	 * @return digitoContaCredito
	 */
	public String getDigitoContaCredito() {
		return digitoContaCredito;
	}

	/**
	 * Set: digitoContaCredito.
	 *
	 * @param digitoContaCredito the digito conta credito
	 */
	public void setDigitoContaCredito(String digitoContaCredito) {
		this.digitoContaCredito = digitoContaCredito;
	}

	/**
	 * Get: cdSituacaoOperacaoPagamento.
	 *
	 * @return cdSituacaoOperacaoPagamento
	 */
	public Integer getCdSituacaoOperacaoPagamento() {
		return cdSituacaoOperacaoPagamento;
	}

	/**
	 * Set: cdSituacaoOperacaoPagamento.
	 *
	 * @param cdSituacaoOperacaoPagamento the cd situacao operacao pagamento
	 */
	public void setCdSituacaoOperacaoPagamento(
			Integer cdSituacaoOperacaoPagamento) {
		this.cdSituacaoOperacaoPagamento = cdSituacaoOperacaoPagamento;
	}

	/**
	 * Get: dsSituacaoOperacaoPagamento.
	 *
	 * @return dsSituacaoOperacaoPagamento
	 */
	public String getDsSituacaoOperacaoPagamento() {
		return dsSituacaoOperacaoPagamento;
	}

	/**
	 * Set: dsSituacaoOperacaoPagamento.
	 *
	 * @param dsSituacaoOperacaoPagamento the ds situacao operacao pagamento
	 */
	public void setDsSituacaoOperacaoPagamento(
			String dsSituacaoOperacaoPagamento) {
		this.dsSituacaoOperacaoPagamento = dsSituacaoOperacaoPagamento;
	}

	/**
	 * Get: dtEfetivacao.
	 *
	 * @return dtEfetivacao
	 */
	public String getDtEfetivacao() {
		return dtEfetivacao;
	}

	/**
	 * Set: dtEfetivacao.
	 *
	 * @param dtEfetivacao the dt efetivacao
	 */
	public void setDtEfetivacao(String dtEfetivacao) {
		this.dtEfetivacao = dtEfetivacao;
	}

	/**
	 * Get: cpfCnpjRecebedor.
	 *
	 * @return cpfCnpjRecebedor
	 */
	public Long getCpfCnpjRecebedor() {
		return cpfCnpjRecebedor;
	}

	/**
	 * Set: cpfCnpjRecebedor.
	 *
	 * @param cpfCnpjRecebedor the cpf cnpj recebedor
	 */
	public void setCpfCnpjRecebedor(Long cpfCnpjRecebedor) {
		this.cpfCnpjRecebedor = cpfCnpjRecebedor;
	}

	/**
	 * Get: filialCpfCnpjRecebedor.
	 *
	 * @return filialCpfCnpjRecebedor
	 */
	public Integer getFilialCpfCnpjRecebedor() {
		return filialCpfCnpjRecebedor;
	}

	/**
	 * Set: filialCpfCnpjRecebedor.
	 *
	 * @param filialCpfCnpjRecebedor the filial cpf cnpj recebedor
	 */
	public void setFilialCpfCnpjRecebedor(Integer filialCpfCnpjRecebedor) {
		this.filialCpfCnpjRecebedor = filialCpfCnpjRecebedor;
	}

	/**
	 * Get: digitoCpfCnpjRecebedor.
	 *
	 * @return digitoCpfCnpjRecebedor
	 */
	public Integer getDigitoCpfCnpjRecebedor() {
		return digitoCpfCnpjRecebedor;
	}

	/**
	 * Set: digitoCpfCnpjRecebedor.
	 *
	 * @param digitoCpfCnpjRecebedor the digito cpf cnpj recebedor
	 */
	public void setDigitoCpfCnpjRecebedor(Integer digitoCpfCnpjRecebedor) {
		this.digitoCpfCnpjRecebedor = digitoCpfCnpjRecebedor;
	}

	/**
	 * Get: cdTipoTela.
	 *
	 * @return cdTipoTela
	 */
	public Integer getCdTipoTela() {
		return cdTipoTela;
	}

	/**
	 * Set: cdTipoTela.
	 *
	 * @param cdTipoTela the cd tipo tela
	 */
	public void setCdTipoTela(Integer cdTipoTela) {
		this.cdTipoTela = cdTipoTela;
	}

	/**
	 * Get: cdAgendadosPagosNaoPagos.
	 *
	 * @return cdAgendadosPagosNaoPagos
	 */
	public Integer getCdAgendadosPagosNaoPagos() {
		return cdAgendadosPagosNaoPagos;
	}

	/**
	 * Get: agenciaCreditoFormatada.
	 *
	 * @return agenciaCreditoFormatada
	 */
	public String getAgenciaCreditoFormatada() {
		return agenciaCreditoFormatada;
	}

	/**
	 * Set: agenciaCreditoFormatada.
	 *
	 * @param agenciaCreditoFormatada the agencia credito formatada
	 */
	public void setAgenciaCreditoFormatada(String agenciaCreditoFormatada) {
		this.agenciaCreditoFormatada = agenciaCreditoFormatada;
	}

	/**
	 * Get: bancoAgenciaContaCreditoFormatado.
	 *
	 * @return bancoAgenciaContaCreditoFormatado
	 */
	public String getBancoAgenciaContaCreditoFormatado() {
		return bancoAgenciaContaCreditoFormatado;
	}

	/**
	 * Set: bancoAgenciaContaCreditoFormatado.
	 *
	 * @param bancoAgenciaContaCreditoFormatado the banco agencia conta credito formatado
	 */
	public void setBancoAgenciaContaCreditoFormatado(
			String bancoAgenciaContaCreditoFormatado) {
		this.bancoAgenciaContaCreditoFormatado = bancoAgenciaContaCreditoFormatado;
	}

	/**
	 * Get: bancoAgenciaContaDebitoFormatado.
	 *
	 * @return bancoAgenciaContaDebitoFormatado
	 */
	public String getBancoAgenciaContaDebitoFormatado() {
		return bancoAgenciaContaDebitoFormatado;
	}

	/**
	 * Set: bancoAgenciaContaDebitoFormatado.
	 *
	 * @param bancoAgenciaContaDebitoFormatado the banco agencia conta debito formatado
	 */
	public void setBancoAgenciaContaDebitoFormatado(
			String bancoAgenciaContaDebitoFormatado) {
		this.bancoAgenciaContaDebitoFormatado = bancoAgenciaContaDebitoFormatado;
	}

	/**
	 * Get: contaCreditoFormatada.
	 *
	 * @return contaCreditoFormatada
	 */
	public String getContaCreditoFormatada() {
		return contaCreditoFormatada;
	}

	/**
	 * Set: contaCreditoFormatada.
	 *
	 * @param contaCreditoFormatada the conta credito formatada
	 */
	public void setContaCreditoFormatada(String contaCreditoFormatada) {
		this.contaCreditoFormatada = contaCreditoFormatada;
	}

	/**
	 * Get: favorecidoBeneficiarioFormatado.
	 *
	 * @return favorecidoBeneficiarioFormatado
	 */
	public String getFavorecidoBeneficiarioFormatado() {
		return favorecidoBeneficiarioFormatado;
	}

	/**
	 * Set: favorecidoBeneficiarioFormatado.
	 *
	 * @param favorecidoBeneficiarioFormatado the favorecido beneficiario formatado
	 */
	public void setFavorecidoBeneficiarioFormatado(
			String favorecidoBeneficiarioFormatado) {
		this.favorecidoBeneficiarioFormatado = favorecidoBeneficiarioFormatado;
	}

	/**
	 * Set: cdAgendadosPagosNaoPagos.
	 *
	 * @param cdAgendadosPagosNaoPagos the cd agendados pagos nao pagos
	 */
	public void setCdAgendadosPagosNaoPagos(Integer cdAgendadosPagosNaoPagos) {
		this.cdAgendadosPagosNaoPagos = cdAgendadosPagosNaoPagos;
	}

	/**
	 * Get: bancoAgenciaContaFavorecidoFormatada.
	 *
	 * @return bancoAgenciaContaFavorecidoFormatada
	 */
	public String getBancoAgenciaContaFavorecidoFormatada() {
		return bancoAgenciaContaFavorecidoFormatada;
	}

	/**
	 * Set: bancoAgenciaContaFavorecidoFormatada.
	 *
	 * @param bancoAgenciaContaFavorecidoFormatada the banco agencia conta favorecido formatada
	 */
	public void setBancoAgenciaContaFavorecidoFormatada(
			String bancoAgenciaContaFavorecidoFormatada) {
		this.bancoAgenciaContaFavorecidoFormatada = bancoAgenciaContaFavorecidoFormatada;
	}

	/**
	 * Get: cpfCnpjClientePagador.
	 *
	 * @return cpfCnpjClientePagador
	 */
	public String getCpfCnpjClientePagador() {
		return CpfCnpjUtils.formatCpfCnpjCompleto(getCdCorpoCpfCnpj(),
				getCdFilialCpfCnpj(), getCdDigitoCpfCnpj());
	}

	/**
	 * Get: cpfCnpjFormatado.
	 *
	 * @return cpfCnpjFormatado
	 */
	public String getCpfCnpjFormatado() {
		return cpfCnpjFormatado;
	}

	/**
	 * Set: cpfCnpjFormatado.
	 *
	 * @param cpfCnpjFormatado the cpf cnpj formatado
	 */
	public void setCpfCnpjFormatado(String cpfCnpjFormatado) {
		this.cpfCnpjFormatado = cpfCnpjFormatado;
	}

	/**
	 * Set: dsMotivoRecusaEstorno.
	 *
	 * @param dsMotivoRecusaEstorno the ds motivo recusa estorno
	 */
	public void setDsMotivoRecusaEstorno(String dsMotivoRecusaEstorno) {
		this.dsMotivoRecusaEstorno = dsMotivoRecusaEstorno;
	}

	/**
	 * Get: dsMotivoRecusaEstorno.
	 *
	 * @return dsMotivoRecusaEstorno
	 */
	public String getDsMotivoRecusaEstorno() {
		return dsMotivoRecusaEstorno;
	}
}