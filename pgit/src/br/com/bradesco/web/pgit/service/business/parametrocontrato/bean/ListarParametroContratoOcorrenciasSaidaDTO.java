package br.com.bradesco.web.pgit.service.business.parametrocontrato.bean;


// TODO: Auto-generated Javadoc
/**
 * Arquivo criado em 22/01/16.
 */
public class ListarParametroContratoOcorrenciasSaidaDTO {

    /** The cd parametro. */
    private Integer cdParametro = null;

    /** The ds parametro. */
    private String dsParametro = null;

    /** The ds parametro contrato. */
    private String dsParametroContrato = null;

    /** The cd usuario inclusao. */
    private String cdUsuarioInclusao = null;

    /** The hr inclusao registro. */
    private String hrInclusaoRegistro = null;

    /** The cd tipo canal. */
    private Integer cdTipoCanal = null;

    /** The ds tipo canal. */
    private String dsTipoCanal = null;

    /**
     * Sets the cd parametro.
     *
     * @param cdParametro the cd parametro
     */
    public void setCdParametro(Integer cdParametro) {
        this.cdParametro = cdParametro;
    }

    /**
     * Gets the cd parametro.
     *
     * @return the cd parametro
     */
    public Integer getCdParametro() {
        return this.cdParametro;
    }

    /**
     * Sets the ds parametro.
     *
     * @param dsParametro the ds parametro
     */
    public void setDsParametro(String dsParametro) {
        this.dsParametro = dsParametro;
    }

    /**
     * Gets the ds parametro.
     *
     * @return the ds parametro
     */
    public String getDsParametro() {
        return this.dsParametro;
    }

    /**
     * Sets the ds parametro contrato.
     *
     * @param dsParametroContrato the ds parametro contrato
     */
    public void setDsParametroContrato(String dsParametroContrato) {
        this.dsParametroContrato = dsParametroContrato;
    }

    /**
     * Gets the ds parametro contrato.
     *
     * @return the ds parametro contrato
     */
    public String getDsParametroContrato() {
        return this.dsParametroContrato;
    }

    /**
     * Sets the cd usuario inclusao.
     *
     * @param cdUsuarioInclusao the cd usuario inclusao
     */
    public void setCdUsuarioInclusao(String cdUsuarioInclusao) {
        this.cdUsuarioInclusao = cdUsuarioInclusao;
    }

    /**
     * Gets the cd usuario inclusao.
     *
     * @return the cd usuario inclusao
     */
    public String getCdUsuarioInclusao() {
        return this.cdUsuarioInclusao;
    }

    /**
     * Sets the hr inclusao registro.
     *
     * @param hrInclusaoRegistro the hr inclusao registro
     */
    public void setHrInclusaoRegistro(String hrInclusaoRegistro) {
        this.hrInclusaoRegistro = hrInclusaoRegistro;
    }

    /**
     * Gets the hr inclusao registro.
     *
     * @return the hr inclusao registro
     */
    public String getHrInclusaoRegistro() {
        return this.hrInclusaoRegistro;
    }

    /**
     * Sets the cd tipo canal.
     *
     * @param cdTipoCanal the cd tipo canal
     */
    public void setCdTipoCanal(Integer cdTipoCanal) {
        this.cdTipoCanal = cdTipoCanal;
    }

    /**
     * Gets the cd tipo canal.
     *
     * @return the cd tipo canal
     */
    public Integer getCdTipoCanal() {
        return this.cdTipoCanal;
    }

    /**
     * Sets the ds tipo canal.
     *
     * @param dsTipoCanal the ds tipo canal
     */
    public void setDsTipoCanal(String dsTipoCanal) {
        this.dsTipoCanal = dsTipoCanal;
    }

    /**
     * Gets the ds tipo canal.
     *
     * @return the ds tipo canal
     */
    public String getDsTipoCanal() {
        return this.dsTipoCanal;
    }
}