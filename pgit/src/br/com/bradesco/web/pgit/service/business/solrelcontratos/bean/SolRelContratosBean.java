/*
 * Nome: br.com.bradesco.web.pgit.service.business.solrelcontratos.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.solrelcontratos.bean;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.apache.commons.lang.StringUtils;

import br.com.bradesco.web.aq.application.error.BradescoViewException.BradescoViewExceptionActionType;
import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterException;
import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterFunctionalException;
import br.com.bradesco.web.aq.application.util.faces.BradescoFacesUtils;
import br.com.bradesco.web.pgit.service.business.combo.IComboService;
import br.com.bradesco.web.pgit.service.business.combo.bean.EmpresaConglomeradoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.EmpresaConglomeradoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.EmpresaSegmentoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarClasseRamoAtvddSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarDependenciaDiretoriaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarDependenciaEmpresaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarModalidadeSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarServicosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarSramoAtvddEconcEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarSramoAtvddEconcSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.impl.bean.ListarModalidadesEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultas.IConsultasService;
import br.com.bradesco.web.pgit.service.business.consultas.bean.ConsultarDescGrupoEconomicoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultas.bean.ConsultarDescGrupoEconomicoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadcontadestino.IManterCadContaDestinoService;
import br.com.bradesco.web.pgit.service.business.mantercadcontadestino.bean.ConsultarBancoAgenciaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadcontadestino.bean.ConsultarBancoAgenciaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.solrelcontratos.ISolRelContratosService;
import br.com.bradesco.web.pgit.utils.PgitUtil;
import br.com.bradesco.web.pgit.view.utils.PgitFacesUtils;

/**
 * Nome: SolRelContratosBean
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class SolRelContratosBean {
	
	// Constante utilizada para evitar redund�ncia apontada pelo Sonar
	/** Atributo CON_MANTER_SOLICITACAO_REL_CONTRATO. */
	private static final String CON_MANTER_SOLICITACAO_REL_CONTRATO = "conManterSolicitacaoRelContrato";
	
	/** Atributo filtroTipoRelatorio. */
	private Integer filtroTipoRelatorio;
	
	/** Atributo tipoServico. */
	private Integer tipoServico;
	
	/** Atributo tipoRelatorio. */
	private Integer tipoRelatorio = 1;
	
	/** Atributo tipoRelatorioDesc. */
	private String tipoRelatorioDesc;
	
	/** Atributo modalidadeServico. */
	private Integer modalidadeServico;	
	
	/** Atributo voltarPesquisar. */
	private String voltarPesquisar;	
	
	/** Atributo filtroDataDe. */
	private Date filtroDataDe;
	
	/** Atributo filtroDataAte. */
	private Date filtroDataAte;
	
	/** Atributo empresa. */
	private Long empresa;
	
	/** Atributo diretoriaRegional. */
	private Integer diretoriaRegional;
	
	/** Atributo gerenciaRegional. */
	private Integer gerenciaRegional;
	
	/** Atributo agenciaOperadora. */
	private String agenciaOperadora;
	
	/** Atributo agenciaDigitada. */
	private String agenciaDigitada;
	
	/** Atributo dsAgencia. */
	private String dsAgencia;
	
	/** Atributo dsAgenciaAux. */
	private String dsAgenciaAux;
	
	/** Atributo segmento. */
	private Integer segmento;	
	
	/** Atributo participante. */
	private Long participante;
	
	/** Atributo participanteDesc. */
	private String participanteDesc;
	
	/** Atributo tipoServicoDesc. */
	private String tipoServicoDesc;
	
	/** Atributo modalidadeServiceDesc. */
	private String modalidadeServiceDesc;
	
	/** Atributo empresaDesc. */
	private String empresaDesc;
	
	/** Atributo diretoriaRegionalDesc. */
	private String diretoriaRegionalDesc;
	
	/** Atributo gerenciaRegionalDesc. */
	private String gerenciaRegionalDesc;
	
	/** Atributo segmentoDesc. */
	private String segmentoDesc;
	
	/** Atributo dataHoraSolicitacao. */
	private String dataHoraSolicitacao;	
	
	/** Atributo listaGrid. */
	private List<ListarSolRelContratosSaidaDTO> listaGrid =  new ArrayList<ListarSolRelContratosSaidaDTO>();
	
	/** Atributo listaControleRadio. */
	private List<SelectItem> listaControleRadio = new ArrayList<SelectItem>();
	
	/** Atributo itemSelecionadoLista. */
	private Integer itemSelecionadoLista;
	
	/** Atributo listaTipoServico. */
	private List<SelectItem> listaTipoServico = new ArrayList<SelectItem>();	
	
	/** Atributo listaTipoServicoHash. */
	private Map<Integer,String> listaTipoServicoHash = new HashMap<Integer,String>();
	
	/** Atributo comboService. */
	private IComboService comboService;
	
	/** Atributo manterCadContaDestinoService. */
	private IManterCadContaDestinoService manterCadContaDestinoService;
	
	/** Atributo consultasService. */
	private IConsultasService consultasService;
	
	/** Atributo criterioSelecaoContratos. */
	private Integer criterioSelecaoContratos ;
	
	/** Atributo listaModalidadeServico. */
	private List<SelectItem> listaModalidadeServico = new ArrayList<SelectItem>();
	
	/** Atributo listaModalidadeServicoHash. */
	private Map<Integer, ListarModalidadeSaidaDTO> listaModalidadeServicoHash = new HashMap<Integer, ListarModalidadeSaidaDTO> ();
	
	/** Atributo manterSolicitacaoRelatorioContratoImpl. */
	private ISolRelContratosService manterSolicitacaoRelatorioContratoImpl;
	
	/** Atributo nivelConsolidacao. */
	private Integer nivelConsolidacao;
	
	/** Atributo itemCheckEmpresaConglomerado. */
	private Boolean itemCheckEmpresaConglomerado;
	
	/** Atributo itemCheckDiretoriaRegional. */
	private Boolean itemCheckDiretoriaRegional;
	
	/** Atributo itemCheckGerenciaRegional. */
	private Boolean itemCheckGerenciaRegional;
	
	/** Atributo itemCheckAgenciaOperadora. */
	private Boolean itemCheckAgenciaOperadora;
	
	/** Atributo itemCheckSegmento. */
	private Boolean itemCheckSegmento;
	
	/** Atributo itemCheckParticipanteGrupoEconomico. */
	private Boolean itemCheckParticipanteGrupoEconomico;
	
	/** Atributo itemCheckTipoServico. */
	private Boolean itemCheckTipoServico;
	
	/** Atributo itemCheckModalidadeServico. */
	private Boolean itemCheckModalidadeServico;
	
	/** Atributo itemCheckClasse. */
	private Boolean itemCheckClasse;
	
	/** Atributo btoAcionado. */
	private Boolean btoAcionado;
	
	
	/** Atributo classeRamo. */
	private String classeRamo;
	
	/** Atributo classeRamoDesc. */
	private String classeRamoDesc;
	
	/** Atributo subRamoAtividadeEconomica. */
	private String subRamoAtividadeEconomica;
	
	/** Atributo subRamoAtividadeEconomicaDesc. */
	private String subRamoAtividadeEconomicaDesc;
	
	/** Atributo listaCboClasseRamo. */
	private List<SelectItem> listaCboClasseRamo = new ArrayList<SelectItem>();

	/** Atributo listaCboClasseRamoHash. */
	private Map<String, ListarClasseRamoAtvddSaidaDTO> listaCboClasseRamoHash = new HashMap<String, ListarClasseRamoAtvddSaidaDTO>();

	/** Atributo listaCboSubRamoAtividade. */
	private List<SelectItem> listaCboSubRamoAtividade = new ArrayList<SelectItem>();

	/** Atributo listaCboSubRamoAtividadeHash. */
	private Map<String, ListarSramoAtvddEconcSaidaDTO> listaCboSubRamoAtividadeHash = new HashMap<String, ListarSramoAtvddEconcSaidaDTO>();
	
	
	/** Atributo listaDiretoriaRegional. */
	private List<SelectItem> listaDiretoriaRegional = new ArrayList<SelectItem>();
	
	/** Atributo listaDiretoriaRegionalHash. */
	private Map listaDiretoriaRegionalHash = new HashMap<Integer,String>();
	
	/** Atributo listaGerenciaRegional. */
	private List<SelectItem> listaGerenciaRegional = new ArrayList<SelectItem>();
	
	/** Atributo listaGerenciaRegionalHash. */
	private Map<Integer,String> listaGerenciaRegionalHash = new HashMap<Integer,String>();
	
	/** Atributo listaSegmento. */
	private List<SelectItem> listaSegmento = new ArrayList<SelectItem>();
	
	/** Atributo listaSegmentoHash. */
	private Map<Integer,String> listaSegmentoHash = new HashMap<Integer,String>();
	
	/** Atributo hiddenObrigatoriedade. */
	private String hiddenObrigatoriedade;
	
	/** Atributo dataHoraInclusao. */
	private String dataHoraInclusao;
	
	/** Atributo usuarioInclusao. */
	private String usuarioInclusao;
	
	/** Atributo tipoCanalInclusao. */
	private String tipoCanalInclusao;
	
	/** Atributo complementoInclusao. */
	private String complementoInclusao;
	
	/** Atributo dataHoraManutencao. */
	private String dataHoraManutencao;
	
	/** Atributo usuarioManutencao. */
	private String usuarioManutencao;
	
	/** Atributo tipoCanalManutencao. */
	private String tipoCanalManutencao;
	
	/** Atributo complementoManutencao. */
	private String complementoManutencao;
	
	/** Atributo listaEmpresaConglomerado. */
	private List<SelectItem> listaEmpresaConglomerado = new ArrayList<SelectItem>();
	
	/** Atributo listaEmpresaConglomeradoHash. */
	private Map<Long,String> listaEmpresaConglomeradoHash = new HashMap<Long,String>();
	
	/** Atributo saidaGrupoEconomico. */
	private ConsultarDescGrupoEconomicoSaidaDTO saidaGrupoEconomico = new ConsultarDescGrupoEconomicoSaidaDTO();

	/**
	 * Get: filtroDataAte.
	 *
	 * @return filtroDataAte
	 */
	public Date getFiltroDataAte() {
		if (filtroDataAte == null){
			filtroDataAte = new Date(); 
		}
		return filtroDataAte;
	}

	/**
	 * Set: filtroDataAte.
	 *
	 * @param filtroDataAte the filtro data ate
	 */
	public void setFiltroDataAte(Date filtroDataAte) {
		this.filtroDataAte = filtroDataAte;
	}

	/**
	 * Get: filtroDataDe.
	 *
	 * @return filtroDataDe
	 */
	public Date getFiltroDataDe() {
		if (filtroDataDe == null){
			filtroDataDe = new Date(); 
		}
		return filtroDataDe;
	}

	/**
	 * Set: filtroDataDe.
	 *
	 * @param filtroDataDe the filtro data de
	 */
	public void setFiltroDataDe(Date filtroDataDe) {
		this.filtroDataDe = filtroDataDe;
	}

	/**
	 * Get: filtroTipoRelatorio.
	 *
	 * @return filtroTipoRelatorio
	 */
	public Integer getFiltroTipoRelatorio() {
		return filtroTipoRelatorio;
	}

	/**
	 * Set: filtroTipoRelatorio.
	 *
	 * @param filtroTipoRelatorio the filtro tipo relatorio
	 */
	public void setFiltroTipoRelatorio(Integer filtroTipoRelatorio) {
		this.filtroTipoRelatorio = filtroTipoRelatorio;
	}

	
	/**
	 * Get: voltarPesquisar.
	 *
	 * @return voltarPesquisar
	 */
	public String getVoltarPesquisar() {
		return voltarPesquisar;
	}

	/**
	 * Set: voltarPesquisar.
	 *
	 * @param voltarPesquisar the voltar pesquisar
	 */
	public void setVoltarPesquisar(String voltarPesquisar) {
		this.voltarPesquisar = voltarPesquisar;
	}

	/**
	 * Get: manterSolicitacaoRelatorioContratoImpl.
	 *
	 * @return manterSolicitacaoRelatorioContratoImpl
	 */
	public ISolRelContratosService getManterSolicitacaoRelatorioContratoImpl() {
		return manterSolicitacaoRelatorioContratoImpl;
	}

	/**
	 * Set: manterSolicitacaoRelatorioContratoImpl.
	 *
	 * @param manterSolicitacaoRelatorioContratoImpl the manter solicitacao relatorio contrato impl
	 */
	public void setManterSolicitacaoRelatorioContratoImpl(
			ISolRelContratosService manterSolicitacaoRelatorioContratoImpl) {
		this.manterSolicitacaoRelatorioContratoImpl = manterSolicitacaoRelatorioContratoImpl;
	}

	/**
	 * Limpar campos.
	 */
	public void limparCampos(){
		this.setFiltroTipoRelatorio(null);
		this.setFiltroDataAte(null);
		this.setFiltroDataDe(null);
		this.setItemSelecionadoLista(null);
		setListaGrid(null);
		setBtoAcionado(false);
	}

	/**
	 * Avancar detalhar.
	 *
	 * @return the string
	 */
	public String avancarDetalhar(){
		try {
			preencheDados();
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), CON_MANTER_SOLICITACAO_REL_CONTRATO, BradescoViewExceptionActionType.ACTION, false);
			return null;
		} 
		return "AVANCAR_DETALHAR";
	}
	
	/**
	 * Avancar incluir.
	 *
	 * @return the string
	 */
	public String avancarIncluir(){
		limparCamposIncluir();
		carregaComboClasseRamo();
		return "AVANCAR_INCLUIR";
	}
	
	/**
	 * Avancar excluir.
	 *
	 * @return the string
	 */
	public String avancarExcluir(){
		try {
			preencheDados();
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), CON_MANTER_SOLICITACAO_REL_CONTRATO, BradescoViewExceptionActionType.ACTION, false);
			return null;
		} 
		return "AVANCAR_EXCLUIR";
	}	

	/**
	 * Voltar pesquisar.
	 *
	 * @return the string
	 */
	public String voltarPesquisar(){
		setItemSelecionadoLista(null);
		return "VOLTAR_PESQUISAR";
	}
	
	/**
	 * Busca ag operadora.
	 */
	public void buscaAgOperadora(){
	    if(getAgenciaOperadora() != null && !getAgenciaOperadora().equals("")){
        	    try{
        		setDsAgencia("");
        		setDsAgenciaAux("");
        		ConsultarBancoAgenciaEntradaDTO entrada = new ConsultarBancoAgenciaEntradaDTO();
        		
        		entrada.setCdAgencia(Integer.parseInt(getAgenciaOperadora()));
        		entrada.setCdBanco(237);
        		entrada.setCdDigitoAgencia(0);
        		entrada.setCdPessoaJuridicaContrato(getEmpresa());
        		
        		ConsultarBancoAgenciaSaidaDTO saida = getManterCadContaDestinoService().consultarDescricaoBancoAgencia(entrada);
        		        		
        		setAgenciaDigitada(getAgenciaOperadora());
        		setDsAgencia(saida.getDsAgencia());
        		setDsAgenciaAux(saida.getDsAgencia());
        	    }catch(PdcAdapterFunctionalException p){
        		setAgenciaDigitada(null);
        		setDsAgencia("");
        		setDsAgenciaAux("");
        		BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);   
        	    }
	    }
	}
	
	/**
	 * Busca desc grupo economico.
	 */
	public void buscaDescGrupoEconomico(){
	    if(getParticipante() != null && !getParticipante().equals("")){
        	    try{
        		saidaGrupoEconomico.setDsGrupoEconomico(null);
        		ConsultarDescGrupoEconomicoEntradaDTO entrada = new ConsultarDescGrupoEconomicoEntradaDTO();
        		
        		entrada.setCdGrupoEconomico(getParticipante());
        		        		
        		saidaGrupoEconomico = getConsultasService().consultarDescGrupoEconomico(entrada);
        		        		
        	    }catch(PdcAdapterFunctionalException p){
        		saidaGrupoEconomico = new ConsultarDescGrupoEconomicoSaidaDTO();
        		BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);   
        	    }
	    }
	}
	
	/**
	 * Limpar agencia.
	 */
	public void limparAgencia(){
	    setAgenciaOperadora("");
	    setDsAgencia("");
	    setDsAgenciaAux("");
	}
	
	/**
	 * Get: diretoriaRegional.
	 *
	 * @return diretoriaRegional
	 */
	public Integer getDiretoriaRegional() {
		return diretoriaRegional;
	}

	/**
	 * Set: diretoriaRegional.
	 *
	 * @param diretoriaRegional the diretoria regional
	 */
	public void setDiretoriaRegional(Integer diretoriaRegional) {
		this.diretoriaRegional = diretoriaRegional;
	}

	/**
	 * Get: empresa.
	 *
	 * @return empresa
	 */
	public Long getEmpresa() {
		return empresa;
	}

	/**
	 * Set: empresa.
	 *
	 * @param empresa the empresa
	 */
	public void setEmpresa(Long empresa) {
		this.empresa = empresa;
	}

	
	
	/**
	 * Get: gerenciaRegional.
	 *
	 * @return gerenciaRegional
	 */
	public Integer getGerenciaRegional() {
		return gerenciaRegional;
	}

	/**
	 * Set: gerenciaRegional.
	 *
	 * @param gerenciaRegional the gerencia regional
	 */
	public void setGerenciaRegional(Integer gerenciaRegional) {
		this.gerenciaRegional = gerenciaRegional;
	}

	/**
	 * Get: segmento.
	 *
	 * @return segmento
	 */
	public Integer getSegmento() {
		return segmento;
	}

	/**
	 * Set: segmento.
	 *
	 * @param segmento the segmento
	 */
	public void setSegmento(Integer segmento) {
		this.segmento = segmento;
	}


	/**
	 * Get: agenciaOperadora.
	 *
	 * @return agenciaOperadora
	 */
	public String getAgenciaOperadora() {
		return agenciaOperadora;
	}

	/**
	 * Set: agenciaOperadora.
	 *
	 * @param unidadeOrganizacionalOperadora the agencia operadora
	 */
	public void setAgenciaOperadora(
			String unidadeOrganizacionalOperadora) {
		this.agenciaOperadora = unidadeOrganizacionalOperadora;
	}

	/**
	 * Limpar campos incluir.
	 */
	public void limparCamposIncluir(){
	
		setTipoRelatorio(1);
		setTipoServico(0);
		setModalidadeServico(0);
		setEmpresa(0l);
		setDiretoriaRegional(0);
		setGerenciaRegional(0);
		setAgenciaOperadora("");
		setDsAgencia("");
		setDsAgenciaAux("");
		setSegmento(0);
		setParticipante(null);
		setClasseRamo("0");		
		setSubRamoAtividadeEconomica("0");
		setClasseRamoDesc("");
		setSubRamoAtividadeEconomicaDesc("");
		setItemCheckAgenciaOperadora(false);		
		setItemCheckClasse(false);
		setItemCheckDiretoriaRegional(false);
		setItemCheckEmpresaConglomerado(false);
		setItemCheckGerenciaRegional(false);
		setItemCheckModalidadeServico(false);
		setItemCheckParticipanteGrupoEconomico(false);		
		setItemCheckSegmento(false);		
		setItemCheckTipoServico(false);
		setEmpresa(2269651L);
	
	}
	
	/**
	 * Avancar incluir confirmar.
	 *
	 * @return the string
	 */
	public String avancarIncluirConfirmar(){
		
		//if (getHiddenObrigatoriedade() != null && getHiddenObrigatoriedade().equals("T")){
			
	    		setAgenciaOperadora(getAgenciaDigitada());
			setTipoServicoDesc(getItemCheckTipoServico()?(String)listaTipoServicoHash.get(getTipoServico()):"");
			setModalidadeServiceDesc(getItemCheckModalidadeServico()? listaModalidadeServicoHash.get(getModalidadeServico()).getDsModalidade():"");
			setEmpresaDesc(getEmpresa() != 0L ? (String)listaEmpresaConglomeradoHash.get(getEmpresa()):"");
			setDiretoriaRegionalDesc(getItemCheckDiretoriaRegional()? (String)listaDiretoriaRegionalHash.get(getDiretoriaRegional()):"");
			setGerenciaRegionalDesc(getItemCheckGerenciaRegional()? (String) listaGerenciaRegionalHash.get(getGerenciaRegional()):"");
			setSegmentoDesc(getItemCheckSegmento() ?  (String) listaSegmentoHash.get(getSegmento()):"");
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			setDataHoraSolicitacao(sdf.format(new Date()));
			setClasseRamoDesc(getClasseRamo() == null || getClasseRamo().equals("0") ? "" : listaCboClasseRamoHash.get(getClasseRamo()).getClasseRamoFormatado());
			setSubRamoAtividadeEconomicaDesc(getSubRamoAtividadeEconomica() == null || getSubRamoAtividadeEconomica().equals("0") ? "" : listaCboSubRamoAtividadeHash.get(getSubRamoAtividadeEconomica()).getSubRamoAtividadeFormatado());
			setDsAgencia(getDsAgenciaAux());
			return "AVANCAR_INCLUIR2";
	//	}
		
	//	return "";
	}
	
	/**
	 * Voltar incluir.
	 *
	 * @return the string
	 */
	public String voltarIncluir(){
		return "VOLTAR_INCLUIR";
	}

	
	/**
	 * Carrega grid.
	 *
	 * @return the string
	 */
	public String carregaGrid(){
		
		try{
			
			ListarSolRelContratosEntradaDTO listarSolRelContratosEntradaDTO = new ListarSolRelContratosEntradaDTO();
				
			SimpleDateFormat simplaDateFormat = new SimpleDateFormat("dd/MM/yyyy");
			
			listarSolRelContratosEntradaDTO.setDataFinal(simplaDateFormat.format(getFiltroDataAte()));
			listarSolRelContratosEntradaDTO.setDataInicial(simplaDateFormat.format(getFiltroDataDe()));
			listarSolRelContratosEntradaDTO.setTipoRelatorio(getFiltroTipoRelatorio()!=null ?getFiltroTipoRelatorio():0);

			setListaGrid(getManterSolicitacaoRelatorioContratoImpl().listarSolicitacaoRelatorioContratos(listarSolRelContratosEntradaDTO));
			setBtoAcionado(true);
			this.listaControleRadio =  new ArrayList<SelectItem>();
			for (int i = 0; i <= getListaGrid().size();i++) {
				this.listaControleRadio.add(new SelectItem(i," "));
			}
			setItemSelecionadoLista(null);
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			setListaGrid(null);

		}
		return "";
		
	}

	/**
	 * Get: listaGrid.
	 *
	 * @return listaGrid
	 */
	public List<ListarSolRelContratosSaidaDTO> getListaGrid() {
		return listaGrid;
	}

	/**
	 * Set: listaGrid.
	 *
	 * @param listaGrid the lista grid
	 */
	public void setListaGrid(List<ListarSolRelContratosSaidaDTO> listaGrid) {
		this.listaGrid = listaGrid;
	}

	/**
	 * Get: itemSelecionadoLista.
	 *
	 * @return itemSelecionadoLista
	 */
	public Integer getItemSelecionadoLista() {
		return itemSelecionadoLista;
	}

	/**
	 * Set: itemSelecionadoLista.
	 *
	 * @param itemSelecionadoLista the item selecionado lista
	 */
	public void setItemSelecionadoLista(Integer itemSelecionadoLista) {
		this.itemSelecionadoLista = itemSelecionadoLista;
	}

	/**
	 * Get: listaControleRadio.
	 *
	 * @return listaControleRadio
	 */
	public List<SelectItem> getListaControleRadio() {
		return listaControleRadio;
	}

	/**
	 * Set: listaControleRadio.
	 *
	 * @param listaControleRadio the lista controle radio
	 */
	public void setListaControleRadio(List<SelectItem> listaControleRadio) {
		this.listaControleRadio = listaControleRadio;
	}

	/**
	 * Get: listaTipoServico.
	 *
	 * @return listaTipoServico
	 */
	public List<SelectItem> getListaTipoServico() {
		
		try{
			this.listaTipoServico = new ArrayList<SelectItem>();
			
			List<ListarServicosSaidaDTO> listaServico = new ArrayList<ListarServicosSaidaDTO>();
			
			listaServico = comboService.listarTipoServicos(0);
			
			listaTipoServicoHash.clear();
			
			for(ListarServicosSaidaDTO combo : listaServico){
				listaTipoServicoHash.put(combo.getCdServico(), combo.getDsServico());
				listaTipoServico.add(new SelectItem(combo.getCdServico(),combo.getDsServico()));
			}
		}catch(PdcAdapterFunctionalException e	){
			listaTipoServico = new ArrayList<SelectItem>();
		}
		
		return this.listaTipoServico;
	}

	/**
	 * Set: listaTipoServico.
	 *
	 * @param listaTipoServico the lista tipo servico
	 */
	public void setListaTipoServico(List<SelectItem> listaTipoServico) {
		this.listaTipoServico = listaTipoServico;
	}

	/**
	 * Get: comboService.
	 *
	 * @return comboService
	 */
	public IComboService getComboService() {
		return comboService;
	}

	/**
	 * Set: comboService.
	 *
	 * @param comboService the combo service
	 */
	public void setComboService(IComboService comboService) {
		this.comboService = comboService;
	}

	/**
	 * Get: listaTipoServicoHash.
	 *
	 * @return listaTipoServicoHash
	 */
	Map<Integer, String> getListaTipoServicoHash() {
		return listaTipoServicoHash;
	}

	/**
	 * Set lista tipo servico hash.
	 *
	 * @param listaTipoServicoHash the lista tipo servico hash
	 */
	void setListaTipoServicoHash(Map<Integer, String> listaTipoServicoHash) {
		this.listaTipoServicoHash = listaTipoServicoHash;
	}

	/**
	 * Get: tipoServico.
	 *
	 * @return tipoServico
	 */
	public Integer getTipoServico() {
		return tipoServico;
	}

	/**
	 * Set: tipoServico.
	 *
	 * @param tipoServico the tipo servico
	 */
	public void setTipoServico(Integer tipoServico) {
		this.tipoServico = tipoServico;
	}

	/**
	 * Set: tipoRelatorio.
	 *
	 * @param tipoRelatorio the tipo relatorio
	 */
	public void setTipoRelatorio(Integer tipoRelatorio) {
		this.tipoRelatorio = tipoRelatorio;
	}

	/**
	 * Get: tipoRelatorio.
	 *
	 * @return tipoRelatorio
	 */
	public Integer getTipoRelatorio() {
		return tipoRelatorio;
	}

	/**
	 * Get: criterioSelecaoContratos.
	 *
	 * @return criterioSelecaoContratos
	 */
	public Integer getCriterioSelecaoContratos() {
		return criterioSelecaoContratos;
	}

	/**
	 * Set: criterioSelecaoContratos.
	 *
	 * @param criterioSelecaoContratos the criterio selecao contratos
	 */
	public void setCriterioSelecaoContratos(Integer criterioSelecaoContratos) {
		this.criterioSelecaoContratos = criterioSelecaoContratos;
	}
	
	/**
	 * Teste.
	 */
	public void teste(){

	}

	/**
	 * Get: listaModalidadeServico.
	 *
	 * @return listaModalidadeServico
	 */
	public List<SelectItem> getListaModalidadeServico() {
		
		if (getTipoServico() != null && getTipoServico() != 0){
			try{
				
				this.listaModalidadeServico = new ArrayList<SelectItem>();
				
				List<ListarModalidadeSaidaDTO> listaModalidades = new ArrayList<ListarModalidadeSaidaDTO>();
				ListarModalidadesEntradaDTO listarModalidadeEntradaDTO = new ListarModalidadesEntradaDTO();
				
				listarModalidadeEntradaDTO.setCdServico(getTipoServico());
				
			
				listaModalidades = comboService.listarModalidades(listarModalidadeEntradaDTO);
				
				listaModalidadeServicoHash.clear();
				
				for(ListarModalidadeSaidaDTO combo : listaModalidades){				
					this.listaModalidadeServico.add(new SelectItem(combo.getCdModalidade(),combo.getDsModalidade()));
					listaModalidadeServicoHash.put(combo.getCdModalidade(),combo);
					
				}
				
			} catch (PdcAdapterException p) {
				FacesContext.getCurrentInstance().addMessage("erro", new FacesMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage()));
				this.setListaModalidadeServico(new ArrayList<SelectItem>());
			}
		}else{
			listaModalidadeServico = new ArrayList<SelectItem>();
		}
		return listaModalidadeServico;
	}

	/**
	 * Set: listaModalidadeServico.
	 *
	 * @param listaModalidadeServico the lista modalidade servico
	 */
	public void setListaModalidadeServico(List<SelectItem> listaModalidadeServico) {
		this.listaModalidadeServico = listaModalidadeServico;
	}

	/**
	 * Get: listaModalidadeServicoHash.
	 *
	 * @return listaModalidadeServicoHash
	 */
	Map<Integer, ListarModalidadeSaidaDTO> getListaModalidadeServicoHash() {
		return listaModalidadeServicoHash;
	}

	/**
	 * Set lista modalidade servico hash.
	 *
	 * @param listaModalidadeServicoHash the lista modalidade servico hash
	 */
	void setListaModalidadeServicoHash(
			Map<Integer, ListarModalidadeSaidaDTO> listaModalidadeServicoHash) {
		this.listaModalidadeServicoHash = listaModalidadeServicoHash;
	}

	/**
	 * Get: modalidadeServico.
	 *
	 * @return modalidadeServico
	 */
	public Integer getModalidadeServico() {
		return modalidadeServico;
	}

	/**
	 * Set: modalidadeServico.
	 *
	 * @param modalidadeServico the modalidade servico
	 */
	public void setModalidadeServico(Integer modalidadeServico) {
		this.modalidadeServico = modalidadeServico;
	}

	/**
	 * Set: nivelConsolidacao.
	 *
	 * @param nivelConsolidacao the nivel consolidacao
	 */
	public void setNivelConsolidacao(Integer nivelConsolidacao) {
		this.nivelConsolidacao = nivelConsolidacao;
	}

	/**
	 * Get: listaEmpresaConglomerado.
	 *
	 * @return listaEmpresaConglomerado
	 */
	public List<SelectItem> getListaEmpresaConglomerado() {
		try{
			listaEmpresaConglomerado = new ArrayList<SelectItem>();
			List<EmpresaConglomeradoSaidaDTO> listaConglomerado;
			
			EmpresaConglomeradoEntradaDTO tipoUnidadeOrganizacionalDTO = new EmpresaConglomeradoEntradaDTO();
			tipoUnidadeOrganizacionalDTO.setNrOcorrencias(50);
			tipoUnidadeOrganizacionalDTO.setCdSituacao(0);
		
			listaConglomerado = comboService.listarEmpresaConglomerado(tipoUnidadeOrganizacionalDTO);
			
			listaEmpresaConglomeradoHash.clear();
			for (int i = 0; i < listaConglomerado.size(); i++) {
				listaEmpresaConglomeradoHash.put(listaConglomerado.get(i).getCodClub(), PgitUtil.concatenarCampos(listaConglomerado.get(i).getCodClub(), listaConglomerado.get(i).getDsRazaoSocial()));
				this.listaEmpresaConglomerado.add(new SelectItem(String.valueOf(listaConglomerado.get(i).getCodClub()),PgitUtil.concatenarCampos(listaConglomerado.get(i).getCodClub(), listaConglomerado.get(i).getDsRazaoSocial())));
			}
		}catch(PdcAdapterFunctionalException e){
			listaEmpresaConglomerado = new ArrayList<SelectItem>();
		}

		return this.listaEmpresaConglomerado;
	}

	/**
	 * Set: listaEmpresaConglomerado.
	 *
	 * @param listaEmpresaConglomerado the lista empresa conglomerado
	 */
	public void setListaEmpresaConglomerado(
			List<SelectItem> listaEmpresaConglomerado) {
		this.listaEmpresaConglomerado = listaEmpresaConglomerado;
	}

	

	/**
	 * Get: itemCheckEmpresaConglomerado.
	 *
	 * @return itemCheckEmpresaConglomerado
	 */
	public Boolean getItemCheckEmpresaConglomerado() {
		return itemCheckEmpresaConglomerado;
	}

	/**
	 * Set: itemCheckEmpresaConglomerado.
	 *
	 * @param itemCheckEmpresaConglomerado the item check empresa conglomerado
	 */
	public void setItemCheckEmpresaConglomerado(Boolean itemCheckEmpresaConglomerado) {
		this.itemCheckEmpresaConglomerado = itemCheckEmpresaConglomerado;
	}

	/**
	 * Get: nivelConsolidacao.
	 *
	 * @return nivelConsolidacao
	 */
	public Integer getNivelConsolidacao() {
		return nivelConsolidacao;
	}

	/**
	 * Get: itemCheckDiretoriaRegional.
	 *
	 * @return itemCheckDiretoriaRegional
	 */
	public Boolean getItemCheckDiretoriaRegional() {
		return itemCheckDiretoriaRegional;
	}

	/**
	 * Set: itemCheckDiretoriaRegional.
	 *
	 * @param itemCheckDiretoriaRegional the item check diretoria regional
	 */
	public void setItemCheckDiretoriaRegional(Boolean itemCheckDiretoriaRegional) {
		this.itemCheckDiretoriaRegional = itemCheckDiretoriaRegional;
	}

	/**
	 * Get: listaDiretoriaRegional.
	 *
	 * @return listaDiretoriaRegional
	 */
	@SuppressWarnings("unchecked")
	public List<SelectItem> getListaDiretoriaRegional() {
		try{
			if (getEmpresa() !=null && getEmpresa() != 0L  ){
				List<ListarDependenciaEmpresaSaidaDTO> list = comboService.pesquisaDiretoria(getEmpresa());
				
				listaDiretoriaRegionalHash.clear();
				listaDiretoriaRegional.clear();
				for(ListarDependenciaEmpresaSaidaDTO combo : list){
					listaDiretoriaRegionalHash.put(combo.getCdUnidadeOrganizacional(), PgitUtil.concatenarCampos(combo.getCdUnidadeOrganizacional(), combo.getDsUnidadeOrganizacional()));
					listaDiretoriaRegional.add(new SelectItem(combo.getCdUnidadeOrganizacional().toString(),PgitUtil.concatenarCampos(combo.getCdUnidadeOrganizacional(), combo.getDsUnidadeOrganizacional())));
				}
			}else{
				listaDiretoriaRegional = new ArrayList<SelectItem>();
			}
		}catch(PdcAdapterFunctionalException e){
			listaDiretoriaRegional = new ArrayList<SelectItem>();
		}

		return listaDiretoriaRegional;
	}

	/**
	 * Set: listaDiretoriaRegional.
	 *
	 * @param listaDiretoriaRegional the lista diretoria regional
	 */
	public void setListaDiretoriaRegional(List<SelectItem> listaDiretoriaRegional) {
		this.listaDiretoriaRegional = listaDiretoriaRegional;
	}

	/**
	 * Get: itemCheckGerenciaRegional.
	 *
	 * @return itemCheckGerenciaRegional
	 */
	public Boolean getItemCheckGerenciaRegional() {
		return itemCheckGerenciaRegional;
	}

	/**
	 * Set: itemCheckGerenciaRegional.
	 *
	 * @param itemCheckGerenciaRegional the item check gerencia regional
	 */
	public void setItemCheckGerenciaRegional(Boolean itemCheckGerenciaRegional) {
		this.itemCheckGerenciaRegional = itemCheckGerenciaRegional;
	}

	/**
	 * Get: listaGerenciaRegional.
	 *
	 * @return listaGerenciaRegional
	 */
	public List<SelectItem> getListaGerenciaRegional() {
		
		if ( getEmpresa() != null && getDiretoriaRegional() != null && getEmpresa() != 0L && getDiretoriaRegional() != 0L){ 
			try{
			
				List<ListarDependenciaDiretoriaSaidaDTO> list = comboService.pesquisaGerencia(getEmpresa(), Long.parseLong(getDiretoriaRegional().toString()),Integer.parseInt("0"));
				
				listaGerenciaRegional.clear();
				listaGerenciaRegionalHash.clear();
				for(ListarDependenciaDiretoriaSaidaDTO combo : list){
					listaGerenciaRegional.add(new SelectItem(combo.getCdDependencia(),PgitUtil.concatenarCampos(combo.getCdDependencia(), combo.getDsDependencia())));
					listaGerenciaRegionalHash.put(combo.getCdDependencia(), PgitUtil.concatenarCampos(combo.getCdDependencia(), combo.getDsDependencia()));
				
				}	
			}catch(PdcAdapterFunctionalException p){
				BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
				listaGerenciaRegional = new ArrayList<SelectItem>();
			}
			
		
		}else{
			listaGerenciaRegional = new ArrayList<SelectItem>();
		}
		
		return listaGerenciaRegional;
	}

	/**
	 * Set: listaGerenciaRegional.
	 *
	 * @param listaGerenciaRegional the lista gerencia regional
	 */
	public void setListaGerenciaRegional(List<SelectItem> listaGerenciaRegional) {
		this.listaGerenciaRegional = listaGerenciaRegional;
	}

	/**
	 * Get: itemCheckAgenciaOperadora.
	 *
	 * @return itemCheckAgenciaOperadora
	 */
	public Boolean getItemCheckAgenciaOperadora() {
		return itemCheckAgenciaOperadora;
	}

	/**
	 * Set: itemCheckAgenciaOperadora.
	 *
	 * @param itemCheckAgenciaOperadora the item check agencia operadora
	 */
	public void setItemCheckAgenciaOperadora(Boolean itemCheckAgenciaOperadora) {
		this.itemCheckAgenciaOperadora = itemCheckAgenciaOperadora;
	}
	
	/**
	 * Limpar agencia operadora.
	 */
	public void limparAgenciaOperadora(){
		if (!getItemCheckAgenciaOperadora() || !getItemCheckGerenciaRegional() || getGerenciaRegional() == null || getGerenciaRegional() ==0){
			setAgenciaOperadora("");
			setDsAgencia("");
			setDsAgenciaAux("");
			setItemCheckAgenciaOperadora(false);
		}
	}
	
	/**
	 * Limpar gerencia regional.
	 */
	public void limparGerenciaRegional(){
		if (!getItemCheckGerenciaRegional()){
			setGerenciaRegional(0);			
			setAgenciaOperadora("");
			setDsAgencia("");
			setDsAgenciaAux("");
			setItemCheckAgenciaOperadora(false);
		}
	}
	
	/**
	 * Limpar diretoria regional.
	 */
	public void limparDiretoriaRegional(){
		if (!getItemCheckDiretoriaRegional()){
			setDiretoriaRegional(0);
			setItemCheckGerenciaRegional(false);
			getListaGerenciaRegional();
			setGerenciaRegional(0);
			setItemCheckAgenciaOperadora(false);
			setAgenciaOperadora("");
			setDsAgencia("");
			setDsAgenciaAux("");
		}
	}
	
	/**
	 * Limpar empresa.
	 */
	public void limparEmpresa(){
		if (!getItemCheckEmpresaConglomerado()){
			setEmpresa(0l);			
			getListaDiretoriaRegional();
			setDiretoriaRegional(0);
			getListaGerenciaRegional();
			setGerenciaRegional(0);
			setItemCheckDiretoriaRegional(false);
			setItemCheckGerenciaRegional(false);
			setItemCheckAgenciaOperadora(false);
			setAgenciaOperadora("");
			setDsAgencia("");
			setDsAgenciaAux("");
		}
	}
	
	/**
	 * Limpar criterio selecao contratos.
	 */
	public void limparCriterioSelecaoContratos(){
		
		if (getCriterioSelecaoContratos() != null){
			if (getCriterioSelecaoContratos() == 0){
				setModalidadeServico(0);				
			}else{
				setTipoServico(0);
			}
		}
		
	}

	/**
	 * Get: listaSegmento.
	 *
	 * @return listaSegmento
	 */
	public List<SelectItem> getListaSegmento() {
		try{
			List<EmpresaSegmentoSaidaDTO> list = comboService.pesquisaSegmento();
			
			listaSegmento = new ArrayList<SelectItem>();
			
			listaSegmentoHash.clear();
			listaSegmento.clear();
			for(EmpresaSegmentoSaidaDTO combo : list){
				listaSegmentoHash.put(combo.getCdSegmento(), combo.getDsSegmento());
				listaSegmento.add(new SelectItem(combo.getCdSegmento(),combo.getDsSegmento()));		
			}
		}catch(PdcAdapterFunctionalException e){
			listaSegmento = new ArrayList<SelectItem>();
		}
		return listaSegmento;
	}
	
	
	/**
	 * Carrega combo classe ramo.
	 */
	public void carregaComboClasseRamo() {
		try {
			listaCboClasseRamo = new ArrayList<SelectItem>();
			setClasseRamo(null);

			List<ListarClasseRamoAtvddSaidaDTO> lista = getComboService()
					.listarClasseRamoAtvdd();
			listaCboClasseRamoHash.clear();
			for (ListarClasseRamoAtvddSaidaDTO combo : lista) {
				listaCboClasseRamo.add(new SelectItem(combo
						.getCdRamoAtividade()
						+ "/" + combo.getCdClassAtividade(), combo
						.getClasseRamoFormatado()));
				listaCboClasseRamoHash.put(combo.getCdRamoAtividade() + "/"
						+ combo.getCdClassAtividade(), combo);
			}
		} catch (PdcAdapterFunctionalException p) {
			listaCboClasseRamo = new ArrayList<SelectItem>();
			setClasseRamo(null);
		}
	}
	
	/**
	 * Carrega combo sub ramo atividade.
	 */
	public void carregaComboSubRamoAtividade() {
		try {
			listaCboSubRamoAtividade = new ArrayList<SelectItem>();
			setSubRamoAtividadeEconomica(null);

			if (getClasseRamo() != null && !getClasseRamo().equals("0")) {
				ListarSramoAtvddEconcEntradaDTO entrada = new ListarSramoAtvddEconcEntradaDTO();
				entrada.setCdClassAtividade(listaCboClasseRamoHash.get(
						getClasseRamo()).getCdClassAtividade());
				entrada.setCdRamoAtividade(listaCboClasseRamoHash.get(
						getClasseRamo()).getCdRamoAtividade());

				List<ListarSramoAtvddEconcSaidaDTO> lista = getComboService()
						.listarSramoAtvddEconc(entrada);

				for (ListarSramoAtvddEconcSaidaDTO combo : lista) {
					listaCboSubRamoAtividade.add(new SelectItem(combo
							.getCdAtividadeEconomica() + "/" + combo.getCdSubRamoAtividade(), combo
							.getSubRamoAtividadeFormatado()));
					listaCboSubRamoAtividadeHash.put(combo
							.getCdAtividadeEconomica() + "/" + combo.getCdSubRamoAtividade(), combo);
				}
			}
		} catch (PdcAdapterFunctionalException p) {
			listaCboSubRamoAtividade = new ArrayList<SelectItem>();
			setSubRamoAtividadeEconomica(null);
		}
	}

	/**
	 * Set: listaSegmento.
	 *
	 * @param listaSegmento the lista segmento
	 */
	public void setListaSegmento(List<SelectItem> listaSegmento) {
		this.listaSegmento = listaSegmento;
	}
	
	/**
	 * Limpar segmento.
	 */
	public void limparSegmento(){
		if (!getItemCheckSegmento()){
			setSegmento(0);
		}
	}
	
	/**
	 * Limpar participante.
	 */
	public void limparParticipante(){
		if (!getItemCheckParticipanteGrupoEconomico()){
			setParticipante(null);
		}
	}
	

	/**
	 * Get: itemCheckSegmento.
	 *
	 * @return itemCheckSegmento
	 */
	public Boolean getItemCheckSegmento() {
		return itemCheckSegmento;
	}

	/**
	 * Set: itemCheckSegmento.
	 *
	 * @param itemCheckSegmento the item check segmento
	 */
	public void setItemCheckSegmento(Boolean itemCheckSegmento) {
		this.itemCheckSegmento = itemCheckSegmento;
	}

	/**
	 * Get: itemCheckParticipanteGrupoEconomico.
	 *
	 * @return itemCheckParticipanteGrupoEconomico
	 */
	public Boolean getItemCheckParticipanteGrupoEconomico() {
		return itemCheckParticipanteGrupoEconomico;
	}

	/**
	 * Set: itemCheckParticipanteGrupoEconomico.
	 *
	 * @param itemCheckParticipanteGrupoEconomico the item check participante grupo economico
	 */
	public void setItemCheckParticipanteGrupoEconomico(
			Boolean itemCheckParticipanteGrupoEconomico) {
		this.itemCheckParticipanteGrupoEconomico = itemCheckParticipanteGrupoEconomico;
	}

	
	/**
	 * Preenche lista modalidade.
	 */
	public void preencheListaModalidade(){
		getListaModalidadeServico();
	}
	
	
	/**
	 * Preenche lista diretoria regional.
	 */
	public void preencheListaDiretoriaRegional(){
		getListaDiretoriaRegional();
	}
	
	/**
	 * Preenche lista gerencia regional.
	 */
	public void preencheListaGerenciaRegional(){
		getListaGerenciaRegional();
	}
	
	/**
	 * Limpar tipo servico.
	 */
	public void limparTipoServico(){
		if (!getItemCheckTipoServico()){
			setTipoServico(0);
			preencheListaModalidade();
			setItemCheckModalidadeServico(false);			
		}
	}

	/**
	 * Get: itemCheckTipoServico.
	 *
	 * @return itemCheckTipoServico
	 */
	public Boolean getItemCheckTipoServico() {
		return itemCheckTipoServico;
	}

	/**
	 * Set: itemCheckTipoServico.
	 *
	 * @param itemCheckTipoServico the item check tipo servico
	 */
	public void setItemCheckTipoServico(Boolean itemCheckTipoServico) {
		this.itemCheckTipoServico = itemCheckTipoServico;
	}

	/**
	 * Get: itemCheckModalidadeServico.
	 *
	 * @return itemCheckModalidadeServico
	 */
	public Boolean getItemCheckModalidadeServico() {
		return itemCheckModalidadeServico;
	}

	/**
	 * Set: itemCheckModalidadeServico.
	 *
	 * @param itemCheckModalidadeServico the item check modalidade servico
	 */
	public void setItemCheckModalidadeServico(Boolean itemCheckModalidadeServico) {
		this.itemCheckModalidadeServico = itemCheckModalidadeServico;
	}
	
	/**
	 * Limpar modalidade servico.
	 */
	public void limparModalidadeServico(){
		if (!getItemCheckModalidadeServico()){
			setModalidadeServico(0);
		}
	}
	
	/**
	 * Limpar classe.
	 */
	public void limparClasse(){
		if (!getItemCheckClasse()){
			setClasseRamo("0");			
			setSubRamoAtividadeEconomica("0");
			setListaCboSubRamoAtividade(new ArrayList<SelectItem>());			
		}
	}
	

	

	/**
	 * Get: classeRamo.
	 *
	 * @return classeRamo
	 */
	public String getClasseRamo() {
		return classeRamo;
	}

	/**
	 * Set: classeRamo.
	 *
	 * @param classe the classe ramo
	 */
	public void setClasseRamo(String classe) {
		this.classeRamo = classe;
	}

	
	/**
	 * Get: itemCheckClasse.
	 *
	 * @return itemCheckClasse
	 */
	public Boolean getItemCheckClasse() {
		return itemCheckClasse;
	}

	/**
	 * Set: itemCheckClasse.
	 *
	 * @param itemCheckClasse the item check classe
	 */
	public void setItemCheckClasse(Boolean itemCheckClasse) {
		this.itemCheckClasse = itemCheckClasse;
	}


	/**
	 * Get: subRamoAtividadeEconomica.
	 *
	 * @return subRamoAtividadeEconomica
	 */
	public String getSubRamoAtividadeEconomica() {
		return subRamoAtividadeEconomica;
	}

	/**
	 * Set: subRamoAtividadeEconomica.
	 *
	 * @param subRamo the sub ramo atividade economica
	 */
	public void setSubRamoAtividadeEconomica(String subRamo) {
		this.subRamoAtividadeEconomica = subRamo;
	}

	/**
	 * Get: hiddenObrigatoriedade.
	 *
	 * @return hiddenObrigatoriedade
	 */
	public String getHiddenObrigatoriedade() {
		return hiddenObrigatoriedade;
	}

	/**
	 * Set: hiddenObrigatoriedade.
	 *
	 * @param hiddenObrigatoriedade the hidden obrigatoriedade
	 */
	public void setHiddenObrigatoriedade(String hiddenObrigatoriedade) {
		this.hiddenObrigatoriedade = hiddenObrigatoriedade;
	}

	/**
	 * Get: diretoriaRegionalDesc.
	 *
	 * @return diretoriaRegionalDesc
	 */
	public String getDiretoriaRegionalDesc() {
		return diretoriaRegionalDesc;
	}

	/**
	 * Set: diretoriaRegionalDesc.
	 *
	 * @param diretoriaRegionalDesc the diretoria regional desc
	 */
	public void setDiretoriaRegionalDesc(String diretoriaRegionalDesc) {
		this.diretoriaRegionalDesc = diretoriaRegionalDesc;
	}

	/**
	 * Get: empresaDesc.
	 *
	 * @return empresaDesc
	 */
	public String getEmpresaDesc() {
		return empresaDesc;
	}

	/**
	 * Set: empresaDesc.
	 *
	 * @param empresaDesc the empresa desc
	 */
	public void setEmpresaDesc(String empresaDesc) {
		this.empresaDesc = empresaDesc;
	}

	/**
	 * Get: gerenciaRegionalDesc.
	 *
	 * @return gerenciaRegionalDesc
	 */
	public String getGerenciaRegionalDesc() {
		return gerenciaRegionalDesc;
	}

	/**
	 * Set: gerenciaRegionalDesc.
	 *
	 * @param gerenciaRegionalDesc the gerencia regional desc
	 */
	public void setGerenciaRegionalDesc(String gerenciaRegionalDesc) {
		this.gerenciaRegionalDesc = gerenciaRegionalDesc;
	}

	/**
	 * Get: modalidadeServiceDesc.
	 *
	 * @return modalidadeServiceDesc
	 */
	public String getModalidadeServiceDesc() {
		return modalidadeServiceDesc;
	}

	/**
	 * Set: modalidadeServiceDesc.
	 *
	 * @param modalidadeServiceDesc the modalidade service desc
	 */
	public void setModalidadeServiceDesc(String modalidadeServiceDesc) {
		this.modalidadeServiceDesc = modalidadeServiceDesc;
	}

	/**
	 * Get: segmentoDesc.
	 *
	 * @return segmentoDesc
	 */
	public String getSegmentoDesc() {
		return segmentoDesc;
	}

	/**
	 * Set: segmentoDesc.
	 *
	 * @param segmentoDesc the segmento desc
	 */
	public void setSegmentoDesc(String segmentoDesc) {
		this.segmentoDesc = segmentoDesc;
	}

	/**
	 * Get: tipoServicoDesc.
	 *
	 * @return tipoServicoDesc
	 */
	public String getTipoServicoDesc() {
		return tipoServicoDesc;
	}

	/**
	 * Set: tipoServicoDesc.
	 *
	 * @param tipoServicoDesc the tipo servico desc
	 */
	public void setTipoServicoDesc(String tipoServicoDesc) {
		this.tipoServicoDesc = tipoServicoDesc;
	}
	
	
	/**
	 * Confirmar incluir.
	 *
	 * @return the string
	 */
	public String confirmarIncluir(){
		
		try {
			IncluirSolRelContratosEntradaDTO entradaDTO = new IncluirSolRelContratosEntradaDTO();
					
			entradaDTO.setCdTipoServico(getItemCheckTipoServico()?getTipoServico():0);
			entradaDTO.setCdModalidadeServico(getItemCheckModalidadeServico()?getModalidadeServico():0);
			entradaDTO.setCdTipoRelacionamento(getItemCheckModalidadeServico()? listaModalidadeServicoHash.get(getModalidadeServico()).getCdRelacionamentoProduto():0);			
			entradaDTO.setCdDiretoriaRegional(getItemCheckDiretoriaRegional()? getDiretoriaRegional() :0);			
			entradaDTO.setCdGerenciaRegional(getItemCheckGerenciaRegional()? getGerenciaRegional():0);			
			entradaDTO.setCdEmpresaConglomeradoAgenciaOperadora(getItemCheckAgenciaOperadora()  ? getEmpresa() : 0);
			entradaDTO.setCdEmpresaConglomeradoDiretoriaRegional(getItemCheckDiretoriaRegional() ? getEmpresa() : 0);
			entradaDTO.setCdEmpresaConglomeradoGerenciaRegional(getItemCheckGerenciaRegional()  ? getEmpresa() : 0);
			entradaDTO.setCdAgenciaOperadora(getItemCheckAgenciaOperadora() ? PgitUtil.verificaStringToIntegerNula(getAgenciaOperadora()) :0);			
			entradaDTO.setTipoRelatorio(getTipoRelatorio());			
			entradaDTO.setCdSegmento(getItemCheckSegmento() ? getSegmento() : 0);
			entradaDTO.setCdParticipanteGrupoEconomico(getItemCheckParticipanteGrupoEconomico() ? getParticipante() : 0l);
			
			entradaDTO.setCdClassAtividadeEconomica(getItemCheckClasse() && getClasseRamo() != null && !getClasseRamo().equals("0")? listaCboClasseRamoHash.get(getClasseRamo()).getCdClassAtividade() : "");
			entradaDTO.setCdRamoAtividadeEconomica(getItemCheckClasse() && getClasseRamo() != null && !getClasseRamo().equals("0")? listaCboClasseRamoHash.get(getClasseRamo()).getCdRamoAtividade() : 0 );
			entradaDTO.setCdSubRamoAtividadeEconomica(getItemCheckClasse() && getSubRamoAtividadeEconomica() != null && !getSubRamoAtividadeEconomica().equals("0") ? listaCboSubRamoAtividadeHash.get(getSubRamoAtividadeEconomica()).getCdSubRamoAtividade() : 0);
			entradaDTO.setCdAtividadeEconomica( getItemCheckClasse() && getSubRamoAtividadeEconomica() != null && !getSubRamoAtividadeEconomica().equals("0") ?  listaCboSubRamoAtividadeHash.get(getSubRamoAtividadeEconomica()).getCdAtividadeEconomica() : 0);
			
			IncluirSolRelContratosSaidaDTO saidaDTO = getManterSolicitacaoRelatorioContratoImpl().solicitarRelatorioContratos(entradaDTO);
			
			BradescoFacesUtils.addInfoModalMessage("(" +saidaDTO.getCodMensagem() + ") " + saidaDTO.getMensagem(), CON_MANTER_SOLICITACAO_REL_CONTRATO, BradescoViewExceptionActionType.ACTION, false);
			
			if(getListaGrid() != null && getListaGrid().size() > 0){
			    carregaGrid();
			}
			
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			return null;
		}
		return "";
	}
	
	/**
	 * Consultar.
	 *
	 * @param evt the evt
	 */
	public void consultar(ActionEvent evt){
	    carregaGrid();
	}
	
	/**
	 * Confirmar excluir.
	 *
	 * @return the string
	 */
	public String confirmarExcluir(){
		
		try {
			CancelarSolRelContratosEntradaDTO entradaDTO = new CancelarSolRelContratosEntradaDTO();
			
			ListarSolRelContratosSaidaDTO listarSolRelContratosSaidaDTO = getListaGrid().get(getItemSelecionadoLista());
				
		
			entradaDTO.setCdSolicitacaoPagamentoIntegrado(listarSolRelContratosSaidaDTO.getTipoSolicitacao());
			entradaDTO.setNrSolicitacaoPagamentoIntegrado(listarSolRelContratosSaidaDTO.getSolicitacao());
			entradaDTO.setCdTipoRelatorio(getTipoRelatorio());
			entradaDTO.setDataFinal("");
			entradaDTO.setDataInicial("");
			
			
			CancelarSolRelContratosSaidaDTO saidaDTO = getManterSolicitacaoRelatorioContratoImpl().cancelarRelatorioContratos(entradaDTO);
			
			    
			BradescoFacesUtils.addInfoModalMessage("(" + saidaDTO.getCodMensagem() + ") " + saidaDTO.getMensagem(), "conManterSolicitacaoRelContrato",
				    "#{manterSolicitacaoRelatorioContratoBean.consultar}", false);				
			
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			return null;
		}
		return "";
	}
	
	/**
	 * Iniciar tela.
	 *
	 * @param evt the evt
	 */
	public void iniciarTela(ActionEvent evt){
		setFiltroDataAte(null);
		setFiltroDataDe(null);
		setFiltroTipoRelatorio(0);
		setListaGrid(null);
		setBtoAcionado(false);
	}

	/**
	 * Get: listaDiretoriaRegionalHash.
	 *
	 * @return listaDiretoriaRegionalHash
	 */
	Map getListaDiretoriaRegionalHash() {
		return listaDiretoriaRegionalHash;
	}

	/**
	 * Set: listaDiretoriaRegionalHash.
	 *
	 * @param listaDiretoriaRegionalHash the lista diretoria regional hash
	 */
	void setListaDiretoriaRegionalHash(Map listaDiretoriaRegionalHash) {
		this.listaDiretoriaRegionalHash = listaDiretoriaRegionalHash;
	}

	/**
	 * Get: listaEmpresaConglomeradoHash.
	 *
	 * @return listaEmpresaConglomeradoHash
	 */
	Map<Long, String> getListaEmpresaConglomeradoHash() {
		return listaEmpresaConglomeradoHash;
	}

	/**
	 * Set lista empresa conglomerado hash.
	 *
	 * @param listaEmpresaConglomeradoHash the lista empresa conglomerado hash
	 */
	void setListaEmpresaConglomeradoHash(
			Map<Long, String> listaEmpresaConglomeradoHash) {
		this.listaEmpresaConglomeradoHash = listaEmpresaConglomeradoHash;
	}

	/**
	 * Get: listaGerenciaRegionalHash.
	 *
	 * @return listaGerenciaRegionalHash
	 */
	Map<Integer, String> getListaGerenciaRegionalHash() {
		return listaGerenciaRegionalHash;
	}

	/**
	 * Set lista gerencia regional hash.
	 *
	 * @param listaGerenciaRegionalHash the lista gerencia regional hash
	 */
	void setListaGerenciaRegionalHash(Map<Integer, String> listaGerenciaRegionalHash) {
		this.listaGerenciaRegionalHash = listaGerenciaRegionalHash;
	}

	/**
	 * Get: listaSegmentoHash.
	 *
	 * @return listaSegmentoHash
	 */
	Map<Integer, String> getListaSegmentoHash() {
		return listaSegmentoHash;
	}

	/**
	 * Set lista segmento hash.
	 *
	 * @param listaSegmentoHash the lista segmento hash
	 */
	void setListaSegmentoHash(Map<Integer, String> listaSegmentoHash) {
		this.listaSegmentoHash = listaSegmentoHash;
	}

	/**
	 * Get: participante.
	 *
	 * @return participante
	 */
	public Long getParticipante() {
		return participante;
	}

	/**
	 * Set: participante.
	 *
	 * @param participante the participante
	 */
	public void setParticipante(Long participante) {
		this.participante = participante;
	}

	/**
	 * Preenche dados.
	 */
	private void preencheDados(){
		
		ListarSolRelContratosSaidaDTO listarSolRelContratosSaidaDTO = getListaGrid().get(getItemSelecionadoLista());
		
		DetalharSolRelContratosEntradaDTO detalharSolRelContratosEntradaDTO = new DetalharSolRelContratosEntradaDTO();
		
		detalharSolRelContratosEntradaDTO.setCdSolicitacaoPagamentoIntegrado(listarSolRelContratosSaidaDTO.getTipoSolicitacao());
		detalharSolRelContratosEntradaDTO.setNrSolicitacaoPagamentoIntegrado(listarSolRelContratosSaidaDTO.getSolicitacao());
		detalharSolRelContratosEntradaDTO.setTipoRelatorio(0);
		
		
		DetalharSolRelContratosSaidaDTO detalharSolRelContratosSaidaDTO = getManterSolicitacaoRelatorioContratoImpl().detalharSolicitacaoRelatorioContratos(detalharSolRelContratosEntradaDTO);
		
		setTipoServicoDesc(detalharSolRelContratosSaidaDTO.getCdTipoServico() !=0? detalharSolRelContratosSaidaDTO.getDsTipoServico():"");
		setModalidadeServiceDesc(detalharSolRelContratosSaidaDTO.getCdModalidadeServico() != 0? detalharSolRelContratosSaidaDTO.getDsModalidadeServico():"");
		
		String descEmpresa="";
		
//		if (detalharSolRelContratosSaidaDTO.getCdEmpresaConglomeradoDiretoriaRegional() != 0l){
//			descEmpresa = detalharSolRelContratosSaidaDTO.getDsEmpresaConglomeradoGerenciaRegional();
//		}else{
//			if (detalharSolRelContratosSaidaDTO.getCdEmpresaConglomeradoGerenciaRegional() != 0l){
//				descEmpresa = detalharSolRelContratosSaidaDTO.getDsEmpresaConglomeradoDiretoriaRegional();
//			}else{
//				if (detalharSolRelContratosSaidaDTO.getCdEmpresaConglomeradoAgenciaOperadora() != 0l){
//					descEmpresa = detalharSolRelContratosSaidaDTO.getDsEmpresaConglomeradoAgenciaOperadora();
//				}	
//			}
//			
//		}
		
		descEmpresa = detalharSolRelContratosSaidaDTO.getDsEmpresa();
		
		
		setEmpresaDesc(detalharSolRelContratosSaidaDTO.getCdEmpresaConglomeradoAgenciaOperadora() + " - " + descEmpresa);
		setDiretoriaRegionalDesc(detalharSolRelContratosSaidaDTO.getCdDiretoriaRegional() != 0 ? detalharSolRelContratosSaidaDTO.getCdDiretoriaRegional()  + " - "  +  detalharSolRelContratosSaidaDTO.getDsEmpresaConglomeradoDiretoriaRegional() : "");
		setGerenciaRegionalDesc(detalharSolRelContratosSaidaDTO.getCdEmpresaConglomeradoGerenciaRegional() != 0l?detalharSolRelContratosSaidaDTO.getCdGerenciaRegional() + " - " + detalharSolRelContratosSaidaDTO.getDsGerenciaRegional():"");		
		setAgenciaOperadora(detalharSolRelContratosSaidaDTO.getCdAgenciaOperadora()!=0?detalharSolRelContratosSaidaDTO.getCdAgenciaOperadora() + " - " + detalharSolRelContratosSaidaDTO.getDsAgenciaOperadora():"" );
		
		setSegmentoDesc(detalharSolRelContratosSaidaDTO.getCdSegmento() !=0 ?detalharSolRelContratosSaidaDTO.getDsSegmento():"");
		setParticipante(detalharSolRelContratosSaidaDTO.getCdParticipanteGrupoEconomico() !=0L ? detalharSolRelContratosSaidaDTO.getCdParticipanteGrupoEconomico() : null);
		setParticipanteDesc(detalharSolRelContratosSaidaDTO.getCdParticipanteGrupoEconomico()==0l?"":String.valueOf(detalharSolRelContratosSaidaDTO.getDsParticipanteGrupoEconomico()));
		
		setClasseRamo(detalharSolRelContratosSaidaDTO.getCdClassAtividadeEconomica() + "/" + detalharSolRelContratosSaidaDTO.getCdRamoAtividadeEconomica());
		setClasseRamoDesc(PgitUtil.concatenarCampos(detalharSolRelContratosSaidaDTO.getCdClassAtividadeEconomica(), PgitUtil.concatenarCampos(detalharSolRelContratosSaidaDTO.getCdRamoAtividadeEconomica(), detalharSolRelContratosSaidaDTO.getDsSubRamo()), "/"));
		setSubRamoAtividadeEconomica(PgitUtil.concatenarCampos(detalharSolRelContratosSaidaDTO.getCdSubRamoAtividadeEconomica(), PgitUtil.concatenarCampos(detalharSolRelContratosSaidaDTO.getCdAtividadeEconomica(), detalharSolRelContratosSaidaDTO.getDsAtividadeEconomica()), "/"));
		setSubRamoAtividadeEconomicaDesc(detalharSolRelContratosSaidaDTO.getDsSubRamo());
		
		setDataHoraSolicitacao(detalharSolRelContratosSaidaDTO.getDataHoraSolicitacao());
		setTipoRelatorio(detalharSolRelContratosSaidaDTO.getTipoRelatorio());
		//setTipoRelatorioDesc(detalharSolRelContratosSaidaDTO.get)
		setDataHoraInclusao(detalharSolRelContratosSaidaDTO.getDataHoraInclusao());
		setDataHoraManutencao(detalharSolRelContratosSaidaDTO.getDataHoraManutencao());
		setUsuarioInclusao(detalharSolRelContratosSaidaDTO.getUsuarioInclusao());
		setUsuarioManutencao(detalharSolRelContratosSaidaDTO.getUsuarioManutencao());
		if (detalharSolRelContratosSaidaDTO.getCdCanalInclusao() != 0 ){
			setTipoCanalInclusao(detalharSolRelContratosSaidaDTO.getCdCanalInclusao() + " - " + detalharSolRelContratosSaidaDTO.getDsCanalInclusao());	
		}else{
			setTipoCanalInclusao("");
		}
		
		if (detalharSolRelContratosSaidaDTO.getCdCanalManutencao() != 0){
			setTipoCanalManutencao(detalharSolRelContratosSaidaDTO.getCdCanalManutencao() + " - " + detalharSolRelContratosSaidaDTO.getDsCanalManutencao());
		}
		setComplementoInclusao(detalharSolRelContratosSaidaDTO.getComplementoInclusao());
		setComplementoManutencao(detalharSolRelContratosSaidaDTO.getComplementoManutencao());
		
	}

	/**
	 * Get: dataHoraSolicitacao.
	 *
	 * @return dataHoraSolicitacao
	 */
	public String getDataHoraSolicitacao() {
		return dataHoraSolicitacao;
	}

	/**
	 * Set: dataHoraSolicitacao.
	 *
	 * @param dataHoraSolicitacao the data hora solicitacao
	 */
	public void setDataHoraSolicitacao(String dataHoraSolicitacao) {
		this.dataHoraSolicitacao = dataHoraSolicitacao;
	}

	/**
	 * Get: complementoInclusao.
	 *
	 * @return complementoInclusao
	 */
	public String getComplementoInclusao() {
		return complementoInclusao;
	}

	/**
	 * Set: complementoInclusao.
	 *
	 * @param complementoInclusao the complemento inclusao
	 */
	public void setComplementoInclusao(String complementoInclusao) {
		this.complementoInclusao = complementoInclusao;
	}

	/**
	 * Get: complementoManutencao.
	 *
	 * @return complementoManutencao
	 */
	public String getComplementoManutencao() {
		return complementoManutencao;
	}

	/**
	 * Set: complementoManutencao.
	 *
	 * @param complementoManutencao the complemento manutencao
	 */
	public void setComplementoManutencao(String complementoManutencao) {
		this.complementoManutencao = complementoManutencao;
	}

	/**
	 * Get: dataHoraInclusao.
	 *
	 * @return dataHoraInclusao
	 */
	public String getDataHoraInclusao() {
		return dataHoraInclusao;
	}

	/**
	 * Set: dataHoraInclusao.
	 *
	 * @param dataHoraInclusao the data hora inclusao
	 */
	public void setDataHoraInclusao(String dataHoraInclusao) {
		this.dataHoraInclusao = dataHoraInclusao;
	}

	/**
	 * Get: dataHoraManutencao.
	 *
	 * @return dataHoraManutencao
	 */
	public String getDataHoraManutencao() {
		return dataHoraManutencao;
	}

	/**
	 * Set: dataHoraManutencao.
	 *
	 * @param dataHoraManutencao the data hora manutencao
	 */
	public void setDataHoraManutencao(String dataHoraManutencao) {
		this.dataHoraManutencao = dataHoraManutencao;
	}

	/**
	 * Get: tipoCanalInclusao.
	 *
	 * @return tipoCanalInclusao
	 */
	public String getTipoCanalInclusao() {
		return tipoCanalInclusao;
	}

	/**
	 * Set: tipoCanalInclusao.
	 *
	 * @param tipoCanalInclusao the tipo canal inclusao
	 */
	public void setTipoCanalInclusao(String tipoCanalInclusao) {
		this.tipoCanalInclusao = tipoCanalInclusao;
	}

	/**
	 * Get: tipoCanalManutencao.
	 *
	 * @return tipoCanalManutencao
	 */
	public String getTipoCanalManutencao() {
		return tipoCanalManutencao;
	}

	/**
	 * Set: tipoCanalManutencao.
	 *
	 * @param tipoCanalManutencao the tipo canal manutencao
	 */
	public void setTipoCanalManutencao(String tipoCanalManutencao) {
		this.tipoCanalManutencao = tipoCanalManutencao;
	}

	/**
	 * Get: usuarioInclusao.
	 *
	 * @return usuarioInclusao
	 */
	public String getUsuarioInclusao() {
		return usuarioInclusao;
	}

	/**
	 * Set: usuarioInclusao.
	 *
	 * @param usuarioInclusao the usuario inclusao
	 */
	public void setUsuarioInclusao(String usuarioInclusao) {
		this.usuarioInclusao = usuarioInclusao;
	}

	/**
	 * Get: usuarioManutencao.
	 *
	 * @return usuarioManutencao
	 */
	public String getUsuarioManutencao() {
		return usuarioManutencao;
	}

	/**
	 * Set: usuarioManutencao.
	 *
	 * @param usuarioManutencao the usuario manutencao
	 */
	public void setUsuarioManutencao(String usuarioManutencao) {
		this.usuarioManutencao = usuarioManutencao;
	}
	
	/**
	 * Pesquisar.
	 *
	 * @return the string
	 */
	public String pesquisar(){
		return "";
	}

	/**
	 * Get: participanteDesc.
	 *
	 * @return participanteDesc
	 */
	public String getParticipanteDesc() {
		return participanteDesc;
	}

	/**
	 * Set: participanteDesc.
	 *
	 * @param participanteDesc the participante desc
	 */
	public void setParticipanteDesc(String participanteDesc) {
		this.participanteDesc = participanteDesc;
	}

	/**
	 * Get: btoAcionado.
	 *
	 * @return btoAcionado
	 */
	public Boolean getBtoAcionado() {
		return btoAcionado;
	}

	/**
	 * Set: btoAcionado.
	 *
	 * @param btoAcionado the bto acionado
	 */
	public void setBtoAcionado(Boolean btoAcionado) {
		this.btoAcionado = btoAcionado;
	}

	/**
	 * Get: listaCboClasseRamo.
	 *
	 * @return listaCboClasseRamo
	 */
	public List<SelectItem> getListaCboClasseRamo() {
		return listaCboClasseRamo;
	}

	/**
	 * Set: listaCboClasseRamo.
	 *
	 * @param listaCboClasseRamo the lista cbo classe ramo
	 */
	public void setListaCboClasseRamo(List<SelectItem> listaCboClasseRamo) {
		this.listaCboClasseRamo = listaCboClasseRamo;
	}

	/**
	 * Get: listaCboSubRamoAtividade.
	 *
	 * @return listaCboSubRamoAtividade
	 */
	public List<SelectItem> getListaCboSubRamoAtividade() {
		return listaCboSubRamoAtividade;
	}

	/**
	 * Set: listaCboSubRamoAtividade.
	 *
	 * @param listaCboSubRamoAtividade the lista cbo sub ramo atividade
	 */
	public void setListaCboSubRamoAtividade(
			List<SelectItem> listaCboSubRamoAtividade) {
		this.listaCboSubRamoAtividade = listaCboSubRamoAtividade;
	}

	/**
	 * Get: classeRamoDesc.
	 *
	 * @return classeRamoDesc
	 */
	public String getClasseRamoDesc() {
		return classeRamoDesc;
	}

	/**
	 * Set: classeRamoDesc.
	 *
	 * @param classeRamoDesc the classe ramo desc
	 */
	public void setClasseRamoDesc(String classeRamoDesc) {
		this.classeRamoDesc = classeRamoDesc;
	}

	/**
	 * Get: subRamoAtividadeEconomicaDesc.
	 *
	 * @return subRamoAtividadeEconomicaDesc
	 */
	public String getSubRamoAtividadeEconomicaDesc() {
		return subRamoAtividadeEconomicaDesc;
	}

	/**
	 * Set: subRamoAtividadeEconomicaDesc.
	 *
	 * @param subRamoAtividadeEconomicaDesc the sub ramo atividade economica desc
	 */
	public void setSubRamoAtividadeEconomicaDesc(
			String subRamoAtividadeEconomicaDesc) {
		this.subRamoAtividadeEconomicaDesc = subRamoAtividadeEconomicaDesc;
	}

	/**
	 * Get: tipoRelatorioDesc.
	 *
	 * @return tipoRelatorioDesc
	 */
	public String getTipoRelatorioDesc() {
		return tipoRelatorioDesc;
	}

	/**
	 * Set: tipoRelatorioDesc.
	 *
	 * @param tipoRelatorioDesc the tipo relatorio desc
	 */
	public void setTipoRelatorioDesc(String tipoRelatorioDesc) {
		this.tipoRelatorioDesc = tipoRelatorioDesc;
	}

	/**
	 * Get: dsAgencia.
	 *
	 * @return dsAgencia
	 */
	public String getDsAgencia() {
	    return dsAgencia;
	}

	/**
	 * Set: dsAgencia.
	 *
	 * @param dsAgencia the ds agencia
	 */
	public void setDsAgencia(String dsAgencia) {
	    this.dsAgencia = dsAgencia;
	}

	/**
	 * Get: manterCadContaDestinoService.
	 *
	 * @return manterCadContaDestinoService
	 */
	public IManterCadContaDestinoService getManterCadContaDestinoService() {
	    return manterCadContaDestinoService;
	}

	/**
	 * Set: manterCadContaDestinoService.
	 *
	 * @param manterCadContaDestinoService the manter cad conta destino service
	 */
	public void setManterCadContaDestinoService(IManterCadContaDestinoService manterCadContaDestinoService) {
	    this.manterCadContaDestinoService = manterCadContaDestinoService;
	}

	/**
	 * Get: agenciaDigitada.
	 *
	 * @return agenciaDigitada
	 */
	public String getAgenciaDigitada() {
	    return agenciaDigitada;
	}

	/**
	 * Set: agenciaDigitada.
	 *
	 * @param agenciaDigitada the agencia digitada
	 */
	public void setAgenciaDigitada(String agenciaDigitada) {
	    this.agenciaDigitada = agenciaDigitada;
	}

	/**
	 * Get: saidaGrupoEconomico.
	 *
	 * @return saidaGrupoEconomico
	 */
	public ConsultarDescGrupoEconomicoSaidaDTO getSaidaGrupoEconomico() {
	    return saidaGrupoEconomico;
	}

	/**
	 * Set: saidaGrupoEconomico.
	 *
	 * @param saidaGrupoEconomico the saida grupo economico
	 */
	public void setSaidaGrupoEconomico(ConsultarDescGrupoEconomicoSaidaDTO saidaGrupoEconomico) {
	    this.saidaGrupoEconomico = saidaGrupoEconomico;
	}

	/**
	 * Get: consultasService.
	 *
	 * @return consultasService
	 */
	public IConsultasService getConsultasService() {
	    return consultasService;
	}

	/**
	 * Set: consultasService.
	 *
	 * @param consultasService the consultas service
	 */
	public void setConsultasService(IConsultasService consultasService) {
	    this.consultasService = consultasService;
	}

	/**
	 * Get: dsAgenciaAux.
	 *
	 * @return dsAgenciaAux
	 */
	public String getDsAgenciaAux() {
		return dsAgenciaAux;
	}

	/**
	 * Set: dsAgenciaAux.
	 *
	 * @param dsAgenciaAux the ds agencia aux
	 */
	public void setDsAgenciaAux(String dsAgenciaAux) {
		this.dsAgenciaAux = dsAgenciaAux;
	}
	
}