/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantervinculacaoconveniocontasalario.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantervinculacaoconveniocontasalario.bean;

/**
 * Nome: IncluirVincConvnCtaSalarioSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class IncluirVincConvnCtaSalarioSaidaDTO {

	/** Atributo codMensagem. */
	private String codMensagem;
	
	/** Atributo mensagem. */
	private String mensagem;

	/**
	 * Incluir vinc convn cta salario saida dto.
	 */
	public IncluirVincConvnCtaSalarioSaidaDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * Incluir vinc convn cta salario saida dto.
	 *
	 * @param codMensagem the cod mensagem
	 * @param mensagem the mensagem
	 */
	public IncluirVincConvnCtaSalarioSaidaDTO(String codMensagem, String mensagem) {
		super();
		this.codMensagem = codMensagem;
		this.mensagem = mensagem;
	}

	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}

	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}

	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}

	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
}