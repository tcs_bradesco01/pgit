/*
 * Nome: br.com.bradesco.web.pgit.service.business.combo.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.combo.bean;

/**
 * Nome: TipoLayoutArquivoEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class TipoLayoutArquivoEntradaDTO {

	/** Atributo cdSituacaoVinculacaoConta. */
	private int cdSituacaoVinculacaoConta;
	
	/** Atributo cdSerie. */
	private int cdSerie;

	/**
	 * Get: cdSituacaoVinculacaoConta.
	 *
	 * @return cdSituacaoVinculacaoConta
	 */
	public int getCdSituacaoVinculacaoConta() {
		return cdSituacaoVinculacaoConta;
	}
	
	/**
	 * Set: cdSituacaoVinculacaoConta.
	 *
	 * @param cdSituacaoVinculacaoConta the cd situacao vinculacao conta
	 */
	public void setCdSituacaoVinculacaoConta(int cdSituacaoVinculacaoConta) {
		this.cdSituacaoVinculacaoConta = cdSituacaoVinculacaoConta;
	}
	
	/**
	 * Get: cdSerie.
	 *
	 * @return cdSerie
	 */
	public int getCdSerie() {
		return cdSerie;
	}
	
	/**
	 * Set: cdSerie.
	 *
	 * @param cdSerie the cd serie
	 */
	public void setCdSerie(int cdSerie) {
		this.cdSerie = cdSerie;
	}
}