/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultarcontratoclientefiltro.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ConsultarContratoClienteFiltroRequest.
 * 
 * @version $Revision$ $Date$
 */
public class ConsultarContratoClienteFiltroRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _numeroOcorrencias
     */
    private int _numeroOcorrencias = 0;

    /**
     * keeps track of state for field: _numeroOcorrencias
     */
    private boolean _has_numeroOcorrencias;

    /**
     * Field _cdClubPessoa
     */
    private long _cdClubPessoa = 0;

    /**
     * keeps track of state for field: _cdClubPessoa
     */
    private boolean _has_cdClubPessoa;

    /**
     * Field _cdCpfCnpjPssoa
     */
    private long _cdCpfCnpjPssoa = 0;

    /**
     * keeps track of state for field: _cdCpfCnpjPssoa
     */
    private boolean _has_cdCpfCnpjPssoa;

    /**
     * Field _cdFilialCnpjPssoa
     */
    private int _cdFilialCnpjPssoa = 0;

    /**
     * keeps track of state for field: _cdFilialCnpjPssoa
     */
    private boolean _has_cdFilialCnpjPssoa;

    /**
     * Field _cdControleCpfPssoa
     */
    private int _cdControleCpfPssoa = 0;

    /**
     * keeps track of state for field: _cdControleCpfPssoa
     */
    private boolean _has_cdControleCpfPssoa;


      //----------------/
     //- Constructors -/
    //----------------/

    public ConsultarContratoClienteFiltroRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarcontratoclientefiltro.request.ConsultarContratoClienteFiltroRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdClubPessoa
     * 
     */
    public void deleteCdClubPessoa()
    {
        this._has_cdClubPessoa= false;
    } //-- void deleteCdClubPessoa() 

    /**
     * Method deleteCdControleCpfPssoa
     * 
     */
    public void deleteCdControleCpfPssoa()
    {
        this._has_cdControleCpfPssoa= false;
    } //-- void deleteCdControleCpfPssoa() 

    /**
     * Method deleteCdCpfCnpjPssoa
     * 
     */
    public void deleteCdCpfCnpjPssoa()
    {
        this._has_cdCpfCnpjPssoa= false;
    } //-- void deleteCdCpfCnpjPssoa() 

    /**
     * Method deleteCdFilialCnpjPssoa
     * 
     */
    public void deleteCdFilialCnpjPssoa()
    {
        this._has_cdFilialCnpjPssoa= false;
    } //-- void deleteCdFilialCnpjPssoa() 

    /**
     * Method deleteNumeroOcorrencias
     * 
     */
    public void deleteNumeroOcorrencias()
    {
        this._has_numeroOcorrencias= false;
    } //-- void deleteNumeroOcorrencias() 

    /**
     * Returns the value of field 'cdClubPessoa'.
     * 
     * @return long
     * @return the value of field 'cdClubPessoa'.
     */
    public long getCdClubPessoa()
    {
        return this._cdClubPessoa;
    } //-- long getCdClubPessoa() 

    /**
     * Returns the value of field 'cdControleCpfPssoa'.
     * 
     * @return int
     * @return the value of field 'cdControleCpfPssoa'.
     */
    public int getCdControleCpfPssoa()
    {
        return this._cdControleCpfPssoa;
    } //-- int getCdControleCpfPssoa() 

    /**
     * Returns the value of field 'cdCpfCnpjPssoa'.
     * 
     * @return long
     * @return the value of field 'cdCpfCnpjPssoa'.
     */
    public long getCdCpfCnpjPssoa()
    {
        return this._cdCpfCnpjPssoa;
    } //-- long getCdCpfCnpjPssoa() 

    /**
     * Returns the value of field 'cdFilialCnpjPssoa'.
     * 
     * @return int
     * @return the value of field 'cdFilialCnpjPssoa'.
     */
    public int getCdFilialCnpjPssoa()
    {
        return this._cdFilialCnpjPssoa;
    } //-- int getCdFilialCnpjPssoa() 

    /**
     * Returns the value of field 'numeroOcorrencias'.
     * 
     * @return int
     * @return the value of field 'numeroOcorrencias'.
     */
    public int getNumeroOcorrencias()
    {
        return this._numeroOcorrencias;
    } //-- int getNumeroOcorrencias() 

    /**
     * Method hasCdClubPessoa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdClubPessoa()
    {
        return this._has_cdClubPessoa;
    } //-- boolean hasCdClubPessoa() 

    /**
     * Method hasCdControleCpfPssoa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdControleCpfPssoa()
    {
        return this._has_cdControleCpfPssoa;
    } //-- boolean hasCdControleCpfPssoa() 

    /**
     * Method hasCdCpfCnpjPssoa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCpfCnpjPssoa()
    {
        return this._has_cdCpfCnpjPssoa;
    } //-- boolean hasCdCpfCnpjPssoa() 

    /**
     * Method hasCdFilialCnpjPssoa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFilialCnpjPssoa()
    {
        return this._has_cdFilialCnpjPssoa;
    } //-- boolean hasCdFilialCnpjPssoa() 

    /**
     * Method hasNumeroOcorrencias
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNumeroOcorrencias()
    {
        return this._has_numeroOcorrencias;
    } //-- boolean hasNumeroOcorrencias() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdClubPessoa'.
     * 
     * @param cdClubPessoa the value of field 'cdClubPessoa'.
     */
    public void setCdClubPessoa(long cdClubPessoa)
    {
        this._cdClubPessoa = cdClubPessoa;
        this._has_cdClubPessoa = true;
    } //-- void setCdClubPessoa(long) 

    /**
     * Sets the value of field 'cdControleCpfPssoa'.
     * 
     * @param cdControleCpfPssoa the value of field
     * 'cdControleCpfPssoa'.
     */
    public void setCdControleCpfPssoa(int cdControleCpfPssoa)
    {
        this._cdControleCpfPssoa = cdControleCpfPssoa;
        this._has_cdControleCpfPssoa = true;
    } //-- void setCdControleCpfPssoa(int) 

    /**
     * Sets the value of field 'cdCpfCnpjPssoa'.
     * 
     * @param cdCpfCnpjPssoa the value of field 'cdCpfCnpjPssoa'.
     */
    public void setCdCpfCnpjPssoa(long cdCpfCnpjPssoa)
    {
        this._cdCpfCnpjPssoa = cdCpfCnpjPssoa;
        this._has_cdCpfCnpjPssoa = true;
    } //-- void setCdCpfCnpjPssoa(long) 

    /**
     * Sets the value of field 'cdFilialCnpjPssoa'.
     * 
     * @param cdFilialCnpjPssoa the value of field
     * 'cdFilialCnpjPssoa'.
     */
    public void setCdFilialCnpjPssoa(int cdFilialCnpjPssoa)
    {
        this._cdFilialCnpjPssoa = cdFilialCnpjPssoa;
        this._has_cdFilialCnpjPssoa = true;
    } //-- void setCdFilialCnpjPssoa(int) 

    /**
     * Sets the value of field 'numeroOcorrencias'.
     * 
     * @param numeroOcorrencias the value of field
     * 'numeroOcorrencias'.
     */
    public void setNumeroOcorrencias(int numeroOcorrencias)
    {
        this._numeroOcorrencias = numeroOcorrencias;
        this._has_numeroOcorrencias = true;
    } //-- void setNumeroOcorrencias(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ConsultarContratoClienteFiltroRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultarcontratoclientefiltro.request.ConsultarContratoClienteFiltroRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultarcontratoclientefiltro.request.ConsultarContratoClienteFiltroRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultarcontratoclientefiltro.request.ConsultarContratoClienteFiltroRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarcontratoclientefiltro.request.ConsultarContratoClienteFiltroRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
