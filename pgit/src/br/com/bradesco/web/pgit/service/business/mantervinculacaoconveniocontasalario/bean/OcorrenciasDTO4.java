package br.com.bradesco.web.pgit.service.business.mantervinculacaoconveniocontasalario.bean;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.ListarConvenioContaSalarioResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.Ocorrencias4;

public class OcorrenciasDTO4 implements Serializable{
	
	private static final long serialVersionUID = -4199743626061190421L;
	
	private Integer cdBancoCredtCsignst;
	private String dsBancoCredtCsignst;
	private BigDecimal vlRepasMesConsign; 
	private BigDecimal vlCartCsign;
	
	public OcorrenciasDTO4() {
		super();
	}

	public OcorrenciasDTO4(Integer cdBancoCredtCsignst,
			String dsBancoCredtCsignst, BigDecimal vlRepasMesConsign,
			BigDecimal vlCartCsign) {
		super();
		this.cdBancoCredtCsignst = cdBancoCredtCsignst;
		this.dsBancoCredtCsignst = dsBancoCredtCsignst;
		this.vlRepasMesConsign = vlRepasMesConsign;
		this.vlCartCsign = vlCartCsign;
	}

	public List<OcorrenciasDTO4> listarOcorrenciasDTO4(ListarConvenioContaSalarioResponse response) {
		
		List<OcorrenciasDTO4> listaOcorrenciasDTO4 = new ArrayList<OcorrenciasDTO4>();
		OcorrenciasDTO4 ocorrenciasDTO4 = new OcorrenciasDTO4();
		
		for(Ocorrencias4 retorno : response.getOcorrencias4()){
			ocorrenciasDTO4.setCdBancoCredtCsignst(retorno.getCdBancoCredtCsignst());
			ocorrenciasDTO4.setDsBancoCredtCsignst(retorno.getDsBancoCredtCsignst());
			ocorrenciasDTO4.setVlRepasMesConsign(retorno.getVlRepasMesConsign());
			ocorrenciasDTO4.setVlCartCsign(retorno.getVlCartCsign());
			listaOcorrenciasDTO4.add(ocorrenciasDTO4);
		}
		return listaOcorrenciasDTO4;
	}
	
	public Integer getCdBancoCredtCsignst() {
		return cdBancoCredtCsignst;
	}

	public void setCdBancoCredtCsignst(Integer cdBancoCredtCsignst) {
		this.cdBancoCredtCsignst = cdBancoCredtCsignst;
	}

	public String getDsBancoCredtCsignst() {
		return dsBancoCredtCsignst;
	}

	public void setDsBancoCredtCsignst(String dsBancoCredtCsignst) {
		this.dsBancoCredtCsignst = dsBancoCredtCsignst;
	}

	public BigDecimal getVlRepasMesConsign() {
		return vlRepasMesConsign;
	}

	public void setVlRepasMesConsign(BigDecimal vlRepasMesConsign) {
		this.vlRepasMesConsign = vlRepasMesConsign;
	}

	public BigDecimal getVlCartCsign() {
		return vlCartCsign;
	}

	public void setVlCartCsign(BigDecimal vlCartCsign) {
		this.vlCartCsign = vlCartCsign;
	}

}
