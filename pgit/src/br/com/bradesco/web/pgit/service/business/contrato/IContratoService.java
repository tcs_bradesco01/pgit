/**
 * Nome: br.com.bradesco.web.pgit.service.business.conveniosalarial
 * Compilador: JDK 1.5
 * Prop�sito: INSERIR O PROP�SITO DAS CLASSES DO PACOTE
 * Data da cria��o: <dd/MM/yyyy>
 * Par�metros de compila��o: -d
 */
package br.com.bradesco.web.pgit.service.business.contrato;

import java.io.OutputStream;

import br.com.bradesco.web.pgit.service.business.contrato.bean.ImprimirContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.contrato.bean.ImprimirContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.contrato.bean.ListarComprovanteSalarialEntradaDTO;
import br.com.bradesco.web.pgit.service.business.contrato.bean.ListarComprovanteSalarialSaidaDTO;

/**
 * Nome: IContratoService
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public interface IContratoService {

	/**
	 * Listar comprovante salarial.
	 *
	 * @param entrada the entrada
	 * @return the listar comprovante salarial saida dto
	 */
	ListarComprovanteSalarialSaidaDTO listarComprovanteSalarial(ListarComprovanteSalarialEntradaDTO entrada); 
	
	/**
	 * Imprimir contrato.
	 *
	 * @param entrada the entrada
	 * @return the imprimir contrato saida dto
	 */
	ImprimirContratoSaidaDTO imprimirContrato(ImprimirContratoEntradaDTO entrada);
	
	/**
	 * Imprimir contrato prestacao servico.
	 *
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 * @param cdPessoaJuridicaContrato the cdpessoa juridica contrato
	 * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
	 * @param responseOutput the response output
	 * @param pathRel the path rel
	 */
	void imprimirContratoPrestacaoServico(Integer cdTipoContratoNegocio, Long cdPessoaJuridicaContrato,
			Long nrSequenciaContratoNegocio, Long nrAditivoContratoNegocio, OutputStream  responseOutput, String pathRel);
}