/*
 * Nome: br.com.bradesco.web.pgit.service.business.regsegregacessodados.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.regsegregacessodados.bean;

/**
 * Nome: DetalharRegraSegregacaoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class DetalharRegraSegregacaoSaidaDTO {
	
	/** Atributo codMensangem. */
	private String codMensangem;
	
	/** Atributo mensagem. */
	private String mensagem;
	
	/** Atributo cdRegraAcessoDado. */
	private int cdRegraAcessoDado;
	
	/** Atributo cdTipoUnidadeOrganizacionalUsuario. */
	private int cdTipoUnidadeOrganizacionalUsuario;
	
	/** Atributo dsTipoUnidadeOrganizacionalUsuario. */
	private String dsTipoUnidadeOrganizacionalUsuario;
	
	/** Atributo cdTipoUnidadeOrganizacionalProprietarioDados. */
	private int cdTipoUnidadeOrganizacionalProprietarioDados;
	
	/** Atributo dsTipoUnidadeOrganizacionalProprietarioDados. */
	private String dsTipoUnidadeOrganizacionalProprietarioDados;
	
	/** Atributo tipoAcao. */
	private int tipoAcao;
	
	/** Atributo nivelPermissaoAcessoAosDados. */
	private int nivelPermissaoAcessoAosDados;
	
	/** Atributo cdCanalInclusao. */
	private int cdCanalInclusao;
	
	/** Atributo dsCanalInclusao. */
	private String dsCanalInclusao;
	
	/** Atributo usuarioInclusao. */
	private String usuarioInclusao;
	
	/** Atributo complementoInclusao. */
	private String complementoInclusao;
	
	/** Atributo dataHoraInclusao. */
	private String dataHoraInclusao;
	
	/** Atributo cdCanalManutencao. */
	private int cdCanalManutencao;
	
	/** Atributo dsCanalManutencao. */
	private String dsCanalManutencao;
	
	/** Atributo usuarioManutencao. */
	private String usuarioManutencao;
	
	/** Atributo complementoManutencao. */
	private String complementoManutencao;
	
	/** Atributo dataHoraManutencao. */
	private String dataHoraManutencao;
	
	
	/**
	 * Get: cdCanalInclusao.
	 *
	 * @return cdCanalInclusao
	 */
	public int getCdCanalInclusao() {
		return cdCanalInclusao;
	}
	
	/**
	 * Set: cdCanalInclusao.
	 *
	 * @param cdCanalInclusao the cd canal inclusao
	 */
	public void setCdCanalInclusao(int cdCanalInclusao) {
		this.cdCanalInclusao = cdCanalInclusao;
	}
	
	/**
	 * Get: cdCanalManutencao.
	 *
	 * @return cdCanalManutencao
	 */
	public int getCdCanalManutencao() {
		return cdCanalManutencao;
	}
	
	/**
	 * Set: cdCanalManutencao.
	 *
	 * @param cdCanalManutencao the cd canal manutencao
	 */
	public void setCdCanalManutencao(int cdCanalManutencao) {
		this.cdCanalManutencao = cdCanalManutencao;
	}
	
	/**
	 * Get: nivelPermissaoAcessoAosDados.
	 *
	 * @return nivelPermissaoAcessoAosDados
	 */
	public int getNivelPermissaoAcessoAosDados() {
		return nivelPermissaoAcessoAosDados;
	}
	
	/**
	 * Set: nivelPermissaoAcessoAosDados.
	 *
	 * @param cdNivelPermissaoAcessoAosDados the nivel permissao acesso aos dados
	 */
	public void setNivelPermissaoAcessoAosDados(int cdNivelPermissaoAcessoAosDados) {
		this.nivelPermissaoAcessoAosDados = cdNivelPermissaoAcessoAosDados;
	}
	
	/**
	 * Get: cdRegraAcessoDado.
	 *
	 * @return cdRegraAcessoDado
	 */
	public int getCdRegraAcessoDado() {
		return cdRegraAcessoDado;
	}
	
	/**
	 * Set: cdRegraAcessoDado.
	 *
	 * @param cdRegraAcessoDado the cd regra acesso dado
	 */
	public void setCdRegraAcessoDado(int cdRegraAcessoDado) {
		this.cdRegraAcessoDado = cdRegraAcessoDado;
	}
	
	/**
	 * Get: tipoAcao.
	 *
	 * @return tipoAcao
	 */
	public int getTipoAcao() {
		return tipoAcao;
	}
	
	/**
	 * Set: tipoAcao.
	 *
	 * @param cdTipoAcao the tipo acao
	 */
	public void setTipoAcao(int cdTipoAcao) {
		this.tipoAcao = cdTipoAcao;
	}
	
	/**
	 * Get: cdTipoUnidadeOrganizacionalProprietarioDados.
	 *
	 * @return cdTipoUnidadeOrganizacionalProprietarioDados
	 */
	public int getCdTipoUnidadeOrganizacionalProprietarioDados() {
		return cdTipoUnidadeOrganizacionalProprietarioDados;
	}
	
	/**
	 * Set: cdTipoUnidadeOrganizacionalProprietarioDados.
	 *
	 * @param cdTipoUnidadeOrganizacionalProprietarioDados the cd tipo unidade organizacional proprietario dados
	 */
	public void setCdTipoUnidadeOrganizacionalProprietarioDados(
			int cdTipoUnidadeOrganizacionalProprietarioDados) {
		this.cdTipoUnidadeOrganizacionalProprietarioDados = cdTipoUnidadeOrganizacionalProprietarioDados;
	}
	
	/**
	 * Get: cdTipoUnidadeOrganizacionalUsuario.
	 *
	 * @return cdTipoUnidadeOrganizacionalUsuario
	 */
	public int getCdTipoUnidadeOrganizacionalUsuario() {
		return cdTipoUnidadeOrganizacionalUsuario;
	}
	
	/**
	 * Set: cdTipoUnidadeOrganizacionalUsuario.
	 *
	 * @param cdTipoUnidadeOrganizacionalUsuario the cd tipo unidade organizacional usuario
	 */
	public void setCdTipoUnidadeOrganizacionalUsuario(
			int cdTipoUnidadeOrganizacionalUsuario) {
		this.cdTipoUnidadeOrganizacionalUsuario = cdTipoUnidadeOrganizacionalUsuario;
	}
	
	/**
	 * Get: complementoInclusao.
	 *
	 * @return complementoInclusao
	 */
	public String getComplementoInclusao() {
		return complementoInclusao;
	}
	
	/**
	 * Set: complementoInclusao.
	 *
	 * @param complementoInclusao the complemento inclusao
	 */
	public void setComplementoInclusao(String complementoInclusao) {
		this.complementoInclusao = complementoInclusao;
	}
	
	/**
	 * Get: complementoManutencao.
	 *
	 * @return complementoManutencao
	 */
	public String getComplementoManutencao() {
		return complementoManutencao;
	}
	
	/**
	 * Set: complementoManutencao.
	 *
	 * @param complementoManutencao the complemento manutencao
	 */
	public void setComplementoManutencao(String complementoManutencao) {
		this.complementoManutencao = complementoManutencao;
	}
	
	/**
	 * Get: dataHoraInclusao.
	 *
	 * @return dataHoraInclusao
	 */
	public String getDataHoraInclusao() {
		return dataHoraInclusao;
	}
	
	/**
	 * Set: dataHoraInclusao.
	 *
	 * @param dataHoraInclusao the data hora inclusao
	 */
	public void setDataHoraInclusao(String dataHoraInclusao) {
		this.dataHoraInclusao = dataHoraInclusao;
	}
	
	/**
	 * Get: dataHoraManutencao.
	 *
	 * @return dataHoraManutencao
	 */
	public String getDataHoraManutencao() {
		return dataHoraManutencao;
	}
	
	/**
	 * Set: dataHoraManutencao.
	 *
	 * @param dataHoraManutencao the data hora manutencao
	 */
	public void setDataHoraManutencao(String dataHoraManutencao) {
		this.dataHoraManutencao = dataHoraManutencao;
	}
	
	/**
	 * Get: dsCanalInclusao.
	 *
	 * @return dsCanalInclusao
	 */
	public String getDsCanalInclusao() {
		return dsCanalInclusao;
	}
	
	/**
	 * Set: dsCanalInclusao.
	 *
	 * @param dsCanalInclusao the ds canal inclusao
	 */
	public void setDsCanalInclusao(String dsCanalInclusao) {
		this.dsCanalInclusao = dsCanalInclusao;
	}
	
	/**
	 * Get: dsCanalManutencao.
	 *
	 * @return dsCanalManutencao
	 */
	public String getDsCanalManutencao() {
		return dsCanalManutencao;
	}
	
	/**
	 * Set: dsCanalManutencao.
	 *
	 * @param dsCanalManutencao the ds canal manutencao
	 */
	public void setDsCanalManutencao(String dsCanalManutencao) {
		this.dsCanalManutencao = dsCanalManutencao;
	}
	
	/**
	 * Get: dsTipoUnidadeOrganizacionalProprietarioDados.
	 *
	 * @return dsTipoUnidadeOrganizacionalProprietarioDados
	 */
	public String getDsTipoUnidadeOrganizacionalProprietarioDados() {
		return dsTipoUnidadeOrganizacionalProprietarioDados;
	}
	
	/**
	 * Set: dsTipoUnidadeOrganizacionalProprietarioDados.
	 *
	 * @param dsTipoUnidadeOrganizacionalProprietarioDados the ds tipo unidade organizacional proprietario dados
	 */
	public void setDsTipoUnidadeOrganizacionalProprietarioDados(
			String dsTipoUnidadeOrganizacionalProprietarioDados) {
		this.dsTipoUnidadeOrganizacionalProprietarioDados = dsTipoUnidadeOrganizacionalProprietarioDados;
	}
	
	/**
	 * Get: dsTipoUnidadeOrganizacionalUsuario.
	 *
	 * @return dsTipoUnidadeOrganizacionalUsuario
	 */
	public String getDsTipoUnidadeOrganizacionalUsuario() {
		return dsTipoUnidadeOrganizacionalUsuario;
	}
	
	/**
	 * Set: dsTipoUnidadeOrganizacionalUsuario.
	 *
	 * @param dsTipoUnidadeOrganizacionalUsuario the ds tipo unidade organizacional usuario
	 */
	public void setDsTipoUnidadeOrganizacionalUsuario(
			String dsTipoUnidadeOrganizacionalUsuario) {
		this.dsTipoUnidadeOrganizacionalUsuario = dsTipoUnidadeOrganizacionalUsuario;
	}
	
	/**
	 * Get: usuarioInclusao.
	 *
	 * @return usuarioInclusao
	 */
	public String getUsuarioInclusao() {
		return usuarioInclusao;
	}
	
	/**
	 * Set: usuarioInclusao.
	 *
	 * @param usuarioInclusao the usuario inclusao
	 */
	public void setUsuarioInclusao(String usuarioInclusao) {
		this.usuarioInclusao = usuarioInclusao;
	}
	
	/**
	 * Get: usuarioManutencao.
	 *
	 * @return usuarioManutencao
	 */
	public String getUsuarioManutencao() {
		return usuarioManutencao;
	}
	
	/**
	 * Set: usuarioManutencao.
	 *
	 * @param usuarioManutencao the usuario manutencao
	 */
	public void setUsuarioManutencao(String usuarioManutencao) {
		this.usuarioManutencao = usuarioManutencao;
	}
	
	/**
	 * Get: codMensangem.
	 *
	 * @return codMensangem
	 */
	public String getCodMensangem() {
		return codMensangem;
	}
	
	/**
	 * Set: codMensangem.
	 *
	 * @param codMensangem the cod mensangem
	 */
	public void setCodMensangem(String codMensangem) {
		this.codMensangem = codMensangem;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	
	
	





}
