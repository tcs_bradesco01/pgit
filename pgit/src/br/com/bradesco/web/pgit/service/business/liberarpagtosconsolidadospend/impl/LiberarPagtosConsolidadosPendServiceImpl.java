/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/implementation.ftl,v $
 * $Id: implementation.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: implementation.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */
 
package br.com.bradesco.web.pgit.service.business.liberarpagtosconsolidadospend.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import br.com.bradesco.web.pgit.service.business.liberarpagtosconsolidadospend.ILiberarPagtosConsolidadosPendService;
import br.com.bradesco.web.pgit.service.business.liberarpagtosconsolidadospend.ILiberarPagtosConsolidadosPendServiceConstants;
import br.com.bradesco.web.pgit.service.business.liberarpagtosconsolidadospend.bean.ConsultarListaPagamentoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.liberarpagtosconsolidadospend.bean.ConsultarListaPagamentoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.liberarpagtosconsolidadospend.bean.ConsultarPagtoPendConsolidadoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.liberarpagtosconsolidadospend.bean.ConsultarPagtoPendConsolidadoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.liberarpagtosconsolidadospend.bean.LiberarPagtoPendConIntegralEntradaDTO;
import br.com.bradesco.web.pgit.service.business.liberarpagtosconsolidadospend.bean.LiberarPagtoPendConIntegralSaidaDTO;
import br.com.bradesco.web.pgit.service.business.liberarpagtosconsolidadospend.bean.LiberarPagtoPendConParcialEntradaDTO;
import br.com.bradesco.web.pgit.service.business.liberarpagtosconsolidadospend.bean.LiberarPagtoPendConParcialSaidaDTO;
import br.com.bradesco.web.pgit.service.business.liberarpagtosconsolidadospend.bean.LiberarPendenciaPagtoConIntegralEntradaDTO;
import br.com.bradesco.web.pgit.service.business.liberarpagtosconsolidadospend.bean.LiberarPendenciaPagtoConIntegralSaidaDTO;
import br.com.bradesco.web.pgit.service.business.liberarpagtosconsolidadospend.bean.LiberarPendenciaPagtoConParcialEntradaDTO;
import br.com.bradesco.web.pgit.service.business.liberarpagtosconsolidadospend.bean.LiberarPendenciaPagtoConParcialSaidaDTO;
import br.com.bradesco.web.pgit.service.data.pdc.FactoryAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarlistapagamento.request.ConsultarListaPagamentoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarlistapagamento.response.ConsultarListaPagamentoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.consultarpagtopendconsolidado.request.ConsultarPagtoPendConsolidadoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarpagtopendconsolidado.response.ConsultarPagtoPendConsolidadoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.liberarpagtopendconintegral.request.LiberarPagtoPendConIntegralRequest;
import br.com.bradesco.web.pgit.service.data.pdc.liberarpagtopendconintegral.response.LiberarPagtoPendConIntegralResponse;
import br.com.bradesco.web.pgit.service.data.pdc.liberarpagtopendconparcial.request.LiberarPagtoPendConParcialRequest;
import br.com.bradesco.web.pgit.service.data.pdc.liberarpagtopendconparcial.response.LiberarPagtoPendConParcialResponse;
import br.com.bradesco.web.pgit.service.data.pdc.liberarpendenciapagtoconintegral.request.LiberarPendenciaPagtoConIntegralRequest;
import br.com.bradesco.web.pgit.service.data.pdc.liberarpendenciapagtoconintegral.response.LiberarPendenciaPagtoConIntegralResponse;
import br.com.bradesco.web.pgit.service.data.pdc.liberarpendenciapagtoconparcial.request.LiberarPendenciaPagtoConParcialRequest;
import br.com.bradesco.web.pgit.service.data.pdc.liberarpendenciapagtoconparcial.response.LiberarPendenciaPagtoConParcialResponse;
import br.com.bradesco.web.pgit.utils.CpfCnpjUtils;
import br.com.bradesco.web.pgit.utils.DateUtils;
import br.com.bradesco.web.pgit.utils.PgitUtil;
import br.com.bradesco.web.pgit.utils.constants.ErrorMessageConstants;
import br.com.bradesco.web.pgit.view.converters.FormatarData;


/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Implementa��o do adaptador: LiberarPagtosConsolidadosPend
 * </p>
 * 
 * @comment C�DIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public class LiberarPagtosConsolidadosPendServiceImpl implements ILiberarPagtosConsolidadosPendService {
	
	/** Atributo factoryAdapter. */
	private FactoryAdapter factoryAdapter;
	
	

    /**
     * Get: factoryAdapter.
     *
     * @return factoryAdapter
     */
    public FactoryAdapter getFactoryAdapter() {
		return factoryAdapter;
	}

	/**
	 * Set: factoryAdapter.
	 *
	 * @param factoryAdapter the factory adapter
	 */
	public void setFactoryAdapter(FactoryAdapter factoryAdapter) {
		this.factoryAdapter = factoryAdapter;
	}

    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.liberarpagtosconsolidadospend.ILiberarPagtosConsolidadosPendService#consultarPagtoPendConsolidado(br.com.bradesco.web.pgit.service.business.liberarpagtosconsolidadospend.bean.ConsultarPagtoPendConsolidadoEntradaDTO)
     */
    public List<ConsultarPagtoPendConsolidadoSaidaDTO> consultarPagtoPendConsolidado(ConsultarPagtoPendConsolidadoEntradaDTO entradaDTO){
    	java.text.NumberFormat nf = java.text.NumberFormat.getInstance( new Locale( "pt_br" ) );
    	List<ConsultarPagtoPendConsolidadoSaidaDTO> listaSaida = new ArrayList<ConsultarPagtoPendConsolidadoSaidaDTO>();
    	ConsultarPagtoPendConsolidadoRequest request = new ConsultarPagtoPendConsolidadoRequest();
    	ConsultarPagtoPendConsolidadoResponse response = new ConsultarPagtoPendConsolidadoResponse();
    	
		request.setCdTipoPesquisa(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoPesquisa()));
		request.setNrOcorrencias(ILiberarPagtosConsolidadosPendServiceConstants.NUMERO_OCORRENCIAS_CONSULTAR);
		request.setCdPessoaJuridica(PgitUtil.verificaLongNulo(entradaDTO.getCdPessoaJuridica()));
		request.setCdTipoContrato(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoContrato()));
		request.setNrSequencialContrato(PgitUtil.verificaLongNulo(entradaDTO.getNrSequencialContrato()));
		request.setDtInicialPagamento(PgitUtil.verificaStringNula(entradaDTO.getDtInicialPagamento()));
		request.setDtFinalPagamento(PgitUtil.verificaStringNula(entradaDTO.getDtFinalPagamento()));
		request.setNrRemessa(PgitUtil.verificaLongNulo(entradaDTO.getNrRemessa()));
		request.setCdBanco(PgitUtil.verificaIntegerNulo(entradaDTO.getCdBanco()));
		request.setCdAgencia(PgitUtil.verificaIntegerNulo(entradaDTO.getCdAgencia()));
		request.setCdConta(PgitUtil.verificaLongNulo(entradaDTO.getCdConta()));
		request.setCdDigitoConta(PgitUtil.DIGITO_CONTA);
		request.setCdProdutoServicoOperacao(PgitUtil.verificaIntegerNulo(entradaDTO.getCdProdutoServicoOperacao()));
		request.setCdProdutoServicoRelacionado(PgitUtil.verificaIntegerNulo(entradaDTO.getCdProdutoServicoRelacionado()));
		request.setCdSituacaoOperacaoPagamento(ILiberarPagtosConsolidadosPendServiceConstants.CODIGO_SITUACAO_OPERACAO_PAGAMENTO);
		request.setCdMotivoSituacaoPagamento(PgitUtil.verificaIntegerNulo(entradaDTO.getCdMotivoSituacaoPagamento()));
		
		response = getFactoryAdapter().getConsultarPagtoPendConsolidadoPDCAdapter().invokeProcess(request);
    	
		for(int i=0;i<response.getOcorrenciasCount();i++){
			ConsultarPagtoPendConsolidadoSaidaDTO saidaDTO = new ConsultarPagtoPendConsolidadoSaidaDTO();
			saidaDTO.setCdMensagem(response.getCodMensagem());
			saidaDTO.setMensagem(response.getMensagem());
			saidaDTO.setCdPessoaJuridica(response.getOcorrencias(i).getCdPessoaJuridica());
			saidaDTO.setDsRazaoSocial(response.getOcorrencias(i).getDsRazaoSocial());
			saidaDTO.setCdTipoContrato(response.getOcorrencias(i).getCdTipoContrato());
			saidaDTO.setNrContrato(response.getOcorrencias(i).getNrContrato());
			saidaDTO.setNrCnpjCpf(response.getOcorrencias(i).getNrCnpjCpf());
			saidaDTO.setNrFilialCnpjCpf(response.getOcorrencias(i).getNrFilialCnpjCpf());
			saidaDTO.setCdDigitoCnpjCpf(response.getOcorrencias(i).getCdDigitoCnpjCpf());
			saidaDTO.setNrRemessa(response.getOcorrencias(i).getNrRemessa());
			saidaDTO.setDtPagamento(response.getOcorrencias(i).getDtPagamento());
			saidaDTO.setCdBanco(response.getOcorrencias(i).getCdBanco());
			saidaDTO.setCdAgencia(response.getOcorrencias(i).getCdAgencia());
			saidaDTO.setCdDigitoAgencia(response.getOcorrencias(i).getCdDigitoAgencia());
			saidaDTO.setCdConta(response.getOcorrencias(i).getCdConta());
			saidaDTO.setCdDigitoConta(response.getOcorrencias(i).getCdDigitoConta());
			saidaDTO.setCdPessoaJuridicaDebito(response.getOcorrencias(i).getCdPessoaJuridicaDebito());
			saidaDTO.setCdTipoContratoDebito(response.getOcorrencias(i).getCdTipoContratoDebito());
			saidaDTO.setNrSequenciaContratoDebito(response.getOcorrencias(i).getNrSequenciaContratoDebito());
			saidaDTO.setCdProdutoServicoOperacao(response.getOcorrencias(i).getCdProdutoServicoOperacao());
			saidaDTO.setDsResumoProduto(response.getOcorrencias(i).getDsResumoProduto());
			saidaDTO.setCdProdutoServicoRelacionado(response.getOcorrencias(i).getCdProdutoServicoRelacionado());
			saidaDTO.setDsOperacao(response.getOcorrencias(i).getDsOperacao());
			saidaDTO.setCdSituacaoOperacaoPagamento(response.getOcorrencias(i).getCdSituacaoOperacaoPagamento());
			saidaDTO.setDsSituacaoOperacaoPagamento(response.getOcorrencias(i).getDsSituacaoOperacaoPagamento());
			saidaDTO.setCdMotivoOperacaoPagamento(response.getOcorrencias(i).getCdMotivoOperacaoPagamento());
			saidaDTO.setDsMotivoOperacaoPagamento(response.getOcorrencias(i).getDsMotivoOperacaoPagamento());
			saidaDTO.setQtPagamento(response.getOcorrencias(i).getQtPagamento());
			saidaDTO.setVlPagamento(response.getOcorrencias(i).getVlPagamento());
		
			saidaDTO.setEmpresaConglomeradoFormatada(PgitUtil.concatenarCampos(saidaDTO.getCdPessoaJuridica(), saidaDTO.getDsRazaoSocial()));
			saidaDTO.setCnpjCpfFormatado(saidaDTO.getNrCnpjCpf() == 0L && saidaDTO.getNrFilialCnpjCpf() == 0 && saidaDTO.getCdDigitoCnpjCpf() == 0 ? "" :
					CpfCnpjUtils.formatarCpfCnpj(saidaDTO.getNrCnpjCpf(), saidaDTO.getNrFilialCnpjCpf(), saidaDTO.getCdDigitoCnpjCpf()));
			saidaDTO.setContaDebitoFormatada(PgitUtil.formatBancoAgenciaConta(saidaDTO.getCdBanco(), saidaDTO.getCdAgencia(), saidaDTO.getCdDigitoAgencia(), saidaDTO.getCdConta(), saidaDTO.getCdDigitoConta(), true));
			saidaDTO.setTipoServicoFormatado(PgitUtil.concatenarCampos(saidaDTO.getCdProdutoServicoOperacao(), saidaDTO.getDsResumoProduto()));
			saidaDTO.setModalidadeFormatada(PgitUtil.concatenarCampos(saidaDTO.getCdProdutoServicoRelacionado(), saidaDTO.getDsOperacao()));
			saidaDTO.setNumeroContratoFormatado(saidaDTO.getNrContrato() == 0L ? "" : String.valueOf(saidaDTO.getNrContrato()));
			saidaDTO.setRemessaFormatada(saidaDTO.getNrRemessa() == 0L ? "" : String.valueOf(saidaDTO.getNrRemessa()));
			saidaDTO.setQtddPagamentoFormatado(saidaDTO.getQtPagamento() == 0L ? "" : nf.format(saidaDTO.getQtPagamento()));
			saidaDTO.setDtPagamentoFormatada(DateUtils.formartarDataPDCparaPadraPtBr(saidaDTO.getDtPagamento(), "dd.MM.yyyy", "dd/MM/yyyy"));
			
			listaSaida.add(saidaDTO);
    	}
    	
    	return listaSaida;
	}
    
    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.liberarpagtosconsolidadospend.ILiberarPagtosConsolidadosPendService#consultarListaPagamento(br.com.bradesco.web.pgit.service.business.liberarpagtosconsolidadospend.bean.ConsultarListaPagamentoEntradaDTO)
     */
    public List<ConsultarListaPagamentoSaidaDTO> consultarListaPagamento (ConsultarListaPagamentoEntradaDTO entrada){
    	List<ConsultarListaPagamentoSaidaDTO> listaRetorno = new ArrayList<ConsultarListaPagamentoSaidaDTO>();
    	ConsultarListaPagamentoRequest request = new ConsultarListaPagamentoRequest();
    	ConsultarListaPagamentoResponse response = new ConsultarListaPagamentoResponse();
    	
        request.setCdPessoaJuridicaContrato(PgitUtil.verificaLongNulo(entrada.getCdPessoaJuridicaContrato()));
        request.setCdTipoContratonegocio(PgitUtil.verificaIntegerNulo(entrada.getCdTipoContratoNegocio()));
        request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(entrada.getNrSequenciaContratoNegocio()));
        request.setCdCorpoCpfCnpj(PgitUtil.verificaLongNulo(entrada.getCdCorpoCpfCnpj()));
        request.setCdFilialCpfCnpj(PgitUtil.verificaIntegerNulo(entrada.getCdFilialCpfCnpj()));
        request.setCdControleCpfCnpj(PgitUtil.verificaIntegerNulo(entrada.getCdControleCpfCnpj()));
        request.setNrArquivoRemessa(PgitUtil.verificaLongNulo(entrada.getNrArquivoRemessa()));
        request.setDtCreditoPagamento(FormatarData.formatarDataFromPdc(PgitUtil.verificaStringNula(entrada.getDtCreditoPagamento())));
        request.setCdBanco(PgitUtil.verificaIntegerNulo(entrada.getCdBanco()));
        request.setCdAgencia(PgitUtil.verificaIntegerNulo(entrada.getCdAgencia()));
        request.setCdConta(PgitUtil.verificaLongNulo(entrada.getCdConta()));
        request.setCdDigitoConta(PgitUtil.complementaDigitoConta(entrada.getCdDigitoConta()));
        request.setCdPessoaJuridicaDebito(PgitUtil.verificaLongNulo(entrada.getCdPessoaJuridicaDebito()));
        request.setCdTipoContratoDebito(PgitUtil.verificaIntegerNulo(entrada.getCdTipoContratoDebito()));
        request.setNrSequenciaContratoDebito(PgitUtil.verificaLongNulo(entrada.getNrSequenciaContratoDebito()));
        request.setCdProdutoServicoOperacao(PgitUtil.verificaIntegerNulo(entrada.getCdProdutoServicoOperacao()));
        request.setCdProdutoServicoRelacionado(PgitUtil.verificaIntegerNulo(entrada.getCdProdutoServicoRelacionado()));
        request.setCdSituacaoOperacaoPagamento(PgitUtil.verificaIntegerNulo(entrada.getCdSituacaoOperacaoPagamento()));        
        request.setCdMotivoPendencia(PgitUtil.verificaIntegerNulo(entrada.getCdMotivoPendencia()));
        request.setNumeroOcorrencias(ILiberarPagtosConsolidadosPendServiceConstants.NUMERO_OCORRENCIAS_LISTAR_PAGAMENTOS);
       
    	
        response = getFactoryAdapter().getConsultarListaPagamentoPDCAdapter().invokeProcess(request);
        
        ConsultarListaPagamentoSaidaDTO saida;
        
        for(int i=0;i<response.getOcorrenciasCount();i++){
        	saida = new ConsultarListaPagamentoSaidaDTO();
            saida.setCodMensagem(response.getCodMensagem());
            saida.setMensagem(response.getMensagem());
            saida.setCdPessoaJuridicaContrato(response.getOcorrencias(i).getCdPessoaJuridicaContrato());
            saida.setCdTipoContratoNegocio(response.getOcorrencias(i).getCdTipoContratoNegocio());
            saida.setNrSequenciaContratoNegocio(response.getOcorrencias(i).getNrSequenciaContratoNegocio());
            saida.setCdTipoCanal(response.getOcorrencias(i).getCdTipoCanal());
            saida.setNrPagamento(response.getOcorrencias(i).getNrPagamento());
            saida.setVlrPagamento(response.getOcorrencias(i).getVlrPagamento());
            saida.setCdCorpoCpfCnpjFavorecido(response.getOcorrencias(i).getCdContaFavorecido());
            saida.setCdFilialCpfCnpjFavorecido(response.getOcorrencias(i).getCdFilialCpfCnpjFavorecido());
            saida.setCdControleCpfCnpjFavorecido(response.getOcorrencias(i).getCdControleCpfCnpjFavorecido());
            saida.setCdFavorecido(response.getOcorrencias(i).getCdFavorecido());
            saida.setDsBeneficiario(response.getOcorrencias(i).getDsBeneficiario());
            saida.setCdBancoFavorecido(response.getOcorrencias(i).getCdBancoFavorecido());
            saida.setDsBanco(response.getOcorrencias(i).getDsBanco());
            saida.setCdAgenciaFavorecido(response.getOcorrencias(i).getCdAgenciaFavorecido());
            saida.setCdDigitoAgencia(response.getOcorrencias(i).getCdDigitoAgencia());
            saida.setDsAgencia(response.getOcorrencias(i).getDsAgencia());
            saida.setCdContaFavorecido(response.getOcorrencias(i).getCdContaFavorecido());
            saida.setCdDigitoContaFavorecido(response.getOcorrencias(i).getCdDigitoContaFavorecido());
            saida.setCdTipoTela(response.getOcorrencias(i).getCdTipoTela());
            
            saida.setCpfFormatado(CpfCnpjUtils.formatarCpfCnpj(saida.getCdCorpoCpfCnpjFavorecido(), saida.getCdFilialCpfCnpjFavorecido(), saida.getCdControleCpfCnpjFavorecido()));
            saida.setFavorecidoBeneficiario(saida.getDsBeneficiario());
            saida.setBancoAgenciaContaFormatada(PgitUtil.formatBancoAgenciaConta(saida.getCdBancoFavorecido(), saida.getCdAgenciaFavorecido(), saida.getCdDigitoAgencia(), saida.getCdContaFavorecido(), saida.getCdDigitoContaFavorecido(), true));
            saida.setBancoFormatado(PgitUtil.formatBanco(saida.getCdBancoFavorecido(), saida.getDsBanco(), false));
            saida.setAgenciaFormatada(PgitUtil.formatAgencia(saida.getCdAgenciaFavorecido(), saida.getCdDigitoAgencia(), saida.getDsAgencia(), false));
            saida.setContaFormatada(PgitUtil.formatConta(saida.getCdContaFavorecido(), saida.getCdDigitoContaFavorecido(), false));
            listaRetorno.add(saida);
        
        }
        
    	return listaRetorno;
    }
    
    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.liberarpagtosconsolidadospend.ILiberarPagtosConsolidadosPendService#liberarPagtoPendConIntegral(br.com.bradesco.web.pgit.service.business.liberarpagtosconsolidadospend.bean.LiberarPagtoPendConIntegralEntradaDTO)
     */
    public LiberarPagtoPendConIntegralSaidaDTO liberarPagtoPendConIntegral (LiberarPagtoPendConIntegralEntradaDTO entrada){
    	LiberarPagtoPendConIntegralSaidaDTO saida = new LiberarPagtoPendConIntegralSaidaDTO();
    	LiberarPagtoPendConIntegralRequest request = new LiberarPagtoPendConIntegralRequest();
    	LiberarPagtoPendConIntegralResponse response = new LiberarPagtoPendConIntegralResponse();
    	
		   request.setNrOcorrencias(ILiberarPagtosConsolidadosPendServiceConstants.NUMERO_OCORRENCIAS_INTEGRAL);
		   request.setCdPessoaJuridica(PgitUtil.verificaLongNulo(entrada.getCdPessoaJuridica()));
    	   request.setCdTipoContrato(PgitUtil.verificaIntegerNulo(entrada.getCdTipoContrato()));
    	   request.setNrSequenciaContrato(PgitUtil.verificaLongNulo(entrada.getNrSequenciaContrato()));
    	   request.setCdCnpjCliente(PgitUtil.verificaLongNulo(entrada.getCdCnpjCliente()));
    	   request.setCdCnpjFilial(PgitUtil.verificaIntegerNulo(entrada.getCdCnpjFilial()));
    	   request.setCdCnpjDigito(PgitUtil.verificaIntegerNulo(entrada.getCdCnpjDigito()));
    	   request.setNrRemessa(PgitUtil.verificaLongNulo(entrada.getNrRemessa()));
    	   request.setDtCredito(DateUtils.formartarDataPDCparaPadraPtBr(PgitUtil.verificaStringNula(entrada.getDtCredito()), "dd.MM.yyyy", "dd/MM/yyyy"));
    	   request.setCdPessoaJuridicaDebito(PgitUtil.verificaLongNulo(entrada.getCdPessoaJuridicaDebito()));
    	   request.setCdTipoContratoDebito(PgitUtil.verificaIntegerNulo(entrada.getCdTipoContratoDebito()));
    	   request.setNrSequenciaContratoDebito(PgitUtil.verificaLongNulo(entrada.getNrSequenciaContratoDebito()));
    	   request.setCdProdutoServicoOperacao(PgitUtil.verificaIntegerNulo(entrada.getCdProdutoServicoOperacao()));
    	   request.setCdProdutoServicoRelacionado(PgitUtil.verificaIntegerNulo(entrada.getCdProdutoServicoRelacionado()));
    	   request.setCdSituacaoOperacaoPagamento(PgitUtil.verificaIntegerNulo(entrada.getCdSituacaoOperacaoPagamento()));
    	   request.setCdMotivoPendencia(PgitUtil.verificaIntegerNulo(entrada.getCdMotivoPendencia()));
    	   
		do {
			response = getFactoryAdapter().getLiberarPagtoPendConIntegralPDCAdapter().invokeProcess(request);
			saida.setCodMensagem(response.getCodMensagem());
			saida.setMensagem(response.getMensagem());
		} while (ErrorMessageConstants.PGIT0009.equals(saida.getCodMensagem()));
		
    	return saida;
    }
    
    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.liberarpagtosconsolidadospend.ILiberarPagtosConsolidadosPendService#liberarPagtoPendConParcial(java.util.List)
     */
    public LiberarPagtoPendConParcialSaidaDTO liberarPagtoPendConParcial (List<LiberarPagtoPendConParcialEntradaDTO> listaEntrada){
    	LiberarPagtoPendConParcialSaidaDTO saida = new LiberarPagtoPendConParcialSaidaDTO();
    	LiberarPagtoPendConParcialRequest request = new LiberarPagtoPendConParcialRequest();   	
    	
    	ArrayList<br.com.bradesco.web.pgit.service.data.pdc.liberarpagtopendconparcial.request.Ocorrencias> listaOcorrencia = new ArrayList<br.com.bradesco.web.pgit.service.data.pdc.liberarpagtopendconparcial.request.Ocorrencias>(); 
    	br.com.bradesco.web.pgit.service.data.pdc.liberarpagtopendconparcial.request.Ocorrencias ocorrencias;
    	
    	for(int i=0;i < listaEntrada.size();i++){
    		ocorrencias = new br.com.bradesco.web.pgit.service.data.pdc.liberarpagtopendconparcial.request.Ocorrencias();
    		ocorrencias.setCdControlePagamento(PgitUtil.verificaStringNula(listaEntrada.get(i).getCdControlePagamento()));
    		ocorrencias.setCdModalidade(PgitUtil.verificaIntegerNulo(listaEntrada.get(i).getCdModalidade()));
    		ocorrencias.setCdPessoaJuridica(PgitUtil.verificaLongNulo(listaEntrada.get(i).getCdPessoaJuridica()));
    		ocorrencias.setCdTipoCanal(PgitUtil.verificaIntegerNulo(listaEntrada.get(i).getCdTipoCanal()));
    		ocorrencias.setCdTipoContrato(PgitUtil.verificaIntegerNulo(listaEntrada.get(i).getCdTipoContrato()));
    		ocorrencias.setNrSequencialContrato(PgitUtil.verificaLongNulo(listaEntrada.get(i).getNrSequencialContrato()));
    		listaOcorrencia.add(ocorrencias);
    	}
    	
    	request.setQtRegistros(listaEntrada.size());
    	request.setOcorrencias((br.com.bradesco.web.pgit.service.data.pdc.liberarpagtopendconparcial.request.Ocorrencias[])listaOcorrencia.toArray(new br.com.bradesco.web.pgit.service.data.pdc.liberarpagtopendconparcial.request.Ocorrencias[0]));
    	
    	
    	LiberarPagtoPendConParcialResponse response = getFactoryAdapter().getLiberarPagtoPendConParcialPDCAdapter().invokeProcess(request);
    	
    	saida.setCodMensagem(response.getCodMensagem());
    	saida.setMensagem(response.getMensagem());
    	
    	return saida;
    }
    
    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.liberarpagtosconsolidadospend.ILiberarPagtosConsolidadosPendService#liberarPendenciaPagtoConIntegral(br.com.bradesco.web.pgit.service.business.liberarpagtosconsolidadospend.bean.LiberarPendenciaPagtoConIntegralEntradaDTO)
     */
    public LiberarPendenciaPagtoConIntegralSaidaDTO liberarPendenciaPagtoConIntegral (LiberarPendenciaPagtoConIntegralEntradaDTO entrada){
    	LiberarPendenciaPagtoConIntegralSaidaDTO saida = new LiberarPendenciaPagtoConIntegralSaidaDTO();
    	LiberarPendenciaPagtoConIntegralRequest request = new LiberarPendenciaPagtoConIntegralRequest();
    	
        request.setNrOcorrencias(ILiberarPagtosConsolidadosPendServiceConstants.NUMERO_OCORRENCIAS_INTEGRAL_PENDENCIA);
        request.setCdPessoaJuridica(PgitUtil.verificaLongNulo(entrada.getCdPessoaJuridica()));
        request.setCdTipoContrato(PgitUtil.verificaIntegerNulo(entrada.getCdTipoContrato()));
        request.setNrSequenciaContrato(PgitUtil.verificaLongNulo(entrada.getNrSequenciaContrato()));
        request.setCdCnpjCliente(PgitUtil.verificaLongNulo(entrada.getCdCnpjCliente()));
        request.setCdCnpjFilial(PgitUtil.verificaIntegerNulo(entrada.getCdCnpjFilial()));
        request.setCdCnpjDigito(PgitUtil.verificaIntegerNulo(entrada.getCdCnpjDigito()));
        request.setNrRemessa(PgitUtil.verificaLongNulo(entrada.getNrRemessa()));
        request.setDtCredito(DateUtils.formartarDataPDCparaPadraPtBr(PgitUtil.verificaStringNula(entrada.getDtCredito()), "dd.MM.yyyy", "dd/MM/yyyy"));
        request.setCdPessoaJuridicaDebito(PgitUtil.verificaLongNulo(entrada.getCdPessoaJuridicaDebito()));
        request.setCdTipoContratoDebito(PgitUtil.verificaIntegerNulo(entrada.getCdTipoContratoDebito()));
        request.setNrSequenciaContratoDebito(PgitUtil.verificaLongNulo(entrada.getNrSequenciaContratoDebito()));
        request.setCdProdutoServicoOperacao(PgitUtil.verificaIntegerNulo(entrada.getCdProdutoServicoOperacao()));
        request.setCdProdutoServicoRelacionado(PgitUtil.verificaIntegerNulo(entrada.getCdProdutoServicoRelacionado()));
        request.setCdSituacaoOperacaoPagamento(PgitUtil.verificaIntegerNulo(entrada.getCdSituacaoOperacaoPagamento()));
        request.setCdMotivoPendencia(PgitUtil.verificaIntegerNulo(entrada.getCdMotivoPendencia()));
        
        do {
        	LiberarPendenciaPagtoConIntegralResponse response = getFactoryAdapter().getLiberarPendenciaPagtoConIntegralPDCAdapter().invokeProcess(request);
        	saida.setCodMensagem(response.getCodMensagem());
        	saida.setMensagem(response.getMensagem());
		} while (ErrorMessageConstants.PGIT0009.equals(saida.getCodMensagem()));
    	
    	return saida;
    }
    
    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.liberarpagtosconsolidadospend.ILiberarPagtosConsolidadosPendService#liberarPendenciaPagtoConParcial(java.util.List)
     */
    public LiberarPendenciaPagtoConParcialSaidaDTO liberarPendenciaPagtoConParcial(List<LiberarPendenciaPagtoConParcialEntradaDTO> listaEntrada){
    	
    	LiberarPendenciaPagtoConParcialSaidaDTO saida = new LiberarPendenciaPagtoConParcialSaidaDTO();
    	LiberarPendenciaPagtoConParcialRequest request = new LiberarPendenciaPagtoConParcialRequest();   	
    	LiberarPendenciaPagtoConParcialResponse response = new LiberarPendenciaPagtoConParcialResponse();
    	
    	ArrayList<br.com.bradesco.web.pgit.service.data.pdc.liberarpendenciapagtoconparcial.request.Ocorrencias> listaOcorrencia = new ArrayList<br.com.bradesco.web.pgit.service.data.pdc.liberarpendenciapagtoconparcial.request.Ocorrencias>(); 
    	br.com.bradesco.web.pgit.service.data.pdc.liberarpendenciapagtoconparcial.request.Ocorrencias ocorrencias;
    	
    	for(int i=0;i < listaEntrada.size();i++){
    		ocorrencias = new br.com.bradesco.web.pgit.service.data.pdc.liberarpendenciapagtoconparcial.request.Ocorrencias();
    		ocorrencias.setCdPessoaJuridicaContrato(PgitUtil.verificaLongNulo(listaEntrada.get(i).getCdPessoaJuridicaContrato()));
    		ocorrencias.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(listaEntrada.get(i).getCdTipoContratoNegocio()));
    		ocorrencias.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(listaEntrada.get(i).getNrSequenciaContratoNegocio()));
    		ocorrencias.setCdTipoCanal(PgitUtil.verificaIntegerNulo(listaEntrada.get(i).getCdTipoCanal()));
    		ocorrencias.setCdControlePagamento(PgitUtil.verificaStringNula(listaEntrada.get(i).getCdControlePagamento()));
    		ocorrencias.setCdModalidade(PgitUtil.verificaIntegerNulo(listaEntrada.get(i).getCdModalidade()));
    		ocorrencias.setCdMotivoPendencia(PgitUtil.verificaIntegerNulo(listaEntrada.get(i).getCdMotivoPendencia()));
    		listaOcorrencia.add(ocorrencias);
    	}
    	
    	request.setQtRegistros(listaEntrada.size());
    	request.setOcorrencias((br.com.bradesco.web.pgit.service.data.pdc.liberarpendenciapagtoconparcial.request.Ocorrencias[])listaOcorrencia.toArray(new br.com.bradesco.web.pgit.service.data.pdc.liberarpendenciapagtoconparcial.request.Ocorrencias[0]));
    	response = getFactoryAdapter().getLiberarPendenciaPagtoConParcialPDCAdapter().invokeProcess(request);
    	
    	saida.setCodMensagem(response.getCodMensagem());
    	saida.setMensagem(response.getMensagem());
    	
    	return saida;
    }
}

