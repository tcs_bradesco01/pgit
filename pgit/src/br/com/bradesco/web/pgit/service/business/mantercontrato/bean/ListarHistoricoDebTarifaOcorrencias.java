package br.com.bradesco.web.pgit.service.business.mantercontrato.bean;

import br.com.bradesco.web.pgit.utils.PgitUtil;

public class ListarHistoricoDebTarifaOcorrencias {
	
    private Integer cdProdutoServicoOperacao;
    private String  dsProdutoServicoOperacao;
    private Integer cdProdutoOperacaoRelacionado;
    private String dsProdutoOperacaoRelacionada;
    private Long cdServicoCompostoPagamento;
    private Integer cdOperacaoProdutoServico;
    private String dsOperacaoProdutoServico;
    private Integer cdNaturezaOperacaoPagamento;
    private String dsNatuzOperPgto;
    private String cdCorpoCpfCnpj;
    private Long cdParticipante;
    private String nmParticipante;
    private Integer cdBanco;
    private String dsBanco;
    private Integer cdAgencia;
    private Integer cdDigitoAgencia;
    private String dsAgencia;
    private Long cdConta;
    private String cdDigitoConta;
    private Integer cdTipoConta;
    private String dsTipoConta;
    private String hrInclusaoRegistroHist;
    private String dtManutencao;
    private String hrManutencao;
    private String cdUsuario;
    private String dsTipoManutencao;
    
    public String getDataHoraManuFormatado(){
    		return PgitUtil.concatenarCampos(dtManutencao , hrManutencao," ");
    }
    
    public String getCdProdutoServicoOperacaoFormatado(){
    	if(cdProdutoServicoOperacao!= null && cdProdutoServicoOperacao !=0){
    		return PgitUtil.concatenarCampos(cdProdutoServicoOperacao, dsProdutoServicoOperacao, "-");
    	}
    	return "";
    }
    
    public String getCdProdutoOperacaoRelacionadoFormatado(){
    	if(cdProdutoOperacaoRelacionado!= null && cdProdutoOperacaoRelacionado !=0){
    		return PgitUtil.concatenarCampos(cdProdutoOperacaoRelacionado, dsProdutoOperacaoRelacionada, "-");
    	}
    	return "";
    }
    
    public String getCdOperacaoProdutoServicoFormatado(){
    	if(cdOperacaoProdutoServico!= null && cdOperacaoProdutoServico !=0){
    		return PgitUtil.concatenarCampos(cdOperacaoProdutoServico, dsOperacaoProdutoServico, "-");
    	}
    	return "";
    }
    
    public String getCdTipoContaFormatada(){
    	if(cdTipoConta!= null && cdTipoConta !=0){
    		return dsTipoConta;
    	}
    	return "";
    }
    
    public String getCdDigitoAgenciaFormatada(){
    	if(cdDigitoAgencia != null && cdDigitoAgencia  !=0){
    		StringBuilder agenciaContab = new StringBuilder();
    		agenciaContab.append(cdBanco);
    		agenciaContab.append("/");
    		agenciaContab.append(cdAgencia);
    		agenciaContab.append("/");
    		agenciaContab.append(cdConta);
    		agenciaContab.append("-");
    		agenciaContab.append(cdDigitoConta);
    		
    		return agenciaContab.toString();
    	}
    	return "";
    }
    
	public Integer getCdProdutoServicoOperacao() {
		return cdProdutoServicoOperacao;
	}
	public void setCdProdutoServicoOperacao(Integer cdProdutoServicoOperacao) {
		this.cdProdutoServicoOperacao = cdProdutoServicoOperacao;
	}
	public String getDsProdutoServicoOperacao() {
		return dsProdutoServicoOperacao;
	}
	public void setDsProdutoServicoOperacao(String dsProdutoServicoOperacao) {
		this.dsProdutoServicoOperacao = dsProdutoServicoOperacao;
	}
	public Integer getCdProdutoOperacaoRelacionado() {
		return cdProdutoOperacaoRelacionado;
	}
	public void setCdProdutoOperacaoRelacionado(Integer cdProdutoOperacaoRelacionado) {
		this.cdProdutoOperacaoRelacionado = cdProdutoOperacaoRelacionado;
	}
	public String getDsProdutoOperacaoRelacionada() {
		return dsProdutoOperacaoRelacionada;
	}
	public void setDsProdutoOperacaoRelacionada(String dsProdutoOperacaoRelacionada) {
		this.dsProdutoOperacaoRelacionada = dsProdutoOperacaoRelacionada;
	}
	public Long getCdServicoCompostoPagamento() {
		return cdServicoCompostoPagamento;
	}
	public void setCdServicoCompostoPagamento(Long cdServicoCompostoPagamento) {
		this.cdServicoCompostoPagamento = cdServicoCompostoPagamento;
	}
	public Integer getCdOperacaoProdutoServico() {
		return cdOperacaoProdutoServico;
	}
	public void setCdOperacaoProdutoServico(Integer cdOperacaoProdutoServico) {
		this.cdOperacaoProdutoServico = cdOperacaoProdutoServico;
	}
	public String getDsOperacaoProdutoServico() {
		return dsOperacaoProdutoServico;
	}
	public void setDsOperacaoProdutoServico(String dsOperacaoProdutoServico) {
		this.dsOperacaoProdutoServico = dsOperacaoProdutoServico;
	}
	public Integer getCdNaturezaOperacaoPagamento() {
		return cdNaturezaOperacaoPagamento;
	}
	public void setCdNaturezaOperacaoPagamento(Integer cdNaturezaOperacaoPagamento) {
		this.cdNaturezaOperacaoPagamento = cdNaturezaOperacaoPagamento;
	}
	public String getDsNatuzOperPgto() {
		return dsNatuzOperPgto;
	}
	public void setDsNatuzOperPgto(String dsNatuzOperPgto) {
		this.dsNatuzOperPgto = dsNatuzOperPgto;
	}
	public String getCdCorpoCpfCnpj() {
		return cdCorpoCpfCnpj;
	}
	public void setCdCorpoCpfCnpj(String cdCorpoCpfCnpj) {
		this.cdCorpoCpfCnpj = cdCorpoCpfCnpj;
	}
	public Long getCdParticipante() {
		return cdParticipante;
	}
	public void setCdParticipante(Long cdParticipante) {
		this.cdParticipante = cdParticipante;
	}
	public String getNmParticipante() {
		return nmParticipante;
	}
	public void setNmParticipante(String nmParticipante) {
		this.nmParticipante = nmParticipante;
	}
	public Integer getCdBanco() {
		return cdBanco;
	}
	public void setCdBanco(Integer cdBanco) {
		this.cdBanco = cdBanco;
	}
	public String getDsBanco() {
		return dsBanco;
	}
	public void setDsBanco(String dsBanco) {
		this.dsBanco = dsBanco;
	}
	public Integer getCdAgencia() {
		return cdAgencia;
	}
	public void setCdAgencia(Integer cdAgencia) {
		this.cdAgencia = cdAgencia;
	}
	public Integer getCdDigitoAgencia() {
		return cdDigitoAgencia;
	}
	public void setCdDigitoAgencia(Integer cdDigitoAgencia) {
		this.cdDigitoAgencia = cdDigitoAgencia;
	}
	public String getDsAgencia() {
		return dsAgencia;
	}
	public void setDsAgencia(String dsAgencia) {
		this.dsAgencia = dsAgencia;
	}
	public Long getCdConta() {
		return cdConta;
	}
	public void setCdConta(Long cdConta) {
		this.cdConta = cdConta;
	}
	public String getCdDigitoConta() {
		return cdDigitoConta;
	}
	public void setCdDigitoConta(String cdDigitoConta) {
		this.cdDigitoConta = cdDigitoConta;
	}
	public Integer getCdTipoConta() {
		return cdTipoConta;
	}
	public void setCdTipoConta(Integer cdTipoConta) {
		this.cdTipoConta = cdTipoConta;
	}
	public String getDsTipoConta() {
		return dsTipoConta;
	}
	public void setDsTipoConta(String dsTipoConta) {
		this.dsTipoConta = dsTipoConta;
	}
	public String getHrInclusaoRegistroHist() {
		return hrInclusaoRegistroHist;
	}
	public void setHrInclusaoRegistroHist(String hrInclusaoRegistroHist) {
		this.hrInclusaoRegistroHist = hrInclusaoRegistroHist;
	}
	public String getDtManutencao() {
		return dtManutencao;
	}
	public void setDtManutencao(String dtManutencao) {
		this.dtManutencao = dtManutencao;
	}
	public String getHrManutencao() {
		return hrManutencao;
	}
	public void setHrManutencao(String hrManutencao) {
		this.hrManutencao = hrManutencao;
	}
	public String getCdUsuario() {
		return cdUsuario;
	}
	public void setCdUsuario(String cdUsuario) {
		this.cdUsuario = cdUsuario;
	}
	public String getDsTipoManutencao() {
		return dsTipoManutencao;
	}
	public void setDsTipoManutencao(String dsTipoManutencao) {
		this.dsTipoManutencao = dsTipoManutencao;
	}

}
