/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listaralcada.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdSistema
     */
    private java.lang.String _cdSistema;

    /**
     * Field _cdProgPagamentoIntegrado
     */
    private java.lang.String _cdProgPagamentoIntegrado;

    /**
     * Field _cdTipoAcaoPagamento
     */
    private int _cdTipoAcaoPagamento = 0;

    /**
     * keeps track of state for field: _cdTipoAcaoPagamento
     */
    private boolean _has_cdTipoAcaoPagamento;

    /**
     * Field _dsTipoAcaoPagamento
     */
    private java.lang.String _dsTipoAcaoPagamento;

    /**
     * Field _cdTipoAcessoAlcada
     */
    private int _cdTipoAcessoAlcada = 0;

    /**
     * keeps track of state for field: _cdTipoAcessoAlcada
     */
    private boolean _has_cdTipoAcessoAlcada;

    /**
     * Field _cdRegraAlcada
     */
    private int _cdRegraAlcada = 0;

    /**
     * keeps track of state for field: _cdRegraAlcada
     */
    private boolean _has_cdRegraAlcada;

    /**
     * Field _dsProdutoServicoOperacao
     */
    private java.lang.String _dsProdutoServicoOperacao;

    /**
     * Field _dsOperacaoProdutoServico
     */
    private java.lang.String _dsOperacaoProdutoServico;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listaralcada.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdRegraAlcada
     * 
     */
    public void deleteCdRegraAlcada()
    {
        this._has_cdRegraAlcada= false;
    } //-- void deleteCdRegraAlcada() 

    /**
     * Method deleteCdTipoAcaoPagamento
     * 
     */
    public void deleteCdTipoAcaoPagamento()
    {
        this._has_cdTipoAcaoPagamento= false;
    } //-- void deleteCdTipoAcaoPagamento() 

    /**
     * Method deleteCdTipoAcessoAlcada
     * 
     */
    public void deleteCdTipoAcessoAlcada()
    {
        this._has_cdTipoAcessoAlcada= false;
    } //-- void deleteCdTipoAcessoAlcada() 

    /**
     * Returns the value of field 'cdProgPagamentoIntegrado'.
     * 
     * @return String
     * @return the value of field 'cdProgPagamentoIntegrado'.
     */
    public java.lang.String getCdProgPagamentoIntegrado()
    {
        return this._cdProgPagamentoIntegrado;
    } //-- java.lang.String getCdProgPagamentoIntegrado() 

    /**
     * Returns the value of field 'cdRegraAlcada'.
     * 
     * @return int
     * @return the value of field 'cdRegraAlcada'.
     */
    public int getCdRegraAlcada()
    {
        return this._cdRegraAlcada;
    } //-- int getCdRegraAlcada() 

    /**
     * Returns the value of field 'cdSistema'.
     * 
     * @return String
     * @return the value of field 'cdSistema'.
     */
    public java.lang.String getCdSistema()
    {
        return this._cdSistema;
    } //-- java.lang.String getCdSistema() 

    /**
     * Returns the value of field 'cdTipoAcaoPagamento'.
     * 
     * @return int
     * @return the value of field 'cdTipoAcaoPagamento'.
     */
    public int getCdTipoAcaoPagamento()
    {
        return this._cdTipoAcaoPagamento;
    } //-- int getCdTipoAcaoPagamento() 

    /**
     * Returns the value of field 'cdTipoAcessoAlcada'.
     * 
     * @return int
     * @return the value of field 'cdTipoAcessoAlcada'.
     */
    public int getCdTipoAcessoAlcada()
    {
        return this._cdTipoAcessoAlcada;
    } //-- int getCdTipoAcessoAlcada() 

    /**
     * Returns the value of field 'dsOperacaoProdutoServico'.
     * 
     * @return String
     * @return the value of field 'dsOperacaoProdutoServico'.
     */
    public java.lang.String getDsOperacaoProdutoServico()
    {
        return this._dsOperacaoProdutoServico;
    } //-- java.lang.String getDsOperacaoProdutoServico() 

    /**
     * Returns the value of field 'dsProdutoServicoOperacao'.
     * 
     * @return String
     * @return the value of field 'dsProdutoServicoOperacao'.
     */
    public java.lang.String getDsProdutoServicoOperacao()
    {
        return this._dsProdutoServicoOperacao;
    } //-- java.lang.String getDsProdutoServicoOperacao() 

    /**
     * Returns the value of field 'dsTipoAcaoPagamento'.
     * 
     * @return String
     * @return the value of field 'dsTipoAcaoPagamento'.
     */
    public java.lang.String getDsTipoAcaoPagamento()
    {
        return this._dsTipoAcaoPagamento;
    } //-- java.lang.String getDsTipoAcaoPagamento() 

    /**
     * Method hasCdRegraAlcada
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdRegraAlcada()
    {
        return this._has_cdRegraAlcada;
    } //-- boolean hasCdRegraAlcada() 

    /**
     * Method hasCdTipoAcaoPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoAcaoPagamento()
    {
        return this._has_cdTipoAcaoPagamento;
    } //-- boolean hasCdTipoAcaoPagamento() 

    /**
     * Method hasCdTipoAcessoAlcada
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoAcessoAlcada()
    {
        return this._has_cdTipoAcessoAlcada;
    } //-- boolean hasCdTipoAcessoAlcada() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdProgPagamentoIntegrado'.
     * 
     * @param cdProgPagamentoIntegrado the value of field
     * 'cdProgPagamentoIntegrado'.
     */
    public void setCdProgPagamentoIntegrado(java.lang.String cdProgPagamentoIntegrado)
    {
        this._cdProgPagamentoIntegrado = cdProgPagamentoIntegrado;
    } //-- void setCdProgPagamentoIntegrado(java.lang.String) 

    /**
     * Sets the value of field 'cdRegraAlcada'.
     * 
     * @param cdRegraAlcada the value of field 'cdRegraAlcada'.
     */
    public void setCdRegraAlcada(int cdRegraAlcada)
    {
        this._cdRegraAlcada = cdRegraAlcada;
        this._has_cdRegraAlcada = true;
    } //-- void setCdRegraAlcada(int) 

    /**
     * Sets the value of field 'cdSistema'.
     * 
     * @param cdSistema the value of field 'cdSistema'.
     */
    public void setCdSistema(java.lang.String cdSistema)
    {
        this._cdSistema = cdSistema;
    } //-- void setCdSistema(java.lang.String) 

    /**
     * Sets the value of field 'cdTipoAcaoPagamento'.
     * 
     * @param cdTipoAcaoPagamento the value of field
     * 'cdTipoAcaoPagamento'.
     */
    public void setCdTipoAcaoPagamento(int cdTipoAcaoPagamento)
    {
        this._cdTipoAcaoPagamento = cdTipoAcaoPagamento;
        this._has_cdTipoAcaoPagamento = true;
    } //-- void setCdTipoAcaoPagamento(int) 

    /**
     * Sets the value of field 'cdTipoAcessoAlcada'.
     * 
     * @param cdTipoAcessoAlcada the value of field
     * 'cdTipoAcessoAlcada'.
     */
    public void setCdTipoAcessoAlcada(int cdTipoAcessoAlcada)
    {
        this._cdTipoAcessoAlcada = cdTipoAcessoAlcada;
        this._has_cdTipoAcessoAlcada = true;
    } //-- void setCdTipoAcessoAlcada(int) 

    /**
     * Sets the value of field 'dsOperacaoProdutoServico'.
     * 
     * @param dsOperacaoProdutoServico the value of field
     * 'dsOperacaoProdutoServico'.
     */
    public void setDsOperacaoProdutoServico(java.lang.String dsOperacaoProdutoServico)
    {
        this._dsOperacaoProdutoServico = dsOperacaoProdutoServico;
    } //-- void setDsOperacaoProdutoServico(java.lang.String) 

    /**
     * Sets the value of field 'dsProdutoServicoOperacao'.
     * 
     * @param dsProdutoServicoOperacao the value of field
     * 'dsProdutoServicoOperacao'.
     */
    public void setDsProdutoServicoOperacao(java.lang.String dsProdutoServicoOperacao)
    {
        this._dsProdutoServicoOperacao = dsProdutoServicoOperacao;
    } //-- void setDsProdutoServicoOperacao(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoAcaoPagamento'.
     * 
     * @param dsTipoAcaoPagamento the value of field
     * 'dsTipoAcaoPagamento'.
     */
    public void setDsTipoAcaoPagamento(java.lang.String dsTipoAcaoPagamento)
    {
        this._dsTipoAcaoPagamento = dsTipoAcaoPagamento;
    } //-- void setDsTipoAcaoPagamento(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listaralcada.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listaralcada.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listaralcada.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listaralcada.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
