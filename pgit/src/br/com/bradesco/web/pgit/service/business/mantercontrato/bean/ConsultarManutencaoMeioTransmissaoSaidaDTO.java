/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantercontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantercontrato.bean;

import java.math.BigDecimal;

/**
 * Nome: ConsultarManutencaoMeioTransmissaoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarManutencaoMeioTransmissaoSaidaDTO {

	/** Atributo codMensagem. */
	private String codMensagem;
	
	/** Atributo mensagem. */
	private String mensagem;
	
	/** Atributo cdPessoaJuridicaContrato. */
	private Long cdPessoaJuridicaContrato;
	
	/** Atributo cdTipoContratoNegocio. */
	private Integer cdTipoContratoNegocio;
	
	/** Atributo nrSequenciaContratoNegocio. */
	private Long nrSequenciaContratoNegocio;
	
	/** Atributo cdTipoManutencaoContrato. */
	private Integer cdTipoManutencaoContrato;
	
	/** Atributo nrManutencaoContratoNegocio. */
	private Long nrManutencaoContratoNegocio;
	
	/** Atributo cdSituacaoManutencaoContrato. */
	private Integer cdSituacaoManutencaoContrato;
	
	/** Atributo dsSituacaoManutContr. */
	private String dsSituacaoManutContr;
	
	/** Atributo cdMotivoManutContr. */
	private Integer cdMotivoManutContr;
	
	/** Atributo dsMotivoManuContr. */
	private String dsMotivoManuContr;
	
	/** Atributo nrAditivoContratoNegocio. */
	private Long nrAditivoContratoNegocio;
	
	/** Atributo cdIndicadorTipoManutencao. */
	private Integer cdIndicadorTipoManutencao;
	
	/** Atributo dsIndicadorTipoManutencao. */
	private String dsIndicadorTipoManutencao;
	
	/** Atributo cdPerfilTrocaArq. */
	private Long cdPerfilTrocaArq;
	
	/** Atributo cdTipoLayoutArquivo. */
	private Integer cdTipoLayoutArquivo;
	
	/** Atributo dsCodTipoLayout. */
	private String dsCodTipoLayout;
	
	/** Atributo dsLayoutProprio. */
	private String dsLayoutProprio;
	
	/** Atributo cdAplicacaoTransmPagamento. */
	private Long cdAplicacaoTransmPagamento;
	
	/** Atributo dsAplicFormat. */
	private String dsAplicFormat;
	
	/** Atributo cdMeioPrincipalRemessa. */
	private Integer cdMeioPrincipalRemessa;
	
	/** Atributo dsCodMeioPrincipalRemessa. */
	private String dsCodMeioPrincipalRemessa;
	
	/** Atributo cdMeioAlternRemessa. */
	private Integer cdMeioAlternRemessa;
	
	/** Atributo dsCodMeioAlternRemessa. */
	private String dsCodMeioAlternRemessa;
	
	/** Atributo cdMeioPrincipalRetorno. */
	private Integer cdMeioPrincipalRetorno;
	
	/** Atributo cdCodMeioPrincipalRetorno. */
	private String cdCodMeioPrincipalRetorno;
	
	/** Atributo cdMeioAltrnRetorno. */
	private Integer cdMeioAltrnRetorno;
	
	/** Atributo dsMeioAltrnRetorno. */
	private String dsMeioAltrnRetorno;
	
	/** Atributo cdResponsavelCustoEmpresa. */
	private Integer cdResponsavelCustoEmpresa;
	
	/** Atributo dsResponsavelCustoEmpresa. */
	private String dsResponsavelCustoEmpresa;
	
	/** Atributo cdPessoaJuridicaParceiro. */
	private Long cdPessoaJuridicaParceiro;
	
	/** Atributo dsEmpresa. */
	private String dsEmpresa;
	
	/** Atributo pcCustoOrganizacaoTransmissao. */
	private BigDecimal pcCustoOrganizacaoTransmissao;
	
	/** Atributo dsUtilizacaoEmpresaVan. */
	private String dsUtilizacaoEmpresaVan;
	
	/** Atributo cdPessoaJuridica. */
	private Long cdPessoaJuridica;
	
	/** Atributo cdUnidadeOrganizacional. */
	private Integer cdUnidadeOrganizacional;
	
	/** Atributo qtMesRegistroTrafg. */
	private Long qtMesRegistroTrafg;
	
	/** Atributo cdOperacaoCanalInclusao. */
	private String cdOperacaoCanalInclusao;
	
	/** Atributo cdTipoCanalInclusao. */
	private Integer cdTipoCanalInclusao;
	
	/** Atributo dsCanalInclusao. */
	private String dsCanalInclusao;
	
	/** Atributo cdUsuarioInclusao. */
	private String cdUsuarioInclusao;
	
	/** Atributo cdUsuarioInclusaoExterno. */
	private String cdUsuarioInclusaoExterno;
	
	/** Atributo hrInclusaoRegistro. */
	private String hrInclusaoRegistro;
	
	/** Atributo cdOperacaoCanalManutencao. */
	private String cdOperacaoCanalManutencao;
	
	/** Atributo cdTipoCanalManutencao. */
	private Integer cdTipoCanalManutencao;
	
	/** Atributo dsCanalManutencao. */
	private String dsCanalManutencao;
	
	/** Atributo cdUsuarioManutencao. */
	private String cdUsuarioManutencao;
	
	/** Atributo cdUsuarioManutencaoexterno. */
	private String cdUsuarioManutencaoexterno;
	
	/** Atributo hrManutencaoRegistro. */
	private String hrManutencaoRegistro;

	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}

	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}

	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}

	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	/**
	 * Get: cdPessoaJuridicaContrato.
	 *
	 * @return cdPessoaJuridicaContrato
	 */
	public Long getCdPessoaJuridicaContrato() {
		return cdPessoaJuridicaContrato;
	}

	/**
	 * Set: cdPessoaJuridicaContrato.
	 *
	 * @param cdPessoaJuridicaContrato the cd pessoa juridica contrato
	 */
	public void setCdPessoaJuridicaContrato(Long cdPessoaJuridicaContrato) {
		this.cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
	}

	/**
	 * Get: cdTipoContratoNegocio.
	 *
	 * @return cdTipoContratoNegocio
	 */
	public Integer getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}

	/**
	 * Set: cdTipoContratoNegocio.
	 *
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}

	/**
	 * Get: nrSequenciaContratoNegocio.
	 *
	 * @return nrSequenciaContratoNegocio
	 */
	public Long getNrSequenciaContratoNegocio() {
		return nrSequenciaContratoNegocio;
	}

	/**
	 * Set: nrSequenciaContratoNegocio.
	 *
	 * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
	 */
	public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}

	/**
	 * Get: cdTipoManutencaoContrato.
	 *
	 * @return cdTipoManutencaoContrato
	 */
	public Integer getCdTipoManutencaoContrato() {
		return cdTipoManutencaoContrato;
	}

	/**
	 * Set: cdTipoManutencaoContrato.
	 *
	 * @param cdTipoManutencaoContrato the cd tipo manutencao contrato
	 */
	public void setCdTipoManutencaoContrato(Integer cdTipoManutencaoContrato) {
		this.cdTipoManutencaoContrato = cdTipoManutencaoContrato;
	}

	/**
	 * Get: nrManutencaoContratoNegocio.
	 *
	 * @return nrManutencaoContratoNegocio
	 */
	public Long getNrManutencaoContratoNegocio() {
		return nrManutencaoContratoNegocio;
	}

	/**
	 * Set: nrManutencaoContratoNegocio.
	 *
	 * @param nrManutencaoContratoNegocio the nr manutencao contrato negocio
	 */
	public void setNrManutencaoContratoNegocio(Long nrManutencaoContratoNegocio) {
		this.nrManutencaoContratoNegocio = nrManutencaoContratoNegocio;
	}

	/**
	 * Get: cdSituacaoManutencaoContrato.
	 *
	 * @return cdSituacaoManutencaoContrato
	 */
	public Integer getCdSituacaoManutencaoContrato() {
		return cdSituacaoManutencaoContrato;
	}

	/**
	 * Set: cdSituacaoManutencaoContrato.
	 *
	 * @param cdSituacaoManutencaoContrato the cd situacao manutencao contrato
	 */
	public void setCdSituacaoManutencaoContrato(
			Integer cdSituacaoManutencaoContrato) {
		this.cdSituacaoManutencaoContrato = cdSituacaoManutencaoContrato;
	}

	/**
	 * Get: dsSituacaoManutContr.
	 *
	 * @return dsSituacaoManutContr
	 */
	public String getDsSituacaoManutContr() {
		return dsSituacaoManutContr;
	}

	/**
	 * Set: dsSituacaoManutContr.
	 *
	 * @param dsSituacaoManutContr the ds situacao manut contr
	 */
	public void setDsSituacaoManutContr(String dsSituacaoManutContr) {
		this.dsSituacaoManutContr = dsSituacaoManutContr;
	}

	/**
	 * Get: cdMotivoManutContr.
	 *
	 * @return cdMotivoManutContr
	 */
	public Integer getCdMotivoManutContr() {
		return cdMotivoManutContr;
	}

	/**
	 * Set: cdMotivoManutContr.
	 *
	 * @param cdMotivoManutContr the cd motivo manut contr
	 */
	public void setCdMotivoManutContr(Integer cdMotivoManutContr) {
		this.cdMotivoManutContr = cdMotivoManutContr;
	}

	/**
	 * Get: dsMotivoManuContr.
	 *
	 * @return dsMotivoManuContr
	 */
	public String getDsMotivoManuContr() {
		return dsMotivoManuContr;
	}

	/**
	 * Set: dsMotivoManuContr.
	 *
	 * @param dsMotivoManuContr the ds motivo manu contr
	 */
	public void setDsMotivoManuContr(String dsMotivoManuContr) {
		this.dsMotivoManuContr = dsMotivoManuContr;
	}

	/**
	 * Get: nrAditivoContratoNegocio.
	 *
	 * @return nrAditivoContratoNegocio
	 */
	public Long getNrAditivoContratoNegocio() {
		return nrAditivoContratoNegocio;
	}

	/**
	 * Set: nrAditivoContratoNegocio.
	 *
	 * @param nrAditivoContratoNegocio the nr aditivo contrato negocio
	 */
	public void setNrAditivoContratoNegocio(Long nrAditivoContratoNegocio) {
		this.nrAditivoContratoNegocio = nrAditivoContratoNegocio;
	}

	/**
	 * Get: cdIndicadorTipoManutencao.
	 *
	 * @return cdIndicadorTipoManutencao
	 */
	public Integer getCdIndicadorTipoManutencao() {
		return cdIndicadorTipoManutencao;
	}

	/**
	 * Set: cdIndicadorTipoManutencao.
	 *
	 * @param cdIndicadorTipoManutencao the cd indicador tipo manutencao
	 */
	public void setCdIndicadorTipoManutencao(Integer cdIndicadorTipoManutencao) {
		this.cdIndicadorTipoManutencao = cdIndicadorTipoManutencao;
	}

	/**
	 * Get: dsIndicadorTipoManutencao.
	 *
	 * @return dsIndicadorTipoManutencao
	 */
	public String getDsIndicadorTipoManutencao() {
		return dsIndicadorTipoManutencao;
	}

	/**
	 * Set: dsIndicadorTipoManutencao.
	 *
	 * @param dsIndicadorTipoManutencao the ds indicador tipo manutencao
	 */
	public void setDsIndicadorTipoManutencao(String dsIndicadorTipoManutencao) {
		this.dsIndicadorTipoManutencao = dsIndicadorTipoManutencao;
	}

	/**
	 * Get: cdPerfilTrocaArq.
	 *
	 * @return cdPerfilTrocaArq
	 */
	public Long getCdPerfilTrocaArq() {
		return cdPerfilTrocaArq;
	}

	/**
	 * Set: cdPerfilTrocaArq.
	 *
	 * @param cdPerfilTrocaArq the cd perfil troca arq
	 */
	public void setCdPerfilTrocaArq(Long cdPerfilTrocaArq) {
		this.cdPerfilTrocaArq = cdPerfilTrocaArq;
	}

	/**
	 * Get: cdTipoLayoutArquivo.
	 *
	 * @return cdTipoLayoutArquivo
	 */
	public Integer getCdTipoLayoutArquivo() {
		return cdTipoLayoutArquivo;
	}

	/**
	 * Set: cdTipoLayoutArquivo.
	 *
	 * @param cdTipoLayoutArquivo the cd tipo layout arquivo
	 */
	public void setCdTipoLayoutArquivo(Integer cdTipoLayoutArquivo) {
		this.cdTipoLayoutArquivo = cdTipoLayoutArquivo;
	}

	/**
	 * Get: dsCodTipoLayout.
	 *
	 * @return dsCodTipoLayout
	 */
	public String getDsCodTipoLayout() {
		return dsCodTipoLayout;
	}

	/**
	 * Set: dsCodTipoLayout.
	 *
	 * @param dsCodTipoLayout the ds cod tipo layout
	 */
	public void setDsCodTipoLayout(String dsCodTipoLayout) {
		this.dsCodTipoLayout = dsCodTipoLayout;
	}

	/**
	 * Get: dsLayoutProprio.
	 *
	 * @return dsLayoutProprio
	 */
	public String getDsLayoutProprio() {
		return dsLayoutProprio;
	}

	/**
	 * Set: dsLayoutProprio.
	 *
	 * @param dsLayoutProprio the ds layout proprio
	 */
	public void setDsLayoutProprio(String dsLayoutProprio) {
		this.dsLayoutProprio = dsLayoutProprio;
	}

	/**
	 * Get: cdAplicacaoTransmPagamento.
	 *
	 * @return cdAplicacaoTransmPagamento
	 */
	public Long getCdAplicacaoTransmPagamento() {
		return cdAplicacaoTransmPagamento;
	}

	/**
	 * Set: cdAplicacaoTransmPagamento.
	 *
	 * @param cdAplicacaoTransmPagamento the cd aplicacao transm pagamento
	 */
	public void setCdAplicacaoTransmPagamento(Long cdAplicacaoTransmPagamento) {
		this.cdAplicacaoTransmPagamento = cdAplicacaoTransmPagamento;
	}

	/**
	 * Get: dsAplicFormat.
	 *
	 * @return dsAplicFormat
	 */
	public String getDsAplicFormat() {
		return dsAplicFormat;
	}

	/**
	 * Set: dsAplicFormat.
	 *
	 * @param dsAplicFormat the ds aplic format
	 */
	public void setDsAplicFormat(String dsAplicFormat) {
		this.dsAplicFormat = dsAplicFormat;
	}

	/**
	 * Get: cdMeioPrincipalRemessa.
	 *
	 * @return cdMeioPrincipalRemessa
	 */
	public Integer getCdMeioPrincipalRemessa() {
		return cdMeioPrincipalRemessa;
	}

	/**
	 * Set: cdMeioPrincipalRemessa.
	 *
	 * @param cdMeioPrincipalRemessa the cd meio principal remessa
	 */
	public void setCdMeioPrincipalRemessa(Integer cdMeioPrincipalRemessa) {
		this.cdMeioPrincipalRemessa = cdMeioPrincipalRemessa;
	}

	/**
	 * Get: dsCodMeioPrincipalRemessa.
	 *
	 * @return dsCodMeioPrincipalRemessa
	 */
	public String getDsCodMeioPrincipalRemessa() {
		return dsCodMeioPrincipalRemessa;
	}

	/**
	 * Set: dsCodMeioPrincipalRemessa.
	 *
	 * @param dsCodMeioPrincipalRemessa the ds cod meio principal remessa
	 */
	public void setDsCodMeioPrincipalRemessa(String dsCodMeioPrincipalRemessa) {
		this.dsCodMeioPrincipalRemessa = dsCodMeioPrincipalRemessa;
	}

	/**
	 * Get: cdMeioAlternRemessa.
	 *
	 * @return cdMeioAlternRemessa
	 */
	public Integer getCdMeioAlternRemessa() {
		return cdMeioAlternRemessa;
	}

	/**
	 * Set: cdMeioAlternRemessa.
	 *
	 * @param cdMeioAlternRemessa the cd meio altern remessa
	 */
	public void setCdMeioAlternRemessa(Integer cdMeioAlternRemessa) {
		this.cdMeioAlternRemessa = cdMeioAlternRemessa;
	}

	/**
	 * Get: dsCodMeioAlternRemessa.
	 *
	 * @return dsCodMeioAlternRemessa
	 */
	public String getDsCodMeioAlternRemessa() {
		return dsCodMeioAlternRemessa;
	}

	/**
	 * Set: dsCodMeioAlternRemessa.
	 *
	 * @param dsCodMeioAlternRemessa the ds cod meio altern remessa
	 */
	public void setDsCodMeioAlternRemessa(String dsCodMeioAlternRemessa) {
		this.dsCodMeioAlternRemessa = dsCodMeioAlternRemessa;
	}

	/**
	 * Get: cdMeioPrincipalRetorno.
	 *
	 * @return cdMeioPrincipalRetorno
	 */
	public Integer getCdMeioPrincipalRetorno() {
		return cdMeioPrincipalRetorno;
	}

	/**
	 * Set: cdMeioPrincipalRetorno.
	 *
	 * @param cdMeioPrincipalRetorno the cd meio principal retorno
	 */
	public void setCdMeioPrincipalRetorno(Integer cdMeioPrincipalRetorno) {
		this.cdMeioPrincipalRetorno = cdMeioPrincipalRetorno;
	}

	/**
	 * Get: cdCodMeioPrincipalRetorno.
	 *
	 * @return cdCodMeioPrincipalRetorno
	 */
	public String getCdCodMeioPrincipalRetorno() {
		return cdCodMeioPrincipalRetorno;
	}

	/**
	 * Set: cdCodMeioPrincipalRetorno.
	 *
	 * @param cdCodMeioPrincipalRetorno the cd cod meio principal retorno
	 */
	public void setCdCodMeioPrincipalRetorno(String cdCodMeioPrincipalRetorno) {
		this.cdCodMeioPrincipalRetorno = cdCodMeioPrincipalRetorno;
	}

	/**
	 * Get: cdMeioAltrnRetorno.
	 *
	 * @return cdMeioAltrnRetorno
	 */
	public Integer getCdMeioAltrnRetorno() {
		return cdMeioAltrnRetorno;
	}

	/**
	 * Set: cdMeioAltrnRetorno.
	 *
	 * @param cdMeioAltrnRetorno the cd meio altrn retorno
	 */
	public void setCdMeioAltrnRetorno(Integer cdMeioAltrnRetorno) {
		this.cdMeioAltrnRetorno = cdMeioAltrnRetorno;
	}

	/**
	 * Get: dsMeioAltrnRetorno.
	 *
	 * @return dsMeioAltrnRetorno
	 */
	public String getDsMeioAltrnRetorno() {
		return dsMeioAltrnRetorno;
	}

	/**
	 * Set: dsMeioAltrnRetorno.
	 *
	 * @param dsMeioAltrnRetorno the ds meio altrn retorno
	 */
	public void setDsMeioAltrnRetorno(String dsMeioAltrnRetorno) {
		this.dsMeioAltrnRetorno = dsMeioAltrnRetorno;
	}

	/**
	 * Get: cdResponsavelCustoEmpresa.
	 *
	 * @return cdResponsavelCustoEmpresa
	 */
	public Integer getCdResponsavelCustoEmpresa() {
		return cdResponsavelCustoEmpresa;
	}

	/**
	 * Set: cdResponsavelCustoEmpresa.
	 *
	 * @param cdResponsavelCustoEmpresa the cd responsavel custo empresa
	 */
	public void setCdResponsavelCustoEmpresa(Integer cdResponsavelCustoEmpresa) {
		this.cdResponsavelCustoEmpresa = cdResponsavelCustoEmpresa;
	}

	/**
	 * Get: dsResponsavelCustoEmpresa.
	 *
	 * @return dsResponsavelCustoEmpresa
	 */
	public String getDsResponsavelCustoEmpresa() {
		return dsResponsavelCustoEmpresa;
	}

	/**
	 * Set: dsResponsavelCustoEmpresa.
	 *
	 * @param dsResponsavelCustoEmpresa the ds responsavel custo empresa
	 */
	public void setDsResponsavelCustoEmpresa(String dsResponsavelCustoEmpresa) {
		this.dsResponsavelCustoEmpresa = dsResponsavelCustoEmpresa;
	}

	/**
	 * Get: cdPessoaJuridicaParceiro.
	 *
	 * @return cdPessoaJuridicaParceiro
	 */
	public Long getCdPessoaJuridicaParceiro() {
		return cdPessoaJuridicaParceiro;
	}

	/**
	 * Set: cdPessoaJuridicaParceiro.
	 *
	 * @param cdPessoaJuridicaParceiro the cd pessoa juridica parceiro
	 */
	public void setCdPessoaJuridicaParceiro(Long cdPessoaJuridicaParceiro) {
		this.cdPessoaJuridicaParceiro = cdPessoaJuridicaParceiro;
	}

	/**
	 * Get: dsEmpresa.
	 *
	 * @return dsEmpresa
	 */
	public String getDsEmpresa() {
		return dsEmpresa;
	}

	/**
	 * Set: dsEmpresa.
	 *
	 * @param dsEmpresa the ds empresa
	 */
	public void setDsEmpresa(String dsEmpresa) {
		this.dsEmpresa = dsEmpresa;
	}

	/**
	 * Get: pcCustoOrganizacaoTransmissao.
	 *
	 * @return pcCustoOrganizacaoTransmissao
	 */
	public BigDecimal getPcCustoOrganizacaoTransmissao() {
		return pcCustoOrganizacaoTransmissao;
	}

	/**
	 * Set: pcCustoOrganizacaoTransmissao.
	 *
	 * @param pcCustoOrganizacaoTransmissao the pc custo organizacao transmissao
	 */
	public void setPcCustoOrganizacaoTransmissao(
			BigDecimal pcCustoOrganizacaoTransmissao) {
		this.pcCustoOrganizacaoTransmissao = pcCustoOrganizacaoTransmissao;
	}

	/**
	 * Get: dsUtilizacaoEmpresaVan.
	 *
	 * @return dsUtilizacaoEmpresaVan
	 */
	public String getDsUtilizacaoEmpresaVan() {
		return dsUtilizacaoEmpresaVan;
	}

	/**
	 * Set: dsUtilizacaoEmpresaVan.
	 *
	 * @param dsUtilizacaoEmpresaVan the ds utilizacao empresa van
	 */
	public void setDsUtilizacaoEmpresaVan(String dsUtilizacaoEmpresaVan) {
		this.dsUtilizacaoEmpresaVan = dsUtilizacaoEmpresaVan;
	}

	/**
	 * Get: cdPessoaJuridica.
	 *
	 * @return cdPessoaJuridica
	 */
	public Long getCdPessoaJuridica() {
		return cdPessoaJuridica;
	}

	/**
	 * Set: cdPessoaJuridica.
	 *
	 * @param cdPessoaJuridica the cd pessoa juridica
	 */
	public void setCdPessoaJuridica(Long cdPessoaJuridica) {
		this.cdPessoaJuridica = cdPessoaJuridica;
	}

	/**
	 * Get: cdUnidadeOrganizacional.
	 *
	 * @return cdUnidadeOrganizacional
	 */
	public Integer getCdUnidadeOrganizacional() {
		return cdUnidadeOrganizacional;
	}

	/**
	 * Set: cdUnidadeOrganizacional.
	 *
	 * @param cdUnidadeOrganizacional the cd unidade organizacional
	 */
	public void setCdUnidadeOrganizacional(Integer cdUnidadeOrganizacional) {
		this.cdUnidadeOrganizacional = cdUnidadeOrganizacional;
	}

	/**
	 * Get: qtMesRegistroTrafg.
	 *
	 * @return qtMesRegistroTrafg
	 */
	public Long getQtMesRegistroTrafg() {
		return qtMesRegistroTrafg;
	}

	/**
	 * Set: qtMesRegistroTrafg.
	 *
	 * @param qtMesRegistroTrafg the qt mes registro trafg
	 */
	public void setQtMesRegistroTrafg(Long qtMesRegistroTrafg) {
		this.qtMesRegistroTrafg = qtMesRegistroTrafg;
	}

	/**
	 * Get: cdOperacaoCanalInclusao.
	 *
	 * @return cdOperacaoCanalInclusao
	 */
	public String getCdOperacaoCanalInclusao() {
		return cdOperacaoCanalInclusao;
	}

	/**
	 * Set: cdOperacaoCanalInclusao.
	 *
	 * @param cdOperacaoCanalInclusao the cd operacao canal inclusao
	 */
	public void setCdOperacaoCanalInclusao(String cdOperacaoCanalInclusao) {
		this.cdOperacaoCanalInclusao = cdOperacaoCanalInclusao;
	}

	/**
	 * Get: cdTipoCanalInclusao.
	 *
	 * @return cdTipoCanalInclusao
	 */
	public Integer getCdTipoCanalInclusao() {
		return cdTipoCanalInclusao;
	}

	/**
	 * Set: cdTipoCanalInclusao.
	 *
	 * @param cdTipoCanalInclusao the cd tipo canal inclusao
	 */
	public void setCdTipoCanalInclusao(Integer cdTipoCanalInclusao) {
		this.cdTipoCanalInclusao = cdTipoCanalInclusao;
	}

	/**
	 * Get: dsCanalInclusao.
	 *
	 * @return dsCanalInclusao
	 */
	public String getDsCanalInclusao() {
		return dsCanalInclusao;
	}

	/**
	 * Set: dsCanalInclusao.
	 *
	 * @param dsCanalInclusao the ds canal inclusao
	 */
	public void setDsCanalInclusao(String dsCanalInclusao) {
		this.dsCanalInclusao = dsCanalInclusao;
	}

	/**
	 * Get: cdUsuarioInclusao.
	 *
	 * @return cdUsuarioInclusao
	 */
	public String getCdUsuarioInclusao() {
		return cdUsuarioInclusao;
	}

	/**
	 * Set: cdUsuarioInclusao.
	 *
	 * @param cdUsuarioInclusao the cd usuario inclusao
	 */
	public void setCdUsuarioInclusao(String cdUsuarioInclusao) {
		this.cdUsuarioInclusao = cdUsuarioInclusao;
	}

	/**
	 * Get: cdUsuarioInclusaoExterno.
	 *
	 * @return cdUsuarioInclusaoExterno
	 */
	public String getCdUsuarioInclusaoExterno() {
		return cdUsuarioInclusaoExterno;
	}

	/**
	 * Set: cdUsuarioInclusaoExterno.
	 *
	 * @param cdUsuarioInclusaoExterno the cd usuario inclusao externo
	 */
	public void setCdUsuarioInclusaoExterno(String cdUsuarioInclusaoExterno) {
		this.cdUsuarioInclusaoExterno = cdUsuarioInclusaoExterno;
	}

	/**
	 * Get: hrInclusaoRegistro.
	 *
	 * @return hrInclusaoRegistro
	 */
	public String getHrInclusaoRegistro() {
		return hrInclusaoRegistro;
	}

	/**
	 * Set: hrInclusaoRegistro.
	 *
	 * @param hrInclusaoRegistro the hr inclusao registro
	 */
	public void setHrInclusaoRegistro(String hrInclusaoRegistro) {
		this.hrInclusaoRegistro = hrInclusaoRegistro;
	}

	/**
	 * Get: cdOperacaoCanalManutencao.
	 *
	 * @return cdOperacaoCanalManutencao
	 */
	public String getCdOperacaoCanalManutencao() {
		return cdOperacaoCanalManutencao;
	}

	/**
	 * Set: cdOperacaoCanalManutencao.
	 *
	 * @param cdOperacaoCanalManutencao the cd operacao canal manutencao
	 */
	public void setCdOperacaoCanalManutencao(String cdOperacaoCanalManutencao) {
		this.cdOperacaoCanalManutencao = cdOperacaoCanalManutencao;
	}


	/**
	 * Get: dsCanalManutencao.
	 *
	 * @return dsCanalManutencao
	 */
	public String getDsCanalManutencao() {
		return dsCanalManutencao;
	}

	/**
	 * Set: dsCanalManutencao.
	 *
	 * @param dsCanalManutencao the ds canal manutencao
	 */
	public void setDsCanalManutencao(String dsCanalManutencao) {
		this.dsCanalManutencao = dsCanalManutencao;
	}

	/**
	 * Get: cdUsuarioManutencao.
	 *
	 * @return cdUsuarioManutencao
	 */
	public String getCdUsuarioManutencao() {
		return cdUsuarioManutencao;
	}

	/**
	 * Set: cdUsuarioManutencao.
	 *
	 * @param cdUsuarioManutencao the cd usuario manutencao
	 */
	public void setCdUsuarioManutencao(String cdUsuarioManutencao) {
		this.cdUsuarioManutencao = cdUsuarioManutencao;
	}

	/**
	 * Get: cdUsuarioManutencaoexterno.
	 *
	 * @return cdUsuarioManutencaoexterno
	 */
	public String getCdUsuarioManutencaoexterno() {
		return cdUsuarioManutencaoexterno;
	}

	/**
	 * Set: cdUsuarioManutencaoexterno.
	 *
	 * @param cdUsuarioManutencaoexterno the cd usuario manutencaoexterno
	 */
	public void setCdUsuarioManutencaoexterno(String cdUsuarioManutencaoexterno) {
		this.cdUsuarioManutencaoexterno = cdUsuarioManutencaoexterno;
	}

	/**
	 * Get: hrManutencaoRegistro.
	 *
	 * @return hrManutencaoRegistro
	 */
	public String getHrManutencaoRegistro() {
		return hrManutencaoRegistro;
	}

	/**
	 * Set: hrManutencaoRegistro.
	 *
	 * @param hrManutencaoRegistro the hr manutencao registro
	 */
	public void setHrManutencaoRegistro(String hrManutencaoRegistro) {
		this.hrManutencaoRegistro = hrManutencaoRegistro;
	}

	/**
	 * Set: cdTipoCanalManutencao.
	 *
	 * @param cdTipoCanalManutencao the cd tipo canal manutencao
	 */
	public void setCdTipoCanalManutencao(Integer cdTipoCanalManutencao) {
		this.cdTipoCanalManutencao = cdTipoCanalManutencao;
	}

	/**
	 * Get: cdTipoCanalManutencao.
	 *
	 * @return cdTipoCanalManutencao
	 */
	public Integer getCdTipoCanalManutencao() {
		return cdTipoCanalManutencao;
	}
}
