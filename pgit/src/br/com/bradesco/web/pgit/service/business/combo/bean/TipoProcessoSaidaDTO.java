/*
 * Nome: br.com.bradesco.web.pgit.service.business.combo.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.combo.bean;

/**
 * Nome: TipoProcessoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class TipoProcessoSaidaDTO {
	
	/** Atributo codMensagem. */
	private String codMensagem;
	
	/** Atributo mensagem. */
	private String mensagem;
	
	/** Atributo cdTipoProcesso. */
	private int cdTipoProcesso;
	
	/** Atributo dsTipoProcesso. */
	private String dsTipoProcesso;
	
	
	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}
	
	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}
	
	/**
	 * Get: dsTipoProcesso.
	 *
	 * @return dsTipoProcesso
	 */
	public String getDsTipoProcesso() {
		return dsTipoProcesso;
	}
	
	/**
	 * Set: dsTipoProcesso.
	 *
	 * @param dsTipoProcesso the ds tipo processo
	 */
	public void setDsTipoProcesso(String dsTipoProcesso) {
		this.dsTipoProcesso = dsTipoProcesso;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	/**
	 * Get: cdTipoProcesso.
	 *
	 * @return cdTipoProcesso
	 */
	public int getCdTipoProcesso() {
		return cdTipoProcesso;
	}
	
	/**
	 * Set: cdTipoProcesso.
	 *
	 * @param cdTipoProcesso the cd tipo processo
	 */
	public void setCdTipoProcesso(int cdTipoProcesso) {
		this.cdTipoProcesso = cdTipoProcesso;
	}
	
	/**
	 * (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof TipoProcessoSaidaDTO)) {
			return false;
		}

		TipoProcessoSaidaDTO other = (TipoProcessoSaidaDTO) obj;
		return this.getCdTipoProcesso() == other.getCdTipoProcesso();
	}

	/**
	 * (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return getCdTipoProcesso();
	}


}
