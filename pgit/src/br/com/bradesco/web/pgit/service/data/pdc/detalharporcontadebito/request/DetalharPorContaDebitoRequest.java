/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.detalharporcontadebito.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class DetalharPorContaDebitoRequest.
 * 
 * @version $Revision$ $Date$
 */
public class DetalharPorContaDebitoRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _nrOcorrencias
     */
    private int _nrOcorrencias = 0;

    /**
     * keeps track of state for field: _nrOcorrencias
     */
    private boolean _has_nrOcorrencias;

    /**
     * Field _cdCpfCnpj
     */
    private long _cdCpfCnpj = 0;

    /**
     * keeps track of state for field: _cdCpfCnpj
     */
    private boolean _has_cdCpfCnpj;

    /**
     * Field _cdFilialCpfCnpj
     */
    private int _cdFilialCpfCnpj = 0;

    /**
     * keeps track of state for field: _cdFilialCpfCnpj
     */
    private boolean _has_cdFilialCpfCnpj;

    /**
     * Field _cdDigitoCpfCnpj
     */
    private int _cdDigitoCpfCnpj = 0;

    /**
     * keeps track of state for field: _cdDigitoCpfCnpj
     */
    private boolean _has_cdDigitoCpfCnpj;

    /**
     * Field _dsComplemento
     */
    private java.lang.String _dsComplemento;

    /**
     * Field _cdUnidadeOrganizacional
     */
    private int _cdUnidadeOrganizacional = 0;

    /**
     * keeps track of state for field: _cdUnidadeOrganizacional
     */
    private boolean _has_cdUnidadeOrganizacional;

    /**
     * Field _cdPessoaJuridicaContrato
     */
    private long _cdPessoaJuridicaContrato = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridicaContrato
     */
    private boolean _has_cdPessoaJuridicaContrato;

    /**
     * Field _cdTipoContratoNegocio
     */
    private int _cdTipoContratoNegocio = 0;

    /**
     * keeps track of state for field: _cdTipoContratoNegocio
     */
    private boolean _has_cdTipoContratoNegocio;

    /**
     * Field _nrSequenciaContratoNegocio
     */
    private long _nrSequenciaContratoNegocio = 0;

    /**
     * keeps track of state for field: _nrSequenciaContratoNegocio
     */
    private boolean _has_nrSequenciaContratoNegocio;

    /**
     * Field _cdProdutoServicoOperacao
     */
    private int _cdProdutoServicoOperacao = 0;

    /**
     * keeps track of state for field: _cdProdutoServicoOperacao
     */
    private boolean _has_cdProdutoServicoOperacao;

    /**
     * Field _cdProdutoServicoRelacionado
     */
    private int _cdProdutoServicoRelacionado = 0;

    /**
     * keeps track of state for field: _cdProdutoServicoRelacionado
     */
    private boolean _has_cdProdutoServicoRelacionado;


      //----------------/
     //- Constructors -/
    //----------------/

    public DetalharPorContaDebitoRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharporcontadebito.request.DetalharPorContaDebitoRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdCpfCnpj
     * 
     */
    public void deleteCdCpfCnpj()
    {
        this._has_cdCpfCnpj= false;
    } //-- void deleteCdCpfCnpj() 

    /**
     * Method deleteCdDigitoCpfCnpj
     * 
     */
    public void deleteCdDigitoCpfCnpj()
    {
        this._has_cdDigitoCpfCnpj= false;
    } //-- void deleteCdDigitoCpfCnpj() 

    /**
     * Method deleteCdFilialCpfCnpj
     * 
     */
    public void deleteCdFilialCpfCnpj()
    {
        this._has_cdFilialCpfCnpj= false;
    } //-- void deleteCdFilialCpfCnpj() 

    /**
     * Method deleteCdPessoaJuridicaContrato
     * 
     */
    public void deleteCdPessoaJuridicaContrato()
    {
        this._has_cdPessoaJuridicaContrato= false;
    } //-- void deleteCdPessoaJuridicaContrato() 

    /**
     * Method deleteCdProdutoServicoOperacao
     * 
     */
    public void deleteCdProdutoServicoOperacao()
    {
        this._has_cdProdutoServicoOperacao= false;
    } //-- void deleteCdProdutoServicoOperacao() 

    /**
     * Method deleteCdProdutoServicoRelacionado
     * 
     */
    public void deleteCdProdutoServicoRelacionado()
    {
        this._has_cdProdutoServicoRelacionado= false;
    } //-- void deleteCdProdutoServicoRelacionado() 

    /**
     * Method deleteCdTipoContratoNegocio
     * 
     */
    public void deleteCdTipoContratoNegocio()
    {
        this._has_cdTipoContratoNegocio= false;
    } //-- void deleteCdTipoContratoNegocio() 

    /**
     * Method deleteCdUnidadeOrganizacional
     * 
     */
    public void deleteCdUnidadeOrganizacional()
    {
        this._has_cdUnidadeOrganizacional= false;
    } //-- void deleteCdUnidadeOrganizacional() 

    /**
     * Method deleteNrOcorrencias
     * 
     */
    public void deleteNrOcorrencias()
    {
        this._has_nrOcorrencias= false;
    } //-- void deleteNrOcorrencias() 

    /**
     * Method deleteNrSequenciaContratoNegocio
     * 
     */
    public void deleteNrSequenciaContratoNegocio()
    {
        this._has_nrSequenciaContratoNegocio= false;
    } //-- void deleteNrSequenciaContratoNegocio() 

    /**
     * Returns the value of field 'cdCpfCnpj'.
     * 
     * @return long
     * @return the value of field 'cdCpfCnpj'.
     */
    public long getCdCpfCnpj()
    {
        return this._cdCpfCnpj;
    } //-- long getCdCpfCnpj() 

    /**
     * Returns the value of field 'cdDigitoCpfCnpj'.
     * 
     * @return int
     * @return the value of field 'cdDigitoCpfCnpj'.
     */
    public int getCdDigitoCpfCnpj()
    {
        return this._cdDigitoCpfCnpj;
    } //-- int getCdDigitoCpfCnpj() 

    /**
     * Returns the value of field 'cdFilialCpfCnpj'.
     * 
     * @return int
     * @return the value of field 'cdFilialCpfCnpj'.
     */
    public int getCdFilialCpfCnpj()
    {
        return this._cdFilialCpfCnpj;
    } //-- int getCdFilialCpfCnpj() 

    /**
     * Returns the value of field 'cdPessoaJuridicaContrato'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridicaContrato'.
     */
    public long getCdPessoaJuridicaContrato()
    {
        return this._cdPessoaJuridicaContrato;
    } //-- long getCdPessoaJuridicaContrato() 

    /**
     * Returns the value of field 'cdProdutoServicoOperacao'.
     * 
     * @return int
     * @return the value of field 'cdProdutoServicoOperacao'.
     */
    public int getCdProdutoServicoOperacao()
    {
        return this._cdProdutoServicoOperacao;
    } //-- int getCdProdutoServicoOperacao() 

    /**
     * Returns the value of field 'cdProdutoServicoRelacionado'.
     * 
     * @return int
     * @return the value of field 'cdProdutoServicoRelacionado'.
     */
    public int getCdProdutoServicoRelacionado()
    {
        return this._cdProdutoServicoRelacionado;
    } //-- int getCdProdutoServicoRelacionado() 

    /**
     * Returns the value of field 'cdTipoContratoNegocio'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoNegocio'.
     */
    public int getCdTipoContratoNegocio()
    {
        return this._cdTipoContratoNegocio;
    } //-- int getCdTipoContratoNegocio() 

    /**
     * Returns the value of field 'cdUnidadeOrganizacional'.
     * 
     * @return int
     * @return the value of field 'cdUnidadeOrganizacional'.
     */
    public int getCdUnidadeOrganizacional()
    {
        return this._cdUnidadeOrganizacional;
    } //-- int getCdUnidadeOrganizacional() 

    /**
     * Returns the value of field 'dsComplemento'.
     * 
     * @return String
     * @return the value of field 'dsComplemento'.
     */
    public java.lang.String getDsComplemento()
    {
        return this._dsComplemento;
    } //-- java.lang.String getDsComplemento() 

    /**
     * Returns the value of field 'nrOcorrencias'.
     * 
     * @return int
     * @return the value of field 'nrOcorrencias'.
     */
    public int getNrOcorrencias()
    {
        return this._nrOcorrencias;
    } //-- int getNrOcorrencias() 

    /**
     * Returns the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @return long
     * @return the value of field 'nrSequenciaContratoNegocio'.
     */
    public long getNrSequenciaContratoNegocio()
    {
        return this._nrSequenciaContratoNegocio;
    } //-- long getNrSequenciaContratoNegocio() 

    /**
     * Method hasCdCpfCnpj
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCpfCnpj()
    {
        return this._has_cdCpfCnpj;
    } //-- boolean hasCdCpfCnpj() 

    /**
     * Method hasCdDigitoCpfCnpj
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdDigitoCpfCnpj()
    {
        return this._has_cdDigitoCpfCnpj;
    } //-- boolean hasCdDigitoCpfCnpj() 

    /**
     * Method hasCdFilialCpfCnpj
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFilialCpfCnpj()
    {
        return this._has_cdFilialCpfCnpj;
    } //-- boolean hasCdFilialCpfCnpj() 

    /**
     * Method hasCdPessoaJuridicaContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridicaContrato()
    {
        return this._has_cdPessoaJuridicaContrato;
    } //-- boolean hasCdPessoaJuridicaContrato() 

    /**
     * Method hasCdProdutoServicoOperacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdutoServicoOperacao()
    {
        return this._has_cdProdutoServicoOperacao;
    } //-- boolean hasCdProdutoServicoOperacao() 

    /**
     * Method hasCdProdutoServicoRelacionado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdutoServicoRelacionado()
    {
        return this._has_cdProdutoServicoRelacionado;
    } //-- boolean hasCdProdutoServicoRelacionado() 

    /**
     * Method hasCdTipoContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoNegocio()
    {
        return this._has_cdTipoContratoNegocio;
    } //-- boolean hasCdTipoContratoNegocio() 

    /**
     * Method hasCdUnidadeOrganizacional
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdUnidadeOrganizacional()
    {
        return this._has_cdUnidadeOrganizacional;
    } //-- boolean hasCdUnidadeOrganizacional() 

    /**
     * Method hasNrOcorrencias
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrOcorrencias()
    {
        return this._has_nrOcorrencias;
    } //-- boolean hasNrOcorrencias() 

    /**
     * Method hasNrSequenciaContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaContratoNegocio()
    {
        return this._has_nrSequenciaContratoNegocio;
    } //-- boolean hasNrSequenciaContratoNegocio() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdCpfCnpj'.
     * 
     * @param cdCpfCnpj the value of field 'cdCpfCnpj'.
     */
    public void setCdCpfCnpj(long cdCpfCnpj)
    {
        this._cdCpfCnpj = cdCpfCnpj;
        this._has_cdCpfCnpj = true;
    } //-- void setCdCpfCnpj(long) 

    /**
     * Sets the value of field 'cdDigitoCpfCnpj'.
     * 
     * @param cdDigitoCpfCnpj the value of field 'cdDigitoCpfCnpj'.
     */
    public void setCdDigitoCpfCnpj(int cdDigitoCpfCnpj)
    {
        this._cdDigitoCpfCnpj = cdDigitoCpfCnpj;
        this._has_cdDigitoCpfCnpj = true;
    } //-- void setCdDigitoCpfCnpj(int) 

    /**
     * Sets the value of field 'cdFilialCpfCnpj'.
     * 
     * @param cdFilialCpfCnpj the value of field 'cdFilialCpfCnpj'.
     */
    public void setCdFilialCpfCnpj(int cdFilialCpfCnpj)
    {
        this._cdFilialCpfCnpj = cdFilialCpfCnpj;
        this._has_cdFilialCpfCnpj = true;
    } //-- void setCdFilialCpfCnpj(int) 

    /**
     * Sets the value of field 'cdPessoaJuridicaContrato'.
     * 
     * @param cdPessoaJuridicaContrato the value of field
     * 'cdPessoaJuridicaContrato'.
     */
    public void setCdPessoaJuridicaContrato(long cdPessoaJuridicaContrato)
    {
        this._cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
        this._has_cdPessoaJuridicaContrato = true;
    } //-- void setCdPessoaJuridicaContrato(long) 

    /**
     * Sets the value of field 'cdProdutoServicoOperacao'.
     * 
     * @param cdProdutoServicoOperacao the value of field
     * 'cdProdutoServicoOperacao'.
     */
    public void setCdProdutoServicoOperacao(int cdProdutoServicoOperacao)
    {
        this._cdProdutoServicoOperacao = cdProdutoServicoOperacao;
        this._has_cdProdutoServicoOperacao = true;
    } //-- void setCdProdutoServicoOperacao(int) 

    /**
     * Sets the value of field 'cdProdutoServicoRelacionado'.
     * 
     * @param cdProdutoServicoRelacionado the value of field
     * 'cdProdutoServicoRelacionado'.
     */
    public void setCdProdutoServicoRelacionado(int cdProdutoServicoRelacionado)
    {
        this._cdProdutoServicoRelacionado = cdProdutoServicoRelacionado;
        this._has_cdProdutoServicoRelacionado = true;
    } //-- void setCdProdutoServicoRelacionado(int) 

    /**
     * Sets the value of field 'cdTipoContratoNegocio'.
     * 
     * @param cdTipoContratoNegocio the value of field
     * 'cdTipoContratoNegocio'.
     */
    public void setCdTipoContratoNegocio(int cdTipoContratoNegocio)
    {
        this._cdTipoContratoNegocio = cdTipoContratoNegocio;
        this._has_cdTipoContratoNegocio = true;
    } //-- void setCdTipoContratoNegocio(int) 

    /**
     * Sets the value of field 'cdUnidadeOrganizacional'.
     * 
     * @param cdUnidadeOrganizacional the value of field
     * 'cdUnidadeOrganizacional'.
     */
    public void setCdUnidadeOrganizacional(int cdUnidadeOrganizacional)
    {
        this._cdUnidadeOrganizacional = cdUnidadeOrganizacional;
        this._has_cdUnidadeOrganizacional = true;
    } //-- void setCdUnidadeOrganizacional(int) 

    /**
     * Sets the value of field 'dsComplemento'.
     * 
     * @param dsComplemento the value of field 'dsComplemento'.
     */
    public void setDsComplemento(java.lang.String dsComplemento)
    {
        this._dsComplemento = dsComplemento;
    } //-- void setDsComplemento(java.lang.String) 

    /**
     * Sets the value of field 'nrOcorrencias'.
     * 
     * @param nrOcorrencias the value of field 'nrOcorrencias'.
     */
    public void setNrOcorrencias(int nrOcorrencias)
    {
        this._nrOcorrencias = nrOcorrencias;
        this._has_nrOcorrencias = true;
    } //-- void setNrOcorrencias(int) 

    /**
     * Sets the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @param nrSequenciaContratoNegocio the value of field
     * 'nrSequenciaContratoNegocio'.
     */
    public void setNrSequenciaContratoNegocio(long nrSequenciaContratoNegocio)
    {
        this._nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
        this._has_nrSequenciaContratoNegocio = true;
    } //-- void setNrSequenciaContratoNegocio(long) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return DetalharPorContaDebitoRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.detalharporcontadebito.request.DetalharPorContaDebitoRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.detalharporcontadebito.request.DetalharPorContaDebitoRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.detalharporcontadebito.request.DetalharPorContaDebitoRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharporcontadebito.request.DetalharPorContaDebitoRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
