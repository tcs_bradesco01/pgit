/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantercontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 25/05/2016
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantercontrato.bean;

/**
 * Nome: VerificarAtributosDadosBasicosEntradaDto
 * <p>
 * Prop�sito:
 * </p>
 * 	
 * @author : todo!
 * 
 * @version :
 */
public class VerificarAtributosDadosBasicosEntradaDto {

    /** Atributo cdPessoaJuridicaContrato. */
    private Long cdPessoaJuridicaContrato = null;

    /** Atributo cdTipoContratoNegocio. */
    private Integer cdTipoContratoNegocio = null;

    /** Atributo nrSequenciaContratoNegocio. */
    private Long nrSequenciaContratoNegocio = null;

    /**
     * Nome: getCdPessoaJuridicaContrato
     *
     * @return cdPessoaJuridicaContrato
     */
    public Long getCdPessoaJuridicaContrato() {
        return cdPessoaJuridicaContrato;
    }

    /**
     * Nome: setCdPessoaJuridicaContrato
     *
     * @param cdPessoaJuridicaContrato
     */
    public void setCdPessoaJuridicaContrato(Long cdPessoaJuridicaContrato) {
        this.cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
    }

    /**
     * Nome: getCdTipoContratoNegocio
     *
     * @return cdTipoContratoNegocio
     */
    public Integer getCdTipoContratoNegocio() {
        return cdTipoContratoNegocio;
    }

    /**
     * Nome: setCdTipoContratoNegocio
     *
     * @param cdTipoContratoNegocio
     */
    public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
        this.cdTipoContratoNegocio = cdTipoContratoNegocio;
    }

    /**
     * Nome: getNrSequenciaContratoNegocio
     *
     * @return nrSequenciaContratoNegocio
     */
    public Long getNrSequenciaContratoNegocio() {
        return nrSequenciaContratoNegocio;
    }

    /**
     * Nome: setNrSequenciaContratoNegocio
     *
     * @param nrSequenciaContratoNegocio
     */
    public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
        this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
    }
}
