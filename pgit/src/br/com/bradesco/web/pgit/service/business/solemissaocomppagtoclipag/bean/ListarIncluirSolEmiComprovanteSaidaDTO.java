/*
 * Nome: br.com.bradesco.web.pgit.service.business.solemissaocomppagtoclipag.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.solemissaocomppagtoclipag.bean;

import java.math.BigDecimal;

/**
 * Nome: ListarIncluirSolEmiComprovanteSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarIncluirSolEmiComprovanteSaidaDTO {
    
    /** Atributo codMensagem. */
    private String codMensagem;

    /** Atributo mensagem. */
    private String mensagem;

    /** Atributo numeroConsultas. */
    private Integer numeroConsultas;

    /** Atributo cdProdutoServicoOperacao. */
    private Integer cdProdutoServicoOperacao;

    /** Atributo dsResumoProdutoServico. */
    private String dsResumoProdutoServico;

    /** Atributo cdProdutoServicoRelacionado. */
    private Integer cdProdutoServicoRelacionado;

    /** Atributo dsOperacaoProdutoServico. */
    private String dsOperacaoProdutoServico;

    /** Atributo cdControlePagamento. */
    private String cdControlePagamento;

    /** Atributo dtCreditoPagamento. */
    private String dtCreditoPagamento;

    /** Atributo vlEfetivoPagamento. */
    private BigDecimal vlEfetivoPagamento;

    /** Atributo cdInscricaoFavorecido. */
    private Long cdInscricaoFavorecido;

    /** Atributo dsFavorecido. */
    private String dsFavorecido;

    /** Atributo cdBancoDebito. */
    private Integer cdBancoDebito;

    /** Atributo cdAgenciaDebito. */
    private Integer cdAgenciaDebito;

    /** Atributo cdDigitoAgenciaDebito. */
    private Integer cdDigitoAgenciaDebito;

    /** Atributo cdContaDebito. */
    private Long cdContaDebito;

    /** Atributo cdDigitoContaDebito. */
    private String cdDigitoContaDebito;

    /** Atributo registroSelecionado. */
    private boolean registroSelecionado;

    /** Atributo cdBancoFormatado. */
    private String cdBancoFormatado;

    /** Atributo cdAgenciaFormatado. */
    private String cdAgenciaFormatado;

    /** Atributo cdDigitoAgenciaFormatado. */
    private String cdDigitoAgenciaFormatado;

    /** Atributo cdContaFormatado. */
    private String cdContaFormatado;

    /** Atributo contaDebitoFormatada. */
    private String contaDebitoFormatada;

    /** Atributo dsEfetivacaoPagamento. */
    private String dsEfetivacaoPagamento;

    /** Atributo dtCreditoPagamentoFormatada. */
    private String dtCreditoPagamentoFormatada;

    /** Atributo favorecidoFormatado. */
    private String favorecidoFormatado;
    
    /** Atributo cdSituacaoOperacaoPagamento. */
    private Integer cdSituacaoOperacaoPagamento;
    
    /** Atributo dsSituacaoOperacaoPagamento. */
    private String dsSituacaoOperacaoPagamento;    

    /** Atributo cdTipoTela. */
    private Integer cdTipoTela;
    
    // check da grid de consulta
    /** Atributo check. */
    private boolean check = false;

    /**
     * Is check.
     *
     * @return true, if is check
     */
    public boolean isCheck() {
	return check;
    }

    /**
     * Set: check.
     *
     * @param check the check
     */
    public void setCheck(boolean check) {
	this.check = check;
    }

    /**
     * Get: cdAgenciaFormatado.
     *
     * @return cdAgenciaFormatado
     */
    public String getCdAgenciaFormatado() {
	return cdAgenciaFormatado;
    }

    /**
     * Set: cdAgenciaFormatado.
     *
     * @param cdAgenciaFormatado the cd agencia formatado
     */
    public void setCdAgenciaFormatado(String cdAgenciaFormatado) {
	this.cdAgenciaFormatado = cdAgenciaFormatado;
    }

    /**
     * Get: cdBancoFormatado.
     *
     * @return cdBancoFormatado
     */
    public String getCdBancoFormatado() {
	return cdBancoFormatado;
    }

    /**
     * Set: cdBancoFormatado.
     *
     * @param cdBancoFormatado the cd banco formatado
     */
    public void setCdBancoFormatado(String cdBancoFormatado) {
	this.cdBancoFormatado = cdBancoFormatado;
    }

    /**
     * Get: cdContaFormatado.
     *
     * @return cdContaFormatado
     */
    public String getCdContaFormatado() {
	return cdContaFormatado;
    }

    /**
     * Set: cdContaFormatado.
     *
     * @param cdContaFormatado the cd conta formatado
     */
    public void setCdContaFormatado(String cdContaFormatado) {
	this.cdContaFormatado = cdContaFormatado;
    }

    /**
     * Get: cdDigitoAgenciaFormatado.
     *
     * @return cdDigitoAgenciaFormatado
     */
    public String getCdDigitoAgenciaFormatado() {
	return cdDigitoAgenciaFormatado;
    }

    /**
     * Set: cdDigitoAgenciaFormatado.
     *
     * @param cdDigitoAgenciaFormatado the cd digito agencia formatado
     */
    public void setCdDigitoAgenciaFormatado(String cdDigitoAgenciaFormatado) {
	this.cdDigitoAgenciaFormatado = cdDigitoAgenciaFormatado;
    }

    /**
     * Get: contaDebitoFormatada.
     *
     * @return contaDebitoFormatada
     */
    public String getContaDebitoFormatada() {
	return contaDebitoFormatada;
    }

    /**
     * Set: contaDebitoFormatada.
     *
     * @param contaDebitoFormatada the conta debito formatada
     */
    public void setContaDebitoFormatada(String contaDebitoFormatada) {
	this.contaDebitoFormatada = contaDebitoFormatada;
    }

    /**
     * Is registro selecionado.
     *
     * @return true, if is registro selecionado
     */
    public boolean isRegistroSelecionado() {
	return registroSelecionado;
    }

    /**
     * Set: registroSelecionado.
     *
     * @param registroSelecionado the registro selecionado
     */
    public void setRegistroSelecionado(boolean registroSelecionado) {
	this.registroSelecionado = registroSelecionado;
    }

    /**
     * Get: cdAgenciaDebito.
     *
     * @return cdAgenciaDebito
     */
    public Integer getCdAgenciaDebito() {
	return cdAgenciaDebito;
    }

    /**
     * Set: cdAgenciaDebito.
     *
     * @param cdAgenciaDebito the cd agencia debito
     */
    public void setCdAgenciaDebito(Integer cdAgenciaDebito) {
	this.cdAgenciaDebito = cdAgenciaDebito;
    }

    /**
     * Get: cdBancoDebito.
     *
     * @return cdBancoDebito
     */
    public Integer getCdBancoDebito() {
	return cdBancoDebito;
    }

    /**
     * Set: cdBancoDebito.
     *
     * @param cdBancoDebito the cd banco debito
     */
    public void setCdBancoDebito(Integer cdBancoDebito) {
	this.cdBancoDebito = cdBancoDebito;
    }

    /**
     * Get: cdContaDebito.
     *
     * @return cdContaDebito
     */
    public Long getCdContaDebito() {
	return cdContaDebito;
    }

    /**
     * Set: cdContaDebito.
     *
     * @param cdContaDebito the cd conta debito
     */
    public void setCdContaDebito(Long cdContaDebito) {
	this.cdContaDebito = cdContaDebito;
    }

    /**
     * Get: cdControlePagamento.
     *
     * @return cdControlePagamento
     */
    public String getCdControlePagamento() {
	return cdControlePagamento;
    }

    /**
     * Set: cdControlePagamento.
     *
     * @param cdControlePagamento the cd controle pagamento
     */
    public void setCdControlePagamento(String cdControlePagamento) {
	this.cdControlePagamento = cdControlePagamento;
    }

    /**
     * Get: cdDigitoAgenciaDebito.
     *
     * @return cdDigitoAgenciaDebito
     */
    public Integer getCdDigitoAgenciaDebito() {
	return cdDigitoAgenciaDebito;
    }

    /**
     * Set: cdDigitoAgenciaDebito.
     *
     * @param cdDigitoAgenciaDebito the cd digito agencia debito
     */
    public void setCdDigitoAgenciaDebito(Integer cdDigitoAgenciaDebito) {
	this.cdDigitoAgenciaDebito = cdDigitoAgenciaDebito;
    }

    /**
     * Get: cdDigitoContaDebito.
     *
     * @return cdDigitoContaDebito
     */
    public String getCdDigitoContaDebito() {
	return cdDigitoContaDebito;
    }

    /**
     * Set: cdDigitoContaDebito.
     *
     * @param cdDigitoContaDebito the cd digito conta debito
     */
    public void setCdDigitoContaDebito(String cdDigitoContaDebito) {
	this.cdDigitoContaDebito = cdDigitoContaDebito;
    }

    /**
     * Get: cdInscricaoFavorecido.
     *
     * @return cdInscricaoFavorecido
     */
    public Long getCdInscricaoFavorecido() {
	return cdInscricaoFavorecido;
    }

    /**
     * Set: cdInscricaoFavorecido.
     *
     * @param cdInscricaoFavorecido the cd inscricao favorecido
     */
    public void setCdInscricaoFavorecido(Long cdInscricaoFavorecido) {
	this.cdInscricaoFavorecido = cdInscricaoFavorecido;
    }

    /**
     * Get: cdProdutoServicoOperacao.
     *
     * @return cdProdutoServicoOperacao
     */
    public Integer getCdProdutoServicoOperacao() {
	return cdProdutoServicoOperacao;
    }

    /**
     * Set: cdProdutoServicoOperacao.
     *
     * @param cdProdutoServicoOperacao the cd produto servico operacao
     */
    public void setCdProdutoServicoOperacao(Integer cdProdutoServicoOperacao) {
	this.cdProdutoServicoOperacao = cdProdutoServicoOperacao;
    }

    /**
     * Get: cdProdutoServicoRelacionado.
     *
     * @return cdProdutoServicoRelacionado
     */
    public Integer getCdProdutoServicoRelacionado() {
	return cdProdutoServicoRelacionado;
    }

    /**
     * Set: cdProdutoServicoRelacionado.
     *
     * @param cdProdutoServicoRelacionado the cd produto servico relacionado
     */
    public void setCdProdutoServicoRelacionado(Integer cdProdutoServicoRelacionado) {
	this.cdProdutoServicoRelacionado = cdProdutoServicoRelacionado;
    }

    /**
     * Get: codMensagem.
     *
     * @return codMensagem
     */
    public String getCodMensagem() {
	return codMensagem;
    }

    /**
     * Set: codMensagem.
     *
     * @param codMensagem the cod mensagem
     */
    public void setCodMensagem(String codMensagem) {
	this.codMensagem = codMensagem;
    }

    /**
     * Get: dsFavorecido.
     *
     * @return dsFavorecido
     */
    public String getDsFavorecido() {
	return dsFavorecido;
    }

    /**
     * Set: dsFavorecido.
     *
     * @param dsFavorecido the ds favorecido
     */
    public void setDsFavorecido(String dsFavorecido) {
	this.dsFavorecido = dsFavorecido;
    }

    /**
     * Get: dsOperacaoProdutoServico.
     *
     * @return dsOperacaoProdutoServico
     */
    public String getDsOperacaoProdutoServico() {
	return dsOperacaoProdutoServico;
    }

    /**
     * Set: dsOperacaoProdutoServico.
     *
     * @param dsOperacaoProdutoServico the ds operacao produto servico
     */
    public void setDsOperacaoProdutoServico(String dsOperacaoProdutoServico) {
	this.dsOperacaoProdutoServico = dsOperacaoProdutoServico;
    }

    /**
     * Get: dsResumoProdutoServico.
     *
     * @return dsResumoProdutoServico
     */
    public String getDsResumoProdutoServico() {
	return dsResumoProdutoServico;
    }

    /**
     * Set: dsResumoProdutoServico.
     *
     * @param dsResumoProdutoServico the ds resumo produto servico
     */
    public void setDsResumoProdutoServico(String dsResumoProdutoServico) {
	this.dsResumoProdutoServico = dsResumoProdutoServico;
    }

    /**
     * Get: dtCreditoPagamento.
     *
     * @return dtCreditoPagamento
     */
    public String getDtCreditoPagamento() {
	return dtCreditoPagamento;
    }

    /**
     * Set: dtCreditoPagamento.
     *
     * @param dtCreditoPagamento the dt credito pagamento
     */
    public void setDtCreditoPagamento(String dtCreditoPagamento) {
	this.dtCreditoPagamento = dtCreditoPagamento;
    }

    /**
     * Get: mensagem.
     *
     * @return mensagem
     */
    public String getMensagem() {
	return mensagem;
    }

    /**
     * Set: mensagem.
     *
     * @param mensagem the mensagem
     */
    public void setMensagem(String mensagem) {
	this.mensagem = mensagem;
    }

    /**
     * Get: numeroConsultas.
     *
     * @return numeroConsultas
     */
    public Integer getNumeroConsultas() {
	return numeroConsultas;
    }

    /**
     * Set: numeroConsultas.
     *
     * @param numeroConsultas the numero consultas
     */
    public void setNumeroConsultas(Integer numeroConsultas) {
	this.numeroConsultas = numeroConsultas;
    }

    /**
     * Get: vlEfetivoPagamento.
     *
     * @return vlEfetivoPagamento
     */
    public BigDecimal getVlEfetivoPagamento() {
	return vlEfetivoPagamento;
    }

    /**
     * Set: vlEfetivoPagamento.
     *
     * @param vlEfetivoPagamento the vl efetivo pagamento
     */
    public void setVlEfetivoPagamento(BigDecimal vlEfetivoPagamento) {
	this.vlEfetivoPagamento = vlEfetivoPagamento;
    }

    /**
     * Get: dtCreditoPagamentoFormatada.
     *
     * @return dtCreditoPagamentoFormatada
     */
    public String getDtCreditoPagamentoFormatada() {
	return dtCreditoPagamentoFormatada;
    }

    /**
     * Set: dtCreditoPagamentoFormatada.
     *
     * @param dtCreditoPagamentoFormatada the dt credito pagamento formatada
     */
    public void setDtCreditoPagamentoFormatada(String dtCreditoPagamentoFormatada) {
	this.dtCreditoPagamentoFormatada = dtCreditoPagamentoFormatada;
    }

    /**
     * Get: favorecidoFormatado.
     *
     * @return favorecidoFormatado
     */
    public String getFavorecidoFormatado() {
	return favorecidoFormatado;
    }

    /**
     * Set: favorecidoFormatado.
     *
     * @param favorecidoFormatado the favorecido formatado
     */
    public void setFavorecidoFormatado(String favorecidoFormatado) {
	this.favorecidoFormatado = favorecidoFormatado;
    }

    /**
     * Get: dsEfetivacaoPagamento.
     *
     * @return dsEfetivacaoPagamento
     */
    public String getDsEfetivacaoPagamento() {
	return dsEfetivacaoPagamento;
    }

    /**
     * Set: dsEfetivacaoPagamento.
     *
     * @param dsEfetivacaoPagamento the ds efetivacao pagamento
     */
    public void setDsEfetivacaoPagamento(String dsEfetivacaoPagamento) {
	this.dsEfetivacaoPagamento = dsEfetivacaoPagamento;
    }

    /**
     * Get: cdSituacaoOperacaoPagamento.
     *
     * @return cdSituacaoOperacaoPagamento
     */
    public Integer getCdSituacaoOperacaoPagamento() {
        return cdSituacaoOperacaoPagamento;
    }

    /**
     * Set: cdSituacaoOperacaoPagamento.
     *
     * @param cdSituacaoOperacaoPagamento the cd situacao operacao pagamento
     */
    public void setCdSituacaoOperacaoPagamento(Integer cdSituacaoOperacaoPagamento) {
        this.cdSituacaoOperacaoPagamento = cdSituacaoOperacaoPagamento;
    }

    /**
     * Get: dsSituacaoOperacaoPagamento.
     *
     * @return dsSituacaoOperacaoPagamento
     */
    public String getDsSituacaoOperacaoPagamento() {
        return dsSituacaoOperacaoPagamento;
    }

    /**
     * Set: dsSituacaoOperacaoPagamento.
     *
     * @param dsSituacaoOperacaoPagamento the ds situacao operacao pagamento
     */
    public void setDsSituacaoOperacaoPagamento(String dsSituacaoOperacaoPagamento) {
        this.dsSituacaoOperacaoPagamento = dsSituacaoOperacaoPagamento;
    }

    /**
     * Get: cdTipoTela.
     *
     * @return cdTipoTela
     */
    public Integer getCdTipoTela() {
        return cdTipoTela;
    }

    /**
     * Set: cdTipoTela.
     *
     * @param cdTipoTela the cd tipo tela
     */
    public void setCdTipoTela(Integer cdTipoTela) {
        this.cdTipoTela = cdTipoTela;
    }
}