/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/implementation.ftl,v $
 * $Id: implementation.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: implementation.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */
 
package br.com.bradesco.web.pgit.service.business.funcoesalcada.impl;

import java.util.ArrayList;
import java.util.List;

import br.com.bradesco.web.pgit.service.business.funcoesalcada.IFuncAlcadaServiceConstants;
import br.com.bradesco.web.pgit.service.business.funcoesalcada.IFuncoesAlcadaService;
import br.com.bradesco.web.pgit.service.business.funcoesalcada.bean.AlterarFuncoesAlcadaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.funcoesalcada.bean.AlterarFuncoesAlcadaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.funcoesalcada.bean.DetalharFuncoesAlcadaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.funcoesalcada.bean.DetalharFuncoesAlcadaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.funcoesalcada.bean.ExcluirFuncoesAlcadaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.funcoesalcada.bean.ExcluirFuncoesAlcadaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.funcoesalcada.bean.IncluirFuncoesAlcadaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.funcoesalcada.bean.IncluirFuncoesAlcadaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.funcoesalcada.bean.ListarFuncoesAlcadaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.funcoesalcada.bean.ListarFuncoesAlcadaSaidaDTO;
import br.com.bradesco.web.pgit.service.data.pdc.FactoryAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.alteraralcada.request.AlterarAlcadaRequest;
import br.com.bradesco.web.pgit.service.data.pdc.alteraralcada.response.AlterarAlcadaResponse;
import br.com.bradesco.web.pgit.service.data.pdc.detalharalcada.request.DetalharAlcadaRequest;
import br.com.bradesco.web.pgit.service.data.pdc.detalharalcada.response.DetalharAlcadaResponse;
import br.com.bradesco.web.pgit.service.data.pdc.excluiralcada.request.ExcluirAlcadaRequest;
import br.com.bradesco.web.pgit.service.data.pdc.excluiralcada.response.ExcluirAlcadaResponse;
import br.com.bradesco.web.pgit.service.data.pdc.incluiralcada.request.IncluirAlcadaRequest;
import br.com.bradesco.web.pgit.service.data.pdc.incluiralcada.response.IncluirAlcadaResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listaralcada.request.ListarAlcadaRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listaralcada.response.ListarAlcadaResponse;


/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Implementa��o do adaptador: ManterFuncoesAlcada
 * </p>
 * 
 * @comment C�DIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public class FuncoesAlcadaServiceImpl implements IFuncoesAlcadaService {
	
	/** Atributo factoryAdapter. */
	private FactoryAdapter factoryAdapter; 

    /**
     * Construtor.
     */
    public FuncoesAlcadaServiceImpl() {
        // TODO: Implementa��o
    }
    
    /**
     * M�todo de exemplo.
     *
     * @see br.com.bradesco.web.pgit.service.business.funcoesalcada.IManterFuncoesAlcada#sampleManterFuncoesAlcada()
     */
    public void sampleManterFuncoesAlcada() {
        // TODO: Implementa�ao
    }

	/**
	 * Get: factoryAdapter.
	 *
	 * @return factoryAdapter
	 */
	public FactoryAdapter getFactoryAdapter() {
		return factoryAdapter;
	}

	/**
	 * Set: factoryAdapter.
	 *
	 * @param factoryAdapter the factory adapter
	 */
	public void setFactoryAdapter(FactoryAdapter factoryAdapter) {
		this.factoryAdapter = factoryAdapter;
	}
	
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.funcoesalcada.IFuncoesAlcadaService#listarFuncoesAlcadas(br.com.bradesco.web.pgit.service.business.funcoesalcada.bean.ListarFuncoesAlcadaEntradaDTO)
	 */
	public List<ListarFuncoesAlcadaSaidaDTO> listarFuncoesAlcadas(ListarFuncoesAlcadaEntradaDTO listarFuncoesAlcadaEntradaDTO) {
		
		List<ListarFuncoesAlcadaSaidaDTO> listaRetorno = new ArrayList<ListarFuncoesAlcadaSaidaDTO>();		
		ListarAlcadaRequest request = new ListarAlcadaRequest();
		ListarAlcadaResponse response = new ListarAlcadaResponse();
		
		request.setCdSistema(listarFuncoesAlcadaEntradaDTO.getCentroCusto());
		request.setCdTipoAcaoPagamento(listarFuncoesAlcadaEntradaDTO.getAcao());
		request.setCdProgPagamentoIntegrado(listarFuncoesAlcadaEntradaDTO.getCodigoAplicacao());
		request.setCdTipoAcessoAlcada(listarFuncoesAlcadaEntradaDTO.getIndicadorAcesso());
		request.setQtConsultas(IFuncAlcadaServiceConstants.QTDE_CONSULTAS_LISTAR);
		
	
		response = getFactoryAdapter().getListarAlcadaPDCAdapter().invokeProcess(request);

		ListarFuncoesAlcadaSaidaDTO saidaDTO;
		
		for (int i=0; i<response.getOcorrenciasCount();i++){
			saidaDTO = new ListarFuncoesAlcadaSaidaDTO();
			saidaDTO.setCodMensagem(response.getCodMensagem());
			saidaDTO.setMensagem(response.getMensagem());
			saidaDTO.setCentroCusto(response.getOcorrencias(i).getCdSistema());
			saidaDTO.setDsAcao(response.getOcorrencias(i).getDsTipoAcaoPagamento());
			saidaDTO.setCdAcao(response.getOcorrencias(i).getCdTipoAcaoPagamento());
			saidaDTO.setCodigoAplicacao(response.getOcorrencias(i).getCdProgPagamentoIntegrado());
			saidaDTO.setIndicadorAcesso(response.getOcorrencias(i).getCdTipoAcessoAlcada());
			saidaDTO.setDsTipoServico(response.getOcorrencias(i).getDsOperacaoProdutoServico()); //tipo servi�o
			saidaDTO.setDsModalidadeServico(response.getOcorrencias(i).getDsProdutoServicoOperacao()); //modalidade
			saidaDTO.setCdRegra(response.getOcorrencias(i).getCdRegraAlcada());			
			listaRetorno.add(saidaDTO);
			
		}
		
		return listaRetorno;
	}
	
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.funcoesalcada.IFuncoesAlcadaService#incluirFuncoesAlcada(br.com.bradesco.web.pgit.service.business.funcoesalcada.bean.IncluirFuncoesAlcadaEntradaDTO)
	 */
	public IncluirFuncoesAlcadaSaidaDTO incluirFuncoesAlcada(IncluirFuncoesAlcadaEntradaDTO incluirFuncoesAlcadaEntradaDTO) {
		
		IncluirFuncoesAlcadaSaidaDTO incluirFuncoesAlcadaSaidaDTO = new IncluirFuncoesAlcadaSaidaDTO();
		IncluirAlcadaRequest incluirAlcadaRequest = new IncluirAlcadaRequest();
		IncluirAlcadaResponse incluirAlcadaResponse = new IncluirAlcadaResponse();
		
		incluirAlcadaRequest.setCdProgPagamentoIntegrado(incluirFuncoesAlcadaEntradaDTO.getCodigoAplicacao());
		incluirAlcadaRequest.setCdTipoAcaoPagamento(incluirFuncoesAlcadaEntradaDTO.getAcao());
		incluirAlcadaRequest.setCdTipoAcessoAlcada(incluirFuncoesAlcadaEntradaDTO.getTipoAcesso());
		incluirAlcadaRequest.setCdTipoRegraAlcada(incluirFuncoesAlcadaEntradaDTO.getTipoRegra());
		incluirAlcadaRequest.setCdTipoTratoAlcada(incluirFuncoesAlcadaEntradaDTO.getTipoTratamento());
		incluirAlcadaRequest.setCdSistemaRetor(incluirFuncoesAlcadaEntradaDTO.getCentroCustoRetorno());
		incluirAlcadaRequest.setCdCist(incluirFuncoesAlcadaEntradaDTO.getCentroCusto());
		incluirAlcadaRequest.setCdProRetornoPagamento(incluirFuncoesAlcadaEntradaDTO.getCodigoAplicacaoRetorno());
		incluirAlcadaRequest.setCdProdutoOperacaoDeflt(incluirFuncoesAlcadaEntradaDTO.getServico());
		incluirAlcadaRequest.setCdOperProdutoServico(incluirFuncoesAlcadaEntradaDTO.getOperacao());
		incluirAlcadaRequest.setCdRegraAlcada(incluirFuncoesAlcadaEntradaDTO.getRegra());
		
		incluirAlcadaResponse = getFactoryAdapter().getIncluirAlcadaPDCAdapter().invokeProcess(incluirAlcadaRequest);
		
		incluirFuncoesAlcadaSaidaDTO.setCodMensagem(incluirAlcadaResponse.getCodMensagem());
		incluirFuncoesAlcadaSaidaDTO.setMensagem(incluirAlcadaResponse.getMensagem());
	
		return incluirFuncoesAlcadaSaidaDTO;
	}
	
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.funcoesalcada.IFuncoesAlcadaService#detalharFuncoesAlcadas(br.com.bradesco.web.pgit.service.business.funcoesalcada.bean.DetalharFuncoesAlcadaEntradaDTO)
	 */
	public DetalharFuncoesAlcadaSaidaDTO detalharFuncoesAlcadas(DetalharFuncoesAlcadaEntradaDTO detalharFuncoesAlcadaEntradaDTO) {
		
		DetalharFuncoesAlcadaSaidaDTO saidaDTO = new DetalharFuncoesAlcadaSaidaDTO();
		
		DetalharAlcadaRequest request = new DetalharAlcadaRequest();
		DetalharAlcadaResponse response = new DetalharAlcadaResponse();
	
		request.setCdSistema(detalharFuncoesAlcadaEntradaDTO.getCentroCusto());
		request.setCdProgPagamentoIntegrado(detalharFuncoesAlcadaEntradaDTO.getCodigoAplicacao());
		request.setCdTipoAcaoPagamento(detalharFuncoesAlcadaEntradaDTO.getAcao());
		request.setCdTipoAcessoAlcada(detalharFuncoesAlcadaEntradaDTO.getIndicadorAcesso());
		request.setQtConsultas(0);
		
		response = getFactoryAdapter().getDetalharAlcadaPDCAdapter().invokeProcess(request);
	
		saidaDTO = new DetalharFuncoesAlcadaSaidaDTO();
		saidaDTO.setCodMensagem(response.getCodMensagem());
		saidaDTO.setMensagem(response.getMensagem());
	
		saidaDTO.setCentroCusto(response.getOcorrencias(0).getCdSistema());
		saidaDTO.setCodigoAplicacao(response.getOcorrencias(0).getCdProgPagamentoIntegrado());
		saidaDTO.setCdAcao(response.getOcorrencias(0).getCdTipoAcaoPagamento());
		saidaDTO.setDsAcao(response.getOcorrencias(0).getDsTipoAcaoPagamento());
		saidaDTO.setIndicadorAcesso(response.getOcorrencias(0).getCdTipoAcessoAlcada());
		saidaDTO.setTipoRegraAlcada(response.getOcorrencias(0).getCdTipoRegraAlcada());
		saidaDTO.setTipoTratamento(response.getOcorrencias(0).getCdTipoTratoAlcada());
		saidaDTO.setCentroCustoRetorno(response.getOcorrencias(0).getCdSistemaRetorno());
		saidaDTO.setCodigoAplicacaoRetorno(response.getOcorrencias(0).getCdProgRetornoPagamento());
		saidaDTO.setCdServico(response.getOcorrencias(0).getCdProdutoOperDeflt());
		saidaDTO.setDsServico(response.getOcorrencias(0).getDsProdutoDeflt());
		saidaDTO.setCdOperacao(response.getOcorrencias(0).getCdOperProdutoServico());
		saidaDTO.setDsOperacao(response.getOcorrencias(0).getDsCdOperServico());
		saidaDTO.setCdRegra(response.getOcorrencias(0).getCdRegraAlcada());
		saidaDTO.setDsRegra(response.getOcorrencias(0).getDsRegraAlcada());
		

		saidaDTO.setCdCanalInclusao(response.getOcorrencias(0).getCdCanalInclusao());
		saidaDTO.setDsCanalInclusao(response.getOcorrencias(0).getDsCanalInclusao()); 
		saidaDTO.setUsuarioInclusao(response.getOcorrencias(0).getCdAutenSegrcInclusao());
		saidaDTO.setComplementoInclusao(response.getOcorrencias(0).getNmOperFluxoInclusao());
		saidaDTO.setDataHoraInclusao(response.getOcorrencias(0).getHrInclusaoRegistro());
		
		saidaDTO.setCdCanalManutencao(response.getOcorrencias(0).getCdCanalManutencao());
		saidaDTO.setDsCanalManutencao(response.getOcorrencias(0).getDsCanalManutencao()); 
		saidaDTO.setUsuarioManutencao(response.getOcorrencias(0).getCdAutenSegrcManutencao());
		saidaDTO.setComplementoManutencao(response.getOcorrencias(0).getNmOperFluxoManutencao());
		saidaDTO.setDataHoraManutencao(response.getOcorrencias(0).getHrManutencaoRegistro());
		
		saidaDTO.setDsCentroCusto(response.getOcorrencias(0).getDsSistema());
		saidaDTO.setDsCentroCustoRetorno(response.getOcorrencias(0).getDsSistemaRetorno());
		
		return saidaDTO;
		
	}


	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.funcoesalcada.IFuncoesAlcadaService#excluirFuncoesAlcada(br.com.bradesco.web.pgit.service.business.funcoesalcada.bean.ExcluirFuncoesAlcadaEntradaDTO)
	 */
	public ExcluirFuncoesAlcadaSaidaDTO excluirFuncoesAlcada(ExcluirFuncoesAlcadaEntradaDTO excluirFuncoesAlcadaEntradaDTO) {
		ExcluirFuncoesAlcadaSaidaDTO excluirFuncoesAlcadaSaidaDTO = new ExcluirFuncoesAlcadaSaidaDTO();
		ExcluirAlcadaRequest excluirAlcadaRequest = new ExcluirAlcadaRequest();
		ExcluirAlcadaResponse excluirAlcadaResponse = new ExcluirAlcadaResponse();
	
		excluirAlcadaRequest.setCdSistema(excluirFuncoesAlcadaEntradaDTO.getCentroCusto());
		excluirAlcadaRequest.setCdProgPagamentoIntegrado(excluirFuncoesAlcadaEntradaDTO.getCodigoAplicacao());
		excluirAlcadaRequest.setCdTipoAcaoPagamento(excluirFuncoesAlcadaEntradaDTO.getAcao());
		excluirAlcadaRequest.setCdTipoAcessoAlcada(excluirFuncoesAlcadaEntradaDTO.getIndicadorAcesso());
		excluirAlcadaRequest.setQtConsultas(0);
		
	
		excluirAlcadaResponse = getFactoryAdapter().getExcluirAlcadaPDCAdapter().invokeProcess(excluirAlcadaRequest);
	
		excluirFuncoesAlcadaSaidaDTO.setCodMensagem(excluirAlcadaResponse.getCodMensagem());
		excluirFuncoesAlcadaSaidaDTO.setMensagem(excluirAlcadaResponse.getMensagem());
	
		return excluirFuncoesAlcadaSaidaDTO;
	}
	
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.funcoesalcada.IFuncoesAlcadaService#alterarFuncoesAlcada(br.com.bradesco.web.pgit.service.business.funcoesalcada.bean.AlterarFuncoesAlcadaEntradaDTO)
	 */
	public AlterarFuncoesAlcadaSaidaDTO alterarFuncoesAlcada(AlterarFuncoesAlcadaEntradaDTO alterarFuncoesAlcadaEntradaDTO) {
		
		
		AlterarAlcadaRequest alterarAlcadaRequest = new AlterarAlcadaRequest();
		AlterarAlcadaResponse alterarAlcadaResponse = new AlterarAlcadaResponse();
		AlterarFuncoesAlcadaSaidaDTO alterarFuncoesAlcadaSaidaDTO = new AlterarFuncoesAlcadaSaidaDTO();
		
		alterarAlcadaRequest.setCdSistema(alterarFuncoesAlcadaEntradaDTO.getCentroCusto());
		alterarAlcadaRequest.setCdProgPagamentoIntegrado(alterarFuncoesAlcadaEntradaDTO.getCodigoAplicacao());
		alterarAlcadaRequest.setCdTipoAcaoPagamento(alterarFuncoesAlcadaEntradaDTO.getAcao());
		alterarAlcadaRequest.setCdTipoAcessoAlcada(alterarFuncoesAlcadaEntradaDTO.getTipoAcesso());
		alterarAlcadaRequest.setCdTipoRegraAlcada(alterarFuncoesAlcadaEntradaDTO.getTipoRegra());
		alterarAlcadaRequest.setCdTipoTratoAlcada(alterarFuncoesAlcadaEntradaDTO.getTipoTratamento());		
		alterarAlcadaRequest.setCdSistemaRetorno(alterarFuncoesAlcadaEntradaDTO.getCentroCustoRetorno());
		alterarAlcadaRequest.setCdProgRetorPagamento(alterarFuncoesAlcadaEntradaDTO.getCodigoAplicacaoRetorno());
		alterarAlcadaRequest.setCdProdutoOperDeflt(alterarFuncoesAlcadaEntradaDTO.getServico());
		alterarAlcadaRequest.setCdOperProdutoServico(alterarFuncoesAlcadaEntradaDTO.getOperacao());
		alterarAlcadaRequest.setCdRegraAlcada(alterarFuncoesAlcadaEntradaDTO.getRegra());
	
		alterarAlcadaResponse = getFactoryAdapter().getAlterarAlcadaPDCAdapter().invokeProcess(alterarAlcadaRequest);
					
		alterarFuncoesAlcadaSaidaDTO.setCodMensagem(alterarAlcadaResponse.getCodMensagem());
		alterarFuncoesAlcadaSaidaDTO.setMensagem(alterarAlcadaResponse.getMensagem());
				
		return alterarFuncoesAlcadaSaidaDTO;
		
	}

    
}

