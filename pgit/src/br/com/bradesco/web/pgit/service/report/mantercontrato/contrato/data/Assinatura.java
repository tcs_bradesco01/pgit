/*
 * Nome: br.com.bradesco.web.pgit.service.report.contrato.data
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 05/02/2016
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.report.mantercontrato.contrato.data;

import java.util.ArrayList;
import java.util.List;

/**
 * Nome: Assinatura
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class Assinatura {

    /** Atributo data. */
    private String data = null;
    
    /** Atributo cliente. */
    private String cliente = null;

    /** Atributo clienteCnpjCpf. */
    private String clienteCnpjCpf = null;
    
    /** Atributo participantes. */
    private List<Participante> participantes = new ArrayList<Participante>();

    /**
     * Instancia um novo Assinatura
     *
     * @see
     */
    public Assinatura() {
        super();
    }

    /**
     * Nome: getData.
     *
     * @return data
     */
    public String getData() {
        return data;
    }

    /**
     * Nome: getParticipantes
     *
     * @return participantes
     */
    public List<Participante> getParticipantes() {
        return participantes;
    }

    /**
     * Nome: getCliente
     *
     * @return cliente
     */
	public String getCliente() {
		return cliente;
	}

    /**
     * Nome: setCliente
     *
     * @param cliente
     */
	public void setCliente(String cliente) {
		this.cliente = cliente;
	}

	/**
     * Nome: setData
     *
     * @param data
     */
	public void setData(String data) {
		this.data = data;
	}

	/**
     * Nome: setParticipantes
     *
     * @param participantes
     */
	public void setParticipantes(List<Participante> participantes) {
		this.participantes = participantes;
	}

    /**
     * Nome: getClienteCnpjCpf
     *
     * @return clienteCnpjCpf
     */
    public String getClienteCnpjCpf() {
        return clienteCnpjCpf;
    }

    /**
     * Nome: setClienteCnpjCpf
     *
     * @param clienteCnpjCpf
     */
    public void setClienteCnpjCpf(String clienteCnpjCpf) {
        this.clienteCnpjCpf = clienteCnpjCpf;
    }
}
