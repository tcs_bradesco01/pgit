/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultarsolicitacaoestornopagtos.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ConsultarSolicitacaoEstornoPagtosRequest.
 * 
 * @version $Revision$ $Date$
 */
public class ConsultarSolicitacaoEstornoPagtosRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _numeroOcorrencias
     */
    private int _numeroOcorrencias = 0;

    /**
     * keeps track of state for field: _numeroOcorrencias
     */
    private boolean _has_numeroOcorrencias;

    /**
     * Field _cdPessoaJuridicaContrato
     */
    private long _cdPessoaJuridicaContrato = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridicaContrato
     */
    private boolean _has_cdPessoaJuridicaContrato;

    /**
     * Field _cdTipoContratoNegocio
     */
    private int _cdTipoContratoNegocio = 0;

    /**
     * keeps track of state for field: _cdTipoContratoNegocio
     */
    private boolean _has_cdTipoContratoNegocio;

    /**
     * Field _nrSequenciaContratoNegocio
     */
    private long _nrSequenciaContratoNegocio = 0;

    /**
     * keeps track of state for field: _nrSequenciaContratoNegocio
     */
    private boolean _has_nrSequenciaContratoNegocio;

    /**
     * Field _dtSolicitacaoInicio
     */
    private java.lang.String _dtSolicitacaoInicio;

    /**
     * Field _dtSolicitacaoFim
     */
    private java.lang.String _dtSolicitacaoFim;

    /**
     * Field _cdSituacaoSolicitacaoPagamento
     */
    private int _cdSituacaoSolicitacaoPagamento = 0;

    /**
     * keeps track of state for field:
     * _cdSituacaoSolicitacaoPagamento
     */
    private boolean _has_cdSituacaoSolicitacaoPagamento;

    /**
     * Field _cdMotivoSituacaoSolicitacao
     */
    private int _cdMotivoSituacaoSolicitacao = 0;

    /**
     * keeps track of state for field: _cdMotivoSituacaoSolicitacao
     */
    private boolean _has_cdMotivoSituacaoSolicitacao;

    /**
     * Field _cdModalidadePagamentoCliente
     */
    private int _cdModalidadePagamentoCliente = 0;

    /**
     * keeps track of state for field: _cdModalidadePagamentoCliente
     */
    private boolean _has_cdModalidadePagamentoCliente;


      //----------------/
     //- Constructors -/
    //----------------/

    public ConsultarSolicitacaoEstornoPagtosRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarsolicitacaoestornopagtos.request.ConsultarSolicitacaoEstornoPagtosRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdModalidadePagamentoCliente
     * 
     */
    public void deleteCdModalidadePagamentoCliente()
    {
        this._has_cdModalidadePagamentoCliente= false;
    } //-- void deleteCdModalidadePagamentoCliente() 

    /**
     * Method deleteCdMotivoSituacaoSolicitacao
     * 
     */
    public void deleteCdMotivoSituacaoSolicitacao()
    {
        this._has_cdMotivoSituacaoSolicitacao= false;
    } //-- void deleteCdMotivoSituacaoSolicitacao() 

    /**
     * Method deleteCdPessoaJuridicaContrato
     * 
     */
    public void deleteCdPessoaJuridicaContrato()
    {
        this._has_cdPessoaJuridicaContrato= false;
    } //-- void deleteCdPessoaJuridicaContrato() 

    /**
     * Method deleteCdSituacaoSolicitacaoPagamento
     * 
     */
    public void deleteCdSituacaoSolicitacaoPagamento()
    {
        this._has_cdSituacaoSolicitacaoPagamento= false;
    } //-- void deleteCdSituacaoSolicitacaoPagamento() 

    /**
     * Method deleteCdTipoContratoNegocio
     * 
     */
    public void deleteCdTipoContratoNegocio()
    {
        this._has_cdTipoContratoNegocio= false;
    } //-- void deleteCdTipoContratoNegocio() 

    /**
     * Method deleteNrSequenciaContratoNegocio
     * 
     */
    public void deleteNrSequenciaContratoNegocio()
    {
        this._has_nrSequenciaContratoNegocio= false;
    } //-- void deleteNrSequenciaContratoNegocio() 

    /**
     * Method deleteNumeroOcorrencias
     * 
     */
    public void deleteNumeroOcorrencias()
    {
        this._has_numeroOcorrencias= false;
    } //-- void deleteNumeroOcorrencias() 

    /**
     * Returns the value of field 'cdModalidadePagamentoCliente'.
     * 
     * @return int
     * @return the value of field 'cdModalidadePagamentoCliente'.
     */
    public int getCdModalidadePagamentoCliente()
    {
        return this._cdModalidadePagamentoCliente;
    } //-- int getCdModalidadePagamentoCliente() 

    /**
     * Returns the value of field 'cdMotivoSituacaoSolicitacao'.
     * 
     * @return int
     * @return the value of field 'cdMotivoSituacaoSolicitacao'.
     */
    public int getCdMotivoSituacaoSolicitacao()
    {
        return this._cdMotivoSituacaoSolicitacao;
    } //-- int getCdMotivoSituacaoSolicitacao() 

    /**
     * Returns the value of field 'cdPessoaJuridicaContrato'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridicaContrato'.
     */
    public long getCdPessoaJuridicaContrato()
    {
        return this._cdPessoaJuridicaContrato;
    } //-- long getCdPessoaJuridicaContrato() 

    /**
     * Returns the value of field 'cdSituacaoSolicitacaoPagamento'.
     * 
     * @return int
     * @return the value of field 'cdSituacaoSolicitacaoPagamento'.
     */
    public int getCdSituacaoSolicitacaoPagamento()
    {
        return this._cdSituacaoSolicitacaoPagamento;
    } //-- int getCdSituacaoSolicitacaoPagamento() 

    /**
     * Returns the value of field 'cdTipoContratoNegocio'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoNegocio'.
     */
    public int getCdTipoContratoNegocio()
    {
        return this._cdTipoContratoNegocio;
    } //-- int getCdTipoContratoNegocio() 

    /**
     * Returns the value of field 'dtSolicitacaoFim'.
     * 
     * @return String
     * @return the value of field 'dtSolicitacaoFim'.
     */
    public java.lang.String getDtSolicitacaoFim()
    {
        return this._dtSolicitacaoFim;
    } //-- java.lang.String getDtSolicitacaoFim() 

    /**
     * Returns the value of field 'dtSolicitacaoInicio'.
     * 
     * @return String
     * @return the value of field 'dtSolicitacaoInicio'.
     */
    public java.lang.String getDtSolicitacaoInicio()
    {
        return this._dtSolicitacaoInicio;
    } //-- java.lang.String getDtSolicitacaoInicio() 

    /**
     * Returns the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @return long
     * @return the value of field 'nrSequenciaContratoNegocio'.
     */
    public long getNrSequenciaContratoNegocio()
    {
        return this._nrSequenciaContratoNegocio;
    } //-- long getNrSequenciaContratoNegocio() 

    /**
     * Returns the value of field 'numeroOcorrencias'.
     * 
     * @return int
     * @return the value of field 'numeroOcorrencias'.
     */
    public int getNumeroOcorrencias()
    {
        return this._numeroOcorrencias;
    } //-- int getNumeroOcorrencias() 

    /**
     * Method hasCdModalidadePagamentoCliente
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdModalidadePagamentoCliente()
    {
        return this._has_cdModalidadePagamentoCliente;
    } //-- boolean hasCdModalidadePagamentoCliente() 

    /**
     * Method hasCdMotivoSituacaoSolicitacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMotivoSituacaoSolicitacao()
    {
        return this._has_cdMotivoSituacaoSolicitacao;
    } //-- boolean hasCdMotivoSituacaoSolicitacao() 

    /**
     * Method hasCdPessoaJuridicaContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridicaContrato()
    {
        return this._has_cdPessoaJuridicaContrato;
    } //-- boolean hasCdPessoaJuridicaContrato() 

    /**
     * Method hasCdSituacaoSolicitacaoPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSituacaoSolicitacaoPagamento()
    {
        return this._has_cdSituacaoSolicitacaoPagamento;
    } //-- boolean hasCdSituacaoSolicitacaoPagamento() 

    /**
     * Method hasCdTipoContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoNegocio()
    {
        return this._has_cdTipoContratoNegocio;
    } //-- boolean hasCdTipoContratoNegocio() 

    /**
     * Method hasNrSequenciaContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaContratoNegocio()
    {
        return this._has_nrSequenciaContratoNegocio;
    } //-- boolean hasNrSequenciaContratoNegocio() 

    /**
     * Method hasNumeroOcorrencias
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNumeroOcorrencias()
    {
        return this._has_numeroOcorrencias;
    } //-- boolean hasNumeroOcorrencias() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdModalidadePagamentoCliente'.
     * 
     * @param cdModalidadePagamentoCliente the value of field
     * 'cdModalidadePagamentoCliente'.
     */
    public void setCdModalidadePagamentoCliente(int cdModalidadePagamentoCliente)
    {
        this._cdModalidadePagamentoCliente = cdModalidadePagamentoCliente;
        this._has_cdModalidadePagamentoCliente = true;
    } //-- void setCdModalidadePagamentoCliente(int) 

    /**
     * Sets the value of field 'cdMotivoSituacaoSolicitacao'.
     * 
     * @param cdMotivoSituacaoSolicitacao the value of field
     * 'cdMotivoSituacaoSolicitacao'.
     */
    public void setCdMotivoSituacaoSolicitacao(int cdMotivoSituacaoSolicitacao)
    {
        this._cdMotivoSituacaoSolicitacao = cdMotivoSituacaoSolicitacao;
        this._has_cdMotivoSituacaoSolicitacao = true;
    } //-- void setCdMotivoSituacaoSolicitacao(int) 

    /**
     * Sets the value of field 'cdPessoaJuridicaContrato'.
     * 
     * @param cdPessoaJuridicaContrato the value of field
     * 'cdPessoaJuridicaContrato'.
     */
    public void setCdPessoaJuridicaContrato(long cdPessoaJuridicaContrato)
    {
        this._cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
        this._has_cdPessoaJuridicaContrato = true;
    } //-- void setCdPessoaJuridicaContrato(long) 

    /**
     * Sets the value of field 'cdSituacaoSolicitacaoPagamento'.
     * 
     * @param cdSituacaoSolicitacaoPagamento the value of field
     * 'cdSituacaoSolicitacaoPagamento'.
     */
    public void setCdSituacaoSolicitacaoPagamento(int cdSituacaoSolicitacaoPagamento)
    {
        this._cdSituacaoSolicitacaoPagamento = cdSituacaoSolicitacaoPagamento;
        this._has_cdSituacaoSolicitacaoPagamento = true;
    } //-- void setCdSituacaoSolicitacaoPagamento(int) 

    /**
     * Sets the value of field 'cdTipoContratoNegocio'.
     * 
     * @param cdTipoContratoNegocio the value of field
     * 'cdTipoContratoNegocio'.
     */
    public void setCdTipoContratoNegocio(int cdTipoContratoNegocio)
    {
        this._cdTipoContratoNegocio = cdTipoContratoNegocio;
        this._has_cdTipoContratoNegocio = true;
    } //-- void setCdTipoContratoNegocio(int) 

    /**
     * Sets the value of field 'dtSolicitacaoFim'.
     * 
     * @param dtSolicitacaoFim the value of field 'dtSolicitacaoFim'
     */
    public void setDtSolicitacaoFim(java.lang.String dtSolicitacaoFim)
    {
        this._dtSolicitacaoFim = dtSolicitacaoFim;
    } //-- void setDtSolicitacaoFim(java.lang.String) 

    /**
     * Sets the value of field 'dtSolicitacaoInicio'.
     * 
     * @param dtSolicitacaoInicio the value of field
     * 'dtSolicitacaoInicio'.
     */
    public void setDtSolicitacaoInicio(java.lang.String dtSolicitacaoInicio)
    {
        this._dtSolicitacaoInicio = dtSolicitacaoInicio;
    } //-- void setDtSolicitacaoInicio(java.lang.String) 

    /**
     * Sets the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @param nrSequenciaContratoNegocio the value of field
     * 'nrSequenciaContratoNegocio'.
     */
    public void setNrSequenciaContratoNegocio(long nrSequenciaContratoNegocio)
    {
        this._nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
        this._has_nrSequenciaContratoNegocio = true;
    } //-- void setNrSequenciaContratoNegocio(long) 

    /**
     * Sets the value of field 'numeroOcorrencias'.
     * 
     * @param numeroOcorrencias the value of field
     * 'numeroOcorrencias'.
     */
    public void setNumeroOcorrencias(int numeroOcorrencias)
    {
        this._numeroOcorrencias = numeroOcorrencias;
        this._has_numeroOcorrencias = true;
    } //-- void setNumeroOcorrencias(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ConsultarSolicitacaoEstornoPagtosRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultarsolicitacaoestornopagtos.request.ConsultarSolicitacaoEstornoPagtosRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultarsolicitacaoestornopagtos.request.ConsultarSolicitacaoEstornoPagtosRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultarsolicitacaoestornopagtos.request.ConsultarSolicitacaoEstornoPagtosRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarsolicitacaoestornopagtos.request.ConsultarSolicitacaoEstornoPagtosRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
