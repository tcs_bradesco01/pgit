/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.detalharsolicitacaorelatoriofavorecidos.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.math.BigDecimal;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class DetalharSolicitacaoRelatorioFavorecidosResponse.
 * 
 * @version $Revision$ $Date$
 */
public class DetalharSolicitacaoRelatorioFavorecidosResponse implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _codMensagem
     */
    private java.lang.String _codMensagem;

    /**
     * Field _mensagem
     */
    private java.lang.String _mensagem;

    /**
     * Field _cdSituacaoSolicitacaoPagamento
     */
    private int _cdSituacaoSolicitacaoPagamento = 0;

    /**
     * keeps track of state for field:
     * _cdSituacaoSolicitacaoPagamento
     */
    private boolean _has_cdSituacaoSolicitacaoPagamento;

    /**
     * Field _dsSolicitacaoPagamento
     */
    private java.lang.String _dsSolicitacaoPagamento;

    /**
     * Field _cdMotivoSituacaoSolicitacao
     */
    private int _cdMotivoSituacaoSolicitacao = 0;

    /**
     * keeps track of state for field: _cdMotivoSituacaoSolicitacao
     */
    private boolean _has_cdMotivoSituacaoSolicitacao;

    /**
     * Field _dsMotivoSolicitacao
     */
    private java.lang.String _dsMotivoSolicitacao;

    /**
     * Field _dtSolicitacao
     */
    private java.lang.String _dtSolicitacao;

    /**
     * Field _hrSolicitacao
     */
    private java.lang.String _hrSolicitacao;

    /**
     * Field _dtAtendimento
     */
    private java.lang.String _dtAtendimento;

    /**
     * Field _hrAtendimento
     */
    private java.lang.String _hrAtendimento;

    /**
     * Field _nrTarifaBonificacao
     */
    private java.math.BigDecimal _nrTarifaBonificacao = new java.math.BigDecimal("0");

    /**
     * Field _cdProdutoServicoOperacao
     */
    private int _cdProdutoServicoOperacao = 0;

    /**
     * keeps track of state for field: _cdProdutoServicoOperacao
     */
    private boolean _has_cdProdutoServicoOperacao;

    /**
     * Field _dsProdutoServicoOperacao
     */
    private java.lang.String _dsProdutoServicoOperacao;

    /**
     * Field _cdProdutoOperRelacionado
     */
    private int _cdProdutoOperRelacionado = 0;

    /**
     * keeps track of state for field: _cdProdutoOperRelacionado
     */
    private boolean _has_cdProdutoOperRelacionado;

    /**
     * Field _dsProdutoOperRelacionado
     */
    private java.lang.String _dsProdutoOperRelacionado;

    /**
     * Field _cdRelacionamentoProduto
     */
    private int _cdRelacionamentoProduto = 0;

    /**
     * keeps track of state for field: _cdRelacionamentoProduto
     */
    private boolean _has_cdRelacionamentoProduto;

    /**
     * Field _cdPessoaJuridicaDir
     */
    private long _cdPessoaJuridicaDir = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridicaDir
     */
    private boolean _has_cdPessoaJuridicaDir;

    /**
     * Field _nrSeqUnidadeOrganizacionalDir
     */
    private int _nrSeqUnidadeOrganizacionalDir = 0;

    /**
     * keeps track of state for field: _nrSeqUnidadeOrganizacionalDi
     */
    private boolean _has_nrSeqUnidadeOrganizacionalDir;

    /**
     * Field _dsUnidadeOrganizacionalDir
     */
    private java.lang.String _dsUnidadeOrganizacionalDir;

    /**
     * Field _cdPessoaJuridicaGerc
     */
    private long _cdPessoaJuridicaGerc = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridicaGerc
     */
    private boolean _has_cdPessoaJuridicaGerc;

    /**
     * Field _nrUnidadeOrganizacionalGerc
     */
    private int _nrUnidadeOrganizacionalGerc = 0;

    /**
     * keeps track of state for field: _nrUnidadeOrganizacionalGerc
     */
    private boolean _has_nrUnidadeOrganizacionalGerc;

    /**
     * Field _dsOrganizacionalGerc
     */
    private java.lang.String _dsOrganizacionalGerc;

    /**
     * Field _cdPessoaJuridica
     */
    private long _cdPessoaJuridica = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridica
     */
    private boolean _has_cdPessoaJuridica;

    /**
     * Field _nrUnidadeOrganizacional
     */
    private int _nrUnidadeOrganizacional = 0;

    /**
     * keeps track of state for field: _nrUnidadeOrganizacional
     */
    private boolean _has_nrUnidadeOrganizacional;

    /**
     * Field _dsOrganizacaoOperacional
     */
    private java.lang.String _dsOrganizacaoOperacional;

    /**
     * Field _cdTipoRelatorio
     */
    private int _cdTipoRelatorio = 0;

    /**
     * keeps track of state for field: _cdTipoRelatorio
     */
    private boolean _has_cdTipoRelatorio;

    /**
     * Field _dsTipoRelatorio
     */
    private java.lang.String _dsTipoRelatorio;

    /**
     * Field _cdSegmentoCliente
     */
    private int _cdSegmentoCliente = 0;

    /**
     * keeps track of state for field: _cdSegmentoCliente
     */
    private boolean _has_cdSegmentoCliente;

    /**
     * Field _dsSegmentoCliente
     */
    private java.lang.String _dsSegmentoCliente;

    /**
     * Field _cdGrupoEconomico
     */
    private long _cdGrupoEconomico = 0;

    /**
     * keeps track of state for field: _cdGrupoEconomico
     */
    private boolean _has_cdGrupoEconomico;

    /**
     * Field _dsGrupoEconomico
     */
    private java.lang.String _dsGrupoEconomico;

    /**
     * Field _cdClassAtividade
     */
    private java.lang.String _cdClassAtividade;

    /**
     * Field _dsClassAtividade
     */
    private java.lang.String _dsClassAtividade;

    /**
     * Field _cdRamoAtividadeEconomica
     */
    private int _cdRamoAtividadeEconomica = 0;

    /**
     * keeps track of state for field: _cdRamoAtividadeEconomica
     */
    private boolean _has_cdRamoAtividadeEconomica;

    /**
     * Field _dsRamoAtividadeEconomica
     */
    private java.lang.String _dsRamoAtividadeEconomica;

    /**
     * Field _cdRamoAtividade
     */
    private int _cdRamoAtividade = 0;

    /**
     * keeps track of state for field: _cdRamoAtividade
     */
    private boolean _has_cdRamoAtividade;

    /**
     * Field _dsRamoAtividade
     */
    private java.lang.String _dsRamoAtividade;

    /**
     * Field _cdAtividadeEconomica
     */
    private int _cdAtividadeEconomica = 0;

    /**
     * keeps track of state for field: _cdAtividadeEconomica
     */
    private boolean _has_cdAtividadeEconomica;

    /**
     * Field _dsAtividadeEconomica
     */
    private java.lang.String _dsAtividadeEconomica;

    /**
     * Field _cdUsuarioInclusao
     */
    private java.lang.String _cdUsuarioInclusao;

    /**
     * Field _cdUsuarioInclusaoExter
     */
    private java.lang.String _cdUsuarioInclusaoExter;

    /**
     * Field _dtInclusao
     */
    private java.lang.String _dtInclusao;

    /**
     * Field _hrInclusao
     */
    private java.lang.String _hrInclusao;

    /**
     * Field _cdOperCanalInclusao
     */
    private java.lang.String _cdOperCanalInclusao;

    /**
     * Field _cdTipoCanalInclusao
     */
    private int _cdTipoCanalInclusao = 0;

    /**
     * keeps track of state for field: _cdTipoCanalInclusao
     */
    private boolean _has_cdTipoCanalInclusao;

    /**
     * Field _dsCanalInclusao
     */
    private java.lang.String _dsCanalInclusao;

    /**
     * Field _cdUsuarioManutencao
     */
    private java.lang.String _cdUsuarioManutencao;

    /**
     * Field _cdUsuarioManutencaoExter
     */
    private java.lang.String _cdUsuarioManutencaoExter;

    /**
     * Field _dtManutencao
     */
    private java.lang.String _dtManutencao;

    /**
     * Field _hrManutencao
     */
    private java.lang.String _hrManutencao;

    /**
     * Field _cdOperCanalManutencao
     */
    private java.lang.String _cdOperCanalManutencao;

    /**
     * Field _cdTipoCanalManutencao
     */
    private int _cdTipoCanalManutencao = 0;

    /**
     * keeps track of state for field: _cdTipoCanalManutencao
     */
    private boolean _has_cdTipoCanalManutencao;

    /**
     * Field _dsCanalManutencao
     */
    private java.lang.String _dsCanalManutencao;

    /**
     * Field _dsEmpresa
     */
    private java.lang.String _dsEmpresa;


      //----------------/
     //- Constructors -/
    //----------------/

    public DetalharSolicitacaoRelatorioFavorecidosResponse() 
     {
        super();
        setNrTarifaBonificacao(new java.math.BigDecimal("0"));
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharsolicitacaorelatoriofavorecidos.response.DetalharSolicitacaoRelatorioFavorecidosResponse()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdAtividadeEconomica
     * 
     */
    public void deleteCdAtividadeEconomica()
    {
        this._has_cdAtividadeEconomica= false;
    } //-- void deleteCdAtividadeEconomica() 

    /**
     * Method deleteCdGrupoEconomico
     * 
     */
    public void deleteCdGrupoEconomico()
    {
        this._has_cdGrupoEconomico= false;
    } //-- void deleteCdGrupoEconomico() 

    /**
     * Method deleteCdMotivoSituacaoSolicitacao
     * 
     */
    public void deleteCdMotivoSituacaoSolicitacao()
    {
        this._has_cdMotivoSituacaoSolicitacao= false;
    } //-- void deleteCdMotivoSituacaoSolicitacao() 

    /**
     * Method deleteCdPessoaJuridica
     * 
     */
    public void deleteCdPessoaJuridica()
    {
        this._has_cdPessoaJuridica= false;
    } //-- void deleteCdPessoaJuridica() 

    /**
     * Method deleteCdPessoaJuridicaDir
     * 
     */
    public void deleteCdPessoaJuridicaDir()
    {
        this._has_cdPessoaJuridicaDir= false;
    } //-- void deleteCdPessoaJuridicaDir() 

    /**
     * Method deleteCdPessoaJuridicaGerc
     * 
     */
    public void deleteCdPessoaJuridicaGerc()
    {
        this._has_cdPessoaJuridicaGerc= false;
    } //-- void deleteCdPessoaJuridicaGerc() 

    /**
     * Method deleteCdProdutoOperRelacionado
     * 
     */
    public void deleteCdProdutoOperRelacionado()
    {
        this._has_cdProdutoOperRelacionado= false;
    } //-- void deleteCdProdutoOperRelacionado() 

    /**
     * Method deleteCdProdutoServicoOperacao
     * 
     */
    public void deleteCdProdutoServicoOperacao()
    {
        this._has_cdProdutoServicoOperacao= false;
    } //-- void deleteCdProdutoServicoOperacao() 

    /**
     * Method deleteCdRamoAtividade
     * 
     */
    public void deleteCdRamoAtividade()
    {
        this._has_cdRamoAtividade= false;
    } //-- void deleteCdRamoAtividade() 

    /**
     * Method deleteCdRamoAtividadeEconomica
     * 
     */
    public void deleteCdRamoAtividadeEconomica()
    {
        this._has_cdRamoAtividadeEconomica= false;
    } //-- void deleteCdRamoAtividadeEconomica() 

    /**
     * Method deleteCdRelacionamentoProduto
     * 
     */
    public void deleteCdRelacionamentoProduto()
    {
        this._has_cdRelacionamentoProduto= false;
    } //-- void deleteCdRelacionamentoProduto() 

    /**
     * Method deleteCdSegmentoCliente
     * 
     */
    public void deleteCdSegmentoCliente()
    {
        this._has_cdSegmentoCliente= false;
    } //-- void deleteCdSegmentoCliente() 

    /**
     * Method deleteCdSituacaoSolicitacaoPagamento
     * 
     */
    public void deleteCdSituacaoSolicitacaoPagamento()
    {
        this._has_cdSituacaoSolicitacaoPagamento= false;
    } //-- void deleteCdSituacaoSolicitacaoPagamento() 

    /**
     * Method deleteCdTipoCanalInclusao
     * 
     */
    public void deleteCdTipoCanalInclusao()
    {
        this._has_cdTipoCanalInclusao= false;
    } //-- void deleteCdTipoCanalInclusao() 

    /**
     * Method deleteCdTipoCanalManutencao
     * 
     */
    public void deleteCdTipoCanalManutencao()
    {
        this._has_cdTipoCanalManutencao= false;
    } //-- void deleteCdTipoCanalManutencao() 

    /**
     * Method deleteCdTipoRelatorio
     * 
     */
    public void deleteCdTipoRelatorio()
    {
        this._has_cdTipoRelatorio= false;
    } //-- void deleteCdTipoRelatorio() 

    /**
     * Method deleteNrSeqUnidadeOrganizacionalDir
     * 
     */
    public void deleteNrSeqUnidadeOrganizacionalDir()
    {
        this._has_nrSeqUnidadeOrganizacionalDir= false;
    } //-- void deleteNrSeqUnidadeOrganizacionalDir() 

    /**
     * Method deleteNrUnidadeOrganizacional
     * 
     */
    public void deleteNrUnidadeOrganizacional()
    {
        this._has_nrUnidadeOrganizacional= false;
    } //-- void deleteNrUnidadeOrganizacional() 

    /**
     * Method deleteNrUnidadeOrganizacionalGerc
     * 
     */
    public void deleteNrUnidadeOrganizacionalGerc()
    {
        this._has_nrUnidadeOrganizacionalGerc= false;
    } //-- void deleteNrUnidadeOrganizacionalGerc() 

    /**
     * Returns the value of field 'cdAtividadeEconomica'.
     * 
     * @return int
     * @return the value of field 'cdAtividadeEconomica'.
     */
    public int getCdAtividadeEconomica()
    {
        return this._cdAtividadeEconomica;
    } //-- int getCdAtividadeEconomica() 

    /**
     * Returns the value of field 'cdClassAtividade'.
     * 
     * @return String
     * @return the value of field 'cdClassAtividade'.
     */
    public java.lang.String getCdClassAtividade()
    {
        return this._cdClassAtividade;
    } //-- java.lang.String getCdClassAtividade() 

    /**
     * Returns the value of field 'cdGrupoEconomico'.
     * 
     * @return long
     * @return the value of field 'cdGrupoEconomico'.
     */
    public long getCdGrupoEconomico()
    {
        return this._cdGrupoEconomico;
    } //-- long getCdGrupoEconomico() 

    /**
     * Returns the value of field 'cdMotivoSituacaoSolicitacao'.
     * 
     * @return int
     * @return the value of field 'cdMotivoSituacaoSolicitacao'.
     */
    public int getCdMotivoSituacaoSolicitacao()
    {
        return this._cdMotivoSituacaoSolicitacao;
    } //-- int getCdMotivoSituacaoSolicitacao() 

    /**
     * Returns the value of field 'cdOperCanalInclusao'.
     * 
     * @return String
     * @return the value of field 'cdOperCanalInclusao'.
     */
    public java.lang.String getCdOperCanalInclusao()
    {
        return this._cdOperCanalInclusao;
    } //-- java.lang.String getCdOperCanalInclusao() 

    /**
     * Returns the value of field 'cdOperCanalManutencao'.
     * 
     * @return String
     * @return the value of field 'cdOperCanalManutencao'.
     */
    public java.lang.String getCdOperCanalManutencao()
    {
        return this._cdOperCanalManutencao;
    } //-- java.lang.String getCdOperCanalManutencao() 

    /**
     * Returns the value of field 'cdPessoaJuridica'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridica'.
     */
    public long getCdPessoaJuridica()
    {
        return this._cdPessoaJuridica;
    } //-- long getCdPessoaJuridica() 

    /**
     * Returns the value of field 'cdPessoaJuridicaDir'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridicaDir'.
     */
    public long getCdPessoaJuridicaDir()
    {
        return this._cdPessoaJuridicaDir;
    } //-- long getCdPessoaJuridicaDir() 

    /**
     * Returns the value of field 'cdPessoaJuridicaGerc'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridicaGerc'.
     */
    public long getCdPessoaJuridicaGerc()
    {
        return this._cdPessoaJuridicaGerc;
    } //-- long getCdPessoaJuridicaGerc() 

    /**
     * Returns the value of field 'cdProdutoOperRelacionado'.
     * 
     * @return int
     * @return the value of field 'cdProdutoOperRelacionado'.
     */
    public int getCdProdutoOperRelacionado()
    {
        return this._cdProdutoOperRelacionado;
    } //-- int getCdProdutoOperRelacionado() 

    /**
     * Returns the value of field 'cdProdutoServicoOperacao'.
     * 
     * @return int
     * @return the value of field 'cdProdutoServicoOperacao'.
     */
    public int getCdProdutoServicoOperacao()
    {
        return this._cdProdutoServicoOperacao;
    } //-- int getCdProdutoServicoOperacao() 

    /**
     * Returns the value of field 'cdRamoAtividade'.
     * 
     * @return int
     * @return the value of field 'cdRamoAtividade'.
     */
    public int getCdRamoAtividade()
    {
        return this._cdRamoAtividade;
    } //-- int getCdRamoAtividade() 

    /**
     * Returns the value of field 'cdRamoAtividadeEconomica'.
     * 
     * @return int
     * @return the value of field 'cdRamoAtividadeEconomica'.
     */
    public int getCdRamoAtividadeEconomica()
    {
        return this._cdRamoAtividadeEconomica;
    } //-- int getCdRamoAtividadeEconomica() 

    /**
     * Returns the value of field 'cdRelacionamentoProduto'.
     * 
     * @return int
     * @return the value of field 'cdRelacionamentoProduto'.
     */
    public int getCdRelacionamentoProduto()
    {
        return this._cdRelacionamentoProduto;
    } //-- int getCdRelacionamentoProduto() 

    /**
     * Returns the value of field 'cdSegmentoCliente'.
     * 
     * @return int
     * @return the value of field 'cdSegmentoCliente'.
     */
    public int getCdSegmentoCliente()
    {
        return this._cdSegmentoCliente;
    } //-- int getCdSegmentoCliente() 

    /**
     * Returns the value of field 'cdSituacaoSolicitacaoPagamento'.
     * 
     * @return int
     * @return the value of field 'cdSituacaoSolicitacaoPagamento'.
     */
    public int getCdSituacaoSolicitacaoPagamento()
    {
        return this._cdSituacaoSolicitacaoPagamento;
    } //-- int getCdSituacaoSolicitacaoPagamento() 

    /**
     * Returns the value of field 'cdTipoCanalInclusao'.
     * 
     * @return int
     * @return the value of field 'cdTipoCanalInclusao'.
     */
    public int getCdTipoCanalInclusao()
    {
        return this._cdTipoCanalInclusao;
    } //-- int getCdTipoCanalInclusao() 

    /**
     * Returns the value of field 'cdTipoCanalManutencao'.
     * 
     * @return int
     * @return the value of field 'cdTipoCanalManutencao'.
     */
    public int getCdTipoCanalManutencao()
    {
        return this._cdTipoCanalManutencao;
    } //-- int getCdTipoCanalManutencao() 

    /**
     * Returns the value of field 'cdTipoRelatorio'.
     * 
     * @return int
     * @return the value of field 'cdTipoRelatorio'.
     */
    public int getCdTipoRelatorio()
    {
        return this._cdTipoRelatorio;
    } //-- int getCdTipoRelatorio() 

    /**
     * Returns the value of field 'cdUsuarioInclusao'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioInclusao'.
     */
    public java.lang.String getCdUsuarioInclusao()
    {
        return this._cdUsuarioInclusao;
    } //-- java.lang.String getCdUsuarioInclusao() 

    /**
     * Returns the value of field 'cdUsuarioInclusaoExter'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioInclusaoExter'.
     */
    public java.lang.String getCdUsuarioInclusaoExter()
    {
        return this._cdUsuarioInclusaoExter;
    } //-- java.lang.String getCdUsuarioInclusaoExter() 

    /**
     * Returns the value of field 'cdUsuarioManutencao'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioManutencao'.
     */
    public java.lang.String getCdUsuarioManutencao()
    {
        return this._cdUsuarioManutencao;
    } //-- java.lang.String getCdUsuarioManutencao() 

    /**
     * Returns the value of field 'cdUsuarioManutencaoExter'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioManutencaoExter'.
     */
    public java.lang.String getCdUsuarioManutencaoExter()
    {
        return this._cdUsuarioManutencaoExter;
    } //-- java.lang.String getCdUsuarioManutencaoExter() 

    /**
     * Returns the value of field 'codMensagem'.
     * 
     * @return String
     * @return the value of field 'codMensagem'.
     */
    public java.lang.String getCodMensagem()
    {
        return this._codMensagem;
    } //-- java.lang.String getCodMensagem() 

    /**
     * Returns the value of field 'dsAtividadeEconomica'.
     * 
     * @return String
     * @return the value of field 'dsAtividadeEconomica'.
     */
    public java.lang.String getDsAtividadeEconomica()
    {
        return this._dsAtividadeEconomica;
    } //-- java.lang.String getDsAtividadeEconomica() 

    /**
     * Returns the value of field 'dsCanalInclusao'.
     * 
     * @return String
     * @return the value of field 'dsCanalInclusao'.
     */
    public java.lang.String getDsCanalInclusao()
    {
        return this._dsCanalInclusao;
    } //-- java.lang.String getDsCanalInclusao() 

    /**
     * Returns the value of field 'dsCanalManutencao'.
     * 
     * @return String
     * @return the value of field 'dsCanalManutencao'.
     */
    public java.lang.String getDsCanalManutencao()
    {
        return this._dsCanalManutencao;
    } //-- java.lang.String getDsCanalManutencao() 

    /**
     * Returns the value of field 'dsClassAtividade'.
     * 
     * @return String
     * @return the value of field 'dsClassAtividade'.
     */
    public java.lang.String getDsClassAtividade()
    {
        return this._dsClassAtividade;
    } //-- java.lang.String getDsClassAtividade() 

    /**
     * Returns the value of field 'dsEmpresa'.
     * 
     * @return String
     * @return the value of field 'dsEmpresa'.
     */
    public java.lang.String getDsEmpresa()
    {
        return this._dsEmpresa;
    } //-- java.lang.String getDsEmpresa() 

    /**
     * Returns the value of field 'dsGrupoEconomico'.
     * 
     * @return String
     * @return the value of field 'dsGrupoEconomico'.
     */
    public java.lang.String getDsGrupoEconomico()
    {
        return this._dsGrupoEconomico;
    } //-- java.lang.String getDsGrupoEconomico() 

    /**
     * Returns the value of field 'dsMotivoSolicitacao'.
     * 
     * @return String
     * @return the value of field 'dsMotivoSolicitacao'.
     */
    public java.lang.String getDsMotivoSolicitacao()
    {
        return this._dsMotivoSolicitacao;
    } //-- java.lang.String getDsMotivoSolicitacao() 

    /**
     * Returns the value of field 'dsOrganizacaoOperacional'.
     * 
     * @return String
     * @return the value of field 'dsOrganizacaoOperacional'.
     */
    public java.lang.String getDsOrganizacaoOperacional()
    {
        return this._dsOrganizacaoOperacional;
    } //-- java.lang.String getDsOrganizacaoOperacional() 

    /**
     * Returns the value of field 'dsOrganizacionalGerc'.
     * 
     * @return String
     * @return the value of field 'dsOrganizacionalGerc'.
     */
    public java.lang.String getDsOrganizacionalGerc()
    {
        return this._dsOrganizacionalGerc;
    } //-- java.lang.String getDsOrganizacionalGerc() 

    /**
     * Returns the value of field 'dsProdutoOperRelacionado'.
     * 
     * @return String
     * @return the value of field 'dsProdutoOperRelacionado'.
     */
    public java.lang.String getDsProdutoOperRelacionado()
    {
        return this._dsProdutoOperRelacionado;
    } //-- java.lang.String getDsProdutoOperRelacionado() 

    /**
     * Returns the value of field 'dsProdutoServicoOperacao'.
     * 
     * @return String
     * @return the value of field 'dsProdutoServicoOperacao'.
     */
    public java.lang.String getDsProdutoServicoOperacao()
    {
        return this._dsProdutoServicoOperacao;
    } //-- java.lang.String getDsProdutoServicoOperacao() 

    /**
     * Returns the value of field 'dsRamoAtividade'.
     * 
     * @return String
     * @return the value of field 'dsRamoAtividade'.
     */
    public java.lang.String getDsRamoAtividade()
    {
        return this._dsRamoAtividade;
    } //-- java.lang.String getDsRamoAtividade() 

    /**
     * Returns the value of field 'dsRamoAtividadeEconomica'.
     * 
     * @return String
     * @return the value of field 'dsRamoAtividadeEconomica'.
     */
    public java.lang.String getDsRamoAtividadeEconomica()
    {
        return this._dsRamoAtividadeEconomica;
    } //-- java.lang.String getDsRamoAtividadeEconomica() 

    /**
     * Returns the value of field 'dsSegmentoCliente'.
     * 
     * @return String
     * @return the value of field 'dsSegmentoCliente'.
     */
    public java.lang.String getDsSegmentoCliente()
    {
        return this._dsSegmentoCliente;
    } //-- java.lang.String getDsSegmentoCliente() 

    /**
     * Returns the value of field 'dsSolicitacaoPagamento'.
     * 
     * @return String
     * @return the value of field 'dsSolicitacaoPagamento'.
     */
    public java.lang.String getDsSolicitacaoPagamento()
    {
        return this._dsSolicitacaoPagamento;
    } //-- java.lang.String getDsSolicitacaoPagamento() 

    /**
     * Returns the value of field 'dsTipoRelatorio'.
     * 
     * @return String
     * @return the value of field 'dsTipoRelatorio'.
     */
    public java.lang.String getDsTipoRelatorio()
    {
        return this._dsTipoRelatorio;
    } //-- java.lang.String getDsTipoRelatorio() 

    /**
     * Returns the value of field 'dsUnidadeOrganizacionalDir'.
     * 
     * @return String
     * @return the value of field 'dsUnidadeOrganizacionalDir'.
     */
    public java.lang.String getDsUnidadeOrganizacionalDir()
    {
        return this._dsUnidadeOrganizacionalDir;
    } //-- java.lang.String getDsUnidadeOrganizacionalDir() 

    /**
     * Returns the value of field 'dtAtendimento'.
     * 
     * @return String
     * @return the value of field 'dtAtendimento'.
     */
    public java.lang.String getDtAtendimento()
    {
        return this._dtAtendimento;
    } //-- java.lang.String getDtAtendimento() 

    /**
     * Returns the value of field 'dtInclusao'.
     * 
     * @return String
     * @return the value of field 'dtInclusao'.
     */
    public java.lang.String getDtInclusao()
    {
        return this._dtInclusao;
    } //-- java.lang.String getDtInclusao() 

    /**
     * Returns the value of field 'dtManutencao'.
     * 
     * @return String
     * @return the value of field 'dtManutencao'.
     */
    public java.lang.String getDtManutencao()
    {
        return this._dtManutencao;
    } //-- java.lang.String getDtManutencao() 

    /**
     * Returns the value of field 'dtSolicitacao'.
     * 
     * @return String
     * @return the value of field 'dtSolicitacao'.
     */
    public java.lang.String getDtSolicitacao()
    {
        return this._dtSolicitacao;
    } //-- java.lang.String getDtSolicitacao() 

    /**
     * Returns the value of field 'hrAtendimento'.
     * 
     * @return String
     * @return the value of field 'hrAtendimento'.
     */
    public java.lang.String getHrAtendimento()
    {
        return this._hrAtendimento;
    } //-- java.lang.String getHrAtendimento() 

    /**
     * Returns the value of field 'hrInclusao'.
     * 
     * @return String
     * @return the value of field 'hrInclusao'.
     */
    public java.lang.String getHrInclusao()
    {
        return this._hrInclusao;
    } //-- java.lang.String getHrInclusao() 

    /**
     * Returns the value of field 'hrManutencao'.
     * 
     * @return String
     * @return the value of field 'hrManutencao'.
     */
    public java.lang.String getHrManutencao()
    {
        return this._hrManutencao;
    } //-- java.lang.String getHrManutencao() 

    /**
     * Returns the value of field 'hrSolicitacao'.
     * 
     * @return String
     * @return the value of field 'hrSolicitacao'.
     */
    public java.lang.String getHrSolicitacao()
    {
        return this._hrSolicitacao;
    } //-- java.lang.String getHrSolicitacao() 

    /**
     * Returns the value of field 'mensagem'.
     * 
     * @return String
     * @return the value of field 'mensagem'.
     */
    public java.lang.String getMensagem()
    {
        return this._mensagem;
    } //-- java.lang.String getMensagem() 

    /**
     * Returns the value of field 'nrSeqUnidadeOrganizacionalDir'.
     * 
     * @return int
     * @return the value of field 'nrSeqUnidadeOrganizacionalDir'.
     */
    public int getNrSeqUnidadeOrganizacionalDir()
    {
        return this._nrSeqUnidadeOrganizacionalDir;
    } //-- int getNrSeqUnidadeOrganizacionalDir() 

    /**
     * Returns the value of field 'nrTarifaBonificacao'.
     * 
     * @return BigDecimal
     * @return the value of field 'nrTarifaBonificacao'.
     */
    public java.math.BigDecimal getNrTarifaBonificacao()
    {
        return this._nrTarifaBonificacao;
    } //-- java.math.BigDecimal getNrTarifaBonificacao() 

    /**
     * Returns the value of field 'nrUnidadeOrganizacional'.
     * 
     * @return int
     * @return the value of field 'nrUnidadeOrganizacional'.
     */
    public int getNrUnidadeOrganizacional()
    {
        return this._nrUnidadeOrganizacional;
    } //-- int getNrUnidadeOrganizacional() 

    /**
     * Returns the value of field 'nrUnidadeOrganizacionalGerc'.
     * 
     * @return int
     * @return the value of field 'nrUnidadeOrganizacionalGerc'.
     */
    public int getNrUnidadeOrganizacionalGerc()
    {
        return this._nrUnidadeOrganizacionalGerc;
    } //-- int getNrUnidadeOrganizacionalGerc() 

    /**
     * Method hasCdAtividadeEconomica
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAtividadeEconomica()
    {
        return this._has_cdAtividadeEconomica;
    } //-- boolean hasCdAtividadeEconomica() 

    /**
     * Method hasCdGrupoEconomico
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdGrupoEconomico()
    {
        return this._has_cdGrupoEconomico;
    } //-- boolean hasCdGrupoEconomico() 

    /**
     * Method hasCdMotivoSituacaoSolicitacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMotivoSituacaoSolicitacao()
    {
        return this._has_cdMotivoSituacaoSolicitacao;
    } //-- boolean hasCdMotivoSituacaoSolicitacao() 

    /**
     * Method hasCdPessoaJuridica
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridica()
    {
        return this._has_cdPessoaJuridica;
    } //-- boolean hasCdPessoaJuridica() 

    /**
     * Method hasCdPessoaJuridicaDir
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridicaDir()
    {
        return this._has_cdPessoaJuridicaDir;
    } //-- boolean hasCdPessoaJuridicaDir() 

    /**
     * Method hasCdPessoaJuridicaGerc
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridicaGerc()
    {
        return this._has_cdPessoaJuridicaGerc;
    } //-- boolean hasCdPessoaJuridicaGerc() 

    /**
     * Method hasCdProdutoOperRelacionado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdutoOperRelacionado()
    {
        return this._has_cdProdutoOperRelacionado;
    } //-- boolean hasCdProdutoOperRelacionado() 

    /**
     * Method hasCdProdutoServicoOperacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdutoServicoOperacao()
    {
        return this._has_cdProdutoServicoOperacao;
    } //-- boolean hasCdProdutoServicoOperacao() 

    /**
     * Method hasCdRamoAtividade
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdRamoAtividade()
    {
        return this._has_cdRamoAtividade;
    } //-- boolean hasCdRamoAtividade() 

    /**
     * Method hasCdRamoAtividadeEconomica
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdRamoAtividadeEconomica()
    {
        return this._has_cdRamoAtividadeEconomica;
    } //-- boolean hasCdRamoAtividadeEconomica() 

    /**
     * Method hasCdRelacionamentoProduto
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdRelacionamentoProduto()
    {
        return this._has_cdRelacionamentoProduto;
    } //-- boolean hasCdRelacionamentoProduto() 

    /**
     * Method hasCdSegmentoCliente
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSegmentoCliente()
    {
        return this._has_cdSegmentoCliente;
    } //-- boolean hasCdSegmentoCliente() 

    /**
     * Method hasCdSituacaoSolicitacaoPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSituacaoSolicitacaoPagamento()
    {
        return this._has_cdSituacaoSolicitacaoPagamento;
    } //-- boolean hasCdSituacaoSolicitacaoPagamento() 

    /**
     * Method hasCdTipoCanalInclusao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoCanalInclusao()
    {
        return this._has_cdTipoCanalInclusao;
    } //-- boolean hasCdTipoCanalInclusao() 

    /**
     * Method hasCdTipoCanalManutencao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoCanalManutencao()
    {
        return this._has_cdTipoCanalManutencao;
    } //-- boolean hasCdTipoCanalManutencao() 

    /**
     * Method hasCdTipoRelatorio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoRelatorio()
    {
        return this._has_cdTipoRelatorio;
    } //-- boolean hasCdTipoRelatorio() 

    /**
     * Method hasNrSeqUnidadeOrganizacionalDir
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSeqUnidadeOrganizacionalDir()
    {
        return this._has_nrSeqUnidadeOrganizacionalDir;
    } //-- boolean hasNrSeqUnidadeOrganizacionalDir() 

    /**
     * Method hasNrUnidadeOrganizacional
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrUnidadeOrganizacional()
    {
        return this._has_nrUnidadeOrganizacional;
    } //-- boolean hasNrUnidadeOrganizacional() 

    /**
     * Method hasNrUnidadeOrganizacionalGerc
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrUnidadeOrganizacionalGerc()
    {
        return this._has_nrUnidadeOrganizacionalGerc;
    } //-- boolean hasNrUnidadeOrganizacionalGerc() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdAtividadeEconomica'.
     * 
     * @param cdAtividadeEconomica the value of field
     * 'cdAtividadeEconomica'.
     */
    public void setCdAtividadeEconomica(int cdAtividadeEconomica)
    {
        this._cdAtividadeEconomica = cdAtividadeEconomica;
        this._has_cdAtividadeEconomica = true;
    } //-- void setCdAtividadeEconomica(int) 

    /**
     * Sets the value of field 'cdClassAtividade'.
     * 
     * @param cdClassAtividade the value of field 'cdClassAtividade'
     */
    public void setCdClassAtividade(java.lang.String cdClassAtividade)
    {
        this._cdClassAtividade = cdClassAtividade;
    } //-- void setCdClassAtividade(java.lang.String) 

    /**
     * Sets the value of field 'cdGrupoEconomico'.
     * 
     * @param cdGrupoEconomico the value of field 'cdGrupoEconomico'
     */
    public void setCdGrupoEconomico(long cdGrupoEconomico)
    {
        this._cdGrupoEconomico = cdGrupoEconomico;
        this._has_cdGrupoEconomico = true;
    } //-- void setCdGrupoEconomico(long) 

    /**
     * Sets the value of field 'cdMotivoSituacaoSolicitacao'.
     * 
     * @param cdMotivoSituacaoSolicitacao the value of field
     * 'cdMotivoSituacaoSolicitacao'.
     */
    public void setCdMotivoSituacaoSolicitacao(int cdMotivoSituacaoSolicitacao)
    {
        this._cdMotivoSituacaoSolicitacao = cdMotivoSituacaoSolicitacao;
        this._has_cdMotivoSituacaoSolicitacao = true;
    } //-- void setCdMotivoSituacaoSolicitacao(int) 

    /**
     * Sets the value of field 'cdOperCanalInclusao'.
     * 
     * @param cdOperCanalInclusao the value of field
     * 'cdOperCanalInclusao'.
     */
    public void setCdOperCanalInclusao(java.lang.String cdOperCanalInclusao)
    {
        this._cdOperCanalInclusao = cdOperCanalInclusao;
    } //-- void setCdOperCanalInclusao(java.lang.String) 

    /**
     * Sets the value of field 'cdOperCanalManutencao'.
     * 
     * @param cdOperCanalManutencao the value of field
     * 'cdOperCanalManutencao'.
     */
    public void setCdOperCanalManutencao(java.lang.String cdOperCanalManutencao)
    {
        this._cdOperCanalManutencao = cdOperCanalManutencao;
    } //-- void setCdOperCanalManutencao(java.lang.String) 

    /**
     * Sets the value of field 'cdPessoaJuridica'.
     * 
     * @param cdPessoaJuridica the value of field 'cdPessoaJuridica'
     */
    public void setCdPessoaJuridica(long cdPessoaJuridica)
    {
        this._cdPessoaJuridica = cdPessoaJuridica;
        this._has_cdPessoaJuridica = true;
    } //-- void setCdPessoaJuridica(long) 

    /**
     * Sets the value of field 'cdPessoaJuridicaDir'.
     * 
     * @param cdPessoaJuridicaDir the value of field
     * 'cdPessoaJuridicaDir'.
     */
    public void setCdPessoaJuridicaDir(long cdPessoaJuridicaDir)
    {
        this._cdPessoaJuridicaDir = cdPessoaJuridicaDir;
        this._has_cdPessoaJuridicaDir = true;
    } //-- void setCdPessoaJuridicaDir(long) 

    /**
     * Sets the value of field 'cdPessoaJuridicaGerc'.
     * 
     * @param cdPessoaJuridicaGerc the value of field
     * 'cdPessoaJuridicaGerc'.
     */
    public void setCdPessoaJuridicaGerc(long cdPessoaJuridicaGerc)
    {
        this._cdPessoaJuridicaGerc = cdPessoaJuridicaGerc;
        this._has_cdPessoaJuridicaGerc = true;
    } //-- void setCdPessoaJuridicaGerc(long) 

    /**
     * Sets the value of field 'cdProdutoOperRelacionado'.
     * 
     * @param cdProdutoOperRelacionado the value of field
     * 'cdProdutoOperRelacionado'.
     */
    public void setCdProdutoOperRelacionado(int cdProdutoOperRelacionado)
    {
        this._cdProdutoOperRelacionado = cdProdutoOperRelacionado;
        this._has_cdProdutoOperRelacionado = true;
    } //-- void setCdProdutoOperRelacionado(int) 

    /**
     * Sets the value of field 'cdProdutoServicoOperacao'.
     * 
     * @param cdProdutoServicoOperacao the value of field
     * 'cdProdutoServicoOperacao'.
     */
    public void setCdProdutoServicoOperacao(int cdProdutoServicoOperacao)
    {
        this._cdProdutoServicoOperacao = cdProdutoServicoOperacao;
        this._has_cdProdutoServicoOperacao = true;
    } //-- void setCdProdutoServicoOperacao(int) 

    /**
     * Sets the value of field 'cdRamoAtividade'.
     * 
     * @param cdRamoAtividade the value of field 'cdRamoAtividade'.
     */
    public void setCdRamoAtividade(int cdRamoAtividade)
    {
        this._cdRamoAtividade = cdRamoAtividade;
        this._has_cdRamoAtividade = true;
    } //-- void setCdRamoAtividade(int) 

    /**
     * Sets the value of field 'cdRamoAtividadeEconomica'.
     * 
     * @param cdRamoAtividadeEconomica the value of field
     * 'cdRamoAtividadeEconomica'.
     */
    public void setCdRamoAtividadeEconomica(int cdRamoAtividadeEconomica)
    {
        this._cdRamoAtividadeEconomica = cdRamoAtividadeEconomica;
        this._has_cdRamoAtividadeEconomica = true;
    } //-- void setCdRamoAtividadeEconomica(int) 

    /**
     * Sets the value of field 'cdRelacionamentoProduto'.
     * 
     * @param cdRelacionamentoProduto the value of field
     * 'cdRelacionamentoProduto'.
     */
    public void setCdRelacionamentoProduto(int cdRelacionamentoProduto)
    {
        this._cdRelacionamentoProduto = cdRelacionamentoProduto;
        this._has_cdRelacionamentoProduto = true;
    } //-- void setCdRelacionamentoProduto(int) 

    /**
     * Sets the value of field 'cdSegmentoCliente'.
     * 
     * @param cdSegmentoCliente the value of field
     * 'cdSegmentoCliente'.
     */
    public void setCdSegmentoCliente(int cdSegmentoCliente)
    {
        this._cdSegmentoCliente = cdSegmentoCliente;
        this._has_cdSegmentoCliente = true;
    } //-- void setCdSegmentoCliente(int) 

    /**
     * Sets the value of field 'cdSituacaoSolicitacaoPagamento'.
     * 
     * @param cdSituacaoSolicitacaoPagamento the value of field
     * 'cdSituacaoSolicitacaoPagamento'.
     */
    public void setCdSituacaoSolicitacaoPagamento(int cdSituacaoSolicitacaoPagamento)
    {
        this._cdSituacaoSolicitacaoPagamento = cdSituacaoSolicitacaoPagamento;
        this._has_cdSituacaoSolicitacaoPagamento = true;
    } //-- void setCdSituacaoSolicitacaoPagamento(int) 

    /**
     * Sets the value of field 'cdTipoCanalInclusao'.
     * 
     * @param cdTipoCanalInclusao the value of field
     * 'cdTipoCanalInclusao'.
     */
    public void setCdTipoCanalInclusao(int cdTipoCanalInclusao)
    {
        this._cdTipoCanalInclusao = cdTipoCanalInclusao;
        this._has_cdTipoCanalInclusao = true;
    } //-- void setCdTipoCanalInclusao(int) 

    /**
     * Sets the value of field 'cdTipoCanalManutencao'.
     * 
     * @param cdTipoCanalManutencao the value of field
     * 'cdTipoCanalManutencao'.
     */
    public void setCdTipoCanalManutencao(int cdTipoCanalManutencao)
    {
        this._cdTipoCanalManutencao = cdTipoCanalManutencao;
        this._has_cdTipoCanalManutencao = true;
    } //-- void setCdTipoCanalManutencao(int) 

    /**
     * Sets the value of field 'cdTipoRelatorio'.
     * 
     * @param cdTipoRelatorio the value of field 'cdTipoRelatorio'.
     */
    public void setCdTipoRelatorio(int cdTipoRelatorio)
    {
        this._cdTipoRelatorio = cdTipoRelatorio;
        this._has_cdTipoRelatorio = true;
    } //-- void setCdTipoRelatorio(int) 

    /**
     * Sets the value of field 'cdUsuarioInclusao'.
     * 
     * @param cdUsuarioInclusao the value of field
     * 'cdUsuarioInclusao'.
     */
    public void setCdUsuarioInclusao(java.lang.String cdUsuarioInclusao)
    {
        this._cdUsuarioInclusao = cdUsuarioInclusao;
    } //-- void setCdUsuarioInclusao(java.lang.String) 

    /**
     * Sets the value of field 'cdUsuarioInclusaoExter'.
     * 
     * @param cdUsuarioInclusaoExter the value of field
     * 'cdUsuarioInclusaoExter'.
     */
    public void setCdUsuarioInclusaoExter(java.lang.String cdUsuarioInclusaoExter)
    {
        this._cdUsuarioInclusaoExter = cdUsuarioInclusaoExter;
    } //-- void setCdUsuarioInclusaoExter(java.lang.String) 

    /**
     * Sets the value of field 'cdUsuarioManutencao'.
     * 
     * @param cdUsuarioManutencao the value of field
     * 'cdUsuarioManutencao'.
     */
    public void setCdUsuarioManutencao(java.lang.String cdUsuarioManutencao)
    {
        this._cdUsuarioManutencao = cdUsuarioManutencao;
    } //-- void setCdUsuarioManutencao(java.lang.String) 

    /**
     * Sets the value of field 'cdUsuarioManutencaoExter'.
     * 
     * @param cdUsuarioManutencaoExter the value of field
     * 'cdUsuarioManutencaoExter'.
     */
    public void setCdUsuarioManutencaoExter(java.lang.String cdUsuarioManutencaoExter)
    {
        this._cdUsuarioManutencaoExter = cdUsuarioManutencaoExter;
    } //-- void setCdUsuarioManutencaoExter(java.lang.String) 

    /**
     * Sets the value of field 'codMensagem'.
     * 
     * @param codMensagem the value of field 'codMensagem'.
     */
    public void setCodMensagem(java.lang.String codMensagem)
    {
        this._codMensagem = codMensagem;
    } //-- void setCodMensagem(java.lang.String) 

    /**
     * Sets the value of field 'dsAtividadeEconomica'.
     * 
     * @param dsAtividadeEconomica the value of field
     * 'dsAtividadeEconomica'.
     */
    public void setDsAtividadeEconomica(java.lang.String dsAtividadeEconomica)
    {
        this._dsAtividadeEconomica = dsAtividadeEconomica;
    } //-- void setDsAtividadeEconomica(java.lang.String) 

    /**
     * Sets the value of field 'dsCanalInclusao'.
     * 
     * @param dsCanalInclusao the value of field 'dsCanalInclusao'.
     */
    public void setDsCanalInclusao(java.lang.String dsCanalInclusao)
    {
        this._dsCanalInclusao = dsCanalInclusao;
    } //-- void setDsCanalInclusao(java.lang.String) 

    /**
     * Sets the value of field 'dsCanalManutencao'.
     * 
     * @param dsCanalManutencao the value of field
     * 'dsCanalManutencao'.
     */
    public void setDsCanalManutencao(java.lang.String dsCanalManutencao)
    {
        this._dsCanalManutencao = dsCanalManutencao;
    } //-- void setDsCanalManutencao(java.lang.String) 

    /**
     * Sets the value of field 'dsClassAtividade'.
     * 
     * @param dsClassAtividade the value of field 'dsClassAtividade'
     */
    public void setDsClassAtividade(java.lang.String dsClassAtividade)
    {
        this._dsClassAtividade = dsClassAtividade;
    } //-- void setDsClassAtividade(java.lang.String) 

    /**
     * Sets the value of field 'dsEmpresa'.
     * 
     * @param dsEmpresa the value of field 'dsEmpresa'.
     */
    public void setDsEmpresa(java.lang.String dsEmpresa)
    {
        this._dsEmpresa = dsEmpresa;
    } //-- void setDsEmpresa(java.lang.String) 

    /**
     * Sets the value of field 'dsGrupoEconomico'.
     * 
     * @param dsGrupoEconomico the value of field 'dsGrupoEconomico'
     */
    public void setDsGrupoEconomico(java.lang.String dsGrupoEconomico)
    {
        this._dsGrupoEconomico = dsGrupoEconomico;
    } //-- void setDsGrupoEconomico(java.lang.String) 

    /**
     * Sets the value of field 'dsMotivoSolicitacao'.
     * 
     * @param dsMotivoSolicitacao the value of field
     * 'dsMotivoSolicitacao'.
     */
    public void setDsMotivoSolicitacao(java.lang.String dsMotivoSolicitacao)
    {
        this._dsMotivoSolicitacao = dsMotivoSolicitacao;
    } //-- void setDsMotivoSolicitacao(java.lang.String) 

    /**
     * Sets the value of field 'dsOrganizacaoOperacional'.
     * 
     * @param dsOrganizacaoOperacional the value of field
     * 'dsOrganizacaoOperacional'.
     */
    public void setDsOrganizacaoOperacional(java.lang.String dsOrganizacaoOperacional)
    {
        this._dsOrganizacaoOperacional = dsOrganizacaoOperacional;
    } //-- void setDsOrganizacaoOperacional(java.lang.String) 

    /**
     * Sets the value of field 'dsOrganizacionalGerc'.
     * 
     * @param dsOrganizacionalGerc the value of field
     * 'dsOrganizacionalGerc'.
     */
    public void setDsOrganizacionalGerc(java.lang.String dsOrganizacionalGerc)
    {
        this._dsOrganizacionalGerc = dsOrganizacionalGerc;
    } //-- void setDsOrganizacionalGerc(java.lang.String) 

    /**
     * Sets the value of field 'dsProdutoOperRelacionado'.
     * 
     * @param dsProdutoOperRelacionado the value of field
     * 'dsProdutoOperRelacionado'.
     */
    public void setDsProdutoOperRelacionado(java.lang.String dsProdutoOperRelacionado)
    {
        this._dsProdutoOperRelacionado = dsProdutoOperRelacionado;
    } //-- void setDsProdutoOperRelacionado(java.lang.String) 

    /**
     * Sets the value of field 'dsProdutoServicoOperacao'.
     * 
     * @param dsProdutoServicoOperacao the value of field
     * 'dsProdutoServicoOperacao'.
     */
    public void setDsProdutoServicoOperacao(java.lang.String dsProdutoServicoOperacao)
    {
        this._dsProdutoServicoOperacao = dsProdutoServicoOperacao;
    } //-- void setDsProdutoServicoOperacao(java.lang.String) 

    /**
     * Sets the value of field 'dsRamoAtividade'.
     * 
     * @param dsRamoAtividade the value of field 'dsRamoAtividade'.
     */
    public void setDsRamoAtividade(java.lang.String dsRamoAtividade)
    {
        this._dsRamoAtividade = dsRamoAtividade;
    } //-- void setDsRamoAtividade(java.lang.String) 

    /**
     * Sets the value of field 'dsRamoAtividadeEconomica'.
     * 
     * @param dsRamoAtividadeEconomica the value of field
     * 'dsRamoAtividadeEconomica'.
     */
    public void setDsRamoAtividadeEconomica(java.lang.String dsRamoAtividadeEconomica)
    {
        this._dsRamoAtividadeEconomica = dsRamoAtividadeEconomica;
    } //-- void setDsRamoAtividadeEconomica(java.lang.String) 

    /**
     * Sets the value of field 'dsSegmentoCliente'.
     * 
     * @param dsSegmentoCliente the value of field
     * 'dsSegmentoCliente'.
     */
    public void setDsSegmentoCliente(java.lang.String dsSegmentoCliente)
    {
        this._dsSegmentoCliente = dsSegmentoCliente;
    } //-- void setDsSegmentoCliente(java.lang.String) 

    /**
     * Sets the value of field 'dsSolicitacaoPagamento'.
     * 
     * @param dsSolicitacaoPagamento the value of field
     * 'dsSolicitacaoPagamento'.
     */
    public void setDsSolicitacaoPagamento(java.lang.String dsSolicitacaoPagamento)
    {
        this._dsSolicitacaoPagamento = dsSolicitacaoPagamento;
    } //-- void setDsSolicitacaoPagamento(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoRelatorio'.
     * 
     * @param dsTipoRelatorio the value of field 'dsTipoRelatorio'.
     */
    public void setDsTipoRelatorio(java.lang.String dsTipoRelatorio)
    {
        this._dsTipoRelatorio = dsTipoRelatorio;
    } //-- void setDsTipoRelatorio(java.lang.String) 

    /**
     * Sets the value of field 'dsUnidadeOrganizacionalDir'.
     * 
     * @param dsUnidadeOrganizacionalDir the value of field
     * 'dsUnidadeOrganizacionalDir'.
     */
    public void setDsUnidadeOrganizacionalDir(java.lang.String dsUnidadeOrganizacionalDir)
    {
        this._dsUnidadeOrganizacionalDir = dsUnidadeOrganizacionalDir;
    } //-- void setDsUnidadeOrganizacionalDir(java.lang.String) 

    /**
     * Sets the value of field 'dtAtendimento'.
     * 
     * @param dtAtendimento the value of field 'dtAtendimento'.
     */
    public void setDtAtendimento(java.lang.String dtAtendimento)
    {
        this._dtAtendimento = dtAtendimento;
    } //-- void setDtAtendimento(java.lang.String) 

    /**
     * Sets the value of field 'dtInclusao'.
     * 
     * @param dtInclusao the value of field 'dtInclusao'.
     */
    public void setDtInclusao(java.lang.String dtInclusao)
    {
        this._dtInclusao = dtInclusao;
    } //-- void setDtInclusao(java.lang.String) 

    /**
     * Sets the value of field 'dtManutencao'.
     * 
     * @param dtManutencao the value of field 'dtManutencao'.
     */
    public void setDtManutencao(java.lang.String dtManutencao)
    {
        this._dtManutencao = dtManutencao;
    } //-- void setDtManutencao(java.lang.String) 

    /**
     * Sets the value of field 'dtSolicitacao'.
     * 
     * @param dtSolicitacao the value of field 'dtSolicitacao'.
     */
    public void setDtSolicitacao(java.lang.String dtSolicitacao)
    {
        this._dtSolicitacao = dtSolicitacao;
    } //-- void setDtSolicitacao(java.lang.String) 

    /**
     * Sets the value of field 'hrAtendimento'.
     * 
     * @param hrAtendimento the value of field 'hrAtendimento'.
     */
    public void setHrAtendimento(java.lang.String hrAtendimento)
    {
        this._hrAtendimento = hrAtendimento;
    } //-- void setHrAtendimento(java.lang.String) 

    /**
     * Sets the value of field 'hrInclusao'.
     * 
     * @param hrInclusao the value of field 'hrInclusao'.
     */
    public void setHrInclusao(java.lang.String hrInclusao)
    {
        this._hrInclusao = hrInclusao;
    } //-- void setHrInclusao(java.lang.String) 

    /**
     * Sets the value of field 'hrManutencao'.
     * 
     * @param hrManutencao the value of field 'hrManutencao'.
     */
    public void setHrManutencao(java.lang.String hrManutencao)
    {
        this._hrManutencao = hrManutencao;
    } //-- void setHrManutencao(java.lang.String) 

    /**
     * Sets the value of field 'hrSolicitacao'.
     * 
     * @param hrSolicitacao the value of field 'hrSolicitacao'.
     */
    public void setHrSolicitacao(java.lang.String hrSolicitacao)
    {
        this._hrSolicitacao = hrSolicitacao;
    } //-- void setHrSolicitacao(java.lang.String) 

    /**
     * Sets the value of field 'mensagem'.
     * 
     * @param mensagem the value of field 'mensagem'.
     */
    public void setMensagem(java.lang.String mensagem)
    {
        this._mensagem = mensagem;
    } //-- void setMensagem(java.lang.String) 

    /**
     * Sets the value of field 'nrSeqUnidadeOrganizacionalDir'.
     * 
     * @param nrSeqUnidadeOrganizacionalDir the value of field
     * 'nrSeqUnidadeOrganizacionalDir'.
     */
    public void setNrSeqUnidadeOrganizacionalDir(int nrSeqUnidadeOrganizacionalDir)
    {
        this._nrSeqUnidadeOrganizacionalDir = nrSeqUnidadeOrganizacionalDir;
        this._has_nrSeqUnidadeOrganizacionalDir = true;
    } //-- void setNrSeqUnidadeOrganizacionalDir(int) 

    /**
     * Sets the value of field 'nrTarifaBonificacao'.
     * 
     * @param nrTarifaBonificacao the value of field
     * 'nrTarifaBonificacao'.
     */
    public void setNrTarifaBonificacao(java.math.BigDecimal nrTarifaBonificacao)
    {
        this._nrTarifaBonificacao = nrTarifaBonificacao;
    } //-- void setNrTarifaBonificacao(java.math.BigDecimal) 

    /**
     * Sets the value of field 'nrUnidadeOrganizacional'.
     * 
     * @param nrUnidadeOrganizacional the value of field
     * 'nrUnidadeOrganizacional'.
     */
    public void setNrUnidadeOrganizacional(int nrUnidadeOrganizacional)
    {
        this._nrUnidadeOrganizacional = nrUnidadeOrganizacional;
        this._has_nrUnidadeOrganizacional = true;
    } //-- void setNrUnidadeOrganizacional(int) 

    /**
     * Sets the value of field 'nrUnidadeOrganizacionalGerc'.
     * 
     * @param nrUnidadeOrganizacionalGerc the value of field
     * 'nrUnidadeOrganizacionalGerc'.
     */
    public void setNrUnidadeOrganizacionalGerc(int nrUnidadeOrganizacionalGerc)
    {
        this._nrUnidadeOrganizacionalGerc = nrUnidadeOrganizacionalGerc;
        this._has_nrUnidadeOrganizacionalGerc = true;
    } //-- void setNrUnidadeOrganizacionalGerc(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return DetalharSolicitacaoRelatorioFavorecidosResponse
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.detalharsolicitacaorelatoriofavorecidos.response.DetalharSolicitacaoRelatorioFavorecidosResponse unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.detalharsolicitacaorelatoriofavorecidos.response.DetalharSolicitacaoRelatorioFavorecidosResponse) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.detalharsolicitacaorelatoriofavorecidos.response.DetalharSolicitacaoRelatorioFavorecidosResponse.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharsolicitacaorelatoriofavorecidos.response.DetalharSolicitacaoRelatorioFavorecidosResponse unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
