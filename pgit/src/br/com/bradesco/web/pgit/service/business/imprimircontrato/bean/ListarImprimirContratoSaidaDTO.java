/*
 * Nome: br.com.bradesco.web.pgit.service.business.imprimircontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.imprimircontrato.bean;

/**
 * Nome: ListarImprimirContratoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarImprimirContratoSaidaDTO {
	
	/** Atributo cdDocumento. */
	private int cdDocumento;
	
	/** Atributo cdVersao. */
	private int cdVersao;
	
	/** Atributo dsDataGeracao. */
	private String dsDataGeracao;
	
	/** Atributo dsIdioma. */
	private String dsIdioma;
	
	/** Atributo dsFormulario. */
	private String dsFormulario;
	
	/** Atributo cdAditivo. */
	private int cdAditivo;
	
	/** Atributo dsDataInclusao. */
	private String dsDataInclusao;
	
	/** Atributo dsSituacao. */
	private String dsSituacao;
	
	/** Atributo dsSubstituiContratoComercial. */
	private String dsSubstituiContratoComercial;
	
	/**
	 * Get: cdDocumento.
	 *
	 * @return cdDocumento
	 */
	public int getCdDocumento() {
		return cdDocumento;
	}
	
	/**
	 * Set: cdDocumento.
	 *
	 * @param cdDocumento the cd documento
	 */
	public void setCdDocumento(int cdDocumento) {
		this.cdDocumento = cdDocumento;
	}
	
	/**
	 * Get: cdVersao.
	 *
	 * @return cdVersao
	 */
	public int getCdVersao() {
		return cdVersao;
	}
	
	/**
	 * Set: cdVersao.
	 *
	 * @param cdVersao the cd versao
	 */
	public void setCdVersao(int cdVersao) {
		this.cdVersao = cdVersao;
	}
	
	/**
	 * Get: dsDataGeracao.
	 *
	 * @return dsDataGeracao
	 */
	public String getDsDataGeracao() {
		return dsDataGeracao;
	}
	
	/**
	 * Set: dsDataGeracao.
	 *
	 * @param dsDataGeracao the ds data geracao
	 */
	public void setDsDataGeracao(String dsDataGeracao) {
		this.dsDataGeracao = dsDataGeracao;
	}
	
	/**
	 * Get: dsFormulario.
	 *
	 * @return dsFormulario
	 */
	public String getDsFormulario() {
		return dsFormulario;
	}
	
	/**
	 * Set: dsFormulario.
	 *
	 * @param dsFormulario the ds formulario
	 */
	public void setDsFormulario(String dsFormulario) {
		this.dsFormulario = dsFormulario;
	}
	
	/**
	 * Get: dsIdioma.
	 *
	 * @return dsIdioma
	 */
	public String getDsIdioma() {
		return dsIdioma;
	}
	
	/**
	 * Set: dsIdioma.
	 *
	 * @param dsIdioma the ds idioma
	 */
	public void setDsIdioma(String dsIdioma) {
		this.dsIdioma = dsIdioma;
	}
	
	/**
	 * Get: cdAditivo.
	 *
	 * @return cdAditivo
	 */
	public int getCdAditivo() {
		return cdAditivo;
	}
	
	/**
	 * Set: cdAditivo.
	 *
	 * @param cdAditivo the cd aditivo
	 */
	public void setCdAditivo(int cdAditivo) {
		this.cdAditivo = cdAditivo;
	}
	
	/**
	 * Get: dsDataInclusao.
	 *
	 * @return dsDataInclusao
	 */
	public String getDsDataInclusao() {
		return dsDataInclusao;
	}
	
	/**
	 * Set: dsDataInclusao.
	 *
	 * @param dsDataInclusao the ds data inclusao
	 */
	public void setDsDataInclusao(String dsDataInclusao) {
		this.dsDataInclusao = dsDataInclusao;
	}
	
	/**
	 * Get: dsSituacao.
	 *
	 * @return dsSituacao
	 */
	public String getDsSituacao() {
		return dsSituacao;
	}
	
	/**
	 * Set: dsSituacao.
	 *
	 * @param dsSituacao the ds situacao
	 */
	public void setDsSituacao(String dsSituacao) {
		this.dsSituacao = dsSituacao;
	}
	
	/**
	 * Get: dsSubstituiContratoComercial.
	 *
	 * @return dsSubstituiContratoComercial
	 */
	public String getDsSubstituiContratoComercial() {
		return dsSubstituiContratoComercial;
	}
	
	/**
	 * Set: dsSubstituiContratoComercial.
	 *
	 * @param dsSubstituiContratoComercial the ds substitui contrato comercial
	 */
	public void setDsSubstituiContratoComercial(String dsSubstituiContratoComercial) {
		this.dsSubstituiContratoComercial = dsSubstituiContratoComercial;
	}
}
