package br.com.bradesco.web.pgit.service.business.mantercontrato.bean;

public class ListarContaDebTarifaOcorrencias {
	
    private Long cdPessoaJuridicaVinculo;
    private Integer cdTipoContratoVinculo;
    private Long nrSequenciaContratoVinculo;
    private Integer cdTipoVinculoContrato;
    private String cdCpfCnpj;
    private Long cdParticipante;
    private String nmParticipante;
    private Integer cdBanco;
    private String dsBanco;
    private Integer cdAgencia;
    private Integer cdDigitoAgencia;
    private String dsAgencia;
    private Long cdConta;
    private String cdDigitoConta;
    private Integer cdTipoConta;
    private String dsTipoConta;
    
    
	public Long getCdPessoaJuridicaVinculo() {
		return cdPessoaJuridicaVinculo;
	}
	public void setCdPessoaJuridicaVinculo(Long cdPessoaJuridicaVinculo) {
		this.cdPessoaJuridicaVinculo = cdPessoaJuridicaVinculo;
	}
	public Integer getCdTipoContratoVinculo() {
		return cdTipoContratoVinculo;
	}
	public void setCdTipoContratoVinculo(Integer cdTipoContratoVinculo) {
		this.cdTipoContratoVinculo = cdTipoContratoVinculo;
	}
	public Long getNrSequenciaContratoVinculo() {
		return nrSequenciaContratoVinculo;
	}
	public void setNrSequenciaContratoVinculo(Long nrSequenciaContratoVinculo) {
		this.nrSequenciaContratoVinculo = nrSequenciaContratoVinculo;
	}
	public Integer getCdTipoVinculoContrato() {
		return cdTipoVinculoContrato;
	}
	public void setCdTipoVinculoContrato(Integer cdTipoVinculoContrato) {
		this.cdTipoVinculoContrato = cdTipoVinculoContrato;
	}
	public String getCdCpfCnpj() {
		return cdCpfCnpj;
	}
	public void setCdCpfCnpj(String cdCpfCnpj) {
		this.cdCpfCnpj = cdCpfCnpj;
	}
	public Long getCdParticipante() {
		return cdParticipante;
	}
	public void setCdParticipante(Long cdParticipante) {
		this.cdParticipante = cdParticipante;
	}
	public String getNmParticipante() {
		return nmParticipante;
	}
	public void setNmParticipante(String nmParticipante) {
		this.nmParticipante = nmParticipante;
	}
	public Integer getCdBanco() {
		return cdBanco;
	}
	public void setCdBanco(Integer cdBanco) {
		this.cdBanco = cdBanco;
	}
	public String getDsBanco() {
		return dsBanco;
	}
	public void setDsBanco(String dsBanco) {
		this.dsBanco = dsBanco;
	}
	public Integer getCdAgencia() {
		return cdAgencia;
	}
	public void setCdAgencia(Integer cdAgencia) {
		this.cdAgencia = cdAgencia;
	}
	public Integer getCdDigitoAgencia() {
		return cdDigitoAgencia;
	}
	public void setCdDigitoAgencia(Integer cdDigitoAgencia) {
		this.cdDigitoAgencia = cdDigitoAgencia;
	}
	public String getDsAgencia() {
		return dsAgencia;
	}
	public void setDsAgencia(String dsAgencia) {
		this.dsAgencia = dsAgencia;
	}
	public Long getCdConta() {
		return cdConta;
	}
	public void setCdConta(Long cdConta) {
		this.cdConta = cdConta;
	}
	public String getCdDigitoConta() {
		return cdDigitoConta;
	}
	public void setCdDigitoConta(String cdDigitoConta) {
		this.cdDigitoConta = cdDigitoConta;
	}
	public Integer getCdTipoConta() {
		return cdTipoConta;
	}
	public void setCdTipoConta(Integer cdTipoConta) {
		this.cdTipoConta = cdTipoConta;
	}
	public String getDsTipoConta() {
		return dsTipoConta;
	}
	public void setDsTipoConta(String dsTipoConta) {
		this.dsTipoConta = dsTipoConta;
	}

}
