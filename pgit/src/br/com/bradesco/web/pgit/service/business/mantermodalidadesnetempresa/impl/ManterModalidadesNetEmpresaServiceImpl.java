/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/implementation.ftl,v $
 * $Id: implementation.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: implementation.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.mantermodalidadesnetempresa.impl;

import java.util.ArrayList;
import java.util.List;

import br.com.bradesco.web.pgit.service.business.mantermodalidadesnetempresa.IManterModalidadesNetEmpresaService;
import br.com.bradesco.web.pgit.service.business.mantermodalidadesnetempresa.bean.AlterarHorarioLimiteEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantermodalidadesnetempresa.bean.AlterarHorarioLimiteSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantermodalidadesnetempresa.bean.DetalharHorarioLimiteEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantermodalidadesnetempresa.bean.DetalharHorarioLimiteSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantermodalidadesnetempresa.bean.ExcluirHorarioLimiteEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantermodalidadesnetempresa.bean.ExcluirHorarioLimiteSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantermodalidadesnetempresa.bean.ListarHorarioLimiteEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantermodalidadesnetempresa.bean.ListarHorarioLimiteOcorrenciasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantermodalidadesnetempresa.bean.ListarHorarioLimiteSaidaDTO;
import br.com.bradesco.web.pgit.service.data.pdc.FactoryAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.alterarhorariolimite.request.AlterarHorarioLimiteRequest;
import br.com.bradesco.web.pgit.service.data.pdc.alterarhorariolimite.response.AlterarHorarioLimiteResponse;
import br.com.bradesco.web.pgit.service.data.pdc.detalharhorariolimite.request.DetalharHorarioLimiteRequest;
import br.com.bradesco.web.pgit.service.data.pdc.detalharhorariolimite.response.DetalharHorarioLimiteResponse;
import br.com.bradesco.web.pgit.service.data.pdc.excluirhorariolimite.request.ExcluirHorarioLimiteRequest;
import br.com.bradesco.web.pgit.service.data.pdc.excluirhorariolimite.response.ExcluirHorarioLimiteResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarhorariolimite.request.ListarHorarioLimiteRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarhorariolimite.response.ListarHorarioLimiteResponse;

/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Implementa��o do adaptador: ManterOperacoesServRelacionado
 * </p>
 * 
 * @comment C�DIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public class ManterModalidadesNetEmpresaServiceImpl implements IManterModalidadesNetEmpresaService {

    /** Atributo factoryAdapter. */
    private FactoryAdapter factoryAdapter;

    /**
     * Get: factoryAdapter.
     *
     * @return factoryAdapter
     */
    public FactoryAdapter getFactoryAdapter() {
	return factoryAdapter;
    }

    /**
     * Construtor.
     */
    public ManterModalidadesNetEmpresaServiceImpl() {
	// TODO: Implementa��o
    }

    /**
     * Set: factoryAdapter.
     *
     * @param factoryAdapter the factory adapter
     */
    public void setFactoryAdapter(FactoryAdapter factoryAdapter) {
	this.factoryAdapter = factoryAdapter;
    }

    /* (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.funcoesalcada.IFuncoesAlcadaService#listarFuncoesAlcadas(br.com.bradesco.web.pgit.service.business.funcoesalcada.bean.ListarFuncoesAlcadaEntradaDTO)
     */
    public ListarHorarioLimiteSaidaDTO listarHorarioLimite(ListarHorarioLimiteEntradaDTO entrada) {

	ListarHorarioLimiteRequest request = new ListarHorarioLimiteRequest();
	request.setNumeroOcorrencias(entrada.getNumeroOcorrencias());

	ListarHorarioLimiteResponse response = getFactoryAdapter().getListarHorarioLimitePDCAdapter().invokeProcess(request);

	ListarHorarioLimiteSaidaDTO saidaDto = new ListarHorarioLimiteSaidaDTO();

	saidaDto.setNrModalidades( response.getNrModalidades());
	List<ListarHorarioLimiteOcorrenciasSaidaDTO> ocorrencias = new ArrayList<ListarHorarioLimiteOcorrenciasSaidaDTO>();

	for (int i = 0; i < response.getOcorrenciasCount(); i++) {
	    ListarHorarioLimiteOcorrenciasSaidaDTO ocorrencia = new ListarHorarioLimiteOcorrenciasSaidaDTO();

	    ocorrencia.setCdModalidade(response.getOcorrencias(i).getCdModalidade());
	    ocorrencia.setDsModalidade(response.getOcorrencias(i).getDsModalidade());
	    ocorrencia.setHrLimiteAutorizacao(response.getOcorrencias(i).getHrLimiteAutorizacao());
	    ocorrencia.setHrLimiteTransmissao(response.getOcorrencias(i).getHrLimiteTransmissao());
	    ocorrencia.setCdSequenciaTipoModalidade(response.getOcorrencias(i).getCdSequencialOrdemTipoModalidade());
	    ocorrencia.setCdSituacaoTipoModalidade(response.getOcorrencias(i).getCdSituacaoTipoModalidade());
	    ocorrencia.setHrLimiteManutencao(response.getOcorrencias(i).getHrLimiteManutencao());
	    ocorrencias.add(ocorrencia);
	}
	
	saidaDto.setOcorrencias(ocorrencias);
	return saidaDto;
    }

    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.funcoesalcada.IFuncoesAlcadaService#detalharFuncoesAlcadas(br.com.bradesco.web.pgit.service.business.funcoesalcada.bean.DetalharFuncoesAlcadaEntradaDTO)
     */
    public DetalharHorarioLimiteSaidaDTO detalharHorarioLimite(DetalharHorarioLimiteEntradaDTO detalharHorarioLimiteEntradaDTO) {
	DetalharHorarioLimiteRequest request = new DetalharHorarioLimiteRequest();
	request.setCdModalidade(detalharHorarioLimiteEntradaDTO.getCdModalidade());

	DetalharHorarioLimiteResponse response = getFactoryAdapter().getDetalharHorarioLimitePDCAdapter().invokeProcess(request);

	DetalharHorarioLimiteSaidaDTO saidaDTO = new DetalharHorarioLimiteSaidaDTO();
	
	saidaDTO.setCdModalidade(response.getCdModalidade());
	saidaDTO.setDsModalidade(response.getDsModalidade());
	saidaDTO.setHrLimiteAutorizacao(response.getHrLimiteAutorizacao());
	saidaDTO.setHrLimiteTransmissao(response.getHrLimiteTransmissao());
	
	saidaDTO.setHrLimiteManutencao(response.getHrLimiteManutencao());
	saidaDTO.setCdSequenciaTipoModalidade(response.getCdSequencialOrdemTipoModalidade());
	saidaDTO.setCdSituacaoTipoModalidade(response.getCdSituacaoTipoModalidade());

	return saidaDTO;

    }

    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.funcoesalcada.IFuncoesAlcadaService#excluirFuncoesAlcada(br.com.bradesco.web.pgit.service.business.funcoesalcada.bean.ExcluirFuncoesAlcadaEntradaDTO)
     */
    public ExcluirHorarioLimiteSaidaDTO excluirHorarioLimite(ExcluirHorarioLimiteEntradaDTO entrada) {
	ExcluirHorarioLimiteRequest request = new ExcluirHorarioLimiteRequest();
	request.setCdModalidade(entrada.getCdModalidade());
	request.setCdSequenciaTipoModalidade(entrada.getCdSequenciaTipoModalidade());
	request.setCdSituacaoTipoModalidade(entrada.getCdSituacaoTipoModalidade());
	request.setDsModalidade(entrada.getDsModalidade());
	request.setHrLimiteAutorizacao(entrada.getHrLimiteAutorizacao());
	request.setHrLimiteTransmissao(entrada.getHrLimiteTransmissao());
	request.setHrLimiteManutencao(entrada.getHrLimiteManutencao());
	ExcluirHorarioLimiteResponse response = getFactoryAdapter().getExcluirHorarioLimitePDCAdapter().invokeProcess(request);
	
	ExcluirHorarioLimiteSaidaDTO excluirHorarioLimiteSaidaDTO = new ExcluirHorarioLimiteSaidaDTO();
	excluirHorarioLimiteSaidaDTO.setCodMensagem(response.getCodMensagem());
	excluirHorarioLimiteSaidaDTO.setMensagem(response.getMensagem());

	return excluirHorarioLimiteSaidaDTO;
    }

    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.funcoesalcada.IFuncoesAlcadaService#alterarFuncoesAlcada(br.com.bradesco.web.pgit.service.business.funcoesalcada.bean.AlterarFuncoesAlcadaEntradaDTO)
     */
    public AlterarHorarioLimiteSaidaDTO alterarHorarioLimite(AlterarHorarioLimiteEntradaDTO alterarHorarioLimiteEntradaDTO) {
	 AlterarHorarioLimiteRequest request = new AlterarHorarioLimiteRequest();

	request.setDsModalidade(alterarHorarioLimiteEntradaDTO.getDsModalidade());
	request.setHrLimiteAutorizacao(alterarHorarioLimiteEntradaDTO.getHrLimiteAutorizacao());
	request.setCdModalidade(alterarHorarioLimiteEntradaDTO.getCdModalidade());
	request.setHrLimiteTransmissao(alterarHorarioLimiteEntradaDTO.getHrLimiteTransmissao());
	request.setCdSequenciaTipoModalidade(alterarHorarioLimiteEntradaDTO.getCdSequenciaTipoModalidade());
	request.setCdSituacaoTipoModalidade(alterarHorarioLimiteEntradaDTO.getCdSituacaoTipoModalidade());
	request.setHrLimiteManutencao(alterarHorarioLimiteEntradaDTO.getHrLimiteManutencao());
	
	AlterarHorarioLimiteResponse response = getFactoryAdapter().getAlterarHorarioLimitePDCAdapter().invokeProcess(request);

	AlterarHorarioLimiteSaidaDTO saidaDto = new AlterarHorarioLimiteSaidaDTO();
	saidaDto.setCodMensagem(response.getCodMensagem());
	saidaDto.setMensagem(response.getMensagem());

	return saidaDto;
    }
}

