/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.detalharhistoricooperacaoservicopagtointegrado.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.math.BigDecimal;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class DetalharHistoricoOperacaoServicoPagtoIntegradoResponse.
 * 
 * @version $Revision$ $Date$
 */
public class DetalharHistoricoOperacaoServicoPagtoIntegradoResponse implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _codMensagem
     */
    private java.lang.String _codMensagem;

    /**
     * Field _mensagem
     */
    private java.lang.String _mensagem;

    /**
     * Field _cdOperacaoServicoPagamentoIntegrado
     */
    private int _cdOperacaoServicoPagamentoIntegrado = 0;

    /**
     * keeps track of state for field:
     * _cdOperacaoServicoPagamentoIntegrado
     */
    private boolean _has_cdOperacaoServicoPagamentoIntegrado;

    /**
     * Field _cdNaturezaOperacaoPagamento
     */
    private int _cdNaturezaOperacaoPagamento = 0;

    /**
     * keeps track of state for field: _cdNaturezaOperacaoPagamento
     */
    private boolean _has_cdNaturezaOperacaoPagamento;

    /**
     * Field _dsNaturezaOperacaoPagamento
     */
    private java.lang.String _dsNaturezaOperacaoPagamento;

    /**
     * Field _cdTipoCondcTarifa
     */
    private int _cdTipoCondcTarifa = 0;

    /**
     * keeps track of state for field: _cdTipoCondcTarifa
     */
    private boolean _has_cdTipoCondcTarifa;

    /**
     * Field _dsTipoCondcTarifa
     */
    private java.lang.String _dsTipoCondcTarifa;

    /**
     * Field _vrAlcadaTarifaAgencia
     */
    private java.math.BigDecimal _vrAlcadaTarifaAgencia = new java.math.BigDecimal("0");

    /**
     * Field _pcAlcadaTarifaAgencia
     */
    private java.math.BigDecimal _pcAlcadaTarifaAgencia = new java.math.BigDecimal("0");

    /**
     * Field _cdOperCanalInclusao
     */
    private java.lang.String _cdOperCanalInclusao;

    /**
     * Field _cdTipoCanalInclusao
     */
    private int _cdTipoCanalInclusao = 0;

    /**
     * keeps track of state for field: _cdTipoCanalInclusao
     */
    private boolean _has_cdTipoCanalInclusao;

    /**
     * Field _dsTipoCanalInclusao
     */
    private java.lang.String _dsTipoCanalInclusao;

    /**
     * Field _cdUsuarioInclusao
     */
    private java.lang.String _cdUsuarioInclusao;

    /**
     * Field _cdUsuarioInclusaoExterno
     */
    private java.lang.String _cdUsuarioInclusaoExterno;

    /**
     * Field _hrInclusaoRegistro
     */
    private java.lang.String _hrInclusaoRegistro;

    /**
     * Field _cdOperacaoCanalManutencao
     */
    private java.lang.String _cdOperacaoCanalManutencao;

    /**
     * Field _cdTipoCanalManutencao
     */
    private int _cdTipoCanalManutencao = 0;

    /**
     * keeps track of state for field: _cdTipoCanalManutencao
     */
    private boolean _has_cdTipoCanalManutencao;

    /**
     * Field _dsTipoCanalManutencao
     */
    private java.lang.String _dsTipoCanalManutencao;

    /**
     * Field _cdUsuarioManutencao
     */
    private java.lang.String _cdUsuarioManutencao;

    /**
     * Field _cdUsuarioManutencaoExterno
     */
    private java.lang.String _cdUsuarioManutencaoExterno;

    /**
     * Field _hrManutencaoRegistro
     */
    private java.lang.String _hrManutencaoRegistro;


      //----------------/
     //- Constructors -/
    //----------------/

    public DetalharHistoricoOperacaoServicoPagtoIntegradoResponse() 
     {
        super();
        setVrAlcadaTarifaAgencia(new java.math.BigDecimal("0"));
        setPcAlcadaTarifaAgencia(new java.math.BigDecimal("0"));
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharhistoricooperacaoservicopagtointegrado.response.DetalharHistoricoOperacaoServicoPagtoIntegradoResponse()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdNaturezaOperacaoPagamento
     * 
     */
    public void deleteCdNaturezaOperacaoPagamento()
    {
        this._has_cdNaturezaOperacaoPagamento= false;
    } //-- void deleteCdNaturezaOperacaoPagamento() 

    /**
     * Method deleteCdOperacaoServicoPagamentoIntegrado
     * 
     */
    public void deleteCdOperacaoServicoPagamentoIntegrado()
    {
        this._has_cdOperacaoServicoPagamentoIntegrado= false;
    } //-- void deleteCdOperacaoServicoPagamentoIntegrado() 

    /**
     * Method deleteCdTipoCanalInclusao
     * 
     */
    public void deleteCdTipoCanalInclusao()
    {
        this._has_cdTipoCanalInclusao= false;
    } //-- void deleteCdTipoCanalInclusao() 

    /**
     * Method deleteCdTipoCanalManutencao
     * 
     */
    public void deleteCdTipoCanalManutencao()
    {
        this._has_cdTipoCanalManutencao= false;
    } //-- void deleteCdTipoCanalManutencao() 

    /**
     * Method deleteCdTipoCondcTarifa
     * 
     */
    public void deleteCdTipoCondcTarifa()
    {
        this._has_cdTipoCondcTarifa= false;
    } //-- void deleteCdTipoCondcTarifa() 

    /**
     * Returns the value of field 'cdNaturezaOperacaoPagamento'.
     * 
     * @return int
     * @return the value of field 'cdNaturezaOperacaoPagamento'.
     */
    public int getCdNaturezaOperacaoPagamento()
    {
        return this._cdNaturezaOperacaoPagamento;
    } //-- int getCdNaturezaOperacaoPagamento() 

    /**
     * Returns the value of field 'cdOperCanalInclusao'.
     * 
     * @return String
     * @return the value of field 'cdOperCanalInclusao'.
     */
    public java.lang.String getCdOperCanalInclusao()
    {
        return this._cdOperCanalInclusao;
    } //-- java.lang.String getCdOperCanalInclusao() 

    /**
     * Returns the value of field 'cdOperacaoCanalManutencao'.
     * 
     * @return String
     * @return the value of field 'cdOperacaoCanalManutencao'.
     */
    public java.lang.String getCdOperacaoCanalManutencao()
    {
        return this._cdOperacaoCanalManutencao;
    } //-- java.lang.String getCdOperacaoCanalManutencao() 

    /**
     * Returns the value of field
     * 'cdOperacaoServicoPagamentoIntegrado'.
     * 
     * @return int
     * @return the value of field
     * 'cdOperacaoServicoPagamentoIntegrado'.
     */
    public int getCdOperacaoServicoPagamentoIntegrado()
    {
        return this._cdOperacaoServicoPagamentoIntegrado;
    } //-- int getCdOperacaoServicoPagamentoIntegrado() 

    /**
     * Returns the value of field 'cdTipoCanalInclusao'.
     * 
     * @return int
     * @return the value of field 'cdTipoCanalInclusao'.
     */
    public int getCdTipoCanalInclusao()
    {
        return this._cdTipoCanalInclusao;
    } //-- int getCdTipoCanalInclusao() 

    /**
     * Returns the value of field 'cdTipoCanalManutencao'.
     * 
     * @return int
     * @return the value of field 'cdTipoCanalManutencao'.
     */
    public int getCdTipoCanalManutencao()
    {
        return this._cdTipoCanalManutencao;
    } //-- int getCdTipoCanalManutencao() 

    /**
     * Returns the value of field 'cdTipoCondcTarifa'.
     * 
     * @return int
     * @return the value of field 'cdTipoCondcTarifa'.
     */
    public int getCdTipoCondcTarifa()
    {
        return this._cdTipoCondcTarifa;
    } //-- int getCdTipoCondcTarifa() 

    /**
     * Returns the value of field 'cdUsuarioInclusao'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioInclusao'.
     */
    public java.lang.String getCdUsuarioInclusao()
    {
        return this._cdUsuarioInclusao;
    } //-- java.lang.String getCdUsuarioInclusao() 

    /**
     * Returns the value of field 'cdUsuarioInclusaoExterno'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioInclusaoExterno'.
     */
    public java.lang.String getCdUsuarioInclusaoExterno()
    {
        return this._cdUsuarioInclusaoExterno;
    } //-- java.lang.String getCdUsuarioInclusaoExterno() 

    /**
     * Returns the value of field 'cdUsuarioManutencao'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioManutencao'.
     */
    public java.lang.String getCdUsuarioManutencao()
    {
        return this._cdUsuarioManutencao;
    } //-- java.lang.String getCdUsuarioManutencao() 

    /**
     * Returns the value of field 'cdUsuarioManutencaoExterno'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioManutencaoExterno'.
     */
    public java.lang.String getCdUsuarioManutencaoExterno()
    {
        return this._cdUsuarioManutencaoExterno;
    } //-- java.lang.String getCdUsuarioManutencaoExterno() 

    /**
     * Returns the value of field 'codMensagem'.
     * 
     * @return String
     * @return the value of field 'codMensagem'.
     */
    public java.lang.String getCodMensagem()
    {
        return this._codMensagem;
    } //-- java.lang.String getCodMensagem() 

    /**
     * Returns the value of field 'dsNaturezaOperacaoPagamento'.
     * 
     * @return String
     * @return the value of field 'dsNaturezaOperacaoPagamento'.
     */
    public java.lang.String getDsNaturezaOperacaoPagamento()
    {
        return this._dsNaturezaOperacaoPagamento;
    } //-- java.lang.String getDsNaturezaOperacaoPagamento() 

    /**
     * Returns the value of field 'dsTipoCanalInclusao'.
     * 
     * @return String
     * @return the value of field 'dsTipoCanalInclusao'.
     */
    public java.lang.String getDsTipoCanalInclusao()
    {
        return this._dsTipoCanalInclusao;
    } //-- java.lang.String getDsTipoCanalInclusao() 

    /**
     * Returns the value of field 'dsTipoCanalManutencao'.
     * 
     * @return String
     * @return the value of field 'dsTipoCanalManutencao'.
     */
    public java.lang.String getDsTipoCanalManutencao()
    {
        return this._dsTipoCanalManutencao;
    } //-- java.lang.String getDsTipoCanalManutencao() 

    /**
     * Returns the value of field 'dsTipoCondcTarifa'.
     * 
     * @return String
     * @return the value of field 'dsTipoCondcTarifa'.
     */
    public java.lang.String getDsTipoCondcTarifa()
    {
        return this._dsTipoCondcTarifa;
    } //-- java.lang.String getDsTipoCondcTarifa() 

    /**
     * Returns the value of field 'hrInclusaoRegistro'.
     * 
     * @return String
     * @return the value of field 'hrInclusaoRegistro'.
     */
    public java.lang.String getHrInclusaoRegistro()
    {
        return this._hrInclusaoRegistro;
    } //-- java.lang.String getHrInclusaoRegistro() 

    /**
     * Returns the value of field 'hrManutencaoRegistro'.
     * 
     * @return String
     * @return the value of field 'hrManutencaoRegistro'.
     */
    public java.lang.String getHrManutencaoRegistro()
    {
        return this._hrManutencaoRegistro;
    } //-- java.lang.String getHrManutencaoRegistro() 

    /**
     * Returns the value of field 'mensagem'.
     * 
     * @return String
     * @return the value of field 'mensagem'.
     */
    public java.lang.String getMensagem()
    {
        return this._mensagem;
    } //-- java.lang.String getMensagem() 

    /**
     * Returns the value of field 'pcAlcadaTarifaAgencia'.
     * 
     * @return BigDecimal
     * @return the value of field 'pcAlcadaTarifaAgencia'.
     */
    public java.math.BigDecimal getPcAlcadaTarifaAgencia()
    {
        return this._pcAlcadaTarifaAgencia;
    } //-- java.math.BigDecimal getPcAlcadaTarifaAgencia() 

    /**
     * Returns the value of field 'vrAlcadaTarifaAgencia'.
     * 
     * @return BigDecimal
     * @return the value of field 'vrAlcadaTarifaAgencia'.
     */
    public java.math.BigDecimal getVrAlcadaTarifaAgencia()
    {
        return this._vrAlcadaTarifaAgencia;
    } //-- java.math.BigDecimal getVrAlcadaTarifaAgencia() 

    /**
     * Method hasCdNaturezaOperacaoPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdNaturezaOperacaoPagamento()
    {
        return this._has_cdNaturezaOperacaoPagamento;
    } //-- boolean hasCdNaturezaOperacaoPagamento() 

    /**
     * Method hasCdOperacaoServicoPagamentoIntegrado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdOperacaoServicoPagamentoIntegrado()
    {
        return this._has_cdOperacaoServicoPagamentoIntegrado;
    } //-- boolean hasCdOperacaoServicoPagamentoIntegrado() 

    /**
     * Method hasCdTipoCanalInclusao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoCanalInclusao()
    {
        return this._has_cdTipoCanalInclusao;
    } //-- boolean hasCdTipoCanalInclusao() 

    /**
     * Method hasCdTipoCanalManutencao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoCanalManutencao()
    {
        return this._has_cdTipoCanalManutencao;
    } //-- boolean hasCdTipoCanalManutencao() 

    /**
     * Method hasCdTipoCondcTarifa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoCondcTarifa()
    {
        return this._has_cdTipoCondcTarifa;
    } //-- boolean hasCdTipoCondcTarifa() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdNaturezaOperacaoPagamento'.
     * 
     * @param cdNaturezaOperacaoPagamento the value of field
     * 'cdNaturezaOperacaoPagamento'.
     */
    public void setCdNaturezaOperacaoPagamento(int cdNaturezaOperacaoPagamento)
    {
        this._cdNaturezaOperacaoPagamento = cdNaturezaOperacaoPagamento;
        this._has_cdNaturezaOperacaoPagamento = true;
    } //-- void setCdNaturezaOperacaoPagamento(int) 

    /**
     * Sets the value of field 'cdOperCanalInclusao'.
     * 
     * @param cdOperCanalInclusao the value of field
     * 'cdOperCanalInclusao'.
     */
    public void setCdOperCanalInclusao(java.lang.String cdOperCanalInclusao)
    {
        this._cdOperCanalInclusao = cdOperCanalInclusao;
    } //-- void setCdOperCanalInclusao(java.lang.String) 

    /**
     * Sets the value of field 'cdOperacaoCanalManutencao'.
     * 
     * @param cdOperacaoCanalManutencao the value of field
     * 'cdOperacaoCanalManutencao'.
     */
    public void setCdOperacaoCanalManutencao(java.lang.String cdOperacaoCanalManutencao)
    {
        this._cdOperacaoCanalManutencao = cdOperacaoCanalManutencao;
    } //-- void setCdOperacaoCanalManutencao(java.lang.String) 

    /**
     * Sets the value of field
     * 'cdOperacaoServicoPagamentoIntegrado'.
     * 
     * @param cdOperacaoServicoPagamentoIntegrado the value of
     * field 'cdOperacaoServicoPagamentoIntegrado'.
     */
    public void setCdOperacaoServicoPagamentoIntegrado(int cdOperacaoServicoPagamentoIntegrado)
    {
        this._cdOperacaoServicoPagamentoIntegrado = cdOperacaoServicoPagamentoIntegrado;
        this._has_cdOperacaoServicoPagamentoIntegrado = true;
    } //-- void setCdOperacaoServicoPagamentoIntegrado(int) 

    /**
     * Sets the value of field 'cdTipoCanalInclusao'.
     * 
     * @param cdTipoCanalInclusao the value of field
     * 'cdTipoCanalInclusao'.
     */
    public void setCdTipoCanalInclusao(int cdTipoCanalInclusao)
    {
        this._cdTipoCanalInclusao = cdTipoCanalInclusao;
        this._has_cdTipoCanalInclusao = true;
    } //-- void setCdTipoCanalInclusao(int) 

    /**
     * Sets the value of field 'cdTipoCanalManutencao'.
     * 
     * @param cdTipoCanalManutencao the value of field
     * 'cdTipoCanalManutencao'.
     */
    public void setCdTipoCanalManutencao(int cdTipoCanalManutencao)
    {
        this._cdTipoCanalManutencao = cdTipoCanalManutencao;
        this._has_cdTipoCanalManutencao = true;
    } //-- void setCdTipoCanalManutencao(int) 

    /**
     * Sets the value of field 'cdTipoCondcTarifa'.
     * 
     * @param cdTipoCondcTarifa the value of field
     * 'cdTipoCondcTarifa'.
     */
    public void setCdTipoCondcTarifa(int cdTipoCondcTarifa)
    {
        this._cdTipoCondcTarifa = cdTipoCondcTarifa;
        this._has_cdTipoCondcTarifa = true;
    } //-- void setCdTipoCondcTarifa(int) 

    /**
     * Sets the value of field 'cdUsuarioInclusao'.
     * 
     * @param cdUsuarioInclusao the value of field
     * 'cdUsuarioInclusao'.
     */
    public void setCdUsuarioInclusao(java.lang.String cdUsuarioInclusao)
    {
        this._cdUsuarioInclusao = cdUsuarioInclusao;
    } //-- void setCdUsuarioInclusao(java.lang.String) 

    /**
     * Sets the value of field 'cdUsuarioInclusaoExterno'.
     * 
     * @param cdUsuarioInclusaoExterno the value of field
     * 'cdUsuarioInclusaoExterno'.
     */
    public void setCdUsuarioInclusaoExterno(java.lang.String cdUsuarioInclusaoExterno)
    {
        this._cdUsuarioInclusaoExterno = cdUsuarioInclusaoExterno;
    } //-- void setCdUsuarioInclusaoExterno(java.lang.String) 

    /**
     * Sets the value of field 'cdUsuarioManutencao'.
     * 
     * @param cdUsuarioManutencao the value of field
     * 'cdUsuarioManutencao'.
     */
    public void setCdUsuarioManutencao(java.lang.String cdUsuarioManutencao)
    {
        this._cdUsuarioManutencao = cdUsuarioManutencao;
    } //-- void setCdUsuarioManutencao(java.lang.String) 

    /**
     * Sets the value of field 'cdUsuarioManutencaoExterno'.
     * 
     * @param cdUsuarioManutencaoExterno the value of field
     * 'cdUsuarioManutencaoExterno'.
     */
    public void setCdUsuarioManutencaoExterno(java.lang.String cdUsuarioManutencaoExterno)
    {
        this._cdUsuarioManutencaoExterno = cdUsuarioManutencaoExterno;
    } //-- void setCdUsuarioManutencaoExterno(java.lang.String) 

    /**
     * Sets the value of field 'codMensagem'.
     * 
     * @param codMensagem the value of field 'codMensagem'.
     */
    public void setCodMensagem(java.lang.String codMensagem)
    {
        this._codMensagem = codMensagem;
    } //-- void setCodMensagem(java.lang.String) 

    /**
     * Sets the value of field 'dsNaturezaOperacaoPagamento'.
     * 
     * @param dsNaturezaOperacaoPagamento the value of field
     * 'dsNaturezaOperacaoPagamento'.
     */
    public void setDsNaturezaOperacaoPagamento(java.lang.String dsNaturezaOperacaoPagamento)
    {
        this._dsNaturezaOperacaoPagamento = dsNaturezaOperacaoPagamento;
    } //-- void setDsNaturezaOperacaoPagamento(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoCanalInclusao'.
     * 
     * @param dsTipoCanalInclusao the value of field
     * 'dsTipoCanalInclusao'.
     */
    public void setDsTipoCanalInclusao(java.lang.String dsTipoCanalInclusao)
    {
        this._dsTipoCanalInclusao = dsTipoCanalInclusao;
    } //-- void setDsTipoCanalInclusao(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoCanalManutencao'.
     * 
     * @param dsTipoCanalManutencao the value of field
     * 'dsTipoCanalManutencao'.
     */
    public void setDsTipoCanalManutencao(java.lang.String dsTipoCanalManutencao)
    {
        this._dsTipoCanalManutencao = dsTipoCanalManutencao;
    } //-- void setDsTipoCanalManutencao(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoCondcTarifa'.
     * 
     * @param dsTipoCondcTarifa the value of field
     * 'dsTipoCondcTarifa'.
     */
    public void setDsTipoCondcTarifa(java.lang.String dsTipoCondcTarifa)
    {
        this._dsTipoCondcTarifa = dsTipoCondcTarifa;
    } //-- void setDsTipoCondcTarifa(java.lang.String) 

    /**
     * Sets the value of field 'hrInclusaoRegistro'.
     * 
     * @param hrInclusaoRegistro the value of field
     * 'hrInclusaoRegistro'.
     */
    public void setHrInclusaoRegistro(java.lang.String hrInclusaoRegistro)
    {
        this._hrInclusaoRegistro = hrInclusaoRegistro;
    } //-- void setHrInclusaoRegistro(java.lang.String) 

    /**
     * Sets the value of field 'hrManutencaoRegistro'.
     * 
     * @param hrManutencaoRegistro the value of field
     * 'hrManutencaoRegistro'.
     */
    public void setHrManutencaoRegistro(java.lang.String hrManutencaoRegistro)
    {
        this._hrManutencaoRegistro = hrManutencaoRegistro;
    } //-- void setHrManutencaoRegistro(java.lang.String) 

    /**
     * Sets the value of field 'mensagem'.
     * 
     * @param mensagem the value of field 'mensagem'.
     */
    public void setMensagem(java.lang.String mensagem)
    {
        this._mensagem = mensagem;
    } //-- void setMensagem(java.lang.String) 

    /**
     * Sets the value of field 'pcAlcadaTarifaAgencia'.
     * 
     * @param pcAlcadaTarifaAgencia the value of field
     * 'pcAlcadaTarifaAgencia'.
     */
    public void setPcAlcadaTarifaAgencia(java.math.BigDecimal pcAlcadaTarifaAgencia)
    {
        this._pcAlcadaTarifaAgencia = pcAlcadaTarifaAgencia;
    } //-- void setPcAlcadaTarifaAgencia(java.math.BigDecimal) 

    /**
     * Sets the value of field 'vrAlcadaTarifaAgencia'.
     * 
     * @param vrAlcadaTarifaAgencia the value of field
     * 'vrAlcadaTarifaAgencia'.
     */
    public void setVrAlcadaTarifaAgencia(java.math.BigDecimal vrAlcadaTarifaAgencia)
    {
        this._vrAlcadaTarifaAgencia = vrAlcadaTarifaAgencia;
    } //-- void setVrAlcadaTarifaAgencia(java.math.BigDecimal) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return DetalharHistoricoOperacaoServicoPagtoIntegradoRespons
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.detalharhistoricooperacaoservicopagtointegrado.response.DetalharHistoricoOperacaoServicoPagtoIntegradoResponse unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.detalharhistoricooperacaoservicopagtointegrado.response.DetalharHistoricoOperacaoServicoPagtoIntegradoResponse) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.detalharhistoricooperacaoservicopagtointegrado.response.DetalharHistoricoOperacaoServicoPagtoIntegradoResponse.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharhistoricooperacaoservicopagtointegrado.response.DetalharHistoricoOperacaoServicoPagtoIntegradoResponse unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
