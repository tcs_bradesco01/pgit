/**
 * Nome: br.com.bradesco.web.pgit.service.business.contacomplementar
 * Compilador: JDK 1.5
 * Prop�sito: INSERIR O PROP�SITO DAS CLASSES DO PACOTE
 * Data da cria��o: <dd/MM/yyyy>
 * Par�metros de compila��o: -d
 */
package br.com.bradesco.web.pgit.service.business.contacomplementar;

import java.util.List;

import br.com.bradesco.web.pgit.service.business.contacomplementar.bean.AlterarContaComplementarSaidaDTO;
import br.com.bradesco.web.pgit.service.business.contacomplementar.bean.DetalharContaComplementarEntradaDTO;
import br.com.bradesco.web.pgit.service.business.contacomplementar.bean.DetalharContaComplementarSaidaDTO;
import br.com.bradesco.web.pgit.service.business.contacomplementar.bean.IncluirContaComplementarEntradaDTO;
import br.com.bradesco.web.pgit.service.business.contacomplementar.bean.IncluirContaComplementarSaidaDTO;
import br.com.bradesco.web.pgit.service.business.contacomplementar.bean.ListarContaComplParticipantesEntradaDTO;
import br.com.bradesco.web.pgit.service.business.contacomplementar.bean.ListarContaComplParticipantesSaidaDTO;
import br.com.bradesco.web.pgit.service.business.contacomplementar.bean.ListarContaComplementarEntrada;
import br.com.bradesco.web.pgit.service.business.contacomplementar.bean.ListarContaComplementarSaida;

/**
 * Nome: IContaComplementarService
 * <p>
 * Prop�sito: Interface do adaptador ContaComplementar
 * </p>
 * 
 * @author CPM Braxis / TI Melhorias - Arquitetura
 * @version 1.0
 */
public interface IContaComplementarService {
	
	ListarContaComplementarSaida listarContasComplementar(ListarContaComplementarEntrada entrada);
	
	List<ListarContaComplParticipantesSaidaDTO> listaParticipantes(ListarContaComplParticipantesEntradaDTO entrada);
	
	IncluirContaComplementarSaidaDTO incluirContaComplementar(IncluirContaComplementarEntradaDTO entrada);
	
	AlterarContaComplementarSaidaDTO alterarContaComplementar(IncluirContaComplementarEntradaDTO entrada);
	
	DetalharContaComplementarSaidaDTO detalharContaComplementar(DetalharContaComplementarEntradaDTO entrada);
	
	AlterarContaComplementarSaidaDTO excluirContaComplementar(IncluirContaComplementarEntradaDTO entrada);
}