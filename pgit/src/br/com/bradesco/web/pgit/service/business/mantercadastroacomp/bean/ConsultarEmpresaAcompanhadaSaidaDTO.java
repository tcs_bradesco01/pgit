/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantercadastroacomp.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantercadastroacomp.bean;

import java.util.List;

/**
 * Nome: ConsultarEmpresaAcompanhadaSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarEmpresaAcompanhadaSaidaDTO {
	
	/** Atributo codMensagem. */
	private String codMensagem;
    
    /** Atributo mensagem. */
    private String mensagem;
    
    /** Atributo nrOcorrencias. */
    private Integer nrOcorrencias;
    
    /** Atributo ocorrencias. */
    private List<ConsultarEmpresaAcompanhadaListaDTO> ocorrencias;
    
	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}
	
	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	/**
	 * Get: nrOcorrencias.
	 *
	 * @return nrOcorrencias
	 */
	public Integer getNrOcorrencias() {
		return nrOcorrencias;
	}
	
	/**
	 * Set: nrOcorrencias.
	 *
	 * @param nrOcorrencias the nr ocorrencias
	 */
	public void setNrOcorrencias(Integer nrOcorrencias) {
		this.nrOcorrencias = nrOcorrencias;
	}
	
	/**
	 * Get: ocorrencias.
	 *
	 * @return ocorrencias
	 */
	public List<ConsultarEmpresaAcompanhadaListaDTO> getOcorrencias() {
		return ocorrencias;
	}
	
	/**
	 * Set: ocorrencias.
	 *
	 * @param ocorrencias the ocorrencias
	 */
	public void setOcorrencias(List<ConsultarEmpresaAcompanhadaListaDTO> ocorrencias) {
		this.ocorrencias = ocorrencias;
	}
}
