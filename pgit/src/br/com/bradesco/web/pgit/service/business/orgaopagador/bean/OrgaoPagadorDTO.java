/*
 * Nome: br.com.bradesco.web.pgit.service.business.orgaopagador.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.orgaopagador.bean;

/**
 * Nome: OrgaoPagadorDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class OrgaoPagadorDTO {
	
	/** Atributo codigoOrgaoPagador. */
	private String codigoOrgaoPagador;
	
	/** Atributo agenciaResponsavel. */
	private String agenciaResponsavel;
	
	/** Atributo status. */
	private String status;
	
	/** Atributo empresaParceira. */
	private String empresaParceira;  // PCAB
	
	/** Atributo tarifa. */
	private String tarifa;
	
	
	/**
	 * Get: tarifa.
	 *
	 * @return tarifa
	 */
	public String getTarifa() {
		return tarifa;
	}
	
	/**
	 * Set: tarifa.
	 *
	 * @param tarifa the tarifa
	 */
	public void setTarifa(String tarifa) {
		this.tarifa = tarifa;
	}
	
	/**
	 * Get: agenciaResponsavel.
	 *
	 * @return agenciaResponsavel
	 */
	public String getAgenciaResponsavel() {
		return agenciaResponsavel;
	}
	
	/**
	 * Set: agenciaResponsavel.
	 *
	 * @param agenciaResponsavel the agencia responsavel
	 */
	public void setAgenciaResponsavel(String agenciaResponsavel) {
		this.agenciaResponsavel = agenciaResponsavel;
	}
	
	/**
	 * Get: codigoOrgaoPagador.
	 *
	 * @return codigoOrgaoPagador
	 */
	public String getCodigoOrgaoPagador() {
		return codigoOrgaoPagador;
	}
	
	/**
	 * Set: codigoOrgaoPagador.
	 *
	 * @param codigoOrgaoPagador the codigo orgao pagador
	 */
	public void setCodigoOrgaoPagador(String codigoOrgaoPagador) {
		this.codigoOrgaoPagador = codigoOrgaoPagador;
	}
	
	/**
	 * Get: empresaParceira.
	 *
	 * @return empresaParceira
	 */
	public String getEmpresaParceira() {
		return empresaParceira;
	}
	
	/**
	 * Set: empresaParceira.
	 *
	 * @param empresaParceira the empresa parceira
	 */
	public void setEmpresaParceira(String empresaParceira) {
		this.empresaParceira = empresaParceira;
	}
	
	/**
	 * Get: status.
	 *
	 * @return status
	 */
	public String getStatus() {
		return status;
	}
	
	/**
	 * Set: status.
	 *
	 * @param status the status
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	
	
	
	
	
	
	
	
}
