/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.substituirvinculoctasalarioempresa.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;

/**
 * Class SubstituirVinculoCtaSalarioEmpresaRequest.
 * 
 * @version $Revision$ $Date$
 */
public class SubstituirVinculoCtaSalarioEmpresaRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdPessoa
     */
    private long _cdPessoa = 0;

    /**
     * keeps track of state for field: _cdPessoa
     */
    private boolean _has_cdPessoa;

    /**
     * Field _cdTipoContrato
     */
    private int _cdTipoContrato = 0;

    /**
     * keeps track of state for field: _cdTipoContrato
     */
    private boolean _has_cdTipoContrato;

    /**
     * Field _nrSeqContrato
     */
    private long _nrSeqContrato = 0;

    /**
     * keeps track of state for field: _nrSeqContrato
     */
    private boolean _has_nrSeqContrato;

    /**
     * Field _cdPessoaJuridicaVinculo
     */
    private long _cdPessoaJuridicaVinculo = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridicaVinculo
     */
    private boolean _has_cdPessoaJuridicaVinculo;

    /**
     * Field _cdTipoContratoVinculo
     */
    private int _cdTipoContratoVinculo = 0;

    /**
     * keeps track of state for field: _cdTipoContratoVinculo
     */
    private boolean _has_cdTipoContratoVinculo;

    /**
     * Field _nrSeqContratoVinculo
     */
    private long _nrSeqContratoVinculo = 0;

    /**
     * keeps track of state for field: _nrSeqContratoVinculo
     */
    private boolean _has_nrSeqContratoVinculo;

    /**
     * Field _cdTipoVinculoContrato
     */
    private int _cdTipoVinculoContrato = 0;

    /**
     * keeps track of state for field: _cdTipoVinculoContrato
     */
    private boolean _has_cdTipoVinculoContrato;

    /**
     * Field _cdBancoAtualizado
     */
    private int _cdBancoAtualizado = 0;

    /**
     * keeps track of state for field: _cdBancoAtualizado
     */
    private boolean _has_cdBancoAtualizado;

    /**
     * Field _cdAgenciaAtualizada
     */
    private int _cdAgenciaAtualizada = 0;

    /**
     * keeps track of state for field: _cdAgenciaAtualizada
     */
    private boolean _has_cdAgenciaAtualizada;

    /**
     * Field _cdDigitoAgenciaAtualizada
     */
    private int _cdDigitoAgenciaAtualizada = 0;

    /**
     * keeps track of state for field: _cdDigitoAgenciaAtualizada
     */
    private boolean _has_cdDigitoAgenciaAtualizada;

    /**
     * Field _cdContaAtualizada
     */
    private long _cdContaAtualizada = 0;

    /**
     * keeps track of state for field: _cdContaAtualizada
     */
    private boolean _has_cdContaAtualizada;

    /**
     * Field _cdDigitoContaAtualizada
     */
    private java.lang.String _cdDigitoContaAtualizada;


      //----------------/
     //- Constructors -/
    //----------------/

    public SubstituirVinculoCtaSalarioEmpresaRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.substituirvinculoctasalarioempresa.request.SubstituirVinculoCtaSalarioEmpresaRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdAgenciaAtualizada
     * 
     */
    public void deleteCdAgenciaAtualizada()
    {
        this._has_cdAgenciaAtualizada= false;
    } //-- void deleteCdAgenciaAtualizada() 

    /**
     * Method deleteCdBancoAtualizado
     * 
     */
    public void deleteCdBancoAtualizado()
    {
        this._has_cdBancoAtualizado= false;
    } //-- void deleteCdBancoAtualizado() 

    /**
     * Method deleteCdContaAtualizada
     * 
     */
    public void deleteCdContaAtualizada()
    {
        this._has_cdContaAtualizada= false;
    } //-- void deleteCdContaAtualizada() 

    /**
     * Method deleteCdDigitoAgenciaAtualizada
     * 
     */
    public void deleteCdDigitoAgenciaAtualizada()
    {
        this._has_cdDigitoAgenciaAtualizada= false;
    } //-- void deleteCdDigitoAgenciaAtualizada() 

    /**
     * Method deleteCdPessoa
     * 
     */
    public void deleteCdPessoa()
    {
        this._has_cdPessoa= false;
    } //-- void deleteCdPessoa() 

    /**
     * Method deleteCdPessoaJuridicaVinculo
     * 
     */
    public void deleteCdPessoaJuridicaVinculo()
    {
        this._has_cdPessoaJuridicaVinculo= false;
    } //-- void deleteCdPessoaJuridicaVinculo() 

    /**
     * Method deleteCdTipoContrato
     * 
     */
    public void deleteCdTipoContrato()
    {
        this._has_cdTipoContrato= false;
    } //-- void deleteCdTipoContrato() 

    /**
     * Method deleteCdTipoContratoVinculo
     * 
     */
    public void deleteCdTipoContratoVinculo()
    {
        this._has_cdTipoContratoVinculo= false;
    } //-- void deleteCdTipoContratoVinculo() 

    /**
     * Method deleteCdTipoVinculoContrato
     * 
     */
    public void deleteCdTipoVinculoContrato()
    {
        this._has_cdTipoVinculoContrato= false;
    } //-- void deleteCdTipoVinculoContrato() 

    /**
     * Method deleteNrSeqContrato
     * 
     */
    public void deleteNrSeqContrato()
    {
        this._has_nrSeqContrato= false;
    } //-- void deleteNrSeqContrato() 

    /**
     * Method deleteNrSeqContratoVinculo
     * 
     */
    public void deleteNrSeqContratoVinculo()
    {
        this._has_nrSeqContratoVinculo= false;
    } //-- void deleteNrSeqContratoVinculo() 

    /**
     * Returns the value of field 'cdAgenciaAtualizada'.
     * 
     * @return int
     * @return the value of field 'cdAgenciaAtualizada'.
     */
    public int getCdAgenciaAtualizada()
    {
        return this._cdAgenciaAtualizada;
    } //-- int getCdAgenciaAtualizada() 

    /**
     * Returns the value of field 'cdBancoAtualizado'.
     * 
     * @return int
     * @return the value of field 'cdBancoAtualizado'.
     */
    public int getCdBancoAtualizado()
    {
        return this._cdBancoAtualizado;
    } //-- int getCdBancoAtualizado() 

    /**
     * Returns the value of field 'cdContaAtualizada'.
     * 
     * @return long
     * @return the value of field 'cdContaAtualizada'.
     */
    public long getCdContaAtualizada()
    {
        return this._cdContaAtualizada;
    } //-- long getCdContaAtualizada() 

    /**
     * Returns the value of field 'cdDigitoAgenciaAtualizada'.
     * 
     * @return int
     * @return the value of field 'cdDigitoAgenciaAtualizada'.
     */
    public int getCdDigitoAgenciaAtualizada()
    {
        return this._cdDigitoAgenciaAtualizada;
    } //-- int getCdDigitoAgenciaAtualizada() 

    /**
     * Returns the value of field 'cdDigitoContaAtualizada'.
     * 
     * @return String
     * @return the value of field 'cdDigitoContaAtualizada'.
     */
    public java.lang.String getCdDigitoContaAtualizada()
    {
        return this._cdDigitoContaAtualizada;
    } //-- java.lang.String getCdDigitoContaAtualizada() 

    /**
     * Returns the value of field 'cdPessoa'.
     * 
     * @return long
     * @return the value of field 'cdPessoa'.
     */
    public long getCdPessoa()
    {
        return this._cdPessoa;
    } //-- long getCdPessoa() 

    /**
     * Returns the value of field 'cdPessoaJuridicaVinculo'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridicaVinculo'.
     */
    public long getCdPessoaJuridicaVinculo()
    {
        return this._cdPessoaJuridicaVinculo;
    } //-- long getCdPessoaJuridicaVinculo() 

    /**
     * Returns the value of field 'cdTipoContrato'.
     * 
     * @return int
     * @return the value of field 'cdTipoContrato'.
     */
    public int getCdTipoContrato()
    {
        return this._cdTipoContrato;
    } //-- int getCdTipoContrato() 

    /**
     * Returns the value of field 'cdTipoContratoVinculo'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoVinculo'.
     */
    public int getCdTipoContratoVinculo()
    {
        return this._cdTipoContratoVinculo;
    } //-- int getCdTipoContratoVinculo() 

    /**
     * Returns the value of field 'cdTipoVinculoContrato'.
     * 
     * @return int
     * @return the value of field 'cdTipoVinculoContrato'.
     */
    public int getCdTipoVinculoContrato()
    {
        return this._cdTipoVinculoContrato;
    } //-- int getCdTipoVinculoContrato() 

    /**
     * Returns the value of field 'nrSeqContrato'.
     * 
     * @return long
     * @return the value of field 'nrSeqContrato'.
     */
    public long getNrSeqContrato()
    {
        return this._nrSeqContrato;
    } //-- long getNrSeqContrato() 

    /**
     * Returns the value of field 'nrSeqContratoVinculo'.
     * 
     * @return long
     * @return the value of field 'nrSeqContratoVinculo'.
     */
    public long getNrSeqContratoVinculo()
    {
        return this._nrSeqContratoVinculo;
    } //-- long getNrSeqContratoVinculo() 

    /**
     * Method hasCdAgenciaAtualizada
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgenciaAtualizada()
    {
        return this._has_cdAgenciaAtualizada;
    } //-- boolean hasCdAgenciaAtualizada() 

    /**
     * Method hasCdBancoAtualizado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdBancoAtualizado()
    {
        return this._has_cdBancoAtualizado;
    } //-- boolean hasCdBancoAtualizado() 

    /**
     * Method hasCdContaAtualizada
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdContaAtualizada()
    {
        return this._has_cdContaAtualizada;
    } //-- boolean hasCdContaAtualizada() 

    /**
     * Method hasCdDigitoAgenciaAtualizada
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdDigitoAgenciaAtualizada()
    {
        return this._has_cdDigitoAgenciaAtualizada;
    } //-- boolean hasCdDigitoAgenciaAtualizada() 

    /**
     * Method hasCdPessoa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoa()
    {
        return this._has_cdPessoa;
    } //-- boolean hasCdPessoa() 

    /**
     * Method hasCdPessoaJuridicaVinculo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridicaVinculo()
    {
        return this._has_cdPessoaJuridicaVinculo;
    } //-- boolean hasCdPessoaJuridicaVinculo() 

    /**
     * Method hasCdTipoContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContrato()
    {
        return this._has_cdTipoContrato;
    } //-- boolean hasCdTipoContrato() 

    /**
     * Method hasCdTipoContratoVinculo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoVinculo()
    {
        return this._has_cdTipoContratoVinculo;
    } //-- boolean hasCdTipoContratoVinculo() 

    /**
     * Method hasCdTipoVinculoContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoVinculoContrato()
    {
        return this._has_cdTipoVinculoContrato;
    } //-- boolean hasCdTipoVinculoContrato() 

    /**
     * Method hasNrSeqContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSeqContrato()
    {
        return this._has_nrSeqContrato;
    } //-- boolean hasNrSeqContrato() 

    /**
     * Method hasNrSeqContratoVinculo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSeqContratoVinculo()
    {
        return this._has_nrSeqContratoVinculo;
    } //-- boolean hasNrSeqContratoVinculo() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdAgenciaAtualizada'.
     * 
     * @param cdAgenciaAtualizada the value of field
     * 'cdAgenciaAtualizada'.
     */
    public void setCdAgenciaAtualizada(int cdAgenciaAtualizada)
    {
        this._cdAgenciaAtualizada = cdAgenciaAtualizada;
        this._has_cdAgenciaAtualizada = true;
    } //-- void setCdAgenciaAtualizada(int) 

    /**
     * Sets the value of field 'cdBancoAtualizado'.
     * 
     * @param cdBancoAtualizado the value of field
     * 'cdBancoAtualizado'.
     */
    public void setCdBancoAtualizado(int cdBancoAtualizado)
    {
        this._cdBancoAtualizado = cdBancoAtualizado;
        this._has_cdBancoAtualizado = true;
    } //-- void setCdBancoAtualizado(int) 

    /**
     * Sets the value of field 'cdContaAtualizada'.
     * 
     * @param cdContaAtualizada the value of field
     * 'cdContaAtualizada'.
     */
    public void setCdContaAtualizada(long cdContaAtualizada)
    {
        this._cdContaAtualizada = cdContaAtualizada;
        this._has_cdContaAtualizada = true;
    } //-- void setCdContaAtualizada(long) 

    /**
     * Sets the value of field 'cdDigitoAgenciaAtualizada'.
     * 
     * @param cdDigitoAgenciaAtualizada the value of field
     * 'cdDigitoAgenciaAtualizada'.
     */
    public void setCdDigitoAgenciaAtualizada(int cdDigitoAgenciaAtualizada)
    {
        this._cdDigitoAgenciaAtualizada = cdDigitoAgenciaAtualizada;
        this._has_cdDigitoAgenciaAtualizada = true;
    } //-- void setCdDigitoAgenciaAtualizada(int) 

    /**
     * Sets the value of field 'cdDigitoContaAtualizada'.
     * 
     * @param cdDigitoContaAtualizada the value of field
     * 'cdDigitoContaAtualizada'.
     */
    public void setCdDigitoContaAtualizada(java.lang.String cdDigitoContaAtualizada)
    {
        this._cdDigitoContaAtualizada = cdDigitoContaAtualizada;
    } //-- void setCdDigitoContaAtualizada(java.lang.String) 

    /**
     * Sets the value of field 'cdPessoa'.
     * 
     * @param cdPessoa the value of field 'cdPessoa'.
     */
    public void setCdPessoa(long cdPessoa)
    {
        this._cdPessoa = cdPessoa;
        this._has_cdPessoa = true;
    } //-- void setCdPessoa(long) 

    /**
     * Sets the value of field 'cdPessoaJuridicaVinculo'.
     * 
     * @param cdPessoaJuridicaVinculo the value of field
     * 'cdPessoaJuridicaVinculo'.
     */
    public void setCdPessoaJuridicaVinculo(long cdPessoaJuridicaVinculo)
    {
        this._cdPessoaJuridicaVinculo = cdPessoaJuridicaVinculo;
        this._has_cdPessoaJuridicaVinculo = true;
    } //-- void setCdPessoaJuridicaVinculo(long) 

    /**
     * Sets the value of field 'cdTipoContrato'.
     * 
     * @param cdTipoContrato the value of field 'cdTipoContrato'.
     */
    public void setCdTipoContrato(int cdTipoContrato)
    {
        this._cdTipoContrato = cdTipoContrato;
        this._has_cdTipoContrato = true;
    } //-- void setCdTipoContrato(int) 

    /**
     * Sets the value of field 'cdTipoContratoVinculo'.
     * 
     * @param cdTipoContratoVinculo the value of field
     * 'cdTipoContratoVinculo'.
     */
    public void setCdTipoContratoVinculo(int cdTipoContratoVinculo)
    {
        this._cdTipoContratoVinculo = cdTipoContratoVinculo;
        this._has_cdTipoContratoVinculo = true;
    } //-- void setCdTipoContratoVinculo(int) 

    /**
     * Sets the value of field 'cdTipoVinculoContrato'.
     * 
     * @param cdTipoVinculoContrato the value of field
     * 'cdTipoVinculoContrato'.
     */
    public void setCdTipoVinculoContrato(int cdTipoVinculoContrato)
    {
        this._cdTipoVinculoContrato = cdTipoVinculoContrato;
        this._has_cdTipoVinculoContrato = true;
    } //-- void setCdTipoVinculoContrato(int) 

    /**
     * Sets the value of field 'nrSeqContrato'.
     * 
     * @param nrSeqContrato the value of field 'nrSeqContrato'.
     */
    public void setNrSeqContrato(long nrSeqContrato)
    {
        this._nrSeqContrato = nrSeqContrato;
        this._has_nrSeqContrato = true;
    } //-- void setNrSeqContrato(long) 

    /**
     * Sets the value of field 'nrSeqContratoVinculo'.
     * 
     * @param nrSeqContratoVinculo the value of field
     * 'nrSeqContratoVinculo'.
     */
    public void setNrSeqContratoVinculo(long nrSeqContratoVinculo)
    {
        this._nrSeqContratoVinculo = nrSeqContratoVinculo;
        this._has_nrSeqContratoVinculo = true;
    } //-- void setNrSeqContratoVinculo(long) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return SubstituirVinculoCtaSalarioEmpresaRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.substituirvinculoctasalarioempresa.request.SubstituirVinculoCtaSalarioEmpresaRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.substituirvinculoctasalarioempresa.request.SubstituirVinculoCtaSalarioEmpresaRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.substituirvinculoctasalarioempresa.request.SubstituirVinculoCtaSalarioEmpresaRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.substituirvinculoctasalarioempresa.request.SubstituirVinculoCtaSalarioEmpresaRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
