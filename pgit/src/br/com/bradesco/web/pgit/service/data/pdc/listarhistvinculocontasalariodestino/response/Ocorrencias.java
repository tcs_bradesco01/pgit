/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listarhistvinculocontasalariodestino.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _hrInclusaoManutencao
     */
    private java.lang.String _hrInclusaoManutencao;

    /**
     * Field _cdPessoaContratoVinculado
     */
    private long _cdPessoaContratoVinculado = 0;

    /**
     * keeps track of state for field: _cdPessoaContratoVinculado
     */
    private boolean _has_cdPessoaContratoVinculado;

    /**
     * Field _cdTipoVincContrato
     */
    private int _cdTipoVincContrato = 0;

    /**
     * keeps track of state for field: _cdTipoVincContrato
     */
    private boolean _has_cdTipoVincContrato;

    /**
     * Field _nrSequenciaVinculacaoContrato
     */
    private long _nrSequenciaVinculacaoContrato = 0;

    /**
     * keeps track of state for field: _nrSequenciaVinculacaoContrat
     */
    private boolean _has_nrSequenciaVinculacaoContrato;

    /**
     * Field _cdUsuario
     */
    private java.lang.String _cdUsuario;

    /**
     * Field _dtManutencao
     */
    private java.lang.String _dtManutencao;

    /**
     * Field _hrManutencao
     */
    private java.lang.String _hrManutencao;

    /**
     * Field _cdBancoSalario
     */
    private int _cdBancoSalario = 0;

    /**
     * keeps track of state for field: _cdBancoSalario
     */
    private boolean _has_cdBancoSalario;

    /**
     * Field _dsBancoSalario
     */
    private java.lang.String _dsBancoSalario;

    /**
     * Field _cdAgenciaSalario
     */
    private int _cdAgenciaSalario = 0;

    /**
     * keeps track of state for field: _cdAgenciaSalario
     */
    private boolean _has_cdAgenciaSalario;

    /**
     * Field _cdDigitoAgenciaSalario
     */
    private int _cdDigitoAgenciaSalario = 0;

    /**
     * keeps track of state for field: _cdDigitoAgenciaSalario
     */
    private boolean _has_cdDigitoAgenciaSalario;

    /**
     * Field _dsAgenciaSalario
     */
    private java.lang.String _dsAgenciaSalario;

    /**
     * Field _cdContaSalario
     */
    private long _cdContaSalario = 0;

    /**
     * keeps track of state for field: _cdContaSalario
     */
    private boolean _has_cdContaSalario;

    /**
     * Field _cdDigitoContaSalario
     */
    private java.lang.String _cdDigitoContaSalario;

    /**
     * Field _cdBancoDestino
     */
    private int _cdBancoDestino = 0;

    /**
     * keeps track of state for field: _cdBancoDestino
     */
    private boolean _has_cdBancoDestino;

    /**
     * Field _dsBancoDestino
     */
    private java.lang.String _dsBancoDestino;

    /**
     * Field _cdAgenciaDestino
     */
    private int _cdAgenciaDestino = 0;

    /**
     * keeps track of state for field: _cdAgenciaDestino
     */
    private boolean _has_cdAgenciaDestino;

    /**
     * Field _dgAgenciaDestino
     */
    private int _dgAgenciaDestino = 0;

    /**
     * keeps track of state for field: _dgAgenciaDestino
     */
    private boolean _has_dgAgenciaDestino;

    /**
     * Field _dsAgenciaDestino
     */
    private java.lang.String _dsAgenciaDestino;

    /**
     * Field _cdContaDestino
     */
    private long _cdContaDestino = 0;

    /**
     * keeps track of state for field: _cdContaDestino
     */
    private boolean _has_cdContaDestino;

    /**
     * Field _cdDigitoContaDestino
     */
    private java.lang.String _cdDigitoContaDestino;

    /**
     * Field _cdTipoContaDestino
     */
    private java.lang.String _cdTipoContaDestino;

    /**
     * Field _dsSituacaoContaDestino
     */
    private java.lang.String _dsSituacaoContaDestino;

    /**
     * Field _cdContingenciaTed
     */
    private java.lang.String _cdContingenciaTed;

    /**
     * Field _nmParticipante
     */
    private java.lang.String _nmParticipante;

    /**
     * Field _cdCpfParticipante
     */
    private java.lang.String _cdCpfParticipante;

    /**
     * Field _tpManutencao
     */
    private java.lang.String _tpManutencao;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarhistvinculocontasalariodestino.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdAgenciaDestino
     * 
     */
    public void deleteCdAgenciaDestino()
    {
        this._has_cdAgenciaDestino= false;
    } //-- void deleteCdAgenciaDestino() 

    /**
     * Method deleteCdAgenciaSalario
     * 
     */
    public void deleteCdAgenciaSalario()
    {
        this._has_cdAgenciaSalario= false;
    } //-- void deleteCdAgenciaSalario() 

    /**
     * Method deleteCdBancoDestino
     * 
     */
    public void deleteCdBancoDestino()
    {
        this._has_cdBancoDestino= false;
    } //-- void deleteCdBancoDestino() 

    /**
     * Method deleteCdBancoSalario
     * 
     */
    public void deleteCdBancoSalario()
    {
        this._has_cdBancoSalario= false;
    } //-- void deleteCdBancoSalario() 

    /**
     * Method deleteCdContaDestino
     * 
     */
    public void deleteCdContaDestino()
    {
        this._has_cdContaDestino= false;
    } //-- void deleteCdContaDestino() 

    /**
     * Method deleteCdContaSalario
     * 
     */
    public void deleteCdContaSalario()
    {
        this._has_cdContaSalario= false;
    } //-- void deleteCdContaSalario() 

    /**
     * Method deleteCdDigitoAgenciaSalario
     * 
     */
    public void deleteCdDigitoAgenciaSalario()
    {
        this._has_cdDigitoAgenciaSalario= false;
    } //-- void deleteCdDigitoAgenciaSalario() 

    /**
     * Method deleteCdPessoaContratoVinculado
     * 
     */
    public void deleteCdPessoaContratoVinculado()
    {
        this._has_cdPessoaContratoVinculado= false;
    } //-- void deleteCdPessoaContratoVinculado() 

    /**
     * Method deleteCdTipoVincContrato
     * 
     */
    public void deleteCdTipoVincContrato()
    {
        this._has_cdTipoVincContrato= false;
    } //-- void deleteCdTipoVincContrato() 

    /**
     * Method deleteDgAgenciaDestino
     * 
     */
    public void deleteDgAgenciaDestino()
    {
        this._has_dgAgenciaDestino= false;
    } //-- void deleteDgAgenciaDestino() 

    /**
     * Method deleteNrSequenciaVinculacaoContrato
     * 
     */
    public void deleteNrSequenciaVinculacaoContrato()
    {
        this._has_nrSequenciaVinculacaoContrato= false;
    } //-- void deleteNrSequenciaVinculacaoContrato() 

    /**
     * Returns the value of field 'cdAgenciaDestino'.
     * 
     * @return int
     * @return the value of field 'cdAgenciaDestino'.
     */
    public int getCdAgenciaDestino()
    {
        return this._cdAgenciaDestino;
    } //-- int getCdAgenciaDestino() 

    /**
     * Returns the value of field 'cdAgenciaSalario'.
     * 
     * @return int
     * @return the value of field 'cdAgenciaSalario'.
     */
    public int getCdAgenciaSalario()
    {
        return this._cdAgenciaSalario;
    } //-- int getCdAgenciaSalario() 

    /**
     * Returns the value of field 'cdBancoDestino'.
     * 
     * @return int
     * @return the value of field 'cdBancoDestino'.
     */
    public int getCdBancoDestino()
    {
        return this._cdBancoDestino;
    } //-- int getCdBancoDestino() 

    /**
     * Returns the value of field 'cdBancoSalario'.
     * 
     * @return int
     * @return the value of field 'cdBancoSalario'.
     */
    public int getCdBancoSalario()
    {
        return this._cdBancoSalario;
    } //-- int getCdBancoSalario() 

    /**
     * Returns the value of field 'cdContaDestino'.
     * 
     * @return long
     * @return the value of field 'cdContaDestino'.
     */
    public long getCdContaDestino()
    {
        return this._cdContaDestino;
    } //-- long getCdContaDestino() 

    /**
     * Returns the value of field 'cdContaSalario'.
     * 
     * @return long
     * @return the value of field 'cdContaSalario'.
     */
    public long getCdContaSalario()
    {
        return this._cdContaSalario;
    } //-- long getCdContaSalario() 

    /**
     * Returns the value of field 'cdContingenciaTed'.
     * 
     * @return String
     * @return the value of field 'cdContingenciaTed'.
     */
    public java.lang.String getCdContingenciaTed()
    {
        return this._cdContingenciaTed;
    } //-- java.lang.String getCdContingenciaTed() 

    /**
     * Returns the value of field 'cdCpfParticipante'.
     * 
     * @return String
     * @return the value of field 'cdCpfParticipante'.
     */
    public java.lang.String getCdCpfParticipante()
    {
        return this._cdCpfParticipante;
    } //-- java.lang.String getCdCpfParticipante() 

    /**
     * Returns the value of field 'cdDigitoAgenciaSalario'.
     * 
     * @return int
     * @return the value of field 'cdDigitoAgenciaSalario'.
     */
    public int getCdDigitoAgenciaSalario()
    {
        return this._cdDigitoAgenciaSalario;
    } //-- int getCdDigitoAgenciaSalario() 

    /**
     * Returns the value of field 'cdDigitoContaDestino'.
     * 
     * @return String
     * @return the value of field 'cdDigitoContaDestino'.
     */
    public java.lang.String getCdDigitoContaDestino()
    {
        return this._cdDigitoContaDestino;
    } //-- java.lang.String getCdDigitoContaDestino() 

    /**
     * Returns the value of field 'cdDigitoContaSalario'.
     * 
     * @return String
     * @return the value of field 'cdDigitoContaSalario'.
     */
    public java.lang.String getCdDigitoContaSalario()
    {
        return this._cdDigitoContaSalario;
    } //-- java.lang.String getCdDigitoContaSalario() 

    /**
     * Returns the value of field 'cdPessoaContratoVinculado'.
     * 
     * @return long
     * @return the value of field 'cdPessoaContratoVinculado'.
     */
    public long getCdPessoaContratoVinculado()
    {
        return this._cdPessoaContratoVinculado;
    } //-- long getCdPessoaContratoVinculado() 

    /**
     * Returns the value of field 'cdTipoContaDestino'.
     * 
     * @return String
     * @return the value of field 'cdTipoContaDestino'.
     */
    public java.lang.String getCdTipoContaDestino()
    {
        return this._cdTipoContaDestino;
    } //-- java.lang.String getCdTipoContaDestino() 

    /**
     * Returns the value of field 'cdTipoVincContrato'.
     * 
     * @return int
     * @return the value of field 'cdTipoVincContrato'.
     */
    public int getCdTipoVincContrato()
    {
        return this._cdTipoVincContrato;
    } //-- int getCdTipoVincContrato() 

    /**
     * Returns the value of field 'cdUsuario'.
     * 
     * @return String
     * @return the value of field 'cdUsuario'.
     */
    public java.lang.String getCdUsuario()
    {
        return this._cdUsuario;
    } //-- java.lang.String getCdUsuario() 

    /**
     * Returns the value of field 'dgAgenciaDestino'.
     * 
     * @return int
     * @return the value of field 'dgAgenciaDestino'.
     */
    public int getDgAgenciaDestino()
    {
        return this._dgAgenciaDestino;
    } //-- int getDgAgenciaDestino() 

    /**
     * Returns the value of field 'dsAgenciaDestino'.
     * 
     * @return String
     * @return the value of field 'dsAgenciaDestino'.
     */
    public java.lang.String getDsAgenciaDestino()
    {
        return this._dsAgenciaDestino;
    } //-- java.lang.String getDsAgenciaDestino() 

    /**
     * Returns the value of field 'dsAgenciaSalario'.
     * 
     * @return String
     * @return the value of field 'dsAgenciaSalario'.
     */
    public java.lang.String getDsAgenciaSalario()
    {
        return this._dsAgenciaSalario;
    } //-- java.lang.String getDsAgenciaSalario() 

    /**
     * Returns the value of field 'dsBancoDestino'.
     * 
     * @return String
     * @return the value of field 'dsBancoDestino'.
     */
    public java.lang.String getDsBancoDestino()
    {
        return this._dsBancoDestino;
    } //-- java.lang.String getDsBancoDestino() 

    /**
     * Returns the value of field 'dsBancoSalario'.
     * 
     * @return String
     * @return the value of field 'dsBancoSalario'.
     */
    public java.lang.String getDsBancoSalario()
    {
        return this._dsBancoSalario;
    } //-- java.lang.String getDsBancoSalario() 

    /**
     * Returns the value of field 'dsSituacaoContaDestino'.
     * 
     * @return String
     * @return the value of field 'dsSituacaoContaDestino'.
     */
    public java.lang.String getDsSituacaoContaDestino()
    {
        return this._dsSituacaoContaDestino;
    } //-- java.lang.String getDsSituacaoContaDestino() 

    /**
     * Returns the value of field 'dtManutencao'.
     * 
     * @return String
     * @return the value of field 'dtManutencao'.
     */
    public java.lang.String getDtManutencao()
    {
        return this._dtManutencao;
    } //-- java.lang.String getDtManutencao() 

    /**
     * Returns the value of field 'hrInclusaoManutencao'.
     * 
     * @return String
     * @return the value of field 'hrInclusaoManutencao'.
     */
    public java.lang.String getHrInclusaoManutencao()
    {
        return this._hrInclusaoManutencao;
    } //-- java.lang.String getHrInclusaoManutencao() 

    /**
     * Returns the value of field 'hrManutencao'.
     * 
     * @return String
     * @return the value of field 'hrManutencao'.
     */
    public java.lang.String getHrManutencao()
    {
        return this._hrManutencao;
    } //-- java.lang.String getHrManutencao() 

    /**
     * Returns the value of field 'nmParticipante'.
     * 
     * @return String
     * @return the value of field 'nmParticipante'.
     */
    public java.lang.String getNmParticipante()
    {
        return this._nmParticipante;
    } //-- java.lang.String getNmParticipante() 

    /**
     * Returns the value of field 'nrSequenciaVinculacaoContrato'.
     * 
     * @return long
     * @return the value of field 'nrSequenciaVinculacaoContrato'.
     */
    public long getNrSequenciaVinculacaoContrato()
    {
        return this._nrSequenciaVinculacaoContrato;
    } //-- long getNrSequenciaVinculacaoContrato() 

    /**
     * Returns the value of field 'tpManutencao'.
     * 
     * @return String
     * @return the value of field 'tpManutencao'.
     */
    public java.lang.String getTpManutencao()
    {
        return this._tpManutencao;
    } //-- java.lang.String getTpManutencao() 

    /**
     * Method hasCdAgenciaDestino
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgenciaDestino()
    {
        return this._has_cdAgenciaDestino;
    } //-- boolean hasCdAgenciaDestino() 

    /**
     * Method hasCdAgenciaSalario
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgenciaSalario()
    {
        return this._has_cdAgenciaSalario;
    } //-- boolean hasCdAgenciaSalario() 

    /**
     * Method hasCdBancoDestino
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdBancoDestino()
    {
        return this._has_cdBancoDestino;
    } //-- boolean hasCdBancoDestino() 

    /**
     * Method hasCdBancoSalario
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdBancoSalario()
    {
        return this._has_cdBancoSalario;
    } //-- boolean hasCdBancoSalario() 

    /**
     * Method hasCdContaDestino
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdContaDestino()
    {
        return this._has_cdContaDestino;
    } //-- boolean hasCdContaDestino() 

    /**
     * Method hasCdContaSalario
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdContaSalario()
    {
        return this._has_cdContaSalario;
    } //-- boolean hasCdContaSalario() 

    /**
     * Method hasCdDigitoAgenciaSalario
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdDigitoAgenciaSalario()
    {
        return this._has_cdDigitoAgenciaSalario;
    } //-- boolean hasCdDigitoAgenciaSalario() 

    /**
     * Method hasCdPessoaContratoVinculado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaContratoVinculado()
    {
        return this._has_cdPessoaContratoVinculado;
    } //-- boolean hasCdPessoaContratoVinculado() 

    /**
     * Method hasCdTipoVincContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoVincContrato()
    {
        return this._has_cdTipoVincContrato;
    } //-- boolean hasCdTipoVincContrato() 

    /**
     * Method hasDgAgenciaDestino
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasDgAgenciaDestino()
    {
        return this._has_dgAgenciaDestino;
    } //-- boolean hasDgAgenciaDestino() 

    /**
     * Method hasNrSequenciaVinculacaoContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaVinculacaoContrato()
    {
        return this._has_nrSequenciaVinculacaoContrato;
    } //-- boolean hasNrSequenciaVinculacaoContrato() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdAgenciaDestino'.
     * 
     * @param cdAgenciaDestino the value of field 'cdAgenciaDestino'
     */
    public void setCdAgenciaDestino(int cdAgenciaDestino)
    {
        this._cdAgenciaDestino = cdAgenciaDestino;
        this._has_cdAgenciaDestino = true;
    } //-- void setCdAgenciaDestino(int) 

    /**
     * Sets the value of field 'cdAgenciaSalario'.
     * 
     * @param cdAgenciaSalario the value of field 'cdAgenciaSalario'
     */
    public void setCdAgenciaSalario(int cdAgenciaSalario)
    {
        this._cdAgenciaSalario = cdAgenciaSalario;
        this._has_cdAgenciaSalario = true;
    } //-- void setCdAgenciaSalario(int) 

    /**
     * Sets the value of field 'cdBancoDestino'.
     * 
     * @param cdBancoDestino the value of field 'cdBancoDestino'.
     */
    public void setCdBancoDestino(int cdBancoDestino)
    {
        this._cdBancoDestino = cdBancoDestino;
        this._has_cdBancoDestino = true;
    } //-- void setCdBancoDestino(int) 

    /**
     * Sets the value of field 'cdBancoSalario'.
     * 
     * @param cdBancoSalario the value of field 'cdBancoSalario'.
     */
    public void setCdBancoSalario(int cdBancoSalario)
    {
        this._cdBancoSalario = cdBancoSalario;
        this._has_cdBancoSalario = true;
    } //-- void setCdBancoSalario(int) 

    /**
     * Sets the value of field 'cdContaDestino'.
     * 
     * @param cdContaDestino the value of field 'cdContaDestino'.
     */
    public void setCdContaDestino(long cdContaDestino)
    {
        this._cdContaDestino = cdContaDestino;
        this._has_cdContaDestino = true;
    } //-- void setCdContaDestino(long) 

    /**
     * Sets the value of field 'cdContaSalario'.
     * 
     * @param cdContaSalario the value of field 'cdContaSalario'.
     */
    public void setCdContaSalario(long cdContaSalario)
    {
        this._cdContaSalario = cdContaSalario;
        this._has_cdContaSalario = true;
    } //-- void setCdContaSalario(long) 

    /**
     * Sets the value of field 'cdContingenciaTed'.
     * 
     * @param cdContingenciaTed the value of field
     * 'cdContingenciaTed'.
     */
    public void setCdContingenciaTed(java.lang.String cdContingenciaTed)
    {
        this._cdContingenciaTed = cdContingenciaTed;
    } //-- void setCdContingenciaTed(java.lang.String) 

    /**
     * Sets the value of field 'cdCpfParticipante'.
     * 
     * @param cdCpfParticipante the value of field
     * 'cdCpfParticipante'.
     */
    public void setCdCpfParticipante(java.lang.String cdCpfParticipante)
    {
        this._cdCpfParticipante = cdCpfParticipante;
    } //-- void setCdCpfParticipante(java.lang.String) 

    /**
     * Sets the value of field 'cdDigitoAgenciaSalario'.
     * 
     * @param cdDigitoAgenciaSalario the value of field
     * 'cdDigitoAgenciaSalario'.
     */
    public void setCdDigitoAgenciaSalario(int cdDigitoAgenciaSalario)
    {
        this._cdDigitoAgenciaSalario = cdDigitoAgenciaSalario;
        this._has_cdDigitoAgenciaSalario = true;
    } //-- void setCdDigitoAgenciaSalario(int) 

    /**
     * Sets the value of field 'cdDigitoContaDestino'.
     * 
     * @param cdDigitoContaDestino the value of field
     * 'cdDigitoContaDestino'.
     */
    public void setCdDigitoContaDestino(java.lang.String cdDigitoContaDestino)
    {
        this._cdDigitoContaDestino = cdDigitoContaDestino;
    } //-- void setCdDigitoContaDestino(java.lang.String) 

    /**
     * Sets the value of field 'cdDigitoContaSalario'.
     * 
     * @param cdDigitoContaSalario the value of field
     * 'cdDigitoContaSalario'.
     */
    public void setCdDigitoContaSalario(java.lang.String cdDigitoContaSalario)
    {
        this._cdDigitoContaSalario = cdDigitoContaSalario;
    } //-- void setCdDigitoContaSalario(java.lang.String) 

    /**
     * Sets the value of field 'cdPessoaContratoVinculado'.
     * 
     * @param cdPessoaContratoVinculado the value of field
     * 'cdPessoaContratoVinculado'.
     */
    public void setCdPessoaContratoVinculado(long cdPessoaContratoVinculado)
    {
        this._cdPessoaContratoVinculado = cdPessoaContratoVinculado;
        this._has_cdPessoaContratoVinculado = true;
    } //-- void setCdPessoaContratoVinculado(long) 

    /**
     * Sets the value of field 'cdTipoContaDestino'.
     * 
     * @param cdTipoContaDestino the value of field
     * 'cdTipoContaDestino'.
     */
    public void setCdTipoContaDestino(java.lang.String cdTipoContaDestino)
    {
        this._cdTipoContaDestino = cdTipoContaDestino;
    } //-- void setCdTipoContaDestino(java.lang.String) 

    /**
     * Sets the value of field 'cdTipoVincContrato'.
     * 
     * @param cdTipoVincContrato the value of field
     * 'cdTipoVincContrato'.
     */
    public void setCdTipoVincContrato(int cdTipoVincContrato)
    {
        this._cdTipoVincContrato = cdTipoVincContrato;
        this._has_cdTipoVincContrato = true;
    } //-- void setCdTipoVincContrato(int) 

    /**
     * Sets the value of field 'cdUsuario'.
     * 
     * @param cdUsuario the value of field 'cdUsuario'.
     */
    public void setCdUsuario(java.lang.String cdUsuario)
    {
        this._cdUsuario = cdUsuario;
    } //-- void setCdUsuario(java.lang.String) 

    /**
     * Sets the value of field 'dgAgenciaDestino'.
     * 
     * @param dgAgenciaDestino the value of field 'dgAgenciaDestino'
     */
    public void setDgAgenciaDestino(int dgAgenciaDestino)
    {
        this._dgAgenciaDestino = dgAgenciaDestino;
        this._has_dgAgenciaDestino = true;
    } //-- void setDgAgenciaDestino(int) 

    /**
     * Sets the value of field 'dsAgenciaDestino'.
     * 
     * @param dsAgenciaDestino the value of field 'dsAgenciaDestino'
     */
    public void setDsAgenciaDestino(java.lang.String dsAgenciaDestino)
    {
        this._dsAgenciaDestino = dsAgenciaDestino;
    } //-- void setDsAgenciaDestino(java.lang.String) 

    /**
     * Sets the value of field 'dsAgenciaSalario'.
     * 
     * @param dsAgenciaSalario the value of field 'dsAgenciaSalario'
     */
    public void setDsAgenciaSalario(java.lang.String dsAgenciaSalario)
    {
        this._dsAgenciaSalario = dsAgenciaSalario;
    } //-- void setDsAgenciaSalario(java.lang.String) 

    /**
     * Sets the value of field 'dsBancoDestino'.
     * 
     * @param dsBancoDestino the value of field 'dsBancoDestino'.
     */
    public void setDsBancoDestino(java.lang.String dsBancoDestino)
    {
        this._dsBancoDestino = dsBancoDestino;
    } //-- void setDsBancoDestino(java.lang.String) 

    /**
     * Sets the value of field 'dsBancoSalario'.
     * 
     * @param dsBancoSalario the value of field 'dsBancoSalario'.
     */
    public void setDsBancoSalario(java.lang.String dsBancoSalario)
    {
        this._dsBancoSalario = dsBancoSalario;
    } //-- void setDsBancoSalario(java.lang.String) 

    /**
     * Sets the value of field 'dsSituacaoContaDestino'.
     * 
     * @param dsSituacaoContaDestino the value of field
     * 'dsSituacaoContaDestino'.
     */
    public void setDsSituacaoContaDestino(java.lang.String dsSituacaoContaDestino)
    {
        this._dsSituacaoContaDestino = dsSituacaoContaDestino;
    } //-- void setDsSituacaoContaDestino(java.lang.String) 

    /**
     * Sets the value of field 'dtManutencao'.
     * 
     * @param dtManutencao the value of field 'dtManutencao'.
     */
    public void setDtManutencao(java.lang.String dtManutencao)
    {
        this._dtManutencao = dtManutencao;
    } //-- void setDtManutencao(java.lang.String) 

    /**
     * Sets the value of field 'hrInclusaoManutencao'.
     * 
     * @param hrInclusaoManutencao the value of field
     * 'hrInclusaoManutencao'.
     */
    public void setHrInclusaoManutencao(java.lang.String hrInclusaoManutencao)
    {
        this._hrInclusaoManutencao = hrInclusaoManutencao;
    } //-- void setHrInclusaoManutencao(java.lang.String) 

    /**
     * Sets the value of field 'hrManutencao'.
     * 
     * @param hrManutencao the value of field 'hrManutencao'.
     */
    public void setHrManutencao(java.lang.String hrManutencao)
    {
        this._hrManutencao = hrManutencao;
    } //-- void setHrManutencao(java.lang.String) 

    /**
     * Sets the value of field 'nmParticipante'.
     * 
     * @param nmParticipante the value of field 'nmParticipante'.
     */
    public void setNmParticipante(java.lang.String nmParticipante)
    {
        this._nmParticipante = nmParticipante;
    } //-- void setNmParticipante(java.lang.String) 

    /**
     * Sets the value of field 'nrSequenciaVinculacaoContrato'.
     * 
     * @param nrSequenciaVinculacaoContrato the value of field
     * 'nrSequenciaVinculacaoContrato'.
     */
    public void setNrSequenciaVinculacaoContrato(long nrSequenciaVinculacaoContrato)
    {
        this._nrSequenciaVinculacaoContrato = nrSequenciaVinculacaoContrato;
        this._has_nrSequenciaVinculacaoContrato = true;
    } //-- void setNrSequenciaVinculacaoContrato(long) 

    /**
     * Sets the value of field 'tpManutencao'.
     * 
     * @param tpManutencao the value of field 'tpManutencao'.
     */
    public void setTpManutencao(java.lang.String tpManutencao)
    {
        this._tpManutencao = tpManutencao;
    } //-- void setTpManutencao(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listarhistvinculocontasalariodestino.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listarhistvinculocontasalariodestino.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listarhistvinculocontasalariodestino.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarhistvinculocontasalariodestino.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
