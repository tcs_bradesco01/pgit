/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultararquivorecuperacao.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.math.BigDecimal;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdPessoaJuridicaContrato
     */
    private long _cdPessoaJuridicaContrato = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridicaContrato
     */
    private boolean _has_cdPessoaJuridicaContrato;

    /**
     * Field _dsPessoaJuridicaContrato
     */
    private java.lang.String _dsPessoaJuridicaContrato;

    /**
     * Field _cdTipoContratoNegocio
     */
    private int _cdTipoContratoNegocio = 0;

    /**
     * keeps track of state for field: _cdTipoContratoNegocio
     */
    private boolean _has_cdTipoContratoNegocio;

    /**
     * Field _dsTipoContratoNegocio
     */
    private java.lang.String _dsTipoContratoNegocio;

    /**
     * Field _nrSequenciaContratoNegocio
     */
    private long _nrSequenciaContratoNegocio = 0;

    /**
     * keeps track of state for field: _nrSequenciaContratoNegocio
     */
    private boolean _has_nrSequenciaContratoNegocio;

    /**
     * Field _dsContrato
     */
    private java.lang.String _dsContrato;

    /**
     * Field _cdPessoa
     */
    private long _cdPessoa = 0;

    /**
     * keeps track of state for field: _cdPessoa
     */
    private boolean _has_cdPessoa;

    /**
     * Field _cdCnpjCpfPessoa
     */
    private java.lang.String _cdCnpjCpfPessoa;

    /**
     * Field _dsPessoa
     */
    private java.lang.String _dsPessoa;

    /**
     * Field _cdTipoLayoutArquivo
     */
    private int _cdTipoLayoutArquivo = 0;

    /**
     * keeps track of state for field: _cdTipoLayoutArquivo
     */
    private boolean _has_cdTipoLayoutArquivo;

    /**
     * Field _dsTipoLayoutArquivo
     */
    private java.lang.String _dsTipoLayoutArquivo;

    /**
     * Field _cdClienteTransfArquivo
     */
    private long _cdClienteTransfArquivo = 0;

    /**
     * keeps track of state for field: _cdClienteTransfArquivo
     */
    private boolean _has_cdClienteTransfArquivo;

    /**
     * Field _dsArquivoRemessaEnvio
     */
    private java.lang.String _dsArquivoRemessaEnvio;

    /**
     * Field _nrArquivoRemessa
     */
    private long _nrArquivoRemessa = 0;

    /**
     * keeps track of state for field: _nrArquivoRemessa
     */
    private boolean _has_nrArquivoRemessa;

    /**
     * Field _hrInclusaoRegistro
     */
    private java.lang.String _hrInclusaoRegistro;

    /**
     * Field _dsSituacaoProcessamentoRemessa
     */
    private java.lang.String _dsSituacaoProcessamentoRemessa;

    /**
     * Field _dsCondicaoProcessamentoRemessa
     */
    private java.lang.String _dsCondicaoProcessamentoRemessa;

    /**
     * Field _dsMeioTransmissao
     */
    private java.lang.String _dsMeioTransmissao;

    /**
     * Field _dsAmbiente
     */
    private java.lang.String _dsAmbiente;

    /**
     * Field _qtRegistroArqRemessa
     */
    private long _qtRegistroArqRemessa = 0;

    /**
     * keeps track of state for field: _qtRegistroArqRemessa
     */
    private boolean _has_qtRegistroArqRemessa;

    /**
     * Field _vlRegistroArqRemessa
     */
    private java.math.BigDecimal _vlRegistroArqRemessa = new java.math.BigDecimal("0");


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
        setVlRegistroArqRemessa(new java.math.BigDecimal("0"));
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultararquivorecuperacao.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdClienteTransfArquivo
     * 
     */
    public void deleteCdClienteTransfArquivo()
    {
        this._has_cdClienteTransfArquivo= false;
    } //-- void deleteCdClienteTransfArquivo() 

    /**
     * Method deleteCdPessoa
     * 
     */
    public void deleteCdPessoa()
    {
        this._has_cdPessoa= false;
    } //-- void deleteCdPessoa() 

    /**
     * Method deleteCdPessoaJuridicaContrato
     * 
     */
    public void deleteCdPessoaJuridicaContrato()
    {
        this._has_cdPessoaJuridicaContrato= false;
    } //-- void deleteCdPessoaJuridicaContrato() 

    /**
     * Method deleteCdTipoContratoNegocio
     * 
     */
    public void deleteCdTipoContratoNegocio()
    {
        this._has_cdTipoContratoNegocio= false;
    } //-- void deleteCdTipoContratoNegocio() 

    /**
     * Method deleteCdTipoLayoutArquivo
     * 
     */
    public void deleteCdTipoLayoutArquivo()
    {
        this._has_cdTipoLayoutArquivo= false;
    } //-- void deleteCdTipoLayoutArquivo() 

    /**
     * Method deleteNrArquivoRemessa
     * 
     */
    public void deleteNrArquivoRemessa()
    {
        this._has_nrArquivoRemessa= false;
    } //-- void deleteNrArquivoRemessa() 

    /**
     * Method deleteNrSequenciaContratoNegocio
     * 
     */
    public void deleteNrSequenciaContratoNegocio()
    {
        this._has_nrSequenciaContratoNegocio= false;
    } //-- void deleteNrSequenciaContratoNegocio() 

    /**
     * Method deleteQtRegistroArqRemessa
     * 
     */
    public void deleteQtRegistroArqRemessa()
    {
        this._has_qtRegistroArqRemessa= false;
    } //-- void deleteQtRegistroArqRemessa() 

    /**
     * Returns the value of field 'cdClienteTransfArquivo'.
     * 
     * @return long
     * @return the value of field 'cdClienteTransfArquivo'.
     */
    public long getCdClienteTransfArquivo()
    {
        return this._cdClienteTransfArquivo;
    } //-- long getCdClienteTransfArquivo() 

    /**
     * Returns the value of field 'cdCnpjCpfPessoa'.
     * 
     * @return String
     * @return the value of field 'cdCnpjCpfPessoa'.
     */
    public java.lang.String getCdCnpjCpfPessoa()
    {
        return this._cdCnpjCpfPessoa;
    } //-- java.lang.String getCdCnpjCpfPessoa() 

    /**
     * Returns the value of field 'cdPessoa'.
     * 
     * @return long
     * @return the value of field 'cdPessoa'.
     */
    public long getCdPessoa()
    {
        return this._cdPessoa;
    } //-- long getCdPessoa() 

    /**
     * Returns the value of field 'cdPessoaJuridicaContrato'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridicaContrato'.
     */
    public long getCdPessoaJuridicaContrato()
    {
        return this._cdPessoaJuridicaContrato;
    } //-- long getCdPessoaJuridicaContrato() 

    /**
     * Returns the value of field 'cdTipoContratoNegocio'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoNegocio'.
     */
    public int getCdTipoContratoNegocio()
    {
        return this._cdTipoContratoNegocio;
    } //-- int getCdTipoContratoNegocio() 

    /**
     * Returns the value of field 'cdTipoLayoutArquivo'.
     * 
     * @return int
     * @return the value of field 'cdTipoLayoutArquivo'.
     */
    public int getCdTipoLayoutArquivo()
    {
        return this._cdTipoLayoutArquivo;
    } //-- int getCdTipoLayoutArquivo() 

    /**
     * Returns the value of field 'dsAmbiente'.
     * 
     * @return String
     * @return the value of field 'dsAmbiente'.
     */
    public java.lang.String getDsAmbiente()
    {
        return this._dsAmbiente;
    } //-- java.lang.String getDsAmbiente() 

    /**
     * Returns the value of field 'dsArquivoRemessaEnvio'.
     * 
     * @return String
     * @return the value of field 'dsArquivoRemessaEnvio'.
     */
    public java.lang.String getDsArquivoRemessaEnvio()
    {
        return this._dsArquivoRemessaEnvio;
    } //-- java.lang.String getDsArquivoRemessaEnvio() 

    /**
     * Returns the value of field 'dsCondicaoProcessamentoRemessa'.
     * 
     * @return String
     * @return the value of field 'dsCondicaoProcessamentoRemessa'.
     */
    public java.lang.String getDsCondicaoProcessamentoRemessa()
    {
        return this._dsCondicaoProcessamentoRemessa;
    } //-- java.lang.String getDsCondicaoProcessamentoRemessa() 

    /**
     * Returns the value of field 'dsContrato'.
     * 
     * @return String
     * @return the value of field 'dsContrato'.
     */
    public java.lang.String getDsContrato()
    {
        return this._dsContrato;
    } //-- java.lang.String getDsContrato() 

    /**
     * Returns the value of field 'dsMeioTransmissao'.
     * 
     * @return String
     * @return the value of field 'dsMeioTransmissao'.
     */
    public java.lang.String getDsMeioTransmissao()
    {
        return this._dsMeioTransmissao;
    } //-- java.lang.String getDsMeioTransmissao() 

    /**
     * Returns the value of field 'dsPessoa'.
     * 
     * @return String
     * @return the value of field 'dsPessoa'.
     */
    public java.lang.String getDsPessoa()
    {
        return this._dsPessoa;
    } //-- java.lang.String getDsPessoa() 

    /**
     * Returns the value of field 'dsPessoaJuridicaContrato'.
     * 
     * @return String
     * @return the value of field 'dsPessoaJuridicaContrato'.
     */
    public java.lang.String getDsPessoaJuridicaContrato()
    {
        return this._dsPessoaJuridicaContrato;
    } //-- java.lang.String getDsPessoaJuridicaContrato() 

    /**
     * Returns the value of field 'dsSituacaoProcessamentoRemessa'.
     * 
     * @return String
     * @return the value of field 'dsSituacaoProcessamentoRemessa'.
     */
    public java.lang.String getDsSituacaoProcessamentoRemessa()
    {
        return this._dsSituacaoProcessamentoRemessa;
    } //-- java.lang.String getDsSituacaoProcessamentoRemessa() 

    /**
     * Returns the value of field 'dsTipoContratoNegocio'.
     * 
     * @return String
     * @return the value of field 'dsTipoContratoNegocio'.
     */
    public java.lang.String getDsTipoContratoNegocio()
    {
        return this._dsTipoContratoNegocio;
    } //-- java.lang.String getDsTipoContratoNegocio() 

    /**
     * Returns the value of field 'dsTipoLayoutArquivo'.
     * 
     * @return String
     * @return the value of field 'dsTipoLayoutArquivo'.
     */
    public java.lang.String getDsTipoLayoutArquivo()
    {
        return this._dsTipoLayoutArquivo;
    } //-- java.lang.String getDsTipoLayoutArquivo() 

    /**
     * Returns the value of field 'hrInclusaoRegistro'.
     * 
     * @return String
     * @return the value of field 'hrInclusaoRegistro'.
     */
    public java.lang.String getHrInclusaoRegistro()
    {
        return this._hrInclusaoRegistro;
    } //-- java.lang.String getHrInclusaoRegistro() 

    /**
     * Returns the value of field 'nrArquivoRemessa'.
     * 
     * @return long
     * @return the value of field 'nrArquivoRemessa'.
     */
    public long getNrArquivoRemessa()
    {
        return this._nrArquivoRemessa;
    } //-- long getNrArquivoRemessa() 

    /**
     * Returns the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @return long
     * @return the value of field 'nrSequenciaContratoNegocio'.
     */
    public long getNrSequenciaContratoNegocio()
    {
        return this._nrSequenciaContratoNegocio;
    } //-- long getNrSequenciaContratoNegocio() 

    /**
     * Returns the value of field 'qtRegistroArqRemessa'.
     * 
     * @return long
     * @return the value of field 'qtRegistroArqRemessa'.
     */
    public long getQtRegistroArqRemessa()
    {
        return this._qtRegistroArqRemessa;
    } //-- long getQtRegistroArqRemessa() 

    /**
     * Returns the value of field 'vlRegistroArqRemessa'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlRegistroArqRemessa'.
     */
    public java.math.BigDecimal getVlRegistroArqRemessa()
    {
        return this._vlRegistroArqRemessa;
    } //-- java.math.BigDecimal getVlRegistroArqRemessa() 

    /**
     * Method hasCdClienteTransfArquivo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdClienteTransfArquivo()
    {
        return this._has_cdClienteTransfArquivo;
    } //-- boolean hasCdClienteTransfArquivo() 

    /**
     * Method hasCdPessoa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoa()
    {
        return this._has_cdPessoa;
    } //-- boolean hasCdPessoa() 

    /**
     * Method hasCdPessoaJuridicaContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridicaContrato()
    {
        return this._has_cdPessoaJuridicaContrato;
    } //-- boolean hasCdPessoaJuridicaContrato() 

    /**
     * Method hasCdTipoContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoNegocio()
    {
        return this._has_cdTipoContratoNegocio;
    } //-- boolean hasCdTipoContratoNegocio() 

    /**
     * Method hasCdTipoLayoutArquivo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoLayoutArquivo()
    {
        return this._has_cdTipoLayoutArquivo;
    } //-- boolean hasCdTipoLayoutArquivo() 

    /**
     * Method hasNrArquivoRemessa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrArquivoRemessa()
    {
        return this._has_nrArquivoRemessa;
    } //-- boolean hasNrArquivoRemessa() 

    /**
     * Method hasNrSequenciaContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaContratoNegocio()
    {
        return this._has_nrSequenciaContratoNegocio;
    } //-- boolean hasNrSequenciaContratoNegocio() 

    /**
     * Method hasQtRegistroArqRemessa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtRegistroArqRemessa()
    {
        return this._has_qtRegistroArqRemessa;
    } //-- boolean hasQtRegistroArqRemessa() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdClienteTransfArquivo'.
     * 
     * @param cdClienteTransfArquivo the value of field
     * 'cdClienteTransfArquivo'.
     */
    public void setCdClienteTransfArquivo(long cdClienteTransfArquivo)
    {
        this._cdClienteTransfArquivo = cdClienteTransfArquivo;
        this._has_cdClienteTransfArquivo = true;
    } //-- void setCdClienteTransfArquivo(long) 

    /**
     * Sets the value of field 'cdCnpjCpfPessoa'.
     * 
     * @param cdCnpjCpfPessoa the value of field 'cdCnpjCpfPessoa'.
     */
    public void setCdCnpjCpfPessoa(java.lang.String cdCnpjCpfPessoa)
    {
        this._cdCnpjCpfPessoa = cdCnpjCpfPessoa;
    } //-- void setCdCnpjCpfPessoa(java.lang.String) 

    /**
     * Sets the value of field 'cdPessoa'.
     * 
     * @param cdPessoa the value of field 'cdPessoa'.
     */
    public void setCdPessoa(long cdPessoa)
    {
        this._cdPessoa = cdPessoa;
        this._has_cdPessoa = true;
    } //-- void setCdPessoa(long) 

    /**
     * Sets the value of field 'cdPessoaJuridicaContrato'.
     * 
     * @param cdPessoaJuridicaContrato the value of field
     * 'cdPessoaJuridicaContrato'.
     */
    public void setCdPessoaJuridicaContrato(long cdPessoaJuridicaContrato)
    {
        this._cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
        this._has_cdPessoaJuridicaContrato = true;
    } //-- void setCdPessoaJuridicaContrato(long) 

    /**
     * Sets the value of field 'cdTipoContratoNegocio'.
     * 
     * @param cdTipoContratoNegocio the value of field
     * 'cdTipoContratoNegocio'.
     */
    public void setCdTipoContratoNegocio(int cdTipoContratoNegocio)
    {
        this._cdTipoContratoNegocio = cdTipoContratoNegocio;
        this._has_cdTipoContratoNegocio = true;
    } //-- void setCdTipoContratoNegocio(int) 

    /**
     * Sets the value of field 'cdTipoLayoutArquivo'.
     * 
     * @param cdTipoLayoutArquivo the value of field
     * 'cdTipoLayoutArquivo'.
     */
    public void setCdTipoLayoutArquivo(int cdTipoLayoutArquivo)
    {
        this._cdTipoLayoutArquivo = cdTipoLayoutArquivo;
        this._has_cdTipoLayoutArquivo = true;
    } //-- void setCdTipoLayoutArquivo(int) 

    /**
     * Sets the value of field 'dsAmbiente'.
     * 
     * @param dsAmbiente the value of field 'dsAmbiente'.
     */
    public void setDsAmbiente(java.lang.String dsAmbiente)
    {
        this._dsAmbiente = dsAmbiente;
    } //-- void setDsAmbiente(java.lang.String) 

    /**
     * Sets the value of field 'dsArquivoRemessaEnvio'.
     * 
     * @param dsArquivoRemessaEnvio the value of field
     * 'dsArquivoRemessaEnvio'.
     */
    public void setDsArquivoRemessaEnvio(java.lang.String dsArquivoRemessaEnvio)
    {
        this._dsArquivoRemessaEnvio = dsArquivoRemessaEnvio;
    } //-- void setDsArquivoRemessaEnvio(java.lang.String) 

    /**
     * Sets the value of field 'dsCondicaoProcessamentoRemessa'.
     * 
     * @param dsCondicaoProcessamentoRemessa the value of field
     * 'dsCondicaoProcessamentoRemessa'.
     */
    public void setDsCondicaoProcessamentoRemessa(java.lang.String dsCondicaoProcessamentoRemessa)
    {
        this._dsCondicaoProcessamentoRemessa = dsCondicaoProcessamentoRemessa;
    } //-- void setDsCondicaoProcessamentoRemessa(java.lang.String) 

    /**
     * Sets the value of field 'dsContrato'.
     * 
     * @param dsContrato the value of field 'dsContrato'.
     */
    public void setDsContrato(java.lang.String dsContrato)
    {
        this._dsContrato = dsContrato;
    } //-- void setDsContrato(java.lang.String) 

    /**
     * Sets the value of field 'dsMeioTransmissao'.
     * 
     * @param dsMeioTransmissao the value of field
     * 'dsMeioTransmissao'.
     */
    public void setDsMeioTransmissao(java.lang.String dsMeioTransmissao)
    {
        this._dsMeioTransmissao = dsMeioTransmissao;
    } //-- void setDsMeioTransmissao(java.lang.String) 

    /**
     * Sets the value of field 'dsPessoa'.
     * 
     * @param dsPessoa the value of field 'dsPessoa'.
     */
    public void setDsPessoa(java.lang.String dsPessoa)
    {
        this._dsPessoa = dsPessoa;
    } //-- void setDsPessoa(java.lang.String) 

    /**
     * Sets the value of field 'dsPessoaJuridicaContrato'.
     * 
     * @param dsPessoaJuridicaContrato the value of field
     * 'dsPessoaJuridicaContrato'.
     */
    public void setDsPessoaJuridicaContrato(java.lang.String dsPessoaJuridicaContrato)
    {
        this._dsPessoaJuridicaContrato = dsPessoaJuridicaContrato;
    } //-- void setDsPessoaJuridicaContrato(java.lang.String) 

    /**
     * Sets the value of field 'dsSituacaoProcessamentoRemessa'.
     * 
     * @param dsSituacaoProcessamentoRemessa the value of field
     * 'dsSituacaoProcessamentoRemessa'.
     */
    public void setDsSituacaoProcessamentoRemessa(java.lang.String dsSituacaoProcessamentoRemessa)
    {
        this._dsSituacaoProcessamentoRemessa = dsSituacaoProcessamentoRemessa;
    } //-- void setDsSituacaoProcessamentoRemessa(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoContratoNegocio'.
     * 
     * @param dsTipoContratoNegocio the value of field
     * 'dsTipoContratoNegocio'.
     */
    public void setDsTipoContratoNegocio(java.lang.String dsTipoContratoNegocio)
    {
        this._dsTipoContratoNegocio = dsTipoContratoNegocio;
    } //-- void setDsTipoContratoNegocio(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoLayoutArquivo'.
     * 
     * @param dsTipoLayoutArquivo the value of field
     * 'dsTipoLayoutArquivo'.
     */
    public void setDsTipoLayoutArquivo(java.lang.String dsTipoLayoutArquivo)
    {
        this._dsTipoLayoutArquivo = dsTipoLayoutArquivo;
    } //-- void setDsTipoLayoutArquivo(java.lang.String) 

    /**
     * Sets the value of field 'hrInclusaoRegistro'.
     * 
     * @param hrInclusaoRegistro the value of field
     * 'hrInclusaoRegistro'.
     */
    public void setHrInclusaoRegistro(java.lang.String hrInclusaoRegistro)
    {
        this._hrInclusaoRegistro = hrInclusaoRegistro;
    } //-- void setHrInclusaoRegistro(java.lang.String) 

    /**
     * Sets the value of field 'nrArquivoRemessa'.
     * 
     * @param nrArquivoRemessa the value of field 'nrArquivoRemessa'
     */
    public void setNrArquivoRemessa(long nrArquivoRemessa)
    {
        this._nrArquivoRemessa = nrArquivoRemessa;
        this._has_nrArquivoRemessa = true;
    } //-- void setNrArquivoRemessa(long) 

    /**
     * Sets the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @param nrSequenciaContratoNegocio the value of field
     * 'nrSequenciaContratoNegocio'.
     */
    public void setNrSequenciaContratoNegocio(long nrSequenciaContratoNegocio)
    {
        this._nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
        this._has_nrSequenciaContratoNegocio = true;
    } //-- void setNrSequenciaContratoNegocio(long) 

    /**
     * Sets the value of field 'qtRegistroArqRemessa'.
     * 
     * @param qtRegistroArqRemessa the value of field
     * 'qtRegistroArqRemessa'.
     */
    public void setQtRegistroArqRemessa(long qtRegistroArqRemessa)
    {
        this._qtRegistroArqRemessa = qtRegistroArqRemessa;
        this._has_qtRegistroArqRemessa = true;
    } //-- void setQtRegistroArqRemessa(long) 

    /**
     * Sets the value of field 'vlRegistroArqRemessa'.
     * 
     * @param vlRegistroArqRemessa the value of field
     * 'vlRegistroArqRemessa'.
     */
    public void setVlRegistroArqRemessa(java.math.BigDecimal vlRegistroArqRemessa)
    {
        this._vlRegistroArqRemessa = vlRegistroArqRemessa;
    } //-- void setVlRegistroArqRemessa(java.math.BigDecimal) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultararquivorecuperacao.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultararquivorecuperacao.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultararquivorecuperacao.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultararquivorecuperacao.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
