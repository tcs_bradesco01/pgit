/*
 * Nome: br.com.bradesco.web.pgit.service.business.vincmsghistcompopecont.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.vincmsghistcompopecont.bean;

/**
 * Nome: DetalheVinMsgHistComOpSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class DetalheVinMsgHistComOpSaidaDTO {
	
	/** Atributo cdPessoaJuridicaContrato. */
	private Long cdPessoaJuridicaContrato;
	
	/** Atributo cdTipoContratoNegocio. */
	private int cdTipoContratoNegocio;
	
	/** Atributo nrSequenciaContratoNegocio. */
	private Long nrSequenciaContratoNegocio;
	
	/** Atributo cdMensagemLinhaExtrato. */
	private int cdMensagemLinhaExtrato;
	
	/** Atributo cdSistemaLancamentoDebito. */
	private String cdSistemaLancamentoDebito;
	
	/** Atributo dsSistemaLancamentoDebito. */
	private String dsSistemaLancamentoDebito;
	
	/** Atributo nrEventoLancamentoDebito. */
	private int nrEventoLancamentoDebito;
	
	/** Atributo dsEventoLancamentoDebito. */
	private String dsEventoLancamentoDebito;
	
	/** Atributo cdRecursoLancamentoDebito. */
	private Integer cdRecursoLancamentoDebito;
	
	/** Atributo dsRecursoLancamentoDebito. */
	private String dsRecursoLancamentoDebito;
	
	/** Atributo cdIdiomaLancamentoDebito. */
	private int cdIdiomaLancamentoDebito;
	
	/** Atributo dsIdiomaLancamentoDebito. */
	private String dsIdiomaLancamentoDebito;	  
	
	/** Atributo cdSistemaLancamentoCredito. */
	private String cdSistemaLancamentoCredito;
	
	/** Atributo dsSistemaLancamentoCredito. */
	private String dsSistemaLancamentoCredito;
	
	/** Atributo nrEventoLancamentoCredito. */
	private int nrEventoLancamentoCredito;
	
	/** Atributo dsEventoLancamentoCredito. */
	private String dsEventoLancamentoCredito;
	
	/** Atributo cdRecursoLancamentoCredito. */
	private Integer cdRecursoLancamentoCredito;			   
	
	/** Atributo dsRecursoLancamentoCredito. */
	private String dsRecursoLancamentoCredito;		
	
	/** Atributo cdIdiomaLancamentoCredito. */
	private int cdIdiomaLancamentoCredito;	  
	
	/** Atributo dsIdiomaLancamentoCredito. */
	private String dsIdiomaLancamentoCredito;
	
	/** Atributo cdIndicadorRestricaoContrato. */
	private int cdIndicadorRestricaoContrato;
	
	/** Atributo cdTipoMensagemExtrato. */
	private int cdTipoMensagemExtrato;
	
	/** Atributo cdProdutoServicoOperacao. */
	private int cdProdutoServicoOperacao;
	
	/** Atributo dsProdutoServicoOperacao. */
	private String dsProdutoServicoOperacao;
	
	/** Atributo cdProdutoOperacaoRelacionado. */
	private int cdProdutoOperacaoRelacionado;
	
	/** Atributo dsProdutoOperacaoRelacionado. */
	private String dsProdutoOperacaoRelacionado;
	
	/** Atributo cdRelacionadoProduto. */
	private int cdRelacionadoProduto;
	
	/** Atributo cdCanalInclusao. */
	private int cdCanalInclusao;
	
	/** Atributo dsCanalInclusao. */
	private String dsCanalInclusao;
	
	/** Atributo cdAutenticacaoSegurancaInclusao. */
	private String cdAutenticacaoSegurancaInclusao;
	
	/** Atributo nmOperacaoFluxoInclusao. */
	private String nmOperacaoFluxoInclusao;
	
	/** Atributo hrInclusaoRegistro. */
	private String hrInclusaoRegistro;
	
	/** Atributo cdCanalManutencao. */
	private int cdCanalManutencao;
	
	/** Atributo dsCanalManutencao. */
	private String dsCanalManutencao;
	
	/** Atributo cdAutenticacaoSegurancaManutencao. */
	private String cdAutenticacaoSegurancaManutencao;
	
	/** Atributo nmOperacaoFluxoManutencao. */
	private String nmOperacaoFluxoManutencao;
	
	/** Atributo hrManutencaoRegistro. */
	private String hrManutencaoRegistro;
	
	/** Atributo mensagem. */
	private String mensagem;
	
	/** Atributo codMensagem. */
	private String codMensagem;
	
	private String dsMensagemLinhaDebito;
	
	private String dsMensagemLinhaCredito;
	
	/**
	 * Get: cdAutenticacaoSegurancaInclusao.
	 *
	 * @return cdAutenticacaoSegurancaInclusao
	 */
	public String getCdAutenticacaoSegurancaInclusao() {
		return cdAutenticacaoSegurancaInclusao;
	}
	
	/**
	 * Set: cdAutenticacaoSegurancaInclusao.
	 *
	 * @param cdAutenticacaoSegurancaInclusao the cd autenticacao seguranca inclusao
	 */
	public void setCdAutenticacaoSegurancaInclusao(
			String cdAutenticacaoSegurancaInclusao) {
		this.cdAutenticacaoSegurancaInclusao = cdAutenticacaoSegurancaInclusao;
	}
	
	/**
	 * Get: cdAutenticacaoSegurancaManutencao.
	 *
	 * @return cdAutenticacaoSegurancaManutencao
	 */
	public String getCdAutenticacaoSegurancaManutencao() {
		return cdAutenticacaoSegurancaManutencao;
	}
	
	/**
	 * Set: cdAutenticacaoSegurancaManutencao.
	 *
	 * @param cdAutenticacaoSegurancaManutencao the cd autenticacao seguranca manutencao
	 */
	public void setCdAutenticacaoSegurancaManutencao(
			String cdAutenticacaoSegurancaManutencao) {
		this.cdAutenticacaoSegurancaManutencao = cdAutenticacaoSegurancaManutencao;
	}
	
	/**
	 * Get: cdCanalInclusao.
	 *
	 * @return cdCanalInclusao
	 */
	public int getCdCanalInclusao() {
		return cdCanalInclusao;
	}
	
	/**
	 * Set: cdCanalInclusao.
	 *
	 * @param cdCanalInclusao the cd canal inclusao
	 */
	public void setCdCanalInclusao(int cdCanalInclusao) {
		this.cdCanalInclusao = cdCanalInclusao;
	}
	
	/**
	 * Get: cdCanalManutencao.
	 *
	 * @return cdCanalManutencao
	 */
	public int getCdCanalManutencao() {
		return cdCanalManutencao;
	}
	
	/**
	 * Set: cdCanalManutencao.
	 *
	 * @param cdCanalManutencao the cd canal manutencao
	 */
	public void setCdCanalManutencao(int cdCanalManutencao) {
		this.cdCanalManutencao = cdCanalManutencao;
	}
	
	/**
	 * Get: cdIdiomaLancamentoCredito.
	 *
	 * @return cdIdiomaLancamentoCredito
	 */
	public int getCdIdiomaLancamentoCredito() {
		return cdIdiomaLancamentoCredito;
	}
	
	/**
	 * Set: cdIdiomaLancamentoCredito.
	 *
	 * @param cdIdiomaLancamentoCredito the cd idioma lancamento credito
	 */
	public void setCdIdiomaLancamentoCredito(int cdIdiomaLancamentoCredito) {
		this.cdIdiomaLancamentoCredito = cdIdiomaLancamentoCredito;
	}
	
	/**
	 * Get: cdIdiomaLancamentoDebito.
	 *
	 * @return cdIdiomaLancamentoDebito
	 */
	public int getCdIdiomaLancamentoDebito() {
		return cdIdiomaLancamentoDebito;
	}
	
	/**
	 * Set: cdIdiomaLancamentoDebito.
	 *
	 * @param cdIdiomaLancamentoDebito the cd idioma lancamento debito
	 */
	public void setCdIdiomaLancamentoDebito(int cdIdiomaLancamentoDebito) {
		this.cdIdiomaLancamentoDebito = cdIdiomaLancamentoDebito;
	}
	
	/**
	 * Get: cdIndicadorRestricaoContrato.
	 *
	 * @return cdIndicadorRestricaoContrato
	 */
	public int getCdIndicadorRestricaoContrato() {
		return cdIndicadorRestricaoContrato;
	}
	
	/**
	 * Set: cdIndicadorRestricaoContrato.
	 *
	 * @param cdIndicadorRestricaoContrato the cd indicador restricao contrato
	 */
	public void setCdIndicadorRestricaoContrato(int cdIndicadorRestricaoContrato) {
		this.cdIndicadorRestricaoContrato = cdIndicadorRestricaoContrato;
	}
	
	/**
	 * Get: cdMensagemLinhaExtrato.
	 *
	 * @return cdMensagemLinhaExtrato
	 */
	public int getCdMensagemLinhaExtrato() {
		return cdMensagemLinhaExtrato;
	}
	
	/**
	 * Set: cdMensagemLinhaExtrato.
	 *
	 * @param cdMensagemLinhaExtrato the cd mensagem linha extrato
	 */
	public void setCdMensagemLinhaExtrato(int cdMensagemLinhaExtrato) {
		this.cdMensagemLinhaExtrato = cdMensagemLinhaExtrato;
	}
	
	/**
	 * Get: cdPessoaJuridicaContrato.
	 *
	 * @return cdPessoaJuridicaContrato
	 */
	public Long getCdPessoaJuridicaContrato() {
		return cdPessoaJuridicaContrato;
	}
	
	/**
	 * Set: cdPessoaJuridicaContrato.
	 *
	 * @param cdPessoaJuridicaContrato the cd pessoa juridica contrato
	 */
	public void setCdPessoaJuridicaContrato(Long cdPessoaJuridicaContrato) {
		this.cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
	}
	
	/**
	 * Get: cdProdutoOperacaoRelacionado.
	 *
	 * @return cdProdutoOperacaoRelacionado
	 */
	public int getCdProdutoOperacaoRelacionado() {
		return cdProdutoOperacaoRelacionado;
	}
	
	/**
	 * Set: cdProdutoOperacaoRelacionado.
	 *
	 * @param cdProdutoOperacaoRelacionado the cd produto operacao relacionado
	 */
	public void setCdProdutoOperacaoRelacionado(int cdProdutoOperacaoRelacionado) {
		this.cdProdutoOperacaoRelacionado = cdProdutoOperacaoRelacionado;
	}
	
	/**
	 * Get: cdProdutoServicoOperacao.
	 *
	 * @return cdProdutoServicoOperacao
	 */
	public int getCdProdutoServicoOperacao() {
		return cdProdutoServicoOperacao;
	}
	
	/**
	 * Set: cdProdutoServicoOperacao.
	 *
	 * @param cdProdutoServicoOperacao the cd produto servico operacao
	 */
	public void setCdProdutoServicoOperacao(int cdProdutoServicoOperacao) {
		this.cdProdutoServicoOperacao = cdProdutoServicoOperacao;
	}
	
	/**
	 * Get: cdRecursoLancamentoCredito.
	 *
	 * @return cdRecursoLancamentoCredito
	 */
	public int getCdRecursoLancamentoCredito() {
		return cdRecursoLancamentoCredito;
	}
	
	/**
	 * Set: cdRecursoLancamentoCredito.
	 *
	 * @param cdRecursoLancamentoCredito the cd recurso lancamento credito
	 */
	public void setCdRecursoLancamentoCredito(Integer cdRecursoLancamentoCredito) {
		this.cdRecursoLancamentoCredito = cdRecursoLancamentoCredito;
	}
	
	/**
	 * Get: cdRecursoLancamentoDebito.
	 *
	 * @return cdRecursoLancamentoDebito
	 */
	public int getCdRecursoLancamentoDebito() {
		return cdRecursoLancamentoDebito;
	}
	
	/**
	 * Set: cdRecursoLancamentoDebito.
	 *
	 * @param cdRecursoLancamentoDebito the cd recurso lancamento debito
	 */
	public void setCdRecursoLancamentoDebito(Integer cdRecursoLancamentoDebito) {
		this.cdRecursoLancamentoDebito = cdRecursoLancamentoDebito;
	}
	
	/**
	 * Get: cdRelacionadoProduto.
	 *
	 * @return cdRelacionadoProduto
	 */
	public int getCdRelacionadoProduto() {
		return cdRelacionadoProduto;
	}
	
	/**
	 * Set: cdRelacionadoProduto.
	 *
	 * @param cdRelacionadoProduto the cd relacionado produto
	 */
	public void setCdRelacionadoProduto(int cdRelacionadoProduto) {
		this.cdRelacionadoProduto = cdRelacionadoProduto;
	}
	
	/**
	 * Get: cdSistemaLancamentoCredito.
	 *
	 * @return cdSistemaLancamentoCredito
	 */
	public String getCdSistemaLancamentoCredito() {
		return cdSistemaLancamentoCredito;
	}
	
	/**
	 * Set: cdSistemaLancamentoCredito.
	 *
	 * @param cdSistemaLancamentoCredito the cd sistema lancamento credito
	 */
	public void setCdSistemaLancamentoCredito(String cdSistemaLancamentoCredito) {
		this.cdSistemaLancamentoCredito = cdSistemaLancamentoCredito;
	}
	
	/**
	 * Get: cdSistemaLancamentoDebito.
	 *
	 * @return cdSistemaLancamentoDebito
	 */
	public String getCdSistemaLancamentoDebito() {
		return cdSistemaLancamentoDebito;
	}
	
	/**
	 * Set: cdSistemaLancamentoDebito.
	 *
	 * @param cdSistemaLancamentoDebito the cd sistema lancamento debito
	 */
	public void setCdSistemaLancamentoDebito(String cdSistemaLancamentoDebito) {
		this.cdSistemaLancamentoDebito = cdSistemaLancamentoDebito;
	}
	
	/**
	 * Get: cdTipoContratoNegocio.
	 *
	 * @return cdTipoContratoNegocio
	 */
	public int getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}
	
	/**
	 * Set: cdTipoContratoNegocio.
	 *
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(int cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}
	
	/**
	 * Get: cdTipoMensagemExtrato.
	 *
	 * @return cdTipoMensagemExtrato
	 */
	public int getCdTipoMensagemExtrato() {
		return cdTipoMensagemExtrato;
	}
	
	/**
	 * Set: cdTipoMensagemExtrato.
	 *
	 * @param cdTipoMensagemExtrato the cd tipo mensagem extrato
	 */
	public void setCdTipoMensagemExtrato(int cdTipoMensagemExtrato) {
		this.cdTipoMensagemExtrato = cdTipoMensagemExtrato;
	}
	
	/**
	 * Get: dsCanalInclusao.
	 *
	 * @return dsCanalInclusao
	 */
	public String getDsCanalInclusao() {
		return dsCanalInclusao;
	}
	
	/**
	 * Set: dsCanalInclusao.
	 *
	 * @param dsCanalInclusao the ds canal inclusao
	 */
	public void setDsCanalInclusao(String dsCanalInclusao) {
		this.dsCanalInclusao = dsCanalInclusao;
	}
	
	/**
	 * Get: dsCanalManutencao.
	 *
	 * @return dsCanalManutencao
	 */
	public String getDsCanalManutencao() {
		return dsCanalManutencao;
	}
	
	/**
	 * Set: dsCanalManutencao.
	 *
	 * @param dsCanalManutencao the ds canal manutencao
	 */
	public void setDsCanalManutencao(String dsCanalManutencao) {
		this.dsCanalManutencao = dsCanalManutencao;
	}
	
	/**
	 * Get: dsEventoLancamentoCredito.
	 *
	 * @return dsEventoLancamentoCredito
	 */
	public String getDsEventoLancamentoCredito() {
		return dsEventoLancamentoCredito;
	}
	
	/**
	 * Set: dsEventoLancamentoCredito.
	 *
	 * @param dsEventoLancamentoCredito the ds evento lancamento credito
	 */
	public void setDsEventoLancamentoCredito(String dsEventoLancamentoCredito) {
		this.dsEventoLancamentoCredito = dsEventoLancamentoCredito;
	}
	
	/**
	 * Get: dsEventoLancamentoDebito.
	 *
	 * @return dsEventoLancamentoDebito
	 */
	public String getDsEventoLancamentoDebito() {
		return dsEventoLancamentoDebito;
	}
	
	/**
	 * Set: dsEventoLancamentoDebito.
	 *
	 * @param dsEventoLancamentoDebito the ds evento lancamento debito
	 */
	public void setDsEventoLancamentoDebito(String dsEventoLancamentoDebito) {
		this.dsEventoLancamentoDebito = dsEventoLancamentoDebito;
	}
	
	/**
	 * Get: dsIdiomaLancamentoCredito.
	 *
	 * @return dsIdiomaLancamentoCredito
	 */
	public String getDsIdiomaLancamentoCredito() {
		return dsIdiomaLancamentoCredito;
	}
	
	/**
	 * Set: dsIdiomaLancamentoCredito.
	 *
	 * @param dsIdiomaLancamentoCredito the ds idioma lancamento credito
	 */
	public void setDsIdiomaLancamentoCredito(String dsIdiomaLancamentoCredito) {
		this.dsIdiomaLancamentoCredito = dsIdiomaLancamentoCredito;
	}
	
	/**
	 * Get: dsIdiomaLancamentoDebito.
	 *
	 * @return dsIdiomaLancamentoDebito
	 */
	public String getDsIdiomaLancamentoDebito() {
		return dsIdiomaLancamentoDebito;
	}
	
	/**
	 * Set: dsIdiomaLancamentoDebito.
	 *
	 * @param dsIdiomaLancamentoDebito the ds idioma lancamento debito
	 */
	public void setDsIdiomaLancamentoDebito(String dsIdiomaLancamentoDebito) {
		this.dsIdiomaLancamentoDebito = dsIdiomaLancamentoDebito;
	}
	
	/**
	 * Get: dsProdutoOperacaoRelacionado.
	 *
	 * @return dsProdutoOperacaoRelacionado
	 */
	public String getDsProdutoOperacaoRelacionado() {
		return dsProdutoOperacaoRelacionado;
	}
	
	/**
	 * Set: dsProdutoOperacaoRelacionado.
	 *
	 * @param dsProdutoOperacaoRelacionado the ds produto operacao relacionado
	 */
	public void setDsProdutoOperacaoRelacionado(String dsProdutoOperacaoRelacionado) {
		this.dsProdutoOperacaoRelacionado = dsProdutoOperacaoRelacionado;
	}
	
	/**
	 * Get: dsProdutoServicoOperacao.
	 *
	 * @return dsProdutoServicoOperacao
	 */
	public String getDsProdutoServicoOperacao() {
		return dsProdutoServicoOperacao;
	}
	
	/**
	 * Set: dsProdutoServicoOperacao.
	 *
	 * @param dsProdutoServicoOperacao the ds produto servico operacao
	 */
	public void setDsProdutoServicoOperacao(String dsProdutoServicoOperacao) {
		this.dsProdutoServicoOperacao = dsProdutoServicoOperacao;
	}
	
	/**
	 * Get: dsRecursoLancamentoCredito.
	 *
	 * @return dsRecursoLancamentoCredito
	 */
	public String getDsRecursoLancamentoCredito() {
		return dsRecursoLancamentoCredito;
	}
	
	/**
	 * Set: dsRecursoLancamentoCredito.
	 *
	 * @param dsRecursoLancamentoCredito the ds recurso lancamento credito
	 */
	public void setDsRecursoLancamentoCredito(String dsRecursoLancamentoCredito) {
		this.dsRecursoLancamentoCredito = dsRecursoLancamentoCredito;
	}
	
	/**
	 * Get: dsRecursoLancamentoDebito.
	 *
	 * @return dsRecursoLancamentoDebito
	 */
	public String getDsRecursoLancamentoDebito() {
		return dsRecursoLancamentoDebito;
	}
	
	/**
	 * Set: dsRecursoLancamentoDebito.
	 *
	 * @param dsRecursoLancamentoDebito the ds recurso lancamento debito
	 */
	public void setDsRecursoLancamentoDebito(String dsRecursoLancamentoDebito) {
		this.dsRecursoLancamentoDebito = dsRecursoLancamentoDebito;
	}
	
	/**
	 * Get: dsSistemaLancamentoCredito.
	 *
	 * @return dsSistemaLancamentoCredito
	 */
	public String getDsSistemaLancamentoCredito() {
		return dsSistemaLancamentoCredito;
	}
	
	/**
	 * Set: dsSistemaLancamentoCredito.
	 *
	 * @param dsSistemaLancamentoCredito the ds sistema lancamento credito
	 */
	public void setDsSistemaLancamentoCredito(String dsSistemaLancamentoCredito) {
		this.dsSistemaLancamentoCredito = dsSistemaLancamentoCredito;
	}
	
	/**
	 * Get: dsSistemaLancamentoDebito.
	 *
	 * @return dsSistemaLancamentoDebito
	 */
	public String getDsSistemaLancamentoDebito() {
		return dsSistemaLancamentoDebito;
	}
	
	/**
	 * Set: dsSistemaLancamentoDebito.
	 *
	 * @param dsSistemaLancamentoDebito the ds sistema lancamento debito
	 */
	public void setDsSistemaLancamentoDebito(String dsSistemaLancamentoDebito) {
		this.dsSistemaLancamentoDebito = dsSistemaLancamentoDebito;
	}
	
	/**
	 * Get: hrInclusaoRegistro.
	 *
	 * @return hrInclusaoRegistro
	 */
	public String getHrInclusaoRegistro() {
		return hrInclusaoRegistro;
	}
	
	/**
	 * Set: hrInclusaoRegistro.
	 *
	 * @param hrInclusaoRegistro the hr inclusao registro
	 */
	public void setHrInclusaoRegistro(String hrInclusaoRegistro) {
		this.hrInclusaoRegistro = hrInclusaoRegistro;
	}
	
	/**
	 * Get: hrManutencaoRegistro.
	 *
	 * @return hrManutencaoRegistro
	 */
	public String getHrManutencaoRegistro() {
		return hrManutencaoRegistro;
	}
	
	/**
	 * Set: hrManutencaoRegistro.
	 *
	 * @param hrManutencaoRegistro the hr manutencao registro
	 */
	public void setHrManutencaoRegistro(String hrManutencaoRegistro) {
		this.hrManutencaoRegistro = hrManutencaoRegistro;
	}
	
	/**
	 * Get: nmOperacaoFluxoInclusao.
	 *
	 * @return nmOperacaoFluxoInclusao
	 */
	public String getNmOperacaoFluxoInclusao() {
		return nmOperacaoFluxoInclusao;
	}
	
	/**
	 * Set: nmOperacaoFluxoInclusao.
	 *
	 * @param nmOperacaoFluxoInclusao the nm operacao fluxo inclusao
	 */
	public void setNmOperacaoFluxoInclusao(String nmOperacaoFluxoInclusao) {
		this.nmOperacaoFluxoInclusao = nmOperacaoFluxoInclusao;
	}
	
	/**
	 * Get: nmOperacaoFluxoManutencao.
	 *
	 * @return nmOperacaoFluxoManutencao
	 */
	public String getNmOperacaoFluxoManutencao() {
		return nmOperacaoFluxoManutencao;
	}
	
	/**
	 * Set: nmOperacaoFluxoManutencao.
	 *
	 * @param nmOperacaoFluxoManutencao the nm operacao fluxo manutencao
	 */
	public void setNmOperacaoFluxoManutencao(String nmOperacaoFluxoManutencao) {
		this.nmOperacaoFluxoManutencao = nmOperacaoFluxoManutencao;
	}
	
	/**
	 * Get: nrEventoLancamentoCredito.
	 *
	 * @return nrEventoLancamentoCredito
	 */
	public int getNrEventoLancamentoCredito() {
		return nrEventoLancamentoCredito;
	}
	
	/**
	 * Set: nrEventoLancamentoCredito.
	 *
	 * @param nrEventoLancamentoCredito the nr evento lancamento credito
	 */
	public void setNrEventoLancamentoCredito(int nrEventoLancamentoCredito) {
		this.nrEventoLancamentoCredito = nrEventoLancamentoCredito;
	}
	
	/**
	 * Get: nrEventoLancamentoDebito.
	 *
	 * @return nrEventoLancamentoDebito
	 */
	public int getNrEventoLancamentoDebito() {
		return nrEventoLancamentoDebito;
	}
	
	/**
	 * Set: nrEventoLancamentoDebito.
	 *
	 * @param nrEventoLancamentoDebito the nr evento lancamento debito
	 */
	public void setNrEventoLancamentoDebito(int nrEventoLancamentoDebito) {
		this.nrEventoLancamentoDebito = nrEventoLancamentoDebito;
	}
	
	/**
	 * Get: nrSequenciaContratoNegocio.
	 *
	 * @return nrSequenciaContratoNegocio
	 */
	public Long getNrSequenciaContratoNegocio() {
		return nrSequenciaContratoNegocio;
	}
	
	/**
	 * Set: nrSequenciaContratoNegocio.
	 *
	 * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
	 */
	public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}
	
	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}
	
	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	/**
	 * Get: cdRecursoLancamentoCreditoStr.
	 *
	 * @return cdRecursoLancamentoCreditoStr
	 */
	public String getCdRecursoLancamentoCreditoStr(){
		return this.cdRecursoLancamentoCredito == null || this.cdRecursoLancamentoCredito.equals(0) ? "" : cdRecursoLancamentoCredito.toString();
	}
	
	/**
	 * Get: cdRecursoLancamentoDebitoStr.
	 *
	 * @return cdRecursoLancamentoDebitoStr
	 */
	public String getCdRecursoLancamentoDebitoStr(){
		return this.cdRecursoLancamentoDebito == null || this.cdRecursoLancamentoDebito.equals(0) ? "" : cdRecursoLancamentoDebito.toString();
	}

	/**
	 * Get: dsMensagemLinhaDebito
	 * @return the dsMensagemLinhaDebito
	 */
	public String getDsMensagemLinhaDebito() {
		return dsMensagemLinhaDebito;
	}

	/**
	 * Set: dsMensagemLinhaDebito
	 * @param dsMensagemLinhaDebito the dsMensagemLinhaDebito to set
	 */
	public void setDsMensagemLinhaDebito(String dsMensagemLinhaDebito) {
		this.dsMensagemLinhaDebito = dsMensagemLinhaDebito;
	}

	/**
	 * Get: dsMensagemLinhaCredito
	 * @return the dsMensagemLinhaCredito
	 */
	public String getDsMensagemLinhaCredito() {
		return dsMensagemLinhaCredito;
	}

	/**
	 * Set: dsMensagemLinhaCredito
	 * @param dsMensagemLinhaCredito the dsMensagemLinhaCredito to set
	 */
	public void setDsMensagemLinhaCredito(String dsMensagemLinhaCredito) {
		this.dsMensagemLinhaCredito = dsMensagemLinhaCredito;
	}
	
}
