/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantercontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantercontrato.bean;

/**
 * Nome: ExcluirParticipanteContratoPgitEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ExcluirParticipanteContratoPgitEntradaDTO {


	/** Atributo codEmpresaContrato. */
	private long codEmpresaContrato;
	
	/** Atributo tipoContrato. */
	private int tipoContrato;
	
	/** Atributo numSequenciaContrato. */
	private long numSequenciaContrato;
	
	/** Atributo codTipoParticipacao. */
	private int codTipoParticipacao;
	
	/** Atributo codParticipante. */
	private long codParticipante;
	
	/** Atributo cpfCnpjParticipante. */
	private long cpfCnpjParticipante;
	
	/** Atributo codFilialCnpj. */
	private int codFilialCnpj;
	
	/** Atributo codControleCpfCnpj. */
	private int codControleCpfCnpj;
	
	/**
	 * Get: codControleCpfCnpj.
	 *
	 * @return codControleCpfCnpj
	 */
	public int getCodControleCpfCnpj() {
		return codControleCpfCnpj;
	}
	
	/**
	 * Set: codControleCpfCnpj.
	 *
	 * @param codControleCpfCnpj the cod controle cpf cnpj
	 */
	public void setCodControleCpfCnpj(int codControleCpfCnpj) {
		this.codControleCpfCnpj = codControleCpfCnpj;
	}
	
	/**
	 * Get: codEmpresaContrato.
	 *
	 * @return codEmpresaContrato
	 */
	public long getCodEmpresaContrato() {
		return codEmpresaContrato;
	}
	
	/**
	 * Set: codEmpresaContrato.
	 *
	 * @param codEmpresaContrato the cod empresa contrato
	 */
	public void setCodEmpresaContrato(long codEmpresaContrato) {
		this.codEmpresaContrato = codEmpresaContrato;
	}
	
	/**
	 * Get: codFilialCnpj.
	 *
	 * @return codFilialCnpj
	 */
	public int getCodFilialCnpj() {
		return codFilialCnpj;
	}
	
	/**
	 * Set: codFilialCnpj.
	 *
	 * @param codFilialCnpj the cod filial cnpj
	 */
	public void setCodFilialCnpj(int codFilialCnpj) {
		this.codFilialCnpj = codFilialCnpj;
	}

	/**
	 * Get: codParticipante.
	 *
	 * @return codParticipante
	 */
	public long getCodParticipante() {
		return codParticipante;
	}
	
	/**
	 * Set: codParticipante.
	 *
	 * @param codParticipante the cod participante
	 */
	public void setCodParticipante(long codParticipante) {
		this.codParticipante = codParticipante;
	}
	
	/**
	 * Get: codTipoParticipacao.
	 *
	 * @return codTipoParticipacao
	 */
	public int getCodTipoParticipacao() {
		return codTipoParticipacao;
	}
	
	/**
	 * Set: codTipoParticipacao.
	 *
	 * @param codTipoParticipacao the cod tipo participacao
	 */
	public void setCodTipoParticipacao(int codTipoParticipacao) {
		this.codTipoParticipacao = codTipoParticipacao;
	}
	
	/**
	 * Get: cpfCnpjParticipante.
	 *
	 * @return cpfCnpjParticipante
	 */
	public long getCpfCnpjParticipante() {
		return cpfCnpjParticipante;
	}
	
	/**
	 * Set: cpfCnpjParticipante.
	 *
	 * @param cpfCnpjParticipante the cpf cnpj participante
	 */
	public void setCpfCnpjParticipante(long cpfCnpjParticipante) {
		this.cpfCnpjParticipante = cpfCnpjParticipante;
	}
	
	/**
	 * Get: numSequenciaContrato.
	 *
	 * @return numSequenciaContrato
	 */
	public long getNumSequenciaContrato() {
		return numSequenciaContrato;
	}
	
	/**
	 * Set: numSequenciaContrato.
	 *
	 * @param numSequenciaContrato the num sequencia contrato
	 */
	public void setNumSequenciaContrato(long numSequenciaContrato) {
		this.numSequenciaContrato = numSequenciaContrato;
	}

	/**
	 * Get: tipoContrato.
	 *
	 * @return tipoContrato
	 */
	public int getTipoContrato() {
		return tipoContrato;
	}
	
	/**
	 * Set: tipoContrato.
	 *
	 * @param tipoContrato the tipo contrato
	 */
	public void setTipoContrato(int tipoContrato) {
		this.tipoContrato = tipoContrato;
	}
	
}
