/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/interfaz.ftl,v $
 * $Id: interfaz.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: interfaz.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.anteciparpostergarpagtosagecon;

import java.util.List;

import br.com.bradesco.web.pgit.service.business.anteciparpostergarpagtosagecon.bean.AntPostergarPagtosIntegralEntradaDTO;
import br.com.bradesco.web.pgit.service.business.anteciparpostergarpagtosagecon.bean.AntPostergarPagtosIntegralSaidaDTO;
import br.com.bradesco.web.pgit.service.business.anteciparpostergarpagtosagecon.bean.AntPostergarPagtosParcialEntradaDTO;
import br.com.bradesco.web.pgit.service.business.anteciparpostergarpagtosagecon.bean.AntPostergarPagtosParcialSaidaDTO;
import br.com.bradesco.web.pgit.service.business.anteciparpostergarpagtosagecon.bean.ConsultaPostergarConsolidadoSolicitacaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.anteciparpostergarpagtosagecon.bean.ConsultaPostergarConsolidadoSolicitacaoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.anteciparpostergarpagtosagecon.bean.ConsultarPagtosConsAntPostergacaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.anteciparpostergarpagtosagecon.bean.ConsultarPagtosConsAntPostergacaoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.anteciparpostergarpagtosagecon.bean.DetalharAntPostergarPagtosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.anteciparpostergarpagtosagecon.bean.DetalharAntPostergarPagtosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.anteciparpostergarpagtosagecon.bean.DetalhePostergarConsolidadoSolicitacaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.anteciparpostergarpagtosagecon.bean.DetalhePostergarConsolidadoSolicitacaoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.anteciparpostergarpagtosagecon.bean.ExcluirPostergarConsolidadoSolicitacaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.anteciparpostergarpagtosagecon.bean.ExcluirPostergarConsolidadoSolicitacaoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.anteciparpostergarpagtosagecon.bean.ListarPostergarConsolidadoSolicitacaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.anteciparpostergarpagtosagecon.bean.ListarPostergarConsolidadoSolicitacaoSaidaDTO;


// TODO: Auto-generated Javadoc
/**
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Interface do adaptador: AnteciparPostergarPagtosAgeCon
 * </p>.
 *
 * @comment CODIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public interface IAnteciparPostergarPagtosAgeConService {
    
    /**
     * Consultar pagtos cons ant postergacao.
     *
     * @param entrada the entrada
     * @return the list< consultar pagtos cons ant postergacao saida dt o>
     */
    List<ConsultarPagtosConsAntPostergacaoSaidaDTO> consultarPagtosConsAntPostergacao(ConsultarPagtosConsAntPostergacaoEntradaDTO entrada);
    
    /**
     * Detalhar ant postergar pagtos.
     *
     * @param entradaDTO the entrada dto
     * @return the detalhar ant postergar pagtos saida dto
     */
    DetalharAntPostergarPagtosSaidaDTO detalharAntPostergarPagtos(DetalharAntPostergarPagtosEntradaDTO entradaDTO);
    
    /**
     * Ant postergar pagtos integral.
     *
     * @param entradaDTO the entrada dto
     * @return the ant postergar pagtos integral saida dto
     */
    AntPostergarPagtosIntegralSaidaDTO antPostergarPagtosIntegral(AntPostergarPagtosIntegralEntradaDTO entradaDTO);
    
    /**
     * Ant postergar pagtos parcial.
     *
     * @param entradaDTO the entrada dto
     * @return the ant postergar pagtos parcial saida dto
     */
    AntPostergarPagtosParcialSaidaDTO antPostergarPagtosParcial(AntPostergarPagtosParcialEntradaDTO entradaDTO);
    
        
    /**
     * Detalhe postergar consolidado solicitacao.
     *
     * @param entradaDTO the entrada dto
     * @return the detalhe postergar consolidado solicitacao saida dto
     */
    DetalhePostergarConsolidadoSolicitacaoSaidaDTO detalhePostergarConsolidadoSolicitacao(DetalhePostergarConsolidadoSolicitacaoEntradaDTO entradaDTO);
    
    /**
     * Consulta postergar consolidado solicitacao.
     *
     * @param entradaDTO the entrada dto
     * @return the consulta postergar consolidado solicitacao saida dto
     */
    ConsultaPostergarConsolidadoSolicitacaoSaidaDTO consultaPostergarConsolidadoSolicitacao(ConsultaPostergarConsolidadoSolicitacaoEntradaDTO entradaDTO);
    
    /**
     * Listar postergar consolidado solicitacao.
     *
     * @param entradaDTO the entrada dto
     * @return the listar postergar consolidado solicitacao saida dto
     */
    ListarPostergarConsolidadoSolicitacaoSaidaDTO listarPostergarConsolidadoSolicitacao(ListarPostergarConsolidadoSolicitacaoEntradaDTO entradaDTO);
    
    /**
     * Excluir postergar consolidado solicitacao.
     *
     * @param entradaDTO the entrada dto
     * @return the excluir postergar consolidado solicitacao saida dto
     */
    ExcluirPostergarConsolidadoSolicitacaoSaidaDTO excluirPostergarConsolidadoSolicitacao(ExcluirPostergarConsolidadoSolicitacaoEntradaDTO entradaDTO);
}

