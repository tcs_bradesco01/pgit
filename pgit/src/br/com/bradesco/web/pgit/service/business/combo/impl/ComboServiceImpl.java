/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/implementation.ftl,v $
 * $Id: implementation.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: implementation.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.combo.impl;

import static br.com.bradesco.web.pgit.utils.PgitUtil.verificaIntegerNulo;
import static br.com.bradesco.web.pgit.utils.PgitUtil.verificaLongNulo;

import java.util.ArrayList;
import java.util.List;

import br.com.bradesco.web.pgit.service.business.combo.IComboService;
import br.com.bradesco.web.pgit.service.business.combo.IComboServiceConstants;
import br.com.bradesco.web.pgit.service.business.combo.bean.*;
import br.com.bradesco.web.pgit.service.business.combo.impl.bean.ListarModalidadesEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.impl.bean.ListarModalidadesListaDebitoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterfavorecido.bean.ConsultarMotivoBloqueioContaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterfavorecido.bean.ListarMotivoBloqueioFavorecidoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manteroperacoesservrelacionado.IManterOperacoesServRelacionadoServiceConstants;
import br.com.bradesco.web.pgit.service.data.pdc.FactoryAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultardescricaomensagem.request.ConsultarDescricaoMensagemRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultardescricaomensagem.response.ConsultarDescricaoMensagemResponse;
import br.com.bradesco.web.pgit.service.data.pdc.consultarlistamodalidades.request.ConsultarListaModalidadesRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarlistamodalidades.response.ConsultarListaModalidadesResponse;
import br.com.bradesco.web.pgit.service.data.pdc.consultarlistaperiodicidadesistemapgic.request.ConsultarListaPeriodicidadeSistemaPGICRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarlistaperiodicidadesistemapgic.response.ConsultarListaPeriodicidadeSistemaPGICResponse;
import br.com.bradesco.web.pgit.service.data.pdc.consultarlistaservicospgto.request.ConsultarListaServicosPgtoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarlistaservicospgto.response.ConsultarListaServicosPgtoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.consultarlistavaloresdiscretos.request.ConsultarListaValoresDiscretosRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarlistavaloresdiscretos.response.ConsultarListaValoresDiscretosResponse;
import br.com.bradesco.web.pgit.service.data.pdc.consultarmodalidade.request.ConsultarModalidadeRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarmodalidade.response.ConsultarModalidadeResponse;
import br.com.bradesco.web.pgit.service.data.pdc.consultarmodalidadepagto.request.ConsultarModalidadePagtoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarmodalidadepagto.response.ConsultarModalidadePagtoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.consultarmodalidadepgit.request.ConsultarModalidadePGITRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarmodalidadepgit.response.ConsultarModalidadePGITResponse;
import br.com.bradesco.web.pgit.service.data.pdc.consultarmodalidadeservico.request.ConsultarModalidadeServicoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarmodalidadeservico.response.ConsultarModalidadeServicoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.consultarmotivobloqueioconta.request.ConsultarMotivoBloqueioContaRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarmotivobloqueioconta.response.ConsultarMotivoBloqueioContaResponse;
import br.com.bradesco.web.pgit.service.data.pdc.consultarmotivosituacaoestorno.request.ConsultarMotivoSituacaoEstornoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarmotivosituacaoestorno.response.ConsultarMotivoSituacaoEstornoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.consultarmotivosituacaopagamento.request.ConsultarMotivoSituacaoPagamentoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarmotivosituacaopagamento.response.ConsultarMotivoSituacaoPagamentoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.consultarmotivosituacaopagpendente.request.ConsultarMotivoSituacaoPagPendenteRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarmotivosituacaopagpendente.response.ConsultarMotivoSituacaoPagPendenteResponse;
import br.com.bradesco.web.pgit.service.data.pdc.consultarservico.request.ConsultarServicoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarservico.response.ConsultarServicoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.consultarservicopagto.request.ConsultarServicoPagtoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarservicopagto.response.ConsultarServicoPagtoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.consultarservicoscatalogo.request.ConsultarServicosCatalogoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarservicoscatalogo.response.ConsultarServicosCatalogoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.consultarsituacaopagamento.request.ConsultarSituacaoPagamentoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarsituacaopagamento.response.ConsultarSituacaoPagamentoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.consultarsituacaosolconsistenciaprevia.request.ConsultarSituacaoSolConsistenciaPreviaRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarsituacaosolconsistenciaprevia.response.ConsultarSituacaoSolConsistenciaPreviaResponse;
import br.com.bradesco.web.pgit.service.data.pdc.consultarsituacaosolicitacaoestorno.request.ConsultarSituacaoSolicitacaoEstornoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarsituacaosolicitacaoestorno.response.ConsultarSituacaoSolicitacaoEstornoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.consultartipoconta.request.ConsultarTipoContaRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultartipoconta.response.ConsultarTipoContaResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listaraplicativoformatacaoarquivo.request.ListarAplicativoFormatacaoArquivoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listaraplicativoformatacaoarquivo.response.ListarAplicativoFormatacaoArquivoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listaraplictvformttipolayoutarqv.request.ListarAplictvFormtTipoLayoutArqvRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listaraplictvformttipolayoutarqv.response.ListarAplictvFormtTipoLayoutArqvResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarasslotelayoutarquivo.request.ListarAssLoteLayoutArquivoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarasslotelayoutarquivo.response.ListarAssLoteLayoutArquivoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarcentrocusto.request.ListarCentroCustoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarcentrocusto.response.ListarCentroCustoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarclasseramoatvdd.request.ListarClasseRamoAtvddRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarclasseramoatvdd.response.ListarClasseRamoAtvddResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarcomboformaliquidacao.request.ListarComboFormaLiquidacaoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarcomboformaliquidacao.response.ListarComboFormaLiquidacaoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarcontasvinculadascontrato.request.ListarContasVinculadasContratoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarcontasvinculadascontrato.response.ListarContasVinculadasContratoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listardependdiretoria.request.ListarDependDiretoriaRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listardependdiretoria.response.ListarDependDiretoriaResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listardependempresa.request.ListarDependEmpresaRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listardependempresa.response.ListarDependEmpresaResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarempresagestora.request.ListarEmpresaGestoraRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarempresagestora.response.ListarEmpresaGestoraResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarempresatransmissaoarquivovan.request.ListarEmpresaTransmissaoArquivoVanRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarempresatransmissaoarquivovan.response.ListarEmpresaTransmissaoArquivoVanResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarfinalidadeconta.request.ListarFinalidadeContaRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarfinalidadeconta.response.ListarFinalidadeContaResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarfinalidadeendereco.request.ListarFinalidadeEnderecoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarfinalidadeendereco.response.ListarFinalidadeEnderecoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarformalanccnab.request.ListarFormaLancCnabRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarformalanccnab.response.ListarFormaLancCnabResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarformaliquidacao.request.ListarFormaLiquidacaoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarformaliquidacao.response.ListarFormaLiquidacaoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listaridioma.request.ListarIdiomaRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listaridioma.response.ListarIdiomaResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarindiceeconomico.request.ListarIndiceEconomicoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarindiceeconomico.response.ListarIndiceEconomicoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarinscricaofavorecidos.request.ListarInscricaoFavorecidosRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarinscricaofavorecidos.response.ListarInscricaoFavorecidosResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarlayoutscontrato.request.ListarLayoutsContratoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarlayoutscontrato.response.ListarLayoutsContratoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarliquidacaopagamento.request.ListarLiquidacaoPagamentoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarliquidacaopagamento.response.ListarLiquidacaoPagamentoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarmeiotransmissao.request.ListarMeioTransmissaoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarmeiotransmissao.response.ListarMeioTransmissaoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarmeiotransmissaopagamento.request.ListarMeioTransmissaoPagamentoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarmeiotransmissaopagamento.response.ListarMeioTransmissaoPagamentoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarmodalidadecontrato.request.ListarModalidadeContratoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarmodalidadecontrato.response.ListarModalidadeContratoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarmodalidadelistadebito.request.ListarModalidadeListaDebitoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarmodalidadelistadebito.response.ListarModalidadeListaDebitoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarmodalidades.request.ListarModalidadesRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarmodalidades.response.ListarModalidadesResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarmodalidadesestorno.request.ListarModalidadesEstornoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarmodalidadesestorno.response.ListarModalidadesEstornoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarmoedaeconomica.request.ListarMoedaEconomicaRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarmoedaeconomica.response.ListarMoedaEconomicaResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarmotivobloqueiodesbloqueio.request.ListarMotivoBloqueioDesbloqueioRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarmotivobloqueiodesbloqueio.response.ListarMotivoBloqueioDesbloqueioResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarmotivobloqueiofavorecido.request.ListarMotivoBloqueioFavorecidoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarmotivobloqueiofavorecido.response.ListarMotivoBloqueioFavorecidoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarmotivosituacaoaditivocontrato.request.ListarMotivoSituacaoAditivoContratoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarmotivosituacaoaditivocontrato.response.ListarMotivoSituacaoAditivoContratoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarmotivosituacaocontrato.request.ListarMotivoSituacaoContratoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarmotivosituacaocontrato.response.ListarMotivoSituacaoContratoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarmotivosituacaopendcontrato.request.ListarMotivoSituacaoPendContratoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarmotivosituacaopendcontrato.response.ListarMotivoSituacaoPendContratoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarmotivosituacaoservcontrato.request.ListarMotivoSituacaoServContratoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarmotivosituacaoservcontrato.response.ListarMotivoSituacaoServContratoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarmotivosituacaovinccontrato.request.ListarMotivoSituacaoVincContratoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarmotivosituacaovinccontrato.response.ListarMotivoSituacaoVincContratoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarmsgcomprovantesalrl.request.ListarMsgComprovanteSalrlRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarmsgcomprovantesalrl.response.ListarMsgComprovanteSalrlResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarmunicipio.request.ListarMunicipioRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarmunicipio.response.ListarMunicipioResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarnumeroversaolayout.request.ListarNumeroVersaoLayoutRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarnumeroversaolayout.response.ListarNumeroVersaoLayoutResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listaroperacaoservicopagtointegrado.request.ListarOperacaoServicoPagtoIntegradoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listaroperacaoservicopagtointegrado.response.ListarOperacaoServicoPagtoIntegradoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listaroperacoesservico.request.ListarOperacoesServicoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listaroperacoesservico.response.ListarOperacoesServicoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarorgaopagador.request.ListarOrgaoPagadorRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarorgaopagador.response.ListarOrgaoPagadorResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarparametros.request.ListarParametrosRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarparametros.response.ListarParametrosResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarparticipantes.request.ListarParticipantesRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarparticipantes.response.ListarParticipantesResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarperiodicidade.request.ListarPeriodicidadeRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarperiodicidade.response.ListarPeriodicidadeResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarpessoajuridicacontratomarq.request.ListarPessoaJuridicaContratoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarpessoajuridicacontratomarq.response.ListarPessoaJuridicaContratoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarrecursocanalmsg.request.ListarRecursoCanalMsgRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarrecursocanalmsg.response.ListarRecursoCanalMsgResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarresultadoprocessamentoremessa.request.ListarResultadoProcessamentoRemessaRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarresultadoprocessamentoremessa.response.ListarResultadoProcessamentoRemessaResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarsegmentoclientes.request.ListarSegmentoClientesRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarsegmentoclientes.response.ListarSegmentoClientesResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarservicocnab.request.ListarServicoCnabRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarservicocnab.response.ListarServicoCnabResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarservicocomposto.request.ListarServicoCompostoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarservicocomposto.response.ListarServicoCompostoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarservicomodalidadeemissaoaviso.request.ListarServicoModalidadeEmissaoAvisoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarservicomodalidadeemissaoaviso.response.ListarServicoModalidadeEmissaoAvisoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarservicorelacionadocdps.request.ListarServicoRelacionadoCdpsRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarservicorelacionadocdps.response.ListarServicoRelacionadoCdpsResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarservicorelacionadopgit.request.ListarServicoRelacionadoPgitRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarservicorelacionadopgit.response.ListarServicoRelacionadoPgitResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarservicorelacionadopgitcdps.request.ListarServicoRelacionadoPgitCdpsRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarservicorelacionadopgitcdps.response.ListarServicoRelacionadoPgitCdpsResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarservicos.request.ListarServicosRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarservicos.response.ListarServicosResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarservicosemissao.request.ListarServicosEmissaoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarservicosemissao.response.ListarServicosEmissaoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarsituacaoaditivocontrato.request.ListarSituacaoAditivoContratoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarsituacaoaditivocontrato.response.ListarSituacaoAditivoContratoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarsituacaocontrato.request.ListarSituacaoContratoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarsituacaocontrato.response.ListarSituacaoContratoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarsituacaopendcontrato.request.ListarSituacaoPendContratoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarsituacaopendcontrato.response.ListarSituacaoPendContratoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarsituacaoremessa.request.ListarSituacaoRemessaRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarsituacaoremessa.response.ListarSituacaoRemessaResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarsituacaoretorno.request.ListarSituacaoRetornoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarsituacaoretorno.response.ListarSituacaoRetornoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarsituacaosolicitacao.request.ListarSituacaoSolicitacaoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarsituacaosolicitacao.response.ListarSituacaoSolicitacaoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarsituacaovinccontrato.request.ListarSituacaoVincContratoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarsituacaovinccontrato.response.ListarSituacaoVincContratoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarsramoatvddeconc.request.ListarSramoAtvddEconcRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarsramoatvddeconc.response.ListarSramoAtvddEconcResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listartipoacao.request.ListarTipoAcaoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listartipoacao.response.ListarTipoAcaoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listartipoarquivoretorno.request.ListarTipoArquivoRetornoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listartipoarquivoretorno.response.ListarTipoArquivoRetornoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listartipoconta.request.ListarTipoContaRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listartipoconta.response.ListarTipoContaResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listartipocontrato.request.ListarTipoContratoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listartipocontrato.response.ListarTipoContratoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listartipofavorecidos.request.ListarTipoFavorecidosRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listartipofavorecidos.response.ListarTipoFavorecidosResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listartipolayout.request.ListarTipoLayoutRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listartipolayout.response.ListarTipoLayoutResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listartipolayoutarquivo.request.ListarTipoLayoutArquivoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listartipolayoutarquivo.response.ListarTipoLayoutArquivoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listartipolote.request.ListarTipoLoteRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listartipolote.response.ListarTipoLoteResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listartipolote.response.Ocorrencias;
import br.com.bradesco.web.pgit.service.data.pdc.listartipopendencia.request.ListarTipoPendenciaRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listartipopendencia.response.ListarTipoPendenciaResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listartipoprocessamentolayoutarquivo.request.ListarTipoProcessamentoLayoutArquivoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listartipoprocessamentolayoutarquivo.response.ListarTipoProcessamentoLayoutArquivoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listartipoprocesso.request.ListarTipoProcessoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listartipoprocesso.response.ListarTipoProcessoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listartiporelacconta.request.ListarTipoRelacContaRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listartiporelacconta.response.ListarTipoRelacContaResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listartiposistema.request.ListarTipoSistemaRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listartiposistema.response.ListarTipoSistemaResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listartipounidadeorganizacional.request.ListarTipoUnidadeOrganizacionalRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listartipounidadeorganizacional.response.ListarTipoUnidadeOrganizacionalResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listartipovinculo.request.ListarTipoVinculoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listartipovinculo.response.ListarTipoVinculoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarunidadefederativa.request.ListarUnidadeFederativaRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarunidadefederativa.response.ListarUnidadeFederativaResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listatiposervicocontrato.request.ListaTipoServicoContratoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listatiposervicocontrato.response.ListaTipoServicoContratoResponse;
import br.com.bradesco.web.pgit.utils.PgitUtil;

// TODO: Auto-generated Javadoc
/**
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Implementa��o do adaptador: Combo
 * </p>
 * .
 * 
 * @comment C�DIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public class ComboServiceImpl implements IComboService {

    /** Atributo factoryAdapter. */
    private FactoryAdapter factoryAdapter;

    /**
     * Combo service impl.
     */
    public ComboServiceImpl() {

    }

    /**
     * Get: factoryAdapter.
     * 
     * @return factoryAdapter
     */
    public FactoryAdapter getFactoryAdapter() {
        return factoryAdapter;
    }

    /**
     * Set: factoryAdapter.
     * 
     * @param factoryAdapter
     *            the factory adapter
     */
    public void setFactoryAdapter(FactoryAdapter factoryAdapter) {
        this.factoryAdapter = factoryAdapter;
    }

    /**
     * (non-Javadoc).
     * 
     * @param tipoUnidadeOrganizacionalEntradaDTO
     *            the tipo unidade organizacional entrada dto
     * @return the list< tipo unidade organizacional dt o>
     * @see br.com.bradesco.web.pgit.service.business.combo.IComboService#listarTipoUnidadeOrganizacional(br.com.bradesco.web.pgit.service.business.combo.bean.TipoUnidadeOrganizacionalDTO)
     */
    public List<TipoUnidadeOrganizacionalDTO> listarTipoUnidadeOrganizacional(
        TipoUnidadeOrganizacionalDTO tipoUnidadeOrganizacionalEntradaDTO) {

        List<TipoUnidadeOrganizacionalDTO> listaTipoUnidadeOrganizacional =
            new ArrayList<TipoUnidadeOrganizacionalDTO>();
        ListarTipoUnidadeOrganizacionalRequest listarTipoUnidadeOrganizacionalRequest =
            new ListarTipoUnidadeOrganizacionalRequest();
        ListarTipoUnidadeOrganizacionalResponse listarTipoUnidadeOrganizacionalResponse =
            new ListarTipoUnidadeOrganizacionalResponse();

        listarTipoUnidadeOrganizacionalRequest.setCdSituacaoVinculacaoConta(tipoUnidadeOrganizacionalEntradaDTO
            .getCdSituacaoVinculacaoConta());
        listarTipoUnidadeOrganizacionalRequest
            .setMaximoOcorrencias(IComboServiceConstants.NUMERO_OCORRENCIAS_LISTAR_TIPO_UNIDADE_ORGANIZACIONAL);

        listarTipoUnidadeOrganizacionalResponse =
            getFactoryAdapter().getListarTipoUnidadeOrganizacionalPDCAdapter().invokeProcess(
                listarTipoUnidadeOrganizacionalRequest);

        TipoUnidadeOrganizacionalDTO tipoUnidadeOrganizacionalDTORetorno;
        for (int i = 0; i < listarTipoUnidadeOrganizacionalResponse.getOcorrenciasCount(); i++) {
            tipoUnidadeOrganizacionalDTORetorno = new TipoUnidadeOrganizacionalDTO();
            tipoUnidadeOrganizacionalDTORetorno
                .setCodMensagem(listarTipoUnidadeOrganizacionalResponse.getCodMensagem());
            tipoUnidadeOrganizacionalDTORetorno.setMensagem(listarTipoUnidadeOrganizacionalResponse.getMensagem());
            tipoUnidadeOrganizacionalDTORetorno.setCdTipoUnidade(listarTipoUnidadeOrganizacionalResponse
                .getOcorrencias(i).getCdTipoUnidade());
            tipoUnidadeOrganizacionalDTORetorno.setDsTipoUnidade(listarTipoUnidadeOrganizacionalResponse
                .getOcorrencias(i).getDsTipoUnidade());
            listaTipoUnidadeOrganizacional.add(tipoUnidadeOrganizacionalDTORetorno);
        }

        return listaTipoUnidadeOrganizacional;
    }

    /**
     * (non-Javadoc).
     * 
     * @param centroCustoEntradaDTO
     *            the centro custo entrada dto
     * @return the list< centro custo saida dt o>
     * @see br.com.bradesco.web.pgit.service.business.combo.IComboService#listarCentroCusto(br.com.bradesco.web.pgit.service.business.combo.bean.CentroCustoEntradaDTO)
     */
    public List<CentroCustoSaidaDTO> listarCentroCusto(CentroCustoEntradaDTO centroCustoEntradaDTO) {
        ListarCentroCustoRequest listarCentroCustoRequest = new ListarCentroCustoRequest();
        listarCentroCustoRequest.setCdSistema(centroCustoEntradaDTO.getCdSistema() != null ? centroCustoEntradaDTO
            .getCdSistema().toUpperCase() : "");
        listarCentroCustoRequest.setNumeroOcorrencias(IComboServiceConstants.NUMERO_OCORRENCIAS_LISTAR_CENTRO_CUSTO);

        ListarCentroCustoResponse listarCentroCustoResponse =
            getFactoryAdapter().getListarCentroCustoPDCAdapter().invokeProcess(listarCentroCustoRequest);

        CentroCustoSaidaDTO centroCustoSaidaDTORetorno;
        List<CentroCustoSaidaDTO> listaCentroCusto = new ArrayList<CentroCustoSaidaDTO>();
        for (int i = 0; i < listarCentroCustoResponse.getOcorrenciasCount(); i++) {
            centroCustoSaidaDTORetorno = new CentroCustoSaidaDTO();
            centroCustoSaidaDTORetorno.setCodMensagem(listarCentroCustoResponse.getCodMensagem());
            centroCustoSaidaDTORetorno.setMensagem(listarCentroCustoResponse.getMensagem());
            centroCustoSaidaDTORetorno.setCdCentroCusto(listarCentroCustoResponse.getOcorrencias(i).getCdCentroCusto());
            centroCustoSaidaDTORetorno.setDsCentroCusto(listarCentroCustoResponse.getOcorrencias(i).getDsCentroCusto());
            listaCentroCusto.add(centroCustoSaidaDTORetorno);

        }

        return listaCentroCusto;
    }

    /**
     * (non-Javadoc).
     * 
     * @param tipoProcessoEntradaDTO
     *            the tipo processo entrada dto
     * @return the list< tipo processo saida dt o>
     * @see br.com.bradesco.web.pgit.service.business.combo.IComboService#listarTipoProcesso(br.com.bradesco.web.pgit.service.business.combo.bean.TipoProcessoEntradaDTO)
     */
    public List<TipoProcessoSaidaDTO> listarTipoProcesso(TipoProcessoEntradaDTO tipoProcessoEntradaDTO) {
        ListarTipoProcessoRequest listarTipoProcessoRequest = new ListarTipoProcessoRequest();

        // listarTipoProcessoRequest.setCdNaturezaServico(PgitUtil.verificaIntegerNulo(tipoProcessoEntradaDTO.getCdNaturezaServico()));
        listarTipoProcessoRequest.setCdTipoProcsSistema(PgitUtil.verificaIntegerNulo(tipoProcessoEntradaDTO
            .getCdTipoProcsSistema()));
        listarTipoProcessoRequest.setNrOcorrencias(IComboServiceConstants.NUMERO_OCORRENCIAS_LISTAR_TIPO_PROCESSOS);

        ListarTipoProcessoResponse listarTipoProcessoResponse =
            getFactoryAdapter().getListarTipoProcessoPDCAdapter().invokeProcess(listarTipoProcessoRequest);

        List<TipoProcessoSaidaDTO> listaTipoProcesso = new ArrayList<TipoProcessoSaidaDTO>();
        TipoProcessoSaidaDTO tipoProcessoSaidaDTORetorno;
        for (int i = 0; i < listarTipoProcessoResponse.getOcorrenciasCount(); i++) {
            tipoProcessoSaidaDTORetorno = new TipoProcessoSaidaDTO();
            tipoProcessoSaidaDTORetorno.setCodMensagem(listarTipoProcessoResponse.getCodMensagem());
            tipoProcessoSaidaDTORetorno.setMensagem(listarTipoProcessoResponse.getMensagem());
            tipoProcessoSaidaDTORetorno.setCdTipoProcesso(listarTipoProcessoResponse.getOcorrencias(i)
                .getCdTipoProcsSistema());
            tipoProcessoSaidaDTORetorno.setDsTipoProcesso(listarTipoProcessoResponse.getOcorrencias(i)
                .getDsTipoProcsSistema());
            listaTipoProcesso.add(tipoProcessoSaidaDTORetorno);
        }

        return listaTipoProcesso;
    }

    // public List<EmpresaGestoraSaidaDTO>
    // listarEmpresaGestora(EmpresaGestoraEntradaDTO empresaGestoraEntradaDTO) {
    // List<EmpresaGestoraSaidaDTO> listaEmpresaGestora = new
    // ArrayList<EmpresaGestoraSaidaDTO>();
    // ListarEmpresaGestoraRequest listarEmpresaGestoraRequest = new
    // ListarEmpresaGestoraRequest();
    // ListarEmpresaGestoraResponse listarEmpresaGestoraResponse = new
    // ListarEmpresaGestoraResponse();
    //		
    // listarEmpresaGestoraRequest.setCdSituacaoVinculacaoConta(empresaGestoraEntradaDTO.getCdSituacaoVinculacaoConta());
    // listarEmpresaGestoraRequest.setNumeroOcorrencias(IComboServiceConstants.NUMERO_OCORRENCIAS_LISTAR_EMPRESA_GESTORA);
    //		
    //		
    // try{
    //			
    // // listarEmpresaGestoraResponse =
    // getFactoryAdapter().getListarEmpresaGestoraPDCAdapter().invokeProcess(listarEmpresaGestoraRequest);
    //
    // EmpresaGestoraSaidaDTO empresaGestoraSaidaDTORetorno;
    // for (int i=0; i<listarEmpresaGestoraResponse.getOcorrenciasCount();i++){
    //				
    // empresaGestoraSaidaDTORetorno = new EmpresaGestoraSaidaDTO();
    // empresaGestoraSaidaDTORetorno.setCodMensagem(listarEmpresaGestoraResponse.getCodMensagem());
    // empresaGestoraSaidaDTORetorno.setMensagem(listarEmpresaGestoraResponse.getMensagem());
    // empresaGestoraSaidaDTORetorno.setCdPessoaJuridicaContrato(listarEmpresaGestoraResponse.getOcorrencias(i).getCdPessoaJuridicaContrato());
    // empresaGestoraSaidaDTORetorno.setDsCnpj(listarEmpresaGestoraResponse.getOcorrencias(i).getDsCnpj());
    // listaEmpresaGestora.add(empresaGestoraSaidaDTORetorno);
    //				
    // }
    // } catch (PdcAdapterException lException) {
    // throw lException;
    // } catch (ComboServiceException lException) {
    // throw lException;
    // } catch (Exception lException) {
    //			
    // throw new ComboServiceException();
    // }
    //
    //		
    //		
    // return listaEmpresaGestora;
    // }

    /**
     * (non-Javadoc).
     * 
     * @param tipoLoteEntradaDTO
     *            the tipo lote entrada dto
     * @return the list< tipo lote saida dt o>
     * @see br.com.bradesco.web.pgit.service.business.combo.IComboService#listarTipoLote(br.com.bradesco.web.pgit.service.business.combo.bean.TipoLoteEntradaDTO)
     */
    public List<TipoLoteSaidaDTO> listarTipoLote(TipoLoteEntradaDTO tipoLoteEntradaDTO) {
        ListarTipoLoteRequest listarTipoLoteRequest = new ListarTipoLoteRequest();
        ListarTipoLoteResponse listarTipoLoteResponse = new ListarTipoLoteResponse();

        listarTipoLoteRequest.setCdSituacaoVinculacaoConta(tipoLoteEntradaDTO.getCdSituacaoVinculacaoConta());
        listarTipoLoteRequest.setNumeroOcorrencias(IComboServiceConstants.NUMERO_OCORRENCIAS_LISTAR_TIPO_LOTE);

        listarTipoLoteResponse = getFactoryAdapter().getListarTipoLotePDCAdapter().invokeProcess(listarTipoLoteRequest);

        if (listarTipoLoteResponse == null) {
            return new ArrayList<TipoLoteSaidaDTO>();
        }

        List<TipoLoteSaidaDTO> listLote = new ArrayList<TipoLoteSaidaDTO>();

        for (int i = 0; i < listarTipoLoteResponse.getOcorrenciasCount(); i++) {
            Ocorrencias param = listarTipoLoteResponse.getOcorrencias(i);

            TipoLoteSaidaDTO saida = new TipoLoteSaidaDTO(param.getCdTipoLoteLayout(), param.getDsTipoLoteLayout());
            listLote.add(saida);

        }

        return listLote;
    }

    /**
     * (non-Javadoc).
     * 
     * @param tipoLayoutArquivoEntradaDTO
     *            the tipo layout arquivo entrada dto
     * @return the tipo layout arquivo saida dto
     * @see br.com.bradesco.web.pgit.service.business.combo.IComboService#listarTipoLayoutArquivo(br.com.bradesco.web.pgit.service.business.combo.bean.TipoLayoutArquivoEntradaDTO)
     */
    @SuppressWarnings("unchecked")
    public TipoLayoutArquivoSaidaDTO listarTipoLayoutArquivo(TipoLayoutArquivoEntradaDTO tipoLayoutArquivoEntradaDTO) {
        ListarTipoLayoutArquivoRequest listarTipoLayoutArquivoRequest = new ListarTipoLayoutArquivoRequest();
        ListarTipoLayoutArquivoResponse listarTipoLayoutArquivoResponse = new ListarTipoLayoutArquivoResponse();

        listarTipoLayoutArquivoRequest.setCdSituacaoVinculacaoConta(tipoLayoutArquivoEntradaDTO
            .getCdSituacaoVinculacaoConta());
        listarTipoLayoutArquivoRequest
            .setNumeroOcorrencias(IComboServiceConstants.NUMERO_OCORRENCIAS_LISTAR_TIPO_LAYOUT);

        listarTipoLayoutArquivoResponse =
            getFactoryAdapter().getListarTipoLayoutArquivoPDCAdapter().invokeProcess(listarTipoLayoutArquivoRequest);

        if (listarTipoLayoutArquivoResponse == null) {
            return null;
        }

        TipoLayoutArquivoSaidaDTO saida = new TipoLayoutArquivoSaidaDTO();

        saida.setNumeroVersaoLayoutArquivo(listarTipoLayoutArquivoResponse.getNumeroVersaoLayoutArquivo());
        saida.setNumeroLinhas(listarTipoLayoutArquivoResponse.getNumeroLinhas());

        List listaTipoLayoutArquivo = new ArrayList();

        for (int i = 0; i < listarTipoLayoutArquivoResponse.getOcorrenciasCount(); i++) {
            br.com.bradesco.web.pgit.service.data.pdc.listartipolayoutarquivo.response.Ocorrencias param =
                listarTipoLayoutArquivoResponse.getOcorrencias(i);

            TipoLayoutArquivoSaidaDTO saidaLista =
                new TipoLayoutArquivoSaidaDTO(param.getCdTipoLayout(), param.getDsTipoLayout());
            listaTipoLayoutArquivo.add(saidaLista);
            saida.setOcorrencias(listaTipoLayoutArquivo);
        }

        return saida;
    }

    /**
     * (non-Javadoc).
     * 
     * @param periodicidadeEntradaDTO
     *            the periodicidade entrada dto
     * @return the list< periodicidade saida dt o>
     * @see br.com.bradesco.web.pgit.service.business.combo.IComboService#listarPeriodicidade(br.com.bradesco.web.pgit.service.business.combo.bean.PeriodicidadeEntradaDTO)
     */
    public List<PeriodicidadeSaidaDTO> listarPeriodicidade(PeriodicidadeEntradaDTO periodicidadeEntradaDTO) {
        ListarPeriodicidadeRequest listarPeriodicidadeRequest = new ListarPeriodicidadeRequest();
        listarPeriodicidadeRequest.setCdSituacaoVinculacaoConta(periodicidadeEntradaDTO.getCdSituacaoVinculacaoConta());
        listarPeriodicidadeRequest.setNumeroOcorrencias(IComboServiceConstants.NUMERO_OCORRENCIAS_LISTAR_PERIODICIDADE);

        ListarPeriodicidadeResponse listarPeriodicidadeResponse =
            getFactoryAdapter().getListarPeriodicidadePDCAdapter().invokeProcess(listarPeriodicidadeRequest);

        List<PeriodicidadeSaidaDTO> listaPeriodicidadeSaidaDTO = new ArrayList<PeriodicidadeSaidaDTO>();
        PeriodicidadeSaidaDTO periodicidadeSaidaDTORetorno;
        for (int i = 0; i < listarPeriodicidadeResponse.getOcorrenciasCount(); i++) {
            periodicidadeSaidaDTORetorno = new PeriodicidadeSaidaDTO();
            periodicidadeSaidaDTORetorno.setCodMensagem(listarPeriodicidadeResponse.getCodMensagem());
            periodicidadeSaidaDTORetorno.setMensagem(listarPeriodicidadeResponse.getMensagem());
            periodicidadeSaidaDTORetorno.setCdPeriodicidade(listarPeriodicidadeResponse.getOcorrencias(i)
                .getCdPeriodicidade());
            periodicidadeSaidaDTORetorno.setDsPeriodicidade(listarPeriodicidadeResponse.getOcorrencias(i)
                .getDsPeriodicidade());
            listaPeriodicidadeSaidaDTO.add(periodicidadeSaidaDTORetorno);
        }

        return listaPeriodicidadeSaidaDTO;
    }

    /**
     * (non-Javadoc).
     * 
     * @param periodicidadeEntradaDTO
     *            the periodicidade entrada dto
     * @return the list< periodicidade saida dt o>
     * @see br.com.bradesco.web.pgit.service.business.combo.IComboService#consultarListaPeriodicidade(br.com.bradesco.web.pgit.service.business.combo.bean.PeriodicidadeEntradaDTO)
     */
    public List<PeriodicidadeSaidaDTO> consultarListaPeriodicidade(PeriodicidadeEntradaDTO periodicidadeEntradaDTO) {
        ConsultarListaPeriodicidadeSistemaPGICRequest request = new ConsultarListaPeriodicidadeSistemaPGICRequest();
        ConsultarListaPeriodicidadeSistemaPGICResponse response = new ConsultarListaPeriodicidadeSistemaPGICResponse();

        request.setCdSituacaoVinculacaoConta(periodicidadeEntradaDTO.getCdSituacaoVinculacaoConta());
        request.setNrOcorrencias(IComboServiceConstants.NUMERO_OCORRENCIAS_LISTAR_PERIODICIDADE);

        response = getFactoryAdapter().getConsultarListaPeriodicidadeSistemaPGICPDCAdapter().invokeProcess(request);

        List<PeriodicidadeSaidaDTO> listaPeriodicidadeSaidaDTO = new ArrayList<PeriodicidadeSaidaDTO>();
        PeriodicidadeSaidaDTO periodicidadeSaidaDTORetorno;
        for (int i = 0; i < response.getOcorrenciasCount(); i++) {
            periodicidadeSaidaDTORetorno = new PeriodicidadeSaidaDTO();
            periodicidadeSaidaDTORetorno.setCodMensagem(response.getCodMensagem());
            periodicidadeSaidaDTORetorno.setMensagem(response.getMensagem());
            periodicidadeSaidaDTORetorno.setCdPeriodicidade(response.getOcorrencias(i).getCdPeriodicidade());
            periodicidadeSaidaDTORetorno.setDsPeriodicidade(response.getOcorrencias(i).getDsPeriodicidade());
            listaPeriodicidadeSaidaDTO.add(periodicidadeSaidaDTORetorno);
        }

        return listaPeriodicidadeSaidaDTO;
    }

    /**
     * (non-Javadoc).
     * 
     * @return the list< empresa gestora saida dt o>
     * @see br.com.bradesco.web.pgit.service.business.combo.IComboService#listarEmpresasGestoras()
     */
    public List<EmpresaGestoraSaidaDTO> listarEmpresasGestoras() {

        ListarEmpresaGestoraRequest request = new ListarEmpresaGestoraRequest();
        request.setCdSituacaoVinculacaoConta(0);
        request.setNumeroOcorrencias(IComboServiceConstants.LISTAR_EMPRESA_GESTORA);

        ListarEmpresaGestoraResponse response =
            getFactoryAdapter().getListarEmpresaGestoraPDCAdapter().invokeProcess(request);

        List<EmpresaGestoraSaidaDTO> list = new ArrayList<EmpresaGestoraSaidaDTO>();
        for (br.com.bradesco.web.pgit.service.data.pdc.listarempresagestora.response.Ocorrencias saida : response
            .getOcorrencias()) {
            list.add(new EmpresaGestoraSaidaDTO(saida));
        }

        return list;
    }

    /**
     * (non-Javadoc).
     * 
     * @return the list< tipo contrato saida dt o>
     * @see br.com.bradesco.web.pgit.service.business.combo.IComboService#listarTipoContrato()
     */
    public List<TipoContratoSaidaDTO> listarTipoContrato() {

        ListarTipoContratoRequest request = new ListarTipoContratoRequest();
        request.setCdSituacaoVinculacaoConta(0);
        request.setNumeroOcorrencias(30);

        ListarTipoContratoResponse response =
            getFactoryAdapter().getListarTipoContratoPDCAdapter().invokeProcess(request);

        List<TipoContratoSaidaDTO> list = new ArrayList<TipoContratoSaidaDTO>();
        for (br.com.bradesco.web.pgit.service.data.pdc.listartipocontrato.response.Ocorrencias saida : response
            .getOcorrencias()) {
            list.add(new TipoContratoSaidaDTO(saida));
        }

        return list;
    }

    /**
     * (non-Javadoc).
     * 
     * @param empresaConglomeradoEntradaDTO
     *            the empresa conglomerado entrada dto
     * @return the list< empresa conglomerado saida dt o>
     * @see br.com.bradesco.web.pgit.service.business.combo.IComboService#listarEmpresaConglomerado(br.com.bradesco.web.pgit.service.business.combo.bean.EmpresaConglomeradoEntradaDTO)
     */
    public List<EmpresaConglomeradoSaidaDTO> listarEmpresaConglomerado(
        EmpresaConglomeradoEntradaDTO empresaConglomeradoEntradaDTO) {
        ListarEmpresaGestoraRequest request = new ListarEmpresaGestoraRequest();

        request.setCdSituacaoVinculacaoConta(empresaConglomeradoEntradaDTO.getCdSituacao() != null
            ? empresaConglomeradoEntradaDTO.getCdSituacao() : 0);
        request.setNumeroOcorrencias(IComboServiceConstants.LISTAR_EMPRESA_GESTORA);

        ListarEmpresaGestoraResponse response =
            getFactoryAdapter().getListarEmpresaGestoraPDCAdapter().invokeProcess(request);

        if (response == null) {
            return null;
        }

        List<EmpresaConglomeradoSaidaDTO> listaEmpresaConglomerado = new ArrayList<EmpresaConglomeradoSaidaDTO>();
        for (br.com.bradesco.web.pgit.service.data.pdc.listarempresagestora.response.Ocorrencias saida : response
            .getOcorrencias()) {
            EmpresaConglomeradoSaidaDTO empresa = new EmpresaConglomeradoSaidaDTO();
            empresa.setCodClub(saida.getCdPessoaJuridicaContrato());
            empresa.setDsRazaoSocial(saida.getDsCnpj());
            listaEmpresaConglomerado.add(empresa);
        }

        return listaEmpresaConglomerado;
    }

    /**
     * (non-Javadoc).
     * 
     * @param moedaEconomicaEntradaDTO
     *            the moeda economica entrada dto
     * @return the list< moeda economica saida dt o>
     * @see br.com.bradesco.web.pgit.service.business.combo.IComboService#listarMoedaEconomica(br.com.bradesco.web.pgit.service.business.combo.bean.MoedaEconomicaEntradaDTO)
     */
    public List<MoedaEconomicaSaidaDTO> listarMoedaEconomica(MoedaEconomicaEntradaDTO moedaEconomicaEntradaDTO) {
        ListarMoedaEconomicaRequest request = new ListarMoedaEconomicaRequest();
        request.setNrOcorrencias(100);
        request.setCdSituacao(0);

        ListarMoedaEconomicaResponse response =
            getFactoryAdapter().getListarMoedaEconomicaPDCAdapter().invokeProcess(request);

        if (response == null) {
            return null;
        }

        List<MoedaEconomicaSaidaDTO> listarMoedaEconomica = new ArrayList<MoedaEconomicaSaidaDTO>();
        for (br.com.bradesco.web.pgit.service.data.pdc.listarmoedaeconomica.response.Ocorrencias saida : response
            .getOcorrencias()) {
            MoedaEconomicaSaidaDTO listarMoeda = new MoedaEconomicaSaidaDTO();
            listarMoeda.setCdIndicadorEconomico(saida.getCdIndicadorEconomico());
            listarMoeda.setDsIndicadorEconomico(saida.getDsIndicadorEconomico());
            listarMoedaEconomica.add(listarMoeda);
        }

        return listarMoedaEconomica;
    }

    /**
     * (non-Javadoc).
     * 
     * @return the list< tipo favorecido saida dt o>
     * @see br.com.bradesco.web.pgit.service.business.combo.IComboService#listarTipoFavorecido()
     */
    public List<TipoFavorecidoSaidaDTO> listarTipoFavorecido() {

        ListarTipoFavorecidosRequest request = new ListarTipoFavorecidosRequest();

        request.setCdSituacaoVinculacaoConta(0);
        request.setNumeroOcorrencias(30);

        ListarTipoFavorecidosResponse response =
            getFactoryAdapter().getListarTipoFavorecidosPDCAdapter().invokeProcess(request);

        List<TipoFavorecidoSaidaDTO> list = new ArrayList<TipoFavorecidoSaidaDTO>();

        for (br.com.bradesco.web.pgit.service.data.pdc.listartipofavorecidos.response.Ocorrencias saida : response
            .getOcorrencias()) {
            list.add(new TipoFavorecidoSaidaDTO(saida.getCdTipoFavorecidos(), saida.getDsTipoFavorecidos()));
        }

        return list;
    }

    /**
     * (non-Javadoc).
     * 
     * @return the list< tipo sistema saida dt o>
     * @see br.com.bradesco.web.pgit.service.business.combo.IComboService#listarTipoSistema()
     */
    public List<TipoSistemaSaidaDTO> listarTipoSistema() {

        ListarTipoSistemaRequest request = new ListarTipoSistemaRequest();

        request.setNumeroOcorrencias(100);

        ListarTipoSistemaResponse response =
            getFactoryAdapter().getListarTipoSistemaPDCAdapter().invokeProcess(request);

        List<TipoSistemaSaidaDTO> list = new ArrayList<TipoSistemaSaidaDTO>();

        for (br.com.bradesco.web.pgit.service.data.pdc.listartiposistema.response.Ocorrencias saida : response
            .getOcorrencias()) {
            list.add(new TipoSistemaSaidaDTO(saida.getCdBaseSistema(), saida.getDsBaseSistema()));
        }

        return list;
    }

    /**
     * (non-Javadoc).
     * 
     * @return the list< inscricao favorecidos saida dt o>
     * @see br.com.bradesco.web.pgit.service.business.combo.IComboService#listarTipoInscricao()
     */
    public List<InscricaoFavorecidosSaidaDTO> listarTipoInscricao() {

        ListarInscricaoFavorecidosRequest request = new ListarInscricaoFavorecidosRequest();

        request.setCdSituacaoVinculacaoConta(0);
        request.setNumeroOcorrencias(30);

        ListarInscricaoFavorecidosResponse response =
            getFactoryAdapter().getListarInscricaoFavorecidosPDCAdapter().invokeProcess(request);

        List<InscricaoFavorecidosSaidaDTO> list = new ArrayList<InscricaoFavorecidosSaidaDTO>();

        for (br.com.bradesco.web.pgit.service.data.pdc.listarinscricaofavorecidos.response.Ocorrencias saida : response
            .getOcorrencias()) {
            list.add(new InscricaoFavorecidosSaidaDTO(saida.getCdTipoInscricaoFavorecidos(), saida
                .getDsTipoInscricaoFavorecidos()));
        }

        return list;
    }

    /**
     * (non-Javadoc).
     * 
     * @return the list< listar forma liquidacao saida dt o>
     * @see br.com.bradesco.web.pgit.service.business.combo.IComboService#listarFormaLiquidacao()
     */
    public List<ListarFormaLiquidacaoSaidaDTO> listarFormaLiquidacao() {

        ListarFormaLiquidacaoRequest request = new ListarFormaLiquidacaoRequest();

        request.setCdSituacaoVinculacaoConta(0);
        request.setNumeroOcorrencias(50);

        ListarFormaLiquidacaoResponse response =
            getFactoryAdapter().getListarFormaLiquidacaoPDCAdapter().invokeProcess(request);
        List<ListarFormaLiquidacaoSaidaDTO> list = new ArrayList<ListarFormaLiquidacaoSaidaDTO>();

        for (br.com.bradesco.web.pgit.service.data.pdc.listarformaliquidacao.response.Ocorrencias ocorrencia : response
            .getOcorrencias()) {
            list.add(new ListarFormaLiquidacaoSaidaDTO(ocorrencia.getCdFormaLiquidacao(), ocorrencia
                .getDsFormaLiquidacao()));
        }

        return list;
    }

    /**
     * (non-Javadoc).
     * 
     * @return the list< listar forma liquidacao saida dt o>
     * @see br.com.bradesco.web.pgit.service.business.combo.IComboService#listarComboFormaLiquidacao()
     */
    public List<ListarFormaLiquidacaoSaidaDTO> listarComboFormaLiquidacao() {

        ListarComboFormaLiquidacaoRequest request = new ListarComboFormaLiquidacaoRequest();

        request.setCdSituacaoVinculacaoConta(0);
        request.setNumeroOcorrencias(50);

        ListarComboFormaLiquidacaoResponse response =
            getFactoryAdapter().getListarComboFormaLiquidacaoPDCAdapter().invokeProcess(request);
        List<ListarFormaLiquidacaoSaidaDTO> list = new ArrayList<ListarFormaLiquidacaoSaidaDTO>();

        for (br.com.bradesco.web.pgit.service.data.pdc.listarcomboformaliquidacao.response.Ocorrencias ocorrencia : response
            .getOcorrencias()) {
            list.add(new ListarFormaLiquidacaoSaidaDTO(ocorrencia.getCdFormaLiquidacao(), ocorrencia
                .getDsFormaLiquidacao()));
        }

        return list;
    }

    /**
     * (non-Javadoc).
     * 
     * @param entradaDTO
     *            the entrada dto
     * @return the list< listar modalidade saida dt o>
     * @see br.com.bradesco.web.pgit.service.business.combo.IComboService#listarModalidades(br.com.bradesco.web.pgit.service.business.combo.impl.bean.ListarModalidadesEntradaDTO)
     */
    public List<ListarModalidadeSaidaDTO> listarModalidades(ListarModalidadesEntradaDTO entradaDTO) {
        ListarModalidadesRequest request = new ListarModalidadesRequest();

        request.setNumeroOcorrencias(IComboServiceConstants.NUMERO_OCORRENCIAS_LISTAR_MODALIDADES);
        request.setCdProdutoServicoOperacao(PgitUtil.verificaIntegerNulo(entradaDTO.getCdServico()));

        ListarModalidadesResponse response =
            getFactoryAdapter().getListarModalidadesPDCAdapter().invokeProcess(request);
        List<ListarModalidadeSaidaDTO> list = new ArrayList<ListarModalidadeSaidaDTO>();

        for (br.com.bradesco.web.pgit.service.data.pdc.listarmodalidades.response.Ocorrencias ocorrencia : response
            .getOcorrencias()) {
            list.add(new ListarModalidadeSaidaDTO(ocorrencia.getCdProdutoOperacaoRelacionado(), ocorrencia
                .getDsProdutoOperacaoRelacionado(), ocorrencia.getCdRelacionamentoProduto()));

        }
        return list;
    }

    /**
     * (non-Javadoc).
     * 
     * @param entradaDTO
     *            the entrada dto
     * @return the list< listar modalidade saida dt o>
     * @see br.com.bradesco.web.pgit.service.business.combo.IComboService#listarModalidadesEntrada(br.com.bradesco.web.pgit.service.business.combo.impl.bean.ListarModalidadesEntradaDTO)
     */
    public List<ListarModalidadeSaidaDTO> listarModalidadesEntrada(ListarModalidadesEntradaDTO entradaDTO) {
        ListarModalidadesRequest request = new ListarModalidadesRequest();

        request.setNumeroOcorrencias(entradaDTO.getNumeroOcorrencias());
        request.setCdProdutoServicoOperacao(entradaDTO.getCdServico());

        ListarModalidadesResponse response =
            getFactoryAdapter().getListarModalidadesPDCAdapter().invokeProcess(request);
        List<ListarModalidadeSaidaDTO> list = new ArrayList<ListarModalidadeSaidaDTO>();

        for (br.com.bradesco.web.pgit.service.data.pdc.listarmodalidades.response.Ocorrencias ocorrencia : response
            .getOcorrencias()) {
            list.add(new ListarModalidadeSaidaDTO(ocorrencia.getCdProdutoOperacaoRelacionado(), ocorrencia
                .getDsProdutoOperacaoRelacionado(), ocorrencia.getCdRelacionamentoProduto()));

        }
        return list;
    }

    /**
     * (non-Javadoc).
     * 
     * @param entradaDTO
     *            the entrada dto
     * @return the list< listar modalidade lista debito saida dt o>
     * @see br.com.bradesco.web.pgit.service.business.combo.IComboService#listarModalidadesListaDebitoEntrada(br.com.bradesco.web.pgit.service.business.combo.impl.bean.ListarModalidadesListaDebitoEntradaDTO)
     */
    public List<ListarModalidadeListaDebitoSaidaDTO> listarModalidadesListaDebitoEntrada(
        ListarModalidadesListaDebitoEntradaDTO entradaDTO) {
        ListarModalidadeListaDebitoRequest request = new ListarModalidadeListaDebitoRequest();

        request.setNrOcorrencias(entradaDTO.getNrOcorrencias());
        request.setCdProdutoServicoOperacao(entradaDTO.getCdProdutoServicoOperacao());

        ListarModalidadeListaDebitoResponse response =
            getFactoryAdapter().getListarModalidadeListaDebitoPDCAdapter().invokeProcess(request);
        List<ListarModalidadeListaDebitoSaidaDTO> list = new ArrayList<ListarModalidadeListaDebitoSaidaDTO>();

        for (br.com.bradesco.web.pgit.service.data.pdc.listarmodalidadelistadebito.response.Ocorrencias ocorrencia : response
            .getOcorrencias()) {
            list.add(new ListarModalidadeListaDebitoSaidaDTO(ocorrencia.getCdProdutoOperacaoRelacionado(), ocorrencia
                .getDsProdutoOperacaoRelacionada(), ocorrencia.getCdRelacionamentoProduto(), ocorrencia
                .getCdServicoCompostoPgit()));

        }
        return list;
    }

    /**
     * (non-Javadoc).
     * 
     * @return the list< listar motivo bloqueio favorecido saida dt o>
     * @see br.com.bradesco.web.pgit.service.business.combo.IComboService#listarMotivoBloqueioFavorecido()
     */
    public List<ListarMotivoBloqueioFavorecidoSaidaDTO> listarMotivoBloqueioFavorecido() {

        ListarMotivoBloqueioFavorecidoRequest request = new ListarMotivoBloqueioFavorecidoRequest();

        request.setCdSituacaoVinculacaoConta(0);
        request.setNumeroOcorrencia(30);

        ListarMotivoBloqueioFavorecidoResponse response =
            getFactoryAdapter().getListarMotivoBloqueioFavorecidoPDCAdapter().invokeProcess(request);

        List<ListarMotivoBloqueioFavorecidoSaidaDTO> list = new ArrayList<ListarMotivoBloqueioFavorecidoSaidaDTO>();

        for (br.com.bradesco.web.pgit.service.data.pdc.listarmotivobloqueiofavorecido.response.Ocorrencias saida : response
            .getOcorrencias()) {
            list.add(new ListarMotivoBloqueioFavorecidoSaidaDTO(saida.getCdMotivoBloqueioFavorecido(), saida
                .getDtMotivoBloqueioFavorecido()));
        }

        return list;
    }

    /**
     * (non-Javadoc).
     * 
     * @return the list< consultar motivo bloqueio conta saida dt o>
     * @see br.com.bradesco.web.pgit.service.business.combo.IComboService#listarMotivoBloqueioContaFavorecido()
     */
    public List<ConsultarMotivoBloqueioContaSaidaDTO> listarMotivoBloqueioContaFavorecido() {

        ConsultarMotivoBloqueioContaRequest request = new ConsultarMotivoBloqueioContaRequest();

        request.setCdSituacaoVinculacaoConta(0);
        request.setNumeroOcorrencias(30);

        ConsultarMotivoBloqueioContaResponse response =
            getFactoryAdapter().getConsultarMotivoBloqueioContaPDCAdapter().invokeProcess(request);

        List<ConsultarMotivoBloqueioContaSaidaDTO> list = new ArrayList<ConsultarMotivoBloqueioContaSaidaDTO>();

        for (br.com.bradesco.web.pgit.service.data.pdc.consultarmotivobloqueioconta.response.Ocorrencias saida : response
            .getOcorrencias()) {
            list.add(new ConsultarMotivoBloqueioContaSaidaDTO(saida.getCdMotivoBloqueioConta(), saida
                .getDsMotivoBloqueioConta()));
        }

        return list;
    }

    /**
     * (non-Javadoc).
     * 
     * @param natureza
     *            the natureza
     * @return the list< listar servicos saida dt o>
     * @see br.com.bradesco.web.pgit.service.business.combo.IComboService#listarTipoServicos(int)
     */
    public List<ListarServicosSaidaDTO> listarTipoServicos(int natureza) {

        ListarServicosRequest request = new ListarServicosRequest();

        request.setCdNaturezaServico(natureza);
        request.setNumeroOcorrencias(30);

        ListarServicosResponse response = getFactoryAdapter().getListarServicosPDCAdapter().invokeProcess(request);

        List<ListarServicosSaidaDTO> list = new ArrayList<ListarServicosSaidaDTO>();

        for (br.com.bradesco.web.pgit.service.data.pdc.listarservicos.response.Ocorrencias saida : response
            .getOcorrencias()) {
            list.add(new ListarServicosSaidaDTO(saida.getCdServico(), saida.getDsServico()));
        }
        return list;
    }

    /**
     * (non-Javadoc).
     * 
     * @param entrada
     *            the entrada
     * @return the list< listar modalidade saida dt o>
     * @see br.com.bradesco.web.pgit.service.business.combo.IComboService#listarTipoModalidades(br.com.bradesco.web.pgit.service.business.combo.impl.bean.ListarModalidadesEntradaDTO)
     */
    public List<ListarModalidadeSaidaDTO> listarTipoModalidades(ListarModalidadesEntradaDTO entrada) {

        ListarModalidadesRequest request = new ListarModalidadesRequest();

        request.setCdProdutoServicoOperacao(entrada.getCdServico());

        ListarModalidadesResponse response =
            getFactoryAdapter().getListarModalidadesPDCAdapter().invokeProcess(request);
        List<ListarModalidadeSaidaDTO> list = new ArrayList<ListarModalidadeSaidaDTO>();

        for (br.com.bradesco.web.pgit.service.data.pdc.listarmodalidades.response.Ocorrencias saida : response
            .getOcorrencias()) {
            list.add(new ListarModalidadeSaidaDTO(saida.getCdProdutoOperacaoRelacionado(), saida
                .getDsProdutoOperacaoRelacionado(), saida.getCdRelacionamentoProduto()));
        }
        return list;
    }

    /**
     * (non-Javadoc).
     * 
     * @return the list< empresa segmento saida dt o>
     * @see br.com.bradesco.web.pgit.service.business.combo.IComboService#pesquisaSegmento()
     */
    public List<EmpresaSegmentoSaidaDTO> pesquisaSegmento() {

        ListarSegmentoClientesRequest request = new ListarSegmentoClientesRequest();

        request.setCdSituacaoVinculacaoConta(0);
        request.setNumeroOcorrencias(80);

        ListarSegmentoClientesResponse response =
            getFactoryAdapter().getListarSegmentoClientesPDCAdapter().invokeProcess(request);

        List<EmpresaSegmentoSaidaDTO> list = new ArrayList<EmpresaSegmentoSaidaDTO>();

        for (br.com.bradesco.web.pgit.service.data.pdc.listarsegmentoclientes.response.Ocorrencias saida : response
            .getOcorrencias()) {
            list.add(new EmpresaSegmentoSaidaDTO(saida.getCdSegmento(), saida.getDsSegmento(), saida
                .getCdSituacaoSegmento(), saida.getDsSituacaoSegmento()));
        }

        return list;
    }

    /**
     * (non-Javadoc).
     * 
     * @param empresaGestora
     *            the empresa gestora
     * @return the list< listar dependencia empresa saida dt o>
     * @see br.com.bradesco.web.pgit.service.business.combo.IComboService#pesquisaDiretoria(java.lang.Long)
     */
    public List<ListarDependenciaEmpresaSaidaDTO> pesquisaDiretoria(Long empresaGestora) {

        ListarDependEmpresaRequest request = new ListarDependEmpresaRequest();
        request.setCdTipoUnidadeOrganizacional(24);
        request.setCdTipoPessoaJuridica(empresaGestora);
        request.setNumeroOcorrencias(100);

        ListarDependEmpresaResponse response =
            getFactoryAdapter().getListarDependEmpresaPDCAdapter().invokeProcess(request);

        List<ListarDependenciaEmpresaSaidaDTO> list = new ArrayList<ListarDependenciaEmpresaSaidaDTO>();

        for (br.com.bradesco.web.pgit.service.data.pdc.listardependempresa.response.Ocorrencias saida : response
            .getOcorrencias()) {
            list.add(new ListarDependenciaEmpresaSaidaDTO(saida.getCdUnidadeOrganizacional(), saida
                .getDsUnidadeOrganizacional()));
        }

        return list;
    }

    /**
     * (non-Javadoc).
     * 
     * @param codPessoaJuridica
     *            the cod pessoa juridica
     * @param empresaGerencia
     *            the empresa gerencia
     * @param tipoPesquisa
     *            the tipo pesquisa
     * @return the list< listar dependencia diretoria saida dt o>
     * @see br.com.bradesco.web.pgit.service.business.combo.IComboService#pesquisaGerencia(java.lang.Long,
     *      java.lang.Long, java.lang.Integer)
     */
    public List<ListarDependenciaDiretoriaSaidaDTO> pesquisaGerencia(Long codPessoaJuridica, Long empresaGerencia,
        Integer tipoPesquisa) {

        ListarDependDiretoriaRequest request = new ListarDependDiretoriaRequest();

        request.setCdPessoa(codPessoaJuridica);
        request.setCdTipoPesquisa(24);
        request.setNrOcorrencias(100);
        request.setNrSeqUnidadeOrganizacional(empresaGerencia.intValue());

        List<ListarDependenciaDiretoriaSaidaDTO> list = new ArrayList<ListarDependenciaDiretoriaSaidaDTO>();

        ListarDependDiretoriaResponse response =
            getFactoryAdapter().getListarDependDiretoriaPDCAdapter().invokeProcess(request);

        for (br.com.bradesco.web.pgit.service.data.pdc.listardependdiretoria.response.Ocorrencias saida : response
            .getOcorrencias()) {
            list.add(new ListarDependenciaDiretoriaSaidaDTO(saida.getCdEmpresa(), saida.getCdDependencia(), saida
                .getDsDependencia(), saida.getCdNaturezaDependencia()));
        }

        return list;
    }

    /**
     * (non-Javadoc).
     * 
     * @return the list< listar tipo arquivo retorno saida dt o>
     * @see br.com.bradesco.web.pgit.service.business.combo.IComboService#listarTipoArquivoRetorno()
     */
    public List<ListarTipoArquivoRetornoSaidaDTO> listarTipoArquivoRetorno() {

        ListarTipoArquivoRetornoRequest request = new ListarTipoArquivoRetornoRequest();
        request.setCdTipoLayout(0);
        request.setNumeroOcorrencias(30);

        ListarTipoArquivoRetornoResponse response =
            getFactoryAdapter().getListarTipoArquivoRetornoPDCAdapter().invokeProcess(request);

        List<ListarTipoArquivoRetornoSaidaDTO> list = new ArrayList<ListarTipoArquivoRetornoSaidaDTO>();

        for (br.com.bradesco.web.pgit.service.data.pdc.listartipoarquivoretorno.response.Ocorrencias ocorrencia : response
            .getOcorrencias()) {
            list.add(new ListarTipoArquivoRetornoSaidaDTO(ocorrencia.getCdArquivoRetorno(), ocorrencia
                .getDsArquivoRetorno()));
        }

        return list;
    }

    /**
     * (non-Javadoc).
     * 
     * @param entradaDTO
     *            the entrada dto
     * @return the list< listar tipo arquivo retorno saida dt o>
     * @see br.com.bradesco.web.pgit.service.business.combo.IComboService#listarTipoArquivoRetorno(br.com.bradesco.web.pgit.service.business.combo.bean.ListarTipoArquivoRetornoEntradaDTO)
     */
    public List<ListarTipoArquivoRetornoSaidaDTO> listarTipoArquivoRetorno(ListarTipoArquivoRetornoEntradaDTO entradaDTO) {

        ListarTipoArquivoRetornoRequest request = new ListarTipoArquivoRetornoRequest();

        request.setCdTipoLayout(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoLayout()));
        request.setNumeroOcorrencias(30);

        ListarTipoArquivoRetornoResponse response =
            getFactoryAdapter().getListarTipoArquivoRetornoPDCAdapter().invokeProcess(request);
        List<ListarTipoArquivoRetornoSaidaDTO> list = new ArrayList<ListarTipoArquivoRetornoSaidaDTO>();

        for (br.com.bradesco.web.pgit.service.data.pdc.listartipoarquivoretorno.response.Ocorrencias ocorrencia : response
            .getOcorrencias()) {
            list.add(new ListarTipoArquivoRetornoSaidaDTO(ocorrencia.getCdArquivoRetorno(), ocorrencia
                .getDsArquivoRetorno()));

        }
        return list;
    }

    /**
     * (non-Javadoc).
     * 
     * @return the list< tipo acao saida dt o>
     * @see br.com.bradesco.web.pgit.service.business.combo.IComboService#listarTipoAcao()
     */
    public List<TipoAcaoSaidaDTO> listarTipoAcao() {
        ListarTipoAcaoRequest listarTipoAcaoRequest = new ListarTipoAcaoRequest();
        listarTipoAcaoRequest.setNumeroOcorrencia(IComboServiceConstants.NUMERO_OCORRENCIAS_LISTAR_TIPO_ACAO);
        listarTipoAcaoRequest.setCdSituacaoVinculacaoConta(0);

        ListarTipoAcaoResponse listarTipoAcaoResponse =
            getFactoryAdapter().getListarTipoAcaoPDCAdapter().invokeProcess(listarTipoAcaoRequest);

        TipoAcaoSaidaDTO tipoAcaoSaidaDTO;
        List<TipoAcaoSaidaDTO> listaTipoAcao = new ArrayList<TipoAcaoSaidaDTO>();
        for (int i = 0; i < listarTipoAcaoResponse.getOcorrenciasCount(); i++) {
            tipoAcaoSaidaDTO = new TipoAcaoSaidaDTO();
            tipoAcaoSaidaDTO.setCodMensagem(listarTipoAcaoResponse.getCodMensagem());
            tipoAcaoSaidaDTO.setMensagem(listarTipoAcaoResponse.getMensagem());
            tipoAcaoSaidaDTO.setCdAcao(listarTipoAcaoResponse.getOcorrencias(i).getCdAcao());
            tipoAcaoSaidaDTO.setDsAcao(listarTipoAcaoResponse.getOcorrencias(i).getDsAcao());
            listaTipoAcao.add(tipoAcaoSaidaDTO);
        }

        return listaTipoAcao;
    }

    /**
     * (non-Javadoc).
     * 
     * @param entradaDTO
     *            the entrada dto
     * @return the list< listar operacoes servico saida dt o>
     * @see br.com.bradesco.web.pgit.service.business.combo.IComboService#listarOperacoesServicos(br.com.bradesco.web.pgit.service.business.combo.bean.ListarOperacoesServicosEntradaDTO)
     */
    public List<ListarOperacoesServicoSaidaDTO> listarOperacoesServicos(ListarOperacoesServicosEntradaDTO entradaDTO) {
        ListarOperacoesServicoRequest request = new ListarOperacoesServicoRequest();

        request.setNrOcorrencias(IComboServiceConstants.NUMERO_OCORRENCIAS_LISTAR_OPERACOES_SERVICOS);
        request.setCdProduto(entradaDTO.getCdProduto());

        ListarOperacoesServicoResponse response =
            getFactoryAdapter().getListarOperacoesServicoPDCAdapter().invokeProcess(request);
        List<ListarOperacoesServicoSaidaDTO> list = new ArrayList<ListarOperacoesServicoSaidaDTO>();

        for (br.com.bradesco.web.pgit.service.data.pdc.listaroperacoesservico.response.Ocorrencias ocorrencia : response
            .getOcorrencias()) {
            list.add(new ListarOperacoesServicoSaidaDTO(ocorrencia.getCdProduto(), ocorrencia.getDsOperacao()));

        }
        return list;
    }

    /**
     * (non-Javadoc).
     * 
     * @return the list< listar finalidade endereco saida dt o>
     * @see br.com.bradesco.web.pgit.service.business.combo.IComboService#listarFinalidadeEndereco()
     */
    public List<ListarFinalidadeEnderecoSaidaDTO> listarFinalidadeEndereco() {

        br.com.bradesco.web.pgit.service.data.pdc.listarfinalidadeendereco.request.ListarFinalidadeEnderecoRequest listarFinalidadeEnderecoRequest =
            new ListarFinalidadeEnderecoRequest();
        br.com.bradesco.web.pgit.service.data.pdc.listarfinalidadeendereco.response.ListarFinalidadeEnderecoResponse listarFinalidadeEnderecoResponse =
            new ListarFinalidadeEnderecoResponse();

        listarFinalidadeEnderecoRequest.setCdSituacaoVinculacaoConta(0);
        listarFinalidadeEnderecoRequest
            .setNumeroOcorrencias(IComboServiceConstants.NUMERO_OCORRENCIAS_LISTAR_FINALIDADE_ENDERECO);

        listarFinalidadeEnderecoResponse =
            getFactoryAdapter().getListarFinalidadeEnderecoPDCAdapter().invokeProcess(listarFinalidadeEnderecoRequest);

        if (listarFinalidadeEnderecoResponse == null) {
            return new ArrayList();
        }

        List<ListarFinalidadeEnderecoSaidaDTO> listaRetorno = new ArrayList<ListarFinalidadeEnderecoSaidaDTO>();

        for (br.com.bradesco.web.pgit.service.data.pdc.listarfinalidadeendereco.response.Ocorrencias ocorrencia : listarFinalidadeEnderecoResponse
            .getOcorrencias()) {
            listaRetorno.add(new ListarFinalidadeEnderecoSaidaDTO(ocorrencia.getCdTipoFinalidade(), ocorrencia
                .getDsTipoFinalidade()));

        }

        return listaRetorno;
    }

    /**
     * (non-Javadoc).
     * 
     * @return the list< listar tipo conta saida dt o>
     * @see br.com.bradesco.web.pgit.service.business.combo.IComboService#listarTipoConta()
     */
    public List<ListarTipoContaSaidaDTO> listarTipoConta() {

        ListarTipoContaRequest request = new ListarTipoContaRequest();
        List<ListarTipoContaSaidaDTO> listaTipoConta = new ArrayList<ListarTipoContaSaidaDTO>();

        request.setCdSituacao(0);
        request.setNrOcorrencias(30);

        ListarTipoContaResponse response = getFactoryAdapter().getListarTipoContaPDCAdapter().invokeProcess(request);

        for (br.com.bradesco.web.pgit.service.data.pdc.listartipoconta.response.Ocorrencias ocorrencia : response
            .getOcorrencias()) {
            listaTipoConta.add(new ListarTipoContaSaidaDTO(ocorrencia));
        }

        return listaTipoConta;
    }

    /**
     * (non-Javadoc).
     * 
     * @param listarParticipantesEntradaDTO
     *            the listar participantes entrada dto
     * @return the list< listar participantes saida dt o>
     * @see br.com.bradesco.web.pgit.service.business.combo.IComboService#listarParticipantes(br.com.bradesco.web.pgit.service.business.combo.bean.ListarParticipantesEntradaDTO)
     */
    public List<ListarParticipantesSaidaDTO> listarParticipantes(
        ListarParticipantesEntradaDTO listarParticipantesEntradaDTO) {

        ListarParticipantesRequest request = new ListarParticipantesRequest();
        List<ListarParticipantesSaidaDTO> listaParticipantes = new ArrayList<ListarParticipantesSaidaDTO>();

        request.setCdPessoaJuridica(listarParticipantesEntradaDTO.getCdPessoaJuridica());
        request.setCdTipoContrato(listarParticipantesEntradaDTO.getCdTipoContrato());
        request.setNrSequenciaContrato(listarParticipantesEntradaDTO.getNrSequenciaContrato());
        request.setNrOcorrencias(IComboServiceConstants.NUMERO_OCORRENCIAS_LISTAR_PARTICIPANTES);

        ListarParticipantesResponse response =
            getFactoryAdapter().getListarParticipantesPDCAdapter().invokeProcess(request);

        for (br.com.bradesco.web.pgit.service.data.pdc.listarparticipantes.response.Ocorrencias ocorrencia : response
            .getOcorrencias()) {
            listaParticipantes.add(new ListarParticipantesSaidaDTO(ocorrencia));
        }

        return listaParticipantes;
    }

    /**
     * (non-Javadoc).
     * 
     * @return the list< idioma saida dt o>
     * @see br.com.bradesco.web.pgit.service.business.combo.IComboService#listarIdioma()
     */
    public List<IdiomaSaidaDTO> listarIdioma() {
        ListarIdiomaRequest request = new ListarIdiomaRequest();
        List<IdiomaSaidaDTO> listaIdiomas = new ArrayList<IdiomaSaidaDTO>();

        request.setCdSituacaoVinculacaoConta(0);
        request.setNumeroOcorrencias(IComboServiceConstants.NUMERO_OCORRENCIAS_LISTAR_IDIOMA);

        ListarIdiomaResponse response = getFactoryAdapter().getListarIdiomaPDCAdapter().invokeProcess(request);

        for (br.com.bradesco.web.pgit.service.data.pdc.listaridioma.response.Ocorrencias ocorrencia : response
            .getOcorrencias()) {
            listaIdiomas.add(new IdiomaSaidaDTO(ocorrencia.getCdIdioma(), ocorrencia.getDsIdioma()));
        }

        return listaIdiomas;
    }

    /**
     * (non-Javadoc).
     * 
     * @return the list< listar recurso canal msg saida dt o>
     * @see br.com.bradesco.web.pgit.service.business.combo.IComboService#listarRecursoCanalMsg()
     */
    public List<ListarRecursoCanalMsgSaidaDTO> listarRecursoCanalMsg() {

        ListarRecursoCanalMsgRequest request = new ListarRecursoCanalMsgRequest();
        List<ListarRecursoCanalMsgSaidaDTO> listaRecursos = new ArrayList<ListarRecursoCanalMsgSaidaDTO>();

        request.setCdSituacaoVinculacaoConta(0);
        request.setNrOcorrencias(IComboServiceConstants.NUMERO_OCORRENCIAS_LISTAR_RECURSO_CANAL_MSG);

        ListarRecursoCanalMsgResponse response =
            getFactoryAdapter().getListarRecursoCanalMsgPDCAdapter().invokeProcess(request);

        for (br.com.bradesco.web.pgit.service.data.pdc.listarrecursocanalmsg.response.Ocorrencias ocorrencia : response
            .getOcorrencias()) {
            listaRecursos.add(new ListarRecursoCanalMsgSaidaDTO(ocorrencia.getCdTipoCanal(), ocorrencia
                .getDsTipoCanal()));
        }

        return listaRecursos;
    }

    /**
     * (non-Javadoc).
     * 
     * @return the list< listar situacao solicitacao saida dt o>
     * @see br.com.bradesco.web.pgit.service.business.combo.IComboService#listarSituacaoSolicitacao()
     */
    public List<ListarSituacaoSolicitacaoSaidaDTO> listarSituacaoSolicitacao() {

        ListarSituacaoSolicitacaoRequest request = new ListarSituacaoSolicitacaoRequest();

        request.setCdSituacaoVinculacaoConta(0);
        request.setNumeroOcorrencias(30);

        ListarSituacaoSolicitacaoResponse response =
            getFactoryAdapter().getListarSituacaoSolicitacaoPDCAdapter().invokeProcess(request);

        List<ListarSituacaoSolicitacaoSaidaDTO> list = new ArrayList<ListarSituacaoSolicitacaoSaidaDTO>();

        for (br.com.bradesco.web.pgit.service.data.pdc.listarsituacaosolicitacao.response.Ocorrencias saida : response
            .getOcorrencias()) {
            list.add(new ListarSituacaoSolicitacaoSaidaDTO(saida.getCdSolicitacaoPagamentoIntegrado(), saida
                .getDsTipoSolicitacaoPagamento()));
        }

        return list;
    }

    /**
     * (non-Javadoc).
     * 
     * @return the list< listar forma lanc cnab saida dt o>
     * @see br.com.bradesco.web.pgit.service.business.combo.IComboService#listarFormaLancCnab()
     */
    public List<ListarFormaLancCnabSaidaDTO> listarFormaLancCnab() {

        ListarFormaLancCnabRequest request = new ListarFormaLancCnabRequest();

        request.setCdSituacaoVinculacaoConta(0);
        request.setNumeroOcorrencias(30);

        ListarFormaLancCnabResponse response =
            getFactoryAdapter().getListarFormaLancCnabPDCAdapter().invokeProcess(request);

        List<ListarFormaLancCnabSaidaDTO> list = new ArrayList<ListarFormaLancCnabSaidaDTO>();

        for (br.com.bradesco.web.pgit.service.data.pdc.listarformalanccnab.response.Ocorrencias saida : response
            .getOcorrencias()) {
            list
                .add(new ListarFormaLancCnabSaidaDTO(saida.getCdFormaLancamentoCnab(), saida.getDsFormaLancamentoCnab()));
        }

        return list;
    }

    /**
     * (non-Javadoc).
     * 
     * @return the list< listar servico cnab saida dt o>
     * @see br.com.bradesco.web.pgit.service.business.combo.IComboService#listarServicoCnab()
     */
    public List<ListarServicoCnabSaidaDTO> listarServicoCnab() {

        ListarServicoCnabRequest request = new ListarServicoCnabRequest();

        request.setCdSituacaoVinculacaoConta(0);
        request.setNumeroOcorrencias(30);

        ListarServicoCnabResponse response =
            getFactoryAdapter().getListarServicoCnabPDCAdapter().invokeProcess(request);

        List<ListarServicoCnabSaidaDTO> list = new ArrayList<ListarServicoCnabSaidaDTO>();

        for (br.com.bradesco.web.pgit.service.data.pdc.listarservicocnab.response.Ocorrencias saida : response
            .getOcorrencias()) {
            list.add(new ListarServicoCnabSaidaDTO(saida.getCdTipoServicoCnab(), saida.getDsTipoServicoCnab()));
        }

        return list;
    }

    /**
     * (non-Javadoc).
     * 
     * @return the list< listar situacao contrato saida dt o>
     * @see br.com.bradesco.web.pgit.service.business.combo.IComboService#listarSituacaoContrato()
     */
    public List<ListarSituacaoContratoSaidaDTO> listarSituacaoContrato() {

        ListarSituacaoContratoRequest request = new ListarSituacaoContratoRequest();

        request.setCdSituacaoVinculacaoConta(0);
        request.setNumeroOcorrencias(IComboServiceConstants.NUMERO_OCORRENCIAS_LISTAR_SITUACAO_CONTRATO);

        ListarSituacaoContratoResponse response =
            getFactoryAdapter().getListarSituacaoContratoPDCAdapter().invokeProcess(request);

        List<ListarSituacaoContratoSaidaDTO> list = new ArrayList<ListarSituacaoContratoSaidaDTO>();

        for (br.com.bradesco.web.pgit.service.data.pdc.listarsituacaocontrato.response.Ocorrencias saida : response
            .getOcorrencias()) {
            list.add(new ListarSituacaoContratoSaidaDTO(saida.getCdSituacaoContratoNegocio(), saida
                .getDsSituacaoContratoNegocio()));
        }

        return list;
    }

    /**
     * (non-Javadoc).
     * 
     * @param entradaDTO
     *            the entrada dto
     * @return the list< listar motivo situacao contrato saida dt o>
     * @see br.com.bradesco.web.pgit.service.business.combo.IComboService#listarMotivoSituacaoContrato(br.com.bradesco.web.pgit.service.business.combo.bean.ListarMotivoSituacaoContratoEntradaDTO)
     */
    public List<ListarMotivoSituacaoContratoSaidaDTO> listarMotivoSituacaoContrato(
        ListarMotivoSituacaoContratoEntradaDTO entradaDTO) {

        ListarMotivoSituacaoContratoRequest request = new ListarMotivoSituacaoContratoRequest();

        request.setCdSituacao(entradaDTO.getCdSituacao());
        request.setNrOcorrencias(IComboServiceConstants.NUMERO_OCORRENCIAS_LISTAR_MOTIVO_SITUACAO_CONTRATO);

        ListarMotivoSituacaoContratoResponse response =
            getFactoryAdapter().getListarMotivoSituacaoContratoPDCAdapter().invokeProcess(request);

        List<ListarMotivoSituacaoContratoSaidaDTO> list = new ArrayList<ListarMotivoSituacaoContratoSaidaDTO>();

        for (br.com.bradesco.web.pgit.service.data.pdc.listarmotivosituacaocontrato.response.Ocorrencias saida : response
            .getOcorrencias()) {
            list.add(new ListarMotivoSituacaoContratoSaidaDTO(saida.getCdMotivoSituacaoOper(), saida
                .getDsMotivoSituacaoOper()));
        }

        return list;
    }

    /**
     * (non-Javadoc).
     * 
     * @param entradaDTO
     *            the entrada dto
     * @return the list< listar motivo situacao vinc contrato saida dt o>
     * @see br.com.bradesco.web.pgit.service.business.combo.IComboService#listarMotivoSituacaoVincContrato(br.com.bradesco.web.pgit.service.business.combo.bean.ListarMotivoSituacaoVincContratoEntradaDTO)
     */
    public List<ListarMotivoSituacaoVincContratoSaidaDTO> listarMotivoSituacaoVincContrato(
        ListarMotivoSituacaoVincContratoEntradaDTO entradaDTO) {

        ListarMotivoSituacaoVincContratoRequest request = new ListarMotivoSituacaoVincContratoRequest();

        request.setCdSituacaoVinculacaoConta(entradaDTO.getCdSituacaoVincConta());
        request.setNumeroOcorrencias(IComboServiceConstants.NUMERO_OCORRENCIAS_LISTAR_MOTIVO_SITUACAO_CONTRATO);

        ListarMotivoSituacaoVincContratoResponse response =
            getFactoryAdapter().getListarMotivoSituacaoVincContratoPDCAdapter().invokeProcess(request);

        List<ListarMotivoSituacaoVincContratoSaidaDTO> list = new ArrayList<ListarMotivoSituacaoVincContratoSaidaDTO>();

        for (br.com.bradesco.web.pgit.service.data.pdc.listarmotivosituacaovinccontrato.response.Ocorrencias saida : response
            .getOcorrencias()) {
            list.add(new ListarMotivoSituacaoVincContratoSaidaDTO(saida.getCdMotivoSituacaoContrato(), saida
                .getDsMotivoSituacaoContrato()));
        }

        return list;
    }

    /**
     * (non-Javadoc).
     * 
     * @param entrada
     *            the entrada
     * @return the list< listar tipo mensagem saida dt o>
     * @see br.com.bradesco.web.pgit.service.business.combo.IComboService#listarTipoMensagem(br.com.bradesco.web.pgit.service.business.combo.bean.ListarTipoMensagemEntradaDTO)
     */
    public List<ListarTipoMensagemSaidaDTO> listarTipoMensagem(ListarTipoMensagemEntradaDTO entrada) {
        ListarMsgComprovanteSalrlRequest request = new ListarMsgComprovanteSalrlRequest();

        request.setCdControleMensagemSalarial(entrada.getCdControleMensagemSalarial());
        request.setCdTipoComprovanteSalarial(entrada.getCdTipoComprovanteSalarial());
        request.setCdTipoMensagemSalarial(entrada.getCdTipoMensagemSalarial());
        request.setCdRestMensagemSalarial(entrada.getCdRestMensagemSalarial());

        request.setNumeroOcorrencias(30);

        ListarMsgComprovanteSalrlResponse response =
            getFactoryAdapter().getListarMsgComprovanteSalrlPDCAdapter().invokeProcess(request);

        List<ListarTipoMensagemSaidaDTO> list = new ArrayList<ListarTipoMensagemSaidaDTO>();

        for (br.com.bradesco.web.pgit.service.data.pdc.listarmsgcomprovantesalrl.response.Ocorrencias saida : response
            .getOcorrencias()) {
            list
                .add(new ListarTipoMensagemSaidaDTO(saida.getCdControleMensagemSalarial(), saida
                    .getCdTipoComprovanteSalarial(), saida.getDtTipoComprovanteSalarial(), saida
                    .getCdTipoMensagemSalarial(), saida.getDsControleMensagemSalarial(), saida
                    .getCdRestMensagemSalarial()));
        }

        return list;
    }

    /**
     * (non-Javadoc).
     * 
     * @param entrada
     *            the entrada
     * @return the list< listar municipio saida dt o>
     * @see br.com.bradesco.web.pgit.service.business.combo.IComboService#listarMunicipio(br.com.bradesco.web.pgit.service.business.combo.bean.ListarMunicipioEntradaDTO)
     */
    public List<ListarMunicipioSaidaDTO> listarMunicipio(ListarMunicipioEntradaDTO entrada) {
        List<ListarMunicipioSaidaDTO> saida = new ArrayList<ListarMunicipioSaidaDTO>();

        ListarMunicipioRequest request = new ListarMunicipioRequest();
        ListarMunicipioResponse response = new ListarMunicipioResponse();

        request.setCdUf(PgitUtil.verificaIntegerNulo(entrada.getCdUf()));
        request.setNrOcorrencias(300);

        response = getFactoryAdapter().getListarMunicipioPDCAdapter().invokeProcess(request);

        ListarMunicipioSaidaDTO municipio;
        for (int i = 0; i < response.getOcorrenciasCount(); i++) {
            municipio = new ListarMunicipioSaidaDTO();
            municipio.setCdMunicipio(response.getOcorrencias(i).getCdMunicipio());
            municipio.setDsMunicipio(response.getOcorrencias(i).getDsMunicipio());
            saida.add(municipio);
        }

        return saida;

    }

    /**
     * (non-Javadoc).
     * 
     * @return the list< listar unidade federativa saida dt o>
     * @see br.com.bradesco.web.pgit.service.business.combo.IComboService#listarUnidadeFederativa()
     */
    public List<ListarUnidadeFederativaSaidaDTO> listarUnidadeFederativa() {
        ListarUnidadeFederativaRequest request = new ListarUnidadeFederativaRequest();
        request.setCdSituacaoVinculacaoConta(0);
        request.setNumeroOcorrencias(30);

        ListarUnidadeFederativaResponse response =
            getFactoryAdapter().getListarUnidadeFederativaPDCAdapter().invokeProcess(request);

        List<ListarUnidadeFederativaSaidaDTO> list = new ArrayList<ListarUnidadeFederativaSaidaDTO>();

        for (br.com.bradesco.web.pgit.service.data.pdc.listarunidadefederativa.response.Ocorrencias saida : response
            .getOcorrencias()) {
            list
                .add(new ListarUnidadeFederativaSaidaDTO(saida.getCdUnidadeFederativa(), saida.getDsUnidadeFederativa()));
        }

        return list;
    }

    /**
     * (non-Javadoc).
     * 
     * @return the list< listar situacao pend contrato saida dt o>
     * @see br.com.bradesco.web.pgit.service.business.combo.IComboService#listarSituacaoPendContrato()
     */
    public List<ListarSituacaoPendContratoSaidaDTO> listarSituacaoPendContrato() {

        ListarSituacaoPendContratoRequest request = new ListarSituacaoPendContratoRequest();

        request.setCdSituacaoVinculacaoConta(0);
        request.setNumeroOcorrencias(30);

        ListarSituacaoPendContratoResponse response =
            getFactoryAdapter().getListarSituacaoPendContratoPDCAdapter().invokeProcess(request);

        List<ListarSituacaoPendContratoSaidaDTO> list = new ArrayList<ListarSituacaoPendContratoSaidaDTO>();

        for (br.com.bradesco.web.pgit.service.data.pdc.listarsituacaopendcontrato.response.Ocorrencias saida : response
            .getOcorrencias()) {
            list.add(new ListarSituacaoPendContratoSaidaDTO(saida.getCdSituacaoPendenciaContrato(), saida
                .getRsSituacaoPendenciaContrato()));
        }

        return list;
    }

    /**
     * (non-Javadoc).
     * 
     * @return the list< listar finalidade conta saida dt o>
     * @see br.com.bradesco.web.pgit.service.business.combo.IComboService#listarFinalidadeConta()
     */
    public List<ListarFinalidadeContaSaidaDTO> listarFinalidadeConta() {
        ListarFinalidadeContaRequest request = new ListarFinalidadeContaRequest();
        request.setNumeroOcorrencias(30);
        request.setCdSituacaoVinculacaoConta(0);

        ListarFinalidadeContaResponse response =
            getFactoryAdapter().getListarFinalidadeContaPDCAdapter().invokeProcess(request);
        List<ListarFinalidadeContaSaidaDTO> list = new ArrayList<ListarFinalidadeContaSaidaDTO>();

        for (br.com.bradesco.web.pgit.service.data.pdc.listarfinalidadeconta.response.Ocorrencias saida : response
            .getOcorrencias()) {
            list.add(new ListarFinalidadeContaSaidaDTO(saida.getCdFinalidadeContaPagamento(), saida
                .getRsFinalidadeContaPagamento()));
        }

        return list;
    }

    /**
     * (non-Javadoc).
     * 
     * @return the list< listar tipo pendencia saida dt o>
     * @see br.com.bradesco.web.pgit.service.business.combo.IComboService#listarTipoPendencia()
     */
    public List<ListarTipoPendenciaSaidaDTO> listarTipoPendencia() {

        ListarTipoPendenciaRequest request = new ListarTipoPendenciaRequest();

        request.setCdSituacaoVinculacaoConta(0);
        request.setNrOcorrencias(30);

        ListarTipoPendenciaResponse response =
            getFactoryAdapter().getListarTipoPendenciaPDCAdapter().invokeProcess(request);

        List<ListarTipoPendenciaSaidaDTO> list = new ArrayList<ListarTipoPendenciaSaidaDTO>();

        for (br.com.bradesco.web.pgit.service.data.pdc.listartipopendencia.response.Ocorrencias saida : response
            .getOcorrencias()) {
            list.add(new ListarTipoPendenciaSaidaDTO(saida.getCdPendenciaPagamentoIntegrado(), saida
                .getDsPendenciaPagamentoIntegrado()));
        }

        return list;
    }

    /**
     * (non-Javadoc).
     * 
     * @param motivoSituacaoPendenciaContratoEntradaDTO
     *            the motivo situacao pendencia contrato entrada dto
     * @return the list< motivo situacao pendencia contrato saida dt o>
     * @see br.com.bradesco.web.pgit.service.business.combo.IComboService#listarMotivoSituacaoPendContrato(br.com.bradesco.web.pgit.service.business.combo.bean.MotivoSituacaoPendenciaContratoEntradaDTO)
     */
    public List<MotivoSituacaoPendenciaContratoSaidaDTO> listarMotivoSituacaoPendContrato(
        MotivoSituacaoPendenciaContratoEntradaDTO motivoSituacaoPendenciaContratoEntradaDTO) {

        ListarMotivoSituacaoPendContratoRequest request = new ListarMotivoSituacaoPendContratoRequest();

        request.setCdSituacaoPendenciaContrato(motivoSituacaoPendenciaContratoEntradaDTO
            .getCdSituacaoPendenciaContrato());
        request.setNumeroOcorrencias(20);

        ListarMotivoSituacaoPendContratoResponse response =
            getFactoryAdapter().getListarMotivoSituacaoPendContratoPDCAdapter().invokeProcess(request);

        List<MotivoSituacaoPendenciaContratoSaidaDTO> list = new ArrayList<MotivoSituacaoPendenciaContratoSaidaDTO>();

        for (br.com.bradesco.web.pgit.service.data.pdc.listarmotivosituacaopendcontrato.response.Ocorrencias saida : response
            .getOcorrencias()) {
            list.add(new MotivoSituacaoPendenciaContratoSaidaDTO(saida.getCdMotivoSituacaoPendencia(), saida
                .getDsMotivoSituacaoPendencia()));
        }

        return list;
    }

    /**
     * (non-Javadoc).
     * 
     * @return the list< lista servico composto saida dt o>
     * @see br.com.bradesco.web.pgit.service.business.combo.IComboService#listarServicoComposto()
     */
    public List<ListaServicoCompostoSaidaDTO> listarServicoComposto() {

        ListarServicoCompostoRequest request = new ListarServicoCompostoRequest();

        request.setCdFormaLancamentoCnab(0);
        request.setCdFormaLiquidacao(0);
        request.setCdServicoCompostoPagamento(0);
        request.setCdTipoServicoCnab(0);
        request.setDsServicoCompostoPagamento("");
        request.setQtDiaLctoFuturo(0);
        request.setCdIndLctoFuturo(0);
        request.setQtOcorrencias(50);

        ListarServicoCompostoResponse response =
            getFactoryAdapter().getListarServicoCompostoPDCAdapter().invokeProcess(request);

        List<ListaServicoCompostoSaidaDTO> lista = new ArrayList<ListaServicoCompostoSaidaDTO>();

        for (br.com.bradesco.web.pgit.service.data.pdc.listarservicocomposto.response.Ocorrencias saida : response
            .getOcorrencias()) {
            lista.add(new ListaServicoCompostoSaidaDTO(saida.getCdServicoCompostoPagamento(), saida
                .getDsServicoCompostoPagamento()));
        }

        return lista;
    }

    /**
     * (non-Javadoc).
     * 
     * @return the list< listar servicos saida dt o>
     * @see br.com.bradesco.web.pgit.service.business.combo.IComboService#listarTipoServicosRelacionados()
     */
    public List<ListarServicosSaidaDTO> listarTipoServicosRelacionados() {

        ListarServicoRelacionadoCdpsRequest request = new ListarServicoRelacionadoCdpsRequest();

        request.setCdProdutoServicoOperacao(0);
        request.setCdProdutoOperacaoRelacionado(0);
        request.setCdRelacionamentoProduto(0);
        request.setNrOcorrencias(25);

        ListarServicoRelacionadoCdpsResponse response =
            getFactoryAdapter().getListarServicoRelacionadoCdpsPDCAdapter().invokeProcess(request);

        List<ListarServicosSaidaDTO> lista = new ArrayList<ListarServicosSaidaDTO>();

        for (br.com.bradesco.web.pgit.service.data.pdc.listarservicorelacionadocdps.response.Ocorrencias saida : response
            .getOcorrencias()) {
            lista.add(new ListarServicosSaidaDTO(saida.getCdProdutoOperacaoRelacionado(), saida
                .getDsProdutoOperacaoRelacionado()));
        }

        return lista;
    }

    /**
     * (non-Javadoc).
     * 
     * @param entrada
     *            the entrada
     * @return the list< listar servicos emissao saida dt o>
     * @see br.com.bradesco.web.pgit.service.business.combo.IComboService#listarServicosEmissao(br.com.bradesco.web.pgit.service.business.combo.bean.ListarServicosEmissaoEntradaDTO)
     */
    public List<ListarServicosEmissaoSaidaDTO> listarServicosEmissao(ListarServicosEmissaoEntradaDTO entrada) {
        List<ListarServicosEmissaoSaidaDTO> lista = new ArrayList<ListarServicosEmissaoSaidaDTO>();
        ListarServicosEmissaoRequest request = new ListarServicosEmissaoRequest();
        ListarServicosEmissaoResponse response = new ListarServicosEmissaoResponse();

        request.setNrOcorrencias(30);
        request.setCdpessoaJuridicaContrato(PgitUtil.verificaLongNulo(entrada.getCdPessoaJuridicaContrato()));
        request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(entrada.getCdTipoContratoNegocio()));
        request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(entrada.getNumSequenciaContratoNegocio()));

        response = getFactoryAdapter().getListarServicosEmissaoPDCAdapter().invokeProcess(request);

        if (response == null) {
            return lista;
        }

        for (br.com.bradesco.web.pgit.service.data.pdc.listarservicosemissao.response.Ocorrencias saida : response
            .getOcorrencias()) {
            lista.add(new ListarServicosEmissaoSaidaDTO(saida));
        }

        return lista;
    }

    /**
     * (non-Javadoc).
     * 
     * @param entradaDTO
     *            the entrada dto
     * @return the list< listar modalidade saida dt o>
     * @see br.com.bradesco.web.pgit.service.business.combo.IComboService#listarModalidadesRealcionados(br.com.bradesco.web.pgit.service.business.combo.impl.bean.ListarModalidadesEntradaDTO)
     */
    public List<ListarModalidadeSaidaDTO> listarModalidadesRealcionados(ListarModalidadesEntradaDTO entradaDTO) {
        ListarServicoRelacionadoCdpsRequest request = new ListarServicoRelacionadoCdpsRequest();

        request.setCdProdutoServicoOperacao(entradaDTO.getCdServico());
        request.setCdProdutoOperacaoRelacionado(0);
        request.setCdRelacionamentoProduto(0);
        request.setNrOcorrencias(25);

        ListarServicoRelacionadoCdpsResponse response =
            getFactoryAdapter().getListarServicoRelacionadoCdpsPDCAdapter().invokeProcess(request);

        List<ListarModalidadeSaidaDTO> lista = new ArrayList<ListarModalidadeSaidaDTO>();

        for (br.com.bradesco.web.pgit.service.data.pdc.listarservicorelacionadocdps.response.Ocorrencias saida : response
            .getOcorrencias()) {
            lista.add(new ListarModalidadeSaidaDTO(saida.getCdProdutoOperacaoRelacionado(), saida
                .getDsProdutoOperacaoRelacionado(), saida.getCdRelacionamentoProduto()));
        }

        return lista;
    }

    /*
     * (non-Javadoc)
     * 
     * @see br.com.bradesco.web.pgit.service.business.combo.IComboService#listarServicoRelacionadoPgit
     * (br.com.bradesco.web.pgit.service.business.combo.bean.ListarServicoRelacionadoPgitEntradaDTO)
     */
    public ListarServicoRelacionadoPgitSaidaDTO listarServicoRelacionadoPgit(
        ListarServicoRelacionadoPgitEntradaDTO entrada) {
        ListarServicoRelacionadoPgitRequest request = new ListarServicoRelacionadoPgitRequest();

        request.setCdNaturezaServico(verificaIntegerNulo(entrada.getCdNaturezaServico()));
        request.setMaxOcorrencias(verificaIntegerNulo(entrada.getMaxOcorrencias()));

        ListarServicoRelacionadoPgitResponse response =
            getFactoryAdapter().getListarServicoRelacionadoPgitPDCAdapter().invokeProcess(request);

        ListarServicoRelacionadoPgitSaidaDTO saidaDto = new ListarServicoRelacionadoPgitSaidaDTO();

        saidaDto.setCodMensagem(response.getCodMensagem());
        saidaDto.setMensagem(response.getMensagem());
        saidaDto.setNrLinhas(response.getNrLinhas());

        List<ListarServicoRelacionadoPgitOcorrenciasSaidaDTO> lista =
            new ArrayList<ListarServicoRelacionadoPgitOcorrenciasSaidaDTO>();
        for (br.com.bradesco.web.pgit.service.data.pdc.listarservicorelacionadopgit.response.Ocorrencias saida : response
            .getOcorrencias()) {
            lista.add(new ListarServicoRelacionadoPgitOcorrenciasSaidaDTO(saida.getCdServico(), saida.getDsServico()));
        }

        saidaDto.setOcorrencias(lista);

        return saidaDto;
    }

    /*
     * (non-Javadoc)
     * 
     * @see br.com.bradesco.web.pgit.service.business.combo.IComboService#listarServicoRelacionadoPgitCdps
     * (br.com.bradesco.web.pgit.service.business.combo.bean.ListarServicoRelacionadoPgitCdpsEntradaDTO)
     */
    public ListarServicoRelacionadoPgitCdpsSaidaDTO listarServicoRelacionadoPgitCdps(
        ListarServicoRelacionadoPgitCdpsEntradaDTO entrada) {

        ListarServicoRelacionadoPgitCdpsRequest request = new ListarServicoRelacionadoPgitCdpsRequest();

        request.setCdProdutoServicoOperacao(0);
        request.setCdProdutoOperacaoRelacionado(0);
        request.setCdRelacionamentoProduto(0);
        request.setMaxOcorrencias(25);

        ListarServicoRelacionadoPgitCdpsResponse response =
            getFactoryAdapter().getListarServicoRelacionadoPgitCdpsPDCAdapter().invokeProcess(request);
        ListarServicoRelacionadoPgitCdpsSaidaDTO saidaDto = new ListarServicoRelacionadoPgitCdpsSaidaDTO();

        saidaDto.setCodMensagem(response.getCodMensagem());
        saidaDto.setMensagem(response.getMensagem());
        saidaDto.setNrLinhas(response.getNrLinhas());

        List<ListarServicoRelacionadoPgitCdpsOcorrenciasSaidaDTO> lista =
            new ArrayList<ListarServicoRelacionadoPgitCdpsOcorrenciasSaidaDTO>();
        for (br.com.bradesco.web.pgit.service.data.pdc.listarservicorelacionadopgitcdps.response.Ocorrencias saida : response
            .getOcorrencias()) {
            lista.add(new ListarServicoRelacionadoPgitCdpsOcorrenciasSaidaDTO(saida.getCdProdutoOperacaoRelacionado(),
                saida.getDsProdutoServicoRelacionado(), saida.getCdRelacionamentoProduto(), saida
                    .getDsRelacionamentoProduto(), saida.getCdTipoProdutoServico(), saida.getDsTipoProdutoServico()));
        }

        saidaDto.setOcorrencias(lista);

        return saidaDto;
    }

    /**
     * (non-Javadoc).
     * 
     * @param entradaDTO
     *            the entrada dto
     * @return the consulta mensagem saida dto
     * @see br.com.bradesco.web.pgit.service.business.combo.IComboService#consultarMensagem(br.com.bradesco.web.pgit.service.business.combo.bean.ConsultaMensagemEntradaDTO)
     */
    public ConsultaMensagemSaidaDTO consultarMensagem(ConsultaMensagemEntradaDTO entradaDTO) {
        ConsultarDescricaoMensagemRequest request = new ConsultarDescricaoMensagemRequest();
        request.setCdSistema(entradaDTO.getCdSistema());
        request.setNrEventoMensagemNegocio(entradaDTO.getNrEventoMensagemNegocio());
        request.setCdRecursoGeradorMensagem(entradaDTO.getCdRecursoGeradorMensagem());
        request.setCdIdiomaTextoMensagem(entradaDTO.getCdIdiomaTextoMensagem());

        ConsultarDescricaoMensagemResponse response =
            getFactoryAdapter().getConsultarDescricaoMensagemPDCAdapter().invokeProcess(request);

        if (response == null) {
            return null;
        }

        ConsultaMensagemSaidaDTO saida = new ConsultaMensagemSaidaDTO();
        saida.setNrEventoMensagemNegocio(response.getNrEventoMensagemNegocio());
        saida.setDsEventoMensagemNegocio(response.getDsEventoMensagemNegocio());

        return saida;
    }

    /**
     * (non-Javadoc).
     * 
     * @param listarIndiceEconomicoEntradaDTO
     *            the listar indice economico entrada dto
     * @return the list< listar indice economico saida dt o>
     * @see br.com.bradesco.web.pgit.service.business.combo.IComboService#listarIndiceEconomico(br.com.bradesco.web.pgit.service.business.combo.bean.ListarIndiceEconomicoEntradaDTO)
     */
    public List<ListarIndiceEconomicoSaidaDTO> listarIndiceEconomico(
        ListarIndiceEconomicoEntradaDTO listarIndiceEconomicoEntradaDTO) {

        ListarIndiceEconomicoRequest request = new ListarIndiceEconomicoRequest();
        request.setCdProdutoServicoOperacao(PgitUtil.verificaIntegerNulo(listarIndiceEconomicoEntradaDTO
            .getCdProdutoServicoOperacao()));
        request.setNrOcorrencias(30);

        ListarIndiceEconomicoResponse response =
            getFactoryAdapter().getListarIndiceEconomicoPDCAdapter().invokeProcess(request);

        List<ListarIndiceEconomicoSaidaDTO> lista = new ArrayList<ListarIndiceEconomicoSaidaDTO>();

        for (br.com.bradesco.web.pgit.service.data.pdc.listarindiceeconomico.response.Ocorrencias saida : response
            .getOcorrencias()) {
            lista.add(new ListarIndiceEconomicoSaidaDTO(saida.getCdIndicadorEconomico(), saida
                .getDsIndicadorEconomico(), saida.getCdProdutoServicoOperacao()));

        }

        return lista;
    }

    /**
     * (non-Javadoc).
     * 
     * @return the list< listar tipo vinculo saida dt o>
     * @see br.com.bradesco.web.pgit.service.business.combo.IComboService#listarTipoVinculo()
     */
    public List<ListarTipoVinculoSaidaDTO> listarTipoVinculo() {

        ListarTipoVinculoRequest request = new ListarTipoVinculoRequest();

        request.setCdSituacaoVinculacaoConta(0);
        request.setNumeroOcorrencias(30);

        ListarTipoVinculoResponse response =
            getFactoryAdapter().getListarTipoVinculoPDCAdapter().invokeProcess(request);

        List<ListarTipoVinculoSaidaDTO> lista = new ArrayList<ListarTipoVinculoSaidaDTO>();

        for (br.com.bradesco.web.pgit.service.data.pdc.listartipovinculo.response.Ocorrencias saida : response
            .getOcorrencias()) {
            lista.add(new ListarTipoVinculoSaidaDTO(saida.getCdTipoVinculo(), saida.getDsTipoVinculo()));
        }

        return lista;
    }

    /**
     * (non-Javadoc).
     * 
     * @return the list< listar situacao vinc contrato saida dt o>
     * @see br.com.bradesco.web.pgit.service.business.combo.IComboService#listarSituacaoVincContrato()
     */
    public List<ListarSituacaoVincContratoSaidaDTO> listarSituacaoVincContrato() {
        ListarSituacaoVincContratoRequest request = new ListarSituacaoVincContratoRequest();

        request.setCdSituacaoVinculacaoConta(0);
        request.setNumeroOcorrencias(30);

        ListarSituacaoVincContratoResponse response =
            getFactoryAdapter().getListarSituacaoVincContratoPDCAdapter().invokeProcess(request);

        List<ListarSituacaoVincContratoSaidaDTO> lista = new ArrayList<ListarSituacaoVincContratoSaidaDTO>();

        for (br.com.bradesco.web.pgit.service.data.pdc.listarsituacaovinccontrato.response.Ocorrencias saida : response
            .getOcorrencias()) {
            lista.add(new ListarSituacaoVincContratoSaidaDTO(saida.getCdSituacaoVinculacaoContrato(), saida
                .getDsSituacaoVinculacaoContrato()));
        }

        return lista;
    }

    /**
     * (non-Javadoc).
     * 
     * @param entrada
     *            the entrada
     * @return the list< listar motivo situacao serv contrato saida dt o>
     * @see br.com.bradesco.web.pgit.service.business.combo.IComboService#listarMotivoSituacaoServContrato(br.com.bradesco.web.pgit.service.business.combo.bean.ListarMotivoSituacaoServContratoEntradaDTO)
     */
    public List<ListarMotivoSituacaoServContratoSaidaDTO> listarMotivoSituacaoServContrato(
        ListarMotivoSituacaoServContratoEntradaDTO entrada) {

        ListarMotivoSituacaoServContratoRequest request = new ListarMotivoSituacaoServContratoRequest();

        request.setCdSituacaoVinculacaoConta(entrada.getCdSituacaoVinculacaoConta());
        request.setNumeroOcorrencias(30);

        ListarMotivoSituacaoServContratoResponse response =
            getFactoryAdapter().getListarMotivoSituacaoServContratoPDCAdapter().invokeProcess(request);

        List<ListarMotivoSituacaoServContratoSaidaDTO> lista =
            new ArrayList<ListarMotivoSituacaoServContratoSaidaDTO>();

        for (br.com.bradesco.web.pgit.service.data.pdc.listarmotivosituacaoservcontrato.response.Ocorrencias saida : response
            .getOcorrencias()) {
            lista.add(new ListarMotivoSituacaoServContratoSaidaDTO(saida.getCdMotivoSituacaoProduto(), saida
                .getDsMotivoSituacaoProduto()));
        }

        return lista;
    }

    /**
     * (non-Javadoc).
     * 
     * @param entrada
     *            the entrada
     * @return the list< listar motivo situacao aditivo cont saida dt o>
     * @see br.com.bradesco.web.pgit.service.business.combo.IComboService#listarMotivoSituacaoAditivoContrato(br.com.bradesco.web.pgit.service.business.combo.bean.ListarMotivoSituacaoAditivoContEntradaDTO)
     */
    public List<ListarMotivoSituacaoAditivoContSaidaDTO> listarMotivoSituacaoAditivoContrato(
        ListarMotivoSituacaoAditivoContEntradaDTO entrada) {
        ListarMotivoSituacaoAditivoContratoRequest request = new ListarMotivoSituacaoAditivoContratoRequest();

        request.setCdSituacao(entrada.getCdSituacao());
        request.setNrOcorrencias(30);

        ListarMotivoSituacaoAditivoContratoResponse response =
            getFactoryAdapter().getListarMotivoSituacaoAditivoContratoPDCAdapter().invokeProcess(request);

        List<ListarMotivoSituacaoAditivoContSaidaDTO> lista = new ArrayList<ListarMotivoSituacaoAditivoContSaidaDTO>();

        for (br.com.bradesco.web.pgit.service.data.pdc.listarmotivosituacaoaditivocontrato.response.Ocorrencias saida : response
            .getOcorrencias()) {
            lista.add(new ListarMotivoSituacaoAditivoContSaidaDTO(saida.getCdMotivoSituacaoOperacao(), saida
                .getDsMotivoSituacaoOperacao()));
        }

        return lista;
    }

    /**
     * (non-Javadoc).
     * 
     * @param entrada
     *            the entrada
     * @return the list< listar meio transmissao saida dt o>
     * @see br.com.bradesco.web.pgit.service.business.combo.IComboService#listarMeioTransmissao(br.com.bradesco.web.pgit.service.business.combo.bean.ListarMeioTransmissaoEntradaDTO)
     */
    public List<ListarMeioTransmissaoSaidaDTO> listarMeioTransmissao(ListarMeioTransmissaoEntradaDTO entrada) {
        ListarMeioTransmissaoRequest request = new ListarMeioTransmissaoRequest();

        request.setCdSituacaoVinculacaoConta(entrada.getCdSituacaoVinculacaoConta());
        request.setNumeroOcorrencias(30);

        ListarMeioTransmissaoResponse response =
            getFactoryAdapter().getListarMeioTransmissaoPDCAdapter().invokeProcess(request);

        List<ListarMeioTransmissaoSaidaDTO> lista = new ArrayList<ListarMeioTransmissaoSaidaDTO>();

        for (br.com.bradesco.web.pgit.service.data.pdc.listarmeiotransmissao.response.Ocorrencias saida : response
            .getOcorrencias()) {
            lista.add(new ListarMeioTransmissaoSaidaDTO(saida.getCdTipoMeioTransmissao(), saida
                .getDsTipoMeioTransmissao()));
        }

        return lista;
    }

    /**
     * (non-Javadoc).
     * 
     * @return the list< listar situacao aditivo contrato saida dt o>
     * @see br.com.bradesco.web.pgit.service.business.combo.IComboService#listarSituacaoAditivoContrato()
     */
    public List<ListarSituacaoAditivoContratoSaidaDTO> listarSituacaoAditivoContrato() {
        ListarSituacaoAditivoContratoRequest request = new ListarSituacaoAditivoContratoRequest();

        request.setCdSituacaoVinculacaoConta(0);
        request.setNrOcorrencias(30);

        ListarSituacaoAditivoContratoResponse response =
            getFactoryAdapter().getListarSituacaoAditivoContratoPDCAdapter().invokeProcess(request);

        List<ListarSituacaoAditivoContratoSaidaDTO> lista = new ArrayList<ListarSituacaoAditivoContratoSaidaDTO>();

        for (br.com.bradesco.web.pgit.service.data.pdc.listarsituacaoaditivocontrato.response.Ocorrencias saida : response
            .getOcorrencias()) {
            lista.add(new ListarSituacaoAditivoContratoSaidaDTO(saida.getCdSituacaContratoNegocio(), saida
                .getDsSituacaoContratoNegocio()));
        }

        return lista;
    }

    /**
     * (non-Javadoc).
     * 
     * @return the list< listar situacao sol consistencia previa saida dt o>
     * @see br.com.bradesco.web.pgit.service.business.combo.IComboService#listarSituacaoSolConsistenciaPrevia()
     */
    public List<ListarSituacaoSolConsistenciaPreviaSaidaDTO> listarSituacaoSolConsistenciaPrevia() {
        ConsultarSituacaoSolConsistenciaPreviaRequest request = new ConsultarSituacaoSolConsistenciaPreviaRequest();

        request.setCdSituacao(0);
        request.setNumeroOcorrencias(100);

        ConsultarSituacaoSolConsistenciaPreviaResponse response =
            getFactoryAdapter().getConsultarSituacaoSolConsistenciaPreviaPDCAdapter().invokeProcess(request);

        List<ListarSituacaoSolConsistenciaPreviaSaidaDTO> lista =
            new ArrayList<ListarSituacaoSolConsistenciaPreviaSaidaDTO>();

        for (br.com.bradesco.web.pgit.service.data.pdc.consultarsituacaosolconsistenciaprevia.response.Ocorrencias saida : response
            .getOcorrencias()) {
            lista.add(new ListarSituacaoSolConsistenciaPreviaSaidaDTO(saida.getCodigo(), saida.getDescricao()));
        }

        return lista;
    }

    /**
     * (non-Javadoc).
     * 
     * @param listarModalidadesEntradaDTO
     *            the listar modalidades entrada dto
     * @return the list< listar modalidade saida dt o>
     * @see br.com.bradesco.web.pgit.service.business.combo.IComboService#listarModalidadesParametros(br.com.bradesco.web.pgit.service.business.combo.impl.bean.ListarModalidadesEntradaDTO)
     */
    public List<ListarModalidadeSaidaDTO> listarModalidadesParametros(
        ListarModalidadesEntradaDTO listarModalidadesEntradaDTO) {

        ListarModalidadesRequest request = new ListarModalidadesRequest();

        request.setCdProdutoServicoOperacao(listarModalidadesEntradaDTO.getCdServico());
        request.setNumeroOcorrencias(listarModalidadesEntradaDTO.getNumeroOcorrencias());

        ListarModalidadesResponse response =
            getFactoryAdapter().getListarModalidadesPDCAdapter().invokeProcess(request);
        List<ListarModalidadeSaidaDTO> list = new ArrayList<ListarModalidadeSaidaDTO>();

        for (br.com.bradesco.web.pgit.service.data.pdc.listarmodalidades.response.Ocorrencias ocorrencia : response
            .getOcorrencias()) {
            list.add(new ListarModalidadeSaidaDTO(ocorrencia.getCdProdutoOperacaoRelacionado(), ocorrencia
                .getDsProdutoOperacaoRelacionado(), ocorrencia.getCdRelacionamentoProduto()));

        }
        return list;
    }

    /**
     * (non-Javadoc).
     * 
     * @param entrada
     *            the entrada
     * @return the list< listar servicos saida dt o>
     * @see br.com.bradesco.web.pgit.service.business.combo.IComboService#listarTipoServico(br.com.bradesco.web.pgit.service.business.combo.bean.ListarServicosEntradaDTO)
     */
    public List<ListarServicosSaidaDTO> listarTipoServico(ListarServicosEntradaDTO entrada) {
        ListarServicosRequest request = new ListarServicosRequest();

        request.setCdNaturezaServico(PgitUtil.verificaIntegerNulo(entrada.getCdNaturezaServico()));
        request.setNumeroOcorrencias(PgitUtil.verificaIntegerNulo(entrada.getNumeroOcorrencias()));

        ListarServicosResponse response = getFactoryAdapter().getListarServicosPDCAdapter().invokeProcess(request);

        List<ListarServicosSaidaDTO> list = new ArrayList<ListarServicosSaidaDTO>();

        for (br.com.bradesco.web.pgit.service.data.pdc.listarservicos.response.Ocorrencias saida : response
            .getOcorrencias()) {
            list.add(new ListarServicosSaidaDTO(saida.getCdServico(), saida.getDsServico()));
        }

        return list;
    }

    /**
     * (non-Javadoc).
     * 
     * @param entrada
     *            the entrada
     * @return the list< inscricao favorecidos saida dt o>
     * @see br.com.bradesco.web.pgit.service.business.combo.IComboService#listarInscricaoFavorecido(br.com.bradesco.web.pgit.service.business.combo.bean.InscricaoFavorecidosEntradaDTO)
     */
    public List<InscricaoFavorecidosSaidaDTO> listarInscricaoFavorecido(InscricaoFavorecidosEntradaDTO entrada) {

        ListarInscricaoFavorecidosRequest request = new ListarInscricaoFavorecidosRequest();

        request.setCdSituacaoVinculacaoConta(entrada.getCdSituacaoVinculacaoConta());
        request.setNumeroOcorrencias(entrada.getNumeroOcorrencias());

        ListarInscricaoFavorecidosResponse response =
            getFactoryAdapter().getListarInscricaoFavorecidosPDCAdapter().invokeProcess(request);

        List<InscricaoFavorecidosSaidaDTO> list = new ArrayList<InscricaoFavorecidosSaidaDTO>();

        for (br.com.bradesco.web.pgit.service.data.pdc.listarinscricaofavorecidos.response.Ocorrencias saida : response
            .getOcorrencias()) {
            list.add(new InscricaoFavorecidosSaidaDTO(saida.getCdTipoInscricaoFavorecidos(), saida
                .getDsTipoInscricaoFavorecidos()));
        }

        return list;
    }

    /**
     * (non-Javadoc).
     * 
     * @param entrada
     *            the entrada
     * @return the list< consultar situacao pagamento saida dt o>
     * @see br.com.bradesco.web.pgit.service.business.combo.IComboService#consultarSituacaoPagamento(br.com.bradesco.web.pgit.service.business.combo.bean.ConsultarSituacaoPagamentoEntradaDTO)
     */
    public List<ConsultarSituacaoPagamentoSaidaDTO> consultarSituacaoPagamento(
        ConsultarSituacaoPagamentoEntradaDTO entrada) {
        ConsultarSituacaoPagamentoRequest request = new ConsultarSituacaoPagamentoRequest();

        request.setCdSituacao(entrada.getCdSituacao());
        request.setNumeroOcorrencias(IComboServiceConstants.OCORRENCIA_CONSULTAR_SITUACAO_PAG);

        ConsultarSituacaoPagamentoResponse response =
            getFactoryAdapter().getConsultarSituacaoPagamentoPDCAdapter().invokeProcess(request);

        List<ConsultarSituacaoPagamentoSaidaDTO> list = new ArrayList<ConsultarSituacaoPagamentoSaidaDTO>();

        for (br.com.bradesco.web.pgit.service.data.pdc.consultarsituacaopagamento.response.Ocorrencias saida : response
            .getOcorrencias()) {
            list.add(new ConsultarSituacaoPagamentoSaidaDTO(saida.getCodigo(), saida.getDescricao()));
        }

        return list;
    }

    /**
     * (non-Javadoc).
     * 
     * @param entrada
     *            the entrada
     * @return the list< consultar motivo situacao pagamento saida dt o>
     * @see br.com.bradesco.web.pgit.service.business.combo.IComboService#consultarMotivoSituacaoPagamento(br.com.bradesco.web.pgit.service.business.combo.bean.ConsultarMotivoSituacaoPagamentoEntradaDTO)
     */
    public List<ConsultarMotivoSituacaoPagamentoSaidaDTO> consultarMotivoSituacaoPagamento(
        ConsultarMotivoSituacaoPagamentoEntradaDTO entrada) {
        ConsultarMotivoSituacaoPagamentoRequest request = new ConsultarMotivoSituacaoPagamentoRequest();

        request.setCdSituacao(entrada.getCdSituacao());
        request.setNumeroOcorrencias(IComboServiceConstants.OCORRENCIA_CONSULTAR_MOTIVO_SITUACAO_PAG);

        ConsultarMotivoSituacaoPagamentoResponse response =
            getFactoryAdapter().getConsultarMotivoSituacaoPagamentoPDCAdapter().invokeProcess(request);

        List<ConsultarMotivoSituacaoPagamentoSaidaDTO> list = new ArrayList<ConsultarMotivoSituacaoPagamentoSaidaDTO>();

        for (br.com.bradesco.web.pgit.service.data.pdc.consultarmotivosituacaopagamento.response.Ocorrencias saida : response
            .getOcorrencias()) {
            list.add(new ConsultarMotivoSituacaoPagamentoSaidaDTO(saida.getCodigo(), saida.getDescricao()));
        }

        return list;
    }

    /**
     * (non-Javadoc).
     * 
     * @param entrada
     *            the entrada
     * @return the list< listar orgao pagador saida dt o>
     * @see br.com.bradesco.web.pgit.service.business.combo.IComboService#listarOrgaoPagadorDescOrgao(br.com.bradesco.web.pgit.service.business.combo.bean.ListarOrgaoPagadorEntradaDTO)
     */
    public List<ListarOrgaoPagadorSaidaDTO> listarOrgaoPagadorDescOrgao(ListarOrgaoPagadorEntradaDTO entrada) {
        ListarOrgaoPagadorRequest request = new ListarOrgaoPagadorRequest();

        request.setCdOrgaoPagadorOrganizacao(entrada.getCdOrgaoPagadorOrganizacao());
        request.setCdPessoaJuridica(entrada.getCdPessoaJuridica());
        request.setCdSituacaoOrgaoPagador(entrada.getCdSituacaoOrgaoPagador());
        request.setCdTarifaOrgaoPagador(entrada.getCdTarifaOrgaoPagador());
        request.setCdTipoUnidadeOrganizacao(entrada.getCdTipoUnidadeOrganizacao());
        request.setMaximoOcorrencias(entrada.getMaximoOcorrencias());
        request.setNrSequenciaUnidadeOrganizacao(entrada.getNrSequenciaUnidadeOrganizacao());

        ListarOrgaoPagadorResponse response =
            getFactoryAdapter().getListarOrgaoPagadorPDCAdapter().invokeProcess(request);

        List<ListarOrgaoPagadorSaidaDTO> list = new ArrayList<ListarOrgaoPagadorSaidaDTO>();

        for (br.com.bradesco.web.pgit.service.data.pdc.listarorgaopagador.response.Ocorrencias saida : response
            .getOcorrencias()) {
            list.add(new ListarOrgaoPagadorSaidaDTO(saida.getCdOrgaoPagadorOrganizacao(), saida
                .getCdOrgaoPagadorOrganizacao()));
        }

        return list;
    }

    /**
     * (non-Javadoc).
     * 
     * @param entrada
     *            the entrada
     * @return the list< listar orgao pagador saida dt o>
     * @see br.com.bradesco.web.pgit.service.business.combo.IComboService#listarOrgaoPagador(br.com.bradesco.web.pgit.service.business.combo.bean.ListarOrgaoPagadorEntradaDTO)
     */
    public List<ListarOrgaoPagadorSaidaDTO> listarOrgaoPagador(ListarOrgaoPagadorEntradaDTO entrada) {
        ListarOrgaoPagadorRequest request = new ListarOrgaoPagadorRequest();

        request.setCdOrgaoPagadorOrganizacao(entrada.getCdOrgaoPagadorOrganizacao());
        request.setCdPessoaJuridica(entrada.getCdPessoaJuridica());
        request.setCdSituacaoOrgaoPagador(entrada.getCdSituacaoOrgaoPagador());
        request.setCdTarifaOrgaoPagador(entrada.getCdTarifaOrgaoPagador());
        request.setCdTipoUnidadeOrganizacao(entrada.getCdTipoUnidadeOrganizacao());
        request.setMaximoOcorrencias(entrada.getMaximoOcorrencias());
        request.setNrSequenciaUnidadeOrganizacao(entrada.getNrSequenciaUnidadeOrganizacao());

        ListarOrgaoPagadorResponse response =
            getFactoryAdapter().getListarOrgaoPagadorPDCAdapter().invokeProcess(request);

        List<ListarOrgaoPagadorSaidaDTO> list = new ArrayList<ListarOrgaoPagadorSaidaDTO>();
        ListarOrgaoPagadorSaidaDTO saidaDTO;
        for (br.com.bradesco.web.pgit.service.data.pdc.listarorgaopagador.response.Ocorrencias saida : response
            .getOcorrencias()) {
            saidaDTO = new ListarOrgaoPagadorSaidaDTO();
            saidaDTO.setCdOrgaoPagadorOrganizacao(saida.getCdOrgaoPagadorOrganizacao());
            saidaDTO.setCdTipoUnidadeOrganizacao(saida.getCdTipoUnidadeOrganizacao());
            saidaDTO.setDsTipoUnidadeOrganizacao(saida.getDsTipoUnidadeOrganizacao());
            saidaDTO.setCdPessoaJuridica(saida.getCdPessoaJuridica());
            saidaDTO.setDsPessoaJuridica(saida.getDsPessoaJuridica());
            saidaDTO.setNrSequenciaUnidadeOrganizacao(saida.getNrSequenciaUnidadeOrganizacao());
            saidaDTO.setDsNumeroSequenciaUnidadeOrganizacao(saida.getDsNumeroSequenciaUnidadeOrganizacao());
            saidaDTO.setCdSituacaoOrganizacaoPagador(saida.getCdSituacaoOrganizacaoPagador());
            list.add(saidaDTO);

        }

        return list;
    }

    /**
     * (non-Javadoc).
     * 
     * @param entrada
     *            the entrada
     * @return the list< consultar situacao solicitacao estorno saida dt o>
     * @see br.com.bradesco.web.pgit.service.business.combo.IComboService#consultarSituacaoSolicitacaoEstorno(br.com.bradesco.web.pgit.service.business.combo.bean.ConsultarSituacaoSolicitacaoEstornoEntradaDTO)
     */
    public List<ConsultarSituacaoSolicitacaoEstornoSaidaDTO> consultarSituacaoSolicitacaoEstorno(
        ConsultarSituacaoSolicitacaoEstornoEntradaDTO entrada) {
        ConsultarSituacaoSolicitacaoEstornoRequest request = new ConsultarSituacaoSolicitacaoEstornoRequest();

        request.setCdSituacao(entrada.getCdSituacao());
        request.setNumeroOcorrencias(entrada.getNumeroOcorrencias());
        request.setCdIndicador(entrada.getCdIndicador());

        ConsultarSituacaoSolicitacaoEstornoResponse response =
            getFactoryAdapter().getConsultarSituacaoSolicitacaoEstornoPDCAdapter().invokeProcess(request);

        List<ConsultarSituacaoSolicitacaoEstornoSaidaDTO> list =
            new ArrayList<ConsultarSituacaoSolicitacaoEstornoSaidaDTO>();

        for (br.com.bradesco.web.pgit.service.data.pdc.consultarsituacaosolicitacaoestorno.response.Ocorrencias saida : response
            .getOcorrencias()) {
            list.add(new ConsultarSituacaoSolicitacaoEstornoSaidaDTO(saida.getCodigo(), saida.getDescricao()));
        }

        return list;
    }

    /**
     * (non-Javadoc).
     * 
     * @return the list< listar tipo contrato marq saida dt o>
     * @see br.com.bradesco.web.pgit.service.business.combo.IComboService#listarTipoContratoMarq()
     */
    public List<ListarTipoContratoMarqSaidaDTO> listarTipoContratoMarq() {
        br.com.bradesco.web.pgit.service.data.pdc.listartipocontratomarq.request.ListarTipoContratoRequest request =
            new br.com.bradesco.web.pgit.service.data.pdc.listartipocontratomarq.request.ListarTipoContratoRequest();

        request.setNrOcorrencias(IComboServiceConstants.OCORRENCIA_LISTAR_TIPO_CONTRATO_MARQ);

        br.com.bradesco.web.pgit.service.data.pdc.listartipocontratomarq.response.ListarTipoContratoResponse response =
            getFactoryAdapter().getListarTipoContratoMarqPDCAdapter().invokeProcess(request);

        List<ListarTipoContratoMarqSaidaDTO> listaSaida = new ArrayList<ListarTipoContratoMarqSaidaDTO>();
        for (br.com.bradesco.web.pgit.service.data.pdc.listartipocontratomarq.response.Ocorrencias saida : response
            .getOcorrencias()) {
            listaSaida.add(new ListarTipoContratoMarqSaidaDTO(saida.getCdTipoContratoNegocio(), saida
                .getDsTipoContratoNegocio()));
        }

        return listaSaida;

    }

    /**
     * (non-Javadoc).
     * 
     * @param tipoLoteEntradaDTO
     *            the tipo lote entrada dto
     * @return the list< tipo lote saida dt o>
     * @see br.com.bradesco.web.pgit.service.business.combo.IComboService#listarTipoLoteCem(br.com.bradesco.web.pgit.service.business.combo.bean.TipoLoteEntradaDTO)
     */
    public List<TipoLoteSaidaDTO> listarTipoLoteCem(TipoLoteEntradaDTO tipoLoteEntradaDTO) {
        ListarTipoLoteRequest listarTipoLoteRequest = new ListarTipoLoteRequest();

        listarTipoLoteRequest.setCdSituacaoVinculacaoConta(tipoLoteEntradaDTO.getCdSituacaoVinculacaoConta());
        listarTipoLoteRequest.setNumeroOcorrencias(IComboServiceConstants.NUMERO_OCORRENCIAS_LISTAR_TIPO_LOTE_CEM);

        ListarTipoLoteResponse response =
            getFactoryAdapter().getListarTipoLotePDCAdapter().invokeProcess(listarTipoLoteRequest);
        List<TipoLoteSaidaDTO> listaSaida = new ArrayList<TipoLoteSaidaDTO>();

        for (br.com.bradesco.web.pgit.service.data.pdc.listartipolote.response.Ocorrencias saida : response
            .getOcorrencias()) {
            listaSaida.add(new TipoLoteSaidaDTO(saida.getCdTipoLoteLayout(), saida.getDsTipoLoteLayout()));
        }

        return listaSaida;
    }

    /**
     * (non-Javadoc).
     * 
     * @param tipoLayoutArquivoEntradaDTO
     *            the tipo layout arquivo entrada dto
     * @return the list< tipo layout arquivo saida dt o>
     * @see br.com.bradesco.web.pgit.service.business.combo.IComboService#listarTipoLayoutArquivoCem(br.com.bradesco.web.pgit.service.business.combo.bean.TipoLayoutArquivoEntradaDTO)
     */
    public List<TipoLayoutArquivoSaidaDTO> listarTipoLayoutArquivoCem(
        TipoLayoutArquivoEntradaDTO tipoLayoutArquivoEntradaDTO) {
        ListarTipoLayoutArquivoRequest listarTipoLayoutArquivoRequest = new ListarTipoLayoutArquivoRequest();

        listarTipoLayoutArquivoRequest.setCdSituacaoVinculacaoConta(tipoLayoutArquivoEntradaDTO
            .getCdSituacaoVinculacaoConta());
        listarTipoLayoutArquivoRequest
            .setNumeroOcorrencias(IComboServiceConstants.NUMERO_OCORRENCIAS_LISTAR_TIPO_LAYOUT_ARQUIVO);

        ListarTipoLayoutArquivoResponse response =
            getFactoryAdapter().getListarTipoLayoutArquivoPDCAdapter().invokeProcess(listarTipoLayoutArquivoRequest);

        List<TipoLayoutArquivoSaidaDTO> listaSaida = new ArrayList<TipoLayoutArquivoSaidaDTO>();

        for (br.com.bradesco.web.pgit.service.data.pdc.listartipolayoutarquivo.response.Ocorrencias saida : response
            .getOcorrencias()) {
            listaSaida.add(new TipoLayoutArquivoSaidaDTO(saida.getCdTipoLayout(), saida.getDsTipoLayout()));
        }

        return listaSaida;
    }

    /**
     * (non-Javadoc).
     * 
     * @param entrada
     *            the entrada
     * @return the list< tipo layout arquivo saida dt o>
     * @see br.com.bradesco.web.pgit.service.business.combo.IComboService#listarAplictvFormtTipoLayoutArqv(br.com.bradesco.web.pgit.service.business.combo.bean.TipoLayoutArquivoEntradaDTO)
     */
    public List<TipoLayoutArquivoSaidaDTO> listarAplictvFormtTipoLayoutArqv(TipoLayoutArquivoEntradaDTO entrada) {
        ListarAplictvFormtTipoLayoutArqvRequest request = new ListarAplictvFormtTipoLayoutArqvRequest();
        ListarAplictvFormtTipoLayoutArqvResponse response = new ListarAplictvFormtTipoLayoutArqvResponse();

        request.setCdSituacaoVinculacaoConta(entrada.getCdSituacaoVinculacaoConta());
        request.setCdSerie(PgitUtil.verificaIntegerNulo(entrada.getCdSerie()));
        request.setCdAplictvTransmPgto(PgitUtil.verificaIntegerNulo(entrada.getCdSerie()));
        request.setCdSituacaoVinculacaoConta(PgitUtil.verificaIntegerNulo(entrada.getCdSituacaoVinculacaoConta()));
        request.setNrOcorrencias(50);

        response = getFactoryAdapter().getListarAplictvFormtTipoLayoutArqvPDCAdapter().invokeProcess(request);

        List<TipoLayoutArquivoSaidaDTO> listaSaida = new ArrayList<TipoLayoutArquivoSaidaDTO>();

        for (br.com.bradesco.web.pgit.service.data.pdc.listaraplictvformttipolayoutarqv.response.Ocorrencias saida : response
            .getOcorrencias()) {
            listaSaida.add(new TipoLayoutArquivoSaidaDTO(saida.getCdTipoLayout(), saida.getDsTipoLayout()));
        }

        return listaSaida;
    }

    /**
     * (non-Javadoc).
     * 
     * @return the list< listar pessoa juridica contrato marq saida dt o>
     * @see br.com.bradesco.web.pgit.service.business.combo.IComboService#listarPessoaJuridicaContratoMarq()
     */
    public List<ListarPessoaJuridicaContratoMarqSaidaDTO> listarPessoaJuridicaContratoMarq() {
        ListarPessoaJuridicaContratoRequest request = new ListarPessoaJuridicaContratoRequest();

        request.setNrOcorrencias(IComboServiceConstants.OCORRENCIA_LISTAR_PESSOA_JURIDICA_MARQ);

        ListarPessoaJuridicaContratoResponse response =
            getFactoryAdapter().getListarPessoaJuridicaContratoMarqPDCAdapter().invokeProcess(request);

        List<ListarPessoaJuridicaContratoMarqSaidaDTO> listaSaida =
            new ArrayList<ListarPessoaJuridicaContratoMarqSaidaDTO>();
        for (br.com.bradesco.web.pgit.service.data.pdc.listarpessoajuridicacontratomarq.response.Ocorrencias saida : response
            .getOcorrencias()) {
            listaSaida.add(new ListarPessoaJuridicaContratoMarqSaidaDTO(saida.getCdPessoaJuridicaContrato(), saida
                .getDsPessoaJuridicaContrato()));
        }

        return listaSaida;

    }

    /**
     * (non-Javadoc).
     * 
     * @param entrada
     *            the entrada
     * @return the list< consultar tipo conta saida dt o>
     * @see br.com.bradesco.web.pgit.service.business.combo.IComboService#consultarTipoConta(br.com.bradesco.web.pgit.service.business.combo.bean.ConsultarTipoContaEntradaDTO)
     */
    public List<ConsultarTipoContaSaidaDTO> consultarTipoConta(ConsultarTipoContaEntradaDTO entrada) {
        ConsultarTipoContaRequest request = new ConsultarTipoContaRequest();

        request.setCdAgencia(PgitUtil.verificaIntegerNulo(entrada.getCdAgencia()));
        request.setCdBanco(PgitUtil.verificaIntegerNulo(entrada.getCdBanco()));
        request.setCdConta(PgitUtil.verificaLongNulo(entrada.getCdConta()));
        request.setCdDigitoAgencia(PgitUtil.verificaIntegerNulo(entrada.getCdDigitoAgencia()));
        request
            .setCdDigitoConta(PgitUtil.complementaDigito(PgitUtil.verificaStringNula(entrada.getCdDigitoConta()), 2));
        request.setNumeroOcorrencias(IComboServiceConstants.OCORRENCIA_CONSULTAR_TIPO_CONTA);

        ConsultarTipoContaResponse response =
            getFactoryAdapter().getConsultarTipoContaPDCAdapter().invokeProcess(request);

        List<ConsultarTipoContaSaidaDTO> listaSaida = new ArrayList<ConsultarTipoContaSaidaDTO>();
        for (br.com.bradesco.web.pgit.service.data.pdc.consultartipoconta.response.Ocorrencias saida : response
            .getOcorrencias()) {
            listaSaida.add(new ConsultarTipoContaSaidaDTO(saida));
        }

        return listaSaida;

    }

    /**
     * (non-Javadoc).
     * 
     * @param entradaDTO
     *            the entrada dto
     * @return the list< consultar modalidade pgit saida dt o>
     * @see br.com.bradesco.web.pgit.service.business.combo.IComboService#consultarModalidadePGIT(br.com.bradesco.web.pgit.service.business.combo.bean.ConsultarModalidadePGITEntradaDTO)
     */
    public List<ConsultarModalidadePGITSaidaDTO> consultarModalidadePGIT(ConsultarModalidadePGITEntradaDTO entradaDTO) {

        ConsultarModalidadePGITRequest request = new ConsultarModalidadePGITRequest();

        request.setNumeroOcorrencias(IComboServiceConstants.NUMERO_OCORRENCIAS_CONSULTAR_MODALIDADE_PGIT);
        request.setCdPessoaJuridicaContrato(PgitUtil.verificaLongNulo(entradaDTO.getCdPessoaJuridicaContrato()));
        request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoContratoNegocio()));
        request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(entradaDTO.getNrSequenciaContratoNegocio()));
        request.setCdProdutoServicoOperacao(PgitUtil.verificaIntegerNulo(entradaDTO.getCdProdutoServicoOperacao()));

        ConsultarModalidadePGITResponse response =
            getFactoryAdapter().getConsultarModalidadePGITPDCAdapter().invokeProcess(request);

        List<ConsultarModalidadePGITSaidaDTO> list = new ArrayList<ConsultarModalidadePGITSaidaDTO>();

        for (br.com.bradesco.web.pgit.service.data.pdc.consultarmodalidadepgit.response.Ocorrencias saida : response
            .getOcorrencias()) {
            list.add(new ConsultarModalidadePGITSaidaDTO(saida.getCdProdutoServicoRelacionado(), saida
                .getDsProdutoServicoRelacionado()));
        }

        return list;
    }

    /**
     * (non-Javadoc).
     * 
     * @param entradaDTO
     *            the entrada dto
     * @return the list< listar modalidades estorno saida dt o>
     * @see br.com.bradesco.web.pgit.service.business.combo.IComboService#consultarModalidadeEstorno(br.com.bradesco.web.pgit.service.business.combo.bean.ListarModalidadesEstornoEntradaDTO)
     */
    public List<ListarModalidadesEstornoSaidaDTO> consultarModalidadeEstorno(
        ListarModalidadesEstornoEntradaDTO entradaDTO) {

        ListarModalidadesEstornoRequest request = new ListarModalidadesEstornoRequest();

        request.setNumeroOcorrencias(IComboServiceConstants.NUMERO_OCORRENCIAS_CONSULTAR_MODALIDADE_PGIT);
        request.setCdpessoaJuridicaContrato(PgitUtil.verificaLongNulo(entradaDTO.getCdPessoaJuridicaContrato()));
        request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoContratoNegocio()));
        request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(entradaDTO.getNrSequenciaContratoNegocio()));
        request.setCdProdutoServicoOperacao(PgitUtil.verificaIntegerNulo(entradaDTO.getCdProdutoServicoOperacao()));

        ListarModalidadesEstornoResponse response =
            getFactoryAdapter().getListarModalidadesEstornoPDCAdapter().invokeProcess(request);

        List<ListarModalidadesEstornoSaidaDTO> list = new ArrayList<ListarModalidadesEstornoSaidaDTO>();

        for (br.com.bradesco.web.pgit.service.data.pdc.listarmodalidadesestorno.response.Ocorrencias saida : response
            .getOcorrencias()) {
            list.add(new ListarModalidadesEstornoSaidaDTO(saida.getCdProdutoServicoRelacionado(), saida
                .getDsProdutoServicoRelacionado()));
        }

        return list;
    }

    /**
     * (non-Javadoc).
     * 
     * @return the list< listar situacao retorno saida dt o>
     * @see br.com.bradesco.web.pgit.service.business.combo.IComboService#listarSituacaoRetorno()
     */
    public List<ListarSituacaoRetornoSaidaDTO> listarSituacaoRetorno() {

        ListarSituacaoRetornoRequest request = new ListarSituacaoRetornoRequest();

        request.setNumeroOcorrencias(IComboServiceConstants.NUMERO_OCORRENCIAS_LISTAR_SITUACAO_RETORNO);

        ListarSituacaoRetornoResponse response =
            getFactoryAdapter().getListarSituacaoRetornoPDCAdapter().invokeProcess(request);

        List<ListarSituacaoRetornoSaidaDTO> list = new ArrayList<ListarSituacaoRetornoSaidaDTO>();

        for (br.com.bradesco.web.pgit.service.data.pdc.listarsituacaoretorno.response.Ocorrencias saida : response
            .getOcorrencias()) {
            list.add(new ListarSituacaoRetornoSaidaDTO(saida.getCodigo(), saida.getDescricao()));
        }
        return list;
    }

    /**
     * (non-Javadoc).
     * 
     * @return the list< listar tipo layout saida dt o>
     * @see br.com.bradesco.web.pgit.service.business.combo.IComboService#listarTipoLayout()
     */
    public List<ListarTipoLayoutSaidaDTO> listarTipoLayout() {

        ListarTipoLayoutRequest request = new ListarTipoLayoutRequest();

        request.setNrOcorrencias(IComboServiceConstants.NUMERO_OCORRENCIAS_LISTAR_TIPO_LAYOUT_100);

        ListarTipoLayoutResponse response = getFactoryAdapter().getListarTipoLayoutPDCAdapter().invokeProcess(request);

        List<ListarTipoLayoutSaidaDTO> list = new ArrayList<ListarTipoLayoutSaidaDTO>();

        for (br.com.bradesco.web.pgit.service.data.pdc.listartipolayout.response.Ocorrencias saida : response
            .getOcorrencias()) {
            list.add(new ListarTipoLayoutSaidaDTO(saida.getCdTipoLayoutArquivo(), saida.getDsTipoLayoutArquivo()));
        }
        return list;
    }

    /**
     * (non-Javadoc).
     * 
     * @param entradaDTO
     *            the entrada dto
     * @return the list< listar contas vinculadas contrato saida dt o>
     * @see br.com.bradesco.web.pgit.service.business.combo.IComboService#listarContasVinculadasContrato(br.com.bradesco.web.pgit.service.business.combo.bean.ListarContasVinculadasContratoEntradaDTO)
     */
    public List<ListarContasVinculadasContratoSaidaDTO> listarContasVinculadasContrato(
        ListarContasVinculadasContratoEntradaDTO entradaDTO) {
        ListarContasVinculadasContratoRequest request = new ListarContasVinculadasContratoRequest();

        request.setCdPessoaJuridicaNegocio(PgitUtil.verificaLongNulo(entradaDTO.getCdPessoaJuridicaNegocio()));
        request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoContratoNegocio()));
        request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(entradaDTO.getNrSequenciaContratoNegocio()));
        request.setCdCorpoCfpCnpj(PgitUtil.verificaLongNulo(entradaDTO.getCdCorpoCfpCnpj()));
        request.setCdControleCpfCnpj(PgitUtil.verificaIntegerNulo(entradaDTO.getCdControleCpfCnpj()));
        request.setCdDigitoCpfCnpj(PgitUtil.verificaIntegerNulo(entradaDTO.getCdDigitoCpfCnpj()));
        request.setCdFinalidade(PgitUtil.verificaIntegerNulo(entradaDTO.getCdFinalidade()));
        request.setCdBanco(PgitUtil.verificaIntegerNulo(entradaDTO.getCdBanco()));
        request.setCdAgencia(PgitUtil.verificaIntegerNulo(entradaDTO.getCdAgencia()));
        request.setCdDigitoAgencia(PgitUtil.verificaIntegerNulo(entradaDTO.getCdDigitoAgencia()));
        request.setCdConta(PgitUtil.verificaLongNulo(entradaDTO.getCdConta()));
        request.setCdDigitoConta(PgitUtil.verificaStringNula(entradaDTO.getCdDigitoConta()));
        request.setCdTipoConta(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoConta()));
        request.setNrOcorrencias(IComboServiceConstants.NUMERO_OCORRENCIAS_LISTA_CONTAS_VINCULADAS_CONTRATO);

        ListarContasVinculadasContratoResponse response =
            getFactoryAdapter().getListarContasVinculadasContratoPDCAdapter().invokeProcess(request);

        List<ListarContasVinculadasContratoSaidaDTO> list = new ArrayList<ListarContasVinculadasContratoSaidaDTO>();

        for (br.com.bradesco.web.pgit.service.data.pdc.listarcontasvinculadascontrato.response.Ocorrencias saida : response
            .getOcorrencias()) {
            list.add(new ListarContasVinculadasContratoSaidaDTO(saida.getCdBanco(), saida.getCdAgencia(), saida
                .getCdDigitoAgencia(), saida.getCdConta(), saida.getDsBanco(), saida.getDsAgencia(), saida
                .getCdDigitoConta(), saida.getCdTipoConta(), saida.getDsTipoConta(), saida
                .getCdCpfCnpj(), saida.getDsNomeParticipante(), saida.getCdPessoaJuridicaVinculo(), saida
                .getCdTipoContratoVinculo(), saida.getNrSequenciaContratoVinculo()
            ));
        }

        return list;
    }

    /**
     * (non-Javadoc).
     * 
     * @return the list< listar situacao remessa saida dt o>
     * @see br.com.bradesco.web.pgit.service.business.combo.IComboService#listarSituacaoRemessa()
     */
    public List<ListarSituacaoRemessaSaidaDTO> listarSituacaoRemessa() {
        ListarSituacaoRemessaRequest request = new ListarSituacaoRemessaRequest();

        request.setNumeroOcorrencias(IComboServiceConstants.NUMERO_OCORRENCIAS_LISTA_SITUACAO_REMESSA);

        ListarSituacaoRemessaResponse response =
            getFactoryAdapter().getListarSituacaoRemessaPDCAdapter().invokeProcess(request);

        List<ListarSituacaoRemessaSaidaDTO> listaSaida = new ArrayList<ListarSituacaoRemessaSaidaDTO>();

        for (br.com.bradesco.web.pgit.service.data.pdc.listarsituacaoremessa.response.Ocorrencias saida : response
            .getOcorrencias()) {
            listaSaida.add(new ListarSituacaoRemessaSaidaDTO(saida.getCodigo(), saida.getDescricao()));
        }

        return listaSaida;
    }

    /**
     * (non-Javadoc).
     * 
     * @return the list< listar resultado processamento remessa saida dt o>
     * @see br.com.bradesco.web.pgit.service.business.combo.IComboService#listarResultadoProcessamentoRemessa()
     */
    public List<ListarResultadoProcessamentoRemessaSaidaDTO> listarResultadoProcessamentoRemessa() {
        ListarResultadoProcessamentoRemessaRequest request = new ListarResultadoProcessamentoRemessaRequest();

        request.setNumeroOcorrencias(IComboServiceConstants.NUMERO_OCORRENCIAS_LISTA_RESULTADO_PROCESSAMENTO);

        ListarResultadoProcessamentoRemessaResponse response =
            getFactoryAdapter().getListarResultadoProcessamentoRemessaPDCAdapter().invokeProcess(request);

        List<ListarResultadoProcessamentoRemessaSaidaDTO> listaSaida =
            new ArrayList<ListarResultadoProcessamentoRemessaSaidaDTO>();

        for (br.com.bradesco.web.pgit.service.data.pdc.listarresultadoprocessamentoremessa.response.Ocorrencias saida : response
            .getOcorrencias()) {
            listaSaida.add(new ListarResultadoProcessamentoRemessaSaidaDTO(saida.getCodigo(), saida.getDescricao()));
        }

        return listaSaida;
    }

    /**
     * (non-Javadoc).
     * 
     * @param entrada
     *            the entrada
     * @return the list< consultar servico saida dt o>
     * @see br.com.bradesco.web.pgit.service.business.combo.IComboService#consultarServico(br.com.bradesco.web.pgit.service.business.combo.bean.ConsultarServicoEntradaDTO)
     */
    public List<ConsultarServicoSaidaDTO> consultarServico(ConsultarServicoEntradaDTO entrada) {
        ConsultarServicoRequest request = new ConsultarServicoRequest();

        request.setNumeroOcorrencias(100);
        request.setCdpessoaJuridicaContrato(entrada.getCdpessoaJuridicaContrato());
        request.setCdTipoContrato(entrada.getCdTipoContrato());
        request.setNrSequenciaContrato(entrada.getNrSequenciaContrato());

        ConsultarServicoResponse response = getFactoryAdapter().getConsultarServicoPDCAdapter().invokeProcess(request);

        List<ConsultarServicoSaidaDTO> listaSaida = new ArrayList<ConsultarServicoSaidaDTO>();

        for (br.com.bradesco.web.pgit.service.data.pdc.consultarservico.response.Ocorrencias saida : response
            .getOcorrencias()) {
            listaSaida.add(new ConsultarServicoSaidaDTO(saida.getCdServico(), saida.getDsServico()));
        }

        return listaSaida;
    }

    /**
     * (non-Javadoc).
     * 
     * @param entradaDTO
     *            the entrada dto
     * @return the list< consultar modalidade saida dt o>
     * @see br.com.bradesco.web.pgit.service.business.combo.IComboService#consultarModalidade(br.com.bradesco.web.pgit.service.business.combo.bean.ConsultarModalidadeEntradaDTO)
     */
    public List<ConsultarModalidadeSaidaDTO> consultarModalidade(ConsultarModalidadeEntradaDTO entradaDTO) {
        ConsultarModalidadeRequest request = new ConsultarModalidadeRequest();

        request.setCdProdutoServicoOperacao(PgitUtil.verificaIntegerNulo(entradaDTO.getCdProdutoServicoOperacao()));
        request.setQuantidadeOcorrencias(IComboServiceConstants.NUMERO_OCORRENCIAS_LISTA_CONSULTAR_MODALIDADE);

        ConsultarModalidadeResponse response =
            getFactoryAdapter().getConsultarModalidadePDCAdapter().invokeProcess(request);

        List<ConsultarModalidadeSaidaDTO> listaRetorno = new ArrayList<ConsultarModalidadeSaidaDTO>();

        for (br.com.bradesco.web.pgit.service.data.pdc.consultarmodalidade.response.Ocorrencias saida : response
            .getOcorrencias()) {
            listaRetorno.add(new ConsultarModalidadeSaidaDTO(saida.getCdProdutoOperacaoRelacionada(), saida
                .getDsProdutoOperacaoRelacionada(), saida.getCdRelacionamentoProdutoProduto()));
        }

        return listaRetorno;
    }

    /**
     * (non-Javadoc).
     * 
     * @return the list< consultar servico pagto saida dt o>
     * @see br.com.bradesco.web.pgit.service.business.combo.IComboService#consultarServicoPagto()
     */
    public List<ConsultarServicoPagtoSaidaDTO> consultarServicoPagto() {
        ConsultarServicoPagtoRequest request = new ConsultarServicoPagtoRequest();

        request.setMaximoOcorrencias(100);

        ConsultarServicoPagtoResponse response =
            getFactoryAdapter().getConsultarServicoPagtoPDCAdapter().invokeProcess(request);

        List<ConsultarServicoPagtoSaidaDTO> listaRetorno = new ArrayList<ConsultarServicoPagtoSaidaDTO>();

        for (br.com.bradesco.web.pgit.service.data.pdc.consultarservicopagto.response.Ocorrencias saida : response
            .getOcorrencias()) {
            listaRetorno.add(new ConsultarServicoPagtoSaidaDTO(saida.getCdServico(), saida.getDsServico()));
        }

        return listaRetorno;
    }

    /**
     * (non-Javadoc).
     * 
     * @param entradaDTO
     *            the entrada dto
     * @return the list< consultar motivo situacao estorno saida dt o>
     * @see br.com.bradesco.web.pgit.service.business.combo.IComboService#consultarMotivoSituacaoEstornoSaida(br.com.bradesco.web.pgit.service.business.combo.bean.ConsultarMotivoSituacaoEstornoEntradaDTO)
     */
    public List<ConsultarMotivoSituacaoEstornoSaidaDTO> consultarMotivoSituacaoEstornoSaida(
        ConsultarMotivoSituacaoEstornoEntradaDTO entradaDTO) {
        ConsultarMotivoSituacaoEstornoRequest request = new ConsultarMotivoSituacaoEstornoRequest();

        request.setCdSituacao(PgitUtil.verificaIntegerNulo(entradaDTO.getCdSituacao()));
        request.setNumeroOcorrencias(IComboServiceConstants.NUMERO_OCORRENCIAS_MOTIVO_SITUACAO_EST);

        ConsultarMotivoSituacaoEstornoResponse response =
            getFactoryAdapter().getConsultarMotivoSituacaoEstornoPDCAdapter().invokeProcess(request);
        List<ConsultarMotivoSituacaoEstornoSaidaDTO> listaRetorno =
            new ArrayList<ConsultarMotivoSituacaoEstornoSaidaDTO>();

        for (br.com.bradesco.web.pgit.service.data.pdc.consultarmotivosituacaoestorno.response.Ocorrencias saida : response
            .getOcorrencias()) {
            listaRetorno.add(new ConsultarMotivoSituacaoEstornoSaidaDTO(saida.getCodigo(), saida.getDescricao()));
        }

        return listaRetorno;
    }

    /**
     * (non-Javadoc).
     * 
     * @param cdProdutoServicoOperacao
     *            the cd produto servico operacao
     * @return the list< consultar modalidade pagto saida dt o>
     * @see br.com.bradesco.web.pgit.service.business.combo.IComboService#consultarModalidadePagto(java.lang.Integer)
     */
    public List<ConsultarModalidadePagtoSaidaDTO> consultarModalidadePagto(Integer cdProdutoServicoOperacao) {
        ConsultarModalidadePagtoRequest request = new ConsultarModalidadePagtoRequest();

        request.setCdProdutoServicoOperacao(cdProdutoServicoOperacao);
        request.setMaximoOcorrencias(100);

        ConsultarModalidadePagtoResponse response =
            getFactoryAdapter().getConsultarModalidadePagtoPDCAdapter().invokeProcess(request);

        List<ConsultarModalidadePagtoSaidaDTO> listaRetorno = new ArrayList<ConsultarModalidadePagtoSaidaDTO>();

        for (br.com.bradesco.web.pgit.service.data.pdc.consultarmodalidadepagto.response.Ocorrencias saida : response
            .getOcorrencias()) {
            listaRetorno.add(new ConsultarModalidadePagtoSaidaDTO(saida.getCdProdutoOperacaoRelacionado(), saida
                .getDsProdutoOperacaoRelacionado()));
        }

        return listaRetorno;
    }

    /**
     * (non-Javadoc).
     * 
     * @param entradaDTO
     *            the entrada dto
     * @return the list< consultar lista valores discretos saida dt o>
     * @see br.com.bradesco.web.pgit.service.business.combo.IComboService#consultarListaValoresDiscretos(br.com.bradesco.web.pgit.service.business.combo.bean.ConsultarListaValoresDiscretosEntradaDTO)
     */
    
    //forest
    public List<ConsultarListaValoresDiscretosSaidaDTO> consultarListaValoresDiscretos(
        ConsultarListaValoresDiscretosEntradaDTO entradaDTO) {
    	
        ConsultarListaValoresDiscretosRequest request = new ConsultarListaValoresDiscretosRequest();
        ConsultarListaValoresDiscretosResponse response = new ConsultarListaValoresDiscretosResponse();

        request.setNumeroCombo(PgitUtil.verificaIntegerNulo(entradaDTO.getNumeroCombo()));

        response = getFactoryAdapter().getConsultarListaValoresDiscretosPDCAdapter().invokeProcess(request);
        List<ConsultarListaValoresDiscretosSaidaDTO> listaRetorno =
            new ArrayList<ConsultarListaValoresDiscretosSaidaDTO>();

        for (br.com.bradesco.web.pgit.service.data.pdc.consultarlistavaloresdiscretos.response.Ocorrencias saida : response
            .getOcorrencias()) {
            listaRetorno.add(new ConsultarListaValoresDiscretosSaidaDTO(saida.getCdCombo(), saida.getDsCombo()));
        }

        return listaRetorno;
        
    } 
    
    public List<ConsultarListaValoresDiscretosSaidaDTO> consultarConfiguracaoServicoModalidade(
            ConsultarListaValoresDiscretosEntradaDTO entradaDTO) {
    	
        ConsultarListaValoresDiscretosRequest request = new ConsultarListaValoresDiscretosRequest();
        ConsultarListaValoresDiscretosResponse response = new ConsultarListaValoresDiscretosResponse();

        request.setNumeroCombo(PgitUtil.verificaIntegerNulo(62));

        response = getFactoryAdapter().getConsultarListaValoresDiscretosPDCAdapter().invokeProcess(request);
        List<ConsultarListaValoresDiscretosSaidaDTO> listaRetorno =
            new ArrayList<ConsultarListaValoresDiscretosSaidaDTO>();

        for (br.com.bradesco.web.pgit.service.data.pdc.consultarlistavaloresdiscretos.response.Ocorrencias saida : response
            .getOcorrencias()) {
            listaRetorno.add(new ConsultarListaValoresDiscretosSaidaDTO(saida.getCdCombo(), saida.getDsCombo()));
        }

        return listaRetorno;
    }

    /**
     * (non-Javadoc).
     * 
     * @param entrada
     *            the entrada
     * @return the list< listar aplicativo formatacao arquivo saida dt o>
     * @see br.com.bradesco.web.pgit.service.business.combo.IComboService#listarAplicativoFormatacaoArquivo(br.com.bradesco.web.pgit.service.business.combo.bean.ListarAplicativoFormatacaoArquivoEntradaDTO)
     */
    public List<ListarAplicativoFormatacaoArquivoSaidaDTO> listarAplicativoFormatacaoArquivo(
        ListarAplicativoFormatacaoArquivoEntradaDTO entrada) {
        ListarAplicativoFormatacaoArquivoRequest request = new ListarAplicativoFormatacaoArquivoRequest();
        ListarAplicativoFormatacaoArquivoResponse response = new ListarAplicativoFormatacaoArquivoResponse();
        List<ListarAplicativoFormatacaoArquivoSaidaDTO> listaSaida =
            new ArrayList<ListarAplicativoFormatacaoArquivoSaidaDTO>();

        request.setCdAplictvTransmPgto(PgitUtil.verificaIntegerNulo(entrada.getCdAplictvTransmPgto()));
        request.setCdSituacao(PgitUtil.verificaIntegerNulo(entrada.getCdSituacao()));
        request.setCdTipoLayoutArquivo(PgitUtil.verificaIntegerNulo(entrada.getCdTipoLayoutArquivo()));
        request.setNrOcorrencias(50);

        response = getFactoryAdapter().getListarAplicativoFormatacaoArquivoPDCAdapter().invokeProcess(request);

        if (response == null) {
            return null;
        }

        for (br.com.bradesco.web.pgit.service.data.pdc.listaraplicativoformatacaoarquivo.response.Ocorrencias saida : response
            .getOcorrencias()) {
            listaSaida.add(new ListarAplicativoFormatacaoArquivoSaidaDTO(saida.getCdAplicacaoTransmicaoPagamento(),
                saida.getDsAplicacaoTransmicaoPagamento()));
        }

        return listaSaida;
    }

    /**
     * (non-Javadoc).
     * 
     * @param entradaDTO
     *            the entrada dto
     * @return the list< listar empresa transmissao arquivo van saida dt o>
     * @see br.com.bradesco.web.pgit.service.business.combo.IComboService#consultarEmpresaTransmissaoArquivoVan(br.com.bradesco.web.pgit.service.business.combo.bean.ListarEmpresaTransmissaoArquivoVanEntradaDTO)
     */
    public List<ListarEmpresaTransmissaoArquivoVanSaidaDTO> consultarEmpresaTransmissaoArquivoVan(
        ListarEmpresaTransmissaoArquivoVanEntradaDTO entradaDTO) {
        ListarEmpresaTransmissaoArquivoVanRequest request = new ListarEmpresaTransmissaoArquivoVanRequest();
        ListarEmpresaTransmissaoArquivoVanResponse response = new ListarEmpresaTransmissaoArquivoVanResponse();

        request.setNrOcorrencias(entradaDTO.getNrOcorrencias());
        request.setCdSituacaoVinculacaoConta(entradaDTO.getCdSituacaoVinculacaoConta());

        response = getFactoryAdapter().getListarEmpresaTransmissaoArquivoVanPDCAdapter().invokeProcess(request);
        List<ListarEmpresaTransmissaoArquivoVanSaidaDTO> listaRetorno =
            new ArrayList<ListarEmpresaTransmissaoArquivoVanSaidaDTO>();

        for (br.com.bradesco.web.pgit.service.data.pdc.listarempresatransmissaoarquivovan.response.Ocorrencias saida : response
            .getOcorrencias()) {
            listaRetorno.add(new ListarEmpresaTransmissaoArquivoVanSaidaDTO(saida.getCdTipoParticipacao(), saida
                .getDsTipoParticipacao()));
        }

        return listaRetorno;
    }

    /**
     * (non-Javadoc).
     * 
     * @param entradaDTO
     *            the entrada dto
     * @return the list< listar motivo bloqueio desb saida dt o>
     * @see br.com.bradesco.web.pgit.service.business.combo.IComboService#listarMotivoBloqueioDesb(br.com.bradesco.web.pgit.service.business.combo.bean.ListarMotivoBloqueioDesbEntradaDTO)
     */
    public List<ListarMotivoBloqueioDesbSaidaDTO> listarMotivoBloqueioDesb(ListarMotivoBloqueioDesbEntradaDTO entradaDTO) {
        ListarMotivoBloqueioDesbloqueioRequest request = new ListarMotivoBloqueioDesbloqueioRequest();
        ListarMotivoBloqueioDesbloqueioResponse response = new ListarMotivoBloqueioDesbloqueioResponse();

        request.setCdSituacaoParticipanteContrato(PgitUtil.verificaIntegerNulo(entradaDTO
            .getCdSituacaoParticipanteContrato()));
        request.setNrOcorrencias(IComboServiceConstants.NUMERO_LISTAR_MOTIVO_BLOQUEIO_DESBLOQUEIO);
        response = getFactoryAdapter().getListarMotivoBloqueioDesbloqueioPDCAdapter().invokeProcess(request);

        List<ListarMotivoBloqueioDesbSaidaDTO> listaRetorno = new ArrayList<ListarMotivoBloqueioDesbSaidaDTO>();

        for (br.com.bradesco.web.pgit.service.data.pdc.listarmotivobloqueiodesbloqueio.response.Ocorrencias saida : response
            .getOcorrencias()) {
            listaRetorno.add(new ListarMotivoBloqueioDesbSaidaDTO(saida.getCdMotivoSituacaoParticipante(), saida
                .getDsMotivoSituacaoParticipante()));
        }

        return listaRetorno;
    }

    /**
     * (non-Javadoc).
     * 
     * @param entradaDTO
     *            the entrada dto
     * @return the list< listar ass lote layout arquivo saida dt o>
     * @see br.com.bradesco.web.pgit.service.business.combo.IComboService#listarAssLoteLayoutArquivo(br.com.bradesco.web.pgit.service.business.combo.bean.ListarAssLoteLayoutArquivoEntradaDTO)
     */
    public List<ListarAssLoteLayoutArquivoSaidaDTO> listarAssLoteLayoutArquivo(
        ListarAssLoteLayoutArquivoEntradaDTO entradaDTO) {
        ListarAssLoteLayoutArquivoRequest request = new ListarAssLoteLayoutArquivoRequest();
        ListarAssLoteLayoutArquivoResponse response = new ListarAssLoteLayoutArquivoResponse();

        request.setCdTipoLayoutArquivo(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoLayoutArquivo()));
        request.setCdTipoLoteLayout(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoLoteLayout()));
        request.setQtConsultas(IComboServiceConstants.NUMERO_CONSULTA_LISTAR_ASS_LOTE_LAYOUT_ARQUIVO);

        response = getFactoryAdapter().getListarAssLoteLayoutArquivoPDCAdapter().invokeProcess(request);

        List<ListarAssLoteLayoutArquivoSaidaDTO> listaRetorno = new ArrayList<ListarAssLoteLayoutArquivoSaidaDTO>();

        for (br.com.bradesco.web.pgit.service.data.pdc.listarasslotelayoutarquivo.response.Ocorrencias saida : response
            .getOcorrencias()) {
            listaRetorno.add(new ListarAssLoteLayoutArquivoSaidaDTO(saida.getCdTipoLoteLayout(), saida
                .getDsTipoLoteLayout()));
        }

        return listaRetorno;
    }

    /**
     * (non-Javadoc).
     * 
     * @return the list< consultar modalidade servico saida dt o>
     * @see br.com.bradesco.web.pgit.service.business.combo.IComboService#consultarModalidadeServico()
     */
    public List<ConsultarModalidadeServicoSaidaDTO> consultarModalidadeServico() {
        List<ConsultarModalidadeServicoSaidaDTO> listaSaida = new ArrayList<ConsultarModalidadeServicoSaidaDTO>();
        ConsultarModalidadeServicoRequest request = new ConsultarModalidadeServicoRequest();
        ConsultarModalidadeServicoResponse response = new ConsultarModalidadeServicoResponse();

        request.setNumeroOcorrencias(IComboServiceConstants.NUMERO_CONSULTA_MODALIDADE_SERVICO);

        response = getFactoryAdapter().getConsultarModalidadeServicoPDCAdapter().invokeProcess(request);

        for (int i = 0; i < response.getOcorrenciasCount(); i++) {
            ConsultarModalidadeServicoSaidaDTO saida = new ConsultarModalidadeServicoSaidaDTO();
            saida.setCodMensagem(response.getCodMensagem());
            saida.setMensagem(response.getMensagem());
            saida.setNumeroLinhas(response.getNumeroLinhas());
            saida.setCdModalidadePagamentoCliente(response.getOcorrencias(i).getCdModalidadePagamentoCliente());
            saida.setDsModalidadePagamentoCliente(response.getOcorrencias(i).getDsModalidadePagamentoCliente());

            listaSaida.add(saida);
        }

        return listaSaida;
    }

    /**
     * (non-Javadoc).
     * 
     * @param entrada
     *            the entrada
     * @return the list< listar liquidacao pagamento saida dt o>
     * @see br.com.bradesco.web.pgit.service.business.combo.IComboService#listarLiquidacaoPagamento(br.com.bradesco.web.pgit.service.business.combo.bean.ListarLiquidacaoPagamentoEntradaDTO)
     */
    public List<ListarLiquidacaoPagamentoSaidaDTO> listarLiquidacaoPagamento(ListarLiquidacaoPagamentoEntradaDTO entrada) {
        ListarLiquidacaoPagamentoRequest request = new ListarLiquidacaoPagamentoRequest();
        request.setQtConsultas(50);
        request.setCdFormaLiquidacao(PgitUtil.verificaIntegerNulo(entrada.getCdFormaLiquidacao()));
        request.setDtInicioVigencia(PgitUtil.verificaStringNula(entrada.getDtInicioVigencia()));
        request.setDtFinalVigencia(PgitUtil.verificaStringNula(entrada.getDtFinalVigencia()));
        request.setHrInclusaoRegistroHist(PgitUtil.verificaStringNula(entrada.getHrInclusaoRegistroHist()));
        request.setHrConsultaFolhaPgto(PgitUtil.verificaStringNula(entrada.getHrConsultaFolhaPgto()));
        request.setHrConsultasSaldoPagamento(PgitUtil.verificaStringNula(entrada.getHrConsultasSaldoPagamento()));

        ListarLiquidacaoPagamentoResponse response =
            getFactoryAdapter().getListarLiquidacaoPagamentoPDCAdapter().invokeProcess(request);

        List<ListarLiquidacaoPagamentoSaidaDTO> listaSaida = new ArrayList<ListarLiquidacaoPagamentoSaidaDTO>();

        ListarLiquidacaoPagamentoSaidaDTO saida = null;
        for (int i = 0; i < response.getOcorrenciasCount(); i++) {
            br.com.bradesco.web.pgit.service.data.pdc.listarliquidacaopagamento.response.Ocorrencias occor =
                response.getOcorrencias(i);

            saida = new ListarLiquidacaoPagamentoSaidaDTO();
            saida.setCodMensagem(response.getCodMensagem());
            saida.setMensagem(response.getMensagem());
            saida.setNumeroConsultas(response.getNumeroConsultas());
            saida.setCdFormaLiquidacao(occor.getCdFormaLiquidacao());
            saida.setDsFormaLiquidacao(occor.getDsFormaLiquidacao());
            saida.setCdSistema(occor.getCdSistema());
            saida.setCdPrioridadeDebito(occor.getCdPrioridadeDebito());
            saida.setCdControleHora(occor.getCdControleHora());
            saida.setQtMinutosMargemSeguranca(occor.getQtMinutosMargemSeguranca());
            saida.setHrLimiteProcessamento(occor.getHrLimiteProcessamento());
            saida.setHrConsultaFolhaPgto(occor.getHrConsultaFolhaPgto());
            saida.setHrConsultasSaldoPagamento(occor.getHrConsultasSaldoPagamento());
            saida.setQtMinutosValorSuperior(occor.getQtMinutosValorSuperior());
            saida.setHrLimiteValorSuperior(occor.getHrLimiteValorSuperior());
            saida.setQtdTempoAgendamentoCobranca(occor.getQtdTempoAgendamentoCobranca());
            saida.setQtdTempoEfetivacaoCiclicaCobranca(occor.getQtdTempoEfetivacaoCiclicaCobranca());
            saida.setQtdTempoEfetivacaoDiariaCobranca(occor.getQtdTempoEfetivacaoDiariaCobranca());
            listaSaida.add(saida);
        }

        return listaSaida;
    }

    /**
     * (non-Javadoc).
     * 
     * @param entrada
     *            the entrada
     * @return the list< consultar motivo situacao pag pendente saida dt o>
     * @see br.com.bradesco.web.pgit.service.business.combo.IComboService#consultarMotivoSituacaoPagPendente(br.com.bradesco.web.pgit.service.business.combo.bean.ConsultarMotivoSituacaoPagPendenteEntradaDTO)
     */
    public List<ConsultarMotivoSituacaoPagPendenteSaidaDTO> consultarMotivoSituacaoPagPendente(
        ConsultarMotivoSituacaoPagPendenteEntradaDTO entrada) {
        List<ConsultarMotivoSituacaoPagPendenteSaidaDTO> listaSaida =
            new ArrayList<ConsultarMotivoSituacaoPagPendenteSaidaDTO>();
        ConsultarMotivoSituacaoPagPendenteRequest request = new ConsultarMotivoSituacaoPagPendenteRequest();
        ConsultarMotivoSituacaoPagPendenteResponse response = new ConsultarMotivoSituacaoPagPendenteResponse();

        request.setNrOcorrencias(IComboServiceConstants.NUMERO_OCORRENCIAS_LISTA_MOTIVO_SITUACAO_PAG_PENDENTE);
        request.setCdSituacaoPagamento(PgitUtil.verificaIntegerNulo(entrada.getCdSituacaoPagamento()));

        response = getFactoryAdapter().getConsultarMotivoSituacaoPagPendentePDCAdapter().invokeProcess(request);

        for (int i = 0; i < response.getOcorrenciasCount(); i++) {
            ConsultarMotivoSituacaoPagPendenteSaidaDTO saida = new ConsultarMotivoSituacaoPagPendenteSaidaDTO();
            saida.setCodMensagem(response.getCodMensagem());
            saida.setMensagem(response.getMensagem());
            saida.setNumeroLinhas(response.getNumeroLinhas());
            saida.setCdMotivoSituacaoPagamento(response.getOcorrencias(i).getCdMotivoSituacaoPagamento());
            saida.setDsMotivoSituacaoPag(response.getOcorrencias(i).getDsMotivoSituacaoPag());

            listaSaida.add(saida);
        }

        return listaSaida;
    }

    /**
     * (non-Javadoc).
     * 
     * @return the list< listar classe ramo atvdd saida dt o>
     * @see br.com.bradesco.web.pgit.service.business.combo.IComboService#listarClasseRamoAtvdd()
     */
    public List<ListarClasseRamoAtvddSaidaDTO> listarClasseRamoAtvdd() {
        List<ListarClasseRamoAtvddSaidaDTO> listaSaida = new ArrayList<ListarClasseRamoAtvddSaidaDTO>();
        ListarClasseRamoAtvddRequest request = new ListarClasseRamoAtvddRequest();

        request.setNumeroOcorrencias(50);

        ListarClasseRamoAtvddResponse response =
            getFactoryAdapter().getListarClasseRamoAtvddPDCAdapter().invokeProcess(request);

        ListarClasseRamoAtvddSaidaDTO saida;

        for (int i = 0; i < response.getOcorrenciasCount(); i++) {
            saida = new ListarClasseRamoAtvddSaidaDTO();
            saida.setCodMensagem(response.getCodMensagem());
            saida.setMensagem(response.getMensagem());
            saida.setCdClassAtividade(response.getOcorrencias(i).getCdClassAtividade());
            saida.setCdRamoAtividade(response.getOcorrencias(i).getCdRamoAtividade());
            saida.setDsRamoAtividade(response.getOcorrencias(i).getDsRamoAtividade());

            saida.setClasseRamoFormatado(PgitUtil.concatenarCampos(saida.getCdClassAtividade(), PgitUtil
                .concatenarCampos(saida.getCdRamoAtividade(), saida.getDsRamoAtividade()), "/"));

            listaSaida.add(saida);
        }

        return listaSaida;
    }

    /**
     * (non-Javadoc).
     * 
     * @param entrada
     *            the entrada
     * @return the list< listar sramo atvdd econc saida dt o>
     * @see br.com.bradesco.web.pgit.service.business.combo.IComboService#listarSramoAtvddEconc(br.com.bradesco.web.pgit.service.business.combo.bean.ListarSramoAtvddEconcEntradaDTO)
     */
    public List<ListarSramoAtvddEconcSaidaDTO> listarSramoAtvddEconc(ListarSramoAtvddEconcEntradaDTO entrada) {
        List<ListarSramoAtvddEconcSaidaDTO> listaSaida = new ArrayList<ListarSramoAtvddEconcSaidaDTO>();
        ListarSramoAtvddEconcRequest request = new ListarSramoAtvddEconcRequest();

        request.setCdClassAtividade(entrada.getCdClassAtividade());
        request.setCdRamoAtividade(entrada.getCdRamoAtividade());
        request.setOcorrencias(50);

        ListarSramoAtvddEconcResponse response =
            getFactoryAdapter().getListarSramoAtvddEconcPDCAdapter().invokeProcess(request);

        ListarSramoAtvddEconcSaidaDTO saida;
        for (int i = 0; i < response.getOcorrenciasCount(); i++) {
            saida = new ListarSramoAtvddEconcSaidaDTO();
            saida.setCodMensagem(response.getCodMensagem());
            saida.setMensagem(response.getMensagem());
            saida.setCdAtividadeEconomica(response.getOcorrencias(i).getCdAtividadeEconomica());
            saida.setCdSubRamoAtividade(response.getOcorrencias(i).getCdSubRamoAtividade());
            saida.setDsAtividadeEconomica(response.getOcorrencias(i).getDsAtividadeEconomica());

            saida.setSubRamoAtividadeFormatado(PgitUtil.concatenarCampos(saida.getCdSubRamoAtividade(), PgitUtil
                .concatenarCampos(saida.getCdAtividadeEconomica(), saida.getDsAtividadeEconomica()), "/"));

            listaSaida.add(saida);
        }

        return listaSaida;

    }

    /**
     * (non-Javadoc).
     * 
     * @param entrada
     *            the entrada
     * @return the list< listar tipo relac conta saida dt o>
     * @see br.com.bradesco.web.pgit.service.business.combo.IComboService#listarTipoRelacConta(br.com.bradesco.web.pgit.service.business.combo.bean.ListarTipoRelacContaEntradaDTO)
     */
    public List<ListarTipoRelacContaSaidaDTO> listarTipoRelacConta(ListarTipoRelacContaEntradaDTO entrada) {
        List<ListarTipoRelacContaSaidaDTO> listaRetorno = new ArrayList<ListarTipoRelacContaSaidaDTO>();
        ListarTipoRelacContaRequest request = new ListarTipoRelacContaRequest();

        request.setNumeroOcorrencias(30);
        request.setCdSituacaoVinculacaoConta(entrada.getCdSituacaoVinculacaoConta());

        ListarTipoRelacContaResponse response =
            getFactoryAdapter().getListarTipoRelacContaPDCAdapter().invokeProcess(request);

        ListarTipoRelacContaSaidaDTO saida;
        for (int i = 0; i < response.getOcorrenciasCount(); i++) {
            saida = new ListarTipoRelacContaSaidaDTO();
            saida.setCodMensagem(response.getCodMensagem());
            saida.setMensagem(response.getMensagem());
            saida.setTpRelacionamentoContrato(response.getOcorrencias(i).getTpRelacionamentoContrato());
            saida.setRsTipoRelacionamentoContrato(response.getOcorrencias(i).getRsTipoRelacionamentoContrato());
            listaRetorno.add(saida);
        }

        return listaRetorno;
    }

    /**
     * (non-Javadoc).
     * 
     * @param entrada
     *            the entrada
     * @return the list< listar operacao servico pagto integrado saida dt o>
     * @see br.com.bradesco.web.pgit.service.business.combo.IComboService#listarOperacaoServicoPagtoIntegrado(br.com.bradesco.web.pgit.service.business.combo.bean.ListarOperacaoServicoPagtoIntegradoEntradaDTO)
     */
    public List<ListarOperacaoServicoPagtoIntegradoSaidaDTO> listarOperacaoServicoPagtoIntegrado(
        ListarOperacaoServicoPagtoIntegradoEntradaDTO entrada) {
        List<ListarOperacaoServicoPagtoIntegradoSaidaDTO> listaSaida =
            new ArrayList<ListarOperacaoServicoPagtoIntegradoSaidaDTO>();
        ListarOperacaoServicoPagtoIntegradoRequest request = new ListarOperacaoServicoPagtoIntegradoRequest();
        ListarOperacaoServicoPagtoIntegradoResponse response = new ListarOperacaoServicoPagtoIntegradoResponse();

        request.setMaxOcorrencias(IManterOperacoesServRelacionadoServiceConstants.NUMERO_OCORRENCIAS_CONSULTA);
        request.setCdprodutoServicoOperacao(PgitUtil.verificaIntegerNulo(entrada.getCdprodutoServicoOperacao()));
        request
            .setCdProdutoOperacaoRelacionado(PgitUtil.verificaIntegerNulo(entrada.getCdProdutoOperacaoRelacionado()));
        request.setCdOperacaoProdutoServico(PgitUtil.verificaIntegerNulo(entrada.getCdOperacaoProdutoServico()));

        response = getFactoryAdapter().getListarOperacaoServicoPagtoIntegradoPDCAdapter().invokeProcess(request);

        for (int i = 0; i < response.getOcorrenciasCount(); i++) {
            ListarOperacaoServicoPagtoIntegradoSaidaDTO saida = new ListarOperacaoServicoPagtoIntegradoSaidaDTO();
            saida.setCodMensagem(response.getCodMensagem());
            saida.setMensagem(response.getMensagem());
            saida.setNumeroLinhas(response.getNumeroLinhas());
            saida.setCdprodutoServicoOperacao(response.getOcorrencias(i).getCdprodutoServicoOperacao());
            saida.setCdProdutoOperacaoRelacionado(response.getOcorrencias(i).getCdProdutoOperacaoRelacionado());
            saida.setCdOperacaoProdutoServico(response.getOcorrencias(i).getCdOperacaoProdutoServico());
            saida.setDsTipoServico(response.getOcorrencias(i).getDsTipoServico());
            saida.setDsModalidadeServico(response.getOcorrencias(i).getDsModalidadeServico());
            saida.setDsOperacaoCatalogo(response.getOcorrencias(i).getDsOperacaoCatalogo());
            saida.setCdOperacaoServicoIntegrado(response.getOcorrencias(i).getCdOperacaoServicoIntegrado() == 0 ? ""
                : String.valueOf(response.getOcorrencias(i).getCdOperacaoServicoIntegrado()));
            saida.setTipoServicoFormatado(PgitUtil.concatenarCampos(saida.getCdprodutoServicoOperacao(), saida
                .getDsTipoServico()));
            saida.setModalidadeServicoFormatado(PgitUtil.concatenarCampos(saida.getCdProdutoOperacaoRelacionado(),
                saida.getDsModalidadeServico()));
            saida.setOperacaoCatalogoFormatado(PgitUtil.concatenarCampos(saida.getCdOperacaoProdutoServico(), saida
                .getDsOperacaoCatalogo()));
            saida.setDsNatureza(response.getOcorrencias(i).getDsNatureza());

            listaSaida.add(saida);
        }

        return listaSaida;
    }

    /**
     * (non-Javadoc).
     * 
     * @param entrada
     *            the entrada
     * @return the list< lista tipo servico contrato saida dt o>
     * @see br.com.bradesco.web.pgit.service.business.combo.IComboService#listarTipoServicoContrato(br.com.bradesco.web.pgit.service.business.combo.bean.ListaTipoServicoContratoEntradaDTO)
     */
    public List<ListaTipoServicoContratoSaidaDTO> listarTipoServicoContrato(ListaTipoServicoContratoEntradaDTO entrada) {

        List<ListaTipoServicoContratoSaidaDTO> listaSaida = new ArrayList<ListaTipoServicoContratoSaidaDTO>();
        ListaTipoServicoContratoRequest request = new ListaTipoServicoContratoRequest();
        ListaTipoServicoContratoResponse response = new ListaTipoServicoContratoResponse();

        request.setNumeroOcorrencias(PgitUtil.verificaIntegerNulo(entrada.getNumeroOcorrencias()));
        request.setCdNaturezaOperacaoPagamento(PgitUtil.verificaIntegerNulo(entrada.getCdNaturezaOperacaoPagamento()));
        request.setCdPessoaJuridicaContrato(PgitUtil.verificaLongNulo(entrada.getCdPessoaJuridicaContrato()));
        request
            .setCdProdutoOperacaoRelacionado(PgitUtil.verificaIntegerNulo(entrada.getCdProdutoOperacaoRelacionado()));
        request.setCdprodutoServicoOperacao(PgitUtil.verificaIntegerNulo(entrada.getCdprodutoServicoOperacao()));
        request.setCdTipoContrato(PgitUtil.verificaIntegerNulo(entrada.getCdTipoContrato()));
        request.setNrSequenciaContrato(PgitUtil.verificaLongNulo(entrada.getNrSequenciaContrato()));

        response = getFactoryAdapter().getListaTipoServicoContratoPDCAdapter().invokeProcess(request);

        for (int i = 0; i < response.getOcorrenciasCount(); i++) {
            ListaTipoServicoContratoSaidaDTO saida = new ListaTipoServicoContratoSaidaDTO();

            saida.setCdNaturezaOperacaoPagamento(response.getOcorrencias(i).getCdNaturezaOperacaoPagamento());
            saida.setCdParametroTela(response.getOcorrencias(i).getCdParametroTela());
            saida.setCdProdutoOperacaoRelacionado(response.getOcorrencias(i).getCdProdutoOperacaoRelacionado());
            saida.setCdprodutoServicoOperacao(response.getOcorrencias(i).getCdprodutoServicoOperacao());
            saida.setCdRelacionamentoProdutoProduto(response.getOcorrencias(i).getCdRelacionamentoProdutoProduto());
            saida.setCdServicoCompostoPagamento(response.getOcorrencias(i).getCdServicoCompostoPagamento());
            saida.setCdSituacaoServicoRelacionado(response.getOcorrencias(i).getCdSituacaoServicoRelacionado());
            saida.setDsProdutoOperacaoRelacionado(response.getOcorrencias(i).getDsProdutoOperacaoRelacionado());
            saida.setDsSituacaoServicoRelacionado(response.getOcorrencias(i).getDsSituacaoServicoRelacionado());
            saida.setDtInclusaoServico(response.getOcorrencias(i).getDtInclusaoServico());

            listaSaida.add(saida);
        }

        return listaSaida;
    }

    /**
     * (non-Javadoc).
     * 
     * @param entrada
     *            the entrada
     * @return the list< listar servico modalidade emissao aviso saida dt o>
     * @see br.com.bradesco.web.pgit.service.business.combo.IComboService#listarServicoModalidadeEmissaoAviso(br.com.bradesco.web.pgit.service.business.combo.bean.ListarServicoModalidadeEmissaoAvisoEntradaDTO)
     */
    public List<ListarServicoModalidadeEmissaoAvisoSaidaDTO> listarServicoModalidadeEmissaoAviso(
        ListarServicoModalidadeEmissaoAvisoEntradaDTO entrada) {

        List<ListarServicoModalidadeEmissaoAvisoSaidaDTO> listaSaida =
            new ArrayList<ListarServicoModalidadeEmissaoAvisoSaidaDTO>();
        ListarServicoModalidadeEmissaoAvisoRequest request = new ListarServicoModalidadeEmissaoAvisoRequest();
        ListarServicoModalidadeEmissaoAvisoResponse response = new ListarServicoModalidadeEmissaoAvisoResponse();

        request.setNrOcorrencias(PgitUtil.verificaIntegerNulo(entrada.getNrOcorrencias()));
        request.setCdpessoaJuridicaContrato(PgitUtil.verificaLongNulo(entrada.getCdPessoaJuridicaContrato()));
        request.setCdTipoContrato(PgitUtil.verificaIntegerNulo(entrada.getCdTipoContrato()));
        request.setNrSequenciaContrato(PgitUtil.verificaLongNulo(entrada.getNrSequenciaContrato()));
        request.setCdProdutoServicoEmissao(PgitUtil.verificaIntegerNulo(entrada.getCdProdutoServicoEmissao()));
        request.setCdProdutoServicoOperacao(PgitUtil.verificaIntegerNulo(entrada.getCdProdutoServicoOperacao()));
        request.setCdNaturezaOperacaoPagamento(PgitUtil.verificaIntegerNulo(entrada.getCdNaturezaOperacaoPagamento()));

        response = getFactoryAdapter().getListarServicoModalidadeEmissaoAvisoPDCAdapter().invokeProcess(request);

        for (int i = 0; i < response.getOcorrenciasCount(); i++) {
            ListarServicoModalidadeEmissaoAvisoSaidaDTO saida = new ListarServicoModalidadeEmissaoAvisoSaidaDTO();

            saida.setCdParametroTela(response.getOcorrencias(i).getCdParametroTela());
            saida.setCdProdutoOperacaoRelacionado(response.getOcorrencias(i).getCdProdutoOperacaoRelacionado());
            saida.setDsProdutoOperacaoRelacionado(response.getOcorrencias(i).getDsProdutoOperacaoRelacionado());
            saida.setCdprodutoServicoOperacao(response.getOcorrencias(i).getCdProdutoServicoOperacao());
            saida.setDsProdutoServicoOperacao(response.getOcorrencias(i).getDsProdutoServicoOperacao());
            saida.setCdRelacionamentoProdutoProduto(response.getOcorrencias(i).getCdRelacionamentoProdutoProduto());
            saida.setCdSituacaoServicoRelacionado(response.getOcorrencias(i).getCdSituacaoServicoRelacionado());
            saida.setDsSituacaoServicoRelacionado(response.getOcorrencias(i).getDsSituacaoServicoRelacionado());
            saida.setCdNaturezaOperacaoPagamento(response.getOcorrencias(i).getCdNaturezaOperacaoPagamento());
            saida.setCdServicoCompostoPagamento(response.getOcorrencias(i).getCdServicoCompostoPagamento());

            listaSaida.add(saida);
        }

        return listaSaida;
    }

    /**
     * (non-Javadoc).
     * 
     * @param entrada
     *            the entrada
     * @return the list< listar modalidade contrato ocorrencia saida dt o>
     * @see br.com.bradesco.web.pgit.service.business.combo.IComboService#listarModalidadeContrato(br.com.bradesco.web.pgit.service.business.combo.bean.ListarModalidadeContratoEntradaDTO)
     */
    public List<ListarModalidadeContratoOcorrenciaSaidaDTO> listarModalidadeContrato(
        ListarModalidadeContratoEntradaDTO entrada) {

        List<ListarModalidadeContratoOcorrenciaSaidaDTO> listaSaida =
            new ArrayList<ListarModalidadeContratoOcorrenciaSaidaDTO>();
        ListarModalidadeContratoRequest request = new ListarModalidadeContratoRequest();
        ListarModalidadeContratoResponse response = new ListarModalidadeContratoResponse();

        request.setCdNaturezaOperacaoPagamento(entrada.getCdNaturezaOperacaoPagamento());
        request.setCdPessoaJuridicaContrato(entrada.getCdPessoaJuridicaContrato());
        request.setCdProdutoOperacaoRelacionado(entrada.getCdProdutoOperacaoRelacionado());
        request.setCdprodutoServicoOperacao(entrada.getCdprodutoServicoOperacao());
        request.setCdTipoContrato(entrada.getCdTipoContrato());
        request.setNrSequenciaContrato(entrada.getNrSequenciaContrato());
        request.setNumeroOcorrencias(entrada.getNumeroOcorrencias());

        response = getFactoryAdapter().getListarModalidadeContratoPDCAdapter().invokeProcess(request);

        for (int i = 0; i < response.getOcorrenciasCount(); i++) {
            ListarModalidadeContratoOcorrenciaSaidaDTO saida = new ListarModalidadeContratoOcorrenciaSaidaDTO();

            saida.setCdNaturezaOperacaoPagamento(response.getOcorrencias(i).getCdNaturezaOperacaoPagamento());
            saida.setCdParametroTela(response.getOcorrencias(i).getCdParametroTela());
            saida.setCdProdutoOperacaoRelacionado(response.getOcorrencias(i).getCdProdutoOperacaoRelacionado());
            saida.setCdprodutoServicoOperacao(response.getOcorrencias(i).getCdprodutoServicoOperacao());
            saida.setCdRelacionamentoProdutoProduto(response.getOcorrencias(i).getCdRelacionamentoProdutoProduto());
            saida.setCdServicoCompostoPagamento(response.getOcorrencias(i).getCdServicoCompostoPagamento());
            saida.setCdSituacaoServicoRelacionado(response.getOcorrencias(i).getCdSituacaoServicoRelacionado());
            saida.setDsProdutoOperacaoRelacionado(response.getOcorrencias(i).getDsProdutoOperacaoRelacionado());
            saida.setDsProdutoServicoOperacao(response.getOcorrencias(i).getDsProdutoServicoOperacao());
            saida.setDsSituacaoServicoRelacionado(response.getOcorrencias(i).getDsSituacaoServicoRelacionado());
            saida.setDtInclusaoServico(response.getOcorrencias(i).getDtInclusaoServico());

            listaSaida.add(saida);
        }

        return listaSaida;
    }

    /**
     * (non-Javadoc).
     * 
     * @param entrada
     *            the entrada
     * @return the listar meio transmissao pagamento saida dto
     * @see br.com.bradesco.web.pgit.service.business.combo.IComboService#listarMeioTransmissaoPagamento(br.com.bradesco.web.pgit.service.business.combo.bean.ListarMeioTransmissaoPagamentoEntradaDTO)
     */
    public ListarMeioTransmissaoPagamentoSaidaDTO listarMeioTransmissaoPagamento(
        ListarMeioTransmissaoPagamentoEntradaDTO entrada) {

        ListarMeioTransmissaoPagamentoSaidaDTO saida = new ListarMeioTransmissaoPagamentoSaidaDTO();
        ListarMeioTransmissaoPagamentoRequest request = new ListarMeioTransmissaoPagamentoRequest();
        ListarMeioTransmissaoPagamentoResponse response = new ListarMeioTransmissaoPagamentoResponse();
        List<ListarMeioTransmissaoPagamentoOcorrenciaSaidaDTO> listaSaida =
            new ArrayList<ListarMeioTransmissaoPagamentoOcorrenciaSaidaDTO>();

        request.setCdSituacaoVinculacaoConta(PgitUtil.verificaIntegerNulo(entrada.getCdSituacaoVinculacaoConta()));
        request.setNrOcorrencias(50);

        response = getFactoryAdapter().getListarMeioTransmissaoPagamentoPDCAdapter().invokeProcess(request);

        saida.setCodMensagem(response.getCodMensagem());
        saida.setMensagem(response.getMensagem());
        saida.setNumeroLinhas(response.getNumeroLinhas());

        for (int i = 0; i < response.getOcorrenciasCount(); i++) {
            ListarMeioTransmissaoPagamentoOcorrenciaSaidaDTO ocorrencia =
                new ListarMeioTransmissaoPagamentoOcorrenciaSaidaDTO();

            ocorrencia.setCdTipoMeioTransmissao(response.getOcorrencias(i).getCdTipoMeioTransmissao());
            ocorrencia.setDsTipoMeioTransmissao(response.getOcorrencias(i).getDsTipoMeioTransmissao());
            listaSaida.add(ocorrencia);
        }

        saida.setOcorrencias(listaSaida);
        return saida;
    }

    /**
     * (non-Javadoc).
     * 
     * @param cdpessoaJuridicaContrato
     *            the cdpessoa juridica contrato
     * @param cdTipoContrato
     *            the cd tipo contrato
     * @param nrSequenciaContrato
     *            the nr sequencia contrato
     * @return the list< consultar servico pagto saida dt o>
     * @see br.com.bradesco.web.pgit.service.business.combo.IComboService#consultarListaServicosPgto(java.lang.Long,
     *      java.lang.Integer, java.lang.Long)
     */
    public List<ConsultarServicoPagtoSaidaDTO> consultarListaServicosPgto(Long cdpessoaJuridicaContrato,
        Integer cdTipoContrato, Long nrSequenciaContrato) {
        ConsultarListaServicosPgtoRequest request = new ConsultarListaServicosPgtoRequest();

        request.setCdpessoaJuridicaContrato(PgitUtil.verificaLongNulo(cdpessoaJuridicaContrato));
        request.setCdTipoContrato(PgitUtil.verificaIntegerNulo(cdTipoContrato));
        request.setNrSequenciaContrato(PgitUtil.verificaLongNulo(nrSequenciaContrato));
        request.setNrOcorrencias(50);

        ConsultarListaServicosPgtoResponse response =
            getFactoryAdapter().getConsultarListaServicosPgtoPDCAdapter().invokeProcess(request);

        List<ConsultarServicoPagtoSaidaDTO> listaRetorno = new ArrayList<ConsultarServicoPagtoSaidaDTO>();

        for (br.com.bradesco.web.pgit.service.data.pdc.consultarlistaservicospgto.response.Ocorrencias saida : response
            .getOcorrencias()) {
            listaRetorno.add(new ConsultarServicoPagtoSaidaDTO(saida.getCdServico(), saida.getDsServico()));
        }

        return listaRetorno;
    }

    /**
     * (non-Javadoc).
     * 
     * @param entradaDTO
     *            the entrada dto
     * @return the list< listar modalidade saida dt o>
     * @see br.com.bradesco.web.pgit.service.business.combo.IComboService#consultarListaModalidades(br.com.bradesco.web.pgit.service.business.combo.impl.bean.ListarModalidadesEntradaDTO)
     */
    public List<ListarModalidadeSaidaDTO> consultarListaModalidades(ListarModalidadesEntradaDTO entradaDTO) {
        ConsultarListaModalidadesRequest request = new ConsultarListaModalidadesRequest();

        request.setNrOcorrencias(entradaDTO.getNumeroOcorrencias());
        request.setCdProdutoServicoOperacao(entradaDTO.getCdServico());

        ConsultarListaModalidadesResponse response =
            getFactoryAdapter().getConsultarListaModalidadesPDCAdapter().invokeProcess(request);
        List<ListarModalidadeSaidaDTO> list = new ArrayList<ListarModalidadeSaidaDTO>();

        for (br.com.bradesco.web.pgit.service.data.pdc.consultarlistamodalidades.response.Ocorrencias ocorrencia : response
            .getOcorrencias()) {
            list.add(new ListarModalidadeSaidaDTO(ocorrencia.getCdProdutoOperacaoRelacionado(), ocorrencia
                .getDsProdutoOperacaoRelacionada(), ocorrencia.getCdRelacionamentoProduto()));

        }
        return list;
    }

    /**
     * (non-Javadoc).
     * 
     * @param entrada
     *            the entrada
     * @return the listar numero versao layout saida dto
     * @see br.com.bradesco.web.pgit.service.business.combo.IComboService#listarNumeroVersaoLayout(br.com.bradesco.web.pgit.service.business.combo.bean.ListarNumeroVersaoLayoutEntradaDTO)
     */
    public ListarNumeroVersaoLayoutSaidaDTO listarNumeroVersaoLayout(ListarNumeroVersaoLayoutEntradaDTO entrada) {

        ListarNumeroVersaoLayoutSaidaDTO saida = new ListarNumeroVersaoLayoutSaidaDTO();
        ListarNumeroVersaoLayoutRequest request = new ListarNumeroVersaoLayoutRequest();
        ListarNumeroVersaoLayoutResponse response = new ListarNumeroVersaoLayoutResponse();
        List<ListarNumeroVersaoLayoutOcorrSaidaDTO> listaSaida = new ArrayList<ListarNumeroVersaoLayoutOcorrSaidaDTO>();

        request.setNumeroCombo(verificaIntegerNulo(entrada.getNumeroCombo()));

        response = getFactoryAdapter().getListarNumeroVersaoLayoutPDCAdapter().invokeProcess(request);

        saida.setCodMensagem(response.getCodMensagem());
        saida.setMensagem(response.getMensagem());
        saida.setNumeroLinhas(response.getNumeroLinhas());

        for (int i = 0; i < response.getOcorrenciasCount(); i++) {
            ListarNumeroVersaoLayoutOcorrSaidaDTO ocorrencia = new ListarNumeroVersaoLayoutOcorrSaidaDTO();

            ocorrencia.setNumeroVersaoLayoutArquivo(response.getOcorrencias(i).getNumeroVersaoLayoutArquivo());
            listaSaida.add(ocorrencia);
        }

        saida.setOcorrencias(listaSaida);
        return saida;
    }

    /*
     * (non-Javadoc)
     * 
     * @see br.com.bradesco.web.pgit.service.business.combo.IComboService#listarTipoProcessamentoLayoutArquivo
     * (br.com.bradesco.web.pgit.service.business.combo.bean.ListarTipoProcessamentoLayoutArquivoEntradaDTO)
     */
    public ListarTipoProcessamentoLayoutArquivoSaidaDTO listarTipoProcessamentoLayoutArquivo(
        ListarTipoProcessamentoLayoutArquivoEntradaDTO entrada) {

        ListarTipoProcessamentoLayoutArquivoRequest request = new ListarTipoProcessamentoLayoutArquivoRequest();
        request.setCdpessoaJuridicaContrato(verificaLongNulo(entrada.getCdpessoaJuridicaContrato()));
        request.setCdTipoContratoNegocio(verificaIntegerNulo(entrada.getCdTipoContratoNegocio()));
        request.setNrSequenciaContratoNegocio(verificaLongNulo(entrada.getNrSequenciaContratoNegocio()));
        request.setCdProdutoServicoOperacao(verificaIntegerNulo(entrada.getCdProdutoServicoOperacao()));
        request.setCdProdutoOperacaoRelacionado(verificaIntegerNulo(entrada.getCdProdutoOperacaoRelacionado()));
        request.setCdRelacionamentoProduto(verificaIntegerNulo(entrada.getCdRelacionamentoProduto()));

        ListarTipoProcessamentoLayoutArquivoResponse response =
            getFactoryAdapter().getListarTipoProcessamentoLayoutArquivoPDCAdapter().invokeProcess(request);
        ListarTipoProcessamentoLayoutArquivoSaidaDTO saida = new ListarTipoProcessamentoLayoutArquivoSaidaDTO();

        saida.setCodMensagem(response.getCodMensagem());
        saida.setMensagem(response.getMensagem());
        saida.setNrLinhas(response.getNrLinhas());

        List<ListarTipoProcessamentoLayoutArquivoOcorrenciasSaidaDTO> listaSaida =
            new ArrayList<ListarTipoProcessamentoLayoutArquivoOcorrenciasSaidaDTO>();

        for (int i = 0; i < response.getOcorrenciasCount(); i++) {
            ListarTipoProcessamentoLayoutArquivoOcorrenciasSaidaDTO ocorrencia =
                new ListarTipoProcessamentoLayoutArquivoOcorrenciasSaidaDTO();

            ocorrencia.setCdCombo(response.getOcorrencias(i).getCdCombo());
            ocorrencia.setDsCombo(response.getOcorrencias(i).getDsCombo());

            listaSaida.add(ocorrencia);
        }

        saida.setOcorrencias(listaSaida);
        return saida;
    }

    /*
     * (non-Javadoc)
     * 
     * @see br.com.bradesco.web.pgit.service.business.combo.IComboService#listarParametros(
     * br.com.bradesco.web.pgit.service.business.combo.bean.ListarParametrosEntradaDTO)
     */
    public ListarParametrosSaidaDTO listarParametros(ListarParametrosEntradaDTO entrada) {
        ListarParametrosRequest request = new ListarParametrosRequest();

        request.setNrMaximoOcorrencias(100);

        ListarParametrosResponse response = getFactoryAdapter().getListarParametrosPDCAdapter().invokeProcess(request);

        ListarParametrosSaidaDTO saida = new ListarParametrosSaidaDTO();

        saida.setCodMensagem(response.getCodMensagem());
        saida.setMensagem(response.getMensagem());
        saida.setNrLinhas(response.getNrLinhas());

        List<ListarParametrosOcorrenciasSaidaDTO> listParametrosOcorrencias =
            new ArrayList<ListarParametrosOcorrenciasSaidaDTO>(response.getNrLinhas());

        for (int i = 0; i < response.getNrLinhas(); i++) {
            ListarParametrosOcorrenciasSaidaDTO ocorrencia = new ListarParametrosOcorrenciasSaidaDTO();

            ocorrencia.setCdParametro(response.getOcorrencias(i).getCdParametro());
            ocorrencia.setDsParametro(response.getOcorrencias(i).getDsParametro());
            ocorrencia.setCdIndicadorUtilizaDescricao(response.getOcorrencias(i).getCdIndicadorUtilizaDescricao());

            listParametrosOcorrencias.add(ocorrencia);
        }

        saida.setOcorrencias(listParametrosOcorrencias);

        return saida;
    }

    /**
     * (non-Javadoc).
     * 
     * @param entrada
     *            the entrada
     * @return the listar layouts contrato saida dto
     * @see br.com.bradesco.web.pgit.service.business.combo.IComboService#listarLayoutsContrato
     *      (br.com.bradesco.web.pgit.service.business.combo.bean.ListarLayoutsContratoEntradaDTO)
     */
    public ListarLayoutsContratoSaidaDTO listarLayoutsContrato(ListarLayoutsContratoEntradaDTO entrada) {

        ListarLayoutsContratoRequest request = new ListarLayoutsContratoRequest();
        request.setCdpessoaJuridicaContrato(verificaLongNulo(entrada.getCdpessoaJuridicaContrato()));
        request.setNrSequenciaContratoNegocio(verificaLongNulo(entrada.getNrSequenciaContratoNegocio()));
        request.setMaxOcorrencias(verificaIntegerNulo(entrada.getMaxOcorrencias()));
        request.setCdTipoContrato(verificaIntegerNulo(entrada.getCdTipoContrato()));
        request.setCdProdutoServicoOperacao(verificaIntegerNulo(entrada.getCdProdutoServicoOperacao()));
        request.setCdProdutoOperacaoRelacionado(verificaIntegerNulo(entrada.getCdProdutoOperacaoRelacionado()));

        ListarLayoutsContratoResponse response =
            getFactoryAdapter().getListarLayoutsContratoPDCAdapter().invokeProcess(request);

        ListarLayoutsContratoSaidaDTO saida = new ListarLayoutsContratoSaidaDTO();
        saida.setCodMensagem(response.getCodMensagem());
        saida.setMensagem(response.getMensagem());
        saida.setNrLinhas(response.getNrLinhas());

        saida.setOcorrencias(new ArrayList<ListarLayoutsContratoOcorrenciasSaidaDTO>());
        for (int i = 0; i < response.getOcorrenciasCount(); i++) {
            ListarLayoutsContratoOcorrenciasSaidaDTO item = new ListarLayoutsContratoOcorrenciasSaidaDTO();

            item.setCdTipoLayoutArquivo(response.getOcorrencias(i).getCdTipoLayoutArquivo());
            item.setDsTipoLayoutArquivo(response.getOcorrencias(i).getDsTipoLayoutArquivo());

            saida.getOcorrencias().add(item);
        }

        return saida;
    }

    /**
     * (non-Javadoc)
     * 
     * @see br.com.bradesco.web.pgit.service.business.combo.IComboService#consultarServicosCatalogo
     * (br.com.bradesco.web.pgit.service.business.combo.bean.ConsultarServicosCatalogoEntradaDTO)
     */
    public ConsultarServicosCatalogoSaidaDTO consultarServicosCatalogo(ConsultarServicosCatalogoEntradaDTO entrada) {
        ConsultarServicosCatalogoRequest request = new ConsultarServicosCatalogoRequest();

        request.setCdProdtRelacionadoCdps(verificaIntegerNulo(entrada.getCdProdtRelacionadoCdps()));
        request.setCdProducaoServicoCdps(verificaIntegerNulo(entrada.getCdProducaoServicoCdps()));
        request.setCdProdutoPgit(verificaLongNulo(entrada.getCdProdutoPgit()));
        request.setCdTipoAcesso(verificaIntegerNulo(entrada.getCdTipoAcesso()));

        ConsultarServicosCatalogoResponse response =
            getFactoryAdapter().getConsultarServicosCatalogoPDCAdapter().invokeProcess(request);

        ConsultarServicosCatalogoSaidaDTO saida = new ConsultarServicosCatalogoSaidaDTO();

        saida.setCdProdtPgit(response.getCdProdtPgit());
        saida.setCdProdtRelacionadoCdps(response.getCdProdtRelacionadoCdps());
        saida.setCdProduServicoCdps(response.getCdProduServicoCdps());
        saida.setCdSituacaoServc(response.getCdSituacaoServc());
        saida.setCdTipoRelacionadoCdps(response.getCdTipoRelacionadoCdps());
        saida.setCodMensagem(response.getCodMensagem());
        saida.setMensagem(response.getMensagem());

        return saida;
    }
}
