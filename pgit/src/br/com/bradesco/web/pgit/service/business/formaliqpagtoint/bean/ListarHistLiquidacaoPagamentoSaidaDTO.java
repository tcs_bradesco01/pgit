/*
 * Nome: br.com.bradesco.web.pgit.service.business.formaliqpagtoint.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.formaliqpagtoint.bean;

import br.com.bradesco.web.pgit.service.data.pdc.listarhistliquidacaopagamento.response.Ocorrencias;

/**
 * Nome: ListarHistLiquidacaoPagamentoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarHistLiquidacaoPagamentoSaidaDTO {
    
    /** Atributo hrManutencaoRegistro. */
    private String hrManutencaoRegistro;
    
    /** Atributo cdUsuarioManutencao. */
    private String cdUsuarioManutencao;
    
    /** Atributo dsTipoManutencao. */
    private String dsTipoManutencao;
    
    /** Atributo hrInclusaoRegistro. */
    private String hrInclusaoRegistro;
    
    /** Atributo cdFormaLiquidacao. */
    private Integer cdFormaLiquidacao;
    
    /** Atributo dsFormaLiquidacao. */
    private String dsFormaLiquidacao;    
   
	/**
	 * Listar hist liquidacao pagamento saida dto.
	 */
	public ListarHistLiquidacaoPagamentoSaidaDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * Listar hist liquidacao pagamento saida dto.
	 *
	 * @param ocorrencia the ocorrencia
	 */
	public ListarHistLiquidacaoPagamentoSaidaDTO(Ocorrencias ocorrencia) {
	    this.hrManutencaoRegistro = ocorrencia.getHrManutencaoRegistro();
	    this.cdUsuarioManutencao = ocorrencia.getCdUsuarioManutencao();
	    this.dsTipoManutencao = ocorrencia.getDsTipoManutencao();
	    this.hrInclusaoRegistro = ocorrencia.getHrInclusaoRegistro();
	    this.cdFormaLiquidacao = ocorrencia.getCdFormaLiquidacao();
	    this.dsFormaLiquidacao = ocorrencia.getDsFormaLiquidacao();
	}
	
	/**
	 * Get: cdUsuarioManutencao.
	 *
	 * @return cdUsuarioManutencao
	 */
	public String getCdUsuarioManutencao() {
		return cdUsuarioManutencao;
	}
	
	/**
	 * Set: cdUsuarioManutencao.
	 *
	 * @param cdUsuarioManutencao the cd usuario manutencao
	 */
	public void setCdUsuarioManutencao(String cdUsuarioManutencao) {
		this.cdUsuarioManutencao = cdUsuarioManutencao;
	}
	
	/**
	 * Get: dsTipoManutencao.
	 *
	 * @return dsTipoManutencao
	 */
	public String getDsTipoManutencao() {
		return dsTipoManutencao;
	}
	
	/**
	 * Set: dsTipoManutencao.
	 *
	 * @param dsTipoManutencao the ds tipo manutencao
	 */
	public void setDsTipoManutencao(String dsTipoManutencao) {
		this.dsTipoManutencao = dsTipoManutencao;
	}
	
	/**
	 * Get: hrInclusaoRegistro.
	 *
	 * @return hrInclusaoRegistro
	 */
	public String getHrInclusaoRegistro() {
		return hrInclusaoRegistro;
	}
	
	/**
	 * Set: hrInclusaoRegistro.
	 *
	 * @param hrInclusaoRegistro the hr inclusao registro
	 */
	public void setHrInclusaoRegistro(String hrInclusaoRegistro) {
		this.hrInclusaoRegistro = hrInclusaoRegistro;
	}
	
	/**
	 * Get: hrManutencaoRegistro.
	 *
	 * @return hrManutencaoRegistro
	 */
	public String getHrManutencaoRegistro() {
		return hrManutencaoRegistro;
	}
	
	/**
	 * Set: hrManutencaoRegistro.
	 *
	 * @param hrManutencaoRegistro the hr manutencao registro
	 */
	public void setHrManutencaoRegistro(String hrManutencaoRegistro) {
		this.hrManutencaoRegistro = hrManutencaoRegistro;
	}
    
	/**
	 * Get: cdFormaLiquidacao.
	 *
	 * @return cdFormaLiquidacao
	 */
	public Integer getCdFormaLiquidacao() {
		return cdFormaLiquidacao;
	}

	/**
	 * Set: cdFormaLiquidacao.
	 *
	 * @param cdFormaLiquidacao the cd forma liquidacao
	 */
	public void setCdFormaLiquidacao(Integer cdFormaLiquidacao) {
		this.cdFormaLiquidacao = cdFormaLiquidacao;
	}

	/**
	 * Get: dsFormaLiquidacao.
	 *
	 * @return dsFormaLiquidacao
	 */
	public String getDsFormaLiquidacao() {
		return dsFormaLiquidacao;
	}

	/**
	 * Set: dsFormaLiquidacao.
	 *
	 * @param dsFormaLiquidacao the ds forma liquidacao
	 */
	public void setDsFormaLiquidacao(String dsFormaLiquidacao) {
		this.dsFormaLiquidacao = dsFormaLiquidacao;
	}
}
