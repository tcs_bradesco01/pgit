/*
 * Nome: br.com.bradesco.web.pgit.service.business.prioridadecredtipocomp.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.prioridadecredtipocomp.bean;

/**
 * Nome: DetalharPrioridadeCredTipoCompEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class DetalharPrioridadeCredTipoCompEntradaDTO {
	
	/** Atributo tipoCompromisso. */
	private int tipoCompromisso;

	
	
	/**
	 * Get: tipoCompromisso.
	 *
	 * @return tipoCompromisso
	 */
	public int getTipoCompromisso() {
		return tipoCompromisso;
	}

	/**
	 * Set: tipoCompromisso.
	 *
	 * @param tipoCompromisso the tipo compromisso
	 */
	public void setTipoCompromisso(int tipoCompromisso) {
		this.tipoCompromisso = tipoCompromisso;
	}

	
	
	
	
	
	
	

}
