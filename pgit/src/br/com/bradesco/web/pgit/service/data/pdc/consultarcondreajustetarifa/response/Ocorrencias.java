/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultarcondreajustetarifa.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdTipoReajuste
     */
    private int _cdTipoReajuste = 0;

    /**
     * keeps track of state for field: _cdTipoReajuste
     */
    private boolean _has_cdTipoReajuste;

    /**
     * Field _dsTipoReajusteTarifa
     */
    private java.lang.String _dsTipoReajusteTarifa;

    /**
     * Field _qtMesReajusteTarifa
     */
    private int _qtMesReajusteTarifa = 0;

    /**
     * keeps track of state for field: _qtMesReajusteTarifa
     */
    private boolean _has_qtMesReajusteTarifa;

    /**
     * Field _cdIndicadorEconomicoReajuste
     */
    private int _cdIndicadorEconomicoReajuste = 0;

    /**
     * keeps track of state for field: _cdIndicadorEconomicoReajuste
     */
    private boolean _has_cdIndicadorEconomicoReajuste;

    /**
     * Field _dsIndicadorEconomicoReajuste
     */
    private java.lang.String _dsIndicadorEconomicoReajuste;

    /**
     * Field _cdPercentualIndicadorReajusteTarifa
     */
    private double _cdPercentualIndicadorReajusteTarifa = 0;

    /**
     * keeps track of state for field:
     * _cdPercentualIndicadorReajusteTarifa
     */
    private boolean _has_cdPercentualIndicadorReajusteTarifa;

    /**
     * Field _cdPercentualReducaoTarifaCatalogo
     */
    private double _cdPercentualReducaoTarifaCatalogo = 0;

    /**
     * keeps track of state for field:
     * _cdPercentualReducaoTarifaCatalogo
     */
    private boolean _has_cdPercentualReducaoTarifaCatalogo;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarcondreajustetarifa.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdIndicadorEconomicoReajuste
     * 
     */
    public void deleteCdIndicadorEconomicoReajuste()
    {
        this._has_cdIndicadorEconomicoReajuste= false;
    } //-- void deleteCdIndicadorEconomicoReajuste() 

    /**
     * Method deleteCdPercentualIndicadorReajusteTarifa
     * 
     */
    public void deleteCdPercentualIndicadorReajusteTarifa()
    {
        this._has_cdPercentualIndicadorReajusteTarifa= false;
    } //-- void deleteCdPercentualIndicadorReajusteTarifa() 

    /**
     * Method deleteCdPercentualReducaoTarifaCatalogo
     * 
     */
    public void deleteCdPercentualReducaoTarifaCatalogo()
    {
        this._has_cdPercentualReducaoTarifaCatalogo= false;
    } //-- void deleteCdPercentualReducaoTarifaCatalogo() 

    /**
     * Method deleteCdTipoReajuste
     * 
     */
    public void deleteCdTipoReajuste()
    {
        this._has_cdTipoReajuste= false;
    } //-- void deleteCdTipoReajuste() 

    /**
     * Method deleteQtMesReajusteTarifa
     * 
     */
    public void deleteQtMesReajusteTarifa()
    {
        this._has_qtMesReajusteTarifa= false;
    } //-- void deleteQtMesReajusteTarifa() 

    /**
     * Returns the value of field 'cdIndicadorEconomicoReajuste'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorEconomicoReajuste'.
     */
    public int getCdIndicadorEconomicoReajuste()
    {
        return this._cdIndicadorEconomicoReajuste;
    } //-- int getCdIndicadorEconomicoReajuste() 

    /**
     * Returns the value of field
     * 'cdPercentualIndicadorReajusteTarifa'.
     * 
     * @return double
     * @return the value of field
     * 'cdPercentualIndicadorReajusteTarifa'.
     */
    public double getCdPercentualIndicadorReajusteTarifa()
    {
        return this._cdPercentualIndicadorReajusteTarifa;
    } //-- double getCdPercentualIndicadorReajusteTarifa() 

    /**
     * Returns the value of field
     * 'cdPercentualReducaoTarifaCatalogo'.
     * 
     * @return double
     * @return the value of field
     * 'cdPercentualReducaoTarifaCatalogo'.
     */
    public double getCdPercentualReducaoTarifaCatalogo()
    {
        return this._cdPercentualReducaoTarifaCatalogo;
    } //-- double getCdPercentualReducaoTarifaCatalogo() 

    /**
     * Returns the value of field 'cdTipoReajuste'.
     * 
     * @return int
     * @return the value of field 'cdTipoReajuste'.
     */
    public int getCdTipoReajuste()
    {
        return this._cdTipoReajuste;
    } //-- int getCdTipoReajuste() 

    /**
     * Returns the value of field 'dsIndicadorEconomicoReajuste'.
     * 
     * @return String
     * @return the value of field 'dsIndicadorEconomicoReajuste'.
     */
    public java.lang.String getDsIndicadorEconomicoReajuste()
    {
        return this._dsIndicadorEconomicoReajuste;
    } //-- java.lang.String getDsIndicadorEconomicoReajuste() 

    /**
     * Returns the value of field 'dsTipoReajusteTarifa'.
     * 
     * @return String
     * @return the value of field 'dsTipoReajusteTarifa'.
     */
    public java.lang.String getDsTipoReajusteTarifa()
    {
        return this._dsTipoReajusteTarifa;
    } //-- java.lang.String getDsTipoReajusteTarifa() 

    /**
     * Returns the value of field 'qtMesReajusteTarifa'.
     * 
     * @return int
     * @return the value of field 'qtMesReajusteTarifa'.
     */
    public int getQtMesReajusteTarifa()
    {
        return this._qtMesReajusteTarifa;
    } //-- int getQtMesReajusteTarifa() 

    /**
     * Method hasCdIndicadorEconomicoReajuste
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorEconomicoReajuste()
    {
        return this._has_cdIndicadorEconomicoReajuste;
    } //-- boolean hasCdIndicadorEconomicoReajuste() 

    /**
     * Method hasCdPercentualIndicadorReajusteTarifa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPercentualIndicadorReajusteTarifa()
    {
        return this._has_cdPercentualIndicadorReajusteTarifa;
    } //-- boolean hasCdPercentualIndicadorReajusteTarifa() 

    /**
     * Method hasCdPercentualReducaoTarifaCatalogo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPercentualReducaoTarifaCatalogo()
    {
        return this._has_cdPercentualReducaoTarifaCatalogo;
    } //-- boolean hasCdPercentualReducaoTarifaCatalogo() 

    /**
     * Method hasCdTipoReajuste
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoReajuste()
    {
        return this._has_cdTipoReajuste;
    } //-- boolean hasCdTipoReajuste() 

    /**
     * Method hasQtMesReajusteTarifa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtMesReajusteTarifa()
    {
        return this._has_qtMesReajusteTarifa;
    } //-- boolean hasQtMesReajusteTarifa() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdIndicadorEconomicoReajuste'.
     * 
     * @param cdIndicadorEconomicoReajuste the value of field
     * 'cdIndicadorEconomicoReajuste'.
     */
    public void setCdIndicadorEconomicoReajuste(int cdIndicadorEconomicoReajuste)
    {
        this._cdIndicadorEconomicoReajuste = cdIndicadorEconomicoReajuste;
        this._has_cdIndicadorEconomicoReajuste = true;
    } //-- void setCdIndicadorEconomicoReajuste(int) 

    /**
     * Sets the value of field
     * 'cdPercentualIndicadorReajusteTarifa'.
     * 
     * @param cdPercentualIndicadorReajusteTarifa the value of
     * field 'cdPercentualIndicadorReajusteTarifa'.
     */
    public void setCdPercentualIndicadorReajusteTarifa(double cdPercentualIndicadorReajusteTarifa)
    {
        this._cdPercentualIndicadorReajusteTarifa = cdPercentualIndicadorReajusteTarifa;
        this._has_cdPercentualIndicadorReajusteTarifa = true;
    } //-- void setCdPercentualIndicadorReajusteTarifa(double) 

    /**
     * Sets the value of field 'cdPercentualReducaoTarifaCatalogo'.
     * 
     * @param cdPercentualReducaoTarifaCatalogo the value of field
     * 'cdPercentualReducaoTarifaCatalogo'.
     */
    public void setCdPercentualReducaoTarifaCatalogo(double cdPercentualReducaoTarifaCatalogo)
    {
        this._cdPercentualReducaoTarifaCatalogo = cdPercentualReducaoTarifaCatalogo;
        this._has_cdPercentualReducaoTarifaCatalogo = true;
    } //-- void setCdPercentualReducaoTarifaCatalogo(double) 

    /**
     * Sets the value of field 'cdTipoReajuste'.
     * 
     * @param cdTipoReajuste the value of field 'cdTipoReajuste'.
     */
    public void setCdTipoReajuste(int cdTipoReajuste)
    {
        this._cdTipoReajuste = cdTipoReajuste;
        this._has_cdTipoReajuste = true;
    } //-- void setCdTipoReajuste(int) 

    /**
     * Sets the value of field 'dsIndicadorEconomicoReajuste'.
     * 
     * @param dsIndicadorEconomicoReajuste the value of field
     * 'dsIndicadorEconomicoReajuste'.
     */
    public void setDsIndicadorEconomicoReajuste(java.lang.String dsIndicadorEconomicoReajuste)
    {
        this._dsIndicadorEconomicoReajuste = dsIndicadorEconomicoReajuste;
    } //-- void setDsIndicadorEconomicoReajuste(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoReajusteTarifa'.
     * 
     * @param dsTipoReajusteTarifa the value of field
     * 'dsTipoReajusteTarifa'.
     */
    public void setDsTipoReajusteTarifa(java.lang.String dsTipoReajusteTarifa)
    {
        this._dsTipoReajusteTarifa = dsTipoReajusteTarifa;
    } //-- void setDsTipoReajusteTarifa(java.lang.String) 

    /**
     * Sets the value of field 'qtMesReajusteTarifa'.
     * 
     * @param qtMesReajusteTarifa the value of field
     * 'qtMesReajusteTarifa'.
     */
    public void setQtMesReajusteTarifa(int qtMesReajusteTarifa)
    {
        this._qtMesReajusteTarifa = qtMesReajusteTarifa;
        this._has_qtMesReajusteTarifa = true;
    } //-- void setQtMesReajusteTarifa(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultarcondreajustetarifa.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultarcondreajustetarifa.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultarcondreajustetarifa.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarcondreajustetarifa.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
