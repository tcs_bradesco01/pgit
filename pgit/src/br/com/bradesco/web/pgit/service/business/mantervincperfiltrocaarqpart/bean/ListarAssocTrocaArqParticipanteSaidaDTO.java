/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantervincperfiltrocaarqpart.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantervincperfiltrocaarqpart.bean;

import br.com.bradesco.web.pgit.utils.CpfCnpjUtils;

/**
 * Nome: ListarAssocTrocaArqParticipanteSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarAssocTrocaArqParticipanteSaidaDTO {

	/** Atributo codMensagem. */
	private String codMensagem;

	/** Atributo mensagem. */
	private String mensagem;

	/** Atributo numeroLinhas. */
	private Integer numeroLinhas;

	/** Atributo cdClub. */
	private Long cdClub;

	/** Atributo cdCpfCnpj. */
	private Long cdCpfCnpj;

	/** Atributo cdCnpjContrato. */
	private Integer cdCnpjContrato;

	/** Atributo cdCpfCnpjDigito. */
	private Integer cdCpfCnpjDigito;

	/** Atributo cdRazaoSocial. */
	private String cdRazaoSocial;

	/** Atributo cdTipoLayoutArquivo. */
	private Integer cdTipoLayoutArquivo;

	/** Atributo cdPerfilTrocaArq. */
	private Long cdPerfilTrocaArq;

	/** Atributo cdTipoParticipacaoPessoa. */
	private Integer cdTipoParticipacaoPessoa;

	/** Atributo dsTipoParticipante. */
	private String dsTipoParticipante;

	/** Atributo dsTipoLayoutArquivo. */
	private String dsTipoLayoutArquivo;

	/** Atributo dsLayoutProprio. */
	private String dsLayoutProprio;

	/** Atributo cdProdutoServicoOperacao. */
	private Integer cdProdutoServicoOperacao;

	/** Atributo cdAplicacaoTransmPagamento. */
	private Integer cdAplicacaoTransmPagamento;

	/** Atributo dsProdutoServico. */
	private String dsProdutoServico;

	/** Atributo dsAplicacaoTransmicaoPagamento. */
	private String dsAplicacaoTransmicaoPagamento;

	/** Atributo cdMeioPrincipalRemessa. */
	private Integer cdMeioPrincipalRemessa;

	/** Atributo dsMeioPrincRemss. */
	private String dsMeioPrincRemss;

	/** Atributo cdMeioAlternRemessa. */
	private Integer cdMeioAlternRemessa;

	/** Atributo dsMeioAltrnRemss. */
	private String dsMeioAltrnRemss;

	/** Atributo cdMeioPrincipalRetorno. */
	private Integer cdMeioPrincipalRetorno;

	/** Atributo dsMeioPrincRetor. */
	private String dsMeioPrincRetor;

	/** Atributo cdMeioAltrnRetorno. */
	private Integer cdMeioAltrnRetorno;

	/** Atributo dsMeioAltrnRetor. */
	private String dsMeioAltrnRetor;

	/** Atributo cdPessoaJuridicaParceiro. */
	private Long cdPessoaJuridicaParceiro;

	/** Atributo dsRzScialPcero. */
	private String dsRzScialPcero;

	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}

	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}

	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}

	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	/**
	 * Get: numeroLinhas.
	 *
	 * @return numeroLinhas
	 */
	public Integer getNumeroLinhas() {
		return numeroLinhas;
	}

	/**
	 * Set: numeroLinhas.
	 *
	 * @param numeroLinhas the numero linhas
	 */
	public void setNumeroLinhas(Integer numeroLinhas) {
		this.numeroLinhas = numeroLinhas;
	}

	/**
	 * Get: cdClub.
	 *
	 * @return cdClub
	 */
	public Long getCdClub() {
		return cdClub;
	}

	/**
	 * Set: cdClub.
	 *
	 * @param cdClub the cd club
	 */
	public void setCdClub(Long cdClub) {
		this.cdClub = cdClub;
	}

	/**
	 * Get: cdCpfCnpj.
	 *
	 * @return cdCpfCnpj
	 */
	public Long getCdCpfCnpj() {
		return cdCpfCnpj;
	}

	/**
	 * Set: cdCpfCnpj.
	 *
	 * @param cdCpfCnpj the cd cpf cnpj
	 */
	public void setCdCpfCnpj(Long cdCpfCnpj) {
		this.cdCpfCnpj = cdCpfCnpj;
	}

	/**
	 * Get: cdCnpjContrato.
	 *
	 * @return cdCnpjContrato
	 */
	public Integer getCdCnpjContrato() {
		return cdCnpjContrato;
	}

	/**
	 * Set: cdCnpjContrato.
	 *
	 * @param cdCnpjContrato the cd cnpj contrato
	 */
	public void setCdCnpjContrato(Integer cdCnpjContrato) {
		this.cdCnpjContrato = cdCnpjContrato;
	}

	/**
	 * Get: cdCpfCnpjDigito.
	 *
	 * @return cdCpfCnpjDigito
	 */
	public Integer getCdCpfCnpjDigito() {
		return cdCpfCnpjDigito;
	}

	/**
	 * Set: cdCpfCnpjDigito.
	 *
	 * @param cdCpfCnpjDigito the cd cpf cnpj digito
	 */
	public void setCdCpfCnpjDigito(Integer cdCpfCnpjDigito) {
		this.cdCpfCnpjDigito = cdCpfCnpjDigito;
	}

	/**
	 * Get: cdRazaoSocial.
	 *
	 * @return cdRazaoSocial
	 */
	public String getCdRazaoSocial() {
		return cdRazaoSocial;
	}

	/**
	 * Set: cdRazaoSocial.
	 *
	 * @param cdRazaoSocial the cd razao social
	 */
	public void setCdRazaoSocial(String cdRazaoSocial) {
		this.cdRazaoSocial = cdRazaoSocial;
	}

	/**
	 * Get: cdTipoLayoutArquivo.
	 *
	 * @return cdTipoLayoutArquivo
	 */
	public Integer getCdTipoLayoutArquivo() {
		return cdTipoLayoutArquivo;
	}

	/**
	 * Set: cdTipoLayoutArquivo.
	 *
	 * @param cdTipoLayoutArquivo the cd tipo layout arquivo
	 */
	public void setCdTipoLayoutArquivo(Integer cdTipoLayoutArquivo) {
		this.cdTipoLayoutArquivo = cdTipoLayoutArquivo;
	}

	/**
	 * Get: cdPerfilTrocaArq.
	 *
	 * @return cdPerfilTrocaArq
	 */
	public Long getCdPerfilTrocaArq() {
		return cdPerfilTrocaArq;
	}

	/**
	 * Set: cdPerfilTrocaArq.
	 *
	 * @param cdPerfilTrocaArq the cd perfil troca arq
	 */
	public void setCdPerfilTrocaArq(Long cdPerfilTrocaArq) {
		this.cdPerfilTrocaArq = cdPerfilTrocaArq;
	}

	/**
	 * Get: cdTipoParticipacaoPessoa.
	 *
	 * @return cdTipoParticipacaoPessoa
	 */
	public Integer getCdTipoParticipacaoPessoa() {
		return cdTipoParticipacaoPessoa;
	}

	/**
	 * Set: cdTipoParticipacaoPessoa.
	 *
	 * @param cdTipoParticipacaoPessoa the cd tipo participacao pessoa
	 */
	public void setCdTipoParticipacaoPessoa(Integer cdTipoParticipacaoPessoa) {
		this.cdTipoParticipacaoPessoa = cdTipoParticipacaoPessoa;
	}

	/**
	 * Get: dsTipoParticipante.
	 *
	 * @return dsTipoParticipante
	 */
	public String getDsTipoParticipante() {
		return dsTipoParticipante;
	}

	/**
	 * Set: dsTipoParticipante.
	 *
	 * @param dsTipoParticipante the ds tipo participante
	 */
	public void setDsTipoParticipante(String dsTipoParticipante) {
		this.dsTipoParticipante = dsTipoParticipante;
	}

	/**
	 * Get: dsTipoLayoutArquivo.
	 *
	 * @return dsTipoLayoutArquivo
	 */
	public String getDsTipoLayoutArquivo() {
		return dsTipoLayoutArquivo;
	}

	/**
	 * Set: dsTipoLayoutArquivo.
	 *
	 * @param dsTipoLayoutArquivo the ds tipo layout arquivo
	 */
	public void setDsTipoLayoutArquivo(String dsTipoLayoutArquivo) {
		this.dsTipoLayoutArquivo = dsTipoLayoutArquivo;
	}

	/**
	 * Get: dsLayoutProprio.
	 *
	 * @return dsLayoutProprio
	 */
	public String getDsLayoutProprio() {
		return dsLayoutProprio;
	}

	/**
	 * Set: dsLayoutProprio.
	 *
	 * @param dsLayoutProprio the ds layout proprio
	 */
	public void setDsLayoutProprio(String dsLayoutProprio) {
		this.dsLayoutProprio = dsLayoutProprio;
	}

	/**
	 * Get: cdProdutoServicoOperacao.
	 *
	 * @return cdProdutoServicoOperacao
	 */
	public Integer getCdProdutoServicoOperacao() {
		return cdProdutoServicoOperacao;
	}

	/**
	 * Set: cdProdutoServicoOperacao.
	 *
	 * @param cdProdutoServicoOperacao the cd produto servico operacao
	 */
	public void setCdProdutoServicoOperacao(Integer cdProdutoServicoOperacao) {
		this.cdProdutoServicoOperacao = cdProdutoServicoOperacao;
	}

	/**
	 * Get: cdAplicacaoTransmPagamento.
	 *
	 * @return cdAplicacaoTransmPagamento
	 */
	public Integer getCdAplicacaoTransmPagamento() {
		return cdAplicacaoTransmPagamento;
	}

	/**
	 * Set: cdAplicacaoTransmPagamento.
	 *
	 * @param cdAplicacaoTransmPagamento the cd aplicacao transm pagamento
	 */
	public void setCdAplicacaoTransmPagamento(Integer cdAplicacaoTransmPagamento) {
		this.cdAplicacaoTransmPagamento = cdAplicacaoTransmPagamento;
	}

	/**
	 * Get: dsProdutoServico.
	 *
	 * @return dsProdutoServico
	 */
	public String getDsProdutoServico() {
		return dsProdutoServico;
	}

	/**
	 * Set: dsProdutoServico.
	 *
	 * @param dsProdutoServico the ds produto servico
	 */
	public void setDsProdutoServico(String dsProdutoServico) {
		this.dsProdutoServico = dsProdutoServico;
	}

	/**
	 * Get: dsAplicacaoTransmicaoPagamento.
	 *
	 * @return dsAplicacaoTransmicaoPagamento
	 */
	public String getDsAplicacaoTransmicaoPagamento() {
		return dsAplicacaoTransmicaoPagamento;
	}

	/**
	 * Set: dsAplicacaoTransmicaoPagamento.
	 *
	 * @param dsAplicacaoTransmicaoPagamento the ds aplicacao transmicao pagamento
	 */
	public void setDsAplicacaoTransmicaoPagamento(String dsAplicacaoTransmicaoPagamento) {
		this.dsAplicacaoTransmicaoPagamento = dsAplicacaoTransmicaoPagamento;
	}

	/**
	 * Get: cdMeioPrincipalRemessa.
	 *
	 * @return cdMeioPrincipalRemessa
	 */
	public Integer getCdMeioPrincipalRemessa() {
		return cdMeioPrincipalRemessa;
	}

	/**
	 * Set: cdMeioPrincipalRemessa.
	 *
	 * @param cdMeioPrincipalRemessa the cd meio principal remessa
	 */
	public void setCdMeioPrincipalRemessa(Integer cdMeioPrincipalRemessa) {
		this.cdMeioPrincipalRemessa = cdMeioPrincipalRemessa;
	}

	/**
	 * Get: dsMeioPrincRemss.
	 *
	 * @return dsMeioPrincRemss
	 */
	public String getDsMeioPrincRemss() {
		return dsMeioPrincRemss;
	}

	/**
	 * Set: dsMeioPrincRemss.
	 *
	 * @param dsMeioPrincRemss the ds meio princ remss
	 */
	public void setDsMeioPrincRemss(String dsMeioPrincRemss) {
		this.dsMeioPrincRemss = dsMeioPrincRemss;
	}

	/**
	 * Get: cdMeioAlternRemessa.
	 *
	 * @return cdMeioAlternRemessa
	 */
	public Integer getCdMeioAlternRemessa() {
		return cdMeioAlternRemessa;
	}

	/**
	 * Set: cdMeioAlternRemessa.
	 *
	 * @param cdMeioAlternRemessa the cd meio altern remessa
	 */
	public void setCdMeioAlternRemessa(Integer cdMeioAlternRemessa) {
		this.cdMeioAlternRemessa = cdMeioAlternRemessa;
	}

	/**
	 * Get: dsMeioAltrnRemss.
	 *
	 * @return dsMeioAltrnRemss
	 */
	public String getDsMeioAltrnRemss() {
		return dsMeioAltrnRemss;
	}

	/**
	 * Set: dsMeioAltrnRemss.
	 *
	 * @param dsMeioAltrnRemss the ds meio altrn remss
	 */
	public void setDsMeioAltrnRemss(String dsMeioAltrnRemss) {
		this.dsMeioAltrnRemss = dsMeioAltrnRemss;
	}

	/**
	 * Get: cdMeioPrincipalRetorno.
	 *
	 * @return cdMeioPrincipalRetorno
	 */
	public Integer getCdMeioPrincipalRetorno() {
		return cdMeioPrincipalRetorno;
	}

	/**
	 * Set: cdMeioPrincipalRetorno.
	 *
	 * @param cdMeioPrincipalRetorno the cd meio principal retorno
	 */
	public void setCdMeioPrincipalRetorno(Integer cdMeioPrincipalRetorno) {
		this.cdMeioPrincipalRetorno = cdMeioPrincipalRetorno;
	}

	/**
	 * Get: dsMeioPrincRetor.
	 *
	 * @return dsMeioPrincRetor
	 */
	public String getDsMeioPrincRetor() {
		return dsMeioPrincRetor;
	}

	/**
	 * Set: dsMeioPrincRetor.
	 *
	 * @param dsMeioPrincRetor the ds meio princ retor
	 */
	public void setDsMeioPrincRetor(String dsMeioPrincRetor) {
		this.dsMeioPrincRetor = dsMeioPrincRetor;
	}

	/**
	 * Get: cdMeioAltrnRetorno.
	 *
	 * @return cdMeioAltrnRetorno
	 */
	public Integer getCdMeioAltrnRetorno() {
		return cdMeioAltrnRetorno;
	}

	/**
	 * Set: cdMeioAltrnRetorno.
	 *
	 * @param cdMeioAltrnRetorno the cd meio altrn retorno
	 */
	public void setCdMeioAltrnRetorno(Integer cdMeioAltrnRetorno) {
		this.cdMeioAltrnRetorno = cdMeioAltrnRetorno;
	}

	/**
	 * Get: dsMeioAltrnRetor.
	 *
	 * @return dsMeioAltrnRetor
	 */
	public String getDsMeioAltrnRetor() {
		return dsMeioAltrnRetor;
	}

	/**
	 * Set: dsMeioAltrnRetor.
	 *
	 * @param dsMeioAltrnRetor the ds meio altrn retor
	 */
	public void setDsMeioAltrnRetor(String dsMeioAltrnRetor) {
		this.dsMeioAltrnRetor = dsMeioAltrnRetor;
	}

	/**
	 * Get: cdPessoaJuridicaParceiro.
	 *
	 * @return cdPessoaJuridicaParceiro
	 */
	public Long getCdPessoaJuridicaParceiro() {
		return cdPessoaJuridicaParceiro;
	}

	/**
	 * Set: cdPessoaJuridicaParceiro.
	 *
	 * @param cdPessoaJuridicaParceiro the cd pessoa juridica parceiro
	 */
	public void setCdPessoaJuridicaParceiro(Long cdPessoaJuridicaParceiro) {
		this.cdPessoaJuridicaParceiro = cdPessoaJuridicaParceiro;
	}

	/**
	 * Get: dsRzScialPcero.
	 *
	 * @return dsRzScialPcero
	 */
	public String getDsRzScialPcero() {
		return dsRzScialPcero;
	}

	/**
	 * Set: dsRzScialPcero.
	 *
	 * @param dsRzScialPcero the ds rz scial pcero
	 */
	public void setDsRzScialPcero(String dsRzScialPcero) {
		this.dsRzScialPcero = dsRzScialPcero;
	}

	/**
	 * Get: cpfCnpjFormatado.
	 *
	 * @return cpfCnpjFormatado
	 */
	public String getCpfCnpjFormatado() {
		return CpfCnpjUtils.formatarCpfCnpj(cdCpfCnpj, cdCnpjContrato, cdCpfCnpjDigito);
	}
}