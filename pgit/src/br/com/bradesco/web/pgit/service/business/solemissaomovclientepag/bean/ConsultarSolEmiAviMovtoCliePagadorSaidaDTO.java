/*
 * Nome: br.com.bradesco.web.pgit.service.business.solemissaomovclientepag.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.solemissaomovclientepag.bean;

/**
 * Nome: ConsultarSolEmiAviMovtoCliePagadorSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarSolEmiAviMovtoCliePagadorSaidaDTO {
    
    /** Atributo codMensagem. */
    private String codMensagem;
    
    /** Atributo mensagem. */
    private String mensagem;
    
    /** Atributo cdTipoSolicitacao. */
    private Integer cdTipoSolicitacao;
    
    /** Atributo nrSolicitacao. */
    private Integer nrSolicitacao;
    
    /** Atributo dtHoraSolicitacao. */
    private String dtHoraSolicitacao;
    
    /** Atributo dtHoraAtendimento. */
    private String dtHoraAtendimento;
    
    /** Atributo cdSituacaoSolicitacao. */
    private Integer cdSituacaoSolicitacao;
    
    /** Atributo dsSituacaoSolicitacao. */
    private String dsSituacaoSolicitacao;
    
    /** Atributo cdMotivoSituacao. */
    private Integer cdMotivoSituacao;
    
    /** Atributo dsMotivoSituacao. */
    private String dsMotivoSituacao;
    
    /** Atributo cdTipoCanal. */
    private Integer cdTipoCanal;
    
    /** Atributo dsTipoCanal. */
    private String dsTipoCanal;
    
    /** Atributo cdPessoaJuridica. */
    private Long cdPessoaJuridica;
    
    /** Atributo cdTipoContrato. */
    private Integer cdTipoContrato;
    
    /** Atributo nrSequenciaContrato. */
    private Long nrSequenciaContrato;
    
    /** Atributo cdUsuarioInclusao. */
    private String cdUsuarioInclusao;
    
    /** Atributo contratoFormatado. */
    private String contratoFormatado;
    
    /** Atributo situacaoFormatada. */
    private String situacaoFormatada;
    
    /** Atributo motivoFormatado. */
    private String motivoFormatado;
    
    /** Atributo dtHoraSolFormatada. */
    private String dtHoraSolFormatada;
    
	/**
	 * Get: dtHoraSolFormatada.
	 *
	 * @return dtHoraSolFormatada
	 */
	public String getDtHoraSolFormatada() {
		return dtHoraSolFormatada;
	}
	
	/**
	 * Set: dtHoraSolFormatada.
	 *
	 * @param dtHoraSolFormatada the dt hora sol formatada
	 */
	public void setDtHoraSolFormatada(String dtHoraSolFormatada) {
		this.dtHoraSolFormatada = dtHoraSolFormatada;
	}
	
	/**
	 * Get: motivoFormatado.
	 *
	 * @return motivoFormatado
	 */
	public String getMotivoFormatado() {
		return motivoFormatado;
	}
	
	/**
	 * Set: motivoFormatado.
	 *
	 * @param motivoFormatado the motivo formatado
	 */
	public void setMotivoFormatado(String motivoFormatado) {
		this.motivoFormatado = motivoFormatado;
	}
	
	/**
	 * Get: situacaoFormatada.
	 *
	 * @return situacaoFormatada
	 */
	public String getSituacaoFormatada() {
		return situacaoFormatada;
	}
	
	/**
	 * Set: situacaoFormatada.
	 *
	 * @param situacaoFormatada the situacao formatada
	 */
	public void setSituacaoFormatada(String situacaoFormatada) {
		this.situacaoFormatada = situacaoFormatada;
	}
	
	/**
	 * Get: contratoFormatado.
	 *
	 * @return contratoFormatado
	 */
	public String getContratoFormatado() {
		return contratoFormatado;
	}
	
	/**
	 * Set: contratoFormatado.
	 *
	 * @param contratoFormatado the contrato formatado
	 */
	public void setContratoFormatado(String contratoFormatado) {
		this.contratoFormatado = contratoFormatado;
	}
	
	/**
	 * Get: cdMotivoSituacao.
	 *
	 * @return cdMotivoSituacao
	 */
	public Integer getCdMotivoSituacao() {
		return cdMotivoSituacao;
	}
	
	/**
	 * Set: cdMotivoSituacao.
	 *
	 * @param cdMotivoSituacao the cd motivo situacao
	 */
	public void setCdMotivoSituacao(Integer cdMotivoSituacao) {
		this.cdMotivoSituacao = cdMotivoSituacao;
	}
	
	/**
	 * Get: cdPessoaJuridica.
	 *
	 * @return cdPessoaJuridica
	 */
	public Long getCdPessoaJuridica() {
		return cdPessoaJuridica;
	}
	
	/**
	 * Set: cdPessoaJuridica.
	 *
	 * @param cdPessoaJuridica the cd pessoa juridica
	 */
	public void setCdPessoaJuridica(Long cdPessoaJuridica) {
		this.cdPessoaJuridica = cdPessoaJuridica;
	}
	
	/**
	 * Get: cdSituacaoSolicitacao.
	 *
	 * @return cdSituacaoSolicitacao
	 */
	public Integer getCdSituacaoSolicitacao() {
		return cdSituacaoSolicitacao;
	}
	
	/**
	 * Set: cdSituacaoSolicitacao.
	 *
	 * @param cdSituacaoSolicitacao the cd situacao solicitacao
	 */
	public void setCdSituacaoSolicitacao(Integer cdSituacaoSolicitacao) {
		this.cdSituacaoSolicitacao = cdSituacaoSolicitacao;
	}
	
	/**
	 * Get: cdTipoCanal.
	 *
	 * @return cdTipoCanal
	 */
	public Integer getCdTipoCanal() {
		return cdTipoCanal;
	}
	
	/**
	 * Set: cdTipoCanal.
	 *
	 * @param cdTipoCanal the cd tipo canal
	 */
	public void setCdTipoCanal(Integer cdTipoCanal) {
		this.cdTipoCanal = cdTipoCanal;
	}
	
	/**
	 * Get: cdTipoContrato.
	 *
	 * @return cdTipoContrato
	 */
	public Integer getCdTipoContrato() {
		return cdTipoContrato;
	}
	
	/**
	 * Set: cdTipoContrato.
	 *
	 * @param cdTipoContrato the cd tipo contrato
	 */
	public void setCdTipoContrato(Integer cdTipoContrato) {
		this.cdTipoContrato = cdTipoContrato;
	}
	
	/**
	 * Get: cdTipoSolicitacao.
	 *
	 * @return cdTipoSolicitacao
	 */
	public Integer getCdTipoSolicitacao() {
		return cdTipoSolicitacao;
	}
	
	/**
	 * Set: cdTipoSolicitacao.
	 *
	 * @param cdTipoSolicitacao the cd tipo solicitacao
	 */
	public void setCdTipoSolicitacao(Integer cdTipoSolicitacao) {
		this.cdTipoSolicitacao = cdTipoSolicitacao;
	}
	
	/**
	 * Get: cdUsuarioInclusao.
	 *
	 * @return cdUsuarioInclusao
	 */
	public String getCdUsuarioInclusao() {
		return cdUsuarioInclusao;
	}
	
	/**
	 * Set: cdUsuarioInclusao.
	 *
	 * @param cdUsuarioInclusao the cd usuario inclusao
	 */
	public void setCdUsuarioInclusao(String cdUsuarioInclusao) {
		this.cdUsuarioInclusao = cdUsuarioInclusao;
	}
	
	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}
	
	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}
	
	/**
	 * Get: dsMotivoSituacao.
	 *
	 * @return dsMotivoSituacao
	 */
	public String getDsMotivoSituacao() {
		return dsMotivoSituacao;
	}
	
	/**
	 * Set: dsMotivoSituacao.
	 *
	 * @param dsMotivoSituacao the ds motivo situacao
	 */
	public void setDsMotivoSituacao(String dsMotivoSituacao) {
		this.dsMotivoSituacao = dsMotivoSituacao;
	}
	
	/**
	 * Get: dsSituacaoSolicitacao.
	 *
	 * @return dsSituacaoSolicitacao
	 */
	public String getDsSituacaoSolicitacao() {
		return dsSituacaoSolicitacao;
	}
	
	/**
	 * Set: dsSituacaoSolicitacao.
	 *
	 * @param dsSituacaoSolicitacao the ds situacao solicitacao
	 */
	public void setDsSituacaoSolicitacao(String dsSituacaoSolicitacao) {
		this.dsSituacaoSolicitacao = dsSituacaoSolicitacao;
	}
	
	/**
	 * Get: dsTipoCanal.
	 *
	 * @return dsTipoCanal
	 */
	public String getDsTipoCanal() {
		return dsTipoCanal;
	}
	
	/**
	 * Set: dsTipoCanal.
	 *
	 * @param dsTipoCanal the ds tipo canal
	 */
	public void setDsTipoCanal(String dsTipoCanal) {
		this.dsTipoCanal = dsTipoCanal;
	}
	
	/**
	 * Get: dtHoraAtendimento.
	 *
	 * @return dtHoraAtendimento
	 */
	public String getDtHoraAtendimento() {
		return dtHoraAtendimento;
	}
	
	/**
	 * Set: dtHoraAtendimento.
	 *
	 * @param dtHoraAtendimento the dt hora atendimento
	 */
	public void setDtHoraAtendimento(String dtHoraAtendimento) {
		this.dtHoraAtendimento = dtHoraAtendimento;
	}
	
	/**
	 * Get: dtHoraSolicitacao.
	 *
	 * @return dtHoraSolicitacao
	 */
	public String getDtHoraSolicitacao() {
		return dtHoraSolicitacao;
	}
	
	/**
	 * Set: dtHoraSolicitacao.
	 *
	 * @param dtHoraSolicitacao the dt hora solicitacao
	 */
	public void setDtHoraSolicitacao(String dtHoraSolicitacao) {
		this.dtHoraSolicitacao = dtHoraSolicitacao;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	/**
	 * Get: nrSequenciaContrato.
	 *
	 * @return nrSequenciaContrato
	 */
	public Long getNrSequenciaContrato() {
		return nrSequenciaContrato;
	}
	
	/**
	 * Set: nrSequenciaContrato.
	 *
	 * @param nrSequenciaContrato the nr sequencia contrato
	 */
	public void setNrSequenciaContrato(Long nrSequenciaContrato) {
		this.nrSequenciaContrato = nrSequenciaContrato;
	}
	
	/**
	 * Get: nrSolicitacao.
	 *
	 * @return nrSolicitacao
	 */
	public Integer getNrSolicitacao() {
		return nrSolicitacao;
	}
	
	/**
	 * Set: nrSolicitacao.
	 *
	 * @param nrSolicitacao the nr solicitacao
	 */
	public void setNrSolicitacao(Integer nrSolicitacao) {
		this.nrSolicitacao = nrSolicitacao;
	}
    
    
}
