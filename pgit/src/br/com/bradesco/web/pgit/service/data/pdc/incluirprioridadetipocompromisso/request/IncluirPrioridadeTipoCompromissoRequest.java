/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.incluirprioridadetipocompromisso.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class IncluirPrioridadeTipoCompromissoRequest.
 * 
 * @version $Revision$ $Date$
 */
public class IncluirPrioridadeTipoCompromissoRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdCompromissoPrioridadeProduto
     */
    private int _cdCompromissoPrioridadeProduto = 0;

    /**
     * keeps track of state for field:
     * _cdCompromissoPrioridadeProduto
     */
    private boolean _has_cdCompromissoPrioridadeProduto;

    /**
     * Field _centroCustoPrioridadeProduto
     */
    private java.lang.String _centroCustoPrioridadeProduto;

    /**
     * Field _dtInicioVigenciaCompromisso
     */
    private java.lang.String _dtInicioVigenciaCompromisso;

    /**
     * Field _dtFimVigenciaCompromisso
     */
    private java.lang.String _dtFimVigenciaCompromisso;

    /**
     * Field _cdOrdemPrioridadeCompromisso
     */
    private int _cdOrdemPrioridadeCompromisso = 0;

    /**
     * keeps track of state for field: _cdOrdemPrioridadeCompromisso
     */
    private boolean _has_cdOrdemPrioridadeCompromisso;


      //----------------/
     //- Constructors -/
    //----------------/

    public IncluirPrioridadeTipoCompromissoRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.incluirprioridadetipocompromisso.request.IncluirPrioridadeTipoCompromissoRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdCompromissoPrioridadeProduto
     * 
     */
    public void deleteCdCompromissoPrioridadeProduto()
    {
        this._has_cdCompromissoPrioridadeProduto= false;
    } //-- void deleteCdCompromissoPrioridadeProduto() 

    /**
     * Method deleteCdOrdemPrioridadeCompromisso
     * 
     */
    public void deleteCdOrdemPrioridadeCompromisso()
    {
        this._has_cdOrdemPrioridadeCompromisso= false;
    } //-- void deleteCdOrdemPrioridadeCompromisso() 

    /**
     * Returns the value of field 'cdCompromissoPrioridadeProduto'.
     * 
     * @return int
     * @return the value of field 'cdCompromissoPrioridadeProduto'.
     */
    public int getCdCompromissoPrioridadeProduto()
    {
        return this._cdCompromissoPrioridadeProduto;
    } //-- int getCdCompromissoPrioridadeProduto() 

    /**
     * Returns the value of field 'cdOrdemPrioridadeCompromisso'.
     * 
     * @return int
     * @return the value of field 'cdOrdemPrioridadeCompromisso'.
     */
    public int getCdOrdemPrioridadeCompromisso()
    {
        return this._cdOrdemPrioridadeCompromisso;
    } //-- int getCdOrdemPrioridadeCompromisso() 

    /**
     * Returns the value of field 'centroCustoPrioridadeProduto'.
     * 
     * @return String
     * @return the value of field 'centroCustoPrioridadeProduto'.
     */
    public java.lang.String getCentroCustoPrioridadeProduto()
    {
        return this._centroCustoPrioridadeProduto;
    } //-- java.lang.String getCentroCustoPrioridadeProduto() 

    /**
     * Returns the value of field 'dtFimVigenciaCompromisso'.
     * 
     * @return String
     * @return the value of field 'dtFimVigenciaCompromisso'.
     */
    public java.lang.String getDtFimVigenciaCompromisso()
    {
        return this._dtFimVigenciaCompromisso;
    } //-- java.lang.String getDtFimVigenciaCompromisso() 

    /**
     * Returns the value of field 'dtInicioVigenciaCompromisso'.
     * 
     * @return String
     * @return the value of field 'dtInicioVigenciaCompromisso'.
     */
    public java.lang.String getDtInicioVigenciaCompromisso()
    {
        return this._dtInicioVigenciaCompromisso;
    } //-- java.lang.String getDtInicioVigenciaCompromisso() 

    /**
     * Method hasCdCompromissoPrioridadeProduto
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCompromissoPrioridadeProduto()
    {
        return this._has_cdCompromissoPrioridadeProduto;
    } //-- boolean hasCdCompromissoPrioridadeProduto() 

    /**
     * Method hasCdOrdemPrioridadeCompromisso
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdOrdemPrioridadeCompromisso()
    {
        return this._has_cdOrdemPrioridadeCompromisso;
    } //-- boolean hasCdOrdemPrioridadeCompromisso() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdCompromissoPrioridadeProduto'.
     * 
     * @param cdCompromissoPrioridadeProduto the value of field
     * 'cdCompromissoPrioridadeProduto'.
     */
    public void setCdCompromissoPrioridadeProduto(int cdCompromissoPrioridadeProduto)
    {
        this._cdCompromissoPrioridadeProduto = cdCompromissoPrioridadeProduto;
        this._has_cdCompromissoPrioridadeProduto = true;
    } //-- void setCdCompromissoPrioridadeProduto(int) 

    /**
     * Sets the value of field 'cdOrdemPrioridadeCompromisso'.
     * 
     * @param cdOrdemPrioridadeCompromisso the value of field
     * 'cdOrdemPrioridadeCompromisso'.
     */
    public void setCdOrdemPrioridadeCompromisso(int cdOrdemPrioridadeCompromisso)
    {
        this._cdOrdemPrioridadeCompromisso = cdOrdemPrioridadeCompromisso;
        this._has_cdOrdemPrioridadeCompromisso = true;
    } //-- void setCdOrdemPrioridadeCompromisso(int) 

    /**
     * Sets the value of field 'centroCustoPrioridadeProduto'.
     * 
     * @param centroCustoPrioridadeProduto the value of field
     * 'centroCustoPrioridadeProduto'.
     */
    public void setCentroCustoPrioridadeProduto(java.lang.String centroCustoPrioridadeProduto)
    {
        this._centroCustoPrioridadeProduto = centroCustoPrioridadeProduto;
    } //-- void setCentroCustoPrioridadeProduto(java.lang.String) 

    /**
     * Sets the value of field 'dtFimVigenciaCompromisso'.
     * 
     * @param dtFimVigenciaCompromisso the value of field
     * 'dtFimVigenciaCompromisso'.
     */
    public void setDtFimVigenciaCompromisso(java.lang.String dtFimVigenciaCompromisso)
    {
        this._dtFimVigenciaCompromisso = dtFimVigenciaCompromisso;
    } //-- void setDtFimVigenciaCompromisso(java.lang.String) 

    /**
     * Sets the value of field 'dtInicioVigenciaCompromisso'.
     * 
     * @param dtInicioVigenciaCompromisso the value of field
     * 'dtInicioVigenciaCompromisso'.
     */
    public void setDtInicioVigenciaCompromisso(java.lang.String dtInicioVigenciaCompromisso)
    {
        this._dtInicioVigenciaCompromisso = dtInicioVigenciaCompromisso;
    } //-- void setDtInicioVigenciaCompromisso(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return IncluirPrioridadeTipoCompromissoRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.incluirprioridadetipocompromisso.request.IncluirPrioridadeTipoCompromissoRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.incluirprioridadetipocompromisso.request.IncluirPrioridadeTipoCompromissoRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.incluirprioridadetipocompromisso.request.IncluirPrioridadeTipoCompromissoRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.incluirprioridadetipocompromisso.request.IncluirPrioridadeTipoCompromissoRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
