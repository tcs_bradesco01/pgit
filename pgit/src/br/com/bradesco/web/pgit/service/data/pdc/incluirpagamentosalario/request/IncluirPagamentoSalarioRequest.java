/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.incluirpagamentosalario.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.math.BigDecimal;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class IncluirPagamentoSalarioRequest.
 * 
 * @version $Revision$ $Date$
 */
public class IncluirPagamentoSalarioRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdpessoaJuridicaContrato
     */
    private long _cdpessoaJuridicaContrato = 0;

    /**
     * keeps track of state for field: _cdpessoaJuridicaContrato
     */
    private boolean _has_cdpessoaJuridicaContrato;

    /**
     * Field _cdTipoContratoNegocio
     */
    private int _cdTipoContratoNegocio = 0;

    /**
     * keeps track of state for field: _cdTipoContratoNegocio
     */
    private boolean _has_cdTipoContratoNegocio;

    /**
     * Field _nrSequenciaContratoNegocio
     */
    private long _nrSequenciaContratoNegocio = 0;

    /**
     * keeps track of state for field: _nrSequenciaContratoNegocio
     */
    private boolean _has_nrSequenciaContratoNegocio;

    /**
     * Field _cdProdutoServicoOperacao
     */
    private int _cdProdutoServicoOperacao = 0;

    /**
     * keeps track of state for field: _cdProdutoServicoOperacao
     */
    private boolean _has_cdProdutoServicoOperacao;

    /**
     * Field _cdProdutoOperacaoRelacionado
     */
    private int _cdProdutoOperacaoRelacionado = 0;

    /**
     * keeps track of state for field: _cdProdutoOperacaoRelacionado
     */
    private boolean _has_cdProdutoOperacaoRelacionado;

    /**
     * Field _cdClubRepresentante
     */
    private long _cdClubRepresentante = 0;

    /**
     * keeps track of state for field: _cdClubRepresentante
     */
    private boolean _has_cdClubRepresentante;

    /**
     * Field _cdCpfCnpjRepresentante
     */
    private long _cdCpfCnpjRepresentante = 0;

    /**
     * keeps track of state for field: _cdCpfCnpjRepresentante
     */
    private boolean _has_cdCpfCnpjRepresentante;

    /**
     * Field _cdFilialCnpjRepresentante
     */
    private int _cdFilialCnpjRepresentante = 0;

    /**
     * keeps track of state for field: _cdFilialCnpjRepresentante
     */
    private boolean _has_cdFilialCnpjRepresentante;

    /**
     * Field _cdControleCpfRepresentante
     */
    private int _cdControleCpfRepresentante = 0;

    /**
     * keeps track of state for field: _cdControleCpfRepresentante
     */
    private boolean _has_cdControleCpfRepresentante;

    /**
     * Field _nmRazaoRepresentante
     */
    private java.lang.String _nmRazaoRepresentante;

    /**
     * Field _cdPessoaJuridicaConta
     */
    private long _cdPessoaJuridicaConta = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridicaConta
     */
    private boolean _has_cdPessoaJuridicaConta;

    /**
     * Field _cdTipoContratoConta
     */
    private int _cdTipoContratoConta = 0;

    /**
     * keeps track of state for field: _cdTipoContratoConta
     */
    private boolean _has_cdTipoContratoConta;

    /**
     * Field _nrSequenciaContratoConta
     */
    private long _nrSequenciaContratoConta = 0;

    /**
     * keeps track of state for field: _nrSequenciaContratoConta
     */
    private boolean _has_nrSequenciaContratoConta;

    /**
     * Field _cdInstituicaoBancaria
     */
    private int _cdInstituicaoBancaria = 0;

    /**
     * keeps track of state for field: _cdInstituicaoBancaria
     */
    private boolean _has_cdInstituicaoBancaria;

    /**
     * Field _cdUnidadeOrganizacional
     */
    private int _cdUnidadeOrganizacional = 0;

    /**
     * keeps track of state for field: _cdUnidadeOrganizacional
     */
    private boolean _has_cdUnidadeOrganizacional;

    /**
     * Field _nrConta
     */
    private int _nrConta = 0;

    /**
     * keeps track of state for field: _nrConta
     */
    private boolean _has_nrConta;

    /**
     * Field _nrContaDigito
     */
    private java.lang.String _nrContaDigito;

    /**
     * Field _cdIndicadorNumConve
     */
    private int _cdIndicadorNumConve = 0;

    /**
     * keeps track of state for field: _cdIndicadorNumConve
     */
    private boolean _has_cdIndicadorNumConve;

    /**
     * Field _cdConveCtaSalarial
     */
    private long _cdConveCtaSalarial = 0;

    /**
     * keeps track of state for field: _cdConveCtaSalarial
     */
    private boolean _has_cdConveCtaSalarial;

    /**
     * Field _vlPagamentoMes
     */
    private java.math.BigDecimal _vlPagamentoMes = new java.math.BigDecimal("0");

    /**
     * Field _qtdFuncionarioFlPgto
     */
    private int _qtdFuncionarioFlPgto = 0;

    /**
     * keeps track of state for field: _qtdFuncionarioFlPgto
     */
    private boolean _has_qtdFuncionarioFlPgto;

    /**
     * Field _vlMediaSalarialMes
     */
    private java.math.BigDecimal _vlMediaSalarialMes = new java.math.BigDecimal("0");

    /**
     * Field _cdPagamentoSalarialMes
     */
    private int _cdPagamentoSalarialMes = 0;

    /**
     * keeps track of state for field: _cdPagamentoSalarialMes
     */
    private boolean _has_cdPagamentoSalarialMes;

    /**
     * Field _cdPagamentoSalarialQzena
     */
    private int _cdPagamentoSalarialQzena = 0;

    /**
     * keeps track of state for field: _cdPagamentoSalarialQzena
     */
    private boolean _has_cdPagamentoSalarialQzena;

    /**
     * Field _cdLocalContaSalarial
     */
    private int _cdLocalContaSalarial = 0;

    /**
     * keeps track of state for field: _cdLocalContaSalarial
     */
    private boolean _has_cdLocalContaSalarial;

    /**
     * Field _dtReftFlPgto
     */
    private int _dtReftFlPgto = 0;

    /**
     * keeps track of state for field: _dtReftFlPgto
     */
    private boolean _has_dtReftFlPgto;

    /**
     * Field _cdIndicadorAlcadaAgencia
     */
    private int _cdIndicadorAlcadaAgencia = 0;

    /**
     * keeps track of state for field: _cdIndicadorAlcadaAgencia
     */
    private boolean _has_cdIndicadorAlcadaAgencia;

    /**
     * Field _cdFidelize
     */
    private int _cdFidelize = 0;

    /**
     * keeps track of state for field: _cdFidelize
     */
    private boolean _has_cdFidelize;

    /**
     * Field _dsEmailEmpresa
     */
    private java.lang.String _dsEmailEmpresa;

    /**
     * Field _dsEmailAgencia
     */
    private java.lang.String _dsEmailAgencia;

    /**
     * Field _cdAltoRotvo
     */
    private int _cdAltoRotvo;

    /**
     * keeps track of state for field: _cdAltoRotvo
     */
    private boolean _has_cdAltoRotvo;

    /**
     * Field _cdOfertCatao
     */
    private int _cdOfertCatao;

    /**
     * keeps track of state for field: _cdOfertCatao
     */
    private boolean _has_cdOfertCatao;

    /**
     * Field _cdAbertAplic
     */
    private int _cdAbertAplic;

    /**
     * keeps track of state for field: _cdAbertAplic
     */
    private boolean _has_cdAbertAplic;


      //----------------/
     //- Constructors -/
    //----------------/

    public IncluirPagamentoSalarioRequest() 
     {
        super();
        setVlPagamentoMes(new java.math.BigDecimal("0"));
        setVlMediaSalarialMes(new java.math.BigDecimal("0"));
    } //-- br.com.bradesco.web.pgit.service.data.pdc.incluirpagamentosalariopdcadapter.request.IncluirPagamentoSalarioRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdAbertAplic
     * 
     */
    public void deleteCdAbertAplic()
    {
        this._has_cdAbertAplic= false;
    } //-- void deleteCdAbertAplic() 

    /**
     * Method deleteCdAltoRotvo
     * 
     */
    public void deleteCdAltoRotvo()
    {
        this._has_cdAltoRotvo= false;
    } //-- void deleteCdAltoRotvo() 

    /**
     * Method deleteCdClubRepresentante
     * 
     */
    public void deleteCdClubRepresentante()
    {
        this._has_cdClubRepresentante= false;
    } //-- void deleteCdClubRepresentante() 

    /**
     * Method deleteCdControleCpfRepresentante
     * 
     */
    public void deleteCdControleCpfRepresentante()
    {
        this._has_cdControleCpfRepresentante= false;
    } //-- void deleteCdControleCpfRepresentante() 

    /**
     * Method deleteCdConveCtaSalarial
     * 
     */
    public void deleteCdConveCtaSalarial()
    {
        this._has_cdConveCtaSalarial= false;
    } //-- void deleteCdConveCtaSalarial() 

    /**
     * Method deleteCdCpfCnpjRepresentante
     * 
     */
    public void deleteCdCpfCnpjRepresentante()
    {
        this._has_cdCpfCnpjRepresentante= false;
    } //-- void deleteCdCpfCnpjRepresentante() 

    /**
     * Method deleteCdFidelize
     * 
     */
    public void deleteCdFidelize()
    {
        this._has_cdFidelize= false;
    } //-- void deleteCdFidelize() 

    /**
     * Method deleteCdFilialCnpjRepresentante
     * 
     */
    public void deleteCdFilialCnpjRepresentante()
    {
        this._has_cdFilialCnpjRepresentante= false;
    } //-- void deleteCdFilialCnpjRepresentante() 

    /**
     * Method deleteCdIndicadorAlcadaAgencia
     * 
     */
    public void deleteCdIndicadorAlcadaAgencia()
    {
        this._has_cdIndicadorAlcadaAgencia= false;
    } //-- void deleteCdIndicadorAlcadaAgencia() 

    /**
     * Method deleteCdIndicadorNumConve
     * 
     */
    public void deleteCdIndicadorNumConve()
    {
        this._has_cdIndicadorNumConve= false;
    } //-- void deleteCdIndicadorNumConve() 

    /**
     * Method deleteCdInstituicaoBancaria
     * 
     */
    public void deleteCdInstituicaoBancaria()
    {
        this._has_cdInstituicaoBancaria= false;
    } //-- void deleteCdInstituicaoBancaria() 

    /**
     * Method deleteCdLocalContaSalarial
     * 
     */
    public void deleteCdLocalContaSalarial()
    {
        this._has_cdLocalContaSalarial= false;
    } //-- void deleteCdLocalContaSalarial() 

    /**
     * Method deleteCdOfertCatao
     * 
     */
    public void deleteCdOfertCatao()
    {
        this._has_cdOfertCatao= false;
    } //-- void deleteCdOfertCatao() 

    /**
     * Method deleteCdPagamentoSalarialMes
     * 
     */
    public void deleteCdPagamentoSalarialMes()
    {
        this._has_cdPagamentoSalarialMes= false;
    } //-- void deleteCdPagamentoSalarialMes() 

    /**
     * Method deleteCdPagamentoSalarialQzena
     * 
     */
    public void deleteCdPagamentoSalarialQzena()
    {
        this._has_cdPagamentoSalarialQzena= false;
    } //-- void deleteCdPagamentoSalarialQzena() 

    /**
     * Method deleteCdPessoaJuridicaConta
     * 
     */
    public void deleteCdPessoaJuridicaConta()
    {
        this._has_cdPessoaJuridicaConta= false;
    } //-- void deleteCdPessoaJuridicaConta() 

    /**
     * Method deleteCdProdutoOperacaoRelacionado
     * 
     */
    public void deleteCdProdutoOperacaoRelacionado()
    {
        this._has_cdProdutoOperacaoRelacionado= false;
    } //-- void deleteCdProdutoOperacaoRelacionado() 

    /**
     * Method deleteCdProdutoServicoOperacao
     * 
     */
    public void deleteCdProdutoServicoOperacao()
    {
        this._has_cdProdutoServicoOperacao= false;
    } //-- void deleteCdProdutoServicoOperacao() 

    /**
     * Method deleteCdTipoContratoConta
     * 
     */
    public void deleteCdTipoContratoConta()
    {
        this._has_cdTipoContratoConta= false;
    } //-- void deleteCdTipoContratoConta() 

    /**
     * Method deleteCdTipoContratoNegocio
     * 
     */
    public void deleteCdTipoContratoNegocio()
    {
        this._has_cdTipoContratoNegocio= false;
    } //-- void deleteCdTipoContratoNegocio() 

    /**
     * Method deleteCdUnidadeOrganizacional
     * 
     */
    public void deleteCdUnidadeOrganizacional()
    {
        this._has_cdUnidadeOrganizacional= false;
    } //-- void deleteCdUnidadeOrganizacional() 

    /**
     * Method deleteCdpessoaJuridicaContrato
     * 
     */
    public void deleteCdpessoaJuridicaContrato()
    {
        this._has_cdpessoaJuridicaContrato= false;
    } //-- void deleteCdpessoaJuridicaContrato() 

    /**
     * Method deleteDtReftFlPgto
     * 
     */
    public void deleteDtReftFlPgto()
    {
        this._has_dtReftFlPgto= false;
    } //-- void deleteDtReftFlPgto() 

    /**
     * Method deleteNrConta
     * 
     */
    public void deleteNrConta()
    {
        this._has_nrConta= false;
    } //-- void deleteNrConta() 

    /**
     * Method deleteNrSequenciaContratoConta
     * 
     */
    public void deleteNrSequenciaContratoConta()
    {
        this._has_nrSequenciaContratoConta= false;
    } //-- void deleteNrSequenciaContratoConta() 

    /**
     * Method deleteNrSequenciaContratoNegocio
     * 
     */
    public void deleteNrSequenciaContratoNegocio()
    {
        this._has_nrSequenciaContratoNegocio= false;
    } //-- void deleteNrSequenciaContratoNegocio() 

    /**
     * Method deleteQtdFuncionarioFlPgto
     * 
     */
    public void deleteQtdFuncionarioFlPgto()
    {
        this._has_qtdFuncionarioFlPgto= false;
    } //-- void deleteQtdFuncionarioFlPgto() 

    /**
     * Returns the value of field 'cdAbertAplic'.
     * 
     * @return int
     * @return the value of field 'cdAbertAplic'.
     */
    public int getCdAbertAplic()
    {
        return this._cdAbertAplic;
    } //-- int getCdAbertAplic() 

    /**
     * Returns the value of field 'cdAltoRotvo'.
     * 
     * @return int
     * @return the value of field 'cdAltoRotvo'.
     */
    public int getCdAltoRotvo()
    {
        return this._cdAltoRotvo;
    } //-- int getCdAltoRotvo() 

    /**
     * Returns the value of field 'cdClubRepresentante'.
     * 
     * @return long
     * @return the value of field 'cdClubRepresentante'.
     */
    public long getCdClubRepresentante()
    {
        return this._cdClubRepresentante;
    } //-- long getCdClubRepresentante() 

    /**
     * Returns the value of field 'cdControleCpfRepresentante'.
     * 
     * @return int
     * @return the value of field 'cdControleCpfRepresentante'.
     */
    public int getCdControleCpfRepresentante()
    {
        return this._cdControleCpfRepresentante;
    } //-- int getCdControleCpfRepresentante() 

    /**
     * Returns the value of field 'cdConveCtaSalarial'.
     * 
     * @return long
     * @return the value of field 'cdConveCtaSalarial'.
     */
    public long getCdConveCtaSalarial()
    {
        return this._cdConveCtaSalarial;
    } //-- long getCdConveCtaSalarial() 

    /**
     * Returns the value of field 'cdCpfCnpjRepresentante'.
     * 
     * @return long
     * @return the value of field 'cdCpfCnpjRepresentante'.
     */
    public long getCdCpfCnpjRepresentante()
    {
        return this._cdCpfCnpjRepresentante;
    } //-- long getCdCpfCnpjRepresentante() 

    /**
     * Returns the value of field 'cdFidelize'.
     * 
     * @return int
     * @return the value of field 'cdFidelize'.
     */
    public int getCdFidelize()
    {
        return this._cdFidelize;
    } //-- int getCdFidelize() 

    /**
     * Returns the value of field 'cdFilialCnpjRepresentante'.
     * 
     * @return int
     * @return the value of field 'cdFilialCnpjRepresentante'.
     */
    public int getCdFilialCnpjRepresentante()
    {
        return this._cdFilialCnpjRepresentante;
    } //-- int getCdFilialCnpjRepresentante() 

    /**
     * Returns the value of field 'cdIndicadorAlcadaAgencia'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorAlcadaAgencia'.
     */
    public int getCdIndicadorAlcadaAgencia()
    {
        return this._cdIndicadorAlcadaAgencia;
    } //-- int getCdIndicadorAlcadaAgencia() 

    /**
     * Returns the value of field 'cdIndicadorNumConve'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorNumConve'.
     */
    public int getCdIndicadorNumConve()
    {
        return this._cdIndicadorNumConve;
    } //-- int getCdIndicadorNumConve() 

    /**
     * Returns the value of field 'cdInstituicaoBancaria'.
     * 
     * @return int
     * @return the value of field 'cdInstituicaoBancaria'.
     */
    public int getCdInstituicaoBancaria()
    {
        return this._cdInstituicaoBancaria;
    } //-- int getCdInstituicaoBancaria() 

    /**
     * Returns the value of field 'cdLocalContaSalarial'.
     * 
     * @return int
     * @return the value of field 'cdLocalContaSalarial'.
     */
    public int getCdLocalContaSalarial()
    {
        return this._cdLocalContaSalarial;
    } //-- int getCdLocalContaSalarial() 

    /**
     * Returns the value of field 'cdOfertCatao'.
     * 
     * @return int
     * @return the value of field 'cdOfertCatao'.
     */
    public int getCdOfertCatao()
    {
        return this._cdOfertCatao;
    } //-- int getCdOfertCatao() 

    /**
     * Returns the value of field 'cdPagamentoSalarialMes'.
     * 
     * @return int
     * @return the value of field 'cdPagamentoSalarialMes'.
     */
    public int getCdPagamentoSalarialMes()
    {
        return this._cdPagamentoSalarialMes;
    } //-- int getCdPagamentoSalarialMes() 

    /**
     * Returns the value of field 'cdPagamentoSalarialQzena'.
     * 
     * @return int
     * @return the value of field 'cdPagamentoSalarialQzena'.
     */
    public int getCdPagamentoSalarialQzena()
    {
        return this._cdPagamentoSalarialQzena;
    } //-- int getCdPagamentoSalarialQzena() 

    /**
     * Returns the value of field 'cdPessoaJuridicaConta'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridicaConta'.
     */
    public long getCdPessoaJuridicaConta()
    {
        return this._cdPessoaJuridicaConta;
    } //-- long getCdPessoaJuridicaConta() 

    /**
     * Returns the value of field 'cdProdutoOperacaoRelacionado'.
     * 
     * @return int
     * @return the value of field 'cdProdutoOperacaoRelacionado'.
     */
    public int getCdProdutoOperacaoRelacionado()
    {
        return this._cdProdutoOperacaoRelacionado;
    } //-- int getCdProdutoOperacaoRelacionado() 

    /**
     * Returns the value of field 'cdProdutoServicoOperacao'.
     * 
     * @return int
     * @return the value of field 'cdProdutoServicoOperacao'.
     */
    public int getCdProdutoServicoOperacao()
    {
        return this._cdProdutoServicoOperacao;
    } //-- int getCdProdutoServicoOperacao() 

    /**
     * Returns the value of field 'cdTipoContratoConta'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoConta'.
     */
    public int getCdTipoContratoConta()
    {
        return this._cdTipoContratoConta;
    } //-- int getCdTipoContratoConta() 

    /**
     * Returns the value of field 'cdTipoContratoNegocio'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoNegocio'.
     */
    public int getCdTipoContratoNegocio()
    {
        return this._cdTipoContratoNegocio;
    } //-- int getCdTipoContratoNegocio() 

    /**
     * Returns the value of field 'cdUnidadeOrganizacional'.
     * 
     * @return int
     * @return the value of field 'cdUnidadeOrganizacional'.
     */
    public int getCdUnidadeOrganizacional()
    {
        return this._cdUnidadeOrganizacional;
    } //-- int getCdUnidadeOrganizacional() 

    /**
     * Returns the value of field 'cdpessoaJuridicaContrato'.
     * 
     * @return long
     * @return the value of field 'cdpessoaJuridicaContrato'.
     */
    public long getCdpessoaJuridicaContrato()
    {
        return this._cdpessoaJuridicaContrato;
    } //-- long getCdpessoaJuridicaContrato() 

    /**
     * Returns the value of field 'dsEmailAgencia'.
     * 
     * @return String
     * @return the value of field 'dsEmailAgencia'.
     */
    public java.lang.String getDsEmailAgencia()
    {
        return this._dsEmailAgencia;
    } //-- java.lang.String getDsEmailAgencia() 

    /**
     * Returns the value of field 'dsEmailEmpresa'.
     * 
     * @return String
     * @return the value of field 'dsEmailEmpresa'.
     */
    public java.lang.String getDsEmailEmpresa()
    {
        return this._dsEmailEmpresa;
    } //-- java.lang.String getDsEmailEmpresa() 

    /**
     * Returns the value of field 'dtReftFlPgto'.
     * 
     * @return int
     * @return the value of field 'dtReftFlPgto'.
     */
    public int getDtReftFlPgto()
    {
        return this._dtReftFlPgto;
    } //-- int getDtReftFlPgto() 

    /**
     * Returns the value of field 'nmRazaoRepresentante'.
     * 
     * @return String
     * @return the value of field 'nmRazaoRepresentante'.
     */
    public java.lang.String getNmRazaoRepresentante()
    {
        return this._nmRazaoRepresentante;
    } //-- java.lang.String getNmRazaoRepresentante() 

    /**
     * Returns the value of field 'nrConta'.
     * 
     * @return int
     * @return the value of field 'nrConta'.
     */
    public int getNrConta()
    {
        return this._nrConta;
    } //-- int getNrConta() 

    /**
     * Returns the value of field 'nrContaDigito'.
     * 
     * @return String
     * @return the value of field 'nrContaDigito'.
     */
    public java.lang.String getNrContaDigito()
    {
        return this._nrContaDigito;
    } //-- java.lang.String getNrContaDigito() 

    /**
     * Returns the value of field 'nrSequenciaContratoConta'.
     * 
     * @return long
     * @return the value of field 'nrSequenciaContratoConta'.
     */
    public long getNrSequenciaContratoConta()
    {
        return this._nrSequenciaContratoConta;
    } //-- long getNrSequenciaContratoConta() 

    /**
     * Returns the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @return long
     * @return the value of field 'nrSequenciaContratoNegocio'.
     */
    public long getNrSequenciaContratoNegocio()
    {
        return this._nrSequenciaContratoNegocio;
    } //-- long getNrSequenciaContratoNegocio() 

    /**
     * Returns the value of field 'qtdFuncionarioFlPgto'.
     * 
     * @return int
     * @return the value of field 'qtdFuncionarioFlPgto'.
     */
    public int getQtdFuncionarioFlPgto()
    {
        return this._qtdFuncionarioFlPgto;
    } //-- int getQtdFuncionarioFlPgto() 

    /**
     * Returns the value of field 'vlMediaSalarialMes'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlMediaSalarialMes'.
     */
    public java.math.BigDecimal getVlMediaSalarialMes()
    {
        return this._vlMediaSalarialMes;
    } //-- java.math.BigDecimal getVlMediaSalarialMes() 

    /**
     * Returns the value of field 'vlPagamentoMes'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlPagamentoMes'.
     */
    public java.math.BigDecimal getVlPagamentoMes()
    {
        return this._vlPagamentoMes;
    } //-- java.math.BigDecimal getVlPagamentoMes() 

    /**
     * Method hasCdAbertAplic
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAbertAplic()
    {
        return this._has_cdAbertAplic;
    } //-- boolean hasCdAbertAplic() 

    /**
     * Method hasCdAltoRotvo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAltoRotvo()
    {
        return this._has_cdAltoRotvo;
    } //-- boolean hasCdAltoRotvo() 

    /**
     * Method hasCdClubRepresentante
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdClubRepresentante()
    {
        return this._has_cdClubRepresentante;
    } //-- boolean hasCdClubRepresentante() 

    /**
     * Method hasCdControleCpfRepresentante
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdControleCpfRepresentante()
    {
        return this._has_cdControleCpfRepresentante;
    } //-- boolean hasCdControleCpfRepresentante() 

    /**
     * Method hasCdConveCtaSalarial
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdConveCtaSalarial()
    {
        return this._has_cdConveCtaSalarial;
    } //-- boolean hasCdConveCtaSalarial() 

    /**
     * Method hasCdCpfCnpjRepresentante
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCpfCnpjRepresentante()
    {
        return this._has_cdCpfCnpjRepresentante;
    } //-- boolean hasCdCpfCnpjRepresentante() 

    /**
     * Method hasCdFidelize
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFidelize()
    {
        return this._has_cdFidelize;
    } //-- boolean hasCdFidelize() 

    /**
     * Method hasCdFilialCnpjRepresentante
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFilialCnpjRepresentante()
    {
        return this._has_cdFilialCnpjRepresentante;
    } //-- boolean hasCdFilialCnpjRepresentante() 

    /**
     * Method hasCdIndicadorAlcadaAgencia
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorAlcadaAgencia()
    {
        return this._has_cdIndicadorAlcadaAgencia;
    } //-- boolean hasCdIndicadorAlcadaAgencia() 

    /**
     * Method hasCdIndicadorNumConve
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorNumConve()
    {
        return this._has_cdIndicadorNumConve;
    } //-- boolean hasCdIndicadorNumConve() 

    /**
     * Method hasCdInstituicaoBancaria
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdInstituicaoBancaria()
    {
        return this._has_cdInstituicaoBancaria;
    } //-- boolean hasCdInstituicaoBancaria() 

    /**
     * Method hasCdLocalContaSalarial
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdLocalContaSalarial()
    {
        return this._has_cdLocalContaSalarial;
    } //-- boolean hasCdLocalContaSalarial() 

    /**
     * Method hasCdOfertCatao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdOfertCatao()
    {
        return this._has_cdOfertCatao;
    } //-- boolean hasCdOfertCatao() 

    /**
     * Method hasCdPagamentoSalarialMes
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPagamentoSalarialMes()
    {
        return this._has_cdPagamentoSalarialMes;
    } //-- boolean hasCdPagamentoSalarialMes() 

    /**
     * Method hasCdPagamentoSalarialQzena
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPagamentoSalarialQzena()
    {
        return this._has_cdPagamentoSalarialQzena;
    } //-- boolean hasCdPagamentoSalarialQzena() 

    /**
     * Method hasCdPessoaJuridicaConta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridicaConta()
    {
        return this._has_cdPessoaJuridicaConta;
    } //-- boolean hasCdPessoaJuridicaConta() 

    /**
     * Method hasCdProdutoOperacaoRelacionado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdutoOperacaoRelacionado()
    {
        return this._has_cdProdutoOperacaoRelacionado;
    } //-- boolean hasCdProdutoOperacaoRelacionado() 

    /**
     * Method hasCdProdutoServicoOperacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdutoServicoOperacao()
    {
        return this._has_cdProdutoServicoOperacao;
    } //-- boolean hasCdProdutoServicoOperacao() 

    /**
     * Method hasCdTipoContratoConta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoConta()
    {
        return this._has_cdTipoContratoConta;
    } //-- boolean hasCdTipoContratoConta() 

    /**
     * Method hasCdTipoContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoNegocio()
    {
        return this._has_cdTipoContratoNegocio;
    } //-- boolean hasCdTipoContratoNegocio() 

    /**
     * Method hasCdUnidadeOrganizacional
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdUnidadeOrganizacional()
    {
        return this._has_cdUnidadeOrganizacional;
    } //-- boolean hasCdUnidadeOrganizacional() 

    /**
     * Method hasCdpessoaJuridicaContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdpessoaJuridicaContrato()
    {
        return this._has_cdpessoaJuridicaContrato;
    } //-- boolean hasCdpessoaJuridicaContrato() 

    /**
     * Method hasDtReftFlPgto
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasDtReftFlPgto()
    {
        return this._has_dtReftFlPgto;
    } //-- boolean hasDtReftFlPgto() 

    /**
     * Method hasNrConta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrConta()
    {
        return this._has_nrConta;
    } //-- boolean hasNrConta() 

    /**
     * Method hasNrSequenciaContratoConta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaContratoConta()
    {
        return this._has_nrSequenciaContratoConta;
    } //-- boolean hasNrSequenciaContratoConta() 

    /**
     * Method hasNrSequenciaContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaContratoNegocio()
    {
        return this._has_nrSequenciaContratoNegocio;
    } //-- boolean hasNrSequenciaContratoNegocio() 

    /**
     * Method hasQtdFuncionarioFlPgto
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtdFuncionarioFlPgto()
    {
        return this._has_qtdFuncionarioFlPgto;
    } //-- boolean hasQtdFuncionarioFlPgto() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdAbertAplic'.
     * 
     * @param cdAbertAplic the value of field 'cdAbertAplic'.
     */
    public void setCdAbertAplic(int cdAbertAplic)
    {
        this._cdAbertAplic = cdAbertAplic;
        this._has_cdAbertAplic = true;
    } //-- void setCdAbertAplic(int) 

    /**
     * Sets the value of field 'cdAltoRotvo'.
     * 
     * @param cdAltoRotvo the value of field 'cdAltoRotvo'.
     */
    public void setCdAltoRotvo(int cdAltoRotvo)
    {
        this._cdAltoRotvo = cdAltoRotvo;
        this._has_cdAltoRotvo = true;
    } //-- void setCdAltoRotvo(int) 

    /**
     * Sets the value of field 'cdClubRepresentante'.
     * 
     * @param cdClubRepresentante the value of field
     * 'cdClubRepresentante'.
     */
    public void setCdClubRepresentante(long cdClubRepresentante)
    {
        this._cdClubRepresentante = cdClubRepresentante;
        this._has_cdClubRepresentante = true;
    } //-- void setCdClubRepresentante(long) 

    /**
     * Sets the value of field 'cdControleCpfRepresentante'.
     * 
     * @param cdControleCpfRepresentante the value of field
     * 'cdControleCpfRepresentante'.
     */
    public void setCdControleCpfRepresentante(int cdControleCpfRepresentante)
    {
        this._cdControleCpfRepresentante = cdControleCpfRepresentante;
        this._has_cdControleCpfRepresentante = true;
    } //-- void setCdControleCpfRepresentante(int) 

    /**
     * Sets the value of field 'cdConveCtaSalarial'.
     * 
     * @param cdConveCtaSalarial the value of field
     * 'cdConveCtaSalarial'.
     */
    public void setCdConveCtaSalarial(long cdConveCtaSalarial)
    {
        this._cdConveCtaSalarial = cdConveCtaSalarial;
        this._has_cdConveCtaSalarial = true;
    } //-- void setCdConveCtaSalarial(long) 

    /**
     * Sets the value of field 'cdCpfCnpjRepresentante'.
     * 
     * @param cdCpfCnpjRepresentante the value of field
     * 'cdCpfCnpjRepresentante'.
     */
    public void setCdCpfCnpjRepresentante(long cdCpfCnpjRepresentante)
    {
        this._cdCpfCnpjRepresentante = cdCpfCnpjRepresentante;
        this._has_cdCpfCnpjRepresentante = true;
    } //-- void setCdCpfCnpjRepresentante(long) 

    /**
     * Sets the value of field 'cdFidelize'.
     * 
     * @param cdFidelize the value of field 'cdFidelize'.
     */
    public void setCdFidelize(int cdFidelize)
    {
        this._cdFidelize = cdFidelize;
        this._has_cdFidelize = true;
    } //-- void setCdFidelize(int) 

    /**
     * Sets the value of field 'cdFilialCnpjRepresentante'.
     * 
     * @param cdFilialCnpjRepresentante the value of field
     * 'cdFilialCnpjRepresentante'.
     */
    public void setCdFilialCnpjRepresentante(int cdFilialCnpjRepresentante)
    {
        this._cdFilialCnpjRepresentante = cdFilialCnpjRepresentante;
        this._has_cdFilialCnpjRepresentante = true;
    } //-- void setCdFilialCnpjRepresentante(int) 

    /**
     * Sets the value of field 'cdIndicadorAlcadaAgencia'.
     * 
     * @param cdIndicadorAlcadaAgencia the value of field
     * 'cdIndicadorAlcadaAgencia'.
     */
    public void setCdIndicadorAlcadaAgencia(int cdIndicadorAlcadaAgencia)
    {
        this._cdIndicadorAlcadaAgencia = cdIndicadorAlcadaAgencia;
        this._has_cdIndicadorAlcadaAgencia = true;
    } //-- void setCdIndicadorAlcadaAgencia(int) 

    /**
     * Sets the value of field 'cdIndicadorNumConve'.
     * 
     * @param cdIndicadorNumConve the value of field
     * 'cdIndicadorNumConve'.
     */
    public void setCdIndicadorNumConve(int cdIndicadorNumConve)
    {
        this._cdIndicadorNumConve = cdIndicadorNumConve;
        this._has_cdIndicadorNumConve = true;
    } //-- void setCdIndicadorNumConve(int) 

    /**
     * Sets the value of field 'cdInstituicaoBancaria'.
     * 
     * @param cdInstituicaoBancaria the value of field
     * 'cdInstituicaoBancaria'.
     */
    public void setCdInstituicaoBancaria(int cdInstituicaoBancaria)
    {
        this._cdInstituicaoBancaria = cdInstituicaoBancaria;
        this._has_cdInstituicaoBancaria = true;
    } //-- void setCdInstituicaoBancaria(int) 

    /**
     * Sets the value of field 'cdLocalContaSalarial'.
     * 
     * @param cdLocalContaSalarial the value of field
     * 'cdLocalContaSalarial'.
     */
    public void setCdLocalContaSalarial(int cdLocalContaSalarial)
    {
        this._cdLocalContaSalarial = cdLocalContaSalarial;
        this._has_cdLocalContaSalarial = true;
    } //-- void setCdLocalContaSalarial(int) 

    /**
     * Sets the value of field 'cdOfertCatao'.
     * 
     * @param cdOfertCatao the value of field 'cdOfertCatao'.
     */
    public void setCdOfertCatao(int cdOfertCatao)
    {
        this._cdOfertCatao = cdOfertCatao;
        this._has_cdOfertCatao = true;
    } //-- void setCdOfertCatao(int) 

    /**
     * Sets the value of field 'cdPagamentoSalarialMes'.
     * 
     * @param cdPagamentoSalarialMes the value of field
     * 'cdPagamentoSalarialMes'.
     */
    public void setCdPagamentoSalarialMes(int cdPagamentoSalarialMes)
    {
        this._cdPagamentoSalarialMes = cdPagamentoSalarialMes;
        this._has_cdPagamentoSalarialMes = true;
    } //-- void setCdPagamentoSalarialMes(int) 

    /**
     * Sets the value of field 'cdPagamentoSalarialQzena'.
     * 
     * @param cdPagamentoSalarialQzena the value of field
     * 'cdPagamentoSalarialQzena'.
     */
    public void setCdPagamentoSalarialQzena(int cdPagamentoSalarialQzena)
    {
        this._cdPagamentoSalarialQzena = cdPagamentoSalarialQzena;
        this._has_cdPagamentoSalarialQzena = true;
    } //-- void setCdPagamentoSalarialQzena(int) 

    /**
     * Sets the value of field 'cdPessoaJuridicaConta'.
     * 
     * @param cdPessoaJuridicaConta the value of field
     * 'cdPessoaJuridicaConta'.
     */
    public void setCdPessoaJuridicaConta(long cdPessoaJuridicaConta)
    {
        this._cdPessoaJuridicaConta = cdPessoaJuridicaConta;
        this._has_cdPessoaJuridicaConta = true;
    } //-- void setCdPessoaJuridicaConta(long) 

    /**
     * Sets the value of field 'cdProdutoOperacaoRelacionado'.
     * 
     * @param cdProdutoOperacaoRelacionado the value of field
     * 'cdProdutoOperacaoRelacionado'.
     */
    public void setCdProdutoOperacaoRelacionado(int cdProdutoOperacaoRelacionado)
    {
        this._cdProdutoOperacaoRelacionado = cdProdutoOperacaoRelacionado;
        this._has_cdProdutoOperacaoRelacionado = true;
    } //-- void setCdProdutoOperacaoRelacionado(int) 

    /**
     * Sets the value of field 'cdProdutoServicoOperacao'.
     * 
     * @param cdProdutoServicoOperacao the value of field
     * 'cdProdutoServicoOperacao'.
     */
    public void setCdProdutoServicoOperacao(int cdProdutoServicoOperacao)
    {
        this._cdProdutoServicoOperacao = cdProdutoServicoOperacao;
        this._has_cdProdutoServicoOperacao = true;
    } //-- void setCdProdutoServicoOperacao(int) 

    /**
     * Sets the value of field 'cdTipoContratoConta'.
     * 
     * @param cdTipoContratoConta the value of field
     * 'cdTipoContratoConta'.
     */
    public void setCdTipoContratoConta(int cdTipoContratoConta)
    {
        this._cdTipoContratoConta = cdTipoContratoConta;
        this._has_cdTipoContratoConta = true;
    } //-- void setCdTipoContratoConta(int) 

    /**
     * Sets the value of field 'cdTipoContratoNegocio'.
     * 
     * @param cdTipoContratoNegocio the value of field
     * 'cdTipoContratoNegocio'.
     */
    public void setCdTipoContratoNegocio(int cdTipoContratoNegocio)
    {
        this._cdTipoContratoNegocio = cdTipoContratoNegocio;
        this._has_cdTipoContratoNegocio = true;
    } //-- void setCdTipoContratoNegocio(int) 

    /**
     * Sets the value of field 'cdUnidadeOrganizacional'.
     * 
     * @param cdUnidadeOrganizacional the value of field
     * 'cdUnidadeOrganizacional'.
     */
    public void setCdUnidadeOrganizacional(int cdUnidadeOrganizacional)
    {
        this._cdUnidadeOrganizacional = cdUnidadeOrganizacional;
        this._has_cdUnidadeOrganizacional = true;
    } //-- void setCdUnidadeOrganizacional(int) 

    /**
     * Sets the value of field 'cdpessoaJuridicaContrato'.
     * 
     * @param cdpessoaJuridicaContrato the value of field
     * 'cdpessoaJuridicaContrato'.
     */
    public void setCdpessoaJuridicaContrato(long cdpessoaJuridicaContrato)
    {
        this._cdpessoaJuridicaContrato = cdpessoaJuridicaContrato;
        this._has_cdpessoaJuridicaContrato = true;
    } //-- void setCdpessoaJuridicaContrato(long) 

    /**
     * Sets the value of field 'dsEmailAgencia'.
     * 
     * @param dsEmailAgencia the value of field 'dsEmailAgencia'.
     */
    public void setDsEmailAgencia(java.lang.String dsEmailAgencia)
    {
        this._dsEmailAgencia = dsEmailAgencia;
    } //-- void setDsEmailAgencia(java.lang.String) 

    /**
     * Sets the value of field 'dsEmailEmpresa'.
     * 
     * @param dsEmailEmpresa the value of field 'dsEmailEmpresa'.
     */
    public void setDsEmailEmpresa(java.lang.String dsEmailEmpresa)
    {
        this._dsEmailEmpresa = dsEmailEmpresa;
    } //-- void setDsEmailEmpresa(java.lang.String) 

    /**
     * Sets the value of field 'dtReftFlPgto'.
     * 
     * @param dtReftFlPgto the value of field 'dtReftFlPgto'.
     */
    public void setDtReftFlPgto(int dtReftFlPgto)
    {
        this._dtReftFlPgto = dtReftFlPgto;
        this._has_dtReftFlPgto = true;
    } //-- void setDtReftFlPgto(int) 

    /**
     * Sets the value of field 'nmRazaoRepresentante'.
     * 
     * @param nmRazaoRepresentante the value of field
     * 'nmRazaoRepresentante'.
     */
    public void setNmRazaoRepresentante(java.lang.String nmRazaoRepresentante)
    {
        this._nmRazaoRepresentante = nmRazaoRepresentante;
    } //-- void setNmRazaoRepresentante(java.lang.String) 

    /**
     * Sets the value of field 'nrConta'.
     * 
     * @param nrConta the value of field 'nrConta'.
     */
    public void setNrConta(int nrConta)
    {
        this._nrConta = nrConta;
        this._has_nrConta = true;
    } //-- void setNrConta(int) 

    /**
     * Sets the value of field 'nrContaDigito'.
     * 
     * @param nrContaDigito the value of field 'nrContaDigito'.
     */
    public void setNrContaDigito(java.lang.String nrContaDigito)
    {
        this._nrContaDigito = nrContaDigito;
    } //-- void setNrContaDigito(java.lang.String) 

    /**
     * Sets the value of field 'nrSequenciaContratoConta'.
     * 
     * @param nrSequenciaContratoConta the value of field
     * 'nrSequenciaContratoConta'.
     */
    public void setNrSequenciaContratoConta(long nrSequenciaContratoConta)
    {
        this._nrSequenciaContratoConta = nrSequenciaContratoConta;
        this._has_nrSequenciaContratoConta = true;
    } //-- void setNrSequenciaContratoConta(long) 

    /**
     * Sets the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @param nrSequenciaContratoNegocio the value of field
     * 'nrSequenciaContratoNegocio'.
     */
    public void setNrSequenciaContratoNegocio(long nrSequenciaContratoNegocio)
    {
        this._nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
        this._has_nrSequenciaContratoNegocio = true;
    } //-- void setNrSequenciaContratoNegocio(long) 

    /**
     * Sets the value of field 'qtdFuncionarioFlPgto'.
     * 
     * @param qtdFuncionarioFlPgto the value of field
     * 'qtdFuncionarioFlPgto'.
     */
    public void setQtdFuncionarioFlPgto(int qtdFuncionarioFlPgto)
    {
        this._qtdFuncionarioFlPgto = qtdFuncionarioFlPgto;
        this._has_qtdFuncionarioFlPgto = true;
    } //-- void setQtdFuncionarioFlPgto(int) 

    /**
     * Sets the value of field 'vlMediaSalarialMes'.
     * 
     * @param vlMediaSalarialMes the value of field
     * 'vlMediaSalarialMes'.
     */
    public void setVlMediaSalarialMes(java.math.BigDecimal vlMediaSalarialMes)
    {
        this._vlMediaSalarialMes = vlMediaSalarialMes;
    } //-- void setVlMediaSalarialMes(java.math.BigDecimal) 

    /**
     * Sets the value of field 'vlPagamentoMes'.
     * 
     * @param vlPagamentoMes the value of field 'vlPagamentoMes'.
     */
    public void setVlPagamentoMes(java.math.BigDecimal vlPagamentoMes)
    {
        this._vlPagamentoMes = vlPagamentoMes;
    } //-- void setVlPagamentoMes(java.math.BigDecimal) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return IncluirPagamentoSalarioRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.incluirpagamentosalario.request.IncluirPagamentoSalarioRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.incluirpagamentosalario.request.IncluirPagamentoSalarioRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.incluirpagamentosalario.request.IncluirPagamentoSalarioRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.incluirpagamentosalariopdcadapter.request.IncluirPagamentoSalarioRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
