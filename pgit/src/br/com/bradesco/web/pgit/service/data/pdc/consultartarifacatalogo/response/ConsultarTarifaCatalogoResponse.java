/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultartarifacatalogo.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.math.BigDecimal;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ConsultarTarifaCatalogoResponse.
 * 
 * @version $Revision$ $Date$
 */
public class ConsultarTarifaCatalogoResponse implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _codMensagem
     */
    private java.lang.String _codMensagem;

    /**
     * Field _mensagem
     */
    private java.lang.String _mensagem;

    /**
     * Field _cdCondicaoEconomica
     */
    private int _cdCondicaoEconomica = 0;

    /**
     * keeps track of state for field: _cdCondicaoEconomica
     */
    private boolean _has_cdCondicaoEconomica;

    /**
     * Field _vrTarifaMinimaNegociada
     */
    private java.math.BigDecimal _vrTarifaMinimaNegociada = new java.math.BigDecimal("0");

    /**
     * Field _vrTarifaMaximaNegociada
     */
    private java.math.BigDecimal _vrTarifaMaximaNegociada = new java.math.BigDecimal("0");


      //----------------/
     //- Constructors -/
    //----------------/

    public ConsultarTarifaCatalogoResponse() 
     {
        super();
        setVrTarifaMinimaNegociada(new java.math.BigDecimal("0"));
        setVrTarifaMaximaNegociada(new java.math.BigDecimal("0"));
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultartarifacatalogo.response.ConsultarTarifaCatalogoResponse()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdCondicaoEconomica
     * 
     */
    public void deleteCdCondicaoEconomica()
    {
        this._has_cdCondicaoEconomica= false;
    } //-- void deleteCdCondicaoEconomica() 

    /**
     * Returns the value of field 'cdCondicaoEconomica'.
     * 
     * @return int
     * @return the value of field 'cdCondicaoEconomica'.
     */
    public int getCdCondicaoEconomica()
    {
        return this._cdCondicaoEconomica;
    } //-- int getCdCondicaoEconomica() 

    /**
     * Returns the value of field 'codMensagem'.
     * 
     * @return String
     * @return the value of field 'codMensagem'.
     */
    public java.lang.String getCodMensagem()
    {
        return this._codMensagem;
    } //-- java.lang.String getCodMensagem() 

    /**
     * Returns the value of field 'mensagem'.
     * 
     * @return String
     * @return the value of field 'mensagem'.
     */
    public java.lang.String getMensagem()
    {
        return this._mensagem;
    } //-- java.lang.String getMensagem() 

    /**
     * Returns the value of field 'vrTarifaMaximaNegociada'.
     * 
     * @return BigDecimal
     * @return the value of field 'vrTarifaMaximaNegociada'.
     */
    public java.math.BigDecimal getVrTarifaMaximaNegociada()
    {
        return this._vrTarifaMaximaNegociada;
    } //-- java.math.BigDecimal getVrTarifaMaximaNegociada() 

    /**
     * Returns the value of field 'vrTarifaMinimaNegociada'.
     * 
     * @return BigDecimal
     * @return the value of field 'vrTarifaMinimaNegociada'.
     */
    public java.math.BigDecimal getVrTarifaMinimaNegociada()
    {
        return this._vrTarifaMinimaNegociada;
    } //-- java.math.BigDecimal getVrTarifaMinimaNegociada() 

    /**
     * Method hasCdCondicaoEconomica
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCondicaoEconomica()
    {
        return this._has_cdCondicaoEconomica;
    } //-- boolean hasCdCondicaoEconomica() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdCondicaoEconomica'.
     * 
     * @param cdCondicaoEconomica the value of field
     * 'cdCondicaoEconomica'.
     */
    public void setCdCondicaoEconomica(int cdCondicaoEconomica)
    {
        this._cdCondicaoEconomica = cdCondicaoEconomica;
        this._has_cdCondicaoEconomica = true;
    } //-- void setCdCondicaoEconomica(int) 

    /**
     * Sets the value of field 'codMensagem'.
     * 
     * @param codMensagem the value of field 'codMensagem'.
     */
    public void setCodMensagem(java.lang.String codMensagem)
    {
        this._codMensagem = codMensagem;
    } //-- void setCodMensagem(java.lang.String) 

    /**
     * Sets the value of field 'mensagem'.
     * 
     * @param mensagem the value of field 'mensagem'.
     */
    public void setMensagem(java.lang.String mensagem)
    {
        this._mensagem = mensagem;
    } //-- void setMensagem(java.lang.String) 

    /**
     * Sets the value of field 'vrTarifaMaximaNegociada'.
     * 
     * @param vrTarifaMaximaNegociada the value of field
     * 'vrTarifaMaximaNegociada'.
     */
    public void setVrTarifaMaximaNegociada(java.math.BigDecimal vrTarifaMaximaNegociada)
    {
        this._vrTarifaMaximaNegociada = vrTarifaMaximaNegociada;
    } //-- void setVrTarifaMaximaNegociada(java.math.BigDecimal) 

    /**
     * Sets the value of field 'vrTarifaMinimaNegociada'.
     * 
     * @param vrTarifaMinimaNegociada the value of field
     * 'vrTarifaMinimaNegociada'.
     */
    public void setVrTarifaMinimaNegociada(java.math.BigDecimal vrTarifaMinimaNegociada)
    {
        this._vrTarifaMinimaNegociada = vrTarifaMinimaNegociada;
    } //-- void setVrTarifaMinimaNegociada(java.math.BigDecimal) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ConsultarTarifaCatalogoResponse
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultartarifacatalogo.response.ConsultarTarifaCatalogoResponse unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultartarifacatalogo.response.ConsultarTarifaCatalogoResponse) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultartarifacatalogo.response.ConsultarTarifaCatalogoResponse.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultartarifacatalogo.response.ConsultarTarifaCatalogoResponse unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
