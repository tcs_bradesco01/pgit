/*
 * Nome: br.com.bradesco.web.pgit.service.business.combo.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.combo.bean;

/**
 * Nome: TipoAcaoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class TipoAcaoSaidaDTO {
	
	/** Atributo codMensagem. */
	private String codMensagem;
	
	/** Atributo mensagem. */
	private String mensagem;
	
	/** Atributo cdAcao. */
	private Integer cdAcao;
	
	/** Atributo dsAcao. */
	private String dsAcao;
	
	/**
	 * Get: cdAcao.
	 *
	 * @return cdAcao
	 */
	public Integer getCdAcao() {
		return cdAcao;
	}
	
	/**
	 * Set: cdAcao.
	 *
	 * @param cdAcao the cd acao
	 */
	public void setCdAcao(Integer cdAcao) {
		this.cdAcao = cdAcao;
	}
	
	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}
	
	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}
	
	/**
	 * Get: dsAcao.
	 *
	 * @return dsAcao
	 */
	public String getDsAcao() {
		return dsAcao;
	}
	
	/**
	 * Set: dsAcao.
	 *
	 * @param dsAcao the ds acao
	 */
	public void setDsAcao(String dsAcao) {
		this.dsAcao = dsAcao;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

}
