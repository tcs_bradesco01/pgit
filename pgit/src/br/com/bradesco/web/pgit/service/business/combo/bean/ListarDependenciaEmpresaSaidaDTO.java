/*
 * Nome: br.com.bradesco.web.pgit.service.business.combo.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.combo.bean;

/**
 * Nome: ListarDependenciaEmpresaSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarDependenciaEmpresaSaidaDTO {
	
	/** Atributo cdUnidadeOrganizacional. */
	private Integer cdUnidadeOrganizacional;
    
    /** Atributo dsUnidadeOrganizacional. */
    private String dsUnidadeOrganizacional;
    
	/**
	 * Listar dependencia empresa saida dto.
	 *
	 * @param cdUnidadeOrganizacional the cd unidade organizacional
	 * @param dsUnidadeOrganizacional the ds unidade organizacional
	 */
	public ListarDependenciaEmpresaSaidaDTO(Integer cdUnidadeOrganizacional, String dsUnidadeOrganizacional) {
		super();
		this.cdUnidadeOrganizacional = cdUnidadeOrganizacional;
		this.dsUnidadeOrganizacional = dsUnidadeOrganizacional;
	}

	/**
	 * Get: cdUnidadeOrganizacional.
	 *
	 * @return cdUnidadeOrganizacional
	 */
	public Integer getCdUnidadeOrganizacional() {
		return cdUnidadeOrganizacional;
	}

	/**
	 * Set: cdUnidadeOrganizacional.
	 *
	 * @param cdUnidadeOrganizacional the cd unidade organizacional
	 */
	public void setCdUnidadeOrganizacional(Integer cdUnidadeOrganizacional) {
		this.cdUnidadeOrganizacional = cdUnidadeOrganizacional;
	}

	/**
	 * Get: dsUnidadeOrganizacional.
	 *
	 * @return dsUnidadeOrganizacional
	 */
	public String getDsUnidadeOrganizacional() {
		return dsUnidadeOrganizacional;
	}

	/**
	 * Set: dsUnidadeOrganizacional.
	 *
	 * @param dsUnidadeOrganizacional the ds unidade organizacional
	 */
	public void setDsUnidadeOrganizacional(String dsUnidadeOrganizacional) {
		this.dsUnidadeOrganizacional = dsUnidadeOrganizacional;
	}
    
    

}
