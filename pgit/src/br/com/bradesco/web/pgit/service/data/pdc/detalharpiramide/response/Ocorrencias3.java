/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.detalharpiramide.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias3.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias3 implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _dsLogradouroFilialQuest
     */
    private java.lang.String _dsLogradouroFilialQuest;

    /**
     * Field _dsBairroFilialQuest
     */
    private java.lang.String _dsBairroFilialQuest;

    /**
     * Field _cdMunicipioFilialQuest
     */
    private long _cdMunicipioFilialQuest = 0;

    /**
     * keeps track of state for field: _cdMunicipioFilialQuest
     */
    private boolean _has_cdMunicipioFilialQuest;

    /**
     * Field _dsMuncFilialQuest
     */
    private java.lang.String _dsMuncFilialQuest;

    /**
     * Field _cdCepFilialQuest
     */
    private int _cdCepFilialQuest = 0;

    /**
     * keeps track of state for field: _cdCepFilialQuest
     */
    private boolean _has_cdCepFilialQuest;

    /**
     * Field _cdCepComplQuesf
     */
    private int _cdCepComplQuesf = 0;

    /**
     * keeps track of state for field: _cdCepComplQuesf
     */
    private boolean _has_cdCepComplQuesf;

    /**
     * Field _cdUfFilialQuest
     */
    private int _cdUfFilialQuest = 0;

    /**
     * keeps track of state for field: _cdUfFilialQuest
     */
    private boolean _has_cdUfFilialQuest;

    /**
     * Field _cdSegmentoFilialQuest
     */
    private java.lang.String _cdSegmentoFilialQuest;

    /**
     * Field _qtdFuncFilalQuest
     */
    private long _qtdFuncFilalQuest = 0;

    /**
     * keeps track of state for field: _qtdFuncFilalQuest
     */
    private boolean _has_qtdFuncFilalQuest;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias3() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharpiramide.response.Ocorrencias3()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdCepComplQuesf
     * 
     */
    public void deleteCdCepComplQuesf()
    {
        this._has_cdCepComplQuesf= false;
    } //-- void deleteCdCepComplQuesf() 

    /**
     * Method deleteCdCepFilialQuest
     * 
     */
    public void deleteCdCepFilialQuest()
    {
        this._has_cdCepFilialQuest= false;
    } //-- void deleteCdCepFilialQuest() 

    /**
     * Method deleteCdMunicipioFilialQuest
     * 
     */
    public void deleteCdMunicipioFilialQuest()
    {
        this._has_cdMunicipioFilialQuest= false;
    } //-- void deleteCdMunicipioFilialQuest() 

    /**
     * Method deleteCdUfFilialQuest
     * 
     */
    public void deleteCdUfFilialQuest()
    {
        this._has_cdUfFilialQuest= false;
    } //-- void deleteCdUfFilialQuest() 

    /**
     * Method deleteQtdFuncFilalQuest
     * 
     */
    public void deleteQtdFuncFilalQuest()
    {
        this._has_qtdFuncFilalQuest= false;
    } //-- void deleteQtdFuncFilalQuest() 

    /**
     * Returns the value of field 'cdCepComplQuesf'.
     * 
     * @return int
     * @return the value of field 'cdCepComplQuesf'.
     */
    public int getCdCepComplQuesf()
    {
        return this._cdCepComplQuesf;
    } //-- int getCdCepComplQuesf() 

    /**
     * Returns the value of field 'cdCepFilialQuest'.
     * 
     * @return int
     * @return the value of field 'cdCepFilialQuest'.
     */
    public int getCdCepFilialQuest()
    {
        return this._cdCepFilialQuest;
    } //-- int getCdCepFilialQuest() 

    /**
     * Returns the value of field 'cdMunicipioFilialQuest'.
     * 
     * @return long
     * @return the value of field 'cdMunicipioFilialQuest'.
     */
    public long getCdMunicipioFilialQuest()
    {
        return this._cdMunicipioFilialQuest;
    } //-- long getCdMunicipioFilialQuest() 

    /**
     * Returns the value of field 'cdSegmentoFilialQuest'.
     * 
     * @return String
     * @return the value of field 'cdSegmentoFilialQuest'.
     */
    public java.lang.String getCdSegmentoFilialQuest()
    {
        return this._cdSegmentoFilialQuest;
    } //-- java.lang.String getCdSegmentoFilialQuest() 

    /**
     * Returns the value of field 'cdUfFilialQuest'.
     * 
     * @return int
     * @return the value of field 'cdUfFilialQuest'.
     */
    public int getCdUfFilialQuest()
    {
        return this._cdUfFilialQuest;
    } //-- int getCdUfFilialQuest() 

    /**
     * Returns the value of field 'dsBairroFilialQuest'.
     * 
     * @return String
     * @return the value of field 'dsBairroFilialQuest'.
     */
    public java.lang.String getDsBairroFilialQuest()
    {
        return this._dsBairroFilialQuest;
    } //-- java.lang.String getDsBairroFilialQuest() 

    /**
     * Returns the value of field 'dsLogradouroFilialQuest'.
     * 
     * @return String
     * @return the value of field 'dsLogradouroFilialQuest'.
     */
    public java.lang.String getDsLogradouroFilialQuest()
    {
        return this._dsLogradouroFilialQuest;
    } //-- java.lang.String getDsLogradouroFilialQuest() 

    /**
     * Returns the value of field 'dsMuncFilialQuest'.
     * 
     * @return String
     * @return the value of field 'dsMuncFilialQuest'.
     */
    public java.lang.String getDsMuncFilialQuest()
    {
        return this._dsMuncFilialQuest;
    } //-- java.lang.String getDsMuncFilialQuest() 

    /**
     * Returns the value of field 'qtdFuncFilalQuest'.
     * 
     * @return long
     * @return the value of field 'qtdFuncFilalQuest'.
     */
    public long getQtdFuncFilalQuest()
    {
        return this._qtdFuncFilalQuest;
    } //-- long getQtdFuncFilalQuest() 

    /**
     * Method hasCdCepComplQuesf
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCepComplQuesf()
    {
        return this._has_cdCepComplQuesf;
    } //-- boolean hasCdCepComplQuesf() 

    /**
     * Method hasCdCepFilialQuest
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCepFilialQuest()
    {
        return this._has_cdCepFilialQuest;
    } //-- boolean hasCdCepFilialQuest() 

    /**
     * Method hasCdMunicipioFilialQuest
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMunicipioFilialQuest()
    {
        return this._has_cdMunicipioFilialQuest;
    } //-- boolean hasCdMunicipioFilialQuest() 

    /**
     * Method hasCdUfFilialQuest
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdUfFilialQuest()
    {
        return this._has_cdUfFilialQuest;
    } //-- boolean hasCdUfFilialQuest() 

    /**
     * Method hasQtdFuncFilalQuest
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtdFuncFilalQuest()
    {
        return this._has_qtdFuncFilalQuest;
    } //-- boolean hasQtdFuncFilalQuest() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdCepComplQuesf'.
     * 
     * @param cdCepComplQuesf the value of field 'cdCepComplQuesf'.
     */
    public void setCdCepComplQuesf(int cdCepComplQuesf)
    {
        this._cdCepComplQuesf = cdCepComplQuesf;
        this._has_cdCepComplQuesf = true;
    } //-- void setCdCepComplQuesf(int) 

    /**
     * Sets the value of field 'cdCepFilialQuest'.
     * 
     * @param cdCepFilialQuest the value of field 'cdCepFilialQuest'
     */
    public void setCdCepFilialQuest(int cdCepFilialQuest)
    {
        this._cdCepFilialQuest = cdCepFilialQuest;
        this._has_cdCepFilialQuest = true;
    } //-- void setCdCepFilialQuest(int) 

    /**
     * Sets the value of field 'cdMunicipioFilialQuest'.
     * 
     * @param cdMunicipioFilialQuest the value of field
     * 'cdMunicipioFilialQuest'.
     */
    public void setCdMunicipioFilialQuest(long cdMunicipioFilialQuest)
    {
        this._cdMunicipioFilialQuest = cdMunicipioFilialQuest;
        this._has_cdMunicipioFilialQuest = true;
    } //-- void setCdMunicipioFilialQuest(long) 

    /**
     * Sets the value of field 'cdSegmentoFilialQuest'.
     * 
     * @param cdSegmentoFilialQuest the value of field
     * 'cdSegmentoFilialQuest'.
     */
    public void setCdSegmentoFilialQuest(java.lang.String cdSegmentoFilialQuest)
    {
        this._cdSegmentoFilialQuest = cdSegmentoFilialQuest;
    } //-- void setCdSegmentoFilialQuest(java.lang.String) 

    /**
     * Sets the value of field 'cdUfFilialQuest'.
     * 
     * @param cdUfFilialQuest the value of field 'cdUfFilialQuest'.
     */
    public void setCdUfFilialQuest(int cdUfFilialQuest)
    {
        this._cdUfFilialQuest = cdUfFilialQuest;
        this._has_cdUfFilialQuest = true;
    } //-- void setCdUfFilialQuest(int) 

    /**
     * Sets the value of field 'dsBairroFilialQuest'.
     * 
     * @param dsBairroFilialQuest the value of field
     * 'dsBairroFilialQuest'.
     */
    public void setDsBairroFilialQuest(java.lang.String dsBairroFilialQuest)
    {
        this._dsBairroFilialQuest = dsBairroFilialQuest;
    } //-- void setDsBairroFilialQuest(java.lang.String) 

    /**
     * Sets the value of field 'dsLogradouroFilialQuest'.
     * 
     * @param dsLogradouroFilialQuest the value of field
     * 'dsLogradouroFilialQuest'.
     */
    public void setDsLogradouroFilialQuest(java.lang.String dsLogradouroFilialQuest)
    {
        this._dsLogradouroFilialQuest = dsLogradouroFilialQuest;
    } //-- void setDsLogradouroFilialQuest(java.lang.String) 

    /**
     * Sets the value of field 'dsMuncFilialQuest'.
     * 
     * @param dsMuncFilialQuest the value of field
     * 'dsMuncFilialQuest'.
     */
    public void setDsMuncFilialQuest(java.lang.String dsMuncFilialQuest)
    {
        this._dsMuncFilialQuest = dsMuncFilialQuest;
    } //-- void setDsMuncFilialQuest(java.lang.String) 

    /**
     * Sets the value of field 'qtdFuncFilalQuest'.
     * 
     * @param qtdFuncFilalQuest the value of field
     * 'qtdFuncFilalQuest'.
     */
    public void setQtdFuncFilalQuest(long qtdFuncFilalQuest)
    {
        this._qtdFuncFilalQuest = qtdFuncFilalQuest;
        this._has_qtdFuncFilalQuest = true;
    } //-- void setQtdFuncFilalQuest(long) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias3
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.detalharpiramide.response.Ocorrencias3 unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.detalharpiramide.response.Ocorrencias3) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.detalharpiramide.response.Ocorrencias3.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharpiramide.response.Ocorrencias3 unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
