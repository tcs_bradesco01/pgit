/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/implementation.ftl,v $
 * $Id: implementation.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: implementation.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */
 
package br.com.bradesco.web.pgit.service.business.msgcompsalariall.impl;

import java.util.ArrayList;
import java.util.List;

import br.com.bradesco.web.pgit.service.business.msgcompsalarial.IMsgCompSalarialConstants;
import br.com.bradesco.web.pgit.service.business.msgcompsalarial.IMsgCompSalarialService;
import br.com.bradesco.web.pgit.service.business.msgcompsalarial.bean.AlterarMsgComprovanteSalrlEntradaDTO;
import br.com.bradesco.web.pgit.service.business.msgcompsalarial.bean.AlterarMsgComprovanteSalrlSaidaDTO;
import br.com.bradesco.web.pgit.service.business.msgcompsalarial.bean.DetalharMsgComprovanteSalrlEntradaDTO;
import br.com.bradesco.web.pgit.service.business.msgcompsalarial.bean.DetalharMsgComprovanteSalrlSaidaDTO;
import br.com.bradesco.web.pgit.service.business.msgcompsalarial.bean.ExcluirMsgComprovanteSalrlEntradaDTO;
import br.com.bradesco.web.pgit.service.business.msgcompsalarial.bean.ExcluirMsgComprovanteSalrlSaidaDTO;
import br.com.bradesco.web.pgit.service.business.msgcompsalarial.bean.IncluirMsgComprovanteSalrlEntradaDTO;
import br.com.bradesco.web.pgit.service.business.msgcompsalarial.bean.IncluirMsgComprovanteSalrlSaidaDTO;
import br.com.bradesco.web.pgit.service.business.msgcompsalarial.bean.ListarMsgComprovanteSalrlEntradaDTO;
import br.com.bradesco.web.pgit.service.business.msgcompsalarial.bean.ListarMsgComprovanteSalrlSaidaDTO;
import br.com.bradesco.web.pgit.service.data.pdc.FactoryAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.alterarmsgcomprovantesalrl.request.AlterarMsgComprovanteSalrlRequest;
import br.com.bradesco.web.pgit.service.data.pdc.alterarmsgcomprovantesalrl.response.AlterarMsgComprovanteSalrlResponse;
import br.com.bradesco.web.pgit.service.data.pdc.consultardetalhemsgcomprovantesalrl.request.ConsultarDetalheMsgComprovanteSalrlRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultardetalhemsgcomprovantesalrl.response.ConsultarDetalheMsgComprovanteSalrlResponse;
import br.com.bradesco.web.pgit.service.data.pdc.excluirmsgcomprovantesalrl.request.ExcluirMsgComprovanteSalrlRequest;
import br.com.bradesco.web.pgit.service.data.pdc.excluirmsgcomprovantesalrl.response.ExcluirMsgComprovanteSalrlResponse;
import br.com.bradesco.web.pgit.service.data.pdc.incluirmsgcomprovantesalrl.request.IncluirMsgComprovanteSalrlRequest;
import br.com.bradesco.web.pgit.service.data.pdc.incluirmsgcomprovantesalrl.response.IncluirMsgComprovanteSalrlResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarmsgcomprovantesalrl.request.ListarMsgComprovanteSalrlRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarmsgcomprovantesalrl.response.ListarMsgComprovanteSalrlResponse;
import br.com.bradesco.web.pgit.utils.PgitUtil;


/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Implementa��o do adaptador: ManterMensagemComprovanteSalarial
 * </p>
 * 
 * @comment C�DIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public class MsgCompSalServiceImpl implements IMsgCompSalarialService {
	
	/** Atributo factoryAdapter. */
	private FactoryAdapter factoryAdapter; 

    /**
     * Get: factoryAdapter.
     *
     * @return factoryAdapter
     */
    public FactoryAdapter getFactoryAdapter() {
		return factoryAdapter;
	}

	/**
	 * Set: factoryAdapter.
	 *
	 * @param factoryAdapter the factory adapter
	 */
	public void setFactoryAdapter(FactoryAdapter factoryAdapter) {
		this.factoryAdapter = factoryAdapter;
	}

	/**
     * Construtor.
     */
    public MsgCompSalServiceImpl() {
        // TODO: Implementa��o
    }
    
    /**
     * M�todo de exemplo.
     *
     * @see br.com.bradesco.web.pgit.service.business.msgcompsalarial.IManterMensagemComprovanteSalarial#sampleManterMensagemComprovanteSalarial()
     */
    public void sampleManterMensagemComprovanteSalarial() {
        // TODO: Implementa�ao
    }
    
    
    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.msgcompsalarial.IMsgCompSalarialService#listarMsgComprovanteSalrl(br.com.bradesco.web.pgit.service.business.msgcompsalarial.bean.ListarMsgComprovanteSalrlEntradaDTO)
     */
    public List<ListarMsgComprovanteSalrlSaidaDTO> listarMsgComprovanteSalrl(ListarMsgComprovanteSalrlEntradaDTO listarMsgComprovanteSalrlEntradaDTO) {
		
		List<ListarMsgComprovanteSalrlSaidaDTO> listaRetorno = new ArrayList<ListarMsgComprovanteSalrlSaidaDTO>();		
		ListarMsgComprovanteSalrlRequest request = new ListarMsgComprovanteSalrlRequest();
		ListarMsgComprovanteSalrlResponse response = new ListarMsgComprovanteSalrlResponse();
		
		request.setCdControleMensagemSalarial(listarMsgComprovanteSalrlEntradaDTO.getCdControleMensagemSalarial());		
		request.setCdTipoComprovanteSalarial(listarMsgComprovanteSalrlEntradaDTO.getCdTipoComprovanteSalarial());		
		request.setNumeroOcorrencias(IMsgCompSalarialConstants.QTDE_CONSULTAS_LISTAR);
		request.setCdTipoMensagemSalarial(PgitUtil.verificaIntegerNulo(listarMsgComprovanteSalrlEntradaDTO.getCdTipoMensagemSalarial()));
		request.setCdRestMensagemSalarial(PgitUtil.verificaIntegerNulo(listarMsgComprovanteSalrlEntradaDTO.getCdRestMensagemSalarial()));
		
		response = getFactoryAdapter().getListarMsgComprovanteSalrlPDCAdapter().invokeProcess(request);

		ListarMsgComprovanteSalrlSaidaDTO saidaDTO;
		
		for (int i=0; i<response.getOcorrenciasCount();i++){
			saidaDTO = new ListarMsgComprovanteSalrlSaidaDTO();
			saidaDTO.setCodMensagem(response.getCodMensagem());
			saidaDTO.setMensagem(response.getMensagem());
			saidaDTO.setCodigo(response.getOcorrencias(i).getCdControleMensagemSalarial());
			saidaDTO.setDescricao(response.getOcorrencias(i).getDsControleMensagemSalarial());
			saidaDTO.setNivelMensagem(response.getOcorrencias(i).getCdTipoMensagemSalarial());
			saidaDTO.setRestricao(response.getOcorrencias(i).getCdRestMensagemSalarial());
			saidaDTO.setDescTipoComprovanteSalarial(response.getOcorrencias(i).getDtTipoComprovanteSalarial());
			listaRetorno.add(saidaDTO);
			
		}
		
		return listaRetorno;
	}
    
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.msgcompsalarial.IMsgCompSalarialService#incluirMsgComprovanteSalrl(br.com.bradesco.web.pgit.service.business.msgcompsalarial.bean.IncluirMsgComprovanteSalrlEntradaDTO)
	 */
	public IncluirMsgComprovanteSalrlSaidaDTO incluirMsgComprovanteSalrl(IncluirMsgComprovanteSalrlEntradaDTO incluirMsgComprovanteSalrlEntradaDTO) {
		
		IncluirMsgComprovanteSalrlSaidaDTO incluirMsgComprovanteSalrlSaidaDTO = new IncluirMsgComprovanteSalrlSaidaDTO();
		IncluirMsgComprovanteSalrlRequest incluirMsgComprovanteSalrlRequest = new IncluirMsgComprovanteSalrlRequest();
		IncluirMsgComprovanteSalrlResponse incluirMsgComprovanteSalrlResponse = new IncluirMsgComprovanteSalrlResponse();
		
		incluirMsgComprovanteSalrlRequest.setCdControleMensagemSalarial(incluirMsgComprovanteSalrlEntradaDTO.getCodigoTipoMensagem());
		incluirMsgComprovanteSalrlRequest.setCdRestMensagemComprovante(incluirMsgComprovanteSalrlEntradaDTO.getRestricao());
		incluirMsgComprovanteSalrlRequest.setCdTipoComprovanteSalarial(incluirMsgComprovanteSalrlEntradaDTO.getTipoComprovante());
		incluirMsgComprovanteSalrlRequest.setCdTipoMensagemSalarial(incluirMsgComprovanteSalrlEntradaDTO.getNivelMensagem());
		incluirMsgComprovanteSalrlRequest.setDsControleMensagemSalarial(incluirMsgComprovanteSalrlEntradaDTO.getDescricao());
		
		incluirMsgComprovanteSalrlResponse = getFactoryAdapter().getIncluirMsgComprovanteSalrlPDCAdapter().invokeProcess(incluirMsgComprovanteSalrlRequest);
		
		incluirMsgComprovanteSalrlSaidaDTO.setCodMensagem(incluirMsgComprovanteSalrlResponse.getCodMensagem());
		incluirMsgComprovanteSalrlSaidaDTO.setMensagem(incluirMsgComprovanteSalrlResponse.getMensagem());
	
		return incluirMsgComprovanteSalrlSaidaDTO;
	}
	
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.msgcompsalarial.IMsgCompSalarialService#detalharMsgComprovanteSalrl(br.com.bradesco.web.pgit.service.business.msgcompsalarial.bean.DetalharMsgComprovanteSalrlEntradaDTO)
	 */
	public DetalharMsgComprovanteSalrlSaidaDTO detalharMsgComprovanteSalrl(DetalharMsgComprovanteSalrlEntradaDTO detalharMsgComprovanteSalrlEntradaDTO) {
		
		DetalharMsgComprovanteSalrlSaidaDTO saidaDTO = new DetalharMsgComprovanteSalrlSaidaDTO();
		
		ConsultarDetalheMsgComprovanteSalrlRequest request = new ConsultarDetalheMsgComprovanteSalrlRequest();
		ConsultarDetalheMsgComprovanteSalrlResponse response = new ConsultarDetalheMsgComprovanteSalrlResponse();
		
		request.setCdControleMensagemSalarial(detalharMsgComprovanteSalrlEntradaDTO.getCodigoTipoMensagem());
		
		response = getFactoryAdapter().getConsultarDetalheMsgComprovanteSalrlPDCAdapter().invokeProcess(request);
	
		
		saidaDTO = new DetalharMsgComprovanteSalrlSaidaDTO();
		saidaDTO.setCodMensagem(response.getCodMensagem());
		saidaDTO.setMensagem(response.getMensagem());		
	
		saidaDTO.setCodigoTipoMensagem(response.getOcorrencias(0).getCdControleMensagemSalarial());
		saidaDTO.setCdTipoComprovante(response.getOcorrencias(0).getCdTipoComprovanteSalarial());
		saidaDTO.setDsTipoComprovante(response.getOcorrencias(0).getDsTipoComprovanteSalarial());
		saidaDTO.setCdNivelMensagem(response.getOcorrencias(0).getCdTipoMensagemSalarial());
		saidaDTO.setDescricao(response.getOcorrencias(0).getDsControleMensagemSalarial());
		saidaDTO.setCdRestricao(response.getOcorrencias(0).getCdRestaurarMensagemComprovante());
		saidaDTO.setCdCanalInclusao(response.getOcorrencias(0).getCdCanalInclusao());
		saidaDTO.setDsCanalInclusao(response.getOcorrencias(0).getDsCanalInclusao()); 
		saidaDTO.setUsuarioInclusao(response.getOcorrencias(0).getCdAutenticacaoSegurancaInclusao());
		saidaDTO.setComplementoInclusao(response.getOcorrencias(0).getNrOperacaoFluxoInclusao());
		saidaDTO.setDataHoraInclusao(response.getOcorrencias(0).getHrInclusaoRegistro());
		saidaDTO.setCdCanalManutencao(response.getOcorrencias(0).getCdCanalManutencao());
		saidaDTO.setDsCanalManutencao(response.getOcorrencias(0).getDsCanalManutencao()); 
		saidaDTO.setUsuarioManutencao(response.getOcorrencias(0).getCdAutenticacaoSegurancaManutencao());
		saidaDTO.setComplementoManutencao(response.getOcorrencias(0).getNrOperacaoFluxoManutencao());
		saidaDTO.setDataHoraManutencao(response.getOcorrencias(0).getHrManutencaoRegistro());
		
		return saidaDTO;
		
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.msgcompsalarial.IMsgCompSalarialService#alterarMsgComprovanteSalrl(br.com.bradesco.web.pgit.service.business.msgcompsalarial.bean.AlterarMsgComprovanteSalrlEntradaDTO)
	 */
	public AlterarMsgComprovanteSalrlSaidaDTO alterarMsgComprovanteSalrl(AlterarMsgComprovanteSalrlEntradaDTO alterarMsgComprovanteSalrlEntradaDTO) {
		
		
		AlterarMsgComprovanteSalrlRequest request = new AlterarMsgComprovanteSalrlRequest();
		AlterarMsgComprovanteSalrlResponse response = new AlterarMsgComprovanteSalrlResponse();
		AlterarMsgComprovanteSalrlSaidaDTO alterarMsgComprovanteSalrlSaidaDTO = new AlterarMsgComprovanteSalrlSaidaDTO();
		
		request.setCdControleMensagemSalarial(alterarMsgComprovanteSalrlEntradaDTO.getCodigoTipoMensagem());
		request.setCdRestMensagemComprovante(alterarMsgComprovanteSalrlEntradaDTO.getRestricao());
		request.setCdTipoComprovanteSalarial(alterarMsgComprovanteSalrlEntradaDTO.getTipoComprovante());
		request.setCdTipoMensagemSalarial(alterarMsgComprovanteSalrlEntradaDTO.getNivelMensagem());
		request.setDsControleMensagemSalarial(alterarMsgComprovanteSalrlEntradaDTO.getDescricao());
		
		response = getFactoryAdapter().getAlterarMsgComprovanteSalrlPDCAdapter().invokeProcess(request);
					
		alterarMsgComprovanteSalrlSaidaDTO.setCodMensagem(response.getCodMensagem());
		alterarMsgComprovanteSalrlSaidaDTO.setMensagem(response.getMensagem());
				
		return alterarMsgComprovanteSalrlSaidaDTO;
		
	}
	
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.msgcompsalarial.IMsgCompSalarialService#excluirFuncoesAlcada(br.com.bradesco.web.pgit.service.business.msgcompsalarial.bean.ExcluirMsgComprovanteSalrlEntradaDTO)
	 */
	public ExcluirMsgComprovanteSalrlSaidaDTO excluirFuncoesAlcada(ExcluirMsgComprovanteSalrlEntradaDTO excluirMsgComprovanteSalrlEntradaDTO) {
		
		ExcluirMsgComprovanteSalrlSaidaDTO excluirMsgComprovanteSalrlSaidaDTO = new ExcluirMsgComprovanteSalrlSaidaDTO();
		ExcluirMsgComprovanteSalrlRequest request = new ExcluirMsgComprovanteSalrlRequest();
		ExcluirMsgComprovanteSalrlResponse response = new ExcluirMsgComprovanteSalrlResponse();
		
		request.setCdControleMensagemSalarial(excluirMsgComprovanteSalrlEntradaDTO.getCodigoTipoMensagem());
	
		response = getFactoryAdapter().getExcluirMsgComprovanteSalrlPDCAdapter().invokeProcess(request);
	
		excluirMsgComprovanteSalrlSaidaDTO.setCodMensagem(response.getCodMensagem());
		excluirMsgComprovanteSalrlSaidaDTO.setMensagem(response.getMensagem());
	
		return excluirMsgComprovanteSalrlSaidaDTO;
	}

}

