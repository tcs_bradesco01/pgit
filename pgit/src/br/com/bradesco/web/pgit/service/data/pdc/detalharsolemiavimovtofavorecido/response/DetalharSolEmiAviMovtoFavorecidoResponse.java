/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.detalharsolemiavimovtofavorecido.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.Vector;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class DetalharSolEmiAviMovtoFavorecidoResponse.
 * 
 * @version $Revision$ $Date$
 */
public class DetalharSolEmiAviMovtoFavorecidoResponse implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _codMensagem
     */
    private java.lang.String _codMensagem;

    /**
     * Field _mensagem
     */
    private java.lang.String _mensagem;

    /**
     * Field _cdSolicitacaoPagamentoIntegrado
     */
    private int _cdSolicitacaoPagamentoIntegrado = 0;

    /**
     * keeps track of state for field:
     * _cdSolicitacaoPagamentoIntegrado
     */
    private boolean _has_cdSolicitacaoPagamentoIntegrado;

    /**
     * Field _nrSolicitacaoPagamentoIntegrado
     */
    private int _nrSolicitacaoPagamentoIntegrado = 0;

    /**
     * keeps track of state for field:
     * _nrSolicitacaoPagamentoIntegrado
     */
    private boolean _has_nrSolicitacaoPagamentoIntegrado;

    /**
     * Field _cdSituacaoSolicitacao
     */
    private int _cdSituacaoSolicitacao = 0;

    /**
     * keeps track of state for field: _cdSituacaoSolicitacao
     */
    private boolean _has_cdSituacaoSolicitacao;

    /**
     * Field _dsSituacaoSolicitacao
     */
    private java.lang.String _dsSituacaoSolicitacao;

    /**
     * Field _dtInicioPeriodoMovimentacao
     */
    private java.lang.String _dtInicioPeriodoMovimentacao;

    /**
     * Field _dtFimPeriodoMovimentacao
     */
    private java.lang.String _dtFimPeriodoMovimentacao;

    /**
     * Field _cdProdutoServicoOperacao
     */
    private int _cdProdutoServicoOperacao = 0;

    /**
     * keeps track of state for field: _cdProdutoServicoOperacao
     */
    private boolean _has_cdProdutoServicoOperacao;

    /**
     * Field _dsProdutoServicoOperacao
     */
    private java.lang.String _dsProdutoServicoOperacao;

    /**
     * Field _cdProdutoOperacaoRelacionado
     */
    private int _cdProdutoOperacaoRelacionado = 0;

    /**
     * keeps track of state for field: _cdProdutoOperacaoRelacionado
     */
    private boolean _has_cdProdutoOperacaoRelacionado;

    /**
     * Field _dsProdutoOperacaoRelacionado
     */
    private java.lang.String _dsProdutoOperacaoRelacionado;

    /**
     * Field _cdRecebedorCredito
     */
    private long _cdRecebedorCredito = 0;

    /**
     * keeps track of state for field: _cdRecebedorCredito
     */
    private boolean _has_cdRecebedorCredito;

    /**
     * Field _cdTipoInscricaoRecebedor
     */
    private int _cdTipoInscricaoRecebedor = 0;

    /**
     * keeps track of state for field: _cdTipoInscricaoRecebedor
     */
    private boolean _has_cdTipoInscricaoRecebedor;

    /**
     * Field _cdCpfCnpjRecebedor
     */
    private long _cdCpfCnpjRecebedor = 0;

    /**
     * keeps track of state for field: _cdCpfCnpjRecebedor
     */
    private boolean _has_cdCpfCnpjRecebedor;

    /**
     * Field _cdFilialCnpjRecebedor
     */
    private int _cdFilialCnpjRecebedor = 0;

    /**
     * keeps track of state for field: _cdFilialCnpjRecebedor
     */
    private boolean _has_cdFilialCnpjRecebedor;

    /**
     * Field _cdControleCpfRecebedor
     */
    private int _cdControleCpfRecebedor = 0;

    /**
     * keeps track of state for field: _cdControleCpfRecebedor
     */
    private boolean _has_cdControleCpfRecebedor;

    /**
     * Field _cdDestinoCorrespSolicitacao
     */
    private int _cdDestinoCorrespSolicitacao = 0;

    /**
     * keeps track of state for field: _cdDestinoCorrespSolicitacao
     */
    private boolean _has_cdDestinoCorrespSolicitacao;

    /**
     * Field _cdTipoPostagemSolicitacao
     */
    private java.lang.String _cdTipoPostagemSolicitacao;

    /**
     * Field _dsLogradouroPagador
     */
    private java.lang.String _dsLogradouroPagador;

    /**
     * Field _dsNumeroLogradouroPagador
     */
    private java.lang.String _dsNumeroLogradouroPagador;

    /**
     * Field _dsComplementoLogradouroPagador
     */
    private java.lang.String _dsComplementoLogradouroPagador;

    /**
     * Field _dsBairroClientePagador
     */
    private java.lang.String _dsBairroClientePagador;

    /**
     * Field _dsMunicipioClientePagador
     */
    private java.lang.String _dsMunicipioClientePagador;

    /**
     * Field _cdSiglaUfPagador
     */
    private java.lang.String _cdSiglaUfPagador;

    /**
     * Field _cdCepPagador
     */
    private int _cdCepPagador = 0;

    /**
     * keeps track of state for field: _cdCepPagador
     */
    private boolean _has_cdCepPagador;

    /**
     * Field _cdCepComplementoPagador
     */
    private int _cdCepComplementoPagador = 0;

    /**
     * keeps track of state for field: _cdCepComplementoPagador
     */
    private boolean _has_cdCepComplementoPagador;

    /**
     * Field _dsEmailClientePagador
     */
    private java.lang.String _dsEmailClientePagador;

    /**
     * Field _cdAgenciaOperadora
     */
    private int _cdAgenciaOperadora = 0;

    /**
     * keeps track of state for field: _cdAgenciaOperadora
     */
    private boolean _has_cdAgenciaOperadora;

    /**
     * Field _dsAgencia
     */
    private java.lang.String _dsAgencia;

    /**
     * Field _cdDepartamentoUnidade
     */
    private int _cdDepartamentoUnidade = 0;

    /**
     * keeps track of state for field: _cdDepartamentoUnidade
     */
    private boolean _has_cdDepartamentoUnidade;

    /**
     * Field _vlTarifaPadrao
     */
    private java.math.BigDecimal _vlTarifaPadrao = new java.math.BigDecimal("0");

    /**
     * Field _cdPercentualDescTarifa
     */
    private java.math.BigDecimal _cdPercentualDescTarifa = new java.math.BigDecimal("0");

    /**
     * Field _vlrTarifaAtual
     */
    private java.math.BigDecimal _vlrTarifaAtual = new java.math.BigDecimal("0");

    /**
     * Field _cdUsuarioInclusao
     */
    private java.lang.String _cdUsuarioInclusao;

    /**
     * Field _hrInclusaoRegistro
     */
    private java.lang.String _hrInclusaoRegistro;

    /**
     * Field _cdOperacaoCanalInclusao
     */
    private java.lang.String _cdOperacaoCanalInclusao;

    /**
     * Field _cdTipoCanalInclusao
     */
    private int _cdTipoCanalInclusao = 0;

    /**
     * keeps track of state for field: _cdTipoCanalInclusao
     */
    private boolean _has_cdTipoCanalInclusao;

    /**
     * Field _dsTipoCanalInclusao
     */
    private java.lang.String _dsTipoCanalInclusao;

    /**
     * Field _nrLinhas
     */
    private int _nrLinhas = 0;

    /**
     * keeps track of state for field: _nrLinhas
     */
    private boolean _has_nrLinhas;

    /**
     * Field _ocorrenciasList
     */
    private java.util.Vector _ocorrenciasList;


      //----------------/
     //- Constructors -/
    //----------------/

    public DetalharSolEmiAviMovtoFavorecidoResponse() 
     {
        super();
        setVlTarifaPadrao(new java.math.BigDecimal("0"));
        setCdPercentualDescTarifa(new java.math.BigDecimal("0"));
        setVlrTarifaAtual(new java.math.BigDecimal("0"));
        _ocorrenciasList = new Vector();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharsolemiavimovtofavorecido.response.DetalharSolEmiAviMovtoFavorecidoResponse()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method addOcorrencias
     * 
     * 
     * 
     * @param vOcorrencias
     */
    public void addOcorrencias(br.com.bradesco.web.pgit.service.data.pdc.detalharsolemiavimovtofavorecido.response.Ocorrencias vOcorrencias)
        throws java.lang.IndexOutOfBoundsException
    {
        _ocorrenciasList.addElement(vOcorrencias);
    } //-- void addOcorrencias(br.com.bradesco.web.pgit.service.data.pdc.detalharsolemiavimovtofavorecido.response.Ocorrencias) 

    /**
     * Method addOcorrencias
     * 
     * 
     * 
     * @param index
     * @param vOcorrencias
     */
    public void addOcorrencias(int index, br.com.bradesco.web.pgit.service.data.pdc.detalharsolemiavimovtofavorecido.response.Ocorrencias vOcorrencias)
        throws java.lang.IndexOutOfBoundsException
    {
        _ocorrenciasList.insertElementAt(vOcorrencias, index);
    } //-- void addOcorrencias(int, br.com.bradesco.web.pgit.service.data.pdc.detalharsolemiavimovtofavorecido.response.Ocorrencias) 

    /**
     * Method deleteCdAgenciaOperadora
     * 
     */
    public void deleteCdAgenciaOperadora()
    {
        this._has_cdAgenciaOperadora= false;
    } //-- void deleteCdAgenciaOperadora() 

    /**
     * Method deleteCdCepComplementoPagador
     * 
     */
    public void deleteCdCepComplementoPagador()
    {
        this._has_cdCepComplementoPagador= false;
    } //-- void deleteCdCepComplementoPagador() 

    /**
     * Method deleteCdCepPagador
     * 
     */
    public void deleteCdCepPagador()
    {
        this._has_cdCepPagador= false;
    } //-- void deleteCdCepPagador() 

    /**
     * Method deleteCdControleCpfRecebedor
     * 
     */
    public void deleteCdControleCpfRecebedor()
    {
        this._has_cdControleCpfRecebedor= false;
    } //-- void deleteCdControleCpfRecebedor() 

    /**
     * Method deleteCdCpfCnpjRecebedor
     * 
     */
    public void deleteCdCpfCnpjRecebedor()
    {
        this._has_cdCpfCnpjRecebedor= false;
    } //-- void deleteCdCpfCnpjRecebedor() 

    /**
     * Method deleteCdDepartamentoUnidade
     * 
     */
    public void deleteCdDepartamentoUnidade()
    {
        this._has_cdDepartamentoUnidade= false;
    } //-- void deleteCdDepartamentoUnidade() 

    /**
     * Method deleteCdDestinoCorrespSolicitacao
     * 
     */
    public void deleteCdDestinoCorrespSolicitacao()
    {
        this._has_cdDestinoCorrespSolicitacao= false;
    } //-- void deleteCdDestinoCorrespSolicitacao() 

    /**
     * Method deleteCdFilialCnpjRecebedor
     * 
     */
    public void deleteCdFilialCnpjRecebedor()
    {
        this._has_cdFilialCnpjRecebedor= false;
    } //-- void deleteCdFilialCnpjRecebedor() 

    /**
     * Method deleteCdProdutoOperacaoRelacionado
     * 
     */
    public void deleteCdProdutoOperacaoRelacionado()
    {
        this._has_cdProdutoOperacaoRelacionado= false;
    } //-- void deleteCdProdutoOperacaoRelacionado() 

    /**
     * Method deleteCdProdutoServicoOperacao
     * 
     */
    public void deleteCdProdutoServicoOperacao()
    {
        this._has_cdProdutoServicoOperacao= false;
    } //-- void deleteCdProdutoServicoOperacao() 

    /**
     * Method deleteCdRecebedorCredito
     * 
     */
    public void deleteCdRecebedorCredito()
    {
        this._has_cdRecebedorCredito= false;
    } //-- void deleteCdRecebedorCredito() 

    /**
     * Method deleteCdSituacaoSolicitacao
     * 
     */
    public void deleteCdSituacaoSolicitacao()
    {
        this._has_cdSituacaoSolicitacao= false;
    } //-- void deleteCdSituacaoSolicitacao() 

    /**
     * Method deleteCdSolicitacaoPagamentoIntegrado
     * 
     */
    public void deleteCdSolicitacaoPagamentoIntegrado()
    {
        this._has_cdSolicitacaoPagamentoIntegrado= false;
    } //-- void deleteCdSolicitacaoPagamentoIntegrado() 

    /**
     * Method deleteCdTipoCanalInclusao
     * 
     */
    public void deleteCdTipoCanalInclusao()
    {
        this._has_cdTipoCanalInclusao= false;
    } //-- void deleteCdTipoCanalInclusao() 

    /**
     * Method deleteCdTipoInscricaoRecebedor
     * 
     */
    public void deleteCdTipoInscricaoRecebedor()
    {
        this._has_cdTipoInscricaoRecebedor= false;
    } //-- void deleteCdTipoInscricaoRecebedor() 

    /**
     * Method deleteNrLinhas
     * 
     */
    public void deleteNrLinhas()
    {
        this._has_nrLinhas= false;
    } //-- void deleteNrLinhas() 

    /**
     * Method deleteNrSolicitacaoPagamentoIntegrado
     * 
     */
    public void deleteNrSolicitacaoPagamentoIntegrado()
    {
        this._has_nrSolicitacaoPagamentoIntegrado= false;
    } //-- void deleteNrSolicitacaoPagamentoIntegrado() 

    /**
     * Method enumerateOcorrencias
     * 
     * 
     * 
     * @return Enumeration
     */
    public java.util.Enumeration enumerateOcorrencias()
    {
        return _ocorrenciasList.elements();
    } //-- java.util.Enumeration enumerateOcorrencias() 

    /**
     * Returns the value of field 'cdAgenciaOperadora'.
     * 
     * @return int
     * @return the value of field 'cdAgenciaOperadora'.
     */
    public int getCdAgenciaOperadora()
    {
        return this._cdAgenciaOperadora;
    } //-- int getCdAgenciaOperadora() 

    /**
     * Returns the value of field 'cdCepComplementoPagador'.
     * 
     * @return int
     * @return the value of field 'cdCepComplementoPagador'.
     */
    public int getCdCepComplementoPagador()
    {
        return this._cdCepComplementoPagador;
    } //-- int getCdCepComplementoPagador() 

    /**
     * Returns the value of field 'cdCepPagador'.
     * 
     * @return int
     * @return the value of field 'cdCepPagador'.
     */
    public int getCdCepPagador()
    {
        return this._cdCepPagador;
    } //-- int getCdCepPagador() 

    /**
     * Returns the value of field 'cdControleCpfRecebedor'.
     * 
     * @return int
     * @return the value of field 'cdControleCpfRecebedor'.
     */
    public int getCdControleCpfRecebedor()
    {
        return this._cdControleCpfRecebedor;
    } //-- int getCdControleCpfRecebedor() 

    /**
     * Returns the value of field 'cdCpfCnpjRecebedor'.
     * 
     * @return long
     * @return the value of field 'cdCpfCnpjRecebedor'.
     */
    public long getCdCpfCnpjRecebedor()
    {
        return this._cdCpfCnpjRecebedor;
    } //-- long getCdCpfCnpjRecebedor() 

    /**
     * Returns the value of field 'cdDepartamentoUnidade'.
     * 
     * @return int
     * @return the value of field 'cdDepartamentoUnidade'.
     */
    public int getCdDepartamentoUnidade()
    {
        return this._cdDepartamentoUnidade;
    } //-- int getCdDepartamentoUnidade() 

    /**
     * Returns the value of field 'cdDestinoCorrespSolicitacao'.
     * 
     * @return int
     * @return the value of field 'cdDestinoCorrespSolicitacao'.
     */
    public int getCdDestinoCorrespSolicitacao()
    {
        return this._cdDestinoCorrespSolicitacao;
    } //-- int getCdDestinoCorrespSolicitacao() 

    /**
     * Returns the value of field 'cdFilialCnpjRecebedor'.
     * 
     * @return int
     * @return the value of field 'cdFilialCnpjRecebedor'.
     */
    public int getCdFilialCnpjRecebedor()
    {
        return this._cdFilialCnpjRecebedor;
    } //-- int getCdFilialCnpjRecebedor() 

    /**
     * Returns the value of field 'cdOperacaoCanalInclusao'.
     * 
     * @return String
     * @return the value of field 'cdOperacaoCanalInclusao'.
     */
    public java.lang.String getCdOperacaoCanalInclusao()
    {
        return this._cdOperacaoCanalInclusao;
    } //-- java.lang.String getCdOperacaoCanalInclusao() 

    /**
     * Returns the value of field 'cdPercentualDescTarifa'.
     * 
     * @return BigDecimal
     * @return the value of field 'cdPercentualDescTarifa'.
     */
    public java.math.BigDecimal getCdPercentualDescTarifa()
    {
        return this._cdPercentualDescTarifa;
    } //-- java.math.BigDecimal getCdPercentualDescTarifa() 

    /**
     * Returns the value of field 'cdProdutoOperacaoRelacionado'.
     * 
     * @return int
     * @return the value of field 'cdProdutoOperacaoRelacionado'.
     */
    public int getCdProdutoOperacaoRelacionado()
    {
        return this._cdProdutoOperacaoRelacionado;
    } //-- int getCdProdutoOperacaoRelacionado() 

    /**
     * Returns the value of field 'cdProdutoServicoOperacao'.
     * 
     * @return int
     * @return the value of field 'cdProdutoServicoOperacao'.
     */
    public int getCdProdutoServicoOperacao()
    {
        return this._cdProdutoServicoOperacao;
    } //-- int getCdProdutoServicoOperacao() 

    /**
     * Returns the value of field 'cdRecebedorCredito'.
     * 
     * @return long
     * @return the value of field 'cdRecebedorCredito'.
     */
    public long getCdRecebedorCredito()
    {
        return this._cdRecebedorCredito;
    } //-- long getCdRecebedorCredito() 

    /**
     * Returns the value of field 'cdSiglaUfPagador'.
     * 
     * @return String
     * @return the value of field 'cdSiglaUfPagador'.
     */
    public java.lang.String getCdSiglaUfPagador()
    {
        return this._cdSiglaUfPagador;
    } //-- java.lang.String getCdSiglaUfPagador() 

    /**
     * Returns the value of field 'cdSituacaoSolicitacao'.
     * 
     * @return int
     * @return the value of field 'cdSituacaoSolicitacao'.
     */
    public int getCdSituacaoSolicitacao()
    {
        return this._cdSituacaoSolicitacao;
    } //-- int getCdSituacaoSolicitacao() 

    /**
     * Returns the value of field
     * 'cdSolicitacaoPagamentoIntegrado'.
     * 
     * @return int
     * @return the value of field 'cdSolicitacaoPagamentoIntegrado'.
     */
    public int getCdSolicitacaoPagamentoIntegrado()
    {
        return this._cdSolicitacaoPagamentoIntegrado;
    } //-- int getCdSolicitacaoPagamentoIntegrado() 

    /**
     * Returns the value of field 'cdTipoCanalInclusao'.
     * 
     * @return int
     * @return the value of field 'cdTipoCanalInclusao'.
     */
    public int getCdTipoCanalInclusao()
    {
        return this._cdTipoCanalInclusao;
    } //-- int getCdTipoCanalInclusao() 

    /**
     * Returns the value of field 'cdTipoInscricaoRecebedor'.
     * 
     * @return int
     * @return the value of field 'cdTipoInscricaoRecebedor'.
     */
    public int getCdTipoInscricaoRecebedor()
    {
        return this._cdTipoInscricaoRecebedor;
    } //-- int getCdTipoInscricaoRecebedor() 

    /**
     * Returns the value of field 'cdTipoPostagemSolicitacao'.
     * 
     * @return String
     * @return the value of field 'cdTipoPostagemSolicitacao'.
     */
    public java.lang.String getCdTipoPostagemSolicitacao()
    {
        return this._cdTipoPostagemSolicitacao;
    } //-- java.lang.String getCdTipoPostagemSolicitacao() 

    /**
     * Returns the value of field 'cdUsuarioInclusao'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioInclusao'.
     */
    public java.lang.String getCdUsuarioInclusao()
    {
        return this._cdUsuarioInclusao;
    } //-- java.lang.String getCdUsuarioInclusao() 

    /**
     * Returns the value of field 'codMensagem'.
     * 
     * @return String
     * @return the value of field 'codMensagem'.
     */
    public java.lang.String getCodMensagem()
    {
        return this._codMensagem;
    } //-- java.lang.String getCodMensagem() 

    /**
     * Returns the value of field 'dsAgencia'.
     * 
     * @return String
     * @return the value of field 'dsAgencia'.
     */
    public java.lang.String getDsAgencia()
    {
        return this._dsAgencia;
    } //-- java.lang.String getDsAgencia() 

    /**
     * Returns the value of field 'dsBairroClientePagador'.
     * 
     * @return String
     * @return the value of field 'dsBairroClientePagador'.
     */
    public java.lang.String getDsBairroClientePagador()
    {
        return this._dsBairroClientePagador;
    } //-- java.lang.String getDsBairroClientePagador() 

    /**
     * Returns the value of field 'dsComplementoLogradouroPagador'.
     * 
     * @return String
     * @return the value of field 'dsComplementoLogradouroPagador'.
     */
    public java.lang.String getDsComplementoLogradouroPagador()
    {
        return this._dsComplementoLogradouroPagador;
    } //-- java.lang.String getDsComplementoLogradouroPagador() 

    /**
     * Returns the value of field 'dsEmailClientePagador'.
     * 
     * @return String
     * @return the value of field 'dsEmailClientePagador'.
     */
    public java.lang.String getDsEmailClientePagador()
    {
        return this._dsEmailClientePagador;
    } //-- java.lang.String getDsEmailClientePagador() 

    /**
     * Returns the value of field 'dsLogradouroPagador'.
     * 
     * @return String
     * @return the value of field 'dsLogradouroPagador'.
     */
    public java.lang.String getDsLogradouroPagador()
    {
        return this._dsLogradouroPagador;
    } //-- java.lang.String getDsLogradouroPagador() 

    /**
     * Returns the value of field 'dsMunicipioClientePagador'.
     * 
     * @return String
     * @return the value of field 'dsMunicipioClientePagador'.
     */
    public java.lang.String getDsMunicipioClientePagador()
    {
        return this._dsMunicipioClientePagador;
    } //-- java.lang.String getDsMunicipioClientePagador() 

    /**
     * Returns the value of field 'dsNumeroLogradouroPagador'.
     * 
     * @return String
     * @return the value of field 'dsNumeroLogradouroPagador'.
     */
    public java.lang.String getDsNumeroLogradouroPagador()
    {
        return this._dsNumeroLogradouroPagador;
    } //-- java.lang.String getDsNumeroLogradouroPagador() 

    /**
     * Returns the value of field 'dsProdutoOperacaoRelacionado'.
     * 
     * @return String
     * @return the value of field 'dsProdutoOperacaoRelacionado'.
     */
    public java.lang.String getDsProdutoOperacaoRelacionado()
    {
        return this._dsProdutoOperacaoRelacionado;
    } //-- java.lang.String getDsProdutoOperacaoRelacionado() 

    /**
     * Returns the value of field 'dsProdutoServicoOperacao'.
     * 
     * @return String
     * @return the value of field 'dsProdutoServicoOperacao'.
     */
    public java.lang.String getDsProdutoServicoOperacao()
    {
        return this._dsProdutoServicoOperacao;
    } //-- java.lang.String getDsProdutoServicoOperacao() 

    /**
     * Returns the value of field 'dsSituacaoSolicitacao'.
     * 
     * @return String
     * @return the value of field 'dsSituacaoSolicitacao'.
     */
    public java.lang.String getDsSituacaoSolicitacao()
    {
        return this._dsSituacaoSolicitacao;
    } //-- java.lang.String getDsSituacaoSolicitacao() 

    /**
     * Returns the value of field 'dsTipoCanalInclusao'.
     * 
     * @return String
     * @return the value of field 'dsTipoCanalInclusao'.
     */
    public java.lang.String getDsTipoCanalInclusao()
    {
        return this._dsTipoCanalInclusao;
    } //-- java.lang.String getDsTipoCanalInclusao() 

    /**
     * Returns the value of field 'dtFimPeriodoMovimentacao'.
     * 
     * @return String
     * @return the value of field 'dtFimPeriodoMovimentacao'.
     */
    public java.lang.String getDtFimPeriodoMovimentacao()
    {
        return this._dtFimPeriodoMovimentacao;
    } //-- java.lang.String getDtFimPeriodoMovimentacao() 

    /**
     * Returns the value of field 'dtInicioPeriodoMovimentacao'.
     * 
     * @return String
     * @return the value of field 'dtInicioPeriodoMovimentacao'.
     */
    public java.lang.String getDtInicioPeriodoMovimentacao()
    {
        return this._dtInicioPeriodoMovimentacao;
    } //-- java.lang.String getDtInicioPeriodoMovimentacao() 

    /**
     * Returns the value of field 'hrInclusaoRegistro'.
     * 
     * @return String
     * @return the value of field 'hrInclusaoRegistro'.
     */
    public java.lang.String getHrInclusaoRegistro()
    {
        return this._hrInclusaoRegistro;
    } //-- java.lang.String getHrInclusaoRegistro() 

    /**
     * Returns the value of field 'mensagem'.
     * 
     * @return String
     * @return the value of field 'mensagem'.
     */
    public java.lang.String getMensagem()
    {
        return this._mensagem;
    } //-- java.lang.String getMensagem() 

    /**
     * Returns the value of field 'nrLinhas'.
     * 
     * @return int
     * @return the value of field 'nrLinhas'.
     */
    public int getNrLinhas()
    {
        return this._nrLinhas;
    } //-- int getNrLinhas() 

    /**
     * Returns the value of field
     * 'nrSolicitacaoPagamentoIntegrado'.
     * 
     * @return int
     * @return the value of field 'nrSolicitacaoPagamentoIntegrado'.
     */
    public int getNrSolicitacaoPagamentoIntegrado()
    {
        return this._nrSolicitacaoPagamentoIntegrado;
    } //-- int getNrSolicitacaoPagamentoIntegrado() 

    /**
     * Method getOcorrencias
     * 
     * 
     * 
     * @param index
     * @return Ocorrencias
     */
    public br.com.bradesco.web.pgit.service.data.pdc.detalharsolemiavimovtofavorecido.response.Ocorrencias getOcorrencias(int index)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index >= _ocorrenciasList.size())) {
            throw new IndexOutOfBoundsException("getOcorrencias: Index value '"+index+"' not in range [0.."+(_ocorrenciasList.size() - 1) + "]");
        }
        
        return (br.com.bradesco.web.pgit.service.data.pdc.detalharsolemiavimovtofavorecido.response.Ocorrencias) _ocorrenciasList.elementAt(index);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharsolemiavimovtofavorecido.response.Ocorrencias getOcorrencias(int) 

    /**
     * Method getOcorrencias
     * 
     * 
     * 
     * @return Ocorrencias
     */
    public br.com.bradesco.web.pgit.service.data.pdc.detalharsolemiavimovtofavorecido.response.Ocorrencias[] getOcorrencias()
    {
        int size = _ocorrenciasList.size();
        br.com.bradesco.web.pgit.service.data.pdc.detalharsolemiavimovtofavorecido.response.Ocorrencias[] mArray = new br.com.bradesco.web.pgit.service.data.pdc.detalharsolemiavimovtofavorecido.response.Ocorrencias[size];
        for (int index = 0; index < size; index++) {
            mArray[index] = (br.com.bradesco.web.pgit.service.data.pdc.detalharsolemiavimovtofavorecido.response.Ocorrencias) _ocorrenciasList.elementAt(index);
        }
        return mArray;
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharsolemiavimovtofavorecido.response.Ocorrencias[] getOcorrencias() 

    /**
     * Method getOcorrenciasCount
     * 
     * 
     * 
     * @return int
     */
    public int getOcorrenciasCount()
    {
        return _ocorrenciasList.size();
    } //-- int getOcorrenciasCount() 

    /**
     * Returns the value of field 'vlTarifaPadrao'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlTarifaPadrao'.
     */
    public java.math.BigDecimal getVlTarifaPadrao()
    {
        return this._vlTarifaPadrao;
    } //-- java.math.BigDecimal getVlTarifaPadrao() 

    /**
     * Returns the value of field 'vlrTarifaAtual'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlrTarifaAtual'.
     */
    public java.math.BigDecimal getVlrTarifaAtual()
    {
        return this._vlrTarifaAtual;
    } //-- java.math.BigDecimal getVlrTarifaAtual() 

    /**
     * Method hasCdAgenciaOperadora
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgenciaOperadora()
    {
        return this._has_cdAgenciaOperadora;
    } //-- boolean hasCdAgenciaOperadora() 

    /**
     * Method hasCdCepComplementoPagador
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCepComplementoPagador()
    {
        return this._has_cdCepComplementoPagador;
    } //-- boolean hasCdCepComplementoPagador() 

    /**
     * Method hasCdCepPagador
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCepPagador()
    {
        return this._has_cdCepPagador;
    } //-- boolean hasCdCepPagador() 

    /**
     * Method hasCdControleCpfRecebedor
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdControleCpfRecebedor()
    {
        return this._has_cdControleCpfRecebedor;
    } //-- boolean hasCdControleCpfRecebedor() 

    /**
     * Method hasCdCpfCnpjRecebedor
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCpfCnpjRecebedor()
    {
        return this._has_cdCpfCnpjRecebedor;
    } //-- boolean hasCdCpfCnpjRecebedor() 

    /**
     * Method hasCdDepartamentoUnidade
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdDepartamentoUnidade()
    {
        return this._has_cdDepartamentoUnidade;
    } //-- boolean hasCdDepartamentoUnidade() 

    /**
     * Method hasCdDestinoCorrespSolicitacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdDestinoCorrespSolicitacao()
    {
        return this._has_cdDestinoCorrespSolicitacao;
    } //-- boolean hasCdDestinoCorrespSolicitacao() 

    /**
     * Method hasCdFilialCnpjRecebedor
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFilialCnpjRecebedor()
    {
        return this._has_cdFilialCnpjRecebedor;
    } //-- boolean hasCdFilialCnpjRecebedor() 

    /**
     * Method hasCdProdutoOperacaoRelacionado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdutoOperacaoRelacionado()
    {
        return this._has_cdProdutoOperacaoRelacionado;
    } //-- boolean hasCdProdutoOperacaoRelacionado() 

    /**
     * Method hasCdProdutoServicoOperacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdutoServicoOperacao()
    {
        return this._has_cdProdutoServicoOperacao;
    } //-- boolean hasCdProdutoServicoOperacao() 

    /**
     * Method hasCdRecebedorCredito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdRecebedorCredito()
    {
        return this._has_cdRecebedorCredito;
    } //-- boolean hasCdRecebedorCredito() 

    /**
     * Method hasCdSituacaoSolicitacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSituacaoSolicitacao()
    {
        return this._has_cdSituacaoSolicitacao;
    } //-- boolean hasCdSituacaoSolicitacao() 

    /**
     * Method hasCdSolicitacaoPagamentoIntegrado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSolicitacaoPagamentoIntegrado()
    {
        return this._has_cdSolicitacaoPagamentoIntegrado;
    } //-- boolean hasCdSolicitacaoPagamentoIntegrado() 

    /**
     * Method hasCdTipoCanalInclusao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoCanalInclusao()
    {
        return this._has_cdTipoCanalInclusao;
    } //-- boolean hasCdTipoCanalInclusao() 

    /**
     * Method hasCdTipoInscricaoRecebedor
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoInscricaoRecebedor()
    {
        return this._has_cdTipoInscricaoRecebedor;
    } //-- boolean hasCdTipoInscricaoRecebedor() 

    /**
     * Method hasNrLinhas
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrLinhas()
    {
        return this._has_nrLinhas;
    } //-- boolean hasNrLinhas() 

    /**
     * Method hasNrSolicitacaoPagamentoIntegrado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSolicitacaoPagamentoIntegrado()
    {
        return this._has_nrSolicitacaoPagamentoIntegrado;
    } //-- boolean hasNrSolicitacaoPagamentoIntegrado() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Method removeAllOcorrencias
     * 
     */
    public void removeAllOcorrencias()
    {
        _ocorrenciasList.removeAllElements();
    } //-- void removeAllOcorrencias() 

    /**
     * Method removeOcorrencias
     * 
     * 
     * 
     * @param index
     * @return Ocorrencias
     */
    public br.com.bradesco.web.pgit.service.data.pdc.detalharsolemiavimovtofavorecido.response.Ocorrencias removeOcorrencias(int index)
    {
        java.lang.Object obj = _ocorrenciasList.elementAt(index);
        _ocorrenciasList.removeElementAt(index);
        return (br.com.bradesco.web.pgit.service.data.pdc.detalharsolemiavimovtofavorecido.response.Ocorrencias) obj;
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharsolemiavimovtofavorecido.response.Ocorrencias removeOcorrencias(int) 

    /**
     * Sets the value of field 'cdAgenciaOperadora'.
     * 
     * @param cdAgenciaOperadora the value of field
     * 'cdAgenciaOperadora'.
     */
    public void setCdAgenciaOperadora(int cdAgenciaOperadora)
    {
        this._cdAgenciaOperadora = cdAgenciaOperadora;
        this._has_cdAgenciaOperadora = true;
    } //-- void setCdAgenciaOperadora(int) 

    /**
     * Sets the value of field 'cdCepComplementoPagador'.
     * 
     * @param cdCepComplementoPagador the value of field
     * 'cdCepComplementoPagador'.
     */
    public void setCdCepComplementoPagador(int cdCepComplementoPagador)
    {
        this._cdCepComplementoPagador = cdCepComplementoPagador;
        this._has_cdCepComplementoPagador = true;
    } //-- void setCdCepComplementoPagador(int) 

    /**
     * Sets the value of field 'cdCepPagador'.
     * 
     * @param cdCepPagador the value of field 'cdCepPagador'.
     */
    public void setCdCepPagador(int cdCepPagador)
    {
        this._cdCepPagador = cdCepPagador;
        this._has_cdCepPagador = true;
    } //-- void setCdCepPagador(int) 

    /**
     * Sets the value of field 'cdControleCpfRecebedor'.
     * 
     * @param cdControleCpfRecebedor the value of field
     * 'cdControleCpfRecebedor'.
     */
    public void setCdControleCpfRecebedor(int cdControleCpfRecebedor)
    {
        this._cdControleCpfRecebedor = cdControleCpfRecebedor;
        this._has_cdControleCpfRecebedor = true;
    } //-- void setCdControleCpfRecebedor(int) 

    /**
     * Sets the value of field 'cdCpfCnpjRecebedor'.
     * 
     * @param cdCpfCnpjRecebedor the value of field
     * 'cdCpfCnpjRecebedor'.
     */
    public void setCdCpfCnpjRecebedor(long cdCpfCnpjRecebedor)
    {
        this._cdCpfCnpjRecebedor = cdCpfCnpjRecebedor;
        this._has_cdCpfCnpjRecebedor = true;
    } //-- void setCdCpfCnpjRecebedor(long) 

    /**
     * Sets the value of field 'cdDepartamentoUnidade'.
     * 
     * @param cdDepartamentoUnidade the value of field
     * 'cdDepartamentoUnidade'.
     */
    public void setCdDepartamentoUnidade(int cdDepartamentoUnidade)
    {
        this._cdDepartamentoUnidade = cdDepartamentoUnidade;
        this._has_cdDepartamentoUnidade = true;
    } //-- void setCdDepartamentoUnidade(int) 

    /**
     * Sets the value of field 'cdDestinoCorrespSolicitacao'.
     * 
     * @param cdDestinoCorrespSolicitacao the value of field
     * 'cdDestinoCorrespSolicitacao'.
     */
    public void setCdDestinoCorrespSolicitacao(int cdDestinoCorrespSolicitacao)
    {
        this._cdDestinoCorrespSolicitacao = cdDestinoCorrespSolicitacao;
        this._has_cdDestinoCorrespSolicitacao = true;
    } //-- void setCdDestinoCorrespSolicitacao(int) 

    /**
     * Sets the value of field 'cdFilialCnpjRecebedor'.
     * 
     * @param cdFilialCnpjRecebedor the value of field
     * 'cdFilialCnpjRecebedor'.
     */
    public void setCdFilialCnpjRecebedor(int cdFilialCnpjRecebedor)
    {
        this._cdFilialCnpjRecebedor = cdFilialCnpjRecebedor;
        this._has_cdFilialCnpjRecebedor = true;
    } //-- void setCdFilialCnpjRecebedor(int) 

    /**
     * Sets the value of field 'cdOperacaoCanalInclusao'.
     * 
     * @param cdOperacaoCanalInclusao the value of field
     * 'cdOperacaoCanalInclusao'.
     */
    public void setCdOperacaoCanalInclusao(java.lang.String cdOperacaoCanalInclusao)
    {
        this._cdOperacaoCanalInclusao = cdOperacaoCanalInclusao;
    } //-- void setCdOperacaoCanalInclusao(java.lang.String) 

    /**
     * Sets the value of field 'cdPercentualDescTarifa'.
     * 
     * @param cdPercentualDescTarifa the value of field
     * 'cdPercentualDescTarifa'.
     */
    public void setCdPercentualDescTarifa(java.math.BigDecimal cdPercentualDescTarifa)
    {
        this._cdPercentualDescTarifa = cdPercentualDescTarifa;
    } //-- void setCdPercentualDescTarifa(java.math.BigDecimal) 

    /**
     * Sets the value of field 'cdProdutoOperacaoRelacionado'.
     * 
     * @param cdProdutoOperacaoRelacionado the value of field
     * 'cdProdutoOperacaoRelacionado'.
     */
    public void setCdProdutoOperacaoRelacionado(int cdProdutoOperacaoRelacionado)
    {
        this._cdProdutoOperacaoRelacionado = cdProdutoOperacaoRelacionado;
        this._has_cdProdutoOperacaoRelacionado = true;
    } //-- void setCdProdutoOperacaoRelacionado(int) 

    /**
     * Sets the value of field 'cdProdutoServicoOperacao'.
     * 
     * @param cdProdutoServicoOperacao the value of field
     * 'cdProdutoServicoOperacao'.
     */
    public void setCdProdutoServicoOperacao(int cdProdutoServicoOperacao)
    {
        this._cdProdutoServicoOperacao = cdProdutoServicoOperacao;
        this._has_cdProdutoServicoOperacao = true;
    } //-- void setCdProdutoServicoOperacao(int) 

    /**
     * Sets the value of field 'cdRecebedorCredito'.
     * 
     * @param cdRecebedorCredito the value of field
     * 'cdRecebedorCredito'.
     */
    public void setCdRecebedorCredito(long cdRecebedorCredito)
    {
        this._cdRecebedorCredito = cdRecebedorCredito;
        this._has_cdRecebedorCredito = true;
    } //-- void setCdRecebedorCredito(long) 

    /**
     * Sets the value of field 'cdSiglaUfPagador'.
     * 
     * @param cdSiglaUfPagador the value of field 'cdSiglaUfPagador'
     */
    public void setCdSiglaUfPagador(java.lang.String cdSiglaUfPagador)
    {
        this._cdSiglaUfPagador = cdSiglaUfPagador;
    } //-- void setCdSiglaUfPagador(java.lang.String) 

    /**
     * Sets the value of field 'cdSituacaoSolicitacao'.
     * 
     * @param cdSituacaoSolicitacao the value of field
     * 'cdSituacaoSolicitacao'.
     */
    public void setCdSituacaoSolicitacao(int cdSituacaoSolicitacao)
    {
        this._cdSituacaoSolicitacao = cdSituacaoSolicitacao;
        this._has_cdSituacaoSolicitacao = true;
    } //-- void setCdSituacaoSolicitacao(int) 

    /**
     * Sets the value of field 'cdSolicitacaoPagamentoIntegrado'.
     * 
     * @param cdSolicitacaoPagamentoIntegrado the value of field
     * 'cdSolicitacaoPagamentoIntegrado'.
     */
    public void setCdSolicitacaoPagamentoIntegrado(int cdSolicitacaoPagamentoIntegrado)
    {
        this._cdSolicitacaoPagamentoIntegrado = cdSolicitacaoPagamentoIntegrado;
        this._has_cdSolicitacaoPagamentoIntegrado = true;
    } //-- void setCdSolicitacaoPagamentoIntegrado(int) 

    /**
     * Sets the value of field 'cdTipoCanalInclusao'.
     * 
     * @param cdTipoCanalInclusao the value of field
     * 'cdTipoCanalInclusao'.
     */
    public void setCdTipoCanalInclusao(int cdTipoCanalInclusao)
    {
        this._cdTipoCanalInclusao = cdTipoCanalInclusao;
        this._has_cdTipoCanalInclusao = true;
    } //-- void setCdTipoCanalInclusao(int) 

    /**
     * Sets the value of field 'cdTipoInscricaoRecebedor'.
     * 
     * @param cdTipoInscricaoRecebedor the value of field
     * 'cdTipoInscricaoRecebedor'.
     */
    public void setCdTipoInscricaoRecebedor(int cdTipoInscricaoRecebedor)
    {
        this._cdTipoInscricaoRecebedor = cdTipoInscricaoRecebedor;
        this._has_cdTipoInscricaoRecebedor = true;
    } //-- void setCdTipoInscricaoRecebedor(int) 

    /**
     * Sets the value of field 'cdTipoPostagemSolicitacao'.
     * 
     * @param cdTipoPostagemSolicitacao the value of field
     * 'cdTipoPostagemSolicitacao'.
     */
    public void setCdTipoPostagemSolicitacao(java.lang.String cdTipoPostagemSolicitacao)
    {
        this._cdTipoPostagemSolicitacao = cdTipoPostagemSolicitacao;
    } //-- void setCdTipoPostagemSolicitacao(java.lang.String) 

    /**
     * Sets the value of field 'cdUsuarioInclusao'.
     * 
     * @param cdUsuarioInclusao the value of field
     * 'cdUsuarioInclusao'.
     */
    public void setCdUsuarioInclusao(java.lang.String cdUsuarioInclusao)
    {
        this._cdUsuarioInclusao = cdUsuarioInclusao;
    } //-- void setCdUsuarioInclusao(java.lang.String) 

    /**
     * Sets the value of field 'codMensagem'.
     * 
     * @param codMensagem the value of field 'codMensagem'.
     */
    public void setCodMensagem(java.lang.String codMensagem)
    {
        this._codMensagem = codMensagem;
    } //-- void setCodMensagem(java.lang.String) 

    /**
     * Sets the value of field 'dsAgencia'.
     * 
     * @param dsAgencia the value of field 'dsAgencia'.
     */
    public void setDsAgencia(java.lang.String dsAgencia)
    {
        this._dsAgencia = dsAgencia;
    } //-- void setDsAgencia(java.lang.String) 

    /**
     * Sets the value of field 'dsBairroClientePagador'.
     * 
     * @param dsBairroClientePagador the value of field
     * 'dsBairroClientePagador'.
     */
    public void setDsBairroClientePagador(java.lang.String dsBairroClientePagador)
    {
        this._dsBairroClientePagador = dsBairroClientePagador;
    } //-- void setDsBairroClientePagador(java.lang.String) 

    /**
     * Sets the value of field 'dsComplementoLogradouroPagador'.
     * 
     * @param dsComplementoLogradouroPagador the value of field
     * 'dsComplementoLogradouroPagador'.
     */
    public void setDsComplementoLogradouroPagador(java.lang.String dsComplementoLogradouroPagador)
    {
        this._dsComplementoLogradouroPagador = dsComplementoLogradouroPagador;
    } //-- void setDsComplementoLogradouroPagador(java.lang.String) 

    /**
     * Sets the value of field 'dsEmailClientePagador'.
     * 
     * @param dsEmailClientePagador the value of field
     * 'dsEmailClientePagador'.
     */
    public void setDsEmailClientePagador(java.lang.String dsEmailClientePagador)
    {
        this._dsEmailClientePagador = dsEmailClientePagador;
    } //-- void setDsEmailClientePagador(java.lang.String) 

    /**
     * Sets the value of field 'dsLogradouroPagador'.
     * 
     * @param dsLogradouroPagador the value of field
     * 'dsLogradouroPagador'.
     */
    public void setDsLogradouroPagador(java.lang.String dsLogradouroPagador)
    {
        this._dsLogradouroPagador = dsLogradouroPagador;
    } //-- void setDsLogradouroPagador(java.lang.String) 

    /**
     * Sets the value of field 'dsMunicipioClientePagador'.
     * 
     * @param dsMunicipioClientePagador the value of field
     * 'dsMunicipioClientePagador'.
     */
    public void setDsMunicipioClientePagador(java.lang.String dsMunicipioClientePagador)
    {
        this._dsMunicipioClientePagador = dsMunicipioClientePagador;
    } //-- void setDsMunicipioClientePagador(java.lang.String) 

    /**
     * Sets the value of field 'dsNumeroLogradouroPagador'.
     * 
     * @param dsNumeroLogradouroPagador the value of field
     * 'dsNumeroLogradouroPagador'.
     */
    public void setDsNumeroLogradouroPagador(java.lang.String dsNumeroLogradouroPagador)
    {
        this._dsNumeroLogradouroPagador = dsNumeroLogradouroPagador;
    } //-- void setDsNumeroLogradouroPagador(java.lang.String) 

    /**
     * Sets the value of field 'dsProdutoOperacaoRelacionado'.
     * 
     * @param dsProdutoOperacaoRelacionado the value of field
     * 'dsProdutoOperacaoRelacionado'.
     */
    public void setDsProdutoOperacaoRelacionado(java.lang.String dsProdutoOperacaoRelacionado)
    {
        this._dsProdutoOperacaoRelacionado = dsProdutoOperacaoRelacionado;
    } //-- void setDsProdutoOperacaoRelacionado(java.lang.String) 

    /**
     * Sets the value of field 'dsProdutoServicoOperacao'.
     * 
     * @param dsProdutoServicoOperacao the value of field
     * 'dsProdutoServicoOperacao'.
     */
    public void setDsProdutoServicoOperacao(java.lang.String dsProdutoServicoOperacao)
    {
        this._dsProdutoServicoOperacao = dsProdutoServicoOperacao;
    } //-- void setDsProdutoServicoOperacao(java.lang.String) 

    /**
     * Sets the value of field 'dsSituacaoSolicitacao'.
     * 
     * @param dsSituacaoSolicitacao the value of field
     * 'dsSituacaoSolicitacao'.
     */
    public void setDsSituacaoSolicitacao(java.lang.String dsSituacaoSolicitacao)
    {
        this._dsSituacaoSolicitacao = dsSituacaoSolicitacao;
    } //-- void setDsSituacaoSolicitacao(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoCanalInclusao'.
     * 
     * @param dsTipoCanalInclusao the value of field
     * 'dsTipoCanalInclusao'.
     */
    public void setDsTipoCanalInclusao(java.lang.String dsTipoCanalInclusao)
    {
        this._dsTipoCanalInclusao = dsTipoCanalInclusao;
    } //-- void setDsTipoCanalInclusao(java.lang.String) 

    /**
     * Sets the value of field 'dtFimPeriodoMovimentacao'.
     * 
     * @param dtFimPeriodoMovimentacao the value of field
     * 'dtFimPeriodoMovimentacao'.
     */
    public void setDtFimPeriodoMovimentacao(java.lang.String dtFimPeriodoMovimentacao)
    {
        this._dtFimPeriodoMovimentacao = dtFimPeriodoMovimentacao;
    } //-- void setDtFimPeriodoMovimentacao(java.lang.String) 

    /**
     * Sets the value of field 'dtInicioPeriodoMovimentacao'.
     * 
     * @param dtInicioPeriodoMovimentacao the value of field
     * 'dtInicioPeriodoMovimentacao'.
     */
    public void setDtInicioPeriodoMovimentacao(java.lang.String dtInicioPeriodoMovimentacao)
    {
        this._dtInicioPeriodoMovimentacao = dtInicioPeriodoMovimentacao;
    } //-- void setDtInicioPeriodoMovimentacao(java.lang.String) 

    /**
     * Sets the value of field 'hrInclusaoRegistro'.
     * 
     * @param hrInclusaoRegistro the value of field
     * 'hrInclusaoRegistro'.
     */
    public void setHrInclusaoRegistro(java.lang.String hrInclusaoRegistro)
    {
        this._hrInclusaoRegistro = hrInclusaoRegistro;
    } //-- void setHrInclusaoRegistro(java.lang.String) 

    /**
     * Sets the value of field 'mensagem'.
     * 
     * @param mensagem the value of field 'mensagem'.
     */
    public void setMensagem(java.lang.String mensagem)
    {
        this._mensagem = mensagem;
    } //-- void setMensagem(java.lang.String) 

    /**
     * Sets the value of field 'nrLinhas'.
     * 
     * @param nrLinhas the value of field 'nrLinhas'.
     */
    public void setNrLinhas(int nrLinhas)
    {
        this._nrLinhas = nrLinhas;
        this._has_nrLinhas = true;
    } //-- void setNrLinhas(int) 

    /**
     * Sets the value of field 'nrSolicitacaoPagamentoIntegrado'.
     * 
     * @param nrSolicitacaoPagamentoIntegrado the value of field
     * 'nrSolicitacaoPagamentoIntegrado'.
     */
    public void setNrSolicitacaoPagamentoIntegrado(int nrSolicitacaoPagamentoIntegrado)
    {
        this._nrSolicitacaoPagamentoIntegrado = nrSolicitacaoPagamentoIntegrado;
        this._has_nrSolicitacaoPagamentoIntegrado = true;
    } //-- void setNrSolicitacaoPagamentoIntegrado(int) 

    /**
     * Method setOcorrencias
     * 
     * 
     * 
     * @param index
     * @param vOcorrencias
     */
    public void setOcorrencias(int index, br.com.bradesco.web.pgit.service.data.pdc.detalharsolemiavimovtofavorecido.response.Ocorrencias vOcorrencias)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index >= _ocorrenciasList.size())) {
            throw new IndexOutOfBoundsException("setOcorrencias: Index value '"+index+"' not in range [0.." + (_ocorrenciasList.size() - 1) + "]");
        }
        _ocorrenciasList.setElementAt(vOcorrencias, index);
    } //-- void setOcorrencias(int, br.com.bradesco.web.pgit.service.data.pdc.detalharsolemiavimovtofavorecido.response.Ocorrencias) 

    /**
     * Method setOcorrencias
     * 
     * 
     * 
     * @param ocorrenciasArray
     */
    public void setOcorrencias(br.com.bradesco.web.pgit.service.data.pdc.detalharsolemiavimovtofavorecido.response.Ocorrencias[] ocorrenciasArray)
    {
        //-- copy array
        _ocorrenciasList.removeAllElements();
        for (int i = 0; i < ocorrenciasArray.length; i++) {
            _ocorrenciasList.addElement(ocorrenciasArray[i]);
        }
    } //-- void setOcorrencias(br.com.bradesco.web.pgit.service.data.pdc.detalharsolemiavimovtofavorecido.response.Ocorrencias) 

    /**
     * Sets the value of field 'vlTarifaPadrao'.
     * 
     * @param vlTarifaPadrao the value of field 'vlTarifaPadrao'.
     */
    public void setVlTarifaPadrao(java.math.BigDecimal vlTarifaPadrao)
    {
        this._vlTarifaPadrao = vlTarifaPadrao;
    } //-- void setVlTarifaPadrao(java.math.BigDecimal) 

    /**
     * Sets the value of field 'vlrTarifaAtual'.
     * 
     * @param vlrTarifaAtual the value of field 'vlrTarifaAtual'.
     */
    public void setVlrTarifaAtual(java.math.BigDecimal vlrTarifaAtual)
    {
        this._vlrTarifaAtual = vlrTarifaAtual;
    } //-- void setVlrTarifaAtual(java.math.BigDecimal) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return DetalharSolEmiAviMovtoFavorecidoResponse
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.detalharsolemiavimovtofavorecido.response.DetalharSolEmiAviMovtoFavorecidoResponse unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.detalharsolemiavimovtofavorecido.response.DetalharSolEmiAviMovtoFavorecidoResponse) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.detalharsolemiavimovtofavorecido.response.DetalharSolEmiAviMovtoFavorecidoResponse.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharsolemiavimovtofavorecido.response.DetalharSolEmiAviMovtoFavorecidoResponse unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
