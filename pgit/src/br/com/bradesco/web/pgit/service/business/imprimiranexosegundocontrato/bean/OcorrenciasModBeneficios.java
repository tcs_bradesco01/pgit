/*
 * Nome: br.com.bradesco.web.pgit.service.business.imprimiranexosegundocontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.imprimiranexosegundocontrato.bean;

/**
 * Nome: OcorrenciasModBeneficios
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class OcorrenciasModBeneficios {

	/** Atributo cdModalidadeBeneficio. */
	private String cdModalidadeBeneficio;

	/**
	 * Ocorrencias mod beneficios.
	 */
	public OcorrenciasModBeneficios() {
		super();
	}

	/**
	 * Ocorrencias mod beneficios.
	 *
	 * @param cdModalidadeBeneficio the cd modalidade beneficio
	 */
	public OcorrenciasModBeneficios(String cdModalidadeBeneficio) {
		super();
		this.cdModalidadeBeneficio = cdModalidadeBeneficio;
	}

	/**
	 * Get: cdModalidadeBeneficio.
	 *
	 * @return cdModalidadeBeneficio
	 */
	public String getCdModalidadeBeneficio() {
		return cdModalidadeBeneficio;
	}

	/**
	 * Set: cdModalidadeBeneficio.
	 *
	 * @param cdModalidadeBeneficio the cd modalidade beneficio
	 */
	public void setCdModalidadeBeneficio(String cdModalidadeBeneficio) {
		this.cdModalidadeBeneficio = cdModalidadeBeneficio;
	}
}