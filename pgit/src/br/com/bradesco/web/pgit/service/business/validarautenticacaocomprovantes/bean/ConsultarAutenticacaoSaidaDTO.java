/*
 * Nome: br.com.bradesco.web.pgit.service.business.validarautenticacaocomprovantes.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.validarautenticacaocomprovantes.bean;


/**
 * Nome: ConsultarAutenticacaoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarAutenticacaoSaidaDTO {

	/** Atributo mensagem. */
	private String mensagem;
	
	/** Atributo codMensagem. */
	private String codMensagem;
	
	/** Atributo cdAutenticacao. */
	private String cdAutenticacao;
	
	
	
	/**
	 * Consultar autenticacao saida dto.
	 *
	 * @param mensagem the mensagem
	 * @param codMensagem the cod mensagem
	 * @param cdAutenticacao the cd autenticacao
	 */
	public ConsultarAutenticacaoSaidaDTO(String mensagem, String codMensagem, String cdAutenticacao){
		this.mensagem = mensagem;
		this.codMensagem = codMensagem;
		this.cdAutenticacao = cdAutenticacao;		
	}
	
	/**
	 * Get: cdAutenticacao.
	 *
	 * @return cdAutenticacao
	 */
	public String getCdAutenticacao() {
		return cdAutenticacao;
	}
	
	/**
	 * Set: cdAutenticacao.
	 *
	 * @param cdAutenticacao the cd autenticacao
	 */
	public void setCdAutenticacao(String cdAutenticacao) {
		this.cdAutenticacao = cdAutenticacao;
	}
	
	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}
	
	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	
}
