/*
 * Nome: br.com.bradesco.web.pgit.service.business.formaliqpagtoint.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.formaliqpagtoint.bean;

import java.util.List;



/**
 * Nome: ListarLiquidacaoPagamentoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarLiquidacaoPagamentoSaidaDTO {
	
	/** Atributo codMensagem. */
	private String codMensagem;
    
    /** Atributo mensagem. */
    private String mensagem;
    
    /** Atributo numeroConsultas. */
    private Integer numeroConsultas;
    
    /** Atributo ocorrencias. */
    private List<ListarLiquidacaoPagamentoOcorrenciaSaidaDTO> ocorrencias;

    /**
     * Listar liquidacao pagamento saida dto.
     */
    public ListarLiquidacaoPagamentoSaidaDTO(){
    	
    }
    
    /**
     * Listar liquidacao pagamento saida dto.
     *
     * @param ocorrencias the ocorrencias
     */
    public ListarLiquidacaoPagamentoSaidaDTO(
			List<ListarLiquidacaoPagamentoOcorrenciaSaidaDTO> ocorrencias) {
		super();
		this.ocorrencias = ocorrencias;
	}
	
	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}
	
	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	/**
	 * Get: numeroConsultas.
	 *
	 * @return numeroConsultas
	 */
	public Integer getNumeroConsultas() {
		return numeroConsultas;
	}
	
	/**
	 * Set: numeroConsultas.
	 *
	 * @param numeroConsultas the numero consultas
	 */
	public void setNumeroConsultas(Integer numeroConsultas) {
		this.numeroConsultas = numeroConsultas;
	}
	
	/**
	 * Get: ocorrencias.
	 *
	 * @return ocorrencias
	 */
	public List<ListarLiquidacaoPagamentoOcorrenciaSaidaDTO> getOcorrencias() {
		return ocorrencias;
	}
	
	/**
	 * Set: ocorrencias.
	 *
	 * @param ocorrencias the ocorrencias
	 */
	public void setOcorrencias(
			List<ListarLiquidacaoPagamentoOcorrenciaSaidaDTO> ocorrencias) {
		this.ocorrencias = ocorrencias;
	}
	
}