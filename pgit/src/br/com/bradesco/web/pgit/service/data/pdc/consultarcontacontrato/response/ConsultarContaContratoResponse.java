/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultarcontacontrato.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.Vector;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ConsultarContaContratoResponse.
 * 
 * @version $Revision$ $Date$
 */
public class ConsultarContaContratoResponse implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _codMensagem
     */
    private java.lang.String _codMensagem;

    /**
     * Field _mensagem
     */
    private java.lang.String _mensagem;

    /**
     * Field _cdSituacaoVinculacaoConta
     */
    private int _cdSituacaoVinculacaoConta = 0;

    /**
     * keeps track of state for field: _cdSituacaoVinculacaoConta
     */
    private boolean _has_cdSituacaoVinculacaoConta;

    /**
     * Field _dsSituacaoVinculacaoConta
     */
    private java.lang.String _dsSituacaoVinculacaoConta;

    /**
     * Field _cdMotivoSituacaoConta
     */
    private int _cdMotivoSituacaoConta = 0;

    /**
     * keeps track of state for field: _cdMotivoSituacaoConta
     */
    private boolean _has_cdMotivoSituacaoConta;

    /**
     * Field _dsMotivoSituacaoConta
     */
    private java.lang.String _dsMotivoSituacaoConta;

    /**
     * Field _cdUf
     */
    private int _cdUf = 0;

    /**
     * keeps track of state for field: _cdUf
     */
    private boolean _has_cdUf;

    /**
     * Field _dsUf
     */
    private java.lang.String _dsUf;

    /**
     * Field _cdSiglaUf
     */
    private java.lang.String _cdSiglaUf;

    /**
     * Field _cdMunicipio
     */
    private long _cdMunicipio = 0;

    /**
     * keeps track of state for field: _cdMunicipio
     */
    private boolean _has_cdMunicipio;

    /**
     * Field _dsMunicipio
     */
    private java.lang.String _dsMunicipio;

    /**
     * Field _cdMunicipioFeriadoLocal
     */
    private int _cdMunicipioFeriadoLocal = 0;

    /**
     * keeps track of state for field: _cdMunicipioFeriadoLocal
     */
    private boolean _has_cdMunicipioFeriadoLocal;

    /**
     * Field _vlAdicionalContratoConta
     */
    private java.math.BigDecimal _vlAdicionalContratoConta = new java.math.BigDecimal("0");

    /**
     * Field _dtInicioValorAdicional
     */
    private java.lang.String _dtInicioValorAdicional;

    /**
     * Field _dtFimValorAdicional
     */
    private java.lang.String _dtFimValorAdicional;

    /**
     * Field _vlAdicContrCtaTed
     */
    private java.math.BigDecimal _vlAdicContrCtaTed = new java.math.BigDecimal("0");

    /**
     * Field _dtIniVlrAdicionalTed
     */
    private java.lang.String _dtIniVlrAdicionalTed;

    /**
     * Field _dtFimVlrAdicionalTed
     */
    private java.lang.String _dtFimVlrAdicionalTed;

    /**
     * Field _descApelido
     */
    private java.lang.String _descApelido;

    /**
     * Field _cdUsuarioInclusao
     */
    private java.lang.String _cdUsuarioInclusao;

    /**
     * Field _cdUsuarioInclusaoExterno
     */
    private java.lang.String _cdUsuarioInclusaoExterno;

    /**
     * Field _hrInclusaoRegistro
     */
    private java.lang.String _hrInclusaoRegistro;

    /**
     * Field _cdOperacaoCanalInclusao
     */
    private java.lang.String _cdOperacaoCanalInclusao;

    /**
     * Field _cdTipoCanalInclusao
     */
    private int _cdTipoCanalInclusao = 0;

    /**
     * keeps track of state for field: _cdTipoCanalInclusao
     */
    private boolean _has_cdTipoCanalInclusao;

    /**
     * Field _dsTipoCanalInclusao
     */
    private java.lang.String _dsTipoCanalInclusao;

    /**
     * Field _cdUsuarioManutencao
     */
    private java.lang.String _cdUsuarioManutencao;

    /**
     * Field _cdUsuarioManutencaoExterno
     */
    private java.lang.String _cdUsuarioManutencaoExterno;

    /**
     * Field _hrManutencaoRegistro
     */
    private java.lang.String _hrManutencaoRegistro;

    /**
     * Field _cdOperacaoCanalManutencao
     */
    private java.lang.String _cdOperacaoCanalManutencao;

    /**
     * Field _cdTipoCanalManutencao
     */
    private int _cdTipoCanalManutencao = 0;

    /**
     * keeps track of state for field: _cdTipoCanalManutencao
     */
    private boolean _has_cdTipoCanalManutencao;

    /**
     * Field _dsTipoCanalManutencao
     */
    private java.lang.String _dsTipoCanalManutencao;

    /**
     * Field _numeroOcorrencias
     */
    private int _numeroOcorrencias = 0;

    /**
     * keeps track of state for field: _numeroOcorrencias
     */
    private boolean _has_numeroOcorrencias;

    /**
     * Field _ocorrenciasList
     */
    private java.util.Vector _ocorrenciasList;


      //----------------/
     //- Constructors -/
    //----------------/

    public ConsultarContaContratoResponse() 
     {
        super();
        setVlAdicionalContratoConta(new java.math.BigDecimal("0"));
        setVlAdicContrCtaTed(new java.math.BigDecimal("0"));
        _ocorrenciasList = new Vector();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarcontacontrato.response.ConsultarContaContratoResponse()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method addOcorrencias
     * 
     * 
     * 
     * @param vOcorrencias
     */
    public void addOcorrencias(br.com.bradesco.web.pgit.service.data.pdc.consultarcontacontrato.response.Ocorrencias vOcorrencias)
        throws java.lang.IndexOutOfBoundsException
    {
        _ocorrenciasList.addElement(vOcorrencias);
    } //-- void addOcorrencias(br.com.bradesco.web.pgit.service.data.pdc.consultarcontacontrato.response.Ocorrencias) 

    /**
     * Method addOcorrencias
     * 
     * 
     * 
     * @param index
     * @param vOcorrencias
     */
    public void addOcorrencias(int index, br.com.bradesco.web.pgit.service.data.pdc.consultarcontacontrato.response.Ocorrencias vOcorrencias)
        throws java.lang.IndexOutOfBoundsException
    {
        _ocorrenciasList.insertElementAt(vOcorrencias, index);
    } //-- void addOcorrencias(int, br.com.bradesco.web.pgit.service.data.pdc.consultarcontacontrato.response.Ocorrencias) 

    /**
     * Method deleteCdMotivoSituacaoConta
     * 
     */
    public void deleteCdMotivoSituacaoConta()
    {
        this._has_cdMotivoSituacaoConta= false;
    } //-- void deleteCdMotivoSituacaoConta() 

    /**
     * Method deleteCdMunicipio
     * 
     */
    public void deleteCdMunicipio()
    {
        this._has_cdMunicipio= false;
    } //-- void deleteCdMunicipio() 

    /**
     * Method deleteCdMunicipioFeriadoLocal
     * 
     */
    public void deleteCdMunicipioFeriadoLocal()
    {
        this._has_cdMunicipioFeriadoLocal= false;
    } //-- void deleteCdMunicipioFeriadoLocal() 

    /**
     * Method deleteCdSituacaoVinculacaoConta
     * 
     */
    public void deleteCdSituacaoVinculacaoConta()
    {
        this._has_cdSituacaoVinculacaoConta= false;
    } //-- void deleteCdSituacaoVinculacaoConta() 

    /**
     * Method deleteCdTipoCanalInclusao
     * 
     */
    public void deleteCdTipoCanalInclusao()
    {
        this._has_cdTipoCanalInclusao= false;
    } //-- void deleteCdTipoCanalInclusao() 

    /**
     * Method deleteCdTipoCanalManutencao
     * 
     */
    public void deleteCdTipoCanalManutencao()
    {
        this._has_cdTipoCanalManutencao= false;
    } //-- void deleteCdTipoCanalManutencao() 

    /**
     * Method deleteCdUf
     * 
     */
    public void deleteCdUf()
    {
        this._has_cdUf= false;
    } //-- void deleteCdUf() 

    /**
     * Method deleteNumeroOcorrencias
     * 
     */
    public void deleteNumeroOcorrencias()
    {
        this._has_numeroOcorrencias= false;
    } //-- void deleteNumeroOcorrencias() 

    /**
     * Method enumerateOcorrencias
     * 
     * 
     * 
     * @return Enumeration
     */
    public java.util.Enumeration enumerateOcorrencias()
    {
        return _ocorrenciasList.elements();
    } //-- java.util.Enumeration enumerateOcorrencias() 

    /**
     * Returns the value of field 'cdMotivoSituacaoConta'.
     * 
     * @return int
     * @return the value of field 'cdMotivoSituacaoConta'.
     */
    public int getCdMotivoSituacaoConta()
    {
        return this._cdMotivoSituacaoConta;
    } //-- int getCdMotivoSituacaoConta() 

    /**
     * Returns the value of field 'cdMunicipio'.
     * 
     * @return long
     * @return the value of field 'cdMunicipio'.
     */
    public long getCdMunicipio()
    {
        return this._cdMunicipio;
    } //-- long getCdMunicipio() 

    /**
     * Returns the value of field 'cdMunicipioFeriadoLocal'.
     * 
     * @return int
     * @return the value of field 'cdMunicipioFeriadoLocal'.
     */
    public int getCdMunicipioFeriadoLocal()
    {
        return this._cdMunicipioFeriadoLocal;
    } //-- int getCdMunicipioFeriadoLocal() 

    /**
     * Returns the value of field 'cdOperacaoCanalInclusao'.
     * 
     * @return String
     * @return the value of field 'cdOperacaoCanalInclusao'.
     */
    public java.lang.String getCdOperacaoCanalInclusao()
    {
        return this._cdOperacaoCanalInclusao;
    } //-- java.lang.String getCdOperacaoCanalInclusao() 

    /**
     * Returns the value of field 'cdOperacaoCanalManutencao'.
     * 
     * @return String
     * @return the value of field 'cdOperacaoCanalManutencao'.
     */
    public java.lang.String getCdOperacaoCanalManutencao()
    {
        return this._cdOperacaoCanalManutencao;
    } //-- java.lang.String getCdOperacaoCanalManutencao() 

    /**
     * Returns the value of field 'cdSiglaUf'.
     * 
     * @return String
     * @return the value of field 'cdSiglaUf'.
     */
    public java.lang.String getCdSiglaUf()
    {
        return this._cdSiglaUf;
    } //-- java.lang.String getCdSiglaUf() 

    /**
     * Returns the value of field 'cdSituacaoVinculacaoConta'.
     * 
     * @return int
     * @return the value of field 'cdSituacaoVinculacaoConta'.
     */
    public int getCdSituacaoVinculacaoConta()
    {
        return this._cdSituacaoVinculacaoConta;
    } //-- int getCdSituacaoVinculacaoConta() 

    /**
     * Returns the value of field 'cdTipoCanalInclusao'.
     * 
     * @return int
     * @return the value of field 'cdTipoCanalInclusao'.
     */
    public int getCdTipoCanalInclusao()
    {
        return this._cdTipoCanalInclusao;
    } //-- int getCdTipoCanalInclusao() 

    /**
     * Returns the value of field 'cdTipoCanalManutencao'.
     * 
     * @return int
     * @return the value of field 'cdTipoCanalManutencao'.
     */
    public int getCdTipoCanalManutencao()
    {
        return this._cdTipoCanalManutencao;
    } //-- int getCdTipoCanalManutencao() 

    /**
     * Returns the value of field 'cdUf'.
     * 
     * @return int
     * @return the value of field 'cdUf'.
     */
    public int getCdUf()
    {
        return this._cdUf;
    } //-- int getCdUf() 

    /**
     * Returns the value of field 'cdUsuarioInclusao'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioInclusao'.
     */
    public java.lang.String getCdUsuarioInclusao()
    {
        return this._cdUsuarioInclusao;
    } //-- java.lang.String getCdUsuarioInclusao() 

    /**
     * Returns the value of field 'cdUsuarioInclusaoExterno'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioInclusaoExterno'.
     */
    public java.lang.String getCdUsuarioInclusaoExterno()
    {
        return this._cdUsuarioInclusaoExterno;
    } //-- java.lang.String getCdUsuarioInclusaoExterno() 

    /**
     * Returns the value of field 'cdUsuarioManutencao'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioManutencao'.
     */
    public java.lang.String getCdUsuarioManutencao()
    {
        return this._cdUsuarioManutencao;
    } //-- java.lang.String getCdUsuarioManutencao() 

    /**
     * Returns the value of field 'cdUsuarioManutencaoExterno'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioManutencaoExterno'.
     */
    public java.lang.String getCdUsuarioManutencaoExterno()
    {
        return this._cdUsuarioManutencaoExterno;
    } //-- java.lang.String getCdUsuarioManutencaoExterno() 

    /**
     * Returns the value of field 'codMensagem'.
     * 
     * @return String
     * @return the value of field 'codMensagem'.
     */
    public java.lang.String getCodMensagem()
    {
        return this._codMensagem;
    } //-- java.lang.String getCodMensagem() 

    /**
     * Returns the value of field 'descApelido'.
     * 
     * @return String
     * @return the value of field 'descApelido'.
     */
    public java.lang.String getDescApelido()
    {
        return this._descApelido;
    } //-- java.lang.String getDescApelido() 

    /**
     * Returns the value of field 'dsMotivoSituacaoConta'.
     * 
     * @return String
     * @return the value of field 'dsMotivoSituacaoConta'.
     */
    public java.lang.String getDsMotivoSituacaoConta()
    {
        return this._dsMotivoSituacaoConta;
    } //-- java.lang.String getDsMotivoSituacaoConta() 

    /**
     * Returns the value of field 'dsMunicipio'.
     * 
     * @return String
     * @return the value of field 'dsMunicipio'.
     */
    public java.lang.String getDsMunicipio()
    {
        return this._dsMunicipio;
    } //-- java.lang.String getDsMunicipio() 

    /**
     * Returns the value of field 'dsSituacaoVinculacaoConta'.
     * 
     * @return String
     * @return the value of field 'dsSituacaoVinculacaoConta'.
     */
    public java.lang.String getDsSituacaoVinculacaoConta()
    {
        return this._dsSituacaoVinculacaoConta;
    } //-- java.lang.String getDsSituacaoVinculacaoConta() 

    /**
     * Returns the value of field 'dsTipoCanalInclusao'.
     * 
     * @return String
     * @return the value of field 'dsTipoCanalInclusao'.
     */
    public java.lang.String getDsTipoCanalInclusao()
    {
        return this._dsTipoCanalInclusao;
    } //-- java.lang.String getDsTipoCanalInclusao() 

    /**
     * Returns the value of field 'dsTipoCanalManutencao'.
     * 
     * @return String
     * @return the value of field 'dsTipoCanalManutencao'.
     */
    public java.lang.String getDsTipoCanalManutencao()
    {
        return this._dsTipoCanalManutencao;
    } //-- java.lang.String getDsTipoCanalManutencao() 

    /**
     * Returns the value of field 'dsUf'.
     * 
     * @return String
     * @return the value of field 'dsUf'.
     */
    public java.lang.String getDsUf()
    {
        return this._dsUf;
    } //-- java.lang.String getDsUf() 

    /**
     * Returns the value of field 'dtFimValorAdicional'.
     * 
     * @return String
     * @return the value of field 'dtFimValorAdicional'.
     */
    public java.lang.String getDtFimValorAdicional()
    {
        return this._dtFimValorAdicional;
    } //-- java.lang.String getDtFimValorAdicional() 

    /**
     * Returns the value of field 'dtFimVlrAdicionalTed'.
     * 
     * @return String
     * @return the value of field 'dtFimVlrAdicionalTed'.
     */
    public java.lang.String getDtFimVlrAdicionalTed()
    {
        return this._dtFimVlrAdicionalTed;
    } //-- java.lang.String getDtFimVlrAdicionalTed() 

    /**
     * Returns the value of field 'dtIniVlrAdicionalTed'.
     * 
     * @return String
     * @return the value of field 'dtIniVlrAdicionalTed'.
     */
    public java.lang.String getDtIniVlrAdicionalTed()
    {
        return this._dtIniVlrAdicionalTed;
    } //-- java.lang.String getDtIniVlrAdicionalTed() 

    /**
     * Returns the value of field 'dtInicioValorAdicional'.
     * 
     * @return String
     * @return the value of field 'dtInicioValorAdicional'.
     */
    public java.lang.String getDtInicioValorAdicional()
    {
        return this._dtInicioValorAdicional;
    } //-- java.lang.String getDtInicioValorAdicional() 

    /**
     * Returns the value of field 'hrInclusaoRegistro'.
     * 
     * @return String
     * @return the value of field 'hrInclusaoRegistro'.
     */
    public java.lang.String getHrInclusaoRegistro()
    {
        return this._hrInclusaoRegistro;
    } //-- java.lang.String getHrInclusaoRegistro() 

    /**
     * Returns the value of field 'hrManutencaoRegistro'.
     * 
     * @return String
     * @return the value of field 'hrManutencaoRegistro'.
     */
    public java.lang.String getHrManutencaoRegistro()
    {
        return this._hrManutencaoRegistro;
    } //-- java.lang.String getHrManutencaoRegistro() 

    /**
     * Returns the value of field 'mensagem'.
     * 
     * @return String
     * @return the value of field 'mensagem'.
     */
    public java.lang.String getMensagem()
    {
        return this._mensagem;
    } //-- java.lang.String getMensagem() 

    /**
     * Returns the value of field 'numeroOcorrencias'.
     * 
     * @return int
     * @return the value of field 'numeroOcorrencias'.
     */
    public int getNumeroOcorrencias()
    {
        return this._numeroOcorrencias;
    } //-- int getNumeroOcorrencias() 

    /**
     * Method getOcorrencias
     * 
     * 
     * 
     * @param index
     * @return Ocorrencias
     */
    public br.com.bradesco.web.pgit.service.data.pdc.consultarcontacontrato.response.Ocorrencias getOcorrencias(int index)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index >= _ocorrenciasList.size())) {
            throw new IndexOutOfBoundsException("getOcorrencias: Index value '"+index+"' not in range [0.."+(_ocorrenciasList.size() - 1) + "]");
        }
        
        return (br.com.bradesco.web.pgit.service.data.pdc.consultarcontacontrato.response.Ocorrencias) _ocorrenciasList.elementAt(index);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarcontacontrato.response.Ocorrencias getOcorrencias(int) 

    /**
     * Method getOcorrencias
     * 
     * 
     * 
     * @return Ocorrencias
     */
    public br.com.bradesco.web.pgit.service.data.pdc.consultarcontacontrato.response.Ocorrencias[] getOcorrencias()
    {
        int size = _ocorrenciasList.size();
        br.com.bradesco.web.pgit.service.data.pdc.consultarcontacontrato.response.Ocorrencias[] mArray = new br.com.bradesco.web.pgit.service.data.pdc.consultarcontacontrato.response.Ocorrencias[size];
        for (int index = 0; index < size; index++) {
            mArray[index] = (br.com.bradesco.web.pgit.service.data.pdc.consultarcontacontrato.response.Ocorrencias) _ocorrenciasList.elementAt(index);
        }
        return mArray;
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarcontacontrato.response.Ocorrencias[] getOcorrencias() 

    /**
     * Method getOcorrenciasCount
     * 
     * 
     * 
     * @return int
     */
    public int getOcorrenciasCount()
    {
        return _ocorrenciasList.size();
    } //-- int getOcorrenciasCount() 

    /**
     * Returns the value of field 'vlAdicContrCtaTed'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlAdicContrCtaTed'.
     */
    public java.math.BigDecimal getVlAdicContrCtaTed()
    {
        return this._vlAdicContrCtaTed;
    } //-- java.math.BigDecimal getVlAdicContrCtaTed() 

    /**
     * Returns the value of field 'vlAdicionalContratoConta'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlAdicionalContratoConta'.
     */
    public java.math.BigDecimal getVlAdicionalContratoConta()
    {
        return this._vlAdicionalContratoConta;
    } //-- java.math.BigDecimal getVlAdicionalContratoConta() 

    /**
     * Method hasCdMotivoSituacaoConta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMotivoSituacaoConta()
    {
        return this._has_cdMotivoSituacaoConta;
    } //-- boolean hasCdMotivoSituacaoConta() 

    /**
     * Method hasCdMunicipio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMunicipio()
    {
        return this._has_cdMunicipio;
    } //-- boolean hasCdMunicipio() 

    /**
     * Method hasCdMunicipioFeriadoLocal
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMunicipioFeriadoLocal()
    {
        return this._has_cdMunicipioFeriadoLocal;
    } //-- boolean hasCdMunicipioFeriadoLocal() 

    /**
     * Method hasCdSituacaoVinculacaoConta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSituacaoVinculacaoConta()
    {
        return this._has_cdSituacaoVinculacaoConta;
    } //-- boolean hasCdSituacaoVinculacaoConta() 

    /**
     * Method hasCdTipoCanalInclusao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoCanalInclusao()
    {
        return this._has_cdTipoCanalInclusao;
    } //-- boolean hasCdTipoCanalInclusao() 

    /**
     * Method hasCdTipoCanalManutencao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoCanalManutencao()
    {
        return this._has_cdTipoCanalManutencao;
    } //-- boolean hasCdTipoCanalManutencao() 

    /**
     * Method hasCdUf
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdUf()
    {
        return this._has_cdUf;
    } //-- boolean hasCdUf() 

    /**
     * Method hasNumeroOcorrencias
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNumeroOcorrencias()
    {
        return this._has_numeroOcorrencias;
    } //-- boolean hasNumeroOcorrencias() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Method removeAllOcorrencias
     * 
     */
    public void removeAllOcorrencias()
    {
        _ocorrenciasList.removeAllElements();
    } //-- void removeAllOcorrencias() 

    /**
     * Method removeOcorrencias
     * 
     * 
     * 
     * @param index
     * @return Ocorrencias
     */
    public br.com.bradesco.web.pgit.service.data.pdc.consultarcontacontrato.response.Ocorrencias removeOcorrencias(int index)
    {
        java.lang.Object obj = _ocorrenciasList.elementAt(index);
        _ocorrenciasList.removeElementAt(index);
        return (br.com.bradesco.web.pgit.service.data.pdc.consultarcontacontrato.response.Ocorrencias) obj;
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarcontacontrato.response.Ocorrencias removeOcorrencias(int) 

    /**
     * Sets the value of field 'cdMotivoSituacaoConta'.
     * 
     * @param cdMotivoSituacaoConta the value of field
     * 'cdMotivoSituacaoConta'.
     */
    public void setCdMotivoSituacaoConta(int cdMotivoSituacaoConta)
    {
        this._cdMotivoSituacaoConta = cdMotivoSituacaoConta;
        this._has_cdMotivoSituacaoConta = true;
    } //-- void setCdMotivoSituacaoConta(int) 

    /**
     * Sets the value of field 'cdMunicipio'.
     * 
     * @param cdMunicipio the value of field 'cdMunicipio'.
     */
    public void setCdMunicipio(long cdMunicipio)
    {
        this._cdMunicipio = cdMunicipio;
        this._has_cdMunicipio = true;
    } //-- void setCdMunicipio(long) 

    /**
     * Sets the value of field 'cdMunicipioFeriadoLocal'.
     * 
     * @param cdMunicipioFeriadoLocal the value of field
     * 'cdMunicipioFeriadoLocal'.
     */
    public void setCdMunicipioFeriadoLocal(int cdMunicipioFeriadoLocal)
    {
        this._cdMunicipioFeriadoLocal = cdMunicipioFeriadoLocal;
        this._has_cdMunicipioFeriadoLocal = true;
    } //-- void setCdMunicipioFeriadoLocal(int) 

    /**
     * Sets the value of field 'cdOperacaoCanalInclusao'.
     * 
     * @param cdOperacaoCanalInclusao the value of field
     * 'cdOperacaoCanalInclusao'.
     */
    public void setCdOperacaoCanalInclusao(java.lang.String cdOperacaoCanalInclusao)
    {
        this._cdOperacaoCanalInclusao = cdOperacaoCanalInclusao;
    } //-- void setCdOperacaoCanalInclusao(java.lang.String) 

    /**
     * Sets the value of field 'cdOperacaoCanalManutencao'.
     * 
     * @param cdOperacaoCanalManutencao the value of field
     * 'cdOperacaoCanalManutencao'.
     */
    public void setCdOperacaoCanalManutencao(java.lang.String cdOperacaoCanalManutencao)
    {
        this._cdOperacaoCanalManutencao = cdOperacaoCanalManutencao;
    } //-- void setCdOperacaoCanalManutencao(java.lang.String) 

    /**
     * Sets the value of field 'cdSiglaUf'.
     * 
     * @param cdSiglaUf the value of field 'cdSiglaUf'.
     */
    public void setCdSiglaUf(java.lang.String cdSiglaUf)
    {
        this._cdSiglaUf = cdSiglaUf;
    } //-- void setCdSiglaUf(java.lang.String) 

    /**
     * Sets the value of field 'cdSituacaoVinculacaoConta'.
     * 
     * @param cdSituacaoVinculacaoConta the value of field
     * 'cdSituacaoVinculacaoConta'.
     */
    public void setCdSituacaoVinculacaoConta(int cdSituacaoVinculacaoConta)
    {
        this._cdSituacaoVinculacaoConta = cdSituacaoVinculacaoConta;
        this._has_cdSituacaoVinculacaoConta = true;
    } //-- void setCdSituacaoVinculacaoConta(int) 

    /**
     * Sets the value of field 'cdTipoCanalInclusao'.
     * 
     * @param cdTipoCanalInclusao the value of field
     * 'cdTipoCanalInclusao'.
     */
    public void setCdTipoCanalInclusao(int cdTipoCanalInclusao)
    {
        this._cdTipoCanalInclusao = cdTipoCanalInclusao;
        this._has_cdTipoCanalInclusao = true;
    } //-- void setCdTipoCanalInclusao(int) 

    /**
     * Sets the value of field 'cdTipoCanalManutencao'.
     * 
     * @param cdTipoCanalManutencao the value of field
     * 'cdTipoCanalManutencao'.
     */
    public void setCdTipoCanalManutencao(int cdTipoCanalManutencao)
    {
        this._cdTipoCanalManutencao = cdTipoCanalManutencao;
        this._has_cdTipoCanalManutencao = true;
    } //-- void setCdTipoCanalManutencao(int) 

    /**
     * Sets the value of field 'cdUf'.
     * 
     * @param cdUf the value of field 'cdUf'.
     */
    public void setCdUf(int cdUf)
    {
        this._cdUf = cdUf;
        this._has_cdUf = true;
    } //-- void setCdUf(int) 

    /**
     * Sets the value of field 'cdUsuarioInclusao'.
     * 
     * @param cdUsuarioInclusao the value of field
     * 'cdUsuarioInclusao'.
     */
    public void setCdUsuarioInclusao(java.lang.String cdUsuarioInclusao)
    {
        this._cdUsuarioInclusao = cdUsuarioInclusao;
    } //-- void setCdUsuarioInclusao(java.lang.String) 

    /**
     * Sets the value of field 'cdUsuarioInclusaoExterno'.
     * 
     * @param cdUsuarioInclusaoExterno the value of field
     * 'cdUsuarioInclusaoExterno'.
     */
    public void setCdUsuarioInclusaoExterno(java.lang.String cdUsuarioInclusaoExterno)
    {
        this._cdUsuarioInclusaoExterno = cdUsuarioInclusaoExterno;
    } //-- void setCdUsuarioInclusaoExterno(java.lang.String) 

    /**
     * Sets the value of field 'cdUsuarioManutencao'.
     * 
     * @param cdUsuarioManutencao the value of field
     * 'cdUsuarioManutencao'.
     */
    public void setCdUsuarioManutencao(java.lang.String cdUsuarioManutencao)
    {
        this._cdUsuarioManutencao = cdUsuarioManutencao;
    } //-- void setCdUsuarioManutencao(java.lang.String) 

    /**
     * Sets the value of field 'cdUsuarioManutencaoExterno'.
     * 
     * @param cdUsuarioManutencaoExterno the value of field
     * 'cdUsuarioManutencaoExterno'.
     */
    public void setCdUsuarioManutencaoExterno(java.lang.String cdUsuarioManutencaoExterno)
    {
        this._cdUsuarioManutencaoExterno = cdUsuarioManutencaoExterno;
    } //-- void setCdUsuarioManutencaoExterno(java.lang.String) 

    /**
     * Sets the value of field 'codMensagem'.
     * 
     * @param codMensagem the value of field 'codMensagem'.
     */
    public void setCodMensagem(java.lang.String codMensagem)
    {
        this._codMensagem = codMensagem;
    } //-- void setCodMensagem(java.lang.String) 

    /**
     * Sets the value of field 'descApelido'.
     * 
     * @param descApelido the value of field 'descApelido'.
     */
    public void setDescApelido(java.lang.String descApelido)
    {
        this._descApelido = descApelido;
    } //-- void setDescApelido(java.lang.String) 

    /**
     * Sets the value of field 'dsMotivoSituacaoConta'.
     * 
     * @param dsMotivoSituacaoConta the value of field
     * 'dsMotivoSituacaoConta'.
     */
    public void setDsMotivoSituacaoConta(java.lang.String dsMotivoSituacaoConta)
    {
        this._dsMotivoSituacaoConta = dsMotivoSituacaoConta;
    } //-- void setDsMotivoSituacaoConta(java.lang.String) 

    /**
     * Sets the value of field 'dsMunicipio'.
     * 
     * @param dsMunicipio the value of field 'dsMunicipio'.
     */
    public void setDsMunicipio(java.lang.String dsMunicipio)
    {
        this._dsMunicipio = dsMunicipio;
    } //-- void setDsMunicipio(java.lang.String) 

    /**
     * Sets the value of field 'dsSituacaoVinculacaoConta'.
     * 
     * @param dsSituacaoVinculacaoConta the value of field
     * 'dsSituacaoVinculacaoConta'.
     */
    public void setDsSituacaoVinculacaoConta(java.lang.String dsSituacaoVinculacaoConta)
    {
        this._dsSituacaoVinculacaoConta = dsSituacaoVinculacaoConta;
    } //-- void setDsSituacaoVinculacaoConta(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoCanalInclusao'.
     * 
     * @param dsTipoCanalInclusao the value of field
     * 'dsTipoCanalInclusao'.
     */
    public void setDsTipoCanalInclusao(java.lang.String dsTipoCanalInclusao)
    {
        this._dsTipoCanalInclusao = dsTipoCanalInclusao;
    } //-- void setDsTipoCanalInclusao(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoCanalManutencao'.
     * 
     * @param dsTipoCanalManutencao the value of field
     * 'dsTipoCanalManutencao'.
     */
    public void setDsTipoCanalManutencao(java.lang.String dsTipoCanalManutencao)
    {
        this._dsTipoCanalManutencao = dsTipoCanalManutencao;
    } //-- void setDsTipoCanalManutencao(java.lang.String) 

    /**
     * Sets the value of field 'dsUf'.
     * 
     * @param dsUf the value of field 'dsUf'.
     */
    public void setDsUf(java.lang.String dsUf)
    {
        this._dsUf = dsUf;
    } //-- void setDsUf(java.lang.String) 

    /**
     * Sets the value of field 'dtFimValorAdicional'.
     * 
     * @param dtFimValorAdicional the value of field
     * 'dtFimValorAdicional'.
     */
    public void setDtFimValorAdicional(java.lang.String dtFimValorAdicional)
    {
        this._dtFimValorAdicional = dtFimValorAdicional;
    } //-- void setDtFimValorAdicional(java.lang.String) 

    /**
     * Sets the value of field 'dtFimVlrAdicionalTed'.
     * 
     * @param dtFimVlrAdicionalTed the value of field
     * 'dtFimVlrAdicionalTed'.
     */
    public void setDtFimVlrAdicionalTed(java.lang.String dtFimVlrAdicionalTed)
    {
        this._dtFimVlrAdicionalTed = dtFimVlrAdicionalTed;
    } //-- void setDtFimVlrAdicionalTed(java.lang.String) 

    /**
     * Sets the value of field 'dtIniVlrAdicionalTed'.
     * 
     * @param dtIniVlrAdicionalTed the value of field
     * 'dtIniVlrAdicionalTed'.
     */
    public void setDtIniVlrAdicionalTed(java.lang.String dtIniVlrAdicionalTed)
    {
        this._dtIniVlrAdicionalTed = dtIniVlrAdicionalTed;
    } //-- void setDtIniVlrAdicionalTed(java.lang.String) 

    /**
     * Sets the value of field 'dtInicioValorAdicional'.
     * 
     * @param dtInicioValorAdicional the value of field
     * 'dtInicioValorAdicional'.
     */
    public void setDtInicioValorAdicional(java.lang.String dtInicioValorAdicional)
    {
        this._dtInicioValorAdicional = dtInicioValorAdicional;
    } //-- void setDtInicioValorAdicional(java.lang.String) 

    /**
     * Sets the value of field 'hrInclusaoRegistro'.
     * 
     * @param hrInclusaoRegistro the value of field
     * 'hrInclusaoRegistro'.
     */
    public void setHrInclusaoRegistro(java.lang.String hrInclusaoRegistro)
    {
        this._hrInclusaoRegistro = hrInclusaoRegistro;
    } //-- void setHrInclusaoRegistro(java.lang.String) 

    /**
     * Sets the value of field 'hrManutencaoRegistro'.
     * 
     * @param hrManutencaoRegistro the value of field
     * 'hrManutencaoRegistro'.
     */
    public void setHrManutencaoRegistro(java.lang.String hrManutencaoRegistro)
    {
        this._hrManutencaoRegistro = hrManutencaoRegistro;
    } //-- void setHrManutencaoRegistro(java.lang.String) 

    /**
     * Sets the value of field 'mensagem'.
     * 
     * @param mensagem the value of field 'mensagem'.
     */
    public void setMensagem(java.lang.String mensagem)
    {
        this._mensagem = mensagem;
    } //-- void setMensagem(java.lang.String) 

    /**
     * Sets the value of field 'numeroOcorrencias'.
     * 
     * @param numeroOcorrencias the value of field
     * 'numeroOcorrencias'.
     */
    public void setNumeroOcorrencias(int numeroOcorrencias)
    {
        this._numeroOcorrencias = numeroOcorrencias;
        this._has_numeroOcorrencias = true;
    } //-- void setNumeroOcorrencias(int) 

    /**
     * Method setOcorrencias
     * 
     * 
     * 
     * @param index
     * @param vOcorrencias
     */
    public void setOcorrencias(int index, br.com.bradesco.web.pgit.service.data.pdc.consultarcontacontrato.response.Ocorrencias vOcorrencias)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index >= _ocorrenciasList.size())) {
            throw new IndexOutOfBoundsException("setOcorrencias: Index value '"+index+"' not in range [0.." + (_ocorrenciasList.size() - 1) + "]");
        }
        _ocorrenciasList.setElementAt(vOcorrencias, index);
    } //-- void setOcorrencias(int, br.com.bradesco.web.pgit.service.data.pdc.consultarcontacontrato.response.Ocorrencias) 

    /**
     * Method setOcorrencias
     * 
     * 
     * 
     * @param ocorrenciasArray
     */
    public void setOcorrencias(br.com.bradesco.web.pgit.service.data.pdc.consultarcontacontrato.response.Ocorrencias[] ocorrenciasArray)
    {
        //-- copy array
        _ocorrenciasList.removeAllElements();
        for (int i = 0; i < ocorrenciasArray.length; i++) {
            _ocorrenciasList.addElement(ocorrenciasArray[i]);
        }
    } //-- void setOcorrencias(br.com.bradesco.web.pgit.service.data.pdc.consultarcontacontrato.response.Ocorrencias) 

    /**
     * Sets the value of field 'vlAdicContrCtaTed'.
     * 
     * @param vlAdicContrCtaTed the value of field
     * 'vlAdicContrCtaTed'.
     */
    public void setVlAdicContrCtaTed(java.math.BigDecimal vlAdicContrCtaTed)
    {
        this._vlAdicContrCtaTed = vlAdicContrCtaTed;
    } //-- void setVlAdicContrCtaTed(java.math.BigDecimal) 

    /**
     * Sets the value of field 'vlAdicionalContratoConta'.
     * 
     * @param vlAdicionalContratoConta the value of field
     * 'vlAdicionalContratoConta'.
     */
    public void setVlAdicionalContratoConta(java.math.BigDecimal vlAdicionalContratoConta)
    {
        this._vlAdicionalContratoConta = vlAdicionalContratoConta;
    } //-- void setVlAdicionalContratoConta(java.math.BigDecimal) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ConsultarContaContratoResponse
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultarcontacontrato.response.ConsultarContaContratoResponse unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultarcontacontrato.response.ConsultarContaContratoResponse) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultarcontacontrato.response.ConsultarContaContratoResponse.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarcontacontrato.response.ConsultarContaContratoResponse unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
