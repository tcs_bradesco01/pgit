/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantercontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantercontrato.bean;

import java.math.BigDecimal;

/**
 * Nome: AlterarTarifaAcimaPadraoEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class AlterarTarifaAcimaPadraoEntradaDTO {
	
	/** Atributo cdpessoaJuridicaContrato. */
	private Long cdpessoaJuridicaContrato;
	
	/** Atributo cdTipoContratoNegocio. */
	private Integer cdTipoContratoNegocio;
	
	/** Atributo nrSequenciaContratoNegocio. */
	private Long nrSequenciaContratoNegocio;
	
	/** Atributo cdProduto. */
	private Integer cdProduto;
	
	/** Atributo cdProdutoServicoRelacionado. */
	private Integer cdProdutoServicoRelacionado;
	
	/** Atributo cdRelacionamentoProduto. */
	private Integer cdRelacionamentoProduto;
	
	/** Atributo cdOperacao. */
	private Integer cdOperacao;
	
	/** Atributo vlrTarifaContratoNegocio. */
	private BigDecimal vlrTarifaContratoNegocio;
	
	/**
	 * Set: vlrTarifaContratoNegocio.
	 *
	 * @param vlrTarifaContratoNegocio the vlr tarifa contrato negocio
	 */
	public void setVlrTarifaContratoNegocio(BigDecimal vlrTarifaContratoNegocio) {
		this.vlrTarifaContratoNegocio = vlrTarifaContratoNegocio;
	}
	
	/**
	 * Get: vlrTarifaContratoNegocio.
	 *
	 * @return vlrTarifaContratoNegocio
	 */
	public BigDecimal getVlrTarifaContratoNegocio() {
		return vlrTarifaContratoNegocio;
	}
	
	/**
	 * Get: cdpessoaJuridicaContrato.
	 *
	 * @return cdpessoaJuridicaContrato
	 */
	public Long getCdpessoaJuridicaContrato() {
		return cdpessoaJuridicaContrato;
	}
	
	/**
	 * Set: cdpessoaJuridicaContrato.
	 *
	 * @param cdpessoaJuridicaContrato the cdpessoa juridica contrato
	 */
	public void setCdpessoaJuridicaContrato(Long cdpessoaJuridicaContrato) {
		this.cdpessoaJuridicaContrato = cdpessoaJuridicaContrato;
	}
	
	/**
	 * Get: cdTipoContratoNegocio.
	 *
	 * @return cdTipoContratoNegocio
	 */
	public Integer getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}
	
	/**
	 * Set: cdTipoContratoNegocio.
	 *
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}
	
	/**
	 * Get: nrSequenciaContratoNegocio.
	 *
	 * @return nrSequenciaContratoNegocio
	 */
	public Long getNrSequenciaContratoNegocio() {
		return nrSequenciaContratoNegocio;
	}
	
	/**
	 * Set: nrSequenciaContratoNegocio.
	 *
	 * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
	 */
	public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}
	
	/**
	 * Get: cdProduto.
	 *
	 * @return cdProduto
	 */
	public Integer getCdProduto() {
		return cdProduto;
	}
	
	/**
	 * Set: cdProduto.
	 *
	 * @param cdProduto the cd produto
	 */
	public void setCdProduto(Integer cdProduto) {
		this.cdProduto = cdProduto;
	}
	
	/**
	 * Get: cdProdutoServicoRelacionado.
	 *
	 * @return cdProdutoServicoRelacionado
	 */
	public Integer getCdProdutoServicoRelacionado() {
		return cdProdutoServicoRelacionado;
	}
	
	/**
	 * Set: cdProdutoServicoRelacionado.
	 *
	 * @param cdProdutoServicoRelacionado the cd produto servico relacionado
	 */
	public void setCdProdutoServicoRelacionado(Integer cdProdutoServicoRelacionado) {
		this.cdProdutoServicoRelacionado = cdProdutoServicoRelacionado;
	}
	
	/**
	 * Get: cdRelacionamentoProduto.
	 *
	 * @return cdRelacionamentoProduto
	 */
	public Integer getCdRelacionamentoProduto() {
		return cdRelacionamentoProduto;
	}
	
	/**
	 * Set: cdRelacionamentoProduto.
	 *
	 * @param cdRelacionamentoProduto the cd relacionamento produto
	 */
	public void setCdRelacionamentoProduto(Integer cdRelacionamentoProduto) {
		this.cdRelacionamentoProduto = cdRelacionamentoProduto;
	}
	
	/**
	 * Get: cdOperacao.
	 *
	 * @return cdOperacao
	 */
	public Integer getCdOperacao() {
		return cdOperacao;
	}
	
	/**
	 * Set: cdOperacao.
	 *
	 * @param cdOperacao the cd operacao
	 */
	public void setCdOperacao(Integer cdOperacao) {
		this.cdOperacao = cdOperacao;
	}
	
}
