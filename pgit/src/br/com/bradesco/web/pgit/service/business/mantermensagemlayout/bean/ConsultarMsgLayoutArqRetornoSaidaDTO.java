/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantermensagemlayout.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantermensagemlayout.bean;

/**
 * Nome: ConsultarMsgLayoutArqRetornoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarMsgLayoutArqRetornoSaidaDTO {
	
	/** Atributo codMensagem. */
	private String codMensagem;

	/** Atributo mensagem. */
	private String mensagem;

	/** Atributo numeroLinhas. */
	private Integer numeroLinhas;

	/** Atributo cdTipoLayoutArquivo. */
	private Integer cdTipoLayoutArquivo;

	/** Atributo dsTipoLayoutArquivo. */
	private String dsTipoLayoutArquivo;

	/** Atributo nrMensagemArquivoRetorno. */
	private Integer nrMensagemArquivoRetorno;

	/** Atributo cdMensagemArquivoRetorno. */
	private String cdMensagemArquivoRetorno;

	/** Atributo cdTipoMensagemRetorno. */
	private Integer cdTipoMensagemRetorno;

	/** Atributo dsMensagemLayoutRetorno. */
	private String dsMensagemLayoutRetorno;

	/** Atributo dsTipoMansagemRetorno. */
	private String dsTipoMansagemRetorno;

	/** Atributo tipoLayoutArquivoFormatado. */
	private String tipoLayoutArquivoFormatado;

	/**
	 * Get: dsTipoMansagemRetorno.
	 *
	 * @return dsTipoMansagemRetorno
	 */
	public String getDsTipoMansagemRetorno() {
		return dsTipoMansagemRetorno;
	}

	/**
	 * Set: dsTipoMansagemRetorno.
	 *
	 * @param dsTipoMansagemRetorno the ds tipo mansagem retorno
	 */
	public void setDsTipoMansagemRetorno(String dsTipoMansagemRetorno) {
		this.dsTipoMansagemRetorno = dsTipoMansagemRetorno;
	}

	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}

	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}

	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}

	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	/**
	 * Get: numeroLinhas.
	 *
	 * @return numeroLinhas
	 */
	public Integer getNumeroLinhas() {
		return numeroLinhas;
	}

	/**
	 * Set: numeroLinhas.
	 *
	 * @param numeroLinhas the numero linhas
	 */
	public void setNumeroLinhas(Integer numeroLinhas) {
		this.numeroLinhas = numeroLinhas;
	}

	/**
	 * Get: cdTipoLayoutArquivo.
	 *
	 * @return cdTipoLayoutArquivo
	 */
	public Integer getCdTipoLayoutArquivo() {
		return cdTipoLayoutArquivo;
	}

	/**
	 * Set: cdTipoLayoutArquivo.
	 *
	 * @param cdTipoLayoutArquivo the cd tipo layout arquivo
	 */
	public void setCdTipoLayoutArquivo(Integer cdTipoLayoutArquivo) {
		this.cdTipoLayoutArquivo = cdTipoLayoutArquivo;
	}

	/**
	 * Get: nrMensagemArquivoRetorno.
	 *
	 * @return nrMensagemArquivoRetorno
	 */
	public Integer getNrMensagemArquivoRetorno() {
		return nrMensagemArquivoRetorno;
	}

	/**
	 * Set: nrMensagemArquivoRetorno.
	 *
	 * @param nrMensagemArquivoRetorno the nr mensagem arquivo retorno
	 */
	public void setNrMensagemArquivoRetorno(Integer nrMensagemArquivoRetorno) {
		this.nrMensagemArquivoRetorno = nrMensagemArquivoRetorno;
	}

	/**
	 * Get: cdMensagemArquivoRetorno.
	 *
	 * @return cdMensagemArquivoRetorno
	 */
	public String getCdMensagemArquivoRetorno() {
		return cdMensagemArquivoRetorno;
	}

	/**
	 * Set: cdMensagemArquivoRetorno.
	 *
	 * @param cdMensagemArquivoRetorno the cd mensagem arquivo retorno
	 */
	public void setCdMensagemArquivoRetorno(String cdMensagemArquivoRetorno) {
		this.cdMensagemArquivoRetorno = cdMensagemArquivoRetorno;
	}

	/**
	 * Get: cdTipoMensagemRetorno.
	 *
	 * @return cdTipoMensagemRetorno
	 */
	public Integer getCdTipoMensagemRetorno() {
		return cdTipoMensagemRetorno;
	}

	/**
	 * Set: cdTipoMensagemRetorno.
	 *
	 * @param cdTipoMensagemRetorno the cd tipo mensagem retorno
	 */
	public void setCdTipoMensagemRetorno(Integer cdTipoMensagemRetorno) {
		this.cdTipoMensagemRetorno = cdTipoMensagemRetorno;
	}

	/**
	 * Get: dsMensagemLayoutRetorno.
	 *
	 * @return dsMensagemLayoutRetorno
	 */
	public String getDsMensagemLayoutRetorno() {
		return dsMensagemLayoutRetorno;
	}

	/**
	 * Set: dsMensagemLayoutRetorno.
	 *
	 * @param dsMensagemLayoutRetorno the ds mensagem layout retorno
	 */
	public void setDsMensagemLayoutRetorno(String dsMensagemLayoutRetorno) {
		this.dsMensagemLayoutRetorno = dsMensagemLayoutRetorno;
	}

	/**
	 * Get: dsTipoLayoutArquivo.
	 *
	 * @return dsTipoLayoutArquivo
	 */
	public String getDsTipoLayoutArquivo() {
		return dsTipoLayoutArquivo;
	}

	/**
	 * Set: dsTipoLayoutArquivo.
	 *
	 * @param dsTipoLayoutArquivo the ds tipo layout arquivo
	 */
	public void setDsTipoLayoutArquivo(String dsTipoLayoutArquivo) {
		this.dsTipoLayoutArquivo = dsTipoLayoutArquivo;
	}

	/**
	 * Get: tipoLayoutArquivoFormatado.
	 *
	 * @return tipoLayoutArquivoFormatado
	 */
	public String getTipoLayoutArquivoFormatado() {
		return tipoLayoutArquivoFormatado;
	}

	/**
	 * Set: tipoLayoutArquivoFormatado.
	 *
	 * @param tipoLayoutArquivoFormatado the tipo layout arquivo formatado
	 */
	public void setTipoLayoutArquivoFormatado(String tipoLayoutArquivoFormatado) {
		this.tipoLayoutArquivoFormatado = tipoLayoutArquivoFormatado;
	}

}