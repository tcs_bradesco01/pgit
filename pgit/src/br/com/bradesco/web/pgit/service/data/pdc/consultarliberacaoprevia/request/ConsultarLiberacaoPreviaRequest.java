/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultarliberacaoprevia.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ConsultarLiberacaoPreviaRequest.
 * 
 * @version $Revision$ $Date$
 */
public class ConsultarLiberacaoPreviaRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _nrOcorrencias
     */
    private int _nrOcorrencias = 0;

    /**
     * keeps track of state for field: _nrOcorrencias
     */
    private boolean _has_nrOcorrencias;

    /**
     * Field _cdTipoPesquisa
     */
    private int _cdTipoPesquisa = 0;

    /**
     * keeps track of state for field: _cdTipoPesquisa
     */
    private boolean _has_cdTipoPesquisa;

    /**
     * Field _nmPessoaJuridica
     */
    private long _nmPessoaJuridica = 0;

    /**
     * keeps track of state for field: _nmPessoaJuridica
     */
    private boolean _has_nmPessoaJuridica;

    /**
     * Field _cdTipoContrato
     */
    private int _cdTipoContrato = 0;

    /**
     * keeps track of state for field: _cdTipoContrato
     */
    private boolean _has_cdTipoContrato;

    /**
     * Field _nrSequencialContrato
     */
    private long _nrSequencialContrato = 0;

    /**
     * keeps track of state for field: _nrSequencialContrato
     */
    private boolean _has_nrSequencialContrato;

    /**
     * Field _cdPessoaParticipacao
     */
    private long _cdPessoaParticipacao = 0;

    /**
     * keeps track of state for field: _cdPessoaParticipacao
     */
    private boolean _has_cdPessoaParticipacao;

    /**
     * Field _dtInicioLiberacao
     */
    private java.lang.String _dtInicioLiberacao;

    /**
     * Field _dtFinalLiberacao
     */
    private java.lang.String _dtFinalLiberacao;

    /**
     * Field _cdBanco
     */
    private int _cdBanco = 0;

    /**
     * keeps track of state for field: _cdBanco
     */
    private boolean _has_cdBanco;

    /**
     * Field _cdAgencia
     */
    private int _cdAgencia = 0;

    /**
     * keeps track of state for field: _cdAgencia
     */
    private boolean _has_cdAgencia;

    /**
     * Field _cdConta
     */
    private long _cdConta = 0;

    /**
     * keeps track of state for field: _cdConta
     */
    private boolean _has_cdConta;

    /**
     * Field _cdDigitoConta
     */
    private java.lang.String _cdDigitoConta;


      //----------------/
     //- Constructors -/
    //----------------/

    public ConsultarLiberacaoPreviaRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarliberacaoprevia.request.ConsultarLiberacaoPreviaRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdAgencia
     * 
     */
    public void deleteCdAgencia()
    {
        this._has_cdAgencia= false;
    } //-- void deleteCdAgencia() 

    /**
     * Method deleteCdBanco
     * 
     */
    public void deleteCdBanco()
    {
        this._has_cdBanco= false;
    } //-- void deleteCdBanco() 

    /**
     * Method deleteCdConta
     * 
     */
    public void deleteCdConta()
    {
        this._has_cdConta= false;
    } //-- void deleteCdConta() 

    /**
     * Method deleteCdPessoaParticipacao
     * 
     */
    public void deleteCdPessoaParticipacao()
    {
        this._has_cdPessoaParticipacao= false;
    } //-- void deleteCdPessoaParticipacao() 

    /**
     * Method deleteCdTipoContrato
     * 
     */
    public void deleteCdTipoContrato()
    {
        this._has_cdTipoContrato= false;
    } //-- void deleteCdTipoContrato() 

    /**
     * Method deleteCdTipoPesquisa
     * 
     */
    public void deleteCdTipoPesquisa()
    {
        this._has_cdTipoPesquisa= false;
    } //-- void deleteCdTipoPesquisa() 

    /**
     * Method deleteNmPessoaJuridica
     * 
     */
    public void deleteNmPessoaJuridica()
    {
        this._has_nmPessoaJuridica= false;
    } //-- void deleteNmPessoaJuridica() 

    /**
     * Method deleteNrOcorrencias
     * 
     */
    public void deleteNrOcorrencias()
    {
        this._has_nrOcorrencias= false;
    } //-- void deleteNrOcorrencias() 

    /**
     * Method deleteNrSequencialContrato
     * 
     */
    public void deleteNrSequencialContrato()
    {
        this._has_nrSequencialContrato= false;
    } //-- void deleteNrSequencialContrato() 

    /**
     * Returns the value of field 'cdAgencia'.
     * 
     * @return int
     * @return the value of field 'cdAgencia'.
     */
    public int getCdAgencia()
    {
        return this._cdAgencia;
    } //-- int getCdAgencia() 

    /**
     * Returns the value of field 'cdBanco'.
     * 
     * @return int
     * @return the value of field 'cdBanco'.
     */
    public int getCdBanco()
    {
        return this._cdBanco;
    } //-- int getCdBanco() 

    /**
     * Returns the value of field 'cdConta'.
     * 
     * @return long
     * @return the value of field 'cdConta'.
     */
    public long getCdConta()
    {
        return this._cdConta;
    } //-- long getCdConta() 

    /**
     * Returns the value of field 'cdDigitoConta'.
     * 
     * @return String
     * @return the value of field 'cdDigitoConta'.
     */
    public java.lang.String getCdDigitoConta()
    {
        return this._cdDigitoConta;
    } //-- java.lang.String getCdDigitoConta() 

    /**
     * Returns the value of field 'cdPessoaParticipacao'.
     * 
     * @return long
     * @return the value of field 'cdPessoaParticipacao'.
     */
    public long getCdPessoaParticipacao()
    {
        return this._cdPessoaParticipacao;
    } //-- long getCdPessoaParticipacao() 

    /**
     * Returns the value of field 'cdTipoContrato'.
     * 
     * @return int
     * @return the value of field 'cdTipoContrato'.
     */
    public int getCdTipoContrato()
    {
        return this._cdTipoContrato;
    } //-- int getCdTipoContrato() 

    /**
     * Returns the value of field 'cdTipoPesquisa'.
     * 
     * @return int
     * @return the value of field 'cdTipoPesquisa'.
     */
    public int getCdTipoPesquisa()
    {
        return this._cdTipoPesquisa;
    } //-- int getCdTipoPesquisa() 

    /**
     * Returns the value of field 'dtFinalLiberacao'.
     * 
     * @return String
     * @return the value of field 'dtFinalLiberacao'.
     */
    public java.lang.String getDtFinalLiberacao()
    {
        return this._dtFinalLiberacao;
    } //-- java.lang.String getDtFinalLiberacao() 

    /**
     * Returns the value of field 'dtInicioLiberacao'.
     * 
     * @return String
     * @return the value of field 'dtInicioLiberacao'.
     */
    public java.lang.String getDtInicioLiberacao()
    {
        return this._dtInicioLiberacao;
    } //-- java.lang.String getDtInicioLiberacao() 

    /**
     * Returns the value of field 'nmPessoaJuridica'.
     * 
     * @return long
     * @return the value of field 'nmPessoaJuridica'.
     */
    public long getNmPessoaJuridica()
    {
        return this._nmPessoaJuridica;
    } //-- long getNmPessoaJuridica() 

    /**
     * Returns the value of field 'nrOcorrencias'.
     * 
     * @return int
     * @return the value of field 'nrOcorrencias'.
     */
    public int getNrOcorrencias()
    {
        return this._nrOcorrencias;
    } //-- int getNrOcorrencias() 

    /**
     * Returns the value of field 'nrSequencialContrato'.
     * 
     * @return long
     * @return the value of field 'nrSequencialContrato'.
     */
    public long getNrSequencialContrato()
    {
        return this._nrSequencialContrato;
    } //-- long getNrSequencialContrato() 

    /**
     * Method hasCdAgencia
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgencia()
    {
        return this._has_cdAgencia;
    } //-- boolean hasCdAgencia() 

    /**
     * Method hasCdBanco
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdBanco()
    {
        return this._has_cdBanco;
    } //-- boolean hasCdBanco() 

    /**
     * Method hasCdConta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdConta()
    {
        return this._has_cdConta;
    } //-- boolean hasCdConta() 

    /**
     * Method hasCdPessoaParticipacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaParticipacao()
    {
        return this._has_cdPessoaParticipacao;
    } //-- boolean hasCdPessoaParticipacao() 

    /**
     * Method hasCdTipoContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContrato()
    {
        return this._has_cdTipoContrato;
    } //-- boolean hasCdTipoContrato() 

    /**
     * Method hasCdTipoPesquisa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoPesquisa()
    {
        return this._has_cdTipoPesquisa;
    } //-- boolean hasCdTipoPesquisa() 

    /**
     * Method hasNmPessoaJuridica
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNmPessoaJuridica()
    {
        return this._has_nmPessoaJuridica;
    } //-- boolean hasNmPessoaJuridica() 

    /**
     * Method hasNrOcorrencias
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrOcorrencias()
    {
        return this._has_nrOcorrencias;
    } //-- boolean hasNrOcorrencias() 

    /**
     * Method hasNrSequencialContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequencialContrato()
    {
        return this._has_nrSequencialContrato;
    } //-- boolean hasNrSequencialContrato() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdAgencia'.
     * 
     * @param cdAgencia the value of field 'cdAgencia'.
     */
    public void setCdAgencia(int cdAgencia)
    {
        this._cdAgencia = cdAgencia;
        this._has_cdAgencia = true;
    } //-- void setCdAgencia(int) 

    /**
     * Sets the value of field 'cdBanco'.
     * 
     * @param cdBanco the value of field 'cdBanco'.
     */
    public void setCdBanco(int cdBanco)
    {
        this._cdBanco = cdBanco;
        this._has_cdBanco = true;
    } //-- void setCdBanco(int) 

    /**
     * Sets the value of field 'cdConta'.
     * 
     * @param cdConta the value of field 'cdConta'.
     */
    public void setCdConta(long cdConta)
    {
        this._cdConta = cdConta;
        this._has_cdConta = true;
    } //-- void setCdConta(long) 

    /**
     * Sets the value of field 'cdDigitoConta'.
     * 
     * @param cdDigitoConta the value of field 'cdDigitoConta'.
     */
    public void setCdDigitoConta(java.lang.String cdDigitoConta)
    {
        this._cdDigitoConta = cdDigitoConta;
    } //-- void setCdDigitoConta(java.lang.String) 

    /**
     * Sets the value of field 'cdPessoaParticipacao'.
     * 
     * @param cdPessoaParticipacao the value of field
     * 'cdPessoaParticipacao'.
     */
    public void setCdPessoaParticipacao(long cdPessoaParticipacao)
    {
        this._cdPessoaParticipacao = cdPessoaParticipacao;
        this._has_cdPessoaParticipacao = true;
    } //-- void setCdPessoaParticipacao(long) 

    /**
     * Sets the value of field 'cdTipoContrato'.
     * 
     * @param cdTipoContrato the value of field 'cdTipoContrato'.
     */
    public void setCdTipoContrato(int cdTipoContrato)
    {
        this._cdTipoContrato = cdTipoContrato;
        this._has_cdTipoContrato = true;
    } //-- void setCdTipoContrato(int) 

    /**
     * Sets the value of field 'cdTipoPesquisa'.
     * 
     * @param cdTipoPesquisa the value of field 'cdTipoPesquisa'.
     */
    public void setCdTipoPesquisa(int cdTipoPesquisa)
    {
        this._cdTipoPesquisa = cdTipoPesquisa;
        this._has_cdTipoPesquisa = true;
    } //-- void setCdTipoPesquisa(int) 

    /**
     * Sets the value of field 'dtFinalLiberacao'.
     * 
     * @param dtFinalLiberacao the value of field 'dtFinalLiberacao'
     */
    public void setDtFinalLiberacao(java.lang.String dtFinalLiberacao)
    {
        this._dtFinalLiberacao = dtFinalLiberacao;
    } //-- void setDtFinalLiberacao(java.lang.String) 

    /**
     * Sets the value of field 'dtInicioLiberacao'.
     * 
     * @param dtInicioLiberacao the value of field
     * 'dtInicioLiberacao'.
     */
    public void setDtInicioLiberacao(java.lang.String dtInicioLiberacao)
    {
        this._dtInicioLiberacao = dtInicioLiberacao;
    } //-- void setDtInicioLiberacao(java.lang.String) 

    /**
     * Sets the value of field 'nmPessoaJuridica'.
     * 
     * @param nmPessoaJuridica the value of field 'nmPessoaJuridica'
     */
    public void setNmPessoaJuridica(long nmPessoaJuridica)
    {
        this._nmPessoaJuridica = nmPessoaJuridica;
        this._has_nmPessoaJuridica = true;
    } //-- void setNmPessoaJuridica(long) 

    /**
     * Sets the value of field 'nrOcorrencias'.
     * 
     * @param nrOcorrencias the value of field 'nrOcorrencias'.
     */
    public void setNrOcorrencias(int nrOcorrencias)
    {
        this._nrOcorrencias = nrOcorrencias;
        this._has_nrOcorrencias = true;
    } //-- void setNrOcorrencias(int) 

    /**
     * Sets the value of field 'nrSequencialContrato'.
     * 
     * @param nrSequencialContrato the value of field
     * 'nrSequencialContrato'.
     */
    public void setNrSequencialContrato(long nrSequencialContrato)
    {
        this._nrSequencialContrato = nrSequencialContrato;
        this._has_nrSequencialContrato = true;
    } //-- void setNrSequencialContrato(long) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ConsultarLiberacaoPreviaRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultarliberacaoprevia.request.ConsultarLiberacaoPreviaRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultarliberacaoprevia.request.ConsultarLiberacaoPreviaRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultarliberacaoprevia.request.ConsultarLiberacaoPreviaRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarliberacaoprevia.request.ConsultarLiberacaoPreviaRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
