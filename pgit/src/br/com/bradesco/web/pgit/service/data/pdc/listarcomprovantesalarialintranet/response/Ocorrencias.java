/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listarcomprovantesalarialintranet.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdBanco
     */
    private int _cdBanco = 0;

    /**
     * keeps track of state for field: _cdBanco
     */
    private boolean _has_cdBanco;

    /**
     * Field _dsBanco
     */
    private java.lang.String _dsBanco;

    /**
     * Field _cdAgenciaBancaria
     */
    private int _cdAgenciaBancaria = 0;

    /**
     * keeps track of state for field: _cdAgenciaBancaria
     */
    private boolean _has_cdAgenciaBancaria;

    /**
     * Field _cdContaCorrente
     */
    private long _cdContaCorrente = 0;

    /**
     * keeps track of state for field: _cdContaCorrente
     */
    private boolean _has_cdContaCorrente;

    /**
     * Field _cdDigitoConta
     */
    private java.lang.String _cdDigitoConta;

    /**
     * Field _cdCpfCnpj
     */
    private java.lang.String _cdCpfCnpj;

    /**
     * Field _cdEmpresaComprovanteSalarial
     */
    private long _cdEmpresaComprovanteSalarial = 0;

    /**
     * keeps track of state for field: _cdEmpresaComprovanteSalarial
     */
    private boolean _has_cdEmpresaComprovanteSalarial;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarcomprovantesalarialintranet.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdAgenciaBancaria
     * 
     */
    public void deleteCdAgenciaBancaria()
    {
        this._has_cdAgenciaBancaria= false;
    } //-- void deleteCdAgenciaBancaria() 

    /**
     * Method deleteCdBanco
     * 
     */
    public void deleteCdBanco()
    {
        this._has_cdBanco= false;
    } //-- void deleteCdBanco() 

    /**
     * Method deleteCdContaCorrente
     * 
     */
    public void deleteCdContaCorrente()
    {
        this._has_cdContaCorrente= false;
    } //-- void deleteCdContaCorrente() 

    /**
     * Method deleteCdEmpresaComprovanteSalarial
     * 
     */
    public void deleteCdEmpresaComprovanteSalarial()
    {
        this._has_cdEmpresaComprovanteSalarial= false;
    } //-- void deleteCdEmpresaComprovanteSalarial() 

    /**
     * Returns the value of field 'cdAgenciaBancaria'.
     * 
     * @return int
     * @return the value of field 'cdAgenciaBancaria'.
     */
    public int getCdAgenciaBancaria()
    {
        return this._cdAgenciaBancaria;
    } //-- int getCdAgenciaBancaria() 

    /**
     * Returns the value of field 'cdBanco'.
     * 
     * @return int
     * @return the value of field 'cdBanco'.
     */
    public int getCdBanco()
    {
        return this._cdBanco;
    } //-- int getCdBanco() 

    /**
     * Returns the value of field 'cdContaCorrente'.
     * 
     * @return long
     * @return the value of field 'cdContaCorrente'.
     */
    public long getCdContaCorrente()
    {
        return this._cdContaCorrente;
    } //-- long getCdContaCorrente() 

    /**
     * Returns the value of field 'cdCpfCnpj'.
     * 
     * @return String
     * @return the value of field 'cdCpfCnpj'.
     */
    public java.lang.String getCdCpfCnpj()
    {
        return this._cdCpfCnpj;
    } //-- java.lang.String getCdCpfCnpj() 

    /**
     * Returns the value of field 'cdDigitoConta'.
     * 
     * @return String
     * @return the value of field 'cdDigitoConta'.
     */
    public java.lang.String getCdDigitoConta()
    {
        return this._cdDigitoConta;
    } //-- java.lang.String getCdDigitoConta() 

    /**
     * Returns the value of field 'cdEmpresaComprovanteSalarial'.
     * 
     * @return long
     * @return the value of field 'cdEmpresaComprovanteSalarial'.
     */
    public long getCdEmpresaComprovanteSalarial()
    {
        return this._cdEmpresaComprovanteSalarial;
    } //-- long getCdEmpresaComprovanteSalarial() 

    /**
     * Returns the value of field 'dsBanco'.
     * 
     * @return String
     * @return the value of field 'dsBanco'.
     */
    public java.lang.String getDsBanco()
    {
        return this._dsBanco;
    } //-- java.lang.String getDsBanco() 

    /**
     * Method hasCdAgenciaBancaria
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgenciaBancaria()
    {
        return this._has_cdAgenciaBancaria;
    } //-- boolean hasCdAgenciaBancaria() 

    /**
     * Method hasCdBanco
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdBanco()
    {
        return this._has_cdBanco;
    } //-- boolean hasCdBanco() 

    /**
     * Method hasCdContaCorrente
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdContaCorrente()
    {
        return this._has_cdContaCorrente;
    } //-- boolean hasCdContaCorrente() 

    /**
     * Method hasCdEmpresaComprovanteSalarial
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdEmpresaComprovanteSalarial()
    {
        return this._has_cdEmpresaComprovanteSalarial;
    } //-- boolean hasCdEmpresaComprovanteSalarial() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdAgenciaBancaria'.
     * 
     * @param cdAgenciaBancaria the value of field
     * 'cdAgenciaBancaria'.
     */
    public void setCdAgenciaBancaria(int cdAgenciaBancaria)
    {
        this._cdAgenciaBancaria = cdAgenciaBancaria;
        this._has_cdAgenciaBancaria = true;
    } //-- void setCdAgenciaBancaria(int) 

    /**
     * Sets the value of field 'cdBanco'.
     * 
     * @param cdBanco the value of field 'cdBanco'.
     */
    public void setCdBanco(int cdBanco)
    {
        this._cdBanco = cdBanco;
        this._has_cdBanco = true;
    } //-- void setCdBanco(int) 

    /**
     * Sets the value of field 'cdContaCorrente'.
     * 
     * @param cdContaCorrente the value of field 'cdContaCorrente'.
     */
    public void setCdContaCorrente(long cdContaCorrente)
    {
        this._cdContaCorrente = cdContaCorrente;
        this._has_cdContaCorrente = true;
    } //-- void setCdContaCorrente(long) 

    /**
     * Sets the value of field 'cdCpfCnpj'.
     * 
     * @param cdCpfCnpj the value of field 'cdCpfCnpj'.
     */
    public void setCdCpfCnpj(java.lang.String cdCpfCnpj)
    {
        this._cdCpfCnpj = cdCpfCnpj;
    } //-- void setCdCpfCnpj(java.lang.String) 

    /**
     * Sets the value of field 'cdDigitoConta'.
     * 
     * @param cdDigitoConta the value of field 'cdDigitoConta'.
     */
    public void setCdDigitoConta(java.lang.String cdDigitoConta)
    {
        this._cdDigitoConta = cdDigitoConta;
    } //-- void setCdDigitoConta(java.lang.String) 

    /**
     * Sets the value of field 'cdEmpresaComprovanteSalarial'.
     * 
     * @param cdEmpresaComprovanteSalarial the value of field
     * 'cdEmpresaComprovanteSalarial'.
     */
    public void setCdEmpresaComprovanteSalarial(long cdEmpresaComprovanteSalarial)
    {
        this._cdEmpresaComprovanteSalarial = cdEmpresaComprovanteSalarial;
        this._has_cdEmpresaComprovanteSalarial = true;
    } //-- void setCdEmpresaComprovanteSalarial(long) 

    /**
     * Sets the value of field 'dsBanco'.
     * 
     * @param dsBanco the value of field 'dsBanco'.
     */
    public void setDsBanco(java.lang.String dsBanco)
    {
        this._dsBanco = dsBanco;
    } //-- void setDsBanco(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listarcomprovantesalarialintranet.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listarcomprovantesalarialintranet.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listarcomprovantesalarialintranet.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarcomprovantesalarialintranet.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
