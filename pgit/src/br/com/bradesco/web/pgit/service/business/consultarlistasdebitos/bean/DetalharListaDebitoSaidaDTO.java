/*
 * Nome: br.com.bradesco.web.pgit.service.business.consultarlistasdebitos.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.consultarlistasdebitos.bean;

import java.math.BigDecimal;

/**
 * Nome: DetalharListaDebitoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class DetalharListaDebitoSaidaDTO {
	
	/** Atributo codMensagem. */
	private String codMensagem;
	
	/** Atributo mensagem. */
	private String mensagem;
	
	/** Atributo numeroLinhas. */
	private Integer numeroLinhas;
	
	/** Atributo cdControlePagamento. */
	private String cdControlePagamento;
	
	/** Atributo vlPagamento. */
	private BigDecimal vlPagamento;
	
	/** Atributo cdFavorecidoBeneficiario. */
	private Long cdFavorecidoBeneficiario;
	
	/** Atributo dsFavorecidoBeneficiario. */
	private String dsFavorecidoBeneficiario;
	
	/** Atributo cdBanco. */
	private Integer cdBanco;
	
	/** Atributo dsBanco. */
	private String dsBanco;
	
	/** Atributo cdAgencia. */
	private Integer cdAgencia;
	
	/** Atributo cdDigitoAgencia. */
	private Integer cdDigitoAgencia;
	
	/** Atributo dsAgencia. */
	private String dsAgencia;
	
	/** Atributo cdConta. */
	private Long cdConta;
	
	/** Atributo cdDigitoConta. */
	private String cdDigitoConta;
	
	/** Atributo cdSituacaoOperacaoPagamento. */
	private Integer cdSituacaoOperacaoPagamento;
	
	/** Atributo dsSituacaoPagamento. */
	private String dsSituacaoPagamento;
	
	/** Atributo cdSituacaoPagamentoLista. */
	private Integer cdSituacaoPagamentoLista;
	
	/** Atributo dsSituacaoPagamentoLista. */
	private String dsSituacaoPagamentoLista;
	
	/** Atributo dsContaDebitoFormatado. */
	private String dsContaDebitoFormatado;
	
	/** Atributo cdMotivoSituacaoPagamento. */
	private Integer cdMotivoSituacaoPagamento;
	
	/** Atributo dsMotivoPagamento. */
	private String dsMotivoPagamento;
	
	/** Atributo cdTipoTela. */
	private Integer cdTipoTela;
	
	/** Atributo cdTabelaConsulta. */
	private Integer cdTabelaConsulta;
	
	/** Atributo favorecidoBeneficiario. */
	private String favorecidoBeneficiario;	
	
	/** Atributo dtEfetivacaoPagamento. */
	private String dtEfetivacaoPagamento;

	/**
	 * Get: favorecidoBeneficiario.
	 *
	 * @return favorecidoBeneficiario
	 */
	public String getFavorecidoBeneficiario() {
		return favorecidoBeneficiario;
	}

	/**
	 * Set: favorecidoBeneficiario.
	 *
	 * @param favorecidoBeneficiario the favorecido beneficiario
	 */
	public void setFavorecidoBeneficiario(String favorecidoBeneficiario) {
		this.favorecidoBeneficiario = favorecidoBeneficiario;
	}

	/**
	 * Get: dsContaDebitoFormatado.
	 *
	 * @return dsContaDebitoFormatado
	 */
	public String getDsContaDebitoFormatado() {
		return dsContaDebitoFormatado;
	}

	/**
	 * Set: dsContaDebitoFormatado.
	 *
	 * @param dsContaDebitoFormatado the ds conta debito formatado
	 */
	public void setDsContaDebitoFormatado(String dsContaDebitoFormatado) {
		this.dsContaDebitoFormatado = dsContaDebitoFormatado;
	}

	/**
	 * Get: cdAgencia.
	 *
	 * @return cdAgencia
	 */
	public Integer getCdAgencia() {
		return cdAgencia;
	}

	/**
	 * Set: cdAgencia.
	 *
	 * @param cdAgencia the cd agencia
	 */
	public void setCdAgencia(Integer cdAgencia) {
		this.cdAgencia = cdAgencia;
	}

	/**
	 * Get: cdBanco.
	 *
	 * @return cdBanco
	 */
	public Integer getCdBanco() {
		return cdBanco;
	}

	/**
	 * Set: cdBanco.
	 *
	 * @param cdBanco the cd banco
	 */
	public void setCdBanco(Integer cdBanco) {
		this.cdBanco = cdBanco;
	}

	/**
	 * Get: cdConta.
	 *
	 * @return cdConta
	 */
	public Long getCdConta() {
		return cdConta;
	}

	/**
	 * Set: cdConta.
	 *
	 * @param cdConta the cd conta
	 */
	public void setCdConta(Long cdConta) {
		this.cdConta = cdConta;
	}

	/**
	 * Get: cdControlePagamento.
	 *
	 * @return cdControlePagamento
	 */
	public String getCdControlePagamento() {
		return cdControlePagamento;
	}

	/**
	 * Set: cdControlePagamento.
	 *
	 * @param cdControlePagamento the cd controle pagamento
	 */
	public void setCdControlePagamento(String cdControlePagamento) {
		this.cdControlePagamento = cdControlePagamento;
	}

	/**
	 * Get: cdDigitoAgencia.
	 *
	 * @return cdDigitoAgencia
	 */
	public Integer getCdDigitoAgencia() {
		return cdDigitoAgencia;
	}

	/**
	 * Set: cdDigitoAgencia.
	 *
	 * @param cdDigitoAgencia the cd digito agencia
	 */
	public void setCdDigitoAgencia(Integer cdDigitoAgencia) {
		this.cdDigitoAgencia = cdDigitoAgencia;
	}

	/**
	 * Get: cdDigitoConta.
	 *
	 * @return cdDigitoConta
	 */
	public String getCdDigitoConta() {
		return cdDigitoConta;
	}

	/**
	 * Set: cdDigitoConta.
	 *
	 * @param cdDigitoConta the cd digito conta
	 */
	public void setCdDigitoConta(String cdDigitoConta) {
		this.cdDigitoConta = cdDigitoConta;
	}

	/**
	 * Get: cdFavorecidoBeneficiario.
	 *
	 * @return cdFavorecidoBeneficiario
	 */
	public Long getCdFavorecidoBeneficiario() {
		return cdFavorecidoBeneficiario;
	}

	/**
	 * Set: cdFavorecidoBeneficiario.
	 *
	 * @param cdFavorecidoBeneficiario the cd favorecido beneficiario
	 */
	public void setCdFavorecidoBeneficiario(Long cdFavorecidoBeneficiario) {
		this.cdFavorecidoBeneficiario = cdFavorecidoBeneficiario;
	}

	/**
	 * Get: cdSituacaoOperacaoPagamento.
	 *
	 * @return cdSituacaoOperacaoPagamento
	 */
	public Integer getCdSituacaoOperacaoPagamento() {
		return cdSituacaoOperacaoPagamento;
	}

	/**
	 * Set: cdSituacaoOperacaoPagamento.
	 *
	 * @param cdSituacaoOperacaoPagamento the cd situacao operacao pagamento
	 */
	public void setCdSituacaoOperacaoPagamento(
			Integer cdSituacaoOperacaoPagamento) {
		this.cdSituacaoOperacaoPagamento = cdSituacaoOperacaoPagamento;
	}

	/**
	 * Get: cdSituacaoPagamentoLista.
	 *
	 * @return cdSituacaoPagamentoLista
	 */
	public Integer getCdSituacaoPagamentoLista() {
		return cdSituacaoPagamentoLista;
	}

	/**
	 * Set: cdSituacaoPagamentoLista.
	 *
	 * @param cdSituacaoPagamentoLista the cd situacao pagamento lista
	 */
	public void setCdSituacaoPagamentoLista(Integer cdSituacaoPagamentoLista) {
		this.cdSituacaoPagamentoLista = cdSituacaoPagamentoLista;
	}

	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}

	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}

	/**
	 * Get: dsAgencia.
	 *
	 * @return dsAgencia
	 */
	public String getDsAgencia() {
		return dsAgencia;
	}

	/**
	 * Set: dsAgencia.
	 *
	 * @param dsAgencia the ds agencia
	 */
	public void setDsAgencia(String dsAgencia) {
		this.dsAgencia = dsAgencia;
	}

	/**
	 * Get: dsBanco.
	 *
	 * @return dsBanco
	 */
	public String getDsBanco() {
		return dsBanco;
	}

	/**
	 * Set: dsBanco.
	 *
	 * @param dsBanco the ds banco
	 */
	public void setDsBanco(String dsBanco) {
		this.dsBanco = dsBanco;
	}

	/**
	 * Get: dsFavorecidoBeneficiario.
	 *
	 * @return dsFavorecidoBeneficiario
	 */
	public String getDsFavorecidoBeneficiario() {
		return dsFavorecidoBeneficiario;
	}

	/**
	 * Set: dsFavorecidoBeneficiario.
	 *
	 * @param dsFavorecidoBeneficiario the ds favorecido beneficiario
	 */
	public void setDsFavorecidoBeneficiario(String dsFavorecidoBeneficiario) {
		this.dsFavorecidoBeneficiario = dsFavorecidoBeneficiario;
	}

	/**
	 * Get: dsSituacaoPagamento.
	 *
	 * @return dsSituacaoPagamento
	 */
	public String getDsSituacaoPagamento() {
		return dsSituacaoPagamento;
	}

	/**
	 * Set: dsSituacaoPagamento.
	 *
	 * @param dsSituacaoPagamento the ds situacao pagamento
	 */
	public void setDsSituacaoPagamento(String dsSituacaoPagamento) {
		this.dsSituacaoPagamento = dsSituacaoPagamento;
	}

	/**
	 * Get: dsSituacaoPagamentoLista.
	 *
	 * @return dsSituacaoPagamentoLista
	 */
	public String getDsSituacaoPagamentoLista() {
		return dsSituacaoPagamentoLista;
	}

	/**
	 * Set: dsSituacaoPagamentoLista.
	 *
	 * @param dsSituacaoPagamentoLista the ds situacao pagamento lista
	 */
	public void setDsSituacaoPagamentoLista(String dsSituacaoPagamentoLista) {
		this.dsSituacaoPagamentoLista = dsSituacaoPagamentoLista;
	}

	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}

	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	/**
	 * Get: numeroLinhas.
	 *
	 * @return numeroLinhas
	 */
	public Integer getNumeroLinhas() {
		return numeroLinhas;
	}

	/**
	 * Set: numeroLinhas.
	 *
	 * @param numeroLinhas the numero linhas
	 */
	public void setNumeroLinhas(Integer numeroLinhas) {
		this.numeroLinhas = numeroLinhas;
	}

	/**
	 * Get: vlPagamento.
	 *
	 * @return vlPagamento
	 */
	public BigDecimal getVlPagamento() {
		return vlPagamento;
	}

	/**
	 * Set: vlPagamento.
	 *
	 * @param vlPagamento the vl pagamento
	 */
	public void setVlPagamento(BigDecimal vlPagamento) {
		this.vlPagamento = vlPagamento;
	}

	/**
	 * Get: cdMotivoSituacaoPagamento.
	 *
	 * @return cdMotivoSituacaoPagamento
	 */
	public Integer getCdMotivoSituacaoPagamento() {
	    return cdMotivoSituacaoPagamento;
	}

	/**
	 * Set: cdMotivoSituacaoPagamento.
	 *
	 * @param cdMotivoSituacaoPagamento the cd motivo situacao pagamento
	 */
	public void setCdMotivoSituacaoPagamento(Integer cdMotivoSituacaoPagamento) {
	    this.cdMotivoSituacaoPagamento = cdMotivoSituacaoPagamento;
	}

	/**
	 * Get: dsMotivoPagamento.
	 *
	 * @return dsMotivoPagamento
	 */
	public String getDsMotivoPagamento() {
	    return dsMotivoPagamento;
	}

	/**
	 * Set: dsMotivoPagamento.
	 *
	 * @param dsMotivoPagamento the ds motivo pagamento
	 */
	public void setDsMotivoPagamento(String dsMotivoPagamento) {
	    this.dsMotivoPagamento = dsMotivoPagamento;
	}

	/**
	 * Get: cdTipoTela.
	 *
	 * @return cdTipoTela
	 */
	public Integer getCdTipoTela() {
	    return cdTipoTela;
	}

	/**
	 * Set: cdTipoTela.
	 *
	 * @param cdTipoTela the cd tipo tela
	 */
	public void setCdTipoTela(Integer cdTipoTela) {
	    this.cdTipoTela = cdTipoTela;
	}

	/**
	 * Get: cdTabelaConsulta.
	 *
	 * @return cdTabelaConsulta
	 */
	public Integer getCdTabelaConsulta() {
	    return cdTabelaConsulta;
	}

	/**
	 * Set: cdTabelaConsulta.
	 *
	 * @param cdTabelaConsulta the cd tabela consulta
	 */
	public void setCdTabelaConsulta(Integer cdTabelaConsulta) {
	    this.cdTabelaConsulta = cdTabelaConsulta;
	}

	/**
	 * @param dtEfetivacaoPagamento the dtEfetivacaoPagamento to set
	 */
	public void setDtEfetivacaoPagamento(String dtEfetivacaoPagamento) {
		this.dtEfetivacaoPagamento = dtEfetivacaoPagamento;
	}

	/**
	 * @return the dtEfetivacaoPagamento
	 */
	public String getDtEfetivacaoPagamento() {
		return dtEfetivacaoPagamento;
	}
}