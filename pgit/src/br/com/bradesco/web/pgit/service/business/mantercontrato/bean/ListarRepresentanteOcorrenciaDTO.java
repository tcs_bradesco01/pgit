package br.com.bradesco.web.pgit.service.business.mantercontrato.bean;

import br.com.bradesco.web.pgit.utils.CpfCnpjUtils;

public class ListarRepresentanteOcorrenciaDTO {

	/** Atributo cdPessoa. */
	private Long cdPessoa;

	/** Atributo cdTipoParticipacaoPessoa. */
	private Integer cdTipoParticipacaoPessoa;

	/** Atributo cdCPF. */
	private Long cdCPF;

	/** Atributo cdFilialCnpj. */
	private Integer cdFilialCnpj;

	/** Atributo cdControleCnpj. */
	private Integer cdControleCnpj;

	/** Atributo dsNomeRazao. */
	private String dsNomeRazao;

	/** Atributo dsnome. */
	private String dsnome;

	/** Atributo cdAgencia. */
	private Integer cdAgencia;

	/** Atributo cdConta. */
	private Integer cdConta;

	/** Atributo cdContaDigito. */
	private Integer cdContaDigito;

	/** Atributo selecionado */
	private boolean selecionado;

	/**
	 * @param cdPessoa
	 * @param cdTipoParticipacaoPessoa
	 * @param cdCPF
	 * @param cdFilialCnpj
	 * @param cdControleCnpj
	 * @param dsNomeRazao
	 * @param dsnome
	 * @param cdAgencia
	 * @param cdConta
	 * @param cdContaDigito
	 */
	public ListarRepresentanteOcorrenciaDTO(Long cdPessoa,
			Integer cdTipoParticipacaoPessoa, Long cdCPF, Integer cdFilialCnpj,
			Integer cdControleCnpj, String dsNomeRazao, String dsnome,
			Integer cdAgencia, Integer cdConta, Integer cdContaDigito) {
		super();
		this.cdPessoa = cdPessoa;
		this.cdTipoParticipacaoPessoa = cdTipoParticipacaoPessoa;
		this.cdCPF = cdCPF;
		this.cdFilialCnpj = cdFilialCnpj;
		this.cdControleCnpj = cdControleCnpj;
		this.dsNomeRazao = dsNomeRazao;
		this.dsnome = dsnome;
		this.cdAgencia = cdAgencia;
		this.cdConta = cdConta;
		this.cdContaDigito = cdContaDigito;
	}

	/**
	 * @return the cdPessoa
	 */
	public Long getCdPessoa() {
		return cdPessoa;
	}

	/**
	 * @param cdPessoa
	 *            the cdPessoa to set
	 */
	public void setCdPessoa(Long cdPessoa) {
		this.cdPessoa = cdPessoa;
	}

	/**
	 * @return the cdTipoParticipacaoPessoa
	 */
	public Integer getCdTipoParticipacaoPessoa() {
		return cdTipoParticipacaoPessoa;
	}

	/**
	 * @param cdTipoParticipacaoPessoa
	 *            the cdTipoParticipacaoPessoa to set
	 */
	public void setCdTipoParticipacaoPessoa(Integer cdTipoParticipacaoPessoa) {
		this.cdTipoParticipacaoPessoa = cdTipoParticipacaoPessoa;
	}

	/**
	 * @return the cdCPF
	 */
	public Long getCdCPF() {
		return cdCPF;
	}

	/**
	 * @param cdCPF
	 *            the cdCPF to set
	 */
	public void setCdCPF(Long cdCPF) {
		this.cdCPF = cdCPF;
	}

	/**
	 * @return the cdFilialCnpj
	 */
	public Integer getCdFilialCnpj() {
		return cdFilialCnpj;
	}

	/**
	 * @param cdFilialCnpj
	 *            the cdFilialCnpj to set
	 */
	public void setCdFilialCnpj(Integer cdFilialCnpj) {
		this.cdFilialCnpj = cdFilialCnpj;
	}

	/**
	 * @return the cdControleCnpj
	 */
	public Integer getCdControleCnpj() {
		return cdControleCnpj;
	}

	/**
	 * @param cdControleCnpj
	 *            the cdControleCnpj to set
	 */
	public void setCdControleCnpj(Integer cdControleCnpj) {
		this.cdControleCnpj = cdControleCnpj;
	}

	/**
	 * @return the dsNomeRazao
	 */
	public String getDsNomeRazao() {
		return dsNomeRazao;
	}

	/**
	 * @param dsNomeRazao
	 *            the dsNomeRazao to set
	 */
	public void setDsNomeRazao(String dsNomeRazao) {
		this.dsNomeRazao = dsNomeRazao;
	}

	/**
	 * @return the dsnome
	 */
	public String getDsnome() {
		return dsnome;
	}

	/**
	 * @param dsnome
	 *            the dsnome to set
	 */
	public void setDsnome(String dsnome) {
		this.dsnome = dsnome;
	}

	/**
	 * @return the cdAgencia
	 */
	public Integer getCdAgencia() {
		return cdAgencia;
	}

	/**
	 * @param cdAgencia
	 *            the cdAgencia to set
	 */
	public void setCdAgencia(Integer cdAgencia) {
		this.cdAgencia = cdAgencia;
	}

	/**
	 * @return the cdConta
	 */
	public Integer getCdConta() {
		return cdConta;
	}

	/**
	 * @param cdConta
	 *            the cdConta to set
	 */
	public void setCdConta(Integer cdConta) {
		this.cdConta = cdConta;
	}

	/**
	 * @return the cdContaDigito
	 */
	public Integer getCdContaDigito() {
		return cdContaDigito;
	}

	/**
	 * @param cdContaDigito
	 *            the cdContaDigito to set
	 */
	public void setCdContaDigito(Integer cdContaDigito) {
		this.cdContaDigito = cdContaDigito;
	}

	/**
	 * 
	 * @return the Cpf Cnpj Formatado
	 */
	public String getCpfCnpjFormatado() {
		return CpfCnpjUtils
				.formatarCpfCnpj(cdCPF, cdFilialCnpj, cdControleCnpj);
	}

	/**
	 * @return the selecionado
	 */
	public boolean isSelecionado() {
		return selecionado;
	}

	/**
	 * @param selecionado
	 *            the selecionado to set
	 */
	public void setSelecionado(boolean selecionado) {
		this.selecionado = selecionado;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cdCPF == null) ? 0 : cdCPF.hashCode());
		result = prime * result
				+ ((cdControleCnpj == null) ? 0 : cdControleCnpj.hashCode());
		result = prime * result
				+ ((cdFilialCnpj == null) ? 0 : cdFilialCnpj.hashCode());
		result = prime * result
				+ ((cdPessoa == null) ? 0 : cdPessoa.hashCode());
		result = prime
				* result
				+ ((cdTipoParticipacaoPessoa == null)
						? 0
						: cdTipoParticipacaoPessoa.hashCode());
		result = prime * result
				+ ((dsNomeRazao == null) ? 0 : dsNomeRazao.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ListarRepresentanteOcorrenciaDTO other = (ListarRepresentanteOcorrenciaDTO) obj;
		if (cdCPF == null) {
			if (other.cdCPF != null)
				return false;
		} else if (!cdCPF.equals(other.cdCPF))
			return false;
		if (cdControleCnpj == null) {
			if (other.cdControleCnpj != null)
				return false;
		} else if (!cdControleCnpj.equals(other.cdControleCnpj))
			return false;
		if (cdFilialCnpj == null) {
			if (other.cdFilialCnpj != null)
				return false;
		} else if (!cdFilialCnpj.equals(other.cdFilialCnpj))
			return false;
		if (cdPessoa == null) {
			if (other.cdPessoa != null)
				return false;
		} else if (!cdPessoa.equals(other.cdPessoa))
			return false;
		if (cdTipoParticipacaoPessoa == null) {
			if (other.cdTipoParticipacaoPessoa != null)
				return false;
		} else if (!cdTipoParticipacaoPessoa
				.equals(other.cdTipoParticipacaoPessoa))
			return false;
		if (dsNomeRazao == null) {
			if (other.dsNomeRazao != null)
				return false;
		} else if (!dsNomeRazao.equals(other.dsNomeRazao))
			return false;
		return true;
	}
	
}
