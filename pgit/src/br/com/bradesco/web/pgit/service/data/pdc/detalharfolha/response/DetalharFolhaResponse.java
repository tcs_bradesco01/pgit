/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.detalharfolha.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.math.BigDecimal;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class DetalharFolhaResponse.
 * 
 * @version $Revision$ $Date$
 */
public class DetalharFolhaResponse implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _codMensagem
     */
    private java.lang.String _codMensagem;

    /**
     * Field _mensagem
     */
    private java.lang.String _mensagem;

    /**
     * Field _vlFolhaPagamentoEmpresa
     */
    private java.math.BigDecimal _vlFolhaPagamentoEmpresa = new java.math.BigDecimal("0");

    /**
     * Field _qtFuncionarioEmpresaPagadora
     */
    private long _qtFuncionarioEmpresaPagadora = 0;

    /**
     * keeps track of state for field: _qtFuncionarioEmpresaPagadora
     */
    private boolean _has_qtFuncionarioEmpresaPagadora;

    /**
     * Field _vlMediaSalarialEmpresa
     */
    private java.math.BigDecimal _vlMediaSalarialEmpresa = new java.math.BigDecimal("0");

    /**
     * Field _dtMesAnoCorrespondente
     */
    private int _dtMesAnoCorrespondente = 0;

    /**
     * keeps track of state for field: _dtMesAnoCorrespondente
     */
    private boolean _has_dtMesAnoCorrespondente;

    /**
     * Field _cdIndicadorPagamentoMes
     */
    private int _cdIndicadorPagamentoMes = 0;

    /**
     * keeps track of state for field: _cdIndicadorPagamentoMes
     */
    private boolean _has_cdIndicadorPagamentoMes;

    /**
     * Field _cdIndicadorPagamentoQuinzena
     */
    private int _cdIndicadorPagamentoQuinzena = 0;

    /**
     * keeps track of state for field: _cdIndicadorPagamentoQuinzena
     */
    private boolean _has_cdIndicadorPagamentoQuinzena;

    /**
     * Field _cdAberturaContaSalario
     */
    private int _cdAberturaContaSalario = 0;

    /**
     * keeps track of state for field: _cdAberturaContaSalario
     */
    private boolean _has_cdAberturaContaSalario;


      //----------------/
     //- Constructors -/
    //----------------/

    public DetalharFolhaResponse() 
     {
        super();
        setVlFolhaPagamentoEmpresa(new java.math.BigDecimal("0"));
        setVlMediaSalarialEmpresa(new java.math.BigDecimal("0"));
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharfolha.response.DetalharFolhaResponse()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdAberturaContaSalario
     * 
     */
    public void deleteCdAberturaContaSalario()
    {
        this._has_cdAberturaContaSalario= false;
    } //-- void deleteCdAberturaContaSalario() 

    /**
     * Method deleteCdIndicadorPagamentoMes
     * 
     */
    public void deleteCdIndicadorPagamentoMes()
    {
        this._has_cdIndicadorPagamentoMes= false;
    } //-- void deleteCdIndicadorPagamentoMes() 

    /**
     * Method deleteCdIndicadorPagamentoQuinzena
     * 
     */
    public void deleteCdIndicadorPagamentoQuinzena()
    {
        this._has_cdIndicadorPagamentoQuinzena= false;
    } //-- void deleteCdIndicadorPagamentoQuinzena() 

    /**
     * Method deleteDtMesAnoCorrespondente
     * 
     */
    public void deleteDtMesAnoCorrespondente()
    {
        this._has_dtMesAnoCorrespondente= false;
    } //-- void deleteDtMesAnoCorrespondente() 

    /**
     * Method deleteQtFuncionarioEmpresaPagadora
     * 
     */
    public void deleteQtFuncionarioEmpresaPagadora()
    {
        this._has_qtFuncionarioEmpresaPagadora= false;
    } //-- void deleteQtFuncionarioEmpresaPagadora() 

    /**
     * Returns the value of field 'cdAberturaContaSalario'.
     * 
     * @return int
     * @return the value of field 'cdAberturaContaSalario'.
     */
    public int getCdAberturaContaSalario()
    {
        return this._cdAberturaContaSalario;
    } //-- int getCdAberturaContaSalario() 

    /**
     * Returns the value of field 'cdIndicadorPagamentoMes'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorPagamentoMes'.
     */
    public int getCdIndicadorPagamentoMes()
    {
        return this._cdIndicadorPagamentoMes;
    } //-- int getCdIndicadorPagamentoMes() 

    /**
     * Returns the value of field 'cdIndicadorPagamentoQuinzena'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorPagamentoQuinzena'.
     */
    public int getCdIndicadorPagamentoQuinzena()
    {
        return this._cdIndicadorPagamentoQuinzena;
    } //-- int getCdIndicadorPagamentoQuinzena() 

    /**
     * Returns the value of field 'codMensagem'.
     * 
     * @return String
     * @return the value of field 'codMensagem'.
     */
    public java.lang.String getCodMensagem()
    {
        return this._codMensagem;
    } //-- java.lang.String getCodMensagem() 

    /**
     * Returns the value of field 'dtMesAnoCorrespondente'.
     * 
     * @return int
     * @return the value of field 'dtMesAnoCorrespondente'.
     */
    public int getDtMesAnoCorrespondente()
    {
        return this._dtMesAnoCorrespondente;
    } //-- int getDtMesAnoCorrespondente() 

    /**
     * Returns the value of field 'mensagem'.
     * 
     * @return String
     * @return the value of field 'mensagem'.
     */
    public java.lang.String getMensagem()
    {
        return this._mensagem;
    } //-- java.lang.String getMensagem() 

    /**
     * Returns the value of field 'qtFuncionarioEmpresaPagadora'.
     * 
     * @return long
     * @return the value of field 'qtFuncionarioEmpresaPagadora'.
     */
    public long getQtFuncionarioEmpresaPagadora()
    {
        return this._qtFuncionarioEmpresaPagadora;
    } //-- long getQtFuncionarioEmpresaPagadora() 

    /**
     * Returns the value of field 'vlFolhaPagamentoEmpresa'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlFolhaPagamentoEmpresa'.
     */
    public java.math.BigDecimal getVlFolhaPagamentoEmpresa()
    {
        return this._vlFolhaPagamentoEmpresa;
    } //-- java.math.BigDecimal getVlFolhaPagamentoEmpresa() 

    /**
     * Returns the value of field 'vlMediaSalarialEmpresa'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlMediaSalarialEmpresa'.
     */
    public java.math.BigDecimal getVlMediaSalarialEmpresa()
    {
        return this._vlMediaSalarialEmpresa;
    } //-- java.math.BigDecimal getVlMediaSalarialEmpresa() 

    /**
     * Method hasCdAberturaContaSalario
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAberturaContaSalario()
    {
        return this._has_cdAberturaContaSalario;
    } //-- boolean hasCdAberturaContaSalario() 

    /**
     * Method hasCdIndicadorPagamentoMes
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorPagamentoMes()
    {
        return this._has_cdIndicadorPagamentoMes;
    } //-- boolean hasCdIndicadorPagamentoMes() 

    /**
     * Method hasCdIndicadorPagamentoQuinzena
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorPagamentoQuinzena()
    {
        return this._has_cdIndicadorPagamentoQuinzena;
    } //-- boolean hasCdIndicadorPagamentoQuinzena() 

    /**
     * Method hasDtMesAnoCorrespondente
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasDtMesAnoCorrespondente()
    {
        return this._has_dtMesAnoCorrespondente;
    } //-- boolean hasDtMesAnoCorrespondente() 

    /**
     * Method hasQtFuncionarioEmpresaPagadora
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtFuncionarioEmpresaPagadora()
    {
        return this._has_qtFuncionarioEmpresaPagadora;
    } //-- boolean hasQtFuncionarioEmpresaPagadora() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdAberturaContaSalario'.
     * 
     * @param cdAberturaContaSalario the value of field
     * 'cdAberturaContaSalario'.
     */
    public void setCdAberturaContaSalario(int cdAberturaContaSalario)
    {
        this._cdAberturaContaSalario = cdAberturaContaSalario;
        this._has_cdAberturaContaSalario = true;
    } //-- void setCdAberturaContaSalario(int) 

    /**
     * Sets the value of field 'cdIndicadorPagamentoMes'.
     * 
     * @param cdIndicadorPagamentoMes the value of field
     * 'cdIndicadorPagamentoMes'.
     */
    public void setCdIndicadorPagamentoMes(int cdIndicadorPagamentoMes)
    {
        this._cdIndicadorPagamentoMes = cdIndicadorPagamentoMes;
        this._has_cdIndicadorPagamentoMes = true;
    } //-- void setCdIndicadorPagamentoMes(int) 

    /**
     * Sets the value of field 'cdIndicadorPagamentoQuinzena'.
     * 
     * @param cdIndicadorPagamentoQuinzena the value of field
     * 'cdIndicadorPagamentoQuinzena'.
     */
    public void setCdIndicadorPagamentoQuinzena(int cdIndicadorPagamentoQuinzena)
    {
        this._cdIndicadorPagamentoQuinzena = cdIndicadorPagamentoQuinzena;
        this._has_cdIndicadorPagamentoQuinzena = true;
    } //-- void setCdIndicadorPagamentoQuinzena(int) 

    /**
     * Sets the value of field 'codMensagem'.
     * 
     * @param codMensagem the value of field 'codMensagem'.
     */
    public void setCodMensagem(java.lang.String codMensagem)
    {
        this._codMensagem = codMensagem;
    } //-- void setCodMensagem(java.lang.String) 

    /**
     * Sets the value of field 'dtMesAnoCorrespondente'.
     * 
     * @param dtMesAnoCorrespondente the value of field
     * 'dtMesAnoCorrespondente'.
     */
    public void setDtMesAnoCorrespondente(int dtMesAnoCorrespondente)
    {
        this._dtMesAnoCorrespondente = dtMesAnoCorrespondente;
        this._has_dtMesAnoCorrespondente = true;
    } //-- void setDtMesAnoCorrespondente(int) 

    /**
     * Sets the value of field 'mensagem'.
     * 
     * @param mensagem the value of field 'mensagem'.
     */
    public void setMensagem(java.lang.String mensagem)
    {
        this._mensagem = mensagem;
    } //-- void setMensagem(java.lang.String) 

    /**
     * Sets the value of field 'qtFuncionarioEmpresaPagadora'.
     * 
     * @param qtFuncionarioEmpresaPagadora the value of field
     * 'qtFuncionarioEmpresaPagadora'.
     */
    public void setQtFuncionarioEmpresaPagadora(long qtFuncionarioEmpresaPagadora)
    {
        this._qtFuncionarioEmpresaPagadora = qtFuncionarioEmpresaPagadora;
        this._has_qtFuncionarioEmpresaPagadora = true;
    } //-- void setQtFuncionarioEmpresaPagadora(long) 

    /**
     * Sets the value of field 'vlFolhaPagamentoEmpresa'.
     * 
     * @param vlFolhaPagamentoEmpresa the value of field
     * 'vlFolhaPagamentoEmpresa'.
     */
    public void setVlFolhaPagamentoEmpresa(java.math.BigDecimal vlFolhaPagamentoEmpresa)
    {
        this._vlFolhaPagamentoEmpresa = vlFolhaPagamentoEmpresa;
    } //-- void setVlFolhaPagamentoEmpresa(java.math.BigDecimal) 

    /**
     * Sets the value of field 'vlMediaSalarialEmpresa'.
     * 
     * @param vlMediaSalarialEmpresa the value of field
     * 'vlMediaSalarialEmpresa'.
     */
    public void setVlMediaSalarialEmpresa(java.math.BigDecimal vlMediaSalarialEmpresa)
    {
        this._vlMediaSalarialEmpresa = vlMediaSalarialEmpresa;
    } //-- void setVlMediaSalarialEmpresa(java.math.BigDecimal) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return DetalharFolhaResponse
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.detalharfolha.response.DetalharFolhaResponse unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.detalharfolha.response.DetalharFolhaResponse) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.detalharfolha.response.DetalharFolhaResponse.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharfolha.response.DetalharFolhaResponse unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
