/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.excluirbloqueiorastreamento.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _nmControleRastreabilidadeTitulo
     */
    private int _nmControleRastreabilidadeTitulo = 0;

    /**
     * keeps track of state for field:
     * _nmControleRastreabilidadeTitulo
     */
    private boolean _has_nmControleRastreabilidadeTitulo;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.excluirbloqueiorastreamento.request.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteNmControleRastreabilidadeTitulo
     * 
     */
    public void deleteNmControleRastreabilidadeTitulo()
    {
        this._has_nmControleRastreabilidadeTitulo= false;
    } //-- void deleteNmControleRastreabilidadeTitulo() 

    /**
     * Returns the value of field
     * 'nmControleRastreabilidadeTitulo'.
     * 
     * @return int
     * @return the value of field 'nmControleRastreabilidadeTitulo'.
     */
    public int getNmControleRastreabilidadeTitulo()
    {
        return this._nmControleRastreabilidadeTitulo;
    } //-- int getNmControleRastreabilidadeTitulo() 

    /**
     * Method hasNmControleRastreabilidadeTitulo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNmControleRastreabilidadeTitulo()
    {
        return this._has_nmControleRastreabilidadeTitulo;
    } //-- boolean hasNmControleRastreabilidadeTitulo() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'nmControleRastreabilidadeTitulo'.
     * 
     * @param nmControleRastreabilidadeTitulo the value of field
     * 'nmControleRastreabilidadeTitulo'.
     */
    public void setNmControleRastreabilidadeTitulo(int nmControleRastreabilidadeTitulo)
    {
        this._nmControleRastreabilidadeTitulo = nmControleRastreabilidadeTitulo;
        this._has_nmControleRastreabilidadeTitulo = true;
    } //-- void setNmControleRastreabilidadeTitulo(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.excluirbloqueiorastreamento.request.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.excluirbloqueiorastreamento.request.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.excluirbloqueiorastreamento.request.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.excluirbloqueiorastreamento.request.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
