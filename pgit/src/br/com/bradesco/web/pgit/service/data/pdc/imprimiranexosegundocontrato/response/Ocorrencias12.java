/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.math.BigDecimal;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias12.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias12 implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _dsOpgTipoServico
     */
    private java.lang.String _dsOpgTipoServico;

    /**
     * Field _dsOpgTipoConsultaSaldo
     */
    private java.lang.String _dsOpgTipoConsultaSaldo;

    /**
     * Field _dsOpgTipoProcessamento
     */
    private java.lang.String _dsOpgTipoProcessamento;

    /**
     * Field _dsOpgTipoTratamentoFeriado
     */
    private java.lang.String _dsOpgTipoTratamentoFeriado;

    /**
     * Field _dsOpgTipoRejeicaoEfetivacao
     */
    private java.lang.String _dsOpgTipoRejeicaoEfetivacao;

    /**
     * Field _dsOpgTiporejeicaoAgendado
     */
    private java.lang.String _dsOpgTiporejeicaoAgendado;

    /**
     * Field _cdOpgIndicadorPercentualMaximo
     */
    private java.lang.String _cdOpgIndicadorPercentualMaximo;

    /**
     * Field _pcOpgMaximoInconsistencia
     */
    private int _pcOpgMaximoInconsistencia = 0;

    /**
     * keeps track of state for field: _pcOpgMaximoInconsistencia
     */
    private boolean _has_pcOpgMaximoInconsistencia;

    /**
     * Field _cdOpgIndicadorQuantidadeMaximo
     */
    private java.lang.String _cdOpgIndicadorQuantidadeMaximo;

    /**
     * Field _qtOpgMaximoInconsistencia
     */
    private int _qtOpgMaximoInconsistencia = 0;

    /**
     * keeps track of state for field: _qtOpgMaximoInconsistencia
     */
    private boolean _has_qtOpgMaximoInconsistencia;

    /**
     * Field _cdOpgIndicadorValorDia
     */
    private java.lang.String _cdOpgIndicadorValorDia;

    /**
     * Field _vlOpgLimiteDiario
     */
    private java.math.BigDecimal _vlOpgLimiteDiario = new java.math.BigDecimal("0");

    /**
     * Field _cdOpgIndicadorValorIndividual
     */
    private java.lang.String _cdOpgIndicadorValorIndividual;

    /**
     * Field _vlOpgLimiteIndividual
     */
    private java.math.BigDecimal _vlOpgLimiteIndividual = new java.math.BigDecimal("0");

    /**
     * Field _cdOpgIndicadorQuantidadeFloating
     */
    private java.lang.String _cdOpgIndicadorQuantidadeFloating;

    /**
     * Field _qtOpgDiaFloating
     */
    private int _qtOpgDiaFloating = 0;

    /**
     * keeps track of state for field: _qtOpgDiaFloating
     */
    private boolean _has_qtOpgDiaFloating;

    /**
     * Field _dsOpgPrioridadeDebito
     */
    private java.lang.String _dsOpgPrioridadeDebito;

    /**
     * Field _cdOpgIndicadorValorMaximo
     */
    private java.lang.String _cdOpgIndicadorValorMaximo;

    /**
     * Field _vlOpgMaximoFavorecidoNao
     */
    private java.math.BigDecimal _vlOpgMaximoFavorecidoNao = new java.math.BigDecimal("0");

    /**
     * Field _cdOpgIndicadorCadastroFavorecido
     */
    private java.lang.String _cdOpgIndicadorCadastroFavorecido;

    /**
     * Field _cdOpgUtilizacaoCadastroFavorecido
     */
    private java.lang.String _cdOpgUtilizacaoCadastroFavorecido;

    /**
     * Field _dsOpgOcorrenciasDebito
     */
    private java.lang.String _dsOpgOcorrenciasDebito;

    /**
     * Field _dsOpgTipoInscricao
     */
    private java.lang.String _dsOpgTipoInscricao;

    /**
     * Field _cdOpgIndicadorQuantidadeRepique
     */
    private java.lang.String _cdOpgIndicadorQuantidadeRepique;

    /**
     * Field _qtOpgDiaRepique
     */
    private int _qtOpgDiaRepique = 0;

    /**
     * keeps track of state for field: _qtOpgDiaRepique
     */
    private boolean _has_qtOpgDiaRepique;

    /**
     * Field _qtOpgDiaExpiracao
     */
    private int _qtOpgDiaExpiracao = 0;

    /**
     * keeps track of state for field: _qtOpgDiaExpiracao
     */
    private boolean _has_qtOpgDiaExpiracao;

    /**
     * Field _dsTipoConsFavorecido
     */
    private java.lang.String _dsTipoConsFavorecido;

    /**
     * Field _cdFeriLoc
     */
    private java.lang.String _cdFeriLoc;

    /**
     * Field _oPgEmissao
     */
    private java.lang.String _oPgEmissao;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias12() 
     {
        super();
        setVlOpgLimiteDiario(new java.math.BigDecimal("0"));
        setVlOpgLimiteIndividual(new java.math.BigDecimal("0"));
        setVlOpgMaximoFavorecidoNao(new java.math.BigDecimal("0"));
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias12()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deletePcOpgMaximoInconsistencia
     * 
     */
    public void deletePcOpgMaximoInconsistencia()
    {
        this._has_pcOpgMaximoInconsistencia= false;
    } //-- void deletePcOpgMaximoInconsistencia() 

    /**
     * Method deleteQtOpgDiaExpiracao
     * 
     */
    public void deleteQtOpgDiaExpiracao()
    {
        this._has_qtOpgDiaExpiracao= false;
    } //-- void deleteQtOpgDiaExpiracao() 

    /**
     * Method deleteQtOpgDiaFloating
     * 
     */
    public void deleteQtOpgDiaFloating()
    {
        this._has_qtOpgDiaFloating= false;
    } //-- void deleteQtOpgDiaFloating() 

    /**
     * Method deleteQtOpgDiaRepique
     * 
     */
    public void deleteQtOpgDiaRepique()
    {
        this._has_qtOpgDiaRepique= false;
    } //-- void deleteQtOpgDiaRepique() 

    /**
     * Method deleteQtOpgMaximoInconsistencia
     * 
     */
    public void deleteQtOpgMaximoInconsistencia()
    {
        this._has_qtOpgMaximoInconsistencia= false;
    } //-- void deleteQtOpgMaximoInconsistencia() 

    /**
     * Returns the value of field 'cdFeriLoc'.
     * 
     * @return String
     * @return the value of field 'cdFeriLoc'.
     */
    public java.lang.String getCdFeriLoc()
    {
        return this._cdFeriLoc;
    } //-- java.lang.String getCdFeriLoc() 

    /**
     * Returns the value of field
     * 'cdOpgIndicadorCadastroFavorecido'.
     * 
     * @return String
     * @return the value of field 'cdOpgIndicadorCadastroFavorecido'
     */
    public java.lang.String getCdOpgIndicadorCadastroFavorecido()
    {
        return this._cdOpgIndicadorCadastroFavorecido;
    } //-- java.lang.String getCdOpgIndicadorCadastroFavorecido() 

    /**
     * Returns the value of field 'cdOpgIndicadorPercentualMaximo'.
     * 
     * @return String
     * @return the value of field 'cdOpgIndicadorPercentualMaximo'.
     */
    public java.lang.String getCdOpgIndicadorPercentualMaximo()
    {
        return this._cdOpgIndicadorPercentualMaximo;
    } //-- java.lang.String getCdOpgIndicadorPercentualMaximo() 

    /**
     * Returns the value of field
     * 'cdOpgIndicadorQuantidadeFloating'.
     * 
     * @return String
     * @return the value of field 'cdOpgIndicadorQuantidadeFloating'
     */
    public java.lang.String getCdOpgIndicadorQuantidadeFloating()
    {
        return this._cdOpgIndicadorQuantidadeFloating;
    } //-- java.lang.String getCdOpgIndicadorQuantidadeFloating() 

    /**
     * Returns the value of field 'cdOpgIndicadorQuantidadeMaximo'.
     * 
     * @return String
     * @return the value of field 'cdOpgIndicadorQuantidadeMaximo'.
     */
    public java.lang.String getCdOpgIndicadorQuantidadeMaximo()
    {
        return this._cdOpgIndicadorQuantidadeMaximo;
    } //-- java.lang.String getCdOpgIndicadorQuantidadeMaximo() 

    /**
     * Returns the value of field
     * 'cdOpgIndicadorQuantidadeRepique'.
     * 
     * @return String
     * @return the value of field 'cdOpgIndicadorQuantidadeRepique'.
     */
    public java.lang.String getCdOpgIndicadorQuantidadeRepique()
    {
        return this._cdOpgIndicadorQuantidadeRepique;
    } //-- java.lang.String getCdOpgIndicadorQuantidadeRepique() 

    /**
     * Returns the value of field 'cdOpgIndicadorValorDia'.
     * 
     * @return String
     * @return the value of field 'cdOpgIndicadorValorDia'.
     */
    public java.lang.String getCdOpgIndicadorValorDia()
    {
        return this._cdOpgIndicadorValorDia;
    } //-- java.lang.String getCdOpgIndicadorValorDia() 

    /**
     * Returns the value of field 'cdOpgIndicadorValorIndividual'.
     * 
     * @return String
     * @return the value of field 'cdOpgIndicadorValorIndividual'.
     */
    public java.lang.String getCdOpgIndicadorValorIndividual()
    {
        return this._cdOpgIndicadorValorIndividual;
    } //-- java.lang.String getCdOpgIndicadorValorIndividual() 

    /**
     * Returns the value of field 'cdOpgIndicadorValorMaximo'.
     * 
     * @return String
     * @return the value of field 'cdOpgIndicadorValorMaximo'.
     */
    public java.lang.String getCdOpgIndicadorValorMaximo()
    {
        return this._cdOpgIndicadorValorMaximo;
    } //-- java.lang.String getCdOpgIndicadorValorMaximo() 

    /**
     * Returns the value of field
     * 'cdOpgUtilizacaoCadastroFavorecido'.
     * 
     * @return String
     * @return the value of field
     * 'cdOpgUtilizacaoCadastroFavorecido'.
     */
    public java.lang.String getCdOpgUtilizacaoCadastroFavorecido()
    {
        return this._cdOpgUtilizacaoCadastroFavorecido;
    } //-- java.lang.String getCdOpgUtilizacaoCadastroFavorecido() 

    /**
     * Returns the value of field 'dsOpgOcorrenciasDebito'.
     * 
     * @return String
     * @return the value of field 'dsOpgOcorrenciasDebito'.
     */
    public java.lang.String getDsOpgOcorrenciasDebito()
    {
        return this._dsOpgOcorrenciasDebito;
    } //-- java.lang.String getDsOpgOcorrenciasDebito() 

    /**
     * Returns the value of field 'dsOpgPrioridadeDebito'.
     * 
     * @return String
     * @return the value of field 'dsOpgPrioridadeDebito'.
     */
    public java.lang.String getDsOpgPrioridadeDebito()
    {
        return this._dsOpgPrioridadeDebito;
    } //-- java.lang.String getDsOpgPrioridadeDebito() 

    /**
     * Returns the value of field 'dsOpgTipoConsultaSaldo'.
     * 
     * @return String
     * @return the value of field 'dsOpgTipoConsultaSaldo'.
     */
    public java.lang.String getDsOpgTipoConsultaSaldo()
    {
        return this._dsOpgTipoConsultaSaldo;
    } //-- java.lang.String getDsOpgTipoConsultaSaldo() 

    /**
     * Returns the value of field 'dsOpgTipoInscricao'.
     * 
     * @return String
     * @return the value of field 'dsOpgTipoInscricao'.
     */
    public java.lang.String getDsOpgTipoInscricao()
    {
        return this._dsOpgTipoInscricao;
    } //-- java.lang.String getDsOpgTipoInscricao() 

    /**
     * Returns the value of field 'dsOpgTipoProcessamento'.
     * 
     * @return String
     * @return the value of field 'dsOpgTipoProcessamento'.
     */
    public java.lang.String getDsOpgTipoProcessamento()
    {
        return this._dsOpgTipoProcessamento;
    } //-- java.lang.String getDsOpgTipoProcessamento() 

    /**
     * Returns the value of field 'dsOpgTipoRejeicaoEfetivacao'.
     * 
     * @return String
     * @return the value of field 'dsOpgTipoRejeicaoEfetivacao'.
     */
    public java.lang.String getDsOpgTipoRejeicaoEfetivacao()
    {
        return this._dsOpgTipoRejeicaoEfetivacao;
    } //-- java.lang.String getDsOpgTipoRejeicaoEfetivacao() 

    /**
     * Returns the value of field 'dsOpgTipoServico'.
     * 
     * @return String
     * @return the value of field 'dsOpgTipoServico'.
     */
    public java.lang.String getDsOpgTipoServico()
    {
        return this._dsOpgTipoServico;
    } //-- java.lang.String getDsOpgTipoServico() 

    /**
     * Returns the value of field 'dsOpgTipoTratamentoFeriado'.
     * 
     * @return String
     * @return the value of field 'dsOpgTipoTratamentoFeriado'.
     */
    public java.lang.String getDsOpgTipoTratamentoFeriado()
    {
        return this._dsOpgTipoTratamentoFeriado;
    } //-- java.lang.String getDsOpgTipoTratamentoFeriado() 

    /**
     * Returns the value of field 'dsOpgTiporejeicaoAgendado'.
     * 
     * @return String
     * @return the value of field 'dsOpgTiporejeicaoAgendado'.
     */
    public java.lang.String getDsOpgTiporejeicaoAgendado()
    {
        return this._dsOpgTiporejeicaoAgendado;
    } //-- java.lang.String getDsOpgTiporejeicaoAgendado() 

    /**
     * Returns the value of field 'dsTipoConsFavorecido'.
     * 
     * @return String
     * @return the value of field 'dsTipoConsFavorecido'.
     */
    public java.lang.String getDsTipoConsFavorecido()
    {
        return this._dsTipoConsFavorecido;
    } //-- java.lang.String getDsTipoConsFavorecido() 

    /**
     * Returns the value of field 'oPgEmissao'.
     * 
     * @return String
     * @return the value of field 'oPgEmissao'.
     */
    public java.lang.String getOPgEmissao()
    {
        return this._oPgEmissao;
    } //-- java.lang.String getOPgEmissao() 

    /**
     * Returns the value of field 'pcOpgMaximoInconsistencia'.
     * 
     * @return int
     * @return the value of field 'pcOpgMaximoInconsistencia'.
     */
    public int getPcOpgMaximoInconsistencia()
    {
        return this._pcOpgMaximoInconsistencia;
    } //-- int getPcOpgMaximoInconsistencia() 

    /**
     * Returns the value of field 'qtOpgDiaExpiracao'.
     * 
     * @return int
     * @return the value of field 'qtOpgDiaExpiracao'.
     */
    public int getQtOpgDiaExpiracao()
    {
        return this._qtOpgDiaExpiracao;
    } //-- int getQtOpgDiaExpiracao() 

    /**
     * Returns the value of field 'qtOpgDiaFloating'.
     * 
     * @return int
     * @return the value of field 'qtOpgDiaFloating'.
     */
    public int getQtOpgDiaFloating()
    {
        return this._qtOpgDiaFloating;
    } //-- int getQtOpgDiaFloating() 

    /**
     * Returns the value of field 'qtOpgDiaRepique'.
     * 
     * @return int
     * @return the value of field 'qtOpgDiaRepique'.
     */
    public int getQtOpgDiaRepique()
    {
        return this._qtOpgDiaRepique;
    } //-- int getQtOpgDiaRepique() 

    /**
     * Returns the value of field 'qtOpgMaximoInconsistencia'.
     * 
     * @return int
     * @return the value of field 'qtOpgMaximoInconsistencia'.
     */
    public int getQtOpgMaximoInconsistencia()
    {
        return this._qtOpgMaximoInconsistencia;
    } //-- int getQtOpgMaximoInconsistencia() 

    /**
     * Returns the value of field 'vlOpgLimiteDiario'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlOpgLimiteDiario'.
     */
    public java.math.BigDecimal getVlOpgLimiteDiario()
    {
        return this._vlOpgLimiteDiario;
    } //-- java.math.BigDecimal getVlOpgLimiteDiario() 

    /**
     * Returns the value of field 'vlOpgLimiteIndividual'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlOpgLimiteIndividual'.
     */
    public java.math.BigDecimal getVlOpgLimiteIndividual()
    {
        return this._vlOpgLimiteIndividual;
    } //-- java.math.BigDecimal getVlOpgLimiteIndividual() 

    /**
     * Returns the value of field 'vlOpgMaximoFavorecidoNao'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlOpgMaximoFavorecidoNao'.
     */
    public java.math.BigDecimal getVlOpgMaximoFavorecidoNao()
    {
        return this._vlOpgMaximoFavorecidoNao;
    } //-- java.math.BigDecimal getVlOpgMaximoFavorecidoNao() 

    /**
     * Method hasPcOpgMaximoInconsistencia
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasPcOpgMaximoInconsistencia()
    {
        return this._has_pcOpgMaximoInconsistencia;
    } //-- boolean hasPcOpgMaximoInconsistencia() 

    /**
     * Method hasQtOpgDiaExpiracao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtOpgDiaExpiracao()
    {
        return this._has_qtOpgDiaExpiracao;
    } //-- boolean hasQtOpgDiaExpiracao() 

    /**
     * Method hasQtOpgDiaFloating
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtOpgDiaFloating()
    {
        return this._has_qtOpgDiaFloating;
    } //-- boolean hasQtOpgDiaFloating() 

    /**
     * Method hasQtOpgDiaRepique
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtOpgDiaRepique()
    {
        return this._has_qtOpgDiaRepique;
    } //-- boolean hasQtOpgDiaRepique() 

    /**
     * Method hasQtOpgMaximoInconsistencia
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtOpgMaximoInconsistencia()
    {
        return this._has_qtOpgMaximoInconsistencia;
    } //-- boolean hasQtOpgMaximoInconsistencia() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdFeriLoc'.
     * 
     * @param cdFeriLoc the value of field 'cdFeriLoc'.
     */
    public void setCdFeriLoc(java.lang.String cdFeriLoc)
    {
        this._cdFeriLoc = cdFeriLoc;
    } //-- void setCdFeriLoc(java.lang.String) 

    /**
     * Sets the value of field 'cdOpgIndicadorCadastroFavorecido'.
     * 
     * @param cdOpgIndicadorCadastroFavorecido the value of field
     * 'cdOpgIndicadorCadastroFavorecido'.
     */
    public void setCdOpgIndicadorCadastroFavorecido(java.lang.String cdOpgIndicadorCadastroFavorecido)
    {
        this._cdOpgIndicadorCadastroFavorecido = cdOpgIndicadorCadastroFavorecido;
    } //-- void setCdOpgIndicadorCadastroFavorecido(java.lang.String) 

    /**
     * Sets the value of field 'cdOpgIndicadorPercentualMaximo'.
     * 
     * @param cdOpgIndicadorPercentualMaximo the value of field
     * 'cdOpgIndicadorPercentualMaximo'.
     */
    public void setCdOpgIndicadorPercentualMaximo(java.lang.String cdOpgIndicadorPercentualMaximo)
    {
        this._cdOpgIndicadorPercentualMaximo = cdOpgIndicadorPercentualMaximo;
    } //-- void setCdOpgIndicadorPercentualMaximo(java.lang.String) 

    /**
     * Sets the value of field 'cdOpgIndicadorQuantidadeFloating'.
     * 
     * @param cdOpgIndicadorQuantidadeFloating the value of field
     * 'cdOpgIndicadorQuantidadeFloating'.
     */
    public void setCdOpgIndicadorQuantidadeFloating(java.lang.String cdOpgIndicadorQuantidadeFloating)
    {
        this._cdOpgIndicadorQuantidadeFloating = cdOpgIndicadorQuantidadeFloating;
    } //-- void setCdOpgIndicadorQuantidadeFloating(java.lang.String) 

    /**
     * Sets the value of field 'cdOpgIndicadorQuantidadeMaximo'.
     * 
     * @param cdOpgIndicadorQuantidadeMaximo the value of field
     * 'cdOpgIndicadorQuantidadeMaximo'.
     */
    public void setCdOpgIndicadorQuantidadeMaximo(java.lang.String cdOpgIndicadorQuantidadeMaximo)
    {
        this._cdOpgIndicadorQuantidadeMaximo = cdOpgIndicadorQuantidadeMaximo;
    } //-- void setCdOpgIndicadorQuantidadeMaximo(java.lang.String) 

    /**
     * Sets the value of field 'cdOpgIndicadorQuantidadeRepique'.
     * 
     * @param cdOpgIndicadorQuantidadeRepique the value of field
     * 'cdOpgIndicadorQuantidadeRepique'.
     */
    public void setCdOpgIndicadorQuantidadeRepique(java.lang.String cdOpgIndicadorQuantidadeRepique)
    {
        this._cdOpgIndicadorQuantidadeRepique = cdOpgIndicadorQuantidadeRepique;
    } //-- void setCdOpgIndicadorQuantidadeRepique(java.lang.String) 

    /**
     * Sets the value of field 'cdOpgIndicadorValorDia'.
     * 
     * @param cdOpgIndicadorValorDia the value of field
     * 'cdOpgIndicadorValorDia'.
     */
    public void setCdOpgIndicadorValorDia(java.lang.String cdOpgIndicadorValorDia)
    {
        this._cdOpgIndicadorValorDia = cdOpgIndicadorValorDia;
    } //-- void setCdOpgIndicadorValorDia(java.lang.String) 

    /**
     * Sets the value of field 'cdOpgIndicadorValorIndividual'.
     * 
     * @param cdOpgIndicadorValorIndividual the value of field
     * 'cdOpgIndicadorValorIndividual'.
     */
    public void setCdOpgIndicadorValorIndividual(java.lang.String cdOpgIndicadorValorIndividual)
    {
        this._cdOpgIndicadorValorIndividual = cdOpgIndicadorValorIndividual;
    } //-- void setCdOpgIndicadorValorIndividual(java.lang.String) 

    /**
     * Sets the value of field 'cdOpgIndicadorValorMaximo'.
     * 
     * @param cdOpgIndicadorValorMaximo the value of field
     * 'cdOpgIndicadorValorMaximo'.
     */
    public void setCdOpgIndicadorValorMaximo(java.lang.String cdOpgIndicadorValorMaximo)
    {
        this._cdOpgIndicadorValorMaximo = cdOpgIndicadorValorMaximo;
    } //-- void setCdOpgIndicadorValorMaximo(java.lang.String) 

    /**
     * Sets the value of field 'cdOpgUtilizacaoCadastroFavorecido'.
     * 
     * @param cdOpgUtilizacaoCadastroFavorecido the value of field
     * 'cdOpgUtilizacaoCadastroFavorecido'.
     */
    public void setCdOpgUtilizacaoCadastroFavorecido(java.lang.String cdOpgUtilizacaoCadastroFavorecido)
    {
        this._cdOpgUtilizacaoCadastroFavorecido = cdOpgUtilizacaoCadastroFavorecido;
    } //-- void setCdOpgUtilizacaoCadastroFavorecido(java.lang.String) 

    /**
     * Sets the value of field 'dsOpgOcorrenciasDebito'.
     * 
     * @param dsOpgOcorrenciasDebito the value of field
     * 'dsOpgOcorrenciasDebito'.
     */
    public void setDsOpgOcorrenciasDebito(java.lang.String dsOpgOcorrenciasDebito)
    {
        this._dsOpgOcorrenciasDebito = dsOpgOcorrenciasDebito;
    } //-- void setDsOpgOcorrenciasDebito(java.lang.String) 

    /**
     * Sets the value of field 'dsOpgPrioridadeDebito'.
     * 
     * @param dsOpgPrioridadeDebito the value of field
     * 'dsOpgPrioridadeDebito'.
     */
    public void setDsOpgPrioridadeDebito(java.lang.String dsOpgPrioridadeDebito)
    {
        this._dsOpgPrioridadeDebito = dsOpgPrioridadeDebito;
    } //-- void setDsOpgPrioridadeDebito(java.lang.String) 

    /**
     * Sets the value of field 'dsOpgTipoConsultaSaldo'.
     * 
     * @param dsOpgTipoConsultaSaldo the value of field
     * 'dsOpgTipoConsultaSaldo'.
     */
    public void setDsOpgTipoConsultaSaldo(java.lang.String dsOpgTipoConsultaSaldo)
    {
        this._dsOpgTipoConsultaSaldo = dsOpgTipoConsultaSaldo;
    } //-- void setDsOpgTipoConsultaSaldo(java.lang.String) 

    /**
     * Sets the value of field 'dsOpgTipoInscricao'.
     * 
     * @param dsOpgTipoInscricao the value of field
     * 'dsOpgTipoInscricao'.
     */
    public void setDsOpgTipoInscricao(java.lang.String dsOpgTipoInscricao)
    {
        this._dsOpgTipoInscricao = dsOpgTipoInscricao;
    } //-- void setDsOpgTipoInscricao(java.lang.String) 

    /**
     * Sets the value of field 'dsOpgTipoProcessamento'.
     * 
     * @param dsOpgTipoProcessamento the value of field
     * 'dsOpgTipoProcessamento'.
     */
    public void setDsOpgTipoProcessamento(java.lang.String dsOpgTipoProcessamento)
    {
        this._dsOpgTipoProcessamento = dsOpgTipoProcessamento;
    } //-- void setDsOpgTipoProcessamento(java.lang.String) 

    /**
     * Sets the value of field 'dsOpgTipoRejeicaoEfetivacao'.
     * 
     * @param dsOpgTipoRejeicaoEfetivacao the value of field
     * 'dsOpgTipoRejeicaoEfetivacao'.
     */
    public void setDsOpgTipoRejeicaoEfetivacao(java.lang.String dsOpgTipoRejeicaoEfetivacao)
    {
        this._dsOpgTipoRejeicaoEfetivacao = dsOpgTipoRejeicaoEfetivacao;
    } //-- void setDsOpgTipoRejeicaoEfetivacao(java.lang.String) 

    /**
     * Sets the value of field 'dsOpgTipoServico'.
     * 
     * @param dsOpgTipoServico the value of field 'dsOpgTipoServico'
     */
    public void setDsOpgTipoServico(java.lang.String dsOpgTipoServico)
    {
        this._dsOpgTipoServico = dsOpgTipoServico;
    } //-- void setDsOpgTipoServico(java.lang.String) 

    /**
     * Sets the value of field 'dsOpgTipoTratamentoFeriado'.
     * 
     * @param dsOpgTipoTratamentoFeriado the value of field
     * 'dsOpgTipoTratamentoFeriado'.
     */
    public void setDsOpgTipoTratamentoFeriado(java.lang.String dsOpgTipoTratamentoFeriado)
    {
        this._dsOpgTipoTratamentoFeriado = dsOpgTipoTratamentoFeriado;
    } //-- void setDsOpgTipoTratamentoFeriado(java.lang.String) 

    /**
     * Sets the value of field 'dsOpgTiporejeicaoAgendado'.
     * 
     * @param dsOpgTiporejeicaoAgendado the value of field
     * 'dsOpgTiporejeicaoAgendado'.
     */
    public void setDsOpgTiporejeicaoAgendado(java.lang.String dsOpgTiporejeicaoAgendado)
    {
        this._dsOpgTiporejeicaoAgendado = dsOpgTiporejeicaoAgendado;
    } //-- void setDsOpgTiporejeicaoAgendado(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoConsFavorecido'.
     * 
     * @param dsTipoConsFavorecido the value of field
     * 'dsTipoConsFavorecido'.
     */
    public void setDsTipoConsFavorecido(java.lang.String dsTipoConsFavorecido)
    {
        this._dsTipoConsFavorecido = dsTipoConsFavorecido;
    } //-- void setDsTipoConsFavorecido(java.lang.String) 

    /**
     * Sets the value of field 'oPgEmissao'.
     * 
     * @param oPgEmissao the value of field 'oPgEmissao'.
     */
    public void setOPgEmissao(java.lang.String oPgEmissao)
    {
        this._oPgEmissao = oPgEmissao;
    } //-- void setOPgEmissao(java.lang.String) 

    /**
     * Sets the value of field 'pcOpgMaximoInconsistencia'.
     * 
     * @param pcOpgMaximoInconsistencia the value of field
     * 'pcOpgMaximoInconsistencia'.
     */
    public void setPcOpgMaximoInconsistencia(int pcOpgMaximoInconsistencia)
    {
        this._pcOpgMaximoInconsistencia = pcOpgMaximoInconsistencia;
        this._has_pcOpgMaximoInconsistencia = true;
    } //-- void setPcOpgMaximoInconsistencia(int) 

    /**
     * Sets the value of field 'qtOpgDiaExpiracao'.
     * 
     * @param qtOpgDiaExpiracao the value of field
     * 'qtOpgDiaExpiracao'.
     */
    public void setQtOpgDiaExpiracao(int qtOpgDiaExpiracao)
    {
        this._qtOpgDiaExpiracao = qtOpgDiaExpiracao;
        this._has_qtOpgDiaExpiracao = true;
    } //-- void setQtOpgDiaExpiracao(int) 

    /**
     * Sets the value of field 'qtOpgDiaFloating'.
     * 
     * @param qtOpgDiaFloating the value of field 'qtOpgDiaFloating'
     */
    public void setQtOpgDiaFloating(int qtOpgDiaFloating)
    {
        this._qtOpgDiaFloating = qtOpgDiaFloating;
        this._has_qtOpgDiaFloating = true;
    } //-- void setQtOpgDiaFloating(int) 

    /**
     * Sets the value of field 'qtOpgDiaRepique'.
     * 
     * @param qtOpgDiaRepique the value of field 'qtOpgDiaRepique'.
     */
    public void setQtOpgDiaRepique(int qtOpgDiaRepique)
    {
        this._qtOpgDiaRepique = qtOpgDiaRepique;
        this._has_qtOpgDiaRepique = true;
    } //-- void setQtOpgDiaRepique(int) 

    /**
     * Sets the value of field 'qtOpgMaximoInconsistencia'.
     * 
     * @param qtOpgMaximoInconsistencia the value of field
     * 'qtOpgMaximoInconsistencia'.
     */
    public void setQtOpgMaximoInconsistencia(int qtOpgMaximoInconsistencia)
    {
        this._qtOpgMaximoInconsistencia = qtOpgMaximoInconsistencia;
        this._has_qtOpgMaximoInconsistencia = true;
    } //-- void setQtOpgMaximoInconsistencia(int) 

    /**
     * Sets the value of field 'vlOpgLimiteDiario'.
     * 
     * @param vlOpgLimiteDiario the value of field
     * 'vlOpgLimiteDiario'.
     */
    public void setVlOpgLimiteDiario(java.math.BigDecimal vlOpgLimiteDiario)
    {
        this._vlOpgLimiteDiario = vlOpgLimiteDiario;
    } //-- void setVlOpgLimiteDiario(java.math.BigDecimal) 

    /**
     * Sets the value of field 'vlOpgLimiteIndividual'.
     * 
     * @param vlOpgLimiteIndividual the value of field
     * 'vlOpgLimiteIndividual'.
     */
    public void setVlOpgLimiteIndividual(java.math.BigDecimal vlOpgLimiteIndividual)
    {
        this._vlOpgLimiteIndividual = vlOpgLimiteIndividual;
    } //-- void setVlOpgLimiteIndividual(java.math.BigDecimal) 

    /**
     * Sets the value of field 'vlOpgMaximoFavorecidoNao'.
     * 
     * @param vlOpgMaximoFavorecidoNao the value of field
     * 'vlOpgMaximoFavorecidoNao'.
     */
    public void setVlOpgMaximoFavorecidoNao(java.math.BigDecimal vlOpgMaximoFavorecidoNao)
    {
        this._vlOpgMaximoFavorecidoNao = vlOpgMaximoFavorecidoNao;
    } //-- void setVlOpgMaximoFavorecidoNao(java.math.BigDecimal) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias12
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias12 unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias12) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias12.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias12 unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
