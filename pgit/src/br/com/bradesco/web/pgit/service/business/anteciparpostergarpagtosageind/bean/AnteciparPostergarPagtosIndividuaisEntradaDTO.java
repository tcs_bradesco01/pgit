/*
 * Nome: br.com.bradesco.web.pgit.service.business.anteciparpostergarpagtosageind.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.anteciparpostergarpagtosageind.bean;

import java.util.ArrayList;
import java.util.List;

/**
 * Nome: AnteciparPostergarPagtosIndividuaisEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class AnteciparPostergarPagtosIndividuaisEntradaDTO {

	/** Atributo dtQuitacao. */
	private String dtQuitacao;

	/** Atributo listaOcorrencias. */
	private List<OcorrenciasAnteciparPostergarPagtosIndividuaisEntradaDTO> listaOcorrencias = new ArrayList<OcorrenciasAnteciparPostergarPagtosIndividuaisEntradaDTO>();

	/**
	 * Get: dtQuitacao.
	 *
	 * @return dtQuitacao
	 */
	public String getDtQuitacao() {
		return dtQuitacao;
	}

	/**
	 * Set: dtQuitacao.
	 *
	 * @param dtQuitacao the dt quitacao
	 */
	public void setDtQuitacao(String dtQuitacao) {
		this.dtQuitacao = dtQuitacao;
	}

	/**
	 * Get: listaOcorrencias.
	 *
	 * @return listaOcorrencias
	 */
	public List<OcorrenciasAnteciparPostergarPagtosIndividuaisEntradaDTO> getListaOcorrencias() {
		return listaOcorrencias;
	}

	/**
	 * Set: listaOcorrencias.
	 *
	 * @param listaOcorrencias the lista ocorrencias
	 */
	public void setListaOcorrencias(
			List<OcorrenciasAnteciparPostergarPagtosIndividuaisEntradaDTO> listaOcorrencias) {
		this.listaOcorrencias = listaOcorrencias;
	}
	
	

}
