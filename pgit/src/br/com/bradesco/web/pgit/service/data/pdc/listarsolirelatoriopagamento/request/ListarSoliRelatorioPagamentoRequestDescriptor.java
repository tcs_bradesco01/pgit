/*
 * Nome: br.com.bradesco.web.pgit.service.data.pdc.listarsolirelatoriopagamento.request
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */

package br.com.bradesco.web.pgit.service.data.pdc.listarsolirelatoriopagamento.request;

//---------------------------------/
//- Imported classes and packages -/
//---------------------------------/

import org.exolab.castor.xml.util.XMLClassDescriptorImpl;
import org.exolab.castor.xml.validators.IntegerValidator;
import org.exolab.castor.xml.validators.StringValidator;

/**
 * Nome: ListarSoliRelatorioPagamentoRequestDescriptor
 * <p>
 * Prop�sito:
 * </p>
 * 
 * @author : todo!
 * @see XMLClassDescriptorImpl
 * @version :
 */
public class ListarSoliRelatorioPagamentoRequestDescriptor extends org.exolab.castor.xml.util.XMLClassDescriptorImpl {

    // --------------------------/
    // - Class/Member Variables -/
    // --------------------------/

    /**
     * Field elementDefinition
     */
    private boolean elementDefinition;

    /**
     * Field nsPrefix
     */
    private java.lang.String nsPrefix;

    /**
     * Field nsURI
     */
    private java.lang.String nsURI;

    /**
     * Field xmlName
     */
    private java.lang.String xmlName;

    /**
     * Field identity
     */
    private org.exolab.castor.xml.XMLFieldDescriptor identity;

    // ----------------/
    // - Constructors -/
    // ----------------/

    /**
     * Instancia um novo ListarSoliRelatorioPagamentoRequestDescriptor
     * 
     * @exception
     * 
     */
    public ListarSoliRelatorioPagamentoRequestDescriptor() {
        super();
        xmlName = "listarSoliRelatorioPagamentoRequest";
        elementDefinition = true;

        // -- set grouping compositor
        setCompositorAsSequence();
        org.exolab.castor.xml.util.XMLFieldDescriptorImpl desc = null;
        org.exolab.castor.mapping.FieldHandler handler = null;
        org.exolab.castor.xml.FieldValidator fieldValidator = null;
        // -- initialize attribute descriptors

        // -- initialize element descriptors

        // -- _numeroOcorrencias
        desc =
            new org.exolab.castor.xml.util.XMLFieldDescriptorImpl(java.lang.Integer.TYPE, "_numeroOcorrencias",
                "numeroOcorrencias", org.exolab.castor.xml.NodeType.Element);
        handler = new org.exolab.castor.xml.XMLFieldHandler() {
            /**
             * (non-Javadoc)
             * 
             * @see org.exolab.castor.xml.XMLFieldHandler#getValue(java.lang.Object)
             */
            public java.lang.Object getValue(java.lang.Object object) throws IllegalStateException {
                ListarSoliRelatorioPagamentoRequest target = (ListarSoliRelatorioPagamentoRequest) object;
                if (!target.hasNumeroOcorrencias()) {
                    return null;
                }
                return new java.lang.Integer(target.getNumeroOcorrencias());
            }

            /**
             * (non-Javadoc)
             * 
             * @see org.exolab.castor.xml.XMLFieldHandler#setValue(java.lang.Object, java.lang.Object)
             */
            public void setValue(java.lang.Object object, java.lang.Object value) throws IllegalStateException,
                IllegalArgumentException {
                try {
                    ListarSoliRelatorioPagamentoRequest target = (ListarSoliRelatorioPagamentoRequest) object;
                    // ignore null values for non optional primitives
                    if (value == null){
                        return;
                    }
                    
                    target.setNumeroOcorrencias(((java.lang.Integer) value).intValue());
                } catch (java.lang.Exception ex) {
                    throw new IllegalStateException(ex.toString());
                }
            }

            /**
             * (non-Javadoc)
             * 
             * @see org.exolab.castor.xml.XMLFieldHandler#newInstance(java.lang.Object)
             */
            public java.lang.Object newInstance(java.lang.Object parent) {
                return null;
            }
        };
        desc.setHandler(handler);
        desc.setRequired(true);
        desc.setMultivalued(false);
        addFieldDescriptor(desc);

        // -- validation code for: _numeroOcorrencias
        fieldValidator = new org.exolab.castor.xml.FieldValidator();
        fieldValidator.setMinOccurs(1);
        { // -- local scope
            IntegerValidator typeValidator = new IntegerValidator();
            fieldValidator.setValidator(typeValidator);
        }
        desc.setValidator(fieldValidator);
        // -- _cdTipoSolicitacao
        desc =
            new org.exolab.castor.xml.util.XMLFieldDescriptorImpl(java.lang.Integer.TYPE, "_cdTipoSolicitacao",
                "cdTipoSolicitacao", org.exolab.castor.xml.NodeType.Element);
        handler = new org.exolab.castor.xml.XMLFieldHandler() {
            /**
             * (non-Javadoc)
             * 
             * @see org.exolab.castor.xml.XMLFieldHandler#getValue(java.lang.Object)
             */
            public java.lang.Object getValue(java.lang.Object object) throws IllegalStateException {
                ListarSoliRelatorioPagamentoRequest target = (ListarSoliRelatorioPagamentoRequest) object;
                if (!target.hasCdTipoSolicitacao()){
                    return null;
                }
                return new java.lang.Integer(target.getCdTipoSolicitacao());
            }

            /**
             * (non-Javadoc)
             * 
             * @see org.exolab.castor.xml.XMLFieldHandler#setValue(java.lang.Object, java.lang.Object)
             */
            public void setValue(java.lang.Object object, java.lang.Object value) throws IllegalStateException,
                IllegalArgumentException {
                try {
                    ListarSoliRelatorioPagamentoRequest target = (ListarSoliRelatorioPagamentoRequest) object;
                    // ignore null values for non optional primitives
                    if (value == null){
                        return;
                    }
                    
                    target.setCdTipoSolicitacao(((java.lang.Integer) value).intValue());
                } catch (java.lang.Exception ex) {
                    throw new IllegalStateException(ex.toString());
                }
            }

            /**
             * (non-Javadoc)
             * 
             * @see org.exolab.castor.xml.XMLFieldHandler#newInstance(java.lang.Object)
             */
            public java.lang.Object newInstance(java.lang.Object parent) {
                return null;
            }
        };
        desc.setHandler(handler);
        desc.setRequired(true);
        desc.setMultivalued(false);
        addFieldDescriptor(desc);

        // -- validation code for: _cdTipoSolicitacao
        fieldValidator = new org.exolab.castor.xml.FieldValidator();
        fieldValidator.setMinOccurs(1);
        { // -- local scope
            IntegerValidator typeValidator = new IntegerValidator();
            fieldValidator.setValidator(typeValidator);
        }
        desc.setValidator(fieldValidator);
        // -- _dtInicioSolicitacao
        desc =
            new org.exolab.castor.xml.util.XMLFieldDescriptorImpl(java.lang.String.class, "_dtInicioSolicitacao",
                "dtInicioSolicitacao", org.exolab.castor.xml.NodeType.Element);
        desc.setImmutable(true);
        handler = new org.exolab.castor.xml.XMLFieldHandler() {
            /**
             * (non-Javadoc)
             * 
             * @see org.exolab.castor.xml.XMLFieldHandler#getValue(java.lang.Object)
             */
            public java.lang.Object getValue(java.lang.Object object) throws IllegalStateException {
                ListarSoliRelatorioPagamentoRequest target = (ListarSoliRelatorioPagamentoRequest) object;
                return target.getDtInicioSolicitacao();
            }

            /**
             * (non-Javadoc)
             * 
             * @see org.exolab.castor.xml.XMLFieldHandler#setValue(java.lang.Object, java.lang.Object)
             */
            public void setValue(java.lang.Object object, java.lang.Object value) throws IllegalStateException,
                IllegalArgumentException {
                try {
                    ListarSoliRelatorioPagamentoRequest target = (ListarSoliRelatorioPagamentoRequest) object;
                    target.setDtInicioSolicitacao((java.lang.String) value);
                } catch (java.lang.Exception ex) {
                    throw new IllegalStateException(ex.toString());
                }
            }

            /**
             * (non-Javadoc)
             * 
             * @see org.exolab.castor.xml.XMLFieldHandler#newInstance(java.lang.Object)
             */
            public java.lang.Object newInstance(java.lang.Object parent) {
                return null;
            }
        };
        desc.setHandler(handler);
        desc.setRequired(true);
        desc.setMultivalued(false);
        addFieldDescriptor(desc);

        // -- validation code for: _dtInicioSolicitacao
        fieldValidator = new org.exolab.castor.xml.FieldValidator();
        fieldValidator.setMinOccurs(1);
        { // -- local scope
            StringValidator typeValidator = new StringValidator();
            typeValidator.setWhiteSpace("preserve");
            fieldValidator.setValidator(typeValidator);
        }
        desc.setValidator(fieldValidator);
        // -- _dtFimSolicitacao
        desc =
            new org.exolab.castor.xml.util.XMLFieldDescriptorImpl(java.lang.String.class, "_dtFimSolicitacao",
                "dtFimSolicitacao", org.exolab.castor.xml.NodeType.Element);
        desc.setImmutable(true);
        handler = new org.exolab.castor.xml.XMLFieldHandler() {
            /**
             * (non-Javadoc)
             * 
             * @see org.exolab.castor.xml.XMLFieldHandler#getValue(java.lang.Object)
             */
            public java.lang.Object getValue(java.lang.Object object) throws IllegalStateException {
                ListarSoliRelatorioPagamentoRequest target = (ListarSoliRelatorioPagamentoRequest) object;
                return target.getDtFimSolicitacao();
            }

            /**
             * (non-Javadoc)
             * 
             * @see org.exolab.castor.xml.XMLFieldHandler#setValue(java.lang.Object, java.lang.Object)
             */
            public void setValue(java.lang.Object object, java.lang.Object value) throws IllegalStateException,
                IllegalArgumentException {
                try {
                    ListarSoliRelatorioPagamentoRequest target = (ListarSoliRelatorioPagamentoRequest) object;
                    target.setDtFimSolicitacao((java.lang.String) value);
                } catch (java.lang.Exception ex) {
                    throw new IllegalStateException(ex.toString());
                }
            }

            /**
             * (non-Javadoc)
             * 
             * @see org.exolab.castor.xml.XMLFieldHandler#newInstance(java.lang.Object)
             */
            public java.lang.Object newInstance(java.lang.Object parent) {
                return null;
            }
        };
        desc.setHandler(handler);
        desc.setRequired(true);
        desc.setMultivalued(false);
        addFieldDescriptor(desc);

        // -- validation code for: _dtFimSolicitacao
        fieldValidator = new org.exolab.castor.xml.FieldValidator();
        fieldValidator.setMinOccurs(1);
        { // -- local scope
            StringValidator typeValidator = new StringValidator();
            typeValidator.setWhiteSpace("preserve");
            fieldValidator.setValidator(typeValidator);
        }
        desc.setValidator(fieldValidator);
    } 

    /**
     * Method getAccessMode
     * 
     * 
     * 
     * @return AccessMode
     */
    public org.exolab.castor.mapping.AccessMode getAccessMode() {
        return null;
    } // -- org.exolab.castor.mapping.AccessMode getAccessMode()

    /**
     * Method getExtends
     * 
     * 
     * 
     * @return ClassDescriptor
     */
    public org.exolab.castor.mapping.ClassDescriptor getExtends() {
        return null;
    } // -- org.exolab.castor.mapping.ClassDescriptor getExtends()

    /**
     * Method getIdentity
     * 
     * 
     * 
     * @return FieldDescriptor
     */
    public org.exolab.castor.mapping.FieldDescriptor getIdentity() {
        return identity;
    } // -- org.exolab.castor.mapping.FieldDescriptor getIdentity()

    /**
     * Method getJavaClass
     * 
     * 
     * 
     * @return Class
     */
    public java.lang.Class getJavaClass() {
        return br.com.bradesco.web.pgit.service.data.pdc.listarsolirelatoriopagamento.
                request.ListarSoliRelatorioPagamentoRequest.class;
    } // -- java.lang.Class getJavaClass()

    /**
     * Method getNameSpacePrefix
     * 
     * 
     * 
     * @return String
     */
    public java.lang.String getNameSpacePrefix() {
        return nsPrefix;
    } // -- java.lang.String getNameSpacePrefix()

    /**
     * Method getNameSpaceURI
     * 
     * 
     * 
     * @return String
     */
    public java.lang.String getNameSpaceURI() {
        return nsURI;
    } // -- java.lang.String getNameSpaceURI()

    /**
     * Method getValidator
     * 
     * 
     * 
     * @return TypeValidator
     */
    public org.exolab.castor.xml.TypeValidator getValidator() {
        return this;
    } // -- org.exolab.castor.xml.TypeValidator getValidator()

    /**
     * Method getXMLName
     * 
     * 
     * 
     * @return String
     */
    public java.lang.String getXMLName() {
        return xmlName;
    } // -- java.lang.String getXMLName()

    /**
     * Method isElementDefinition
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isElementDefinition() {
        return elementDefinition;
    } // -- boolean isElementDefinition()

}
