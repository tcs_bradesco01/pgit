/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/implementation.ftl,v $
 * $Id: implementation.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: implementation.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */
 
package br.com.bradesco.web.pgit.service.business.regsegregacessodados.impl;

import java.util.ArrayList;
import java.util.List;

import br.com.bradesco.web.pgit.service.business.regsegregacessodados.IRegSegAcessoDadosConstants;
import br.com.bradesco.web.pgit.service.business.regsegregacessodados.IRegSegregAcessoDadosService;
import br.com.bradesco.web.pgit.service.business.regsegregacessodados.bean.AlterarExcRegraSegregacaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.regsegregacessodados.bean.AlterarExcRegraSegregacaoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.regsegregacessodados.bean.AlterarRegraSegregacaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.regsegregacessodados.bean.AlterarRegraSegregacaoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.regsegregacessodados.bean.DetalharExcRegraSegregacaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.regsegregacessodados.bean.DetalharExcRegraSegregacaoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.regsegregacessodados.bean.DetalharRegraSegregacaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.regsegregacessodados.bean.DetalharRegraSegregacaoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.regsegregacessodados.bean.ExcluirExcRegraSegregacaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.regsegregacessodados.bean.ExcluirExcRegraSegregacaoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.regsegregacessodados.bean.ExcluirRegraSegregacaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.regsegregacessodados.bean.ExcluirRegraSegregacaoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.regsegregacessodados.bean.IncluirExcRegraSegregacaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.regsegregacessodados.bean.IncluirExcRegraSegregacaoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.regsegregacessodados.bean.IncluirRegraSegregacaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.regsegregacessodados.bean.IncluirRegraSegregacaoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.regsegregacessodados.bean.ListarExcRegraSegregacaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.regsegregacessodados.bean.ListarExcRegraSegregacaoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.regsegregacessodados.bean.ListarRegraSegregacaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.regsegregacessodados.bean.ListarRegraSegregacaoSaidaDTO;
import br.com.bradesco.web.pgit.service.data.pdc.FactoryAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.alterarexcecaoregrasegregacaoacesso.request.AlterarExcecaoRegraSegregacaoAcessoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.alterarexcecaoregrasegregacaoacesso.response.AlterarExcecaoRegraSegregacaoAcessoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.alterarregrasegregacaoacesso.request.AlterarRegraSegregacaoAcessoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.alterarregrasegregacaoacesso.response.AlterarRegraSegregacaoAcessoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.detalharexcecaoregrasegregacaoacesso.request.DetalhaExcecaoRegraSegregacaoAcessoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.detalharexcecaoregrasegregacaoacesso.response.DetalhaExcecaoRegraSegregacaoAcessoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.detalharregrasegregacaoacesso.request.DetalharRegraSegregacaoAcessoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.detalharregrasegregacaoacesso.response.DetalharRegraSegregacaoAcessoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.excluirexcecaoregrasegregacaoacesso.request.ExcluirExcecaoRegraSegregacaoAcessoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.excluirexcecaoregrasegregacaoacesso.response.ExcluirExcecaoRegraSegregacaoAcessoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.excluirregrasegregacaoacesso.request.ExcluirRegraSegregacaoAcessoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.excluirregrasegregacaoacesso.response.ExcluirRegraSegregacaoAcessoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.incluirexcecaoregrasegregacaoacesso.request.IncluirExcecaoRegraSegregacaoAcessoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.incluirexcecaoregrasegregacaoacesso.response.IncluirExcecaoRegraSegregacaoAcessoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.incluirregrasegregacaoacesso.request.IncluirRegraSegregacaoAcessoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.incluirregrasegregacaoacesso.response.IncluirRegraSegregacaoAcessoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarexcecaoregrasegregacaoacesso.request.ListarExcecaoRegraSegregacaoAcessoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarexcecaoregrasegregacaoacesso.response.ListarExcecaoRegraSegregacaoAcessoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarregrasegregacaoacesso.request.ListarRegraSegregacaoAcessoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarregrasegregacaoacesso.response.ListarRegraSegregacaoAcessoResponse;


/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Implementa��o do adaptador: ManterRegraSegregacaoAcessoDados
 * </p>
 * 
 * @comment C�DIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public class RegSegregAcessoDadosServiceImpl implements IRegSegregAcessoDadosService {
	
	/** Atributo factoryAdapter. */
	private FactoryAdapter factoryAdapter;

    /**
     * Construtor.
     */
    public RegSegregAcessoDadosServiceImpl() {
        // TODO: Implementa��o
    }
    
    /**
     * M�todo de exemplo.
     *
     * @see br.com.bradesco.web.pgit.service.business.regsegregacessodados.IManterRegraSegregacaoAcessoDados#sampleManterRegraSegregacaoAcessoDados()
     */
    public void sampleManterRegraSegregacaoAcessoDados() {
        // TODO: Implementa�ao
    }

	/**
	 * Get: factoryAdapter.
	 *
	 * @return factoryAdapter
	 */
	public FactoryAdapter getFactoryAdapter() {
		return factoryAdapter;
	}

	/**
	 * Set: factoryAdapter.
	 *
	 * @param factoryAdapter the factory adapter
	 */
	public void setFactoryAdapter(FactoryAdapter factoryAdapter) {
		this.factoryAdapter = factoryAdapter;
	}
	
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.regsegregacessodados.IRegSegregAcessoDadosService#listaRegraSegregacao(br.com.bradesco.web.pgit.service.business.regsegregacessodados.bean.ListarRegraSegregacaoEntradaDTO)
	 */
	public List<ListarRegraSegregacaoSaidaDTO> listaRegraSegregacao (ListarRegraSegregacaoEntradaDTO listarRegraSegregacaoEntradaDTO) {
		
		List<ListarRegraSegregacaoSaidaDTO> listaRetorno = new ArrayList<ListarRegraSegregacaoSaidaDTO>();
		ListarRegraSegregacaoAcessoRequest request = new ListarRegraSegregacaoAcessoRequest();
		ListarRegraSegregacaoAcessoResponse response = new ListarRegraSegregacaoAcessoResponse();
		
		request.setCdRegraAcessoDado(listarRegraSegregacaoEntradaDTO.getCodigoRegra());
		request.setCdTipoUnidadeUsuario(listarRegraSegregacaoEntradaDTO.getTipoUnidadeOrganizacionalUsuario());
		request.setCdIndicadorAcao(listarRegraSegregacaoEntradaDTO.getTipoAcao());
		request.setCdNivelPermissaoAcesso(listarRegraSegregacaoEntradaDTO.getNivelPermissaoAcessoDados());
		request.setQtConsultas(IRegSegAcessoDadosConstants.QTDE_CONSULTAS_LISTAR);
		
		response = getFactoryAdapter().getListarRegraSegregacaoAcessoPDCAdapter().invokeProcess(request);
		
		ListarRegraSegregacaoSaidaDTO saidaDTO;
		
		for (int i=0; i<response.getOcorrenciasCount();i++){
			saidaDTO = new ListarRegraSegregacaoSaidaDTO();
			saidaDTO.setCdMensagem(response.getCodMensagem());
			saidaDTO.setMensagem(response.getMensagem());
			saidaDTO.setCodigoRegra(response.getOcorrencias(i).getCdRegraAcessoDado());
			saidaDTO.setCdTipoUnidadeOrganizacionalUsuario(response.getOcorrencias(i).getCdTipoUnidadeUsuario());
			saidaDTO.setDsTipoUnidadeOrganizacionalUsuario(response.getOcorrencias(i).getDsTipoUnidadeUsuario());
			saidaDTO.setCdTipoUnidadeOrganizacionalProprietario(response.getOcorrencias(i).getCdTipoUnidadeProprietario());
			saidaDTO.setDsTipoUnidadeOrganizacionalProprietario(response.getOcorrencias(i).getDsTipoUnidadeProprietario());
			saidaDTO.setTipoAcao(response.getOcorrencias(i).getCdIndicadorAcao());
			saidaDTO.setNivelPermissao(response.getOcorrencias(i).getCdNivelPermissaoAcesso());
			
			listaRetorno.add(saidaDTO);
		}
		
		return listaRetorno;
	}
	
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.regsegregacessodados.IRegSegregAcessoDadosService#listarExcRegraSegregacao(br.com.bradesco.web.pgit.service.business.regsegregacessodados.bean.ListarExcRegraSegregacaoEntradaDTO)
	 */
	public List<ListarExcRegraSegregacaoSaidaDTO> listarExcRegraSegregacao (ListarExcRegraSegregacaoEntradaDTO listarExcSegregacaoAcessoEntradaDTO) {
		
		List<ListarExcRegraSegregacaoSaidaDTO> listaRetorno = new ArrayList<ListarExcRegraSegregacaoSaidaDTO>();
		ListarExcecaoRegraSegregacaoAcessoRequest request = new ListarExcecaoRegraSegregacaoAcessoRequest();
		ListarExcecaoRegraSegregacaoAcessoResponse response = new ListarExcecaoRegraSegregacaoAcessoResponse();
		
		request.setCdRegraAcessoDado(listarExcSegregacaoAcessoEntradaDTO.getCdRegraAcessoDado());
		request.setCdExcecaoAcessoDado(listarExcSegregacaoAcessoEntradaDTO.getCdExcecaoAcessoDado());
		request.setCdTipoUnidadeUsuario(listarExcSegregacaoAcessoEntradaDTO.getTipoUnidadeOrganizacionalUsuario());
		request.setCdPessoaJuridicaUsuario(listarExcSegregacaoAcessoEntradaDTO.getEmpresa());
		request.setNrUnidadeOrganizacionalUsuario(listarExcSegregacaoAcessoEntradaDTO.getUnidadeOrganizacional());
		request.setCdTipoExcecaoAcesso(listarExcSegregacaoAcessoEntradaDTO.getTipoExcecao());
		request.setCdTipoAbrangenciaExcecao(listarExcSegregacaoAcessoEntradaDTO.getTipoAbrangenciaAcesso());
		request.setQtConsultas(IRegSegAcessoDadosConstants.QTDE_CONSULTAS_LISTAR_EXC);
		
		
		response = getFactoryAdapter().getListarExcecaoRegraSegregacaoAcessoPDCAdapter().invokeProcess(request);
		
		ListarExcRegraSegregacaoSaidaDTO saidaDTO;
	
		for(int i=0; i<response.getOcorrenciasCount(); i++){
			saidaDTO = new ListarExcRegraSegregacaoSaidaDTO();
			saidaDTO.setCodMensagem(response.getCodMensagem());
			saidaDTO.setMensagem(response.getMensagem());
			saidaDTO.setCdRegra(response.getOcorrencias(i).getCdRegraAcessoDado());
			saidaDTO.setCdExcecao(response.getOcorrencias(i).getCdExcecaoAcessoDado());
			saidaDTO.setCdTipoUnidadeOrganizacionalUsuario(response.getOcorrencias(i).getCdTipoUnidadeUsuario());
			saidaDTO.setDsTipoUnidadeOrganizacionalUsuario(response.getOcorrencias(i).getDsTipoUnidadeUsuario());
			saidaDTO.setEmpresa(response.getOcorrencias(i).getCdPessoaJuridicaUsuario());
			saidaDTO.setCdUnidadeOrganizacionalUsuario(response.getOcorrencias(i).getNrUnidadeOrganizacionalUsuario());
			saidaDTO.setDsUnidadeOrganizacionalUsuario(response.getOcorrencias(i).getDsUnidadeOrganizaciolUsuario());
			saidaDTO.setTipoExcecao(response.getOcorrencias(i).getCdTipoExcecaoAcesso());
			saidaDTO.setTipoAbrangenciaExcecao(response.getOcorrencias(i).getCdTipoAbrangenciaExcecao());
			
			listaRetorno.add(saidaDTO);
		}
		
		return listaRetorno;
	}
	
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.regsegregacessodados.IRegSegregAcessoDadosService#incluirRegraSegregacao(br.com.bradesco.web.pgit.service.business.regsegregacessodados.bean.IncluirRegraSegregacaoEntradaDTO)
	 */
	public IncluirRegraSegregacaoSaidaDTO incluirRegraSegregacao (IncluirRegraSegregacaoEntradaDTO incluirRegraSegregacaoEntradaDTO) {
		
		IncluirRegraSegregacaoSaidaDTO incluirRegraSegregacaoSaidaDTO = new IncluirRegraSegregacaoSaidaDTO();
		IncluirRegraSegregacaoAcessoRequest incluirRegraSegregacaoAcessoRequest = new IncluirRegraSegregacaoAcessoRequest();
		IncluirRegraSegregacaoAcessoResponse incluirRegraSegregacaoAcessoResponse = new IncluirRegraSegregacaoAcessoResponse();
		
		incluirRegraSegregacaoAcessoRequest.setCdRegraAcessoDado(0);
		incluirRegraSegregacaoAcessoRequest.setCdTipoUnidadeUsuario(incluirRegraSegregacaoEntradaDTO.getTipoUnidadeOrganizacionalUsuario());
		incluirRegraSegregacaoAcessoRequest.setCdTipoUnidadeProprietario(incluirRegraSegregacaoEntradaDTO.getTipoUnidadeOrganizacionalProprietarioDados());
		incluirRegraSegregacaoAcessoRequest.setCdIndicadorAcao(incluirRegraSegregacaoEntradaDTO.getTipoAcao());
		incluirRegraSegregacaoAcessoRequest.setCdNivelPermissaoAcesso(incluirRegraSegregacaoEntradaDTO.getNivelPermissaoDados());
		
		incluirRegraSegregacaoAcessoResponse = getFactoryAdapter().getIncluirRegraSegregacaoAcessoPDCAdapter().invokeProcess(incluirRegraSegregacaoAcessoRequest);
		
		incluirRegraSegregacaoSaidaDTO.setCdMensagem(incluirRegraSegregacaoAcessoResponse.getCodMensagem());
		incluirRegraSegregacaoSaidaDTO.setMensagem(incluirRegraSegregacaoAcessoResponse.getMensagem());
		
		return incluirRegraSegregacaoSaidaDTO;
	}
	
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.regsegregacessodados.IRegSegregAcessoDadosService#incluirExcRegraSegregacao(br.com.bradesco.web.pgit.service.business.regsegregacessodados.bean.IncluirExcRegraSegregacaoEntradaDTO)
	 */
	public IncluirExcRegraSegregacaoSaidaDTO incluirExcRegraSegregacao (IncluirExcRegraSegregacaoEntradaDTO incluirRegraSegregacaoAcessoEntradaDTO) {
		
		IncluirExcRegraSegregacaoSaidaDTO incluirExcRegraSegregacaoSaidaDTO = new IncluirExcRegraSegregacaoSaidaDTO();
		IncluirExcecaoRegraSegregacaoAcessoRequest incluirExcecaoRegraSegregacaoAcessoRequest = new IncluirExcecaoRegraSegregacaoAcessoRequest();
		IncluirExcecaoRegraSegregacaoAcessoResponse incluirExcecaoRegraSegregacaoAcessoResponse = new IncluirExcecaoRegraSegregacaoAcessoResponse();
		
		incluirExcecaoRegraSegregacaoAcessoRequest.setCdRegraAcessoDado(incluirRegraSegregacaoAcessoEntradaDTO.getCodigoRegra());
		incluirExcecaoRegraSegregacaoAcessoRequest.setCdExcecaoAcessoDado(0);
		incluirExcecaoRegraSegregacaoAcessoRequest.setCdTipoUnidadeUsuario(incluirRegraSegregacaoAcessoEntradaDTO.getTipoUnidadeOrganizacionalUsuario());
		incluirExcecaoRegraSegregacaoAcessoRequest.setCdPessoaJuridicaUsuario(incluirRegraSegregacaoAcessoEntradaDTO.getEmpresaConglomeradoUsuario());
		incluirExcecaoRegraSegregacaoAcessoRequest.setNrUnidadeOrganizacionalUsuario(incluirRegraSegregacaoAcessoEntradaDTO.getUnidadeOrganizacionalUsuario());
		incluirExcecaoRegraSegregacaoAcessoRequest.setCdTipoUnidadeProprietario(incluirRegraSegregacaoAcessoEntradaDTO.getTipoUnidadeOrganizacionalProprietario());
		incluirExcecaoRegraSegregacaoAcessoRequest.setCdPessoaJuridicaProprietario(incluirRegraSegregacaoAcessoEntradaDTO.getEmpresaConglomeradoProprietario());
		incluirExcecaoRegraSegregacaoAcessoRequest.setNrUnidadeOrganizacionalProprietario(incluirRegraSegregacaoAcessoEntradaDTO.getUnidadeOrganizacionalProprietario());
		incluirExcecaoRegraSegregacaoAcessoRequest.setCdTipoExcecaoAcesso(incluirRegraSegregacaoAcessoEntradaDTO.getTipoExcecao());
		incluirExcecaoRegraSegregacaoAcessoRequest.setCdTipoAbrangenciaExcecao(incluirRegraSegregacaoAcessoEntradaDTO.getTipoAbrangenciaExcecao());

		incluirExcecaoRegraSegregacaoAcessoResponse = getFactoryAdapter().getIncluirExcecaoRegraSegregacaoAcessoPDCAdapter().invokeProcess(incluirExcecaoRegraSegregacaoAcessoRequest);
		
		incluirExcRegraSegregacaoSaidaDTO.setCodMensagem(incluirExcecaoRegraSegregacaoAcessoResponse.getCodMensagem());
		incluirExcRegraSegregacaoSaidaDTO.setMensagem(incluirExcecaoRegraSegregacaoAcessoResponse.getMensagem());
		
		return incluirExcRegraSegregacaoSaidaDTO;
	}
	
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.regsegregacessodados.IRegSegregAcessoDadosService#detalharRegraSegregacao(br.com.bradesco.web.pgit.service.business.regsegregacessodados.bean.DetalharRegraSegregacaoEntradaDTO)
	 */
	public DetalharRegraSegregacaoSaidaDTO detalharRegraSegregacao (DetalharRegraSegregacaoEntradaDTO detalharRegraSegregacaoEntradaDTO) {
		
		DetalharRegraSegregacaoSaidaDTO saidaDTO = new DetalharRegraSegregacaoSaidaDTO();
		DetalharRegraSegregacaoAcessoRequest request = new DetalharRegraSegregacaoAcessoRequest();
		DetalharRegraSegregacaoAcessoResponse response = new DetalharRegraSegregacaoAcessoResponse();
		
		request.setCdRegraAcessoDado(detalharRegraSegregacaoEntradaDTO.getCdRegraAcessoDado());
		request.setQtConsultas(0);
	
		response = getFactoryAdapter().getDetalharRegraSegregacaoAcessoPDCAdapter().invokeProcess(request);
		
		saidaDTO = new DetalharRegraSegregacaoSaidaDTO();
		saidaDTO.setCodMensangem(response.getCodMensagem());
		saidaDTO.setMensagem(response.getMensagem());
		saidaDTO.setCdRegraAcessoDado(response.getOcorrencias(0).getCdRegraAcessoDado());
		saidaDTO.setCdTipoUnidadeOrganizacionalUsuario(response.getOcorrencias(0).getCdTipoUnidadeUsuario());
		saidaDTO.setDsTipoUnidadeOrganizacionalUsuario(response.getOcorrencias(0).getDsTipoUnidadeUsuario());
		saidaDTO.setCdTipoUnidadeOrganizacionalProprietarioDados(response.getOcorrencias(0).getCdTipoUnidadeProprietario());
		saidaDTO.setDsTipoUnidadeOrganizacionalProprietarioDados(response.getOcorrencias(0).getDsTipoUnidadeProprietario());
		saidaDTO.setTipoAcao(response.getOcorrencias(0).getCdIndicadorAcao());
		saidaDTO.setNivelPermissaoAcessoAosDados(response.getOcorrencias(0).getCdNivelPermissaoAcesso());
		saidaDTO.setCdCanalInclusao(response.getOcorrencias(0).getCdCanalInclusao());
		saidaDTO.setDsCanalInclusao(response.getOcorrencias(0).getDsCanalInclusao());
		saidaDTO.setUsuarioInclusao(response.getOcorrencias(0).getCdAutenSegrcInclusao());
		saidaDTO.setComplementoInclusao(response.getOcorrencias(0).getNmOperFluxoInclusao());
		saidaDTO.setDataHoraInclusao(response.getOcorrencias(0).getHrInclusaoRegistro());
		saidaDTO.setCdCanalManutencao(response.getOcorrencias(0).getCdCanalManutencao());
		saidaDTO.setDsCanalManutencao(response.getOcorrencias(0).getDsCanalManutencao());
		saidaDTO.setUsuarioManutencao(response.getOcorrencias(0).getCdAutenSegrcManutencao());
		saidaDTO.setComplementoManutencao(response.getOcorrencias(0).getNmOperFluxoManutencao());
		saidaDTO.setDataHoraManutencao(response.getOcorrencias(0).getHrManutencaoRegistro());
		
		return saidaDTO;
	}
	
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.regsegregacessodados.IRegSegregAcessoDadosService#detalharExcRegraSegregacao(br.com.bradesco.web.pgit.service.business.regsegregacessodados.bean.DetalharExcRegraSegregacaoEntradaDTO)
	 */
	public DetalharExcRegraSegregacaoSaidaDTO detalharExcRegraSegregacao (DetalharExcRegraSegregacaoEntradaDTO detalharExcRegraSegregacaoEntradaDTO) {
		
		DetalharExcRegraSegregacaoSaidaDTO saidaDTO = new DetalharExcRegraSegregacaoSaidaDTO();
		DetalhaExcecaoRegraSegregacaoAcessoRequest request = new DetalhaExcecaoRegraSegregacaoAcessoRequest();
		DetalhaExcecaoRegraSegregacaoAcessoResponse response = new DetalhaExcecaoRegraSegregacaoAcessoResponse();
		
		request.setCdRegraAcessoDado(detalharExcRegraSegregacaoEntradaDTO.getCodigoRegra());
		request.setCdExcecaoAcessoDado(detalharExcRegraSegregacaoEntradaDTO.getCodigoExcecao());

		response = getFactoryAdapter().getDetalharExcecaoRegraSegregacaoAcessoPDCAdapter().invokeProcess(request);
		
		saidaDTO = new DetalharExcRegraSegregacaoSaidaDTO();
		saidaDTO.setCodMensagem(response.getCodMensagem());
		saidaDTO.setMensagem(response.getMensagem());
		saidaDTO.setCodigoRegra(response.getOcorrencias(0).getCdRegraAcessoDado());
		saidaDTO.setCodigoExcecao(response.getOcorrencias(0).getCdExcecaoAcessoDado());
		saidaDTO.setCdTipoUnidadeOrganizacionalUsuario(response.getOcorrencias(0).getCdTipoUnidadeUsuario());
		saidaDTO.setDsTipoUnidadeOrganizacionalUsuario(response.getOcorrencias(0).getDsTipoUnidadeUsuario());
		saidaDTO.setCdEmpresaConglomeradoUsuario(response.getOcorrencias(0).getCdPessoaJuridicaUsuario());
		saidaDTO.setDsEmpresaConglomeradoUsuario(response.getOcorrencias(0).getDsPessoaJuridicaUsuario());
		saidaDTO.setCdUnidadeOrganizacionalUsuario(response.getOcorrencias(0).getNrUnidadeOrganizacional());
		saidaDTO.setDsUnidadeOrganizacionalUsuario(response.getOcorrencias(0).getDsUnidadeOrganizacional());
		saidaDTO.setCdTipoUnidadeOrganizacionalProprietario(response.getOcorrencias(0).getCdTipoUnidadeProprietario());
		saidaDTO.setDsTipoUnidadeOrganizacionalProprietario(response.getOcorrencias(0).getDsTipoUnidadeProprietario());
		saidaDTO.setCdEmpresaConglomeradoProprietario(response.getOcorrencias(0).getCdPessoaJuridicaProprietario());
		saidaDTO.setDsEmpresaConglomeradoProprietario(response.getOcorrencias(0).getDsPessoaJuridicaProprietario());
		saidaDTO.setCdUnidadeOrganizacionalProprietario(response.getOcorrencias(0).getNrUnidadeOrganizacionalProprietario());
		saidaDTO.setDsUnidadeOrganizacionalProprietario(response.getOcorrencias(0).getDsUnidadeOrganizacionalProprietario());
		saidaDTO.setTipoExcecao(response.getOcorrencias(0).getCdTipoExcecaoAcesso());
		saidaDTO.setTipoAbrangenciaExcecao(response.getOcorrencias(0).getCdTipoAbrangenciaExcecao());
		saidaDTO.setCdTipoCanalInclusao(response.getOcorrencias(0).getCdCanalInclusao());
		saidaDTO.setDsTipoCanalInclusao(response.getOcorrencias(0).getDsCanalInclusao());
		saidaDTO.setUsuarioInclusao(response.getOcorrencias(0).getCdAutenSegrcInclusao());
		saidaDTO.setComplementoUsuario(response.getOcorrencias(0).getNmOperFluxoInclusao());
		saidaDTO.setDataHoraInclusao(response.getOcorrencias(0).getHrInclusaoRegistro());
		
		saidaDTO.setCdTipoCanalManutencao(response.getOcorrencias(0).getCdCanalManutencao());
		saidaDTO.setDsTipoCanallManutencao(response.getOcorrencias(0).getDsCanalManutencao());
		saidaDTO.setUsuariolManutencao(response.getOcorrencias(0).getCdAutenSegrcManutencao());
		saidaDTO.setComplementolManutencao(response.getOcorrencias(0).getNmOperFluxoManutencao());
		saidaDTO.setDataHoralManutencao(response.getOcorrencias(0).getHrManutencaoRegistro());

		
		return saidaDTO;
	}
	
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.regsegregacessodados.IRegSegregAcessoDadosService#excluirRegraSegregacao(br.com.bradesco.web.pgit.service.business.regsegregacessodados.bean.ExcluirRegraSegregacaoEntradaDTO)
	 */
	public ExcluirRegraSegregacaoSaidaDTO excluirRegraSegregacao (ExcluirRegraSegregacaoEntradaDTO excluirRegraSegregacaoEntradaDTO) {
		
		ExcluirRegraSegregacaoSaidaDTO excluirRegraSegregacaoSaidaDTO = new ExcluirRegraSegregacaoSaidaDTO();
		ExcluirRegraSegregacaoAcessoRequest excluirRegraSegregacaoAcessoRequest = new ExcluirRegraSegregacaoAcessoRequest();
		ExcluirRegraSegregacaoAcessoResponse excluirRegraSegregacaoAcessoResponse = new ExcluirRegraSegregacaoAcessoResponse();
		
		excluirRegraSegregacaoAcessoRequest.setCdRegraAcessoDado(excluirRegraSegregacaoEntradaDTO.getCdRegraAcessoDado());
		excluirRegraSegregacaoAcessoRequest.setQtConsultas(0);
		
		excluirRegraSegregacaoAcessoResponse = getFactoryAdapter().getExcluirRegraSegregacaoAcessoPDCAdapter().invokeProcess(excluirRegraSegregacaoAcessoRequest);
		
		excluirRegraSegregacaoSaidaDTO.setCodMensagem(excluirRegraSegregacaoAcessoResponse.getCodMensagem());
		excluirRegraSegregacaoSaidaDTO.setMensagem(excluirRegraSegregacaoAcessoResponse.getMensagem());
		
		return excluirRegraSegregacaoSaidaDTO;
	}
	
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.regsegregacessodados.IRegSegregAcessoDadosService#excluirExcRegraSegregacao(br.com.bradesco.web.pgit.service.business.regsegregacessodados.bean.ExcluirExcRegraSegregacaoEntradaDTO)
	 */
	public ExcluirExcRegraSegregacaoSaidaDTO excluirExcRegraSegregacao (ExcluirExcRegraSegregacaoEntradaDTO excluirExcRegraSegregacaoEntradaDTO) {
		
		ExcluirExcRegraSegregacaoSaidaDTO excluirExcRegraSegregacaoSaidaDTO = new ExcluirExcRegraSegregacaoSaidaDTO();
		ExcluirExcecaoRegraSegregacaoAcessoRequest excluirExcecaoRegraSegregacaoAcessoRequest = new ExcluirExcecaoRegraSegregacaoAcessoRequest();
		ExcluirExcecaoRegraSegregacaoAcessoResponse excluirExcecaoRegraSegregacaoAcessoResponse = new ExcluirExcecaoRegraSegregacaoAcessoResponse();
		
		excluirExcecaoRegraSegregacaoAcessoRequest.setCdRegraAcessoDado(excluirExcRegraSegregacaoEntradaDTO.getCodigoRegra());
		excluirExcecaoRegraSegregacaoAcessoRequest.setCdExcecaoAcessoDado(excluirExcRegraSegregacaoEntradaDTO.getCodigoExcecao());
		
		excluirExcecaoRegraSegregacaoAcessoResponse = getFactoryAdapter().getExcluirExcecaoRegraSegregacaoAcessoPDCAdapter().invokeProcess(excluirExcecaoRegraSegregacaoAcessoRequest);
		
		excluirExcRegraSegregacaoSaidaDTO.setCodMensagem(excluirExcecaoRegraSegregacaoAcessoResponse.getCodMensagem());
		excluirExcRegraSegregacaoSaidaDTO.setMensagem(excluirExcecaoRegraSegregacaoAcessoResponse.getMensagem());
		
		return excluirExcRegraSegregacaoSaidaDTO;
	}
	
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.regsegregacessodados.IRegSegregAcessoDadosService#alterarRegraSegregacao(br.com.bradesco.web.pgit.service.business.regsegregacessodados.bean.AlterarRegraSegregacaoEntradaDTO)
	 */
	public AlterarRegraSegregacaoSaidaDTO alterarRegraSegregacao (AlterarRegraSegregacaoEntradaDTO alterarRegraSegregacaoEntradaDTO) {
		
		AlterarRegraSegregacaoSaidaDTO alterarRegraSegregacaoSaidaDTO = new AlterarRegraSegregacaoSaidaDTO();
		AlterarRegraSegregacaoAcessoRequest alterarRegraSegregacaoRequest = new AlterarRegraSegregacaoAcessoRequest();
    	AlterarRegraSegregacaoAcessoResponse alterarRegraSegregacaoResponse = new AlterarRegraSegregacaoAcessoResponse();
		
    	alterarRegraSegregacaoRequest.setCdRegraAcessoDado(alterarRegraSegregacaoEntradaDTO.getCodigoRegra());
    	alterarRegraSegregacaoRequest.setCdTipoUnidadeUsuario(alterarRegraSegregacaoEntradaDTO.getTipoUnidadeOrganizacionalUsuario());
    	alterarRegraSegregacaoRequest.setCdTipoUnidadeProprietario(alterarRegraSegregacaoEntradaDTO.getTipoUnidadeOrganizacionalProprietarioDados());
    	alterarRegraSegregacaoRequest.setCdIndicadorAcao(alterarRegraSegregacaoEntradaDTO.getTipoAcao());
    	alterarRegraSegregacaoRequest.setCdNivelPermissaoAcesso(alterarRegraSegregacaoEntradaDTO.getNivelPermissaoAcessoDados());
		
    	alterarRegraSegregacaoResponse = getFactoryAdapter().getAlterarRegraSegregacaoAcessoPDCAdapter().invokeProcess(alterarRegraSegregacaoRequest);
		
    	alterarRegraSegregacaoSaidaDTO.setCodMensagem(alterarRegraSegregacaoResponse.getCodMensagem());
    	alterarRegraSegregacaoSaidaDTO.setMensagem(alterarRegraSegregacaoResponse.getMensagem());
		
		return alterarRegraSegregacaoSaidaDTO;
		
	}
	
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.regsegregacessodados.IRegSegregAcessoDadosService#alterarExcRegraSegregacao(br.com.bradesco.web.pgit.service.business.regsegregacessodados.bean.AlterarExcRegraSegregacaoEntradaDTO)
	 */
	public AlterarExcRegraSegregacaoSaidaDTO alterarExcRegraSegregacao (AlterarExcRegraSegregacaoEntradaDTO alterarRegraSegregacaoEntradaDTO) {
	
		AlterarExcRegraSegregacaoSaidaDTO alterarExcRegraSegregacaoSaidaDTO = new AlterarExcRegraSegregacaoSaidaDTO();
		AlterarExcecaoRegraSegregacaoAcessoRequest alterarExcecaoRegraSegregacaoAcessoRequest = new AlterarExcecaoRegraSegregacaoAcessoRequest();
		AlterarExcecaoRegraSegregacaoAcessoResponse altararExcecaoRegraSegregacaoAcessoResponse = new AlterarExcecaoRegraSegregacaoAcessoResponse();
		
		alterarExcecaoRegraSegregacaoAcessoRequest.setCdRegraAcessoDado(alterarRegraSegregacaoEntradaDTO.getCodigoRegra());
		alterarExcecaoRegraSegregacaoAcessoRequest.setCdExcecaoAcessoDado(alterarRegraSegregacaoEntradaDTO.getCodigoExcecao());
		alterarExcecaoRegraSegregacaoAcessoRequest.setCdTipoUnidadeUsuario(alterarRegraSegregacaoEntradaDTO.getTipoUnidadeOrganizacionalUsuario());
		alterarExcecaoRegraSegregacaoAcessoRequest.setCdPessoaJuridicaUsuario(alterarRegraSegregacaoEntradaDTO.getEmpresaConglomeradoUsuario());
		alterarExcecaoRegraSegregacaoAcessoRequest.setNrUnidadeOrganizacionalUsuario(alterarRegraSegregacaoEntradaDTO.getUnidadeOrganizacionalUsuario());
		alterarExcecaoRegraSegregacaoAcessoRequest.setCdTipoUnidadeProprietario(alterarRegraSegregacaoEntradaDTO.getTipoUnidadeOrganizacionalProprietario());
		alterarExcecaoRegraSegregacaoAcessoRequest.setCdPessoaJuridicaProprietario(alterarRegraSegregacaoEntradaDTO.getEmpresaConglomeradoProprietario());
		alterarExcecaoRegraSegregacaoAcessoRequest.setNrUnidadeOrganizacionalProprietario(alterarRegraSegregacaoEntradaDTO.getUnidadeOrganizacionalProprietario());
		alterarExcecaoRegraSegregacaoAcessoRequest.setCdTipoExcecaoAcesso(alterarRegraSegregacaoEntradaDTO.getTipoExcecao());
		alterarExcecaoRegraSegregacaoAcessoRequest.setCdTipoAbrangenciaExcecao(alterarRegraSegregacaoEntradaDTO.getTipoAbrangenciaExcecao());
		
		altararExcecaoRegraSegregacaoAcessoResponse = getFactoryAdapter().getAlterarExcecaoRegraSegregacaoAcessoPDCAdapter().invokeProcess(alterarExcecaoRegraSegregacaoAcessoRequest);

		alterarExcRegraSegregacaoSaidaDTO.setCodMensagem(altararExcecaoRegraSegregacaoAcessoResponse.getCodMensagem());
		alterarExcRegraSegregacaoSaidaDTO.setMensagem(altararExcecaoRegraSegregacaoAcessoResponse.getMensagem());

		
		return alterarExcRegraSegregacaoSaidaDTO;
		
	}
    
    
  
}

