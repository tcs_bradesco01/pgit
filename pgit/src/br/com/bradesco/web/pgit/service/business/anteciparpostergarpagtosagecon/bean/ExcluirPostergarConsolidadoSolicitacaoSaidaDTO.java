package br.com.bradesco.web.pgit.service.business.anteciparpostergarpagtosagecon.bean;


// TODO: Auto-generated Javadoc
/**
 * Arquivo criado em 23/05/17.
 *
 * @author : todo!
 * @version :
 */
public class ExcluirPostergarConsolidadoSolicitacaoSaidaDTO {

    /** The cod mensagem. */
    private String codMensagem;

    /** The mensagem. */
    private String mensagem;

    /**
     * Set cod mensagem.
     *
     * @param codMensagem the cod mensagem
     */
    public void setCodMensagem(String codMensagem) {
        this.codMensagem = codMensagem;
    }

    /**
     * Get cod mensagem.
     *
     * @return the cod mensagem
     */
    public String getCodMensagem() {
        return this.codMensagem;
    }

    /**
     * Set mensagem.
     *
     * @param mensagem the mensagem
     */
    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }

    /**
     * Get mensagem.
     *
     * @return the mensagem
     */
    public String getMensagem() {
        return this.mensagem;
    }
}