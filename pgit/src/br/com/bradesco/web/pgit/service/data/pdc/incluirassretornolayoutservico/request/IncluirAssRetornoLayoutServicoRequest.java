/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.incluirassretornolayoutservico.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class IncluirAssRetornoLayoutServicoRequest.
 * 
 * @version $Revision$ $Date$
 */
public class IncluirAssRetornoLayoutServicoRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdTipoLayoutArquivo
     */
    private int _cdTipoLayoutArquivo = 0;

    /**
     * keeps track of state for field: _cdTipoLayoutArquivo
     */
    private boolean _has_cdTipoLayoutArquivo;

    /**
     * Field _cdTipoRetorno
     */
    private int _cdTipoRetorno = 0;

    /**
     * keeps track of state for field: _cdTipoRetorno
     */
    private boolean _has_cdTipoRetorno;

    /**
     * Field _cdProdutoServicoOper
     */
    private int _cdProdutoServicoOper = 0;

    /**
     * keeps track of state for field: _cdProdutoServicoOper
     */
    private boolean _has_cdProdutoServicoOper;

    /**
     * Field _qtConsultas
     */
    private int _qtConsultas = 0;

    /**
     * keeps track of state for field: _qtConsultas
     */
    private boolean _has_qtConsultas;


      //----------------/
     //- Constructors -/
    //----------------/

    public IncluirAssRetornoLayoutServicoRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.incluirassretornolayoutservico.request.IncluirAssRetornoLayoutServicoRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdProdutoServicoOper
     * 
     */
    public void deleteCdProdutoServicoOper()
    {
        this._has_cdProdutoServicoOper= false;
    } //-- void deleteCdProdutoServicoOper() 

    /**
     * Method deleteCdTipoLayoutArquivo
     * 
     */
    public void deleteCdTipoLayoutArquivo()
    {
        this._has_cdTipoLayoutArquivo= false;
    } //-- void deleteCdTipoLayoutArquivo() 

    /**
     * Method deleteCdTipoRetorno
     * 
     */
    public void deleteCdTipoRetorno()
    {
        this._has_cdTipoRetorno= false;
    } //-- void deleteCdTipoRetorno() 

    /**
     * Method deleteQtConsultas
     * 
     */
    public void deleteQtConsultas()
    {
        this._has_qtConsultas= false;
    } //-- void deleteQtConsultas() 

    /**
     * Returns the value of field 'cdProdutoServicoOper'.
     * 
     * @return int
     * @return the value of field 'cdProdutoServicoOper'.
     */
    public int getCdProdutoServicoOper()
    {
        return this._cdProdutoServicoOper;
    } //-- int getCdProdutoServicoOper() 

    /**
     * Returns the value of field 'cdTipoLayoutArquivo'.
     * 
     * @return int
     * @return the value of field 'cdTipoLayoutArquivo'.
     */
    public int getCdTipoLayoutArquivo()
    {
        return this._cdTipoLayoutArquivo;
    } //-- int getCdTipoLayoutArquivo() 

    /**
     * Returns the value of field 'cdTipoRetorno'.
     * 
     * @return int
     * @return the value of field 'cdTipoRetorno'.
     */
    public int getCdTipoRetorno()
    {
        return this._cdTipoRetorno;
    } //-- int getCdTipoRetorno() 

    /**
     * Returns the value of field 'qtConsultas'.
     * 
     * @return int
     * @return the value of field 'qtConsultas'.
     */
    public int getQtConsultas()
    {
        return this._qtConsultas;
    } //-- int getQtConsultas() 

    /**
     * Method hasCdProdutoServicoOper
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdutoServicoOper()
    {
        return this._has_cdProdutoServicoOper;
    } //-- boolean hasCdProdutoServicoOper() 

    /**
     * Method hasCdTipoLayoutArquivo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoLayoutArquivo()
    {
        return this._has_cdTipoLayoutArquivo;
    } //-- boolean hasCdTipoLayoutArquivo() 

    /**
     * Method hasCdTipoRetorno
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoRetorno()
    {
        return this._has_cdTipoRetorno;
    } //-- boolean hasCdTipoRetorno() 

    /**
     * Method hasQtConsultas
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtConsultas()
    {
        return this._has_qtConsultas;
    } //-- boolean hasQtConsultas() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdProdutoServicoOper'.
     * 
     * @param cdProdutoServicoOper the value of field
     * 'cdProdutoServicoOper'.
     */
    public void setCdProdutoServicoOper(int cdProdutoServicoOper)
    {
        this._cdProdutoServicoOper = cdProdutoServicoOper;
        this._has_cdProdutoServicoOper = true;
    } //-- void setCdProdutoServicoOper(int) 

    /**
     * Sets the value of field 'cdTipoLayoutArquivo'.
     * 
     * @param cdTipoLayoutArquivo the value of field
     * 'cdTipoLayoutArquivo'.
     */
    public void setCdTipoLayoutArquivo(int cdTipoLayoutArquivo)
    {
        this._cdTipoLayoutArquivo = cdTipoLayoutArquivo;
        this._has_cdTipoLayoutArquivo = true;
    } //-- void setCdTipoLayoutArquivo(int) 

    /**
     * Sets the value of field 'cdTipoRetorno'.
     * 
     * @param cdTipoRetorno the value of field 'cdTipoRetorno'.
     */
    public void setCdTipoRetorno(int cdTipoRetorno)
    {
        this._cdTipoRetorno = cdTipoRetorno;
        this._has_cdTipoRetorno = true;
    } //-- void setCdTipoRetorno(int) 

    /**
     * Sets the value of field 'qtConsultas'.
     * 
     * @param qtConsultas the value of field 'qtConsultas'.
     */
    public void setQtConsultas(int qtConsultas)
    {
        this._qtConsultas = qtConsultas;
        this._has_qtConsultas = true;
    } //-- void setQtConsultas(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return IncluirAssRetornoLayoutServicoRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.incluirassretornolayoutservico.request.IncluirAssRetornoLayoutServicoRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.incluirassretornolayoutservico.request.IncluirAssRetornoLayoutServicoRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.incluirassretornolayoutservico.request.IncluirAssRetornoLayoutServicoRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.incluirassretornolayoutservico.request.IncluirAssRetornoLayoutServicoRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
