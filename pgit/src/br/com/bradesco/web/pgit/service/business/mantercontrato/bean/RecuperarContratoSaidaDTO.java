/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantercontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantercontrato.bean;

/**
 * Nome: RecuperarContratoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class RecuperarContratoSaidaDTO {
	
	/** Atributo codMesangem. */
	private String codMesangem;
	
	/** Atributo mensagem. */
	private String mensagem;
	
	/**
	 * Get: codMesangem.
	 *
	 * @return codMesangem
	 */
	public String getCodMesangem() {
		return codMesangem;
	}
	
	/**
	 * Set: codMesangem.
	 *
	 * @param codMesangem the cod mesangem
	 */
	public void setCodMesangem(String codMesangem) {
		this.codMesangem = codMesangem;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	
	

}
