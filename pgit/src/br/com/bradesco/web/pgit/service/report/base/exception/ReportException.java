/*
 * Nome: br.com.bradesco.web.pgit.service.report.base.exception
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.report.base.exception;

import br.com.bradesco.web.aq.application.error.BradescoApplicationException;

/**
 * Exce��o lan�ada quando ocorre erro na gera��o de relat�rio.
 *
 */
public class ReportException extends BradescoApplicationException {

	/** Atributo serialVersionUID. */
	private static final long serialVersionUID = 5688584756000649146L;

	/**
	 * Report exception.
	 *
	 * @param message the message
	 * @param exception the exception
	 */
	public ReportException(String message, Throwable exception) {
		super(message, exception);
	}

	/**
	 * Report exception.
	 *
	 * @param exception the exception
	 */
	public ReportException(Throwable exception) {
		super(exception);
	}

	/**
	 * Report exception.
	 *
	 * @param message the message
	 */
	public ReportException(String message) {
		super(message);
	}

}
