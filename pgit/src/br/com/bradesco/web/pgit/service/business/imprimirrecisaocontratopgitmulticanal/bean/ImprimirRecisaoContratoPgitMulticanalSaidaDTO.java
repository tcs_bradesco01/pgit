/*
 * Nome: br.com.bradesco.web.pgit.service.business.imprimirrecisaocontratopgitmulticanal.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.imprimirrecisaocontratopgitmulticanal.bean;

/**
 * Nome: ImprimirRecisaoContratoPgitMulticanalSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ImprimirRecisaoContratoPgitMulticanalSaidaDTO {

	/** Atributo protocolo. */
	private Long protocolo;

	/**
	 * Get: protocolo.
	 *
	 * @return protocolo
	 */
	public Long getProtocolo() {
		return protocolo;
	}
	
	/**
	 * Set: protocolo.
	 *
	 * @param protocolo the protocolo
	 */
	public void setProtocolo(Long protocolo) {
		this.protocolo = protocolo;
	}
}