package br.com.bradesco.web.pgit.service.business.combo.bean;


// TODO: Auto-generated Javadoc
/**
 * Arquivo criado em 11/12/15.
 */
public class ListarServicoRelacionadoPgitCdpsOcorrenciasSaidaDTO {

    /** The cd produto operacao relacionado. */
    private Integer cdProdutoOperacaoRelacionado;

    /** The ds produto servico relacionado. */
    private String dsProdutoServicoRelacionado;

    /** The cd relacionamento produto. */
    private Integer cdRelacionamentoProduto;

    /** The ds relacionamento produto. */
    private String dsRelacionamentoProduto;

    /** The cd tipo produto servico. */
    private Integer cdTipoProdutoServico;

    /** The ds tipo produto servico. */
    private String dsTipoProdutoServico;
    
    

    public ListarServicoRelacionadoPgitCdpsOcorrenciasSaidaDTO(
			Integer cdProdutoOperacaoRelacionado,
			String dsProdutoServicoRelacionado,
			Integer cdRelacionamentoProduto, String dsRelacionamentoProduto,
			Integer cdTipoProdutoServico, String dsTipoProdutoServico) {
		super();
		this.cdProdutoOperacaoRelacionado = cdProdutoOperacaoRelacionado;
		this.dsProdutoServicoRelacionado = dsProdutoServicoRelacionado;
		this.cdRelacionamentoProduto = cdRelacionamentoProduto;
		this.dsRelacionamentoProduto = dsRelacionamentoProduto;
		this.cdTipoProdutoServico = cdTipoProdutoServico;
		this.dsTipoProdutoServico = dsTipoProdutoServico;
	}

	/**
     * Sets the cd produto operacao relacionado.
     *
     * @param cdProdutoOperacaoRelacionado the cd produto operacao relacionado
     */
    public void setCdProdutoOperacaoRelacionado(Integer cdProdutoOperacaoRelacionado) {
        this.cdProdutoOperacaoRelacionado = cdProdutoOperacaoRelacionado;
    }

    /**
     * Gets the cd produto operacao relacionado.
     *
     * @return the cd produto operacao relacionado
     */
    public Integer getCdProdutoOperacaoRelacionado() {
        return this.cdProdutoOperacaoRelacionado;
    }

    /**
     * Sets the ds produto servico relacionado.
     *
     * @param dsProdutoServicoRelacionado the ds produto servico relacionado
     */
    public void setDsProdutoServicoRelacionado(String dsProdutoServicoRelacionado) {
        this.dsProdutoServicoRelacionado = dsProdutoServicoRelacionado;
    }

    /**
     * Gets the ds produto servico relacionado.
     *
     * @return the ds produto servico relacionado
     */
    public String getDsProdutoServicoRelacionado() {
        return this.dsProdutoServicoRelacionado;
    }

    /**
     * Sets the cd relacionamento produto.
     *
     * @param cdRelacionamentoProduto the cd relacionamento produto
     */
    public void setCdRelacionamentoProduto(Integer cdRelacionamentoProduto) {
        this.cdRelacionamentoProduto = cdRelacionamentoProduto;
    }

    /**
     * Gets the cd relacionamento produto.
     *
     * @return the cd relacionamento produto
     */
    public Integer getCdRelacionamentoProduto() {
        return this.cdRelacionamentoProduto;
    }

    /**
     * Sets the ds relacionamento produto.
     *
     * @param dsRelacionamentoProduto the ds relacionamento produto
     */
    public void setDsRelacionamentoProduto(String dsRelacionamentoProduto) {
        this.dsRelacionamentoProduto = dsRelacionamentoProduto;
    }

    /**
     * Gets the ds relacionamento produto.
     *
     * @return the ds relacionamento produto
     */
    public String getDsRelacionamentoProduto() {
        return this.dsRelacionamentoProduto;
    }

    /**
     * Sets the cd tipo produto servico.
     *
     * @param cdTipoProdutoServico the cd tipo produto servico
     */
    public void setCdTipoProdutoServico(Integer cdTipoProdutoServico) {
        this.cdTipoProdutoServico = cdTipoProdutoServico;
    }

    /**
     * Gets the cd tipo produto servico.
     *
     * @return the cd tipo produto servico
     */
    public Integer getCdTipoProdutoServico() {
        return this.cdTipoProdutoServico;
    }

    /**
     * Sets the ds tipo produto servico.
     *
     * @param dsTipoProdutoServico the ds tipo produto servico
     */
    public void setDsTipoProdutoServico(String dsTipoProdutoServico) {
        this.dsTipoProdutoServico = dsTipoProdutoServico;
    }

    /**
     * Gets the ds tipo produto servico.
     *
     * @return the ds tipo produto servico
     */
    public String getDsTipoProdutoServico() {
        return this.dsTipoProdutoServico;
    }
}