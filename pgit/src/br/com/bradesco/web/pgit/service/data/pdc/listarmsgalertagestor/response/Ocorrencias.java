/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listarmsgalertagestor.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdCistProcs
     */
    private java.lang.String _cdCistProcs;

    /**
     * Field _cdTipoProcsSistema
     */
    private int _cdTipoProcsSistema = 0;

    /**
     * keeps track of state for field: _cdTipoProcsSistema
     */
    private boolean _has_cdTipoProcsSistema;

    /**
     * Field _dsTipoProcsSistema
     */
    private java.lang.String _dsTipoProcsSistema;

    /**
     * Field _cdProcsSistema
     */
    private int _cdProcsSistema = 0;

    /**
     * keeps track of state for field: _cdProcsSistema
     */
    private boolean _has_cdProcsSistema;

    /**
     * Field _nrSeqMensagemAlerta
     */
    private int _nrSeqMensagemAlerta = 0;

    /**
     * keeps track of state for field: _nrSeqMensagemAlerta
     */
    private boolean _has_nrSeqMensagemAlerta;

    /**
     * Field _cdPessoa
     */
    private long _cdPessoa = 0;

    /**
     * keeps track of state for field: _cdPessoa
     */
    private boolean _has_cdPessoa;

    /**
     * Field _dsPessoa
     */
    private java.lang.String _dsPessoa;

    /**
     * Field _cdCist
     */
    private java.lang.String _cdCist;

    /**
     * Field _nrEventoMensagemNegocio
     */
    private int _nrEventoMensagemNegocio = 0;

    /**
     * keeps track of state for field: _nrEventoMensagemNegocio
     */
    private boolean _has_nrEventoMensagemNegocio;

    /**
     * Field _cdRecGedorMensagem
     */
    private int _cdRecGedorMensagem = 0;

    /**
     * keeps track of state for field: _cdRecGedorMensagem
     */
    private boolean _has_cdRecGedorMensagem;

    /**
     * Field _dsRecGedorMensagem
     */
    private java.lang.String _dsRecGedorMensagem;

    /**
     * Field _cdIdiomaTextoMensagem
     */
    private int _cdIdiomaTextoMensagem = 0;

    /**
     * keeps track of state for field: _cdIdiomaTextoMensagem
     */
    private boolean _has_cdIdiomaTextoMensagem;

    /**
     * Field _dsIdiomaTextoMensagem
     */
    private java.lang.String _dsIdiomaTextoMensagem;

    /**
     * Field _dsMensagemAlerta
     */
    private java.lang.String _dsMensagemAlerta;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarmsgalertagestor.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdIdiomaTextoMensagem
     * 
     */
    public void deleteCdIdiomaTextoMensagem()
    {
        this._has_cdIdiomaTextoMensagem= false;
    } //-- void deleteCdIdiomaTextoMensagem() 

    /**
     * Method deleteCdPessoa
     * 
     */
    public void deleteCdPessoa()
    {
        this._has_cdPessoa= false;
    } //-- void deleteCdPessoa() 

    /**
     * Method deleteCdProcsSistema
     * 
     */
    public void deleteCdProcsSistema()
    {
        this._has_cdProcsSistema= false;
    } //-- void deleteCdProcsSistema() 

    /**
     * Method deleteCdRecGedorMensagem
     * 
     */
    public void deleteCdRecGedorMensagem()
    {
        this._has_cdRecGedorMensagem= false;
    } //-- void deleteCdRecGedorMensagem() 

    /**
     * Method deleteCdTipoProcsSistema
     * 
     */
    public void deleteCdTipoProcsSistema()
    {
        this._has_cdTipoProcsSistema= false;
    } //-- void deleteCdTipoProcsSistema() 

    /**
     * Method deleteNrEventoMensagemNegocio
     * 
     */
    public void deleteNrEventoMensagemNegocio()
    {
        this._has_nrEventoMensagemNegocio= false;
    } //-- void deleteNrEventoMensagemNegocio() 

    /**
     * Method deleteNrSeqMensagemAlerta
     * 
     */
    public void deleteNrSeqMensagemAlerta()
    {
        this._has_nrSeqMensagemAlerta= false;
    } //-- void deleteNrSeqMensagemAlerta() 

    /**
     * Returns the value of field 'cdCist'.
     * 
     * @return String
     * @return the value of field 'cdCist'.
     */
    public java.lang.String getCdCist()
    {
        return this._cdCist;
    } //-- java.lang.String getCdCist() 

    /**
     * Returns the value of field 'cdCistProcs'.
     * 
     * @return String
     * @return the value of field 'cdCistProcs'.
     */
    public java.lang.String getCdCistProcs()
    {
        return this._cdCistProcs;
    } //-- java.lang.String getCdCistProcs() 

    /**
     * Returns the value of field 'cdIdiomaTextoMensagem'.
     * 
     * @return int
     * @return the value of field 'cdIdiomaTextoMensagem'.
     */
    public int getCdIdiomaTextoMensagem()
    {
        return this._cdIdiomaTextoMensagem;
    } //-- int getCdIdiomaTextoMensagem() 

    /**
     * Returns the value of field 'cdPessoa'.
     * 
     * @return long
     * @return the value of field 'cdPessoa'.
     */
    public long getCdPessoa()
    {
        return this._cdPessoa;
    } //-- long getCdPessoa() 

    /**
     * Returns the value of field 'cdProcsSistema'.
     * 
     * @return int
     * @return the value of field 'cdProcsSistema'.
     */
    public int getCdProcsSistema()
    {
        return this._cdProcsSistema;
    } //-- int getCdProcsSistema() 

    /**
     * Returns the value of field 'cdRecGedorMensagem'.
     * 
     * @return int
     * @return the value of field 'cdRecGedorMensagem'.
     */
    public int getCdRecGedorMensagem()
    {
        return this._cdRecGedorMensagem;
    } //-- int getCdRecGedorMensagem() 

    /**
     * Returns the value of field 'cdTipoProcsSistema'.
     * 
     * @return int
     * @return the value of field 'cdTipoProcsSistema'.
     */
    public int getCdTipoProcsSistema()
    {
        return this._cdTipoProcsSistema;
    } //-- int getCdTipoProcsSistema() 

    /**
     * Returns the value of field 'dsIdiomaTextoMensagem'.
     * 
     * @return String
     * @return the value of field 'dsIdiomaTextoMensagem'.
     */
    public java.lang.String getDsIdiomaTextoMensagem()
    {
        return this._dsIdiomaTextoMensagem;
    } //-- java.lang.String getDsIdiomaTextoMensagem() 

    /**
     * Returns the value of field 'dsMensagemAlerta'.
     * 
     * @return String
     * @return the value of field 'dsMensagemAlerta'.
     */
    public java.lang.String getDsMensagemAlerta()
    {
        return this._dsMensagemAlerta;
    } //-- java.lang.String getDsMensagemAlerta() 

    /**
     * Returns the value of field 'dsPessoa'.
     * 
     * @return String
     * @return the value of field 'dsPessoa'.
     */
    public java.lang.String getDsPessoa()
    {
        return this._dsPessoa;
    } //-- java.lang.String getDsPessoa() 

    /**
     * Returns the value of field 'dsRecGedorMensagem'.
     * 
     * @return String
     * @return the value of field 'dsRecGedorMensagem'.
     */
    public java.lang.String getDsRecGedorMensagem()
    {
        return this._dsRecGedorMensagem;
    } //-- java.lang.String getDsRecGedorMensagem() 

    /**
     * Returns the value of field 'dsTipoProcsSistema'.
     * 
     * @return String
     * @return the value of field 'dsTipoProcsSistema'.
     */
    public java.lang.String getDsTipoProcsSistema()
    {
        return this._dsTipoProcsSistema;
    } //-- java.lang.String getDsTipoProcsSistema() 

    /**
     * Returns the value of field 'nrEventoMensagemNegocio'.
     * 
     * @return int
     * @return the value of field 'nrEventoMensagemNegocio'.
     */
    public int getNrEventoMensagemNegocio()
    {
        return this._nrEventoMensagemNegocio;
    } //-- int getNrEventoMensagemNegocio() 

    /**
     * Returns the value of field 'nrSeqMensagemAlerta'.
     * 
     * @return int
     * @return the value of field 'nrSeqMensagemAlerta'.
     */
    public int getNrSeqMensagemAlerta()
    {
        return this._nrSeqMensagemAlerta;
    } //-- int getNrSeqMensagemAlerta() 

    /**
     * Method hasCdIdiomaTextoMensagem
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIdiomaTextoMensagem()
    {
        return this._has_cdIdiomaTextoMensagem;
    } //-- boolean hasCdIdiomaTextoMensagem() 

    /**
     * Method hasCdPessoa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoa()
    {
        return this._has_cdPessoa;
    } //-- boolean hasCdPessoa() 

    /**
     * Method hasCdProcsSistema
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProcsSistema()
    {
        return this._has_cdProcsSistema;
    } //-- boolean hasCdProcsSistema() 

    /**
     * Method hasCdRecGedorMensagem
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdRecGedorMensagem()
    {
        return this._has_cdRecGedorMensagem;
    } //-- boolean hasCdRecGedorMensagem() 

    /**
     * Method hasCdTipoProcsSistema
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoProcsSistema()
    {
        return this._has_cdTipoProcsSistema;
    } //-- boolean hasCdTipoProcsSistema() 

    /**
     * Method hasNrEventoMensagemNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrEventoMensagemNegocio()
    {
        return this._has_nrEventoMensagemNegocio;
    } //-- boolean hasNrEventoMensagemNegocio() 

    /**
     * Method hasNrSeqMensagemAlerta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSeqMensagemAlerta()
    {
        return this._has_nrSeqMensagemAlerta;
    } //-- boolean hasNrSeqMensagemAlerta() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdCist'.
     * 
     * @param cdCist the value of field 'cdCist'.
     */
    public void setCdCist(java.lang.String cdCist)
    {
        this._cdCist = cdCist;
    } //-- void setCdCist(java.lang.String) 

    /**
     * Sets the value of field 'cdCistProcs'.
     * 
     * @param cdCistProcs the value of field 'cdCistProcs'.
     */
    public void setCdCistProcs(java.lang.String cdCistProcs)
    {
        this._cdCistProcs = cdCistProcs;
    } //-- void setCdCistProcs(java.lang.String) 

    /**
     * Sets the value of field 'cdIdiomaTextoMensagem'.
     * 
     * @param cdIdiomaTextoMensagem the value of field
     * 'cdIdiomaTextoMensagem'.
     */
    public void setCdIdiomaTextoMensagem(int cdIdiomaTextoMensagem)
    {
        this._cdIdiomaTextoMensagem = cdIdiomaTextoMensagem;
        this._has_cdIdiomaTextoMensagem = true;
    } //-- void setCdIdiomaTextoMensagem(int) 

    /**
     * Sets the value of field 'cdPessoa'.
     * 
     * @param cdPessoa the value of field 'cdPessoa'.
     */
    public void setCdPessoa(long cdPessoa)
    {
        this._cdPessoa = cdPessoa;
        this._has_cdPessoa = true;
    } //-- void setCdPessoa(long) 

    /**
     * Sets the value of field 'cdProcsSistema'.
     * 
     * @param cdProcsSistema the value of field 'cdProcsSistema'.
     */
    public void setCdProcsSistema(int cdProcsSistema)
    {
        this._cdProcsSistema = cdProcsSistema;
        this._has_cdProcsSistema = true;
    } //-- void setCdProcsSistema(int) 

    /**
     * Sets the value of field 'cdRecGedorMensagem'.
     * 
     * @param cdRecGedorMensagem the value of field
     * 'cdRecGedorMensagem'.
     */
    public void setCdRecGedorMensagem(int cdRecGedorMensagem)
    {
        this._cdRecGedorMensagem = cdRecGedorMensagem;
        this._has_cdRecGedorMensagem = true;
    } //-- void setCdRecGedorMensagem(int) 

    /**
     * Sets the value of field 'cdTipoProcsSistema'.
     * 
     * @param cdTipoProcsSistema the value of field
     * 'cdTipoProcsSistema'.
     */
    public void setCdTipoProcsSistema(int cdTipoProcsSistema)
    {
        this._cdTipoProcsSistema = cdTipoProcsSistema;
        this._has_cdTipoProcsSistema = true;
    } //-- void setCdTipoProcsSistema(int) 

    /**
     * Sets the value of field 'dsIdiomaTextoMensagem'.
     * 
     * @param dsIdiomaTextoMensagem the value of field
     * 'dsIdiomaTextoMensagem'.
     */
    public void setDsIdiomaTextoMensagem(java.lang.String dsIdiomaTextoMensagem)
    {
        this._dsIdiomaTextoMensagem = dsIdiomaTextoMensagem;
    } //-- void setDsIdiomaTextoMensagem(java.lang.String) 

    /**
     * Sets the value of field 'dsMensagemAlerta'.
     * 
     * @param dsMensagemAlerta the value of field 'dsMensagemAlerta'
     */
    public void setDsMensagemAlerta(java.lang.String dsMensagemAlerta)
    {
        this._dsMensagemAlerta = dsMensagemAlerta;
    } //-- void setDsMensagemAlerta(java.lang.String) 

    /**
     * Sets the value of field 'dsPessoa'.
     * 
     * @param dsPessoa the value of field 'dsPessoa'.
     */
    public void setDsPessoa(java.lang.String dsPessoa)
    {
        this._dsPessoa = dsPessoa;
    } //-- void setDsPessoa(java.lang.String) 

    /**
     * Sets the value of field 'dsRecGedorMensagem'.
     * 
     * @param dsRecGedorMensagem the value of field
     * 'dsRecGedorMensagem'.
     */
    public void setDsRecGedorMensagem(java.lang.String dsRecGedorMensagem)
    {
        this._dsRecGedorMensagem = dsRecGedorMensagem;
    } //-- void setDsRecGedorMensagem(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoProcsSistema'.
     * 
     * @param dsTipoProcsSistema the value of field
     * 'dsTipoProcsSistema'.
     */
    public void setDsTipoProcsSistema(java.lang.String dsTipoProcsSistema)
    {
        this._dsTipoProcsSistema = dsTipoProcsSistema;
    } //-- void setDsTipoProcsSistema(java.lang.String) 

    /**
     * Sets the value of field 'nrEventoMensagemNegocio'.
     * 
     * @param nrEventoMensagemNegocio the value of field
     * 'nrEventoMensagemNegocio'.
     */
    public void setNrEventoMensagemNegocio(int nrEventoMensagemNegocio)
    {
        this._nrEventoMensagemNegocio = nrEventoMensagemNegocio;
        this._has_nrEventoMensagemNegocio = true;
    } //-- void setNrEventoMensagemNegocio(int) 

    /**
     * Sets the value of field 'nrSeqMensagemAlerta'.
     * 
     * @param nrSeqMensagemAlerta the value of field
     * 'nrSeqMensagemAlerta'.
     */
    public void setNrSeqMensagemAlerta(int nrSeqMensagemAlerta)
    {
        this._nrSeqMensagemAlerta = nrSeqMensagemAlerta;
        this._has_nrSeqMensagemAlerta = true;
    } //-- void setNrSeqMensagemAlerta(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listarmsgalertagestor.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listarmsgalertagestor.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listarmsgalertagestor.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarmsgalertagestor.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
