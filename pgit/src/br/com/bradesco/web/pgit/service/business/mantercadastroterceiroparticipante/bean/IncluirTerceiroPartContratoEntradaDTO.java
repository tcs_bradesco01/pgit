/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantercadastroterceiroparticipante.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantercadastroterceiroparticipante.bean;

/**
 * Nome: IncluirTerceiroPartContratoEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class IncluirTerceiroPartContratoEntradaDTO{
	
	/** Atributo cdpessoaJuridicaContrato. */
	private Long cdpessoaJuridicaContrato;
	
	/** Atributo cdTipoContratoNegocio. */
	private Integer cdTipoContratoNegocio;
	
	/** Atributo nrSequenciaContratoNegocio. */
	private Long nrSequenciaContratoNegocio;
	
	/** Atributo cdTipoParticipacaoPessoa. */
	private Integer cdTipoParticipacaoPessoa;
	
	/** Atributo cdPessoa. */
	private Long cdPessoa;
	
	/** Atributo cdCpfCnpjTerceiro. */
	private Long cdCpfCnpjTerceiro;
	
	/** Atributo cdFilialCnpjTerceiro. */
	private Integer cdFilialCnpjTerceiro;
	
	/** Atributo cdControleCnpjTerceiro. */
	private Integer cdControleCnpjTerceiro;
	
	/** Atributo cdIndicadorAgendaTitulo. */
	private Integer cdIndicadorAgendaTitulo;
	
	/** Atributo cdIndicadorTerceiro. */
	private Integer cdIndicadorTerceiro;
	
	/** Atributo cdIndicadorPapeletaTerceiro. */
	private Integer cdIndicadorPapeletaTerceiro;
	
	/** Atributo dtInicioRastreaTerceiro. */
	private String dtInicioRastreaTerceiro;
	
	/** Atributo dtInicioPapeletaTerceiro. */
	private String dtInicioPapeletaTerceiro;

	/**
	 * Get: cdpessoaJuridicaContrato.
	 *
	 * @return cdpessoaJuridicaContrato
	 */
	public Long getCdpessoaJuridicaContrato(){
		return cdpessoaJuridicaContrato;
	}

	/**
	 * Set: cdpessoaJuridicaContrato.
	 *
	 * @param cdpessoaJuridicaContrato the cdpessoa juridica contrato
	 */
	public void setCdpessoaJuridicaContrato(Long cdpessoaJuridicaContrato){
		this.cdpessoaJuridicaContrato = cdpessoaJuridicaContrato;
	}

	/**
	 * Get: cdTipoContratoNegocio.
	 *
	 * @return cdTipoContratoNegocio
	 */
	public Integer getCdTipoContratoNegocio(){
		return cdTipoContratoNegocio;
	}

	/**
	 * Set: cdTipoContratoNegocio.
	 *
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio){
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}

	/**
	 * Get: nrSequenciaContratoNegocio.
	 *
	 * @return nrSequenciaContratoNegocio
	 */
	public Long getNrSequenciaContratoNegocio(){
		return nrSequenciaContratoNegocio;
	}

	/**
	 * Set: nrSequenciaContratoNegocio.
	 *
	 * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
	 */
	public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio){
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}

	/**
	 * Get: cdTipoParticipacaoPessoa.
	 *
	 * @return cdTipoParticipacaoPessoa
	 */
	public Integer getCdTipoParticipacaoPessoa(){
		return cdTipoParticipacaoPessoa;
	}

	/**
	 * Set: cdTipoParticipacaoPessoa.
	 *
	 * @param cdTipoParticipacaoPessoa the cd tipo participacao pessoa
	 */
	public void setCdTipoParticipacaoPessoa(Integer cdTipoParticipacaoPessoa){
		this.cdTipoParticipacaoPessoa = cdTipoParticipacaoPessoa;
	}

	/**
	 * Get: cdPessoa.
	 *
	 * @return cdPessoa
	 */
	public Long getCdPessoa(){
		return cdPessoa;
	}

	/**
	 * Set: cdPessoa.
	 *
	 * @param cdPessoa the cd pessoa
	 */
	public void setCdPessoa(Long cdPessoa){
		this.cdPessoa = cdPessoa;
	}

	/**
	 * Get: cdCpfCnpjTerceiro.
	 *
	 * @return cdCpfCnpjTerceiro
	 */
	public Long getCdCpfCnpjTerceiro(){
		return cdCpfCnpjTerceiro;
	}

	/**
	 * Set: cdCpfCnpjTerceiro.
	 *
	 * @param cdCpfCnpjTerceiro the cd cpf cnpj terceiro
	 */
	public void setCdCpfCnpjTerceiro(Long cdCpfCnpjTerceiro){
		this.cdCpfCnpjTerceiro = cdCpfCnpjTerceiro;
	}

	/**
	 * Get: cdFilialCnpjTerceiro.
	 *
	 * @return cdFilialCnpjTerceiro
	 */
	public Integer getCdFilialCnpjTerceiro(){
		return cdFilialCnpjTerceiro;
	}

	/**
	 * Set: cdFilialCnpjTerceiro.
	 *
	 * @param cdFilialCnpjTerceiro the cd filial cnpj terceiro
	 */
	public void setCdFilialCnpjTerceiro(Integer cdFilialCnpjTerceiro){
		this.cdFilialCnpjTerceiro = cdFilialCnpjTerceiro;
	}

	/**
	 * Get: cdControleCnpjTerceiro.
	 *
	 * @return cdControleCnpjTerceiro
	 */
	public Integer getCdControleCnpjTerceiro(){
		return cdControleCnpjTerceiro;
	}

	/**
	 * Set: cdControleCnpjTerceiro.
	 *
	 * @param cdControleCnpjTerceiro the cd controle cnpj terceiro
	 */
	public void setCdControleCnpjTerceiro(Integer cdControleCnpjTerceiro){
		this.cdControleCnpjTerceiro = cdControleCnpjTerceiro;
	}

	/**
	 * Get: cdIndicadorAgendaTitulo.
	 *
	 * @return cdIndicadorAgendaTitulo
	 */
	public Integer getCdIndicadorAgendaTitulo(){
		return cdIndicadorAgendaTitulo;
	}

	/**
	 * Set: cdIndicadorAgendaTitulo.
	 *
	 * @param cdIndicadorAgendaTitulo the cd indicador agenda titulo
	 */
	public void setCdIndicadorAgendaTitulo(Integer cdIndicadorAgendaTitulo){
		this.cdIndicadorAgendaTitulo = cdIndicadorAgendaTitulo;
	}

	/**
	 * Get: cdIndicadorTerceiro.
	 *
	 * @return cdIndicadorTerceiro
	 */
	public Integer getCdIndicadorTerceiro(){
		return cdIndicadorTerceiro;
	}

	/**
	 * Set: cdIndicadorTerceiro.
	 *
	 * @param cdIndicadorTerceiro the cd indicador terceiro
	 */
	public void setCdIndicadorTerceiro(Integer cdIndicadorTerceiro){
		this.cdIndicadorTerceiro = cdIndicadorTerceiro;
	}

	/**
	 * Get: cdIndicadorPapeletaTerceiro.
	 *
	 * @return cdIndicadorPapeletaTerceiro
	 */
	public Integer getCdIndicadorPapeletaTerceiro(){
		return cdIndicadorPapeletaTerceiro;
	}

	/**
	 * Set: cdIndicadorPapeletaTerceiro.
	 *
	 * @param cdIndicadorPapeletaTerceiro the cd indicador papeleta terceiro
	 */
	public void setCdIndicadorPapeletaTerceiro(Integer cdIndicadorPapeletaTerceiro){
		this.cdIndicadorPapeletaTerceiro = cdIndicadorPapeletaTerceiro;
	}

	/**
	 * Get: dtInicioRastreaTerceiro.
	 *
	 * @return dtInicioRastreaTerceiro
	 */
	public String getDtInicioRastreaTerceiro(){
		return dtInicioRastreaTerceiro;
	}

	/**
	 * Set: dtInicioRastreaTerceiro.
	 *
	 * @param dtInicioRastreaTerceiro the dt inicio rastrea terceiro
	 */
	public void setDtInicioRastreaTerceiro(String dtInicioRastreaTerceiro){
		this.dtInicioRastreaTerceiro = dtInicioRastreaTerceiro;
	}

	/**
	 * Get: dtInicioPapeletaTerceiro.
	 *
	 * @return dtInicioPapeletaTerceiro
	 */
	public String getDtInicioPapeletaTerceiro(){
		return dtInicioPapeletaTerceiro;
	}

	/**
	 * Set: dtInicioPapeletaTerceiro.
	 *
	 * @param dtInicioPapeletaTerceiro the dt inicio papeleta terceiro
	 */
	public void setDtInicioPapeletaTerceiro(String dtInicioPapeletaTerceiro){
		this.dtInicioPapeletaTerceiro = dtInicioPapeletaTerceiro;
	}
}