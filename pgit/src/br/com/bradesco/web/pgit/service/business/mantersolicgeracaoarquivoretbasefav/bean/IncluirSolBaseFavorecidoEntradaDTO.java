/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantersolicgeracaoarquivoretbasefav.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantersolicgeracaoarquivoretbasefav.bean;

import java.math.BigDecimal;

/**
 * Nome: IncluirSolBaseFavorecidoEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class IncluirSolBaseFavorecidoEntradaDTO {

    /** Atributo cdPessoaJuridicaContrato. */
    private Long cdPessoaJuridicaContrato;
    
    /** Atributo cdTipoContratoNegocio. */
    private Integer cdTipoContratoNegocio;
    
    /** Atributo nrSequenciaContratoNegocio. */
    private Long nrSequenciaContratoNegocio;
    
    /** Atributo cdTipoFavorecido. */
    private Integer cdTipoFavorecido;
    
    /** Atributo cdSituacaoFavorecido. */
    private Integer cdSituacaoFavorecido;
    
    /** Atributo cdDestino. */
    private Integer cdDestino;
    
    /** Atributo vlTarifaPadrao. */
    private BigDecimal vlTarifaPadrao;
    
    /** Atributo numPercentualDescTarifa. */
    private BigDecimal numPercentualDescTarifa;
    
    /** Atributo vlrTarifaAtual. */
    private BigDecimal vlrTarifaAtual;
    
	/**
	 * Get: cdDestino.
	 *
	 * @return cdDestino
	 */
	public Integer getCdDestino() {
		return cdDestino;
	}
	
	/**
	 * Set: cdDestino.
	 *
	 * @param cdDestino the cd destino
	 */
	public void setCdDestino(Integer cdDestino) {
		this.cdDestino = cdDestino;
	}
	
	/**
	 * Get: cdPessoaJuridicaContrato.
	 *
	 * @return cdPessoaJuridicaContrato
	 */
	public Long getCdPessoaJuridicaContrato() {
		return cdPessoaJuridicaContrato;
	}
	
	/**
	 * Set: cdPessoaJuridicaContrato.
	 *
	 * @param cdPessoaJuridicaContrato the cd pessoa juridica contrato
	 */
	public void setCdPessoaJuridicaContrato(Long cdPessoaJuridicaContrato) {
		this.cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
	}
	
	/**
	 * Get: cdSituacaoFavorecido.
	 *
	 * @return cdSituacaoFavorecido
	 */
	public Integer getCdSituacaoFavorecido() {
		return cdSituacaoFavorecido;
	}
	
	/**
	 * Set: cdSituacaoFavorecido.
	 *
	 * @param cdSituacaoFavorecido the cd situacao favorecido
	 */
	public void setCdSituacaoFavorecido(Integer cdSituacaoFavorecido) {
		this.cdSituacaoFavorecido = cdSituacaoFavorecido;
	}
	
	/**
	 * Get: cdTipoContratoNegocio.
	 *
	 * @return cdTipoContratoNegocio
	 */
	public Integer getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}
	
	/**
	 * Set: cdTipoContratoNegocio.
	 *
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}
	
	/**
	 * Get: cdTipoFavorecido.
	 *
	 * @return cdTipoFavorecido
	 */
	public Integer getCdTipoFavorecido() {
		return cdTipoFavorecido;
	}
	
	/**
	 * Set: cdTipoFavorecido.
	 *
	 * @param cdTipoFavorecido the cd tipo favorecido
	 */
	public void setCdTipoFavorecido(Integer cdTipoFavorecido) {
		this.cdTipoFavorecido = cdTipoFavorecido;
	}
	
	/**
	 * Get: nrSequenciaContratoNegocio.
	 *
	 * @return nrSequenciaContratoNegocio
	 */
	public Long getNrSequenciaContratoNegocio() {
		return nrSequenciaContratoNegocio;
	}
	
	/**
	 * Set: nrSequenciaContratoNegocio.
	 *
	 * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
	 */
	public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}
	
	/**
	 * Get: vlTarifaPadrao.
	 *
	 * @return vlTarifaPadrao
	 */
	public BigDecimal getVlTarifaPadrao() {
		return vlTarifaPadrao;
	}
	
	/**
	 * Set: vlTarifaPadrao.
	 *
	 * @param vlTarifaPadrao the vl tarifa padrao
	 */
	public void setVlTarifaPadrao(BigDecimal vlTarifaPadrao) {
		this.vlTarifaPadrao = vlTarifaPadrao;
	}
	
	/**
	 * Get: numPercentualDescTarifa.
	 *
	 * @return numPercentualDescTarifa
	 */
	public BigDecimal getNumPercentualDescTarifa() {
		return numPercentualDescTarifa;
	}
	
	/**
	 * Set: numPercentualDescTarifa.
	 *
	 * @param numPercentualDescTarifa the num percentual desc tarifa
	 */
	public void setNumPercentualDescTarifa(BigDecimal numPercentualDescTarifa) {
		this.numPercentualDescTarifa = numPercentualDescTarifa;
	}
	
	/**
	 * Get: vlrTarifaAtual.
	 *
	 * @return vlrTarifaAtual
	 */
	public BigDecimal getVlrTarifaAtual() {
		return vlrTarifaAtual;
	}
	
	/**
	 * Set: vlrTarifaAtual.
	 *
	 * @param vlrTarifaAtual the vlr tarifa atual
	 */
	public void setVlrTarifaAtual(BigDecimal vlrTarifaAtual) {
		this.vlrTarifaAtual = vlrTarifaAtual;
	}	
}
