/*
 * Nome: br.com.bradesco.web.pgit.service.business.combo.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.combo.bean;

/**
 * Nome: TipoParticipacaoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class TipoParticipacaoSaidaDTO {
	
	/** Atributo cdTipoParticipacao. */
	private int cdTipoParticipacao;
	
	/** Atributo dsTipoParticipacao. */
	private String dsTipoParticipacao;
	
	/**
	 * Tipo participacao saida dto.
	 */
	public TipoParticipacaoSaidaDTO(){
		super();
	}
	
	/**
	 * Tipo participacao saida dto.
	 *
	 * @param cdTipoParticipacao the cd tipo participacao
	 * @param dsTipoParticipacao the ds tipo participacao
	 */
	public TipoParticipacaoSaidaDTO(int cdTipoParticipacao, String dsTipoParticipacao){
		super();
		this.cdTipoParticipacao = cdTipoParticipacao;
		this.dsTipoParticipacao = dsTipoParticipacao;
	}
	
	/**
	 * Get: cdTipoParticipacao.
	 *
	 * @return cdTipoParticipacao
	 */
	public int getCdTipoParticipacao() {
		return cdTipoParticipacao;
	}
	
	/**
	 * Set: cdTipoParticipacao.
	 *
	 * @param cdTipoParticipacao the cd tipo participacao
	 */
	public void setCdTipoParticipacao(int cdTipoParticipacao) {
		this.cdTipoParticipacao = cdTipoParticipacao;
	}
	
	/**
	 * Get: dsTipoParticipacao.
	 *
	 * @return dsTipoParticipacao
	 */
	public String getDsTipoParticipacao() {
		return dsTipoParticipacao;
	}
	
	/**
	 * Set: dsTipoParticipacao.
	 *
	 * @param dsTipoParticipacao the ds tipo participacao
	 */
	public void setDsTipoParticipacao(String dsTipoParticipacao) {
		this.dsTipoParticipacao = dsTipoParticipacao;
	}
	
}
