/*
 * Nome: br.com.bradesco.web.pgit.service.business.assoclinhatipomsg.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.assoclinhatipomsg.bean;

/**
 * Nome: ListarLinhaMsgComprovanteEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarLinhaMsgComprovanteEntradaDTO {
	
	/** Atributo tipoMensagem. */
	private int tipoMensagem;
	
	/** Atributo centroCusto. */
	private String centroCusto;

	/**
	 * Get: tipoMensagem.
	 *
	 * @return tipoMensagem
	 */
	public int getTipoMensagem() {
		return tipoMensagem;
	}

	/**
	 * Set: tipoMensagem.
	 *
	 * @param tipoMensagem the tipo mensagem
	 */
	public void setTipoMensagem(int tipoMensagem) {
		this.tipoMensagem = tipoMensagem;
	}

	/**
	 * Get: centroCusto.
	 *
	 * @return centroCusto
	 */
	public String getCentroCusto() {
		return centroCusto;
	}

	/**
	 * Set: centroCusto.
	 *
	 * @param centroCusto the centro custo
	 */
	public void setCentroCusto(String centroCusto) {
		this.centroCusto = centroCusto;
	}
	
	

}
