package br.com.bradesco.web.pgit.service.business.mantervinculacaoconveniocontasalario.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.ListarConvenioContaSalarioResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.Ocorrencias6;

public class OcorrenciasDTO6 implements Serializable{
	
	private static final long serialVersionUID = -3987630654535077229L;
	
	Integer cdCrenAtendimentoQuest; 
	String dsCrenAtendimentoQuest; 
	Long qtdCrenQuest; 
	Long qtdFuncCrenQuest; 
	Integer cdUnidadeMedAgQuest; 
	Integer cdDistcCrenQuest;
	
	public OcorrenciasDTO6() {
		super();
	}

	public OcorrenciasDTO6(Integer cdCrenAtendimentoQuest,
			String dsCrenAtendimentoQuest, Long qtdCrenQuest,
			Long qtdFuncCrenQuest, Integer cdUnidadeMedAgQuest,
			Integer cdDistcCrenQuest) {
		super();
		this.cdCrenAtendimentoQuest = cdCrenAtendimentoQuest;
		this.dsCrenAtendimentoQuest = dsCrenAtendimentoQuest;
		this.qtdCrenQuest = qtdCrenQuest;
		this.qtdFuncCrenQuest = qtdFuncCrenQuest;
		this.cdUnidadeMedAgQuest = cdUnidadeMedAgQuest;
		this.cdDistcCrenQuest = cdDistcCrenQuest;
	}
	
	public List<OcorrenciasDTO6> listarOcorrenciasDTO6(ListarConvenioContaSalarioResponse response) {
		
		List<OcorrenciasDTO6> listaOcorrenciasDTO6 = new ArrayList<OcorrenciasDTO6>();
		OcorrenciasDTO6 ocorrenciasDTO6 = new OcorrenciasDTO6();
		
		for(Ocorrencias6 retorno : response.getOcorrencias6()){
			ocorrenciasDTO6.setCdCrenAtendimentoQuest(retorno.getCdCrenAtendimentoQuest());
			ocorrenciasDTO6.setDsCrenAtendimentoQuest(retorno.getDsCrenAtendimentoQuest());
			ocorrenciasDTO6.setQtdCrenQuest(retorno.getQtdCrenQuest());
			ocorrenciasDTO6.setQtdFuncCrenQuest(retorno.getQtdFuncCrenQuest());
			ocorrenciasDTO6.setCdUnidadeMedAgQuest(retorno.getCdUnidadeMedAgQuest());
			ocorrenciasDTO6.setCdDistcCrenQuest(retorno.getCdDistcCrenQuest());
			listaOcorrenciasDTO6.add(ocorrenciasDTO6);
		}
		return listaOcorrenciasDTO6;
	}
	

	public Integer getCdCrenAtendimentoQuest() {
		return cdCrenAtendimentoQuest;
	}

	public void setCdCrenAtendimentoQuest(Integer cdCrenAtendimentoQuest) {
		this.cdCrenAtendimentoQuest = cdCrenAtendimentoQuest;
	}

	public String getDsCrenAtendimentoQuest() {
		return dsCrenAtendimentoQuest;
	}

	public void setDsCrenAtendimentoQuest(String dsCrenAtendimentoQuest) {
		this.dsCrenAtendimentoQuest = dsCrenAtendimentoQuest;
	}

	public Long getQtdCrenQuest() {
		return qtdCrenQuest;
	}

	public void setQtdCrenQuest(Long qtdCrenQuest) {
		this.qtdCrenQuest = qtdCrenQuest;
	}

	public Long getQtdFuncCrenQuest() {
		return qtdFuncCrenQuest;
	}

	public void setQtdFuncCrenQuest(Long qtdFuncCrenQuest) {
		this.qtdFuncCrenQuest = qtdFuncCrenQuest;
	}

	public Integer getCdUnidadeMedAgQuest() {
		return cdUnidadeMedAgQuest;
	}

	public void setCdUnidadeMedAgQuest(Integer cdUnidadeMedAgQuest) {
		this.cdUnidadeMedAgQuest = cdUnidadeMedAgQuest;
	}

	public Integer getCdDistcCrenQuest() {
		return cdDistcCrenQuest;
	}

	public void setCdDistcCrenQuest(Integer cdDistcCrenQuest) {
		this.cdDistcCrenQuest = cdDistcCrenQuest;
	}

}
