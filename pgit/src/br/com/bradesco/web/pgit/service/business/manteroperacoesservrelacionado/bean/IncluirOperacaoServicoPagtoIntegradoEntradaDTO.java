/*
 * Nome: br.com.bradesco.web.pgit.service.business.manteroperacoesservrelacionado.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.manteroperacoesservrelacionado.bean;

import java.math.BigDecimal;

/**
 * Nome: IncluirOperacaoServicoPagtoIntegradoEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class IncluirOperacaoServicoPagtoIntegradoEntradaDTO{
	
	/** Atributo cdprodutoServicoOperacao. */
	private Integer cdprodutoServicoOperacao;
	
	/** Atributo cdProdutoOperacaoRelacionado. */
	private Integer cdProdutoOperacaoRelacionado;
	
	/** Atributo cdOperacaoProdutoServico. */
	private Integer cdOperacaoProdutoServico;
	
	/** Atributo cdOperacaoServicoIntegrado. */
	private Integer cdOperacaoServicoIntegrado;
	
	/** Atributo cdNaturezaOperacaoPagamento. */
	private Integer cdNaturezaOperacaoPagamento;
	
	/**
	 * Atributo Integer
	 */
	private Integer cdTipoAlcadaTarifa;

	/**
	 * Atributo BigDecimal
	 */
	private BigDecimal vrAlcadaTarifaAgencia;
	
	/**
	 * Atributo BigDecimal
	 */
	private BigDecimal pcAlcadaTarifaAgencia;

	/**
	 * Get: cdNaturezaOperacaoPagamento.
	 *
	 * @return cdNaturezaOperacaoPagamento
	 */
	public Integer getCdNaturezaOperacaoPagamento() {
	    return cdNaturezaOperacaoPagamento;
	}

	/**
	 * Set: cdNaturezaOperacaoPagamento.
	 *
	 * @param cdNaturezaOperacaoPagamento the cd natureza operacao pagamento
	 */
	public void setCdNaturezaOperacaoPagamento(Integer cdNaturezaOperacaoPagamento) {
	    this.cdNaturezaOperacaoPagamento = cdNaturezaOperacaoPagamento;
	}

	/**
	 * Get: cdprodutoServicoOperacao.
	 *
	 * @return cdprodutoServicoOperacao
	 */
	public Integer getCdprodutoServicoOperacao(){
		return cdprodutoServicoOperacao;
	}

	/**
	 * Set: cdprodutoServicoOperacao.
	 *
	 * @param cdprodutoServicoOperacao the cdproduto servico operacao
	 */
	public void setCdprodutoServicoOperacao(Integer cdprodutoServicoOperacao){
		this.cdprodutoServicoOperacao = cdprodutoServicoOperacao;
	}

	/**
	 * Get: cdProdutoOperacaoRelacionado.
	 *
	 * @return cdProdutoOperacaoRelacionado
	 */
	public Integer getCdProdutoOperacaoRelacionado(){
		return cdProdutoOperacaoRelacionado;
	}

	/**
	 * Set: cdProdutoOperacaoRelacionado.
	 *
	 * @param cdProdutoOperacaoRelacionado the cd produto operacao relacionado
	 */
	public void setCdProdutoOperacaoRelacionado(Integer cdProdutoOperacaoRelacionado){
		this.cdProdutoOperacaoRelacionado = cdProdutoOperacaoRelacionado;
	}

	/**
	 * Get: cdOperacaoProdutoServico.
	 *
	 * @return cdOperacaoProdutoServico
	 */
	public Integer getCdOperacaoProdutoServico(){
		return cdOperacaoProdutoServico;
	}

	/**
	 * Set: cdOperacaoProdutoServico.
	 *
	 * @param cdOperacaoProdutoServico the cd operacao produto servico
	 */
	public void setCdOperacaoProdutoServico(Integer cdOperacaoProdutoServico){
		this.cdOperacaoProdutoServico = cdOperacaoProdutoServico;
	}

	/**
	 * Get: cdOperacaoServicoIntegrado.
	 *
	 * @return cdOperacaoServicoIntegrado
	 */
	public Integer getCdOperacaoServicoIntegrado(){
		return cdOperacaoServicoIntegrado;
	}

	/**
	 * Set: cdOperacaoServicoIntegrado.
	 *
	 * @param cdOperacaoServicoIntegrado the cd operacao servico integrado
	 */
	public void setCdOperacaoServicoIntegrado(Integer cdOperacaoServicoIntegrado){
		this.cdOperacaoServicoIntegrado = cdOperacaoServicoIntegrado;
	}

    /**
     * Nome: getCdTipoAlcadaTarifa
     *
     * @return cdTipoAlcadaTarifa
     */
    public Integer getCdTipoAlcadaTarifa() {
        return cdTipoAlcadaTarifa;
    }

    /**
     * Nome: setCdTipoAlcadaTarifa
     *
     * @param cdTipoAlcadaTarifa
     */
    public void setCdTipoAlcadaTarifa(Integer cdTipoAlcadaTarifa) {
        this.cdTipoAlcadaTarifa = cdTipoAlcadaTarifa;
    }

    /**
     * Nome: getVrAlcadaTarifaAgencia
     *
     * @return vrAlcadaTarifaAgencia
     */
    public BigDecimal getVrAlcadaTarifaAgencia() {
        return vrAlcadaTarifaAgencia;
    }

    /**
     * Nome: setVrAlcadaTarifaAgencia
     *
     * @param vrAlcadaTarifaAgencia
     */
    public void setVrAlcadaTarifaAgencia(BigDecimal vrAlcadaTarifaAgencia) {
        this.vrAlcadaTarifaAgencia = vrAlcadaTarifaAgencia;
    }

    /**
     * Nome: getPcAlcadaTarifaAgencia
     *
     * @return pcAlcadaTarifaAgencia
     */
    public BigDecimal getPcAlcadaTarifaAgencia() {
        return pcAlcadaTarifaAgencia;
    }

    /**
     * Nome: setPcAlcadaTarifaAgencia
     *
     * @param pcAlcadaTarifaAgencia
     */
    public void setPcAlcadaTarifaAgencia(BigDecimal pcAlcadaTarifaAgencia) {
        this.pcAlcadaTarifaAgencia = pcAlcadaTarifaAgencia;
    }
}