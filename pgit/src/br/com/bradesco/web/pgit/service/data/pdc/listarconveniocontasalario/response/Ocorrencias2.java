/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.math.BigDecimal;

import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;

/**
 * Class Ocorrencias2.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias2 implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
	 * 
	 */
	private static final long serialVersionUID = -9137900581328163700L;

	/**
     * Field _cdFaixaRenovQuest
     */
    private int _cdFaixaRenovQuest = 0;

    /**
     * keeps track of state for field: _cdFaixaRenovQuest
     */
    private boolean _has_cdFaixaRenovQuest;

    /**
     * Field _dsFaixaRenovQuest
     */
    private java.lang.String _dsFaixaRenovQuest;

    /**
     * Field _qtdFuncCasaQuest
     */
    private long _qtdFuncCasaQuest = 0;

    /**
     * keeps track of state for field: _qtdFuncCasaQuest
     */
    private boolean _has_qtdFuncCasaQuest;

    /**
     * Field _qtdFuncDeslgQuest
     */
    private long _qtdFuncDeslgQuest = 0;

    /**
     * keeps track of state for field: _qtdFuncDeslgQuest
     */
    private boolean _has_qtdFuncDeslgQuest;

    /**
     * Field _ptcRenovFaixaQuest
     */
    private java.math.BigDecimal _ptcRenovFaixaQuest = new java.math.BigDecimal("0");


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias2() 
     {
        super();
        setPtcRenovFaixaQuest(new java.math.BigDecimal("0"));
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.Ocorrencias2()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdFaixaRenovQuest
     * 
     */
    public void deleteCdFaixaRenovQuest()
    {
        this._has_cdFaixaRenovQuest= false;
    } //-- void deleteCdFaixaRenovQuest() 

    /**
     * Method deleteQtdFuncCasaQuest
     * 
     */
    public void deleteQtdFuncCasaQuest()
    {
        this._has_qtdFuncCasaQuest= false;
    } //-- void deleteQtdFuncCasaQuest() 

    /**
     * Method deleteQtdFuncDeslgQuest
     * 
     */
    public void deleteQtdFuncDeslgQuest()
    {
        this._has_qtdFuncDeslgQuest= false;
    } //-- void deleteQtdFuncDeslgQuest() 

    /**
     * Returns the value of field 'cdFaixaRenovQuest'.
     * 
     * @return int
     * @return the value of field 'cdFaixaRenovQuest'.
     */
    public int getCdFaixaRenovQuest()
    {
        return this._cdFaixaRenovQuest;
    } //-- int getCdFaixaRenovQuest() 

    /**
     * Returns the value of field 'dsFaixaRenovQuest'.
     * 
     * @return String
     * @return the value of field 'dsFaixaRenovQuest'.
     */
    public java.lang.String getDsFaixaRenovQuest()
    {
        return this._dsFaixaRenovQuest;
    } //-- java.lang.String getDsFaixaRenovQuest() 

    /**
     * Returns the value of field 'ptcRenovFaixaQuest'.
     * 
     * @return BigDecimal
     * @return the value of field 'ptcRenovFaixaQuest'.
     */
    public java.math.BigDecimal getPtcRenovFaixaQuest()
    {
        return this._ptcRenovFaixaQuest;
    } //-- java.math.BigDecimal getPtcRenovFaixaQuest() 

    /**
     * Returns the value of field 'qtdFuncCasaQuest'.
     * 
     * @return long
     * @return the value of field 'qtdFuncCasaQuest'.
     */
    public long getQtdFuncCasaQuest()
    {
        return this._qtdFuncCasaQuest;
    } //-- long getQtdFuncCasaQuest() 

    /**
     * Returns the value of field 'qtdFuncDeslgQuest'.
     * 
     * @return long
     * @return the value of field 'qtdFuncDeslgQuest'.
     */
    public long getQtdFuncDeslgQuest()
    {
        return this._qtdFuncDeslgQuest;
    } //-- long getQtdFuncDeslgQuest() 

    /**
     * Method hasCdFaixaRenovQuest
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFaixaRenovQuest()
    {
        return this._has_cdFaixaRenovQuest;
    } //-- boolean hasCdFaixaRenovQuest() 

    /**
     * Method hasQtdFuncCasaQuest
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtdFuncCasaQuest()
    {
        return this._has_qtdFuncCasaQuest;
    } //-- boolean hasQtdFuncCasaQuest() 

    /**
     * Method hasQtdFuncDeslgQuest
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtdFuncDeslgQuest()
    {
        return this._has_qtdFuncDeslgQuest;
    } //-- boolean hasQtdFuncDeslgQuest() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdFaixaRenovQuest'.
     * 
     * @param cdFaixaRenovQuest the value of field
     * 'cdFaixaRenovQuest'.
     */
    public void setCdFaixaRenovQuest(int cdFaixaRenovQuest)
    {
        this._cdFaixaRenovQuest = cdFaixaRenovQuest;
        this._has_cdFaixaRenovQuest = true;
    } //-- void setCdFaixaRenovQuest(int) 

    /**
     * Sets the value of field 'dsFaixaRenovQuest'.
     * 
     * @param dsFaixaRenovQuest the value of field
     * 'dsFaixaRenovQuest'.
     */
    public void setDsFaixaRenovQuest(java.lang.String dsFaixaRenovQuest)
    {
        this._dsFaixaRenovQuest = dsFaixaRenovQuest;
    } //-- void setDsFaixaRenovQuest(java.lang.String) 

    /**
     * Sets the value of field 'ptcRenovFaixaQuest'.
     * 
     * @param ptcRenovFaixaQuest the value of field
     * 'ptcRenovFaixaQuest'.
     */
    public void setPtcRenovFaixaQuest(java.math.BigDecimal ptcRenovFaixaQuest)
    {
        this._ptcRenovFaixaQuest = ptcRenovFaixaQuest;
    } //-- void setPtcRenovFaixaQuest(java.math.BigDecimal) 

    /**
     * Sets the value of field 'qtdFuncCasaQuest'.
     * 
     * @param qtdFuncCasaQuest the value of field 'qtdFuncCasaQuest'
     */
    public void setQtdFuncCasaQuest(long qtdFuncCasaQuest)
    {
        this._qtdFuncCasaQuest = qtdFuncCasaQuest;
        this._has_qtdFuncCasaQuest = true;
    } //-- void setQtdFuncCasaQuest(long) 

    /**
     * Sets the value of field 'qtdFuncDeslgQuest'.
     * 
     * @param qtdFuncDeslgQuest the value of field
     * 'qtdFuncDeslgQuest'.
     */
    public void setQtdFuncDeslgQuest(long qtdFuncDeslgQuest)
    {
        this._qtdFuncDeslgQuest = qtdFuncDeslgQuest;
        this._has_qtdFuncDeslgQuest = true;
    } //-- void setQtdFuncDeslgQuest(long) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias2
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.Ocorrencias2 unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.Ocorrencias2) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.Ocorrencias2.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.Ocorrencias2 unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 


	public Ocorrencias2(int cdFaixaRenovQuest, String dsFaixaRenovQuest, long qtdFuncCasaQuest,long qtdFuncDeslgQuest, BigDecimal ptcRenovFaixaQuest) {
		super();
		_cdFaixaRenovQuest = cdFaixaRenovQuest;
		_dsFaixaRenovQuest = dsFaixaRenovQuest;
		_qtdFuncCasaQuest = qtdFuncCasaQuest;
		_qtdFuncDeslgQuest = qtdFuncDeslgQuest;
		_ptcRenovFaixaQuest = ptcRenovFaixaQuest;
	}

}
