/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultarempresaacompanhada.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdCnpjCpf
     */
    private long _cdCnpjCpf = 0;

    /**
     * keeps track of state for field: _cdCnpjCpf
     */
    private boolean _has_cdCnpjCpf;

    /**
     * Field _cdFilialCpfCnpj
     */
    private int _cdFilialCpfCnpj = 0;

    /**
     * keeps track of state for field: _cdFilialCpfCnpj
     */
    private boolean _has_cdFilialCpfCnpj;

    /**
     * Field _cdCtrlCpfCnpjSaida
     */
    private int _cdCtrlCpfCnpjSaida = 0;

    /**
     * keeps track of state for field: _cdCtrlCpfCnpjSaida
     */
    private boolean _has_cdCtrlCpfCnpjSaida;

    /**
     * Field _dsRazaoSocial
     */
    private java.lang.String _dsRazaoSocial;

    /**
     * Field _cdAgenciaSaida
     */
    private int _cdAgenciaSaida = 0;

    /**
     * keeps track of state for field: _cdAgenciaSaida
     */
    private boolean _has_cdAgenciaSaida;

    /**
     * Field _cdContaCorrenteSaida
     */
    private int _cdContaCorrenteSaida = 0;

    /**
     * keeps track of state for field: _cdContaCorrenteSaida
     */
    private boolean _has_cdContaCorrenteSaida;

    /**
     * Field _cdDigitoConta
     */
    private java.lang.String _cdDigitoConta;

    /**
     * Field _cdConveCtaSalarial
     */
    private long _cdConveCtaSalarial = 0;

    /**
     * keeps track of state for field: _cdConveCtaSalarial
     */
    private boolean _has_cdConveCtaSalarial;

    /**
     * Field _cdIndicadorPrevidencia
     */
    private java.lang.String _cdIndicadorPrevidencia;

    /**
     * Field _dtInclusao
     */
    private java.lang.String _dtInclusao;

    /**
     * Field _hrInclusao
     */
    private java.lang.String _hrInclusao;

    /**
     * Field _cdUsuarioInclusao
     */
    private java.lang.String _cdUsuarioInclusao;

    /**
     * Field _cdUsuarioInclusaoExterno
     */
    private java.lang.String _cdUsuarioInclusaoExterno;

    /**
     * Field _cdOperacaoCanalInclusao
     */
    private java.lang.String _cdOperacaoCanalInclusao;

    /**
     * Field _dsTipoCanalInclusao
     */
    private java.lang.String _dsTipoCanalInclusao;

    /**
     * Field _dsComplementoInclusao
     */
    private int _dsComplementoInclusao = 0;

    /**
     * keeps track of state for field: _dsComplementoInclusao
     */
    private boolean _has_dsComplementoInclusao;

    /**
     * Field _dtManutencao
     */
    private java.lang.String _dtManutencao;

    /**
     * Field _hrManutencao
     */
    private java.lang.String _hrManutencao;

    /**
     * Field _cdUsuarioManutencao
     */
    private java.lang.String _cdUsuarioManutencao;

    /**
     * Field _cdUsuarioManutencaoexterno
     */
    private java.lang.String _cdUsuarioManutencaoexterno;

    /**
     * Field _cdOperacaoCanalManutencao
     */
    private java.lang.String _cdOperacaoCanalManutencao;

    /**
     * Field _dsTipoCanalManutencao
     */
    private java.lang.String _dsTipoCanalManutencao;

    /**
     * Field _dsComplementoManutencao
     */
    private int _dsComplementoManutencao = 0;

    /**
     * keeps track of state for field: _dsComplementoManutencao
     */
    private boolean _has_dsComplementoManutencao;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarempresaacompanhada.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdAgenciaSaida
     * 
     */
    public void deleteCdAgenciaSaida()
    {
        this._has_cdAgenciaSaida= false;
    } //-- void deleteCdAgenciaSaida() 

    /**
     * Method deleteCdCnpjCpf
     * 
     */
    public void deleteCdCnpjCpf()
    {
        this._has_cdCnpjCpf= false;
    } //-- void deleteCdCnpjCpf() 

    /**
     * Method deleteCdContaCorrenteSaida
     * 
     */
    public void deleteCdContaCorrenteSaida()
    {
        this._has_cdContaCorrenteSaida= false;
    } //-- void deleteCdContaCorrenteSaida() 

    /**
     * Method deleteCdConveCtaSalarial
     * 
     */
    public void deleteCdConveCtaSalarial()
    {
        this._has_cdConveCtaSalarial= false;
    } //-- void deleteCdConveCtaSalarial() 

    /**
     * Method deleteCdCtrlCpfCnpjSaida
     * 
     */
    public void deleteCdCtrlCpfCnpjSaida()
    {
        this._has_cdCtrlCpfCnpjSaida= false;
    } //-- void deleteCdCtrlCpfCnpjSaida() 

    /**
     * Method deleteCdFilialCpfCnpj
     * 
     */
    public void deleteCdFilialCpfCnpj()
    {
        this._has_cdFilialCpfCnpj= false;
    } //-- void deleteCdFilialCpfCnpj() 

    /**
     * Method deleteDsComplementoInclusao
     * 
     */
    public void deleteDsComplementoInclusao()
    {
        this._has_dsComplementoInclusao= false;
    } //-- void deleteDsComplementoInclusao() 

    /**
     * Method deleteDsComplementoManutencao
     * 
     */
    public void deleteDsComplementoManutencao()
    {
        this._has_dsComplementoManutencao= false;
    } //-- void deleteDsComplementoManutencao() 

    /**
     * Returns the value of field 'cdAgenciaSaida'.
     * 
     * @return int
     * @return the value of field 'cdAgenciaSaida'.
     */
    public int getCdAgenciaSaida()
    {
        return this._cdAgenciaSaida;
    } //-- int getCdAgenciaSaida() 

    /**
     * Returns the value of field 'cdCnpjCpf'.
     * 
     * @return long
     * @return the value of field 'cdCnpjCpf'.
     */
    public long getCdCnpjCpf()
    {
        return this._cdCnpjCpf;
    } //-- long getCdCnpjCpf() 

    /**
     * Returns the value of field 'cdContaCorrenteSaida'.
     * 
     * @return int
     * @return the value of field 'cdContaCorrenteSaida'.
     */
    public int getCdContaCorrenteSaida()
    {
        return this._cdContaCorrenteSaida;
    } //-- int getCdContaCorrenteSaida() 

    /**
     * Returns the value of field 'cdConveCtaSalarial'.
     * 
     * @return long
     * @return the value of field 'cdConveCtaSalarial'.
     */
    public long getCdConveCtaSalarial()
    {
        return this._cdConveCtaSalarial;
    } //-- long getCdConveCtaSalarial() 

    /**
     * Returns the value of field 'cdCtrlCpfCnpjSaida'.
     * 
     * @return int
     * @return the value of field 'cdCtrlCpfCnpjSaida'.
     */
    public int getCdCtrlCpfCnpjSaida()
    {
        return this._cdCtrlCpfCnpjSaida;
    } //-- int getCdCtrlCpfCnpjSaida() 

    /**
     * Returns the value of field 'cdDigitoConta'.
     * 
     * @return String
     * @return the value of field 'cdDigitoConta'.
     */
    public java.lang.String getCdDigitoConta()
    {
        return this._cdDigitoConta;
    } //-- java.lang.String getCdDigitoConta() 

    /**
     * Returns the value of field 'cdFilialCpfCnpj'.
     * 
     * @return int
     * @return the value of field 'cdFilialCpfCnpj'.
     */
    public int getCdFilialCpfCnpj()
    {
        return this._cdFilialCpfCnpj;
    } //-- int getCdFilialCpfCnpj() 

    /**
     * Returns the value of field 'cdIndicadorPrevidencia'.
     * 
     * @return String
     * @return the value of field 'cdIndicadorPrevidencia'.
     */
    public java.lang.String getCdIndicadorPrevidencia()
    {
        return this._cdIndicadorPrevidencia;
    } //-- java.lang.String getCdIndicadorPrevidencia() 

    /**
     * Returns the value of field 'cdOperacaoCanalInclusao'.
     * 
     * @return String
     * @return the value of field 'cdOperacaoCanalInclusao'.
     */
    public java.lang.String getCdOperacaoCanalInclusao()
    {
        return this._cdOperacaoCanalInclusao;
    } //-- java.lang.String getCdOperacaoCanalInclusao() 

    /**
     * Returns the value of field 'cdOperacaoCanalManutencao'.
     * 
     * @return String
     * @return the value of field 'cdOperacaoCanalManutencao'.
     */
    public java.lang.String getCdOperacaoCanalManutencao()
    {
        return this._cdOperacaoCanalManutencao;
    } //-- java.lang.String getCdOperacaoCanalManutencao() 

    /**
     * Returns the value of field 'cdUsuarioInclusao'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioInclusao'.
     */
    public java.lang.String getCdUsuarioInclusao()
    {
        return this._cdUsuarioInclusao;
    } //-- java.lang.String getCdUsuarioInclusao() 

    /**
     * Returns the value of field 'cdUsuarioInclusaoExterno'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioInclusaoExterno'.
     */
    public java.lang.String getCdUsuarioInclusaoExterno()
    {
        return this._cdUsuarioInclusaoExterno;
    } //-- java.lang.String getCdUsuarioInclusaoExterno() 

    /**
     * Returns the value of field 'cdUsuarioManutencao'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioManutencao'.
     */
    public java.lang.String getCdUsuarioManutencao()
    {
        return this._cdUsuarioManutencao;
    } //-- java.lang.String getCdUsuarioManutencao() 

    /**
     * Returns the value of field 'cdUsuarioManutencaoexterno'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioManutencaoexterno'.
     */
    public java.lang.String getCdUsuarioManutencaoexterno()
    {
        return this._cdUsuarioManutencaoexterno;
    } //-- java.lang.String getCdUsuarioManutencaoexterno() 

    /**
     * Returns the value of field 'dsComplementoInclusao'.
     * 
     * @return int
     * @return the value of field 'dsComplementoInclusao'.
     */
    public int getDsComplementoInclusao()
    {
        return this._dsComplementoInclusao;
    } //-- int getDsComplementoInclusao() 

    /**
     * Returns the value of field 'dsComplementoManutencao'.
     * 
     * @return int
     * @return the value of field 'dsComplementoManutencao'.
     */
    public int getDsComplementoManutencao()
    {
        return this._dsComplementoManutencao;
    } //-- int getDsComplementoManutencao() 

    /**
     * Returns the value of field 'dsRazaoSocial'.
     * 
     * @return String
     * @return the value of field 'dsRazaoSocial'.
     */
    public java.lang.String getDsRazaoSocial()
    {
        return this._dsRazaoSocial;
    } //-- java.lang.String getDsRazaoSocial() 

    /**
     * Returns the value of field 'dsTipoCanalInclusao'.
     * 
     * @return String
     * @return the value of field 'dsTipoCanalInclusao'.
     */
    public java.lang.String getDsTipoCanalInclusao()
    {
        return this._dsTipoCanalInclusao;
    } //-- java.lang.String getDsTipoCanalInclusao() 

    /**
     * Returns the value of field 'dsTipoCanalManutencao'.
     * 
     * @return String
     * @return the value of field 'dsTipoCanalManutencao'.
     */
    public java.lang.String getDsTipoCanalManutencao()
    {
        return this._dsTipoCanalManutencao;
    } //-- java.lang.String getDsTipoCanalManutencao() 

    /**
     * Returns the value of field 'dtInclusao'.
     * 
     * @return String
     * @return the value of field 'dtInclusao'.
     */
    public java.lang.String getDtInclusao()
    {
        return this._dtInclusao;
    } //-- java.lang.String getDtInclusao() 

    /**
     * Returns the value of field 'dtManutencao'.
     * 
     * @return String
     * @return the value of field 'dtManutencao'.
     */
    public java.lang.String getDtManutencao()
    {
        return this._dtManutencao;
    } //-- java.lang.String getDtManutencao() 

    /**
     * Returns the value of field 'hrInclusao'.
     * 
     * @return String
     * @return the value of field 'hrInclusao'.
     */
    public java.lang.String getHrInclusao()
    {
        return this._hrInclusao;
    } //-- java.lang.String getHrInclusao() 

    /**
     * Returns the value of field 'hrManutencao'.
     * 
     * @return String
     * @return the value of field 'hrManutencao'.
     */
    public java.lang.String getHrManutencao()
    {
        return this._hrManutencao;
    } //-- java.lang.String getHrManutencao() 

    /**
     * Method hasCdAgenciaSaida
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgenciaSaida()
    {
        return this._has_cdAgenciaSaida;
    } //-- boolean hasCdAgenciaSaida() 

    /**
     * Method hasCdCnpjCpf
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCnpjCpf()
    {
        return this._has_cdCnpjCpf;
    } //-- boolean hasCdCnpjCpf() 

    /**
     * Method hasCdContaCorrenteSaida
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdContaCorrenteSaida()
    {
        return this._has_cdContaCorrenteSaida;
    } //-- boolean hasCdContaCorrenteSaida() 

    /**
     * Method hasCdConveCtaSalarial
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdConveCtaSalarial()
    {
        return this._has_cdConveCtaSalarial;
    } //-- boolean hasCdConveCtaSalarial() 

    /**
     * Method hasCdCtrlCpfCnpjSaida
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCtrlCpfCnpjSaida()
    {
        return this._has_cdCtrlCpfCnpjSaida;
    } //-- boolean hasCdCtrlCpfCnpjSaida() 

    /**
     * Method hasCdFilialCpfCnpj
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFilialCpfCnpj()
    {
        return this._has_cdFilialCpfCnpj;
    } //-- boolean hasCdFilialCpfCnpj() 

    /**
     * Method hasDsComplementoInclusao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasDsComplementoInclusao()
    {
        return this._has_dsComplementoInclusao;
    } //-- boolean hasDsComplementoInclusao() 

    /**
     * Method hasDsComplementoManutencao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasDsComplementoManutencao()
    {
        return this._has_dsComplementoManutencao;
    } //-- boolean hasDsComplementoManutencao() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdAgenciaSaida'.
     * 
     * @param cdAgenciaSaida the value of field 'cdAgenciaSaida'.
     */
    public void setCdAgenciaSaida(int cdAgenciaSaida)
    {
        this._cdAgenciaSaida = cdAgenciaSaida;
        this._has_cdAgenciaSaida = true;
    } //-- void setCdAgenciaSaida(int) 

    /**
     * Sets the value of field 'cdCnpjCpf'.
     * 
     * @param cdCnpjCpf the value of field 'cdCnpjCpf'.
     */
    public void setCdCnpjCpf(long cdCnpjCpf)
    {
        this._cdCnpjCpf = cdCnpjCpf;
        this._has_cdCnpjCpf = true;
    } //-- void setCdCnpjCpf(long) 

    /**
     * Sets the value of field 'cdContaCorrenteSaida'.
     * 
     * @param cdContaCorrenteSaida the value of field
     * 'cdContaCorrenteSaida'.
     */
    public void setCdContaCorrenteSaida(int cdContaCorrenteSaida)
    {
        this._cdContaCorrenteSaida = cdContaCorrenteSaida;
        this._has_cdContaCorrenteSaida = true;
    } //-- void setCdContaCorrenteSaida(int) 

    /**
     * Sets the value of field 'cdConveCtaSalarial'.
     * 
     * @param cdConveCtaSalarial the value of field
     * 'cdConveCtaSalarial'.
     */
    public void setCdConveCtaSalarial(long cdConveCtaSalarial)
    {
        this._cdConveCtaSalarial = cdConveCtaSalarial;
        this._has_cdConveCtaSalarial = true;
    } //-- void setCdConveCtaSalarial(long) 

    /**
     * Sets the value of field 'cdCtrlCpfCnpjSaida'.
     * 
     * @param cdCtrlCpfCnpjSaida the value of field
     * 'cdCtrlCpfCnpjSaida'.
     */
    public void setCdCtrlCpfCnpjSaida(int cdCtrlCpfCnpjSaida)
    {
        this._cdCtrlCpfCnpjSaida = cdCtrlCpfCnpjSaida;
        this._has_cdCtrlCpfCnpjSaida = true;
    } //-- void setCdCtrlCpfCnpjSaida(int) 

    /**
     * Sets the value of field 'cdDigitoConta'.
     * 
     * @param cdDigitoConta the value of field 'cdDigitoConta'.
     */
    public void setCdDigitoConta(java.lang.String cdDigitoConta)
    {
        this._cdDigitoConta = cdDigitoConta;
    } //-- void setCdDigitoConta(java.lang.String) 

    /**
     * Sets the value of field 'cdFilialCpfCnpj'.
     * 
     * @param cdFilialCpfCnpj the value of field 'cdFilialCpfCnpj'.
     */
    public void setCdFilialCpfCnpj(int cdFilialCpfCnpj)
    {
        this._cdFilialCpfCnpj = cdFilialCpfCnpj;
        this._has_cdFilialCpfCnpj = true;
    } //-- void setCdFilialCpfCnpj(int) 

    /**
     * Sets the value of field 'cdIndicadorPrevidencia'.
     * 
     * @param cdIndicadorPrevidencia the value of field
     * 'cdIndicadorPrevidencia'.
     */
    public void setCdIndicadorPrevidencia(java.lang.String cdIndicadorPrevidencia)
    {
        this._cdIndicadorPrevidencia = cdIndicadorPrevidencia;
    } //-- void setCdIndicadorPrevidencia(java.lang.String) 

    /**
     * Sets the value of field 'cdOperacaoCanalInclusao'.
     * 
     * @param cdOperacaoCanalInclusao the value of field
     * 'cdOperacaoCanalInclusao'.
     */
    public void setCdOperacaoCanalInclusao(java.lang.String cdOperacaoCanalInclusao)
    {
        this._cdOperacaoCanalInclusao = cdOperacaoCanalInclusao;
    } //-- void setCdOperacaoCanalInclusao(java.lang.String) 

    /**
     * Sets the value of field 'cdOperacaoCanalManutencao'.
     * 
     * @param cdOperacaoCanalManutencao the value of field
     * 'cdOperacaoCanalManutencao'.
     */
    public void setCdOperacaoCanalManutencao(java.lang.String cdOperacaoCanalManutencao)
    {
        this._cdOperacaoCanalManutencao = cdOperacaoCanalManutencao;
    } //-- void setCdOperacaoCanalManutencao(java.lang.String) 

    /**
     * Sets the value of field 'cdUsuarioInclusao'.
     * 
     * @param cdUsuarioInclusao the value of field
     * 'cdUsuarioInclusao'.
     */
    public void setCdUsuarioInclusao(java.lang.String cdUsuarioInclusao)
    {
        this._cdUsuarioInclusao = cdUsuarioInclusao;
    } //-- void setCdUsuarioInclusao(java.lang.String) 

    /**
     * Sets the value of field 'cdUsuarioInclusaoExterno'.
     * 
     * @param cdUsuarioInclusaoExterno the value of field
     * 'cdUsuarioInclusaoExterno'.
     */
    public void setCdUsuarioInclusaoExterno(java.lang.String cdUsuarioInclusaoExterno)
    {
        this._cdUsuarioInclusaoExterno = cdUsuarioInclusaoExterno;
    } //-- void setCdUsuarioInclusaoExterno(java.lang.String) 

    /**
     * Sets the value of field 'cdUsuarioManutencao'.
     * 
     * @param cdUsuarioManutencao the value of field
     * 'cdUsuarioManutencao'.
     */
    public void setCdUsuarioManutencao(java.lang.String cdUsuarioManutencao)
    {
        this._cdUsuarioManutencao = cdUsuarioManutencao;
    } //-- void setCdUsuarioManutencao(java.lang.String) 

    /**
     * Sets the value of field 'cdUsuarioManutencaoexterno'.
     * 
     * @param cdUsuarioManutencaoexterno the value of field
     * 'cdUsuarioManutencaoexterno'.
     */
    public void setCdUsuarioManutencaoexterno(java.lang.String cdUsuarioManutencaoexterno)
    {
        this._cdUsuarioManutencaoexterno = cdUsuarioManutencaoexterno;
    } //-- void setCdUsuarioManutencaoexterno(java.lang.String) 

    /**
     * Sets the value of field 'dsComplementoInclusao'.
     * 
     * @param dsComplementoInclusao the value of field
     * 'dsComplementoInclusao'.
     */
    public void setDsComplementoInclusao(int dsComplementoInclusao)
    {
        this._dsComplementoInclusao = dsComplementoInclusao;
        this._has_dsComplementoInclusao = true;
    } //-- void setDsComplementoInclusao(int) 

    /**
     * Sets the value of field 'dsComplementoManutencao'.
     * 
     * @param dsComplementoManutencao the value of field
     * 'dsComplementoManutencao'.
     */
    public void setDsComplementoManutencao(int dsComplementoManutencao)
    {
        this._dsComplementoManutencao = dsComplementoManutencao;
        this._has_dsComplementoManutencao = true;
    } //-- void setDsComplementoManutencao(int) 

    /**
     * Sets the value of field 'dsRazaoSocial'.
     * 
     * @param dsRazaoSocial the value of field 'dsRazaoSocial'.
     */
    public void setDsRazaoSocial(java.lang.String dsRazaoSocial)
    {
        this._dsRazaoSocial = dsRazaoSocial;
    } //-- void setDsRazaoSocial(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoCanalInclusao'.
     * 
     * @param dsTipoCanalInclusao the value of field
     * 'dsTipoCanalInclusao'.
     */
    public void setDsTipoCanalInclusao(java.lang.String dsTipoCanalInclusao)
    {
        this._dsTipoCanalInclusao = dsTipoCanalInclusao;
    } //-- void setDsTipoCanalInclusao(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoCanalManutencao'.
     * 
     * @param dsTipoCanalManutencao the value of field
     * 'dsTipoCanalManutencao'.
     */
    public void setDsTipoCanalManutencao(java.lang.String dsTipoCanalManutencao)
    {
        this._dsTipoCanalManutencao = dsTipoCanalManutencao;
    } //-- void setDsTipoCanalManutencao(java.lang.String) 

    /**
     * Sets the value of field 'dtInclusao'.
     * 
     * @param dtInclusao the value of field 'dtInclusao'.
     */
    public void setDtInclusao(java.lang.String dtInclusao)
    {
        this._dtInclusao = dtInclusao;
    } //-- void setDtInclusao(java.lang.String) 

    /**
     * Sets the value of field 'dtManutencao'.
     * 
     * @param dtManutencao the value of field 'dtManutencao'.
     */
    public void setDtManutencao(java.lang.String dtManutencao)
    {
        this._dtManutencao = dtManutencao;
    } //-- void setDtManutencao(java.lang.String) 

    /**
     * Sets the value of field 'hrInclusao'.
     * 
     * @param hrInclusao the value of field 'hrInclusao'.
     */
    public void setHrInclusao(java.lang.String hrInclusao)
    {
        this._hrInclusao = hrInclusao;
    } //-- void setHrInclusao(java.lang.String) 

    /**
     * Sets the value of field 'hrManutencao'.
     * 
     * @param hrManutencao the value of field 'hrManutencao'.
     */
    public void setHrManutencao(java.lang.String hrManutencao)
    {
        this._hrManutencao = hrManutencao;
    } //-- void setHrManutencao(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultarempresaacompanhada.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultarempresaacompanhada.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultarempresaacompanhada.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarempresaacompanhada.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
