/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.alterarperfilcontrato.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.math.BigDecimal;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class AlterarPerfilContratoRequest.
 * 
 * @version $Revision$ $Date$
 */
public class AlterarPerfilContratoRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdPerfilTrocaArquivo
     */
    private long _cdPerfilTrocaArquivo = 0;

    /**
     * keeps track of state for field: _cdPerfilTrocaArquivo
     */
    private boolean _has_cdPerfilTrocaArquivo;

    /**
     * Field _cdTipoLayoutArquivo
     */
    private int _cdTipoLayoutArquivo = 0;

    /**
     * keeps track of state for field: _cdTipoLayoutArquivo
     */
    private boolean _has_cdTipoLayoutArquivo;

    /**
     * Field _cdProdutoServicoOperacao
     */
    private int _cdProdutoServicoOperacao = 0;

    /**
     * keeps track of state for field: _cdProdutoServicoOperacao
     */
    private boolean _has_cdProdutoServicoOperacao;

    /**
     * Field _cdProdutoOperacaoRelacionado
     */
    private int _cdProdutoOperacaoRelacionado = 0;

    /**
     * keeps track of state for field: _cdProdutoOperacaoRelacionado
     */
    private boolean _has_cdProdutoOperacaoRelacionado;

    /**
     * Field _cdRelacionamentoProduto
     */
    private int _cdRelacionamentoProduto = 0;

    /**
     * keeps track of state for field: _cdRelacionamentoProduto
     */
    private boolean _has_cdRelacionamentoProduto;

    /**
     * Field _cdMeioPrincipalRemessa
     */
    private int _cdMeioPrincipalRemessa = 0;

    /**
     * keeps track of state for field: _cdMeioPrincipalRemessa
     */
    private boolean _has_cdMeioPrincipalRemessa;

    /**
     * Field _cdmeioAlternativoRemessa
     */
    private int _cdmeioAlternativoRemessa = 0;

    /**
     * keeps track of state for field: _cdmeioAlternativoRemessa
     */
    private boolean _has_cdmeioAlternativoRemessa;

    /**
     * Field _cdMeioPrincipalRetorno
     */
    private int _cdMeioPrincipalRetorno = 0;

    /**
     * keeps track of state for field: _cdMeioPrincipalRetorno
     */
    private boolean _has_cdMeioPrincipalRetorno;

    /**
     * Field _cdMeioAlternativoRetorno
     */
    private int _cdMeioAlternativoRetorno = 0;

    /**
     * keeps track of state for field: _cdMeioAlternativoRetorno
     */
    private boolean _has_cdMeioAlternativoRetorno;

    /**
     * Field _cdPessoaJuridicaParceiro
     */
    private long _cdPessoaJuridicaParceiro = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridicaParceiro
     */
    private boolean _has_cdPessoaJuridicaParceiro;

    /**
     * Field _cdEmpresaResponsavel
     */
    private int _cdEmpresaResponsavel = 0;

    /**
     * keeps track of state for field: _cdEmpresaResponsavel
     */
    private boolean _has_cdEmpresaResponsavel;

    /**
     * Field _cdCustoTransmicao
     */
    private java.math.BigDecimal _cdCustoTransmicao = new java.math.BigDecimal("0");

    /**
     * Field _cdApliFormat
     */
    private long _cdApliFormat = 0;

    /**
     * keeps track of state for field: _cdApliFormat
     */
    private boolean _has_cdApliFormat;

    /**
     * Field _cdPessoaJuridica
     */
    private long _cdPessoaJuridica = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridica
     */
    private boolean _has_cdPessoaJuridica;

    /**
     * Field _cdUnidadeOrganizacional
     */
    private int _cdUnidadeOrganizacional = 0;

    /**
     * keeps track of state for field: _cdUnidadeOrganizacional
     */
    private boolean _has_cdUnidadeOrganizacional;

    /**
     * Field _dsNomeContatoCliente
     */
    private java.lang.String _dsNomeContatoCliente;

    /**
     * Field _cdAreaFone
     */
    private int _cdAreaFone = 0;

    /**
     * keeps track of state for field: _cdAreaFone
     */
    private boolean _has_cdAreaFone;

    /**
     * Field _cdFoneContatoCliente
     */
    private long _cdFoneContatoCliente = 0;

    /**
     * keeps track of state for field: _cdFoneContatoCliente
     */
    private boolean _has_cdFoneContatoCliente;

    /**
     * Field _cdRamalContatoCliente
     */
    private java.lang.String _cdRamalContatoCliente;

    /**
     * Field _dsEmailContatoCliente
     */
    private java.lang.String _dsEmailContatoCliente;

    /**
     * Field _dsSolicitacaoPendente
     */
    private java.lang.String _dsSolicitacaoPendente;

    /**
     * Field _cdAreaFonePend
     */
    private int _cdAreaFonePend = 0;

    /**
     * keeps track of state for field: _cdAreaFonePend
     */
    private boolean _has_cdAreaFonePend;

    /**
     * Field _cdFoneSolicitacaoPend
     */
    private long _cdFoneSolicitacaoPend = 0;

    /**
     * keeps track of state for field: _cdFoneSolicitacaoPend
     */
    private boolean _has_cdFoneSolicitacaoPend;

    /**
     * Field _cdRamalSolctPend
     */
    private java.lang.String _cdRamalSolctPend;

    /**
     * Field _dsEmailSolctPend
     */
    private java.lang.String _dsEmailSolctPend;

    /**
     * Field _qtMesRegistroTrafg
     */
    private long _qtMesRegistroTrafg = 0;

    /**
     * keeps track of state for field: _qtMesRegistroTrafg
     */
    private boolean _has_qtMesRegistroTrafg;

    /**
     * Field _dsObsGeralPerfil
     */
    private java.lang.String _dsObsGeralPerfil;


      //----------------/
     //- Constructors -/
    //----------------/

    public AlterarPerfilContratoRequest() 
     {
        super();
        setCdCustoTransmicao(new java.math.BigDecimal("0"));
    } //-- br.com.bradesco.web.pgit.service.data.pdc.alterarperfilcontrato.request.AlterarPerfilContratoRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdApliFormat
     * 
     */
    public void deleteCdApliFormat()
    {
        this._has_cdApliFormat= false;
    } //-- void deleteCdApliFormat() 

    /**
     * Method deleteCdAreaFone
     * 
     */
    public void deleteCdAreaFone()
    {
        this._has_cdAreaFone= false;
    } //-- void deleteCdAreaFone() 

    /**
     * Method deleteCdAreaFonePend
     * 
     */
    public void deleteCdAreaFonePend()
    {
        this._has_cdAreaFonePend= false;
    } //-- void deleteCdAreaFonePend() 

    /**
     * Method deleteCdEmpresaResponsavel
     * 
     */
    public void deleteCdEmpresaResponsavel()
    {
        this._has_cdEmpresaResponsavel= false;
    } //-- void deleteCdEmpresaResponsavel() 

    /**
     * Method deleteCdFoneContatoCliente
     * 
     */
    public void deleteCdFoneContatoCliente()
    {
        this._has_cdFoneContatoCliente= false;
    } //-- void deleteCdFoneContatoCliente() 

    /**
     * Method deleteCdFoneSolicitacaoPend
     * 
     */
    public void deleteCdFoneSolicitacaoPend()
    {
        this._has_cdFoneSolicitacaoPend= false;
    } //-- void deleteCdFoneSolicitacaoPend() 

    /**
     * Method deleteCdMeioAlternativoRetorno
     * 
     */
    public void deleteCdMeioAlternativoRetorno()
    {
        this._has_cdMeioAlternativoRetorno= false;
    } //-- void deleteCdMeioAlternativoRetorno() 

    /**
     * Method deleteCdMeioPrincipalRemessa
     * 
     */
    public void deleteCdMeioPrincipalRemessa()
    {
        this._has_cdMeioPrincipalRemessa= false;
    } //-- void deleteCdMeioPrincipalRemessa() 

    /**
     * Method deleteCdMeioPrincipalRetorno
     * 
     */
    public void deleteCdMeioPrincipalRetorno()
    {
        this._has_cdMeioPrincipalRetorno= false;
    } //-- void deleteCdMeioPrincipalRetorno() 

    /**
     * Method deleteCdPerfilTrocaArquivo
     * 
     */
    public void deleteCdPerfilTrocaArquivo()
    {
        this._has_cdPerfilTrocaArquivo= false;
    } //-- void deleteCdPerfilTrocaArquivo() 

    /**
     * Method deleteCdPessoaJuridica
     * 
     */
    public void deleteCdPessoaJuridica()
    {
        this._has_cdPessoaJuridica= false;
    } //-- void deleteCdPessoaJuridica() 

    /**
     * Method deleteCdPessoaJuridicaParceiro
     * 
     */
    public void deleteCdPessoaJuridicaParceiro()
    {
        this._has_cdPessoaJuridicaParceiro= false;
    } //-- void deleteCdPessoaJuridicaParceiro() 

    /**
     * Method deleteCdProdutoOperacaoRelacionado
     * 
     */
    public void deleteCdProdutoOperacaoRelacionado()
    {
        this._has_cdProdutoOperacaoRelacionado= false;
    } //-- void deleteCdProdutoOperacaoRelacionado() 

    /**
     * Method deleteCdProdutoServicoOperacao
     * 
     */
    public void deleteCdProdutoServicoOperacao()
    {
        this._has_cdProdutoServicoOperacao= false;
    } //-- void deleteCdProdutoServicoOperacao() 

    /**
     * Method deleteCdRelacionamentoProduto
     * 
     */
    public void deleteCdRelacionamentoProduto()
    {
        this._has_cdRelacionamentoProduto= false;
    } //-- void deleteCdRelacionamentoProduto() 

    /**
     * Method deleteCdTipoLayoutArquivo
     * 
     */
    public void deleteCdTipoLayoutArquivo()
    {
        this._has_cdTipoLayoutArquivo= false;
    } //-- void deleteCdTipoLayoutArquivo() 

    /**
     * Method deleteCdUnidadeOrganizacional
     * 
     */
    public void deleteCdUnidadeOrganizacional()
    {
        this._has_cdUnidadeOrganizacional= false;
    } //-- void deleteCdUnidadeOrganizacional() 

    /**
     * Method deleteCdmeioAlternativoRemessa
     * 
     */
    public void deleteCdmeioAlternativoRemessa()
    {
        this._has_cdmeioAlternativoRemessa= false;
    } //-- void deleteCdmeioAlternativoRemessa() 

    /**
     * Method deleteQtMesRegistroTrafg
     * 
     */
    public void deleteQtMesRegistroTrafg()
    {
        this._has_qtMesRegistroTrafg= false;
    } //-- void deleteQtMesRegistroTrafg() 

    /**
     * Returns the value of field 'cdApliFormat'.
     * 
     * @return long
     * @return the value of field 'cdApliFormat'.
     */
    public long getCdApliFormat()
    {
        return this._cdApliFormat;
    } //-- long getCdApliFormat() 

    /**
     * Returns the value of field 'cdAreaFone'.
     * 
     * @return int
     * @return the value of field 'cdAreaFone'.
     */
    public int getCdAreaFone()
    {
        return this._cdAreaFone;
    } //-- int getCdAreaFone() 

    /**
     * Returns the value of field 'cdAreaFonePend'.
     * 
     * @return int
     * @return the value of field 'cdAreaFonePend'.
     */
    public int getCdAreaFonePend()
    {
        return this._cdAreaFonePend;
    } //-- int getCdAreaFonePend() 

    /**
     * Returns the value of field 'cdCustoTransmicao'.
     * 
     * @return BigDecimal
     * @return the value of field 'cdCustoTransmicao'.
     */
    public java.math.BigDecimal getCdCustoTransmicao()
    {
        return this._cdCustoTransmicao;
    } //-- java.math.BigDecimal getCdCustoTransmicao() 

    /**
     * Returns the value of field 'cdEmpresaResponsavel'.
     * 
     * @return int
     * @return the value of field 'cdEmpresaResponsavel'.
     */
    public int getCdEmpresaResponsavel()
    {
        return this._cdEmpresaResponsavel;
    } //-- int getCdEmpresaResponsavel() 

    /**
     * Returns the value of field 'cdFoneContatoCliente'.
     * 
     * @return long
     * @return the value of field 'cdFoneContatoCliente'.
     */
    public long getCdFoneContatoCliente()
    {
        return this._cdFoneContatoCliente;
    } //-- long getCdFoneContatoCliente() 

    /**
     * Returns the value of field 'cdFoneSolicitacaoPend'.
     * 
     * @return long
     * @return the value of field 'cdFoneSolicitacaoPend'.
     */
    public long getCdFoneSolicitacaoPend()
    {
        return this._cdFoneSolicitacaoPend;
    } //-- long getCdFoneSolicitacaoPend() 

    /**
     * Returns the value of field 'cdMeioAlternativoRetorno'.
     * 
     * @return int
     * @return the value of field 'cdMeioAlternativoRetorno'.
     */
    public int getCdMeioAlternativoRetorno()
    {
        return this._cdMeioAlternativoRetorno;
    } //-- int getCdMeioAlternativoRetorno() 

    /**
     * Returns the value of field 'cdMeioPrincipalRemessa'.
     * 
     * @return int
     * @return the value of field 'cdMeioPrincipalRemessa'.
     */
    public int getCdMeioPrincipalRemessa()
    {
        return this._cdMeioPrincipalRemessa;
    } //-- int getCdMeioPrincipalRemessa() 

    /**
     * Returns the value of field 'cdMeioPrincipalRetorno'.
     * 
     * @return int
     * @return the value of field 'cdMeioPrincipalRetorno'.
     */
    public int getCdMeioPrincipalRetorno()
    {
        return this._cdMeioPrincipalRetorno;
    } //-- int getCdMeioPrincipalRetorno() 

    /**
     * Returns the value of field 'cdPerfilTrocaArquivo'.
     * 
     * @return long
     * @return the value of field 'cdPerfilTrocaArquivo'.
     */
    public long getCdPerfilTrocaArquivo()
    {
        return this._cdPerfilTrocaArquivo;
    } //-- long getCdPerfilTrocaArquivo() 

    /**
     * Returns the value of field 'cdPessoaJuridica'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridica'.
     */
    public long getCdPessoaJuridica()
    {
        return this._cdPessoaJuridica;
    } //-- long getCdPessoaJuridica() 

    /**
     * Returns the value of field 'cdPessoaJuridicaParceiro'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridicaParceiro'.
     */
    public long getCdPessoaJuridicaParceiro()
    {
        return this._cdPessoaJuridicaParceiro;
    } //-- long getCdPessoaJuridicaParceiro() 

    /**
     * Returns the value of field 'cdProdutoOperacaoRelacionado'.
     * 
     * @return int
     * @return the value of field 'cdProdutoOperacaoRelacionado'.
     */
    public int getCdProdutoOperacaoRelacionado()
    {
        return this._cdProdutoOperacaoRelacionado;
    } //-- int getCdProdutoOperacaoRelacionado() 

    /**
     * Returns the value of field 'cdProdutoServicoOperacao'.
     * 
     * @return int
     * @return the value of field 'cdProdutoServicoOperacao'.
     */
    public int getCdProdutoServicoOperacao()
    {
        return this._cdProdutoServicoOperacao;
    } //-- int getCdProdutoServicoOperacao() 

    /**
     * Returns the value of field 'cdRamalContatoCliente'.
     * 
     * @return String
     * @return the value of field 'cdRamalContatoCliente'.
     */
    public java.lang.String getCdRamalContatoCliente()
    {
        return this._cdRamalContatoCliente;
    } //-- java.lang.String getCdRamalContatoCliente() 

    /**
     * Returns the value of field 'cdRamalSolctPend'.
     * 
     * @return String
     * @return the value of field 'cdRamalSolctPend'.
     */
    public java.lang.String getCdRamalSolctPend()
    {
        return this._cdRamalSolctPend;
    } //-- java.lang.String getCdRamalSolctPend() 

    /**
     * Returns the value of field 'cdRelacionamentoProduto'.
     * 
     * @return int
     * @return the value of field 'cdRelacionamentoProduto'.
     */
    public int getCdRelacionamentoProduto()
    {
        return this._cdRelacionamentoProduto;
    } //-- int getCdRelacionamentoProduto() 

    /**
     * Returns the value of field 'cdTipoLayoutArquivo'.
     * 
     * @return int
     * @return the value of field 'cdTipoLayoutArquivo'.
     */
    public int getCdTipoLayoutArquivo()
    {
        return this._cdTipoLayoutArquivo;
    } //-- int getCdTipoLayoutArquivo() 

    /**
     * Returns the value of field 'cdUnidadeOrganizacional'.
     * 
     * @return int
     * @return the value of field 'cdUnidadeOrganizacional'.
     */
    public int getCdUnidadeOrganizacional()
    {
        return this._cdUnidadeOrganizacional;
    } //-- int getCdUnidadeOrganizacional() 

    /**
     * Returns the value of field 'cdmeioAlternativoRemessa'.
     * 
     * @return int
     * @return the value of field 'cdmeioAlternativoRemessa'.
     */
    public int getCdmeioAlternativoRemessa()
    {
        return this._cdmeioAlternativoRemessa;
    } //-- int getCdmeioAlternativoRemessa() 

    /**
     * Returns the value of field 'dsEmailContatoCliente'.
     * 
     * @return String
     * @return the value of field 'dsEmailContatoCliente'.
     */
    public java.lang.String getDsEmailContatoCliente()
    {
        return this._dsEmailContatoCliente;
    } //-- java.lang.String getDsEmailContatoCliente() 

    /**
     * Returns the value of field 'dsEmailSolctPend'.
     * 
     * @return String
     * @return the value of field 'dsEmailSolctPend'.
     */
    public java.lang.String getDsEmailSolctPend()
    {
        return this._dsEmailSolctPend;
    } //-- java.lang.String getDsEmailSolctPend() 

    /**
     * Returns the value of field 'dsNomeContatoCliente'.
     * 
     * @return String
     * @return the value of field 'dsNomeContatoCliente'.
     */
    public java.lang.String getDsNomeContatoCliente()
    {
        return this._dsNomeContatoCliente;
    } //-- java.lang.String getDsNomeContatoCliente() 

    /**
     * Returns the value of field 'dsObsGeralPerfil'.
     * 
     * @return String
     * @return the value of field 'dsObsGeralPerfil'.
     */
    public java.lang.String getDsObsGeralPerfil()
    {
        return this._dsObsGeralPerfil;
    } //-- java.lang.String getDsObsGeralPerfil() 

    /**
     * Returns the value of field 'dsSolicitacaoPendente'.
     * 
     * @return String
     * @return the value of field 'dsSolicitacaoPendente'.
     */
    public java.lang.String getDsSolicitacaoPendente()
    {
        return this._dsSolicitacaoPendente;
    } //-- java.lang.String getDsSolicitacaoPendente() 

    /**
     * Returns the value of field 'qtMesRegistroTrafg'.
     * 
     * @return long
     * @return the value of field 'qtMesRegistroTrafg'.
     */
    public long getQtMesRegistroTrafg()
    {
        return this._qtMesRegistroTrafg;
    } //-- long getQtMesRegistroTrafg() 

    /**
     * Method hasCdApliFormat
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdApliFormat()
    {
        return this._has_cdApliFormat;
    } //-- boolean hasCdApliFormat() 

    /**
     * Method hasCdAreaFone
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAreaFone()
    {
        return this._has_cdAreaFone;
    } //-- boolean hasCdAreaFone() 

    /**
     * Method hasCdAreaFonePend
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAreaFonePend()
    {
        return this._has_cdAreaFonePend;
    } //-- boolean hasCdAreaFonePend() 

    /**
     * Method hasCdEmpresaResponsavel
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdEmpresaResponsavel()
    {
        return this._has_cdEmpresaResponsavel;
    } //-- boolean hasCdEmpresaResponsavel() 

    /**
     * Method hasCdFoneContatoCliente
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFoneContatoCliente()
    {
        return this._has_cdFoneContatoCliente;
    } //-- boolean hasCdFoneContatoCliente() 

    /**
     * Method hasCdFoneSolicitacaoPend
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFoneSolicitacaoPend()
    {
        return this._has_cdFoneSolicitacaoPend;
    } //-- boolean hasCdFoneSolicitacaoPend() 

    /**
     * Method hasCdMeioAlternativoRetorno
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMeioAlternativoRetorno()
    {
        return this._has_cdMeioAlternativoRetorno;
    } //-- boolean hasCdMeioAlternativoRetorno() 

    /**
     * Method hasCdMeioPrincipalRemessa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMeioPrincipalRemessa()
    {
        return this._has_cdMeioPrincipalRemessa;
    } //-- boolean hasCdMeioPrincipalRemessa() 

    /**
     * Method hasCdMeioPrincipalRetorno
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMeioPrincipalRetorno()
    {
        return this._has_cdMeioPrincipalRetorno;
    } //-- boolean hasCdMeioPrincipalRetorno() 

    /**
     * Method hasCdPerfilTrocaArquivo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPerfilTrocaArquivo()
    {
        return this._has_cdPerfilTrocaArquivo;
    } //-- boolean hasCdPerfilTrocaArquivo() 

    /**
     * Method hasCdPessoaJuridica
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridica()
    {
        return this._has_cdPessoaJuridica;
    } //-- boolean hasCdPessoaJuridica() 

    /**
     * Method hasCdPessoaJuridicaParceiro
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridicaParceiro()
    {
        return this._has_cdPessoaJuridicaParceiro;
    } //-- boolean hasCdPessoaJuridicaParceiro() 

    /**
     * Method hasCdProdutoOperacaoRelacionado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdutoOperacaoRelacionado()
    {
        return this._has_cdProdutoOperacaoRelacionado;
    } //-- boolean hasCdProdutoOperacaoRelacionado() 

    /**
     * Method hasCdProdutoServicoOperacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdutoServicoOperacao()
    {
        return this._has_cdProdutoServicoOperacao;
    } //-- boolean hasCdProdutoServicoOperacao() 

    /**
     * Method hasCdRelacionamentoProduto
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdRelacionamentoProduto()
    {
        return this._has_cdRelacionamentoProduto;
    } //-- boolean hasCdRelacionamentoProduto() 

    /**
     * Method hasCdTipoLayoutArquivo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoLayoutArquivo()
    {
        return this._has_cdTipoLayoutArquivo;
    } //-- boolean hasCdTipoLayoutArquivo() 

    /**
     * Method hasCdUnidadeOrganizacional
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdUnidadeOrganizacional()
    {
        return this._has_cdUnidadeOrganizacional;
    } //-- boolean hasCdUnidadeOrganizacional() 

    /**
     * Method hasCdmeioAlternativoRemessa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdmeioAlternativoRemessa()
    {
        return this._has_cdmeioAlternativoRemessa;
    } //-- boolean hasCdmeioAlternativoRemessa() 

    /**
     * Method hasQtMesRegistroTrafg
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtMesRegistroTrafg()
    {
        return this._has_qtMesRegistroTrafg;
    } //-- boolean hasQtMesRegistroTrafg() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdApliFormat'.
     * 
     * @param cdApliFormat the value of field 'cdApliFormat'.
     */
    public void setCdApliFormat(long cdApliFormat)
    {
        this._cdApliFormat = cdApliFormat;
        this._has_cdApliFormat = true;
    } //-- void setCdApliFormat(long) 

    /**
     * Sets the value of field 'cdAreaFone'.
     * 
     * @param cdAreaFone the value of field 'cdAreaFone'.
     */
    public void setCdAreaFone(int cdAreaFone)
    {
        this._cdAreaFone = cdAreaFone;
        this._has_cdAreaFone = true;
    } //-- void setCdAreaFone(int) 

    /**
     * Sets the value of field 'cdAreaFonePend'.
     * 
     * @param cdAreaFonePend the value of field 'cdAreaFonePend'.
     */
    public void setCdAreaFonePend(int cdAreaFonePend)
    {
        this._cdAreaFonePend = cdAreaFonePend;
        this._has_cdAreaFonePend = true;
    } //-- void setCdAreaFonePend(int) 

    /**
     * Sets the value of field 'cdCustoTransmicao'.
     * 
     * @param cdCustoTransmicao the value of field
     * 'cdCustoTransmicao'.
     */
    public void setCdCustoTransmicao(java.math.BigDecimal cdCustoTransmicao)
    {
        this._cdCustoTransmicao = cdCustoTransmicao;
    } //-- void setCdCustoTransmicao(java.math.BigDecimal) 

    /**
     * Sets the value of field 'cdEmpresaResponsavel'.
     * 
     * @param cdEmpresaResponsavel the value of field
     * 'cdEmpresaResponsavel'.
     */
    public void setCdEmpresaResponsavel(int cdEmpresaResponsavel)
    {
        this._cdEmpresaResponsavel = cdEmpresaResponsavel;
        this._has_cdEmpresaResponsavel = true;
    } //-- void setCdEmpresaResponsavel(int) 

    /**
     * Sets the value of field 'cdFoneContatoCliente'.
     * 
     * @param cdFoneContatoCliente the value of field
     * 'cdFoneContatoCliente'.
     */
    public void setCdFoneContatoCliente(long cdFoneContatoCliente)
    {
        this._cdFoneContatoCliente = cdFoneContatoCliente;
        this._has_cdFoneContatoCliente = true;
    } //-- void setCdFoneContatoCliente(long) 

    /**
     * Sets the value of field 'cdFoneSolicitacaoPend'.
     * 
     * @param cdFoneSolicitacaoPend the value of field
     * 'cdFoneSolicitacaoPend'.
     */
    public void setCdFoneSolicitacaoPend(long cdFoneSolicitacaoPend)
    {
        this._cdFoneSolicitacaoPend = cdFoneSolicitacaoPend;
        this._has_cdFoneSolicitacaoPend = true;
    } //-- void setCdFoneSolicitacaoPend(long) 

    /**
     * Sets the value of field 'cdMeioAlternativoRetorno'.
     * 
     * @param cdMeioAlternativoRetorno the value of field
     * 'cdMeioAlternativoRetorno'.
     */
    public void setCdMeioAlternativoRetorno(int cdMeioAlternativoRetorno)
    {
        this._cdMeioAlternativoRetorno = cdMeioAlternativoRetorno;
        this._has_cdMeioAlternativoRetorno = true;
    } //-- void setCdMeioAlternativoRetorno(int) 

    /**
     * Sets the value of field 'cdMeioPrincipalRemessa'.
     * 
     * @param cdMeioPrincipalRemessa the value of field
     * 'cdMeioPrincipalRemessa'.
     */
    public void setCdMeioPrincipalRemessa(int cdMeioPrincipalRemessa)
    {
        this._cdMeioPrincipalRemessa = cdMeioPrincipalRemessa;
        this._has_cdMeioPrincipalRemessa = true;
    } //-- void setCdMeioPrincipalRemessa(int) 

    /**
     * Sets the value of field 'cdMeioPrincipalRetorno'.
     * 
     * @param cdMeioPrincipalRetorno the value of field
     * 'cdMeioPrincipalRetorno'.
     */
    public void setCdMeioPrincipalRetorno(int cdMeioPrincipalRetorno)
    {
        this._cdMeioPrincipalRetorno = cdMeioPrincipalRetorno;
        this._has_cdMeioPrincipalRetorno = true;
    } //-- void setCdMeioPrincipalRetorno(int) 

    /**
     * Sets the value of field 'cdPerfilTrocaArquivo'.
     * 
     * @param cdPerfilTrocaArquivo the value of field
     * 'cdPerfilTrocaArquivo'.
     */
    public void setCdPerfilTrocaArquivo(long cdPerfilTrocaArquivo)
    {
        this._cdPerfilTrocaArquivo = cdPerfilTrocaArquivo;
        this._has_cdPerfilTrocaArquivo = true;
    } //-- void setCdPerfilTrocaArquivo(long) 

    /**
     * Sets the value of field 'cdPessoaJuridica'.
     * 
     * @param cdPessoaJuridica the value of field 'cdPessoaJuridica'
     */
    public void setCdPessoaJuridica(long cdPessoaJuridica)
    {
        this._cdPessoaJuridica = cdPessoaJuridica;
        this._has_cdPessoaJuridica = true;
    } //-- void setCdPessoaJuridica(long) 

    /**
     * Sets the value of field 'cdPessoaJuridicaParceiro'.
     * 
     * @param cdPessoaJuridicaParceiro the value of field
     * 'cdPessoaJuridicaParceiro'.
     */
    public void setCdPessoaJuridicaParceiro(long cdPessoaJuridicaParceiro)
    {
        this._cdPessoaJuridicaParceiro = cdPessoaJuridicaParceiro;
        this._has_cdPessoaJuridicaParceiro = true;
    } //-- void setCdPessoaJuridicaParceiro(long) 

    /**
     * Sets the value of field 'cdProdutoOperacaoRelacionado'.
     * 
     * @param cdProdutoOperacaoRelacionado the value of field
     * 'cdProdutoOperacaoRelacionado'.
     */
    public void setCdProdutoOperacaoRelacionado(int cdProdutoOperacaoRelacionado)
    {
        this._cdProdutoOperacaoRelacionado = cdProdutoOperacaoRelacionado;
        this._has_cdProdutoOperacaoRelacionado = true;
    } //-- void setCdProdutoOperacaoRelacionado(int) 

    /**
     * Sets the value of field 'cdProdutoServicoOperacao'.
     * 
     * @param cdProdutoServicoOperacao the value of field
     * 'cdProdutoServicoOperacao'.
     */
    public void setCdProdutoServicoOperacao(int cdProdutoServicoOperacao)
    {
        this._cdProdutoServicoOperacao = cdProdutoServicoOperacao;
        this._has_cdProdutoServicoOperacao = true;
    } //-- void setCdProdutoServicoOperacao(int) 

    /**
     * Sets the value of field 'cdRamalContatoCliente'.
     * 
     * @param cdRamalContatoCliente the value of field
     * 'cdRamalContatoCliente'.
     */
    public void setCdRamalContatoCliente(java.lang.String cdRamalContatoCliente)
    {
        this._cdRamalContatoCliente = cdRamalContatoCliente;
    } //-- void setCdRamalContatoCliente(java.lang.String) 

    /**
     * Sets the value of field 'cdRamalSolctPend'.
     * 
     * @param cdRamalSolctPend the value of field 'cdRamalSolctPend'
     */
    public void setCdRamalSolctPend(java.lang.String cdRamalSolctPend)
    {
        this._cdRamalSolctPend = cdRamalSolctPend;
    } //-- void setCdRamalSolctPend(java.lang.String) 

    /**
     * Sets the value of field 'cdRelacionamentoProduto'.
     * 
     * @param cdRelacionamentoProduto the value of field
     * 'cdRelacionamentoProduto'.
     */
    public void setCdRelacionamentoProduto(int cdRelacionamentoProduto)
    {
        this._cdRelacionamentoProduto = cdRelacionamentoProduto;
        this._has_cdRelacionamentoProduto = true;
    } //-- void setCdRelacionamentoProduto(int) 

    /**
     * Sets the value of field 'cdTipoLayoutArquivo'.
     * 
     * @param cdTipoLayoutArquivo the value of field
     * 'cdTipoLayoutArquivo'.
     */
    public void setCdTipoLayoutArquivo(int cdTipoLayoutArquivo)
    {
        this._cdTipoLayoutArquivo = cdTipoLayoutArquivo;
        this._has_cdTipoLayoutArquivo = true;
    } //-- void setCdTipoLayoutArquivo(int) 

    /**
     * Sets the value of field 'cdUnidadeOrganizacional'.
     * 
     * @param cdUnidadeOrganizacional the value of field
     * 'cdUnidadeOrganizacional'.
     */
    public void setCdUnidadeOrganizacional(int cdUnidadeOrganizacional)
    {
        this._cdUnidadeOrganizacional = cdUnidadeOrganizacional;
        this._has_cdUnidadeOrganizacional = true;
    } //-- void setCdUnidadeOrganizacional(int) 

    /**
     * Sets the value of field 'cdmeioAlternativoRemessa'.
     * 
     * @param cdmeioAlternativoRemessa the value of field
     * 'cdmeioAlternativoRemessa'.
     */
    public void setCdmeioAlternativoRemessa(int cdmeioAlternativoRemessa)
    {
        this._cdmeioAlternativoRemessa = cdmeioAlternativoRemessa;
        this._has_cdmeioAlternativoRemessa = true;
    } //-- void setCdmeioAlternativoRemessa(int) 

    /**
     * Sets the value of field 'dsEmailContatoCliente'.
     * 
     * @param dsEmailContatoCliente the value of field
     * 'dsEmailContatoCliente'.
     */
    public void setDsEmailContatoCliente(java.lang.String dsEmailContatoCliente)
    {
        this._dsEmailContatoCliente = dsEmailContatoCliente;
    } //-- void setDsEmailContatoCliente(java.lang.String) 

    /**
     * Sets the value of field 'dsEmailSolctPend'.
     * 
     * @param dsEmailSolctPend the value of field 'dsEmailSolctPend'
     */
    public void setDsEmailSolctPend(java.lang.String dsEmailSolctPend)
    {
        this._dsEmailSolctPend = dsEmailSolctPend;
    } //-- void setDsEmailSolctPend(java.lang.String) 

    /**
     * Sets the value of field 'dsNomeContatoCliente'.
     * 
     * @param dsNomeContatoCliente the value of field
     * 'dsNomeContatoCliente'.
     */
    public void setDsNomeContatoCliente(java.lang.String dsNomeContatoCliente)
    {
        this._dsNomeContatoCliente = dsNomeContatoCliente;
    } //-- void setDsNomeContatoCliente(java.lang.String) 

    /**
     * Sets the value of field 'dsObsGeralPerfil'.
     * 
     * @param dsObsGeralPerfil the value of field 'dsObsGeralPerfil'
     */
    public void setDsObsGeralPerfil(java.lang.String dsObsGeralPerfil)
    {
        this._dsObsGeralPerfil = dsObsGeralPerfil;
    } //-- void setDsObsGeralPerfil(java.lang.String) 

    /**
     * Sets the value of field 'dsSolicitacaoPendente'.
     * 
     * @param dsSolicitacaoPendente the value of field
     * 'dsSolicitacaoPendente'.
     */
    public void setDsSolicitacaoPendente(java.lang.String dsSolicitacaoPendente)
    {
        this._dsSolicitacaoPendente = dsSolicitacaoPendente;
    } //-- void setDsSolicitacaoPendente(java.lang.String) 

    /**
     * Sets the value of field 'qtMesRegistroTrafg'.
     * 
     * @param qtMesRegistroTrafg the value of field
     * 'qtMesRegistroTrafg'.
     */
    public void setQtMesRegistroTrafg(long qtMesRegistroTrafg)
    {
        this._qtMesRegistroTrafg = qtMesRegistroTrafg;
        this._has_qtMesRegistroTrafg = true;
    } //-- void setQtMesRegistroTrafg(long) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return AlterarPerfilContratoRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.alterarperfilcontrato.request.AlterarPerfilContratoRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.alterarperfilcontrato.request.AlterarPerfilContratoRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.alterarperfilcontrato.request.AlterarPerfilContratoRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.alterarperfilcontrato.request.AlterarPerfilContratoRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
