/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.detalharmsgalertagestor.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdCist
     */
    private java.lang.String _cdCist;

    /**
     * Field _cdProcsSistema
     */
    private int _cdProcsSistema = 0;

    /**
     * keeps track of state for field: _cdProcsSistema
     */
    private boolean _has_cdProcsSistema;

    /**
     * Field _dsProcsSistema
     */
    private java.lang.String _dsProcsSistema;

    /**
     * Field _cdPessoa
     */
    private long _cdPessoa = 0;

    /**
     * keeps track of state for field: _cdPessoa
     */
    private boolean _has_cdPessoa;

    /**
     * Field _dsPessoa
     */
    private java.lang.String _dsPessoa;

    /**
     * Field _dsMensagemAlerta
     */
    private java.lang.String _dsMensagemAlerta;

    /**
     * Field _cdCistEvendoMensagem
     */
    private java.lang.String _cdCistEvendoMensagem;

    /**
     * Field _nrEventoMensagemNegocio
     */
    private int _nrEventoMensagemNegocio = 0;

    /**
     * keeps track of state for field: _nrEventoMensagemNegocio
     */
    private boolean _has_nrEventoMensagemNegocio;

    /**
     * Field _cdRecGedorMensagem
     */
    private int _cdRecGedorMensagem = 0;

    /**
     * keeps track of state for field: _cdRecGedorMensagem
     */
    private boolean _has_cdRecGedorMensagem;

    /**
     * Field _dsRecGedorMensagem
     */
    private java.lang.String _dsRecGedorMensagem;

    /**
     * Field _cdIdiomaTextoMensagem
     */
    private int _cdIdiomaTextoMensagem = 0;

    /**
     * keeps track of state for field: _cdIdiomaTextoMensagem
     */
    private boolean _has_cdIdiomaTextoMensagem;

    /**
     * Field _dsIdiomaTextoMensagem
     */
    private java.lang.String _dsIdiomaTextoMensagem;

    /**
     * Field _cdIndicadorMeioTransmissao
     */
    private int _cdIndicadorMeioTransmissao = 0;

    /**
     * keeps track of state for field: _cdIndicadorMeioTransmissao
     */
    private boolean _has_cdIndicadorMeioTransmissao;

    /**
     * Field _cdCanalInclusao
     */
    private int _cdCanalInclusao = 0;

    /**
     * keeps track of state for field: _cdCanalInclusao
     */
    private boolean _has_cdCanalInclusao;

    /**
     * Field _dsCanalInclusao
     */
    private java.lang.String _dsCanalInclusao;

    /**
     * Field _cdAutenSegrcInclusao
     */
    private java.lang.String _cdAutenSegrcInclusao;

    /**
     * Field _nmOperFluxoInclusao
     */
    private java.lang.String _nmOperFluxoInclusao;

    /**
     * Field _hrInclusaoRegistro
     */
    private java.lang.String _hrInclusaoRegistro;

    /**
     * Field _cdCanalManutencao
     */
    private int _cdCanalManutencao = 0;

    /**
     * keeps track of state for field: _cdCanalManutencao
     */
    private boolean _has_cdCanalManutencao;

    /**
     * Field _dsCanalManutencao
     */
    private java.lang.String _dsCanalManutencao;

    /**
     * Field _cdAutenSegrcManutencao
     */
    private java.lang.String _cdAutenSegrcManutencao;

    /**
     * Field _nmOperFluxoManutencao
     */
    private java.lang.String _nmOperFluxoManutencao;

    /**
     * Field _hrManutencaoRegistro
     */
    private java.lang.String _hrManutencaoRegistro;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharmsgalertagestor.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdCanalInclusao
     * 
     */
    public void deleteCdCanalInclusao()
    {
        this._has_cdCanalInclusao= false;
    } //-- void deleteCdCanalInclusao() 

    /**
     * Method deleteCdCanalManutencao
     * 
     */
    public void deleteCdCanalManutencao()
    {
        this._has_cdCanalManutencao= false;
    } //-- void deleteCdCanalManutencao() 

    /**
     * Method deleteCdIdiomaTextoMensagem
     * 
     */
    public void deleteCdIdiomaTextoMensagem()
    {
        this._has_cdIdiomaTextoMensagem= false;
    } //-- void deleteCdIdiomaTextoMensagem() 

    /**
     * Method deleteCdIndicadorMeioTransmissao
     * 
     */
    public void deleteCdIndicadorMeioTransmissao()
    {
        this._has_cdIndicadorMeioTransmissao= false;
    } //-- void deleteCdIndicadorMeioTransmissao() 

    /**
     * Method deleteCdPessoa
     * 
     */
    public void deleteCdPessoa()
    {
        this._has_cdPessoa= false;
    } //-- void deleteCdPessoa() 

    /**
     * Method deleteCdProcsSistema
     * 
     */
    public void deleteCdProcsSistema()
    {
        this._has_cdProcsSistema= false;
    } //-- void deleteCdProcsSistema() 

    /**
     * Method deleteCdRecGedorMensagem
     * 
     */
    public void deleteCdRecGedorMensagem()
    {
        this._has_cdRecGedorMensagem= false;
    } //-- void deleteCdRecGedorMensagem() 

    /**
     * Method deleteNrEventoMensagemNegocio
     * 
     */
    public void deleteNrEventoMensagemNegocio()
    {
        this._has_nrEventoMensagemNegocio= false;
    } //-- void deleteNrEventoMensagemNegocio() 

    /**
     * Returns the value of field 'cdAutenSegrcInclusao'.
     * 
     * @return String
     * @return the value of field 'cdAutenSegrcInclusao'.
     */
    public java.lang.String getCdAutenSegrcInclusao()
    {
        return this._cdAutenSegrcInclusao;
    } //-- java.lang.String getCdAutenSegrcInclusao() 

    /**
     * Returns the value of field 'cdAutenSegrcManutencao'.
     * 
     * @return String
     * @return the value of field 'cdAutenSegrcManutencao'.
     */
    public java.lang.String getCdAutenSegrcManutencao()
    {
        return this._cdAutenSegrcManutencao;
    } //-- java.lang.String getCdAutenSegrcManutencao() 

    /**
     * Returns the value of field 'cdCanalInclusao'.
     * 
     * @return int
     * @return the value of field 'cdCanalInclusao'.
     */
    public int getCdCanalInclusao()
    {
        return this._cdCanalInclusao;
    } //-- int getCdCanalInclusao() 

    /**
     * Returns the value of field 'cdCanalManutencao'.
     * 
     * @return int
     * @return the value of field 'cdCanalManutencao'.
     */
    public int getCdCanalManutencao()
    {
        return this._cdCanalManutencao;
    } //-- int getCdCanalManutencao() 

    /**
     * Returns the value of field 'cdCist'.
     * 
     * @return String
     * @return the value of field 'cdCist'.
     */
    public java.lang.String getCdCist()
    {
        return this._cdCist;
    } //-- java.lang.String getCdCist() 

    /**
     * Returns the value of field 'cdCistEvendoMensagem'.
     * 
     * @return String
     * @return the value of field 'cdCistEvendoMensagem'.
     */
    public java.lang.String getCdCistEvendoMensagem()
    {
        return this._cdCistEvendoMensagem;
    } //-- java.lang.String getCdCistEvendoMensagem() 

    /**
     * Returns the value of field 'cdIdiomaTextoMensagem'.
     * 
     * @return int
     * @return the value of field 'cdIdiomaTextoMensagem'.
     */
    public int getCdIdiomaTextoMensagem()
    {
        return this._cdIdiomaTextoMensagem;
    } //-- int getCdIdiomaTextoMensagem() 

    /**
     * Returns the value of field 'cdIndicadorMeioTransmissao'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorMeioTransmissao'.
     */
    public int getCdIndicadorMeioTransmissao()
    {
        return this._cdIndicadorMeioTransmissao;
    } //-- int getCdIndicadorMeioTransmissao() 

    /**
     * Returns the value of field 'cdPessoa'.
     * 
     * @return long
     * @return the value of field 'cdPessoa'.
     */
    public long getCdPessoa()
    {
        return this._cdPessoa;
    } //-- long getCdPessoa() 

    /**
     * Returns the value of field 'cdProcsSistema'.
     * 
     * @return int
     * @return the value of field 'cdProcsSistema'.
     */
    public int getCdProcsSistema()
    {
        return this._cdProcsSistema;
    } //-- int getCdProcsSistema() 

    /**
     * Returns the value of field 'cdRecGedorMensagem'.
     * 
     * @return int
     * @return the value of field 'cdRecGedorMensagem'.
     */
    public int getCdRecGedorMensagem()
    {
        return this._cdRecGedorMensagem;
    } //-- int getCdRecGedorMensagem() 

    /**
     * Returns the value of field 'dsCanalInclusao'.
     * 
     * @return String
     * @return the value of field 'dsCanalInclusao'.
     */
    public java.lang.String getDsCanalInclusao()
    {
        return this._dsCanalInclusao;
    } //-- java.lang.String getDsCanalInclusao() 

    /**
     * Returns the value of field 'dsCanalManutencao'.
     * 
     * @return String
     * @return the value of field 'dsCanalManutencao'.
     */
    public java.lang.String getDsCanalManutencao()
    {
        return this._dsCanalManutencao;
    } //-- java.lang.String getDsCanalManutencao() 

    /**
     * Returns the value of field 'dsIdiomaTextoMensagem'.
     * 
     * @return String
     * @return the value of field 'dsIdiomaTextoMensagem'.
     */
    public java.lang.String getDsIdiomaTextoMensagem()
    {
        return this._dsIdiomaTextoMensagem;
    } //-- java.lang.String getDsIdiomaTextoMensagem() 

    /**
     * Returns the value of field 'dsMensagemAlerta'.
     * 
     * @return String
     * @return the value of field 'dsMensagemAlerta'.
     */
    public java.lang.String getDsMensagemAlerta()
    {
        return this._dsMensagemAlerta;
    } //-- java.lang.String getDsMensagemAlerta() 

    /**
     * Returns the value of field 'dsPessoa'.
     * 
     * @return String
     * @return the value of field 'dsPessoa'.
     */
    public java.lang.String getDsPessoa()
    {
        return this._dsPessoa;
    } //-- java.lang.String getDsPessoa() 

    /**
     * Returns the value of field 'dsProcsSistema'.
     * 
     * @return String
     * @return the value of field 'dsProcsSistema'.
     */
    public java.lang.String getDsProcsSistema()
    {
        return this._dsProcsSistema;
    } //-- java.lang.String getDsProcsSistema() 

    /**
     * Returns the value of field 'dsRecGedorMensagem'.
     * 
     * @return String
     * @return the value of field 'dsRecGedorMensagem'.
     */
    public java.lang.String getDsRecGedorMensagem()
    {
        return this._dsRecGedorMensagem;
    } //-- java.lang.String getDsRecGedorMensagem() 

    /**
     * Returns the value of field 'hrInclusaoRegistro'.
     * 
     * @return String
     * @return the value of field 'hrInclusaoRegistro'.
     */
    public java.lang.String getHrInclusaoRegistro()
    {
        return this._hrInclusaoRegistro;
    } //-- java.lang.String getHrInclusaoRegistro() 

    /**
     * Returns the value of field 'hrManutencaoRegistro'.
     * 
     * @return String
     * @return the value of field 'hrManutencaoRegistro'.
     */
    public java.lang.String getHrManutencaoRegistro()
    {
        return this._hrManutencaoRegistro;
    } //-- java.lang.String getHrManutencaoRegistro() 

    /**
     * Returns the value of field 'nmOperFluxoInclusao'.
     * 
     * @return String
     * @return the value of field 'nmOperFluxoInclusao'.
     */
    public java.lang.String getNmOperFluxoInclusao()
    {
        return this._nmOperFluxoInclusao;
    } //-- java.lang.String getNmOperFluxoInclusao() 

    /**
     * Returns the value of field 'nmOperFluxoManutencao'.
     * 
     * @return String
     * @return the value of field 'nmOperFluxoManutencao'.
     */
    public java.lang.String getNmOperFluxoManutencao()
    {
        return this._nmOperFluxoManutencao;
    } //-- java.lang.String getNmOperFluxoManutencao() 

    /**
     * Returns the value of field 'nrEventoMensagemNegocio'.
     * 
     * @return int
     * @return the value of field 'nrEventoMensagemNegocio'.
     */
    public int getNrEventoMensagemNegocio()
    {
        return this._nrEventoMensagemNegocio;
    } //-- int getNrEventoMensagemNegocio() 

    /**
     * Method hasCdCanalInclusao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCanalInclusao()
    {
        return this._has_cdCanalInclusao;
    } //-- boolean hasCdCanalInclusao() 

    /**
     * Method hasCdCanalManutencao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCanalManutencao()
    {
        return this._has_cdCanalManutencao;
    } //-- boolean hasCdCanalManutencao() 

    /**
     * Method hasCdIdiomaTextoMensagem
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIdiomaTextoMensagem()
    {
        return this._has_cdIdiomaTextoMensagem;
    } //-- boolean hasCdIdiomaTextoMensagem() 

    /**
     * Method hasCdIndicadorMeioTransmissao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorMeioTransmissao()
    {
        return this._has_cdIndicadorMeioTransmissao;
    } //-- boolean hasCdIndicadorMeioTransmissao() 

    /**
     * Method hasCdPessoa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoa()
    {
        return this._has_cdPessoa;
    } //-- boolean hasCdPessoa() 

    /**
     * Method hasCdProcsSistema
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProcsSistema()
    {
        return this._has_cdProcsSistema;
    } //-- boolean hasCdProcsSistema() 

    /**
     * Method hasCdRecGedorMensagem
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdRecGedorMensagem()
    {
        return this._has_cdRecGedorMensagem;
    } //-- boolean hasCdRecGedorMensagem() 

    /**
     * Method hasNrEventoMensagemNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrEventoMensagemNegocio()
    {
        return this._has_nrEventoMensagemNegocio;
    } //-- boolean hasNrEventoMensagemNegocio() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdAutenSegrcInclusao'.
     * 
     * @param cdAutenSegrcInclusao the value of field
     * 'cdAutenSegrcInclusao'.
     */
    public void setCdAutenSegrcInclusao(java.lang.String cdAutenSegrcInclusao)
    {
        this._cdAutenSegrcInclusao = cdAutenSegrcInclusao;
    } //-- void setCdAutenSegrcInclusao(java.lang.String) 

    /**
     * Sets the value of field 'cdAutenSegrcManutencao'.
     * 
     * @param cdAutenSegrcManutencao the value of field
     * 'cdAutenSegrcManutencao'.
     */
    public void setCdAutenSegrcManutencao(java.lang.String cdAutenSegrcManutencao)
    {
        this._cdAutenSegrcManutencao = cdAutenSegrcManutencao;
    } //-- void setCdAutenSegrcManutencao(java.lang.String) 

    /**
     * Sets the value of field 'cdCanalInclusao'.
     * 
     * @param cdCanalInclusao the value of field 'cdCanalInclusao'.
     */
    public void setCdCanalInclusao(int cdCanalInclusao)
    {
        this._cdCanalInclusao = cdCanalInclusao;
        this._has_cdCanalInclusao = true;
    } //-- void setCdCanalInclusao(int) 

    /**
     * Sets the value of field 'cdCanalManutencao'.
     * 
     * @param cdCanalManutencao the value of field
     * 'cdCanalManutencao'.
     */
    public void setCdCanalManutencao(int cdCanalManutencao)
    {
        this._cdCanalManutencao = cdCanalManutencao;
        this._has_cdCanalManutencao = true;
    } //-- void setCdCanalManutencao(int) 

    /**
     * Sets the value of field 'cdCist'.
     * 
     * @param cdCist the value of field 'cdCist'.
     */
    public void setCdCist(java.lang.String cdCist)
    {
        this._cdCist = cdCist;
    } //-- void setCdCist(java.lang.String) 

    /**
     * Sets the value of field 'cdCistEvendoMensagem'.
     * 
     * @param cdCistEvendoMensagem the value of field
     * 'cdCistEvendoMensagem'.
     */
    public void setCdCistEvendoMensagem(java.lang.String cdCistEvendoMensagem)
    {
        this._cdCistEvendoMensagem = cdCistEvendoMensagem;
    } //-- void setCdCistEvendoMensagem(java.lang.String) 

    /**
     * Sets the value of field 'cdIdiomaTextoMensagem'.
     * 
     * @param cdIdiomaTextoMensagem the value of field
     * 'cdIdiomaTextoMensagem'.
     */
    public void setCdIdiomaTextoMensagem(int cdIdiomaTextoMensagem)
    {
        this._cdIdiomaTextoMensagem = cdIdiomaTextoMensagem;
        this._has_cdIdiomaTextoMensagem = true;
    } //-- void setCdIdiomaTextoMensagem(int) 

    /**
     * Sets the value of field 'cdIndicadorMeioTransmissao'.
     * 
     * @param cdIndicadorMeioTransmissao the value of field
     * 'cdIndicadorMeioTransmissao'.
     */
    public void setCdIndicadorMeioTransmissao(int cdIndicadorMeioTransmissao)
    {
        this._cdIndicadorMeioTransmissao = cdIndicadorMeioTransmissao;
        this._has_cdIndicadorMeioTransmissao = true;
    } //-- void setCdIndicadorMeioTransmissao(int) 

    /**
     * Sets the value of field 'cdPessoa'.
     * 
     * @param cdPessoa the value of field 'cdPessoa'.
     */
    public void setCdPessoa(long cdPessoa)
    {
        this._cdPessoa = cdPessoa;
        this._has_cdPessoa = true;
    } //-- void setCdPessoa(long) 

    /**
     * Sets the value of field 'cdProcsSistema'.
     * 
     * @param cdProcsSistema the value of field 'cdProcsSistema'.
     */
    public void setCdProcsSistema(int cdProcsSistema)
    {
        this._cdProcsSistema = cdProcsSistema;
        this._has_cdProcsSistema = true;
    } //-- void setCdProcsSistema(int) 

    /**
     * Sets the value of field 'cdRecGedorMensagem'.
     * 
     * @param cdRecGedorMensagem the value of field
     * 'cdRecGedorMensagem'.
     */
    public void setCdRecGedorMensagem(int cdRecGedorMensagem)
    {
        this._cdRecGedorMensagem = cdRecGedorMensagem;
        this._has_cdRecGedorMensagem = true;
    } //-- void setCdRecGedorMensagem(int) 

    /**
     * Sets the value of field 'dsCanalInclusao'.
     * 
     * @param dsCanalInclusao the value of field 'dsCanalInclusao'.
     */
    public void setDsCanalInclusao(java.lang.String dsCanalInclusao)
    {
        this._dsCanalInclusao = dsCanalInclusao;
    } //-- void setDsCanalInclusao(java.lang.String) 

    /**
     * Sets the value of field 'dsCanalManutencao'.
     * 
     * @param dsCanalManutencao the value of field
     * 'dsCanalManutencao'.
     */
    public void setDsCanalManutencao(java.lang.String dsCanalManutencao)
    {
        this._dsCanalManutencao = dsCanalManutencao;
    } //-- void setDsCanalManutencao(java.lang.String) 

    /**
     * Sets the value of field 'dsIdiomaTextoMensagem'.
     * 
     * @param dsIdiomaTextoMensagem the value of field
     * 'dsIdiomaTextoMensagem'.
     */
    public void setDsIdiomaTextoMensagem(java.lang.String dsIdiomaTextoMensagem)
    {
        this._dsIdiomaTextoMensagem = dsIdiomaTextoMensagem;
    } //-- void setDsIdiomaTextoMensagem(java.lang.String) 

    /**
     * Sets the value of field 'dsMensagemAlerta'.
     * 
     * @param dsMensagemAlerta the value of field 'dsMensagemAlerta'
     */
    public void setDsMensagemAlerta(java.lang.String dsMensagemAlerta)
    {
        this._dsMensagemAlerta = dsMensagemAlerta;
    } //-- void setDsMensagemAlerta(java.lang.String) 

    /**
     * Sets the value of field 'dsPessoa'.
     * 
     * @param dsPessoa the value of field 'dsPessoa'.
     */
    public void setDsPessoa(java.lang.String dsPessoa)
    {
        this._dsPessoa = dsPessoa;
    } //-- void setDsPessoa(java.lang.String) 

    /**
     * Sets the value of field 'dsProcsSistema'.
     * 
     * @param dsProcsSistema the value of field 'dsProcsSistema'.
     */
    public void setDsProcsSistema(java.lang.String dsProcsSistema)
    {
        this._dsProcsSistema = dsProcsSistema;
    } //-- void setDsProcsSistema(java.lang.String) 

    /**
     * Sets the value of field 'dsRecGedorMensagem'.
     * 
     * @param dsRecGedorMensagem the value of field
     * 'dsRecGedorMensagem'.
     */
    public void setDsRecGedorMensagem(java.lang.String dsRecGedorMensagem)
    {
        this._dsRecGedorMensagem = dsRecGedorMensagem;
    } //-- void setDsRecGedorMensagem(java.lang.String) 

    /**
     * Sets the value of field 'hrInclusaoRegistro'.
     * 
     * @param hrInclusaoRegistro the value of field
     * 'hrInclusaoRegistro'.
     */
    public void setHrInclusaoRegistro(java.lang.String hrInclusaoRegistro)
    {
        this._hrInclusaoRegistro = hrInclusaoRegistro;
    } //-- void setHrInclusaoRegistro(java.lang.String) 

    /**
     * Sets the value of field 'hrManutencaoRegistro'.
     * 
     * @param hrManutencaoRegistro the value of field
     * 'hrManutencaoRegistro'.
     */
    public void setHrManutencaoRegistro(java.lang.String hrManutencaoRegistro)
    {
        this._hrManutencaoRegistro = hrManutencaoRegistro;
    } //-- void setHrManutencaoRegistro(java.lang.String) 

    /**
     * Sets the value of field 'nmOperFluxoInclusao'.
     * 
     * @param nmOperFluxoInclusao the value of field
     * 'nmOperFluxoInclusao'.
     */
    public void setNmOperFluxoInclusao(java.lang.String nmOperFluxoInclusao)
    {
        this._nmOperFluxoInclusao = nmOperFluxoInclusao;
    } //-- void setNmOperFluxoInclusao(java.lang.String) 

    /**
     * Sets the value of field 'nmOperFluxoManutencao'.
     * 
     * @param nmOperFluxoManutencao the value of field
     * 'nmOperFluxoManutencao'.
     */
    public void setNmOperFluxoManutencao(java.lang.String nmOperFluxoManutencao)
    {
        this._nmOperFluxoManutencao = nmOperFluxoManutencao;
    } //-- void setNmOperFluxoManutencao(java.lang.String) 

    /**
     * Sets the value of field 'nrEventoMensagemNegocio'.
     * 
     * @param nrEventoMensagemNegocio the value of field
     * 'nrEventoMensagemNegocio'.
     */
    public void setNrEventoMensagemNegocio(int nrEventoMensagemNegocio)
    {
        this._nrEventoMensagemNegocio = nrEventoMensagemNegocio;
        this._has_nrEventoMensagemNegocio = true;
    } //-- void setNrEventoMensagemNegocio(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.detalharmsgalertagestor.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.detalharmsgalertagestor.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.detalharmsgalertagestor.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharmsgalertagestor.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
