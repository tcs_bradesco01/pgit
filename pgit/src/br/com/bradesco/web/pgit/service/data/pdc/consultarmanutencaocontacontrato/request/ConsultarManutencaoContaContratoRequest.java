/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultarmanutencaocontacontrato.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ConsultarManutencaoContaContratoRequest.
 * 
 * @version $Revision$ $Date$
 */
public class ConsultarManutencaoContaContratoRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdPessoaJuridica
     */
    private long _cdPessoaJuridica = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridica
     */
    private boolean _has_cdPessoaJuridica;

    /**
     * Field _cdTipoContratoNegocio
     */
    private int _cdTipoContratoNegocio = 0;

    /**
     * keeps track of state for field: _cdTipoContratoNegocio
     */
    private boolean _has_cdTipoContratoNegocio;

    /**
     * Field _nrSequenciaContratoNegocio
     */
    private long _nrSequenciaContratoNegocio = 0;

    /**
     * keeps track of state for field: _nrSequenciaContratoNegocio
     */
    private boolean _has_nrSequenciaContratoNegocio;

    /**
     * Field _cdTipoManutencaoContrato
     */
    private int _cdTipoManutencaoContrato = 0;

    /**
     * keeps track of state for field: _cdTipoManutencaoContrato
     */
    private boolean _has_cdTipoManutencaoContrato;

    /**
     * Field _nrManutencaoContratoNegocio
     */
    private long _nrManutencaoContratoNegocio = 0;

    /**
     * keeps track of state for field: _nrManutencaoContratoNegocio
     */
    private boolean _has_nrManutencaoContratoNegocio;


      //----------------/
     //- Constructors -/
    //----------------/

    public ConsultarManutencaoContaContratoRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarmanutencaocontacontrato.request.ConsultarManutencaoContaContratoRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdPessoaJuridica
     * 
     */
    public void deleteCdPessoaJuridica()
    {
        this._has_cdPessoaJuridica= false;
    } //-- void deleteCdPessoaJuridica() 

    /**
     * Method deleteCdTipoContratoNegocio
     * 
     */
    public void deleteCdTipoContratoNegocio()
    {
        this._has_cdTipoContratoNegocio= false;
    } //-- void deleteCdTipoContratoNegocio() 

    /**
     * Method deleteCdTipoManutencaoContrato
     * 
     */
    public void deleteCdTipoManutencaoContrato()
    {
        this._has_cdTipoManutencaoContrato= false;
    } //-- void deleteCdTipoManutencaoContrato() 

    /**
     * Method deleteNrManutencaoContratoNegocio
     * 
     */
    public void deleteNrManutencaoContratoNegocio()
    {
        this._has_nrManutencaoContratoNegocio= false;
    } //-- void deleteNrManutencaoContratoNegocio() 

    /**
     * Method deleteNrSequenciaContratoNegocio
     * 
     */
    public void deleteNrSequenciaContratoNegocio()
    {
        this._has_nrSequenciaContratoNegocio= false;
    } //-- void deleteNrSequenciaContratoNegocio() 

    /**
     * Returns the value of field 'cdPessoaJuridica'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridica'.
     */
    public long getCdPessoaJuridica()
    {
        return this._cdPessoaJuridica;
    } //-- long getCdPessoaJuridica() 

    /**
     * Returns the value of field 'cdTipoContratoNegocio'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoNegocio'.
     */
    public int getCdTipoContratoNegocio()
    {
        return this._cdTipoContratoNegocio;
    } //-- int getCdTipoContratoNegocio() 

    /**
     * Returns the value of field 'cdTipoManutencaoContrato'.
     * 
     * @return int
     * @return the value of field 'cdTipoManutencaoContrato'.
     */
    public int getCdTipoManutencaoContrato()
    {
        return this._cdTipoManutencaoContrato;
    } //-- int getCdTipoManutencaoContrato() 

    /**
     * Returns the value of field 'nrManutencaoContratoNegocio'.
     * 
     * @return long
     * @return the value of field 'nrManutencaoContratoNegocio'.
     */
    public long getNrManutencaoContratoNegocio()
    {
        return this._nrManutencaoContratoNegocio;
    } //-- long getNrManutencaoContratoNegocio() 

    /**
     * Returns the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @return long
     * @return the value of field 'nrSequenciaContratoNegocio'.
     */
    public long getNrSequenciaContratoNegocio()
    {
        return this._nrSequenciaContratoNegocio;
    } //-- long getNrSequenciaContratoNegocio() 

    /**
     * Method hasCdPessoaJuridica
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridica()
    {
        return this._has_cdPessoaJuridica;
    } //-- boolean hasCdPessoaJuridica() 

    /**
     * Method hasCdTipoContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoNegocio()
    {
        return this._has_cdTipoContratoNegocio;
    } //-- boolean hasCdTipoContratoNegocio() 

    /**
     * Method hasCdTipoManutencaoContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoManutencaoContrato()
    {
        return this._has_cdTipoManutencaoContrato;
    } //-- boolean hasCdTipoManutencaoContrato() 

    /**
     * Method hasNrManutencaoContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrManutencaoContratoNegocio()
    {
        return this._has_nrManutencaoContratoNegocio;
    } //-- boolean hasNrManutencaoContratoNegocio() 

    /**
     * Method hasNrSequenciaContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaContratoNegocio()
    {
        return this._has_nrSequenciaContratoNegocio;
    } //-- boolean hasNrSequenciaContratoNegocio() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdPessoaJuridica'.
     * 
     * @param cdPessoaJuridica the value of field 'cdPessoaJuridica'
     */
    public void setCdPessoaJuridica(long cdPessoaJuridica)
    {
        this._cdPessoaJuridica = cdPessoaJuridica;
        this._has_cdPessoaJuridica = true;
    } //-- void setCdPessoaJuridica(long) 

    /**
     * Sets the value of field 'cdTipoContratoNegocio'.
     * 
     * @param cdTipoContratoNegocio the value of field
     * 'cdTipoContratoNegocio'.
     */
    public void setCdTipoContratoNegocio(int cdTipoContratoNegocio)
    {
        this._cdTipoContratoNegocio = cdTipoContratoNegocio;
        this._has_cdTipoContratoNegocio = true;
    } //-- void setCdTipoContratoNegocio(int) 

    /**
     * Sets the value of field 'cdTipoManutencaoContrato'.
     * 
     * @param cdTipoManutencaoContrato the value of field
     * 'cdTipoManutencaoContrato'.
     */
    public void setCdTipoManutencaoContrato(int cdTipoManutencaoContrato)
    {
        this._cdTipoManutencaoContrato = cdTipoManutencaoContrato;
        this._has_cdTipoManutencaoContrato = true;
    } //-- void setCdTipoManutencaoContrato(int) 

    /**
     * Sets the value of field 'nrManutencaoContratoNegocio'.
     * 
     * @param nrManutencaoContratoNegocio the value of field
     * 'nrManutencaoContratoNegocio'.
     */
    public void setNrManutencaoContratoNegocio(long nrManutencaoContratoNegocio)
    {
        this._nrManutencaoContratoNegocio = nrManutencaoContratoNegocio;
        this._has_nrManutencaoContratoNegocio = true;
    } //-- void setNrManutencaoContratoNegocio(long) 

    /**
     * Sets the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @param nrSequenciaContratoNegocio the value of field
     * 'nrSequenciaContratoNegocio'.
     */
    public void setNrSequenciaContratoNegocio(long nrSequenciaContratoNegocio)
    {
        this._nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
        this._has_nrSequenciaContratoNegocio = true;
    } //-- void setNrSequenciaContratoNegocio(long) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ConsultarManutencaoContaContratoRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultarmanutencaocontacontrato.request.ConsultarManutencaoContaContratoRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultarmanutencaocontacontrato.request.ConsultarManutencaoContaContratoRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultarmanutencaocontacontrato.request.ConsultarManutencaoContaContratoRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarmanutencaocontacontrato.request.ConsultarManutencaoContaContratoRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
