/*
 * Nome: br.com.bradesco.web.pgit.service.business.manterfavorecido.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.manterfavorecido.bean;

import java.math.BigDecimal;

import br.com.bradesco.web.pgit.utils.NumberUtils;
import br.com.bradesco.web.pgit.utils.PgitUtil;

/**
 * Nome: ConsultarDetalheFavorecidoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarDetalheFavorecidoSaidaDTO {

	/** Atributo dsTipoFavorecido. */
	private String dsTipoFavorecido;

	/** Atributo dsInscricaoFavorecido. */
	private String dsInscricaoFavorecido;

	/** Atributo mtBloqueio. */
	private String mtBloqueio;

	/** Atributo cdTipoFavorecido. */
	private Integer cdTipoFavorecido;

	/** Atributo cdTipoInscricaoFavorecido. */
	private Integer cdTipoInscricaoFavorecido;

	/** Atributo cdMotivoBloqueioFavorecido. */
	private Integer cdMotivoBloqueioFavorecido;

	/** Atributo cdInscricaoFavorecidoCliente. */
	private Long cdInscricaoFavorecidoCliente;

	/** Atributo dsCompletaFavorecidoCliente. */
	private String dsCompletaFavorecidoCliente;

	/** Atributo dsAbreviacaoFavorecidoCliente. */
	private String dsAbreviacaoFavorecidoCliente;

	/** Atributo dsOrganizacaoEmissorDocumento. */
	private String dsOrganizacaoEmissorDocumento;

	/** Atributo dtExpedicaoDocumentoFavorecido. */
	private String dtExpedicaoDocumentoFavorecido;

	/** Atributo cdSexoFavorecidoCliente. */
	private String cdSexoFavorecidoCliente;

	/** Atributo cdAreaFavorecidoCliente. */
	private Integer cdAreaFavorecidoCliente;

	/** Atributo cdFoneFavorecidoCliente. */
	private String cdFoneFavorecidoCliente;

	/** Atributo cdAreaFaxFavorecido. */
	private Integer cdAreaFaxFavorecido;

	/** Atributo cdFaxFavorecidoCliente. */
	private String cdFaxFavorecidoCliente;

	/** Atributo cdRamalFaxFavorecido. */
	private Integer cdRamalFaxFavorecido;

	/** Atributo cdAreaCelularFavorecido. */
	private Integer cdAreaCelularFavorecido;

	/** Atributo cdCelularFavorecidoCliente. */
	private String cdCelularFavorecidoCliente;

	/** Atributo cdAreaFoneComercial. */
	private Integer cdAreaFoneComercial;

	/** Atributo cdFoneComercialFavorecido. */
	private String cdFoneComercialFavorecido;

	/** Atributo cdRamalComercialFavorecido. */
	private Integer cdRamalComercialFavorecido;

	/** Atributo enEmailFavorecidoCliente. */
	private String enEmailFavorecidoCliente;

	/** Atributo enEmailComercialFavorecido. */
	private String enEmailComercialFavorecido;

	/** Atributo cdSmsFavorecidoCliente. */
	private String cdSmsFavorecidoCliente;

	/** Atributo enLogradouroFavorecidoCliente. */
	private String enLogradouroFavorecidoCliente;

	/** Atributo enNumeroLogradouroFavorecido. */
	private String enNumeroLogradouroFavorecido;

	/** Atributo enComplementoLogradouroFavorecido. */
	private String enComplementoLogradouroFavorecido;

	/** Atributo enBairroFavorecidoCliente. */
	private String enBairroFavorecidoCliente;

	/** Atributo dsEstadoFavorecidoCliente. */
	private String dsEstadoFavorecidoCliente;

	/** Atributo cdSiglaEstadoFavorecidoCliente. */
	private String cdSiglaEstadoFavorecidoCliente;

	/** Atributo cdCepFavorecidoCliente. */
	private Integer cdCepFavorecidoCliente;

	/** Atributo cdComplementoCepFavorecido. */
	private Integer cdComplementoCepFavorecido;

	/** Atributo enLogradrouroCorrespondenciaFavorecido. */
	private String enLogradrouroCorrespondenciaFavorecido;

	/** Atributo enNumeroLogradouroCorrespondencia. */
	private String enNumeroLogradouroCorrespondencia;

	/** Atributo enComplementoLogradouroCorrespondencia. */
	private String enComplementoLogradouroCorrespondencia;

	/** Atributo enBairroCorrespondenciaFavorecido. */
	private String enBairroCorrespondenciaFavorecido;

	/** Atributo dsEstadoCorrespondenciaFavorecido. */
	private String dsEstadoCorrespondenciaFavorecido;

	/** Atributo cdSiglaEstadoCorrespondencia. */
	private String cdSiglaEstadoCorrespondencia;

	/** Atributo cdCepCorrespondenciaFavorecido. */
	private Integer cdCepCorrespondenciaFavorecido;

	/** Atributo cdComplementoCepCorrespondencia. */
	private Integer cdComplementoCepCorrespondencia;

	/** Atributo cdNitFavorecidoCliente. */
	private Long cdNitFavorecidoCliente;

	/** Atributo dsMunicipioFavorecidoCliente. */
	private String dsMunicipioFavorecidoCliente;

	/** Atributo cdInscricaoEstadoFavorecido. */
	private Long cdInscricaoEstadoFavorecido;

	/** Atributo cdSiglaUfInscricaoEstadual. */
	private String cdSiglaUfInscricaoEstadual;

	/** Atributo dsMunicipalInscricaoEstado. */
	private String dsMunicipalInscricaoEstado;

	/** Atributo cdSituacaoFavorecidoCliente. */
	private Integer cdSituacaoFavorecidoCliente;

	/** Atributo hrBloqueioFavorecidoCliente. */
	private String hrBloqueioFavorecidoCliente;

	/** Atributo dsObservacaoControleFavorecido. */
	private String dsObservacaoControleFavorecido;

	/** Atributo cdIndicadorRetornoCliente. */
	private Integer cdIndicadorRetornoCliente;

	/** Atributo dsRetornoCliente. */
	private String dsRetornoCliente;

	/** Atributo dtUltimaMovimentacaoFavorecido. */
	private String dtUltimaMovimentacaoFavorecido;

	/** Atributo cdIndicadorFavorecidoInativo. */
	private Integer cdIndicadorFavorecidoInativo;

	/** Atributo cdEstadoCivilFavorecido. */
	private Integer cdEstadoCivilFavorecido;

	/** Atributo cdUfEmpresaFavorecido. */
	private String cdUfEmpresaFavorecido;

	/** Atributo dtNascimentoFavorecidoCliente. */
	private String dtNascimentoFavorecidoCliente;

	/** Atributo dsEmpresaFavorecidoCliente. */
	private String dsEmpresaFavorecidoCliente;

	/** Atributo dsMunicipioEmpresaFavorecido. */
	private String dsMunicipioEmpresaFavorecido;

	/** Atributo dsConjugeFavorecidoCliente. */
	private String dsConjugeFavorecidoCliente;

	/** Atributo dsNacionalidadeFavorecidoCliente. */
	private String dsNacionalidadeFavorecidoCliente;

	/** Atributo dsProfissaoFavorecidoCliente. */
	private String dsProfissaoFavorecidoCliente;

	/** Atributo dsCargoFavorecidoCliente. */
	private String dsCargoFavorecidoCliente;

	/** Atributo dsUfEmpreFavorecido. */
	private String dsUfEmpreFavorecido;

	/** Atributo dsMaeFavorecidoCliente. */
	private String dsMaeFavorecidoCliente;

	/** Atributo dsPaiFavorecidoCliente. */
	private String dsPaiFavorecidoCliente;

	/** Atributo dsMunicipioNascimentoFavorecido. */
	private String dsMunicipioNascimentoFavorecido;

	/** Atributo dsUfNascimentoFavorecido. */
	private String dsUfNascimentoFavorecido;

	/** Atributo vlRendaMensalFavorecido. */
	private BigDecimal vlRendaMensalFavorecido;

	/** Atributo dtAtualizarEnderecoResidencial. */
	private String dtAtualizarEnderecoResidencial;

	/** Atributo dtAtualizarEnderecoCorrespondencia. */
	private String dtAtualizarEnderecoCorrespondencia;

	/** Atributo cdUsuarioInclusao. */
	private String cdUsuarioInclusao;

	/** Atributo cdUsuarioInclusaoExterna. */
	private String cdUsuarioInclusaoExterna;

	/** Atributo dtInclusao. */
	private String dtInclusao;

	/** Atributo hrInclusao. */
	private String hrInclusao;

	/** Atributo cdOperacaoCanalInclusao. */
	private String cdOperacaoCanalInclusao;

	/** Atributo cdTipoCanalInclusao. */
	private Integer cdTipoCanalInclusao;

	/** Atributo dsCanalinclusao. */
	private String dsCanalinclusao;

	/** Atributo cdUsuarioManutencao. */
	private String cdUsuarioManutencao;

	/** Atributo cdUsuarioManutencaoExterna. */
	private String cdUsuarioManutencaoExterna;

	/** Atributo dtManutencao. */
	private String dtManutencao;

	/** Atributo hrManutencao. */
	private String hrManutencao;

	/** Atributo cdOperacaoCanalManutencao. */
	private String cdOperacaoCanalManutencao;

	/** Atributo cdTipoCanalManutencao. */
	private Integer cdTipoCanalManutencao;

	/** Atributo dsCanalManutencao. */
	private String dsCanalManutencao;

	/** Atributo cdAcaoManutencao. */
	private Integer cdAcaoManutencao;
	
	/**
	 * Consultar detalhe favorecido saida dto.
	 */
	public ConsultarDetalheFavorecidoSaidaDTO() {
		super();
	}

	/**
	 * Get: sexo.
	 *
	 * @return sexo
	 */
	public String getSexo() {
		if (this.getCdSexoFavorecidoCliente().equalsIgnoreCase("M")) {
			return "MASCULINO";
		} else if (this.getCdSexoFavorecidoCliente().equalsIgnoreCase("F")) {
			return "FEMININO";
		} else {
			return " ";
		}
	}

	/**
	 * Get: telefoneResidencial.
	 *
	 * @return telefoneResidencial
	 */
	public String getTelefoneResidencial() {
		return NumberUtils.formtarFoneSemZero(cdAreaFavorecidoCliente, cdFoneFavorecidoCliente);
	}

	/**
	 * Get: telefoneFax.
	 *
	 * @return telefoneFax
	 */
	public String getTelefoneFax() {
		return NumberUtils.formtarFoneSemZero(cdAreaFaxFavorecido, cdFaxFavorecidoCliente);
	}

	/**
	 * Get: telefoneComercial.
	 *
	 * @return telefoneComercial
	 */
	public String getTelefoneComercial() {
		return NumberUtils.formtarFoneSemZero(cdAreaFoneComercial, cdFoneComercialFavorecido);
	}

	/**
	 * Get: telefoneCelular.
	 *
	 * @return telefoneCelular
	 */
	public String getTelefoneCelular() {
		return NumberUtils.formtarFoneSemZero(cdAreaCelularFavorecido, cdCelularFavorecidoCliente);
	}

	/**
	 * Get: indicadorRetorno.
	 *
	 * @return indicadorRetorno
	 */
	public String getIndicadorRetorno() {
		return cdIndicadorRetornoCliente.equals(1) ? "RETORNO PENDENTE DE ENVIO" : "RETORNO ENVIADO";
	}

	/**
	 * Get: indicadorFavorecido.
	 *
	 * @return indicadorFavorecido
	 */
	public String getIndicadorFavorecido() {
		return cdIndicadorFavorecidoInativo.equals(1) ? "SIM" : "N�O";
	}

	/**
	 * Get: estadoCivil.
	 *
	 * @return estadoCivil
	 */
	public String getEstadoCivil() {
		if (cdEstadoCivilFavorecido.equals(1)) {
			return "SOLTEIRO";
		} else 	if (cdEstadoCivilFavorecido.equals(2)) {
			return "CASADO";
		} else 	if (cdEstadoCivilFavorecido.equals(3)) {
			return "DIVORCIADO";
		} else {
			return "VI�VO";
		}
	}

	/**
	 * Get: rendaMensal.
	 *
	 * @return rendaMensal
	 */
	public String getRendaMensal() {
	    	if (getVlRendaMensalFavorecido().compareTo(BigDecimal.ZERO) == 0){
	    	    return "";
	    	}
		return NumberUtils.format(this.getVlRendaMensalFavorecido());
	}
	
	/**
	 * Get: cdAcaoManutencao.
	 *
	 * @return cdAcaoManutencao
	 */
	public Integer getCdAcaoManutencao() {
		return cdAcaoManutencao;
	}

	/**
	 * Set: cdAcaoManutencao.
	 *
	 * @param cdAcaoManutencao the cd acao manutencao
	 */
	public void setCdAcaoManutencao(Integer cdAcaoManutencao) {
		this.cdAcaoManutencao = cdAcaoManutencao;
	}

	/**
	 * Get: cdAreaCelularFavorecido.
	 *
	 * @return cdAreaCelularFavorecido
	 */
	public Integer getCdAreaCelularFavorecido() {
		return cdAreaCelularFavorecido;
	}

	/**
	 * Set: cdAreaCelularFavorecido.
	 *
	 * @param cdAreaCelularFavorecido the cd area celular favorecido
	 */
	public void setCdAreaCelularFavorecido(Integer cdAreaCelularFavorecido) {
		this.cdAreaCelularFavorecido = cdAreaCelularFavorecido;
	}

	/**
	 * Get: cdAreaFavorecidoCliente.
	 *
	 * @return cdAreaFavorecidoCliente
	 */
	public Integer getCdAreaFavorecidoCliente() {
		return cdAreaFavorecidoCliente;
	}

	/**
	 * Set: cdAreaFavorecidoCliente.
	 *
	 * @param cdAreaFavorecidoCliente the cd area favorecido cliente
	 */
	public void setCdAreaFavorecidoCliente(Integer cdAreaFavorecidoCliente) {
		this.cdAreaFavorecidoCliente = cdAreaFavorecidoCliente;
	}

	/**
	 * Get: cdAreaFaxFavorecido.
	 *
	 * @return cdAreaFaxFavorecido
	 */
	public Integer getCdAreaFaxFavorecido() {
		return cdAreaFaxFavorecido;
	}

	/**
	 * Set: cdAreaFaxFavorecido.
	 *
	 * @param cdAreaFaxFavorecido the cd area fax favorecido
	 */
	public void setCdAreaFaxFavorecido(Integer cdAreaFaxFavorecido) {
		this.cdAreaFaxFavorecido = cdAreaFaxFavorecido;
	}

	/**
	 * Get: cdAreaFoneComercial.
	 *
	 * @return cdAreaFoneComercial
	 */
	public Integer getCdAreaFoneComercial() {
		return cdAreaFoneComercial;
	}

	/**
	 * Set: cdAreaFoneComercial.
	 *
	 * @param cdAreaFoneComercial the cd area fone comercial
	 */
	public void setCdAreaFoneComercial(Integer cdAreaFoneComercial) {
		this.cdAreaFoneComercial = cdAreaFoneComercial;
	}

	/**
	 * Get: cdCelularFavorecidoCliente.
	 *
	 * @return cdCelularFavorecidoCliente
	 */
	public String getCdCelularFavorecidoCliente() {
		return cdCelularFavorecidoCliente;
	}

	/**
	 * Set: cdCelularFavorecidoCliente.
	 *
	 * @param cdCelularFavorecidoCliente the cd celular favorecido cliente
	 */
	public void setCdCelularFavorecidoCliente(String cdCelularFavorecidoCliente) {
		this.cdCelularFavorecidoCliente = cdCelularFavorecidoCliente;
	}

	/**
	 * Get: cdCepCorrespondenciaFavorecido.
	 *
	 * @return cdCepCorrespondenciaFavorecido
	 */
	public Integer getCdCepCorrespondenciaFavorecido() {
		return cdCepCorrespondenciaFavorecido;
	}

	/**
	 * Set: cdCepCorrespondenciaFavorecido.
	 *
	 * @param cdCepCorrespondenciaFavorecido the cd cep correspondencia favorecido
	 */
	public void setCdCepCorrespondenciaFavorecido(Integer cdCepCorrespondenciaFavorecido) {
		this.cdCepCorrespondenciaFavorecido = cdCepCorrespondenciaFavorecido;
	}

	/**
	 * Get: cdCepFavorecidoCliente.
	 *
	 * @return cdCepFavorecidoCliente
	 */
	public Integer getCdCepFavorecidoCliente() {
		return cdCepFavorecidoCliente;
	}
	
	

	/**
	 * Set: cdCepFavorecidoCliente.
	 *
	 * @param cdCepFavorecidoCliente the cd cep favorecido cliente
	 */
	public void setCdCepFavorecidoCliente(Integer cdCepFavorecidoCliente) {
		this.cdCepFavorecidoCliente = cdCepFavorecidoCliente;
	}

	/**
	 * Get: cdComplementoCepCorrespondencia.
	 *
	 * @return cdComplementoCepCorrespondencia
	 */
	public Integer getCdComplementoCepCorrespondencia() {
		return cdComplementoCepCorrespondencia;
	}

	/**
	 * Set: cdComplementoCepCorrespondencia.
	 *
	 * @param cdComplementoCepCorrespondencia the cd complemento cep correspondencia
	 */
	public void setCdComplementoCepCorrespondencia(Integer cdComplementoCepCorrespondencia) {
		this.cdComplementoCepCorrespondencia = cdComplementoCepCorrespondencia;
	}

	/**
	 * Get: cdComplementoCepFavorecido.
	 *
	 * @return cdComplementoCepFavorecido
	 */
	public Integer getCdComplementoCepFavorecido() {	    	
		return cdComplementoCepFavorecido;
	}

	/**
	 * Set: cdComplementoCepFavorecido.
	 *
	 * @param cdComplementoCepFavorecido the cd complemento cep favorecido
	 */
	public void setCdComplementoCepFavorecido(Integer cdComplementoCepFavorecido) {
		this.cdComplementoCepFavorecido = cdComplementoCepFavorecido;
	}

	/**
	 * Get: cdEstadoCivilFavorecido.
	 *
	 * @return cdEstadoCivilFavorecido
	 */
	public Integer getCdEstadoCivilFavorecido() {
		return cdEstadoCivilFavorecido;
	}

	/**
	 * Set: cdEstadoCivilFavorecido.
	 *
	 * @param cdEstadoCivilFavorecido the cd estado civil favorecido
	 */
	public void setCdEstadoCivilFavorecido(Integer cdEstadoCivilFavorecido) {
		this.cdEstadoCivilFavorecido = cdEstadoCivilFavorecido;
	}

	/**
	 * Get: cdFaxFavorecidoCliente.
	 *
	 * @return cdFaxFavorecidoCliente
	 */
	public String getCdFaxFavorecidoCliente() {
		return cdFaxFavorecidoCliente;
	}

	/**
	 * Set: cdFaxFavorecidoCliente.
	 *
	 * @param cdFaxFavorecidoCliente the cd fax favorecido cliente
	 */
	public void setCdFaxFavorecidoCliente(String cdFaxFavorecidoCliente) {
		this.cdFaxFavorecidoCliente = cdFaxFavorecidoCliente;
	}

	/**
	 * Get: cdFoneComercialFavorecido.
	 *
	 * @return cdFoneComercialFavorecido
	 */
	public String getCdFoneComercialFavorecido() {
		return cdFoneComercialFavorecido;
	}

	/**
	 * Set: cdFoneComercialFavorecido.
	 *
	 * @param cdFoneComercialFavorecido the cd fone comercial favorecido
	 */
	public void setCdFoneComercialFavorecido(String cdFoneComercialFavorecido) {
		this.cdFoneComercialFavorecido = cdFoneComercialFavorecido;
	}

	/**
	 * Get: cdFoneFavorecidoCliente.
	 *
	 * @return cdFoneFavorecidoCliente
	 */
	public String getCdFoneFavorecidoCliente() {
		return cdFoneFavorecidoCliente;
	}

	/**
	 * Set: cdFoneFavorecidoCliente.
	 *
	 * @param cdFoneFavorecidoCliente the cd fone favorecido cliente
	 */
	public void setCdFoneFavorecidoCliente(String cdFoneFavorecidoCliente) {
		this.cdFoneFavorecidoCliente = cdFoneFavorecidoCliente;
	}

	/**
	 * Get: cdIndicadorFavorecidoInativo.
	 *
	 * @return cdIndicadorFavorecidoInativo
	 */
	public Integer getCdIndicadorFavorecidoInativo() {
		return cdIndicadorFavorecidoInativo;
	}

	/**
	 * Set: cdIndicadorFavorecidoInativo.
	 *
	 * @param cdIndicadorFavorecidoInativo the cd indicador favorecido inativo
	 */
	public void setCdIndicadorFavorecidoInativo(Integer cdIndicadorFavorecidoInativo) {
		this.cdIndicadorFavorecidoInativo = cdIndicadorFavorecidoInativo;
	}

	/**
	 * Get: cdIndicadorRetornoCliente.
	 *
	 * @return cdIndicadorRetornoCliente
	 */
	public Integer getCdIndicadorRetornoCliente() {
		return cdIndicadorRetornoCliente;
	}

	/**
	 * Set: cdIndicadorRetornoCliente.
	 *
	 * @param cdIndicadorRetornoCliente the cd indicador retorno cliente
	 */
	public void setCdIndicadorRetornoCliente(Integer cdIndicadorRetornoCliente) {
		this.cdIndicadorRetornoCliente = cdIndicadorRetornoCliente;
	}

	/**
	 * Get: cdInscricaoEstadoFavorecido.
	 *
	 * @return cdInscricaoEstadoFavorecido
	 */
	public Long getCdInscricaoEstadoFavorecido() {	    	
		return cdInscricaoEstadoFavorecido;
	}
	
	/**
	 * Get: dsInscricaoEstadoFavorecido.
	 *
	 * @return dsInscricaoEstadoFavorecido
	 */
	public String getDsInscricaoEstadoFavorecido() {
	    	if (cdInscricaoEstadoFavorecido == null || cdInscricaoEstadoFavorecido.equals(0l)){
	    	    return "";
	    	}
		return cdInscricaoEstadoFavorecido.toString();
	}

	/**
	 * Set: cdInscricaoEstadoFavorecido.
	 *
	 * @param cdInscricaoEstadoFavorecido the cd inscricao estado favorecido
	 */
	public void setCdInscricaoEstadoFavorecido(Long cdInscricaoEstadoFavorecido) {
		this.cdInscricaoEstadoFavorecido = cdInscricaoEstadoFavorecido;
	}

	/**
	 * Get: cdInscricaoFavorecidoCliente.
	 *
	 * @return cdInscricaoFavorecidoCliente
	 */
	public Long getCdInscricaoFavorecidoCliente() {
		return cdInscricaoFavorecidoCliente;
	}

	/**
	 * Set: cdInscricaoFavorecidoCliente.
	 *
	 * @param cdInscricaoFavorecidoCliente the cd inscricao favorecido cliente
	 */
	public void setCdInscricaoFavorecidoCliente(Long cdInscricaoFavorecidoCliente) {
		this.cdInscricaoFavorecidoCliente = cdInscricaoFavorecidoCliente;
	}

	/**
	 * Get: cdMotivoBloqueioFavorecido.
	 *
	 * @return cdMotivoBloqueioFavorecido
	 */
	public Integer getCdMotivoBloqueioFavorecido() {
		return cdMotivoBloqueioFavorecido;
	}

	/**
	 * Set: cdMotivoBloqueioFavorecido.
	 *
	 * @param cdMotivoBloqueioFavorecido the cd motivo bloqueio favorecido
	 */
	public void setCdMotivoBloqueioFavorecido(Integer cdMotivoBloqueioFavorecido) {
		this.cdMotivoBloqueioFavorecido = cdMotivoBloqueioFavorecido;
	}

	/**
	 * Get: cdNitFavorecidoCliente.
	 *
	 * @return cdNitFavorecidoCliente
	 */
	public Long getCdNitFavorecidoCliente() {	    	
		return cdNitFavorecidoCliente;
	}
	
	/**
	 * Get: dsNitFavorecidoCliente.
	 *
	 * @return dsNitFavorecidoCliente
	 */
	public String getDsNitFavorecidoCliente() {
	    	if (cdNitFavorecidoCliente == null || cdNitFavorecidoCliente.equals(0l)){
	    	    return "";
	    	}
		return cdNitFavorecidoCliente.toString();
	}

	/**
	 * Set: cdNitFavorecidoCliente.
	 *
	 * @param cdNitFavorecidoCliente the cd nit favorecido cliente
	 */
	public void setCdNitFavorecidoCliente(Long cdNitFavorecidoCliente) {
		this.cdNitFavorecidoCliente = cdNitFavorecidoCliente;
	}

	/**
	 * Get: cdOperacaoCanalInclusao.
	 *
	 * @return cdOperacaoCanalInclusao
	 */
	public String getCdOperacaoCanalInclusao() {
		return cdOperacaoCanalInclusao;
	}

	/**
	 * Set: cdOperacaoCanalInclusao.
	 *
	 * @param cdOperacaoCanalInclusao the cd operacao canal inclusao
	 */
	public void setCdOperacaoCanalInclusao(String cdOperacaoCanalInclusao) {
		this.cdOperacaoCanalInclusao = cdOperacaoCanalInclusao;
	}

	/**
	 * Get: cdOperacaoCanalManutencao.
	 *
	 * @return cdOperacaoCanalManutencao
	 */
	public String getCdOperacaoCanalManutencao() {
		return cdOperacaoCanalManutencao;
	}

	/**
	 * Set: cdOperacaoCanalManutencao.
	 *
	 * @param cdOperacaoCanalManutencao the cd operacao canal manutencao
	 */
	public void setCdOperacaoCanalManutencao(String cdOperacaoCanalManutencao) {
		this.cdOperacaoCanalManutencao = cdOperacaoCanalManutencao;
	}

	/**
	 * Get: cdRamalComercialFavorecido.
	 *
	 * @return cdRamalComercialFavorecido
	 */
	public Integer getCdRamalComercialFavorecido() {	    	
		return cdRamalComercialFavorecido;
	}
	
	/**
	 * Get: dsRamalComercialFavorecido.
	 *
	 * @return dsRamalComercialFavorecido
	 */
	public String getDsRamalComercialFavorecido() {
	    	if(cdRamalComercialFavorecido == null || cdRamalComercialFavorecido.intValue() == 0){
	    	    return "";
	    	}
		return cdRamalComercialFavorecido.toString();
	}

	/**
	 * Set: cdRamalComercialFavorecido.
	 *
	 * @param cdRamalComercialFavorecido the cd ramal comercial favorecido
	 */
	public void setCdRamalComercialFavorecido(Integer cdRamalComercialFavorecido) {
		this.cdRamalComercialFavorecido = cdRamalComercialFavorecido;
	}

	/**
	 * Get: cdRamalFaxFavorecido.
	 *
	 * @return cdRamalFaxFavorecido
	 */
	public Integer getCdRamalFaxFavorecido() {	    	
		return cdRamalFaxFavorecido;
	}
	
	/**
	 * Get: dsRamalFaxFavorecido.
	 *
	 * @return dsRamalFaxFavorecido
	 */
	public String getDsRamalFaxFavorecido() {
	    	if (cdRamalFaxFavorecido == null || cdRamalFaxFavorecido.intValue() == 0){
	    	    return "";
	    	}
		return cdRamalFaxFavorecido.toString();
	}

	/**
	 * Set: cdRamalFaxFavorecido.
	 *
	 * @param cdRamalFaxFavorecido the cd ramal fax favorecido
	 */
	public void setCdRamalFaxFavorecido(Integer cdRamalFaxFavorecido) {
		this.cdRamalFaxFavorecido = cdRamalFaxFavorecido;
	}

	/**
	 * Get: cdSexoFavorecidoCliente.
	 *
	 * @return cdSexoFavorecidoCliente
	 */
	public String getCdSexoFavorecidoCliente() {
		return cdSexoFavorecidoCliente;
	}

	/**
	 * Set: cdSexoFavorecidoCliente.
	 *
	 * @param cdSexoFavorecidoCliente the cd sexo favorecido cliente
	 */
	public void setCdSexoFavorecidoCliente(String cdSexoFavorecidoCliente) {
		this.cdSexoFavorecidoCliente = cdSexoFavorecidoCliente;
	}

	/**
	 * Get: cdSiglaEstadoCorrespondencia.
	 *
	 * @return cdSiglaEstadoCorrespondencia
	 */
	public String getCdSiglaEstadoCorrespondencia() {
		return cdSiglaEstadoCorrespondencia;
	}

	/**
	 * Set: cdSiglaEstadoCorrespondencia.
	 *
	 * @param cdSiglaEstadoCorrespondencia the cd sigla estado correspondencia
	 */
	public void setCdSiglaEstadoCorrespondencia(String cdSiglaEstadoCorrespondencia) {
		this.cdSiglaEstadoCorrespondencia = cdSiglaEstadoCorrespondencia;
	}

	/**
	 * Get: cdSiglaEstadoFavorecidoCliente.
	 *
	 * @return cdSiglaEstadoFavorecidoCliente
	 */
	public String getCdSiglaEstadoFavorecidoCliente() {
		return cdSiglaEstadoFavorecidoCliente;
	}

	/**
	 * Set: cdSiglaEstadoFavorecidoCliente.
	 *
	 * @param cdSiglaEstadoFavorecidoCliente the cd sigla estado favorecido cliente
	 */
	public void setCdSiglaEstadoFavorecidoCliente(String cdSiglaEstadoFavorecidoCliente) {
		this.cdSiglaEstadoFavorecidoCliente = cdSiglaEstadoFavorecidoCliente;
	}

	/**
	 * Get: cdSiglaUfInscricaoEstadual.
	 *
	 * @return cdSiglaUfInscricaoEstadual
	 */
	public String getCdSiglaUfInscricaoEstadual() {
		return cdSiglaUfInscricaoEstadual;
	}

	/**
	 * Set: cdSiglaUfInscricaoEstadual.
	 *
	 * @param cdSiglaUfInscricaoEstadual the cd sigla uf inscricao estadual
	 */
	public void setCdSiglaUfInscricaoEstadual(String cdSiglaUfInscricaoEstadual) {
		this.cdSiglaUfInscricaoEstadual = cdSiglaUfInscricaoEstadual;
	}

	/**
	 * Get: cdSituacaoFavorecidoCliente.
	 *
	 * @return cdSituacaoFavorecidoCliente
	 */
	public Integer getCdSituacaoFavorecidoCliente() {
		return cdSituacaoFavorecidoCliente;
	}

	/**
	 * Set: cdSituacaoFavorecidoCliente.
	 *
	 * @param cdSituacaoFavorecidoCliente the cd situacao favorecido cliente
	 */
	public void setCdSituacaoFavorecidoCliente(Integer cdSituacaoFavorecidoCliente) {
		this.cdSituacaoFavorecidoCliente = cdSituacaoFavorecidoCliente;
	}

	/**
	 * Get: cdSmsFavorecidoCliente.
	 *
	 * @return cdSmsFavorecidoCliente
	 */
	public String getCdSmsFavorecidoCliente() {
		return cdSmsFavorecidoCliente;
	}

	/**
	 * Set: cdSmsFavorecidoCliente.
	 *
	 * @param cdSmsFavorecidoCliente the cd sms favorecido cliente
	 */
	public void setCdSmsFavorecidoCliente(String cdSmsFavorecidoCliente) {
		this.cdSmsFavorecidoCliente = cdSmsFavorecidoCliente;
	}

	/**
	 * Get: cdTipoCanalInclusao.
	 *
	 * @return cdTipoCanalInclusao
	 */
	public Integer getCdTipoCanalInclusao() {
		return cdTipoCanalInclusao;
	}

	/**
	 * Set: cdTipoCanalInclusao.
	 *
	 * @param cdTipoCanalInclusao the cd tipo canal inclusao
	 */
	public void setCdTipoCanalInclusao(Integer cdTipoCanalInclusao) {
		this.cdTipoCanalInclusao = cdTipoCanalInclusao;
	}

	/**
	 * Get: cdTipoCanalManutencao.
	 *
	 * @return cdTipoCanalManutencao
	 */
	public Integer getCdTipoCanalManutencao() {
		return cdTipoCanalManutencao;
	}

	/**
	 * Set: cdTipoCanalManutencao.
	 *
	 * @param cdTipoCanalManutencao the cd tipo canal manutencao
	 */
	public void setCdTipoCanalManutencao(Integer cdTipoCanalManutencao) {
		this.cdTipoCanalManutencao = cdTipoCanalManutencao;
	}

	/**
	 * Get: cdTipoFavorecido.
	 *
	 * @return cdTipoFavorecido
	 */
	public Integer getCdTipoFavorecido() {
		return cdTipoFavorecido;
	}

	/**
	 * Set: cdTipoFavorecido.
	 *
	 * @param cdTipoFavorecido the cd tipo favorecido
	 */
	public void setCdTipoFavorecido(Integer cdTipoFavorecido) {
		this.cdTipoFavorecido = cdTipoFavorecido;
	}

	/**
	 * Get: cdTipoInscricaoFavorecido.
	 *
	 * @return cdTipoInscricaoFavorecido
	 */
	public Integer getCdTipoInscricaoFavorecido() {
		return cdTipoInscricaoFavorecido;
	}

	/**
	 * Set: cdTipoInscricaoFavorecido.
	 *
	 * @param cdTipoInscricaoFavorecido the cd tipo inscricao favorecido
	 */
	public void setCdTipoInscricaoFavorecido(Integer cdTipoInscricaoFavorecido) {
		this.cdTipoInscricaoFavorecido = cdTipoInscricaoFavorecido;
	}

	/**
	 * Get: cdUfEmpresaFavorecido.
	 *
	 * @return cdUfEmpresaFavorecido
	 */
	public String getCdUfEmpresaFavorecido() {
		return cdUfEmpresaFavorecido;
	}

	/**
	 * Set: cdUfEmpresaFavorecido.
	 *
	 * @param cdUfEmpresaFavorecido the cd uf empresa favorecido
	 */
	public void setCdUfEmpresaFavorecido(String cdUfEmpresaFavorecido) {
		this.cdUfEmpresaFavorecido = cdUfEmpresaFavorecido;
	}

	/**
	 * Get: cdUsuarioInclusao.
	 *
	 * @return cdUsuarioInclusao
	 */
	public String getCdUsuarioInclusao() {
		return cdUsuarioInclusao;
	}

	/**
	 * Set: cdUsuarioInclusao.
	 *
	 * @param cdUsuarioInclusao the cd usuario inclusao
	 */
	public void setCdUsuarioInclusao(String cdUsuarioInclusao) {
		this.cdUsuarioInclusao = cdUsuarioInclusao;
	}

	/**
	 * Get: cdUsuarioInclusaoExterna.
	 *
	 * @return cdUsuarioInclusaoExterna
	 */
	public String getCdUsuarioInclusaoExterna() {
		return cdUsuarioInclusaoExterna;
	}

	/**
	 * Set: cdUsuarioInclusaoExterna.
	 *
	 * @param cdUsuarioInclusaoExterna the cd usuario inclusao externa
	 */
	public void setCdUsuarioInclusaoExterna(String cdUsuarioInclusaoExterna) {
		this.cdUsuarioInclusaoExterna = cdUsuarioInclusaoExterna;
	}

	/**
	 * Get: cdUsuarioManutencao.
	 *
	 * @return cdUsuarioManutencao
	 */
	public String getCdUsuarioManutencao() {
		return cdUsuarioManutencao;
	}

	/**
	 * Set: cdUsuarioManutencao.
	 *
	 * @param cdUsuarioManutencao the cd usuario manutencao
	 */
	public void setCdUsuarioManutencao(String cdUsuarioManutencao) {
		this.cdUsuarioManutencao = cdUsuarioManutencao;
	}

	/**
	 * Get: cdUsuarioManutencaoExterna.
	 *
	 * @return cdUsuarioManutencaoExterna
	 */
	public String getCdUsuarioManutencaoExterna() {
		return cdUsuarioManutencaoExterna;
	}

	/**
	 * Set: cdUsuarioManutencaoExterna.
	 *
	 * @param cdUsuarioManutencaoExterna the cd usuario manutencao externa
	 */
	public void setCdUsuarioManutencaoExterna(String cdUsuarioManutencaoExterna) {
		this.cdUsuarioManutencaoExterna = cdUsuarioManutencaoExterna;
	}

	/**
	 * Get: dsAbreviacaoFavorecidoCliente.
	 *
	 * @return dsAbreviacaoFavorecidoCliente
	 */
	public String getDsAbreviacaoFavorecidoCliente() {
		return dsAbreviacaoFavorecidoCliente;
	}

	/**
	 * Set: dsAbreviacaoFavorecidoCliente.
	 *
	 * @param dsAbreviacaoFavorecidoCliente the ds abreviacao favorecido cliente
	 */
	public void setDsAbreviacaoFavorecidoCliente(String dsAbreviacaoFavorecidoCliente) {
		this.dsAbreviacaoFavorecidoCliente = dsAbreviacaoFavorecidoCliente;
	}

	/**
	 * Get: dsCanalinclusao.
	 *
	 * @return dsCanalinclusao
	 */
	public String getDsCanalinclusao() {
		return dsCanalinclusao;
	}

	/**
	 * Set: dsCanalinclusao.
	 *
	 * @param dsCanalinclusao the ds canalinclusao
	 */
	public void setDsCanalinclusao(String dsCanalinclusao) {
		this.dsCanalinclusao = dsCanalinclusao;
	}

	/**
	 * Get: dsCanalManutencao.
	 *
	 * @return dsCanalManutencao
	 */
	public String getDsCanalManutencao() {
		return dsCanalManutencao;
	}

	/**
	 * Set: dsCanalManutencao.
	 *
	 * @param dsCanalManutencao the ds canal manutencao
	 */
	public void setDsCanalManutencao(String dsCanalManutencao) {
		this.dsCanalManutencao = dsCanalManutencao;
	}

	/**
	 * Get: dsCargoFavorecidoCliente.
	 *
	 * @return dsCargoFavorecidoCliente
	 */
	public String getDsCargoFavorecidoCliente() {
		return dsCargoFavorecidoCliente;
	}

	/**
	 * Set: dsCargoFavorecidoCliente.
	 *
	 * @param dsCargoFavorecidoCliente the ds cargo favorecido cliente
	 */
	public void setDsCargoFavorecidoCliente(String dsCargoFavorecidoCliente) {
		this.dsCargoFavorecidoCliente = dsCargoFavorecidoCliente;
	}

	/**
	 * Get: dsCompletaFavorecidoCliente.
	 *
	 * @return dsCompletaFavorecidoCliente
	 */
	public String getDsCompletaFavorecidoCliente() {
		return dsCompletaFavorecidoCliente;
	}

	/**
	 * Set: dsCompletaFavorecidoCliente.
	 *
	 * @param dsCompletaFavorecidoCliente the ds completa favorecido cliente
	 */
	public void setDsCompletaFavorecidoCliente(String dsCompletaFavorecidoCliente) {
		this.dsCompletaFavorecidoCliente = dsCompletaFavorecidoCliente;
	}

	/**
	 * Get: dsConjugeFavorecidoCliente.
	 *
	 * @return dsConjugeFavorecidoCliente
	 */
	public String getDsConjugeFavorecidoCliente() {
		return dsConjugeFavorecidoCliente;
	}

	/**
	 * Set: dsConjugeFavorecidoCliente.
	 *
	 * @param dsConjugeFavorecidoCliente the ds conjuge favorecido cliente
	 */
	public void setDsConjugeFavorecidoCliente(String dsConjugeFavorecidoCliente) {
		this.dsConjugeFavorecidoCliente = dsConjugeFavorecidoCliente;
	}

	/**
	 * Get: dsEmpresaFavorecidoCliente.
	 *
	 * @return dsEmpresaFavorecidoCliente
	 */
	public String getDsEmpresaFavorecidoCliente() {
		return dsEmpresaFavorecidoCliente;
	}

	/**
	 * Set: dsEmpresaFavorecidoCliente.
	 *
	 * @param dsEmpresaFavorecidoCliente the ds empresa favorecido cliente
	 */
	public void setDsEmpresaFavorecidoCliente(String dsEmpresaFavorecidoCliente) {
		this.dsEmpresaFavorecidoCliente = dsEmpresaFavorecidoCliente;
	}

	/**
	 * Get: dsEstadoCorrespondenciaFavorecido.
	 *
	 * @return dsEstadoCorrespondenciaFavorecido
	 */
	public String getDsEstadoCorrespondenciaFavorecido() {
		return dsEstadoCorrespondenciaFavorecido;
	}

	/**
	 * Set: dsEstadoCorrespondenciaFavorecido.
	 *
	 * @param dsEstadoCorrespondenciaFavorecido the ds estado correspondencia favorecido
	 */
	public void setDsEstadoCorrespondenciaFavorecido(String dsEstadoCorrespondenciaFavorecido) {
		this.dsEstadoCorrespondenciaFavorecido = dsEstadoCorrespondenciaFavorecido;
	}

	/**
	 * Get: dsEstadoFavorecidoCliente.
	 *
	 * @return dsEstadoFavorecidoCliente
	 */
	public String getDsEstadoFavorecidoCliente() {
		return dsEstadoFavorecidoCliente;
	}

	/**
	 * Set: dsEstadoFavorecidoCliente.
	 *
	 * @param dsEstadoFavorecidoCliente the ds estado favorecido cliente
	 */
	public void setDsEstadoFavorecidoCliente(String dsEstadoFavorecidoCliente) {
		this.dsEstadoFavorecidoCliente = dsEstadoFavorecidoCliente;
	}

	/**
	 * Get: dsInscricaoFavorecido.
	 *
	 * @return dsInscricaoFavorecido
	 */
	public String getDsInscricaoFavorecido() {
		return dsInscricaoFavorecido;
	}

	/**
	 * Set: dsInscricaoFavorecido.
	 *
	 * @param dsInscricaoFavorecido the ds inscricao favorecido
	 */
	public void setDsInscricaoFavorecido(String dsInscricaoFavorecido) {
		this.dsInscricaoFavorecido = dsInscricaoFavorecido;
	}

	/**
	 * Get: dsMaeFavorecidoCliente.
	 *
	 * @return dsMaeFavorecidoCliente
	 */
	public String getDsMaeFavorecidoCliente() {
		return dsMaeFavorecidoCliente;
	}

	/**
	 * Set: dsMaeFavorecidoCliente.
	 *
	 * @param dsMaeFavorecidoCliente the ds mae favorecido cliente
	 */
	public void setDsMaeFavorecidoCliente(String dsMaeFavorecidoCliente) {
		this.dsMaeFavorecidoCliente = dsMaeFavorecidoCliente;
	}

	/**
	 * Get: dsMunicipalInscricaoEstado.
	 *
	 * @return dsMunicipalInscricaoEstado
	 */
	public String getDsMunicipalInscricaoEstado() {
		return dsMunicipalInscricaoEstado;
	}

	/**
	 * Set: dsMunicipalInscricaoEstado.
	 *
	 * @param dsMunicipalInscricaoEstado the ds municipal inscricao estado
	 */
	public void setDsMunicipalInscricaoEstado(String dsMunicipalInscricaoEstado) {
		this.dsMunicipalInscricaoEstado = dsMunicipalInscricaoEstado;
	}

	/**
	 * Get: dsMunicipioEmpresaFavorecido.
	 *
	 * @return dsMunicipioEmpresaFavorecido
	 */
	public String getDsMunicipioEmpresaFavorecido() {
		return dsMunicipioEmpresaFavorecido;
	}

	/**
	 * Set: dsMunicipioEmpresaFavorecido.
	 *
	 * @param dsMunicipioEmpresaFavorecido the ds municipio empresa favorecido
	 */
	public void setDsMunicipioEmpresaFavorecido(String dsMunicipioEmpresaFavorecido) {
		this.dsMunicipioEmpresaFavorecido = dsMunicipioEmpresaFavorecido;
	}

	/**
	 * Get: dsMunicipioFavorecidoCliente.
	 *
	 * @return dsMunicipioFavorecidoCliente
	 */
	public String getDsMunicipioFavorecidoCliente() {
		return dsMunicipioFavorecidoCliente;
	}

	/**
	 * Set: dsMunicipioFavorecidoCliente.
	 *
	 * @param dsMunicipioFavorecidoCliente the ds municipio favorecido cliente
	 */
	public void setDsMunicipioFavorecidoCliente(String dsMunicipioFavorecidoCliente) {
		this.dsMunicipioFavorecidoCliente = dsMunicipioFavorecidoCliente;
	}

	/**
	 * Get: dsMunicipioNascimentoFavorecido.
	 *
	 * @return dsMunicipioNascimentoFavorecido
	 */
	public String getDsMunicipioNascimentoFavorecido() {
		return dsMunicipioNascimentoFavorecido;
	}

	/**
	 * Set: dsMunicipioNascimentoFavorecido.
	 *
	 * @param dsMunicipioNascimentoFavorecido the ds municipio nascimento favorecido
	 */
	public void setDsMunicipioNascimentoFavorecido(String dsMunicipioNascimentoFavorecido) {
		this.dsMunicipioNascimentoFavorecido = dsMunicipioNascimentoFavorecido;
	}

	/**
	 * Get: dsNacionalidadeFavorecidoCliente.
	 *
	 * @return dsNacionalidadeFavorecidoCliente
	 */
	public String getDsNacionalidadeFavorecidoCliente() {
		return dsNacionalidadeFavorecidoCliente;
	}

	/**
	 * Set: dsNacionalidadeFavorecidoCliente.
	 *
	 * @param dsNacionalidadeFavorecidoCliente the ds nacionalidade favorecido cliente
	 */
	public void setDsNacionalidadeFavorecidoCliente(String dsNacionalidadeFavorecidoCliente) {
		this.dsNacionalidadeFavorecidoCliente = dsNacionalidadeFavorecidoCliente;
	}

	/**
	 * Get: dsObservacaoControleFavorecido.
	 *
	 * @return dsObservacaoControleFavorecido
	 */
	public String getDsObservacaoControleFavorecido() {
		return dsObservacaoControleFavorecido;
	}

	/**
	 * Set: dsObservacaoControleFavorecido.
	 *
	 * @param dsObservacaoControleFavorecido the ds observacao controle favorecido
	 */
	public void setDsObservacaoControleFavorecido(String dsObservacaoControleFavorecido) {
		this.dsObservacaoControleFavorecido = dsObservacaoControleFavorecido;
	}

	/**
	 * Get: dsOrganizacaoEmissorDocumento.
	 *
	 * @return dsOrganizacaoEmissorDocumento
	 */
	public String getDsOrganizacaoEmissorDocumento() {
		return dsOrganizacaoEmissorDocumento;
	}

	/**
	 * Set: dsOrganizacaoEmissorDocumento.
	 *
	 * @param dsOrganizacaoEmissorDocumento the ds organizacao emissor documento
	 */
	public void setDsOrganizacaoEmissorDocumento(String dsOrganizacaoEmissorDocumento) {
		this.dsOrganizacaoEmissorDocumento = dsOrganizacaoEmissorDocumento;
	}

	/**
	 * Get: dsPaiFavorecidoCliente.
	 *
	 * @return dsPaiFavorecidoCliente
	 */
	public String getDsPaiFavorecidoCliente() {
		return dsPaiFavorecidoCliente;
	}

	/**
	 * Set: dsPaiFavorecidoCliente.
	 *
	 * @param dsPaiFavorecidoCliente the ds pai favorecido cliente
	 */
	public void setDsPaiFavorecidoCliente(String dsPaiFavorecidoCliente) {
		this.dsPaiFavorecidoCliente = dsPaiFavorecidoCliente;
	}

	/**
	 * Get: dsProfissaoFavorecidoCliente.
	 *
	 * @return dsProfissaoFavorecidoCliente
	 */
	public String getDsProfissaoFavorecidoCliente() {
		return dsProfissaoFavorecidoCliente;
	}

	/**
	 * Set: dsProfissaoFavorecidoCliente.
	 *
	 * @param dsProfissaoFavorecidoCliente the ds profissao favorecido cliente
	 */
	public void setDsProfissaoFavorecidoCliente(String dsProfissaoFavorecidoCliente) {
		this.dsProfissaoFavorecidoCliente = dsProfissaoFavorecidoCliente;
	}

	/**
	 * Get: dsRetornoCliente.
	 *
	 * @return dsRetornoCliente
	 */
	public String getDsRetornoCliente() {
		return dsRetornoCliente;
	}

	/**
	 * Set: dsRetornoCliente.
	 *
	 * @param dsRetornoCliente the ds retorno cliente
	 */
	public void setDsRetornoCliente(String dsRetornoCliente) {
		this.dsRetornoCliente = dsRetornoCliente;
	}

	/**
	 * Get: dsTipoFavorecido.
	 *
	 * @return dsTipoFavorecido
	 */
	public String getDsTipoFavorecido() {
		return dsTipoFavorecido;
	}

	/**
	 * Set: dsTipoFavorecido.
	 *
	 * @param dsTipoFavorecido the ds tipo favorecido
	 */
	public void setDsTipoFavorecido(String dsTipoFavorecido) {
		this.dsTipoFavorecido = dsTipoFavorecido;
	}

	/**
	 * Get: dsUfEmpreFavorecido.
	 *
	 * @return dsUfEmpreFavorecido
	 */
	public String getDsUfEmpreFavorecido() {
		return dsUfEmpreFavorecido;
	}

	/**
	 * Set: dsUfEmpreFavorecido.
	 *
	 * @param dsUfEmpreFavorecido the ds uf empre favorecido
	 */
	public void setDsUfEmpreFavorecido(String dsUfEmpreFavorecido) {
		this.dsUfEmpreFavorecido = dsUfEmpreFavorecido;
	}

	/**
	 * Get: dsUfNascimentoFavorecido.
	 *
	 * @return dsUfNascimentoFavorecido
	 */
	public String getDsUfNascimentoFavorecido() {
		return dsUfNascimentoFavorecido;
	}

	/**
	 * Set: dsUfNascimentoFavorecido.
	 *
	 * @param dsUfNascimentoFavorecido the ds uf nascimento favorecido
	 */
	public void setDsUfNascimentoFavorecido(String dsUfNascimentoFavorecido) {
		this.dsUfNascimentoFavorecido = dsUfNascimentoFavorecido;
	}

	/**
	 * Get: dtAtualizarEnderecoCorrespondencia.
	 *
	 * @return dtAtualizarEnderecoCorrespondencia
	 */
	public String getDtAtualizarEnderecoCorrespondencia() {
		return dtAtualizarEnderecoCorrespondencia;
	}

	/**
	 * Set: dtAtualizarEnderecoCorrespondencia.
	 *
	 * @param dtAtualizarEnderecoCorrespondencia the dt atualizar endereco correspondencia
	 */
	public void setDtAtualizarEnderecoCorrespondencia(String dtAtualizarEnderecoCorrespondencia) {
		this.dtAtualizarEnderecoCorrespondencia = dtAtualizarEnderecoCorrespondencia;
	}

	/**
	 * Get: dtAtualizarEnderecoResidencial.
	 *
	 * @return dtAtualizarEnderecoResidencial
	 */
	public String getDtAtualizarEnderecoResidencial() {
		return dtAtualizarEnderecoResidencial;
	}

	/**
	 * Set: dtAtualizarEnderecoResidencial.
	 *
	 * @param dtAtualizarEnderecoResidencial the dt atualizar endereco residencial
	 */
	public void setDtAtualizarEnderecoResidencial(String dtAtualizarEnderecoResidencial) {
		this.dtAtualizarEnderecoResidencial = dtAtualizarEnderecoResidencial;
	}

	/**
	 * Get: dtExpedicaoDocumentoFavorecido.
	 *
	 * @return dtExpedicaoDocumentoFavorecido
	 */
	public String getDtExpedicaoDocumentoFavorecido() {
		return dtExpedicaoDocumentoFavorecido;
	}

	/**
	 * Set: dtExpedicaoDocumentoFavorecido.
	 *
	 * @param dtExpedicaoDocumentoFavorecido the dt expedicao documento favorecido
	 */
	public void setDtExpedicaoDocumentoFavorecido(String dtExpedicaoDocumentoFavorecido) {
		this.dtExpedicaoDocumentoFavorecido = dtExpedicaoDocumentoFavorecido;
	}

	/**
	 * Get: dtInclusao.
	 *
	 * @return dtInclusao
	 */
	public String getDtInclusao() {
		return dtInclusao;
	}

	/**
	 * Set: dtInclusao.
	 *
	 * @param dtInclusao the dt inclusao
	 */
	public void setDtInclusao(String dtInclusao) {
		this.dtInclusao = dtInclusao;
	}

	/**
	 * Get: dtManutencao.
	 *
	 * @return dtManutencao
	 */
	public String getDtManutencao() {
		return dtManutencao;
	}

	/**
	 * Set: dtManutencao.
	 *
	 * @param dtManutencao the dt manutencao
	 */
	public void setDtManutencao(String dtManutencao) {
		this.dtManutencao = dtManutencao;
	}

	/**
	 * Get: dtNascimentoFavorecidoCliente.
	 *
	 * @return dtNascimentoFavorecidoCliente
	 */
	public String getDtNascimentoFavorecidoCliente() {
		return dtNascimentoFavorecidoCliente;
	}

	/**
	 * Set: dtNascimentoFavorecidoCliente.
	 *
	 * @param dtNascimentoFavorecidoCliente the dt nascimento favorecido cliente
	 */
	public void setDtNascimentoFavorecidoCliente(String dtNascimentoFavorecidoCliente) {
		this.dtNascimentoFavorecidoCliente = dtNascimentoFavorecidoCliente;
	}

	/**
	 * Get: dtUltimaMovimentacaoFavorecido.
	 *
	 * @return dtUltimaMovimentacaoFavorecido
	 */
	public String getDtUltimaMovimentacaoFavorecido() {
		return dtUltimaMovimentacaoFavorecido;
	}

	/**
	 * Set: dtUltimaMovimentacaoFavorecido.
	 *
	 * @param dtUltimaMovimentacaoFavorecido the dt ultima movimentacao favorecido
	 */
	public void setDtUltimaMovimentacaoFavorecido(String dtUltimaMovimentacaoFavorecido) {
		this.dtUltimaMovimentacaoFavorecido = dtUltimaMovimentacaoFavorecido;
	}

	/**
	 * Get: enBairroCorrespondenciaFavorecido.
	 *
	 * @return enBairroCorrespondenciaFavorecido
	 */
	public String getEnBairroCorrespondenciaFavorecido() {
		return enBairroCorrespondenciaFavorecido;
	}

	/**
	 * Set: enBairroCorrespondenciaFavorecido.
	 *
	 * @param enBairroCorrespondenciaFavorecido the en bairro correspondencia favorecido
	 */
	public void setEnBairroCorrespondenciaFavorecido(String enBairroCorrespondenciaFavorecido) {
		this.enBairroCorrespondenciaFavorecido = enBairroCorrespondenciaFavorecido;
	}

	/**
	 * Get: enBairroFavorecidoCliente.
	 *
	 * @return enBairroFavorecidoCliente
	 */
	public String getEnBairroFavorecidoCliente() {
		return enBairroFavorecidoCliente;
	}

	/**
	 * Set: enBairroFavorecidoCliente.
	 *
	 * @param enBairroFavorecidoCliente the en bairro favorecido cliente
	 */
	public void setEnBairroFavorecidoCliente(String enBairroFavorecidoCliente) {
		this.enBairroFavorecidoCliente = enBairroFavorecidoCliente;
	}

	/**
	 * Get: enComplementoLogradouroCorrespondencia.
	 *
	 * @return enComplementoLogradouroCorrespondencia
	 */
	public String getEnComplementoLogradouroCorrespondencia() {
		return enComplementoLogradouroCorrespondencia;
	}

	/**
	 * Set: enComplementoLogradouroCorrespondencia.
	 *
	 * @param enComplementoLogradouroCorrespondencia the en complemento logradouro correspondencia
	 */
	public void setEnComplementoLogradouroCorrespondencia(String enComplementoLogradouroCorrespondencia) {
		this.enComplementoLogradouroCorrespondencia = enComplementoLogradouroCorrespondencia;
	}

	/**
	 * Get: enComplementoLogradouroFavorecido.
	 *
	 * @return enComplementoLogradouroFavorecido
	 */
	public String getEnComplementoLogradouroFavorecido() {
		return enComplementoLogradouroFavorecido;
	}

	/**
	 * Set: enComplementoLogradouroFavorecido.
	 *
	 * @param enComplementoLogradouroFavorecido the en complemento logradouro favorecido
	 */
	public void setEnComplementoLogradouroFavorecido(String enComplementoLogradouroFavorecido) {
		this.enComplementoLogradouroFavorecido = enComplementoLogradouroFavorecido;
	}

	/**
	 * Get: enEmailComercialFavorecido.
	 *
	 * @return enEmailComercialFavorecido
	 */
	public String getEnEmailComercialFavorecido() {
		return enEmailComercialFavorecido;
	}

	/**
	 * Set: enEmailComercialFavorecido.
	 *
	 * @param enEmailComercialFavorecido the en email comercial favorecido
	 */
	public void setEnEmailComercialFavorecido(String enEmailComercialFavorecido) {
		this.enEmailComercialFavorecido = enEmailComercialFavorecido;
	}

	/**
	 * Get: enEmailFavorecidoCliente.
	 *
	 * @return enEmailFavorecidoCliente
	 */
	public String getEnEmailFavorecidoCliente() {
		return enEmailFavorecidoCliente;
	}

	/**
	 * Set: enEmailFavorecidoCliente.
	 *
	 * @param enEmailFavorecidoCliente the en email favorecido cliente
	 */
	public void setEnEmailFavorecidoCliente(String enEmailFavorecidoCliente) {
		this.enEmailFavorecidoCliente = enEmailFavorecidoCliente;
	}

	/**
	 * Get: enLogradouroFavorecidoCliente.
	 *
	 * @return enLogradouroFavorecidoCliente
	 */
	public String getEnLogradouroFavorecidoCliente() {
		return enLogradouroFavorecidoCliente;
	}

	/**
	 * Set: enLogradouroFavorecidoCliente.
	 *
	 * @param enLogradouroFavorecidoCliente the en logradouro favorecido cliente
	 */
	public void setEnLogradouroFavorecidoCliente(String enLogradouroFavorecidoCliente) {
		this.enLogradouroFavorecidoCliente = enLogradouroFavorecidoCliente;
	}

	/**
	 * Get: enLogradrouroCorrespondenciaFavorecido.
	 *
	 * @return enLogradrouroCorrespondenciaFavorecido
	 */
	public String getEnLogradrouroCorrespondenciaFavorecido() {
		return enLogradrouroCorrespondenciaFavorecido;
	}

	/**
	 * Set: enLogradrouroCorrespondenciaFavorecido.
	 *
	 * @param enLogradrouroCorrespondenciaFavorecido the en logradrouro correspondencia favorecido
	 */
	public void setEnLogradrouroCorrespondenciaFavorecido(String enLogradrouroCorrespondenciaFavorecido) {
		this.enLogradrouroCorrespondenciaFavorecido = enLogradrouroCorrespondenciaFavorecido;
	}

	/**
	 * Get: enNumeroLogradouroCorrespondencia.
	 *
	 * @return enNumeroLogradouroCorrespondencia
	 */
	public String getEnNumeroLogradouroCorrespondencia() {
		return enNumeroLogradouroCorrespondencia;
	}

	/**
	 * Set: enNumeroLogradouroCorrespondencia.
	 *
	 * @param enNumeroLogradouroCorrespondencia the en numero logradouro correspondencia
	 */
	public void setEnNumeroLogradouroCorrespondencia(String enNumeroLogradouroCorrespondencia) {
		this.enNumeroLogradouroCorrespondencia = enNumeroLogradouroCorrespondencia;
	}

	/**
	 * Get: enNumeroLogradouroFavorecido.
	 *
	 * @return enNumeroLogradouroFavorecido
	 */
	public String getEnNumeroLogradouroFavorecido() {
		return enNumeroLogradouroFavorecido;
	}

	/**
	 * Set: enNumeroLogradouroFavorecido.
	 *
	 * @param enNumeroLogradouroFavorecido the en numero logradouro favorecido
	 */
	public void setEnNumeroLogradouroFavorecido(String enNumeroLogradouroFavorecido) {
		this.enNumeroLogradouroFavorecido = enNumeroLogradouroFavorecido;
	}

	/**
	 * Get: hrBloqueioFavorecidoCliente.
	 *
	 * @return hrBloqueioFavorecidoCliente
	 */
	public String getHrBloqueioFavorecidoCliente() {
		return hrBloqueioFavorecidoCliente;
	}

	/**
	 * Set: hrBloqueioFavorecidoCliente.
	 *
	 * @param hrBloqueioFavorecidoCliente the hr bloqueio favorecido cliente
	 */
	public void setHrBloqueioFavorecidoCliente(String hrBloqueioFavorecidoCliente) {
		this.hrBloqueioFavorecidoCliente = hrBloqueioFavorecidoCliente;
	}

	/**
	 * Get: hrInclusao.
	 *
	 * @return hrInclusao
	 */
	public String getHrInclusao() {
		return hrInclusao;
	}

	/**
	 * Set: hrInclusao.
	 *
	 * @param hrInclusao the hr inclusao
	 */
	public void setHrInclusao(String hrInclusao) {
		this.hrInclusao = hrInclusao;
	}

	/**
	 * Get: hrManutencao.
	 *
	 * @return hrManutencao
	 */
	public String getHrManutencao() {
		return hrManutencao;
	}

	/**
	 * Set: hrManutencao.
	 *
	 * @param hrManutencao the hr manutencao
	 */
	public void setHrManutencao(String hrManutencao) {
		this.hrManutencao = hrManutencao;
	}

	/**
	 * Get: mtBloqueio.
	 *
	 * @return mtBloqueio
	 */
	public String getMtBloqueio() {
		return mtBloqueio;
	}

	/**
	 * Set: mtBloqueio.
	 *
	 * @param mtBloqueio the mt bloqueio
	 */
	public void setMtBloqueio(String mtBloqueio) {
		this.mtBloqueio = mtBloqueio;
	}

	/**
	 * Get: vlRendaMensalFavorecido.
	 *
	 * @return vlRendaMensalFavorecido
	 */
	public BigDecimal getVlRendaMensalFavorecido() {
		return vlRendaMensalFavorecido;
	}

	/**
	 * Set: vlRendaMensalFavorecido.
	 *
	 * @param vlRendaMensalFavorecido the vl renda mensal favorecido
	 */
	public void setVlRendaMensalFavorecido(BigDecimal vlRendaMensalFavorecido) {
		this.vlRendaMensalFavorecido = vlRendaMensalFavorecido;
	}

	/**
	 * Get: cepFavorecidoCliente.
	 *
	 * @return cepFavorecidoCliente
	 */
	public String getCepFavorecidoCliente() {
	    return PgitUtil.concatenarCampos(getCdCepFavorecidoCliente(), getCdComplementoCepFavorecido());
	}

	/**
	 * Get: cepCorrespondenciaFavorecido.
	 *
	 * @return cepCorrespondenciaFavorecido
	 */
	public String getCepCorrespondenciaFavorecido() {
	    return PgitUtil.concatenarCampos(getCdCepCorrespondenciaFavorecido(), getCdComplementoCepCorrespondencia());
	}
}
