/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/implementation.ftl,v $
 * $Id: implementation.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: implementation.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.manterservicorelacionados.impl;

import static br.com.bradesco.web.pgit.utils.PgitUtil.verificaIntegerNulo;
import static br.com.bradesco.web.pgit.utils.PgitUtil.verificaLongNulo;

import java.util.ArrayList;
import java.util.List;

import br.com.bradesco.web.pgit.service.business.manterservicorelacionados.IManterServicoRelacionadosService;
import br.com.bradesco.web.pgit.service.business.manterservicorelacionados.IManterServicoRelacionadosServiceConstants;
import br.com.bradesco.web.pgit.service.business.manterservicorelacionados.bean.AlterarServicoRelacionadoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterservicorelacionados.bean.AlterarServicoRelacionadoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterservicorelacionados.bean.DetalharServicoRelacionadoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterservicorelacionados.bean.DetalharServicoRelacionadoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterservicorelacionados.bean.ExcluirServicoRelacionadoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterservicorelacionados.bean.ExcluirServicoRelacionadoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterservicorelacionados.bean.IncluirServicoRelacionadoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterservicorelacionados.bean.IncluirServicoRelacionadoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterservicorelacionados.bean.ListarServicoRelacionadoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterservicorelacionados.bean.ListarServicoRelacionadoSaidaDTO;
import br.com.bradesco.web.pgit.service.data.pdc.FactoryAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.alterarservicorelacionado.request.AlterarServicoRelacionadoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.alterarservicorelacionado.response.AlterarServicoRelacionadoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.detalharservicorelacionado.request.DetalharServicoRelacionadoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.detalharservicorelacionado.response.DetalharServicoRelacionadoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.excluirservicorelacionado.request.ExcluirServicoRelacionadoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.excluirservicorelacionado.response.ExcluirServicoRelacionadoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.incluirservicorelacionado.request.IncluirServicoRelacionadoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.incluirservicorelacionado.response.IncluirServicoRelacionadoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarservicorelacionado.request.ListarServicoRelacionadoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarservicorelacionado.response.ListarServicoRelacionadoResponse;
import br.com.bradesco.web.pgit.utils.PgitUtil;

/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Implementa��o do adaptador: ManterServicoRelacionados
 * </p>
 * 
 * @comment C�DIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public class ManterServicoRelacionadosServiceImpl implements
		IManterServicoRelacionadosService {

	/** Atributo factoryAdapter. */
	private FactoryAdapter factoryAdapter;

	/**
	 * Get: factoryAdapter.
	 * 
	 * @return factoryAdapter
	 */
	public FactoryAdapter getFactoryAdapter() {
		return factoryAdapter;
	}

	/**
	 * Set: factoryAdapter.
	 * 
	 * @param factoryAdapter
	 *            the factory adapter
	 */
	public void setFactoryAdapter(FactoryAdapter factoryAdapter) {
		this.factoryAdapter = factoryAdapter;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.service.business.manterservicorelacionados.IManterServicoRelacionadosService#detalharServicoRelacionadoDTO(br.com.bradesco.web.pgit.service.business.manterservicorelacionados.bean.DetalharServicoRelacionadoEntradaDTO)
	 */
	public DetalharServicoRelacionadoSaidaDTO detalharServicoRelacionadoDTO(
			DetalharServicoRelacionadoEntradaDTO detalharServicoRelacionadoEntradaDTO) {

		DetalharServicoRelacionadoSaidaDTO saidaDTO = new DetalharServicoRelacionadoSaidaDTO();
		DetalharServicoRelacionadoRequest request = new DetalharServicoRelacionadoRequest();
		DetalharServicoRelacionadoResponse response = new DetalharServicoRelacionadoResponse();

		request
				.setCdProdutoOperacaoRelacionado(detalharServicoRelacionadoEntradaDTO
						.getCodModalidadeServico());
		request
				.setCdProdutoServicoOperacao(detalharServicoRelacionadoEntradaDTO
						.getCodTipoServico());
		request.setCdRelacionamentoProduto(detalharServicoRelacionadoEntradaDTO
				.getCodTipoRelacionamento());

		response = getFactoryAdapter()
				.getDetalharServicoRelacionadoPDCAdapter().invokeProcess(
						request);

		saidaDTO = new DetalharServicoRelacionadoSaidaDTO();
		saidaDTO.setCodMensagem(response.getCodMensagem());
		saidaDTO.setMensagem(response.getMensagem());

		saidaDTO.setCodModalidadeServico(response
				.getCdProdutoOperacaoRelacionado());
		saidaDTO.setDsModalidadeServico(response
				.getDsProdutoOperacaoRelacionado());
		saidaDTO.setCodNaturezaServico(response
				.getCdNaturezaOperacaoPagamento());
		saidaDTO
				.setCodServicoComposto(response.getCdServicoCompostoPagamento());
		saidaDTO.setDsServicoComposto(response.getDsServicoCompostoPagamento());
		saidaDTO.setDsTipoServico(response.getDsProdutoServicoOperacao());
		saidaDTO.setCodTipoServico(response.getCdProdutoServicoOperacao());

		/* Trilha de Auditoria */

		saidaDTO.setCodTipoCanalInclusao(response.getCdCanalInclusao());
		saidaDTO.setCodTipoCanalManutencao(response.getCdCanalManutencao());

		saidaDTO.setDataHraInclusao(response.getHrInclusaoRegistro());
		saidaDTO.setDataHraManutencao(response.getHrManutencaoRegistro());

		saidaDTO.setDsTipoCanalInclusao(response.getDsCanalInclusao());
		saidaDTO.setDsTipoCanalManutencao(response.getDsCanalManutencao());

		saidaDTO.setComplementoInclusao(response.getNmOperacaoFluxoInclusao());
		saidaDTO.setComplementoManutencao(response
				.getNmOperacaoFluxoManutencao());

		saidaDTO.setCodUsuarioInclusao(response
				.getCdAutenticacaoSegurancaInclusao());
		saidaDTO.setCodUsuarioManutencao(response
				.getCdAutenticacaoSegurancaManutencao());

		saidaDTO.setNrOrdenacaoServicoRelacionado(response
				.getNrOrdenacaoServicoRelacionado());
		saidaDTO.setCdIndicadorServicoNegociavel(response
				.getCdIndicadorServicoNegociavel());
		saidaDTO.setCdIndicadorNegociavelServico(response
				.getCdIndicadorNegociavelServico());
		saidaDTO.setCdIndicadorContigenciaServico(response
				.getCdIndicadorContigenciaServico());
		saidaDTO.setCdIndicadorManutencaoServico(response
				.getCdIndicadorManutencaoServico());

		return saidaDTO;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.service.business.manterservicorelacionados.IManterServicoRelacionadosService#excluirServicoRelacionadoDTO(br.com.bradesco.web.pgit.service.business.manterservicorelacionados.bean.ExcluirServicoRelacionadoEntradaDTO)
	 */
	public ExcluirServicoRelacionadoSaidaDTO excluirServicoRelacionadoDTO(
			ExcluirServicoRelacionadoEntradaDTO excluirServicoRelacionadoEntradaDTO) {

		ExcluirServicoRelacionadoSaidaDTO excluirServicoRelacionadoSaidaDTO = new ExcluirServicoRelacionadoSaidaDTO();
		ExcluirServicoRelacionadoRequest excluirServicoRelacionadoRequest = new ExcluirServicoRelacionadoRequest();
		ExcluirServicoRelacionadoResponse excluirServicoRelacionadoResponse = new ExcluirServicoRelacionadoResponse();

		excluirServicoRelacionadoRequest
				.setCdProdutoOperacaoRelacionado(excluirServicoRelacionadoEntradaDTO
						.getCodModalidadeServico());
		excluirServicoRelacionadoRequest
				.setCdProdutoServicoOperacao(excluirServicoRelacionadoEntradaDTO
						.getCodTipoServico());
		excluirServicoRelacionadoRequest
				.setCdRelacionamentoProduto(excluirServicoRelacionadoEntradaDTO
						.getCodTipoRelacionamento());

		excluirServicoRelacionadoResponse = getFactoryAdapter()
				.getExcluirServicoRelacionadoPDCAdapter().invokeProcess(
						excluirServicoRelacionadoRequest);

		excluirServicoRelacionadoSaidaDTO
				.setCodMensagem(excluirServicoRelacionadoResponse
						.getCodMensagem());
		excluirServicoRelacionadoSaidaDTO
				.setMensagem(excluirServicoRelacionadoResponse.getMensagem());

		return excluirServicoRelacionadoSaidaDTO;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.service.business.manterservicorelacionados.IManterServicoRelacionadosService#incluirServicoRelacionadoDTO(br.com.bradesco.web.pgit.service.business.manterservicorelacionados.bean.IncluirServicoRelacionadoEntradaDTO)
	 */
	public IncluirServicoRelacionadoSaidaDTO incluirServicoRelacionadoDTO(
			IncluirServicoRelacionadoEntradaDTO entrada) {

		IncluirServicoRelacionadoSaidaDTO saida = new IncluirServicoRelacionadoSaidaDTO();
		IncluirServicoRelacionadoRequest request = new IncluirServicoRelacionadoRequest();
		IncluirServicoRelacionadoResponse response = null;

		request.setCdNaturezaOperacaoPagamento(verificaIntegerNulo(entrada
				.getCodNaturezaServico()));
		request.setCdProdutoOperacaoRelacionado(verificaIntegerNulo(entrada
				.getCodModalidadeServico()));
		request.setCdProdutoServicoOperacao(verificaIntegerNulo(entrada
				.getCodTipoServico()));
		request.setCdRelacionamentoProduto(verificaIntegerNulo(entrada
				.getCodTipoRelacionamento()));
		request.setCdServicoCompostoPagamento(verificaIntegerNulo(entrada
				.getCodServicoComposto()));

		request.setNrOrdenacaoServicoRelacionado(verificaIntegerNulo(entrada
				.getNrOrdenacaoServicoRelacionado()));
		request.setCdIndicadorServicoNegociavel(verificaIntegerNulo(entrada
				.getCdIndicadorServicoNegociavel()));
		request.setCdIndicadorNegociavelServico(verificaIntegerNulo(entrada
				.getCdIndicadorNegociavelServico()));
		request.setCdIndicadorContigenciaServico(verificaIntegerNulo(entrada
				.getCdIndicadorContigenciaServico()));
		request.setCdIndicadorManutencaoServico(verificaIntegerNulo(entrada
				.getCdIndicadorManutencaoServico()));

		response = getFactoryAdapter().getIncluirServicoRelacionadoPDCAdapter()
				.invokeProcess(request);

		saida.setCodMensagem(response.getCodMensagem());
		saida.setMensagem(response.getMensagem());

		return saida;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.service.business.manterservicorelacionados.IManterServicoRelacionadosService#listarServicoRelacionadoDTO(br.com.bradesco.web.pgit.service.business.manterservicorelacionados.bean.ListarServicoRelacionadoEntradaDTO)
	 */
	public List<ListarServicoRelacionadoSaidaDTO> listarServicoRelacionadoDTO(
			ListarServicoRelacionadoEntradaDTO listarServicoRelacionadoEntradaDTO) {

		List<ListarServicoRelacionadoSaidaDTO> listaRetorno = new ArrayList<ListarServicoRelacionadoSaidaDTO>();
		ListarServicoRelacionadoRequest request = new ListarServicoRelacionadoRequest();
		ListarServicoRelacionadoResponse response = new ListarServicoRelacionadoResponse();

		request.setCdProdutoServicoOperacao(listarServicoRelacionadoEntradaDTO
				.getTipoServico());

		request.setCdRelacionamentoProduto(listarServicoRelacionadoEntradaDTO
				.getTipoRelacionamento());

		request
				.setCdProdutoOperacaoRelacionado(listarServicoRelacionadoEntradaDTO
						.getModalidadeServico());
		request
				.setQtOcorrencias(IManterServicoRelacionadosServiceConstants.QTDE_CONSULTAS);

		response = getFactoryAdapter().getListarServicoRelacionadoPDCAdapter()
				.invokeProcess(request);

		ListarServicoRelacionadoSaidaDTO saidaDTO;

		for (int i = 0; i < response.getOcorrenciasCount(); i++) {
			saidaDTO = new ListarServicoRelacionadoSaidaDTO();
			saidaDTO.setCodMensagem(response.getCodMensagem());
			saidaDTO.setMensagem(response.getMensagem());

			saidaDTO.setCodModalidadeServico(response.getOcorrencias(i)
					.getCdProdutoOperacaoRelacionado());
			saidaDTO.setCodNaturezaServico(response.getOcorrencias(i)
					.getCdNaturezaOperacaoPagamento());
			saidaDTO.setCodRelacionamentoProd(response.getOcorrencias(i)
					.getCdRelacionamentoProduto());
			saidaDTO.setCodServicoComposto(response.getOcorrencias(i)
					.getCdServicoCompostoPagamento());
			saidaDTO.setCodTipoServico(response.getOcorrencias(i)
					.getCdProdutoServicoOperacao());
			saidaDTO.setDsModalidadeServico(response.getOcorrencias(i)
					.getDsProdutoOperacaoRelacionado());
			saidaDTO.setDsServicoComposto(response.getOcorrencias(i)
					.getDsServicoCompostoPagamento());
			saidaDTO.setDsTipoServico(response.getOcorrencias(i)
					.getDsProdutoServicoOperacao());

			saidaDTO.setTipoServicoFormatado(PgitUtil.concatenarCampos(response
					.getOcorrencias(i).getCdProdutoServicoOperacao(), response
					.getOcorrencias(i).getDsProdutoServicoOperacao()));
			saidaDTO.setModalidadeFormatado(PgitUtil.concatenarCampos(response
					.getOcorrencias(i).getCdProdutoOperacaoRelacionado(),
					response.getOcorrencias(i)
							.getDsProdutoOperacaoRelacionado()));
			saidaDTO
					.setServicoCompostoFormatado(PgitUtil.concatenarCampos(
							response.getOcorrencias(i)
									.getCdServicoCompostoPagamento(), response
									.getOcorrencias(i)
									.getDsServicoCompostoPagamento()));

			listaRetorno.add(saidaDTO);
		}

		return listaRetorno;

	}

	public AlterarServicoRelacionadoSaidaDTO alterarServicoRelacionado(
			AlterarServicoRelacionadoEntradaDTO entrada) {

		AlterarServicoRelacionadoSaidaDTO saida = new AlterarServicoRelacionadoSaidaDTO();
		AlterarServicoRelacionadoRequest request = new AlterarServicoRelacionadoRequest();
		AlterarServicoRelacionadoResponse response = null;

		request.setCdProdutoServicoOperacao(verificaIntegerNulo(entrada
				.getCdProdutoServicoOperacao()));
		request.setCdProdutoOperacaoRelacionado(verificaIntegerNulo(entrada
				.getCdProdutoOperacaoRelacionado()));
		request.setCdRelacionamentoProduto(verificaIntegerNulo(entrada
				.getCdRelacionamentoProduto()));
		request.setCdServicoCompostoPagamento(verificaLongNulo(entrada
				.getCdServicoCompostoPagamento()));
		request.setCdNaturezaOperacaoPagamento(verificaIntegerNulo(entrada
				.getCdNaturezaOperacaoPagamento()));
		request.setNrOrdenacaoServicoRelacionado(verificaIntegerNulo(entrada
				.getNrOrdenacaoServicoRelacionado()));
		request.setCdIndicadorServicoNegociavel(verificaIntegerNulo(entrada
				.getCdIndicadorServicoNegociavel()));
		request.setCdIndicadorNegociavelServico(verificaIntegerNulo(entrada
				.getCdIndicadorNegociavelServico()));
		request.setCdIndicadorContigenciaServico(verificaIntegerNulo(entrada
				.getCdIndicadorContigenciaServico()));
		request.setCdIndicadorManutencaoServico(verificaIntegerNulo(entrada
				.getCdIndicadorManutencaoServico()));

		response = getFactoryAdapter().getAlterarServicoRelacionadoPDCAdapter()
				.invokeProcess(request);

		saida.setCodMensagem(response.getCodMensagem());
		saida.setMensagem(response.getMensagem());

		return saida;
	}

}
