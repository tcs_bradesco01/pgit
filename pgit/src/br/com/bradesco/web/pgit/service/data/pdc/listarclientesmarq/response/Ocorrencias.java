/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listarclientesmarq.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdClub
     */
    private long _cdClub = 0;

    /**
     * keeps track of state for field: _cdClub
     */
    private boolean _has_cdClub;

    /**
     * Field _cdCpfCnpj
     */
    private long _cdCpfCnpj = 0;

    /**
     * keeps track of state for field: _cdCpfCnpj
     */
    private boolean _has_cdCpfCnpj;

    /**
     * Field _cdFilialCnpj
     */
    private int _cdFilialCnpj = 0;

    /**
     * keeps track of state for field: _cdFilialCnpj
     */
    private boolean _has_cdFilialCnpj;

    /**
     * Field _cdControleCnpj
     */
    private int _cdControleCnpj = 0;

    /**
     * keeps track of state for field: _cdControleCnpj
     */
    private boolean _has_cdControleCnpj;

    /**
     * Field _dsNomeRazao
     */
    private java.lang.String _dsNomeRazao;

    /**
     * Field _dtNascimentoFund
     */
    private java.lang.String _dtNascimentoFund;

    /**
     * Field _cdSegmentoCliente
     */
    private int _cdSegmentoCliente = 0;

    /**
     * keeps track of state for field: _cdSegmentoCliente
     */
    private boolean _has_cdSegmentoCliente;

    /**
     * Field _dsSegmentoCliente
     */
    private java.lang.String _dsSegmentoCliente;

    /**
     * Field _cdSubSegmentoCliente
     */
    private int _cdSubSegmentoCliente = 0;

    /**
     * keeps track of state for field: _cdSubSegmentoCliente
     */
    private boolean _has_cdSubSegmentoCliente;

    /**
     * Field _dsSubSegmentoCliente
     */
    private java.lang.String _dsSubSegmentoCliente;

    /**
     * Field _cdAtividadeEconomica
     */
    private int _cdAtividadeEconomica = 0;

    /**
     * keeps track of state for field: _cdAtividadeEconomica
     */
    private boolean _has_cdAtividadeEconomica;

    /**
     * Field _dsAtividadeEconomica
     */
    private java.lang.String _dsAtividadeEconomica;

    /**
     * Field _cdGrupoEconomico
     */
    private long _cdGrupoEconomico = 0;

    /**
     * keeps track of state for field: _cdGrupoEconomico
     */
    private boolean _has_cdGrupoEconomico;

    /**
     * Field _dsGrupoEconomico
     */
    private java.lang.String _dsGrupoEconomico;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarclientesmarq.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdAtividadeEconomica
     * 
     */
    public void deleteCdAtividadeEconomica()
    {
        this._has_cdAtividadeEconomica= false;
    } //-- void deleteCdAtividadeEconomica() 

    /**
     * Method deleteCdClub
     * 
     */
    public void deleteCdClub()
    {
        this._has_cdClub= false;
    } //-- void deleteCdClub() 

    /**
     * Method deleteCdControleCnpj
     * 
     */
    public void deleteCdControleCnpj()
    {
        this._has_cdControleCnpj= false;
    } //-- void deleteCdControleCnpj() 

    /**
     * Method deleteCdCpfCnpj
     * 
     */
    public void deleteCdCpfCnpj()
    {
        this._has_cdCpfCnpj= false;
    } //-- void deleteCdCpfCnpj() 

    /**
     * Method deleteCdFilialCnpj
     * 
     */
    public void deleteCdFilialCnpj()
    {
        this._has_cdFilialCnpj= false;
    } //-- void deleteCdFilialCnpj() 

    /**
     * Method deleteCdGrupoEconomico
     * 
     */
    public void deleteCdGrupoEconomico()
    {
        this._has_cdGrupoEconomico= false;
    } //-- void deleteCdGrupoEconomico() 

    /**
     * Method deleteCdSegmentoCliente
     * 
     */
    public void deleteCdSegmentoCliente()
    {
        this._has_cdSegmentoCliente= false;
    } //-- void deleteCdSegmentoCliente() 

    /**
     * Method deleteCdSubSegmentoCliente
     * 
     */
    public void deleteCdSubSegmentoCliente()
    {
        this._has_cdSubSegmentoCliente= false;
    } //-- void deleteCdSubSegmentoCliente() 

    /**
     * Returns the value of field 'cdAtividadeEconomica'.
     * 
     * @return int
     * @return the value of field 'cdAtividadeEconomica'.
     */
    public int getCdAtividadeEconomica()
    {
        return this._cdAtividadeEconomica;
    } //-- int getCdAtividadeEconomica() 

    /**
     * Returns the value of field 'cdClub'.
     * 
     * @return long
     * @return the value of field 'cdClub'.
     */
    public long getCdClub()
    {
        return this._cdClub;
    } //-- long getCdClub() 

    /**
     * Returns the value of field 'cdControleCnpj'.
     * 
     * @return int
     * @return the value of field 'cdControleCnpj'.
     */
    public int getCdControleCnpj()
    {
        return this._cdControleCnpj;
    } //-- int getCdControleCnpj() 

    /**
     * Returns the value of field 'cdCpfCnpj'.
     * 
     * @return long
     * @return the value of field 'cdCpfCnpj'.
     */
    public long getCdCpfCnpj()
    {
        return this._cdCpfCnpj;
    } //-- long getCdCpfCnpj() 

    /**
     * Returns the value of field 'cdFilialCnpj'.
     * 
     * @return int
     * @return the value of field 'cdFilialCnpj'.
     */
    public int getCdFilialCnpj()
    {
        return this._cdFilialCnpj;
    } //-- int getCdFilialCnpj() 

    /**
     * Returns the value of field 'cdGrupoEconomico'.
     * 
     * @return long
     * @return the value of field 'cdGrupoEconomico'.
     */
    public long getCdGrupoEconomico()
    {
        return this._cdGrupoEconomico;
    } //-- long getCdGrupoEconomico() 

    /**
     * Returns the value of field 'cdSegmentoCliente'.
     * 
     * @return int
     * @return the value of field 'cdSegmentoCliente'.
     */
    public int getCdSegmentoCliente()
    {
        return this._cdSegmentoCliente;
    } //-- int getCdSegmentoCliente() 

    /**
     * Returns the value of field 'cdSubSegmentoCliente'.
     * 
     * @return int
     * @return the value of field 'cdSubSegmentoCliente'.
     */
    public int getCdSubSegmentoCliente()
    {
        return this._cdSubSegmentoCliente;
    } //-- int getCdSubSegmentoCliente() 

    /**
     * Returns the value of field 'dsAtividadeEconomica'.
     * 
     * @return String
     * @return the value of field 'dsAtividadeEconomica'.
     */
    public java.lang.String getDsAtividadeEconomica()
    {
        return this._dsAtividadeEconomica;
    } //-- java.lang.String getDsAtividadeEconomica() 

    /**
     * Returns the value of field 'dsGrupoEconomico'.
     * 
     * @return String
     * @return the value of field 'dsGrupoEconomico'.
     */
    public java.lang.String getDsGrupoEconomico()
    {
        return this._dsGrupoEconomico;
    } //-- java.lang.String getDsGrupoEconomico() 

    /**
     * Returns the value of field 'dsNomeRazao'.
     * 
     * @return String
     * @return the value of field 'dsNomeRazao'.
     */
    public java.lang.String getDsNomeRazao()
    {
        return this._dsNomeRazao;
    } //-- java.lang.String getDsNomeRazao() 

    /**
     * Returns the value of field 'dsSegmentoCliente'.
     * 
     * @return String
     * @return the value of field 'dsSegmentoCliente'.
     */
    public java.lang.String getDsSegmentoCliente()
    {
        return this._dsSegmentoCliente;
    } //-- java.lang.String getDsSegmentoCliente() 

    /**
     * Returns the value of field 'dsSubSegmentoCliente'.
     * 
     * @return String
     * @return the value of field 'dsSubSegmentoCliente'.
     */
    public java.lang.String getDsSubSegmentoCliente()
    {
        return this._dsSubSegmentoCliente;
    } //-- java.lang.String getDsSubSegmentoCliente() 

    /**
     * Returns the value of field 'dtNascimentoFund'.
     * 
     * @return String
     * @return the value of field 'dtNascimentoFund'.
     */
    public java.lang.String getDtNascimentoFund()
    {
        return this._dtNascimentoFund;
    } //-- java.lang.String getDtNascimentoFund() 

    /**
     * Method hasCdAtividadeEconomica
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAtividadeEconomica()
    {
        return this._has_cdAtividadeEconomica;
    } //-- boolean hasCdAtividadeEconomica() 

    /**
     * Method hasCdClub
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdClub()
    {
        return this._has_cdClub;
    } //-- boolean hasCdClub() 

    /**
     * Method hasCdControleCnpj
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdControleCnpj()
    {
        return this._has_cdControleCnpj;
    } //-- boolean hasCdControleCnpj() 

    /**
     * Method hasCdCpfCnpj
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCpfCnpj()
    {
        return this._has_cdCpfCnpj;
    } //-- boolean hasCdCpfCnpj() 

    /**
     * Method hasCdFilialCnpj
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFilialCnpj()
    {
        return this._has_cdFilialCnpj;
    } //-- boolean hasCdFilialCnpj() 

    /**
     * Method hasCdGrupoEconomico
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdGrupoEconomico()
    {
        return this._has_cdGrupoEconomico;
    } //-- boolean hasCdGrupoEconomico() 

    /**
     * Method hasCdSegmentoCliente
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSegmentoCliente()
    {
        return this._has_cdSegmentoCliente;
    } //-- boolean hasCdSegmentoCliente() 

    /**
     * Method hasCdSubSegmentoCliente
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSubSegmentoCliente()
    {
        return this._has_cdSubSegmentoCliente;
    } //-- boolean hasCdSubSegmentoCliente() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdAtividadeEconomica'.
     * 
     * @param cdAtividadeEconomica the value of field
     * 'cdAtividadeEconomica'.
     */
    public void setCdAtividadeEconomica(int cdAtividadeEconomica)
    {
        this._cdAtividadeEconomica = cdAtividadeEconomica;
        this._has_cdAtividadeEconomica = true;
    } //-- void setCdAtividadeEconomica(int) 

    /**
     * Sets the value of field 'cdClub'.
     * 
     * @param cdClub the value of field 'cdClub'.
     */
    public void setCdClub(long cdClub)
    {
        this._cdClub = cdClub;
        this._has_cdClub = true;
    } //-- void setCdClub(long) 

    /**
     * Sets the value of field 'cdControleCnpj'.
     * 
     * @param cdControleCnpj the value of field 'cdControleCnpj'.
     */
    public void setCdControleCnpj(int cdControleCnpj)
    {
        this._cdControleCnpj = cdControleCnpj;
        this._has_cdControleCnpj = true;
    } //-- void setCdControleCnpj(int) 

    /**
     * Sets the value of field 'cdCpfCnpj'.
     * 
     * @param cdCpfCnpj the value of field 'cdCpfCnpj'.
     */
    public void setCdCpfCnpj(long cdCpfCnpj)
    {
        this._cdCpfCnpj = cdCpfCnpj;
        this._has_cdCpfCnpj = true;
    } //-- void setCdCpfCnpj(long) 

    /**
     * Sets the value of field 'cdFilialCnpj'.
     * 
     * @param cdFilialCnpj the value of field 'cdFilialCnpj'.
     */
    public void setCdFilialCnpj(int cdFilialCnpj)
    {
        this._cdFilialCnpj = cdFilialCnpj;
        this._has_cdFilialCnpj = true;
    } //-- void setCdFilialCnpj(int) 

    /**
     * Sets the value of field 'cdGrupoEconomico'.
     * 
     * @param cdGrupoEconomico the value of field 'cdGrupoEconomico'
     */
    public void setCdGrupoEconomico(long cdGrupoEconomico)
    {
        this._cdGrupoEconomico = cdGrupoEconomico;
        this._has_cdGrupoEconomico = true;
    } //-- void setCdGrupoEconomico(long) 

    /**
     * Sets the value of field 'cdSegmentoCliente'.
     * 
     * @param cdSegmentoCliente the value of field
     * 'cdSegmentoCliente'.
     */
    public void setCdSegmentoCliente(int cdSegmentoCliente)
    {
        this._cdSegmentoCliente = cdSegmentoCliente;
        this._has_cdSegmentoCliente = true;
    } //-- void setCdSegmentoCliente(int) 

    /**
     * Sets the value of field 'cdSubSegmentoCliente'.
     * 
     * @param cdSubSegmentoCliente the value of field
     * 'cdSubSegmentoCliente'.
     */
    public void setCdSubSegmentoCliente(int cdSubSegmentoCliente)
    {
        this._cdSubSegmentoCliente = cdSubSegmentoCliente;
        this._has_cdSubSegmentoCliente = true;
    } //-- void setCdSubSegmentoCliente(int) 

    /**
     * Sets the value of field 'dsAtividadeEconomica'.
     * 
     * @param dsAtividadeEconomica the value of field
     * 'dsAtividadeEconomica'.
     */
    public void setDsAtividadeEconomica(java.lang.String dsAtividadeEconomica)
    {
        this._dsAtividadeEconomica = dsAtividadeEconomica;
    } //-- void setDsAtividadeEconomica(java.lang.String) 

    /**
     * Sets the value of field 'dsGrupoEconomico'.
     * 
     * @param dsGrupoEconomico the value of field 'dsGrupoEconomico'
     */
    public void setDsGrupoEconomico(java.lang.String dsGrupoEconomico)
    {
        this._dsGrupoEconomico = dsGrupoEconomico;
    } //-- void setDsGrupoEconomico(java.lang.String) 

    /**
     * Sets the value of field 'dsNomeRazao'.
     * 
     * @param dsNomeRazao the value of field 'dsNomeRazao'.
     */
    public void setDsNomeRazao(java.lang.String dsNomeRazao)
    {
        this._dsNomeRazao = dsNomeRazao;
    } //-- void setDsNomeRazao(java.lang.String) 

    /**
     * Sets the value of field 'dsSegmentoCliente'.
     * 
     * @param dsSegmentoCliente the value of field
     * 'dsSegmentoCliente'.
     */
    public void setDsSegmentoCliente(java.lang.String dsSegmentoCliente)
    {
        this._dsSegmentoCliente = dsSegmentoCliente;
    } //-- void setDsSegmentoCliente(java.lang.String) 

    /**
     * Sets the value of field 'dsSubSegmentoCliente'.
     * 
     * @param dsSubSegmentoCliente the value of field
     * 'dsSubSegmentoCliente'.
     */
    public void setDsSubSegmentoCliente(java.lang.String dsSubSegmentoCliente)
    {
        this._dsSubSegmentoCliente = dsSubSegmentoCliente;
    } //-- void setDsSubSegmentoCliente(java.lang.String) 

    /**
     * Sets the value of field 'dtNascimentoFund'.
     * 
     * @param dtNascimentoFund the value of field 'dtNascimentoFund'
     */
    public void setDtNascimentoFund(java.lang.String dtNascimentoFund)
    {
        this._dtNascimentoFund = dtNascimentoFund;
    } //-- void setDtNascimentoFund(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listarclientesmarq.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listarclientesmarq.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listarclientesmarq.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarclientesmarq.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
