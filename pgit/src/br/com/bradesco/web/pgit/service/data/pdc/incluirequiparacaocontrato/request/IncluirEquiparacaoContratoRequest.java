/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.incluirequiparacaocontrato.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.util.Enumeration;
import java.util.Vector;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class IncluirEquiparacaoContratoRequest.
 * 
 * @version $Revision$ $Date$
 */
public class IncluirEquiparacaoContratoRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _maxOcorrencias
     */
    private int _maxOcorrencias;

    /**
     * keeps track of state for field: _maxOcorrencias
     */
    private boolean _has_maxOcorrencias;

    /**
     * Field _ocorrenciasList
     */
    private java.util.Vector _ocorrenciasList;


      //----------------/
     //- Constructors -/
    //----------------/

    public IncluirEquiparacaoContratoRequest() 
     {
        super();
        _ocorrenciasList = new Vector();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.incluirequiparacaocontrato.request.IncluirEquiparacaoContratoRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method addOcorrencias
     * 
     * 
     * 
     * @param vOcorrencias
     */
    public void addOcorrencias(br.com.bradesco.web.pgit.service.data.pdc.incluirequiparacaocontrato.request.Ocorrencias vOcorrencias)
        throws java.lang.IndexOutOfBoundsException
    {
        _ocorrenciasList.addElement(vOcorrencias);
    } //-- void addOcorrencias(br.com.bradesco.web.pgit.service.data.pdc.incluirequiparacaocontrato.request.Ocorrencias) 

    /**
     * Method addOcorrencias
     * 
     * 
     * 
     * @param index
     * @param vOcorrencias
     */
    public void addOcorrencias(int index, br.com.bradesco.web.pgit.service.data.pdc.incluirequiparacaocontrato.request.Ocorrencias vOcorrencias)
        throws java.lang.IndexOutOfBoundsException
    {
        _ocorrenciasList.insertElementAt(vOcorrencias, index);
    } //-- void addOcorrencias(int, br.com.bradesco.web.pgit.service.data.pdc.incluirequiparacaocontrato.request.Ocorrencias) 

    /**
     * Method deleteMaxOcorrencias
     * 
     */
    public void deleteMaxOcorrencias()
    {
        this._has_maxOcorrencias= false;
    } //-- void deleteMaxOcorrencias() 

    /**
     * Method enumerateOcorrencias
     * 
     * 
     * 
     * @return Enumeration
     */
    public java.util.Enumeration enumerateOcorrencias()
    {
        return _ocorrenciasList.elements();
    } //-- java.util.Enumeration enumerateOcorrencias() 

    /**
     * Returns the value of field 'maxOcorrencias'.
     * 
     * @return int
     * @return the value of field 'maxOcorrencias'.
     */
    public int getMaxOcorrencias()
    {
        return this._maxOcorrencias;
    } //-- int getMaxOcorrencias() 

    /**
     * Method getOcorrencias
     * 
     * 
     * 
     * @param index
     * @return Ocorrencias
     */
    public br.com.bradesco.web.pgit.service.data.pdc.incluirequiparacaocontrato.request.Ocorrencias getOcorrencias(int index)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index >= _ocorrenciasList.size())) {
            throw new IndexOutOfBoundsException("getOcorrencias: Index value '"+index+"' not in range [0.."+(_ocorrenciasList.size() - 1) + "]");
        }
        
        return (br.com.bradesco.web.pgit.service.data.pdc.incluirequiparacaocontrato.request.Ocorrencias) _ocorrenciasList.elementAt(index);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.incluirequiparacaocontrato.request.Ocorrencias getOcorrencias(int) 

    /**
     * Method getOcorrencias
     * 
     * 
     * 
     * @return Ocorrencias
     */
    public br.com.bradesco.web.pgit.service.data.pdc.incluirequiparacaocontrato.request.Ocorrencias[] getOcorrencias()
    {
        int size = _ocorrenciasList.size();
        br.com.bradesco.web.pgit.service.data.pdc.incluirequiparacaocontrato.request.Ocorrencias[] mArray = new br.com.bradesco.web.pgit.service.data.pdc.incluirequiparacaocontrato.request.Ocorrencias[size];
        for (int index = 0; index < size; index++) {
            mArray[index] = (br.com.bradesco.web.pgit.service.data.pdc.incluirequiparacaocontrato.request.Ocorrencias) _ocorrenciasList.elementAt(index);
        }
        return mArray;
    } //-- br.com.bradesco.web.pgit.service.data.pdc.incluirequiparacaocontrato.request.Ocorrencias[] getOcorrencias() 

    /**
     * Method getOcorrenciasCount
     * 
     * 
     * 
     * @return int
     */
    public int getOcorrenciasCount()
    {
        return _ocorrenciasList.size();
    } //-- int getOcorrenciasCount() 

    /**
     * Method hasMaxOcorrencias
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasMaxOcorrencias()
    {
        return this._has_maxOcorrencias;
    } //-- boolean hasMaxOcorrencias() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Method removeAllOcorrencias
     * 
     */
    public void removeAllOcorrencias()
    {
        _ocorrenciasList.removeAllElements();
    } //-- void removeAllOcorrencias() 

    /**
     * Method removeOcorrencias
     * 
     * 
     * 
     * @param index
     * @return Ocorrencias
     */
    public br.com.bradesco.web.pgit.service.data.pdc.incluirequiparacaocontrato.request.Ocorrencias removeOcorrencias(int index)
    {
        java.lang.Object obj = _ocorrenciasList.elementAt(index);
        _ocorrenciasList.removeElementAt(index);
        return (br.com.bradesco.web.pgit.service.data.pdc.incluirequiparacaocontrato.request.Ocorrencias) obj;
    } //-- br.com.bradesco.web.pgit.service.data.pdc.incluirequiparacaocontrato.request.Ocorrencias removeOcorrencias(int) 

    /**
     * Sets the value of field 'maxOcorrencias'.
     * 
     * @param maxOcorrencias the value of field 'maxOcorrencias'.
     */
    public void setMaxOcorrencias(int maxOcorrencias)
    {
        this._maxOcorrencias = maxOcorrencias;
        this._has_maxOcorrencias = true;
    } //-- void setMaxOcorrencias(int) 

    /**
     * Method setOcorrencias
     * 
     * 
     * 
     * @param index
     * @param vOcorrencias
     */
    public void setOcorrencias(int index, br.com.bradesco.web.pgit.service.data.pdc.incluirequiparacaocontrato.request.Ocorrencias vOcorrencias)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index >= _ocorrenciasList.size())) {
            throw new IndexOutOfBoundsException("setOcorrencias: Index value '"+index+"' not in range [0.." + (_ocorrenciasList.size() - 1) + "]");
        }
        _ocorrenciasList.setElementAt(vOcorrencias, index);
    } //-- void setOcorrencias(int, br.com.bradesco.web.pgit.service.data.pdc.incluirequiparacaocontrato.request.Ocorrencias) 

    /**
     * Method setOcorrencias
     * 
     * 
     * 
     * @param ocorrenciasArray
     */
    public void setOcorrencias(br.com.bradesco.web.pgit.service.data.pdc.incluirequiparacaocontrato.request.Ocorrencias[] ocorrenciasArray)
    {
        //-- copy array
        _ocorrenciasList.removeAllElements();
        for (int i = 0; i < ocorrenciasArray.length; i++) {
            _ocorrenciasList.addElement(ocorrenciasArray[i]);
        }
    } //-- void setOcorrencias(br.com.bradesco.web.pgit.service.data.pdc.incluirequiparacaocontrato.request.Ocorrencias) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return IncluirEquiparacaoContratoRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.incluirequiparacaocontrato.request.IncluirEquiparacaoContratoRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.incluirequiparacaocontrato.request.IncluirEquiparacaoContratoRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.incluirequiparacaocontrato.request.IncluirEquiparacaoContratoRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.incluirequiparacaocontrato.request.IncluirEquiparacaoContratoRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
