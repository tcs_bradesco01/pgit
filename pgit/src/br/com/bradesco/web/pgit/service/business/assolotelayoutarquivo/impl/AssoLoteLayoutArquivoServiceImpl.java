/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/implementation.ftl,v $
 * $Id: implementation.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: implementation.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */
 
package br.com.bradesco.web.pgit.service.business.assolotelayoutarquivo.impl;

import java.util.ArrayList;
import java.util.List;

import br.com.bradesco.web.pgit.service.business.assolotelayoutarquivo.IAssoLoteLayoutArquivoService;
import br.com.bradesco.web.pgit.service.business.assolotelayoutarquivo.bean.AssLoteLayoutArquivoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.assolotelayoutarquivo.bean.AssLoteLayoutArquivoSaidaDTO;
import br.com.bradesco.web.pgit.service.data.pdc.FactoryAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.detalharasslotelayoutarquivo.request.DetalharAssLoteLayoutArquivoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.detalharasslotelayoutarquivo.response.DetalharAssLoteLayoutArquivoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.excluirasslotelayoutarquivo.request.ExcluirAssLoteLayoutArquivoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.excluirasslotelayoutarquivo.response.ExcluirAssLoteLayoutArquivoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.incluirasslotelayoutarquivo.request.IncluirAssLoteLayoutArquivoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.incluirasslotelayoutarquivo.response.IncluirAssLoteLayoutArquivoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarasslotelayoutarquivo.request.ListarAssLoteLayoutArquivoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarasslotelayoutarquivo.response.ListarAssLoteLayoutArquivoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarasslotelayoutarquivo.response.Ocorrencias;


/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Implementa��o do adaptador: AssoLoteLayoutArquivo
 * </p>
 * 
 * @comment C�DIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public class AssoLoteLayoutArquivoServiceImpl implements IAssoLoteLayoutArquivoService {
	
	/** Atributo factoryAdapter. */
	private FactoryAdapter factoryAdapter;
   

	/**
	 * Get: factoryAdapter.
	 *
	 * @return factoryAdapter
	 */
	public FactoryAdapter getFactoryAdapter() {
		return factoryAdapter;
	}

	/**
	 * Set: factoryAdapter.
	 *
	 * @param factoryAdapter the factory adapter
	 */
	public void setFactoryAdapter(FactoryAdapter factoryAdapter) {
		this.factoryAdapter = factoryAdapter;
	}



	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.assolotelayoutarquivo.IAssoLoteLayoutArquivoService#detalharAssociacaoLoteLayoutArquivo(br.com.bradesco.web.pgit.service.business.assolotelayoutarquivo.bean.AssLoteLayoutArquivoEntradaDTO)
	 */
	public AssLoteLayoutArquivoSaidaDTO detalharAssociacaoLoteLayoutArquivo(AssLoteLayoutArquivoEntradaDTO assLoteLayoutArquivoEntradaDTO) {
		AssLoteLayoutArquivoSaidaDTO assLoteLayoutArquivoSaidaDTO = new AssLoteLayoutArquivoSaidaDTO();
		DetalharAssLoteLayoutArquivoRequest detalharAssLoteLayoutArquivoRequest = new DetalharAssLoteLayoutArquivoRequest();
		DetalharAssLoteLayoutArquivoResponse detalharAssLoteLayoutArquivoResponse = new DetalharAssLoteLayoutArquivoResponse();
		
		detalharAssLoteLayoutArquivoRequest.setCdTipoLayoutArquivo(assLoteLayoutArquivoEntradaDTO.getCdTipoLayoutArquivo());
		detalharAssLoteLayoutArquivoRequest.setCdTipoLoteLayout(assLoteLayoutArquivoEntradaDTO.getCdTipoLoteLayout());
		detalharAssLoteLayoutArquivoRequest.setQtConsultas(1);
		
		detalharAssLoteLayoutArquivoResponse = getFactoryAdapter().getDetalharAssLoteLayoutArquivoPDCAdapter().invokeProcess(detalharAssLoteLayoutArquivoRequest);
		
		if(detalharAssLoteLayoutArquivoResponse == null){
    		
    		return new AssLoteLayoutArquivoSaidaDTO();
    	}
    	
    	    	 
    	for(int i =0; i < detalharAssLoteLayoutArquivoResponse.getOcorrenciasCount(); i++){
    		br.com.bradesco.web.pgit.service.data.pdc.detalharasslotelayoutarquivo.response.Ocorrencias parametro = detalharAssLoteLayoutArquivoResponse.getOcorrencias(i); 
    		
    		assLoteLayoutArquivoSaidaDTO.setCdTipoLoteLayout(parametro.getCdTipoLoteLayout());			
    		assLoteLayoutArquivoSaidaDTO.setDsTipoLoteLayout(parametro.getDsTipoLoteLayout());
    		assLoteLayoutArquivoSaidaDTO.setCdTipoLayoutArquivo(parametro.getCdTipoLayoutArquivo());
    		assLoteLayoutArquivoSaidaDTO.setDsTipoLayoutArquivo(parametro.getDsTipoLayoutArquivo());
    		assLoteLayoutArquivoSaidaDTO.setCdCanalInclusao(parametro.getCdCanalInclusao());
    		assLoteLayoutArquivoSaidaDTO.setDsCanalInclusao(parametro.getDsCanalInclusao());
    		assLoteLayoutArquivoSaidaDTO.setCdAutenticacaoSegurancaInclusao(parametro.getCdAutenSegregacaoInclusao());
    		assLoteLayoutArquivoSaidaDTO.setNrOperacaoFluxoInclusao(parametro.getNmOperacaoFluxoInclusao());
    		assLoteLayoutArquivoSaidaDTO.setHrInclusaoRegistro(parametro.getHrInclusaoRegistro());
    		
    		assLoteLayoutArquivoSaidaDTO.setCdCanalManutencao(parametro.getCdCanalManutencao());
    		assLoteLayoutArquivoSaidaDTO.setDsCanalManutencao(parametro.getDsCanalManutencao());
    		assLoteLayoutArquivoSaidaDTO.setCdAutenticacaoSegurancaManutencao(parametro.getCdAutenSegregacaoManutencao());
    		assLoteLayoutArquivoSaidaDTO.setNrOperacaoFluxoManutencao(parametro.getNmOperacaoFluxoManutencao());
    		assLoteLayoutArquivoSaidaDTO.setHrManutencaoRegistroManutencao(parametro.getHrManutencaoRegistro());
    		
    		break;
    	}
							
		return assLoteLayoutArquivoSaidaDTO;	

	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.assolotelayoutarquivo.IAssoLoteLayoutArquivoService#excluirAssociacaoLoteLayoutArquivo(br.com.bradesco.web.pgit.service.business.assolotelayoutarquivo.bean.AssLoteLayoutArquivoEntradaDTO)
	 */
	public AssLoteLayoutArquivoSaidaDTO excluirAssociacaoLoteLayoutArquivo(AssLoteLayoutArquivoEntradaDTO assLoteLayoutArquivoEntradaDTO) {
		AssLoteLayoutArquivoSaidaDTO assLoteLayoutArquivoSaidaDTO = new AssLoteLayoutArquivoSaidaDTO();
		ExcluirAssLoteLayoutArquivoRequest excluirAssLoteLayoutArquivoRequest = new ExcluirAssLoteLayoutArquivoRequest();
		ExcluirAssLoteLayoutArquivoResponse excluirAssLoteLayoutArquivoResponse = new ExcluirAssLoteLayoutArquivoResponse();
		
		excluirAssLoteLayoutArquivoRequest.setCdTipoLayoutArquivo(assLoteLayoutArquivoEntradaDTO.getCdTipoLayoutArquivo());		
		excluirAssLoteLayoutArquivoRequest.setCdTipoLoteLayout(assLoteLayoutArquivoEntradaDTO.getCdTipoLoteLayout());
		excluirAssLoteLayoutArquivoRequest.setQtConsultas(assLoteLayoutArquivoEntradaDTO.getQtConsultas());
						
		excluirAssLoteLayoutArquivoResponse = getFactoryAdapter().getExcluirAssLoteLayoutArquivoPDCAdapter().invokeProcess(excluirAssLoteLayoutArquivoRequest);
				
		assLoteLayoutArquivoSaidaDTO.setCodMensagem(excluirAssLoteLayoutArquivoResponse.getCodMensagem());
		assLoteLayoutArquivoSaidaDTO.setMensagem(excluirAssLoteLayoutArquivoResponse.getMensagem());
			
		return assLoteLayoutArquivoSaidaDTO;

	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.assolotelayoutarquivo.IAssoLoteLayoutArquivoService#incluirAssociacaoLoteLayoutArquivo(br.com.bradesco.web.pgit.service.business.assolotelayoutarquivo.bean.AssLoteLayoutArquivoEntradaDTO)
	 */
	public AssLoteLayoutArquivoSaidaDTO incluirAssociacaoLoteLayoutArquivo(AssLoteLayoutArquivoEntradaDTO assLoteLayoutArquivoEntradaDTO) {
		AssLoteLayoutArquivoSaidaDTO assLoteLayoutArquivoSaidaDTO = new AssLoteLayoutArquivoSaidaDTO();
		IncluirAssLoteLayoutArquivoRequest incluirAssLoteLayoutArquivoRequest = new IncluirAssLoteLayoutArquivoRequest();
		IncluirAssLoteLayoutArquivoResponse incluirAssLoteLayoutArquivoResponse = new IncluirAssLoteLayoutArquivoResponse();
		
		incluirAssLoteLayoutArquivoRequest.setCdTipoLayoutArquivo(assLoteLayoutArquivoEntradaDTO.getCdTipoLayoutArquivo());
		incluirAssLoteLayoutArquivoRequest.setCdTipoLoteLayout(assLoteLayoutArquivoEntradaDTO.getCdTipoLoteLayout());
		incluirAssLoteLayoutArquivoRequest.setQtConsultas(1);
		
		incluirAssLoteLayoutArquivoResponse = getFactoryAdapter().getIncluirAssLoteLayoutArquivoPDCAdapter().invokeProcess(incluirAssLoteLayoutArquivoRequest);
					
		assLoteLayoutArquivoSaidaDTO.setCodMensagem(incluirAssLoteLayoutArquivoResponse.getCodMensagem());
		assLoteLayoutArquivoSaidaDTO.setMensagem(incluirAssLoteLayoutArquivoResponse.getMensagem());
				
		return assLoteLayoutArquivoSaidaDTO;
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.assolotelayoutarquivo.IAssoLoteLayoutArquivoService#listarAssociacaoLoteLayoutArquivo(br.com.bradesco.web.pgit.service.business.assolotelayoutarquivo.bean.AssLoteLayoutArquivoEntradaDTO)
	 */
	public List<AssLoteLayoutArquivoSaidaDTO> listarAssociacaoLoteLayoutArquivo(AssLoteLayoutArquivoEntradaDTO assLoteLayoutArquivoEntradaDTO) {
		ListarAssLoteLayoutArquivoRequest listarAssLoteLayoutArquivoRequest = new ListarAssLoteLayoutArquivoRequest();
		ListarAssLoteLayoutArquivoResponse listarAssLoteLayoutArquivoResponse = new ListarAssLoteLayoutArquivoResponse();
		
		listarAssLoteLayoutArquivoRequest.setCdTipoLayoutArquivo(assLoteLayoutArquivoEntradaDTO.getCdTipoLayoutArquivo());
		listarAssLoteLayoutArquivoRequest.setCdTipoLoteLayout(assLoteLayoutArquivoEntradaDTO.getCdTipoLoteLayout());
		listarAssLoteLayoutArquivoRequest.setQtConsultas(assLoteLayoutArquivoEntradaDTO.getQtConsultas());
		
		listarAssLoteLayoutArquivoResponse = getFactoryAdapter().getListarAssLoteLayoutArquivoPDCAdapter().invokeProcess(listarAssLoteLayoutArquivoRequest);

		if(listarAssLoteLayoutArquivoResponse == null){
    		
    		return new ArrayList<AssLoteLayoutArquivoSaidaDTO>();
    	}
    	
    	 List<AssLoteLayoutArquivoSaidaDTO> listRetorno= new ArrayList<AssLoteLayoutArquivoSaidaDTO>();
    	 
    	for(int i =0; i < listarAssLoteLayoutArquivoResponse.getOcorrenciasCount(); i++){
    		Ocorrencias parametro = listarAssLoteLayoutArquivoResponse.getOcorrencias(i); 
    		
    		if(parametro.getCdTipoLayoutArquivo() > 0){
    			AssLoteLayoutArquivoSaidaDTO saida = new AssLoteLayoutArquivoSaidaDTO(parametro.getCdTipoLayoutArquivo(), parametro.getCdTipoLoteLayout(), parametro.getDsTipoLayoutArquivo(), parametro.getDsTipoLoteLayout());
    			listRetorno.add(saida);
    		}
    	}
		
		return listRetorno;	

	}
    
    
    
}

