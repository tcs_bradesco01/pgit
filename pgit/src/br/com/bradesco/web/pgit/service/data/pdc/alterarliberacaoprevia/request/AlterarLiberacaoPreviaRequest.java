/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.alterarliberacaoprevia.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;

/**
 * Class AlterarLiberacaoPreviaRequest.
 * 
 * @version $Revision$ $Date$
 */
public class AlterarLiberacaoPreviaRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdPessoaJuridicaContrato
     */
    private long _cdPessoaJuridicaContrato = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridicaContrato
     */
    private boolean _has_cdPessoaJuridicaContrato;

    /**
     * Field _cdTipoContratoNegocio
     */
    private int _cdTipoContratoNegocio = 0;

    /**
     * keeps track of state for field: _cdTipoContratoNegocio
     */
    private boolean _has_cdTipoContratoNegocio;

    /**
     * Field _nrSequenciaContratoNegocio
     */
    private long _nrSequenciaContratoNegocio = 0;

    /**
     * keeps track of state for field: _nrSequenciaContratoNegocio
     */
    private boolean _has_nrSequenciaContratoNegocio;

    /**
     * Field _cdTipoSolicitacao
     */
    private int _cdTipoSolicitacao = 0;

    /**
     * keeps track of state for field: _cdTipoSolicitacao
     */
    private boolean _has_cdTipoSolicitacao;

    /**
     * Field _nrSolicitacao
     */
    private long _nrSolicitacao = 0;

    /**
     * keeps track of state for field: _nrSolicitacao
     */
    private boolean _has_nrSolicitacao;

    /**
     * Field _dtProgramada
     */
    private java.lang.String _dtProgramada;

    /**
     * Field _vlAutorizado
     */
    private java.math.BigDecimal _vlAutorizado = new java.math.BigDecimal("0");


      //----------------/
     //- Constructors -/
    //----------------/

    public AlterarLiberacaoPreviaRequest() 
     {
        super();
        setVlAutorizado(new java.math.BigDecimal("0"));
    } //-- br.com.bradesco.web.pgit.service.data.pdc.alterarliberacaoprevia.request.AlterarLiberacaoPreviaRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdPessoaJuridicaContrato
     * 
     */
    public void deleteCdPessoaJuridicaContrato()
    {
        this._has_cdPessoaJuridicaContrato= false;
    } //-- void deleteCdPessoaJuridicaContrato() 

    /**
     * Method deleteCdTipoContratoNegocio
     * 
     */
    public void deleteCdTipoContratoNegocio()
    {
        this._has_cdTipoContratoNegocio= false;
    } //-- void deleteCdTipoContratoNegocio() 

    /**
     * Method deleteCdTipoSolicitacao
     * 
     */
    public void deleteCdTipoSolicitacao()
    {
        this._has_cdTipoSolicitacao= false;
    } //-- void deleteCdTipoSolicitacao() 

    /**
     * Method deleteNrSequenciaContratoNegocio
     * 
     */
    public void deleteNrSequenciaContratoNegocio()
    {
        this._has_nrSequenciaContratoNegocio= false;
    } //-- void deleteNrSequenciaContratoNegocio() 

    /**
     * Method deleteNrSolicitacao
     * 
     */
    public void deleteNrSolicitacao()
    {
        this._has_nrSolicitacao= false;
    } //-- void deleteNrSolicitacao() 

    /**
     * Returns the value of field 'cdPessoaJuridicaContrato'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridicaContrato'.
     */
    public long getCdPessoaJuridicaContrato()
    {
        return this._cdPessoaJuridicaContrato;
    } //-- long getCdPessoaJuridicaContrato() 

    /**
     * Returns the value of field 'cdTipoContratoNegocio'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoNegocio'.
     */
    public int getCdTipoContratoNegocio()
    {
        return this._cdTipoContratoNegocio;
    } //-- int getCdTipoContratoNegocio() 

    /**
     * Returns the value of field 'cdTipoSolicitacao'.
     * 
     * @return int
     * @return the value of field 'cdTipoSolicitacao'.
     */
    public int getCdTipoSolicitacao()
    {
        return this._cdTipoSolicitacao;
    } //-- int getCdTipoSolicitacao() 

    /**
     * Returns the value of field 'dtProgramada'.
     * 
     * @return String
     * @return the value of field 'dtProgramada'.
     */
    public java.lang.String getDtProgramada()
    {
        return this._dtProgramada;
    } //-- java.lang.String getDtProgramada() 

    /**
     * Returns the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @return long
     * @return the value of field 'nrSequenciaContratoNegocio'.
     */
    public long getNrSequenciaContratoNegocio()
    {
        return this._nrSequenciaContratoNegocio;
    } //-- long getNrSequenciaContratoNegocio() 

    /**
     * Returns the value of field 'nrSolicitacao'.
     * 
     * @return long
     * @return the value of field 'nrSolicitacao'.
     */
    public long getNrSolicitacao()
    {
        return this._nrSolicitacao;
    } //-- long getNrSolicitacao() 

    /**
     * Returns the value of field 'vlAutorizado'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlAutorizado'.
     */
    public java.math.BigDecimal getVlAutorizado()
    {
        return this._vlAutorizado;
    } //-- java.math.BigDecimal getVlAutorizado() 

    /**
     * Method hasCdPessoaJuridicaContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridicaContrato()
    {
        return this._has_cdPessoaJuridicaContrato;
    } //-- boolean hasCdPessoaJuridicaContrato() 

    /**
     * Method hasCdTipoContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoNegocio()
    {
        return this._has_cdTipoContratoNegocio;
    } //-- boolean hasCdTipoContratoNegocio() 

    /**
     * Method hasCdTipoSolicitacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoSolicitacao()
    {
        return this._has_cdTipoSolicitacao;
    } //-- boolean hasCdTipoSolicitacao() 

    /**
     * Method hasNrSequenciaContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaContratoNegocio()
    {
        return this._has_nrSequenciaContratoNegocio;
    } //-- boolean hasNrSequenciaContratoNegocio() 

    /**
     * Method hasNrSolicitacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSolicitacao()
    {
        return this._has_nrSolicitacao;
    } //-- boolean hasNrSolicitacao() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdPessoaJuridicaContrato'.
     * 
     * @param cdPessoaJuridicaContrato the value of field
     * 'cdPessoaJuridicaContrato'.
     */
    public void setCdPessoaJuridicaContrato(long cdPessoaJuridicaContrato)
    {
        this._cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
        this._has_cdPessoaJuridicaContrato = true;
    } //-- void setCdPessoaJuridicaContrato(long) 

    /**
     * Sets the value of field 'cdTipoContratoNegocio'.
     * 
     * @param cdTipoContratoNegocio the value of field
     * 'cdTipoContratoNegocio'.
     */
    public void setCdTipoContratoNegocio(int cdTipoContratoNegocio)
    {
        this._cdTipoContratoNegocio = cdTipoContratoNegocio;
        this._has_cdTipoContratoNegocio = true;
    } //-- void setCdTipoContratoNegocio(int) 

    /**
     * Sets the value of field 'cdTipoSolicitacao'.
     * 
     * @param cdTipoSolicitacao the value of field
     * 'cdTipoSolicitacao'.
     */
    public void setCdTipoSolicitacao(int cdTipoSolicitacao)
    {
        this._cdTipoSolicitacao = cdTipoSolicitacao;
        this._has_cdTipoSolicitacao = true;
    } //-- void setCdTipoSolicitacao(int) 

    /**
     * Sets the value of field 'dtProgramada'.
     * 
     * @param dtProgramada the value of field 'dtProgramada'.
     */
    public void setDtProgramada(java.lang.String dtProgramada)
    {
        this._dtProgramada = dtProgramada;
    } //-- void setDtProgramada(java.lang.String) 

    /**
     * Sets the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @param nrSequenciaContratoNegocio the value of field
     * 'nrSequenciaContratoNegocio'.
     */
    public void setNrSequenciaContratoNegocio(long nrSequenciaContratoNegocio)
    {
        this._nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
        this._has_nrSequenciaContratoNegocio = true;
    } //-- void setNrSequenciaContratoNegocio(long) 

    /**
     * Sets the value of field 'nrSolicitacao'.
     * 
     * @param nrSolicitacao the value of field 'nrSolicitacao'.
     */
    public void setNrSolicitacao(long nrSolicitacao)
    {
        this._nrSolicitacao = nrSolicitacao;
        this._has_nrSolicitacao = true;
    } //-- void setNrSolicitacao(long) 

    /**
     * Sets the value of field 'vlAutorizado'.
     * 
     * @param vlAutorizado the value of field 'vlAutorizado'.
     */
    public void setVlAutorizado(java.math.BigDecimal vlAutorizado)
    {
        this._vlAutorizado = vlAutorizado;
    } //-- void setVlAutorizado(java.math.BigDecimal) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return AlterarLiberacaoPreviaRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.alterarliberacaoprevia.request.AlterarLiberacaoPreviaRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.alterarliberacaoprevia.request.AlterarLiberacaoPreviaRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.alterarliberacaoprevia.request.AlterarLiberacaoPreviaRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.alterarliberacaoprevia.request.AlterarLiberacaoPreviaRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
