/*
 * Nome: br.com.bradesco.web.pgit.service.business.assoclayoutarqprodserv.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.assoclayoutarqprodserv.bean;

/**
 * Nome: ListaLayoutArqProdServDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListaLayoutArqProdServDTO {
	
	/** Atributo servico. */
	private String servico;
	
	/** Atributo tipoLayoutArquivo. */
	private String tipoLayoutArquivo;
	
	/**
	 * Get: servico.
	 *
	 * @return servico
	 */
	public String getServico() {
		return servico;
	}
	
	/**
	 * Set: servico.
	 *
	 * @param servico the servico
	 */
	public void setServico(String servico) {
		this.servico = servico;
	}
	
	/**
	 * Get: tipoLayoutArquivo.
	 *
	 * @return tipoLayoutArquivo
	 */
	public String getTipoLayoutArquivo() {
		return tipoLayoutArquivo;
	}
	
	/**
	 * Set: tipoLayoutArquivo.
	 *
	 * @param tipoLayoutArquivo the tipo layout arquivo
	 */
	public void setTipoLayoutArquivo(String tipoLayoutArquivo) {
		this.tipoLayoutArquivo = tipoLayoutArquivo;
	}
	
	
	
}
