/*
 * Nome: br.com.bradesco.web.pgit.service.report.mantercontrato
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.report.mantercontrato;

import java.awt.Color;
import java.util.Date;

import org.apache.commons.lang.StringUtils;

import br.com.bradesco.web.pgit.service.business.imprimirformalizacaoalteracaocontrato.bean.ImprimirAditivoContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.report.base.BasicPdfReport;
import br.com.bradesco.web.pgit.utils.DateUtils;

import com.lowagie.text.Chunk;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.Image;
import com.lowagie.text.Paragraph;
import com.lowagie.text.pdf.PdfCell;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;

/**
 * Nome: TermoAditivoReport
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class TermoAditivoReport extends BasicPdfReport {
	
	/** Atributo fTexto. */
	private Font fTexto = new Font(Font.TIMES_ROMAN, 12, Font.NORMAL);
	
	/** Atributo fTextoTabela. */
	private Font fTextoTabela = new Font(Font.TIMES_ROMAN, 12, Font.BOLD);
	
	/** Atributo fTextoTabelaNormal. */
	private Font fTextoTabelaNormal = new Font(Font.TIMES_ROMAN, 12, Font.NORMAL);
	
	/** Atributo fTextoTabelaCinza. */
	private Font fTextoTabelaCinza = new Font(Font.TIMES_ROMAN, 8, Font.BOLD,Color.GRAY);
	
	/** Atributo imagemRelatorio. */
	private Image imagemRelatorio = null;
	
	/** Atributo rodape. */
	private ContratoRodapeReport rodape = new ContratoRodapeReport();
	
	/** Atributo saidaContratoAditivo. */
	private ImprimirAditivoContratoSaidaDTO saidaContratoAditivo;
	
	/**
	 * Termo aditivo report.
	 *
	 * @param saidaContratoAditivo the saida contrato aditivo
	 */
	public TermoAditivoReport(ImprimirAditivoContratoSaidaDTO saidaContratoAditivo) {
		super();
		this.saidaContratoAditivo = saidaContratoAditivo;
		this.imagemRelatorio = getImagemRelatorio(); 
	}


	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.report.base.BasicPdfReport#popularPdf(com.lowagie.text.Document)
	 */
	@Override
	protected void popularPdf(Document document) throws DocumentException {
		
		preencherCabec(document);
				
		popularCabec(document);
		
		document.add(Chunk.NEWLINE);
		preencherParagrafo1(document);
		preencherParagrafo2(document);
		preencherParagrafo3(document);
		preencherParagrafo4(document);
		
		document.add(Chunk.NEWLINE);
		preencherParagrafo5(document);
		preencherParagrafo6(document);
		
		document.add(Chunk.NEWLINE);
		preencherParagrafo7(document);
		preencherParagrafo8(document);
		preencherParagrafo9(document);
		preencherParagrafo10(document);
		preencherParagrafo11(document);
		
		document.add(Chunk.NEWLINE);
		preencherParagrafo12(document);
		preencherParagrafo13(document);
		
		document.add(Chunk.NEWLINE);
		preencherParagrafo14(document);
		preencherParagrafo15(document);
		preencherParagrafo16(document);
		document.add(Chunk.NEWLINE);
		document.add(Chunk.NEWLINE);
		document.add(Chunk.NEWLINE);
		
		document.add(Chunk.NEXTPAGE);
		document.add(Chunk.NEWLINE);
		preencherData(document);
		
		document.add(Chunk.NEWLINE);
		document.add(Chunk.NEWLINE);
		document.add(Chunk.NEWLINE);
		document.add(Chunk.NEWLINE);
		document.add(Chunk.NEWLINE);
		assinaturaUm(document);
		
		document.add(Chunk.NEWLINE);
		document.add(Chunk.NEWLINE);
		document.add(Chunk.NEWLINE);
		document.add(Chunk.NEWLINE);
		document.add(Chunk.NEWLINE);
		preencherAssinaturas3(document);
		document.add(Chunk.NEWLINE);
		document.add(Chunk.NEWLINE);
		assinaturaDois(document);
		
		document.add(Chunk.NEWLINE);
		document.add(Chunk.NEWLINE);
		document.add(Chunk.NEWLINE);
		document.add(Chunk.NEWLINE);
		document.add(Chunk.NEWLINE);
		document.add(Chunk.NEWLINE);
		document.add(Chunk.NEWLINE);
		document.add(Chunk.NEWLINE);
		document.add(Chunk.NEWLINE);
		document.add(Chunk.NEWLINE);
		document.add(Chunk.NEWLINE);
		document.add(Chunk.NEWLINE);
		document.add(Chunk.NEWLINE);
		rodape.popularPdf(document);
			}
	
	/**
	 * Preencher cabec.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherCabec(Document document) throws DocumentException {
		
		PdfPTable tabela = new PdfPTable(2);
		tabela.setWidthPercentage(100f);
		tabela.setWidths(new float[] {30.0f, 70.0f});
		
		Paragraph pLinha1Esq = new Paragraph(new Chunk("ADITAMENTO ANEXO AO CONTRATO DE PRESTA��O DE SERVI�OS DE PAGAMENTOS.", fTextoTabelaCinza));
		pLinha1Esq.setAlignment(Element.ALIGN_CENTER);
				
		PdfPCell celula1 = new PdfPCell();
		celula1.setBorder(PdfPCell.NO_BORDER);
		celula1.addElement(imagemRelatorio);
		
		PdfPCell celula2 = new PdfPCell();
		celula2.setBorder(PdfCell.NO_BORDER);
		
		celula2.addElement(pLinha1Esq);
		tabela.addCell(celula1);
		tabela.addCell(celula2);
		
		document.add(tabela);
		
	}
	
/**
 * Popular cabec.
 *
 * @param document the document
 * @throws DocumentException the document exception
 */
public void popularCabec(Document document) throws DocumentException {
		
		PdfPTable tabela = new PdfPTable(1);
		tabela.setWidthPercentage(100f);
		
		Paragraph pLinha1Dir = new Paragraph(new Chunk("Identifica��o do Banco:", fTextoTabela));
		pLinha1Dir.setAlignment(Element.ALIGN_LEFT);
		Paragraph pLinha2Dir = new Paragraph(new Chunk("CNPJ/MF", fTextoTabelaNormal));
		pLinha2Dir.setAlignment(Element.ALIGN_LEFT);
		Paragraph pLinha2Dir1 = new Paragraph(new Chunk("	60.746.948/0001-12", fTextoTabela));
		pLinha2Dir1.setAlignment(Element.ALIGN_LEFT);
		Paragraph pLinha3Dir = new Paragraph(new Chunk("Endere�o:", fTextoTabelaNormal));
		pLinha3Dir.setAlignment(Element.ALIGN_LEFT);
		Paragraph pLinha3Dir1 = new Paragraph(new Chunk("	CIDADE DE DEUS - VILA YARA - CEP: 06029-900  - OSASCO - S�O PAULO", fTextoTabela));
		pLinha3Dir1.setAlignment(Element.ALIGN_LEFT);
		
		Paragraph pLinha4Dir = new Paragraph(new Chunk("Identifica��o do Cliente:", fTextoTabela));
		pLinha4Dir.setAlignment(Element.ALIGN_LEFT);

		PdfPTable tabelaInterna = new PdfPTable(1);
		tabelaInterna.setWidthPercentage(100f);
		
		PdfPCell cellIdentBanco = new PdfPCell();
		cellIdentBanco.setBorder(PdfCell.BOX);
		cellIdentBanco.addElement(pLinha1Dir);
		
		PdfPCell celulaBordaPrincipal = new PdfPCell();
		celulaBordaPrincipal.setBorder(PdfCell.NO_BORDER);
		celulaBordaPrincipal.setBorderWidth(0);
		celulaBordaPrincipal.setPaddingTop(20);
		celulaBordaPrincipal.setPaddingBottom(0);
		celulaBordaPrincipal.addElement(tabelaInterna);
		
		//***********************************

		PdfPTable tabelaInterna1 = new PdfPTable(4);
		tabelaInterna1.setWidthPercentage(100f);
		tabelaInterna1.setWidths(new float[] {5.0f, 45.0f, 5.0f, 45.0f});

		PdfPCell cellBanco = new PdfPCell();
		cellBanco.setBorder(PdfCell.BOX);
		cellBanco.addElement(new Paragraph(new Chunk(" 01", fTextoTabela)));
		cellBanco.addElement(new Paragraph());
		
		PdfPCell cellBanco1 = new PdfPCell();
		cellBanco1.setBorder(PdfCell.BOX);
		cellBanco1.addElement(pLinha2Dir);
		cellBanco1.addElement(pLinha2Dir1);
		
		PdfPCell cellBanco2 = new PdfPCell();
		cellBanco2.setBorder(PdfCell.BOX);
		cellBanco2.addElement(new Paragraph(new Chunk(" 02", fTextoTabela)));
		cellBanco2.addElement(new Paragraph());
		
		PdfPCell cellBanco3 = new PdfPCell();
		cellBanco3.setBorder(PdfCell.BOX);
		cellBanco3.addElement(new Paragraph(new Chunk(" ", fTextoTabela)));
		cellBanco3.addElement(new Paragraph(new Chunk("BANCO BRADESCO S.A.", fTextoTabela)));
		
		
		tabelaInterna.addCell(cellIdentBanco);
		tabelaInterna1.addCell(cellBanco);
		tabelaInterna1.addCell(cellBanco1);
		tabelaInterna1.addCell(cellBanco2);
		tabelaInterna1.addCell(cellBanco3);
		
		PdfPCell celulaBordaPrincipal1 = new PdfPCell();
		celulaBordaPrincipal1.setBorder(PdfCell.NO_BORDER);
		celulaBordaPrincipal1.setBorderWidth(0);
		celulaBordaPrincipal1.setPaddingTop(0);
		celulaBordaPrincipal1.setPaddingBottom(0);
		celulaBordaPrincipal1.addElement(tabelaInterna1);
		
		//		***********************************
		
		PdfPTable tabelaInterna2 = new PdfPTable(2);
		tabelaInterna2.setWidthPercentage(100f);
		tabelaInterna2.setWidths(new float[] {5.0f, 95.0f});

		PdfPCell cellEndereco = new PdfPCell();
		cellEndereco.setBorder(PdfCell.BOX);
		cellEndereco.addElement(new Paragraph(new Chunk(" 03", fTextoTabela)));
		cellEndereco.addElement(new Paragraph());
		
		PdfPCell cellEndereco1 = new PdfPCell();
		cellEndereco1.setBorder(PdfCell.BOX);
		cellEndereco1.addElement(pLinha3Dir);
		cellEndereco1.addElement(pLinha3Dir1);
		
		PdfPCell celulaBordaPrincipal2 = new PdfPCell();
		celulaBordaPrincipal2.setBorder(PdfCell.NO_BORDER);
		celulaBordaPrincipal2.setBorderWidth(0);
		celulaBordaPrincipal2.setPaddingTop(0);
		celulaBordaPrincipal2.setPaddingBottom(0);
		celulaBordaPrincipal2.addElement(tabelaInterna2);
		
		tabelaInterna2.addCell(cellEndereco);
		tabelaInterna2.addCell(cellEndereco1);
		
		//		***********************************
		
		PdfPTable tabelaInterna3 = new PdfPTable(1);
		tabelaInterna3.setWidthPercentage(100f);
		
		PdfPCell cellIdentCliente = new PdfPCell();
		cellIdentCliente.setBorder(PdfCell.BOX);
		cellIdentCliente.addElement(pLinha4Dir);
		
		PdfPCell celulaBordaPrincipal3 = new PdfPCell();
		celulaBordaPrincipal3.setBorder(PdfCell.NO_BORDER);
		celulaBordaPrincipal3.setBorderWidth(0);
		celulaBordaPrincipal3.setPaddingTop(0);
		celulaBordaPrincipal3.setPaddingBottom(0);
		celulaBordaPrincipal3.addElement(tabelaInterna3);
		
		tabelaInterna3.addCell(cellIdentCliente);
		
		//		***********************************
		
		PdfPTable tabelaInterna4 = new PdfPTable(4);
		tabelaInterna4.setWidthPercentage(100f);
		tabelaInterna4.setWidths(new float[] {5.0f, 60.0f, 5.0f, 30.0f});
		
		PdfPCell cellIdentCliente1 = new PdfPCell();
		cellIdentCliente1.setBorder(PdfCell.BOX);
		cellIdentCliente1.addElement(new Paragraph(new Chunk(" 04", fTextoTabela)));
		cellIdentCliente1.addElement(new Paragraph());
		
		PdfPCell cellIdentCliente2 = new PdfPCell();
		cellIdentCliente2.setBorder(PdfCell.BOX);
		cellIdentCliente2.addElement(new Paragraph(new Chunk("Nome do Cliente", fTextoTabelaNormal)));
		cellIdentCliente2.addElement(new Paragraph(new Chunk(saidaContratoAditivo.getNomeCliente(), fTextoTabela)));
		
		PdfPCell cellIdentCliente3 = new PdfPCell();
		cellIdentCliente3.setBorder(PdfCell.BOX);
		cellIdentCliente3.addElement(new Paragraph(new Chunk(" 05", fTextoTabela)));
		cellIdentCliente3.addElement(new Paragraph());
		
		PdfPCell cellIdentCliente4 = new PdfPCell();
		cellIdentCliente4.setBorder(PdfCell.BOX);
		cellIdentCliente4.addElement(new Paragraph(new Chunk("CNPJ/MF ou", fTextoTabelaNormal)));
		cellIdentCliente4.addElement(new Paragraph(new Chunk("	" + saidaContratoAditivo.getCdCpfCnpjEmpresa(), fTextoTabela)));
		
		PdfPCell celulaBordaPrincipal4 = new PdfPCell();
		celulaBordaPrincipal4.setBorder(PdfCell.NO_BORDER);
		celulaBordaPrincipal4.setBorderWidth(0);
		celulaBordaPrincipal4.setPaddingTop(0);
		celulaBordaPrincipal4.setPaddingBottom(0);
		celulaBordaPrincipal4.addElement(tabelaInterna4);
		
		tabelaInterna4.addCell(cellIdentCliente1);
		tabelaInterna4.addCell(cellIdentCliente2);
		tabelaInterna4.addCell(cellIdentCliente3);
		tabelaInterna4.addCell(cellIdentCliente4);
		
//		***********************************
		PdfPTable tabelaInterna5 = new PdfPTable(2);
		tabelaInterna5.setWidthPercentage(100f);
		tabelaInterna5.setWidths(new float[] {5.0f, 95.0f});

		PdfPCell cellClienteEndereco = new PdfPCell();
		cellClienteEndereco.setBorder(PdfCell.BOX);
		cellClienteEndereco.addElement(new Paragraph(new Chunk(" 06", fTextoTabela)));
		cellClienteEndereco.addElement(new Paragraph());
		
		PdfPCell cellClienteEndereco1 = new PdfPCell();
		cellClienteEndereco1.setBorder(PdfCell.BOX);
		cellClienteEndereco1.addElement(new Paragraph(new Chunk("Sede / Endere�o", fTextoTabelaNormal)));
		cellClienteEndereco1.addElement(new Paragraph(new Chunk(saidaContratoAditivo.getDsLogradouroPessoa() + " " + saidaContratoAditivo.getDsLogradouroNumero() + " " + saidaContratoAditivo.getDsComplementoEndereco() + " " + saidaContratoAditivo.getDsBairroEndereco(), fTextoTabela)));
		
		PdfPCell celulaBordaPrincipal5 = new PdfPCell();
		celulaBordaPrincipal5.setBorder(PdfCell.NO_BORDER);
		celulaBordaPrincipal5.setBorderWidth(0);
		celulaBordaPrincipal5.setPaddingTop(0);
		celulaBordaPrincipal5.setPaddingBottom(0);
		celulaBordaPrincipal5.addElement(tabelaInterna5);
		
		tabelaInterna5.addCell(cellClienteEndereco);
		tabelaInterna5.addCell(cellClienteEndereco1);
		
		
		//		***********************************
		
		PdfPTable tabelaInterna6 = new PdfPTable(5);
		tabelaInterna6.setWidthPercentage(100f);
		tabelaInterna6.setWidths(new float[] {5.0f, 15.0f, 35.0f, 35.0f, 10.0f});

		PdfPCell cellClienteEndereco2 = new PdfPCell();
		cellClienteEndereco2.setBorder(PdfCell.BOX);
		cellClienteEndereco2.addElement(new Paragraph(new Chunk(" 07", fTextoTabela)));
		cellClienteEndereco2.addElement(new Paragraph());
		
		PdfPCell cellClienteEndereco3 = new PdfPCell();
		cellClienteEndereco3.setBorder(PdfCell.BOX);
		cellClienteEndereco3.addElement(new Paragraph(new Chunk("CEP", fTextoTabelaNormal)));
		cellClienteEndereco3.addElement(new Paragraph(new Chunk(saidaContratoAditivo.getCdCep() + "-" + saidaContratoAditivo.getCdCepComplemento(), fTextoTabela)));
		
		PdfPCell cellClienteEndereco4 = new PdfPCell();
		cellClienteEndereco4.setBorder(PdfCell.BOX);
		cellClienteEndereco4.addElement(new Paragraph(new Chunk("Bairro", fTextoTabelaNormal)));
		cellClienteEndereco4.addElement(new Paragraph(new Chunk(saidaContratoAditivo.getDsBairroEndereco(), fTextoTabela)));
		
		PdfPCell cellClienteEndereco5 = new PdfPCell();
		cellClienteEndereco5.setBorder(PdfCell.BOX);
		cellClienteEndereco5.addElement(new Paragraph(new Chunk("Cidade", fTextoTabelaNormal)));
		cellClienteEndereco5.addElement(new Paragraph(new Chunk(saidaContratoAditivo.getDsCidadeEndereco() , fTextoTabela)));
		
		PdfPCell cellClienteEndereco6 = new PdfPCell();
		cellClienteEndereco6.setBorder(PdfCell.BOX);
		cellClienteEndereco6.addElement(new Paragraph(new Chunk("UF", fTextoTabelaNormal)));
		cellClienteEndereco6.addElement(new Paragraph(new Chunk(saidaContratoAditivo.getCdSiglaUf() , fTextoTabela)));
		
		PdfPCell celulaBordaPrincipal6 = new PdfPCell();
		celulaBordaPrincipal6.setBorder(PdfCell.NO_BORDER);
		celulaBordaPrincipal6.setBorderWidth(0);
		celulaBordaPrincipal6.setPaddingTop(0);
		celulaBordaPrincipal6.setPaddingBottom(0);
		celulaBordaPrincipal6.addElement(tabelaInterna6);
		
		tabelaInterna6.addCell(cellClienteEndereco2);
		tabelaInterna6.addCell(cellClienteEndereco3);
		tabelaInterna6.addCell(cellClienteEndereco4);
		tabelaInterna6.addCell(cellClienteEndereco5);
		tabelaInterna6.addCell(cellClienteEndereco6);
		
//		***********************************
		
		PdfPTable tabelaInterna7 = new PdfPTable(5);
		tabelaInterna7.setWidthPercentage(100f);
		tabelaInterna7.setWidths(new float[] {5.0f, 50.0f, 5.0f, 10.0f, 30.0f});

		PdfPCell cellClienteEndereco7 = new PdfPCell();
		cellClienteEndereco7.setBorder(PdfCell.BOX);
		cellClienteEndereco7.addElement(new Paragraph(new Chunk(" 08", fTextoTabela)));
		cellClienteEndereco7.addElement(new Paragraph());
		
		PdfPCell cellClienteEndereco8 = new PdfPCell();
		cellClienteEndereco8.setBorder(PdfCell.BOX);
		cellClienteEndereco8.addElement(new Paragraph(new Chunk("C�digo	    Dig.     Ag�ncia", fTextoTabelaNormal)));
		cellClienteEndereco8.addElement(new Paragraph(new Chunk(StringUtils.rightPad(saidaContratoAditivo.getCdAgenciaGestorContrato().toString(), 18, " ") + StringUtils.rightPad(saidaContratoAditivo.getDigitoAgenciaGestor().toString(), 9 , " ") + saidaContratoAditivo.getDsAgenciaGestor(), fTextoTabela)));
		
		PdfPCell cellClienteEndereco9 = new PdfPCell();
		cellClienteEndereco9.setBorder(PdfCell.BOX);
		cellClienteEndereco9.addElement(new Paragraph(new Chunk(" 9", fTextoTabela)));
		cellClienteEndereco9.addElement(new Paragraph());
		
		PdfPCell cellClienteEndereco10 = new PdfPCell();
		cellClienteEndereco10.setBorder(PdfCell.BOX);
		cellClienteEndereco10.addElement(new Paragraph(new Chunk("Raz�o", fTextoTabelaNormal)));
		cellClienteEndereco10.addElement(new Paragraph(new Chunk("RZ" , fTextoTabela)));
		
		PdfPCell cellClienteEndereco11 = new PdfPCell();
		cellClienteEndereco11.setBorder(PdfCell.BOX);
		cellClienteEndereco11.addElement(new Paragraph(new Chunk("N� Conta Corrente	        Dig.", fTextoTabelaNormal)));
		cellClienteEndereco11.addElement(new Paragraph(new Chunk(StringUtils.rightPad(saidaContratoAditivo.getCdContaAgenciaGestor().toString(), 35, " ") + saidaContratoAditivo.getDigitoContaGestor(), fTextoTabela)));
		
		PdfPCell celulaBordaPrincipal7 = new PdfPCell();
		celulaBordaPrincipal7.setBorder(PdfCell.NO_BORDER);
		celulaBordaPrincipal7.setBorderWidth(0);
		celulaBordaPrincipal7.setPaddingTop(0);
		celulaBordaPrincipal7.setPaddingBottom(0);
		celulaBordaPrincipal7.addElement(tabelaInterna7);
		
		tabelaInterna7.addCell(cellClienteEndereco7);
		tabelaInterna7.addCell(cellClienteEndereco8);
		tabelaInterna7.addCell(cellClienteEndereco9);
		tabelaInterna7.addCell(cellClienteEndereco10);
		tabelaInterna7.addCell(cellClienteEndereco11);
		
		
		//		***********************************
		
		tabela.addCell(celulaBordaPrincipal);
		tabela.addCell(celulaBordaPrincipal1);
		tabela.addCell(celulaBordaPrincipal2);
		tabela.addCell(celulaBordaPrincipal3);
		tabela.addCell(celulaBordaPrincipal4);
		tabela.addCell(celulaBordaPrincipal5);
		tabela.addCell(celulaBordaPrincipal6);
		tabela.addCell(celulaBordaPrincipal7);

		document.add(tabela);
	}
	
	/**
	 * Preencher paragrafo1.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherParagrafo1(Document document) throws DocumentException {
		Paragraph titulo = new Paragraph(new Chunk("CONSIDERANDO QUE:", fTextoTabela));
		titulo.setAlignment(Element.ALIGN_LEFT);
		document.add(titulo);
	}
	
	/**
	 * Preencher paragrafo2.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherParagrafo2(Document document) throws DocumentException {
		StringBuilder textoClausula = new StringBuilder();
		textoClausula.append("em .......... (inserir data), o Banco e o Cliente firmaram o Contrato de Presta��o de Servi�os ");
		textoClausula.append("de Pagamentos doravante denominado simplesmente �Contrato�, cujo objeto consiste na presta��o, ");
		textoClausula.append("pelo Banco ao Cliente, dos servi�os de pagamento de ............................ (inserir os servi�os); e,");
		Paragraph paragrafo = new Paragraph();
		paragrafo.add(new Chunk("i) ", fTextoTabela));
		paragrafo.add(new Chunk(textoClausula.toString(), fTexto));
		paragrafo.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(paragrafo);
	}
	
	/**
	 * Preencher paragrafo3.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherParagrafo3(Document document) throws DocumentException {
		StringBuilder textoClausula = new StringBuilder();
		textoClausula.append("o Contrato foi celebrado com prazo de vig�ncia indeterminado a contar da data de sua celebra��o.");
		Paragraph paragrafo = new Paragraph();
		paragrafo.add(new Chunk("ii) ", fTextoTabela));
		paragrafo.add(new Chunk(textoClausula.toString(), fTexto));
		paragrafo.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(paragrafo);
	}
	
	/**
	 * Preencher paragrafo4.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherParagrafo4(Document document) throws DocumentException {
		StringBuilder textoClausula = new StringBuilder();
		textoClausula.append("Resolvem as partes, por seus representantes legais ao final assinados, conforme previsto em seus ");
		textoClausula.append("atos constitutivos atualizados, celebrar este .......... Termo Aditivo (�Aditivo�) ");
		textoClausula.append("ao Contrato, conforme as cl�usulas e condi��es adiante enunciadas.");
		Paragraph paragrafo = new Paragraph();
		paragrafo.add(new Chunk(textoClausula.toString(), fTexto));
		paragrafo.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(paragrafo);
	}
	
	/**
	 * Preencher paragrafo5.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherParagrafo5(Document document) throws DocumentException {
		Paragraph titulo = new Paragraph(new Chunk("CL�USULA PRIMEIRA: DO OBJETO", fTextoTabela));
		titulo.setAlignment(Element.ALIGN_LEFT);
		document.add(titulo);
	}
	
	/**
	 * Preencher paragrafo6.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherParagrafo6(Document document) throws DocumentException {
		StringBuilder textoClausula = new StringBuilder();
		textoClausula.append("Este Aditivo tem por objeto alterar a condi��o ........... ");
		textoClausula.append("(descrever o motivo pelo qual o contrato ser� aditado), disposto no Anexo ...........");
		textoClausula.append("( inserir o n�mero do anexo referente ao servi�o).");
		Paragraph paragrafo = new Paragraph();
		paragrafo.add(new Chunk("1.1. ", fTextoTabela));
		paragrafo.add(new Chunk(textoClausula.toString(), fTexto));
		paragrafo.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(paragrafo);
	}
	
	/**
	 * Preencher paragrafo7.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherParagrafo7(Document document) throws DocumentException {
		Paragraph titulo = new Paragraph(new Chunk("CL�USULA SEGUNDA: DA ALTERA��O", fTextoTabela));
		titulo.setAlignment(Element.ALIGN_LEFT);
		document.add(titulo);
	}
	
	/**
	 * Preencher paragrafo8.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherParagrafo8(Document document) throws DocumentException {
		StringBuilder textoClausula = new StringBuilder();
		textoClausula.append("Por este Aditivo e na melhor forma de direito, as partes aven�am aditar ");
		textoClausula.append("o Anexo ... do Contrato, para ficar consignado a altera��o/inclus�o das cl�usulas e condi��es que seguem.");
		Paragraph paragrafo = new Paragraph();
		paragrafo.add(new Chunk("2.1.) ", fTextoTabela));
		paragrafo.add(new Chunk(textoClausula.toString(), fTexto));
		paragrafo.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(paragrafo);
	}
	
	/**
	 * Preencher paragrafo9.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherParagrafo9(Document document) throws DocumentException {
		StringBuilder textoClausula = new StringBuilder();
		textoClausula.append("(......)");
		Paragraph paragrafo = new Paragraph();
		paragrafo.add(new Chunk("2.2. ", fTextoTabela));
		paragrafo.add(new Chunk(textoClausula.toString(), fTexto));
		document.add(paragrafo);
	}
	
	/**
	 * Preencher paragrafo10.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherParagrafo10(Document document) throws DocumentException {
		StringBuilder textoClausula = new StringBuilder();
		textoClausula.append("(......)");
		Paragraph paragrafo = new Paragraph();
		paragrafo.add(new Chunk("2.3. ", fTextoTabela));
		paragrafo.add(new Chunk(textoClausula.toString(), fTexto));
		document.add(paragrafo);
	}
	
	/**
	 * Preencher paragrafo11.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherParagrafo11(Document document) throws DocumentException {
		StringBuilder textoClausula = new StringBuilder();
		textoClausula.append("(......)");
		Paragraph paragrafo = new Paragraph();
		paragrafo.add(new Chunk("2.4. ", fTextoTabela));
		paragrafo.add(new Chunk(textoClausula.toString(), fTexto));
		document.add(paragrafo);
	}
	
	/**
	 * Preencher paragrafo12.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherParagrafo12(Document document) throws DocumentException {
		Paragraph titulo = new Paragraph(new Chunk("CL�USULA TERCEIRA: DAS DECLARA��ES DAS PARTES", fTextoTabela));
		titulo.setAlignment(Element.ALIGN_LEFT);
		document.add(titulo);
	}
	
	/**
	 * Preencher paragrafo13.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherParagrafo13(Document document) throws DocumentException {
		StringBuilder textoClausula = new StringBuilder();
		textoClausula.append("As Partes declaram, para todos os fins de direito, que examinaram todos os termos, ");
		textoClausula.append("cl�usulas e condi��es deste Aditivo, reconhecendo-o como concernente com a Lei e v�lido sob todos os aspectos.");
		Paragraph paragrafo = new Paragraph();
		paragrafo.add(new Chunk("3.1. ", fTextoTabela));
		paragrafo.add(new Chunk(textoClausula.toString(), fTexto));
		paragrafo.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(paragrafo);
	}
	
	/**
	 * Preencher paragrafo14.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherParagrafo14(Document document) throws DocumentException {
		Paragraph titulo = new Paragraph(new Chunk("CL�USULA QUARTA: DAS DEMAIS CONDI��ES", fTextoTabela));
		titulo.setAlignment(Element.ALIGN_LEFT);
		document.add(titulo);
	}
	
	/**
	 * Preencher paragrafo15.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherParagrafo15(Document document) throws DocumentException {
		StringBuilder textoClausula = new StringBuilder();
		textoClausula.append("Partes declaram que tiveram pr�vio conhecimento de todas as cl�usulas e condi��es deste ");
		textoClausula.append("Aditivo, concordando expressamente com todos os seus termos e ratificam, ainda, ");
		textoClausula.append("em todos os seus termos, as cl�usulas, itens e demais condi��es estabelecidas no Contrato, ");
		textoClausula.append("seus Anexos e nos demais Termos Aditivos porventura existentes, ");
		textoClausula.append("n�o expressamente alterados por este Aditivo.");
		Paragraph paragrafo = new Paragraph();
		paragrafo.add(new Chunk("4.1.  ", fTextoTabela));
		paragrafo.add(new Chunk(textoClausula.toString(), fTexto));
		paragrafo.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(paragrafo);
	}
	
	/**
	 * Preencher paragrafo16.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherParagrafo16(Document document) throws DocumentException {
		StringBuilder textoClausula = new StringBuilder();
		textoClausula.append("E por estarem assim, justas e contratadas, as Partes obrigam-se entre ");
		textoClausula.append("si e seus sucessores ao fiel cumprimento de todas as suas cl�usulas e condi��es, pelo");
		textoClausula.append("que assinam este Aditivo em 04 (quatro) vias de igual teor ");
		textoClausula.append("e forma, juntamente com as 02 (duas) testemunhas abaixo nomeadas.");
		Paragraph paragrafo = new Paragraph();
		paragrafo.add(new Chunk(textoClausula.toString(), fTexto));
		paragrafo.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(paragrafo);
	}
	
		
	/**
	 * Preencher data.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherData(Document document) throws DocumentException {
		Paragraph titulo = new Paragraph(new Chunk("_________________________________________", fTexto));
		Paragraph titulo1 = new Paragraph(new Chunk("Osasco, " + DateUtils.formatarDataPorExtenso(new Date()) + ".", fTexto));
		titulo.setAlignment(Element.ALIGN_RIGHT);
		titulo1.setAlignment(Element.ALIGN_RIGHT);
		document.add(titulo);
		document.add(titulo1);
	}
	
	/**
	 * Assinatura um.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void assinaturaUm(Document document) throws DocumentException {
		
		PdfPTable tabela = new PdfPTable(2);
		tabela.setWidthPercentage(100f);
		
		Paragraph pLinha1Dir = new Paragraph(new Chunk("_________________________________________", fTextoTabela));
		pLinha1Dir.setAlignment(Element.ALIGN_CENTER);
		Paragraph pLinha1Esq = new Paragraph(new Chunk("_________________________________________", fTextoTabela));
		pLinha1Esq.setAlignment(Element.ALIGN_CENTER);
		Paragraph pLinha2Dir = new Paragraph(new Chunk("  CLIENTE:", fTextoTabela));
		pLinha2Dir.setAlignment(Element.ALIGN_LEFT);
		Paragraph pLinha2Esq = new Paragraph(new Chunk("  BANCO BRADESCO S/A:", fTextoTabela));
		pLinha2Esq.setAlignment(Element.ALIGN_LEFT);
		Paragraph pLinha3Esq = new Paragraph(new Chunk("  Ag�ncia:", fTexto));
		pLinha3Esq.setAlignment(Element.ALIGN_LEFT);
				
		PdfPCell celula1 = new PdfPCell();
		celula1.setBorder(PdfCell.NO_BORDER);
		
		celula1.addElement(pLinha1Dir);
		celula1.addElement(pLinha2Dir);
				
		PdfPCell celula2 = new PdfPCell();
		celula2.setBorder(PdfCell.NO_BORDER);
		
		celula2.addElement(pLinha1Esq);
		celula2.addElement(pLinha2Esq);
		celula2.addElement(pLinha3Esq);
				
		tabela.addCell(celula1);
		tabela.addCell(celula2);
		
		document.add(tabela);
		
	}
	
	/**
	 * Preencher assinaturas3.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherAssinaturas3(Document document) throws DocumentException {
		Paragraph testemunha = new Paragraph(new Chunk("  TESTEMUNHAS:"));
		testemunha.setAlignment(Element.ALIGN_LEFT);
		document.add(testemunha);
	}
	
	/**
	 * Assinatura dois.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void assinaturaDois(Document document) throws DocumentException {
		
		PdfPTable tabela = new PdfPTable(2);
		tabela.setWidthPercentage(100f);
		
		Paragraph pLinha1Dir = new Paragraph(new Chunk("_________________________________________", fTextoTabela));
		pLinha1Dir.setAlignment(Element.ALIGN_CENTER);
		Paragraph pLinha1Esq = new Paragraph(new Chunk("_________________________________________", fTextoTabela));
		pLinha1Esq.setAlignment(Element.ALIGN_CENTER);
		Paragraph pLinha2Dir = new Paragraph(new Chunk("  NOME:", fTextoTabela));
		pLinha2Dir.setAlignment(Element.ALIGN_LEFT);
		Paragraph pLinha3Dir = new Paragraph(new Chunk("  NOME:", fTextoTabela));
		pLinha3Dir.setAlignment(Element.ALIGN_LEFT);
		Paragraph pLinha2Esq = new Paragraph(new Chunk("  CPF:", fTextoTabela));
		pLinha2Esq.setAlignment(Element.ALIGN_LEFT);
		Paragraph pLinha3Esq = new Paragraph(new Chunk("  CPF:", fTextoTabela));
		pLinha3Esq.setAlignment(Element.ALIGN_LEFT);
				
		PdfPCell celula1 = new PdfPCell();
		celula1.setBorder(PdfCell.NO_BORDER);
		
		celula1.addElement(pLinha1Dir);
		celula1.addElement(pLinha2Dir);
		celula1.addElement(pLinha3Esq);
				
		PdfPCell celula2 = new PdfPCell();
		celula2.setBorder(PdfCell.NO_BORDER);
		
		celula2.addElement(pLinha1Esq);
		celula2.addElement(pLinha2Esq);
		celula2.addElement(pLinha3Dir);
				
		tabela.addCell(celula1);
		tabela.addCell(celula2);
		
		document.add(tabela);
		
	}

	/**
	 * Get: saidaContratoAditivo.
	 *
	 * @return saidaContratoAditivo
	 */
	public ImprimirAditivoContratoSaidaDTO getSaidaContratoAditivo() {
		return saidaContratoAditivo;
	}

	/**
	 * Set: saidaContratoAditivo.
	 *
	 * @param saidaContratoAditivo the saida contrato aditivo
	 */
	public void setSaidaContratoAditivo(ImprimirAditivoContratoSaidaDTO saidaContratoAditivo) {
		this.saidaContratoAditivo = saidaContratoAditivo;
	}
	
}

