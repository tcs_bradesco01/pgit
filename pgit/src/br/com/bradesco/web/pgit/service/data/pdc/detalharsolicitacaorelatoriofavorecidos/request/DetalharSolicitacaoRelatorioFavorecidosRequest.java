/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.detalharsolicitacaorelatoriofavorecidos.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class DetalharSolicitacaoRelatorioFavorecidosRequest.
 * 
 * @version $Revision$ $Date$
 */
public class DetalharSolicitacaoRelatorioFavorecidosRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdTipoSolicitacao
     */
    private int _cdTipoSolicitacao = 0;

    /**
     * keeps track of state for field: _cdTipoSolicitacao
     */
    private boolean _has_cdTipoSolicitacao;

    /**
     * Field _nrSolicitacao
     */
    private int _nrSolicitacao = 0;

    /**
     * keeps track of state for field: _nrSolicitacao
     */
    private boolean _has_nrSolicitacao;


      //----------------/
     //- Constructors -/
    //----------------/

    public DetalharSolicitacaoRelatorioFavorecidosRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharsolicitacaorelatoriofavorecidos.request.DetalharSolicitacaoRelatorioFavorecidosRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdTipoSolicitacao
     * 
     */
    public void deleteCdTipoSolicitacao()
    {
        this._has_cdTipoSolicitacao= false;
    } //-- void deleteCdTipoSolicitacao() 

    /**
     * Method deleteNrSolicitacao
     * 
     */
    public void deleteNrSolicitacao()
    {
        this._has_nrSolicitacao= false;
    } //-- void deleteNrSolicitacao() 

    /**
     * Returns the value of field 'cdTipoSolicitacao'.
     * 
     * @return int
     * @return the value of field 'cdTipoSolicitacao'.
     */
    public int getCdTipoSolicitacao()
    {
        return this._cdTipoSolicitacao;
    } //-- int getCdTipoSolicitacao() 

    /**
     * Returns the value of field 'nrSolicitacao'.
     * 
     * @return int
     * @return the value of field 'nrSolicitacao'.
     */
    public int getNrSolicitacao()
    {
        return this._nrSolicitacao;
    } //-- int getNrSolicitacao() 

    /**
     * Method hasCdTipoSolicitacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoSolicitacao()
    {
        return this._has_cdTipoSolicitacao;
    } //-- boolean hasCdTipoSolicitacao() 

    /**
     * Method hasNrSolicitacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSolicitacao()
    {
        return this._has_nrSolicitacao;
    } //-- boolean hasNrSolicitacao() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdTipoSolicitacao'.
     * 
     * @param cdTipoSolicitacao the value of field
     * 'cdTipoSolicitacao'.
     */
    public void setCdTipoSolicitacao(int cdTipoSolicitacao)
    {
        this._cdTipoSolicitacao = cdTipoSolicitacao;
        this._has_cdTipoSolicitacao = true;
    } //-- void setCdTipoSolicitacao(int) 

    /**
     * Sets the value of field 'nrSolicitacao'.
     * 
     * @param nrSolicitacao the value of field 'nrSolicitacao'.
     */
    public void setNrSolicitacao(int nrSolicitacao)
    {
        this._nrSolicitacao = nrSolicitacao;
        this._has_nrSolicitacao = true;
    } //-- void setNrSolicitacao(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return DetalharSolicitacaoRelatorioFavorecidosRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.detalharsolicitacaorelatoriofavorecidos.request.DetalharSolicitacaoRelatorioFavorecidosRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.detalharsolicitacaorelatoriofavorecidos.request.DetalharSolicitacaoRelatorioFavorecidosRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.detalharsolicitacaorelatoriofavorecidos.request.DetalharSolicitacaoRelatorioFavorecidosRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharsolicitacaorelatoriofavorecidos.request.DetalharSolicitacaoRelatorioFavorecidosRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
