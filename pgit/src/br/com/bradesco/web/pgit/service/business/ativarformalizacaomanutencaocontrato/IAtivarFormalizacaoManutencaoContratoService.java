/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/interfaz.ftl,v $
 * $Id: interfaz.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: interfaz.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.ativarformalizacaomanutencaocontrato;

import br.com.bradesco.web.pgit.service.business.ativarformalizacaomanutencaocontrato.bean.AtivarAditivoContratoPendAssEntradaDTO;
import br.com.bradesco.web.pgit.service.business.ativarformalizacaomanutencaocontrato.bean.AtivarAditivoContratoPendAssSaidaDTO;

/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Interface do adaptador: AtivarFormalizacaoManutencaoContrato
 * </p>
 * 
 * @comment CODIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public interface IAtivarFormalizacaoManutencaoContratoService {

    /**
     * M�todo de exemplo.
     */
    void sampleAtivarFormalizacaoManutencaoContrato();
    
    /**
     * Ativar formalizacao manutencao contrato.
     *
     * @param ativarAditivoContratoPendAssEntradaDTO the ativar aditivo contrato pend ass entrada dto
     * @return the ativar aditivo contrato pend ass saida dto
     */
    AtivarAditivoContratoPendAssSaidaDTO ativarFormalizacaoManutencaoContrato(AtivarAditivoContratoPendAssEntradaDTO ativarAditivoContratoPendAssEntradaDTO);

}

