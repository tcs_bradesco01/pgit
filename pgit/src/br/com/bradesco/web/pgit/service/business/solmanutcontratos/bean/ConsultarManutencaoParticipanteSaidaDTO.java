/*
 * Nome: br.com.bradesco.web.pgit.service.business.solmanutcontratos.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.solmanutcontratos.bean;

/**
 * Nome: ConsultarManutencaoParticipanteSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarManutencaoParticipanteSaidaDTO {
	
	/** Atributo dataHoraManutencao. */
	private String dataHoraManutencao;	
	
	/** Atributo usuarioManutencao. */
	private String usuarioManutencao;	
	
	/** Atributo complementoManutencao. */
	private String complementoManutencao;
	
	/** Atributo cdCanalManutencao. */
	private int cdCanalManutencao;
	
	/** Atributo dsCanalManutencao. */
	private String dsCanalManutencao;
	
	/** Atributo dataHoraInclusao. */
	private String dataHoraInclusao;	
	
	/** Atributo usuarioInclusao. */
	private String usuarioInclusao;
	
	/** Atributo complementoInclusao. */
	private String complementoInclusao;
	
	/** Atributo cdCanalInclusao. */
	private int cdCanalInclusao;
	
	/** Atributo dsCanalInclusao. */
	private String dsCanalInclusao;
	
	/** Atributo cpfParticipante. */
	private String cpfParticipante;
	
	/** Atributo nomeParticipane. */
	private String nomeParticipane;
	
	/** Atributo dataNascimento. */
	private String dataNascimento;
	
	/** Atributo dsGrupoEconomicoParticipante. */
	private String dsGrupoEconomicoParticipante;
	
	/** Atributo usuarioSolicitante. */
	private String usuarioSolicitante;
	
	/** Atributo dataHoraSolicitacao. */
	private String dataHoraSolicitacao;	
	
	/** Atributo dsTipoParticipacaoPessoa. */
	private String dsTipoParticipacaoPessoa;
	
	/** Atributo cdSituacaoManutencaoContrato. */
	private int cdSituacaoManutencaoContrato;
	
	/** Atributo dsSituacaoManutencaoContrato. */
	private String dsSituacaoManutencaoContrato;
	
	/** Atributo cdMotivoManutencaoContrato. */
	private int cdMotivoManutencaoContrato;
	
	/** Atributo dsMotivoManutencaoContrato. */
	private String dsMotivoManutencaoContrato;
    
    /** Atributo cdIndicadorTipoManutencao. */
    private Integer cdIndicadorTipoManutencao;
    
    /** Atributo dsIndicadorTipoManutencao. */
    private String dsIndicadorTipoManutencao;
    
    /** Atributo nrAditivoContratoNegocio. */
    private Long nrAditivoContratoNegocio;
    
	/** Atributo codMensagem. */
	private String codMensagem;
	
	/** Atributo mensagem. */
	private String mensagem;
	
	/**
	 * Get: cdCanalInclusao.
	 *
	 * @return cdCanalInclusao
	 */
	public int getCdCanalInclusao() {
		return cdCanalInclusao;
	}
	
	/**
	 * Set: cdCanalInclusao.
	 *
	 * @param cdCanalInclusao the cd canal inclusao
	 */
	public void setCdCanalInclusao(int cdCanalInclusao) {
		this.cdCanalInclusao = cdCanalInclusao;
	}
	
	/**
	 * Get: cdCanalManutencao.
	 *
	 * @return cdCanalManutencao
	 */
	public int getCdCanalManutencao() {
		return cdCanalManutencao;
	}
	
	/**
	 * Set: cdCanalManutencao.
	 *
	 * @param cdCanalManutencao the cd canal manutencao
	 */
	public void setCdCanalManutencao(int cdCanalManutencao) {
		this.cdCanalManutencao = cdCanalManutencao;
	}
	
	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}
	
	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}
	
	/**
	 * Get: complementoInclusao.
	 *
	 * @return complementoInclusao
	 */
	public String getComplementoInclusao() {
		return complementoInclusao;
	}
	
	/**
	 * Set: complementoInclusao.
	 *
	 * @param complementoInclusao the complemento inclusao
	 */
	public void setComplementoInclusao(String complementoInclusao) {
		this.complementoInclusao = complementoInclusao;
	}
	
	/**
	 * Get: complementoManutencao.
	 *
	 * @return complementoManutencao
	 */
	public String getComplementoManutencao() {
		return complementoManutencao;
	}
	
	/**
	 * Set: complementoManutencao.
	 *
	 * @param complementoManutencao the complemento manutencao
	 */
	public void setComplementoManutencao(String complementoManutencao) {
		this.complementoManutencao = complementoManutencao;
	}
	
	/**
	 * Get: dataHoraInclusao.
	 *
	 * @return dataHoraInclusao
	 */
	public String getDataHoraInclusao() {
		return dataHoraInclusao;
	}
	
	/**
	 * Set: dataHoraInclusao.
	 *
	 * @param dataHoraInclusao the data hora inclusao
	 */
	public void setDataHoraInclusao(String dataHoraInclusao) {
		this.dataHoraInclusao = dataHoraInclusao;
	}
	
	/**
	 * Get: dataHoraManutencao.
	 *
	 * @return dataHoraManutencao
	 */
	public String getDataHoraManutencao() {
		return dataHoraManutencao;
	}
	
	/**
	 * Set: dataHoraManutencao.
	 *
	 * @param dataHoraManutencao the data hora manutencao
	 */
	public void setDataHoraManutencao(String dataHoraManutencao) {
		this.dataHoraManutencao = dataHoraManutencao;
	}
	
	/**
	 * Get: dsCanalInclusao.
	 *
	 * @return dsCanalInclusao
	 */
	public String getDsCanalInclusao() {
		return dsCanalInclusao;
	}
	
	/**
	 * Set: dsCanalInclusao.
	 *
	 * @param dsCanalInclusao the ds canal inclusao
	 */
	public void setDsCanalInclusao(String dsCanalInclusao) {
		this.dsCanalInclusao = dsCanalInclusao;
	}
	
	/**
	 * Get: dsCanalManutencao.
	 *
	 * @return dsCanalManutencao
	 */
	public String getDsCanalManutencao() {
		return dsCanalManutencao;
	}
	
	/**
	 * Set: dsCanalManutencao.
	 *
	 * @param dsCanalManutencao the ds canal manutencao
	 */
	public void setDsCanalManutencao(String dsCanalManutencao) {
		this.dsCanalManutencao = dsCanalManutencao;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	/**
	 * Get: usuarioInclusao.
	 *
	 * @return usuarioInclusao
	 */
	public String getUsuarioInclusao() {
		return usuarioInclusao;
	}
	
	/**
	 * Set: usuarioInclusao.
	 *
	 * @param usuarioInclusao the usuario inclusao
	 */
	public void setUsuarioInclusao(String usuarioInclusao) {
		this.usuarioInclusao = usuarioInclusao;
	}
	
	/**
	 * Get: usuarioManutencao.
	 *
	 * @return usuarioManutencao
	 */
	public String getUsuarioManutencao() {
		return usuarioManutencao;
	}
	
	/**
	 * Set: usuarioManutencao.
	 *
	 * @param usuarioManutencao the usuario manutencao
	 */
	public void setUsuarioManutencao(String usuarioManutencao) {
		this.usuarioManutencao = usuarioManutencao;
	}
	
	/**
	 * Get: cpfParticipante.
	 *
	 * @return cpfParticipante
	 */
	public String getCpfParticipante() {
		return cpfParticipante;
	}
	
	/**
	 * Set: cpfParticipante.
	 *
	 * @param cpfParticipante the cpf participante
	 */
	public void setCpfParticipante(String cpfParticipante) {
		this.cpfParticipante = cpfParticipante;
	}
	
	/**
	 * Get: dataHoraSolicitacao.
	 *
	 * @return dataHoraSolicitacao
	 */
	public String getDataHoraSolicitacao() {
		return dataHoraSolicitacao;
	}
	
	/**
	 * Set: dataHoraSolicitacao.
	 *
	 * @param dataHoraSolicitacao the data hora solicitacao
	 */
	public void setDataHoraSolicitacao(String dataHoraSolicitacao) {
		this.dataHoraSolicitacao = dataHoraSolicitacao;
	}
	
	/**
	 * Get: dataNascimento.
	 *
	 * @return dataNascimento
	 */
	public String getDataNascimento() {
		return dataNascimento;
	}
	
	/**
	 * Set: dataNascimento.
	 *
	 * @param dataNascimentoConstituicao the data nascimento
	 */
	public void setDataNascimento(String dataNascimentoConstituicao) {
		this.dataNascimento = dataNascimentoConstituicao;
	}
	
	/**
	 * Get: dsGrupoEconomicoParticipante.
	 *
	 * @return dsGrupoEconomicoParticipante
	 */
	public String getDsGrupoEconomicoParticipante() {
		return dsGrupoEconomicoParticipante;
	}
	
	/**
	 * Set: dsGrupoEconomicoParticipante.
	 *
	 * @param dsGrupoEconomicoParticipante the ds grupo economico participante
	 */
	public void setDsGrupoEconomicoParticipante(String dsGrupoEconomicoParticipante) {
		this.dsGrupoEconomicoParticipante = dsGrupoEconomicoParticipante;
	}
	
	/**
	 * Get: nomeParticipane.
	 *
	 * @return nomeParticipane
	 */
	public String getNomeParticipane() {
		return nomeParticipane;
	}
	
	/**
	 * Set: nomeParticipane.
	 *
	 * @param nomeParticipane the nome participane
	 */
	public void setNomeParticipane(String nomeParticipane) {
		this.nomeParticipane = nomeParticipane;
	}
	
	/**
	 * Get: usuarioSolicitante.
	 *
	 * @return usuarioSolicitante
	 */
	public String getUsuarioSolicitante() {
		return usuarioSolicitante;
	}
	
	/**
	 * Set: usuarioSolicitante.
	 *
	 * @param usuarioSolicitante the usuario solicitante
	 */
	public void setUsuarioSolicitante(String usuarioSolicitante) {
		this.usuarioSolicitante = usuarioSolicitante;
	}
	
	/**
	 * Get: cdMotivoManutencaoContrato.
	 *
	 * @return cdMotivoManutencaoContrato
	 */
	public int getCdMotivoManutencaoContrato() {
		return cdMotivoManutencaoContrato;
	}
	
	/**
	 * Set: cdMotivoManutencaoContrato.
	 *
	 * @param cdMotivoManutencaoContrato the cd motivo manutencao contrato
	 */
	public void setCdMotivoManutencaoContrato(int cdMotivoManutencaoContrato) {
		this.cdMotivoManutencaoContrato = cdMotivoManutencaoContrato;
	}
	
	/**
	 * Get: cdSituacaoManutencaoContrato.
	 *
	 * @return cdSituacaoManutencaoContrato
	 */
	public int getCdSituacaoManutencaoContrato() {
		return cdSituacaoManutencaoContrato;
	}
	
	/**
	 * Set: cdSituacaoManutencaoContrato.
	 *
	 * @param cdSituacaoManutencaoContrato the cd situacao manutencao contrato
	 */
	public void setCdSituacaoManutencaoContrato(int cdSituacaoManutencaoContrato) {
		this.cdSituacaoManutencaoContrato = cdSituacaoManutencaoContrato;
	}
	
	/**
	 * Get: dsMotivoManutencaoContrato.
	 *
	 * @return dsMotivoManutencaoContrato
	 */
	public String getDsMotivoManutencaoContrato() {
		return dsMotivoManutencaoContrato;
	}
	
	/**
	 * Set: dsMotivoManutencaoContrato.
	 *
	 * @param dsMotivoManutencaoContrato the ds motivo manutencao contrato
	 */
	public void setDsMotivoManutencaoContrato(String dsMotivoManutencaoContrato) {
		this.dsMotivoManutencaoContrato = dsMotivoManutencaoContrato;
	}
	
	/**
	 * Get: dsSituacaoManutencaoContrato.
	 *
	 * @return dsSituacaoManutencaoContrato
	 */
	public String getDsSituacaoManutencaoContrato() {
		return dsSituacaoManutencaoContrato;
	}
	
	/**
	 * Set: dsSituacaoManutencaoContrato.
	 *
	 * @param dsSituacaoManutencaoContrato the ds situacao manutencao contrato
	 */
	public void setDsSituacaoManutencaoContrato(String dsSituacaoManutencaoContrato) {
		this.dsSituacaoManutencaoContrato = dsSituacaoManutencaoContrato;
	}
	
	/**
	 * Get: dsTipoParticipacaoPessoa.
	 *
	 * @return dsTipoParticipacaoPessoa
	 */
	public String getDsTipoParticipacaoPessoa() {
		return dsTipoParticipacaoPessoa;
	}
	
	/**
	 * Set: dsTipoParticipacaoPessoa.
	 *
	 * @param dsTipoParticipacaoPessoa the ds tipo participacao pessoa
	 */
	public void setDsTipoParticipacaoPessoa(String dsTipoParticipacaoPessoa) {
		this.dsTipoParticipacaoPessoa = dsTipoParticipacaoPessoa;
	}
	
	/**
	 * Get: cdIndicadorTipoManutencao.
	 *
	 * @return cdIndicadorTipoManutencao
	 */
	public Integer getCdIndicadorTipoManutencao() {
		return cdIndicadorTipoManutencao;
	}
	
	/**
	 * Set: cdIndicadorTipoManutencao.
	 *
	 * @param cdIndicadorTipoManutencao the cd indicador tipo manutencao
	 */
	public void setCdIndicadorTipoManutencao(Integer cdIndicadorTipoManutencao) {
		this.cdIndicadorTipoManutencao = cdIndicadorTipoManutencao;
	}
	
	/**
	 * Get: dsIndicadorTipoManutencao.
	 *
	 * @return dsIndicadorTipoManutencao
	 */
	public String getDsIndicadorTipoManutencao() {
		return dsIndicadorTipoManutencao;
	}
	
	/**
	 * Set: dsIndicadorTipoManutencao.
	 *
	 * @param dsIndicadorTipoManutencao the ds indicador tipo manutencao
	 */
	public void setDsIndicadorTipoManutencao(String dsIndicadorTipoManutencao) {
		this.dsIndicadorTipoManutencao = dsIndicadorTipoManutencao;
	}
	
	/**
	 * Get: nrAditivoContratoNegocio.
	 *
	 * @return nrAditivoContratoNegocio
	 */
	public Long getNrAditivoContratoNegocio() {
		return nrAditivoContratoNegocio;
	}
	
	/**
	 * Set: nrAditivoContratoNegocio.
	 *
	 * @param nrAditivoContratoNegocio the nr aditivo contrato negocio
	 */
	public void setNrAditivoContratoNegocio(Long nrAditivoContratoNegocio) {
		this.nrAditivoContratoNegocio = nrAditivoContratoNegocio;
	}
}