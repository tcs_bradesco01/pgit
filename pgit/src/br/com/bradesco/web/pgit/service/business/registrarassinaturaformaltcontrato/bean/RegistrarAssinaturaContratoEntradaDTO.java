/*
 * Nome: br.com.bradesco.web.pgit.service.business.registrarassinaturaformaltcontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.registrarassinaturaformaltcontrato.bean;

/**
 * Nome: RegistrarAssinaturaContratoEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class RegistrarAssinaturaContratoEntradaDTO {
    
    /** Atributo cdPessoaJuridicaContrato. */
    private Long cdPessoaJuridicaContrato;
    
    /** Atributo cdTipoContratoNegocio. */
    private Integer cdTipoContratoNegocio;
	
	/** Atributo nrSequenciaContratoNegocio. */
	private Long nrSequenciaContratoNegocio;
	
	/** Atributo nrAditivoContratoNegocio. */
	private Long nrAditivoContratoNegocio;
	
	/** Atributo cdMotivoSituacaoAditivo. */
	private Integer cdMotivoSituacaoAditivo;
	
	/** Atributo dtAssinaturaAditivo. */
	private String dtAssinaturaAditivo;
	
	/**
	 * Get: cdMotivoSituacaoAditivo.
	 *
	 * @return cdMotivoSituacaoAditivo
	 */
	public Integer getCdMotivoSituacaoAditivo() {
		return cdMotivoSituacaoAditivo;
	}
	
	/**
	 * Set: cdMotivoSituacaoAditivo.
	 *
	 * @param cdMotivoSituacaoAditivo the cd motivo situacao aditivo
	 */
	public void setCdMotivoSituacaoAditivo(Integer cdMotivoSituacaoAditivo) {
		this.cdMotivoSituacaoAditivo = cdMotivoSituacaoAditivo;
	}
	
	/**
	 * Get: cdPessoaJuridicaContrato.
	 *
	 * @return cdPessoaJuridicaContrato
	 */
	public Long getCdPessoaJuridicaContrato() {
		return cdPessoaJuridicaContrato;
	}
	
	/**
	 * Set: cdPessoaJuridicaContrato.
	 *
	 * @param cdPessoaJuridicaContrato the cd pessoa juridica contrato
	 */
	public void setCdPessoaJuridicaContrato(Long cdPessoaJuridicaContrato) {
		this.cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
	}
	
	/**
	 * Get: cdTipoContratoNegocio.
	 *
	 * @return cdTipoContratoNegocio
	 */
	public Integer getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}
	
	/**
	 * Set: cdTipoContratoNegocio.
	 *
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}
	
	/**
	 * Get: dtAssinaturaAditivo.
	 *
	 * @return dtAssinaturaAditivo
	 */
	public String getDtAssinaturaAditivo() {
		return dtAssinaturaAditivo;
	}
	
	/**
	 * Set: dtAssinaturaAditivo.
	 *
	 * @param dtAssinaturaAditivo the dt assinatura aditivo
	 */
	public void setDtAssinaturaAditivo(String dtAssinaturaAditivo) {
		this.dtAssinaturaAditivo = dtAssinaturaAditivo;
	}
	
	/**
	 * Get: nrAditivoContratoNegocio.
	 *
	 * @return nrAditivoContratoNegocio
	 */
	public Long getNrAditivoContratoNegocio() {
		return nrAditivoContratoNegocio;
	}
	
	/**
	 * Set: nrAditivoContratoNegocio.
	 *
	 * @param nrAditivoContratoNegocio the nr aditivo contrato negocio
	 */
	public void setNrAditivoContratoNegocio(Long nrAditivoContratoNegocio) {
		this.nrAditivoContratoNegocio = nrAditivoContratoNegocio;
	}
	
	/**
	 * Get: nrSequenciaContratoNegocio.
	 *
	 * @return nrSequenciaContratoNegocio
	 */
	public Long getNrSequenciaContratoNegocio() {
		return nrSequenciaContratoNegocio;
	}
	
	/**
	 * Set: nrSequenciaContratoNegocio.
	 *
	 * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
	 */
	public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}
	
	
}
