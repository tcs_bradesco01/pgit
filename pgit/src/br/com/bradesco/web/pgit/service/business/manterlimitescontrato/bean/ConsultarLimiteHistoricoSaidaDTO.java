package br.com.bradesco.web.pgit.service.business.manterlimitescontrato.bean;

import java.util.List;

/**
 * Arquivo criado em 22/09/15.
 */
public class ConsultarLimiteHistoricoSaidaDTO {

    private String codMensagem;

    private String mensagem;

    private Integer numeroConsulta;

    private List<ConsultarLimiteHistoricoOcorrenciasSaidaDTO> ocorrencias;

    public void setCodMensagem(String codMensagem) {
        this.codMensagem = codMensagem;
    }

    public String getCodMensagem() {
        return this.codMensagem;
    }

    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }

    public String getMensagem() {
        return this.mensagem;
    }

    public void setNumeroConsulta(Integer numeroConsulta) {
        this.numeroConsulta = numeroConsulta;
    }

    public Integer getNumeroConsulta() {
        return this.numeroConsulta;
    }

    public void setOcorrencias(List<ConsultarLimiteHistoricoOcorrenciasSaidaDTO> ocorrencias) {
        this.ocorrencias = ocorrencias;
    }

    public List<ConsultarLimiteHistoricoOcorrenciasSaidaDTO> getOcorrencias() {
        return this.ocorrencias;
    }
}