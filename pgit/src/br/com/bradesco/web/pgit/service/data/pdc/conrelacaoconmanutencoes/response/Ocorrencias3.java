/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.conrelacaoconmanutencoes.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias3.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias3 implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdPerfilTrocaArquivo
     */
    private long _cdPerfilTrocaArquivo = 0;

    /**
     * keeps track of state for field: _cdPerfilTrocaArquivo
     */
    private boolean _has_cdPerfilTrocaArquivo;

    /**
     * Field _dsLayoutProprio
     */
    private java.lang.String _dsLayoutProprio;

    /**
     * Field _dsCodigoTipoLayout
     */
    private java.lang.String _dsCodigoTipoLayout;

    /**
     * Field _dsProdutoServicoRelacionadoPerfil
     */
    private java.lang.String _dsProdutoServicoRelacionadoPerfil;

    /**
     * Field _dsAplicacaoFormato
     */
    private java.lang.String _dsAplicacaoFormato;

    /**
     * Field _dsMeioPrincipalRemessa
     */
    private java.lang.String _dsMeioPrincipalRemessa;

    /**
     * Field _dsMeioAlternadoRemessa
     */
    private java.lang.String _dsMeioAlternadoRemessa;

    /**
     * Field _dsMeioPrincipalRetorno
     */
    private java.lang.String _dsMeioPrincipalRetorno;

    /**
     * Field _dsMeioAlternadoRetorno
     */
    private java.lang.String _dsMeioAlternadoRetorno;

    /**
     * Field _dsEmpresa
     */
    private java.lang.String _dsEmpresa;

    /**
     * Field _dsAcaoManutencaoPerfil
     */
    private java.lang.String _dsAcaoManutencaoPerfil;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias3() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.conrelacaoconmanutencoes.response.Ocorrencias3()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdPerfilTrocaArquivo
     * 
     */
    public void deleteCdPerfilTrocaArquivo()
    {
        this._has_cdPerfilTrocaArquivo= false;
    } //-- void deleteCdPerfilTrocaArquivo() 

    /**
     * Returns the value of field 'cdPerfilTrocaArquivo'.
     * 
     * @return long
     * @return the value of field 'cdPerfilTrocaArquivo'.
     */
    public long getCdPerfilTrocaArquivo()
    {
        return this._cdPerfilTrocaArquivo;
    } //-- long getCdPerfilTrocaArquivo() 

    /**
     * Returns the value of field 'dsAcaoManutencaoPerfil'.
     * 
     * @return String
     * @return the value of field 'dsAcaoManutencaoPerfil'.
     */
    public java.lang.String getDsAcaoManutencaoPerfil()
    {
        return this._dsAcaoManutencaoPerfil;
    } //-- java.lang.String getDsAcaoManutencaoPerfil() 

    /**
     * Returns the value of field 'dsAplicacaoFormato'.
     * 
     * @return String
     * @return the value of field 'dsAplicacaoFormato'.
     */
    public java.lang.String getDsAplicacaoFormato()
    {
        return this._dsAplicacaoFormato;
    } //-- java.lang.String getDsAplicacaoFormato() 

    /**
     * Returns the value of field 'dsCodigoTipoLayout'.
     * 
     * @return String
     * @return the value of field 'dsCodigoTipoLayout'.
     */
    public java.lang.String getDsCodigoTipoLayout()
    {
        return this._dsCodigoTipoLayout;
    } //-- java.lang.String getDsCodigoTipoLayout() 

    /**
     * Returns the value of field 'dsEmpresa'.
     * 
     * @return String
     * @return the value of field 'dsEmpresa'.
     */
    public java.lang.String getDsEmpresa()
    {
        return this._dsEmpresa;
    } //-- java.lang.String getDsEmpresa() 

    /**
     * Returns the value of field 'dsLayoutProprio'.
     * 
     * @return String
     * @return the value of field 'dsLayoutProprio'.
     */
    public java.lang.String getDsLayoutProprio()
    {
        return this._dsLayoutProprio;
    } //-- java.lang.String getDsLayoutProprio() 

    /**
     * Returns the value of field 'dsMeioAlternadoRemessa'.
     * 
     * @return String
     * @return the value of field 'dsMeioAlternadoRemessa'.
     */
    public java.lang.String getDsMeioAlternadoRemessa()
    {
        return this._dsMeioAlternadoRemessa;
    } //-- java.lang.String getDsMeioAlternadoRemessa() 

    /**
     * Returns the value of field 'dsMeioAlternadoRetorno'.
     * 
     * @return String
     * @return the value of field 'dsMeioAlternadoRetorno'.
     */
    public java.lang.String getDsMeioAlternadoRetorno()
    {
        return this._dsMeioAlternadoRetorno;
    } //-- java.lang.String getDsMeioAlternadoRetorno() 

    /**
     * Returns the value of field 'dsMeioPrincipalRemessa'.
     * 
     * @return String
     * @return the value of field 'dsMeioPrincipalRemessa'.
     */
    public java.lang.String getDsMeioPrincipalRemessa()
    {
        return this._dsMeioPrincipalRemessa;
    } //-- java.lang.String getDsMeioPrincipalRemessa() 

    /**
     * Returns the value of field 'dsMeioPrincipalRetorno'.
     * 
     * @return String
     * @return the value of field 'dsMeioPrincipalRetorno'.
     */
    public java.lang.String getDsMeioPrincipalRetorno()
    {
        return this._dsMeioPrincipalRetorno;
    } //-- java.lang.String getDsMeioPrincipalRetorno() 

    /**
     * Returns the value of field
     * 'dsProdutoServicoRelacionadoPerfil'.
     * 
     * @return String
     * @return the value of field
     * 'dsProdutoServicoRelacionadoPerfil'.
     */
    public java.lang.String getDsProdutoServicoRelacionadoPerfil()
    {
        return this._dsProdutoServicoRelacionadoPerfil;
    } //-- java.lang.String getDsProdutoServicoRelacionadoPerfil() 

    /**
     * Method hasCdPerfilTrocaArquivo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPerfilTrocaArquivo()
    {
        return this._has_cdPerfilTrocaArquivo;
    } //-- boolean hasCdPerfilTrocaArquivo() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdPerfilTrocaArquivo'.
     * 
     * @param cdPerfilTrocaArquivo the value of field
     * 'cdPerfilTrocaArquivo'.
     */
    public void setCdPerfilTrocaArquivo(long cdPerfilTrocaArquivo)
    {
        this._cdPerfilTrocaArquivo = cdPerfilTrocaArquivo;
        this._has_cdPerfilTrocaArquivo = true;
    } //-- void setCdPerfilTrocaArquivo(long) 

    /**
     * Sets the value of field 'dsAcaoManutencaoPerfil'.
     * 
     * @param dsAcaoManutencaoPerfil the value of field
     * 'dsAcaoManutencaoPerfil'.
     */
    public void setDsAcaoManutencaoPerfil(java.lang.String dsAcaoManutencaoPerfil)
    {
        this._dsAcaoManutencaoPerfil = dsAcaoManutencaoPerfil;
    } //-- void setDsAcaoManutencaoPerfil(java.lang.String) 

    /**
     * Sets the value of field 'dsAplicacaoFormato'.
     * 
     * @param dsAplicacaoFormato the value of field
     * 'dsAplicacaoFormato'.
     */
    public void setDsAplicacaoFormato(java.lang.String dsAplicacaoFormato)
    {
        this._dsAplicacaoFormato = dsAplicacaoFormato;
    } //-- void setDsAplicacaoFormato(java.lang.String) 

    /**
     * Sets the value of field 'dsCodigoTipoLayout'.
     * 
     * @param dsCodigoTipoLayout the value of field
     * 'dsCodigoTipoLayout'.
     */
    public void setDsCodigoTipoLayout(java.lang.String dsCodigoTipoLayout)
    {
        this._dsCodigoTipoLayout = dsCodigoTipoLayout;
    } //-- void setDsCodigoTipoLayout(java.lang.String) 

    /**
     * Sets the value of field 'dsEmpresa'.
     * 
     * @param dsEmpresa the value of field 'dsEmpresa'.
     */
    public void setDsEmpresa(java.lang.String dsEmpresa)
    {
        this._dsEmpresa = dsEmpresa;
    } //-- void setDsEmpresa(java.lang.String) 

    /**
     * Sets the value of field 'dsLayoutProprio'.
     * 
     * @param dsLayoutProprio the value of field 'dsLayoutProprio'.
     */
    public void setDsLayoutProprio(java.lang.String dsLayoutProprio)
    {
        this._dsLayoutProprio = dsLayoutProprio;
    } //-- void setDsLayoutProprio(java.lang.String) 

    /**
     * Sets the value of field 'dsMeioAlternadoRemessa'.
     * 
     * @param dsMeioAlternadoRemessa the value of field
     * 'dsMeioAlternadoRemessa'.
     */
    public void setDsMeioAlternadoRemessa(java.lang.String dsMeioAlternadoRemessa)
    {
        this._dsMeioAlternadoRemessa = dsMeioAlternadoRemessa;
    } //-- void setDsMeioAlternadoRemessa(java.lang.String) 

    /**
     * Sets the value of field 'dsMeioAlternadoRetorno'.
     * 
     * @param dsMeioAlternadoRetorno the value of field
     * 'dsMeioAlternadoRetorno'.
     */
    public void setDsMeioAlternadoRetorno(java.lang.String dsMeioAlternadoRetorno)
    {
        this._dsMeioAlternadoRetorno = dsMeioAlternadoRetorno;
    } //-- void setDsMeioAlternadoRetorno(java.lang.String) 

    /**
     * Sets the value of field 'dsMeioPrincipalRemessa'.
     * 
     * @param dsMeioPrincipalRemessa the value of field
     * 'dsMeioPrincipalRemessa'.
     */
    public void setDsMeioPrincipalRemessa(java.lang.String dsMeioPrincipalRemessa)
    {
        this._dsMeioPrincipalRemessa = dsMeioPrincipalRemessa;
    } //-- void setDsMeioPrincipalRemessa(java.lang.String) 

    /**
     * Sets the value of field 'dsMeioPrincipalRetorno'.
     * 
     * @param dsMeioPrincipalRetorno the value of field
     * 'dsMeioPrincipalRetorno'.
     */
    public void setDsMeioPrincipalRetorno(java.lang.String dsMeioPrincipalRetorno)
    {
        this._dsMeioPrincipalRetorno = dsMeioPrincipalRetorno;
    } //-- void setDsMeioPrincipalRetorno(java.lang.String) 

    /**
     * Sets the value of field 'dsProdutoServicoRelacionadoPerfil'.
     * 
     * @param dsProdutoServicoRelacionadoPerfil the value of field
     * 'dsProdutoServicoRelacionadoPerfil'.
     */
    public void setDsProdutoServicoRelacionadoPerfil(java.lang.String dsProdutoServicoRelacionadoPerfil)
    {
        this._dsProdutoServicoRelacionadoPerfil = dsProdutoServicoRelacionadoPerfil;
    } //-- void setDsProdutoServicoRelacionadoPerfil(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias3
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.conrelacaoconmanutencoes.response.Ocorrencias3 unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.conrelacaoconmanutencoes.response.Ocorrencias3) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.conrelacaoconmanutencoes.response.Ocorrencias3.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.conrelacaoconmanutencoes.response.Ocorrencias3 unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
