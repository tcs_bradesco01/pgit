/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantercontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantercontrato.bean;

import br.com.bradesco.web.pgit.utils.CpfCnpjUtils;

/**
 * Nome: ListarParticipanteAgregadoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarParticipanteAgregadoSaidaDTO {

	/** Atributo cdCorpoCpfCnpj. */
	private Long cdCorpoCpfCnpj;
	
	/** Atributo cdControleCpfCnpj. */
	private Integer cdControleCpfCnpj;
	
	/** Atributo cdFilialCnpj. */
	private Integer cdFilialCnpj;
	
	/** Atributo dsNomeParticipante. */
	private String dsNomeParticipante;
	
	/** Atributo dsSacadoEletronico. */
	private String dsSacadoEletronico;

	/**
	 * Listar participante agregado saida dto.
	 *
	 * @param cdCorpoCpfCnpj the cd corpo cpf cnpj
	 * @param cdControleCpfCnpj the cd controle cpf cnpj
	 * @param cdFilialCnpj the cd filial cnpj
	 * @param dsNomeParticipante the ds nome participante
	 * @param dsSacadoEletronico the ds sacado eletronico
	 */
	public ListarParticipanteAgregadoSaidaDTO(Long cdCorpoCpfCnpj,
			Integer cdControleCpfCnpj, Integer cdFilialCnpj,
			String dsNomeParticipante, String dsSacadoEletronico) {
		super();
		this.cdCorpoCpfCnpj = cdCorpoCpfCnpj;
		this.cdControleCpfCnpj = cdControleCpfCnpj;
		this.cdFilialCnpj = cdFilialCnpj;
		this.dsNomeParticipante = dsNomeParticipante;
		this.dsSacadoEletronico = dsSacadoEletronico;
	}

	/**
	 * Get: cpfCnpjFormatado.
	 *
	 * @return cpfCnpjFormatado
	 */
	public String getCpfCnpjFormatado() {
		return CpfCnpjUtils.formatarCpfCnpj(cdCorpoCpfCnpj, cdFilialCnpj, cdControleCpfCnpj);
	}

	/**
	 * Get: cdCorpoCpfCnpj.
	 *
	 * @return cdCorpoCpfCnpj
	 */
	public Long getCdCorpoCpfCnpj() {
		return cdCorpoCpfCnpj;
	}
	
	/**
	 * Set: cdCorpoCpfCnpj.
	 *
	 * @param cdCorpoCpfCnpj the cd corpo cpf cnpj
	 */
	public void setCdCorpoCpfCnpj(Long cdCorpoCpfCnpj) {
		this.cdCorpoCpfCnpj = cdCorpoCpfCnpj;
	}
	
	/**
	 * Get: cdControleCpfCnpj.
	 *
	 * @return cdControleCpfCnpj
	 */
	public Integer getCdControleCpfCnpj() {
		return cdControleCpfCnpj;
	}
	
	/**
	 * Set: cdControleCpfCnpj.
	 *
	 * @param cdControleCpfCnpj the cd controle cpf cnpj
	 */
	public void setCdControleCpfCnpj(Integer cdControleCpfCnpj) {
		this.cdControleCpfCnpj = cdControleCpfCnpj;
	}
	
	/**
	 * Get: cdFilialCnpj.
	 *
	 * @return cdFilialCnpj
	 */
	public Integer getCdFilialCnpj() {
		return cdFilialCnpj;
	}
	
	/**
	 * Set: cdFilialCnpj.
	 *
	 * @param cdFilialCnpj the cd filial cnpj
	 */
	public void setCdFilialCnpj(Integer cdFilialCnpj) {
		this.cdFilialCnpj = cdFilialCnpj;
	}
	
	/**
	 * Get: dsNomeParticipante.
	 *
	 * @return dsNomeParticipante
	 */
	public String getDsNomeParticipante() {
		return dsNomeParticipante;
	}
	
	/**
	 * Set: dsNomeParticipante.
	 *
	 * @param dsNomeParticipante the ds nome participante
	 */
	public void setDsNomeParticipante(String dsNomeParticipante) {
		this.dsNomeParticipante = dsNomeParticipante;
	}
	
	/**
	 * Get: dsSacadoEletronico.
	 *
	 * @return dsSacadoEletronico
	 */
	public String getDsSacadoEletronico() {
		return dsSacadoEletronico;
	}
	
	/**
	 * Set: dsSacadoEletronico.
	 *
	 * @param dsSacadoEletronico the ds sacado eletronico
	 */
	public void setDsSacadoEletronico(String dsSacadoEletronico) {
		this.dsSacadoEletronico = dsSacadoEletronico;
	}
}