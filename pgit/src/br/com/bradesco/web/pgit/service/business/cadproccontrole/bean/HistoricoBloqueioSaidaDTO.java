/*
 * Nome: br.com.bradesco.web.pgit.service.business.cadproccontrole.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.cadproccontrole.bean;

import java.io.Serializable;

/**
 * Nome: HistoricoBloqueioSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class HistoricoBloqueioSaidaDTO implements Serializable {

	/** Atributo serialVersionUID. */
	private static final long serialVersionUID = -6926814825809487001L;

	/** Atributo hrBloqueioProcessoSistema. */
	private String hrBloqueioProcessoSistema;

    /** Atributo cdIndicadorTipoManutencao. */
    private Integer cdIndicadorTipoManutencao;

    /** Atributo dsIndicadorTipoManutencao. */
    private String dsIndicadorTipoManutencao;

    /** Atributo cdUsuarioInclusao. */
    private String cdUsuarioInclusao;

    /** Atributo dsUsuarioInclusao. */
    private String dsUsuarioInclusao;

    /** Atributo cdSistema. */
    private String cdSistema;

    /** Atributo dsCodigoSistema. */
    private String dsCodigoSistema;

    /** Atributo cdProcessoSistema. */
    private Integer cdProcessoSistema;

    /** Atributo cdTipoProcessoSistema. */
    private Integer cdTipoProcessoSistema;

    /** Atributo dsCodigoTipoProcessoSistema. */
    private String dsCodigoTipoProcessoSistema;

    /** Atributo cdPeriodicidade. */
    private Integer cdPeriodicidade;

    /** Atributo dsCodigoPeriodicidade. */
    private String dsCodigoPeriodicidade;

    /** Atributo dsProcessoSistema. */
    private String dsProcessoSistema;

    /** Atributo cdSituacaoProcessoSistema. */
    private Integer cdSituacaoProcessoSistema;

    /** Atributo cdNetProcessamentoPgto. */
    private String cdNetProcessamentoPgto;

    /** Atributo cdobProcessamentoPgto. */
    private String cdobProcessamentoPgto;

    /** Atributo dsBloqueioProcessoSistema. */
    private String dsBloqueioProcessoSistema;

    /** Atributo cdCanalInclusao. */
    private Integer cdCanalInclusao;
    
    /** Atributo dsCanalInclusao. */
    private String dsCanalInclusao;

    /** Atributo cdAutenSegregacaoInclusao. */
    private String cdAutenSegregacaoInclusao;

    /** Atributo nmOperacaoFluxoInclusao. */
    private String nmOperacaoFluxoInclusao;

    /** Atributo hrInclusaoRegistro. */
    private String hrInclusaoRegistro;

    /** Atributo cdCanalManutencao. */
    private Integer cdCanalManutencao;

    /** Atributo dsCanalManutencao. */
    private String dsCanalManutencao;

    /** Atributo cdAutenSegregacaoManutencao. */
    private String cdAutenSegregacaoManutencao;

    /** Atributo nmOperacaoFluxoManutencao. */
    private String nmOperacaoFluxoManutencao;

    /** Atributo hrInclusaoManutencao. */
    private String hrInclusaoManutencao;
    
    /** Atributo dsSistemaProcesso. */
    private String dsSistemaProcesso;

	/**
	 * Get: dsSistemaProcesso.
	 *
	 * @return dsSistemaProcesso
	 */
	public String getDsSistemaProcesso() {
		return dsSistemaProcesso;
	}

	/**
	 * Set: dsSistemaProcesso.
	 *
	 * @param dsSistemaProcesso the ds sistema processo
	 */
	public void setDsSistemaProcesso(String dsSistemaProcesso) {
		this.dsSistemaProcesso = dsSistemaProcesso;
	}

	/**
	 * Get: cdIndicadorTipoManutencao.
	 *
	 * @return cdIndicadorTipoManutencao
	 */
	public Integer getCdIndicadorTipoManutencao() {
		return cdIndicadorTipoManutencao;
	}

	/**
	 * Set: cdIndicadorTipoManutencao.
	 *
	 * @param cdIndicadorTipoManutencao the cd indicador tipo manutencao
	 */
	public void setCdIndicadorTipoManutencao(Integer cdIndicadorTipoManutencao) {
		this.cdIndicadorTipoManutencao = cdIndicadorTipoManutencao;
	}

	/**
	 * Get: cdUsuarioInclusao.
	 *
	 * @return cdUsuarioInclusao
	 */
	public String getCdUsuarioInclusao() {
		return cdUsuarioInclusao;
	}

	/**
	 * Set: cdUsuarioInclusao.
	 *
	 * @param cdUsuarioInclusao the cd usuario inclusao
	 */
	public void setCdUsuarioInclusao(String cdUsuarioInclusao) {
		this.cdUsuarioInclusao = cdUsuarioInclusao;
	}

	/**
	 * Get: dsIndicadorTipoManutencao.
	 *
	 * @return dsIndicadorTipoManutencao
	 */
	public String getDsIndicadorTipoManutencao() {
		return dsIndicadorTipoManutencao;
	}

	/**
	 * Set: dsIndicadorTipoManutencao.
	 *
	 * @param dsIndicadorTipoManutencao the ds indicador tipo manutencao
	 */
	public void setDsIndicadorTipoManutencao(String dsIndicadorTipoManutencao) {
		this.dsIndicadorTipoManutencao = dsIndicadorTipoManutencao;
	}

	/**
	 * Get: dsUsuarioInclusao.
	 *
	 * @return dsUsuarioInclusao
	 */
	public String getDsUsuarioInclusao() {
		return dsUsuarioInclusao;
	}

	/**
	 * Set: dsUsuarioInclusao.
	 *
	 * @param dsUsuarioInclusao the ds usuario inclusao
	 */
	public void setDsUsuarioInclusao(String dsUsuarioInclusao) {
		this.dsUsuarioInclusao = dsUsuarioInclusao;
	}

	/**
	 * Get: hrBloqueioProcessoSistema.
	 *
	 * @return hrBloqueioProcessoSistema
	 */
	public String getHrBloqueioProcessoSistema() {
		return hrBloqueioProcessoSistema;
	}

	/**
	 * Set: hrBloqueioProcessoSistema.
	 *
	 * @param hrBloqueioProcessoSistema the hr bloqueio processo sistema
	 */
	public void setHrBloqueioProcessoSistema(String hrBloqueioProcessoSistema) {
		this.hrBloqueioProcessoSistema = hrBloqueioProcessoSistema;
	}

	/**
	 * Get: cdAutenSegregacaoInclusao.
	 *
	 * @return cdAutenSegregacaoInclusao
	 */
	public String getCdAutenSegregacaoInclusao() {
		return cdAutenSegregacaoInclusao;
	}

	/**
	 * Set: cdAutenSegregacaoInclusao.
	 *
	 * @param cdAutenSegregacaoInclusao the cd auten segregacao inclusao
	 */
	public void setCdAutenSegregacaoInclusao(String cdAutenSegregacaoInclusao) {
		this.cdAutenSegregacaoInclusao = cdAutenSegregacaoInclusao;
	}

	/**
	 * Get: cdAutenSegregacaoManutencao.
	 *
	 * @return cdAutenSegregacaoManutencao
	 */
	public String getCdAutenSegregacaoManutencao() {
		return cdAutenSegregacaoManutencao;
	}

	/**
	 * Set: cdAutenSegregacaoManutencao.
	 *
	 * @param cdAutenSegregacaoManutencao the cd auten segregacao manutencao
	 */
	public void setCdAutenSegregacaoManutencao(String cdAutenSegregacaoManutencao) {
		this.cdAutenSegregacaoManutencao = cdAutenSegregacaoManutencao;
	}

	/**
	 * Get: cdCanalInclusao.
	 *
	 * @return cdCanalInclusao
	 */
	public Integer getCdCanalInclusao() {
		return cdCanalInclusao;
	}

	/**
	 * Set: cdCanalInclusao.
	 *
	 * @param cdCanalInclusao the cd canal inclusao
	 */
	public void setCdCanalInclusao(Integer cdCanalInclusao) {
		this.cdCanalInclusao = cdCanalInclusao;
	}

	/**
	 * Get: cdCanalManutencao.
	 *
	 * @return cdCanalManutencao
	 */
	public Integer getCdCanalManutencao() {
		return cdCanalManutencao;
	}

	/**
	 * Set: cdCanalManutencao.
	 *
	 * @param cdCanalManutencao the cd canal manutencao
	 */
	public void setCdCanalManutencao(Integer cdCanalManutencao) {
		this.cdCanalManutencao = cdCanalManutencao;
	}

	/**
	 * Get: cdNetProcessamentoPgto.
	 *
	 * @return cdNetProcessamentoPgto
	 */
	public String getCdNetProcessamentoPgto() {
		return cdNetProcessamentoPgto;
	}

	/**
	 * Set: cdNetProcessamentoPgto.
	 *
	 * @param cdNetProcessamentoPgto the cd net processamento pgto
	 */
	public void setCdNetProcessamentoPgto(String cdNetProcessamentoPgto) {
		this.cdNetProcessamentoPgto = cdNetProcessamentoPgto;
	}

	/**
	 * Get: cdobProcessamentoPgto.
	 *
	 * @return cdobProcessamentoPgto
	 */
	public String getCdobProcessamentoPgto() {
		return cdobProcessamentoPgto;
	}

	/**
	 * Set: cdobProcessamentoPgto.
	 *
	 * @param cdobProcessamentoPgto the cdob processamento pgto
	 */
	public void setCdobProcessamentoPgto(String cdobProcessamentoPgto) {
		this.cdobProcessamentoPgto = cdobProcessamentoPgto;
	}

	/**
	 * Get: cdPeriodicidade.
	 *
	 * @return cdPeriodicidade
	 */
	public Integer getCdPeriodicidade() {
		return cdPeriodicidade;
	}

	/**
	 * Set: cdPeriodicidade.
	 *
	 * @param cdPeriodicidade the cd periodicidade
	 */
	public void setCdPeriodicidade(Integer cdPeriodicidade) {
		this.cdPeriodicidade = cdPeriodicidade;
	}

	/**
	 * Get: cdProcessoSistema.
	 *
	 * @return cdProcessoSistema
	 */
	public Integer getCdProcessoSistema() {
		return cdProcessoSistema;
	}

	/**
	 * Set: cdProcessoSistema.
	 *
	 * @param cdProcessoSistema the cd processo sistema
	 */
	public void setCdProcessoSistema(Integer cdProcessoSistema) {
		this.cdProcessoSistema = cdProcessoSistema;
	}

	/**
	 * Get: cdSistema.
	 *
	 * @return cdSistema
	 */
	public String getCdSistema() {
		return cdSistema;
	}

	/**
	 * Set: cdSistema.
	 *
	 * @param cdSistema the cd sistema
	 */
	public void setCdSistema(String cdSistema) {
		this.cdSistema = cdSistema;
	}

	/**
	 * Get: cdSituacaoProcessoSistema.
	 *
	 * @return cdSituacaoProcessoSistema
	 */
	public Integer getCdSituacaoProcessoSistema() {
		return cdSituacaoProcessoSistema;
	}

	/**
	 * Set: cdSituacaoProcessoSistema.
	 *
	 * @param cdSituacaoProcessoSistema the cd situacao processo sistema
	 */
	public void setCdSituacaoProcessoSistema(Integer cdSituacaoProcessoSistema) {
		this.cdSituacaoProcessoSistema = cdSituacaoProcessoSistema;
	}

	/**
	 * Get: cdTipoProcessoSistema.
	 *
	 * @return cdTipoProcessoSistema
	 */
	public Integer getCdTipoProcessoSistema() {
		return cdTipoProcessoSistema;
	}

	/**
	 * Set: cdTipoProcessoSistema.
	 *
	 * @param cdTipoProcessoSistema the cd tipo processo sistema
	 */
	public void setCdTipoProcessoSistema(Integer cdTipoProcessoSistema) {
		this.cdTipoProcessoSistema = cdTipoProcessoSistema;
	}

	/**
	 * Get: dsBloqueioProcessoSistema.
	 *
	 * @return dsBloqueioProcessoSistema
	 */
	public String getDsBloqueioProcessoSistema() {
		return dsBloqueioProcessoSistema;
	}

	/**
	 * Set: dsBloqueioProcessoSistema.
	 *
	 * @param dsBloqueioProcessoSistema the ds bloqueio processo sistema
	 */
	public void setDsBloqueioProcessoSistema(String dsBloqueioProcessoSistema) {
		this.dsBloqueioProcessoSistema = dsBloqueioProcessoSistema;
	}

	/**
	 * Get: dsCodigoPeriodicidade.
	 *
	 * @return dsCodigoPeriodicidade
	 */
	public String getDsCodigoPeriodicidade() {
		return dsCodigoPeriodicidade;
	}

	/**
	 * Set: dsCodigoPeriodicidade.
	 *
	 * @param dsCodigoPeriodicidade the ds codigo periodicidade
	 */
	public void setDsCodigoPeriodicidade(String dsCodigoPeriodicidade) {
		this.dsCodigoPeriodicidade = dsCodigoPeriodicidade;
	}

	/**
	 * Get: dsCodigoSistema.
	 *
	 * @return dsCodigoSistema
	 */
	public String getDsCodigoSistema() {
		return dsCodigoSistema;
	}

	/**
	 * Set: dsCodigoSistema.
	 *
	 * @param dsCodigoSistema the ds codigo sistema
	 */
	public void setDsCodigoSistema(String dsCodigoSistema) {
		this.dsCodigoSistema = dsCodigoSistema;
	}

	/**
	 * Get: dsCodigoTipoProcessoSistema.
	 *
	 * @return dsCodigoTipoProcessoSistema
	 */
	public String getDsCodigoTipoProcessoSistema() {
		return dsCodigoTipoProcessoSistema;
	}

	/**
	 * Set: dsCodigoTipoProcessoSistema.
	 *
	 * @param dsCodigoTipoProcessoSistema the ds codigo tipo processo sistema
	 */
	public void setDsCodigoTipoProcessoSistema(String dsCodigoTipoProcessoSistema) {
		this.dsCodigoTipoProcessoSistema = dsCodigoTipoProcessoSistema;
	}

	/**
	 * Get: dsProcessoSistema.
	 *
	 * @return dsProcessoSistema
	 */
	public String getDsProcessoSistema() {
		return dsProcessoSistema;
	}

	/**
	 * Set: dsProcessoSistema.
	 *
	 * @param dsProcessoSistema the ds processo sistema
	 */
	public void setDsProcessoSistema(String dsProcessoSistema) {
		this.dsProcessoSistema = dsProcessoSistema;
	}

	/**
	 * Get: hrInclusaoManutencao.
	 *
	 * @return hrInclusaoManutencao
	 */
	public String getHrInclusaoManutencao() {
		return hrInclusaoManutencao;
	}

	/**
	 * Set: hrInclusaoManutencao.
	 *
	 * @param hrInclusaoManutencao the hr inclusao manutencao
	 */
	public void setHrInclusaoManutencao(String hrInclusaoManutencao) {
		this.hrInclusaoManutencao = hrInclusaoManutencao;
	}

	/**
	 * Get: hrInclusaoRegistro.
	 *
	 * @return hrInclusaoRegistro
	 */
	public String getHrInclusaoRegistro() {
		return hrInclusaoRegistro;
	}

	/**
	 * Set: hrInclusaoRegistro.
	 *
	 * @param hrInclusaoRegistro the hr inclusao registro
	 */
	public void setHrInclusaoRegistro(String hrInclusaoRegistro) {
		this.hrInclusaoRegistro = hrInclusaoRegistro;
	}

	/**
	 * Get: nmOperacaoFluxoInclusao.
	 *
	 * @return nmOperacaoFluxoInclusao
	 */
	public String getNmOperacaoFluxoInclusao() {
		return nmOperacaoFluxoInclusao;
	}

	/**
	 * Set: nmOperacaoFluxoInclusao.
	 *
	 * @param nmOperacaoFluxoInclusao the nm operacao fluxo inclusao
	 */
	public void setNmOperacaoFluxoInclusao(String nmOperacaoFluxoInclusao) {
		this.nmOperacaoFluxoInclusao = nmOperacaoFluxoInclusao;
	}

	/**
	 * Get: nmOperacaoFluxoManutencao.
	 *
	 * @return nmOperacaoFluxoManutencao
	 */
	public String getNmOperacaoFluxoManutencao() {
		return nmOperacaoFluxoManutencao;
	}

	/**
	 * Set: nmOperacaoFluxoManutencao.
	 *
	 * @param nmOperacaoFluxoManutencao the nm operacao fluxo manutencao
	 */
	public void setNmOperacaoFluxoManutencao(String nmOperacaoFluxoManutencao) {
		this.nmOperacaoFluxoManutencao = nmOperacaoFluxoManutencao;
	}

	/**
	 * Get: dsCanalInclusao.
	 *
	 * @return dsCanalInclusao
	 */
	public String getDsCanalInclusao() {
		return dsCanalInclusao;
	}

	/**
	 * Set: dsCanalInclusao.
	 *
	 * @param dsCanalInclusao the ds canal inclusao
	 */
	public void setDsCanalInclusao(String dsCanalInclusao) {
		this.dsCanalInclusao = dsCanalInclusao;
	}

	/**
	 * Get: dsCanalManutencao.
	 *
	 * @return dsCanalManutencao
	 */
	public String getDsCanalManutencao() {
		return dsCanalManutencao;
	}

	/**
	 * Set: dsCanalManutencao.
	 *
	 * @param dsCanalManutencao the ds canal manutencao
	 */
	public void setDsCanalManutencao(String dsCanalManutencao) {
		this.dsCanalManutencao = dsCanalManutencao;
	}

	/**
	 * Get: canalInclusao.
	 *
	 * @return canalInclusao
	 */
	public String getCanalInclusao() {
		if (getCdCanalInclusao() != null && getCdCanalInclusao() != 0) {
			return getCdCanalInclusao() + " - " + getDsCanalInclusao();
		}
		return "";
	}
	
	/**
	 * Get: canalManutencao.
	 *
	 * @return canalManutencao
	 */
	public String getCanalManutencao() {
		if (getCdCanalManutencao() != null && getCdCanalManutencao() != 0) {
			return getCdCanalManutencao() + " - " + getDsCanalManutencao();
		}
		return "";
	}
}
