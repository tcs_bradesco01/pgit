/*
 * Nome: br.com.bradesco.web.pgit.service.business.solrecpagvennaopagos.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.solrecpagvennaopagos.bean;

import java.util.ArrayList;
import java.util.List;

/**
 * Nome: SolicitarRecPagtosVencidosNaoPagosEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class SolicitarRecPagtosVencidosNaoPagosEntradaDTO {
	
	/** Atributo dtQuitacao. */
	private String dtQuitacao;
	
	/** Atributo nrPagamento. */
	private Integer nrPagamento;
	
	/** Atributo cdSolicitacaoPagamento. */
	private Integer cdSolicitacaoPagamento;
	
	/** Atributo nrSolicitacaoPagamento. */
	private Integer nrSolicitacaoPagamento;
	
	/** Atributo flagPrimeiraSolicitacao. */
	private String flagPrimeiraSolicitacao;
	
	/** Atributo cdProduto. */
	private Integer cdProduto;
	
	/** Atributo cdProdutoRelacionado. */
	private Integer cdProdutoRelacionado;	 
 
	/** Atributo ocorrencias. */
	private List<OcorrenciasDTO> ocorrencias = new ArrayList<OcorrenciasDTO>();
	
	/**
	 * Get: dtQuitacao.
	 *
	 * @return dtQuitacao
	 */
	public String getDtQuitacao() {
		return dtQuitacao;
	}
	
	/**
	 * Set: dtQuitacao.
	 *
	 * @param dtQuitacao the dt quitacao
	 */
	public void setDtQuitacao(String dtQuitacao) {
		this.dtQuitacao = dtQuitacao;
	}
	
	/**
	 * Get: nrPagamento.
	 *
	 * @return nrPagamento
	 */
	public Integer getNrPagamento() {
		return nrPagamento;
	}
	
	/**
	 * Set: nrPagamento.
	 *
	 * @param nrPagamento the nr pagamento
	 */
	public void setNrPagamento(Integer nrPagamento) {
		this.nrPagamento = nrPagamento;
	}
	
	/**
	 * Get: ocorrencias.
	 *
	 * @return ocorrencias
	 */
	public List<OcorrenciasDTO> getOcorrencias() {
		return ocorrencias;
	}
	
	/**
	 * Set: ocorrencias.
	 *
	 * @param ocorrencias the ocorrencias
	 */
	public void setOcorrencias(List<OcorrenciasDTO> ocorrencias) {
		this.ocorrencias = ocorrencias;
	}
	
	/**
	 * Get: cdSolicitacaoPagamento.
	 *
	 * @return cdSolicitacaoPagamento
	 */
	public Integer getCdSolicitacaoPagamento() {
		return cdSolicitacaoPagamento;
	}
	
	/**
	 * Set: cdSolicitacaoPagamento.
	 *
	 * @param cdSolicitacaoPagamento the cd solicitacao pagamento
	 */
	public void setCdSolicitacaoPagamento(Integer cdSolicitacaoPagamento) {
		this.cdSolicitacaoPagamento = cdSolicitacaoPagamento;
	}
	
	/**
	 * Get: nrSolicitacaoPagamento.
	 *
	 * @return nrSolicitacaoPagamento
	 */
	public Integer getNrSolicitacaoPagamento() {
		return nrSolicitacaoPagamento;
	}
	
	/**
	 * Set: nrSolicitacaoPagamento.
	 *
	 * @param nrSolicitacaoPagamento the nr solicitacao pagamento
	 */
	public void setNrSolicitacaoPagamento(Integer nrSolicitacaoPagamento) {
		this.nrSolicitacaoPagamento = nrSolicitacaoPagamento;
	}
	
	/**
	 * Get: flagPrimeiraSolicitacao.
	 *
	 * @return flagPrimeiraSolicitacao
	 */
	public String getFlagPrimeiraSolicitacao() {
		return flagPrimeiraSolicitacao;
	}
	
	/**
	 * Set: flagPrimeiraSolicitacao.
	 *
	 * @param flagPrimeiraSolicitacao the flag primeira solicitacao
	 */
	public void setFlagPrimeiraSolicitacao(String flagPrimeiraSolicitacao) {
		this.flagPrimeiraSolicitacao = flagPrimeiraSolicitacao;
	}
	
	/**
	 * Get: cdProduto.
	 *
	 * @return cdProduto
	 */
	public Integer getCdProduto() {
		return cdProduto;
	}
	
	/**
	 * Set: cdProduto.
	 *
	 * @param cdProduto the cd produto
	 */
	public void setCdProduto(Integer cdProduto) {
		this.cdProduto = cdProduto;
	}
	
	/**
	 * Get: cdProdutoRelacionado.
	 *
	 * @return cdProdutoRelacionado
	 */
	public Integer getCdProdutoRelacionado() {
		return cdProdutoRelacionado;
	}
	
	/**
	 * Set: cdProdutoRelacionado.
	 *
	 * @param cdProdutoRelacionado the cd produto relacionado
	 */
	public void setCdProdutoRelacionado(Integer cdProdutoRelacionado) {
		this.cdProdutoRelacionado = cdProdutoRelacionado;
	}
}