/*
 * Nome: br.com.bradesco.web.pgit.service.business.combo.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.combo.bean;

/**
 * Nome: MotivoSituacaoPendenciaContratoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class MotivoSituacaoPendenciaContratoSaidaDTO {
	
	
	/** Atributo cdMotivoSituacaoPendencia. */
	private Integer cdMotivoSituacaoPendencia;
    
    /** Atributo dsMotivoSituacaoPendencia. */
    private String dsMotivoSituacaoPendencia;
    
    /**
     * Motivo situacao pendencia contrato saida dto.
     */
    MotivoSituacaoPendenciaContratoSaidaDTO(){
		super();
	}
	
	
	 /**
 	 * Motivo situacao pendencia contrato saida dto.
 	 *
 	 * @param cdMotivoSituacaoPendencia the cd motivo situacao pendencia
 	 * @param dsMotivoSituacaoPendencia the ds motivo situacao pendencia
 	 */
 	public MotivoSituacaoPendenciaContratoSaidaDTO(Integer  cdMotivoSituacaoPendencia, String dsMotivoSituacaoPendencia) {
			super();
			this.cdMotivoSituacaoPendencia = cdMotivoSituacaoPendencia;
			this.dsMotivoSituacaoPendencia = dsMotivoSituacaoPendencia;
		}


	/**
	 * Get: cdMotivoSituacaoPendencia.
	 *
	 * @return cdMotivoSituacaoPendencia
	 */
	public Integer getCdMotivoSituacaoPendencia() {
		return cdMotivoSituacaoPendencia;
	}


	/**
	 * Set: cdMotivoSituacaoPendencia.
	 *
	 * @param cdMotivoSituacaoPendencia the cd motivo situacao pendencia
	 */
	public void setCdMotivoSituacaoPendencia(Integer cdMotivoSituacaoPendencia) {
		this.cdMotivoSituacaoPendencia = cdMotivoSituacaoPendencia;
	}


	/**
	 * Get: dsMotivoSituacaoPendencia.
	 *
	 * @return dsMotivoSituacaoPendencia
	 */
	public String getDsMotivoSituacaoPendencia() {
		return dsMotivoSituacaoPendencia;
	}


	/**
	 * Set: dsMotivoSituacaoPendencia.
	 *
	 * @param dsMotivoSituacaoPendencia the ds motivo situacao pendencia
	 */
	public void setDsMotivoSituacaoPendencia(String dsMotivoSituacaoPendencia) {
		this.dsMotivoSituacaoPendencia = dsMotivoSituacaoPendencia;
	}
    
    

    
    




}
