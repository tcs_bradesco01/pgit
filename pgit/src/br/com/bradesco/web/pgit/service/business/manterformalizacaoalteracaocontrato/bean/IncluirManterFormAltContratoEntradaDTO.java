/*
 * Nome: br.com.bradesco.web.pgit.service.business.manterformalizacaoalteracaocontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.manterformalizacaoalteracaocontrato.bean;

/**
 * Nome: IncluirManterFormAltContratoEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class IncluirManterFormAltContratoEntradaDTO {
	
	
	
	/** Atributo codPessoaJuridicaContrato. */
	private int codPessoaJuridicaContrato;
	
	/** Atributo codTipoContratoNegocio. */
	private int codTipoContratoNegocio;
	
	/** Atributo numSeqContratoNegocio. */
	private long numSeqContratoNegocio;
	
	/** Atributo codIndicadorNovoContrato. */
	private int codIndicadorNovoContrato;
	
	
	/**
	 * Get: codIndicadorNovoContrato.
	 *
	 * @return codIndicadorNovoContrato
	 */
	public int getCodIndicadorNovoContrato() {
		return codIndicadorNovoContrato;
	}
	
	/**
	 * Set: codIndicadorNovoContrato.
	 *
	 * @param codIndicadorNovoContrato the cod indicador novo contrato
	 */
	public void setCodIndicadorNovoContrato(int codIndicadorNovoContrato) {
		this.codIndicadorNovoContrato = codIndicadorNovoContrato;
	}
	
	/**
	 * Get: codPessoaJuridicaContrato.
	 *
	 * @return codPessoaJuridicaContrato
	 */
	public int getCodPessoaJuridicaContrato() {
		return codPessoaJuridicaContrato;
	}
	
	/**
	 * Set: codPessoaJuridicaContrato.
	 *
	 * @param codPessoaJuridicaContrato the cod pessoa juridica contrato
	 */
	public void setCodPessoaJuridicaContrato(int codPessoaJuridicaContrato) {
		this.codPessoaJuridicaContrato = codPessoaJuridicaContrato;
	}
	
	/**
	 * Get: codTipoContratoNegocio.
	 *
	 * @return codTipoContratoNegocio
	 */
	public int getCodTipoContratoNegocio() {
		return codTipoContratoNegocio;
	}
	
	/**
	 * Set: codTipoContratoNegocio.
	 *
	 * @param codTipoContratoNegocio the cod tipo contrato negocio
	 */
	public void setCodTipoContratoNegocio(int codTipoContratoNegocio) {
		this.codTipoContratoNegocio = codTipoContratoNegocio;
	}
	
	/**
	 * Get: numSeqContratoNegocio.
	 *
	 * @return numSeqContratoNegocio
	 */
	public long getNumSeqContratoNegocio() {
		return numSeqContratoNegocio;
	}
	
	/**
	 * Set: numSeqContratoNegocio.
	 *
	 * @param numSeqContratoNegocio the num seq contrato negocio
	 */
	public void setNumSeqContratoNegocio(long numSeqContratoNegocio) {
		this.numSeqContratoNegocio = numSeqContratoNegocio;
	}
	
	
}
