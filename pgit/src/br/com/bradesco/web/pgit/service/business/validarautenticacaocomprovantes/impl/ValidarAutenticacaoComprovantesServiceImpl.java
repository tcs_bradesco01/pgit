/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/implementation.ftl,v $
 * $Id: implementation.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: implementation.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */
 
package br.com.bradesco.web.pgit.service.business.validarautenticacaocomprovantes.impl;

import br.com.bradesco.web.pgit.service.business.validarautenticacaocomprovantes.IValidarAutenticacaoComprovantesService;
import br.com.bradesco.web.pgit.service.business.validarautenticacaocomprovantes.bean.ConsultarAutenticacaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.validarautenticacaocomprovantes.bean.ConsultarAutenticacaoSaidaDTO;
import br.com.bradesco.web.pgit.service.data.pdc.FactoryAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarautenticacao.request.ConsultarAutenticacaoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarautenticacao.response.ConsultarAutenticacaoResponse;
import br.com.bradesco.web.pgit.utils.PgitUtil;


/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Implementa��o do adaptador: ValidarAutenticacaoComprovantes
 * </p>
 * 
 * @comment C�DIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public class ValidarAutenticacaoComprovantesServiceImpl implements IValidarAutenticacaoComprovantesService {
	
	/** Atributo factoryAdapter. */
	private FactoryAdapter factoryAdapter;
	
	

	/**
	 * Get: factoryAdapter.
	 *
	 * @return factoryAdapter
	 */
	public FactoryAdapter getFactoryAdapter() {
		return factoryAdapter;
	}



	/**
	 * Set: factoryAdapter.
	 *
	 * @param factoryAdapter the factory adapter
	 */
	public void setFactoryAdapter(FactoryAdapter factoryAdapter) {
		this.factoryAdapter = factoryAdapter;
	}



	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.validarautenticacaocomprovantes.IValidarAutenticacaoComprovantesService#consultarAutenticacao(br.com.bradesco.web.pgit.service.business.validarautenticacaocomprovantes.bean.ConsultarAutenticacaoEntradaDTO)
	 */
	public ConsultarAutenticacaoSaidaDTO consultarAutenticacao(ConsultarAutenticacaoEntradaDTO consultarAutenticacaoEntradaDTO) {
		ConsultarAutenticacaoRequest request = new ConsultarAutenticacaoRequest();
		request.setCdAgencia(PgitUtil.verificaIntegerNulo(consultarAutenticacaoEntradaDTO.getCdAgencia()));
		request.setCdBanco(PgitUtil.verificaIntegerNulo(consultarAutenticacaoEntradaDTO.getCdBanco()));
		request.setCdConta(PgitUtil.verificaLongNulo(consultarAutenticacaoEntradaDTO.getCdConta()));
		request.setCdDigitoConta(PgitUtil.DIGITO_CONTA);
		request.setCdModalidade(PgitUtil.verificaIntegerNulo(consultarAutenticacaoEntradaDTO.getCdModalidade()));
		request.setCdServico(PgitUtil.verificaIntegerNulo(consultarAutenticacaoEntradaDTO.getCdServico()));
		request.setDtPagamento(PgitUtil.verificaStringNula(consultarAutenticacaoEntradaDTO.getDtPagamento()));
		request.setVlrPagamentoClientePagador(PgitUtil.verificaBigDecimalNulo(consultarAutenticacaoEntradaDTO.getValorPagamento()));
		request.setCdFlagModalidade(PgitUtil.verificaIntegerNulo(consultarAutenticacaoEntradaDTO.getCdFlagModalidade()));
		request.setCdFlCodigoBarras(PgitUtil.verificaStringNula(consultarAutenticacaoEntradaDTO.getCdFlCodigoBarras()));
		request.setNrPagamento(PgitUtil.verificaStringNula(consultarAutenticacaoEntradaDTO.getNrPagamento()));
		
		ConsultarAutenticacaoResponse response = getFactoryAdapter().getConsultarAutenticacaoPDCAdapter().invokeProcess(request);
		return new ConsultarAutenticacaoSaidaDTO(response.getMensagem(), response.getCodMensagem(), response.getCdAutenticacao());
		
	}

}

