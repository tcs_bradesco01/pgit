/**
 * Nome: br.com.bradesco.web.pgit.service.business.solrelempresasacomp
 * Compilador: JDK 1.5
 * Prop�sito: INSERIR O PROP�SITO DAS CLASSES DO PACOTE
 * Data da cria��o: <dd/MM/yyyy>
 * Par�metros de compila��o: -d
 */
package br.com.bradesco.web.pgit.service.business.solrelempresasacomp;

import java.util.Date;
import java.util.List;

import br.com.bradesco.web.pgit.service.business.geral.MensagemSaida;
import br.com.bradesco.web.pgit.service.business.solrelempresasacomp.bean.ExcluirSoliRelatorioEmpresaAcompanhadaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.solrelempresasacomp.bean.IncluirSoliRelatorioEmpresaAcompanhadaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.solrelempresasacomp.bean.IncluirSoliRelatorioEmpresaAcompanhadaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.solrelempresasacomp.bean.SolicitacaoRelatorioEmpresasAcompOcorrencia;

/**
 * Nome: ISolrelempresasacompService
 * <p>
 * Prop�sito: Interface do adaptador Solrelempresasacomp
 * </p>
 * 
 * @author CPM Braxis / TI Melhorias - Arquitetura
 * @version 1.0
 */
public interface ISolRelEmpresasAcompService {

	/**
	 * Listar solicitacoes.
	 *
	 * @param dataInicial the data inicial
	 * @param dataFinal the data final
	 * @return the list< solicitacao relatorio empresas acomp ocorrencia>
	 */
	List<SolicitacaoRelatorioEmpresasAcompOcorrencia> listarSolicitacoes(Date dataInicial, Date dataFinal);
	
	/**
	 * Incluir solicitacoes.
	 *
	 * @param entrada the entrada
	 * @return the incluir soli relatorio empresa acompanhada saida dto
	 */
	IncluirSoliRelatorioEmpresaAcompanhadaSaidaDTO incluirSolicitacoes(IncluirSoliRelatorioEmpresaAcompanhadaEntradaDTO entrada);
	
	/**
	 * Excluir solicitacoes.
	 *
	 * @param entrada the entrada
	 * @return the mensagem saida
	 */
	MensagemSaida excluirSolicitacoes(ExcluirSoliRelatorioEmpresaAcompanhadaEntradaDTO entrada);
}