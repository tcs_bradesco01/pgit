/*
 * Nome: br.com.bradesco.web.pgit.service.business.simulacaotarifasnegociacao.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.simulacaotarifasnegociacao.bean;

import java.math.BigDecimal;

/**
 * Nome: OcorrenciasDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class OcorrenciasDTO {
	 
 	/** Atributo cdProdutoServicoOperacao. */
 	private Integer cdProdutoServicoOperacao;
	   
   	/** Atributo cdProdutoOperacaoRelacionado. */
   	private Integer cdProdutoOperacaoRelacionado;
	   
   	/** Atributo cdRelacionamentoProduto. */
   	private Integer cdRelacionamentoProduto;
	   
   	/** Atributo cdOperacaoProdutoServico. */
   	private Integer cdOperacaoProdutoServico;
	   
   	/** Atributo vlTarifaPadrao. */
   	private BigDecimal vlTarifaPadrao;
	   
   	/** Atributo vlTarifaProposta. */
   	private BigDecimal vlTarifaProposta;
	   
   	/** Atributo qtEstimada. */
   	private Long qtEstimada;
	   
   	/** Atributo qtDiasFloat. */
   	private Integer qtDiasFloat;
	
	/**
	 * Get: cdOperacaoProdutoServico.
	 *
	 * @return cdOperacaoProdutoServico
	 */
	public Integer getCdOperacaoProdutoServico() {
		return cdOperacaoProdutoServico;
	}
	
	/**
	 * Set: cdOperacaoProdutoServico.
	 *
	 * @param cdOperacaoProdutoServico the cd operacao produto servico
	 */
	public void setCdOperacaoProdutoServico(Integer cdOperacaoProdutoServico) {
		this.cdOperacaoProdutoServico = cdOperacaoProdutoServico;
	}
	
	/**
	 * Get: cdProdutoOperacaoRelacionado.
	 *
	 * @return cdProdutoOperacaoRelacionado
	 */
	public Integer getCdProdutoOperacaoRelacionado() {
		return cdProdutoOperacaoRelacionado;
	}
	
	/**
	 * Set: cdProdutoOperacaoRelacionado.
	 *
	 * @param cdProdutoOperacaoRelacionado the cd produto operacao relacionado
	 */
	public void setCdProdutoOperacaoRelacionado(Integer cdProdutoOperacaoRelacionado) {
		this.cdProdutoOperacaoRelacionado = cdProdutoOperacaoRelacionado;
	}
	
	/**
	 * Get: cdProdutoServicoOperacao.
	 *
	 * @return cdProdutoServicoOperacao
	 */
	public Integer getCdProdutoServicoOperacao() {
		return cdProdutoServicoOperacao;
	}
	
	/**
	 * Set: cdProdutoServicoOperacao.
	 *
	 * @param cdProdutoServicoOperacao the cd produto servico operacao
	 */
	public void setCdProdutoServicoOperacao(Integer cdProdutoServicoOperacao) {
		this.cdProdutoServicoOperacao = cdProdutoServicoOperacao;
	}
	
	/**
	 * Get: cdRelacionamentoProduto.
	 *
	 * @return cdRelacionamentoProduto
	 */
	public Integer getCdRelacionamentoProduto() {
		return cdRelacionamentoProduto;
	}
	
	/**
	 * Set: cdRelacionamentoProduto.
	 *
	 * @param cdRelacionamentoProduto the cd relacionamento produto
	 */
	public void setCdRelacionamentoProduto(Integer cdRelacionamentoProduto) {
		this.cdRelacionamentoProduto = cdRelacionamentoProduto;
	}
	
	/**
	 * Get: qtDiasFloat.
	 *
	 * @return qtDiasFloat
	 */
	public Integer getQtDiasFloat() {
		return qtDiasFloat;
	}
	
	/**
	 * Set: qtDiasFloat.
	 *
	 * @param qtDiasFloat the qt dias float
	 */
	public void setQtDiasFloat(Integer qtDiasFloat) {
		this.qtDiasFloat = qtDiasFloat;
	}
	
	/**
	 * Get: qtEstimada.
	 *
	 * @return qtEstimada
	 */
	public Long getQtEstimada() {
		return qtEstimada;
	}
	
	/**
	 * Set: qtEstimada.
	 *
	 * @param qtEstimada the qt estimada
	 */
	public void setQtEstimada(Long qtEstimada) {
		this.qtEstimada = qtEstimada;
	}
	
	/**
	 * Get: vlTarifaPadrao.
	 *
	 * @return vlTarifaPadrao
	 */
	public BigDecimal getVlTarifaPadrao() {
		return vlTarifaPadrao;
	}
	
	/**
	 * Set: vlTarifaPadrao.
	 *
	 * @param vlTarifaPadrao the vl tarifa padrao
	 */
	public void setVlTarifaPadrao(BigDecimal vlTarifaPadrao) {
		this.vlTarifaPadrao = vlTarifaPadrao;
	}
	
	/**
	 * Get: vlTarifaProposta.
	 *
	 * @return vlTarifaProposta
	 */
	public BigDecimal getVlTarifaProposta() {
		return vlTarifaProposta;
	}
	
	/**
	 * Set: vlTarifaProposta.
	 *
	 * @param vlTarifaProposta the vl tarifa proposta
	 */
	public void setVlTarifaProposta(BigDecimal vlTarifaProposta) {
		this.vlTarifaProposta = vlTarifaProposta;
	}
	   
	   
}
