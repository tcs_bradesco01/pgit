/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultarmanutencaocontacontrato.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ConsultarManutencaoContaContratoResponse.
 * 
 * @version $Revision$ $Date$
 */
public class ConsultarManutencaoContaContratoResponse implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _codMensagem
     */
    private java.lang.String _codMensagem;

    /**
     * Field _mensagem
     */
    private java.lang.String _mensagem;

    /**
     * Field _cdPessoaJuridicaContrato
     */
    private long _cdPessoaJuridicaContrato = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridicaContrato
     */
    private boolean _has_cdPessoaJuridicaContrato;

    /**
     * Field _cdTipoContratoNegocio
     */
    private int _cdTipoContratoNegocio = 0;

    /**
     * keeps track of state for field: _cdTipoContratoNegocio
     */
    private boolean _has_cdTipoContratoNegocio;

    /**
     * Field _nrSequenciaContratoNegocio
     */
    private long _nrSequenciaContratoNegocio = 0;

    /**
     * keeps track of state for field: _nrSequenciaContratoNegocio
     */
    private boolean _has_nrSequenciaContratoNegocio;

    /**
     * Field _cdTipoManutencaoContrato
     */
    private int _cdTipoManutencaoContrato = 0;

    /**
     * keeps track of state for field: _cdTipoManutencaoContrato
     */
    private boolean _has_cdTipoManutencaoContrato;

    /**
     * Field _nrManutencaoContratoNegocio
     */
    private long _nrManutencaoContratoNegocio = 0;

    /**
     * keeps track of state for field: _nrManutencaoContratoNegocio
     */
    private boolean _has_nrManutencaoContratoNegocio;

    /**
     * Field _cdSituacaoManutencaoContrato
     */
    private int _cdSituacaoManutencaoContrato = 0;

    /**
     * keeps track of state for field: _cdSituacaoManutencaoContrato
     */
    private boolean _has_cdSituacaoManutencaoContrato;

    /**
     * Field _dsSituacaoManutencaoContrato
     */
    private java.lang.String _dsSituacaoManutencaoContrato;

    /**
     * Field _cdMotivoManutencaoContrato
     */
    private int _cdMotivoManutencaoContrato = 0;

    /**
     * keeps track of state for field: _cdMotivoManutencaoContrato
     */
    private boolean _has_cdMotivoManutencaoContrato;

    /**
     * Field _dsMotivoManutencaoContrato
     */
    private java.lang.String _dsMotivoManutencaoContrato;

    /**
     * Field _nrAditivoContratoNegocio
     */
    private long _nrAditivoContratoNegocio = 0;

    /**
     * keeps track of state for field: _nrAditivoContratoNegocio
     */
    private boolean _has_nrAditivoContratoNegocio;

    /**
     * Field _cdIndicadorTipoManutencao
     */
    private int _cdIndicadorTipoManutencao = 0;

    /**
     * keeps track of state for field: _cdIndicadorTipoManutencao
     */
    private boolean _has_cdIndicadorTipoManutencao;

    /**
     * Field _dsIndicadorTipoManutencao
     */
    private java.lang.String _dsIndicadorTipoManutencao;

    /**
     * Field _dsTipoContaContrato
     */
    private java.lang.String _dsTipoContaContrato;

    /**
     * Field _dtVinculo
     */
    private java.lang.String _dtVinculo;

    /**
     * Field _cdSituacaoVinculo
     */
    private java.lang.String _cdSituacaoVinculo;

    /**
     * Field _cdBanco
     */
    private int _cdBanco = 0;

    /**
     * keeps track of state for field: _cdBanco
     */
    private boolean _has_cdBanco;

    /**
     * Field _dsBanco
     */
    private java.lang.String _dsBanco;

    /**
     * Field _cdComercialAgenciaContabil
     */
    private int _cdComercialAgenciaContabil = 0;

    /**
     * keeps track of state for field: _cdComercialAgenciaContabil
     */
    private boolean _has_cdComercialAgenciaContabil;

    /**
     * Field _dsComercialAgenciaContabil
     */
    private java.lang.String _dsComercialAgenciaContabil;

    /**
     * Field _nrConta
     */
    private long _nrConta = 0;

    /**
     * keeps track of state for field: _nrConta
     */
    private boolean _has_nrConta;

    /**
     * Field _cdDigitoConta
     */
    private java.lang.String _cdDigitoConta;

    /**
     * Field _cdCpfCnpj
     */
    private java.lang.String _cdCpfCnpj;

    /**
     * Field _nmPessoa
     */
    private java.lang.String _nmPessoa;

    /**
     * Field _hrInclusaoRegistro
     */
    private java.lang.String _hrInclusaoRegistro;

    /**
     * Field _hrManutencaoRegistro
     */
    private java.lang.String _hrManutencaoRegistro;

    /**
     * Field _cdUsuarioInclusao
     */
    private java.lang.String _cdUsuarioInclusao;

    /**
     * Field _cdUsuarioInclusaoExterno
     */
    private java.lang.String _cdUsuarioInclusaoExterno;

    /**
     * Field _cdOperacaoCanalInclusao
     */
    private java.lang.String _cdOperacaoCanalInclusao;

    /**
     * Field _cdTipoCanalInclusao
     */
    private int _cdTipoCanalInclusao = 0;

    /**
     * keeps track of state for field: _cdTipoCanalInclusao
     */
    private boolean _has_cdTipoCanalInclusao;

    /**
     * Field _dsCanalInclusao
     */
    private java.lang.String _dsCanalInclusao;

    /**
     * Field _cdusuarioManutencao
     */
    private java.lang.String _cdusuarioManutencao;

    /**
     * Field _cdUsuarioManutencaoExterno
     */
    private java.lang.String _cdUsuarioManutencaoExterno;

    /**
     * Field _cdOperacaoCanalManutencao
     */
    private java.lang.String _cdOperacaoCanalManutencao;

    /**
     * Field _cdTipoCanalManutencao
     */
    private int _cdTipoCanalManutencao = 0;

    /**
     * keeps track of state for field: _cdTipoCanalManutencao
     */
    private boolean _has_cdTipoCanalManutencao;

    /**
     * Field _dsCanalManutencao
     */
    private java.lang.String _dsCanalManutencao;


      //----------------/
     //- Constructors -/
    //----------------/

    public ConsultarManutencaoContaContratoResponse() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarmanutencaocontacontrato.response.ConsultarManutencaoContaContratoResponse()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdBanco
     * 
     */
    public void deleteCdBanco()
    {
        this._has_cdBanco= false;
    } //-- void deleteCdBanco() 

    /**
     * Method deleteCdComercialAgenciaContabil
     * 
     */
    public void deleteCdComercialAgenciaContabil()
    {
        this._has_cdComercialAgenciaContabil= false;
    } //-- void deleteCdComercialAgenciaContabil() 

    /**
     * Method deleteCdIndicadorTipoManutencao
     * 
     */
    public void deleteCdIndicadorTipoManutencao()
    {
        this._has_cdIndicadorTipoManutencao= false;
    } //-- void deleteCdIndicadorTipoManutencao() 

    /**
     * Method deleteCdMotivoManutencaoContrato
     * 
     */
    public void deleteCdMotivoManutencaoContrato()
    {
        this._has_cdMotivoManutencaoContrato= false;
    } //-- void deleteCdMotivoManutencaoContrato() 

    /**
     * Method deleteCdPessoaJuridicaContrato
     * 
     */
    public void deleteCdPessoaJuridicaContrato()
    {
        this._has_cdPessoaJuridicaContrato= false;
    } //-- void deleteCdPessoaJuridicaContrato() 

    /**
     * Method deleteCdSituacaoManutencaoContrato
     * 
     */
    public void deleteCdSituacaoManutencaoContrato()
    {
        this._has_cdSituacaoManutencaoContrato= false;
    } //-- void deleteCdSituacaoManutencaoContrato() 

    /**
     * Method deleteCdTipoCanalInclusao
     * 
     */
    public void deleteCdTipoCanalInclusao()
    {
        this._has_cdTipoCanalInclusao= false;
    } //-- void deleteCdTipoCanalInclusao() 

    /**
     * Method deleteCdTipoCanalManutencao
     * 
     */
    public void deleteCdTipoCanalManutencao()
    {
        this._has_cdTipoCanalManutencao= false;
    } //-- void deleteCdTipoCanalManutencao() 

    /**
     * Method deleteCdTipoContratoNegocio
     * 
     */
    public void deleteCdTipoContratoNegocio()
    {
        this._has_cdTipoContratoNegocio= false;
    } //-- void deleteCdTipoContratoNegocio() 

    /**
     * Method deleteCdTipoManutencaoContrato
     * 
     */
    public void deleteCdTipoManutencaoContrato()
    {
        this._has_cdTipoManutencaoContrato= false;
    } //-- void deleteCdTipoManutencaoContrato() 

    /**
     * Method deleteNrAditivoContratoNegocio
     * 
     */
    public void deleteNrAditivoContratoNegocio()
    {
        this._has_nrAditivoContratoNegocio= false;
    } //-- void deleteNrAditivoContratoNegocio() 

    /**
     * Method deleteNrConta
     * 
     */
    public void deleteNrConta()
    {
        this._has_nrConta= false;
    } //-- void deleteNrConta() 

    /**
     * Method deleteNrManutencaoContratoNegocio
     * 
     */
    public void deleteNrManutencaoContratoNegocio()
    {
        this._has_nrManutencaoContratoNegocio= false;
    } //-- void deleteNrManutencaoContratoNegocio() 

    /**
     * Method deleteNrSequenciaContratoNegocio
     * 
     */
    public void deleteNrSequenciaContratoNegocio()
    {
        this._has_nrSequenciaContratoNegocio= false;
    } //-- void deleteNrSequenciaContratoNegocio() 

    /**
     * Returns the value of field 'cdBanco'.
     * 
     * @return int
     * @return the value of field 'cdBanco'.
     */
    public int getCdBanco()
    {
        return this._cdBanco;
    } //-- int getCdBanco() 

    /**
     * Returns the value of field 'cdComercialAgenciaContabil'.
     * 
     * @return int
     * @return the value of field 'cdComercialAgenciaContabil'.
     */
    public int getCdComercialAgenciaContabil()
    {
        return this._cdComercialAgenciaContabil;
    } //-- int getCdComercialAgenciaContabil() 

    /**
     * Returns the value of field 'cdCpfCnpj'.
     * 
     * @return String
     * @return the value of field 'cdCpfCnpj'.
     */
    public java.lang.String getCdCpfCnpj()
    {
        return this._cdCpfCnpj;
    } //-- java.lang.String getCdCpfCnpj() 

    /**
     * Returns the value of field 'cdDigitoConta'.
     * 
     * @return String
     * @return the value of field 'cdDigitoConta'.
     */
    public java.lang.String getCdDigitoConta()
    {
        return this._cdDigitoConta;
    } //-- java.lang.String getCdDigitoConta() 

    /**
     * Returns the value of field 'cdIndicadorTipoManutencao'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorTipoManutencao'.
     */
    public int getCdIndicadorTipoManutencao()
    {
        return this._cdIndicadorTipoManutencao;
    } //-- int getCdIndicadorTipoManutencao() 

    /**
     * Returns the value of field 'cdMotivoManutencaoContrato'.
     * 
     * @return int
     * @return the value of field 'cdMotivoManutencaoContrato'.
     */
    public int getCdMotivoManutencaoContrato()
    {
        return this._cdMotivoManutencaoContrato;
    } //-- int getCdMotivoManutencaoContrato() 

    /**
     * Returns the value of field 'cdOperacaoCanalInclusao'.
     * 
     * @return String
     * @return the value of field 'cdOperacaoCanalInclusao'.
     */
    public java.lang.String getCdOperacaoCanalInclusao()
    {
        return this._cdOperacaoCanalInclusao;
    } //-- java.lang.String getCdOperacaoCanalInclusao() 

    /**
     * Returns the value of field 'cdOperacaoCanalManutencao'.
     * 
     * @return String
     * @return the value of field 'cdOperacaoCanalManutencao'.
     */
    public java.lang.String getCdOperacaoCanalManutencao()
    {
        return this._cdOperacaoCanalManutencao;
    } //-- java.lang.String getCdOperacaoCanalManutencao() 

    /**
     * Returns the value of field 'cdPessoaJuridicaContrato'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridicaContrato'.
     */
    public long getCdPessoaJuridicaContrato()
    {
        return this._cdPessoaJuridicaContrato;
    } //-- long getCdPessoaJuridicaContrato() 

    /**
     * Returns the value of field 'cdSituacaoManutencaoContrato'.
     * 
     * @return int
     * @return the value of field 'cdSituacaoManutencaoContrato'.
     */
    public int getCdSituacaoManutencaoContrato()
    {
        return this._cdSituacaoManutencaoContrato;
    } //-- int getCdSituacaoManutencaoContrato() 

    /**
     * Returns the value of field 'cdSituacaoVinculo'.
     * 
     * @return String
     * @return the value of field 'cdSituacaoVinculo'.
     */
    public java.lang.String getCdSituacaoVinculo()
    {
        return this._cdSituacaoVinculo;
    } //-- java.lang.String getCdSituacaoVinculo() 

    /**
     * Returns the value of field 'cdTipoCanalInclusao'.
     * 
     * @return int
     * @return the value of field 'cdTipoCanalInclusao'.
     */
    public int getCdTipoCanalInclusao()
    {
        return this._cdTipoCanalInclusao;
    } //-- int getCdTipoCanalInclusao() 

    /**
     * Returns the value of field 'cdTipoCanalManutencao'.
     * 
     * @return int
     * @return the value of field 'cdTipoCanalManutencao'.
     */
    public int getCdTipoCanalManutencao()
    {
        return this._cdTipoCanalManutencao;
    } //-- int getCdTipoCanalManutencao() 

    /**
     * Returns the value of field 'cdTipoContratoNegocio'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoNegocio'.
     */
    public int getCdTipoContratoNegocio()
    {
        return this._cdTipoContratoNegocio;
    } //-- int getCdTipoContratoNegocio() 

    /**
     * Returns the value of field 'cdTipoManutencaoContrato'.
     * 
     * @return int
     * @return the value of field 'cdTipoManutencaoContrato'.
     */
    public int getCdTipoManutencaoContrato()
    {
        return this._cdTipoManutencaoContrato;
    } //-- int getCdTipoManutencaoContrato() 

    /**
     * Returns the value of field 'cdUsuarioInclusao'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioInclusao'.
     */
    public java.lang.String getCdUsuarioInclusao()
    {
        return this._cdUsuarioInclusao;
    } //-- java.lang.String getCdUsuarioInclusao() 

    /**
     * Returns the value of field 'cdUsuarioInclusaoExterno'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioInclusaoExterno'.
     */
    public java.lang.String getCdUsuarioInclusaoExterno()
    {
        return this._cdUsuarioInclusaoExterno;
    } //-- java.lang.String getCdUsuarioInclusaoExterno() 

    /**
     * Returns the value of field 'cdUsuarioManutencaoExterno'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioManutencaoExterno'.
     */
    public java.lang.String getCdUsuarioManutencaoExterno()
    {
        return this._cdUsuarioManutencaoExterno;
    } //-- java.lang.String getCdUsuarioManutencaoExterno() 

    /**
     * Returns the value of field 'cdusuarioManutencao'.
     * 
     * @return String
     * @return the value of field 'cdusuarioManutencao'.
     */
    public java.lang.String getCdusuarioManutencao()
    {
        return this._cdusuarioManutencao;
    } //-- java.lang.String getCdusuarioManutencao() 

    /**
     * Returns the value of field 'codMensagem'.
     * 
     * @return String
     * @return the value of field 'codMensagem'.
     */
    public java.lang.String getCodMensagem()
    {
        return this._codMensagem;
    } //-- java.lang.String getCodMensagem() 

    /**
     * Returns the value of field 'dsBanco'.
     * 
     * @return String
     * @return the value of field 'dsBanco'.
     */
    public java.lang.String getDsBanco()
    {
        return this._dsBanco;
    } //-- java.lang.String getDsBanco() 

    /**
     * Returns the value of field 'dsCanalInclusao'.
     * 
     * @return String
     * @return the value of field 'dsCanalInclusao'.
     */
    public java.lang.String getDsCanalInclusao()
    {
        return this._dsCanalInclusao;
    } //-- java.lang.String getDsCanalInclusao() 

    /**
     * Returns the value of field 'dsCanalManutencao'.
     * 
     * @return String
     * @return the value of field 'dsCanalManutencao'.
     */
    public java.lang.String getDsCanalManutencao()
    {
        return this._dsCanalManutencao;
    } //-- java.lang.String getDsCanalManutencao() 

    /**
     * Returns the value of field 'dsComercialAgenciaContabil'.
     * 
     * @return String
     * @return the value of field 'dsComercialAgenciaContabil'.
     */
    public java.lang.String getDsComercialAgenciaContabil()
    {
        return this._dsComercialAgenciaContabil;
    } //-- java.lang.String getDsComercialAgenciaContabil() 

    /**
     * Returns the value of field 'dsIndicadorTipoManutencao'.
     * 
     * @return String
     * @return the value of field 'dsIndicadorTipoManutencao'.
     */
    public java.lang.String getDsIndicadorTipoManutencao()
    {
        return this._dsIndicadorTipoManutencao;
    } //-- java.lang.String getDsIndicadorTipoManutencao() 

    /**
     * Returns the value of field 'dsMotivoManutencaoContrato'.
     * 
     * @return String
     * @return the value of field 'dsMotivoManutencaoContrato'.
     */
    public java.lang.String getDsMotivoManutencaoContrato()
    {
        return this._dsMotivoManutencaoContrato;
    } //-- java.lang.String getDsMotivoManutencaoContrato() 

    /**
     * Returns the value of field 'dsSituacaoManutencaoContrato'.
     * 
     * @return String
     * @return the value of field 'dsSituacaoManutencaoContrato'.
     */
    public java.lang.String getDsSituacaoManutencaoContrato()
    {
        return this._dsSituacaoManutencaoContrato;
    } //-- java.lang.String getDsSituacaoManutencaoContrato() 

    /**
     * Returns the value of field 'dsTipoContaContrato'.
     * 
     * @return String
     * @return the value of field 'dsTipoContaContrato'.
     */
    public java.lang.String getDsTipoContaContrato()
    {
        return this._dsTipoContaContrato;
    } //-- java.lang.String getDsTipoContaContrato() 

    /**
     * Returns the value of field 'dtVinculo'.
     * 
     * @return String
     * @return the value of field 'dtVinculo'.
     */
    public java.lang.String getDtVinculo()
    {
        return this._dtVinculo;
    } //-- java.lang.String getDtVinculo() 

    /**
     * Returns the value of field 'hrInclusaoRegistro'.
     * 
     * @return String
     * @return the value of field 'hrInclusaoRegistro'.
     */
    public java.lang.String getHrInclusaoRegistro()
    {
        return this._hrInclusaoRegistro;
    } //-- java.lang.String getHrInclusaoRegistro() 

    /**
     * Returns the value of field 'hrManutencaoRegistro'.
     * 
     * @return String
     * @return the value of field 'hrManutencaoRegistro'.
     */
    public java.lang.String getHrManutencaoRegistro()
    {
        return this._hrManutencaoRegistro;
    } //-- java.lang.String getHrManutencaoRegistro() 

    /**
     * Returns the value of field 'mensagem'.
     * 
     * @return String
     * @return the value of field 'mensagem'.
     */
    public java.lang.String getMensagem()
    {
        return this._mensagem;
    } //-- java.lang.String getMensagem() 

    /**
     * Returns the value of field 'nmPessoa'.
     * 
     * @return String
     * @return the value of field 'nmPessoa'.
     */
    public java.lang.String getNmPessoa()
    {
        return this._nmPessoa;
    } //-- java.lang.String getNmPessoa() 

    /**
     * Returns the value of field 'nrAditivoContratoNegocio'.
     * 
     * @return long
     * @return the value of field 'nrAditivoContratoNegocio'.
     */
    public long getNrAditivoContratoNegocio()
    {
        return this._nrAditivoContratoNegocio;
    } //-- long getNrAditivoContratoNegocio() 

    /**
     * Returns the value of field 'nrConta'.
     * 
     * @return long
     * @return the value of field 'nrConta'.
     */
    public long getNrConta()
    {
        return this._nrConta;
    } //-- long getNrConta() 

    /**
     * Returns the value of field 'nrManutencaoContratoNegocio'.
     * 
     * @return long
     * @return the value of field 'nrManutencaoContratoNegocio'.
     */
    public long getNrManutencaoContratoNegocio()
    {
        return this._nrManutencaoContratoNegocio;
    } //-- long getNrManutencaoContratoNegocio() 

    /**
     * Returns the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @return long
     * @return the value of field 'nrSequenciaContratoNegocio'.
     */
    public long getNrSequenciaContratoNegocio()
    {
        return this._nrSequenciaContratoNegocio;
    } //-- long getNrSequenciaContratoNegocio() 

    /**
     * Method hasCdBanco
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdBanco()
    {
        return this._has_cdBanco;
    } //-- boolean hasCdBanco() 

    /**
     * Method hasCdComercialAgenciaContabil
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdComercialAgenciaContabil()
    {
        return this._has_cdComercialAgenciaContabil;
    } //-- boolean hasCdComercialAgenciaContabil() 

    /**
     * Method hasCdIndicadorTipoManutencao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorTipoManutencao()
    {
        return this._has_cdIndicadorTipoManutencao;
    } //-- boolean hasCdIndicadorTipoManutencao() 

    /**
     * Method hasCdMotivoManutencaoContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMotivoManutencaoContrato()
    {
        return this._has_cdMotivoManutencaoContrato;
    } //-- boolean hasCdMotivoManutencaoContrato() 

    /**
     * Method hasCdPessoaJuridicaContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridicaContrato()
    {
        return this._has_cdPessoaJuridicaContrato;
    } //-- boolean hasCdPessoaJuridicaContrato() 

    /**
     * Method hasCdSituacaoManutencaoContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSituacaoManutencaoContrato()
    {
        return this._has_cdSituacaoManutencaoContrato;
    } //-- boolean hasCdSituacaoManutencaoContrato() 

    /**
     * Method hasCdTipoCanalInclusao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoCanalInclusao()
    {
        return this._has_cdTipoCanalInclusao;
    } //-- boolean hasCdTipoCanalInclusao() 

    /**
     * Method hasCdTipoCanalManutencao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoCanalManutencao()
    {
        return this._has_cdTipoCanalManutencao;
    } //-- boolean hasCdTipoCanalManutencao() 

    /**
     * Method hasCdTipoContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoNegocio()
    {
        return this._has_cdTipoContratoNegocio;
    } //-- boolean hasCdTipoContratoNegocio() 

    /**
     * Method hasCdTipoManutencaoContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoManutencaoContrato()
    {
        return this._has_cdTipoManutencaoContrato;
    } //-- boolean hasCdTipoManutencaoContrato() 

    /**
     * Method hasNrAditivoContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrAditivoContratoNegocio()
    {
        return this._has_nrAditivoContratoNegocio;
    } //-- boolean hasNrAditivoContratoNegocio() 

    /**
     * Method hasNrConta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrConta()
    {
        return this._has_nrConta;
    } //-- boolean hasNrConta() 

    /**
     * Method hasNrManutencaoContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrManutencaoContratoNegocio()
    {
        return this._has_nrManutencaoContratoNegocio;
    } //-- boolean hasNrManutencaoContratoNegocio() 

    /**
     * Method hasNrSequenciaContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaContratoNegocio()
    {
        return this._has_nrSequenciaContratoNegocio;
    } //-- boolean hasNrSequenciaContratoNegocio() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdBanco'.
     * 
     * @param cdBanco the value of field 'cdBanco'.
     */
    public void setCdBanco(int cdBanco)
    {
        this._cdBanco = cdBanco;
        this._has_cdBanco = true;
    } //-- void setCdBanco(int) 

    /**
     * Sets the value of field 'cdComercialAgenciaContabil'.
     * 
     * @param cdComercialAgenciaContabil the value of field
     * 'cdComercialAgenciaContabil'.
     */
    public void setCdComercialAgenciaContabil(int cdComercialAgenciaContabil)
    {
        this._cdComercialAgenciaContabil = cdComercialAgenciaContabil;
        this._has_cdComercialAgenciaContabil = true;
    } //-- void setCdComercialAgenciaContabil(int) 

    /**
     * Sets the value of field 'cdCpfCnpj'.
     * 
     * @param cdCpfCnpj the value of field 'cdCpfCnpj'.
     */
    public void setCdCpfCnpj(java.lang.String cdCpfCnpj)
    {
        this._cdCpfCnpj = cdCpfCnpj;
    } //-- void setCdCpfCnpj(java.lang.String) 

    /**
     * Sets the value of field 'cdDigitoConta'.
     * 
     * @param cdDigitoConta the value of field 'cdDigitoConta'.
     */
    public void setCdDigitoConta(java.lang.String cdDigitoConta)
    {
        this._cdDigitoConta = cdDigitoConta;
    } //-- void setCdDigitoConta(java.lang.String) 

    /**
     * Sets the value of field 'cdIndicadorTipoManutencao'.
     * 
     * @param cdIndicadorTipoManutencao the value of field
     * 'cdIndicadorTipoManutencao'.
     */
    public void setCdIndicadorTipoManutencao(int cdIndicadorTipoManutencao)
    {
        this._cdIndicadorTipoManutencao = cdIndicadorTipoManutencao;
        this._has_cdIndicadorTipoManutencao = true;
    } //-- void setCdIndicadorTipoManutencao(int) 

    /**
     * Sets the value of field 'cdMotivoManutencaoContrato'.
     * 
     * @param cdMotivoManutencaoContrato the value of field
     * 'cdMotivoManutencaoContrato'.
     */
    public void setCdMotivoManutencaoContrato(int cdMotivoManutencaoContrato)
    {
        this._cdMotivoManutencaoContrato = cdMotivoManutencaoContrato;
        this._has_cdMotivoManutencaoContrato = true;
    } //-- void setCdMotivoManutencaoContrato(int) 

    /**
     * Sets the value of field 'cdOperacaoCanalInclusao'.
     * 
     * @param cdOperacaoCanalInclusao the value of field
     * 'cdOperacaoCanalInclusao'.
     */
    public void setCdOperacaoCanalInclusao(java.lang.String cdOperacaoCanalInclusao)
    {
        this._cdOperacaoCanalInclusao = cdOperacaoCanalInclusao;
    } //-- void setCdOperacaoCanalInclusao(java.lang.String) 

    /**
     * Sets the value of field 'cdOperacaoCanalManutencao'.
     * 
     * @param cdOperacaoCanalManutencao the value of field
     * 'cdOperacaoCanalManutencao'.
     */
    public void setCdOperacaoCanalManutencao(java.lang.String cdOperacaoCanalManutencao)
    {
        this._cdOperacaoCanalManutencao = cdOperacaoCanalManutencao;
    } //-- void setCdOperacaoCanalManutencao(java.lang.String) 

    /**
     * Sets the value of field 'cdPessoaJuridicaContrato'.
     * 
     * @param cdPessoaJuridicaContrato the value of field
     * 'cdPessoaJuridicaContrato'.
     */
    public void setCdPessoaJuridicaContrato(long cdPessoaJuridicaContrato)
    {
        this._cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
        this._has_cdPessoaJuridicaContrato = true;
    } //-- void setCdPessoaJuridicaContrato(long) 

    /**
     * Sets the value of field 'cdSituacaoManutencaoContrato'.
     * 
     * @param cdSituacaoManutencaoContrato the value of field
     * 'cdSituacaoManutencaoContrato'.
     */
    public void setCdSituacaoManutencaoContrato(int cdSituacaoManutencaoContrato)
    {
        this._cdSituacaoManutencaoContrato = cdSituacaoManutencaoContrato;
        this._has_cdSituacaoManutencaoContrato = true;
    } //-- void setCdSituacaoManutencaoContrato(int) 

    /**
     * Sets the value of field 'cdSituacaoVinculo'.
     * 
     * @param cdSituacaoVinculo the value of field
     * 'cdSituacaoVinculo'.
     */
    public void setCdSituacaoVinculo(java.lang.String cdSituacaoVinculo)
    {
        this._cdSituacaoVinculo = cdSituacaoVinculo;
    } //-- void setCdSituacaoVinculo(java.lang.String) 

    /**
     * Sets the value of field 'cdTipoCanalInclusao'.
     * 
     * @param cdTipoCanalInclusao the value of field
     * 'cdTipoCanalInclusao'.
     */
    public void setCdTipoCanalInclusao(int cdTipoCanalInclusao)
    {
        this._cdTipoCanalInclusao = cdTipoCanalInclusao;
        this._has_cdTipoCanalInclusao = true;
    } //-- void setCdTipoCanalInclusao(int) 

    /**
     * Sets the value of field 'cdTipoCanalManutencao'.
     * 
     * @param cdTipoCanalManutencao the value of field
     * 'cdTipoCanalManutencao'.
     */
    public void setCdTipoCanalManutencao(int cdTipoCanalManutencao)
    {
        this._cdTipoCanalManutencao = cdTipoCanalManutencao;
        this._has_cdTipoCanalManutencao = true;
    } //-- void setCdTipoCanalManutencao(int) 

    /**
     * Sets the value of field 'cdTipoContratoNegocio'.
     * 
     * @param cdTipoContratoNegocio the value of field
     * 'cdTipoContratoNegocio'.
     */
    public void setCdTipoContratoNegocio(int cdTipoContratoNegocio)
    {
        this._cdTipoContratoNegocio = cdTipoContratoNegocio;
        this._has_cdTipoContratoNegocio = true;
    } //-- void setCdTipoContratoNegocio(int) 

    /**
     * Sets the value of field 'cdTipoManutencaoContrato'.
     * 
     * @param cdTipoManutencaoContrato the value of field
     * 'cdTipoManutencaoContrato'.
     */
    public void setCdTipoManutencaoContrato(int cdTipoManutencaoContrato)
    {
        this._cdTipoManutencaoContrato = cdTipoManutencaoContrato;
        this._has_cdTipoManutencaoContrato = true;
    } //-- void setCdTipoManutencaoContrato(int) 

    /**
     * Sets the value of field 'cdUsuarioInclusao'.
     * 
     * @param cdUsuarioInclusao the value of field
     * 'cdUsuarioInclusao'.
     */
    public void setCdUsuarioInclusao(java.lang.String cdUsuarioInclusao)
    {
        this._cdUsuarioInclusao = cdUsuarioInclusao;
    } //-- void setCdUsuarioInclusao(java.lang.String) 

    /**
     * Sets the value of field 'cdUsuarioInclusaoExterno'.
     * 
     * @param cdUsuarioInclusaoExterno the value of field
     * 'cdUsuarioInclusaoExterno'.
     */
    public void setCdUsuarioInclusaoExterno(java.lang.String cdUsuarioInclusaoExterno)
    {
        this._cdUsuarioInclusaoExterno = cdUsuarioInclusaoExterno;
    } //-- void setCdUsuarioInclusaoExterno(java.lang.String) 

    /**
     * Sets the value of field 'cdUsuarioManutencaoExterno'.
     * 
     * @param cdUsuarioManutencaoExterno the value of field
     * 'cdUsuarioManutencaoExterno'.
     */
    public void setCdUsuarioManutencaoExterno(java.lang.String cdUsuarioManutencaoExterno)
    {
        this._cdUsuarioManutencaoExterno = cdUsuarioManutencaoExterno;
    } //-- void setCdUsuarioManutencaoExterno(java.lang.String) 

    /**
     * Sets the value of field 'cdusuarioManutencao'.
     * 
     * @param cdusuarioManutencao the value of field
     * 'cdusuarioManutencao'.
     */
    public void setCdusuarioManutencao(java.lang.String cdusuarioManutencao)
    {
        this._cdusuarioManutencao = cdusuarioManutencao;
    } //-- void setCdusuarioManutencao(java.lang.String) 

    /**
     * Sets the value of field 'codMensagem'.
     * 
     * @param codMensagem the value of field 'codMensagem'.
     */
    public void setCodMensagem(java.lang.String codMensagem)
    {
        this._codMensagem = codMensagem;
    } //-- void setCodMensagem(java.lang.String) 

    /**
     * Sets the value of field 'dsBanco'.
     * 
     * @param dsBanco the value of field 'dsBanco'.
     */
    public void setDsBanco(java.lang.String dsBanco)
    {
        this._dsBanco = dsBanco;
    } //-- void setDsBanco(java.lang.String) 

    /**
     * Sets the value of field 'dsCanalInclusao'.
     * 
     * @param dsCanalInclusao the value of field 'dsCanalInclusao'.
     */
    public void setDsCanalInclusao(java.lang.String dsCanalInclusao)
    {
        this._dsCanalInclusao = dsCanalInclusao;
    } //-- void setDsCanalInclusao(java.lang.String) 

    /**
     * Sets the value of field 'dsCanalManutencao'.
     * 
     * @param dsCanalManutencao the value of field
     * 'dsCanalManutencao'.
     */
    public void setDsCanalManutencao(java.lang.String dsCanalManutencao)
    {
        this._dsCanalManutencao = dsCanalManutencao;
    } //-- void setDsCanalManutencao(java.lang.String) 

    /**
     * Sets the value of field 'dsComercialAgenciaContabil'.
     * 
     * @param dsComercialAgenciaContabil the value of field
     * 'dsComercialAgenciaContabil'.
     */
    public void setDsComercialAgenciaContabil(java.lang.String dsComercialAgenciaContabil)
    {
        this._dsComercialAgenciaContabil = dsComercialAgenciaContabil;
    } //-- void setDsComercialAgenciaContabil(java.lang.String) 

    /**
     * Sets the value of field 'dsIndicadorTipoManutencao'.
     * 
     * @param dsIndicadorTipoManutencao the value of field
     * 'dsIndicadorTipoManutencao'.
     */
    public void setDsIndicadorTipoManutencao(java.lang.String dsIndicadorTipoManutencao)
    {
        this._dsIndicadorTipoManutencao = dsIndicadorTipoManutencao;
    } //-- void setDsIndicadorTipoManutencao(java.lang.String) 

    /**
     * Sets the value of field 'dsMotivoManutencaoContrato'.
     * 
     * @param dsMotivoManutencaoContrato the value of field
     * 'dsMotivoManutencaoContrato'.
     */
    public void setDsMotivoManutencaoContrato(java.lang.String dsMotivoManutencaoContrato)
    {
        this._dsMotivoManutencaoContrato = dsMotivoManutencaoContrato;
    } //-- void setDsMotivoManutencaoContrato(java.lang.String) 

    /**
     * Sets the value of field 'dsSituacaoManutencaoContrato'.
     * 
     * @param dsSituacaoManutencaoContrato the value of field
     * 'dsSituacaoManutencaoContrato'.
     */
    public void setDsSituacaoManutencaoContrato(java.lang.String dsSituacaoManutencaoContrato)
    {
        this._dsSituacaoManutencaoContrato = dsSituacaoManutencaoContrato;
    } //-- void setDsSituacaoManutencaoContrato(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoContaContrato'.
     * 
     * @param dsTipoContaContrato the value of field
     * 'dsTipoContaContrato'.
     */
    public void setDsTipoContaContrato(java.lang.String dsTipoContaContrato)
    {
        this._dsTipoContaContrato = dsTipoContaContrato;
    } //-- void setDsTipoContaContrato(java.lang.String) 

    /**
     * Sets the value of field 'dtVinculo'.
     * 
     * @param dtVinculo the value of field 'dtVinculo'.
     */
    public void setDtVinculo(java.lang.String dtVinculo)
    {
        this._dtVinculo = dtVinculo;
    } //-- void setDtVinculo(java.lang.String) 

    /**
     * Sets the value of field 'hrInclusaoRegistro'.
     * 
     * @param hrInclusaoRegistro the value of field
     * 'hrInclusaoRegistro'.
     */
    public void setHrInclusaoRegistro(java.lang.String hrInclusaoRegistro)
    {
        this._hrInclusaoRegistro = hrInclusaoRegistro;
    } //-- void setHrInclusaoRegistro(java.lang.String) 

    /**
     * Sets the value of field 'hrManutencaoRegistro'.
     * 
     * @param hrManutencaoRegistro the value of field
     * 'hrManutencaoRegistro'.
     */
    public void setHrManutencaoRegistro(java.lang.String hrManutencaoRegistro)
    {
        this._hrManutencaoRegistro = hrManutencaoRegistro;
    } //-- void setHrManutencaoRegistro(java.lang.String) 

    /**
     * Sets the value of field 'mensagem'.
     * 
     * @param mensagem the value of field 'mensagem'.
     */
    public void setMensagem(java.lang.String mensagem)
    {
        this._mensagem = mensagem;
    } //-- void setMensagem(java.lang.String) 

    /**
     * Sets the value of field 'nmPessoa'.
     * 
     * @param nmPessoa the value of field 'nmPessoa'.
     */
    public void setNmPessoa(java.lang.String nmPessoa)
    {
        this._nmPessoa = nmPessoa;
    } //-- void setNmPessoa(java.lang.String) 

    /**
     * Sets the value of field 'nrAditivoContratoNegocio'.
     * 
     * @param nrAditivoContratoNegocio the value of field
     * 'nrAditivoContratoNegocio'.
     */
    public void setNrAditivoContratoNegocio(long nrAditivoContratoNegocio)
    {
        this._nrAditivoContratoNegocio = nrAditivoContratoNegocio;
        this._has_nrAditivoContratoNegocio = true;
    } //-- void setNrAditivoContratoNegocio(long) 

    /**
     * Sets the value of field 'nrConta'.
     * 
     * @param nrConta the value of field 'nrConta'.
     */
    public void setNrConta(long nrConta)
    {
        this._nrConta = nrConta;
        this._has_nrConta = true;
    } //-- void setNrConta(long) 

    /**
     * Sets the value of field 'nrManutencaoContratoNegocio'.
     * 
     * @param nrManutencaoContratoNegocio the value of field
     * 'nrManutencaoContratoNegocio'.
     */
    public void setNrManutencaoContratoNegocio(long nrManutencaoContratoNegocio)
    {
        this._nrManutencaoContratoNegocio = nrManutencaoContratoNegocio;
        this._has_nrManutencaoContratoNegocio = true;
    } //-- void setNrManutencaoContratoNegocio(long) 

    /**
     * Sets the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @param nrSequenciaContratoNegocio the value of field
     * 'nrSequenciaContratoNegocio'.
     */
    public void setNrSequenciaContratoNegocio(long nrSequenciaContratoNegocio)
    {
        this._nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
        this._has_nrSequenciaContratoNegocio = true;
    } //-- void setNrSequenciaContratoNegocio(long) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ConsultarManutencaoContaContratoResponse
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultarmanutencaocontacontrato.response.ConsultarManutencaoContaContratoResponse unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultarmanutencaocontacontrato.response.ConsultarManutencaoContaContratoResponse) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultarmanutencaocontacontrato.response.ConsultarManutencaoContaContratoResponse.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarmanutencaocontacontrato.response.ConsultarManutencaoContaContratoResponse unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
