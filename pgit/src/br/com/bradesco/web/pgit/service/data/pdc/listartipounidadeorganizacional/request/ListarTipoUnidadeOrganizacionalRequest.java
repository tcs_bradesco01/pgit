/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listartipounidadeorganizacional.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ListarTipoUnidadeOrganizacionalRequest.
 * 
 * @version $Revision$ $Date$
 */
public class ListarTipoUnidadeOrganizacionalRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _maximoOcorrencias
     */
    private int _maximoOcorrencias = 0;

    /**
     * keeps track of state for field: _maximoOcorrencias
     */
    private boolean _has_maximoOcorrencias;

    /**
     * Field _cdSituacaoVinculacaoConta
     */
    private int _cdSituacaoVinculacaoConta = 0;

    /**
     * keeps track of state for field: _cdSituacaoVinculacaoConta
     */
    private boolean _has_cdSituacaoVinculacaoConta;


      //----------------/
     //- Constructors -/
    //----------------/

    public ListarTipoUnidadeOrganizacionalRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listartipounidadeorganizacional.request.ListarTipoUnidadeOrganizacionalRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdSituacaoVinculacaoConta
     * 
     */
    public void deleteCdSituacaoVinculacaoConta()
    {
        this._has_cdSituacaoVinculacaoConta= false;
    } //-- void deleteCdSituacaoVinculacaoConta() 

    /**
     * Method deleteMaximoOcorrencias
     * 
     */
    public void deleteMaximoOcorrencias()
    {
        this._has_maximoOcorrencias= false;
    } //-- void deleteMaximoOcorrencias() 

    /**
     * Returns the value of field 'cdSituacaoVinculacaoConta'.
     * 
     * @return int
     * @return the value of field 'cdSituacaoVinculacaoConta'.
     */
    public int getCdSituacaoVinculacaoConta()
    {
        return this._cdSituacaoVinculacaoConta;
    } //-- int getCdSituacaoVinculacaoConta() 

    /**
     * Returns the value of field 'maximoOcorrencias'.
     * 
     * @return int
     * @return the value of field 'maximoOcorrencias'.
     */
    public int getMaximoOcorrencias()
    {
        return this._maximoOcorrencias;
    } //-- int getMaximoOcorrencias() 

    /**
     * Method hasCdSituacaoVinculacaoConta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSituacaoVinculacaoConta()
    {
        return this._has_cdSituacaoVinculacaoConta;
    } //-- boolean hasCdSituacaoVinculacaoConta() 

    /**
     * Method hasMaximoOcorrencias
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasMaximoOcorrencias()
    {
        return this._has_maximoOcorrencias;
    } //-- boolean hasMaximoOcorrencias() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdSituacaoVinculacaoConta'.
     * 
     * @param cdSituacaoVinculacaoConta the value of field
     * 'cdSituacaoVinculacaoConta'.
     */
    public void setCdSituacaoVinculacaoConta(int cdSituacaoVinculacaoConta)
    {
        this._cdSituacaoVinculacaoConta = cdSituacaoVinculacaoConta;
        this._has_cdSituacaoVinculacaoConta = true;
    } //-- void setCdSituacaoVinculacaoConta(int) 

    /**
     * Sets the value of field 'maximoOcorrencias'.
     * 
     * @param maximoOcorrencias the value of field
     * 'maximoOcorrencias'.
     */
    public void setMaximoOcorrencias(int maximoOcorrencias)
    {
        this._maximoOcorrencias = maximoOcorrencias;
        this._has_maximoOcorrencias = true;
    } //-- void setMaximoOcorrencias(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ListarTipoUnidadeOrganizacionalRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listartipounidadeorganizacional.request.ListarTipoUnidadeOrganizacionalRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listartipounidadeorganizacional.request.ListarTipoUnidadeOrganizacionalRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listartipounidadeorganizacional.request.ListarTipoUnidadeOrganizacionalRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listartipounidadeorganizacional.request.ListarTipoUnidadeOrganizacionalRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
