/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consutarcontrenvioformaliquidacao.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.util.Enumeration;
import java.util.Vector;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ConsutarContrEnvioFormaLiquidacaoResponse.
 * 
 * @version $Revision$ $Date$
 */
public class ConsutarContrEnvioFormaLiquidacaoResponse implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _codMensagem
     */
    private java.lang.String _codMensagem;

    /**
     * Field _mensagem
     */
    private java.lang.String _mensagem;

    /**
     * Field _numeroLinhas
     */
    private int _numeroLinhas = 0;

    /**
     * keeps track of state for field: _numeroLinhas
     */
    private boolean _has_numeroLinhas;

    /**
     * Field _ocorrenicasList
     */
    private java.util.Vector _ocorrenicasList;


      //----------------/
     //- Constructors -/
    //----------------/

    public ConsutarContrEnvioFormaLiquidacaoResponse() 
     {
        super();
        _ocorrenicasList = new Vector();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consutarcontrenvioformaliquidacao.response.ConsutarContrEnvioFormaLiquidacaoResponse()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method addOcorrenicas
     * 
     * 
     * 
     * @param vOcorrenicas
     */
    public void addOcorrenicas(br.com.bradesco.web.pgit.service.data.pdc.consutarcontrenvioformaliquidacao.response.Ocorrenicas vOcorrenicas)
        throws java.lang.IndexOutOfBoundsException
    {
        _ocorrenicasList.addElement(vOcorrenicas);
    } //-- void addOcorrenicas(br.com.bradesco.web.pgit.service.data.pdc.consutarcontrenvioformaliquidacao.response.Ocorrenicas) 

    /**
     * Method addOcorrenicas
     * 
     * 
     * 
     * @param index
     * @param vOcorrenicas
     */
    public void addOcorrenicas(int index, br.com.bradesco.web.pgit.service.data.pdc.consutarcontrenvioformaliquidacao.response.Ocorrenicas vOcorrenicas)
        throws java.lang.IndexOutOfBoundsException
    {
        _ocorrenicasList.insertElementAt(vOcorrenicas, index);
    } //-- void addOcorrenicas(int, br.com.bradesco.web.pgit.service.data.pdc.consutarcontrenvioformaliquidacao.response.Ocorrenicas) 

    /**
     * Method deleteNumeroLinhas
     * 
     */
    public void deleteNumeroLinhas()
    {
        this._has_numeroLinhas= false;
    } //-- void deleteNumeroLinhas() 

    /**
     * Method enumerateOcorrenicas
     * 
     * 
     * 
     * @return Enumeration
     */
    public java.util.Enumeration enumerateOcorrenicas()
    {
        return _ocorrenicasList.elements();
    } //-- java.util.Enumeration enumerateOcorrenicas() 

    /**
     * Returns the value of field 'codMensagem'.
     * 
     * @return String
     * @return the value of field 'codMensagem'.
     */
    public java.lang.String getCodMensagem()
    {
        return this._codMensagem;
    } //-- java.lang.String getCodMensagem() 

    /**
     * Returns the value of field 'mensagem'.
     * 
     * @return String
     * @return the value of field 'mensagem'.
     */
    public java.lang.String getMensagem()
    {
        return this._mensagem;
    } //-- java.lang.String getMensagem() 

    /**
     * Returns the value of field 'numeroLinhas'.
     * 
     * @return int
     * @return the value of field 'numeroLinhas'.
     */
    public int getNumeroLinhas()
    {
        return this._numeroLinhas;
    } //-- int getNumeroLinhas() 

    /**
     * Method getOcorrenicas
     * 
     * 
     * 
     * @param index
     * @return Ocorrenicas
     */
    public br.com.bradesco.web.pgit.service.data.pdc.consutarcontrenvioformaliquidacao.response.Ocorrenicas getOcorrenicas(int index)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index >= _ocorrenicasList.size())) {
            throw new IndexOutOfBoundsException("getOcorrenicas: Index value '"+index+"' not in range [0.."+(_ocorrenicasList.size() - 1) + "]");
        }
        
        return (br.com.bradesco.web.pgit.service.data.pdc.consutarcontrenvioformaliquidacao.response.Ocorrenicas) _ocorrenicasList.elementAt(index);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consutarcontrenvioformaliquidacao.response.Ocorrenicas getOcorrenicas(int) 

    /**
     * Method getOcorrenicas
     * 
     * 
     * 
     * @return Ocorrenicas
     */
    public br.com.bradesco.web.pgit.service.data.pdc.consutarcontrenvioformaliquidacao.response.Ocorrenicas[] getOcorrenicas()
    {
        int size = _ocorrenicasList.size();
        br.com.bradesco.web.pgit.service.data.pdc.consutarcontrenvioformaliquidacao.response.Ocorrenicas[] mArray = new br.com.bradesco.web.pgit.service.data.pdc.consutarcontrenvioformaliquidacao.response.Ocorrenicas[size];
        for (int index = 0; index < size; index++) {
            mArray[index] = (br.com.bradesco.web.pgit.service.data.pdc.consutarcontrenvioformaliquidacao.response.Ocorrenicas) _ocorrenicasList.elementAt(index);
        }
        return mArray;
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consutarcontrenvioformaliquidacao.response.Ocorrenicas[] getOcorrenicas() 

    /**
     * Method getOcorrenicasCount
     * 
     * 
     * 
     * @return int
     */
    public int getOcorrenicasCount()
    {
        return _ocorrenicasList.size();
    } //-- int getOcorrenicasCount() 

    /**
     * Method hasNumeroLinhas
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNumeroLinhas()
    {
        return this._has_numeroLinhas;
    } //-- boolean hasNumeroLinhas() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Method removeAllOcorrenicas
     * 
     */
    public void removeAllOcorrenicas()
    {
        _ocorrenicasList.removeAllElements();
    } //-- void removeAllOcorrenicas() 

    /**
     * Method removeOcorrenicas
     * 
     * 
     * 
     * @param index
     * @return Ocorrenicas
     */
    public br.com.bradesco.web.pgit.service.data.pdc.consutarcontrenvioformaliquidacao.response.Ocorrenicas removeOcorrenicas(int index)
    {
        java.lang.Object obj = _ocorrenicasList.elementAt(index);
        _ocorrenicasList.removeElementAt(index);
        return (br.com.bradesco.web.pgit.service.data.pdc.consutarcontrenvioformaliquidacao.response.Ocorrenicas) obj;
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consutarcontrenvioformaliquidacao.response.Ocorrenicas removeOcorrenicas(int) 

    /**
     * Sets the value of field 'codMensagem'.
     * 
     * @param codMensagem the value of field 'codMensagem'.
     */
    public void setCodMensagem(java.lang.String codMensagem)
    {
        this._codMensagem = codMensagem;
    } //-- void setCodMensagem(java.lang.String) 

    /**
     * Sets the value of field 'mensagem'.
     * 
     * @param mensagem the value of field 'mensagem'.
     */
    public void setMensagem(java.lang.String mensagem)
    {
        this._mensagem = mensagem;
    } //-- void setMensagem(java.lang.String) 

    /**
     * Sets the value of field 'numeroLinhas'.
     * 
     * @param numeroLinhas the value of field 'numeroLinhas'.
     */
    public void setNumeroLinhas(int numeroLinhas)
    {
        this._numeroLinhas = numeroLinhas;
        this._has_numeroLinhas = true;
    } //-- void setNumeroLinhas(int) 

    /**
     * Method setOcorrenicas
     * 
     * 
     * 
     * @param index
     * @param vOcorrenicas
     */
    public void setOcorrenicas(int index, br.com.bradesco.web.pgit.service.data.pdc.consutarcontrenvioformaliquidacao.response.Ocorrenicas vOcorrenicas)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index >= _ocorrenicasList.size())) {
            throw new IndexOutOfBoundsException("setOcorrenicas: Index value '"+index+"' not in range [0.." + (_ocorrenicasList.size() - 1) + "]");
        }
        _ocorrenicasList.setElementAt(vOcorrenicas, index);
    } //-- void setOcorrenicas(int, br.com.bradesco.web.pgit.service.data.pdc.consutarcontrenvioformaliquidacao.response.Ocorrenicas) 

    /**
     * Method setOcorrenicas
     * 
     * 
     * 
     * @param ocorrenicasArray
     */
    public void setOcorrenicas(br.com.bradesco.web.pgit.service.data.pdc.consutarcontrenvioformaliquidacao.response.Ocorrenicas[] ocorrenicasArray)
    {
        //-- copy array
        _ocorrenicasList.removeAllElements();
        for (int i = 0; i < ocorrenicasArray.length; i++) {
            _ocorrenicasList.addElement(ocorrenicasArray[i]);
        }
    } //-- void setOcorrenicas(br.com.bradesco.web.pgit.service.data.pdc.consutarcontrenvioformaliquidacao.response.Ocorrenicas) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ConsutarContrEnvioFormaLiquidacaoResponse
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consutarcontrenvioformaliquidacao.response.ConsutarContrEnvioFormaLiquidacaoResponse unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consutarcontrenvioformaliquidacao.response.ConsutarContrEnvioFormaLiquidacaoResponse) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consutarcontrenvioformaliquidacao.response.ConsutarContrEnvioFormaLiquidacaoResponse.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consutarcontrenvioformaliquidacao.response.ConsutarContrEnvioFormaLiquidacaoResponse unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
