/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listarremessa.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ListarRemessaRequest.
 * 
 * @version $Revision$ $Date$
 */
public class ListarRemessaRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _numeroOcorrencias
     */
    private int _numeroOcorrencias = 0;

    /**
     * keeps track of state for field: _numeroOcorrencias
     */
    private boolean _has_numeroOcorrencias;

    /**
     * Field _horaRecepInicio
     */
    private java.lang.String _horaRecepInicio;

    /**
     * Field _horaRecepFim
     */
    private java.lang.String _horaRecepFim;

    /**
     * Field _centroCusto
     */
    private java.lang.String _centroCusto;

    /**
     * Field _cpfCnpj
     */
    private long _cpfCnpj = 0;

    /**
     * keeps track of state for field: _cpfCnpj
     */
    private boolean _has_cpfCnpj;

    /**
     * Field _filial
     */
    private int _filial = 0;

    /**
     * keeps track of state for field: _filial
     */
    private boolean _has_filial;

    /**
     * Field _controle
     */
    private int _controle = 0;

    /**
     * keeps track of state for field: _controle
     */
    private boolean _has_controle;

    /**
     * Field _perfilComunicacao
     */
    private long _perfilComunicacao = 0;

    /**
     * keeps track of state for field: _perfilComunicacao
     */
    private boolean _has_perfilComunicacao;

    /**
     * Field _numeroArquivoRemessa
     */
    private long _numeroArquivoRemessa = 0;

    /**
     * keeps track of state for field: _numeroArquivoRemessa
     */
    private boolean _has_numeroArquivoRemessa;

    /**
     * Field _situacaoProcessamento
     */
    private int _situacaoProcessamento = 0;

    /**
     * keeps track of state for field: _situacaoProcessamento
     */
    private boolean _has_situacaoProcessamento;

    /**
     * Field _processamentoRemessa
     */
    private int _processamentoRemessa = 0;

    /**
     * keeps track of state for field: _processamentoRemessa
     */
    private boolean _has_processamentoRemessa;


      //----------------/
     //- Constructors -/
    //----------------/

    public ListarRemessaRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarremessa.request.ListarRemessaRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteControle
     * 
     */
    public void deleteControle()
    {
        this._has_controle= false;
    } //-- void deleteControle() 

    /**
     * Method deleteCpfCnpj
     * 
     */
    public void deleteCpfCnpj()
    {
        this._has_cpfCnpj= false;
    } //-- void deleteCpfCnpj() 

    /**
     * Method deleteFilial
     * 
     */
    public void deleteFilial()
    {
        this._has_filial= false;
    } //-- void deleteFilial() 

    /**
     * Method deleteNumeroArquivoRemessa
     * 
     */
    public void deleteNumeroArquivoRemessa()
    {
        this._has_numeroArquivoRemessa= false;
    } //-- void deleteNumeroArquivoRemessa() 

    /**
     * Method deleteNumeroOcorrencias
     * 
     */
    public void deleteNumeroOcorrencias()
    {
        this._has_numeroOcorrencias= false;
    } //-- void deleteNumeroOcorrencias() 

    /**
     * Method deletePerfilComunicacao
     * 
     */
    public void deletePerfilComunicacao()
    {
        this._has_perfilComunicacao= false;
    } //-- void deletePerfilComunicacao() 

    /**
     * Method deleteProcessamentoRemessa
     * 
     */
    public void deleteProcessamentoRemessa()
    {
        this._has_processamentoRemessa= false;
    } //-- void deleteProcessamentoRemessa() 

    /**
     * Method deleteSituacaoProcessamento
     * 
     */
    public void deleteSituacaoProcessamento()
    {
        this._has_situacaoProcessamento= false;
    } //-- void deleteSituacaoProcessamento() 

    /**
     * Returns the value of field 'centroCusto'.
     * 
     * @return String
     * @return the value of field 'centroCusto'.
     */
    public java.lang.String getCentroCusto()
    {
        return this._centroCusto;
    } //-- java.lang.String getCentroCusto() 

    /**
     * Returns the value of field 'controle'.
     * 
     * @return int
     * @return the value of field 'controle'.
     */
    public int getControle()
    {
        return this._controle;
    } //-- int getControle() 

    /**
     * Returns the value of field 'cpfCnpj'.
     * 
     * @return long
     * @return the value of field 'cpfCnpj'.
     */
    public long getCpfCnpj()
    {
        return this._cpfCnpj;
    } //-- long getCpfCnpj() 

    /**
     * Returns the value of field 'filial'.
     * 
     * @return int
     * @return the value of field 'filial'.
     */
    public int getFilial()
    {
        return this._filial;
    } //-- int getFilial() 

    /**
     * Returns the value of field 'horaRecepFim'.
     * 
     * @return String
     * @return the value of field 'horaRecepFim'.
     */
    public java.lang.String getHoraRecepFim()
    {
        return this._horaRecepFim;
    } //-- java.lang.String getHoraRecepFim() 

    /**
     * Returns the value of field 'horaRecepInicio'.
     * 
     * @return String
     * @return the value of field 'horaRecepInicio'.
     */
    public java.lang.String getHoraRecepInicio()
    {
        return this._horaRecepInicio;
    } //-- java.lang.String getHoraRecepInicio() 

    /**
     * Returns the value of field 'numeroArquivoRemessa'.
     * 
     * @return long
     * @return the value of field 'numeroArquivoRemessa'.
     */
    public long getNumeroArquivoRemessa()
    {
        return this._numeroArquivoRemessa;
    } //-- long getNumeroArquivoRemessa() 

    /**
     * Returns the value of field 'numeroOcorrencias'.
     * 
     * @return int
     * @return the value of field 'numeroOcorrencias'.
     */
    public int getNumeroOcorrencias()
    {
        return this._numeroOcorrencias;
    } //-- int getNumeroOcorrencias() 

    /**
     * Returns the value of field 'perfilComunicacao'.
     * 
     * @return long
     * @return the value of field 'perfilComunicacao'.
     */
    public long getPerfilComunicacao()
    {
        return this._perfilComunicacao;
    } //-- long getPerfilComunicacao() 

    /**
     * Returns the value of field 'processamentoRemessa'.
     * 
     * @return int
     * @return the value of field 'processamentoRemessa'.
     */
    public int getProcessamentoRemessa()
    {
        return this._processamentoRemessa;
    } //-- int getProcessamentoRemessa() 

    /**
     * Returns the value of field 'situacaoProcessamento'.
     * 
     * @return int
     * @return the value of field 'situacaoProcessamento'.
     */
    public int getSituacaoProcessamento()
    {
        return this._situacaoProcessamento;
    } //-- int getSituacaoProcessamento() 

    /**
     * Method hasControle
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasControle()
    {
        return this._has_controle;
    } //-- boolean hasControle() 

    /**
     * Method hasCpfCnpj
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCpfCnpj()
    {
        return this._has_cpfCnpj;
    } //-- boolean hasCpfCnpj() 

    /**
     * Method hasFilial
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasFilial()
    {
        return this._has_filial;
    } //-- boolean hasFilial() 

    /**
     * Method hasNumeroArquivoRemessa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNumeroArquivoRemessa()
    {
        return this._has_numeroArquivoRemessa;
    } //-- boolean hasNumeroArquivoRemessa() 

    /**
     * Method hasNumeroOcorrencias
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNumeroOcorrencias()
    {
        return this._has_numeroOcorrencias;
    } //-- boolean hasNumeroOcorrencias() 

    /**
     * Method hasPerfilComunicacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasPerfilComunicacao()
    {
        return this._has_perfilComunicacao;
    } //-- boolean hasPerfilComunicacao() 

    /**
     * Method hasProcessamentoRemessa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasProcessamentoRemessa()
    {
        return this._has_processamentoRemessa;
    } //-- boolean hasProcessamentoRemessa() 

    /**
     * Method hasSituacaoProcessamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasSituacaoProcessamento()
    {
        return this._has_situacaoProcessamento;
    } //-- boolean hasSituacaoProcessamento() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'centroCusto'.
     * 
     * @param centroCusto the value of field 'centroCusto'.
     */
    public void setCentroCusto(java.lang.String centroCusto)
    {
        this._centroCusto = centroCusto;
    } //-- void setCentroCusto(java.lang.String) 

    /**
     * Sets the value of field 'controle'.
     * 
     * @param controle the value of field 'controle'.
     */
    public void setControle(int controle)
    {
        this._controle = controle;
        this._has_controle = true;
    } //-- void setControle(int) 

    /**
     * Sets the value of field 'cpfCnpj'.
     * 
     * @param cpfCnpj the value of field 'cpfCnpj'.
     */
    public void setCpfCnpj(long cpfCnpj)
    {
        this._cpfCnpj = cpfCnpj;
        this._has_cpfCnpj = true;
    } //-- void setCpfCnpj(long) 

    /**
     * Sets the value of field 'filial'.
     * 
     * @param filial the value of field 'filial'.
     */
    public void setFilial(int filial)
    {
        this._filial = filial;
        this._has_filial = true;
    } //-- void setFilial(int) 

    /**
     * Sets the value of field 'horaRecepFim'.
     * 
     * @param horaRecepFim the value of field 'horaRecepFim'.
     */
    public void setHoraRecepFim(java.lang.String horaRecepFim)
    {
        this._horaRecepFim = horaRecepFim;
    } //-- void setHoraRecepFim(java.lang.String) 

    /**
     * Sets the value of field 'horaRecepInicio'.
     * 
     * @param horaRecepInicio the value of field 'horaRecepInicio'.
     */
    public void setHoraRecepInicio(java.lang.String horaRecepInicio)
    {
        this._horaRecepInicio = horaRecepInicio;
    } //-- void setHoraRecepInicio(java.lang.String) 

    /**
     * Sets the value of field 'numeroArquivoRemessa'.
     * 
     * @param numeroArquivoRemessa the value of field
     * 'numeroArquivoRemessa'.
     */
    public void setNumeroArquivoRemessa(long numeroArquivoRemessa)
    {
        this._numeroArquivoRemessa = numeroArquivoRemessa;
        this._has_numeroArquivoRemessa = true;
    } //-- void setNumeroArquivoRemessa(long) 

    /**
     * Sets the value of field 'numeroOcorrencias'.
     * 
     * @param numeroOcorrencias the value of field
     * 'numeroOcorrencias'.
     */
    public void setNumeroOcorrencias(int numeroOcorrencias)
    {
        this._numeroOcorrencias = numeroOcorrencias;
        this._has_numeroOcorrencias = true;
    } //-- void setNumeroOcorrencias(int) 

    /**
     * Sets the value of field 'perfilComunicacao'.
     * 
     * @param perfilComunicacao the value of field
     * 'perfilComunicacao'.
     */
    public void setPerfilComunicacao(long perfilComunicacao)
    {
        this._perfilComunicacao = perfilComunicacao;
        this._has_perfilComunicacao = true;
    } //-- void setPerfilComunicacao(long) 

    /**
     * Sets the value of field 'processamentoRemessa'.
     * 
     * @param processamentoRemessa the value of field
     * 'processamentoRemessa'.
     */
    public void setProcessamentoRemessa(int processamentoRemessa)
    {
        this._processamentoRemessa = processamentoRemessa;
        this._has_processamentoRemessa = true;
    } //-- void setProcessamentoRemessa(int) 

    /**
     * Sets the value of field 'situacaoProcessamento'.
     * 
     * @param situacaoProcessamento the value of field
     * 'situacaoProcessamento'.
     */
    public void setSituacaoProcessamento(int situacaoProcessamento)
    {
        this._situacaoProcessamento = situacaoProcessamento;
        this._has_situacaoProcessamento = true;
    } //-- void setSituacaoProcessamento(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ListarRemessaRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listarremessa.request.ListarRemessaRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listarremessa.request.ListarRemessaRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listarremessa.request.ListarRemessaRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarremessa.request.ListarRemessaRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
