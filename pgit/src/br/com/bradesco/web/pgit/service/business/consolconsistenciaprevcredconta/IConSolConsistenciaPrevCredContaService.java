/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/interfaz.ftl,v $
 * $Id: interfaz.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: interfaz.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.consolconsistenciaprevcredconta;

import java.util.List;

import br.com.bradesco.web.pgit.service.business.consolconsistenciaprevcredconta.bean.AprovarSolicitacaoConsistPreviaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consolconsistenciaprevcredconta.bean.AprovarSolicitacaoConsistPreviaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consolconsistenciaprevcredconta.bean.ConsultarSolicitacaoConsistPreviaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consolconsistenciaprevcredconta.bean.ConsultarSolicitacaoConsistPreviaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consolconsistenciaprevcredconta.bean.RejeitarSolicitacaoConsistPreviaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consolconsistenciaprevcredconta.bean.RejeitarSolicitacaoConsistPreviaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consolconsistenciaprevcredconta.bean.ReprocessarSolicitacaoConsistPreviaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consolconsistenciaprevcredconta.bean.ReprocessarSolicitacaoConsistPreviaSaidaDTO;

/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Interface do adaptador: ConSolConsistenciaPrevCredConta
 * </p>
 * 
 * @comment CODIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public interface IConSolConsistenciaPrevCredContaService {

		
	/**
	 * Reprocessar solicitacao consist previa.
	 *
	 * @param reprocessarSolicitacaoConsistPreviaEntradaDTO the reprocessar solicitacao consist previa entrada dto
	 * @return the reprocessar solicitacao consist previa saida dto
	 */
	ReprocessarSolicitacaoConsistPreviaSaidaDTO reprocessarSolicitacaoConsistPrevia(ReprocessarSolicitacaoConsistPreviaEntradaDTO reprocessarSolicitacaoConsistPreviaEntradaDTO);
	
	/**
	 * Aprovar solicitacao consist previa.
	 *
	 * @param aprovarSolicitacaoConsistPreviaEntradaDTO the aprovar solicitacao consist previa entrada dto
	 * @return the aprovar solicitacao consist previa saida dto
	 */
	AprovarSolicitacaoConsistPreviaSaidaDTO aprovarSolicitacaoConsistPrevia(AprovarSolicitacaoConsistPreviaEntradaDTO aprovarSolicitacaoConsistPreviaEntradaDTO);
	
	/**
	 * Rejeitar solicitacao consist previa.
	 *
	 * @param rejeitarSolicitacaoConsistPreviaEntradaDTO the rejeitar solicitacao consist previa entrada dto
	 * @return the rejeitar solicitacao consist previa saida dto
	 */
	RejeitarSolicitacaoConsistPreviaSaidaDTO rejeitarSolicitacaoConsistPrevia(RejeitarSolicitacaoConsistPreviaEntradaDTO rejeitarSolicitacaoConsistPreviaEntradaDTO);
	
	/**
	 * Consultar solicitacao consist previa.
	 *
	 * @param consultarSolicitacaoConsistPreviaEntradaDTO the consultar solicitacao consist previa entrada dto
	 * @return the list< consultar solicitacao consist previa saida dt o>
	 */
	List<ConsultarSolicitacaoConsistPreviaSaidaDTO> consultarSolicitacaoConsistPrevia(ConsultarSolicitacaoConsistPreviaEntradaDTO consultarSolicitacaoConsistPreviaEntradaDTO);
}

