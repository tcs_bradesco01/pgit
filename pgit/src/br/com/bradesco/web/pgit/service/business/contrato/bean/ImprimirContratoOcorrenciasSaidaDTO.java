/*
 * Nome: br.com.bradesco.web.pgit.service.business.contrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 19/02/2016
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.contrato.bean;


/**
 * Nome: ImprimirContratoOcorrenciasSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ImprimirContratoOcorrenciasSaidaDTO {

	/** The cd indicador fornecedor contrato. */
	private String cdIndicadorFornecedorContrato = null;
	
    /** The cd indicador modalidade fornecedor. */
    private Integer cdIndicadorModalidadeFornecedor = null;

    /** The cd produto relacionado fornecedor. */
    private Integer cdProdutoRelacionadoFornecedor = null;

    /** The ds produto relacionado fornecedor. */
    private String dsProdutoRelacionadoFornecedor = null;

    /** The cd momento processamento fornecedor. */
    private Integer cdMomentoProcessamentoFornecedor = null;

    /** The ds momento processamento fornecedor. */
    private String dsMomentoProcessamentoFornecedor = null;

    /** The cd pagamento util fornecedor. */
    private Integer cdPagamentoUtilFornecedor = null;

    /** The ds pagamento util fornecedor. */
    private String dsPagamentoUtilFornecedor = null;

    /** The nr quantidade dias float fornecedor. */
    private Integer nrQuantidadeDiasFloatFornecedor = null;

    /** The cd indicador feriado fornecedor. */
    private Integer cdIndicadorFeriadoFornecedor = null;

    /** The ds indicador feriado fornecedor. */
    private String dsIndicadorFeriadoFornecedor = null;

    /**
     * Instancia um novo ImprimirContratoOcorrenciasSaidaDTO
     *
     * @see
     */
    public ImprimirContratoOcorrenciasSaidaDTO() {
        super();
    }

    /**
     * Sets the cd indicador modalidade fornecedor.
     *
     * @param cdIndicadorModalidadeFornecedor the cd indicador modalidade fornecedor
     */
    public void setCdIndicadorModalidadeFornecedor(Integer cdIndicadorModalidadeFornecedor) {
        this.cdIndicadorModalidadeFornecedor = cdIndicadorModalidadeFornecedor;
    }

    /**
     * Gets the cd indicador modalidade fornecedor.
     *
     * @return the cd indicador modalidade fornecedor
     */
    public Integer getCdIndicadorModalidadeFornecedor() {
        return this.cdIndicadorModalidadeFornecedor;
    }

    /**
     * Sets the cd produto relacionado fornecedor.
     *
     * @param cdProdutoRelacionadoFornecedor the cd produto relacionado fornecedor
     */
    public void setCdProdutoRelacionadoFornecedor(Integer cdProdutoRelacionadoFornecedor) {
        this.cdProdutoRelacionadoFornecedor = cdProdutoRelacionadoFornecedor;
    }

    /**
     * Gets the cd produto relacionado fornecedor.
     *
     * @return the cd produto relacionado fornecedor
     */
    public Integer getCdProdutoRelacionadoFornecedor() {
        return this.cdProdutoRelacionadoFornecedor;
    }

    /**
     * Sets the ds produto relacionado fornecedor.
     *
     * @param dsProdutoRelacionadoFornecedor the ds produto relacionado fornecedor
     */
    public void setDsProdutoRelacionadoFornecedor(String dsProdutoRelacionadoFornecedor) {
        this.dsProdutoRelacionadoFornecedor = dsProdutoRelacionadoFornecedor;
    }

    /**
     * Gets the ds produto relacionado fornecedor.
     *
     * @return the ds produto relacionado fornecedor
     */
    public String getDsProdutoRelacionadoFornecedor() {
        return this.dsProdutoRelacionadoFornecedor;
    }

    /**
     * Sets the cd momento processamento fornecedor.
     *
     * @param cdMomentoProcessamentoFornecedor the cd momento processamento fornecedor
     */
    public void setCdMomentoProcessamentoFornecedor(Integer cdMomentoProcessamentoFornecedor) {
        this.cdMomentoProcessamentoFornecedor = cdMomentoProcessamentoFornecedor;
    }

    /**
     * Gets the cd momento processamento fornecedor.
     *
     * @return the cd momento processamento fornecedor
     */
    public Integer getCdMomentoProcessamentoFornecedor() {
        return this.cdMomentoProcessamentoFornecedor;
    }

    /**
     * Sets the ds momento processamento fornecedor.
     *
     * @param dsMomentoProcessamentoFornecedor the ds momento processamento fornecedor
     */
    public void setDsMomentoProcessamentoFornecedor(String dsMomentoProcessamentoFornecedor) {
        this.dsMomentoProcessamentoFornecedor = dsMomentoProcessamentoFornecedor;
    }

    /**
     * Gets the ds momento processamento fornecedor.
     *
     * @return the ds momento processamento fornecedor
     */
    public String getDsMomentoProcessamentoFornecedor() {
        return this.dsMomentoProcessamentoFornecedor;
    }

    /**
     * Sets the cd pagamento util fornecedor.
     *
     * @param cdPagamentoUtilFornecedor the cd pagamento util fornecedor
     */
    public void setCdPagamentoUtilFornecedor(Integer cdPagamentoUtilFornecedor) {
        this.cdPagamentoUtilFornecedor = cdPagamentoUtilFornecedor;
    }

    /**
     * Gets the cd pagamento util fornecedor.
     *
     * @return the cd pagamento util fornecedor
     */
    public Integer getCdPagamentoUtilFornecedor() {
        return this.cdPagamentoUtilFornecedor;
    }

    /**
     * Sets the ds pagamento util fornecedor.
     *
     * @param dsPagamentoUtilFornecedor the ds pagamento util fornecedor
     */
    public void setDsPagamentoUtilFornecedor(String dsPagamentoUtilFornecedor) {
        this.dsPagamentoUtilFornecedor = dsPagamentoUtilFornecedor;
    }

    /**
     * Gets the ds pagamento util fornecedor.
     *
     * @return the ds pagamento util fornecedor
     */
    public String getDsPagamentoUtilFornecedor() {
        return this.dsPagamentoUtilFornecedor;
    }

    /**
     * Sets the nr quantidade dias float fornecedor.
     *
     * @param nrQuantidadeDiasFloatFornecedor the nr quantidade dias float fornecedor
     */
    public void setNrQuantidadeDiasFloatFornecedor(Integer nrQuantidadeDiasFloatFornecedor) {
        this.nrQuantidadeDiasFloatFornecedor = nrQuantidadeDiasFloatFornecedor;
    }

    /**
     * Gets the nr quantidade dias float fornecedor.
     *
     * @return the nr quantidade dias float fornecedor
     */
    public Integer getNrQuantidadeDiasFloatFornecedor() {
        return this.nrQuantidadeDiasFloatFornecedor;
    }

    /**
     * Sets the cd indicador feriado fornecedor.
     *
     * @param cdIndicadorFeriadoFornecedor the cd indicador feriado fornecedor
     */
    public void setCdIndicadorFeriadoFornecedor(Integer cdIndicadorFeriadoFornecedor) {
        this.cdIndicadorFeriadoFornecedor = cdIndicadorFeriadoFornecedor;
    }

    /**
     * Gets the cd indicador feriado fornecedor.
     *
     * @return the cd indicador feriado fornecedor
     */
    public Integer getCdIndicadorFeriadoFornecedor() {
        return this.cdIndicadorFeriadoFornecedor;
    }

    /**
     * Sets the ds indicador feriado fornecedor.
     *
     * @param dsIndicadorFeriadoFornecedor the ds indicador feriado fornecedor
     */
    public void setDsIndicadorFeriadoFornecedor(String dsIndicadorFeriadoFornecedor) {
        this.dsIndicadorFeriadoFornecedor = dsIndicadorFeriadoFornecedor;
    }

    /**
     * Gets the ds indicador feriado fornecedor.
     *
     * @return the ds indicador feriado fornecedor
     */
    public String getDsIndicadorFeriadoFornecedor() {
        return this.dsIndicadorFeriadoFornecedor;
    }

	/**
	 * Gets the cd indicador fornecedor contrato.
	 *
	 * @return the cd indicador fornecedor contrato
	 */
	public String getCdIndicadorFornecedorContrato() {
		return cdIndicadorFornecedorContrato;
	}

	/**
	 * Sets the cd indicador fornecedor contrato.
	 *
	 * @param cdIndicadorFornecedorContrato the new cd indicador fornecedor contrato
	 */
	public void setCdIndicadorFornecedorContrato(
			String cdIndicadorFornecedorContrato) {
		this.cdIndicadorFornecedorContrato = cdIndicadorFornecedorContrato;
	}
}