/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultarpagtosindantpostergacao.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.math.BigDecimal;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _nrCnpjCpf
     */
    private long _nrCnpjCpf = 0;

    /**
     * keeps track of state for field: _nrCnpjCpf
     */
    private boolean _has_nrCnpjCpf;

    /**
     * Field _nrFilialCnpjCpf
     */
    private int _nrFilialCnpjCpf = 0;

    /**
     * keeps track of state for field: _nrFilialCnpjCpf
     */
    private boolean _has_nrFilialCnpjCpf;

    /**
     * Field _nrDigitoCnpjCpf
     */
    private int _nrDigitoCnpjCpf = 0;

    /**
     * keeps track of state for field: _nrDigitoCnpjCpf
     */
    private boolean _has_nrDigitoCnpjCpf;

    /**
     * Field _cdPessoaJuridicaContrato
     */
    private long _cdPessoaJuridicaContrato = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridicaContrato
     */
    private boolean _has_cdPessoaJuridicaContrato;

    /**
     * Field _dsRazaoSocial
     */
    private java.lang.String _dsRazaoSocial;

    /**
     * Field _cdEmpresa
     */
    private long _cdEmpresa = 0;

    /**
     * keeps track of state for field: _cdEmpresa
     */
    private boolean _has_cdEmpresa;

    /**
     * Field _dsEmpresa
     */
    private java.lang.String _dsEmpresa;

    /**
     * Field _cdTipoContratoNegocio
     */
    private int _cdTipoContratoNegocio = 0;

    /**
     * keeps track of state for field: _cdTipoContratoNegocio
     */
    private boolean _has_cdTipoContratoNegocio;

    /**
     * Field _nrContrato
     */
    private long _nrContrato = 0;

    /**
     * keeps track of state for field: _nrContrato
     */
    private boolean _has_nrContrato;

    /**
     * Field _cdProdutoServicoOperacao
     */
    private int _cdProdutoServicoOperacao = 0;

    /**
     * keeps track of state for field: _cdProdutoServicoOperacao
     */
    private boolean _has_cdProdutoServicoOperacao;

    /**
     * Field _dsResumoProdutoServico
     */
    private java.lang.String _dsResumoProdutoServico;

    /**
     * Field _cdProdutoServicoRelacionado
     */
    private int _cdProdutoServicoRelacionado = 0;

    /**
     * keeps track of state for field: _cdProdutoServicoRelacionado
     */
    private boolean _has_cdProdutoServicoRelacionado;

    /**
     * Field _dsOperacaoProdutoServico
     */
    private java.lang.String _dsOperacaoProdutoServico;

    /**
     * Field _cdControlePagamento
     */
    private java.lang.String _cdControlePagamento;

    /**
     * Field _dtCreditoPagamento
     */
    private java.lang.String _dtCreditoPagamento;

    /**
     * Field _vlEfetivoPagamento
     */
    private java.math.BigDecimal _vlEfetivoPagamento = new java.math.BigDecimal("0");

    /**
     * Field _cdInscricaoFavorecido
     */
    private long _cdInscricaoFavorecido = 0;

    /**
     * keeps track of state for field: _cdInscricaoFavorecido
     */
    private boolean _has_cdInscricaoFavorecido;

    /**
     * Field _dsFavorecido
     */
    private java.lang.String _dsFavorecido;

    /**
     * Field _cdBancoDebito
     */
    private int _cdBancoDebito = 0;

    /**
     * keeps track of state for field: _cdBancoDebito
     */
    private boolean _has_cdBancoDebito;

    /**
     * Field _cdAgenciaDebito
     */
    private int _cdAgenciaDebito = 0;

    /**
     * keeps track of state for field: _cdAgenciaDebito
     */
    private boolean _has_cdAgenciaDebito;

    /**
     * Field _cdDigitoAgenciaDebito
     */
    private int _cdDigitoAgenciaDebito = 0;

    /**
     * keeps track of state for field: _cdDigitoAgenciaDebito
     */
    private boolean _has_cdDigitoAgenciaDebito;

    /**
     * Field _cdContaDebito
     */
    private long _cdContaDebito = 0;

    /**
     * keeps track of state for field: _cdContaDebito
     */
    private boolean _has_cdContaDebito;

    /**
     * Field _cdDigitoContaDebito
     */
    private java.lang.String _cdDigitoContaDebito;

    /**
     * Field _cdBancoCredito
     */
    private int _cdBancoCredito = 0;

    /**
     * keeps track of state for field: _cdBancoCredito
     */
    private boolean _has_cdBancoCredito;

    /**
     * Field _cdAgenciaCredito
     */
    private int _cdAgenciaCredito = 0;

    /**
     * keeps track of state for field: _cdAgenciaCredito
     */
    private boolean _has_cdAgenciaCredito;

    /**
     * Field _cdDigitoAgenciaCredito
     */
    private int _cdDigitoAgenciaCredito = 0;

    /**
     * keeps track of state for field: _cdDigitoAgenciaCredito
     */
    private boolean _has_cdDigitoAgenciaCredito;

    /**
     * Field _cdContaCredito
     */
    private long _cdContaCredito = 0;

    /**
     * keeps track of state for field: _cdContaCredito
     */
    private boolean _has_cdContaCredito;

    /**
     * Field _cdDigitoContaCredito
     */
    private java.lang.String _cdDigitoContaCredito;

    /**
     * Field _cdSituacaoOperacaoPagamento
     */
    private int _cdSituacaoOperacaoPagamento = 0;

    /**
     * keeps track of state for field: _cdSituacaoOperacaoPagamento
     */
    private boolean _has_cdSituacaoOperacaoPagamento;

    /**
     * Field _dsSituacaoOperacaoPagamento
     */
    private java.lang.String _dsSituacaoOperacaoPagamento;

    /**
     * Field _cdMotivoSituacaoPagamento
     */
    private int _cdMotivoSituacaoPagamento = 0;

    /**
     * keeps track of state for field: _cdMotivoSituacaoPagamento
     */
    private boolean _has_cdMotivoSituacaoPagamento;

    /**
     * Field _dsMotivoSituacaoPagamento
     */
    private java.lang.String _dsMotivoSituacaoPagamento;

    /**
     * Field _cdTipoCanal
     */
    private int _cdTipoCanal = 0;

    /**
     * keeps track of state for field: _cdTipoCanal
     */
    private boolean _has_cdTipoCanal;

    /**
     * Field _cdTipoTela
     */
    private int _cdTipoTela = 0;

    /**
     * keeps track of state for field: _cdTipoTela
     */
    private boolean _has_cdTipoTela;

    /**
     * Field _dsEfetivacaoPagamento
     */
    private java.lang.String _dsEfetivacaoPagamento;

    /**
     * Field _dsIndicadorPagamento
     */
    private java.lang.String _dsIndicadorPagamento;

    /**
     * Field _cdBanco
     */
    private int _cdBanco = 0;

    /**
     * keeps track of state for field: _cdBanco
     */
    private boolean _has_cdBanco;

    /**
     * Field _cdAgenciaSalario
     */
    private int _cdAgenciaSalario = 0;

    /**
     * keeps track of state for field: _cdAgenciaSalario
     */
    private boolean _has_cdAgenciaSalario;

    /**
     * Field _cdDigitoAgenciaSalarial
     */
    private int _cdDigitoAgenciaSalarial = 0;

    /**
     * keeps track of state for field: _cdDigitoAgenciaSalarial
     */
    private boolean _has_cdDigitoAgenciaSalarial;

    /**
     * Field _cdContaSalarial
     */
    private long _cdContaSalarial = 0;

    /**
     * keeps track of state for field: _cdContaSalarial
     */
    private boolean _has_cdContaSalarial;

    /**
     * Field _cdDigitoContaSalarial
     */
    private java.lang.String _cdDigitoContaSalarial;

    /**
     * Field _cdIspbPagtoDestino
     */
    private java.lang.String _cdIspbPagtoDestino;

    /**
     * Field _contaPagtoDestino
     */
    private java.lang.String _contaPagtoDestino = "0";


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
        setVlEfetivoPagamento(new java.math.BigDecimal("0"));
        setContaPagtoDestino("0");
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarpagtosindantpostergacao.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdAgenciaCredito
     * 
     */
    public void deleteCdAgenciaCredito()
    {
        this._has_cdAgenciaCredito= false;
    } //-- void deleteCdAgenciaCredito() 

    /**
     * Method deleteCdAgenciaDebito
     * 
     */
    public void deleteCdAgenciaDebito()
    {
        this._has_cdAgenciaDebito= false;
    } //-- void deleteCdAgenciaDebito() 

    /**
     * Method deleteCdAgenciaSalario
     * 
     */
    public void deleteCdAgenciaSalario()
    {
        this._has_cdAgenciaSalario= false;
    } //-- void deleteCdAgenciaSalario() 

    /**
     * Method deleteCdBanco
     * 
     */
    public void deleteCdBanco()
    {
        this._has_cdBanco= false;
    } //-- void deleteCdBanco() 

    /**
     * Method deleteCdBancoCredito
     * 
     */
    public void deleteCdBancoCredito()
    {
        this._has_cdBancoCredito= false;
    } //-- void deleteCdBancoCredito() 

    /**
     * Method deleteCdBancoDebito
     * 
     */
    public void deleteCdBancoDebito()
    {
        this._has_cdBancoDebito= false;
    } //-- void deleteCdBancoDebito() 

    /**
     * Method deleteCdContaCredito
     * 
     */
    public void deleteCdContaCredito()
    {
        this._has_cdContaCredito= false;
    } //-- void deleteCdContaCredito() 

    /**
     * Method deleteCdContaDebito
     * 
     */
    public void deleteCdContaDebito()
    {
        this._has_cdContaDebito= false;
    } //-- void deleteCdContaDebito() 

    /**
     * Method deleteCdContaSalarial
     * 
     */
    public void deleteCdContaSalarial()
    {
        this._has_cdContaSalarial= false;
    } //-- void deleteCdContaSalarial() 

    /**
     * Method deleteCdDigitoAgenciaCredito
     * 
     */
    public void deleteCdDigitoAgenciaCredito()
    {
        this._has_cdDigitoAgenciaCredito= false;
    } //-- void deleteCdDigitoAgenciaCredito() 

    /**
     * Method deleteCdDigitoAgenciaDebito
     * 
     */
    public void deleteCdDigitoAgenciaDebito()
    {
        this._has_cdDigitoAgenciaDebito= false;
    } //-- void deleteCdDigitoAgenciaDebito() 

    /**
     * Method deleteCdDigitoAgenciaSalarial
     * 
     */
    public void deleteCdDigitoAgenciaSalarial()
    {
        this._has_cdDigitoAgenciaSalarial= false;
    } //-- void deleteCdDigitoAgenciaSalarial() 

    /**
     * Method deleteCdEmpresa
     * 
     */
    public void deleteCdEmpresa()
    {
        this._has_cdEmpresa= false;
    } //-- void deleteCdEmpresa() 

    /**
     * Method deleteCdInscricaoFavorecido
     * 
     */
    public void deleteCdInscricaoFavorecido()
    {
        this._has_cdInscricaoFavorecido= false;
    } //-- void deleteCdInscricaoFavorecido() 

    /**
     * Method deleteCdMotivoSituacaoPagamento
     * 
     */
    public void deleteCdMotivoSituacaoPagamento()
    {
        this._has_cdMotivoSituacaoPagamento= false;
    } //-- void deleteCdMotivoSituacaoPagamento() 

    /**
     * Method deleteCdPessoaJuridicaContrato
     * 
     */
    public void deleteCdPessoaJuridicaContrato()
    {
        this._has_cdPessoaJuridicaContrato= false;
    } //-- void deleteCdPessoaJuridicaContrato() 

    /**
     * Method deleteCdProdutoServicoOperacao
     * 
     */
    public void deleteCdProdutoServicoOperacao()
    {
        this._has_cdProdutoServicoOperacao= false;
    } //-- void deleteCdProdutoServicoOperacao() 

    /**
     * Method deleteCdProdutoServicoRelacionado
     * 
     */
    public void deleteCdProdutoServicoRelacionado()
    {
        this._has_cdProdutoServicoRelacionado= false;
    } //-- void deleteCdProdutoServicoRelacionado() 

    /**
     * Method deleteCdSituacaoOperacaoPagamento
     * 
     */
    public void deleteCdSituacaoOperacaoPagamento()
    {
        this._has_cdSituacaoOperacaoPagamento= false;
    } //-- void deleteCdSituacaoOperacaoPagamento() 

    /**
     * Method deleteCdTipoCanal
     * 
     */
    public void deleteCdTipoCanal()
    {
        this._has_cdTipoCanal= false;
    } //-- void deleteCdTipoCanal() 

    /**
     * Method deleteCdTipoContratoNegocio
     * 
     */
    public void deleteCdTipoContratoNegocio()
    {
        this._has_cdTipoContratoNegocio= false;
    } //-- void deleteCdTipoContratoNegocio() 

    /**
     * Method deleteCdTipoTela
     * 
     */
    public void deleteCdTipoTela()
    {
        this._has_cdTipoTela= false;
    } //-- void deleteCdTipoTela() 

    /**
     * Method deleteNrCnpjCpf
     * 
     */
    public void deleteNrCnpjCpf()
    {
        this._has_nrCnpjCpf= false;
    } //-- void deleteNrCnpjCpf() 

    /**
     * Method deleteNrContrato
     * 
     */
    public void deleteNrContrato()
    {
        this._has_nrContrato= false;
    } //-- void deleteNrContrato() 

    /**
     * Method deleteNrDigitoCnpjCpf
     * 
     */
    public void deleteNrDigitoCnpjCpf()
    {
        this._has_nrDigitoCnpjCpf= false;
    } //-- void deleteNrDigitoCnpjCpf() 

    /**
     * Method deleteNrFilialCnpjCpf
     * 
     */
    public void deleteNrFilialCnpjCpf()
    {
        this._has_nrFilialCnpjCpf= false;
    } //-- void deleteNrFilialCnpjCpf() 

    /**
     * Returns the value of field 'cdAgenciaCredito'.
     * 
     * @return int
     * @return the value of field 'cdAgenciaCredito'.
     */
    public int getCdAgenciaCredito()
    {
        return this._cdAgenciaCredito;
    } //-- int getCdAgenciaCredito() 

    /**
     * Returns the value of field 'cdAgenciaDebito'.
     * 
     * @return int
     * @return the value of field 'cdAgenciaDebito'.
     */
    public int getCdAgenciaDebito()
    {
        return this._cdAgenciaDebito;
    } //-- int getCdAgenciaDebito() 

    /**
     * Returns the value of field 'cdAgenciaSalario'.
     * 
     * @return int
     * @return the value of field 'cdAgenciaSalario'.
     */
    public int getCdAgenciaSalario()
    {
        return this._cdAgenciaSalario;
    } //-- int getCdAgenciaSalario() 

    /**
     * Returns the value of field 'cdBanco'.
     * 
     * @return int
     * @return the value of field 'cdBanco'.
     */
    public int getCdBanco()
    {
        return this._cdBanco;
    } //-- int getCdBanco() 

    /**
     * Returns the value of field 'cdBancoCredito'.
     * 
     * @return int
     * @return the value of field 'cdBancoCredito'.
     */
    public int getCdBancoCredito()
    {
        return this._cdBancoCredito;
    } //-- int getCdBancoCredito() 

    /**
     * Returns the value of field 'cdBancoDebito'.
     * 
     * @return int
     * @return the value of field 'cdBancoDebito'.
     */
    public int getCdBancoDebito()
    {
        return this._cdBancoDebito;
    } //-- int getCdBancoDebito() 

    /**
     * Returns the value of field 'cdContaCredito'.
     * 
     * @return long
     * @return the value of field 'cdContaCredito'.
     */
    public long getCdContaCredito()
    {
        return this._cdContaCredito;
    } //-- long getCdContaCredito() 

    /**
     * Returns the value of field 'cdContaDebito'.
     * 
     * @return long
     * @return the value of field 'cdContaDebito'.
     */
    public long getCdContaDebito()
    {
        return this._cdContaDebito;
    } //-- long getCdContaDebito() 

    /**
     * Returns the value of field 'cdContaSalarial'.
     * 
     * @return long
     * @return the value of field 'cdContaSalarial'.
     */
    public long getCdContaSalarial()
    {
        return this._cdContaSalarial;
    } //-- long getCdContaSalarial() 

    /**
     * Returns the value of field 'cdControlePagamento'.
     * 
     * @return String
     * @return the value of field 'cdControlePagamento'.
     */
    public java.lang.String getCdControlePagamento()
    {
        return this._cdControlePagamento;
    } //-- java.lang.String getCdControlePagamento() 

    /**
     * Returns the value of field 'cdDigitoAgenciaCredito'.
     * 
     * @return int
     * @return the value of field 'cdDigitoAgenciaCredito'.
     */
    public int getCdDigitoAgenciaCredito()
    {
        return this._cdDigitoAgenciaCredito;
    } //-- int getCdDigitoAgenciaCredito() 

    /**
     * Returns the value of field 'cdDigitoAgenciaDebito'.
     * 
     * @return int
     * @return the value of field 'cdDigitoAgenciaDebito'.
     */
    public int getCdDigitoAgenciaDebito()
    {
        return this._cdDigitoAgenciaDebito;
    } //-- int getCdDigitoAgenciaDebito() 

    /**
     * Returns the value of field 'cdDigitoAgenciaSalarial'.
     * 
     * @return int
     * @return the value of field 'cdDigitoAgenciaSalarial'.
     */
    public int getCdDigitoAgenciaSalarial()
    {
        return this._cdDigitoAgenciaSalarial;
    } //-- int getCdDigitoAgenciaSalarial() 

    /**
     * Returns the value of field 'cdDigitoContaCredito'.
     * 
     * @return String
     * @return the value of field 'cdDigitoContaCredito'.
     */
    public java.lang.String getCdDigitoContaCredito()
    {
        return this._cdDigitoContaCredito;
    } //-- java.lang.String getCdDigitoContaCredito() 

    /**
     * Returns the value of field 'cdDigitoContaDebito'.
     * 
     * @return String
     * @return the value of field 'cdDigitoContaDebito'.
     */
    public java.lang.String getCdDigitoContaDebito()
    {
        return this._cdDigitoContaDebito;
    } //-- java.lang.String getCdDigitoContaDebito() 

    /**
     * Returns the value of field 'cdDigitoContaSalarial'.
     * 
     * @return String
     * @return the value of field 'cdDigitoContaSalarial'.
     */
    public java.lang.String getCdDigitoContaSalarial()
    {
        return this._cdDigitoContaSalarial;
    } //-- java.lang.String getCdDigitoContaSalarial() 

    /**
     * Returns the value of field 'cdEmpresa'.
     * 
     * @return long
     * @return the value of field 'cdEmpresa'.
     */
    public long getCdEmpresa()
    {
        return this._cdEmpresa;
    } //-- long getCdEmpresa() 

    /**
     * Returns the value of field 'cdInscricaoFavorecido'.
     * 
     * @return long
     * @return the value of field 'cdInscricaoFavorecido'.
     */
    public long getCdInscricaoFavorecido()
    {
        return this._cdInscricaoFavorecido;
    } //-- long getCdInscricaoFavorecido() 

    /**
     * Returns the value of field 'cdIspbPagtoDestino'.
     * 
     * @return String
     * @return the value of field 'cdIspbPagtoDestino'.
     */
    public java.lang.String getCdIspbPagtoDestino()
    {
        return this._cdIspbPagtoDestino;
    } //-- java.lang.String getCdIspbPagtoDestino() 

    /**
     * Returns the value of field 'cdMotivoSituacaoPagamento'.
     * 
     * @return int
     * @return the value of field 'cdMotivoSituacaoPagamento'.
     */
    public int getCdMotivoSituacaoPagamento()
    {
        return this._cdMotivoSituacaoPagamento;
    } //-- int getCdMotivoSituacaoPagamento() 

    /**
     * Returns the value of field 'cdPessoaJuridicaContrato'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridicaContrato'.
     */
    public long getCdPessoaJuridicaContrato()
    {
        return this._cdPessoaJuridicaContrato;
    } //-- long getCdPessoaJuridicaContrato() 

    /**
     * Returns the value of field 'cdProdutoServicoOperacao'.
     * 
     * @return int
     * @return the value of field 'cdProdutoServicoOperacao'.
     */
    public int getCdProdutoServicoOperacao()
    {
        return this._cdProdutoServicoOperacao;
    } //-- int getCdProdutoServicoOperacao() 

    /**
     * Returns the value of field 'cdProdutoServicoRelacionado'.
     * 
     * @return int
     * @return the value of field 'cdProdutoServicoRelacionado'.
     */
    public int getCdProdutoServicoRelacionado()
    {
        return this._cdProdutoServicoRelacionado;
    } //-- int getCdProdutoServicoRelacionado() 

    /**
     * Returns the value of field 'cdSituacaoOperacaoPagamento'.
     * 
     * @return int
     * @return the value of field 'cdSituacaoOperacaoPagamento'.
     */
    public int getCdSituacaoOperacaoPagamento()
    {
        return this._cdSituacaoOperacaoPagamento;
    } //-- int getCdSituacaoOperacaoPagamento() 

    /**
     * Returns the value of field 'cdTipoCanal'.
     * 
     * @return int
     * @return the value of field 'cdTipoCanal'.
     */
    public int getCdTipoCanal()
    {
        return this._cdTipoCanal;
    } //-- int getCdTipoCanal() 

    /**
     * Returns the value of field 'cdTipoContratoNegocio'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoNegocio'.
     */
    public int getCdTipoContratoNegocio()
    {
        return this._cdTipoContratoNegocio;
    } //-- int getCdTipoContratoNegocio() 

    /**
     * Returns the value of field 'cdTipoTela'.
     * 
     * @return int
     * @return the value of field 'cdTipoTela'.
     */
    public int getCdTipoTela()
    {
        return this._cdTipoTela;
    } //-- int getCdTipoTela() 

    /**
     * Returns the value of field 'contaPagtoDestino'.
     * 
     * @return String
     * @return the value of field 'contaPagtoDestino'.
     */
    public java.lang.String getContaPagtoDestino()
    {
        return this._contaPagtoDestino;
    } //-- java.lang.String getContaPagtoDestino() 

    /**
     * Returns the value of field 'dsEfetivacaoPagamento'.
     * 
     * @return String
     * @return the value of field 'dsEfetivacaoPagamento'.
     */
    public java.lang.String getDsEfetivacaoPagamento()
    {
        return this._dsEfetivacaoPagamento;
    } //-- java.lang.String getDsEfetivacaoPagamento() 

    /**
     * Returns the value of field 'dsEmpresa'.
     * 
     * @return String
     * @return the value of field 'dsEmpresa'.
     */
    public java.lang.String getDsEmpresa()
    {
        return this._dsEmpresa;
    } //-- java.lang.String getDsEmpresa() 

    /**
     * Returns the value of field 'dsFavorecido'.
     * 
     * @return String
     * @return the value of field 'dsFavorecido'.
     */
    public java.lang.String getDsFavorecido()
    {
        return this._dsFavorecido;
    } //-- java.lang.String getDsFavorecido() 

    /**
     * Returns the value of field 'dsIndicadorPagamento'.
     * 
     * @return String
     * @return the value of field 'dsIndicadorPagamento'.
     */
    public java.lang.String getDsIndicadorPagamento()
    {
        return this._dsIndicadorPagamento;
    } //-- java.lang.String getDsIndicadorPagamento() 

    /**
     * Returns the value of field 'dsMotivoSituacaoPagamento'.
     * 
     * @return String
     * @return the value of field 'dsMotivoSituacaoPagamento'.
     */
    public java.lang.String getDsMotivoSituacaoPagamento()
    {
        return this._dsMotivoSituacaoPagamento;
    } //-- java.lang.String getDsMotivoSituacaoPagamento() 

    /**
     * Returns the value of field 'dsOperacaoProdutoServico'.
     * 
     * @return String
     * @return the value of field 'dsOperacaoProdutoServico'.
     */
    public java.lang.String getDsOperacaoProdutoServico()
    {
        return this._dsOperacaoProdutoServico;
    } //-- java.lang.String getDsOperacaoProdutoServico() 

    /**
     * Returns the value of field 'dsRazaoSocial'.
     * 
     * @return String
     * @return the value of field 'dsRazaoSocial'.
     */
    public java.lang.String getDsRazaoSocial()
    {
        return this._dsRazaoSocial;
    } //-- java.lang.String getDsRazaoSocial() 

    /**
     * Returns the value of field 'dsResumoProdutoServico'.
     * 
     * @return String
     * @return the value of field 'dsResumoProdutoServico'.
     */
    public java.lang.String getDsResumoProdutoServico()
    {
        return this._dsResumoProdutoServico;
    } //-- java.lang.String getDsResumoProdutoServico() 

    /**
     * Returns the value of field 'dsSituacaoOperacaoPagamento'.
     * 
     * @return String
     * @return the value of field 'dsSituacaoOperacaoPagamento'.
     */
    public java.lang.String getDsSituacaoOperacaoPagamento()
    {
        return this._dsSituacaoOperacaoPagamento;
    } //-- java.lang.String getDsSituacaoOperacaoPagamento() 

    /**
     * Returns the value of field 'dtCreditoPagamento'.
     * 
     * @return String
     * @return the value of field 'dtCreditoPagamento'.
     */
    public java.lang.String getDtCreditoPagamento()
    {
        return this._dtCreditoPagamento;
    } //-- java.lang.String getDtCreditoPagamento() 

    /**
     * Returns the value of field 'nrCnpjCpf'.
     * 
     * @return long
     * @return the value of field 'nrCnpjCpf'.
     */
    public long getNrCnpjCpf()
    {
        return this._nrCnpjCpf;
    } //-- long getNrCnpjCpf() 

    /**
     * Returns the value of field 'nrContrato'.
     * 
     * @return long
     * @return the value of field 'nrContrato'.
     */
    public long getNrContrato()
    {
        return this._nrContrato;
    } //-- long getNrContrato() 

    /**
     * Returns the value of field 'nrDigitoCnpjCpf'.
     * 
     * @return int
     * @return the value of field 'nrDigitoCnpjCpf'.
     */
    public int getNrDigitoCnpjCpf()
    {
        return this._nrDigitoCnpjCpf;
    } //-- int getNrDigitoCnpjCpf() 

    /**
     * Returns the value of field 'nrFilialCnpjCpf'.
     * 
     * @return int
     * @return the value of field 'nrFilialCnpjCpf'.
     */
    public int getNrFilialCnpjCpf()
    {
        return this._nrFilialCnpjCpf;
    } //-- int getNrFilialCnpjCpf() 

    /**
     * Returns the value of field 'vlEfetivoPagamento'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlEfetivoPagamento'.
     */
    public java.math.BigDecimal getVlEfetivoPagamento()
    {
        return this._vlEfetivoPagamento;
    } //-- java.math.BigDecimal getVlEfetivoPagamento() 

    /**
     * Method hasCdAgenciaCredito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgenciaCredito()
    {
        return this._has_cdAgenciaCredito;
    } //-- boolean hasCdAgenciaCredito() 

    /**
     * Method hasCdAgenciaDebito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgenciaDebito()
    {
        return this._has_cdAgenciaDebito;
    } //-- boolean hasCdAgenciaDebito() 

    /**
     * Method hasCdAgenciaSalario
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgenciaSalario()
    {
        return this._has_cdAgenciaSalario;
    } //-- boolean hasCdAgenciaSalario() 

    /**
     * Method hasCdBanco
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdBanco()
    {
        return this._has_cdBanco;
    } //-- boolean hasCdBanco() 

    /**
     * Method hasCdBancoCredito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdBancoCredito()
    {
        return this._has_cdBancoCredito;
    } //-- boolean hasCdBancoCredito() 

    /**
     * Method hasCdBancoDebito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdBancoDebito()
    {
        return this._has_cdBancoDebito;
    } //-- boolean hasCdBancoDebito() 

    /**
     * Method hasCdContaCredito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdContaCredito()
    {
        return this._has_cdContaCredito;
    } //-- boolean hasCdContaCredito() 

    /**
     * Method hasCdContaDebito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdContaDebito()
    {
        return this._has_cdContaDebito;
    } //-- boolean hasCdContaDebito() 

    /**
     * Method hasCdContaSalarial
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdContaSalarial()
    {
        return this._has_cdContaSalarial;
    } //-- boolean hasCdContaSalarial() 

    /**
     * Method hasCdDigitoAgenciaCredito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdDigitoAgenciaCredito()
    {
        return this._has_cdDigitoAgenciaCredito;
    } //-- boolean hasCdDigitoAgenciaCredito() 

    /**
     * Method hasCdDigitoAgenciaDebito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdDigitoAgenciaDebito()
    {
        return this._has_cdDigitoAgenciaDebito;
    } //-- boolean hasCdDigitoAgenciaDebito() 

    /**
     * Method hasCdDigitoAgenciaSalarial
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdDigitoAgenciaSalarial()
    {
        return this._has_cdDigitoAgenciaSalarial;
    } //-- boolean hasCdDigitoAgenciaSalarial() 

    /**
     * Method hasCdEmpresa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdEmpresa()
    {
        return this._has_cdEmpresa;
    } //-- boolean hasCdEmpresa() 

    /**
     * Method hasCdInscricaoFavorecido
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdInscricaoFavorecido()
    {
        return this._has_cdInscricaoFavorecido;
    } //-- boolean hasCdInscricaoFavorecido() 

    /**
     * Method hasCdMotivoSituacaoPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMotivoSituacaoPagamento()
    {
        return this._has_cdMotivoSituacaoPagamento;
    } //-- boolean hasCdMotivoSituacaoPagamento() 

    /**
     * Method hasCdPessoaJuridicaContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridicaContrato()
    {
        return this._has_cdPessoaJuridicaContrato;
    } //-- boolean hasCdPessoaJuridicaContrato() 

    /**
     * Method hasCdProdutoServicoOperacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdutoServicoOperacao()
    {
        return this._has_cdProdutoServicoOperacao;
    } //-- boolean hasCdProdutoServicoOperacao() 

    /**
     * Method hasCdProdutoServicoRelacionado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdutoServicoRelacionado()
    {
        return this._has_cdProdutoServicoRelacionado;
    } //-- boolean hasCdProdutoServicoRelacionado() 

    /**
     * Method hasCdSituacaoOperacaoPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSituacaoOperacaoPagamento()
    {
        return this._has_cdSituacaoOperacaoPagamento;
    } //-- boolean hasCdSituacaoOperacaoPagamento() 

    /**
     * Method hasCdTipoCanal
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoCanal()
    {
        return this._has_cdTipoCanal;
    } //-- boolean hasCdTipoCanal() 

    /**
     * Method hasCdTipoContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoNegocio()
    {
        return this._has_cdTipoContratoNegocio;
    } //-- boolean hasCdTipoContratoNegocio() 

    /**
     * Method hasCdTipoTela
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoTela()
    {
        return this._has_cdTipoTela;
    } //-- boolean hasCdTipoTela() 

    /**
     * Method hasNrCnpjCpf
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrCnpjCpf()
    {
        return this._has_nrCnpjCpf;
    } //-- boolean hasNrCnpjCpf() 

    /**
     * Method hasNrContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrContrato()
    {
        return this._has_nrContrato;
    } //-- boolean hasNrContrato() 

    /**
     * Method hasNrDigitoCnpjCpf
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrDigitoCnpjCpf()
    {
        return this._has_nrDigitoCnpjCpf;
    } //-- boolean hasNrDigitoCnpjCpf() 

    /**
     * Method hasNrFilialCnpjCpf
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrFilialCnpjCpf()
    {
        return this._has_nrFilialCnpjCpf;
    } //-- boolean hasNrFilialCnpjCpf() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdAgenciaCredito'.
     * 
     * @param cdAgenciaCredito the value of field 'cdAgenciaCredito'
     */
    public void setCdAgenciaCredito(int cdAgenciaCredito)
    {
        this._cdAgenciaCredito = cdAgenciaCredito;
        this._has_cdAgenciaCredito = true;
    } //-- void setCdAgenciaCredito(int) 

    /**
     * Sets the value of field 'cdAgenciaDebito'.
     * 
     * @param cdAgenciaDebito the value of field 'cdAgenciaDebito'.
     */
    public void setCdAgenciaDebito(int cdAgenciaDebito)
    {
        this._cdAgenciaDebito = cdAgenciaDebito;
        this._has_cdAgenciaDebito = true;
    } //-- void setCdAgenciaDebito(int) 

    /**
     * Sets the value of field 'cdAgenciaSalario'.
     * 
     * @param cdAgenciaSalario the value of field 'cdAgenciaSalario'
     */
    public void setCdAgenciaSalario(int cdAgenciaSalario)
    {
        this._cdAgenciaSalario = cdAgenciaSalario;
        this._has_cdAgenciaSalario = true;
    } //-- void setCdAgenciaSalario(int) 

    /**
     * Sets the value of field 'cdBanco'.
     * 
     * @param cdBanco the value of field 'cdBanco'.
     */
    public void setCdBanco(int cdBanco)
    {
        this._cdBanco = cdBanco;
        this._has_cdBanco = true;
    } //-- void setCdBanco(int) 

    /**
     * Sets the value of field 'cdBancoCredito'.
     * 
     * @param cdBancoCredito the value of field 'cdBancoCredito'.
     */
    public void setCdBancoCredito(int cdBancoCredito)
    {
        this._cdBancoCredito = cdBancoCredito;
        this._has_cdBancoCredito = true;
    } //-- void setCdBancoCredito(int) 

    /**
     * Sets the value of field 'cdBancoDebito'.
     * 
     * @param cdBancoDebito the value of field 'cdBancoDebito'.
     */
    public void setCdBancoDebito(int cdBancoDebito)
    {
        this._cdBancoDebito = cdBancoDebito;
        this._has_cdBancoDebito = true;
    } //-- void setCdBancoDebito(int) 

    /**
     * Sets the value of field 'cdContaCredito'.
     * 
     * @param cdContaCredito the value of field 'cdContaCredito'.
     */
    public void setCdContaCredito(long cdContaCredito)
    {
        this._cdContaCredito = cdContaCredito;
        this._has_cdContaCredito = true;
    } //-- void setCdContaCredito(long) 

    /**
     * Sets the value of field 'cdContaDebito'.
     * 
     * @param cdContaDebito the value of field 'cdContaDebito'.
     */
    public void setCdContaDebito(long cdContaDebito)
    {
        this._cdContaDebito = cdContaDebito;
        this._has_cdContaDebito = true;
    } //-- void setCdContaDebito(long) 

    /**
     * Sets the value of field 'cdContaSalarial'.
     * 
     * @param cdContaSalarial the value of field 'cdContaSalarial'.
     */
    public void setCdContaSalarial(long cdContaSalarial)
    {
        this._cdContaSalarial = cdContaSalarial;
        this._has_cdContaSalarial = true;
    } //-- void setCdContaSalarial(long) 

    /**
     * Sets the value of field 'cdControlePagamento'.
     * 
     * @param cdControlePagamento the value of field
     * 'cdControlePagamento'.
     */
    public void setCdControlePagamento(java.lang.String cdControlePagamento)
    {
        this._cdControlePagamento = cdControlePagamento;
    } //-- void setCdControlePagamento(java.lang.String) 

    /**
     * Sets the value of field 'cdDigitoAgenciaCredito'.
     * 
     * @param cdDigitoAgenciaCredito the value of field
     * 'cdDigitoAgenciaCredito'.
     */
    public void setCdDigitoAgenciaCredito(int cdDigitoAgenciaCredito)
    {
        this._cdDigitoAgenciaCredito = cdDigitoAgenciaCredito;
        this._has_cdDigitoAgenciaCredito = true;
    } //-- void setCdDigitoAgenciaCredito(int) 

    /**
     * Sets the value of field 'cdDigitoAgenciaDebito'.
     * 
     * @param cdDigitoAgenciaDebito the value of field
     * 'cdDigitoAgenciaDebito'.
     */
    public void setCdDigitoAgenciaDebito(int cdDigitoAgenciaDebito)
    {
        this._cdDigitoAgenciaDebito = cdDigitoAgenciaDebito;
        this._has_cdDigitoAgenciaDebito = true;
    } //-- void setCdDigitoAgenciaDebito(int) 

    /**
     * Sets the value of field 'cdDigitoAgenciaSalarial'.
     * 
     * @param cdDigitoAgenciaSalarial the value of field
     * 'cdDigitoAgenciaSalarial'.
     */
    public void setCdDigitoAgenciaSalarial(int cdDigitoAgenciaSalarial)
    {
        this._cdDigitoAgenciaSalarial = cdDigitoAgenciaSalarial;
        this._has_cdDigitoAgenciaSalarial = true;
    } //-- void setCdDigitoAgenciaSalarial(int) 

    /**
     * Sets the value of field 'cdDigitoContaCredito'.
     * 
     * @param cdDigitoContaCredito the value of field
     * 'cdDigitoContaCredito'.
     */
    public void setCdDigitoContaCredito(java.lang.String cdDigitoContaCredito)
    {
        this._cdDigitoContaCredito = cdDigitoContaCredito;
    } //-- void setCdDigitoContaCredito(java.lang.String) 

    /**
     * Sets the value of field 'cdDigitoContaDebito'.
     * 
     * @param cdDigitoContaDebito the value of field
     * 'cdDigitoContaDebito'.
     */
    public void setCdDigitoContaDebito(java.lang.String cdDigitoContaDebito)
    {
        this._cdDigitoContaDebito = cdDigitoContaDebito;
    } //-- void setCdDigitoContaDebito(java.lang.String) 

    /**
     * Sets the value of field 'cdDigitoContaSalarial'.
     * 
     * @param cdDigitoContaSalarial the value of field
     * 'cdDigitoContaSalarial'.
     */
    public void setCdDigitoContaSalarial(java.lang.String cdDigitoContaSalarial)
    {
        this._cdDigitoContaSalarial = cdDigitoContaSalarial;
    } //-- void setCdDigitoContaSalarial(java.lang.String) 

    /**
     * Sets the value of field 'cdEmpresa'.
     * 
     * @param cdEmpresa the value of field 'cdEmpresa'.
     */
    public void setCdEmpresa(long cdEmpresa)
    {
        this._cdEmpresa = cdEmpresa;
        this._has_cdEmpresa = true;
    } //-- void setCdEmpresa(long) 

    /**
     * Sets the value of field 'cdInscricaoFavorecido'.
     * 
     * @param cdInscricaoFavorecido the value of field
     * 'cdInscricaoFavorecido'.
     */
    public void setCdInscricaoFavorecido(long cdInscricaoFavorecido)
    {
        this._cdInscricaoFavorecido = cdInscricaoFavorecido;
        this._has_cdInscricaoFavorecido = true;
    } //-- void setCdInscricaoFavorecido(long) 

    /**
     * Sets the value of field 'cdIspbPagtoDestino'.
     * 
     * @param cdIspbPagtoDestino the value of field
     * 'cdIspbPagtoDestino'.
     */
    public void setCdIspbPagtoDestino(java.lang.String cdIspbPagtoDestino)
    {
        this._cdIspbPagtoDestino = cdIspbPagtoDestino;
    } //-- void setCdIspbPagtoDestino(java.lang.String) 

    /**
     * Sets the value of field 'cdMotivoSituacaoPagamento'.
     * 
     * @param cdMotivoSituacaoPagamento the value of field
     * 'cdMotivoSituacaoPagamento'.
     */
    public void setCdMotivoSituacaoPagamento(int cdMotivoSituacaoPagamento)
    {
        this._cdMotivoSituacaoPagamento = cdMotivoSituacaoPagamento;
        this._has_cdMotivoSituacaoPagamento = true;
    } //-- void setCdMotivoSituacaoPagamento(int) 

    /**
     * Sets the value of field 'cdPessoaJuridicaContrato'.
     * 
     * @param cdPessoaJuridicaContrato the value of field
     * 'cdPessoaJuridicaContrato'.
     */
    public void setCdPessoaJuridicaContrato(long cdPessoaJuridicaContrato)
    {
        this._cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
        this._has_cdPessoaJuridicaContrato = true;
    } //-- void setCdPessoaJuridicaContrato(long) 

    /**
     * Sets the value of field 'cdProdutoServicoOperacao'.
     * 
     * @param cdProdutoServicoOperacao the value of field
     * 'cdProdutoServicoOperacao'.
     */
    public void setCdProdutoServicoOperacao(int cdProdutoServicoOperacao)
    {
        this._cdProdutoServicoOperacao = cdProdutoServicoOperacao;
        this._has_cdProdutoServicoOperacao = true;
    } //-- void setCdProdutoServicoOperacao(int) 

    /**
     * Sets the value of field 'cdProdutoServicoRelacionado'.
     * 
     * @param cdProdutoServicoRelacionado the value of field
     * 'cdProdutoServicoRelacionado'.
     */
    public void setCdProdutoServicoRelacionado(int cdProdutoServicoRelacionado)
    {
        this._cdProdutoServicoRelacionado = cdProdutoServicoRelacionado;
        this._has_cdProdutoServicoRelacionado = true;
    } //-- void setCdProdutoServicoRelacionado(int) 

    /**
     * Sets the value of field 'cdSituacaoOperacaoPagamento'.
     * 
     * @param cdSituacaoOperacaoPagamento the value of field
     * 'cdSituacaoOperacaoPagamento'.
     */
    public void setCdSituacaoOperacaoPagamento(int cdSituacaoOperacaoPagamento)
    {
        this._cdSituacaoOperacaoPagamento = cdSituacaoOperacaoPagamento;
        this._has_cdSituacaoOperacaoPagamento = true;
    } //-- void setCdSituacaoOperacaoPagamento(int) 

    /**
     * Sets the value of field 'cdTipoCanal'.
     * 
     * @param cdTipoCanal the value of field 'cdTipoCanal'.
     */
    public void setCdTipoCanal(int cdTipoCanal)
    {
        this._cdTipoCanal = cdTipoCanal;
        this._has_cdTipoCanal = true;
    } //-- void setCdTipoCanal(int) 

    /**
     * Sets the value of field 'cdTipoContratoNegocio'.
     * 
     * @param cdTipoContratoNegocio the value of field
     * 'cdTipoContratoNegocio'.
     */
    public void setCdTipoContratoNegocio(int cdTipoContratoNegocio)
    {
        this._cdTipoContratoNegocio = cdTipoContratoNegocio;
        this._has_cdTipoContratoNegocio = true;
    } //-- void setCdTipoContratoNegocio(int) 

    /**
     * Sets the value of field 'cdTipoTela'.
     * 
     * @param cdTipoTela the value of field 'cdTipoTela'.
     */
    public void setCdTipoTela(int cdTipoTela)
    {
        this._cdTipoTela = cdTipoTela;
        this._has_cdTipoTela = true;
    } //-- void setCdTipoTela(int) 

    /**
     * Sets the value of field 'contaPagtoDestino'.
     * 
     * @param contaPagtoDestino the value of field
     * 'contaPagtoDestino'.
     */
    public void setContaPagtoDestino(java.lang.String contaPagtoDestino)
    {
        this._contaPagtoDestino = contaPagtoDestino;
    } //-- void setContaPagtoDestino(java.lang.String) 

    /**
     * Sets the value of field 'dsEfetivacaoPagamento'.
     * 
     * @param dsEfetivacaoPagamento the value of field
     * 'dsEfetivacaoPagamento'.
     */
    public void setDsEfetivacaoPagamento(java.lang.String dsEfetivacaoPagamento)
    {
        this._dsEfetivacaoPagamento = dsEfetivacaoPagamento;
    } //-- void setDsEfetivacaoPagamento(java.lang.String) 

    /**
     * Sets the value of field 'dsEmpresa'.
     * 
     * @param dsEmpresa the value of field 'dsEmpresa'.
     */
    public void setDsEmpresa(java.lang.String dsEmpresa)
    {
        this._dsEmpresa = dsEmpresa;
    } //-- void setDsEmpresa(java.lang.String) 

    /**
     * Sets the value of field 'dsFavorecido'.
     * 
     * @param dsFavorecido the value of field 'dsFavorecido'.
     */
    public void setDsFavorecido(java.lang.String dsFavorecido)
    {
        this._dsFavorecido = dsFavorecido;
    } //-- void setDsFavorecido(java.lang.String) 

    /**
     * Sets the value of field 'dsIndicadorPagamento'.
     * 
     * @param dsIndicadorPagamento the value of field
     * 'dsIndicadorPagamento'.
     */
    public void setDsIndicadorPagamento(java.lang.String dsIndicadorPagamento)
    {
        this._dsIndicadorPagamento = dsIndicadorPagamento;
    } //-- void setDsIndicadorPagamento(java.lang.String) 

    /**
     * Sets the value of field 'dsMotivoSituacaoPagamento'.
     * 
     * @param dsMotivoSituacaoPagamento the value of field
     * 'dsMotivoSituacaoPagamento'.
     */
    public void setDsMotivoSituacaoPagamento(java.lang.String dsMotivoSituacaoPagamento)
    {
        this._dsMotivoSituacaoPagamento = dsMotivoSituacaoPagamento;
    } //-- void setDsMotivoSituacaoPagamento(java.lang.String) 

    /**
     * Sets the value of field 'dsOperacaoProdutoServico'.
     * 
     * @param dsOperacaoProdutoServico the value of field
     * 'dsOperacaoProdutoServico'.
     */
    public void setDsOperacaoProdutoServico(java.lang.String dsOperacaoProdutoServico)
    {
        this._dsOperacaoProdutoServico = dsOperacaoProdutoServico;
    } //-- void setDsOperacaoProdutoServico(java.lang.String) 

    /**
     * Sets the value of field 'dsRazaoSocial'.
     * 
     * @param dsRazaoSocial the value of field 'dsRazaoSocial'.
     */
    public void setDsRazaoSocial(java.lang.String dsRazaoSocial)
    {
        this._dsRazaoSocial = dsRazaoSocial;
    } //-- void setDsRazaoSocial(java.lang.String) 

    /**
     * Sets the value of field 'dsResumoProdutoServico'.
     * 
     * @param dsResumoProdutoServico the value of field
     * 'dsResumoProdutoServico'.
     */
    public void setDsResumoProdutoServico(java.lang.String dsResumoProdutoServico)
    {
        this._dsResumoProdutoServico = dsResumoProdutoServico;
    } //-- void setDsResumoProdutoServico(java.lang.String) 

    /**
     * Sets the value of field 'dsSituacaoOperacaoPagamento'.
     * 
     * @param dsSituacaoOperacaoPagamento the value of field
     * 'dsSituacaoOperacaoPagamento'.
     */
    public void setDsSituacaoOperacaoPagamento(java.lang.String dsSituacaoOperacaoPagamento)
    {
        this._dsSituacaoOperacaoPagamento = dsSituacaoOperacaoPagamento;
    } //-- void setDsSituacaoOperacaoPagamento(java.lang.String) 

    /**
     * Sets the value of field 'dtCreditoPagamento'.
     * 
     * @param dtCreditoPagamento the value of field
     * 'dtCreditoPagamento'.
     */
    public void setDtCreditoPagamento(java.lang.String dtCreditoPagamento)
    {
        this._dtCreditoPagamento = dtCreditoPagamento;
    } //-- void setDtCreditoPagamento(java.lang.String) 

    /**
     * Sets the value of field 'nrCnpjCpf'.
     * 
     * @param nrCnpjCpf the value of field 'nrCnpjCpf'.
     */
    public void setNrCnpjCpf(long nrCnpjCpf)
    {
        this._nrCnpjCpf = nrCnpjCpf;
        this._has_nrCnpjCpf = true;
    } //-- void setNrCnpjCpf(long) 

    /**
     * Sets the value of field 'nrContrato'.
     * 
     * @param nrContrato the value of field 'nrContrato'.
     */
    public void setNrContrato(long nrContrato)
    {
        this._nrContrato = nrContrato;
        this._has_nrContrato = true;
    } //-- void setNrContrato(long) 

    /**
     * Sets the value of field 'nrDigitoCnpjCpf'.
     * 
     * @param nrDigitoCnpjCpf the value of field 'nrDigitoCnpjCpf'.
     */
    public void setNrDigitoCnpjCpf(int nrDigitoCnpjCpf)
    {
        this._nrDigitoCnpjCpf = nrDigitoCnpjCpf;
        this._has_nrDigitoCnpjCpf = true;
    } //-- void setNrDigitoCnpjCpf(int) 

    /**
     * Sets the value of field 'nrFilialCnpjCpf'.
     * 
     * @param nrFilialCnpjCpf the value of field 'nrFilialCnpjCpf'.
     */
    public void setNrFilialCnpjCpf(int nrFilialCnpjCpf)
    {
        this._nrFilialCnpjCpf = nrFilialCnpjCpf;
        this._has_nrFilialCnpjCpf = true;
    } //-- void setNrFilialCnpjCpf(int) 

    /**
     * Sets the value of field 'vlEfetivoPagamento'.
     * 
     * @param vlEfetivoPagamento the value of field
     * 'vlEfetivoPagamento'.
     */
    public void setVlEfetivoPagamento(java.math.BigDecimal vlEfetivoPagamento)
    {
        this._vlEfetivoPagamento = vlEfetivoPagamento;
    } //-- void setVlEfetivoPagamento(java.math.BigDecimal) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultarpagtosindantpostergacao.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultarpagtosindantpostergacao.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultarpagtosindantpostergacao.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarpagtosindantpostergacao.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
