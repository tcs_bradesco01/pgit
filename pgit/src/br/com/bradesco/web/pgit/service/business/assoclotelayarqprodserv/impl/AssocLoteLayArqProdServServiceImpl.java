/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/implementation.ftl,v $
 * $Id: implementation.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: implementation.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */
 
package br.com.bradesco.web.pgit.service.business.assoclotelayarqprodserv.impl;

import java.util.ArrayList;
import java.util.List;

import br.com.bradesco.web.pgit.service.business.assoclotelayarqprodserv.IAssocLoteLayArqProdServService;
import br.com.bradesco.web.pgit.service.business.assoclotelayarqprodserv.IAssocLoteLayArqProdServServiceConstants;
import br.com.bradesco.web.pgit.service.business.assoclotelayarqprodserv.bean.DetalharAsLotLaArProdServEntradaDTO;
import br.com.bradesco.web.pgit.service.business.assoclotelayarqprodserv.bean.DetalharAsLotLaArProdServSaidaDTO;
import br.com.bradesco.web.pgit.service.business.assoclotelayarqprodserv.bean.ExcluirAsLotLaArProdServEntradaDTO;
import br.com.bradesco.web.pgit.service.business.assoclotelayarqprodserv.bean.ExcluirAsLotLaArProdServSaidaDTO;
import br.com.bradesco.web.pgit.service.business.assoclotelayarqprodserv.bean.IncluirAsLotLaArProdServEntradaDTO;
import br.com.bradesco.web.pgit.service.business.assoclotelayarqprodserv.bean.IncluirAsLotLaArProdServSaidaDTO;
import br.com.bradesco.web.pgit.service.business.assoclotelayarqprodserv.bean.ListarAsLotLaArProdServEntradaDTO;
import br.com.bradesco.web.pgit.service.business.assoclotelayarqprodserv.bean.ListarAsLotLaArProdServSaidaDTO;
import br.com.bradesco.web.pgit.service.data.pdc.FactoryAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.detalharasslotelayoutarqservico.request.DetalharAssLoteLayoutArqServicoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.detalharasslotelayoutarqservico.response.DetalharAssLoteLayoutArqServicoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.excluirasslotelayoutarqservico.request.ExcluirAssLoteLayoutArqServicoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.excluirasslotelayoutarqservico.response.ExcluirAssLoteLayoutArqServicoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.incluirasslotelayoutarqservico.request.IncluirAssLoteLayoutArqServicoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.incluirasslotelayoutarqservico.response.IncluirAssLoteLayoutArqServicoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarasslotelayoutarqservico.request.ListarAssLoteLayoutArqServicoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarasslotelayoutarqservico.response.ListarAssLoteLayoutArqServicoResponse;


/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Implementa��o do adaptador: AssocLoteOperacaoProdServ
 * </p>
 * 
 * @comment C�DIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public class AssocLoteLayArqProdServServiceImpl implements IAssocLoteLayArqProdServService {

	/** Atributo factoryAdapter. */
	private FactoryAdapter factoryAdapter;
	
    /**
     * Construtor.
     */
    public AssocLoteLayArqProdServServiceImpl() {
        // TODO: Implementa��o
    }
    
    /**
     * M�todo de exemplo.
     *
     * @see br.com.bradesco.web.pgic.service.business.assocloteoperacaoprodserv.IAssocLoteOperacaoProdServ#sampleAssocLoteOperacaoProdServ()
     */
    public void sampleAssocLoteOperacaoProdServ() {
        // TODO: Implementa�ao
    }

	/**
	 * Get: factoryAdapter.
	 *
	 * @return factoryAdapter
	 */
	public FactoryAdapter getFactoryAdapter() {
		return factoryAdapter;
	}

	/**
	 * Set: factoryAdapter.
	 *
	 * @param factoryAdapter the factory adapter
	 */
	public void setFactoryAdapter(FactoryAdapter factoryAdapter) {
		this.factoryAdapter = factoryAdapter;
	}
	
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.assoclotelayarqprodserv.IAssocLoteLayArqProdServService#listarAssocLoteLayArProdServ(br.com.bradesco.web.pgit.service.business.assoclotelayarqprodserv.bean.ListarAsLotLaArProdServEntradaDTO)
	 */
	public List<ListarAsLotLaArProdServSaidaDTO> listarAssocLoteLayArProdServ (ListarAsLotLaArProdServEntradaDTO listarAsLotLaArProdServEntradaDTO) {
		
		List<ListarAsLotLaArProdServSaidaDTO> listaRetorno = new ArrayList<ListarAsLotLaArProdServSaidaDTO>();
		ListarAssLoteLayoutArqServicoRequest request = new ListarAssLoteLayoutArqServicoRequest();
		ListarAssLoteLayoutArqServicoResponse response = new ListarAssLoteLayoutArqServicoResponse();
		
		request.setCdTipoLoteLayout(listarAsLotLaArProdServEntradaDTO.getTipoLote());
		request.setCdTipoLayoutArquivo(listarAsLotLaArProdServEntradaDTO.getTipoLayoutArquivo());
		request.setCdProdutoServicoOper(listarAsLotLaArProdServEntradaDTO.getTipoServico());
		request.setQtConsultas(IAssocLoteLayArqProdServServiceConstants.QTDE_CONSULTAS_LISTAR);
		
		response = getFactoryAdapter().getListarAssLoteLayoutArqServicoPDCAdapter().invokeProcess(request);		
		
		ListarAsLotLaArProdServSaidaDTO saidaDTO;
		
		for (int i=0; i<response.getOcorrenciasCount();i++){
			saidaDTO = new ListarAsLotLaArProdServSaidaDTO();
			saidaDTO.setCodMensagem(response.getCodMensagem());
			saidaDTO.setMensagem(response.getMensagem());
			saidaDTO.setCdTipoLayoutArquivo(response.getOcorrencias(i).getCdTipoLayoutArquivo());
			saidaDTO.setDsTipoLayoutArquivo(response.getOcorrencias(i).getDsTipoLayoutArquivo());
			saidaDTO.setCdTipoLote(response.getOcorrencias(i).getCdTipoLoteLayout());
			saidaDTO.setDsTipoLote(response.getOcorrencias(i).getDsTipoLoteLayout());
			saidaDTO.setCdTipoServico(response.getOcorrencias(i).getCdProdutoServicoOper());
			saidaDTO.setDsTipoServico(response.getOcorrencias(i).getDsProdutoServicoOper());
		
			listaRetorno.add(saidaDTO);
		}
		
		return listaRetorno;
	}
	
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.assoclotelayarqprodserv.IAssocLoteLayArqProdServService#incluirAssocLoteLayArProdServ(br.com.bradesco.web.pgit.service.business.assoclotelayarqprodserv.bean.IncluirAsLotLaArProdServEntradaDTO)
	 */
	public IncluirAsLotLaArProdServSaidaDTO incluirAssocLoteLayArProdServ (IncluirAsLotLaArProdServEntradaDTO incluirAsLotLaArProdServEntradaDTO) {
		
		IncluirAsLotLaArProdServSaidaDTO incluirAsLotLaArProdServSaidaDTO = new IncluirAsLotLaArProdServSaidaDTO();
		IncluirAssLoteLayoutArqServicoRequest incluirAssLoteLayoutArqServicoRequest = new IncluirAssLoteLayoutArqServicoRequest();
		IncluirAssLoteLayoutArqServicoResponse incluirAssLoteLayoutArqServicoResponse = new IncluirAssLoteLayoutArqServicoResponse();
		
		incluirAssLoteLayoutArqServicoRequest.setCdTipoLoteLayout(incluirAsLotLaArProdServEntradaDTO.getTipoLote());
		incluirAssLoteLayoutArqServicoRequest.setCdTipoLayoutArquivo(incluirAsLotLaArProdServEntradaDTO.getTipoLayoutArquivo());
		incluirAssLoteLayoutArqServicoRequest.setCdProdutoServicoOper(incluirAsLotLaArProdServEntradaDTO.getTipoServico());
		incluirAssLoteLayoutArqServicoRequest.setQtConsultas(0);
		
		
		incluirAssLoteLayoutArqServicoResponse = getFactoryAdapter().getIncluirAssLoteLayoutArqServicoPDCAdapter().invokeProcess(incluirAssLoteLayoutArqServicoRequest);
		
		incluirAsLotLaArProdServSaidaDTO.setCodMensagem(incluirAssLoteLayoutArqServicoResponse.getCodMensagem());
		incluirAsLotLaArProdServSaidaDTO.setMensagem(incluirAssLoteLayoutArqServicoResponse.getMensagem());
		
	
		return incluirAsLotLaArProdServSaidaDTO;
	}
	
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.assoclotelayarqprodserv.IAssocLoteLayArqProdServService#excluirAssocLoteLayArProdServ(br.com.bradesco.web.pgit.service.business.assoclotelayarqprodserv.bean.ExcluirAsLotLaArProdServEntradaDTO)
	 */
	public ExcluirAsLotLaArProdServSaidaDTO excluirAssocLoteLayArProdServ (ExcluirAsLotLaArProdServEntradaDTO excluirAsLotLaArProdServEntradaDTO) {
		ExcluirAsLotLaArProdServSaidaDTO excluirAsLotLaArProdServSaidaDTO = new ExcluirAsLotLaArProdServSaidaDTO();
		ExcluirAssLoteLayoutArqServicoRequest excluirAssLoteLayoutArqServicoRequest = new ExcluirAssLoteLayoutArqServicoRequest();
		ExcluirAssLoteLayoutArqServicoResponse excluirAssLoteLayoutArqServicoResponse = new ExcluirAssLoteLayoutArqServicoResponse();
		
		excluirAssLoteLayoutArqServicoRequest.setCdTipoLoteLayout(excluirAsLotLaArProdServEntradaDTO.getTipoLote());
		excluirAssLoteLayoutArqServicoRequest.setCdTipoLayoutArquivo(excluirAsLotLaArProdServEntradaDTO.getTipoLayoutArquivo());
		excluirAssLoteLayoutArqServicoRequest.setCdProdutoServicoOper(excluirAsLotLaArProdServEntradaDTO.getTipoServico());
		excluirAssLoteLayoutArqServicoRequest.setQtConsultas(0);
		
		excluirAssLoteLayoutArqServicoResponse = getFactoryAdapter().getExcluirAssLoteLayoutArqServicoPDCAdapter().invokeProcess(excluirAssLoteLayoutArqServicoRequest);
		
		excluirAsLotLaArProdServSaidaDTO.setCodMensagem(excluirAssLoteLayoutArqServicoResponse.getCodMensagem());
		excluirAsLotLaArProdServSaidaDTO.setMensagem(excluirAssLoteLayoutArqServicoResponse.getMensagem());
		
	
		return excluirAsLotLaArProdServSaidaDTO;
	}
	
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.assoclotelayarqprodserv.IAssocLoteLayArqProdServService#detalharAssocLoteLayArProdServ(br.com.bradesco.web.pgit.service.business.assoclotelayarqprodserv.bean.DetalharAsLotLaArProdServEntradaDTO)
	 */
	public DetalharAsLotLaArProdServSaidaDTO detalharAssocLoteLayArProdServ (DetalharAsLotLaArProdServEntradaDTO detalharAsLotLaArProdServEntradaDTO) {
		
		DetalharAsLotLaArProdServSaidaDTO saidaDTO = new DetalharAsLotLaArProdServSaidaDTO();
		DetalharAssLoteLayoutArqServicoRequest request = new DetalharAssLoteLayoutArqServicoRequest();
		DetalharAssLoteLayoutArqServicoResponse response = new DetalharAssLoteLayoutArqServicoResponse();
		
		request.setCdTipoLoteLayout(detalharAsLotLaArProdServEntradaDTO.getTipoLote());
		request.setCdTipoLayoutArquivo(detalharAsLotLaArProdServEntradaDTO.getTipoLayoutArquivo());
		request.setCdProdutoServicoOper(detalharAsLotLaArProdServEntradaDTO.getTipoServico());
		request.setQtConsultas(0);
		
		response = getFactoryAdapter().getDetalharAssLoteLayoutArqServicoPDCAdapter().invokeProcess(request);
		
		saidaDTO = new DetalharAsLotLaArProdServSaidaDTO();
		saidaDTO.setCodMensagem(response.getCodMensagem());
		saidaDTO.setMensagem(response.getMensagem());
		
		saidaDTO.setCdLayoutArquivo(response.getOcorrencias(0).getCdTipoLayoutArquivo());
		saidaDTO.setDsLayoutArquivo(response.getOcorrencias(0).getDsTipoLayoutArquivo());
		saidaDTO.setCdTipoLote(response.getOcorrencias(0).getCdTipoLoteLayout());
		saidaDTO.setDsTipoLote(response.getOcorrencias(0).getDsTipoLoteLayout());
		saidaDTO.setCdTipoServico(response.getOcorrencias(0).getCdProdutoServicoOper());
		saidaDTO.setDsTipoServico(response.getOcorrencias(0).getDsProdutoServicoOper());
		saidaDTO.setCdCanalInclusao(response.getOcorrencias(0).getCdCanalInclusao());
		saidaDTO.setDsCanalInclusao(response.getOcorrencias(0).getDsCanalInclusao());
		saidaDTO.setUsuarioInclusao(response.getOcorrencias(0).getCdAutenSegrcInclusao());
		saidaDTO.setComplementoInclusao(response.getOcorrencias(0).getNmOperFluxoInclusao());
		saidaDTO.setDataHoraInclusao(response.getOcorrencias(0).getHrInclusaoRegistro());
		saidaDTO.setCdCanalManutencao(response.getOcorrencias(0).getCdCanalManutencao());
		saidaDTO.setDsCanalManutencao(response.getOcorrencias(0).getDsCanalManutencao());
		saidaDTO.setUsuarioManutencao(response.getOcorrencias(0).getCdAutenSegrcManutencao());
		saidaDTO.setComplementoManutencao(response.getOcorrencias(0).getNmOperFluxoManutencao());
		saidaDTO.setDataHoraManutencao(response.getOcorrencias(0).getHrManutencaoRegistro());
		
		return saidaDTO;
	}

    
    
}

