package br.com.bradesco.web.pgit.service.business.manterperfiltrocaarqlayout.bean;

public class IncluirPerfilTrocaArquivoOcorrenciasEntradaDTO {
	
	/** Atributo cdProdutoOperacaoRelacionado. */
	private Integer cdProdutoOperacaoRelacionado;
	
	/** Atributo cdIndicadorLayoutProprio. */
	private Integer cdIndicadorLayoutProprio;
	
	/** Atributo cdApliFormat. */
	private Integer cdApliFormat;
	
	private boolean selecionado;
	
	private String dsApliFormat;
	
	private String dsIndicadorLayoutProprio;
	
	private String dsProdutoOperacaoRelacionado;
	
	
	public IncluirPerfilTrocaArquivoOcorrenciasEntradaDTO(
			Integer cdProdutoOperacaoRelacionado,
			Integer cdIndicadorLayoutProprio, Integer cdApliFormat,
			boolean selecionado, String dsProdutoOperacaoRelacionado) {
		super();
		this.cdProdutoOperacaoRelacionado = cdProdutoOperacaoRelacionado;
		this.cdIndicadorLayoutProprio = cdIndicadorLayoutProprio;
		this.cdApliFormat = cdApliFormat;
		this.selecionado = selecionado;
		this.dsProdutoOperacaoRelacionado = dsProdutoOperacaoRelacionado;
		
	}
	
	public IncluirPerfilTrocaArquivoOcorrenciasEntradaDTO(
			Integer cdProdutoOperacaoRelacionado,
			Integer cdIndicadorLayoutProprio, Integer cdApliFormat,
			boolean selecionado, String dsProdutoOperacaoRelacionado, String dsApliFormat) {
		super();
		this.cdProdutoOperacaoRelacionado = cdProdutoOperacaoRelacionado;
		this.cdIndicadorLayoutProprio = cdIndicadorLayoutProprio;
		this.cdApliFormat = cdApliFormat;
		this.selecionado = selecionado;
		this.dsApliFormat = dsApliFormat;
		this.dsProdutoOperacaoRelacionado = dsProdutoOperacaoRelacionado;
		
		if(cdIndicadorLayoutProprio == 1){
			setDsIndicadorLayoutProprio("Sim");
		}else{
			setDsIndicadorLayoutProprio("N�o");
		}
	}

	

	public IncluirPerfilTrocaArquivoOcorrenciasEntradaDTO() {
		super();
	}


	/**
	 * Get: cdProdutoOperacaoRelacionado.
	 *
	 * @return cdProdutoOperacaoRelacionado
	 */
	public Integer getCdProdutoOperacaoRelacionado() {
		return cdProdutoOperacaoRelacionado;
	}
	
	/**
	 * Set: cdProdutoOperacaoRelacionado.
	 *
	 * @param cdProdutoOperacaoRelacionado the codigo produto operacao relacionado
	 */
	public void setCdProdutoOperacaoRelacionado(Integer cdProdutoOperacaoRelacionado) {
		this.cdProdutoOperacaoRelacionado = cdProdutoOperacaoRelacionado;
	}
	
	
	/**
	 * Get: cdIndicadorLayoutProprio.
	 *
	 * @return cdIndicadorLayoutProprio
	 */
	public Integer getCdIndicadorLayoutProprio() {
		return cdIndicadorLayoutProprio;
	}
	
	/**
	 * Set: cdIndicadorLayoutProprio.
	 *
	 * @param cdIndicadorLayoutProprio the indicador layout proprio
	 */
	public void setCdIndicadorLayoutProprio(Integer cdIndicadorLayoutProprio) {
		this.cdIndicadorLayoutProprio = cdIndicadorLayoutProprio;
	}
	
	/**
	 * Get: cdApliFormat.
	 *
	 * @return cdApliFormat
	 */
	public Integer getCdApliFormat() {
		return cdApliFormat;
	}
	
	/**
	 * Set: cdApliFormat.
	 *
	 * @param cdApliFormat the codigo aplicacao formatada
	 */
	public void setCdApliFormat(Integer cdApliFormat) {
		this.cdApliFormat = cdApliFormat;
	}

	public boolean isSelecionado() {
		return selecionado;
	}

	public void setSelecionado(boolean selecionado) {
		this.selecionado = selecionado;
	}

	public String getDsApliFormat() {
		return dsApliFormat;
	}

	public void setDsApliFormat(String dsApliFormat) {
		this.dsApliFormat = dsApliFormat;
	}


	public String getDsIndicadorLayoutProprio() {
		return dsIndicadorLayoutProprio;
	}


	public void setDsIndicadorLayoutProprio(String dsIndicadorLayoutProprio) {
		this.dsIndicadorLayoutProprio = dsIndicadorLayoutProprio;
	}


	public String getDsProdutoOperacaoRelacionado() {
		return dsProdutoOperacaoRelacionado;
	}


	public void setDsProdutoOperacaoRelacionado(String dsProdutoOperacaoRelacionado) {
		this.dsProdutoOperacaoRelacionado = dsProdutoOperacaoRelacionado;
	}
	
	
}
