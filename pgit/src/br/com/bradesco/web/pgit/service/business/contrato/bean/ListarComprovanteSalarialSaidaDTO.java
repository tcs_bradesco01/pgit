/*
 * Nome: br.com.bradesco.web.pgit.service.business.contrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 19/02/2016
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.contrato.bean;

import java.util.List;

/**
 * Nome: ListarComprovanteSalarialSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarComprovanteSalarialSaidaDTO {

    /** The cod mensagem. */
    private String codMensagem = null;

    /** The mensagem. */
    private String mensagem = null;

    /** The nr comprovantes. */
    private Integer nrComprovantes = null;
    
    /** The qtde maxima detalhe comprovante. */
    private Integer qtdeMaximaDetalheComprovante = null;

    /** The qtde emissao comprovante pagamento. */
    private Integer qtdeEmissaoComprovantePagamento = null;

    /** The cd midia comprovante salarial. */
    private String cdMidiaComprovanteSalarial = null;

    /** The ocorrencias. */
    private List<ListarComprovanteSalarialOcorrenciasSaidaDTO> ocorrencias = null;

    /**
     * Instancia um novo ListarComprovanteSalarialSaidaDTO
     *
     * @see
     */
    public ListarComprovanteSalarialSaidaDTO() {
        super();
    }

    /**
     * Sets the cod mensagem.
     *
     * @param codMensagem the cod mensagem
     */
    public void setCodMensagem(String codMensagem) {
        this.codMensagem = codMensagem;
    }

    /**
     * Gets the cod mensagem.
     *
     * @return the cod mensagem
     */
    public String getCodMensagem() {
        return this.codMensagem;
    }

    /**
     * Sets the mensagem.
     *
     * @param mensagem the mensagem
     */
    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }

    /**
     * Gets the mensagem.
     *
     * @return the mensagem
     */
    public String getMensagem() {
        return this.mensagem;
    }

    /**
     * Sets the nr comprovantes.
     *
     * @param nrComprovantes the nr comprovantes
     */
    public void setNrComprovantes(Integer nrComprovantes) {
        this.nrComprovantes = nrComprovantes;
    }

    /**
     * Gets the nr comprovantes.
     *
     * @return the nr comprovantes
     */
    public Integer getNrComprovantes() {
        return this.nrComprovantes;
    }

    /**
     * Sets the ocorrencias.
     *
     * @param ocorrencias the ocorrencias
     */
    public void setOcorrencias(List<ListarComprovanteSalarialOcorrenciasSaidaDTO> ocorrencias) {
        this.ocorrencias = ocorrencias;
    }

    /**
     * Gets the ocorrencias.
     *
     * @return the ocorrencias
     */
    public List<ListarComprovanteSalarialOcorrenciasSaidaDTO> getOcorrencias() {
        return this.ocorrencias;
    }

	/**
	 * Get: qtdeMaximaDetalheComprovante.
	 *
	 * @return qtdeMaximaDetalheComprovante
	 */
	public Integer getQtdeMaximaDetalheComprovante() {
		return qtdeMaximaDetalheComprovante;
	}

	/**
	 * Set: qtdeMaximaDetalheComprovante.
	 *
	 * @param qtdeMaximaDetalheComprovante the qtde maxima detalhe comprovante
	 */
	public void setQtdeMaximaDetalheComprovante(Integer qtdeMaximaDetalheComprovante) {
		this.qtdeMaximaDetalheComprovante = qtdeMaximaDetalheComprovante;
	}

	/**
	 * Get: qtdeEmissaoComprovantePagamento.
	 *
	 * @return qtdeEmissaoComprovantePagamento
	 */
	public Integer getQtdeEmissaoComprovantePagamento() {
		return qtdeEmissaoComprovantePagamento;
	}

	/**
	 * Set: qtdeEmissaoComprovantePagamento.
	 *
	 * @param qtdeEmissaoComprovantePagamento the qtde emissao comprovante pagamento
	 */
	public void setQtdeEmissaoComprovantePagamento(
			Integer qtdeEmissaoComprovantePagamento) {
		this.qtdeEmissaoComprovantePagamento = qtdeEmissaoComprovantePagamento;
	}

	/**
	 * Get: cdMidiaComprovanteSalarial.
	 *
	 * @return cdMidiaComprovanteSalarial
	 */
	public String getCdMidiaComprovanteSalarial() {
		return cdMidiaComprovanteSalarial;
	}

	/**
	 * Set: cdMidiaComprovanteSalarial.
	 *
	 * @param cdMidiaComprovanteSalarial the cd midia comprovante salarial
	 */
	public void setCdMidiaComprovanteSalarial(String cdMidiaComprovanteSalarial) {
		this.cdMidiaComprovanteSalarial = cdMidiaComprovanteSalarial;
	}
}