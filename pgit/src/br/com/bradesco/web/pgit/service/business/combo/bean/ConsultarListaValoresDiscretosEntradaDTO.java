/*
 * Nome: br.com.bradesco.web.pgit.service.business.combo.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.combo.bean;

/**
 * Nome: ConsultarListaValoresDiscretosEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarListaValoresDiscretosEntradaDTO {
	
	/** Atributo numeroCombo. */
	private Integer numeroCombo;

	/**
	 * Get: numeroCombo.
	 *
	 * @return numeroCombo
	 */
	public Integer getNumeroCombo() {
		return numeroCombo;
	}

	/**
	 * Set: numeroCombo.
	 *
	 * @param numeroCombo the numero combo
	 */
	public void setNumeroCombo(Integer numeroCombo) {
		this.numeroCombo = numeroCombo;
	}
	
	

}
