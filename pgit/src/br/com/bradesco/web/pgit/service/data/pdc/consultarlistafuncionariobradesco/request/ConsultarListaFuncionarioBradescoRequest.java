/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultarlistafuncionariobradesco.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ConsultarListaFuncionarioBradescoRequest.
 * 
 * @version $Revision$ $Date$
 */
public class ConsultarListaFuncionarioBradescoRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdFuncionarioBradesco
     */
    private long _cdFuncionarioBradesco = 0;

    /**
     * keeps track of state for field: _cdFuncionarioBradesco
     */
    private boolean _has_cdFuncionarioBradesco;


      //----------------/
     //- Constructors -/
    //----------------/

    public ConsultarListaFuncionarioBradescoRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarlistafuncionariobradesco.request.ConsultarListaFuncionarioBradescoRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdFuncionarioBradesco
     * 
     */
    public void deleteCdFuncionarioBradesco()
    {
        this._has_cdFuncionarioBradesco= false;
    } //-- void deleteCdFuncionarioBradesco() 

    /**
     * Returns the value of field 'cdFuncionarioBradesco'.
     * 
     * @return long
     * @return the value of field 'cdFuncionarioBradesco'.
     */
    public long getCdFuncionarioBradesco()
    {
        return this._cdFuncionarioBradesco;
    } //-- long getCdFuncionarioBradesco() 

    /**
     * Method hasCdFuncionarioBradesco
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFuncionarioBradesco()
    {
        return this._has_cdFuncionarioBradesco;
    } //-- boolean hasCdFuncionarioBradesco() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdFuncionarioBradesco'.
     * 
     * @param cdFuncionarioBradesco the value of field
     * 'cdFuncionarioBradesco'.
     */
    public void setCdFuncionarioBradesco(long cdFuncionarioBradesco)
    {
        this._cdFuncionarioBradesco = cdFuncionarioBradesco;
        this._has_cdFuncionarioBradesco = true;
    } //-- void setCdFuncionarioBradesco(long) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ConsultarListaFuncionarioBradescoRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultarlistafuncionariobradesco.request.ConsultarListaFuncionarioBradescoRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultarlistafuncionariobradesco.request.ConsultarListaFuncionarioBradescoRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultarlistafuncionariobradesco.request.ConsultarListaFuncionarioBradescoRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarlistafuncionariobradesco.request.ConsultarListaFuncionarioBradescoRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
