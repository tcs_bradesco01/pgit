/*
 * Nome: br.com.bradesco.web.pgit.service.business.emissaoautcomp.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.emissaoautcomp.bean;

/**
 * Nome: ExcluirEmissaoAutCompEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ExcluirEmissaoAutCompEntradaDTO {
	
	/** Atributo contratoPssoaJuridContr. */
	private long contratoPssoaJuridContr;
	
	/** Atributo contratoTpoContrNegoc. */
	private int contratoTpoContrNegoc;
	
	/** Atributo contratoSegContrNegoc. */
	private long contratoSegContrNegoc;
	
	/** Atributo servicoEmissaoContratado. */
	private int servicoEmissaoContratado;	
	
	/** Atributo tipoServico. */
	private int tipoServico;
	
	/** Atributo modalidadeServico. */
	private int modalidadeServico;
	
	/** Atributo tipoRelacionamento. */
	private int tipoRelacionamento;
	
	/**
	 * Get: contratoPssoaJuridContr.
	 *
	 * @return contratoPssoaJuridContr
	 */
	public long getContratoPssoaJuridContr() {
		return contratoPssoaJuridContr;
	}
	
	/**
	 * Set: contratoPssoaJuridContr.
	 *
	 * @param contratoPssoaJuridContr the contrato pssoa jurid contr
	 */
	public void setContratoPssoaJuridContr(long contratoPssoaJuridContr) {
		this.contratoPssoaJuridContr = contratoPssoaJuridContr;
	}
	
	/**
	 * Get: contratoSegContrNegoc.
	 *
	 * @return contratoSegContrNegoc
	 */
	public long getContratoSegContrNegoc() {
		return contratoSegContrNegoc;
	}
	
	/**
	 * Set: contratoSegContrNegoc.
	 *
	 * @param contratoSegContrNegoc the contrato seg contr negoc
	 */
	public void setContratoSegContrNegoc(long contratoSegContrNegoc) {
		this.contratoSegContrNegoc = contratoSegContrNegoc;
	}
	
	/**
	 * Get: contratoTpoContrNegoc.
	 *
	 * @return contratoTpoContrNegoc
	 */
	public int getContratoTpoContrNegoc() {
		return contratoTpoContrNegoc;
	}
	
	/**
	 * Set: contratoTpoContrNegoc.
	 *
	 * @param contratoTpoContrNegoc the contrato tpo contr negoc
	 */
	public void setContratoTpoContrNegoc(int contratoTpoContrNegoc) {
		this.contratoTpoContrNegoc = contratoTpoContrNegoc;
	}
	
	/**
	 * Get: modalidadeServico.
	 *
	 * @return modalidadeServico
	 */
	public int getModalidadeServico() {
		return modalidadeServico;
	}
	
	/**
	 * Set: modalidadeServico.
	 *
	 * @param modalidadeServico the modalidade servico
	 */
	public void setModalidadeServico(int modalidadeServico) {
		this.modalidadeServico = modalidadeServico;
	}

	/**
	 * Get: servicoEmissaoContratado.
	 *
	 * @return servicoEmissaoContratado
	 */
	public int getServicoEmissaoContratado() {
		return servicoEmissaoContratado;
	}
	
	/**
	 * Set: servicoEmissaoContratado.
	 *
	 * @param servicoEmissaoContratado the servico emissao contratado
	 */
	public void setServicoEmissaoContratado(int servicoEmissaoContratado) {
		this.servicoEmissaoContratado = servicoEmissaoContratado;
	}
	
	/**
	 * Get: tipoRelacionamento.
	 *
	 * @return tipoRelacionamento
	 */
	public int getTipoRelacionamento() {
		return tipoRelacionamento;
	}
	
	/**
	 * Set: tipoRelacionamento.
	 *
	 * @param tipoRelacionamento the tipo relacionamento
	 */
	public void setTipoRelacionamento(int tipoRelacionamento) {
		this.tipoRelacionamento = tipoRelacionamento;
	}
	
	/**
	 * Get: tipoServico.
	 *
	 * @return tipoServico
	 */
	public int getTipoServico() {
		return tipoServico;
	}
	
	/**
	 * Set: tipoServico.
	 *
	 * @param tipoServico the tipo servico
	 */
	public void setTipoServico(int tipoServico) {
		this.tipoServico = tipoServico;
	}
	
	
}
