/*
 * Nome: br.com.bradesco.web.pgit.service.business.manterambienteoperacaocontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.manterambienteoperacaocontrato.bean;

import java.util.Date;

/**
 * Nome: SolicitacaoMudancaAmbienteContratoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class SolicitacaoMudancaAmbienteContratoSaidaDTO {
	
	/** Atributo servico. */
	private String servico;
	
	/** Atributo ambienteOperacao. */
	private String ambienteOperacao;
	
	/** Atributo situacaoSolicitacao. */
	private String situacaoSolicitacao;
	
	/** Atributo dataProgramada. */
	private Date dataProgramada; 
	
	/** Atributo dataHoraSolicitacao. */
	private Date dataHoraSolicitacao;
	
	
	/**
	 * Get: ambienteOperacao.
	 *
	 * @return ambienteOperacao
	 */
	public String getAmbienteOperacao() {
		return ambienteOperacao;
	}
	
	/**
	 * Set: ambienteOperacao.
	 *
	 * @param ambienteOperacao the ambiente operacao
	 */
	public void setAmbienteOperacao(String ambienteOperacao) {
		this.ambienteOperacao = ambienteOperacao;
	}
	
	/**
	 * Get: dataHoraSolicitacao.
	 *
	 * @return dataHoraSolicitacao
	 */
	public Date getDataHoraSolicitacao() {
		return dataHoraSolicitacao;
	}
	
	/**
	 * Set: dataHoraSolicitacao.
	 *
	 * @param dataHoraSolicitacao the data hora solicitacao
	 */
	public void setDataHoraSolicitacao(Date dataHoraSolicitacao) {
		this.dataHoraSolicitacao = dataHoraSolicitacao;
	}
	
	/**
	 * Get: dataProgramada.
	 *
	 * @return dataProgramada
	 */
	public Date getDataProgramada() {
		return dataProgramada;
	}
	
	/**
	 * Set: dataProgramada.
	 *
	 * @param dataProgramada the data programada
	 */
	public void setDataProgramada(Date dataProgramada) {
		this.dataProgramada = dataProgramada;
	}
	
	/**
	 * Get: servico.
	 *
	 * @return servico
	 */
	public String getServico() {
		return servico;
	}
	
	/**
	 * Set: servico.
	 *
	 * @param servico the servico
	 */
	public void setServico(String servico) {
		this.servico = servico;
	}
	
	/**
	 * Get: situacaoSolicitacao.
	 *
	 * @return situacaoSolicitacao
	 */
	public String getSituacaoSolicitacao() {
		return situacaoSolicitacao;
	}
	
	/**
	 * Set: situacaoSolicitacao.
	 *
	 * @param situacaoSolicitacao the situacao solicitacao
	 */
	public void setSituacaoSolicitacao(String situacaoSolicitacao) {
		this.situacaoSolicitacao = situacaoSolicitacao;
	}
}
