/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantersolicretransmissaoarquivosretorno.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantersolicretransmissaoarquivosretorno.bean;

/**
 * Nome: ListarSolBaseRetransmissaoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarSolBaseRetransmissaoSaidaDTO {
	
	/** Atributo codMensagem. */
	private String codMensagem;
    
    /** Atributo mensagem. */
    private String mensagem;
    
    /** Atributo qtdeRegistros. */
    private Integer qtdeRegistros;    
    
    /** Atributo nrSolicitacaoPagamento. */
    private Integer nrSolicitacaoPagamento;
    
    /** Atributo hrSolicitacaoSolicitacao. */
    private String hrSolicitacaoSolicitacao;
    
    /** Atributo cdSituacaoSolicitacao. */
    private Integer cdSituacaoSolicitacao;
    
    /** Atributo dsSituacaoSolicitacao. */
    private String dsSituacaoSolicitacao;
    
    /** Atributo resultadoProcessamento. */
    private String resultadoProcessamento;
    
    /** Atributo hrAtendimentoSolicitacao. */
    private String hrAtendimentoSolicitacao;
    
    /** Atributo qtdeRegistroSolicitacao. */
    private Long qtdeRegistroSolicitacao;    
    
    /** Atributo hrSolicitacaoSolicitacaoFormatada. */
    private String hrSolicitacaoSolicitacaoFormatada;
    
    /** Atributo hrAtendimentoSolicitacaoFormatada. */
    private String hrAtendimentoSolicitacaoFormatada;
    
    /** Atributo qtdeRegistroSolicitacaoFormatado. */
    private String qtdeRegistroSolicitacaoFormatado;
    
	/**
	 * Get: cdSituacaoSolicitacao.
	 *
	 * @return cdSituacaoSolicitacao
	 */
	public Integer getCdSituacaoSolicitacao() {
		return cdSituacaoSolicitacao;
	}
	
	/**
	 * Set: cdSituacaoSolicitacao.
	 *
	 * @param cdSituacaoSolicitacao the cd situacao solicitacao
	 */
	public void setCdSituacaoSolicitacao(Integer cdSituacaoSolicitacao) {
		this.cdSituacaoSolicitacao = cdSituacaoSolicitacao;
	}
	
	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}
	
	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}
	
	/**
	 * Get: dsSituacaoSolicitacao.
	 *
	 * @return dsSituacaoSolicitacao
	 */
	public String getDsSituacaoSolicitacao() {
		return dsSituacaoSolicitacao;
	}
	
	/**
	 * Set: dsSituacaoSolicitacao.
	 *
	 * @param dsSituacaoSolicitacao the ds situacao solicitacao
	 */
	public void setDsSituacaoSolicitacao(String dsSituacaoSolicitacao) {
		this.dsSituacaoSolicitacao = dsSituacaoSolicitacao;
	}
	
	/**
	 * Get: hrAtendimentoSolicitacao.
	 *
	 * @return hrAtendimentoSolicitacao
	 */
	public String getHrAtendimentoSolicitacao() {
		return hrAtendimentoSolicitacao;
	}
	
	/**
	 * Set: hrAtendimentoSolicitacao.
	 *
	 * @param hrAtendimentoSolicitacao the hr atendimento solicitacao
	 */
	public void setHrAtendimentoSolicitacao(String hrAtendimentoSolicitacao) {
		this.hrAtendimentoSolicitacao = hrAtendimentoSolicitacao;
	}
	
	/**
	 * Get: hrAtendimentoSolicitacaoFormatada.
	 *
	 * @return hrAtendimentoSolicitacaoFormatada
	 */
	public String getHrAtendimentoSolicitacaoFormatada() {
		return hrAtendimentoSolicitacaoFormatada;
	}
	
	/**
	 * Set: hrAtendimentoSolicitacaoFormatada.
	 *
	 * @param hrAtendimentoSolicitacaoFormatada the hr atendimento solicitacao formatada
	 */
	public void setHrAtendimentoSolicitacaoFormatada(
			String hrAtendimentoSolicitacaoFormatada) {
		this.hrAtendimentoSolicitacaoFormatada = hrAtendimentoSolicitacaoFormatada;
	}
	
	/**
	 * Get: hrSolicitacaoSolicitacao.
	 *
	 * @return hrSolicitacaoSolicitacao
	 */
	public String getHrSolicitacaoSolicitacao() {
		return hrSolicitacaoSolicitacao;
	}
	
	/**
	 * Set: hrSolicitacaoSolicitacao.
	 *
	 * @param hrSolicitacaoSolicitacao the hr solicitacao solicitacao
	 */
	public void setHrSolicitacaoSolicitacao(String hrSolicitacaoSolicitacao) {
		this.hrSolicitacaoSolicitacao = hrSolicitacaoSolicitacao;
	}
	
	/**
	 * Get: hrSolicitacaoSolicitacaoFormatada.
	 *
	 * @return hrSolicitacaoSolicitacaoFormatada
	 */
	public String getHrSolicitacaoSolicitacaoFormatada() {
		return hrSolicitacaoSolicitacaoFormatada;
	}
	
	/**
	 * Set: hrSolicitacaoSolicitacaoFormatada.
	 *
	 * @param hrSolicitacaoSolicitacaoFormatada the hr solicitacao solicitacao formatada
	 */
	public void setHrSolicitacaoSolicitacaoFormatada(
			String hrSolicitacaoSolicitacaoFormatada) {
		this.hrSolicitacaoSolicitacaoFormatada = hrSolicitacaoSolicitacaoFormatada;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	/**
	 * Get: nrSolicitacaoPagamento.
	 *
	 * @return nrSolicitacaoPagamento
	 */
	public Integer getNrSolicitacaoPagamento() {
		return nrSolicitacaoPagamento;
	}
	
	/**
	 * Set: nrSolicitacaoPagamento.
	 *
	 * @param nrSolicitacaoPagamento the nr solicitacao pagamento
	 */
	public void setNrSolicitacaoPagamento(Integer nrSolicitacaoPagamento) {
		this.nrSolicitacaoPagamento = nrSolicitacaoPagamento;
	}
	
	/**
	 * Get: qtdeRegistros.
	 *
	 * @return qtdeRegistros
	 */
	public Integer getQtdeRegistros() {
		return qtdeRegistros;
	}
	
	/**
	 * Set: qtdeRegistros.
	 *
	 * @param qtdeRegistros the qtde registros
	 */
	public void setQtdeRegistros(Integer qtdeRegistros) {
		this.qtdeRegistros = qtdeRegistros;
	}
	
	/**
	 * Get: qtdeRegistroSolicitacao.
	 *
	 * @return qtdeRegistroSolicitacao
	 */
	public Long getQtdeRegistroSolicitacao() {
		return qtdeRegistroSolicitacao;
	}
	
	/**
	 * Set: qtdeRegistroSolicitacao.
	 *
	 * @param qtdeRegistroSolicitacao the qtde registro solicitacao
	 */
	public void setQtdeRegistroSolicitacao(Long qtdeRegistroSolicitacao) {
		this.qtdeRegistroSolicitacao = qtdeRegistroSolicitacao;
	}
	
	/**
	 * Get: resultadoProcessamento.
	 *
	 * @return resultadoProcessamento
	 */
	public String getResultadoProcessamento() {
		return resultadoProcessamento;
	}
	
	/**
	 * Set: resultadoProcessamento.
	 *
	 * @param resultadoProcessamento the resultado processamento
	 */
	public void setResultadoProcessamento(String resultadoProcessamento) {
		this.resultadoProcessamento = resultadoProcessamento;
	}
	
	/**
	 * Get: qtdeRegistroSolicitacaoFormatado.
	 *
	 * @return qtdeRegistroSolicitacaoFormatado
	 */
	public String getQtdeRegistroSolicitacaoFormatado() {
		return qtdeRegistroSolicitacaoFormatado;
	}
	
	/**
	 * Set: qtdeRegistroSolicitacaoFormatado.
	 *
	 * @param qtdeRegistroSolicitacaoFormatado the qtde registro solicitacao formatado
	 */
	public void setQtdeRegistroSolicitacaoFormatado(
			String qtdeRegistroSolicitacaoFormatado) {
		this.qtdeRegistroSolicitacaoFormatado = qtdeRegistroSolicitacaoFormatado;
	}    
}
