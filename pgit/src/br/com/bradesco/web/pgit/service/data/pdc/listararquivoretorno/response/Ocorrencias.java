/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listararquivoretorno.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.math.BigDecimal;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdPessoaJuridicaContrato
     */
    private long _cdPessoaJuridicaContrato = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridicaContrato
     */
    private boolean _has_cdPessoaJuridicaContrato;

    /**
     * Field _dsPessoaJuridicaContrato
     */
    private java.lang.String _dsPessoaJuridicaContrato;

    /**
     * Field _cdTipoContratoNegocio
     */
    private int _cdTipoContratoNegocio = 0;

    /**
     * keeps track of state for field: _cdTipoContratoNegocio
     */
    private boolean _has_cdTipoContratoNegocio;

    /**
     * Field _dsTipoContratoNegocio
     */
    private java.lang.String _dsTipoContratoNegocio;

    /**
     * Field _nrSequenciaContratoNegocio
     */
    private long _nrSequenciaContratoNegocio = 0;

    /**
     * keeps track of state for field: _nrSequenciaContratoNegocio
     */
    private boolean _has_nrSequenciaContratoNegocio;

    /**
     * Field _dsContrato
     */
    private java.lang.String _dsContrato;

    /**
     * Field _cdPessoa
     */
    private long _cdPessoa = 0;

    /**
     * keeps track of state for field: _cdPessoa
     */
    private boolean _has_cdPessoa;

    /**
     * Field _corpoCpfCnpj
     */
    private java.lang.String _corpoCpfCnpj;

    /**
     * Field _dsPessoa
     */
    private java.lang.String _dsPessoa;

    /**
     * Field _cdTipoLayoutArquivo
     */
    private int _cdTipoLayoutArquivo = 0;

    /**
     * keeps track of state for field: _cdTipoLayoutArquivo
     */
    private boolean _has_cdTipoLayoutArquivo;

    /**
     * Field _dsTipoLayoutArquivo
     */
    private java.lang.String _dsTipoLayoutArquivo;

    /**
     * Field _cdClienteTransferenciaArquivo
     */
    private long _cdClienteTransferenciaArquivo = 0;

    /**
     * keeps track of state for field: _cdClienteTransferenciaArquiv
     */
    private boolean _has_cdClienteTransferenciaArquivo;

    /**
     * Field _dsArquivoRetorno
     */
    private java.lang.String _dsArquivoRetorno;

    /**
     * Field _nrArquivoRetorno
     */
    private long _nrArquivoRetorno = 0;

    /**
     * keeps track of state for field: _nrArquivoRetorno
     */
    private boolean _has_nrArquivoRetorno;

    /**
     * Field _hrInclusao
     */
    private java.lang.String _hrInclusao;

    /**
     * Field _dsSituacaoProcessamentoRetorno
     */
    private java.lang.String _dsSituacaoProcessamentoRetorno;

    /**
     * Field _dsMeioTransmissao
     */
    private java.lang.String _dsMeioTransmissao;

    /**
     * Field _dsAmbiente
     */
    private java.lang.String _dsAmbiente;

    /**
     * Field _qtdeRegistroArquivoRetorno
     */
    private long _qtdeRegistroArquivoRetorno = 0;

    /**
     * keeps track of state for field: _qtdeRegistroArquivoRetorno
     */
    private boolean _has_qtdeRegistroArquivoRetorno;

    /**
     * Field _vlrRegistroArquivoRetorno
     */
    private java.math.BigDecimal _vlrRegistroArquivoRetorno = new java.math.BigDecimal("0");


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
        setVlrRegistroArquivoRetorno(new java.math.BigDecimal("0"));
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listararquivoretorno.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdClienteTransferenciaArquivo
     * 
     */
    public void deleteCdClienteTransferenciaArquivo()
    {
        this._has_cdClienteTransferenciaArquivo= false;
    } //-- void deleteCdClienteTransferenciaArquivo() 

    /**
     * Method deleteCdPessoa
     * 
     */
    public void deleteCdPessoa()
    {
        this._has_cdPessoa= false;
    } //-- void deleteCdPessoa() 

    /**
     * Method deleteCdPessoaJuridicaContrato
     * 
     */
    public void deleteCdPessoaJuridicaContrato()
    {
        this._has_cdPessoaJuridicaContrato= false;
    } //-- void deleteCdPessoaJuridicaContrato() 

    /**
     * Method deleteCdTipoContratoNegocio
     * 
     */
    public void deleteCdTipoContratoNegocio()
    {
        this._has_cdTipoContratoNegocio= false;
    } //-- void deleteCdTipoContratoNegocio() 

    /**
     * Method deleteCdTipoLayoutArquivo
     * 
     */
    public void deleteCdTipoLayoutArquivo()
    {
        this._has_cdTipoLayoutArquivo= false;
    } //-- void deleteCdTipoLayoutArquivo() 

    /**
     * Method deleteNrArquivoRetorno
     * 
     */
    public void deleteNrArquivoRetorno()
    {
        this._has_nrArquivoRetorno= false;
    } //-- void deleteNrArquivoRetorno() 

    /**
     * Method deleteNrSequenciaContratoNegocio
     * 
     */
    public void deleteNrSequenciaContratoNegocio()
    {
        this._has_nrSequenciaContratoNegocio= false;
    } //-- void deleteNrSequenciaContratoNegocio() 

    /**
     * Method deleteQtdeRegistroArquivoRetorno
     * 
     */
    public void deleteQtdeRegistroArquivoRetorno()
    {
        this._has_qtdeRegistroArquivoRetorno= false;
    } //-- void deleteQtdeRegistroArquivoRetorno() 

    /**
     * Returns the value of field 'cdClienteTransferenciaArquivo'.
     * 
     * @return long
     * @return the value of field 'cdClienteTransferenciaArquivo'.
     */
    public long getCdClienteTransferenciaArquivo()
    {
        return this._cdClienteTransferenciaArquivo;
    } //-- long getCdClienteTransferenciaArquivo() 

    /**
     * Returns the value of field 'cdPessoa'.
     * 
     * @return long
     * @return the value of field 'cdPessoa'.
     */
    public long getCdPessoa()
    {
        return this._cdPessoa;
    } //-- long getCdPessoa() 

    /**
     * Returns the value of field 'cdPessoaJuridicaContrato'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridicaContrato'.
     */
    public long getCdPessoaJuridicaContrato()
    {
        return this._cdPessoaJuridicaContrato;
    } //-- long getCdPessoaJuridicaContrato() 

    /**
     * Returns the value of field 'cdTipoContratoNegocio'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoNegocio'.
     */
    public int getCdTipoContratoNegocio()
    {
        return this._cdTipoContratoNegocio;
    } //-- int getCdTipoContratoNegocio() 

    /**
     * Returns the value of field 'cdTipoLayoutArquivo'.
     * 
     * @return int
     * @return the value of field 'cdTipoLayoutArquivo'.
     */
    public int getCdTipoLayoutArquivo()
    {
        return this._cdTipoLayoutArquivo;
    } //-- int getCdTipoLayoutArquivo() 

    /**
     * Returns the value of field 'corpoCpfCnpj'.
     * 
     * @return String
     * @return the value of field 'corpoCpfCnpj'.
     */
    public java.lang.String getCorpoCpfCnpj()
    {
        return this._corpoCpfCnpj;
    } //-- java.lang.String getCorpoCpfCnpj() 

    /**
     * Returns the value of field 'dsAmbiente'.
     * 
     * @return String
     * @return the value of field 'dsAmbiente'.
     */
    public java.lang.String getDsAmbiente()
    {
        return this._dsAmbiente;
    } //-- java.lang.String getDsAmbiente() 

    /**
     * Returns the value of field 'dsArquivoRetorno'.
     * 
     * @return String
     * @return the value of field 'dsArquivoRetorno'.
     */
    public java.lang.String getDsArquivoRetorno()
    {
        return this._dsArquivoRetorno;
    } //-- java.lang.String getDsArquivoRetorno() 

    /**
     * Returns the value of field 'dsContrato'.
     * 
     * @return String
     * @return the value of field 'dsContrato'.
     */
    public java.lang.String getDsContrato()
    {
        return this._dsContrato;
    } //-- java.lang.String getDsContrato() 

    /**
     * Returns the value of field 'dsMeioTransmissao'.
     * 
     * @return String
     * @return the value of field 'dsMeioTransmissao'.
     */
    public java.lang.String getDsMeioTransmissao()
    {
        return this._dsMeioTransmissao;
    } //-- java.lang.String getDsMeioTransmissao() 

    /**
     * Returns the value of field 'dsPessoa'.
     * 
     * @return String
     * @return the value of field 'dsPessoa'.
     */
    public java.lang.String getDsPessoa()
    {
        return this._dsPessoa;
    } //-- java.lang.String getDsPessoa() 

    /**
     * Returns the value of field 'dsPessoaJuridicaContrato'.
     * 
     * @return String
     * @return the value of field 'dsPessoaJuridicaContrato'.
     */
    public java.lang.String getDsPessoaJuridicaContrato()
    {
        return this._dsPessoaJuridicaContrato;
    } //-- java.lang.String getDsPessoaJuridicaContrato() 

    /**
     * Returns the value of field 'dsSituacaoProcessamentoRetorno'.
     * 
     * @return String
     * @return the value of field 'dsSituacaoProcessamentoRetorno'.
     */
    public java.lang.String getDsSituacaoProcessamentoRetorno()
    {
        return this._dsSituacaoProcessamentoRetorno;
    } //-- java.lang.String getDsSituacaoProcessamentoRetorno() 

    /**
     * Returns the value of field 'dsTipoContratoNegocio'.
     * 
     * @return String
     * @return the value of field 'dsTipoContratoNegocio'.
     */
    public java.lang.String getDsTipoContratoNegocio()
    {
        return this._dsTipoContratoNegocio;
    } //-- java.lang.String getDsTipoContratoNegocio() 

    /**
     * Returns the value of field 'dsTipoLayoutArquivo'.
     * 
     * @return String
     * @return the value of field 'dsTipoLayoutArquivo'.
     */
    public java.lang.String getDsTipoLayoutArquivo()
    {
        return this._dsTipoLayoutArquivo;
    } //-- java.lang.String getDsTipoLayoutArquivo() 

    /**
     * Returns the value of field 'hrInclusao'.
     * 
     * @return String
     * @return the value of field 'hrInclusao'.
     */
    public java.lang.String getHrInclusao()
    {
        return this._hrInclusao;
    } //-- java.lang.String getHrInclusao() 

    /**
     * Returns the value of field 'nrArquivoRetorno'.
     * 
     * @return long
     * @return the value of field 'nrArquivoRetorno'.
     */
    public long getNrArquivoRetorno()
    {
        return this._nrArquivoRetorno;
    } //-- long getNrArquivoRetorno() 

    /**
     * Returns the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @return long
     * @return the value of field 'nrSequenciaContratoNegocio'.
     */
    public long getNrSequenciaContratoNegocio()
    {
        return this._nrSequenciaContratoNegocio;
    } //-- long getNrSequenciaContratoNegocio() 

    /**
     * Returns the value of field 'qtdeRegistroArquivoRetorno'.
     * 
     * @return long
     * @return the value of field 'qtdeRegistroArquivoRetorno'.
     */
    public long getQtdeRegistroArquivoRetorno()
    {
        return this._qtdeRegistroArquivoRetorno;
    } //-- long getQtdeRegistroArquivoRetorno() 

    /**
     * Returns the value of field 'vlrRegistroArquivoRetorno'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlrRegistroArquivoRetorno'.
     */
    public java.math.BigDecimal getVlrRegistroArquivoRetorno()
    {
        return this._vlrRegistroArquivoRetorno;
    } //-- java.math.BigDecimal getVlrRegistroArquivoRetorno() 

    /**
     * Method hasCdClienteTransferenciaArquivo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdClienteTransferenciaArquivo()
    {
        return this._has_cdClienteTransferenciaArquivo;
    } //-- boolean hasCdClienteTransferenciaArquivo() 

    /**
     * Method hasCdPessoa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoa()
    {
        return this._has_cdPessoa;
    } //-- boolean hasCdPessoa() 

    /**
     * Method hasCdPessoaJuridicaContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridicaContrato()
    {
        return this._has_cdPessoaJuridicaContrato;
    } //-- boolean hasCdPessoaJuridicaContrato() 

    /**
     * Method hasCdTipoContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoNegocio()
    {
        return this._has_cdTipoContratoNegocio;
    } //-- boolean hasCdTipoContratoNegocio() 

    /**
     * Method hasCdTipoLayoutArquivo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoLayoutArquivo()
    {
        return this._has_cdTipoLayoutArquivo;
    } //-- boolean hasCdTipoLayoutArquivo() 

    /**
     * Method hasNrArquivoRetorno
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrArquivoRetorno()
    {
        return this._has_nrArquivoRetorno;
    } //-- boolean hasNrArquivoRetorno() 

    /**
     * Method hasNrSequenciaContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaContratoNegocio()
    {
        return this._has_nrSequenciaContratoNegocio;
    } //-- boolean hasNrSequenciaContratoNegocio() 

    /**
     * Method hasQtdeRegistroArquivoRetorno
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtdeRegistroArquivoRetorno()
    {
        return this._has_qtdeRegistroArquivoRetorno;
    } //-- boolean hasQtdeRegistroArquivoRetorno() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdClienteTransferenciaArquivo'.
     * 
     * @param cdClienteTransferenciaArquivo the value of field
     * 'cdClienteTransferenciaArquivo'.
     */
    public void setCdClienteTransferenciaArquivo(long cdClienteTransferenciaArquivo)
    {
        this._cdClienteTransferenciaArquivo = cdClienteTransferenciaArquivo;
        this._has_cdClienteTransferenciaArquivo = true;
    } //-- void setCdClienteTransferenciaArquivo(long) 

    /**
     * Sets the value of field 'cdPessoa'.
     * 
     * @param cdPessoa the value of field 'cdPessoa'.
     */
    public void setCdPessoa(long cdPessoa)
    {
        this._cdPessoa = cdPessoa;
        this._has_cdPessoa = true;
    } //-- void setCdPessoa(long) 

    /**
     * Sets the value of field 'cdPessoaJuridicaContrato'.
     * 
     * @param cdPessoaJuridicaContrato the value of field
     * 'cdPessoaJuridicaContrato'.
     */
    public void setCdPessoaJuridicaContrato(long cdPessoaJuridicaContrato)
    {
        this._cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
        this._has_cdPessoaJuridicaContrato = true;
    } //-- void setCdPessoaJuridicaContrato(long) 

    /**
     * Sets the value of field 'cdTipoContratoNegocio'.
     * 
     * @param cdTipoContratoNegocio the value of field
     * 'cdTipoContratoNegocio'.
     */
    public void setCdTipoContratoNegocio(int cdTipoContratoNegocio)
    {
        this._cdTipoContratoNegocio = cdTipoContratoNegocio;
        this._has_cdTipoContratoNegocio = true;
    } //-- void setCdTipoContratoNegocio(int) 

    /**
     * Sets the value of field 'cdTipoLayoutArquivo'.
     * 
     * @param cdTipoLayoutArquivo the value of field
     * 'cdTipoLayoutArquivo'.
     */
    public void setCdTipoLayoutArquivo(int cdTipoLayoutArquivo)
    {
        this._cdTipoLayoutArquivo = cdTipoLayoutArquivo;
        this._has_cdTipoLayoutArquivo = true;
    } //-- void setCdTipoLayoutArquivo(int) 

    /**
     * Sets the value of field 'corpoCpfCnpj'.
     * 
     * @param corpoCpfCnpj the value of field 'corpoCpfCnpj'.
     */
    public void setCorpoCpfCnpj(java.lang.String corpoCpfCnpj)
    {
        this._corpoCpfCnpj = corpoCpfCnpj;
    } //-- void setCorpoCpfCnpj(java.lang.String) 

    /**
     * Sets the value of field 'dsAmbiente'.
     * 
     * @param dsAmbiente the value of field 'dsAmbiente'.
     */
    public void setDsAmbiente(java.lang.String dsAmbiente)
    {
        this._dsAmbiente = dsAmbiente;
    } //-- void setDsAmbiente(java.lang.String) 

    /**
     * Sets the value of field 'dsArquivoRetorno'.
     * 
     * @param dsArquivoRetorno the value of field 'dsArquivoRetorno'
     */
    public void setDsArquivoRetorno(java.lang.String dsArquivoRetorno)
    {
        this._dsArquivoRetorno = dsArquivoRetorno;
    } //-- void setDsArquivoRetorno(java.lang.String) 

    /**
     * Sets the value of field 'dsContrato'.
     * 
     * @param dsContrato the value of field 'dsContrato'.
     */
    public void setDsContrato(java.lang.String dsContrato)
    {
        this._dsContrato = dsContrato;
    } //-- void setDsContrato(java.lang.String) 

    /**
     * Sets the value of field 'dsMeioTransmissao'.
     * 
     * @param dsMeioTransmissao the value of field
     * 'dsMeioTransmissao'.
     */
    public void setDsMeioTransmissao(java.lang.String dsMeioTransmissao)
    {
        this._dsMeioTransmissao = dsMeioTransmissao;
    } //-- void setDsMeioTransmissao(java.lang.String) 

    /**
     * Sets the value of field 'dsPessoa'.
     * 
     * @param dsPessoa the value of field 'dsPessoa'.
     */
    public void setDsPessoa(java.lang.String dsPessoa)
    {
        this._dsPessoa = dsPessoa;
    } //-- void setDsPessoa(java.lang.String) 

    /**
     * Sets the value of field 'dsPessoaJuridicaContrato'.
     * 
     * @param dsPessoaJuridicaContrato the value of field
     * 'dsPessoaJuridicaContrato'.
     */
    public void setDsPessoaJuridicaContrato(java.lang.String dsPessoaJuridicaContrato)
    {
        this._dsPessoaJuridicaContrato = dsPessoaJuridicaContrato;
    } //-- void setDsPessoaJuridicaContrato(java.lang.String) 

    /**
     * Sets the value of field 'dsSituacaoProcessamentoRetorno'.
     * 
     * @param dsSituacaoProcessamentoRetorno the value of field
     * 'dsSituacaoProcessamentoRetorno'.
     */
    public void setDsSituacaoProcessamentoRetorno(java.lang.String dsSituacaoProcessamentoRetorno)
    {
        this._dsSituacaoProcessamentoRetorno = dsSituacaoProcessamentoRetorno;
    } //-- void setDsSituacaoProcessamentoRetorno(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoContratoNegocio'.
     * 
     * @param dsTipoContratoNegocio the value of field
     * 'dsTipoContratoNegocio'.
     */
    public void setDsTipoContratoNegocio(java.lang.String dsTipoContratoNegocio)
    {
        this._dsTipoContratoNegocio = dsTipoContratoNegocio;
    } //-- void setDsTipoContratoNegocio(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoLayoutArquivo'.
     * 
     * @param dsTipoLayoutArquivo the value of field
     * 'dsTipoLayoutArquivo'.
     */
    public void setDsTipoLayoutArquivo(java.lang.String dsTipoLayoutArquivo)
    {
        this._dsTipoLayoutArquivo = dsTipoLayoutArquivo;
    } //-- void setDsTipoLayoutArquivo(java.lang.String) 

    /**
     * Sets the value of field 'hrInclusao'.
     * 
     * @param hrInclusao the value of field 'hrInclusao'.
     */
    public void setHrInclusao(java.lang.String hrInclusao)
    {
        this._hrInclusao = hrInclusao;
    } //-- void setHrInclusao(java.lang.String) 

    /**
     * Sets the value of field 'nrArquivoRetorno'.
     * 
     * @param nrArquivoRetorno the value of field 'nrArquivoRetorno'
     */
    public void setNrArquivoRetorno(long nrArquivoRetorno)
    {
        this._nrArquivoRetorno = nrArquivoRetorno;
        this._has_nrArquivoRetorno = true;
    } //-- void setNrArquivoRetorno(long) 

    /**
     * Sets the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @param nrSequenciaContratoNegocio the value of field
     * 'nrSequenciaContratoNegocio'.
     */
    public void setNrSequenciaContratoNegocio(long nrSequenciaContratoNegocio)
    {
        this._nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
        this._has_nrSequenciaContratoNegocio = true;
    } //-- void setNrSequenciaContratoNegocio(long) 

    /**
     * Sets the value of field 'qtdeRegistroArquivoRetorno'.
     * 
     * @param qtdeRegistroArquivoRetorno the value of field
     * 'qtdeRegistroArquivoRetorno'.
     */
    public void setQtdeRegistroArquivoRetorno(long qtdeRegistroArquivoRetorno)
    {
        this._qtdeRegistroArquivoRetorno = qtdeRegistroArquivoRetorno;
        this._has_qtdeRegistroArquivoRetorno = true;
    } //-- void setQtdeRegistroArquivoRetorno(long) 

    /**
     * Sets the value of field 'vlrRegistroArquivoRetorno'.
     * 
     * @param vlrRegistroArquivoRetorno the value of field
     * 'vlrRegistroArquivoRetorno'.
     */
    public void setVlrRegistroArquivoRetorno(java.math.BigDecimal vlrRegistroArquivoRetorno)
    {
        this._vlrRegistroArquivoRetorno = vlrRegistroArquivoRetorno;
    } //-- void setVlrRegistroArquivoRetorno(java.math.BigDecimal) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listararquivoretorno.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listararquivoretorno.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listararquivoretorno.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listararquivoretorno.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
