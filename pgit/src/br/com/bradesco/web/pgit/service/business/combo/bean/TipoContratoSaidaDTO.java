/*
 * Nome: br.com.bradesco.web.pgit.service.business.combo.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.combo.bean;

import br.com.bradesco.web.pgit.service.data.pdc.listartipocontrato.response.Ocorrencias;

/**
 * Nome: TipoContratoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class TipoContratoSaidaDTO {

	/** Atributo cdTipoContrato. */
	private Integer cdTipoContrato;
	
	/** Atributo dsTipoContrato. */
	private String dsTipoContrato;

	/**
	 * Tipo contrato saida dto.
	 */
	public TipoContratoSaidaDTO() {
	}

	/**
	 * Tipo contrato saida dto.
	 *
	 * @param saida the saida
	 */
	public TipoContratoSaidaDTO(Ocorrencias saida) {
		this.cdTipoContrato = saida.getCdTipoContrato();
		this.dsTipoContrato = saida.getDsTipoContrato();
	}

	/**
	 * Get: cdTipoContrato.
	 *
	 * @return cdTipoContrato
	 */
	public Integer getCdTipoContrato() {
		return cdTipoContrato;
	}

	/**
	 * Set: cdTipoContrato.
	 *
	 * @param cdTipoContrato the cd tipo contrato
	 */
	public void setCdTipoContrato(Integer cdTipoContrato) {
		this.cdTipoContrato = cdTipoContrato;
	}

	/**
	 * Get: dsTipoContrato.
	 *
	 * @return dsTipoContrato
	 */
	public String getDsTipoContrato() {
		return dsTipoContrato;
	}

	/**
	 * Set: dsTipoContrato.
	 *
	 * @param dsTipoContrato the ds tipo contrato
	 */
	public void setDsTipoContrato(String dsTipoContrato) {
		this.dsTipoContrato = dsTipoContrato;
	}

}
