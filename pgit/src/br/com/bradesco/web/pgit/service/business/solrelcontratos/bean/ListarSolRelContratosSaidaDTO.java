/*
 * Nome: br.com.bradesco.web.pgit.service.business.solrelcontratos.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.solrelcontratos.bean;

/**
 * Nome: ListarSolRelContratosSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarSolRelContratosSaidaDTO {
	
	/** Atributo codMensagem. */
	private String codMensagem;
	
	/** Atributo mensagem. */
	private String mensagem;
	
	/** Atributo tipoSolicitacao. */
	private int tipoSolicitacao;
	
	/** Atributo solicitacao. */
	private int solicitacao;
	
	/** Atributo tipoRelatorio. */
	private String tipoRelatorio;
	
	/** Atributo dataHoraSolicitacao. */
	private String dataHoraSolicitacao;
	
	/** Atributo situacao. */
	private String situacao;	
	 
		 
	
	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}
	
	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	/**
	 * Get: solicitacao.
	 *
	 * @return solicitacao
	 */
	public int getSolicitacao() {
		return solicitacao;
	}
	
	/**
	 * Set: solicitacao.
	 *
	 * @param solicitacao the solicitacao
	 */
	public void setSolicitacao(int solicitacao) {
		this.solicitacao = solicitacao;
	}
	
	/**
	 * Get: tipoSolicitacao.
	 *
	 * @return tipoSolicitacao
	 */
	public int getTipoSolicitacao() {
		return tipoSolicitacao;
	}
	
	/**
	 * Set: tipoSolicitacao.
	 *
	 * @param tipoSolicitacao the tipo solicitacao
	 */
	public void setTipoSolicitacao(int tipoSolicitacao) {
		this.tipoSolicitacao = tipoSolicitacao;
	}
	
	/**
	 * Get: tipoRelatorio.
	 *
	 * @return tipoRelatorio
	 */
	public String getTipoRelatorio() {
		return tipoRelatorio;
	}
	
	/**
	 * Set: tipoRelatorio.
	 *
	 * @param tipoRelatorio the tipo relatorio
	 */
	public void setTipoRelatorio(String tipoRelatorio) {
		this.tipoRelatorio = tipoRelatorio;
	}
	
	/**
	 * Get: dataHoraSolicitacao.
	 *
	 * @return dataHoraSolicitacao
	 */
	public String getDataHoraSolicitacao() {
		return dataHoraSolicitacao;
	}
	
	/**
	 * Set: dataHoraSolicitacao.
	 *
	 * @param dataHoraSolicitacao the data hora solicitacao
	 */
	public void setDataHoraSolicitacao(String dataHoraSolicitacao) {
		this.dataHoraSolicitacao = dataHoraSolicitacao;
	}
	
	/**
	 * Get: situacao.
	 *
	 * @return situacao
	 */
	public String getSituacao() {
		return situacao;
	}
	
	/**
	 * Set: situacao.
	 *
	 * @param situacao the situacao
	 */
	public void setSituacao(String situacao) {
		this.situacao = situacao;
	}
	
	

}
