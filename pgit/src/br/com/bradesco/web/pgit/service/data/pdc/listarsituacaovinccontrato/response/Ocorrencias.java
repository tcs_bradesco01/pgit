/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listarsituacaovinccontrato.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdSituacaoVinculacaoContrato
     */
    private int _cdSituacaoVinculacaoContrato = 0;

    /**
     * keeps track of state for field: _cdSituacaoVinculacaoContrato
     */
    private boolean _has_cdSituacaoVinculacaoContrato;

    /**
     * Field _dsSituacaoVinculacaoContrato
     */
    private java.lang.String _dsSituacaoVinculacaoContrato;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarsituacaovinccontrato.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdSituacaoVinculacaoContrato
     * 
     */
    public void deleteCdSituacaoVinculacaoContrato()
    {
        this._has_cdSituacaoVinculacaoContrato= false;
    } //-- void deleteCdSituacaoVinculacaoContrato() 

    /**
     * Returns the value of field 'cdSituacaoVinculacaoContrato'.
     * 
     * @return int
     * @return the value of field 'cdSituacaoVinculacaoContrato'.
     */
    public int getCdSituacaoVinculacaoContrato()
    {
        return this._cdSituacaoVinculacaoContrato;
    } //-- int getCdSituacaoVinculacaoContrato() 

    /**
     * Returns the value of field 'dsSituacaoVinculacaoContrato'.
     * 
     * @return String
     * @return the value of field 'dsSituacaoVinculacaoContrato'.
     */
    public java.lang.String getDsSituacaoVinculacaoContrato()
    {
        return this._dsSituacaoVinculacaoContrato;
    } //-- java.lang.String getDsSituacaoVinculacaoContrato() 

    /**
     * Method hasCdSituacaoVinculacaoContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSituacaoVinculacaoContrato()
    {
        return this._has_cdSituacaoVinculacaoContrato;
    } //-- boolean hasCdSituacaoVinculacaoContrato() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdSituacaoVinculacaoContrato'.
     * 
     * @param cdSituacaoVinculacaoContrato the value of field
     * 'cdSituacaoVinculacaoContrato'.
     */
    public void setCdSituacaoVinculacaoContrato(int cdSituacaoVinculacaoContrato)
    {
        this._cdSituacaoVinculacaoContrato = cdSituacaoVinculacaoContrato;
        this._has_cdSituacaoVinculacaoContrato = true;
    } //-- void setCdSituacaoVinculacaoContrato(int) 

    /**
     * Sets the value of field 'dsSituacaoVinculacaoContrato'.
     * 
     * @param dsSituacaoVinculacaoContrato the value of field
     * 'dsSituacaoVinculacaoContrato'.
     */
    public void setDsSituacaoVinculacaoContrato(java.lang.String dsSituacaoVinculacaoContrato)
    {
        this._dsSituacaoVinculacaoContrato = dsSituacaoVinculacaoContrato;
    } //-- void setDsSituacaoVinculacaoContrato(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listarsituacaovinccontrato.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listarsituacaovinccontrato.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listarsituacaovinccontrato.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarsituacaovinccontrato.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
