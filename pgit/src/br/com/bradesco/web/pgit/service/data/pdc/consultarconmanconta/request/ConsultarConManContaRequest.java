/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultarconmanconta.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ConsultarConManContaRequest.
 * 
 * @version $Revision$ $Date$
 */
public class ConsultarConManContaRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _hrInclusaoRegistro
     */
    private java.lang.String _hrInclusaoRegistro;

    /**
     * Field _cdPessoaJuridicaConta
     */
    private long _cdPessoaJuridicaConta = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridicaConta
     */
    private boolean _has_cdPessoaJuridicaConta;

    /**
     * Field _cdTipoContratoConta
     */
    private int _cdTipoContratoConta = 0;

    /**
     * keeps track of state for field: _cdTipoContratoConta
     */
    private boolean _has_cdTipoContratoConta;

    /**
     * Field _nrSequenciaContratoConta
     */
    private long _nrSequenciaContratoConta = 0;

    /**
     * keeps track of state for field: _nrSequenciaContratoConta
     */
    private boolean _has_nrSequenciaContratoConta;

    /**
     * Field _cdPessoaJuridicaContrato
     */
    private long _cdPessoaJuridicaContrato = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridicaContrato
     */
    private boolean _has_cdPessoaJuridicaContrato;

    /**
     * Field _cdTipoContratoNegocio
     */
    private int _cdTipoContratoNegocio = 0;

    /**
     * keeps track of state for field: _cdTipoContratoNegocio
     */
    private boolean _has_cdTipoContratoNegocio;

    /**
     * Field _nrSequenciaContratoNegocio
     */
    private long _nrSequenciaContratoNegocio = 0;

    /**
     * keeps track of state for field: _nrSequenciaContratoNegocio
     */
    private boolean _has_nrSequenciaContratoNegocio;

    /**
     * Field _cdTipoVinculoContrato
     */
    private int _cdTipoVinculoContrato = 0;

    /**
     * keeps track of state for field: _cdTipoVinculoContrato
     */
    private boolean _has_cdTipoVinculoContrato;


      //----------------/
     //- Constructors -/
    //----------------/

    public ConsultarConManContaRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarconmanconta.request.ConsultarConManContaRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdPessoaJuridicaConta
     * 
     */
    public void deleteCdPessoaJuridicaConta()
    {
        this._has_cdPessoaJuridicaConta= false;
    } //-- void deleteCdPessoaJuridicaConta() 

    /**
     * Method deleteCdPessoaJuridicaContrato
     * 
     */
    public void deleteCdPessoaJuridicaContrato()
    {
        this._has_cdPessoaJuridicaContrato= false;
    } //-- void deleteCdPessoaJuridicaContrato() 

    /**
     * Method deleteCdTipoContratoConta
     * 
     */
    public void deleteCdTipoContratoConta()
    {
        this._has_cdTipoContratoConta= false;
    } //-- void deleteCdTipoContratoConta() 

    /**
     * Method deleteCdTipoContratoNegocio
     * 
     */
    public void deleteCdTipoContratoNegocio()
    {
        this._has_cdTipoContratoNegocio= false;
    } //-- void deleteCdTipoContratoNegocio() 

    /**
     * Method deleteCdTipoVinculoContrato
     * 
     */
    public void deleteCdTipoVinculoContrato()
    {
        this._has_cdTipoVinculoContrato= false;
    } //-- void deleteCdTipoVinculoContrato() 

    /**
     * Method deleteNrSequenciaContratoConta
     * 
     */
    public void deleteNrSequenciaContratoConta()
    {
        this._has_nrSequenciaContratoConta= false;
    } //-- void deleteNrSequenciaContratoConta() 

    /**
     * Method deleteNrSequenciaContratoNegocio
     * 
     */
    public void deleteNrSequenciaContratoNegocio()
    {
        this._has_nrSequenciaContratoNegocio= false;
    } //-- void deleteNrSequenciaContratoNegocio() 

    /**
     * Returns the value of field 'cdPessoaJuridicaConta'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridicaConta'.
     */
    public long getCdPessoaJuridicaConta()
    {
        return this._cdPessoaJuridicaConta;
    } //-- long getCdPessoaJuridicaConta() 

    /**
     * Returns the value of field 'cdPessoaJuridicaContrato'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridicaContrato'.
     */
    public long getCdPessoaJuridicaContrato()
    {
        return this._cdPessoaJuridicaContrato;
    } //-- long getCdPessoaJuridicaContrato() 

    /**
     * Returns the value of field 'cdTipoContratoConta'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoConta'.
     */
    public int getCdTipoContratoConta()
    {
        return this._cdTipoContratoConta;
    } //-- int getCdTipoContratoConta() 

    /**
     * Returns the value of field 'cdTipoContratoNegocio'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoNegocio'.
     */
    public int getCdTipoContratoNegocio()
    {
        return this._cdTipoContratoNegocio;
    } //-- int getCdTipoContratoNegocio() 

    /**
     * Returns the value of field 'cdTipoVinculoContrato'.
     * 
     * @return int
     * @return the value of field 'cdTipoVinculoContrato'.
     */
    public int getCdTipoVinculoContrato()
    {
        return this._cdTipoVinculoContrato;
    } //-- int getCdTipoVinculoContrato() 

    /**
     * Returns the value of field 'hrInclusaoRegistro'.
     * 
     * @return String
     * @return the value of field 'hrInclusaoRegistro'.
     */
    public java.lang.String getHrInclusaoRegistro()
    {
        return this._hrInclusaoRegistro;
    } //-- java.lang.String getHrInclusaoRegistro() 

    /**
     * Returns the value of field 'nrSequenciaContratoConta'.
     * 
     * @return long
     * @return the value of field 'nrSequenciaContratoConta'.
     */
    public long getNrSequenciaContratoConta()
    {
        return this._nrSequenciaContratoConta;
    } //-- long getNrSequenciaContratoConta() 

    /**
     * Returns the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @return long
     * @return the value of field 'nrSequenciaContratoNegocio'.
     */
    public long getNrSequenciaContratoNegocio()
    {
        return this._nrSequenciaContratoNegocio;
    } //-- long getNrSequenciaContratoNegocio() 

    /**
     * Method hasCdPessoaJuridicaConta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridicaConta()
    {
        return this._has_cdPessoaJuridicaConta;
    } //-- boolean hasCdPessoaJuridicaConta() 

    /**
     * Method hasCdPessoaJuridicaContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridicaContrato()
    {
        return this._has_cdPessoaJuridicaContrato;
    } //-- boolean hasCdPessoaJuridicaContrato() 

    /**
     * Method hasCdTipoContratoConta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoConta()
    {
        return this._has_cdTipoContratoConta;
    } //-- boolean hasCdTipoContratoConta() 

    /**
     * Method hasCdTipoContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoNegocio()
    {
        return this._has_cdTipoContratoNegocio;
    } //-- boolean hasCdTipoContratoNegocio() 

    /**
     * Method hasCdTipoVinculoContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoVinculoContrato()
    {
        return this._has_cdTipoVinculoContrato;
    } //-- boolean hasCdTipoVinculoContrato() 

    /**
     * Method hasNrSequenciaContratoConta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaContratoConta()
    {
        return this._has_nrSequenciaContratoConta;
    } //-- boolean hasNrSequenciaContratoConta() 

    /**
     * Method hasNrSequenciaContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaContratoNegocio()
    {
        return this._has_nrSequenciaContratoNegocio;
    } //-- boolean hasNrSequenciaContratoNegocio() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdPessoaJuridicaConta'.
     * 
     * @param cdPessoaJuridicaConta the value of field
     * 'cdPessoaJuridicaConta'.
     */
    public void setCdPessoaJuridicaConta(long cdPessoaJuridicaConta)
    {
        this._cdPessoaJuridicaConta = cdPessoaJuridicaConta;
        this._has_cdPessoaJuridicaConta = true;
    } //-- void setCdPessoaJuridicaConta(long) 

    /**
     * Sets the value of field 'cdPessoaJuridicaContrato'.
     * 
     * @param cdPessoaJuridicaContrato the value of field
     * 'cdPessoaJuridicaContrato'.
     */
    public void setCdPessoaJuridicaContrato(long cdPessoaJuridicaContrato)
    {
        this._cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
        this._has_cdPessoaJuridicaContrato = true;
    } //-- void setCdPessoaJuridicaContrato(long) 

    /**
     * Sets the value of field 'cdTipoContratoConta'.
     * 
     * @param cdTipoContratoConta the value of field
     * 'cdTipoContratoConta'.
     */
    public void setCdTipoContratoConta(int cdTipoContratoConta)
    {
        this._cdTipoContratoConta = cdTipoContratoConta;
        this._has_cdTipoContratoConta = true;
    } //-- void setCdTipoContratoConta(int) 

    /**
     * Sets the value of field 'cdTipoContratoNegocio'.
     * 
     * @param cdTipoContratoNegocio the value of field
     * 'cdTipoContratoNegocio'.
     */
    public void setCdTipoContratoNegocio(int cdTipoContratoNegocio)
    {
        this._cdTipoContratoNegocio = cdTipoContratoNegocio;
        this._has_cdTipoContratoNegocio = true;
    } //-- void setCdTipoContratoNegocio(int) 

    /**
     * Sets the value of field 'cdTipoVinculoContrato'.
     * 
     * @param cdTipoVinculoContrato the value of field
     * 'cdTipoVinculoContrato'.
     */
    public void setCdTipoVinculoContrato(int cdTipoVinculoContrato)
    {
        this._cdTipoVinculoContrato = cdTipoVinculoContrato;
        this._has_cdTipoVinculoContrato = true;
    } //-- void setCdTipoVinculoContrato(int) 

    /**
     * Sets the value of field 'hrInclusaoRegistro'.
     * 
     * @param hrInclusaoRegistro the value of field
     * 'hrInclusaoRegistro'.
     */
    public void setHrInclusaoRegistro(java.lang.String hrInclusaoRegistro)
    {
        this._hrInclusaoRegistro = hrInclusaoRegistro;
    } //-- void setHrInclusaoRegistro(java.lang.String) 

    /**
     * Sets the value of field 'nrSequenciaContratoConta'.
     * 
     * @param nrSequenciaContratoConta the value of field
     * 'nrSequenciaContratoConta'.
     */
    public void setNrSequenciaContratoConta(long nrSequenciaContratoConta)
    {
        this._nrSequenciaContratoConta = nrSequenciaContratoConta;
        this._has_nrSequenciaContratoConta = true;
    } //-- void setNrSequenciaContratoConta(long) 

    /**
     * Sets the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @param nrSequenciaContratoNegocio the value of field
     * 'nrSequenciaContratoNegocio'.
     */
    public void setNrSequenciaContratoNegocio(long nrSequenciaContratoNegocio)
    {
        this._nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
        this._has_nrSequenciaContratoNegocio = true;
    } //-- void setNrSequenciaContratoNegocio(long) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ConsultarConManContaRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultarconmanconta.request.ConsultarConManContaRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultarconmanconta.request.ConsultarConManContaRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultarconmanconta.request.ConsultarConManContaRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarconmanconta.request.ConsultarConManContaRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
