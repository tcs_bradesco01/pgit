/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.detalharpiramide.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.math.BigDecimal;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias4.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias4 implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdBancoCredtCsignst
     */
    private int _cdBancoCredtCsignst = 0;

    /**
     * keeps track of state for field: _cdBancoCredtCsignst
     */
    private boolean _has_cdBancoCredtCsignst;

    /**
     * Field _dsBancoCredtCsignst
     */
    private java.lang.String _dsBancoCredtCsignst;

    /**
     * Field _vlRepasMesConsign
     */
    private java.math.BigDecimal _vlRepasMesConsign = new java.math.BigDecimal("0");

    /**
     * Field _vlCartCsign
     */
    private java.math.BigDecimal _vlCartCsign = new java.math.BigDecimal("0");


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias4() 
     {
        super();
        setVlRepasMesConsign(new java.math.BigDecimal("0"));
        setVlCartCsign(new java.math.BigDecimal("0"));
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharpiramide.response.Ocorrencias4()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdBancoCredtCsignst
     * 
     */
    public void deleteCdBancoCredtCsignst()
    {
        this._has_cdBancoCredtCsignst= false;
    } //-- void deleteCdBancoCredtCsignst() 

    /**
     * Returns the value of field 'cdBancoCredtCsignst'.
     * 
     * @return int
     * @return the value of field 'cdBancoCredtCsignst'.
     */
    public int getCdBancoCredtCsignst()
    {
        return this._cdBancoCredtCsignst;
    } //-- int getCdBancoCredtCsignst() 

    /**
     * Returns the value of field 'dsBancoCredtCsignst'.
     * 
     * @return String
     * @return the value of field 'dsBancoCredtCsignst'.
     */
    public java.lang.String getDsBancoCredtCsignst()
    {
        return this._dsBancoCredtCsignst;
    } //-- java.lang.String getDsBancoCredtCsignst() 

    /**
     * Returns the value of field 'vlCartCsign'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlCartCsign'.
     */
    public java.math.BigDecimal getVlCartCsign()
    {
        return this._vlCartCsign;
    } //-- java.math.BigDecimal getVlCartCsign() 

    /**
     * Returns the value of field 'vlRepasMesConsign'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlRepasMesConsign'.
     */
    public java.math.BigDecimal getVlRepasMesConsign()
    {
        return this._vlRepasMesConsign;
    } //-- java.math.BigDecimal getVlRepasMesConsign() 

    /**
     * Method hasCdBancoCredtCsignst
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdBancoCredtCsignst()
    {
        return this._has_cdBancoCredtCsignst;
    } //-- boolean hasCdBancoCredtCsignst() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdBancoCredtCsignst'.
     * 
     * @param cdBancoCredtCsignst the value of field
     * 'cdBancoCredtCsignst'.
     */
    public void setCdBancoCredtCsignst(int cdBancoCredtCsignst)
    {
        this._cdBancoCredtCsignst = cdBancoCredtCsignst;
        this._has_cdBancoCredtCsignst = true;
    } //-- void setCdBancoCredtCsignst(int) 

    /**
     * Sets the value of field 'dsBancoCredtCsignst'.
     * 
     * @param dsBancoCredtCsignst the value of field
     * 'dsBancoCredtCsignst'.
     */
    public void setDsBancoCredtCsignst(java.lang.String dsBancoCredtCsignst)
    {
        this._dsBancoCredtCsignst = dsBancoCredtCsignst;
    } //-- void setDsBancoCredtCsignst(java.lang.String) 

    /**
     * Sets the value of field 'vlCartCsign'.
     * 
     * @param vlCartCsign the value of field 'vlCartCsign'.
     */
    public void setVlCartCsign(java.math.BigDecimal vlCartCsign)
    {
        this._vlCartCsign = vlCartCsign;
    } //-- void setVlCartCsign(java.math.BigDecimal) 

    /**
     * Sets the value of field 'vlRepasMesConsign'.
     * 
     * @param vlRepasMesConsign the value of field
     * 'vlRepasMesConsign'.
     */
    public void setVlRepasMesConsign(java.math.BigDecimal vlRepasMesConsign)
    {
        this._vlRepasMesConsign = vlRepasMesConsign;
    } //-- void setVlRepasMesConsign(java.math.BigDecimal) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias4
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.detalharpiramide.response.Ocorrencias4 unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.detalharpiramide.response.Ocorrencias4) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.detalharpiramide.response.Ocorrencias4.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharpiramide.response.Ocorrencias4 unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
