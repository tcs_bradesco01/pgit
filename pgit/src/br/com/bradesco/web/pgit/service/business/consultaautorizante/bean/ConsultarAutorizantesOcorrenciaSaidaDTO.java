package br.com.bradesco.web.pgit.service.business.consultaautorizante.bean;

/**
 * Nome: ConsultarAutorizantesOcorrenciaSaidaDTO
 * <p>
 * Propůsito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarAutorizantesOcorrenciaSaidaDTO {
    
    /** Atributo qtdAutorizadoPagamento. */
    private String qtdAutorizadoPagamento;
    
    /** Atributo dtHoraManutencao. */
    private String dtHoraManutencao;
    
    /** Atributo cdTipocpfcnpj. */
    private Integer cdTipoCpfCnpj;
    
    /** Atributo cdCpfCnpj. */
    private String cdCpfCnpj;
    
    /** Atributo dsAutorizante. */
    private String dsAutorizante;
    
    /** Atributo cdAcaoEvento. */
    private Integer cdAcaoEvento;
    
    /** Atributo dsAcaoEvento. */
    private String dsAcaoEvento;
    
    /**
     * Get: qtdAutorizadoPagamento.
     *
     * @return qtdAutorizadoPagamento
     */
    public String getQtdAutorizadoPagamento() {
        return qtdAutorizadoPagamento;
    }
    
    /**
     * Set: qtdAutorizadoPagamento.
     *
     * @param qtdAutorizadoPagamento the qtd autorizado pagamento
     */
    public void setQtdAutorizadoPagamento(String qtdAutorizadoPagamento) {
        this.qtdAutorizadoPagamento = qtdAutorizadoPagamento;
    }
    
    /**
     * Get: dtHoraManutencao.
     *
     * @return dtHoraManutencao
     */
    public String getDtHoraManutencao() {
        return dtHoraManutencao;
    }
    
    /**
     * Set: dtHoraManutencao.
     *
     * @param dtHoraManutencao the dt hora manutencao
     */
    public void setDtHoraManutencao(String dtHoraManutencao) {
        this.dtHoraManutencao = dtHoraManutencao;
    }
    
    /**
     * Get: cdCpfCnpj.
     *
     * @return cdCpfCnpj
     */
    public String getCdCpfCnpj() {
        return cdCpfCnpj;
    }
    
    /**
     * Set: cdCpfCnpj.
     *
     * @param cdCpfCnpj the cd cpf cnpj
     */
    public void setCdCpfCnpj(String cdCpfCnpj) {
        this.cdCpfCnpj = cdCpfCnpj;
    }
    
    /**
     * Get: dsAutorizante.
     *
     * @return dsAutorizante
     */
    public String getDsAutorizante() {
        return dsAutorizante;
    }
    
    /**
     * Set: dsAutorizante.
     *
     * @param dsAutorizante the ds autorizante
     */
    public void setDsAutorizante(String dsAutorizante) {
        this.dsAutorizante = dsAutorizante;
    }
    
    /**
     * Get: cdAcaoEvento.
     *
     * @return cdAcaoEvento
     */
    public Integer getCdAcaoEvento() {
        return cdAcaoEvento;
    }
    
    /**
     * Set: cdAcaoEvento.
     *
     * @param cdAcaoEvento the cd acao evento
     */
    public void setCdAcaoEvento(Integer cdAcaoEvento) {
        this.cdAcaoEvento = cdAcaoEvento;
    }
    
    /**
     * Get: dsAcaoEvento.
     *
     * @return dsAcaoEvento
     */
    public String getDsAcaoEvento() {
        return dsAcaoEvento;
    }
    
    /**
     * Set: dsAcaoEvento.
     *
     * @param dsAcaoEvento the ds acao evento
     */
    public void setDsAcaoEvento(String dsAcaoEvento) {
        this.dsAcaoEvento = dsAcaoEvento;
    }

    /**
     * Nome: setCdTipoCpfCnpj
     *
     * @param cdTipoCpfCnpj
     */
    public void setCdTipoCpfCnpj(Integer cdTipoCpfCnpj) {
        this.cdTipoCpfCnpj = cdTipoCpfCnpj;
    }

    /**
     * Nome: getCdTipoCpfCnpj
     *
     * @return cdTipoCpfCnpj
     */
    public Integer getCdTipoCpfCnpj() {
        return cdTipoCpfCnpj;
    }

}
