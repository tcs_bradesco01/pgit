package br.com.bradesco.web.pgit.service.business.mantersolicliberacaolotepgto.bean;

import java.util.List;

public class ConsultarSolicLiberacaoLotePgtoSaidaDTO {

	private String codMensagem;
	private String mensagem;
	private Integer numeroLinhas;
	private List<ConsultarSolicLiberacaoLotePgtoOcorrenciasDTO> ocorrencias;

	public String getCodMensagem() {
		return codMensagem;
	}

	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}

	public String getMensagem() {
		return mensagem;
	}

	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	public Integer getNumeroLinhas() {
		return numeroLinhas;
	}

	public void setNumeroLinhas(Integer numeroLinhas) {
		this.numeroLinhas = numeroLinhas;
	}

	public List<ConsultarSolicLiberacaoLotePgtoOcorrenciasDTO> getOcorrencias() {
		return ocorrencias;
	}

	public void setOcorrencias(
			List<ConsultarSolicLiberacaoLotePgtoOcorrenciasDTO> ocorrencias) {
		this.ocorrencias = ocorrencias;
	}

}
