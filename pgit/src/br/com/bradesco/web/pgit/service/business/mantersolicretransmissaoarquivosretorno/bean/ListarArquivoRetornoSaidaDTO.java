/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantersolicretransmissaoarquivosretorno.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantersolicretransmissaoarquivosretorno.bean;

import java.math.BigDecimal;

/**
 * Nome: ListarArquivoRetornoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarArquivoRetornoSaidaDTO {
	
	/** Atributo codMensagem. */
	private String codMensagem;
	
	/** Atributo mensagem. */
	private String mensagem;
	
	/** Atributo cdPessoaJuridicaContrato. */
	private Long cdPessoaJuridicaContrato;
    
    /** Atributo dsPessoaJuridicaContrato. */
    private String dsPessoaJuridicaContrato;
    
    /** Atributo cdTipoContratoNegocio. */
    private Integer cdTipoContratoNegocio;
    
    /** Atributo dsTipoContratoNegocio. */
    private String dsTipoContratoNegocio;
    
    /** Atributo nrSequenciaContratoNegocio. */
    private Long nrSequenciaContratoNegocio;
    
    /** Atributo dsContrato. */
    private String dsContrato;
    
    /** Atributo cdPessoa. */
    private Long cdPessoa;
    
    /** Atributo corpoCpfCnpj. */
    private String corpoCpfCnpj;
    
    /** Atributo dsPessoa. */
    private String dsPessoa;
    
    /** Atributo cdTipoLayoutArquivo. */
    private Integer cdTipoLayoutArquivo;
    
    /** Atributo dsTipoLayoutArquivo. */
    private String dsTipoLayoutArquivo;
    
    /** Atributo cdClienteTransferenciaArquivo. */
    private Long cdClienteTransferenciaArquivo;
    
    /** Atributo dsArquivoRetorno. */
    private String dsArquivoRetorno;
    
    /** Atributo nrArquivoRetorno. */
    private Long nrArquivoRetorno;
    
    /** Atributo hrInclusao. */
    private String hrInclusao;
    
    /** Atributo dsSituacaoProcessamentoRetorno. */
    private String dsSituacaoProcessamentoRetorno;
    
    /** Atributo dsMeioTransmissao. */
    private String dsMeioTransmissao;
    
    /** Atributo dsAmbiente. */
    private String dsAmbiente;
    
    /** Atributo qtdeRegistroArquivoRetorno. */
    private Long qtdeRegistroArquivoRetorno;
    
    /** Atributo vlrRegistroArquivoRetorno. */
    private BigDecimal vlrRegistroArquivoRetorno;
    
    /** Atributo hrInclusaoFormatada. */
    private String hrInclusaoFormatada;
    
    /** Atributo qtdeRegistroArquivoRetornoFormatada. */
    private String qtdeRegistroArquivoRetornoFormatada;
    
    /** Atributo check. */
    private boolean check;
    
	/**
	 * Get: cdClienteTransferenciaArquivo.
	 *
	 * @return cdClienteTransferenciaArquivo
	 */
	public Long getCdClienteTransferenciaArquivo() {
		return cdClienteTransferenciaArquivo;
	}
	
	/**
	 * Set: cdClienteTransferenciaArquivo.
	 *
	 * @param cdClienteTransferenciaArquivo the cd cliente transferencia arquivo
	 */
	public void setCdClienteTransferenciaArquivo(Long cdClienteTransferenciaArquivo) {
		this.cdClienteTransferenciaArquivo = cdClienteTransferenciaArquivo;
	}
	
	/**
	 * Get: cdPessoa.
	 *
	 * @return cdPessoa
	 */
	public Long getCdPessoa() {
		return cdPessoa;
	}
	
	/**
	 * Set: cdPessoa.
	 *
	 * @param cdPessoa the cd pessoa
	 */
	public void setCdPessoa(Long cdPessoa) {
		this.cdPessoa = cdPessoa;
	}
	
	/**
	 * Get: cdPessoaJuridicaContrato.
	 *
	 * @return cdPessoaJuridicaContrato
	 */
	public Long getCdPessoaJuridicaContrato() {
		return cdPessoaJuridicaContrato;
	}
	
	/**
	 * Set: cdPessoaJuridicaContrato.
	 *
	 * @param cdPessoaJuridicaContrato the cd pessoa juridica contrato
	 */
	public void setCdPessoaJuridicaContrato(Long cdPessoaJuridicaContrato) {
		this.cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
	}
	
	/**
	 * Get: cdTipoContratoNegocio.
	 *
	 * @return cdTipoContratoNegocio
	 */
	public Integer getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}
	
	/**
	 * Set: cdTipoContratoNegocio.
	 *
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}
	
	/**
	 * Get: cdTipoLayoutArquivo.
	 *
	 * @return cdTipoLayoutArquivo
	 */
	public Integer getCdTipoLayoutArquivo() {
		return cdTipoLayoutArquivo;
	}
	
	/**
	 * Set: cdTipoLayoutArquivo.
	 *
	 * @param cdTipoLayoutArquivo the cd tipo layout arquivo
	 */
	public void setCdTipoLayoutArquivo(Integer cdTipoLayoutArquivo) {
		this.cdTipoLayoutArquivo = cdTipoLayoutArquivo;
	}
	
	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}
	
	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}
	
	/**
	 * Get: corpoCpfCnpj.
	 *
	 * @return corpoCpfCnpj
	 */
	public String getCorpoCpfCnpj() {
		return corpoCpfCnpj;
	}
	
	/**
	 * Set: corpoCpfCnpj.
	 *
	 * @param corpoCpfCnpj the corpo cpf cnpj
	 */
	public void setCorpoCpfCnpj(String corpoCpfCnpj) {
		this.corpoCpfCnpj = corpoCpfCnpj;
	}
	
	/**
	 * Get: dsAmbiente.
	 *
	 * @return dsAmbiente
	 */
	public String getDsAmbiente() {
		return dsAmbiente;
	}
	
	/**
	 * Set: dsAmbiente.
	 *
	 * @param dsAmbiente the ds ambiente
	 */
	public void setDsAmbiente(String dsAmbiente) {
		this.dsAmbiente = dsAmbiente;
	}
	
	/**
	 * Get: dsArquivoRetorno.
	 *
	 * @return dsArquivoRetorno
	 */
	public String getDsArquivoRetorno() {
		return dsArquivoRetorno;
	}
	
	/**
	 * Set: dsArquivoRetorno.
	 *
	 * @param dsArquivoRetorno the ds arquivo retorno
	 */
	public void setDsArquivoRetorno(String dsArquivoRetorno) {
		this.dsArquivoRetorno = dsArquivoRetorno;
	}
	
	/**
	 * Get: dsContrato.
	 *
	 * @return dsContrato
	 */
	public String getDsContrato() {
		return dsContrato;
	}
	
	/**
	 * Set: dsContrato.
	 *
	 * @param dsContrato the ds contrato
	 */
	public void setDsContrato(String dsContrato) {
		this.dsContrato = dsContrato;
	}
	
	/**
	 * Get: dsMeioTransmissao.
	 *
	 * @return dsMeioTransmissao
	 */
	public String getDsMeioTransmissao() {
		return dsMeioTransmissao;
	}
	
	/**
	 * Set: dsMeioTransmissao.
	 *
	 * @param dsMeioTransmissao the ds meio transmissao
	 */
	public void setDsMeioTransmissao(String dsMeioTransmissao) {
		this.dsMeioTransmissao = dsMeioTransmissao;
	}
	
	/**
	 * Get: dsPessoa.
	 *
	 * @return dsPessoa
	 */
	public String getDsPessoa() {
		return dsPessoa;
	}
	
	/**
	 * Set: dsPessoa.
	 *
	 * @param dsPessoa the ds pessoa
	 */
	public void setDsPessoa(String dsPessoa) {
		this.dsPessoa = dsPessoa;
	}
	
	/**
	 * Get: dsPessoaJuridicaContrato.
	 *
	 * @return dsPessoaJuridicaContrato
	 */
	public String getDsPessoaJuridicaContrato() {
		return dsPessoaJuridicaContrato;
	}
	
	/**
	 * Set: dsPessoaJuridicaContrato.
	 *
	 * @param dsPessoaJuridicaContrato the ds pessoa juridica contrato
	 */
	public void setDsPessoaJuridicaContrato(String dsPessoaJuridicaContrato) {
		this.dsPessoaJuridicaContrato = dsPessoaJuridicaContrato;
	}
	
	/**
	 * Get: dsSituacaoProcessamentoRetorno.
	 *
	 * @return dsSituacaoProcessamentoRetorno
	 */
	public String getDsSituacaoProcessamentoRetorno() {
		return dsSituacaoProcessamentoRetorno;
	}
	
	/**
	 * Set: dsSituacaoProcessamentoRetorno.
	 *
	 * @param dsSituacaoProcessamentoRetorno the ds situacao processamento retorno
	 */
	public void setDsSituacaoProcessamentoRetorno(
			String dsSituacaoProcessamentoRetorno) {
		this.dsSituacaoProcessamentoRetorno = dsSituacaoProcessamentoRetorno;
	}
	
	/**
	 * Get: dsTipoContratoNegocio.
	 *
	 * @return dsTipoContratoNegocio
	 */
	public String getDsTipoContratoNegocio() {
		return dsTipoContratoNegocio;
	}
	
	/**
	 * Set: dsTipoContratoNegocio.
	 *
	 * @param dsTipoContratoNegocio the ds tipo contrato negocio
	 */
	public void setDsTipoContratoNegocio(String dsTipoContratoNegocio) {
		this.dsTipoContratoNegocio = dsTipoContratoNegocio;
	}
	
	/**
	 * Get: dsTipoLayoutArquivo.
	 *
	 * @return dsTipoLayoutArquivo
	 */
	public String getDsTipoLayoutArquivo() {
		return dsTipoLayoutArquivo;
	}
	
	/**
	 * Set: dsTipoLayoutArquivo.
	 *
	 * @param dsTipoLayoutArquivo the ds tipo layout arquivo
	 */
	public void setDsTipoLayoutArquivo(String dsTipoLayoutArquivo) {
		this.dsTipoLayoutArquivo = dsTipoLayoutArquivo;
	}
	
	/**
	 * Get: hrInclusao.
	 *
	 * @return hrInclusao
	 */
	public String getHrInclusao() {
		return hrInclusao;
	}
	
	/**
	 * Set: hrInclusao.
	 *
	 * @param hrInclusao the hr inclusao
	 */
	public void setHrInclusao(String hrInclusao) {
		this.hrInclusao = hrInclusao;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	/**
	 * Get: nrArquivoRetorno.
	 *
	 * @return nrArquivoRetorno
	 */
	public Long getNrArquivoRetorno() {
		return nrArquivoRetorno;
	}
	
	/**
	 * Set: nrArquivoRetorno.
	 *
	 * @param nrArquivoRetorno the nr arquivo retorno
	 */
	public void setNrArquivoRetorno(Long nrArquivoRetorno) {
		this.nrArquivoRetorno = nrArquivoRetorno;
	}
	
	/**
	 * Get: nrSequenciaContratoNegocio.
	 *
	 * @return nrSequenciaContratoNegocio
	 */
	public Long getNrSequenciaContratoNegocio() {
		return nrSequenciaContratoNegocio;
	}
	
	/**
	 * Set: nrSequenciaContratoNegocio.
	 *
	 * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
	 */
	public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}
	
	/**
	 * Get: qtdeRegistroArquivoRetorno.
	 *
	 * @return qtdeRegistroArquivoRetorno
	 */
	public Long getQtdeRegistroArquivoRetorno() {
		return qtdeRegistroArquivoRetorno;
	}
	
	/**
	 * Set: qtdeRegistroArquivoRetorno.
	 *
	 * @param qtdeRegistroArquivoRetorno the qtde registro arquivo retorno
	 */
	public void setQtdeRegistroArquivoRetorno(Long qtdeRegistroArquivoRetorno) {
		this.qtdeRegistroArquivoRetorno = qtdeRegistroArquivoRetorno;
	}
	
	/**
	 * Get: vlrRegistroArquivoRetorno.
	 *
	 * @return vlrRegistroArquivoRetorno
	 */
	public BigDecimal getVlrRegistroArquivoRetorno() {
		return vlrRegistroArquivoRetorno;
	}
	
	/**
	 * Set: vlrRegistroArquivoRetorno.
	 *
	 * @param vlrRegistroArquivoRetorno the vlr registro arquivo retorno
	 */
	public void setVlrRegistroArquivoRetorno(BigDecimal vlrRegistroArquivoRetorno) {
		this.vlrRegistroArquivoRetorno = vlrRegistroArquivoRetorno;
	}
	
	/**
	 * Is check.
	 *
	 * @return true, if is check
	 */
	public boolean isCheck() {
		return check;
	}
	
	/**
	 * Set: check.
	 *
	 * @param check the check
	 */
	public void setCheck(boolean check) {
		this.check = check;
	}
	
	/**
	 * Get: hrInclusaoFormatada.
	 *
	 * @return hrInclusaoFormatada
	 */
	public String getHrInclusaoFormatada() {
		return hrInclusaoFormatada;
	}
	
	/**
	 * Set: hrInclusaoFormatada.
	 *
	 * @param hrInclusaoFormatada the hr inclusao formatada
	 */
	public void setHrInclusaoFormatada(String hrInclusaoFormatada) {
		this.hrInclusaoFormatada = hrInclusaoFormatada;
	}
	
	/**
	 * Get: qtdeRegistroArquivoRetornoFormatada.
	 *
	 * @return qtdeRegistroArquivoRetornoFormatada
	 */
	public String getQtdeRegistroArquivoRetornoFormatada() {
		return qtdeRegistroArquivoRetornoFormatada;
	}
	
	/**
	 * Set: qtdeRegistroArquivoRetornoFormatada.
	 *
	 * @param qtdeRegistroArquivoRetornoFormatada the qtde registro arquivo retorno formatada
	 */
	public void setQtdeRegistroArquivoRetornoFormatada(
			String qtdeRegistroArquivoRetornoFormatada) {
		this.qtdeRegistroArquivoRetornoFormatada = qtdeRegistroArquivoRetornoFormatada;
	}
		
	
}
