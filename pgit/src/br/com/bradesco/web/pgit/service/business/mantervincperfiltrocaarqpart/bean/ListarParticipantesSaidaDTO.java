/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantervincperfiltrocaarqpart.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantervincperfiltrocaarqpart.bean;

import br.com.bradesco.web.pgit.utils.CpfCnpjUtils;


/**
 * Nome: ListarParticipantesSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarParticipantesSaidaDTO {
	
    /** Atributo cdParticipante. */
    private Long cdParticipante;
    
    /** Atributo dsParticipante. */
    private String dsParticipante;
    
    /** Atributo cpf. */
    private String cpf;
	
	/** Atributo nomeRazao. */
	private String nomeRazao;
	
	/** Atributo cdGrupoEconomico. */
	private Long cdGrupoEconomico;
	
	/** Atributo dsGrupoEconomico. */
	private String dsGrupoEconomico;
	
	/** Atributo cdAtividadeEconomica. */
	private int cdAtividadeEconomica;
	
	/** Atributo dsAtividadeEconomica. */
	private String dsAtividadeEconomica;
	
	/** Atributo tipoParticipacaoFormatador. */
	private String tipoParticipacaoFormatador;
	
	/** Atributo dsSituacaoParticipacao. */
	private String dsSituacaoParticipacao;
	
	/** Atributo tipoParticipacao. */
	private int tipoParticipacao;
	
	/** Atributo cdCpfCnpj. */
	private Long cdCpfCnpj;
	
	/** Atributo cdFilialCnpj. */
	private Integer cdFilialCnpj;
	
	/** Atributo cdControleCnpj. */
	private Integer cdControleCnpj;
	
	

	
	/**
	 * Get: cdControleCnpj.
	 *
	 * @return cdControleCnpj
	 */
	public Integer getCdControleCnpj() {
	    return cdControleCnpj;
	}
	
	/**
	 * Set: cdControleCnpj.
	 *
	 * @param cdControleCnpj the cd controle cnpj
	 */
	public void setCdControleCnpj(Integer cdControleCnpj) {
	    this.cdControleCnpj = cdControleCnpj;
	}
	
	/**
	 * Get: cdCpfCnpj.
	 *
	 * @return cdCpfCnpj
	 */
	public Long getCdCpfCnpj() {
	    return cdCpfCnpj;
	}
	
	/**
	 * Set: cdCpfCnpj.
	 *
	 * @param cdCpfCnpj the cd cpf cnpj
	 */
	public void setCdCpfCnpj(Long cdCpfCnpj) {
	    this.cdCpfCnpj = cdCpfCnpj;
	}
	
	/**
	 * Get: cdFilialCnpj.
	 *
	 * @return cdFilialCnpj
	 */
	public Integer getCdFilialCnpj() {
	    return cdFilialCnpj;
	}
	
	/**
	 * Set: cdFilialCnpj.
	 *
	 * @param cdFilialCnpj the cd filial cnpj
	 */
	public void setCdFilialCnpj(Integer cdFilialCnpj) {
	    this.cdFilialCnpj = cdFilialCnpj;
	}
	
	/**
	 * Get: dsSituacaoParticipacao.
	 *
	 * @return dsSituacaoParticipacao
	 */
	public String getDsSituacaoParticipacao() {
		return dsSituacaoParticipacao;
	}
	
	/**
	 * Set: dsSituacaoParticipacao.
	 *
	 * @param dsSituacaoParticipacao the ds situacao participacao
	 */
	public void setDsSituacaoParticipacao(String dsSituacaoParticipacao) {
		this.dsSituacaoParticipacao = dsSituacaoParticipacao;
	}
	
	/**
	 * Get: cdParticipante.
	 *
	 * @return cdParticipante
	 */
	public Long getCdParticipante() {
		return cdParticipante;
	}
	
	/**
	 * Set: cdParticipante.
	 *
	 * @param cdParticipante the cd participante
	 */
	public void setCdParticipante(Long cdParticipante) {
		this.cdParticipante = cdParticipante;
	}
	
	/**
	 * Get: dsParticipante.
	 *
	 * @return dsParticipante
	 */
	public String getDsParticipante() {
		return dsParticipante;
	}
	
	/**
	 * Set: dsParticipante.
	 *
	 * @param dsParticipante the ds participante
	 */
	public void setDsParticipante(String dsParticipante) {
		this.dsParticipante = dsParticipante;
	}
	
	/**
	 * Get: cdAtividadeEconomica.
	 *
	 * @return cdAtividadeEconomica
	 */
	public int getCdAtividadeEconomica() {
		return cdAtividadeEconomica;
	}
	
	/**
	 * Set: cdAtividadeEconomica.
	 *
	 * @param cdAtividadeEconomica the cd atividade economica
	 */
	public void setCdAtividadeEconomica(int cdAtividadeEconomica) {
		this.cdAtividadeEconomica = cdAtividadeEconomica;
	}
	
	/**
	 * Get: cdGrupoEconomico.
	 *
	 * @return cdGrupoEconomico
	 */
	public Long getCdGrupoEconomico() {
		return cdGrupoEconomico;
	}
	
	/**
	 * Set: cdGrupoEconomico.
	 *
	 * @param cdGrupoEconomico the cd grupo economico
	 */
	public void setCdGrupoEconomico(Long cdGrupoEconomico) {
		this.cdGrupoEconomico = cdGrupoEconomico;
	}
	
	/**
	 * Get: dsAtividadeEconomica.
	 *
	 * @return dsAtividadeEconomica
	 */
	public String getDsAtividadeEconomica() {
		return dsAtividadeEconomica;
	}
	
	/**
	 * Set: dsAtividadeEconomica.
	 *
	 * @param dsAtividadeEconomica the ds atividade economica
	 */
	public void setDsAtividadeEconomica(String dsAtividadeEconomica) {
		this.dsAtividadeEconomica = dsAtividadeEconomica;
	}
	
	/**
	 * Get: dsGrupoEconomico.
	 *
	 * @return dsGrupoEconomico
	 */
	public String getDsGrupoEconomico() {
		return dsGrupoEconomico;
	}
	
	/**
	 * Set: dsGrupoEconomico.
	 *
	 * @param dsGrupoEconomico the ds grupo economico
	 */
	public void setDsGrupoEconomico(String dsGrupoEconomico) {
		this.dsGrupoEconomico = dsGrupoEconomico;
	}
	
	/**
	 * Get: nomeRazao.
	 *
	 * @return nomeRazao
	 */
	public String getNomeRazao() {
		return nomeRazao;
	}
	
	/**
	 * Set: nomeRazao.
	 *
	 * @param nomeRazao the nome razao
	 */
	public void setNomeRazao(String nomeRazao) {
		this.nomeRazao = nomeRazao;
	}
	
	/**
	 * Get: cpf.
	 *
	 * @return cpf
	 */
	public String getCpf() {
		return cpf;
	}
	
	/**
	 * Set: cpf.
	 *
	 * @param cpf the cpf
	 */
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	
	/**
	 * Get: tipoParticipacaoFormatador.
	 *
	 * @return tipoParticipacaoFormatador
	 */
	public String getTipoParticipacaoFormatador() {
		return tipoParticipacaoFormatador;
	}
	
	/**
	 * Set: tipoParticipacaoFormatador.
	 *
	 * @param tipoParticipacaoFormatador the tipo participacao formatador
	 */
	public void setTipoParticipacaoFormatador(String tipoParticipacaoFormatador) {
		this.tipoParticipacaoFormatador = tipoParticipacaoFormatador;
	}
	
	/**
	 * Get: tipoParticipacao.
	 *
	 * @return tipoParticipacao
	 */
	public int getTipoParticipacao() {
		return tipoParticipacao;
	}
	
	/**
	 * Set: tipoParticipacao.
	 *
	 * @param tipoParticipacao the tipo participacao
	 */
	public void setTipoParticipacao(int tipoParticipacao) {
		this.tipoParticipacao = tipoParticipacao;
	}
	
	/**
	 * Get: cpfCnpjFormatado.
	 *
	 * @return cpfCnpjFormatado
	 */
	public String getCpfCnpjFormatado() {
		return CpfCnpjUtils.formatCpfCnpjCompleto(cdCpfCnpj,
				cdFilialCnpj, cdControleCnpj);
	}

}
