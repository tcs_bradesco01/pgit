/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listarlayoutarqservico.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdTipoLayoutArquivo
     */
    private int _cdTipoLayoutArquivo = 0;

    /**
     * keeps track of state for field: _cdTipoLayoutArquivo
     */
    private boolean _has_cdTipoLayoutArquivo;

    /**
     * Field _dsTipoLayoutArquivo
     */
    private java.lang.String _dsTipoLayoutArquivo;

    /**
     * Field _cdProdutoServicoOperacao
     */
    private int _cdProdutoServicoOperacao = 0;

    /**
     * keeps track of state for field: _cdProdutoServicoOperacao
     */
    private boolean _has_cdProdutoServicoOperacao;

    /**
     * Field _dsProsutoServicoOperacao
     */
    private java.lang.String _dsProsutoServicoOperacao;

    /**
     * Field _cdIdentificadorLayoutNegocio
     */
    private int _cdIdentificadorLayoutNegocio = 0;

    /**
     * keeps track of state for field: _cdIdentificadorLayoutNegocio
     */
    private boolean _has_cdIdentificadorLayoutNegocio;

    /**
     * Field _dsIdentificadorLayoutNegocio
     */
    private java.lang.String _dsIdentificadorLayoutNegocio;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarlayoutarqservico.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdIdentificadorLayoutNegocio
     * 
     */
    public void deleteCdIdentificadorLayoutNegocio()
    {
        this._has_cdIdentificadorLayoutNegocio= false;
    } //-- void deleteCdIdentificadorLayoutNegocio() 

    /**
     * Method deleteCdProdutoServicoOperacao
     * 
     */
    public void deleteCdProdutoServicoOperacao()
    {
        this._has_cdProdutoServicoOperacao= false;
    } //-- void deleteCdProdutoServicoOperacao() 

    /**
     * Method deleteCdTipoLayoutArquivo
     * 
     */
    public void deleteCdTipoLayoutArquivo()
    {
        this._has_cdTipoLayoutArquivo= false;
    } //-- void deleteCdTipoLayoutArquivo() 

    /**
     * Returns the value of field 'cdIdentificadorLayoutNegocio'.
     * 
     * @return int
     * @return the value of field 'cdIdentificadorLayoutNegocio'.
     */
    public int getCdIdentificadorLayoutNegocio()
    {
        return this._cdIdentificadorLayoutNegocio;
    } //-- int getCdIdentificadorLayoutNegocio() 

    /**
     * Returns the value of field 'cdProdutoServicoOperacao'.
     * 
     * @return int
     * @return the value of field 'cdProdutoServicoOperacao'.
     */
    public int getCdProdutoServicoOperacao()
    {
        return this._cdProdutoServicoOperacao;
    } //-- int getCdProdutoServicoOperacao() 

    /**
     * Returns the value of field 'cdTipoLayoutArquivo'.
     * 
     * @return int
     * @return the value of field 'cdTipoLayoutArquivo'.
     */
    public int getCdTipoLayoutArquivo()
    {
        return this._cdTipoLayoutArquivo;
    } //-- int getCdTipoLayoutArquivo() 

    /**
     * Returns the value of field 'dsIdentificadorLayoutNegocio'.
     * 
     * @return String
     * @return the value of field 'dsIdentificadorLayoutNegocio'.
     */
    public java.lang.String getDsIdentificadorLayoutNegocio()
    {
        return this._dsIdentificadorLayoutNegocio;
    } //-- java.lang.String getDsIdentificadorLayoutNegocio() 

    /**
     * Returns the value of field 'dsProsutoServicoOperacao'.
     * 
     * @return String
     * @return the value of field 'dsProsutoServicoOperacao'.
     */
    public java.lang.String getDsProsutoServicoOperacao()
    {
        return this._dsProsutoServicoOperacao;
    } //-- java.lang.String getDsProsutoServicoOperacao() 

    /**
     * Returns the value of field 'dsTipoLayoutArquivo'.
     * 
     * @return String
     * @return the value of field 'dsTipoLayoutArquivo'.
     */
    public java.lang.String getDsTipoLayoutArquivo()
    {
        return this._dsTipoLayoutArquivo;
    } //-- java.lang.String getDsTipoLayoutArquivo() 

    /**
     * Method hasCdIdentificadorLayoutNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIdentificadorLayoutNegocio()
    {
        return this._has_cdIdentificadorLayoutNegocio;
    } //-- boolean hasCdIdentificadorLayoutNegocio() 

    /**
     * Method hasCdProdutoServicoOperacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdutoServicoOperacao()
    {
        return this._has_cdProdutoServicoOperacao;
    } //-- boolean hasCdProdutoServicoOperacao() 

    /**
     * Method hasCdTipoLayoutArquivo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoLayoutArquivo()
    {
        return this._has_cdTipoLayoutArquivo;
    } //-- boolean hasCdTipoLayoutArquivo() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdIdentificadorLayoutNegocio'.
     * 
     * @param cdIdentificadorLayoutNegocio the value of field
     * 'cdIdentificadorLayoutNegocio'.
     */
    public void setCdIdentificadorLayoutNegocio(int cdIdentificadorLayoutNegocio)
    {
        this._cdIdentificadorLayoutNegocio = cdIdentificadorLayoutNegocio;
        this._has_cdIdentificadorLayoutNegocio = true;
    } //-- void setCdIdentificadorLayoutNegocio(int) 

    /**
     * Sets the value of field 'cdProdutoServicoOperacao'.
     * 
     * @param cdProdutoServicoOperacao the value of field
     * 'cdProdutoServicoOperacao'.
     */
    public void setCdProdutoServicoOperacao(int cdProdutoServicoOperacao)
    {
        this._cdProdutoServicoOperacao = cdProdutoServicoOperacao;
        this._has_cdProdutoServicoOperacao = true;
    } //-- void setCdProdutoServicoOperacao(int) 

    /**
     * Sets the value of field 'cdTipoLayoutArquivo'.
     * 
     * @param cdTipoLayoutArquivo the value of field
     * 'cdTipoLayoutArquivo'.
     */
    public void setCdTipoLayoutArquivo(int cdTipoLayoutArquivo)
    {
        this._cdTipoLayoutArquivo = cdTipoLayoutArquivo;
        this._has_cdTipoLayoutArquivo = true;
    } //-- void setCdTipoLayoutArquivo(int) 

    /**
     * Sets the value of field 'dsIdentificadorLayoutNegocio'.
     * 
     * @param dsIdentificadorLayoutNegocio the value of field
     * 'dsIdentificadorLayoutNegocio'.
     */
    public void setDsIdentificadorLayoutNegocio(java.lang.String dsIdentificadorLayoutNegocio)
    {
        this._dsIdentificadorLayoutNegocio = dsIdentificadorLayoutNegocio;
    } //-- void setDsIdentificadorLayoutNegocio(java.lang.String) 

    /**
     * Sets the value of field 'dsProsutoServicoOperacao'.
     * 
     * @param dsProsutoServicoOperacao the value of field
     * 'dsProsutoServicoOperacao'.
     */
    public void setDsProsutoServicoOperacao(java.lang.String dsProsutoServicoOperacao)
    {
        this._dsProsutoServicoOperacao = dsProsutoServicoOperacao;
    } //-- void setDsProsutoServicoOperacao(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoLayoutArquivo'.
     * 
     * @param dsTipoLayoutArquivo the value of field
     * 'dsTipoLayoutArquivo'.
     */
    public void setDsTipoLayoutArquivo(java.lang.String dsTipoLayoutArquivo)
    {
        this._dsTipoLayoutArquivo = dsTipoLayoutArquivo;
    } //-- void setDsTipoLayoutArquivo(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listarlayoutarqservico.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listarlayoutarqservico.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listarlayoutarqservico.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarlayoutarqservico.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
