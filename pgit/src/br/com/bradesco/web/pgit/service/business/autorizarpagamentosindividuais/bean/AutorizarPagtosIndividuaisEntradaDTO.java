/*
 * Nome: br.com.bradesco.web.pgit.service.business.autorizarpagamentosindividuais.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.autorizarpagamentosindividuais.bean;

import java.util.ArrayList;
import java.util.List;

/**
 * Nome: AutorizarPagtosIndividuaisEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class AutorizarPagtosIndividuaisEntradaDTO {
	
	/** Atributo cdPessoaJuridicaContrato. */
	private Long cdPessoaJuridicaContrato;
    
    /** Atributo cdTipoContratoNegocio. */
    private Integer cdTipoContratoNegocio;
    
    /** Atributo nrSequenciaContratoNegocio. */
    private Long nrSequenciaContratoNegocio;
    
    /** Atributo cdTipoCanal. */
    private Integer cdTipoCanal;
    
    /** Atributo cdProdutoServicoOperacao. */
    private Integer cdProdutoServicoOperacao;
    
    /** Atributo cdOperacaoProdutoServico. */
    private Integer cdOperacaoProdutoServico;
   
   /** Atributo cdControlePagamento. */
   private String cdControlePagamento;
   
   /** Atributo numeroLinhas. */
   private Integer numeroLinhas;
   
   /** Atributo listaAutorizarIndividuais. */
   private List<AutorizarPagtosIndividuaisEntradaDTO> listaAutorizarIndividuais = new ArrayList<AutorizarPagtosIndividuaisEntradaDTO>();
   
   
   
/**
 * Get: cdControlePagamento.
 *
 * @return cdControlePagamento
 */
public String getCdControlePagamento() {
	return cdControlePagamento;
}

/**
 * Set: cdControlePagamento.
 *
 * @param cdControlePagamento the cd controle pagamento
 */
public void setCdControlePagamento(String cdControlePagamento) {
	this.cdControlePagamento = cdControlePagamento;
}

/**
 * Get: cdOperacaoProdutoServico.
 *
 * @return cdOperacaoProdutoServico
 */
public Integer getCdOperacaoProdutoServico() {
	return cdOperacaoProdutoServico;
}

/**
 * Get: numeroLinhas.
 *
 * @return numeroLinhas
 */
public Integer getNumeroLinhas() {
	return numeroLinhas;
}

/**
 * Set: numeroLinhas.
 *
 * @param numeroLinhas the numero linhas
 */
public void setNumeroLinhas(Integer numeroLinhas) {
	this.numeroLinhas = numeroLinhas;
}

/**
 * Set: cdOperacaoProdutoServico.
 *
 * @param cdOperacaoProdutoServico the cd operacao produto servico
 */
public void setCdOperacaoProdutoServico(Integer cdOperacaoProdutoServico) {
	this.cdOperacaoProdutoServico = cdOperacaoProdutoServico;
}

/**
 * Get: cdPessoaJuridicaContrato.
 *
 * @return cdPessoaJuridicaContrato
 */
public Long getCdPessoaJuridicaContrato() {
	return cdPessoaJuridicaContrato;
}

/**
 * Set: cdPessoaJuridicaContrato.
 *
 * @param cdPessoaJuridicaContrato the cd pessoa juridica contrato
 */
public void setCdPessoaJuridicaContrato(Long cdPessoaJuridicaContrato) {
	this.cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
}

/**
 * Get: cdProdutoServicoOperacao.
 *
 * @return cdProdutoServicoOperacao
 */
public Integer getCdProdutoServicoOperacao() {
	return cdProdutoServicoOperacao;
}

/**
 * Set: cdProdutoServicoOperacao.
 *
 * @param cdProdutoServicoOperacao the cd produto servico operacao
 */
public void setCdProdutoServicoOperacao(Integer cdProdutoServicoOperacao) {
	this.cdProdutoServicoOperacao = cdProdutoServicoOperacao;
}

/**
 * Get: cdTipoCanal.
 *
 * @return cdTipoCanal
 */
public Integer getCdTipoCanal() {
	return cdTipoCanal;
}

/**
 * Set: cdTipoCanal.
 *
 * @param cdTipoCanal the cd tipo canal
 */
public void setCdTipoCanal(Integer cdTipoCanal) {
	this.cdTipoCanal = cdTipoCanal;
}

/**
 * Get: cdTipoContratoNegocio.
 *
 * @return cdTipoContratoNegocio
 */
public Integer getCdTipoContratoNegocio() {
	return cdTipoContratoNegocio;
}

/**
 * Set: cdTipoContratoNegocio.
 *
 * @param cdTipoContratoNegocio the cd tipo contrato negocio
 */
public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
	this.cdTipoContratoNegocio = cdTipoContratoNegocio;
}

/**
 * Get: nrSequenciaContratoNegocio.
 *
 * @return nrSequenciaContratoNegocio
 */
public Long getNrSequenciaContratoNegocio() {
	return nrSequenciaContratoNegocio;
}

/**
 * Set: nrSequenciaContratoNegocio.
 *
 * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
 */
public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
	this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
}

/**
 * Get: listaAutorizarIndividuais.
 *
 * @return listaAutorizarIndividuais
 */
public List<AutorizarPagtosIndividuaisEntradaDTO> getListaAutorizarIndividuais() {
	return listaAutorizarIndividuais;
}

/**
 * Set: listaAutorizarIndividuais.
 *
 * @param listaAutorizarIndividuais the lista autorizar individuais
 */
public void setListaAutorizarIndividuais(
		List<AutorizarPagtosIndividuaisEntradaDTO> listaAutorizarIndividuais) {
	this.listaAutorizarIndividuais = listaAutorizarIndividuais;
}

   
   
   


}
