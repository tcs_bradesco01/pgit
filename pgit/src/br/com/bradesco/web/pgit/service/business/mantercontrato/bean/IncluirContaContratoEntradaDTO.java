/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantercontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantercontrato.bean;

/**
 * Nome: IncluirContaContratoEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class IncluirContaContratoEntradaDTO {
	
	/** Atributo cdPessoaJuridica. */
	private Long cdPessoaJuridica;
	
	/** Atributo cdTipoContratoNegocio. */
	private int cdTipoContratoNegocio;
	
	/** Atributo nrSequenciaContratoNegocio. */
	private Long nrSequenciaContratoNegocio;
	
	/** Atributo cdPessoaJuridicaVinculo. */
	private Long cdPessoaJuridicaVinculo;
	
	/** Atributo cdTipoContratoNegocioVinculo. */
	private int cdTipoContratoNegocioVinculo;
	
	/** Atributo nrSequenciaContratoNegocioVinculo. */
	private Long nrSequenciaContratoNegocioVinculo;
	
	/** Atributo cdTipoVinculoContrato. */
	private int cdTipoVinculoContrato;
	
	/**
	 * Get: cdPessoaJuridica.
	 *
	 * @return cdPessoaJuridica
	 */
	public Long getCdPessoaJuridica() {
		return cdPessoaJuridica;
	}
	
	/**
	 * Set: cdPessoaJuridica.
	 *
	 * @param cdPessoaJuridica the cd pessoa juridica
	 */
	public void setCdPessoaJuridica(Long cdPessoaJuridica) {
		this.cdPessoaJuridica = cdPessoaJuridica;
	}
	
	/**
	 * Get: cdPessoaJuridicaVinculo.
	 *
	 * @return cdPessoaJuridicaVinculo
	 */
	public Long getCdPessoaJuridicaVinculo() {
		return cdPessoaJuridicaVinculo;
	}
	
	/**
	 * Set: cdPessoaJuridicaVinculo.
	 *
	 * @param cdPessoaJuridicaVinculo the cd pessoa juridica vinculo
	 */
	public void setCdPessoaJuridicaVinculo(Long cdPessoaJuridicaVinculo) {
		this.cdPessoaJuridicaVinculo = cdPessoaJuridicaVinculo;
	}
	
	/**
	 * Get: cdTipoContratoNegocio.
	 *
	 * @return cdTipoContratoNegocio
	 */
	public int getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}
	
	/**
	 * Set: cdTipoContratoNegocio.
	 *
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(int cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}
	
	/**
	 * Get: cdTipoContratoNegocioVinculo.
	 *
	 * @return cdTipoContratoNegocioVinculo
	 */
	public int getCdTipoContratoNegocioVinculo() {
		return cdTipoContratoNegocioVinculo;
	}
	
	/**
	 * Set: cdTipoContratoNegocioVinculo.
	 *
	 * @param cdTipoContratoNegocioVinculo the cd tipo contrato negocio vinculo
	 */
	public void setCdTipoContratoNegocioVinculo(int cdTipoContratoNegocioVinculo) {
		this.cdTipoContratoNegocioVinculo = cdTipoContratoNegocioVinculo;
	}
	
	/**
	 * Get: cdTipoVinculoContrato.
	 *
	 * @return cdTipoVinculoContrato
	 */
	public int getCdTipoVinculoContrato() {
		return cdTipoVinculoContrato;
	}
	
	/**
	 * Set: cdTipoVinculoContrato.
	 *
	 * @param cdTipoVinculoContrato the cd tipo vinculo contrato
	 */
	public void setCdTipoVinculoContrato(int cdTipoVinculoContrato) {
		this.cdTipoVinculoContrato = cdTipoVinculoContrato;
	}
	
	/**
	 * Get: nrSequenciaContratoNegocio.
	 *
	 * @return nrSequenciaContratoNegocio
	 */
	public Long getNrSequenciaContratoNegocio() {
		return nrSequenciaContratoNegocio;
	}
	
	/**
	 * Set: nrSequenciaContratoNegocio.
	 *
	 * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
	 */
	public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}
	
	/**
	 * Get: nrSequenciaContratoNegocioVinculo.
	 *
	 * @return nrSequenciaContratoNegocioVinculo
	 */
	public Long getNrSequenciaContratoNegocioVinculo() {
		return nrSequenciaContratoNegocioVinculo;
	}
	
	/**
	 * Set: nrSequenciaContratoNegocioVinculo.
	 *
	 * @param nrSequenciaContratoNegocioVinculo the nr sequencia contrato negocio vinculo
	 */
	public void setNrSequenciaContratoNegocioVinculo(
			Long nrSequenciaContratoNegocioVinculo) {
		this.nrSequenciaContratoNegocioVinculo = nrSequenciaContratoNegocioVinculo;
	}
	
	
}
