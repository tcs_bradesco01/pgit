/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.registrarformalizacaomancontrato.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class RegistrarFormalizacaoManContratoRequest.
 * 
 * @version $Revision$ $Date$
 */
public class RegistrarFormalizacaoManContratoRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdPessoaJuridicaContrato
     */
    private long _cdPessoaJuridicaContrato = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridicaContrato
     */
    private boolean _has_cdPessoaJuridicaContrato;

    /**
     * Field _cdTipoContratoNegocio
     */
    private int _cdTipoContratoNegocio = 0;

    /**
     * keeps track of state for field: _cdTipoContratoNegocio
     */
    private boolean _has_cdTipoContratoNegocio;

    /**
     * Field _nrSequenciaContratoNEgocio
     */
    private long _nrSequenciaContratoNEgocio = 0;

    /**
     * keeps track of state for field: _nrSequenciaContratoNEgocio
     */
    private boolean _has_nrSequenciaContratoNEgocio;

    /**
     * Field _nrAditivoContratoNegocio
     */
    private long _nrAditivoContratoNegocio = 0;

    /**
     * keeps track of state for field: _nrAditivoContratoNegocio
     */
    private boolean _has_nrAditivoContratoNegocio;

    /**
     * Field _cdContratoNegocioPagamento
     */
    private java.lang.String _cdContratoNegocioPagamento;

    /**
     * Field _dtAssinaturaAditivo
     */
    private java.lang.String _dtAssinaturaAditivo;


      //----------------/
     //- Constructors -/
    //----------------/

    public RegistrarFormalizacaoManContratoRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.registrarformalizacaomancontrato.request.RegistrarFormalizacaoManContratoRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdPessoaJuridicaContrato
     * 
     */
    public void deleteCdPessoaJuridicaContrato()
    {
        this._has_cdPessoaJuridicaContrato= false;
    } //-- void deleteCdPessoaJuridicaContrato() 

    /**
     * Method deleteCdTipoContratoNegocio
     * 
     */
    public void deleteCdTipoContratoNegocio()
    {
        this._has_cdTipoContratoNegocio= false;
    } //-- void deleteCdTipoContratoNegocio() 

    /**
     * Method deleteNrAditivoContratoNegocio
     * 
     */
    public void deleteNrAditivoContratoNegocio()
    {
        this._has_nrAditivoContratoNegocio= false;
    } //-- void deleteNrAditivoContratoNegocio() 

    /**
     * Method deleteNrSequenciaContratoNEgocio
     * 
     */
    public void deleteNrSequenciaContratoNEgocio()
    {
        this._has_nrSequenciaContratoNEgocio= false;
    } //-- void deleteNrSequenciaContratoNEgocio() 

    /**
     * Returns the value of field 'cdContratoNegocioPagamento'.
     * 
     * @return String
     * @return the value of field 'cdContratoNegocioPagamento'.
     */
    public java.lang.String getCdContratoNegocioPagamento()
    {
        return this._cdContratoNegocioPagamento;
    } //-- java.lang.String getCdContratoNegocioPagamento() 

    /**
     * Returns the value of field 'cdPessoaJuridicaContrato'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridicaContrato'.
     */
    public long getCdPessoaJuridicaContrato()
    {
        return this._cdPessoaJuridicaContrato;
    } //-- long getCdPessoaJuridicaContrato() 

    /**
     * Returns the value of field 'cdTipoContratoNegocio'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoNegocio'.
     */
    public int getCdTipoContratoNegocio()
    {
        return this._cdTipoContratoNegocio;
    } //-- int getCdTipoContratoNegocio() 

    /**
     * Returns the value of field 'dtAssinaturaAditivo'.
     * 
     * @return String
     * @return the value of field 'dtAssinaturaAditivo'.
     */
    public java.lang.String getDtAssinaturaAditivo()
    {
        return this._dtAssinaturaAditivo;
    } //-- java.lang.String getDtAssinaturaAditivo() 

    /**
     * Returns the value of field 'nrAditivoContratoNegocio'.
     * 
     * @return long
     * @return the value of field 'nrAditivoContratoNegocio'.
     */
    public long getNrAditivoContratoNegocio()
    {
        return this._nrAditivoContratoNegocio;
    } //-- long getNrAditivoContratoNegocio() 

    /**
     * Returns the value of field 'nrSequenciaContratoNEgocio'.
     * 
     * @return long
     * @return the value of field 'nrSequenciaContratoNEgocio'.
     */
    public long getNrSequenciaContratoNEgocio()
    {
        return this._nrSequenciaContratoNEgocio;
    } //-- long getNrSequenciaContratoNEgocio() 

    /**
     * Method hasCdPessoaJuridicaContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridicaContrato()
    {
        return this._has_cdPessoaJuridicaContrato;
    } //-- boolean hasCdPessoaJuridicaContrato() 

    /**
     * Method hasCdTipoContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoNegocio()
    {
        return this._has_cdTipoContratoNegocio;
    } //-- boolean hasCdTipoContratoNegocio() 

    /**
     * Method hasNrAditivoContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrAditivoContratoNegocio()
    {
        return this._has_nrAditivoContratoNegocio;
    } //-- boolean hasNrAditivoContratoNegocio() 

    /**
     * Method hasNrSequenciaContratoNEgocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaContratoNEgocio()
    {
        return this._has_nrSequenciaContratoNEgocio;
    } //-- boolean hasNrSequenciaContratoNEgocio() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdContratoNegocioPagamento'.
     * 
     * @param cdContratoNegocioPagamento the value of field
     * 'cdContratoNegocioPagamento'.
     */
    public void setCdContratoNegocioPagamento(java.lang.String cdContratoNegocioPagamento)
    {
        this._cdContratoNegocioPagamento = cdContratoNegocioPagamento;
    } //-- void setCdContratoNegocioPagamento(java.lang.String) 

    /**
     * Sets the value of field 'cdPessoaJuridicaContrato'.
     * 
     * @param cdPessoaJuridicaContrato the value of field
     * 'cdPessoaJuridicaContrato'.
     */
    public void setCdPessoaJuridicaContrato(long cdPessoaJuridicaContrato)
    {
        this._cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
        this._has_cdPessoaJuridicaContrato = true;
    } //-- void setCdPessoaJuridicaContrato(long) 

    /**
     * Sets the value of field 'cdTipoContratoNegocio'.
     * 
     * @param cdTipoContratoNegocio the value of field
     * 'cdTipoContratoNegocio'.
     */
    public void setCdTipoContratoNegocio(int cdTipoContratoNegocio)
    {
        this._cdTipoContratoNegocio = cdTipoContratoNegocio;
        this._has_cdTipoContratoNegocio = true;
    } //-- void setCdTipoContratoNegocio(int) 

    /**
     * Sets the value of field 'dtAssinaturaAditivo'.
     * 
     * @param dtAssinaturaAditivo the value of field
     * 'dtAssinaturaAditivo'.
     */
    public void setDtAssinaturaAditivo(java.lang.String dtAssinaturaAditivo)
    {
        this._dtAssinaturaAditivo = dtAssinaturaAditivo;
    } //-- void setDtAssinaturaAditivo(java.lang.String) 

    /**
     * Sets the value of field 'nrAditivoContratoNegocio'.
     * 
     * @param nrAditivoContratoNegocio the value of field
     * 'nrAditivoContratoNegocio'.
     */
    public void setNrAditivoContratoNegocio(long nrAditivoContratoNegocio)
    {
        this._nrAditivoContratoNegocio = nrAditivoContratoNegocio;
        this._has_nrAditivoContratoNegocio = true;
    } //-- void setNrAditivoContratoNegocio(long) 

    /**
     * Sets the value of field 'nrSequenciaContratoNEgocio'.
     * 
     * @param nrSequenciaContratoNEgocio the value of field
     * 'nrSequenciaContratoNEgocio'.
     */
    public void setNrSequenciaContratoNEgocio(long nrSequenciaContratoNEgocio)
    {
        this._nrSequenciaContratoNEgocio = nrSequenciaContratoNEgocio;
        this._has_nrSequenciaContratoNEgocio = true;
    } //-- void setNrSequenciaContratoNEgocio(long) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return RegistrarFormalizacaoManContratoRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.registrarformalizacaomancontrato.request.RegistrarFormalizacaoManContratoRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.registrarformalizacaomancontrato.request.RegistrarFormalizacaoManContratoRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.registrarformalizacaomancontrato.request.RegistrarFormalizacaoManContratoRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.registrarformalizacaomancontrato.request.RegistrarFormalizacaoManContratoRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
