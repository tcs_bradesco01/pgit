/*
 * Nome: br.com.bradesco.web.pgit.service.business.manterambienteoperacaocontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.manterambienteoperacaocontrato.bean;

/**
 * Nome: IncluirAlteracaoAmbienteEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class IncluirAlteracaoAmbienteEntradaDTO {
	
	/** Atributo cdPessoaJuridicaNegocio. */
	private long cdPessoaJuridicaNegocio; 
	
	/** Atributo cdTipoContratoNegocio. */
	private int cdTipoContratoNegocio; 
	
	/** Atributo nrSequenciaContratoNegocio. */
	private long nrSequenciaContratoNegocio; 
	
	/** Atributo cdServico. */
	private int cdServico; 
	
	/** Atributo cdModalidade. */
	private int cdModalidade; 
	
	/** Atributo cdRelacionamentoProduto. */
	private int cdRelacionamentoProduto; 
	
	/** Atributo cdFormaMudanca. */
	private int cdFormaMudanca;
	
	/** Atributo dtMudanca. */
	private String dtMudanca;
	
	/** Atributo cdIndicadorArmazenamentoInformacao. */
	private int cdIndicadorArmazenamentoInformacao;
	
	/** Atributo cdAmbienteAtivoSolicitacao. */
	private String cdAmbienteAtivoSolicitacao;
		
	/**
	 * Get: cdAmbienteAtivoSolicitacao.
	 *
	 * @return cdAmbienteAtivoSolicitacao
	 */
	public String getCdAmbienteAtivoSolicitacao() {
		return cdAmbienteAtivoSolicitacao;
	}
	
	/**
	 * Set: cdAmbienteAtivoSolicitacao.
	 *
	 * @param cdAmbienteAtivoSolicitacao the cd ambiente ativo solicitacao
	 */
	public void setCdAmbienteAtivoSolicitacao(String cdAmbienteAtivoSolicitacao) {
		this.cdAmbienteAtivoSolicitacao = cdAmbienteAtivoSolicitacao;
	}
	
	/**
	 * Get: cdFormaMudanca.
	 *
	 * @return cdFormaMudanca
	 */
	public int getCdFormaMudanca() {
		return cdFormaMudanca;
	}
	
	/**
	 * Set: cdFormaMudanca.
	 *
	 * @param cdFormaMudanca the cd forma mudanca
	 */
	public void setCdFormaMudanca(int cdFormaMudanca) {
		this.cdFormaMudanca = cdFormaMudanca;
	}
	
	/**
	 * Get: cdIndicadorArmazenamentoInformacao.
	 *
	 * @return cdIndicadorArmazenamentoInformacao
	 */
	public int getCdIndicadorArmazenamentoInformacao() {
		return cdIndicadorArmazenamentoInformacao;
	}
	
	/**
	 * Set: cdIndicadorArmazenamentoInformacao.
	 *
	 * @param cdIndicadorArmazenamentoInformacao the cd indicador armazenamento informacao
	 */
	public void setCdIndicadorArmazenamentoInformacao(
			int cdIndicadorArmazenamentoInformacao) {
		this.cdIndicadorArmazenamentoInformacao = cdIndicadorArmazenamentoInformacao;
	}
	
	/**
	 * Get: cdModalidade.
	 *
	 * @return cdModalidade
	 */
	public int getCdModalidade() {
		return cdModalidade;
	}
	
	/**
	 * Set: cdModalidade.
	 *
	 * @param cdModalidade the cd modalidade
	 */
	public void setCdModalidade(int cdModalidade) {
		this.cdModalidade = cdModalidade;
	}
	
	/**
	 * Get: cdPessoaJuridicaNegocio.
	 *
	 * @return cdPessoaJuridicaNegocio
	 */
	public long getCdPessoaJuridicaNegocio() {
		return cdPessoaJuridicaNegocio;
	}
	
	/**
	 * Set: cdPessoaJuridicaNegocio.
	 *
	 * @param cdPessoaJuridicaNegocio the cd pessoa juridica negocio
	 */
	public void setCdPessoaJuridicaNegocio(long cdPessoaJuridicaNegocio) {
		this.cdPessoaJuridicaNegocio = cdPessoaJuridicaNegocio;
	}
	
	/**
	 * Get: cdRelacionamentoProduto.
	 *
	 * @return cdRelacionamentoProduto
	 */
	public int getCdRelacionamentoProduto() {
		return cdRelacionamentoProduto;
	}
	
	/**
	 * Set: cdRelacionamentoProduto.
	 *
	 * @param cdRelacionamentoProduto the cd relacionamento produto
	 */
	public void setCdRelacionamentoProduto(int cdRelacionamentoProduto) {
		this.cdRelacionamentoProduto = cdRelacionamentoProduto;
	}
	
	/**
	 * Get: cdServico.
	 *
	 * @return cdServico
	 */
	public int getCdServico() {
		return cdServico;
	}
	
	/**
	 * Set: cdServico.
	 *
	 * @param cdServico the cd servico
	 */
	public void setCdServico(int cdServico) {
		this.cdServico = cdServico;
	}
	
	/**
	 * Get: cdTipoContratoNegocio.
	 *
	 * @return cdTipoContratoNegocio
	 */
	public int getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}
	
	/**
	 * Set: cdTipoContratoNegocio.
	 *
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(int cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}
	
	/**
	 * Get: dtMudanca.
	 *
	 * @return dtMudanca
	 */
	public String getDtMudanca() {
		return dtMudanca;
	}
	
	/**
	 * Set: dtMudanca.
	 *
	 * @param dtMudanca the dt mudanca
	 */
	public void setDtMudanca(String dtMudanca) {
		this.dtMudanca = dtMudanca;
	}
	
	/**
	 * Get: nrSequenciaContratoNegocio.
	 *
	 * @return nrSequenciaContratoNegocio
	 */
	public long getNrSequenciaContratoNegocio() {
		return nrSequenciaContratoNegocio;
	}
	
	/**
	 * Set: nrSequenciaContratoNegocio.
	 *
	 * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
	 */
	public void setNrSequenciaContratoNegocio(long nrSequenciaContratoNegocio) {
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}
	
}
