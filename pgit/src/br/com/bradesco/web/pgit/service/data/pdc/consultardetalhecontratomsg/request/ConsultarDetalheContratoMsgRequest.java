/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultardetalhecontratomsg.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ConsultarDetalheContratoMsgRequest.
 * 
 * @version $Revision$ $Date$
 */
public class ConsultarDetalheContratoMsgRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdCtrlMensagemSalarial
     */
    private int _cdCtrlMensagemSalarial = 0;

    /**
     * keeps track of state for field: _cdCtrlMensagemSalarial
     */
    private boolean _has_cdCtrlMensagemSalarial;

    /**
     * Field _cdPessoa
     */
    private long _cdPessoa = 0;

    /**
     * keeps track of state for field: _cdPessoa
     */
    private boolean _has_cdPessoa;

    /**
     * Field _cdTipoContrato
     */
    private int _cdTipoContrato = 0;

    /**
     * keeps track of state for field: _cdTipoContrato
     */
    private boolean _has_cdTipoContrato;

    /**
     * Field _nrSeqContrato
     */
    private long _nrSeqContrato = 0;

    /**
     * keeps track of state for field: _nrSeqContrato
     */
    private boolean _has_nrSeqContrato;

    /**
     * Field _qtConsultas
     */
    private int _qtConsultas = 0;

    /**
     * keeps track of state for field: _qtConsultas
     */
    private boolean _has_qtConsultas;


      //----------------/
     //- Constructors -/
    //----------------/

    public ConsultarDetalheContratoMsgRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultardetalhecontratomsg.request.ConsultarDetalheContratoMsgRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdCtrlMensagemSalarial
     * 
     */
    public void deleteCdCtrlMensagemSalarial()
    {
        this._has_cdCtrlMensagemSalarial= false;
    } //-- void deleteCdCtrlMensagemSalarial() 

    /**
     * Method deleteCdPessoa
     * 
     */
    public void deleteCdPessoa()
    {
        this._has_cdPessoa= false;
    } //-- void deleteCdPessoa() 

    /**
     * Method deleteCdTipoContrato
     * 
     */
    public void deleteCdTipoContrato()
    {
        this._has_cdTipoContrato= false;
    } //-- void deleteCdTipoContrato() 

    /**
     * Method deleteNrSeqContrato
     * 
     */
    public void deleteNrSeqContrato()
    {
        this._has_nrSeqContrato= false;
    } //-- void deleteNrSeqContrato() 

    /**
     * Method deleteQtConsultas
     * 
     */
    public void deleteQtConsultas()
    {
        this._has_qtConsultas= false;
    } //-- void deleteQtConsultas() 

    /**
     * Returns the value of field 'cdCtrlMensagemSalarial'.
     * 
     * @return int
     * @return the value of field 'cdCtrlMensagemSalarial'.
     */
    public int getCdCtrlMensagemSalarial()
    {
        return this._cdCtrlMensagemSalarial;
    } //-- int getCdCtrlMensagemSalarial() 

    /**
     * Returns the value of field 'cdPessoa'.
     * 
     * @return long
     * @return the value of field 'cdPessoa'.
     */
    public long getCdPessoa()
    {
        return this._cdPessoa;
    } //-- long getCdPessoa() 

    /**
     * Returns the value of field 'cdTipoContrato'.
     * 
     * @return int
     * @return the value of field 'cdTipoContrato'.
     */
    public int getCdTipoContrato()
    {
        return this._cdTipoContrato;
    } //-- int getCdTipoContrato() 

    /**
     * Returns the value of field 'nrSeqContrato'.
     * 
     * @return long
     * @return the value of field 'nrSeqContrato'.
     */
    public long getNrSeqContrato()
    {
        return this._nrSeqContrato;
    } //-- long getNrSeqContrato() 

    /**
     * Returns the value of field 'qtConsultas'.
     * 
     * @return int
     * @return the value of field 'qtConsultas'.
     */
    public int getQtConsultas()
    {
        return this._qtConsultas;
    } //-- int getQtConsultas() 

    /**
     * Method hasCdCtrlMensagemSalarial
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCtrlMensagemSalarial()
    {
        return this._has_cdCtrlMensagemSalarial;
    } //-- boolean hasCdCtrlMensagemSalarial() 

    /**
     * Method hasCdPessoa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoa()
    {
        return this._has_cdPessoa;
    } //-- boolean hasCdPessoa() 

    /**
     * Method hasCdTipoContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContrato()
    {
        return this._has_cdTipoContrato;
    } //-- boolean hasCdTipoContrato() 

    /**
     * Method hasNrSeqContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSeqContrato()
    {
        return this._has_nrSeqContrato;
    } //-- boolean hasNrSeqContrato() 

    /**
     * Method hasQtConsultas
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtConsultas()
    {
        return this._has_qtConsultas;
    } //-- boolean hasQtConsultas() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdCtrlMensagemSalarial'.
     * 
     * @param cdCtrlMensagemSalarial the value of field
     * 'cdCtrlMensagemSalarial'.
     */
    public void setCdCtrlMensagemSalarial(int cdCtrlMensagemSalarial)
    {
        this._cdCtrlMensagemSalarial = cdCtrlMensagemSalarial;
        this._has_cdCtrlMensagemSalarial = true;
    } //-- void setCdCtrlMensagemSalarial(int) 

    /**
     * Sets the value of field 'cdPessoa'.
     * 
     * @param cdPessoa the value of field 'cdPessoa'.
     */
    public void setCdPessoa(long cdPessoa)
    {
        this._cdPessoa = cdPessoa;
        this._has_cdPessoa = true;
    } //-- void setCdPessoa(long) 

    /**
     * Sets the value of field 'cdTipoContrato'.
     * 
     * @param cdTipoContrato the value of field 'cdTipoContrato'.
     */
    public void setCdTipoContrato(int cdTipoContrato)
    {
        this._cdTipoContrato = cdTipoContrato;
        this._has_cdTipoContrato = true;
    } //-- void setCdTipoContrato(int) 

    /**
     * Sets the value of field 'nrSeqContrato'.
     * 
     * @param nrSeqContrato the value of field 'nrSeqContrato'.
     */
    public void setNrSeqContrato(long nrSeqContrato)
    {
        this._nrSeqContrato = nrSeqContrato;
        this._has_nrSeqContrato = true;
    } //-- void setNrSeqContrato(long) 

    /**
     * Sets the value of field 'qtConsultas'.
     * 
     * @param qtConsultas the value of field 'qtConsultas'.
     */
    public void setQtConsultas(int qtConsultas)
    {
        this._qtConsultas = qtConsultas;
        this._has_qtConsultas = true;
    } //-- void setQtConsultas(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ConsultarDetalheContratoMsgRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultardetalhecontratomsg.request.ConsultarDetalheContratoMsgRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultardetalhecontratomsg.request.ConsultarDetalheContratoMsgRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultardetalhecontratomsg.request.ConsultarDetalheContratoMsgRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultardetalhecontratomsg.request.ConsultarDetalheContratoMsgRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
