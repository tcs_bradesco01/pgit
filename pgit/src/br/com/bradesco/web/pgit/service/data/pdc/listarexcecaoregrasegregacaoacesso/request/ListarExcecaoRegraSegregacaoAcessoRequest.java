/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listarexcecaoregrasegregacaoacesso.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ListarExcecaoRegraSegregacaoAcessoRequest.
 * 
 * @version $Revision$ $Date$
 */
public class ListarExcecaoRegraSegregacaoAcessoRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdRegraAcessoDado
     */
    private int _cdRegraAcessoDado = 0;

    /**
     * keeps track of state for field: _cdRegraAcessoDado
     */
    private boolean _has_cdRegraAcessoDado;

    /**
     * Field _cdExcecaoAcessoDado
     */
    private int _cdExcecaoAcessoDado = 0;

    /**
     * keeps track of state for field: _cdExcecaoAcessoDado
     */
    private boolean _has_cdExcecaoAcessoDado;

    /**
     * Field _cdTipoUnidadeUsuario
     */
    private int _cdTipoUnidadeUsuario = 0;

    /**
     * keeps track of state for field: _cdTipoUnidadeUsuario
     */
    private boolean _has_cdTipoUnidadeUsuario;

    /**
     * Field _cdPessoaJuridicaUsuario
     */
    private long _cdPessoaJuridicaUsuario = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridicaUsuario
     */
    private boolean _has_cdPessoaJuridicaUsuario;

    /**
     * Field _nrUnidadeOrganizacionalUsuario
     */
    private int _nrUnidadeOrganizacionalUsuario = 0;

    /**
     * keeps track of state for field:
     * _nrUnidadeOrganizacionalUsuario
     */
    private boolean _has_nrUnidadeOrganizacionalUsuario;

    /**
     * Field _cdTipoExcecaoAcesso
     */
    private int _cdTipoExcecaoAcesso = 0;

    /**
     * keeps track of state for field: _cdTipoExcecaoAcesso
     */
    private boolean _has_cdTipoExcecaoAcesso;

    /**
     * Field _cdTipoAbrangenciaExcecao
     */
    private int _cdTipoAbrangenciaExcecao = 0;

    /**
     * keeps track of state for field: _cdTipoAbrangenciaExcecao
     */
    private boolean _has_cdTipoAbrangenciaExcecao;

    /**
     * Field _qtConsultas
     */
    private int _qtConsultas = 0;

    /**
     * keeps track of state for field: _qtConsultas
     */
    private boolean _has_qtConsultas;


      //----------------/
     //- Constructors -/
    //----------------/

    public ListarExcecaoRegraSegregacaoAcessoRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarexcecaoregrasegregacaoacesso.request.ListarExcecaoRegraSegregacaoAcessoRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdExcecaoAcessoDado
     * 
     */
    public void deleteCdExcecaoAcessoDado()
    {
        this._has_cdExcecaoAcessoDado= false;
    } //-- void deleteCdExcecaoAcessoDado() 

    /**
     * Method deleteCdPessoaJuridicaUsuario
     * 
     */
    public void deleteCdPessoaJuridicaUsuario()
    {
        this._has_cdPessoaJuridicaUsuario= false;
    } //-- void deleteCdPessoaJuridicaUsuario() 

    /**
     * Method deleteCdRegraAcessoDado
     * 
     */
    public void deleteCdRegraAcessoDado()
    {
        this._has_cdRegraAcessoDado= false;
    } //-- void deleteCdRegraAcessoDado() 

    /**
     * Method deleteCdTipoAbrangenciaExcecao
     * 
     */
    public void deleteCdTipoAbrangenciaExcecao()
    {
        this._has_cdTipoAbrangenciaExcecao= false;
    } //-- void deleteCdTipoAbrangenciaExcecao() 

    /**
     * Method deleteCdTipoExcecaoAcesso
     * 
     */
    public void deleteCdTipoExcecaoAcesso()
    {
        this._has_cdTipoExcecaoAcesso= false;
    } //-- void deleteCdTipoExcecaoAcesso() 

    /**
     * Method deleteCdTipoUnidadeUsuario
     * 
     */
    public void deleteCdTipoUnidadeUsuario()
    {
        this._has_cdTipoUnidadeUsuario= false;
    } //-- void deleteCdTipoUnidadeUsuario() 

    /**
     * Method deleteNrUnidadeOrganizacionalUsuario
     * 
     */
    public void deleteNrUnidadeOrganizacionalUsuario()
    {
        this._has_nrUnidadeOrganizacionalUsuario= false;
    } //-- void deleteNrUnidadeOrganizacionalUsuario() 

    /**
     * Method deleteQtConsultas
     * 
     */
    public void deleteQtConsultas()
    {
        this._has_qtConsultas= false;
    } //-- void deleteQtConsultas() 

    /**
     * Returns the value of field 'cdExcecaoAcessoDado'.
     * 
     * @return int
     * @return the value of field 'cdExcecaoAcessoDado'.
     */
    public int getCdExcecaoAcessoDado()
    {
        return this._cdExcecaoAcessoDado;
    } //-- int getCdExcecaoAcessoDado() 

    /**
     * Returns the value of field 'cdPessoaJuridicaUsuario'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridicaUsuario'.
     */
    public long getCdPessoaJuridicaUsuario()
    {
        return this._cdPessoaJuridicaUsuario;
    } //-- long getCdPessoaJuridicaUsuario() 

    /**
     * Returns the value of field 'cdRegraAcessoDado'.
     * 
     * @return int
     * @return the value of field 'cdRegraAcessoDado'.
     */
    public int getCdRegraAcessoDado()
    {
        return this._cdRegraAcessoDado;
    } //-- int getCdRegraAcessoDado() 

    /**
     * Returns the value of field 'cdTipoAbrangenciaExcecao'.
     * 
     * @return int
     * @return the value of field 'cdTipoAbrangenciaExcecao'.
     */
    public int getCdTipoAbrangenciaExcecao()
    {
        return this._cdTipoAbrangenciaExcecao;
    } //-- int getCdTipoAbrangenciaExcecao() 

    /**
     * Returns the value of field 'cdTipoExcecaoAcesso'.
     * 
     * @return int
     * @return the value of field 'cdTipoExcecaoAcesso'.
     */
    public int getCdTipoExcecaoAcesso()
    {
        return this._cdTipoExcecaoAcesso;
    } //-- int getCdTipoExcecaoAcesso() 

    /**
     * Returns the value of field 'cdTipoUnidadeUsuario'.
     * 
     * @return int
     * @return the value of field 'cdTipoUnidadeUsuario'.
     */
    public int getCdTipoUnidadeUsuario()
    {
        return this._cdTipoUnidadeUsuario;
    } //-- int getCdTipoUnidadeUsuario() 

    /**
     * Returns the value of field 'nrUnidadeOrganizacionalUsuario'.
     * 
     * @return int
     * @return the value of field 'nrUnidadeOrganizacionalUsuario'.
     */
    public int getNrUnidadeOrganizacionalUsuario()
    {
        return this._nrUnidadeOrganizacionalUsuario;
    } //-- int getNrUnidadeOrganizacionalUsuario() 

    /**
     * Returns the value of field 'qtConsultas'.
     * 
     * @return int
     * @return the value of field 'qtConsultas'.
     */
    public int getQtConsultas()
    {
        return this._qtConsultas;
    } //-- int getQtConsultas() 

    /**
     * Method hasCdExcecaoAcessoDado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdExcecaoAcessoDado()
    {
        return this._has_cdExcecaoAcessoDado;
    } //-- boolean hasCdExcecaoAcessoDado() 

    /**
     * Method hasCdPessoaJuridicaUsuario
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridicaUsuario()
    {
        return this._has_cdPessoaJuridicaUsuario;
    } //-- boolean hasCdPessoaJuridicaUsuario() 

    /**
     * Method hasCdRegraAcessoDado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdRegraAcessoDado()
    {
        return this._has_cdRegraAcessoDado;
    } //-- boolean hasCdRegraAcessoDado() 

    /**
     * Method hasCdTipoAbrangenciaExcecao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoAbrangenciaExcecao()
    {
        return this._has_cdTipoAbrangenciaExcecao;
    } //-- boolean hasCdTipoAbrangenciaExcecao() 

    /**
     * Method hasCdTipoExcecaoAcesso
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoExcecaoAcesso()
    {
        return this._has_cdTipoExcecaoAcesso;
    } //-- boolean hasCdTipoExcecaoAcesso() 

    /**
     * Method hasCdTipoUnidadeUsuario
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoUnidadeUsuario()
    {
        return this._has_cdTipoUnidadeUsuario;
    } //-- boolean hasCdTipoUnidadeUsuario() 

    /**
     * Method hasNrUnidadeOrganizacionalUsuario
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrUnidadeOrganizacionalUsuario()
    {
        return this._has_nrUnidadeOrganizacionalUsuario;
    } //-- boolean hasNrUnidadeOrganizacionalUsuario() 

    /**
     * Method hasQtConsultas
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtConsultas()
    {
        return this._has_qtConsultas;
    } //-- boolean hasQtConsultas() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdExcecaoAcessoDado'.
     * 
     * @param cdExcecaoAcessoDado the value of field
     * 'cdExcecaoAcessoDado'.
     */
    public void setCdExcecaoAcessoDado(int cdExcecaoAcessoDado)
    {
        this._cdExcecaoAcessoDado = cdExcecaoAcessoDado;
        this._has_cdExcecaoAcessoDado = true;
    } //-- void setCdExcecaoAcessoDado(int) 

    /**
     * Sets the value of field 'cdPessoaJuridicaUsuario'.
     * 
     * @param cdPessoaJuridicaUsuario the value of field
     * 'cdPessoaJuridicaUsuario'.
     */
    public void setCdPessoaJuridicaUsuario(long cdPessoaJuridicaUsuario)
    {
        this._cdPessoaJuridicaUsuario = cdPessoaJuridicaUsuario;
        this._has_cdPessoaJuridicaUsuario = true;
    } //-- void setCdPessoaJuridicaUsuario(long) 

    /**
     * Sets the value of field 'cdRegraAcessoDado'.
     * 
     * @param cdRegraAcessoDado the value of field
     * 'cdRegraAcessoDado'.
     */
    public void setCdRegraAcessoDado(int cdRegraAcessoDado)
    {
        this._cdRegraAcessoDado = cdRegraAcessoDado;
        this._has_cdRegraAcessoDado = true;
    } //-- void setCdRegraAcessoDado(int) 

    /**
     * Sets the value of field 'cdTipoAbrangenciaExcecao'.
     * 
     * @param cdTipoAbrangenciaExcecao the value of field
     * 'cdTipoAbrangenciaExcecao'.
     */
    public void setCdTipoAbrangenciaExcecao(int cdTipoAbrangenciaExcecao)
    {
        this._cdTipoAbrangenciaExcecao = cdTipoAbrangenciaExcecao;
        this._has_cdTipoAbrangenciaExcecao = true;
    } //-- void setCdTipoAbrangenciaExcecao(int) 

    /**
     * Sets the value of field 'cdTipoExcecaoAcesso'.
     * 
     * @param cdTipoExcecaoAcesso the value of field
     * 'cdTipoExcecaoAcesso'.
     */
    public void setCdTipoExcecaoAcesso(int cdTipoExcecaoAcesso)
    {
        this._cdTipoExcecaoAcesso = cdTipoExcecaoAcesso;
        this._has_cdTipoExcecaoAcesso = true;
    } //-- void setCdTipoExcecaoAcesso(int) 

    /**
     * Sets the value of field 'cdTipoUnidadeUsuario'.
     * 
     * @param cdTipoUnidadeUsuario the value of field
     * 'cdTipoUnidadeUsuario'.
     */
    public void setCdTipoUnidadeUsuario(int cdTipoUnidadeUsuario)
    {
        this._cdTipoUnidadeUsuario = cdTipoUnidadeUsuario;
        this._has_cdTipoUnidadeUsuario = true;
    } //-- void setCdTipoUnidadeUsuario(int) 

    /**
     * Sets the value of field 'nrUnidadeOrganizacionalUsuario'.
     * 
     * @param nrUnidadeOrganizacionalUsuario the value of field
     * 'nrUnidadeOrganizacionalUsuario'.
     */
    public void setNrUnidadeOrganizacionalUsuario(int nrUnidadeOrganizacionalUsuario)
    {
        this._nrUnidadeOrganizacionalUsuario = nrUnidadeOrganizacionalUsuario;
        this._has_nrUnidadeOrganizacionalUsuario = true;
    } //-- void setNrUnidadeOrganizacionalUsuario(int) 

    /**
     * Sets the value of field 'qtConsultas'.
     * 
     * @param qtConsultas the value of field 'qtConsultas'.
     */
    public void setQtConsultas(int qtConsultas)
    {
        this._qtConsultas = qtConsultas;
        this._has_qtConsultas = true;
    } //-- void setQtConsultas(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ListarExcecaoRegraSegregacaoAcessoRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listarexcecaoregrasegregacaoacesso.request.ListarExcecaoRegraSegregacaoAcessoRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listarexcecaoregrasegregacaoacesso.request.ListarExcecaoRegraSegregacaoAcessoRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listarexcecaoregrasegregacaoacesso.request.ListarExcecaoRegraSegregacaoAcessoRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarexcecaoregrasegregacaoacesso.request.ListarExcecaoRegraSegregacaoAcessoRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
