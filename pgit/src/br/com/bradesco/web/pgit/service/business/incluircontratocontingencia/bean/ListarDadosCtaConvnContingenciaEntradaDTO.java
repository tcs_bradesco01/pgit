/*
 * Nome: br.com.bradesco.web.pgit.service.business.incluircontratocontingencia.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.incluircontratocontingencia.bean;

import java.util.List;

/**
 * Nome: ListarDadosCtaConvnContingenciaEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarDadosCtaConvnContingenciaEntradaDTO {

	/** Atributo cdCpfCnpjRepresentante. */
	private Long cdCpfCnpjRepresentante;
	
	/** Atributo cdFilialCnpjRepresentante. */
	private Integer cdFilialCnpjRepresentante;
	
	/** Atributo cdControleCnpjRepresentante. */
	private Integer cdControleCnpjRepresentante;
	
	/** Atributo nrOcorrencias. */
	private Integer nrOcorrencias;

	/** Atributo ocorrencias. */
	private List<ListarDadosCtaConvnContingenciaOcorrenciasEntradaDTO> ocorrencias;

	/**
	 * Get: cdCpfCnpjRepresentante.
	 *
	 * @return cdCpfCnpjRepresentante
	 */
	public Long getCdCpfCnpjRepresentante() {
		return cdCpfCnpjRepresentante;
	}

	/**
	 * Set: cdCpfCnpjRepresentante.
	 *
	 * @param cdCpfCnpjRepresentante the cd cpf cnpj representante
	 */
	public void setCdCpfCnpjRepresentante(Long cdCpfCnpjRepresentante) {
		this.cdCpfCnpjRepresentante = cdCpfCnpjRepresentante;
	}

	/**
	 * Get: cdFilialCnpjRepresentante.
	 *
	 * @return cdFilialCnpjRepresentante
	 */
	public Integer getCdFilialCnpjRepresentante() {
		return cdFilialCnpjRepresentante;
	}

	/**
	 * Set: cdFilialCnpjRepresentante.
	 *
	 * @param cdFilialCnpjRepresentante the cd filial cnpj representante
	 */
	public void setCdFilialCnpjRepresentante(Integer cdFilialCnpjRepresentante) {
		this.cdFilialCnpjRepresentante = cdFilialCnpjRepresentante;
	}

	/**
	 * Get: cdControleCnpjRepresentante.
	 *
	 * @return cdControleCnpjRepresentante
	 */
	public Integer getCdControleCnpjRepresentante() {
		return cdControleCnpjRepresentante;
	}

	/**
	 * Set: cdControleCnpjRepresentante.
	 *
	 * @param cdControleCnpjRepresentante the cd controle cnpj representante
	 */
	public void setCdControleCnpjRepresentante(Integer cdControleCnpjRepresentante) {
		this.cdControleCnpjRepresentante = cdControleCnpjRepresentante;
	}

	/**
	 * Get: ocorrencias.
	 *
	 * @return ocorrencias
	 */
	public List<ListarDadosCtaConvnContingenciaOcorrenciasEntradaDTO> getOcorrencias() {
		return ocorrencias;
	}

	/**
	 * Set: ocorrencias.
	 *
	 * @param ocorrencias the ocorrencias
	 */
	public void setOcorrencias(
			List<ListarDadosCtaConvnContingenciaOcorrenciasEntradaDTO> ocorrencias) {
		this.ocorrencias = ocorrencias;
	}

	/**
	 * Get: nrOcorrencias.
	 *
	 * @return nrOcorrencias
	 */
	public Integer getNrOcorrencias() {
		return nrOcorrencias;
	}

	/**
	 * Set: nrOcorrencias.
	 *
	 * @param nrOcorrencias the nr ocorrencias
	 */
	public void setNrOcorrencias(Integer nrOcorrencias) {
		this.nrOcorrencias = nrOcorrencias;
	}
}