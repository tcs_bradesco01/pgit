/*
 * Nome: br.com.bradesco.web.pgit.service.business.tipocomprovante.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.tipocomprovante.bean;

/**
 * Nome: ListarTipoComprovanteSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarTipoComprovanteSaidaDTO {
	
	/** Atributo codMensagem. */
	private String codMensagem;
	
	/** Atributo mensagem. */
	private String mensagem;
	
	/** Atributo cdComprovanteSalarial. */
	private int cdComprovanteSalarial;
	
	/** Atributo dsComprovanteSalarial. */
	private String dsComprovanteSalarial;
	
	/** Atributo cdMensagemPadraoOrganizacao. */
	private int cdMensagemPadraoOrganizacao;
	
	/** Atributo dsMensagemPadraoOrganizacao. */
	private String dsMensagemPadraoOrganizacao;
	
	/**
	 * Get: cdComprovanteSalarial.
	 *
	 * @return cdComprovanteSalarial
	 */
	public int getCdComprovanteSalarial() {
		return cdComprovanteSalarial;
	}
	
	/**
	 * Set: cdComprovanteSalarial.
	 *
	 * @param cdComprovanteSalarial the cd comprovante salarial
	 */
	public void setCdComprovanteSalarial(int cdComprovanteSalarial) {
		this.cdComprovanteSalarial = cdComprovanteSalarial;
	}
	
	/**
	 * Get: cdMensagemPadraoOrganizacao.
	 *
	 * @return cdMensagemPadraoOrganizacao
	 */
	public int getCdMensagemPadraoOrganizacao() {
		return cdMensagemPadraoOrganizacao;
	}
	
	/**
	 * Set: cdMensagemPadraoOrganizacao.
	 *
	 * @param cdMensagemPadraoOrganizacao the cd mensagem padrao organizacao
	 */
	public void setCdMensagemPadraoOrganizacao(int cdMensagemPadraoOrganizacao) {
		this.cdMensagemPadraoOrganizacao = cdMensagemPadraoOrganizacao;
	}
	
	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}
	
	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}
	
	/**
	 * Get: dsComprovanteSalarial.
	 *
	 * @return dsComprovanteSalarial
	 */
	public String getDsComprovanteSalarial() {
		return dsComprovanteSalarial;
	}
	
	/**
	 * Set: dsComprovanteSalarial.
	 *
	 * @param dsComprovanteSalarial the ds comprovante salarial
	 */
	public void setDsComprovanteSalarial(String dsComprovanteSalarial) {
		this.dsComprovanteSalarial = dsComprovanteSalarial;
	}
	
	/**
	 * Get: dsMensagemPadraoOrganizacao.
	 *
	 * @return dsMensagemPadraoOrganizacao
	 */
	public String getDsMensagemPadraoOrganizacao() {
		return dsMensagemPadraoOrganizacao;
	}
	
	/**
	 * Set: dsMensagemPadraoOrganizacao.
	 *
	 * @param dsMensagemPadraoOrganizacao the ds mensagem padrao organizacao
	 */
	public void setDsMensagemPadraoOrganizacao(String dsMensagemPadraoOrganizacao) {
		this.dsMensagemPadraoOrganizacao = dsMensagemPadraoOrganizacao;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	

}
