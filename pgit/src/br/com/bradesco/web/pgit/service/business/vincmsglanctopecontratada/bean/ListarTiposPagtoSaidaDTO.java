package br.com.bradesco.web.pgit.service.business.vincmsglanctopecontratada.bean;

import java.util.List;

public class ListarTiposPagtoSaidaDTO {
	
    /** Atributo codMensagem. */
	private String codMensagem;
    
	/** Atributo mensagem. */
	private String mensagem;
	
	/** Atributo numeroLinhas. */
    private Integer numeroLinhas;
	
    /** Atributo ocorrencias. */
    private List<ListarTiposPagtoSaidaOcorrenciasDTO> ocorrencias;

	public String getCodMensagem() {
		return codMensagem;
	}

	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}

	public String getMensagem() {
		return mensagem;
	}

	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	public Integer getNumeroLinhas() {
		return numeroLinhas;
	}

	public void setNumeroLinhas(Integer numeroLinhas) {
		this.numeroLinhas = numeroLinhas;
	}

	public List<ListarTiposPagtoSaidaOcorrenciasDTO> getOcorrencias() {
		return ocorrencias;
	}

	public void setOcorrencias(List<ListarTiposPagtoSaidaOcorrenciasDTO> ocorrencias) {
		this.ocorrencias = ocorrencias;
	}    
}
