/*
 * Nome: br.com.bradesco.web.pgit.service.report.contrato.domain
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 04/02/2016
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.report.mantercontrato.contrato.data;


/**
 * Nome: DadosIdentificacao
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class DadosIdentificacao {

    /** Atributo razaoSocialNome. */
    private String razaoSocialNome = null;
    
    /** Atributo cnpjCpf. */
    private String cnpjCpf = null;
    
    /** Atributo endereco. */
    private String endereco = null;
    
    /** Atributo bairro. */
    private String bairro = null;
    
    /** Atributo cidade. */
    private String cidade = null;
    
    /** Atributo cep. */
    private String cep = null;
    
    /** Atributo uf. */
    private String uf = null;

    /**
     * Instancia um novo DadosIdentificacao
     *
     * @see
     */
    public DadosIdentificacao() {
        super();
    }

    /**
     * Nome: getRazaoSocialNome
     *
     * @return razaoSocialNome
     */
    public String getRazaoSocialNome() {
        return razaoSocialNome;
    }

    /**
     * Nome: setRazaoSocialNome
     *
     * @param razaoSocialNome
     */
    public void setRazaoSocialNome(String razaoSocialNome) {
        this.razaoSocialNome = razaoSocialNome;
    }

    /**
     * Nome: getCnpjCpf
     *
     * @return cnpjCpf
     */
    public String getCnpjCpf() {
        return cnpjCpf;
    }

    /**
     * Nome: setCnpjCpf
     *
     * @param cnpjCpf
     */
    public void setCnpjCpf(String cnpjCpf) {
        this.cnpjCpf = cnpjCpf;
    }

    /**
     * Nome: getEndereco
     *
     * @return endereco
     */
    public String getEndereco() {
        return endereco;
    }

    /**
     * Nome: setEndereco
     *
     * @param endereco
     */
    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    /**
     * Nome: getBairro
     *
     * @return bairro
     */
    public String getBairro() {
        return bairro;
    }

    /**
     * Nome: setBairro
     *
     * @param bairro
     */
    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    /**
     * Nome: getCidade
     *
     * @return cidade
     */
    public String getCidade() {
        return cidade;
    }

    /**
     * Nome: setCidade
     *
     * @param cidade
     */
    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    /**
     * Nome: getCep
     *
     * @return cep
     */
    public String getCep() {
        return cep;
    }

    /**
     * Nome: setCep
     *
     * @param cep
     */
    public void setCep(String cep) {
        this.cep = cep;
    }

    /**
     * Nome: getUf
     *
     * @return uf
     */
    public String getUf() {
        return uf;
    }

    /**
     * Nome: setUf
     *
     * @param uf
     */
    public void setUf(String uf) {
        this.uf = uf;
    }

}
