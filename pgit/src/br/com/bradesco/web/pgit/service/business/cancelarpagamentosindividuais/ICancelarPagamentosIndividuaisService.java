/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/interfaz.ftl,v $
 * $Id: interfaz.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: interfaz.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.cancelarpagamentosindividuais;

import java.util.List;

import br.com.bradesco.web.pgit.service.business.cancelarpagamentosindividuais.bean.CancelarPagtosIndividuaisEntradaDTO;
import br.com.bradesco.web.pgit.service.business.cancelarpagamentosindividuais.bean.CancelarPagtosIndividuaisSaidaDTO;
import br.com.bradesco.web.pgit.service.business.cancelarpagamentosindividuais.bean.ConsultarPagtosIndParaCancelamentoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.cancelarpagamentosindividuais.bean.ConsultarPagtosIndParaCancelamentoSaidaDTO;

/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Interface do adaptador: CancelarPagamentosIndividuais
 * </p>
 * 
 * @comment CODIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public interface ICancelarPagamentosIndividuaisService {

    /**
     * Consultar pagtos ind para cancelamento.
     *
     * @param entrada the entrada
     * @return the list< consultar pagtos ind para cancelamento saida dt o>
     */
    List<ConsultarPagtosIndParaCancelamentoSaidaDTO> consultarPagtosIndParaCancelamento (ConsultarPagtosIndParaCancelamentoEntradaDTO entrada);
    
    /**
     * Cancelar pagtos individuais.
     *
     * @param listaEntrada the lista entrada
     * @return the cancelar pagtos individuais saida dto
     */
    CancelarPagtosIndividuaisSaidaDTO cancelarPagtosIndividuais (List<CancelarPagtosIndividuaisEntradaDTO> listaEntrada);
}

