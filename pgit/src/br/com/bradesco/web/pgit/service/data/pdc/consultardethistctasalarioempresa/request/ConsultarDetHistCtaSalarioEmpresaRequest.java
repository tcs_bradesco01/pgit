/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultardethistctasalarioempresa.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ConsultarDetHistCtaSalarioEmpresaRequest.
 * 
 * @version $Revision$ $Date$
 */
public class ConsultarDetHistCtaSalarioEmpresaRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdPessoa
     */
    private long _cdPessoa = 0;

    /**
     * keeps track of state for field: _cdPessoa
     */
    private boolean _has_cdPessoa;

    /**
     * Field _cdTipoContrato
     */
    private int _cdTipoContrato = 0;

    /**
     * keeps track of state for field: _cdTipoContrato
     */
    private boolean _has_cdTipoContrato;

    /**
     * Field _nrSeqContrato
     */
    private long _nrSeqContrato = 0;

    /**
     * keeps track of state for field: _nrSeqContrato
     */
    private boolean _has_nrSeqContrato;

    /**
     * Field _cdPessoaVinc
     */
    private long _cdPessoaVinc = 0;

    /**
     * keeps track of state for field: _cdPessoaVinc
     */
    private boolean _has_cdPessoaVinc;

    /**
     * Field _cdTipoContratoVinc
     */
    private int _cdTipoContratoVinc = 0;

    /**
     * keeps track of state for field: _cdTipoContratoVinc
     */
    private boolean _has_cdTipoContratoVinc;

    /**
     * Field _nrSeqContratoVinc
     */
    private long _nrSeqContratoVinc = 0;

    /**
     * keeps track of state for field: _nrSeqContratoVinc
     */
    private boolean _has_nrSeqContratoVinc;

    /**
     * Field _cdTipoVincContrato
     */
    private int _cdTipoVincContrato = 0;

    /**
     * keeps track of state for field: _cdTipoVincContrato
     */
    private boolean _has_cdTipoVincContrato;

    /**
     * Field _hrInclusaoRegistroHist
     */
    private java.lang.String _hrInclusaoRegistroHist;


      //----------------/
     //- Constructors -/
    //----------------/

    public ConsultarDetHistCtaSalarioEmpresaRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultardethistctasalarioempresa.request.ConsultarDetHistCtaSalarioEmpresaRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdPessoa
     * 
     */
    public void deleteCdPessoa()
    {
        this._has_cdPessoa= false;
    } //-- void deleteCdPessoa() 

    /**
     * Method deleteCdPessoaVinc
     * 
     */
    public void deleteCdPessoaVinc()
    {
        this._has_cdPessoaVinc= false;
    } //-- void deleteCdPessoaVinc() 

    /**
     * Method deleteCdTipoContrato
     * 
     */
    public void deleteCdTipoContrato()
    {
        this._has_cdTipoContrato= false;
    } //-- void deleteCdTipoContrato() 

    /**
     * Method deleteCdTipoContratoVinc
     * 
     */
    public void deleteCdTipoContratoVinc()
    {
        this._has_cdTipoContratoVinc= false;
    } //-- void deleteCdTipoContratoVinc() 

    /**
     * Method deleteCdTipoVincContrato
     * 
     */
    public void deleteCdTipoVincContrato()
    {
        this._has_cdTipoVincContrato= false;
    } //-- void deleteCdTipoVincContrato() 

    /**
     * Method deleteNrSeqContrato
     * 
     */
    public void deleteNrSeqContrato()
    {
        this._has_nrSeqContrato= false;
    } //-- void deleteNrSeqContrato() 

    /**
     * Method deleteNrSeqContratoVinc
     * 
     */
    public void deleteNrSeqContratoVinc()
    {
        this._has_nrSeqContratoVinc= false;
    } //-- void deleteNrSeqContratoVinc() 

    /**
     * Returns the value of field 'cdPessoa'.
     * 
     * @return long
     * @return the value of field 'cdPessoa'.
     */
    public long getCdPessoa()
    {
        return this._cdPessoa;
    } //-- long getCdPessoa() 

    /**
     * Returns the value of field 'cdPessoaVinc'.
     * 
     * @return long
     * @return the value of field 'cdPessoaVinc'.
     */
    public long getCdPessoaVinc()
    {
        return this._cdPessoaVinc;
    } //-- long getCdPessoaVinc() 

    /**
     * Returns the value of field 'cdTipoContrato'.
     * 
     * @return int
     * @return the value of field 'cdTipoContrato'.
     */
    public int getCdTipoContrato()
    {
        return this._cdTipoContrato;
    } //-- int getCdTipoContrato() 

    /**
     * Returns the value of field 'cdTipoContratoVinc'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoVinc'.
     */
    public int getCdTipoContratoVinc()
    {
        return this._cdTipoContratoVinc;
    } //-- int getCdTipoContratoVinc() 

    /**
     * Returns the value of field 'cdTipoVincContrato'.
     * 
     * @return int
     * @return the value of field 'cdTipoVincContrato'.
     */
    public int getCdTipoVincContrato()
    {
        return this._cdTipoVincContrato;
    } //-- int getCdTipoVincContrato() 

    /**
     * Returns the value of field 'hrInclusaoRegistroHist'.
     * 
     * @return String
     * @return the value of field 'hrInclusaoRegistroHist'.
     */
    public java.lang.String getHrInclusaoRegistroHist()
    {
        return this._hrInclusaoRegistroHist;
    } //-- java.lang.String getHrInclusaoRegistroHist() 

    /**
     * Returns the value of field 'nrSeqContrato'.
     * 
     * @return long
     * @return the value of field 'nrSeqContrato'.
     */
    public long getNrSeqContrato()
    {
        return this._nrSeqContrato;
    } //-- long getNrSeqContrato() 

    /**
     * Returns the value of field 'nrSeqContratoVinc'.
     * 
     * @return long
     * @return the value of field 'nrSeqContratoVinc'.
     */
    public long getNrSeqContratoVinc()
    {
        return this._nrSeqContratoVinc;
    } //-- long getNrSeqContratoVinc() 

    /**
     * Method hasCdPessoa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoa()
    {
        return this._has_cdPessoa;
    } //-- boolean hasCdPessoa() 

    /**
     * Method hasCdPessoaVinc
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaVinc()
    {
        return this._has_cdPessoaVinc;
    } //-- boolean hasCdPessoaVinc() 

    /**
     * Method hasCdTipoContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContrato()
    {
        return this._has_cdTipoContrato;
    } //-- boolean hasCdTipoContrato() 

    /**
     * Method hasCdTipoContratoVinc
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoVinc()
    {
        return this._has_cdTipoContratoVinc;
    } //-- boolean hasCdTipoContratoVinc() 

    /**
     * Method hasCdTipoVincContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoVincContrato()
    {
        return this._has_cdTipoVincContrato;
    } //-- boolean hasCdTipoVincContrato() 

    /**
     * Method hasNrSeqContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSeqContrato()
    {
        return this._has_nrSeqContrato;
    } //-- boolean hasNrSeqContrato() 

    /**
     * Method hasNrSeqContratoVinc
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSeqContratoVinc()
    {
        return this._has_nrSeqContratoVinc;
    } //-- boolean hasNrSeqContratoVinc() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdPessoa'.
     * 
     * @param cdPessoa the value of field 'cdPessoa'.
     */
    public void setCdPessoa(long cdPessoa)
    {
        this._cdPessoa = cdPessoa;
        this._has_cdPessoa = true;
    } //-- void setCdPessoa(long) 

    /**
     * Sets the value of field 'cdPessoaVinc'.
     * 
     * @param cdPessoaVinc the value of field 'cdPessoaVinc'.
     */
    public void setCdPessoaVinc(long cdPessoaVinc)
    {
        this._cdPessoaVinc = cdPessoaVinc;
        this._has_cdPessoaVinc = true;
    } //-- void setCdPessoaVinc(long) 

    /**
     * Sets the value of field 'cdTipoContrato'.
     * 
     * @param cdTipoContrato the value of field 'cdTipoContrato'.
     */
    public void setCdTipoContrato(int cdTipoContrato)
    {
        this._cdTipoContrato = cdTipoContrato;
        this._has_cdTipoContrato = true;
    } //-- void setCdTipoContrato(int) 

    /**
     * Sets the value of field 'cdTipoContratoVinc'.
     * 
     * @param cdTipoContratoVinc the value of field
     * 'cdTipoContratoVinc'.
     */
    public void setCdTipoContratoVinc(int cdTipoContratoVinc)
    {
        this._cdTipoContratoVinc = cdTipoContratoVinc;
        this._has_cdTipoContratoVinc = true;
    } //-- void setCdTipoContratoVinc(int) 

    /**
     * Sets the value of field 'cdTipoVincContrato'.
     * 
     * @param cdTipoVincContrato the value of field
     * 'cdTipoVincContrato'.
     */
    public void setCdTipoVincContrato(int cdTipoVincContrato)
    {
        this._cdTipoVincContrato = cdTipoVincContrato;
        this._has_cdTipoVincContrato = true;
    } //-- void setCdTipoVincContrato(int) 

    /**
     * Sets the value of field 'hrInclusaoRegistroHist'.
     * 
     * @param hrInclusaoRegistroHist the value of field
     * 'hrInclusaoRegistroHist'.
     */
    public void setHrInclusaoRegistroHist(java.lang.String hrInclusaoRegistroHist)
    {
        this._hrInclusaoRegistroHist = hrInclusaoRegistroHist;
    } //-- void setHrInclusaoRegistroHist(java.lang.String) 

    /**
     * Sets the value of field 'nrSeqContrato'.
     * 
     * @param nrSeqContrato the value of field 'nrSeqContrato'.
     */
    public void setNrSeqContrato(long nrSeqContrato)
    {
        this._nrSeqContrato = nrSeqContrato;
        this._has_nrSeqContrato = true;
    } //-- void setNrSeqContrato(long) 

    /**
     * Sets the value of field 'nrSeqContratoVinc'.
     * 
     * @param nrSeqContratoVinc the value of field
     * 'nrSeqContratoVinc'.
     */
    public void setNrSeqContratoVinc(long nrSeqContratoVinc)
    {
        this._nrSeqContratoVinc = nrSeqContratoVinc;
        this._has_nrSeqContratoVinc = true;
    } //-- void setNrSeqContratoVinc(long) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ConsultarDetHistCtaSalarioEmpresaRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultardethistctasalarioempresa.request.ConsultarDetHistCtaSalarioEmpresaRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultardethistctasalarioempresa.request.ConsultarDetHistCtaSalarioEmpresaRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultardethistctasalarioempresa.request.ConsultarDetHistCtaSalarioEmpresaRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultardethistctasalarioempresa.request.ConsultarDetHistCtaSalarioEmpresaRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
