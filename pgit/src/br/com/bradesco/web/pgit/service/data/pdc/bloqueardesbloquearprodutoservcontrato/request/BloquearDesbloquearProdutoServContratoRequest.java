/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.bloqueardesbloquearprodutoservcontrato.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.util.Enumeration;
import java.util.Vector;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class BloquearDesbloquearProdutoServContratoRequest.
 * 
 * @version $Revision$ $Date$
 */
public class BloquearDesbloquearProdutoServContratoRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdPessoaJuridicaContrato
     */
    private long _cdPessoaJuridicaContrato = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridicaContrato
     */
    private boolean _has_cdPessoaJuridicaContrato;

    /**
     * Field _cdTipoContratoNegocio
     */
    private int _cdTipoContratoNegocio = 0;

    /**
     * keeps track of state for field: _cdTipoContratoNegocio
     */
    private boolean _has_cdTipoContratoNegocio;

    /**
     * Field _nrSequenciaContratoNegocio
     */
    private long _nrSequenciaContratoNegocio = 0;

    /**
     * keeps track of state for field: _nrSequenciaContratoNegocio
     */
    private boolean _has_nrSequenciaContratoNegocio;

    /**
     * Field _cdProdutoServicoOperacao
     */
    private int _cdProdutoServicoOperacao = 0;

    /**
     * keeps track of state for field: _cdProdutoServicoOperacao
     */
    private boolean _has_cdProdutoServicoOperacao;

    /**
     * Field _ocorrenciasList
     */
    private java.util.Vector _ocorrenciasList;

    /**
     * Field _cdSituacaoServicoRelacionado
     */
    private int _cdSituacaoServicoRelacionado = 0;

    /**
     * keeps track of state for field: _cdSituacaoServicoRelacionado
     */
    private boolean _has_cdSituacaoServicoRelacionado;

    /**
     * Field _cdMotivoServicoRelacionado
     */
    private int _cdMotivoServicoRelacionado = 0;

    /**
     * keeps track of state for field: _cdMotivoServicoRelacionado
     */
    private boolean _has_cdMotivoServicoRelacionado;

    /**
     * Field _cdParametro
     */
    private int _cdParametro = 0;

    /**
     * keeps track of state for field: _cdParametro
     */
    private boolean _has_cdParametro;


      //----------------/
     //- Constructors -/
    //----------------/

    public BloquearDesbloquearProdutoServContratoRequest() 
     {
        super();
        _ocorrenciasList = new Vector();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.bloqueardesbloquearprodutoservcontrato.request.BloquearDesbloquearProdutoServContratoRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method addOcorrencias
     * 
     * 
     * 
     * @param vOcorrencias
     */
    public void addOcorrencias(br.com.bradesco.web.pgit.service.data.pdc.bloqueardesbloquearprodutoservcontrato.request.Ocorrencias vOcorrencias)
        throws java.lang.IndexOutOfBoundsException
    {
        _ocorrenciasList.addElement(vOcorrencias);
    } //-- void addOcorrencias(br.com.bradesco.web.pgit.service.data.pdc.bloqueardesbloquearprodutoservcontrato.request.Ocorrencias) 

    /**
     * Method addOcorrencias
     * 
     * 
     * 
     * @param index
     * @param vOcorrencias
     */
    public void addOcorrencias(int index, br.com.bradesco.web.pgit.service.data.pdc.bloqueardesbloquearprodutoservcontrato.request.Ocorrencias vOcorrencias)
        throws java.lang.IndexOutOfBoundsException
    {
        _ocorrenciasList.insertElementAt(vOcorrencias, index);
    } //-- void addOcorrencias(int, br.com.bradesco.web.pgit.service.data.pdc.bloqueardesbloquearprodutoservcontrato.request.Ocorrencias) 

    /**
     * Method deleteCdMotivoServicoRelacionado
     * 
     */
    public void deleteCdMotivoServicoRelacionado()
    {
        this._has_cdMotivoServicoRelacionado= false;
    } //-- void deleteCdMotivoServicoRelacionado() 

    /**
     * Method deleteCdParametro
     * 
     */
    public void deleteCdParametro()
    {
        this._has_cdParametro= false;
    } //-- void deleteCdParametro() 

    /**
     * Method deleteCdPessoaJuridicaContrato
     * 
     */
    public void deleteCdPessoaJuridicaContrato()
    {
        this._has_cdPessoaJuridicaContrato= false;
    } //-- void deleteCdPessoaJuridicaContrato() 

    /**
     * Method deleteCdProdutoServicoOperacao
     * 
     */
    public void deleteCdProdutoServicoOperacao()
    {
        this._has_cdProdutoServicoOperacao= false;
    } //-- void deleteCdProdutoServicoOperacao() 

    /**
     * Method deleteCdSituacaoServicoRelacionado
     * 
     */
    public void deleteCdSituacaoServicoRelacionado()
    {
        this._has_cdSituacaoServicoRelacionado= false;
    } //-- void deleteCdSituacaoServicoRelacionado() 

    /**
     * Method deleteCdTipoContratoNegocio
     * 
     */
    public void deleteCdTipoContratoNegocio()
    {
        this._has_cdTipoContratoNegocio= false;
    } //-- void deleteCdTipoContratoNegocio() 

    /**
     * Method deleteNrSequenciaContratoNegocio
     * 
     */
    public void deleteNrSequenciaContratoNegocio()
    {
        this._has_nrSequenciaContratoNegocio= false;
    } //-- void deleteNrSequenciaContratoNegocio() 

    /**
     * Method enumerateOcorrencias
     * 
     * 
     * 
     * @return Enumeration
     */
    public java.util.Enumeration enumerateOcorrencias()
    {
        return _ocorrenciasList.elements();
    } //-- java.util.Enumeration enumerateOcorrencias() 

    /**
     * Returns the value of field 'cdMotivoServicoRelacionado'.
     * 
     * @return int
     * @return the value of field 'cdMotivoServicoRelacionado'.
     */
    public int getCdMotivoServicoRelacionado()
    {
        return this._cdMotivoServicoRelacionado;
    } //-- int getCdMotivoServicoRelacionado() 

    /**
     * Returns the value of field 'cdParametro'.
     * 
     * @return int
     * @return the value of field 'cdParametro'.
     */
    public int getCdParametro()
    {
        return this._cdParametro;
    } //-- int getCdParametro() 

    /**
     * Returns the value of field 'cdPessoaJuridicaContrato'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridicaContrato'.
     */
    public long getCdPessoaJuridicaContrato()
    {
        return this._cdPessoaJuridicaContrato;
    } //-- long getCdPessoaJuridicaContrato() 

    /**
     * Returns the value of field 'cdProdutoServicoOperacao'.
     * 
     * @return int
     * @return the value of field 'cdProdutoServicoOperacao'.
     */
    public int getCdProdutoServicoOperacao()
    {
        return this._cdProdutoServicoOperacao;
    } //-- int getCdProdutoServicoOperacao() 

    /**
     * Returns the value of field 'cdSituacaoServicoRelacionado'.
     * 
     * @return int
     * @return the value of field 'cdSituacaoServicoRelacionado'.
     */
    public int getCdSituacaoServicoRelacionado()
    {
        return this._cdSituacaoServicoRelacionado;
    } //-- int getCdSituacaoServicoRelacionado() 

    /**
     * Returns the value of field 'cdTipoContratoNegocio'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoNegocio'.
     */
    public int getCdTipoContratoNegocio()
    {
        return this._cdTipoContratoNegocio;
    } //-- int getCdTipoContratoNegocio() 

    /**
     * Returns the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @return long
     * @return the value of field 'nrSequenciaContratoNegocio'.
     */
    public long getNrSequenciaContratoNegocio()
    {
        return this._nrSequenciaContratoNegocio;
    } //-- long getNrSequenciaContratoNegocio() 

    /**
     * Method getOcorrencias
     * 
     * 
     * 
     * @param index
     * @return Ocorrencias
     */
    public br.com.bradesco.web.pgit.service.data.pdc.bloqueardesbloquearprodutoservcontrato.request.Ocorrencias getOcorrencias(int index)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index >= _ocorrenciasList.size())) {
            throw new IndexOutOfBoundsException("getOcorrencias: Index value '"+index+"' not in range [0.."+(_ocorrenciasList.size() - 1) + "]");
        }
        
        return (br.com.bradesco.web.pgit.service.data.pdc.bloqueardesbloquearprodutoservcontrato.request.Ocorrencias) _ocorrenciasList.elementAt(index);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.bloqueardesbloquearprodutoservcontrato.request.Ocorrencias getOcorrencias(int) 

    /**
     * Method getOcorrencias
     * 
     * 
     * 
     * @return Ocorrencias
     */
    public br.com.bradesco.web.pgit.service.data.pdc.bloqueardesbloquearprodutoservcontrato.request.Ocorrencias[] getOcorrencias()
    {
        int size = _ocorrenciasList.size();
        br.com.bradesco.web.pgit.service.data.pdc.bloqueardesbloquearprodutoservcontrato.request.Ocorrencias[] mArray = new br.com.bradesco.web.pgit.service.data.pdc.bloqueardesbloquearprodutoservcontrato.request.Ocorrencias[size];
        for (int index = 0; index < size; index++) {
            mArray[index] = (br.com.bradesco.web.pgit.service.data.pdc.bloqueardesbloquearprodutoservcontrato.request.Ocorrencias) _ocorrenciasList.elementAt(index);
        }
        return mArray;
    } //-- br.com.bradesco.web.pgit.service.data.pdc.bloqueardesbloquearprodutoservcontrato.request.Ocorrencias[] getOcorrencias() 

    /**
     * Method getOcorrenciasCount
     * 
     * 
     * 
     * @return int
     */
    public int getOcorrenciasCount()
    {
        return _ocorrenciasList.size();
    } //-- int getOcorrenciasCount() 

    /**
     * Method hasCdMotivoServicoRelacionado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMotivoServicoRelacionado()
    {
        return this._has_cdMotivoServicoRelacionado;
    } //-- boolean hasCdMotivoServicoRelacionado() 

    /**
     * Method hasCdParametro
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdParametro()
    {
        return this._has_cdParametro;
    } //-- boolean hasCdParametro() 

    /**
     * Method hasCdPessoaJuridicaContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridicaContrato()
    {
        return this._has_cdPessoaJuridicaContrato;
    } //-- boolean hasCdPessoaJuridicaContrato() 

    /**
     * Method hasCdProdutoServicoOperacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdutoServicoOperacao()
    {
        return this._has_cdProdutoServicoOperacao;
    } //-- boolean hasCdProdutoServicoOperacao() 

    /**
     * Method hasCdSituacaoServicoRelacionado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSituacaoServicoRelacionado()
    {
        return this._has_cdSituacaoServicoRelacionado;
    } //-- boolean hasCdSituacaoServicoRelacionado() 

    /**
     * Method hasCdTipoContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoNegocio()
    {
        return this._has_cdTipoContratoNegocio;
    } //-- boolean hasCdTipoContratoNegocio() 

    /**
     * Method hasNrSequenciaContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaContratoNegocio()
    {
        return this._has_nrSequenciaContratoNegocio;
    } //-- boolean hasNrSequenciaContratoNegocio() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Method removeAllOcorrencias
     * 
     */
    public void removeAllOcorrencias()
    {
        _ocorrenciasList.removeAllElements();
    } //-- void removeAllOcorrencias() 

    /**
     * Method removeOcorrencias
     * 
     * 
     * 
     * @param index
     * @return Ocorrencias
     */
    public br.com.bradesco.web.pgit.service.data.pdc.bloqueardesbloquearprodutoservcontrato.request.Ocorrencias removeOcorrencias(int index)
    {
        java.lang.Object obj = _ocorrenciasList.elementAt(index);
        _ocorrenciasList.removeElementAt(index);
        return (br.com.bradesco.web.pgit.service.data.pdc.bloqueardesbloquearprodutoservcontrato.request.Ocorrencias) obj;
    } //-- br.com.bradesco.web.pgit.service.data.pdc.bloqueardesbloquearprodutoservcontrato.request.Ocorrencias removeOcorrencias(int) 

    /**
     * Sets the value of field 'cdMotivoServicoRelacionado'.
     * 
     * @param cdMotivoServicoRelacionado the value of field
     * 'cdMotivoServicoRelacionado'.
     */
    public void setCdMotivoServicoRelacionado(int cdMotivoServicoRelacionado)
    {
        this._cdMotivoServicoRelacionado = cdMotivoServicoRelacionado;
        this._has_cdMotivoServicoRelacionado = true;
    } //-- void setCdMotivoServicoRelacionado(int) 

    /**
     * Sets the value of field 'cdParametro'.
     * 
     * @param cdParametro the value of field 'cdParametro'.
     */
    public void setCdParametro(int cdParametro)
    {
        this._cdParametro = cdParametro;
        this._has_cdParametro = true;
    } //-- void setCdParametro(int) 

    /**
     * Sets the value of field 'cdPessoaJuridicaContrato'.
     * 
     * @param cdPessoaJuridicaContrato the value of field
     * 'cdPessoaJuridicaContrato'.
     */
    public void setCdPessoaJuridicaContrato(long cdPessoaJuridicaContrato)
    {
        this._cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
        this._has_cdPessoaJuridicaContrato = true;
    } //-- void setCdPessoaJuridicaContrato(long) 

    /**
     * Sets the value of field 'cdProdutoServicoOperacao'.
     * 
     * @param cdProdutoServicoOperacao the value of field
     * 'cdProdutoServicoOperacao'.
     */
    public void setCdProdutoServicoOperacao(int cdProdutoServicoOperacao)
    {
        this._cdProdutoServicoOperacao = cdProdutoServicoOperacao;
        this._has_cdProdutoServicoOperacao = true;
    } //-- void setCdProdutoServicoOperacao(int) 

    /**
     * Sets the value of field 'cdSituacaoServicoRelacionado'.
     * 
     * @param cdSituacaoServicoRelacionado the value of field
     * 'cdSituacaoServicoRelacionado'.
     */
    public void setCdSituacaoServicoRelacionado(int cdSituacaoServicoRelacionado)
    {
        this._cdSituacaoServicoRelacionado = cdSituacaoServicoRelacionado;
        this._has_cdSituacaoServicoRelacionado = true;
    } //-- void setCdSituacaoServicoRelacionado(int) 

    /**
     * Sets the value of field 'cdTipoContratoNegocio'.
     * 
     * @param cdTipoContratoNegocio the value of field
     * 'cdTipoContratoNegocio'.
     */
    public void setCdTipoContratoNegocio(int cdTipoContratoNegocio)
    {
        this._cdTipoContratoNegocio = cdTipoContratoNegocio;
        this._has_cdTipoContratoNegocio = true;
    } //-- void setCdTipoContratoNegocio(int) 

    /**
     * Sets the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @param nrSequenciaContratoNegocio the value of field
     * 'nrSequenciaContratoNegocio'.
     */
    public void setNrSequenciaContratoNegocio(long nrSequenciaContratoNegocio)
    {
        this._nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
        this._has_nrSequenciaContratoNegocio = true;
    } //-- void setNrSequenciaContratoNegocio(long) 

    /**
     * Method setOcorrencias
     * 
     * 
     * 
     * @param index
     * @param vOcorrencias
     */
    public void setOcorrencias(int index, br.com.bradesco.web.pgit.service.data.pdc.bloqueardesbloquearprodutoservcontrato.request.Ocorrencias vOcorrencias)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index >= _ocorrenciasList.size())) {
            throw new IndexOutOfBoundsException("setOcorrencias: Index value '"+index+"' not in range [0.." + (_ocorrenciasList.size() - 1) + "]");
        }
        _ocorrenciasList.setElementAt(vOcorrencias, index);
    } //-- void setOcorrencias(int, br.com.bradesco.web.pgit.service.data.pdc.bloqueardesbloquearprodutoservcontrato.request.Ocorrencias) 

    /**
     * Method setOcorrencias
     * 
     * 
     * 
     * @param ocorrenciasArray
     */
    public void setOcorrencias(br.com.bradesco.web.pgit.service.data.pdc.bloqueardesbloquearprodutoservcontrato.request.Ocorrencias[] ocorrenciasArray)
    {
        //-- copy array
        _ocorrenciasList.removeAllElements();
        for (int i = 0; i < ocorrenciasArray.length; i++) {
            _ocorrenciasList.addElement(ocorrenciasArray[i]);
        }
    } //-- void setOcorrencias(br.com.bradesco.web.pgit.service.data.pdc.bloqueardesbloquearprodutoservcontrato.request.Ocorrencias) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return BloquearDesbloquearProdutoServContratoRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.bloqueardesbloquearprodutoservcontrato.request.BloquearDesbloquearProdutoServContratoRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.bloqueardesbloquearprodutoservcontrato.request.BloquearDesbloquearProdutoServContratoRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.bloqueardesbloquearprodutoservcontrato.request.BloquearDesbloquearProdutoServContratoRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.bloqueardesbloquearprodutoservcontrato.request.BloquearDesbloquearProdutoServContratoRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
