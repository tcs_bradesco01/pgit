/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.imprimircontrato.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.math.BigDecimal;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ImprimirContratoResponse.
 * 
 * @version $Revision$ $Date$
 */
public class ImprimirContratoResponse implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _codMensagem
     */
    private java.lang.String _codMensagem;

    /**
     * Field _mensagem
     */
    private java.lang.String _mensagem;

    /**
     * Field _cdClubPessoa
     */
    private long _cdClubPessoa = 0;

    /**
     * keeps track of state for field: _cdClubPessoa
     */
    private boolean _has_cdClubPessoa;

    /**
     * Field _cdCpfCnpjParticipante
     */
    private java.lang.String _cdCpfCnpjParticipante;

    /**
     * Field _nmParticipante
     */
    private java.lang.String _nmParticipante;

    /**
     * Field _cdPessoaEmpresa
     */
    private long _cdPessoaEmpresa = 0;

    /**
     * keeps track of state for field: _cdPessoaEmpresa
     */
    private boolean _has_cdPessoaEmpresa;

    /**
     * Field _cdCpfCnpjEmpresa
     */
    private java.lang.String _cdCpfCnpjEmpresa;

    /**
     * Field _nmEmpresa
     */
    private java.lang.String _nmEmpresa;

    /**
     * Field _cdAgenciaGestorContrato
     */
    private int _cdAgenciaGestorContrato = 0;

    /**
     * keeps track of state for field: _cdAgenciaGestorContrato
     */
    private boolean _has_cdAgenciaGestorContrato;

    /**
     * Field _cdDigitoAgenciaGestor
     */
    private int _cdDigitoAgenciaGestor = 0;

    /**
     * keeps track of state for field: _cdDigitoAgenciaGestor
     */
    private boolean _has_cdDigitoAgenciaGestor;

    /**
     * Field _dsAgenciaGestor
     */
    private java.lang.String _dsAgenciaGestor;

    /**
     * Field _cdContaAgenciaGestor
     */
    private long _cdContaAgenciaGestor = 0;

    /**
     * keeps track of state for field: _cdContaAgenciaGestor
     */
    private boolean _has_cdContaAgenciaGestor;

    /**
     * Field _cdDigitoContaGestor
     */
    private java.lang.String _cdDigitoContaGestor;

    /**
     * Field _cdCepAgencia
     */
    private int _cdCepAgencia = 0;

    /**
     * keeps track of state for field: _cdCepAgencia
     */
    private boolean _has_cdCepAgencia;

    /**
     * Field _cdComplementoCepAgencia
     */
    private int _cdComplementoCepAgencia = 0;

    /**
     * keeps track of state for field: _cdComplementoCepAgencia
     */
    private boolean _has_cdComplementoCepAgencia;

    /**
     * Field _dsLogradouroAgencia
     */
    private java.lang.String _dsLogradouroAgencia;

    /**
     * Field _nrLogradouroAgencia
     */
    private java.lang.String _nrLogradouroAgencia;

    /**
     * Field _dsComplementoAgencia
     */
    private java.lang.String _dsComplementoAgencia;

    /**
     * Field _dsBairroAgencia
     */
    private java.lang.String _dsBairroAgencia;

    /**
     * Field _dsCidadeAgencia
     */
    private java.lang.String _dsCidadeAgencia;

    /**
     * Field _cdSiglaUfAgencia
     */
    private java.lang.String _cdSiglaUfAgencia;

    /**
     * Field _cdCep
     */
    private int _cdCep = 0;

    /**
     * keeps track of state for field: _cdCep
     */
    private boolean _has_cdCep;

    /**
     * Field _cdCepComplemento
     */
    private int _cdCepComplemento = 0;

    /**
     * keeps track of state for field: _cdCepComplemento
     */
    private boolean _has_cdCepComplemento;

    /**
     * Field _dsLogradouroPessoa
     */
    private java.lang.String _dsLogradouroPessoa;

    /**
     * Field _dsLogradouroNumero
     */
    private java.lang.String _dsLogradouroNumero;

    /**
     * Field _dsComplementoEndereco
     */
    private java.lang.String _dsComplementoEndereco;

    /**
     * Field _dsBairroEndereco
     */
    private java.lang.String _dsBairroEndereco;

    /**
     * Field _dsCidadeEndereco
     */
    private java.lang.String _dsCidadeEndereco;

    /**
     * Field _cdSiglaUf
     */
    private java.lang.String _cdSiglaUf;

    /**
     * Field _qtDiaInatividade
     */
    private int _qtDiaInatividade = 0;

    /**
     * keeps track of state for field: _qtDiaInatividade
     */
    private boolean _has_qtDiaInatividade;

    /**
     * Field _dsDiaInatividade
     */
    private java.lang.String _dsDiaInatividade;

    /**
     * Field _cdImpressaoFolha
     */
    private java.lang.String _cdImpressaoFolha;

    /**
     * Field _vlTarifaFolha
     */
    private java.math.BigDecimal _vlTarifaFolha = new java.math.BigDecimal("0");

    /**
     * Field _dsTarifa
     */
    private java.lang.String _dsTarifa;

    /**
     * Field _qtDiaAviso
     */
    private int _qtDiaAviso = 0;

    /**
     * keeps track of state for field: _qtDiaAviso
     */
    private boolean _has_qtDiaAviso;

    /**
     * Field _dsDiaAviso
     */
    private java.lang.String _dsDiaAviso;

    /**
     * Field _dsImpressaoFornecedor
     */
    private java.lang.String _dsImpressaoFornecedor;

    /**
     * Field _dsCreditoConta
     */
    private java.lang.String _dsCreditoConta;

    /**
     * Field _dsChequeOp
     */
    private java.lang.String _dsChequeOp;

    /**
     * Field _dsDoc
     */
    private java.lang.String _dsDoc;

    /**
     * Field _dsTed
     */
    private java.lang.String _dsTed;

    /**
     * Field _dsTituloBradesco
     */
    private java.lang.String _dsTituloBradesco;

    /**
     * Field _dsTituloOutros
     */
    private java.lang.String _dsTituloOutros;

    /**
     * Field _dsImpressaoPtrbLine
     */
    private java.lang.String _dsImpressaoPtrbLine;

    /**
     * Field _dsImpressaoPtrbEmpresa
     */
    private java.lang.String _dsImpressaoPtrbEmpresa;

    /**
     * Field _dsImpressaoPtrbTributo
     */
    private java.lang.String _dsImpressaoPtrbTributo;

    /**
     * Field _dsImpressaoComprovante
     */
    private java.lang.String _dsImpressaoComprovante;

    /**
     * Field _qtMesComprovante
     */
    private int _qtMesComprovante = 0;

    /**
     * keeps track of state for field: _qtMesComprovante
     */
    private boolean _has_qtMesComprovante;

    /**
     * Field _dsTipoRejeicao
     */
    private java.lang.String _dsTipoRejeicao;


      //----------------/
     //- Constructors -/
    //----------------/

    public ImprimirContratoResponse() 
     {
        super();
        setVlTarifaFolha(new java.math.BigDecimal("0"));
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimircontrato.response.ImprimirContratoResponse()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdAgenciaGestorContrato
     * 
     */
    public void deleteCdAgenciaGestorContrato()
    {
        this._has_cdAgenciaGestorContrato= false;
    } //-- void deleteCdAgenciaGestorContrato() 

    /**
     * Method deleteCdCep
     * 
     */
    public void deleteCdCep()
    {
        this._has_cdCep= false;
    } //-- void deleteCdCep() 

    /**
     * Method deleteCdCepAgencia
     * 
     */
    public void deleteCdCepAgencia()
    {
        this._has_cdCepAgencia= false;
    } //-- void deleteCdCepAgencia() 

    /**
     * Method deleteCdCepComplemento
     * 
     */
    public void deleteCdCepComplemento()
    {
        this._has_cdCepComplemento= false;
    } //-- void deleteCdCepComplemento() 

    /**
     * Method deleteCdClubPessoa
     * 
     */
    public void deleteCdClubPessoa()
    {
        this._has_cdClubPessoa= false;
    } //-- void deleteCdClubPessoa() 

    /**
     * Method deleteCdComplementoCepAgencia
     * 
     */
    public void deleteCdComplementoCepAgencia()
    {
        this._has_cdComplementoCepAgencia= false;
    } //-- void deleteCdComplementoCepAgencia() 

    /**
     * Method deleteCdContaAgenciaGestor
     * 
     */
    public void deleteCdContaAgenciaGestor()
    {
        this._has_cdContaAgenciaGestor= false;
    } //-- void deleteCdContaAgenciaGestor() 

    /**
     * Method deleteCdDigitoAgenciaGestor
     * 
     */
    public void deleteCdDigitoAgenciaGestor()
    {
        this._has_cdDigitoAgenciaGestor= false;
    } //-- void deleteCdDigitoAgenciaGestor() 

    /**
     * Method deleteCdPessoaEmpresa
     * 
     */
    public void deleteCdPessoaEmpresa()
    {
        this._has_cdPessoaEmpresa= false;
    } //-- void deleteCdPessoaEmpresa() 

    /**
     * Method deleteQtDiaAviso
     * 
     */
    public void deleteQtDiaAviso()
    {
        this._has_qtDiaAviso= false;
    } //-- void deleteQtDiaAviso() 

    /**
     * Method deleteQtDiaInatividade
     * 
     */
    public void deleteQtDiaInatividade()
    {
        this._has_qtDiaInatividade= false;
    } //-- void deleteQtDiaInatividade() 

    /**
     * Method deleteQtMesComprovante
     * 
     */
    public void deleteQtMesComprovante()
    {
        this._has_qtMesComprovante= false;
    } //-- void deleteQtMesComprovante() 

    /**
     * Returns the value of field 'cdAgenciaGestorContrato'.
     * 
     * @return int
     * @return the value of field 'cdAgenciaGestorContrato'.
     */
    public int getCdAgenciaGestorContrato()
    {
        return this._cdAgenciaGestorContrato;
    } //-- int getCdAgenciaGestorContrato() 

    /**
     * Returns the value of field 'cdCep'.
     * 
     * @return int
     * @return the value of field 'cdCep'.
     */
    public int getCdCep()
    {
        return this._cdCep;
    } //-- int getCdCep() 

    /**
     * Returns the value of field 'cdCepAgencia'.
     * 
     * @return int
     * @return the value of field 'cdCepAgencia'.
     */
    public int getCdCepAgencia()
    {
        return this._cdCepAgencia;
    } //-- int getCdCepAgencia() 

    /**
     * Returns the value of field 'cdCepComplemento'.
     * 
     * @return int
     * @return the value of field 'cdCepComplemento'.
     */
    public int getCdCepComplemento()
    {
        return this._cdCepComplemento;
    } //-- int getCdCepComplemento() 

    /**
     * Returns the value of field 'cdClubPessoa'.
     * 
     * @return long
     * @return the value of field 'cdClubPessoa'.
     */
    public long getCdClubPessoa()
    {
        return this._cdClubPessoa;
    } //-- long getCdClubPessoa() 

    /**
     * Returns the value of field 'cdComplementoCepAgencia'.
     * 
     * @return int
     * @return the value of field 'cdComplementoCepAgencia'.
     */
    public int getCdComplementoCepAgencia()
    {
        return this._cdComplementoCepAgencia;
    } //-- int getCdComplementoCepAgencia() 

    /**
     * Returns the value of field 'cdContaAgenciaGestor'.
     * 
     * @return long
     * @return the value of field 'cdContaAgenciaGestor'.
     */
    public long getCdContaAgenciaGestor()
    {
        return this._cdContaAgenciaGestor;
    } //-- long getCdContaAgenciaGestor() 

    /**
     * Returns the value of field 'cdCpfCnpjEmpresa'.
     * 
     * @return String
     * @return the value of field 'cdCpfCnpjEmpresa'.
     */
    public java.lang.String getCdCpfCnpjEmpresa()
    {
        return this._cdCpfCnpjEmpresa;
    } //-- java.lang.String getCdCpfCnpjEmpresa() 

    /**
     * Returns the value of field 'cdCpfCnpjParticipante'.
     * 
     * @return String
     * @return the value of field 'cdCpfCnpjParticipante'.
     */
    public java.lang.String getCdCpfCnpjParticipante()
    {
        return this._cdCpfCnpjParticipante;
    } //-- java.lang.String getCdCpfCnpjParticipante() 

    /**
     * Returns the value of field 'cdDigitoAgenciaGestor'.
     * 
     * @return int
     * @return the value of field 'cdDigitoAgenciaGestor'.
     */
    public int getCdDigitoAgenciaGestor()
    {
        return this._cdDigitoAgenciaGestor;
    } //-- int getCdDigitoAgenciaGestor() 

    /**
     * Returns the value of field 'cdDigitoContaGestor'.
     * 
     * @return String
     * @return the value of field 'cdDigitoContaGestor'.
     */
    public java.lang.String getCdDigitoContaGestor()
    {
        return this._cdDigitoContaGestor;
    } //-- java.lang.String getCdDigitoContaGestor() 

    /**
     * Returns the value of field 'cdImpressaoFolha'.
     * 
     * @return String
     * @return the value of field 'cdImpressaoFolha'.
     */
    public java.lang.String getCdImpressaoFolha()
    {
        return this._cdImpressaoFolha;
    } //-- java.lang.String getCdImpressaoFolha() 

    /**
     * Returns the value of field 'cdPessoaEmpresa'.
     * 
     * @return long
     * @return the value of field 'cdPessoaEmpresa'.
     */
    public long getCdPessoaEmpresa()
    {
        return this._cdPessoaEmpresa;
    } //-- long getCdPessoaEmpresa() 

    /**
     * Returns the value of field 'cdSiglaUf'.
     * 
     * @return String
     * @return the value of field 'cdSiglaUf'.
     */
    public java.lang.String getCdSiglaUf()
    {
        return this._cdSiglaUf;
    } //-- java.lang.String getCdSiglaUf() 

    /**
     * Returns the value of field 'cdSiglaUfAgencia'.
     * 
     * @return String
     * @return the value of field 'cdSiglaUfAgencia'.
     */
    public java.lang.String getCdSiglaUfAgencia()
    {
        return this._cdSiglaUfAgencia;
    } //-- java.lang.String getCdSiglaUfAgencia() 

    /**
     * Returns the value of field 'codMensagem'.
     * 
     * @return String
     * @return the value of field 'codMensagem'.
     */
    public java.lang.String getCodMensagem()
    {
        return this._codMensagem;
    } //-- java.lang.String getCodMensagem() 

    /**
     * Returns the value of field 'dsAgenciaGestor'.
     * 
     * @return String
     * @return the value of field 'dsAgenciaGestor'.
     */
    public java.lang.String getDsAgenciaGestor()
    {
        return this._dsAgenciaGestor;
    } //-- java.lang.String getDsAgenciaGestor() 

    /**
     * Returns the value of field 'dsBairroAgencia'.
     * 
     * @return String
     * @return the value of field 'dsBairroAgencia'.
     */
    public java.lang.String getDsBairroAgencia()
    {
        return this._dsBairroAgencia;
    } //-- java.lang.String getDsBairroAgencia() 

    /**
     * Returns the value of field 'dsBairroEndereco'.
     * 
     * @return String
     * @return the value of field 'dsBairroEndereco'.
     */
    public java.lang.String getDsBairroEndereco()
    {
        return this._dsBairroEndereco;
    } //-- java.lang.String getDsBairroEndereco() 

    /**
     * Returns the value of field 'dsChequeOp'.
     * 
     * @return String
     * @return the value of field 'dsChequeOp'.
     */
    public java.lang.String getDsChequeOp()
    {
        return this._dsChequeOp;
    } //-- java.lang.String getDsChequeOp() 

    /**
     * Returns the value of field 'dsCidadeAgencia'.
     * 
     * @return String
     * @return the value of field 'dsCidadeAgencia'.
     */
    public java.lang.String getDsCidadeAgencia()
    {
        return this._dsCidadeAgencia;
    } //-- java.lang.String getDsCidadeAgencia() 

    /**
     * Returns the value of field 'dsCidadeEndereco'.
     * 
     * @return String
     * @return the value of field 'dsCidadeEndereco'.
     */
    public java.lang.String getDsCidadeEndereco()
    {
        return this._dsCidadeEndereco;
    } //-- java.lang.String getDsCidadeEndereco() 

    /**
     * Returns the value of field 'dsComplementoAgencia'.
     * 
     * @return String
     * @return the value of field 'dsComplementoAgencia'.
     */
    public java.lang.String getDsComplementoAgencia()
    {
        return this._dsComplementoAgencia;
    } //-- java.lang.String getDsComplementoAgencia() 

    /**
     * Returns the value of field 'dsComplementoEndereco'.
     * 
     * @return String
     * @return the value of field 'dsComplementoEndereco'.
     */
    public java.lang.String getDsComplementoEndereco()
    {
        return this._dsComplementoEndereco;
    } //-- java.lang.String getDsComplementoEndereco() 

    /**
     * Returns the value of field 'dsCreditoConta'.
     * 
     * @return String
     * @return the value of field 'dsCreditoConta'.
     */
    public java.lang.String getDsCreditoConta()
    {
        return this._dsCreditoConta;
    } //-- java.lang.String getDsCreditoConta() 

    /**
     * Returns the value of field 'dsDiaAviso'.
     * 
     * @return String
     * @return the value of field 'dsDiaAviso'.
     */
    public java.lang.String getDsDiaAviso()
    {
        return this._dsDiaAviso;
    } //-- java.lang.String getDsDiaAviso() 

    /**
     * Returns the value of field 'dsDiaInatividade'.
     * 
     * @return String
     * @return the value of field 'dsDiaInatividade'.
     */
    public java.lang.String getDsDiaInatividade()
    {
        return this._dsDiaInatividade;
    } //-- java.lang.String getDsDiaInatividade() 

    /**
     * Returns the value of field 'dsDoc'.
     * 
     * @return String
     * @return the value of field 'dsDoc'.
     */
    public java.lang.String getDsDoc()
    {
        return this._dsDoc;
    } //-- java.lang.String getDsDoc() 

    /**
     * Returns the value of field 'dsImpressaoComprovante'.
     * 
     * @return String
     * @return the value of field 'dsImpressaoComprovante'.
     */
    public java.lang.String getDsImpressaoComprovante()
    {
        return this._dsImpressaoComprovante;
    } //-- java.lang.String getDsImpressaoComprovante() 

    /**
     * Returns the value of field 'dsImpressaoFornecedor'.
     * 
     * @return String
     * @return the value of field 'dsImpressaoFornecedor'.
     */
    public java.lang.String getDsImpressaoFornecedor()
    {
        return this._dsImpressaoFornecedor;
    } //-- java.lang.String getDsImpressaoFornecedor() 

    /**
     * Returns the value of field 'dsImpressaoPtrbEmpresa'.
     * 
     * @return String
     * @return the value of field 'dsImpressaoPtrbEmpresa'.
     */
    public java.lang.String getDsImpressaoPtrbEmpresa()
    {
        return this._dsImpressaoPtrbEmpresa;
    } //-- java.lang.String getDsImpressaoPtrbEmpresa() 

    /**
     * Returns the value of field 'dsImpressaoPtrbLine'.
     * 
     * @return String
     * @return the value of field 'dsImpressaoPtrbLine'.
     */
    public java.lang.String getDsImpressaoPtrbLine()
    {
        return this._dsImpressaoPtrbLine;
    } //-- java.lang.String getDsImpressaoPtrbLine() 

    /**
     * Returns the value of field 'dsImpressaoPtrbTributo'.
     * 
     * @return String
     * @return the value of field 'dsImpressaoPtrbTributo'.
     */
    public java.lang.String getDsImpressaoPtrbTributo()
    {
        return this._dsImpressaoPtrbTributo;
    } //-- java.lang.String getDsImpressaoPtrbTributo() 

    /**
     * Returns the value of field 'dsLogradouroAgencia'.
     * 
     * @return String
     * @return the value of field 'dsLogradouroAgencia'.
     */
    public java.lang.String getDsLogradouroAgencia()
    {
        return this._dsLogradouroAgencia;
    } //-- java.lang.String getDsLogradouroAgencia() 

    /**
     * Returns the value of field 'dsLogradouroNumero'.
     * 
     * @return String
     * @return the value of field 'dsLogradouroNumero'.
     */
    public java.lang.String getDsLogradouroNumero()
    {
        return this._dsLogradouroNumero;
    } //-- java.lang.String getDsLogradouroNumero() 

    /**
     * Returns the value of field 'dsLogradouroPessoa'.
     * 
     * @return String
     * @return the value of field 'dsLogradouroPessoa'.
     */
    public java.lang.String getDsLogradouroPessoa()
    {
        return this._dsLogradouroPessoa;
    } //-- java.lang.String getDsLogradouroPessoa() 

    /**
     * Returns the value of field 'dsTarifa'.
     * 
     * @return String
     * @return the value of field 'dsTarifa'.
     */
    public java.lang.String getDsTarifa()
    {
        return this._dsTarifa;
    } //-- java.lang.String getDsTarifa() 

    /**
     * Returns the value of field 'dsTed'.
     * 
     * @return String
     * @return the value of field 'dsTed'.
     */
    public java.lang.String getDsTed()
    {
        return this._dsTed;
    } //-- java.lang.String getDsTed() 

    /**
     * Returns the value of field 'dsTipoRejeicao'.
     * 
     * @return String
     * @return the value of field 'dsTipoRejeicao'.
     */
    public java.lang.String getDsTipoRejeicao()
    {
        return this._dsTipoRejeicao;
    } //-- java.lang.String getDsTipoRejeicao() 

    /**
     * Returns the value of field 'dsTituloBradesco'.
     * 
     * @return String
     * @return the value of field 'dsTituloBradesco'.
     */
    public java.lang.String getDsTituloBradesco()
    {
        return this._dsTituloBradesco;
    } //-- java.lang.String getDsTituloBradesco() 

    /**
     * Returns the value of field 'dsTituloOutros'.
     * 
     * @return String
     * @return the value of field 'dsTituloOutros'.
     */
    public java.lang.String getDsTituloOutros()
    {
        return this._dsTituloOutros;
    } //-- java.lang.String getDsTituloOutros() 

    /**
     * Returns the value of field 'mensagem'.
     * 
     * @return String
     * @return the value of field 'mensagem'.
     */
    public java.lang.String getMensagem()
    {
        return this._mensagem;
    } //-- java.lang.String getMensagem() 

    /**
     * Returns the value of field 'nmEmpresa'.
     * 
     * @return String
     * @return the value of field 'nmEmpresa'.
     */
    public java.lang.String getNmEmpresa()
    {
        return this._nmEmpresa;
    } //-- java.lang.String getNmEmpresa() 

    /**
     * Returns the value of field 'nmParticipante'.
     * 
     * @return String
     * @return the value of field 'nmParticipante'.
     */
    public java.lang.String getNmParticipante()
    {
        return this._nmParticipante;
    } //-- java.lang.String getNmParticipante() 

    /**
     * Returns the value of field 'nrLogradouroAgencia'.
     * 
     * @return String
     * @return the value of field 'nrLogradouroAgencia'.
     */
    public java.lang.String getNrLogradouroAgencia()
    {
        return this._nrLogradouroAgencia;
    } //-- java.lang.String getNrLogradouroAgencia() 

    /**
     * Returns the value of field 'qtDiaAviso'.
     * 
     * @return int
     * @return the value of field 'qtDiaAviso'.
     */
    public int getQtDiaAviso()
    {
        return this._qtDiaAviso;
    } //-- int getQtDiaAviso() 

    /**
     * Returns the value of field 'qtDiaInatividade'.
     * 
     * @return int
     * @return the value of field 'qtDiaInatividade'.
     */
    public int getQtDiaInatividade()
    {
        return this._qtDiaInatividade;
    } //-- int getQtDiaInatividade() 

    /**
     * Returns the value of field 'qtMesComprovante'.
     * 
     * @return int
     * @return the value of field 'qtMesComprovante'.
     */
    public int getQtMesComprovante()
    {
        return this._qtMesComprovante;
    } //-- int getQtMesComprovante() 

    /**
     * Returns the value of field 'vlTarifaFolha'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlTarifaFolha'.
     */
    public java.math.BigDecimal getVlTarifaFolha()
    {
        return this._vlTarifaFolha;
    } //-- java.math.BigDecimal getVlTarifaFolha() 

    /**
     * Method hasCdAgenciaGestorContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgenciaGestorContrato()
    {
        return this._has_cdAgenciaGestorContrato;
    } //-- boolean hasCdAgenciaGestorContrato() 

    /**
     * Method hasCdCep
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCep()
    {
        return this._has_cdCep;
    } //-- boolean hasCdCep() 

    /**
     * Method hasCdCepAgencia
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCepAgencia()
    {
        return this._has_cdCepAgencia;
    } //-- boolean hasCdCepAgencia() 

    /**
     * Method hasCdCepComplemento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCepComplemento()
    {
        return this._has_cdCepComplemento;
    } //-- boolean hasCdCepComplemento() 

    /**
     * Method hasCdClubPessoa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdClubPessoa()
    {
        return this._has_cdClubPessoa;
    } //-- boolean hasCdClubPessoa() 

    /**
     * Method hasCdComplementoCepAgencia
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdComplementoCepAgencia()
    {
        return this._has_cdComplementoCepAgencia;
    } //-- boolean hasCdComplementoCepAgencia() 

    /**
     * Method hasCdContaAgenciaGestor
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdContaAgenciaGestor()
    {
        return this._has_cdContaAgenciaGestor;
    } //-- boolean hasCdContaAgenciaGestor() 

    /**
     * Method hasCdDigitoAgenciaGestor
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdDigitoAgenciaGestor()
    {
        return this._has_cdDigitoAgenciaGestor;
    } //-- boolean hasCdDigitoAgenciaGestor() 

    /**
     * Method hasCdPessoaEmpresa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaEmpresa()
    {
        return this._has_cdPessoaEmpresa;
    } //-- boolean hasCdPessoaEmpresa() 

    /**
     * Method hasQtDiaAviso
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtDiaAviso()
    {
        return this._has_qtDiaAviso;
    } //-- boolean hasQtDiaAviso() 

    /**
     * Method hasQtDiaInatividade
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtDiaInatividade()
    {
        return this._has_qtDiaInatividade;
    } //-- boolean hasQtDiaInatividade() 

    /**
     * Method hasQtMesComprovante
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtMesComprovante()
    {
        return this._has_qtMesComprovante;
    } //-- boolean hasQtMesComprovante() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdAgenciaGestorContrato'.
     * 
     * @param cdAgenciaGestorContrato the value of field
     * 'cdAgenciaGestorContrato'.
     */
    public void setCdAgenciaGestorContrato(int cdAgenciaGestorContrato)
    {
        this._cdAgenciaGestorContrato = cdAgenciaGestorContrato;
        this._has_cdAgenciaGestorContrato = true;
    } //-- void setCdAgenciaGestorContrato(int) 

    /**
     * Sets the value of field 'cdCep'.
     * 
     * @param cdCep the value of field 'cdCep'.
     */
    public void setCdCep(int cdCep)
    {
        this._cdCep = cdCep;
        this._has_cdCep = true;
    } //-- void setCdCep(int) 

    /**
     * Sets the value of field 'cdCepAgencia'.
     * 
     * @param cdCepAgencia the value of field 'cdCepAgencia'.
     */
    public void setCdCepAgencia(int cdCepAgencia)
    {
        this._cdCepAgencia = cdCepAgencia;
        this._has_cdCepAgencia = true;
    } //-- void setCdCepAgencia(int) 

    /**
     * Sets the value of field 'cdCepComplemento'.
     * 
     * @param cdCepComplemento the value of field 'cdCepComplemento'
     */
    public void setCdCepComplemento(int cdCepComplemento)
    {
        this._cdCepComplemento = cdCepComplemento;
        this._has_cdCepComplemento = true;
    } //-- void setCdCepComplemento(int) 

    /**
     * Sets the value of field 'cdClubPessoa'.
     * 
     * @param cdClubPessoa the value of field 'cdClubPessoa'.
     */
    public void setCdClubPessoa(long cdClubPessoa)
    {
        this._cdClubPessoa = cdClubPessoa;
        this._has_cdClubPessoa = true;
    } //-- void setCdClubPessoa(long) 

    /**
     * Sets the value of field 'cdComplementoCepAgencia'.
     * 
     * @param cdComplementoCepAgencia the value of field
     * 'cdComplementoCepAgencia'.
     */
    public void setCdComplementoCepAgencia(int cdComplementoCepAgencia)
    {
        this._cdComplementoCepAgencia = cdComplementoCepAgencia;
        this._has_cdComplementoCepAgencia = true;
    } //-- void setCdComplementoCepAgencia(int) 

    /**
     * Sets the value of field 'cdContaAgenciaGestor'.
     * 
     * @param cdContaAgenciaGestor the value of field
     * 'cdContaAgenciaGestor'.
     */
    public void setCdContaAgenciaGestor(long cdContaAgenciaGestor)
    {
        this._cdContaAgenciaGestor = cdContaAgenciaGestor;
        this._has_cdContaAgenciaGestor = true;
    } //-- void setCdContaAgenciaGestor(long) 

    /**
     * Sets the value of field 'cdCpfCnpjEmpresa'.
     * 
     * @param cdCpfCnpjEmpresa the value of field 'cdCpfCnpjEmpresa'
     */
    public void setCdCpfCnpjEmpresa(java.lang.String cdCpfCnpjEmpresa)
    {
        this._cdCpfCnpjEmpresa = cdCpfCnpjEmpresa;
    } //-- void setCdCpfCnpjEmpresa(java.lang.String) 

    /**
     * Sets the value of field 'cdCpfCnpjParticipante'.
     * 
     * @param cdCpfCnpjParticipante the value of field
     * 'cdCpfCnpjParticipante'.
     */
    public void setCdCpfCnpjParticipante(java.lang.String cdCpfCnpjParticipante)
    {
        this._cdCpfCnpjParticipante = cdCpfCnpjParticipante;
    } //-- void setCdCpfCnpjParticipante(java.lang.String) 

    /**
     * Sets the value of field 'cdDigitoAgenciaGestor'.
     * 
     * @param cdDigitoAgenciaGestor the value of field
     * 'cdDigitoAgenciaGestor'.
     */
    public void setCdDigitoAgenciaGestor(int cdDigitoAgenciaGestor)
    {
        this._cdDigitoAgenciaGestor = cdDigitoAgenciaGestor;
        this._has_cdDigitoAgenciaGestor = true;
    } //-- void setCdDigitoAgenciaGestor(int) 

    /**
     * Sets the value of field 'cdDigitoContaGestor'.
     * 
     * @param cdDigitoContaGestor the value of field
     * 'cdDigitoContaGestor'.
     */
    public void setCdDigitoContaGestor(java.lang.String cdDigitoContaGestor)
    {
        this._cdDigitoContaGestor = cdDigitoContaGestor;
    } //-- void setCdDigitoContaGestor(java.lang.String) 

    /**
     * Sets the value of field 'cdImpressaoFolha'.
     * 
     * @param cdImpressaoFolha the value of field 'cdImpressaoFolha'
     */
    public void setCdImpressaoFolha(java.lang.String cdImpressaoFolha)
    {
        this._cdImpressaoFolha = cdImpressaoFolha;
    } //-- void setCdImpressaoFolha(java.lang.String) 

    /**
     * Sets the value of field 'cdPessoaEmpresa'.
     * 
     * @param cdPessoaEmpresa the value of field 'cdPessoaEmpresa'.
     */
    public void setCdPessoaEmpresa(long cdPessoaEmpresa)
    {
        this._cdPessoaEmpresa = cdPessoaEmpresa;
        this._has_cdPessoaEmpresa = true;
    } //-- void setCdPessoaEmpresa(long) 

    /**
     * Sets the value of field 'cdSiglaUf'.
     * 
     * @param cdSiglaUf the value of field 'cdSiglaUf'.
     */
    public void setCdSiglaUf(java.lang.String cdSiglaUf)
    {
        this._cdSiglaUf = cdSiglaUf;
    } //-- void setCdSiglaUf(java.lang.String) 

    /**
     * Sets the value of field 'cdSiglaUfAgencia'.
     * 
     * @param cdSiglaUfAgencia the value of field 'cdSiglaUfAgencia'
     */
    public void setCdSiglaUfAgencia(java.lang.String cdSiglaUfAgencia)
    {
        this._cdSiglaUfAgencia = cdSiglaUfAgencia;
    } //-- void setCdSiglaUfAgencia(java.lang.String) 

    /**
     * Sets the value of field 'codMensagem'.
     * 
     * @param codMensagem the value of field 'codMensagem'.
     */
    public void setCodMensagem(java.lang.String codMensagem)
    {
        this._codMensagem = codMensagem;
    } //-- void setCodMensagem(java.lang.String) 

    /**
     * Sets the value of field 'dsAgenciaGestor'.
     * 
     * @param dsAgenciaGestor the value of field 'dsAgenciaGestor'.
     */
    public void setDsAgenciaGestor(java.lang.String dsAgenciaGestor)
    {
        this._dsAgenciaGestor = dsAgenciaGestor;
    } //-- void setDsAgenciaGestor(java.lang.String) 

    /**
     * Sets the value of field 'dsBairroAgencia'.
     * 
     * @param dsBairroAgencia the value of field 'dsBairroAgencia'.
     */
    public void setDsBairroAgencia(java.lang.String dsBairroAgencia)
    {
        this._dsBairroAgencia = dsBairroAgencia;
    } //-- void setDsBairroAgencia(java.lang.String) 

    /**
     * Sets the value of field 'dsBairroEndereco'.
     * 
     * @param dsBairroEndereco the value of field 'dsBairroEndereco'
     */
    public void setDsBairroEndereco(java.lang.String dsBairroEndereco)
    {
        this._dsBairroEndereco = dsBairroEndereco;
    } //-- void setDsBairroEndereco(java.lang.String) 

    /**
     * Sets the value of field 'dsChequeOp'.
     * 
     * @param dsChequeOp the value of field 'dsChequeOp'.
     */
    public void setDsChequeOp(java.lang.String dsChequeOp)
    {
        this._dsChequeOp = dsChequeOp;
    } //-- void setDsChequeOp(java.lang.String) 

    /**
     * Sets the value of field 'dsCidadeAgencia'.
     * 
     * @param dsCidadeAgencia the value of field 'dsCidadeAgencia'.
     */
    public void setDsCidadeAgencia(java.lang.String dsCidadeAgencia)
    {
        this._dsCidadeAgencia = dsCidadeAgencia;
    } //-- void setDsCidadeAgencia(java.lang.String) 

    /**
     * Sets the value of field 'dsCidadeEndereco'.
     * 
     * @param dsCidadeEndereco the value of field 'dsCidadeEndereco'
     */
    public void setDsCidadeEndereco(java.lang.String dsCidadeEndereco)
    {
        this._dsCidadeEndereco = dsCidadeEndereco;
    } //-- void setDsCidadeEndereco(java.lang.String) 

    /**
     * Sets the value of field 'dsComplementoAgencia'.
     * 
     * @param dsComplementoAgencia the value of field
     * 'dsComplementoAgencia'.
     */
    public void setDsComplementoAgencia(java.lang.String dsComplementoAgencia)
    {
        this._dsComplementoAgencia = dsComplementoAgencia;
    } //-- void setDsComplementoAgencia(java.lang.String) 

    /**
     * Sets the value of field 'dsComplementoEndereco'.
     * 
     * @param dsComplementoEndereco the value of field
     * 'dsComplementoEndereco'.
     */
    public void setDsComplementoEndereco(java.lang.String dsComplementoEndereco)
    {
        this._dsComplementoEndereco = dsComplementoEndereco;
    } //-- void setDsComplementoEndereco(java.lang.String) 

    /**
     * Sets the value of field 'dsCreditoConta'.
     * 
     * @param dsCreditoConta the value of field 'dsCreditoConta'.
     */
    public void setDsCreditoConta(java.lang.String dsCreditoConta)
    {
        this._dsCreditoConta = dsCreditoConta;
    } //-- void setDsCreditoConta(java.lang.String) 

    /**
     * Sets the value of field 'dsDiaAviso'.
     * 
     * @param dsDiaAviso the value of field 'dsDiaAviso'.
     */
    public void setDsDiaAviso(java.lang.String dsDiaAviso)
    {
        this._dsDiaAviso = dsDiaAviso;
    } //-- void setDsDiaAviso(java.lang.String) 

    /**
     * Sets the value of field 'dsDiaInatividade'.
     * 
     * @param dsDiaInatividade the value of field 'dsDiaInatividade'
     */
    public void setDsDiaInatividade(java.lang.String dsDiaInatividade)
    {
        this._dsDiaInatividade = dsDiaInatividade;
    } //-- void setDsDiaInatividade(java.lang.String) 

    /**
     * Sets the value of field 'dsDoc'.
     * 
     * @param dsDoc the value of field 'dsDoc'.
     */
    public void setDsDoc(java.lang.String dsDoc)
    {
        this._dsDoc = dsDoc;
    } //-- void setDsDoc(java.lang.String) 

    /**
     * Sets the value of field 'dsImpressaoComprovante'.
     * 
     * @param dsImpressaoComprovante the value of field
     * 'dsImpressaoComprovante'.
     */
    public void setDsImpressaoComprovante(java.lang.String dsImpressaoComprovante)
    {
        this._dsImpressaoComprovante = dsImpressaoComprovante;
    } //-- void setDsImpressaoComprovante(java.lang.String) 

    /**
     * Sets the value of field 'dsImpressaoFornecedor'.
     * 
     * @param dsImpressaoFornecedor the value of field
     * 'dsImpressaoFornecedor'.
     */
    public void setDsImpressaoFornecedor(java.lang.String dsImpressaoFornecedor)
    {
        this._dsImpressaoFornecedor = dsImpressaoFornecedor;
    } //-- void setDsImpressaoFornecedor(java.lang.String) 

    /**
     * Sets the value of field 'dsImpressaoPtrbEmpresa'.
     * 
     * @param dsImpressaoPtrbEmpresa the value of field
     * 'dsImpressaoPtrbEmpresa'.
     */
    public void setDsImpressaoPtrbEmpresa(java.lang.String dsImpressaoPtrbEmpresa)
    {
        this._dsImpressaoPtrbEmpresa = dsImpressaoPtrbEmpresa;
    } //-- void setDsImpressaoPtrbEmpresa(java.lang.String) 

    /**
     * Sets the value of field 'dsImpressaoPtrbLine'.
     * 
     * @param dsImpressaoPtrbLine the value of field
     * 'dsImpressaoPtrbLine'.
     */
    public void setDsImpressaoPtrbLine(java.lang.String dsImpressaoPtrbLine)
    {
        this._dsImpressaoPtrbLine = dsImpressaoPtrbLine;
    } //-- void setDsImpressaoPtrbLine(java.lang.String) 

    /**
     * Sets the value of field 'dsImpressaoPtrbTributo'.
     * 
     * @param dsImpressaoPtrbTributo the value of field
     * 'dsImpressaoPtrbTributo'.
     */
    public void setDsImpressaoPtrbTributo(java.lang.String dsImpressaoPtrbTributo)
    {
        this._dsImpressaoPtrbTributo = dsImpressaoPtrbTributo;
    } //-- void setDsImpressaoPtrbTributo(java.lang.String) 

    /**
     * Sets the value of field 'dsLogradouroAgencia'.
     * 
     * @param dsLogradouroAgencia the value of field
     * 'dsLogradouroAgencia'.
     */
    public void setDsLogradouroAgencia(java.lang.String dsLogradouroAgencia)
    {
        this._dsLogradouroAgencia = dsLogradouroAgencia;
    } //-- void setDsLogradouroAgencia(java.lang.String) 

    /**
     * Sets the value of field 'dsLogradouroNumero'.
     * 
     * @param dsLogradouroNumero the value of field
     * 'dsLogradouroNumero'.
     */
    public void setDsLogradouroNumero(java.lang.String dsLogradouroNumero)
    {
        this._dsLogradouroNumero = dsLogradouroNumero;
    } //-- void setDsLogradouroNumero(java.lang.String) 

    /**
     * Sets the value of field 'dsLogradouroPessoa'.
     * 
     * @param dsLogradouroPessoa the value of field
     * 'dsLogradouroPessoa'.
     */
    public void setDsLogradouroPessoa(java.lang.String dsLogradouroPessoa)
    {
        this._dsLogradouroPessoa = dsLogradouroPessoa;
    } //-- void setDsLogradouroPessoa(java.lang.String) 

    /**
     * Sets the value of field 'dsTarifa'.
     * 
     * @param dsTarifa the value of field 'dsTarifa'.
     */
    public void setDsTarifa(java.lang.String dsTarifa)
    {
        this._dsTarifa = dsTarifa;
    } //-- void setDsTarifa(java.lang.String) 

    /**
     * Sets the value of field 'dsTed'.
     * 
     * @param dsTed the value of field 'dsTed'.
     */
    public void setDsTed(java.lang.String dsTed)
    {
        this._dsTed = dsTed;
    } //-- void setDsTed(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoRejeicao'.
     * 
     * @param dsTipoRejeicao the value of field 'dsTipoRejeicao'.
     */
    public void setDsTipoRejeicao(java.lang.String dsTipoRejeicao)
    {
        this._dsTipoRejeicao = dsTipoRejeicao;
    } //-- void setDsTipoRejeicao(java.lang.String) 

    /**
     * Sets the value of field 'dsTituloBradesco'.
     * 
     * @param dsTituloBradesco the value of field 'dsTituloBradesco'
     */
    public void setDsTituloBradesco(java.lang.String dsTituloBradesco)
    {
        this._dsTituloBradesco = dsTituloBradesco;
    } //-- void setDsTituloBradesco(java.lang.String) 

    /**
     * Sets the value of field 'dsTituloOutros'.
     * 
     * @param dsTituloOutros the value of field 'dsTituloOutros'.
     */
    public void setDsTituloOutros(java.lang.String dsTituloOutros)
    {
        this._dsTituloOutros = dsTituloOutros;
    } //-- void setDsTituloOutros(java.lang.String) 

    /**
     * Sets the value of field 'mensagem'.
     * 
     * @param mensagem the value of field 'mensagem'.
     */
    public void setMensagem(java.lang.String mensagem)
    {
        this._mensagem = mensagem;
    } //-- void setMensagem(java.lang.String) 

    /**
     * Sets the value of field 'nmEmpresa'.
     * 
     * @param nmEmpresa the value of field 'nmEmpresa'.
     */
    public void setNmEmpresa(java.lang.String nmEmpresa)
    {
        this._nmEmpresa = nmEmpresa;
    } //-- void setNmEmpresa(java.lang.String) 

    /**
     * Sets the value of field 'nmParticipante'.
     * 
     * @param nmParticipante the value of field 'nmParticipante'.
     */
    public void setNmParticipante(java.lang.String nmParticipante)
    {
        this._nmParticipante = nmParticipante;
    } //-- void setNmParticipante(java.lang.String) 

    /**
     * Sets the value of field 'nrLogradouroAgencia'.
     * 
     * @param nrLogradouroAgencia the value of field
     * 'nrLogradouroAgencia'.
     */
    public void setNrLogradouroAgencia(java.lang.String nrLogradouroAgencia)
    {
        this._nrLogradouroAgencia = nrLogradouroAgencia;
    } //-- void setNrLogradouroAgencia(java.lang.String) 

    /**
     * Sets the value of field 'qtDiaAviso'.
     * 
     * @param qtDiaAviso the value of field 'qtDiaAviso'.
     */
    public void setQtDiaAviso(int qtDiaAviso)
    {
        this._qtDiaAviso = qtDiaAviso;
        this._has_qtDiaAviso = true;
    } //-- void setQtDiaAviso(int) 

    /**
     * Sets the value of field 'qtDiaInatividade'.
     * 
     * @param qtDiaInatividade the value of field 'qtDiaInatividade'
     */
    public void setQtDiaInatividade(int qtDiaInatividade)
    {
        this._qtDiaInatividade = qtDiaInatividade;
        this._has_qtDiaInatividade = true;
    } //-- void setQtDiaInatividade(int) 

    /**
     * Sets the value of field 'qtMesComprovante'.
     * 
     * @param qtMesComprovante the value of field 'qtMesComprovante'
     */
    public void setQtMesComprovante(int qtMesComprovante)
    {
        this._qtMesComprovante = qtMesComprovante;
        this._has_qtMesComprovante = true;
    } //-- void setQtMesComprovante(int) 

    /**
     * Sets the value of field 'vlTarifaFolha'.
     * 
     * @param vlTarifaFolha the value of field 'vlTarifaFolha'.
     */
    public void setVlTarifaFolha(java.math.BigDecimal vlTarifaFolha)
    {
        this._vlTarifaFolha = vlTarifaFolha;
    } //-- void setVlTarifaFolha(java.math.BigDecimal) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ImprimirContratoResponse
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.imprimircontrato.response.ImprimirContratoResponse unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.imprimircontrato.response.ImprimirContratoResponse) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.imprimircontrato.response.ImprimirContratoResponse.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimircontrato.response.ImprimirContratoResponse unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
