/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.detalharhorariolimite.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class PGICWI56HEADER.
 * 
 * @version $Revision$ $Date$
 */
public class PGICWI56HEADER implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _PGICWI56CODLAYOUT
     */
    private java.lang.String _PGICWI56CODLAYOUT = "PGICWI56";

    /**
     * Field _PGICWI56TAMLAYOUT
     */
    private int _PGICWI56TAMLAYOUT = 00015;

    /**
     * keeps track of state for field: _PGICWI56TAMLAYOUT
     */
    private boolean _has_PGICWI56TAMLAYOUT;


      //----------------/
     //- Constructors -/
    //----------------/

    public PGICWI56HEADER() 
     {
        super();
        setPGICWI56CODLAYOUT("PGICWI56");
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharhorariolimite.request.PGICWI56HEADER()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deletePGICWI56TAMLAYOUT
     * 
     */
    public void deletePGICWI56TAMLAYOUT()
    {
        this._has_PGICWI56TAMLAYOUT= false;
    } //-- void deletePGICWI56TAMLAYOUT() 

    /**
     * Returns the value of field 'PGICWI56CODLAYOUT'.
     * 
     * @return String
     * @return the value of field 'PGICWI56CODLAYOUT'.
     */
    public java.lang.String getPGICWI56CODLAYOUT()
    {
        return this._PGICWI56CODLAYOUT;
    } //-- java.lang.String getPGICWI56CODLAYOUT() 

    /**
     * Returns the value of field 'PGICWI56TAMLAYOUT'.
     * 
     * @return int
     * @return the value of field 'PGICWI56TAMLAYOUT'.
     */
    public int getPGICWI56TAMLAYOUT()
    {
        return this._PGICWI56TAMLAYOUT;
    } //-- int getPGICWI56TAMLAYOUT() 

    /**
     * Method hasPGICWI56TAMLAYOUT
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasPGICWI56TAMLAYOUT()
    {
        return this._has_PGICWI56TAMLAYOUT;
    } //-- boolean hasPGICWI56TAMLAYOUT() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'PGICWI56CODLAYOUT'.
     * 
     * @param PGICWI56CODLAYOUT the value of field
     * 'PGICWI56CODLAYOUT'.
     */
    public void setPGICWI56CODLAYOUT(java.lang.String PGICWI56CODLAYOUT)
    {
        this._PGICWI56CODLAYOUT = PGICWI56CODLAYOUT;
    } //-- void setPGICWI56CODLAYOUT(java.lang.String) 

    /**
     * Sets the value of field 'PGICWI56TAMLAYOUT'.
     * 
     * @param PGICWI56TAMLAYOUT the value of field
     * 'PGICWI56TAMLAYOUT'.
     */
    public void setPGICWI56TAMLAYOUT(int PGICWI56TAMLAYOUT)
    {
        this._PGICWI56TAMLAYOUT = PGICWI56TAMLAYOUT;
        this._has_PGICWI56TAMLAYOUT = true;
    } //-- void setPGICWI56TAMLAYOUT(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return PGICWI56HEADER
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.detalharhorariolimite.request.PGICWI56HEADER unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.detalharhorariolimite.request.PGICWI56HEADER) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.detalharhorariolimite.request.PGICWI56HEADER.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharhorariolimite.request.PGICWI56HEADER unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
