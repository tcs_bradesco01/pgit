/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantersolestornopagto.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantersolestornopagto.bean;


/**
 * Nome: ConsultarSolicitacaoEstornoPagtosSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarSolicitacaoEstornoPagtosSaidaDTO {
	
	   /** Atributo cdSolicitacaoPagamento. */
   	private Integer cdSolicitacaoPagamento;
	   
   	/** Atributo nrSolicitacaoPagamentoIntegrado. */
   	private Integer nrSolicitacaoPagamentoIntegrado;
	   
   	/** Atributo cdPessoaJuridicaContrato. */
   	private Long cdPessoaJuridicaContrato;
	   
   	/** Atributo cdTipoContratoNegocio. */
   	private Integer cdTipoContratoNegocio;
	   
   	/** Atributo nrSequenciaContratoNegocio. */
   	private Long nrSequenciaContratoNegocio;
	   
   	/** Atributo cdSituacaoSolicitacao. */
   	private Integer cdSituacaoSolicitacao;

	/** Atributo cdCpfCnpjParticipante. */
   	private String cdCpfCnpjParticipante;
	   
   	/** Atributo cdPessoaCompleto. */
   	private String cdPessoaCompleto;
	   
   	/** Atributo dsSituacaoSolicitante. */
   	private String dsSituacaoSolicitante;
	   
   	/** Atributo cdMotivoSituacaoSolicitante. */
   	private Integer cdMotivoSituacaoSolicitante;
	   
   	/** Atributo dsMotivoSituacaoSolicitante. */
   	private String dsMotivoSituacaoSolicitante;
	   
   	/** Atributo dsDataSolicitacao. */
   	private String dsDataSolicitacao;
	   
   	/** Atributo dsHoraSolicitacao. */
   	private String dsHoraSolicitacao;
	   
   	/** Atributo cdModalidade. */
   	private Integer cdModalidade;
	   
   	/** Atributo dsModalidade. */
   	private String dsModalidade;
   	
	/** Atributo cdFormaEstrnPagamento. */
    private Integer cdFormaEstrnPagamento;
    
    /** Atributo dsFormaEstrnPagamento. */
    private String dsFormaEstrnPagamento;
    
    
    public String getTipoSaldoFormatado(){
		if(cdModalidade != null){
			return dsFormaEstrnPagamento;
		}else{
			return "";
		}
    }
	   
	/**
	 * Get: cdCpfCnpjParticipante.
	 *
	 * @return cdCpfCnpjParticipante
	 */
	public String getCdCpfCnpjParticipante() {
		return cdCpfCnpjParticipante;
	}
	
	/**
	 * Set: cdCpfCnpjParticipante.
	 *
	 * @param cdCpfCnpjParticipante the cd cpf cnpj participante
	 */
	public void setCdCpfCnpjParticipante(String cdCpfCnpjParticipante) {
		this.cdCpfCnpjParticipante = cdCpfCnpjParticipante;
	}
	
	/**
	 * Get: cdModalidade.
	 *
	 * @return cdModalidade
	 */
	public Integer getCdModalidade() {
		return cdModalidade;
	}
	
	/**
	 * Set: cdModalidade.
	 *
	 * @param cdModalidade the cd modalidade
	 */
	public void setCdModalidade(Integer cdModalidade) {
		this.cdModalidade = cdModalidade;
	}
	
	/**
	 * Get: cdMotivoSituacaoSolicitante.
	 *
	 * @return cdMotivoSituacaoSolicitante
	 */
	public Integer getCdMotivoSituacaoSolicitante() {
		return cdMotivoSituacaoSolicitante;
	}
	
	/**
	 * Set: cdMotivoSituacaoSolicitante.
	 *
	 * @param cdMotivoSituacaoSolicitante the cd motivo situacao solicitante
	 */
	public void setCdMotivoSituacaoSolicitante(Integer cdMotivoSituacaoSolicitante) {
		this.cdMotivoSituacaoSolicitante = cdMotivoSituacaoSolicitante;
	}
	
	/**
	 * Get: cdPessoaCompleto.
	 *
	 * @return cdPessoaCompleto
	 */
	public String getCdPessoaCompleto() {
		return cdPessoaCompleto;
	}
	
	/**
	 * Set: cdPessoaCompleto.
	 *
	 * @param cdPessoaCompleto the cd pessoa completo
	 */
	public void setCdPessoaCompleto(String cdPessoaCompleto) {
		this.cdPessoaCompleto = cdPessoaCompleto;
	}
	
	/**
	 * Get: cdPessoaJuridicaContrato.
	 *
	 * @return cdPessoaJuridicaContrato
	 */
	public Long getCdPessoaJuridicaContrato() {
		return cdPessoaJuridicaContrato;
	}
	
	/**
	 * Set: cdPessoaJuridicaContrato.
	 *
	 * @param cdPessoaJuridicaContrato the cd pessoa juridica contrato
	 */
	public void setCdPessoaJuridicaContrato(Long cdPessoaJuridicaContrato) {
		this.cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
	}
	
	/**
	 * Get: cdSituacaoSolicitacao.
	 *
	 * @return cdSituacaoSolicitacao
	 */
	public Integer getCdSituacaoSolicitacao() {
		return cdSituacaoSolicitacao;
	}
	
	/**
	 * Set: cdSituacaoSolicitacao.
	 *
	 * @param cdSituacaoSolicitacao the cd situacao solicitacao
	 */
	public void setCdSituacaoSolicitacao(Integer cdSituacaoSolicitacao) {
		this.cdSituacaoSolicitacao = cdSituacaoSolicitacao;
	}
	
	/**
	 * Get: cdSolicitacaoPagamento.
	 *
	 * @return cdSolicitacaoPagamento
	 */
	public Integer getCdSolicitacaoPagamento() {
		return cdSolicitacaoPagamento;
	}
	
	/**
	 * Set: cdSolicitacaoPagamento.
	 *
	 * @param cdSolicitacaoPagamento the cd solicitacao pagamento
	 */
	public void setCdSolicitacaoPagamento(Integer cdSolicitacaoPagamento) {
		this.cdSolicitacaoPagamento = cdSolicitacaoPagamento;
	}
	
	/**
	 * Get: cdTipoContratoNegocio.
	 *
	 * @return cdTipoContratoNegocio
	 */
	public Integer getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}
	
	/**
	 * Set: cdTipoContratoNegocio.
	 *
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}
	
	/**
	 * Get: dsDataSolicitacao.
	 *
	 * @return dsDataSolicitacao
	 */
	public String getDsDataSolicitacao() {
		return dsDataSolicitacao;
	}
	
	/**
	 * Set: dsDataSolicitacao.
	 *
	 * @param dsDataSolicitacao the ds data solicitacao
	 */
	public void setDsDataSolicitacao(String dsDataSolicitacao) {
		this.dsDataSolicitacao = dsDataSolicitacao;
	}
	
	/**
	 * Get: dsHoraSolicitacao.
	 *
	 * @return dsHoraSolicitacao
	 */
	public String getDsHoraSolicitacao() {
		return dsHoraSolicitacao;
	}
	
	/**
	 * Set: dsHoraSolicitacao.
	 *
	 * @param dsHoraSolicitacao the ds hora solicitacao
	 */
	public void setDsHoraSolicitacao(String dsHoraSolicitacao) {
		this.dsHoraSolicitacao = dsHoraSolicitacao;
	}
	
	/**
	 * Get: dsModalidade.
	 *
	 * @return dsModalidade
	 */
	public String getDsModalidade() {
		return dsModalidade;
	}
	
	/**
	 * Set: dsModalidade.
	 *
	 * @param dsModalidade the ds modalidade
	 */
	public void setDsModalidade(String dsModalidade) {
		this.dsModalidade = dsModalidade;
	}
	
	/**
	 * Get: dsMotivoSituacaoSolicitante.
	 *
	 * @return dsMotivoSituacaoSolicitante
	 */
	public String getDsMotivoSituacaoSolicitante() {
		return dsMotivoSituacaoSolicitante;
	}
	
	/**
	 * Set: dsMotivoSituacaoSolicitante.
	 *
	 * @param dsMotivoSituacaoSolicitante the ds motivo situacao solicitante
	 */
	public void setDsMotivoSituacaoSolicitante(String dsMotivoSituacaoSolicitante) {
		this.dsMotivoSituacaoSolicitante = dsMotivoSituacaoSolicitante;
	}
	
	/**
	 * Get: dsSituacaoSolicitante.
	 *
	 * @return dsSituacaoSolicitante
	 */
	public String getDsSituacaoSolicitante() {
		return dsSituacaoSolicitante;
	}
	
	/**
	 * Set: dsSituacaoSolicitante.
	 *
	 * @param dsSituacaoSolicitante the ds situacao solicitante
	 */
	public void setDsSituacaoSolicitante(String dsSituacaoSolicitante) {
		this.dsSituacaoSolicitante = dsSituacaoSolicitante;
	}
	
	/**
	 * Get: nrSequenciaContratoNegocio.
	 *
	 * @return nrSequenciaContratoNegocio
	 */
	public Long getNrSequenciaContratoNegocio() {
		return nrSequenciaContratoNegocio;
	}
	
	/**
	 * Set: nrSequenciaContratoNegocio.
	 *
	 * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
	 */
	public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}
	
	/**
	 * Get: nrSolicitacaoPagamentoIntegrado.
	 *
	 * @return nrSolicitacaoPagamentoIntegrado
	 */
	public Integer getNrSolicitacaoPagamentoIntegrado() {
		return nrSolicitacaoPagamentoIntegrado;
	}
	
	/**
	 * Set: nrSolicitacaoPagamentoIntegrado.
	 *
	 * @param nrSolicitacaoPagamentoIntegrado the nr solicitacao pagamento integrado
	 */
	public void setNrSolicitacaoPagamentoIntegrado(
			Integer nrSolicitacaoPagamentoIntegrado) {
		this.nrSolicitacaoPagamentoIntegrado = nrSolicitacaoPagamentoIntegrado;
	}

	public Integer getCdFormaEstrnPagamento() {
		return cdFormaEstrnPagamento;
	}

	public void setCdFormaEstrnPagamento(Integer cdFormaEstrnPagamento) {
		this.cdFormaEstrnPagamento = cdFormaEstrnPagamento;
	}

	public String getDsFormaEstrnPagamento() {
		return dsFormaEstrnPagamento;
	}

	public void setDsFormaEstrnPagamento(String dsFormaEstrnPagamento) {
		this.dsFormaEstrnPagamento = dsFormaEstrnPagamento;
	}


}
