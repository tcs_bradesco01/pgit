/*
 * Nome: br.com.bradesco.web.pgit.service.business.simulacaotarifasnegociacao.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.simulacaotarifasnegociacao.bean;

/**
 * Nome: DetalharSimulacaoTarifaNegociacaoEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarConfigPadraoAtribTipoServModEntradaDTO {

  	/** Atributo cdProdutoServicoOperacao. */
    private Integer cdProdutoServicoOperacao;
    /** Atributo cdProdutoOperacaoRelacionado. */
    private Integer cdProdutoOperacaoRelacionado;
    /** Atributo cdRelacionamentoProduto. */
    private Integer cdRelacionamentoProduto;
	
    
    /**
	 * Nome: getCdProdutoServicoOperacao
	 *
	 * @exception
	 * @throws
	 * @return cdProdutoServicoOperacao
	 */
	public Integer getCdProdutoServicoOperacao() {
		return cdProdutoServicoOperacao;
	}
	/**
	 * Nome: setCdProdutoServicoOperacao
	 *
	 * @exception
	 * @throws
	 * @param cdProdutoServicoOperacao
	 */
	public void setCdProdutoServicoOperacao(Integer cdProdutoServicoOperacao) {
		this.cdProdutoServicoOperacao = cdProdutoServicoOperacao;
	}
	/**
	 * Nome: getCdProdutoOperacaoRelacionado
	 *
	 * @exception
	 * @throws
	 * @return cdProdutoOperacaoRelacionado
	 */
	public Integer getCdProdutoOperacaoRelacionado() {
		return cdProdutoOperacaoRelacionado;
	}
	/**
	 * Nome: setCdProdutoOperacaoRelacionado
	 *
	 * @exception
	 * @throws
	 * @param cdProdutoOperacaoRelacionado
	 */
	public void setCdProdutoOperacaoRelacionado(Integer cdProdutoOperacaoRelacionado) {
		this.cdProdutoOperacaoRelacionado = cdProdutoOperacaoRelacionado;
	}
	/**
	 * Nome: getCdRelacionamentoProduto
	 *
	 * @exception
	 * @throws
	 * @return cdRelacionamentoProduto
	 */
	public Integer getCdRelacionamentoProduto() {
		return cdRelacionamentoProduto;
	}
	/**
	 * Nome: setCdRelacionamentoProduto
	 *
	 * @exception
	 * @throws
	 * @param cdRelacionamentoProduto
	 */
	public void setCdRelacionamentoProduto(Integer cdRelacionamentoProduto) {
		this.cdRelacionamentoProduto = cdRelacionamentoProduto;
	}
	  
	  
}
