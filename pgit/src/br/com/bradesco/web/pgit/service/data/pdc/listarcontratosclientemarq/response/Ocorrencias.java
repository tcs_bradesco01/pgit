/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listarcontratosclientemarq.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdPessoaJuridicaContrato
     */
    private long _cdPessoaJuridicaContrato = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridicaContrato
     */
    private boolean _has_cdPessoaJuridicaContrato;

    /**
     * Field _dsPessoaJuridicaContrato
     */
    private java.lang.String _dsPessoaJuridicaContrato;

    /**
     * Field _cdTipoParticipacaoPessoa
     */
    private int _cdTipoParticipacaoPessoa = 0;

    /**
     * keeps track of state for field: _cdTipoParticipacaoPessoa
     */
    private boolean _has_cdTipoParticipacaoPessoa;

    /**
     * Field _dsTipoParticipacaoPessoa
     */
    private java.lang.String _dsTipoParticipacaoPessoa;

    /**
     * Field _cdTipoContratoNegocio
     */
    private int _cdTipoContratoNegocio = 0;

    /**
     * keeps track of state for field: _cdTipoContratoNegocio
     */
    private boolean _has_cdTipoContratoNegocio;

    /**
     * Field _dsTipoContratoNegocio
     */
    private java.lang.String _dsTipoContratoNegocio;

    /**
     * Field _nrSequenciaContratoNegocio
     */
    private long _nrSequenciaContratoNegocio = 0;

    /**
     * keeps track of state for field: _nrSequenciaContratoNegocio
     */
    private boolean _has_nrSequenciaContratoNegocio;

    /**
     * Field _cdSituacaoParticipanteContrato
     */
    private int _cdSituacaoParticipanteContrato = 0;

    /**
     * keeps track of state for field:
     * _cdSituacaoParticipanteContrato
     */
    private boolean _has_cdSituacaoParticipanteContrato;

    /**
     * Field _dsSituacaoParticipanteContrato
     */
    private java.lang.String _dsSituacaoParticipanteContrato;

    /**
     * Field _dtInicioParticipacaoContrato
     */
    private java.lang.String _dtInicioParticipacaoContrato;

    /**
     * Field _dtFimParticipacaoContrato
     */
    private java.lang.String _dtFimParticipacaoContrato;

    /**
     * Field _dsContrato
     */
    private java.lang.String _dsContrato;

    /**
     * Field _cdSituacaoContratoNegocio
     */
    private int _cdSituacaoContratoNegocio = 0;

    /**
     * keeps track of state for field: _cdSituacaoContratoNegocio
     */
    private boolean _has_cdSituacaoContratoNegocio;

    /**
     * Field _dsSituacaoContratoNegocio
     */
    private java.lang.String _dsSituacaoContratoNegocio;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarcontratosclientemarq.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdPessoaJuridicaContrato
     * 
     */
    public void deleteCdPessoaJuridicaContrato()
    {
        this._has_cdPessoaJuridicaContrato= false;
    } //-- void deleteCdPessoaJuridicaContrato() 

    /**
     * Method deleteCdSituacaoContratoNegocio
     * 
     */
    public void deleteCdSituacaoContratoNegocio()
    {
        this._has_cdSituacaoContratoNegocio= false;
    } //-- void deleteCdSituacaoContratoNegocio() 

    /**
     * Method deleteCdSituacaoParticipanteContrato
     * 
     */
    public void deleteCdSituacaoParticipanteContrato()
    {
        this._has_cdSituacaoParticipanteContrato= false;
    } //-- void deleteCdSituacaoParticipanteContrato() 

    /**
     * Method deleteCdTipoContratoNegocio
     * 
     */
    public void deleteCdTipoContratoNegocio()
    {
        this._has_cdTipoContratoNegocio= false;
    } //-- void deleteCdTipoContratoNegocio() 

    /**
     * Method deleteCdTipoParticipacaoPessoa
     * 
     */
    public void deleteCdTipoParticipacaoPessoa()
    {
        this._has_cdTipoParticipacaoPessoa= false;
    } //-- void deleteCdTipoParticipacaoPessoa() 

    /**
     * Method deleteNrSequenciaContratoNegocio
     * 
     */
    public void deleteNrSequenciaContratoNegocio()
    {
        this._has_nrSequenciaContratoNegocio= false;
    } //-- void deleteNrSequenciaContratoNegocio() 

    /**
     * Returns the value of field 'cdPessoaJuridicaContrato'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridicaContrato'.
     */
    public long getCdPessoaJuridicaContrato()
    {
        return this._cdPessoaJuridicaContrato;
    } //-- long getCdPessoaJuridicaContrato() 

    /**
     * Returns the value of field 'cdSituacaoContratoNegocio'.
     * 
     * @return int
     * @return the value of field 'cdSituacaoContratoNegocio'.
     */
    public int getCdSituacaoContratoNegocio()
    {
        return this._cdSituacaoContratoNegocio;
    } //-- int getCdSituacaoContratoNegocio() 

    /**
     * Returns the value of field 'cdSituacaoParticipanteContrato'.
     * 
     * @return int
     * @return the value of field 'cdSituacaoParticipanteContrato'.
     */
    public int getCdSituacaoParticipanteContrato()
    {
        return this._cdSituacaoParticipanteContrato;
    } //-- int getCdSituacaoParticipanteContrato() 

    /**
     * Returns the value of field 'cdTipoContratoNegocio'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoNegocio'.
     */
    public int getCdTipoContratoNegocio()
    {
        return this._cdTipoContratoNegocio;
    } //-- int getCdTipoContratoNegocio() 

    /**
     * Returns the value of field 'cdTipoParticipacaoPessoa'.
     * 
     * @return int
     * @return the value of field 'cdTipoParticipacaoPessoa'.
     */
    public int getCdTipoParticipacaoPessoa()
    {
        return this._cdTipoParticipacaoPessoa;
    } //-- int getCdTipoParticipacaoPessoa() 

    /**
     * Returns the value of field 'dsContrato'.
     * 
     * @return String
     * @return the value of field 'dsContrato'.
     */
    public java.lang.String getDsContrato()
    {
        return this._dsContrato;
    } //-- java.lang.String getDsContrato() 

    /**
     * Returns the value of field 'dsPessoaJuridicaContrato'.
     * 
     * @return String
     * @return the value of field 'dsPessoaJuridicaContrato'.
     */
    public java.lang.String getDsPessoaJuridicaContrato()
    {
        return this._dsPessoaJuridicaContrato;
    } //-- java.lang.String getDsPessoaJuridicaContrato() 

    /**
     * Returns the value of field 'dsSituacaoContratoNegocio'.
     * 
     * @return String
     * @return the value of field 'dsSituacaoContratoNegocio'.
     */
    public java.lang.String getDsSituacaoContratoNegocio()
    {
        return this._dsSituacaoContratoNegocio;
    } //-- java.lang.String getDsSituacaoContratoNegocio() 

    /**
     * Returns the value of field 'dsSituacaoParticipanteContrato'.
     * 
     * @return String
     * @return the value of field 'dsSituacaoParticipanteContrato'.
     */
    public java.lang.String getDsSituacaoParticipanteContrato()
    {
        return this._dsSituacaoParticipanteContrato;
    } //-- java.lang.String getDsSituacaoParticipanteContrato() 

    /**
     * Returns the value of field 'dsTipoContratoNegocio'.
     * 
     * @return String
     * @return the value of field 'dsTipoContratoNegocio'.
     */
    public java.lang.String getDsTipoContratoNegocio()
    {
        return this._dsTipoContratoNegocio;
    } //-- java.lang.String getDsTipoContratoNegocio() 

    /**
     * Returns the value of field 'dsTipoParticipacaoPessoa'.
     * 
     * @return String
     * @return the value of field 'dsTipoParticipacaoPessoa'.
     */
    public java.lang.String getDsTipoParticipacaoPessoa()
    {
        return this._dsTipoParticipacaoPessoa;
    } //-- java.lang.String getDsTipoParticipacaoPessoa() 

    /**
     * Returns the value of field 'dtFimParticipacaoContrato'.
     * 
     * @return String
     * @return the value of field 'dtFimParticipacaoContrato'.
     */
    public java.lang.String getDtFimParticipacaoContrato()
    {
        return this._dtFimParticipacaoContrato;
    } //-- java.lang.String getDtFimParticipacaoContrato() 

    /**
     * Returns the value of field 'dtInicioParticipacaoContrato'.
     * 
     * @return String
     * @return the value of field 'dtInicioParticipacaoContrato'.
     */
    public java.lang.String getDtInicioParticipacaoContrato()
    {
        return this._dtInicioParticipacaoContrato;
    } //-- java.lang.String getDtInicioParticipacaoContrato() 

    /**
     * Returns the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @return long
     * @return the value of field 'nrSequenciaContratoNegocio'.
     */
    public long getNrSequenciaContratoNegocio()
    {
        return this._nrSequenciaContratoNegocio;
    } //-- long getNrSequenciaContratoNegocio() 

    /**
     * Method hasCdPessoaJuridicaContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridicaContrato()
    {
        return this._has_cdPessoaJuridicaContrato;
    } //-- boolean hasCdPessoaJuridicaContrato() 

    /**
     * Method hasCdSituacaoContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSituacaoContratoNegocio()
    {
        return this._has_cdSituacaoContratoNegocio;
    } //-- boolean hasCdSituacaoContratoNegocio() 

    /**
     * Method hasCdSituacaoParticipanteContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSituacaoParticipanteContrato()
    {
        return this._has_cdSituacaoParticipanteContrato;
    } //-- boolean hasCdSituacaoParticipanteContrato() 

    /**
     * Method hasCdTipoContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoNegocio()
    {
        return this._has_cdTipoContratoNegocio;
    } //-- boolean hasCdTipoContratoNegocio() 

    /**
     * Method hasCdTipoParticipacaoPessoa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoParticipacaoPessoa()
    {
        return this._has_cdTipoParticipacaoPessoa;
    } //-- boolean hasCdTipoParticipacaoPessoa() 

    /**
     * Method hasNrSequenciaContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaContratoNegocio()
    {
        return this._has_nrSequenciaContratoNegocio;
    } //-- boolean hasNrSequenciaContratoNegocio() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdPessoaJuridicaContrato'.
     * 
     * @param cdPessoaJuridicaContrato the value of field
     * 'cdPessoaJuridicaContrato'.
     */
    public void setCdPessoaJuridicaContrato(long cdPessoaJuridicaContrato)
    {
        this._cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
        this._has_cdPessoaJuridicaContrato = true;
    } //-- void setCdPessoaJuridicaContrato(long) 

    /**
     * Sets the value of field 'cdSituacaoContratoNegocio'.
     * 
     * @param cdSituacaoContratoNegocio the value of field
     * 'cdSituacaoContratoNegocio'.
     */
    public void setCdSituacaoContratoNegocio(int cdSituacaoContratoNegocio)
    {
        this._cdSituacaoContratoNegocio = cdSituacaoContratoNegocio;
        this._has_cdSituacaoContratoNegocio = true;
    } //-- void setCdSituacaoContratoNegocio(int) 

    /**
     * Sets the value of field 'cdSituacaoParticipanteContrato'.
     * 
     * @param cdSituacaoParticipanteContrato the value of field
     * 'cdSituacaoParticipanteContrato'.
     */
    public void setCdSituacaoParticipanteContrato(int cdSituacaoParticipanteContrato)
    {
        this._cdSituacaoParticipanteContrato = cdSituacaoParticipanteContrato;
        this._has_cdSituacaoParticipanteContrato = true;
    } //-- void setCdSituacaoParticipanteContrato(int) 

    /**
     * Sets the value of field 'cdTipoContratoNegocio'.
     * 
     * @param cdTipoContratoNegocio the value of field
     * 'cdTipoContratoNegocio'.
     */
    public void setCdTipoContratoNegocio(int cdTipoContratoNegocio)
    {
        this._cdTipoContratoNegocio = cdTipoContratoNegocio;
        this._has_cdTipoContratoNegocio = true;
    } //-- void setCdTipoContratoNegocio(int) 

    /**
     * Sets the value of field 'cdTipoParticipacaoPessoa'.
     * 
     * @param cdTipoParticipacaoPessoa the value of field
     * 'cdTipoParticipacaoPessoa'.
     */
    public void setCdTipoParticipacaoPessoa(int cdTipoParticipacaoPessoa)
    {
        this._cdTipoParticipacaoPessoa = cdTipoParticipacaoPessoa;
        this._has_cdTipoParticipacaoPessoa = true;
    } //-- void setCdTipoParticipacaoPessoa(int) 

    /**
     * Sets the value of field 'dsContrato'.
     * 
     * @param dsContrato the value of field 'dsContrato'.
     */
    public void setDsContrato(java.lang.String dsContrato)
    {
        this._dsContrato = dsContrato;
    } //-- void setDsContrato(java.lang.String) 

    /**
     * Sets the value of field 'dsPessoaJuridicaContrato'.
     * 
     * @param dsPessoaJuridicaContrato the value of field
     * 'dsPessoaJuridicaContrato'.
     */
    public void setDsPessoaJuridicaContrato(java.lang.String dsPessoaJuridicaContrato)
    {
        this._dsPessoaJuridicaContrato = dsPessoaJuridicaContrato;
    } //-- void setDsPessoaJuridicaContrato(java.lang.String) 

    /**
     * Sets the value of field 'dsSituacaoContratoNegocio'.
     * 
     * @param dsSituacaoContratoNegocio the value of field
     * 'dsSituacaoContratoNegocio'.
     */
    public void setDsSituacaoContratoNegocio(java.lang.String dsSituacaoContratoNegocio)
    {
        this._dsSituacaoContratoNegocio = dsSituacaoContratoNegocio;
    } //-- void setDsSituacaoContratoNegocio(java.lang.String) 

    /**
     * Sets the value of field 'dsSituacaoParticipanteContrato'.
     * 
     * @param dsSituacaoParticipanteContrato the value of field
     * 'dsSituacaoParticipanteContrato'.
     */
    public void setDsSituacaoParticipanteContrato(java.lang.String dsSituacaoParticipanteContrato)
    {
        this._dsSituacaoParticipanteContrato = dsSituacaoParticipanteContrato;
    } //-- void setDsSituacaoParticipanteContrato(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoContratoNegocio'.
     * 
     * @param dsTipoContratoNegocio the value of field
     * 'dsTipoContratoNegocio'.
     */
    public void setDsTipoContratoNegocio(java.lang.String dsTipoContratoNegocio)
    {
        this._dsTipoContratoNegocio = dsTipoContratoNegocio;
    } //-- void setDsTipoContratoNegocio(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoParticipacaoPessoa'.
     * 
     * @param dsTipoParticipacaoPessoa the value of field
     * 'dsTipoParticipacaoPessoa'.
     */
    public void setDsTipoParticipacaoPessoa(java.lang.String dsTipoParticipacaoPessoa)
    {
        this._dsTipoParticipacaoPessoa = dsTipoParticipacaoPessoa;
    } //-- void setDsTipoParticipacaoPessoa(java.lang.String) 

    /**
     * Sets the value of field 'dtFimParticipacaoContrato'.
     * 
     * @param dtFimParticipacaoContrato the value of field
     * 'dtFimParticipacaoContrato'.
     */
    public void setDtFimParticipacaoContrato(java.lang.String dtFimParticipacaoContrato)
    {
        this._dtFimParticipacaoContrato = dtFimParticipacaoContrato;
    } //-- void setDtFimParticipacaoContrato(java.lang.String) 

    /**
     * Sets the value of field 'dtInicioParticipacaoContrato'.
     * 
     * @param dtInicioParticipacaoContrato the value of field
     * 'dtInicioParticipacaoContrato'.
     */
    public void setDtInicioParticipacaoContrato(java.lang.String dtInicioParticipacaoContrato)
    {
        this._dtInicioParticipacaoContrato = dtInicioParticipacaoContrato;
    } //-- void setDtInicioParticipacaoContrato(java.lang.String) 

    /**
     * Sets the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @param nrSequenciaContratoNegocio the value of field
     * 'nrSequenciaContratoNegocio'.
     */
    public void setNrSequenciaContratoNegocio(long nrSequenciaContratoNegocio)
    {
        this._nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
        this._has_nrSequenciaContratoNegocio = true;
    } //-- void setNrSequenciaContratoNegocio(long) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listarcontratosclientemarq.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listarcontratosclientemarq.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listarcontratosclientemarq.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarcontratosclientemarq.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
