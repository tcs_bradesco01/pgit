/*
 * Nome: br.com.bradesco.web.pgit.service.business.listarsolicitcriacaocontatipo.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.listarsolicitcriacaocontatipo.bean;

/**
 * Nome: ListarSolicCriacaoContaTipoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarSolicCriacaoContaTipoSaidaDTO {

	/** Atributo nrSolicitContaDesp. */
	private Long nrSolicitContaDesp;
    
    /** Atributo cdBanco. */
    private Integer cdBanco;
    
    /** Atributo dsBanco. */
    private String dsBanco;
    
    /** Atributo cdAgencia. */
    private Integer cdAgencia;
    
    /** Atributo dsAgencia. */
    private String dsAgencia;
    
    /** Atributo cdGrupoContabil. */
    private Integer cdGrupoContabil;
    
    /** Atributo cdSubGrupoContabil. */
    private Integer cdSubGrupoContabil;
    
    /** Atributo cdConta. */
    private Long cdConta;
    
    /** Atributo cdDigitoConta. */
    private String cdDigitoConta;
    
    /** Atributo cdSituacaoSolicitacao. */
    private Integer cdSituacaoSolicitacao;
    
    /** Atributo dsSituacaoSolicitacao. */
    private String dsSituacaoSolicitacao;
    
    /** Atributo hrInclusaoRegistro. */
    private String hrInclusaoRegistro;
    
    /** Atributo check. */
    private boolean check = false;
    
	/**
	 * Get: cdAgencia.
	 *
	 * @return cdAgencia
	 */
	public Integer getCdAgencia() {
		return cdAgencia;
	}
	
	/**
	 * Set: cdAgencia.
	 *
	 * @param cdAgencia the cd agencia
	 */
	public void setCdAgencia(Integer cdAgencia) {
		this.cdAgencia = cdAgencia;
	}
	
	/**
	 * Get: cdBanco.
	 *
	 * @return cdBanco
	 */
	public Integer getCdBanco() {
		return cdBanco;
	}
	
	/**
	 * Set: cdBanco.
	 *
	 * @param cdBanco the cd banco
	 */
	public void setCdBanco(Integer cdBanco) {
		this.cdBanco = cdBanco;
	}
	
	/**
	 * Get: cdConta.
	 *
	 * @return cdConta
	 */
	public Long getCdConta() {
		return cdConta;
	}
	
	/**
	 * Set: cdConta.
	 *
	 * @param cdConta the cd conta
	 */
	public void setCdConta(Long cdConta) {
		this.cdConta = cdConta;
	}
	
	/**
	 * Get: cdDigitoConta.
	 *
	 * @return cdDigitoConta
	 */
	public String getCdDigitoConta() {
		return cdDigitoConta;
	}
	
	/**
	 * Set: cdDigitoConta.
	 *
	 * @param cdDigitoConta the cd digito conta
	 */
	public void setCdDigitoConta(String cdDigitoConta) {
		this.cdDigitoConta = cdDigitoConta;
	}
	
	/**
	 * Get: cdGrupoContabil.
	 *
	 * @return cdGrupoContabil
	 */
	public Integer getCdGrupoContabil() {
		return cdGrupoContabil;
	}
	
	/**
	 * Set: cdGrupoContabil.
	 *
	 * @param cdGrupoContabil the cd grupo contabil
	 */
	public void setCdGrupoContabil(Integer cdGrupoContabil) {
		this.cdGrupoContabil = cdGrupoContabil;
	}
	
	/**
	 * Get: cdSituacaoSolicitacao.
	 *
	 * @return cdSituacaoSolicitacao
	 */
	public Integer getCdSituacaoSolicitacao() {
		return cdSituacaoSolicitacao;
	}
	
	/**
	 * Set: cdSituacaoSolicitacao.
	 *
	 * @param cdSituacaoSolicitacao the cd situacao solicitacao
	 */
	public void setCdSituacaoSolicitacao(Integer cdSituacaoSolicitacao) {
		this.cdSituacaoSolicitacao = cdSituacaoSolicitacao;
	}
	
	/**
	 * Get: cdSubGrupoContabil.
	 *
	 * @return cdSubGrupoContabil
	 */
	public Integer getCdSubGrupoContabil() {
		return cdSubGrupoContabil;
	}
	
	/**
	 * Set: cdSubGrupoContabil.
	 *
	 * @param cdSubGrupoContabil the cd sub grupo contabil
	 */
	public void setCdSubGrupoContabil(Integer cdSubGrupoContabil) {
		this.cdSubGrupoContabil = cdSubGrupoContabil;
	}
	
	/**
	 * Get: dsAgencia.
	 *
	 * @return dsAgencia
	 */
	public String getDsAgencia() {
		return dsAgencia;
	}
	
	/**
	 * Set: dsAgencia.
	 *
	 * @param dsAgencia the ds agencia
	 */
	public void setDsAgencia(String dsAgencia) {
		this.dsAgencia = dsAgencia;
	}
	
	/**
	 * Get: dsBanco.
	 *
	 * @return dsBanco
	 */
	public String getDsBanco() {
		return dsBanco;
	}
	
	/**
	 * Set: dsBanco.
	 *
	 * @param dsBanco the ds banco
	 */
	public void setDsBanco(String dsBanco) {
		this.dsBanco = dsBanco;
	}
	
	/**
	 * Get: dsSituacaoSolicitacao.
	 *
	 * @return dsSituacaoSolicitacao
	 */
	public String getDsSituacaoSolicitacao() {
		return dsSituacaoSolicitacao;
	}
	
	/**
	 * Set: dsSituacaoSolicitacao.
	 *
	 * @param dsSituacaoSolicitacao the ds situacao solicitacao
	 */
	public void setDsSituacaoSolicitacao(String dsSituacaoSolicitacao) {
		this.dsSituacaoSolicitacao = dsSituacaoSolicitacao;
	}
	
	/**
	 * Get: hrInclusaoRegistro.
	 *
	 * @return hrInclusaoRegistro
	 */
	public String getHrInclusaoRegistro() {
		return hrInclusaoRegistro;
	}
	
	/**
	 * Set: hrInclusaoRegistro.
	 *
	 * @param hrInclusaoRegistro the hr inclusao registro
	 */
	public void setHrInclusaoRegistro(String hrInclusaoRegistro) {
		this.hrInclusaoRegistro = hrInclusaoRegistro;
	}
	
	/**
	 * Get: nrSolicitContaDesp.
	 *
	 * @return nrSolicitContaDesp
	 */
	public Long getNrSolicitContaDesp() {
		return nrSolicitContaDesp;
	}
	
	/**
	 * Set: nrSolicitContaDesp.
	 *
	 * @param nrSolicitContaDesp the nr solicit conta desp
	 */
	public void setNrSolicitContaDesp(Long nrSolicitContaDesp) {
		this.nrSolicitContaDesp = nrSolicitContaDesp;
	}
	
	/**
	 * Is check.
	 *
	 * @return true, if is check
	 */
	public boolean isCheck() {
		return check;
	}
	
	/**
	 * Set: check.
	 *
	 * @param check the check
	 */
	public void setCheck(boolean check) {
		this.check = check;
	}
	
}