/*
 * Nome: br.com.bradesco.web.pgit.service.business.consultasolicitacaoestornoop.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.consultasolicitacaoestornoop.bean;

import java.util.Date;


/**
 * Nome: ConsultaSolicitacaoEstornoOPEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultaSolicitacaoEstornoOPEntradaDTO {
	
	/** Atributo cdpessoaJuridicaContrato. */
	private Long cdpessoaJuridicaContrato;
	
	/** Atributo cdTipoContratoNegocio. */
	private Integer cdTipoContratoNegocio;
	
	/** Atributo nrSequenciaContratoNegocio. */
	private Long nrSequenciaContratoNegocio;
	
	/** Atributo dtSolicitacaoInicio. */
	private Date dtSolicitacaoInicio;
	
	/** Atributo dtSolicitacaoFim. */
	private Date dtSolicitacaoFim;
	
	/** Atributo cdSituacaoSolicitacaoPagamento. */
	private Integer cdSituacaoSolicitacaoPagamento;
	
	/** Atributo cdMotivoSituacaoSolicitacao. */
	private Integer cdMotivoSituacaoSolicitacao;
	
	/** Atributo cdModalidadePagamentoCliente. */
	private Integer cdModalidadePagamentoCliente;
	
	/** Atributo cdProdutoServicoOperacao. */
	private Integer cdProdutoServicoOperacao;
	 
	/**
	 * Get: cdpessoaJuridicaContrato.
	 *
	 * @return cdpessoaJuridicaContrato
	 */
	public Long getCdpessoaJuridicaContrato() {
		return cdpessoaJuridicaContrato;
	}
	
	/**
	 * Set: cdpessoaJuridicaContrato.
	 *
	 * @param cdpessoaJuridicaContrato the cdpessoa juridica contrato
	 */
	public void setCdpessoaJuridicaContrato(Long cdpessoaJuridicaContrato) {
		this.cdpessoaJuridicaContrato = cdpessoaJuridicaContrato;
	}
	
	/**
	 * Get: cdTipoContratoNegocio.
	 *
	 * @return cdTipoContratoNegocio
	 */
	public Integer getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}
	
	/**
	 * Set: cdTipoContratoNegocio.
	 *
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}
	
	/**
	 * Get: nrSequenciaContratoNegocio.
	 *
	 * @return nrSequenciaContratoNegocio
	 */
	public Long getNrSequenciaContratoNegocio() {
		return nrSequenciaContratoNegocio;
	}
	
	/**
	 * Set: nrSequenciaContratoNegocio.
	 *
	 * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
	 */
	public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}
	
	/**
	 * Get: dtSolicitacaoInicio.
	 *
	 * @return dtSolicitacaoInicio
	 */
	public Date getDtSolicitacaoInicio() {
		return dtSolicitacaoInicio;
	}
	
	/**
	 * Set: dtSolicitacaoInicio.
	 *
	 * @param dtSolicitacaoInicio the dt solicitacao inicio
	 */
	public void setDtSolicitacaoInicio(Date dtSolicitacaoInicio) {
		this.dtSolicitacaoInicio = dtSolicitacaoInicio;
	}
	
	/**
	 * Get: dtSolicitacaoFim.
	 *
	 * @return dtSolicitacaoFim
	 */
	public Date getDtSolicitacaoFim() {
		return dtSolicitacaoFim;
	}
	
	/**
	 * Set: dtSolicitacaoFim.
	 *
	 * @param dtSolicitacaoFim the dt solicitacao fim
	 */
	public void setDtSolicitacaoFim(Date dtSolicitacaoFim) {
		this.dtSolicitacaoFim = dtSolicitacaoFim;
	}
	
	/**
	 * Get: cdSituacaoSolicitacaoPagamento.
	 *
	 * @return cdSituacaoSolicitacaoPagamento
	 */
	public Integer getCdSituacaoSolicitacaoPagamento() {
		return cdSituacaoSolicitacaoPagamento;
	}
	
	/**
	 * Set: cdSituacaoSolicitacaoPagamento.
	 *
	 * @param cdSituacaoSolicitacaoPagamento the cd situacao solicitacao pagamento
	 */
	public void setCdSituacaoSolicitacaoPagamento(Integer cdSituacaoSolicitacaoPagamento) {
		this.cdSituacaoSolicitacaoPagamento = cdSituacaoSolicitacaoPagamento;
	}
	
	/**
	 * Get: cdMotivoSituacaoSolicitacao.
	 *
	 * @return cdMotivoSituacaoSolicitacao
	 */
	public Integer getCdMotivoSituacaoSolicitacao() {
		return cdMotivoSituacaoSolicitacao;
	}
	
	/**
	 * Set: cdMotivoSituacaoSolicitacao.
	 *
	 * @param cdMotivoSituacaoSolicitacao the cd motivo situacao solicitacao
	 */
	public void setCdMotivoSituacaoSolicitacao(Integer cdMotivoSituacaoSolicitacao) {
		this.cdMotivoSituacaoSolicitacao = cdMotivoSituacaoSolicitacao;
	}
	
	/**
	 * Get: cdModalidadePagamentoCliente.
	 *
	 * @return cdModalidadePagamentoCliente
	 */
	public Integer getCdModalidadePagamentoCliente() {
		return cdModalidadePagamentoCliente;
	}
	
	/**
	 * Set: cdModalidadePagamentoCliente.
	 *
	 * @param cdModalidadePagamentoCliente the cd modalidade pagamento cliente
	 */
	public void setCdModalidadePagamentoCliente(Integer cdModalidadePagamentoCliente) {
		this.cdModalidadePagamentoCliente = cdModalidadePagamentoCliente;
	}
	
	/**
	 * Get: cdProdutoServicoOperacao.
	 *
	 * @return cdProdutoServicoOperacao
	 */
	public Integer getCdProdutoServicoOperacao() {
		return cdProdutoServicoOperacao;
	}
	
	/**
	 * Set: cdProdutoServicoOperacao.
	 *
	 * @param cdProdutoServicoOperacao the cd produto servico operacao
	 */
	public void setCdProdutoServicoOperacao(Integer cdProdutoServicoOperacao) {
		this.cdProdutoServicoOperacao = cdProdutoServicoOperacao;
	}
}