/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/interfaz.ftl,v $
 * $Id: interfaz.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: interfaz.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.arquivoremessa;

import java.util.List;

import br.com.bradesco.web.pgit.service.business.arquivoremessa.bean.ConsultarArquivoRecuperacaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.arquivoremessa.bean.ConsultarArquivoRecuperacaoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.arquivoremessa.bean.ConsultarSolicRecuperacaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.arquivoremessa.bean.ConsultarSolicRecuperacaoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.arquivoremessa.bean.DetalharSolicRecuperacaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.arquivoremessa.bean.DetalharSolicRecuperacaoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.arquivoremessa.bean.ExcluirSolicRecuperacaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.arquivoremessa.bean.ExcluirSolicRecuperacaoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.arquivoremessa.bean.IncluirSolicRecuperacaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.arquivoremessa.bean.IncluirSolicRecuperacaoSaidaDTO;

/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Interface do adaptador: ArquivoRemessa
 * </p>
 * 
 * @comment CODIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public interface IArquivoRemessaService {

    /**
     * Consultar solic recuperacao.
     *
     * @param consultarSolicRecuperacaoEntradaDTO the consultar solic recuperacao entrada dto
     * @return the list< consultar solic recuperacao saida dt o>
     */
    List<ConsultarSolicRecuperacaoSaidaDTO> consultarSolicRecuperacao(ConsultarSolicRecuperacaoEntradaDTO consultarSolicRecuperacaoEntradaDTO);
    
    /**
     * Detalhar solic recuperacao.
     *
     * @param detalharSolicRecuperacaoEntradaDTO the detalhar solic recuperacao entrada dto
     * @return the detalhar solic recuperacao saida dto
     */
    DetalharSolicRecuperacaoSaidaDTO detalharSolicRecuperacao(DetalharSolicRecuperacaoEntradaDTO detalharSolicRecuperacaoEntradaDTO);
    
    /**
     * Consultar arquivo recuperacao.
     *
     * @param consultarArquivoRecuperacaoEntradaDTO the consultar arquivo recuperacao entrada dto
     * @return the list< consultar arquivo recuperacao saida dt o>
     */
    List<ConsultarArquivoRecuperacaoSaidaDTO> consultarArquivoRecuperacao(ConsultarArquivoRecuperacaoEntradaDTO consultarArquivoRecuperacaoEntradaDTO);
    
    /**
     * Incluir solic recuperacao.
     *
     * @param incluirSolicRecuperacaoEntradaDTO the incluir solic recuperacao entrada dto
     * @return the incluir solic recuperacao saida dto
     */
    IncluirSolicRecuperacaoSaidaDTO incluirSolicRecuperacao(IncluirSolicRecuperacaoEntradaDTO incluirSolicRecuperacaoEntradaDTO);    
    
    /**
     * Excluir solic recuperacao.
     *
     * @param excluirSolicRecuperacaoEntradaDTO the excluir solic recuperacao entrada dto
     * @return the excluir solic recuperacao saida dto
     */
    ExcluirSolicRecuperacaoSaidaDTO excluirSolicRecuperacao(ExcluirSolicRecuperacaoEntradaDTO excluirSolicRecuperacaoEntradaDTO);
    
    

}

