/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/implementation.ftl,v $
 * $Id: implementation.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: implementation.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.mantercadastroterceiroparticipante.impl;

import java.util.ArrayList;
import java.util.List;

import br.com.bradesco.web.pgit.service.business.mantercadastroterceiroparticipante.IManterCadastroTerceiroParticipanteService;
import br.com.bradesco.web.pgit.service.business.mantercadastroterceiroparticipante.IManterCadastroTerceiroParticipanteServiceConstants;
import br.com.bradesco.web.pgit.service.business.mantercadastroterceiroparticipante.bean.AlterarTercParticiContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadastroterceiroparticipante.bean.AlterarTercParticiContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadastroterceiroparticipante.bean.ConsultarTercPartContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadastroterceiroparticipante.bean.ConsultarTercPartContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadastroterceiroparticipante.bean.ExcluirTerceiroPartContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadastroterceiroparticipante.bean.ExcluirTerceiroPartContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadastroterceiroparticipante.bean.IncluirTerceiroPartContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadastroterceiroparticipante.bean.IncluirTerceiroPartContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadastroterceiroparticipante.bean.ListarTercParticipanteContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadastroterceiroparticipante.bean.ListarTercParticipanteContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadastroterceiroparticipante.bean.OcorrenciasListarTercParticipanteContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.data.pdc.FactoryAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.alterartercparticicontrato.request.AlterarTercParticiContratoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.alterartercparticicontrato.response.AlterarTercParticiContratoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.consultartercpartcontrato.request.ConsultarTercPartContratoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultartercpartcontrato.response.ConsultarTercPartContratoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.excluirterceiropartcontrato.request.ExcluirTerceiroPartContratoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.excluirterceiropartcontrato.response.ExcluirTerceiroPartContratoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.incluirterceiropartcontrato.request.IncluirTerceiroPartContratoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.incluirterceiropartcontrato.response.IncluirTerceiroPartContratoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listartercparticipantecontrato.request.ListarTercParticipanteContratoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listartercparticipantecontrato.response.ListarTercParticipanteContratoResponse;
import br.com.bradesco.web.pgit.utils.CpfCnpjUtils;
import br.com.bradesco.web.pgit.utils.PgitUtil;

/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Implementa��o do adaptador: ManterCadastroTerceiroParticipante
 * </p>
 * 
 * @comment C�DIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public class ManterCadastroTerceiroParticipanteServiceImpl implements IManterCadastroTerceiroParticipanteService {

    /** Atributo factoryAdapter. */
    private FactoryAdapter factoryAdapter;

    /**
     * Get: factoryAdapter.
     *
     * @return factoryAdapter
     */
    public FactoryAdapter getFactoryAdapter() {
	return factoryAdapter;
    }

    /**
     * Set: factoryAdapter.
     *
     * @param factoryAdapter the factory adapter
     */
    public void setFactoryAdapter(FactoryAdapter factoryAdapter) {
	this.factoryAdapter = factoryAdapter;
    }

    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.mantercadastroterceiroparticipante.IManterCadastroTerceiroParticipanteService#listarTercParticipanteContrato(br.com.bradesco.web.pgit.service.business.mantercadastroterceiroparticipante.bean.ListarTercParticipanteContratoEntradaDTO)
     */
    public ListarTercParticipanteContratoSaidaDTO listarTercParticipanteContrato(ListarTercParticipanteContratoEntradaDTO entrada) {
	ListarTercParticipanteContratoSaidaDTO saida = new ListarTercParticipanteContratoSaidaDTO();
	ListarTercParticipanteContratoRequest request = new ListarTercParticipanteContratoRequest();
	ListarTercParticipanteContratoResponse response = new ListarTercParticipanteContratoResponse();

	request.setQtdeConsultas(IManterCadastroTerceiroParticipanteServiceConstants.NUMERO_OCORRENCIAS_CONSULTAR);
	request.setCdpessoaJuridicaContrato(PgitUtil.verificaLongNulo(entrada.getCdpessoaJuridicaContrato()));
	request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(entrada.getCdTipoContratoNegocio()));
	request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(entrada.getNrSequenciaContratoNegocio()));
	request.setCdTipoParticipacaoPessoa(PgitUtil.verificaIntegerNulo(entrada.getCdTipoParticipacaoPessoa()));
	request.setCdPessoa(PgitUtil.verificaLongNulo(entrada.getCdPessoa()));
	request.setCdCpfCnpjTerceiro(PgitUtil.verificaLongNulo(entrada.getCdCpfCnpjTerceiro()));
	request.setCdFilialCnpjTerceiro(PgitUtil.verificaIntegerNulo(entrada.getCdFilialCnpjTerceiro()));
	request.setCdControleCnpjTerceiro(PgitUtil.verificaIntegerNulo(entrada.getCdControleCnpjTerceiro()));
	request.setNrRastreaTerceiro(PgitUtil.verificaIntegerNulo(entrada.getNrRastreaTerceiro()));

	response = getFactoryAdapter().getListarTercParticipanteContratoPDCAdapter().invokeProcess(request);

	saida.setCodMensagem(response.getCodMensagem());
	saida.setMensagem(response.getMensagem());
	saida.setNumeroLinhas(response.getNumeroLinhas());

	List<OcorrenciasListarTercParticipanteContratoSaidaDTO> listaOcorrencias = new ArrayList<OcorrenciasListarTercParticipanteContratoSaidaDTO>();
	for (int i = 0; i < response.getOcorrenciasCount(); i++) {
	    OcorrenciasListarTercParticipanteContratoSaidaDTO ocorrencias = new OcorrenciasListarTercParticipanteContratoSaidaDTO();
	    ocorrencias.setCdCpfCnpjTerceiro(response.getOcorrencias(i).getCdCpfCnpjTerceiro());
	    ocorrencias.setCdFilialCnpjTerceiro(response.getOcorrencias(i).getCdFilialCnpjTerceiro());
	    ocorrencias.setCdControleCnpjTerceiro(response.getOcorrencias(i).getCdControleCnpjTerceiro());
	    ocorrencias.setNrRastreaTerceiro(response.getOcorrencias(i).getNrRastreaTerceiro());
	    ocorrencias.setDtInicioRastreaTerceiro(response.getOcorrencias(i).getDtInicioRastreaTerceiro().replace(".", "/"));
	    ocorrencias.setCpfCnpjFormatado(CpfCnpjUtils.formatCpfCnpjCompleto(response.getOcorrencias(i).getCdCpfCnpjTerceiro(), response
		    .getOcorrencias(i).getCdFilialCnpjTerceiro(), response.getOcorrencias(i).getCdControleCnpjTerceiro()));

	    listaOcorrencias.add(ocorrencias);
	}
	saida.setListaOcorrencias(listaOcorrencias);

	return saida;
    }

    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.mantercadastroterceiroparticipante.IManterCadastroTerceiroParticipanteService#incluirTerceiroPartContrato(br.com.bradesco.web.pgit.service.business.mantercadastroterceiroparticipante.bean.IncluirTerceiroPartContratoEntradaDTO)
     */
    public IncluirTerceiroPartContratoSaidaDTO incluirTerceiroPartContrato(IncluirTerceiroPartContratoEntradaDTO entrada) {
	IncluirTerceiroPartContratoSaidaDTO saida = new IncluirTerceiroPartContratoSaidaDTO();
	IncluirTerceiroPartContratoRequest request = new IncluirTerceiroPartContratoRequest();
	IncluirTerceiroPartContratoResponse response = new IncluirTerceiroPartContratoResponse();

	request.setCdpessoaJuridicaContrato(PgitUtil.verificaLongNulo(entrada.getCdpessoaJuridicaContrato()));
	request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(entrada.getCdTipoContratoNegocio()));
	request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(entrada.getNrSequenciaContratoNegocio()));
	request.setCdTipoParticipacaoPessoa(PgitUtil.verificaIntegerNulo(entrada.getCdTipoParticipacaoPessoa()));
	request.setCdPessoa(PgitUtil.verificaLongNulo(entrada.getCdPessoa()));
	request.setCdCpfCnpjTerceiro(PgitUtil.verificaLongNulo(entrada.getCdCpfCnpjTerceiro()));
	request.setCdFilialCnpjTerceiro(PgitUtil.verificaIntegerNulo(entrada.getCdFilialCnpjTerceiro()));
	request.setCdControleCnpjTerceiro(PgitUtil.verificaIntegerNulo(entrada.getCdControleCnpjTerceiro()));
	request.setCdIndicadorAgendaTitulo(PgitUtil.verificaIntegerNulo(entrada.getCdIndicadorAgendaTitulo()));
	request.setCdIndicadorTerceiro(PgitUtil.verificaIntegerNulo(entrada.getCdIndicadorTerceiro()));
	request.setCdIndicadorPapeletaTerceiro(PgitUtil.verificaIntegerNulo(entrada.getCdIndicadorPapeletaTerceiro()));
	request.setDtInicioRastreaTerceiro(PgitUtil.verificaStringNula(entrada.getDtInicioRastreaTerceiro()));
	request.setDtInicioPapeletaTerceiro(PgitUtil.verificaStringNula(entrada.getDtInicioPapeletaTerceiro()));

	response = getFactoryAdapter().getIncluirTerceiroPartContratoPDCAdapter().invokeProcess(request);

	saida.setCodMensagem(response.getCodMensagem());
	saida.setMensagem(response.getMensagem());

	return saida;
    }

    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.mantercadastroterceiroparticipante.IManterCadastroTerceiroParticipanteService#excluirTerceiroPartContrato(br.com.bradesco.web.pgit.service.business.mantercadastroterceiroparticipante.bean.ExcluirTerceiroPartContratoEntradaDTO)
     */
    public ExcluirTerceiroPartContratoSaidaDTO excluirTerceiroPartContrato(ExcluirTerceiroPartContratoEntradaDTO entrada) {
	ExcluirTerceiroPartContratoSaidaDTO saida = new ExcluirTerceiroPartContratoSaidaDTO();
	ExcluirTerceiroPartContratoRequest request = new ExcluirTerceiroPartContratoRequest();
	ExcluirTerceiroPartContratoResponse response = new ExcluirTerceiroPartContratoResponse();

	request.setCdpessoaJuridicaContrato(PgitUtil.verificaLongNulo(entrada.getCdpessoaJuridicaContrato()));
	request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(entrada.getCdTipoContratoNegocio()));
	request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(entrada.getNrSequenciaContratoNegocio()));
	request.setCdTipoParticipacaoPessoa(PgitUtil.verificaIntegerNulo(entrada.getCdTipoParticipacaoPessoa()));
	request.setCdPessoa(PgitUtil.verificaLongNulo(entrada.getCdPessoa()));
	request.setNrRastreaTerceiro(PgitUtil.verificaIntegerNulo(entrada.getNrRastreaTerceiro()));

	response = getFactoryAdapter().getExcluirTerceiroPartContratoPDCAdapter().invokeProcess(request);

	saida.setCodMensagem(response.getCodMensagem());
	saida.setMensagem(response.getMensagem());

	return saida;
    }

    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.mantercadastroterceiroparticipante.IManterCadastroTerceiroParticipanteService#consultarTercPartContrato(br.com.bradesco.web.pgit.service.business.mantercadastroterceiroparticipante.bean.ConsultarTercPartContratoEntradaDTO)
     */
    public ConsultarTercPartContratoSaidaDTO consultarTercPartContrato(ConsultarTercPartContratoEntradaDTO entrada) {
	ConsultarTercPartContratoSaidaDTO saida = new ConsultarTercPartContratoSaidaDTO();
	ConsultarTercPartContratoRequest request = new ConsultarTercPartContratoRequest();
	ConsultarTercPartContratoResponse response = new ConsultarTercPartContratoResponse();

	request.setCdpessoaJuridicaContrato(PgitUtil.verificaLongNulo(entrada.getCdpessoaJuridicaContrato()));
	request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(entrada.getCdTipoContratoNegocio()));
	request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(entrada.getNrSequenciaContratoNegocio()));
	request.setCdTipoParticipacaoPessoa(PgitUtil.verificaIntegerNulo(entrada.getCdTipoParticipacaoPessoa()));
	request.setCdPessoa(PgitUtil.verificaLongNulo(entrada.getCdPessoa()));
	request.setNrRastreaTerceiro(PgitUtil.verificaIntegerNulo(entrada.getNrRastreaTerceiro()));
	request.setCdCpfCnpjTerceiro(PgitUtil.verificaLongNulo(entrada.getCdCpfCnpjTerceiro()));
	request.setCdFilialCnpjTerceiro(PgitUtil.verificaIntegerNulo(entrada.getCdFilialCnpjTerceiro()));
	request.setCdControleCnpjTerceiro(PgitUtil.verificaIntegerNulo(entrada.getCdControleCnpjTerceiro()));

	response = getFactoryAdapter().getConsultarTercPartContratoPDCAdapter().invokeProcess(request);

	saida.setCodMensagem(response.getCodMensagem());
	saida.setMensagem(response.getMensagem());
	saida.setCdpessoaJuridicaContrato(response.getCdpessoaJuridicaContrato());
	saida.setCdTipoContratoNegocio(response.getCdTipoContratoNegocio());
	saida.setNrSequenciaContratoNegocio(response.getNrSequenciaContratoNegocio());
	saida.setCdTipoParticipacaoPessoa(response.getCdTipoParticipacaoPessoa());
	saida.setCdPessoa(response.getCdPessoa());
	saida.setNrRastreaTerceiro(response.getNrRastreaTerceiro());
	saida.setCdCpfCnpjTerceiro(response.getCdCpfCnpjTerceiro());
	saida.setCdFilialCnpjTerceiro(response.getCdFilialCnpjTerceiro());
	saida.setCdControleCnpjTerceiro(response.getCdControleCnpjTerceiro());
	saida.setCdIndicadorAgendaTitulo(response.getCdIndicadorAgendaTitulo());
	saida.setCdIndicadorNotaTerceiro(response.getCdIndicadorNotaTerceiro());
	saida.setCdIndicadorPapeletaTerceiro(response.getCdIndicadorPapeletaTerceiro());
	saida.setDtInicioRastreaTerceiro(response.getDtInicioRastreaTerceiro());
	saida.setDtInicioPapeletaTerceiro(response.getDtInicioPapeletaTerceiro());
	saida.setCdCanalInclusao(response.getCdCanalInclusao());
	saida.setDsCanalInclusao(response.getDsCanalInclusao());
	saida.setCdAutenticacaoSegurancaInclusao(response.getCdAutenticacaoSegurancaInclusao());
	saida.setNmOperacaoFluxoInclusao(response.getNmOperacaoFluxoInclusao());
	saida.setHrInclusaoRegistro(response.getHrInclusaoRegistro());
	saida.setCdCanalManutencao(response.getCdCanalManutencao());
	saida.setDsCanalManutencao(response.getDsCanalManutencao());
	saida.setCdAutenticacaoSegurancaManutencao(response.getCdAutenticacaoSegurancaManutencao());
	saida.setNmOperacaoFluxoManutencao(response.getNmOperacaoFluxoManutencao());
	saida.setHrManutencaoRegistro(response.getHrManutencaoRegistro());
	saida.setCdIndicadorBloqueioRastreabilidade(response.getCdIndicadorBloqueioRastreabilidade());
	saida.setDsIndicadorBloqueioRastreabilidade(response.getDsIndicadorBloqueioRastreabilidade());

	return saida;
    }

    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.mantercadastroterceiroparticipante.IManterCadastroTerceiroParticipanteService#alterarTercParticiContrato(br.com.bradesco.web.pgit.service.business.mantercadastroterceiroparticipante.bean.AlterarTercParticiContratoEntradaDTO)
     */
    public AlterarTercParticiContratoSaidaDTO alterarTercParticiContrato(AlterarTercParticiContratoEntradaDTO entrada) {
	AlterarTercParticiContratoSaidaDTO saida = new AlterarTercParticiContratoSaidaDTO();
	AlterarTercParticiContratoRequest request = new AlterarTercParticiContratoRequest();
	AlterarTercParticiContratoResponse response = new AlterarTercParticiContratoResponse();

	request.setCdpessoaJuridicaContrato(PgitUtil.verificaLongNulo(entrada.getCdpessoaJuridicaContrato()));
	request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(entrada.getCdTipoContratoNegocio()));
	request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(entrada.getNrSequenciaContratoNegocio()));
	request.setCdTipoParticipacaoPessoa(PgitUtil.verificaIntegerNulo(entrada.getCdTipoParticipacaoPessoa()));
	request.setCdPessoa(PgitUtil.verificaLongNulo(entrada.getCdPessoa()));
	request.setNrRastreaTerceiro(PgitUtil.verificaIntegerNulo(entrada.getNrRastreaTerceiro()));
	request.setCdCpfCnpjTerceiro(PgitUtil.verificaLongNulo(entrada.getCdCpfCnpjTerceiro()));
	request.setCdFilialCnpjTerceiro(PgitUtil.verificaIntegerNulo(entrada.getCdFilialCnpjTerceiro()));
	request.setCdControleCnpjTerceiro(PgitUtil.verificaIntegerNulo(entrada.getCdControleCnpjTerceiro()));
	request.setCdIndicadorAgendaTitulo(PgitUtil.verificaIntegerNulo(entrada.getCdIndicadorAgendaTitulo()));
	request.setCdIndicadorTerceiro(PgitUtil.verificaIntegerNulo(entrada.getCdIndicadorTerceiro()));
	request.setCdIndicadorPapeletaTerceiro(PgitUtil.verificaIntegerNulo(entrada.getCdIndicadorPapeletaTerceiro()));
	request.setDtInicioRastreaTerceiro(PgitUtil.verificaStringNula(entrada.getDtInicioRastreaTerceiro()));
	request.setDtInicioPapeletaTerceiro(PgitUtil.verificaStringNula(entrada.getDtInicioPapeletaTerceiro()));
	request.setCdIndicadorBloqueioRastreabilidade(entrada.getCdIndicadorBloqueioRastreabilidade());
	request.setCdIndicadorBloqueioPapeleta(entrada.getDsIndicadorBloqueioRastreabilidade());

	response = getFactoryAdapter().getAlterarTercParticiContratoPDCAdapter().invokeProcess(request);

	saida.setCodMensagem(response.getCodMensagem());
	saida.setMensagem(response.getMensagem());

	return saida;
    }

}
