/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/interfaz.ftl,v $
 * $Id: interfaz.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: interfaz.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.assoclotelayarqprodserv;

import java.util.List;

import br.com.bradesco.web.pgit.service.business.assoclotelayarqprodserv.bean.DetalharAsLotLaArProdServEntradaDTO;
import br.com.bradesco.web.pgit.service.business.assoclotelayarqprodserv.bean.DetalharAsLotLaArProdServSaidaDTO;
import br.com.bradesco.web.pgit.service.business.assoclotelayarqprodserv.bean.ExcluirAsLotLaArProdServEntradaDTO;
import br.com.bradesco.web.pgit.service.business.assoclotelayarqprodserv.bean.ExcluirAsLotLaArProdServSaidaDTO;
import br.com.bradesco.web.pgit.service.business.assoclotelayarqprodserv.bean.IncluirAsLotLaArProdServEntradaDTO;
import br.com.bradesco.web.pgit.service.business.assoclotelayarqprodserv.bean.IncluirAsLotLaArProdServSaidaDTO;
import br.com.bradesco.web.pgit.service.business.assoclotelayarqprodserv.bean.ListarAsLotLaArProdServEntradaDTO;
import br.com.bradesco.web.pgit.service.business.assoclotelayarqprodserv.bean.ListarAsLotLaArProdServSaidaDTO;


/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Interface do adaptador: AssocLoteOperacaoProdServ
 * </p>
 * 
 * @comment CODIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public interface IAssocLoteLayArqProdServService {

	/**
	 * Listar assoc lote lay ar prod serv.
	 *
	 * @param listarAsLotLaArProdServEntradaDTO the listar as lot la ar prod serv entrada dto
	 * @return the list< listar as lot la ar prod serv saida dt o>
	 */
	List<ListarAsLotLaArProdServSaidaDTO> listarAssocLoteLayArProdServ (ListarAsLotLaArProdServEntradaDTO listarAsLotLaArProdServEntradaDTO);
	
	/**
	 * Incluir assoc lote lay ar prod serv.
	 *
	 * @param incluirAsLotLaArProdServEntradaDTO the incluir as lot la ar prod serv entrada dto
	 * @return the incluir as lot la ar prod serv saida dto
	 */
	IncluirAsLotLaArProdServSaidaDTO incluirAssocLoteLayArProdServ (IncluirAsLotLaArProdServEntradaDTO incluirAsLotLaArProdServEntradaDTO);
	
	/**
	 * Excluir assoc lote lay ar prod serv.
	 *
	 * @param excluirAsLotLaArProdServEntradaDTO the excluir as lot la ar prod serv entrada dto
	 * @return the excluir as lot la ar prod serv saida dto
	 */
	ExcluirAsLotLaArProdServSaidaDTO excluirAssocLoteLayArProdServ (ExcluirAsLotLaArProdServEntradaDTO excluirAsLotLaArProdServEntradaDTO);
	
	/**
	 * Detalhar assoc lote lay ar prod serv.
	 *
	 * @param detalharAsLotLaArProdServEntradaDTO the detalhar as lot la ar prod serv entrada dto
	 * @return the detalhar as lot la ar prod serv saida dto
	 */
	DetalharAsLotLaArProdServSaidaDTO detalharAssocLoteLayArProdServ (DetalharAsLotLaArProdServEntradaDTO detalharAsLotLaArProdServEntradaDTO);
	
	/**
	 * Sample assoc lote operacao prod serv.
	 */
	void sampleAssocLoteOperacaoProdServ();

}

