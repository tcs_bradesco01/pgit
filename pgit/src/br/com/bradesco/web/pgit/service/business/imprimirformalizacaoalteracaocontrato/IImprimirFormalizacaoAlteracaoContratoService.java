/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/interfaz.ftl,v $
 * $Id: interfaz.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: interfaz.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.imprimirformalizacaoalteracaocontrato;

import java.io.OutputStream;

import br.com.bradesco.web.pgit.service.business.imprimirformalizacaoalteracaocontrato.bean.ImprimirAditivoContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.imprimirformalizacaoalteracaocontrato.bean.ImprimirAditivoContratoSaidaDTO;

import com.lowagie.text.DocumentException;


/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Interface do adaptador: ImprimirFormalizacaoAlteracaoContrato
 * </p>
 * 
 * @comment CODIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public interface IImprimirFormalizacaoAlteracaoContratoService {

    /**
     * M�todo de exemplo.
     */
    void sampleImprimirFormalizacaoAlteracaoContrato();
    
    /**
     * Imprimir aditivo contrato.
     *
     * @param imprimirAditivoContratoEntradaDTO the imprimir aditivo contrato entrada dto
     * @return the imprimir aditivo contrato saida dto
     */
    ImprimirAditivoContratoSaidaDTO imprimirAditivoContrato (ImprimirAditivoContratoEntradaDTO imprimirAditivoContratoEntradaDTO);
    
    /**
     * Gerar contrato aditivo.
     *
     * @param saidaContratoAditivo the saida contrato aditivo
     * @param outputStream the output stream
     * @throws DocumentException the document exception
     */
    void gerarContratoAditivo(ImprimirAditivoContratoSaidaDTO saidaContratoAditivo, OutputStream outputStream) throws DocumentException;
}

