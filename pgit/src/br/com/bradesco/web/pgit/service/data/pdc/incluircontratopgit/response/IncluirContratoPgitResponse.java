/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.incluircontratopgit.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class IncluirContratoPgitResponse.
 * 
 * @version $Revision$ $Date$
 */
public class IncluirContratoPgitResponse implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _codMensagem
     */
    private java.lang.String _codMensagem;

    /**
     * Field _mensagem
     */
    private java.lang.String _mensagem;

    /**
     * Field _cdRetorno
     */
    private int _cdRetorno = 0;

    /**
     * keeps track of state for field: _cdRetorno
     */
    private boolean _has_cdRetorno;

    /**
     * Field _cdFaseSaida
     */
    private int _cdFaseSaida = 0;

    /**
     * keeps track of state for field: _cdFaseSaida
     */
    private boolean _has_cdFaseSaida;

    /**
     * Field _nrSequenciaContratoNegocio
     */
    private long _nrSequenciaContratoNegocio = 0;

    /**
     * keeps track of state for field: _nrSequenciaContratoNegocio
     */
    private boolean _has_nrSequenciaContratoNegocio;


      //----------------/
     //- Constructors -/
    //----------------/

    public IncluirContratoPgitResponse() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.incluircontratopgit.response.IncluirContratoPgitResponse()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdFaseSaida
     * 
     */
    public void deleteCdFaseSaida()
    {
        this._has_cdFaseSaida= false;
    } //-- void deleteCdFaseSaida() 

    /**
     * Method deleteCdRetorno
     * 
     */
    public void deleteCdRetorno()
    {
        this._has_cdRetorno= false;
    } //-- void deleteCdRetorno() 

    /**
     * Method deleteNrSequenciaContratoNegocio
     * 
     */
    public void deleteNrSequenciaContratoNegocio()
    {
        this._has_nrSequenciaContratoNegocio= false;
    } //-- void deleteNrSequenciaContratoNegocio() 

    /**
     * Returns the value of field 'cdFaseSaida'.
     * 
     * @return int
     * @return the value of field 'cdFaseSaida'.
     */
    public int getCdFaseSaida()
    {
        return this._cdFaseSaida;
    } //-- int getCdFaseSaida() 

    /**
     * Returns the value of field 'cdRetorno'.
     * 
     * @return int
     * @return the value of field 'cdRetorno'.
     */
    public int getCdRetorno()
    {
        return this._cdRetorno;
    } //-- int getCdRetorno() 

    /**
     * Returns the value of field 'codMensagem'.
     * 
     * @return String
     * @return the value of field 'codMensagem'.
     */
    public java.lang.String getCodMensagem()
    {
        return this._codMensagem;
    } //-- java.lang.String getCodMensagem() 

    /**
     * Returns the value of field 'mensagem'.
     * 
     * @return String
     * @return the value of field 'mensagem'.
     */
    public java.lang.String getMensagem()
    {
        return this._mensagem;
    } //-- java.lang.String getMensagem() 

    /**
     * Returns the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @return long
     * @return the value of field 'nrSequenciaContratoNegocio'.
     */
    public long getNrSequenciaContratoNegocio()
    {
        return this._nrSequenciaContratoNegocio;
    } //-- long getNrSequenciaContratoNegocio() 

    /**
     * Method hasCdFaseSaida
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFaseSaida()
    {
        return this._has_cdFaseSaida;
    } //-- boolean hasCdFaseSaida() 

    /**
     * Method hasCdRetorno
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdRetorno()
    {
        return this._has_cdRetorno;
    } //-- boolean hasCdRetorno() 

    /**
     * Method hasNrSequenciaContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaContratoNegocio()
    {
        return this._has_nrSequenciaContratoNegocio;
    } //-- boolean hasNrSequenciaContratoNegocio() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdFaseSaida'.
     * 
     * @param cdFaseSaida the value of field 'cdFaseSaida'.
     */
    public void setCdFaseSaida(int cdFaseSaida)
    {
        this._cdFaseSaida = cdFaseSaida;
        this._has_cdFaseSaida = true;
    } //-- void setCdFaseSaida(int) 

    /**
     * Sets the value of field 'cdRetorno'.
     * 
     * @param cdRetorno the value of field 'cdRetorno'.
     */
    public void setCdRetorno(int cdRetorno)
    {
        this._cdRetorno = cdRetorno;
        this._has_cdRetorno = true;
    } //-- void setCdRetorno(int) 

    /**
     * Sets the value of field 'codMensagem'.
     * 
     * @param codMensagem the value of field 'codMensagem'.
     */
    public void setCodMensagem(java.lang.String codMensagem)
    {
        this._codMensagem = codMensagem;
    } //-- void setCodMensagem(java.lang.String) 

    /**
     * Sets the value of field 'mensagem'.
     * 
     * @param mensagem the value of field 'mensagem'.
     */
    public void setMensagem(java.lang.String mensagem)
    {
        this._mensagem = mensagem;
    } //-- void setMensagem(java.lang.String) 

    /**
     * Sets the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @param nrSequenciaContratoNegocio the value of field
     * 'nrSequenciaContratoNegocio'.
     */
    public void setNrSequenciaContratoNegocio(long nrSequenciaContratoNegocio)
    {
        this._nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
        this._has_nrSequenciaContratoNegocio = true;
    } //-- void setNrSequenciaContratoNegocio(long) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return IncluirContratoPgitResponse
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.incluircontratopgit.response.IncluirContratoPgitResponse unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.incluircontratopgit.response.IncluirContratoPgitResponse) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.incluircontratopgit.response.IncluirContratoPgitResponse.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.incluircontratopgit.response.IncluirContratoPgitResponse unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
