/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.detalharsolbasefavorecido.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class DetalharSolBaseFavorecidoRequest.
 * 
 * @version $Revision$ $Date$
 */
public class DetalharSolBaseFavorecidoRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _nrSolicitacaoPagamento
     */
    private int _nrSolicitacaoPagamento = 0;

    /**
     * keeps track of state for field: _nrSolicitacaoPagamento
     */
    private boolean _has_nrSolicitacaoPagamento;


      //----------------/
     //- Constructors -/
    //----------------/

    public DetalharSolBaseFavorecidoRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharsolbasefavorecido.request.DetalharSolBaseFavorecidoRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteNrSolicitacaoPagamento
     * 
     */
    public void deleteNrSolicitacaoPagamento()
    {
        this._has_nrSolicitacaoPagamento= false;
    } //-- void deleteNrSolicitacaoPagamento() 

    /**
     * Returns the value of field 'nrSolicitacaoPagamento'.
     * 
     * @return int
     * @return the value of field 'nrSolicitacaoPagamento'.
     */
    public int getNrSolicitacaoPagamento()
    {
        return this._nrSolicitacaoPagamento;
    } //-- int getNrSolicitacaoPagamento() 

    /**
     * Method hasNrSolicitacaoPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSolicitacaoPagamento()
    {
        return this._has_nrSolicitacaoPagamento;
    } //-- boolean hasNrSolicitacaoPagamento() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'nrSolicitacaoPagamento'.
     * 
     * @param nrSolicitacaoPagamento the value of field
     * 'nrSolicitacaoPagamento'.
     */
    public void setNrSolicitacaoPagamento(int nrSolicitacaoPagamento)
    {
        this._nrSolicitacaoPagamento = nrSolicitacaoPagamento;
        this._has_nrSolicitacaoPagamento = true;
    } //-- void setNrSolicitacaoPagamento(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return DetalharSolBaseFavorecidoRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.detalharsolbasefavorecido.request.DetalharSolBaseFavorecidoRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.detalharsolbasefavorecido.request.DetalharSolBaseFavorecidoRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.detalharsolbasefavorecido.request.DetalharSolBaseFavorecidoRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharsolbasefavorecido.request.DetalharSolBaseFavorecidoRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
