/*
 * Nome: br.com.bradesco.web.pgit.service.business.manterfavorecido.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.manterfavorecido.bean;

/**
 * Nome: ConsultarListaHistoricoContaFavorecidoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarListaHistoricoContaFavorecidoSaidaDTO {

    /** Atributo codMensagem. */
    private String codMensagem;

    /** Atributo mensagem. */
    private String mensagem;

    /** Atributo numeroConsultas. */
    private Integer numeroConsultas;

    /** Atributo nrOcorrenciaConta. */
    private Integer nrOcorrenciaConta;

    /** Atributo hrInclusaoRegistro. */
    private String hrInclusaoRegistro;

    /** Atributo cdBanco. */
    private Integer cdBanco;

    /** Atributo dsBanco. */
    private String dsBanco;

    /** Atributo cdAgencia. */
    private Integer cdAgencia;

    /** Atributo dsAgencia. */
    private String dsAgencia;

    /** Atributo cdDigitoAgencia. */
    private Integer cdDigitoAgencia;

    /** Atributo cdConta. */
    private Long cdConta;

    /** Atributo cdDigitoConta. */
    private String cdDigitoConta;

    /** Atributo cdTipoConta. */
    private String cdTipoConta;

    /** Atributo cdSituacaoConta. */
    private String cdSituacaoConta;

    /** Atributo dtManutencao. */
    private String dtManutencao;

    /** Atributo hrManutencao. */
    private String hrManutencao;

    /** Atributo cdUsuarioManutencao. */
    private String cdUsuarioManutencao;

    /** Atributo bancoFormatado. */
    private String bancoFormatado;

    /** Atributo agenciaFormatada. */
    private String agenciaFormatada;

    /** Atributo contaFormatada. */
    private String contaFormatada;
    
    /** Atributo tpManutencao. */
    private String tpManutencao;

    /**
     * Get: cdUsuarioManutencao.
     *
     * @return cdUsuarioManutencao
     */
    public String getCdUsuarioManutencao() {
	return cdUsuarioManutencao;
    }

    /**
     * Set: cdUsuarioManutencao.
     *
     * @param cdUsuarioManutencao the cd usuario manutencao
     */
    public void setCdUsuarioManutencao(String cdUsuarioManutencao) {
	this.cdUsuarioManutencao = cdUsuarioManutencao;
    }

    /**
     * Get: dtManutencao.
     *
     * @return dtManutencao
     */
    public String getDtManutencao() {
	return dtManutencao;
    }

    /**
     * Set: dtManutencao.
     *
     * @param dtManutencao the dt manutencao
     */
    public void setDtManutencao(String dtManutencao) {
	this.dtManutencao = dtManutencao;
    }

    /**
     * Get: hrInclusaoRegistro.
     *
     * @return hrInclusaoRegistro
     */
    public String getHrInclusaoRegistro() {
	return hrInclusaoRegistro;
    }

    /**
     * Set: hrInclusaoRegistro.
     *
     * @param hrInclusaoRegistro the hr inclusao registro
     */
    public void setHrInclusaoRegistro(String hrInclusaoRegistro) {
	this.hrInclusaoRegistro = hrInclusaoRegistro;
    }

    /**
     * Get: hrManutencao.
     *
     * @return hrManutencao
     */
    public String getHrManutencao() {
	return hrManutencao;
    }

    /**
     * Set: hrManutencao.
     *
     * @param hrManutencao the hr manutencao
     */
    public void setHrManutencao(String hrManutencao) {
	this.hrManutencao = hrManutencao;
    }

    /**
     * Get: cdAgencia.
     *
     * @return cdAgencia
     */
    public Integer getCdAgencia() {
	return cdAgencia;
    }

    /**
     * Set: cdAgencia.
     *
     * @param cdAgencia the cd agencia
     */
    public void setCdAgencia(Integer cdAgencia) {
	this.cdAgencia = cdAgencia;
    }

    /**
     * Get: cdBanco.
     *
     * @return cdBanco
     */
    public Integer getCdBanco() {
	return cdBanco;
    }

    /**
     * Set: cdBanco.
     *
     * @param cdBanco the cd banco
     */
    public void setCdBanco(Integer cdBanco) {
	this.cdBanco = cdBanco;
    }

    /**
     * Get: cdConta.
     *
     * @return cdConta
     */
    public Long getCdConta() {
	return cdConta;
    }

    /**
     * Set: cdConta.
     *
     * @param cdConta the cd conta
     */
    public void setCdConta(Long cdConta) {
	this.cdConta = cdConta;
    }

    /**
     * Get: cdDigitoAgencia.
     *
     * @return cdDigitoAgencia
     */
    public Integer getCdDigitoAgencia() {
	return cdDigitoAgencia;
    }

    /**
     * Set: cdDigitoAgencia.
     *
     * @param cdDigitoAgencia the cd digito agencia
     */
    public void setCdDigitoAgencia(Integer cdDigitoAgencia) {
	this.cdDigitoAgencia = cdDigitoAgencia;
    }

    /**
     * Get: cdDigitoConta.
     *
     * @return cdDigitoConta
     */
    public String getCdDigitoConta() {
	return cdDigitoConta;
    }

    /**
     * Set: cdDigitoConta.
     *
     * @param cdDigitoConta the cd digito conta
     */
    public void setCdDigitoConta(String cdDigitoConta) {
	this.cdDigitoConta = cdDigitoConta;
    }

    /**
     * Get: cdSituacaoConta.
     *
     * @return cdSituacaoConta
     */
    public String getCdSituacaoConta() {
	return cdSituacaoConta;
    }

    /**
     * Set: cdSituacaoConta.
     *
     * @param cdSituacaoConta the cd situacao conta
     */
    public void setCdSituacaoConta(String cdSituacaoConta) {
	this.cdSituacaoConta = cdSituacaoConta;
    }

    /**
     * Get: cdTipoConta.
     *
     * @return cdTipoConta
     */
    public String getCdTipoConta() {
	return cdTipoConta;
    }

    /**
     * Set: cdTipoConta.
     *
     * @param cdTipoConta the cd tipo conta
     */
    public void setCdTipoConta(String cdTipoConta) {
	this.cdTipoConta = cdTipoConta;
    }

    /**
     * Get: codMensagem.
     *
     * @return codMensagem
     */
    public String getCodMensagem() {
	return codMensagem;
    }

    /**
     * Set: codMensagem.
     *
     * @param codMensagem the cod mensagem
     */
    public void setCodMensagem(String codMensagem) {
	this.codMensagem = codMensagem;
    }

    /**
     * Get: dsAgencia.
     *
     * @return dsAgencia
     */
    public String getDsAgencia() {
	return dsAgencia;
    }

    /**
     * Set: dsAgencia.
     *
     * @param dsAgencia the ds agencia
     */
    public void setDsAgencia(String dsAgencia) {
	this.dsAgencia = dsAgencia;
    }

    /**
     * Get: dsBanco.
     *
     * @return dsBanco
     */
    public String getDsBanco() {
	return dsBanco;
    }

    /**
     * Set: dsBanco.
     *
     * @param dsBanco the ds banco
     */
    public void setDsBanco(String dsBanco) {
	this.dsBanco = dsBanco;
    }

    /**
     * Get: mensagem.
     *
     * @return mensagem
     */
    public String getMensagem() {
	return mensagem;
    }

    /**
     * Set: mensagem.
     *
     * @param mensagem the mensagem
     */
    public void setMensagem(String mensagem) {
	this.mensagem = mensagem;
    }

    /**
     * Get: nrOcorrenciaConta.
     *
     * @return nrOcorrenciaConta
     */
    public Integer getNrOcorrenciaConta() {
	return nrOcorrenciaConta;
    }

    /**
     * Set: nrOcorrenciaConta.
     *
     * @param nrOcorrenciaConta the nr ocorrencia conta
     */
    public void setNrOcorrenciaConta(Integer nrOcorrenciaConta) {
	this.nrOcorrenciaConta = nrOcorrenciaConta;
    }

    /**
     * Get: numeroConsultas.
     *
     * @return numeroConsultas
     */
    public Integer getNumeroConsultas() {
	return numeroConsultas;
    }

    /**
     * Set: numeroConsultas.
     *
     * @param numeroConsultas the numero consultas
     */
    public void setNumeroConsultas(Integer numeroConsultas) {
	this.numeroConsultas = numeroConsultas;
    }

    /**
     * Get: agenciaFormatada.
     *
     * @return agenciaFormatada
     */
    public String getAgenciaFormatada() {
        return agenciaFormatada;
    }

    /**
     * Set: agenciaFormatada.
     *
     * @param agenciaFormatada the agencia formatada
     */
    public void setAgenciaFormatada(String agenciaFormatada) {
        this.agenciaFormatada = agenciaFormatada;
    }

    /**
     * Get: bancoFormatado.
     *
     * @return bancoFormatado
     */
    public String getBancoFormatado() {
	return bancoFormatado;
    }

    /**
     * Set: bancoFormatado.
     *
     * @param bancoFormatado the banco formatado
     */
    public void setBancoFormatado(String bancoFormatado) {
	this.bancoFormatado = bancoFormatado;
    }

    /**
     * Get: contaFormatada.
     *
     * @return contaFormatada
     */
    public String getContaFormatada() {
	return contaFormatada;
    }

    /**
     * Set: contaFormatada.
     *
     * @param contaFormatada the conta formatada
     */
    public void setContaFormatada(String contaFormatada) {
	this.contaFormatada = contaFormatada;
    }

    /**
     * Get: tpManutencao.
     *
     * @return tpManutencao
     */
    public String getTpManutencao() {
        return tpManutencao;
    }

    /**
     * Set: tpManutencao.
     *
     * @param tpManutencao the tp manutencao
     */
    public void setTpManutencao(String tpManutencao) {
        this.tpManutencao = tpManutencao;
    }
}