/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/interfaz.ftl,v $
 * $Id: interfaz.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: interfaz.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.assolotelayoutarquivo;

import java.util.List;

import br.com.bradesco.web.pgit.service.business.assolotelayoutarquivo.bean.AssLoteLayoutArquivoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.assolotelayoutarquivo.bean.AssLoteLayoutArquivoSaidaDTO;

/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Interface do adaptador: AssoLoteLayoutArquivo
 * </p>
 * 
 * @comment CODIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public interface IAssoLoteLayoutArquivoService {

     

	/**
	 * Listar associacao lote layout arquivo.
	 *
	 * @param assLoteLayoutArquivoEntradaDTO the ass lote layout arquivo entrada dto
	 * @return the list< ass lote layout arquivo saida dt o>
	 */
	List<AssLoteLayoutArquivoSaidaDTO> listarAssociacaoLoteLayoutArquivo(AssLoteLayoutArquivoEntradaDTO assLoteLayoutArquivoEntradaDTO);
	
	/**
	 * Incluir associacao lote layout arquivo.
	 *
	 * @param assLoteLayoutArquivoEntradaDTO the ass lote layout arquivo entrada dto
	 * @return the ass lote layout arquivo saida dto
	 */
	AssLoteLayoutArquivoSaidaDTO incluirAssociacaoLoteLayoutArquivo(AssLoteLayoutArquivoEntradaDTO assLoteLayoutArquivoEntradaDTO);
	
	/**
	 * Excluir associacao lote layout arquivo.
	 *
	 * @param assLoteLayoutArquivoEntradaDTO the ass lote layout arquivo entrada dto
	 * @return the ass lote layout arquivo saida dto
	 */
	AssLoteLayoutArquivoSaidaDTO excluirAssociacaoLoteLayoutArquivo(AssLoteLayoutArquivoEntradaDTO assLoteLayoutArquivoEntradaDTO);
	
	/**
	 * Detalhar associacao lote layout arquivo.
	 *
	 * @param assLoteLayoutArquivoEntradaDTO the ass lote layout arquivo entrada dto
	 * @return the ass lote layout arquivo saida dto
	 */
	AssLoteLayoutArquivoSaidaDTO detalharAssociacaoLoteLayoutArquivo(AssLoteLayoutArquivoEntradaDTO assLoteLayoutArquivoEntradaDTO);
	
}

