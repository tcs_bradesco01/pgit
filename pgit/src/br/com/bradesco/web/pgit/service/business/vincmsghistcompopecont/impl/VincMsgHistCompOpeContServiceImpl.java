/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/implementation.ftl,v $
 * $Id: implementation.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: implementation.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */
 
package br.com.bradesco.web.pgit.service.business.vincmsghistcompopecont.impl;

import java.util.ArrayList;
import java.util.List;

import br.com.bradesco.web.pgit.service.business.vincmsghistcompopecont.IVincMsgHistCompOpeContService;
import br.com.bradesco.web.pgit.service.business.vincmsghistcompopecont.IVincMsgHistCompOpeContServiceConstants;
import br.com.bradesco.web.pgit.service.business.vincmsghistcompopecont.bean.DetalheMsgHistComOpEntradaDTO;
import br.com.bradesco.web.pgit.service.business.vincmsghistcompopecont.bean.DetalheMsgHistComOpSaidaDTO;
import br.com.bradesco.web.pgit.service.business.vincmsghistcompopecont.bean.DetalheVinMsgHistComOpEntradaDTO;
import br.com.bradesco.web.pgit.service.business.vincmsghistcompopecont.bean.DetalheVinMsgHistComOpSaidaDTO;
import br.com.bradesco.web.pgit.service.business.vincmsghistcompopecont.bean.ExcluirVinMsgHistComOpEntradaDTO;
import br.com.bradesco.web.pgit.service.business.vincmsghistcompopecont.bean.ExcluirVinMsgHistComOpSaidaDTO;
import br.com.bradesco.web.pgit.service.business.vincmsghistcompopecont.bean.IncluirVinMsgHistComOpEntradaDTO;
import br.com.bradesco.web.pgit.service.business.vincmsghistcompopecont.bean.IncluirVinMsgHistComOpSaidaDTO;
import br.com.bradesco.web.pgit.service.business.vincmsghistcompopecont.bean.ListarMsgHistCompEntradaDTO;
import br.com.bradesco.web.pgit.service.business.vincmsghistcompopecont.bean.ListarMsgHistCompSaidaDTO;
import br.com.bradesco.web.pgit.service.business.vincmsghistcompopecont.bean.ListarVinMsgHistComOpEntradaDTO;
import br.com.bradesco.web.pgit.service.business.vincmsghistcompopecont.bean.ListarVinMsgHistComOpSaidaDTO;
import br.com.bradesco.web.pgit.service.data.pdc.FactoryAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.detalharmsghistoricocomplementar.request.DetalharMsgHistoricoComplementarRequest;
import br.com.bradesco.web.pgit.service.data.pdc.detalharmsghistoricocomplementar.response.DetalharMsgHistoricoComplementarResponse;
import br.com.bradesco.web.pgit.service.data.pdc.detalharvincmsghistcomplementaroperacao.request.DetalharVincMsgHistComplementarOperacaoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.detalharvincmsghistcomplementaroperacao.response.DetalharVincMsgHistComplementarOperacaoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.excluirvincmsghistcomplementaroperacao.request.ExcluirVincMsgHistComplementarOperacaoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.excluirvincmsghistcomplementaroperacao.response.ExcluirVincMsgHistComplementarOperacaoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.incluirvincmsghistcomplementaroperacao.request.IncluirVincMsgHistComplementarOperacaoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.incluirvincmsghistcomplementaroperacao.response.IncluirVincMsgHistComplementarOperacaoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarmsghistoricocomplementar.request.ListarMsgHistoricoComplementarRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarmsghistoricocomplementar.response.ListarMsgHistoricoComplementarResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarvincmsghistcomplementaroperacao.request.ListarVincMsgHistComplementarOperacaoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarvincmsghistcomplementaroperacao.response.ListarVincMsgHistComplementarOperacaoResponse;


/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Implementa��o do adaptador: VincMsgHistCompOpeCont
 * </p>
 * 
 * @comment C�DIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public class VincMsgHistCompOpeContServiceImpl implements IVincMsgHistCompOpeContService {
	
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.vincmsghistcompopecont.IVincMsgHistCompOpeContService#listarVinculoMsgHistCompOperacao(br.com.bradesco.web.pgit.service.business.vincmsghistcompopecont.bean.ListarVinMsgHistComOpEntradaDTO)
	 */
	public List<ListarVinMsgHistComOpSaidaDTO> listarVinculoMsgHistCompOperacao(ListarVinMsgHistComOpEntradaDTO msgVinMsgHistComOpEntradaDTO) {
		
		List<ListarVinMsgHistComOpSaidaDTO> listaVinMsgHistComOp = new ArrayList<ListarVinMsgHistComOpSaidaDTO>();
		ListarVincMsgHistComplementarOperacaoRequest listarVinMsgHistComOpRequest = new ListarVincMsgHistComplementarOperacaoRequest();
		ListarVincMsgHistComplementarOperacaoResponse listarVinMsgHistComOpResponse = new ListarVincMsgHistComplementarOperacaoResponse();
		
		listarVinMsgHistComOpRequest.setCdMensagemLinhaExtrato(msgVinMsgHistComOpEntradaDTO.getCdMensagemLinhaExtrato());
		listarVinMsgHistComOpRequest.setCdPessoaJuridicaContrato(msgVinMsgHistComOpEntradaDTO.getCdPessoaJuridicaContrato());
		listarVinMsgHistComOpRequest.setCdProdutoOperacaoRelacionado(msgVinMsgHistComOpEntradaDTO.getCdProdutoOperacaoRelacionado());
		listarVinMsgHistComOpRequest.setCdProdutoServicoOperacao(msgVinMsgHistComOpEntradaDTO.getCdProdutoServicoOperacao());
		listarVinMsgHistComOpRequest.setCdRelacionamentoProduto(msgVinMsgHistComOpEntradaDTO.getCdRelacionamentoProduto());
		listarVinMsgHistComOpRequest.setCdTipoContratoNegocio(msgVinMsgHistComOpEntradaDTO.getCdTipoContratoNegocio());
		listarVinMsgHistComOpRequest.setNrSequenciaContratoNegocio(msgVinMsgHistComOpEntradaDTO.getNrSequenciaContratoNegocio());
		listarVinMsgHistComOpRequest.setQtOcorrencias(IVincMsgHistCompOpeContServiceConstants.QTDE_CONSULTAS_LISTAR);
		
		listarVinMsgHistComOpResponse = getFactoryAdapter().getListarVincMsgHistComplementarOperacaoPDCAdapter().invokeProcess(listarVinMsgHistComOpRequest);

		ListarVinMsgHistComOpSaidaDTO listarVinMsgHistComOpSaidaDTO;
		
		for (int i=0; i<listarVinMsgHistComOpResponse.getOcorrenciasCount();i++){
			
			listarVinMsgHistComOpSaidaDTO = new ListarVinMsgHistComOpSaidaDTO();
			
			listarVinMsgHistComOpSaidaDTO.setCodMensagem(listarVinMsgHistComOpResponse.getCodMensagem());
			listarVinMsgHistComOpSaidaDTO.setDsMensagem(listarVinMsgHistComOpResponse.getMensagem());
			
			listarVinMsgHistComOpSaidaDTO.setCdMensagemLinhaExtrato(listarVinMsgHistComOpResponse.getOcorrencias(i).getCdMensagemLinhaExtrato());
			listarVinMsgHistComOpSaidaDTO.setCdProdutoOperacaoRelacionado(listarVinMsgHistComOpResponse.getOcorrencias(i).getCdProdutoOperacaoRelacionado());
			listarVinMsgHistComOpSaidaDTO.setCdProdutoServicoOperacao(listarVinMsgHistComOpResponse.getOcorrencias(i).getCdProdutoServicoOperacao());
			listarVinMsgHistComOpSaidaDTO.setCdRelacionamentoProduto(listarVinMsgHistComOpResponse.getOcorrencias(i).getCdRelacionamentoProduto());
			listarVinMsgHistComOpSaidaDTO.setDsMensagemLinhaCredito(listarVinMsgHistComOpResponse.getOcorrencias(i).getDsMensagemLinhaCredito());
			listarVinMsgHistComOpSaidaDTO.setDsMensagemLinhaDebito(listarVinMsgHistComOpResponse.getOcorrencias(i).getDsMensagemLinhaDebito());
			listarVinMsgHistComOpSaidaDTO.setDsProdutoOperacaoRelacionado(listarVinMsgHistComOpResponse.getOcorrencias(i).getDsProdutoOperacaoRelacionado());
			listarVinMsgHistComOpSaidaDTO.setDsProdutoServicoOperacao(listarVinMsgHistComOpResponse.getOcorrencias(i).getDsProdutoServicoOperacao());
			
			listaVinMsgHistComOp.add(listarVinMsgHistComOpSaidaDTO);
		}
					
		
		
		return listaVinMsgHistComOp;	
		
	}	
	
	
	
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.vincmsghistcompopecont.IVincMsgHistCompOpeContService#listarMsgHistComplementar(br.com.bradesco.web.pgit.service.business.vincmsghistcompopecont.bean.ListarMsgHistCompEntradaDTO)
	 */
	public List<ListarMsgHistCompSaidaDTO> listarMsgHistComplementar(ListarMsgHistCompEntradaDTO msgMsgHistCompEntradaDTO) {
		List<ListarMsgHistCompSaidaDTO> listaMsgHistComOp = new ArrayList<ListarMsgHistCompSaidaDTO>();
		ListarMsgHistoricoComplementarRequest listarMsgHistoricoComplementarRequest = new ListarMsgHistoricoComplementarRequest();
		ListarMsgHistoricoComplementarResponse listarMsgHistoricoComplementarResponse = new ListarMsgHistoricoComplementarResponse();
		
		listarMsgHistoricoComplementarRequest.setCdIndicadorRestricaoContrato(msgMsgHistCompEntradaDTO.getCdIndicadorRestricaoContrato());
		listarMsgHistoricoComplementarRequest.setCdMensagemLinhaExtrato(msgMsgHistCompEntradaDTO.getCdMensagemLinhaExtrato());
		listarMsgHistoricoComplementarRequest.setCdProdutoOperacaoRelacionado(msgMsgHistCompEntradaDTO.getCdProdutoOperacaoRelacionado());
		listarMsgHistoricoComplementarRequest.setCdProdutoServicoOperacao(msgMsgHistCompEntradaDTO.getCdProdutoServicoOperacao());
		listarMsgHistoricoComplementarRequest.setCdRelacionamentoProduto(msgMsgHistCompEntradaDTO.getCdRelacionamentoProduto());
		listarMsgHistoricoComplementarRequest.setCdTipoMensagemExtrato(msgMsgHistCompEntradaDTO.getCdTipoMensagemExtrato());
		listarMsgHistoricoComplementarRequest.setQtOcorrencias(IVincMsgHistCompOpeContServiceConstants.QTDE_CONSULTAS_LISTAR);
		
		listarMsgHistoricoComplementarResponse = getFactoryAdapter().getListarMsgHistoricoComplementarPDCAdapter().invokeProcess(listarMsgHistoricoComplementarRequest);

		ListarMsgHistCompSaidaDTO dto;
		
		for (int i=0; i<listarMsgHistoricoComplementarResponse.getOcorrenciasCount();i++){
			
			dto = new ListarMsgHistCompSaidaDTO();
			
			dto.setCdMensagem(listarMsgHistoricoComplementarResponse.getCodMensagem());
			dto.setMensagem(listarMsgHistoricoComplementarResponse.getMensagem());
			
			dto.setCdIndicadorRestricaoContrato(listarMsgHistoricoComplementarResponse.getOcorrencias(i).getCdIndicadorRestricaoContrato());
			dto.setCdMensagemLinhaExtrato(listarMsgHistoricoComplementarResponse.getOcorrencias(i).getCdMensagemLinhaExtrato());
			dto.setCdProdutoOperacaoRelacionado(listarMsgHistoricoComplementarResponse.getOcorrencias(i).getCdProdutoOperacaoRelacionado());
			dto.setCdProdutoServicoOperacao(listarMsgHistoricoComplementarResponse.getOcorrencias(i).getCdProdutoServicoOperacao());
			dto.setCdRelacionadoProduto(listarMsgHistoricoComplementarResponse.getOcorrencias(i).getCdRelacionadoProduto());
			dto.setCdTipoMensagemExtrato(listarMsgHistoricoComplementarResponse.getOcorrencias(i).getCdTipoMensagemExtrato());
			dto.setDsProdutoOperacaoRelacionado(listarMsgHistoricoComplementarResponse.getOcorrencias(i).getDsProdutoOperacaoRelacionado());
			
			if (listarMsgHistoricoComplementarResponse.getOcorrencias(i).getCdIndicadorRestricaoContrato() == 1){
				dto.setRestricaoDesc("Sim");
			}
			else if (listarMsgHistoricoComplementarResponse.getOcorrencias(i).getCdIndicadorRestricaoContrato() == 2){
				dto.setRestricaoDesc("N�o");
			}
			else{
				dto.setRestricaoDesc("");
			}
			
			dto.setDsProdutoServicoOperacao(listarMsgHistoricoComplementarResponse.getOcorrencias(i).getDsProdutoServicoOperacao());
			
			listaMsgHistComOp.add(dto);
		}
		
		return listaMsgHistComOp;	
		
	}		
	
	
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.vincmsghistcompopecont.IVincMsgHistCompOpeContService#excluirVinMsgHistoricoComplementar(br.com.bradesco.web.pgit.service.business.vincmsghistcompopecont.bean.ExcluirVinMsgHistComOpEntradaDTO)
	 */
	public ExcluirVinMsgHistComOpSaidaDTO excluirVinMsgHistoricoComplementar(ExcluirVinMsgHistComOpEntradaDTO msgVinMsgHistCompOpEntradaDTO) {
		
		ExcluirVinMsgHistComOpSaidaDTO excluirVinMsgHistComOpSaidaDTO = new ExcluirVinMsgHistComOpSaidaDTO();
		
		ExcluirVincMsgHistComplementarOperacaoRequest excluirVincMsgHistCompOpeRequest = new ExcluirVincMsgHistComplementarOperacaoRequest();
		ExcluirVincMsgHistComplementarOperacaoResponse excluirVincMsgHistCompOpeResponse = new ExcluirVincMsgHistComplementarOperacaoResponse();
		
		excluirVincMsgHistCompOpeRequest.setCdMensagemLinhaExtrato(msgVinMsgHistCompOpEntradaDTO.getCdMensagemLinhaExtrato());
		excluirVincMsgHistCompOpeRequest.setCdPessoaJuridicaContrato(msgVinMsgHistCompOpEntradaDTO.getCdPessoaJuridicaContrato());
		excluirVincMsgHistCompOpeRequest.setCdProdutoOperacaoRelacionado(msgVinMsgHistCompOpEntradaDTO.getCdProdutoOperacaoRelacionado());
		excluirVincMsgHistCompOpeRequest.setCdProdutoServicoOperacao(msgVinMsgHistCompOpEntradaDTO.getCdProdutoServicoOperacao());
		excluirVincMsgHistCompOpeRequest.setCdRelacionamentoProduto(msgVinMsgHistCompOpEntradaDTO.getCdRelacionamentoProduto());
		excluirVincMsgHistCompOpeRequest.setCdTipoContratoNegocio(msgVinMsgHistCompOpEntradaDTO.getCdTipoContratoNegocio());
		excluirVincMsgHistCompOpeRequest.setNrSequenciaContratoNegocio(msgVinMsgHistCompOpEntradaDTO.getNrSequenciaContratoNegocio());
		excluirVincMsgHistCompOpeRequest.setQtOcorrencias(0);
		
		excluirVincMsgHistCompOpeResponse = getFactoryAdapter().getExcluirVincMsgHistComplementarOperacaoPDCAdapter().invokeProcess(excluirVincMsgHistCompOpeRequest);
				
		excluirVinMsgHistComOpSaidaDTO.setCodMensagem(excluirVincMsgHistCompOpeResponse.getCodMensagem());
		excluirVinMsgHistComOpSaidaDTO.setMensagem(excluirVincMsgHistCompOpeResponse.getMensagem());
		
				
		return excluirVinMsgHistComOpSaidaDTO;
	}	
	
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.vincmsghistcompopecont.IVincMsgHistCompOpeContService#incluirVinMsgHistoricoComplementar(br.com.bradesco.web.pgit.service.business.vincmsghistcompopecont.bean.IncluirVinMsgHistComOpEntradaDTO)
	 */
	public IncluirVinMsgHistComOpSaidaDTO incluirVinMsgHistoricoComplementar(IncluirVinMsgHistComOpEntradaDTO msgVinMsgHistComOpEntradaDTO) {
		IncluirVinMsgHistComOpSaidaDTO incluirVinMsgHistComOpSaidaDTO = new IncluirVinMsgHistComOpSaidaDTO();
		IncluirVincMsgHistComplementarOperacaoRequest incluirVincMsgHistComplementarOperacaoRequest = new IncluirVincMsgHistComplementarOperacaoRequest();
		IncluirVincMsgHistComplementarOperacaoResponse incluirVincMsgHistComplementarOperacaoResponse = new IncluirVincMsgHistComplementarOperacaoResponse();		
		
		incluirVincMsgHistComplementarOperacaoRequest.setCdMensagemLinhaExtrato(msgVinMsgHistComOpEntradaDTO.getCdMensagemLinhaExtrato());
		incluirVincMsgHistComplementarOperacaoRequest.setCdPessoaJuridicaContrato(msgVinMsgHistComOpEntradaDTO.getCdPessoaJuridicaContrato());
		incluirVincMsgHistComplementarOperacaoRequest.setCdProdutoOperacaoRelacionado(msgVinMsgHistComOpEntradaDTO.getCdProdutoOperacaoRelacionado());
		incluirVincMsgHistComplementarOperacaoRequest.setCdProdutoServicoOperacao(msgVinMsgHistComOpEntradaDTO.getCdProdutoServicoOperacao());
		incluirVincMsgHistComplementarOperacaoRequest.setCdRelacionamentoProduto(msgVinMsgHistComOpEntradaDTO.getCdRelacionamentoProduto());
		incluirVincMsgHistComplementarOperacaoRequest.setCdTipoContratoNegocio(msgVinMsgHistComOpEntradaDTO.getCdTipoContratoNegocio());
		incluirVincMsgHistComplementarOperacaoRequest.setNrSequenciaContratoNegocio(msgVinMsgHistComOpEntradaDTO.getNrSequenciaContratoNegocio());
		incluirVincMsgHistComplementarOperacaoRequest.setQtOcorrencias(IVincMsgHistCompOpeContServiceConstants.QTDE_INCLUIR);
		
		incluirVincMsgHistComplementarOperacaoResponse = getFactoryAdapter().getIncluirVincMsgHistComplementarOperacaoPDCAdapter().invokeProcess(incluirVincMsgHistComplementarOperacaoRequest);
		
		incluirVinMsgHistComOpSaidaDTO.setCodMensagem(incluirVincMsgHistComplementarOperacaoResponse.getCodMensagem());
		incluirVinMsgHistComOpSaidaDTO.setMensagem(incluirVincMsgHistComplementarOperacaoResponse.getMensagem());
				
		return incluirVinMsgHistComOpSaidaDTO;
		
	}	
	
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.vincmsghistcompopecont.IVincMsgHistCompOpeContService#detalharVinMsgHistoricoComplementar(br.com.bradesco.web.pgit.service.business.vincmsghistcompopecont.bean.DetalheVinMsgHistComOpEntradaDTO)
	 */
	public DetalheVinMsgHistComOpSaidaDTO detalharVinMsgHistoricoComplementar(DetalheVinMsgHistComOpEntradaDTO msgVinMsgHistComOpEntradaDTO) {
		
		DetalharVincMsgHistComplementarOperacaoRequest request = new DetalharVincMsgHistComplementarOperacaoRequest();
		DetalharVincMsgHistComplementarOperacaoResponse response = new DetalharVincMsgHistComplementarOperacaoResponse();
		
		request.setCdMensagemLinhaExtrato(msgVinMsgHistComOpEntradaDTO.getCdMensagemLinhaExtrato());
		request.setCdPessoaJuridicaContrato(msgVinMsgHistComOpEntradaDTO.getCdPessoaJuridicaContrato());
		request.setCdProdutoOperacaoRelacionado(msgVinMsgHistComOpEntradaDTO.getCdProdutoOperacaoRelacionado());
		request.setCdProdutoServicoOperacao(msgVinMsgHistComOpEntradaDTO.getCdProdutoServicoOperacao());
		request.setCdRelacionamentoProduto(msgVinMsgHistComOpEntradaDTO.getCdRelacionamentoProduto());
		request.setCdTipoContratoNegocio(msgVinMsgHistComOpEntradaDTO.getCdTipoContratoNegocio());
		request.setNrSequenciaContratoNegocio(msgVinMsgHistComOpEntradaDTO.getNrSequenciaContratoNegocio());
		request.setQtOcorrencias(0);
		
		
		DetalheVinMsgHistComOpSaidaDTO saidaDTO = new DetalheVinMsgHistComOpSaidaDTO();
		
		response = getFactoryAdapter().getDetalharVincMsgHistComplementarOperacaoPDCAdapter().invokeProcess(request);
	
		saidaDTO.setCodMensagem(response.getCodMensagem());
		saidaDTO.setMensagem(response.getMensagem());
		
		saidaDTO.setCdAutenticacaoSegurancaInclusao(response.getOcorrencias(0).getCdAutenticacaoSegurancaInclusao());
		saidaDTO.setCdAutenticacaoSegurancaManutencao(response.getOcorrencias(0).getCdAutenticacaoSegurancaManutencao());
		saidaDTO.setCdCanalInclusao(response.getOcorrencias(0).getCdCanalInclusao());
		saidaDTO.setCdCanalManutencao(response.getOcorrencias(0).getCdCanalManutencao());
		saidaDTO.setCdIdiomaLancamentoCredito(response.getOcorrencias(0).getCdIdiomaLancamentoCredito());
		saidaDTO.setCdIdiomaLancamentoDebito(response.getOcorrencias(0).getCdIdiomaLancamentoDebito());
		saidaDTO.setCdIndicadorRestricaoContrato(response.getOcorrencias(0).getCdIndicadorRestricaoContrato());
		saidaDTO.setCdMensagemLinhaExtrato(response.getOcorrencias(0).getCdMensagemLinhaExtrato());
		saidaDTO.setCdPessoaJuridicaContrato(response.getOcorrencias(0).getCdPessoaJuridicaContrato());
		saidaDTO.setCdProdutoOperacaoRelacionado(response.getOcorrencias(0).getCdProdutoOperacaoRelacionado());
		saidaDTO.setCdProdutoServicoOperacao(response.getOcorrencias(0).getCdProdutoServicoOperacao());
		saidaDTO.setCdRecursoLancamentoCredito(response.getOcorrencias(0).getCdRecursoLancamentoCredito());
		saidaDTO.setCdRecursoLancamentoDebito(response.getOcorrencias(0).getCdRecursoLancamentoDebito());
		saidaDTO.setCdRelacionadoProduto(response.getOcorrencias(0).getCdRelacionadoProduto());
		saidaDTO.setCdSistemaLancamentoCredito(response.getOcorrencias(0).getCdSistemaLancamentoCredito());
		saidaDTO.setCdSistemaLancamentoDebito(response.getOcorrencias(0).getCdSistemaLancamentoDebito());
		saidaDTO.setCdTipoContratoNegocio(response.getOcorrencias(0).getCdTipoContratoNegocio());
		saidaDTO.setCdTipoMensagemExtrato(response.getOcorrencias(0).getCdTipoMensagemExtrato());
		saidaDTO.setCodMensagem(response.getCodMensagem());
		saidaDTO.setMensagem(response.getMensagem());
		saidaDTO.setDsCanalInclusao(response.getOcorrencias(0).getDsCanalInclusao());
		saidaDTO.setDsCanalManutencao(response.getOcorrencias(0).getDsCanalManutencao());
		saidaDTO.setDsEventoLancamentoCredito(response.getOcorrencias(0).getDsEventoLancamentoCredito());
		saidaDTO.setDsEventoLancamentoDebito(response.getOcorrencias(0).getDsEventoLancamentoDebito());
		saidaDTO.setDsIdiomaLancamentoCredito(response.getOcorrencias(0).getDsIdiomaLancamentoCredito());
		saidaDTO.setDsIdiomaLancamentoDebito(response.getOcorrencias(0).getDsIdiomaLancamentoDebito());
		saidaDTO.setDsProdutoOperacaoRelacionado(response.getOcorrencias(0).getDsProdutoOperacaoRelacionado());
		saidaDTO.setDsProdutoServicoOperacao(response.getOcorrencias(0).getDsProdutoServicoOperacao());
		saidaDTO.setDsRecursoLancamentoCredito(response.getOcorrencias(0).getDsRecursoLancamentoCredito());
		saidaDTO.setDsRecursoLancamentoDebito(response.getOcorrencias(0).getDsRecursoLancamentoDebito());
		saidaDTO.setDsSistemaLancamentoCredito(response.getOcorrencias(0).getDsSistemaLancamentoCredito());
		saidaDTO.setDsSistemaLancamentoDebito(response.getOcorrencias(0).getDsSistemaLancamentoDebito());	
		saidaDTO.setNmOperacaoFluxoInclusao(response.getOcorrencias(0).getNmOperacaoFluxoInclusao());
		saidaDTO.setNmOperacaoFluxoManutencao(response.getOcorrencias(0).getNmOperacaoFluxoManutencao());
		saidaDTO.setNrEventoLancamentoCredito(response.getOcorrencias(0).getNrEventoLancamentoCredito());
		saidaDTO.setNrEventoLancamentoDebito(response.getOcorrencias(0).getNrEventoLancamentoDebito());
		saidaDTO.setNrSequenciaContratoNegocio(response.getOcorrencias(0).getNrSequenciaContratoNegocio());
		saidaDTO.setDsMensagemLinhaCredito(response.getOcorrencias(0).getDsMensagemLinhaCredito());
		saidaDTO.setDsMensagemLinhaDebito(response.getOcorrencias(0).getDsMensagemLinhaDebito());

		saidaDTO.setHrManutencaoRegistro(response.getOcorrencias(0).getHrManutencaoRegistro());
		saidaDTO.setHrInclusaoRegistro(response.getOcorrencias(0).getHrInclusaoRegistro());
		
		return saidaDTO;
		
	}	
	
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.vincmsghistcompopecont.IVincMsgHistCompOpeContService#detalharMsgHistoricoComplementar(br.com.bradesco.web.pgit.service.business.vincmsghistcompopecont.bean.DetalheMsgHistComOpEntradaDTO)
	 */
	public DetalheMsgHistComOpSaidaDTO detalharMsgHistoricoComplementar(DetalheMsgHistComOpEntradaDTO msgMsgHistComOpEntradaDTO) {
		
		DetalharMsgHistoricoComplementarRequest request = new DetalharMsgHistoricoComplementarRequest();
		DetalharMsgHistoricoComplementarResponse response = new DetalharMsgHistoricoComplementarResponse();
		
		request.setCdMensagemLinhaExtrato(msgMsgHistComOpEntradaDTO.getCdMensagemLinhaExtrato());
		request.setCdProdutoOperacaoRelacionado(msgMsgHistComOpEntradaDTO.getCdProdutoOperacaoRelacionado());
		request.setCdProdutoServicoOperacao(msgMsgHistComOpEntradaDTO.getCdProdutoServicoOperacao());
		request.setCdRelacionadoProduto(msgMsgHistComOpEntradaDTO.getCdRelacionamentoProduto());
		
		DetalheMsgHistComOpSaidaDTO saidaDTO = new DetalheMsgHistComOpSaidaDTO();
		
		response = getFactoryAdapter().getDetalharMsgHistoricoComplementarPDCAdapter().invokeProcess(request);
	
		saidaDTO.setCodMensagem(response.getCodMensagem());
		saidaDTO.setMensagem(response.getMensagem());
		
		saidaDTO.setCdAutenticacaoSegurancaInclusao(response.getOcorrencias(0).getCdAutenticacaoSegurancaInclusao());
		saidaDTO.setCdAutenticacaoSegurancaManutencao(response.getOcorrencias(0).getCdAutenticacaoSegurancaManutencao());
		saidaDTO.setCdCanalInclusao(response.getOcorrencias(0).getCdCanalInclusao());
		saidaDTO.setCdCanalManutencao(response.getOcorrencias(0).getCdCanalManutencao());
		saidaDTO.setCdIdiomaLancamentoCredito(response.getOcorrencias(0).getCdIdiomaLancamentoCredito());
		saidaDTO.setCdIdiomaLancamentoDebito(response.getOcorrencias(0).getCdIdiomaLancamentoDebito());
		saidaDTO.setCdIndicadorRestricaoContrato(response.getOcorrencias(0).getCdIndicadorRestricaoContrato());
		saidaDTO.setCdMensagemLinhaExtrato(response.getOcorrencias(0).getCdMensagemLinhaExtrato());
		saidaDTO.setCdProdutoOperacaoRelacionado(response.getOcorrencias(0).getCdProdutoOperacaoRelacionado());
		saidaDTO.setCdProdutoServicoOperacao(response.getOcorrencias(0).getCdProdutoServicoOperacao());
		saidaDTO.setCdRecursoLancamentoCredito(response.getOcorrencias(0).getCdRecursoLancamentoCredito());
		saidaDTO.setCdRecursoLancamentoDebito(response.getOcorrencias(0).getCdRecursoLancamentoDebito());
		saidaDTO.setCdRelacionadoProduto(response.getOcorrencias(0).getCdRelacionadoProduto());
		saidaDTO.setCdSistemaLancamentoCredito(response.getOcorrencias(0).getCdSistemaLancamentoCredito());
		saidaDTO.setCdSistemaLancamentoDebito(response.getOcorrencias(0).getCdSistemaLancamentoDebito());
		saidaDTO.setCdTipoMensagemExtrato(response.getOcorrencias(0).getCdTipoMensagemExtrato());
		saidaDTO.setCodMensagem(response.getCodMensagem());
		saidaDTO.setMensagem(response.getMensagem());
		saidaDTO.setDsCanalInclusao(response.getOcorrencias(0).getDsCanalInclusao());
		saidaDTO.setDsCanalManutencao(response.getOcorrencias(0).getDsCanalManutencao());
		saidaDTO.setDsEventoLancamentoCredito(response.getOcorrencias(0).getDsEventoLancamentoCredito());
		saidaDTO.setDsEventoLancamentoDebito(response.getOcorrencias(0).getDsEventoLancamentoDebito());
		saidaDTO.setDsIdiomaLancamentoCredito(response.getOcorrencias(0).getDsIdiomaLancamentoCredito());
		saidaDTO.setDsIdiomaLancamentoDebito(response.getOcorrencias(0).getDsIdiomaLancamentoDebito());
		saidaDTO.setDsProdutoOperacaoRelacionado(response.getOcorrencias(0).getDsProdutoOperacaoRelacionado());
		saidaDTO.setDsProdutoServicoOperacao(response.getOcorrencias(0).getDsProdutoServicoOperacao());
		saidaDTO.setDsRecursoLancamentoCredito(response.getOcorrencias(0).getDsRecursoLancamentoCredito());
		saidaDTO.setDsRecursoLancamentoDebito(response.getOcorrencias(0).getDsRecursoLancamentoDebito());
		saidaDTO.setDsSistemaLancamentoCredito(response.getOcorrencias(0).getDsSistemaLancamentoCredito());
		saidaDTO.setDsSistemaLancamentoDebito(response.getOcorrencias(0).getDsSistemaLancamentoDebito());	
		saidaDTO.setNmOperacaoFluxoInclusao(response.getOcorrencias(0).getNmOperacaoFluxoInclusao());
		saidaDTO.setNmOperacaoFluxoManutencao(response.getOcorrencias(0).getNmOperacaoFluxoManutencao());
		saidaDTO.setNrEventoLancamentoCredito(response.getOcorrencias(0).getNrEventoLancamentoCredito());
		saidaDTO.setNrEventoLancamentoDebito(response.getOcorrencias(0).getNrEventoLancamentoDebito());
		saidaDTO.setDsMensagemLinhaCredito(response.getOcorrencias(0).getDsSegundaLinhaExtratoCredito());
		saidaDTO.setDsMensagemLinhaDebito(response.getOcorrencias(0).getDsSegundaLinhaExtratoDebito());
		
		return saidaDTO;
		
	}		
	
	/** Atributo factoryAdapter. */
	private FactoryAdapter factoryAdapter;

	/**
	 * Get: factoryAdapter.
	 *
	 * @return factoryAdapter
	 */
	public FactoryAdapter getFactoryAdapter() {
		return factoryAdapter;
	}

	/**
	 * Set: factoryAdapter.
	 *
	 * @param factoryAdapter the factory adapter
	 */
	public void setFactoryAdapter(FactoryAdapter factoryAdapter) {
		this.factoryAdapter = factoryAdapter;
	}
	
	
}

