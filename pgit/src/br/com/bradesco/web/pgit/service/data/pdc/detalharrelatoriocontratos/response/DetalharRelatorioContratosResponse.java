/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.detalharrelatoriocontratos.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.math.BigDecimal;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class DetalharRelatorioContratosResponse.
 * 
 * @version $Revision$ $Date$
 */
public class DetalharRelatorioContratosResponse implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _codMensagem
     */
    private java.lang.String _codMensagem;

    /**
     * Field _mensagem
     */
    private java.lang.String _mensagem;

    /**
     * Field _cdSituacaoSolicitacaoPagamento
     */
    private int _cdSituacaoSolicitacaoPagamento = 0;

    /**
     * keeps track of state for field:
     * _cdSituacaoSolicitacaoPagamento
     */
    private boolean _has_cdSituacaoSolicitacaoPagamento;

    /**
     * Field _dsSolicitacaoPagamento
     */
    private java.lang.String _dsSolicitacaoPagamento;

    /**
     * Field _cdMotivoSituacaoSolicitacao
     */
    private int _cdMotivoSituacaoSolicitacao = 0;

    /**
     * keeps track of state for field: _cdMotivoSituacaoSolicitacao
     */
    private boolean _has_cdMotivoSituacaoSolicitacao;

    /**
     * Field _dsMotivoSolicitacao
     */
    private java.lang.String _dsMotivoSolicitacao;

    /**
     * Field _dtSolicitacao
     */
    private java.lang.String _dtSolicitacao;

    /**
     * Field _hrSolicitacao
     */
    private java.lang.String _hrSolicitacao;

    /**
     * Field _dtAtendimento
     */
    private java.lang.String _dtAtendimento;

    /**
     * Field _hrAtendimento
     */
    private java.lang.String _hrAtendimento;

    /**
     * Field _cdPercentualTarifaSolic
     */
    private java.math.BigDecimal _cdPercentualTarifaSolic = new java.math.BigDecimal("0");

    /**
     * Field _cdProdutoServicoOper
     */
    private int _cdProdutoServicoOper = 0;

    /**
     * keeps track of state for field: _cdProdutoServicoOper
     */
    private boolean _has_cdProdutoServicoOper;

    /**
     * Field _dsProdutoServicoOperacao
     */
    private java.lang.String _dsProdutoServicoOperacao;

    /**
     * Field _cdProdutoOperRelacionado
     */
    private int _cdProdutoOperRelacionado = 0;

    /**
     * keeps track of state for field: _cdProdutoOperRelacionado
     */
    private boolean _has_cdProdutoOperRelacionado;

    /**
     * Field _dsProdutoOperRelacionado
     */
    private java.lang.String _dsProdutoOperRelacionado;

    /**
     * Field _cdRelacionamentoProduto
     */
    private int _cdRelacionamentoProduto = 0;

    /**
     * keeps track of state for field: _cdRelacionamentoProduto
     */
    private boolean _has_cdRelacionamentoProduto;

    /**
     * Field _cdPessoaJuridDir
     */
    private long _cdPessoaJuridDir = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridDir
     */
    private boolean _has_cdPessoaJuridDir;

    /**
     * Field _nrSeqUnidadeOrgnzDir
     */
    private int _nrSeqUnidadeOrgnzDir = 0;

    /**
     * keeps track of state for field: _nrSeqUnidadeOrgnzDir
     */
    private boolean _has_nrSeqUnidadeOrgnzDir;

    /**
     * Field _dsUnidadeOrganizacionalDir
     */
    private java.lang.String _dsUnidadeOrganizacionalDir;

    /**
     * Field _cdPessoaJuridGerc
     */
    private long _cdPessoaJuridGerc = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridGerc
     */
    private boolean _has_cdPessoaJuridGerc;

    /**
     * Field _nrUnidadeOrgnzGerc
     */
    private int _nrUnidadeOrgnzGerc = 0;

    /**
     * keeps track of state for field: _nrUnidadeOrgnzGerc
     */
    private boolean _has_nrUnidadeOrgnzGerc;

    /**
     * Field _dsOrganizacionalGerc
     */
    private java.lang.String _dsOrganizacionalGerc;

    /**
     * Field _cdPessoaJuridNegocio
     */
    private long _cdPessoaJuridNegocio = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridNegocio
     */
    private boolean _has_cdPessoaJuridNegocio;

    /**
     * Field _nrUnidadeOrgnzOper
     */
    private int _nrUnidadeOrgnzOper = 0;

    /**
     * keeps track of state for field: _nrUnidadeOrgnzOper
     */
    private boolean _has_nrUnidadeOrgnzOper;

    /**
     * Field _dsOrganizacaoOperacional
     */
    private java.lang.String _dsOrganizacaoOperacional;

    /**
     * Field _cdTipoRelatPagamento
     */
    private int _cdTipoRelatPagamento = 0;

    /**
     * keeps track of state for field: _cdTipoRelatPagamento
     */
    private boolean _has_cdTipoRelatPagamento;

    /**
     * Field _dsTipoRelatorio
     */
    private java.lang.String _dsTipoRelatorio;

    /**
     * Field _cdSegmentoCliente
     */
    private int _cdSegmentoCliente = 0;

    /**
     * keeps track of state for field: _cdSegmentoCliente
     */
    private boolean _has_cdSegmentoCliente;

    /**
     * Field _dsSegmentoCliente
     */
    private java.lang.String _dsSegmentoCliente;

    /**
     * Field _cdGrpEconomicoCli
     */
    private long _cdGrpEconomicoCli = 0;

    /**
     * keeps track of state for field: _cdGrpEconomicoCli
     */
    private boolean _has_cdGrpEconomicoCli;

    /**
     * Field _dsGrupoEconomico
     */
    private java.lang.String _dsGrupoEconomico;

    /**
     * Field _cdClassAtividadeEconomica
     */
    private java.lang.String _cdClassAtividadeEconomica;

    /**
     * Field _dsClassAtividade
     */
    private java.lang.String _dsClassAtividade;

    /**
     * Field _cdRamoAtividadeEconomica
     */
    private int _cdRamoAtividadeEconomica = 0;

    /**
     * keeps track of state for field: _cdRamoAtividadeEconomica
     */
    private boolean _has_cdRamoAtividadeEconomica;

    /**
     * Field _dsRamoAtividadeEconomica
     */
    private java.lang.String _dsRamoAtividadeEconomica;

    /**
     * Field _cdSubRamoAtividadeEconomica
     */
    private int _cdSubRamoAtividadeEconomica = 0;

    /**
     * keeps track of state for field: _cdSubRamoAtividadeEconomica
     */
    private boolean _has_cdSubRamoAtividadeEconomica;

    /**
     * Field _dsRamoAtividade
     */
    private java.lang.String _dsRamoAtividade;

    /**
     * Field _cdAtividadeEconomica
     */
    private int _cdAtividadeEconomica = 0;

    /**
     * keeps track of state for field: _cdAtividadeEconomica
     */
    private boolean _has_cdAtividadeEconomica;

    /**
     * Field _dsAtividadeEconomica
     */
    private java.lang.String _dsAtividadeEconomica;

    /**
     * Field _cdUsuarioInclusao
     */
    private java.lang.String _cdUsuarioInclusao;

    /**
     * Field _cdUsuarioInclusaoExterno
     */
    private java.lang.String _cdUsuarioInclusaoExterno;

    /**
     * Field _dtInclusao
     */
    private java.lang.String _dtInclusao;

    /**
     * Field _hrInclusao
     */
    private java.lang.String _hrInclusao;

    /**
     * Field _cdOperacaoCanalInclusao
     */
    private java.lang.String _cdOperacaoCanalInclusao;

    /**
     * Field _cdTipoCanalInclusao
     */
    private int _cdTipoCanalInclusao = 0;

    /**
     * keeps track of state for field: _cdTipoCanalInclusao
     */
    private boolean _has_cdTipoCanalInclusao;

    /**
     * Field _dsCanalInclusao
     */
    private java.lang.String _dsCanalInclusao;

    /**
     * Field _cdUsuarioManutencao
     */
    private java.lang.String _cdUsuarioManutencao;

    /**
     * Field _cdUsuarioManutencaoExterno
     */
    private java.lang.String _cdUsuarioManutencaoExterno;

    /**
     * Field _dtManutencao
     */
    private java.lang.String _dtManutencao;

    /**
     * Field _hrManutencao
     */
    private java.lang.String _hrManutencao;

    /**
     * Field _cdOperacaoCanalManutencao
     */
    private java.lang.String _cdOperacaoCanalManutencao;

    /**
     * Field _cdTipoCanalManutencao
     */
    private int _cdTipoCanalManutencao = 0;

    /**
     * keeps track of state for field: _cdTipoCanalManutencao
     */
    private boolean _has_cdTipoCanalManutencao;

    /**
     * Field _dsCanalManutencao
     */
    private java.lang.String _dsCanalManutencao;

    /**
     * Field _dsEmpresa
     */
    private java.lang.String _dsEmpresa;


      //----------------/
     //- Constructors -/
    //----------------/

    public DetalharRelatorioContratosResponse() 
     {
        super();
        setCdPercentualTarifaSolic(new java.math.BigDecimal("0"));
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharrelatoriocontratos.response.DetalharRelatorioContratosResponse()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdAtividadeEconomica
     * 
     */
    public void deleteCdAtividadeEconomica()
    {
        this._has_cdAtividadeEconomica= false;
    } //-- void deleteCdAtividadeEconomica() 

    /**
     * Method deleteCdGrpEconomicoCli
     * 
     */
    public void deleteCdGrpEconomicoCli()
    {
        this._has_cdGrpEconomicoCli= false;
    } //-- void deleteCdGrpEconomicoCli() 

    /**
     * Method deleteCdMotivoSituacaoSolicitacao
     * 
     */
    public void deleteCdMotivoSituacaoSolicitacao()
    {
        this._has_cdMotivoSituacaoSolicitacao= false;
    } //-- void deleteCdMotivoSituacaoSolicitacao() 

    /**
     * Method deleteCdPessoaJuridDir
     * 
     */
    public void deleteCdPessoaJuridDir()
    {
        this._has_cdPessoaJuridDir= false;
    } //-- void deleteCdPessoaJuridDir() 

    /**
     * Method deleteCdPessoaJuridGerc
     * 
     */
    public void deleteCdPessoaJuridGerc()
    {
        this._has_cdPessoaJuridGerc= false;
    } //-- void deleteCdPessoaJuridGerc() 

    /**
     * Method deleteCdPessoaJuridNegocio
     * 
     */
    public void deleteCdPessoaJuridNegocio()
    {
        this._has_cdPessoaJuridNegocio= false;
    } //-- void deleteCdPessoaJuridNegocio() 

    /**
     * Method deleteCdProdutoOperRelacionado
     * 
     */
    public void deleteCdProdutoOperRelacionado()
    {
        this._has_cdProdutoOperRelacionado= false;
    } //-- void deleteCdProdutoOperRelacionado() 

    /**
     * Method deleteCdProdutoServicoOper
     * 
     */
    public void deleteCdProdutoServicoOper()
    {
        this._has_cdProdutoServicoOper= false;
    } //-- void deleteCdProdutoServicoOper() 

    /**
     * Method deleteCdRamoAtividadeEconomica
     * 
     */
    public void deleteCdRamoAtividadeEconomica()
    {
        this._has_cdRamoAtividadeEconomica= false;
    } //-- void deleteCdRamoAtividadeEconomica() 

    /**
     * Method deleteCdRelacionamentoProduto
     * 
     */
    public void deleteCdRelacionamentoProduto()
    {
        this._has_cdRelacionamentoProduto= false;
    } //-- void deleteCdRelacionamentoProduto() 

    /**
     * Method deleteCdSegmentoCliente
     * 
     */
    public void deleteCdSegmentoCliente()
    {
        this._has_cdSegmentoCliente= false;
    } //-- void deleteCdSegmentoCliente() 

    /**
     * Method deleteCdSituacaoSolicitacaoPagamento
     * 
     */
    public void deleteCdSituacaoSolicitacaoPagamento()
    {
        this._has_cdSituacaoSolicitacaoPagamento= false;
    } //-- void deleteCdSituacaoSolicitacaoPagamento() 

    /**
     * Method deleteCdSubRamoAtividadeEconomica
     * 
     */
    public void deleteCdSubRamoAtividadeEconomica()
    {
        this._has_cdSubRamoAtividadeEconomica= false;
    } //-- void deleteCdSubRamoAtividadeEconomica() 

    /**
     * Method deleteCdTipoCanalInclusao
     * 
     */
    public void deleteCdTipoCanalInclusao()
    {
        this._has_cdTipoCanalInclusao= false;
    } //-- void deleteCdTipoCanalInclusao() 

    /**
     * Method deleteCdTipoCanalManutencao
     * 
     */
    public void deleteCdTipoCanalManutencao()
    {
        this._has_cdTipoCanalManutencao= false;
    } //-- void deleteCdTipoCanalManutencao() 

    /**
     * Method deleteCdTipoRelatPagamento
     * 
     */
    public void deleteCdTipoRelatPagamento()
    {
        this._has_cdTipoRelatPagamento= false;
    } //-- void deleteCdTipoRelatPagamento() 

    /**
     * Method deleteNrSeqUnidadeOrgnzDir
     * 
     */
    public void deleteNrSeqUnidadeOrgnzDir()
    {
        this._has_nrSeqUnidadeOrgnzDir= false;
    } //-- void deleteNrSeqUnidadeOrgnzDir() 

    /**
     * Method deleteNrUnidadeOrgnzGerc
     * 
     */
    public void deleteNrUnidadeOrgnzGerc()
    {
        this._has_nrUnidadeOrgnzGerc= false;
    } //-- void deleteNrUnidadeOrgnzGerc() 

    /**
     * Method deleteNrUnidadeOrgnzOper
     * 
     */
    public void deleteNrUnidadeOrgnzOper()
    {
        this._has_nrUnidadeOrgnzOper= false;
    } //-- void deleteNrUnidadeOrgnzOper() 

    /**
     * Returns the value of field 'cdAtividadeEconomica'.
     * 
     * @return int
     * @return the value of field 'cdAtividadeEconomica'.
     */
    public int getCdAtividadeEconomica()
    {
        return this._cdAtividadeEconomica;
    } //-- int getCdAtividadeEconomica() 

    /**
     * Returns the value of field 'cdClassAtividadeEconomica'.
     * 
     * @return String
     * @return the value of field 'cdClassAtividadeEconomica'.
     */
    public java.lang.String getCdClassAtividadeEconomica()
    {
        return this._cdClassAtividadeEconomica;
    } //-- java.lang.String getCdClassAtividadeEconomica() 

    /**
     * Returns the value of field 'cdGrpEconomicoCli'.
     * 
     * @return long
     * @return the value of field 'cdGrpEconomicoCli'.
     */
    public long getCdGrpEconomicoCli()
    {
        return this._cdGrpEconomicoCli;
    } //-- long getCdGrpEconomicoCli() 

    /**
     * Returns the value of field 'cdMotivoSituacaoSolicitacao'.
     * 
     * @return int
     * @return the value of field 'cdMotivoSituacaoSolicitacao'.
     */
    public int getCdMotivoSituacaoSolicitacao()
    {
        return this._cdMotivoSituacaoSolicitacao;
    } //-- int getCdMotivoSituacaoSolicitacao() 

    /**
     * Returns the value of field 'cdOperacaoCanalInclusao'.
     * 
     * @return String
     * @return the value of field 'cdOperacaoCanalInclusao'.
     */
    public java.lang.String getCdOperacaoCanalInclusao()
    {
        return this._cdOperacaoCanalInclusao;
    } //-- java.lang.String getCdOperacaoCanalInclusao() 

    /**
     * Returns the value of field 'cdOperacaoCanalManutencao'.
     * 
     * @return String
     * @return the value of field 'cdOperacaoCanalManutencao'.
     */
    public java.lang.String getCdOperacaoCanalManutencao()
    {
        return this._cdOperacaoCanalManutencao;
    } //-- java.lang.String getCdOperacaoCanalManutencao() 

    /**
     * Returns the value of field 'cdPercentualTarifaSolic'.
     * 
     * @return BigDecimal
     * @return the value of field 'cdPercentualTarifaSolic'.
     */
    public java.math.BigDecimal getCdPercentualTarifaSolic()
    {
        return this._cdPercentualTarifaSolic;
    } //-- java.math.BigDecimal getCdPercentualTarifaSolic() 

    /**
     * Returns the value of field 'cdPessoaJuridDir'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridDir'.
     */
    public long getCdPessoaJuridDir()
    {
        return this._cdPessoaJuridDir;
    } //-- long getCdPessoaJuridDir() 

    /**
     * Returns the value of field 'cdPessoaJuridGerc'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridGerc'.
     */
    public long getCdPessoaJuridGerc()
    {
        return this._cdPessoaJuridGerc;
    } //-- long getCdPessoaJuridGerc() 

    /**
     * Returns the value of field 'cdPessoaJuridNegocio'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridNegocio'.
     */
    public long getCdPessoaJuridNegocio()
    {
        return this._cdPessoaJuridNegocio;
    } //-- long getCdPessoaJuridNegocio() 

    /**
     * Returns the value of field 'cdProdutoOperRelacionado'.
     * 
     * @return int
     * @return the value of field 'cdProdutoOperRelacionado'.
     */
    public int getCdProdutoOperRelacionado()
    {
        return this._cdProdutoOperRelacionado;
    } //-- int getCdProdutoOperRelacionado() 

    /**
     * Returns the value of field 'cdProdutoServicoOper'.
     * 
     * @return int
     * @return the value of field 'cdProdutoServicoOper'.
     */
    public int getCdProdutoServicoOper()
    {
        return this._cdProdutoServicoOper;
    } //-- int getCdProdutoServicoOper() 

    /**
     * Returns the value of field 'cdRamoAtividadeEconomica'.
     * 
     * @return int
     * @return the value of field 'cdRamoAtividadeEconomica'.
     */
    public int getCdRamoAtividadeEconomica()
    {
        return this._cdRamoAtividadeEconomica;
    } //-- int getCdRamoAtividadeEconomica() 

    /**
     * Returns the value of field 'cdRelacionamentoProduto'.
     * 
     * @return int
     * @return the value of field 'cdRelacionamentoProduto'.
     */
    public int getCdRelacionamentoProduto()
    {
        return this._cdRelacionamentoProduto;
    } //-- int getCdRelacionamentoProduto() 

    /**
     * Returns the value of field 'cdSegmentoCliente'.
     * 
     * @return int
     * @return the value of field 'cdSegmentoCliente'.
     */
    public int getCdSegmentoCliente()
    {
        return this._cdSegmentoCliente;
    } //-- int getCdSegmentoCliente() 

    /**
     * Returns the value of field 'cdSituacaoSolicitacaoPagamento'.
     * 
     * @return int
     * @return the value of field 'cdSituacaoSolicitacaoPagamento'.
     */
    public int getCdSituacaoSolicitacaoPagamento()
    {
        return this._cdSituacaoSolicitacaoPagamento;
    } //-- int getCdSituacaoSolicitacaoPagamento() 

    /**
     * Returns the value of field 'cdSubRamoAtividadeEconomica'.
     * 
     * @return int
     * @return the value of field 'cdSubRamoAtividadeEconomica'.
     */
    public int getCdSubRamoAtividadeEconomica()
    {
        return this._cdSubRamoAtividadeEconomica;
    } //-- int getCdSubRamoAtividadeEconomica() 

    /**
     * Returns the value of field 'cdTipoCanalInclusao'.
     * 
     * @return int
     * @return the value of field 'cdTipoCanalInclusao'.
     */
    public int getCdTipoCanalInclusao()
    {
        return this._cdTipoCanalInclusao;
    } //-- int getCdTipoCanalInclusao() 

    /**
     * Returns the value of field 'cdTipoCanalManutencao'.
     * 
     * @return int
     * @return the value of field 'cdTipoCanalManutencao'.
     */
    public int getCdTipoCanalManutencao()
    {
        return this._cdTipoCanalManutencao;
    } //-- int getCdTipoCanalManutencao() 

    /**
     * Returns the value of field 'cdTipoRelatPagamento'.
     * 
     * @return int
     * @return the value of field 'cdTipoRelatPagamento'.
     */
    public int getCdTipoRelatPagamento()
    {
        return this._cdTipoRelatPagamento;
    } //-- int getCdTipoRelatPagamento() 

    /**
     * Returns the value of field 'cdUsuarioInclusao'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioInclusao'.
     */
    public java.lang.String getCdUsuarioInclusao()
    {
        return this._cdUsuarioInclusao;
    } //-- java.lang.String getCdUsuarioInclusao() 

    /**
     * Returns the value of field 'cdUsuarioInclusaoExterno'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioInclusaoExterno'.
     */
    public java.lang.String getCdUsuarioInclusaoExterno()
    {
        return this._cdUsuarioInclusaoExterno;
    } //-- java.lang.String getCdUsuarioInclusaoExterno() 

    /**
     * Returns the value of field 'cdUsuarioManutencao'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioManutencao'.
     */
    public java.lang.String getCdUsuarioManutencao()
    {
        return this._cdUsuarioManutencao;
    } //-- java.lang.String getCdUsuarioManutencao() 

    /**
     * Returns the value of field 'cdUsuarioManutencaoExterno'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioManutencaoExterno'.
     */
    public java.lang.String getCdUsuarioManutencaoExterno()
    {
        return this._cdUsuarioManutencaoExterno;
    } //-- java.lang.String getCdUsuarioManutencaoExterno() 

    /**
     * Returns the value of field 'codMensagem'.
     * 
     * @return String
     * @return the value of field 'codMensagem'.
     */
    public java.lang.String getCodMensagem()
    {
        return this._codMensagem;
    } //-- java.lang.String getCodMensagem() 

    /**
     * Returns the value of field 'dsAtividadeEconomica'.
     * 
     * @return String
     * @return the value of field 'dsAtividadeEconomica'.
     */
    public java.lang.String getDsAtividadeEconomica()
    {
        return this._dsAtividadeEconomica;
    } //-- java.lang.String getDsAtividadeEconomica() 

    /**
     * Returns the value of field 'dsCanalInclusao'.
     * 
     * @return String
     * @return the value of field 'dsCanalInclusao'.
     */
    public java.lang.String getDsCanalInclusao()
    {
        return this._dsCanalInclusao;
    } //-- java.lang.String getDsCanalInclusao() 

    /**
     * Returns the value of field 'dsCanalManutencao'.
     * 
     * @return String
     * @return the value of field 'dsCanalManutencao'.
     */
    public java.lang.String getDsCanalManutencao()
    {
        return this._dsCanalManutencao;
    } //-- java.lang.String getDsCanalManutencao() 

    /**
     * Returns the value of field 'dsClassAtividade'.
     * 
     * @return String
     * @return the value of field 'dsClassAtividade'.
     */
    public java.lang.String getDsClassAtividade()
    {
        return this._dsClassAtividade;
    } //-- java.lang.String getDsClassAtividade() 

    /**
     * Returns the value of field 'dsEmpresa'.
     * 
     * @return String
     * @return the value of field 'dsEmpresa'.
     */
    public java.lang.String getDsEmpresa()
    {
        return this._dsEmpresa;
    } //-- java.lang.String getDsEmpresa() 

    /**
     * Returns the value of field 'dsGrupoEconomico'.
     * 
     * @return String
     * @return the value of field 'dsGrupoEconomico'.
     */
    public java.lang.String getDsGrupoEconomico()
    {
        return this._dsGrupoEconomico;
    } //-- java.lang.String getDsGrupoEconomico() 

    /**
     * Returns the value of field 'dsMotivoSolicitacao'.
     * 
     * @return String
     * @return the value of field 'dsMotivoSolicitacao'.
     */
    public java.lang.String getDsMotivoSolicitacao()
    {
        return this._dsMotivoSolicitacao;
    } //-- java.lang.String getDsMotivoSolicitacao() 

    /**
     * Returns the value of field 'dsOrganizacaoOperacional'.
     * 
     * @return String
     * @return the value of field 'dsOrganizacaoOperacional'.
     */
    public java.lang.String getDsOrganizacaoOperacional()
    {
        return this._dsOrganizacaoOperacional;
    } //-- java.lang.String getDsOrganizacaoOperacional() 

    /**
     * Returns the value of field 'dsOrganizacionalGerc'.
     * 
     * @return String
     * @return the value of field 'dsOrganizacionalGerc'.
     */
    public java.lang.String getDsOrganizacionalGerc()
    {
        return this._dsOrganizacionalGerc;
    } //-- java.lang.String getDsOrganizacionalGerc() 

    /**
     * Returns the value of field 'dsProdutoOperRelacionado'.
     * 
     * @return String
     * @return the value of field 'dsProdutoOperRelacionado'.
     */
    public java.lang.String getDsProdutoOperRelacionado()
    {
        return this._dsProdutoOperRelacionado;
    } //-- java.lang.String getDsProdutoOperRelacionado() 

    /**
     * Returns the value of field 'dsProdutoServicoOperacao'.
     * 
     * @return String
     * @return the value of field 'dsProdutoServicoOperacao'.
     */
    public java.lang.String getDsProdutoServicoOperacao()
    {
        return this._dsProdutoServicoOperacao;
    } //-- java.lang.String getDsProdutoServicoOperacao() 

    /**
     * Returns the value of field 'dsRamoAtividade'.
     * 
     * @return String
     * @return the value of field 'dsRamoAtividade'.
     */
    public java.lang.String getDsRamoAtividade()
    {
        return this._dsRamoAtividade;
    } //-- java.lang.String getDsRamoAtividade() 

    /**
     * Returns the value of field 'dsRamoAtividadeEconomica'.
     * 
     * @return String
     * @return the value of field 'dsRamoAtividadeEconomica'.
     */
    public java.lang.String getDsRamoAtividadeEconomica()
    {
        return this._dsRamoAtividadeEconomica;
    } //-- java.lang.String getDsRamoAtividadeEconomica() 

    /**
     * Returns the value of field 'dsSegmentoCliente'.
     * 
     * @return String
     * @return the value of field 'dsSegmentoCliente'.
     */
    public java.lang.String getDsSegmentoCliente()
    {
        return this._dsSegmentoCliente;
    } //-- java.lang.String getDsSegmentoCliente() 

    /**
     * Returns the value of field 'dsSolicitacaoPagamento'.
     * 
     * @return String
     * @return the value of field 'dsSolicitacaoPagamento'.
     */
    public java.lang.String getDsSolicitacaoPagamento()
    {
        return this._dsSolicitacaoPagamento;
    } //-- java.lang.String getDsSolicitacaoPagamento() 

    /**
     * Returns the value of field 'dsTipoRelatorio'.
     * 
     * @return String
     * @return the value of field 'dsTipoRelatorio'.
     */
    public java.lang.String getDsTipoRelatorio()
    {
        return this._dsTipoRelatorio;
    } //-- java.lang.String getDsTipoRelatorio() 

    /**
     * Returns the value of field 'dsUnidadeOrganizacionalDir'.
     * 
     * @return String
     * @return the value of field 'dsUnidadeOrganizacionalDir'.
     */
    public java.lang.String getDsUnidadeOrganizacionalDir()
    {
        return this._dsUnidadeOrganizacionalDir;
    } //-- java.lang.String getDsUnidadeOrganizacionalDir() 

    /**
     * Returns the value of field 'dtAtendimento'.
     * 
     * @return String
     * @return the value of field 'dtAtendimento'.
     */
    public java.lang.String getDtAtendimento()
    {
        return this._dtAtendimento;
    } //-- java.lang.String getDtAtendimento() 

    /**
     * Returns the value of field 'dtInclusao'.
     * 
     * @return String
     * @return the value of field 'dtInclusao'.
     */
    public java.lang.String getDtInclusao()
    {
        return this._dtInclusao;
    } //-- java.lang.String getDtInclusao() 

    /**
     * Returns the value of field 'dtManutencao'.
     * 
     * @return String
     * @return the value of field 'dtManutencao'.
     */
    public java.lang.String getDtManutencao()
    {
        return this._dtManutencao;
    } //-- java.lang.String getDtManutencao() 

    /**
     * Returns the value of field 'dtSolicitacao'.
     * 
     * @return String
     * @return the value of field 'dtSolicitacao'.
     */
    public java.lang.String getDtSolicitacao()
    {
        return this._dtSolicitacao;
    } //-- java.lang.String getDtSolicitacao() 

    /**
     * Returns the value of field 'hrAtendimento'.
     * 
     * @return String
     * @return the value of field 'hrAtendimento'.
     */
    public java.lang.String getHrAtendimento()
    {
        return this._hrAtendimento;
    } //-- java.lang.String getHrAtendimento() 

    /**
     * Returns the value of field 'hrInclusao'.
     * 
     * @return String
     * @return the value of field 'hrInclusao'.
     */
    public java.lang.String getHrInclusao()
    {
        return this._hrInclusao;
    } //-- java.lang.String getHrInclusao() 

    /**
     * Returns the value of field 'hrManutencao'.
     * 
     * @return String
     * @return the value of field 'hrManutencao'.
     */
    public java.lang.String getHrManutencao()
    {
        return this._hrManutencao;
    } //-- java.lang.String getHrManutencao() 

    /**
     * Returns the value of field 'hrSolicitacao'.
     * 
     * @return String
     * @return the value of field 'hrSolicitacao'.
     */
    public java.lang.String getHrSolicitacao()
    {
        return this._hrSolicitacao;
    } //-- java.lang.String getHrSolicitacao() 

    /**
     * Returns the value of field 'mensagem'.
     * 
     * @return String
     * @return the value of field 'mensagem'.
     */
    public java.lang.String getMensagem()
    {
        return this._mensagem;
    } //-- java.lang.String getMensagem() 

    /**
     * Returns the value of field 'nrSeqUnidadeOrgnzDir'.
     * 
     * @return int
     * @return the value of field 'nrSeqUnidadeOrgnzDir'.
     */
    public int getNrSeqUnidadeOrgnzDir()
    {
        return this._nrSeqUnidadeOrgnzDir;
    } //-- int getNrSeqUnidadeOrgnzDir() 

    /**
     * Returns the value of field 'nrUnidadeOrgnzGerc'.
     * 
     * @return int
     * @return the value of field 'nrUnidadeOrgnzGerc'.
     */
    public int getNrUnidadeOrgnzGerc()
    {
        return this._nrUnidadeOrgnzGerc;
    } //-- int getNrUnidadeOrgnzGerc() 

    /**
     * Returns the value of field 'nrUnidadeOrgnzOper'.
     * 
     * @return int
     * @return the value of field 'nrUnidadeOrgnzOper'.
     */
    public int getNrUnidadeOrgnzOper()
    {
        return this._nrUnidadeOrgnzOper;
    } //-- int getNrUnidadeOrgnzOper() 

    /**
     * Method hasCdAtividadeEconomica
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAtividadeEconomica()
    {
        return this._has_cdAtividadeEconomica;
    } //-- boolean hasCdAtividadeEconomica() 

    /**
     * Method hasCdGrpEconomicoCli
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdGrpEconomicoCli()
    {
        return this._has_cdGrpEconomicoCli;
    } //-- boolean hasCdGrpEconomicoCli() 

    /**
     * Method hasCdMotivoSituacaoSolicitacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMotivoSituacaoSolicitacao()
    {
        return this._has_cdMotivoSituacaoSolicitacao;
    } //-- boolean hasCdMotivoSituacaoSolicitacao() 

    /**
     * Method hasCdPessoaJuridDir
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridDir()
    {
        return this._has_cdPessoaJuridDir;
    } //-- boolean hasCdPessoaJuridDir() 

    /**
     * Method hasCdPessoaJuridGerc
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridGerc()
    {
        return this._has_cdPessoaJuridGerc;
    } //-- boolean hasCdPessoaJuridGerc() 

    /**
     * Method hasCdPessoaJuridNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridNegocio()
    {
        return this._has_cdPessoaJuridNegocio;
    } //-- boolean hasCdPessoaJuridNegocio() 

    /**
     * Method hasCdProdutoOperRelacionado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdutoOperRelacionado()
    {
        return this._has_cdProdutoOperRelacionado;
    } //-- boolean hasCdProdutoOperRelacionado() 

    /**
     * Method hasCdProdutoServicoOper
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdutoServicoOper()
    {
        return this._has_cdProdutoServicoOper;
    } //-- boolean hasCdProdutoServicoOper() 

    /**
     * Method hasCdRamoAtividadeEconomica
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdRamoAtividadeEconomica()
    {
        return this._has_cdRamoAtividadeEconomica;
    } //-- boolean hasCdRamoAtividadeEconomica() 

    /**
     * Method hasCdRelacionamentoProduto
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdRelacionamentoProduto()
    {
        return this._has_cdRelacionamentoProduto;
    } //-- boolean hasCdRelacionamentoProduto() 

    /**
     * Method hasCdSegmentoCliente
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSegmentoCliente()
    {
        return this._has_cdSegmentoCliente;
    } //-- boolean hasCdSegmentoCliente() 

    /**
     * Method hasCdSituacaoSolicitacaoPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSituacaoSolicitacaoPagamento()
    {
        return this._has_cdSituacaoSolicitacaoPagamento;
    } //-- boolean hasCdSituacaoSolicitacaoPagamento() 

    /**
     * Method hasCdSubRamoAtividadeEconomica
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSubRamoAtividadeEconomica()
    {
        return this._has_cdSubRamoAtividadeEconomica;
    } //-- boolean hasCdSubRamoAtividadeEconomica() 

    /**
     * Method hasCdTipoCanalInclusao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoCanalInclusao()
    {
        return this._has_cdTipoCanalInclusao;
    } //-- boolean hasCdTipoCanalInclusao() 

    /**
     * Method hasCdTipoCanalManutencao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoCanalManutencao()
    {
        return this._has_cdTipoCanalManutencao;
    } //-- boolean hasCdTipoCanalManutencao() 

    /**
     * Method hasCdTipoRelatPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoRelatPagamento()
    {
        return this._has_cdTipoRelatPagamento;
    } //-- boolean hasCdTipoRelatPagamento() 

    /**
     * Method hasNrSeqUnidadeOrgnzDir
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSeqUnidadeOrgnzDir()
    {
        return this._has_nrSeqUnidadeOrgnzDir;
    } //-- boolean hasNrSeqUnidadeOrgnzDir() 

    /**
     * Method hasNrUnidadeOrgnzGerc
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrUnidadeOrgnzGerc()
    {
        return this._has_nrUnidadeOrgnzGerc;
    } //-- boolean hasNrUnidadeOrgnzGerc() 

    /**
     * Method hasNrUnidadeOrgnzOper
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrUnidadeOrgnzOper()
    {
        return this._has_nrUnidadeOrgnzOper;
    } //-- boolean hasNrUnidadeOrgnzOper() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdAtividadeEconomica'.
     * 
     * @param cdAtividadeEconomica the value of field
     * 'cdAtividadeEconomica'.
     */
    public void setCdAtividadeEconomica(int cdAtividadeEconomica)
    {
        this._cdAtividadeEconomica = cdAtividadeEconomica;
        this._has_cdAtividadeEconomica = true;
    } //-- void setCdAtividadeEconomica(int) 

    /**
     * Sets the value of field 'cdClassAtividadeEconomica'.
     * 
     * @param cdClassAtividadeEconomica the value of field
     * 'cdClassAtividadeEconomica'.
     */
    public void setCdClassAtividadeEconomica(java.lang.String cdClassAtividadeEconomica)
    {
        this._cdClassAtividadeEconomica = cdClassAtividadeEconomica;
    } //-- void setCdClassAtividadeEconomica(java.lang.String) 

    /**
     * Sets the value of field 'cdGrpEconomicoCli'.
     * 
     * @param cdGrpEconomicoCli the value of field
     * 'cdGrpEconomicoCli'.
     */
    public void setCdGrpEconomicoCli(long cdGrpEconomicoCli)
    {
        this._cdGrpEconomicoCli = cdGrpEconomicoCli;
        this._has_cdGrpEconomicoCli = true;
    } //-- void setCdGrpEconomicoCli(long) 

    /**
     * Sets the value of field 'cdMotivoSituacaoSolicitacao'.
     * 
     * @param cdMotivoSituacaoSolicitacao the value of field
     * 'cdMotivoSituacaoSolicitacao'.
     */
    public void setCdMotivoSituacaoSolicitacao(int cdMotivoSituacaoSolicitacao)
    {
        this._cdMotivoSituacaoSolicitacao = cdMotivoSituacaoSolicitacao;
        this._has_cdMotivoSituacaoSolicitacao = true;
    } //-- void setCdMotivoSituacaoSolicitacao(int) 

    /**
     * Sets the value of field 'cdOperacaoCanalInclusao'.
     * 
     * @param cdOperacaoCanalInclusao the value of field
     * 'cdOperacaoCanalInclusao'.
     */
    public void setCdOperacaoCanalInclusao(java.lang.String cdOperacaoCanalInclusao)
    {
        this._cdOperacaoCanalInclusao = cdOperacaoCanalInclusao;
    } //-- void setCdOperacaoCanalInclusao(java.lang.String) 

    /**
     * Sets the value of field 'cdOperacaoCanalManutencao'.
     * 
     * @param cdOperacaoCanalManutencao the value of field
     * 'cdOperacaoCanalManutencao'.
     */
    public void setCdOperacaoCanalManutencao(java.lang.String cdOperacaoCanalManutencao)
    {
        this._cdOperacaoCanalManutencao = cdOperacaoCanalManutencao;
    } //-- void setCdOperacaoCanalManutencao(java.lang.String) 

    /**
     * Sets the value of field 'cdPercentualTarifaSolic'.
     * 
     * @param cdPercentualTarifaSolic the value of field
     * 'cdPercentualTarifaSolic'.
     */
    public void setCdPercentualTarifaSolic(java.math.BigDecimal cdPercentualTarifaSolic)
    {
        this._cdPercentualTarifaSolic = cdPercentualTarifaSolic;
    } //-- void setCdPercentualTarifaSolic(java.math.BigDecimal) 

    /**
     * Sets the value of field 'cdPessoaJuridDir'.
     * 
     * @param cdPessoaJuridDir the value of field 'cdPessoaJuridDir'
     */
    public void setCdPessoaJuridDir(long cdPessoaJuridDir)
    {
        this._cdPessoaJuridDir = cdPessoaJuridDir;
        this._has_cdPessoaJuridDir = true;
    } //-- void setCdPessoaJuridDir(long) 

    /**
     * Sets the value of field 'cdPessoaJuridGerc'.
     * 
     * @param cdPessoaJuridGerc the value of field
     * 'cdPessoaJuridGerc'.
     */
    public void setCdPessoaJuridGerc(long cdPessoaJuridGerc)
    {
        this._cdPessoaJuridGerc = cdPessoaJuridGerc;
        this._has_cdPessoaJuridGerc = true;
    } //-- void setCdPessoaJuridGerc(long) 

    /**
     * Sets the value of field 'cdPessoaJuridNegocio'.
     * 
     * @param cdPessoaJuridNegocio the value of field
     * 'cdPessoaJuridNegocio'.
     */
    public void setCdPessoaJuridNegocio(long cdPessoaJuridNegocio)
    {
        this._cdPessoaJuridNegocio = cdPessoaJuridNegocio;
        this._has_cdPessoaJuridNegocio = true;
    } //-- void setCdPessoaJuridNegocio(long) 

    /**
     * Sets the value of field 'cdProdutoOperRelacionado'.
     * 
     * @param cdProdutoOperRelacionado the value of field
     * 'cdProdutoOperRelacionado'.
     */
    public void setCdProdutoOperRelacionado(int cdProdutoOperRelacionado)
    {
        this._cdProdutoOperRelacionado = cdProdutoOperRelacionado;
        this._has_cdProdutoOperRelacionado = true;
    } //-- void setCdProdutoOperRelacionado(int) 

    /**
     * Sets the value of field 'cdProdutoServicoOper'.
     * 
     * @param cdProdutoServicoOper the value of field
     * 'cdProdutoServicoOper'.
     */
    public void setCdProdutoServicoOper(int cdProdutoServicoOper)
    {
        this._cdProdutoServicoOper = cdProdutoServicoOper;
        this._has_cdProdutoServicoOper = true;
    } //-- void setCdProdutoServicoOper(int) 

    /**
     * Sets the value of field 'cdRamoAtividadeEconomica'.
     * 
     * @param cdRamoAtividadeEconomica the value of field
     * 'cdRamoAtividadeEconomica'.
     */
    public void setCdRamoAtividadeEconomica(int cdRamoAtividadeEconomica)
    {
        this._cdRamoAtividadeEconomica = cdRamoAtividadeEconomica;
        this._has_cdRamoAtividadeEconomica = true;
    } //-- void setCdRamoAtividadeEconomica(int) 

    /**
     * Sets the value of field 'cdRelacionamentoProduto'.
     * 
     * @param cdRelacionamentoProduto the value of field
     * 'cdRelacionamentoProduto'.
     */
    public void setCdRelacionamentoProduto(int cdRelacionamentoProduto)
    {
        this._cdRelacionamentoProduto = cdRelacionamentoProduto;
        this._has_cdRelacionamentoProduto = true;
    } //-- void setCdRelacionamentoProduto(int) 

    /**
     * Sets the value of field 'cdSegmentoCliente'.
     * 
     * @param cdSegmentoCliente the value of field
     * 'cdSegmentoCliente'.
     */
    public void setCdSegmentoCliente(int cdSegmentoCliente)
    {
        this._cdSegmentoCliente = cdSegmentoCliente;
        this._has_cdSegmentoCliente = true;
    } //-- void setCdSegmentoCliente(int) 

    /**
     * Sets the value of field 'cdSituacaoSolicitacaoPagamento'.
     * 
     * @param cdSituacaoSolicitacaoPagamento the value of field
     * 'cdSituacaoSolicitacaoPagamento'.
     */
    public void setCdSituacaoSolicitacaoPagamento(int cdSituacaoSolicitacaoPagamento)
    {
        this._cdSituacaoSolicitacaoPagamento = cdSituacaoSolicitacaoPagamento;
        this._has_cdSituacaoSolicitacaoPagamento = true;
    } //-- void setCdSituacaoSolicitacaoPagamento(int) 

    /**
     * Sets the value of field 'cdSubRamoAtividadeEconomica'.
     * 
     * @param cdSubRamoAtividadeEconomica the value of field
     * 'cdSubRamoAtividadeEconomica'.
     */
    public void setCdSubRamoAtividadeEconomica(int cdSubRamoAtividadeEconomica)
    {
        this._cdSubRamoAtividadeEconomica = cdSubRamoAtividadeEconomica;
        this._has_cdSubRamoAtividadeEconomica = true;
    } //-- void setCdSubRamoAtividadeEconomica(int) 

    /**
     * Sets the value of field 'cdTipoCanalInclusao'.
     * 
     * @param cdTipoCanalInclusao the value of field
     * 'cdTipoCanalInclusao'.
     */
    public void setCdTipoCanalInclusao(int cdTipoCanalInclusao)
    {
        this._cdTipoCanalInclusao = cdTipoCanalInclusao;
        this._has_cdTipoCanalInclusao = true;
    } //-- void setCdTipoCanalInclusao(int) 

    /**
     * Sets the value of field 'cdTipoCanalManutencao'.
     * 
     * @param cdTipoCanalManutencao the value of field
     * 'cdTipoCanalManutencao'.
     */
    public void setCdTipoCanalManutencao(int cdTipoCanalManutencao)
    {
        this._cdTipoCanalManutencao = cdTipoCanalManutencao;
        this._has_cdTipoCanalManutencao = true;
    } //-- void setCdTipoCanalManutencao(int) 

    /**
     * Sets the value of field 'cdTipoRelatPagamento'.
     * 
     * @param cdTipoRelatPagamento the value of field
     * 'cdTipoRelatPagamento'.
     */
    public void setCdTipoRelatPagamento(int cdTipoRelatPagamento)
    {
        this._cdTipoRelatPagamento = cdTipoRelatPagamento;
        this._has_cdTipoRelatPagamento = true;
    } //-- void setCdTipoRelatPagamento(int) 

    /**
     * Sets the value of field 'cdUsuarioInclusao'.
     * 
     * @param cdUsuarioInclusao the value of field
     * 'cdUsuarioInclusao'.
     */
    public void setCdUsuarioInclusao(java.lang.String cdUsuarioInclusao)
    {
        this._cdUsuarioInclusao = cdUsuarioInclusao;
    } //-- void setCdUsuarioInclusao(java.lang.String) 

    /**
     * Sets the value of field 'cdUsuarioInclusaoExterno'.
     * 
     * @param cdUsuarioInclusaoExterno the value of field
     * 'cdUsuarioInclusaoExterno'.
     */
    public void setCdUsuarioInclusaoExterno(java.lang.String cdUsuarioInclusaoExterno)
    {
        this._cdUsuarioInclusaoExterno = cdUsuarioInclusaoExterno;
    } //-- void setCdUsuarioInclusaoExterno(java.lang.String) 

    /**
     * Sets the value of field 'cdUsuarioManutencao'.
     * 
     * @param cdUsuarioManutencao the value of field
     * 'cdUsuarioManutencao'.
     */
    public void setCdUsuarioManutencao(java.lang.String cdUsuarioManutencao)
    {
        this._cdUsuarioManutencao = cdUsuarioManutencao;
    } //-- void setCdUsuarioManutencao(java.lang.String) 

    /**
     * Sets the value of field 'cdUsuarioManutencaoExterno'.
     * 
     * @param cdUsuarioManutencaoExterno the value of field
     * 'cdUsuarioManutencaoExterno'.
     */
    public void setCdUsuarioManutencaoExterno(java.lang.String cdUsuarioManutencaoExterno)
    {
        this._cdUsuarioManutencaoExterno = cdUsuarioManutencaoExterno;
    } //-- void setCdUsuarioManutencaoExterno(java.lang.String) 

    /**
     * Sets the value of field 'codMensagem'.
     * 
     * @param codMensagem the value of field 'codMensagem'.
     */
    public void setCodMensagem(java.lang.String codMensagem)
    {
        this._codMensagem = codMensagem;
    } //-- void setCodMensagem(java.lang.String) 

    /**
     * Sets the value of field 'dsAtividadeEconomica'.
     * 
     * @param dsAtividadeEconomica the value of field
     * 'dsAtividadeEconomica'.
     */
    public void setDsAtividadeEconomica(java.lang.String dsAtividadeEconomica)
    {
        this._dsAtividadeEconomica = dsAtividadeEconomica;
    } //-- void setDsAtividadeEconomica(java.lang.String) 

    /**
     * Sets the value of field 'dsCanalInclusao'.
     * 
     * @param dsCanalInclusao the value of field 'dsCanalInclusao'.
     */
    public void setDsCanalInclusao(java.lang.String dsCanalInclusao)
    {
        this._dsCanalInclusao = dsCanalInclusao;
    } //-- void setDsCanalInclusao(java.lang.String) 

    /**
     * Sets the value of field 'dsCanalManutencao'.
     * 
     * @param dsCanalManutencao the value of field
     * 'dsCanalManutencao'.
     */
    public void setDsCanalManutencao(java.lang.String dsCanalManutencao)
    {
        this._dsCanalManutencao = dsCanalManutencao;
    } //-- void setDsCanalManutencao(java.lang.String) 

    /**
     * Sets the value of field 'dsClassAtividade'.
     * 
     * @param dsClassAtividade the value of field 'dsClassAtividade'
     */
    public void setDsClassAtividade(java.lang.String dsClassAtividade)
    {
        this._dsClassAtividade = dsClassAtividade;
    } //-- void setDsClassAtividade(java.lang.String) 

    /**
     * Sets the value of field 'dsEmpresa'.
     * 
     * @param dsEmpresa the value of field 'dsEmpresa'.
     */
    public void setDsEmpresa(java.lang.String dsEmpresa)
    {
        this._dsEmpresa = dsEmpresa;
    } //-- void setDsEmpresa(java.lang.String) 

    /**
     * Sets the value of field 'dsGrupoEconomico'.
     * 
     * @param dsGrupoEconomico the value of field 'dsGrupoEconomico'
     */
    public void setDsGrupoEconomico(java.lang.String dsGrupoEconomico)
    {
        this._dsGrupoEconomico = dsGrupoEconomico;
    } //-- void setDsGrupoEconomico(java.lang.String) 

    /**
     * Sets the value of field 'dsMotivoSolicitacao'.
     * 
     * @param dsMotivoSolicitacao the value of field
     * 'dsMotivoSolicitacao'.
     */
    public void setDsMotivoSolicitacao(java.lang.String dsMotivoSolicitacao)
    {
        this._dsMotivoSolicitacao = dsMotivoSolicitacao;
    } //-- void setDsMotivoSolicitacao(java.lang.String) 

    /**
     * Sets the value of field 'dsOrganizacaoOperacional'.
     * 
     * @param dsOrganizacaoOperacional the value of field
     * 'dsOrganizacaoOperacional'.
     */
    public void setDsOrganizacaoOperacional(java.lang.String dsOrganizacaoOperacional)
    {
        this._dsOrganizacaoOperacional = dsOrganizacaoOperacional;
    } //-- void setDsOrganizacaoOperacional(java.lang.String) 

    /**
     * Sets the value of field 'dsOrganizacionalGerc'.
     * 
     * @param dsOrganizacionalGerc the value of field
     * 'dsOrganizacionalGerc'.
     */
    public void setDsOrganizacionalGerc(java.lang.String dsOrganizacionalGerc)
    {
        this._dsOrganizacionalGerc = dsOrganizacionalGerc;
    } //-- void setDsOrganizacionalGerc(java.lang.String) 

    /**
     * Sets the value of field 'dsProdutoOperRelacionado'.
     * 
     * @param dsProdutoOperRelacionado the value of field
     * 'dsProdutoOperRelacionado'.
     */
    public void setDsProdutoOperRelacionado(java.lang.String dsProdutoOperRelacionado)
    {
        this._dsProdutoOperRelacionado = dsProdutoOperRelacionado;
    } //-- void setDsProdutoOperRelacionado(java.lang.String) 

    /**
     * Sets the value of field 'dsProdutoServicoOperacao'.
     * 
     * @param dsProdutoServicoOperacao the value of field
     * 'dsProdutoServicoOperacao'.
     */
    public void setDsProdutoServicoOperacao(java.lang.String dsProdutoServicoOperacao)
    {
        this._dsProdutoServicoOperacao = dsProdutoServicoOperacao;
    } //-- void setDsProdutoServicoOperacao(java.lang.String) 

    /**
     * Sets the value of field 'dsRamoAtividade'.
     * 
     * @param dsRamoAtividade the value of field 'dsRamoAtividade'.
     */
    public void setDsRamoAtividade(java.lang.String dsRamoAtividade)
    {
        this._dsRamoAtividade = dsRamoAtividade;
    } //-- void setDsRamoAtividade(java.lang.String) 

    /**
     * Sets the value of field 'dsRamoAtividadeEconomica'.
     * 
     * @param dsRamoAtividadeEconomica the value of field
     * 'dsRamoAtividadeEconomica'.
     */
    public void setDsRamoAtividadeEconomica(java.lang.String dsRamoAtividadeEconomica)
    {
        this._dsRamoAtividadeEconomica = dsRamoAtividadeEconomica;
    } //-- void setDsRamoAtividadeEconomica(java.lang.String) 

    /**
     * Sets the value of field 'dsSegmentoCliente'.
     * 
     * @param dsSegmentoCliente the value of field
     * 'dsSegmentoCliente'.
     */
    public void setDsSegmentoCliente(java.lang.String dsSegmentoCliente)
    {
        this._dsSegmentoCliente = dsSegmentoCliente;
    } //-- void setDsSegmentoCliente(java.lang.String) 

    /**
     * Sets the value of field 'dsSolicitacaoPagamento'.
     * 
     * @param dsSolicitacaoPagamento the value of field
     * 'dsSolicitacaoPagamento'.
     */
    public void setDsSolicitacaoPagamento(java.lang.String dsSolicitacaoPagamento)
    {
        this._dsSolicitacaoPagamento = dsSolicitacaoPagamento;
    } //-- void setDsSolicitacaoPagamento(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoRelatorio'.
     * 
     * @param dsTipoRelatorio the value of field 'dsTipoRelatorio'.
     */
    public void setDsTipoRelatorio(java.lang.String dsTipoRelatorio)
    {
        this._dsTipoRelatorio = dsTipoRelatorio;
    } //-- void setDsTipoRelatorio(java.lang.String) 

    /**
     * Sets the value of field 'dsUnidadeOrganizacionalDir'.
     * 
     * @param dsUnidadeOrganizacionalDir the value of field
     * 'dsUnidadeOrganizacionalDir'.
     */
    public void setDsUnidadeOrganizacionalDir(java.lang.String dsUnidadeOrganizacionalDir)
    {
        this._dsUnidadeOrganizacionalDir = dsUnidadeOrganizacionalDir;
    } //-- void setDsUnidadeOrganizacionalDir(java.lang.String) 

    /**
     * Sets the value of field 'dtAtendimento'.
     * 
     * @param dtAtendimento the value of field 'dtAtendimento'.
     */
    public void setDtAtendimento(java.lang.String dtAtendimento)
    {
        this._dtAtendimento = dtAtendimento;
    } //-- void setDtAtendimento(java.lang.String) 

    /**
     * Sets the value of field 'dtInclusao'.
     * 
     * @param dtInclusao the value of field 'dtInclusao'.
     */
    public void setDtInclusao(java.lang.String dtInclusao)
    {
        this._dtInclusao = dtInclusao;
    } //-- void setDtInclusao(java.lang.String) 

    /**
     * Sets the value of field 'dtManutencao'.
     * 
     * @param dtManutencao the value of field 'dtManutencao'.
     */
    public void setDtManutencao(java.lang.String dtManutencao)
    {
        this._dtManutencao = dtManutencao;
    } //-- void setDtManutencao(java.lang.String) 

    /**
     * Sets the value of field 'dtSolicitacao'.
     * 
     * @param dtSolicitacao the value of field 'dtSolicitacao'.
     */
    public void setDtSolicitacao(java.lang.String dtSolicitacao)
    {
        this._dtSolicitacao = dtSolicitacao;
    } //-- void setDtSolicitacao(java.lang.String) 

    /**
     * Sets the value of field 'hrAtendimento'.
     * 
     * @param hrAtendimento the value of field 'hrAtendimento'.
     */
    public void setHrAtendimento(java.lang.String hrAtendimento)
    {
        this._hrAtendimento = hrAtendimento;
    } //-- void setHrAtendimento(java.lang.String) 

    /**
     * Sets the value of field 'hrInclusao'.
     * 
     * @param hrInclusao the value of field 'hrInclusao'.
     */
    public void setHrInclusao(java.lang.String hrInclusao)
    {
        this._hrInclusao = hrInclusao;
    } //-- void setHrInclusao(java.lang.String) 

    /**
     * Sets the value of field 'hrManutencao'.
     * 
     * @param hrManutencao the value of field 'hrManutencao'.
     */
    public void setHrManutencao(java.lang.String hrManutencao)
    {
        this._hrManutencao = hrManutencao;
    } //-- void setHrManutencao(java.lang.String) 

    /**
     * Sets the value of field 'hrSolicitacao'.
     * 
     * @param hrSolicitacao the value of field 'hrSolicitacao'.
     */
    public void setHrSolicitacao(java.lang.String hrSolicitacao)
    {
        this._hrSolicitacao = hrSolicitacao;
    } //-- void setHrSolicitacao(java.lang.String) 

    /**
     * Sets the value of field 'mensagem'.
     * 
     * @param mensagem the value of field 'mensagem'.
     */
    public void setMensagem(java.lang.String mensagem)
    {
        this._mensagem = mensagem;
    } //-- void setMensagem(java.lang.String) 

    /**
     * Sets the value of field 'nrSeqUnidadeOrgnzDir'.
     * 
     * @param nrSeqUnidadeOrgnzDir the value of field
     * 'nrSeqUnidadeOrgnzDir'.
     */
    public void setNrSeqUnidadeOrgnzDir(int nrSeqUnidadeOrgnzDir)
    {
        this._nrSeqUnidadeOrgnzDir = nrSeqUnidadeOrgnzDir;
        this._has_nrSeqUnidadeOrgnzDir = true;
    } //-- void setNrSeqUnidadeOrgnzDir(int) 

    /**
     * Sets the value of field 'nrUnidadeOrgnzGerc'.
     * 
     * @param nrUnidadeOrgnzGerc the value of field
     * 'nrUnidadeOrgnzGerc'.
     */
    public void setNrUnidadeOrgnzGerc(int nrUnidadeOrgnzGerc)
    {
        this._nrUnidadeOrgnzGerc = nrUnidadeOrgnzGerc;
        this._has_nrUnidadeOrgnzGerc = true;
    } //-- void setNrUnidadeOrgnzGerc(int) 

    /**
     * Sets the value of field 'nrUnidadeOrgnzOper'.
     * 
     * @param nrUnidadeOrgnzOper the value of field
     * 'nrUnidadeOrgnzOper'.
     */
    public void setNrUnidadeOrgnzOper(int nrUnidadeOrgnzOper)
    {
        this._nrUnidadeOrgnzOper = nrUnidadeOrgnzOper;
        this._has_nrUnidadeOrgnzOper = true;
    } //-- void setNrUnidadeOrgnzOper(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return DetalharRelatorioContratosResponse
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.detalharrelatoriocontratos.response.DetalharRelatorioContratosResponse unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.detalharrelatoriocontratos.response.DetalharRelatorioContratosResponse) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.detalharrelatoriocontratos.response.DetalharRelatorioContratosResponse.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharrelatoriocontratos.response.DetalharRelatorioContratosResponse unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
