/*
 * Nome: br.com.bradesco.web.pgit.service.business.combo.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.combo.bean;

/**
 * Nome: ListaServicoCompostoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListaServicoCompostoSaidaDTO {

	/** Atributo cdServicoComposto. */
	private Long cdServicoComposto;

	/** Atributo dsServicoComposto. */
	private String dsServicoComposto;

	/**
	 * Lista servico composto saida dto.
	 */
	public ListaServicoCompostoSaidaDTO() {
		super();
	}

	/**
	 * Lista servico composto saida dto.
	 *
	 * @param cdServicoComposto the cd servico composto
	 * @param dsServicoComposto the ds servico composto
	 */
	public ListaServicoCompostoSaidaDTO(Long cdServicoComposto, String dsServicoComposto) {
		super();
		this.cdServicoComposto = cdServicoComposto;
		this.dsServicoComposto = dsServicoComposto;
	}

	/**
	 * Get: cdServicoComposto.
	 *
	 * @return cdServicoComposto
	 */
	public Long getCdServicoComposto() {
		return cdServicoComposto;
	}

	/**
	 * Set: cdServicoComposto.
	 *
	 * @param cdServicoComposto the cd servico composto
	 */
	public void setCdServicoComposto(Long cdServicoComposto) {
		this.cdServicoComposto = cdServicoComposto;
	}

	/**
	 * Get: dsServicoComposto.
	 *
	 * @return dsServicoComposto
	 */
	public String getDsServicoComposto() {
		return dsServicoComposto;
	}

	/**
	 * Set: dsServicoComposto.
	 *
	 * @param dsServicoComposto the ds servico composto
	 */
	public void setDsServicoComposto(String dsServicoComposto) {
		this.dsServicoComposto = dsServicoComposto;
	}
}
