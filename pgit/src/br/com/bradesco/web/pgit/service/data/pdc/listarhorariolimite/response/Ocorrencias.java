/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listarhorariolimite.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdModalidade
     */
    private int _cdModalidade = 0;

    /**
     * keeps track of state for field: _cdModalidade
     */
    private boolean _has_cdModalidade;

    /**
     * Field _dsModalidade
     */
    private java.lang.String _dsModalidade;

    /**
     * Field _cdSituacaoTipoModalidade
     */
    private int _cdSituacaoTipoModalidade = 0;

    /**
     * keeps track of state for field: _cdSituacaoTipoModalidade
     */
    private boolean _has_cdSituacaoTipoModalidade;

    /**
     * Field _cdSequencialOrdemTipoModalidade
     */
    private int _cdSequencialOrdemTipoModalidade = 0;

    /**
     * keeps track of state for field:
     * _cdSequencialOrdemTipoModalidade
     */
    private boolean _has_cdSequencialOrdemTipoModalidade;

    /**
     * Field _hrLimiteAutorizacao
     */
    private java.lang.String _hrLimiteAutorizacao;

    /**
     * Field _hrLimiteTransmissao
     */
    private java.lang.String _hrLimiteTransmissao;

    /**
     * Field _hrLimiteManutencao
     */
    private java.lang.String _hrLimiteManutencao;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarhorariolimite.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdModalidade
     * 
     */
    public void deleteCdModalidade()
    {
        this._has_cdModalidade= false;
    } //-- void deleteCdModalidade() 

    /**
     * Method deleteCdSequencialOrdemTipoModalidade
     * 
     */
    public void deleteCdSequencialOrdemTipoModalidade()
    {
        this._has_cdSequencialOrdemTipoModalidade= false;
    } //-- void deleteCdSequencialOrdemTipoModalidade() 

    /**
     * Method deleteCdSituacaoTipoModalidade
     * 
     */
    public void deleteCdSituacaoTipoModalidade()
    {
        this._has_cdSituacaoTipoModalidade= false;
    } //-- void deleteCdSituacaoTipoModalidade() 

    /**
     * Returns the value of field 'cdModalidade'.
     * 
     * @return int
     * @return the value of field 'cdModalidade'.
     */
    public int getCdModalidade()
    {
        return this._cdModalidade;
    } //-- int getCdModalidade() 

    /**
     * Returns the value of field
     * 'cdSequencialOrdemTipoModalidade'.
     * 
     * @return int
     * @return the value of field 'cdSequencialOrdemTipoModalidade'.
     */
    public int getCdSequencialOrdemTipoModalidade()
    {
        return this._cdSequencialOrdemTipoModalidade;
    } //-- int getCdSequencialOrdemTipoModalidade() 

    /**
     * Returns the value of field 'cdSituacaoTipoModalidade'.
     * 
     * @return int
     * @return the value of field 'cdSituacaoTipoModalidade'.
     */
    public int getCdSituacaoTipoModalidade()
    {
        return this._cdSituacaoTipoModalidade;
    } //-- int getCdSituacaoTipoModalidade() 

    /**
     * Returns the value of field 'dsModalidade'.
     * 
     * @return String
     * @return the value of field 'dsModalidade'.
     */
    public java.lang.String getDsModalidade()
    {
        return this._dsModalidade;
    } //-- java.lang.String getDsModalidade() 

    /**
     * Returns the value of field 'hrLimiteAutorizacao'.
     * 
     * @return String
     * @return the value of field 'hrLimiteAutorizacao'.
     */
    public java.lang.String getHrLimiteAutorizacao()
    {
        return this._hrLimiteAutorizacao;
    } //-- java.lang.String getHrLimiteAutorizacao() 

    /**
     * Returns the value of field 'hrLimiteManutencao'.
     * 
     * @return String
     * @return the value of field 'hrLimiteManutencao'.
     */
    public java.lang.String getHrLimiteManutencao()
    {
        return this._hrLimiteManutencao;
    } //-- java.lang.String getHrLimiteManutencao() 

    /**
     * Returns the value of field 'hrLimiteTransmissao'.
     * 
     * @return String
     * @return the value of field 'hrLimiteTransmissao'.
     */
    public java.lang.String getHrLimiteTransmissao()
    {
        return this._hrLimiteTransmissao;
    } //-- java.lang.String getHrLimiteTransmissao() 

    /**
     * Method hasCdModalidade
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdModalidade()
    {
        return this._has_cdModalidade;
    } //-- boolean hasCdModalidade() 

    /**
     * Method hasCdSequencialOrdemTipoModalidade
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSequencialOrdemTipoModalidade()
    {
        return this._has_cdSequencialOrdemTipoModalidade;
    } //-- boolean hasCdSequencialOrdemTipoModalidade() 

    /**
     * Method hasCdSituacaoTipoModalidade
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSituacaoTipoModalidade()
    {
        return this._has_cdSituacaoTipoModalidade;
    } //-- boolean hasCdSituacaoTipoModalidade() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdModalidade'.
     * 
     * @param cdModalidade the value of field 'cdModalidade'.
     */
    public void setCdModalidade(int cdModalidade)
    {
        this._cdModalidade = cdModalidade;
        this._has_cdModalidade = true;
    } //-- void setCdModalidade(int) 

    /**
     * Sets the value of field 'cdSequencialOrdemTipoModalidade'.
     * 
     * @param cdSequencialOrdemTipoModalidade the value of field
     * 'cdSequencialOrdemTipoModalidade'.
     */
    public void setCdSequencialOrdemTipoModalidade(int cdSequencialOrdemTipoModalidade)
    {
        this._cdSequencialOrdemTipoModalidade = cdSequencialOrdemTipoModalidade;
        this._has_cdSequencialOrdemTipoModalidade = true;
    } //-- void setCdSequencialOrdemTipoModalidade(int) 

    /**
     * Sets the value of field 'cdSituacaoTipoModalidade'.
     * 
     * @param cdSituacaoTipoModalidade the value of field
     * 'cdSituacaoTipoModalidade'.
     */
    public void setCdSituacaoTipoModalidade(int cdSituacaoTipoModalidade)
    {
        this._cdSituacaoTipoModalidade = cdSituacaoTipoModalidade;
        this._has_cdSituacaoTipoModalidade = true;
    } //-- void setCdSituacaoTipoModalidade(int) 

    /**
     * Sets the value of field 'dsModalidade'.
     * 
     * @param dsModalidade the value of field 'dsModalidade'.
     */
    public void setDsModalidade(java.lang.String dsModalidade)
    {
        this._dsModalidade = dsModalidade;
    } //-- void setDsModalidade(java.lang.String) 

    /**
     * Sets the value of field 'hrLimiteAutorizacao'.
     * 
     * @param hrLimiteAutorizacao the value of field
     * 'hrLimiteAutorizacao'.
     */
    public void setHrLimiteAutorizacao(java.lang.String hrLimiteAutorizacao)
    {
        this._hrLimiteAutorizacao = hrLimiteAutorizacao;
    } //-- void setHrLimiteAutorizacao(java.lang.String) 

    /**
     * Sets the value of field 'hrLimiteManutencao'.
     * 
     * @param hrLimiteManutencao the value of field
     * 'hrLimiteManutencao'.
     */
    public void setHrLimiteManutencao(java.lang.String hrLimiteManutencao)
    {
        this._hrLimiteManutencao = hrLimiteManutencao;
    } //-- void setHrLimiteManutencao(java.lang.String) 

    /**
     * Sets the value of field 'hrLimiteTransmissao'.
     * 
     * @param hrLimiteTransmissao the value of field
     * 'hrLimiteTransmissao'.
     */
    public void setHrLimiteTransmissao(java.lang.String hrLimiteTransmissao)
    {
        this._hrLimiteTransmissao = hrLimiteTransmissao;
    } //-- void setHrLimiteTransmissao(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listarhorariolimite.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listarhorariolimite.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listarhorariolimite.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarhorariolimite.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
