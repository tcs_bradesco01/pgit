/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.excluirorgaopagador.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ExcluirOrgaoPagadorRequest.
 * 
 * @version $Revision$ $Date$
 */
public class ExcluirOrgaoPagadorRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdOrgaoPagador
     */
    private int _cdOrgaoPagador = 0;

    /**
     * keeps track of state for field: _cdOrgaoPagador
     */
    private boolean _has_cdOrgaoPagador;

    /**
     * Field _hrInclusaoRegistro
     */
    private java.lang.String _hrInclusaoRegistro;


      //----------------/
     //- Constructors -/
    //----------------/

    public ExcluirOrgaoPagadorRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.excluirorgaopagador.request.ExcluirOrgaoPagadorRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdOrgaoPagador
     * 
     */
    public void deleteCdOrgaoPagador()
    {
        this._has_cdOrgaoPagador= false;
    } //-- void deleteCdOrgaoPagador() 

    /**
     * Returns the value of field 'cdOrgaoPagador'.
     * 
     * @return int
     * @return the value of field 'cdOrgaoPagador'.
     */
    public int getCdOrgaoPagador()
    {
        return this._cdOrgaoPagador;
    } //-- int getCdOrgaoPagador() 

    /**
     * Returns the value of field 'hrInclusaoRegistro'.
     * 
     * @return String
     * @return the value of field 'hrInclusaoRegistro'.
     */
    public java.lang.String getHrInclusaoRegistro()
    {
        return this._hrInclusaoRegistro;
    } //-- java.lang.String getHrInclusaoRegistro() 

    /**
     * Method hasCdOrgaoPagador
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdOrgaoPagador()
    {
        return this._has_cdOrgaoPagador;
    } //-- boolean hasCdOrgaoPagador() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdOrgaoPagador'.
     * 
     * @param cdOrgaoPagador the value of field 'cdOrgaoPagador'.
     */
    public void setCdOrgaoPagador(int cdOrgaoPagador)
    {
        this._cdOrgaoPagador = cdOrgaoPagador;
        this._has_cdOrgaoPagador = true;
    } //-- void setCdOrgaoPagador(int) 

    /**
     * Sets the value of field 'hrInclusaoRegistro'.
     * 
     * @param hrInclusaoRegistro the value of field
     * 'hrInclusaoRegistro'.
     */
    public void setHrInclusaoRegistro(java.lang.String hrInclusaoRegistro)
    {
        this._hrInclusaoRegistro = hrInclusaoRegistro;
    } //-- void setHrInclusaoRegistro(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ExcluirOrgaoPagadorRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.excluirorgaopagador.request.ExcluirOrgaoPagadorRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.excluirorgaopagador.request.ExcluirOrgaoPagadorRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.excluirorgaopagador.request.ExcluirOrgaoPagadorRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.excluirorgaopagador.request.ExcluirOrgaoPagadorRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
