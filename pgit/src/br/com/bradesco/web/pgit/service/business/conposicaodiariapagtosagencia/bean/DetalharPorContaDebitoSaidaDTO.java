/*
 * Nome: br.com.bradesco.web.pgit.service.business.conposicaodiariapagtosagencia.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.conposicaodiariapagtosagencia.bean;

import java.math.BigDecimal;

/**
 * Nome: DetalharPorContaDebitoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class DetalharPorContaDebitoSaidaDTO{
    
    /** Atributo codMensagem. */
    private String codMensagem;
    
    /** Atributo mensagem. */
    private String mensagem;
    
    /** Atributo nrConsultas. */
    private Integer nrConsultas;
    
    /** Atributo cdBanco. */
    private Integer cdBanco;
    
    /** Atributo cdAgencia. */
    private Integer cdAgencia;
    
    /** Atributo cdDigitoAgencia. */
    private Integer cdDigitoAgencia;
    
    /** Atributo cdContaBancaria. */
    private Long cdContaBancaria;
    
    /** Atributo cdDigitoContaBancaria. */
    private String cdDigitoContaBancaria;
    
    /** Atributo vlrPagamentoPendente. */
    private BigDecimal vlrPagamentoPendente;
    
    /** Atributo vlrPagamentoSemSaldo. */
    private BigDecimal vlrPagamentoSemSaldo;
    
    /** Atributo vlrPagamentoAgendado. */
    private BigDecimal vlrPagamentoAgendado;
    
    /** Atributo vlrPagamentoEfetivado. */
    private BigDecimal vlrPagamentoEfetivado;
    
    /** Atributo vlTotalPagamentos. */
    private BigDecimal vlTotalPagamentos;
    
    /** Atributo bancoAgenciaConta. */
    private String bancoAgenciaConta;
    
    /** Atributo cdTipoConta. */
    private Integer cdTipoConta;
    
    /** Atributo cdPessoaContratoDebito. */
    private Long cdPessoaContratoDebito;
    
    /** Atributo cdTipoContratoDebito. */
    private Integer cdTipoContratoDebito;
    
    /** Atributo nrSequenciaContratoDebito. */
    private Long nrSequenciaContratoDebito;    
    
	/**
	 * Get: cdPessoaContratoDebito.
	 *
	 * @return cdPessoaContratoDebito
	 */
	public Long getCdPessoaContratoDebito() {
		return cdPessoaContratoDebito;
	}
	
	/**
	 * Set: cdPessoaContratoDebito.
	 *
	 * @param cdPessoaContratoDebito the cd pessoa contrato debito
	 */
	public void setCdPessoaContratoDebito(Long cdPessoaContratoDebito) {
		this.cdPessoaContratoDebito = cdPessoaContratoDebito;
	}
	
	/**
	 * Get: cdTipoContratoDebito.
	 *
	 * @return cdTipoContratoDebito
	 */
	public Integer getCdTipoContratoDebito() {
		return cdTipoContratoDebito;
	}
	
	/**
	 * Set: cdTipoContratoDebito.
	 *
	 * @param cdTipoContratoDebito the cd tipo contrato debito
	 */
	public void setCdTipoContratoDebito(Integer cdTipoContratoDebito) {
		this.cdTipoContratoDebito = cdTipoContratoDebito;
	}
	
	/**
	 * Get: nrSequenciaContratoDebito.
	 *
	 * @return nrSequenciaContratoDebito
	 */
	public Long getNrSequenciaContratoDebito() {
		return nrSequenciaContratoDebito;
	}
	
	/**
	 * Set: nrSequenciaContratoDebito.
	 *
	 * @param nrSequenciaContratoDebito the nr sequencia contrato debito
	 */
	public void setNrSequenciaContratoDebito(Long nrSequenciaContratoDebito) {
		this.nrSequenciaContratoDebito = nrSequenciaContratoDebito;
	}
	
	/**
	 * Get: cdAgencia.
	 *
	 * @return cdAgencia
	 */
	public Integer getCdAgencia() {
		return cdAgencia;
	}
	
	/**
	 * Set: cdAgencia.
	 *
	 * @param cdAgencia the cd agencia
	 */
	public void setCdAgencia(Integer cdAgencia) {
		this.cdAgencia = cdAgencia;
	}
	
	/**
	 * Get: cdBanco.
	 *
	 * @return cdBanco
	 */
	public Integer getCdBanco() {
		return cdBanco;
	}
	
	/**
	 * Set: cdBanco.
	 *
	 * @param cdBanco the cd banco
	 */
	public void setCdBanco(Integer cdBanco) {
		this.cdBanco = cdBanco;
	}
	
	/**
	 * Get: cdContaBancaria.
	 *
	 * @return cdContaBancaria
	 */
	public Long getCdContaBancaria() {
		return cdContaBancaria;
	}
	
	/**
	 * Set: cdContaBancaria.
	 *
	 * @param cdContaBancaria the cd conta bancaria
	 */
	public void setCdContaBancaria(Long cdContaBancaria) {
		this.cdContaBancaria = cdContaBancaria;
	}
	
	/**
	 * Get: cdDigitoAgencia.
	 *
	 * @return cdDigitoAgencia
	 */
	public Integer getCdDigitoAgencia() {
		return cdDigitoAgencia;
	}
	
	/**
	 * Set: cdDigitoAgencia.
	 *
	 * @param cdDigitoAgencia the cd digito agencia
	 */
	public void setCdDigitoAgencia(Integer cdDigitoAgencia) {
		this.cdDigitoAgencia = cdDigitoAgencia;
	}
	
	/**
	 * Get: cdDigitoContaBancaria.
	 *
	 * @return cdDigitoContaBancaria
	 */
	public String getCdDigitoContaBancaria() {
		return cdDigitoContaBancaria;
	}
	
	/**
	 * Set: cdDigitoContaBancaria.
	 *
	 * @param cdDigitoContaBancaria the cd digito conta bancaria
	 */
	public void setCdDigitoContaBancaria(String cdDigitoContaBancaria) {
		this.cdDigitoContaBancaria = cdDigitoContaBancaria;
	}
	
	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}
	
	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	/**
	 * Get: nrConsultas.
	 *
	 * @return nrConsultas
	 */
	public Integer getNrConsultas() {
		return nrConsultas;
	}
	
	/**
	 * Set: nrConsultas.
	 *
	 * @param nrConsultas the nr consultas
	 */
	public void setNrConsultas(Integer nrConsultas) {
		this.nrConsultas = nrConsultas;
	}
	
	/**
	 * Get: vlrPagamentoAgendado.
	 *
	 * @return vlrPagamentoAgendado
	 */
	public BigDecimal getVlrPagamentoAgendado() {
		return vlrPagamentoAgendado;
	}
	
	/**
	 * Set: vlrPagamentoAgendado.
	 *
	 * @param vlrPagamentoAgendado the vlr pagamento agendado
	 */
	public void setVlrPagamentoAgendado(BigDecimal vlrPagamentoAgendado) {
		this.vlrPagamentoAgendado = vlrPagamentoAgendado;
	}
	
	/**
	 * Get: vlrPagamentoEfetivado.
	 *
	 * @return vlrPagamentoEfetivado
	 */
	public BigDecimal getVlrPagamentoEfetivado() {
		return vlrPagamentoEfetivado;
	}
	
	/**
	 * Set: vlrPagamentoEfetivado.
	 *
	 * @param vlrPagamentoEfetivado the vlr pagamento efetivado
	 */
	public void setVlrPagamentoEfetivado(BigDecimal vlrPagamentoEfetivado) {
		this.vlrPagamentoEfetivado = vlrPagamentoEfetivado;
	}
	
	/**
	 * Get: vlrPagamentoPendente.
	 *
	 * @return vlrPagamentoPendente
	 */
	public BigDecimal getVlrPagamentoPendente() {
		return vlrPagamentoPendente;
	}
	
	/**
	 * Set: vlrPagamentoPendente.
	 *
	 * @param vlrPagamentoPendente the vlr pagamento pendente
	 */
	public void setVlrPagamentoPendente(BigDecimal vlrPagamentoPendente) {
		this.vlrPagamentoPendente = vlrPagamentoPendente;
	}
	
	/**
	 * Get: vlrPagamentoSemSaldo.
	 *
	 * @return vlrPagamentoSemSaldo
	 */
	public BigDecimal getVlrPagamentoSemSaldo() {
		return vlrPagamentoSemSaldo;
	}
	
	/**
	 * Set: vlrPagamentoSemSaldo.
	 *
	 * @param vlrPagamentoSemSaldo the vlr pagamento sem saldo
	 */
	public void setVlrPagamentoSemSaldo(BigDecimal vlrPagamentoSemSaldo) {
		this.vlrPagamentoSemSaldo = vlrPagamentoSemSaldo;
	}
	
	/**
	 * Get: vlTotalPagamentos.
	 *
	 * @return vlTotalPagamentos
	 */
	public BigDecimal getVlTotalPagamentos() {
		return vlTotalPagamentos;
	}
	
	/**
	 * Set: vlTotalPagamentos.
	 *
	 * @param vlTotalPagamentos the vl total pagamentos
	 */
	public void setVlTotalPagamentos(BigDecimal vlTotalPagamentos) {
		this.vlTotalPagamentos = vlTotalPagamentos;
	}
	
	/**
	 * Get: bancoAgenciaConta.
	 *
	 * @return bancoAgenciaConta
	 */
	public String getBancoAgenciaConta() {
		return bancoAgenciaConta;
	}
	
	/**
	 * Set: bancoAgenciaConta.
	 *
	 * @param bancoAgenciaConta the banco agencia conta
	 */
	public void setBancoAgenciaConta(String bancoAgenciaConta) {
		this.bancoAgenciaConta = bancoAgenciaConta;
	}
	
	/**
	 * Get: cdTipoConta.
	 *
	 * @return cdTipoConta
	 */
	public Integer getCdTipoConta() {
		return cdTipoConta;
	}
	
	/**
	 * Set: cdTipoConta.
	 *
	 * @param cdTipoConta the cd tipo conta
	 */
	public void setCdTipoConta(Integer cdTipoConta) {
		this.cdTipoConta = cdTipoConta;
	}
    
    
}
