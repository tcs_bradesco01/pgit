/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/implementation.ftl,v $
 * $Id: implementation.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: implementation.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */
 
package br.com.bradesco.web.pgit.service.business.filtroagendamentoefetivacaoestorno.impl;

import java.util.ArrayList;
import java.util.List;

import br.com.bradesco.web.pgit.service.business.filtroagendamentoefetivacaoestorno.IFiltroAgendamentoEfetivacaoEstornoService;
import br.com.bradesco.web.pgit.service.business.filtroagendamentoefetivacaoestorno.IFiltroAgendamentoEfetivacaoEstornoServiceConstants;
import br.com.bradesco.web.pgit.service.business.filtroagendamentoefetivacaoestorno.bean.ConsultarContratoClienteFiltroEntradaDTO;
import br.com.bradesco.web.pgit.service.business.filtroagendamentoefetivacaoestorno.bean.ConsultarContratoClienteFiltroSaidaDTO;
import br.com.bradesco.web.pgit.service.business.filtroagendamentoefetivacaoestorno.bean.ListarClientesMarqEntradaDTO;
import br.com.bradesco.web.pgit.service.business.filtroagendamentoefetivacaoestorno.bean.ListarClientesMarqSaidaDTO;
import br.com.bradesco.web.pgit.service.business.filtroagendamentoefetivacaoestorno.bean.ListarContratoMarqEntradaDTO;
import br.com.bradesco.web.pgit.service.business.filtroagendamentoefetivacaoestorno.bean.ListarContratoMarqSaidaDTO;
import br.com.bradesco.web.pgit.service.business.filtroagendamentoefetivacaoestorno.bean.ListarContratosClienteMarqEntradaDTO;
import br.com.bradesco.web.pgit.service.business.filtroagendamentoefetivacaoestorno.bean.ListarContratosClienteMarqSaidaDTO;
import br.com.bradesco.web.pgit.service.business.filtroagendamentoefetivacaoestorno.bean.ListarPerfilTrocaArquivoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.filtroagendamentoefetivacaoestorno.bean.ListarPerfilTrocaArquivoSaidaDTO;
import br.com.bradesco.web.pgit.service.data.pdc.FactoryAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarcontratoclientefiltro.request.ConsultarContratoClienteFiltroRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarcontratoclientefiltro.response.ConsultarContratoClienteFiltroResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarclientesmarq.request.ListarClientesRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarclientesmarq.response.ListarClientesResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarcontratomarq.request.ListarContratoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarcontratomarq.response.ListarContratoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarcontratosclientemarq.request.ListarContratosClienteRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarcontratosclientemarq.response.ListarContratosClienteResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarperfiltrocaarquivo.request.ListarPerfilTrocaArquivoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarperfiltrocaarquivo.response.ListarPerfilTrocaArquivoResponse;
import br.com.bradesco.web.pgit.utils.CpfCnpjUtils;
import br.com.bradesco.web.pgit.utils.DateUtils;
import br.com.bradesco.web.pgit.utils.PgitUtil;


/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Implementa��o do adaptador: FiltroAgendamentoEfetivacaoEstorno
 * </p>
 * 
 * @comment C�DIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public class FiltroAgendamentoEfetivacaoEstornoServiceImpl implements IFiltroAgendamentoEfetivacaoEstornoService {
	
	/** Atributo factoryAdapter. */
	private FactoryAdapter factoryAdapter;
	

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.filtroagendamentoefetivacaoestorno.IFiltroAgendamentoEfetivacaoEstornoService#listarClientesMarq(br.com.bradesco.web.pgit.service.business.filtroagendamentoefetivacaoestorno.bean.ListarClientesMarqEntradaDTO)
	 */
	public List<ListarClientesMarqSaidaDTO> listarClientesMarq(ListarClientesMarqEntradaDTO entrada){
		ListarClientesRequest request = new ListarClientesRequest();
		
		request.setCdAgenciaBancaria(PgitUtil.verificaIntegerNulo(entrada.getCdAgenciaBancaria()));
		request.setCdBanco(PgitUtil.verificaIntegerNulo(entrada.getCdBanco()));
		request.setCdContaBancaria(PgitUtil.verificaLongNulo(entrada.getCdContaBancaria()));
		request.setCdControleCnpj(PgitUtil.verificaIntegerNulo(entrada.getCdControleCnpj()));
		request.setCdCpfCnpj(PgitUtil.verificaLongNulo(entrada.getCdCpfCnpj()));
		request.setCdFilialCnpj(PgitUtil.verificaIntegerNulo(entrada.getCdFilialCnpj()));
		request.setCdTipoConta(PgitUtil.verificaIntegerNulo(entrada.getCdTipoConta()));
		request.setDsNomeRazao(PgitUtil.verificaStringNula(entrada.getDsNomeRazao()));
		request.setDtNascimentoFund(PgitUtil.verificaStringNula(entrada.getDtNascimentoFund()));
		request.setNrOcorrencias(IFiltroAgendamentoEfetivacaoEstornoServiceConstants.OCORRENCIA_LISTAR_CLIENTES_MARQ);
		
		List<ListarClientesMarqSaidaDTO> listaSaida = new ArrayList<ListarClientesMarqSaidaDTO>();
		ListarClientesResponse response = getFactoryAdapter().getListarClientesMarqPDCAdapter().invokeProcess(request);
		
		ListarClientesMarqSaidaDTO registro; 
		for(int i=0; i<response.getOcorrenciasCount(); i++){
			registro = new ListarClientesMarqSaidaDTO();
			
			registro.setCdAtividadeEconomica(response.getOcorrencias(i).getCdAtividadeEconomica());
			registro.setCdClub(response.getOcorrencias(i).getCdClub());
			registro.setCdControleCnpj(response.getOcorrencias(i).getCdControleCnpj());
			registro.setCdCpfCnpj(response.getOcorrencias(i).getCdCpfCnpj());
			registro.setCdFilialCnpj(response.getOcorrencias(i).getCdFilialCnpj());
			registro.setCdGrupoEconomico(response.getOcorrencias(i).getCdGrupoEconomico());
			registro.setCdSegmentoCliente(response.getOcorrencias(i).getCdSegmentoCliente());
			registro.setCdSubSegmentoCliente(response.getOcorrencias(i).getCdSubSegmentoCliente());
			registro.setCodMensagem(response.getCodMensagem());
			registro.setDsAtividadeEconomica(response.getOcorrencias(i).getDsAtividadeEconomica());
			registro.setDsGrupoEconomico(response.getOcorrencias(i).getDsGrupoEconomico());
			registro.setDsNomeRazao(response.getOcorrencias(i).getDsNomeRazao());
			registro.setDsSegmentoCliente(response.getOcorrencias(i).getDsSegmentoCliente());
			registro.setDsSubSegmentoCliente(response.getOcorrencias(i).getDsSubSegmentoCliente());
			registro.setDtNascimentoFund(response.getOcorrencias(i).getDtNascimentoFund());
			registro.setMensagem(response.getMensagem());
			
			registro.setCnpjOuCpfFormatado(CpfCnpjUtils.formatCpfCnpjCompleto(registro.getCdCpfCnpj(), registro.getCdFilialCnpj(),
					registro.getCdControleCnpj()));

			registro.setDtNascimentoFundacaoFormatado(DateUtils.formartarDataPDCparaPadraPtBr(registro.getDtNascimentoFund(), "dd.MM.yyyy", "dd/MM/yyyy"));
			
			registro.setNumeroLinhas(response.getNumeroLinhas());
			
			listaSaida.add(registro);
		}
		
		return listaSaida;
	}
	
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.filtroagendamentoefetivacaoestorno.IFiltroAgendamentoEfetivacaoEstornoService#listarContratosClienteMarq(br.com.bradesco.web.pgit.service.business.filtroagendamentoefetivacaoestorno.bean.ListarContratosClienteMarqEntradaDTO)
	 */
	public List<ListarContratosClienteMarqSaidaDTO> listarContratosClienteMarq(ListarContratosClienteMarqEntradaDTO entrada){
		ListarContratosClienteRequest request = new ListarContratosClienteRequest();
		
		request.setCdClub(PgitUtil.verificaLongNulo(entrada.getCdClub()));
		request.setNrOcorrencias(IFiltroAgendamentoEfetivacaoEstornoServiceConstants.OCORRENCIA_LISTAR_CONTRATO_CLIENTES_MARQ);

		List<ListarContratosClienteMarqSaidaDTO> listaSaida = new ArrayList<ListarContratosClienteMarqSaidaDTO>();
		ListarContratosClienteResponse response = getFactoryAdapter().getListarContratosClienteMarqPDCAdapter().invokeProcess(request);

		ListarContratosClienteMarqSaidaDTO registro;
		for(int i=0; i<response.getOcorrenciasCount(); i++){
			registro = new ListarContratosClienteMarqSaidaDTO();
			
			registro.setCdPessoaJuridicaContrato(response.getOcorrencias(i).getCdPessoaJuridicaContrato());
			registro.setCdSituacaoContratoNegocio(response.getOcorrencias(i).getCdSituacaoContratoNegocio());
			registro.setCdSituacaoParticipanteContrato(response.getOcorrencias(i).getCdSituacaoParticipanteContrato());
			registro.setCdTipoContratoNegocio(response.getOcorrencias(i).getCdTipoContratoNegocio());
			registro.setCdTipoParticipacaoPessoa(response.getOcorrencias(i).getCdTipoParticipacaoPessoa());
			registro.setCodMensagem(response.getCodMensagem());
			registro.setDsContrato(response.getOcorrencias(i).getDsContrato());
			registro.setDsPessoaJuridicaContrato(response.getOcorrencias(i).getDsPessoaJuridicaContrato());
			registro.setDsSituacaoContratoNegocio(response.getOcorrencias(i).getDsSituacaoContratoNegocio());
			registro.setDsSituacaoParticipanteContrato(response.getOcorrencias(i).getDsSituacaoParticipanteContrato());
			registro.setDsTipoContratoNegocio(response.getOcorrencias(i).getDsTipoContratoNegocio());
			registro.setDsTipoParticipacaoPessoa(response.getOcorrencias(i).getDsTipoParticipacaoPessoa());
			registro.setDtFimParticipacaoContrato(response.getOcorrencias(i).getDtFimParticipacaoContrato());
			registro.setDtInicioParticipacaoContrato(response.getOcorrencias(i).getDtInicioParticipacaoContrato());
			registro.setMensagem(response.getMensagem());
			registro.setNrSequenciaContratoNegocio(response.getOcorrencias(i).getNrSequenciaContratoNegocio());
			registro.setNumeroLinhas(response.getNumeroLinhas());
			
			listaSaida.add(registro);
			
		}
		
		return listaSaida;
	}
	
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.filtroagendamentoefetivacaoestorno.IFiltroAgendamentoEfetivacaoEstornoService#listarContratoMarq(br.com.bradesco.web.pgit.service.business.filtroagendamentoefetivacaoestorno.bean.ListarContratoMarqEntradaDTO)
	 */
	public List<ListarContratoMarqSaidaDTO> listarContratoMarq(ListarContratoMarqEntradaDTO entrada){
		ListarContratoRequest request = new ListarContratoRequest();
		
		request.setCdPessoaJuridicaContrato(PgitUtil.verificaLongNulo(entrada.getCdPessoaJuridicaContrato()));
		request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(entrada.getCdTipoContratoNegocio()));
		request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(entrada.getNrSequenciaContratoNegocio()));
		
		request.setNrOcorrencias(IFiltroAgendamentoEfetivacaoEstornoServiceConstants.OCORRENCIA_LISTAR_CONTRATO_MARQ); 
		
		List<ListarContratoMarqSaidaDTO> listaSaida = new ArrayList<ListarContratoMarqSaidaDTO>();
		ListarContratoResponse response = getFactoryAdapter().getListarContratoMarqPDCAdapter().invokeProcess(request);
		
		ListarContratoMarqSaidaDTO registro;
		for(int i=0; i<response.getOcorrenciasCount(); i++){
			registro = new ListarContratoMarqSaidaDTO();
			
			registro.setCdPessoJuridicaContrato(response.getOcorrencias(i).getCdPessoJuridicaContrato());
			registro.setCdSituacaoContratoNegocio(response.getOcorrencias(i).getCdSituacaoContratoNegocio());
			registro.setCdTipoContratoNegocio(response.getOcorrencias(i).getCdTipoContratoNegocio());
			registro.setCodMensagem(response.getCodMensagem());
			registro.setDsContrato(response.getOcorrencias(i).getDsContrato());
			registro.setDsPessoaJuridicaContrato(response.getOcorrencias(i).getDsPessoaJuridicaContrato());
			registro.setDsSituacaoContratoNegocio(response.getOcorrencias(i).getDsSituacaoContratoNegocio());
			registro.setDsTipoContratoNegocio(response.getOcorrencias(i).getDsTipoContratoNegocio());
			registro.setMensagem(response.getMensagem());
			registro.setNrSequenciaContratoNegocio(response.getOcorrencias(i).getNrSequenciaContratoNegocio());
			registro.setNumeroLinhas(response.getNumeroLinhas());
			
			listaSaida.add(registro);
		}
		
		return listaSaida;
	}
	
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.filtroagendamentoefetivacaoestorno.IFiltroAgendamentoEfetivacaoEstornoService#listarPerfilTrocaArquivo(br.com.bradesco.web.pgit.service.business.filtroagendamentoefetivacaoestorno.bean.ListarPerfilTrocaArquivoEntradaDTO)
	 */
	public List<ListarPerfilTrocaArquivoSaidaDTO> listarPerfilTrocaArquivo(ListarPerfilTrocaArquivoEntradaDTO entrada){
		ListarPerfilTrocaArquivoRequest request = new ListarPerfilTrocaArquivoRequest();
		
		request.setCdPessoa(PgitUtil.verificaLongNulo(entrada.getCdPessoa()));
		request.setCdPessoaJuridicaContrato(PgitUtil.verificaLongNulo(entrada.getCdPessoaJuridicaContrato()));
		request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(entrada.getCdTipoContratoNegocio()));
		request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(entrada.getNrSequenciaContratoNegocio()));
		request.setNumeroOcorrencias(IFiltroAgendamentoEfetivacaoEstornoServiceConstants.OCORRENCIA_LISTAR_PERFIL_TROCA_ARQUIVO);
		
		List<ListarPerfilTrocaArquivoSaidaDTO> listaSaida = new ArrayList<ListarPerfilTrocaArquivoSaidaDTO>();
		ListarPerfilTrocaArquivoResponse response = getFactoryAdapter().getListarPerfilTrocaArquivoPDCAdapter().invokeProcess(request);
		
		ListarPerfilTrocaArquivoSaidaDTO registro;
		for(int i=0; i<response.getOcorrenciasCount(); i++){
			registro = new ListarPerfilTrocaArquivoSaidaDTO();
			registro.setCdCpfCnpjPessoa(response.getOcorrencias(i).getCdCpfCnpjPessoa());
			registro.setCdPerfilTrocaArquivo(response.getOcorrencias(i).getCdPerfilTrocaArquivo());
			registro.setCdPessoa(response.getOcorrencias(i).getCdPessoa());
			registro.setCodMensagem(response.getCodMensagem());
			registro.setDsPessoa(response.getOcorrencias(i).getDsPessoa());
			registro.setDsSituacaoPerfil(response.getOcorrencias(i).getDsSituacaoPerfil());
			registro.setMensagem(response.getMensagem());
			registro.setNumeroLinhas(response.getNumeroLinhas());
			
			listaSaida.add(registro);
		}	
		
		return listaSaida;
	}
	
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.filtroagendamentoefetivacaoestorno.IFiltroAgendamentoEfetivacaoEstornoService#consultarContratoClienteFiltro(br.com.bradesco.web.pgit.service.business.filtroagendamentoefetivacaoestorno.bean.ConsultarContratoClienteFiltroEntradaDTO)
	 */
	public List<ConsultarContratoClienteFiltroSaidaDTO> consultarContratoClienteFiltro(ConsultarContratoClienteFiltroEntradaDTO entrada){
		ConsultarContratoClienteFiltroRequest request = new ConsultarContratoClienteFiltroRequest();
		request.setCdClubPessoa(PgitUtil.verificaLongNulo(entrada.getCdClubPessoa()));
		request.setCdControleCpfPssoa(PgitUtil.verificaIntegerNulo(entrada.getCdControleCpfPssoa()));
		request.setCdCpfCnpjPssoa(PgitUtil.verificaLongNulo(entrada.getCdCpfCnpjPssoa()));
		request.setCdFilialCnpjPssoa(PgitUtil.verificaIntegerNulo(entrada.getCdFilialCnpjPssoa()));
		request.setNumeroOcorrencias(IFiltroAgendamentoEfetivacaoEstornoServiceConstants.OCORRENCIA_CONTRATO_CLIENTE_FILTRO);
		
		List<ConsultarContratoClienteFiltroSaidaDTO> listaSaida = new ArrayList<ConsultarContratoClienteFiltroSaidaDTO>();
		ConsultarContratoClienteFiltroResponse response = getFactoryAdapter().getConsultarContratoClienteFiltroPDCAdapter().invokeProcess(request);
		
		ConsultarContratoClienteFiltroSaidaDTO registro;
		for(int i=0; i<response.getOcorrenciasCount(); i++){
			registro = new ConsultarContratoClienteFiltroSaidaDTO();
			registro.setCodMensagem(response.getCodMensagem());
			registro.setMensagem(response.getMensagem());
			registro.setCdCorpoCnpjRepresentante(response.getOcorrencias(i).getCdCorpoCnpjRepresentante());
			registro.setCdFilialCnpjRepresentante(response.getOcorrencias(i).getCdFilialCnpjRepresentante());
			registro.setCdControleCnpjRepresentante(response.getOcorrencias(i).getCdControleCnpjRepresentante());
			registro.setCdPessoaJuridica(response.getOcorrencias(i).getCdPessoaJuridica());
			registro.setDsPessoaJuridica(response.getOcorrencias(i).getDsPessoaJuridica());
			registro.setCdTipoContrato(response.getOcorrencias(i).getCdTipoContrato());
			registro.setDsTipoContrato(response.getOcorrencias(i).getDsTipoContrato());
			registro.setNrSequenciaContrato(response.getOcorrencias(i).getNrSequenciaContrato());
			registro.setDsContrato(response.getOcorrencias(i).getDsContrato());
			registro.setCdSituacaoContrato(response.getOcorrencias(i).getCdSituacaoContrato());
			registro.setDsSituacaoContrato(response.getOcorrencias(i).getDsSituacaoContrato());
			registro.setCdTipoParticipacaoPessoa(response.getOcorrencias(i).getCdTipoParticipacaoPessoa());
			registro.setDsTipoParticipacaoPessoa(response.getOcorrencias(i).getDsTipoParticipacaoPessoa());
			registro.setCdPessoaParticipante(response.getOcorrencias(i).getCdPessoaParticipante());
			registro.setDsPessoaParticipante(response.getOcorrencias(i).getDsPessoaParticipante());
			
			listaSaida.add(registro);
		}
		
		return listaSaida;
		
	}

	
	/**
     * Construtor.
     */
    public FiltroAgendamentoEfetivacaoEstornoServiceImpl() {
        // TODO: Implementa��o
    }
	
    /**
     * Get: factoryAdapter.
     *
     * @return factoryAdapter
     */
    public FactoryAdapter getFactoryAdapter() {
		return factoryAdapter;
	}

	/**
	 * Set: factoryAdapter.
	 *
	 * @param factoryAdapter the factory adapter
	 */
	public void setFactoryAdapter(FactoryAdapter factoryAdapter) {
		this.factoryAdapter = factoryAdapter;
	}
    
    
}

