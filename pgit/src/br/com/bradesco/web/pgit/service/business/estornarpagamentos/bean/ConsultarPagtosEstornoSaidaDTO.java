/*
 * Nome: br.com.bradesco.web.pgit.service.business.estornarpagamentos.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.estornarpagamentos.bean;

import java.math.BigDecimal;

/**
 * Nome: ConsultarPagtosEstornoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarPagtosEstornoSaidaDTO {

    /** Atributo codMensagem. */
    private String codMensagem;

    /** Atributo mensagem. */
    private String mensagem;

    /** Atributo corpoCfpCnpj. */
    private Long corpoCfpCnpj;

    /** Atributo filialCfpCnpj. */
    private int filialCfpCnpj;

    /** Atributo controleCfpCnpj. */
    private int controleCfpCnpj;

    /** Atributo cdPessoaJuridicaContrato. */
    private Long cdPessoaJuridicaContrato;

    /** Atributo nmRazaoSocial. */
    private String nmRazaoSocial;

    /** Atributo cdEmpresa. */
    private Long cdEmpresa;

    /** Atributo dsEmpresa. */
    private String dsEmpresa;

    /** Atributo cdTipoContratoNegocio. */
    private int cdTipoContratoNegocio;

    /** Atributo nrContrato. */
    private Long nrContrato;

    /** Atributo cdProdutoServicoOperacao. */
    private int cdProdutoServicoOperacao;

    /** Atributo cdResumoProdutoServico. */
    private String cdResumoProdutoServico;

    /** Atributo cdProdutoServicoRelacionado. */
    private int cdProdutoServicoRelacionado;

    /** Atributo dsOperacaoProdutoServico. */
    private String dsOperacaoProdutoServico;

    /** Atributo cdControlePagamento. */
    private String cdControlePagamento;

    /** Atributo dtCraditoPagamento. */
    private String dtCraditoPagamento;

    /** Atributo vlrEfetivacaoPagamentoCliente. */
    private BigDecimal vlrEfetivacaoPagamentoCliente;

    /** Atributo cdInscricaoFavorecido. */
    private Long cdInscricaoFavorecido;

    /** Atributo dsAbrevFavorecido. */
    private String dsAbrevFavorecido;

    /** Atributo cdBancoDebito. */
    private int cdBancoDebito;

    /** Atributo cdAgenciaDebito. */
    private int cdAgenciaDebito;

    /** Atributo cdDigitoAgenciaDebito. */
    private int cdDigitoAgenciaDebito;

    /** Atributo cdContaDebito. */
    private long cdContaDebito;

    /** Atributo cdDigitoContaDebito. */
    private String cdDigitoContaDebito;

    /** Atributo cdBancoCredito. */
    private int cdBancoCredito;

    /** Atributo cdAgenciaCredito. */
    private int cdAgenciaCredito;

    /** Atributo cdDigitoAgenciaCredito. */
    private int cdDigitoAgenciaCredito;

    /** Atributo cdContaCredito. */
    private long cdContaCredito;

    /** Atributo cdDigitoContaCredito. */
    private String cdDigitoContaCredito;

    /** Atributo cdSituacaoOperacaoPagamento. */
    private int cdSituacaoOperacaoPagamento;

    /** Atributo dsSituacaoOperacaoPagamento. */
    private String dsSituacaoOperacaoPagamento;

    /** Atributo cdMotivoSituacaoPagamento. */
    private int cdMotivoSituacaoPagamento;

    /** Atributo dsMotivoSituacaoPagamento. */
    private String dsMotivoSituacaoPagamento;

    /** Atributo cdTipoCanal. */
    private int cdTipoCanal;

    /** Atributo dsTipoTela. */
    private int dsTipoTela;

    /** Atributo dtEfetivacao. */
    private String dtEfetivacao;

    /** Atributo cdCpfCnpjRecebedor. */
    private Long cdCpfCnpjRecebedor;

    /** Atributo cdFilialRecebedor. */
    private int cdFilialRecebedor;

    /** Atributo cdDigitoCpfRecebedor. */
    private int cdDigitoCpfRecebedor;

    /** Atributo vlEfetivoPagamentoFormatado. */
    private String vlEfetivoPagamentoFormatado;

    /** Atributo favorecidoBeneficiarioFormatado. */
    private String favorecidoBeneficiarioFormatado;

    /** Atributo bancoAgenciaContaCreditoFormatado. */
    private String bancoAgenciaContaCreditoFormatado;

    /** Atributo bancoAgenciaContaDebitoFormatado. */
    private String bancoAgenciaContaDebitoFormatado;

    /** Atributo cdCpfCnpjFormatado. */
    private String cdCpfCnpjFormatado;

    /** Atributo dsRazaoSocial. */
    private String dsRazaoSocial;

    /** Atributo dsResumoProdutoServico. */
    private String dsResumoProdutoServico;

    /** Atributo dsCodigoTabelaConsulta. */
    private Integer dsCodigoTabelaConsulta;

    /** Atributo cdBancoDebitoFormatado. */
    private String cdBancoDebitoFormatado;

    /** Atributo cdAgenciaDebitoFormatado. */
    private String cdAgenciaDebitoFormatado;

    /** Atributo cdDigitoAgenciaDebitoFormatado. */
    private String cdDigitoAgenciaDebitoFormatado;

    /** Atributo cdContaDebitoFormatado. */
    private String cdContaDebitoFormatado;

    /** Atributo cdDigitoContaDebitoFormatado. */
    private String cdDigitoContaDebitoFormatado;

    /** Atributo cdBancoCreditoFormatado. */
    private String cdBancoCreditoFormatado;

    /** Atributo cdAgenciaCreditoFormatado. */
    private String cdAgenciaCreditoFormatado;

    /** Atributo cdDigitoAgenciaCreditoFormatado. */
    private String cdDigitoAgenciaCreditoFormatado;

    /** Atributo cdContaCreditoFormatado. */
    private String cdContaCreditoFormatado;

    /** Atributo cdDigitoContaCreditoFormatado. */
    private String cdDigitoContaCreditoFormatado;

    /** Atributo agenciaCreditoFormatada. */
    private String agenciaCreditoFormatada;

    /** Atributo contaDebitoFormatada. */
    private String contaDebitoFormatada;

    /** Atributo contaCreditoFormatada. */
    private String contaCreditoFormatada;

    /** Atributo dtCraditoPagamentoFormatada. */
    private String dtCraditoPagamentoFormatada;

    /** Atributo nrArquivoRemessa. */
    private Long nrArquivoRemessa;

    /** Atributo cdFormaLiquidacao. */
    private Integer cdFormaLiquidacao;

    // check da grid de consulta
    /** Atributo check. */
    private boolean check = false;

    /**
     * Get: agenciaCreditoFormatada.
     *
     * @return agenciaCreditoFormatada
     */
    public String getAgenciaCreditoFormatada() {
	return agenciaCreditoFormatada;
    }

    /**
     * Set: agenciaCreditoFormatada.
     *
     * @param agenciaCreditoFormatada the agencia credito formatada
     */
    public void setAgenciaCreditoFormatada(String agenciaCreditoFormatada) {
	this.agenciaCreditoFormatada = agenciaCreditoFormatada;
    }

    /**
     * Get: bancoAgenciaContaCreditoFormatado.
     *
     * @return bancoAgenciaContaCreditoFormatado
     */
    public String getBancoAgenciaContaCreditoFormatado() {
	return bancoAgenciaContaCreditoFormatado;
    }

    /**
     * Set: bancoAgenciaContaCreditoFormatado.
     *
     * @param bancoAgenciaContaCreditoFormatado the banco agencia conta credito formatado
     */
    public void setBancoAgenciaContaCreditoFormatado(String bancoAgenciaContaCreditoFormatado) {
	this.bancoAgenciaContaCreditoFormatado = bancoAgenciaContaCreditoFormatado;
    }

    /**
     * Get: bancoAgenciaContaDebitoFormatado.
     *
     * @return bancoAgenciaContaDebitoFormatado
     */
    public String getBancoAgenciaContaDebitoFormatado() {
	return bancoAgenciaContaDebitoFormatado;
    }

    /**
     * Set: bancoAgenciaContaDebitoFormatado.
     *
     * @param bancoAgenciaContaDebitoFormatado the banco agencia conta debito formatado
     */
    public void setBancoAgenciaContaDebitoFormatado(String bancoAgenciaContaDebitoFormatado) {
	this.bancoAgenciaContaDebitoFormatado = bancoAgenciaContaDebitoFormatado;
    }

    /**
     * Get: cdAgenciaCredito.
     *
     * @return cdAgenciaCredito
     */
    public int getCdAgenciaCredito() {
	return cdAgenciaCredito;
    }

    /**
     * Set: cdAgenciaCredito.
     *
     * @param cdAgenciaCredito the cd agencia credito
     */
    public void setCdAgenciaCredito(int cdAgenciaCredito) {
	this.cdAgenciaCredito = cdAgenciaCredito;
    }

    /**
     * Get: cdAgenciaCreditoFormatado.
     *
     * @return cdAgenciaCreditoFormatado
     */
    public String getCdAgenciaCreditoFormatado() {
	return cdAgenciaCreditoFormatado;
    }

    /**
     * Set: cdAgenciaCreditoFormatado.
     *
     * @param cdAgenciaCreditoFormatado the cd agencia credito formatado
     */
    public void setCdAgenciaCreditoFormatado(String cdAgenciaCreditoFormatado) {
	this.cdAgenciaCreditoFormatado = cdAgenciaCreditoFormatado;
    }

    /**
     * Get: cdAgenciaDebito.
     *
     * @return cdAgenciaDebito
     */
    public int getCdAgenciaDebito() {
	return cdAgenciaDebito;
    }

    /**
     * Set: cdAgenciaDebito.
     *
     * @param cdAgenciaDebito the cd agencia debito
     */
    public void setCdAgenciaDebito(int cdAgenciaDebito) {
	this.cdAgenciaDebito = cdAgenciaDebito;
    }

    /**
     * Get: cdAgenciaDebitoFormatado.
     *
     * @return cdAgenciaDebitoFormatado
     */
    public String getCdAgenciaDebitoFormatado() {
	return cdAgenciaDebitoFormatado;
    }

    /**
     * Set: cdAgenciaDebitoFormatado.
     *
     * @param cdAgenciaDebitoFormatado the cd agencia debito formatado
     */
    public void setCdAgenciaDebitoFormatado(String cdAgenciaDebitoFormatado) {
	this.cdAgenciaDebitoFormatado = cdAgenciaDebitoFormatado;
    }

    /**
     * Get: cdBancoCredito.
     *
     * @return cdBancoCredito
     */
    public int getCdBancoCredito() {
	return cdBancoCredito;
    }

    /**
     * Set: cdBancoCredito.
     *
     * @param cdBancoCredito the cd banco credito
     */
    public void setCdBancoCredito(int cdBancoCredito) {
	this.cdBancoCredito = cdBancoCredito;
    }

    /**
     * Get: cdBancoCreditoFormatado.
     *
     * @return cdBancoCreditoFormatado
     */
    public String getCdBancoCreditoFormatado() {
	return cdBancoCreditoFormatado;
    }

    /**
     * Set: cdBancoCreditoFormatado.
     *
     * @param cdBancoCreditoFormatado the cd banco credito formatado
     */
    public void setCdBancoCreditoFormatado(String cdBancoCreditoFormatado) {
	this.cdBancoCreditoFormatado = cdBancoCreditoFormatado;
    }

    /**
     * Get: cdBancoDebito.
     *
     * @return cdBancoDebito
     */
    public int getCdBancoDebito() {
	return cdBancoDebito;
    }

    /**
     * Set: cdBancoDebito.
     *
     * @param cdBancoDebito the cd banco debito
     */
    public void setCdBancoDebito(int cdBancoDebito) {
	this.cdBancoDebito = cdBancoDebito;
    }

    /**
     * Get: cdBancoDebitoFormatado.
     *
     * @return cdBancoDebitoFormatado
     */
    public String getCdBancoDebitoFormatado() {
	return cdBancoDebitoFormatado;
    }

    /**
     * Set: cdBancoDebitoFormatado.
     *
     * @param cdBancoDebitoFormatado the cd banco debito formatado
     */
    public void setCdBancoDebitoFormatado(String cdBancoDebitoFormatado) {
	this.cdBancoDebitoFormatado = cdBancoDebitoFormatado;
    }

    /**
     * Get: cdContaCredito.
     *
     * @return cdContaCredito
     */
    public long getCdContaCredito() {
	return cdContaCredito;
    }

    /**
     * Set: cdContaCredito.
     *
     * @param cdContaCredito the cd conta credito
     */
    public void setCdContaCredito(long cdContaCredito) {
	this.cdContaCredito = cdContaCredito;
    }

    /**
     * Get: cdContaCreditoFormatado.
     *
     * @return cdContaCreditoFormatado
     */
    public String getCdContaCreditoFormatado() {
	return cdContaCreditoFormatado;
    }

    /**
     * Set: cdContaCreditoFormatado.
     *
     * @param cdContaCreditoFormatado the cd conta credito formatado
     */
    public void setCdContaCreditoFormatado(String cdContaCreditoFormatado) {
	this.cdContaCreditoFormatado = cdContaCreditoFormatado;
    }

    /**
     * Get: cdContaDebito.
     *
     * @return cdContaDebito
     */
    public long getCdContaDebito() {
	return cdContaDebito;
    }

    /**
     * Set: cdContaDebito.
     *
     * @param cdContaDebito the cd conta debito
     */
    public void setCdContaDebito(long cdContaDebito) {
	this.cdContaDebito = cdContaDebito;
    }

    /**
     * Get: cdContaDebitoFormatado.
     *
     * @return cdContaDebitoFormatado
     */
    public String getCdContaDebitoFormatado() {
	return cdContaDebitoFormatado;
    }

    /**
     * Set: cdContaDebitoFormatado.
     *
     * @param cdContaDebitoFormatado the cd conta debito formatado
     */
    public void setCdContaDebitoFormatado(String cdContaDebitoFormatado) {
	this.cdContaDebitoFormatado = cdContaDebitoFormatado;
    }

    /**
     * Get: cdControlePagamento.
     *
     * @return cdControlePagamento
     */
    public String getCdControlePagamento() {
	return cdControlePagamento;
    }

    /**
     * Set: cdControlePagamento.
     *
     * @param cdControlePagamento the cd controle pagamento
     */
    public void setCdControlePagamento(String cdControlePagamento) {
	this.cdControlePagamento = cdControlePagamento;
    }

    /**
     * Get: cdCpfCnpjFormatado.
     *
     * @return cdCpfCnpjFormatado
     */
    public String getCdCpfCnpjFormatado() {
	return cdCpfCnpjFormatado;
    }

    /**
     * Set: cdCpfCnpjFormatado.
     *
     * @param cdCpfCnpjFormatado the cd cpf cnpj formatado
     */
    public void setCdCpfCnpjFormatado(String cdCpfCnpjFormatado) {
	this.cdCpfCnpjFormatado = cdCpfCnpjFormatado;
    }

    /**
     * Get: cdCpfCnpjRecebedor.
     *
     * @return cdCpfCnpjRecebedor
     */
    public Long getCdCpfCnpjRecebedor() {
	return cdCpfCnpjRecebedor;
    }

    /**
     * Set: cdCpfCnpjRecebedor.
     *
     * @param cdCpfCnpjRecebedor the cd cpf cnpj recebedor
     */
    public void setCdCpfCnpjRecebedor(Long cdCpfCnpjRecebedor) {
	this.cdCpfCnpjRecebedor = cdCpfCnpjRecebedor;
    }

    /**
     * Get: cdDigitoAgenciaCredito.
     *
     * @return cdDigitoAgenciaCredito
     */
    public int getCdDigitoAgenciaCredito() {
	return cdDigitoAgenciaCredito;
    }

    /**
     * Set: cdDigitoAgenciaCredito.
     *
     * @param cdDigitoAgenciaCredito the cd digito agencia credito
     */
    public void setCdDigitoAgenciaCredito(int cdDigitoAgenciaCredito) {
	this.cdDigitoAgenciaCredito = cdDigitoAgenciaCredito;
    }

    /**
     * Get: cdDigitoAgenciaCreditoFormatado.
     *
     * @return cdDigitoAgenciaCreditoFormatado
     */
    public String getCdDigitoAgenciaCreditoFormatado() {
	return cdDigitoAgenciaCreditoFormatado;
    }

    /**
     * Set: cdDigitoAgenciaCreditoFormatado.
     *
     * @param cdDigitoAgenciaCreditoFormatado the cd digito agencia credito formatado
     */
    public void setCdDigitoAgenciaCreditoFormatado(String cdDigitoAgenciaCreditoFormatado) {
	this.cdDigitoAgenciaCreditoFormatado = cdDigitoAgenciaCreditoFormatado;
    }

    /**
     * Get: cdDigitoAgenciaDebito.
     *
     * @return cdDigitoAgenciaDebito
     */
    public int getCdDigitoAgenciaDebito() {
	return cdDigitoAgenciaDebito;
    }

    /**
     * Set: cdDigitoAgenciaDebito.
     *
     * @param cdDigitoAgenciaDebito the cd digito agencia debito
     */
    public void setCdDigitoAgenciaDebito(int cdDigitoAgenciaDebito) {
	this.cdDigitoAgenciaDebito = cdDigitoAgenciaDebito;
    }

    /**
     * Get: cdDigitoAgenciaDebitoFormatado.
     *
     * @return cdDigitoAgenciaDebitoFormatado
     */
    public String getCdDigitoAgenciaDebitoFormatado() {
	return cdDigitoAgenciaDebitoFormatado;
    }

    /**
     * Set: cdDigitoAgenciaDebitoFormatado.
     *
     * @param cdDigitoAgenciaDebitoFormatado the cd digito agencia debito formatado
     */
    public void setCdDigitoAgenciaDebitoFormatado(String cdDigitoAgenciaDebitoFormatado) {
	this.cdDigitoAgenciaDebitoFormatado = cdDigitoAgenciaDebitoFormatado;
    }

    /**
     * Get: cdDigitoContaCredito.
     *
     * @return cdDigitoContaCredito
     */
    public String getCdDigitoContaCredito() {
	return cdDigitoContaCredito;
    }

    /**
     * Set: cdDigitoContaCredito.
     *
     * @param cdDigitoContaCredito the cd digito conta credito
     */
    public void setCdDigitoContaCredito(String cdDigitoContaCredito) {
	this.cdDigitoContaCredito = cdDigitoContaCredito;
    }

    /**
     * Get: cdDigitoContaCreditoFormatado.
     *
     * @return cdDigitoContaCreditoFormatado
     */
    public String getCdDigitoContaCreditoFormatado() {
	return cdDigitoContaCreditoFormatado;
    }

    /**
     * Set: cdDigitoContaCreditoFormatado.
     *
     * @param cdDigitoContaCreditoFormatado the cd digito conta credito formatado
     */
    public void setCdDigitoContaCreditoFormatado(String cdDigitoContaCreditoFormatado) {
	this.cdDigitoContaCreditoFormatado = cdDigitoContaCreditoFormatado;
    }

    /**
     * Get: cdDigitoContaDebito.
     *
     * @return cdDigitoContaDebito
     */
    public String getCdDigitoContaDebito() {
	return cdDigitoContaDebito;
    }

    /**
     * Set: cdDigitoContaDebito.
     *
     * @param cdDigitoContaDebito the cd digito conta debito
     */
    public void setCdDigitoContaDebito(String cdDigitoContaDebito) {
	this.cdDigitoContaDebito = cdDigitoContaDebito;
    }

    /**
     * Get: cdDigitoContaDebitoFormatado.
     *
     * @return cdDigitoContaDebitoFormatado
     */
    public String getCdDigitoContaDebitoFormatado() {
	return cdDigitoContaDebitoFormatado;
    }

    /**
     * Set: cdDigitoContaDebitoFormatado.
     *
     * @param cdDigitoContaDebitoFormatado the cd digito conta debito formatado
     */
    public void setCdDigitoContaDebitoFormatado(String cdDigitoContaDebitoFormatado) {
	this.cdDigitoContaDebitoFormatado = cdDigitoContaDebitoFormatado;
    }

    /**
     * Get: cdDigitoCpfRecebedor.
     *
     * @return cdDigitoCpfRecebedor
     */
    public int getCdDigitoCpfRecebedor() {
	return cdDigitoCpfRecebedor;
    }

    /**
     * Set: cdDigitoCpfRecebedor.
     *
     * @param cdDigitoCpfRecebedor the cd digito cpf recebedor
     */
    public void setCdDigitoCpfRecebedor(int cdDigitoCpfRecebedor) {
	this.cdDigitoCpfRecebedor = cdDigitoCpfRecebedor;
    }

    /**
     * Get: cdEmpresa.
     *
     * @return cdEmpresa
     */
    public Long getCdEmpresa() {
	return cdEmpresa;
    }

    /**
     * Set: cdEmpresa.
     *
     * @param cdEmpresa the cd empresa
     */
    public void setCdEmpresa(Long cdEmpresa) {
	this.cdEmpresa = cdEmpresa;
    }

    /**
     * Get: cdFilialRecebedor.
     *
     * @return cdFilialRecebedor
     */
    public int getCdFilialRecebedor() {
	return cdFilialRecebedor;
    }

    /**
     * Set: cdFilialRecebedor.
     *
     * @param cdFilialRecebedor the cd filial recebedor
     */
    public void setCdFilialRecebedor(int cdFilialRecebedor) {
	this.cdFilialRecebedor = cdFilialRecebedor;
    }

    /**
     * Get: cdInscricaoFavorecido.
     *
     * @return cdInscricaoFavorecido
     */
    public Long getCdInscricaoFavorecido() {
	return cdInscricaoFavorecido;
    }

    /**
     * Set: cdInscricaoFavorecido.
     *
     * @param cdInscricaoFavorecido the cd inscricao favorecido
     */
    public void setCdInscricaoFavorecido(Long cdInscricaoFavorecido) {
	this.cdInscricaoFavorecido = cdInscricaoFavorecido;
    }

    /**
     * Get: cdMotivoSituacaoPagamento.
     *
     * @return cdMotivoSituacaoPagamento
     */
    public int getCdMotivoSituacaoPagamento() {
	return cdMotivoSituacaoPagamento;
    }

    /**
     * Set: cdMotivoSituacaoPagamento.
     *
     * @param cdMotivoSituacaoPagamento the cd motivo situacao pagamento
     */
    public void setCdMotivoSituacaoPagamento(int cdMotivoSituacaoPagamento) {
	this.cdMotivoSituacaoPagamento = cdMotivoSituacaoPagamento;
    }

    /**
     * Get: cdPessoaJuridicaContrato.
     *
     * @return cdPessoaJuridicaContrato
     */
    public Long getCdPessoaJuridicaContrato() {
	return cdPessoaJuridicaContrato;
    }

    /**
     * Set: cdPessoaJuridicaContrato.
     *
     * @param cdPessoaJuridicaContrato the cd pessoa juridica contrato
     */
    public void setCdPessoaJuridicaContrato(Long cdPessoaJuridicaContrato) {
	this.cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
    }

    /**
     * Get: cdProdutoServicoOperacao.
     *
     * @return cdProdutoServicoOperacao
     */
    public int getCdProdutoServicoOperacao() {
	return cdProdutoServicoOperacao;
    }

    /**
     * Set: cdProdutoServicoOperacao.
     *
     * @param cdProdutoServicoOperacao the cd produto servico operacao
     */
    public void setCdProdutoServicoOperacao(int cdProdutoServicoOperacao) {
	this.cdProdutoServicoOperacao = cdProdutoServicoOperacao;
    }

    /**
     * Get: cdProdutoServicoRelacionado.
     *
     * @return cdProdutoServicoRelacionado
     */
    public int getCdProdutoServicoRelacionado() {
	return cdProdutoServicoRelacionado;
    }

    /**
     * Set: cdProdutoServicoRelacionado.
     *
     * @param cdProdutoServicoRelacionado the cd produto servico relacionado
     */
    public void setCdProdutoServicoRelacionado(int cdProdutoServicoRelacionado) {
	this.cdProdutoServicoRelacionado = cdProdutoServicoRelacionado;
    }

    /**
     * Get: cdResumoProdutoServico.
     *
     * @return cdResumoProdutoServico
     */
    public String getCdResumoProdutoServico() {
	return cdResumoProdutoServico;
    }

    /**
     * Set: cdResumoProdutoServico.
     *
     * @param cdResumoProdutoServico the cd resumo produto servico
     */
    public void setCdResumoProdutoServico(String cdResumoProdutoServico) {
	this.cdResumoProdutoServico = cdResumoProdutoServico;
    }

    /**
     * Get: cdSituacaoOperacaoPagamento.
     *
     * @return cdSituacaoOperacaoPagamento
     */
    public int getCdSituacaoOperacaoPagamento() {
	return cdSituacaoOperacaoPagamento;
    }

    /**
     * Set: cdSituacaoOperacaoPagamento.
     *
     * @param cdSituacaoOperacaoPagamento the cd situacao operacao pagamento
     */
    public void setCdSituacaoOperacaoPagamento(int cdSituacaoOperacaoPagamento) {
	this.cdSituacaoOperacaoPagamento = cdSituacaoOperacaoPagamento;
    }

    /**
     * Get: cdTipoCanal.
     *
     * @return cdTipoCanal
     */
    public int getCdTipoCanal() {
	return cdTipoCanal;
    }

    /**
     * Set: cdTipoCanal.
     *
     * @param cdTipoCanal the cd tipo canal
     */
    public void setCdTipoCanal(int cdTipoCanal) {
	this.cdTipoCanal = cdTipoCanal;
    }

    /**
     * Get: cdTipoContratoNegocio.
     *
     * @return cdTipoContratoNegocio
     */
    public int getCdTipoContratoNegocio() {
	return cdTipoContratoNegocio;
    }

    /**
     * Set: cdTipoContratoNegocio.
     *
     * @param cdTipoContratoNegocio the cd tipo contrato negocio
     */
    public void setCdTipoContratoNegocio(int cdTipoContratoNegocio) {
	this.cdTipoContratoNegocio = cdTipoContratoNegocio;
    }

    /**
     * Is check.
     *
     * @return true, if is check
     */
    public boolean isCheck() {
	return check;
    }

    /**
     * Set: check.
     *
     * @param check the check
     */
    public void setCheck(boolean check) {
	this.check = check;
    }

    /**
     * Get: codMensagem.
     *
     * @return codMensagem
     */
    public String getCodMensagem() {
	return codMensagem;
    }

    /**
     * Set: codMensagem.
     *
     * @param codMensagem the cod mensagem
     */
    public void setCodMensagem(String codMensagem) {
	this.codMensagem = codMensagem;
    }

    /**
     * Get: contaCreditoFormatada.
     *
     * @return contaCreditoFormatada
     */
    public String getContaCreditoFormatada() {
	return contaCreditoFormatada;
    }

    /**
     * Set: contaCreditoFormatada.
     *
     * @param contaCreditoFormatada the conta credito formatada
     */
    public void setContaCreditoFormatada(String contaCreditoFormatada) {
	this.contaCreditoFormatada = contaCreditoFormatada;
    }

    /**
     * Get: contaDebitoFormatada.
     *
     * @return contaDebitoFormatada
     */
    public String getContaDebitoFormatada() {
	return contaDebitoFormatada;
    }

    /**
     * Set: contaDebitoFormatada.
     *
     * @param contaDebitoFormatada the conta debito formatada
     */
    public void setContaDebitoFormatada(String contaDebitoFormatada) {
	this.contaDebitoFormatada = contaDebitoFormatada;
    }

    /**
     * Get: controleCfpCnpj.
     *
     * @return controleCfpCnpj
     */
    public int getControleCfpCnpj() {
	return controleCfpCnpj;
    }

    /**
     * Set: controleCfpCnpj.
     *
     * @param controleCfpCnpj the controle cfp cnpj
     */
    public void setControleCfpCnpj(int controleCfpCnpj) {
	this.controleCfpCnpj = controleCfpCnpj;
    }

    /**
     * Get: corpoCfpCnpj.
     *
     * @return corpoCfpCnpj
     */
    public Long getCorpoCfpCnpj() {
	return corpoCfpCnpj;
    }

    /**
     * Set: corpoCfpCnpj.
     *
     * @param corpoCfpCnpj the corpo cfp cnpj
     */
    public void setCorpoCfpCnpj(Long corpoCfpCnpj) {
	this.corpoCfpCnpj = corpoCfpCnpj;
    }

    /**
     * Get: dsAbrevFavorecido.
     *
     * @return dsAbrevFavorecido
     */
    public String getDsAbrevFavorecido() {
	return dsAbrevFavorecido;
    }

    /**
     * Set: dsAbrevFavorecido.
     *
     * @param dsAbrevFavorecido the ds abrev favorecido
     */
    public void setDsAbrevFavorecido(String dsAbrevFavorecido) {
	this.dsAbrevFavorecido = dsAbrevFavorecido;
    }

    /**
     * Get: dsEmpresa.
     *
     * @return dsEmpresa
     */
    public String getDsEmpresa() {
	return dsEmpresa;
    }

    /**
     * Set: dsEmpresa.
     *
     * @param dsEmpresa the ds empresa
     */
    public void setDsEmpresa(String dsEmpresa) {
	this.dsEmpresa = dsEmpresa;
    }

    /**
     * Get: dsMotivoSituacaoPagamento.
     *
     * @return dsMotivoSituacaoPagamento
     */
    public String getDsMotivoSituacaoPagamento() {
	return dsMotivoSituacaoPagamento;
    }

    /**
     * Set: dsMotivoSituacaoPagamento.
     *
     * @param dsMotivoSituacaoPagamento the ds motivo situacao pagamento
     */
    public void setDsMotivoSituacaoPagamento(String dsMotivoSituacaoPagamento) {
	this.dsMotivoSituacaoPagamento = dsMotivoSituacaoPagamento;
    }

    /**
     * Get: dsOperacaoProdutoServico.
     *
     * @return dsOperacaoProdutoServico
     */
    public String getDsOperacaoProdutoServico() {
	return dsOperacaoProdutoServico;
    }

    /**
     * Set: dsOperacaoProdutoServico.
     *
     * @param dsOperacaoProdutoServico the ds operacao produto servico
     */
    public void setDsOperacaoProdutoServico(String dsOperacaoProdutoServico) {
	this.dsOperacaoProdutoServico = dsOperacaoProdutoServico;
    }

    /**
     * Get: dsSituacaoOperacaoPagamento.
     *
     * @return dsSituacaoOperacaoPagamento
     */
    public String getDsSituacaoOperacaoPagamento() {
	return dsSituacaoOperacaoPagamento;
    }

    /**
     * Set: dsSituacaoOperacaoPagamento.
     *
     * @param dsSituacaoOperacaoPagamento the ds situacao operacao pagamento
     */
    public void setDsSituacaoOperacaoPagamento(String dsSituacaoOperacaoPagamento) {
	this.dsSituacaoOperacaoPagamento = dsSituacaoOperacaoPagamento;
    }

    /**
     * Get: dsTipoTela.
     *
     * @return dsTipoTela
     */
    public int getDsTipoTela() {
	return dsTipoTela;
    }

    /**
     * Set: dsTipoTela.
     *
     * @param dsTipoTela the ds tipo tela
     */
    public void setDsTipoTela(int dsTipoTela) {
	this.dsTipoTela = dsTipoTela;
    }

    /**
     * Get: dtCraditoPagamento.
     *
     * @return dtCraditoPagamento
     */
    public String getDtCraditoPagamento() {
	return dtCraditoPagamento;
    }

    /**
     * Set: dtCraditoPagamento.
     *
     * @param dtCraditoPagamento the dt cradito pagamento
     */
    public void setDtCraditoPagamento(String dtCraditoPagamento) {
	this.dtCraditoPagamento = dtCraditoPagamento;
    }

    /**
     * Get: dtEfetivacao.
     *
     * @return dtEfetivacao
     */
    public String getDtEfetivacao() {
	return dtEfetivacao;
    }

    /**
     * Set: dtEfetivacao.
     *
     * @param dtEfetivacao the dt efetivacao
     */
    public void setDtEfetivacao(String dtEfetivacao) {
	this.dtEfetivacao = dtEfetivacao;
    }

    /**
     * Get: favorecidoBeneficiarioFormatado.
     *
     * @return favorecidoBeneficiarioFormatado
     */
    public String getFavorecidoBeneficiarioFormatado() {
	return favorecidoBeneficiarioFormatado;
    }

    /**
     * Set: favorecidoBeneficiarioFormatado.
     *
     * @param favorecidoBeneficiarioFormatado the favorecido beneficiario formatado
     */
    public void setFavorecidoBeneficiarioFormatado(String favorecidoBeneficiarioFormatado) {
	this.favorecidoBeneficiarioFormatado = favorecidoBeneficiarioFormatado;
    }

    /**
     * Get: filialCfpCnpj.
     *
     * @return filialCfpCnpj
     */
    public int getFilialCfpCnpj() {
	return filialCfpCnpj;
    }

    /**
     * Set: filialCfpCnpj.
     *
     * @param filialCfpCnpj the filial cfp cnpj
     */
    public void setFilialCfpCnpj(int filialCfpCnpj) {
	this.filialCfpCnpj = filialCfpCnpj;
    }

    /**
     * Get: mensagem.
     *
     * @return mensagem
     */
    public String getMensagem() {
	return mensagem;
    }

    /**
     * Set: mensagem.
     *
     * @param mensagem the mensagem
     */
    public void setMensagem(String mensagem) {
	this.mensagem = mensagem;
    }

    /**
     * Get: nmRazaoSocial.
     *
     * @return nmRazaoSocial
     */
    public String getNmRazaoSocial() {
	return nmRazaoSocial;
    }

    /**
     * Set: nmRazaoSocial.
     *
     * @param nmRazaoSocial the nm razao social
     */
    public void setNmRazaoSocial(String nmRazaoSocial) {
	this.nmRazaoSocial = nmRazaoSocial;
    }

    /**
     * Get: nrContrato.
     *
     * @return nrContrato
     */
    public Long getNrContrato() {
	return nrContrato;
    }

    /**
     * Set: nrContrato.
     *
     * @param nrContrato the nr contrato
     */
    public void setNrContrato(Long nrContrato) {
	this.nrContrato = nrContrato;
    }

    /**
     * Get: vlEfetivoPagamentoFormatado.
     *
     * @return vlEfetivoPagamentoFormatado
     */
    public String getVlEfetivoPagamentoFormatado() {
	return vlEfetivoPagamentoFormatado;
    }

    /**
     * Set: vlEfetivoPagamentoFormatado.
     *
     * @param vlEfetivoPagamentoFormatado the vl efetivo pagamento formatado
     */
    public void setVlEfetivoPagamentoFormatado(String vlEfetivoPagamentoFormatado) {
	this.vlEfetivoPagamentoFormatado = vlEfetivoPagamentoFormatado;
    }

    /**
     * Get: vlrEfetivacaoPagamentoCliente.
     *
     * @return vlrEfetivacaoPagamentoCliente
     */
    public BigDecimal getVlrEfetivacaoPagamentoCliente() {
	return vlrEfetivacaoPagamentoCliente;
    }

    /**
     * Set: vlrEfetivacaoPagamentoCliente.
     *
     * @param vlrEfetivacaoPagamentoCliente the vlr efetivacao pagamento cliente
     */
    public void setVlrEfetivacaoPagamentoCliente(BigDecimal vlrEfetivacaoPagamentoCliente) {
	this.vlrEfetivacaoPagamentoCliente = vlrEfetivacaoPagamentoCliente;
    }

    /**
     * Get: dsRazaoSocial.
     *
     * @return dsRazaoSocial
     */
    public String getDsRazaoSocial() {
	return dsRazaoSocial;
    }

    /**
     * Set: dsRazaoSocial.
     *
     * @param dsRazaoSocial the ds razao social
     */
    public void setDsRazaoSocial(String dsRazaoSocial) {
	this.dsRazaoSocial = dsRazaoSocial;
    }

    /**
     * Get: dsResumoProdutoServico.
     *
     * @return dsResumoProdutoServico
     */
    public String getDsResumoProdutoServico() {
	return dsResumoProdutoServico;
    }

    /**
     * Set: dsResumoProdutoServico.
     *
     * @param dsResumoProdutoServico the ds resumo produto servico
     */
    public void setDsResumoProdutoServico(String dsResumoProdutoServico) {
	this.dsResumoProdutoServico = dsResumoProdutoServico;
    }

    /**
     * Get: dtCraditoPagamentoFormatada.
     *
     * @return dtCraditoPagamentoFormatada
     */
    public String getDtCraditoPagamentoFormatada() {
	return dtCraditoPagamentoFormatada;
    }

    /**
     * Set: dtCraditoPagamentoFormatada.
     *
     * @param dtCraditoPagamentoFormatada the dt cradito pagamento formatada
     */
    public void setDtCraditoPagamentoFormatada(String dtCraditoPagamentoFormatada) {
	this.dtCraditoPagamentoFormatada = dtCraditoPagamentoFormatada;
    }

    /**
     * Get: dsCodigoTabelaConsulta.
     *
     * @return dsCodigoTabelaConsulta
     */
    public Integer getDsCodigoTabelaConsulta() {
	return dsCodigoTabelaConsulta;
    }

    /**
     * Set: dsCodigoTabelaConsulta.
     *
     * @param dsCodigoTabelaConsulta the ds codigo tabela consulta
     */
    public void setDsCodigoTabelaConsulta(Integer dsCodigoTabelaConsulta) {
	this.dsCodigoTabelaConsulta = dsCodigoTabelaConsulta;
    }

	/**
	 * Get: nrArquivoRemessa.
	 *
	 * @return nrArquivoRemessa
	 */
	public Long getNrArquivoRemessa() {
		return nrArquivoRemessa;
	}

	/**
	 * Set: nrArquivoRemessa.
	 *
	 * @param nrArquivoRemessa the nr arquivo remessa
	 */
	public void setNrArquivoRemessa(Long nrArquivoRemessa) {
		this.nrArquivoRemessa = nrArquivoRemessa;
	}

	public Integer getCdFormaLiquidacao() {
		return cdFormaLiquidacao;
	}

	public void setCdFormaLiquidacao(Integer cdFormaLiquidacao) {
		this.cdFormaLiquidacao = cdFormaLiquidacao;
	}

}
