/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/implementation.ftl,v $
 * $Id: implementation.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: implementation.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */
 
package br.com.bradesco.web.pgit.service.business.assocmsgcompsal.impl;

import java.util.ArrayList;
import java.util.List;

import br.com.bradesco.web.pgit.service.business.assocmsgcompsal.IAssocMsgCompSalService;
import br.com.bradesco.web.pgit.service.business.assocmsgcompsal.IAssocMsgCompSalServiceConstants;
import br.com.bradesco.web.pgit.service.business.assocmsgcompsal.bean.DetalharAssociacaoContratoMsgEntradaDTO;
import br.com.bradesco.web.pgit.service.business.assocmsgcompsal.bean.DetalharAssociacaoContratoMsgSaidaDTO;
import br.com.bradesco.web.pgit.service.business.assocmsgcompsal.bean.ExcluirAssociacaoContratoMsgEntradaDTO;
import br.com.bradesco.web.pgit.service.business.assocmsgcompsal.bean.ExcluirAssociacaoContratoMsgSaidaDTO;
import br.com.bradesco.web.pgit.service.business.assocmsgcompsal.bean.IncluirAssociacaoContratoMsgEntradaDTO;
import br.com.bradesco.web.pgit.service.business.assocmsgcompsal.bean.IncluirAssociacaoContratoMsgSaidaDTO;
import br.com.bradesco.web.pgit.service.business.assocmsgcompsal.bean.ListarAssociacaoContratoMsgEntradaDTO;
import br.com.bradesco.web.pgit.service.business.assocmsgcompsal.bean.ListarAssociacaoContratoMsgSaidaDTO;
import br.com.bradesco.web.pgit.service.data.pdc.FactoryAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultardetalhecontratomsg.request.ConsultarDetalheContratoMsgRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultardetalhecontratomsg.response.ConsultarDetalheContratoMsgResponse;
import br.com.bradesco.web.pgit.service.data.pdc.excluirassociacaocontratomsg.request.ExcluirAssociacaoContratoMsgRequest;
import br.com.bradesco.web.pgit.service.data.pdc.excluirassociacaocontratomsg.response.ExcluirAssociacaoContratoMsgResponse;
import br.com.bradesco.web.pgit.service.data.pdc.incluirassociacaocontratomsg.request.IncluirAssociacaoContratoMsgRequest;
import br.com.bradesco.web.pgit.service.data.pdc.incluirassociacaocontratomsg.response.IncluirAssociacaoContratoMsgResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarassociacaocontratomsg.request.ListarAssociacaoContratoMsgRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarassociacaocontratomsg.response.ListarAssociacaoContratoMsgResponse;


/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Implementa��o do adaptador: ManterAssociacaoMensagemComprovanteSalarial
 * </p>
 * 
 * @comment C�DIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public class AssocMsgCompSalServiceImpl implements IAssocMsgCompSalService {
	
	/** Atributo factoryAdapter. */
	private FactoryAdapter factoryAdapter;
	
    /**
     * Construtor.
     */
    public AssocMsgCompSalServiceImpl() {
        // TODO: Implementa��o
    }
    
    /**
     * M�todo de exemplo.
     *
     * @see br.com.bradesco.web.pgit.service.business.assocmsgcompsal.IManterAssociacaoMensagemComprovanteSalarial#sampleManterAssociacaoMensagemComprovanteSalarial()
     */
    public void sampleManterAssociacaoMensagemComprovanteSalarial() {
        // TODO: Implementa�ao
    }

	/**
	 * Get: factoryAdapter.
	 *
	 * @return factoryAdapter
	 */
	public FactoryAdapter getFactoryAdapter() {
		return factoryAdapter;
	}

	/**
	 * Set: factoryAdapter.
	 *
	 * @param factoryAdapter the factory adapter
	 */
	public void setFactoryAdapter(FactoryAdapter factoryAdapter) {
		this.factoryAdapter = factoryAdapter;
	}
	
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.assocmsgcompsal.IAssocMsgCompSalService#listarAssociacaoContratoMsg(br.com.bradesco.web.pgit.service.business.assocmsgcompsal.bean.ListarAssociacaoContratoMsgEntradaDTO)
	 */
	public List<ListarAssociacaoContratoMsgSaidaDTO> listarAssociacaoContratoMsg (ListarAssociacaoContratoMsgEntradaDTO listarAssociacaoContratoMsgEntradaDTO) {
		
		List<ListarAssociacaoContratoMsgSaidaDTO> listaRetorno = new ArrayList<ListarAssociacaoContratoMsgSaidaDTO>();
		ListarAssociacaoContratoMsgRequest request = new ListarAssociacaoContratoMsgRequest();
		ListarAssociacaoContratoMsgResponse response = new ListarAssociacaoContratoMsgResponse();
		
		request.setCdCtrlMensagemSalarial(listarAssociacaoContratoMsgEntradaDTO.getTipoMensagem());
		request.setCdPessoaJuridContrato(listarAssociacaoContratoMsgEntradaDTO.getContratoPssoaJurdContr());
		request.setCdTipoContratoNegocio(listarAssociacaoContratoMsgEntradaDTO.getContratoTpoContrNegoc());
		request.setNrSeqContratoNegocio(listarAssociacaoContratoMsgEntradaDTO.getContratoSeqContrNegoc());
		request.setQtConsultas(IAssocMsgCompSalServiceConstants.QTDE_CONSULTAS_LISTAR);

		response = getFactoryAdapter().getListarAssociacaoContratoMsgPDCAdapter().invokeProcess(request);
		
		ListarAssociacaoContratoMsgSaidaDTO saidaDTO;

		for(int i=0; i<response.getOcorrenciasCount(); i++){
			saidaDTO = new ListarAssociacaoContratoMsgSaidaDTO();
			saidaDTO.setCodMensagem(response.getCodMensagem());
			saidaDTO.setMensagem(response.getMensagem());
			saidaDTO.setCodMensagemSalarial(response.getOcorrencias(i).getCdCtrlMensagemSalarial());
			saidaDTO.setDsMensagemSalarial(response.getOcorrencias(i).getDsCtrlMensagemSalarial());
			saidaDTO.setContratoPssoaJurdContr(response.getOcorrencias(i).getCdPessoaJuridContrato());
			saidaDTO.setContratoTpoContrNegoc(response.getOcorrencias(i).getCdTipoContratoNegocio());
			saidaDTO.setContratoSeqContrNegoc(response.getOcorrencias(i).getNrSeqContratoNegocio());
			listaRetorno.add(saidaDTO);
		}

		return listaRetorno;
	}
	
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.assocmsgcompsal.IAssocMsgCompSalService#incluirAssociacaoContratoMsg(br.com.bradesco.web.pgit.service.business.assocmsgcompsal.bean.IncluirAssociacaoContratoMsgEntradaDTO)
	 */
	public IncluirAssociacaoContratoMsgSaidaDTO incluirAssociacaoContratoMsg (IncluirAssociacaoContratoMsgEntradaDTO incluirAssociacaoContratoMsgEntradaDTO){
		
		IncluirAssociacaoContratoMsgSaidaDTO incluirAssociacaoContratoMsgSaidaDTO = new IncluirAssociacaoContratoMsgSaidaDTO();
		IncluirAssociacaoContratoMsgRequest incluirAssociacaoContratoMsgRequest = new IncluirAssociacaoContratoMsgRequest();
		IncluirAssociacaoContratoMsgResponse incluirAssociacaoContratoMsgResponse = new IncluirAssociacaoContratoMsgResponse();

		incluirAssociacaoContratoMsgRequest.setCdCtrlMensagemSalarial(incluirAssociacaoContratoMsgEntradaDTO.getTipoMensagem());
		incluirAssociacaoContratoMsgRequest.setCdPessoaJuridContrato(incluirAssociacaoContratoMsgEntradaDTO.getContratoPssoaJurdContr());
		incluirAssociacaoContratoMsgRequest.setCdTipoContratoNegocio(incluirAssociacaoContratoMsgEntradaDTO.getContratoTpoContrNegoc());
		incluirAssociacaoContratoMsgRequest.setNrSeqContratoNegocio(incluirAssociacaoContratoMsgEntradaDTO.getContratoSeqContrNegoc());
		incluirAssociacaoContratoMsgRequest.setQtConsultas(0);
	
		incluirAssociacaoContratoMsgResponse = getFactoryAdapter().getIncluirAssociacaoContratoMsgPDCAdapter().invokeProcess(incluirAssociacaoContratoMsgRequest);

		incluirAssociacaoContratoMsgSaidaDTO.setCodMensagem(incluirAssociacaoContratoMsgResponse.getCodMensagem());
		incluirAssociacaoContratoMsgSaidaDTO.setMensagem(incluirAssociacaoContratoMsgResponse.getMensagem());
	
		return incluirAssociacaoContratoMsgSaidaDTO;
	}
	
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.assocmsgcompsal.IAssocMsgCompSalService#excluirAssociacaoContratoMsg(br.com.bradesco.web.pgit.service.business.assocmsgcompsal.bean.ExcluirAssociacaoContratoMsgEntradaDTO)
	 */
	public ExcluirAssociacaoContratoMsgSaidaDTO excluirAssociacaoContratoMsg (ExcluirAssociacaoContratoMsgEntradaDTO excluirAssociacaoContratoMsgEntradaDTO) {
		
		ExcluirAssociacaoContratoMsgSaidaDTO excluirAssociacaoContratoMsgSaidaDTO = new ExcluirAssociacaoContratoMsgSaidaDTO();
		ExcluirAssociacaoContratoMsgRequest excluirAssociacaoContratoMsgRequest = new ExcluirAssociacaoContratoMsgRequest();
		ExcluirAssociacaoContratoMsgResponse excluirAssociacaoContratoMsgResponse = new ExcluirAssociacaoContratoMsgResponse();
		
		excluirAssociacaoContratoMsgRequest.setCdCtrlMensagemSalarial(excluirAssociacaoContratoMsgEntradaDTO.getTipoMensagem());
		excluirAssociacaoContratoMsgRequest.setCdPessoaJuridContrato(excluirAssociacaoContratoMsgEntradaDTO.getContratoPssoaJurdContr());
		excluirAssociacaoContratoMsgRequest.setCdTipoContratoNegocio(excluirAssociacaoContratoMsgEntradaDTO.getContratoTpoContrNegoc());
		excluirAssociacaoContratoMsgRequest.setNrSeqContratoNegocio(excluirAssociacaoContratoMsgEntradaDTO.getContratoSeqContrNegoc());
		excluirAssociacaoContratoMsgRequest.setQtConsultas(0);
		
		excluirAssociacaoContratoMsgResponse = getFactoryAdapter().getExcluirAssociacaoContratoMsgPDCAdapter().invokeProcess(excluirAssociacaoContratoMsgRequest);
		
		excluirAssociacaoContratoMsgSaidaDTO.setCodMensagem(excluirAssociacaoContratoMsgResponse.getCodMensagem());
		excluirAssociacaoContratoMsgSaidaDTO.setMensagem(excluirAssociacaoContratoMsgResponse.getMensagem());

		return excluirAssociacaoContratoMsgSaidaDTO;
	}
	
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.assocmsgcompsal.IAssocMsgCompSalService#detalharAssociacaoContratoMsg(br.com.bradesco.web.pgit.service.business.assocmsgcompsal.bean.DetalharAssociacaoContratoMsgEntradaDTO)
	 */
	public DetalharAssociacaoContratoMsgSaidaDTO detalharAssociacaoContratoMsg (DetalharAssociacaoContratoMsgEntradaDTO detalharAssociacaoContratoMsgEntradaDTO){
		
		DetalharAssociacaoContratoMsgSaidaDTO saidaDTO = new DetalharAssociacaoContratoMsgSaidaDTO();
		ConsultarDetalheContratoMsgRequest request = new ConsultarDetalheContratoMsgRequest();
		ConsultarDetalheContratoMsgResponse response = new ConsultarDetalheContratoMsgResponse();
		
		request.setCdCtrlMensagemSalarial(detalharAssociacaoContratoMsgEntradaDTO.getTipoMensagem());
		request.setCdPessoa(detalharAssociacaoContratoMsgEntradaDTO.getContratoPssoaJuridContr());
		request.setCdTipoContrato(detalharAssociacaoContratoMsgEntradaDTO.getContratoTpoContrNegoc());
		request.setNrSeqContrato(detalharAssociacaoContratoMsgEntradaDTO.getContratoSeqContrNegoc());
		request.setQtConsultas(0);

		response = getFactoryAdapter().getConsultarDetalheContratoMsgPDCAdapter().invokeProcess(request);
		
		saidaDTO = new DetalharAssociacaoContratoMsgSaidaDTO();
		saidaDTO.setCodMensagem(response.getCodMensagem());
		saidaDTO.setMensagem(response.getMensagem());
		saidaDTO.setCodTipoMensagem(response.getOcorrencias(0).getCdCtrlMensagemSalarial());
		saidaDTO.setDsTipoMensagem(response.getOcorrencias(0).getDsCtrlMensagemSalarial());
		saidaDTO.setContratoPssoaJurdContr(response.getOcorrencias(0).getCdPessoa());
		saidaDTO.setContratoTpoContrNegoc(response.getOcorrencias(0).getCdTipoContrato());
		saidaDTO.setContratoSeqContrNegoc(response.getOcorrencias(0).getNrSeqContrato());
		saidaDTO.setCdCanalInclusao(response.getOcorrencias(0).getCdCanalInclusao());
		saidaDTO.setDsCanalInclusao(response.getOcorrencias(0).getDsCanalInclusao());
		saidaDTO.setUsuarioInclusao(response.getOcorrencias(0).getCdAutenSegrcInclusao());
		saidaDTO.setComplementoInclusao(response.getOcorrencias(0).getNmOperFluxoInclusao());
		saidaDTO.setDataHoraInclusao(response.getOcorrencias(0).getHrInclusaoRegistro());
		saidaDTO.setCdCanalManutencao(response.getOcorrencias(0).getCdCanalManutencao());
		saidaDTO.setDsCanalManutencao(response.getOcorrencias(0).getDsCanalManutencao());
		saidaDTO.setUsuarioManutencao(response.getOcorrencias(0).getCdAutenSegrcManutencao());
		saidaDTO.setComplementoManutencao(response.getOcorrencias(0).getNmOperFluxoManutencao());
		saidaDTO.setDataHoraManutencao(response.getOcorrencias(0).getHrManutencaoRegistro());

		
		return saidaDTO;
	}
    
    
    
}

