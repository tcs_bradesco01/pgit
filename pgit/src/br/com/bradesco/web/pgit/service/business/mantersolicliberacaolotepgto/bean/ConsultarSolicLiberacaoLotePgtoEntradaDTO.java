package br.com.bradesco.web.pgit.service.business.mantersolicliberacaolotepgto.bean;

public class ConsultarSolicLiberacaoLotePgtoEntradaDTO {

	private Integer nrOcorrencias;
	private Long cdpessoaJuridicaContrato;
	private Integer cdTipoContratoNegocio;
	private Long nrSequenciaContratoNegocio;
	private Long nrLoteInterno;
	private Integer cdTipoLayoutArquivo;
	private String dtPgtoInicial;
	private String dtPgtoFinal;
	private Integer cdSituacaoSolicitacaoPagamento;
	private Integer cdMotivoSolicitacao;

	public Integer getNrOcorrencias() {
		return nrOcorrencias;
	}

	public void setNrOcorrencias(Integer nrOcorrencias) {
		this.nrOcorrencias = nrOcorrencias;
	}

	public Long getCdpessoaJuridicaContrato() {
		return cdpessoaJuridicaContrato;
	}

	public void setCdpessoaJuridicaContrato(Long cdpessoaJuridicaContrato) {
		this.cdpessoaJuridicaContrato = cdpessoaJuridicaContrato;
	}

	public Integer getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}

	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}

	public Long getNrSequenciaContratoNegocio() {
		return nrSequenciaContratoNegocio;
	}

	public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}

	public Long getNrLoteInterno() {
		return nrLoteInterno;
	}

	public void setNrLoteInterno(Long nrLoteInterno) {
		this.nrLoteInterno = nrLoteInterno;
	}

	public Integer getCdTipoLayoutArquivo() {
		return cdTipoLayoutArquivo;
	}

	public void setCdTipoLayoutArquivo(Integer cdTipoLayoutArquivo) {
		this.cdTipoLayoutArquivo = cdTipoLayoutArquivo;
	}

	public String getDtPgtoInicial() {
		return dtPgtoInicial;
	}

	public void setDtPgtoInicial(String dtPgtoInicial) {
		this.dtPgtoInicial = dtPgtoInicial;
	}

	public String getDtPgtoFinal() {
		return dtPgtoFinal;
	}

	public void setDtPgtoFinal(String dtPgtoFinal) {
		this.dtPgtoFinal = dtPgtoFinal;
	}

	public Integer getCdSituacaoSolicitacaoPagamento() {
		return cdSituacaoSolicitacaoPagamento;
	}

	public void setCdSituacaoSolicitacaoPagamento(
			Integer cdSituacaoSolicitacaoPagamento) {
		this.cdSituacaoSolicitacaoPagamento = cdSituacaoSolicitacaoPagamento;
	}

	public Integer getCdMotivoSolicitacao() {
		return cdMotivoSolicitacao;
	}

	public void setCdMotivoSolicitacao(Integer cdMotivoSolicitacao) {
		this.cdMotivoSolicitacao = cdMotivoSolicitacao;
	}

}
