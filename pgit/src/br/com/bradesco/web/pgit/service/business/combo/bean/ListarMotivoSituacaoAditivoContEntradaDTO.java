/*
 * Nome: br.com.bradesco.web.pgit.service.business.combo.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.combo.bean;

/**
 * Nome: ListarMotivoSituacaoAditivoContEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarMotivoSituacaoAditivoContEntradaDTO {
	
	/** Atributo cdSituacao. */
	private Integer cdSituacao;

	/**
	 * Get: cdSituacao.
	 *
	 * @return cdSituacao
	 */
	public Integer getCdSituacao() {
		return cdSituacao;
	}

	/**
	 * Set: cdSituacao.
	 *
	 * @param cdSituacao the cd situacao
	 */
	public void setCdSituacao(Integer cdSituacao) {
		this.cdSituacao = cdSituacao;
	}
	
}
