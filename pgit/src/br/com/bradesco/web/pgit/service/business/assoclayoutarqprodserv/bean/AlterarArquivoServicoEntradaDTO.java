package br.com.bradesco.web.pgit.service.business.assoclayoutarqprodserv.bean;


/**
 * Arquivo criado em 23/10/15.
 */
public class AlterarArquivoServicoEntradaDTO {

    private Integer cdTipoLayoutArquivo;

    private Integer cdProdutoServicoOperacao;

    private Integer cdIdentificadorLayoutNegocio;

    private Integer cdIndicadorLayoutDefault;

    public void setCdTipoLayoutArquivo(Integer cdTipoLayoutArquivo) {
        this.cdTipoLayoutArquivo = cdTipoLayoutArquivo;
    }

    public Integer getCdTipoLayoutArquivo() {
        return this.cdTipoLayoutArquivo;
    }

    public void setCdProdutoServicoOperacao(Integer cdProdutoServicoOperacao) {
        this.cdProdutoServicoOperacao = cdProdutoServicoOperacao;
    }

    public Integer getCdProdutoServicoOperacao() {
        return this.cdProdutoServicoOperacao;
    }

    public void setCdIdentificadorLayoutNegocio(Integer cdIdentificadorLayoutNegocio) {
        this.cdIdentificadorLayoutNegocio = cdIdentificadorLayoutNegocio;
    }

    public Integer getCdIdentificadorLayoutNegocio() {
        return this.cdIdentificadorLayoutNegocio;
    }

    public void setCdIndicadorLayoutDefault(Integer cdIndicadorLayoutDefault) {
        this.cdIndicadorLayoutDefault = cdIndicadorLayoutDefault;
    }

    public Integer getCdIndicadorLayoutDefault() {
        return this.cdIndicadorLayoutDefault;
    }
}