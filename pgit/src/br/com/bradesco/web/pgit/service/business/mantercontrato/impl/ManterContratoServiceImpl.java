/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/implementation.ftl,v $
 * $Id: implementation.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: implementation.ftl,v $
 * Revision 1.1  1809/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.mantercontrato.impl;

import static br.com.bradesco.web.pgit.utils.CpfCnpjUtils.formatCpfCnpjCompleto;
import static br.com.bradesco.web.pgit.utils.NumberUtils.format;
import static br.com.bradesco.web.pgit.utils.PgitUtil.concatenarCampos;
import static br.com.bradesco.web.pgit.utils.PgitUtil.formatAgencia;
import static br.com.bradesco.web.pgit.utils.PgitUtil.formatBanco;
import static br.com.bradesco.web.pgit.utils.PgitUtil.formatConta;
import static br.com.bradesco.web.pgit.utils.PgitUtil.verificaBigDecimalNulo;
import static br.com.bradesco.web.pgit.utils.PgitUtil.verificaIntegerNulo;
import static br.com.bradesco.web.pgit.utils.PgitUtil.verificaLongNulo;
import static br.com.bradesco.web.pgit.utils.PgitUtil.verificaStringNula;
import static br.com.bradesco.web.pgit.utils.PgitUtil.verificaUsuarioInternoExterno;
import static br.com.bradesco.web.pgit.view.converters.FormatarData.formatarData;
import static br.com.bradesco.web.pgit.view.converters.FormatarData.formatarDataTrilha;

import java.io.OutputStream;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import br.com.bradesco.web.aq.application.error.i18n.MessageHelperUtils;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarContasRelacionadasParaContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarContasRelacionadasParaContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarServModEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarServicosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.geral.MensagemSaida;
import br.com.bradesco.web.pgit.service.business.imprimircontrato.bean.ImprimirContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.IManterContratoService;
import br.com.bradesco.web.pgit.service.business.mantercontrato.IManterContratoServiceConstants;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.*;
import br.com.bradesco.web.pgit.service.business.simulacaotarifasnegociacao.bean.ListaOperacaoTarifaTipoServicoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.simulacaotarifasnegociacao.bean.ListaOperacaoTarifaTipoServicoSaidaDTO;
import br.com.bradesco.web.pgit.service.data.pdc.FactoryAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.alteraragenciagestora.request.AlterarAgenciaGestoraRequest;
import br.com.bradesco.web.pgit.service.data.pdc.alteraragenciagestora.response.AlterarAgenciaGestoraResponse;
import br.com.bradesco.web.pgit.service.data.pdc.alterarcondcobtarifa.request.AlterarCondCobTarifaRequest;
import br.com.bradesco.web.pgit.service.data.pdc.alterarcondcobtarifa.response.AlterarCondCobTarifaResponse;
import br.com.bradesco.web.pgit.service.data.pdc.alterarcondreajustetarifa.request.AlterarCondReajusteTarifaRequest;
import br.com.bradesco.web.pgit.service.data.pdc.alterarcondreajustetarifa.response.AlterarCondReajusteTarifaResponse;
import br.com.bradesco.web.pgit.service.data.pdc.alterarconfcontacontrato.request.AlterarConfContaContratoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.alterarconfcontacontrato.response.AlterarConfContaContratoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.alterarconfiguracaolayoutarqcontrato.request.AlterarConfiguracaoLayoutArqContratoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.alterarconfiguracaolayoutarqcontrato.response.AlterarConfiguracaoLayoutArqContratoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.alterarconfiguracaotiposervmodcontrato.request.AlterarConfiguracaoTipoServModContratoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.alterarconfiguracaotiposervmodcontrato.response.AlterarConfiguracaoTipoServModContratoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.alterardadosbasicocontrato.request.AlterarDadosBasicoContratoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.alterardadosbasicocontrato.response.AlterarDadosBasicoContratoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.alterarlayoutservico.request.AlterarLayoutServicoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.alterarlayoutservico.response.AlterarLayoutServicoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.alterarparticipantecontratopgit.request.AlterarParticipanteContratoPgitRequest;
import br.com.bradesco.web.pgit.service.data.pdc.alterarparticipantecontratopgit.response.AlterarParticipanteContratoPgitResponse;
import br.com.bradesco.web.pgit.service.data.pdc.alterarrepresentantecontratopgit.request.AlterarRepresentanteContratoPgitRequest;
import br.com.bradesco.web.pgit.service.data.pdc.alterarrepresentantecontratopgit.response.AlterarRepresentanteContratoPgitResponse;
import br.com.bradesco.web.pgit.service.data.pdc.alterartarifaacimapadrao.request.AlterarTarifaAcimaPadraoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.alterartarifaacimapadrao.response.AlterarTarifaAcimaPadraoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.alterartiporetornolayout.request.AlterarTipoRetornoLayoutRequest;
import br.com.bradesco.web.pgit.service.data.pdc.alterartiporetornolayout.response.AlterarTipoRetornoLayoutResponse;
import br.com.bradesco.web.pgit.service.data.pdc.bloqueardesbloquearcontratopgit.request.BloquearDesbloquearContratoPgitRequest;
import br.com.bradesco.web.pgit.service.data.pdc.bloqueardesbloquearcontratopgit.response.BloquearDesbloquearContratoPgitResponse;
import br.com.bradesco.web.pgit.service.data.pdc.consultarcondcobtarifa.request.ConsultarCondCobTarifaRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarcondcobtarifa.response.ConsultarCondCobTarifaResponse;
import br.com.bradesco.web.pgit.service.data.pdc.consultarcondreajustetarifa.request.ConsultarCondReajusteTarifaRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarcondreajustetarifa.response.ConsultarCondReajusteTarifaResponse;
import br.com.bradesco.web.pgit.service.data.pdc.consultarconmanambpart.request.ConsultarConManAmbPartRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarconmanambpart.response.ConsultarConManAmbPartResponse;
import br.com.bradesco.web.pgit.service.data.pdc.consultarconmanconta.request.ConsultarConManContaRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarconmanconta.response.ConsultarConManContaResponse;
import br.com.bradesco.web.pgit.service.data.pdc.consultarconmandadosbasicos.request.ConsultarConManDadosBasicosRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarconmandadosbasicos.response.ConsultarConManDadosBasicosResponse;
import br.com.bradesco.web.pgit.service.data.pdc.consultarconmanlayout.request.ConsultarConManLayoutRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarconmanlayout.response.ConsultarConManLayoutResponse;
import br.com.bradesco.web.pgit.service.data.pdc.consultarconmanparticipantes.request.ConsultarConManParticipantesRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarconmanparticipantes.response.ConsultarConManParticipantesResponse;
import br.com.bradesco.web.pgit.service.data.pdc.consultarconmanservicos.request.ConsultarConManServicosRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarconmanservicos.response.ConsultarConManServicosResponse;
import br.com.bradesco.web.pgit.service.data.pdc.consultarconmantarifas.request.ConsultarConManTarifasRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarconmantarifas.response.ConsultarConManTarifasResponse;
import br.com.bradesco.web.pgit.service.data.pdc.consultarcontacontrato.request.ConsultarContaContratoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarcontacontrato.response.ConsultarContaContratoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.consultarcontadebtarifa.request.ConsultarContaDebTarifaRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarcontadebtarifa.response.ConsultarContaDebTarifaResponse;
import br.com.bradesco.web.pgit.service.data.pdc.consultardadosbasicocontrato.request.ConsultarDadosBasicoContratoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultardadosbasicocontrato.response.ConsultarDadosBasicoContratoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.consultarlayoutarquivocontrato.request.ConsultarLayoutArquivoContratoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarlayoutarquivocontrato.response.ConsultarLayoutArquivoContratoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.consultarlistalayoutarquivocontrato.request.ConsultarListaLayoutArquivoContratoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarlistalayoutarquivocontrato.response.ConsultarListaLayoutArquivoContratoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.consultarlistaoperacaotarifatiposervico.request.ConsultarListaOperacaoTarifaTipoServicoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarlistaoperacaotarifatiposervico.response.ConsultarListaOperacaoTarifaTipoServicoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.consultarlistatiporetornolayout.request.ConsultarListaTipoRetornoLayoutRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarlistatiporetornolayout.response.ConsultarListaTipoRetornoLayoutResponse;
import br.com.bradesco.web.pgit.service.data.pdc.consultarmanutencaoparticipante.request.ConsultarManutencaoParticipanteRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarmanutencaoparticipante.response.ConsultarManutencaoParticipanteResponse;
import br.com.bradesco.web.pgit.service.data.pdc.consultartarifacontrato.request.ConsultarTarifaContratoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultartarifacontrato.response.ConsultarTarifaContratoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.detalharcontarelacionadahist.request.DetalharContaRelacionadaHistRequest;
import br.com.bradesco.web.pgit.service.data.pdc.detalharcontarelacionadahist.response.DetalharContaRelacionadaHistResponse;
import br.com.bradesco.web.pgit.service.data.pdc.detalharhistoricodebtarifa.request.DetalharHistoricoDebTarifaRequest;
import br.com.bradesco.web.pgit.service.data.pdc.detalharhistoricodebtarifa.response.DetalharHistoricoDebTarifaResponse;
import br.com.bradesco.web.pgit.service.data.pdc.detalharhistoricolayoutcontrato.request.DetalharHistoricoLayoutContratoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.detalharhistoricolayoutcontrato.response.DetalharHistoricoLayoutContratoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.detalhartiporetornolayout.request.DetalharTipoRetornoLayoutRequest;
import br.com.bradesco.web.pgit.service.data.pdc.detalhartiporetornolayout.response.DetalharTipoRetornoLayoutResponse;
import br.com.bradesco.web.pgit.service.data.pdc.detalhartiposervmodcontrato.request.DetalharTipoServModContratoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.detalhartiposervmodcontrato.response.DetalharTipoServModContratoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.encerrarcontrato.request.EncerrarContratoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.encerrarcontrato.response.EncerrarContratoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.excluircontacontrato.request.ExcluirContaContratoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.excluircontacontrato.response.ExcluirContaContratoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.excluirlayout.request.ExcluirLayoutRequest;
import br.com.bradesco.web.pgit.service.data.pdc.excluirlayout.response.ExcluirLayoutResponse;
import br.com.bradesco.web.pgit.service.data.pdc.excluirparticipantecontratopgit.request.ExcluirParticipanteContratoPgitRequest;
import br.com.bradesco.web.pgit.service.data.pdc.excluirparticipantecontratopgit.response.ExcluirParticipanteContratoPgitResponse;
import br.com.bradesco.web.pgit.service.data.pdc.excluirrelaciassoc.request.ExcluirRelaciAssocRequest;
import br.com.bradesco.web.pgit.service.data.pdc.excluirrelaciassoc.response.ExcluirRelaciAssocResponse;
import br.com.bradesco.web.pgit.service.data.pdc.excluirtiporetornolayout.request.ExcluirTipoRetornoLayoutRequest;
import br.com.bradesco.web.pgit.service.data.pdc.excluirtiporetornolayout.response.ExcluirTipoRetornoLayoutResponse;
import br.com.bradesco.web.pgit.service.data.pdc.excluirtiposervmodcontrato.request.ExcluirTipoServModContratoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.excluirtiposervmodcontrato.response.ExcluirTipoServModContratoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.incluircontacontrato.request.IncluirContaContratoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.incluircontacontrato.response.IncluirContaContratoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.incluircontadebtarifa.request.IncluirContaDebTarifaRequest;
import br.com.bradesco.web.pgit.service.data.pdc.incluircontadebtarifa.response.IncluirContaDebTarifaResponse;
import br.com.bradesco.web.pgit.service.data.pdc.incluirlayout.request.IncluirLayoutRequest;
import br.com.bradesco.web.pgit.service.data.pdc.incluirlayout.response.IncluirLayoutResponse;
import br.com.bradesco.web.pgit.service.data.pdc.incluirparticipantecontratopgit.request.IncluirParticipanteContratoPgitRequest;
import br.com.bradesco.web.pgit.service.data.pdc.incluirparticipantecontratopgit.response.IncluirParticipanteContratoPgitResponse;
import br.com.bradesco.web.pgit.service.data.pdc.incluirrelaccontacontrato.request.IncluirRelacContaContratoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.incluirrelaccontacontrato.response.IncluirRelacContaContratoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.incluirrepresentante.request.IncluirRepresentanteRequest;
import br.com.bradesco.web.pgit.service.data.pdc.incluirrepresentante.response.IncluirRepresentanteResponse;
import br.com.bradesco.web.pgit.service.data.pdc.incluirtiposervmodcontrato.request.IncluirTipoServModContratoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.incluirtiposervmodcontrato.response.IncluirTipoServModContratoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.inlcuirtiporetornolayout.request.InlcuirTipoRetornoLayoutRequest;
import br.com.bradesco.web.pgit.service.data.pdc.inlcuirtiporetornolayout.response.InlcuirTipoRetornoLayoutResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listaragregado.request.ListarAgregadoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listaragregado.response.ListarAgregadoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarconmanambpart.request.ListarConManAmbPartRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarconmanambpart.response.ListarConManAmbPartResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarconmancontavinculada.request.ListarConManContaVinculadaRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarconmancontavinculada.response.ListarConManContaVinculadaResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarconmandadosbasicos.request.ListarConManDadosBasicosRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarconmandadosbasicos.response.ListarConManDadosBasicosResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarconmanlayout.request.ListarConManLayoutRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarconmanlayout.response.ListarConManLayoutResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarconmanparticipante.request.ListarConManParticipanteRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarconmanparticipante.response.ListarConManParticipanteResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarconmanservico.request.ListarConManServicoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarconmanservico.response.ListarConManServicoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarconmantarifa.request.ListarConManTarifaRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarconmantarifa.response.ListarConManTarifaResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarcontadebtarifa.request.ListarContaDebTarifaRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarcontadebtarifa.response.ListarContaDebTarifaResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarcontasexclusao.request.ListarContasExclusaoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarcontasexclusao.response.ListarContasExclusaoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarcontaspossiveisinclusao.request.ListarContasPossiveisInclusaoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarcontaspossiveisinclusao.response.ListarContasPossiveisInclusaoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarcontasrelacionadasconta.request.ListarContasRelacionadasContaRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarcontasrelacionadasconta.response.ListarContasRelacionadasContaResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarcontasrelacionadashist.request.ListarContasRelacionadasHistRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarcontasrelacionadashist.response.ListarContasRelacionadasHistResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarcontasrelacionadasparacontrato.request.ListarContasRelacionadasParaContratoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarcontasrelacionadasparacontrato.response.ListarContasRelacionadasParaContratoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarcontasvinculadascontrato.request.ListarContasVinculadasContratoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarcontasvinculadascontrato.response.ListarContasVinculadasContratoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarcontasvinculadascontratoloop.request.ListarContasVinculadasContratoLoopRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarcontasvinculadascontratoloop.response.ListarContasVinculadasContratoLoopResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarhistoricodebtarifa.request.ListarHistoricoDebTarifaRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarhistoricodebtarifa.response.ListarHistoricoDebTarifaResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarhistoricolayoutcontrato.request.ListarHistoricoLayoutContratoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarhistoricolayoutcontrato.response.ListarHistoricoLayoutContratoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarmodalidadetiposervico.request.ListarModalidadeTipoServicoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarmodalidadetiposervico.response.ListarModalidadeTipoServicoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listaroperacaocontadebtarifa.request.ListarOperacaoContaDebTarifaRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listaroperacaocontadebtarifa.response.ListarOperacaoContaDebTarifaResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarparticipanteagregado.request.ListarParticipanteAgregadoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarparticipanteagregado.response.ListarParticipanteAgregadoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarparticipanteagregado.response.Ocorrencias;
import br.com.bradesco.web.pgit.service.data.pdc.listarparticipantes.request.ListarParticipantesRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarparticipantes.response.ListarParticipantesResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarparticipantescontrato.request.ListarParticipantesContratoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarparticipantescontrato.response.ListarParticipantesContratoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarparticipantesloop.request.ListarParticipantesLoopRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarparticipantesloop.response.ListarParticipantesLoopResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarrepresentante.request.ListarRepresentanteRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarrepresentante.response.ListarRepresentanteResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarservicorelacionadocdps.request.ListarServicoRelacionadoCdpsRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarservicorelacionadocdps.response.ListarServicoRelacionadoCdpsResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarservicorelacionadocdpsincluir.request.ListarServicoRelacionadoCdpsIncluirRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarservicorelacionadocdpsincluir.response.ListarServicoRelacionadoCdpsIncluirResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listartarifascontrato.request.ListarTarifasContratoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listartarifascontrato.response.ListarTarifasContratoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listartipolayoutcontrato.request.ListarTipoLayoutContratoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listartipolayoutcontrato.response.ListarTipoLayoutContratoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.recuperarcontrato.request.RecuperarContratoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.recuperarcontrato.response.RecuperarContratoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.substituirrelaccontacontrato.request.SubstituirRelacContaContratoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.substituirrelaccontacontrato.response.SubstituirRelacContaContratoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.validarpropostaandamento.request.ValidarPropostaAndamentoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.validarpropostaandamento.response.ValidarPropostaAndamentoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.validarqtddiascobrancaapsapuracao.request.ValidarQtdDiasCobrancaApsApuracaoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.validarqtddiascobrancaapsapuracao.response.ValidarQtdDiasCobrancaApsApuracaoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.validarusuario.request.ValidarUsuarioRequest;
import br.com.bradesco.web.pgit.service.data.pdc.validarusuario.response.ValidarUsuarioResponse;
import br.com.bradesco.web.pgit.service.data.pdc.validarvinculacaoconveniocontasalario.request.ValidarVinculacaoConvenioContaSalarioRequest;
import br.com.bradesco.web.pgit.service.data.pdc.validarvinculacaoconveniocontasalario.response.ValidarVinculacaoConvenioContaSalarioResponse;
import br.com.bradesco.web.pgit.service.data.pdc.verificaratributosdadosbasicos.request.VerificarAtributosDadosBasicosRequest;
import br.com.bradesco.web.pgit.service.data.pdc.verificaratributosdadosbasicos.response.VerificarAtributosDadosBasicosResponse;
import br.com.bradesco.web.pgit.service.data.pdc.verificaratributosservicomodalidade.request.VerificarAtributosServicoModalidadeRequest;
import br.com.bradesco.web.pgit.service.data.pdc.verificaratributosservicomodalidade.response.VerificarAtributosServicoModalidadeResponse;
import br.com.bradesco.web.pgit.service.report.mantercontrato.ManterContratoReport;
import br.com.bradesco.web.pgit.utils.NumberUtils;
import br.com.bradesco.web.pgit.utils.PgitUtil;
import br.com.bradesco.web.pgit.view.converters.FormatarData;

import com.lowagie.text.DocumentException;

/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Implementa��o do adaptador: ManterContrato
 * </p>
 * 
 * @comment C�DIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public class ManterContratoServiceImpl implements IManterContratoService {

	/** Atributo factoryAdapter. */
	private FactoryAdapter factoryAdapter = null;

	/**
	 * Get: factoryAdapter.
	 * 
	 * @return factoryAdapter
	 */
	public FactoryAdapter getFactoryAdapter() {
		return factoryAdapter;
	}

	/**
	 * Set: factoryAdapter.
	 * 
	 * @param factoryAdapter
	 *            the factory adapter
	 */
	public void setFactoryAdapter(FactoryAdapter factoryAdapter) {
		this.factoryAdapter = factoryAdapter;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.service.business.mantercontrato.
	 *      IManterContratoService#alterarDadosBasicoContrato
	 *      (br.com.bradesco.web.pgit.service.business.mantercontrato.bean.AlterarDadosBasicoContratoEntradaDTO)
	 */
	public AlterarDadosBasicoContratoSaidaDTO alterarDadosBasicoContrato(
			AlterarDadosBasicoContratoEntradaDTO entrada) {
		AlterarDadosBasicoContratoRequest request = new AlterarDadosBasicoContratoRequest();

		request.setCdPessoaJuridicaContrato(verificaLongNulo(entrada
				.getCdPessoaJuridicaContrato()));
		request.setCdTipoContratoNegocio(verificaIntegerNulo(entrada
				.getCdTipoContratoNegocio()));
		request.setNrSequenciaContratoNegocio(verificaLongNulo(entrada
				.getNrSequenciaContratoNegocio()));
		request.setCdSituacaoContrato(verificaIntegerNulo(entrada
				.getCdSituacaoContrato()));
		request.setCdMotivoSituacaoOperacao(verificaIntegerNulo(entrada
				.getCdMotivoSituacaoOperacao()));
		request.setCdIndicadorEconomicoMoeda(verificaIntegerNulo(entrada
				.getCdIndicadorEconomicoMoeda()));
		request.setCdIdioma(verificaIntegerNulo(entrada.getCdIdioma()));
		request.setCdSetorContrato(verificaIntegerNulo(entrada
				.getCdSetorContrato()));
		request.setCdPermissaoEncerramentoContrato(verificaStringNula(entrada
				.getCdPermissaoEncerramentoContrato()));
		request.setDsContratoNegocio(verificaStringNula(entrada
				.getDsContratoNegocio()));
		request.setHrAberturaContratoNegocio(verificaStringNula(entrada
				.getHrAberturaContratoNegocio()));
		request.setCdUsuario(verificaStringNula(entrada.getCdUsuario()));
		request.setCdUnidadeOperacionalContrato(verificaIntegerNulo(entrada
				.getCdUnidadeOperacionalContrato()));

		String zeros = "000000000";
		String cdFuncionario = "";
		if (entrada.getCdFuncionarioBradesco() != null
				&& entrada.getCdFuncionarioBradesco().length() < 9) {

			cdFuncionario = zeros.substring(0,
					9 - entrada.getCdFuncionarioBradesco().length()).concat(
					entrada.getCdFuncionarioBradesco());
		}

		request.setCdFuncionarioBradesco(verificaStringNula(cdFuncionario));
		request.setCdPessoaJuridica(verificaLongNulo(entrada
				.getCdPessoaJuridica()));
		request.setNrSequenciaUnidadeOrganizacional(verificaIntegerNulo(entrada
				.getNrSequenciaUnidadeOrganizacional()));
		request.setVlSaldoVirtualTeste(verificaBigDecimalNulo(entrada
				.getVlSaldoVirtualTeste()));
		request.setCdFormaAutorizacaoPagamento(verificaIntegerNulo(entrada.getCdFormaAutorizacaoPagamento()));

		AlterarDadosBasicoContratoResponse response = getFactoryAdapter()
				.getAlterarDadosBasicoContratoPDCAdapter().invokeProcess(
						request);

		AlterarDadosBasicoContratoSaidaDTO saidaDTO = new AlterarDadosBasicoContratoSaidaDTO();
		saidaDTO.setCodMensagem(response.getCodMensagem());
		saidaDTO.setMensagem(response.getMensagem());

		return saidaDTO;

	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.service.business.mantercontrato.
	 *      IManterContratoService#consultarDadosBasicoContrato
	 *      (br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ConsultarDadosBasicoContratoEntradaDTO)
	 */
	public ConsultarDadosBasicoContratoSaidaDTO consultarDadosBasicoContrato(
			ConsultarDadosBasicoContratoEntradaDTO entrada) {
		ConsultarDadosBasicoContratoRequest request = new ConsultarDadosBasicoContratoRequest();

		request.setCdPessoaJuridicaContrato(entrada
				.getCdPessoaJuridicaContrato());
		request.setCdTipoContratoNegocio(entrada.getCdTipoContrato());
		request.setNrSequenciaContratoNegocio(entrada
				.getNumeroSequenciaContrato());
		request.setCdClub(entrada.getCdClub());
		request.setCdCnpjCpf(entrada.getCdCgcCpf());
		request.setCdFilialCnpjCpf(entrada.getCdFilialCgcCpf());
		request.setCdControleCnpjCpf(entrada.getCdControleCgcCpf());

		ConsultarDadosBasicoContratoResponse response = getFactoryAdapter()
				.getConsultarDadosBasicoContratoPDCAdapter().invokeProcess(
						request);

		ConsultarDadosBasicoContratoSaidaDTO saidaDTO = new ConsultarDadosBasicoContratoSaidaDTO();
		saidaDTO = new ConsultarDadosBasicoContratoSaidaDTO();
		saidaDTO.setCodMensagem(response.getCodMensagem());
		saidaDTO.setMensagem(response.getMensagem());
		saidaDTO.setCdGrupoEconomicoParticipante(response
				.getCdGrupoEconomicoMaster());
		saidaDTO.setDsGrupoEconomico(response.getDsGrupoEconomicoMaster());
		saidaDTO.setCdAtividadeEconomicaParticipante(response
				.getCdAtividadeEconomicaMaster());
		saidaDTO.setDsAtividadeEconomica(response
				.getDsAtividadeEconomicaMaster());
		saidaDTO.setCdSegmentoEconomicoParticipante(response
				.getCdSegmentoParticipanteMaster());
		saidaDTO.setDsSegmentoEconomico(response
				.getDsSegmentoParticipanteMaster());
		saidaDTO.setCdSubSegmentoEconomico(response.getCdSubSegmentoMaster());
		saidaDTO.setDsSubSegmentoEconomico(response.getDsSubSegmentoMaster());
		saidaDTO.setNomeContrato(response.getDsContratoNegocio());
		saidaDTO.setCdIdioma(response.getCdIdioma());
		saidaDTO.setDsIdioma(response.getDsIdioma());
		saidaDTO.setCdMoeda(response.getCdMoeda());
		saidaDTO.setDsMoeda(response.getDsMoeda());
		saidaDTO.setCdOrigem(response.getCdOrigem());
		saidaDTO.setDsOrigem(response.getDeOrigem());
		saidaDTO.setDsPossuiAditivo(response.getIndicadorAditivo());
		saidaDTO.setCdSituacaoContrato(response.getCdSituacaoMotivoContrato());
		saidaDTO.setDsSituacaoContrato(response.getDsSituacaoMotivoContrato());
		saidaDTO.setDataCadastroContrato(response.getDtCadastroContrato());
		saidaDTO.setHoraCadastroContrato(response.getHrCadastroContrato());
		saidaDTO.setDataVigenciaContrato(response.getDtVigenciaContrato());
		saidaDTO.setHoraVigenciaContrato(response.getHrVigenciaContrato());
		saidaDTO.setNumeroComercialContrato(response.getNrComercialContrato());
		saidaDTO.setDsTipoParticipacaoContrato(response
				.getTipoParticipacaoContrato());
		saidaDTO.setDataAssinaturaContrato(response
				.getDtAssinaturaContratoNegocio());
		saidaDTO.setHoraAssinaturaContrato(response
				.getHrAssinaturaContratoNegocio());
		saidaDTO.setCdIndicaPermiteEncerramento(response
				.getCdIndicadorEncerramentoContrato());
		saidaDTO.setDsIndicaPermiteEncerramento(response
				.getDsindicadorEncerramentoContrato());
		saidaDTO.setCdAgenciaOperadora(response
				.getCdUnidadeOperacionalContrato());
		saidaDTO.setNomeAgenciaOperadora(response
				.getNrUnidadeOperacionalContrato());
		saidaDTO.setCdAgenciaContabil(response.getCdUnidadeContabilContrato());
		saidaDTO
				.setNomeAgenciaContabil(response.getNrUnidadeContabilContrato());
		saidaDTO.setCdDeptoGestor(response.getCdUnidadeGestoraContrato());
		saidaDTO.setDsDeptoGestor(response.getNrUnidadeGestoraContrato());
		saidaDTO.setCdFuncionarioBradesco(response.getCdFuncionarioBradesco());
		saidaDTO
				.setDsFuncionarioBradesco(response.getNomeFuncionarioBradesco());
		saidaDTO.setCdPessoaJuridica(response.getCdPessoaJuridica());
		saidaDTO.setNumeroSequencialUnidadeOrganizacional(response
				.getNrSequenciaUnidadeOrganizacional());
		saidaDTO.setDataInclusaoContrato(response.getDtInclusao());
		saidaDTO.setHoraInclusaoContrato(response.getHrInclusao());
		saidaDTO.setUsuarioInclusaoContrato(response.getCdUsuarioInclusao());
		saidaDTO.setCdTipoCanalInclusao(response.getCdTipoCanalInclusao());
		saidaDTO.setDsTipoCanalInclusao(response.getDsTipoCanalInclusao());
		saidaDTO.setOperacaoFluxoInclusao(response.getOperacaoFluxoInclusao());
		saidaDTO.setDataManutencaoContrato(response.getDtManutencao());
		saidaDTO.setHoraManutencaoContrato(response.getHrManutencao());
		saidaDTO
				.setUsuarioManutencaoContrato(response.getCdUsuarioManutencao());
		saidaDTO.setCdTipoCanalManutencao(response.getCdTipoCanalManutencao());
		saidaDTO.setDsTipoCanalManutencao(response.getDsTipoCanalManutencao());
		saidaDTO.setOperacaoFluxoManutencao(response
				.getOperacaoFluxoManutencao());
		saidaDTO.setCdSetorContrato(response.getCdSetorContrato());
		saidaDTO.setDsSetorContrato(response.getDsSetorContrato());
		saidaDTO.setCdPessoaJuridicaProposta(response
				.getCdPessoaJuridicaProposta());
		saidaDTO
				.setCdTipoContratoProposta(response.getCdTipoContratoProposta());
		if (response.getNrContratoProposta() == 0) {
			saidaDTO.setNrContratoProposta(null);
		} else {
			saidaDTO.setNrContratoProposta(response.getNrContratoProposta());
		}
		saidaDTO.setDsPessoaJuridicaProposta(response
				.getDsPessoaJuridicaProposta());
		saidaDTO
				.setDsTipoContratoProposta(response.getDsTipoContratoProposta());
		saidaDTO.setVlSaldoVirtualTeste(response.getVlSaldoVirtualTeste());
		saidaDTO.setNrSequenciaContratoOutros(response
				.getNrSequenciaContratoOutros());
		saidaDTO.setNrSequenciaContratoPagamentoSalario(response
				.getNrSequenciaContratoPagamentoSalario());
		saidaDTO.setCdFormaAutorizacaoPagamento(response.getCdFormaAutorizacaoPagamento());
        saidaDTO.setDsFormaAutorizacaoPagamento(response.getDsFormaAutorizacaoPagamento());
		return saidaDTO;

	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.service.business.mantercontrato.
	 *      IManterContratoService#encerrarContrato
	 *      (br.com.bradesco.web.pgit.service.business.mantercontrato.bean.EncerrarContratoEntradaDTO)
	 */
	public EncerrarContratoSaidaDTO encerrarContrato(
			EncerrarContratoEntradaDTO entrada) {
		EncerrarContratoRequest request = new EncerrarContratoRequest();
		request.setCdPessoaJuridicaContrato(verificaLongNulo(entrada
				.getCdPessoaJuridicaContrato()));
		request.setCdTipoContratoNegocio(verificaIntegerNulo(entrada
				.getCdTipoContratoNegocio()));
		request.setNrSequenciaContratoNegocio(verificaLongNulo(entrada
				.getNumeroSequencialContratoNegocio()));
		request.setCdSituacaoMotivoContrato(verificaIntegerNulo(entrada
				.getCdSituacaoContrato()));

		EncerrarContratoResponse response = getFactoryAdapter()
				.getEncerrarContratoPDCAdapter().invokeProcess(request);

		EncerrarContratoSaidaDTO saidaDTO = new EncerrarContratoSaidaDTO();
		saidaDTO.setCodMensagem(response.getCodMensagem());
		saidaDTO.setMensagem(response.getMensagem());
		return saidaDTO;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.service.business.mantercontrato.
	 *      IManterContratoService#alterarAgenciaGestora
	 *      (br.com.bradesco.web.pgit.service.business.mantercontrato.bean.AlterarAgenciaGestoraEntradaDTO)
	 */
	public AlterarAgenciaGestoraSaidaDTO alterarAgenciaGestora(
			AlterarAgenciaGestoraEntradaDTO entrada) {
		AlterarAgenciaGestoraRequest request = new AlterarAgenciaGestoraRequest();
		request.setCdpessoaJuridicaContrato(verificaLongNulo(entrada
				.getCdpessoaJuridicaContrato()));
		request.setCdTipoContratoNegocio(verificaIntegerNulo(entrada
				.getCdTipoContratoNegocio()));
		request.setNrSequenciaContratoNegocio(verificaLongNulo(entrada
				.getNrSequenciaContratoNegocio()));
		request.setNuAgencia(verificaIntegerNulo(entrada.getNuAgencia()));

		AlterarAgenciaGestoraResponse response = getFactoryAdapter()
				.getAlterarAgenciaGestoraPDCAdapter().invokeProcess(request);

		AlterarAgenciaGestoraSaidaDTO saidaDTO = new AlterarAgenciaGestoraSaidaDTO();
		saidaDTO.setCodMensagem(response.getCodMensagem());
		saidaDTO.setMensagem(response.getMensagem());
		return saidaDTO;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.service.business.mantercontrato.IManterContratoService#recuperarContrato
	 *      (br.com.bradesco.web.pgit.service.business.mantercontrato.bean.RecuperarContratoEntradaDTO)
	 */
	public RecuperarContratoSaidaDTO recuperarContrato(
			RecuperarContratoEntradaDTO entradaDTO) {
		RecuperarContratoRequest request = new RecuperarContratoRequest();
		request.setCdPessoaJuridicaContrato(verificaLongNulo(entradaDTO
				.getCdPessoaJuridicaContrato()));
		request.setCdSituacaoMotivoContrato(verificaIntegerNulo(entradaDTO
				.getCdSituacaoMotivoContrato()));
		request.setCdTipoContratoNegocio(verificaIntegerNulo(entradaDTO
				.getCdTipoContratoNegocio()));
		request.setNrSequenciaContratoNegocio(verificaLongNulo(entradaDTO
				.getNrSequenciaContratoNegocio()));

		RecuperarContratoResponse response = getFactoryAdapter()
				.getRecuperarContratoPDCAdapter().invokeProcess(request);

		RecuperarContratoSaidaDTO saidaDTO = new RecuperarContratoSaidaDTO();
		saidaDTO.setCodMesangem(response.getCodMensagem());
		saidaDTO.setMensagem(response.getMensagem());
		return saidaDTO;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.service.business.mantercontrato.
	 *      IManterContratoService#incluirContaContrato
	 *      (br.com.bradesco.web.pgit.service.business.mantercontrato.bean.IncluirContaContratoEntradaDTO)
	 */
	public IncluirContaContratoSaidaDTO incluirContaContrato(
			IncluirContaContratoEntradaDTO entrada) {
		IncluirContaContratoRequest request = new IncluirContaContratoRequest();
		request.setCdPessoaJuridica(entrada.getCdPessoaJuridica());
		request
				.setCdPessoaJuridicaVinculo(entrada
						.getCdPessoaJuridicaVinculo());
		request.setCdTipoContratoNegocio(entrada.getCdTipoContratoNegocio());
		request.setCdTipoContratoNegocioVinculo(entrada
				.getCdTipoContratoNegocioVinculo());

		request.setCdTipoVinculoContrato(entrada.getCdTipoVinculoContrato());
		request.setNrSequenciaContratoNegocio(entrada
				.getNrSequenciaContratoNegocio());
		request.setNrSequenciaContratoNegocioVinculo(entrada
				.getNrSequenciaContratoNegocioVinculo());

		IncluirContaContratoResponse response = getFactoryAdapter()
				.getIncluirContaContratoPDCAdapter().invokeProcess(request);

		IncluirContaContratoSaidaDTO saidaDTO = new IncluirContaContratoSaidaDTO();
		saidaDTO.setCodMensagem(response.getCodMensagem());
		saidaDTO.setMensagem(response.getMensagem());
		return saidaDTO;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.service.business.mantercontrato.
	 *      IManterContratoService#alterarConfiguracaoContraContrato
	 *      (br.com.bradesco.web.pgit.service.business.mantercontrato.bean.AlterarConfContaContEntradaDTO)
	 */
	public AlterarConfContaContSaidaDTO alterarConfiguracaoContraContrato(
			AlterarConfContaContEntradaDTO entrada) {
		AlterarConfContaContratoRequest request = new AlterarConfContaContratoRequest();

		request.setCdMotivoSituacaoConta(entrada.getCdMotivoSituacaoConta());
		request.setCdMunicipio(entrada.getCdMunicipio());
		request
				.setCdMunicipioFeriadoLocal(entrada
						.getCdMunicipioFeriadoLocal());
		request.setCdPessoaJuridicaContrato(entrada
				.getCdPessoaJuridicaContrato());
		request
				.setCdPessoaJuridicaVinculo(entrada
						.getCdPessoaJuridicaVinculo());
		request.setCdSituacaoVinculacaoConta(entrada
				.getCdSituacaoVinculacaoConta());
		request.setCdTipoContratoNegocio(entrada.getCdTipoContratoNegocio());
		request.setCdTipoContratoNegocioVinculo(entrada
				.getCdTipoContratoNegocioVinculo());
		request.setCdTipoVinculoContrato(entrada.getCdTipoVinculoContrato());
		request.setNrSequenciaContratoNegocio(entrada
				.getNrSequenciaContratoNegocio());
		request.setNrSequenciaContratoVinculo(entrada
				.getNrSequenciaContratoVinculo());
		request.setDescApelido(PgitUtil.verificaStringNula(entrada.getDescApelido()));
		
		
		request.setNumOcorrenciasEntrada(entrada.getListaFinalidade().size());

		for (int i = 0; i < request.getNumOcorrenciasEntrada(); i++) {
            br.com.bradesco.web.pgit.service.data.pdc.alterarconfcontacontrato.request.Ocorrencias ocorrencia =
                new br.com.bradesco.web.pgit.service.data.pdc.alterarconfcontacontrato.request.Ocorrencias();
			ocorrencia.setCdFinalidadeContaPagamento(entrada
					.getListaFinalidade().get(i)
					.getCdFinalidadeContaPagamento());
			ocorrencia.setCdTipoAcaoFinalidade(entrada.getListaFinalidade()
					.get(i).getCdTipoAcaoFinalidade());
			request.addOcorrencias(ocorrencia);
		}

		AlterarConfContaContratoResponse response = getFactoryAdapter()
				.getAlterarConfContaContratoPDCAdapter().invokeProcess(request);

		AlterarConfContaContSaidaDTO saida = new AlterarConfContaContSaidaDTO();
		saida.setCodMensagem(response.getCodMensagem());
		saida.setMensagem(response.getMensagem());
		return saida;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.service.business.mantercontrato.
	 *      IManterContratoService#incluirRelacionamentoContaContrato
	 *      (br.com.bradesco.web.pgit.service.business.mantercontrato.bean.IncluirRelacContaContEntradaDTO)
	 */
	public IncluirRelacContaContSaidaDTO incluirRelacionamentoContaContrato(
			IncluirRelacContaContEntradaDTO entrada) {
		IncluirRelacContaContratoRequest request = new IncluirRelacContaContratoRequest();
		request.setCdFinalidadeContaPagamento(entrada
				.getCdFinalidadeContaPagamento());
		request.setCdPessoaJuridicaContrato(entrada
				.getCdPessoaJuridicaContrato());
		request.setCdPessoaJuridicaRelacionada(entrada
				.getCdPessoaJuridicaRelacionada());
		request.setCdTipoContratoNegocio(entrada.getCdTipoContratoNegocio());
		request.setCdTipoContratoRelacionado(entrada
				.getCdTipoContratoRelacionado());
		request.setCdTipoRelacionamentoConta(entrada
				.getCdTipoRelacionamentoConta());
		request.setCdTipoVinculoContrato(entrada.getCdTipoVinculoContrato());
		request.setNrSequenciaContratoNegocio(entrada
				.getNrSequenciaContratoNegocio());
		request.setNrSequenciaContratoRelacionado(entrada
				.getNrSequenciaContratoRelacionado());
		request.setNumeroOcorrencias(entrada.getOcorrenciasContas().size());
		request.setCdPsooa(entrada.getCdPssoa());
		request.setCpfCNPJ(entrada.getCpfCNPJ());
		request.setCdTipoInscricao(entrada.getCdTipoInscricao());

        ArrayList<br.com.bradesco.web.pgit.service.data.pdc.incluirrelaccontacontrato.request.Ocorrencias> ocorrencias =
            new ArrayList<br.com.bradesco.web.pgit.service.data.pdc.incluirrelaccontacontrato.request.Ocorrencias>();

		// V�rias Ocorr�ncias para Inclus�o
		for (int i = 0; i < request.getNumeroOcorrencias(); i++) {
            br.com.bradesco.web.pgit.service.data.pdc.incluirrelaccontacontrato.request.Ocorrencias ocorrencia =
                new br.com.bradesco.web.pgit.service.data.pdc.incluirrelaccontacontrato.request.Ocorrencias();
			ocorrencia
					.setCdPessoaJuridicaVinculo(entrada.getOcorrenciasContas()
							.get(i).getCdPessoaJuridicaVinculo());
			ocorrencia.setCdTipoContratoVinculo(entrada.getOcorrenciasContas()
					.get(i).getCdTipoContratoVinculo());
			ocorrencia.setNrSequenciaContratoVinculo(entrada
					.getOcorrenciasContas().get(i)
					.getNrSequenciaContratoVinculo());
			ocorrencias.add(ocorrencia);
		}

        request.setOcorrencias(ocorrencias
            .toArray(new br.com.bradesco.web.pgit.service.data.pdc.incluirrelaccontacontrato.request.Ocorrencias[0]));

		IncluirRelacContaContratoResponse response = getFactoryAdapter().getIncluirRelacContaContratoPDCAdapter().invokeProcess(request);

		IncluirRelacContaContSaidaDTO saida = new IncluirRelacContaContSaidaDTO();
		saida.setCodMensagem(response.getCodMensagem());
		saida.setMensagem(response.getMensagem());
		return saida;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.service.business.mantercontrato.
	 *      IManterContratoService#excluirContaContrato
	 *      (br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ExcluirContaContratoEntradaDTO)
	 */
	public ExcluirContaContratoSaidaDTO excluirContaContrato(
			ExcluirContaContratoEntradaDTO entrada) {
		ExcluirContaContratoRequest request = new ExcluirContaContratoRequest();
		request.setCdPessoaJuridica(entrada.getCdPessoaJuridica());
		request
				.setCdPessoaJuridicaVinculo(entrada
						.getCdPessoaJuridicaVinculo());
		request.setCdTipoContratoNegocio(entrada.getCdTipoContratoNegocio());
		request.setCdTipoContratoVinculo(entrada.getCdTipoContratoVinculo());
		request.setCdTipoVinculoContrato(entrada.getCdTipoVinculoContrato());
		request.setNrSequenciaContratoNegocio(entrada
				.getNrSequenciaContratoNegocio());
		request.setNrSequenciaContratoVinculo(entrada
				.getNrSequenciaContratoVinculo());
		request.setCdCpfCnpj(entrada.getCdCpfCnpj());

		ExcluirContaContratoResponse response = getFactoryAdapter()
				.getExcluirContaContratoPDCAdapter().invokeProcess(request);

		ExcluirContaContratoSaidaDTO saida = new ExcluirContaContratoSaidaDTO();
		saida.setCodMensagem(response.getCodMensagem());
		saida.setMensagem(response.getMensagem());
		return saida;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.service.business.mantercontrato.
	 *      IManterContratoService#substituirRelacionamentoContaContrato
	 *      (br.com.bradesco.web.pgit.service.business.mantercontrato.bean.SubsRelacContaContratoEntradaDTO)
	 */
	public SubsRelacContaContratoSaidaDTO substituirRelacionamentoContaContrato(
			SubsRelacContaContratoEntradaDTO entrada) {
		SubstituirRelacContaContratoRequest request = new SubstituirRelacContaContratoRequest();
		request.setCdFinalidadeContaPagamento(entrada
				.getCdFinalidadeContaPagamento());
		request.setCdPessoaJuridicaContrato(entrada
				.getCdPessoaJuridicaContrato());
		request.setCdPessoaJuridicaRelacionada(entrada
				.getCdPessoaJuridicaRelacionada());
		request.setCdTipoContratoNegocio(entrada.getCdTipoContratoNegocio());
		request.setCdTipoContratoRelacionado(entrada
				.getCdTipoContratoRelacionado());
		request.setCdTipoRelacionamentoConta(entrada
				.getCdTipoRelacionamentoConta());
		request.setCdTipoVinculoContrato(entrada.getCdTipoVinculoContrato());
		request.setNrSequenciaContratoRelacionado(entrada
				.getNrSequenciaContratoRelacionado());
		request.setNrSequencialContratoNegocio(entrada
				.getNrSequencialContratoNegocio());
		request.setCdTipoInscricao(entrada.getCdTipoInscricao());
		request.setCpfCNPJ(entrada.getCpfCNPJ());
		request.setCdPssoa(entrada.getCdPssoa());
		request.setNumeroOcorrencias(1);

        List<br.com.bradesco.web.pgit.service.data.pdc.substituirrelaccontacontrato.request.Ocorrencias> ocorrencias =
            new ArrayList<br.com.bradesco.web.pgit.service.data.pdc.substituirrelaccontacontrato.request.Ocorrencias>();

        br.com.bradesco.web.pgit.service.data.pdc.substituirrelaccontacontrato.request.Ocorrencias ocorrencia =
            new br.com.bradesco.web.pgit.service.data.pdc.substituirrelaccontacontrato.request.Ocorrencias();
		ocorrencia.setCdPessoaJuridicaVinculo(entrada.getOcorrencia()
				.getCdPessoaJuridicaVinculo());
		ocorrencia.setCdTipoContratoVinculo(entrada.getOcorrencia()
				.getCdTipoContratoVinculo());
		ocorrencia.setNrSequenciaContratoVinculo(entrada.getOcorrencia()
				.getNrSequenciaContratoVinculo());
		ocorrencias.add(ocorrencia);
        request.setOcorrencias(ocorrencias
           .toArray(new br.com.bradesco.web.pgit.service.data.pdc.substituirrelaccontacontrato.request.Ocorrencias[0]));

		SubstituirRelacContaContratoResponse response = getFactoryAdapter()
				.getSubstituirRelacContaContratoPDCAdapter().invokeProcess(
						request);

		SubsRelacContaContratoSaidaDTO saida = new SubsRelacContaContratoSaidaDTO();
		saida.setCodMensagem(response.getCodMensagem());
		saida.setMensagem(response.getMensagem());

		return saida;

	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.service.business.mantercontrato.
	 *      IManterContratoService#listarContasRelacionadas
	 *      (br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ListarContasRelContaEntradaDTO)
	 */
	public List<ListarContasRelContaSaidaDTO> listarContasRelacionadas(
			ListarContasRelContaEntradaDTO entrada) {
		ListarContasRelacionadasContaRequest request = new ListarContasRelacionadasContaRequest();
		request.setCdTipoRelacionamentoSelecionado(entrada
				.getCdFinalidadeContaPagamento());
		request.setCdPessoaJuridicaContrato(entrada
				.getCdPessoaJuridicaContrato());
		request
				.setCdPessoaJuridicaVinculo(entrada
						.getCdPessoaJuridicaVinculo());
		request.setCdTipoContratoNegocio(entrada.getCdTipoContratoNegocio());
		request.setCdTipoContratoVinculo(entrada.getCdTipoContratoVinculo());
		request.setCdTipoVinculoContrato(entrada.getCdTipoVinculoContrato());
		request.setNrSequenciaContratoNegocio(entrada
				.getNrSequenciaContratoNegocio());
		request.setNrSequenciaContratoVinculo(entrada
				.getNrSequenciaContratoVinculo());
		request.setQtOcorrencias(30);

		ListarContasRelacionadasContaResponse response = getFactoryAdapter()
				.getListarContasRelacionadasContaPDCAdapter().invokeProcess(
						request);

		List<ListarContasRelContaSaidaDTO> lista = new ArrayList<ListarContasRelContaSaidaDTO>();
		ListarContasRelContaSaidaDTO saida;

		for (int i = 0; i < response.getOcorrenciasCount(); i++) {
			saida = new ListarContasRelContaSaidaDTO();
			saida.setCdAgenciaContabil(response.getOcorrencias(i)
					.getCdAgenciaContabil());
			saida.setCdBanco(response.getOcorrencias(i).getCdBanco());
			saida.setCdConta(response.getOcorrencias(i).getCdConta());
			saida.setCdCpfCnpj(response.getOcorrencias(i).getCdCpfCnpj());
			saida.setCdDigitoConta(response.getOcorrencias(i)
					.getCdDigitoConta());
			saida.setCdPessoa(response.getOcorrencias(i).getCdPessoa());
			saida.setCdPessoaJuridicaRelacionada(response.getOcorrencias(i)
					.getCdPessoaJuridicaRelacionada());
			saida.setCdTipoConta(response.getOcorrencias(i).getCdTipoConta());
			saida.setCdTipoContratoRelacionada(response.getOcorrencias(i)
					.getCdTipoContratoRelacionada());
			saida.setCdTipoRelacionamento(response.getOcorrencias(i)
					.getCdTipoRelacionamento());
			saida.setCodMensagem(response.getCodMensagem());
			saida.setDsAgenciaContabil(response.getOcorrencias(i)
					.getDsAgenciaContabil());
			saida.setDsBanco(response.getOcorrencias(i).getDsBanco());
			saida.setDsPessoa(response.getOcorrencias(i).getDsPessoa());
			saida.setDsTipoConta(response.getOcorrencias(i).getDsTipoConta());
			saida.setDsTipoRelacionamento(response.getOcorrencias(i)
					.getDsTipoRelacionamento());
			saida.setMensagem(response.getMensagem());
			saida.setNrSequenciaContratoRelacionada(response.getOcorrencias(i)
					.getNrSequenciaContratoRelacionada());
			saida.setBancoFormatado(formatBanco(saida.getCdBanco(), saida
					.getDsBanco(), false));
			saida.setAgenciaFormatada(formatAgencia(saida
					.getCdAgenciaContabil(), null,
					saida.getDsAgenciaContabil(), false));
			saida.setContaFormatada(formatConta(saida.getCdConta(), saida
					.getCdDigitoConta(), false));
			lista.add(saida);
		}
		return lista;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.service.business.mantercontrato.]
	 *      IManterContratoService#listarContasVinculadasContrato
	 *      (br.com.bradesco.web.pgit.service.business.mantercontrato.bean.
	 *      ListarContasVincContEntradaDTO)
	 */
	public List<ListarContasVincContSaidaDTO> listarContasVinculadasContrato(
			ListarContasVincContEntradaDTO entrada) {

		ListarContasVinculadasContratoLoopRequest request = new ListarContasVinculadasContratoLoopRequest();
		request.setCdAgencia(verificaIntegerNulo(entrada.getCdAgencia()));
		request.setCdBanco(verificaIntegerNulo(entrada.getCdBanco()));
		request.setCdConta(verificaLongNulo(entrada.getCdConta()));
		request.setCdControleCpfCnpj(verificaIntegerNulo(entrada
				.getCdControleCpfCnpj()));
		request
				.setCdCorpoCfpCnpj(verificaLongNulo(entrada.getCdCorpoCfpCnpj()));
		request.setCdDigitoAgencia(verificaIntegerNulo(entrada
				.getCdDigitoAgencia()));
		request
				.setCdDigitoConta(verificaStringNula(entrada.getCdDigitoConta()));
		request.setCdDigitoCpfCnpj(verificaIntegerNulo(entrada
				.getCdDigitoCpfCnpj()));
		request.setCdFinalidade(verificaIntegerNulo(entrada.getCdFinalidade()));
		request.setCdPessoaJuridicaNegocio(verificaLongNulo(entrada
				.getCdPessoaJuridicaNegocio()));
		request.setCdTipoConta(verificaIntegerNulo(entrada.getCdTipoConta()));
		request.setCdTipoContratoNegocio(verificaIntegerNulo(entrada
				.getCdTipoContratoNegocio()));
		request.setNrSequenciaContratoNegocio(verificaLongNulo(entrada
				.getNrSequenciaContratoNegocio()));
		request.setNrOcorrencias(30);

		// Buffer utilizada para concatena��o de dados para o campo Finalidade
		StringBuffer fieldFinalidade;

		ListarContasVinculadasContratoLoopResponse response = getFactoryAdapter()
				.getListarContasVinculadasContratoLoopPDCAdapter().invokeProcess(
						request);

		List<ListarContasVincContSaidaDTO> lista = new ArrayList<ListarContasVincContSaidaDTO>();

		ListarContasVincContSaidaDTO dto;
		for (int i = 0; i < response.getOcorrenciasCount(); i++) {
			fieldFinalidade = new StringBuffer();

			dto = new ListarContasVincContSaidaDTO();
			dto.setCodMensagem(response.getCodMensagem());
			dto.setMensagem(response.getMensagem());

			dto.setCdDigitoAgencia(response.getOcorrencias(i)
					.getCdDigitoAgencia());
			dto.setCdAgencia(response.getOcorrencias(i).getCdAgencia());
			dto.setCdBanco(response.getOcorrencias(i).getCdBanco());
			dto.setCdConta(response.getOcorrencias(i).getCdConta());
			dto.setCdCpfCnpj(response.getOcorrencias(i).getCdCpfCnpj());
			dto.setCdDigitoConta(response.getOcorrencias(i).getCdDigitoConta());
			dto.setCdPessoaJuridicaVinculo(response.getOcorrencias(i)
					.getCdPessoaJuridicaVinculo());
			dto.setCdTipoConta(response.getOcorrencias(i).getCdTipoConta());
			dto.setDsTipoConta(response.getOcorrencias(i).getDsTipoConta());
			dto.setCdTipoContratoVinculo(response.getOcorrencias(i)
					.getCdTipoContratoVinculo());
			dto.setCdTipoDebito(response.getOcorrencias(i).getCdTipoDebito());
			dto.setCdTipoEstorno(response.getOcorrencias(i).getCdTipoEstorno());
			dto.setCdTipoTarifa(response.getOcorrencias(i).getCdTipoTarifa());
			dto.setCdTipoVinculoContrato(response.getOcorrencias(i)
					.getCdTipoVinculoContrato());
			dto.setDsAgencia(response.getOcorrencias(i).getDsAgencia());
			dto.setDsAlternativa(response.getOcorrencias(i).getDsAlternativa());
			dto.setDsDevolucao(response.getOcorrencias(i).getDsDevolucao());
			dto.setDsBanco(response.getOcorrencias(i).getDsBanco());
			dto.setNomeParticipante(response.getOcorrencias(i)
					.getDsNomeParticipante());
			dto.setNrSequenciaContratoVinculo(response.getOcorrencias(i)
					.getNrSequenciaContratoVinculo());
			dto.setCdParticipante(response.getOcorrencias(i)
					.getCdParticipante());
			dto.setCdSituacao(response.getOcorrencias(i).getCdSituacao());
			dto.setDsSituacao(response.getOcorrencias(i).getDsSituacao());
			dto.setCdSituacaoVinculacaoConta(response.getOcorrencias(i)
					.getCdSituacaoVinculacaoConta());
			dto.setDsSituacaoVinculacaoConta(response.getOcorrencias(i)
					.getDsSituacaoVinculacaoConta());
			dto.setDsDevolucao(response.getOcorrencias(i).getDsDevolucao());
			dto.setDsEstornoDevolucao(response.getOcorrencias(i).getDsEstornoDevolucao());

			// formatando campos
			dto.setDsAgenciaFormatada(PgitUtil.formatAgencia(response
					.getOcorrencias(i).getCdAgencia(), response.getOcorrencias(
					i).getCdDigitoAgencia(), response.getOcorrencias(i)
					.getDsAgencia(), false));
			dto.setDsBancoFormatado(PgitUtil.formatBanco(response
					.getOcorrencias(i).getCdBanco(), response.getOcorrencias(i)
					.getDsBanco(), false));
			dto.setDsContaFormatada(PgitUtil.formatConta(response
					.getOcorrencias(i).getCdConta(), response.getOcorrencias(i)
					.getCdDigitoConta(), false));

			if (dto.getCdTipoDebito().equals("SIM")) {
				fieldFinalidade.append(MessageHelperUtils
						.getI18nMessage("conContasManterContrato_grid_debito"));
			}
			if (dto.getCdTipoTarifa().equals("SIM")) {
				if (fieldFinalidade.length() > 0) {
					fieldFinalidade.append(" - ");
				}
				fieldFinalidade.append(MessageHelperUtils
						.getI18nMessage("conContasManterContrato_grid_tarifa"));
			}
			if (dto.getCdTipoEstorno().equals("SIM")) {
				if (fieldFinalidade.length() > 0) {
					fieldFinalidade.append(" - ");
				}
				fieldFinalidade
						.append(MessageHelperUtils
								.getI18nMessage("conContasManterContrato_grid_estorno"));
			}
			if (dto.getDsAlternativa().equals("SIM")) {
				if (fieldFinalidade.length() > 0) {
					fieldFinalidade.append(" - ");
				}
				fieldFinalidade
						.append(MessageHelperUtils
								.getI18nMessage("conContasManterContrato_grid_alternativa_debito"));
			}
			if (dto.getDsDevolucao().equals("SIM")) {
				if (fieldFinalidade.length() > 0) {
					fieldFinalidade.append(" - ");
				}
				fieldFinalidade
						.append(MessageHelperUtils
								.getI18nMessage("conContasManterContrato_grid_devolu��o"));
			}
			if (dto.getDsEstornoDevolucao().equals("SIM")) {
				if (fieldFinalidade.length() > 0) {
					fieldFinalidade.append(" - ");
				}
				fieldFinalidade
						.append(MessageHelperUtils
								.getI18nMessage("conContasManterContrato_grid_estorno_devolu��o_op"));
			}
			dto.setDsFinalidade(fieldFinalidade.toString());
			// Campo usado em bloquear Contas Contrato
			dto.setContaDebito(dto.getCdBanco() + "/" + dto.getCdAgencia()
					+ "/" + dto.getCdConta() + " - " + dto.getCdDigitoConta());
			lista.add(dto);
		}
		return lista;
	}

	public List<ListarContasRelacionadasParaContratoSaidaDTO> listarContasRelacionadasContrato(
			ListarContasRelacionadasParaContratoEntradaDTO entrada) {
		ListarContasRelacionadasParaContratoRequest request = new ListarContasRelacionadasParaContratoRequest();

		request.setCdAgencia(verificaIntegerNulo(entrada.getCdAgencia()));
		request.setCdBanco(verificaIntegerNulo(entrada.getCdBanco()));
		request.setCdConta(verificaLongNulo(entrada.getCdConta()));
		request.setCdControleCpfCnpj(verificaIntegerNulo(entrada
				.getCdControleCpfCnpj()));
		request
				.setCdCorpoCpfCnpj(verificaLongNulo(entrada.getCdCorpoCfpCnpj()));
		request.setCdDigitoAgencia(verificaIntegerNulo(entrada
				.getCdDigitoAgencia()));
		request
				.setCdDigitoConta(verificaStringNula(entrada.getCdDigitoConta()));
		request.setCdDigitoCpfCnpj(verificaIntegerNulo(entrada
				.getCdDigitoCpfCnpj()));
		request.setCdFinalidade(verificaIntegerNulo(entrada.getCdFinalidade()));
		request.setCdPessoaJuridicaNegocio(verificaLongNulo(entrada
				.getCdPessoaJuridicaNegocio()));
		request.setCdTipoConta(verificaIntegerNulo(entrada.getCdTipoConta()));
		request.setCdTipoContratoNegocio(verificaIntegerNulo(entrada
				.getCdTipoContratoNegocio()));
		request.setNrSequenciaContratoNegocio(verificaLongNulo(entrada
				.getNrSequenciaContratoNegocio()));
		request
				.setNrOcorrencias(verificaIntegerNulo(entrada
						.getNrOcorrencias()));
		request
				.setCdCorpoCpfCnpj(verificaLongNulo(entrada.getCdCorpoCpfCnpj()));
		request.setCodBancoPrincipal(verificaIntegerNulo(entrada
				.getCodBancoPrincipal()));
		request.setCodAgenciaPrincipal(verificaIntegerNulo(entrada
				.getCodAgenciaPrincipal()));
		request.setCodDigAgenciaPrincipal(verificaIntegerNulo(entrada
				.getCodDigAgenciaPrincipal()));
		request.setCodContaPrincipal(verificaLongNulo(entrada
				.getCodContaPrincipal()));
		request.setCodDigitoContaPrincipal(verificaStringNula(entrada
				.getCodDigitoContaPrincipal()));
		request.setNrOcorrencias(30);

		// Buffer utilizada para concatena��o de dados para o campo Finalidade
		StringBuffer fieldFinalidade;

		ListarContasRelacionadasParaContratoResponse response = getFactoryAdapter()
				.getListarContasRelacionadasParaContratoPDCAdapter()
				.invokeProcess(request);

        List<ListarContasRelacionadasParaContratoSaidaDTO> lista =
            new ArrayList<ListarContasRelacionadasParaContratoSaidaDTO>();
		ListarContasRelacionadasParaContratoSaidaDTO dto;
		for (int i = 0; i < response.getOcorrenciasCount(); i++) {
			fieldFinalidade = new StringBuffer();

			dto = new ListarContasRelacionadasParaContratoSaidaDTO();
			dto.setCodMensagem(response.getCodMensagem());
			dto.setMensagem(response.getMensagem());
			dto.setCdDigitoAgencia(response.getOcorrencias(i)
					.getCdDigitoAgencia());
			dto.setCdAgencia(response.getOcorrencias(i).getCdAgencia());
			dto.setCdBanco(response.getOcorrencias(i).getCdBanco());
			dto.setCdConta(response.getOcorrencias(i).getCdConta());
			dto.setCdCpfCnpj(response.getOcorrencias(i).getCdCpfCnpj());
			dto.setCdDigitoConta(response.getOcorrencias(i).getCdDigitoConta());
			dto.setCdPessoaJuridicaVinculo(response.getOcorrencias(i)
					.getCdPessoaJuridicaVinculo());
			dto.setCdTipoConta(response.getOcorrencias(i).getCdTipoConta());
			dto.setDsTipoConta(response.getOcorrencias(i).getDsTipoConta());
			dto.setCdTipoContratoVinculo(response.getOcorrencias(i)
					.getCdTipoContratoVinculo());
			dto.setCdTipoVinculoContrato(response.getOcorrencias(i)
					.getCdTipoVinculoContrato());
			dto.setDsAgencia(response.getOcorrencias(i).getDsAgencia());
			dto.setDsAlternativa(response.getOcorrencias(i).getDsAlternativa());
			dto.setDsDevolucao(response.getOcorrencias(i).getDsDevolucao());
			dto.setDsBanco(response.getOcorrencias(i).getDsBanco());
			dto.setNrSequenciaContratoVinculo(response.getOcorrencias(i)
					.getNrSequenciaContratoVinculo());
			dto.setCdParticipante(response.getOcorrencias(i)
					.getCdParticipante());
			dto.setCdSituacao(response.getOcorrencias(i).getCdSituacao());
			dto.setDsSituacao(response.getOcorrencias(i).getDsSituacao());
			dto.setCdSituacaoVinculacaoConta(response.getOcorrencias(i)
					.getCdSituacaoVinculacaoConta());
			dto.setDsSituacaoVinculacaoConta(response.getOcorrencias(i)
					.getDsSituacaoVinculacaoConta());
			dto.setNmParticipante(response.getOcorrencias(i)
					.getNmParticipante());
			dto.setDsTipoDebito(response.getOcorrencias(i).getDsTipoDebito());
			dto.setDsTipoTarifa(response.getOcorrencias(i).getDsTipoTarifa());
			dto.setDsTipoEstorno(response.getOcorrencias(i).getDsTipoEstorno());
			dto.setCdTipoIncricao(response.getOcorrencias(i).getCdTipoInscricao());

			// formatando campos
			dto.setDsAgenciaFormatada(PgitUtil.formatAgencia(response
					.getOcorrencias(i).getCdAgencia(), response.getOcorrencias(
					i).getCdDigitoAgencia(), response.getOcorrencias(i)
					.getDsAgencia(), false));
			dto.setDsBancoFormatado(PgitUtil.formatBanco(response
					.getOcorrencias(i).getCdBanco(), response.getOcorrencias(i)
					.getDsBanco(), false));
			dto.setDsContaFormatada(PgitUtil.formatConta(response
					.getOcorrencias(i).getCdConta(), response.getOcorrencias(i)
					.getCdDigitoConta(), false));

			if (dto.getDsTipoDebito().equals("SIM")) {
				fieldFinalidade.append(MessageHelperUtils
						.getI18nMessage("conContasManterContrato_grid_debito"));
			}
			if (dto.getDsTipoTarifa().equals("SIM")) {
				if (fieldFinalidade.length() > 0) {
					fieldFinalidade.append(" - ");
				}
				fieldFinalidade.append(MessageHelperUtils
						.getI18nMessage("conContasManterContrato_grid_tarifa"));
			}
			if (dto.getDsTipoEstorno().equals("SIM")) {
				if (fieldFinalidade.length() > 0) {
					fieldFinalidade.append(" - ");
				}
				fieldFinalidade
						.append(MessageHelperUtils
								.getI18nMessage("conContasManterContrato_grid_estorno"));
			}
			if (dto.getDsAlternativa().equals("SIM")) {
				if (fieldFinalidade.length() > 0) {
					fieldFinalidade.append(" - ");
				}
				fieldFinalidade
						.append(MessageHelperUtils
								.getI18nMessage("conContasManterContrato_grid_alternativa_debito"));
			}
			if (dto.getDsDevolucao().equals("SIM")) {
				if (fieldFinalidade.length() > 0) {
					fieldFinalidade.append(" - ");
				}
				fieldFinalidade
						.append(MessageHelperUtils
								.getI18nMessage("conContasManterContrato_grid_devolu��o"));
			}
			dto.setDsFinalidade(fieldFinalidade.toString());
			// Campo usado em bloquear Contas Contrato
			dto.setContaDebito(dto.getCdBanco() + "/" + dto.getCdAgencia()
					+ "/" + dto.getCdConta() + " - " + dto.getCdDigitoConta());
			lista.add(dto);
		}
		return lista;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.service.business.mantercontrato.
	 *      IManterContratoService#listarContasVinculadasContratoCinquenta
	 *      (br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ListarContasVincContEntradaDTO)
	 */
	public List<ListarContasVincContSaidaDTO> listarContasVinculadasContratoCinquenta(
			ListarContasVincContEntradaDTO entrada) {
		ListarContasVinculadasContratoRequest request = new ListarContasVinculadasContratoRequest();
		request.setCdAgencia(entrada.getCdAgencia());
		request.setCdBanco(entrada.getCdBanco());
		request.setCdConta(entrada.getCdConta());
		request.setCdControleCpfCnpj(entrada.getCdControleCpfCnpj());
		request.setCdCorpoCfpCnpj(entrada.getCdCorpoCfpCnpj());
		request.setCdDigitoAgencia(entrada.getCdDigitoAgencia());
		request.setCdDigitoConta(entrada.getCdDigitoConta());
		request.setCdDigitoCpfCnpj(entrada.getCdDigitoCpfCnpj());
		request.setCdFinalidade(entrada.getCdFinalidade());
		request
				.setCdPessoaJuridicaNegocio(entrada
						.getCdPessoaJuridicaNegocio());
		request.setCdTipoConta(entrada.getCdTipoConta());
		request.setCdTipoContratoNegocio(entrada.getCdTipoContratoNegocio());
		request.setNrSequenciaContratoNegocio(entrada
				.getNrSequenciaContratoNegocio());
		request.setNrOcorrencias(50);

		// Buffer utilizada para concatena��o de dados para o campo Finalidade
		StringBuffer fieldFinalidade;

		ListarContasVinculadasContratoResponse response = getFactoryAdapter()
				.getListarContasVinculadasContratoPDCAdapter().invokeProcess(
						request);

		List<ListarContasVincContSaidaDTO> lista = new ArrayList<ListarContasVincContSaidaDTO>();
		ListarContasVincContSaidaDTO dto;
		for (int i = 0; i < response.getOcorrenciasCount(); i++) {
			fieldFinalidade = new StringBuffer("");

			dto = new ListarContasVincContSaidaDTO();
			dto.setCodMensagem(response.getCodMensagem());
			dto.setMensagem(response.getMensagem());
			dto.setCdDigitoAgencia(response.getOcorrencias(i)
					.getCdDigitoAgencia());
			dto.setCdAgencia(response.getOcorrencias(i).getCdAgencia());
			dto.setCdBanco(response.getOcorrencias(i).getCdBanco());
			dto.setCdConta(response.getOcorrencias(i).getCdConta());
			dto.setCdCpfCnpj(response.getOcorrencias(i).getCdCpfCnpj());
			dto.setCdDigitoConta(response.getOcorrencias(i).getCdDigitoConta());
			dto.setCdPessoaJuridicaVinculo(response.getOcorrencias(i)
					.getCdPessoaJuridicaVinculo());
			dto.setCdTipoConta(response.getOcorrencias(i).getCdTipoConta());
			dto.setDsTipoConta(response.getOcorrencias(i).getDsTipoConta());
			dto.setCdTipoContratoVinculo(response.getOcorrencias(i)
					.getCdTipoContratoVinculo());
			dto.setCdTipoDebito(response.getOcorrencias(i).getCdTipoDebito());
			dto.setCdTipoEstorno(response.getOcorrencias(i).getCdTipoEstorno());
			dto.setCdTipoTarifa(response.getOcorrencias(i).getCdTipoTarifa());
			dto.setCdTipoVinculoContrato(response.getOcorrencias(i)
					.getCdTipoVinculoContrato());
			dto.setDsAgencia(response.getOcorrencias(i).getDsAgencia());
			dto.setDsAlternativa(response.getOcorrencias(i).getDsAlternativa());
			dto.setDsBanco(response.getOcorrencias(i).getDsBanco());
			dto.setNomeParticipante(response.getOcorrencias(i)
					.getDsNomeParticipante());
			dto.setNrSequenciaContratoVinculo(response.getOcorrencias(i)
					.getNrSequenciaContratoVinculo());
			dto.setCdParticipante(response.getOcorrencias(i)
					.getCdParticipante());
			dto.setDsDevolucao(response.getOcorrencias(i).getDsDevolucao());
			dto.setDsEstornoDevolucao(response.getOcorrencias(i).getDsEstornoDevolucao());

			// formatando campos
			dto.setDsAgenciaFormatada(formatAgencia(response.getOcorrencias(i)
					.getCdAgencia(), response.getOcorrencias(i)
					.getCdDigitoAgencia(), response.getOcorrencias(i)
					.getDsAgencia(), false));
			dto.setDsBancoFormatado(formatBanco(response.getOcorrencias(i)
					.getCdBanco(), response.getOcorrencias(i).getDsBanco(),
					false));
			dto.setDsContaFormatada(formatConta(response.getOcorrencias(i)
					.getCdConta(), response.getOcorrencias(i)
					.getCdDigitoConta(), false));

			if (dto.getCdTipoDebito().equals("SIM")) {
				fieldFinalidade.append(MessageHelperUtils
						.getI18nMessage("conContasManterContrato_grid_debito"));
			}
			if (dto.getCdTipoTarifa().equals("SIM")) {
				if (!fieldFinalidade.toString().equals("")) {
					fieldFinalidade.append(" - ");
				}
				fieldFinalidade.append(MessageHelperUtils
						.getI18nMessage("conContasManterContrato_grid_tarifa"));
			}
			if (dto.getCdTipoEstorno().equals("SIM")) {
				if (!fieldFinalidade.toString().equals("")) {
					fieldFinalidade.append(" - ");
				}
				fieldFinalidade
						.append(MessageHelperUtils
								.getI18nMessage("conContasManterContrato_grid_estorno_devolu��o"));
			}
			if (dto.getDsAlternativa().equals("SIM")) {
				if (!fieldFinalidade.toString().equals("")) {
					fieldFinalidade.append(" - ");
				}
				fieldFinalidade
						.append(MessageHelperUtils
								.getI18nMessage("conContasManterContrato_grid_alternativa_debito"));
			}
			
			if (dto.getDsEstornoDevolucao().equals("SIM")) {
				if (!fieldFinalidade.toString().equals("")) {
					fieldFinalidade.append(" - ");
				}
				fieldFinalidade
						.append(MessageHelperUtils
								.getI18nMessage("conContasManterContrato_grid_estorno_devolu��o_op"));
			}
			
			dto.setDsFinalidade(fieldFinalidade.toString());
			// Campo usado em bloquear Contas Contrato
			dto.setContaDebito(dto.getCdBanco() + "/" + dto.getCdAgencia()
					+ "/" + dto.getCdConta() + " - " + dto.getCdDigitoConta());
			lista.add(dto);
		}
		return lista;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.service.business.mantercontrato.
	 *      IManterContratoService#listarContasPossiveisInclusao
	 *      (br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ListarContasPossiveisInclusaoEntradaDTO)
	 */
	public List<ListarContasPossiveisInclusaoSaidaDTO> listarContasPossiveisInclusao(
			ListarContasPossiveisInclusaoEntradaDTO entrada) {
		ListarContasPossiveisInclusaoRequest request = new ListarContasPossiveisInclusaoRequest();

		request.setCdPessoaJuridicaContrato(verificaLongNulo(entrada
				.getCdPessoaJuridicaContrato()));
		request.setCdTipoContratoNegocio(verificaIntegerNulo(entrada
				.getCdTipoContratoNegocio()));
		request.setNrSequenciaContratoNegocio(verificaLongNulo(entrada
				.getNrSequenciaContratoNegocio()));
		request
				.setCdCorpoCpfCnpj(verificaLongNulo(entrada.getCdCorpoCpfCnpj()));
		request.setCdFilialCpfCnpj(verificaIntegerNulo(entrada
				.getCdFilialCpfCnpj()));
		request.setCdControleCpfCnpj(verificaIntegerNulo(entrada
				.getCdDigitoCpfCnpj()));
		request.setCdBanco(verificaIntegerNulo(entrada.getCdBanco()));
		request.setCdAgencia(verificaIntegerNulo(entrada.getCdAgencia()));
		request.setCdDigitoAgencia(verificaIntegerNulo(entrada
				.getCdDigitoAgencia()));
		request.setCdConta(verificaLongNulo(entrada.getCdConta()));
		request
				.setCdDigitoConta(verificaIntegerNulo(entrada
						.getCdDigitoConta()));
		request.setDsTipoConta(verificaIntegerNulo(entrada.getDsTipoConta()));
		request.setNumeroOcorrencias(30);

		ListarContasPossiveisInclusaoResponse response = getFactoryAdapter()
				.getListarContasPossiveisInclusaoPDCAdapter().invokeProcess(
						request);

		List<ListarContasPossiveisInclusaoSaidaDTO> lista = new ArrayList<ListarContasPossiveisInclusaoSaidaDTO>();
		ListarContasPossiveisInclusaoSaidaDTO dto;
		for (int i = 0; i < response.getOcorrenciasCount(); i++) {
			dto = new ListarContasPossiveisInclusaoSaidaDTO();
			dto.setCodMensagem(response.getCodMensagem());
			dto.setMensagem(response.getMensagem());
			dto.setCdPessoaJuridicaVinculo(response.getOcorrencias(i)
					.getCdPessoVinc());
			dto.setCdTipoContratoVinculo(response.getOcorrencias(i)
					.getCdTipoContratoVinc());
			dto.setNrSequenciaContratoVinculo(response.getOcorrencias(i)
					.getNrSeqContratoVinc());
			dto.setCdCpfCnpj(response.getOcorrencias(i).getCdCorpoCpfCnpj());
			dto.setNmParticipante(response.getOcorrencias(i)
					.getNmParticipante());
			dto.setCdBanco(response.getOcorrencias(i).getCdBanco());
			dto.setDsBanco(response.getOcorrencias(i).getDsBanco());
			dto.setCdAgencia(response.getOcorrencias(i).getCdAgencia());
			dto.setDsAgencia(response.getOcorrencias(i).getDsAgencia());
			dto.setCdConta(response.getOcorrencias(i).getCdConta());

			dto.setCdDigitoAgencia(response.getOcorrencias(i)
					.getCdDigitoAgencia());
			dto.setCdDigitoConta(response.getOcorrencias(i).getCdDigitoConta());
			dto.setCdTipoConta(response.getOcorrencias(i).getCdTipoConta());
			dto.setDsTipoConta(response.getOcorrencias(i).getDsTipoConta());

			dto.setBancoFormatado(PgitUtil.formatBanco(response.getOcorrencias(
					i).getCdBanco(), response.getOcorrencias(i).getDsBanco(),
					false));
			dto.setAgenciaFormatada(PgitUtil.formatAgencia(response
					.getOcorrencias(i).getCdAgencia(), response.getOcorrencias(
					i).getCdDigitoAgencia(), response.getOcorrencias(i)
					.getDsAgencia(), false));
			dto.setContaFormatada(PgitUtil.formatConta(response.getOcorrencias(
					i).getCdConta(), response.getOcorrencias(i)
					.getCdDigitoConta(), false));
			dto.setCdPessoaVinc(response.getOcorrencias(i).getCdPessoVinc());
			
			dto.setCheck(false);
			
			lista.add(dto);
		}
		return lista;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.service.business.mantercontrato.
	 *      IManterContratoService#listarParticipantes
	 *      (br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ListarParticipantesEntradaDTO)
	 */
	public List<ListarParticipantesSaidaDTO> listarParticipantes(
			ListarParticipantesEntradaDTO entrada) {
		ListarParticipantesLoopRequest request = new ListarParticipantesLoopRequest();

		request.setCdPessoaJuridica(entrada.getCodPessoaJuridica());
		request.setCdTipoContrato(entrada.getCodTipoContrato());
		request.setNrOcorrencias(entrada.getMaxOcorrencias());
		request.setNrSequenciaContrato(entrada.getNrSequenciaContrato());

		ListarParticipantesLoopResponse response = getFactoryAdapter()
				.getListarParticipantesLoopPDCAdapter().invokeProcess(request);

		List<ListarParticipantesSaidaDTO> listaRetorno = new ArrayList<ListarParticipantesSaidaDTO>();
		ListarParticipantesSaidaDTO saidaDTO;
		for (int i = 0; i < response.getOcorrenciasCount(); i++) {
			saidaDTO = new ListarParticipantesSaidaDTO();
			saidaDTO.setCodMensagem(response.getCodMensagem());
			saidaDTO.setMensagem(response.getMensagem());

			saidaDTO.setCdPessoa(response.getOcorrencias(i).getCdPessoa());
			saidaDTO.setCdCpfCnpj(response.getOcorrencias(i).getCdCpfCnpj());
			saidaDTO.setCdFilialCnpj(response.getOcorrencias(i)
					.getCdFilialCnpj());
			saidaDTO.setCdControleCnpj(response.getOcorrencias(i)
					.getCdControleCnpj());
			saidaDTO.setNomeRazao(response.getOcorrencias(i).getNmRazao());
			saidaDTO.setCdGrupoEconomico(response.getOcorrencias(i)
					.getCdGrupoEconomico());
			saidaDTO.setDsGrupoEconomico(response.getOcorrencias(i)
					.getDsGrupoEconomico());
			saidaDTO.setCdAtividadeEconomica(response.getOcorrencias(i)
					.getCdAtividadeEconomica());
			saidaDTO.setDsAtividadeEconomica(response.getOcorrencias(i)
					.getDsAtividadeEconomica());
			saidaDTO.setCdSegmentoEconomico(response.getOcorrencias(i)
					.getCdSegmentoEconomico());
			saidaDTO.setDsSegmentoEconomico(response.getOcorrencias(i)
					.getDsSegmentoEconomico());
			saidaDTO.setCdSubSegmento(response.getOcorrencias(i)
					.getCdSubSegmento());
			saidaDTO.setDsSubSegmento(response.getOcorrencias(i)
					.getDsSubSegmento());
			saidaDTO.setCdTipoParticipacao(response.getOcorrencias(i)
					.getCdTipoParticipacao());
			saidaDTO.setDsTipoParticipacao(response.getOcorrencias(i)
					.getDsTipoParticipacao());
			saidaDTO.setCdSituacaoParticipacao(response.getOcorrencias(i)
					.getCdSituacaoParticipacao());
			saidaDTO.setDsSituacaoParticipacao(response.getOcorrencias(i)
					.getDsSituacaoParticipacao());
			saidaDTO.setCdUsuarioInclusao(response.getOcorrencias(i)
					.getCdUsuarioInclusao());
			saidaDTO.setCdUsuarioExternoInclusao(response.getOcorrencias(i)
					.getCdUsuarioExternoInclusao());
			saidaDTO.setHoraManutencaoRegInclusao(formatarDataTrilha(response
					.getOcorrencias(i).getHrManutencaoRegistroInclusao()));
			saidaDTO.setCdCanalInclusao(response.getOcorrencias(i)
					.getCdCanalInclusao());
			saidaDTO.setDsCanalInclusao(response.getOcorrencias(i)
					.getDsCanalInclusao());
			saidaDTO.setNomeOperacaoFluxoInclusao(response.getOcorrencias(i)
					.getNmOperacaoFluxoInclusao());
			saidaDTO.setCdUsuarioAlteracao(response.getOcorrencias(i)
					.getCdUsuarioAlteracao());
			saidaDTO.setCdUsuarioExternoAlteracao(response.getOcorrencias(i)
					.getCdUsuarioExternoAlteracao());
			saidaDTO.setHoraManutencaoResAlteracao(formatarDataTrilha(response
					.getOcorrencias(i).getHrManutencaoRegistroAlteracao()));
			saidaDTO.setCdCanalAlteracao(response.getOcorrencias(i)
					.getCdCanalAlteracao());
			saidaDTO.setDsCanalAlteracao(response.getOcorrencias(i)
					.getDsCanalAlteracao());
			saidaDTO.setNomeOperacaoFluxoAlteracao(response.getOcorrencias(i)
					.getNmOperacaoFluxoAlteracao());
			saidaDTO.setCnpjOuCpfFormatado(formatCpfCnpjCompleto(saidaDTO
					.getCdCpfCnpj(), saidaDTO.getCdFilialCnpj(), saidaDTO
					.getCdControleCnpj()));
			saidaDTO.setTipoParticipacaoFormatador(saidaDTO
					.getCdTipoParticipacao()
					+ " - " + saidaDTO.getDsTipoParticipacao());
			saidaDTO.setHrManutencaoRegistroInclusao(response.getOcorrencias(i)
					.getHrManutencaoRegistroInclusao());
			saidaDTO.setNmOperacaoFluxoInclusao(response.getOcorrencias(i)
					.getNmOperacaoFluxoInclusao());
			saidaDTO.setHrManutencaoRegistroAlteracao(response
					.getOcorrencias(i).getHrManutencaoRegistroAlteracao());
			saidaDTO.setNmOperacaoFluxoAlteracao(response.getOcorrencias(i)
					.getNmOperacaoFluxoAlteracao());
			saidaDTO.setCdMotivoParticipacao(response.getOcorrencias(i)
					.getCdMotivoParticipacao());
			saidaDTO.setDsMotivoParticipacao(response.getOcorrencias(i)
					.getDsMotivoParticipacao());
			saidaDTO.setCdClasificacaoParticipante(response.getOcorrencias(i)
					.getCdClasificacaoParticipante());
			saidaDTO.setDsClasificacaoParticipante(response.getOcorrencias(i)
					.getDsClasificacaoParticipante());
			saidaDTO.setNmRazao(response.getOcorrencias(i).getNmRazao());
			listaRetorno.add(saidaDTO);
		}
		return listaRetorno;
	}
	
	@Override
	public List<ListarParticipantesSaidaDTO> listarParticipantesSemLoop(
			ListarParticipantesEntradaDTO entrada) {
		ListarParticipantesRequest request = new ListarParticipantesRequest();

		request.setCdPessoaJuridica(entrada.getCodPessoaJuridica());
		request.setCdTipoContrato(entrada.getCodTipoContrato());
		request.setNrOcorrencias(entrada.getMaxOcorrencias());
		request.setNrSequenciaContrato(entrada.getNrSequenciaContrato());

		ListarParticipantesResponse response = getFactoryAdapter()
				.getListarParticipantesPDCAdapter().invokeProcess(request);

		List<ListarParticipantesSaidaDTO> listaRetorno = new ArrayList<ListarParticipantesSaidaDTO>();
		ListarParticipantesSaidaDTO saidaDTO;
		for (int i = 0; i < response.getOcorrenciasCount(); i++) {
			saidaDTO = new ListarParticipantesSaidaDTO();
			saidaDTO.setCodMensagem(response.getCodMensagem());
			saidaDTO.setMensagem(response.getMensagem());

			saidaDTO.setCdPessoa(response.getOcorrencias(i).getCdPessoa());
			saidaDTO.setCdCpfCnpj(response.getOcorrencias(i).getCdCpfCnpj());
			saidaDTO.setCdFilialCnpj(response.getOcorrencias(i)
					.getCdFilialCnpj());
			saidaDTO.setCdControleCnpj(response.getOcorrencias(i)
					.getCdControleCnpj());
			saidaDTO.setNomeRazao(response.getOcorrencias(i).getNmRazao());
			saidaDTO.setCdGrupoEconomico(response.getOcorrencias(i)
					.getCdGrupoEconomico());
			saidaDTO.setDsGrupoEconomico(response.getOcorrencias(i)
					.getDsGrupoEconomico());
			saidaDTO.setCdAtividadeEconomica(response.getOcorrencias(i)
					.getCdAtividadeEconomica());
			saidaDTO.setDsAtividadeEconomica(response.getOcorrencias(i)
					.getDsAtividadeEconomica());
			saidaDTO.setCdSegmentoEconomico(response.getOcorrencias(i)
					.getCdSegmentoEconomico());
			saidaDTO.setDsSegmentoEconomico(response.getOcorrencias(i)
					.getDsSegmentoEconomico());
			saidaDTO.setCdSubSegmento(response.getOcorrencias(i)
					.getCdSubSegmento());
			saidaDTO.setDsSubSegmento(response.getOcorrencias(i)
					.getDsSubSegmento());
			saidaDTO.setCdTipoParticipacao(response.getOcorrencias(i)
					.getCdTipoParticipacao());
			saidaDTO.setDsTipoParticipacao(response.getOcorrencias(i)
					.getDsTipoParticipacao());
			saidaDTO.setCdSituacaoParticipacao(response.getOcorrencias(i)
					.getCdSituacaoParticipacao());
			saidaDTO.setDsSituacaoParticipacao(response.getOcorrencias(i)
					.getDsSituacaoParticipacao());
			saidaDTO.setCdUsuarioInclusao(response.getOcorrencias(i)
					.getCdUsuarioInclusao());
			saidaDTO.setCdUsuarioExternoInclusao(response.getOcorrencias(i)
					.getCdUsuarioExternoInclusao());
			saidaDTO.setHoraManutencaoRegInclusao(formatarDataTrilha(response
					.getOcorrencias(i).getHrManutencaoRegistroInclusao()));
			saidaDTO.setCdCanalInclusao(response.getOcorrencias(i)
					.getCdCanalInclusao());
			saidaDTO.setDsCanalInclusao(response.getOcorrencias(i)
					.getDsCanalInclusao());
			saidaDTO.setNomeOperacaoFluxoInclusao(response.getOcorrencias(i)
					.getNmOperacaoFluxoInclusao());
			saidaDTO.setCdUsuarioAlteracao(response.getOcorrencias(i)
					.getCdUsuarioAlteracao());
			saidaDTO.setCdUsuarioExternoAlteracao(response.getOcorrencias(i)
					.getCdUsuarioExternoAlteracao());
			saidaDTO.setHoraManutencaoResAlteracao(formatarDataTrilha(response
					.getOcorrencias(i).getHrManutencaoRegistroAlteracao()));
			saidaDTO.setCdCanalAlteracao(response.getOcorrencias(i)
					.getCdCanalAlteracao());
			saidaDTO.setDsCanalAlteracao(response.getOcorrencias(i)
					.getDsCanalAlteracao());
			saidaDTO.setNomeOperacaoFluxoAlteracao(response.getOcorrencias(i)
					.getNmOperacaoFluxoAlteracao());
			saidaDTO.setCnpjOuCpfFormatado(formatCpfCnpjCompleto(saidaDTO
					.getCdCpfCnpj(), saidaDTO.getCdFilialCnpj(), saidaDTO
					.getCdControleCnpj()));
			saidaDTO.setTipoParticipacaoFormatador(saidaDTO
					.getCdTipoParticipacao()
					+ " - " + saidaDTO.getDsTipoParticipacao());
			saidaDTO.setHrManutencaoRegistroInclusao(response.getOcorrencias(i)
					.getHrManutencaoRegistroInclusao());
			saidaDTO.setNmOperacaoFluxoInclusao(response.getOcorrencias(i)
					.getNmOperacaoFluxoInclusao());
			saidaDTO.setHrManutencaoRegistroAlteracao(response
					.getOcorrencias(i).getHrManutencaoRegistroAlteracao());
			saidaDTO.setNmOperacaoFluxoAlteracao(response.getOcorrencias(i)
					.getNmOperacaoFluxoAlteracao());
			saidaDTO.setCdMotivoParticipacao(response.getOcorrencias(i)
					.getCdMotivoParticipacao());
			saidaDTO.setDsMotivoParticipacao(response.getOcorrencias(i)
					.getDsMotivoParticipacao());
			saidaDTO.setCdClasificacaoParticipante(response.getOcorrencias(i)
					.getCdClasificacaoParticipante());
			saidaDTO.setDsClasificacaoParticipante(response.getOcorrencias(i)
					.getDsClasificacaoParticipante());
			saidaDTO.setNmRazao(response.getOcorrencias(i).getNmRazao());
			listaRetorno.add(saidaDTO);
		}
		return listaRetorno;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.service.business.mantercontrato.
	 *      IManterContratoService#incluirParticipanteContratoPgit(java.util.List)
	 */
	public IncluirParticipanteContratoPgitSaidaDTO incluirParticipanteContratoPgit(
			IncluirParticipanteContratoPgitEntradaDTO entradaDTO, List<ListarContasPossiveisInclusaoSaidaDTO> listaContasParaInclusao) {
	    
	    
		IncluirParticipanteContratoPgitRequest request = new IncluirParticipanteContratoPgitRequest();
		IncluirParticipanteContratoPgitResponse response = new IncluirParticipanteContratoPgitResponse(); 

		request.setCdPessoaJuridicaContrato(verificaLongNulo(entradaDTO.getCdPessoaJuridica()));
		request.setCdTipoContratoNegocio(verificaIntegerNulo(entradaDTO.getCodTipoContrato()));
		request.setNrSequencialContratoNegocio(verificaLongNulo(entradaDTO.getNumSequenciaContrato()));	
		request.setCdPessoa(verificaLongNulo(entradaDTO.getCodParticipante()));
        
        int inicio = 0;
        int fim = 0;
        
        do {
            List<br.com.bradesco.web.pgit.service.data.pdc.incluirparticipantecontratopgit.request.Ocorrencias> ocorrencias =
                new ArrayList<br.com.bradesco.web.pgit.service.data.pdc.incluirparticipantecontratopgit.request.Ocorrencias>();
            
            if(!listaContasParaInclusao.isEmpty()){                
                
                fim = ((fim + 50) > listaContasParaInclusao.size()) ? listaContasParaInclusao.size() : (fim += 50);
                
                List<ListarContasPossiveisInclusaoSaidaDTO> subList = 
                    new ArrayList<ListarContasPossiveisInclusaoSaidaDTO>(listaContasParaInclusao.subList(inicio, fim));
                
                for (int i = 0; i < subList.size(); i++)
                {
                    br.com.bradesco.web.pgit.service.data.pdc.incluirparticipantecontratopgit.request.Ocorrencias ocorrencia =
                        new br.com.bradesco.web.pgit.service.data.pdc.incluirparticipantecontratopgit.request.Ocorrencias();
        
                    ocorrencia.setCdPessoaJuridicaVinculo(verificaLongNulo(subList.get(i).getCdPessoaJuridicaVinculo()));
        			ocorrencia.setCdTipoContratoVinculo(verificaIntegerNulo(subList.get(i).getCdTipoContratoVinculo()));
        			ocorrencia.setNrSequencialContratoVinculo(verificaLongNulo(subList.get(i).getNrSequenciaContratoVinculo()));
        
        			ocorrencias.add(ocorrencia);
                }
                
                inicio += 50;
                
                request.setQtdeContaVinculadaParticipante(verificaIntegerNulo(subList.size()));
              
                request.setOcorrencias(ocorrencias
                    .toArray(new br.com.bradesco.web.pgit.service.data.pdc.incluirparticipantecontratopgit.request.Ocorrencias[0]));
            }else{
                request.setQtdeContaVinculadaParticipante(0);
                br.com.bradesco.web.pgit.service.data.pdc.incluirparticipantecontratopgit.request.Ocorrencias ocor = 
                    new br.com.bradesco.web.pgit.service.data.pdc.incluirparticipantecontratopgit.request.Ocorrencias();
                
                ocor.setCdPessoaJuridicaVinculo(0L);
                ocor.setCdTipoContratoVinculo(0);
                ocor.setNrSequencialContratoVinculo(0L);
                                
                request.addOcorrencias(ocor);
            }
            
            response = getFactoryAdapter()
            .getIncluirParticipanteContratoPgitPDCAdapter().invokeProcess(
                request);
		} while(fim < listaContasParaInclusao.size());
		

		IncluirParticipanteContratoPgitSaidaDTO saidaDTO = new IncluirParticipanteContratoPgitSaidaDTO();
		saidaDTO.setCodMensagem(response.getCodMensagem());
		saidaDTO.setMensagem(response.getMensagem());
		return saidaDTO;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.service.business.mantercontrato.
	 *      IManterContratoService#excluirParticipanteContratoPgit
	 *      (br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ExcluirParticipanteContratoPgitEntradaDTO)
	 */
	public ExcluirParticipanteContratoPgitSaidaDTO excluirParticipanteContratoPgit(
			ExcluirParticipanteContratoPgitEntradaDTO entradaDTO) {
		ExcluirParticipanteContratoPgitRequest request = new ExcluirParticipanteContratoPgitRequest();

		request.setCdPessoaJuridica(entradaDTO.getCodEmpresaContrato());
		request.setCdTipoContrato(entradaDTO.getTipoContrato());
		request.setNrSequenciaContrato(entradaDTO.getNumSequenciaContrato());
		request.setCdTipoParticipacao(entradaDTO.getCodTipoParticipacao());
		request.setCdPessoaParticipacao(entradaDTO.getCodParticipante());
		request.setCdCpfCnpj(entradaDTO.getCpfCnpjParticipante());
		request.setCdFilialCnpj(entradaDTO.getCodFilialCnpj());
		request.setCdControleCpfCnpj(entradaDTO.getCodControleCpfCnpj());

		ExcluirParticipanteContratoPgitResponse response = getFactoryAdapter()
				.getExcluirParticipanteContratoPgitPDCAdapter().invokeProcess(
						request);

		ExcluirParticipanteContratoPgitSaidaDTO saidaDTO = new ExcluirParticipanteContratoPgitSaidaDTO();
		saidaDTO.setCodMensagem(response.getCodMensagem());
		saidaDTO.setMensagem(response.getMensagem());
		return saidaDTO;
	}

	// ------------ LAYOUTS -----------------------------------------
	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.service.business.mantercontrato.
	 *      IManterContratoService#listarTipoLayoutContrato(
	 *      br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ListarTipoLayoutContratoEntradaDTO)
	 */
	public List<ListarTipoLayoutContratoSaidaDTO> listarTipoLayoutContrato(
			ListarTipoLayoutContratoEntradaDTO entrada) {
		ListarTipoLayoutContratoRequest request = new ListarTipoLayoutContratoRequest();

		request.setCdPessoaJuridicaContrato(entrada
				.getCdPessoaJuridicaContrato());
		request.setCdTipoContrato(entrada.getCdTipoContrato());
		request.setCdTipoLayout(entrada.getCdTipoLayout());
		request.setNrSequenciaContrato(entrada.getNrSequenciaContrato());
		request.setNumeroOcorrencias(30);

		ListarTipoLayoutContratoResponse response = getFactoryAdapter()
				.getListarTipoLayoutContratoPDCAdapter().invokeProcess(request);

		List<ListarTipoLayoutContratoSaidaDTO> lista = new ArrayList<ListarTipoLayoutContratoSaidaDTO>();
		ListarTipoLayoutContratoSaidaDTO registro;

		for (int i = 0; i < response.getNumeroLinhas(); i++) {
			registro = new ListarTipoLayoutContratoSaidaDTO();

			registro.setCdRelacionamentoProdutoProduto(response.getOcorrencias(
					i).getCdRelacionamentoProdutoProduto());
			registro.setCdTipoLayout(response.getOcorrencias(i)
					.getCdTipoLayout());
			registro.setCdTipoServico(response.getOcorrencias(i)
					.getCdTipoServico());
			registro.setCodMensagem(response.getCodMensagem());
			registro.setDsTipoLayout(response.getOcorrencias(i)
					.getDsTipoLayout());
			registro.setDsTipoServico(response.getOcorrencias(i)
					.getDsTipoServico());
			registro.setMensagem(response.getMensagem());
			lista.add(registro);
		}
		return lista;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.service.business.mantercontrato.
	 *      IManterContratoService#alterarLayoutServico
	 *      (br.com.bradesco.web.pgit.service.business.mantercontrato.bean.AlterarLayoutServicoEntradaDTO)
	 */
	public AlterarLayoutServicoSaidaDTO alterarLayoutServico(
			AlterarLayoutServicoEntradaDTO entrada) {
		AlterarLayoutServicoRequest request = new AlterarLayoutServicoRequest();
		request.setCdPessoaJuridica(entrada.getCdPessoaJuridica());
		request.setCdTipoContrato(entrada.getCdTipoContrato());
		request.setNrSequenciaContrato(entrada.getNrSequenciaContrato());

        List<br.com.bradesco.web.pgit.service.data.pdc.alterarlayoutservico.request.Ocorrencias> lista =
            new ArrayList<br.com.bradesco.web.pgit.service.data.pdc.alterarlayoutservico.request.Ocorrencias>();
		br.com.bradesco.web.pgit.service.data.pdc.alterarlayoutservico.request.Ocorrencias ocorrencia;

		for (int i = 0; i < entrada.getListaLayoutServicos().size(); i++) {
			ocorrencia = new br.com.bradesco.web.pgit.service.data.pdc.alterarlayoutservico.request.Ocorrencias();

			ocorrencia.setCdRelacionamentoProdutoProduto(entrada
					.getListaLayoutServicos().get(i)
					.getCdRelacionamentoProdutoProduto());
			ocorrencia.setCdTipoLayout(entrada.getListaLayoutServicos().get(i)
					.getCdTipoLayout());
			ocorrencia.setCdTipoLayoutNovo(entrada.getListaLayoutServicos()
					.get(i).getCdTipoLayoutNovo());
			ocorrencia.setCdTipoServico(entrada.getListaLayoutServicos().get(i)
					.getCdTipoServico());
			lista.add(ocorrencia);
		}

        request.setOcorrencias(lista
            .toArray(new br.com.bradesco.web.pgit.service.data.pdc.alterarlayoutservico.request.Ocorrencias[0]));
		AlterarLayoutServicoResponse response = getFactoryAdapter()
				.getAlterarLayoutServicoPDCAdapter().invokeProcess(request);

		AlterarLayoutServicoSaidaDTO saida = new AlterarLayoutServicoSaidaDTO();
		saida.setCodMensagem(response.getCodMensagem());
		saida.setMensagem(response.getMensagem());
		return saida;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.service.business.mantercontrato.
	 *      IManterContratoService#consultarListaLayoutArquivoContrato(
	 *      br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ConsultarListaLayoutArquivoContratoEntradaDTO)
	 */
	public List<ConsultarListaLayoutArquivoContratoSaidaDTO> consultarListaLayoutArquivoContrato(
			ConsultarListaLayoutArquivoContratoEntradaDTO entradaDTO) {

		ConsultarListaLayoutArquivoContratoRequest request = new ConsultarListaLayoutArquivoContratoRequest();
		request.setNumeroOcorrencias(30);
		request.setCdPessoaJuridicaContrato(entradaDTO
				.getCdPessoaJuridicaContrato());
		request.setCdTipoContratoNegocio(entradaDTO.getCdTipoContratoNegocio());
		request.setNrSequenciaContratoNegocio(entradaDTO
				.getNrSequenciaContratoNegocio());
		request.setCdProduto(entradaDTO.getCdProduto());

		ConsultarListaLayoutArquivoContratoResponse response = getFactoryAdapter()
				.getConsultarListaLayoutArquivoContratoPDCAdapter()
				.invokeProcess(request);

        List<ConsultarListaLayoutArquivoContratoSaidaDTO> listaRetorno =
            new ArrayList<ConsultarListaLayoutArquivoContratoSaidaDTO>();
		ConsultarListaLayoutArquivoContratoSaidaDTO saidaDTO;
		for (int i = 0; i < response.getOcorrenciasCount(); i++) {
			saidaDTO = new ConsultarListaLayoutArquivoContratoSaidaDTO();
			saidaDTO.setCodMensagem(response.getCodMensagem());
			saidaDTO.setMensagem(response.getMensagem());
			saidaDTO.setCdProduto(response.getOcorrencias(i).getCdProduto());
			saidaDTO.setDsProduto(response.getOcorrencias(i).getDsProduto());
			saidaDTO.setCdTipoLayout(response.getOcorrencias(i)
					.getCdTipoLayout());
			saidaDTO.setDsTipoLayout(response.getOcorrencias(i)
					.getDsTipoLayout());
			saidaDTO.setCdSituacaoLayoutContrato(response.getOcorrencias(i)
					.getCdSituacaoLayoutContrato());
			saidaDTO.setDsSituacaoLayoutContrato(response.getOcorrencias(i)
					.getDsSituacaoLayoutContrato());
			saidaDTO.setNumeroLinhas(30);
			listaRetorno.add(saidaDTO);
		}
		return listaRetorno;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.service.business.mantercontrato.
	 *      IManterContratoService#consultarListaTipoRetornoLayout(
	 *      br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ConsultarListaTipoRetornoLayoutEntradaDTO)
	 */
	public List<ConsultarListaTipoRetornoLayoutSaidaDTO> consultarListaTipoRetornoLayout(
			ConsultarListaTipoRetornoLayoutEntradaDTO entradaDTO) {

		ConsultarListaTipoRetornoLayoutRequest request = new ConsultarListaTipoRetornoLayoutRequest();
		request.setCdPessoaJuridicaContrato(entradaDTO
				.getCdPessoaJuridicaContrato());
		request.setCdTipoContratoNegocio(entradaDTO.getCdTipoContratoNegocio());
		request.setNrSequenciaContratoNegocio(entradaDTO
				.getNrSequenciaContratoNegocio());
		request.setCdTipoLayoutArquivo(entradaDTO.getCdTipoLayoutArquivo());
		request.setCdTipoTela(entradaDTO.getCdTipoTela());
		request.setNumeroOcorrencias(30);

		ConsultarListaTipoRetornoLayoutResponse response = getFactoryAdapter()
				.getConsultarListaTipoRetornoLayoutPDCAdapter().invokeProcess(
						request);

        List<ConsultarListaTipoRetornoLayoutSaidaDTO> listaRetorno =
            new ArrayList<ConsultarListaTipoRetornoLayoutSaidaDTO>();
		ConsultarListaTipoRetornoLayoutSaidaDTO saidaDTO;

		for (int i = 0; i < response.getOcorrenciasCount(); i++) {
			saidaDTO = new ConsultarListaTipoRetornoLayoutSaidaDTO();

			saidaDTO.setCodMensagem(response.getCodMensagem());
			saidaDTO.setMensagem(response.getMensagem());
			saidaDTO.setCdTipoArquivoRetorno(response.getOcorrencias(i)
					.getCdTipoArquivoRetorno());
			saidaDTO.setDsTipoArquivoRetorno(response.getOcorrencias(i)
					.getDsTipoArquivoRetorno());
			SimpleDateFormat formato1 = new SimpleDateFormat("yyyy-MM-dd");
			SimpleDateFormat formato2 = new SimpleDateFormat("dd/MM/yyyy");
			try {
				saidaDTO.setDtInclusaoRegistro(formato2.format(formato1
						.parse(response.getOcorrencias(i)
								.getDtInclusaoRegistro())));
			} catch (IndexOutOfBoundsException e) {

			} catch (ParseException e) {
				saidaDTO.setDtInclusaoRegistro("");
			}
			listaRetorno.add(saidaDTO);
		}
		return listaRetorno;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.service.business.mantercontrato.
	 *      IManterContratoService#incluirTipoRetornoLayout
	 *      (br.com.bradesco.web.pgit.service.business.mantercontrato.bean.IncluirTipoRetornoLayoutEntradaDTO)
	 */
	public IncluirTipoRetornoLayoutSaidaDTO incluirTipoRetornoLayout(
			IncluirTipoRetornoLayoutEntradaDTO entradaDTO) {
		InlcuirTipoRetornoLayoutRequest request = new InlcuirTipoRetornoLayoutRequest();
		request.setCdArquivoRetornoPagamento(entradaDTO
				.getCdArquivoRetornoPagamento());
		request.setCdPessoaJuridicaContrato(entradaDTO
				.getCdPessoaJuridicaContrato());
		request.setCdTipoClassificacaoRetorno(entradaDTO
				.getCdTipoClassificacaoRetorno());
		request.setCdTipoContratoNegocio(entradaDTO.getCdTipoContratoNegocio());
		request.setCdTipoGeracaoRetorno(entradaDTO.getCdTipoGeracaoRetorno());
		request.setCdTipoLayoutArquivo(entradaDTO.getCdTipoLayoutArquivo());
		request.setCdTipoMontaRetorno(entradaDTO.getCdTipoMontaRetorno());
		request.setCdTipoOcorrenciaRetorno(entradaDTO
				.getCdTipoOcorrenciaRetorno());
		request.setCdTipoOrdemRegistroRetorno(entradaDTO
				.getCdTipoOrdemRegistroRetorno());
		request.setNrSequenciaContratoNegocio(entradaDTO
				.getNrSequenciaContratoNegocio());

		InlcuirTipoRetornoLayoutResponse response = getFactoryAdapter()
				.getInlcuirTipoRetornoLayoutPDCAdapter().invokeProcess(request);

		IncluirTipoRetornoLayoutSaidaDTO saidaDTO = new IncluirTipoRetornoLayoutSaidaDTO();
		saidaDTO.setCodMensagem(response.getCodMensagem());
		saidaDTO.setMensagem(response.getMensagem());

		return saidaDTO;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.service.business.mantercontrato.
	 *      IManterContratoService#alterarConfiguracaoLayoutArqContrato(br.com.bradesco.web.
	 *      pgit.service.business.mantercontrato.bean.AlterarConfiguracaoLayoutArqContratoEntradaDTO)
	 */
	public AlterarConfiguracaoLayoutArqContratoSaidaDTO alterarConfiguracaoLayoutArqContrato(
			ConfiguracaoLayoutArquivoContratoEntradaDto entrada) {

		AlterarConfiguracaoLayoutArqContratoRequest request = new AlterarConfiguracaoLayoutArqContratoRequest();
		request.setCdPessoaJuridicaContrato(verificaLongNulo(entrada
				.getCdPessoaJuridicaContrato()));
		request.setCdTipoContratoNegocio(verificaIntegerNulo(entrada
				.getCdTipoContratoNegocio()));
		request.setNrSequenciaContratoNegocio(verificaLongNulo(entrada
				.getNrSequenciaContratoNegocio()));
		request.setCdTipoLayoutArquivo(verificaIntegerNulo(entrada
				.getCdTipoLayoutArquivo()));
		request.setCdSituacaoLayoutContrato(verificaIntegerNulo(entrada
				.getCdSituacaoLayoutContrato()));
		request.setCdResponsavelCustoEmpresa(verificaIntegerNulo(entrada
				.getCdResponsavelCustoEmpresa()));
		request
				.setPercentualCustoOrganizacaoTransmissao(verificaBigDecimalNulo(entrada
						.getPercentualCustoOrganizacaoTransmissao()));
		request.setCdMensagemLinhaExtrato(verificaIntegerNulo(entrada
				.getCdMensagemLinhaExtrato()));
		request.setNumeroVersaoLayoutArquivo(verificaIntegerNulo(entrada
				.getNumeroVersaoLayoutArquivo()));
		
		request.setCdTipoControlePagamento(verificaIntegerNulo(entrada.getCdTipoControlePagamento()));

		AlterarConfiguracaoLayoutArqContratoResponse response = getFactoryAdapter()
				.getAlterarConfiguracaoLayoutArqContratoPDCAdapter()
				.invokeProcess(request);

		AlterarConfiguracaoLayoutArqContratoSaidaDTO saida = new AlterarConfiguracaoLayoutArqContratoSaidaDTO();
		saida.setCodMensagem(response.getCodMensagem());
		saida.setMensagem(response.getMensagem());
		return saida;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.service.business.mantercontrato.IManterContratoService#incluirLayout(
	 * br.com.bradesco.web.pgit.service.business.mantercontrato.bean.IncluirLayoutEntradaDTO)
	 */
	public IncluirLayoutSaidaDTO incluirLayout(IncluirLayoutEntradaDTO entrada) {
		IncluirLayoutRequest request = new IncluirLayoutRequest();

		request.setCdPessoaJuridicaNegocio(verificaLongNulo(entrada
				.getCdPessoaJuridicaNegocio()));
		request.setCdTipoContratoNegocio(verificaIntegerNulo(entrada
				.getCdTipoContratoNegocio()));
		request.setNrSequenciaContratoNegocio(verificaLongNulo(entrada
				.getNrSequenciaContratoNegocio()));
		request.setCdTipoLayoutArquivo(verificaIntegerNulo(entrada
				.getCdTipoLayoutArquivo()));
		request.setNumeroVersaoLayoutArquivo(verificaIntegerNulo(entrada
				.getNumeroVersaoLayoutArquivo()));
		request.setCdSituacaoLayoutContrato(verificaIntegerNulo(entrada
				.getCdSituacaoLayoutContrato()));
		request.setCdResponsavelCustoEmpresa(verificaIntegerNulo(entrada
				.getCdResponsavelCustoEmpresa()));
		request
				.setPercentualCustoOrganizacaoTransmissao(verificaBigDecimalNulo(entrada
						.getPercentualCustoOrganizacaoTransmissao()));
		request.setCdMensagemLinhaExtrato(verificaIntegerNulo(entrada
				.getCdMensagemLinhaExtrato()));

		IncluirLayoutResponse response = getFactoryAdapter()
				.getIncluirLayoutPDCAdapter().invokeProcess(request);

		IncluirLayoutSaidaDTO saida = new IncluirLayoutSaidaDTO();
		saida.setCodMensagem(response.getCodMensagem());
		saida.setMensagem(response.getMensagem());
		return saida;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.service.business.mantercontrato.IManterContratoService#excluirLayout(
	 * br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ExcluirLayoutEntradaDTO)
	 */
	public ExcluirLayoutSaidaDTO excluirLayout(ExcluirLayoutEntradaDTO entrada) {
		ExcluirLayoutRequest request = new ExcluirLayoutRequest();
		request.setCdPessoaJuridicaNegocio(verificaLongNulo(entrada
				.getCdPessoaJuridicaNegocio()));
		request.setCdTipoContratoNegocio(verificaIntegerNulo(entrada
				.getCdTipoContratoNegocio()));
		request.setNrSequenciaContratoNegocio(verificaLongNulo(entrada
				.getNrSequenciaContratoNegocio()));
		request.setCdTipoLayoutArquivo(verificaIntegerNulo(entrada
				.getCdTipoLayoutArquivo()));
		request.setNumeroVersaoLayoutArquivo(verificaIntegerNulo(entrada
				.getNumeroVersaoLayoutArquivo()));

		ExcluirLayoutResponse response = getFactoryAdapter()
				.getExcluirLayoutPDCAdapter().invokeProcess(request);

		ExcluirLayoutSaidaDTO saida = new ExcluirLayoutSaidaDTO();
		saida.setCodMensagem(response.getCodMensagem());
		saida.setMensagem(response.getMensagem());
		return saida;
	}
	// ---------------------------FIM LAYOUT
	// ------------------------------------------------------

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.service.business.mantercontrato.IManterContratoService#consultarContaContrato
	 *      (br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ConsultarContaContratoEntradaDTO)
	 */
	public ConsultarContaContratoSaidaDTO consultarContaContrato(
			ConsultarContaContratoEntradaDTO entrada) {
		ConsultarContaContratoRequest request = new ConsultarContaContratoRequest();
		request.setCdPessoaJuridicaContrato(entrada
				.getCdPessoaJuridicaContrato());
		request
				.setCdPessoaJuridicaVinculo(entrada
						.getCdPessoaJuridicaVinculo());
		request.setCdTipoContratoNegocio(entrada.getCdTipoContratoNegocio());
		request.setCdTipoContratoVinculo(entrada.getCdTipoContratoVinculo());
		request.setCdTipoVinculoContrato(entrada.getCdTipoVinculoContrato());
		request.setNrSequenciaContratoNegocio(entrada
				.getNrSequenciaContratoNegocio());
		request.setNrSequenciaContratoVinculo(entrada
				.getNrSequenciaContratoVinculo());

		ConsultarContaContratoResponse response = getFactoryAdapter()
				.getConsultarContaContratoPDCAdapter().invokeProcess(request);

		ConsultarContaContratoSaidaDTO saida = new ConsultarContaContratoSaidaDTO();
		saida.setCdMotivoSituacaoConta(response.getCdMotivoSituacaoConta());
		saida.setCdMunicipio(response.getCdMunicipio());
		saida.setCdMunicipioFeriadoLocal(response.getCdMunicipioFeriadoLocal());
		saida.setCdOperacaoCanalInclusao(response.getCdOperacaoCanalInclusao());
		saida.setCdOperacaoCanalManutencao(response
				.getCdOperacaoCanalManutencao());
		saida.setCdSiglaUf(response.getCdSiglaUf());
		saida.setCdSituacaoVinculacaoConta(response
				.getCdSituacaoVinculacaoConta());
		saida.setCdTipoCanalInclusao(response.getCdTipoCanalInclusao());
		saida.setCdTipoCanalManutencao(response.getCdTipoCanalManutencao());
		saida.setCdUf(response.getCdUf());
		saida.setCdUsuarioInclusao(response.getCdUsuarioInclusao());
		saida.setCdUsuarioInclusaoExterno(response
				.getCdUsuarioInclusaoExterno());
		saida.setCdUsuarioManutencao(response.getCdUsuarioManutencao());
		saida.setCdUsuarioManutencaoExterno(response
				.getCdUsuarioInclusaoExterno());
		saida.setCodMensagem(response.getCodMensagem());
		saida.setDsMotivoSituacaoConta(response.getDsMotivoSituacaoConta());
		saida.setDsMunicipio(response.getDsMunicipio());
		saida.setDsSituacaoVinculacaoConta(response
				.getDsSituacaoVinculacaoConta());
		saida.setDsTipoCanalInclusao(response.getDsTipoCanalInclusao());
		saida.setDsTipoCanalManutencao(response.getDsTipoCanalManutencao());
		saida.setDsUf(response.getDsUf());
		saida.setHrInclusaoRegistro(FormatarData.formatarDataTrilha(response
				.getHrInclusaoRegistro()));
		saida.setHrManutencaoRegistro(FormatarData.formatarDataTrilha(response
				.getHrManutencaoRegistro()));
		saida.setVlAdicionalContratoConta(response
				.getVlAdicionalContratoConta());
		saida.setDtInicioValorAdicional(response.getDtInicioValorAdicional());
		saida.setDtFimValorAdicional(response.getDtFimValorAdicional());
		saida.setVlAdicContrCtaTed(response.getVlAdicContrCtaTed());
		saida.setDtIniVlrAdicionalTed(response.getDtIniVlrAdicionalTed());
		saida.setDtFimVlrAdicionalTed(response.getDtFimVlrAdicionalTed());
		saida.setDescApelido(response.getDescApelido());
		saida.setMensagem(response.getMensagem());

		List<FinalidadeSaidaDTO> listaFinalidades = new ArrayList<FinalidadeSaidaDTO>();
		FinalidadeSaidaDTO finalidade;

		for (int i = 0; i < response.getOcorrenciasCount(); i++) {
			finalidade = new FinalidadeSaidaDTO();
			finalidade.setCdFinalidade(response.getOcorrencias(i)
					.getCdFinalidadeContaPagamento());
			finalidade.setDsFinalidade(response.getOcorrencias(i)
					.getDsFinalidadeContaPagamento());

			listaFinalidades.add(finalidade);
		}
		saida.setListaFinalidade(listaFinalidades);
		return saida;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.service.business.mantercontrato.
	 *      IManterContratoService#listarModalidadeTipoServico(br.com.bradesco.web.pgit.service.
	 *      business.mantercontrato.bean.ListarModalidadeTipoServicoEntradaDTO)
	 */
	public List<ListarModalidadeTipoServicoSaidaDTO> listarModalidadeTipoServico(
			ListarModalidadeTipoServicoEntradaDTO entrada) {
		ListarModalidadeTipoServicoRequest request = new ListarModalidadeTipoServicoRequest();

		request.setCdPessoaJuridicaContrato(entrada.getCdPessoaJuridica());
		request.setCdProdutoOperacaoRelacionado(entrada.getCdModalidade());
		request.setCdProdutoServicoOperacao(entrada.getCdTipoServico());
		request.setCdTipoContrato(entrada.getCdTipoContrato());
		request.setNrSequenciaContrato(entrada.getNrSequenciaContrato());
		request.setNrMaximoOcorrencias(30);

		ListarModalidadeTipoServicoResponse response = getFactoryAdapter()
				.getListarModalidadeTipoServicoPDCAdapter().invokeProcess(
						request);

		List<ListarModalidadeTipoServicoSaidaDTO> lista = new ArrayList<ListarModalidadeTipoServicoSaidaDTO>();
		ListarModalidadeTipoServicoSaidaDTO saida;

		for (int i = 0; i < response.getOcorrenciasCount(); i++) {
			saida = new ListarModalidadeTipoServicoSaidaDTO();
			saida.setCodMensagem(response.getCodMensagem());
			saida.setMensagem(response.getMensagem());
			saida.setCdAmbienteServicoContrato(response.getOcorrencias(i)
					.getCdAmbienteServicoContrato());
			saida.setCdParametroTela(response.getOcorrencias(i)
					.getCdParametroTela());
			saida.setCdServico(response.getOcorrencias(i)
					.getCdProdutoServicoOperacao());
			saida.setDsServico(response.getOcorrencias(i)
					.getDsProdutoServicoOperacao());
			saida.setCdModalidade(response.getOcorrencias(i)
					.getCdProdutoOperacaoRelacionado());
			saida.setDsModalidade(response.getOcorrencias(i)
					.getDsProdutoOperacaoRelacionado());
			saida.setCdRelacionamentoProduto(response.getOcorrencias(i)
					.getCdRelacionamentoProdutoProduto());
			saida.setCdSituacaoServicoRelacionado(response.getOcorrencias(i)
					.getCdSituacaoServicoRelacionado());
			saida.setDsSituacaoServicoRelacionado(response.getOcorrencias(i)
					.getDsSituacaoServicoRelacionado());
			saida.setQtdeModalidade(response.getOcorrencias(i)
					.getQtdeModalidade());
			saida.setCdProdutoOperacaoRelacionado(response.getOcorrencias(i)
					.getCdProdutoOperacaoRelacionado());
			saida.setDsProdutoOperacaoRelacionado(response.getOcorrencias(i)
					.getDsProdutoOperacaoRelacionado());

			if (response.getOcorrencias(i).getDtInclusaoServico() != null) {
				SimpleDateFormat formato1 = new SimpleDateFormat("yyyy-MM-dd");
				SimpleDateFormat formato2 = new SimpleDateFormat("dd/MM/yyyy");
				if (!"".equals(response.getOcorrencias(i)
						.getDtInclusaoServico())) {
					String stringData = response.getOcorrencias(i)
							.getDtInclusaoServico().substring(0, 10);
					try {
						saida.setDtInicialServico(formato2.format(formato1
								.parse(stringData)));
					} catch (ParseException e) {
						saida.setDtInicialServico("");
					}
				} else {
					saida.setDtInicialServico("");
				}
			}
			lista.add(saida);
		}
		return lista;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.service.business.mantercontrato.IManterContratoService#listarConManTarifa
	 *      (br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ListarConManTarifaEntradaDTO)
	 */
	public List<ListarConManTarifaSaidaDTO> listarConManTarifa(
			ListarConManTarifaEntradaDTO entrada) {
		ListarConManTarifaRequest request = new ListarConManTarifaRequest();
		request
				.setCdPessoaJuridicaNegocio(entrada
						.getCdPessoaJuridicaNegocio());
		request.setCdTipoContratoNegocio(entrada.getCdTipoContratoNegocio());
		request.setNrSequenciaContratoNegocio(entrada
				.getNrSequenciaContratoNegocio());
		request.setCdProdutoServicoOperacao(entrada
				.getCdProdutoServicoOperacao());
		request.setCdProdutoOperacaoRelacionado(entrada
				.getCdProdutoOperacaoRelacionado());
		request.setCdOperacaoProdutoServico(entrada
				.getCdOperacaoProdutoServico());
		request.setDtInicio(entrada.getDtInicio());
		request.setDtFim(entrada.getDtFim());
		request.setNrOcorrencias(30);

		ListarConManTarifaResponse response = getFactoryAdapter()
				.getListarConManTarifaPDCAdapter().invokeProcess(request);

		List<ListarConManTarifaSaidaDTO> lista = new ArrayList<ListarConManTarifaSaidaDTO>();
		ListarConManTarifaSaidaDTO saida;

		SimpleDateFormat formato1 = new SimpleDateFormat("dd.MM.yyyy");
		SimpleDateFormat formato2 = new SimpleDateFormat("dd/MM/yyyy");

		for (int i = 0; i < response.getOcorrenciasCount(); i++) {
			saida = new ListarConManTarifaSaidaDTO();
			saida.setCodMensagem(response.getCodMensagem());
			saida.setMensagem(response.getMensagem());
			saida.setHrInclusaoRegistroHistorico(response.getOcorrencias(i)
					.getHrInclusaoRegistroHistorico());
			saida.setDsServico(response.getOcorrencias(i).getDsServico());
			saida.setDsModalidade(response.getOcorrencias(i).getDsModalidade());
			saida.setDsOperacao(response.getOcorrencias(i).getDsOperacao());
			saida.setDtInicioVigencia(response.getOcorrencias(i)
					.getDtInicioVigencia());
			saida.setDtManutencao(response.getOcorrencias(i).getDtManutencao());
			saida.setHrManutencao(response.getOcorrencias(i).getHrManutencao());
			saida.setCdUsuario(response.getOcorrencias(i).getCdUsuario());
			saida.setCodTipoServico(response.getOcorrencias(i).getCdServico());
			saida.setCodModalidadeServico(response.getOcorrencias(i)
					.getCdModalidade());
			saida.setCodOperacaoModalidadeServico(response.getOcorrencias(i)
					.getCdOperacao());
			saida.setDsTipoManutencao(response.getOcorrencias(i)
					.getDsTipoManutencao());
			try {
				saida.setDtManutencaoFormatada(String.valueOf(formato2
						.format(formato1.parse(response.getOcorrencias(i)
								.getDtManutencao()))));
			} catch (ParseException e) {
				saida.setDtManutencaoFormatada("");
			}
			lista.add(saida);
		}
		return lista;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.service.business.mantercontrato.
	 *      IManterContratoService#listarHistCondCobrReajuste(br.com.bradesco.web.pgit.service.
	 *      business.mantercontrato.bean.ListarConManServicoEntradaDTO)
	 */
	public List<ListarConManServicoSaidaDTO> listarHistCondCobrReajuste(
			ListarConManServicoEntradaDTO entrada) {
		ListarConManServicoRequest request = new ListarConManServicoRequest();
		request.setCdTipoServico(entrada.getCdTipoServico());
		request.setCdPessoaJuridicaNegocio(verificaLongNulo(entrada
				.getCdPessoaJuridicaNegocio()));
		request.setCdTipoContratoNegocio(verificaIntegerNulo(entrada
				.getCdTipoContratoNegocio()));
		request.setNrSequenciaContratoNegocio(verificaLongNulo(entrada
				.getNrSequenciaContratoNegocio()));
		request.setCdProdutoServicoOperacao(verificaIntegerNulo(entrada
				.getCdProdutoServicoOperacao()));
		request.setCdProdutoOperacaoRelacionado(verificaIntegerNulo(entrada
				.getCdProdutoOperacaoRelacionado()));
		request.setDtInicio(entrada.getDtInicio());
		request.setDtFim(entrada.getDtFim());
		request.setNrOcorrencias(30);

		ListarConManServicoResponse response = getFactoryAdapter()
				.getListarConManServicoPDCAdapter().invokeProcess(request);

		List<ListarConManServicoSaidaDTO> lista = new ArrayList<ListarConManServicoSaidaDTO>();
		ListarConManServicoSaidaDTO saida;

		SimpleDateFormat formato1 = new SimpleDateFormat("dd.MM.yyyy");
		SimpleDateFormat formato2 = new SimpleDateFormat("dd/MM/yyyy");

		for (int i = 0; i < response.getOcorrenciasCount(); i++) {
			saida = new ListarConManServicoSaidaDTO();
			saida.setCodMensagem(response.getCodMensagem());
			saida.setMensagem(response.getMensagem());
			saida.setCdModServico(response.getOcorrencias(i).getCdModalidade());
			saida.setCdProduto(response.getOcorrencias(i).getCdProduto());
			saida.setCdTipoServico(response.getOcorrencias(i)
					.getCdTipoServico());
			saida.setCdUsuario(response.getOcorrencias(i).getCdUsuario());
			saida.setDsModalidade(response.getOcorrencias(i).getDsModalidade());
			saida.setDsProduto(response.getOcorrencias(i).getDsProduto());
			saida.setDsTipoManutencao(response.getOcorrencias(i)
					.getDsTipoManutencao());
			saida.setDtManutencao(response.getOcorrencias(i).getDtManutencao());
			saida.setHrInclusaoRegistroHistorico(response.getOcorrencias(i)
					.getHrInclusaoRegistroHistorico());
			saida.setHrManutencao(response.getOcorrencias(i).getHrManutencao());

			try {
				saida.setDtManutencaoFormatada(String.valueOf(formato2
						.format(formato1.parse(response.getOcorrencias(i)
								.getDtManutencao()))));
			} catch (ParseException e) {
				saida.setDtManutencaoFormatada("");
			}
			lista.add(saida);
		}
		return lista;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.service.business.mantercontrato.
	 *      IManterContratoService#listarTarifasContrato(br.com.bradesco.web.pgit.service.business.
	 *      mantercontrato.bean.ListarTarifasContratoEntradaDTO)
	 */
	public List<ListarTarifasContratoSaidaDTO> listarTarifasContrato(
			ListarTarifasContratoEntradaDTO entrada) {
		ListarTarifasContratoRequest request = new ListarTarifasContratoRequest();
		request.setCdPessoaJuridica(entrada.getCdPessoaJuridica());
		request.setCdTipoContrato(entrada.getCdTipoContrato());
		request.setNrSequenciaContrato(entrada.getNrSequenciaContrato());
		request.setCdTipoServico(entrada.getCdTipoServico());
		request.setCdModalidade(entrada.getCdModalidade());
		request
				.setCdRelacionamentoProduto(entrada
						.getCdRelacionamentoProduto());
		request.setNrOcorrencias(50);

		ListarTarifasContratoResponse response = getFactoryAdapter()
				.getListarTarifasContratoPDCAdapter().invokeProcess(request);

		List<ListarTarifasContratoSaidaDTO> lista = new ArrayList<ListarTarifasContratoSaidaDTO>();
		ListarTarifasContratoSaidaDTO saida;
		for (int i = 0; i < response.getOcorrenciasCount(); i++) {
			saida = new ListarTarifasContratoSaidaDTO();
			saida.setCodMensagem(response.getCodMensagem());
			saida.setMensagem(response.getMensagem());
			saida.setCdServico(response.getOcorrencias(i).getCdServico());
			saida.setDsServico(response.getOcorrencias(i).getDsServico());
			saida.setCdModalidade(response.getOcorrencias(i).getCdModalidade());
			saida.setDsModalidade(response.getOcorrencias(i).getDsModalidade());
			saida.setCdRelacionamentoProduto(response.getOcorrencias(i)
					.getCdRelacionamentoProduto());
			saida.setDtInclusaoServico(response.getOcorrencias(i)
					.getDtInclusaoServico());
			saida.setCdSituacaoServicoRelacionado(response.getOcorrencias(i)
					.getCdSituacaoServicoRelacionado());
			saida.setDtSituacaoServicoRelacionado(response.getOcorrencias(i)
					.getDtSituacaoServicoRelacionado());
			lista.add(saida);
		}
		return lista;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.service.business.mantercontrato.
	 *      IManterContratoService#validarQtdDiasCobrancaApsApuracao(br.com.bradesco.web.pgit.service.
	 *      business.mantercontrato.bean.ValidarQtdDiasCobrancaApsApuracaoEntradaDTO)
	 */
	public ValidarQtdDiasCobrancaApsApuracaoSaidaDTO validarQtdDiasCobrancaApsApuracao(
			ValidarQtdDiasCobrancaApsApuracaoEntradaDTO entrada) {
		ValidarQtdDiasCobrancaApsApuracaoRequest request = new ValidarQtdDiasCobrancaApsApuracaoRequest();
		request.setCdPeriodicidadeCobrancaTarifa(verificaIntegerNulo(entrada.getCdPeriodicidadeCobrancaTarifa()));
		request.setQtDiaCobrancaTarifa(verificaIntegerNulo(entrada.getQuantidadeDiasCobrancaTarifa()));

		ValidarQtdDiasCobrancaApsApuracaoResponse response = getFactoryAdapter()
				.getValidarQtdDiasCobrancaApsApuracaoPDCAdapter()
				.invokeProcess(request);

		ValidarQtdDiasCobrancaApsApuracaoSaidaDTO saida = new ValidarQtdDiasCobrancaApsApuracaoSaidaDTO();
		saida.setCodMensagem(response.getCodMensagem());
		saida.setMensagem(response.getMensagem());
		return saida;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.service.business.mantercontrato.
	 *      IManterContratoService#alterarCondCobTarifa(br.com.bradesco.web.pgit.service
	 *      .business.mantercontrato.bean.AlterarCondCobTarifaEntradaDTO)
	 */
	public AlterarCondCobTarifaSaidaDTO alterarCondCobTarifa(
			AlterarCondCobTarifaEntradaDTO entrada) {
		AlterarCondCobTarifaRequest request = new AlterarCondCobTarifaRequest();
		request.setCdPessoaContrato(entrada.getCdPessoaContrato());
		request.setCdTipoContrato(entrada.getCdTipoContrato());
		request.setNrSequenciaContrato(entrada.getNrSequenciaContrato());
		request.setCdProdutoServicoOperacao(entrada.getCdTipoServico());
		request.setCdProdutoOperacaoRelacionado(entrada.getCdModalidade());
		request.setCdPeriodicidadeCobrancaTarifa(entrada
				.getCdPeriodicidadeCobrancaTarifa());
		request.setQuantidadeDiasCobrancaTarifa(entrada
				.getQuantidadeDiasCobrancaTarifa());
		request.setNrFechamentoApuracaoTarifa(entrada
				.getNrFechamentoApuracaoTarifa());

		AlterarCondCobTarifaResponse response = getFactoryAdapter()
				.getAlterarCondCobTarifaPDCAdapter().invokeProcess(request);

		AlterarCondCobTarifaSaidaDTO saida = new AlterarCondCobTarifaSaidaDTO();
		saida.setCodMensagem(response.getCodMensagem());
		saida.setMensagem(response.getMensagem());
		return saida;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.service.business.mantercontrato.
	 *      IManterContratoService#alterarCondReajusteTarifa(br.com.bradesco.web.pgit.service.
	 *      business.mantercontrato.bean.AlterarCondReajusteTarifaEntradaDTO)
	 */
	public AlterarCondReajusteTarifaSaidaDTO alterarCondReajusteTarifa(
			AlterarCondReajusteTarifaEntradaDTO entrada) {
		AlterarCondReajusteTarifaRequest request = new AlterarCondReajusteTarifaRequest();
		request
				.setCdPessoaJuridicaNegocio(entrada
						.getCdPessoaJuridicaNegocio());
		request.setCdTipoContratoNegocio(entrada.getCdTipoContratoNegocio());
		request.setNrSequenciaContratoNegocio(entrada
				.getNrSequenciaContratoNegocio());
		request.setCdServico(entrada.getCdServico());
		request.setCdModalidade(entrada.getCdModalidade());
		request
				.setCdRelacionamentoProduto(entrada
						.getCdRelacionamentoProduto());
		request.setCdTipoReajusteTarifa(entrada.getCdTipoReajusteTarifa());
		request.setQtMesReajusteTarifa(entrada.getQtMesReajusteTarifa());
		request.setCdIndicadorEconomicoReajuste(entrada
				.getCdIndicadorEconomicoReajuste());
		request.setCdPercentualReajusteTarifa(verificaBigDecimalNulo(entrada
				.getCdPercentualIndice()));
		request.setCdPercentualTarifaCatalogada(verificaBigDecimalNulo(entrada
				.getCdPercentualFlexibilizacao()));

		AlterarCondReajusteTarifaResponse response = getFactoryAdapter()
				.getAlterarCondReajusteTarifaPDCAdapter()
				.invokeProcess(request);

		AlterarCondReajusteTarifaSaidaDTO saida = new AlterarCondReajusteTarifaSaidaDTO();
		saida.setCodMensagem(response.getCodMensagem());
		saida.setMensagem(response.getMensagem());
		return saida;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.service.business.mantercontrato.
	 *      IManterContratoService#consultarCondCobTarifa(br.com.bradesco.web.pgit.service.business.
	 *      mantercontrato.bean.ConsultarCondCobTarifaEntradaDTO)
	 */
	public ConsultarCondCobTarifaSaidaDTO consultarCondCobTarifa(
			ConsultarCondCobTarifaEntradaDTO entrada) {
		ConsultarCondCobTarifaRequest request = new ConsultarCondCobTarifaRequest();
		request.setCdPessoaContrato(entrada.getCdPessoaContrato());
		request.setCdTipoContrato(entrada.getCdTipoContrato());
		request.setNrSequenciaContrato(entrada.getNrSequenciaContrato());
		request.setCdServico(entrada.getCdServico());
		request.setCdModalidade(entrada.getCdModalidade());
		request
				.setCdRelacionamentoProduto(entrada
						.getCdRelacionamentoProduto());

		ConsultarCondCobTarifaResponse response = getFactoryAdapter()
				.getConsultarCondCobTarifaPDCAdapter().invokeProcess(request);

		ConsultarCondCobTarifaSaidaDTO saidaDTO = new ConsultarCondCobTarifaSaidaDTO();
		saidaDTO = new ConsultarCondCobTarifaSaidaDTO();
		saidaDTO.setCodMensagem(response.getCodMensagem());
		saidaDTO.setMensagem(response.getMensagem());
		saidaDTO.setCdPeriodicidadeCobrancaTarifa(response
				.getCdPeriodicidadeCobrancaTarifa());
		saidaDTO.setDsPeriodicidadeCobrancaTarifa(response
				.getDsPeriodicidadeCobrancaTarifa());
		saidaDTO.setQtDiaCobrancaTarifa(response.getQtDiaCobrancaTarifa());
		saidaDTO.setNrFechamentoApuracaoTarifa(response
				.getNrFechamentoApuracaoTarifa());
		saidaDTO.setCdTipoReajusteTarifa(response.getCdTipoReajusteTarifa());
		saidaDTO.setCdPeriodicidadeReajusteTarifa(response
				.getCdPeriodicidadeReajusteTarifa());
		saidaDTO.setQtMesReajusteTarifa(response.getQtMesReajusteTarifa());
		saidaDTO.setCdIndicadorEconomicoReajuste(response
				.getCdIndicadorEconomicoReajuste());
		saidaDTO
				.setCdPercentualIndice(response.getCdPercentualReajusteTarifa());
		saidaDTO.setCdPercentualFlexibilizacao(response
				.getCdPercentualTarifaCatalogo());
		return saidaDTO;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.service.business.mantercontrato.
	 *      IManterContratoService#consultarCondReajusteTarifa(br.com.bradesco.web.pgit.service.
	 *      business.mantercontrato.bean.ConsultarCondReajusteTarifaEntradaDTO)
	 */
	public ConsultarCondReajusteTarifaSaidaDTO consultarCondReajusteTarifa(
			ConsultarCondReajusteTarifaEntradaDTO entrada) {
		ConsultarCondReajusteTarifaRequest request = new ConsultarCondReajusteTarifaRequest();

		request.setCdPessoaContrato(entrada.getCdPessoaContrato());
		request.setCdTipoContrato(entrada.getCdTipoContrato());
		request.setNrSequenciaContrato(entrada.getNrSequenciaContrato());
		request.setCdServico(entrada.getCdServico());
		request.setCdModalidade(entrada.getCdModalidade());
		request
				.setCdRelacionamentoProduto(entrada
						.getCdRelacionamentoProduto());

		ConsultarCondReajusteTarifaResponse response = getFactoryAdapter()
				.getConsultarCondReajusteTarifaPDCAdapter().invokeProcess(
						request);

		ConsultarCondReajusteTarifaSaidaDTO saidaDTO = new ConsultarCondReajusteTarifaSaidaDTO();
		saidaDTO = new ConsultarCondReajusteTarifaSaidaDTO();
		saidaDTO.setCodMensagem(response.getCodMensagem());
		saidaDTO.setMensagem(response.getMensagem());
		saidaDTO.setCdPeriodicidadeCobrancaTarifa(response
				.getCdPeriodicidadeCobrancaTarifa());
		saidaDTO.setDsPeriodicidadeCobrancaTarifa(response
				.getDsPeriodicidadeCobrancaTarifa());
		saidaDTO.setQtDiaCobrancaTarifa(response.getQtDiaCobrancaTarifa());
		saidaDTO.setNrFechamentoApuracaoTarifa(response
				.getNrFechamentoApuracaoTarifa());
		saidaDTO.setCdTipoReajusteTarifa(response.getCdTipoReajusteTarifa());
		saidaDTO.setCdPeriodicidadeReajusteTarifa(response
				.getCdPeriodicidadeReajusteTarifa());
		saidaDTO.setQtMesReajusteTarifa(response.getQtMesReajusteTarifa());
		saidaDTO.setCdIndicadorEconomicoReajuste(response
				.getCdIndicadorEconomicoReajuste());
		saidaDTO.setCdPercentualReajusteTarifa(response
				.getCdPercentualReajusteTarifa());
		saidaDTO.setCdPercentualTarifaCatalogo(response
				.getCdPercentualTarifaCatalogo());
		saidaDTO.setCdIndicadorTipoReajusteBloqueio(response
				.getCdIndicadorTipoReajusteBloqueio());
		saidaDTO.setDsTipoReajuste(response.getDsTipoReajusteTarifa());
		return saidaDTO;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.service.business.mantercontrato.
	 *      IManterContratoService#consultarTarifaContrato(br.com.bradesco.web.pgit.service.
	 *      business.mantercontrato.bean.ConsultarTarifaContratoEntradaDTO)
	 */
	public List<ConsultarTarifaContratoSaidaDTO> consultarTarifaContrato(
			ConsultarTarifaContratoEntradaDTO entrada) {
		ConsultarTarifaContratoRequest request = new ConsultarTarifaContratoRequest();

		request.setCdPessoaJuridica(entrada.getCdPessoaJuridica());
		request.setCdTipoContrato(entrada.getCdTipoContrato());
		request.setNrSequenciaContrato(entrada.getNrSequenciaContrato());
		request.setCdTipoServico(entrada.getCdTipoServico());
		request.setCdModalidade(entrada.getCdModalidade());
		request.setCdRelacionamentoProdutoProduto(entrada
				.getCdRelacionamentoProduto());
		request.setNumeroOcorrencias(30);

		ConsultarTarifaContratoResponse response = getFactoryAdapter()
				.getConsultarTarifaContratoPDCAdapter().invokeProcess(request);

		List<ConsultarTarifaContratoSaidaDTO> lista = new ArrayList<ConsultarTarifaContratoSaidaDTO>();
		ConsultarTarifaContratoSaidaDTO saidaDTO;

		for (int i = 0; i < response.getOcorrenciasCount(); i++) {
			saidaDTO = new ConsultarTarifaContratoSaidaDTO();

			saidaDTO.setCodMensagem(response.getCodMensagem());
			saidaDTO.setMensagem(response.getMensagem());
			saidaDTO.setCdProduto(response.getOcorrencias(i).getCdProduto());
			saidaDTO.setDsProduto(response.getOcorrencias(i).getDsProduto());
			saidaDTO.setCdOperacao(response.getOcorrencias(i).getCdOperacao());
			saidaDTO.setDsOperacao(response.getOcorrencias(i).getDsOperacao());
			saidaDTO.setDtInicioVigencia(FormatarData.formatarData(response
					.getOcorrencias(i).getDtInicioVigencia()));
			saidaDTO.setDtFimVigencia(FormatarData.formatarData(response
					.getOcorrencias(i).getDtFimVigencia()));
			saidaDTO.setVlTarifa(NumberUtils.format(response.getOcorrencias(i)
					.getVlTarifaContratoNegocio()));
			saidaDTO.setVlrReferenciaMinimo(format(response.getOcorrencias(i)
					.getVlTarifaReferMinima()));
			saidaDTO.setVlrReferenciaMaximo(format(response.getOcorrencias(i)
					.getVlTarifaReferMaxima()));
			saidaDTO.setCdUsuarioInclusao(response.getOcorrencias(i)
					.getCdUsuarioInclusao());
			saidaDTO.setCdUsuarioInclusaoExterno(response.getOcorrencias(i)
					.getCdUsuarioInclusaoExterno());
			saidaDTO.setHrInclusaoRegistro(formatarDataTrilha(response
					.getOcorrencias(i).getHrInclusaoRegistro()));
			saidaDTO.setCdOperacaoCanalInclusao(response.getOcorrencias(i)
					.getCdOperacaoCanalInclusao());
			saidaDTO.setCdTipoCanalInclusao(response.getOcorrencias(i)
					.getCdTipoCanalInclusao());
			saidaDTO.setDsTipoCanalInclusao(response.getOcorrencias(i)
					.getDsTipoCanalInclusao());
			saidaDTO.setCdUsuarioManutencao(response.getOcorrencias(i)
					.getCdUsuarioManutencao());
			saidaDTO.setCdUsuarioManutencaoExterno(response.getOcorrencias(i)
					.getCdUsuarioManutencaoExterno());
			saidaDTO.setHrManutencaoRegistro(formatarDataTrilha(response
					.getOcorrencias(i).getHrManutencaoRegistro()));
			saidaDTO.setCdOperacaoCanalManutencao(response.getOcorrencias(i)
					.getCdOperacaoCanalManutencao());
			saidaDTO.setCdTipoCanalManutencao(response.getOcorrencias(i)
					.getCdTipoCanalManutencao());
			saidaDTO.setDsTipoCanalManutencao(response.getOcorrencias(i)
					.getDsTipoCanalManutencao());
			saidaDTO.setCdProdutoServicoRelacionado(response.getOcorrencias(i)
					.getCdProdutoServicoRelacionado());
			saidaDTO.setDsProdutoServicoRelacionado(response.getOcorrencias(i)
					.getDsProdutoServicoRelacionado());
			lista.add(saidaDTO);
		}
		return lista;
	}
	
	public List<ConsultarTarifaContratoSaidaDTO> consultarTarifaContratoLoop(
			ConsultarTarifaContratoEntradaDTO entrada) {
		ConsultarTarifaContratoRequest request = new ConsultarTarifaContratoRequest();

		request.setCdPessoaJuridica(entrada.getCdPessoaJuridica());
		request.setCdTipoContrato(entrada.getCdTipoContrato());
		request.setNrSequenciaContrato(entrada.getNrSequenciaContrato());
		request.setCdTipoServico(entrada.getCdTipoServico());
		request.setCdModalidade(entrada.getCdModalidade());
		request.setCdRelacionamentoProdutoProduto(entrada
				.getCdRelacionamentoProduto());
		request.setNumeroOcorrencias(30);

		ConsultarTarifaContratoResponse response = getFactoryAdapter()
				.getConsultarTarifaContratoPDCAdapter().invokeProcess(request);

		List<ConsultarTarifaContratoSaidaDTO> lista = new ArrayList<ConsultarTarifaContratoSaidaDTO>();
		ConsultarTarifaContratoSaidaDTO saidaDTO;

		for (int i = 0; i < response.getOcorrenciasCount(); i++) {
			saidaDTO = new ConsultarTarifaContratoSaidaDTO();

			saidaDTO.setCodMensagem(response.getCodMensagem());
			saidaDTO.setMensagem(response.getMensagem());
			saidaDTO.setCdProduto(response.getOcorrencias(i).getCdProduto());
			saidaDTO.setDsProduto(response.getOcorrencias(i).getDsProduto());
			saidaDTO.setCdOperacao(response.getOcorrencias(i).getCdOperacao());
			saidaDTO.setDsOperacao(response.getOcorrencias(i).getDsOperacao());
			saidaDTO.setDtInicioVigencia(FormatarData.formatarData(response
					.getOcorrencias(i).getDtInicioVigencia()));
			saidaDTO.setDtFimVigencia(FormatarData.formatarData(response
					.getOcorrencias(i).getDtFimVigencia()));
			saidaDTO.setVlTarifa(NumberUtils.format(response.getOcorrencias(i)
					.getVlTarifaContratoNegocio()));
			saidaDTO.setVlrReferenciaMinimo(format(response.getOcorrencias(i)
					.getVlTarifaReferMinima()));
			saidaDTO.setVlrReferenciaMaximo(format(response.getOcorrencias(i)
					.getVlTarifaReferMaxima()));
			saidaDTO.setCdUsuarioInclusao(response.getOcorrencias(i)
					.getCdUsuarioInclusao());
			saidaDTO.setCdUsuarioInclusaoExterno(response.getOcorrencias(i)
					.getCdUsuarioInclusaoExterno());
			saidaDTO.setHrInclusaoRegistro(formatarDataTrilha(response
					.getOcorrencias(i).getHrInclusaoRegistro()));
			saidaDTO.setCdOperacaoCanalInclusao(response.getOcorrencias(i)
					.getCdOperacaoCanalInclusao());
			saidaDTO.setCdTipoCanalInclusao(response.getOcorrencias(i)
					.getCdTipoCanalInclusao());
			saidaDTO.setDsTipoCanalInclusao(response.getOcorrencias(i)
					.getDsTipoCanalInclusao());
			saidaDTO.setCdUsuarioManutencao(response.getOcorrencias(i)
					.getCdUsuarioManutencao());
			saidaDTO.setCdUsuarioManutencaoExterno(response.getOcorrencias(i)
					.getCdUsuarioManutencaoExterno());
			saidaDTO.setHrManutencaoRegistro(formatarDataTrilha(response
					.getOcorrencias(i).getHrManutencaoRegistro()));
			saidaDTO.setCdOperacaoCanalManutencao(response.getOcorrencias(i)
					.getCdOperacaoCanalManutencao());
			saidaDTO.setCdTipoCanalManutencao(response.getOcorrencias(i)
					.getCdTipoCanalManutencao());
			saidaDTO.setDsTipoCanalManutencao(response.getOcorrencias(i)
					.getDsTipoCanalManutencao());
			saidaDTO.setCdProdutoServicoRelacionado(response.getOcorrencias(i)
					.getCdProdutoServicoRelacionado());
			saidaDTO.setDsProdutoServicoRelacionado(response.getOcorrencias(i)
					.getDsProdutoServicoRelacionado());
			lista.add(saidaDTO);
		}
		return lista;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.service.business.mantercontrato.
	 *      IManterContratoService#listarConManContaVinculadaHistorico(br.com.bradesco.web.pgit.service.business.
	 *      mantercontrato.bean.ListarConManContaVinculadaEntradaDTO)
	 */
	public List<ListarConManContaVinculadaSaidaDTO> listarConManContaVinculadaHistorico(
			ListarConManContaVinculadaEntradaDTO entrada) {
		ListarConManContaVinculadaRequest request = new ListarConManContaVinculadaRequest();
		request.setCdAgencia(entrada.getCdAgencia());
		request.setCdBanco(entrada.getCdBanco());
		request.setCdConta(entrada.getCdConta());
		request.setCdDigitoAgencia(entrada.getCdDigitoAgencia());
		request.setCdDigitoConta(entrada.getCdDigitoConta());
		request
				.setCdPessoaJuridicaNegocio(entrada
						.getCdPessoaJuridicaNegocio());
		request.setCdTipoContratoNegocio(entrada.getCdTipoContratoNegocio());
		request.setDtFim(entrada.getDtFim());
		request.setDtInicio(entrada.getDtInicio());
		request.setNrOcorrencias(30);
		request.setNrSequenciaContratoNegocio(entrada
				.getNrSequenciaContratoNegocio());

		ListarConManContaVinculadaResponse response = getFactoryAdapter()
				.getListarConManContaVinculadaPDCAdapter().invokeProcess(
						request);

		List<ListarConManContaVinculadaSaidaDTO> listaSaida = new ArrayList<ListarConManContaVinculadaSaidaDTO>();
		ListarConManContaVinculadaSaidaDTO saidaDTO;

		for (int i = 0; i < response.getOcorrenciasCount(); i++) {
			saidaDTO = new ListarConManContaVinculadaSaidaDTO();
			saidaDTO.setCdAgencia(response.getOcorrencias(i).getCdAgencia());
			saidaDTO.setCdBanco(response.getOcorrencias(i).getCdBanco());
			saidaDTO.setCdConta(response.getOcorrencias(i).getCdConta());
			saidaDTO.setCdDigitoAgencia(response.getOcorrencias(i)
					.getCdDigitoAgencia());
			saidaDTO.setCdDigitoConta(response.getOcorrencias(i)
					.getCdDigitoConta());
			saidaDTO.setCdPessoaJuridicaVinculo(response.getOcorrencias(i)
					.getCdPessoaJuridicaVinculo());
			saidaDTO.setCdTipoContratoVinculo(response.getOcorrencias(i)
					.getCdTipoContratoVinculo());
			saidaDTO.setCdUsuario(response.getOcorrencias(i).getCdUsuario());
			saidaDTO.setCodMensagem(response.getCodMensagem());
			saidaDTO.setDsAgencia(response.getOcorrencias(i).getDsAgencia());
			saidaDTO.setDsBanco(response.getOcorrencias(i).getDsBanco());
			saidaDTO.setDtManutencao(response.getOcorrencias(i)
					.getDtManutencao());
			saidaDTO.setHrInclusaoRegistroHistorico(response.getOcorrencias(i)
					.getHrInclusaoRegistroHistorico());
			saidaDTO.setHrManutencao(response.getOcorrencias(i)
					.getHrManutencao());
			saidaDTO.setMensagem(response.getMensagem());
			saidaDTO.setNrSequenciaContratoVinculo(response.getOcorrencias(i)
					.getNrSequenciaContratoVinculo());
			saidaDTO.setCdSituacaoConta(response.getOcorrencias(i)
					.getCdSituacaoConta());
			saidaDTO.setDsSituacaoConta(response.getOcorrencias(i)
					.getDsSituacaoConta());
			saidaDTO
					.setCdTipoConta(response.getOcorrencias(i).getCdTipoConta());
			saidaDTO
					.setDsTipoConta(response.getOcorrencias(i).getDsTipoConta());
			saidaDTO.setCdCpfCnpj(response.getOcorrencias(i).getCdCpfCnpj());
			saidaDTO.setDsNomeParticipante(response.getOcorrencias(i)
					.getDsNomeParticipante());
			saidaDTO.setDsTipoManutencao(response.getOcorrencias(i)
					.getDsTipoManutencao());
			listaSaida.add(saidaDTO);
		}
		return listaSaida;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.service.business.mantercontrato.
	 *      IManterContratoService#incluirTipoServModContrato(br.com.bradesco.web.pgit.service.business.
	 *      mantercontrato.bean.IncluirTipoServModContratoEntradaDTO)
	 */
	public IncluirTipoServModContratoSaidaDTO incluirTipoServModContrato(
			IncluirTipoServModContratoEntradaDTO entrada) {
		IncluirTipoServModContratoRequest request = new IncluirTipoServModContratoRequest();
		request.setCdPessoaJuridicaContrato(verificaLongNulo(entrada
				.getCdPessoaJuridicaContrato()));
		request.setCdTipoContrato(verificaIntegerNulo(entrada
				.getCdTipoContrato()));
		request.setNrSequenciaContrato(verificaLongNulo(entrada
				.getNrSequenciaContrato()));
		request.setNrQtdeServicos(verificaIntegerNulo(entrada
				.getNrQtdeServicos()));
		request.setNrQtdeModalidades(verificaIntegerNulo(entrada
				.getNrQtdeModalidades()));

		if (request.getNrQtdeServicos() != 0) {
			for (int i = 0; i < entrada.getListaServicos().size(); i++) {
                br.com.bradesco.web.pgit.service.data.pdc.incluirtiposervmodcontrato.request.Ocorrencias ocorrencia =
                    new br.com.bradesco.web.pgit.service.data.pdc.incluirtiposervmodcontrato.request.Ocorrencias();
				ocorrencia.setCdTipoServico(entrada.getListaServicos().get(i));
				request.addOcorrencias(ocorrencia);
			}
		} else {
            br.com.bradesco.web.pgit.service.data.pdc.incluirtiposervmodcontrato.request.Ocorrencias ocorrencia =
                new br.com.bradesco.web.pgit.service.data.pdc.incluirtiposervmodcontrato.request.Ocorrencias();
			ocorrencia.setCdTipoServico(0);
			request.addOcorrencias(ocorrencia);
		}

		if (request.getNrQtdeModalidades() != 0) {
			for (int i = 0; i < entrada.getListaModalidade().size(); i++) {
                br.com.bradesco.web.pgit.service.data.pdc.incluirtiposervmodcontrato.request.Ocorrencias1 ocorrencia =
                    new br.com.bradesco.web.pgit.service.data.pdc.incluirtiposervmodcontrato.request.Ocorrencias1();
				ocorrencia.setCdModServico(entrada.getListaModalidade().get(i)
						.getCdServico());
				ocorrencia.setCdTipoServicoModalidade(entrada
						.getListaModalidade().get(i).getCdTipoServico());
				request.addOcorrencias1(ocorrencia);
			}
		} else {
            br.com.bradesco.web.pgit.service.data.pdc.incluirtiposervmodcontrato.request.Ocorrencias1 ocorrencia =
                new br.com.bradesco.web.pgit.service.data.pdc.incluirtiposervmodcontrato.request.Ocorrencias1();
			ocorrencia.setCdModServico(0);
			ocorrencia.setCdTipoServicoModalidade(0);
			request.addOcorrencias1(ocorrencia);
		}

		IncluirTipoServModContratoResponse response = getFactoryAdapter()
				.getIncluirTipoServModContratoPDCAdapter().invokeProcess(
						request);

		IncluirTipoServModContratoSaidaDTO saidaDTO = new IncluirTipoServModContratoSaidaDTO();
		saidaDTO.setCodMensagem(response.getCodMensagem());
		saidaDTO.setMensagem(response.getMensagem());
		return saidaDTO;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.service.business.mantercontrato.
	 *      IManterContratoService#excluirTipoServModContrato(br.com.bradesco.web.pgit.service.business.
	 *      mantercontrato.bean.ExcluirTipoServModContratoEntradaDTO)
	 */
	public ExcluirTipoServModContratoSaidaDTO excluirTipoServModContrato(
			ExcluirTipoServModContratoEntradaDTO entrada) {

		ExcluirTipoServModContratoRequest request = new ExcluirTipoServModContratoRequest();
		request.setCdPessoaJuridicaContrato(entrada
				.getCdPessoaJuridicaContrato());
		request.setCdTipoContrato(entrada.getCdTipoContrato());
		request.setNrSequenciaContrato(entrada.getNrSequenciaContrato());
		if (entrada.getCdModalidade1() == null) {
			request.setCdModalidade1(entrada.getCdTipoServico());
			request.setCdTipoServico(0);
		} else {
			request.setCdModalidade1(entrada.getCdModalidade1());
			request.setCdTipoServico(entrada.getCdTipoServico());
		}

		request.setCdModalidade2(entrada.getCdModalidade2() == null
				? 0 : entrada.getCdModalidade2());
		request.setCdModalidade3(entrada.getCdModalidade3() == null
				? 0 : entrada.getCdModalidade3());
		request.setCdModalidade4(entrada.getCdModalidade4() == null
				? 0 : entrada.getCdModalidade4());
		request.setCdModalidade5(entrada.getCdModalidade5() == null
				? 0 : entrada.getCdModalidade5());
		request.setCdModalidade6(entrada.getCdModalidade6() == null
				? 0 : entrada.getCdModalidade6());
		request.setCdModalidade7(entrada.getCdModalidade7() == null
				? 0 : entrada.getCdModalidade7());
		request.setCdModalidade8(entrada.getCdModalidade8() == null
				? 0 : entrada.getCdModalidade8());
		request.setCdModalidade9(entrada.getCdModalidade9() == null
				? 0 : entrada.getCdModalidade9());
		request.setCdModalidade10(entrada.getCdModalidade10() == null
				? 0 : entrada.getCdModalidade10());
		request.setCdExerServicoModalidade(verificaIntegerNulo(entrada
				.getCdExerServicoModalidade()));

		ExcluirTipoServModContratoResponse response = getFactoryAdapter()
				.getExcluirTipoServModContratoPDCAdapter().invokeProcess(
						request);

		ExcluirTipoServModContratoSaidaDTO saidaDTO = new ExcluirTipoServModContratoSaidaDTO();
		saidaDTO.setCodMensagem(response.getCodMensagem());
		saidaDTO.setMensagem(response.getMensagem());
		return saidaDTO;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.service.business.mantercontrato.
	 *      IManterContratoService#consultarConManContaHistorico(br.com.bradesco.web.pgit.service.business.
	 *      mantercontrato.bean.ConsultarConManContaEntradaDTO)
	 */
	public ConsultarConManContaSaidaDTO consultarConManContaHistorico(
			ConsultarConManContaEntradaDTO entrada) {

		ConsultarConManContaRequest request = new ConsultarConManContaRequest();
		request.setCdPessoaJuridicaConta(verificaLongNulo(entrada.getCdPessoaJuridicaConta()));
		request.setCdPessoaJuridicaContrato(verificaLongNulo(entrada.getCdPessoaJuridicaContrato()));
		request.setCdTipoContratoConta(verificaIntegerNulo(entrada.getCdTipoContratoConta()));
		request.setCdTipoContratoNegocio(verificaIntegerNulo(entrada.getCdTipoContratoNegocio()));
		request.setCdTipoVinculoContrato(verificaIntegerNulo(entrada.getCdTipoVinculoContrato()));
		request.setHrInclusaoRegistro(verificaStringNula(entrada.getHrInclusaoRegistro()));
		request.setNrSequenciaContratoConta(verificaLongNulo(entrada.getNrSequenciaContratoConta()));
		request.setNrSequenciaContratoNegocio(verificaLongNulo(entrada.getNrSequenciaContratoNegocio()));

		ConsultarConManContaResponse response = getFactoryAdapter().getConsultarConManContaPDCAdapter().invokeProcess(request);

		ConsultarConManContaSaidaDTO saida = new ConsultarConManContaSaidaDTO();
		saida.setCdBanco(response.getCdBanco());
		saida.setCdComercialAgenciaContabil(response.getCdComercialAgenciaContabil());
		saida.setCdDigitoContaBancaria(response.getCdDigitoContaBancaria());
		saida.setCdMunicipio(response.getCdMunicipio());
		saida.setCdMunicipioFeri(response.getCdMunicipioFeri());
		saida.setCdNumeroContaBancaria(response.getCdNumeroContaBancaria());
		saida.setCdOperacaoCanalInclusao(response.getCdOperacaoCanalInclusao());
		saida.setCdOperacaoCanalManutencao(response.getCdOperacaoCanalManutencao());
		saida.setCdSituacaoVinculo(response.getCdSituacaoVinculo());
		saida.setCdTipoCanalInclusao(response.getCdTipoCanalInclusao());
		saida.setCdTipoCanalManutencao(response.getCdTipoCanalManutencao());
		saida.setCdTipoConta(response.getCdTipoConta());
		saida.setCdUnidadeFederativa(response.getCdUnidadeFederativa());
		saida.setCdUsuarioInclusao(response.getCdUsuarioInclusao());
		saida.setCdUsuarioInclusaoExterno(response.getCdUsuarioInclusaoExterno());
		saida.setCdUsuarioManutencao(response.getCdUsuarioManutencao());
		saida.setCdUsuarioManutencaoExterno(response.getCdUsuarioManutencaoExterno());
		saida.setCodMensagem(response.getCodMensagem());
		saida.setDsBanco(response.getDsBanco());
		saida.setDsCanalInclusao(response.getDsCanalInclusao());
		saida.setDsCanalManutencao(response.getDsCanalManutencao());
		saida.setDsComercialAgenciaContabil(response.getDsComercialAgenciaContabil());
		saida.setDsMunicipio(response.getDsMunicipio());
		saida.setDsMunicipioFeri(response.getDsMunicipioFeri());
		saida.setDsSituacaoVinculo(response.getDsSituacaoVinculo());
		saida.setDsTipoConta(response.getDsTipoConta());
		saida.setDtVinculoFinal(response.getDtVinculoFinal());
		saida.setDtVinculoInicial(response.getDtVinculoInicial());
		saida.setHrInclusaoRegistro(FormatarData.formatarDataTrilha(response.getHrInclusaoRegistro()));
		saida.setHrManutencaoRegistro(FormatarData.formatarDataTrilha(response.getHrManutencaoRegistro()));
		saida.setMensagem(response.getMensagem());
		saida.setDsUnidadeFederativa(response.getDsUnidadeFederativa());
		saida.setCdMotivoSituacaoConta(response.getCdMotivoSituacaoConta());
		saida.setDsMotivoSituacaoConta(response.getDsMotivoSituacaoConta());
		saida.setValorAdicionadoContratoConta(response.getValorAdicionadoContratoConta());
		saida.setDtInicialValorAdicionado(response.getDtInicialValorAdicionado());
		saida.setDtFinalValorAdicionado(response.getDtFinalValorAdicionado());
		saida.setCdIndicadorTipoManutencao(response.getCdIndicadorTipoManutencao());
		saida.setDsIndicadorTipoManutencao(response.getDsIndicadorTipoManutencao());
		saida.setVlAdicionalTed(response.getValorAdicionadoContratoTed());
		saida.setDtValidadeInicialTed(response.getDtInicioValidadeTed());
		saida.setDtValidadeFinalTed(response.getDtFimValidadeTed());
		saida.setDsEmpresaSegLinha(response.getDsEmpresaSegLinha());
		return saida;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.service.business.mantercontrato.
	 *      IManterContratoService#consultarConManParticipantes(br.com.bradesco.web.pgit.service.business.
	 *      mantercontrato.bean.ConsultarConManParticipantesEntradaDTO)
	 */
	public ConsultarConManParticipantesSaidaDTO consultarConManParticipantes(
			ConsultarConManParticipantesEntradaDTO entradaDTO) {

		ConsultarConManParticipantesRequest request = new ConsultarConManParticipantesRequest();

		request.setCdDependenciaOperacao(entradaDTO.getCdDependenciaOperacao());
		request.setCdPessoaJuridicaContrato(entradaDTO
				.getCdPessoaJuridicaContrato());
		request.setCdPessoaJuridicaParticipante(entradaDTO
				.getCdPessoaJuridicaParticipante());
		request.setCdTipoContratoNegocio(entradaDTO.getCdTipoContratoNegocio());
		request.setCdTipoParticipacaoPessoa(entradaDTO
				.getCdTipoParticipacaoPessoa());
		request.setHrInclusaoRegistroHistorico(entradaDTO
				.getHrInclusaoRegistroHistorico());
		request.setNrSequenciaContratoNegocio(entradaDTO
				.getNrSequenciaContratoNegocio());
		request.setCdusuario(entradaDTO.getCdUsuario());

		ConsultarConManParticipantesResponse response = factoryAdapter
				.getConsultarConManParticipantesPDCAdapter().invokeProcess(
						request);

		ConsultarConManParticipantesSaidaDTO saida = new ConsultarConManParticipantesSaidaDTO();
		saida.setCodMensagem(response.getCodMensagem());
		saida.setMensagem(response.getMensagem());
		saida.setCdAtividadeEconomica(response.getCdAtividadeEconomica());
		saida.setCdCanalManutencao(response.getCdCanalManutencao());
		saida.setCdControleCpfCnpj(response.getCdControleCpfCnpj());
		saida.setCdCpfCnpj(response.getCdCpfCnpj());
		saida.setCdFilialCpfCnpj(response.getCdFilialCpfCnpj());
		saida.setCdGrupoEconomico(response.getCdGrupoEconomico());
		saida.setCdIndicadorTipoManutencao(response
				.getCdIndicadorTipoManutencao());
		saida.setCdSegCliente(response.getCdSegCliente());
		saida.setCdSsgtoCliente(response.getCdSsgtoCliente());
		saida.setCdTipoParticipacao(response.getCdTipoParticipacao());
		saida.setCdUsuarioInclusao(response.getCdUsuarioInclusao());
		saida
				.setCpfFormatado(formatCpfCnpjCompleto(response.getCdCpfCnpj(),
						response.getCdFilialCpfCnpj(), response
								.getCdControleCpfCnpj()));
		saida.setDsAtividadeEconomica(response.getDsAtividadeEconomica());
		saida.setDsCanalInclusao(response.getDsCanalInclusao());
		saida.setDsCanalManutencao(response.getDsCanalManutencao());
		saida.setDsGrupoEconomico(response.getDsGrupoEconomico());
		saida.setDsIndicadorTipoManutencao(response
				.getDsIndicadorTipoManutencao());
		saida.setDsSegCliente(response.getDsSegCliente());
		saida.setDsSsgtoCliente(response.getDsSsgtoCliente());
		saida.setDsTipoParticipacao(response.getDsTipoParticipacao());
		saida.setHrInclusaoRegistro(FormatarData.formatarDataTrilha(response
				.getHrInclusaoRegistro()));
		saida.setNome(response.getNome());
		saida.setNrOperacaoFluxoManutencao(response
				.getNrOperacaoFluxoManutencao());
		saida.setCdUsuarioInclusaoExterno(response
				.getCdUsuarioInclusaoExterno());
		saida.setCdUsuarioManutencao(response.getCdUsuarioManutencao());
		saida.setCdCanalInclusao(response.getCdCanalInclusao());
		saida.setNrOperacaoFluxoInclusao(response.getNmOperacaoFluxoInclusao());
		saida.setHrManutencaoRegistro(FormatarData.formatarDataTrilha(response
				.getHrManutencaoRegistro()));
		saida.setCdSituacao(response.getCdSituacaoParticipanteContrato());
		saida.setDsSituacao(response.getDsSituacaoParticipanteContrato());
		saida.setCdMotivoSituacaoParticipante(response
				.getCdMotivoSituacaoParticipante());
		saida.setDsMotivoSituacaoParticipante(response
				.getDsMotivoSituacaoParticipante());
		saida.setCdClassificacaoAreaParticipante(response
				.getCdClassificacaoAreaParticipante());
		saida.setDsClassificacaoAreaParticipante(response
				.getDsClassificacaoAreaParticipante());
		return saida;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.service.business.mantercontrato.
	 *      IManterContratoService#listarConManParticipante(br.com.bradesco.web.pgit.service.business.mantercontrato.
	 *      bean.ListarConManParticipanteEntradaDTO)
	 */
	public List<ListarConManParticipanteSaidaDTO> listarConManParticipante(
			ListarConManParticipanteEntradaDTO entradaDTO) {

		ListarConManParticipanteRequest request = new ListarConManParticipanteRequest();
		request.setCdPessoa(entradaDTO.getCdPessoa());
		request.setCdPessoaJuridicaNegocio(entradaDTO
				.getCdPessoaJuridicaNegocio());
		request.setCdTipoContratoNegocio(entradaDTO.getCdTipoContratoNegocio());
		request.setCdTipoParticipacaoPessoa(entradaDTO
				.getCdTipoParticipacaoPessoa());
		request.setDtFim(entradaDTO.getDtFim());
		request.setDtInicio(entradaDTO.getDtInicio());
		request.setNrOcorrencias(30);
		request.setNrSequenciaContratoNegocio(entradaDTO
				.getNrSequenciaContratoNegocio());

		ListarConManParticipanteResponse response = getFactoryAdapter()
				.getListarConManParticipantePDCAdapter().invokeProcess(request);

		List<ListarConManParticipanteSaidaDTO> listaSaida = new ArrayList<ListarConManParticipanteSaidaDTO>();
		ListarConManParticipanteSaidaDTO saida;
		for (int i = 0; i < response.getOcorrenciasCount(); i++) {
			saida = new ListarConManParticipanteSaidaDTO();
			saida.setCdCpfCnpj(response.getOcorrencias(i).getCdCpfCnpj());
			saida.setCdUsuarioManutencao(response.getOcorrencias(i)
					.getCdUsuarioManutencao());
			saida.setCodMensagem(response.getCodMensagem());
			saida.setDtManutencao(response.getOcorrencias(i).getDtManutencao());
			saida.setHrInclusaoRegistroHistorico(response.getOcorrencias(i)
					.getHrInclusaoRegistroHistorico());
			saida.setHrManutencao(response.getOcorrencias(i).getHrManutencao());
			saida.setMensagem(response.getMensagem());
			saida.setNmParticipante(response.getOcorrencias(i)
					.getNmParticipante());
			saida.setNumeroConsultas(response.getNumeroConsultas());
			listaSaida.add(saida);
		}
		return listaSaida;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.service.business.mantercontrato.
	 *      IManterContratoService#consultarLayoutArquivoContrato(br.com.bradesco.web.pgit.service.business
	 *      .mantercontrato.bean.ConsultarLayoutArquivoContratoEntradaDTO)
	 */
	public ConsultarLayoutArquivoContratoSaidaDTO consultarLayoutArquivoContrato(
			ConsultarLayoutArquivoContratoEntradaDTO entradaDTO) {

		ConsultarLayoutArquivoContratoRequest request = new ConsultarLayoutArquivoContratoRequest();
		request.setCdPessoaJuridicaContrato(entradaDTO
				.getCdPessoaJuridicaContrato());
		request.setCdTipoContratoNegocio(entradaDTO.getCdTipoContratoNegocio());
		request.setCdTipoLayoutArquivo(entradaDTO.getCdTipoLayoutArquivo());
		request.setNrSequenciaContratoNegocio(entradaDTO
				.getNrSequenciaContratoNegocio());

		ConsultarLayoutArquivoContratoResponse response = getFactoryAdapter()
				.getConsultarLayoutArquivoContratoPDCAdapter().invokeProcess(
						request);

		ConsultarLayoutArquivoContratoSaidaDTO saidaDTO = new ConsultarLayoutArquivoContratoSaidaDTO();
		saidaDTO.setCodMensagem(response.getCodMensagem());
		saidaDTO.setMensagem(response.getMensagem());
		saidaDTO.setCdSituacaoLayoutContrato(response
				.getCdSituacaoLayoutContrato());
		saidaDTO.setDsSituacaoLayoutContrato(response
				.getDsSituacaoLayoutContrato());
		saidaDTO.setCdRespostaCustoEmpr(response.getCdRespostaCustoEmpr());
		saidaDTO.setDsRespostaCustoEmpr(response.getDsRespostaCustoEmpr());
		saidaDTO.setCdPercentualOrgnzTransmissao(response
				.getCdPercentualOrgnzTransmissao());
		saidaDTO
				.setCdMensagemLinhaExtrato(response.getCdMensagemLinhaExtrato());
		saidaDTO.setCdOperacaoCanalInclusao(response
				.getCdOperacaoCanalInclusao());
		saidaDTO.setCdOperacaoCanalManutencao(response
				.getCdOperacaoCanalManutencao());
		saidaDTO.setCdTipoCanalInclusao(response.getCdTipoCanalInclusao());
		saidaDTO.setCdTipoCanalManutencao(response.getCdTipoCanalManutencao());
		saidaDTO.setCdUsuarioInclusao(response.getCdUsuarioInclusao());
		saidaDTO.setCdUsuarioInclusaoExterno(response
				.getCdUsuarioInclusaoExterno());
		saidaDTO.setCdUsuarioManutencao(response.getCdUsuarioManutencao());
		saidaDTO.setCdUsuarioManutencaoExterno(response
				.getCdUsuarioManutencaoExterno());
		saidaDTO.setDsTipoCanalInclusao(response.getDsTipoCanalInclusao());
		saidaDTO.setDsTipoCanalManutencao(response.getDsTipoCanalManutencao());
		saidaDTO.setHrInclusaoRegistro(response.getHrInclusaoRegistro());
		saidaDTO.setHrManutencaoRegistro(response.getHrManutencaoRegistro());
		saidaDTO.setNrVersaoLayoutArquivo(response.getNrVersaoLayoutArquivo());
		saidaDTO.setCdTipoControlePagamento(response.getCdTipoControlePagamento());
		saidaDTO.setDsTipoControlePagamento(response.getDsTipoControlePagamento());
		return saidaDTO;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.service.business.mantercontrato.
	 *      IManterContratoService#alterarTipoRetornoLayout(br.com.bradesco.web.pgit.service.
	 *      business.mantercontrato.bean.AlterarTipoRetornoLayoutEntradaDTO)
	 */
	public AlterarTipoRetornoLayoutSaidaDTO alterarTipoRetornoLayout(
			AlterarTipoRetornoLayoutEntradaDTO entradaDTO) {

		AlterarTipoRetornoLayoutRequest request = new AlterarTipoRetornoLayoutRequest();
		request.setCdArquivoRetornoPagamento(entradaDTO
				.getCdArquivoRetornoPagamento());
		request.setCdPessoaJuridicaContrato(entradaDTO
				.getCdPessoaJuridicaContrato());
		request.setCdTipoClassificacaoRetorno(entradaDTO
				.getCdTipoClassificacaoRetorno());
		request.setCdTipoContratoNegocio(entradaDTO.getCdTipoContratoNegocio());
		request.setCdTipoGeracaoRetorno(entradaDTO.getCdTipoGeracaoRetorno());
		request.setCdTipoLayoutArquivo(entradaDTO.getCdTipoLayoutArquivo());
		request.setCdTipoMontaRetorno(entradaDTO.getCdTipoMontaRetorno());
		request.setCdTipoOcorrenciaRetorno(entradaDTO
				.getCdTipoOcorrenciaRetorno());
		request.setCdTipoOrdemRegistroRetorno(entradaDTO
				.getCdTipoOrdemRegistroRetorno());
		request.setNrSequenciaContratoNegocio(entradaDTO
				.getNrSequenciaContratoNegocio());

		AlterarTipoRetornoLayoutResponse response = getFactoryAdapter()
				.getAlterarTipoRetornoLayoutPDCAdapter().invokeProcess(request);

		AlterarTipoRetornoLayoutSaidaDTO saidaDTO = new AlterarTipoRetornoLayoutSaidaDTO();
		saidaDTO.setCodMensagem(response.getCodMensagem());
		saidaDTO.setMensagem(response.getMensagem());
		return saidaDTO;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.service.business.mantercontrato.
	 *      IManterContratoService#excluirTipoRetornoLayout(br.com.bradesco.web.pgit.service.business.mantercontrato.
	 *      bean.ExcluirTipoRetornoLayoutEntradaDTO)
	 */
	public ExcluirTipoRetornoLayoutSaidaDTO excluirTipoRetornoLayout(
			ExcluirTipoRetornoLayoutEntradaDTO entradaDTO) {

		ExcluirTipoRetornoLayoutRequest request = new ExcluirTipoRetornoLayoutRequest();
		request.setCdArquivoRetornoPagamento(entradaDTO
				.getCdArquivoRetornoPagamento());
		request.setCdPessoaJuridicaContrato(entradaDTO
				.getCdPessoaJuridicaContrato());
		request.setCdTipoContratoNegocio(entradaDTO.getCdTipoContratoNegocio());
		request.setCdTipoLayoutArquivo(entradaDTO.getCdTipoLayoutArquivo());
		request.setNrSequenciaContratoNegocio(entradaDTO
				.getNrSequenciaContratoNegocio());

		ExcluirTipoRetornoLayoutResponse response = getFactoryAdapter()
				.getExcluirTipoRetornoLayoutPDCAdapter().invokeProcess(request);

		ExcluirTipoRetornoLayoutSaidaDTO saidaDTO = new ExcluirTipoRetornoLayoutSaidaDTO();
		saidaDTO.setCodMensagem(response.getCodMensagem());
		saidaDTO.setMensagem(response.getMensagem());
		return saidaDTO;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.service.business.mantercontrato.
	 *      IManterContratoService#detalharTipoRetornoLayout(br.com.bradesco.web.pgit.service.business
	 *      .mantercontrato.bean.DetalharTipoRetornoLayoutEntradaDTO)
	 */
	public DetalharTipoRetornoLayoutSaidaDTO detalharTipoRetornoLayout(
			DetalharTipoRetornoLayoutEntradaDTO entradaDTO) {
		DetalharTipoRetornoLayoutRequest request = new DetalharTipoRetornoLayoutRequest();
		request.setCdPessoaJuridicaContrato(entradaDTO
				.getCdPessoaJuridicaContrato());
		request.setCdTipoArquivoRetorno(entradaDTO.getCdTipoArquivoRetorno());
		request.setCdTipoContratoNegocio(entradaDTO.getCdTipoContratoNegocio());
		request.setCdTipoLayoutArquivo(entradaDTO.getCdTipoLayoutArquivo());
		request.setNrSequenciaContratoNegocio(entradaDTO
				.getNrSequenciaContratoNegocio());

		DetalharTipoRetornoLayoutResponse response = getFactoryAdapter()
				.getDetalharTipoRetornoLayoutPDCAdapter()
				.invokeProcess(request);

		DetalharTipoRetornoLayoutSaidaDTO saidaDTO = new DetalharTipoRetornoLayoutSaidaDTO();
		saidaDTO.setCodMensagem(response.getCodMensagem());
		saidaDTO.setMensagem(response.getMensagem());
		saidaDTO.setCdTipoGeracaoRetorno(response.getCdTipoGeracaoRetorno());
		saidaDTO.setCdTipoOcorrenciaRetorno(response
				.getCdTipoOcorrenciaRetorno());
		saidaDTO.setCdTipoMontaRetorno(response.getCdTipoMontaRetorno());
		saidaDTO.setCdTipoClassificacaoRetorno(response
				.getCdTipoClassificacaoRetorno());
		saidaDTO.setCdTipoOrdemRegistroRetorno(response
				.getCdTipoOrdemRegistroRetorno());
		saidaDTO.setCdUsuarioInclusao(response.getCdUsuarioInclusao());
		saidaDTO
				.setCdUsuarioInclusaoExter(response.getCdUsuarioInclusaoExter());
		saidaDTO.setHrInclusaoRegistro(FormatarData.formatarDataTrilha(response
				.getHrInclusaoRegistro()));
		saidaDTO.setCdOperacaoCanalInclusao(response
				.getCdOperacaoCanalInclusao());
		saidaDTO.setCdTipoCanalInclusao(response.getCdTipoCanalInclusao());
		saidaDTO.setCdUsuarioManutencao(response.getCdUsuarioManutencao());
		saidaDTO.setCdUsuarioManutencaoExter(response
				.getCdUsuarioManutencaoExter());
		saidaDTO.setHrManutencaoRegistro(FormatarData
				.formatarDataTrilha(response.getHrManutencaoRegistro()));
		saidaDTO.setCdOperacaoCanalManutencao(response
				.getCdOperacaoCanalManutencao());
		saidaDTO.setCdTipoCanalManutencao(response.getCdTipoCanalManutencao());
		saidaDTO.setDsTipoGeracaoRetorno(response.getDsTipoGeracaoRetorno());
		saidaDTO.setDsTipoOcorrenciaRetorno(response
				.getDsTipoOcorrenciaRetorno());
		saidaDTO.setDsTipoMontaRetorno(response.getDsTipoMontaRetorno());
		saidaDTO.setDsTipoClassificacaoRetorno(response
				.getDsTipoClassificacaoRetorno());
		saidaDTO.setDsTipoOrdemRegistroRetorno(response
				.getDsTipoOrdemRegistroRetorno());
		saidaDTO.setDsTipoCanalInclusao(response.getDsTipoCanalInclusao());
		saidaDTO.setDsTipoCanalManutencao(response.getDsTipoCanalManutencao());
		return saidaDTO;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.service.business.mantercontrato.
	 *      IManterContratoService#listarServicoRelacionadoCdps(br.com.bradesco.web.pgit.service.business.
	 *      mantercontrato.bean.ListarServicoRelacionadoCdpsEntradaDTO)
	 */
	public List<ListarServicoRelacionadoCdpsSaidaDTO> listarServicoRelacionadoCdps(
			ListarServicoRelacionadoCdpsEntradaDTO entrada) {

		ListarServicoRelacionadoCdpsRequest request = new ListarServicoRelacionadoCdpsRequest();
		request.setCdProdutoServicoOperacao(entrada.getCdTipoServico());
		request.setCdProdutoOperacaoRelacionado(entrada.getCdModalidade());
		request
				.setCdRelacionamentoProduto(entrada
						.getCdRelacionamentoProduto());
		request.setNrOcorrencias(20);

		ListarServicoRelacionadoCdpsResponse response = getFactoryAdapter()
				.getListarServicoRelacionadoCdpsPDCAdapter().invokeProcess(
						request);

		ListarServicoRelacionadoCdpsSaidaDTO saida;
		List<ListarServicoRelacionadoCdpsSaidaDTO> lista = new ArrayList<ListarServicoRelacionadoCdpsSaidaDTO>();
		for (int i = 0; i < response.getOcorrenciasCount(); i++) {
			saida = new ListarServicoRelacionadoCdpsSaidaDTO();
			saida.setCodMensagem(response.getCodMensagem());
			saida.setMensagem(response.getMensagem());
			saida.setCdModalidade(response.getOcorrencias(i)
					.getCdProdutoOperacaoRelacionado());
			saida.setDsModalidade(response.getOcorrencias(i)
					.getDsProdutoOperacaoRelacionado());
			saida.setCdRelacionamentoProduto(response.getOcorrencias(i)
					.getCdRelacionamentoProduto());
			saida.setDsRelacionamentoProduto(response.getOcorrencias(i)
					.getDsRelacionamentoProduto());
			saida.setCdTipoServico(response.getOcorrencias(i)
					.getCdTipoProdutoServico());
			saida.setDsTipoServico(response.getOcorrencias(i)
					.getDsTipoProdutoServico());
			lista.add(saida);
		}
		return lista;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.service.business.mantercontrato.
	 *      IManterContratoService#listarConManDadosBasicos(br.com.bradesco.web.pgit.service.business
	 *      .mantercontrato.bean.ListarConManDadosBasicosEntradaDTO)
	 */
	public List<ListarConManDadosBasicosSaidaDTO> listarConManDadosBasicos(
			ListarConManDadosBasicosEntradaDTO entrada) {
		ListarConManDadosBasicosRequest request = new ListarConManDadosBasicosRequest();
		request
				.setCdPessoaJuridicaNegocio(entrada
						.getCdPessoaJuridicaNegocio());
		request.setCdTipoContratoNegocio(entrada.getCdTipoContratoNegocio());
		request.setDtFim(entrada.getDtFim());
		request.setDtInicio(entrada.getDtInicio());
		request.setNrOcorrencias(30);
		request.setNrSequenciaContratoNegocio(entrada
				.getNrSequenciaContratoNegocio());

		ListarConManDadosBasicosResponse response = getFactoryAdapter()
				.getListarConManDadosBasicosPDCAdapter().invokeProcess(request);

		List<ListarConManDadosBasicosSaidaDTO> lista = new ArrayList<ListarConManDadosBasicosSaidaDTO>();
		ListarConManDadosBasicosSaidaDTO dto;

		SimpleDateFormat formato1 = new SimpleDateFormat("dd.MM.yyyy");
		SimpleDateFormat formato2 = new SimpleDateFormat("dd/MM/yyyy");

		for (int i = 0; i < response.getOcorrenciasCount(); i++) {
			dto = new ListarConManDadosBasicosSaidaDTO();
			dto.setCdSituacao(response.getOcorrencias(i).getCdSituacao());
			dto.setCdUsuarioManutencao(response.getOcorrencias(i)
					.getCdUsuarioManutencao());
			dto.setCodMensagem(response.getCodMensagem());
			try {
				dto.setDtManutencao(String.valueOf(formato2.format(formato1
						.parse(response.getOcorrencias(i).getDtManutencao()))));
			} catch (ParseException e) {
				dto.setDtManutencao("");
			}
			dto.setHrInclusaoRegistroHistorico(response.getOcorrencias(i)
					.getHrInclusaoRegistroHistorico());
			dto.setHrManutencao(response.getOcorrencias(i).getHrManutencao());
			dto.setMensagem(response.getMensagem());
			dto.setDsIndicadorTipoManutencao(response.getOcorrencias(i)
					.getDsIndicadorTipoManutencao());
			lista.add(dto);
		}
		return lista;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.service.business.mantercontrato.
	 *      IManterContratoService#detalharTipoServModContrato(br.com.bradesco.web.pgit.service.business.
	 *      mantercontrato.bean.DetalharTipoServModContratoEntradaDTO)
	 */
	
	//forest
	public DetalharTipoServModContratoSaidaDTO detalharTipoServModContrato(DetalharTipoServModContratoEntradaDTO entrada) {
		DetalharTipoServModContratoRequest request = new DetalharTipoServModContratoRequest();
		
		request.setCdPessoaJuridicaContrato(entrada.getCdPessoaJuridicaContrato());
		request.setCdTipoContratoNegocio(entrada.getCdTipoContratoNegocio());
		request.setNrSequenciaContratoNegocio(entrada.getNrSequenciaContratoNegocio());
		request.setCdProdutoServicoOperacao(entrada.getCdServico());
		request.setCdProdutoOperacaoRelacionado(entrada.getCdModalidade());
		request.setCdParametro(entrada.getCdParametroPesquisa());
		
		DetalharTipoServModContratoResponse response = getFactoryAdapter().getDetalharTipoServModContratoPDCAdapter().invokeProcess(request);

		DetalharTipoServModContratoSaidaDTO saidaDTO = new DetalharTipoServModContratoSaidaDTO();
		saidaDTO.setCdMensagem(response.getCodMensagem());
		saidaDTO.setMensagem(response.getMensagem());

		saidaDTO.setQtDiaUtilPgto(response.getQtDiaUtilPgto());
		saidaDTO.setCdIndicadorUtilizaMora(response.getCdIndicadorUtilizaMora());
		saidaDTO.setDsIndicadorUtilizaMora(response.getDsIndicadorUtilizaMora());
		
		
		/************************************ TIPO DE SERVI�O ****************************************/
		// Pagamento de Fornecedores
		if (entrada.getCdParametroTela() == 1) {
			saidaDTO.setCdFormaEnvioPagamento(response
					.getCdFormaEnvioPagamento());
			saidaDTO.setDsFormaEnvioPagamento(response
					.getDsFormaEnvioPagamento());
			saidaDTO.setCdFormaAutorizacaoPagamento(response
					.getCdFormaAutorizacaoPagamento());
			saidaDTO.setDsFormaAutorizacaoPagamento(response
					.getDsFormaAutorizacaoPagamento());
			saidaDTO.setCdUtilizaPreAutorizacaoPagamentos(response
					.getCdIndicadorAutorizacaoCliente());
			saidaDTO.setDsUtilizaPreAutorizacaoPagamentos(response
					.getDsIndicadorAutorizacaoCliente());
			saidaDTO.setCdTipoControleFloating(response.getCdTipoDataFloat());
			saidaDTO.setDsTipoControleFloating(response.getDsTipoDataFloat());
			saidaDTO.setDsCodIndicadorTipoRetornoInternet(response.getDsIndicadorTipoRetornoInternet());
		}
		// Pagamento de Sal�rios
		if (entrada.getCdParametroTela() == 2) {
			saidaDTO.setEmiteCartaoAntecipadoContaSalario(response
					.getCdIndicadorCataoSalario());
			saidaDTO.setDsEmiteCartaoAntecipadoContaSalario(response
					.getDsIndicadorCataoSalario());
			saidaDTO.setCdTipoCartao(response.getCdTipoCataoSalario());
			saidaDTO.setDsTipoCartao(response.getDsTipoCataoSalario());
			saidaDTO.setQtdeLimiteSolicitacaoCartaoContaSalario(response
					.getQtLimiteSolicitacaoCatao());
			saidaDTO.setDtLimiteEnquadramentoConvenioContaSalario(response.getDtEnquaContaSalario());
			saidaDTO.setDsCodIndicadorTipoRetornoInternet(response.getDsIndicadorTipoRetornoInternet());
		}
		// Pagamentos de Tributos e Contas de Consumo
		if (entrada.getCdParametroTela() == 3) {
			saidaDTO.setCdUtilizacaoRastreamentoDebitoVeiculo(response
					.getCdConsDebitoVeiculo());
			saidaDTO.setDsUtilizacaoRastreamentoDebitoVeiculo(response
					.getDsConsDebitoVeiculo());
			saidaDTO.setCdPeriodicidadeRastreamentoDebitoVeiculo(response
					.getCdPerdcConsultaVeiculo());
			saidaDTO.setDsPeriodicidadeRastreamentoDebitoVeiculo(response
					.getDsPerdcConsultaVeiculo());
			saidaDTO.setCdAgendamentoDebitoVeiculoRastreamento(response
					.getCdAgendaDebitoVeiculo());
			saidaDTO.setDsAgendamentoDebitoVeiculoRastreamento(response
					.getDsAgendaDebitoVeiculo());
			saidaDTO.setCdPeriodicidadeCobrancaTarifa(response
					.getCdPerdcCobrancaTarifa());
			saidaDTO.setDsPeriodicidadeCobrancaTarifa(response
					.getDsPerdcCobrancaTarifa());
			saidaDTO.setDiaFechamentoApuracaoTarifaMensal(response
					.getNrFechamentoApuracaoTarifa());
			saidaDTO.setQtdeDiasCobrancaTarifaAposApuracao(response
					.getQtDiaCobrancaTarifa());
			saidaDTO
					.setCdTipoReajusteTarifa(response.getCdTipoReajusteTarifa());
			saidaDTO.setDsTipoReajusteTarifa(response.getDsTipoReajusteTarifa());
			saidaDTO.setQtdeMesesReajusteAutomaticoTarifa(response.getQtMesReajusteTarifa());
			saidaDTO.setCdIndiceEconomicoReajuste(response.getCdPercentualIndicadorReajusteTarifa());
			saidaDTO.setCdTipoAlimentacaoPagamentos(response.getCdFormaEnvioPagamento());
			saidaDTO.setCdTipoAutorizacaoPagamento(response.getCdFormaAutorizacaoPagamento());
			saidaDTO.setDsTipoAutorizacaoPagamento(response.getDsFormaAutorizacaoPagamento());
			saidaDTO.setDsCodIndicadorTipoRetornoInternet(response.getDsIndicadorTipoRetornoInternet());
		}
		// Pagamento de Benef�cios
		if (entrada.getCdParametroTela() == 4) {
			saidaDTO.setCdInformaAgenciaContaCredito(response
					.getCdDispzContaCredito());
			saidaDTO.setDsInformaAgenciaContaCredito(response
					.getDsDispzContaCredito());
			saidaDTO.setCdTipoIdentificacaoBeneficio(response
					.getCdTipoIdBeneficio());
			saidaDTO.setDsTipoIdentificacaoBeneficio(response
					.getDsTipoIdBeneficio());
			saidaDTO.setCdUtilizaCadastroOrgaosPagadores(response
					.getCdIndicadorCadastroOrg());
			saidaDTO.setDsUtilizaCadastroOrgaosPagadores(response
					.getDsIndicadorCadastroOrg());
			saidaDTO.setCdMantemCadastroProcuradores(response
					.getCdIndicadorCadastroProcd());
			saidaDTO.setDsMantemCadastroProcuradores(response
					.getDsIndicadorCadastroProcd());
			saidaDTO.setCdPeriodicidadeManutencaoCadastroProcurador(response
					.getCdPerdcManutencaoProcd());
			saidaDTO.setDsPeriodicidadeManutencaoCadastroProcurador(response
					.getDsPerdcManutencaoProcd());
			saidaDTO.setCdFormaManutencaoCadastroProcurador(response
					.getCdFormaManutencao());
			saidaDTO.setDsFormaManutencaoCadastroProcurador(response
					.getDsFormaManutencao());
			saidaDTO.setCdPossuiExpiracaoCredito(response
					.getCdIndicadorExpiracaoCredito());
			saidaDTO.setDsPossuiExpiracaoCredito(response
					.getDsIndicadorExpiracaoCredito());
			saidaDTO.setCdFormaControleExpiracaoCredito(response
					.getCdFormaExpiracaoCredito());
			saidaDTO.setDsFormaControleExpiracaoCredito(response
					.getDsFormaExpiracaoCredito());
			saidaDTO.setQtdeDiasExpiracaoCredito(response.getQtDiaExpiracao());
			saidaDTO.setCdTratamentoFeriadoFimVigencia(response
					.getCdCreditoNaoUtilizado());
			saidaDTO.setDsTratamentoFeriadoFimVigencia(response
					.getDsCreditoNaoUtilizado());
			saidaDTO.setCdTipoPrestacaoContaCreditoPago(response
					.getCdMomentoCreditoEfetivacao());
			saidaDTO.setDsTipoPrestacaoContaCreditoPago(response
					.getDsMomentoCreditoEfetivacao());
		}
		// Cadastro de Favorecido
		if (entrada.getCdParametroTela() == 5) {
			saidaDTO.setCdFormaManutencaoCadastro(response
					.getCdFormaManutencao());
			saidaDTO.setDsFormaManutencaoCadastro(response
					.getDsFormaManutencao());
			saidaDTO.setQtdeDiasInativacaoFavorecido(response
					.getQtDiaInatividadeFavorecido());
			saidaDTO.setCdConsistenciaCpfCnpjFavorecido(response
					.getCdCtciaInscricaoFavorecido());
			saidaDTO.setDsConsistenciaCpfCnpjFavorecido(response
					.getDsCtciaInscricaoFavorecido());
			saidaDTO.setDsTipoConsistenciaFavorecido(response
					.getDsCtciaInscricaoFavorecido());
		}
		// Aviso de Movimenta��o de D�bito
		// Aviso de Movimenta��o de Cr�dito
		// Aviso de Movimenta��o ao Pagador
		// Aviso de Movimenta��o ao Favorecido
		if (entrada.getCdParametroTela() == 6
				|| entrada.getCdParametroTela() == 7
				|| entrada.getCdParametroTela() == 8
				|| entrada.getCdParametroTela() == 9) {
			saidaDTO.setCdPeriodicidade(response.getCdPeriodicidadeAviso());
			saidaDTO.setDsPeriodicidade(response.getDsPeriodicidadeAviso());
			saidaDTO.setCdDestino(response.getCdDestinoAviso());
			saidaDTO.setCdPermiteCorrespondenciaAberta(response
					.getCdEnvelopeAberto());
			saidaDTO.setCdDemonstraInformacoesAreaReservada(response
					.getCdAreaReservada());
			saidaDTO.setCdAgruparCorrespondencia(response
					.getCdAgrupamentoAviso());
			saidaDTO.setQtdeVias(response.getQtViaAviso());
		}
		// Comprovante Pagamento ao Pagador
		// Comprovante Pagamento ao Favorecido
		if (entrada.getCdParametroTela() == 10
				|| entrada.getCdParametroTela() == 11) {
			saidaDTO.setCdPeriodicidade(response.getCdPerdcComprovante());
			saidaDTO.setDsPeriodicidade(response.getDsPerdcComprovante());
			saidaDTO.setCdDestino(response.getCdDestinoComprovante());
			saidaDTO.setCdPermiteCorrespondenciaAberta(response
					.getCdEnvelopeAberto());
			saidaDTO.setCdDemonstraInformacoesAreaReservada(response
					.getCdAreaReservada());
			saidaDTO.setCdAgruparCorrespondencia(response
					.getCdAgrupamentoComprovado());
			saidaDTO.setQtdeVias(response.getQtViaComprovante());
		}
		// Emiss�o Comprovantes Diversos
		if (entrada.getCdParametroTela() == 12) {
			saidaDTO.setCdTipoRejeicao(response.getCdRejeicaoLote());
			saidaDTO.setCdTipoEmissao(response.getCdDispzDiversarCrrtt());
			saidaDTO.setCdMidiaDisponivelCorrentista(response
					.getCdMidiaDisponivel());
			saidaDTO.setCdMidiaDisponivelNaoCorrentista(response
					.getCdDispzDiversasNao());
			saidaDTO.setCdTipoEnvioCorrespondencia(response
					.getCdDestinoComprovante());
			saidaDTO.setCdFrasesPreCadastratadas(response
					.getCdFrasePreCadastro());
			saidaDTO.setCdCobrarTarifasFuncionarios(response
					.getCdCobrancaTarifa());
			saidaDTO.setQtdeMesesEmissao(response.getQtMesComprovante());
			saidaDTO.setQtdeLinhas(response.getQtLimiteLinha());
			saidaDTO.setQtdeViasEmissaoComprovantes(response
					.getQtViaComprovante());
			saidaDTO.setQtdeViasPagas(response.getQtViaCobranca());
			saidaDTO.setCdFormularioParaImpressao(response
					.getCdTipoLayoutArquivo());
			saidaDTO
					.setCdPeriodicidadeCobrancaTarifaEmissaoComprovantes(response
							.getCdPerdcCobrancaTarifa());
			saidaDTO
					.setDsPeriodicidadeCobrancaTarifaEmissaoComprovantes(response
							.getDsPerdcCobrancaTarifa());
			saidaDTO.setCdFechamentoApuracao(response
					.getNrFechamentoApuracaoTarifa());
			saidaDTO.setQtdeDiasCobrancaAposApuracao(response
					.getQtDiaCobrancaTarifa());
			saidaDTO.setCdTipoReajuste(response.getCdTipoReajusteTarifa());
			saidaDTO.setQtdeMesesReajusteAutomatico(response
					.getQtMesReajusteTarifa());
			saidaDTO.setCdIndiceReajuste(response
					.getCdIndicadorEconomicoReajuste());
			saidaDTO.setPercentualReajuste(response.getCdPercentualIndicadorReajusteTarifa());
			saidaDTO.setPorcentagemBonificacaoTarifa(response.getCdPercentualReducaoTarifaCatalogo());
		}
		// Emiss�o Comprovante Salarial
		if (entrada.getCdParametroTela() == 13) {
			saidaDTO.setCdTipoRejeicao(response.getCdRejeicaoLote());
			saidaDTO.setCdTipoEmissao(response.getCdDispzSalarioCrrtt());
			saidaDTO.setCdMidiaDisponivelCorrentista(response
					.getCdMidiaDisponivel());
			saidaDTO.setCdMidiaDisponivelNaoCorrentista(response
					.getCdDispzSalarioNao());
			saidaDTO.setCdTipoEnvioCorrespondencia(response
					.getCdDestinoComprovante());
			saidaDTO.setCdFrasesPreCadastratadas(response
					.getCdFrasePreCadastro());
			saidaDTO.setCdCobrarTarifasFuncionarios(response
					.getCdCobrancaTarifa());
			saidaDTO.setQtdeMesesEmissao(response.getQtMesComprovante());
			saidaDTO.setQtdeLinhas(response.getQtLimiteLinha());
			saidaDTO.setQtdeViasEmissaoComprovantes(response
					.getQtViaComprovante());
			saidaDTO.setQtdeViasPagas(response.getQtViaCobranca());
			saidaDTO.setCdFormularioParaImpressao(response
					.getCdTipoLayoutArquivo());
			saidaDTO
					.setCdPeriodicidadeCobrancaTarifaEmissaoComprovantes(response
							.getCdPerdcCobrancaTarifa());
			saidaDTO
					.setDsPeriodicidadeCobrancaTarifaEmissaoComprovantes(response
							.getDsPerdcCobrancaTarifa());
			saidaDTO.setCdFechamentoApuracao(response
					.getNrFechamentoApuracaoTarifa());
			saidaDTO.setQtdeDiasCobrancaAposApuracao(response
					.getQtDiaCobrancaTarifa());
			saidaDTO.setCdTipoReajuste(response.getCdTipoReajusteTarifa());
			saidaDTO.setQtdeMesesReajusteAutomatico(response
					.getQtMesReajusteTarifa());
			saidaDTO.setCdIndiceReajuste(response
					.getCdIndicadorEconomicoReajuste());
			saidaDTO.setPercentualReajuste(response.getCdPercentualIndicadorReajusteTarifa());
			saidaDTO.setPorcentagemBonificacaoTarifa(response.getCdPercentualReducaoTarifaCatalogo());
		}
		// Recadastramento de Benefici�rio
		if (entrada.getCdParametroTela() == 14) {
			saidaDTO.setCdTipoIdentificacaoFuncionario(response
					.getCdTipoIdBeneficio());
			saidaDTO.setDsTipoIdentificacaoFuncionario(response
					.getDsTipoIdBeneficio());
			saidaDTO.setCdTipoConsistenciaIdentificador(response
					.getCdCtciaIdentificacaoBeneficio());
			saidaDTO.setDsTipoConsistenciaIdentificador(response
					.getDsCtciaIdentificacaoBeneficio());
			saidaDTO.setCdCondicaoEnquadramento(response
					.getCdCriterioEnquaBeneficio());
			saidaDTO.setDsCondicaoEnquadramento(response
					.getDsCriterioEnquaBeneficio());
			saidaDTO.setCdCriterioPrincipalEnquadramento(response
					.getCdPrincipalEnquaRecadastro());
			saidaDTO.setDsCriterioPrincipalEnquadramento(response
					.getDsPrincipalEnquaRecadastro());
			saidaDTO.setDsCriterioCompostoEnquadramento(response
					.getDsCriterioEnquaRecadastro());
			saidaDTO.setCdCriterioCompostoEnquadramento(response
					.getCdCriterioEnquaRecadastro());
			saidaDTO.setQtdeEtapasRecadastramento(response
					.getQtEtapasRecadastroBeneficio());
			saidaDTO.setQtdeFasesPorEtapa(response
					.getQtFaseRecadastroBeneficio());
			saidaDTO
					.setQtdeMesesPrazoPorFase(response.getQtMesFaseRecadastro());
			saidaDTO.setQtdeMesesPrazoPorEtapa(response
					.getQtMesEtapaRecadastro());
			saidaDTO.setCdPermiteEnvioRemessaManutencao(response
					.getCdManutencaoBaseRecadastro());
			saidaDTO.setCdPeriodicidadeEnvioRemessa(response
					.getCdPerdcEnvioRemessa());
			saidaDTO.setDsPeriodicidadeEnvioRemessa(response
					.getDsPerdcEnvioRemessa());
			saidaDTO.setCdBaseDadosUtilizada(response
					.getCdBaseRecadastroBeneficio());
			saidaDTO.setCdTipoCargaBaseDadosUtilizada(response
					.getCdTipoCargaRecadastro());
			saidaDTO.setCdPermiteAnteciparRecadastramento(response
					.getCdAntecRecadastroBeneficio());
			saidaDTO.setDtLimiteInicioVinculoCargaBase(response
					.getDtLimiteVinculoCarga());
			saidaDTO.setDtInicioRecadastramento(response
					.getDtInicioRecadastroBeneficio());
			saidaDTO.setDtFimRecadastramento(response
					.getDtFimRecadastroBeneficio());
			saidaDTO.setCdPermiteAcertosDados(response
					.getCdAcertoDadoRecadastro());
			saidaDTO.setDtInicioPeriodoAcertoDados(response
					.getDtinicioAcertoRecadastro());
			saidaDTO.setDtFimPeriodoAcertoDados(response
					.getDtFimAcertoRecadastro());
			saidaDTO.setCdEmiteMensagemRecadastramentoMidiaOnLine(response
					.getCdMensagemRecadastroMidia());
			saidaDTO.setCdMidiaMensagemRecastramentoOnLine(response
					.getCdMidiaMensagemRecadastro());
			saidaDTO.setCdPeriodicidadeCobrancaTarifa(response
					.getCdPerdcCobrancaTarifa());
			saidaDTO.setDiaFechamentoApuracaoTarifaMensal(response
					.getNrFechamentoApuracaoTarifa());
			saidaDTO.setQtdeDiasCobrancaTarifaAposApuracao(response
					.getQtDiaCobrancaTarifa());
			saidaDTO
					.setCdTipoReajusteTarifa(response.getCdTipoReajusteTarifa());
			saidaDTO.setQtdeMesesReajusteAutomaticoTarifa(response
					.getQtMesReajusteTarifa());
			saidaDTO.setCdIndiceEconomicoReajusteTarifa(response
					.getCdIndicadorEconomicoReajuste());
			saidaDTO.setDsIndiceEconomicoReajusteTarifa(response
					.getDsIndicadorEconomicoReajuste());
			saidaDTO.setPercentualIndiceEconomicoReajusteTarifa(response.getCdPercentualIndicadorReajusteTarifa());
			saidaDTO.setPercentualBonificacaoTarifaPadrao(response.getCdPercentualReducaoTarifaCatalogo());
		}

		/**************************************** MODALIDADE *****************************************/
		// Aviso de Recadastramento
		if (entrada.getCdParametroTela() == 15) {
			saidaDTO.setCdPeriocidadeEnvio(response
					.getCdMomentoAvisoRacadastro());
			saidaDTO.setDsPeriocidadeEnvio(response
					.getDsMomentoAvisoRacadastro());
			saidaDTO.setCdEnderecoEnvio(response.getCdConsEndereco());
			saidaDTO.setDsEnderecoEnvio(response.getDsConsEndereco());
			saidaDTO.setQntDiasAntecipacao(response.getQtAntecedencia());
			saidaDTO.setCdDestino(response.getCdDestinoAviso());
			saidaDTO.setDsDestino(response.getDsDestinoAviso());
			saidaDTO.setCdPermitirAgrupamento(response.getCdAgrupamentoAviso());
			saidaDTO.setDsPermitirAgrupamento(response.getDsAgrupamentoAviso());
		}
		// Formul�rio de Recadastramento
		if (entrada.getCdParametroTela() == 16) {
			saidaDTO.setCdPeriocidadeEnvio(response
					.getCdMomentoFormularioRecadastro());
			saidaDTO.setDsPeriocidadeEnvio(response
					.getDsMomentoFormularioRecadastro());
			saidaDTO.setCdEnderecoEnvio(response.getCdConsEndereco());
			saidaDTO.setDsEnderecoEnvio(response.getDsConsEndereco());
			saidaDTO.setQntDiasAntecipacao(response.getQtAntecedencia());
			saidaDTO.setCdDestino(response.getCdDestinoFormularioRecadastro());
			saidaDTO.setDsDestino(response.getDsDestinoFormularioRecadastro());
			saidaDTO.setCdPermitirAgrupamento(response
					.getCdAgrupamentoFormularioRecadastro());
			saidaDTO.setDsPermitirAgrupamento(response
					.getDsAgrupamentoFormularioRecadastro());
		}
		// Pagamento de Fornecedor - TED
		// Pagamento de Fornecedor - DOC
		if (entrada.getCdParametroTela() == 17
				|| entrada.getCdParametroTela() == 18) {
			saidaDTO.setCdTipoConsultaSaldo(response.getCdConsSaldoPagamento());
			saidaDTO.setDsTipoConsultaSaldo(response.getDsConsSaldoPagamento());
			saidaDTO.setRdoUtilizaCadFavControlePag(response
					.getCdUtilizacaoFavorecidoControle());
			saidaDTO.setCdTratamentoFeriadosDtaPag(response
					.getCdPagamentoNaoUtilizado());
			saidaDTO.setDsTratamentoFeriadosDtaPag(response
					.getDsPagamentoNaoUtilizado());
			saidaDTO.setCdPrioridadeDebito(response
					.getCdPrioridadeEfetivacaoPagamento());
			saidaDTO.setDsPrioridadeDebito(response
					.getDsPrioridadeEfetivacaoPagamento());
			saidaDTO.setRdoGerarLanctoFuturoCred(response
					.getCdLancamentoFuturoCredito());
			saidaDTO.setValorLimiteIndividual(response.getVlLimiteIndividualPagamento());
			saidaDTO.setQuantDiasRepique(response.getQtDiaRepiqConsulta());
			saidaDTO.setValorMaxPagFavNaoCadastrado(response.getVlFavorecidoNaoCadastro());
			saidaDTO.setCdTipoProcessamento(response
					.getCdMomentoProcessamentoPagamento());
			saidaDTO.setDsTipoProcessamento(response
					.getDsMomentoProcessamentoPagamento());
			saidaDTO.setCdTipoRejeicaoEfetivacao(response
					.getCdRejeicaoEfetivacaoLote());
			saidaDTO.setDsTipoRejeicaoEfetivacao(response
					.getDsRejeicaoEfetivacaoLote());
			saidaDTO.setCdTipoRejeicaoAgendamento(response
					.getCdRejeicaoAgendaLote());
			saidaDTO.setDsTipoRejeicaoAgendamento(response
					.getDsRejeicaoAgendaLote());
			saidaDTO.setRdoGerarLanctoFuturoDeb(response
					.getCdLancamentoFuturoDebito());
			saidaDTO.setRdoPermiteFavConsultarPag(response
					.getCdFavorecidoConsPagamento());
			saidaDTO.setValorLimiteDiario(response.getVlLimiteDiaPagamento());
					
			saidaDTO.setDsOrigemIndicador(response.getDsOrigemIndicador());
			saidaDTO.setDsIndicador(response.getDsIndicador());
			
			saidaDTO.setDsOrigemIndicador(response.getDsOrigemIndicador());
			saidaDTO.setDsIndicador(response.getDsIndicador());
			
		}
		// Pagamento Via Ordem de Pagamento
		if (entrada.getCdParametroTela() == 19) {
			saidaDTO.setCdTipoConsultaSaldo(response.getCdConsSaldoPagamento());
			saidaDTO.setDsTipoConsultaSaldo(response.getDsConsSaldoPagamento());
			saidaDTO.setRdoUtilizaCadFavControlePag(response
					.getCdUtilizacaoFavorecidoControle());
			saidaDTO.setCdTipoRejeicaoAgendamento(response
					.getCdRejeicaoAgendaLote());
			saidaDTO.setDsTipoRejeicaoAgendamento(response
					.getDsRejeicaoAgendaLote());
			saidaDTO.setCdTipoRejeicaoEfetivacao(response
					.getCdRejeicaoEfetivacaoLote());
			saidaDTO.setDsTipoRejeicaoEfetivacao(response
					.getDsRejeicaoEfetivacaoLote());
			saidaDTO.setValorLimiteIndividual(response.getVlLimiteIndividualPagamento());
			saidaDTO.setRdoPermiteFavConsultarPag(response
					.getCdFavorecidoConsPagamento());
			saidaDTO.setQuantDiasRepique(response.getQtDiaRepiqConsulta());
			saidaDTO.setValorMaxPagFavNaoCadastrado(response.getVlFavorecidoNaoCadastro());
			saidaDTO.setCdTipoProcessamento(response
					.getCdMomentoProcessamentoPagamento());
			saidaDTO.setDsTipoProcessamento(response
					.getDsMomentoProcessamentoPagamento());
			saidaDTO.setValorLimiteDiario(response.getVlLimiteDiaPagamento());
			saidaDTO.setRdoPermitePagarVencido(response
					.getCdAgendaPagamentoVencido());
			saidaDTO.setRdoPermitePagarMenor(response.getCdAgendaValorMenor());
			saidaDTO.setQntdLimiteDiasPagVencido(response
					.getQtMaximaTituloVencido());
			saidaDTO.setCdPrioridadeDebito(response
					.getCdPrioridadeEfetivacaoPagamento());
			saidaDTO.setDsPrioridadeDebito(response
					.getDsPrioridadeEfetivacaoPagamento());
			saidaDTO.setRdoGerarLanctoFuturoDeb(response
					.getCdLancamentoFuturoDebito());
			saidaDTO.setCdTratamentoFeriadosDtaPag(response
					.getCdPagamentoNaoUtilizado());
			saidaDTO.setDsTratamentoFeriadosDtaPag(response
					.getDsPagamentoNaoUtilizado());
			saidaDTO.setRdoOcorrenciaDebito(response
					.getCdMomentoDebitoPagamento());
			saidaDTO.setQntdDiasExpiracao(response.getQtDiaExpiracao());
			saidaDTO.setRdoPermiteFavConsultarPag(response
					.getCdFavorecidoConsPagamento());
			saidaDTO.setRdoGerarLanctoFuturoCred(response
					.getCdLancamentoFuturoCredito());
			saidaDTO.setCdOcorrenciaDebito(response
					.getCdMomentoDebitoPagamento());
			saidaDTO.setDsOcorrenciaDebito(response
					.getDsMomentoDebitoPagamento());
			saidaDTO.setDsLocalEmissao(response.getDsLocalEmissao());
		}
		// Pagamento Via Cr�dito em Conta
		if (entrada.getCdParametroTela() == 20) {
			saidaDTO.setCdTipoConsultaSaldo(response.getCdConsSaldoPagamento());
			saidaDTO.setDsTipoConsultaSaldo(response.getDsConsSaldoPagamento());
			saidaDTO.setRdoUtilizaCadFavControlePag(response
					.getCdUtilizacaoFavorecidoControle());
			saidaDTO.setCdTipoRejeicaoAgendamento(response
					.getCdRejeicaoAgendaLote());
			saidaDTO.setDsTipoRejeicaoAgendamento(response
					.getDsRejeicaoAgendaLote());
			saidaDTO.setValorLimiteIndividual(response.getVlLimiteIndividualPagamento());
			saidaDTO.setRdoPermiteFavConsultarPag(response
					.getCdFavorecidoConsPagamento());
			saidaDTO.setQuantDiasRepique(response.getQtDiaRepiqConsulta());
			saidaDTO.setValorMaxPagFavNaoCadastrado(response.getVlFavorecidoNaoCadastro());
			saidaDTO.setCdTipoProcessamento(response
					.getCdMomentoProcessamentoPagamento());
			saidaDTO.setDsTipoProcessamento(response
					.getDsMomentoProcessamentoPagamento());
			saidaDTO.setCdTipoRejeicaoEfetivacao(response
					.getCdRejeicaoEfetivacaoLote());
			saidaDTO.setDsTipoRejeicaoEfetivacao(response
					.getDsRejeicaoEfetivacaoLote());
			saidaDTO.setValorLimiteDiario(response.getVlLimiteDiaPagamento());
			saidaDTO.setRdoGerarLanctoFuturoCred(response
					.getCdLancamentoFuturoCredito());
			saidaDTO.setRdoGerarLanctoFuturoDeb(response
					.getCdLancamentoFuturoDebito());
			saidaDTO.setRdoPermitePagarVencido(response
					.getCdAgendaPagamentoVencido());
			saidaDTO.setRdoPermitePagarMenor(response.getCdAgendaValorMenor());
			saidaDTO.setQntdLimiteDiasPagVencido(response
					.getQtMaximaTituloVencido());
			saidaDTO.setRdoGerarLanctoProgramado(response
					.getCdIndicadorLancamentoPagamento());
			saidaDTO.setCdPrioridadeDebito(response
					.getCdPrioridadeEfetivacaoPagamento());
			saidaDTO.setDsPrioridadeDebito(response
					.getDsPrioridadeEfetivacaoPagamento());
			saidaDTO.setCdEfetuaConsistencia(response
					.getCdCtciaEspecieBeneficio());
			saidaDTO.setDsEfetuaConsistencia(response
					.getDsCtciaEspecieBeneficio());
			saidaDTO.setCdTipoTratamentoContasTransferidas(response
					.getCdContratoContaTransferencia());
			saidaDTO.setDsTipoTratamentoContasTransferidas(response
					.getDsContratoContaTransferencia());
			saidaDTO.setQntdMaxRegInconsistentes(response
					.getQtMaximaInconLote());
			saidaDTO.setPercentualMaxRegInconsistenciaLote(response
					.getCdPercentualMaximoInconLote());
			saidaDTO.setCdDiaFloatPagamento(response.getCdDiaFloatPagamento());
			saidaDTO.setCdMomentoProcessamentoPagamento(response
					.getCdMomentoProcessamentoPagamento());
			saidaDTO
					.setCdRejeicaoAgendaLote(response.getCdRejeicaoAgendaLote());
			saidaDTO.setCdRejeicaoEfetivacaoLote(response
					.getCdRejeicaoEfetivacaoLote());
			saidaDTO.setQtMaximaInconLote(response.getQtMaximaInconLote());
			saidaDTO.setCdPercentualMaximoInconLote(response
					.getCdPercentualMaximoInconLote());
			saidaDTO.setCdPrioridadeEfetivacaoPagamento(response
					.getCdPrioridadeEfetivacaoPagamento());
			saidaDTO.setCdUtilizacaoFavorecidoControle(response
					.getCdUtilizacaoFavorecidoControle());
			saidaDTO.setVlFavorecidoNaoCadastro(response.getVlFavorecidoNaoCadastro());
			saidaDTO
					.setCdConsSaldoPagamento(response.getCdConsSaldoPagamento());
			saidaDTO.setQtDiaRepiqConsulta(response.getQtDiaRepiqConsulta());
			saidaDTO.setCdFavorecidoConsPagamento(response
					.getCdFavorecidoConsPagamento());
			saidaDTO.setCdCtciaEspecieBeneficio(response
					.getCdCtciaEspecieBeneficio());
			saidaDTO.setCdCtciaInscricaoFavorecido(response
					.getCdCtciaInscricaoFavorecido());
			saidaDTO.setCdLancamentoFuturoDebito(response
					.getCdLancamentoFuturoDebito());
			saidaDTO.setCdLancamentoFuturoCredito(response
					.getCdLancamentoFuturoCredito());
			saidaDTO.setCdIndicadorLancamentoPagamento(response
					.getCdIndicadorLancamentoPagamento());
			saidaDTO.setCdPagamentoNaoUtilizado(response
					.getCdPagamentoNaoUtilizado());
			saidaDTO.setCdContratoContaTransferencia(response
					.getCdContratoContaTransferencia());
			saidaDTO.setCdValidacaoNomeFavorecido(response
					.getCdValidacaoNomeFavorecido());
			saidaDTO.setCdLiberacaoLoteProcesso(response
					.getCdLiberacaoLoteProcesso());
			saidaDTO
					.setVlLimiteDiaPagamento(response.getVlLimiteDiaPagamento());
			saidaDTO.setVlLimiteIndividualPagamento(response
					.getVlLimiteIndividualPagamento());
			saidaDTO.setCdMeioPagamentoCredito(response
					.getCdMeioPagamentoCredito());
		}
		// Pagamento Via D�bito em Conta
		if (entrada.getCdParametroTela() == 21) {
			saidaDTO.setCdTipoConsultaSaldo(response.getCdConsSaldoPagamento());
			saidaDTO.setDsTipoConsultaSaldo(response.getDsConsSaldoPagamento());
			saidaDTO.setRdoUtilizaCadFavControlePag(response
					.getCdUtilizacaoFavorecidoControle());
			saidaDTO.setCdTratamentoFeriadosDtaPag(response
					.getCdPagamentoNaoUtilizado());
			saidaDTO.setDsTratamentoFeriadosDtaPag(response
					.getDsPagamentoNaoUtilizado());
			saidaDTO.setCdTipoRejeicaoAgendamento(response
					.getCdRejeicaoAgendaLote());
			saidaDTO.setDsTipoRejeicaoAgendamento(response
					.getDsRejeicaoAgendaLote());
			saidaDTO.setValorLimiteIndividual(response.getVlLimiteIndividualPagamento());
			saidaDTO.setQuantDiasRepique(response.getQtDiaRepiqConsulta());
			saidaDTO.setValorMaximoPagamentoFavorecidoNaoCadastrado(response.getVlFavorecidoNaoCadastro());
			saidaDTO.setCdTipoProcessamento(response
					.getCdMomentoProcessamentoPagamento());
			saidaDTO.setDsTipoProcessamento(response
					.getDsMomentoProcessamentoPagamento());
			saidaDTO.setCdTipoRejeicaoEfetivacao(response
					.getCdRejeicaoEfetivacaoLote());
			saidaDTO.setDsTipoRejeicaoEfetivacao(response
					.getDsRejeicaoEfetivacaoLote());
			saidaDTO.setValorLimiteDiario(response.getVlLimiteDiaPagamento());
			saidaDTO.setRdoGerarLanctoFuturoDeb(response
					.getCdLancamentoFuturoDebito());
			saidaDTO.setRdoGerarLanctoFuturoCred(response
					.getCdLancamentoFuturoCredito());
			saidaDTO.setCdPrioridadeDebito(response
					.getCdPrioridadeEfetivacaoPagamento());
			saidaDTO.setDsPrioridadeDebito(response
					.getDsPrioridadeEfetivacaoPagamento());
			saidaDTO.setRdoPermiteDebitoOnline(response
					.getCdPermissaoDebitoOnline());
			saidaDTO.setValorMaxPagFavNaoCadastrado(response.getVlFavorecidoNaoCadastro());
		}
		// Pagamento Via T�tulo Bradesco
		// Pagamento Via T�tulo Outros Bancos
		if (entrada.getCdParametroTela() == 22
				|| entrada.getCdParametroTela() == 23) {
			saidaDTO.setCdTipoConsultaSaldo(response.getCdConsSaldoPagamento());
			saidaDTO.setDsTipoConsultaSaldo(response.getDsConsSaldoPagamento());
			saidaDTO.setRdoUtilizaCadFavControlePag(response
					.getCdUtilizacaoFavorecidoControle());
			saidaDTO.setCdTratamentoFeriadosDtaPag(response
					.getCdPagamentoNaoUtilizado());
			saidaDTO.setDsTratamentoFeriadosDtaPag(response
					.getDsPagamentoNaoUtilizado());
			saidaDTO.setCdTipoRejeicaoAgendamento(response
					.getCdRejeicaoAgendaLote());
			saidaDTO.setDsTipoRejeicaoAgendamento(response
					.getDsRejeicaoAgendaLote());
			saidaDTO.setValorLimiteIndividual(response.getVlLimiteIndividualPagamento());
			saidaDTO.setRdoPermiteFavConsultarPag(response
					.getCdFavorecidoConsPagamento());
			saidaDTO.setQuantDiasRepique(response.getQtDiaRepiqConsulta());
			saidaDTO.setValorMaxPagFavNaoCadastrado(response.getVlFavorecidoNaoCadastro());
			saidaDTO.setCdTipoProcessamento(response
					.getCdMomentoProcessamentoPagamento());
			saidaDTO.setDsTipoProcessamento(response
					.getDsMomentoProcessamentoPagamento());
			saidaDTO.setCdTipoRejeicaoEfetivacao(response
					.getCdRejeicaoEfetivacaoLote());
			saidaDTO.setDsTipoRejeicaoEfetivacao(response
					.getDsRejeicaoEfetivacaoLote());
			saidaDTO.setValorLimiteDiario(response.getVlLimiteDiaPagamento());
			saidaDTO.setRdoGerarLanctoFuturoDeb(response
					.getCdLancamentoFuturoDebito());
			saidaDTO.setRdoPermitePagarMenor(response.getCdAgendaValorMenor());
			saidaDTO.setRdoPermitePagarVencido(response
					.getCdAgendaPagamentoVencido());
			saidaDTO.setQntdLimiteDiasPagVencido(response
					.getQtMaximaTituloVencido());
			saidaDTO.setCdPrioridadeDebito(response
					.getCdPrioridadeEfetivacaoPagamento());
			saidaDTO.setDsPrioridadeDebito(response
					.getDsPrioridadeEfetivacaoPagamento());
			saidaDTO.setVlPercentualDiferencaTolerada(response.getVlPercentualDiferencaTolerada());
			saidaDTO.setCdConsistenciaCpfCnpjBenefAvalNpc(response
					.getCdConsistenciaCpfCnpjBenefAvalNpc());
			saidaDTO.setDsConsistenciaCpfCnpjBenefAvalNpc(response
					.getDsConsistenciaCpfCnpjBenefAvalNpc());			
		}
		// Pagamento Via Dep�sito Identificado
		if (entrada.getCdParametroTela() == 24) {
			saidaDTO.setCdTipoConsultaSaldo(response.getCdConsSaldoPagamento());
			saidaDTO.setDsTipoConsultaSaldo(response.getDsConsSaldoPagamento());
			saidaDTO.setQuantDiasRepique(response.getQtDiaRepiqConsulta());
			saidaDTO.setRdoUtilizaCadFavControlePag(response
					.getCdUtilizacaoFavorecidoControle());
			saidaDTO.setValorMaxPagFavNaoCadastrado(response.getVlFavorecidoNaoCadastro());
			saidaDTO.setCdTratamentoFeriadosDtaPag(response
					.getCdPagamentoNaoUtilizado());
			saidaDTO.setDsTratamentoFeriadosDtaPag(response
					.getDsPagamentoNaoUtilizado());
			saidaDTO.setCdTipoProcessamento(response
					.getCdMomentoProcessamentoPagamento());
			saidaDTO.setDsTipoProcessamento(response
					.getDsMomentoProcessamentoPagamento());
			saidaDTO.setCdTipoRejeicaoAgendamento(response
					.getCdRejeicaoAgendaLote());
			saidaDTO.setDsTipoRejeicaoAgendamento(response
					.getDsRejeicaoAgendaLote());
			saidaDTO.setCdTipoRejeicaoEfetivacao(response
					.getCdRejeicaoEfetivacaoLote());
			saidaDTO.setDsTipoRejeicaoEfetivacao(response
					.getDsRejeicaoEfetivacaoLote());
			saidaDTO.setValorLimiteIndividual(response
					.getVlLimiteIndividualPagamento());
			saidaDTO.setRdoPermiteFavConsultarPag(response
					.getCdFavorecidoConsPagamento());
			saidaDTO.setValorLimiteDiario(response.getVlLimiteDiaPagamento());
			saidaDTO.setRdoGerarLanctoFuturoDeb(response
					.getCdLancamentoFuturoDebito());
			saidaDTO.setCdTipoRejeicaoEfetivacao(response
					.getCdRejeicaoEfetivacaoLote());
			saidaDTO.setDsTipoRejeicaoEfetivacao(response
					.getDsRejeicaoEfetivacaoLote());
		}
		// Pagamento de Fornecedores - OCT
		if (entrada.getCdParametroTela() == 25) {
			saidaDTO.setCdTipoConsultaSaldo(response.getCdConsSaldoPagamento());
			saidaDTO.setDsTipoConsultaSaldo(response.getDsConsSaldoPagamento());
			saidaDTO.setQuantDiasRepique(response.getQtDiaRepiqConsulta());
			saidaDTO.setRdoUtilizaCadFavControlePag(response
					.getCdUtilizacaoFavorecidoControle());
			saidaDTO.setValorMaxPagFavNaoCadastrado(response
					.getVlFavorecidoNaoCadastro());
			saidaDTO.setCdTratamentoFeriadosDtaPag(response
					.getCdPagamentoNaoUtilizado());
			saidaDTO.setDsTratamentoFeriadosDtaPag(response
					.getDsPagamentoNaoUtilizado());
			saidaDTO.setCdTipoProcessamento(response
					.getCdMomentoProcessamentoPagamento());
			saidaDTO.setDsTipoProcessamento(response
					.getDsMomentoProcessamentoPagamento());
			saidaDTO.setCdTipoRejeicaoAgendamento(response
					.getCdRejeicaoAgendaLote());
			saidaDTO.setDsTipoRejeicaoAgendamento(response
					.getDsRejeicaoAgendaLote());
			saidaDTO.setValorLimiteIndividual(response
					.getVlLimiteIndividualPagamento());
			saidaDTO.setCdTipoRejeicaoEfetivacao(response
					.getCdRejeicaoEfetivacaoLote());
			saidaDTO.setDsTipoRejeicaoEfetivacao(response
					.getDsRejeicaoEfetivacaoLote());
			saidaDTO.setRdoPermiteFavConsultarPag(response
					.getCdFavorecidoConsPagamento());
			saidaDTO.setValorLimiteDiario(response.getVlLimiteDiaPagamento());
		}
		// Rastreamento de T�tulos
		if (entrada.getCdParametroTela() == 26) {
			saidaDTO.setCdTipoRastreamentoTitulos(response
					.getCdCriterioRastreabilidadeTitulo());
			saidaDTO.setDsTipoRastreamentoTitulos(response
					.getDsCriterioRastreabilidadeTitulo());
			saidaDTO.setRdoRastrearTitTerceiros(response
					.getCdRastreabilidadeTituloTerceiro());
			saidaDTO.setRdoRastrearNotasFiscais(response
					.getCdRastreabilidadeNotaFiscal());
			saidaDTO.setRdoCapturarTitDtaRegistro(response
					.getCdCapituloTituloRegistro());
			saidaDTO.setRdoAgendarTitRastProp(response
					.getCdIndicadorAgendaTitulo());
			saidaDTO.setRdoBloquearEmissaoPap(response
					.getCdBloqueioEmissaoPplta());
			saidaDTO.setDataInicioRastreamento(response
					.getDtInicioRastreabilidadeTitulo());
			saidaDTO.setDataInicioBloqPapeleta(response
					.getDtInicioBloqueioPplta());
			saidaDTO.setCdExigeAutFilial(response.getCdExigeAutFilial());
		}
		// Prova de Vida
		if (entrada.getCdParametroTela() == 27) {
			saidaDTO.setCdTipoComprovacao(response.getCdAcaoNaoVida());
			saidaDTO.setDsTipoComprovacao(response.getDsAcaoNaoVida());
			saidaDTO
					.setQntdMesesPeriComprovacao(response.getQtMesComprovante());
			saidaDTO.setQntdDiasAvisoVenc(response
					.getQtAnteriorVencimentoComprovado());
			saidaDTO.setCodUtilizarMsgPersOnline(response
					.getCdIndicadorMensagemPerso());
			saidaDTO.setDsUtilizarMsgPersOnline(response
					.getDsIndicadorMensagemPerso());
			saidaDTO.setQntdDiasAntecedeInicio(response.getQtAntecedencia());
			saidaDTO.setCdDestino(response.getCdDestinoAviso());
			saidaDTO.setDsDestino(response.getDsDestinoAviso());
			saidaDTO.setCdEnderecoUtilizadoEnvio(response.getCdConsEndereco());
			saidaDTO.setDsEnderecoUtilizadoEnvio(response.getDsConsEndereco());
		}
		// GPS
		// GARE
		// Pagamentos de Tributos - DARF
		if (entrada.getCdParametroTela() == 28
				|| entrada.getCdParametroTela() == 29
				|| entrada.getCdParametroTela() == 30) {
			saidaDTO.setCdTipoConsultaSaldo(response.getCdConsSaldoPagamento());
			saidaDTO.setDsTipoConsultaSaldo(response.getDsConsSaldoPagamento());
			saidaDTO.setValorLimiteIndividual(response
					.getVlLimiteIndividualPagamento());
			saidaDTO.setValorLimiteDiario(response.getVlLimiteDiaPagamento());
			saidaDTO.setCdPermiteConsultaFavorecido(response
					.getCdFavorecidoConsPagamento());
			saidaDTO.setDsPermiteConsultaFavorecido(response
					.getDsFavorecidoConsPagamento());
			saidaDTO.setQuantDiasRepique(response.getQtDiaRepiqConsulta());
			saidaDTO.setCdTratamentoFeriado(response
					.getCdPagamentoNaoUtilizado());
			saidaDTO.setDsTratamentoFeriado(response
					.getDsPagamentoNaoUtilizado());
			saidaDTO.setCdTipoProcessamento(response
					.getCdMomentoProcessamentoPagamento());
			saidaDTO.setDsTipoProcessamento(response
					.getDsMomentoProcessamentoPagamento());
			saidaDTO.setCdTipoRejeicaoAgendamento(response
					.getCdRejeicaoAgendaLote());
			saidaDTO.setDsTipoRejeicaoAgendamento(response
					.getDsRejeicaoAgendaLote());
			saidaDTO.setCdTipoRejeicaoEfetivacao(response
					.getCdRejeicaoEfetivacaoLote());
			saidaDTO.setDsTipoRejeicaoEfetivacao(response
					.getDsRejeicaoEfetivacaoLote());
			saidaDTO.setCdPrioridadeDebito(response
					.getCdPrioridadeEfetivacaoPagamento());
			saidaDTO.setDsPrioridadeDebito(response
					.getDsPrioridadeEfetivacaoPagamento());
			saidaDTO.setCdControlePagamentoFavorecido(response
					.getCdUtilizacaoFavorecidoControle());
			saidaDTO.setDsControlePagamentoFavorecido(response
					.getDsUtilizacaoFavorecidoControle());
			saidaDTO.setValorMaxPagFavNaoCadastrado(response
					.getVlFavorecidoNaoCadastro());
			saidaDTO.setPorcentInconsistenciaLote(response
					.getCdPercentualMaximoInconLote());
			saidaDTO.setQtdeMaxInconsistenciaPorLote(response
					.getQtMaximaInconLote());
			saidaDTO.setPermiteContingenciaPag(response
					.getCdContagemConsSaudo());
		}
		// C�digo de Barras
		if (entrada.getCdParametroTela() == 31) {
			saidaDTO.setCdTipoConsultaSaldo(response.getCdConsSaldoPagamento());
			saidaDTO.setDsTipoConsultaSaldo(response.getDsConsSaldoPagamento());
			saidaDTO.setValorLimiteIndividual(response
					.getVlLimiteIndividualPagamento());
			saidaDTO.setValorLimiteDiario(response.getVlLimiteDiaPagamento());
			saidaDTO.setQuantDiasRepique(response.getQtDiaRepiqConsulta());
			saidaDTO.setCdTratamentoFeriado(response
					.getCdPagamentoNaoUtilizado());
			saidaDTO.setDsTratamentoFeriado(response
					.getDsPagamentoNaoUtilizado());
			saidaDTO.setCdTipoProcessamento(response
					.getCdMomentoProcessamentoPagamento());
			saidaDTO.setDsTipoProcessamento(response
					.getDsMomentoProcessamentoPagamento());
			saidaDTO.setCdTipoRejeicaoAgendamento(response
					.getCdRejeicaoAgendaLote());
			saidaDTO.setDsTipoRejeicaoAgendamento(response
					.getDsRejeicaoAgendaLote());
			saidaDTO.setCdTipoRejeicaoEfetivacao(response
					.getCdRejeicaoEfetivacaoLote());
			saidaDTO.setDsTipoRejeicaoEfetivacao(response
					.getDsRejeicaoEfetivacaoLote());
			saidaDTO.setCdPrioridadeDebito(response
					.getCdPrioridadeEfetivacaoPagamento());
			saidaDTO.setDsPrioridadeDebito(response
					.getDsPrioridadeEfetivacaoPagamento());
			saidaDTO.setCdControlePagFavorecido(response
					.getCdUtilizacaoFavorecidoControle());
			saidaDTO.setDsControlePagFavorecido(response
					.getDsUtilizacaoFavorecidoControle());
			saidaDTO.setValorMaxPagFavNaoCadastrado(response
					.getVlFavorecidoNaoCadastro());
			saidaDTO.setPorcentInconsistenciaLote(response
					.getCdPercentualMaximoInconLote());
			saidaDTO.setQtdeMaxInconsistenciaPorLote(response
					.getQtMaximaInconLote());
			saidaDTO.setPermiteContingenciaPag(response
					.getCdContagemConsSaudo()); // Ok
		}
		// D�bito de Ve�culos
		if (entrada.getCdParametroTela() == 32) {
			saidaDTO.setValorLimiteDiario(response.getVlLimiteDiaPagamento());
			saidaDTO.setValorLimiteIndividual(response
					.getVlLimiteIndividualPagamento());
			saidaDTO.setCdTratamentoFeriado(response
					.getCdPagamentoNaoUtilizado());
			saidaDTO.setDsTratamentoFeriado(response
					.getDsPagamentoNaoUtilizado());
			saidaDTO.setCdTipoProcessamento(response
					.getCdMomentoProcessamentoPagamento());
			saidaDTO.setDsTipoProcessamento(response
					.getDsMomentoProcessamentoPagamento());
			saidaDTO.setCdTratamentoValorDivergente(response
					.getCdTipoDivergenciaVeiculo());
			saidaDTO.setDsTratamentoValorDivergente(response
					.getDsTipoDivergenciaVeiculo());
			saidaDTO.setCdControlePagFavorecido(response
					.getCdUtilizacaoFavorecidoControle());
			saidaDTO.setDsControlePagFavorecido(response
					.getDsUtilizacaoFavorecidoControle());
			saidaDTO.setValorMaxPagFavNaoCadastrado(response
					.getVlFavorecidoNaoCadastro());
			saidaDTO.setCdTipoConsistenciaCpfCnpjProp(response
					.getCdCtciaProprietarioVeiculo());
			saidaDTO.setDsTipoConsistenciaCpfCnpjProp(response
					.getDsCtciaProprietarioVeiculo());
			saidaDTO.setCdTipoConsultaSaldo(response.getCdConsSaldoPagamento());
			saidaDTO.setDsTipoConsultaSaldo(response.getDsConsSaldoPagamento());
		}

		/********************************* TRILHA DE AUDITORIA **************************************/

		saidaDTO.setUsuarioInclusao(response.getCdUsuarioInclusao());
		saidaDTO.setUsuarioManutencao(response.getCdUsuarioAlteracao());
		saidaDTO.setHoraInclusao(FormatarData.formatarDataTrilha(response
				.getHrManutencaoRegistroInclusao()));
		saidaDTO.setHoraManutencao(FormatarData.formatarDataTrilha(response
				.getHrManutencaoRegistroAlteracao()));
		saidaDTO.setCdCanalInclusao(response.getCdCanalInclusao());
		saidaDTO.setCdCanalManutencao(response.getCdCanalAlteracao());
		saidaDTO.setDsCanalInclusao(response.getDsCanalInclusao());
		saidaDTO.setDsCanalManutencao(response.getDsCanalAlteracao());
		saidaDTO.setComplementoInclusao(response.getNmOperacaoFluxoInclusao());
		saidaDTO.setComplementoManutencao(response
				.getNmOperacaoFluxoAlteracao());

		/**************************************** NOVAS *******************************************/

		saidaDTO.setCdAcaoNaoVida(response.getCdAcaoNaoVida());
		saidaDTO.setDsAcaoNaoVida(response.getDsAcaoNaoVida());
		saidaDTO
				.setCdAcertoDadoRecadastro(response.getCdAcertoDadoRecadastro());
		saidaDTO
				.setDsAcertoDadoRecadastro(response.getDsAcertoDadoRecadastro());
		saidaDTO.setCdAgendaDebitoVeiculo(response.getCdAgendaDebitoVeiculo());
		saidaDTO.setDsAgendaDebitoVeiculo(response.getDsAgendaDebitoVeiculo());
		saidaDTO.setCdAgendaPagamentoVencido(response
				.getCdAgendaPagamentoVencido());
		saidaDTO.setDsAgendaPagamentoVencido(response
				.getDsAgendaPagamentoVencido());
		saidaDTO.setCdAgendaValorMenor(response.getCdAgendaValorMenor());
		saidaDTO.setDsAgendaValorMenor(response.getDsAgendaValorMenor());
		saidaDTO.setCdAgrupamentoAviso(response.getCdAgrupamentoAviso());
		saidaDTO.setDsAgrupamentoAviso(response.getDsAgrupamentoAviso());
		saidaDTO.setCdAgrupamentoComprovado(response
				.getCdAgrupamentoComprovado());
		saidaDTO.setDsAgrupamentoComprovado(response
				.getDsAgrupamentoComprovado());
		saidaDTO.setCdAgrupamentoFormularioRecadastro(response
				.getCdAgrupamentoFormularioRecadastro());
		saidaDTO.setDsAgrupamentoFormularioRecadastro(response
				.getDsAgrupamentoFormularioRecadastro());
		saidaDTO.setCdAntecRecadastroBeneficio(response
				.getCdAntecRecadastroBeneficio());
		saidaDTO.setDsAntecRecadastroBeneficio(response
				.getDsAntecRecadastroBeneficio());
		saidaDTO.setCdAreaReservada(response.getCdAreaReservada());
		saidaDTO.setDsAreaReservada(response.getDsAreaReservada());
		saidaDTO.setCdBaseRecadastroBeneficio(response
				.getCdBaseRecadastroBeneficio());
		saidaDTO.setDsBaseRecadastroBeneficio(response
				.getDsBaseRecadastroBeneficio());
		saidaDTO
				.setCdBloqueioEmissaoPplta(response.getCdBloqueioEmissaoPplta());
		saidaDTO
				.setDsBloqueioEmissaoPplta(response.getDsBloqueioEmissaoPplta());
		saidaDTO.setCdCapituloTituloRegistro(response
				.getCdCapituloTituloRegistro());
		saidaDTO.setDsCapituloTituloRegistro(response
				.getDsCapituloTituloRegistro());
		saidaDTO.setCdCobrancaTarifa(response.getCdCobrancaTarifa());
		saidaDTO.setDsCobrancaTarifa(response.getDsCobrancaTarifa());
		saidaDTO.setCdConsDebitoVeiculo(response.getCdConsDebitoVeiculo());
		saidaDTO.setDsConsDebitoVeiculo(response.getDsConsDebitoVeiculo());
		saidaDTO.setCdConsEndereco(response.getCdConsEndereco());
		saidaDTO.setDsConsEndereco(response.getDsConsEndereco());
		saidaDTO.setCdConsSaldoPagamento(response.getCdConsSaldoPagamento());
		saidaDTO.setDsConsSaldoPagamento(response.getDsConsSaldoPagamento());
		saidaDTO.setCdContagemConsSaldo(response.getCdContagemConsSaudo());
		saidaDTO.setDsContagemConsSaldo(response.getDsContagemConsSaudo());
		saidaDTO.setCdCreditoNaoUtilizado(response.getCdCreditoNaoUtilizado());
		saidaDTO.setDsCreditoNaoUtilizado(response.getDsCreditoNaoUtilizado());
		saidaDTO.setCdCriterioEnquaBeneficio(response
				.getCdCriterioEnquaBeneficio());
		saidaDTO.setDsCriterioEnquaBeneficio(response
				.getDsCriterioEnquaBeneficio());
		saidaDTO.setCdCriterioEnquaRecadastro(response
				.getCdCriterioEnquaRecadastro());
		saidaDTO.setDsCriterioEnquaRecadastro(response
				.getDsCriterioEnquaRecadastro());
		saidaDTO.setCdCriterioRastreabilidadeTitulo(response
				.getCdCriterioRastreabilidadeTitulo());
		saidaDTO.setDsCriterioRastreabilidadeTitulo(response
				.getDsCriterioRastreabilidadeTitulo());
		saidaDTO.setCdCtciaEspecieBeneficio(response
				.getCdCtciaEspecieBeneficio());
		saidaDTO.setDsCtciaEspecieBeneficio(response
				.getDsCtciaEspecieBeneficio());
		saidaDTO.setCdCtciaIdentificacaoBeneficio(response
				.getCdCtciaIdentificacaoBeneficio());
		saidaDTO.setDsCtciaIdentificacaoBeneficio(response
				.getDsCtciaIdentificacaoBeneficio());
		saidaDTO.setCdCtciaInscricaoFavorecido(response
				.getCdCtciaInscricaoFavorecido());
		saidaDTO.setDsCtciaInscricaoFavorecido(response
				.getDsCtciaInscricaoFavorecido());
		saidaDTO.setCdCtciaProprietarioVeiculo(response
				.getCdCtciaProprietarioVeiculo());
		saidaDTO.setDsCtciaProprietarioVeiculo(response
				.getDsCtciaProprietarioVeiculo());
		saidaDTO.setCdDispzContaCredito(response.getCdDispzContaCredito());
		saidaDTO.setDsDispzContaCredito(response.getDsDispzContaCredito());
		saidaDTO.setCdDispzDiversarCrrtt(response.getCdDispzDiversarCrrtt());
		saidaDTO.setDsDispzDiversarCrrtt(response.getDsDispzDiversarCrrtt());
		saidaDTO.setCdDispzDiversasNao(response.getCdDispzDiversasNao());
		saidaDTO.setDsDispzDiversasNao(response.getDsDispzDiversasNao());
		saidaDTO.setCdDispzSalarioCrrtt(response.getCdDispzSalarioCrrtt());
		saidaDTO.setDsDispzSalarioCrrtt(response.getDsDispzSalarioCrrtt());
		saidaDTO.setCdDispzSalarioNao(response.getCdDispzSalarioNao());
		saidaDTO.setDsDispzSalarioNao(response.getDsDispzSalarioNao());
		saidaDTO.setCdDestinoAviso(response.getCdDestinoAviso());
		saidaDTO.setDsDestinoAviso(response.getDsDestinoAviso());
		saidaDTO.setCdDestinoComprovante(response.getCdDestinoComprovante());
		saidaDTO.setDsDestinoComprovante(response.getDsDestinoComprovante());
		saidaDTO.setCdDestinoFormularioRecadastro(response
				.getCdDestinoFormularioRecadastro());
		saidaDTO.setDsDestinoFormularioRecadastro(response
				.getDsDestinoFormularioRecadastro());
		saidaDTO.setCdEnvelopeAberto(response.getCdEnvelopeAberto());
		saidaDTO.setDsEnvelopeAberto(response.getDsEnvelopeAberto());
		saidaDTO.setCdFavorecidoConsPagamento(response
				.getCdFavorecidoConsPagamento());
		saidaDTO.setDsFavorecidoConsPagamento(response
				.getDsFavorecidoConsPagamento());
		saidaDTO.setCdFormaAutorizacaoPagamento(response
				.getCdFormaAutorizacaoPagamento());
		saidaDTO.setDsFormaAutorizacaoPagamento(response
				.getDsFormaAutorizacaoPagamento());
		saidaDTO.setCdFormaEnvioPagamento(response.getCdFormaEnvioPagamento());
		saidaDTO.setDsFormaEnvioPagamento(response.getDsFormaEnvioPagamento());
		saidaDTO.setCdFormaExpiracaoCredito(response
				.getCdFormaExpiracaoCredito());
		saidaDTO.setDsFormaExpiracaoCredito(response
				.getDsFormaExpiracaoCredito());
		saidaDTO.setCdFormaManutencao(response.getCdFormaManutencao());
		saidaDTO.setDsFormaManutencao(response.getDsFormaManutencao());
		saidaDTO.setCdFrasePreCadastro(response.getCdFrasePreCadastro());
		saidaDTO.setDsFrasePreCadastro(response.getDsFrasePreCadastro());
		saidaDTO.setCdIndicadorAgendaTitulo(response
				.getCdIndicadorAgendaTitulo());
		saidaDTO.setDsIndicadorAgendaTitulo(response
				.getDsIndicadorAgendaTitulo());
		saidaDTO.setCdIndicadorAutorizacaoCliente(response
				.getCdIndicadorAutorizacaoCliente());
		saidaDTO.setDsIndicadorAutorizacaoCliente(response
				.getDsIndicadorAutorizacaoCliente());
		saidaDTO.setCdIndicadorAutorizacaoComplemento(response
				.getCdIndicadorAutorizacaoComplemento());
		saidaDTO.setDsIndicadorAutorizacaoComplemento(response
				.getDsIndicadorAutorizacaoComplemento());
		saidaDTO
				.setCdIndicadorCadastroOrg(response.getCdIndicadorCadastroOrg());
		saidaDTO
				.setDsIndicadorCadastroOrg(response.getDsIndicadorCadastroOrg());
		saidaDTO.setCdIndicadorCadastroProcd(response
				.getCdIndicadorCadastroProcd());
		saidaDTO.setDsIndicadorCadastroProcd(response
				.getDsIndicadorCadastroProcd());
		saidaDTO.setCdIndicadorCataoSalario(response
				.getCdIndicadorCataoSalario());
		saidaDTO.setDsIndicadorCataoSalario(response
				.getDsIndicadorCataoSalario());
		saidaDTO.setCdIndicadorEconomicoReajuste(response
				.getCdIndicadorEconomicoReajuste());
		saidaDTO.setDsIndicadorEconomicoReajuste(response
				.getDsIndicadorEconomicoReajuste());
		saidaDTO.setCdIndicadorExpiracaoCredito(response
				.getCdIndicadorExpiracaoCredito());
		saidaDTO.setDsIndicadorExpiracaoCredito(response
				.getDsIndicadorExpiracaoCredito());
		saidaDTO.setCdIndicadorLancamentoPagamento(response
				.getCdIndicadorLancamentoPagamento());
		saidaDTO.setDsIndicadorLancamentoPagamento(response
				.getDsIndicadorLancamentoPagamento());
		saidaDTO.setCdIndicadorMensagemPerso(response
				.getCdIndicadorMensagemPerso());
		saidaDTO.setDsIndicadorMensagemPerso(response
				.getDsIndicadorMensagemPerso());
		saidaDTO.setCdLancamentoFuturoCredito(response
				.getCdLancamentoFuturoCredito());
		saidaDTO.setDsLancamentoFuturoCredito(response
				.getDsLancamentoFuturoCredito());
		saidaDTO.setCdLancamentoFuturoDebito(response
				.getCdLancamentoFuturoDebito());
		saidaDTO.setDsLancamentoFuturoDebito(response
				.getDsLancamentoFuturoDebito());
		saidaDTO.setCdLiberacaoLoteProcesso(response
				.getCdLiberacaoLoteProcesso());
		saidaDTO.setDsLiberacaoLoteProcesso(response
				.getDsLiberacaoLoteProcesso());
		saidaDTO.setCdManutencaoBaseRecadastro(response
				.getCdManutencaoBaseRecadastro());
		saidaDTO.setDsManutencaoBaseRecadastro(response
				.getDsManutencaoBaseRecadastro());
		saidaDTO.setCdMidiaDisponivel(response.getCdMidiaDisponivel());
		saidaDTO.setDsMidiaDisponivel(response.getDsMidiaDisponivel());
		saidaDTO.setCdMidiaMensagemRecadastro(response
				.getCdMidiaMensagemRecadastro());
		saidaDTO.setDsMidiaMensagemRecadastro(response
				.getDsMidiaMensagemRecadastro());
		saidaDTO.setCdMomentoAvisoRacadastro(response
				.getCdMomentoAvisoRacadastro());
		saidaDTO.setDsMomentoAvisoRacadastro(response
				.getDsMomentoAvisoRacadastro());
		saidaDTO.setCdMomentoCreditoEfetivacao(response
				.getCdMomentoCreditoEfetivacao());
		saidaDTO.setDsMomentoCreditoEfetivacao(response
				.getDsMomentoCreditoEfetivacao());
		saidaDTO.setCdMomentoDebitoPagamento(response
				.getCdMomentoDebitoPagamento());
		saidaDTO.setDsMomentoDebitoPagamento(response
				.getDsMomentoDebitoPagamento());
		saidaDTO.setCdMomentoFormularioRecadastro(response
				.getCdMomentoFormularioRecadastro());
		saidaDTO.setDsMomentoFormularioRecadastro(response
				.getDsMomentoFormularioRecadastro());
		saidaDTO.setCdMomentoProcessamentoPagamento(response
				.getCdMomentoProcessamentoPagamento());
		saidaDTO.setDsMomentoProcessamentoPagamento(response
				.getDsMomentoProcessamentoPagamento());
		saidaDTO.setCdMensagemRecadastroMidia(response
				.getCdMensagemRecadastroMidia());
		saidaDTO.setDsMensagemRecadastroMidia(response
				.getDsMensagemRecadastroMidia());
		saidaDTO.setCdPermissaoDebitoOnline(response
				.getCdPermissaoDebitoOnline());
		saidaDTO.setDsPermissaoDebitoOnline(response
				.getDsPermissaoDebitoOnline());
		saidaDTO.setCdNaturezaOperacaoPagamento(response
				.getCdNaturezaOperacaoPagamento());
		saidaDTO.setDsNaturezaOperacaoPagamento(response
				.getDsNaturezaOperacaoPagamento());
		saidaDTO.setCdPeriodicidadeAviso(response.getCdPeriodicidadeAviso());
		saidaDTO.setDsPeriodicidadeAviso(response.getDsPeriodicidadeAviso());
		saidaDTO.setCdPerdcCobrancaTarifa(response.getCdPerdcCobrancaTarifa());
		saidaDTO.setDsPerdcCobrancaTarifa(response.getDsPerdcCobrancaTarifa());
		saidaDTO.setCdPerdcComprovante(response.getCdPerdcComprovante());
		saidaDTO.setDsPerdcComprovante(response.getDsPerdcComprovante());
		saidaDTO
				.setCdPerdcConsultaVeiculo(response.getCdPerdcConsultaVeiculo());
		saidaDTO
				.setDsPerdcConsultaVeiculo(response.getDsPerdcConsultaVeiculo());
		saidaDTO.setCdPerdcEnvioRemessa(response.getCdPerdcEnvioRemessa());
		saidaDTO.setDsPerdcEnvioRemessa(response.getDsPerdcEnvioRemessa());
		saidaDTO
				.setCdPerdcManutencaoProcd(response.getCdPerdcManutencaoProcd());
		saidaDTO
				.setDsPerdcManutencaoProcd(response.getDsPerdcManutencaoProcd());
		saidaDTO.setCdPagamentoNaoUtilizado(response
				.getCdPagamentoNaoUtilizado());
		saidaDTO.setDsPagamentoNaoUtilizado(response
				.getDsPagamentoNaoUtilizado());
		saidaDTO.setCdPrincipalEnquaRecadastro(response
				.getCdPrincipalEnquaRecadastro());
		saidaDTO.setDsPrincipalEnquaRecadastro(response
				.getDsPrincipalEnquaRecadastro());
		saidaDTO.setCdPrioridadeEfetivacaoPagamento(response
				.getCdPrioridadeEfetivacaoPagamento());
		saidaDTO.setDsPrioridadeEfetivacaoPagamento(response
				.getDsPrioridadeEfetivacaoPagamento());
		saidaDTO.setCdRejeicaoAgendaLote(response.getCdRejeicaoAgendaLote());
		saidaDTO.setDsRejeicaoAgendaLote(response.getDsRejeicaoAgendaLote());
		saidaDTO.setCdRejeicaoEfetivacaoLote(response
				.getCdRejeicaoEfetivacaoLote());
		saidaDTO.setDsRejeicaoEfetivacaoLote(response
				.getDsRejeicaoEfetivacaoLote());
		saidaDTO.setCdRejeicaoLote(response.getCdRejeicaoLote());
		saidaDTO.setDsRejeicaoLote(response.getDsRejeicaoLote());
		saidaDTO.setCdRastreabilidadeNotaFiscal(response
				.getCdRastreabilidadeNotaFiscal());
		saidaDTO.setDsRastreabilidadeNotaFiscal(response
				.getDsRastreabilidadeNotaFiscal());
		saidaDTO.setCdRastreabilidadeTituloTerceiro(response
				.getCdRastreabilidadeTituloTerceiro());
		saidaDTO.setDsRastreabilidadeTituloTerceiro(response
				.getDsRastreabilidadeTituloTerceiro());
		saidaDTO.setCdTipoCargaRecadastro(response.getCdTipoCargaRecadastro());
		saidaDTO.setDsTipoCargaRecadastro(response.getDsTipoCargaRecadastro());
		saidaDTO.setCdTipoCataoSalario(response.getCdTipoCataoSalario());
		saidaDTO.setDsTipoCataoSalario(response.getDsTipoCataoSalario());
		saidaDTO.setCdTipoDataFloat(response.getCdTipoDataFloat());
		saidaDTO.setDsTipoDataFloat(response.getDsTipoDataFloat());
		saidaDTO.setCdTipoDivergenciaVeiculo(response
				.getCdTipoDivergenciaVeiculo());
		saidaDTO.setDsTipoDivergenciaVeiculo(response
				.getDsTipoDivergenciaVeiculo());
		saidaDTO.setCdTipoIdBeneficio(response.getCdTipoIdBeneficio());
		saidaDTO.setDsTipoIdBeneficio(response.getDsTipoIdBeneficio());
		saidaDTO.setCdTipoLayoutArquivo(response.getCdTipoLayoutArquivo());
		saidaDTO.setDsTipoLayoutArquivo(response.getDsTipoLayoutArquivo());
		saidaDTO.setCdTipoReajusteTarifa(response.getCdTipoReajusteTarifa());
		saidaDTO.setDsTipoReajusteTarifa(response.getDsTipoReajusteTarifa());
		saidaDTO.setCdContratoContaTransferencia(response
				.getCdContratoContaTransferencia());
		saidaDTO.setDsContratoContaTransferencia(response
				.getDsContratoContaTransferencia());
		saidaDTO.setCdUtilizacaoFavorecidoControle(response
				.getCdUtilizacaoFavorecidoControle());
		saidaDTO.setDsUtilizacaoFavorecidoControle(response
				.getDsUtilizacaoFavorecidoControle());
		saidaDTO.setDtEnquaContaSalario(response.getDtEnquaContaSalario());
		saidaDTO.setDtInicioBloqueioPplta(response.getDtInicioBloqueioPplta());
		saidaDTO.setDtInicioRastreabilidadeTitulo(response
				.getDtInicioRastreabilidadeTitulo());
		saidaDTO.setDtFimAcertoRecadastro(response.getDtFimAcertoRecadastro());
		saidaDTO.setDtFimRecadastroBeneficio(response
				.getDtFimRecadastroBeneficio());
		saidaDTO.setDtinicioAcertoRecadastro(response
				.getDtinicioAcertoRecadastro());
		saidaDTO.setDtInicioRecadastroBeneficio(response
				.getDtInicioRecadastroBeneficio());
		saidaDTO.setDtLimiteVinculoCarga(response.getDtLimiteVinculoCarga());
		saidaDTO.setNrFechamentoApuracaoTarifa(response
				.getNrFechamentoApuracaoTarifa());
		saidaDTO.setCdPercentualIndicadorReajusteTarifa(response
				.getCdPercentualIndicadorReajusteTarifa());
		saidaDTO.setCdPercentualMaximoInconLote(response
				.getCdPercentualMaximoInconLote());
		saidaDTO.setCdPercentualReducaoTarifaCatalogo(response
				.getCdPercentualReducaoTarifaCatalogo());
		saidaDTO.setQtAntecedencia(response.getQtAntecedencia());
		saidaDTO.setQtAnteriorVencimentoComprovado(response
				.getQtAnteriorVencimentoComprovado());
		saidaDTO.setQtDiaCobrancaTarifa(response.getQtDiaCobrancaTarifa());
		saidaDTO.setQtDiaExpiracao(response.getQtDiaExpiracao());
		saidaDTO.setCdDiaFloatPagamento(response.getCdDiaFloatPagamento());
		saidaDTO.setQtDiaInatividadeFavorecido(response
				.getQtDiaInatividadeFavorecido());
		saidaDTO.setQtDiaRepiqConsulta(response.getQtDiaRepiqConsulta());
		saidaDTO.setQtEtapasRecadastroBeneficio(response
				.getQtEtapasRecadastroBeneficio());
		saidaDTO.setQtFaseRecadastroBeneficio(response
				.getQtFaseRecadastroBeneficio());
		saidaDTO.setQtLimiteLinha(response.getQtLimiteLinha());
		saidaDTO.setQtLimiteSolicitacaoCatao(response
				.getQtLimiteSolicitacaoCatao());
		saidaDTO.setQtMaximaInconLote(response.getQtMaximaInconLote());
		saidaDTO.setQtMaximaTituloVencido(response.getQtMaximaTituloVencido());
		saidaDTO.setQtMesComprovante(response.getQtMesComprovante());
		saidaDTO.setQtMesEtapaRecadastro(response.getQtMesEtapaRecadastro());
		saidaDTO.setQtMesFaseRecadastro(response.getQtMesFaseRecadastro());
		saidaDTO.setQtMesReajusteTarifa(response.getQtMesReajusteTarifa());
		saidaDTO.setQtViaAviso(response.getQtViaAviso());
		saidaDTO.setQtViaCobranca(response.getQtViaCobranca());
		saidaDTO.setQtViaComprovante(response.getQtViaComprovante());
		saidaDTO.setVlFavorecidoNaoCadastro(response
				.getVlFavorecidoNaoCadastro());
		saidaDTO.setVlLimiteDiaPagamento(response.getVlLimiteDiaPagamento());
		saidaDTO.setVlLimiteIndividualPagamento(response
				.getVlLimiteIndividualPagamento());
		saidaDTO.setCdIndicadorEmissaoAviso(response
				.getCdIndicadorEmissaoAviso());
		saidaDTO.setDsIndicadorEmissaoAviso(response
				.getDsIndicadorEmissaoAviso());
		saidaDTO.setCdIndicadorRetornoInternet(response
				.getCdIndicadorRetornoInternet());
		saidaDTO.setDsIndicadorRetornoInternet(response
				.getDsIndicadorRetornoInternet());
		saidaDTO.setCdIndicadorRetornoSeparado(response
				.getCdIndicadorRetornoSeparado());
		saidaDTO.setDsCodigoIndicadorRetornoSeparado(response
				.getDsCodigoIndicadorRetornoSeparado());
		saidaDTO
				.setCdIndicadorListaDebito(response.getCdIndicadorListaDebito());
		saidaDTO
				.setDsIndicadorListaDebito(response.getDsIndicadorListaDebito());
		saidaDTO.setCdTipoFormacaoLista(response.getCdTipoFormacaoLista());
		saidaDTO.setDsTipoFormacaoLista(response.getDsTipoFormacaoLista());
		saidaDTO.setCdTipoConsistenciaLista(response
				.getCdTipoConsistenciaLista());
		saidaDTO.setDsTipoConsistenciaLista(response
				.getDsTipoConsistenciaLista());
		saidaDTO.setCdTipoConsultaComprovante(response
				.getCdTipoConsultaComprovante());
		saidaDTO.setDsTipoConsolidacaoComprovante(response
				.getDsTipoConsolidacaoComprovante());
		saidaDTO.setCdValidacaoNomeFavorecido(response
				.getCdValidacaoNomeFavorecido());
		saidaDTO.setDsValidacaoNomeFavorecido(response
				.getDsValidacaoNomeFavorecido());
		saidaDTO.setCdTipoContaFavorecido(response
				.getCdCtciaInscricaoFavorecido());
		saidaDTO.setDsTipoConsistenciaFavorecido(response
				.getDsCtciaInscricaoFavorecido());
		saidaDTO.setCdIndLancamentoPersonalizado(response
				.getCdIndLancamentoPersonalizado());
		saidaDTO.setDsIndLancamentoPersonalizado(response
				.getDsIndLancamentoPersonalizado());
		saidaDTO.setCdAgendaRastreabilidadeFilial(response
				.getCdAgendaRastreabilidadeFilial());
		saidaDTO.setDsAgendamentoRastFilial(response
				.getDsAgendamentoRastFilial());
		saidaDTO.setCdIndicadorAdesaoSacador(response
				.getCdIndicadorAdesaoSacador());
		saidaDTO.setDsIndicadorAdesaoSacador(response
				.getDsIndicadorAdesaoSacador());
		saidaDTO
				.setCdMeioPagamentoCredito(response.getCdMeioPagamentoCredito());
		saidaDTO
				.setDsMeioPagamentoCredito(response.getDsMeioPagamentoCredito());
		saidaDTO.setCdTipoIsncricaoFavorecido(response
				.getCdTipoIsncricaoFavorecido());
		saidaDTO.setDsTipoIscricaoFavorecido(response
				.getDsTipoIscricaoFavorecido());
		saidaDTO
				.setCdIndicadorBancoPostal(response.getCdIndicadorBancoPostal());
		saidaDTO
				.setDsIndicadorBancoPostal(response.getDsIndicadorBancoPostal());
		saidaDTO.setCdFormularioContratoCliente(response
				.getCdFormularioContratoCliente());
		saidaDTO.setDsCodigoFormularioContratoCliente(response
				.getDsCodigoFormularioContratoCliente());
		saidaDTO.setCdUsuarioInclusao(response.getCdUsuarioInclusao());
		saidaDTO.setCdUsuarioExternoInclusao(response
				.getCdUsuarioExternoInclusao());
		saidaDTO.setHrManutencaoRegistroInclusao(response
				.getHrManutencaoRegistroInclusao());
		saidaDTO.setCdCanalInclusao(response.getCdCanalInclusao());
		saidaDTO.setDsCanalInclusao(response.getDsCanalInclusao());
		saidaDTO.setNmOperacaoFluxoInclusao(response
				.getNmOperacaoFluxoInclusao());
		saidaDTO.setDsAreaResrd(response.getDsAreaResrd());
		saidaDTO.setCdConsultaSaldoValorSuperior(response
				.getCdConsultaSaldoValorSuperior());
		saidaDTO.setDsConsultaSaldoSuperior(response
				.getDsConsultaSaldoSuperior());
		saidaDTO.setCdIndicadorFeriadoLocal(response
				.getCdIndicadorFeriadoLocal());
		saidaDTO.setDsCodigoIndFeriadoLocal(response
				.getDsCodigoIndFeriadoLocal());
		saidaDTO.setCdIndicadorSegundaLinha(response
				.getCdIndicadorSegundaLinha());
		saidaDTO.setDsIndicadorSegundaLinha(response
				.getDsIndicadorSegundaLinha());
		saidaDTO
				.setCdFloatServicoContrato(response.getCdFloatServicoContrato());
		saidaDTO
				.setDsFloatServicoContrato(response.getDsFloatServicoContrato());

		saidaDTO.setCdTituloDdaRetorno(response.getCdTituloDdaRetorno());
		saidaDTO.setDsTituloDdaRetorno(response.getDsTituloDdaRetorno());
		saidaDTO.setCdIndicadorAgendaGrade(response.getCdIndicadorAgendaGrade());
		saidaDTO.setDsIndicadorAgendaGrade(response.getDsIndicadorAgendaGrade());
		saidaDTO.setCdPreenchimentoLancamentoPersonalizado(response.getCdPreenchimentoLancamentoPersonalizado());
		saidaDTO.setDsPreenchimentoLancamentoPersonalizado(response.getDsPreenchimentoLancamentoPersonalizado());
		saidaDTO.setCdIndicadorTipoRetornoInternet(response.getCdIndicadorTipoRetornoInternet());
		return saidaDTO;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.service.business.mantercontrato.
	 *      IManterContratoService#consultarConManDadosBasicos(br.com.bradesco.web.pgit.service.business.
	 *      mantercontrato.bean.ConsultarConManDadosBasicosEntradaDTO)
	 */
	public ConsultarConManDadosBasicosSaidaDTO consultarConManDadosBasicos(
			ConsultarConManDadosBasicosEntradaDTO entrada) {
		ConsultarConManDadosBasicosRequest request = new ConsultarConManDadosBasicosRequest();
		request.setCdPessoaJuridicaContrato(entrada
				.getCdPessoaJuridicaContrato());
		request.setCdTipoContratoNegocio(entrada.getCdTipoContratoNegocio());
		request.setHrInclusaoRegistro(entrada.getHrInclusaoRegistro());
		request.setNrSequenciaContratoNegocio(entrada
				.getNrSequenciaContratoNegocio());

		ConsultarConManDadosBasicosResponse response = getFactoryAdapter()
				.getConsultarConManDadosBasicosPDCAdapter().invokeProcess(
						request);

		ConsultarConManDadosBasicosSaidaDTO saida = new ConsultarConManDadosBasicosSaidaDTO();
		saida.setCdAcaoRelacionamento(response.getCdAcaoRelacionamento());
		saida.setCdCanalInclusao(response.getCdCanalInclusao());
		saida.setCdCanalManutencao(response.getCdCanalManutencao());
		saida.setCdContratoNegocio(response.getCdContratoNegocio());
		saida.setCdDeptoGestor(response.getCdDeptoGestor());
		saida.setCdFuncionarioBradesco(response.getCdFuncionarioBradesco());
		saida.setCdIdentificadorIdioma(response.getCdIdentificadorIdioma());
		saida.setCdIndicadorMoeda(response.getCdIndicadorMoeda());
		saida.setCdIndicadorTipoManutencao(response
				.getCdIndicadorTipoManutencao());
		saida.setCdMotivoSituacaoContrato(response
				.getCdMotivoSituacaoContrato());
		saida.setCdOrigemContrato(response.getCdOrigemContrato());
		saida.setCdPessoaJuridicaContrato(response
				.getCdPessoaJuridicaContrato());
		saida.setCdSituacaoContrato(response.getCdSituacaoContrato());
		saida.setCdTipoContratoNegocio(response.getCdTipoContratoNegocio());
		saida.setCdUsuarioInclusao(response.getCdUsuarioInclusao());
		saida.setCdUsuarioInclusaoExterno(response
				.getCdUsuarioInclusaoExterno());
		saida.setCdUsuarioManutencao(response.getCdUsuarioManutencao());
		saida.setCdUsuarioManutencaoExterno(response
				.getCdUsuarioManutencaoExterno());
		saida.setCodMensagem(response.getCodMensagem());
		saida.setDsCanalInclusao(response.getDsCanalInclusao());
		saida.setDsCanalManutencao(response.getDsCanalManutencao());
		saida.setDsContratoNegocio(response.getDsContratoNegocio());
		saida.setDsIdioma(response.getDsIdioma());
		saida.setDsIndicadorAditivo(response.getDsIndicadorAditivo());
		saida.setDsIndicadorParticipante(response.getDsIndicadorParticipante());
		saida.setDsMoeda(response.getDsMoeda());
		saida.setDsMotivoSituacaoContrato(response
				.getDsMotivoSituacaoContrato());
		saida.setDtFimVigencia(response.getDtFimVigencia());
		saida.setDtInicioVigencia(response.getDtInicioVigencia());
		saida.setDsOrigemContrato(response.getDsOrigemContrato());
		saida.setHrAssinaturaContrato(response.getHrAssinaturaContrato());
		saida.setHrInclusaoRegistro(FormatarData.formatarDataTrilha(response
				.getHrInclusaoRegistro()));
		saida.setHrInclusaoRegistroHistorico(response
				.getHrInclusaoRegistroHistorico());
		saida.setHrManutencaoRegistro(FormatarData.formatarDataTrilha(response
				.getHrManutencaoRegistro()));
		saida.setHrSituacaoContrato(response.getHrSituacaoContrato());
		saida.setDsTipoContratoNegocio(response.getDsTipoContratoNegocio());
		saida.setDsSituacaoContrato(response.getDsSituacaoContrato());
		saida.setMensagem(response.getMensagem());
		saida.setNmDepto(response.getNmDepto());
		saida.setNmFuncionarioBradesco(response.getNmFuncionarioBradesco());
		saida.setNmOperacaoFluxoInclusao(response.getNmOperacaoFluxoInclusao());
		saida.setNmOperacaoFluxoManutencao(response
				.getNmOperacaoFluxoManutencao());
		saida.setNmPessoaJuridica(response.getNmPessoaJuridica());
		saida.setNrSequenciaContratoNegocio(response
				.getNrSequenciaContratoNegocio());
		saida.setDsIndicadorTipoManutencao(response
				.getDsIndicadorTipoManutencao());
		saida.setCdSetorContratoPagamento(response
				.getCdSetorContratoPagamento());
		saida.setDsSetorContratoPagamento(response
				.getDsSetorContratoPagamento());
		saida.setVlSaldoVirtualTeste(response.getVlSaldoVirtualTeste());
		saida.setCdPessoaJuridicaProposta(response
				.getCdPessoaJuridicaProposta());
		saida.setCdTipoContratoProposta(response.getCdTipoContratoProposta());
		saida.setNrContratoProposta(response.getNrContratoProposta());
		saida.setDsPessoaJuridicaProposta(response
				.getDsPessoaJuridicaProposta());
		saida.setDsTipoContratoProposta(response.getDsTipoContratoProposta());
		saida.setNrSequenciaContratoOutros(response
				.getNrSequenciaContratoOutros());
		saida.setNrSequenciaContratoPagamentoSalario(response
				.getNrSequenciaContratoPagamentoSalario());
		saida.setCdFormaAutorizacaoPagamento(response.getCdFormaAutorizacaoPagamento());
		saida.setDsFormaAutorizacaoPagamento(response.getDsFormaAutorizacaoPagamento());
		return saida;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.service.business.mantercontrato.
	 *      IManterContratoService#alterarConfiguracaoTipoServModContrato(br.com.bradesco.web.pgit.service.
	 *      business.mantercontrato.bean.AlterarConfiguracaoTipoServModContratoEntradaDTO)
	 */
	
	
	public AlterarConfiguracaoTipoServModContratoSaidaDTO alterarConfiguracaoTipoServModContrato(
			AlterarConfiguracaoTipoServModContratoEntradaDTO entradaDTO) {
	
		AlterarConfiguracaoTipoServModContratoResponse response = new AlterarConfiguracaoTipoServModContratoResponse();
		AlterarConfiguracaoTipoServModContratoRequest request = new AlterarConfiguracaoTipoServModContratoRequest();
	
		DetalharTipoServModContratoResponse responseDetalhar = new DetalharTipoServModContratoResponse();
		DetalharTipoServModContratoRequest requestDetalhar = new DetalharTipoServModContratoRequest();
		
		AlterarConfiguracaoTipoServModContratoSaidaDTO saidaDTO = new AlterarConfiguracaoTipoServModContratoSaidaDTO();
	
		requestDetalhar.setCdPessoaJuridicaContrato(entradaDTO.getCdPessoaJuridicaContrato());
		requestDetalhar.setCdTipoContratoNegocio(entradaDTO.getCdTipoContratoNegocio());
		requestDetalhar.setNrSequenciaContratoNegocio(entradaDTO.getNrSequenciaContratoNegocio());
		requestDetalhar.setCdProdutoServicoOperacao(entradaDTO.getCdTipoServico());
		requestDetalhar.setCdProdutoOperacaoRelacionado(entradaDTO.getCdModalidade());
		requestDetalhar.setCdParametro(entradaDTO.getCdParametro());
		request.setQtDiaUtilPgto(verificaIntegerNulo(entradaDTO.getQtDiaUtilPgto()));
		request.setCdConsistenciaCpfCnpjBenefAvalNpc(0);

		responseDetalhar = getFactoryAdapter().getDetalharTipoServModContratoPDCAdapter().invokeProcess(requestDetalhar);	

		request.setCdIndicadorUtilizaMora(verificaIntegerNulo(entradaDTO.getCdIndicadorUtilizaMora()));
		request.setCdPessoaJuridicaContrato(entradaDTO.getCdPessoaJuridicaContrato());
		request.setCdTipoContratoNegocio(entradaDTO.getCdTipoContratoNegocio());
		request.setNrSequenciaCotratoNegocio(entradaDTO.getNrSequenciaContratoNegocio());
		request.setCdProdutoServicoOperacao(entradaDTO.getCdTipoServico());
		request.setCdProdutoOperacaroRelacionado(entradaDTO.getCdModalidade());
		request.setCdParametro(entradaDTO.getCdParametro());
		request.setCdIndicadorSegundaLinha(0);
		request.setCdExigeAutFilial(verificaIntegerNulo(entradaDTO.getCdExigeAutFilial()));

		// Preenchendo todos os campos
		request.setCdAcaoNaoVida(verificaIntegerNulo(responseDetalhar.getCdAcaoNaoVida()));
		request.setCdAcertoDadoRecadastro(responseDetalhar.getCdAcertoDadoRecadastro());
		request.setCdAgendaDebitoVeiculo(responseDetalhar.getCdAgendaDebitoVeiculo());
		request.setCdAgendaPagamentoVencido(responseDetalhar.getCdAgendaPagamentoVencido());
		request.setCdAgendaValorMenor(responseDetalhar.getCdAgendaValorMenor());
		request.setCdAgrupamentoAviso(responseDetalhar.getCdAgrupamentoAviso());
		request.setCdAgrupamentoComprovado(responseDetalhar.getCdAgrupamentoComprovado());
		request.setCdAgrupamentoFormularioRecadastro(responseDetalhar.getCdAgrupamentoFormularioRecadastro());
		request.setCdAntecRecadastroBeneficio(responseDetalhar.getCdAntecRecadastroBeneficio());
		request.setCdAreaReservada(responseDetalhar.getCdAreaReservada());
		request.setCdBaseRecadastroBeneficio(responseDetalhar.getCdBaseRecadastroBeneficio());
		request.setCdBloqueioEmissaoPplta(responseDetalhar.getCdBloqueioEmissaoPplta());
		request.setCdCapituloTituloRegistro(responseDetalhar.getCdCapituloTituloRegistro());
		request.setCdCobrancaTarifa(responseDetalhar.getCdCobrancaTarifa());
		request.setCdConsDebitoVeiculo(responseDetalhar.getCdConsDebitoVeiculo());
		request.setCdConsEndereco(responseDetalhar.getCdConsEndereco());
		request.setCdConsSaldoPagamento(responseDetalhar.getCdConsSaldoPagamento());
		request.setCdContagemConsSaudo(responseDetalhar.getCdContagemConsSaudo());
		request.setCdCreditoNaoUtilizado(responseDetalhar.getCdCreditoNaoUtilizado());
		request.setCdCriterioEnquaBeneficio(responseDetalhar.getCdCriterioEnquaBeneficio());
		request.setCdCriterioEnquaRecadastro(responseDetalhar.getCdCriterioEnquaRecadastro());
		request.setCdCriterioRastreabilidadeTitulo(responseDetalhar.getCdCriterioRastreabilidadeTitulo());
		request.setCdCtciaEspecieBeneficio(responseDetalhar.getCdCtciaEspecieBeneficio());
		request.setCdCtciaIdentificacaoBeneficio(responseDetalhar.getCdCtciaIdentificacaoBeneficio());
		request.setCdCtciaInscricaoFavorecido(responseDetalhar.getCdCtciaInscricaoFavorecido());
		request.setCdCtciaProprietarioVeiculo(responseDetalhar.getCdCtciaProprietarioVeiculo());
		request.setCdDispzContaCredito(responseDetalhar.getCdDispzContaCredito());
		request.setCdDispzDiversarCrrtt(responseDetalhar.getCdDispzDiversarCrrtt());
		request.setCdDispzDiversasNao(responseDetalhar.getCdDispzDiversasNao());
		request.setCdDispzSalarioNao(responseDetalhar.getCdDispzSalarioNao());
		request.setCdDIspzSalarioCrrtt(responseDetalhar.getCdDispzSalarioCrrtt());
		request.setCdDestinoAviso(responseDetalhar.getCdDestinoAviso());
		request.setCdDestinoComprovante(responseDetalhar.getCdDestinoComprovante());
		request.setCdDestinoFormularioRecadastro(responseDetalhar.getCdDestinoFormularioRecadastro());
		request.setCdEnvelopeAberto(responseDetalhar.getCdEnvelopeAberto());
		request.setCdFavorecidoConsPagamento(responseDetalhar.getCdFavorecidoConsPagamento());
		request.setCdFormaAutorizacaoPagamento(responseDetalhar.getCdFormaAutorizacaoPagamento());
		request.setCdFormaEnvioPagamento(responseDetalhar.getCdFormaEnvioPagamento());
		request.setCdFormaEstornoCredito(0);
		request.setCdFormaExpiracaoCredito(responseDetalhar.getCdFormaExpiracaoCredito());
		request.setCdFormaManutencao(responseDetalhar.getCdFormaManutencao());
		request.setCdFrasePreCadastro(responseDetalhar.getCdFrasePreCadastro());
		request.setCdIndicadorAgendaTitulo(responseDetalhar.getCdIndicadorAgendaTitulo());
		request.setCdIndicadorAutorizacaoCliente(responseDetalhar.getCdIndicadorAutorizacaoCliente());
		request.setCdIndicadorAutorizacaoComplemento(responseDetalhar.getCdIndicadorAutorizacaoComplemento());
		request.setCdIndicadorCadastroOrg(responseDetalhar.getCdIndicadorCadastroOrg());
		request.setCdIndicadorCadastroProcd(responseDetalhar.getCdIndicadorCadastroProcd());
		request.setCdIndicadorCataoSalario(responseDetalhar.getCdIndicadorCataoSalario());
		request.setCdIndicadorExpiracaoCredito(responseDetalhar.getCdIndicadorExpiracaoCredito());
		request.setCdIndicadorLancamentoPagamento(responseDetalhar.getCdIndicadorLancamentoPagamento());
		request.setCdIndicadorMensagemPerso(responseDetalhar.getCdIndicadorMensagemPerso());
		request.setCdLancamentoFuturoCredito(responseDetalhar.getCdLancamentoFuturoCredito());
		request.setCdLancamentoFuturoDebito(responseDetalhar.getCdLancamentoFuturoDebito());
		request.setCdLiberacaoLoteProcesso(responseDetalhar.getCdLiberacaoLoteProcesso());
		request.setCdManutencaoBaseRecadastro(responseDetalhar.getCdManutencaoBaseRecadastro());
		request.setCdMeioPagamentoDebito(0);
		request.setCdMidiaDisponivel(0);
		request.setCdMidiaMensagemRecadastro(responseDetalhar.getCdMidiaMensagemRecadastro());
		request.setCdMomentoAvisoRacadastro(responseDetalhar.getCdMomentoAvisoRacadastro());
		request.setCdMomentoCreditoEfetivacao(responseDetalhar.getCdMomentoCreditoEfetivacao());
		request.setCdMomentoDebitoPagamento(responseDetalhar.getCdMomentoDebitoPagamento());
		request.setCdMomentoFormularioRecadastro(responseDetalhar.getCdMomentoFormularioRecadastro());
		request.setCdMomentoProcessamentoPagamento(responseDetalhar.getCdMomentoProcessamentoPagamento());
		request.setCdMensagemRecadastroMidia(responseDetalhar.getCdMensagemRecadastroMidia());
		request.setCdPermissaoDebitoOnline(responseDetalhar.getCdPermissaoDebitoOnline());
		request.setCdNaturezaOperacaoPagamento(responseDetalhar.getCdNaturezaOperacaoPagamento());
		request.setCdIndicadorFeriadoLocal(verificaIntegerNulo(entradaDTO.getCdIndicadorFeriadoLocal()));
		request.setCdIndicadorSegundaLinha(responseDetalhar.getCdIndicadorSegundaLinha());
		request.setCdPeriodicidadeAviso(responseDetalhar.getCdPeriodicidadeAviso());
		request.setCdPerdcComprovante(responseDetalhar.getCdPerdcComprovante());
		request.setCdPerdcConsultaVeiculo(responseDetalhar.getCdPerdcConsultaVeiculo());
		request.setCdPerdcEnvioRemessa(responseDetalhar.getCdPerdcEnvioRemessa());
		request.setCdPerdcManutencaoProcd(responseDetalhar.getCdPerdcManutencaoProcd());
		request.setCdPagamentoNaoUtilizado(responseDetalhar.getCdPagamentoNaoUtilizado());
		request.setCdPrincipalEnquaRecadastro(responseDetalhar.getCdPrincipalEnquaRecadastro());
		request.setCdPrioridadeEfetivacaoPagamento(responseDetalhar.getCdPrioridadeEfetivacaoPagamento());
		request.setCdRejeicaoAgendaLote(responseDetalhar.getCdRejeicaoAgendaLote());
		request.setCdRejeicaoEfetivacaoLote(responseDetalhar.getCdRejeicaoEfetivacaoLote());
		request.setCdRejeicaoLote(responseDetalhar.getCdRejeicaoLote());
		request.setCdRastreabilidadeNotaFiscal(responseDetalhar.getCdRastreabilidadeNotaFiscal());
		request.setCdRastreabilidadeTituloTerceiro(responseDetalhar.getCdRastreabilidadeTituloTerceiro());
		request.setCdTipoCargaRecadastro(responseDetalhar.getCdTipoCargaRecadastro());
		request.setCdTipoCataoSalario(responseDetalhar.getCdTipoCataoSalario());
		request.setCdTipoDataFloat(responseDetalhar.getCdTipoDataFloat());
		request.setCdTipoDivergenciaVeiculo(responseDetalhar.getCdTipoDivergenciaVeiculo());
		request.setCdTipoIdBeneficio(responseDetalhar.getCdTipoIdBeneficio());
		request.setCdContratoContaTransferencia(responseDetalhar.getCdContratoContaTransferencia());
		request.setCdUtilizacaoFavorecidoControle(responseDetalhar.getCdUtilizacaoFavorecidoControle());
		request.setDtEnquaContaSalario(responseDetalhar.getDtEnquaContaSalario());
		request.setDtFimRecadastroBeneficio(entradaDTO.getDtFimRecadastroBeneficio());
		request.setDtInicioRecadastroBeneficio(entradaDTO.getDtInicioRecadastroBeneficio());
		request.setDtLimiteVinculoCarga(entradaDTO.getDtLimiteVinculoCarga());
		request.setCdPercentualMaximoInconLote(responseDetalhar.getCdPercentualMaximoInconLote());
		request.setQtAntecedencia(responseDetalhar.getQtAntecedencia());
		request.setQtAnteriorVencimentoComprovado(responseDetalhar.getQtAnteriorVencimentoComprovado());
		request.setQtDiaExpiracao(responseDetalhar.getQtDiaExpiracao());
		request.setCdDiaFloatPagamento(responseDetalhar.getCdDiaFloatPagamento());
		request.setQtDiaInatividadeFavorecido(responseDetalhar.getQtDiaInatividadeFavorecido());
		request.setQtDiaRepiqConsulta(responseDetalhar.getQtDiaRepiqConsulta());
		request.setQtEtapasRecadastroBeneficio(responseDetalhar.getQtEtapasRecadastroBeneficio());
		request.setQtFaseRecadastroBeneficio(responseDetalhar.getQtFaseRecadastroBeneficio());
		request.setQtLimiteLinha(responseDetalhar.getQtLimiteLinha());
		request.setQtLimiteSolicitacaoCatao(responseDetalhar.getQtLimiteSolicitacaoCatao());
		request.setQtMaximaInconLote(responseDetalhar.getQtMaximaInconLote());
		request.setQtMaximaTituloVencido(responseDetalhar.getQtMaximaTituloVencido());
		request.setQtMesComprovante(responseDetalhar.getQtMesComprovante());
		request.setQtMesEtapaRecadastro(responseDetalhar.getQtMesEtapaRecadastro());
		request.setQtMesFaseRecadastro(responseDetalhar.getQtMesFaseRecadastro());
		request.setQtViaAviso(responseDetalhar.getQtViaAviso());
		request.setQtViaCobranca(responseDetalhar.getQtViaCobranca());
		request.setQtViaComprovante(responseDetalhar.getQtViaComprovante());
		request.setVlFavorecidoNaoCadastro(Double.parseDouble(String.valueOf(responseDetalhar.getVlFavorecidoNaoCadastro())));
		request.setVlLimiteDiaPagamento(Double.parseDouble(String.valueOf(responseDetalhar.getVlLimiteDiaPagamento())));
				
		if (Integer.valueOf(1).equals(entradaDTO.getRdoPermiteAcertosDados())) {
			request.setDtInicioAcertoRecadastro(entradaDTO.getDtInicioAcertoRecadastro());
			request.setDtFimAcertoRecadastro(entradaDTO.getDtFimAcertoRecadastro());
		} else {
			request.setDtInicioAcertoRecadastro("");
			request.setDtFimAcertoRecadastro("");
		}

		request.setDtInicioBloqueioPapeleta(responseDetalhar.getDtInicioBloqueioPplta());
		request.setDsAreaReservada(responseDetalhar.getDsAreaReservada());

		request.setCdIndicadorRetornoInternet(verificaIntegerNulo(responseDetalhar.getCdIndicadorRetornoInternet()));
		request.setCdIndicadorListaDebito(verificaIntegerNulo(responseDetalhar.getCdIndicadorListaDebito()));
		request.setCdTipoFormacaoLista(verificaIntegerNulo(responseDetalhar.getCdTipoFormacaoLista()));
		request.setCdTipoConsistenciaLista(verificaIntegerNulo(responseDetalhar.getCdTipoConsistenciaLista()));
		request.setCdTipoConsultaComprovante(verificaIntegerNulo(responseDetalhar.getCdTipoConsultaComprovante()));
		request.setCdIndicadorBancoPostal(verificaIntegerNulo(responseDetalhar.getCdIndicadorBancoPostal()));
		request.setCdValidacaoNomeFavorecido(verificaIntegerNulo(responseDetalhar.getCdValidacaoNomeFavorecido()));
		request.setCdTipoContaFavorecido(verificaIntegerNulo(responseDetalhar.getCdTipoContaFavorecido()));
		request.setCdIndLancamentoPersonalizado(verificaIntegerNulo(entradaDTO.getCdIndLancamentoPersonalizado()));
		request.setCdTipoIsncricaoFavorecido(verificaIntegerNulo(responseDetalhar.getCdTipoIsncricaoFavorecido()));
		request.setCdMeioPagamentoCredito(verificaIntegerNulo(responseDetalhar.getCdMeioPagamentoCredito()));
		request.setCdAgendaRastreabilidadeFilial(verificaIntegerNulo(responseDetalhar
				.getCdAgendaRastreabilidadeFilial()));
		request.setCdIndicadorAdesaoSacador(verificaIntegerNulo(responseDetalhar.getCdIndicadorAdesaoSacador()));
		request.setCdFormularioContratoCliente(verificaIntegerNulo(responseDetalhar.getCdFormularioContratoCliente()));
		request.setCdusuario(verificaStringNula(entradaDTO.getCdUsuario()));
		request.setCdUsuarioExterno(verificaStringNula(entradaDTO.getCdUsuarioExterno()));
		request.setCdCanal(verificaIntegerNulo(entradaDTO.getCdCanal()));
		request.setNmOperacaoFluxo(verificaStringNula(entradaDTO.getNmOperacaoFluxo()));
		request.setCdEmpresaOperante(verificaLongNulo(entradaDTO.getCdEmpresaOperante()));
		request.setCdUsuarioExterno(verificaStringNula(entradaDTO.getCdUsuarioExterno()));
		request.setCdDependenteOperante(verificaIntegerNulo(entradaDTO.getCdDependenteOperante()));
		request.setCdIndicadorSegundaLinha(verificaIntegerNulo(entradaDTO.getCdIndicadorSegundaLinha()));
		request.setCdPreenchimentoLancamentoPersonalizado(0);
		request.setCdIndicadorAgendaGrade(0);
		request.setCdTituloDdaRetorno(0);
		request.setVlPercentualDiferencaTolerada(Double.parseDouble(String.valueOf(BigDecimal.ZERO)));

		/**************************************** TIPO DE SERVI�O *******************************************/

		// Pagamento de Fornecedores
		if (entradaDTO.getCdParametroTela() == 1) {
			request.setCdFormaEnvioPagamento(verificaIntegerNulo(entradaDTO.getCdFormaEnvioPagamento()));
			request.setCdFormaAutorizacaoPagamento(verificaIntegerNulo(entradaDTO.getCdFormaAutorizacaoPagamento()));
			request.setCdIndicadorAutorizacaoCliente(verificaIntegerNulo(entradaDTO
					.getCdIndicadorAutorizacaoCliente()));
			request.setCdIndicadorAutorizacaoComplemento(verificaIntegerNulo(entradaDTO
					.getCdIndicadorAutorizacaoComplemento()));
			request.setCdTipoDataFloat(verificaIntegerNulo(entradaDTO.getCdTipoDataFloat()));
			request.setCdIndicadorListaDebito(verificaIntegerNulo(entradaDTO.getCdIndicadorListaDebito()));
			request.setCdTipoFormacaoLista(verificaIntegerNulo(entradaDTO.getCdTipoFormacaoLista()));
			request.setCdTipoConsistenciaLista(verificaIntegerNulo(entradaDTO.getCdTipoConsistenciaLista()));
			request.setCdIndicadorRetornoInternet(verificaIntegerNulo(entradaDTO.getCdIndicadorRetornoInternet()));
			request.setCdIndicadorSeparaCanal(verificaIntegerNulo(entradaDTO.getCdIndicadorSeparaCanal()));
			request.setCdTipoConsultaComprovante(verificaIntegerNulo(entradaDTO.getCdTipoConsultaComprovante()));
			request.setCdIndicadorTipoRetornoInternet(verificaIntegerNulo(entradaDTO.getCdIndicadorTipoRetornoInternet()));
		}

		// Pagamento de Sal�rios
		if (entradaDTO.getCdParametroTela() == 2) {

			request.setCdFormaEnvioPagamento(verificaIntegerNulo(entradaDTO.getCdFormaEnvioPagamento()));
			request.setCdFormaAutorizacaoPagamento(verificaIntegerNulo(entradaDTO
							.getCdFormaAutorizacaoPagamento()));
			request
					.setCdIndicadorAutorizacaoCliente(verificaIntegerNulo(entradaDTO
							.getCdIndicadorAutorizacaoCliente()));
			request
					.setCdIndicadorAutorizacaoComplemento(verificaIntegerNulo(entradaDTO
							.getCdIndicadorAutorizacaoComplemento()));
			request.setCdTipoDataFloat(verificaIntegerNulo(entradaDTO
					.getCdTipoDataFloat()));
			request.setCdIndicadorListaDebito(verificaIntegerNulo(entradaDTO
					.getCdIndicadorListaDebito()));
			request.setCdTipoFormacaoLista(verificaIntegerNulo(entradaDTO
					.getCdTipoFormacaoLista()));
			request.setCdTipoConsistenciaLista(verificaIntegerNulo(entradaDTO
					.getCdTipoConsistenciaLista()));
			request
					.setCdIndicadorRetornoInternet(verificaIntegerNulo(entradaDTO
							.getCdIndicadorRetornoInternet()));
			request.setCdIndicadorSeparaCanal(verificaIntegerNulo(entradaDTO
					.getCdIndicadorSeparaCanal()));
			request.setCdTipoConsultaComprovante(verificaIntegerNulo(entradaDTO
					.getCdTipoConsultaComprovante()));
			request.setDtEnquaContaSalario(verificaStringNula(entradaDTO
					.getDtEnquaContaSalario()));
			request.setQtLimiteSolicitacaoCatao(verificaIntegerNulo(entradaDTO
					.getQtLimiteSolicitacaoCatao()));
			request.setCdTipoCataoSalario(verificaIntegerNulo(entradaDTO
					.getCdTipoCataoSalario()));
			request.setCdIndicadorCataoSalario(verificaIntegerNulo(entradaDTO
					.getCdIndicadorCataoSalario()));
			request.setCdIndicadorBancoPostal(verificaIntegerNulo(entradaDTO
					.getCdIndicadorBancoPostal()));
			request.setCdIndicadorTipoRetornoInternet(verificaIntegerNulo(entradaDTO.getCdIndicadorTipoRetornoInternet()));
		}

		// Pagamentos de Tributos e Contas de Consumo
		if (entradaDTO.getCdParametroTela() == 3) {
			request.setCdFormaEnvioPagamento(verificaIntegerNulo(entradaDTO
					.getCdFormaEnvioPagamento()));
			request
					.setCdFormaAutorizacaoPagamento(verificaIntegerNulo(entradaDTO
							.getCdFormaAutorizacaoPagamento()));
			request
					.setCdIndicadorAutorizacaoCliente(verificaIntegerNulo(entradaDTO
							.getCdIndicadorAutorizacaoCliente()));
			request
					.setCdIndicadorAutorizacaoComplemento(verificaIntegerNulo(entradaDTO
							.getCdIndicadorAutorizacaoComplemento()));
			request
					.setCdIndicadorRetornoInternet(verificaIntegerNulo(entradaDTO
							.getCdIndicadorRetornoInternet()));
			request.setCdIndicadorSeparaCanal(verificaIntegerNulo(entradaDTO
					.getCdIndicadorSeparaCanal()));
			request.setCdTipoDataFloat(verificaIntegerNulo(entradaDTO
					.getCdTipoDataFloat()));
			request.setCdIndicadorListaDebito(verificaIntegerNulo(entradaDTO
					.getCdIndicadorListaDebito()));
			request.setCdTipoFormacaoLista(verificaIntegerNulo(entradaDTO
					.getCdTipoFormacaoLista()));
			request.setCdTipoConsistenciaLista(verificaIntegerNulo(entradaDTO
					.getCdTipoConsistenciaLista()));
			request.setCdTipoConsultaComprovante(verificaIntegerNulo(entradaDTO
					.getCdTipoConsultaComprovante()));
			request.setCdConsDebitoVeiculo(verificaIntegerNulo(entradaDTO
					.getCdConsDebitoVeiculo()));
			request.setCdPerdcConsultaVeiculo(verificaIntegerNulo(entradaDTO
					.getCdPerdcConsultaVeiculo()));
			request.setCdAgendaDebitoVeiculo(verificaIntegerNulo(entradaDTO
					.getCdAgendaDebitoVeiculo()));
			request.setCdIndicadorTipoRetornoInternet(verificaIntegerNulo(entradaDTO.getCdIndicadorTipoRetornoInternet()));
		}

		// Pagamento de Benef�cios
		if (entradaDTO.getCdParametroTela() == 4) {

			request.setCdFormaEnvioPagamento(verificaIntegerNulo(entradaDTO
					.getCdFormaEnvioPagamento()));
			request
					.setCdFormaAutorizacaoPagamento(verificaIntegerNulo(entradaDTO
							.getCdFormaAutorizacaoPagamento()));
			request
					.setCdIndicadorAutorizacaoCliente(verificaIntegerNulo(entradaDTO
							.getCdIndicadorAutorizacaoCliente()));
			request
					.setCdIndicadorAutorizacaoComplemento(verificaIntegerNulo(entradaDTO
							.getCdIndicadorAutorizacaoComplemento()));
			request.setCdTipoDataFloat(verificaIntegerNulo(entradaDTO
					.getCdTipoDataFloat()));
			request.setCdIndicadorListaDebito(verificaIntegerNulo(entradaDTO
					.getCdIndicadorListaDebito()));
			request.setCdTipoFormacaoLista(verificaIntegerNulo(entradaDTO
					.getCdTipoFormacaoLista()));
			request.setCdTipoConsistenciaLista(verificaIntegerNulo(entradaDTO
					.getCdTipoConsistenciaLista()));
			request
					.setCdIndicadorRetornoInternet(verificaIntegerNulo(entradaDTO
							.getCdIndicadorRetornoInternet()));
			request.setCdIndicadorSeparaCanal(verificaIntegerNulo(entradaDTO
					.getCdIndicadorSeparaCanal()));
			request.setCdTipoConsultaComprovante(verificaIntegerNulo(entradaDTO
					.getCdTipoConsultaComprovante()));
			request.setCdTipoIdBeneficio(verificaIntegerNulo(entradaDTO
					.getCdTipoIdBeneficio()));
			request.setCdIndicadorCadastroOrg(verificaIntegerNulo(entradaDTO
					.getCdIndicadorCadastroOrg()));
			request.setCdIndicadorCadastroProcd(verificaIntegerNulo(entradaDTO
					.getCdIndicadorCadastroProcd()));
			request.setCdFormaManutencao(verificaIntegerNulo(entradaDTO
					.getCdFormaManutencao()));
			request.setCdPerdcManutencaoProcd(verificaIntegerNulo(entradaDTO
					.getCdPerdcManutencaoProcd()));
			request.setCdDispzContaCredito(verificaIntegerNulo(entradaDTO
					.getCdDispzContaCredito()));
			request
					.setCdIndicadorExpiracaoCredito(verificaIntegerNulo(entradaDTO
							.getCdIndicadorExpiracaoCredito()));
			request.setCdFormaExpiracaoCredito(verificaIntegerNulo(entradaDTO
					.getCdFormaExpiracaoCredito()));
			request.setQtDiaExpiracao(verificaIntegerNulo(entradaDTO
					.getQtDiaExpiracao()));
			request
					.setCdMomentoCreditoEfetivacao(verificaIntegerNulo(entradaDTO
							.getCdMomentoCreditoEfetivacao()));
			request.setCdCreditoNaoUtilizado(verificaIntegerNulo(entradaDTO
					.getCdCreditoNaoUtilizado()));
		}

		// Cadastro de Favorecido
		if (entradaDTO.getCdParametroTela() == 5) {
			request.setCdFormaManutencao(entradaDTO.getCdFormaManutencao());
			request.setQtDiaInatividadeFavorecido(entradaDTO
					.getQtDiaInatividadeFavorecido());
			request.setCdCtciaInscricaoFavorecido(entradaDTO
					.getCdCtciaInscricaoFavorecido());
			request.setCdIndicadorRetornoInternet(entradaDTO
					.getCdIndicadorRetornoInternet());
		}

		// Aviso de Movimenta��o de D�bito
		// Aviso de Movimenta��o de Cr�dito
		// Aviso de Movimenta��o ao Pagador
		// Aviso de Movimenta��o ao Favorecido
		if (entradaDTO.getCdParametroTela() >= 6
				&& entradaDTO.getCdParametroTela() <= 9) {
			request.setCdPeriodicidadeAviso(verificaIntegerNulo(entradaDTO
					.getCdPeriodicidadeAviso()));
			request.setQtViaAviso(verificaIntegerNulo(entradaDTO
					.getQtViaAviso()));
			request.setCdDestinoAviso(verificaIntegerNulo(entradaDTO
					.getCdDestinoAviso()));
			request.setCdEnvelopeAberto(verificaIntegerNulo(entradaDTO
					.getCdEnvelopeAberto()));
			request.setCdAgrupamentoAviso(verificaIntegerNulo(entradaDTO
					.getCdAgrupamentoAviso()));
			request.setCdAreaReservada(verificaIntegerNulo(entradaDTO
					.getCdAreaReservada()));
			request.setDsAreaReservada(verificaStringNula(entradaDTO
					.getDsAreaReservada()));
			request.setCdConsEndereco(verificaIntegerNulo(entradaDTO
					.getCdConsEndereco()));
		}
		// Comprovante Pagamento ao Pagador
		// Comprovante Pagamento ao Favorecido
		if (entradaDTO.getCdParametroTela() == 10
				|| entradaDTO.getCdParametroTela() == 11) {
			request.setCdPerdcComprovante(verificaIntegerNulo(entradaDTO
					.getCdPerdcComprovante()));
			request.setQtViaComprovante(verificaIntegerNulo(entradaDTO
					.getQtViaComprovante()));
			request.setCdDestinoComprovante(verificaIntegerNulo(entradaDTO
					.getCdDestinoComprovante()));
			request.setCdEnvelopeAberto(verificaIntegerNulo(entradaDTO
					.getCdEnvelopeAberto()));
			request.setCdAgrupamentoComprovado(verificaIntegerNulo(entradaDTO
					.getCdAgrupamentoComprovado()));
			request.setCdAreaReservada(verificaIntegerNulo(entradaDTO
					.getCdAreaReservada()));
			request.setDsAreaReservada(verificaStringNula(entradaDTO
					.getDsAreaReservada()));
			request.setCdConsEndereco(verificaIntegerNulo(entradaDTO
					.getCdConsEndereco()));
		}

		// Emiss�o Comprovantes Diversos
		// Emiss�o Comprovante Salarial
		if (entradaDTO.getCdParametroTela() == 12
				|| entradaDTO.getCdParametroTela() == 13) {

			request.setCdRejeicaoLote(entradaDTO.getCdRejeicaoLote());
			request.setCdDIspzSalarioCrrtt(entradaDTO.getCdDIspzSalarioCrrtt());
			request.setCdMidiaDisponivel(entradaDTO.getCdMidiaDisponivel());
			request.setCdDispzSalarioNao(entradaDTO.getCdDispzSalarioNao());
			request.setCdDestinoComprovante(entradaDTO
					.getCdDestinoComprovante());
			request.setCdConsEndereco(entradaDTO.getCdConsEndereco());
			request.setCdFrasePreCadastro(entradaDTO.getCdFrasePreCadastro());
			request.setCdCobrancaTarifa(entradaDTO.getCdCobrancaTarifa());
			request.setQtMesComprovante(entradaDTO.getQtMesComprovante());
			request.setQtViaComprovante(entradaDTO.getQtViaComprovante());
			request.setQtViaCobranca(entradaDTO.getQtViaCobranca());
			request.setQtLimiteLinha(entradaDTO.getQtLimiteLinha());
			request.setCdFormularioContratoCliente(entradaDTO
					.getCdFormularioContratoCliente());
			request.setCdIndicadorRetornoInternet(entradaDTO
					.getCdIndicadorRetornoInternet());
		}

		// Recadastramento Benefici�rio
		if (entradaDTO.getCdParametroTela() == 14) {

			// novas
			request.setCdTipoIdBeneficio(entradaDTO.getCdTipoIdBeneficio());
			request.setCdCtciaIdentificacaoBeneficio(entradaDTO
					.getCdCtciaIdentificacaoBeneficio());
			request.setCdCriterioEnquaRecadastro(entradaDTO
					.getCdCriterioEnquaRecadastro());
			request.setCdPrincipalEnquaRecadastro(entradaDTO
					.getCdPrincipalEnquaRecadastro());
			request.setCdCriterioEnquaBeneficio(entradaDTO
					.getCdCriterioEnquaBeneficio());
			request.setQtEtapasRecadastroBeneficio(entradaDTO
					.getQtEtapasRecadastroBeneficio());
			request.setQtFaseRecadastroBeneficio(entradaDTO
					.getQtFaseRecadastroBeneficio());
			request.setQtMesEtapaRecadastro(entradaDTO
					.getQtMesEtapaRecadastro());
			request.setQtMesFaseRecadastro(entradaDTO.getQtMesFaseRecadastro());
			request.setCdBaseRecadastroBeneficio(entradaDTO
					.getCdBaseRecadastroBeneficio());
			request.setCdCriterioEnquaBeneficio(entradaDTO
					.getCdCriterioEnquaBeneficio());
			request.setDtInicioRecadastroBeneficio(entradaDTO
					.getDtInicioRecadastroBeneficio());
			request.setDtFimRecadastroBeneficio(entradaDTO
					.getDtFimRecadastroBeneficio());
			request.setCdManutencaoBaseRecadastro(entradaDTO
					.getCdManutencaoBaseRecadastro());
			request.setCdPerdcEnvioRemessa(entradaDTO.getCdPerdcEnvioRemessa());
			request.setCdTipoCargaRecadastro(entradaDTO
					.getCdTipoCargaRecadastro());
			request.setDtInicioRastreabilidadeTitulo(entradaDTO
					.getDtInicioRastreabilidadeTitulo());
			request.setCdAcertoDadoRecadastro(entradaDTO
					.getCdAcertoDadoRecadastro());
			request.setCdAntecRecadastroBeneficio(entradaDTO
					.getCdAntecRecadastroBeneficio());
			if (Integer.valueOf(1).equals(
					entradaDTO.getRdoPermiteAcertosDados())) {
				request.setDtInicioAcertoRecadastro(entradaDTO
						.getDtInicioAcertoRecadastro());
				request.setDtFimAcertoRecadastro(entradaDTO
						.getDtFimAcertoRecadastro());
			} else {
				request.setDtInicioAcertoRecadastro("");
				request.setDtFimAcertoRecadastro("");
			}
			request.setCdMensagemRecadastroMidia(entradaDTO
					.getCdMensagemRecadastroMidia());
			request.setCdMidiaMensagemRecadastro(entradaDTO
					.getCdMidiaMensagemRecadastro());
			request.setCdIndicadorRetornoInternet(entradaDTO
					.getCdIndicadorRetornoInternet());

		}
		/**************************************** MODALIDADE *******************************************/

		// Aviso de Recadastramento
		if (entradaDTO.getCdParametroTela() == 15) {

			request.setCdMomentoAvisoRacadastro(verificaIntegerNulo(entradaDTO
					.getCdMomentoAvisoRacadastro()));
			request.setCdDestinoAviso(verificaIntegerNulo(entradaDTO
					.getCdDestinoAviso()));
			request.setCdConsEndereco(verificaIntegerNulo(entradaDTO
					.getCdConsEndereco()));
			request.setCdAgrupamentoAviso(verificaIntegerNulo(entradaDTO
					.getCdAgrupamentoAviso()));
			request.setQtAntecedencia(verificaIntegerNulo(entradaDTO
					.getQtAntecedencia()));
		}
		// Formul�rio de Recadastramento
		if (entradaDTO.getCdParametroTela() == 16) {
			request.setCdMomentoFormularioRecadastro(entradaDTO
					.getCdMomentoFormularioRecadastro());
			request.setQtAntecedencia(entradaDTO.getQtAntecedencia());
			request.setCdDestinoFormularioRecadastro(entradaDTO
					.getCdDestinoFormularioRecadastro());
			request.setCdConsEndereco(entradaDTO.getCdConsEndereco());
			request.setCdFormularioContratoCliente(entradaDTO
					.getCdFormularioContratoCliente());
			request.setCdAgrupamentoFormularioRecadastro(entradaDTO
					.getCdAgrupamentoFormularioRecadastro());
		}
		// Pagamento de Fornecedor - TED
		// Pagamento de Fornecedor - DOC
		// Pagamento Via Dep�sito Identificado
		// Ordem de Credito
		if (entradaDTO.getCdParametroTela() == 17
				|| entradaDTO.getCdParametroTela() == 18
				|| entradaDTO.getCdParametroTela() == 24
				|| entradaDTO.getCdParametroTela() == 25) {
			request.setCdDiaFloatPagamento(verificaIntegerNulo(entradaDTO
					.getCdDiaFloatPagamento()));
			request
					.setCdMomentoProcessamentoPagamento(verificaIntegerNulo(entradaDTO
							.getCdMomentoProcessamentoPagamento()));
			request.setCdRejeicaoAgendaLote(verificaIntegerNulo(entradaDTO
					.getCdRejeicaoAgendaLote()));
			request.setCdRejeicaoEfetivacaoLote(verificaIntegerNulo(entradaDTO
					.getCdRejeicaoEfetivacaoLote()));
			request.setQtMaximaInconLote(verificaIntegerNulo(entradaDTO
					.getQtMaximaInconLote()));
			request
					.setCdPercentualMaximoInconLote(verificaIntegerNulo(entradaDTO
							.getCdPercentualMaximoInconLote()));
			request
					.setCdPrioridadeEfetivacaoPagamento(verificaIntegerNulo(entradaDTO
							.getCdPrioridadeEfetivacaoPagamento()));
			request.setCdValidacaoNomeFavorecido(verificaIntegerNulo(entradaDTO
					.getCdValidacaoNomeFavorecido()));
			request
					.setCdUtilizacaoFavorecidoControle(verificaIntegerNulo(entradaDTO
							.getCdUtilizacaoFavorecidoControle()));
			request.setVlFavorecidoNaoCadastro(Double.parseDouble(String.valueOf(verificaBigDecimalNulo(entradaDTO.getVlFavorecidoNaoCadastro()))));
			request.setCdConsSaldoPagamento(verificaIntegerNulo(entradaDTO
					.getCdConsSaldoPagamento()));
			request.setQtDiaRepiqConsulta(verificaIntegerNulo(entradaDTO
					.getQtDiaRepiqConsulta()));
			request.setCdFavorecidoConsPagamento(verificaIntegerNulo(entradaDTO
					.getCdFavorecidoConsPagamento()));
			request.setCdLancamentoFuturoDebito(verificaIntegerNulo(entradaDTO
					.getCdLancamentoFuturoDebito()));
			// falta um campo
			request.setCdTipoContaFavorecido(verificaIntegerNulo(entradaDTO
					.getCdTipoContaFavorecido()));
			request.setCdPagamentoNaoUtilizado(verificaIntegerNulo(entradaDTO
					.getCdPagamentoNaoUtilizado()));
			request.setVlLimiteIndividualPagamento(Double.parseDouble(String.valueOf(verificaBigDecimalNulo(entradaDTO.getVlLimiteIndividualPagamento()))));
			request.setVlLimiteDiaPagamento(Double.parseDouble(String.valueOf(verificaBigDecimalNulo(entradaDTO.getVlLimiteDiaPagamento()))));

			if (entradaDTO.getCdParametroTela() == 17
					|| entradaDTO.getCdParametroTela() == 18) {
				request
						.setCdIndicadorSegundaLinha(verificaIntegerNulo(entradaDTO
								.getCdIndicadorSegundaLinha()));
			}

            if (entradaDTO.getCdParametroTela() == 18) {
                request.setCdIndicadorAgendaGrade(verificaIntegerNulo(entradaDTO.getCdIndicadorAgendaGrade()));
            }
            
            request.setCindcdFantsRepas(verificaIntegerNulo(entradaDTO.getCindcdFantsRepas()));
            
		}
		// Pagamento Via Ordem de Pagamento
		if (entradaDTO.getCdParametroTela() == 19) {
			request.setCdDiaFloatPagamento(verificaIntegerNulo(entradaDTO
					.getCdDiaFloatPagamento()));
			request
					.setCdMomentoProcessamentoPagamento(verificaIntegerNulo(entradaDTO
							.getCdMomentoProcessamentoPagamento()));
			request.setCdRejeicaoAgendaLote(verificaIntegerNulo(entradaDTO
					.getCdRejeicaoAgendaLote()));
			request.setCdRejeicaoEfetivacaoLote(verificaIntegerNulo(entradaDTO
					.getCdRejeicaoEfetivacaoLote()));
			request.setQtMaximaInconLote(verificaIntegerNulo(entradaDTO
					.getQtMaximaInconLote()));
			request
					.setCdPercentualMaximoInconLote(verificaIntegerNulo(entradaDTO
							.getCdPercentualMaximoInconLote()));
			request
					.setCdPrioridadeEfetivacaoPagamento(verificaIntegerNulo(entradaDTO
							.getCdPrioridadeEfetivacaoPagamento()));
			request.setCdValidacaoNomeFavorecido(verificaIntegerNulo(entradaDTO
					.getCdValidacaoNomeFavorecido()));
			request
					.setCdUtilizacaoFavorecidoControle(verificaIntegerNulo(entradaDTO
							.getCdUtilizacaoFavorecidoControle()));
			request
					.setVlFavorecidoNaoCadastro(Double.parseDouble(String.valueOf(verificaBigDecimalNulo(entradaDTO.getVlFavorecidoNaoCadastro()))));
			request.setCdConsSaldoPagamento(verificaIntegerNulo(entradaDTO
					.getCdConsSaldoPagamento()));
			request.setQtDiaRepiqConsulta(verificaIntegerNulo(entradaDTO
					.getQtDiaRepiqConsulta()));
			request.setCdFavorecidoConsPagamento(verificaIntegerNulo(entradaDTO
					.getCdFavorecidoConsPagamento()));
			request.setCdLancamentoFuturoDebito(verificaIntegerNulo(entradaDTO
					.getCdLancamentoFuturoDebito()));
			// falta um campo
			request.setCdTipoContaFavorecido(verificaIntegerNulo(entradaDTO
					.getCdTipoContaFavorecido()));
			request.setCdPagamentoNaoUtilizado(verificaIntegerNulo(entradaDTO
					.getCdPagamentoNaoUtilizado()));
			request.setCdMomentoDebitoPagamento(verificaIntegerNulo(entradaDTO
					.getCdMomentoDebitoPagamento()));
			request.setCdTipoIsncricaoFavorecido(verificaIntegerNulo(entradaDTO
					.getCdTipoIsncricaoFavorecido()));
			request.setQtDiaExpiracao(verificaIntegerNulo(entradaDTO
					.getQtdeDiasExpiracaoCredito()));
			request.setVlLimiteIndividualPagamento(Double.parseDouble(String.valueOf(verificaBigDecimalNulo(entradaDTO.getVlLimiteIndividualPagamento()))));
			request.setVlLimiteDiaPagamento(Double.parseDouble(String.valueOf(verificaBigDecimalNulo(entradaDTO.getVlLimiteDiaPagamento()))));
			request.setCdIndicadorSegundaLinha(verificaIntegerNulo(entradaDTO
					.getCdIndicadorSegundaLinha()));
		}

		// Pagamento Via Cr�dito em Conta
		if (entradaDTO.getCdParametroTela() == 20) {
			request.setCdDiaFloatPagamento(verificaIntegerNulo(entradaDTO
					.getCdDiaFloatPagamento()));
			request
					.setCdMomentoProcessamentoPagamento(verificaIntegerNulo(entradaDTO
							.getCdMomentoProcessamentoPagamento()));
			request.setCdRejeicaoAgendaLote(verificaIntegerNulo(entradaDTO
					.getCdRejeicaoAgendaLote()));
			request.setCdRejeicaoEfetivacaoLote(verificaIntegerNulo(entradaDTO
					.getCdRejeicaoEfetivacaoLote()));
			request.setQtMaximaInconLote(verificaIntegerNulo(entradaDTO
					.getQtMaximaInconLote()));
			request
					.setCdPercentualMaximoInconLote(verificaIntegerNulo(entradaDTO
							.getCdPercentualMaximoInconLote()));
			request
					.setCdPrioridadeEfetivacaoPagamento(verificaIntegerNulo(entradaDTO
							.getCdPrioridadeEfetivacaoPagamento()));
			request
					.setCdUtilizacaoFavorecidoControle(verificaIntegerNulo(entradaDTO
							.getCdUtilizacaoFavorecidoControle()));
			request.setVlFavorecidoNaoCadastro(Double.parseDouble(String.valueOf(verificaBigDecimalNulo(entradaDTO.getVlFavorecidoNaoCadastro()))));
			request.setCdConsSaldoPagamento(verificaIntegerNulo(entradaDTO
					.getCdConsSaldoPagamento()));
			request.setQtDiaRepiqConsulta(verificaIntegerNulo(entradaDTO
					.getQtDiaRepiqConsulta()));
			request.setCdFavorecidoConsPagamento(verificaIntegerNulo(entradaDTO
					.getCdFavorecidoConsPagamento()));
			request.setCdCtciaEspecieBeneficio(verificaIntegerNulo(entradaDTO
					.getCdCtciaEspecieBeneficio()));
			request.setCdTipoContaFavorecido(verificaIntegerNulo(entradaDTO
					.getCdTipoContaFavorecido()));
			request.setCdLancamentoFuturoDebito(verificaIntegerNulo(entradaDTO
					.getCdLancamentoFuturoDebito()));
			request.setCdLancamentoFuturoCredito(verificaIntegerNulo(entradaDTO
					.getCdLancamentoFuturoCredito()));
			request
					.setCdIndicadorLancamentoPagamento(verificaIntegerNulo(entradaDTO
							.getCdIndicadorLancamentoPagamento()));
			request.setCdPagamentoNaoUtilizado(verificaIntegerNulo(entradaDTO
					.getCdPagamentoNaoUtilizado()));
			request
					.setCdContratoContaTransferencia(verificaIntegerNulo(entradaDTO
							.getCdContratoContaTransferencia()));
			request.setCdValidacaoNomeFavorecido(verificaIntegerNulo(entradaDTO
					.getCdValidacaoNomeFavorecido()));
			request.setCdLiberacaoLoteProcesso(verificaIntegerNulo(entradaDTO
					.getCdLiberacaoLoteProcesso()));
			request.setVlLimiteIndividualPagamento(Double.parseDouble(String.valueOf(verificaBigDecimalNulo(entradaDTO.getVlLimiteIndividualPagamento()))));
			request.setVlLimiteDiaPagamento(Double.parseDouble(String.valueOf(verificaBigDecimalNulo(entradaDTO.getVlLimiteDiaPagamento()))));
			request.setCdMeioPagamentoCredito(verificaIntegerNulo(entradaDTO
					.getCdMeioPagamentoCredito()));
			request.setCdIndicadorSegundaLinha(verificaIntegerNulo(entradaDTO
					.getCdIndicadorSegundaLinha()));
			request.setCdPreenchimentoLancamentoPersonalizado(verificaIntegerNulo(entradaDTO
                    .getCdPreenchimentoLancamentoPersonalizado()));
		}

		// Pagamento Via D�bito em Conta
		if (entradaDTO.getCdParametroTela() == 21) {
			request.setCdDiaFloatPagamento(verificaIntegerNulo(entradaDTO
					.getCdDiaFloatPagamento()));
			request
					.setCdMomentoProcessamentoPagamento(verificaIntegerNulo(entradaDTO
							.getCdMomentoProcessamentoPagamento()));
			request.setCdRejeicaoAgendaLote(verificaIntegerNulo(entradaDTO
					.getCdRejeicaoAgendaLote()));
			request.setCdRejeicaoEfetivacaoLote(verificaIntegerNulo(entradaDTO
					.getCdRejeicaoEfetivacaoLote()));
			request.setQtMaximaInconLote(verificaIntegerNulo(entradaDTO
					.getQtMaximaInconLote()));
			request
					.setCdPercentualMaximoInconLote(verificaIntegerNulo(entradaDTO
							.getCdPercentualMaximoInconLote()));
			request
					.setCdPrioridadeEfetivacaoPagamento(verificaIntegerNulo(entradaDTO
							.getCdPrioridadeEfetivacaoPagamento()));
			request.setCdValidacaoNomeFavorecido(verificaIntegerNulo(entradaDTO
					.getCdValidacaoNomeFavorecido()));
			request
					.setCdUtilizacaoFavorecidoControle(verificaIntegerNulo(entradaDTO
							.getCdUtilizacaoFavorecidoControle()));
			request.setVlFavorecidoNaoCadastro(Double.parseDouble(String.valueOf(verificaBigDecimalNulo(entradaDTO.getVlFavorecidoNaoCadastro()))));
			request.setCdConsSaldoPagamento(verificaIntegerNulo(entradaDTO
					.getCdConsSaldoPagamento()));
			request.setQtDiaRepiqConsulta(verificaIntegerNulo(entradaDTO
					.getQtDiaRepiqConsulta()));
			request.setCdFavorecidoConsPagamento(verificaIntegerNulo(entradaDTO
					.getCdFavorecidoConsPagamento()));
			request.setCdPagamentoNaoUtilizado(verificaIntegerNulo(entradaDTO
					.getCdPagamentoNaoUtilizado()));
			// falta um campo
			request.setCdTipoContaFavorecido(verificaIntegerNulo(entradaDTO
					.getCdTipoContaFavorecido()));
			request.setCdLancamentoFuturoDebito(verificaIntegerNulo(entradaDTO
					.getCdLancamentoFuturoDebito()));
			request.setCdLancamentoFuturoCredito(verificaIntegerNulo(entradaDTO
					.getCdLancamentoFuturoCredito()));
			request.setCdPermissaoDebitoOnline(verificaIntegerNulo(entradaDTO
					.getRdoPermiteDebitoOnline()));
			request.setVlLimiteIndividualPagamento(Double.parseDouble(String.valueOf(verificaBigDecimalNulo(entradaDTO.getVlLimiteIndividualPagamento()))));
			request.setVlLimiteDiaPagamento(Double.parseDouble(String.valueOf(verificaBigDecimalNulo(entradaDTO.getVlLimiteDiaPagamento()))));
		}
		// Pagamento Via T�tulo Bradesco
		// Pagamento Via T�tulo Outros Bancos
		if (entradaDTO.getCdParametroTela() == 22
				|| entradaDTO.getCdParametroTela() == 23) {
			request.setCdDiaFloatPagamento(verificaIntegerNulo(entradaDTO
					.getCdDiaFloatPagamento()));
			request
					.setCdMomentoProcessamentoPagamento(verificaIntegerNulo(entradaDTO
							.getCdMomentoProcessamentoPagamento()));
			request.setCdRejeicaoAgendaLote(verificaIntegerNulo(entradaDTO
					.getCdRejeicaoAgendaLote()));
			request.setCdRejeicaoEfetivacaoLote(verificaIntegerNulo(entradaDTO
					.getCdRejeicaoEfetivacaoLote()));
			request.setQtMaximaInconLote(verificaIntegerNulo(entradaDTO
					.getQtMaximaInconLote()));
			request
					.setCdPercentualMaximoInconLote(verificaIntegerNulo(entradaDTO
							.getCdPercentualMaximoInconLote()));
			request
					.setCdPrioridadeEfetivacaoPagamento(verificaIntegerNulo(entradaDTO
							.getCdPrioridadeEfetivacaoPagamento()));
			request.setCdValidacaoNomeFavorecido(verificaIntegerNulo(entradaDTO
					.getCdValidacaoNomeFavorecido()));
			request
					.setCdUtilizacaoFavorecidoControle(verificaIntegerNulo(entradaDTO
							.getCdUtilizacaoFavorecidoControle()));
			request.setVlFavorecidoNaoCadastro(Double.parseDouble(String.valueOf(verificaBigDecimalNulo(entradaDTO.getVlFavorecidoNaoCadastro()))));
			request.setCdConsSaldoPagamento(verificaIntegerNulo(entradaDTO
					.getCdConsSaldoPagamento()));
			request.setQtDiaRepiqConsulta(verificaIntegerNulo(entradaDTO
					.getQtDiaRepiqConsulta()));
			request.setCdFavorecidoConsPagamento(verificaIntegerNulo(entradaDTO
					.getCdFavorecidoConsPagamento()));
			request.setCdLancamentoFuturoDebito(verificaIntegerNulo(entradaDTO
					.getCdLancamentoFuturoDebito()));
			// falta um campo
			request.setCdTipoContaFavorecido(entradaDTO
					.getCdTipoContaFavorecido());
			request.setCdPagamentoNaoUtilizado(verificaIntegerNulo(entradaDTO
					.getCdPagamentoNaoUtilizado()));
			request.setCdContagemConsSaudo(verificaIntegerNulo(entradaDTO
					.getPermiteContingenciaPag()));
			request.setCdAgendaPagamentoVencido(verificaIntegerNulo(entradaDTO
					.getCdAgendaPagamentoVencido()));
			request.setCdAgendaValorMenor(verificaIntegerNulo(entradaDTO
					.getCdAgendaValorMenor()));
			request.setQtMaximaTituloVencido(verificaIntegerNulo(entradaDTO
					.getQtMaximaTituloVencido()));
			request
					.setVlLimiteIndividualPagamento(Double.parseDouble(String.valueOf(verificaBigDecimalNulo(entradaDTO.getVlLimiteIndividualPagamento()))));
			request.setVlLimiteDiaPagamento(Double.parseDouble(String.valueOf(verificaBigDecimalNulo(entradaDTO.getVlLimiteDiaPagamento()))));
			request.setCdIndicadorSegundaLinha(Integer.parseInt(String.valueOf(verificaIntegerNulo(entradaDTO.getCdIndicadorSegundaLinha()))));
			request.setCdTituloDdaRetorno(verificaIntegerNulo(entradaDTO.getCdTituloDdaRetorno()));
            request.setVlPercentualDiferencaTolerada(Double.parseDouble(String.valueOf(verificaBigDecimalNulo(entradaDTO.getVlPercentualDiferencaTolerada()))));
            request.setCdConsistenciaCpfCnpjBenefAvalNpc(verificaIntegerNulo(entradaDTO.getCdConsistenciaCpfCnpjBenefAvalNpc()));
		}

		// Rastreamento de T�tulos
		if (entradaDTO.getCdParametroTela() == 26) {

			request
					.setCdCriterioRastreabilidadeTitulo(verificaIntegerNulo(entradaDTO
							.getCdCriterioRastreabilidadeTitulo()));
			request
					.setCdRastreabilidadeTituloTerceiro(verificaIntegerNulo(entradaDTO
							.getCdRastreabilidadeTituloTerceiro()));
			request
					.setCdRastreabilidadeNotaFiscal(verificaIntegerNulo(entradaDTO
							.getCdRastreabilidadeNotaFiscal()));
			request.setCdCapituloTituloRegistro(verificaIntegerNulo(entradaDTO
					.getCdCapituloTituloRegistro()));
			request.setCdIndicadorAgendaTitulo(verificaIntegerNulo(entradaDTO
					.getCdIndicadorAgendaTitulo()));
			request
					.setCdAgendaRastreabilidadeFilial(verificaIntegerNulo(entradaDTO
							.getCdAgendaRastreabilidadeFilial()));
			request.setCdBloqueioEmissaoPplta(verificaIntegerNulo(entradaDTO
					.getCdBloqueioEmissaoPplta()));
			request.setCdIndicadorAdesaoSacador(verificaIntegerNulo(entradaDTO
					.getCdIndicadorAdesaoSacador()));
			request
					.setDtInicioRastreabilidadeTitulo(verificaStringNula(entradaDTO
							.getDtInicioRastreabilidadeTitulo()));
			request.setDtInicioBloqueioPapeleta(verificaStringNula(entradaDTO
					.getDtInicioBloqueioPapeleta()));
		}

		// Prova de Vida
		if (entradaDTO.getCdParametroTela() == 27) {

			request.setCdAcaoNaoVida(verificaIntegerNulo(entradaDTO
					.getCdAcaoNaoVida()));
			request.setQtMesComprovante(verificaIntegerNulo(entradaDTO
					.getQtMesComprovante()));
			request.setCdDestinoComprovante(verificaIntegerNulo(entradaDTO
					.getCdDestinoComprovante()));
			request.setCdConsEndereco(verificaIntegerNulo(entradaDTO
					.getCdConsEndereco()));
			request.setCdIndicadorMensagemPerso(verificaIntegerNulo(entradaDTO
					.getCdIndicadorMensagemPerso()));
			request.setQtAntecedencia(verificaIntegerNulo(entradaDTO
					.getQtAntecedencia()));
			request
					.setQtAnteriorVencimentoComprovado(verificaIntegerNulo(entradaDTO
							.getQtAnteriorVencimentoComprovado()));
			request
					.setCdFormularioContratoCliente(verificaIntegerNulo(entradaDTO
							.getCdFormularioContratoCliente()));
		}

		// GPS
		// GARE
		// Pagamentos de Tributos - DARF
		// Codigo de Barras
		if (entradaDTO.getCdParametroTela() == 28
				|| entradaDTO.getCdParametroTela() == 29
				|| entradaDTO.getCdParametroTela() == 30
				|| entradaDTO.getCdParametroTela() == 31) {

			request.setCdDiaFloatPagamento(verificaIntegerNulo(entradaDTO
					.getCdDiaFloatPagamento()));
			request
					.setCdMomentoProcessamentoPagamento(verificaIntegerNulo(entradaDTO
							.getCdMomentoProcessamentoPagamento()));
			request.setCdRejeicaoAgendaLote(verificaIntegerNulo(entradaDTO
					.getCdRejeicaoAgendaLote()));
			request.setCdRejeicaoEfetivacaoLote(verificaIntegerNulo(entradaDTO
					.getCdRejeicaoEfetivacaoLote()));
			request.setQtMaximaInconLote(verificaIntegerNulo(entradaDTO
					.getQtMaximaInconLote()));
			request
					.setCdPercentualMaximoInconLote(verificaIntegerNulo(entradaDTO
							.getCdPercentualMaximoInconLote()));
			request
					.setCdPrioridadeEfetivacaoPagamento(verificaIntegerNulo(entradaDTO
							.getCdPrioridadeEfetivacaoPagamento()));
			request.setCdValidacaoNomeFavorecido(verificaIntegerNulo(entradaDTO
					.getCdValidacaoNomeFavorecido()));
			request
					.setCdUtilizacaoFavorecidoControle(verificaIntegerNulo(entradaDTO
							.getCdUtilizacaoFavorecidoControle()));
			request.setVlFavorecidoNaoCadastro(Double.parseDouble(String.valueOf(verificaBigDecimalNulo(entradaDTO.getVlFavorecidoNaoCadastro()))));
			request.setCdConsSaldoPagamento(verificaIntegerNulo(entradaDTO
					.getCdConsSaldoPagamento()));
			request.setQtDiaRepiqConsulta(verificaIntegerNulo(entradaDTO
					.getQtDiaRepiqConsulta()));
			request.setCdFavorecidoConsPagamento(verificaIntegerNulo(entradaDTO
					.getCdFavorecidoConsPagamento()));
			request.setCdLancamentoFuturoDebito(verificaIntegerNulo(entradaDTO
					.getCdLancamentoFuturoDebito()));
			// falta um campo
			request.setCdTipoContaFavorecido(verificaIntegerNulo(entradaDTO
					.getCdTipoContaFavorecido()));
			request.setCdPagamentoNaoUtilizado(verificaIntegerNulo(entradaDTO
					.getCdPagamentoNaoUtilizado()));
			request.setCdContagemConsSaudo(verificaIntegerNulo(entradaDTO
					.getPermiteContingenciaPag()));
			request.setVlLimiteIndividualPagamento(Double.parseDouble(String.valueOf(verificaBigDecimalNulo(entradaDTO.getVlLimiteIndividualPagamento()))));
			request.setVlLimiteDiaPagamento(Double.parseDouble(String.valueOf(verificaBigDecimalNulo(entradaDTO.getVlLimiteDiaPagamento()))));
			request.setCdIndicadorSegundaLinha(verificaIntegerNulo(entradaDTO
					.getCdIndicadorSegundaLinha()));
		}

		// D�bito de Ve�culos
		if (entradaDTO.getCdParametroTela() == 32) {

			request.setCdDiaFloatPagamento(verificaIntegerNulo(entradaDTO
					.getCdDiaFloatPagamento()));
			request
					.setCdMomentoProcessamentoPagamento(verificaIntegerNulo(entradaDTO
							.getCdMomentoProcessamentoPagamento()));
			request.setCdRejeicaoAgendaLote(verificaIntegerNulo(entradaDTO
					.getCdRejeicaoAgendaLote()));
			request.setCdRejeicaoEfetivacaoLote(verificaIntegerNulo(entradaDTO
					.getCdRejeicaoEfetivacaoLote()));
			request.setQtMaximaInconLote(verificaIntegerNulo(entradaDTO
					.getQtMaximaInconLote()));
			request
					.setCdPercentualMaximoInconLote(verificaIntegerNulo(entradaDTO
							.getCdPercentualMaximoInconLote()));
			request
					.setCdPrioridadeEfetivacaoPagamento(verificaIntegerNulo(entradaDTO
							.getCdPrioridadeEfetivacaoPagamento()));
			request.setCdValidacaoNomeFavorecido(verificaIntegerNulo(entradaDTO
					.getCdValidacaoNomeFavorecido()));
			request
					.setCdUtilizacaoFavorecidoControle(verificaIntegerNulo(entradaDTO
							.getCdUtilizacaoFavorecidoControle()));
			request.setVlFavorecidoNaoCadastro(Double.parseDouble(String.valueOf(verificaBigDecimalNulo(entradaDTO.getVlFavorecidoNaoCadastro()))));
			request.setCdConsSaldoPagamento(verificaIntegerNulo(entradaDTO
					.getCdConsSaldoPagamento()));
			request.setQtDiaRepiqConsulta(verificaIntegerNulo(entradaDTO
					.getQtDiaRepiqConsulta()));
			request.setCdFavorecidoConsPagamento(verificaIntegerNulo(entradaDTO
					.getCdFavorecidoConsPagamento()));
			request.setCdLancamentoFuturoDebito(verificaIntegerNulo(entradaDTO
					.getCdLancamentoFuturoDebito()));
			// falta um campo
			request.setCdTipoContaFavorecido(entradaDTO
					.getCdTipoContaFavorecido());
			request.setCdPagamentoNaoUtilizado(verificaIntegerNulo(entradaDTO
					.getCdPagamentoNaoUtilizado()));
			request.setCdContagemConsSaudo(verificaIntegerNulo(entradaDTO
					.getPermiteContingenciaPag()));
			request.setVlLimiteIndividualPagamento(Double.parseDouble(String.valueOf(verificaBigDecimalNulo(entradaDTO.getVlLimiteIndividualPagamento()))));
			request.setVlLimiteDiaPagamento(Double.parseDouble(String.valueOf(verificaBigDecimalNulo(entradaDTO.getVlLimiteDiaPagamento()))));
			request.setCdTipoDivergenciaVeiculo(verificaIntegerNulo(entradaDTO
					.getCdTipoDivergenciaVeiculo()));
			request
					.setCdCtciaProprietarioVeiculo(verificaIntegerNulo(entradaDTO
							.getCdCtciaProprietarioVeiculo()));
		}

		request.setDtInicioRastreabilidadeTitulo(verificaStringNula(entradaDTO
				.getDtInicioRastreabilidadeTitulo()));
		request.setDtFimRecadastroBeneficio(verificaStringNula(entradaDTO
				.getDtFimRecadastroBeneficio()));
		request.setDtInicioRecadastroBeneficio(verificaStringNula(entradaDTO
				.getDtInicioRecadastroBeneficio()));
		request.setDtLimiteVinculoCarga(verificaStringNula(entradaDTO
				.getDtLimiteVinculoCarga()));
		request.setVlLimiteDiaPagamento(Double.parseDouble(String.valueOf(verificaBigDecimalNulo(entradaDTO.getVlLimiteDiaPagamento()))));
		request.setVlLimiteIndividualPagamento(Double.parseDouble(String.valueOf(verificaBigDecimalNulo(entradaDTO.getVlLimiteIndividualPagamento()))));
		request.setCdIndicadorEmissaoAviso(verificaIntegerNulo(entradaDTO.getCdIndicadorEmissaoAviso()));
		request.setCdDispzDiversasNao(verificaIntegerNulo(entradaDTO.getCdDispzDiversasNao()));
		request.setCdIndicadorRetornoInternet(verificaIntegerNulo(entradaDTO.getCdIndicadorRetornoInternet()));
		request.setCdIndicadorSeparaCanal(verificaIntegerNulo(entradaDTO.getCdIndicadorSeparaCanal()));
		request.setCdIndicadorManutencaoProcurador(verificaIntegerNulo(entradaDTO
						.getCdIndicadorManutencaoProcurador()));
		request.setCdLocalEmissao(verificaIntegerNulo(entradaDTO
				.getCdLocalEmissao()));
		request.setCdConsultaSaldoValorSuperior(verificaIntegerNulo(entradaDTO
				.getCdConsultaSaldoValorSuperior()));
		request.setCdTipoParticipacaoPessoa(0);
		request.setCdPessoa(0);
		request.setCdSequenciaEnderecoPessoa(0);
		request.setCdTipoEnderecoPessoa(0);
		request.setCdEspecieEnderecoPessoa(0);
		request.setCdFloatServicoContrato(verificaIntegerNulo(entradaDTO.getCdFloatServicoContrato()));
		request.setCdIndicadorTipoRetornoInternet(verificaIntegerNulo(entradaDTO.getCdIndicadorTipoRetornoInternet()));
		
		request.setCindcdFantsRepas(entradaDTO.getCindcdFantsRepas());

		response = getFactoryAdapter().getAlterarConfiguracaoTipoServModContratoPDCAdapter().invokeProcess(request);

		saidaDTO.setCodMensagem(response.getCodMensagem());
		saidaDTO.setMensagem(response.getMensagem());

		return saidaDTO;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.service.business.mantercontrato.
	 *      IManterContratoService#consultarManutencaoParticipante(br.com.bradesco.web.pgit.service.business
	 *      .mantercontrato.bean.ConsultarManParticipanteEntradaDTO)
	 */
	public List<ConsultarManParticipanteSaidaDTO> consultarManutencaoParticipante(
			ConsultarManParticipanteEntradaDTO entrada) {
		ConsultarManutencaoParticipanteRequest request = new ConsultarManutencaoParticipanteRequest();

		request.setCdControleCpfCnpj(entrada.getCdControleCpfCnpj());
		request.setCdCpfCnpj(entrada.getCdCorpoCpfCnpj());
		request.setCdDigitoCpfCnpj(entrada.getCdDigitoCpfCnpj());
		request.setCdPessoaJuridicaNegocio(entrada.getCdPessoaJuridica());
		request.setCdTipoContratoNegocio(entrada.getCdTipoContratoNegocio());
		request.setDtFim(entrada.getDtFim());
		request.setDtinicio(entrada.getDtInicio());
		request.setNrSeqContratoNegocio(entrada.getNrSeqContratoNegocio());
		request.setNrOcorrencias(30);

		ConsultarManutencaoParticipanteResponse response = getFactoryAdapter()
				.getConsultarManutencaoParticipantePDCAdapter().invokeProcess(
						request);

		List<ConsultarManParticipanteSaidaDTO> listaSaida = new ArrayList<ConsultarManParticipanteSaidaDTO>();
		SimpleDateFormat formato1 = new SimpleDateFormat("yyyy-MM-dd-HH.mm.ss");
		SimpleDateFormat formato2 = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		ConsultarManParticipanteSaidaDTO saida;

		for (int i = 0; i < response.getOcorrenciasCount(); i++) {
			saida = new ConsultarManParticipanteSaidaDTO();
			saida.setCdCpfCnpj(response.getOcorrencias(i).getCdCpfCnpj());
			saida.setCdUsuarioManutencao(response.getOcorrencias(i)
					.getCdusuarioManutencao());
			saida.setCodMensagem(response.getCodMensagem());
			saida.setDtManutencao(response.getOcorrencias(i).getDtManutencao());
			saida.setHrInclusaoRegistro(response.getOcorrencias(i)
					.getHrInclusaoRegistro());
			saida.setHrManutencao(response.getOcorrencias(i).getHrManutencao());
			saida.setMensagem(response.getMensagem());
			saida.setNomeParticipante(response.getOcorrencias(i)
					.getNomeParticipante());
			saida.setCdPessoa(response.getOcorrencias(i).getCdPessoa());
			saida.setCdTipoParticipacao(response.getOcorrencias(i)
					.getCdTipoParticipacao());
			saida.setCdIndicadorTipoManutencao(response.getOcorrencias(i)
					.getCdIndicadorTipoManutencao());
			listaSaida.add(saida);

			if (response.getOcorrencias(i).getHrInclusaoRegistro() != null
					&& response.getOcorrencias(i).getHrInclusaoRegistro()
							.length() >= 19) {
				String stringData = response.getOcorrencias(i)
						.getHrInclusaoRegistro().substring(0, 19);

				try {
					saida.setHrInclusaoRegistroFormatada(formato2
							.format(formato1.parse(stringData)));
				} catch (ParseException e) {
					saida.setHrInclusaoRegistroFormatada("");
				}
			}
		}
		return listaSaida;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.service.business.mantercontrato.
	 *      IManterContratoService#listarContasRelacionadasHist(br.com.bradesco.web.pgit.service.business.
	 *      mantercontrato.bean.ListarContasRelacionadasHistEntradaDTO)
	 */
	public List<ListarContasRelacionadasHistSaidaDTO> listarContasRelacionadasHist(
			ListarContasRelacionadasHistEntradaDTO entradaDTO) {

		ListarContasRelacionadasHistRequest request = new ListarContasRelacionadasHistRequest();
		request
				.setNumeroOcorrencias(IManterContratoServiceConstants.OCORRENCIAS_LISTAR_HISTORICO_CONTAS);
		request.setCdPessoaJuridicaNegocio(verificaLongNulo(entradaDTO
				.getCdPessoaJuridicaNegocio()));
		request.setCdTipoContratoNegocio(verificaIntegerNulo(entradaDTO
				.getCdTipoContratoNegocio()));
		request.setNrSequenciaContratoNegocio(verificaLongNulo(entradaDTO
				.getNrSequenciaContratoNegocio()));
		request.setCdPessoaJuridicaVinculo(verificaLongNulo(entradaDTO
				.getCdPessoaJuridicaVinculo()));
		request.setCdTipoContratoVinculo(verificaIntegerNulo(entradaDTO
				.getCdTipoContratoVinculo()));
		request.setNrSequenciaContratoVinculo(verificaLongNulo(entradaDTO
				.getNrSequenciaContratoVinculo()));
		request.setCdTipoVinculoContrato(verificaIntegerNulo(entradaDTO
				.getCdTipoVinculoContrato()));
		request.setCdTipoRelacionamentoConta(verificaIntegerNulo(entradaDTO
				.getCdTipoRelacionamentoConta()));
		request.setCdBancoRelacionamento(verificaIntegerNulo(entradaDTO
				.getCdBancoRelacionamento()));
		request.setCdAgenciaRelacionamento(verificaIntegerNulo(entradaDTO
				.getCdAgenciaRelacionamento()));
		request.setCdContaRelacionamento(verificaLongNulo(entradaDTO
				.getCdContaRelacionamento()));
		request.setCdDigitoContaRelacionamento(verificaStringNula(entradaDTO
				.getCdDigitoContaRelacionamento()));
		request.setCdCorpoCpfCnpjParticipante(verificaLongNulo(entradaDTO
				.getCdCorpoCpfCnpjParticipante()));
		request.setCdFilialCpfCnpjParticipante(verificaIntegerNulo(entradaDTO
				.getCdFilialCpfCnpjParticipante()));
		request.setCdDigitoCpfCnpjParticipante(verificaIntegerNulo(entradaDTO
				.getCdDigitoCpfCnpjParticipante()));
		request.setDtInicioManutencao(verificaIntegerNulo(entradaDTO
				.getDtInicioManutencao()));
		request.setDtFimManutencao(verificaIntegerNulo(entradaDTO
				.getDtFimManutencao()));

		ListarContasRelacionadasHistResponse response = getFactoryAdapter()
				.getListarContasRelacionadasHistPDCAdapter().invokeProcess(
						request);

		List<ListarContasRelacionadasHistSaidaDTO> listaSaida = new ArrayList<ListarContasRelacionadasHistSaidaDTO>();
		for (int i = 0; i < response.getOcorrenciasCount(); i++) {
			ListarContasRelacionadasHistSaidaDTO saida = new ListarContasRelacionadasHistSaidaDTO();
			saida.setCodMensagem(response.getCodMensagem());
			saida.setMensagem(response.getMensagem());
			saida.setCdCorpoCpfCnpjParticipante(response.getOcorrencias(i)
					.getCdCorpoCpfCnpjParticipante());
			saida.setCdFilialCpfCnpjParticipante(response.getOcorrencias(i)
					.getCdFilialCpfCnpjParticipante());
			saida.setCdDigitoCpfCnpjParticipante(response.getOcorrencias(i)
					.getCdDigitoCpfCnpjParticipante());
			saida.setCdBancoRelacionamento(response.getOcorrencias(i)
					.getCdBancoRelacionamento());
			saida.setDsBancoRelacionamento(response.getOcorrencias(i)
					.getDsBancoRelacionamento());
			saida.setCdAgenciaRelacionamento(response.getOcorrencias(i)
					.getCdAgenciaRelacionamento());
			saida.setDsAgenciaRelacionamento(response.getOcorrencias(i)
					.getDsAgenciaRelacionamento());
			saida.setCdContaRelacionamento(response.getOcorrencias(i)
					.getCdContaRelacionamento());
			saida.setDsTipoContaRelacionamentoConta(response.getOcorrencias(i)
					.getDsTipoRelacionamento());
			saida.setCdDigitoContaRelacionamento(response.getOcorrencias(i)
					.getCdDigitoContaRelacionamento());
			saida.setCdTipoConta(response.getOcorrencias(i).getCdTipoConta());
			saida.setDsTipoConta(response.getOcorrencias(i).getDsTipoConta());
			saida.setHrInclusaoManutencao(response.getOcorrencias(i)
					.getHrInclusaoManutencao());
			saida.setCdUsuarioManutencao(response.getOcorrencias(i)
					.getCdUsuarioManutencao());
			saida.setDtManutencao(response.getOcorrencias(i).getDtManutencao());
			saida.setDsTipoManutencao(response.getOcorrencias(i)
					.getDsTipoManutencao());
			saida.setHrManutencao(response.getOcorrencias(i).getHrManutencao());
			saida.setNmParticipante(response.getOcorrencias(i)
					.getNmParticipante());
			saida.setCdTipoRelacionamento(response.getOcorrencias(i)
					.getCdTipoRelacionamento());

			saida
					.setCpfCnpjParticipanteFormatado(formatCpfCnpjCompleto(
							response.getOcorrencias(i)
									.getCdCorpoCpfCnpjParticipante(), response
									.getOcorrencias(i)
									.getCdFilialCpfCnpjParticipante(), response
									.getOcorrencias(i)
									.getCdDigitoCpfCnpjParticipante()));
			saida.setBancoFormatado(PgitUtil.formatBanco(response
					.getOcorrencias(i).getCdBancoRelacionamento(), response
					.getOcorrencias(i).getDsBancoRelacionamento(), false));
			saida.setAgenciaFormatada(PgitUtil.formatAgencia(response
					.getOcorrencias(i).getCdAgenciaRelacionamento(), response
					.getOcorrencias(i).getDsAgenciaRelacionamento(), false));
			saida
					.setContaFormatada(PgitUtil.formatConta(response
							.getOcorrencias(i).getCdContaRelacionamento(),
							response.getOcorrencias(i)
									.getCdDigitoContaRelacionamento(), false));
			saida.setDataHoraManutencaoFormatada(PgitUtil.concatenarCampos(
					response.getOcorrencias(i).getDtManutencao(), response
							.getOcorrencias(i).getHrManutencao()));
			saida.setcPssoa(response.getOcorrencias(i).getCPssoa());
			listaSaida.add(saida);
		}
		return listaSaida;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.service.business.mantercontrato.
	 *      IManterContratoService#listarConManLayout(br.com.bradesco.web.pgit.service.business.
	 *      mantercontrato.bean.ListarConManLayoutEntradaDTO)
	 */
	public List<ListarConManLayoutSaidaDTO> listarConManLayout(
			ListarConManLayoutEntradaDTO entrada) {
		ListarConManLayoutRequest request = new ListarConManLayoutRequest();

		request
				.setCdPessoaJuridicaNegocio(entrada
						.getCdPessoaJuridicaNegocio());
		request.setCdTipoContratoNegocio(entrada.getCdTipoContratoNegocio());
		request.setCdTipoLayoutArquivo(entrada.getCdTipoLayoutArquivo());
		request.setDtFim(entrada.getDtFim());
		request.setDtInicio(entrada.getDtInicio());
		request.setNrSequenciaContratoNegocio(entrada
				.getNrSequenciaContratoNegocio());

		request.setNrOcorrencias(30);

		ListarConManLayoutResponse response = getFactoryAdapter()
				.getListarConManLayoutPDCAdapter().invokeProcess(request);

		List<ListarConManLayoutSaidaDTO> listaSaida = new ArrayList<ListarConManLayoutSaidaDTO>();
		ListarConManLayoutSaidaDTO saida;

		for (int i = 0; i < response.getOcorrenciasCount(); i++) {
			saida = new ListarConManLayoutSaidaDTO();
			saida.setCdTipoLayout(response.getOcorrencias(i).getCdTipoLayout());
			saida.setCdUsuario(response.getOcorrencias(i).getCdUsuario());
			saida.setCodMensagem(response.getCodMensagem());
			saida.setDsLayout(response.getOcorrencias(i).getDsLayout());
			saida.setDtManutencao(response.getOcorrencias(i).getDtManutencao());
			saida.setHrInclusaoRegistro(response.getOcorrencias(i)
					.getHrInclusaoRegistro());
			saida.setHrManutencao(response.getOcorrencias(i).getHrManutencao());
			saida.setMensagem(response.getMensagem());
			saida.setDtHoraInclusaoDesc(formatarDataTrilha(response
					.getOcorrencias(i).getHrInclusaoRegistro()));
			saida.setDtHoraManutencaoDesc(formatarData(response.getOcorrencias(
					i).getDtManutencao())
					+ " " + response.getOcorrencias(i).getHrManutencao());
			saida.setDsTipoManutencao(response.getOcorrencias(i)
					.getDsTipoManutencao());
			listaSaida.add(saida);
		}

		return listaSaida;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.service.business.mantercontrato.
	 *      IManterContratoService#consultarConManLayout(br.com.bradesco.web.pgit.service.business.
	 *      mantercontrato.bean.ConsultarConManLayoutEntradaDTO)
	 */
	public ConsultarConManLayoutSaidaDTO consultarConManLayout(
			ConsultarConManLayoutEntradaDTO entrada) {
		ConsultarConManLayoutRequest request = new ConsultarConManLayoutRequest();
		request.setCdPessoaJuridicaContrato(verificaLongNulo(entrada
				.getCdPessoaJuridicaContrato()));
		request.setCdTipoContratoNegocio(verificaIntegerNulo(entrada
				.getCdTipoContratoNegocio()));
		request.setCdTipoLayoutArquivo(verificaIntegerNulo(entrada
				.getCdTipoLayoutArquivo()));
		request.setHrInclusaoRegistro(verificaStringNula(entrada
				.getHrInclusaoRegistro()));
		request.setNrSequenciaContratoNegocio(verificaLongNulo(entrada
				.getNrSequenciaContratoNegocio()));

		ConsultarConManLayoutResponse response = getFactoryAdapter()
				.getConsultarConManLayoutPDCAdapter().invokeProcess(request);

		ConsultarConManLayoutSaidaDTO saida = new ConsultarConManLayoutSaidaDTO();
		saida.setCodMensagem(response.getCodMensagem());
		saida.setMensagem(response.getMensagem());
		saida.setCdTipoLayoutArquivo(response.getCdTipoLayoutArquivo());
		saida.setDsTipoLayoutArquivo(response.getDsTipoLayoutArquivo());
		saida.setCdSituacaoLayoutContrato(response
				.getCdSituacaoLayoutContrato());
		saida.setDsSituacaoLayoutContrato(response
				.getDsSituacaoLayoutContrato());
		saida.setCdResponsavelCustoEmpresa(response
				.getCdResponsavelCustoEmpresa());
		saida.setDsResponsavelCustoEmpresa(response
				.getDsResponsavelCustoEmpresa());
		saida.setPercentualCustoOrganizacaoTransmissao(response
				.getPercentualCustoOrganizacaoTransmissao());
		saida.setCdUsuarioInclusao(response.getCdUsuarioInclusao());
		saida.setCdUsuarioInclusaoExter(response.getCdUsuarioInclusaoExter());
		saida.setHrInclusaoRegistro(FormatarData.formatarDataTrilha(response
				.getHrInclusaoRegistro()));
		saida.setCdOperacaoCanalInclusao(response.getCdOperacaoCanalInclusao());
		saida.setCdTipoCanalInclusao(response.getCdTipoCanalInclusao());
		saida.setDsCanalInclusao(response.getDsCanalInclusao());
		saida.setCdUsuarioManutencao(response.getCdUsuarioManutencao());
		saida.setCdUsuarioManutencaoExter(response
				.getCdUsuarioManutencaoExter());
		saida.setHrManutencaoRegistro(FormatarData.formatarDataTrilha(response
				.getHrManutencaoRegistro()));
		saida.setCdOperacaoCanalManutencao(response
				.getCdOperacaoCanalManutencao());
		saida.setCdTipoCanalManutencao(response.getCdTipoCanalManutencao());
		saida.setDsCanalManutencao(response.getDsCanalManutencao());
		saida.setCdMsgLinExtrt(response.getCdMsgLinExtrt());
		saida.setCdTipoControlePagamento(response.getCdTipoControlePagamento());
		saida.setDsTipoControlePagamento(response.getDsTipoControlePagamento());

		return saida;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.service.business.mantercontrato.
	 *      IManterContratoService#listarConManServico(br.com.bradesco.web.pgit.service.
	 *      business.mantercontrato.bean.ListarConManServicoEntradaDTO)
	 */
	public List<ListarConManServicoSaidaDTO> listarConManServico(
			ListarConManServicoEntradaDTO entrada) {
		ListarConManServicoRequest request = new ListarConManServicoRequest();
		request
				.setCdPessoaJuridicaNegocio(entrada
						.getCdPessoaJuridicaNegocio());
		request.setCdProdutoOperacaoRelacionado(entrada
				.getCdProdutoOperacaoRelacionado());
		request.setCdProdutoServicoOperacao(entrada
				.getCdProdutoServicoOperacao());
		request.setCdTipoContratoNegocio(entrada.getCdTipoContratoNegocio());
		request.setCdTipoServico(entrada.getCdTipoServico());
		request.setDtFim(entrada.getDtFim());
		request.setDtInicio(entrada.getDtInicio());
		request.setNrSequenciaContratoNegocio(entrada
				.getNrSequenciaContratoNegocio());
		request.setNrOcorrencias(30);

		ListarConManServicoResponse response = getFactoryAdapter()
				.getListarConManServicoPDCAdapter().invokeProcess(request);

		List<ListarConManServicoSaidaDTO> listaSaida = new ArrayList<ListarConManServicoSaidaDTO>();
		ListarConManServicoSaidaDTO saida;
		for (int i = 0; i < response.getOcorrenciasCount(); i++) {
			saida = new ListarConManServicoSaidaDTO();
			saida.setCodMensagem(response.getCodMensagem());
			saida.setMensagem(response.getMensagem());
			saida.setHrInclusaoRegistroHistorico(response.getOcorrencias(i)
					.getHrInclusaoRegistroHistorico());
			saida.setCdProduto(response.getOcorrencias(i).getCdProduto());
			saida.setDsProduto(response.getOcorrencias(i).getDsModalidade());
			saida.setDtManutencao(response.getOcorrencias(i).getDtManutencao());
			saida.setHrManutencao(response.getOcorrencias(i).getHrManutencao());
			saida.setCdUsuario(response.getOcorrencias(i).getCdUsuario());
			saida.setCdModalidade(response.getOcorrencias(i).getCdModalidade());
			saida.setDsModalidade(response.getOcorrencias(i).getDsModalidade());
			saida.setDsTipoManutencao(response.getOcorrencias(i)
					.getDsTipoManutencao());

			SimpleDateFormat formato = new SimpleDateFormat("dd.MM.yyyy");
			SimpleDateFormat formato2 = new SimpleDateFormat("dd/MM/yyyy");
			try {
				saida.setDtManutencaoFormatada(formato2.format(formato
						.parse(response.getOcorrencias(i).getDtManutencao())));
			} catch (ParseException e) {
				saida.setDtManutencaoFormatada("");
			}
			saida.setDataHoraManutencao(saida.getDtManutencaoFormatada() + " "
					+ response.getOcorrencias(i).getHrManutencao());
			saida.setCdParametroTela(response.getOcorrencias(i)
					.getCdTipoServico());
			listaSaida.add(saida);
		}

		return listaSaida;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.service.business.mantercontrato.
	 *      IManterContratoService#consultarConManServico(br.com.bradesco.web.pgit.service.business
	 *      .mantercontrato.bean.ConsultarConManServicosEntradaDTO)
	 */
	
	//forest
	public ConsultarConManServicosSaidaDTO consultarConManServico(ConsultarConManServicosEntradaDTO entrada) {

		ConsultarConManServicosRequest request = new ConsultarConManServicosRequest();
		request.setCdPessoaJuridicaContrato(entrada
				.getCdPessoaJuridicaContrato());
		request.setCdProdutoOperacaoRelacionado(entrada
				.getCdProdutoOperacaoRelacionado());
		request.setCdProdutoServicoOperacao(entrada
				.getCdProdutoServicoOperacao());
		request.setCdTipoContratoNegocio(entrada.getCdTipoContratoNegocio());
		request.setHrInclusaoRegistroHistorico(entrada
				.getHrInclusaoRegistroHistorico());
		request.setNrSequenciaContratoNegocio(entrada
				.getNrSequenciaContratoNegocio());
		request.setCdParametro(entrada.getCdParamentro());

		ConsultarConManServicosResponse response = getFactoryAdapter().getConsultarConManServicosPDCAdapter().invokeProcess(request);

		ConsultarConManServicosSaidaDTO saida = new ConsultarConManServicosSaidaDTO();
		saida.setCodMensagem(response.getCodMensagem());
		saida.setMensagem(response.getMensagem());
		saida.setQtDiaUtilPgto(response.getQtDiaUtilPgto());
		
		/**************************************** TIPO DE SERVI�O *******************************************/

		// Pagamento Fornecedores
		if (entrada.getCdParametroTela() == 1) {
			saida.setCdFormaEnvioPagamento(response.getCdFormaEnvioPagamento());
			saida.setDsFormaEnvioPagamento(response.getDsFormaEnvioPagamento());
			saida.setCdFormaAutorizacaoPagamento(response
					.getCdFormaAutorizacaoPagamento());
			saida.setDsFormaAutorizacaoPagamento(response
					.getDsFormaAutorizacaoPagamento());
			saida.setCdIndicadorAutorizacaoCliente(response
					.getCdIndicadorAutorizacaoCliente());
			saida.setDsIndicadorAutorizacaoCliente(response
					.getDsIndicadorAutorizacaoCliente());
			saida.setCdTipoDataFloating(response.getCdTipoDataFloating());
			saida.setDsTipoDataFloating(response.getDsTipoDataFloating());
			saida.setCdIndicadorListaDebito(response
					.getCdIndicadorListaDebito());
			saida.setCdIndicadorAutorizacaoComplemento(response
					.getCdIndicadorAutorizacaoComplemento());
			saida.setCdTipoFormacaoLista(response.getCdTipoFormacaoLista());
			saida.setDsTipoFormacaoLista(response.getDsTipoFormacaoLista());
			saida.setCdTipoConsistenciaLista(response
					.getCdTipoConsistenciaLista());
			saida.setDsTipoConsistenciaLista(response
					.getDsTipoConsistenciaLista());
			saida.setCdIndicadorRetornoInternet(response
					.getCdIndicadorRetornoInternet());
			saida.setCdTipoConsultaComprovante(response
					.getCdTipoConsultaComprovante());
			saida.setDsTipoConsolidacaoComprovante(response
					.getDsTipoConsolidacaoComprovante());
			saida.setCdIndicadorCadastroProcurador(response
					.getCdIndicadorCadastroProcurador());
			saida.setDsCctciaInscricaoFavorecido(response
					.getDsCctciaInscricaoFavorecido());
			
			saida.setCdIdentificadorTipoRetornoInternet(response.getCdIdentificadorTipoRetornoInternet());
			saida.setDsCodIdentificadorTipoRetornoInternet(response.getDsCodIdentificadortipoRetornoInternet());

		}

		// Pagamento de Sal�rios
		if (entrada.getCdParametroTela() == 2) {
			saida.setCdIndicadorCartaoSalario(response
					.getCdIndicadorCartaoSalario());
			saida.setDsIndicadorCartaoSalario(response
					.getDsIndicadorCartaoSalario());
			saida.setCdTipoCartaoSalario(response.getCdTipoCartaoSalario());
			saida.setDsTipoCartaoSalario(response.getDsTipoCartaoSalario());
			saida.setQuantidadeSolicitacaoCartao(response
					.getQuantidadeSolicitacaoCartao());
			saida.setDtEnquaContaSalario(response.getDtEnquaContaSalario());

			saida.setCdFormaEnvioPagamento(response.getCdFormaEnvioPagamento());
			saida.setDsFormaEnvioPagamento(response.getDsFormaEnvioPagamento());
			saida.setCdFormaAutorizacaoPagamento(response
					.getCdFormaAutorizacaoPagamento());
			saida.setDsFormaAutorizacaoPagamento(response
					.getDsFormaAutorizacaoPagamento());
			saida.setCdIndicadorAutorizacaoCliente(response
					.getCdIndicadorAutorizacaoCliente());
			saida.setCdIndicadorAutorizacaoComplemento(response
					.getCdIndicadorAutorizacaoComplemento());
			saida.setCdTipoDataFloating(response.getCdTipoDataFloating());
			saida.setDsTipoDataFloating(response.getDsTipoDataFloating());
			saida.setCdIndicadorListaDebito(response
					.getCdIndicadorListaDebito());
			saida.setCdTipoFormacaoLista(response.getCdTipoFormacaoLista());
			saida.setDsTipoFormacaoLista(response.getDsTipoFormacaoLista());
			saida.setCdTipoConsistenciaLista(response
					.getCdTipoConsistenciaLista());
			saida.setDsTipoConsistenciaLista(response
					.getDsTipoConsistenciaLista());
			saida.setCdIndicadorRetornoInternet(response
					.getCdIndicadorRetornoInternet());
			saida.setQuantidadeSolicitacaoCartao(response
					.getQuantidadeSolicitacaoCartao());
			saida.setCdTipoConsultaComprovante(response
					.getCdTipoConsultaComprovante());
			saida.setDsTipoConsolidacaoComprovante(response
					.getDsTipoConsolidacaoComprovante());
			saida.setCdIndicadorBancoPostal(response
					.getCdIndicadorBancoPostal());
			saida.setCdIndicadorCadastroProcurador(response
					.getCdIndicadorCadastroProcurador());
			saida.setDsCctciaInscricaoFavorecido(response
			.getDsCctciaInscricaoFavorecido());
			saida.setCdIdentificadorTipoRetornoInternet(response.getCdIdentificadorTipoRetornoInternet());
			saida.setDsCodIdentificadorTipoRetornoInternet(response.getDsCodIdentificadortipoRetornoInternet());
		}

		// Pagamentos de Tributos e Contas de Consumo
		if (entrada.getCdParametroTela() == 3) {
			saida.setCdIndicadorUtilizaMora(response.getCdIndicadorUtilizaMora());
			saida.setDsIndicadorUtilizaMora(response.getDsIndicadorUtilizaMora());
			saida.setCdConsultaDebitoVeiculo(response
					.getCdConsultaDebitoVeiculo());
			saida.setDsConsultaDebitoVeiculo(response
					.getDsConsultaDebitoVeiculo());
			saida.setCdPeriodicidadeConsultaVeiculo(response
					.getCdPeriodicidadeConsultaVeiculo());
			saida.setDsPeriodicidadeConsultaVeiculo(response
					.getDsPeriodicidadeConsultaVeiculo());
			saida.setCdAgendamentoDebitoVeiculo(response
					.getCdAgendamentoDebitoVeiculo());
			saida.setDsAgendamentoDebitoVeiculo(response
					.getDsAgendamentoDebitoVeiculo());
			saida.setCdPeriodicidadeCobrancaTarifa(response
					.getCdPeriodicidadeCobrancaTarifa());
			saida.setDsPeriodicidadeCobrancaTarifa(response
					.getDsPeriodicidadeCobrancaTarifa());
			saida.setNrFechamentoApuracaoTarifa(response
					.getNrFechamentoApuracaoTarifa());
			saida.setQuantidadeDiaCobrancaTarifa(response
					.getQuantidadeDiaCobrancaTarifa());
			saida.setCdTipoReajusteTarifa(response.getCdTipoReajusteTarifa());
			saida.setDsTipoReajusteTarifa(response.getDsTipoReajusteTarifa());
			saida.setQuantidadeMesReajusteTarifa(response
					.getQuantidadeMesReajusteTarifa());
			saida.setCdIndicadorEconomicoReajuste(response
					.getCdIndicadorEconomicoReajuste());
			saida.setCdFormaEnvioPagamento(response.getCdFormaEnvioPagamento());
			saida.setDsFormaEnvioPagamento(response.getDsFormaEnvioPagamento());
			saida.setCdFormaAutorizacaoPagamento(response
					.getCdFormaAutorizacaoPagamento());
			saida.setDsFormaAutorizacaoPagamento(response
					.getDsFormaAutorizacaoPagamento());
			saida.setCdIndicadorRetornoInternet(response
					.getCdIndicadorRetornoInternet());
			saida.setCdTipoDataFloating(response.getCdTipoDataFloating());
			saida.setDsTipoDataFloating(response.getDsTipoDataFloating());
			saida.setCdIndicadorListaDebito(response
					.getCdIndicadorListaDebito());
			saida.setCdTipoFormacaoLista(response.getCdTipoFormacaoLista());
			saida.setDsTipoFormacaoLista(response.getDsTipoFormacaoLista());
			saida.setCdTipoConsistenciaLista(response
					.getCdTipoConsistenciaLista());
			saida.setDsTipoConsistenciaLista(response
					.getDsTipoConsistenciaLista());
			saida.setCdTipoConsultaComprovante(response
					.getCdTipoConsultaComprovante());
			saida.setDsTipoConsolidacaoComprovante(response
					.getDsTipoConsolidacaoComprovante());
			saida.setCdIndicadorCadastroProcurador(response
					.getCdIndicadorCadastroProcurador());
			saida.setDsCctciaInscricaoFavorecido(response
					.getDsCctciaInscricaoFavorecido());
			saida.setCdIdentificadorTipoRetornoInternet(response.getCdIdentificadorTipoRetornoInternet());
			saida.setDsCodIdentificadorTipoRetornoInternet(response.getDsCodIdentificadortipoRetornoInternet());
		}

		// Pagamento de Benef�cios
		if (entrada.getCdParametroTela() == 4) {
			saida.setCdDisponibilizacaoContaCredito(response
					.getCdDisponibilizacaoContaCredito());
			saida.setDsDisponibilizacaoContaCredito(response
					.getDsDisponibilizacaoContaCredito());
			saida.setCdTipoIdentificacaoBeneficio(response
					.getCdTipoIdentificacaoBeneficio());
			saida.setDsTipoIdentificacaoBeneficio(response
					.getDsTipoIdentificacaoBeneficio());
			saida.setCdIndicadorCadastroorganizacao(response
					.getCdIndicadorCadastroorganizacao());
			saida.setDsIndicadorCadastroorganizacao(response
					.getDsIndicadorCadastroorganizacao());
			saida.setCdIndicadorCadastroProcurador(response
					.getCdIndicadorCadastroProcurador());
			saida.setDsIndicadorCadastroorganizacao(response
					.getDsIndicadorCadastroorganizacao());
			saida.setCdPeriodicidadeManutencaoProcd(response
					.getCdPeriodicidadeManutencaoProcd());
			saida.setDsPeriodicidadeManutencaoProcd(response
					.getDsPeriodicidadeManutencaoProcd());
			saida.setCdFormaManutencao(response.getCdFormaManutencao());
			saida.setDsFormaManutencao(response.getDsFormaManutencao());
			saida.setCdindicadorExpiraCredito(response
					.getCdindicadorExpiraCredito());
			saida.setDsindicadorExpiraCredito(response
					.getDsindicadorExpiraCredito());
			saida.setCdFormaExpiracaoCredito(response
					.getCdFormaExpiracaoCredito());
			saida.setDsFormaExpiracaoCredito(response
					.getDsFormaExpiracaoCredito());
			saida.setQuantidadeDiaExpiracao(response
					.getQuantidadeDiaExpiracao());
			saida.setCdCreditoNaoUtilizado(response.getCdCreditoNaoUtilizado());
			saida.setDsCreditoNaoUtilizado(response.getDsCreditoNaoUtilizado());
			saida.setCdMomentoCreditoEfetivacao(response
					.getCdMomentoCreditoEfetivacao());
			saida.setDsMomentoCreditoEfetivacao(response
					.getDsMomentoCreditoEfetivacao());

			saida.setCdFormaEnvioPagamento(response.getCdFormaEnvioPagamento());
			saida.setDsFormaEnvioPagamento(response.getDsFormaEnvioPagamento());
			saida.setCdFormaAutorizacaoPagamento(response
					.getCdFormaAutorizacaoPagamento());
			saida.setDsFormaAutorizacaoPagamento(response
					.getDsFormaAutorizacaoPagamento());
			saida.setCdIndicadorAutorizacaoCliente(response
					.getCdIndicadorAutorizacaoCliente());
			saida.setCdIndicadorAutorizacaoComplemento(response
					.getCdIndicadorAutorizacaoComplemento());
			saida.setCdTipoDataFloating(response.getCdTipoDataFloating());
			saida.setDsTipoDataFloating(response.getDsTipoDataFloating());
			saida.setCdIndicadorListaDebito(response
					.getCdIndicadorListaDebito());
			saida.setCdTipoFormacaoLista(response.getCdTipoFormacaoLista());
			saida.setDsTipoFormacaoLista(response.getDsTipoFormacaoLista());
			saida.setCdTipoConsistenciaLista(response
					.getCdTipoConsistenciaLista());
			saida.setDsTipoConsistenciaLista(response
					.getDsTipoConsistenciaLista());
			saida.setCdIndicadorRetornoInternet(response
					.getCdIndicadorRetornoInternet());
			saida.setCdTipoConsultaComprovante(response
					.getCdTipoConsultaComprovante());
			saida.setDsTipoConsolidacaoComprovante(response
					.getDsTipoConsolidacaoComprovante());
			saida.setCdTipoIdentificacaoBeneficio(response
					.getCdTipoIdentificacaoBeneficio());
			saida.setDsTipoIdentificacaoBeneficio(response
					.getDsTipoIdentificacaoBeneficio());
			saida.setCdIndicadorCadastroProcurador(response
					.getCdIndicadorCadastroProcurador());
			saida.setQuantidadeDiaExpiracao(response
					.getQuantidadeDiaExpiracao());
			saida.setCdMomentoCreditoEfetivacao(response
					.getCdMomentoCreditoEfetivacao());
			saida.setDsMomentoCreditoEfetivacao(response
					.getDsMomentoCreditoEfetivacao());
			saida.setCdCreditoNaoUtilizado(response.getCdCreditoNaoUtilizado());
			saida.setDsCreditoNaoUtilizado(response.getDsCreditoNaoUtilizado());
			saida.setDsCctciaInscricaoFavorecido(response
					.getDsCctciaInscricaoFavorecido());
		}

		// Cadastro de Favorecido
		if (entrada.getCdParametroTela() == 5) {
			saida.setCdFormaManutencao(response.getCdFormaManutencao());
			saida.setDsFormaManutencao(response.getDsFormaManutencao());
			saida.setQuantidadeDiaInatividadeFavorecido(response
					.getQuantidadeDiaInatividadeFavorecido());
			saida.setCdCctciaInscricaoFavorecido(response
					.getCdCctciaInscricaoFavorecido());
			saida.setDsCctciaInscricaoFavorecido(response
					.getDsCctciaInscricaoFavorecido());
			saida.setCdIndicadorRetornoInternet(response
					.getCdIndicadorRetornoInternet());
		}

		// Aviso de Movimenta��o de D�bito
		// Aviso de Movimenta��o de Cr�dito
		// Aviso de Movimenta��o ao Pagador
		// Aviso de Movimenta��o ao Favorecido
		// Comprovante Pagamento ao Pagador
		// Comprovante Pagamento ao Favorecido
		if (entrada.getCdParametroTela() >= 6
				&& entrada.getCdParametroTela() <= 11) {

			if (entrada.getCdParametroTela() == 10
					|| entrada.getCdParametroTela() == 11) {
				saida.setCdPeriodicidadeAviso(response
						.getCdPeriodicidadeComprovante());
				saida.setDsPeriodicidadeAviso(response
						.getDsPeriodicidadeComprovante());
				saida.setCdDestinoAviso(response.getCdDestinoComprovante());
				saida.setDsDestinoAviso(response.getDsDestinoComprovante());
				saida.setCdEnvelopeAberto(response.getCdEnvelopeAberto());
				saida.setCdAreaReservada(response.getCdAreaReservada());
				saida.setCdAgrupamentoAviso(response
						.getCdAgrupamentoComprovante());
				saida.setDsAgrupamentoAviso(response
						.getDsAgrupamentoComprovante());
				saida.setQuantidadeViaAviso(response
						.getQuantidadeViaComprovante());

				saida.setCdConsultaEndereco(response.getCdConsultaEndereco());
				saida.setDsConsultaEndereco(response.getDsConsultaEndereco());
				saida.setCdEnvelopeAberto(response.getCdEnvelopeAberto());
				saida.setDsAreaReservada2(response.getDsAreaReservada2());
				saida.setCdIndicadorRetornoInternet(response
						.getCdIndicadorRetornoInternet());
				saida.setDsCctciaInscricaoFavorecido(response
						.getDsCctciaInscricaoFavorecido());
			} else {
				saida.setCdPeriodicidadeAviso(response
						.getCdPeriodicidadeAviso());
				saida.setDsPeriodicidadeAviso(response
						.getDsPeriodicidadeAviso());
				saida.setCdDestinoAviso(response.getCdDestinoAviso());
				saida.setDsDestinoAviso(response.getDsDestinoAviso());
				saida.setCdEnvelopeAberto(response.getCdEnvelopeAberto());
				saida.setCdAreaReservada(response.getCdAreaReservada());
				saida.setCdAgrupamentoAviso(response.getCdAgrupamentoAviso());
				saida.setDsAgrupamentoAviso(response.getDsAgrupamentoAviso());
				saida.setQuantidadeViaAviso(response.getQuantidadeViaAviso());

				saida.setCdConsultaEndereco(response.getCdConsultaEndereco());
				saida.setDsConsultaEndereco(response.getDsConsultaEndereco());
				saida.setCdEnvelopeAberto(response.getCdEnvelopeAberto());
				saida.setDsAreaReservada2(response.getDsAreaReservada2());
				saida.setCdIndicadorRetornoInternet(response
						.getCdIndicadorRetornoInternet());
				saida.setDsCctciaInscricaoFavorecido(response
						.getDsCctciaInscricaoFavorecido());
			}

		}

		// Emiss�o Comprovantes Diversos
		// Emiss�o Comprovante Salarial
		if (entrada.getCdParametroTela() == 12
				|| entrada.getCdParametroTela() == 13) {
			saida.setCdRejeicaoLote(response.getCdRejeicaoLote());
			saida.setCdDisponibilizacaoDiversoCriterio(response
					.getCdDisponibilizacaoDiversoCriterio());
			saida.setDsDisponibilizacaoDiversoCriterio(response
					.getDsDisponibilizacaoDiversoCriterio());
			saida.setCdMidiaDisponivel(response.getCdMidiaDisponivel());
			saida.setCdDisponibilizacaoDiversoNao(response
					.getCdDisponibilizacaoDiversoNao());
			saida.setDsDisponibilizacaoDiversoNao(response
					.getDsDisponibilizacaoDiversoNao());
			saida.setCdDestinoComprovante(response.getCdDestinoComprovante());
			saida.setCdFrasePrecadastrada(response.getCdFrasePrecadastrada());
			saida.setCdCobrancaTarifa(response.getCdCobrancaTarifa());
			saida.setQuantidadeMesComprovante(response
					.getQuantidadeMesComprovante());
			saida.setQuantidadeLimiteLinha(response.getQuantidadeLimiteLinha());
			saida.setQuantidadeViaComprovante(response
					.getQuantidadeViaComprovante());
			saida.setQuantidadeViaCombranca(response
					.getQuantidadeViaCobranca());
			saida.setCdTipoLayoutArquivo(response.getCdTipoLayoutArquivo());
			saida.setCdPeriodicidadeCobrancaTarifa(response
					.getCdPeriodicidadeCobrancaTarifa());
			saida.setDsPeriodicidadeCobrancaTarifa(response
					.getDsPeriodicidadeCobrancaTarifa());
			saida.setNrFechamentoApuracaoTarifa(response
					.getNrFechamentoApuracaoTarifa());
			saida.setQuantidadeDiaCobrancaTarifa(response
					.getQuantidadeDiaCobrancaTarifa());
			saida.setCdTipoReajusteTarifa(response.getCdTipoReajusteTarifa());
			saida.setCdPeriodicidadeReajusteTarifa(response
					.getCdPeriodicidadeReajusteTarifa());
			saida.setDsPeriodicidadeReajusteTarifa(response
					.getDsPeriodicidadeReajusteTarifa());
			saida.setQuantidadeMesReajusteTarifa(response
					.getQuantidadeMesReajusteTarifa());
			saida.setCdIndicadorEconomicoReajuste(response
					.getCdIndicadorEconomicoReajuste());
			saida.setPercentualIndiceReajusteTarifa(response
					.getPercentualIndiceReajusteTarifa());
			saida.setPeriodicidadeTarifaCatalogo(response
					.getPeriodicidadeTarifaCatalogo());

			saida.setDsRejeicaoLote(response.getDsRejeicaoLote());
			saida.setCdDisponibilizacaoSalarioCriterio(response
					.getCdDisponibilizacaoSalarioCriterio());
			saida.setDsDisponibilizacaoSalarioCriterio(response
					.getDsDisponibilizacaoSalarioCriterio());
			saida.setDsMidiaDisponivel(response.getDsMidiaDisponivel());
			saida.setCdDisponibilizacaoSalarioNao(response
					.getCdDisponibilizacaoSalarioNao());
			saida.setDsDisponibilizacaoSalarioNao(response
					.getDsDisponibilizacaoSalarioNao());
			saida.setDsDestinoComprovante(response.getDsDestinoComprovante());
			saida.setCdConsultaEndereco(response.getCdConsultaEndereco());
			saida.setDsConsultaEndereco(response.getDsConsultaEndereco());
			saida.setCdIndicadorRetornoInternet(response
					.getCdIndicadorRetornoInternet());
			saida.setDsCctciaInscricaoFavorecido(response
					.getDsCctciaInscricaoFavorecido());
		}
		// Recadastramento de Benefici�rio
		if (entrada.getCdParametroTela() == 14) {
			saida.setCdTipoIdentificacaoBeneficio(response
					.getCdTipoIdentificacaoBeneficio());
			saida.setDsTipoIdentificacaoBeneficio(response
					.getDsTipoIdentificacaoBeneficio());
			saida.setCdCctciaIdentificacaoBeneficio(response
					.getCdCctciaIdentificacaoBeneficio());
			saida.setDsCctciaIdentificacaoBeneficio(response
					.getDsCctciaIdentificacaoBeneficio());
			saida.setCdCriterioEnquandraBeneficio(response
					.getCdCriterioEnquandraBeneficio());
			saida.setDsCriterioEnquandraBeneficio(response
					.getDsCriterioEnquandraBeneficio());
			saida.setCdPrincipalEnquaRecadastramento(response
					.getCdPrincipalEnquaRecadastramento());
			saida.setDsPrincipalEnquaRecadastramento(response
					.getDsPrincipalEnquaRecadastramento());
			saida.setDsCriterioEnquadraRecadastramento(response
					.getDsCriterioEnquadraRecadastramento());
			saida.setCdCriterioEnquadraRecadastramento(response
					.getCdCriterioEnquadraRecadastramento());
			saida.setQuantidadeEtapaRecadastramentoBeneficio(response
					.getQuantidadeEtapaRecadastramentoBeneficio());
			saida.setQuantidadeFaseRecadastramentoBeneficio(response
					.getQuantidadeFaseRecadastramentoBeneficio());
			saida.setQuantidadeMesFaseRecadastramento(response
					.getQuantidadeMesFaseRecadastramento());
			saida.setQuantidadeMesEtapaRecadastramento(response
					.getQuantidadeMesEtapaRecadastramento());
			saida.setCdManutencaoBaseRecadastramento(response
					.getCdManutencaoBaseRecadastramento());
			saida.setCdPeriodicidadeEnvioRemessa(response
					.getCdPeriodicidadeEnvioRemessa());
			saida.setDsPeriodicidadeEnvioRemessa(response
					.getDsPeriodicidadeEnvioRemessa());
			saida.setCdBaseRecadastramentoBeneficio(response
					.getCdBaseRecadastramentoBeneficio());
			saida.setCdTipoCargaRecadastramento(response
					.getCdTipoCargaRecadastramento());
			saida.setCdAntecipacaoRecadastramentoBeneficiario(response
					.getCdAntecipacaoRecadastramentoBeneficiario());
			saida.setDtLimiteVinculoCarga(response.getDtLimiteVinculoCarga());
			saida.setDtInicioRecadastramentoBeneficio(response
					.getDtInicioRecadastramentoBeneficio());
			saida.setDtFimRecadastramentoBeneficio(response
					.getDtFimRecadastramentoBeneficio());
			saida.setCdAcertoDadosRecadastramento(response
					.getCdAcertoDadosRecadastramento());
			saida.setDtInicioAcertoRecadastramento(response
					.getDtInicioAcertoRecadastramento());
			saida.setDtFimAcertoRecadastramento(response
					.getDtFimAcertoRecadastramento());
			saida.setCdMensagemRecadastramentoMidia(response
					.getCdMensagemRecadastramentoMidia());
			saida.setCdMidiaMensagemRecadastramento(response
					.getCdMidiaMensagemRecadastramento());
			saida.setCdPeriodicidadeCobrancaTarifa(response
					.getCdPeriodicidadeCobrancaTarifa());
			saida.setDsPeriodicidadeCobrancaTarifa(response
					.getDsPeriodicidadeCobrancaTarifa());
			saida.setNrFechamentoApuracaoTarifa(response
					.getNrFechamentoApuracaoTarifa());
			saida.setQuantidadeDiaCobrancaTarifa(response
					.getQuantidadeDiaCobrancaTarifa());
			saida.setCdPeriodicidadeReajusteTarifa(response
					.getCdPeriodicidadeReajusteTarifa());
			saida.setDsPeriodicidadeReajusteTarifa(response
					.getDsPeriodicidadeReajusteTarifa());
			saida.setCdTipoReajusteTarifa(response.getCdTipoReajusteTarifa());
			saida.setQuantidadeMesReajusteTarifa(response
					.getQuantidadeMesReajusteTarifa());
			saida.setCdIndicadorEconomicoReajuste(response
					.getCdIndicadorEconomicoReajuste());
			saida.setDsIndicadorEconomicoReajuste(response
					.getDsIndicadorEconomicoReajuste());
			saida.setPercentualIndiceReajusteTarifa((response
					.getPercentualIndiceReajusteTarifa()));
			saida.setPeriodicidadeTarifaCatalogo((response
					.getPeriodicidadeTarifaCatalogo()));
			saida.setDsCctciaIdentificacaoBeneficio(response
					.getDsCctciaIdentificacaoBeneficio());
			saida.setCdPrincipalEnquaRecadastramento(response
					.getCdPrincipalEnquaRecadastramento());
			saida.setDsPrincipalEnquaRecadastramento(response
					.getDsPrincipalEnquaRecadastramento());
			saida.setCdCriterioEnquandraBeneficio(response
					.getCdCriterioEnquandraBeneficio());
			saida.setDsCriterioEnquandraBeneficio(response
					.getDsCriterioEnquandraBeneficio());
			saida.setCdBaseRecadastramentoBeneficio(response
					.getCdBaseRecadastramentoBeneficio());
			saida.setDsBaseRecadastramentoBeneficio(response
					.getDsBaseRecadastramentoBeneficio());
			saida.setDtInicioRecadastramentoBeneficio(response
					.getDtInicioRecadastramentoBeneficio());
			saida.setDtFimRecadastramentoBeneficio(response
					.getDtFimRecadastramentoBeneficio());
			saida.setCdManutencaoBaseRecadastramento(response
					.getCdManutencaoBaseRecadastramento());
			saida.setCdPeriodicidadeEnvioRemessa(response
					.getCdPeriodicidadeEnvioRemessa());
			saida.setDsPeriodicidadeEnvioRemessa(response
					.getDsPeriodicidadeEnvioRemessa());
			saida.setCdTipoCargaRecadastramento(response
					.getCdTipoCargaRecadastramento());
			saida.setDsTipoCargaRecadastramento(response
					.getDsTipoCargaRecadastramento());
			saida.setDtInicioRastreabilidadeTitulo(response
					.getDtInicioRastreabilidadeTitulo());
			saida.setCdAcertoDadosRecadastramento(response
					.getCdAcertoDadosRecadastramento());
			saida.setCdAntecipacaoRecadastramentoBeneficiario(response
					.getCdAntecipacaoRecadastramentoBeneficiario());
			saida.setDtInicioAcertoRecadastramento(response
					.getDtInicioAcertoRecadastramento());
			saida.setDtFimAcertoRecadastramento(response
					.getDtFimAcertoRecadastramento());
			saida.setCdMensagemRecadastramentoMidia(response
					.getCdMensagemRecadastramentoMidia());
			saida.setCdMidiaMensagemRecadastramento(response
					.getCdMidiaMensagemRecadastramento());
			saida.setDsMidiaMensagemRecadastramento(response
					.getDsMidiaMensagemRecadastramento());
			saida.setCdIndicadorRetornoInternet(response
					.getCdIndicadorRetornoInternet());
			saida.setDsCctciaInscricaoFavorecido(response
					.getDsCctciaInscricaoFavorecido());
		}

		/**************************************** MODALIDADE *******************************************/
		// Aviso de Recadastramento
		if (entrada.getCdParametroTela() == 15) {
			saida.setCdMomentoAvisoRecadastramento(response
					.getCdMomentoAvisoRecadastramento());
			saida.setDsMomentoAvisoRecadastramento(response
					.getDsMomentoAvisoRecadastramento());
			saida.setCdConsultaEndereco(response.getCdConsultaEndereco());
			saida.setDsConsultaEndereco(response.getDsConsultaEndereco());
			saida.setQuantidadeAntecedencia(response
					.getQuantidadeAntecedencia());
			saida.setCdDestinoAviso(response.getCdDestinoAviso());
			saida.setDsDestinoAviso(response.getDsDestinoAviso());
			saida.setCdAgrupamentoAviso(response.getCdAgrupamentoAviso());
			saida.setDsAgrupamentoAviso(response.getDsAgrupamentoAviso());
			saida.setCdMomentoFormularioRecadastramento(response
					.getCdMomentoFormularioRecadastramento());
			saida.setDsMomentoFormularioRecadastramento(response
					.getDsMomentoFormularioRecadastramento());
			saida.setCdIndicadorRetornoInternet(response
					.getCdIndicadorRetornoInternet());
			saida.setDsCctciaInscricaoFavorecido(response
					.getDsCctciaInscricaoFavorecido());
		}
		// Formul�rio de Recadastramento
		if (entrada.getCdParametroTela() == 16) {
			saida.setCdMomentoAvisoRecadastramento(response
					.getCdMomentoAvisoRecadastramento());
			saida.setDsMomentoAvisoRecadastramento(response
					.getDsMomentoAvisoRecadastramento());
			saida.setCdConsultaEndereco(response.getCdConsultaEndereco());
			saida.setDsConsultaEndereco(response.getDsConsultaEndereco());
			saida.setQuantidadeAntecedencia(response
					.getQuantidadeAntecedencia());
			saida.setCdDestinoAviso(response
					.getCdDestinoFormularioRecadastramento());
			saida.setDsDestinoAviso(response
					.getDsDestinoFormularioRecadastramento());
			saida.setCdAgrupamentoAviso(response
					.getCdAgrupamentoFormularioRecadastro());
			saida.setDsAgrupamentoAviso(response
					.getDsAgrupamentoFormularioRecadastro());
			saida.setCdMomentoFormularioRecadastramento(response
					.getCdMomentoFormularioRecadastramento());
			saida.setDsMomentoFormularioRecadastramento(response
					.getDsMomentoFormularioRecadastramento());
			saida.setCdIndicadorRetornoInternet(response
					.getCdIndicadorRetornoInternet());
			saida.setDsCctciaInscricaoFavorecido(response
					.getDsCctciaInscricaoFavorecido());
		}
		// Pagamento de Fornecedor - TED
		// Pagamento de Fornecedor - DOC
		if (entrada.getCdParametroTela() == 17
				|| entrada.getCdParametroTela() == 18) {
			saida.setCdConsultaSaldoPagamento(response
					.getCdConsultaSaldoPagamento());
			saida.setDsConsultaSaldoPagamento(response
					.getDsConsultaSaldoPagamento());
			saida.setCdUtilizacaoFavorecidoControle(response
					.getCdUtilizacaoFavorecidoControle());
			saida.setCdPagamentoNaoUtil(response.getCdPagamentoNaoUtil());
			saida.setDsPagamentoNaoUtil(response.getDsPagamentoNaoUtil());
			saida.setCdPrioridadeEfetivacaoPagamento(response
					.getCdPrioridadeEfetivacaoPagamento());
			saida.setDsPrioridadeEfetivacaoPagamento(response
					.getDsPrioridadeEfetivacaoPagamento());
			saida.setCdLancamentoFuturoCredito(response
					.getCdLancamentoFuturoCredito());
			saida.setValorLimiteIndividualPagamento(response
					.getValorLimiteIndividualPagamento());
			saida.setQuantidadeDiaRepiqueConsulta(response
					.getQuantidadeDiaRepiqueConsulta());
			saida.setValorFavorecidoNaoCadastrado(response
					.getValorFavorecidoNaoCadastrado());
			saida.setCdMomentoProcessamentoPagamento(response
					.getCdMomentoProcessamentoPagamento());
			saida.setDsMomentoProcessamentoPagamento(response
					.getDsMomentoProcessamentoPagamento());
			saida.setCdRejeicaoEfetivacaoLote(response
					.getCdRejeicaoEfetivacaoLote());
			saida.setDsRejeicaoEfetivacaoLote(response
					.getDsRejeicaoEfetivacaoLote());
			saida.setCdRejeicaoAgendamentoLote(response
					.getCdRejeicaoAgendamentoLote());
			saida.setDsRejeicaoAgendamentoLote(response
					.getDsRejeicaoAgendamentoLote());
			saida.setCdLancamentoFuturoDebito(response
					.getCdLancamentoFuturoDebito());
			saida.setCdFavorecidoConsultaPagamento(response
					.getCdFavorecidoConsultaPagamento());
			saida.setValorLimiteDiarioPagamento(response
					.getValorLimiteDiarioPagamento());
			saida.setQuantidadeDiaFloatingPagamento(response
					.getQuantidadeDiaFloatingPagamento());
			saida.setQuantidadeMaximaInconsistenteLote(response
					.getQuantidadeMaximaInconsistenteLote());
			saida.setPercentualMaximoInconsistenteLote(response
					.getPercentualMaximoInconsistenteLote());
			saida.setDsOutraidentificacaoFavorecido(response
					.getDsOutraidentificacaoFavorecido());
			saida.setCdTipoConsistenciaLista(response
					.getCdTipoConsistenciaLista());
			saida.setDsTipoConsistenciaLista(response
					.getDsTipoConsistenciaLista());
			saida.setCdPagamentoNaoUtil(response.getCdPagamentoNaoUtil());
			saida.setDsPagamentoNaoUtil(response.getDsPagamentoNaoUtil());
			saida.setCdIndicadorRetornoInternet(response
					.getCdIndicadorRetornoInternet());
			saida.setDsCctciaInscricaoFavorecido(response
					.getDsCctciaInscricaoFavorecido());
			
			saida.setDsOrigemIndicador(response.getDsOrigemIndicador());
			saida.setDsIndicador(response.getDsIndicador());

		}
		// Pagamento Via Ordem de Pagamento
		if (entrada.getCdParametroTela() == 19) {
			saida.setCdConsultaSaldoPagamento(response
					.getCdConsultaSaldoPagamento());
			saida.setDsConsultaSaldoPagamento(response
					.getDsConsultaSaldoPagamento());
			saida.setCdUtilizacaoFavorecidoControle(response
					.getCdUtilizacaoFavorecidoControle());
			saida.setCdRejeicaoAgendamentoLote(response
					.getCdRejeicaoAgendamentoLote());
			saida.setDsRejeicaoAgendamentoLote(response
					.getDsRejeicaoAgendamentoLote());
			saida.setCdRejeicaoEfetivacaoLote(response
					.getCdRejeicaoEfetivacaoLote());
			saida.setDsRejeicaoEfetivacaoLote(response
					.getDsRejeicaoEfetivacaoLote());
			saida.setValorLimiteIndividualPagamento(response
					.getValorLimiteIndividualPagamento());
			saida.setCdFavorecidoConsultaPagamento(response
					.getCdFavorecidoConsultaPagamento());
			saida.setQuantidadeDiaRepiqueConsulta(response
					.getQuantidadeDiaRepiqueConsulta());
			saida.setValorFavorecidoNaoCadastrado(response
					.getValorFavorecidoNaoCadastrado());
			saida.setCdMomentoProcessamentoPagamento(response
					.getCdMomentoProcessamentoPagamento());
			saida.setDsMomentoProcessamentoPagamento(response
					.getDsMomentoProcessamentoPagamento());
			saida.setValorLimiteDiarioPagamento(response
					.getValorLimiteDiarioPagamento());
			saida.setCdAgendamentoPagamentoVencido(response
					.getCdAgendamentoPagamentoVencido());
			saida.setCdAgendamentoValorMenor(response
					.getCdAgendamentoValorMenor());
			saida.setQuantidadeMaximaTituloVencido(response
					.getQuantidadeMaximaTituloVencido());
			saida.setCdPrioridadeEfetivacaoPagamento(response
					.getCdPrioridadeEfetivacaoPagamento());
			saida.setDsPrioridadeEfetivacaoPagamento(response
					.getDsPrioridadeEfetivacaoPagamento());
			saida.setCdLancamentoFuturoDebito(response
					.getCdLancamentoFuturoDebito());
			saida.setCdPagamentoNaoUtil(response.getCdPagamentoNaoUtil());
			saida.setDsPagamentoNaoUtil(response.getDsPagamentoNaoUtil());
			saida.setCdMomentoDebitoPagamento(response
					.getCdMomentoDebitoPagamento());
			saida.setQuantidadeDiaExpiracao(response
					.getQuantidadeDiaExpiracao());
			saida.setCdOutraidentificacaoFavorecido(response
					.getCdOutraidentificacaoFavorecido());
			saida.setCdFavorecidoConsultaPagamento(response
					.getCdFavorecidoConsultaPagamento());
			saida.setCdLancamentoFuturoCredito(response
					.getCdLancamentoFuturoCredito());
			saida.setCdMomentoDebitoPagamento(response
					.getCdMomentoDebitoPagamento());
			saida.setDsMomentoDebitoPagamento(response
					.getDsMomentoDebitoPagamento());
			saida.setDsLocalEmissao(response.getCdLocalEmissao());

			// novas
			saida.setQuantidadeDiaFloatingPagamento(response
					.getQuantidadeDiaFloatingPagamento());
			saida.setQuantidadeMaximaInconsistenteLote(response
					.getQuantidadeMaximaInconsistenteLote());
			saida.setPercentualMaximoInconsistenteLote(response
					.getPercentualMaximoInconsistenteLote());
			saida.setDsOutraidentificacaoFavorecido(response
					.getDsOutraidentificacaoFavorecido());
			saida.setCdUtilizacaoFavorecidoControle(response
					.getCdUtilizacaoFavorecidoControle());
			saida.setValorFavorecidoNaoCadastrado(response
					.getValorFavorecidoNaoCadastrado());
			saida.setCdTipoConsistenciaLista(response
					.getCdTipoConsistenciaLista());
			saida.setDsTipoConsistenciaLista(response
					.getDsTipoConsistenciaLista());
			saida.setCdCctciaInscricaoFavorecido(response
					.getCdCctciaInscricaoFavorecido());
			saida.setDsCctciaInscricaoFavorecido(response
					.getDsCctciaInscricaoFavorecido());
			saida.setCdIndicadorRetornoInternet(response
					.getCdIndicadorRetornoInternet());
			saida.setCdTipoIdentificacaoBeneficio(response
					.getCdTipoIdentificacaoBeneficio());
			saida.setDsTipoIdentificacaoBeneficio(response
					.getDsTipoIdentificacaoBeneficio());
		}

		// Pagamento Via Cr�dito em Conta
		if (entrada.getCdParametroTela() == 20) {
			saida.setCdConsultaSaldoPagamento(response
					.getCdConsultaSaldoPagamento());
			saida.setDsConsultaSaldoPagamento(response
					.getDsConsultaSaldoPagamento());
			saida.setCdUtilizacaoFavorecidoControle(response
					.getCdUtilizacaoFavorecidoControle());
			saida.setCdRejeicaoAgendamentoLote(response
					.getCdRejeicaoAgendamentoLote());
			saida.setDsRejeicaoAgendamentoLote(response
					.getDsRejeicaoAgendamentoLote());
			saida.setValorLimiteIndividualPagamento(response
					.getValorLimiteIndividualPagamento());
			saida.setCdFavorecidoConsultaPagamento(response
					.getCdFavorecidoConsultaPagamento());
			saida.setQuantidadeDiaRepiqueConsulta(response
					.getQuantidadeDiaRepiqueConsulta());
			saida.setValorFavorecidoNaoCadastrado(response
					.getValorFavorecidoNaoCadastrado());
			saida.setCdMomentoProcessamentoPagamento(response
					.getCdMomentoProcessamentoPagamento());
			saida.setDsMomentoProcessamentoPagamento(response
					.getDsMomentoProcessamentoPagamento());
			saida.setCdRejeicaoEfetivacaoLote(response
					.getCdRejeicaoEfetivacaoLote());
			saida.setDsRejeicaoEfetivacaoLote(response
					.getDsRejeicaoEfetivacaoLote());
			saida.setValorLimiteDiarioPagamento(response
					.getValorLimiteDiarioPagamento());
			saida.setCdLancamentoFuturoCredito(response
					.getCdLancamentoFuturoCredito());
			saida.setCdAgendamentoPagamentoVencido(response
					.getCdAgendamentoPagamentoVencido());
			saida.setCdAgendamentoValorMenor(response
					.getCdAgendamentoValorMenor());
			saida.setQuantidadeMaximaTituloVencido(response
					.getQuantidadeMaximaTituloVencido());
			saida.setCdindicadorLancamentoProgramado(response
					.getCdindicadorLancamentoProgramado());
			saida.setCdPrioridadeEfetivacaoPagamento(response
					.getCdPrioridadeEfetivacaoPagamento());
			saida.setDsPrioridadeEfetivacaoPagamento(response
					.getDsPrioridadeEfetivacaoPagamento());
			saida.setCdCctciaEspeBeneficio(response.getCdCctciaEspeBeneficio());
			saida.setDsCctciaEspeBeneficio(response.getDsCctciaEspeBeneficio());
			saida.setCdTratamentoContaTransferida(response
					.getCdTratamentoContaTransferida());
			saida.setDsTratamentoContaTransferida(response
					.getDsTratamentoContaTransferida());
			saida.setQuantidadeMaximaInconsistenteLote(response
					.getQuantidadeMaximaInconsistenteLote());
			saida.setPercentualMaximoInconsistenteLote(response
					.getPercentualMaximoInconsistenteLote());
			saida.setValorFavorecidoNaoCadastrado(response
					.getValorFavorecidoNaoCadastrado());
			saida.setQuantidadeDiaFloatingPagamento(response
					.getQuantidadeDiaFloatingPagamento());
			saida.setCdUtilizacaoFavorecidoControle(response
					.getCdUtilizacaoFavorecidoControle());
			saida.setValorFavorecidoNaoCadastrado(response
					.getValorFavorecidoNaoCadastrado());
			saida.setCdTipoConsistenciaLista(response
					.getCdTipoConsistenciaLista());
			saida.setDsTipoConsistenciaLista(response
					.getDsTipoConsistenciaLista());
			saida.setCdLancamentoFuturoDebito(response
					.getCdLancamentoFuturoDebito());
			saida.setCdLancamentoFuturoCredito(response
					.getCdLancamentoFuturoCredito());
			saida.setCdPagamentoNaoUtil(response.getCdPagamentoNaoUtil());
			saida.setDsPagamentoNaoUtil(response.getDsPagamentoNaoUtil());
			saida.setDsOutraidentificacaoFavorecido(response
					.getDsOutraidentificacaoFavorecido());
			saida.setCdLiberacaoLoteProcessado(response
					.getCdLiberacaoLoteProcessado());
			saida.setDsIndicadorMensagemPersonalizada(response
					.getDsIndicadorMensagemPersonalizada());
			saida.setCdIndicadorRetornoInternet(response
					.getCdIndicadorRetornoInternet());
			saida.setDsCctciaInscricaoFavorecido(response
					.getDsCctciaInscricaoFavorecido());
			saida.setCdMeioPagamentoDebito(response.getCdMeioPagamentoDebito());
			saida.setDsMeioPagamentoDebito(response.getDsMeioPagamentoDebito());
		}
		// Pagamento Via D�bito em Conta
		if (entrada.getCdParametroTela() == 21) {
			saida.setCdConsultaSaldoPagamento(response
					.getCdConsultaSaldoPagamento());
			saida.setDsConsultaSaldoPagamento(response
					.getDsConsultaSaldoPagamento());
			saida.setCdUtilizacaoFavorecidoControle(response
					.getCdUtilizacaoFavorecidoControle());
			saida.setCdPagamentoNaoUtil(response.getCdPagamentoNaoUtil());
			saida.setDsPagamentoNaoUtil(response.getDsPagamentoNaoUtil());
			saida.setCdRejeicaoAgendamentoLote(response
					.getCdRejeicaoAgendamentoLote());
			saida.setDsRejeicaoAgendamentoLote(response
					.getDsRejeicaoAgendamentoLote());
			saida.setValorLimiteIndividualPagamento(response
					.getValorLimiteIndividualPagamento());
			saida.setQuantidadeDiaRepiqueConsulta(response
					.getQuantidadeDiaRepiqueConsulta());
			saida.setValorFavorecidoNaoCadastrado(response
					.getValorFavorecidoNaoCadastrado());
			saida.setCdMomentoProcessamentoPagamento(response
					.getCdMomentoProcessamentoPagamento());
			saida.setDsMomentoProcessamentoPagamento(response
					.getDsMomentoProcessamentoPagamento());
			saida.setCdRejeicaoEfetivacaoLote(response
					.getCdRejeicaoEfetivacaoLote());
			saida.setDsRejeicaoEfetivacaoLote(response
					.getDsRejeicaoEfetivacaoLote());
			saida.setValorLimiteDiarioPagamento(response
					.getValorLimiteDiarioPagamento());
			saida.setCdLancamentoFuturoDebito(response
					.getCdLancamentoFuturoDebito());
			saida.setCdPrioridadeEfetivacaoPagamento(response
					.getCdPrioridadeEfetivacaoPagamento());
			saida.setDsPrioridadeEfetivacaoPagamento(response
					.getDsPrioridadeEfetivacaoPagamento());
			saida.setCdPermissaoDebitoOnline(response
					.getCdPermissaoDebitoOnline());
			saida.setQuantidadeDiaFloatingPagamento(response
					.getQuantidadeDiaFloatingPagamento());
			saida.setCdTipoConsistenciaLista(response
					.getCdTipoConsistenciaLista());
			saida.setDsTipoConsistenciaLista(response
					.getDsTipoConsistenciaLista());
			saida.setCdLancamentoFuturoDebito(response
					.getCdLancamentoFuturoDebito());
			saida.setCdLancamentoFuturoCredito(response
					.getCdLancamentoFuturoCredito());
			saida.setCdPagamentoNaoUtil(response.getCdPagamentoNaoUtil());
			saida.setDsPagamentoNaoUtil(response.getDsPagamentoNaoUtil());
			saida.setDsOutraidentificacaoFavorecido(response
					.getDsOutraidentificacaoFavorecido());
			saida.setCdLiberacaoLoteProcessado(response
					.getCdLiberacaoLoteProcessado());
			saida.setDsIndicadorMensagemPersonalizada(response
					.getDsIndicadorMensagemPersonalizada());
			saida.setCdMomentoProcessamentoPagamento(response
					.getCdMomentoProcessamentoPagamento());
			saida.setDsMomentoProcessamentoPagamento(response
					.getDsMomentoProcessamentoPagamento());
			saida.setCdRejeicaoAgendamentoLote(response
					.getCdRejeicaoAgendamentoLote());
			saida.setDsRejeicaoAgendamentoLote(response
					.getDsRejeicaoAgendamentoLote());
			saida.setCdRejeicaoEfetivacaoLote(response
					.getCdRejeicaoEfetivacaoLote());
			saida.setDsRejeicaoEfetivacaoLote(response
					.getDsRejeicaoEfetivacaoLote());
			saida.setQuantidadeMaximaInconsistenteLote(response
					.getQuantidadeMaximaInconsistenteLote());
			saida.setPercentualMaximoInconsistenteLote(response
					.getPercentualMaximoInconsistenteLote());
			saida.setCdFavorecidoConsultaPagamento(response
					.getCdFavorecidoConsultaPagamento());
			saida.setCdCctciaEspeBeneficio(response.getCdCctciaEspeBeneficio());
			saida.setCdIndicadorRetornoInternet(response
					.getCdIndicadorRetornoInternet());
			saida.setDsCctciaInscricaoFavorecido(response
					.getDsCctciaInscricaoFavorecido());
		}
		// Pagamento Via T�tulo Bradesco
		// Pagamento Via T�tulo Outros Bancos
		if (entrada.getCdParametroTela() == 22
				|| entrada.getCdParametroTela() == 23) {
			saida.setCdConsultaSaldoPagamento(response
					.getCdConsultaSaldoPagamento());
			saida.setDsConsultaSaldoPagamento(response
					.getDsConsultaSaldoPagamento());
			saida.setCdUtilizacaoFavorecidoControle(response
					.getCdUtilizacaoFavorecidoControle());
			saida.setCdPagamentoNaoUtil(response.getCdPagamentoNaoUtil());
			saida.setDsPagamentoNaoUtil(response.getDsPagamentoNaoUtil());
			saida.setCdRejeicaoAgendamentoLote(response
					.getCdRejeicaoAgendamentoLote());
			saida.setDsRejeicaoAgendamentoLote(response
					.getDsRejeicaoAgendamentoLote());
			saida.setValorLimiteIndividualPagamento(response
					.getValorLimiteIndividualPagamento());
			saida.setCdFavorecidoConsultaPagamento(response
					.getCdFavorecidoConsultaPagamento());
			saida.setQuantidadeDiaRepiqueConsulta(response
					.getQuantidadeDiaRepiqueConsulta());
			saida.setValorFavorecidoNaoCadastrado(response
					.getValorFavorecidoNaoCadastrado());
			saida.setCdMomentoProcessamentoPagamento(response
					.getCdMomentoProcessamentoPagamento());
			saida.setDsMomentoProcessamentoPagamento(response
					.getDsMomentoProcessamentoPagamento());
			saida.setCdRejeicaoEfetivacaoLote(response
					.getCdRejeicaoEfetivacaoLote());
			saida.setDsRejeicaoEfetivacaoLote(response
					.getDsRejeicaoEfetivacaoLote());
			saida.setValorLimiteDiarioPagamento(response
					.getValorLimiteDiarioPagamento());
			saida.setCdLancamentoFuturoDebito(response
					.getCdLancamentoFuturoDebito());
			saida.setCdAgendamentoValorMenor(response
					.getCdAgendamentoValorMenor());
			saida.setCdAgendamentoPagamentoVencido(response
					.getCdAgendamentoPagamentoVencido());
			saida.setQuantidadeMaximaTituloVencido(response
					.getQuantidadeMaximaTituloVencido());
			saida.setCdPrioridadeEfetivacaoPagamento(response
					.getCdPrioridadeEfetivacaoPagamento());
			saida.setDsPrioridadeEfetivacaoPagamento(response
					.getDsPrioridadeEfetivacaoPagamento());
			saida.setQuantidadeDiaFloatingPagamento(response
					.getQuantidadeDiaFloatingPagamento());
			saida.setQuantidadeMaximaInconsistenteLote(response
					.getQuantidadeMaximaInconsistenteLote());
			saida.setPercentualMaximoInconsistenteLote(response
					.getPercentualMaximoInconsistenteLote());
			saida.setDsOutraidentificacaoFavorecido(response
					.getDsOutraidentificacaoFavorecido());
			saida.setCdTipoConsistenciaLista(response
					.getCdTipoConsistenciaLista());
			saida.setDsTipoConsistenciaLista(response
					.getDsTipoConsistenciaLista());
			saida.setDsAgendamentoPagamentoVencido(response
					.getDsAgendamentoPagamentoVencido());
			saida.setDsAgendamentoValorMenor(response
					.getDsAgendamentoValorMenor());
			saida.setQuantidadeMaximaTituloVencido(response
					.getQuantidadeMaximaTituloVencido());
			saida.setValorLimiteIndividualPagamento(response
					.getValorLimiteIndividualPagamento());
			saida.setValorLimiteDiarioPagamento(response
					.getValorLimiteDiarioPagamento());
			saida.setCdIndicadorRetornoInternet(response
					.getCdIndicadorRetornoInternet());
			saida.setDsCctciaInscricaoFavorecido(response
					.getDsCctciaInscricaoFavorecido());
			saida.setVlPercentualDiferencaTolerada(response.getVlPercentualDiferencaTolerada());


			saida.setCdConsistenciaCpfCnpjBenefAvalNpc(response.getCdConsistenciaCpfCnpjBenefAvalNpc());
			saida.setDsConsistenciaCpfCnpjBenefAvalNpc(response.getDsConsistenciaCpfCnpjBenefAvalNpc());
			
			
		}
		// Pagamento Via Dep�sito Identificado
		if (entrada.getCdParametroTela() == 24) {
			saida.setCdConsultaSaldoPagamento(response
					.getCdConsultaSaldoPagamento());
			saida.setDsConsultaSaldoPagamento(response
					.getDsConsultaSaldoPagamento());
			saida.setQuantidadeDiaRepiqueConsulta(response
					.getQuantidadeDiaRepiqueConsulta());
			saida.setCdUtilizacaoFavorecidoControle(response
					.getCdUtilizacaoFavorecidoControle());
			saida.setDsUtilizacaoFavorecidoControle(response
					.getDsUtilizacaoFavorecidoControle());
			saida.setValorFavorecidoNaoCadastrado(response
					.getValorFavorecidoNaoCadastrado());
			saida.setCdPagamentoNaoUtil(response.getCdPagamentoNaoUtil());
			saida.setDsPagamentoNaoUtil(response.getDsPagamentoNaoUtil());
			saida.setCdMomentoProcessamentoPagamento(response
					.getCdMomentoProcessamentoPagamento());
			saida.setDsMomentoProcessamentoPagamento(response
					.getDsMomentoProcessamentoPagamento());
			saida.setCdRejeicaoAgendamentoLote(response
					.getCdRejeicaoAgendamentoLote());
			saida.setDsRejeicaoAgendamentoLote(response
					.getDsRejeicaoAgendamentoLote());
			saida.setCdRejeicaoEfetivacaoLote(response
					.getCdRejeicaoEfetivacaoLote());
			saida.setDsRejeicaoEfetivacaoLote(response
					.getDsRejeicaoEfetivacaoLote());
			saida.setValorLimiteIndividualPagamento(response
					.getValorLimiteIndividualPagamento());
			saida.setCdFavorecidoConsultaPagamento(response
					.getCdFavorecidoConsultaPagamento());
			saida.setValorLimiteDiarioPagamento(response
					.getValorLimiteDiarioPagamento());
			saida.setCdLancamentoFuturoDebito(response
					.getCdLancamentoFuturoDebito());
			saida.setCdRejeicaoEfetivacaoLote(response
					.getCdRejeicaoEfetivacaoLote());
			saida.setDsRejeicaoEfetivacaoLote(response
					.getDsRejeicaoEfetivacaoLote());
			saida.setCdPrioridadeEfetivacaoPagamento(response
					.getCdPrioridadeEfetivacaoPagamento());
			saida.setDsPrioridadeEfetivacaoPagamento(response
					.getDsPrioridadeEfetivacaoPagamento());
			saida.setCdLancamentoFuturoCredito(response
					.getCdLancamentoFuturoCredito());
			saida.setDsLancamentoFuturoCredito(response
					.getDsLancamentoFuturoCredito());
			saida.setQuantidadeDiaFloatingPagamento(response
					.getQuantidadeDiaFloatingPagamento());
			saida.setQuantidadeMaximaInconsistenteLote(response
					.getQuantidadeMaximaInconsistenteLote());
			saida.setPercentualMaximoInconsistenteLote(response
					.getPercentualMaximoInconsistenteLote());
			saida.setDsOutraidentificacaoFavorecido(response
					.getDsOutraidentificacaoFavorecido());
			saida.setCdTipoConsistenciaLista(response
					.getCdTipoConsistenciaLista());
			saida.setDsTipoConsistenciaLista(response
					.getDsTipoConsistenciaLista());
			saida.setCdPagamentoNaoUtil(response.getCdPagamentoNaoUtil());
			saida.setCdCctciaInscricaoFavorecido(response
					.getCdCctciaInscricaoFavorecido());
			saida.setDsCctciaInscricaoFavorecido(response
					.getDsCctciaInscricaoFavorecido());
			saida.setDsPagamentoNaoUtil(response.getDsPagamentoNaoUtil());
			saida.setCdIndicadorRetornoInternet(response
					.getCdIndicadorRetornoInternet());
		}
		// Pagamento de Fornecedores - OCT
		if (entrada.getCdParametroTela() == 25) {
			saida.setCdConsultaSaldoPagamento(response
					.getCdConsultaSaldoPagamento());
			saida.setDsConsultaSaldoPagamento(response
					.getDsConsultaSaldoPagamento());
			saida.setQuantidadeDiaRepiqueConsulta(response
					.getQuantidadeDiaRepiqueConsulta());
			saida.setCdUtilizacaoFavorecidoControle(response
					.getCdUtilizacaoFavorecidoControle());
			saida.setDsUtilizacaoFavorecidoControle(response
					.getDsUtilizacaoFavorecidoControle());
			saida.setValorFavorecidoNaoCadastrado(response
					.getValorFavorecidoNaoCadastrado());
			saida.setCdPagamentoNaoUtil(response.getCdPagamentoNaoUtil());
			saida.setDsPagamentoNaoUtil(response.getDsPagamentoNaoUtil());
			saida.setCdMomentoProcessamentoPagamento(response
					.getCdMomentoProcessamentoPagamento());
			saida.setDsMomentoProcessamentoPagamento(response
					.getDsMomentoProcessamentoPagamento());
			saida.setCdRejeicaoAgendamentoLote(response
					.getCdRejeicaoAgendamentoLote());
			saida.setDsRejeicaoAgendamentoLote(response
					.getDsRejeicaoAgendamentoLote());
			saida.setCdRejeicaoEfetivacaoLote(response
					.getCdRejeicaoEfetivacaoLote());
			saida.setDsRejeicaoEfetivacaoLote(response
					.getDsRejeicaoEfetivacaoLote());
			saida.setValorLimiteIndividualPagamento(response
					.getValorLimiteIndividualPagamento());
			saida.setCdFavorecidoConsultaPagamento(response
					.getCdFavorecidoConsultaPagamento());
			saida.setValorLimiteDiarioPagamento(response
					.getValorLimiteDiarioPagamento());
			saida.setCdLancamentoFuturoDebito(response
					.getCdLancamentoFuturoDebito());
			saida.setCdRejeicaoEfetivacaoLote(response
					.getCdRejeicaoEfetivacaoLote());
			saida.setDsRejeicaoEfetivacaoLote(response
					.getDsRejeicaoEfetivacaoLote());
			saida.setCdPrioridadeEfetivacaoPagamento(response
					.getCdPrioridadeEfetivacaoPagamento());
			saida.setDsPrioridadeEfetivacaoPagamento(response
					.getDsPrioridadeEfetivacaoPagamento());
			saida.setCdLancamentoFuturoCredito(response
					.getCdLancamentoFuturoCredito());
			saida.setDsLancamentoFuturoCredito(response
					.getDsLancamentoFuturoCredito());
			saida.setQuantidadeDiaFloatingPagamento(response
					.getQuantidadeDiaFloatingPagamento());
			saida.setQuantidadeMaximaInconsistenteLote(response
					.getQuantidadeMaximaInconsistenteLote());
			saida.setPercentualMaximoInconsistenteLote(response
					.getPercentualMaximoInconsistenteLote());
			saida.setDsOutraidentificacaoFavorecido(response
					.getDsOutraidentificacaoFavorecido());
			saida.setCdTipoConsistenciaLista(response
					.getCdTipoConsistenciaLista());
			saida.setDsTipoConsistenciaLista(response
					.getDsTipoConsistenciaLista());
			saida.setCdPagamentoNaoUtil(response.getCdPagamentoNaoUtil());
			saida.setDsPagamentoNaoUtil(response.getDsPagamentoNaoUtil());
			saida.setCdCctciaInscricaoFavorecido(response
					.getCdCctciaInscricaoFavorecido());
			saida.setDsCctciaInscricaoFavorecido(response
					.getDsCctciaInscricaoFavorecido());
			saida.setCdIndicadorRetornoInternet(response
					.getCdIndicadorRetornoInternet());
		}
		// Rastreamento de T�tulos
		if (entrada.getCdParametroTela() == 26) {
			saida.setCdCriterioRastreabilidadeTitulo(response
					.getCdCriterioRastreabilidadeTitulo());
			saida.setDsCriterioRastreabilidadeTitulo(response
					.getDsCriterioRastreabilidadeTitulo());
			saida.setCdRastreabilidadeTituloTerceiro(response
					.getCdRastreabilidadeTituloTerceiro());
			saida.setCdRastreabilidadeNotaFiscal(response
					.getCdRastreabilidadeNotaFiscal());
			saida.setCdCapturaTituloRegistrado(response
					.getCdCapturaTituloRegistrado());
			saida.setCdIndicadorAgendamentoTitulo(response
					.getCdIndicadorAgendamentoTitulo());
			saida.setCdBloqueioEmissaoPapeleta(response
					.getCdBloqueioEmissaoPapeleta());
			saida.setCdAgendamentoRastreabilidadeFinal(response
					.getCdAgendamentoRastreabilidadeFinal());
			saida.setDtInicioRastreabilidadeTitulo(response
					.getDtInicioRastreabilidadeTitulo());
			saida.setDtInicioBloqueioPapeleta(response
					.getDtInicioBloqueioPapeleta());
			saida.setDtRegistroTitulo(response.getDtRegistroTitulo());
			// novas
			saida.setDsPermissaoDebitoOnline(response
					.getDsPermissaoDebitoOnline());
			saida.setCdIndicadorRetornoInternet(response
					.getCdIndicadorRetornoInternet());
			saida.setDsCctciaInscricaoFavorecido(response
					.getDsCctciaInscricaoFavorecido());
			saida.setDsExigeFilial(response.getDsExigeFilial());
		}
		// Prova de Vida
		if (entrada.getCdParametroTela() == 27) {
			saida.setCdAcaoNaoVida(response.getCdAcaoNaoVida());
			saida.setDsAcaoNaoVida(response.getDsAcaoNaoVida());
			saida.setQuantidadeMesComprovante(response
					.getQuantidadeMesComprovante());
			saida.setQuantidadeAnteriorVencimentoComprovante(response
					.getQuantidadeAnteriorVencimentoComprovante());
			saida.setCdIndicadorMensagemPersonalizada(response
					.getCdIndicadorMensagemPersonalizada());
			saida.setDsIndicadorMensagemPersonalizada(response
					.getDsIndicadorMensagemPersonalizada());
			saida.setQuantidadeAntecedencia(response
					.getQuantidadeAntecedencia());
			saida.setCdDestinoAviso(response.getCdDestinoAviso());
			saida.setDsDestinoAviso(response.getDsDestinoAviso());
			saida.setCdConsultaEndereco(response.getCdConsultaEndereco());
			saida.setDsConsultaEndereco(response.getDsConsultaEndereco());

			saida.setCdDestinoComprovante(response.getCdDestinoComprovante());
			saida.setDsDestinoComprovante(response.getDsDestinoComprovante());
			saida.setCdDestinoFormularioRecadastramento(response
					.getCdDestinoFormularioRecadastramento());
			saida.setCdIndicadorRetornoInternet(response
					.getCdIndicadorRetornoInternet());
			saida.setDsCctciaInscricaoFavorecido(response
					.getDsCctciaInscricaoFavorecido());
		}

		// GPS
		// GARE
		// Pagamentos de Tributos - DARF
		if (entrada.getCdParametroTela() == 28
				|| entrada.getCdParametroTela() == 29
				|| entrada.getCdParametroTela() == 30) {
			saida.setCdConsultaSaldoPagamento(response
					.getCdConsultaSaldoPagamento());
			saida.setDsConsultaSaldoPagamento(response
					.getDsConsultaSaldoPagamento());
			saida.setValorLimiteIndividualPagamento(response
					.getValorLimiteIndividualPagamento());
			saida.setCdTipoEfetivacaoPagamento(response
					.getCdTipoEfetivacaoPagamento());
			saida.setDsTipoEfetivacaoPagamento(response
					.getDsTipoEfetivacaoPagamento());
			saida.setValorLimiteDiarioPagamento(response
					.getValorLimiteDiarioPagamento());
			saida.setCdFavorecidoConsultaPagamento(response
					.getCdFavorecidoConsultaPagamento());
			saida.setDsFavorecidoConsultaPagamento(response
					.getDsFavorecidoConsultaPagamento());
			saida.setQuantidadeDiaRepiqueConsulta(response
					.getQuantidadeDiaRepiqueConsulta());
			saida.setCdPagamentoNaoUtil(response.getCdPagamentoNaoUtil());
			saida.setDsPagamentoNaoUtil(response.getDsPagamentoNaoUtil());
			saida.setCdMomentoProcessamentoPagamento(response
					.getCdMomentoProcessamentoPagamento());
			saida.setDsMomentoProcessamentoPagamento(response
					.getDsMomentoProcessamentoPagamento());
			saida.setCdRejeicaoAgendamentoLote(response
					.getCdRejeicaoAgendamentoLote());
			saida.setDsRejeicaoAgendamentoLote(response
					.getDsRejeicaoAgendamentoLote());
			saida.setCdRejeicaoEfetivacaoLote(response
					.getCdRejeicaoEfetivacaoLote());
			saida.setDsRejeicaoEfetivacaoLote(response
					.getDsRejeicaoEfetivacaoLote());
			saida.setCdPrioridadeEfetivacaoPagamento(response
					.getCdPrioridadeEfetivacaoPagamento());
			saida.setDsPrioridadeEfetivacaoPagamento(response
					.getDsPrioridadeEfetivacaoPagamento());
			saida.setCdUtilizacaoFavorecidoControle(response
					.getCdUtilizacaoFavorecidoControle());
			saida.setDsUtilizacaoFavorecidoControle(response
					.getDsUtilizacaoFavorecidoControle());
			saida.setValorFavorecidoNaoCadastrado(response
					.getValorFavorecidoNaoCadastrado());
			saida.setPercentualMaximoInconsistenteLote(response
					.getPercentualMaximoInconsistenteLote());
			saida.setQuantidadeMaximaInconsistenteLote(response
					.getQuantidadeMaximaInconsistenteLote());
			saida.setCdContagemConsultaSaldo(response
					.getCdContagemConsultaSaldo());
			saida.setQuantidadeDiaFloatingPagamento(response
					.getQuantidadeDiaFloatingPagamento());
			saida.setCdTipoConsistenciaLista(response
					.getCdTipoConsistenciaLista());
			saida.setDsTipoConsistenciaLista(response
					.getDsTipoConsistenciaLista());
			saida.setCdLancamentoFuturoDebito(response
					.getCdLancamentoFuturoDebito());
			saida.setCdLancamentoFuturoCredito(response
					.getCdLancamentoFuturoCredito());
			saida.setCdPagamentoNaoUtil(response.getCdPagamentoNaoUtil());
			saida.setDsPagamentoNaoUtil(response.getDsPagamentoNaoUtil());
			saida.setDsOutraidentificacaoFavorecido(response
					.getDsOutraidentificacaoFavorecido());
			saida.setCdIndicadorMensagemPersonalizada(response
					.getCdIndicadorMensagemPersonalizada());
			saida.setDsIndicadorMensagemPersonalizada(response
					.getDsIndicadorMensagemPersonalizada());
			saida.setCdPermissaoDebitoOnline(response
					.getCdPermissaoDebitoOnline());
			saida.setDsContagemConsultaSaldo(response
					.getDsContagemConsultaSaldo());
			saida.setCdIndicadorRetornoInternet(response
					.getCdIndicadorRetornoInternet());
			saida.setDsCctciaInscricaoFavorecido(response
					.getDsCctciaInscricaoFavorecido());
		}
		// C�digo de Barras
		if (entrada.getCdParametroTela() == 31) {
			saida.setCdConsultaSaldoPagamento(response
					.getCdConsultaSaldoPagamento());
			saida.setDsConsultaSaldoPagamento(response
					.getDsConsultaSaldoPagamento());
			saida.setValorLimiteIndividualPagamento(response
					.getValorLimiteIndividualPagamento());
			saida.setCdTipoEfetivacaoPagamento(response
					.getCdTipoEfetivacaoPagamento());
			saida.setDsTipoEfetivacaoPagamento(response
					.getDsTipoEfetivacaoPagamento());
			saida.setValorLimiteDiarioPagamento(response
					.getValorLimiteDiarioPagamento());
			saida.setQuantidadeDiaRepiqueConsulta(response
					.getQuantidadeDiaRepiqueConsulta());
			saida.setCdPagamentoNaoUtil(response.getCdPagamentoNaoUtil());
			saida.setDsPagamentoNaoUtil(response.getDsPagamentoNaoUtil());
			saida.setCdMomentoProcessamentoPagamento(response
					.getCdMomentoProcessamentoPagamento());
			saida.setDsMomentoProcessamentoPagamento(response
					.getDsMomentoProcessamentoPagamento());
			saida.setCdRejeicaoAgendamentoLote(response
					.getCdRejeicaoAgendamentoLote());
			saida.setDsRejeicaoAgendamentoLote(response
					.getDsRejeicaoAgendamentoLote());
			saida.setCdRejeicaoEfetivacaoLote(response
					.getCdRejeicaoEfetivacaoLote());
			saida.setDsRejeicaoEfetivacaoLote(response
					.getDsRejeicaoEfetivacaoLote());
			saida.setCdPrioridadeEfetivacaoPagamento(response
					.getCdPrioridadeEfetivacaoPagamento());
			saida.setDsPrioridadeEfetivacaoPagamento(response
					.getDsPrioridadeEfetivacaoPagamento());
			saida.setCdUtilizacaoFavorecidoControle(response
					.getCdUtilizacaoFavorecidoControle());
			saida.setDsUtilizacaoFavorecidoControle(response
					.getDsUtilizacaoFavorecidoControle());
			saida.setValorFavorecidoNaoCadastrado(response
					.getValorFavorecidoNaoCadastrado());
			saida.setPercentualMaximoInconsistenteLote(response
					.getPercentualMaximoInconsistenteLote());
			saida.setQuantidadeMaximaInconsistenteLote(response
					.getQuantidadeMaximaInconsistenteLote());
			saida.setCdContagemConsultaSaldo(response
					.getCdContagemConsultaSaldo());
			saida.setQuantidadeDiaFloatingPagamento(response
					.getQuantidadeDiaFloatingPagamento());
			saida.setDsOutraidentificacaoFavorecido(response
					.getDsOutraidentificacaoFavorecido());
			saida.setCdFavorecidoConsultaPagamento(response
					.getCdFavorecidoConsultaPagamento());
			saida.setDsContagemConsultaSaldo(response
					.getDsContagemConsultaSaldo());
			saida.setCdLancamentoFuturoDebito(response
					.getCdLancamentoFuturoDebito());
			saida.setCdTipoConsistenciaLista(response
					.getCdTipoConsistenciaLista());
			saida.setDsTipoConsistenciaLista(response
					.getDsTipoConsistenciaLista());
			saida.setCdPagamentoNaoUtil(response.getCdPagamentoNaoUtil());
			saida.setDsPagamentoNaoUtil(response.getDsPagamentoNaoUtil());
			saida.setCdIndicadorRetornoInternet(response
					.getCdIndicadorRetornoInternet());
			saida.setDsCctciaInscricaoFavorecido(response
					.getDsCctciaInscricaoFavorecido());
		}
		// D�bito de Ve�culos
		if (entrada.getCdParametroTela() == 32) {
			saida.setValorLimiteDiarioPagamento(response
					.getValorLimiteDiarioPagamento());
			saida.setValorLimiteIndividualPagamento(response
					.getValorLimiteIndividualPagamento());
			saida.setCdPagamentoNaoUtil(response.getCdPagamentoNaoUtil());
			saida.setDsPagamentoNaoUtil(response.getDsPagamentoNaoUtil());
			saida.setCdMomentoProcessamentoPagamento(response
					.getCdMomentoProcessamentoPagamento());
			saida.setDsMomentoProcessamentoPagamento(response
					.getDsMomentoProcessamentoPagamento());
			saida.setCdTipoDivergenciaVeiculo(response
					.getCdTipoDivergenciaVeiculo());
			saida.setDsTipoDivergenciaVeiculo(response
					.getDsTipoDivergenciaVeiculo());
			saida.setCdUtilizacaoFavorecidoControle(response
					.getCdUtilizacaoFavorecidoControle());
			saida.setDsUtilizacaoFavorecidoControle(response
					.getDsUtilizacaoFavorecidoControle());
			saida.setValorFavorecidoNaoCadastrado(response
					.getValorFavorecidoNaoCadastrado());
			saida.setCdCctciaProprietarioVeculo(response
					.getCdCctciaProprietarioVeculo());
			saida.setDsCctciaProprietarioVeculo(response
					.getDsCctciaProprietarioVeculo());
			saida.setCdConsultaSaldoPagamento(response
					.getCdConsultaSaldoPagamento());
			saida.setDsConsultaSaldoPagamento(response
					.getDsConsultaSaldoPagamento());
			saida.setQuantidadeDiaFloatingPagamento(response
					.getQuantidadeDiaFloatingPagamento());
			saida.setCdRejeicaoAgendamentoLote(response
					.getCdRejeicaoAgendamentoLote());
			saida.setDsRejeicaoAgendamentoLote(response
					.getDsRejeicaoAgendamentoLote());
			saida.setCdRejeicaoEfetivacaoLote(response
					.getCdRejeicaoEfetivacaoLote());
			saida.setDsRejeicaoEfetivacaoLote(response
					.getDsRejeicaoEfetivacaoLote());
			saida.setQuantidadeMaximaInconsistenteLote(response
					.getQuantidadeMaximaInconsistenteLote());
			saida.setPercentualMaximoInconsistenteLote(response
					.getPercentualMaximoInconsistenteLote());
			saida.setCdPrioridadeEfetivacaoPagamento(response
					.getCdPrioridadeEfetivacaoPagamento());
			saida.setDsPrioridadeEfetivacaoPagamento(response
					.getDsPrioridadeEfetivacaoPagamento());
			saida.setDsOutraidentificacaoFavorecido(response
					.getDsOutraidentificacaoFavorecido());
			saida.setCdUtilizacaoFavorecidoControle(response
					.getCdUtilizacaoFavorecidoControle());
			saida.setDsUtilizacaoFavorecidoControle(response
					.getDsUtilizacaoFavorecidoControle());
			saida.setQuantidadeDiaRepiqueConsulta(response
					.getQuantidadeDiaRepiqueConsulta());
			saida.setCdFavorecidoConsultaPagamento(response
					.getCdFavorecidoConsultaPagamento());
			saida.setDsFavorecidoConsultaPagamento(response
					.getDsFavorecidoConsultaPagamento());
			saida.setCdLancamentoFuturoDebito(response
					.getCdLancamentoFuturoDebito());
			saida.setCdTipoConsistenciaLista(response
					.getCdTipoConsistenciaLista());
			saida.setDsTipoConsistenciaLista(response
					.getDsTipoConsistenciaLista());
			saida.setCdIndicadorRetornoInternet(response
					.getCdIndicadorRetornoInternet());
			saida.setDsCctciaInscricaoFavorecido(response
					.getDsCctciaInscricaoFavorecido());
		}
		// **************************************** TRILHA DE AUDITORIA
		// *******************************************
		saida.setCdUsuarioInclusao(response.getCdUsuarioInclusao());
		saida.setCdUsuarioAlteracao(response.getCdUsuarioAlteracao());
		saida.setCdUsuarioExternoAlteracao(response
				.getCdUsuarioExternoAlteracao());
		saida.setCdUsuarioExternoInclusao(response
				.getCdUsuarioExternoInclusao());
		// Campos Condi��es de Reajuste
		saida.setPercentualIndiceReajusteTarifa(response
				.getPercentualIndiceReajusteTarifa());
		saida.setCdTipoReajusteTarifa(response.getCdTipoReajusteTarifa());
		saida.setDsTipoReajusteTarifa(response.getDsTipoReajusteTarifa());
		saida.setQuantidadeMesReajusteTarifa(response
				.getQuantidadeMesReajusteTarifa());
		saida.setCdIndicadorEconomicoReajuste(response
				.getCdIndicadorEconomicoReajuste());
		saida.setDsIndicadorEconomicoReajuste(response
				.getDsIndicadorEconomicoReajuste());
		saida.setPeriodicidadeTarifaCatalogo(response
				.getPeriodicidadeTarifaCatalogo());
		// Campos Condi��es de Conbran�a
		saida.setQuantidadeDiaCobrancaTarifa(response
				.getQuantidadeDiaCobrancaTarifa());
		saida.setNrFechamentoApuracaoTarifa(response
				.getNrFechamentoApuracaoTarifa());
		saida.setCdPeriodicidadeCobrancaTarifa(response
				.getCdPeriodicidadeCobrancaTarifa());
		saida.setDsPeriodicidadeCobrancaTarifa(response
				.getDsPeriodicidadeCobrancaTarifa());
		saida.setHrManutencaoRegistroInclusao(formatarDataTrilha(response
				.getHrManutencaoRegistroInclusao()));
		saida.setHrManutencaoRegistroAlteracao(formatarDataTrilha(response
				.getHrManutencaoRegistroAlteracao()));
		saida.setCdCanalInclusao(response.getCdCanalInclusao());
		saida.setCdCanalAlteracao(response.getCdCanalAlteracao());
		saida.setDsCanalInclusao(response.getDsCanalInclusao());
		saida.setDsCanalAlteracao(response.getDsCanalAlteracao());
		saida.setNrOperacaoFluxoInclusao(response.getNrOperacaoFluxoInclusao());
		saida.setNrOperacaoFluxoAlteracao(response
				.getNrOperacaoFluxoAlteracao());
		saida.setCdAmbienteServicoContrato(response
				.getCdAmbienteServicoContrato());
		saida.setDsSituacaoServicoRelacionado(response
				.getDsSituacaoServicoRelacionado());
		saida.setCdConsultaSaldoValorSuperior(response
				.getCdConsultaSaldoValorSuperior());
		saida.setDsConsultaSaldoValorSuperior(response
				.getDsConsultaSaldoValorSuperior());
		saida.setCdIndicadorFeriadoLocal(response.getCdIndicadorFeriadoLocal());
		saida.setDsCodigoIndFeriadoLocal(response.getDsCodigoIndFeriadoLocal());
		saida.setCdIndicadorSegundaLinha(response.getCdIndicadorSegundaLinha());
		saida.setDsIndicadorSegundaLinha(response.getDsIndicadorSegundaLinha());
		saida.setCdFloatServicoContrato(response.getCdFloatServicoContrato());
		saida.setDsFloatServicoContrato(response.getDsFloatServicoContrato());
		
		saida.setCdTituloDdaRetorno(response.getCdTituloDdaRetorno());
        saida.setDsTituloDdaRetorno(response.getDsTituloDdaRetorno());
        saida.setCdIndicadorAgendaGrade(response.getCdIndicadorAgendaGrade());
        saida.setDsIndicadorAgendaGrade(response.getDsIndicadorAgendaGrade());
        saida.setCdPreenchimentoLancamentoPersonalizado(response.getCdPreenchimentoLancamentoPersonalizado());
        saida.setDsPreenchimentoLancamentoPersonalizado(response.getDsPreenchimentoLancamentoPersonalizado());
		return saida;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.service.business.mantercontrato.
	 *      IManterContratoService#consultarConManTarifas(br.com.bradesco.web.pgit.service.business.
	 *      mantercontrato.bean.ConsultarConManTarifasEntradaDTO)
	 */
	public ConsultarConManTarifasSaidaDTO consultarConManTarifas(
			ConsultarConManTarifasEntradaDTO entrada) {

		ConsultarConManTarifasRequest request = new ConsultarConManTarifasRequest();
		request.setCdOperacaoProdutoServico(entrada
				.getCdOperacaoProdutoServico());
		request.setCdPessoaJuridicaContrato(entrada
				.getCdPessoaJuridicaContrato());
		request.setCdProdutoServicoOperacao(entrada
				.getCdProdutoServicoOperacao());
		request.setCdProdutoServicoRelacionado(entrada
				.getCdProdutoServicoRelacionado());
		request.setCdTipoContratoNegocio(entrada.getCdTipoContratoNegocio());
		request.setDtInicioVigenciaTarifa(entrada.getDtInicioVigenciaTarifa());
		request.setHrInclusaoRegistroHistorico(entrada
				.getHrInclusaoRegistroHistorico());
		request.setNrSequenciaContratoNegocio(entrada
				.getNrSequenciaContratoNegocio());

		ConsultarConManTarifasResponse response = getFactoryAdapter()
				.getConsultarConManTarifasPDCAdapter().invokeProcess(request);

		ConsultarConManTarifasSaidaDTO saida = new ConsultarConManTarifasSaidaDTO();
		saida.setCodMensagem(response.getCodMensagem());
		saida.setMensagem(response.getMensagem());
		saida.setCdProdutoServicoOperacao(response
				.getCdProdutoServicoOperacao());
		saida.setCdProdutoServicoRelacionado(response
				.getCdProdutoServicoRelacionado());
		saida.setCdOperacaoProdutoServico(response
				.getCdOperacaoProdutoServico());
		saida.setHrInclusaoRegistroHistorico(response
				.getHrInclusaoRegistroHistorico());
		saida.setDtInicioVigenciaTarifa(response.getDtInicioVigenciaTarifa());
		saida.setDtFimVigenciaTarifa(response.getDtFimVigenciaTarifa());
		saida.setVlTarifaContrato(response.getVlTarifaContrato());
		saida.setVlTarifaReferenciaMinima(response
				.getVlTarifaReferenciaMinima());
		saida.setVlTarifaReferenciaMaxima(response
				.getVlTarifaReferenciaMaxima());
		saida.setDsProdutoServicoOperacao(response
				.getDsProdutoServicoOperacao());
		saida.setDsProdutoServicoRelacionado(response
				.getDsProdutoServicoRelacionado());
		saida.setDsOperacaoProdutoServico(response
				.getDsOperacaoProdutoServico());
		saida.setCdIndicadorTipoManutencao(response
				.getCdIndicadorTipoManutencao());
		saida.setDsIndicadorTipoManutencao(response
				.getDsIndicadorTipoManutencao());
		saida.setHrInclusaoRegistro(FormatarData.formatarDataTrilha(response
				.getHrInclusaoRegistro()));
		saida.setCdUsuarioInclusao(response.getCdUsuarioInclusao());
		saida.setCdUsuarioInclusaoExterno(response
				.getCdUsuarioInclusaoExterno());
		saida.setCdCanalInclusao(response.getCdCanalInclusao());
		saida.setNrOperacaoFluxoInclusao(response.getNrOperacaoFluxoInclusao());
		saida.setHrManutencaoResgistro(FormatarData.formatarDataTrilha(response
				.getHrManutencaoRegistro()));
		saida.setCdUsuarioManutecao(response.getCdUsuarioManutencao());
		saida.setCdUsuarioManutencaoExterno(response
				.getCdUsuarioManutencaoExterno());
		saida.setCdCanalManutencao(response.getCdCanalManutencao());
		saida.setNrOperacaoFluxoManuntencao(response
				.getNrOperacaoFluxoManutencao());
		saida.setDsCanalInclusao(response.getDsCanalInclusao());
		saida.setDsCanalManutencao(response.getDsCanalManutencao());
		return saida;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.service.business.mantercontrato.
	 *      IManterContratoService#gerarManterContrato(br.com.bradesco.web.pgit.service.business.imprimircontrato.
	 *      bean.ImprimirContratoSaidaDTO, java.io.OutputStream)
	 */
	public void gerarManterContrato(
			ImprimirContratoSaidaDTO saidaImprimirContrato,
			OutputStream outputStream) throws DocumentException {
		new ManterContratoReport(saidaImprimirContrato).gerarPdf(outputStream);
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.service.business.mantercontrato.
	 *      IManterContratoService#detalharContaRelacionadaHist(br.com.bradesco.web.pgit.service.business.
	 *      mantercontrato.bean.DetalharContaRelacionadaHistEntradaDTO)
	 */
	public DetalharContaRelacionadaHistSaidaDTO detalharContaRelacionadaHist(
			DetalharContaRelacionadaHistEntradaDTO entradaDTO) {
		DetalharContaRelacionadaHistRequest request = new DetalharContaRelacionadaHistRequest();

		request.setHrInclusaoRegistro(verificaStringNula(entradaDTO
				.getHrInclusaoRegistro()));
		request.setCdPessoaJuridicaNegocio(verificaLongNulo(entradaDTO
				.getCdPessoaJuridicaNegocio()));
		request.setCdTipoContratoNegocio(verificaIntegerNulo(entradaDTO
				.getCdTipoContratoNegocio()));
		request.setNrSequenciaContratoNegocio(verificaLongNulo(entradaDTO
				.getNrSequenciaContratoNegocio()));
		request.setCdPessoVinc(verificaLongNulo(entradaDTO.getCdPessoVinc()));
		request.setCdTipoContratoVinc(verificaIntegerNulo(entradaDTO
				.getCdTipoContratoVinc()));
		request.setNrSeqContratoVinc(verificaLongNulo(entradaDTO
				.getNrSeqContratoVinc()));
		request.setCdTipoVincContrato(verificaIntegerNulo(entradaDTO
				.getCdTipoVincContrato()));
		request.setCdTipoRelacionamentoConta(verificaIntegerNulo(entradaDTO
				.getCdTipoRelacionamentoConta()));
		request.setCdBancoRelacionamento(verificaIntegerNulo(entradaDTO
				.getCdBancoRelacionamento()));
		request.setCdAgenciaRelacionamento(verificaIntegerNulo(entradaDTO
				.getCdAgenciaRelacionamento()));
		request.setCdContaRelacionamento(verificaLongNulo(entradaDTO
				.getCdContaRelacionamento()));
		request.setDigitoContaRelacionamento(verificaStringNula(entradaDTO
				.getDigitoContaRelacionamento()));
		request
				.setCdTipoConta(verificaIntegerNulo(entradaDTO.getCdTipoConta()));
		request.setCdCorpoCpfCnpjParticipante(verificaLongNulo(entradaDTO
				.getCdCorpoCpfCnpjParticipante()));
		request.setCdFilialCpfCnpjParticipante(verificaIntegerNulo(entradaDTO
				.getCdFilialCpfCnpjParticipante()));
		request.setCdDigitoCpfCnpjParticipante(verificaIntegerNulo(entradaDTO
				.getCdDigitoCpfCnpjParticipante()));
		request.setCdPssoa(verificaLongNulo(entradaDTO.getCdPssoa()));

		DetalharContaRelacionadaHistResponse response = getFactoryAdapter()
				.getDetalharContaRelacionadaHistPDCAdapter().invokeProcess(
						request);

		DetalharContaRelacionadaHistSaidaDTO saidaDTO = new DetalharContaRelacionadaHistSaidaDTO();
		saidaDTO.setCodMensagem(response.getCodMensagem());
		saidaDTO.setMensagem(response.getMensagem());
		saidaDTO.setCdCorpoCpfCnpjParticipante(response
				.getCdCorpoCpfCnpjParticipante());
		saidaDTO.setCdFilialCpfCnpjParticipante(response
				.getCdFilialCpfCnpjParticipante());
		saidaDTO.setCdDigitoCpfCnpjParticipante(response
				.getCdDigitoCpfCnpjParticipante());
		saidaDTO.setCdBancoRelacionamento(response.getCdBancoRelacionamento());
		saidaDTO.setCdAgenciaRelacionamento(response
				.getCdAgenciaRelacionamento());
		saidaDTO.setCdContaRelacionamento(response.getCdContaRelacionamento());
		saidaDTO.setCdDigitoContaRelacionamento(response
				.getCdDigitoContaRelacionamento());
		saidaDTO.setCdTipoContaRelacionamento(response
				.getCdTipoContaRelacionamento());
		saidaDTO.setDsTipoContaRelacionamento(response
				.getDsTipoContaRelacionamento());
		saidaDTO.setCdIndicadorTipoManutencao(response
				.getCdIndicadorTipoManutencao());
		saidaDTO.setDsTipoManutencao(response.getDsTipoManutencao());
		saidaDTO.setCdUsuarioInclusao(response.getCdUsuarioInclusao());
		saidaDTO.setCdUsuarioInclusaoExterno(response
				.getCdUsuarioInclusaoExterno());
		saidaDTO.setHrInclusaoRegistro(response.getHrInclusaoRegistro());
		saidaDTO.setCdOperacaoCanalInclusao(response
				.getCdOperacaoCanalInclusao());
		saidaDTO.setCdTipoCanalInclusao(response.getCdTipoCanalInclusao());
		saidaDTO.setDsCanalInclusao(response.getDsCanalInclusao());
		saidaDTO.setCdUsuarioManutencao(response.getCdUsuarioManutencao());
		saidaDTO.setCdUsuarioManutencaoExterno(response
				.getCdUsuarioManutencaoExterno());
		saidaDTO.setHrManutencaoRegistro(response.getHrManutencaoRegistro());
		saidaDTO.setCdOperacaoCanalManutencao(response
				.getCdOperacaoCanalManutencao());
		saidaDTO.setCdTipoCanalManutencao(response.getCdTipoCanalManutencao());
		saidaDTO.setDsCanalManutencao(response.getDsCanalManutencao());

		saidaDTO.setCpfCnpjParticipanteFormatado(formatCpfCnpjCompleto(response
				.getCdCorpoCpfCnpjParticipante(), response
				.getCdFilialCpfCnpjParticipante(), response
				.getCdDigitoCpfCnpjParticipante()));
		saidaDTO.setUsuarioInclusaoFormatado(verificaUsuarioInternoExterno(
				saidaDTO.getCdUsuarioInclusao(), saidaDTO
						.getCdUsuarioInclusaoExterno()));
		saidaDTO.setTipoCanalInclusaoFormatado(concatenarCampos(saidaDTO
				.getCdTipoCanalInclusao(), saidaDTO.getDsCanalInclusao()));
		saidaDTO.setUsuarioManutencaoFormatado(verificaUsuarioInternoExterno(
				saidaDTO.getCdUsuarioManutencao(), saidaDTO
						.getCdUsuarioManutencaoExterno()));
		saidaDTO.setTipoCanalManutencaoFormatado(concatenarCampos(saidaDTO
				.getCdTipoCanalManutencao(), saidaDTO.getDsCanalManutencao()));

		return saidaDTO;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.service.business.mantercontrato.
	 *      IManterContratoService#detalharHistoricoLayoutContrato(br.com.bradesco.web.pgit.service.business.
	 *      mantercontrato.bean.DetalharHistoricoLayoutContratoEntradaDTO)
	 */
	public DetalharHistoricoLayoutContratoSaidaDTO detalharHistoricoLayoutContrato(
			DetalharHistoricoLayoutContratoEntradaDTO entradaDTO) {

		DetalharHistoricoLayoutContratoRequest request = new DetalharHistoricoLayoutContratoRequest();
		request.setHrInclusaoRegistro(verificaStringNula(entradaDTO
				.getHrInclusaoRegistro()));
		request.setCdPessoaJuridicaNegocio(verificaLongNulo(entradaDTO
				.getCdPessoaJuridicaNegocio()));
		request.setCdTipoContratoNegocio(verificaIntegerNulo(entradaDTO
				.getCdTipoContratoNegocio()));
		request.setNrSequenciaContratoNegocio(verificaLongNulo(entradaDTO
				.getNrSequenciaContratoNegocio()));
		request.setCdTipoLayoutArquivo(verificaIntegerNulo(entradaDTO
				.getCdTipoLayoutArquivo()));
		request.setCdArquivoRetornoPagamento(verificaIntegerNulo(entradaDTO
				.getCdArquivoRetornoPagamento()));

		DetalharHistoricoLayoutContratoResponse response = getFactoryAdapter()
				.getDetalharHistoricoLayoutContratoPDCAdapter().invokeProcess(
						request);

		DetalharHistoricoLayoutContratoSaidaDTO saidaDTO = new DetalharHistoricoLayoutContratoSaidaDTO();
		saidaDTO.setCodMensagem(response.getCodMensagem());
		saidaDTO.setMensagem(response.getMensagem());
		saidaDTO.setCdTipoLayoutArquivo(response.getCdTipoLayoutArquivo());
		saidaDTO.setDsTipoLayoutArquivo(response.getDsTipoLayoutArquivo());
		saidaDTO.setCdArquivoRetornoPagamento(response
				.getCdArquivoRetornoPagamento());
		saidaDTO.setDsArquivoRetornoPagamento(response
				.getDsArquivoRetornoPagamento());
		saidaDTO.setCdTipoGeracaoRetorno(response.getCdTipoGeracaoRetorno());
		saidaDTO.setDsTipoGeracaoRetorno(response.getDsTipoGeracaoRetorno());
		saidaDTO.setCdTipoMontaRetorno(response.getCdTipoMontaRetorno());
		saidaDTO.setDsTipoMontaRetorno(response.getDsTipoMontaRetorno());
		saidaDTO.setCdTipoOcorRetorno(response.getCdTipoOcorRetorno());
		saidaDTO.setDsTipoOcorRetorno(response.getDsTipoOcorRetorno());
		saidaDTO.setCdTipoOrdemRegistroRetorno(response
				.getCdTipoOrdemRegistroRetorno());
		saidaDTO.setDsTipoOrdemRegistroRetorno(response
				.getDsTipoOrdemRegistroRetorno());
		saidaDTO.setCdTipoClassificacaoRetorno(response
				.getCdTipoClassificacaoRetorno());
		saidaDTO.setDsTipoClassificacaoRetorno(response
				.getDsTipoClassificacaoRetorno());
		saidaDTO.setCdIndicadorTipoManutencao(response
				.getCdIndicadorTipoManutencao());
		saidaDTO.setDsTipoManutencao(response.getDsTipoManutencao());
		saidaDTO.setCdUsuarioInclusao(response.getCdUsuarioInclusao());
		saidaDTO.setCdUsuarioInclusaoExterno(response
				.getCdUsuarioInclusaoExterno());
		saidaDTO.setHrInclusaoRegistro(response.getHrInclusaoRegistro());
		saidaDTO.setCdOperacaoCanalInclusao(response
				.getCdOperacaoCanalInclusao());
		saidaDTO.setCdTipoCanalInclusao(response.getCdTipoCanalInclusao());
		saidaDTO.setDsCanalInclusao(response.getDsCanalInclusao());
		saidaDTO.setUsuarioInclusaoFormatado(verificaUsuarioInternoExterno(
				saidaDTO.getCdUsuarioInclusao(), saidaDTO
						.getCdUsuarioInclusaoExterno()));
		saidaDTO.setTipoCanalInclusaoFormatado(concatenarCampos(saidaDTO
				.getCdTipoCanalInclusao(), saidaDTO.getDsCanalInclusao()));
		saidaDTO.setCdUsuarioManutencao(response.getCdUsuarioManutencao());
		saidaDTO.setCdUsuarioManutencaoExterno(response
				.getCdUsuarioManutencaoExterno());
		saidaDTO.setHrManutencaoRegistro(response.getHrManutencaoRegistro());
		saidaDTO.setCdOperacaoCanalManutencao(response
				.getCdOperacaoCanalManutencao());
		saidaDTO.setCdTipoCanalManutencao(response.getCdTipoCanalManutencao());
		saidaDTO.setDsCanalManutencao(response.getDsCanalManutencao());
		saidaDTO.setUsuarioManutencaoFormatado(verificaUsuarioInternoExterno(
				saidaDTO.getCdUsuarioManutencao(), saidaDTO
						.getCdUsuarioManutencaoExterno()));
		saidaDTO.setTipoCanalManutencaoFormatado(concatenarCampos(saidaDTO
				.getCdTipoCanalManutencao(), saidaDTO.getDsCanalManutencao()));

		return saidaDTO;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.service.business.mantercontrato.
	 *      IManterContratoService#listarHistoricoLayoutContrato(br.com.bradesco.web.pgit.service.business.
	 *      mantercontrato.bean.ListarHistoricoLayoutContratoEntradaDTO)
	 */
	public List<ListarHistoricoLayoutContratoSaidaDTO> listarHistoricoLayoutContrato(
			ListarHistoricoLayoutContratoEntradaDTO entradaDTO) {
		ListarHistoricoLayoutContratoRequest request = new ListarHistoricoLayoutContratoRequest();
		request.setCdPessoaJuridicaNegocio(verificaLongNulo(entradaDTO
				.getCdPessoaJuridicaNegocio()));
		request.setCdTipoContratoNegocio(verificaIntegerNulo(entradaDTO
				.getCdTipoContratoNegocio()));
		request.setNrSequenciaContratoNegocio(verificaLongNulo(entradaDTO
				.getNrSequenciaContratoNegocio()));
		request.setCdTipoLayoutArquivo(verificaIntegerNulo(entradaDTO
				.getCdTipoLayoutArquivo()));
		request.setCdArquivoRetornoPagamento(verificaIntegerNulo(entradaDTO
				.getCdArquivoRetornoPagamento()));
		request.setDtInicioManutencao(verificaStringNula(entradaDTO
				.getDtInicioManutencao()));
		request.setDtFimManutencao(verificaStringNula(entradaDTO
				.getDtFimManutencao()));
		request
				.setNrOcorrencias(IManterContratoServiceConstants.OCORRENCIAS_LISTAR_HISTORICO_LAYOYT);

		ListarHistoricoLayoutContratoResponse response = getFactoryAdapter()
				.getListarHistoricoLayoutContratoPDCAdapter().invokeProcess(
						request);

		List<ListarHistoricoLayoutContratoSaidaDTO> listaSaida = new ArrayList<ListarHistoricoLayoutContratoSaidaDTO>();
		ListarHistoricoLayoutContratoSaidaDTO saida;

		for (int i = 0; i < response.getOcorrenciasCount(); i++) {
			saida = new ListarHistoricoLayoutContratoSaidaDTO();
			saida.setCodMensagem(response.getCodMensagem());
			saida.setMensagem(response.getMensagem());
			saida.setCdArquivoRetornoPagamento(response.getOcorrencias(i)
					.getCdArquivoRetornoPagamento());
			saida.setDsArquivoRetornoPagamento(response.getOcorrencias(i)
					.getDsArquivoRetornoPagamento());
			saida.setHrInclusaoManutencao(response.getOcorrencias(i)
					.getHrInclusaoManutencao());
			saida.setCdUsuarioManutencao(response.getOcorrencias(i)
					.getCdUsuarioManutencao());
			saida.setDtManutencao(response.getOcorrencias(i).getDtManutencao());
			saida.setHrManutencao(response.getOcorrencias(i).getHrManutencao());
			saida.setHrInclusaoManutencaoFormatada(formatarDataTrilha(saida
					.getHrInclusaoManutencao()));
			saida.setDsTipoManutencao(response.getOcorrencias(i)
					.getDsTipoManutencao());
			listaSaida.add(saida);
		}

		return listaSaida;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.service.business.mantercontrato.
	 *      IManterContratoService#bloquearDesbloquearContratoPgit(br.com.bradesco.web.
	 *      pgit.service.business.mantercontrato.bean.BloquearDesbloquearContratoPgitEntradaDTO)
	 */
	public BloquearDesbloquearContratoPgitSaidaDTO bloquearDesbloquearContratoPgit(
			BloquearDesbloquearContratoPgitEntradaDTO entradaDTO) {

		BloquearDesbloquearContratoPgitRequest request = new BloquearDesbloquearContratoPgitRequest();
		request.setCdMotivoSituacaoParticipante(verificaIntegerNulo(entradaDTO
				.getCdMotivoSituacaoParticipante()));
		request.setCdPessoa(verificaLongNulo(entradaDTO.getCdPessoa()));
		request.setCdpessoaJuridicaContrato(verificaLongNulo(entradaDTO
				.getCdpessoaJuridicaContrato()));
		request
				.setCdSituacaoParticipanteContrato(verificaIntegerNulo(entradaDTO
						.getCdSituacaoParticipanteContrato()));
		request.setCdTipoContratoNegocio(verificaIntegerNulo(entradaDTO
				.getCdTipoContratoNegocio()));
		request.setCdTipoParticipacaoPessoa(verificaIntegerNulo(entradaDTO
				.getCdTipoParticipacaoPessoa()));
		request.setNrSequenciaContratoNegocio(verificaLongNulo(entradaDTO
				.getNrSequenciaContratoNegocio()));

		BloquearDesbloquearContratoPgitResponse response = getFactoryAdapter()
				.getBloquearDesbloquearContratoPgitPDCAdapter().invokeProcess(
						request);

		BloquearDesbloquearContratoPgitSaidaDTO saidaDTO = new BloquearDesbloquearContratoPgitSaidaDTO();
		saidaDTO.setCodMensagem(response.getCodMensagem());
		saidaDTO.setMensagem(response.getMensagem());

		return saidaDTO;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.service.business.mantercontrato.
	 *      IManterContratoService#listarParticipantesContrato(br.com.bradesco.web.pgit.
	 *      service.business.mantercontrato.bean.ListarParticipantesContratoEntradaDTO)
	 */
	public List<ListarParticipantesContratoSaidaDTO> listarParticipantesContrato(
			ListarParticipantesContratoEntradaDTO entrada) {
		ListarParticipantesContratoRequest request = new ListarParticipantesContratoRequest();

		request
				.setMaxOcorrencias(IManterContratoServiceConstants.OCORRENCIAS_LISTAR_PARTICIPANTES_CONTRATO);
		request.setCdpessoaJuridicaContrato(verificaLongNulo(entrada
				.getCdpessoaJuridicaContrato()));
		request.setCdTipoContratoNegocio(verificaIntegerNulo(entrada
				.getCdTipoContratoNegocio()));
		request.setNrSequenciaContratoNegocio(verificaLongNulo(entrada
				.getNrSequenciaContratoNegocio()));
		request.setCdClub(verificaLongNulo(entrada.getCdClub()));

		ListarParticipantesContratoResponse response = getFactoryAdapter()
				.getListarParticipantesContratoPDCAdapter().invokeProcess(
						request);

		List<ListarParticipantesContratoSaidaDTO> listaSaida = new ArrayList<ListarParticipantesContratoSaidaDTO>();
		for (int i = 0; i < response.getOcorrenciasCount(); i++) {
			ListarParticipantesContratoSaidaDTO saida = new ListarParticipantesContratoSaidaDTO();
			saida.setCodMensagem(response.getCodMensagem());
			saida.setMensagem(response.getMensagem());
			saida.setNumeroLinhas(response.getNumeroLinhas());
			saida.setCdpessoaJuridicaContrato(response.getOcorrencias(i)
					.getCdpessoaJuridicaContrato());
			saida.setCdTipoContratoNegocio(response.getOcorrencias(i)
					.getCdTipoContratoNegocio());
			saida.setNrSequenciaContratoNegocio(response.getOcorrencias(i)
					.getNrSequenciaContratoNegocio());
			saida.setCdClub(response.getOcorrencias(i).getCdClub());
			saida.setCdCorpoCpfCnpj(response.getOcorrencias(i)
					.getCdCorpoCpfCnpj());
			saida.setCdCnpjContrato(response.getOcorrencias(i)
					.getCdCnpjContrato());
			saida.setCdCpfCnpjDigito(response.getOcorrencias(i)
					.getCdCpfCnpjDigito());
			saida.setCdRazaoSocial(response.getOcorrencias(i)
					.getCdRazaoSocial());
			saida.setCdTipoParticipacaoPessoa(response.getOcorrencias(i)
					.getCdTipoParticipacaoPessoa());
			saida.setDsTipoParticipante(response.getOcorrencias(i)
					.getDsTipoParticipante());
			saida.setCdDescricaoSituacaoParticapante(response.getOcorrencias(i)
					.getCdDescricaoSituacaoParticapante());
			saida.setCpfCnpjFormatado(formatCpfCnpjCompleto(saida
					.getCdCorpoCpfCnpj(), saida.getCdCnpjContrato(), saida
					.getCdCpfCnpjDigito()));
			listaSaida.add(saida);
		}
		return listaSaida;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.service.business.mantercontrato.
	 *      IManterContratoService#alterarRepresentanteContratoPgit(br.com.bradesco.web.pgit.service.business.
	 *      mantercontrato.bean.AlterarRepresentanteContratoPgitEntradaDTO)
	 */
	public AlterarRepresentanteContratoPgitSaidaDTO alterarRepresentanteContratoPgit(
			AlterarRepresentanteContratoPgitEntradaDTO entrada) {
		AlterarRepresentanteContratoPgitRequest request = new AlterarRepresentanteContratoPgitRequest();
		request.setCdpessoaJuridicaContrato(verificaLongNulo(entrada
				.getCdpessoaJuridicaContrato()));
		request.setCdTipoContratoNegocio(verificaIntegerNulo(entrada
				.getCdTipoContratoNegocio()));
		request.setNrSequenciaContratoNegocio(verificaLongNulo(entrada
				.getNrSequenciaContratoNegocio()));
		request
				.setCdTipoParticipacaoPessoa(IManterContratoServiceConstants.TIPO_PARTICIPACAO_PESSOA);
		request.setCdPessoa(verificaLongNulo(entrada.getCdPessoa()));

		AlterarRepresentanteContratoPgitResponse response = getFactoryAdapter()
				.getAlterarRepresentanteContratoPgitPDCAdapter().invokeProcess(
						request);

		AlterarRepresentanteContratoPgitSaidaDTO saida = new AlterarRepresentanteContratoPgitSaidaDTO();
		saida.setCodMensagem(response.getCodMensagem());
		saida.setMensagem(response.getMensagem());

		return saida;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.service.business.mantercontrato.
	 *      IManterContratoService#excluirRelaciAssoc(br.com.bradesco.web.pgit.service.
	 *      business.mantercontrato.bean.ExcluirRelaciAssocEntradaDTO)
	 */
	public ExcluirRelaciAssocSaidaDTO excluirRelaciAssoc(
			ExcluirRelaciAssocEntradaDTO entrada) {
		ExcluirRelaciAssocRequest request = new ExcluirRelaciAssocRequest();

		request.setCdPessoaJuridicaContrato(verificaLongNulo(entrada
				.getCdPessoaJuridicaContrato()));
		request.setCdTipoContratoNegocio(verificaIntegerNulo(entrada
				.getCdTipoContratoNegocio()));
		request.setNrSequenciaContratoNegocio(verificaLongNulo(entrada
				.getNrSequenciaContratoNegocio()));
		request.setCdPessoaJuridicaVinculo(verificaLongNulo(entrada
				.getCdPessoaJuridicaVinculo()));
		request.setCdTipoContratoVinculo(verificaIntegerNulo(entrada
				.getCdTipoContratoVinculo()));
		request.setNrSequenciaContratoVinculo(verificaLongNulo(entrada
				.getNrSequenciaContratoVinculo()));
		request.setCdTipoVincContrato(verificaIntegerNulo(entrada
				.getCdTipoVincContrato()));
		request.setCdTipoRelacionamentoConta(verificaIntegerNulo(entrada
				.getCdTipoRelacionamentoConta()));
		request.setCdTipoTesteAuxiliar(verificaStringNula(entrada
				.getCdTipoTesteAuxiliar()));
		request.setCdPssoa(verificaLongNulo(entrada.getCdPssoa()));

		ExcluirRelaciAssocResponse response = getFactoryAdapter()
				.getExcluirRelaciAssocPDCAdapter().invokeProcess(request);

		ExcluirRelaciAssocSaidaDTO saida = new ExcluirRelaciAssocSaidaDTO();
		saida.setCodMensagem(response.getCodMensagem());
		saida.setMensagem(response.getMensagem());

		return saida;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @seebr.com.bradesco.web.pgit.service.business.mantercontrato.
	 * IManterContratoService
	 * #alterarParticipanteContratoPgit(br.com.bradesco.web
	 * .pgit.service.business
	 * .mantercontrato.bean.AlterarParticipanteContratoPgitEntradaDTO)
	 */
	public AlterarParticipanteContratoPgitSaidaDTO alterarParticipanteContratoPgit(
			AlterarParticipanteContratoPgitEntradaDTO entradaDTO) {
		AlterarParticipanteContratoPgitRequest request = new AlterarParticipanteContratoPgitRequest();
		request.setCdPessoaJuridica(verificaLongNulo(entradaDTO
				.getCdPessoaJuridica()));
		request.setCdTipoContrato(verificaIntegerNulo(entradaDTO
				.getCdTipoContrato()));
		request.setNrSequenciaContrato(verificaLongNulo(entradaDTO
				.getNrSequenciaContrato()));
		request.setCdTipoParticipante(verificaIntegerNulo(entradaDTO
				.getCdTipoParticipante()));
		request.setCdPessoaParticipacao(verificaLongNulo(entradaDTO
				.getCdPessoaParticipacao()));
		request
				.setCdclassificacaoAreaParticipante(verificaIntegerNulo(entradaDTO
						.getCdclassificacaoAreaParticipante()));

		AlterarParticipanteContratoPgitResponse response = getFactoryAdapter()
				.getAlterarParticipanteContratoPgitPDCAdapter().invokeProcess(
						request);

		return new AlterarParticipanteContratoPgitSaidaDTO(response
				.getCodMensagem(), response.getMensagem());
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.service.business.mantercontrato.
	 *      IManterContratoService#verificarAtributoServicoModalidade(br.com.bradesco.web.pgit.service.business.
	 *      mantercontrato.bean.VerificarAtributosServicoModalidadeEntradaDTO)
	 */
	public VerificarAtributosServicoModalidadeSaidaDTO verificarAtributoServicoModalidade(
			VerificarAtributosServicoModalidadeEntradaDTO entrada) {
		VerificarAtributosServicoModalidadeRequest request = new VerificarAtributosServicoModalidadeRequest();

		request.setCdpessoaJuridicaContrato(entrada
				.getCdpessoaJuridicaContrato());
		request.setCdTipoContratoNegocio(entrada.getCdTipoContratoNegocio());
		request.setNrSequenciaContratoNegocio(entrada
				.getNrSequenciaContratoNegocio());
		request.setCdProdutoServicoOperacao(entrada
				.getCdProdutoServicoOperacao());
		request.setCdProdutoOperacaoRelacionado(entrada
				.getCdProdutoOperacaoRelacionado());
		request.setCdParametro(entrada.getCdParametro());

		VerificarAtributosServicoModalidadeResponse response = getFactoryAdapter()
				.getVerificarAtributosServicoModalidadePDCAdapter()
				.invokeProcess(request);
		return new VerificarAtributosServicoModalidadeSaidaDTO(response);
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.service.business.mantercontrato.
	 *      IManterContratoService#listarTipoServicosRelacionadosIncluir(br.com.bradesco.web.pgit.service.
	 *      business.combo.bean.ListarServModEntradaDTO)
	 */
	public List<ListarServicosSaidaDTO> listarTipoServicosRelacionadosIncluir(
			ListarServModEntradaDTO entrada) {
		ListarServicoRelacionadoCdpsIncluirRequest request = new ListarServicoRelacionadoCdpsIncluirRequest();
		request.setCdpessoaJuridicaContrato(verificaLongNulo(entrada
				.getCdPessoaJuridicaContrato()));
		request.setCdTipoContratoNegocio(verificaIntegerNulo(entrada
				.getCdTipoContrato()));
		request.setNrSequenciaContratoNegocio(verificaLongNulo(entrada
				.getNumeroContrato()));
		request.setCdProdutoOperacaoRelacionado(verificaIntegerNulo(entrada
				.getCdTipoServico()));
		request.setNrOcorrencias(25);

		ListarServicoRelacionadoCdpsIncluirResponse response = getFactoryAdapter()
				.getListarServicoRelacionadoCdpsIncluirPDCAdapter()
				.invokeProcess(request);

		List<ListarServicosSaidaDTO> lista = new ArrayList<ListarServicosSaidaDTO>();

        for (br.com.bradesco.web.pgit.service.data.pdc.listarservicorelacionadocdpsincluir.response.Ocorrencias saida :
            response.getOcorrencias()) {
			lista
					.add(new ListarServicosSaidaDTO(saida
							.getCdProdutoOperacaoRelacionado(), saida
							.getCdprodutoServicoRelacionado(), saida
							.getCdProdutoServicoOperacao(), saida
							.getDsProdutoServicoOperacao(), saida
							.getCdProdutoServicoOperacao(), saida
							.getCdParametroTela()));
		}
		return lista;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.service.business.mantercontrato.
	 *      IManterContratoService#listarTipoModalidadesRelacionadasIncluir(br.com.bradesco.web.pgit.service.
	 *      business.combo.bean.ListarServModEntradaDTO)
	 */
	public List<ListarServicoRelacionadoCdpsSaidaDTO> listarTipoModalidadesRelacionadasIncluir(
			ListarServModEntradaDTO entrada) {
		ListarServicoRelacionadoCdpsIncluirRequest request = new ListarServicoRelacionadoCdpsIncluirRequest();

		request.setCdpessoaJuridicaContrato(verificaLongNulo(entrada
				.getCdPessoaJuridicaContrato()));
		request.setCdTipoContratoNegocio(verificaIntegerNulo(entrada
				.getCdTipoContrato()));
		request.setNrSequenciaContratoNegocio(verificaLongNulo(entrada
				.getNumeroContrato()));
		request.setCdProdutoOperacaoRelacionado(verificaIntegerNulo(entrada
				.getCdTipoServico()));
		request.setNrOcorrencias(25);

		ListarServicoRelacionadoCdpsIncluirResponse response = getFactoryAdapter()
				.getListarServicoRelacionadoCdpsIncluirPDCAdapter()
				.invokeProcess(request);

		List<ListarServicoRelacionadoCdpsSaidaDTO> lista = new ArrayList<ListarServicoRelacionadoCdpsSaidaDTO>();

		for (br.com.bradesco.web.pgit.service.data.pdc.listarservicorelacionadocdpsincluir.response.Ocorrencias saida : 
		    response.getOcorrencias()) {
			lista.add(new ListarServicoRelacionadoCdpsSaidaDTO(saida
					.getCdProdutoServicoOperacao(), saida
					.getCdprodutoServicoRelacionado(), saida
					.getCdProdutoOperacaoRelacionado()));
		}

		return lista;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.service.business.mantercontrato.
	 *      IManterContratoService#listarParticipanteAgregado(br.com.bradesco.web.pgit.service.
	 *      business.mantercontrato.bean.ListarParticipanteAgregadoEntradaDTO)
	 */
	public List<ListarParticipanteAgregadoSaidaDTO> listarParticipanteAgregado(
			ListarParticipanteAgregadoEntradaDTO entrada) {
		ListarParticipanteAgregadoRequest request = new ListarParticipanteAgregadoRequest();

		request.setCdPessoaJuridica(verificaLongNulo(entrada
				.getCdPessoaJuridica()));
		request.setCdTipoContrato(verificaIntegerNulo(entrada
				.getCdTipoContrato()));
		request.setNrSequenciaContrato(verificaLongNulo(entrada
				.getNrSequenciaContrato()));
		request.setMaxOcorrencias(25);

		ListarParticipanteAgregadoResponse response = getFactoryAdapter()
				.getListarParticipanteAgregadoPDCAdapter().invokeProcess(
						request);

		List<ListarParticipanteAgregadoSaidaDTO> listaSaida = new ArrayList<ListarParticipanteAgregadoSaidaDTO>();

		if (response != null) {
			for (Ocorrencias saida : response.getOcorrencias()) {
				listaSaida.add(new ListarParticipanteAgregadoSaidaDTO(saida
						.getCdCorpoCpfCnpj(), saida.getCdControleCpfCnpj(),
						saida.getCdFilialCnpj(), saida.getDsNomeParticipante(),
						saida.getDsSacdoEltrnco()));
			}
		}
		return listaSaida;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.service.business.mantercontrato.
	 *      IManterContratoService#listarAgregado(br.com.bradesco.web.pgit.service.business.
	 *      mantercontrato.bean.ListarAgregadoEntradaDTO)
	 */
	public List<ListarAgregadoSaidaDTO> listarAgregado(
			ListarAgregadoEntradaDTO entrada) {
		ListarAgregadoRequest request = new ListarAgregadoRequest();

		request
				.setCdCorpoCpfCnpj(verificaLongNulo(entrada.getCdCorpoCpfCnpj()));
		request.setCdControleCpfCnpj(verificaIntegerNulo(entrada
				.getCdControleCpfCnpj()));
		request.setCdFilialCpfCnpj(verificaIntegerNulo(entrada
				.getCdFilialCpnj()));
		request.setMaxOcorrencias(25);

		ListarAgregadoResponse response = getFactoryAdapter()
				.getListarAgregadoPDCAdapter().invokeProcess(request);

		List<ListarAgregadoSaidaDTO> listaSaida = new ArrayList<ListarAgregadoSaidaDTO>();

		if (response != null) {
			for (br.com.bradesco.web.pgit.service.data.pdc.listaragregado.response.Ocorrencias saida : response
					.getOcorrencias()) {
				listaSaida.add(new ListarAgregadoSaidaDTO(saida
						.getCdCorpoCpfCnpj(), saida.getCdControleCpfCnpj(),
						saida.getCdFilialCnpj(), saida.getDsNomeAgregado()));
			}
		}
		return listaSaida;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.service.business.mantercontrato.
	 *      IManterContratoService#consultarListaOperacaoTarifaTipoServico(br.com.bradesco.web.pgit.service.
	 *      business.simulacaotarifasnegociacao.bean.ListaOperacaoTarifaTipoServicoEntradaDTO)
	 */
	public ListaOperacaoTarifaTipoServicoSaidaDTO consultarListaOperacaoTarifaTipoServico(
			ListaOperacaoTarifaTipoServicoEntradaDTO entradaDTO) {
		ConsultarListaOperacaoTarifaTipoServicoRequest request = new ConsultarListaOperacaoTarifaTipoServicoRequest();

		request.setCdPessoaContrato(verificaLongNulo(entradaDTO
				.getCdPessoaContrato()));
		request.setCdTipoContrato(verificaIntegerNulo(entradaDTO
				.getCdTipoContrato()));
		request.setNrSequenciaContrato(verificaLongNulo(entradaDTO
				.getNrSequenciaContrato()));
		request.setCdNegocioRenegocio(verificaIntegerNulo(entradaDTO
				.getCdNegocioRenegocio()));
		request.setNumeroOcorrencias(verificaIntegerNulo(entradaDTO
				.getNumeroOcorrencias()));
		request.setCdFluxoNegocio(0);
		request.setCdGrupoInfoFluxo(0);
		request.setCdProdutoOperacaoDeflt(0);
		request.setCdOperacaoProdutoServico(0);
		request.setCdTipoCanal(0);
		request.setNrSequenciaContratoNegocio(0);
		request.setNrOcorGrupoProposta(0);
		request.setCdPessoaJuridicaProposta(0);
		request.setCdTipoContratoProposta(0);

		ConsultarListaOperacaoTarifaTipoServicoResponse response = getFactoryAdapter()
				.getConsultarListaOperacaoTarifaTipoServicoPDCAdapter()
				.invokeProcess(request);

		if (response == null) {
			return new ListaOperacaoTarifaTipoServicoSaidaDTO();
		}
		ListaOperacaoTarifaTipoServicoSaidaDTO saidaDTO = new ListaOperacaoTarifaTipoServicoSaidaDTO();
		saidaDTO.setMensagem(response.getMensagem());
		saidaDTO.setCdMensagem(response.getCodMensagem());

		return saidaDTO;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.service.business.mantercontrato.
	 *      IManterContratoService#validarPropostaAndamento(br.com.bradesco.web.pgit.service.business.
	 *      mantercontrato.bean.ValidarPropostaAndamentoEntradaDTO)
	 */
	public ValidarPropostaAndamentoSaidaDTO validarPropostaAndamento(
			ValidarPropostaAndamentoEntradaDTO entrada) {
		ValidarPropostaAndamentoRequest request = new ValidarPropostaAndamentoRequest();

		request.setCdpessoaJuridicaContrato(verificaLongNulo(entrada
				.getCdpessoaJuridicaContrato()));
		request.setCdTipoContratoNegocio(verificaIntegerNulo(entrada
				.getCdTipoContratoNegocio()));
		request.setNrSequenciaContratoNegocio(verificaLongNulo(entrada
				.getNrSequenciaContratoNegocio()));
		request.setCdControleCpfPssoa(verificaIntegerNulo(entrada
				.getCdControleCpfPssoa()));
		request
				.setCdCpfCnpjPssoa(verificaLongNulo(entrada.getCdCpfCnpjPssoa()));
		request.setCdFilialCnpjPssoa(verificaIntegerNulo(entrada
				.getCdFilialCnpjPssoa()));

		ValidarPropostaAndamentoResponse response = getFactoryAdapter()
				.getValidarPropostaAndamentoPDCAdapter().invokeProcess(request);

		ValidarPropostaAndamentoSaidaDTO saida = new ValidarPropostaAndamentoSaidaDTO();
		saida.setCodMensagem(response.getCodMensagem());
		saida.setMensagem(response.getCodMensagem());
		saida.setCdProdutoServicoOperacao(response
				.getCdProdutoServicoOperacao());
		return saida;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.service.business.mantercontrato.
	 *      IManterContratoService#consultarConManAmbPart(br.com.bradesco.web.pgit.service.business.
	 *      mantercontrato.bean.ConsultarConManAmbPartEntradaDTO)
	 */
	public ConsultarConManAmbPartSaidaDTO consultarConManAmbPart(
			ConsultarConManAmbPartEntradaDTO entrada) {
		ConsultarConManAmbPartRequest request = new ConsultarConManAmbPartRequest();

		request.setCdPessoa(verificaLongNulo(entrada.getCdPessoa()));
		request.setCdPessoaJuridica(verificaLongNulo(entrada
				.getCdPessoaJuridica()));
		request.setCdProdutoOperacaoRelacionamento(verificaIntegerNulo(entrada
				.getCdProdutoOperacaoRelacionamento()));
		request.setCdProdutoServicoOperacao(verificaIntegerNulo(entrada
				.getCdProdutoServicoOperacao()));
		request.setCdRelacionamentoProdutoProduto(verificaIntegerNulo(entrada
				.getCdRelacionamentoProdutoProduto()));
		request.setCdTipoContrato(verificaIntegerNulo(entrada
				.getCdTipoContrato()));
		request.setCdTipoParticipacaoPessoa(verificaIntegerNulo(entrada
				.getCdTipoParticipacaoPessoa()));
		request.setHrInclusaoRegistroHist(verificaStringNula(entrada
				.getHrInclusaoRegistroHist()));
		request.setNrSequenciaContrato(verificaLongNulo(entrada
				.getNrSequenciaContrato()));

		ConsultarConManAmbPartResponse response = getFactoryAdapter()
				.getConsultarConManAmbPartPDCAdapter().invokeProcess(request);

		ConsultarConManAmbPartSaidaDTO saida = new ConsultarConManAmbPartSaidaDTO();
		saida.setCdCanalInclusao(response.getCdCanalInclusao());
		saida.setCdCanalManutencao(response.getCdCanalManutencao());
		saida.setCdUsuarioInclusao(response.getCdUsuarioInclusao());
		saida.setCdUsuarioManutencao(response.getCdUsuarioManutencao());
		saida.setCodMensagem(response.getCodMensagem());
		saida.setDsCanalInclusao(response.getDsCanalInclusao());
		saida.setDsCanalManutencao(response.getDsCanalManutencao());
		saida.setDsTipoManutencao(response.getDsTipoManutencao());
		saida.setHrManutencaoRegistroManutencao(response
				.getHrManutencaoRegistroManutencao());
		saida.setHrManutencaRegistroInclusao(response
				.getHrManutencaRegistroInclusao());
		saida.setMensagem(response.getMensagem());
		saida.setNmOperacaoFluxoInclusao(response.getNmOperacaoFluxoInclusao());
		saida.setNmOperacaoFluxoManutencao(response
				.getNmOperacaoFluxoManutencao());
		return saida;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.service.business.mantercontrato.
	 *      IManterContratoService#listarConManAmbPart(br.com.bradesco.web.pgit.service.business.
	 *      mantercontrato.bean.ListarConManAmbPartEntradaDTO)
	 */
	public List<ListarConManAmbPartOcorrenciatSaidaDTO> listarConManAmbPart(
			ListarConManAmbPartEntradaDTO entrada) {
		ListarConManAmbPartRequest request = new ListarConManAmbPartRequest();
		request.setCdPessoa(verificaLongNulo(entrada.getCdPessoa()));
		request.setCdPessoaJuridica(verificaLongNulo(entrada
				.getCdPessoaJuridica()));
		request.setCdProdutoOperacaoRelacionamento(verificaIntegerNulo(entrada
				.getCdProdutoOperacaoRelacionamento()));
		request.setCdTipoContrato(verificaIntegerNulo(entrada
				.getCdTipoContrato()));
		request.setNrSequenciaContrato(verificaLongNulo(entrada
				.getNrSequenciaContrato()));
		request.setDtFimManutencao(verificaStringNula(entrada
				.getDtFimManutencao()));
		request.setDtInicioManutencao(verificaStringNula(entrada
				.getDtInicioManutencao()));
		request.setMaxOcorrencias(50);

		ListarConManAmbPartResponse response = getFactoryAdapter()
				.getListarConManAmbPartPDCAdapter().invokeProcess(request);
		List<ListarConManAmbPartOcorrenciatSaidaDTO> lista = new ArrayList<ListarConManAmbPartOcorrenciatSaidaDTO>();

		for (br.com.bradesco.web.pgit.service.data.pdc.listarconmanambpart.response.Ocorrencias item : response
				.getOcorrencias()) {
			ListarConManAmbPartOcorrenciatSaidaDTO ocorrencia = new ListarConManAmbPartOcorrenciatSaidaDTO();
			ocorrencia.setCdControleCpfCnpj(item.getCdControleCpfCnpj());
			ocorrencia.setCdCorpoCpfCnpj(item.getCdCorpoCpfCnpj());
			ocorrencia.setCdFilialCnpj(item.getCdFilialCnpj());
			ocorrencia.setCdIndicadorTipoManutencao(item
					.getCdIndicadorTipoManutencao());
			ocorrencia.setCdPessoa(item.getCdPessoa());
			ocorrencia.setCdProdutoOperacaoRelacionado(item
					.getCdProdutoOperacaoRelacionado());
			ocorrencia.setCdRelacionamentoProdutoProduto(item
					.getCdRelacionamentoProdutoProduto());
			ocorrencia.setCdTipoParticipacaoPessoa(item
					.getCdTipoParticipacaoPessoa());
			ocorrencia.setCdTipoServico(item.getCdTipoServico());
			ocorrencia.setCdUsuarioManutencao(item.getCdUsuarioManutencao());
			ocorrencia.setDsAmbienteOperacao(item.getDsAmbienteOperacao());
			ocorrencia.setDsNomeRazaoSocial(item.getDsNomeRazaoSocial());
			ocorrencia.setDsTipoServico(item.getDsTipoServico());
			ocorrencia.setDtHoraManutencao(item.getDtHoraManutencao());
			ocorrencia.setCdProdutoServicoOperacao(item
					.getCdProdutoServicoOperacao());
			lista.add(ocorrencia);
		}
		return lista;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.service.business.mantercontrato.
	 *      IManterContratoService#alterarTarifaAcimaPadrao(br.com.bradesco.web.pgit.service.
	 *      business.mantercontrato.bean.AlterarTarifaAcimaPadraoEntradaDTO)
	 */
	public AlterarTarifaAcimaPadraoSaidaDTO alterarTarifaAcimaPadrao(
			AlterarTarifaAcimaPadraoEntradaDTO entradaDTO) {
		AlterarTarifaAcimaPadraoRequest request = new AlterarTarifaAcimaPadraoRequest();
		request.setCdOperacao(verificaIntegerNulo(entradaDTO.getCdOperacao()));
		request.setCdpessoaJuridicaContrato(verificaLongNulo(entradaDTO
				.getCdpessoaJuridicaContrato()));
		request.setCdProduto(verificaIntegerNulo(entradaDTO.getCdProduto()));
		request.setCdProdutoServicoRelacionado(verificaIntegerNulo(entradaDTO
				.getCdProdutoServicoRelacionado()));
		request.setCdRelacionamentoProduto(verificaIntegerNulo(entradaDTO
				.getCdRelacionamentoProduto()));
		request.setCdTipoContratoNegocio(verificaIntegerNulo(entradaDTO
				.getCdTipoContratoNegocio()));
		request.setNrSequenciaContratoNegocio(verificaLongNulo(entradaDTO
				.getNrSequenciaContratoNegocio()));
		request.setVlrTarifaContratoNegocio(verificaBigDecimalNulo(entradaDTO
				.getVlrTarifaContratoNegocio()));

		AlterarTarifaAcimaPadraoResponse response = getFactoryAdapter()
				.getAlterarTarifaAcimaPadraoPDCAdapter().invokeProcess(request);

		if (response == null) {
			return new AlterarTarifaAcimaPadraoSaidaDTO();
		}

		AlterarTarifaAcimaPadraoSaidaDTO saidaDTO = new AlterarTarifaAcimaPadraoSaidaDTO();

		saidaDTO.setMensagem(response.getMensagem());
		saidaDTO.setCodMensagem(response.getCodMensagem());

		return saidaDTO;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.service.business.mantercontrato.
	 *      IManterContratoService#consultarContaDebTarifa(br.com.bradesco.web.pgit.service.
	 *      business.mantercontrato.bean.ConsultarContaDebTarifaEntradaDTO)
	 */
	public ConsultarContaDebTarifaSaidaDTO consultarContaDebTarifa(
			ConsultarContaDebTarifaEntradaDTO entrada) {
		ConsultarContaDebTarifaRequest request = new ConsultarContaDebTarifaRequest();

		request.setCdPessoaJuridicaNegocio(verificaLongNulo(entrada
				.getCdPessoaJuridicaNegocio()));
		request.setCdTipoContratoNegocio(verificaIntegerNulo(entrada
				.getCdTipoContratoNegocio()));
		request.setNrSequenciaContratoNegocio(verificaLongNulo(entrada
				.getNrSequenciaContratoNegocio()));
		request.setCdProdutoServicoOperacao(verificaIntegerNulo(entrada
				.getCdProdutoServicoOperacao()));
		request.setCdProdutoOperacaoRelacionado(verificaIntegerNulo(entrada
				.getCdProdutoOperacaoRelacionado()));
		request.setCdOperacaoProdutoServico(verificaIntegerNulo(entrada
				.getCdOperacaoProdutoServico()));
		request.setCdOperacaoServicoIntegrado(verificaIntegerNulo(entrada
				.getCdOperacaoServicoIntegrado()));

		ConsultarContaDebTarifaResponse response = getFactoryAdapter()
				.getConsultarContaDebTarifaPDCAdapter().invokeProcess(request);

		if (response == null) {
			return new ConsultarContaDebTarifaSaidaDTO();
		}

		ConsultarContaDebTarifaSaidaDTO saidaDTO = new ConsultarContaDebTarifaSaidaDTO();
		saidaDTO.setMensagem(response.getMensagem());
		saidaDTO.setCodMensagem(response.getCodMensagem());
		saidaDTO.setCdUsuarioInclusao(response.getCdUsuarioInclusao());
		saidaDTO.setCdUsuarioInclusaoExterno(response
				.getCdUsuarioInclusaoExterno());
		saidaDTO.setHrInclusaoRegistro(response.getHrInclusaoRegistro());
		saidaDTO.setCdOperacaoCanalInclusao(response
				.getCdOperacaoCanalInclusao());
		saidaDTO.setCdTipoCanalInclusao(response.getCdTipoCanalInclusao());
		saidaDTO.setDsTpoCanalInclusao(response.getDsTpoCanalInclusao());
		saidaDTO.setCdUsuarioManutencao(response.getCdUsuarioManutencao());
		saidaDTO.setCdUsuarioManutencaoExterno(response
				.getCdUsuarioManutencaoExterno());
		saidaDTO.setHrManutencaoRegistro(response.getHrManutencaoRegistro());
		saidaDTO.setCdOperacaoCanalManutencao(response
				.getCdOperacaoCanalManutencao());
		saidaDTO.setCdTipoCanalManutencao(response.getCdTipoCanalManutencao());
		saidaDTO.setDsTipoCanalManutencao(response.getDsTipoCanalManutencao());
		return saidaDTO;
	}

	public DetalharHistoricoDebTarifaSaidaDTO detalharHistoricoDebTarifa(
			DetalharHistoricoDebTarifaEntradaDTO entrada) {
		DetalharHistoricoDebTarifaRequest request = new DetalharHistoricoDebTarifaRequest();
		DetalharHistoricoDebTarifaResponse response = new DetalharHistoricoDebTarifaResponse();

		request
				.setNrOcorrencias(verificaIntegerNulo(entrada
						.getNrOcorrencias()));
		request.setCdPessoaJuridicaNegocio(verificaLongNulo(entrada
				.getCdPessoaJuridicaNegocio()));
		request.setCdTipoContratoNegocio(verificaIntegerNulo(entrada
				.getCdTipoContratoNegocio()));
		request.setNrSequenciaContratoNegocio(verificaLongNulo(entrada
				.getNrSequenciaContratoNegocio()));
		request.setCdProdutoServicoOperacao(verificaIntegerNulo(entrada
				.getCdProdutoServicoOperacao()));
		request.setCdProdutoOperacaoRelacionado(verificaIntegerNulo(entrada
				.getCdProdutoOperacaoRelacionado()));
		request.setCdOperacaoProdutoServico(verificaIntegerNulo(entrada
				.getCdOperacaoProdutoServico()));
		request.setHrInclusaoRegistroHist(verificaStringNula(entrada
				.getHrInclusaoRegistroHist()));

		response = getFactoryAdapter()
				.getDetalharHistoricoDebTarifaPDCAdapter().invokeProcess(
						request);

		if (response == null) {
			return new DetalharHistoricoDebTarifaSaidaDTO();
		}

		DetalharHistoricoDebTarifaSaidaDTO saidaDTO = new DetalharHistoricoDebTarifaSaidaDTO();

		saidaDTO.setMensagem(response.getMensagem());
		saidaDTO.setCodMensagem(response.getCodMensagem());
		saidaDTO.setCdLayout(response.getCdLayout());
		saidaDTO.setTamanhoLayout(response.getTamanhoLayout());
		saidaDTO.setCdUsuarioInclusao(response.getCdUsuarioInclusao());
		saidaDTO.setCdUsuarioInclusaoExterno(response
				.getCdUsuarioInclusaoExterno());
		saidaDTO.setHrInclusaoRegistro(response.getHrInclusaoRegistro());
		saidaDTO.setCdOperacaoCanalInclusao(response
				.getCdOperacaoCanalInclusao());
		saidaDTO.setCdTipoCanalInclusao(response.getCdTipoCanalInclusao());
		saidaDTO.setDsTpoCanalInclusao(response.getDsTpoCanalInclusao());
		saidaDTO.setCdUsuarioManutencao(response.getCdUsuarioManutencao());
		saidaDTO.setCdUsuarioManutencaoExterno(response
				.getCdUsuarioManutencaoExterno());
		saidaDTO.setHrManutencaoRegistro(response.getHrManutencaoRegistro());
		saidaDTO.setCdOperacaoCanalManutencao(response
				.getCdOperacaoCanalManutencao());
		saidaDTO.setCdTipoCanalManutencao(response.getCdTipoCanalManutencao());
		saidaDTO.setDsTipoCanalManutencao(response.getDsTipoCanalManutencao());
		return saidaDTO;
	}

	public MensagemSaida incluirContaDebTarifa(
			IncluirContaDebTarifaEntradaDTO entrada) {
		IncluirContaDebTarifaRequest request = new IncluirContaDebTarifaRequest();

		request.setCdPessoaJuridicaNegocio(verificaLongNulo(entrada
				.getCdPessoaJuridicaNegocio()));
		request.setCdTipoContratoNegocio(verificaIntegerNulo(entrada
				.getCdTipoContratoNegocio()));
		request.setNrSequenciaContratoNegocio(verificaLongNulo(entrada
				.getNrSequenciaContratoNegocio()));
		request.setCdPessoaJuridicaVinculo(verificaLongNulo(entrada
				.getCdPessoaJuridicaVinculo()));
		request.setCdTipoContratoVinculo(verificaIntegerNulo(entrada
				.getCdTipoContratoVinculo()));
		request.setNrSequenciaContratoVinculo(verificaLongNulo(entrada
				.getNrSequenciaContratoVinculo()));
		request.setCdTipoVincContrato(verificaIntegerNulo(entrada
				.getCdTipoVincContrato()));
		request.setCdTipoOperacao(verificaIntegerNulo(entrada
				.getCdTipoOperacao()));
		request
				.setNrOcorrencias(verificaIntegerNulo(entrada
						.getNrOcorrencias()));

		if (entrada.getNrOcorrencias() != null
				&& entrada.getNrOcorrencias() != 0) {
			for (int i = 0; i < entrada.getOcorrencias().size(); i++) {

                br.com.bradesco.web.pgit.service.data.pdc.incluircontadebtarifa.request.Ocorrencias ocorrecias =
                    new br.com.bradesco.web.pgit.service.data.pdc.incluircontadebtarifa.request.Ocorrencias();

				ocorrecias.setCdProdutoServicoOperacao(entrada.getOcorrencias()
						.get(i).getCdProdutoServicoOperacao());
				ocorrecias.setCdProdutoOperacaoRelacionado(entrada
						.getOcorrencias().get(i)
						.getCdProdutoOperacaoRelacionado());
				ocorrecias.setCdServicoCompostoPagamento(entrada
						.getOcorrencias().get(i)
						.getCdServicoCompostoPagamento());
				ocorrecias.setCdOperacaoProdutoServico(entrada.getOcorrencias()
						.get(i).getCdOperacaoProdutoServico());
				ocorrecias.setCdOperacaoServicoIntegrado(entrada
						.getOcorrencias().get(i)
						.getCdOperacaoServicoIntegrado());
				ocorrecias.setCdNaturezaOperacaoPagamento(entrada
						.getOcorrencias().get(i)
						.getCdNaturezaOperacaoPagamento());

				request.addOcorrencias(ocorrecias);
			}
		} else {

            br.com.bradesco.web.pgit.service.data.pdc.incluircontadebtarifa.request.Ocorrencias ocorrecias =
                new br.com.bradesco.web.pgit.service.data.pdc.incluircontadebtarifa.request.Ocorrencias();
			ocorrecias.setCdProdutoServicoOperacao(0);
			ocorrecias.setCdProdutoOperacaoRelacionado(0);
			ocorrecias.setCdServicoCompostoPagamento(0);
			ocorrecias.setCdOperacaoProdutoServico(0);
			ocorrecias.setCdOperacaoServicoIntegrado(0);
			ocorrecias.setCdNaturezaOperacaoPagamento(0);
			request.addOcorrencias(ocorrecias);
		}

		IncluirContaDebTarifaResponse response = getFactoryAdapter()
				.getIncluirContaDebTarifaPDCAdapter().invokeProcess(request);
		MensagemSaida objSaida = new MensagemSaida();
		objSaida.setCodMensagem(response.getCodMensagem());
		objSaida.setMensagem(response.getMensagem());

		return objSaida;
	}

	public ListarContaDebTarifaSaidaDTO listarContaDebTarifa(
			ListarContaDebTarifaEntradaDTO entrada) {
		ListarContaDebTarifaRequest request = new ListarContaDebTarifaRequest();
		request.setNrOcorrencias(50);
		request.setCdPessoaJuridicaNegocio(verificaLongNulo(entrada
				.getCdPessoaJuridicaNegocio()));
		request.setCdTipoContratoNegocio(verificaIntegerNulo(entrada
				.getCdTipoContratoNegocio()));
		request.setNrSequenciaContratoNegocio(verificaLongNulo(entrada
				.getNrSequenciaContratoNegocio()));
		request.setCdCorpoCpfCnpj(verificaLongNulo(entrada
				.getCdCorpoCpfCnpj()));
		request.setCdControleCpfCnpj(verificaIntegerNulo(entrada
				.getCdControleCpfCnpj()));
		request.setCdDigitoCpfCnpj(verificaIntegerNulo(entrada
				.getCdDigitoCpfCnpj()));
		request.setCdBanco(verificaIntegerNulo(entrada.getCdBanco()));
		request.setCdAgencia(verificaIntegerNulo(entrada.getCdAgencia()));
		request.setCdDigitoAgencia(verificaIntegerNulo(entrada
				.getCdDigitoAgencia()));
		request.setCdConta(verificaLongNulo(entrada.getCdConta()));
		request.setCdDigitoConta(PgitUtil.verificaBytesDigitoConta(entrada
				.getCdDigitoConta()));
		request.setCdTipoConta(verificaIntegerNulo(entrada.getCdTipoConta()));

		ListarContaDebTarifaResponse response = getFactoryAdapter()
				.getListarContaDebTarifaPDCAdapter().invokeProcess(request);

		List<ListarContaDebTarifaOcorrencias> lista = new ArrayList<ListarContaDebTarifaOcorrencias>();
		ListarContaDebTarifaSaidaDTO objSaida = new ListarContaDebTarifaSaidaDTO();

		objSaida.setCodMensagem(response.getCodMensagem());
		objSaida.setMensagem(response.getMensagem());
		objSaida.setNumeroLinhas(response.getNumeroLinhas());
		objSaida
				.setOcorrencias(new ArrayList<ListarContaDebTarifaOcorrencias>());
		for (br.com.bradesco.web.pgit.service.data.pdc.listarcontadebtarifa.response.Ocorrencias item : response
				.getOcorrencias()) {
			ListarContaDebTarifaOcorrencias ocorrencia = new ListarContaDebTarifaOcorrencias();
			ocorrencia.setCdPessoaJuridicaVinculo(item
					.getCdPessoaJuridicaVinculo());
			ocorrencia
					.setCdTipoContratoVinculo(item.getCdTipoContratoVinculo());
			ocorrencia.setNrSequenciaContratoVinculo(item
					.getNrSequenciaContratoVinculo());
			ocorrencia
					.setCdTipoVinculoContrato(item.getCdTipoVinculoContrato());
			ocorrencia.setCdCpfCnpj(item.getCdCpfCnpj());
			ocorrencia.setCdParticipante(item.getCdParticipante());
			ocorrencia.setNmParticipante(item.getNmParticipante());
			ocorrencia.setCdBanco(item.getCdBanco());
			ocorrencia.setDsBanco(item.getDsBanco());
			ocorrencia.setCdAgencia(item.getCdAgencia());
			ocorrencia.setCdDigitoAgencia(item.getCdDigitoAgencia());
			ocorrencia.setDsAgencia(item.getDsAgencia());
			ocorrencia.setCdConta(item.getCdConta());
			ocorrencia.setCdDigitoConta(item.getCdDigitoConta());
			ocorrencia.setCdTipoConta(item.getCdTipoConta());
			ocorrencia.setDsTipoConta(item.getDsTipoConta());
			lista.add(ocorrencia);
			objSaida.setOcorrencias(lista);
		}
		return objSaida;
	}

	public ListarHistoricoDebTarifaSaidaDTO listarHistoricoDebTarifa(
			ListarHistoricoDebTarifaEntradaDTO entrada) {
		ListarHistoricoDebTarifaRequest request = new ListarHistoricoDebTarifaRequest();

		request.setNrOcorrencias(30);
		request.setCdPessoaJuridicaNegocio(verificaLongNulo(entrada
				.getCdPessoaJuridicaNegocio()));
		request.setCdTipoContratoNegocio(verificaIntegerNulo(entrada
				.getCdTipoContratoNegocio()));
		request.setNrSequenciaContratoNegocio(verificaLongNulo(entrada
				.getNrSequenciaContratoNegocio()));
		request.setDataInicio(verificaStringNula(entrada.getDataInicio()));
		request.setDataFim(verificaStringNula(entrada.getDataFim()));

		ListarHistoricoDebTarifaResponse response = getFactoryAdapter()
				.getListarHistoricoDebTarifaPDCAdapter().invokeProcess(request);

		ListarHistoricoDebTarifaSaidaDTO objSaida = new ListarHistoricoDebTarifaSaidaDTO();
		List<ListarHistoricoDebTarifaOcorrencias> lista = new ArrayList<ListarHistoricoDebTarifaOcorrencias>();
		objSaida.setCodMensagem(response.getCodMensagem());
		objSaida.setMensagem(response.getMensagem());
		objSaida.setNumeroLinhas(response.getNumeroLinhas());
		objSaida
				.setOcorrencias(new ArrayList<ListarHistoricoDebTarifaOcorrencias>());

		for (br.com.bradesco.web.pgit.service.data.pdc.listarhistoricodebtarifa.response.Ocorrencias item : response
				.getOcorrencias()) {
			ListarHistoricoDebTarifaOcorrencias ocorrencia = new ListarHistoricoDebTarifaOcorrencias();

			ocorrencia.setCdProdutoServicoOperacao(item
					.getCdProdutoServicoOperacao());
			ocorrencia.setDsProdutoServicoOperacao(item
					.getDsProdutoServicoOperacao());
			ocorrencia.setCdProdutoOperacaoRelacionado(item
					.getCdProdutoOperacaoRelacionado());
			ocorrencia.setDsProdutoOperacaoRelacionada(item
					.getDsProdutoOperacaoRelacionada());
			ocorrencia.setCdServicoCompostoPagamento(item
					.getCdServicoCompostoPagamento());
			ocorrencia.setCdOperacaoProdutoServico(item
					.getCdOperacaoProdutoServico());
			ocorrencia.setDsOperacaoProdutoServico(item
					.getDsOperacaoProdutoServico());
			ocorrencia.setCdNaturezaOperacaoPagamento(item
					.getCdNaturezaOperacaoPagamento());
			ocorrencia.setDsNatuzOperPgto(item.getDsNatuzOperPgto());
			ocorrencia.setCdCorpoCpfCnpj(item.getCdCorpoCpfCnpj());
			ocorrencia.setCdParticipante(item.getCdParticipante());
			ocorrencia.setNmParticipante(item.getNmParticipante());
			ocorrencia.setCdBanco(item.getCdBanco());
			ocorrencia.setDsBanco(item.getDsBanco());
			ocorrencia.setCdAgencia(item.getCdAgencia());
			ocorrencia.setCdDigitoAgencia(item.getCdDigitoAgencia());
			ocorrencia.setDsAgencia(item.getDsAgencia());
			ocorrencia.setCdConta(item.getCdConta());
			ocorrencia.setCdDigitoConta(item.getCdDigitoConta());
			ocorrencia.setCdTipoConta(item.getCdTipoConta());
			ocorrencia.setDsTipoConta(item.getDsTipoConta());
			ocorrencia.setHrInclusaoRegistroHist(item
					.getHrInclusaoRegistroHist());
			ocorrencia.setDtManutencao(item.getDtManutencao());
			ocorrencia.setHrManutencao(item.getHrManutencao());
			ocorrencia.setCdUsuario(item.getCdUsuario());
			ocorrencia.setDsTipoManutencao(item.getDsTipoManutencao());

			lista.add(ocorrencia);
			objSaida.setOcorrencias(lista);
		}

		return objSaida;
	}

	public ListarOperacaoContaDebTarifaSaidaDTO listarOperacaoContaDebTarifa(
			ListarOperacaoContaDebTarifaEntradaDTO entrada) {
		ListarOperacaoContaDebTarifaRequest request = new ListarOperacaoContaDebTarifaRequest();
		request.setNrOcorrencias(30);
		request
				.setCdPessoaJuridicaNegocio(entrada
						.getCdPessoaJuridicaNegocio());
		request.setCdTipoContratoNegocio(entrada.getCdTipoContratoNegocio());
		request.setNrSequenciaContratoNegocio(entrada
				.getNrSequenciaContratoNegocio());

		ListarOperacaoContaDebTarifaResponse response = getFactoryAdapter()
				.getListarOperacaoContaDebTarifaPDCAdapter().invokeProcess(
						request);
		List<ListarOperacaoContaDebTarifaOcorrencias> lista = new ArrayList<ListarOperacaoContaDebTarifaOcorrencias>();
		ListarOperacaoContaDebTarifaSaidaDTO objSaida = new ListarOperacaoContaDebTarifaSaidaDTO();

		objSaida.setCodMensagem(response.getCodMensagem());
		objSaida.setMensagem(response.getMensagem());
		objSaida.setNumeroLinhas(response.getNumeroLinhas());
		objSaida
				.setOcorrencias(new ArrayList<ListarOperacaoContaDebTarifaOcorrencias>());

		for (br.com.bradesco.web.pgit.service.data.pdc.listaroperacaocontadebtarifa.response.Ocorrencias item : response
				.getOcorrencias()) {
			ListarOperacaoContaDebTarifaOcorrencias ocorrencia = new ListarOperacaoContaDebTarifaOcorrencias();
			ocorrencia.setCdProdutoServicoOperacao(item
					.getCdProdutoServicoOperacao());
			ocorrencia.setDsProdutoServicoOperacao(item
					.getDsProdutoServicoOperacao());
			ocorrencia.setCdProdutoOperacaoRelacionado(item
					.getCdProdutoOperacaoRelacionado());
			ocorrencia.setDsProdutoOperacaoRelacionada(item
					.getDsProdutoOperacaoRelacionada());
			ocorrencia.setCdServicoCompostoPagamento(item
					.getCdServicoCompostoPagamento());
			ocorrencia.setCdOperacaoProdutoServico(item
					.getCdOperacaoProdutoServico());
			ocorrencia.setDsOperacaoProdutoServico(item
					.getDsOperacaoProdutoServico());
			ocorrencia.setCdOperacaoServicoIntegrado(item
					.getCdOperacaoServicoIntegrado());
			ocorrencia.setCdNaturezaOperacaoPagamento(item
					.getCdNaturezaOperacaoPagamento());
			ocorrencia.setDsNatuzOperPgto(item.getDsNatuzOperPgto());
			ocorrencia.setCdPessoaJuridicaVinculo(item
					.getCdPessoaJuridicaVinculo());
			ocorrencia
					.setCdTipoContratoVinculo(item.getCdTipoContratoVinculo());
			ocorrencia.setNrSequenciaContratoVinculo(item
					.getNrSequenciaContratoVinculo());
			ocorrencia.setCdTipoVincContrato(item.getCdTipoVincContrato());
			ocorrencia.setCdCorpoCpfCnpj(item.getCdCorpoCpfCnpj());
			ocorrencia.setCdParticipante(item.getCdParticipante());
			ocorrencia.setNmParticipante(item.getNmParticipante());
			ocorrencia.setCdBanco(item.getCdBanco());
			ocorrencia.setDsBanco(item.getDsBanco());
			ocorrencia.setCdAgencia(item.getCdAgencia());
			ocorrencia.setCdDigitoAgencia(item.getCdDigitoAgencia());
			ocorrencia.setDsAgencia(item.getDsAgencia());
			ocorrencia.setCdConta(item.getCdConta());
			ocorrencia.setCdDigitoConta(item.getCdDigitoConta());
			ocorrencia.setCdTipoConta(item.getCdTipoConta());
			ocorrencia.setDsTipoConta(item.getDsTipoConta());
			lista.add(ocorrencia);
			objSaida.setOcorrencias(lista);
		}
		return objSaida;
	}

	public ListarRepresentanteSaidaDTO listarRepresentantesComprovante(
			ListarRepresentanteEntradaDTO entrada) {
		ListarRepresentanteSaidaDTO saida = new ListarRepresentanteSaidaDTO();

		ListarRepresentanteRequest request = new ListarRepresentanteRequest();
		request.setCdFlagRepresentante(entrada.getCdFlagRepresentante());
		request.setCdpessoaJuridicaContrato(entrada
				.getCdpessoaJuridicaContrato());
		request.setCdTipoContratoNegocio(entrada.getCdTipoContratoNegocio());
		request
				.setMaxOcorrencias(IManterContratoServiceConstants.OCORRENCIAS_LISTAR_HISTORICO_CONTAS);
		request.setNrSequenciaContratoNegocio(entrada
				.getNrSequenciaContratoNegocio());

		ListarRepresentanteResponse response = getFactoryAdapter()
				.getListarRepresentantePDCAdapter().invokeProcess(request);

		if (response != null) {
			saida = new ListarRepresentanteSaidaDTO(response);
		}

		return saida;
	}

	public IncluirRepresentanteSaidaDTO incluirRepresentantesComprovante(
			IncluirRepresentanteEntradaDTO entrada) {
		IncluirRepresentanteSaidaDTO saida = new IncluirRepresentanteSaidaDTO();

		IncluirRepresentanteRequest request = new IncluirRepresentanteRequest();
		request.setCdCanal(entrada.getCdCanal());
		request.setCdDependenciaOperante(entrada.getCdDependenciaOperante());
		request.setCdEmpresaOperante(entrada.getCdEmpresaOperante());
		request.setCdProdutoServicoOperacao(entrada
				.getCdProdutoServicoOperacao());
		request
				.setCdFinalidadeItemAditivo(entrada
						.getCdFinalidadeItemAditivo());
		request.setCdIndicadorFormaContratacao(entrada
				.getCdIndicadorFormaContratacao());
		request.setCdMidiaComprovanteCorrentista(entrada
				.getCdMidiaComprovanteCorrentista());
		request.setCdpessoaJuridicaContrato(entrada
				.getCdpessoaJuridicaContrato());
		request.setCdProdutoOperacaoRelacionado(entrada
				.getCdProdutoOperacaoRelacionado());
		request.setCdRelacionamentoProdutoProduto(entrada
				.getCdRelacionamentoProdutoProduto());
		request.setCdTipoContratoNegocio(entrada.getCdTipoContratoNegocio());
		request.setCdUsuario(entrada.getCdUsuario());
		request.setDsMidiaComprovanteCorrentista(entrada
				.getDsMidiaComprovanteCorrentista());
		request
				.setDsViaCobradaComprovante(entrada
						.getDsViaCobradaComprovante());
		request.setNmOperacaoFluxo(entrada.getNmOperacaoFluxo());
		request.setNrManutencaoContratoNegocio(entrada
				.getNrManutencaoContratoNegocio());
		request.setNrSequenciaContratoNegocio(entrada
				.getNrSequenciaContratoNegocio());
		request.setQtdeTotalItens(entrada.getQtdeTotalItens());
		request.setQtFuncionarioEmpresaPagadora(entrada
				.getQtFuncionarioEmpresaPagadora());
		request
				.setQtViaCobradaComprovante(entrada
						.getQtViaCobradaComprovante());
		request.setQtViaComprovante(entrada.getQtViaComprovante());

        List<br.com.bradesco.web.pgit.service.data.pdc.incluirrepresentante.request.Ocorrencias> ocorrenciasAux =
            new ArrayList<br.com.bradesco.web.pgit.service.data.pdc.incluirrepresentante.request.Ocorrencias>();

		for (ListarRepresentanteOcorrenciaDTO dto : entrada.getOcorrencias()) {
            br.com.bradesco.web.pgit.service.data.pdc.incluirrepresentante.request.Ocorrencias ocorrencia =
                new br.com.bradesco.web.pgit.service.data.pdc.incluirrepresentante.request.Ocorrencias();

			ocorrencia.setCdPessoa(dto.getCdPessoa());
			ocorrencia.setCdTipoParticipacaoPessoa(dto
					.getCdTipoParticipacaoPessoa());
			ocorrencia.setDsRazaoSocialParticipante(dto.getDsNomeRazao());
			ocorrencia.setDsRazaoSocialReduzido(dto.getDsnome());
			ocorrencia.setCdUnidadeOrganizacional(dto.getCdAgencia());
			ocorrencia.setNrConta(dto.getCdConta());
			ocorrencia.setNrContaDigito(dto.getCdContaDigito());
			ocorrencia.setNrControleCnpjParticipante(dto.getCdControleCnpj());
			ocorrencia.setNrCorpoCpfCnpj(dto.getCdCPF());
			ocorrencia.setNrFilialCnpjParticipante(dto.getCdFilialCnpj());

			ocorrenciasAux.add(ocorrencia);
		}

		request
				.setOcorrencias(ocorrenciasAux
						.toArray(new br.com.bradesco.web.pgit.service.data.pdc.incluirrepresentante.request.Ocorrencias[0]));

		IncluirRepresentanteResponse response = getFactoryAdapter()
				.getIncluirRepresentantePDCAdapter().invokeProcess(request);

		if (response != null) {
			saida.setCodMensagem(response.getCodMensagem());
			saida.setMensagem(response.getMensagem());
		}

		return saida;
	}
	
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.mantercontrato.IManterContratoService#validarUsuario(
	 * br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ValidarUsuarioEntradaDTO)
	 */
	public ValidarUsuarioSaidaDTO validarUsuario(ValidarUsuarioEntradaDTO entrada){
		ValidarUsuarioSaidaDTO saida = new ValidarUsuarioSaidaDTO();
		ValidarUsuarioRequest request = new ValidarUsuarioRequest();
		
		request.setMaxOcorrencias(PgitUtil.verificaIntegerNulo(entrada.getMaxOcorrencias()));

		ValidarUsuarioResponse response = getFactoryAdapter()
				.getValidarUsuarioPDCAdapter().invokeProcess(request);

		if (response != null) {
			saida.setCodMensagem(response.getCodMensagem());
			saida.setMensagem(response.getMensagem());
			saida.setCdAcessoDepartamento(response.getCdAcessoDepartamento());
			saida.setDsAcessoDepartamento(response.getDsAcessoDepartamento());
			saida.setNrSequenciaUnidadeOrganizacional(response.getNrSequenciaUnidadeOrganizacional());
		}

		return saida;
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.mantercontrato.IManterContratoService#
	 * validarVincConvenioCtaSalario(
	 * br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ValidarVinculacaoConvenioContaSalarioEntradaDTO)
	 */
	public ValidarVinculacaoConvenioContaSalarioSaidaDTO validarVincConvenioCtaSalario(
			ValidarVinculacaoConvenioContaSalarioEntradaDTO entrada) {
		ValidarVinculacaoConvenioContaSalarioRequest request = new ValidarVinculacaoConvenioContaSalarioRequest();
		ValidarVinculacaoConvenioContaSalarioResponse response = new ValidarVinculacaoConvenioContaSalarioResponse();
		ValidarVinculacaoConvenioContaSalarioSaidaDTO objSaida = new ValidarVinculacaoConvenioContaSalarioSaidaDTO();
		
		 request.setCdpessoaJuridicaContrato(verificaLongNulo(entrada.getCdpessoaJuridicaContrato()));
	     request.setCdTipoContratoNegocio(verificaIntegerNulo(entrada.getCdTipoContratoNegocio()));
	     request.setNrSequenciaContratoNegocio(verificaLongNulo(entrada.getNrSequenciaContratoNegocio()));
	     request.setCdClubRepresentante(verificaLongNulo(entrada.getCdClubRepresentante()));
	     request.setCdCpfCnpjRepresentante(verificaLongNulo(entrada.getCdCpfCnpjRepresentante()));
	     request.setCdFilialCnpjRepresentante(verificaIntegerNulo(entrada.getCdFilialCnpjRepresentante()));
	     request.setCdControleCpfRepresentante(verificaIntegerNulo(entrada.getCdControleCpfRepresentante()));
	     request.setCdPessoaJuridicaConta(verificaLongNulo(entrada.getCdPessoaJuridicaConta()));
	     request.setCdTipoContratoConta(verificaIntegerNulo(entrada.getCdTipoContratoConta()));
	     request.setNrSequenciaContratoConta(verificaLongNulo(entrada.getNrSequenciaContratoConta()));
	     request.setCdInstituicaoBancaria(verificaIntegerNulo(entrada.getCdInstituicaoBancaria()));
	     request.setCdUnidadeOrganizacional(verificaIntegerNulo(entrada.getCdUnidadeOrganizacional()));
	     request.setNrConta(verificaIntegerNulo(entrada.getNrConta()));
	     request.setNrContaDigito(verificaStringNula(entrada.getNrContaDigito()));
	     request.setCdIndicadorNumConve(verificaIntegerNulo(entrada.getCdIndicadorNumConve()));
	     request.setCdConveCtaSalarial(verificaLongNulo(entrada.getCdConveCtaSalarial()));
	     request.setVlPagamentoMes(verificaBigDecimalNulo(entrada.getVlPagamentoMes()));
	     request.setQtdFuncionarioFlPgto(verificaIntegerNulo(entrada.getQtdFuncionarioFlPgto()));
	     request.setVlMediaSalarialMes(verificaBigDecimalNulo(entrada.getVlMediaSalarialMes()));
	     request.setCdPagamentoSalarialMes(verificaIntegerNulo(entrada.getCdPagamentoSalarialMes()));
	     request.setCdPagamentoSalarialQzena(verificaIntegerNulo(entrada.getCdPagamentoSalarialQzena()));
	     request.setCdLocalContaSalarial(verificaIntegerNulo(entrada.getCdLocalContaSalarial()));
	     request.setDtReftFlPgto(verificaIntegerNulo(entrada.getDtReftFlPgto()));
		
	     response = getFactoryAdapter().getValidarVinculacaoConvenioContaSalarioPDCAdapter().invokeProcess(request);
	     
		 objSaida.setCodMensagem(response.getCodMensagem());
	     objSaida.setMensagem(response.getMensagem());
	     objSaida.setCdIndicadorAlcadaAgencia(response.getCdIndicadorAlcadaAgencia());
	     objSaida.setCdProdutoServicoOperacao(response.getCdProdutoServicoOperacao());
		
		return objSaida;
	}

    /**
     * Verificar atributos dados basicos.
     * 
     * @param entrada
     *            the entrada
     * @return the verificar atributos dados basicos saida dto
     */
    public VerificarAtributosDadosBasicosSaidaDto verificarAtributosDadosBasicos(
        VerificarAtributosDadosBasicosEntradaDto entrada) {
        VerificarAtributosDadosBasicosSaidaDto saida = new VerificarAtributosDadosBasicosSaidaDto();
        VerificarAtributosDadosBasicosRequest request = new VerificarAtributosDadosBasicosRequest();
        
        request.setCdPessoaJuridicaContrato(verificaLongNulo(entrada.getCdPessoaJuridicaContrato()));
        request.setCdTipoContratoNegocio(verificaIntegerNulo(entrada.getCdTipoContratoNegocio()));
        request.setNrSequenciaContratoNegocio(verificaLongNulo(entrada.getNrSequenciaContratoNegocio()));

        VerificarAtributosDadosBasicosResponse response =
            getFactoryAdapter().getVerificarAtributosDadosBasicosPDCAdapter().invokeProcess(request);

        saida.setCodMensagem(response.getCodMensagem());
        saida.setMensagem(response.getMensagem());
        saida.setCdFormaAutorizacaoPagamentoAlterar(response.getCdFormaAutorizacaoPagamentoAlterar());

        return saida;
    }
    
    public ListarContasExclusaoSaidaDTO listarContasExclusao(ListarContasExclusaoEntradaDTO entrada){
        
        ListarContasExclusaoRequest request = new ListarContasExclusaoRequest();
        request.setNrMaximoOcorrencias(verificaIntegerNulo(entrada.getNrMaximoOcorrencias()));
        request.setCdPessoaJuridicaNegocio(verificaLongNulo(entrada.getCdPessoaJuridicaNegocio()));
        request.setCdTipoContratoNegocio(verificaIntegerNulo(entrada.getCdTipoContratoNegocio()));
        request.setNrSequenciaContratoNegocio(verificaLongNulo(entrada.getNrSequenciaContratoNegocio()));
        request.setCdCorpoCpfCnpj(verificaLongNulo(entrada.getCdCorpoCpfCnpj()));
        request.setCdFilialCnpjParticipante(verificaIntegerNulo(entrada.getCdFilialCnpjParticipante()));
        request.setCdControleCpfCnpj(verificaIntegerNulo(entrada.getCdControleCpfCnpj()));
        
        ListarContasExclusaoResponse response =  
            getFactoryAdapter().getListarContasExclusaoPDCAdapter().invokeProcess(request);
        
        ListarContasExclusaoSaidaDTO saidaDto = new ListarContasExclusaoSaidaDTO(); 
        
        saidaDto.setCodMensagem(response.getCodMensagem());
        saidaDto.setMensagem(response.getMensagem());
        saidaDto.setNumeroConsultas(response.getNumeroConsultas());
        
        ArrayList<ListarContasExclusaoOcorrenciasSaidaDTO> ocorrencias =  
            new ArrayList<ListarContasExclusaoOcorrenciasSaidaDTO>(response.getOcorrenciasCount());
        
        for (int i = 0; i < response.getOcorrenciasCount(); i++) {

            ListarContasExclusaoOcorrenciasSaidaDTO ocorrencia = new ListarContasExclusaoOcorrenciasSaidaDTO();

            ocorrencia.setCdPessoaJuridicaVinculo(response.getOcorrencias(i).getCdPessoaJuridicaVinculo());
            ocorrencia.setCdTipoContratoVinculo(response.getOcorrencias(i).getCdTipoContratoVinculo());
            ocorrencia.setNrSequenciaContratoVinculo(response.getOcorrencias(i).getNrSequenciaContratoVinculo());
            ocorrencia.setCdTipoVinculoContrato(response.getOcorrencias(i).getCdTipoVinculoContrato());
            ocorrencia.setCdCpfCnpj(response.getOcorrencias(i).getCdCpfCnpj());
            ocorrencia.setCdParticipante(response.getOcorrencias(i).getCdParticipante());
            ocorrencia.setNmParticipante(response.getOcorrencias(i).getNmParticipante());
            ocorrencia.setCdBanco(response.getOcorrencias(i).getCdBanco());
            ocorrencia.setDsBanco(response.getOcorrencias(i).getDsBanco());
            ocorrencia.setCdAgencia(response.getOcorrencias(i).getCdAgencia());
            ocorrencia.setCdDigitoAgencia(response.getOcorrencias(i).getCdDigitoAgencia());
            ocorrencia.setDsAgencia(response.getOcorrencias(i).getDsAgencia());
            ocorrencia.setCdConta(response.getOcorrencias(i).getCdConta());
            ocorrencia.setCdDigitoConta(response.getOcorrencias(i).getCdDigitoConta());
            ocorrencia.setCdTipoConta(response.getOcorrencias(i).getCdTipoConta());
            ocorrencia.setDsTipoConta(response.getOcorrencias(i).getDsTipoConta());
            ocorrencia.setCdSituacao(response.getOcorrencias(i).getCdSituacao());
            ocorrencia.setDsSituacao(response.getOcorrencias(i).getDsSituacao());
            ocorrencia.setCdSituacaoVinculacaoConta(response.getOcorrencias(i).getCdSituacaoVinculacaoConta());
            ocorrencia.setDsSituacaoVinculacaoConta(response.getOcorrencias(i).getDsSituacaoVinculacaoConta());

            ocorrencias.add(ocorrencia);
        }
        
        saidaDto.setOcorrencias(ocorrencias);
        
        return saidaDto;
        
    }
}