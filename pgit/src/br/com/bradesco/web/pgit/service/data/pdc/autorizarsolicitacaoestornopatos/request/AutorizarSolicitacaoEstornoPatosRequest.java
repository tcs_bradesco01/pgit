/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.autorizarsolicitacaoestornopatos.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class AutorizarSolicitacaoEstornoPatosRequest.
 * 
 * @version $Revision$ $Date$
 */
public class AutorizarSolicitacaoEstornoPatosRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdTipoAcao
     */
    private int _cdTipoAcao = 0;

    /**
     * keeps track of state for field: _cdTipoAcao
     */
    private boolean _has_cdTipoAcao;

    /**
     * Field _cdSolicitacaoPagamento
     */
    private int _cdSolicitacaoPagamento = 0;

    /**
     * keeps track of state for field: _cdSolicitacaoPagamento
     */
    private boolean _has_cdSolicitacaoPagamento;

    /**
     * Field _nrSolicitacaoPagamento
     */
    private int _nrSolicitacaoPagamento = 0;

    /**
     * keeps track of state for field: _nrSolicitacaoPagamento
     */
    private boolean _has_nrSolicitacaoPagamento;

    /**
     * Field _dsObservacao
     */
    private java.lang.String _dsObservacao;


      //----------------/
     //- Constructors -/
    //----------------/

    public AutorizarSolicitacaoEstornoPatosRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.autorizarsolicitacaoestornopatos.request.AutorizarSolicitacaoEstornoPatosRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdSolicitacaoPagamento
     * 
     */
    public void deleteCdSolicitacaoPagamento()
    {
        this._has_cdSolicitacaoPagamento= false;
    } //-- void deleteCdSolicitacaoPagamento() 

    /**
     * Method deleteCdTipoAcao
     * 
     */
    public void deleteCdTipoAcao()
    {
        this._has_cdTipoAcao= false;
    } //-- void deleteCdTipoAcao() 

    /**
     * Method deleteNrSolicitacaoPagamento
     * 
     */
    public void deleteNrSolicitacaoPagamento()
    {
        this._has_nrSolicitacaoPagamento= false;
    } //-- void deleteNrSolicitacaoPagamento() 

    /**
     * Returns the value of field 'cdSolicitacaoPagamento'.
     * 
     * @return int
     * @return the value of field 'cdSolicitacaoPagamento'.
     */
    public int getCdSolicitacaoPagamento()
    {
        return this._cdSolicitacaoPagamento;
    } //-- int getCdSolicitacaoPagamento() 

    /**
     * Returns the value of field 'cdTipoAcao'.
     * 
     * @return int
     * @return the value of field 'cdTipoAcao'.
     */
    public int getCdTipoAcao()
    {
        return this._cdTipoAcao;
    } //-- int getCdTipoAcao() 

    /**
     * Returns the value of field 'dsObservacao'.
     * 
     * @return String
     * @return the value of field 'dsObservacao'.
     */
    public java.lang.String getDsObservacao()
    {
        return this._dsObservacao;
    } //-- java.lang.String getDsObservacao() 

    /**
     * Returns the value of field 'nrSolicitacaoPagamento'.
     * 
     * @return int
     * @return the value of field 'nrSolicitacaoPagamento'.
     */
    public int getNrSolicitacaoPagamento()
    {
        return this._nrSolicitacaoPagamento;
    } //-- int getNrSolicitacaoPagamento() 

    /**
     * Method hasCdSolicitacaoPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSolicitacaoPagamento()
    {
        return this._has_cdSolicitacaoPagamento;
    } //-- boolean hasCdSolicitacaoPagamento() 

    /**
     * Method hasCdTipoAcao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoAcao()
    {
        return this._has_cdTipoAcao;
    } //-- boolean hasCdTipoAcao() 

    /**
     * Method hasNrSolicitacaoPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSolicitacaoPagamento()
    {
        return this._has_nrSolicitacaoPagamento;
    } //-- boolean hasNrSolicitacaoPagamento() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdSolicitacaoPagamento'.
     * 
     * @param cdSolicitacaoPagamento the value of field
     * 'cdSolicitacaoPagamento'.
     */
    public void setCdSolicitacaoPagamento(int cdSolicitacaoPagamento)
    {
        this._cdSolicitacaoPagamento = cdSolicitacaoPagamento;
        this._has_cdSolicitacaoPagamento = true;
    } //-- void setCdSolicitacaoPagamento(int) 

    /**
     * Sets the value of field 'cdTipoAcao'.
     * 
     * @param cdTipoAcao the value of field 'cdTipoAcao'.
     */
    public void setCdTipoAcao(int cdTipoAcao)
    {
        this._cdTipoAcao = cdTipoAcao;
        this._has_cdTipoAcao = true;
    } //-- void setCdTipoAcao(int) 

    /**
     * Sets the value of field 'dsObservacao'.
     * 
     * @param dsObservacao the value of field 'dsObservacao'.
     */
    public void setDsObservacao(java.lang.String dsObservacao)
    {
        this._dsObservacao = dsObservacao;
    } //-- void setDsObservacao(java.lang.String) 

    /**
     * Sets the value of field 'nrSolicitacaoPagamento'.
     * 
     * @param nrSolicitacaoPagamento the value of field
     * 'nrSolicitacaoPagamento'.
     */
    public void setNrSolicitacaoPagamento(int nrSolicitacaoPagamento)
    {
        this._nrSolicitacaoPagamento = nrSolicitacaoPagamento;
        this._has_nrSolicitacaoPagamento = true;
    } //-- void setNrSolicitacaoPagamento(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return AutorizarSolicitacaoEstornoPatosRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.autorizarsolicitacaoestornopatos.request.AutorizarSolicitacaoEstornoPatosRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.autorizarsolicitacaoestornopatos.request.AutorizarSolicitacaoEstornoPatosRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.autorizarsolicitacaoestornopatos.request.AutorizarSolicitacaoEstornoPatosRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.autorizarsolicitacaoestornopatos.request.AutorizarSolicitacaoEstornoPatosRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
