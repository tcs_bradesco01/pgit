/*
 * Nome: br.com.bradesco.web.pgit.service.report.mantercontrato
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.report.mantercontrato;

import br.com.bradesco.web.pgit.service.report.base.BasicPdfReport;

import com.lowagie.text.Chunk;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.Paragraph;
import com.lowagie.text.pdf.PdfCell;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;

/**
 * Nome: ContratoRodapeReport
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ContratoRodapeReport extends BasicPdfReport {

	/** Atributo fTexto. */
	private Font fTexto = new Font(Font.DEFAULTSIZE, 10, Font.NORMAL);
	
	/** Atributo fTextoTabela. */
	private Font fTextoTabela = new Font(Font.TIMES_ROMAN, 11, Font.BOLD);
	
	/**
	 * Contrato rodape report.
	 */
	public ContratoRodapeReport() {
		super();
		
	}
	
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.report.base.BasicPdfReport#popularPdf(com.lowagie.text.Document)
	 */
	@Override
	public void popularPdf(Document document) throws DocumentException {
		
		PdfPTable tabela = new PdfPTable(2);
		tabela.setWidthPercentage(100f);
		
		Paragraph pLinha1Dir = new Paragraph(new Chunk("Al� Bradesco", fTextoTabela));
		pLinha1Dir.setAlignment(Element.ALIGN_CENTER);
		Paragraph pLinha1Esq = new Paragraph(new Chunk("Ouvidoria - 0800 727 9933", fTextoTabela));
		pLinha1Esq.setAlignment(Element.ALIGN_CENTER);
		Paragraph pLinha2Dir = new Paragraph(new Chunk("SAC - Servi�o de Apoio ao Cliente Cancelamentos,", fTextoTabela));
		pLinha2Dir.setAlignment(Element.ALIGN_CENTER);
		Paragraph pLinha2Esq = new Paragraph(new Chunk("Atendimento de segunda a sexta-feira das", fTexto));
		pLinha2Esq.setAlignment(Element.ALIGN_CENTER);
		Paragraph pLinha3Dir = new Paragraph(new Chunk("Reclama��es e Informa��es - 0800 704 8383", fTexto));
		pLinha3Dir.setAlignment(Element.ALIGN_CENTER);
		Paragraph pLinha3Esq = new Paragraph(new Chunk("8h �s 18h, exceto feriados.", fTexto));
		pLinha3Esq.setAlignment(Element.ALIGN_CENTER);
		Paragraph pLinha4Dir = new Paragraph(new Chunk("Deficiente Auditivo ou de Fala - 0800 722 0099", fTexto));
		pLinha4Dir.setAlignment(Element.ALIGN_CENTER);
		Paragraph pLinha5Dir = new Paragraph(new Chunk("Atendimento 24 horas, 7 dias por semana.", fTexto));
		pLinha5Dir.setAlignment(Element.ALIGN_CENTER);
		Paragraph pLinha6Dir = new Paragraph(new Chunk(" ", fTexto));
		pLinha6Dir.setAlignment(Element.ALIGN_CENTER);
		
		PdfPCell celula1 = new PdfPCell();
		celula1.setBorder(PdfCell.BOX);
		
		celula1.addElement(pLinha1Dir);
		celula1.addElement(pLinha2Dir);
		celula1.addElement(pLinha3Dir);
		celula1.addElement(pLinha4Dir);
		celula1.addElement(pLinha5Dir);
		celula1.addElement(pLinha6Dir);
		
		
		PdfPCell celula2 = new PdfPCell();
		celula2.setBorder(PdfCell.BOX);
		
		celula2.addElement(pLinha1Esq);
		celula2.addElement(pLinha2Esq);
		celula2.addElement(pLinha3Esq);
		
		tabela.addCell(celula1);
		tabela.addCell(celula2);
		
		document.add(tabela);
		
	}
	
		
}
