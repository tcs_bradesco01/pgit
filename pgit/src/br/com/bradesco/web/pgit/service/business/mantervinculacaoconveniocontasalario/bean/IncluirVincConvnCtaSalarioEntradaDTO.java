/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantervinculacaoconveniocontasalario.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantervinculacaoconveniocontasalario.bean;

/**
 * Nome: IncluirVincConvnCtaSalarioEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class IncluirVincConvnCtaSalarioEntradaDTO {

	/** Atributo cdPessoaJuridica. */
	private Long cdPessoaJuridica;
	
	/** Atributo cdTipoContrato. */
	private Integer cdTipoContrato;
	
	/** Atributo nrSequenciaContrato. */
	private Long nrSequenciaContrato;
	
	/** Atributo cdConveCtaSalarial. */
	private Long cdConveCtaSalarial;
	
	/** Atributo cdConvenNovo. */
	private Integer cdConvenNovo;
	
	/** Atributo cdCnpjEmp. */
	private Long cdCnpjEmp;
	
	/** Atributo cdFilialEmp. */
	private Integer cdFilialEmp;
	
	/** Atributo cdCtrlCnpjEmp. */
	private Integer cdCtrlCnpjEmp;
	
	/** Atributo dsConvCtaEmp. */
	private String dsConvCtaEmp;
	
	/** Atributo cdBcoCtaEmp. */
	private Integer cdBcoCtaEmp;
	
	/** Atributo cdAgencCtaEmp. */
	private Integer cdAgencCtaEmp;
	
	/** Atributo cdNumeroCtaDest. */
	private Long cdNumeroCtaDest;
	
	/** Atributo cdDigitoCtaEmp. */
	private String cdDigitoCtaEmp;

	/**
	 * Get: cdPessoaJuridica.
	 *
	 * @return cdPessoaJuridica
	 */
	public Long getCdPessoaJuridica() {
		return cdPessoaJuridica;
	}

	/**
	 * Set: cdPessoaJuridica.
	 *
	 * @param cdPessoaJuridica the cd pessoa juridica
	 */
	public void setCdPessoaJuridica(Long cdPessoaJuridica) {
		this.cdPessoaJuridica = cdPessoaJuridica;
	}

	/**
	 * Get: cdTipoContrato.
	 *
	 * @return cdTipoContrato
	 */
	public Integer getCdTipoContrato() {
		return cdTipoContrato;
	}

	/**
	 * Set: cdTipoContrato.
	 *
	 * @param cdTipoContrato the cd tipo contrato
	 */
	public void setCdTipoContrato(Integer cdTipoContrato) {
		this.cdTipoContrato = cdTipoContrato;
	}

	/**
	 * Get: nrSequenciaContrato.
	 *
	 * @return nrSequenciaContrato
	 */
	public Long getNrSequenciaContrato() {
		return nrSequenciaContrato;
	}

	/**
	 * Set: nrSequenciaContrato.
	 *
	 * @param nrSequenciaContrato the nr sequencia contrato
	 */
	public void setNrSequenciaContrato(Long nrSequenciaContrato) {
		this.nrSequenciaContrato = nrSequenciaContrato;
	}

	/**
	 * Get: cdConveCtaSalarial.
	 *
	 * @return cdConveCtaSalarial
	 */
	public Long getCdConveCtaSalarial() {
		return cdConveCtaSalarial;
	}

	/**
	 * Set: cdConveCtaSalarial.
	 *
	 * @param cdConveCtaSalarial the cd conve cta salarial
	 */
	public void setCdConveCtaSalarial(Long cdConveCtaSalarial) {
		this.cdConveCtaSalarial = cdConveCtaSalarial;
	}

	/**
	 * Get: cdConvenNovo.
	 *
	 * @return cdConvenNovo
	 */
	public Integer getCdConvenNovo() {
		return cdConvenNovo;
	}

	/**
	 * Set: cdConvenNovo.
	 *
	 * @param cdConvenNovo the cd conven novo
	 */
	public void setCdConvenNovo(Integer cdConvenNovo) {
		this.cdConvenNovo = cdConvenNovo;
	}

	/**
	 * Get: cdCnpjEmp.
	 *
	 * @return cdCnpjEmp
	 */
	public Long getCdCnpjEmp() {
		return cdCnpjEmp;
	}

	/**
	 * Set: cdCnpjEmp.
	 *
	 * @param cdCnpjEmp the cd cnpj emp
	 */
	public void setCdCnpjEmp(Long cdCnpjEmp) {
		this.cdCnpjEmp = cdCnpjEmp;
	}

	/**
	 * Get: cdFilialEmp.
	 *
	 * @return cdFilialEmp
	 */
	public Integer getCdFilialEmp() {
		return cdFilialEmp;
	}

	/**
	 * Set: cdFilialEmp.
	 *
	 * @param cdFilialEmp the cd filial emp
	 */
	public void setCdFilialEmp(Integer cdFilialEmp) {
		this.cdFilialEmp = cdFilialEmp;
	}

	/**
	 * Get: cdCtrlCnpjEmp.
	 *
	 * @return cdCtrlCnpjEmp
	 */
	public Integer getCdCtrlCnpjEmp() {
		return cdCtrlCnpjEmp;
	}

	/**
	 * Set: cdCtrlCnpjEmp.
	 *
	 * @param cdCtrlCnpjEmp the cd ctrl cnpj emp
	 */
	public void setCdCtrlCnpjEmp(Integer cdCtrlCnpjEmp) {
		this.cdCtrlCnpjEmp = cdCtrlCnpjEmp;
	}

	/**
	 * Get: dsConvCtaEmp.
	 *
	 * @return dsConvCtaEmp
	 */
	public String getDsConvCtaEmp() {
		return dsConvCtaEmp;
	}

	/**
	 * Set: dsConvCtaEmp.
	 *
	 * @param dsConvCtaEmp the ds conv cta emp
	 */
	public void setDsConvCtaEmp(String dsConvCtaEmp) {
		this.dsConvCtaEmp = dsConvCtaEmp;
	}

	/**
	 * Get: cdBcoCtaEmp.
	 *
	 * @return cdBcoCtaEmp
	 */
	public Integer getCdBcoCtaEmp() {
		return cdBcoCtaEmp;
	}

	/**
	 * Set: cdBcoCtaEmp.
	 *
	 * @param cdBcoCtaEmp the cd bco cta emp
	 */
	public void setCdBcoCtaEmp(Integer cdBcoCtaEmp) {
		this.cdBcoCtaEmp = cdBcoCtaEmp;
	}

	/**
	 * Get: cdAgencCtaEmp.
	 *
	 * @return cdAgencCtaEmp
	 */
	public Integer getCdAgencCtaEmp() {
		return cdAgencCtaEmp;
	}

	/**
	 * Set: cdAgencCtaEmp.
	 *
	 * @param cdAgencCtaEmp the cd agenc cta emp
	 */
	public void setCdAgencCtaEmp(Integer cdAgencCtaEmp) {
		this.cdAgencCtaEmp = cdAgencCtaEmp;
	}

	/**
	 * Get: cdNumeroCtaDest.
	 *
	 * @return cdNumeroCtaDest
	 */
	public Long getCdNumeroCtaDest() {
		return cdNumeroCtaDest;
	}

	/**
	 * Set: cdNumeroCtaDest.
	 *
	 * @param cdNumeroCtaDest the cd numero cta dest
	 */
	public void setCdNumeroCtaDest(Long cdNumeroCtaDest) {
		this.cdNumeroCtaDest = cdNumeroCtaDest;
	}

	/**
	 * Get: cdDigitoCtaEmp.
	 *
	 * @return cdDigitoCtaEmp
	 */
	public String getCdDigitoCtaEmp() {
		return cdDigitoCtaEmp;
	}

	/**
	 * Set: cdDigitoCtaEmp.
	 *
	 * @param cdDigitoCtaEmp the cd digito cta emp
	 */
	public void setCdDigitoCtaEmp(String cdDigitoCtaEmp) {
		this.cdDigitoCtaEmp = cdDigitoCtaEmp;
	}
}