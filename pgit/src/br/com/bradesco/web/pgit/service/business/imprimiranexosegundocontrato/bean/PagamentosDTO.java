/*
 * Nome: br.com.bradesco.web.pgit.service.business.imprimiranexosegundocontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.imprimiranexosegundocontrato.bean;

// TODO: Auto-generated Javadoc
/**
 * Nome: PagamentosDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class PagamentosDTO {

	// Dados do tipo de pagamento
	/** Atributo dsServicoTipoServico. */
	private String dsServicoTipoServico;
	
	/** Atributo dsFormaEnvioPagamento. */
	private String dsFormaEnvioPagamento;
	
	/** Atributo dsServicoFormaAutorizacaoPagamento. */
	private String dsServicoFormaAutorizacaoPagamento;
	
	/** Atributo dsIndicadorGeradorRetorno. */
	private String dsIndicadorGeradorRetorno;
	
	/** Atributo dsServicoIndicadorRetornoSeparado. */
	private String dsServicoIndicadorRetornoSeparado;
	
	/** Atributo cdServicoIndicadorListaDebito. */
	private String cdServicoIndicadorListaDebito;
	
	/** Atributo dsServicoListaDebito. */
	private String dsServicoListaDebito;
	
	/** Atributo dsServicoTratamentoListaDebito. */
	private String dsServicoTratamentoListaDebito;
	
	/** Atributo dsServicoAutorizacaoComplementoAgencia. */
	private String dsServicoAutorizacaoComplementoAgencia;
	
	/** Atributo dsServicoPreAutorizacaoCliente. */
	private String dsServicoPreAutorizacaoCliente;
	
	/** Atributo dsTibutoAgendadoDebitoVeicular. */
	private String dsTibutoAgendadoDebitoVeicular;
	
	/** Atributo dsTdvTipoConsultaProposta. */
	private String dsTdvTipoConsultaProposta;
	
	/** Atributo dsTdvTipoTratamentoValor. */
	private String dsTdvTipoTratamentoValor;
	
	/** Atributo dsSalarioEmissaoAntecipadoCartao. */
	private String dsSalarioEmissaoAntecipadoCartao;
	
	/** Atributo cdSalarioQuantidadeLimiteSolicitacaoCartao. */
	private String cdSalarioQuantidadeLimiteSolicitacaoCartao;
	
	/** Atributo dsSalarioAberturaContaExpressa. */
	private String dsSalarioAberturaContaExpressa;
	
	/** Atributo dsBeneficioInformadoAgenciaConta. */
	private String dsBeneficioInformadoAgenciaConta;
	
	/** Atributo dsBeneficioTipoIdentificacaoBeneficio. */
	private String dsBeneficioTipoIdentificacaoBeneficio;
	
	/** Atributo dsBeneficioUtilizacaoCadastroOrganizacao. */
	private String dsBeneficioUtilizacaoCadastroOrganizacao;
	
	/** Atributo dsBeneficioUtilizacaoCadastroProcuradores. */
	private String dsBeneficioUtilizacaoCadastroProcuradores;
	
	/** Atributo dsBeneficioPossuiExpiracaoCredito. */
	private String dsBeneficioPossuiExpiracaoCredito;
	
	/** Atributo cdBeneficioQuantidadeDiasExpiracaoCredito. */
	private String cdBeneficioQuantidadeDiasExpiracaoCredito;
	
	/** Atributo dsBprTipoAcao. */
	private String dsBprTipoAcao;
	
	/** Atributo qtBprMesProvisorio. */
	private String qtBprMesProvisorio;
	
	/** Atributo cdBprIndicadorEmissaoAviso. */
	private String cdBprIndicadorEmissaoAviso;
	
	/** Atributo qtBprDiaAnteriorAviso. */
	private String qtBprDiaAnteriorAviso;
	
	/** Atributo qtBprDiaAnteriorVencimento. */
	private String qtBprDiaAnteriorVencimento;
	
	/** Atributo cdBprUtilizadoMensagemPersonalizada. */
	private String cdBprUtilizadoMensagemPersonalizada;
	
	/** Atributo dsBprDestino. */
	private String dsBprDestino;
	
	/** Atributo dsBprCadastroEnderecoUtilizado. */
	private String dsBprCadastroEnderecoUtilizado;
	
	/** Atributo exibeDebitoVeiculo. */
	private String exibeDebitoVeiculo;

	// Dados da modalidade (Cr�dito em Conta)
	/** Atributo dsCretipoServico. */
	private String dsCretipoServico;
	
	/** Atributo dsCreTipoConsultaSaldo. */
	private String dsCreTipoConsultaSaldo;
	
	/** Atributo dsCreTipoProcessamento. */
	private String dsCreTipoProcessamento;
	
	/** Atributo dsCreTipoTratamentoFeriado. */
	private String dsCreTipoTratamentoFeriado;
	
	/** Atributo dsCreTipoRejeicaoEfetivacao. */
	private String dsCreTipoRejeicaoEfetivacao;
	
	/** Atributo dsCreTipoRejeicaoAgendada. */
	private String dsCreTipoRejeicaoAgendada;
	
	/** Atributo cdCreIndicadorPercentualMaximo. */
	private String cdCreIndicadorPercentualMaximo;
	
	/** Atributo pcCreMaximoInconsistente. */
	private String pcCreMaximoInconsistente;
	
	/** Atributo cdCreIndicadorQuantidadeMaxima. */
	private String cdCreIndicadorQuantidadeMaxima;
	
	/** Atributo qtCreMaximaInconsistencia. */
	private String qtCreMaximaInconsistencia;
	
	/** Atributo cdCreIndicadorValorMaximo. */
	private String cdCreIndicadorValorMaximo;
	
	/** Atributo vlCreMaximoFavorecidoNao. */
	private String vlCreMaximoFavorecidoNao;
	
	/** Atributo dsCrePrioridadeDebito. */
	private String dsCrePrioridadeDebito;
	
	/** Atributo cdCreIndicadorValorDia. */
	private String cdCreIndicadorValorDia;
	
	/** Atributo vlCreLimiteDiario. */
	private String vlCreLimiteDiario;
	
	/** Atributo cdCreIndicadorValorIndividual. */
	private String cdCreIndicadorValorIndividual;
	
	/** Atributo vlCreLimiteIndividual. */
	private String vlCreLimiteIndividual;
	
	/** Atributo cdCreIndicadorQuantidadeFloating. */
	private String cdCreIndicadorQuantidadeFloating;
	
	/** Atributo qtCreDiaFloating. */
	private String qtCreDiaFloating;
	
	/** Atributo cdCreIndicadorCadastroFavorecido. */
	private String cdCreIndicadorCadastroFavorecido;
	
	/** Atributo cdCreUtilizacaoCadastroFavorecido. */
	private String cdCreUtilizacaoCadastroFavorecido;
	
	/** Atributo cdCreIndicadorQuantidadeRepique. */
	private String cdCreIndicadorQuantidadeRepique;
	
	/** Atributo qtCreDiaRepique. */
	private String qtCreDiaRepique;
	
	/** Atributo dsCreTipoConsistenciaFavorecido. */
	private String dsCreTipoConsistenciaFavorecido;

	// Dados da modalidade (Titulos)
	/** Atributo dsFtiTipoTitulo. */
	private String dsFtiTipoTitulo;
	
	/** Atributo dsFtiTipoConsultaSaldo. */
	private String dsFtiTipoConsultaSaldo;
	
	/** Atributo dsFtiTipoProcessamento. */
	private String dsFtiTipoProcessamento;
	
	/** Atributo dsFtiTipoTratamentoFeriado. */
	private String dsFtiTipoTratamentoFeriado;
	
	/** Atributo dsFtiTipoRejeicaoEfetivacao. */
	private String dsFtiTipoRejeicaoEfetivacao;
	
	/** Atributo dsFtiTipoRejeicaoAgendamento. */
	private String dsFtiTipoRejeicaoAgendamento;
	
	/** Atributo cdFtiIndicadorPercentualMaximo. */
	private String cdFtiIndicadorPercentualMaximo;
	
	/** Atributo pcFtiMaximoInconsistente. */
	private String pcFtiMaximoInconsistente;
	
	/** Atributo cdFtiIndicadorQuantidadeMaxima. */
	private String cdFtiIndicadorQuantidadeMaxima;
	
	/** Atributo qtFtiMaximaInconsistencia. */
	private String qtFtiMaximaInconsistencia;
	
	/** Atributo cdFtiIndicadorValorMaximo. */
	private String cdFtiIndicadorValorMaximo;
	
	/** Atributo vlFtiMaximoFavorecidoNao. */
	private String vlFtiMaximoFavorecidoNao;
	
	/** Atributo dsFtiPrioridadeDebito. */
	private String dsFtiPrioridadeDebito;
	
	/** Atributo cdFtiIndicadorValorDia. */
	private String cdFtiIndicadorValorDia;
	
	/** Atributo vlFtiLimiteDiario. */
	private String vlFtiLimiteDiario;
	
	/** Atributo cdFtiIndicadorValorIndividual. */
	private String cdFtiIndicadorValorIndividual;
	
	/** Atributo vlFtiLimiteIndividual. */
	private String vlFtiLimiteIndividual;
	
	/** Atributo cdFtiIndicadorQuantidadeFloating. */
	private String cdFtiIndicadorQuantidadeFloating;
	
	/** Atributo qtFtiDiaFloating. */
	private String qtFtiDiaFloating;
	
	/** Atributo cdFtiIndicadorCadastroFavorecido. */
	private String cdFtiIndicadorCadastroFavorecido;
	
	/** Atributo cdFtiUtilizadoFavorecido. */
	private String cdFtiUtilizadoFavorecido;
	
	/** Atributo cdFtiIndicadorQuantidadeRepique. */
	private String cdFtiIndicadorQuantidadeRepique;
	
	/** Atributo qtFtiDiaRepique. */
	private String qtFtiDiaRepique;
	
	/** Atributo dsTIpoConsultaSaldoSuperior. */
	private String dsTIpoConsultaSaldoSuperior;

	// Dados da modalidade (Modularidades)
	/** Atributo dsModTipoServico. */
	private String dsModTipoServico;
	
	/** Atributo dsModTipoOutrosServico. */
	private String dsModTipoOutrosServico;
	
	/** Atributo dsModTipoModalidade. */
	private String dsModTipoModalidade;
	
	/** Atributo dsModTipoConsultaSaldo. */
	private String dsModTipoConsultaSaldo;
	
	/** Atributo dsModTipoProcessamento. */
	private String dsModTipoProcessamento;
	
	/** Atributo dsModTipoTratamentoFeriado. */
	private String dsModTipoTratamentoFeriado;
	
	/** Atributo dsModTipoRejeicaoEfetivacao. */
	private String dsModTipoRejeicaoEfetivacao;
	
	/** Atributo dsModTipoRejeicaoAgendado. */
	private String dsModTipoRejeicaoAgendado;
	
	/** Atributo cdModIndicadorPercentualMaximo. */
	private String cdModIndicadorPercentualMaximo;
	
	/** Atributo cdModIndicadorPercentualMaximoInconsistente. */
	private String cdModIndicadorPercentualMaximoInconsistente;
	
	/** Atributo cdModIndicadorQuantidadeMaxima. */
	private String cdModIndicadorQuantidadeMaxima;
	
	/** Atributo qtModMaximoInconsistencia. */
	private String qtModMaximoInconsistencia;
	
	/** Atributo cdModIndicadorValorMaximo. */
	private String cdModIndicadorValorMaximo;
	
	/** Atributo vlModMaximoFavorecidoNao. */
	private String vlModMaximoFavorecidoNao;
	
	/** Atributo dsModPrioridadeDebito. */
	private String dsModPrioridadeDebito;
	
	/** Atributo cdModIndicadorValorDia. */
	private String cdModIndicadorValorDia;
	
	/** Atributo vlModLimiteDiario. */
	private String vlModLimiteDiario;
	
	/** Atributo cdModIndicadorValorIndividual. */
	private String cdModIndicadorValorIndividual;
	
	/** Atributo vlModLimiteIndividual. */
	private String vlModLimiteIndividual;
	
	/** Atributo cdModIndicadorQuantidadeFloating. */
	private String cdModIndicadorQuantidadeFloating;
	
	/** Atributo qtModDiaFloating. */
	private String qtModDiaFloating;
	
	/** Atributo cdModIndicadorCadastroFavorecido. */
	private String cdModIndicadorCadastroFavorecido;
	
	/** Atributo cdModUtilizacaoCadastroFavorecido. */
	private String cdModUtilizacaoCadastroFavorecido;
	
	/** Atributo cdModIndicadorQuantidadeRepique. */
	private String cdModIndicadorQuantidadeRepique;
	
	/** Atributo qtModDiaRepique. */
	private String qtModDiaRepique;

	// Dados Ordem de Pagamentos
	/** Atributo dsOpgTipoServico. */
	private String dsOpgTipoServico;
	
	/** Atributo dsOpgTipoConsultaSaldo. */
	private String dsOpgTipoConsultaSaldo;
	
	/** Atributo dsOpgTipoProcessamento. */
	private String dsOpgTipoProcessamento;
	
	/** Atributo dsOpgTipoTratamentoFeriado. */
	private String dsOpgTipoTratamentoFeriado;
	
	/** Atributo dsOpgTipoRejeicaoEfetivacao. */
	private String dsOpgTipoRejeicaoEfetivacao;
	
	/** Atributo dsOpgTiporejeicaoAgendado. */
	private String dsOpgTiporejeicaoAgendado;
	
	/** Atributo cdOpgIndicadorPercentualMaximo. */
	private String cdOpgIndicadorPercentualMaximo;
	
	/** Atributo pcOpgMaximoInconsistencia. */
	private String pcOpgMaximoInconsistencia;
	
	/** Atributo cdOpgIndicadorQuantidadeMaximo. */
	private String cdOpgIndicadorQuantidadeMaximo;
	
	/** Atributo qtOpgMaximoInconsistencia. */
	private String qtOpgMaximoInconsistencia;
	
	/** Atributo cdOpgIndicadorValorDia. */
	private String cdOpgIndicadorValorDia;
	
	/** Atributo vlOpgLimiteDiario. */
	private String vlOpgLimiteDiario;
	
	/** Atributo cdOpgIndicadorValorIndividual. */
	private String cdOpgIndicadorValorIndividual;
	
	/** Atributo vlOpgLimiteIndividual. */
	private String vlOpgLimiteIndividual;
	
	/** Atributo cdOpgIndicadorQuantidadeFloating. */
	private String cdOpgIndicadorQuantidadeFloating;
	
	/** Atributo qtOpgDiaFloating. */
	private String qtOpgDiaFloating;
	
	/** Atributo dsOpgPrioridadeDebito. */
	private String dsOpgPrioridadeDebito;
	
	/** Atributo cdOpgIndicadorValorMaximo. */
	private String cdOpgIndicadorValorMaximo;
	
	/** Atributo vlOpgMaximoFavorecidoNao. */
	private String vlOpgMaximoFavorecidoNao;
	
	/** Atributo cdOpgIndicadorCadastroFavorecido. */
	private String cdOpgIndicadorCadastroFavorecido;
	
	/** Atributo cdOpgUtilizacaoCadastroFavorecido. */
	private String cdOpgUtilizacaoCadastroFavorecido;
	
	/** Atributo dsOpgOcorrenciasDebito. */
	private String dsOpgOcorrenciasDebito;
	
	/** Atributo dsOpgTipoInscricao. */
	private String dsOpgTipoInscricao;
	
	/** Atributo cdOpgIndicadorQuantidadeRepique. */
	private String cdOpgIndicadorQuantidadeRepique;
	
	/** Atributo qtOpgDiaRepique. */
	private String qtOpgDiaRepique;
	
	/** Atributo qtOpgDiaExpiracao. */
	private String qtOpgDiaExpiracao;

	// Dados (Rastreamento) - vem de ImprimirAnexoSegundoContratoSaidaDTO
	/** Atributo dsFraTipoRastreabilidade. */
	private String dsFraTipoRastreabilidade;
	
	/** Atributo dsFraRastreabilidadeTerceiro. */
	private String dsFraRastreabilidadeTerceiro;
	
	/** Atributo dsFraRastreabilidadeNota. */
	private String dsFraRastreabilidadeNota;
	
	/** Atributo dsFraCaptacaoTitulo. */
	private String dsFraCaptacaoTitulo;
	
	/** Atributo dsFraAgendaCliente. */
	private String dsFraAgendaCliente;
	
	/** Atributo dsFraAgendaFilial. */
	private String dsFraAgendaFilial;
	
	/** Atributo dsFraBloqueioEmissao. */
	private String dsFraBloqueioEmissao;
	
	/** Atributo dtFraInicioBloqueio. */
	private String dtFraInicioBloqueio;
	
	/** Atributo dtFraInicioRastreabilidade. */
	private String dtFraInicioRastreabilidade;
	
	/** Atributo dsFraAdesaoSacado. */
	private String dsFraAdesaoSacado;

	// Dados TriTributo
	/** Atributo dsTriTipoTributo. */
	private String dsTriTipoTributo;
	
	/** Atributo dsTriTipoConsultaSaldo. */
	private String dsTriTipoConsultaSaldo;
	
	/** Atributo dsTriTipoProcessamento. */
	private String dsTriTipoProcessamento;
	
	/** Atributo dsTriTipoTratamentoFeriado. */
	private String dsTriTipoTratamentoFeriado;
	
	/** Atributo dsTriTipoRejeicaoEfetivacao. */
	private String dsTriTipoRejeicaoEfetivacao;
	
	/** Atributo dsTriTipoRejeicaoAgendado. */
	private String dsTriTipoRejeicaoAgendado;
	
	/** Atributo cdTriIndicadorValorMaximo. */
	private String cdTriIndicadorValorMaximo;
	
	/** Atributo vlTriMaximoFavorecidoNao. */
	private String vlTriMaximoFavorecidoNao;
	
	/** Atributo dsTriPrioridadeDebito. */
	private String dsTriPrioridadeDebito;
	
	/** Atributo cdTriIndicadorValorDia. */
	private String cdTriIndicadorValorDia;
	
	/** Atributo vlTriLimiteDiario. */
	private String vlTriLimiteDiario;
	
	/** Atributo cdTriIndicadorValorIndividual. */
	private String cdTriIndicadorValorIndividual;
	
	/** Atributo vlTriLimiteIndividual. */
	private String vlTriLimiteIndividual;
	
	/** Atributo cdTriIndicadorQuantidadeFloating. */
	private String cdTriIndicadorQuantidadeFloating;
	
	/** Atributo qttriDiaFloating. */
	private String qttriDiaFloating;
	
	/** Atributo cdTriIndicadorCadastroFavorecido. */
	private String cdTriIndicadorCadastroFavorecido;
	
	/** Atributo cdTriUtilizacaoCadastroFavorecido. */
	private String cdTriUtilizacaoCadastroFavorecido;
	
	/** Atributo cdTriIndicadorQuantidadeRepique. */
	private String cdTriIndicadorQuantidadeRepique;
	
	/** Atributo qtTriDiaRepique. */
	private String qtTriDiaRepique;
	
	 /** The ds servico tipo data float. */
    private String dsServicoTipoDataFloat;
    
    /** The cd servico floating. */
    private String cdServicoFloating;
    
    /** The cd servico tipo consolidado. */
    private String cdServicoTipoConsolidado;
   
   /** The fti pagamento vencido. */
   private String ftiPagamentoVencido;			
   
   /** The fti pagamento menor. */
   private String ftiPagamentoMenor;		
   
   /** The fti ds tipo cons favorecido. */
   private String ftiDsTipoConsFavorecido;	
   
   /** The fti cferi loc. */
   private String ftiCferiLoc;					
   
   /** The fti quantidade max vencido. */
   private Integer ftiQuantidadeMaxVencido;		
   
   /** The cd cre cferi loc. */
   private String cdCreCferiLoc;				
   
   /** The cd cre contrato transf. */
   private String cdCreContratoTransf;		
   
   /** The cd lcto pgmd. */
   private String cdLctoPgmd;				
   
   /** The ds tipo cons favorecido. */
   private String dsTipoConsFavorecido;	
   
   /** The cd feri loc. */
   private String cdFeriLoc;				
   
   /** The o pg emissao. */
   private String oPgEmissao;			
   
   /** The ds modalidade cferi loc. */
   private String dsModalidadeCferiLoc;		
   
   /** The ds mod tipo cons favorecido. */
   private String dsModTipoConsFavorecido;  
   
   /** The ds tri tipo cons favorecido. */
   private String dsTriTipoConsFavorecido;  
   
   /** The cd tri cferi loc. */
   private String cdTriCferiLoc;
   
   /** The cd tri cferi loc. */
   private String exibeDsFtiTipoTitulo;	


	/**
	 * Get: dsServicoTipoServico.
	 *
	 * @return dsServicoTipoServico
	 */
	public String getDsServicoTipoServico() {
		return dsServicoTipoServico;
	}

	/**
	 * Set: dsServicoTipoServico.
	 *
	 * @param dsServicoTipoServico the ds servico tipo servico
	 */
	public void setDsServicoTipoServico(String dsServicoTipoServico) {
		this.dsServicoTipoServico = dsServicoTipoServico;
	}

	/**
	 * Get: dsFormaEnvioPagamento.
	 *
	 * @return dsFormaEnvioPagamento
	 */
	public String getDsFormaEnvioPagamento() {
		return dsFormaEnvioPagamento;
	}

	/**
	 * Set: dsFormaEnvioPagamento.
	 *
	 * @param dsFormaEnvioPagamento the ds forma envio pagamento
	 */
	public void setDsFormaEnvioPagamento(String dsFormaEnvioPagamento) {
		this.dsFormaEnvioPagamento = dsFormaEnvioPagamento;
	}

	/**
	 * Get: dsServicoFormaAutorizacaoPagamento.
	 *
	 * @return dsServicoFormaAutorizacaoPagamento
	 */
	public String getDsServicoFormaAutorizacaoPagamento() {
		return dsServicoFormaAutorizacaoPagamento;
	}

	/**
	 * Set: dsServicoFormaAutorizacaoPagamento.
	 *
	 * @param dsServicoFormaAutorizacaoPagamento the ds servico forma autorizacao pagamento
	 */
	public void setDsServicoFormaAutorizacaoPagamento(
			String dsServicoFormaAutorizacaoPagamento) {
		this.dsServicoFormaAutorizacaoPagamento = dsServicoFormaAutorizacaoPagamento;
	}

	/**
	 * Get: dsIndicadorGeradorRetorno.
	 *
	 * @return dsIndicadorGeradorRetorno
	 */
	public String getDsIndicadorGeradorRetorno() {
		return dsIndicadorGeradorRetorno;
	}

	/**
	 * Set: dsIndicadorGeradorRetorno.
	 *
	 * @param dsIndicadorGeradorRetorno the ds indicador gerador retorno
	 */
	public void setDsIndicadorGeradorRetorno(String dsIndicadorGeradorRetorno) {
		this.dsIndicadorGeradorRetorno = dsIndicadorGeradorRetorno;
	}

	/**
	 * Get: dsServicoIndicadorRetornoSeparado.
	 *
	 * @return dsServicoIndicadorRetornoSeparado
	 */
	public String getDsServicoIndicadorRetornoSeparado() {
		return dsServicoIndicadorRetornoSeparado;
	}

	/**
	 * Set: dsServicoIndicadorRetornoSeparado.
	 *
	 * @param dsServicoIndicadorRetornoSeparado the ds servico indicador retorno separado
	 */
	public void setDsServicoIndicadorRetornoSeparado(
			String dsServicoIndicadorRetornoSeparado) {
		this.dsServicoIndicadorRetornoSeparado = dsServicoIndicadorRetornoSeparado;
	}

	/**
	 * Get: cdServicoIndicadorListaDebito.
	 *
	 * @return cdServicoIndicadorListaDebito
	 */
	public String getCdServicoIndicadorListaDebito() {
		return cdServicoIndicadorListaDebito;
	}

	/**
	 * Set: cdServicoIndicadorListaDebito.
	 *
	 * @param cdServicoIndicadorListaDebito the cd servico indicador lista debito
	 */
	public void setCdServicoIndicadorListaDebito(
			String cdServicoIndicadorListaDebito) {
		this.cdServicoIndicadorListaDebito = cdServicoIndicadorListaDebito;
	}

	/**
	 * Get: dsServicoListaDebito.
	 *
	 * @return dsServicoListaDebito
	 */
	public String getDsServicoListaDebito() {
		return dsServicoListaDebito;
	}

	/**
	 * Set: dsServicoListaDebito.
	 *
	 * @param dsServicoListaDebito the ds servico lista debito
	 */
	public void setDsServicoListaDebito(String dsServicoListaDebito) {
		this.dsServicoListaDebito = dsServicoListaDebito;
	}

	/**
	 * Get: dsServicoTratamentoListaDebito.
	 *
	 * @return dsServicoTratamentoListaDebito
	 */
	public String getDsServicoTratamentoListaDebito() {
		return dsServicoTratamentoListaDebito;
	}

	/**
	 * Set: dsServicoTratamentoListaDebito.
	 *
	 * @param dsServicoTratamentoListaDebito the ds servico tratamento lista debito
	 */
	public void setDsServicoTratamentoListaDebito(
			String dsServicoTratamentoListaDebito) {
		this.dsServicoTratamentoListaDebito = dsServicoTratamentoListaDebito;
	}

	/**
	 * Get: dsServicoAutorizacaoComplementoAgencia.
	 *
	 * @return dsServicoAutorizacaoComplementoAgencia
	 */
	public String getDsServicoAutorizacaoComplementoAgencia() {
		return dsServicoAutorizacaoComplementoAgencia;
	}

	/**
	 * Set: dsServicoAutorizacaoComplementoAgencia.
	 *
	 * @param dsServicoAutorizacaoComplementoAgencia the ds servico autorizacao complemento agencia
	 */
	public void setDsServicoAutorizacaoComplementoAgencia(
			String dsServicoAutorizacaoComplementoAgencia) {
		this.dsServicoAutorizacaoComplementoAgencia = dsServicoAutorizacaoComplementoAgencia;
	}

	/**
	 * Get: dsServicoPreAutorizacaoCliente.
	 *
	 * @return dsServicoPreAutorizacaoCliente
	 */
	public String getDsServicoPreAutorizacaoCliente() {
		return dsServicoPreAutorizacaoCliente;
	}

	/**
	 * Set: dsServicoPreAutorizacaoCliente.
	 *
	 * @param dsServicoPreAutorizacaoCliente the ds servico pre autorizacao cliente
	 */
	public void setDsServicoPreAutorizacaoCliente(
			String dsServicoPreAutorizacaoCliente) {
		this.dsServicoPreAutorizacaoCliente = dsServicoPreAutorizacaoCliente;
	}

	/**
	 * Get: dsCretipoServico.
	 *
	 * @return dsCretipoServico
	 */
	public String getDsCretipoServico() {
		return dsCretipoServico;
	}

	/**
	 * Set: dsCretipoServico.
	 *
	 * @param dsCretipoServico the ds cretipo servico
	 */
	public void setDsCretipoServico(String dsCretipoServico) {
		this.dsCretipoServico = dsCretipoServico;
	}

	/**
	 * Get: dsCreTipoConsultaSaldo.
	 *
	 * @return dsCreTipoConsultaSaldo
	 */
	public String getDsCreTipoConsultaSaldo() {
		return dsCreTipoConsultaSaldo;
	}

	/**
	 * Set: dsCreTipoConsultaSaldo.
	 *
	 * @param dsCreTipoConsultaSaldo the ds cre tipo consulta saldo
	 */
	public void setDsCreTipoConsultaSaldo(String dsCreTipoConsultaSaldo) {
		this.dsCreTipoConsultaSaldo = dsCreTipoConsultaSaldo;
	}

	/**
	 * Get: dsCreTipoProcessamento.
	 *
	 * @return dsCreTipoProcessamento
	 */
	public String getDsCreTipoProcessamento() {
		return dsCreTipoProcessamento;
	}

	/**
	 * Set: dsCreTipoProcessamento.
	 *
	 * @param dsCreTipoProcessamento the ds cre tipo processamento
	 */
	public void setDsCreTipoProcessamento(String dsCreTipoProcessamento) {
		this.dsCreTipoProcessamento = dsCreTipoProcessamento;
	}

	/**
	 * Get: dsCreTipoTratamentoFeriado.
	 *
	 * @return dsCreTipoTratamentoFeriado
	 */
	public String getDsCreTipoTratamentoFeriado() {
		return dsCreTipoTratamentoFeriado;
	}

	/**
	 * Set: dsCreTipoTratamentoFeriado.
	 *
	 * @param dsCreTipoTratamentoFeriado the ds cre tipo tratamento feriado
	 */
	public void setDsCreTipoTratamentoFeriado(String dsCreTipoTratamentoFeriado) {
		this.dsCreTipoTratamentoFeriado = dsCreTipoTratamentoFeriado;
	}

	/**
	 * Get: dsCreTipoRejeicaoEfetivacao.
	 *
	 * @return dsCreTipoRejeicaoEfetivacao
	 */
	public String getDsCreTipoRejeicaoEfetivacao() {
		return dsCreTipoRejeicaoEfetivacao;
	}

	/**
	 * Set: dsCreTipoRejeicaoEfetivacao.
	 *
	 * @param dsCreTipoRejeicaoEfetivacao the ds cre tipo rejeicao efetivacao
	 */
	public void setDsCreTipoRejeicaoEfetivacao(
			String dsCreTipoRejeicaoEfetivacao) {
		this.dsCreTipoRejeicaoEfetivacao = dsCreTipoRejeicaoEfetivacao;
	}

	/**
	 * Get: dsCreTipoRejeicaoAgendada.
	 *
	 * @return dsCreTipoRejeicaoAgendada
	 */
	public String getDsCreTipoRejeicaoAgendada() {
		return dsCreTipoRejeicaoAgendada;
	}

	/**
	 * Set: dsCreTipoRejeicaoAgendada.
	 *
	 * @param dsCreTipoRejeicaoAgendada the ds cre tipo rejeicao agendada
	 */
	public void setDsCreTipoRejeicaoAgendada(String dsCreTipoRejeicaoAgendada) {
		this.dsCreTipoRejeicaoAgendada = dsCreTipoRejeicaoAgendada;
	}

	/**
	 * Get: cdCreIndicadorPercentualMaximo.
	 *
	 * @return cdCreIndicadorPercentualMaximo
	 */
	public String getCdCreIndicadorPercentualMaximo() {
		return cdCreIndicadorPercentualMaximo;
	}

	/**
	 * Set: cdCreIndicadorPercentualMaximo.
	 *
	 * @param cdCreIndicadorPercentualMaximo the cd cre indicador percentual maximo
	 */
	public void setCdCreIndicadorPercentualMaximo(
			String cdCreIndicadorPercentualMaximo) {
		this.cdCreIndicadorPercentualMaximo = cdCreIndicadorPercentualMaximo;
	}

	/**
	 * Get: pcCreMaximoInconsistente.
	 *
	 * @return pcCreMaximoInconsistente
	 */
	public String getPcCreMaximoInconsistente() {
		return pcCreMaximoInconsistente;
	}

	/**
	 * Set: pcCreMaximoInconsistente.
	 *
	 * @param pcCreMaximoInconsistente the pc cre maximo inconsistente
	 */
	public void setPcCreMaximoInconsistente(String pcCreMaximoInconsistente) {
		this.pcCreMaximoInconsistente = pcCreMaximoInconsistente;
	}

	/**
	 * Get: cdCreIndicadorQuantidadeMaxima.
	 *
	 * @return cdCreIndicadorQuantidadeMaxima
	 */
	public String getCdCreIndicadorQuantidadeMaxima() {
		return cdCreIndicadorQuantidadeMaxima;
	}

	/**
	 * Set: cdCreIndicadorQuantidadeMaxima.
	 *
	 * @param cdCreIndicadorQuantidadeMaxima the cd cre indicador quantidade maxima
	 */
	public void setCdCreIndicadorQuantidadeMaxima(
			String cdCreIndicadorQuantidadeMaxima) {
		this.cdCreIndicadorQuantidadeMaxima = cdCreIndicadorQuantidadeMaxima;
	}

	/**
	 * Get: qtCreMaximaInconsistencia.
	 *
	 * @return qtCreMaximaInconsistencia
	 */
	public String getQtCreMaximaInconsistencia() {
		return qtCreMaximaInconsistencia;
	}

	/**
	 * Set: qtCreMaximaInconsistencia.
	 *
	 * @param qtCreMaximaInconsistencia the qt cre maxima inconsistencia
	 */
	public void setQtCreMaximaInconsistencia(String qtCreMaximaInconsistencia) {
		this.qtCreMaximaInconsistencia = qtCreMaximaInconsistencia;
	}

	/**
	 * Get: cdCreIndicadorValorMaximo.
	 *
	 * @return cdCreIndicadorValorMaximo
	 */
	public String getCdCreIndicadorValorMaximo() {
		return cdCreIndicadorValorMaximo;
	}

	/**
	 * Set: cdCreIndicadorValorMaximo.
	 *
	 * @param cdCreIndicadorValorMaximo the cd cre indicador valor maximo
	 */
	public void setCdCreIndicadorValorMaximo(String cdCreIndicadorValorMaximo) {
		this.cdCreIndicadorValorMaximo = cdCreIndicadorValorMaximo;
	}

	/**
	 * Get: vlCreMaximoFavorecidoNao.
	 *
	 * @return vlCreMaximoFavorecidoNao
	 */
	public String getVlCreMaximoFavorecidoNao() {
		return vlCreMaximoFavorecidoNao;
	}

	/**
	 * Set: vlCreMaximoFavorecidoNao.
	 *
	 * @param vlCreMaximoFavorecidoNao the vl cre maximo favorecido nao
	 */
	public void setVlCreMaximoFavorecidoNao(String vlCreMaximoFavorecidoNao) {
		this.vlCreMaximoFavorecidoNao = vlCreMaximoFavorecidoNao;
	}

	/**
	 * Get: dsCrePrioridadeDebito.
	 *
	 * @return dsCrePrioridadeDebito
	 */
	public String getDsCrePrioridadeDebito() {
		return dsCrePrioridadeDebito;
	}

	/**
	 * Set: dsCrePrioridadeDebito.
	 *
	 * @param dsCrePrioridadeDebito the ds cre prioridade debito
	 */
	public void setDsCrePrioridadeDebito(String dsCrePrioridadeDebito) {
		this.dsCrePrioridadeDebito = dsCrePrioridadeDebito;
	}

	/**
	 * Get: cdCreIndicadorValorDia.
	 *
	 * @return cdCreIndicadorValorDia
	 */
	public String getCdCreIndicadorValorDia() {
		return cdCreIndicadorValorDia;
	}

	/**
	 * Set: cdCreIndicadorValorDia.
	 *
	 * @param cdCreIndicadorValorDia the cd cre indicador valor dia
	 */
	public void setCdCreIndicadorValorDia(String cdCreIndicadorValorDia) {
		this.cdCreIndicadorValorDia = cdCreIndicadorValorDia;
	}

	/**
	 * Get: vlCreLimiteDiario.
	 *
	 * @return vlCreLimiteDiario
	 */
	public String getVlCreLimiteDiario() {
		return vlCreLimiteDiario;
	}

	/**
	 * Set: vlCreLimiteDiario.
	 *
	 * @param vlCreLimiteDiario the vl cre limite diario
	 */
	public void setVlCreLimiteDiario(String vlCreLimiteDiario) {
		this.vlCreLimiteDiario = vlCreLimiteDiario;
	}

	/**
	 * Get: cdCreIndicadorValorIndividual.
	 *
	 * @return cdCreIndicadorValorIndividual
	 */
	public String getCdCreIndicadorValorIndividual() {
		return cdCreIndicadorValorIndividual;
	}

	/**
	 * Set: cdCreIndicadorValorIndividual.
	 *
	 * @param cdCreIndicadorValorIndividual the cd cre indicador valor individual
	 */
	public void setCdCreIndicadorValorIndividual(
			String cdCreIndicadorValorIndividual) {
		this.cdCreIndicadorValorIndividual = cdCreIndicadorValorIndividual;
	}

	/**
	 * Get: vlCreLimiteIndividual.
	 *
	 * @return vlCreLimiteIndividual
	 */
	public String getVlCreLimiteIndividual() {
		return vlCreLimiteIndividual;
	}

	/**
	 * Set: vlCreLimiteIndividual.
	 *
	 * @param vlCreLimiteIndividual the vl cre limite individual
	 */
	public void setVlCreLimiteIndividual(String vlCreLimiteIndividual) {
		this.vlCreLimiteIndividual = vlCreLimiteIndividual;
	}

	/**
	 * Get: cdCreIndicadorQuantidadeFloating.
	 *
	 * @return cdCreIndicadorQuantidadeFloating
	 */
	public String getCdCreIndicadorQuantidadeFloating() {
		return cdCreIndicadorQuantidadeFloating;
	}

	/**
	 * Set: cdCreIndicadorQuantidadeFloating.
	 *
	 * @param cdCreIndicadorQuantidadeFloating the cd cre indicador quantidade floating
	 */
	public void setCdCreIndicadorQuantidadeFloating(
			String cdCreIndicadorQuantidadeFloating) {
		this.cdCreIndicadorQuantidadeFloating = cdCreIndicadorQuantidadeFloating;
	}

	/**
	 * Get: qtCreDiaFloating.
	 *
	 * @return qtCreDiaFloating
	 */
	public String getQtCreDiaFloating() {
		return qtCreDiaFloating;
	}

	/**
	 * Set: qtCreDiaFloating.
	 *
	 * @param qtCreDiaFloating the qt cre dia floating
	 */
	public void setQtCreDiaFloating(String qtCreDiaFloating) {
		this.qtCreDiaFloating = qtCreDiaFloating;
	}

	/**
	 * Get: cdCreIndicadorCadastroFavorecido.
	 *
	 * @return cdCreIndicadorCadastroFavorecido
	 */
	public String getCdCreIndicadorCadastroFavorecido() {
		return cdCreIndicadorCadastroFavorecido;
	}

	/**
	 * Set: cdCreIndicadorCadastroFavorecido.
	 *
	 * @param cdCreIndicadorCadastroFavorecido the cd cre indicador cadastro favorecido
	 */
	public void setCdCreIndicadorCadastroFavorecido(
			String cdCreIndicadorCadastroFavorecido) {
		this.cdCreIndicadorCadastroFavorecido = cdCreIndicadorCadastroFavorecido;
	}

	/**
	 * Get: cdCreUtilizacaoCadastroFavorecido.
	 *
	 * @return cdCreUtilizacaoCadastroFavorecido
	 */
	public String getCdCreUtilizacaoCadastroFavorecido() {
		return cdCreUtilizacaoCadastroFavorecido;
	}

	/**
	 * Set: cdCreUtilizacaoCadastroFavorecido.
	 *
	 * @param cdCreUtilizacaoCadastroFavorecido the cd cre utilizacao cadastro favorecido
	 */
	public void setCdCreUtilizacaoCadastroFavorecido(
			String cdCreUtilizacaoCadastroFavorecido) {
		this.cdCreUtilizacaoCadastroFavorecido = cdCreUtilizacaoCadastroFavorecido;
	}

	/**
	 * Get: cdCreIndicadorQuantidadeRepique.
	 *
	 * @return cdCreIndicadorQuantidadeRepique
	 */
	public String getCdCreIndicadorQuantidadeRepique() {
		return cdCreIndicadorQuantidadeRepique;
	}

	/**
	 * Set: cdCreIndicadorQuantidadeRepique.
	 *
	 * @param cdCreIndicadorQuantidadeRepique the cd cre indicador quantidade repique
	 */
	public void setCdCreIndicadorQuantidadeRepique(
			String cdCreIndicadorQuantidadeRepique) {
		this.cdCreIndicadorQuantidadeRepique = cdCreIndicadorQuantidadeRepique;
	}

	/**
	 * Get: qtCreDiaRepique.
	 *
	 * @return qtCreDiaRepique
	 */
	public String getQtCreDiaRepique() {
		return qtCreDiaRepique;
	}

	/**
	 * Set: qtCreDiaRepique.
	 *
	 * @param qtCreDiaRepique the qt cre dia repique
	 */
	public void setQtCreDiaRepique(String qtCreDiaRepique) {
		this.qtCreDiaRepique = qtCreDiaRepique;
	}

	/**
	 * Get: dsCreTipoConsistenciaFavorecido.
	 *
	 * @return dsCreTipoConsistenciaFavorecido
	 */
	public String getDsCreTipoConsistenciaFavorecido() {
		return dsCreTipoConsistenciaFavorecido;
	}

	/**
	 * Set: dsCreTipoConsistenciaFavorecido.
	 *
	 * @param dsCreTipoConsistenciaFavorecido the ds cre tipo consistencia favorecido
	 */
	public void setDsCreTipoConsistenciaFavorecido(
			String dsCreTipoConsistenciaFavorecido) {
		this.dsCreTipoConsistenciaFavorecido = dsCreTipoConsistenciaFavorecido;
	}

	/**
	 * Get: dsFtiTipoTitulo.
	 *
	 * @return dsFtiTipoTitulo
	 */
	public String getDsFtiTipoTitulo() {
		return dsFtiTipoTitulo;
	}

	/**
	 * Set: dsFtiTipoTitulo.
	 *
	 * @param dsFtiTipoTitulo the ds fti tipo titulo
	 */
	public void setDsFtiTipoTitulo(String dsFtiTipoTitulo) {
		this.dsFtiTipoTitulo = dsFtiTipoTitulo;
	}

	/**
	 * Get: dsFtiTipoConsultaSaldo.
	 *
	 * @return dsFtiTipoConsultaSaldo
	 */
	public String getDsFtiTipoConsultaSaldo() {
		return dsFtiTipoConsultaSaldo;
	}

	/**
	 * Set: dsFtiTipoConsultaSaldo.
	 *
	 * @param dsFtiTipoConsultaSaldo the ds fti tipo consulta saldo
	 */
	public void setDsFtiTipoConsultaSaldo(String dsFtiTipoConsultaSaldo) {
		this.dsFtiTipoConsultaSaldo = dsFtiTipoConsultaSaldo;
	}

	/**
	 * Get: dsFtiTipoProcessamento.
	 *
	 * @return dsFtiTipoProcessamento
	 */
	public String getDsFtiTipoProcessamento() {
		return dsFtiTipoProcessamento;
	}

	/**
	 * Set: dsFtiTipoProcessamento.
	 *
	 * @param dsFtiTipoProcessamento the ds fti tipo processamento
	 */
	public void setDsFtiTipoProcessamento(String dsFtiTipoProcessamento) {
		this.dsFtiTipoProcessamento = dsFtiTipoProcessamento;
	}

	/**
	 * Get: dsFtiTipoTratamentoFeriado.
	 *
	 * @return dsFtiTipoTratamentoFeriado
	 */
	public String getDsFtiTipoTratamentoFeriado() {
		return dsFtiTipoTratamentoFeriado;
	}

	/**
	 * Set: dsFtiTipoTratamentoFeriado.
	 *
	 * @param dsFtiTipoTratamentoFeriado the ds fti tipo tratamento feriado
	 */
	public void setDsFtiTipoTratamentoFeriado(String dsFtiTipoTratamentoFeriado) {
		this.dsFtiTipoTratamentoFeriado = dsFtiTipoTratamentoFeriado;
	}

	/**
	 * Get: dsFtiTipoRejeicaoEfetivacao.
	 *
	 * @return dsFtiTipoRejeicaoEfetivacao
	 */
	public String getDsFtiTipoRejeicaoEfetivacao() {
		return dsFtiTipoRejeicaoEfetivacao;
	}

	/**
	 * Set: dsFtiTipoRejeicaoEfetivacao.
	 *
	 * @param dsFtiTipoRejeicaoEfetivacao the ds fti tipo rejeicao efetivacao
	 */
	public void setDsFtiTipoRejeicaoEfetivacao(
			String dsFtiTipoRejeicaoEfetivacao) {
		this.dsFtiTipoRejeicaoEfetivacao = dsFtiTipoRejeicaoEfetivacao;
	}

	/**
	 * Get: dsFtiTipoRejeicaoAgendamento.
	 *
	 * @return dsFtiTipoRejeicaoAgendamento
	 */
	public String getDsFtiTipoRejeicaoAgendamento() {
		return dsFtiTipoRejeicaoAgendamento;
	}

	/**
	 * Set: dsFtiTipoRejeicaoAgendamento.
	 *
	 * @param dsFtiTipoRejeicaoAgendamento the ds fti tipo rejeicao agendamento
	 */
	public void setDsFtiTipoRejeicaoAgendamento(
			String dsFtiTipoRejeicaoAgendamento) {
		this.dsFtiTipoRejeicaoAgendamento = dsFtiTipoRejeicaoAgendamento;
	}

	/**
	 * Get: cdFtiIndicadorPercentualMaximo.
	 *
	 * @return cdFtiIndicadorPercentualMaximo
	 */
	public String getCdFtiIndicadorPercentualMaximo() {
		return cdFtiIndicadorPercentualMaximo;
	}

	/**
	 * Set: cdFtiIndicadorPercentualMaximo.
	 *
	 * @param cdFtiIndicadorPercentualMaximo the cd fti indicador percentual maximo
	 */
	public void setCdFtiIndicadorPercentualMaximo(
			String cdFtiIndicadorPercentualMaximo) {
		this.cdFtiIndicadorPercentualMaximo = cdFtiIndicadorPercentualMaximo;
	}

	/**
	 * Get: pcFtiMaximoInconsistente.
	 *
	 * @return pcFtiMaximoInconsistente
	 */
	public String getPcFtiMaximoInconsistente() {
		return pcFtiMaximoInconsistente;
	}

	/**
	 * Set: pcFtiMaximoInconsistente.
	 *
	 * @param pcFtiMaximoInconsistente the pc fti maximo inconsistente
	 */
	public void setPcFtiMaximoInconsistente(String pcFtiMaximoInconsistente) {
		this.pcFtiMaximoInconsistente = pcFtiMaximoInconsistente;
	}

	/**
	 * Get: cdFtiIndicadorQuantidadeMaxima.
	 *
	 * @return cdFtiIndicadorQuantidadeMaxima
	 */
	public String getCdFtiIndicadorQuantidadeMaxima() {
		return cdFtiIndicadorQuantidadeMaxima;
	}

	/**
	 * Set: cdFtiIndicadorQuantidadeMaxima.
	 *
	 * @param cdFtiIndicadorQuantidadeMaxima the cd fti indicador quantidade maxima
	 */
	public void setCdFtiIndicadorQuantidadeMaxima(
			String cdFtiIndicadorQuantidadeMaxima) {
		this.cdFtiIndicadorQuantidadeMaxima = cdFtiIndicadorQuantidadeMaxima;
	}

	/**
	 * Get: qtFtiMaximaInconsistencia.
	 *
	 * @return qtFtiMaximaInconsistencia
	 */
	public String getQtFtiMaximaInconsistencia() {
		return qtFtiMaximaInconsistencia;
	}

	/**
	 * Set: qtFtiMaximaInconsistencia.
	 *
	 * @param qtFtiMaximaInconsistencia the qt fti maxima inconsistencia
	 */
	public void setQtFtiMaximaInconsistencia(String qtFtiMaximaInconsistencia) {
		this.qtFtiMaximaInconsistencia = qtFtiMaximaInconsistencia;
	}

	/**
	 * Get: cdFtiIndicadorValorMaximo.
	 *
	 * @return cdFtiIndicadorValorMaximo
	 */
	public String getCdFtiIndicadorValorMaximo() {
		return cdFtiIndicadorValorMaximo;
	}

	/**
	 * Set: cdFtiIndicadorValorMaximo.
	 *
	 * @param cdFtiIndicadorValorMaximo the cd fti indicador valor maximo
	 */
	public void setCdFtiIndicadorValorMaximo(String cdFtiIndicadorValorMaximo) {
		this.cdFtiIndicadorValorMaximo = cdFtiIndicadorValorMaximo;
	}

	/**
	 * Get: vlFtiMaximoFavorecidoNao.
	 *
	 * @return vlFtiMaximoFavorecidoNao
	 */
	public String getVlFtiMaximoFavorecidoNao() {
		return vlFtiMaximoFavorecidoNao;
	}

	/**
	 * Set: vlFtiMaximoFavorecidoNao.
	 *
	 * @param vlFtiMaximoFavorecidoNao the vl fti maximo favorecido nao
	 */
	public void setVlFtiMaximoFavorecidoNao(String vlFtiMaximoFavorecidoNao) {
		this.vlFtiMaximoFavorecidoNao = vlFtiMaximoFavorecidoNao;
	}

	/**
	 * Get: dsFtiPrioridadeDebito.
	 *
	 * @return dsFtiPrioridadeDebito
	 */
	public String getDsFtiPrioridadeDebito() {
		return dsFtiPrioridadeDebito;
	}

	/**
	 * Set: dsFtiPrioridadeDebito.
	 *
	 * @param dsFtiPrioridadeDebito the ds fti prioridade debito
	 */
	public void setDsFtiPrioridadeDebito(String dsFtiPrioridadeDebito) {
		this.dsFtiPrioridadeDebito = dsFtiPrioridadeDebito;
	}

	/**
	 * Get: cdFtiIndicadorValorDia.
	 *
	 * @return cdFtiIndicadorValorDia
	 */
	public String getCdFtiIndicadorValorDia() {
		return cdFtiIndicadorValorDia;
	}

	/**
	 * Set: cdFtiIndicadorValorDia.
	 *
	 * @param cdFtiIndicadorValorDia the cd fti indicador valor dia
	 */
	public void setCdFtiIndicadorValorDia(String cdFtiIndicadorValorDia) {
		this.cdFtiIndicadorValorDia = cdFtiIndicadorValorDia;
	}

	/**
	 * Get: vlFtiLimiteDiario.
	 *
	 * @return vlFtiLimiteDiario
	 */
	public String getVlFtiLimiteDiario() {
		return vlFtiLimiteDiario;
	}

	/**
	 * Set: vlFtiLimiteDiario.
	 *
	 * @param vlFtiLimiteDiario the vl fti limite diario
	 */
	public void setVlFtiLimiteDiario(String vlFtiLimiteDiario) {
		this.vlFtiLimiteDiario = vlFtiLimiteDiario;
	}

	/**
	 * Get: cdFtiIndicadorValorIndividual.
	 *
	 * @return cdFtiIndicadorValorIndividual
	 */
	public String getCdFtiIndicadorValorIndividual() {
		return cdFtiIndicadorValorIndividual;
	}

	/**
	 * Set: cdFtiIndicadorValorIndividual.
	 *
	 * @param cdFtiIndicadorValorIndividual the cd fti indicador valor individual
	 */
	public void setCdFtiIndicadorValorIndividual(
			String cdFtiIndicadorValorIndividual) {
		this.cdFtiIndicadorValorIndividual = cdFtiIndicadorValorIndividual;
	}

	/**
	 * Get: vlFtiLimiteIndividual.
	 *
	 * @return vlFtiLimiteIndividual
	 */
	public String getVlFtiLimiteIndividual() {
		return vlFtiLimiteIndividual;
	}

	/**
	 * Set: vlFtiLimiteIndividual.
	 *
	 * @param vlFtiLimiteIndividual the vl fti limite individual
	 */
	public void setVlFtiLimiteIndividual(String vlFtiLimiteIndividual) {
		this.vlFtiLimiteIndividual = vlFtiLimiteIndividual;
	}

	/**
	 * Get: cdFtiIndicadorQuantidadeFloating.
	 *
	 * @return cdFtiIndicadorQuantidadeFloating
	 */
	public String getCdFtiIndicadorQuantidadeFloating() {
		return cdFtiIndicadorQuantidadeFloating;
	}

	/**
	 * Set: cdFtiIndicadorQuantidadeFloating.
	 *
	 * @param cdFtiIndicadorQuantidadeFloating the cd fti indicador quantidade floating
	 */
	public void setCdFtiIndicadorQuantidadeFloating(
			String cdFtiIndicadorQuantidadeFloating) {
		this.cdFtiIndicadorQuantidadeFloating = cdFtiIndicadorQuantidadeFloating;
	}

	/**
	 * Get: qtFtiDiaFloating.
	 *
	 * @return qtFtiDiaFloating
	 */
	public String getQtFtiDiaFloating() {
		return qtFtiDiaFloating;
	}

	/**
	 * Set: qtFtiDiaFloating.
	 *
	 * @param qtFtiDiaFloating the qt fti dia floating
	 */
	public void setQtFtiDiaFloating(String qtFtiDiaFloating) {
		this.qtFtiDiaFloating = qtFtiDiaFloating;
	}

	/**
	 * Get: cdFtiIndicadorCadastroFavorecido.
	 *
	 * @return cdFtiIndicadorCadastroFavorecido
	 */
	public String getCdFtiIndicadorCadastroFavorecido() {
		return cdFtiIndicadorCadastroFavorecido;
	}

	/**
	 * Set: cdFtiIndicadorCadastroFavorecido.
	 *
	 * @param cdFtiIndicadorCadastroFavorecido the cd fti indicador cadastro favorecido
	 */
	public void setCdFtiIndicadorCadastroFavorecido(
			String cdFtiIndicadorCadastroFavorecido) {
		this.cdFtiIndicadorCadastroFavorecido = cdFtiIndicadorCadastroFavorecido;
	}

	/**
	 * Get: cdFtiUtilizadoFavorecido.
	 *
	 * @return cdFtiUtilizadoFavorecido
	 */
	public String getCdFtiUtilizadoFavorecido() {
		return cdFtiUtilizadoFavorecido;
	}

	/**
	 * Set: cdFtiUtilizadoFavorecido.
	 *
	 * @param cdFtiUtilizadoFavorecido the cd fti utilizado favorecido
	 */
	public void setCdFtiUtilizadoFavorecido(String cdFtiUtilizadoFavorecido) {
		this.cdFtiUtilizadoFavorecido = cdFtiUtilizadoFavorecido;
	}

	/**
	 * Get: cdFtiIndicadorQuantidadeRepique.
	 *
	 * @return cdFtiIndicadorQuantidadeRepique
	 */
	public String getCdFtiIndicadorQuantidadeRepique() {
		return cdFtiIndicadorQuantidadeRepique;
	}

	/**
	 * Set: cdFtiIndicadorQuantidadeRepique.
	 *
	 * @param cdFtiIndicadorQuantidadeRepique the cd fti indicador quantidade repique
	 */
	public void setCdFtiIndicadorQuantidadeRepique(
			String cdFtiIndicadorQuantidadeRepique) {
		this.cdFtiIndicadorQuantidadeRepique = cdFtiIndicadorQuantidadeRepique;
	}

	/**
	 * Get: qtFtiDiaRepique.
	 *
	 * @return qtFtiDiaRepique
	 */
	public String getQtFtiDiaRepique() {
		return qtFtiDiaRepique;
	}

	/**
	 * Set: qtFtiDiaRepique.
	 *
	 * @param qtFtiDiaRepique the qt fti dia repique
	 */
	public void setQtFtiDiaRepique(String qtFtiDiaRepique) {
		this.qtFtiDiaRepique = qtFtiDiaRepique;
	}

	/**
	 * Get: dsModTipoServico.
	 *
	 * @return dsModTipoServico
	 */
	public String getDsModTipoServico() {
		return dsModTipoServico;
	}

	/**
	 * Set: dsModTipoServico.
	 *
	 * @param dsModTipoServico the ds mod tipo servico
	 */
	public void setDsModTipoServico(String dsModTipoServico) {
		this.dsModTipoServico = dsModTipoServico;
	}

	/**
	 * Get: dsModTipoModalidade.
	 *
	 * @return dsModTipoModalidade
	 */
	public String getDsModTipoModalidade() {
		return dsModTipoModalidade;
	}

	/**
	 * Set: dsModTipoModalidade.
	 *
	 * @param dsModTipoModalidade the ds mod tipo modalidade
	 */
	public void setDsModTipoModalidade(String dsModTipoModalidade) {
		this.dsModTipoModalidade = dsModTipoModalidade;
	}

	/**
	 * Get: dsModTipoConsultaSaldo.
	 *
	 * @return dsModTipoConsultaSaldo
	 */
	public String getDsModTipoConsultaSaldo() {
		return dsModTipoConsultaSaldo;
	}

	/**
	 * Set: dsModTipoConsultaSaldo.
	 *
	 * @param dsModTipoConsultaSaldo the ds mod tipo consulta saldo
	 */
	public void setDsModTipoConsultaSaldo(String dsModTipoConsultaSaldo) {
		this.dsModTipoConsultaSaldo = dsModTipoConsultaSaldo;
	}

	/**
	 * Get: dsModTipoProcessamento.
	 *
	 * @return dsModTipoProcessamento
	 */
	public String getDsModTipoProcessamento() {
		return dsModTipoProcessamento;
	}

	/**
	 * Set: dsModTipoProcessamento.
	 *
	 * @param dsModTipoProcessamento the ds mod tipo processamento
	 */
	public void setDsModTipoProcessamento(String dsModTipoProcessamento) {
		this.dsModTipoProcessamento = dsModTipoProcessamento;
	}

	/**
	 * Get: dsModTipoTratamentoFeriado.
	 *
	 * @return dsModTipoTratamentoFeriado
	 */
	public String getDsModTipoTratamentoFeriado() {
		return dsModTipoTratamentoFeriado;
	}

	/**
	 * Set: dsModTipoTratamentoFeriado.
	 *
	 * @param dsModTipoTratamentoFeriado the ds mod tipo tratamento feriado
	 */
	public void setDsModTipoTratamentoFeriado(String dsModTipoTratamentoFeriado) {
		this.dsModTipoTratamentoFeriado = dsModTipoTratamentoFeriado;
	}

	/**
	 * Get: dsModTipoRejeicaoEfetivacao.
	 *
	 * @return dsModTipoRejeicaoEfetivacao
	 */
	public String getDsModTipoRejeicaoEfetivacao() {
		return dsModTipoRejeicaoEfetivacao;
	}

	/**
	 * Set: dsModTipoRejeicaoEfetivacao.
	 *
	 * @param dsModTipoRejeicaoEfetivacao the ds mod tipo rejeicao efetivacao
	 */
	public void setDsModTipoRejeicaoEfetivacao(
			String dsModTipoRejeicaoEfetivacao) {
		this.dsModTipoRejeicaoEfetivacao = dsModTipoRejeicaoEfetivacao;
	}

	/**
	 * Get: dsModTipoRejeicaoAgendado.
	 *
	 * @return dsModTipoRejeicaoAgendado
	 */
	public String getDsModTipoRejeicaoAgendado() {
		return dsModTipoRejeicaoAgendado;
	}

	/**
	 * Set: dsModTipoRejeicaoAgendado.
	 *
	 * @param dsModTipoRejeicaoAgendado the ds mod tipo rejeicao agendado
	 */
	public void setDsModTipoRejeicaoAgendado(String dsModTipoRejeicaoAgendado) {
		this.dsModTipoRejeicaoAgendado = dsModTipoRejeicaoAgendado;
	}

	/**
	 * Get: cdModIndicadorPercentualMaximo.
	 *
	 * @return cdModIndicadorPercentualMaximo
	 */
	public String getCdModIndicadorPercentualMaximo() {
		return cdModIndicadorPercentualMaximo;
	}

	/**
	 * Set: cdModIndicadorPercentualMaximo.
	 *
	 * @param cdModIndicadorPercentualMaximo the cd mod indicador percentual maximo
	 */
	public void setCdModIndicadorPercentualMaximo(
			String cdModIndicadorPercentualMaximo) {
		this.cdModIndicadorPercentualMaximo = cdModIndicadorPercentualMaximo;
	}

	/**
	 * Get: cdModIndicadorPercentualMaximoInconsistente.
	 *
	 * @return cdModIndicadorPercentualMaximoInconsistente
	 */
	public String getCdModIndicadorPercentualMaximoInconsistente() {
		return cdModIndicadorPercentualMaximoInconsistente;
	}

	/**
	 * Set: cdModIndicadorPercentualMaximoInconsistente.
	 *
	 * @param cdModIndicadorPercentualMaximoInconsistente the cd mod indicador percentual maximo inconsistente
	 */
	public void setCdModIndicadorPercentualMaximoInconsistente(
			String cdModIndicadorPercentualMaximoInconsistente) {
		this.cdModIndicadorPercentualMaximoInconsistente = cdModIndicadorPercentualMaximoInconsistente;
	}

	/**
	 * Get: cdModIndicadorQuantidadeMaxima.
	 *
	 * @return cdModIndicadorQuantidadeMaxima
	 */
	public String getCdModIndicadorQuantidadeMaxima() {
		return cdModIndicadorQuantidadeMaxima;
	}

	/**
	 * Set: cdModIndicadorQuantidadeMaxima.
	 *
	 * @param cdModIndicadorQuantidadeMaxima the cd mod indicador quantidade maxima
	 */
	public void setCdModIndicadorQuantidadeMaxima(
			String cdModIndicadorQuantidadeMaxima) {
		this.cdModIndicadorQuantidadeMaxima = cdModIndicadorQuantidadeMaxima;
	}

	/**
	 * Get: qtModMaximoInconsistencia.
	 *
	 * @return qtModMaximoInconsistencia
	 */
	public String getQtModMaximoInconsistencia() {
		return qtModMaximoInconsistencia;
	}

	/**
	 * Set: qtModMaximoInconsistencia.
	 *
	 * @param qtModMaximoInconsistencia the qt mod maximo inconsistencia
	 */
	public void setQtModMaximoInconsistencia(String qtModMaximoInconsistencia) {
		this.qtModMaximoInconsistencia = qtModMaximoInconsistencia;
	}

	/**
	 * Get: cdModIndicadorValorMaximo.
	 *
	 * @return cdModIndicadorValorMaximo
	 */
	public String getCdModIndicadorValorMaximo() {
		return cdModIndicadorValorMaximo;
	}

	/**
	 * Set: cdModIndicadorValorMaximo.
	 *
	 * @param cdModIndicadorValorMaximo the cd mod indicador valor maximo
	 */
	public void setCdModIndicadorValorMaximo(String cdModIndicadorValorMaximo) {
		this.cdModIndicadorValorMaximo = cdModIndicadorValorMaximo;
	}

	/**
	 * Get: vlModMaximoFavorecidoNao.
	 *
	 * @return vlModMaximoFavorecidoNao
	 */
	public String getVlModMaximoFavorecidoNao() {
		return vlModMaximoFavorecidoNao;
	}

	/**
	 * Set: vlModMaximoFavorecidoNao.
	 *
	 * @param vlModMaximoFavorecidoNao the vl mod maximo favorecido nao
	 */
	public void setVlModMaximoFavorecidoNao(String vlModMaximoFavorecidoNao) {
		this.vlModMaximoFavorecidoNao = vlModMaximoFavorecidoNao;
	}

	/**
	 * Get: dsModPrioridadeDebito.
	 *
	 * @return dsModPrioridadeDebito
	 */
	public String getDsModPrioridadeDebito() {
		return dsModPrioridadeDebito;
	}

	/**
	 * Set: dsModPrioridadeDebito.
	 *
	 * @param dsModPrioridadeDebito the ds mod prioridade debito
	 */
	public void setDsModPrioridadeDebito(String dsModPrioridadeDebito) {
		this.dsModPrioridadeDebito = dsModPrioridadeDebito;
	}

	/**
	 * Get: cdModIndicadorValorDia.
	 *
	 * @return cdModIndicadorValorDia
	 */
	public String getCdModIndicadorValorDia() {
		return cdModIndicadorValorDia;
	}

	/**
	 * Set: cdModIndicadorValorDia.
	 *
	 * @param cdModIndicadorValorDia the cd mod indicador valor dia
	 */
	public void setCdModIndicadorValorDia(String cdModIndicadorValorDia) {
		this.cdModIndicadorValorDia = cdModIndicadorValorDia;
	}

	/**
	 * Get: vlModLimiteDiario.
	 *
	 * @return vlModLimiteDiario
	 */
	public String getVlModLimiteDiario() {
		return vlModLimiteDiario;
	}

	/**
	 * Set: vlModLimiteDiario.
	 *
	 * @param vlModLimiteDiario the vl mod limite diario
	 */
	public void setVlModLimiteDiario(String vlModLimiteDiario) {
		this.vlModLimiteDiario = vlModLimiteDiario;
	}

	/**
	 * Get: cdModIndicadorValorIndividual.
	 *
	 * @return cdModIndicadorValorIndividual
	 */
	public String getCdModIndicadorValorIndividual() {
		return cdModIndicadorValorIndividual;
	}

	/**
	 * Set: cdModIndicadorValorIndividual.
	 *
	 * @param cdModIndicadorValorIndividual the cd mod indicador valor individual
	 */
	public void setCdModIndicadorValorIndividual(
			String cdModIndicadorValorIndividual) {
		this.cdModIndicadorValorIndividual = cdModIndicadorValorIndividual;
	}

	/**
	 * Get: vlModLimiteIndividual.
	 *
	 * @return vlModLimiteIndividual
	 */
	public String getVlModLimiteIndividual() {
		return vlModLimiteIndividual;
	}

	/**
	 * Set: vlModLimiteIndividual.
	 *
	 * @param vlModLimiteIndividual the vl mod limite individual
	 */
	public void setVlModLimiteIndividual(String vlModLimiteIndividual) {
		this.vlModLimiteIndividual = vlModLimiteIndividual;
	}

	/**
	 * Get: cdModIndicadorQuantidadeFloating.
	 *
	 * @return cdModIndicadorQuantidadeFloating
	 */
	public String getCdModIndicadorQuantidadeFloating() {
		return cdModIndicadorQuantidadeFloating;
	}

	/**
	 * Set: cdModIndicadorQuantidadeFloating.
	 *
	 * @param cdModIndicadorQuantidadeFloating the cd mod indicador quantidade floating
	 */
	public void setCdModIndicadorQuantidadeFloating(
			String cdModIndicadorQuantidadeFloating) {
		this.cdModIndicadorQuantidadeFloating = cdModIndicadorQuantidadeFloating;
	}

	/**
	 * Get: qtModDiaFloating.
	 *
	 * @return qtModDiaFloating
	 */
	public String getQtModDiaFloating() {
		return qtModDiaFloating;
	}

	/**
	 * Set: qtModDiaFloating.
	 *
	 * @param qtModDiaFloating the qt mod dia floating
	 */
	public void setQtModDiaFloating(String qtModDiaFloating) {
		this.qtModDiaFloating = qtModDiaFloating;
	}

	/**
	 * Get: cdModIndicadorCadastroFavorecido.
	 *
	 * @return cdModIndicadorCadastroFavorecido
	 */
	public String getCdModIndicadorCadastroFavorecido() {
		return cdModIndicadorCadastroFavorecido;
	}

	/**
	 * Set: cdModIndicadorCadastroFavorecido.
	 *
	 * @param cdModIndicadorCadastroFavorecido the cd mod indicador cadastro favorecido
	 */
	public void setCdModIndicadorCadastroFavorecido(
			String cdModIndicadorCadastroFavorecido) {
		this.cdModIndicadorCadastroFavorecido = cdModIndicadorCadastroFavorecido;
	}

	/**
	 * Get: cdModUtilizacaoCadastroFavorecido.
	 *
	 * @return cdModUtilizacaoCadastroFavorecido
	 */
	public String getCdModUtilizacaoCadastroFavorecido() {
		return cdModUtilizacaoCadastroFavorecido;
	}

	/**
	 * Set: cdModUtilizacaoCadastroFavorecido.
	 *
	 * @param cdModUtilizacaoCadastroFavorecido the cd mod utilizacao cadastro favorecido
	 */
	public void setCdModUtilizacaoCadastroFavorecido(
			String cdModUtilizacaoCadastroFavorecido) {
		this.cdModUtilizacaoCadastroFavorecido = cdModUtilizacaoCadastroFavorecido;
	}

	/**
	 * Get: cdModIndicadorQuantidadeRepique.
	 *
	 * @return cdModIndicadorQuantidadeRepique
	 */
	public String getCdModIndicadorQuantidadeRepique() {
		return cdModIndicadorQuantidadeRepique;
	}

	/**
	 * Set: cdModIndicadorQuantidadeRepique.
	 *
	 * @param cdModIndicadorQuantidadeRepique the cd mod indicador quantidade repique
	 */
	public void setCdModIndicadorQuantidadeRepique(
			String cdModIndicadorQuantidadeRepique) {
		this.cdModIndicadorQuantidadeRepique = cdModIndicadorQuantidadeRepique;
	}

	/**
	 * Get: qtModDiaRepique.
	 *
	 * @return qtModDiaRepique
	 */
	public String getQtModDiaRepique() {
		return qtModDiaRepique;
	}

	/**
	 * Set: qtModDiaRepique.
	 *
	 * @param qtModDiaRepique the qt mod dia repique
	 */
	public void setQtModDiaRepique(String qtModDiaRepique) {
		this.qtModDiaRepique = qtModDiaRepique;
	}

	/**
	 * Get: dsFraTipoRastreabilidade.
	 *
	 * @return dsFraTipoRastreabilidade
	 */
	public String getDsFraTipoRastreabilidade() {
		return dsFraTipoRastreabilidade;
	}

	/**
	 * Set: dsFraTipoRastreabilidade.
	 *
	 * @param dsFraTipoRastreabilidade the ds fra tipo rastreabilidade
	 */
	public void setDsFraTipoRastreabilidade(String dsFraTipoRastreabilidade) {
		this.dsFraTipoRastreabilidade = dsFraTipoRastreabilidade;
	}

	/**
	 * Get: dsFraRastreabilidadeTerceiro.
	 *
	 * @return dsFraRastreabilidadeTerceiro
	 */
	public String getDsFraRastreabilidadeTerceiro() {
		return dsFraRastreabilidadeTerceiro;
	}

	/**
	 * Set: dsFraRastreabilidadeTerceiro.
	 *
	 * @param dsFraRastreabilidadeTerceiro the ds fra rastreabilidade terceiro
	 */
	public void setDsFraRastreabilidadeTerceiro(
			String dsFraRastreabilidadeTerceiro) {
		this.dsFraRastreabilidadeTerceiro = dsFraRastreabilidadeTerceiro;
	}

	/**
	 * Get: dsFraRastreabilidadeNota.
	 *
	 * @return dsFraRastreabilidadeNota
	 */
	public String getDsFraRastreabilidadeNota() {
		return dsFraRastreabilidadeNota;
	}

	/**
	 * Set: dsFraRastreabilidadeNota.
	 *
	 * @param dsFraRastreabilidadeNota the ds fra rastreabilidade nota
	 */
	public void setDsFraRastreabilidadeNota(String dsFraRastreabilidadeNota) {
		this.dsFraRastreabilidadeNota = dsFraRastreabilidadeNota;
	}

	/**
	 * Get: dsFraCaptacaoTitulo.
	 *
	 * @return dsFraCaptacaoTitulo
	 */
	public String getDsFraCaptacaoTitulo() {
		return dsFraCaptacaoTitulo;
	}

	/**
	 * Set: dsFraCaptacaoTitulo.
	 *
	 * @param dsFraCaptacaoTitulo the ds fra captacao titulo
	 */
	public void setDsFraCaptacaoTitulo(String dsFraCaptacaoTitulo) {
		this.dsFraCaptacaoTitulo = dsFraCaptacaoTitulo;
	}

	/**
	 * Get: dsFraAgendaCliente.
	 *
	 * @return dsFraAgendaCliente
	 */
	public String getDsFraAgendaCliente() {
		return dsFraAgendaCliente;
	}

	/**
	 * Set: dsFraAgendaCliente.
	 *
	 * @param dsFraAgendaCliente the ds fra agenda cliente
	 */
	public void setDsFraAgendaCliente(String dsFraAgendaCliente) {
		this.dsFraAgendaCliente = dsFraAgendaCliente;
	}

	/**
	 * Get: dsFraAgendaFilial.
	 *
	 * @return dsFraAgendaFilial
	 */
	public String getDsFraAgendaFilial() {
		return dsFraAgendaFilial;
	}

	/**
	 * Set: dsFraAgendaFilial.
	 *
	 * @param dsFraAgendaFilial the ds fra agenda filial
	 */
	public void setDsFraAgendaFilial(String dsFraAgendaFilial) {
		this.dsFraAgendaFilial = dsFraAgendaFilial;
	}

	/**
	 * Get: dsFraBloqueioEmissao.
	 *
	 * @return dsFraBloqueioEmissao
	 */
	public String getDsFraBloqueioEmissao() {
		return dsFraBloqueioEmissao;
	}

	/**
	 * Set: dsFraBloqueioEmissao.
	 *
	 * @param dsFraBloqueioEmissao the ds fra bloqueio emissao
	 */
	public void setDsFraBloqueioEmissao(String dsFraBloqueioEmissao) {
		this.dsFraBloqueioEmissao = dsFraBloqueioEmissao;
	}

	/**
	 * Get: dtFraInicioBloqueio.
	 *
	 * @return dtFraInicioBloqueio
	 */
	public String getDtFraInicioBloqueio() {
		return dtFraInicioBloqueio;
	}

	/**
	 * Set: dtFraInicioBloqueio.
	 *
	 * @param dtFraInicioBloqueio the dt fra inicio bloqueio
	 */
	public void setDtFraInicioBloqueio(String dtFraInicioBloqueio) {
		this.dtFraInicioBloqueio = dtFraInicioBloqueio;
	}

	/**
	 * Get: dtFraInicioRastreabilidade.
	 *
	 * @return dtFraInicioRastreabilidade
	 */
	public String getDtFraInicioRastreabilidade() {
		return dtFraInicioRastreabilidade;
	}

	/**
	 * Set: dtFraInicioRastreabilidade.
	 *
	 * @param dtFraInicioRastreabilidade the dt fra inicio rastreabilidade
	 */
	public void setDtFraInicioRastreabilidade(String dtFraInicioRastreabilidade) {
		this.dtFraInicioRastreabilidade = dtFraInicioRastreabilidade;
	}

	/**
	 * Get: dsFraAdesaoSacado.
	 *
	 * @return dsFraAdesaoSacado
	 */
	public String getDsFraAdesaoSacado() {
		return dsFraAdesaoSacado;
	}

	/**
	 * Set: dsFraAdesaoSacado.
	 *
	 * @param dsFraAdesaoSacado the ds fra adesao sacado
	 */
	public void setDsFraAdesaoSacado(String dsFraAdesaoSacado) {
		this.dsFraAdesaoSacado = dsFraAdesaoSacado;
	}

	/**
	 * Get: dsOpgTipoServico.
	 *
	 * @return dsOpgTipoServico
	 */
	public String getDsOpgTipoServico() {
		return dsOpgTipoServico;
	}

	/**
	 * Set: dsOpgTipoServico.
	 *
	 * @param dsOpgTipoServico the ds opg tipo servico
	 */
	public void setDsOpgTipoServico(String dsOpgTipoServico) {
		this.dsOpgTipoServico = dsOpgTipoServico;
	}

	/**
	 * Get: dsOpgTipoConsultaSaldo.
	 *
	 * @return dsOpgTipoConsultaSaldo
	 */
	public String getDsOpgTipoConsultaSaldo() {
		return dsOpgTipoConsultaSaldo;
	}

	/**
	 * Set: dsOpgTipoConsultaSaldo.
	 *
	 * @param dsOpgTipoConsultaSaldo the ds opg tipo consulta saldo
	 */
	public void setDsOpgTipoConsultaSaldo(String dsOpgTipoConsultaSaldo) {
		this.dsOpgTipoConsultaSaldo = dsOpgTipoConsultaSaldo;
	}

	/**
	 * Get: dsOpgTipoProcessamento.
	 *
	 * @return dsOpgTipoProcessamento
	 */
	public String getDsOpgTipoProcessamento() {
		return dsOpgTipoProcessamento;
	}

	/**
	 * Set: dsOpgTipoProcessamento.
	 *
	 * @param dsOpgTipoProcessamento the ds opg tipo processamento
	 */
	public void setDsOpgTipoProcessamento(String dsOpgTipoProcessamento) {
		this.dsOpgTipoProcessamento = dsOpgTipoProcessamento;
	}

	/**
	 * Get: dsOpgTipoTratamentoFeriado.
	 *
	 * @return dsOpgTipoTratamentoFeriado
	 */
	public String getDsOpgTipoTratamentoFeriado() {
		return dsOpgTipoTratamentoFeriado;
	}

	/**
	 * Set: dsOpgTipoTratamentoFeriado.
	 *
	 * @param dsOpgTipoTratamentoFeriado the ds opg tipo tratamento feriado
	 */
	public void setDsOpgTipoTratamentoFeriado(String dsOpgTipoTratamentoFeriado) {
		this.dsOpgTipoTratamentoFeriado = dsOpgTipoTratamentoFeriado;
	}

	/**
	 * Get: dsOpgTipoRejeicaoEfetivacao.
	 *
	 * @return dsOpgTipoRejeicaoEfetivacao
	 */
	public String getDsOpgTipoRejeicaoEfetivacao() {
		return dsOpgTipoRejeicaoEfetivacao;
	}

	/**
	 * Set: dsOpgTipoRejeicaoEfetivacao.
	 *
	 * @param dsOpgTipoRejeicaoEfetivacao the ds opg tipo rejeicao efetivacao
	 */
	public void setDsOpgTipoRejeicaoEfetivacao(
			String dsOpgTipoRejeicaoEfetivacao) {
		this.dsOpgTipoRejeicaoEfetivacao = dsOpgTipoRejeicaoEfetivacao;
	}

	/**
	 * Get: dsOpgTiporejeicaoAgendado.
	 *
	 * @return dsOpgTiporejeicaoAgendado
	 */
	public String getDsOpgTiporejeicaoAgendado() {
		return dsOpgTiporejeicaoAgendado;
	}

	/**
	 * Set: dsOpgTiporejeicaoAgendado.
	 *
	 * @param dsOpgTiporejeicaoAgendado the ds opg tiporejeicao agendado
	 */
	public void setDsOpgTiporejeicaoAgendado(String dsOpgTiporejeicaoAgendado) {
		this.dsOpgTiporejeicaoAgendado = dsOpgTiporejeicaoAgendado;
	}

	/**
	 * Get: cdOpgIndicadorPercentualMaximo.
	 *
	 * @return cdOpgIndicadorPercentualMaximo
	 */
	public String getCdOpgIndicadorPercentualMaximo() {
		return cdOpgIndicadorPercentualMaximo;
	}

	/**
	 * Set: cdOpgIndicadorPercentualMaximo.
	 *
	 * @param cdOpgIndicadorPercentualMaximo the cd opg indicador percentual maximo
	 */
	public void setCdOpgIndicadorPercentualMaximo(
			String cdOpgIndicadorPercentualMaximo) {
		this.cdOpgIndicadorPercentualMaximo = cdOpgIndicadorPercentualMaximo;
	}

	/**
	 * Get: pcOpgMaximoInconsistencia.
	 *
	 * @return pcOpgMaximoInconsistencia
	 */
	public String getPcOpgMaximoInconsistencia() {
		return pcOpgMaximoInconsistencia;
	}

	/**
	 * Set: pcOpgMaximoInconsistencia.
	 *
	 * @param pcOpgMaximoInconsistencia the pc opg maximo inconsistencia
	 */
	public void setPcOpgMaximoInconsistencia(String pcOpgMaximoInconsistencia) {
		this.pcOpgMaximoInconsistencia = pcOpgMaximoInconsistencia;
	}

	/**
	 * Get: cdOpgIndicadorQuantidadeMaximo.
	 *
	 * @return cdOpgIndicadorQuantidadeMaximo
	 */
	public String getCdOpgIndicadorQuantidadeMaximo() {
		return cdOpgIndicadorQuantidadeMaximo;
	}

	/**
	 * Set: cdOpgIndicadorQuantidadeMaximo.
	 *
	 * @param cdOpgIndicadorQuantidadeMaximo the cd opg indicador quantidade maximo
	 */
	public void setCdOpgIndicadorQuantidadeMaximo(
			String cdOpgIndicadorQuantidadeMaximo) {
		this.cdOpgIndicadorQuantidadeMaximo = cdOpgIndicadorQuantidadeMaximo;
	}

	/**
	 * Get: qtOpgMaximoInconsistencia.
	 *
	 * @return qtOpgMaximoInconsistencia
	 */
	public String getQtOpgMaximoInconsistencia() {
		return qtOpgMaximoInconsistencia;
	}

	/**
	 * Set: qtOpgMaximoInconsistencia.
	 *
	 * @param qtOpgMaximoInconsistencia the qt opg maximo inconsistencia
	 */
	public void setQtOpgMaximoInconsistencia(String qtOpgMaximoInconsistencia) {
		this.qtOpgMaximoInconsistencia = qtOpgMaximoInconsistencia;
	}

	/**
	 * Get: cdOpgIndicadorValorDia.
	 *
	 * @return cdOpgIndicadorValorDia
	 */
	public String getCdOpgIndicadorValorDia() {
		return cdOpgIndicadorValorDia;
	}

	/**
	 * Set: cdOpgIndicadorValorDia.
	 *
	 * @param cdOpgIndicadorValorDia the cd opg indicador valor dia
	 */
	public void setCdOpgIndicadorValorDia(String cdOpgIndicadorValorDia) {
		this.cdOpgIndicadorValorDia = cdOpgIndicadorValorDia;
	}

	/**
	 * Get: vlOpgLimiteDiario.
	 *
	 * @return vlOpgLimiteDiario
	 */
	public String getVlOpgLimiteDiario() {
		return vlOpgLimiteDiario;
	}

	/**
	 * Set: vlOpgLimiteDiario.
	 *
	 * @param vlOpgLimiteDiario the vl opg limite diario
	 */
	public void setVlOpgLimiteDiario(String vlOpgLimiteDiario) {
		this.vlOpgLimiteDiario = vlOpgLimiteDiario;
	}

	/**
	 * Get: cdOpgIndicadorValorIndividual.
	 *
	 * @return cdOpgIndicadorValorIndividual
	 */
	public String getCdOpgIndicadorValorIndividual() {
		return cdOpgIndicadorValorIndividual;
	}

	/**
	 * Set: cdOpgIndicadorValorIndividual.
	 *
	 * @param cdOpgIndicadorValorIndividual the cd opg indicador valor individual
	 */
	public void setCdOpgIndicadorValorIndividual(
			String cdOpgIndicadorValorIndividual) {
		this.cdOpgIndicadorValorIndividual = cdOpgIndicadorValorIndividual;
	}

	/**
	 * Get: vlOpgLimiteIndividual.
	 *
	 * @return vlOpgLimiteIndividual
	 */
	public String getVlOpgLimiteIndividual() {
		return vlOpgLimiteIndividual;
	}

	/**
	 * Set: vlOpgLimiteIndividual.
	 *
	 * @param vlOpgLimiteIndividual the vl opg limite individual
	 */
	public void setVlOpgLimiteIndividual(String vlOpgLimiteIndividual) {
		this.vlOpgLimiteIndividual = vlOpgLimiteIndividual;
	}

	/**
	 * Get: cdOpgIndicadorQuantidadeFloating.
	 *
	 * @return cdOpgIndicadorQuantidadeFloating
	 */
	public String getCdOpgIndicadorQuantidadeFloating() {
		return cdOpgIndicadorQuantidadeFloating;
	}

	/**
	 * Set: cdOpgIndicadorQuantidadeFloating.
	 *
	 * @param cdOpgIndicadorQuantidadeFloating the cd opg indicador quantidade floating
	 */
	public void setCdOpgIndicadorQuantidadeFloating(
			String cdOpgIndicadorQuantidadeFloating) {
		this.cdOpgIndicadorQuantidadeFloating = cdOpgIndicadorQuantidadeFloating;
	}

	/**
	 * Get: qtOpgDiaFloating.
	 *
	 * @return qtOpgDiaFloating
	 */
	public String getQtOpgDiaFloating() {
		return qtOpgDiaFloating;
	}

	/**
	 * Set: qtOpgDiaFloating.
	 *
	 * @param qtOpgDiaFloating the qt opg dia floating
	 */
	public void setQtOpgDiaFloating(String qtOpgDiaFloating) {
		this.qtOpgDiaFloating = qtOpgDiaFloating;
	}

	/**
	 * Get: dsOpgPrioridadeDebito.
	 *
	 * @return dsOpgPrioridadeDebito
	 */
	public String getDsOpgPrioridadeDebito() {
		return dsOpgPrioridadeDebito;
	}

	/**
	 * Set: dsOpgPrioridadeDebito.
	 *
	 * @param dsOpgPrioridadeDebito the ds opg prioridade debito
	 */
	public void setDsOpgPrioridadeDebito(String dsOpgPrioridadeDebito) {
		this.dsOpgPrioridadeDebito = dsOpgPrioridadeDebito;
	}

	/**
	 * Get: cdOpgIndicadorValorMaximo.
	 *
	 * @return cdOpgIndicadorValorMaximo
	 */
	public String getCdOpgIndicadorValorMaximo() {
		return cdOpgIndicadorValorMaximo;
	}

	/**
	 * Set: cdOpgIndicadorValorMaximo.
	 *
	 * @param cdOpgIndicadorValorMaximo the cd opg indicador valor maximo
	 */
	public void setCdOpgIndicadorValorMaximo(String cdOpgIndicadorValorMaximo) {
		this.cdOpgIndicadorValorMaximo = cdOpgIndicadorValorMaximo;
	}

	/**
	 * Get: vlOpgMaximoFavorecidoNao.
	 *
	 * @return vlOpgMaximoFavorecidoNao
	 */
	public String getVlOpgMaximoFavorecidoNao() {
		return vlOpgMaximoFavorecidoNao;
	}

	/**
	 * Set: vlOpgMaximoFavorecidoNao.
	 *
	 * @param vlOpgMaximoFavorecidoNao the vl opg maximo favorecido nao
	 */
	public void setVlOpgMaximoFavorecidoNao(String vlOpgMaximoFavorecidoNao) {
		this.vlOpgMaximoFavorecidoNao = vlOpgMaximoFavorecidoNao;
	}

	/**
	 * Get: cdOpgIndicadorCadastroFavorecido.
	 *
	 * @return cdOpgIndicadorCadastroFavorecido
	 */
	public String getCdOpgIndicadorCadastroFavorecido() {
		return cdOpgIndicadorCadastroFavorecido;
	}

	/**
	 * Set: cdOpgIndicadorCadastroFavorecido.
	 *
	 * @param cdOpgIndicadorCadastroFavorecido the cd opg indicador cadastro favorecido
	 */
	public void setCdOpgIndicadorCadastroFavorecido(
			String cdOpgIndicadorCadastroFavorecido) {
		this.cdOpgIndicadorCadastroFavorecido = cdOpgIndicadorCadastroFavorecido;
	}

	/**
	 * Get: cdOpgUtilizacaoCadastroFavorecido.
	 *
	 * @return cdOpgUtilizacaoCadastroFavorecido
	 */
	public String getCdOpgUtilizacaoCadastroFavorecido() {
		return cdOpgUtilizacaoCadastroFavorecido;
	}

	/**
	 * Set: cdOpgUtilizacaoCadastroFavorecido.
	 *
	 * @param cdOpgUtilizacaoCadastroFavorecido the cd opg utilizacao cadastro favorecido
	 */
	public void setCdOpgUtilizacaoCadastroFavorecido(
			String cdOpgUtilizacaoCadastroFavorecido) {
		this.cdOpgUtilizacaoCadastroFavorecido = cdOpgUtilizacaoCadastroFavorecido;
	}

	/**
	 * Get: dsOpgOcorrenciasDebito.
	 *
	 * @return dsOpgOcorrenciasDebito
	 */
	public String getDsOpgOcorrenciasDebito() {
		return dsOpgOcorrenciasDebito;
	}

	/**
	 * Set: dsOpgOcorrenciasDebito.
	 *
	 * @param dsOpgOcorrenciasDebito the ds opg ocorrencias debito
	 */
	public void setDsOpgOcorrenciasDebito(String dsOpgOcorrenciasDebito) {
		this.dsOpgOcorrenciasDebito = dsOpgOcorrenciasDebito;
	}

	/**
	 * Get: dsOpgTipoInscricao.
	 *
	 * @return dsOpgTipoInscricao
	 */
	public String getDsOpgTipoInscricao() {
		return dsOpgTipoInscricao;
	}

	/**
	 * Set: dsOpgTipoInscricao.
	 *
	 * @param dsOpgTipoInscricao the ds opg tipo inscricao
	 */
	public void setDsOpgTipoInscricao(String dsOpgTipoInscricao) {
		this.dsOpgTipoInscricao = dsOpgTipoInscricao;
	}

	/**
	 * Get: cdOpgIndicadorQuantidadeRepique.
	 *
	 * @return cdOpgIndicadorQuantidadeRepique
	 */
	public String getCdOpgIndicadorQuantidadeRepique() {
		return cdOpgIndicadorQuantidadeRepique;
	}

	/**
	 * Set: cdOpgIndicadorQuantidadeRepique.
	 *
	 * @param cdOpgIndicadorQuantidadeRepique the cd opg indicador quantidade repique
	 */
	public void setCdOpgIndicadorQuantidadeRepique(
			String cdOpgIndicadorQuantidadeRepique) {
		this.cdOpgIndicadorQuantidadeRepique = cdOpgIndicadorQuantidadeRepique;
	}

	/**
	 * Get: qtOpgDiaRepique.
	 *
	 * @return qtOpgDiaRepique
	 */
	public String getQtOpgDiaRepique() {
		return qtOpgDiaRepique;
	}

	/**
	 * Set: qtOpgDiaRepique.
	 *
	 * @param qtOpgDiaRepique the qt opg dia repique
	 */
	public void setQtOpgDiaRepique(String qtOpgDiaRepique) {
		this.qtOpgDiaRepique = qtOpgDiaRepique;
	}

	/**
	 * Get: qtOpgDiaExpiracao.
	 *
	 * @return qtOpgDiaExpiracao
	 */
	public String getQtOpgDiaExpiracao() {
		return qtOpgDiaExpiracao;
	}

	/**
	 * Set: qtOpgDiaExpiracao.
	 *
	 * @param qtOpgDiaExpiracao the qt opg dia expiracao
	 */
	public void setQtOpgDiaExpiracao(String qtOpgDiaExpiracao) {
		this.qtOpgDiaExpiracao = qtOpgDiaExpiracao;
	}

	/**
	 * Get: dsTriTipoTributo.
	 *
	 * @return dsTriTipoTributo
	 */
	public String getDsTriTipoTributo() {
		return dsTriTipoTributo;
	}

	/**
	 * Set: dsTriTipoTributo.
	 *
	 * @param dsTriTipoTributo the ds tri tipo tributo
	 */
	public void setDsTriTipoTributo(String dsTriTipoTributo) {
		this.dsTriTipoTributo = dsTriTipoTributo;
	}

	/**
	 * Get: dsTriTipoConsultaSaldo.
	 *
	 * @return dsTriTipoConsultaSaldo
	 */
	public String getDsTriTipoConsultaSaldo() {
		return dsTriTipoConsultaSaldo;
	}

	/**
	 * Set: dsTriTipoConsultaSaldo.
	 *
	 * @param dsTriTipoConsultaSaldo the ds tri tipo consulta saldo
	 */
	public void setDsTriTipoConsultaSaldo(String dsTriTipoConsultaSaldo) {
		this.dsTriTipoConsultaSaldo = dsTriTipoConsultaSaldo;
	}

	/**
	 * Get: dsTriTipoProcessamento.
	 *
	 * @return dsTriTipoProcessamento
	 */
	public String getDsTriTipoProcessamento() {
		return dsTriTipoProcessamento;
	}

	/**
	 * Set: dsTriTipoProcessamento.
	 *
	 * @param dsTriTipoProcessamento the ds tri tipo processamento
	 */
	public void setDsTriTipoProcessamento(String dsTriTipoProcessamento) {
		this.dsTriTipoProcessamento = dsTriTipoProcessamento;
	}

	/**
	 * Get: dsTriTipoTratamentoFeriado.
	 *
	 * @return dsTriTipoTratamentoFeriado
	 */
	public String getDsTriTipoTratamentoFeriado() {
		return dsTriTipoTratamentoFeriado;
	}

	/**
	 * Set: dsTriTipoTratamentoFeriado.
	 *
	 * @param dsTriTipoTratamentoFeriado the ds tri tipo tratamento feriado
	 */
	public void setDsTriTipoTratamentoFeriado(String dsTriTipoTratamentoFeriado) {
		this.dsTriTipoTratamentoFeriado = dsTriTipoTratamentoFeriado;
	}

	/**
	 * Get: dsTriTipoRejeicaoEfetivacao.
	 *
	 * @return dsTriTipoRejeicaoEfetivacao
	 */
	public String getDsTriTipoRejeicaoEfetivacao() {
		return dsTriTipoRejeicaoEfetivacao;
	}

	/**
	 * Set: dsTriTipoRejeicaoEfetivacao.
	 *
	 * @param dsTriTipoRejeicaoEfetivacao the ds tri tipo rejeicao efetivacao
	 */
	public void setDsTriTipoRejeicaoEfetivacao(
			String dsTriTipoRejeicaoEfetivacao) {
		this.dsTriTipoRejeicaoEfetivacao = dsTriTipoRejeicaoEfetivacao;
	}

	/**
	 * Get: dsTriTipoRejeicaoAgendado.
	 *
	 * @return dsTriTipoRejeicaoAgendado
	 */
	public String getDsTriTipoRejeicaoAgendado() {
		return dsTriTipoRejeicaoAgendado;
	}

	/**
	 * Set: dsTriTipoRejeicaoAgendado.
	 *
	 * @param dsTriTipoRejeicaoAgendado the ds tri tipo rejeicao agendado
	 */
	public void setDsTriTipoRejeicaoAgendado(String dsTriTipoRejeicaoAgendado) {
		this.dsTriTipoRejeicaoAgendado = dsTriTipoRejeicaoAgendado;
	}

	/**
	 * Get: cdTriIndicadorValorMaximo.
	 *
	 * @return cdTriIndicadorValorMaximo
	 */
	public String getCdTriIndicadorValorMaximo() {
		return cdTriIndicadorValorMaximo;
	}

	/**
	 * Set: cdTriIndicadorValorMaximo.
	 *
	 * @param cdTriIndicadorValorMaximo the cd tri indicador valor maximo
	 */
	public void setCdTriIndicadorValorMaximo(String cdTriIndicadorValorMaximo) {
		this.cdTriIndicadorValorMaximo = cdTriIndicadorValorMaximo;
	}

	/**
	 * Get: vlTriMaximoFavorecidoNao.
	 *
	 * @return vlTriMaximoFavorecidoNao
	 */
	public String getVlTriMaximoFavorecidoNao() {
		return vlTriMaximoFavorecidoNao;
	}

	/**
	 * Set: vlTriMaximoFavorecidoNao.
	 *
	 * @param vlTriMaximoFavorecidoNao the vl tri maximo favorecido nao
	 */
	public void setVlTriMaximoFavorecidoNao(String vlTriMaximoFavorecidoNao) {
		this.vlTriMaximoFavorecidoNao = vlTriMaximoFavorecidoNao;
	}

	/**
	 * Get: dsTriPrioridadeDebito.
	 *
	 * @return dsTriPrioridadeDebito
	 */
	public String getDsTriPrioridadeDebito() {
		return dsTriPrioridadeDebito;
	}

	/**
	 * Set: dsTriPrioridadeDebito.
	 *
	 * @param dsTriPrioridadeDebito the ds tri prioridade debito
	 */
	public void setDsTriPrioridadeDebito(String dsTriPrioridadeDebito) {
		this.dsTriPrioridadeDebito = dsTriPrioridadeDebito;
	}

	/**
	 * Get: cdTriIndicadorValorDia.
	 *
	 * @return cdTriIndicadorValorDia
	 */
	public String getCdTriIndicadorValorDia() {
		return cdTriIndicadorValorDia;
	}

	/**
	 * Set: cdTriIndicadorValorDia.
	 *
	 * @param cdTriIndicadorValorDia the cd tri indicador valor dia
	 */
	public void setCdTriIndicadorValorDia(String cdTriIndicadorValorDia) {
		this.cdTriIndicadorValorDia = cdTriIndicadorValorDia;
	}

	/**
	 * Get: vlTriLimiteDiario.
	 *
	 * @return vlTriLimiteDiario
	 */
	public String getVlTriLimiteDiario() {
		return vlTriLimiteDiario;
	}

	/**
	 * Set: vlTriLimiteDiario.
	 *
	 * @param vlTriLimiteDiario the vl tri limite diario
	 */
	public void setVlTriLimiteDiario(String vlTriLimiteDiario) {
		this.vlTriLimiteDiario = vlTriLimiteDiario;
	}

	/**
	 * Get: cdTriIndicadorValorIndividual.
	 *
	 * @return cdTriIndicadorValorIndividual
	 */
	public String getCdTriIndicadorValorIndividual() {
		return cdTriIndicadorValorIndividual;
	}

	/**
	 * Set: cdTriIndicadorValorIndividual.
	 *
	 * @param cdTriIndicadorValorIndividual the cd tri indicador valor individual
	 */
	public void setCdTriIndicadorValorIndividual(
			String cdTriIndicadorValorIndividual) {
		this.cdTriIndicadorValorIndividual = cdTriIndicadorValorIndividual;
	}

	/**
	 * Get: vlTriLimiteIndividual.
	 *
	 * @return vlTriLimiteIndividual
	 */
	public String getVlTriLimiteIndividual() {
		return vlTriLimiteIndividual;
	}

	/**
	 * Set: vlTriLimiteIndividual.
	 *
	 * @param vlTriLimiteIndividual the vl tri limite individual
	 */
	public void setVlTriLimiteIndividual(String vlTriLimiteIndividual) {
		this.vlTriLimiteIndividual = vlTriLimiteIndividual;
	}

	/**
	 * Get: cdTriIndicadorQuantidadeFloating.
	 *
	 * @return cdTriIndicadorQuantidadeFloating
	 */
	public String getCdTriIndicadorQuantidadeFloating() {
		return cdTriIndicadorQuantidadeFloating;
	}

	/**
	 * Set: cdTriIndicadorQuantidadeFloating.
	 *
	 * @param cdTriIndicadorQuantidadeFloating the cd tri indicador quantidade floating
	 */
	public void setCdTriIndicadorQuantidadeFloating(
			String cdTriIndicadorQuantidadeFloating) {
		this.cdTriIndicadorQuantidadeFloating = cdTriIndicadorQuantidadeFloating;
	}

	/**
	 * Get: qttriDiaFloating.
	 *
	 * @return qttriDiaFloating
	 */
	public String getQttriDiaFloating() {
		return qttriDiaFloating;
	}

	/**
	 * Set: qttriDiaFloating.
	 *
	 * @param qttriDiaFloating the qttri dia floating
	 */
	public void setQttriDiaFloating(String qttriDiaFloating) {
		this.qttriDiaFloating = qttriDiaFloating;
	}

	/**
	 * Get: cdTriIndicadorCadastroFavorecido.
	 *
	 * @return cdTriIndicadorCadastroFavorecido
	 */
	public String getCdTriIndicadorCadastroFavorecido() {
		return cdTriIndicadorCadastroFavorecido;
	}

	/**
	 * Set: cdTriIndicadorCadastroFavorecido.
	 *
	 * @param cdTriIndicadorCadastroFavorecido the cd tri indicador cadastro favorecido
	 */
	public void setCdTriIndicadorCadastroFavorecido(
			String cdTriIndicadorCadastroFavorecido) {
		this.cdTriIndicadorCadastroFavorecido = cdTriIndicadorCadastroFavorecido;
	}

	/**
	 * Get: cdTriUtilizacaoCadastroFavorecido.
	 *
	 * @return cdTriUtilizacaoCadastroFavorecido
	 */
	public String getCdTriUtilizacaoCadastroFavorecido() {
		return cdTriUtilizacaoCadastroFavorecido;
	}

	/**
	 * Set: cdTriUtilizacaoCadastroFavorecido.
	 *
	 * @param cdTriUtilizacaoCadastroFavorecido the cd tri utilizacao cadastro favorecido
	 */
	public void setCdTriUtilizacaoCadastroFavorecido(
			String cdTriUtilizacaoCadastroFavorecido) {
		this.cdTriUtilizacaoCadastroFavorecido = cdTriUtilizacaoCadastroFavorecido;
	}

	/**
	 * Get: cdTriIndicadorQuantidadeRepique.
	 *
	 * @return cdTriIndicadorQuantidadeRepique
	 */
	public String getCdTriIndicadorQuantidadeRepique() {
		return cdTriIndicadorQuantidadeRepique;
	}

	/**
	 * Set: cdTriIndicadorQuantidadeRepique.
	 *
	 * @param cdTriIndicadorQuantidadeRepique the cd tri indicador quantidade repique
	 */
	public void setCdTriIndicadorQuantidadeRepique(
			String cdTriIndicadorQuantidadeRepique) {
		this.cdTriIndicadorQuantidadeRepique = cdTriIndicadorQuantidadeRepique;
	}

	/**
	 * Get: qtTriDiaRepique.
	 *
	 * @return qtTriDiaRepique
	 */
	public String getQtTriDiaRepique() {
		return qtTriDiaRepique;
	}

	/**
	 * Set: qtTriDiaRepique.
	 *
	 * @param qtTriDiaRepique the qt tri dia repique
	 */
	public void setQtTriDiaRepique(String qtTriDiaRepique) {
		this.qtTriDiaRepique = qtTriDiaRepique;
	}

	/**
	 * Get: dsTibutoAgendadoDebitoVeicular.
	 *
	 * @return dsTibutoAgendadoDebitoVeicular
	 */
	public String getDsTibutoAgendadoDebitoVeicular() {
		return dsTibutoAgendadoDebitoVeicular;
	}

	/**
	 * Set: dsTibutoAgendadoDebitoVeicular.
	 *
	 * @param dsTibutoAgendadoDebitoVeicular the ds tibuto agendado debito veicular
	 */
	public void setDsTibutoAgendadoDebitoVeicular(
			String dsTibutoAgendadoDebitoVeicular) {
		this.dsTibutoAgendadoDebitoVeicular = dsTibutoAgendadoDebitoVeicular;
	}

	/**
	 * Get: dsTdvTipoConsultaProposta.
	 *
	 * @return dsTdvTipoConsultaProposta
	 */
	public String getDsTdvTipoConsultaProposta() {
		return dsTdvTipoConsultaProposta;
	}

	/**
	 * Set: dsTdvTipoConsultaProposta.
	 *
	 * @param dsTdvTipoConsultaProposta the ds tdv tipo consulta proposta
	 */
	public void setDsTdvTipoConsultaProposta(String dsTdvTipoConsultaProposta) {
		this.dsTdvTipoConsultaProposta = dsTdvTipoConsultaProposta;
	}

	/**
	 * Get: dsTdvTipoTratamentoValor.
	 *
	 * @return dsTdvTipoTratamentoValor
	 */
	public String getDsTdvTipoTratamentoValor() {
		return dsTdvTipoTratamentoValor;
	}

	/**
	 * Set: dsTdvTipoTratamentoValor.
	 *
	 * @param dsTdvTipoTratamentoValor the ds tdv tipo tratamento valor
	 */
	public void setDsTdvTipoTratamentoValor(String dsTdvTipoTratamentoValor) {
		this.dsTdvTipoTratamentoValor = dsTdvTipoTratamentoValor;
	}

	/**
	 * Get: dsSalarioEmissaoAntecipadoCartao.
	 *
	 * @return dsSalarioEmissaoAntecipadoCartao
	 */
	public String getDsSalarioEmissaoAntecipadoCartao() {
		return dsSalarioEmissaoAntecipadoCartao;
	}

	/**
	 * Set: dsSalarioEmissaoAntecipadoCartao.
	 *
	 * @param dsSalarioEmissaoAntecipadoCartao the ds salario emissao antecipado cartao
	 */
	public void setDsSalarioEmissaoAntecipadoCartao(
			String dsSalarioEmissaoAntecipadoCartao) {
		this.dsSalarioEmissaoAntecipadoCartao = dsSalarioEmissaoAntecipadoCartao;
	}

	/**
	 * Get: cdSalarioQuantidadeLimiteSolicitacaoCartao.
	 *
	 * @return cdSalarioQuantidadeLimiteSolicitacaoCartao
	 */
	public String getCdSalarioQuantidadeLimiteSolicitacaoCartao() {
		return cdSalarioQuantidadeLimiteSolicitacaoCartao;
	}

	/**
	 * Set: cdSalarioQuantidadeLimiteSolicitacaoCartao.
	 *
	 * @param cdSalarioQuantidadeLimiteSolicitacaoCartao the cd salario quantidade limite solicitacao cartao
	 */
	public void setCdSalarioQuantidadeLimiteSolicitacaoCartao(
			String cdSalarioQuantidadeLimiteSolicitacaoCartao) {
		this.cdSalarioQuantidadeLimiteSolicitacaoCartao = cdSalarioQuantidadeLimiteSolicitacaoCartao;
	}

	/**
	 * Get: dsSalarioAberturaContaExpressa.
	 *
	 * @return dsSalarioAberturaContaExpressa
	 */
	public String getDsSalarioAberturaContaExpressa() {
		return dsSalarioAberturaContaExpressa;
	}

	/**
	 * Set: dsSalarioAberturaContaExpressa.
	 *
	 * @param dsSalarioAberturaContaExpressa the ds salario abertura conta expressa
	 */
	public void setDsSalarioAberturaContaExpressa(
			String dsSalarioAberturaContaExpressa) {
		this.dsSalarioAberturaContaExpressa = dsSalarioAberturaContaExpressa;
	}

	/**
	 * Get: dsBeneficioInformadoAgenciaConta.
	 *
	 * @return dsBeneficioInformadoAgenciaConta
	 */
	public String getDsBeneficioInformadoAgenciaConta() {
		return dsBeneficioInformadoAgenciaConta;
	}

	/**
	 * Set: dsBeneficioInformadoAgenciaConta.
	 *
	 * @param dsBeneficioInformadoAgenciaConta the ds beneficio informado agencia conta
	 */
	public void setDsBeneficioInformadoAgenciaConta(
			String dsBeneficioInformadoAgenciaConta) {
		this.dsBeneficioInformadoAgenciaConta = dsBeneficioInformadoAgenciaConta;
	}

	/**
	 * Get: dsBeneficioTipoIdentificacaoBeneficio.
	 *
	 * @return dsBeneficioTipoIdentificacaoBeneficio
	 */
	public String getDsBeneficioTipoIdentificacaoBeneficio() {
		return dsBeneficioTipoIdentificacaoBeneficio;
	}

	/**
	 * Set: dsBeneficioTipoIdentificacaoBeneficio.
	 *
	 * @param dsBeneficioTipoIdentificacaoBeneficio the ds beneficio tipo identificacao beneficio
	 */
	public void setDsBeneficioTipoIdentificacaoBeneficio(
			String dsBeneficioTipoIdentificacaoBeneficio) {
		this.dsBeneficioTipoIdentificacaoBeneficio = dsBeneficioTipoIdentificacaoBeneficio;
	}

	/**
	 * Get: dsBeneficioUtilizacaoCadastroOrganizacao.
	 *
	 * @return dsBeneficioUtilizacaoCadastroOrganizacao
	 */
	public String getDsBeneficioUtilizacaoCadastroOrganizacao() {
		return dsBeneficioUtilizacaoCadastroOrganizacao;
	}

	/**
	 * Set: dsBeneficioUtilizacaoCadastroOrganizacao.
	 *
	 * @param dsBeneficioUtilizacaoCadastroOrganizacao the ds beneficio utilizacao cadastro organizacao
	 */
	public void setDsBeneficioUtilizacaoCadastroOrganizacao(
			String dsBeneficioUtilizacaoCadastroOrganizacao) {
		this.dsBeneficioUtilizacaoCadastroOrganizacao = dsBeneficioUtilizacaoCadastroOrganizacao;
	}

	/**
	 * Get: dsBeneficioUtilizacaoCadastroProcuradores.
	 *
	 * @return dsBeneficioUtilizacaoCadastroProcuradores
	 */
	public String getDsBeneficioUtilizacaoCadastroProcuradores() {
		return dsBeneficioUtilizacaoCadastroProcuradores;
	}

	/**
	 * Set: dsBeneficioUtilizacaoCadastroProcuradores.
	 *
	 * @param dsBeneficioUtilizacaoCadastroProcuradores the ds beneficio utilizacao cadastro procuradores
	 */
	public void setDsBeneficioUtilizacaoCadastroProcuradores(
			String dsBeneficioUtilizacaoCadastroProcuradores) {
		this.dsBeneficioUtilizacaoCadastroProcuradores = dsBeneficioUtilizacaoCadastroProcuradores;
	}

	/**
	 * Get: dsBeneficioPossuiExpiracaoCredito.
	 *
	 * @return dsBeneficioPossuiExpiracaoCredito
	 */
	public String getDsBeneficioPossuiExpiracaoCredito() {
		return dsBeneficioPossuiExpiracaoCredito;
	}

	/**
	 * Set: dsBeneficioPossuiExpiracaoCredito.
	 *
	 * @param dsBeneficioPossuiExpiracaoCredito the ds beneficio possui expiracao credito
	 */
	public void setDsBeneficioPossuiExpiracaoCredito(
			String dsBeneficioPossuiExpiracaoCredito) {
		this.dsBeneficioPossuiExpiracaoCredito = dsBeneficioPossuiExpiracaoCredito;
	}

	/**
	 * Get: cdBeneficioQuantidadeDiasExpiracaoCredito.
	 *
	 * @return cdBeneficioQuantidadeDiasExpiracaoCredito
	 */
	public String getCdBeneficioQuantidadeDiasExpiracaoCredito() {
		return cdBeneficioQuantidadeDiasExpiracaoCredito;
	}

	/**
	 * Set: cdBeneficioQuantidadeDiasExpiracaoCredito.
	 *
	 * @param cdBeneficioQuantidadeDiasExpiracaoCredito the cd beneficio quantidade dias expiracao credito
	 */
	public void setCdBeneficioQuantidadeDiasExpiracaoCredito(
			String cdBeneficioQuantidadeDiasExpiracaoCredito) {
		this.cdBeneficioQuantidadeDiasExpiracaoCredito = cdBeneficioQuantidadeDiasExpiracaoCredito;
	}

	/**
	 * Get: dsBprTipoAcao.
	 *
	 * @return dsBprTipoAcao
	 */
	public String getDsBprTipoAcao() {
		return dsBprTipoAcao;
	}

	/**
	 * Set: dsBprTipoAcao.
	 *
	 * @param dsBprTipoAcao the ds bpr tipo acao
	 */
	public void setDsBprTipoAcao(String dsBprTipoAcao) {
		this.dsBprTipoAcao = dsBprTipoAcao;
	}

	/**
	 * Get: qtBprMesProvisorio.
	 *
	 * @return qtBprMesProvisorio
	 */
	public String getQtBprMesProvisorio() {
		return qtBprMesProvisorio;
	}

	/**
	 * Set: qtBprMesProvisorio.
	 *
	 * @param qtBprMesProvisorio the qt bpr mes provisorio
	 */
	public void setQtBprMesProvisorio(String qtBprMesProvisorio) {
		this.qtBprMesProvisorio = qtBprMesProvisorio;
	}

	/**
	 * Get: cdBprIndicadorEmissaoAviso.
	 *
	 * @return cdBprIndicadorEmissaoAviso
	 */
	public String getCdBprIndicadorEmissaoAviso() {
		return cdBprIndicadorEmissaoAviso;
	}

	/**
	 * Set: cdBprIndicadorEmissaoAviso.
	 *
	 * @param cdBprIndicadorEmissaoAviso the cd bpr indicador emissao aviso
	 */
	public void setCdBprIndicadorEmissaoAviso(String cdBprIndicadorEmissaoAviso) {
		this.cdBprIndicadorEmissaoAviso = cdBprIndicadorEmissaoAviso;
	}

	/**
	 * Get: qtBprDiaAnteriorAviso.
	 *
	 * @return qtBprDiaAnteriorAviso
	 */
	public String getQtBprDiaAnteriorAviso() {
		return qtBprDiaAnteriorAviso;
	}

	/**
	 * Set: qtBprDiaAnteriorAviso.
	 *
	 * @param qtBprDiaAnteriorAviso the qt bpr dia anterior aviso
	 */
	public void setQtBprDiaAnteriorAviso(String qtBprDiaAnteriorAviso) {
		this.qtBprDiaAnteriorAviso = qtBprDiaAnteriorAviso;
	}

	/**
	 * Get: qtBprDiaAnteriorVencimento.
	 *
	 * @return qtBprDiaAnteriorVencimento
	 */
	public String getQtBprDiaAnteriorVencimento() {
		return qtBprDiaAnteriorVencimento;
	}

	/**
	 * Set: qtBprDiaAnteriorVencimento.
	 *
	 * @param qtBprDiaAnteriorVencimento the qt bpr dia anterior vencimento
	 */
	public void setQtBprDiaAnteriorVencimento(String qtBprDiaAnteriorVencimento) {
		this.qtBprDiaAnteriorVencimento = qtBprDiaAnteriorVencimento;
	}

	/**
	 * Get: cdBprUtilizadoMensagemPersonalizada.
	 *
	 * @return cdBprUtilizadoMensagemPersonalizada
	 */
	public String getCdBprUtilizadoMensagemPersonalizada() {
		return cdBprUtilizadoMensagemPersonalizada;
	}

	/**
	 * Set: cdBprUtilizadoMensagemPersonalizada.
	 *
	 * @param cdBprUtilizadoMensagemPersonalizada the cd bpr utilizado mensagem personalizada
	 */
	public void setCdBprUtilizadoMensagemPersonalizada(
			String cdBprUtilizadoMensagemPersonalizada) {
		this.cdBprUtilizadoMensagemPersonalizada = cdBprUtilizadoMensagemPersonalizada;
	}

	/**
	 * Get: dsBprDestino.
	 *
	 * @return dsBprDestino
	 */
	public String getDsBprDestino() {
		return dsBprDestino;
	}

	/**
	 * Set: dsBprDestino.
	 *
	 * @param dsBprDestino the ds bpr destino
	 */
	public void setDsBprDestino(String dsBprDestino) {
		this.dsBprDestino = dsBprDestino;
	}

	/**
	 * Get: dsBprCadastroEnderecoUtilizado.
	 *
	 * @return dsBprCadastroEnderecoUtilizado
	 */
	public String getDsBprCadastroEnderecoUtilizado() {
		return dsBprCadastroEnderecoUtilizado;
	}

	/**
	 * Set: dsBprCadastroEnderecoUtilizado.
	 *
	 * @param dsBprCadastroEnderecoUtilizado the ds bpr cadastro endereco utilizado
	 */
	public void setDsBprCadastroEnderecoUtilizado(
			String dsBprCadastroEnderecoUtilizado) {
		this.dsBprCadastroEnderecoUtilizado = dsBprCadastroEnderecoUtilizado;
	}

	/**
	 * Get: dsModTipoOutrosServico.
	 *
	 * @return dsModTipoOutrosServico
	 */
	public String getDsModTipoOutrosServico() {
		return dsModTipoOutrosServico;
	}

	/**
	 * Set: dsModTipoOutrosServico.
	 *
	 * @param dsModTipoOutrosServico the ds mod tipo outros servico
	 */
	public void setDsModTipoOutrosServico(String dsModTipoOutrosServico) {
		this.dsModTipoOutrosServico = dsModTipoOutrosServico;
	}

	/**
	 * Get: exibeDebitoVeiculo.
	 *
	 * @return exibeDebitoVeiculo
	 */
	public String getExibeDebitoVeiculo() {
		return exibeDebitoVeiculo;
	}

	/**
	 * Set: exibeDebitoVeiculo.
	 *
	 * @param exibeDebitoVeiculo the exibe debito veiculo
	 */
	public void setExibeDebitoVeiculo(String exibeDebitoVeiculo) {
		this.exibeDebitoVeiculo = exibeDebitoVeiculo;
	}

	/**
	 * Get: dsTIpoConsultaSaldoSuperior.
	 *
	 * @return dsTIpoConsultaSaldoSuperior
	 */
	public String getDsTIpoConsultaSaldoSuperior() {
		return dsTIpoConsultaSaldoSuperior;
	}

	/**
	 * Set: dsTIpoConsultaSaldoSuperior.
	 *
	 * @param dsTIpoConsultaSaldoSuperior the ds t ipo consulta saldo superior
	 */
	public void setDsTIpoConsultaSaldoSuperior(
			String dsTIpoConsultaSaldoSuperior) {
		this.dsTIpoConsultaSaldoSuperior = dsTIpoConsultaSaldoSuperior;
	}

	/**
	 * Gets the ds servico tipo data float.
	 *
	 * @return the dsServicoTipoDataFloat
	 */
	public String getDsServicoTipoDataFloat() {
		return dsServicoTipoDataFloat;
	}

	/**
	 * Sets the ds servico tipo data float.
	 *
	 * @param dsServicoTipoDataFloat the dsServicoTipoDataFloat to set
	 */
	public void setDsServicoTipoDataFloat(String dsServicoTipoDataFloat) {
		this.dsServicoTipoDataFloat = dsServicoTipoDataFloat;
	}

	/**
	 * Gets the cd servico floating.
	 *
	 * @return the cdServicoFloating
	 */
	public String getCdServicoFloating() {
		return cdServicoFloating;
	}

	/**
	 * Sets the cd servico floating.
	 *
	 * @param cdServicoFloating the cdServicoFloating to set
	 */
	public void setCdServicoFloating(String cdServicoFloating) {
		this.cdServicoFloating = cdServicoFloating;
	}

	/**
	 * Gets the cd servico tipo consolidado.
	 *
	 * @return the cdServicoTipoConsolidado
	 */
	public String getCdServicoTipoConsolidado() {
		return cdServicoTipoConsolidado;
	}

	/**
	 * Sets the cd servico tipo consolidado.
	 *
	 * @param cdServicoTipoConsolidado the cdServicoTipoConsolidado to set
	 */
	public void setCdServicoTipoConsolidado(String cdServicoTipoConsolidado) {
		this.cdServicoTipoConsolidado = cdServicoTipoConsolidado;
	}

	/**
	 * Gets the fti pagamento vencido.
	 *
	 * @return the ftiPagamentoVencido
	 */
	public String getFtiPagamentoVencido() {
		return ftiPagamentoVencido;
	}

	/**
	 * Sets the fti pagamento vencido.
	 *
	 * @param ftiPagamentoVencido the ftiPagamentoVencido to set
	 */
	public void setFtiPagamentoVencido(String ftiPagamentoVencido) {
		this.ftiPagamentoVencido = ftiPagamentoVencido;
	}

	/**
	 * Gets the fti pagamento menor.
	 *
	 * @return the ftiPagamentoMenor
	 */
	public String getFtiPagamentoMenor() {
		return ftiPagamentoMenor;
	}

	/**
	 * Sets the fti pagamento menor.
	 *
	 * @param ftiPagamentoMenor the ftiPagamentoMenor to set
	 */
	public void setFtiPagamentoMenor(String ftiPagamentoMenor) {
		this.ftiPagamentoMenor = ftiPagamentoMenor;
	}

	/**
	 * Gets the fti ds tipo cons favorecido.
	 *
	 * @return the ftiDsTipoConsFavorecido
	 */
	public String getFtiDsTipoConsFavorecido() {
		return ftiDsTipoConsFavorecido;
	}

	/**
	 * Sets the fti ds tipo cons favorecido.
	 *
	 * @param ftiDsTipoConsFavorecido the ftiDsTipoConsFavorecido to set
	 */
	public void setFtiDsTipoConsFavorecido(String ftiDsTipoConsFavorecido) {
		this.ftiDsTipoConsFavorecido = ftiDsTipoConsFavorecido;
	}

	/**
	 * Gets the fti cferi loc.
	 *
	 * @return the ftiCferiLoc
	 */
	public String getFtiCferiLoc() {
		return ftiCferiLoc;
	}

	/**
	 * Sets the fti cferi loc.
	 *
	 * @param ftiCferiLoc the ftiCferiLoc to set
	 */
	public void setFtiCferiLoc(String ftiCferiLoc) {
		this.ftiCferiLoc = ftiCferiLoc;
	}

	/**
	 * Gets the fti quantidade max vencido.
	 *
	 * @return the ftiQuantidadeMaxVencido
	 */
	public Integer getFtiQuantidadeMaxVencido() {
		return ftiQuantidadeMaxVencido;
	}

	/**
	 * Sets the fti quantidade max vencido.
	 *
	 * @param ftiQuantidadeMaxVencido the ftiQuantidadeMaxVencido to set
	 */
	public void setFtiQuantidadeMaxVencido(Integer ftiQuantidadeMaxVencido) {
		this.ftiQuantidadeMaxVencido = ftiQuantidadeMaxVencido;
	}

	/**
	 * Gets the cd cre cferi loc.
	 *
	 * @return the cdCreCferiLoc
	 */
	public String getCdCreCferiLoc() {
		return cdCreCferiLoc;
	}

	/**
	 * Sets the cd cre cferi loc.
	 *
	 * @param cdCreCferiLoc the cdCreCferiLoc to set
	 */
	public void setCdCreCferiLoc(String cdCreCferiLoc) {
		this.cdCreCferiLoc = cdCreCferiLoc;
	}

	/**
	 * Gets the cd cre contrato transf.
	 *
	 * @return the cdCreContratoTransf
	 */
	public String getCdCreContratoTransf() {
		return cdCreContratoTransf;
	}

	/**
	 * Sets the cd cre contrato transf.
	 *
	 * @param cdCreContratoTransf the cdCreContratoTransf to set
	 */
	public void setCdCreContratoTransf(String cdCreContratoTransf) {
		this.cdCreContratoTransf = cdCreContratoTransf;
	}

	/**
	 * Gets the cd lcto pgmd.
	 *
	 * @return the cdLctoPgmd
	 */
	public String getCdLctoPgmd() {
		return cdLctoPgmd;
	}

	/**
	 * Sets the cd lcto pgmd.
	 *
	 * @param cdLctoPgmd the cdLctoPgmd to set
	 */
	public void setCdLctoPgmd(String cdLctoPgmd) {
		this.cdLctoPgmd = cdLctoPgmd;
	}

	/**
	 * Gets the ds tipo cons favorecido.
	 *
	 * @return the dsTipoConsFavorecido
	 */
	public String getDsTipoConsFavorecido() {
		return dsTipoConsFavorecido;
	}

	/**
	 * Sets the ds tipo cons favorecido.
	 *
	 * @param dsTipoConsFavorecido the dsTipoConsFavorecido to set
	 */
	public void setDsTipoConsFavorecido(String dsTipoConsFavorecido) {
		this.dsTipoConsFavorecido = dsTipoConsFavorecido;
	}

	/**
	 * Gets the cd feri loc.
	 *
	 * @return the cdFeriLoc
	 */
	public String getCdFeriLoc() {
		return cdFeriLoc;
	}

	/**
	 * Sets the cd feri loc.
	 *
	 * @param cdFeriLoc the cdFeriLoc to set
	 */
	public void setCdFeriLoc(String cdFeriLoc) {
		this.cdFeriLoc = cdFeriLoc;
	}

	/**
	 * Gets the o pg emissao.
	 *
	 * @return the oPgEmissao
	 */
	public String getoPgEmissao() {
		return oPgEmissao;
	}

	/**
	 * Sets the o pg emissao.
	 *
	 * @param oPgEmissao the oPgEmissao to set
	 */
	public void setoPgEmissao(String oPgEmissao) {
		this.oPgEmissao = oPgEmissao;
	}

	/**
	 * Gets the ds modalidade cferi loc.
	 *
	 * @return the dsModalidadeCferiLoc
	 */
	public String getDsModalidadeCferiLoc() {
		return dsModalidadeCferiLoc;
	}

	/**
	 * Sets the ds modalidade cferi loc.
	 *
	 * @param dsModalidadeCferiLoc the dsModalidadeCferiLoc to set
	 */
	public void setDsModalidadeCferiLoc(String dsModalidadeCferiLoc) {
		this.dsModalidadeCferiLoc = dsModalidadeCferiLoc;
	}

	/**
	 * Gets the ds mod tipo cons favorecido.
	 *
	 * @return the dsModTipoConsFavorecido
	 */
	public String getDsModTipoConsFavorecido() {
		return dsModTipoConsFavorecido;
	}

	/**
	 * Sets the ds mod tipo cons favorecido.
	 *
	 * @param dsModTipoConsFavorecido the dsModTipoConsFavorecido to set
	 */
	public void setDsModTipoConsFavorecido(String dsModTipoConsFavorecido) {
		this.dsModTipoConsFavorecido = dsModTipoConsFavorecido;
	}

	/**
	 * Gets the ds tri tipo cons favorecido.
	 *
	 * @return the dsTriTipoConsFavorecido
	 */
	public String getDsTriTipoConsFavorecido() {
		return dsTriTipoConsFavorecido;
	}

	/**
	 * Sets the ds tri tipo cons favorecido.
	 *
	 * @param dsTriTipoConsFavorecido the dsTriTipoConsFavorecido to set
	 */
	public void setDsTriTipoConsFavorecido(String dsTriTipoConsFavorecido) {
		this.dsTriTipoConsFavorecido = dsTriTipoConsFavorecido;
	}

	/**
	 * Gets the cd tri cferi loc.
	 *
	 * @return the cdTriCferiLoc
	 */
	public String getCdTriCferiLoc() {
		return cdTriCferiLoc;
	}

	/**
	 * Sets the cd tri cferi loc.
	 *
	 * @param cdTriCferiLoc the cdTriCferiLoc to set
	 */
	public void setCdTriCferiLoc(String cdTriCferiLoc) {
		this.cdTriCferiLoc = cdTriCferiLoc;
	}

	/**
	 * @param exibeDsFtiTipoTitulo the exibeDsFtiTipoTitulo to set
	 */
	public void setExibeDsFtiTipoTitulo(String exibeDsFtiTipoTitulo) {
		this.exibeDsFtiTipoTitulo = exibeDsFtiTipoTitulo;
	}

	/**
	 * @return the exibeDsFtiTipoTitulo
	 */
	public String getExibeDsFtiTipoTitulo() {
		return exibeDsFtiTipoTitulo;
	}
}