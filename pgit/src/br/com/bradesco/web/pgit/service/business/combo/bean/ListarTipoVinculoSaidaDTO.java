/*
 * Nome: br.com.bradesco.web.pgit.service.business.combo.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.combo.bean;

/**
 * Nome: ListarTipoVinculoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarTipoVinculoSaidaDTO {
	
	/** Atributo cdTipoVinculo. */
	private int cdTipoVinculo;
	
	/** Atributo dsTipoVinculo. */
	private String dsTipoVinculo;
	
	/**
	 * Listar tipo vinculo saida dto.
	 */
	public ListarTipoVinculoSaidaDTO(){
		super();
	}
	
	/**
	 * Listar tipo vinculo saida dto.
	 *
	 * @param cdTipoVinculo the cd tipo vinculo
	 * @param dsTipoVinculo the ds tipo vinculo
	 */
	public ListarTipoVinculoSaidaDTO(int cdTipoVinculo, String dsTipoVinculo){
		super();
		this.dsTipoVinculo = dsTipoVinculo;
		this.cdTipoVinculo = cdTipoVinculo;
	}
	
	/**
	 * Get: cdTipoVinculo.
	 *
	 * @return cdTipoVinculo
	 */
	public int getCdTipoVinculo() {
		return cdTipoVinculo;
	}
	
	/**
	 * Set: cdTipoVinculo.
	 *
	 * @param cdTipoVinculo the cd tipo vinculo
	 */
	public void setCdTipoVinculo(int cdTipoVinculo) {
		this.cdTipoVinculo = cdTipoVinculo;
	}
	
	/**
	 * Get: dsTipoVinculo.
	 *
	 * @return dsTipoVinculo
	 */
	public String getDsTipoVinculo() {
		return dsTipoVinculo;
	}
	
	/**
	 * Set: dsTipoVinculo.
	 *
	 * @param dsTipoVinculo the ds tipo vinculo
	 */
	public void setDsTipoVinculo(String dsTipoVinculo) {
		this.dsTipoVinculo = dsTipoVinculo;
	}
	
	
}
