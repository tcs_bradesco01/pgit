/*
 * Nome: br.com.bradesco.web.pgit.service.business.histcomplementar.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.histcomplementar.bean;

/**
 * Nome: ExcluirMantHistoricoComplementarEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ExcluirMantHistoricoComplementarEntradaDTO {
	
	/** Atributo codigoHistoricoComplementar. */
	private int codigoHistoricoComplementar;
	
	/** Atributo tipoServico. */
	private int tipoServico;
	
	/** Atributo modalidadeServico. */
	private int modalidadeServico;
	
	/** Atributo tipoRelacionamento. */
	private int tipoRelacionamento;
	
	/**
	 * Get: codigoHistoricoComplementar.
	 *
	 * @return codigoHistoricoComplementar
	 */
	public int getCodigoHistoricoComplementar() {
		return codigoHistoricoComplementar;
	}
	
	/**
	 * Set: codigoHistoricoComplementar.
	 *
	 * @param codigoHistoricoComplementar the codigo historico complementar
	 */
	public void setCodigoHistoricoComplementar(int codigoHistoricoComplementar) {
		this.codigoHistoricoComplementar = codigoHistoricoComplementar;
	}
	
	/**
	 * Get: modalidadeServico.
	 *
	 * @return modalidadeServico
	 */
	public int getModalidadeServico() {
		return modalidadeServico;
	}
	
	/**
	 * Set: modalidadeServico.
	 *
	 * @param modalidadeServico the modalidade servico
	 */
	public void setModalidadeServico(int modalidadeServico) {
		this.modalidadeServico = modalidadeServico;
	}
	
	/**
	 * Get: tipoRelacionamento.
	 *
	 * @return tipoRelacionamento
	 */
	public int getTipoRelacionamento() {
		return tipoRelacionamento;
	}
	
	/**
	 * Set: tipoRelacionamento.
	 *
	 * @param tipoRelacionamento the tipo relacionamento
	 */
	public void setTipoRelacionamento(int tipoRelacionamento) {
		this.tipoRelacionamento = tipoRelacionamento;
	}
	
	/**
	 * Get: tipoServico.
	 *
	 * @return tipoServico
	 */
	public int getTipoServico() {
		return tipoServico;
	}
	
	/**
	 * Set: tipoServico.
	 *
	 * @param tipoServico the tipo servico
	 */
	public void setTipoServico(int tipoServico) {
		this.tipoServico = tipoServico;
	}
	
	

}

