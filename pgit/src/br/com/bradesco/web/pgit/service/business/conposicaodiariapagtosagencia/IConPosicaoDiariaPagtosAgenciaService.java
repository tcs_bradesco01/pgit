/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/interfaz.ftl,v $
 * $Id: interfaz.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: interfaz.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.conposicaodiariapagtosagencia;

import java.util.List;

import br.com.bradesco.web.pgit.service.business.conposicaodiariapagtosagencia.bean.ConsultarPosDiaPagtoAgenciaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.conposicaodiariapagtosagencia.bean.ConsultarPosDiaPagtoAgenciaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.conposicaodiariapagtosagencia.bean.ConsultarSaldoAtualEntradaDTO;
import br.com.bradesco.web.pgit.service.business.conposicaodiariapagtosagencia.bean.ConsultarSaldoAtualSaidaDTO;
import br.com.bradesco.web.pgit.service.business.conposicaodiariapagtosagencia.bean.DetalharPagamentosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.conposicaodiariapagtosagencia.bean.DetalharPagamentosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.conposicaodiariapagtosagencia.bean.DetalharPorContaDebitoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.conposicaodiariapagtosagencia.bean.DetalharPorContaDebitoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.conposicaodiariapagtosagencia.bean.ValidarAgenciaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.conposicaodiariapagtosagencia.bean.ValidarAgenciaSaidaDTO;


/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Interface do adaptador: ConPosicaoDiariaPagtosAgencia
 * </p>
 * 
 * @comment CODIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public interface IConPosicaoDiariaPagtosAgenciaService {

	/**
	 * Consultar posicao diaria pagtos agencia.
	 *
	 * @param entradaDTO the entrada dto
	 * @return the list< consultar pos dia pagto agencia saida dt o>
	 */
	List<ConsultarPosDiaPagtoAgenciaSaidaDTO> consultarPosicaoDiariaPagtosAgencia(ConsultarPosDiaPagtoAgenciaEntradaDTO entradaDTO);
    
    /**
     * Detalhar por conta debito.
     *
     * @param entradaDTO the entrada dto
     * @return the list< detalhar por conta debito saida dt o>
     */
    List<DetalharPorContaDebitoSaidaDTO> detalharPorContaDebito(DetalharPorContaDebitoEntradaDTO entradaDTO);
    
    /**
     * Detalhar pagamentos.
     *
     * @param entradaDTO the entrada dto
     * @return the list< detalhar pagamentos saida dt o>
     */
    List<DetalharPagamentosSaidaDTO> detalharPagamentos(DetalharPagamentosEntradaDTO entradaDTO);
    
    /**
     * Consultar saldo atual.
     *
     * @param entradaDTO the entrada dto
     * @return the consultar saldo atual saida dto
     */
    ConsultarSaldoAtualSaidaDTO consultarSaldoAtual(ConsultarSaldoAtualEntradaDTO entradaDTO);

    /**
     * Validar Agencia.
     *
     * @param entradaDTO the entrada dto
     * @return the validar agencia saida dto
     */
    ValidarAgenciaSaidaDTO validarAgencia(ValidarAgenciaEntradaDTO entradaDTO);
}

