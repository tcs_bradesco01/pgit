/*
 * Nome: br.com.bradesco.web.pgit.service.business.arquivoremessa.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.arquivoremessa.bean;

import java.math.BigDecimal;

/**
 * Nome: ConsultarArquivoRecuperacaoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarArquivoRecuperacaoSaidaDTO {

	/** Atributo check. */
	private boolean check;
	
	/** Atributo codMensagem. */
	private String codMensagem;
	
	/** Atributo mensagem. */
	private String mensagem;
	
	/** Atributo cdPessoaJuridicaContrato. */
	private Long cdPessoaJuridicaContrato;
	
	/** Atributo dsPessoaJuridicaContrato. */
	private String dsPessoaJuridicaContrato;
	
	/** Atributo cdTipoContratoNegocio. */
	private Integer cdTipoContratoNegocio;
	
	/** Atributo dsTipoContratoNegocio. */
	private String dsTipoContratoNegocio;
	
	/** Atributo nrSequenciaContratoNegocio. */
	private Long nrSequenciaContratoNegocio;
	
	/** Atributo dsContrato. */
	private String dsContrato;
	
	/** Atributo cdPessoa. */
	private Long cdPessoa;
	
	/** Atributo cdCnpjCpfPessoa. */
	private String cdCnpjCpfPessoa;
	
	/** Atributo dsPessoa. */
	private String dsPessoa;
	
	/** Atributo cdTipoLayoutArquivo. */
	private Integer cdTipoLayoutArquivo;
	
	/** Atributo dsTipoLayoutArquivo. */
	private String dsTipoLayoutArquivo;
	
	/** Atributo cdClienteTransfArquivo. */
	private Long cdClienteTransfArquivo;
	
	/** Atributo dsArquivoRemessaEnvio. */
	private String dsArquivoRemessaEnvio;
	
	/** Atributo nrArquivoRemessa. */
	private Long nrArquivoRemessa;
	
	/** Atributo hrInclusaoRegistro. */
	private String hrInclusaoRegistro;
	
	/** Atributo dataFormatada. */
	private String dataFormatada;
	
	/** Atributo dsSituacaoProcessamentoRemessa. */
	private String dsSituacaoProcessamentoRemessa;
	
	/** Atributo dsCondicaoProcessamentoRemessa. */
	private String dsCondicaoProcessamentoRemessa;
	
	/** Atributo dsMeioTransmissao. */
	private String dsMeioTransmissao;
	
	/** Atributo dsAmbiente. */
	private String dsAmbiente;
	
	/** Atributo qtRegistroArqRemessa. */
	private String qtRegistroArqRemessa;
	
	/** Atributo vlRegistroArqRemessa. */
	private BigDecimal vlRegistroArqRemessa;
	
	/**
	 * Get: cdClienteTransfArquivo.
	 *
	 * @return cdClienteTransfArquivo
	 */
	public Long getCdClienteTransfArquivo() {
		return cdClienteTransfArquivo;
	}
	
	/**
	 * Set: cdClienteTransfArquivo.
	 *
	 * @param cdClienteTransfArquivo the cd cliente transf arquivo
	 */
	public void setCdClienteTransfArquivo(Long cdClienteTransfArquivo) {
		this.cdClienteTransfArquivo = cdClienteTransfArquivo;
	}
	
	/**
	 * Get: cdCnpjCpfPessoa.
	 *
	 * @return cdCnpjCpfPessoa
	 */
	public String getCdCnpjCpfPessoa() {
		return cdCnpjCpfPessoa;
	}
	
	/**
	 * Set: cdCnpjCpfPessoa.
	 *
	 * @param cdCnpjCpfPessoa the cd cnpj cpf pessoa
	 */
	public void setCdCnpjCpfPessoa(String cdCnpjCpfPessoa) {
		this.cdCnpjCpfPessoa = cdCnpjCpfPessoa;
	}
	
	/**
	 * Get: cdPessoa.
	 *
	 * @return cdPessoa
	 */
	public Long getCdPessoa() {
		return cdPessoa;
	}
	
	/**
	 * Set: cdPessoa.
	 *
	 * @param cdPessoa the cd pessoa
	 */
	public void setCdPessoa(Long cdPessoa) {
		this.cdPessoa = cdPessoa;
	}
	
	/**
	 * Get: cdPessoaJuridicaContrato.
	 *
	 * @return cdPessoaJuridicaContrato
	 */
	public Long getCdPessoaJuridicaContrato() {
		return cdPessoaJuridicaContrato;
	}
	
	/**
	 * Set: cdPessoaJuridicaContrato.
	 *
	 * @param cdPessoaJuridicaContrato the cd pessoa juridica contrato
	 */
	public void setCdPessoaJuridicaContrato(Long cdPessoaJuridicaContrato) {
		this.cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
	}
	
	/**
	 * Get: cdTipoContratoNegocio.
	 *
	 * @return cdTipoContratoNegocio
	 */
	public Integer getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}
	
	/**
	 * Set: cdTipoContratoNegocio.
	 *
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}
	
	/**
	 * Get: cdTipoLayoutArquivo.
	 *
	 * @return cdTipoLayoutArquivo
	 */
	public Integer getCdTipoLayoutArquivo() {
		return cdTipoLayoutArquivo;
	}
	
	/**
	 * Set: cdTipoLayoutArquivo.
	 *
	 * @param cdTipoLayoutArquivo the cd tipo layout arquivo
	 */
	public void setCdTipoLayoutArquivo(Integer cdTipoLayoutArquivo) {
		this.cdTipoLayoutArquivo = cdTipoLayoutArquivo;
	}
	
	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}
	
	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}
	
	/**
	 * Get: dsAmbiente.
	 *
	 * @return dsAmbiente
	 */
	public String getDsAmbiente() {
		return dsAmbiente;
	}
	
	/**
	 * Set: dsAmbiente.
	 *
	 * @param dsAmbiente the ds ambiente
	 */
	public void setDsAmbiente(String dsAmbiente) {
		this.dsAmbiente = dsAmbiente;
	}
	
	/**
	 * Get: dsArquivoRemessaEnvio.
	 *
	 * @return dsArquivoRemessaEnvio
	 */
	public String getDsArquivoRemessaEnvio() {
		return dsArquivoRemessaEnvio;
	}
	
	/**
	 * Set: dsArquivoRemessaEnvio.
	 *
	 * @param dsArquivoRemessaEnvio the ds arquivo remessa envio
	 */
	public void setDsArquivoRemessaEnvio(String dsArquivoRemessaEnvio) {
		this.dsArquivoRemessaEnvio = dsArquivoRemessaEnvio;
	}
	
	/**
	 * Get: dsCondicaoProcessamentoRemessa.
	 *
	 * @return dsCondicaoProcessamentoRemessa
	 */
	public String getDsCondicaoProcessamentoRemessa() {
		return dsCondicaoProcessamentoRemessa;
	}
	
	/**
	 * Set: dsCondicaoProcessamentoRemessa.
	 *
	 * @param dsCondicaoProcessamentoRemessa the ds condicao processamento remessa
	 */
	public void setDsCondicaoProcessamentoRemessa(
			String dsCondicaoProcessamentoRemessa) {
		this.dsCondicaoProcessamentoRemessa = dsCondicaoProcessamentoRemessa;
	}
	
	/**
	 * Get: dsContrato.
	 *
	 * @return dsContrato
	 */
	public String getDsContrato() {
		return dsContrato;
	}
	
	/**
	 * Set: dsContrato.
	 *
	 * @param dsContrato the ds contrato
	 */
	public void setDsContrato(String dsContrato) {
		this.dsContrato = dsContrato;
	}
	
	/**
	 * Get: dsMeioTransmissao.
	 *
	 * @return dsMeioTransmissao
	 */
	public String getDsMeioTransmissao() {
		return dsMeioTransmissao;
	}
	
	/**
	 * Set: dsMeioTransmissao.
	 *
	 * @param dsMeioTransmissao the ds meio transmissao
	 */
	public void setDsMeioTransmissao(String dsMeioTransmissao) {
		this.dsMeioTransmissao = dsMeioTransmissao;
	}
	
	/**
	 * Get: dsPessoa.
	 *
	 * @return dsPessoa
	 */
	public String getDsPessoa() {
		return dsPessoa;
	}
	
	/**
	 * Set: dsPessoa.
	 *
	 * @param dsPessoa the ds pessoa
	 */
	public void setDsPessoa(String dsPessoa) {
		this.dsPessoa = dsPessoa;
	}
	
	/**
	 * Get: dsPessoaJuridicaContrato.
	 *
	 * @return dsPessoaJuridicaContrato
	 */
	public String getDsPessoaJuridicaContrato() {
		return dsPessoaJuridicaContrato;
	}
	
	/**
	 * Set: dsPessoaJuridicaContrato.
	 *
	 * @param dsPessoaJuridicaContrato the ds pessoa juridica contrato
	 */
	public void setDsPessoaJuridicaContrato(String dsPessoaJuridicaContrato) {
		this.dsPessoaJuridicaContrato = dsPessoaJuridicaContrato;
	}
	
	/**
	 * Get: dsSituacaoProcessamentoRemessa.
	 *
	 * @return dsSituacaoProcessamentoRemessa
	 */
	public String getDsSituacaoProcessamentoRemessa() {
		return dsSituacaoProcessamentoRemessa;
	}
	
	/**
	 * Set: dsSituacaoProcessamentoRemessa.
	 *
	 * @param dsSituacaoProcessamentoRemessa the ds situacao processamento remessa
	 */
	public void setDsSituacaoProcessamentoRemessa(
			String dsSituacaoProcessamentoRemessa) {
		this.dsSituacaoProcessamentoRemessa = dsSituacaoProcessamentoRemessa;
	}
	
	/**
	 * Get: dsTipoContratoNegocio.
	 *
	 * @return dsTipoContratoNegocio
	 */
	public String getDsTipoContratoNegocio() {
		return dsTipoContratoNegocio;
	}
	
	/**
	 * Set: dsTipoContratoNegocio.
	 *
	 * @param dsTipoContratoNegocio the ds tipo contrato negocio
	 */
	public void setDsTipoContratoNegocio(String dsTipoContratoNegocio) {
		this.dsTipoContratoNegocio = dsTipoContratoNegocio;
	}
	
	/**
	 * Get: dsTipoLayoutArquivo.
	 *
	 * @return dsTipoLayoutArquivo
	 */
	public String getDsTipoLayoutArquivo() {
		return dsTipoLayoutArquivo;
	}
	
	/**
	 * Set: dsTipoLayoutArquivo.
	 *
	 * @param dsTipoLayoutArquivo the ds tipo layout arquivo
	 */
	public void setDsTipoLayoutArquivo(String dsTipoLayoutArquivo) {
		this.dsTipoLayoutArquivo = dsTipoLayoutArquivo;
	}
	
	/**
	 * Get: hrInclusaoRegistro.
	 *
	 * @return hrInclusaoRegistro
	 */
	public String getHrInclusaoRegistro() {
		return hrInclusaoRegistro;
	}
	
	/**
	 * Set: hrInclusaoRegistro.
	 *
	 * @param hrInclusaoRegistro the hr inclusao registro
	 */
	public void setHrInclusaoRegistro(String hrInclusaoRegistro) {
		this.hrInclusaoRegistro = hrInclusaoRegistro;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	/**
	 * Get: nrArquivoRemessa.
	 *
	 * @return nrArquivoRemessa
	 */
	public Long getNrArquivoRemessa() {
		return nrArquivoRemessa;
	}
	
	/**
	 * Set: nrArquivoRemessa.
	 *
	 * @param nrArquivoRemessa the nr arquivo remessa
	 */
	public void setNrArquivoRemessa(Long nrArquivoRemessa) {
		this.nrArquivoRemessa = nrArquivoRemessa;
	}
	
	/**
	 * Get: nrSequenciaContratoNegocio.
	 *
	 * @return nrSequenciaContratoNegocio
	 */
	public Long getNrSequenciaContratoNegocio() {
		return nrSequenciaContratoNegocio;
	}
	
	/**
	 * Set: nrSequenciaContratoNegocio.
	 *
	 * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
	 */
	public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}
	
	/**
	 * Get: qtRegistroArqRemessa.
	 *
	 * @return qtRegistroArqRemessa
	 */
	public String getQtRegistroArqRemessa() {
		return qtRegistroArqRemessa;
	}
	
	/**
	 * Set: qtRegistroArqRemessa.
	 *
	 * @param qtRegistroArqRemessa the qt registro arq remessa
	 */
	public void setQtRegistroArqRemessa(String qtRegistroArqRemessa) {
		this.qtRegistroArqRemessa = qtRegistroArqRemessa;
	}
	
	/**
	 * Get: vlRegistroArqRemessa.
	 *
	 * @return vlRegistroArqRemessa
	 */
	public BigDecimal getVlRegistroArqRemessa() {
		return vlRegistroArqRemessa;
	}
	
	/**
	 * Set: vlRegistroArqRemessa.
	 *
	 * @param vlRegistroArqRemessa the vl registro arq remessa
	 */
	public void setVlRegistroArqRemessa(BigDecimal vlRegistroArqRemessa) {
		this.vlRegistroArqRemessa = vlRegistroArqRemessa;
	}
	
	/**
	 * Is check.
	 *
	 * @return true, if is check
	 */
	public boolean isCheck() {
		return check;
	}
	
	/**
	 * Set: check.
	 *
	 * @param check the check
	 */
	public void setCheck(boolean check) {
		this.check = check;
	}
	
	/**
	 * Get: dataFormatada.
	 *
	 * @return dataFormatada
	 */
	public String getDataFormatada() {
		return dataFormatada;
	}
	
	/**
	 * Set: dataFormatada.
	 *
	 * @param dataFormatada the data formatada
	 */
	public void setDataFormatada(String dataFormatada) {
		this.dataFormatada = dataFormatada;
	}
	
	
}
