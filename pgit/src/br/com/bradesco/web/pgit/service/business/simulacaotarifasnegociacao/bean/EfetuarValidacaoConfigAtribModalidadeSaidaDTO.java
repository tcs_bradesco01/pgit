/*
 * Nome: br.com.bradesco.web.pgit.service.business.simulacaotarifasnegociacao.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.simulacaotarifasnegociacao.bean;

import br.com.bradesco.web.pgit.service.data.pdc.efetuarvalidacaoconfigatribmodalidade.response.EfetuarValidacaoConfigAtribModalidadeResponse;

/**
 * Nome: EfetuarValidacaoConfigAtribModalidadeSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class EfetuarValidacaoConfigAtribModalidadeSaidaDTO {

    private String codMensagem;

    private String mensagem;
    
    
    public EfetuarValidacaoConfigAtribModalidadeSaidaDTO() {
    	super();
    }

	public EfetuarValidacaoConfigAtribModalidadeSaidaDTO(EfetuarValidacaoConfigAtribModalidadeResponse response) {
		super();
		this.codMensagem = response.getCodMensagem();
		this.mensagem = response.getMensagem();
	}


	/**
	 * Nome: getCodMensagem
	 *
	 * @exception
	 * @throws
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}

	/**
	 * Nome: setCodMensagem
	 *
	 * @exception
	 * @throws
	 * @param codMensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}

	/**
	 * Nome: getMensagem
	 *
	 * @exception
	 * @throws
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}

	/**
	 * Nome: setMensagem
	 *
	 * @exception
	 * @throws
	 * @param mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

}