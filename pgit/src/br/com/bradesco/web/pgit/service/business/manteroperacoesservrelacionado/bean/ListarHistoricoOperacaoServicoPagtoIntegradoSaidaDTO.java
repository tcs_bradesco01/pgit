/*
 * Nome: br.com.bradesco.web.pgit.service.business.manteroperacoesservrelacionado.bean

 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 10/03/2017
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.manteroperacoesservrelacionado.bean;

import java.util.List;

/**
 * Nome: ListarHistoricoOperacaoServicoPagtoIntegradoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>
 * 
 * @author : todo!
 * 
 * @version :
 */
public class ListarHistoricoOperacaoServicoPagtoIntegradoSaidaDTO {

    /**
     * Atributo String
     */
    private String codMensagem = null;

    /**
     * Atributo String
     */
    private String mensagem = null;

    /**
     * Atributo Integer
     */
    private Integer qtRegistroSaida = null;

    /**
     * Atributo List<ListarHistoricoOperacaoServicoPagtoIntegradoOcorrenciasDTO>
     */
    private List<ListarHistoricoOperacaoServicoPagtoIntegradoOcorrenciasDTO> ocorrencias = null;

    /**
     * Nome: getCodMensagem
     * 
     * @return codMensagem
     */
    public String getCodMensagem() {
        return codMensagem;
    }

    /**
     * Nome: setCodMensagem
     * 
     * @param codMensagem
     */
    public void setCodMensagem(String codMensagem) {
        this.codMensagem = codMensagem;
    }

    /**
     * Nome: getMensagem
     * 
     * @return mensagem
     */
    public String getMensagem() {
        return mensagem;
    }

    /**
     * Nome: setMensagem
     * 
     * @param mensagem
     */
    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }

    /**
     * Nome: getQtRegistroSaida
     * 
     * @return qtRegistroSaida
     */
    public Integer getQtRegistroSaida() {
        return qtRegistroSaida;
    }

    /**
     * Nome: setQtRegistroSaida
     * 
     * @param qtRegistroSaida
     */
    public void setQtRegistroSaida(Integer qtRegistroSaida) {
        this.qtRegistroSaida = qtRegistroSaida;
    }

    /**
     * Nome: getOcorrencias
     * 
     * @return ocorrencias
     */
    public List<ListarHistoricoOperacaoServicoPagtoIntegradoOcorrenciasDTO> getOcorrencias() {
        return ocorrencias;
    }

    /**
     * Nome: setOcorrencias
     * 
     * @param ocorrencias
     */
    public void setOcorrencias(List<ListarHistoricoOperacaoServicoPagtoIntegradoOcorrenciasDTO> ocorrencias) {
        this.ocorrencias = ocorrencias;
    }

}
