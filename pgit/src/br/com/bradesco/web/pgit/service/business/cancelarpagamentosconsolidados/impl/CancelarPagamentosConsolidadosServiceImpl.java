/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/implementation.ftl,v $
 * $Id: implementation.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: implementation.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */
 
package br.com.bradesco.web.pgit.service.business.cancelarpagamentosconsolidados.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import br.com.bradesco.web.pgit.service.business.cancelarpagamentosconsolidados.ICancelarPagamentosConsolidadosService;
import br.com.bradesco.web.pgit.service.business.cancelarpagamentosconsolidados.ICancelarPagamentosConsolidadosServiceConstants;
import br.com.bradesco.web.pgit.service.business.cancelarpagamentosconsolidados.bean.CancelarPagtosIntegralEntradaDTO;
import br.com.bradesco.web.pgit.service.business.cancelarpagamentosconsolidados.bean.CancelarPagtosIntegralSaidaDTO;
import br.com.bradesco.web.pgit.service.business.cancelarpagamentosconsolidados.bean.CancelarPagtosParcialEntradaDTO;
import br.com.bradesco.web.pgit.service.business.cancelarpagamentosconsolidados.bean.CancelarPagtosParcialSaidaDTO;
import br.com.bradesco.web.pgit.service.business.cancelarpagamentosconsolidados.bean.ConsultarPagtosConsParaCancelamentoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.cancelarpagamentosconsolidados.bean.ConsultarPagtosConsParaCancelamentoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.cancelarpagamentosconsolidados.bean.DetalharPagtosConsParaCancelamentoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.cancelarpagamentosconsolidados.bean.DetalharPagtosConsParaCancelamentoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.cancelarpagamentosconsolidados.bean.OcorrenciasPagtosConsParaCancelamentDTO;
import br.com.bradesco.web.pgit.service.data.pdc.FactoryAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.cancelarpagtosintegral.request.CancelarPagtosIntegralRequest;
import br.com.bradesco.web.pgit.service.data.pdc.cancelarpagtosintegral.response.CancelarPagtosIntegralResponse;
import br.com.bradesco.web.pgit.service.data.pdc.cancelarpagtosparcial.request.CancelarPagtosParcialRequest;
import br.com.bradesco.web.pgit.service.data.pdc.cancelarpagtosparcial.response.CancelarPagtosParcialResponse;
import br.com.bradesco.web.pgit.service.data.pdc.consultarpagtosconsparacancelamento.request.ConsultarPagtosConsParaCancelamentoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarpagtosconsparacancelamento.response.ConsultarPagtosConsParaCancelamentoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.detalharpagtosconsparacancelamento.request.DetalharPagtosConsParaCancelamentoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.detalharpagtosconsparacancelamento.response.DetalharPagtosConsParaCancelamentoResponse;
import br.com.bradesco.web.pgit.utils.CpfCnpjUtils;
import br.com.bradesco.web.pgit.utils.PgitUtil;
import br.com.bradesco.web.pgit.view.converters.FormatarData;


/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Implementa��o do adaptador: CancelarPagamentosConsolidados
 * </p>
 * 
 * @comment C�DIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public class CancelarPagamentosConsolidadosServiceImpl implements ICancelarPagamentosConsolidadosService {
	
	/** Atributo nf. */
	private java.text.NumberFormat nf = java.text.NumberFormat.getInstance( new Locale( "pt_br" ) );
	
	/** Atributo factoryAdapter. */
	private FactoryAdapter factoryAdapter;	
		
	
    /**
     * Get: factoryAdapter.
     *
     * @return factoryAdapter
     */
    public FactoryAdapter getFactoryAdapter() {
		return factoryAdapter;
	}

	/**
	 * Set: factoryAdapter.
	 *
	 * @param factoryAdapter the factory adapter
	 */
	public void setFactoryAdapter(FactoryAdapter factoryAdapter) {
		this.factoryAdapter = factoryAdapter;
	}
	
	

	/**
     * Construtor.
     */
    public CancelarPagamentosConsolidadosServiceImpl() {
    }
    
    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.cancelarpagamentosconsolidados.ICancelarPagamentosConsolidadosService#consultarPagtosConsParaCancelamento(br.com.bradesco.web.pgit.service.business.cancelarpagamentosconsolidados.bean.ConsultarPagtosConsParaCancelamentoEntradaDTO)
     */
    public List<ConsultarPagtosConsParaCancelamentoSaidaDTO> consultarPagtosConsParaCancelamento (ConsultarPagtosConsParaCancelamentoEntradaDTO entrada){
       	
    	List<ConsultarPagtosConsParaCancelamentoSaidaDTO> listaRetorno = new ArrayList<ConsultarPagtosConsParaCancelamentoSaidaDTO>();
    	ConsultarPagtosConsParaCancelamentoRequest request = new ConsultarPagtosConsParaCancelamentoRequest();
    	ConsultarPagtosConsParaCancelamentoResponse response = new ConsultarPagtosConsParaCancelamentoResponse();
    	
    	request.setCdTipoPesquisa(PgitUtil.verificaIntegerNulo(entrada.getCdTipoPesquisa()));
        request.setCdAgendamentoPagosNaoPagos(PgitUtil.verificaIntegerNulo(entrada.getCdAgendamentoPagosNaoPagos()));
        request.setNrOcorrencias(ICancelarPagamentosConsolidadosServiceConstants.NUMERO_OCORRENCIAS_CONSULTAR);
        request.setCdPessoaJuridica(PgitUtil.verificaLongNulo(entrada.getCdPessoaJuridica()));
        request.setCdTipoContrato(PgitUtil.verificaIntegerNulo(entrada.getCdTipoContrato()));
        request.setNrSequencialContrato(PgitUtil.verificaLongNulo(entrada.getNrSequencialContrato()));
        request.setDtInicialPagamento(PgitUtil.verificaStringNula(entrada.getDtInicialPagamento()));
        request.setDtFinalPagamento(PgitUtil.verificaStringNula(entrada.getDtFinalPagamento()));
        request.setNrRemessa(PgitUtil.verificaLongNulo(entrada.getNrRemessa()));
        request.setCdBanco(PgitUtil.verificaIntegerNulo(entrada.getCdBanco()));
        request.setCdAgencia(PgitUtil.verificaIntegerNulo(entrada.getCdAgencia()));
        request.setCdConta(PgitUtil.verificaLongNulo(entrada.getCdConta()));
        request.setCdDigitoConta(PgitUtil.DIGITO_CONTA);
        request.setCdProdutoServicoOperacao(PgitUtil.verificaIntegerNulo(entrada.getCdProdutoServicoOperacao()));
        request.setCdProdutoServicoRelacionado(PgitUtil.verificaIntegerNulo(entrada.getCdProdutoServicoRelacionado()));
        request.setCdSituacaoOperacaoPagamento(PgitUtil.verificaIntegerNulo(entrada.getCdSituacaoOperacaoPagamento()));
        request.setCdMotivoSituacaoPagamento(PgitUtil.verificaIntegerNulo(entrada.getCdMotivoSituacaoPagamento()));        
        request.setCdTituloPgtoRastreado(0); 
        
    	response = getFactoryAdapter().getConsultarPagtosConsParaCancelamentoPDCAdapter().invokeProcess(request);
    	ConsultarPagtosConsParaCancelamentoSaidaDTO saida;
    	
    	 for(int i=0;i<response.getOcorrenciasCount();i++){
    		 saida = new ConsultarPagtosConsParaCancelamentoSaidaDTO();
    		 saida.setCodMensagem(PgitUtil.verificaStringNula(response.getCodMensagem()));
             saida.setMensagem(PgitUtil.verificaStringNula(response.getMensagem()));     
             saida.setNrConsultas(response.getNrConsultas());
             
        	 saida.setCdPessoaJuridica(PgitUtil.verificaLongNulo(response.getOcorrencias(i).getCdPessoaJuridica()));
        	 saida.setDsRazaoSocial(PgitUtil.verificaStringNula(response.getOcorrencias(i).getDsRazaoSocial()));
        	 saida.setCdTipoContrato(PgitUtil.verificaIntegerNulo(response.getOcorrencias(i).getCdTipoContrato()));
        	 saida.setNrContrato(PgitUtil.verificaLongNulo(response.getOcorrencias(i).getNrContrato()));
        	 saida.setNrCnpjCpf(PgitUtil.verificaLongNulo(response.getOcorrencias(i).getNrCnpjCpf()));
        	 saida.setNrFilialCnpjCpf(PgitUtil.verificaIntegerNulo(response.getOcorrencias(i).getNrFilialCnpjCpf()));
        	 saida.setCdDigitoCnpjCpf(PgitUtil.verificaIntegerNulo(response.getOcorrencias(i).getCdDigitoCnpjCpf()));
        	 saida.setNrRemessa(PgitUtil.verificaLongNulo(response.getOcorrencias(i).getNrRemessa()));
        	 saida.setDtPagamento( response.getOcorrencias(i).getDtPagamento() != null && !response.getOcorrencias(i).getDtPagamento().equals("") ? 
        			 FormatarData.formatarData(response.getOcorrencias(i).getDtPagamento()) : "");        	 
        	 saida.setCdBanco(PgitUtil.verificaIntegerNulo(response.getOcorrencias(i).getCdBanco()));        	 
        	 saida.setCdAgencia(PgitUtil.verificaIntegerNulo(response.getOcorrencias(i).getCdAgencia()));
        	 saida.setCdDigitoAgencia(PgitUtil.verificaIntegerNulo(response.getOcorrencias(i).getCdDigitoAgencia()));
        	 saida.setCdConta(PgitUtil.verificaLongNulo(response.getOcorrencias(i).getCdConta()));
        	 saida.setCdDigitoConta(PgitUtil.verificaStringNula(response.getOcorrencias(i).getCdDigitoConta()));
        	 saida.setCdProdutoServicoOperacao(PgitUtil.verificaIntegerNulo(response.getOcorrencias(i).getCdProdutoServicoOperacao()));
        	 saida.setDsResumoProdutoServico(PgitUtil.verificaStringNula(response.getOcorrencias(i).getDsResumoProdutoServico()));
        	 saida.setCdProdutoServicoRelacionado(PgitUtil.verificaIntegerNulo(response.getOcorrencias(i).getCdProdutoServicoRelacionado()));
        	 saida.setDsOperacaoProdutoServico(PgitUtil.verificaStringNula(response.getOcorrencias(i).getDsOperacaoProdutoServico()));
        	 saida.setCdSituacaoOperacaoPagamento(PgitUtil.verificaIntegerNulo(response.getOcorrencias(i).getCdSituacaoOperacaoPagamento()));        	 
        	 saida.setDsSituacaoOperacaoPagamento(PgitUtil.verificaStringNula(response.getOcorrencias(i).getDsSituacaoOperacaoPagamento()));
        	 saida.setQtPagamento(PgitUtil.verificaLongNulo(response.getOcorrencias(i).getQtPagamento()));
        	 saida.setVlPagamento(PgitUtil.verificaBigDecimalNulo(response.getOcorrencias(i).getVlEfetivacaoPagamentoCliente()));        	 
        	 saida.setCpfCnpjFormatado(CpfCnpjUtils.formatCpfCnpjCompleto(saida.getNrCnpjCpf(), saida.getNrFilialCnpjCpf(), saida.getCdDigitoCnpjCpf()));           
             saida.setContaFormatado(PgitUtil.formatBancoAgenciaConta(saida.getCdBanco(), saida.getCdAgencia(), saida.getCdDigitoAgencia(), saida.getCdConta(), saida.getCdDigitoConta(), true));
             
             saida.setVlEfetivacaoPagamentoCliente(response.getOcorrencias(i).getVlEfetivacaoPagamentoCliente());
             saida.setCdPessoaContratoDebito(response.getOcorrencias(i).getCdPessoaContratoDebito());
             saida.setCdTipoContratoDebito(response.getOcorrencias(i).getCdTipoContratoDebito());
             saida.setNrSequenciaContratoDebito(response.getOcorrencias(i).getNrSequenciaContratoDebito());
             saida.setQtPagamentoFormatado(nf.format(saida.getQtPagamento()));
        	 listaRetorno.add(saida);
             
    	 }
    	
    	return listaRetorno;
    }
    
    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.cancelarpagamentosconsolidados.ICancelarPagamentosConsolidadosService#detalharPagtosConsParaCancelamento(br.com.bradesco.web.pgit.service.business.cancelarpagamentosconsolidados.bean.DetalharPagtosConsParaCancelamentoEntradaDTO)
     */
    public DetalharPagtosConsParaCancelamentoSaidaDTO detalharPagtosConsParaCancelamento(DetalharPagtosConsParaCancelamentoEntradaDTO entradaDTO){
    	DetalharPagtosConsParaCancelamentoRequest request = new DetalharPagtosConsParaCancelamentoRequest();
    	DetalharPagtosConsParaCancelamentoResponse response = new DetalharPagtosConsParaCancelamentoResponse();

    	request.setNrOcorrencias(ICancelarPagamentosConsolidadosServiceConstants.NUMERO_OCORRENCIAS_DETALHAR);
    	request.setCdPessoaJuridicaContrato(PgitUtil.verificaLongNulo(entradaDTO.getCdPessoaJuridicaContrato()));
    	request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoContratoNegocio()));
    	request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(entradaDTO.getNrSequenciaContratoNegocio()));
    	request.setNrCnpjCpf(PgitUtil.verificaLongNulo(entradaDTO.getNrCnpjCpf()));
    	request.setNrFilialcnpjCpf(PgitUtil.verificaIntegerNulo(entradaDTO.getNrFilialcnpjCpf()));
    	request.setNrControleCnpjCpf(PgitUtil.verificaIntegerNulo(entradaDTO.getNrControleCnpjCpf()));
    	request.setNrArquivoRemessaPagamento(PgitUtil.verificaLongNulo(entradaDTO.getNrArquivoRemessaPagamento()));
    	request.setDtCreditoPagamento(PgitUtil.verificaStringNula(entradaDTO.getDtCreditoPagamento()));
    	request.setCdBanco(PgitUtil.verificaIntegerNulo(entradaDTO.getCdBanco()));
    	request.setCdAgencia(PgitUtil.verificaIntegerNulo(entradaDTO.getCdAgencia()));
    	request.setCdConta(PgitUtil.verificaLongNulo(entradaDTO.getCdConta()));
    	request.setCdDigitoConta(PgitUtil.complementaDigitoConta(entradaDTO.getCdDigitoConta()));
    	request.setCdProdutoServicoOperacao(PgitUtil.verificaIntegerNulo(entradaDTO.getCdProdutoServicoOperacao()));
    	request.setCdProdutoServicoRelacionado(PgitUtil.verificaIntegerNulo(entradaDTO.getCdProdutoServicoRelacionado()));
    	request.setCdSituacaoOperacaoPagamento(PgitUtil.verificaIntegerNulo(entradaDTO.getCdSituacaoOperacaoPagamento()));
    	request.setCdAgendadoPagaoNaoPago(PgitUtil.verificaIntegerNulo(entradaDTO.getCdAgendadoPagaoNaoPago()));
    	request.setCdPessoaContratoDebito(PgitUtil.verificaLongNulo(entradaDTO.getCdPessoaContratoDebito()));
    	request.setCdTipoContratoDebito(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoContratoDebito()));
    	request.setNrSequenciaContratoDebito(PgitUtil.verificaLongNulo(entradaDTO.getNrSequenciaContratoDebito()));
    	request.setCdTituloPgtoRastreado(0);
    	request.setCdIndicadorAutorizacaoPagador(PgitUtil.verificaIntegerNulo(entradaDTO.getCdIndicadorAutorizacaoPagador()));
    	request.setNrLoteInternoPagamento(PgitUtil.verificaLongNulo(entradaDTO.getNrLoteInternoPagamento()));

    	response = getFactoryAdapter().getDetalharPagtosConsParaCancelamentoPDCAdapter().invokeProcess(request);
    	
    	DetalharPagtosConsParaCancelamentoSaidaDTO saida = new DetalharPagtosConsParaCancelamentoSaidaDTO();
    	
    	saida.setCodMensagem(response.getCodMensagem());
    	saida.setMensagem(response.getMensagem());
    	saida.setDsBancoDebito(response.getDsBancoDebito());
    	saida.setDsAgenciaDebito(response.getDsAgenciaDebito());
    	saida.setDsTipoContaDebito(response.getDsTipoContaDebito());
    	saida.setDsContrato(response.getDsContrato());
    	saida.setDsSituacaoContrato(response.getDsSituacaoContrato());
    	saida.setNumeroConsultas(response.getNumeroConsultas());
    	saida.setNomeCliente(response.getNmCliente());
      	
		List<OcorrenciasPagtosConsParaCancelamentDTO> ocorrenciasPagtosConsParaCancelamentDTO = new ArrayList<OcorrenciasPagtosConsParaCancelamentDTO>();
		OcorrenciasPagtosConsParaCancelamentDTO ocorrencia;

    	for(int i=0;i<response.getOcorrenciasCount();i++){
    		ocorrencia = new OcorrenciasPagtosConsParaCancelamentDTO();
 
		    ocorrencia.setNrPagamento(response.getOcorrencias(i).getNrPagamento());
		    ocorrencia.setVlPagamento(response.getOcorrencias(i).getVlPagamento());
		    ocorrencia.setCdCnpjCpfFavorecido(response.getOcorrencias(i).getCdCnpjCpfFavorecido());
		    ocorrencia.setCdFilialCnpjCpfFavorecido(response.getOcorrencias(i).getCdFilialCnpjCpfFavorecido());
		    ocorrencia.setCdControleCnpjCpfFavorecido(response.getOcorrencias(i).getCdControleCnpjCpfFavorecido());
		    ocorrencia.setCdFavorecido(response.getOcorrencias(i).getCdFavorecido() == 0L ? "" : String.valueOf(response.getOcorrencias(i).getCdFavorecido()));
		    ocorrencia.setDsBeneficio(response.getOcorrencias(i).getDsBeneficio());
		    ocorrencia.setCdBanco(response.getOcorrencias(i).getCdBanco());
    		ocorrencia.setCdAgencia(response.getOcorrencias(i).getCdAgencia());
    		ocorrencia.setCdDigitoAgencia(response.getOcorrencias(i).getCdDigitoAgencia());
    		ocorrencia.setCdConta(response.getOcorrencias(i).getCdConta());
    		ocorrencia.setCdDigitoConta(response.getOcorrencias(i).getCdDigitoConta());
    		ocorrencia.setCdTipoCanal(response.getOcorrencias(i).getCdTipoCanal());    		
    		ocorrencia.setCdTipoTela(response.getOcorrencias(i).getCdTipoTela());	   		  
    		ocorrencia.setContaCreditoFormatada(PgitUtil.formatConta(response.getOcorrencias(i).getCdConta(), response.getOcorrencias(i).getCdDigitoConta(), false));
    		
    		ocorrencia.setBancoFormatado(PgitUtil.formatBanco(ocorrencia.getCdBanco(), false)); 
    		ocorrencia.setAgenciaFormatada(PgitUtil.formatAgencia(ocorrencia.getCdAgencia(), ocorrencia.getCdDigitoAgencia(), false));    	
    		ocorrencia.setContaFormatada(PgitUtil.formatConta(response.getOcorrencias(i).getCdConta(), response.getOcorrencias(i).getCdDigitoConta(), false));
    		ocorrencia.setBancoAgenciaContaCreditoFormatado(PgitUtil.formatBancoAgenciaConta(response.getOcorrencias(i).getCdBanco(), response.getOcorrencias(i).getCdAgencia(), response.getOcorrencias(i).getCdDigitoAgencia(), response.getOcorrencias(i).getCdConta(), response.getOcorrencias(i).getCdDigitoConta(), true));
    		ocorrencia.setFavorecidoBeneficiario(response.getOcorrencias(i).getDsBeneficio());
    		ocorrencia.setDsEfetivacaoPagamento(response.getOcorrencias(i).getDsEfetivacaoPagamento());
    		ocorrencia.setCdIspbPagtoDestino(response.getOcorrencias(i).getCdIspbPagtoDestino());
    		ocorrencia.setContaPagtoDestino(response.getOcorrencias(i).getContaPagtoDestino());		
	   	    
	   	 ocorrenciasPagtosConsParaCancelamentDTO.add(ocorrencia);	    	
	   }
    	
       saida.setOcorrencias(ocorrenciasPagtosConsParaCancelamentDTO);	
	   return saida;
    }
    
    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.cancelarpagamentosconsolidados.ICancelarPagamentosConsolidadosService#cancelarPagtosParcial(java.util.List)
     */
    public CancelarPagtosParcialSaidaDTO cancelarPagtosParcial(List<CancelarPagtosParcialEntradaDTO> listaEntrada) {
    	CancelarPagtosParcialSaidaDTO saidaDTO = new CancelarPagtosParcialSaidaDTO();
    	CancelarPagtosParcialRequest request = new CancelarPagtosParcialRequest();
    	CancelarPagtosParcialResponse response = new CancelarPagtosParcialResponse();
		
		request.setNumeroConsultas(listaEntrada.size());
		ArrayList<br.com.bradesco.web.pgit.service.data.pdc.cancelarpagtosparcial.request.Ocorrencias> lista = new ArrayList<br.com.bradesco.web.pgit.service.data.pdc.cancelarpagtosparcial.request.Ocorrencias>();
		br.com.bradesco.web.pgit.service.data.pdc.cancelarpagtosparcial.request.Ocorrencias ocorrencia ; 

		//V�rias Ocorr�ncias para Cancelar Parcial
	   for (int i=0; i< listaEntrada.size(); i++){
		   ocorrencia = new br.com.bradesco.web.pgit.service.data.pdc.cancelarpagtosparcial.request.Ocorrencias();
		  
		   ocorrencia.setCdPessoaJuridicaContrato(PgitUtil.verificaLongNulo(listaEntrada.get(i).getCdPessoaJuridicaContrato()));
		   ocorrencia.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(listaEntrada.get(i).getNrSequenciaContratoNegocio()));
		   ocorrencia.setCdTipoCanal(PgitUtil.verificaIntegerNulo(listaEntrada.get(i).getCdTipoCanal()));
		   ocorrencia.setCdProdutoServicoOperacao(PgitUtil.verificaIntegerNulo(listaEntrada.get(i).getCdProdutoServicoOperacao()));
		   ocorrencia.setCdProdutoServicoRelacionado(PgitUtil.verificaIntegerNulo(listaEntrada.get(i).getCdProdutoServicoRelacionado()));
		   ocorrencia.setCdControlePagamento(PgitUtil.verificaStringNula(listaEntrada.get(i).getCdControlePagamento()));
		 
		   			   
		   lista.add(ocorrencia);
		   
	   }
	   
		request.setOcorrencias((br.com.bradesco.web.pgit.service.data.pdc.cancelarpagtosparcial.request.Ocorrencias[]) lista.toArray(new br.com.bradesco.web.pgit.service.data.pdc.cancelarpagtosparcial.request.Ocorrencias[0]));
				
		response = getFactoryAdapter().getCancelarPagtosParcialPDCAdapter().invokeProcess(request);
		
		saidaDTO.setCodMensagem(response.getCodMensagem());
		saidaDTO.setMensagem(response.getMensagem());
		
		return saidaDTO;
	}
    
    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.cancelarpagamentosconsolidados.ICancelarPagamentosConsolidadosService#cancelarPagtosIntegral(br.com.bradesco.web.pgit.service.business.cancelarpagamentosconsolidados.bean.CancelarPagtosIntegralEntradaDTO)
     */
    public CancelarPagtosIntegralSaidaDTO cancelarPagtosIntegral(CancelarPagtosIntegralEntradaDTO cancelarPagtosIntegralEntradaDTO) {
    	
    	CancelarPagtosIntegralSaidaDTO saidaDTO = new CancelarPagtosIntegralSaidaDTO();
    	CancelarPagtosIntegralRequest request = new CancelarPagtosIntegralRequest();
    	CancelarPagtosIntegralResponse response = new CancelarPagtosIntegralResponse();
	    	
    	request.setCdPessoaJuridicaContrato(PgitUtil.verificaLongNulo(cancelarPagtosIntegralEntradaDTO.getCdPessoaJuridicaContrato()));
    	request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(cancelarPagtosIntegralEntradaDTO.getCdTipoContratoNegocio()));
    	request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(cancelarPagtosIntegralEntradaDTO.getNrSequenciaContratoNegocio()));
    	request.setCdProdutoServicoOperacao(PgitUtil.verificaIntegerNulo(cancelarPagtosIntegralEntradaDTO.getCdProdutoServicoOperacao()));
    	request.setCdProdutoServicoRelacionado(PgitUtil.verificaIntegerNulo(cancelarPagtosIntegralEntradaDTO.getCdProdutoServicoRelacionado()));    	   
    	request.setNrCorpoCpfCnpj(PgitUtil.verificaLongNulo(cancelarPagtosIntegralEntradaDTO.getNrCorpoCpfCnpj()));
    	request.setNrFilialCnpj(PgitUtil.verificaIntegerNulo(cancelarPagtosIntegralEntradaDTO.getNrFilialCnpj()));
    	request.setNrControleCpfCnpj(PgitUtil.verificaIntegerNulo(cancelarPagtosIntegralEntradaDTO.getNrControleCpfCnpj()));
    	request.setNrArquivoRemessaPagamento(PgitUtil.verificaLongNulo(cancelarPagtosIntegralEntradaDTO.getNrArquivoRemessaPagamento()));    
		request.setDtPagamento(PgitUtil.verificaStringNula(cancelarPagtosIntegralEntradaDTO.getDtPagamento()));
    	request.setCdBanco(PgitUtil.verificaIntegerNulo(cancelarPagtosIntegralEntradaDTO.getCdBanco()));
		request.setCdAgenciaBancaria(PgitUtil.verificaIntegerNulo(cancelarPagtosIntegralEntradaDTO.getCdAgenciaBancaria()));		
		request.setCdDigitoContaBancaria(PgitUtil.complementaDigitoConta(cancelarPagtosIntegralEntradaDTO.getCdDigitoContaBancaria()));
		request.setCdContaBancaria(PgitUtil.verificaLongNulo(cancelarPagtosIntegralEntradaDTO.getCdContaBancaria()));		
		request.setCdSituacaoOperacaoPagamento(PgitUtil.verificaIntegerNulo(cancelarPagtosIntegralEntradaDTO.getCdSituacaoOperacaoPagamento()));
		request.setCdPessoaContratoDebito(PgitUtil.verificaLongNulo(cancelarPagtosIntegralEntradaDTO.getCdPessoaContratoDebito()));
		request.setCdTipoContratoDebito(PgitUtil.verificaIntegerNulo(cancelarPagtosIntegralEntradaDTO.getCdTipoContratoDebito()));
		request.setNrSequenciaContratoDebito(PgitUtil.verificaLongNulo(cancelarPagtosIntegralEntradaDTO.getNrSequenciaContratoDebito()));
		request.setCdMotivoSituacaoPagamento(0);
				
		response = getFactoryAdapter().getCancelarPagtosIntegralPDCAdapter().invokeProcess(request);
				
		saidaDTO.setCodMensagem(response.getCodMensagem());
		saidaDTO.setMensagem(response.getMensagem());	
	
		return saidaDTO;
	}  
    
}

