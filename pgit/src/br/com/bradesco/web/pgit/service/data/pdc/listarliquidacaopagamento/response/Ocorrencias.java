/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listarliquidacaopagamento.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdFormaLiquidacao
     */
    private int _cdFormaLiquidacao = 0;

    /**
     * keeps track of state for field: _cdFormaLiquidacao
     */
    private boolean _has_cdFormaLiquidacao;

    /**
     * Field _dsFormaLiquidacao
     */
    private java.lang.String _dsFormaLiquidacao;

    /**
     * Field _cdSistema
     */
    private java.lang.String _cdSistema;

    /**
     * Field _cdPrioridadeDebito
     */
    private int _cdPrioridadeDebito = 0;

    /**
     * keeps track of state for field: _cdPrioridadeDebito
     */
    private boolean _has_cdPrioridadeDebito;

    /**
     * Field _cdControleHora
     */
    private int _cdControleHora = 0;

    /**
     * keeps track of state for field: _cdControleHora
     */
    private boolean _has_cdControleHora;

    /**
     * Field _qtMinutosMargemSeguranca
     */
    private int _qtMinutosMargemSeguranca = 0;

    /**
     * keeps track of state for field: _qtMinutosMargemSeguranca
     */
    private boolean _has_qtMinutosMargemSeguranca;

    /**
     * Field _hrLimiteProcessamento
     */
    private java.lang.String _hrLimiteProcessamento;

    /**
     * Field _hrConsultasSaldoPagamento
     */
    private java.lang.String _hrConsultasSaldoPagamento;

    /**
     * Field _hrConsultaFolhaPgto
     */
    private java.lang.String _hrConsultaFolhaPgto;

    /**
     * Field _qtMinutosValorSuperior
     */
    private int _qtMinutosValorSuperior = 0;

    /**
     * keeps track of state for field: _qtMinutosValorSuperior
     */
    private boolean _has_qtMinutosValorSuperior;

    /**
     * Field _hrLimiteValorSuperior
     */
    private java.lang.String _hrLimiteValorSuperior;

    /**
     * Field _qtdTempoAgendamentoCobranca
     */
    private int _qtdTempoAgendamentoCobranca = 0;

    /**
     * keeps track of state for field: _qtdTempoAgendamentoCobranca
     */
    private boolean _has_qtdTempoAgendamentoCobranca;

    /**
     * Field _qtdTempoEfetivacaoCiclicaCobranca
     */
    private int _qtdTempoEfetivacaoCiclicaCobranca = 0;

    /**
     * keeps track of state for field:
     * _qtdTempoEfetivacaoCiclicaCobranca
     */
    private boolean _has_qtdTempoEfetivacaoCiclicaCobranca;

    /**
     * Field _qtdTempoEfetivacaoDiariaCobranca
     */
    private int _qtdTempoEfetivacaoDiariaCobranca = 0;

    /**
     * keeps track of state for field:
     * _qtdTempoEfetivacaoDiariaCobranca
     */
    private boolean _has_qtdTempoEfetivacaoDiariaCobranca;

    /**
     * Field _qtdLimiteSemResposta
     */
    private int _qtdLimiteSemResposta = 0;

    /**
     * keeps track of state for field: _qtdLimiteSemResposta
     */
    private boolean _has_qtdLimiteSemResposta;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarliquidacaopagamento.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdControleHora
     * 
     */
    public void deleteCdControleHora()
    {
        this._has_cdControleHora= false;
    } //-- void deleteCdControleHora() 

    /**
     * Method deleteCdFormaLiquidacao
     * 
     */
    public void deleteCdFormaLiquidacao()
    {
        this._has_cdFormaLiquidacao= false;
    } //-- void deleteCdFormaLiquidacao() 

    /**
     * Method deleteCdPrioridadeDebito
     * 
     */
    public void deleteCdPrioridadeDebito()
    {
        this._has_cdPrioridadeDebito= false;
    } //-- void deleteCdPrioridadeDebito() 

    /**
     * Method deleteQtMinutosMargemSeguranca
     * 
     */
    public void deleteQtMinutosMargemSeguranca()
    {
        this._has_qtMinutosMargemSeguranca= false;
    } //-- void deleteQtMinutosMargemSeguranca() 

    /**
     * Method deleteQtMinutosValorSuperior
     * 
     */
    public void deleteQtMinutosValorSuperior()
    {
        this._has_qtMinutosValorSuperior= false;
    } //-- void deleteQtMinutosValorSuperior() 

    /**
     * Method deleteQtdLimiteSemResposta
     * 
     */
    public void deleteQtdLimiteSemResposta()
    {
        this._has_qtdLimiteSemResposta= false;
    } //-- void deleteQtdLimiteSemResposta() 

    /**
     * Method deleteQtdTempoAgendamentoCobranca
     * 
     */
    public void deleteQtdTempoAgendamentoCobranca()
    {
        this._has_qtdTempoAgendamentoCobranca= false;
    } //-- void deleteQtdTempoAgendamentoCobranca() 

    /**
     * Method deleteQtdTempoEfetivacaoCiclicaCobranca
     * 
     */
    public void deleteQtdTempoEfetivacaoCiclicaCobranca()
    {
        this._has_qtdTempoEfetivacaoCiclicaCobranca= false;
    } //-- void deleteQtdTempoEfetivacaoCiclicaCobranca() 

    /**
     * Method deleteQtdTempoEfetivacaoDiariaCobranca
     * 
     */
    public void deleteQtdTempoEfetivacaoDiariaCobranca()
    {
        this._has_qtdTempoEfetivacaoDiariaCobranca= false;
    } //-- void deleteQtdTempoEfetivacaoDiariaCobranca() 

    /**
     * Returns the value of field 'cdControleHora'.
     * 
     * @return int
     * @return the value of field 'cdControleHora'.
     */
    public int getCdControleHora()
    {
        return this._cdControleHora;
    } //-- int getCdControleHora() 

    /**
     * Returns the value of field 'cdFormaLiquidacao'.
     * 
     * @return int
     * @return the value of field 'cdFormaLiquidacao'.
     */
    public int getCdFormaLiquidacao()
    {
        return this._cdFormaLiquidacao;
    } //-- int getCdFormaLiquidacao() 

    /**
     * Returns the value of field 'cdPrioridadeDebito'.
     * 
     * @return int
     * @return the value of field 'cdPrioridadeDebito'.
     */
    public int getCdPrioridadeDebito()
    {
        return this._cdPrioridadeDebito;
    } //-- int getCdPrioridadeDebito() 

    /**
     * Returns the value of field 'cdSistema'.
     * 
     * @return String
     * @return the value of field 'cdSistema'.
     */
    public java.lang.String getCdSistema()
    {
        return this._cdSistema;
    } //-- java.lang.String getCdSistema() 

    /**
     * Returns the value of field 'dsFormaLiquidacao'.
     * 
     * @return String
     * @return the value of field 'dsFormaLiquidacao'.
     */
    public java.lang.String getDsFormaLiquidacao()
    {
        return this._dsFormaLiquidacao;
    } //-- java.lang.String getDsFormaLiquidacao() 

    /**
     * Returns the value of field 'hrConsultaFolhaPgto'.
     * 
     * @return String
     * @return the value of field 'hrConsultaFolhaPgto'.
     */
    public java.lang.String getHrConsultaFolhaPgto()
    {
        return this._hrConsultaFolhaPgto;
    } //-- java.lang.String getHrConsultaFolhaPgto() 

    /**
     * Returns the value of field 'hrConsultasSaldoPagamento'.
     * 
     * @return String
     * @return the value of field 'hrConsultasSaldoPagamento'.
     */
    public java.lang.String getHrConsultasSaldoPagamento()
    {
        return this._hrConsultasSaldoPagamento;
    } //-- java.lang.String getHrConsultasSaldoPagamento() 

    /**
     * Returns the value of field 'hrLimiteProcessamento'.
     * 
     * @return String
     * @return the value of field 'hrLimiteProcessamento'.
     */
    public java.lang.String getHrLimiteProcessamento()
    {
        return this._hrLimiteProcessamento;
    } //-- java.lang.String getHrLimiteProcessamento() 

    /**
     * Returns the value of field 'hrLimiteValorSuperior'.
     * 
     * @return String
     * @return the value of field 'hrLimiteValorSuperior'.
     */
    public java.lang.String getHrLimiteValorSuperior()
    {
        return this._hrLimiteValorSuperior;
    } //-- java.lang.String getHrLimiteValorSuperior() 

    /**
     * Returns the value of field 'qtMinutosMargemSeguranca'.
     * 
     * @return int
     * @return the value of field 'qtMinutosMargemSeguranca'.
     */
    public int getQtMinutosMargemSeguranca()
    {
        return this._qtMinutosMargemSeguranca;
    } //-- int getQtMinutosMargemSeguranca() 

    /**
     * Returns the value of field 'qtMinutosValorSuperior'.
     * 
     * @return int
     * @return the value of field 'qtMinutosValorSuperior'.
     */
    public int getQtMinutosValorSuperior()
    {
        return this._qtMinutosValorSuperior;
    } //-- int getQtMinutosValorSuperior() 

    /**
     * Returns the value of field 'qtdLimiteSemResposta'.
     * 
     * @return int
     * @return the value of field 'qtdLimiteSemResposta'.
     */
    public int getQtdLimiteSemResposta()
    {
        return this._qtdLimiteSemResposta;
    } //-- int getQtdLimiteSemResposta() 

    /**
     * Returns the value of field 'qtdTempoAgendamentoCobranca'.
     * 
     * @return int
     * @return the value of field 'qtdTempoAgendamentoCobranca'.
     */
    public int getQtdTempoAgendamentoCobranca()
    {
        return this._qtdTempoAgendamentoCobranca;
    } //-- int getQtdTempoAgendamentoCobranca() 

    /**
     * Returns the value of field
     * 'qtdTempoEfetivacaoCiclicaCobranca'.
     * 
     * @return int
     * @return the value of field
     * 'qtdTempoEfetivacaoCiclicaCobranca'.
     */
    public int getQtdTempoEfetivacaoCiclicaCobranca()
    {
        return this._qtdTempoEfetivacaoCiclicaCobranca;
    } //-- int getQtdTempoEfetivacaoCiclicaCobranca() 

    /**
     * Returns the value of field
     * 'qtdTempoEfetivacaoDiariaCobranca'.
     * 
     * @return int
     * @return the value of field 'qtdTempoEfetivacaoDiariaCobranca'
     */
    public int getQtdTempoEfetivacaoDiariaCobranca()
    {
        return this._qtdTempoEfetivacaoDiariaCobranca;
    } //-- int getQtdTempoEfetivacaoDiariaCobranca() 

    /**
     * Method hasCdControleHora
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdControleHora()
    {
        return this._has_cdControleHora;
    } //-- boolean hasCdControleHora() 

    /**
     * Method hasCdFormaLiquidacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFormaLiquidacao()
    {
        return this._has_cdFormaLiquidacao;
    } //-- boolean hasCdFormaLiquidacao() 

    /**
     * Method hasCdPrioridadeDebito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPrioridadeDebito()
    {
        return this._has_cdPrioridadeDebito;
    } //-- boolean hasCdPrioridadeDebito() 

    /**
     * Method hasQtMinutosMargemSeguranca
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtMinutosMargemSeguranca()
    {
        return this._has_qtMinutosMargemSeguranca;
    } //-- boolean hasQtMinutosMargemSeguranca() 

    /**
     * Method hasQtMinutosValorSuperior
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtMinutosValorSuperior()
    {
        return this._has_qtMinutosValorSuperior;
    } //-- boolean hasQtMinutosValorSuperior() 

    /**
     * Method hasQtdLimiteSemResposta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtdLimiteSemResposta()
    {
        return this._has_qtdLimiteSemResposta;
    } //-- boolean hasQtdLimiteSemResposta() 

    /**
     * Method hasQtdTempoAgendamentoCobranca
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtdTempoAgendamentoCobranca()
    {
        return this._has_qtdTempoAgendamentoCobranca;
    } //-- boolean hasQtdTempoAgendamentoCobranca() 

    /**
     * Method hasQtdTempoEfetivacaoCiclicaCobranca
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtdTempoEfetivacaoCiclicaCobranca()
    {
        return this._has_qtdTempoEfetivacaoCiclicaCobranca;
    } //-- boolean hasQtdTempoEfetivacaoCiclicaCobranca() 

    /**
     * Method hasQtdTempoEfetivacaoDiariaCobranca
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtdTempoEfetivacaoDiariaCobranca()
    {
        return this._has_qtdTempoEfetivacaoDiariaCobranca;
    } //-- boolean hasQtdTempoEfetivacaoDiariaCobranca() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdControleHora'.
     * 
     * @param cdControleHora the value of field 'cdControleHora'.
     */
    public void setCdControleHora(int cdControleHora)
    {
        this._cdControleHora = cdControleHora;
        this._has_cdControleHora = true;
    } //-- void setCdControleHora(int) 

    /**
     * Sets the value of field 'cdFormaLiquidacao'.
     * 
     * @param cdFormaLiquidacao the value of field
     * 'cdFormaLiquidacao'.
     */
    public void setCdFormaLiquidacao(int cdFormaLiquidacao)
    {
        this._cdFormaLiquidacao = cdFormaLiquidacao;
        this._has_cdFormaLiquidacao = true;
    } //-- void setCdFormaLiquidacao(int) 

    /**
     * Sets the value of field 'cdPrioridadeDebito'.
     * 
     * @param cdPrioridadeDebito the value of field
     * 'cdPrioridadeDebito'.
     */
    public void setCdPrioridadeDebito(int cdPrioridadeDebito)
    {
        this._cdPrioridadeDebito = cdPrioridadeDebito;
        this._has_cdPrioridadeDebito = true;
    } //-- void setCdPrioridadeDebito(int) 

    /**
     * Sets the value of field 'cdSistema'.
     * 
     * @param cdSistema the value of field 'cdSistema'.
     */
    public void setCdSistema(java.lang.String cdSistema)
    {
        this._cdSistema = cdSistema;
    } //-- void setCdSistema(java.lang.String) 

    /**
     * Sets the value of field 'dsFormaLiquidacao'.
     * 
     * @param dsFormaLiquidacao the value of field
     * 'dsFormaLiquidacao'.
     */
    public void setDsFormaLiquidacao(java.lang.String dsFormaLiquidacao)
    {
        this._dsFormaLiquidacao = dsFormaLiquidacao;
    } //-- void setDsFormaLiquidacao(java.lang.String) 

    /**
     * Sets the value of field 'hrConsultaFolhaPgto'.
     * 
     * @param hrConsultaFolhaPgto the value of field
     * 'hrConsultaFolhaPgto'.
     */
    public void setHrConsultaFolhaPgto(java.lang.String hrConsultaFolhaPgto)
    {
        this._hrConsultaFolhaPgto = hrConsultaFolhaPgto;
    } //-- void setHrConsultaFolhaPgto(java.lang.String) 

    /**
     * Sets the value of field 'hrConsultasSaldoPagamento'.
     * 
     * @param hrConsultasSaldoPagamento the value of field
     * 'hrConsultasSaldoPagamento'.
     */
    public void setHrConsultasSaldoPagamento(java.lang.String hrConsultasSaldoPagamento)
    {
        this._hrConsultasSaldoPagamento = hrConsultasSaldoPagamento;
    } //-- void setHrConsultasSaldoPagamento(java.lang.String) 

    /**
     * Sets the value of field 'hrLimiteProcessamento'.
     * 
     * @param hrLimiteProcessamento the value of field
     * 'hrLimiteProcessamento'.
     */
    public void setHrLimiteProcessamento(java.lang.String hrLimiteProcessamento)
    {
        this._hrLimiteProcessamento = hrLimiteProcessamento;
    } //-- void setHrLimiteProcessamento(java.lang.String) 

    /**
     * Sets the value of field 'hrLimiteValorSuperior'.
     * 
     * @param hrLimiteValorSuperior the value of field
     * 'hrLimiteValorSuperior'.
     */
    public void setHrLimiteValorSuperior(java.lang.String hrLimiteValorSuperior)
    {
        this._hrLimiteValorSuperior = hrLimiteValorSuperior;
    } //-- void setHrLimiteValorSuperior(java.lang.String) 

    /**
     * Sets the value of field 'qtMinutosMargemSeguranca'.
     * 
     * @param qtMinutosMargemSeguranca the value of field
     * 'qtMinutosMargemSeguranca'.
     */
    public void setQtMinutosMargemSeguranca(int qtMinutosMargemSeguranca)
    {
        this._qtMinutosMargemSeguranca = qtMinutosMargemSeguranca;
        this._has_qtMinutosMargemSeguranca = true;
    } //-- void setQtMinutosMargemSeguranca(int) 

    /**
     * Sets the value of field 'qtMinutosValorSuperior'.
     * 
     * @param qtMinutosValorSuperior the value of field
     * 'qtMinutosValorSuperior'.
     */
    public void setQtMinutosValorSuperior(int qtMinutosValorSuperior)
    {
        this._qtMinutosValorSuperior = qtMinutosValorSuperior;
        this._has_qtMinutosValorSuperior = true;
    } //-- void setQtMinutosValorSuperior(int) 

    /**
     * Sets the value of field 'qtdLimiteSemResposta'.
     * 
     * @param qtdLimiteSemResposta the value of field
     * 'qtdLimiteSemResposta'.
     */
    public void setQtdLimiteSemResposta(int qtdLimiteSemResposta)
    {
        this._qtdLimiteSemResposta = qtdLimiteSemResposta;
        this._has_qtdLimiteSemResposta = true;
    } //-- void setQtdLimiteSemResposta(int) 

    /**
     * Sets the value of field 'qtdTempoAgendamentoCobranca'.
     * 
     * @param qtdTempoAgendamentoCobranca the value of field
     * 'qtdTempoAgendamentoCobranca'.
     */
    public void setQtdTempoAgendamentoCobranca(int qtdTempoAgendamentoCobranca)
    {
        this._qtdTempoAgendamentoCobranca = qtdTempoAgendamentoCobranca;
        this._has_qtdTempoAgendamentoCobranca = true;
    } //-- void setQtdTempoAgendamentoCobranca(int) 

    /**
     * Sets the value of field 'qtdTempoEfetivacaoCiclicaCobranca'.
     * 
     * @param qtdTempoEfetivacaoCiclicaCobranca the value of field
     * 'qtdTempoEfetivacaoCiclicaCobranca'.
     */
    public void setQtdTempoEfetivacaoCiclicaCobranca(int qtdTempoEfetivacaoCiclicaCobranca)
    {
        this._qtdTempoEfetivacaoCiclicaCobranca = qtdTempoEfetivacaoCiclicaCobranca;
        this._has_qtdTempoEfetivacaoCiclicaCobranca = true;
    } //-- void setQtdTempoEfetivacaoCiclicaCobranca(int) 

    /**
     * Sets the value of field 'qtdTempoEfetivacaoDiariaCobranca'.
     * 
     * @param qtdTempoEfetivacaoDiariaCobranca the value of field
     * 'qtdTempoEfetivacaoDiariaCobranca'.
     */
    public void setQtdTempoEfetivacaoDiariaCobranca(int qtdTempoEfetivacaoDiariaCobranca)
    {
        this._qtdTempoEfetivacaoDiariaCobranca = qtdTempoEfetivacaoDiariaCobranca;
        this._has_qtdTempoEfetivacaoDiariaCobranca = true;
    } //-- void setQtdTempoEfetivacaoDiariaCobranca(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listarliquidacaopagamento.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listarliquidacaopagamento.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listarliquidacaopagamento.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarliquidacaopagamento.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
