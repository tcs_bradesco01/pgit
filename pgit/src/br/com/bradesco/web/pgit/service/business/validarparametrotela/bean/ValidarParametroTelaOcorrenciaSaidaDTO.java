/*
 * Nome: br.com.bradesco.web.pgit.service.business.validarparametrotela.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.validarparametrotela.bean;

/**
 * Nome: ValidarParametroTelaOcorrenciaSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ValidarParametroTelaOcorrenciaSaidaDTO {
	
	/** Atributo cdParametroTela. */
	private Integer cdParametroTela;
	
	/** Atributo cdProdutoOperacaoRelacionado. */
	private Integer cdProdutoOperacaoRelacionado;
	
	/** Atributo cdProdutoServicoRelacionado. */
	private String cdProdutoServicoRelacionado;
	
	/** Atributo cdRelacionamentoProdutoProduto. */
	private Integer cdRelacionamentoProdutoProduto;
	
	/** Atributo dsRelacionamentoProdutoProduto. */
	private String dsRelacionamentoProdutoProduto;
	
	/** Atributo cdTipoProdutoServico. */
	private Integer cdTipoProdutoServico;
	
	/** Atributo dsTipoProdutoServico. */
	private String dsTipoProdutoServico;
	
	/** Atributo check. */
	private boolean check;
	
	/**
	 * Get: cdParametroTela.
	 *
	 * @return cdParametroTela
	 */
	public Integer getCdParametroTela() {
		return cdParametroTela;
	}
	
	/**
	 * Set: cdParametroTela.
	 *
	 * @param cdParametroTela the cd parametro tela
	 */
	public void setCdParametroTela(Integer cdParametroTela) {
		this.cdParametroTela = cdParametroTela;
	}
	
	/**
	 * Get: cdProdutoOperacaoRelacionado.
	 *
	 * @return cdProdutoOperacaoRelacionado
	 */
	public Integer getCdProdutoOperacaoRelacionado() {
		return cdProdutoOperacaoRelacionado;
	}
	
	/**
	 * Set: cdProdutoOperacaoRelacionado.
	 *
	 * @param cdProdutoOperacaoRelacionado the cd produto operacao relacionado
	 */
	public void setCdProdutoOperacaoRelacionado(Integer cdProdutoOperacaoRelacionado) {
		this.cdProdutoOperacaoRelacionado = cdProdutoOperacaoRelacionado;
	}
	
	/**
	 * Get: cdProdutoServicoRelacionado.
	 *
	 * @return cdProdutoServicoRelacionado
	 */
	public String getCdProdutoServicoRelacionado() {
		return cdProdutoServicoRelacionado;
	}
	
	/**
	 * Set: cdProdutoServicoRelacionado.
	 *
	 * @param cdProdutoServicoRelacionado the cd produto servico relacionado
	 */
	public void setCdProdutoServicoRelacionado(String cdProdutoServicoRelacionado) {
		this.cdProdutoServicoRelacionado = cdProdutoServicoRelacionado;
	}
	
	/**
	 * Get: cdRelacionamentoProdutoProduto.
	 *
	 * @return cdRelacionamentoProdutoProduto
	 */
	public Integer getCdRelacionamentoProdutoProduto() {
		return cdRelacionamentoProdutoProduto;
	}
	
	/**
	 * Set: cdRelacionamentoProdutoProduto.
	 *
	 * @param cdRelacionamentoProdutoProduto the cd relacionamento produto produto
	 */
	public void setCdRelacionamentoProdutoProduto(
			Integer cdRelacionamentoProdutoProduto) {
		this.cdRelacionamentoProdutoProduto = cdRelacionamentoProdutoProduto;
	}
	
	/**
	 * Get: dsRelacionamentoProdutoProduto.
	 *
	 * @return dsRelacionamentoProdutoProduto
	 */
	public String getDsRelacionamentoProdutoProduto() {
		return dsRelacionamentoProdutoProduto;
	}
	
	/**
	 * Set: dsRelacionamentoProdutoProduto.
	 *
	 * @param dsRelacionamentoProdutoProduto the ds relacionamento produto produto
	 */
	public void setDsRelacionamentoProdutoProduto(
			String dsRelacionamentoProdutoProduto) {
		this.dsRelacionamentoProdutoProduto = dsRelacionamentoProdutoProduto;
	}
	
	/**
	 * Get: cdTipoProdutoServico.
	 *
	 * @return cdTipoProdutoServico
	 */
	public Integer getCdTipoProdutoServico() {
		return cdTipoProdutoServico;
	}
	
	/**
	 * Set: cdTipoProdutoServico.
	 *
	 * @param cdTipoProdutoServico the cd tipo produto servico
	 */
	public void setCdTipoProdutoServico(Integer cdTipoProdutoServico) {
		this.cdTipoProdutoServico = cdTipoProdutoServico;
	}
	
	/**
	 * Get: dsTipoProdutoServico.
	 *
	 * @return dsTipoProdutoServico
	 */
	public String getDsTipoProdutoServico() {
		return dsTipoProdutoServico;
	}
	
	/**
	 * Set: dsTipoProdutoServico.
	 *
	 * @param dsTipoProdutoServico the ds tipo produto servico
	 */
	public void setDsTipoProdutoServico(String dsTipoProdutoServico) {
		this.dsTipoProdutoServico = dsTipoProdutoServico;
	}
	
	/**
	 * Is check.
	 *
	 * @return true, if is check
	 */
	public boolean isCheck() {
		return check;
	}
	
	/**
	 * Set: check.
	 *
	 * @param check the check
	 */
	public void setCheck(boolean check) {
		this.check = check;
	}
}
