/*
 * Nome: br.com.bradesco.web.pgit.service.business.manassocmsglayoutmsgsist.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.manassocmsglayoutmsgsist.bean;

/**
 * Nome: DetalharVinculacaoMsgLayoutSistemaSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class DetalharVinculacaoMsgLayoutSistemaSaidaDTO {
    
    /** Atributo codMensagem. */
    private String codMensagem;

    /** Atributo mensagem. */
    private String mensagem;

    /** Atributo cdTipoLayoutArquivo. */
    private Integer cdTipoLayoutArquivo;

    /** Atributo dsTipoLayoutArquivo. */
    private String dsTipoLayoutArquivo;

    /** Atributo cdSistema. */
    private String cdSistema;
    
    /** Atributo nrMensagemArquivoRetorno. */
    private Integer nrMensagemArquivoRetorno;

    /** Atributo dsCodigoSistema. */
    private String dsCodigoSistema;

    /** Atributo nrEventoMensagemNegocio. */
    private Integer nrEventoMensagemNegocio;

    /** Atributo dsEventoMensagemNegocio. */
    private String dsEventoMensagemNegocio;

    /** Atributo cdRecursoGeradorMensagem. */
    private Integer cdRecursoGeradorMensagem;

    /** Atributo dsRecGedorMensagem. */
    private String dsRecGedorMensagem;

    /** Atributo cdIdiomaTextoMensagem. */
    private Integer cdIdiomaTextoMensagem;

    /** Atributo dsIdiomaTextoMensagem. */
    private String dsIdiomaTextoMensagem;

    /** Atributo cdMensagemArquivoRetorno. */
    private String cdMensagemArquivoRetorno;

    /** Atributo dsMensagemArquivoRetorno. */
    private String dsMensagemArquivoRetorno;

    /** Atributo cdCanalInclusao. */
    private Integer cdCanalInclusao;

    /** Atributo dsCanalInclusao. */
    private String dsCanalInclusao;

    /** Atributo cdAutenticacaoSegurancaInclusao. */
    private String cdAutenticacaoSegurancaInclusao;

    /** Atributo nmOperacaoFluxoInclusao. */
    private String nmOperacaoFluxoInclusao;

    /** Atributo hrInclusaoRegistro. */
    private String hrInclusaoRegistro;

    /** Atributo cdCanalManutencao. */
    private Integer cdCanalManutencao;

    /** Atributo dsCanalManutencao. */
    private String dsCanalManutencao;

    /** Atributo cdAutenticacaoSegurancaManutencao. */
    private String cdAutenticacaoSegurancaManutencao;

    /** Atributo nmOperacaoFluxoManutencao. */
    private String nmOperacaoFluxoManutencao;

    /** Atributo hrManutencaoRegistro. */
    private String hrManutencaoRegistro;

    /** Atributo canalInclusaoFormatado. */
    private String canalInclusaoFormatado;

    /** Atributo canalManutencaoFormatado. */
    private String canalManutencaoFormatado;

    /**
     * Get: codMensagem.
     *
     * @return codMensagem
     */
    public String getCodMensagem() {
	return codMensagem;
    }

    /**
     * Set: codMensagem.
     *
     * @param codMensagem the cod mensagem
     */
    public void setCodMensagem(String codMensagem) {
	this.codMensagem = codMensagem;
    }

    /**
     * Get: mensagem.
     *
     * @return mensagem
     */
    public String getMensagem() {
	return mensagem;
    }

    /**
     * Set: mensagem.
     *
     * @param mensagem the mensagem
     */
    public void setMensagem(String mensagem) {
	this.mensagem = mensagem;
    }

    /**
     * Get: cdTipoLayoutArquivo.
     *
     * @return cdTipoLayoutArquivo
     */
    public Integer getCdTipoLayoutArquivo() {
	return cdTipoLayoutArquivo;
    }

    /**
     * Set: cdTipoLayoutArquivo.
     *
     * @param cdTipoLayoutArquivo the cd tipo layout arquivo
     */
    public void setCdTipoLayoutArquivo(Integer cdTipoLayoutArquivo) {
	this.cdTipoLayoutArquivo = cdTipoLayoutArquivo;
    }

    /**
     * Get: nrEventoMensagemNegocio.
     *
     * @return nrEventoMensagemNegocio
     */
    public Integer getNrEventoMensagemNegocio() {
	return nrEventoMensagemNegocio;
    }

    /**
     * Set: nrEventoMensagemNegocio.
     *
     * @param nrEventoMensagemNegocio the nr evento mensagem negocio
     */
    public void setNrEventoMensagemNegocio(Integer nrEventoMensagemNegocio) {
	this.nrEventoMensagemNegocio = nrEventoMensagemNegocio;
    }

    /**
     * Get: dsEventoMensagemNegocio.
     *
     * @return dsEventoMensagemNegocio
     */
    public String getDsEventoMensagemNegocio() {
	return dsEventoMensagemNegocio;
    }

    /**
     * Set: dsEventoMensagemNegocio.
     *
     * @param dsEventoMensagemNegocio the ds evento mensagem negocio
     */
    public void setDsEventoMensagemNegocio(String dsEventoMensagemNegocio) {
	this.dsEventoMensagemNegocio = dsEventoMensagemNegocio;
    }

    /**
     * Get: cdRecursoGeradorMensagem.
     *
     * @return cdRecursoGeradorMensagem
     */
    public Integer getCdRecursoGeradorMensagem() {
	return cdRecursoGeradorMensagem;
    }

    /**
     * Set: cdRecursoGeradorMensagem.
     *
     * @param cdRecursoGeradorMensagem the cd recurso gerador mensagem
     */
    public void setCdRecursoGeradorMensagem(Integer cdRecursoGeradorMensagem) {
	this.cdRecursoGeradorMensagem = cdRecursoGeradorMensagem;
    }

    /**
     * Get: dsRecGedorMensagem.
     *
     * @return dsRecGedorMensagem
     */
    public String getDsRecGedorMensagem() {
	return dsRecGedorMensagem;
    }

    /**
     * Set: dsRecGedorMensagem.
     *
     * @param dsRecGedorMensagem the ds rec gedor mensagem
     */
    public void setDsRecGedorMensagem(String dsRecGedorMensagem) {
	this.dsRecGedorMensagem = dsRecGedorMensagem;
    }

    /**
     * Get: cdIdiomaTextoMensagem.
     *
     * @return cdIdiomaTextoMensagem
     */
    public Integer getCdIdiomaTextoMensagem() {
	return cdIdiomaTextoMensagem;
    }

    /**
     * Set: cdIdiomaTextoMensagem.
     *
     * @param cdIdiomaTextoMensagem the cd idioma texto mensagem
     */
    public void setCdIdiomaTextoMensagem(Integer cdIdiomaTextoMensagem) {
	this.cdIdiomaTextoMensagem = cdIdiomaTextoMensagem;
    }

    /**
     * Get: dsIdiomaTextoMensagem.
     *
     * @return dsIdiomaTextoMensagem
     */
    public String getDsIdiomaTextoMensagem() {
	return dsIdiomaTextoMensagem;
    }

    /**
     * Set: dsIdiomaTextoMensagem.
     *
     * @param dsIdiomaTextoMensagem the ds idioma texto mensagem
     */
    public void setDsIdiomaTextoMensagem(String dsIdiomaTextoMensagem) {
	this.dsIdiomaTextoMensagem = dsIdiomaTextoMensagem;
    }

    /**
     * Get: cdCanalInclusao.
     *
     * @return cdCanalInclusao
     */
    public Integer getCdCanalInclusao() {
	return cdCanalInclusao;
    }

    /**
     * Set: cdCanalInclusao.
     *
     * @param cdCanalInclusao the cd canal inclusao
     */
    public void setCdCanalInclusao(Integer cdCanalInclusao) {
	this.cdCanalInclusao = cdCanalInclusao;
    }

    /**
     * Get: dsCanalInclusao.
     *
     * @return dsCanalInclusao
     */
    public String getDsCanalInclusao() {
	return dsCanalInclusao;
    }

    /**
     * Set: dsCanalInclusao.
     *
     * @param dsCanalInclusao the ds canal inclusao
     */
    public void setDsCanalInclusao(String dsCanalInclusao) {
	this.dsCanalInclusao = dsCanalInclusao;
    }

    /**
     * Get: cdAutenticacaoSegurancaInclusao.
     *
     * @return cdAutenticacaoSegurancaInclusao
     */
    public String getCdAutenticacaoSegurancaInclusao() {
	return cdAutenticacaoSegurancaInclusao;
    }

    /**
     * Set: cdAutenticacaoSegurancaInclusao.
     *
     * @param cdAutenticacaoSegurancaInclusao the cd autenticacao seguranca inclusao
     */
    public void setCdAutenticacaoSegurancaInclusao(String cdAutenticacaoSegurancaInclusao) {
	this.cdAutenticacaoSegurancaInclusao = cdAutenticacaoSegurancaInclusao;
    }

    /**
     * Get: nmOperacaoFluxoInclusao.
     *
     * @return nmOperacaoFluxoInclusao
     */
    public String getNmOperacaoFluxoInclusao() {
	return nmOperacaoFluxoInclusao;
    }

    /**
     * Set: nmOperacaoFluxoInclusao.
     *
     * @param nmOperacaoFluxoInclusao the nm operacao fluxo inclusao
     */
    public void setNmOperacaoFluxoInclusao(String nmOperacaoFluxoInclusao) {
	this.nmOperacaoFluxoInclusao = nmOperacaoFluxoInclusao;
    }

    /**
     * Get: hrInclusaoRegistro.
     *
     * @return hrInclusaoRegistro
     */
    public String getHrInclusaoRegistro() {
	return hrInclusaoRegistro;
    }

    /**
     * Set: hrInclusaoRegistro.
     *
     * @param hrInclusaoRegistro the hr inclusao registro
     */
    public void setHrInclusaoRegistro(String hrInclusaoRegistro) {
	this.hrInclusaoRegistro = hrInclusaoRegistro;
    }

    /**
     * Get: cdCanalManutencao.
     *
     * @return cdCanalManutencao
     */
    public Integer getCdCanalManutencao() {
	return cdCanalManutencao;
    }

    /**
     * Set: cdCanalManutencao.
     *
     * @param cdCanalManutencao the cd canal manutencao
     */
    public void setCdCanalManutencao(Integer cdCanalManutencao) {
	this.cdCanalManutencao = cdCanalManutencao;
    }

    /**
     * Get: dsCanalManutencao.
     *
     * @return dsCanalManutencao
     */
    public String getDsCanalManutencao() {
	return dsCanalManutencao;
    }

    /**
     * Set: dsCanalManutencao.
     *
     * @param dsCanalManutencao the ds canal manutencao
     */
    public void setDsCanalManutencao(String dsCanalManutencao) {
	this.dsCanalManutencao = dsCanalManutencao;
    }

    /**
     * Get: cdAutenticacaoSegurancaManutencao.
     *
     * @return cdAutenticacaoSegurancaManutencao
     */
    public String getCdAutenticacaoSegurancaManutencao() {
	return cdAutenticacaoSegurancaManutencao;
    }

    /**
     * Set: cdAutenticacaoSegurancaManutencao.
     *
     * @param cdAutenticacaoSegurancaManutencao the cd autenticacao seguranca manutencao
     */
    public void setCdAutenticacaoSegurancaManutencao(String cdAutenticacaoSegurancaManutencao) {
	this.cdAutenticacaoSegurancaManutencao = cdAutenticacaoSegurancaManutencao;
    }

    /**
     * Get: nmOperacaoFluxoManutencao.
     *
     * @return nmOperacaoFluxoManutencao
     */
    public String getNmOperacaoFluxoManutencao() {
	return nmOperacaoFluxoManutencao;
    }

    /**
     * Set: nmOperacaoFluxoManutencao.
     *
     * @param nmOperacaoFluxoManutencao the nm operacao fluxo manutencao
     */
    public void setNmOperacaoFluxoManutencao(String nmOperacaoFluxoManutencao) {
	this.nmOperacaoFluxoManutencao = nmOperacaoFluxoManutencao;
    }

    /**
     * Get: hrManutencaoRegistro.
     *
     * @return hrManutencaoRegistro
     */
    public String getHrManutencaoRegistro() {
	return hrManutencaoRegistro;
    }

    /**
     * Set: hrManutencaoRegistro.
     *
     * @param hrManutencaoRegistro the hr manutencao registro
     */
    public void setHrManutencaoRegistro(String hrManutencaoRegistro) {
	this.hrManutencaoRegistro = hrManutencaoRegistro;
    }

    /**
     * Get: canalInclusaoFormatado.
     *
     * @return canalInclusaoFormatado
     */
    public String getCanalInclusaoFormatado() {
	return canalInclusaoFormatado;
    }

    /**
     * Set: canalInclusaoFormatado.
     *
     * @param canalInclusaoFormatado the canal inclusao formatado
     */
    public void setCanalInclusaoFormatado(String canalInclusaoFormatado) {
	this.canalInclusaoFormatado = canalInclusaoFormatado;
    }

    /**
     * Get: canalManutencaoFormatado.
     *
     * @return canalManutencaoFormatado
     */
    public String getCanalManutencaoFormatado() {
	return canalManutencaoFormatado;
    }

    /**
     * Set: canalManutencaoFormatado.
     *
     * @param canalManutencaoFormatado the canal manutencao formatado
     */
    public void setCanalManutencaoFormatado(String canalManutencaoFormatado) {
	this.canalManutencaoFormatado = canalManutencaoFormatado;
    }

    /**
     * Get: cdMensagemArquivoRetorno.
     *
     * @return cdMensagemArquivoRetorno
     */
    public String getCdMensagemArquivoRetorno() {
	return cdMensagemArquivoRetorno;
    }

    /**
     * Set: cdMensagemArquivoRetorno.
     *
     * @param cdMensagemArquivoRetorno the cd mensagem arquivo retorno
     */
    public void setCdMensagemArquivoRetorno(String cdMensagemArquivoRetorno) {
	this.cdMensagemArquivoRetorno = cdMensagemArquivoRetorno;
    }

    /**
     * Get: cdSistema.
     *
     * @return cdSistema
     */
    public String getCdSistema() {
	return cdSistema;
    }

    /**
     * Set: cdSistema.
     *
     * @param cdSistema the cd sistema
     */
    public void setCdSistema(String cdSistema) {
	this.cdSistema = cdSistema;
    }

    /**
     * Get: dsCodigoSistema.
     *
     * @return dsCodigoSistema
     */
    public String getDsCodigoSistema() {
	return dsCodigoSistema;
    }

    /**
     * Set: dsCodigoSistema.
     *
     * @param dsCodigoSistema the ds codigo sistema
     */
    public void setDsCodigoSistema(String dsCodigoSistema) {
	this.dsCodigoSistema = dsCodigoSistema;
    }

    /**
     * Get: dsMensagemArquivoRetorno.
     *
     * @return dsMensagemArquivoRetorno
     */
    public String getDsMensagemArquivoRetorno() {
	return dsMensagemArquivoRetorno;
    }

    /**
     * Set: dsMensagemArquivoRetorno.
     *
     * @param dsMensagemArquivoRetorno the ds mensagem arquivo retorno
     */
    public void setDsMensagemArquivoRetorno(String dsMensagemArquivoRetorno) {
	this.dsMensagemArquivoRetorno = dsMensagemArquivoRetorno;
    }

    /**
     * Get: dsTipoLayoutArquivo.
     *
     * @return dsTipoLayoutArquivo
     */
    public String getDsTipoLayoutArquivo() {
	return dsTipoLayoutArquivo;
    }

    /**
     * Set: dsTipoLayoutArquivo.
     *
     * @param dsTipoLayoutArquivo the ds tipo layout arquivo
     */
    public void setDsTipoLayoutArquivo(String dsTipoLayoutArquivo) {
	this.dsTipoLayoutArquivo = dsTipoLayoutArquivo;
    }

    /**
     * Get: nrMensagemArquivoRetorno.
     *
     * @return nrMensagemArquivoRetorno
     */
    public Integer getNrMensagemArquivoRetorno() {
        return nrMensagemArquivoRetorno;
    }

    /**
     * Set: nrMensagemArquivoRetorno.
     *
     * @param nrMensagemArquivoRetorno the nr mensagem arquivo retorno
     */
    public void setNrMensagemArquivoRetorno(Integer nrMensagemArquivoRetorno) {
        this.nrMensagemArquivoRetorno = nrMensagemArquivoRetorno;
    }
}