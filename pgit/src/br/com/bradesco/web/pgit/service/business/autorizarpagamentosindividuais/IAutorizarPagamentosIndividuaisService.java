/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/interfaz.ftl,v $
 * $Id: interfaz.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: interfaz.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.autorizarpagamentosindividuais;

import java.util.List;

import br.com.bradesco.web.pgit.service.business.autorizarpagamentosindividuais.bean.AutorizarPagtosIndividuaisEntradaDTO;
import br.com.bradesco.web.pgit.service.business.autorizarpagamentosindividuais.bean.AutorizarPagtosIndividuaisSaidaDTO;
import br.com.bradesco.web.pgit.service.business.autorizarpagamentosindividuais.bean.ConsultarPagtoIndPendAutorizacaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.autorizarpagamentosindividuais.bean.ConsultarPagtoIndPendAutorizacaoSaidaDTO;

/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Interface do adaptador: AutorizarPagamentosIndividuais
 * </p>
 * 
 * @comment CODIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public interface IAutorizarPagamentosIndividuaisService {

  
	
	/**
	 * Autorizar pagtos individuais.
	 *
	 * @param autorizarPagtosIndividuaisEntradaDTO the autorizar pagtos individuais entrada dto
	 * @return the autorizar pagtos individuais saida dto
	 */
	AutorizarPagtosIndividuaisSaidaDTO autorizarPagtosIndividuais(AutorizarPagtosIndividuaisEntradaDTO autorizarPagtosIndividuaisEntradaDTO);
	
	/**
	 * Consultar pagto ind pend autorizacao.
	 *
	 * @param consultarPagtoIndPendAutorizacaoEntradaDTO the consultar pagto ind pend autorizacao entrada dto
	 * @return the list< consultar pagto ind pend autorizacao saida dt o>
	 */
	List<ConsultarPagtoIndPendAutorizacaoSaidaDTO> consultarPagtoIndPendAutorizacao(ConsultarPagtoIndPendAutorizacaoEntradaDTO consultarPagtoIndPendAutorizacaoEntradaDTO);
}

