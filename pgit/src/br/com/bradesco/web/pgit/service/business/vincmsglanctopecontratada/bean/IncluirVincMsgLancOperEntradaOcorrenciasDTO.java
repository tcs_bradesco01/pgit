package br.com.bradesco.web.pgit.service.business.vincmsglanctopecontratada.bean;

public class IncluirVincMsgLancOperEntradaOcorrenciasDTO {

	/** Atributo cdTipoPagamentoCliente. */
	private Integer cdTipoPagamentoCliente;

	public Integer getCdTipoPagamentoCliente() {
		return cdTipoPagamentoCliente;
	}

	public void setCdTipoPagamentoCliente(Integer cdTipoPagamentoCliente) {
		this.cdTipoPagamentoCliente = cdTipoPagamentoCliente;
	}

}
