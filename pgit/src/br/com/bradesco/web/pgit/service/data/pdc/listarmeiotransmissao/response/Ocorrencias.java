/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listarmeiotransmissao.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdTipoMeioTransmissao
     */
    private int _cdTipoMeioTransmissao = 0;

    /**
     * keeps track of state for field: _cdTipoMeioTransmissao
     */
    private boolean _has_cdTipoMeioTransmissao;

    /**
     * Field _dsTipoMeioTransmissao
     */
    private java.lang.String _dsTipoMeioTransmissao;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarmeiotransmissao.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdTipoMeioTransmissao
     * 
     */
    public void deleteCdTipoMeioTransmissao()
    {
        this._has_cdTipoMeioTransmissao= false;
    } //-- void deleteCdTipoMeioTransmissao() 

    /**
     * Returns the value of field 'cdTipoMeioTransmissao'.
     * 
     * @return int
     * @return the value of field 'cdTipoMeioTransmissao'.
     */
    public int getCdTipoMeioTransmissao()
    {
        return this._cdTipoMeioTransmissao;
    } //-- int getCdTipoMeioTransmissao() 

    /**
     * Returns the value of field 'dsTipoMeioTransmissao'.
     * 
     * @return String
     * @return the value of field 'dsTipoMeioTransmissao'.
     */
    public java.lang.String getDsTipoMeioTransmissao()
    {
        return this._dsTipoMeioTransmissao;
    } //-- java.lang.String getDsTipoMeioTransmissao() 

    /**
     * Method hasCdTipoMeioTransmissao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoMeioTransmissao()
    {
        return this._has_cdTipoMeioTransmissao;
    } //-- boolean hasCdTipoMeioTransmissao() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdTipoMeioTransmissao'.
     * 
     * @param cdTipoMeioTransmissao the value of field
     * 'cdTipoMeioTransmissao'.
     */
    public void setCdTipoMeioTransmissao(int cdTipoMeioTransmissao)
    {
        this._cdTipoMeioTransmissao = cdTipoMeioTransmissao;
        this._has_cdTipoMeioTransmissao = true;
    } //-- void setCdTipoMeioTransmissao(int) 

    /**
     * Sets the value of field 'dsTipoMeioTransmissao'.
     * 
     * @param dsTipoMeioTransmissao the value of field
     * 'dsTipoMeioTransmissao'.
     */
    public void setDsTipoMeioTransmissao(java.lang.String dsTipoMeioTransmissao)
    {
        this._dsTipoMeioTransmissao = dsTipoMeioTransmissao;
    } //-- void setDsTipoMeioTransmissao(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listarmeiotransmissao.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listarmeiotransmissao.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listarmeiotransmissao.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarmeiotransmissao.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
