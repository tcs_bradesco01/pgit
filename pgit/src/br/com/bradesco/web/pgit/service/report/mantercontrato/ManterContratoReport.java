/*
 * Nome: br.com.bradesco.web.pgit.service.report.mantercontrato
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.report.mantercontrato;


import java.awt.Color;
import java.util.Date;

import org.apache.commons.lang.StringUtils;

import br.com.bradesco.web.pgit.service.business.imprimircontrato.bean.ImprimirContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.report.base.BasicPdfReport;
import br.com.bradesco.web.pgit.utils.DateUtils;

import com.lowagie.text.Chunk;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.Image;
import com.lowagie.text.Paragraph;
import com.lowagie.text.pdf.PdfCell;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;

/**
 * Nome: ManterContratoReport
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ManterContratoReport extends BasicPdfReport {
	
	/** Atributo fTexto. */
	private Font fTexto = new Font(Font.TIMES_ROMAN, 12, Font.NORMAL);
	
	/** Atributo fTextoTabela. */
	private Font fTextoTabela = new Font(Font.TIMES_ROMAN, 12, Font.BOLD);
	
	/** Atributo fTextoTabelaNormal. */
	private Font fTextoTabelaNormal = new Font(Font.TIMES_ROMAN, 12, Font.NORMAL);
	
	/** Atributo fTextoTabelaCinza. */
	private Font fTextoTabelaCinza = new Font(Font.TIMES_ROMAN, 8, Font.BOLD,Color.GRAY);
	
	/** Atributo imagemRelatorio. */
	private Image imagemRelatorio = null;
	
	/** Atributo rodape. */
	private ContratoRodapeReport rodape = new ContratoRodapeReport();
	
	/** Atributo saidaImprimirContrato. */
	private ImprimirContratoSaidaDTO saidaImprimirContrato;
	
	/**
	 * Manter contrato report.
	 *
	 * @param saidaImprimirContrato the saida imprimir contrato
	 */
	public ManterContratoReport(ImprimirContratoSaidaDTO saidaImprimirContrato) {
		super();
		this.saidaImprimirContrato = saidaImprimirContrato;
		this.imagemRelatorio = getImagemRelatorio(); 
	}
	
	/**
	 * Manter contrato report.
	 */
	public ManterContratoReport() {
		super();
		this.imagemRelatorio = getImagemRelatorio(); 
	}


	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.report.base.BasicPdfReport#popularPdf(com.lowagie.text.Document)
	 */
	@Override
	protected void popularPdf(Document document) throws DocumentException {
		
		preencherCabec(document);
				
		popularCabec(document);
		
		preencherClausulaCondicoes(document);
		
		document.add(Chunk.NEWLINE);
		preencherClausulaPrimeira(document);
		
		document.add(Chunk.NEWLINE);
		preencherParagrafoUnico(document);
		
		document.add(Chunk.NEWLINE);
		preencherClausulaSegunda(document);
		
		document.add(Chunk.NEWLINE);
		preencherParagrafoPrimeiro(document);
		
		document.add(Chunk.NEWLINE);
		preencherParagrafoSegundo(document);
		
		document.add(Chunk.NEWLINE);
		preencherParagrafoTerceiro(document);
		
		document.add(Chunk.NEWLINE);
		preencherParagrafoQuarto(document);
		
		document.add(Chunk.NEWLINE);
		preencherParagrafoQuinto(document);
		
		document.add(Chunk.NEWLINE);
		preencherParagrafoSexto(document);
		
		document.add(Chunk.NEWLINE);
		preencherParagrafoSetimo(document);
		
		document.add(Chunk.NEWLINE);
		preencherParagrafoOitavo(document);
		
		document.add(Chunk.NEWLINE);
		preencherClausulaTerceira(document);
		
		document.add(Chunk.NEWLINE);
		preencherClausulaTerceiraA(document);
		
		document.add(Chunk.NEWLINE);
		preencherClausulaTerceiraB(document);
		
		document.add(Chunk.NEWLINE);
		preencherParagrafoUnicoTerceira(document);
		
		document.add(Chunk.NEWLINE);
		preencherClausulaQuarta(document);
		
		document.add(Chunk.NEWLINE);
		preencherClausulaQuartaA(document);
		
		document.add(Chunk.NEWLINE);
		preencherClausulaQuartaB(document);
		
		document.add(Chunk.NEWLINE);
		preencherClausulaQuinta(document);
		
		document.add(Chunk.NEWLINE);
		preencherClausulaSexta(document);
		
		document.add(Chunk.NEWLINE);
		preencherClausulaSetima(document);
		
		document.add(Chunk.NEWLINE);
		preencherClausulaOitava(document);
		
		document.add(Chunk.NEWLINE);
		preencherClausulaNona(document);
		
		document.add(Chunk.NEWLINE);
		preencherClausulaDez(document);
		
		document.add(Chunk.NEWLINE);
		preencherClausulaOnze(document);
		
		document.add(Chunk.NEWLINE);
		preencherClausulaDoze(document);
		
		document.add(Chunk.NEWLINE);
		preencherClausulaTreze(document);
		
		document.add(Chunk.NEWLINE);
		preencherClausulaQuatorze(document);
		
		document.add(Chunk.NEWLINE);
		preencherClausulaQuinze(document);
		
		document.add(Chunk.NEWLINE);
		preencherClausulaDezesseis(document);
		
		document.add(Chunk.NEWLINE);
		preencherClausulaDezessete(document);
		
		document.add(Chunk.NEWLINE);
		preencherClausulaDezoito(document);
		
		document.add(Chunk.NEWLINE);
		preencherClausulaDezoitoA(document);
		
		document.add(Chunk.NEWLINE);
		preencherClausulaDezoitoB(document);
		
		document.add(Chunk.NEWLINE);
		preencherClausulaDezoitoC(document);
		
		document.add(Chunk.NEWLINE);
		preencherClausulaDezoitoD(document);
		
		document.add(Chunk.NEWLINE);
		preencherClausulaDezoitoE(document);
		
		document.add(Chunk.NEWLINE);
		preencherClausulaDezenove(document);
		
		document.add(Chunk.NEWLINE);
		preencherClausulaCondicao(document);
		
		document.add(Chunk.NEXTPAGE);
		preencherData(document);
		
		document.add(Chunk.NEWLINE);
		document.add(Chunk.NEWLINE);
		document.add(Chunk.NEWLINE);
		document.add(Chunk.NEWLINE);
		assinaturaUm(document);
		
		document.add(Chunk.NEWLINE);
		document.add(Chunk.NEWLINE);
		document.add(Chunk.NEWLINE);
		preencherAssinaturas3(document);
		
		document.add(Chunk.NEWLINE);
		document.add(Chunk.NEWLINE);
		document.add(Chunk.NEWLINE);
		assinaturaDois(document);
				
		document.add(Chunk.NEWLINE);
		document.add(Chunk.NEWLINE);
		document.add(Chunk.NEWLINE);
		document.add(Chunk.NEWLINE);
		document.add(Chunk.NEWLINE);
		document.add(Chunk.NEWLINE);
		document.add(Chunk.NEWLINE);
		document.add(Chunk.NEWLINE);
		document.add(Chunk.NEWLINE);
		document.add(Chunk.NEWLINE);
		document.add(Chunk.NEWLINE);
		document.add(Chunk.NEWLINE);
		document.add(Chunk.NEWLINE);
		document.add(Chunk.NEWLINE);
		document.add(Chunk.NEWLINE);
		document.add(Chunk.NEWLINE);
		rodape.popularPdf(document);
		document.add(Chunk.NEXTPAGE);
		
		//	  ******* ANEXO 1 *******
		if (saidaImprimirContrato.getCdImpressaoFolha().equals("S")) {
			FolhaPagtoContratadaReport anexo1 = new FolhaPagtoContratadaReport(saidaImprimirContrato);
			document.add(Chunk.NEWLINE);
			document.add(Chunk.NEWLINE);
			anexo1.popularPdf(document);
		} 
		//	  ******* ANEXO 2 *******
		if (saidaImprimirContrato.getDsImpressaoFornecedor().equals("S")) {
			PagamentoFornecedorReport anexo2 = new PagamentoFornecedorReport(saidaImprimirContrato);
			document.add(Chunk.NEXTPAGE);
			anexo2.popularPdf(document);
		}
		//	  ******* ANEXO 3 *******
		if (saidaImprimirContrato.getDsImpressaoPtrbLine().equals("S")) {
			PagamentoOnLineReport anexo3 = new PagamentoOnLineReport(saidaImprimirContrato);
			document.add(Chunk.NEXTPAGE);
			anexo3.popularPdf(document);
		}
		//	  ******* ANEXO 4 *******
		if (saidaImprimirContrato.getDsImpressaoPtrbEmpresa().equals("S")) {
			LayoutEmpresaReport anexo4 = new LayoutEmpresaReport(saidaImprimirContrato);
			document.add(Chunk.NEXTPAGE);
			anexo4.popularPdf(document);
		}
		//	  ******* ANEXO 5 *******
		if (saidaImprimirContrato.getDsImpressaoPtrbTributo().equals("S")) {
			PagamentoEletronicoTributosReport anexo5 = new PagamentoEletronicoTributosReport(saidaImprimirContrato);
			document.add(Chunk.NEXTPAGE);
			anexo5.popularPdf(document);
		} 
		//	  ******* ANEXO 6 *******
		if (saidaImprimirContrato.getDsImpressaoComprovante().equals("S")) {
			EmissaoComprovanteSalarialReport anexo6 = new EmissaoComprovanteSalarialReport(saidaImprimirContrato);
			document.add(Chunk.NEXTPAGE);
			anexo6.popularPdf(document);
		}

		// ******* TERMO ADITIVO *
		/*TermoAditivoReport termoAditivo = new TermoAditivoReport(saidaImprimirContrato);
		document.add(Chunk.NEXTPAGE);
		termoAditivo.popularPdf(document);*/
	}
	
	/**
	 * Preencher cabec.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherCabec(Document document) throws DocumentException {
		
		PdfPTable tabela = new PdfPTable(2);
		tabela.setWidthPercentage(100f);
		tabela.setWidths(new float[] {40.0f, 60.0f});
		
		Paragraph pLinha1Esq = new Paragraph(new Chunk("CONTRATO DE PRESTA��O DE SERVI�OS DE PAGAMENTOS.", fTextoTabelaCinza));
		pLinha1Esq.setAlignment(Element.ALIGN_LEFT);
				
		PdfPCell celula1 = new PdfPCell();
		celula1.setBorder(PdfPCell.NO_BORDER);
		celula1.addElement(imagemRelatorio);
		
		PdfPCell celula2 = new PdfPCell();
		celula2.setBorder(PdfCell.NO_BORDER);
		
		celula2.addElement(pLinha1Esq);
		tabela.addCell(celula1);
		tabela.addCell(celula2);
		
		document.add(tabela);
		
	}
	
	/**
	 * Popular cabec.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void popularCabec(Document document) throws DocumentException {
		
		PdfPTable tabela = new PdfPTable(1);
		tabela.setWidthPercentage(100f);
		
		Paragraph pLinha1Dir = new Paragraph(new Chunk("Identifica��o do Banco:", fTextoTabela));
		pLinha1Dir.setAlignment(Element.ALIGN_LEFT);
		Paragraph pLinha2Dir = new Paragraph(new Chunk("CNPJ/MF", fTextoTabelaNormal));
		pLinha2Dir.setAlignment(Element.ALIGN_LEFT);
		Paragraph pLinha2Dir1 = new Paragraph(new Chunk("	60.746.948/0001-12", fTextoTabela));
		pLinha2Dir1.setAlignment(Element.ALIGN_LEFT);
		Paragraph pLinha3Dir = new Paragraph(new Chunk("Endere�o:", fTextoTabelaNormal));
		pLinha3Dir.setAlignment(Element.ALIGN_LEFT);
		Paragraph pLinha3Dir1 = new Paragraph(new Chunk("	CIDADE DE DEUS - VILA YARA - CEP: 06029-900  - OSASCO - S�O PAULO", fTextoTabela));
		pLinha3Dir1.setAlignment(Element.ALIGN_LEFT);
		
		Paragraph pLinha4Dir = new Paragraph(new Chunk("Identifica��o do Cliente:", fTextoTabela));
		pLinha4Dir.setAlignment(Element.ALIGN_LEFT);

		PdfPTable tabelaInterna = new PdfPTable(1);
		tabelaInterna.setWidthPercentage(100f);
		
		PdfPCell cellIdentBanco = new PdfPCell();
		cellIdentBanco.setBorder(PdfCell.BOX);
		cellIdentBanco.addElement(pLinha1Dir);
		
		PdfPCell celulaBordaPrincipal = new PdfPCell();
		celulaBordaPrincipal.setBorder(PdfCell.NO_BORDER);
		celulaBordaPrincipal.setBorderWidth(0);
		celulaBordaPrincipal.setPaddingTop(20);
		celulaBordaPrincipal.setPaddingBottom(0);
		celulaBordaPrincipal.addElement(tabelaInterna);
		
		//***********************************

		PdfPTable tabelaInterna1 = new PdfPTable(4);
		tabelaInterna1.setWidthPercentage(100f);
		tabelaInterna1.setWidths(new float[] {5.0f, 45.0f, 5.0f, 45.0f});

		PdfPCell cellBanco = new PdfPCell();
		cellBanco.setBorder(PdfCell.BOX);
		cellBanco.addElement(new Paragraph(new Chunk(" 01", fTextoTabela)));
		cellBanco.addElement(new Paragraph());
		
		PdfPCell cellBanco1 = new PdfPCell();
		cellBanco1.setBorder(PdfCell.BOX);
		cellBanco1.addElement(pLinha2Dir);
		cellBanco1.addElement(pLinha2Dir1);
		
		PdfPCell cellBanco2 = new PdfPCell();
		cellBanco2.setBorder(PdfCell.BOX);
		cellBanco2.addElement(new Paragraph(new Chunk(" 02", fTextoTabela)));
		cellBanco2.addElement(new Paragraph());
		
		PdfPCell cellBanco3 = new PdfPCell();
		cellBanco3.setBorder(PdfCell.BOX);
		cellBanco3.addElement(new Paragraph(new Chunk(" ", fTextoTabela)));
		cellBanco3.addElement(new Paragraph(new Chunk("BANCO BRADESCO S.A.", fTextoTabela)));
		
		
		tabelaInterna.addCell(cellIdentBanco);
		tabelaInterna1.addCell(cellBanco);
		tabelaInterna1.addCell(cellBanco1);
		tabelaInterna1.addCell(cellBanco2);
		tabelaInterna1.addCell(cellBanco3);
		
		PdfPCell celulaBordaPrincipal1 = new PdfPCell();
		celulaBordaPrincipal1.setBorder(PdfCell.NO_BORDER);
		celulaBordaPrincipal1.setBorderWidth(0);
		celulaBordaPrincipal1.setPaddingTop(0);
		celulaBordaPrincipal1.setPaddingBottom(0);
		celulaBordaPrincipal1.addElement(tabelaInterna1);
		
		//		***********************************
		
		PdfPTable tabelaInterna2 = new PdfPTable(2);
		tabelaInterna2.setWidthPercentage(100f);
		tabelaInterna2.setWidths(new float[] {5.0f, 95.0f});

		PdfPCell cellEndereco = new PdfPCell();
		cellEndereco.setBorder(PdfCell.BOX);
		cellEndereco.addElement(new Paragraph(new Chunk(" 03", fTextoTabela)));
		cellEndereco.addElement(new Paragraph());
		
		PdfPCell cellEndereco1 = new PdfPCell();
		cellEndereco1.setBorder(PdfCell.BOX);
		cellEndereco1.addElement(pLinha3Dir);
		cellEndereco1.addElement(pLinha3Dir1);
		
		PdfPCell celulaBordaPrincipal2 = new PdfPCell();
		celulaBordaPrincipal2.setBorder(PdfCell.NO_BORDER);
		celulaBordaPrincipal2.setBorderWidth(0);
		celulaBordaPrincipal2.setPaddingTop(0);
		celulaBordaPrincipal2.setPaddingBottom(0);
		celulaBordaPrincipal2.addElement(tabelaInterna2);
		
		tabelaInterna2.addCell(cellEndereco);
		tabelaInterna2.addCell(cellEndereco1);
		
		//		***********************************
		
		PdfPTable tabelaInterna3 = new PdfPTable(1);
		tabelaInterna3.setWidthPercentage(100f);
		
		PdfPCell cellIdentCliente = new PdfPCell();
		cellIdentCliente.setBorder(PdfCell.BOX);
		cellIdentCliente.addElement(pLinha4Dir);
		
		PdfPCell celulaBordaPrincipal3 = new PdfPCell();
		celulaBordaPrincipal3.setBorder(PdfCell.NO_BORDER);
		celulaBordaPrincipal3.setBorderWidth(0);
		celulaBordaPrincipal3.setPaddingTop(0);
		celulaBordaPrincipal3.setPaddingBottom(0);
		celulaBordaPrincipal3.addElement(tabelaInterna3);
		
		tabelaInterna3.addCell(cellIdentCliente);
		
		//		***********************************
		
		PdfPTable tabelaInterna4 = new PdfPTable(4);
		tabelaInterna4.setWidthPercentage(100f);
		tabelaInterna4.setWidths(new float[] {5.0f, 60.0f, 5.0f, 30.0f});
		
		PdfPCell cellIdentCliente1 = new PdfPCell();
		cellIdentCliente1.setBorder(PdfCell.BOX);
		cellIdentCliente1.addElement(new Paragraph(new Chunk(" 04", fTextoTabela)));
		cellIdentCliente1.addElement(new Paragraph());
		
		PdfPCell cellIdentCliente2 = new PdfPCell();
		cellIdentCliente2.setBorder(PdfCell.BOX);
		cellIdentCliente2.addElement(new Paragraph(new Chunk("Nome do Cliente", fTextoTabelaNormal)));
		cellIdentCliente2.addElement(new Paragraph(new Chunk(saidaImprimirContrato.getNmParticipante() , fTextoTabela)));
		
		PdfPCell cellIdentCliente3 = new PdfPCell();
		cellIdentCliente3.setBorder(PdfCell.BOX);
		cellIdentCliente3.addElement(new Paragraph(new Chunk(" 05", fTextoTabela)));
		cellIdentCliente3.addElement(new Paragraph());
		
		PdfPCell cellIdentCliente4 = new PdfPCell();
		cellIdentCliente4.setBorder(PdfCell.BOX);
		cellIdentCliente4.addElement(new Paragraph(new Chunk("CNPJ/MF ou", fTextoTabelaNormal)));
		cellIdentCliente4.addElement(new Paragraph(new Chunk("	" + saidaImprimirContrato.getCdCpfCnpjParticipante() , fTextoTabela)));
		
		PdfPCell celulaBordaPrincipal4 = new PdfPCell();
		celulaBordaPrincipal4.setBorder(PdfCell.NO_BORDER);
		celulaBordaPrincipal4.setBorderWidth(0);
		celulaBordaPrincipal4.setPaddingTop(0);
		celulaBordaPrincipal4.setPaddingBottom(0);
		celulaBordaPrincipal4.addElement(tabelaInterna4);
		
		tabelaInterna4.addCell(cellIdentCliente1);
		tabelaInterna4.addCell(cellIdentCliente2);
		tabelaInterna4.addCell(cellIdentCliente3);
		tabelaInterna4.addCell(cellIdentCliente4);
		
//		***********************************
		PdfPTable tabelaInterna5 = new PdfPTable(2);
		tabelaInterna5.setWidthPercentage(100f);
		tabelaInterna5.setWidths(new float[] {5.0f, 95.0f});

		PdfPCell cellClienteEndereco = new PdfPCell();
		cellClienteEndereco.setBorder(PdfCell.BOX);
		cellClienteEndereco.addElement(new Paragraph(new Chunk(" 06", fTextoTabela)));
		cellClienteEndereco.addElement(new Paragraph());
		
		PdfPCell cellClienteEndereco1 = new PdfPCell();
		cellClienteEndereco1.setBorder(PdfCell.BOX);
		cellClienteEndereco1.addElement(new Paragraph(new Chunk("Sede / Endere�o", fTextoTabelaNormal)));
		cellClienteEndereco1.addElement(new Paragraph(new Chunk(saidaImprimirContrato.getDsLogradouroPessoa() + " " + saidaImprimirContrato.getDsLogradouroNumero() + " " + saidaImprimirContrato.getDsComplementoEndereco() + " " + saidaImprimirContrato.getDsBairroEndereco(), fTextoTabela)));
		
		PdfPCell celulaBordaPrincipal5 = new PdfPCell();
		celulaBordaPrincipal5.setBorder(PdfCell.NO_BORDER);
		celulaBordaPrincipal5.setBorderWidth(0);
		celulaBordaPrincipal5.setPaddingTop(0);
		celulaBordaPrincipal5.setPaddingBottom(0);
		celulaBordaPrincipal5.addElement(tabelaInterna5);
		
		tabelaInterna5.addCell(cellClienteEndereco);
		tabelaInterna5.addCell(cellClienteEndereco1);
		
		
		//		***********************************
		
		PdfPTable tabelaInterna6 = new PdfPTable(5);
		tabelaInterna6.setWidthPercentage(100f);
		tabelaInterna6.setWidths(new float[] {5.0f, 15.0f, 35.0f, 35.0f, 10.0f});

		PdfPCell cellClienteEndereco2 = new PdfPCell();
		cellClienteEndereco2.setBorder(PdfCell.BOX);
		cellClienteEndereco2.addElement(new Paragraph(new Chunk(" 07", fTextoTabela)));
		cellClienteEndereco2.addElement(new Paragraph());
		
		PdfPCell cellClienteEndereco3 = new PdfPCell();
		cellClienteEndereco3.setBorder(PdfCell.BOX);
		cellClienteEndereco3.addElement(new Paragraph(new Chunk("CEP", fTextoTabelaNormal)));
		cellClienteEndereco3.addElement(new Paragraph(new Chunk(saidaImprimirContrato.getCdCep() + "-" + saidaImprimirContrato.getCdCepComplemento(), fTextoTabela)));
		
		PdfPCell cellClienteEndereco4 = new PdfPCell();
		cellClienteEndereco4.setBorder(PdfCell.BOX);
		cellClienteEndereco4.addElement(new Paragraph(new Chunk("Bairro", fTextoTabelaNormal)));
		cellClienteEndereco4.addElement(new Paragraph(new Chunk(saidaImprimirContrato.getDsBairroEndereco(), fTextoTabela)));
		
		PdfPCell cellClienteEndereco5 = new PdfPCell();
		cellClienteEndereco5.setBorder(PdfCell.BOX);
		cellClienteEndereco5.addElement(new Paragraph(new Chunk("Cidade", fTextoTabelaNormal)));
		cellClienteEndereco5.addElement(new Paragraph(new Chunk(saidaImprimirContrato.getDsCidadeEndereco() , fTextoTabela)));
		
		PdfPCell cellClienteEndereco6 = new PdfPCell();
		cellClienteEndereco6.setBorder(PdfCell.BOX);
		cellClienteEndereco6.addElement(new Paragraph(new Chunk("UF", fTextoTabelaNormal)));
		cellClienteEndereco6.addElement(new Paragraph(new Chunk(saidaImprimirContrato.getCdSiglaUf() , fTextoTabela)));
		
		PdfPCell celulaBordaPrincipal6 = new PdfPCell();
		celulaBordaPrincipal6.setBorder(PdfCell.NO_BORDER);
		celulaBordaPrincipal6.setBorderWidth(0);
		celulaBordaPrincipal6.setPaddingTop(0);
		celulaBordaPrincipal6.setPaddingBottom(0);
		celulaBordaPrincipal6.addElement(tabelaInterna6);
		
		tabelaInterna6.addCell(cellClienteEndereco2);
		tabelaInterna6.addCell(cellClienteEndereco3);
		tabelaInterna6.addCell(cellClienteEndereco4);
		tabelaInterna6.addCell(cellClienteEndereco5);
		tabelaInterna6.addCell(cellClienteEndereco6);
		
//		***********************************
		
		PdfPTable tabelaInterna7 = new PdfPTable(5);
		tabelaInterna7.setWidthPercentage(100f);
		tabelaInterna7.setWidths(new float[] {5.0f, 50.0f, 5.0f, 10.0f, 30.0f});

		PdfPCell cellClienteEndereco7 = new PdfPCell();
		cellClienteEndereco7.setBorder(PdfCell.BOX);
		cellClienteEndereco7.addElement(new Paragraph(new Chunk(" 08", fTextoTabela)));
		cellClienteEndereco7.addElement(new Paragraph());
		
		PdfPCell cellClienteEndereco8 = new PdfPCell();
		cellClienteEndereco8.setBorder(PdfCell.BOX);
		cellClienteEndereco8.addElement(new Paragraph(new Chunk("C�digo	    Dig.     Ag�ncia", fTextoTabelaNormal)));
		cellClienteEndereco8.addElement(new Paragraph(new Chunk(StringUtils.rightPad(saidaImprimirContrato.getCdAgenciaGestorContrato().toString(), 18, " ") + StringUtils.rightPad(saidaImprimirContrato.getCdDigitoAgenciaGestor().toString(), 9 , " ") + saidaImprimirContrato.getDsAgenciaGestor(), fTextoTabela)));
		
		PdfPCell cellClienteEndereco9 = new PdfPCell();
		cellClienteEndereco9.setBorder(PdfCell.BOX);
		cellClienteEndereco9.addElement(new Paragraph(new Chunk(" 9", fTextoTabela)));
		cellClienteEndereco9.addElement(new Paragraph());
		
		PdfPCell cellClienteEndereco10 = new PdfPCell();
		cellClienteEndereco10.setBorder(PdfCell.BOX);
		cellClienteEndereco10.addElement(new Paragraph(new Chunk("Raz�o", fTextoTabelaNormal)));
		cellClienteEndereco10.addElement(new Paragraph(new Chunk("RZ" , fTextoTabela)));
		
		PdfPCell cellClienteEndereco11 = new PdfPCell();
		cellClienteEndereco11.setBorder(PdfCell.BOX);
		cellClienteEndereco11.addElement(new Paragraph(new Chunk("N� Conta Corrente	        Dig.", fTextoTabelaNormal)));
		cellClienteEndereco11.addElement(new Paragraph(new Chunk(StringUtils.rightPad(saidaImprimirContrato.getCdContaAgenciaGestor().toString(), 35, " ") + saidaImprimirContrato.getCdDigitoContaGestor() , fTextoTabela)));
		
		PdfPCell celulaBordaPrincipal7 = new PdfPCell();
		celulaBordaPrincipal7.setBorder(PdfCell.NO_BORDER);
		celulaBordaPrincipal7.setBorderWidth(0);
		celulaBordaPrincipal7.setPaddingTop(0);
		celulaBordaPrincipal7.setPaddingBottom(0);
		celulaBordaPrincipal7.addElement(tabelaInterna7);
		
		tabelaInterna7.addCell(cellClienteEndereco7);
		tabelaInterna7.addCell(cellClienteEndereco8);
		tabelaInterna7.addCell(cellClienteEndereco9);
		tabelaInterna7.addCell(cellClienteEndereco10);
		tabelaInterna7.addCell(cellClienteEndereco11);
		
		
		//		***********************************
		
		tabela.addCell(celulaBordaPrincipal);
		tabela.addCell(celulaBordaPrincipal1);
		tabela.addCell(celulaBordaPrincipal2);
		tabela.addCell(celulaBordaPrincipal3);
		tabela.addCell(celulaBordaPrincipal4);
		tabela.addCell(celulaBordaPrincipal5);
		tabela.addCell(celulaBordaPrincipal6);
		tabela.addCell(celulaBordaPrincipal7);

		document.add(tabela);
	}
	
	/**
	 * Preencher clausula condicoes.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherClausulaCondicoes(Document document) throws DocumentException {
		StringBuilder textoClausula = new StringBuilder();
		textoClausula.append("As partes acima qualificadas, ");
		textoClausula.append("doravante denominadas Banco e Cliente, ");
		textoClausula.append("por seus representantes legais ao final assinados, ");
		textoClausula.append("de acordo com os seus atos constitutivos atualizados, ");
		textoClausula.append("t�m entre si, justo e contratado este Contrato, ");
		textoClausula.append("nos termos das cl�usulas e condi��es a seguir.");
		Paragraph condicoes = new Paragraph(new Chunk(textoClausula.toString(), fTexto));
		condicoes.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(condicoes);
	}
	
	/**
	 * Preencher clausula primeira.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherClausulaPrimeira(Document document) throws DocumentException {
		StringBuilder textoClausula = new StringBuilder();
		textoClausula.append("Este Contrato tem por objeto a presta��o, ");
		textoClausula.append("pelo Banco ao Cliente, ");
		textoClausula.append("dos servi�os de pagamentos descritos e caracterizados no(s) Anexo(s), ");
		textoClausula.append("em todas as localidades onde o Banco mant�m ag�ncias.");
		Paragraph paragrafo = new Paragraph();
		paragrafo.add(new Chunk("Cl�usula Primeira: ", fTextoTabela));
		paragrafo.add(new Chunk(textoClausula.toString(), fTexto));
		paragrafo.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(paragrafo);
	}
	
	/**
	 * Preencher paragrafo unico.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherParagrafoUnico(Document document) throws DocumentException {
		StringBuilder textoClausula = new StringBuilder();
		textoClausula.append("Todas as condi��es comerciais e operacionais pertinentes ");
		textoClausula.append("aos servi�os contratados encontram-se dispostas no(s) respectivo(s) Anexo(s).");
		Paragraph paragrafo = new Paragraph();
		paragrafo.add(new Chunk("Par�grafo �nico: ", fTextoTabela));
		paragrafo.add(new Chunk(textoClausula.toString(), fTexto));
		paragrafo.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(paragrafo);
	}
	
	/**
	 * Preencher clausula segunda.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherClausulaSegunda(Document document) throws DocumentException {
		StringBuilder textoClausula = new StringBuilder();
		textoClausula.append("Nos casos em que o Banco fornecer � Cliente os softwares OBB Plus, ");
		textoClausula.append("SISLASER, SISRET ou BRADESCO NET EMPRESA (�software�) ");
		textoClausula.append("em regime de licen�a de uso gratuita de software, ");
		textoClausula.append("estes devem ser utilizados exclusivamente para processamento da Cobran�a Escritural Bradesco.");
		Paragraph paragrafo = new Paragraph();
		paragrafo.add(new Chunk("Cl�usula Segunda: ", fTextoTabela));
		paragrafo.add(new Chunk(textoClausula.toString(), fTexto));
		paragrafo.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(paragrafo);
	}
	
	/**
	 * Preencher paragrafo primeiro.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherParagrafoPrimeiro(Document document) throws DocumentException {
		StringBuilder textoClausula = new StringBuilder();
		textoClausula.append("Na hip�tese de o Cliente manifestar interesse na utiliza��o do Software, ");
		textoClausula.append("ser� licenciado gratuitamente pelo Banco ao Cliente, ");
		textoClausula.append("mediante aceita��o pelo Cliente das cl�usulas e condi��es dispostas no termo de aceite ");
		textoClausula.append("eletr�nico que ser� apresentado para concord�ncia quando da instala��o do Software, ");
		textoClausula.append("o qual passar� a fazer parte integrante deste Contrato e do(s) Anexo(s) para todos os fins e efeitos de direito.");
		Paragraph paragrafo = new Paragraph();
		paragrafo.add(new Chunk("Par�grafo Primeiro: ", fTextoTabela));
		paragrafo.add(new Chunk(textoClausula.toString(), fTexto));
		paragrafo.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(paragrafo);
	}
	
	/**
	 * Preencher paragrafo segundo.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherParagrafoSegundo(Document document) throws DocumentException {
		StringBuilder textoClausula = new StringBuilder();
		textoClausula.append("O Banco providenciar� a instala��o, manuten��o do Software e treinamento ");
		textoClausula.append("para sua operacionaliza��o, inclusive garantindo que o Software utilizado ");
		textoClausula.append("na presta��o do servi�o ora contratado, n�o infringe quaisquer patentes e direitos autorais.");
		Paragraph paragrafo = new Paragraph();
		paragrafo.add(new Chunk("Par�grafo Segundo: ", fTextoTabela));
		paragrafo.add(new Chunk(textoClausula.toString(), fTexto));
		paragrafo.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(paragrafo);
	}
	
	/**
	 * Preencher paragrafo terceiro.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherParagrafoTerceiro(Document document) throws DocumentException {
		StringBuilder textoClausula = new StringBuilder();
		textoClausula.append("O Cliente dever� comunicar de imediato o Banco em caso de qualquer ");
		textoClausula.append("tentativa de viola��o por terceiros dos direitos de propriedade e uso do Software, ");
		textoClausula.append("desde que tenha conhecimento de tal circunst�ncia, a fim de que sejam adotadas todas as medidas cab�veis, ");
		textoClausula.append("inclusive a��es legais e/ou medidas extrajudiciais que forem pertinentes, ");
		textoClausula.append("no interesse e na defesa dos direitos das partes.");
		Paragraph paragrafo = new Paragraph();
		paragrafo.add(new Chunk("Par�grafo Terceiro: ", fTextoTabela));
		paragrafo.add(new Chunk(textoClausula.toString(), fTexto));
		paragrafo.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(paragrafo);
	}
	
	/**
	 * Preencher paragrafo quarto.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherParagrafoQuarto(Document document) throws DocumentException {
		StringBuilder textoClausula = new StringBuilder();
		textoClausula.append("O Software, manuais e materiais informativos s�o considerados ");
		textoClausula.append("segredos de neg�cios e dever�o ser mantidos em confidencialidade pelo Cliente ");
		textoClausula.append("e usados exclusivamente na execu��o dos servi�os ora contratados, n�o podendo ser duplicados, copiados, ");
		textoClausula.append("reproduzidos, alterado seu conte�do original, ou exibidos, exceto com a autoriza��o pr�via e por escrito do Banco.");
		Paragraph paragrafo = new Paragraph();
		paragrafo.add(new Chunk("Par�grafo Quarto: ", fTextoTabela));
		paragrafo.add(new Chunk(textoClausula.toString(), fTexto));
		paragrafo.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(paragrafo);
	}
	
	/**
	 * Preencher paragrafo quinto.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherParagrafoQuinto(Document document) throws DocumentException {
		StringBuilder textoClausula = new StringBuilder();
		textoClausula.append("O Cliente deve respeitar e, conforme o caso, ");
		textoClausula.append("fazer respeitar por seus empregados, prepostos, clientes e terceiros que, de alguma forma venham a ter acesso ao Software, ");
		textoClausula.append("manuais e materiais informativos, ");
		textoClausula.append("os direitos de propriedade intelectual e a confidencialidade das informa��es recebidas do Banco.");
		Paragraph paragrafo = new Paragraph();
		paragrafo.add(new Chunk("Par�grafo Quinto: ", fTextoTabela));
		paragrafo.add(new Chunk(textoClausula.toString(), fTexto));
		paragrafo.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(paragrafo);
	}
	
	/**
	 * Preencher paragrafo sexto.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherParagrafoSexto(Document document) throws DocumentException {
		StringBuilder textoClausula = new StringBuilder();
		textoClausula.append("O Cliente n�o poder� ceder, dar em loca��o ou garantia, emprestar, ");
		textoClausula.append("doar ou alienar por qualquer forma, o Software objeto deste Contrato, ");
		textoClausula.append("assim como os manuais e materiais informativos, ");
		textoClausula.append("sem o pr�vio e expresso consentimento escrito do Banco.");
		Paragraph paragrafo = new Paragraph();
		paragrafo.add(new Chunk("Par�grafo Sexto: ", fTextoTabela));
		paragrafo.add(new Chunk(textoClausula.toString(), fTexto));
		paragrafo.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(paragrafo);
	}
	
	/**
	 * Preencher paragrafo setimo.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherParagrafoSetimo(Document document) throws DocumentException {
		StringBuilder textoClausula = new StringBuilder();
		textoClausula.append("Quando finda, por qualquer motivo, a presta��o de servi�os objeto deste Contrato ");
		textoClausula.append("e do(s) Anexo(s), o Cliente dever� deletar/apagar o Software dos hardwares dos computadores ");
		textoClausula.append("onde estiver instalado, e se obriga a manter sob absoluto sigilo, todas as informa��es, dados, ");
		textoClausula.append("materiais, pormenores, documentos, especifica��es t�cnicas e comerciais que lhe tenham sido confiadas.");
		Paragraph paragrafo = new Paragraph();
		paragrafo.add(new Chunk("Par�grafo S�timo: ", fTextoTabela));
		paragrafo.add(new Chunk(textoClausula.toString(), fTexto));
		paragrafo.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(paragrafo);
	}
	
	/**
	 * Preencher paragrafo oitavo.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherParagrafoOitavo(Document document) throws DocumentException {
		StringBuilder textoClausula = new StringBuilder();
		textoClausula.append("O Cliente assume inteira responsabilidade pela guarda, zelo, ");
		textoClausula.append("uso indevido ou fraudulento, por quem quer que seja, do Software, e tamb�m por eventuais preju�zos que venham a causar a si pr�prio, ");
		textoClausula.append("a terceiros ou ao Banco, ");
		textoClausula.append("decorrentes de acesso ao Software por pessoas n�o autorizadas ou credenciadas.");
		Paragraph paragrafo = new Paragraph();
		paragrafo.add(new Chunk("Par�grafo Oitavo: ", fTextoTabela));
		paragrafo.add(new Chunk(textoClausula.toString(), fTexto));
		paragrafo.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(paragrafo);
	}
	
	/**
	 * Preencher clausula terceira.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherClausulaTerceira(Document document) throws DocumentException {
		StringBuilder textoClausula = new StringBuilder();
		textoClausula.append("Este Contrato entrar� em vigor a partir da data da sua assinatura ");
		textoClausula.append("e vigorar� por tempo indeterminado, podendo, todavia, ser resilido a qualquer tempo, por qualquer das partes, ");
		textoClausula.append("sem que tenham direito a quaisquer indeniza��es ou compensa��es, mediante comunica��o escrita, ");
		textoClausula.append("com anteced�ncia de 30 (trinta) dias, contados a partir do recebimento da referida comunica��o pela outra parte.");
		Paragraph paragrafo = new Paragraph();
		paragrafo.add(new Chunk("Cl�usula Terceira: ", fTextoTabela));
		paragrafo.add(new Chunk(textoClausula.toString(), fTexto));
		paragrafo.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(paragrafo);
	}
	
	/**
	 * Preencher clausula terceira a.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherClausulaTerceiraA(Document document) throws DocumentException {
		StringBuilder textoClausula = new StringBuilder();
		textoClausula.append("O Cliente, de forma imotivada, poder� pleitear o cancelamento de quaisquer dos servi�os ");
		textoClausula.append("descritos em um dos Anexos deste Contrato, mediante notifica��o escrita ao Banco, com 30 (trinta) dias ");
		textoClausula.append("de anteced�ncia, contados a partir da data do recebimento da referida comunica��o. Nesta hip�tese, dar-se-� a o fim ");
		textoClausula.append("da presta��o dos servi�os, sem que qualquer uma das partes tenha direito a indeniza��es ou compensa��es. ");
		textoClausula.append("Tudo isto se dar�, sem preju�zo da continuidade da vig�ncia deste Contrato, ");
		textoClausula.append("com rela��o aos demais servi�os.");
		Paragraph paragrafo = new Paragraph();
		paragrafo.add(new Chunk("A) ", fTextoTabela));
		paragrafo.add(new Chunk(textoClausula.toString(), fTexto));
		paragrafo.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(paragrafo);
	}
	
	/**
	 * Preencher clausula terceira b.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherClausulaTerceiraB(Document document) throws DocumentException {
		StringBuilder textoClausula = new StringBuilder();
		textoClausula.append("O Banco, tamb�m de forma imotivada, poder� pleitear o cancelamento de quaisquer dos servi�os ");
		textoClausula.append("descritos em um dos Anexos deste Contrato, mediante notifica��o escrita ao Cliente, com 30 (trinta) dias ");
		textoClausula.append("de anteced�ncia, contados a partir da data do recebimento da referida comunica��o. Nesta hip�tese, ");
		textoClausula.append("dar-se-� o fim da presta��o de servi�os sem que qualquer uma das partes tenha direito a indeniza��es ou compensa��es. ");
		textoClausula.append("Tudo isto se dar�, sem preju�zo da continuidade da vig�ncia deste Contrato, ");
		textoClausula.append("com rela��o aos demais servi�os.");
		Paragraph paragrafo = new Paragraph();
		paragrafo.add(new Chunk("b) ", fTextoTabela));
		paragrafo.add(new Chunk(textoClausula.toString(), fTexto));
		paragrafo.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(paragrafo);
	}
	
	/**
	 * Preencher paragrafo unico terceira.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherParagrafoUnicoTerceira(Document document) throws DocumentException {
		StringBuilder textoClausula = new StringBuilder();
		textoClausula.append("previstas em lei, este Contrato ser� resilido de imediato e sem qualquer aviso, ");
		textoClausula.append("nas seguintes hip�teses: (a) se quaisquer das partes falir, ingressar com pedido de recupera��o judicial ");
		textoClausula.append("ou iniciar procedimentos de recupera��o extrajudicial, tiver sua fal�ncia ou liquida��o requerida, ou tiver ");
		textoClausula.append("sua insolv�ncia requerida/decretada; (b) se o Cliente suspender suas atividades por per�odo superior a " + saidaImprimirContrato.getQtDiaAviso() + " dia(s).");
		Paragraph paragrafo = new Paragraph();
		paragrafo.add(new Chunk("Par�grafo �nico: ", fTextoTabela));
		paragrafo.add(new Chunk(textoClausula.toString(), fTexto));
		paragrafo.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(paragrafo);
	}
	
	/**
	 * Preencher clausula quarta.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherClausulaQuarta(Document document) throws DocumentException {
		StringBuilder textoClausula = new StringBuilder();
		textoClausula.append("A infra��o de quaisquer das cl�usulas ou condi��es aqui estipuladas, ");
		textoClausula.append("poder� ensejar imediata resili��o deste Contrato, por simples notifica��o escrita com indica��o da den�ncia ");
		textoClausula.append("� parte infratora, que ter� prazo de 30 (trinta) dias, ap�s o recebimento, para sanar a falta. ");
		textoClausula.append("Decorrido o prazo e n�o tendo sido sanada a falta, o Contrato ficar� resilido de pleno direito, respondendo ainda, ");
		textoClausula.append("a parte infratora pelas perdas e danos decorrentes.");
		Paragraph paragrafo = new Paragraph();
		paragrafo.add(new Chunk("Cl�usula Quarta: ", fTextoTabela));
		paragrafo.add(new Chunk(textoClausula.toString(), fTexto));
		paragrafo.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(paragrafo);
	}
	
	/**
	 * Preencher clausula quarta a.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherClausulaQuartaA(Document document) throws DocumentException {
		StringBuilder textoClausula = new StringBuilder();
		textoClausula.append("O Cliente, em raz�o de infra��o contratual por parte do Banco, poder� pleitear o cancelamento ");
		textoClausula.append("de quaisquer dos servi�os descritos em um dos Anexos deste Contrato, mediante notifica��o escrita ao Banco, ");
		textoClausula.append("com 10 (dez) dias �teis de anteced�ncia, contados a partir da data do recebimento da referida comunica��o. ");
		textoClausula.append("Nesta hip�tese, dar-se-� o fim da presta��o de um dos servi�os, sem preju�zo, contudo, ");
		textoClausula.append("da continuidade da vig�ncia deste Contrato, com rela��o aos demais.");
		Paragraph paragrafo = new Paragraph();
		paragrafo.add(new Chunk("A) ", fTextoTabela));
		paragrafo.add(new Chunk(textoClausula.toString(), fTexto));
		paragrafo.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(paragrafo);
	}
	
	/**
	 * Preencher clausula quarta b.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherClausulaQuartaB(Document document) throws DocumentException {
		StringBuilder textoClausula = new StringBuilder();
		textoClausula.append("O Banco, tamb�m em raz�o de infra��o contratual, poder� pleitear o cancelamento de quaisquer dos ");
		textoClausula.append("servi�os descritos em um dos Anexos deste Contrato, mediante notifica��o escrita ao Cliente, com ");
		textoClausula.append("10 (dez) dias de anteced�ncia, contados a partir da data do recebimento da referida comunica��o. ");
		textoClausula.append("Nesta hip�tese, dar-se-� o fim da presta��o de um dos servi�os, sem preju�zo, contudo, da continuidade");
		textoClausula.append("da vig�ncia deste Contrato, com rela��o aos demais.");
		Paragraph paragrafo = new Paragraph();
		paragrafo.add(new Chunk("b) ", fTextoTabela));
		paragrafo.add(new Chunk(textoClausula.toString(), fTexto));
		paragrafo.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(paragrafo);
	}
	
	/**
	 * Preencher clausula quinta.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherClausulaQuinta(Document document) throws DocumentException {
		StringBuilder textoClausula = new StringBuilder();
		textoClausula.append("Fica expressamente vedado ao Cliente a utiliza��o dos termos deste Contrato ");
		textoClausula.append("em divulga��o ou publicidade, bem como o uso do nome, marca e logomarca do Banco, para qualquer finalidade ");
		textoClausula.append("e em qualquer meio de comunica��o, quer seja na m�dia impressa, escrita, falada ou eletr�nica, incluindo-se, ");
		textoClausula.append("por�m, sem se limitar, a publica��o em portf�lio de produtos e servi�os, links, etc., sendo que a sua infra��o ");
		textoClausula.append("poder� ensejar a rescis�o autom�tica deste Contrato, a crit�rio do Banco, ");
		textoClausula.append("al�m de sujeitar-se o Cliente ao pagamento das perdas e danos que forem apuradas.");
		Paragraph paragrafo = new Paragraph();
		paragrafo.add(new Chunk("Cl�usula Quinta: ", fTextoTabela));
		paragrafo.add(new Chunk(textoClausula.toString(), fTexto));
		paragrafo.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(paragrafo);
	}
	
	/**
	 * Preencher clausula sexta.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherClausulaSexta(Document document) throws DocumentException {
		StringBuilder textoClausula = new StringBuilder();
		textoClausula.append("Este Contrato cancela eventuais contratos celebrados ");
		textoClausula.append("anteriormente entre as partes, para o mesmo fim, prevalecendo as condi��es aqui estipuladas.");
		Paragraph paragrafo = new Paragraph();
		paragrafo.add(new Chunk("Cl�usula Sexta: ", fTextoTabela));
		paragrafo.add(new Chunk(textoClausula.toString(), fTexto));
		paragrafo.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(paragrafo);
	}
	
	/**
	 * Preencher clausula setima.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherClausulaSetima(Document document) throws DocumentException {
		StringBuilder textoClausula = new StringBuilder();
		textoClausula.append("Os tributos que forem devidos em decorr�ncia direta ou indireta deste Contrato, ");
		textoClausula.append("ou de sua execu��o, constituem �nus de responsabilidade do contribuinte, conforme definido na lei tribut�ria.");
		Paragraph paragrafo = new Paragraph();
		paragrafo.add(new Chunk("Cl�usula S�tima: ", fTextoTabela));
		paragrafo.add(new Chunk(textoClausula.toString(), fTexto));
		paragrafo.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(paragrafo);
	}
	
	/**
	 * Preencher clausula oitava.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherClausulaOitava(Document document) throws DocumentException {
		StringBuilder textoClausula = new StringBuilder();
		textoClausula.append("Este Contrato constitui todo o entendimento e acordo entre as partes e substitui ");
		textoClausula.append("todas as garantias, condi��es, promessas, declara��es, contratos e acordos verbais ou escritos, ");
		textoClausula.append("anteriores sobre o objeto deste Contrato.");
		Paragraph paragrafo = new Paragraph();
		paragrafo.add(new Chunk("Cl�usula Oitava: ", fTextoTabela));
		paragrafo.add(new Chunk(textoClausula.toString(), fTexto));
		paragrafo.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(paragrafo);
	}
	
	/**
	 * Preencher clausula nona.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherClausulaNona(Document document) throws DocumentException {
		StringBuilder textoClausula = new StringBuilder();
		textoClausula.append("Eventuais inclus�es de outras cl�usulas, exclus�es ou altera��es das existentes, ");
		textoClausula.append("ser�o consignadas em aditivo devidamente assinado pelas partes, ");
		textoClausula.append("que passar� a fazer parte integrante deste Contrato.");
		Paragraph paragrafo = new Paragraph();
		paragrafo.add(new Chunk("Cl�usula Nona: ", fTextoTabela));
		paragrafo.add(new Chunk(textoClausula.toString(), fTexto));
		paragrafo.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(paragrafo);
	}
	
	/**
	 * Preencher clausula dez.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherClausulaDez(Document document) throws DocumentException {
		StringBuilder textoClausula = new StringBuilder();
		textoClausula.append("O Cliente, na forma aqui representado, declara estar ciente das disposi��es ");
		textoClausula.append("do C�digo de  Conduta �tica da Organiza��o Bradesco, cujo exemplar lhe � entregue neste ato, ");
		textoClausula.append("comprometendo-se em cumpri-lo e faz�-lo cumprir por seus empregados ou prepostos.");
		Paragraph paragrafo = new Paragraph();
		paragrafo.add(new Chunk("Cl�usula Dez: ", fTextoTabela));
		paragrafo.add(new Chunk(textoClausula.toString(), fTexto));
		paragrafo.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(paragrafo);
	}
	
	/**
	 * Preencher clausula onze.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherClausulaOnze(Document document) throws DocumentException {
		StringBuilder textoClausula = new StringBuilder();
		textoClausula.append("Cada uma das partes garante � outra parte: (i) que est� investida de todos os poderes ");
		textoClausula.append("e autoridade para firmar e cumprir as obriga��es aqui previstas e consumar as transa��es aqui contempladas; e, ");
		textoClausula.append("(ii) que a assinatura e o cumprimento deste Contrato n�o resulta viola��o de qualquer direito de terceiros, ");
		textoClausula.append("lei ou regulamento aplic�vel ou, ainda, viola��o, descumprimento ou inadimplemento de qualquer contrato, ");
		textoClausula.append("instrumento ou documento do qual seja parte ou pelo qual tenha qualquer ou quaisquer de suas propriedades ");
		textoClausula.append("vinculadas e/ou afetadas, nem na necessidade de obter qualquer autoriza��o nos termos de qualquer contrato, ");
		textoClausula.append("instrumento ou documento do qual seja parte, ou pelo qual tenha qualquer ou quaisquer de suas propriedades vinculadas e/ou afetadas.");
		Paragraph paragrafo = new Paragraph();
		paragrafo.add(new Chunk("Cl�usula Onze: ", fTextoTabela));
		paragrafo.add(new Chunk(textoClausula.toString(), fTexto));
		paragrafo.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(paragrafo);
	}
	
	/**
	 * Preencher clausula doze.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherClausulaDoze(Document document) throws DocumentException {
		StringBuilder textoClausula = new StringBuilder();
		textoClausula.append("O Cliente assume neste ato, de maneira irrevog�vel e irretrat�vel, total e integral ");
		textoClausula.append("responsabilidade por quaisquer perdas e danos, pessoais, morais ou materiais, que vierem a ser sofridos pelo ");
		textoClausula.append("Banco ou terceiros, em raz�o da presta��o/execu��o dos servi�os ora aven�ada e que decorram ");
		textoClausula.append("da culpa do Cliente, e, se for o caso, de seus empregados ou prepostos.");
		Paragraph paragrafo = new Paragraph();
		paragrafo.add(new Chunk("Cl�usula Doze: ", fTextoTabela));
		paragrafo.add(new Chunk(textoClausula.toString(), fTexto));
		paragrafo.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(paragrafo);
	}
	
	/**
	 * Preencher clausula treze.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherClausulaTreze(Document document) throws DocumentException {
		StringBuilder textoClausula = new StringBuilder();
		textoClausula.append("As partes s�o consideradas contratantes independentes e nada deste Contrato criar� ");
		textoClausula.append("qualquer outro v�nculo entre ambas, seja pelo aspecto empregat�cio, seja por quaisquer outros aspectos, ");
		textoClausula.append("tais como agente comercial, sociedade subsidi�ria, representa��o legal ou associa��o de neg�cios.");
		Paragraph paragrafo = new Paragraph();
		paragrafo.add(new Chunk("Cl�usula Treze: ", fTextoTabela));
		paragrafo.add(new Chunk(textoClausula.toString(), fTexto));
		paragrafo.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(paragrafo);
	}
	
	/**
	 * Preencher clausula quatorze.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherClausulaQuatorze(Document document) throws DocumentException {
		StringBuilder textoClausula = new StringBuilder();
		textoClausula.append("As partes declaram, neste ato, que a celebra��o deste Contrato, n�o implicou ");
		textoClausula.append("em investimentos consider�veis para a execu��o dos servi�os ora contratados e/ou fornecimento ");
		textoClausula.append("de materiais/produtos.");
		Paragraph paragrafo = new Paragraph();
		paragrafo.add(new Chunk("Cl�usula Quatorze: ", fTextoTabela));
		paragrafo.add(new Chunk(textoClausula.toString(), fTexto));
		paragrafo.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(paragrafo);
	}
	
	/**
	 * Preencher clausula quinze.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherClausulaQuinze(Document document) throws DocumentException {
		StringBuilder textoClausula = new StringBuilder();
		textoClausula.append("Os casos fortuitos e de for�a maior s�o excludentes da responsabilidade ");
		textoClausula.append("das partes, nos termos do artigo 393 do C�digo Civil Brasileiro.");
		Paragraph paragrafo = new Paragraph();
		paragrafo.add(new Chunk("Cl�usula Quinze: ", fTextoTabela));
		paragrafo.add(new Chunk(textoClausula.toString(), fTexto));
		paragrafo.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(paragrafo);
	}
	
	/**
	 * Preencher clausula dezesseis.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherClausulaDezesseis(Document document) throws DocumentException {
		StringBuilder textoClausula = new StringBuilder();
		textoClausula.append("As partes declaram que tiveram pr�vio conhecimento de todas as cl�usulas ");
		textoClausula.append("e condi��es deste Contrato, concordando expressamente com todos os seus termos.");
		Paragraph paragrafo = new Paragraph();
		paragrafo.add(new Chunk("Cl�usula Dezesseis: ", fTextoTabela));
		paragrafo.add(new Chunk(textoClausula.toString(), fTexto));
		paragrafo.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(paragrafo);
	}
	
	/**
	 * Preencher clausula dezessete.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherClausulaDezessete(Document document) throws DocumentException {
		StringBuilder textoClausula = new StringBuilder();
		textoClausula.append("O(s) Anexo(s) devidamente datado(s) e assinado(s) pelas partes integra(m) ");
		textoClausula.append("este Contrato para todos os fins e efeitos de direito, como se nele estivesse(m) transcrito(s).");
		Paragraph paragrafo = new Paragraph();
		paragrafo.add(new Chunk("Cl�usula Dezessete: ", fTextoTabela));
		paragrafo.add(new Chunk(textoClausula.toString(), fTexto));
		paragrafo.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(paragrafo);
	}
	
	/**
	 * Preencher clausula dezoito.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherClausulaDezoito(Document document) throws DocumentException {
		StringBuilder textoClausula = new StringBuilder();
		textoClausula.append("As partes declaram e garantem mutuamente, inclusive, ");
		textoClausula.append("se for o caso, perante seus fornecedores de bens e servi�os, que:");
		Paragraph paragrafo = new Paragraph();
		paragrafo.add(new Chunk("Cl�usula Dezoito: ", fTextoTabela));
		paragrafo.add(new Chunk(textoClausula.toString(), fTexto));
		paragrafo.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(paragrafo);
	}
	
	/**
	 * Preencher clausula dezoito a.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherClausulaDezoitoA(Document document) throws DocumentException {
		StringBuilder textoClausula = new StringBuilder();
		textoClausula.append("exercem suas atividades em conformidade com a legisla��o ");
		textoClausula.append("vigente a elas aplic�vel, e que det�m as aprova��es necess�rias � celebra��o deste Contrato, ");
		textoClausula.append("e ao cumprimento das obriga��es nele previstas;");
		Paragraph paragrafo = new Paragraph();
		paragrafo.add(new Chunk("a) ", fTextoTabela));
		paragrafo.add(new Chunk(textoClausula.toString(), fTexto));
		paragrafo.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(paragrafo);
	}
	
	/**
	 * Preencher clausula dezoito b.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherClausulaDezoitoB(Document document) throws DocumentException {
		StringBuilder textoClausula = new StringBuilder();
		textoClausula.append("utilizam de trabalho ilegal, e comprometem-se a n�o utilizar pr�ticas de trabalho an�logo ");
		textoClausula.append("ao escravo, ou de m�o de obra infantil, salvo este �ltimo na condi��o de aprendiz, observadas �s disposi��es");
		textoClausula.append("da Consolida��o das Leis do Trabalho, seja direta ou indiretamente, ");
		textoClausula.append("por meio de seus respectivos fornecedores de produtos e de servi�os;");
		Paragraph paragrafo = new Paragraph();
		paragrafo.add(new Chunk("b) ", fTextoTabela));
		paragrafo.add(new Chunk(textoClausula.toString(), fTexto));
		paragrafo.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(paragrafo);
	}
	
	/**
	 * Preencher clausula dezoito c.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherClausulaDezoitoC(Document document) throws DocumentException {
		StringBuilder textoClausula = new StringBuilder();
		textoClausula.append("n�o empregam menor at� 18 (dezoito) anos, inclusive menor aprendiz, em locais prejudiciais ");
		textoClausula.append("� sua forma��o, ao seu desenvolvimento f�sico, ps�quico, moral e social, bem como em locais e servi�os");
		textoClausula.append("perigosos ou insalubres, em hor�rios que n�o permitam a freq��ncia � escola e, ainda, em hor�rio noturno, ");
		textoClausula.append("considerando este o per�odo compreendido entre as 22h e 5h;");
		Paragraph paragrafo = new Paragraph();
		paragrafo.add(new Chunk("c) ", fTextoTabela));
		paragrafo.add(new Chunk(textoClausula.toString(), fTexto));
		paragrafo.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(paragrafo);
	}
	
	/**
	 * Preencher clausula dezoito d.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherClausulaDezoitoD(Document document) throws DocumentException {
		StringBuilder textoClausula = new StringBuilder();
		textoClausula.append("n�o utilizam pr�ticas de discrimina��o negativa, e limitativas ao acesso na rela��o de emprego ");
		textoClausula.append("ou a sua manuten��o, tais como, mas n�o se limitando a, motivos de: sexo, origem, ra�a, cor, ");
		textoClausula.append("condi��o f�sica, religi�o, estado civil, idade, situa��o familiar ou estado grav�dico;");
		Paragraph paragrafo = new Paragraph();
		paragrafo.add(new Chunk("d) ", fTextoTabela));
		paragrafo.add(new Chunk(textoClausula.toString(), fTexto));
		paragrafo.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(paragrafo);
	}
	
	/**
	 * Preencher clausula dezoito e.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherClausulaDezoitoE(Document document) throws DocumentException {
		StringBuilder textoClausula = new StringBuilder();
		textoClausula.append("-se a proteger e preservar o meio ambiente, bem como a prevenir e erradicar ");
		textoClausula.append("pr�ticas danosas ao meio ambiente, executando seus servi�os em observ�ncia � legisla��o vigente no que ");
		textoClausula.append("tange � Pol�tica Nacional do Meio Ambiente e dos Crimes Ambientais, bem como dos atos legais, ");
		textoClausula.append("normativos e administrativos relativos � �rea ambiental e correlatas, ");
		textoClausula.append("emanados das esferas Federal, Estaduais e Municipais.");
		Paragraph paragrafo = new Paragraph();
		paragrafo.add(new Chunk("e) ", fTextoTabela));
		paragrafo.add(new Chunk(textoClausula.toString(), fTexto));
		paragrafo.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(paragrafo);
	}
	
	/**
	 * Preencher clausula dezenove.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherClausulaDezenove(Document document) throws DocumentException {
		StringBuilder textoClausula = new StringBuilder();
		textoClausula.append("Para dirimir qualquer pend�ncia ou a��o que se originar deste Contrato ");
		textoClausula.append("e do(s) seu(s) Anexo(s), fica eleito o Foro de domic�lio do Cliente, com ren�ncia a qualquer outro, ");
		textoClausula.append("por mais privilegiado que seja ou venha a ser.");
		Paragraph paragrafo = new Paragraph();
		paragrafo.add(new Chunk("Cl�usula Dezenove: ", fTextoTabela));
		paragrafo.add(new Chunk(textoClausula.toString(), fTexto));
		paragrafo.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(paragrafo);
	}
	
	/**
	 * Preencher clausula condicao.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherClausulaCondicao(Document document) throws DocumentException {
		StringBuilder textoClausula = new StringBuilder();
		textoClausula.append("Por se acharem de pleno acordo, com tudo aqui pactuado, ");
		textoClausula.append("as partes firmam este Contrato em 03 (tr�s) vias de igual teor e forma, ");
		textoClausula.append("juntamente com as testemunhas abaixo.");
		Paragraph condicoes4 = new Paragraph(new Chunk(textoClausula.toString(), fTexto));
		condicoes4.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(condicoes4);
	}
	
	/**
	 * Preencher data.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherData(Document document) throws DocumentException {
		Paragraph data = new Paragraph(new Chunk("Osasco, " + DateUtils.formatarDataPorExtenso(new Date()) + ".", fTexto));
		data.setAlignment(Paragraph.ALIGN_RIGHT);
		document.add(data);
	}
	
	/**
	 * Assinatura um.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void assinaturaUm(Document document) throws DocumentException {
		
		PdfPTable tabela = new PdfPTable(2);
		tabela.setWidthPercentage(100f);
		
		Paragraph pLinha1Dir = new Paragraph(new Chunk("_________________________________________", fTextoTabela));
		pLinha1Dir.setAlignment(Element.ALIGN_CENTER);
		Paragraph pLinha1Esq = new Paragraph(new Chunk("_________________________________________", fTextoTabela));
		pLinha1Esq.setAlignment(Element.ALIGN_CENTER);
		Paragraph pLinha2Dir = new Paragraph(new Chunk("  CLIENTE: ", fTextoTabela));
		pLinha2Dir.setAlignment(Element.ALIGN_LEFT);
		Paragraph pLinha2Esq = new Paragraph(new Chunk("  BANCO BRADESCO S/A:", fTextoTabela));
		pLinha2Esq.setAlignment(Element.ALIGN_LEFT);
		Paragraph pLinha3Esq = new Paragraph(new Chunk("  Ag�ncia: " , fTexto));
		pLinha3Esq.setAlignment(Element.ALIGN_LEFT);
				
		PdfPCell celula1 = new PdfPCell();
		celula1.setBorder(PdfCell.NO_BORDER);
		
		celula1.addElement(pLinha1Dir);
		celula1.addElement(pLinha2Dir);
				
		PdfPCell celula2 = new PdfPCell();
		celula2.setBorder(PdfCell.NO_BORDER);
		
		celula2.addElement(pLinha1Esq);
		celula2.addElement(pLinha2Esq);
		celula2.addElement(pLinha3Esq);
				
		tabela.addCell(celula1);
		tabela.addCell(celula2);
		
		document.add(tabela);
		
	}
	
	/**
	 * Preencher assinaturas3.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherAssinaturas3(Document document) throws DocumentException {
		Paragraph testemunha = new Paragraph(new Chunk("  TESTEMUNHAS:"));
		testemunha.setAlignment(Element.ALIGN_LEFT);
		document.add(testemunha);
	}
	
	/**
	 * Assinatura dois.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void assinaturaDois(Document document) throws DocumentException {
		
		PdfPTable tabela = new PdfPTable(2);
		tabela.setWidthPercentage(100f);
		
		Paragraph pLinha1Dir = new Paragraph(new Chunk("_________________________________________", fTextoTabela));
		pLinha1Dir.setAlignment(Element.ALIGN_CENTER);
		Paragraph pLinha1Esq = new Paragraph(new Chunk("_________________________________________", fTextoTabela));
		pLinha1Esq.setAlignment(Element.ALIGN_CENTER);
		Paragraph pLinha2Dir = new Paragraph(new Chunk("  NOME:", fTextoTabela));
		pLinha2Dir.setAlignment(Element.ALIGN_LEFT);
		Paragraph pLinha3Dir = new Paragraph(new Chunk("  NOME:", fTextoTabela));
		pLinha3Dir.setAlignment(Element.ALIGN_LEFT);
		Paragraph pLinha2Esq = new Paragraph(new Chunk("  CPF:", fTextoTabela));
		pLinha2Esq.setAlignment(Element.ALIGN_LEFT);
		Paragraph pLinha3Esq = new Paragraph(new Chunk("  CPF:", fTextoTabela));
		pLinha3Esq.setAlignment(Element.ALIGN_LEFT);
				
		PdfPCell celula1 = new PdfPCell();
		celula1.setBorder(PdfCell.NO_BORDER);
		
		celula1.addElement(pLinha1Dir);
		celula1.addElement(pLinha2Dir);
		celula1.addElement(pLinha3Esq);
				
		PdfPCell celula2 = new PdfPCell();
		celula2.setBorder(PdfCell.NO_BORDER);
		
		celula2.addElement(pLinha1Esq);
		celula2.addElement(pLinha2Esq);
		celula2.addElement(pLinha3Dir);
				
		tabela.addCell(celula1);
		tabela.addCell(celula2);
		
		document.add(tabela);
		
	}
	
	
}
