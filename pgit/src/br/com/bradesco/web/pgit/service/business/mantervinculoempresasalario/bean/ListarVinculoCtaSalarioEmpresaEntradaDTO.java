/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantervinculoempresasalario.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantervinculoempresasalario.bean;

/**
 * Nome: ListarVinculoCtaSalarioEmpresaEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarVinculoCtaSalarioEmpresaEntradaDTO {
    
	 /** Atributo nrOcorrencias. */
 	private Integer nrOcorrencias;
     
     /** Atributo cdPessoaJuridNegocio. */
     private Long cdPessoaJuridNegocio;
     
     /** Atributo cdTipoContratoNegocio. */
     private Integer cdTipoContratoNegocio;
     
     /** Atributo nrSeqContratoNegocio. */
     private Long nrSeqContratoNegocio;
     
     /** Atributo cdBancoSalario. */
     private Integer cdBancoSalario;
     
     /** Atributo cdAgenciaSalario. */
     private Integer cdAgenciaSalario;
     
     /** Atributo cdDigitoAgenciaSalario. */
     private Integer cdDigitoAgenciaSalario;
     
     /** Atributo cdContaSalario. */
     private Long cdContaSalario;
     
     /** Atributo cdDigitoContaSalario. */
     private String cdDigitoContaSalario;
     
	/**
	 * Get: cdDigitoAgenciaSalario.
	 *
	 * @return cdDigitoAgenciaSalario
	 */
	public Integer getCdDigitoAgenciaSalario() {
		return cdDigitoAgenciaSalario;
	}

	/**
	 * Set: cdDigitoAgenciaSalario.
	 *
	 * @param cdDigitoAgenciaSalario the cd digito agencia salario
	 */
	public void setCdDigitoAgenciaSalario(Integer cdDigitoAgenciaSalario) {
		this.cdDigitoAgenciaSalario = cdDigitoAgenciaSalario;
	}

	/**
	 * Listar vinculo cta salario empresa entrada dto.
	 */
	public ListarVinculoCtaSalarioEmpresaEntradaDTO() {
		super();
	}
	
	/**
	 * Get: cdAgenciaSalario.
	 *
	 * @return cdAgenciaSalario
	 */
	public Integer getCdAgenciaSalario() {
		return cdAgenciaSalario;
	}

	/**
	 * Set: cdAgenciaSalario.
	 *
	 * @param cdAgenciaSalario the cd agencia salario
	 */
	public void setCdAgenciaSalario(Integer cdAgenciaSalario) {
		this.cdAgenciaSalario = cdAgenciaSalario;
	}

	/**
	 * Get: cdBancoSalario.
	 *
	 * @return cdBancoSalario
	 */
	public Integer getCdBancoSalario() {
		return cdBancoSalario;
	}

	/**
	 * Set: cdBancoSalario.
	 *
	 * @param cdBancoSalario the cd banco salario
	 */
	public void setCdBancoSalario(Integer cdBancoSalario) {
		this.cdBancoSalario = cdBancoSalario;
	}

	/**
	 * Get: cdContaSalario.
	 *
	 * @return cdContaSalario
	 */
	public Long getCdContaSalario() {
		return cdContaSalario;
	}

	/**
	 * Set: cdContaSalario.
	 *
	 * @param cdContaSalario the cd conta salario
	 */
	public void setCdContaSalario(Long cdContaSalario) {
		this.cdContaSalario = cdContaSalario;
	}

	/**
	 * Get: cdDigitoContaSalario.
	 *
	 * @return cdDigitoContaSalario
	 */
	public String getCdDigitoContaSalario() {
		return cdDigitoContaSalario;
	}

	/**
	 * Set: cdDigitoContaSalario.
	 *
	 * @param cdDigitoContaSalario the cd digito conta salario
	 */
	public void setCdDigitoContaSalario(String cdDigitoContaSalario) {
		this.cdDigitoContaSalario = cdDigitoContaSalario;
	}

	/**
	 * Get: cdPessoaJuridNegocio.
	 *
	 * @return cdPessoaJuridNegocio
	 */
	public Long getCdPessoaJuridNegocio() {
		return cdPessoaJuridNegocio;
	}

	/**
	 * Set: cdPessoaJuridNegocio.
	 *
	 * @param cdPessoaJuridNegocio the cd pessoa jurid negocio
	 */
	public void setCdPessoaJuridNegocio(Long cdPessoaJuridNegocio) {
		this.cdPessoaJuridNegocio = cdPessoaJuridNegocio;
	}

	/**
	 * Get: cdTipoContratoNegocio.
	 *
	 * @return cdTipoContratoNegocio
	 */
	public Integer getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}
	
	/**
	 * Set: cdTipoContratoNegocio.
	 *
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}

	/**
	 * Get: nrOcorrencias.
	 *
	 * @return nrOcorrencias
	 */
	public Integer getNrOcorrencias() {
		return nrOcorrencias;
	}
	
	/**
	 * Set: nrOcorrencias.
	 *
	 * @param nrOcorrencias the nr ocorrencias
	 */
	public void setNrOcorrencias(Integer nrOcorrencias) {
		this.nrOcorrencias = nrOcorrencias;
	}
	
	/**
	 * Get: nrSeqContratoNegocio.
	 *
	 * @return nrSeqContratoNegocio
	 */
	public Long getNrSeqContratoNegocio() {
		return nrSeqContratoNegocio;
	}
	
	/**
	 * Set: nrSeqContratoNegocio.
	 *
	 * @param nrSeqContratoNegocio the nr seq contrato negocio
	 */
	public void setNrSeqContratoNegocio(Long nrSeqContratoNegocio) {
		this.nrSeqContratoNegocio = nrSeqContratoNegocio;
	}

}
