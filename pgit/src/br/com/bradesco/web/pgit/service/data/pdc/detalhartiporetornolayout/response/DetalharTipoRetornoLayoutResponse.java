/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.detalhartiporetornolayout.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class DetalharTipoRetornoLayoutResponse.
 * 
 * @version $Revision$ $Date$
 */
public class DetalharTipoRetornoLayoutResponse implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _codMensagem
     */
    private java.lang.String _codMensagem;

    /**
     * Field _mensagem
     */
    private java.lang.String _mensagem;

    /**
     * Field _cdTipoGeracaoRetorno
     */
    private int _cdTipoGeracaoRetorno = 0;

    /**
     * keeps track of state for field: _cdTipoGeracaoRetorno
     */
    private boolean _has_cdTipoGeracaoRetorno;

    /**
     * Field _dsTipoGeracaoRetorno
     */
    private java.lang.String _dsTipoGeracaoRetorno;

    /**
     * Field _cdTipoOcorrenciaRetorno
     */
    private int _cdTipoOcorrenciaRetorno = 0;

    /**
     * keeps track of state for field: _cdTipoOcorrenciaRetorno
     */
    private boolean _has_cdTipoOcorrenciaRetorno;

    /**
     * Field _dsTipoOcorrenciaRetorno
     */
    private java.lang.String _dsTipoOcorrenciaRetorno;

    /**
     * Field _cdTipoMontaRetorno
     */
    private int _cdTipoMontaRetorno = 0;

    /**
     * keeps track of state for field: _cdTipoMontaRetorno
     */
    private boolean _has_cdTipoMontaRetorno;

    /**
     * Field _dsTipoMontaRetorno
     */
    private java.lang.String _dsTipoMontaRetorno;

    /**
     * Field _cdTipoClassificacaoRetorno
     */
    private int _cdTipoClassificacaoRetorno = 0;

    /**
     * keeps track of state for field: _cdTipoClassificacaoRetorno
     */
    private boolean _has_cdTipoClassificacaoRetorno;

    /**
     * Field _dsTipoClassificacaoRetorno
     */
    private java.lang.String _dsTipoClassificacaoRetorno;

    /**
     * Field _cdTipoOrdemRegistroRetorno
     */
    private int _cdTipoOrdemRegistroRetorno = 0;

    /**
     * keeps track of state for field: _cdTipoOrdemRegistroRetorno
     */
    private boolean _has_cdTipoOrdemRegistroRetorno;

    /**
     * Field _dsTipoOrdemRegistroRetorno
     */
    private java.lang.String _dsTipoOrdemRegistroRetorno;

    /**
     * Field _cdUsuarioInclusao
     */
    private java.lang.String _cdUsuarioInclusao;

    /**
     * Field _cdUsuarioInclusaoExter
     */
    private java.lang.String _cdUsuarioInclusaoExter;

    /**
     * Field _hrInclusaoRegistro
     */
    private java.lang.String _hrInclusaoRegistro;

    /**
     * Field _cdOperacaoCanalInclusao
     */
    private java.lang.String _cdOperacaoCanalInclusao;

    /**
     * Field _cdTipoCanalInclusao
     */
    private int _cdTipoCanalInclusao = 0;

    /**
     * keeps track of state for field: _cdTipoCanalInclusao
     */
    private boolean _has_cdTipoCanalInclusao;

    /**
     * Field _dsTipoCanalInclusao
     */
    private java.lang.String _dsTipoCanalInclusao;

    /**
     * Field _cdUsuarioManutencao
     */
    private java.lang.String _cdUsuarioManutencao;

    /**
     * Field _cdUsuarioManutencaoExter
     */
    private java.lang.String _cdUsuarioManutencaoExter;

    /**
     * Field _hrManutencaoRegistro
     */
    private java.lang.String _hrManutencaoRegistro;

    /**
     * Field _cdOperacaoCanalManutencao
     */
    private java.lang.String _cdOperacaoCanalManutencao;

    /**
     * Field _cdTipoCanalManutencao
     */
    private int _cdTipoCanalManutencao = 0;

    /**
     * keeps track of state for field: _cdTipoCanalManutencao
     */
    private boolean _has_cdTipoCanalManutencao;

    /**
     * Field _dsTipoCanalManutencao
     */
    private java.lang.String _dsTipoCanalManutencao;


      //----------------/
     //- Constructors -/
    //----------------/

    public DetalharTipoRetornoLayoutResponse() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalhartiporetornolayout.response.DetalharTipoRetornoLayoutResponse()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdTipoCanalInclusao
     * 
     */
    public void deleteCdTipoCanalInclusao()
    {
        this._has_cdTipoCanalInclusao= false;
    } //-- void deleteCdTipoCanalInclusao() 

    /**
     * Method deleteCdTipoCanalManutencao
     * 
     */
    public void deleteCdTipoCanalManutencao()
    {
        this._has_cdTipoCanalManutencao= false;
    } //-- void deleteCdTipoCanalManutencao() 

    /**
     * Method deleteCdTipoClassificacaoRetorno
     * 
     */
    public void deleteCdTipoClassificacaoRetorno()
    {
        this._has_cdTipoClassificacaoRetorno= false;
    } //-- void deleteCdTipoClassificacaoRetorno() 

    /**
     * Method deleteCdTipoGeracaoRetorno
     * 
     */
    public void deleteCdTipoGeracaoRetorno()
    {
        this._has_cdTipoGeracaoRetorno= false;
    } //-- void deleteCdTipoGeracaoRetorno() 

    /**
     * Method deleteCdTipoMontaRetorno
     * 
     */
    public void deleteCdTipoMontaRetorno()
    {
        this._has_cdTipoMontaRetorno= false;
    } //-- void deleteCdTipoMontaRetorno() 

    /**
     * Method deleteCdTipoOcorrenciaRetorno
     * 
     */
    public void deleteCdTipoOcorrenciaRetorno()
    {
        this._has_cdTipoOcorrenciaRetorno= false;
    } //-- void deleteCdTipoOcorrenciaRetorno() 

    /**
     * Method deleteCdTipoOrdemRegistroRetorno
     * 
     */
    public void deleteCdTipoOrdemRegistroRetorno()
    {
        this._has_cdTipoOrdemRegistroRetorno= false;
    } //-- void deleteCdTipoOrdemRegistroRetorno() 

    /**
     * Returns the value of field 'cdOperacaoCanalInclusao'.
     * 
     * @return String
     * @return the value of field 'cdOperacaoCanalInclusao'.
     */
    public java.lang.String getCdOperacaoCanalInclusao()
    {
        return this._cdOperacaoCanalInclusao;
    } //-- java.lang.String getCdOperacaoCanalInclusao() 

    /**
     * Returns the value of field 'cdOperacaoCanalManutencao'.
     * 
     * @return String
     * @return the value of field 'cdOperacaoCanalManutencao'.
     */
    public java.lang.String getCdOperacaoCanalManutencao()
    {
        return this._cdOperacaoCanalManutencao;
    } //-- java.lang.String getCdOperacaoCanalManutencao() 

    /**
     * Returns the value of field 'cdTipoCanalInclusao'.
     * 
     * @return int
     * @return the value of field 'cdTipoCanalInclusao'.
     */
    public int getCdTipoCanalInclusao()
    {
        return this._cdTipoCanalInclusao;
    } //-- int getCdTipoCanalInclusao() 

    /**
     * Returns the value of field 'cdTipoCanalManutencao'.
     * 
     * @return int
     * @return the value of field 'cdTipoCanalManutencao'.
     */
    public int getCdTipoCanalManutencao()
    {
        return this._cdTipoCanalManutencao;
    } //-- int getCdTipoCanalManutencao() 

    /**
     * Returns the value of field 'cdTipoClassificacaoRetorno'.
     * 
     * @return int
     * @return the value of field 'cdTipoClassificacaoRetorno'.
     */
    public int getCdTipoClassificacaoRetorno()
    {
        return this._cdTipoClassificacaoRetorno;
    } //-- int getCdTipoClassificacaoRetorno() 

    /**
     * Returns the value of field 'cdTipoGeracaoRetorno'.
     * 
     * @return int
     * @return the value of field 'cdTipoGeracaoRetorno'.
     */
    public int getCdTipoGeracaoRetorno()
    {
        return this._cdTipoGeracaoRetorno;
    } //-- int getCdTipoGeracaoRetorno() 

    /**
     * Returns the value of field 'cdTipoMontaRetorno'.
     * 
     * @return int
     * @return the value of field 'cdTipoMontaRetorno'.
     */
    public int getCdTipoMontaRetorno()
    {
        return this._cdTipoMontaRetorno;
    } //-- int getCdTipoMontaRetorno() 

    /**
     * Returns the value of field 'cdTipoOcorrenciaRetorno'.
     * 
     * @return int
     * @return the value of field 'cdTipoOcorrenciaRetorno'.
     */
    public int getCdTipoOcorrenciaRetorno()
    {
        return this._cdTipoOcorrenciaRetorno;
    } //-- int getCdTipoOcorrenciaRetorno() 

    /**
     * Returns the value of field 'cdTipoOrdemRegistroRetorno'.
     * 
     * @return int
     * @return the value of field 'cdTipoOrdemRegistroRetorno'.
     */
    public int getCdTipoOrdemRegistroRetorno()
    {
        return this._cdTipoOrdemRegistroRetorno;
    } //-- int getCdTipoOrdemRegistroRetorno() 

    /**
     * Returns the value of field 'cdUsuarioInclusao'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioInclusao'.
     */
    public java.lang.String getCdUsuarioInclusao()
    {
        return this._cdUsuarioInclusao;
    } //-- java.lang.String getCdUsuarioInclusao() 

    /**
     * Returns the value of field 'cdUsuarioInclusaoExter'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioInclusaoExter'.
     */
    public java.lang.String getCdUsuarioInclusaoExter()
    {
        return this._cdUsuarioInclusaoExter;
    } //-- java.lang.String getCdUsuarioInclusaoExter() 

    /**
     * Returns the value of field 'cdUsuarioManutencao'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioManutencao'.
     */
    public java.lang.String getCdUsuarioManutencao()
    {
        return this._cdUsuarioManutencao;
    } //-- java.lang.String getCdUsuarioManutencao() 

    /**
     * Returns the value of field 'cdUsuarioManutencaoExter'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioManutencaoExter'.
     */
    public java.lang.String getCdUsuarioManutencaoExter()
    {
        return this._cdUsuarioManutencaoExter;
    } //-- java.lang.String getCdUsuarioManutencaoExter() 

    /**
     * Returns the value of field 'codMensagem'.
     * 
     * @return String
     * @return the value of field 'codMensagem'.
     */
    public java.lang.String getCodMensagem()
    {
        return this._codMensagem;
    } //-- java.lang.String getCodMensagem() 

    /**
     * Returns the value of field 'dsTipoCanalInclusao'.
     * 
     * @return String
     * @return the value of field 'dsTipoCanalInclusao'.
     */
    public java.lang.String getDsTipoCanalInclusao()
    {
        return this._dsTipoCanalInclusao;
    } //-- java.lang.String getDsTipoCanalInclusao() 

    /**
     * Returns the value of field 'dsTipoCanalManutencao'.
     * 
     * @return String
     * @return the value of field 'dsTipoCanalManutencao'.
     */
    public java.lang.String getDsTipoCanalManutencao()
    {
        return this._dsTipoCanalManutencao;
    } //-- java.lang.String getDsTipoCanalManutencao() 

    /**
     * Returns the value of field 'dsTipoClassificacaoRetorno'.
     * 
     * @return String
     * @return the value of field 'dsTipoClassificacaoRetorno'.
     */
    public java.lang.String getDsTipoClassificacaoRetorno()
    {
        return this._dsTipoClassificacaoRetorno;
    } //-- java.lang.String getDsTipoClassificacaoRetorno() 

    /**
     * Returns the value of field 'dsTipoGeracaoRetorno'.
     * 
     * @return String
     * @return the value of field 'dsTipoGeracaoRetorno'.
     */
    public java.lang.String getDsTipoGeracaoRetorno()
    {
        return this._dsTipoGeracaoRetorno;
    } //-- java.lang.String getDsTipoGeracaoRetorno() 

    /**
     * Returns the value of field 'dsTipoMontaRetorno'.
     * 
     * @return String
     * @return the value of field 'dsTipoMontaRetorno'.
     */
    public java.lang.String getDsTipoMontaRetorno()
    {
        return this._dsTipoMontaRetorno;
    } //-- java.lang.String getDsTipoMontaRetorno() 

    /**
     * Returns the value of field 'dsTipoOcorrenciaRetorno'.
     * 
     * @return String
     * @return the value of field 'dsTipoOcorrenciaRetorno'.
     */
    public java.lang.String getDsTipoOcorrenciaRetorno()
    {
        return this._dsTipoOcorrenciaRetorno;
    } //-- java.lang.String getDsTipoOcorrenciaRetorno() 

    /**
     * Returns the value of field 'dsTipoOrdemRegistroRetorno'.
     * 
     * @return String
     * @return the value of field 'dsTipoOrdemRegistroRetorno'.
     */
    public java.lang.String getDsTipoOrdemRegistroRetorno()
    {
        return this._dsTipoOrdemRegistroRetorno;
    } //-- java.lang.String getDsTipoOrdemRegistroRetorno() 

    /**
     * Returns the value of field 'hrInclusaoRegistro'.
     * 
     * @return String
     * @return the value of field 'hrInclusaoRegistro'.
     */
    public java.lang.String getHrInclusaoRegistro()
    {
        return this._hrInclusaoRegistro;
    } //-- java.lang.String getHrInclusaoRegistro() 

    /**
     * Returns the value of field 'hrManutencaoRegistro'.
     * 
     * @return String
     * @return the value of field 'hrManutencaoRegistro'.
     */
    public java.lang.String getHrManutencaoRegistro()
    {
        return this._hrManutencaoRegistro;
    } //-- java.lang.String getHrManutencaoRegistro() 

    /**
     * Returns the value of field 'mensagem'.
     * 
     * @return String
     * @return the value of field 'mensagem'.
     */
    public java.lang.String getMensagem()
    {
        return this._mensagem;
    } //-- java.lang.String getMensagem() 

    /**
     * Method hasCdTipoCanalInclusao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoCanalInclusao()
    {
        return this._has_cdTipoCanalInclusao;
    } //-- boolean hasCdTipoCanalInclusao() 

    /**
     * Method hasCdTipoCanalManutencao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoCanalManutencao()
    {
        return this._has_cdTipoCanalManutencao;
    } //-- boolean hasCdTipoCanalManutencao() 

    /**
     * Method hasCdTipoClassificacaoRetorno
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoClassificacaoRetorno()
    {
        return this._has_cdTipoClassificacaoRetorno;
    } //-- boolean hasCdTipoClassificacaoRetorno() 

    /**
     * Method hasCdTipoGeracaoRetorno
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoGeracaoRetorno()
    {
        return this._has_cdTipoGeracaoRetorno;
    } //-- boolean hasCdTipoGeracaoRetorno() 

    /**
     * Method hasCdTipoMontaRetorno
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoMontaRetorno()
    {
        return this._has_cdTipoMontaRetorno;
    } //-- boolean hasCdTipoMontaRetorno() 

    /**
     * Method hasCdTipoOcorrenciaRetorno
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoOcorrenciaRetorno()
    {
        return this._has_cdTipoOcorrenciaRetorno;
    } //-- boolean hasCdTipoOcorrenciaRetorno() 

    /**
     * Method hasCdTipoOrdemRegistroRetorno
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoOrdemRegistroRetorno()
    {
        return this._has_cdTipoOrdemRegistroRetorno;
    } //-- boolean hasCdTipoOrdemRegistroRetorno() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdOperacaoCanalInclusao'.
     * 
     * @param cdOperacaoCanalInclusao the value of field
     * 'cdOperacaoCanalInclusao'.
     */
    public void setCdOperacaoCanalInclusao(java.lang.String cdOperacaoCanalInclusao)
    {
        this._cdOperacaoCanalInclusao = cdOperacaoCanalInclusao;
    } //-- void setCdOperacaoCanalInclusao(java.lang.String) 

    /**
     * Sets the value of field 'cdOperacaoCanalManutencao'.
     * 
     * @param cdOperacaoCanalManutencao the value of field
     * 'cdOperacaoCanalManutencao'.
     */
    public void setCdOperacaoCanalManutencao(java.lang.String cdOperacaoCanalManutencao)
    {
        this._cdOperacaoCanalManutencao = cdOperacaoCanalManutencao;
    } //-- void setCdOperacaoCanalManutencao(java.lang.String) 

    /**
     * Sets the value of field 'cdTipoCanalInclusao'.
     * 
     * @param cdTipoCanalInclusao the value of field
     * 'cdTipoCanalInclusao'.
     */
    public void setCdTipoCanalInclusao(int cdTipoCanalInclusao)
    {
        this._cdTipoCanalInclusao = cdTipoCanalInclusao;
        this._has_cdTipoCanalInclusao = true;
    } //-- void setCdTipoCanalInclusao(int) 

    /**
     * Sets the value of field 'cdTipoCanalManutencao'.
     * 
     * @param cdTipoCanalManutencao the value of field
     * 'cdTipoCanalManutencao'.
     */
    public void setCdTipoCanalManutencao(int cdTipoCanalManutencao)
    {
        this._cdTipoCanalManutencao = cdTipoCanalManutencao;
        this._has_cdTipoCanalManutencao = true;
    } //-- void setCdTipoCanalManutencao(int) 

    /**
     * Sets the value of field 'cdTipoClassificacaoRetorno'.
     * 
     * @param cdTipoClassificacaoRetorno the value of field
     * 'cdTipoClassificacaoRetorno'.
     */
    public void setCdTipoClassificacaoRetorno(int cdTipoClassificacaoRetorno)
    {
        this._cdTipoClassificacaoRetorno = cdTipoClassificacaoRetorno;
        this._has_cdTipoClassificacaoRetorno = true;
    } //-- void setCdTipoClassificacaoRetorno(int) 

    /**
     * Sets the value of field 'cdTipoGeracaoRetorno'.
     * 
     * @param cdTipoGeracaoRetorno the value of field
     * 'cdTipoGeracaoRetorno'.
     */
    public void setCdTipoGeracaoRetorno(int cdTipoGeracaoRetorno)
    {
        this._cdTipoGeracaoRetorno = cdTipoGeracaoRetorno;
        this._has_cdTipoGeracaoRetorno = true;
    } //-- void setCdTipoGeracaoRetorno(int) 

    /**
     * Sets the value of field 'cdTipoMontaRetorno'.
     * 
     * @param cdTipoMontaRetorno the value of field
     * 'cdTipoMontaRetorno'.
     */
    public void setCdTipoMontaRetorno(int cdTipoMontaRetorno)
    {
        this._cdTipoMontaRetorno = cdTipoMontaRetorno;
        this._has_cdTipoMontaRetorno = true;
    } //-- void setCdTipoMontaRetorno(int) 

    /**
     * Sets the value of field 'cdTipoOcorrenciaRetorno'.
     * 
     * @param cdTipoOcorrenciaRetorno the value of field
     * 'cdTipoOcorrenciaRetorno'.
     */
    public void setCdTipoOcorrenciaRetorno(int cdTipoOcorrenciaRetorno)
    {
        this._cdTipoOcorrenciaRetorno = cdTipoOcorrenciaRetorno;
        this._has_cdTipoOcorrenciaRetorno = true;
    } //-- void setCdTipoOcorrenciaRetorno(int) 

    /**
     * Sets the value of field 'cdTipoOrdemRegistroRetorno'.
     * 
     * @param cdTipoOrdemRegistroRetorno the value of field
     * 'cdTipoOrdemRegistroRetorno'.
     */
    public void setCdTipoOrdemRegistroRetorno(int cdTipoOrdemRegistroRetorno)
    {
        this._cdTipoOrdemRegistroRetorno = cdTipoOrdemRegistroRetorno;
        this._has_cdTipoOrdemRegistroRetorno = true;
    } //-- void setCdTipoOrdemRegistroRetorno(int) 

    /**
     * Sets the value of field 'cdUsuarioInclusao'.
     * 
     * @param cdUsuarioInclusao the value of field
     * 'cdUsuarioInclusao'.
     */
    public void setCdUsuarioInclusao(java.lang.String cdUsuarioInclusao)
    {
        this._cdUsuarioInclusao = cdUsuarioInclusao;
    } //-- void setCdUsuarioInclusao(java.lang.String) 

    /**
     * Sets the value of field 'cdUsuarioInclusaoExter'.
     * 
     * @param cdUsuarioInclusaoExter the value of field
     * 'cdUsuarioInclusaoExter'.
     */
    public void setCdUsuarioInclusaoExter(java.lang.String cdUsuarioInclusaoExter)
    {
        this._cdUsuarioInclusaoExter = cdUsuarioInclusaoExter;
    } //-- void setCdUsuarioInclusaoExter(java.lang.String) 

    /**
     * Sets the value of field 'cdUsuarioManutencao'.
     * 
     * @param cdUsuarioManutencao the value of field
     * 'cdUsuarioManutencao'.
     */
    public void setCdUsuarioManutencao(java.lang.String cdUsuarioManutencao)
    {
        this._cdUsuarioManutencao = cdUsuarioManutencao;
    } //-- void setCdUsuarioManutencao(java.lang.String) 

    /**
     * Sets the value of field 'cdUsuarioManutencaoExter'.
     * 
     * @param cdUsuarioManutencaoExter the value of field
     * 'cdUsuarioManutencaoExter'.
     */
    public void setCdUsuarioManutencaoExter(java.lang.String cdUsuarioManutencaoExter)
    {
        this._cdUsuarioManutencaoExter = cdUsuarioManutencaoExter;
    } //-- void setCdUsuarioManutencaoExter(java.lang.String) 

    /**
     * Sets the value of field 'codMensagem'.
     * 
     * @param codMensagem the value of field 'codMensagem'.
     */
    public void setCodMensagem(java.lang.String codMensagem)
    {
        this._codMensagem = codMensagem;
    } //-- void setCodMensagem(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoCanalInclusao'.
     * 
     * @param dsTipoCanalInclusao the value of field
     * 'dsTipoCanalInclusao'.
     */
    public void setDsTipoCanalInclusao(java.lang.String dsTipoCanalInclusao)
    {
        this._dsTipoCanalInclusao = dsTipoCanalInclusao;
    } //-- void setDsTipoCanalInclusao(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoCanalManutencao'.
     * 
     * @param dsTipoCanalManutencao the value of field
     * 'dsTipoCanalManutencao'.
     */
    public void setDsTipoCanalManutencao(java.lang.String dsTipoCanalManutencao)
    {
        this._dsTipoCanalManutencao = dsTipoCanalManutencao;
    } //-- void setDsTipoCanalManutencao(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoClassificacaoRetorno'.
     * 
     * @param dsTipoClassificacaoRetorno the value of field
     * 'dsTipoClassificacaoRetorno'.
     */
    public void setDsTipoClassificacaoRetorno(java.lang.String dsTipoClassificacaoRetorno)
    {
        this._dsTipoClassificacaoRetorno = dsTipoClassificacaoRetorno;
    } //-- void setDsTipoClassificacaoRetorno(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoGeracaoRetorno'.
     * 
     * @param dsTipoGeracaoRetorno the value of field
     * 'dsTipoGeracaoRetorno'.
     */
    public void setDsTipoGeracaoRetorno(java.lang.String dsTipoGeracaoRetorno)
    {
        this._dsTipoGeracaoRetorno = dsTipoGeracaoRetorno;
    } //-- void setDsTipoGeracaoRetorno(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoMontaRetorno'.
     * 
     * @param dsTipoMontaRetorno the value of field
     * 'dsTipoMontaRetorno'.
     */
    public void setDsTipoMontaRetorno(java.lang.String dsTipoMontaRetorno)
    {
        this._dsTipoMontaRetorno = dsTipoMontaRetorno;
    } //-- void setDsTipoMontaRetorno(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoOcorrenciaRetorno'.
     * 
     * @param dsTipoOcorrenciaRetorno the value of field
     * 'dsTipoOcorrenciaRetorno'.
     */
    public void setDsTipoOcorrenciaRetorno(java.lang.String dsTipoOcorrenciaRetorno)
    {
        this._dsTipoOcorrenciaRetorno = dsTipoOcorrenciaRetorno;
    } //-- void setDsTipoOcorrenciaRetorno(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoOrdemRegistroRetorno'.
     * 
     * @param dsTipoOrdemRegistroRetorno the value of field
     * 'dsTipoOrdemRegistroRetorno'.
     */
    public void setDsTipoOrdemRegistroRetorno(java.lang.String dsTipoOrdemRegistroRetorno)
    {
        this._dsTipoOrdemRegistroRetorno = dsTipoOrdemRegistroRetorno;
    } //-- void setDsTipoOrdemRegistroRetorno(java.lang.String) 

    /**
     * Sets the value of field 'hrInclusaoRegistro'.
     * 
     * @param hrInclusaoRegistro the value of field
     * 'hrInclusaoRegistro'.
     */
    public void setHrInclusaoRegistro(java.lang.String hrInclusaoRegistro)
    {
        this._hrInclusaoRegistro = hrInclusaoRegistro;
    } //-- void setHrInclusaoRegistro(java.lang.String) 

    /**
     * Sets the value of field 'hrManutencaoRegistro'.
     * 
     * @param hrManutencaoRegistro the value of field
     * 'hrManutencaoRegistro'.
     */
    public void setHrManutencaoRegistro(java.lang.String hrManutencaoRegistro)
    {
        this._hrManutencaoRegistro = hrManutencaoRegistro;
    } //-- void setHrManutencaoRegistro(java.lang.String) 

    /**
     * Sets the value of field 'mensagem'.
     * 
     * @param mensagem the value of field 'mensagem'.
     */
    public void setMensagem(java.lang.String mensagem)
    {
        this._mensagem = mensagem;
    } //-- void setMensagem(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return DetalharTipoRetornoLayoutResponse
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.detalhartiporetornolayout.response.DetalharTipoRetornoLayoutResponse unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.detalhartiporetornolayout.response.DetalharTipoRetornoLayoutResponse) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.detalhartiporetornolayout.response.DetalharTipoRetornoLayoutResponse.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalhartiporetornolayout.response.DetalharTipoRetornoLayoutResponse unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
