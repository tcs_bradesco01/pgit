/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.autdeslistadebitoparcial.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdPessoaJuridicaLista
     */
    private long _cdPessoaJuridicaLista = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridicaLista
     */
    private boolean _has_cdPessoaJuridicaLista;

    /**
     * Field _cdTipoContratoLista
     */
    private int _cdTipoContratoLista = 0;

    /**
     * keeps track of state for field: _cdTipoContratoLista
     */
    private boolean _has_cdTipoContratoLista;

    /**
     * Field _nrSequenciaContratoLista
     */
    private long _nrSequenciaContratoLista = 0;

    /**
     * keeps track of state for field: _nrSequenciaContratoLista
     */
    private boolean _has_nrSequenciaContratoLista;

    /**
     * Field _cdTipoCanalLista
     */
    private int _cdTipoCanalLista = 0;

    /**
     * keeps track of state for field: _cdTipoCanalLista
     */
    private boolean _has_cdTipoCanalLista;

    /**
     * Field _cdListaDebitoPagamento
     */
    private long _cdListaDebitoPagamento = 0;

    /**
     * keeps track of state for field: _cdListaDebitoPagamento
     */
    private boolean _has_cdListaDebitoPagamento;

    /**
     * Field _cdPessoaJuridicaContrato
     */
    private long _cdPessoaJuridicaContrato = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridicaContrato
     */
    private boolean _has_cdPessoaJuridicaContrato;

    /**
     * Field _cdTipoContratoNegocio
     */
    private int _cdTipoContratoNegocio = 0;

    /**
     * keeps track of state for field: _cdTipoContratoNegocio
     */
    private boolean _has_cdTipoContratoNegocio;

    /**
     * Field _nrSequenciaContratoNegocio
     */
    private long _nrSequenciaContratoNegocio = 0;

    /**
     * keeps track of state for field: _nrSequenciaContratoNegocio
     */
    private boolean _has_nrSequenciaContratoNegocio;

    /**
     * Field _cdTipoCanal
     */
    private int _cdTipoCanal = 0;

    /**
     * keeps track of state for field: _cdTipoCanal
     */
    private boolean _has_cdTipoCanal;

    /**
     * Field _cdControlePagamento
     */
    private java.lang.String _cdControlePagamento;

    /**
     * Field _vlPagamento
     */
    private java.math.BigDecimal _vlPagamento = new java.math.BigDecimal("0");


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
        setVlPagamento(new java.math.BigDecimal("0"));
    } //-- br.com.bradesco.web.pgit.service.data.pdc.autdeslistadebitoparcial.request.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdListaDebitoPagamento
     * 
     */
    public void deleteCdListaDebitoPagamento()
    {
        this._has_cdListaDebitoPagamento= false;
    } //-- void deleteCdListaDebitoPagamento() 

    /**
     * Method deleteCdPessoaJuridicaContrato
     * 
     */
    public void deleteCdPessoaJuridicaContrato()
    {
        this._has_cdPessoaJuridicaContrato= false;
    } //-- void deleteCdPessoaJuridicaContrato() 

    /**
     * Method deleteCdPessoaJuridicaLista
     * 
     */
    public void deleteCdPessoaJuridicaLista()
    {
        this._has_cdPessoaJuridicaLista= false;
    } //-- void deleteCdPessoaJuridicaLista() 

    /**
     * Method deleteCdTipoCanal
     * 
     */
    public void deleteCdTipoCanal()
    {
        this._has_cdTipoCanal= false;
    } //-- void deleteCdTipoCanal() 

    /**
     * Method deleteCdTipoCanalLista
     * 
     */
    public void deleteCdTipoCanalLista()
    {
        this._has_cdTipoCanalLista= false;
    } //-- void deleteCdTipoCanalLista() 

    /**
     * Method deleteCdTipoContratoLista
     * 
     */
    public void deleteCdTipoContratoLista()
    {
        this._has_cdTipoContratoLista= false;
    } //-- void deleteCdTipoContratoLista() 

    /**
     * Method deleteCdTipoContratoNegocio
     * 
     */
    public void deleteCdTipoContratoNegocio()
    {
        this._has_cdTipoContratoNegocio= false;
    } //-- void deleteCdTipoContratoNegocio() 

    /**
     * Method deleteNrSequenciaContratoLista
     * 
     */
    public void deleteNrSequenciaContratoLista()
    {
        this._has_nrSequenciaContratoLista= false;
    } //-- void deleteNrSequenciaContratoLista() 

    /**
     * Method deleteNrSequenciaContratoNegocio
     * 
     */
    public void deleteNrSequenciaContratoNegocio()
    {
        this._has_nrSequenciaContratoNegocio= false;
    } //-- void deleteNrSequenciaContratoNegocio() 

    /**
     * Returns the value of field 'cdControlePagamento'.
     * 
     * @return String
     * @return the value of field 'cdControlePagamento'.
     */
    public java.lang.String getCdControlePagamento()
    {
        return this._cdControlePagamento;
    } //-- java.lang.String getCdControlePagamento() 

    /**
     * Returns the value of field 'cdListaDebitoPagamento'.
     * 
     * @return long
     * @return the value of field 'cdListaDebitoPagamento'.
     */
    public long getCdListaDebitoPagamento()
    {
        return this._cdListaDebitoPagamento;
    } //-- long getCdListaDebitoPagamento() 

    /**
     * Returns the value of field 'cdPessoaJuridicaContrato'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridicaContrato'.
     */
    public long getCdPessoaJuridicaContrato()
    {
        return this._cdPessoaJuridicaContrato;
    } //-- long getCdPessoaJuridicaContrato() 

    /**
     * Returns the value of field 'cdPessoaJuridicaLista'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridicaLista'.
     */
    public long getCdPessoaJuridicaLista()
    {
        return this._cdPessoaJuridicaLista;
    } //-- long getCdPessoaJuridicaLista() 

    /**
     * Returns the value of field 'cdTipoCanal'.
     * 
     * @return int
     * @return the value of field 'cdTipoCanal'.
     */
    public int getCdTipoCanal()
    {
        return this._cdTipoCanal;
    } //-- int getCdTipoCanal() 

    /**
     * Returns the value of field 'cdTipoCanalLista'.
     * 
     * @return int
     * @return the value of field 'cdTipoCanalLista'.
     */
    public int getCdTipoCanalLista()
    {
        return this._cdTipoCanalLista;
    } //-- int getCdTipoCanalLista() 

    /**
     * Returns the value of field 'cdTipoContratoLista'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoLista'.
     */
    public int getCdTipoContratoLista()
    {
        return this._cdTipoContratoLista;
    } //-- int getCdTipoContratoLista() 

    /**
     * Returns the value of field 'cdTipoContratoNegocio'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoNegocio'.
     */
    public int getCdTipoContratoNegocio()
    {
        return this._cdTipoContratoNegocio;
    } //-- int getCdTipoContratoNegocio() 

    /**
     * Returns the value of field 'nrSequenciaContratoLista'.
     * 
     * @return long
     * @return the value of field 'nrSequenciaContratoLista'.
     */
    public long getNrSequenciaContratoLista()
    {
        return this._nrSequenciaContratoLista;
    } //-- long getNrSequenciaContratoLista() 

    /**
     * Returns the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @return long
     * @return the value of field 'nrSequenciaContratoNegocio'.
     */
    public long getNrSequenciaContratoNegocio()
    {
        return this._nrSequenciaContratoNegocio;
    } //-- long getNrSequenciaContratoNegocio() 

    /**
     * Returns the value of field 'vlPagamento'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlPagamento'.
     */
    public java.math.BigDecimal getVlPagamento()
    {
        return this._vlPagamento;
    } //-- java.math.BigDecimal getVlPagamento() 

    /**
     * Method hasCdListaDebitoPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdListaDebitoPagamento()
    {
        return this._has_cdListaDebitoPagamento;
    } //-- boolean hasCdListaDebitoPagamento() 

    /**
     * Method hasCdPessoaJuridicaContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridicaContrato()
    {
        return this._has_cdPessoaJuridicaContrato;
    } //-- boolean hasCdPessoaJuridicaContrato() 

    /**
     * Method hasCdPessoaJuridicaLista
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridicaLista()
    {
        return this._has_cdPessoaJuridicaLista;
    } //-- boolean hasCdPessoaJuridicaLista() 

    /**
     * Method hasCdTipoCanal
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoCanal()
    {
        return this._has_cdTipoCanal;
    } //-- boolean hasCdTipoCanal() 

    /**
     * Method hasCdTipoCanalLista
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoCanalLista()
    {
        return this._has_cdTipoCanalLista;
    } //-- boolean hasCdTipoCanalLista() 

    /**
     * Method hasCdTipoContratoLista
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoLista()
    {
        return this._has_cdTipoContratoLista;
    } //-- boolean hasCdTipoContratoLista() 

    /**
     * Method hasCdTipoContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoNegocio()
    {
        return this._has_cdTipoContratoNegocio;
    } //-- boolean hasCdTipoContratoNegocio() 

    /**
     * Method hasNrSequenciaContratoLista
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaContratoLista()
    {
        return this._has_nrSequenciaContratoLista;
    } //-- boolean hasNrSequenciaContratoLista() 

    /**
     * Method hasNrSequenciaContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaContratoNegocio()
    {
        return this._has_nrSequenciaContratoNegocio;
    } //-- boolean hasNrSequenciaContratoNegocio() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdControlePagamento'.
     * 
     * @param cdControlePagamento the value of field
     * 'cdControlePagamento'.
     */
    public void setCdControlePagamento(java.lang.String cdControlePagamento)
    {
        this._cdControlePagamento = cdControlePagamento;
    } //-- void setCdControlePagamento(java.lang.String) 

    /**
     * Sets the value of field 'cdListaDebitoPagamento'.
     * 
     * @param cdListaDebitoPagamento the value of field
     * 'cdListaDebitoPagamento'.
     */
    public void setCdListaDebitoPagamento(long cdListaDebitoPagamento)
    {
        this._cdListaDebitoPagamento = cdListaDebitoPagamento;
        this._has_cdListaDebitoPagamento = true;
    } //-- void setCdListaDebitoPagamento(long) 

    /**
     * Sets the value of field 'cdPessoaJuridicaContrato'.
     * 
     * @param cdPessoaJuridicaContrato the value of field
     * 'cdPessoaJuridicaContrato'.
     */
    public void setCdPessoaJuridicaContrato(long cdPessoaJuridicaContrato)
    {
        this._cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
        this._has_cdPessoaJuridicaContrato = true;
    } //-- void setCdPessoaJuridicaContrato(long) 

    /**
     * Sets the value of field 'cdPessoaJuridicaLista'.
     * 
     * @param cdPessoaJuridicaLista the value of field
     * 'cdPessoaJuridicaLista'.
     */
    public void setCdPessoaJuridicaLista(long cdPessoaJuridicaLista)
    {
        this._cdPessoaJuridicaLista = cdPessoaJuridicaLista;
        this._has_cdPessoaJuridicaLista = true;
    } //-- void setCdPessoaJuridicaLista(long) 

    /**
     * Sets the value of field 'cdTipoCanal'.
     * 
     * @param cdTipoCanal the value of field 'cdTipoCanal'.
     */
    public void setCdTipoCanal(int cdTipoCanal)
    {
        this._cdTipoCanal = cdTipoCanal;
        this._has_cdTipoCanal = true;
    } //-- void setCdTipoCanal(int) 

    /**
     * Sets the value of field 'cdTipoCanalLista'.
     * 
     * @param cdTipoCanalLista the value of field 'cdTipoCanalLista'
     */
    public void setCdTipoCanalLista(int cdTipoCanalLista)
    {
        this._cdTipoCanalLista = cdTipoCanalLista;
        this._has_cdTipoCanalLista = true;
    } //-- void setCdTipoCanalLista(int) 

    /**
     * Sets the value of field 'cdTipoContratoLista'.
     * 
     * @param cdTipoContratoLista the value of field
     * 'cdTipoContratoLista'.
     */
    public void setCdTipoContratoLista(int cdTipoContratoLista)
    {
        this._cdTipoContratoLista = cdTipoContratoLista;
        this._has_cdTipoContratoLista = true;
    } //-- void setCdTipoContratoLista(int) 

    /**
     * Sets the value of field 'cdTipoContratoNegocio'.
     * 
     * @param cdTipoContratoNegocio the value of field
     * 'cdTipoContratoNegocio'.
     */
    public void setCdTipoContratoNegocio(int cdTipoContratoNegocio)
    {
        this._cdTipoContratoNegocio = cdTipoContratoNegocio;
        this._has_cdTipoContratoNegocio = true;
    } //-- void setCdTipoContratoNegocio(int) 

    /**
     * Sets the value of field 'nrSequenciaContratoLista'.
     * 
     * @param nrSequenciaContratoLista the value of field
     * 'nrSequenciaContratoLista'.
     */
    public void setNrSequenciaContratoLista(long nrSequenciaContratoLista)
    {
        this._nrSequenciaContratoLista = nrSequenciaContratoLista;
        this._has_nrSequenciaContratoLista = true;
    } //-- void setNrSequenciaContratoLista(long) 

    /**
     * Sets the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @param nrSequenciaContratoNegocio the value of field
     * 'nrSequenciaContratoNegocio'.
     */
    public void setNrSequenciaContratoNegocio(long nrSequenciaContratoNegocio)
    {
        this._nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
        this._has_nrSequenciaContratoNegocio = true;
    } //-- void setNrSequenciaContratoNegocio(long) 

    /**
     * Sets the value of field 'vlPagamento'.
     * 
     * @param vlPagamento the value of field 'vlPagamento'.
     */
    public void setVlPagamento(java.math.BigDecimal vlPagamento)
    {
        this._vlPagamento = vlPagamento;
    } //-- void setVlPagamento(java.math.BigDecimal) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.autdeslistadebitoparcial.request.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.autdeslistadebitoparcial.request.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.autdeslistadebitoparcial.request.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.autdeslistadebitoparcial.request.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
