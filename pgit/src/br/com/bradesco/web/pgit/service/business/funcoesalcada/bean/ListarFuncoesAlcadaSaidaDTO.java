/*
 * Nome: br.com.bradesco.web.pgit.service.business.funcoesalcada.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.funcoesalcada.bean;

/**
 * Nome: ListarFuncoesAlcadaSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarFuncoesAlcadaSaidaDTO {
	
	/** Atributo codMensagem. */
	private String codMensagem;
	
	/** Atributo mensagem. */
	private String mensagem;	
	
	/** Atributo centroCusto. */
	private String centroCusto;
	
	/** Atributo codigoAplicacao. */
	private String codigoAplicacao;
	
	/** Atributo cdAcao. */
	private int cdAcao;
	
	/** Atributo dsAcao. */
	private String dsAcao;
	
	/** Atributo indicadorAcesso. */
	private int indicadorAcesso;
	
	/** Atributo dsTipoServico. */
	private String dsTipoServico;
	
	/** Atributo dsModalidadeServico. */
	private String dsModalidadeServico;
	
	/** Atributo cdRegra. */
	private int cdRegra;
	
	/**
	 * Get: cdAcao.
	 *
	 * @return cdAcao
	 */
	public int getCdAcao() {
		return cdAcao;
	}
	
	/**
	 * Set: cdAcao.
	 *
	 * @param cdAcao the cd acao
	 */
	public void setCdAcao(int cdAcao) {
		this.cdAcao = cdAcao;
	}
	
	/**
	 * Get: centroCusto.
	 *
	 * @return centroCusto
	 */
	public String getCentroCusto() {
		return centroCusto;
	}
	
	/**
	 * Set: centroCusto.
	 *
	 * @param centroCusto the centro custo
	 */
	public void setCentroCusto(String centroCusto) {
		this.centroCusto = centroCusto;
	}
	
	/**
	 * Get: codigoAplicacao.
	 *
	 * @return codigoAplicacao
	 */
	public String getCodigoAplicacao() {
		return codigoAplicacao;
	}
	
	/**
	 * Set: codigoAplicacao.
	 *
	 * @param codigoAplicacao the codigo aplicacao
	 */
	public void setCodigoAplicacao(String codigoAplicacao) {
		this.codigoAplicacao = codigoAplicacao;
	}
	
	/**
	 * Get: dsAcao.
	 *
	 * @return dsAcao
	 */
	public String getDsAcao() {
		return dsAcao;
	}
	
	/**
	 * Set: dsAcao.
	 *
	 * @param dsAcao the ds acao
	 */
	public void setDsAcao(String dsAcao) {
		this.dsAcao = dsAcao;
	}
	
	/**
	 * Get: indicadorAcesso.
	 *
	 * @return indicadorAcesso
	 */
	public int getIndicadorAcesso() {
		return indicadorAcesso;
	}
	
	/**
	 * Set: indicadorAcesso.
	 *
	 * @param indicadorAcesso the indicador acesso
	 */
	public void setIndicadorAcesso(int indicadorAcesso) {
		this.indicadorAcesso = indicadorAcesso;
	}
	
	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}
	
	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	/**
	 * Get: cdRegra.
	 *
	 * @return cdRegra
	 */
	public int getCdRegra() {
		return cdRegra;
	}
	
	/**
	 * Set: cdRegra.
	 *
	 * @param cdTipoRegra the cd regra
	 */
	public void setCdRegra(int cdTipoRegra) {
		this.cdRegra = cdTipoRegra;
	}
	
	/**
	 * Get: dsModalidadeServico.
	 *
	 * @return dsModalidadeServico
	 */
	public String getDsModalidadeServico() {
		return dsModalidadeServico;
	}
	
	/**
	 * Set: dsModalidadeServico.
	 *
	 * @param dsModalidadeServico the ds modalidade servico
	 */
	public void setDsModalidadeServico(String dsModalidadeServico) {
		this.dsModalidadeServico = dsModalidadeServico;
	}
	
	/**
	 * Get: dsTipoServico.
	 *
	 * @return dsTipoServico
	 */
	public String getDsTipoServico() {
		return dsTipoServico;
	}
	
	/**
	 * Set: dsTipoServico.
	 *
	 * @param dsTipoServico the ds tipo servico
	 */
	public void setDsTipoServico(String dsTipoServico) {
		this.dsTipoServico = dsTipoServico;
	}
	
	
}
