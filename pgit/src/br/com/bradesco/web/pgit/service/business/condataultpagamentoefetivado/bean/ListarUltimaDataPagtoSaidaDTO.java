/*
 * Nome: br.com.bradesco.web.pgit.service.business.condataultpagamentoefetivado.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.condataultpagamentoefetivado.bean;

/**
 * Nome: ListarUltimaDataPagtoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarUltimaDataPagtoSaidaDTO {
	
	/** Atributo codMensagem. */
	private String codMensagem;
    
    /** Atributo mensagem. */
    private String mensagem;    
    
    /** Atributo cdPessoaJuridicaContrato. */
    private Long cdPessoaJuridicaContrato;
    
    /** Atributo cdTipoContratoNegocio. */
    private Integer cdTipoContratoNegocio;
    
    /** Atributo nrSequenciaContratoNegocio. */
    private Long nrSequenciaContratoNegocio;
    
    /** Atributo cdCpfCnpj. */
    private String cdCpfCnpj;
    
    /** Atributo dsRazaoSocial. */
    private String dsRazaoSocial;
    
    /** Atributo dtUltimoPagamento. */
    private String dtUltimoPagamento;
    
    /** Atributo hrUltimoPagamento. */
    private String hrUltimoPagamento;
    
    /** Atributo cdProdutoOperacao. */
    private Integer cdProdutoOperacao;
    
    /** Atributo dsProdutoOperacao. */
    private String dsProdutoOperacao;
    
    /** Atributo cdProdutoRelacionado. */
    private Integer cdProdutoRelacionado;
    
    /** Atributo dsProdutoRelacionado. */
    private String dsProdutoRelacionado;
    
    /** Atributo dataHoraFormatada. */
    private String dataHoraFormatada;
    
    
	/**
	 * Get: dataHoraFormatada.
	 *
	 * @return dataHoraFormatada
	 */
	public String getDataHoraFormatada() {
		return dataHoraFormatada;
	}
	
	/**
	 * Set: dataHoraFormatada.
	 *
	 * @param dataHoraFormatada the data hora formatada
	 */
	public void setDataHoraFormatada(String dataHoraFormatada) {
		this.dataHoraFormatada = dataHoraFormatada;
	}
	
	/**
	 * Get: cdCpfCnpj.
	 *
	 * @return cdCpfCnpj
	 */
	public String getCdCpfCnpj() {
		return cdCpfCnpj;
	}
	
	/**
	 * Set: cdCpfCnpj.
	 *
	 * @param cdCpfCnpj the cd cpf cnpj
	 */
	public void setCdCpfCnpj(String cdCpfCnpj) {
		this.cdCpfCnpj = cdCpfCnpj;
	}
	
	/**
	 * Get: cdPessoaJuridicaContrato.
	 *
	 * @return cdPessoaJuridicaContrato
	 */
	public Long getCdPessoaJuridicaContrato() {
		return cdPessoaJuridicaContrato;
	}
	
	/**
	 * Set: cdPessoaJuridicaContrato.
	 *
	 * @param cdPessoaJuridicaContrato the cd pessoa juridica contrato
	 */
	public void setCdPessoaJuridicaContrato(Long cdPessoaJuridicaContrato) {
		this.cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
	}
	
	/**
	 * Get: cdProdutoOperacao.
	 *
	 * @return cdProdutoOperacao
	 */
	public Integer getCdProdutoOperacao() {
		return cdProdutoOperacao;
	}
	
	/**
	 * Set: cdProdutoOperacao.
	 *
	 * @param cdProdutoOperacao the cd produto operacao
	 */
	public void setCdProdutoOperacao(Integer cdProdutoOperacao) {
		this.cdProdutoOperacao = cdProdutoOperacao;
	}
	
	/**
	 * Get: cdProdutoRelacionado.
	 *
	 * @return cdProdutoRelacionado
	 */
	public Integer getCdProdutoRelacionado() {
		return cdProdutoRelacionado;
	}
	
	/**
	 * Set: cdProdutoRelacionado.
	 *
	 * @param cdProdutoRelacionado the cd produto relacionado
	 */
	public void setCdProdutoRelacionado(Integer cdProdutoRelacionado) {
		this.cdProdutoRelacionado = cdProdutoRelacionado;
	}
	
	/**
	 * Get: cdTipoContratoNegocio.
	 *
	 * @return cdTipoContratoNegocio
	 */
	public Integer getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}
	
	/**
	 * Set: cdTipoContratoNegocio.
	 *
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}
	
	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}
	
	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}
	
	/**
	 * Get: dsProdutoOperacao.
	 *
	 * @return dsProdutoOperacao
	 */
	public String getDsProdutoOperacao() {
		return dsProdutoOperacao;
	}
	
	/**
	 * Set: dsProdutoOperacao.
	 *
	 * @param dsProdutoOperacao the ds produto operacao
	 */
	public void setDsProdutoOperacao(String dsProdutoOperacao) {
		this.dsProdutoOperacao = dsProdutoOperacao;
	}
	
	/**
	 * Get: dsProdutoRelacionado.
	 *
	 * @return dsProdutoRelacionado
	 */
	public String getDsProdutoRelacionado() {
		return dsProdutoRelacionado;
	}
	
	/**
	 * Set: dsProdutoRelacionado.
	 *
	 * @param dsProdutoRelacionado the ds produto relacionado
	 */
	public void setDsProdutoRelacionado(String dsProdutoRelacionado) {
		this.dsProdutoRelacionado = dsProdutoRelacionado;
	}
	
	/**
	 * Get: dsRazaoSocial.
	 *
	 * @return dsRazaoSocial
	 */
	public String getDsRazaoSocial() {
		return dsRazaoSocial;
	}
	
	/**
	 * Set: dsRazaoSocial.
	 *
	 * @param dsRazaoSocial the ds razao social
	 */
	public void setDsRazaoSocial(String dsRazaoSocial) {
		this.dsRazaoSocial = dsRazaoSocial;
	}
	
	/**
	 * Get: dtUltimoPagamento.
	 *
	 * @return dtUltimoPagamento
	 */
	public String getDtUltimoPagamento() {
		return dtUltimoPagamento;
	}
	
	/**
	 * Set: dtUltimoPagamento.
	 *
	 * @param dtUltimoPagamento the dt ultimo pagamento
	 */
	public void setDtUltimoPagamento(String dtUltimoPagamento) {
		this.dtUltimoPagamento = dtUltimoPagamento;
	}
	
	/**
	 * Get: hrUltimoPagamento.
	 *
	 * @return hrUltimoPagamento
	 */
	public String getHrUltimoPagamento() {
		return hrUltimoPagamento;
	}
	
	/**
	 * Set: hrUltimoPagamento.
	 *
	 * @param hrUltimoPagamento the hr ultimo pagamento
	 */
	public void setHrUltimoPagamento(String hrUltimoPagamento) {
		this.hrUltimoPagamento = hrUltimoPagamento;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	/**
	 * Get: nrSequenciaContratoNegocio.
	 *
	 * @return nrSequenciaContratoNegocio
	 */
	public Long getNrSequenciaContratoNegocio() {
		return nrSequenciaContratoNegocio;
	}
	
	/**
	 * Set: nrSequenciaContratoNegocio.
	 *
	 * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
	 */
	public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}
    
    
    
}
