/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantercontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantercontrato.bean;

/**
 * Nome: AlterarLayoutServicoListEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class AlterarLayoutServicoListEntradaDTO {
	
	
	/** Atributo cdTipoServico. */
	private Integer cdTipoServico;
	
	/** Atributo cdRelacionamentoProdutoProduto. */
	private Integer cdRelacionamentoProdutoProduto;
	
	/** Atributo cdTipoLayout. */
	private Integer cdTipoLayout;
	
	/** Atributo cdTipoLayoutNovo. */
	private Integer cdTipoLayoutNovo;
	
	
	/**
	 * Get: cdRelacionamentoProdutoProduto.
	 *
	 * @return cdRelacionamentoProdutoProduto
	 */
	public Integer getCdRelacionamentoProdutoProduto() {
		return cdRelacionamentoProdutoProduto;
	}
	
	/**
	 * Set: cdRelacionamentoProdutoProduto.
	 *
	 * @param cdRelacionamentoProdutoProduto the cd relacionamento produto produto
	 */
	public void setCdRelacionamentoProdutoProduto(
			Integer cdRelacionamentoProdutoProduto) {
		this.cdRelacionamentoProdutoProduto = cdRelacionamentoProdutoProduto;
	}
	
	/**
	 * Get: cdTipoLayout.
	 *
	 * @return cdTipoLayout
	 */
	public Integer getCdTipoLayout() {
		return cdTipoLayout;
	}
	
	/**
	 * Set: cdTipoLayout.
	 *
	 * @param cdTipoLayout the cd tipo layout
	 */
	public void setCdTipoLayout(Integer cdTipoLayout) {
		this.cdTipoLayout = cdTipoLayout;
	}
	
	/**
	 * Get: cdTipoLayoutNovo.
	 *
	 * @return cdTipoLayoutNovo
	 */
	public Integer getCdTipoLayoutNovo() {
		return cdTipoLayoutNovo;
	}
	
	/**
	 * Set: cdTipoLayoutNovo.
	 *
	 * @param cdTipoLayoutNovo the cd tipo layout novo
	 */
	public void setCdTipoLayoutNovo(Integer cdTipoLayoutNovo) {
		this.cdTipoLayoutNovo = cdTipoLayoutNovo;
	}
	
	/**
	 * Get: cdTipoServico.
	 *
	 * @return cdTipoServico
	 */
	public Integer getCdTipoServico() {
		return cdTipoServico;
	}
	
	/**
	 * Set: cdTipoServico.
	 *
	 * @param cdTipoServico the cd tipo servico
	 */
	public void setCdTipoServico(Integer cdTipoServico) {
		this.cdTipoServico = cdTipoServico;
	}
	
	
	
	

}
