/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/interfaz.ftl,v $
 * $Id: interfaz.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: interfaz.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.relatoriofavorecido;

import java.util.List;

import br.com.bradesco.web.pgit.service.business.cadtipopendencia.bean.ConsultarDescricaoUnidOrganizacionalEntradaDTO;
import br.com.bradesco.web.pgit.service.business.cadtipopendencia.bean.ConsultarDescricaoUnidOrganizacionalSaidaDTO;
import br.com.bradesco.web.pgit.service.business.relatoriofavorecido.bean.ConRelFavorecidoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.relatoriofavorecido.bean.ConRelFavorecidoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.relatoriofavorecido.bean.DetRelFavorecidoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.relatoriofavorecido.bean.DetRelFavorecidoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.relatoriofavorecido.bean.ExcRelFavorecidoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.relatoriofavorecido.bean.IncRelFavorecidoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.relatoriofavorecido.bean.IncRelFavorecidoSaidaDTO;
/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Interface do adaptador: RelatorioFavorecido
 * </p>
 * 
 * @comment CODIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public interface IRelatorioFavorecidoService {
	
	/**
	 * Pesquisa rel favorecido.
	 *
	 * @param entrada the entrada
	 * @return the list< con rel favorecido saida dt o>
	 */
	List <ConRelFavorecidoSaidaDTO> pesquisaRelFavorecido(ConRelFavorecidoEntradaDTO entrada);
	
	/**
	 * Pesquisar detalhe rel favorecido.
	 *
	 * @param entrada the entrada
	 * @return the det rel favorecido saida dto
	 */
	DetRelFavorecidoSaidaDTO pesquisarDetalheRelFavorecido(DetRelFavorecidoEntradaDTO entrada);
	
	/**
	 * Excluir motivo bloqueio favorecido.
	 *
	 * @param numSolicitacao the num solicitacao
	 * @return the exc rel favorecido saida dto
	 */
	ExcRelFavorecidoSaidaDTO excluirMotivoBloqueioFavorecido(Integer numSolicitacao);
	
	/**
	 * Incluir motivo bloqueio favorecido.
	 *
	 * @param inclusao the inclusao
	 * @return the inc rel favorecido saida dto
	 */
	IncRelFavorecidoSaidaDTO incluirMotivoBloqueioFavorecido(IncRelFavorecidoEntradaDTO inclusao);
	
	/**
	 * Descricao unidade organizacional.
	 *
	 * @param descricaoUnidOrgEntradaDTO the descricao unid org entrada dto
	 * @return the consultar descricao unid organizacional saida dto
	 */
	ConsultarDescricaoUnidOrganizacionalSaidaDTO descricaoUnidadeOrganizacional(ConsultarDescricaoUnidOrganizacionalEntradaDTO descricaoUnidOrgEntradaDTO);
	
}

