/*
 * Nome: br.com.bradesco.web.pgit.service.business.combo.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.combo.bean;

import java.io.Serializable;

/**
 * Nome: ListarDependenciaDiretoriaEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarDependenciaDiretoriaEntradaDTO implements Serializable {
	
	/** Atributo serialVersionUID. */
	private static final long serialVersionUID = -2995237772025257574L;
	
	/** Atributo nrOcorrencias. */
	private Integer nrOcorrencias;
    
    /** Atributo cdPessoa. */
    private Long cdPessoa;
    
    /** Atributo nrSeqUnidadeOrganizacional. */
    private Integer nrSeqUnidadeOrganizacional;
    
    /** Atributo cdTipoPesquisa. */
    private Integer cdTipoPesquisa;
    
    /**
     * Listar dependencia diretoria entrada dto.
     */
    public ListarDependenciaDiretoriaEntradaDTO() {
    	
    }

	/**
	 * Listar dependencia diretoria entrada dto.
	 *
	 * @param nrOcorrencias the nr ocorrencias
	 * @param cdPessoa the cd pessoa
	 * @param nrSeqUnidadeOrganizacional the nr seq unidade organizacional
	 * @param cdTipoPesquisa the cd tipo pesquisa
	 */
	public ListarDependenciaDiretoriaEntradaDTO(Integer nrOcorrencias, Long cdPessoa, Integer nrSeqUnidadeOrganizacional, Integer cdTipoPesquisa) {
		super();
		this.nrOcorrencias = nrOcorrencias;
		this.cdPessoa = cdPessoa;
		this.nrSeqUnidadeOrganizacional = nrSeqUnidadeOrganizacional;
		this.cdTipoPesquisa = cdTipoPesquisa;
	}

	/**
	 * Get: cdPessoa.
	 *
	 * @return cdPessoa
	 */
	public Long getCdPessoa() {
		return cdPessoa;
	}

	/**
	 * Set: cdPessoa.
	 *
	 * @param cdPessoa the cd pessoa
	 */
	public void setCdPessoa(Long cdPessoa) {
		this.cdPessoa = cdPessoa;
	}

	/**
	 * Get: cdTipoPesquisa.
	 *
	 * @return cdTipoPesquisa
	 */
	public Integer getCdTipoPesquisa() {
		return cdTipoPesquisa;
	}

	/**
	 * Set: cdTipoPesquisa.
	 *
	 * @param cdTipoPesquisa the cd tipo pesquisa
	 */
	public void setCdTipoPesquisa(Integer cdTipoPesquisa) {
		this.cdTipoPesquisa = cdTipoPesquisa;
	}

	/**
	 * Get: nrOcorrencias.
	 *
	 * @return nrOcorrencias
	 */
	public Integer getNrOcorrencias() {
		return nrOcorrencias;
	}

	/**
	 * Set: nrOcorrencias.
	 *
	 * @param nrOcorrencias the nr ocorrencias
	 */
	public void setNrOcorrencias(Integer nrOcorrencias) {
		this.nrOcorrencias = nrOcorrencias;
	}

	/**
	 * Get: nrSeqUnidadeOrganizacional.
	 *
	 * @return nrSeqUnidadeOrganizacional
	 */
	public Integer getNrSeqUnidadeOrganizacional() {
		return nrSeqUnidadeOrganizacional;
	}

	/**
	 * Set: nrSeqUnidadeOrganizacional.
	 *
	 * @param nrSeqUnidadeOrganizacional the nr seq unidade organizacional
	 */
	public void setNrSeqUnidadeOrganizacional(Integer nrSeqUnidadeOrganizacional) {
		this.nrSeqUnidadeOrganizacional = nrSeqUnidadeOrganizacional;
	}
    
}
