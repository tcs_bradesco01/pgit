/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.efetuarvalidacaoconfigatribmodalidade.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.math.BigDecimal;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class EfetuarValidacaoConfigAtribModalidadeRequest.
 * 
 * @version $Revision$ $Date$
 */
public class EfetuarValidacaoConfigAtribModalidadeRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdServico
     */
    private int _cdServico = 0;

    /**
     * keeps track of state for field: _cdServico
     */
    private boolean _has_cdServico;

    /**
     * Field _cdModalidade
     */
    private int _cdModalidade = 0;

    /**
     * keeps track of state for field: _cdModalidade
     */
    private boolean _has_cdModalidade;

    /**
     * Field _cdIndicadorModalidade
     */
    private int _cdIndicadorModalidade = 0;

    /**
     * keeps track of state for field: _cdIndicadorModalidade
     */
    private boolean _has_cdIndicadorModalidade;

    /**
     * Field _cdTipoSaldo
     */
    private int _cdTipoSaldo = 0;

    /**
     * keeps track of state for field: _cdTipoSaldo
     */
    private boolean _has_cdTipoSaldo;

    /**
     * Field _cdTipoProcessamento
     */
    private int _cdTipoProcessamento = 0;

    /**
     * keeps track of state for field: _cdTipoProcessamento
     */
    private boolean _has_cdTipoProcessamento;

    /**
     * Field _cdTratamentoFeriadoDataPagto
     */
    private int _cdTratamentoFeriadoDataPagto = 0;

    /**
     * keeps track of state for field: _cdTratamentoFeriadoDataPagto
     */
    private boolean _has_cdTratamentoFeriadoDataPagto;

    /**
     * Field _cdPermissaoFavorecidoConsultaPagto
     */
    private int _cdPermissaoFavorecidoConsultaPagto = 0;

    /**
     * keeps track of state for field:
     * _cdPermissaoFavorecidoConsultaPagto
     */
    private boolean _has_cdPermissaoFavorecidoConsultaPagto;

    /**
     * Field _cdTipoRejeicaoAgendamento
     */
    private int _cdTipoRejeicaoAgendamento = 0;

    /**
     * keeps track of state for field: _cdTipoRejeicaoAgendamento
     */
    private boolean _has_cdTipoRejeicaoAgendamento;

    /**
     * Field _cdTipoRejeicaoEfetivacao
     */
    private int _cdTipoRejeicaoEfetivacao = 0;

    /**
     * keeps track of state for field: _cdTipoRejeicaoEfetivacao
     */
    private boolean _has_cdTipoRejeicaoEfetivacao;

    /**
     * Field _qtdeMaxRegInconsistenteLote
     */
    private int _qtdeMaxRegInconsistenteLote = 0;

    /**
     * keeps track of state for field: _qtdeMaxRegInconsistenteLote
     */
    private boolean _has_qtdeMaxRegInconsistenteLote;

    /**
     * Field _percentualMaxRegInconsistenteLote
     */
    private int _percentualMaxRegInconsistenteLote = 0;

    /**
     * keeps track of state for field:
     * _percentualMaxRegInconsistenteLote
     */
    private boolean _has_percentualMaxRegInconsistenteLote;

    /**
     * Field _cdPrioridadeDebito
     */
    private int _cdPrioridadeDebito = 0;

    /**
     * keeps track of state for field: _cdPrioridadeDebito
     */
    private boolean _has_cdPrioridadeDebito;

    /**
     * Field _cdGeracaoLancamentoFuturoDebito
     */
    private int _cdGeracaoLancamentoFuturoDebito = 0;

    /**
     * keeps track of state for field:
     * _cdGeracaoLancamentoFuturoDebito
     */
    private boolean _has_cdGeracaoLancamentoFuturoDebito;

    /**
     * Field _cdUtilizacaoFavorecidoControlePagto
     */
    private int _cdUtilizacaoFavorecidoControlePagto = 0;

    /**
     * keeps track of state for field:
     * _cdUtilizacaoFavorecidoControlePagto
     */
    private boolean _has_cdUtilizacaoFavorecidoControlePagto;

    /**
     * Field _vlrMaximoPagtoFavorecidoNaoCadastrado
     */
    private java.math.BigDecimal _vlrMaximoPagtoFavorecidoNaoCadastrado = new java.math.BigDecimal("0");

    /**
     * Field _vlrLimiteIndividual
     */
    private java.math.BigDecimal _vlrLimiteIndividual = new java.math.BigDecimal("0");

    /**
     * Field _vlrLimiteDiario
     */
    private java.math.BigDecimal _vlrLimiteDiario = new java.math.BigDecimal("0");

    /**
     * Field _qtdeDiaRepique
     */
    private int _qtdeDiaRepique = 0;

    /**
     * keeps track of state for field: _qtdeDiaRepique
     */
    private boolean _has_qtdeDiaRepique;

    /**
     * Field _qtdeDiaFloating
     */
    private int _qtdeDiaFloating = 0;

    /**
     * keeps track of state for field: _qtdeDiaFloating
     */
    private boolean _has_qtdeDiaFloating;

    /**
     * Field _cdTipoInscricaoFavorecidoAceita
     */
    private int _cdTipoInscricaoFavorecidoAceita = 0;

    /**
     * keeps track of state for field:
     * _cdTipoInscricaoFavorecidoAceita
     */
    private boolean _has_cdTipoInscricaoFavorecidoAceita;

    /**
     * Field _cdOcorrenciaDiaExpiracao
     */
    private int _cdOcorrenciaDiaExpiracao = 0;

    /**
     * keeps track of state for field: _cdOcorrenciaDiaExpiracao
     */
    private boolean _has_cdOcorrenciaDiaExpiracao;

    /**
     * Field _qtdeDiaExpiracao
     */
    private int _qtdeDiaExpiracao = 0;

    /**
     * keeps track of state for field: _qtdeDiaExpiracao
     */
    private boolean _has_qtdeDiaExpiracao;

    /**
     * Field _cdGeracaoLancamentoFuturoCredito
     */
    private int _cdGeracaoLancamentoFuturoCredito = 0;

    /**
     * keeps track of state for field:
     * _cdGeracaoLancamentoFuturoCredito
     */
    private boolean _has_cdGeracaoLancamentoFuturoCredito;

    /**
     * Field _cdGeracaoLancamentoProgramado
     */
    private int _cdGeracaoLancamentoProgramado = 0;

    /**
     * keeps track of state for field: _cdGeracaoLancamentoProgramad
     */
    private boolean _has_cdGeracaoLancamentoProgramado;

    /**
     * Field _cdTratoContaTransf
     */
    private int _cdTratoContaTransf = 0;

    /**
     * keeps track of state for field: _cdTratoContaTransf
     */
    private boolean _has_cdTratoContaTransf;

    /**
     * Field _cdConsistenciaCpfCnpj
     */
    private int _cdConsistenciaCpfCnpj = 0;

    /**
     * keeps track of state for field: _cdConsistenciaCpfCnpj
     */
    private boolean _has_cdConsistenciaCpfCnpj;

    /**
     * Field _cdTipoConsistenciaCnpjFavorecido
     */
    private int _cdTipoConsistenciaCnpjFavorecido = 0;

    /**
     * keeps track of state for field:
     * _cdTipoConsistenciaCnpjFavorecido
     */
    private boolean _has_cdTipoConsistenciaCnpjFavorecido;

    /**
     * Field _cdTipoContaCredito
     */
    private long _cdTipoContaCredito = 0;

    /**
     * keeps track of state for field: _cdTipoContaCredito
     */
    private boolean _has_cdTipoContaCredito;

    /**
     * Field _cdEfetuaConsistenciaEspBeneficiario
     */
    private int _cdEfetuaConsistenciaEspBeneficiario = 0;

    /**
     * keeps track of state for field:
     * _cdEfetuaConsistenciaEspBeneficiario
     */
    private boolean _has_cdEfetuaConsistenciaEspBeneficiario;

    /**
     * Field _cdUtilizacaoCodigoLancamentoFixo
     */
    private int _cdUtilizacaoCodigoLancamentoFixo = 0;

    /**
     * keeps track of state for field:
     * _cdUtilizacaoCodigoLancamentoFixo
     */
    private boolean _has_cdUtilizacaoCodigoLancamentoFixo;

    /**
     * Field _cdLancamentoFixo
     */
    private int _cdLancamentoFixo = 0;

    /**
     * keeps track of state for field: _cdLancamentoFixo
     */
    private boolean _has_cdLancamentoFixo;

    /**
     * Field _cdPermissaoPagtoVencido
     */
    private int _cdPermissaoPagtoVencido = 0;

    /**
     * keeps track of state for field: _cdPermissaoPagtoVencido
     */
    private boolean _has_cdPermissaoPagtoVencido;

    /**
     * Field _qtdeLimiteDiarioPagtoVencido
     */
    private int _qtdeLimiteDiarioPagtoVencido = 0;

    /**
     * keeps track of state for field: _qtdeLimiteDiarioPagtoVencido
     */
    private boolean _has_qtdeLimiteDiarioPagtoVencido;

    /**
     * Field _cdPermissaoPagtoMenor
     */
    private int _cdPermissaoPagtoMenor = 0;

    /**
     * keeps track of state for field: _cdPermissaoPagtoMenor
     */
    private boolean _has_cdPermissaoPagtoMenor;

    /**
     * Field _cdTipoConsultaSaldo
     */
    private int _cdTipoConsultaSaldo = 0;

    /**
     * keeps track of state for field: _cdTipoConsultaSaldo
     */
    private boolean _has_cdTipoConsultaSaldo;

    /**
     * Field _cdTratamentoFeriado
     */
    private int _cdTratamentoFeriado = 0;

    /**
     * keeps track of state for field: _cdTratamentoFeriado
     */
    private boolean _has_cdTratamentoFeriado;

    /**
     * Field _cdPermissaoContingencia
     */
    private int _cdPermissaoContingencia = 0;

    /**
     * keeps track of state for field: _cdPermissaoContingencia
     */
    private boolean _has_cdPermissaoContingencia;

    /**
     * Field _cdTratamentoValorDivergente
     */
    private int _cdTratamentoValorDivergente = 0;

    /**
     * keeps track of state for field: _cdTratamentoValorDivergente
     */
    private boolean _has_cdTratamentoValorDivergente;

    /**
     * Field _cdTipoConsistenciaCnpjProposta
     */
    private int _cdTipoConsistenciaCnpjProposta = 0;

    /**
     * keeps track of state for field:
     * _cdTipoConsistenciaCnpjProposta
     */
    private boolean _has_cdTipoConsistenciaCnpjProposta;

    /**
     * Field _cdTipoComprovacaoVida
     */
    private int _cdTipoComprovacaoVida = 0;

    /**
     * keeps track of state for field: _cdTipoComprovacaoVida
     */
    private boolean _has_cdTipoComprovacaoVida;

    /**
     * Field _qtdeMesesComprovacaoVida
     */
    private int _qtdeMesesComprovacaoVida = 0;

    /**
     * keeps track of state for field: _qtdeMesesComprovacaoVida
     */
    private boolean _has_qtdeMesesComprovacaoVida;

    /**
     * Field _cdIndicadorEmissaoComprovanteVida
     */
    private int _cdIndicadorEmissaoComprovanteVida = 0;

    /**
     * keeps track of state for field:
     * _cdIndicadorEmissaoComprovanteVida
     */
    private boolean _has_cdIndicadorEmissaoComprovanteVida;

    /**
     * Field _qtdeDiaAvisoComprovanteVidaInicial
     */
    private int _qtdeDiaAvisoComprovanteVidaInicial = 0;

    /**
     * keeps track of state for field:
     * _qtdeDiaAvisoComprovanteVidaInicial
     */
    private boolean _has_qtdeDiaAvisoComprovanteVidaInicial;

    /**
     * Field _qtdeDiaAvisoComprovanteVidaVencimento
     */
    private int _qtdeDiaAvisoComprovanteVidaVencimento = 0;

    /**
     * keeps track of state for field:
     * _qtdeDiaAvisoComprovanteVidaVencimento
     */
    private boolean _has_qtdeDiaAvisoComprovanteVidaVencimento;

    /**
     * Field _cdUtilizacaoMensagemPersonalizada
     */
    private int _cdUtilizacaoMensagemPersonalizada = 0;

    /**
     * keeps track of state for field:
     * _cdUtilizacaoMensagemPersonalizada
     */
    private boolean _has_cdUtilizacaoMensagemPersonalizada;

    /**
     * Field _cdDestino
     */
    private int _cdDestino = 0;

    /**
     * keeps track of state for field: _cdDestino
     */
    private boolean _has_cdDestino;

    /**
     * Field _cdCadastroEnderecoUtilizado
     */
    private int _cdCadastroEnderecoUtilizado = 0;

    /**
     * keeps track of state for field: _cdCadastroEnderecoUtilizado
     */
    private boolean _has_cdCadastroEnderecoUtilizado;

    /**
     * Field _cdutilizacaoFormularioPersonalizado
     */
    private int _cdutilizacaoFormularioPersonalizado = 0;

    /**
     * keeps track of state for field:
     * _cdutilizacaoFormularioPersonalizado
     */
    private boolean _has_cdutilizacaoFormularioPersonalizado;

    /**
     * Field _cdFormulario
     */
    private long _cdFormulario = 0;

    /**
     * keeps track of state for field: _cdFormulario
     */
    private boolean _has_cdFormulario;

    /**
     * Field _cdCriterioRstrbTitulo
     */
    private int _cdCriterioRstrbTitulo = 0;

    /**
     * keeps track of state for field: _cdCriterioRstrbTitulo
     */
    private boolean _has_cdCriterioRstrbTitulo;

    /**
     * Field _cdRastreabTituloTerc
     */
    private int _cdRastreabTituloTerc = 0;

    /**
     * keeps track of state for field: _cdRastreabTituloTerc
     */
    private boolean _has_cdRastreabTituloTerc;

    /**
     * Field _cdRastreabNotaFiscal
     */
    private int _cdRastreabNotaFiscal = 0;

    /**
     * keeps track of state for field: _cdRastreabNotaFiscal
     */
    private boolean _has_cdRastreabNotaFiscal;

    /**
     * Field _cdCaptuTituloRegistro
     */
    private int _cdCaptuTituloRegistro = 0;

    /**
     * keeps track of state for field: _cdCaptuTituloRegistro
     */
    private boolean _has_cdCaptuTituloRegistro;

    /**
     * Field _cdIndicadorAgendaTitulo
     */
    private int _cdIndicadorAgendaTitulo = 0;

    /**
     * keeps track of state for field: _cdIndicadorAgendaTitulo
     */
    private boolean _has_cdIndicadorAgendaTitulo;

    /**
     * Field _cdAgendaRastreabilidadeFilial
     */
    private int _cdAgendaRastreabilidadeFilial = 0;

    /**
     * keeps track of state for field: _cdAgendaRastreabilidadeFilia
     */
    private boolean _has_cdAgendaRastreabilidadeFilial;

    /**
     * Field _cdBloqueioEmissaoPplta
     */
    private int _cdBloqueioEmissaoPplta = 0;

    /**
     * keeps track of state for field: _cdBloqueioEmissaoPplta
     */
    private boolean _has_cdBloqueioEmissaoPplta;

    /**
     * Field _dtInicioRastreabTitulo
     */
    private java.lang.String _dtInicioRastreabTitulo;

    /**
     * Field _dtInicioBloqueioPplta
     */
    private java.lang.String _dtInicioBloqueioPplta;

    /**
     * Field _dtInicioRegTitulo
     */
    private java.lang.String _dtInicioRegTitulo;

    /**
     * Field _cdMomentoAvisoRecadastramento
     */
    private int _cdMomentoAvisoRecadastramento = 0;

    /**
     * keeps track of state for field: _cdMomentoAvisoRecadastrament
     */
    private boolean _has_cdMomentoAvisoRecadastramento;

    /**
     * Field _qtdeAntecipacaoInicioRecadastramento
     */
    private int _qtdeAntecipacaoInicioRecadastramento = 0;

    /**
     * keeps track of state for field:
     * _qtdeAntecipacaoInicioRecadastramento
     */
    private boolean _has_qtdeAntecipacaoInicioRecadastramento;

    /**
     * Field _cdAgruparCorrespondencia
     */
    private int _cdAgruparCorrespondencia = 0;

    /**
     * keeps track of state for field: _cdAgruparCorrespondencia
     */
    private boolean _has_cdAgruparCorrespondencia;

    /**
     * Field _cdAdesaoSacado
     */
    private int _cdAdesaoSacado = 0;

    /**
     * keeps track of state for field: _cdAdesaoSacado
     */
    private boolean _has_cdAdesaoSacado;

    /**
     * Field _cdValidacaoNomeFavorecido
     */
    private int _cdValidacaoNomeFavorecido = 0;

    /**
     * keeps track of state for field: _cdValidacaoNomeFavorecido
     */
    private boolean _has_cdValidacaoNomeFavorecido;

    /**
     * Field _cdPermissaoDebitoOnline
     */
    private int _cdPermissaoDebitoOnline = 0;

    /**
     * keeps track of state for field: _cdPermissaoDebitoOnline
     */
    private boolean _has_cdPermissaoDebitoOnline;

    /**
     * Field _cdLiberacaoLoteProcs
     */
    private int _cdLiberacaoLoteProcs = 0;

    /**
     * keeps track of state for field: _cdLiberacaoLoteProcs
     */
    private boolean _has_cdLiberacaoLoteProcs;

    /**
     * Field _cdTipoSaldoValorSuperior
     */
    private int _cdTipoSaldoValorSuperior = 0;

    /**
     * keeps track of state for field: _cdTipoSaldoValorSuperior
     */
    private boolean _has_cdTipoSaldoValorSuperior;


      //----------------/
     //- Constructors -/
    //----------------/

    public EfetuarValidacaoConfigAtribModalidadeRequest() 
     {
        super();
        setVlrMaximoPagtoFavorecidoNaoCadastrado(new java.math.BigDecimal("0"));
        setVlrLimiteIndividual(new java.math.BigDecimal("0"));
        setVlrLimiteDiario(new java.math.BigDecimal("0"));
    } //-- br.com.bradesco.web.pgit.service.data.pdc.efetuarvalidacaoconfigatribmodalidade.request.EfetuarValidacaoConfigAtribModalidadeRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdAdesaoSacado
     * 
     */
    public void deleteCdAdesaoSacado()
    {
        this._has_cdAdesaoSacado= false;
    } //-- void deleteCdAdesaoSacado() 

    /**
     * Method deleteCdAgendaRastreabilidadeFilial
     * 
     */
    public void deleteCdAgendaRastreabilidadeFilial()
    {
        this._has_cdAgendaRastreabilidadeFilial= false;
    } //-- void deleteCdAgendaRastreabilidadeFilial() 

    /**
     * Method deleteCdAgruparCorrespondencia
     * 
     */
    public void deleteCdAgruparCorrespondencia()
    {
        this._has_cdAgruparCorrespondencia= false;
    } //-- void deleteCdAgruparCorrespondencia() 

    /**
     * Method deleteCdBloqueioEmissaoPplta
     * 
     */
    public void deleteCdBloqueioEmissaoPplta()
    {
        this._has_cdBloqueioEmissaoPplta= false;
    } //-- void deleteCdBloqueioEmissaoPplta() 

    /**
     * Method deleteCdCadastroEnderecoUtilizado
     * 
     */
    public void deleteCdCadastroEnderecoUtilizado()
    {
        this._has_cdCadastroEnderecoUtilizado= false;
    } //-- void deleteCdCadastroEnderecoUtilizado() 

    /**
     * Method deleteCdCaptuTituloRegistro
     * 
     */
    public void deleteCdCaptuTituloRegistro()
    {
        this._has_cdCaptuTituloRegistro= false;
    } //-- void deleteCdCaptuTituloRegistro() 

    /**
     * Method deleteCdConsistenciaCpfCnpj
     * 
     */
    public void deleteCdConsistenciaCpfCnpj()
    {
        this._has_cdConsistenciaCpfCnpj= false;
    } //-- void deleteCdConsistenciaCpfCnpj() 

    /**
     * Method deleteCdCriterioRstrbTitulo
     * 
     */
    public void deleteCdCriterioRstrbTitulo()
    {
        this._has_cdCriterioRstrbTitulo= false;
    } //-- void deleteCdCriterioRstrbTitulo() 

    /**
     * Method deleteCdDestino
     * 
     */
    public void deleteCdDestino()
    {
        this._has_cdDestino= false;
    } //-- void deleteCdDestino() 

    /**
     * Method deleteCdEfetuaConsistenciaEspBeneficiario
     * 
     */
    public void deleteCdEfetuaConsistenciaEspBeneficiario()
    {
        this._has_cdEfetuaConsistenciaEspBeneficiario= false;
    } //-- void deleteCdEfetuaConsistenciaEspBeneficiario() 

    /**
     * Method deleteCdFormulario
     * 
     */
    public void deleteCdFormulario()
    {
        this._has_cdFormulario= false;
    } //-- void deleteCdFormulario() 

    /**
     * Method deleteCdGeracaoLancamentoFuturoCredito
     * 
     */
    public void deleteCdGeracaoLancamentoFuturoCredito()
    {
        this._has_cdGeracaoLancamentoFuturoCredito= false;
    } //-- void deleteCdGeracaoLancamentoFuturoCredito() 

    /**
     * Method deleteCdGeracaoLancamentoFuturoDebito
     * 
     */
    public void deleteCdGeracaoLancamentoFuturoDebito()
    {
        this._has_cdGeracaoLancamentoFuturoDebito= false;
    } //-- void deleteCdGeracaoLancamentoFuturoDebito() 

    /**
     * Method deleteCdGeracaoLancamentoProgramado
     * 
     */
    public void deleteCdGeracaoLancamentoProgramado()
    {
        this._has_cdGeracaoLancamentoProgramado= false;
    } //-- void deleteCdGeracaoLancamentoProgramado() 

    /**
     * Method deleteCdIndicadorAgendaTitulo
     * 
     */
    public void deleteCdIndicadorAgendaTitulo()
    {
        this._has_cdIndicadorAgendaTitulo= false;
    } //-- void deleteCdIndicadorAgendaTitulo() 

    /**
     * Method deleteCdIndicadorEmissaoComprovanteVida
     * 
     */
    public void deleteCdIndicadorEmissaoComprovanteVida()
    {
        this._has_cdIndicadorEmissaoComprovanteVida= false;
    } //-- void deleteCdIndicadorEmissaoComprovanteVida() 

    /**
     * Method deleteCdIndicadorModalidade
     * 
     */
    public void deleteCdIndicadorModalidade()
    {
        this._has_cdIndicadorModalidade= false;
    } //-- void deleteCdIndicadorModalidade() 

    /**
     * Method deleteCdLancamentoFixo
     * 
     */
    public void deleteCdLancamentoFixo()
    {
        this._has_cdLancamentoFixo= false;
    } //-- void deleteCdLancamentoFixo() 

    /**
     * Method deleteCdLiberacaoLoteProcs
     * 
     */
    public void deleteCdLiberacaoLoteProcs()
    {
        this._has_cdLiberacaoLoteProcs= false;
    } //-- void deleteCdLiberacaoLoteProcs() 

    /**
     * Method deleteCdModalidade
     * 
     */
    public void deleteCdModalidade()
    {
        this._has_cdModalidade= false;
    } //-- void deleteCdModalidade() 

    /**
     * Method deleteCdMomentoAvisoRecadastramento
     * 
     */
    public void deleteCdMomentoAvisoRecadastramento()
    {
        this._has_cdMomentoAvisoRecadastramento= false;
    } //-- void deleteCdMomentoAvisoRecadastramento() 

    /**
     * Method deleteCdOcorrenciaDiaExpiracao
     * 
     */
    public void deleteCdOcorrenciaDiaExpiracao()
    {
        this._has_cdOcorrenciaDiaExpiracao= false;
    } //-- void deleteCdOcorrenciaDiaExpiracao() 

    /**
     * Method deleteCdPermissaoContingencia
     * 
     */
    public void deleteCdPermissaoContingencia()
    {
        this._has_cdPermissaoContingencia= false;
    } //-- void deleteCdPermissaoContingencia() 

    /**
     * Method deleteCdPermissaoDebitoOnline
     * 
     */
    public void deleteCdPermissaoDebitoOnline()
    {
        this._has_cdPermissaoDebitoOnline= false;
    } //-- void deleteCdPermissaoDebitoOnline() 

    /**
     * Method deleteCdPermissaoFavorecidoConsultaPagto
     * 
     */
    public void deleteCdPermissaoFavorecidoConsultaPagto()
    {
        this._has_cdPermissaoFavorecidoConsultaPagto= false;
    } //-- void deleteCdPermissaoFavorecidoConsultaPagto() 

    /**
     * Method deleteCdPermissaoPagtoMenor
     * 
     */
    public void deleteCdPermissaoPagtoMenor()
    {
        this._has_cdPermissaoPagtoMenor= false;
    } //-- void deleteCdPermissaoPagtoMenor() 

    /**
     * Method deleteCdPermissaoPagtoVencido
     * 
     */
    public void deleteCdPermissaoPagtoVencido()
    {
        this._has_cdPermissaoPagtoVencido= false;
    } //-- void deleteCdPermissaoPagtoVencido() 

    /**
     * Method deleteCdPrioridadeDebito
     * 
     */
    public void deleteCdPrioridadeDebito()
    {
        this._has_cdPrioridadeDebito= false;
    } //-- void deleteCdPrioridadeDebito() 

    /**
     * Method deleteCdRastreabNotaFiscal
     * 
     */
    public void deleteCdRastreabNotaFiscal()
    {
        this._has_cdRastreabNotaFiscal= false;
    } //-- void deleteCdRastreabNotaFiscal() 

    /**
     * Method deleteCdRastreabTituloTerc
     * 
     */
    public void deleteCdRastreabTituloTerc()
    {
        this._has_cdRastreabTituloTerc= false;
    } //-- void deleteCdRastreabTituloTerc() 

    /**
     * Method deleteCdServico
     * 
     */
    public void deleteCdServico()
    {
        this._has_cdServico= false;
    } //-- void deleteCdServico() 

    /**
     * Method deleteCdTipoComprovacaoVida
     * 
     */
    public void deleteCdTipoComprovacaoVida()
    {
        this._has_cdTipoComprovacaoVida= false;
    } //-- void deleteCdTipoComprovacaoVida() 

    /**
     * Method deleteCdTipoConsistenciaCnpjFavorecido
     * 
     */
    public void deleteCdTipoConsistenciaCnpjFavorecido()
    {
        this._has_cdTipoConsistenciaCnpjFavorecido= false;
    } //-- void deleteCdTipoConsistenciaCnpjFavorecido() 

    /**
     * Method deleteCdTipoConsistenciaCnpjProposta
     * 
     */
    public void deleteCdTipoConsistenciaCnpjProposta()
    {
        this._has_cdTipoConsistenciaCnpjProposta= false;
    } //-- void deleteCdTipoConsistenciaCnpjProposta() 

    /**
     * Method deleteCdTipoConsultaSaldo
     * 
     */
    public void deleteCdTipoConsultaSaldo()
    {
        this._has_cdTipoConsultaSaldo= false;
    } //-- void deleteCdTipoConsultaSaldo() 

    /**
     * Method deleteCdTipoContaCredito
     * 
     */
    public void deleteCdTipoContaCredito()
    {
        this._has_cdTipoContaCredito= false;
    } //-- void deleteCdTipoContaCredito() 

    /**
     * Method deleteCdTipoInscricaoFavorecidoAceita
     * 
     */
    public void deleteCdTipoInscricaoFavorecidoAceita()
    {
        this._has_cdTipoInscricaoFavorecidoAceita= false;
    } //-- void deleteCdTipoInscricaoFavorecidoAceita() 

    /**
     * Method deleteCdTipoProcessamento
     * 
     */
    public void deleteCdTipoProcessamento()
    {
        this._has_cdTipoProcessamento= false;
    } //-- void deleteCdTipoProcessamento() 

    /**
     * Method deleteCdTipoRejeicaoAgendamento
     * 
     */
    public void deleteCdTipoRejeicaoAgendamento()
    {
        this._has_cdTipoRejeicaoAgendamento= false;
    } //-- void deleteCdTipoRejeicaoAgendamento() 

    /**
     * Method deleteCdTipoRejeicaoEfetivacao
     * 
     */
    public void deleteCdTipoRejeicaoEfetivacao()
    {
        this._has_cdTipoRejeicaoEfetivacao= false;
    } //-- void deleteCdTipoRejeicaoEfetivacao() 

    /**
     * Method deleteCdTipoSaldo
     * 
     */
    public void deleteCdTipoSaldo()
    {
        this._has_cdTipoSaldo= false;
    } //-- void deleteCdTipoSaldo() 

    /**
     * Method deleteCdTipoSaldoValorSuperior
     * 
     */
    public void deleteCdTipoSaldoValorSuperior()
    {
        this._has_cdTipoSaldoValorSuperior= false;
    } //-- void deleteCdTipoSaldoValorSuperior() 

    /**
     * Method deleteCdTratamentoFeriado
     * 
     */
    public void deleteCdTratamentoFeriado()
    {
        this._has_cdTratamentoFeriado= false;
    } //-- void deleteCdTratamentoFeriado() 

    /**
     * Method deleteCdTratamentoFeriadoDataPagto
     * 
     */
    public void deleteCdTratamentoFeriadoDataPagto()
    {
        this._has_cdTratamentoFeriadoDataPagto= false;
    } //-- void deleteCdTratamentoFeriadoDataPagto() 

    /**
     * Method deleteCdTratamentoValorDivergente
     * 
     */
    public void deleteCdTratamentoValorDivergente()
    {
        this._has_cdTratamentoValorDivergente= false;
    } //-- void deleteCdTratamentoValorDivergente() 

    /**
     * Method deleteCdTratoContaTransf
     * 
     */
    public void deleteCdTratoContaTransf()
    {
        this._has_cdTratoContaTransf= false;
    } //-- void deleteCdTratoContaTransf() 

    /**
     * Method deleteCdUtilizacaoCodigoLancamentoFixo
     * 
     */
    public void deleteCdUtilizacaoCodigoLancamentoFixo()
    {
        this._has_cdUtilizacaoCodigoLancamentoFixo= false;
    } //-- void deleteCdUtilizacaoCodigoLancamentoFixo() 

    /**
     * Method deleteCdUtilizacaoFavorecidoControlePagto
     * 
     */
    public void deleteCdUtilizacaoFavorecidoControlePagto()
    {
        this._has_cdUtilizacaoFavorecidoControlePagto= false;
    } //-- void deleteCdUtilizacaoFavorecidoControlePagto() 

    /**
     * Method deleteCdUtilizacaoMensagemPersonalizada
     * 
     */
    public void deleteCdUtilizacaoMensagemPersonalizada()
    {
        this._has_cdUtilizacaoMensagemPersonalizada= false;
    } //-- void deleteCdUtilizacaoMensagemPersonalizada() 

    /**
     * Method deleteCdValidacaoNomeFavorecido
     * 
     */
    public void deleteCdValidacaoNomeFavorecido()
    {
        this._has_cdValidacaoNomeFavorecido= false;
    } //-- void deleteCdValidacaoNomeFavorecido() 

    /**
     * Method deleteCdutilizacaoFormularioPersonalizado
     * 
     */
    public void deleteCdutilizacaoFormularioPersonalizado()
    {
        this._has_cdutilizacaoFormularioPersonalizado= false;
    } //-- void deleteCdutilizacaoFormularioPersonalizado() 

    /**
     * Method deletePercentualMaxRegInconsistenteLote
     * 
     */
    public void deletePercentualMaxRegInconsistenteLote()
    {
        this._has_percentualMaxRegInconsistenteLote= false;
    } //-- void deletePercentualMaxRegInconsistenteLote() 

    /**
     * Method deleteQtdeAntecipacaoInicioRecadastramento
     * 
     */
    public void deleteQtdeAntecipacaoInicioRecadastramento()
    {
        this._has_qtdeAntecipacaoInicioRecadastramento= false;
    } //-- void deleteQtdeAntecipacaoInicioRecadastramento() 

    /**
     * Method deleteQtdeDiaAvisoComprovanteVidaInicial
     * 
     */
    public void deleteQtdeDiaAvisoComprovanteVidaInicial()
    {
        this._has_qtdeDiaAvisoComprovanteVidaInicial= false;
    } //-- void deleteQtdeDiaAvisoComprovanteVidaInicial() 

    /**
     * Method deleteQtdeDiaAvisoComprovanteVidaVencimento
     * 
     */
    public void deleteQtdeDiaAvisoComprovanteVidaVencimento()
    {
        this._has_qtdeDiaAvisoComprovanteVidaVencimento= false;
    } //-- void deleteQtdeDiaAvisoComprovanteVidaVencimento() 

    /**
     * Method deleteQtdeDiaExpiracao
     * 
     */
    public void deleteQtdeDiaExpiracao()
    {
        this._has_qtdeDiaExpiracao= false;
    } //-- void deleteQtdeDiaExpiracao() 

    /**
     * Method deleteQtdeDiaFloating
     * 
     */
    public void deleteQtdeDiaFloating()
    {
        this._has_qtdeDiaFloating= false;
    } //-- void deleteQtdeDiaFloating() 

    /**
     * Method deleteQtdeDiaRepique
     * 
     */
    public void deleteQtdeDiaRepique()
    {
        this._has_qtdeDiaRepique= false;
    } //-- void deleteQtdeDiaRepique() 

    /**
     * Method deleteQtdeLimiteDiarioPagtoVencido
     * 
     */
    public void deleteQtdeLimiteDiarioPagtoVencido()
    {
        this._has_qtdeLimiteDiarioPagtoVencido= false;
    } //-- void deleteQtdeLimiteDiarioPagtoVencido() 

    /**
     * Method deleteQtdeMaxRegInconsistenteLote
     * 
     */
    public void deleteQtdeMaxRegInconsistenteLote()
    {
        this._has_qtdeMaxRegInconsistenteLote= false;
    } //-- void deleteQtdeMaxRegInconsistenteLote() 

    /**
     * Method deleteQtdeMesesComprovacaoVida
     * 
     */
    public void deleteQtdeMesesComprovacaoVida()
    {
        this._has_qtdeMesesComprovacaoVida= false;
    } //-- void deleteQtdeMesesComprovacaoVida() 

    /**
     * Returns the value of field 'cdAdesaoSacado'.
     * 
     * @return int
     * @return the value of field 'cdAdesaoSacado'.
     */
    public int getCdAdesaoSacado()
    {
        return this._cdAdesaoSacado;
    } //-- int getCdAdesaoSacado() 

    /**
     * Returns the value of field 'cdAgendaRastreabilidadeFilial'.
     * 
     * @return int
     * @return the value of field 'cdAgendaRastreabilidadeFilial'.
     */
    public int getCdAgendaRastreabilidadeFilial()
    {
        return this._cdAgendaRastreabilidadeFilial;
    } //-- int getCdAgendaRastreabilidadeFilial() 

    /**
     * Returns the value of field 'cdAgruparCorrespondencia'.
     * 
     * @return int
     * @return the value of field 'cdAgruparCorrespondencia'.
     */
    public int getCdAgruparCorrespondencia()
    {
        return this._cdAgruparCorrespondencia;
    } //-- int getCdAgruparCorrespondencia() 

    /**
     * Returns the value of field 'cdBloqueioEmissaoPplta'.
     * 
     * @return int
     * @return the value of field 'cdBloqueioEmissaoPplta'.
     */
    public int getCdBloqueioEmissaoPplta()
    {
        return this._cdBloqueioEmissaoPplta;
    } //-- int getCdBloqueioEmissaoPplta() 

    /**
     * Returns the value of field 'cdCadastroEnderecoUtilizado'.
     * 
     * @return int
     * @return the value of field 'cdCadastroEnderecoUtilizado'.
     */
    public int getCdCadastroEnderecoUtilizado()
    {
        return this._cdCadastroEnderecoUtilizado;
    } //-- int getCdCadastroEnderecoUtilizado() 

    /**
     * Returns the value of field 'cdCaptuTituloRegistro'.
     * 
     * @return int
     * @return the value of field 'cdCaptuTituloRegistro'.
     */
    public int getCdCaptuTituloRegistro()
    {
        return this._cdCaptuTituloRegistro;
    } //-- int getCdCaptuTituloRegistro() 

    /**
     * Returns the value of field 'cdConsistenciaCpfCnpj'.
     * 
     * @return int
     * @return the value of field 'cdConsistenciaCpfCnpj'.
     */
    public int getCdConsistenciaCpfCnpj()
    {
        return this._cdConsistenciaCpfCnpj;
    } //-- int getCdConsistenciaCpfCnpj() 

    /**
     * Returns the value of field 'cdCriterioRstrbTitulo'.
     * 
     * @return int
     * @return the value of field 'cdCriterioRstrbTitulo'.
     */
    public int getCdCriterioRstrbTitulo()
    {
        return this._cdCriterioRstrbTitulo;
    } //-- int getCdCriterioRstrbTitulo() 

    /**
     * Returns the value of field 'cdDestino'.
     * 
     * @return int
     * @return the value of field 'cdDestino'.
     */
    public int getCdDestino()
    {
        return this._cdDestino;
    } //-- int getCdDestino() 

    /**
     * Returns the value of field
     * 'cdEfetuaConsistenciaEspBeneficiario'.
     * 
     * @return int
     * @return the value of field
     * 'cdEfetuaConsistenciaEspBeneficiario'.
     */
    public int getCdEfetuaConsistenciaEspBeneficiario()
    {
        return this._cdEfetuaConsistenciaEspBeneficiario;
    } //-- int getCdEfetuaConsistenciaEspBeneficiario() 

    /**
     * Returns the value of field 'cdFormulario'.
     * 
     * @return long
     * @return the value of field 'cdFormulario'.
     */
    public long getCdFormulario()
    {
        return this._cdFormulario;
    } //-- long getCdFormulario() 

    /**
     * Returns the value of field
     * 'cdGeracaoLancamentoFuturoCredito'.
     * 
     * @return int
     * @return the value of field 'cdGeracaoLancamentoFuturoCredito'
     */
    public int getCdGeracaoLancamentoFuturoCredito()
    {
        return this._cdGeracaoLancamentoFuturoCredito;
    } //-- int getCdGeracaoLancamentoFuturoCredito() 

    /**
     * Returns the value of field
     * 'cdGeracaoLancamentoFuturoDebito'.
     * 
     * @return int
     * @return the value of field 'cdGeracaoLancamentoFuturoDebito'.
     */
    public int getCdGeracaoLancamentoFuturoDebito()
    {
        return this._cdGeracaoLancamentoFuturoDebito;
    } //-- int getCdGeracaoLancamentoFuturoDebito() 

    /**
     * Returns the value of field 'cdGeracaoLancamentoProgramado'.
     * 
     * @return int
     * @return the value of field 'cdGeracaoLancamentoProgramado'.
     */
    public int getCdGeracaoLancamentoProgramado()
    {
        return this._cdGeracaoLancamentoProgramado;
    } //-- int getCdGeracaoLancamentoProgramado() 

    /**
     * Returns the value of field 'cdIndicadorAgendaTitulo'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorAgendaTitulo'.
     */
    public int getCdIndicadorAgendaTitulo()
    {
        return this._cdIndicadorAgendaTitulo;
    } //-- int getCdIndicadorAgendaTitulo() 

    /**
     * Returns the value of field
     * 'cdIndicadorEmissaoComprovanteVida'.
     * 
     * @return int
     * @return the value of field
     * 'cdIndicadorEmissaoComprovanteVida'.
     */
    public int getCdIndicadorEmissaoComprovanteVida()
    {
        return this._cdIndicadorEmissaoComprovanteVida;
    } //-- int getCdIndicadorEmissaoComprovanteVida() 

    /**
     * Returns the value of field 'cdIndicadorModalidade'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorModalidade'.
     */
    public int getCdIndicadorModalidade()
    {
        return this._cdIndicadorModalidade;
    } //-- int getCdIndicadorModalidade() 

    /**
     * Returns the value of field 'cdLancamentoFixo'.
     * 
     * @return int
     * @return the value of field 'cdLancamentoFixo'.
     */
    public int getCdLancamentoFixo()
    {
        return this._cdLancamentoFixo;
    } //-- int getCdLancamentoFixo() 

    /**
     * Returns the value of field 'cdLiberacaoLoteProcs'.
     * 
     * @return int
     * @return the value of field 'cdLiberacaoLoteProcs'.
     */
    public int getCdLiberacaoLoteProcs()
    {
        return this._cdLiberacaoLoteProcs;
    } //-- int getCdLiberacaoLoteProcs() 

    /**
     * Returns the value of field 'cdModalidade'.
     * 
     * @return int
     * @return the value of field 'cdModalidade'.
     */
    public int getCdModalidade()
    {
        return this._cdModalidade;
    } //-- int getCdModalidade() 

    /**
     * Returns the value of field 'cdMomentoAvisoRecadastramento'.
     * 
     * @return int
     * @return the value of field 'cdMomentoAvisoRecadastramento'.
     */
    public int getCdMomentoAvisoRecadastramento()
    {
        return this._cdMomentoAvisoRecadastramento;
    } //-- int getCdMomentoAvisoRecadastramento() 

    /**
     * Returns the value of field 'cdOcorrenciaDiaExpiracao'.
     * 
     * @return int
     * @return the value of field 'cdOcorrenciaDiaExpiracao'.
     */
    public int getCdOcorrenciaDiaExpiracao()
    {
        return this._cdOcorrenciaDiaExpiracao;
    } //-- int getCdOcorrenciaDiaExpiracao() 

    /**
     * Returns the value of field 'cdPermissaoContingencia'.
     * 
     * @return int
     * @return the value of field 'cdPermissaoContingencia'.
     */
    public int getCdPermissaoContingencia()
    {
        return this._cdPermissaoContingencia;
    } //-- int getCdPermissaoContingencia() 

    /**
     * Returns the value of field 'cdPermissaoDebitoOnline'.
     * 
     * @return int
     * @return the value of field 'cdPermissaoDebitoOnline'.
     */
    public int getCdPermissaoDebitoOnline()
    {
        return this._cdPermissaoDebitoOnline;
    } //-- int getCdPermissaoDebitoOnline() 

    /**
     * Returns the value of field
     * 'cdPermissaoFavorecidoConsultaPagto'.
     * 
     * @return int
     * @return the value of field
     * 'cdPermissaoFavorecidoConsultaPagto'.
     */
    public int getCdPermissaoFavorecidoConsultaPagto()
    {
        return this._cdPermissaoFavorecidoConsultaPagto;
    } //-- int getCdPermissaoFavorecidoConsultaPagto() 

    /**
     * Returns the value of field 'cdPermissaoPagtoMenor'.
     * 
     * @return int
     * @return the value of field 'cdPermissaoPagtoMenor'.
     */
    public int getCdPermissaoPagtoMenor()
    {
        return this._cdPermissaoPagtoMenor;
    } //-- int getCdPermissaoPagtoMenor() 

    /**
     * Returns the value of field 'cdPermissaoPagtoVencido'.
     * 
     * @return int
     * @return the value of field 'cdPermissaoPagtoVencido'.
     */
    public int getCdPermissaoPagtoVencido()
    {
        return this._cdPermissaoPagtoVencido;
    } //-- int getCdPermissaoPagtoVencido() 

    /**
     * Returns the value of field 'cdPrioridadeDebito'.
     * 
     * @return int
     * @return the value of field 'cdPrioridadeDebito'.
     */
    public int getCdPrioridadeDebito()
    {
        return this._cdPrioridadeDebito;
    } //-- int getCdPrioridadeDebito() 

    /**
     * Returns the value of field 'cdRastreabNotaFiscal'.
     * 
     * @return int
     * @return the value of field 'cdRastreabNotaFiscal'.
     */
    public int getCdRastreabNotaFiscal()
    {
        return this._cdRastreabNotaFiscal;
    } //-- int getCdRastreabNotaFiscal() 

    /**
     * Returns the value of field 'cdRastreabTituloTerc'.
     * 
     * @return int
     * @return the value of field 'cdRastreabTituloTerc'.
     */
    public int getCdRastreabTituloTerc()
    {
        return this._cdRastreabTituloTerc;
    } //-- int getCdRastreabTituloTerc() 

    /**
     * Returns the value of field 'cdServico'.
     * 
     * @return int
     * @return the value of field 'cdServico'.
     */
    public int getCdServico()
    {
        return this._cdServico;
    } //-- int getCdServico() 

    /**
     * Returns the value of field 'cdTipoComprovacaoVida'.
     * 
     * @return int
     * @return the value of field 'cdTipoComprovacaoVida'.
     */
    public int getCdTipoComprovacaoVida()
    {
        return this._cdTipoComprovacaoVida;
    } //-- int getCdTipoComprovacaoVida() 

    /**
     * Returns the value of field
     * 'cdTipoConsistenciaCnpjFavorecido'.
     * 
     * @return int
     * @return the value of field 'cdTipoConsistenciaCnpjFavorecido'
     */
    public int getCdTipoConsistenciaCnpjFavorecido()
    {
        return this._cdTipoConsistenciaCnpjFavorecido;
    } //-- int getCdTipoConsistenciaCnpjFavorecido() 

    /**
     * Returns the value of field 'cdTipoConsistenciaCnpjProposta'.
     * 
     * @return int
     * @return the value of field 'cdTipoConsistenciaCnpjProposta'.
     */
    public int getCdTipoConsistenciaCnpjProposta()
    {
        return this._cdTipoConsistenciaCnpjProposta;
    } //-- int getCdTipoConsistenciaCnpjProposta() 

    /**
     * Returns the value of field 'cdTipoConsultaSaldo'.
     * 
     * @return int
     * @return the value of field 'cdTipoConsultaSaldo'.
     */
    public int getCdTipoConsultaSaldo()
    {
        return this._cdTipoConsultaSaldo;
    } //-- int getCdTipoConsultaSaldo() 

    /**
     * Returns the value of field 'cdTipoContaCredito'.
     * 
     * @return long
     * @return the value of field 'cdTipoContaCredito'.
     */
    public long getCdTipoContaCredito()
    {
        return this._cdTipoContaCredito;
    } //-- long getCdTipoContaCredito() 

    /**
     * Returns the value of field
     * 'cdTipoInscricaoFavorecidoAceita'.
     * 
     * @return int
     * @return the value of field 'cdTipoInscricaoFavorecidoAceita'.
     */
    public int getCdTipoInscricaoFavorecidoAceita()
    {
        return this._cdTipoInscricaoFavorecidoAceita;
    } //-- int getCdTipoInscricaoFavorecidoAceita() 

    /**
     * Returns the value of field 'cdTipoProcessamento'.
     * 
     * @return int
     * @return the value of field 'cdTipoProcessamento'.
     */
    public int getCdTipoProcessamento()
    {
        return this._cdTipoProcessamento;
    } //-- int getCdTipoProcessamento() 

    /**
     * Returns the value of field 'cdTipoRejeicaoAgendamento'.
     * 
     * @return int
     * @return the value of field 'cdTipoRejeicaoAgendamento'.
     */
    public int getCdTipoRejeicaoAgendamento()
    {
        return this._cdTipoRejeicaoAgendamento;
    } //-- int getCdTipoRejeicaoAgendamento() 

    /**
     * Returns the value of field 'cdTipoRejeicaoEfetivacao'.
     * 
     * @return int
     * @return the value of field 'cdTipoRejeicaoEfetivacao'.
     */
    public int getCdTipoRejeicaoEfetivacao()
    {
        return this._cdTipoRejeicaoEfetivacao;
    } //-- int getCdTipoRejeicaoEfetivacao() 

    /**
     * Returns the value of field 'cdTipoSaldo'.
     * 
     * @return int
     * @return the value of field 'cdTipoSaldo'.
     */
    public int getCdTipoSaldo()
    {
        return this._cdTipoSaldo;
    } //-- int getCdTipoSaldo() 

    /**
     * Returns the value of field 'cdTipoSaldoValorSuperior'.
     * 
     * @return int
     * @return the value of field 'cdTipoSaldoValorSuperior'.
     */
    public int getCdTipoSaldoValorSuperior()
    {
        return this._cdTipoSaldoValorSuperior;
    } //-- int getCdTipoSaldoValorSuperior() 

    /**
     * Returns the value of field 'cdTratamentoFeriado'.
     * 
     * @return int
     * @return the value of field 'cdTratamentoFeriado'.
     */
    public int getCdTratamentoFeriado()
    {
        return this._cdTratamentoFeriado;
    } //-- int getCdTratamentoFeriado() 

    /**
     * Returns the value of field 'cdTratamentoFeriadoDataPagto'.
     * 
     * @return int
     * @return the value of field 'cdTratamentoFeriadoDataPagto'.
     */
    public int getCdTratamentoFeriadoDataPagto()
    {
        return this._cdTratamentoFeriadoDataPagto;
    } //-- int getCdTratamentoFeriadoDataPagto() 

    /**
     * Returns the value of field 'cdTratamentoValorDivergente'.
     * 
     * @return int
     * @return the value of field 'cdTratamentoValorDivergente'.
     */
    public int getCdTratamentoValorDivergente()
    {
        return this._cdTratamentoValorDivergente;
    } //-- int getCdTratamentoValorDivergente() 

    /**
     * Returns the value of field 'cdTratoContaTransf'.
     * 
     * @return int
     * @return the value of field 'cdTratoContaTransf'.
     */
    public int getCdTratoContaTransf()
    {
        return this._cdTratoContaTransf;
    } //-- int getCdTratoContaTransf() 

    /**
     * Returns the value of field
     * 'cdUtilizacaoCodigoLancamentoFixo'.
     * 
     * @return int
     * @return the value of field 'cdUtilizacaoCodigoLancamentoFixo'
     */
    public int getCdUtilizacaoCodigoLancamentoFixo()
    {
        return this._cdUtilizacaoCodigoLancamentoFixo;
    } //-- int getCdUtilizacaoCodigoLancamentoFixo() 

    /**
     * Returns the value of field
     * 'cdUtilizacaoFavorecidoControlePagto'.
     * 
     * @return int
     * @return the value of field
     * 'cdUtilizacaoFavorecidoControlePagto'.
     */
    public int getCdUtilizacaoFavorecidoControlePagto()
    {
        return this._cdUtilizacaoFavorecidoControlePagto;
    } //-- int getCdUtilizacaoFavorecidoControlePagto() 

    /**
     * Returns the value of field
     * 'cdUtilizacaoMensagemPersonalizada'.
     * 
     * @return int
     * @return the value of field
     * 'cdUtilizacaoMensagemPersonalizada'.
     */
    public int getCdUtilizacaoMensagemPersonalizada()
    {
        return this._cdUtilizacaoMensagemPersonalizada;
    } //-- int getCdUtilizacaoMensagemPersonalizada() 

    /**
     * Returns the value of field 'cdValidacaoNomeFavorecido'.
     * 
     * @return int
     * @return the value of field 'cdValidacaoNomeFavorecido'.
     */
    public int getCdValidacaoNomeFavorecido()
    {
        return this._cdValidacaoNomeFavorecido;
    } //-- int getCdValidacaoNomeFavorecido() 

    /**
     * Returns the value of field
     * 'cdutilizacaoFormularioPersonalizado'.
     * 
     * @return int
     * @return the value of field
     * 'cdutilizacaoFormularioPersonalizado'.
     */
    public int getCdutilizacaoFormularioPersonalizado()
    {
        return this._cdutilizacaoFormularioPersonalizado;
    } //-- int getCdutilizacaoFormularioPersonalizado() 

    /**
     * Returns the value of field 'dtInicioBloqueioPplta'.
     * 
     * @return String
     * @return the value of field 'dtInicioBloqueioPplta'.
     */
    public java.lang.String getDtInicioBloqueioPplta()
    {
        return this._dtInicioBloqueioPplta;
    } //-- java.lang.String getDtInicioBloqueioPplta() 

    /**
     * Returns the value of field 'dtInicioRastreabTitulo'.
     * 
     * @return String
     * @return the value of field 'dtInicioRastreabTitulo'.
     */
    public java.lang.String getDtInicioRastreabTitulo()
    {
        return this._dtInicioRastreabTitulo;
    } //-- java.lang.String getDtInicioRastreabTitulo() 

    /**
     * Returns the value of field 'dtInicioRegTitulo'.
     * 
     * @return String
     * @return the value of field 'dtInicioRegTitulo'.
     */
    public java.lang.String getDtInicioRegTitulo()
    {
        return this._dtInicioRegTitulo;
    } //-- java.lang.String getDtInicioRegTitulo() 

    /**
     * Returns the value of field
     * 'percentualMaxRegInconsistenteLote'.
     * 
     * @return int
     * @return the value of field
     * 'percentualMaxRegInconsistenteLote'.
     */
    public int getPercentualMaxRegInconsistenteLote()
    {
        return this._percentualMaxRegInconsistenteLote;
    } //-- int getPercentualMaxRegInconsistenteLote() 

    /**
     * Returns the value of field
     * 'qtdeAntecipacaoInicioRecadastramento'.
     * 
     * @return int
     * @return the value of field
     * 'qtdeAntecipacaoInicioRecadastramento'.
     */
    public int getQtdeAntecipacaoInicioRecadastramento()
    {
        return this._qtdeAntecipacaoInicioRecadastramento;
    } //-- int getQtdeAntecipacaoInicioRecadastramento() 

    /**
     * Returns the value of field
     * 'qtdeDiaAvisoComprovanteVidaInicial'.
     * 
     * @return int
     * @return the value of field
     * 'qtdeDiaAvisoComprovanteVidaInicial'.
     */
    public int getQtdeDiaAvisoComprovanteVidaInicial()
    {
        return this._qtdeDiaAvisoComprovanteVidaInicial;
    } //-- int getQtdeDiaAvisoComprovanteVidaInicial() 

    /**
     * Returns the value of field
     * 'qtdeDiaAvisoComprovanteVidaVencimento'.
     * 
     * @return int
     * @return the value of field
     * 'qtdeDiaAvisoComprovanteVidaVencimento'.
     */
    public int getQtdeDiaAvisoComprovanteVidaVencimento()
    {
        return this._qtdeDiaAvisoComprovanteVidaVencimento;
    } //-- int getQtdeDiaAvisoComprovanteVidaVencimento() 

    /**
     * Returns the value of field 'qtdeDiaExpiracao'.
     * 
     * @return int
     * @return the value of field 'qtdeDiaExpiracao'.
     */
    public int getQtdeDiaExpiracao()
    {
        return this._qtdeDiaExpiracao;
    } //-- int getQtdeDiaExpiracao() 

    /**
     * Returns the value of field 'qtdeDiaFloating'.
     * 
     * @return int
     * @return the value of field 'qtdeDiaFloating'.
     */
    public int getQtdeDiaFloating()
    {
        return this._qtdeDiaFloating;
    } //-- int getQtdeDiaFloating() 

    /**
     * Returns the value of field 'qtdeDiaRepique'.
     * 
     * @return int
     * @return the value of field 'qtdeDiaRepique'.
     */
    public int getQtdeDiaRepique()
    {
        return this._qtdeDiaRepique;
    } //-- int getQtdeDiaRepique() 

    /**
     * Returns the value of field 'qtdeLimiteDiarioPagtoVencido'.
     * 
     * @return int
     * @return the value of field 'qtdeLimiteDiarioPagtoVencido'.
     */
    public int getQtdeLimiteDiarioPagtoVencido()
    {
        return this._qtdeLimiteDiarioPagtoVencido;
    } //-- int getQtdeLimiteDiarioPagtoVencido() 

    /**
     * Returns the value of field 'qtdeMaxRegInconsistenteLote'.
     * 
     * @return int
     * @return the value of field 'qtdeMaxRegInconsistenteLote'.
     */
    public int getQtdeMaxRegInconsistenteLote()
    {
        return this._qtdeMaxRegInconsistenteLote;
    } //-- int getQtdeMaxRegInconsistenteLote() 

    /**
     * Returns the value of field 'qtdeMesesComprovacaoVida'.
     * 
     * @return int
     * @return the value of field 'qtdeMesesComprovacaoVida'.
     */
    public int getQtdeMesesComprovacaoVida()
    {
        return this._qtdeMesesComprovacaoVida;
    } //-- int getQtdeMesesComprovacaoVida() 

    /**
     * Returns the value of field 'vlrLimiteDiario'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlrLimiteDiario'.
     */
    public java.math.BigDecimal getVlrLimiteDiario()
    {
        return this._vlrLimiteDiario;
    } //-- java.math.BigDecimal getVlrLimiteDiario() 

    /**
     * Returns the value of field 'vlrLimiteIndividual'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlrLimiteIndividual'.
     */
    public java.math.BigDecimal getVlrLimiteIndividual()
    {
        return this._vlrLimiteIndividual;
    } //-- java.math.BigDecimal getVlrLimiteIndividual() 

    /**
     * Returns the value of field
     * 'vlrMaximoPagtoFavorecidoNaoCadastrado'.
     * 
     * @return BigDecimal
     * @return the value of field
     * 'vlrMaximoPagtoFavorecidoNaoCadastrado'.
     */
    public java.math.BigDecimal getVlrMaximoPagtoFavorecidoNaoCadastrado()
    {
        return this._vlrMaximoPagtoFavorecidoNaoCadastrado;
    } //-- java.math.BigDecimal getVlrMaximoPagtoFavorecidoNaoCadastrado() 

    /**
     * Method hasCdAdesaoSacado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAdesaoSacado()
    {
        return this._has_cdAdesaoSacado;
    } //-- boolean hasCdAdesaoSacado() 

    /**
     * Method hasCdAgendaRastreabilidadeFilial
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgendaRastreabilidadeFilial()
    {
        return this._has_cdAgendaRastreabilidadeFilial;
    } //-- boolean hasCdAgendaRastreabilidadeFilial() 

    /**
     * Method hasCdAgruparCorrespondencia
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgruparCorrespondencia()
    {
        return this._has_cdAgruparCorrespondencia;
    } //-- boolean hasCdAgruparCorrespondencia() 

    /**
     * Method hasCdBloqueioEmissaoPplta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdBloqueioEmissaoPplta()
    {
        return this._has_cdBloqueioEmissaoPplta;
    } //-- boolean hasCdBloqueioEmissaoPplta() 

    /**
     * Method hasCdCadastroEnderecoUtilizado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCadastroEnderecoUtilizado()
    {
        return this._has_cdCadastroEnderecoUtilizado;
    } //-- boolean hasCdCadastroEnderecoUtilizado() 

    /**
     * Method hasCdCaptuTituloRegistro
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCaptuTituloRegistro()
    {
        return this._has_cdCaptuTituloRegistro;
    } //-- boolean hasCdCaptuTituloRegistro() 

    /**
     * Method hasCdConsistenciaCpfCnpj
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdConsistenciaCpfCnpj()
    {
        return this._has_cdConsistenciaCpfCnpj;
    } //-- boolean hasCdConsistenciaCpfCnpj() 

    /**
     * Method hasCdCriterioRstrbTitulo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCriterioRstrbTitulo()
    {
        return this._has_cdCriterioRstrbTitulo;
    } //-- boolean hasCdCriterioRstrbTitulo() 

    /**
     * Method hasCdDestino
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdDestino()
    {
        return this._has_cdDestino;
    } //-- boolean hasCdDestino() 

    /**
     * Method hasCdEfetuaConsistenciaEspBeneficiario
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdEfetuaConsistenciaEspBeneficiario()
    {
        return this._has_cdEfetuaConsistenciaEspBeneficiario;
    } //-- boolean hasCdEfetuaConsistenciaEspBeneficiario() 

    /**
     * Method hasCdFormulario
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFormulario()
    {
        return this._has_cdFormulario;
    } //-- boolean hasCdFormulario() 

    /**
     * Method hasCdGeracaoLancamentoFuturoCredito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdGeracaoLancamentoFuturoCredito()
    {
        return this._has_cdGeracaoLancamentoFuturoCredito;
    } //-- boolean hasCdGeracaoLancamentoFuturoCredito() 

    /**
     * Method hasCdGeracaoLancamentoFuturoDebito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdGeracaoLancamentoFuturoDebito()
    {
        return this._has_cdGeracaoLancamentoFuturoDebito;
    } //-- boolean hasCdGeracaoLancamentoFuturoDebito() 

    /**
     * Method hasCdGeracaoLancamentoProgramado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdGeracaoLancamentoProgramado()
    {
        return this._has_cdGeracaoLancamentoProgramado;
    } //-- boolean hasCdGeracaoLancamentoProgramado() 

    /**
     * Method hasCdIndicadorAgendaTitulo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorAgendaTitulo()
    {
        return this._has_cdIndicadorAgendaTitulo;
    } //-- boolean hasCdIndicadorAgendaTitulo() 

    /**
     * Method hasCdIndicadorEmissaoComprovanteVida
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorEmissaoComprovanteVida()
    {
        return this._has_cdIndicadorEmissaoComprovanteVida;
    } //-- boolean hasCdIndicadorEmissaoComprovanteVida() 

    /**
     * Method hasCdIndicadorModalidade
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorModalidade()
    {
        return this._has_cdIndicadorModalidade;
    } //-- boolean hasCdIndicadorModalidade() 

    /**
     * Method hasCdLancamentoFixo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdLancamentoFixo()
    {
        return this._has_cdLancamentoFixo;
    } //-- boolean hasCdLancamentoFixo() 

    /**
     * Method hasCdLiberacaoLoteProcs
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdLiberacaoLoteProcs()
    {
        return this._has_cdLiberacaoLoteProcs;
    } //-- boolean hasCdLiberacaoLoteProcs() 

    /**
     * Method hasCdModalidade
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdModalidade()
    {
        return this._has_cdModalidade;
    } //-- boolean hasCdModalidade() 

    /**
     * Method hasCdMomentoAvisoRecadastramento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMomentoAvisoRecadastramento()
    {
        return this._has_cdMomentoAvisoRecadastramento;
    } //-- boolean hasCdMomentoAvisoRecadastramento() 

    /**
     * Method hasCdOcorrenciaDiaExpiracao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdOcorrenciaDiaExpiracao()
    {
        return this._has_cdOcorrenciaDiaExpiracao;
    } //-- boolean hasCdOcorrenciaDiaExpiracao() 

    /**
     * Method hasCdPermissaoContingencia
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPermissaoContingencia()
    {
        return this._has_cdPermissaoContingencia;
    } //-- boolean hasCdPermissaoContingencia() 

    /**
     * Method hasCdPermissaoDebitoOnline
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPermissaoDebitoOnline()
    {
        return this._has_cdPermissaoDebitoOnline;
    } //-- boolean hasCdPermissaoDebitoOnline() 

    /**
     * Method hasCdPermissaoFavorecidoConsultaPagto
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPermissaoFavorecidoConsultaPagto()
    {
        return this._has_cdPermissaoFavorecidoConsultaPagto;
    } //-- boolean hasCdPermissaoFavorecidoConsultaPagto() 

    /**
     * Method hasCdPermissaoPagtoMenor
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPermissaoPagtoMenor()
    {
        return this._has_cdPermissaoPagtoMenor;
    } //-- boolean hasCdPermissaoPagtoMenor() 

    /**
     * Method hasCdPermissaoPagtoVencido
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPermissaoPagtoVencido()
    {
        return this._has_cdPermissaoPagtoVencido;
    } //-- boolean hasCdPermissaoPagtoVencido() 

    /**
     * Method hasCdPrioridadeDebito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPrioridadeDebito()
    {
        return this._has_cdPrioridadeDebito;
    } //-- boolean hasCdPrioridadeDebito() 

    /**
     * Method hasCdRastreabNotaFiscal
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdRastreabNotaFiscal()
    {
        return this._has_cdRastreabNotaFiscal;
    } //-- boolean hasCdRastreabNotaFiscal() 

    /**
     * Method hasCdRastreabTituloTerc
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdRastreabTituloTerc()
    {
        return this._has_cdRastreabTituloTerc;
    } //-- boolean hasCdRastreabTituloTerc() 

    /**
     * Method hasCdServico
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdServico()
    {
        return this._has_cdServico;
    } //-- boolean hasCdServico() 

    /**
     * Method hasCdTipoComprovacaoVida
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoComprovacaoVida()
    {
        return this._has_cdTipoComprovacaoVida;
    } //-- boolean hasCdTipoComprovacaoVida() 

    /**
     * Method hasCdTipoConsistenciaCnpjFavorecido
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoConsistenciaCnpjFavorecido()
    {
        return this._has_cdTipoConsistenciaCnpjFavorecido;
    } //-- boolean hasCdTipoConsistenciaCnpjFavorecido() 

    /**
     * Method hasCdTipoConsistenciaCnpjProposta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoConsistenciaCnpjProposta()
    {
        return this._has_cdTipoConsistenciaCnpjProposta;
    } //-- boolean hasCdTipoConsistenciaCnpjProposta() 

    /**
     * Method hasCdTipoConsultaSaldo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoConsultaSaldo()
    {
        return this._has_cdTipoConsultaSaldo;
    } //-- boolean hasCdTipoConsultaSaldo() 

    /**
     * Method hasCdTipoContaCredito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContaCredito()
    {
        return this._has_cdTipoContaCredito;
    } //-- boolean hasCdTipoContaCredito() 

    /**
     * Method hasCdTipoInscricaoFavorecidoAceita
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoInscricaoFavorecidoAceita()
    {
        return this._has_cdTipoInscricaoFavorecidoAceita;
    } //-- boolean hasCdTipoInscricaoFavorecidoAceita() 

    /**
     * Method hasCdTipoProcessamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoProcessamento()
    {
        return this._has_cdTipoProcessamento;
    } //-- boolean hasCdTipoProcessamento() 

    /**
     * Method hasCdTipoRejeicaoAgendamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoRejeicaoAgendamento()
    {
        return this._has_cdTipoRejeicaoAgendamento;
    } //-- boolean hasCdTipoRejeicaoAgendamento() 

    /**
     * Method hasCdTipoRejeicaoEfetivacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoRejeicaoEfetivacao()
    {
        return this._has_cdTipoRejeicaoEfetivacao;
    } //-- boolean hasCdTipoRejeicaoEfetivacao() 

    /**
     * Method hasCdTipoSaldo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoSaldo()
    {
        return this._has_cdTipoSaldo;
    } //-- boolean hasCdTipoSaldo() 

    /**
     * Method hasCdTipoSaldoValorSuperior
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoSaldoValorSuperior()
    {
        return this._has_cdTipoSaldoValorSuperior;
    } //-- boolean hasCdTipoSaldoValorSuperior() 

    /**
     * Method hasCdTratamentoFeriado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTratamentoFeriado()
    {
        return this._has_cdTratamentoFeriado;
    } //-- boolean hasCdTratamentoFeriado() 

    /**
     * Method hasCdTratamentoFeriadoDataPagto
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTratamentoFeriadoDataPagto()
    {
        return this._has_cdTratamentoFeriadoDataPagto;
    } //-- boolean hasCdTratamentoFeriadoDataPagto() 

    /**
     * Method hasCdTratamentoValorDivergente
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTratamentoValorDivergente()
    {
        return this._has_cdTratamentoValorDivergente;
    } //-- boolean hasCdTratamentoValorDivergente() 

    /**
     * Method hasCdTratoContaTransf
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTratoContaTransf()
    {
        return this._has_cdTratoContaTransf;
    } //-- boolean hasCdTratoContaTransf() 

    /**
     * Method hasCdUtilizacaoCodigoLancamentoFixo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdUtilizacaoCodigoLancamentoFixo()
    {
        return this._has_cdUtilizacaoCodigoLancamentoFixo;
    } //-- boolean hasCdUtilizacaoCodigoLancamentoFixo() 

    /**
     * Method hasCdUtilizacaoFavorecidoControlePagto
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdUtilizacaoFavorecidoControlePagto()
    {
        return this._has_cdUtilizacaoFavorecidoControlePagto;
    } //-- boolean hasCdUtilizacaoFavorecidoControlePagto() 

    /**
     * Method hasCdUtilizacaoMensagemPersonalizada
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdUtilizacaoMensagemPersonalizada()
    {
        return this._has_cdUtilizacaoMensagemPersonalizada;
    } //-- boolean hasCdUtilizacaoMensagemPersonalizada() 

    /**
     * Method hasCdValidacaoNomeFavorecido
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdValidacaoNomeFavorecido()
    {
        return this._has_cdValidacaoNomeFavorecido;
    } //-- boolean hasCdValidacaoNomeFavorecido() 

    /**
     * Method hasCdutilizacaoFormularioPersonalizado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdutilizacaoFormularioPersonalizado()
    {
        return this._has_cdutilizacaoFormularioPersonalizado;
    } //-- boolean hasCdutilizacaoFormularioPersonalizado() 

    /**
     * Method hasPercentualMaxRegInconsistenteLote
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasPercentualMaxRegInconsistenteLote()
    {
        return this._has_percentualMaxRegInconsistenteLote;
    } //-- boolean hasPercentualMaxRegInconsistenteLote() 

    /**
     * Method hasQtdeAntecipacaoInicioRecadastramento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtdeAntecipacaoInicioRecadastramento()
    {
        return this._has_qtdeAntecipacaoInicioRecadastramento;
    } //-- boolean hasQtdeAntecipacaoInicioRecadastramento() 

    /**
     * Method hasQtdeDiaAvisoComprovanteVidaInicial
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtdeDiaAvisoComprovanteVidaInicial()
    {
        return this._has_qtdeDiaAvisoComprovanteVidaInicial;
    } //-- boolean hasQtdeDiaAvisoComprovanteVidaInicial() 

    /**
     * Method hasQtdeDiaAvisoComprovanteVidaVencimento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtdeDiaAvisoComprovanteVidaVencimento()
    {
        return this._has_qtdeDiaAvisoComprovanteVidaVencimento;
    } //-- boolean hasQtdeDiaAvisoComprovanteVidaVencimento() 

    /**
     * Method hasQtdeDiaExpiracao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtdeDiaExpiracao()
    {
        return this._has_qtdeDiaExpiracao;
    } //-- boolean hasQtdeDiaExpiracao() 

    /**
     * Method hasQtdeDiaFloating
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtdeDiaFloating()
    {
        return this._has_qtdeDiaFloating;
    } //-- boolean hasQtdeDiaFloating() 

    /**
     * Method hasQtdeDiaRepique
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtdeDiaRepique()
    {
        return this._has_qtdeDiaRepique;
    } //-- boolean hasQtdeDiaRepique() 

    /**
     * Method hasQtdeLimiteDiarioPagtoVencido
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtdeLimiteDiarioPagtoVencido()
    {
        return this._has_qtdeLimiteDiarioPagtoVencido;
    } //-- boolean hasQtdeLimiteDiarioPagtoVencido() 

    /**
     * Method hasQtdeMaxRegInconsistenteLote
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtdeMaxRegInconsistenteLote()
    {
        return this._has_qtdeMaxRegInconsistenteLote;
    } //-- boolean hasQtdeMaxRegInconsistenteLote() 

    /**
     * Method hasQtdeMesesComprovacaoVida
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtdeMesesComprovacaoVida()
    {
        return this._has_qtdeMesesComprovacaoVida;
    } //-- boolean hasQtdeMesesComprovacaoVida() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdAdesaoSacado'.
     * 
     * @param cdAdesaoSacado the value of field 'cdAdesaoSacado'.
     */
    public void setCdAdesaoSacado(int cdAdesaoSacado)
    {
        this._cdAdesaoSacado = cdAdesaoSacado;
        this._has_cdAdesaoSacado = true;
    } //-- void setCdAdesaoSacado(int) 

    /**
     * Sets the value of field 'cdAgendaRastreabilidadeFilial'.
     * 
     * @param cdAgendaRastreabilidadeFilial the value of field
     * 'cdAgendaRastreabilidadeFilial'.
     */
    public void setCdAgendaRastreabilidadeFilial(int cdAgendaRastreabilidadeFilial)
    {
        this._cdAgendaRastreabilidadeFilial = cdAgendaRastreabilidadeFilial;
        this._has_cdAgendaRastreabilidadeFilial = true;
    } //-- void setCdAgendaRastreabilidadeFilial(int) 

    /**
     * Sets the value of field 'cdAgruparCorrespondencia'.
     * 
     * @param cdAgruparCorrespondencia the value of field
     * 'cdAgruparCorrespondencia'.
     */
    public void setCdAgruparCorrespondencia(int cdAgruparCorrespondencia)
    {
        this._cdAgruparCorrespondencia = cdAgruparCorrespondencia;
        this._has_cdAgruparCorrespondencia = true;
    } //-- void setCdAgruparCorrespondencia(int) 

    /**
     * Sets the value of field 'cdBloqueioEmissaoPplta'.
     * 
     * @param cdBloqueioEmissaoPplta the value of field
     * 'cdBloqueioEmissaoPplta'.
     */
    public void setCdBloqueioEmissaoPplta(int cdBloqueioEmissaoPplta)
    {
        this._cdBloqueioEmissaoPplta = cdBloqueioEmissaoPplta;
        this._has_cdBloqueioEmissaoPplta = true;
    } //-- void setCdBloqueioEmissaoPplta(int) 

    /**
     * Sets the value of field 'cdCadastroEnderecoUtilizado'.
     * 
     * @param cdCadastroEnderecoUtilizado the value of field
     * 'cdCadastroEnderecoUtilizado'.
     */
    public void setCdCadastroEnderecoUtilizado(int cdCadastroEnderecoUtilizado)
    {
        this._cdCadastroEnderecoUtilizado = cdCadastroEnderecoUtilizado;
        this._has_cdCadastroEnderecoUtilizado = true;
    } //-- void setCdCadastroEnderecoUtilizado(int) 

    /**
     * Sets the value of field 'cdCaptuTituloRegistro'.
     * 
     * @param cdCaptuTituloRegistro the value of field
     * 'cdCaptuTituloRegistro'.
     */
    public void setCdCaptuTituloRegistro(int cdCaptuTituloRegistro)
    {
        this._cdCaptuTituloRegistro = cdCaptuTituloRegistro;
        this._has_cdCaptuTituloRegistro = true;
    } //-- void setCdCaptuTituloRegistro(int) 

    /**
     * Sets the value of field 'cdConsistenciaCpfCnpj'.
     * 
     * @param cdConsistenciaCpfCnpj the value of field
     * 'cdConsistenciaCpfCnpj'.
     */
    public void setCdConsistenciaCpfCnpj(int cdConsistenciaCpfCnpj)
    {
        this._cdConsistenciaCpfCnpj = cdConsistenciaCpfCnpj;
        this._has_cdConsistenciaCpfCnpj = true;
    } //-- void setCdConsistenciaCpfCnpj(int) 

    /**
     * Sets the value of field 'cdCriterioRstrbTitulo'.
     * 
     * @param cdCriterioRstrbTitulo the value of field
     * 'cdCriterioRstrbTitulo'.
     */
    public void setCdCriterioRstrbTitulo(int cdCriterioRstrbTitulo)
    {
        this._cdCriterioRstrbTitulo = cdCriterioRstrbTitulo;
        this._has_cdCriterioRstrbTitulo = true;
    } //-- void setCdCriterioRstrbTitulo(int) 

    /**
     * Sets the value of field 'cdDestino'.
     * 
     * @param cdDestino the value of field 'cdDestino'.
     */
    public void setCdDestino(int cdDestino)
    {
        this._cdDestino = cdDestino;
        this._has_cdDestino = true;
    } //-- void setCdDestino(int) 

    /**
     * Sets the value of field
     * 'cdEfetuaConsistenciaEspBeneficiario'.
     * 
     * @param cdEfetuaConsistenciaEspBeneficiario the value of
     * field 'cdEfetuaConsistenciaEspBeneficiario'.
     */
    public void setCdEfetuaConsistenciaEspBeneficiario(int cdEfetuaConsistenciaEspBeneficiario)
    {
        this._cdEfetuaConsistenciaEspBeneficiario = cdEfetuaConsistenciaEspBeneficiario;
        this._has_cdEfetuaConsistenciaEspBeneficiario = true;
    } //-- void setCdEfetuaConsistenciaEspBeneficiario(int) 

    /**
     * Sets the value of field 'cdFormulario'.
     * 
     * @param cdFormulario the value of field 'cdFormulario'.
     */
    public void setCdFormulario(long cdFormulario)
    {
        this._cdFormulario = cdFormulario;
        this._has_cdFormulario = true;
    } //-- void setCdFormulario(long) 

    /**
     * Sets the value of field 'cdGeracaoLancamentoFuturoCredito'.
     * 
     * @param cdGeracaoLancamentoFuturoCredito the value of field
     * 'cdGeracaoLancamentoFuturoCredito'.
     */
    public void setCdGeracaoLancamentoFuturoCredito(int cdGeracaoLancamentoFuturoCredito)
    {
        this._cdGeracaoLancamentoFuturoCredito = cdGeracaoLancamentoFuturoCredito;
        this._has_cdGeracaoLancamentoFuturoCredito = true;
    } //-- void setCdGeracaoLancamentoFuturoCredito(int) 

    /**
     * Sets the value of field 'cdGeracaoLancamentoFuturoDebito'.
     * 
     * @param cdGeracaoLancamentoFuturoDebito the value of field
     * 'cdGeracaoLancamentoFuturoDebito'.
     */
    public void setCdGeracaoLancamentoFuturoDebito(int cdGeracaoLancamentoFuturoDebito)
    {
        this._cdGeracaoLancamentoFuturoDebito = cdGeracaoLancamentoFuturoDebito;
        this._has_cdGeracaoLancamentoFuturoDebito = true;
    } //-- void setCdGeracaoLancamentoFuturoDebito(int) 

    /**
     * Sets the value of field 'cdGeracaoLancamentoProgramado'.
     * 
     * @param cdGeracaoLancamentoProgramado the value of field
     * 'cdGeracaoLancamentoProgramado'.
     */
    public void setCdGeracaoLancamentoProgramado(int cdGeracaoLancamentoProgramado)
    {
        this._cdGeracaoLancamentoProgramado = cdGeracaoLancamentoProgramado;
        this._has_cdGeracaoLancamentoProgramado = true;
    } //-- void setCdGeracaoLancamentoProgramado(int) 

    /**
     * Sets the value of field 'cdIndicadorAgendaTitulo'.
     * 
     * @param cdIndicadorAgendaTitulo the value of field
     * 'cdIndicadorAgendaTitulo'.
     */
    public void setCdIndicadorAgendaTitulo(int cdIndicadorAgendaTitulo)
    {
        this._cdIndicadorAgendaTitulo = cdIndicadorAgendaTitulo;
        this._has_cdIndicadorAgendaTitulo = true;
    } //-- void setCdIndicadorAgendaTitulo(int) 

    /**
     * Sets the value of field 'cdIndicadorEmissaoComprovanteVida'.
     * 
     * @param cdIndicadorEmissaoComprovanteVida the value of field
     * 'cdIndicadorEmissaoComprovanteVida'.
     */
    public void setCdIndicadorEmissaoComprovanteVida(int cdIndicadorEmissaoComprovanteVida)
    {
        this._cdIndicadorEmissaoComprovanteVida = cdIndicadorEmissaoComprovanteVida;
        this._has_cdIndicadorEmissaoComprovanteVida = true;
    } //-- void setCdIndicadorEmissaoComprovanteVida(int) 

    /**
     * Sets the value of field 'cdIndicadorModalidade'.
     * 
     * @param cdIndicadorModalidade the value of field
     * 'cdIndicadorModalidade'.
     */
    public void setCdIndicadorModalidade(int cdIndicadorModalidade)
    {
        this._cdIndicadorModalidade = cdIndicadorModalidade;
        this._has_cdIndicadorModalidade = true;
    } //-- void setCdIndicadorModalidade(int) 

    /**
     * Sets the value of field 'cdLancamentoFixo'.
     * 
     * @param cdLancamentoFixo the value of field 'cdLancamentoFixo'
     */
    public void setCdLancamentoFixo(int cdLancamentoFixo)
    {
        this._cdLancamentoFixo = cdLancamentoFixo;
        this._has_cdLancamentoFixo = true;
    } //-- void setCdLancamentoFixo(int) 

    /**
     * Sets the value of field 'cdLiberacaoLoteProcs'.
     * 
     * @param cdLiberacaoLoteProcs the value of field
     * 'cdLiberacaoLoteProcs'.
     */
    public void setCdLiberacaoLoteProcs(int cdLiberacaoLoteProcs)
    {
        this._cdLiberacaoLoteProcs = cdLiberacaoLoteProcs;
        this._has_cdLiberacaoLoteProcs = true;
    } //-- void setCdLiberacaoLoteProcs(int) 

    /**
     * Sets the value of field 'cdModalidade'.
     * 
     * @param cdModalidade the value of field 'cdModalidade'.
     */
    public void setCdModalidade(int cdModalidade)
    {
        this._cdModalidade = cdModalidade;
        this._has_cdModalidade = true;
    } //-- void setCdModalidade(int) 

    /**
     * Sets the value of field 'cdMomentoAvisoRecadastramento'.
     * 
     * @param cdMomentoAvisoRecadastramento the value of field
     * 'cdMomentoAvisoRecadastramento'.
     */
    public void setCdMomentoAvisoRecadastramento(int cdMomentoAvisoRecadastramento)
    {
        this._cdMomentoAvisoRecadastramento = cdMomentoAvisoRecadastramento;
        this._has_cdMomentoAvisoRecadastramento = true;
    } //-- void setCdMomentoAvisoRecadastramento(int) 

    /**
     * Sets the value of field 'cdOcorrenciaDiaExpiracao'.
     * 
     * @param cdOcorrenciaDiaExpiracao the value of field
     * 'cdOcorrenciaDiaExpiracao'.
     */
    public void setCdOcorrenciaDiaExpiracao(int cdOcorrenciaDiaExpiracao)
    {
        this._cdOcorrenciaDiaExpiracao = cdOcorrenciaDiaExpiracao;
        this._has_cdOcorrenciaDiaExpiracao = true;
    } //-- void setCdOcorrenciaDiaExpiracao(int) 

    /**
     * Sets the value of field 'cdPermissaoContingencia'.
     * 
     * @param cdPermissaoContingencia the value of field
     * 'cdPermissaoContingencia'.
     */
    public void setCdPermissaoContingencia(int cdPermissaoContingencia)
    {
        this._cdPermissaoContingencia = cdPermissaoContingencia;
        this._has_cdPermissaoContingencia = true;
    } //-- void setCdPermissaoContingencia(int) 

    /**
     * Sets the value of field 'cdPermissaoDebitoOnline'.
     * 
     * @param cdPermissaoDebitoOnline the value of field
     * 'cdPermissaoDebitoOnline'.
     */
    public void setCdPermissaoDebitoOnline(int cdPermissaoDebitoOnline)
    {
        this._cdPermissaoDebitoOnline = cdPermissaoDebitoOnline;
        this._has_cdPermissaoDebitoOnline = true;
    } //-- void setCdPermissaoDebitoOnline(int) 

    /**
     * Sets the value of field
     * 'cdPermissaoFavorecidoConsultaPagto'.
     * 
     * @param cdPermissaoFavorecidoConsultaPagto the value of field
     * 'cdPermissaoFavorecidoConsultaPagto'.
     */
    public void setCdPermissaoFavorecidoConsultaPagto(int cdPermissaoFavorecidoConsultaPagto)
    {
        this._cdPermissaoFavorecidoConsultaPagto = cdPermissaoFavorecidoConsultaPagto;
        this._has_cdPermissaoFavorecidoConsultaPagto = true;
    } //-- void setCdPermissaoFavorecidoConsultaPagto(int) 

    /**
     * Sets the value of field 'cdPermissaoPagtoMenor'.
     * 
     * @param cdPermissaoPagtoMenor the value of field
     * 'cdPermissaoPagtoMenor'.
     */
    public void setCdPermissaoPagtoMenor(int cdPermissaoPagtoMenor)
    {
        this._cdPermissaoPagtoMenor = cdPermissaoPagtoMenor;
        this._has_cdPermissaoPagtoMenor = true;
    } //-- void setCdPermissaoPagtoMenor(int) 

    /**
     * Sets the value of field 'cdPermissaoPagtoVencido'.
     * 
     * @param cdPermissaoPagtoVencido the value of field
     * 'cdPermissaoPagtoVencido'.
     */
    public void setCdPermissaoPagtoVencido(int cdPermissaoPagtoVencido)
    {
        this._cdPermissaoPagtoVencido = cdPermissaoPagtoVencido;
        this._has_cdPermissaoPagtoVencido = true;
    } //-- void setCdPermissaoPagtoVencido(int) 

    /**
     * Sets the value of field 'cdPrioridadeDebito'.
     * 
     * @param cdPrioridadeDebito the value of field
     * 'cdPrioridadeDebito'.
     */
    public void setCdPrioridadeDebito(int cdPrioridadeDebito)
    {
        this._cdPrioridadeDebito = cdPrioridadeDebito;
        this._has_cdPrioridadeDebito = true;
    } //-- void setCdPrioridadeDebito(int) 

    /**
     * Sets the value of field 'cdRastreabNotaFiscal'.
     * 
     * @param cdRastreabNotaFiscal the value of field
     * 'cdRastreabNotaFiscal'.
     */
    public void setCdRastreabNotaFiscal(int cdRastreabNotaFiscal)
    {
        this._cdRastreabNotaFiscal = cdRastreabNotaFiscal;
        this._has_cdRastreabNotaFiscal = true;
    } //-- void setCdRastreabNotaFiscal(int) 

    /**
     * Sets the value of field 'cdRastreabTituloTerc'.
     * 
     * @param cdRastreabTituloTerc the value of field
     * 'cdRastreabTituloTerc'.
     */
    public void setCdRastreabTituloTerc(int cdRastreabTituloTerc)
    {
        this._cdRastreabTituloTerc = cdRastreabTituloTerc;
        this._has_cdRastreabTituloTerc = true;
    } //-- void setCdRastreabTituloTerc(int) 

    /**
     * Sets the value of field 'cdServico'.
     * 
     * @param cdServico the value of field 'cdServico'.
     */
    public void setCdServico(int cdServico)
    {
        this._cdServico = cdServico;
        this._has_cdServico = true;
    } //-- void setCdServico(int) 

    /**
     * Sets the value of field 'cdTipoComprovacaoVida'.
     * 
     * @param cdTipoComprovacaoVida the value of field
     * 'cdTipoComprovacaoVida'.
     */
    public void setCdTipoComprovacaoVida(int cdTipoComprovacaoVida)
    {
        this._cdTipoComprovacaoVida = cdTipoComprovacaoVida;
        this._has_cdTipoComprovacaoVida = true;
    } //-- void setCdTipoComprovacaoVida(int) 

    /**
     * Sets the value of field 'cdTipoConsistenciaCnpjFavorecido'.
     * 
     * @param cdTipoConsistenciaCnpjFavorecido the value of field
     * 'cdTipoConsistenciaCnpjFavorecido'.
     */
    public void setCdTipoConsistenciaCnpjFavorecido(int cdTipoConsistenciaCnpjFavorecido)
    {
        this._cdTipoConsistenciaCnpjFavorecido = cdTipoConsistenciaCnpjFavorecido;
        this._has_cdTipoConsistenciaCnpjFavorecido = true;
    } //-- void setCdTipoConsistenciaCnpjFavorecido(int) 

    /**
     * Sets the value of field 'cdTipoConsistenciaCnpjProposta'.
     * 
     * @param cdTipoConsistenciaCnpjProposta the value of field
     * 'cdTipoConsistenciaCnpjProposta'.
     */
    public void setCdTipoConsistenciaCnpjProposta(int cdTipoConsistenciaCnpjProposta)
    {
        this._cdTipoConsistenciaCnpjProposta = cdTipoConsistenciaCnpjProposta;
        this._has_cdTipoConsistenciaCnpjProposta = true;
    } //-- void setCdTipoConsistenciaCnpjProposta(int) 

    /**
     * Sets the value of field 'cdTipoConsultaSaldo'.
     * 
     * @param cdTipoConsultaSaldo the value of field
     * 'cdTipoConsultaSaldo'.
     */
    public void setCdTipoConsultaSaldo(int cdTipoConsultaSaldo)
    {
        this._cdTipoConsultaSaldo = cdTipoConsultaSaldo;
        this._has_cdTipoConsultaSaldo = true;
    } //-- void setCdTipoConsultaSaldo(int) 

    /**
     * Sets the value of field 'cdTipoContaCredito'.
     * 
     * @param cdTipoContaCredito the value of field
     * 'cdTipoContaCredito'.
     */
    public void setCdTipoContaCredito(long cdTipoContaCredito)
    {
        this._cdTipoContaCredito = cdTipoContaCredito;
        this._has_cdTipoContaCredito = true;
    } //-- void setCdTipoContaCredito(long) 

    /**
     * Sets the value of field 'cdTipoInscricaoFavorecidoAceita'.
     * 
     * @param cdTipoInscricaoFavorecidoAceita the value of field
     * 'cdTipoInscricaoFavorecidoAceita'.
     */
    public void setCdTipoInscricaoFavorecidoAceita(int cdTipoInscricaoFavorecidoAceita)
    {
        this._cdTipoInscricaoFavorecidoAceita = cdTipoInscricaoFavorecidoAceita;
        this._has_cdTipoInscricaoFavorecidoAceita = true;
    } //-- void setCdTipoInscricaoFavorecidoAceita(int) 

    /**
     * Sets the value of field 'cdTipoProcessamento'.
     * 
     * @param cdTipoProcessamento the value of field
     * 'cdTipoProcessamento'.
     */
    public void setCdTipoProcessamento(int cdTipoProcessamento)
    {
        this._cdTipoProcessamento = cdTipoProcessamento;
        this._has_cdTipoProcessamento = true;
    } //-- void setCdTipoProcessamento(int) 

    /**
     * Sets the value of field 'cdTipoRejeicaoAgendamento'.
     * 
     * @param cdTipoRejeicaoAgendamento the value of field
     * 'cdTipoRejeicaoAgendamento'.
     */
    public void setCdTipoRejeicaoAgendamento(int cdTipoRejeicaoAgendamento)
    {
        this._cdTipoRejeicaoAgendamento = cdTipoRejeicaoAgendamento;
        this._has_cdTipoRejeicaoAgendamento = true;
    } //-- void setCdTipoRejeicaoAgendamento(int) 

    /**
     * Sets the value of field 'cdTipoRejeicaoEfetivacao'.
     * 
     * @param cdTipoRejeicaoEfetivacao the value of field
     * 'cdTipoRejeicaoEfetivacao'.
     */
    public void setCdTipoRejeicaoEfetivacao(int cdTipoRejeicaoEfetivacao)
    {
        this._cdTipoRejeicaoEfetivacao = cdTipoRejeicaoEfetivacao;
        this._has_cdTipoRejeicaoEfetivacao = true;
    } //-- void setCdTipoRejeicaoEfetivacao(int) 

    /**
     * Sets the value of field 'cdTipoSaldo'.
     * 
     * @param cdTipoSaldo the value of field 'cdTipoSaldo'.
     */
    public void setCdTipoSaldo(int cdTipoSaldo)
    {
        this._cdTipoSaldo = cdTipoSaldo;
        this._has_cdTipoSaldo = true;
    } //-- void setCdTipoSaldo(int) 

    /**
     * Sets the value of field 'cdTipoSaldoValorSuperior'.
     * 
     * @param cdTipoSaldoValorSuperior the value of field
     * 'cdTipoSaldoValorSuperior'.
     */
    public void setCdTipoSaldoValorSuperior(int cdTipoSaldoValorSuperior)
    {
        this._cdTipoSaldoValorSuperior = cdTipoSaldoValorSuperior;
        this._has_cdTipoSaldoValorSuperior = true;
    } //-- void setCdTipoSaldoValorSuperior(int) 

    /**
     * Sets the value of field 'cdTratamentoFeriado'.
     * 
     * @param cdTratamentoFeriado the value of field
     * 'cdTratamentoFeriado'.
     */
    public void setCdTratamentoFeriado(int cdTratamentoFeriado)
    {
        this._cdTratamentoFeriado = cdTratamentoFeriado;
        this._has_cdTratamentoFeriado = true;
    } //-- void setCdTratamentoFeriado(int) 

    /**
     * Sets the value of field 'cdTratamentoFeriadoDataPagto'.
     * 
     * @param cdTratamentoFeriadoDataPagto the value of field
     * 'cdTratamentoFeriadoDataPagto'.
     */
    public void setCdTratamentoFeriadoDataPagto(int cdTratamentoFeriadoDataPagto)
    {
        this._cdTratamentoFeriadoDataPagto = cdTratamentoFeriadoDataPagto;
        this._has_cdTratamentoFeriadoDataPagto = true;
    } //-- void setCdTratamentoFeriadoDataPagto(int) 

    /**
     * Sets the value of field 'cdTratamentoValorDivergente'.
     * 
     * @param cdTratamentoValorDivergente the value of field
     * 'cdTratamentoValorDivergente'.
     */
    public void setCdTratamentoValorDivergente(int cdTratamentoValorDivergente)
    {
        this._cdTratamentoValorDivergente = cdTratamentoValorDivergente;
        this._has_cdTratamentoValorDivergente = true;
    } //-- void setCdTratamentoValorDivergente(int) 

    /**
     * Sets the value of field 'cdTratoContaTransf'.
     * 
     * @param cdTratoContaTransf the value of field
     * 'cdTratoContaTransf'.
     */
    public void setCdTratoContaTransf(int cdTratoContaTransf)
    {
        this._cdTratoContaTransf = cdTratoContaTransf;
        this._has_cdTratoContaTransf = true;
    } //-- void setCdTratoContaTransf(int) 

    /**
     * Sets the value of field 'cdUtilizacaoCodigoLancamentoFixo'.
     * 
     * @param cdUtilizacaoCodigoLancamentoFixo the value of field
     * 'cdUtilizacaoCodigoLancamentoFixo'.
     */
    public void setCdUtilizacaoCodigoLancamentoFixo(int cdUtilizacaoCodigoLancamentoFixo)
    {
        this._cdUtilizacaoCodigoLancamentoFixo = cdUtilizacaoCodigoLancamentoFixo;
        this._has_cdUtilizacaoCodigoLancamentoFixo = true;
    } //-- void setCdUtilizacaoCodigoLancamentoFixo(int) 

    /**
     * Sets the value of field
     * 'cdUtilizacaoFavorecidoControlePagto'.
     * 
     * @param cdUtilizacaoFavorecidoControlePagto the value of
     * field 'cdUtilizacaoFavorecidoControlePagto'.
     */
    public void setCdUtilizacaoFavorecidoControlePagto(int cdUtilizacaoFavorecidoControlePagto)
    {
        this._cdUtilizacaoFavorecidoControlePagto = cdUtilizacaoFavorecidoControlePagto;
        this._has_cdUtilizacaoFavorecidoControlePagto = true;
    } //-- void setCdUtilizacaoFavorecidoControlePagto(int) 

    /**
     * Sets the value of field 'cdUtilizacaoMensagemPersonalizada'.
     * 
     * @param cdUtilizacaoMensagemPersonalizada the value of field
     * 'cdUtilizacaoMensagemPersonalizada'.
     */
    public void setCdUtilizacaoMensagemPersonalizada(int cdUtilizacaoMensagemPersonalizada)
    {
        this._cdUtilizacaoMensagemPersonalizada = cdUtilizacaoMensagemPersonalizada;
        this._has_cdUtilizacaoMensagemPersonalizada = true;
    } //-- void setCdUtilizacaoMensagemPersonalizada(int) 

    /**
     * Sets the value of field 'cdValidacaoNomeFavorecido'.
     * 
     * @param cdValidacaoNomeFavorecido the value of field
     * 'cdValidacaoNomeFavorecido'.
     */
    public void setCdValidacaoNomeFavorecido(int cdValidacaoNomeFavorecido)
    {
        this._cdValidacaoNomeFavorecido = cdValidacaoNomeFavorecido;
        this._has_cdValidacaoNomeFavorecido = true;
    } //-- void setCdValidacaoNomeFavorecido(int) 

    /**
     * Sets the value of field
     * 'cdutilizacaoFormularioPersonalizado'.
     * 
     * @param cdutilizacaoFormularioPersonalizado the value of
     * field 'cdutilizacaoFormularioPersonalizado'.
     */
    public void setCdutilizacaoFormularioPersonalizado(int cdutilizacaoFormularioPersonalizado)
    {
        this._cdutilizacaoFormularioPersonalizado = cdutilizacaoFormularioPersonalizado;
        this._has_cdutilizacaoFormularioPersonalizado = true;
    } //-- void setCdutilizacaoFormularioPersonalizado(int) 

    /**
     * Sets the value of field 'dtInicioBloqueioPplta'.
     * 
     * @param dtInicioBloqueioPplta the value of field
     * 'dtInicioBloqueioPplta'.
     */
    public void setDtInicioBloqueioPplta(java.lang.String dtInicioBloqueioPplta)
    {
        this._dtInicioBloqueioPplta = dtInicioBloqueioPplta;
    } //-- void setDtInicioBloqueioPplta(java.lang.String) 

    /**
     * Sets the value of field 'dtInicioRastreabTitulo'.
     * 
     * @param dtInicioRastreabTitulo the value of field
     * 'dtInicioRastreabTitulo'.
     */
    public void setDtInicioRastreabTitulo(java.lang.String dtInicioRastreabTitulo)
    {
        this._dtInicioRastreabTitulo = dtInicioRastreabTitulo;
    } //-- void setDtInicioRastreabTitulo(java.lang.String) 

    /**
     * Sets the value of field 'dtInicioRegTitulo'.
     * 
     * @param dtInicioRegTitulo the value of field
     * 'dtInicioRegTitulo'.
     */
    public void setDtInicioRegTitulo(java.lang.String dtInicioRegTitulo)
    {
        this._dtInicioRegTitulo = dtInicioRegTitulo;
    } //-- void setDtInicioRegTitulo(java.lang.String) 

    /**
     * Sets the value of field 'percentualMaxRegInconsistenteLote'.
     * 
     * @param percentualMaxRegInconsistenteLote the value of field
     * 'percentualMaxRegInconsistenteLote'.
     */
    public void setPercentualMaxRegInconsistenteLote(int percentualMaxRegInconsistenteLote)
    {
        this._percentualMaxRegInconsistenteLote = percentualMaxRegInconsistenteLote;
        this._has_percentualMaxRegInconsistenteLote = true;
    } //-- void setPercentualMaxRegInconsistenteLote(int) 

    /**
     * Sets the value of field
     * 'qtdeAntecipacaoInicioRecadastramento'.
     * 
     * @param qtdeAntecipacaoInicioRecadastramento the value of
     * field 'qtdeAntecipacaoInicioRecadastramento'.
     */
    public void setQtdeAntecipacaoInicioRecadastramento(int qtdeAntecipacaoInicioRecadastramento)
    {
        this._qtdeAntecipacaoInicioRecadastramento = qtdeAntecipacaoInicioRecadastramento;
        this._has_qtdeAntecipacaoInicioRecadastramento = true;
    } //-- void setQtdeAntecipacaoInicioRecadastramento(int) 

    /**
     * Sets the value of field
     * 'qtdeDiaAvisoComprovanteVidaInicial'.
     * 
     * @param qtdeDiaAvisoComprovanteVidaInicial the value of field
     * 'qtdeDiaAvisoComprovanteVidaInicial'.
     */
    public void setQtdeDiaAvisoComprovanteVidaInicial(int qtdeDiaAvisoComprovanteVidaInicial)
    {
        this._qtdeDiaAvisoComprovanteVidaInicial = qtdeDiaAvisoComprovanteVidaInicial;
        this._has_qtdeDiaAvisoComprovanteVidaInicial = true;
    } //-- void setQtdeDiaAvisoComprovanteVidaInicial(int) 

    /**
     * Sets the value of field
     * 'qtdeDiaAvisoComprovanteVidaVencimento'.
     * 
     * @param qtdeDiaAvisoComprovanteVidaVencimento the value of
     * field 'qtdeDiaAvisoComprovanteVidaVencimento'.
     */
    public void setQtdeDiaAvisoComprovanteVidaVencimento(int qtdeDiaAvisoComprovanteVidaVencimento)
    {
        this._qtdeDiaAvisoComprovanteVidaVencimento = qtdeDiaAvisoComprovanteVidaVencimento;
        this._has_qtdeDiaAvisoComprovanteVidaVencimento = true;
    } //-- void setQtdeDiaAvisoComprovanteVidaVencimento(int) 

    /**
     * Sets the value of field 'qtdeDiaExpiracao'.
     * 
     * @param qtdeDiaExpiracao the value of field 'qtdeDiaExpiracao'
     */
    public void setQtdeDiaExpiracao(int qtdeDiaExpiracao)
    {
        this._qtdeDiaExpiracao = qtdeDiaExpiracao;
        this._has_qtdeDiaExpiracao = true;
    } //-- void setQtdeDiaExpiracao(int) 

    /**
     * Sets the value of field 'qtdeDiaFloating'.
     * 
     * @param qtdeDiaFloating the value of field 'qtdeDiaFloating'.
     */
    public void setQtdeDiaFloating(int qtdeDiaFloating)
    {
        this._qtdeDiaFloating = qtdeDiaFloating;
        this._has_qtdeDiaFloating = true;
    } //-- void setQtdeDiaFloating(int) 

    /**
     * Sets the value of field 'qtdeDiaRepique'.
     * 
     * @param qtdeDiaRepique the value of field 'qtdeDiaRepique'.
     */
    public void setQtdeDiaRepique(int qtdeDiaRepique)
    {
        this._qtdeDiaRepique = qtdeDiaRepique;
        this._has_qtdeDiaRepique = true;
    } //-- void setQtdeDiaRepique(int) 

    /**
     * Sets the value of field 'qtdeLimiteDiarioPagtoVencido'.
     * 
     * @param qtdeLimiteDiarioPagtoVencido the value of field
     * 'qtdeLimiteDiarioPagtoVencido'.
     */
    public void setQtdeLimiteDiarioPagtoVencido(int qtdeLimiteDiarioPagtoVencido)
    {
        this._qtdeLimiteDiarioPagtoVencido = qtdeLimiteDiarioPagtoVencido;
        this._has_qtdeLimiteDiarioPagtoVencido = true;
    } //-- void setQtdeLimiteDiarioPagtoVencido(int) 

    /**
     * Sets the value of field 'qtdeMaxRegInconsistenteLote'.
     * 
     * @param qtdeMaxRegInconsistenteLote the value of field
     * 'qtdeMaxRegInconsistenteLote'.
     */
    public void setQtdeMaxRegInconsistenteLote(int qtdeMaxRegInconsistenteLote)
    {
        this._qtdeMaxRegInconsistenteLote = qtdeMaxRegInconsistenteLote;
        this._has_qtdeMaxRegInconsistenteLote = true;
    } //-- void setQtdeMaxRegInconsistenteLote(int) 

    /**
     * Sets the value of field 'qtdeMesesComprovacaoVida'.
     * 
     * @param qtdeMesesComprovacaoVida the value of field
     * 'qtdeMesesComprovacaoVida'.
     */
    public void setQtdeMesesComprovacaoVida(int qtdeMesesComprovacaoVida)
    {
        this._qtdeMesesComprovacaoVida = qtdeMesesComprovacaoVida;
        this._has_qtdeMesesComprovacaoVida = true;
    } //-- void setQtdeMesesComprovacaoVida(int) 

    /**
     * Sets the value of field 'vlrLimiteDiario'.
     * 
     * @param vlrLimiteDiario the value of field 'vlrLimiteDiario'.
     */
    public void setVlrLimiteDiario(java.math.BigDecimal vlrLimiteDiario)
    {
        this._vlrLimiteDiario = vlrLimiteDiario;
    } //-- void setVlrLimiteDiario(java.math.BigDecimal) 

    /**
     * Sets the value of field 'vlrLimiteIndividual'.
     * 
     * @param vlrLimiteIndividual the value of field
     * 'vlrLimiteIndividual'.
     */
    public void setVlrLimiteIndividual(java.math.BigDecimal vlrLimiteIndividual)
    {
        this._vlrLimiteIndividual = vlrLimiteIndividual;
    } //-- void setVlrLimiteIndividual(java.math.BigDecimal) 

    /**
     * Sets the value of field
     * 'vlrMaximoPagtoFavorecidoNaoCadastrado'.
     * 
     * @param vlrMaximoPagtoFavorecidoNaoCadastrado the value of
     * field 'vlrMaximoPagtoFavorecidoNaoCadastrado'.
     */
    public void setVlrMaximoPagtoFavorecidoNaoCadastrado(java.math.BigDecimal vlrMaximoPagtoFavorecidoNaoCadastrado)
    {
        this._vlrMaximoPagtoFavorecidoNaoCadastrado = vlrMaximoPagtoFavorecidoNaoCadastrado;
    } //-- void setVlrMaximoPagtoFavorecidoNaoCadastrado(java.math.BigDecimal) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return EfetuarValidacaoConfigAtribModalidadeRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.efetuarvalidacaoconfigatribmodalidade.request.EfetuarValidacaoConfigAtribModalidadeRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.efetuarvalidacaoconfigatribmodalidade.request.EfetuarValidacaoConfigAtribModalidadeRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.efetuarvalidacaoconfigatribmodalidade.request.EfetuarValidacaoConfigAtribModalidadeRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.efetuarvalidacaoconfigatribmodalidade.request.EfetuarValidacaoConfigAtribModalidadeRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
