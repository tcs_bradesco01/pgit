/*
 * Nome: br.com.bradesco.web.pgit.service.business.regsegregacessodados.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.regsegregacessodados.bean;

/**
 * Nome: ExcluirExcRegraSegregacaoEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ExcluirExcRegraSegregacaoEntradaDTO {
	
	/** Atributo codigoRegra. */
	private int codigoRegra;
	
	/** Atributo codigoExcecao. */
	private int codigoExcecao;
	
	/**
	 * Get: codigoExcecao.
	 *
	 * @return codigoExcecao
	 */
	public int getCodigoExcecao() {
		return codigoExcecao;
	}
	
	/**
	 * Set: codigoExcecao.
	 *
	 * @param codigoExcecao the codigo excecao
	 */
	public void setCodigoExcecao(int codigoExcecao) {
		this.codigoExcecao = codigoExcecao;
	}
	
	/**
	 * Get: codigoRegra.
	 *
	 * @return codigoRegra
	 */
	public int getCodigoRegra() {
		return codigoRegra;
	}
	
	/**
	 * Set: codigoRegra.
	 *
	 * @param codigoRegra the codigo regra
	 */
	public void setCodigoRegra(int codigoRegra) {
		this.codigoRegra = codigoRegra;
	}
	
	

}
