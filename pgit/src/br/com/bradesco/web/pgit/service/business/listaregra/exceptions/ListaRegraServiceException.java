/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PilotoIntranet/JavaSource/br/com/bradesco/web/pgit/service/business/listaregra/exceptions/ListaRegraServiceException.java,v $
 * $Id: ListaRegraServiceException.java,v 1.1 2009/05/22 18:35:57 corporate\marcio.alves Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: ListaRegraServiceException.java,v $
 * Revision 1.1  2009/05/22 18:35:57  corporate\marcio.alves
 * Adicionando o servico listaregra para teste do pdc
 *
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */
 
package br.com.bradesco.web.pgit.service.business.listaregra.exceptions;
import br.com.bradesco.web.aq.application.error.BradescoApplicationException;

/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Classe do tipo exce�ao do adaptador: ListaRegra
 * </p>
 * 
 * @comment C�DIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public class ListaRegraServiceException extends BradescoApplicationException  {

	/**
	 * Lista regra service exception.
	 */
	public ListaRegraServiceException() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * Lista regra service exception.
	 *
	 * @param message the message
	 * @param code the code
	 */
	public ListaRegraServiceException(String message, String code) {
		super(message, code);
		// TODO Auto-generated constructor stub
	}

	/**
	 * Lista regra service exception.
	 *
	 * @param message the message
	 * @param cause the cause
	 * @param code the code
	 */
	public ListaRegraServiceException(String message, Throwable cause, String code) {
		super(message, cause, code);
		// TODO Auto-generated constructor stub
	}

	/**
	 * Lista regra service exception.
	 *
	 * @param message the message
	 * @param cause the cause
	 */
	public ListaRegraServiceException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	/**
	 * Lista regra service exception.
	 *
	 * @param message the message
	 */
	public ListaRegraServiceException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	/**
	 * Lista regra service exception.
	 *
	 * @param cause the cause
	 * @param code the code
	 */
	public ListaRegraServiceException(Throwable cause, String code) {
		super(cause, code);
		// TODO Auto-generated constructor stub
	}

	/**
	 * Lista regra service exception.
	 *
	 * @param cause the cause
	 */
	public ListaRegraServiceException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}

