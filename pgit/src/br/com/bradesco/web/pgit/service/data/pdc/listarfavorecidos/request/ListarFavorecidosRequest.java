/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listarfavorecidos.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ListarFavorecidosRequest.
 * 
 * @version $Revision$ $Date$
 */
public class ListarFavorecidosRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _numeroOcorrencias
     */
    private int _numeroOcorrencias = 0;

    /**
     * keeps track of state for field: _numeroOcorrencias
     */
    private boolean _has_numeroOcorrencias;

    /**
     * Field _cdPessoaJuridicaContrato
     */
    private long _cdPessoaJuridicaContrato = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridicaContrato
     */
    private boolean _has_cdPessoaJuridicaContrato;

    /**
     * Field _cdTipoContratoNegocio
     */
    private int _cdTipoContratoNegocio = 0;

    /**
     * keeps track of state for field: _cdTipoContratoNegocio
     */
    private boolean _has_cdTipoContratoNegocio;

    /**
     * Field _nrSequencialContratoNegocio
     */
    private long _nrSequencialContratoNegocio = 0;

    /**
     * keeps track of state for field: _nrSequencialContratoNegocio
     */
    private boolean _has_nrSequencialContratoNegocio;

    /**
     * Field _nmFavorecido
     */
    private java.lang.String _nmFavorecido;

    /**
     * Field _cdFavorecidoClientePagador
     */
    private long _cdFavorecidoClientePagador = 0;

    /**
     * keeps track of state for field: _cdFavorecidoClientePagador
     */
    private boolean _has_cdFavorecidoClientePagador;

    /**
     * Field _cdTipoFavorecido
     */
    private int _cdTipoFavorecido = 0;

    /**
     * keeps track of state for field: _cdTipoFavorecido
     */
    private boolean _has_cdTipoFavorecido;

    /**
     * Field _cdInscricaoFavorecidoCliente
     */
    private long _cdInscricaoFavorecidoCliente = 0;

    /**
     * keeps track of state for field: _cdInscricaoFavorecidoCliente
     */
    private boolean _has_cdInscricaoFavorecidoCliente;

    /**
     * Field _cdTipoInscricaoFavorecido
     */
    private int _cdTipoInscricaoFavorecido = 0;

    /**
     * keeps track of state for field: _cdTipoInscricaoFavorecido
     */
    private boolean _has_cdTipoInscricaoFavorecido;

    /**
     * Field _cdSituacaoFavorecidoCliente
     */
    private int _cdSituacaoFavorecidoCliente = 0;

    /**
     * keeps track of state for field: _cdSituacaoFavorecidoCliente
     */
    private boolean _has_cdSituacaoFavorecidoCliente;


      //----------------/
     //- Constructors -/
    //----------------/

    public ListarFavorecidosRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarfavorecidos.request.ListarFavorecidosRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdFavorecidoClientePagador
     * 
     */
    public void deleteCdFavorecidoClientePagador()
    {
        this._has_cdFavorecidoClientePagador= false;
    } //-- void deleteCdFavorecidoClientePagador() 

    /**
     * Method deleteCdInscricaoFavorecidoCliente
     * 
     */
    public void deleteCdInscricaoFavorecidoCliente()
    {
        this._has_cdInscricaoFavorecidoCliente= false;
    } //-- void deleteCdInscricaoFavorecidoCliente() 

    /**
     * Method deleteCdPessoaJuridicaContrato
     * 
     */
    public void deleteCdPessoaJuridicaContrato()
    {
        this._has_cdPessoaJuridicaContrato= false;
    } //-- void deleteCdPessoaJuridicaContrato() 

    /**
     * Method deleteCdSituacaoFavorecidoCliente
     * 
     */
    public void deleteCdSituacaoFavorecidoCliente()
    {
        this._has_cdSituacaoFavorecidoCliente= false;
    } //-- void deleteCdSituacaoFavorecidoCliente() 

    /**
     * Method deleteCdTipoContratoNegocio
     * 
     */
    public void deleteCdTipoContratoNegocio()
    {
        this._has_cdTipoContratoNegocio= false;
    } //-- void deleteCdTipoContratoNegocio() 

    /**
     * Method deleteCdTipoFavorecido
     * 
     */
    public void deleteCdTipoFavorecido()
    {
        this._has_cdTipoFavorecido= false;
    } //-- void deleteCdTipoFavorecido() 

    /**
     * Method deleteCdTipoInscricaoFavorecido
     * 
     */
    public void deleteCdTipoInscricaoFavorecido()
    {
        this._has_cdTipoInscricaoFavorecido= false;
    } //-- void deleteCdTipoInscricaoFavorecido() 

    /**
     * Method deleteNrSequencialContratoNegocio
     * 
     */
    public void deleteNrSequencialContratoNegocio()
    {
        this._has_nrSequencialContratoNegocio= false;
    } //-- void deleteNrSequencialContratoNegocio() 

    /**
     * Method deleteNumeroOcorrencias
     * 
     */
    public void deleteNumeroOcorrencias()
    {
        this._has_numeroOcorrencias= false;
    } //-- void deleteNumeroOcorrencias() 

    /**
     * Returns the value of field 'cdFavorecidoClientePagador'.
     * 
     * @return long
     * @return the value of field 'cdFavorecidoClientePagador'.
     */
    public long getCdFavorecidoClientePagador()
    {
        return this._cdFavorecidoClientePagador;
    } //-- long getCdFavorecidoClientePagador() 

    /**
     * Returns the value of field 'cdInscricaoFavorecidoCliente'.
     * 
     * @return long
     * @return the value of field 'cdInscricaoFavorecidoCliente'.
     */
    public long getCdInscricaoFavorecidoCliente()
    {
        return this._cdInscricaoFavorecidoCliente;
    } //-- long getCdInscricaoFavorecidoCliente() 

    /**
     * Returns the value of field 'cdPessoaJuridicaContrato'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridicaContrato'.
     */
    public long getCdPessoaJuridicaContrato()
    {
        return this._cdPessoaJuridicaContrato;
    } //-- long getCdPessoaJuridicaContrato() 

    /**
     * Returns the value of field 'cdSituacaoFavorecidoCliente'.
     * 
     * @return int
     * @return the value of field 'cdSituacaoFavorecidoCliente'.
     */
    public int getCdSituacaoFavorecidoCliente()
    {
        return this._cdSituacaoFavorecidoCliente;
    } //-- int getCdSituacaoFavorecidoCliente() 

    /**
     * Returns the value of field 'cdTipoContratoNegocio'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoNegocio'.
     */
    public int getCdTipoContratoNegocio()
    {
        return this._cdTipoContratoNegocio;
    } //-- int getCdTipoContratoNegocio() 

    /**
     * Returns the value of field 'cdTipoFavorecido'.
     * 
     * @return int
     * @return the value of field 'cdTipoFavorecido'.
     */
    public int getCdTipoFavorecido()
    {
        return this._cdTipoFavorecido;
    } //-- int getCdTipoFavorecido() 

    /**
     * Returns the value of field 'cdTipoInscricaoFavorecido'.
     * 
     * @return int
     * @return the value of field 'cdTipoInscricaoFavorecido'.
     */
    public int getCdTipoInscricaoFavorecido()
    {
        return this._cdTipoInscricaoFavorecido;
    } //-- int getCdTipoInscricaoFavorecido() 

    /**
     * Returns the value of field 'nmFavorecido'.
     * 
     * @return String
     * @return the value of field 'nmFavorecido'.
     */
    public java.lang.String getNmFavorecido()
    {
        return this._nmFavorecido;
    } //-- java.lang.String getNmFavorecido() 

    /**
     * Returns the value of field 'nrSequencialContratoNegocio'.
     * 
     * @return long
     * @return the value of field 'nrSequencialContratoNegocio'.
     */
    public long getNrSequencialContratoNegocio()
    {
        return this._nrSequencialContratoNegocio;
    } //-- long getNrSequencialContratoNegocio() 

    /**
     * Returns the value of field 'numeroOcorrencias'.
     * 
     * @return int
     * @return the value of field 'numeroOcorrencias'.
     */
    public int getNumeroOcorrencias()
    {
        return this._numeroOcorrencias;
    } //-- int getNumeroOcorrencias() 

    /**
     * Method hasCdFavorecidoClientePagador
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFavorecidoClientePagador()
    {
        return this._has_cdFavorecidoClientePagador;
    } //-- boolean hasCdFavorecidoClientePagador() 

    /**
     * Method hasCdInscricaoFavorecidoCliente
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdInscricaoFavorecidoCliente()
    {
        return this._has_cdInscricaoFavorecidoCliente;
    } //-- boolean hasCdInscricaoFavorecidoCliente() 

    /**
     * Method hasCdPessoaJuridicaContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridicaContrato()
    {
        return this._has_cdPessoaJuridicaContrato;
    } //-- boolean hasCdPessoaJuridicaContrato() 

    /**
     * Method hasCdSituacaoFavorecidoCliente
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSituacaoFavorecidoCliente()
    {
        return this._has_cdSituacaoFavorecidoCliente;
    } //-- boolean hasCdSituacaoFavorecidoCliente() 

    /**
     * Method hasCdTipoContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoNegocio()
    {
        return this._has_cdTipoContratoNegocio;
    } //-- boolean hasCdTipoContratoNegocio() 

    /**
     * Method hasCdTipoFavorecido
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoFavorecido()
    {
        return this._has_cdTipoFavorecido;
    } //-- boolean hasCdTipoFavorecido() 

    /**
     * Method hasCdTipoInscricaoFavorecido
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoInscricaoFavorecido()
    {
        return this._has_cdTipoInscricaoFavorecido;
    } //-- boolean hasCdTipoInscricaoFavorecido() 

    /**
     * Method hasNrSequencialContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequencialContratoNegocio()
    {
        return this._has_nrSequencialContratoNegocio;
    } //-- boolean hasNrSequencialContratoNegocio() 

    /**
     * Method hasNumeroOcorrencias
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNumeroOcorrencias()
    {
        return this._has_numeroOcorrencias;
    } //-- boolean hasNumeroOcorrencias() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdFavorecidoClientePagador'.
     * 
     * @param cdFavorecidoClientePagador the value of field
     * 'cdFavorecidoClientePagador'.
     */
    public void setCdFavorecidoClientePagador(long cdFavorecidoClientePagador)
    {
        this._cdFavorecidoClientePagador = cdFavorecidoClientePagador;
        this._has_cdFavorecidoClientePagador = true;
    } //-- void setCdFavorecidoClientePagador(long) 

    /**
     * Sets the value of field 'cdInscricaoFavorecidoCliente'.
     * 
     * @param cdInscricaoFavorecidoCliente the value of field
     * 'cdInscricaoFavorecidoCliente'.
     */
    public void setCdInscricaoFavorecidoCliente(long cdInscricaoFavorecidoCliente)
    {
        this._cdInscricaoFavorecidoCliente = cdInscricaoFavorecidoCliente;
        this._has_cdInscricaoFavorecidoCliente = true;
    } //-- void setCdInscricaoFavorecidoCliente(long) 

    /**
     * Sets the value of field 'cdPessoaJuridicaContrato'.
     * 
     * @param cdPessoaJuridicaContrato the value of field
     * 'cdPessoaJuridicaContrato'.
     */
    public void setCdPessoaJuridicaContrato(long cdPessoaJuridicaContrato)
    {
        this._cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
        this._has_cdPessoaJuridicaContrato = true;
    } //-- void setCdPessoaJuridicaContrato(long) 

    /**
     * Sets the value of field 'cdSituacaoFavorecidoCliente'.
     * 
     * @param cdSituacaoFavorecidoCliente the value of field
     * 'cdSituacaoFavorecidoCliente'.
     */
    public void setCdSituacaoFavorecidoCliente(int cdSituacaoFavorecidoCliente)
    {
        this._cdSituacaoFavorecidoCliente = cdSituacaoFavorecidoCliente;
        this._has_cdSituacaoFavorecidoCliente = true;
    } //-- void setCdSituacaoFavorecidoCliente(int) 

    /**
     * Sets the value of field 'cdTipoContratoNegocio'.
     * 
     * @param cdTipoContratoNegocio the value of field
     * 'cdTipoContratoNegocio'.
     */
    public void setCdTipoContratoNegocio(int cdTipoContratoNegocio)
    {
        this._cdTipoContratoNegocio = cdTipoContratoNegocio;
        this._has_cdTipoContratoNegocio = true;
    } //-- void setCdTipoContratoNegocio(int) 

    /**
     * Sets the value of field 'cdTipoFavorecido'.
     * 
     * @param cdTipoFavorecido the value of field 'cdTipoFavorecido'
     */
    public void setCdTipoFavorecido(int cdTipoFavorecido)
    {
        this._cdTipoFavorecido = cdTipoFavorecido;
        this._has_cdTipoFavorecido = true;
    } //-- void setCdTipoFavorecido(int) 

    /**
     * Sets the value of field 'cdTipoInscricaoFavorecido'.
     * 
     * @param cdTipoInscricaoFavorecido the value of field
     * 'cdTipoInscricaoFavorecido'.
     */
    public void setCdTipoInscricaoFavorecido(int cdTipoInscricaoFavorecido)
    {
        this._cdTipoInscricaoFavorecido = cdTipoInscricaoFavorecido;
        this._has_cdTipoInscricaoFavorecido = true;
    } //-- void setCdTipoInscricaoFavorecido(int) 

    /**
     * Sets the value of field 'nmFavorecido'.
     * 
     * @param nmFavorecido the value of field 'nmFavorecido'.
     */
    public void setNmFavorecido(java.lang.String nmFavorecido)
    {
        this._nmFavorecido = nmFavorecido;
    } //-- void setNmFavorecido(java.lang.String) 

    /**
     * Sets the value of field 'nrSequencialContratoNegocio'.
     * 
     * @param nrSequencialContratoNegocio the value of field
     * 'nrSequencialContratoNegocio'.
     */
    public void setNrSequencialContratoNegocio(long nrSequencialContratoNegocio)
    {
        this._nrSequencialContratoNegocio = nrSequencialContratoNegocio;
        this._has_nrSequencialContratoNegocio = true;
    } //-- void setNrSequencialContratoNegocio(long) 

    /**
     * Sets the value of field 'numeroOcorrencias'.
     * 
     * @param numeroOcorrencias the value of field
     * 'numeroOcorrencias'.
     */
    public void setNumeroOcorrencias(int numeroOcorrencias)
    {
        this._numeroOcorrencias = numeroOcorrencias;
        this._has_numeroOcorrencias = true;
    } //-- void setNumeroOcorrencias(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ListarFavorecidosRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listarfavorecidos.request.ListarFavorecidosRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listarfavorecidos.request.ListarFavorecidosRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listarfavorecidos.request.ListarFavorecidosRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarfavorecidos.request.ListarFavorecidosRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
