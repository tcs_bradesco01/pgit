/*
 * Nome: br.com.bradesco.web.pgit.service.business.bloqueardesbloquearcontratos.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.bloqueardesbloquearcontratos.bean;

/**
 * Nome: BloquearDesbloquearContratosEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class BloquearDesbloquearContratosEntradaDTO {
	
	/** Atributo cdMotivoBloqueioContrato. */
	private int cdMotivoBloqueioContrato;
	
	/** Atributo cdPessoaJuridica. */
	private long cdPessoaJuridica;
	
	/** Atributo cdSituacaoContrato. */
	private int cdSituacaoContrato;
	
	/** Atributo cdTipoContratoNegocio. */
	private int cdTipoContratoNegocio;
	
	/** Atributo indicadorBloqueioDesbloqueioContrato. */
	private String indicadorBloqueioDesbloqueioContrato;
	
	/** Atributo nrSequenciaContratoNegocio. */
	private long nrSequenciaContratoNegocio;
	
	/**
	 * Get: cdMotivoBloqueioContrato.
	 *
	 * @return cdMotivoBloqueioContrato
	 */
	public int getCdMotivoBloqueioContrato() {
		return cdMotivoBloqueioContrato;
	}
	
	/**
	 * Set: cdMotivoBloqueioContrato.
	 *
	 * @param cdMotivoBloqueioContrato the cd motivo bloqueio contrato
	 */
	public void setCdMotivoBloqueioContrato(int cdMotivoBloqueioContrato) {
		this.cdMotivoBloqueioContrato = cdMotivoBloqueioContrato;
	}
	
	/**
	 * Get: cdPessoaJuridica.
	 *
	 * @return cdPessoaJuridica
	 */
	public long getCdPessoaJuridica() {
		return cdPessoaJuridica;
	}
	
	/**
	 * Set: cdPessoaJuridica.
	 *
	 * @param cdPessoaJuridica the cd pessoa juridica
	 */
	public void setCdPessoaJuridica(long cdPessoaJuridica) {
		this.cdPessoaJuridica = cdPessoaJuridica;
	}
	
	/**
	 * Get: cdSituacaoContrato.
	 *
	 * @return cdSituacaoContrato
	 */
	public int getCdSituacaoContrato() {
		return cdSituacaoContrato;
	}
	
	/**
	 * Set: cdSituacaoContrato.
	 *
	 * @param cdSituacaoContrato the cd situacao contrato
	 */
	public void setCdSituacaoContrato(int cdSituacaoContrato) {
		this.cdSituacaoContrato = cdSituacaoContrato;
	}
	
	/**
	 * Get: cdTipoContratoNegocio.
	 *
	 * @return cdTipoContratoNegocio
	 */
	public int getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}
	
	/**
	 * Set: cdTipoContratoNegocio.
	 *
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(int cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}
	
	/**
	 * Get: indicadorBloqueioDesbloqueioContrato.
	 *
	 * @return indicadorBloqueioDesbloqueioContrato
	 */
	public String getIndicadorBloqueioDesbloqueioContrato() {
		return indicadorBloqueioDesbloqueioContrato;
	}
	
	/**
	 * Set: indicadorBloqueioDesbloqueioContrato.
	 *
	 * @param indicadorBloqueioDesbloqueioContrato the indicador bloqueio desbloqueio contrato
	 */
	public void setIndicadorBloqueioDesbloqueioContrato(
			String indicadorBloqueioDesbloqueioContrato) {
		this.indicadorBloqueioDesbloqueioContrato = indicadorBloqueioDesbloqueioContrato;
	}
	
	/**
	 * Get: nrSequenciaContratoNegocio.
	 *
	 * @return nrSequenciaContratoNegocio
	 */
	public long getNrSequenciaContratoNegocio() {
		return nrSequenciaContratoNegocio;
	}
	
	/**
	 * Set: nrSequenciaContratoNegocio.
	 *
	 * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
	 */
	public void setNrSequenciaContratoNegocio(long nrSequenciaContratoNegocio) {
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}

}
