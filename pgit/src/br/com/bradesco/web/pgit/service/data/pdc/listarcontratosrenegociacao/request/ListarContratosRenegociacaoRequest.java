/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listarcontratosrenegociacao.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ListarContratosRenegociacaoRequest.
 * 
 * @version $Revision$ $Date$
 */
public class ListarContratosRenegociacaoRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _nrOcorrencias
     */
    private int _nrOcorrencias = 0;

    /**
     * keeps track of state for field: _nrOcorrencias
     */
    private boolean _has_nrOcorrencias;

    /**
     * Field _cdClubPessoa
     */
    private long _cdClubPessoa = 0;

    /**
     * keeps track of state for field: _cdClubPessoa
     */
    private boolean _has_cdClubPessoa;

    /**
     * Field _cdPessoaJuridica
     */
    private long _cdPessoaJuridica = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridica
     */
    private boolean _has_cdPessoaJuridica;

    /**
     * Field _cdTipoContrato
     */
    private int _cdTipoContrato = 0;

    /**
     * keeps track of state for field: _cdTipoContrato
     */
    private boolean _has_cdTipoContrato;

    /**
     * Field _nrSequenciaContrato
     */
    private long _nrSequenciaContrato = 0;

    /**
     * keeps track of state for field: _nrSequenciaContrato
     */
    private boolean _has_nrSequenciaContrato;

    /**
     * Field _cdCpfCnpjPssoa
     */
    private long _cdCpfCnpjPssoa = 0;

    /**
     * keeps track of state for field: _cdCpfCnpjPssoa
     */
    private boolean _has_cdCpfCnpjPssoa;

    /**
     * Field _cdFilialCnpjPssoa
     */
    private int _cdFilialCnpjPssoa = 0;

    /**
     * keeps track of state for field: _cdFilialCnpjPssoa
     */
    private boolean _has_cdFilialCnpjPssoa;

    /**
     * Field _cdControleCpfPssoa
     */
    private int _cdControleCpfPssoa = 0;

    /**
     * keeps track of state for field: _cdControleCpfPssoa
     */
    private boolean _has_cdControleCpfPssoa;


      //----------------/
     //- Constructors -/
    //----------------/

    public ListarContratosRenegociacaoRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarcontratosrenegociacao.request.ListarContratosRenegociacaoRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdClubPessoa
     * 
     */
    public void deleteCdClubPessoa()
    {
        this._has_cdClubPessoa= false;
    } //-- void deleteCdClubPessoa() 

    /**
     * Method deleteCdControleCpfPssoa
     * 
     */
    public void deleteCdControleCpfPssoa()
    {
        this._has_cdControleCpfPssoa= false;
    } //-- void deleteCdControleCpfPssoa() 

    /**
     * Method deleteCdCpfCnpjPssoa
     * 
     */
    public void deleteCdCpfCnpjPssoa()
    {
        this._has_cdCpfCnpjPssoa= false;
    } //-- void deleteCdCpfCnpjPssoa() 

    /**
     * Method deleteCdFilialCnpjPssoa
     * 
     */
    public void deleteCdFilialCnpjPssoa()
    {
        this._has_cdFilialCnpjPssoa= false;
    } //-- void deleteCdFilialCnpjPssoa() 

    /**
     * Method deleteCdPessoaJuridica
     * 
     */
    public void deleteCdPessoaJuridica()
    {
        this._has_cdPessoaJuridica= false;
    } //-- void deleteCdPessoaJuridica() 

    /**
     * Method deleteCdTipoContrato
     * 
     */
    public void deleteCdTipoContrato()
    {
        this._has_cdTipoContrato= false;
    } //-- void deleteCdTipoContrato() 

    /**
     * Method deleteNrOcorrencias
     * 
     */
    public void deleteNrOcorrencias()
    {
        this._has_nrOcorrencias= false;
    } //-- void deleteNrOcorrencias() 

    /**
     * Method deleteNrSequenciaContrato
     * 
     */
    public void deleteNrSequenciaContrato()
    {
        this._has_nrSequenciaContrato= false;
    } //-- void deleteNrSequenciaContrato() 

    /**
     * Returns the value of field 'cdClubPessoa'.
     * 
     * @return long
     * @return the value of field 'cdClubPessoa'.
     */
    public long getCdClubPessoa()
    {
        return this._cdClubPessoa;
    } //-- long getCdClubPessoa() 

    /**
     * Returns the value of field 'cdControleCpfPssoa'.
     * 
     * @return int
     * @return the value of field 'cdControleCpfPssoa'.
     */
    public int getCdControleCpfPssoa()
    {
        return this._cdControleCpfPssoa;
    } //-- int getCdControleCpfPssoa() 

    /**
     * Returns the value of field 'cdCpfCnpjPssoa'.
     * 
     * @return long
     * @return the value of field 'cdCpfCnpjPssoa'.
     */
    public long getCdCpfCnpjPssoa()
    {
        return this._cdCpfCnpjPssoa;
    } //-- long getCdCpfCnpjPssoa() 

    /**
     * Returns the value of field 'cdFilialCnpjPssoa'.
     * 
     * @return int
     * @return the value of field 'cdFilialCnpjPssoa'.
     */
    public int getCdFilialCnpjPssoa()
    {
        return this._cdFilialCnpjPssoa;
    } //-- int getCdFilialCnpjPssoa() 

    /**
     * Returns the value of field 'cdPessoaJuridica'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridica'.
     */
    public long getCdPessoaJuridica()
    {
        return this._cdPessoaJuridica;
    } //-- long getCdPessoaJuridica() 

    /**
     * Returns the value of field 'cdTipoContrato'.
     * 
     * @return int
     * @return the value of field 'cdTipoContrato'.
     */
    public int getCdTipoContrato()
    {
        return this._cdTipoContrato;
    } //-- int getCdTipoContrato() 

    /**
     * Returns the value of field 'nrOcorrencias'.
     * 
     * @return int
     * @return the value of field 'nrOcorrencias'.
     */
    public int getNrOcorrencias()
    {
        return this._nrOcorrencias;
    } //-- int getNrOcorrencias() 

    /**
     * Returns the value of field 'nrSequenciaContrato'.
     * 
     * @return long
     * @return the value of field 'nrSequenciaContrato'.
     */
    public long getNrSequenciaContrato()
    {
        return this._nrSequenciaContrato;
    } //-- long getNrSequenciaContrato() 

    /**
     * Method hasCdClubPessoa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdClubPessoa()
    {
        return this._has_cdClubPessoa;
    } //-- boolean hasCdClubPessoa() 

    /**
     * Method hasCdControleCpfPssoa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdControleCpfPssoa()
    {
        return this._has_cdControleCpfPssoa;
    } //-- boolean hasCdControleCpfPssoa() 

    /**
     * Method hasCdCpfCnpjPssoa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCpfCnpjPssoa()
    {
        return this._has_cdCpfCnpjPssoa;
    } //-- boolean hasCdCpfCnpjPssoa() 

    /**
     * Method hasCdFilialCnpjPssoa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFilialCnpjPssoa()
    {
        return this._has_cdFilialCnpjPssoa;
    } //-- boolean hasCdFilialCnpjPssoa() 

    /**
     * Method hasCdPessoaJuridica
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridica()
    {
        return this._has_cdPessoaJuridica;
    } //-- boolean hasCdPessoaJuridica() 

    /**
     * Method hasCdTipoContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContrato()
    {
        return this._has_cdTipoContrato;
    } //-- boolean hasCdTipoContrato() 

    /**
     * Method hasNrOcorrencias
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrOcorrencias()
    {
        return this._has_nrOcorrencias;
    } //-- boolean hasNrOcorrencias() 

    /**
     * Method hasNrSequenciaContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaContrato()
    {
        return this._has_nrSequenciaContrato;
    } //-- boolean hasNrSequenciaContrato() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdClubPessoa'.
     * 
     * @param cdClubPessoa the value of field 'cdClubPessoa'.
     */
    public void setCdClubPessoa(long cdClubPessoa)
    {
        this._cdClubPessoa = cdClubPessoa;
        this._has_cdClubPessoa = true;
    } //-- void setCdClubPessoa(long) 

    /**
     * Sets the value of field 'cdControleCpfPssoa'.
     * 
     * @param cdControleCpfPssoa the value of field
     * 'cdControleCpfPssoa'.
     */
    public void setCdControleCpfPssoa(int cdControleCpfPssoa)
    {
        this._cdControleCpfPssoa = cdControleCpfPssoa;
        this._has_cdControleCpfPssoa = true;
    } //-- void setCdControleCpfPssoa(int) 

    /**
     * Sets the value of field 'cdCpfCnpjPssoa'.
     * 
     * @param cdCpfCnpjPssoa the value of field 'cdCpfCnpjPssoa'.
     */
    public void setCdCpfCnpjPssoa(long cdCpfCnpjPssoa)
    {
        this._cdCpfCnpjPssoa = cdCpfCnpjPssoa;
        this._has_cdCpfCnpjPssoa = true;
    } //-- void setCdCpfCnpjPssoa(long) 

    /**
     * Sets the value of field 'cdFilialCnpjPssoa'.
     * 
     * @param cdFilialCnpjPssoa the value of field
     * 'cdFilialCnpjPssoa'.
     */
    public void setCdFilialCnpjPssoa(int cdFilialCnpjPssoa)
    {
        this._cdFilialCnpjPssoa = cdFilialCnpjPssoa;
        this._has_cdFilialCnpjPssoa = true;
    } //-- void setCdFilialCnpjPssoa(int) 

    /**
     * Sets the value of field 'cdPessoaJuridica'.
     * 
     * @param cdPessoaJuridica the value of field 'cdPessoaJuridica'
     */
    public void setCdPessoaJuridica(long cdPessoaJuridica)
    {
        this._cdPessoaJuridica = cdPessoaJuridica;
        this._has_cdPessoaJuridica = true;
    } //-- void setCdPessoaJuridica(long) 

    /**
     * Sets the value of field 'cdTipoContrato'.
     * 
     * @param cdTipoContrato the value of field 'cdTipoContrato'.
     */
    public void setCdTipoContrato(int cdTipoContrato)
    {
        this._cdTipoContrato = cdTipoContrato;
        this._has_cdTipoContrato = true;
    } //-- void setCdTipoContrato(int) 

    /**
     * Sets the value of field 'nrOcorrencias'.
     * 
     * @param nrOcorrencias the value of field 'nrOcorrencias'.
     */
    public void setNrOcorrencias(int nrOcorrencias)
    {
        this._nrOcorrencias = nrOcorrencias;
        this._has_nrOcorrencias = true;
    } //-- void setNrOcorrencias(int) 

    /**
     * Sets the value of field 'nrSequenciaContrato'.
     * 
     * @param nrSequenciaContrato the value of field
     * 'nrSequenciaContrato'.
     */
    public void setNrSequenciaContrato(long nrSequenciaContrato)
    {
        this._nrSequenciaContrato = nrSequenciaContrato;
        this._has_nrSequenciaContrato = true;
    } //-- void setNrSequenciaContrato(long) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ListarContratosRenegociacaoRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listarcontratosrenegociacao.request.ListarContratosRenegociacaoRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listarcontratosrenegociacao.request.ListarContratosRenegociacaoRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listarcontratosrenegociacao.request.ListarContratosRenegociacaoRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarcontratosrenegociacao.request.ListarContratosRenegociacaoRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
