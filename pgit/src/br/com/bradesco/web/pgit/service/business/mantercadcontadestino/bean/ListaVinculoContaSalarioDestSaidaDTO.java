/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantercadcontadestino.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantercadcontadestino.bean;

import br.com.bradesco.web.pgit.service.data.pdc.listarvinculocontasalariodestino.response.Ocorrencias;
import br.com.bradesco.web.pgit.utils.PgitUtil;

/**
 * Nome: ListaVinculoContaSalarioDestSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListaVinculoContaSalarioDestSaidaDTO {

    /** Atributo cdPessoaJuridVinc. */
    private Long cdPessoaJuridVinc;

    /** Atributo cdTipoContratoVinc. */
    private Integer cdTipoContratoVinc;

    /** Atributo nrSeqContratoVinc. */
    private Long nrSeqContratoVinc;

    /** Atributo cdTipoVinculoContrato. */
    private Integer cdTipoVinculoContrato;

    /** Atributo cdBancoSalario. */
    private Integer cdBancoSalario;

    /** Atributo dsBancoSalario. */
    private String dsBancoSalario;

    /** Atributo cdAgenciaSalario. */
    private Integer cdAgenciaSalario;

    /** Atributo cdDigitoAgenciaSalario. */
    private Integer cdDigitoAgenciaSalario;

    /** Atributo dsAgenciaSalario. */
    private String dsAgenciaSalario;

    /** Atributo cdContaSalario. */
    private Long cdContaSalario;

    /** Atributo cdDigitoContaSalario. */
    private String cdDigitoContaSalario;

    /** Atributo cdBancoDestino. */
    private Integer cdBancoDestino;

    /** Atributo dsBancoDestino. */
    private String dsBancoDestino;

    /** Atributo cdAgenciaDestino. */
    private Integer cdAgenciaDestino;

    /** Atributo cdDigitoAgenciaDestino. */
    private Integer cdDigitoAgenciaDestino;

    /** Atributo dsAgenciaDestino. */
    private String dsAgenciaDestino;

    /** Atributo cdContaDestino. */
    private Long cdContaDestino;

    /** Atributo cdDigitoContaDestino. */
    private String cdDigitoContaDestino;

    /** Atributo cdTipoContaDestino. */
    private String cdTipoContaDestino;

    /** Atributo dsSituacaoContaDestino. */
    private String dsSituacaoContaDestino;

    /** Atributo cdContingenciaTed. */
    private String cdContingenciaTed;

    /** Atributo nmParticipante. */
    private String nmParticipante;

    /** Atributo cdCpfParticipante. */
    private String cdCpfParticipante;

    /**
     * Lista vinculo conta salario dest saida dto.
     *
     * @param saida the saida
     */
    public ListaVinculoContaSalarioDestSaidaDTO(Ocorrencias saida) {
	this.cdPessoaJuridVinc = saida.getCdPessoaJuridVinc();
	this.cdTipoContratoVinc = saida.getCdTipoContratoVinc();
	this.nrSeqContratoVinc = saida.getNrSeqContratoVinc();
	this.cdTipoVinculoContrato = saida.getCdTipoVinculoContrato();
	this.cdBancoSalario = saida.getCdBancoSalario();
	this.dsBancoSalario = saida.getDsBancoSalario();
	this.cdAgenciaSalario = saida.getCdAgenciaSalario();
	this.cdDigitoAgenciaSalario = saida.getCdDigitoAgenciaSalario();
	this.dsAgenciaSalario = saida.getDsAgenciaSalario();
	this.cdContaSalario = saida.getCdContaSalario();
	this.cdDigitoContaSalario = saida.getCdDigitoContaSalario();
	this.cdBancoDestino = saida.getCdBancoDestino();
	this.dsBancoDestino = saida.getDsBancoDestino();
	this.cdAgenciaDestino = saida.getCdAgenciaDestino();
	this.cdDigitoAgenciaDestino = saida.getCdDigitoAgenciaDestino();
	this.dsAgenciaDestino = saida.getDsAgenciaDestino();
	this.cdContaDestino = saida.getCdContaDestino();
	this.cdDigitoContaDestino = saida.getCdDigitoContaDestino();
	this.cdTipoContaDestino = saida.getCdTipoContaDestino();
	this.dsSituacaoContaDestino = saida.getDsSituacaoContaDestino();
	this.cdContingenciaTed = saida.getCdContingenciaTed();
	this.nmParticipante = saida.getNmParticipante();
	this.cdCpfParticipante = saida.getCdCpfParticipante();
    }

    /**
     * Get: contaDigitoDestino.
     *
     * @return contaDigitoDestino
     */
    public String getContaDigitoDestino() {
	if (cdDigitoContaDestino != null && !cdDigitoContaDestino.trim().equals("")) {
	    return cdContaDestino == 0 ? "" : cdContaDestino + "-" + this.cdDigitoContaDestino;
	} else {
	    return String.valueOf(cdContaDestino);
	}
    }

    /**
     * Get: contaDigitoSalario.
     *
     * @return contaDigitoSalario
     */
    public String getContaDigitoSalario() {
	if (cdDigitoContaSalario != null && !cdDigitoContaSalario.trim().equals("")) {
	    return cdContaSalario == 0 ? "" : cdContaSalario + "-" + this.cdDigitoContaSalario;
	} else {
	    return String.valueOf(cdContaSalario);
	}
    }

    /**
     * Get: bancoDestino.
     *
     * @return bancoDestino
     */
    public String getBancoDestino() {
	return dsBancoDestino.equals("") ? String.valueOf(cdBancoDestino) : cdBancoDestino == 0 ? "" : cdBancoDestino + " " + dsBancoDestino;
    }

    /**
     * Get: bancoSalario.
     *
     * @return bancoSalario
     */
    public String getBancoSalario() {
	if (cdBancoSalario != null && dsBancoSalario != null && !dsBancoSalario.trim().equals("")) {
	    return cdBancoSalario == 0 ? "" : cdBancoSalario + " " + dsBancoSalario;
	} else {
	    return cdBancoSalario == null || cdBancoSalario == 0 ? "" : String.valueOf(cdBancoSalario);
	}

    }

    /**
     * Get: agenciaDestino.
     *
     * @return agenciaDestino
     */
    public String getAgenciaDestino() {
	if (cdDigitoAgenciaDestino != null && dsAgenciaDestino != null && !dsAgenciaDestino.trim().equals("")) {
	    return cdAgenciaDestino == 0 ? "" : cdAgenciaDestino + "-" + cdDigitoAgenciaDestino + " " + dsAgenciaDestino;
	} else if (cdDigitoAgenciaDestino == null && dsAgenciaDestino != null && !dsAgenciaDestino.trim().equals("")) {
	    return cdAgenciaDestino == 0 ? "" : cdAgenciaDestino + " " + dsAgenciaDestino;
	} else if (cdDigitoAgenciaDestino != null) {
	    return cdAgenciaDestino == 0 ? "" : cdAgenciaDestino + "-" + cdDigitoAgenciaDestino;
	} else {
	    return String.valueOf(cdAgenciaDestino);
	}
    }

    /**
     * Get: agenciaSalario.
     *
     * @return agenciaSalario
     */
    public String getAgenciaSalario() {
	if (cdDigitoAgenciaSalario != null && dsAgenciaSalario != null && !dsAgenciaSalario.trim().equals("")) {
	    return cdAgenciaSalario == 0 ? "" : cdAgenciaSalario + "-" + cdDigitoAgenciaSalario + " " + dsAgenciaSalario;
	} else if (cdDigitoAgenciaSalario == null && dsAgenciaSalario != null && !dsAgenciaSalario.trim().equals("")) {
	    return cdAgenciaSalario == 0 ? "" : cdAgenciaSalario + " " + dsAgenciaSalario;
	} else if (cdDigitoAgenciaSalario != null) {
	    return cdAgenciaSalario == 0 ? "" : cdAgenciaSalario + "-" + cdDigitoAgenciaSalario;
	} else {
	    return String.valueOf(cdAgenciaSalario);
	}
    }

    /**
     * Get: cdContingenciaTed.
     *
     * @return cdContingenciaTed
     */
    public String getCdContingenciaTed() {
	return cdContingenciaTed;
    }

    /**
     * Set: cdContingenciaTed.
     *
     * @param cdContingenciaTed the cd contingencia ted
     */
    public void setCdContingenciaTed(String cdContingenciaTed) {
	this.cdContingenciaTed = cdContingenciaTed;
    }

    /**
     * Get: cdDigitoAgenciaDestino.
     *
     * @return cdDigitoAgenciaDestino
     */
    public Integer getCdDigitoAgenciaDestino() {
	return cdDigitoAgenciaDestino;
    }

    /**
     * Set: cdDigitoAgenciaDestino.
     *
     * @param cdDigitoAgenciaDestino the cd digito agencia destino
     */
    public void setCdDigitoAgenciaDestino(Integer cdDigitoAgenciaDestino) {
	this.cdDigitoAgenciaDestino = cdDigitoAgenciaDestino;
    }

    /**
     * Get: cdDigitoAgenciaSalario.
     *
     * @return cdDigitoAgenciaSalario
     */
    public Integer getCdDigitoAgenciaSalario() {
	return cdDigitoAgenciaSalario;
    }

    /**
     * Set: cdDigitoAgenciaSalario.
     *
     * @param cdDigitoAgenciaSalario the cd digito agencia salario
     */
    public void setCdDigitoAgenciaSalario(Integer cdDigitoAgenciaSalario) {
	this.cdDigitoAgenciaSalario = cdDigitoAgenciaSalario;
    }

    /**
     * Get: dsAgenciaDestino.
     *
     * @return dsAgenciaDestino
     */
    public String getDsAgenciaDestino() {
	return dsAgenciaDestino;
    }

    /**
     * Set: dsAgenciaDestino.
     *
     * @param dsAgenciaDestino the ds agencia destino
     */
    public void setDsAgenciaDestino(String dsAgenciaDestino) {
	this.dsAgenciaDestino = dsAgenciaDestino;
    }

    /**
     * Get: dsAgenciaSalario.
     *
     * @return dsAgenciaSalario
     */
    public String getDsAgenciaSalario() {
	return dsAgenciaSalario;
    }

    /**
     * Set: dsAgenciaSalario.
     *
     * @param dsAgenciaSalario the ds agencia salario
     */
    public void setDsAgenciaSalario(String dsAgenciaSalario) {
	this.dsAgenciaSalario = dsAgenciaSalario;
    }

    /**
     * Get: dsBancoDestino.
     *
     * @return dsBancoDestino
     */
    public String getDsBancoDestino() {
	return dsBancoDestino;
    }

    /**
     * Set: dsBancoDestino.
     *
     * @param dsBancoDestino the ds banco destino
     */
    public void setDsBancoDestino(String dsBancoDestino) {
	this.dsBancoDestino = dsBancoDestino;
    }

    /**
     * Get: dsBancoSalario.
     *
     * @return dsBancoSalario
     */
    public String getDsBancoSalario() {
	return dsBancoSalario;
    }

    /**
     * Set: dsBancoSalario.
     *
     * @param dsBancoSalario the ds banco salario
     */
    public void setDsBancoSalario(String dsBancoSalario) {
	this.dsBancoSalario = dsBancoSalario;
    }

    /**
     * Lista vinculo conta salario dest saida dto.
     */
    public ListaVinculoContaSalarioDestSaidaDTO() {
	super();
	// TODO Auto-generated constructor stub
    }

    /**
     * Get: cdAgenciaDestino.
     *
     * @return cdAgenciaDestino
     */
    public Integer getCdAgenciaDestino() {
	return cdAgenciaDestino;
    }

    /**
     * Set: cdAgenciaDestino.
     *
     * @param cdAgenciaDestino the cd agencia destino
     */
    public void setCdAgenciaDestino(Integer cdAgenciaDestino) {
	this.cdAgenciaDestino = cdAgenciaDestino;
    }

    /**
     * Get: cdAgenciaSalario.
     *
     * @return cdAgenciaSalario
     */
    public Integer getCdAgenciaSalario() {
	return cdAgenciaSalario;
    }

    /**
     * Set: cdAgenciaSalario.
     *
     * @param cdAgenciaSalario the cd agencia salario
     */
    public void setCdAgenciaSalario(Integer cdAgenciaSalario) {
	this.cdAgenciaSalario = cdAgenciaSalario;
    }

    /**
     * Get: cdBancoDestino.
     *
     * @return cdBancoDestino
     */
    public Integer getCdBancoDestino() {
	return cdBancoDestino;
    }

    /**
     * Set: cdBancoDestino.
     *
     * @param cdBancoDestino the cd banco destino
     */
    public void setCdBancoDestino(Integer cdBancoDestino) {
	this.cdBancoDestino = cdBancoDestino;
    }

    /**
     * Get: cdBancoSalario.
     *
     * @return cdBancoSalario
     */
    public Integer getCdBancoSalario() {
	return cdBancoSalario;
    }

    /**
     * Set: cdBancoSalario.
     *
     * @param cdBancoSalario the cd banco salario
     */
    public void setCdBancoSalario(Integer cdBancoSalario) {
	this.cdBancoSalario = cdBancoSalario;
    }

    /**
     * Get: cdContaDestino.
     *
     * @return cdContaDestino
     */
    public Long getCdContaDestino() {
	return cdContaDestino;
    }

    /**
     * Set: cdContaDestino.
     *
     * @param cdContaDestino the cd conta destino
     */
    public void setCdContaDestino(Long cdContaDestino) {
	this.cdContaDestino = cdContaDestino;
    }

    /**
     * Get: cdContaSalario.
     *
     * @return cdContaSalario
     */
    public Long getCdContaSalario() {
	return cdContaSalario;
    }

    /**
     * Set: cdContaSalario.
     *
     * @param cdContaSalario the cd conta salario
     */
    public void setCdContaSalario(Long cdContaSalario) {
	this.cdContaSalario = cdContaSalario;
    }

    /**
     * Get: cdDigitoContaDestino.
     *
     * @return cdDigitoContaDestino
     */
    public String getCdDigitoContaDestino() {
	return cdDigitoContaDestino;
    }

    /**
     * Set: cdDigitoContaDestino.
     *
     * @param cdDigitoContaDestino the cd digito conta destino
     */
    public void setCdDigitoContaDestino(String cdDigitoContaDestino) {
	this.cdDigitoContaDestino = cdDigitoContaDestino;
    }

    /**
     * Get: cdDigitoContaSalario.
     *
     * @return cdDigitoContaSalario
     */
    public String getCdDigitoContaSalario() {
	return cdDigitoContaSalario;
    }

    /**
     * Set: cdDigitoContaSalario.
     *
     * @param cdDigitoContaSalario the cd digito conta salario
     */
    public void setCdDigitoContaSalario(String cdDigitoContaSalario) {
	this.cdDigitoContaSalario = cdDigitoContaSalario;
    }

    /**
     * Get: cdPessoaJuridVinc.
     *
     * @return cdPessoaJuridVinc
     */
    public Long getCdPessoaJuridVinc() {
	return cdPessoaJuridVinc;
    }

    /**
     * Set: cdPessoaJuridVinc.
     *
     * @param cdPessoaJuridVinc the cd pessoa jurid vinc
     */
    public void setCdPessoaJuridVinc(Long cdPessoaJuridVinc) {
	this.cdPessoaJuridVinc = cdPessoaJuridVinc;
    }

    /**
     * Get: cdTipoContaDestino.
     *
     * @return cdTipoContaDestino
     */
    public String getCdTipoContaDestino() {
	return cdTipoContaDestino;
    }

    /**
     * Set: cdTipoContaDestino.
     *
     * @param cdTipoContaDestino the cd tipo conta destino
     */
    public void setCdTipoContaDestino(String cdTipoContaDestino) {
	this.cdTipoContaDestino = cdTipoContaDestino;
    }

    /**
     * Get: cdTipoContratoVinc.
     *
     * @return cdTipoContratoVinc
     */
    public Integer getCdTipoContratoVinc() {
	return cdTipoContratoVinc;
    }

    /**
     * Set: cdTipoContratoVinc.
     *
     * @param cdTipoContratoVinc the cd tipo contrato vinc
     */
    public void setCdTipoContratoVinc(Integer cdTipoContratoVinc) {
	this.cdTipoContratoVinc = cdTipoContratoVinc;
    }

    /**
     * Get: cdTipoVinculoContrato.
     *
     * @return cdTipoVinculoContrato
     */
    public Integer getCdTipoVinculoContrato() {
	return cdTipoVinculoContrato;
    }

    /**
     * Set: cdTipoVinculoContrato.
     *
     * @param cdTipoVinculoContrato the cd tipo vinculo contrato
     */
    public void setCdTipoVinculoContrato(Integer cdTipoVinculoContrato) {
	this.cdTipoVinculoContrato = cdTipoVinculoContrato;
    }

    /**
     * Get: dsSituacaoContaDestino.
     *
     * @return dsSituacaoContaDestino
     */
    public String getDsSituacaoContaDestino() {
	return dsSituacaoContaDestino;
    }

    /**
     * Set: dsSituacaoContaDestino.
     *
     * @param dsSituacaoContaDestino the ds situacao conta destino
     */
    public void setDsSituacaoContaDestino(String dsSituacaoContaDestino) {
	this.dsSituacaoContaDestino = dsSituacaoContaDestino;
    }

    /**
     * Get: nrSeqContratoVinc.
     *
     * @return nrSeqContratoVinc
     */
    public Long getNrSeqContratoVinc() {
	return nrSeqContratoVinc;
    }

    /**
     * Set: nrSeqContratoVinc.
     *
     * @param nrSeqContratoVinc the nr seq contrato vinc
     */
    public void setNrSeqContratoVinc(Long nrSeqContratoVinc) {
	this.nrSeqContratoVinc = nrSeqContratoVinc;
    }

    /**
     * Get: cdCpfParticipante.
     *
     * @return cdCpfParticipante
     */
    public String getCdCpfParticipante() {
	return cdCpfParticipante;
    }
    
    /**
     * Get: cpfParticipante.
     *
     * @return cpfParticipante
     */
    public String getCpfParticipante(){
	String temp = cdCpfParticipante;
	
	if(temp == null){
	    temp = "";
	}
	
	if(temp.length() < 11){
	    temp = PgitUtil.complementaDigito(temp, 11);
	}
	
	String cpf1 = temp.substring(0, 3);
	String cpf2 = temp.substring(3, 6);
	String cpf3 = temp.substring(6, 9);
	String controle = temp.substring(9, 11);
	
	StringBuilder cnpjOuCpf = new StringBuilder("");
	cnpjOuCpf.append(cpf1);
	cnpjOuCpf.append(".");
	cnpjOuCpf.append(cpf2);
	cnpjOuCpf.append(".");
	cnpjOuCpf.append(cpf3);
	cnpjOuCpf.append("-");
	cnpjOuCpf.append(controle);
	
	return cnpjOuCpf.toString();	
    }

    /**
     * Set: cdCpfParticipante.
     *
     * @param cdCpfParticipante the cd cpf participante
     */
    public void setCdCpfParticipante(String cdCpfParticipante) {
	this.cdCpfParticipante = cdCpfParticipante;
    }

    /**
     * Get: nmParticipante.
     *
     * @return nmParticipante
     */
    public String getNmParticipante() {
	return nmParticipante;
    }

    /**
     * Set: nmParticipante.
     *
     * @param nmParticipante the nm participante
     */
    public void setNmParticipante(String nmParticipante) {
	this.nmParticipante = nmParticipante;
    }
}
