/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PilotoIntranet/JavaSource/br/com/bradesco/web/pgit/service/business/confrontodependencia/exceptions/ConfrontoDepServiceException.java,v $
 * $Id: ConfrontoDepServiceException.java,v 1.2 2009/05/13 19:39:29 cpm.com.br\heslei.silva Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: ConfrontoDepServiceException.java,v $
 * Revision 1.2  2009/05/13 19:39:29  cpm.com.br\heslei.silva
 * voltando para vers�o do branch 2.0
 *
 * Revision 1.1.2.1  2009/05/05 14:24:44  corporate\marcio.alves
 * Adicionando pagina��o ao pgit intranet
 *
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */
 
package br.com.bradesco.web.pgit.service.business.confrontodep.exceptions;
import br.com.bradesco.web.aq.application.error.BradescoApplicationException;

/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Classe do tipo exce�ao do adaptador: ConfrontoDependencia
 * </p>
 * 
 * @comment C�DIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public class ConfrontoDepServiceException extends BradescoApplicationException  {

}

