/*
 * Nome: br.com.bradesco.web.pgit.service.business.combo.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.combo.bean;

/**
 * Nome: ListarOrgaoPagadorSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarOrgaoPagadorSaidaDTO{
    
    /** Atributo codMensagem. */
    private String codMensagem;
    
    /** Atributo mensagem. */
    private String mensagem;
    
    /** Atributo numeroConsultas. */
    private Integer numeroConsultas;
    
    /** Atributo cdOrgaoPagadorOrganizacao. */
    private Integer cdOrgaoPagadorOrganizacao;
    
    /** Atributo descCdOrgaoPagadorOrganizacao. */
    private Integer descCdOrgaoPagadorOrganizacao;
    
    /** Atributo cdTipoUnidadeOrganizacao. */
    private Integer cdTipoUnidadeOrganizacao;
    
    /** Atributo dsTipoUnidadeOrganizacao. */
    private String dsTipoUnidadeOrganizacao;
    
    /** Atributo cdPessoaJuridica. */
    private Long cdPessoaJuridica;
    
    /** Atributo dsPessoaJuridica. */
    private String dsPessoaJuridica;
    
    /** Atributo nrSequenciaUnidadeOrganizacao. */
    private Integer nrSequenciaUnidadeOrganizacao;
    
    /** Atributo dsNumeroSequenciaUnidadeOrganizacao. */
    private String dsNumeroSequenciaUnidadeOrganizacao;
    
    /** Atributo cdSituacaoOrganizacaoPagador. */
    private Integer cdSituacaoOrganizacaoPagador;
    
	/**
	 * Listar orgao pagador saida dto.
	 *
	 * @param cdOrgaoPagadorOrganizacao the cd orgao pagador organizacao
	 * @param descCdOrgaoPagadorOrganizacao the desc cd orgao pagador organizacao
	 */
	public ListarOrgaoPagadorSaidaDTO(Integer cdOrgaoPagadorOrganizacao, Integer descCdOrgaoPagadorOrganizacao) {
		super();
		this.cdOrgaoPagadorOrganizacao = cdOrgaoPagadorOrganizacao;
		this.descCdOrgaoPagadorOrganizacao = descCdOrgaoPagadorOrganizacao;
	}
	
	/**
	 * Listar orgao pagador saida dto.
	 */
	public ListarOrgaoPagadorSaidaDTO() {
		super();
		
	}
	
	/**
	 * Listar orgao pagador saida dto.
	 *
	 * @param cdTipoUnidadeOrganizacao the cd tipo unidade organizacao
	 * @param dsTipoUnidadeOrganizacao the ds tipo unidade organizacao
	 */
	public ListarOrgaoPagadorSaidaDTO(Integer cdTipoUnidadeOrganizacao, String dsTipoUnidadeOrganizacao) {
		super();
		this.cdTipoUnidadeOrganizacao = cdTipoUnidadeOrganizacao;
		this.dsTipoUnidadeOrganizacao = dsTipoUnidadeOrganizacao;
	}
	
	/**
	 * Get: cdOrgaoPagadorOrganizacao.
	 *
	 * @return cdOrgaoPagadorOrganizacao
	 */
	public Integer getCdOrgaoPagadorOrganizacao() {
		return cdOrgaoPagadorOrganizacao;
	}
	
	/**
	 * Set: cdOrgaoPagadorOrganizacao.
	 *
	 * @param cdOrgaoPagadorOrganizacao the cd orgao pagador organizacao
	 */
	public void setCdOrgaoPagadorOrganizacao(Integer cdOrgaoPagadorOrganizacao) {
		this.cdOrgaoPagadorOrganizacao = cdOrgaoPagadorOrganizacao;
	}
	
	/**
	 * Get: cdPessoaJuridica.
	 *
	 * @return cdPessoaJuridica
	 */
	public Long getCdPessoaJuridica() {
		return cdPessoaJuridica;
	}
	
	/**
	 * Set: cdPessoaJuridica.
	 *
	 * @param cdPessoaJuridica the cd pessoa juridica
	 */
	public void setCdPessoaJuridica(Long cdPessoaJuridica) {
		this.cdPessoaJuridica = cdPessoaJuridica;
	}
	
	/**
	 * Get: cdSituacaoOrganizacaoPagador.
	 *
	 * @return cdSituacaoOrganizacaoPagador
	 */
	public Integer getCdSituacaoOrganizacaoPagador() {
		return cdSituacaoOrganizacaoPagador;
	}
	
	/**
	 * Set: cdSituacaoOrganizacaoPagador.
	 *
	 * @param cdSituacaoOrganizacaoPagador the cd situacao organizacao pagador
	 */
	public void setCdSituacaoOrganizacaoPagador(Integer cdSituacaoOrganizacaoPagador) {
		this.cdSituacaoOrganizacaoPagador = cdSituacaoOrganizacaoPagador;
	}
	
	/**
	 * Get: cdTipoUnidadeOrganizacao.
	 *
	 * @return cdTipoUnidadeOrganizacao
	 */
	public Integer getCdTipoUnidadeOrganizacao() {
		return cdTipoUnidadeOrganizacao;
	}
	
	/**
	 * Set: cdTipoUnidadeOrganizacao.
	 *
	 * @param cdTipoUnidadeOrganizacao the cd tipo unidade organizacao
	 */
	public void setCdTipoUnidadeOrganizacao(Integer cdTipoUnidadeOrganizacao) {
		this.cdTipoUnidadeOrganizacao = cdTipoUnidadeOrganizacao;
	}
	
	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}
	
	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}
	
	/**
	 * Get: dsNumeroSequenciaUnidadeOrganizacao.
	 *
	 * @return dsNumeroSequenciaUnidadeOrganizacao
	 */
	public String getDsNumeroSequenciaUnidadeOrganizacao() {
		return dsNumeroSequenciaUnidadeOrganizacao;
	}
	
	/**
	 * Set: dsNumeroSequenciaUnidadeOrganizacao.
	 *
	 * @param dsNumeroSequenciaUnidadeOrganizacao the ds numero sequencia unidade organizacao
	 */
	public void setDsNumeroSequenciaUnidadeOrganizacao(
			String dsNumeroSequenciaUnidadeOrganizacao) {
		this.dsNumeroSequenciaUnidadeOrganizacao = dsNumeroSequenciaUnidadeOrganizacao;
	}
	
	/**
	 * Get: dsPessoaJuridica.
	 *
	 * @return dsPessoaJuridica
	 */
	public String getDsPessoaJuridica() {
		return dsPessoaJuridica;
	}
	
	/**
	 * Set: dsPessoaJuridica.
	 *
	 * @param dsPessoaJuridica the ds pessoa juridica
	 */
	public void setDsPessoaJuridica(String dsPessoaJuridica) {
		this.dsPessoaJuridica = dsPessoaJuridica;
	}
	
	/**
	 * Get: dsTipoUnidadeOrganizacao.
	 *
	 * @return dsTipoUnidadeOrganizacao
	 */
	public String getDsTipoUnidadeOrganizacao() {
		return dsTipoUnidadeOrganizacao;
	}
	
	/**
	 * Set: dsTipoUnidadeOrganizacao.
	 *
	 * @param dsTipoUnidadeOrganizacao the ds tipo unidade organizacao
	 */
	public void setDsTipoUnidadeOrganizacao(String dsTipoUnidadeOrganizacao) {
		this.dsTipoUnidadeOrganizacao = dsTipoUnidadeOrganizacao;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	/**
	 * Get: nrSequenciaUnidadeOrganizacao.
	 *
	 * @return nrSequenciaUnidadeOrganizacao
	 */
	public Integer getNrSequenciaUnidadeOrganizacao() {
		return nrSequenciaUnidadeOrganizacao;
	}
	
	/**
	 * Set: nrSequenciaUnidadeOrganizacao.
	 *
	 * @param nrSequenciaUnidadeOrganizacao the nr sequencia unidade organizacao
	 */
	public void setNrSequenciaUnidadeOrganizacao(
			Integer nrSequenciaUnidadeOrganizacao) {
		this.nrSequenciaUnidadeOrganizacao = nrSequenciaUnidadeOrganizacao;
	}
	
	/**
	 * Get: numeroConsultas.
	 *
	 * @return numeroConsultas
	 */
	public Integer getNumeroConsultas() {
		return numeroConsultas;
	}
	
	/**
	 * Set: numeroConsultas.
	 *
	 * @param numeroConsultas the numero consultas
	 */
	public void setNumeroConsultas(Integer numeroConsultas) {
		this.numeroConsultas = numeroConsultas;
	}

	/**
	 * Get: descCdOrgaoPagadorOrganizacao.
	 *
	 * @return descCdOrgaoPagadorOrganizacao
	 */
	public Integer getDescCdOrgaoPagadorOrganizacao() {
		return descCdOrgaoPagadorOrganizacao;
	}
	
	/**
	 * Set: descCdOrgaoPagadorOrganizacao.
	 *
	 * @param descCdOrgaoPagadorOrganizacao the desc cd orgao pagador organizacao
	 */
	public void setDescCdOrgaoPagadorOrganizacao(
			Integer descCdOrgaoPagadorOrganizacao) {
		this.descCdOrgaoPagadorOrganizacao = descCdOrgaoPagadorOrganizacao;
	}
	
	
    
}
