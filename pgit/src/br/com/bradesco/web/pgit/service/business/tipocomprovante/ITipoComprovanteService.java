/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/interfaz.ftl,v $
 * $Id: interfaz.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: interfaz.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.tipocomprovante;

import java.util.List;

import br.com.bradesco.web.pgit.service.business.msgcompsalarial.bean.ListarMsgComprovanteSalrlEntradaDTO;
import br.com.bradesco.web.pgit.service.business.msgcompsalarial.bean.ListarMsgComprovanteSalrlSaidaDTO;
import br.com.bradesco.web.pgit.service.business.tipocomprovante.bean.AlterarTipoComprovanteEntradaDTO;
import br.com.bradesco.web.pgit.service.business.tipocomprovante.bean.AlterarTipoComprovanteSaidaDTO;
import br.com.bradesco.web.pgit.service.business.tipocomprovante.bean.DetalharTipoComprovanteEntradaDTO;
import br.com.bradesco.web.pgit.service.business.tipocomprovante.bean.DetalharTipoComprovanteSaidaDTO;
import br.com.bradesco.web.pgit.service.business.tipocomprovante.bean.ExcluirTipoComprovanteEntradaDTO;
import br.com.bradesco.web.pgit.service.business.tipocomprovante.bean.ExcluirTipoComprovanteSaidaDTO;
import br.com.bradesco.web.pgit.service.business.tipocomprovante.bean.IncluirTipoComprovanteEntradaDTO;
import br.com.bradesco.web.pgit.service.business.tipocomprovante.bean.IncluirTipoComprovanteSaidaDTO;
import br.com.bradesco.web.pgit.service.business.tipocomprovante.bean.ListarTipoComprovanteEntradaDTO;
import br.com.bradesco.web.pgit.service.business.tipocomprovante.bean.ListarTipoComprovanteSaidaDTO;

/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Interface do adaptador: ManterTipoComprovante
 * </p>
 * 
 * @comment CODIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public interface ITipoComprovanteService {

	/**
	 * Listar tipo comprovante.
	 *
	 * @param listarTipoComprovanteEntradaDTO the listar tipo comprovante entrada dto
	 * @return the list< listar tipo comprovante saida dt o>
	 */
	List<ListarTipoComprovanteSaidaDTO> listarTipoComprovante (ListarTipoComprovanteEntradaDTO listarTipoComprovanteEntradaDTO);
	
	/**
	 * Incluir tipo comprovante.
	 *
	 * @param incluirTipoComprovanteEntradaDTO the incluir tipo comprovante entrada dto
	 * @return the incluir tipo comprovante saida dto
	 */
	IncluirTipoComprovanteSaidaDTO incluirTipoComprovante (IncluirTipoComprovanteEntradaDTO incluirTipoComprovanteEntradaDTO);
	
	/**
	 * Detalhar tipo comprovante.
	 *
	 * @param detalharTipoComprovanteEntradaDTO the detalhar tipo comprovante entrada dto
	 * @return the detalhar tipo comprovante saida dto
	 */
	DetalharTipoComprovanteSaidaDTO detalharTipoComprovante (DetalharTipoComprovanteEntradaDTO detalharTipoComprovanteEntradaDTO);
	
	/**
	 * Excluir tipo comprovante.
	 *
	 * @param excluirTipoComprovanteEntradaDTO the excluir tipo comprovante entrada dto
	 * @return the excluir tipo comprovante saida dto
	 */
	ExcluirTipoComprovanteSaidaDTO excluirTipoComprovante (ExcluirTipoComprovanteEntradaDTO excluirTipoComprovanteEntradaDTO);
	
	/**
	 * Alterar tipo comprovante.
	 *
	 * @param alterarTipoComprovanteEntradaDTO the alterar tipo comprovante entrada dto
	 * @return the alterar tipo comprovante saida dto
	 */
	AlterarTipoComprovanteSaidaDTO alterarTipoComprovante (AlterarTipoComprovanteEntradaDTO alterarTipoComprovanteEntradaDTO);
    
    /**
     * Listar msg comprovante salrl.
     *
     * @param listarMsgComprovanteSalrlEntradaDTO the listar msg comprovante salrl entrada dto
     * @return the list< listar msg comprovante salrl saida dt o>
     */
    List<ListarMsgComprovanteSalrlSaidaDTO> listarMsgComprovanteSalrl(ListarMsgComprovanteSalrlEntradaDTO listarMsgComprovanteSalrlEntradaDTO);	
	
	
    /**
     * M�todo de exemplo.
     */
    void sampleManterTipoComprovante();

}

