/*
 * Nome: br.com.bradesco.web.pgit.service.business.duplarqretorno.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.duplarqretorno.bean;

/**
 * Nome: DuplicacaoArquivoRetornoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class DuplicacaoArquivoRetornoSaidaDTO {
	
	/** Atributo codMensagem. */
	private String codMensagem;
	
	/** Atributo mensagem. */
	private String mensagem;	
	
	/** Atributo cdInstrucaoEnvioRetorno. */
	private Integer cdInstrucaoEnvioRetorno; 
	
	/** Atributo cdPessoaJuridContrato. */
	private Long cdPessoaJuridContrato; 
	
	/** Atributo cdTipoArquivoRetorno. */
	private Integer cdTipoArquivoRetorno; 
	
	/** Atributo cdTipoContratoNegocio. */
	private Integer cdTipoContratoNegocio; 
	
	/** Atributo cdTipoLayoutArquivo. */
	private Integer cdTipoLayoutArquivo; 
	
	/** Atributo nrSeqContratoNegocio. */
	private Long nrSeqContratoNegocio; 
	
	/** Atributo cdPessoaJuridVinc. */
	private Long cdPessoaJuridVinc; 
	
	/** Atributo nrSeqContratoVinc. */
	private Long nrSeqContratoVinc; 
	
	/** Atributo cdIndicadorDuplicidadeRetorno. */
	private Integer cdIndicadorDuplicidadeRetorno; 
	
	/** Atributo cdTipoContratoVinc. */
	private Integer cdTipoContratoVinc; 
	
	/** Atributo dsTipoArquivoRetorno. */
	private String dsTipoArquivoRetorno; 
	
	/** Atributo dsTipoLayoutArquivo. */
	private String dsTipoLayoutArquivo; 
	
	/** Atributo dsPessoaJuridVinc. */
	private String dsPessoaJuridVinc;
	
	/** Atributo dsTipoContratoVinc. */
	private String dsTipoContratoVinc;
	
	/**
	 * Get: cdIndicadorDuplicidadeRetorno.
	 *
	 * @return cdIndicadorDuplicidadeRetorno
	 */
	public Integer getCdIndicadorDuplicidadeRetorno() {
		return cdIndicadorDuplicidadeRetorno;
	}
	
	/**
	 * Set: cdIndicadorDuplicidadeRetorno.
	 *
	 * @param cdIndicadorDuplicidadeRetorno the cd indicador duplicidade retorno
	 */
	public void setCdIndicadorDuplicidadeRetorno(
			Integer cdIndicadorDuplicidadeRetorno) {
		this.cdIndicadorDuplicidadeRetorno = cdIndicadorDuplicidadeRetorno;
	}
	
	/**
	 * Get: cdInstrucaoEnvioRetorno.
	 *
	 * @return cdInstrucaoEnvioRetorno
	 */
	public Integer getCdInstrucaoEnvioRetorno() {
		return cdInstrucaoEnvioRetorno;
	}
	
	/**
	 * Set: cdInstrucaoEnvioRetorno.
	 *
	 * @param cdInstrucaoEnvioRetorno the cd instrucao envio retorno
	 */
	public void setCdInstrucaoEnvioRetorno(Integer cdInstrucaoEnvioRetorno) {
		this.cdInstrucaoEnvioRetorno = cdInstrucaoEnvioRetorno;
	}
	
	/**
	 * Get: cdPessoaJuridContrato.
	 *
	 * @return cdPessoaJuridContrato
	 */
	public Long getCdPessoaJuridContrato() {
		return cdPessoaJuridContrato;
	}
	
	/**
	 * Set: cdPessoaJuridContrato.
	 *
	 * @param cdPessoaJuridContrato the cd pessoa jurid contrato
	 */
	public void setCdPessoaJuridContrato(Long cdPessoaJuridContrato) {
		this.cdPessoaJuridContrato = cdPessoaJuridContrato;
	}
	
	/**
	 * Get: cdPessoaJuridVinc.
	 *
	 * @return cdPessoaJuridVinc
	 */
	public Long getCdPessoaJuridVinc() {
		return cdPessoaJuridVinc;
	}
	
	/**
	 * Set: cdPessoaJuridVinc.
	 *
	 * @param cdPessoaJuridVinc the cd pessoa jurid vinc
	 */
	public void setCdPessoaJuridVinc(Long cdPessoaJuridVinc) {
		this.cdPessoaJuridVinc = cdPessoaJuridVinc;
	}
	
	/**
	 * Get: cdTipoArquivoRetorno.
	 *
	 * @return cdTipoArquivoRetorno
	 */
	public Integer getCdTipoArquivoRetorno() {
		return cdTipoArquivoRetorno;
	}
	
	/**
	 * Set: cdTipoArquivoRetorno.
	 *
	 * @param cdTipoArquivoRetorno the cd tipo arquivo retorno
	 */
	public void setCdTipoArquivoRetorno(Integer cdTipoArquivoRetorno) {
		this.cdTipoArquivoRetorno = cdTipoArquivoRetorno;
	}
	
	/**
	 * Get: cdTipoContratoNegocio.
	 *
	 * @return cdTipoContratoNegocio
	 */
	public Integer getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}
	
	/**
	 * Set: cdTipoContratoNegocio.
	 *
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}
	
	/**
	 * Get: cdTipoContratoVinc.
	 *
	 * @return cdTipoContratoVinc
	 */
	public Integer getCdTipoContratoVinc() {
		return cdTipoContratoVinc;
	}
	
	/**
	 * Set: cdTipoContratoVinc.
	 *
	 * @param cdTipoContratoVinc the cd tipo contrato vinc
	 */
	public void setCdTipoContratoVinc(Integer cdTipoContratoVinc) {
		this.cdTipoContratoVinc = cdTipoContratoVinc;
	}
	
	/**
	 * Get: cdTipoLayoutArquivo.
	 *
	 * @return cdTipoLayoutArquivo
	 */
	public Integer getCdTipoLayoutArquivo() {
		return cdTipoLayoutArquivo;
	}
	
	/**
	 * Set: cdTipoLayoutArquivo.
	 *
	 * @param cdTipoLayoutArquivo the cd tipo layout arquivo
	 */
	public void setCdTipoLayoutArquivo(Integer cdTipoLayoutArquivo) {
		this.cdTipoLayoutArquivo = cdTipoLayoutArquivo;
	}
	
	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}
	
	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}
	
	/**
	 * Get: dsPessoaJuridVinc.
	 *
	 * @return dsPessoaJuridVinc
	 */
	public String getDsPessoaJuridVinc() {
		return dsPessoaJuridVinc;
	}
	
	/**
	 * Set: dsPessoaJuridVinc.
	 *
	 * @param dsPessoaJuridVinc the ds pessoa jurid vinc
	 */
	public void setDsPessoaJuridVinc(String dsPessoaJuridVinc) {
		this.dsPessoaJuridVinc = dsPessoaJuridVinc;
	}
	
	/**
	 * Get: dsTipoArquivoRetorno.
	 *
	 * @return dsTipoArquivoRetorno
	 */
	public String getDsTipoArquivoRetorno() {
		return dsTipoArquivoRetorno;
	}
	
	/**
	 * Set: dsTipoArquivoRetorno.
	 *
	 * @param dsTipoArquivoRetorno the ds tipo arquivo retorno
	 */
	public void setDsTipoArquivoRetorno(String dsTipoArquivoRetorno) {
		this.dsTipoArquivoRetorno = dsTipoArquivoRetorno;
	}
	
	/**
	 * Get: dsTipoContratoVinc.
	 *
	 * @return dsTipoContratoVinc
	 */
	public String getDsTipoContratoVinc() {
		return dsTipoContratoVinc;
	}
	
	/**
	 * Set: dsTipoContratoVinc.
	 *
	 * @param dsTipoContratoVinc the ds tipo contrato vinc
	 */
	public void setDsTipoContratoVinc(String dsTipoContratoVinc) {
		this.dsTipoContratoVinc = dsTipoContratoVinc;
	}
	
	/**
	 * Get: dsTipoLayoutArquivo.
	 *
	 * @return dsTipoLayoutArquivo
	 */
	public String getDsTipoLayoutArquivo() {
		return dsTipoLayoutArquivo;
	}
	
	/**
	 * Set: dsTipoLayoutArquivo.
	 *
	 * @param dsTipoLayoutArquivo the ds tipo layout arquivo
	 */
	public void setDsTipoLayoutArquivo(String dsTipoLayoutArquivo) {
		this.dsTipoLayoutArquivo = dsTipoLayoutArquivo;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	/**
	 * Get: nrSeqContratoNegocio.
	 *
	 * @return nrSeqContratoNegocio
	 */
	public Long getNrSeqContratoNegocio() {
		return nrSeqContratoNegocio;
	}
	
	/**
	 * Set: nrSeqContratoNegocio.
	 *
	 * @param nrSeqContratoNegocio the nr seq contrato negocio
	 */
	public void setNrSeqContratoNegocio(Long nrSeqContratoNegocio) {
		this.nrSeqContratoNegocio = nrSeqContratoNegocio;
	}
	
	/**
	 * Get: nrSeqContratoVinc.
	 *
	 * @return nrSeqContratoVinc
	 */
	public Long getNrSeqContratoVinc() {
		return nrSeqContratoVinc;
	}
	
	/**
	 * Set: nrSeqContratoVinc.
	 *
	 * @param nrSeqContratoVinc the nr seq contrato vinc
	 */
	public void setNrSeqContratoVinc(Long nrSeqContratoVinc) {
		this.nrSeqContratoVinc = nrSeqContratoVinc;
	}
	
	
	
}
