/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listartipospagto.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdTipoPagamentoCliente
     */
    private int _cdTipoPagamentoCliente = 0;

    /**
     * keeps track of state for field: _cdTipoPagamentoCliente
     */
    private boolean _has_cdTipoPagamentoCliente;

    /**
     * Field _dsTipoPagamentoCliente
     */
    private java.lang.String _dsTipoPagamentoCliente;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listartipospagto.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdTipoPagamentoCliente
     * 
     */
    public void deleteCdTipoPagamentoCliente()
    {
        this._has_cdTipoPagamentoCliente= false;
    } //-- void deleteCdTipoPagamentoCliente() 

    /**
     * Returns the value of field 'cdTipoPagamentoCliente'.
     * 
     * @return int
     * @return the value of field 'cdTipoPagamentoCliente'.
     */
    public int getCdTipoPagamentoCliente()
    {
        return this._cdTipoPagamentoCliente;
    } //-- int getCdTipoPagamentoCliente() 

    /**
     * Returns the value of field 'dsTipoPagamentoCliente'.
     * 
     * @return String
     * @return the value of field 'dsTipoPagamentoCliente'.
     */
    public java.lang.String getDsTipoPagamentoCliente()
    {
        return this._dsTipoPagamentoCliente;
    } //-- java.lang.String getDsTipoPagamentoCliente() 

    /**
     * Method hasCdTipoPagamentoCliente
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoPagamentoCliente()
    {
        return this._has_cdTipoPagamentoCliente;
    } //-- boolean hasCdTipoPagamentoCliente() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdTipoPagamentoCliente'.
     * 
     * @param cdTipoPagamentoCliente the value of field
     * 'cdTipoPagamentoCliente'.
     */
    public void setCdTipoPagamentoCliente(int cdTipoPagamentoCliente)
    {
        this._cdTipoPagamentoCliente = cdTipoPagamentoCliente;
        this._has_cdTipoPagamentoCliente = true;
    } //-- void setCdTipoPagamentoCliente(int) 

    /**
     * Sets the value of field 'dsTipoPagamentoCliente'.
     * 
     * @param dsTipoPagamentoCliente the value of field
     * 'dsTipoPagamentoCliente'.
     */
    public void setDsTipoPagamentoCliente(java.lang.String dsTipoPagamentoCliente)
    {
        this._dsTipoPagamentoCliente = dsTipoPagamentoCliente;
    } //-- void setDsTipoPagamentoCliente(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listartipospagto.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listartipospagto.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listartipospagto.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listartipospagto.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
