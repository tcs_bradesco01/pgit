package br.com.bradesco.web.pgit.service.business.mantercontrato.bean;


// TODO: Auto-generated Javadoc
/**
 * Arquivo criado em 03/05/17.
 */
public class ListarContasExclusaoEntradaDTO {

    /** The nr maximo ocorrencias. */
    private Integer nrMaximoOcorrencias;

    /** The cd pessoa juridica negocio. */
    private Long cdPessoaJuridicaNegocio;

    /** The cd tipo contrato negocio. */
    private Integer cdTipoContratoNegocio;

    /** The nr sequencia contrato negocio. */
    private Long nrSequenciaContratoNegocio;

    /** The cd corpo cpf cnpj. */
    private Long cdCorpoCpfCnpj;

    /** The cd filial cnpj participante. */
    private Integer cdFilialCnpjParticipante;

    /** The cd controle cpf cnpj. */
    private Integer cdControleCpfCnpj;

    /**
     * Set nr maximo ocorrencias.
     *
     * @param nrMaximoOcorrencias the nr maximo ocorrencias
     */
    public void setNrMaximoOcorrencias(Integer nrMaximoOcorrencias) {
        this.nrMaximoOcorrencias = nrMaximoOcorrencias;
    }

    /**
     * Get nr maximo ocorrencias.
     *
     * @return the nr maximo ocorrencias
     */
    public Integer getNrMaximoOcorrencias() {
        return this.nrMaximoOcorrencias;
    }

    /**
     * Set cd pessoa juridica negocio.
     *
     * @param cdPessoaJuridicaNegocio the cd pessoa juridica negocio
     */
    public void setCdPessoaJuridicaNegocio(Long cdPessoaJuridicaNegocio) {
        this.cdPessoaJuridicaNegocio = cdPessoaJuridicaNegocio;
    }

    /**
     * Get cd pessoa juridica negocio.
     *
     * @return the cd pessoa juridica negocio
     */
    public Long getCdPessoaJuridicaNegocio() {
        return this.cdPessoaJuridicaNegocio;
    }

    /**
     * Set cd tipo contrato negocio.
     *
     * @param cdTipoContratoNegocio the cd tipo contrato negocio
     */
    public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
        this.cdTipoContratoNegocio = cdTipoContratoNegocio;
    }

    /**
     * Get cd tipo contrato negocio.
     *
     * @return the cd tipo contrato negocio
     */
    public Integer getCdTipoContratoNegocio() {
        return this.cdTipoContratoNegocio;
    }

    /**
     * Set nr sequencia contrato negocio.
     *
     * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
     */
    public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
        this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
    }

    /**
     * Get nr sequencia contrato negocio.
     *
     * @return the nr sequencia contrato negocio
     */
    public Long getNrSequenciaContratoNegocio() {
        return this.nrSequenciaContratoNegocio;
    }

    /**
     * Set cd corpo cpf cnpj.
     *
     * @param cdCorpoCpfCnpj the cd corpo cpf cnpj
     */
    public void setCdCorpoCpfCnpj(Long cdCorpoCpfCnpj) {
        this.cdCorpoCpfCnpj = cdCorpoCpfCnpj;
    }

    /**
     * Get cd corpo cpf cnpj.
     *
     * @return the cd corpo cpf cnpj
     */
    public Long getCdCorpoCpfCnpj() {
        return this.cdCorpoCpfCnpj;
    }

    /**
     * Set cd filial cnpj participante.
     *
     * @param cdFilialCnpjParticipante the cd filial cnpj participante
     */
    public void setCdFilialCnpjParticipante(Integer cdFilialCnpjParticipante) {
        this.cdFilialCnpjParticipante = cdFilialCnpjParticipante;
    }

    /**
     * Get cd filial cnpj participante.
     *
     * @return the cd filial cnpj participante
     */
    public Integer getCdFilialCnpjParticipante() {
        return this.cdFilialCnpjParticipante;
    }

    /**
     * Set cd controle cpf cnpj.
     *
     * @param cdControleCpfCnpj the cd controle cpf cnpj
     */
    public void setCdControleCpfCnpj(Integer cdControleCpfCnpj) {
        this.cdControleCpfCnpj = cdControleCpfCnpj;
    }

    /**
     * Get cd controle cpf cnpj.
     *
     * @return the cd controle cpf cnpj
     */
    public Integer getCdControleCpfCnpj() {
        return this.cdControleCpfCnpj;
    }
}