/*
 * Nome: br.com.bradesco.web.pgit.service.business.moedassistema.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.moedassistema.bean;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.apache.commons.lang.StringUtils;

import br.com.bradesco.web.aq.application.error.BradescoViewException.BradescoViewExceptionActionType;
import br.com.bradesco.web.aq.application.log.ILogManager;
import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterFunctionalException;
import br.com.bradesco.web.aq.application.util.faces.BradescoFacesUtils;
import br.com.bradesco.web.pgit.service.business.combo.IComboService;
import br.com.bradesco.web.pgit.service.business.combo.bean.MoedaEconomicaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.MoedaEconomicaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.TipoLayoutArquivoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.TipoLayoutArquivoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.moedassistema.IMoedasSistemaService;

/**
 * Nome: ManterMoedasSistemaBean
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ManterMoedasSistemaBean {
	
	//Constante Utilizada para evitar redund�ncia apontada pelo Sonar
	/** Atributo MANTER_MOEDAS_SISTEMA. */
	private static final String MANTER_MOEDAS_SISTEMA = "conManterMoedasSistema";
	
	/** Atributo tipoLayoutArquivoFiltro. */
	private Integer tipoLayoutArquivoFiltro;
	
	/** Atributo incluirTipoLayoutArquivoFiltro. */
	private Integer incluirTipoLayoutArquivoFiltro;
	
	/** Atributo moedaIndicePadraoBancoFiltro. */
	private Integer moedaIndicePadraoBancoFiltro;
	
	/** Atributo incluirMoedaIndicePadraoBancoFiltro. */
	private Integer incluirMoedaIndicePadraoBancoFiltro;
	
	/** Atributo moedaIndicePadraoBanco. */
	private String moedaIndicePadraoBanco;
	
	/** Atributo pesquisar. */
	private String pesquisar;
	
	/** Atributo dataHoraInclusao. */
	private String dataHoraInclusao;
	
	/** Atributo usuarioInclusao. */
	private String usuarioInclusao;
	
	/** Atributo tipoCanalInclusao. */
	private String tipoCanalInclusao;
	
	/** Atributo complementoInclusao. */
	private String complementoInclusao;
	
	/** Atributo usuarioManutencao. */
	private String usuarioManutencao;
	
	/** Atributo dataHoraManutencao. */
	private String dataHoraManutencao;
	
	/** Atributo tipoCanalManutencao. */
	private String tipoCanalManutencao;
	
	/** Atributo complementoManutencao. */
	private String complementoManutencao;
	
	/** Atributo desabPerfil. */
	private boolean desabPerfil;
	
	/** Atributo desabTextRemessa. */
	private boolean desabTextRemessa;
	
	/** Atributo moedaIndicePadraoBancoDesc. */
	private String moedaIndicePadraoBancoDesc;
	
	/** Atributo logger. */
	private ILogManager logger;
	
	/** Atributo listaPesquisa. */
	private List<MoedaSistemaSaidaDTO> listaPesquisa;
	
	/** Atributo listaPesquisaControle. */
	private List<SelectItem> listaPesquisaControle = new ArrayList<SelectItem>();
	
	/** Atributo mostraBotoes. */
	private boolean mostraBotoes;
	
	/** Atributo itemSelecionadoLista. */
	private MoedaSistemaSaidaDTO itemSelecionadoLista;
	
	/** Atributo listaTipoLayoutArquivo. */
	private List<SelectItem> listaTipoLayoutArquivo =  new ArrayList<SelectItem>();
	
	/** Atributo listaMoedaEconomica. */
	private List<SelectItem> listaMoedaEconomica =  new ArrayList<SelectItem>();
	
	/** Atributo listaTipoLayoutArquivoHash. */
	private Map<Integer, String> listaTipoLayoutArquivoHash = new HashMap<Integer, String>();
	
	/** Atributo listaTipoMoedaEconomicaHash. */
	private Map<Integer, String> listaTipoMoedaEconomicaHash = new HashMap<Integer, String>();
	
	/** Atributo comboService. */
	private IComboService comboService;
	
	/** Atributo moedaSistemaService. */
	private IMoedasSistemaService moedaSistemaService;
	
	/** Atributo tipoLayoutDesc. */
	private String tipoLayoutDesc;
	
	/** Atributo tipoLayout. */
	private String tipoLayout;
	
	/** Atributo cdIndicadorEconomicoLayout. */
	private String cdIndicadorEconomicoLayout;
	
	/** Atributo btoAcionado. */
	private Boolean btoAcionado;

	/** Atributo saidaTipoLayoutArquivoSaidaDTO. */
	private TipoLayoutArquivoSaidaDTO saidaTipoLayoutArquivoSaidaDTO;
	
	/**
	 * Limpar pagina.
	 *
	 * @param evt the evt
	 */
	public void limparPagina(ActionEvent evt) {
		setTipoLayoutArquivoFiltro(null);
		setMoedaIndicePadraoBancoFiltro(null);
		listaMoedaEconomica = new ArrayList<SelectItem>();
		listaTipoLayoutArquivo = new ArrayList<SelectItem>();
		setMoedaIndicePadraoBanco("");
		setDataHoraInclusao("");
		setUsuarioInclusao("");
		setTipoCanalInclusao("");
		setComplementoInclusao("");
		setUsuarioManutencao("");
		setDataHoraManutencao("");
		setTipoCanalManutencao("");
		setComplementoManutencao("");
		setListaPesquisa(null);
		setItemSelecionadoLista(null);
		setMostraBotoes(false);
		preencheListaMoedaEconomica();
		preencheTipoLayoutArquivo();
		setBtoAcionado(false);
		limparDados();

	}

	/** Atributo manterMoedasSistemaImpl. */
	private IMoedasSistemaService manterMoedasSistemaImpl;

	/**
	 * Get: manterMoedasSistemaImpl.
	 *
	 * @return manterMoedasSistemaImpl
	 */
	public IMoedasSistemaService getManterMoedasSistemaImpl() {
		return manterMoedasSistemaImpl;
	}

	/**
	 * Set: manterMoedasSistemaImpl.
	 *
	 * @param manterMoedasSistemaImpl the manter moedas sistema impl
	 */
	public void setManterMoedasSistemaImpl(
			IMoedasSistemaService manterMoedasSistemaImpl) {
		this.manterMoedasSistemaImpl = manterMoedasSistemaImpl;
	}

	/**
	 * Get: moedaIndicePadraoBancoFiltro.
	 *
	 * @return moedaIndicePadraoBancoFiltro
	 */
	public Integer getMoedaIndicePadraoBancoFiltro() {
		return moedaIndicePadraoBancoFiltro;
	}

	/**
	 * Set: moedaIndicePadraoBancoFiltro.
	 *
	 * @param moedaIndicePadraoBancoFiltro the moeda indice padrao banco filtro
	 */
	public void setMoedaIndicePadraoBancoFiltro(
			Integer moedaIndicePadraoBancoFiltro) {
		this.moedaIndicePadraoBancoFiltro = moedaIndicePadraoBancoFiltro;
	}

	/**
	 * Get: tipoLayoutArquivoFiltro.
	 *
	 * @return tipoLayoutArquivoFiltro
	 */
	public Integer getTipoLayoutArquivoFiltro() {
		return tipoLayoutArquivoFiltro;
	}

	/**
	 * Set: tipoLayoutArquivoFiltro.
	 *
	 * @param tipoLayoutArquivoFiltro the tipo layout arquivo filtro
	 */
	public void setTipoLayoutArquivoFiltro(Integer tipoLayoutArquivoFiltro) {
		this.tipoLayoutArquivoFiltro = tipoLayoutArquivoFiltro;
	}

	/**
	 * Limpar dados.
	 *
	 * @return the string
	 */
	public String limparDados() {

		this.setTipoLayoutArquivoFiltro(null);
		this.setMoedaIndicePadraoBancoFiltro(null);
		listaPesquisa = new ArrayList<MoedaSistemaSaidaDTO>();
		setBtoAcionado(false);

		return MANTER_MOEDAS_SISTEMA;
	}
	
	/**
	 * Voltar consultar.
	 *
	 * @return the string
	 */
	public String voltarConsultar(){
		setItemSelecionadoLista(null);
		return MANTER_MOEDAS_SISTEMA;
	}

	/**
	 * Avancar detalhar.
	 *
	 * @return the string
	 */
	public String avancarDetalhar() {
		try {
			preencheDados();
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(p.getMessage(), MANTER_MOEDAS_SISTEMA, BradescoViewExceptionActionType.ACTION, false);
			return null;
		} 
		
		return "AVANCAR_DETALHAR";

	}

	/**
	 * Preenche dados.
	 */
	private void preencheDados() {
		MoedaSistemaSaidaDTO moedaSistemaSaidaDTO = new MoedaSistemaSaidaDTO();
		MoedaSistemaEntradaDTO moedaSistemaEntradaDTO = new MoedaSistemaEntradaDTO();
		moedaSistemaEntradaDTO.setCdIndicadorEconomicoMoeda(itemSelecionadoLista.getCdIndicadorEconomicoMoeda());
		moedaSistemaEntradaDTO.setCdTipoLayoutArquivo(itemSelecionadoLista.getCdTipoLayoutArquivo());
		moedaSistemaEntradaDTO.setCdIndicadorEconomicoLayout(itemSelecionadoLista.getCdIndicadorEconomicoLayout());
		
		moedaSistemaSaidaDTO = getMoedaSistemaService().detalharMoedaSistema(moedaSistemaEntradaDTO);
		
		setTipoLayout(String.valueOf(moedaSistemaSaidaDTO.getCdTipoLayoutArquivo()));
		setTipoLayoutDesc(moedaSistemaSaidaDTO.getDsTipoLayoutArquivo());
		setCdIndicadorEconomicoLayout(moedaSistemaSaidaDTO.getCdIndicadorEconomicoLayout());
		setMoedaIndicePadraoBanco(String.valueOf(moedaSistemaSaidaDTO.getCdIndicadorEconomicoMoeda()));
		setMoedaIndicePadraoBancoDesc(moedaSistemaSaidaDTO.getDsIndicadorEconomicoMoeda());
		setComplementoInclusao(moedaSistemaSaidaDTO.getNrOperacaoFluxoInclusao().equals("0")? "" : moedaSistemaSaidaDTO.getNrOperacaoFluxoInclusao());
		setComplementoManutencao(moedaSistemaSaidaDTO.getNrOperacaoFluxoManutencao());
		setDataHoraInclusao(moedaSistemaSaidaDTO.getHrInclusaoRegistro());
		setDataHoraManutencao(moedaSistemaSaidaDTO.getHrManutencaoRegistro());
		setTipoCanalInclusao(moedaSistemaSaidaDTO.getCdCanalInclusao() + " - " + moedaSistemaSaidaDTO.getDsCanalInclusao());
		setTipoCanalManutencao(String.valueOf(moedaSistemaSaidaDTO.getCdCanalManutencao()==0? "" :moedaSistemaSaidaDTO.getCdCanalManutencao() + " - " + moedaSistemaSaidaDTO.getDsCanalInclusao()));
		setUsuarioInclusao(String.valueOf(moedaSistemaSaidaDTO.getCdAutenticacaoSegurancaInclusao()));
		setUsuarioManutencao(String.valueOf(moedaSistemaSaidaDTO.getCdAutenticacaoSegurancaManutencao()));
		
	}

	/**
	 * Avancar incluir.
	 *
	 * @return the string
	 */
	public String avancarIncluir() {
		
		setIncluirMoedaIndicePadraoBancoFiltro(null);
		setIncluirTipoLayoutArquivoFiltro(null);
		setMoedaIndicePadraoBanco(null);
		
		return "AVANCAR_INCLUIR";
	}

	/**
	 * Avancar excluir.
	 *
	 * @return the string
	 */
	public String avancarExcluir() {
		try {
			preencheDados();
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(p.getMessage(), MANTER_MOEDAS_SISTEMA, BradescoViewExceptionActionType.ACTION, false);
			return null;
		} 
		return "AVANCAR_EXCLUIR";
	}

	/**
	 * Avancar confirmar.
	 *
	 * @return the string
	 */
	public String avancarConfirmar() {
		
		setMoedaIndicePadraoBancoDesc((String)getListaTipoMoedaEconomicaHash().get(getIncluirMoedaIndicePadraoBancoFiltro()));
		setTipoLayoutDesc((String)getListaTipoLayoutArquivoHash().get(getIncluirTipoLayoutArquivoFiltro()));
		
		return "AVANCAR_CONFIRMAR";
	}

	/**
	 * Voltar incluir.
	 *
	 * @return the string
	 */
	public String voltarIncluir() {
		return "VOLTAR_INCLUIR";
	}

	/**
	 * Confirmar inclusao.
	 *
	 * @return the string
	 */
	public String confirmarInclusao() {
		
		MoedaSistemaEntradaDTO moedaSistemaEntradaDTO = new MoedaSistemaEntradaDTO();
		moedaSistemaEntradaDTO.setCdIndicadorEconomicoMoeda(getIncluirMoedaIndicePadraoBancoFiltro());
		moedaSistemaEntradaDTO.setCdTipoLayoutArquivo(getIncluirTipoLayoutArquivoFiltro());
		moedaSistemaEntradaDTO.setCdIndicadorEconomicoLayout(getMoedaIndicePadraoBanco());
		
		MoedaSistemaSaidaDTO moedaSistemaSaidaDTO = getMoedaSistemaService().incluirMoedaSistema(moedaSistemaEntradaDTO);
		BradescoFacesUtils.addInfoModalMessage("(" + moedaSistemaSaidaDTO.getCodMensagem() + ") "  + moedaSistemaSaidaDTO.getMensagem(), MANTER_MOEDAS_SISTEMA, BradescoViewExceptionActionType.ACTION, false);
		carregaListaSucesso();
		return "";
	}

	/**
	 * Confirmar exclusao.
	 *
	 * @return the string
	 */
	public String confirmarExclusao() {
		
		MoedaSistemaSaidaDTO moedaSistemaSaidaDTO = new MoedaSistemaSaidaDTO();
		MoedaSistemaEntradaDTO moedaSistemaEntradaDTO = new MoedaSistemaEntradaDTO();
		
		moedaSistemaEntradaDTO.setCdIndicadorEconomicoMoeda(itemSelecionadoLista.getCdIndicadorEconomicoMoeda());
		moedaSistemaEntradaDTO.setCdTipoLayoutArquivo(itemSelecionadoLista.getCdTipoLayoutArquivo());
		moedaSistemaEntradaDTO.setCdIndicadorEconomicoLayout(itemSelecionadoLista.getCdIndicadorEconomicoLayout());
		
		moedaSistemaSaidaDTO = getMoedaSistemaService().excluirMoedaSistema(moedaSistemaEntradaDTO);
		BradescoFacesUtils.addInfoModalMessage("(" + moedaSistemaSaidaDTO.getCodMensagem() + ") "  + moedaSistemaSaidaDTO.getMensagem(), MANTER_MOEDAS_SISTEMA, BradescoViewExceptionActionType.ACTION, false);
			
		carregaListaSucesso();
		return "";
	}

	/**
	 * Get: pesquisar.
	 *
	 * @return pesquisar
	 */
	public String getPesquisar() {
		return pesquisar;
	}

	/**
	 * Set: pesquisar.
	 *
	 * @param pesquisar the pesquisar
	 */
	public void setPesquisar(String pesquisar) {
		this.pesquisar = pesquisar;
	}

	

	/**
	 * Get: complementoInclusao.
	 *
	 * @return complementoInclusao
	 */
	public String getComplementoInclusao() {
		return complementoInclusao;
	}

	/**
	 * Set: complementoInclusao.
	 *
	 * @param complementoInclusao the complemento inclusao
	 */
	public void setComplementoInclusao(String complementoInclusao) {
		this.complementoInclusao = complementoInclusao;
	}

	/**
	 * Get: complementoManutencao.
	 *
	 * @return complementoManutencao
	 */
	public String getComplementoManutencao() {
		return complementoManutencao;
	}

	/**
	 * Set: complementoManutencao.
	 *
	 * @param complementoManutencao the complemento manutencao
	 */
	public void setComplementoManutencao(String complementoManutencao) {
		this.complementoManutencao = complementoManutencao;
	}

	/**
	 * Get: dataHoraInclusao.
	 *
	 * @return dataHoraInclusao
	 */
	public String getDataHoraInclusao() {
		return dataHoraInclusao;
	}

	/**
	 * Set: dataHoraInclusao.
	 *
	 * @param dataHoraInclusao the data hora inclusao
	 */
	public void setDataHoraInclusao(String dataHoraInclusao) {
		this.dataHoraInclusao = dataHoraInclusao;
	}

	/**
	 * Get: dataHoraManutencao.
	 *
	 * @return dataHoraManutencao
	 */
	public String getDataHoraManutencao() {
		return dataHoraManutencao;
	}

	/**
	 * Set: dataHoraManutencao.
	 *
	 * @param dataHoraManutencao the data hora manutencao
	 */
	public void setDataHoraManutencao(String dataHoraManutencao) {
		this.dataHoraManutencao = dataHoraManutencao;
	}

	/**
	 * Get: tipoCanalInclusao.
	 *
	 * @return tipoCanalInclusao
	 */
	public String getTipoCanalInclusao() {
		return tipoCanalInclusao;
	}

	/**
	 * Set: tipoCanalInclusao.
	 *
	 * @param tipoCanalInclusao the tipo canal inclusao
	 */
	public void setTipoCanalInclusao(String tipoCanalInclusao) {
		this.tipoCanalInclusao = tipoCanalInclusao;
	}

	/**
	 * Get: tipoCanalManutencao.
	 *
	 * @return tipoCanalManutencao
	 */
	public String getTipoCanalManutencao() {
		return tipoCanalManutencao;
	}

	/**
	 * Set: tipoCanalManutencao.
	 *
	 * @param tipoCanalManutencao the tipo canal manutencao
	 */
	public void setTipoCanalManutencao(String tipoCanalManutencao) {
		this.tipoCanalManutencao = tipoCanalManutencao;
	}

	/**
	 * Get: usuarioInclusao.
	 *
	 * @return usuarioInclusao
	 */
	public String getUsuarioInclusao() {
		return usuarioInclusao;
	}

	/**
	 * Set: usuarioInclusao.
	 *
	 * @param usuarioInclusao the usuario inclusao
	 */
	public void setUsuarioInclusao(String usuarioInclusao) {
		this.usuarioInclusao = usuarioInclusao;
	}

	/**
	 * Get: usuarioManutencao.
	 *
	 * @return usuarioManutencao
	 */
	public String getUsuarioManutencao() {
		return usuarioManutencao;
	}

	/**
	 * Set: usuarioManutencao.
	 *
	 * @param usuarioManutencao the usuario manutencao
	 */
	public void setUsuarioManutencao(String usuarioManutencao) {
		this.usuarioManutencao = usuarioManutencao;
	}

	/**
	 * Get: moedaIndicePadraoBanco.
	 *
	 * @return moedaIndicePadraoBanco
	 */
	public String getMoedaIndicePadraoBanco() {
		return moedaIndicePadraoBanco;
	}

	/**
	 * Set: moedaIndicePadraoBanco.
	 *
	 * @param moedaIndicePadraoBanco the moeda indice padrao banco
	 */
	public void setMoedaIndicePadraoBanco(String moedaIndicePadraoBanco) {
		this.moedaIndicePadraoBanco = moedaIndicePadraoBanco;
	}

	
	/**
	 * Is desab perfil.
	 *
	 * @return true, if is desab perfil
	 */
	public boolean isDesabPerfil() {
		return desabPerfil;
	}

	/**
	 * Set: desabPerfil.
	 *
	 * @param desabPerfil the desab perfil
	 */
	public void setDesabPerfil(boolean desabPerfil) {
		this.desabPerfil = desabPerfil;
	}

	/**
	 * Is desab text remessa.
	 *
	 * @return true, if is desab text remessa
	 */
	public boolean isDesabTextRemessa() {
		return desabTextRemessa;
	}

	/**
	 * Set: desabTextRemessa.
	 *
	 * @param desabTextRemessa the desab text remessa
	 */
	public void setDesabTextRemessa(boolean desabTextRemessa) {
		this.desabTextRemessa = desabTextRemessa;
	}

	/**
	 * Get: logger.
	 *
	 * @return logger
	 */
	public ILogManager getLogger() {
		return logger;
	}

	/**
	 * Set: logger.
	 *
	 * @param logger the logger
	 */
	public void setLogger(ILogManager logger) {
		this.logger = logger;
	}

	/**
	 * Get: moedaIndicePadraoBancoDesc.
	 *
	 * @return moedaIndicePadraoBancoDesc
	 */
	public String getMoedaIndicePadraoBancoDesc() {
		return moedaIndicePadraoBancoDesc;
	}

	/**
	 * Set: moedaIndicePadraoBancoDesc.
	 *
	 * @param moedaIndicePadraoBancoDesc the moeda indice padrao banco desc
	 */
	public void setMoedaIndicePadraoBancoDesc(String moedaIndicePadraoBancoDesc) {
		this.moedaIndicePadraoBancoDesc = moedaIndicePadraoBancoDesc;
	}

	/**
	 * Get: listaPesquisa.
	 *
	 * @return listaPesquisa
	 */
	public List<MoedaSistemaSaidaDTO> getListaPesquisa() {
		return listaPesquisa;
	}

	/**
	 * Set: listaPesquisa.
	 *
	 * @param listaPesquisa the lista pesquisa
	 */
	public void setListaPesquisa(List<MoedaSistemaSaidaDTO> listaPesquisa) {
		this.listaPesquisa = listaPesquisa;
	}

	/**
	 * Get: listaPesquisaControle.
	 *
	 * @return listaPesquisaControle
	 */
	public List<SelectItem> getListaPesquisaControle() {
		return listaPesquisaControle;
	}

	/**
	 * Set: listaPesquisaControle.
	 *
	 * @param listaPesquisaControle the lista pesquisa controle
	 */
	public void setListaPesquisaControle(List<SelectItem> listaPesquisaControle) {
		this.listaPesquisaControle = listaPesquisaControle;
	}

	/**
	 * Is mostra botoes.
	 *
	 * @return true, if is mostra botoes
	 */
	public boolean isMostraBotoes() {
		return mostraBotoes;
	}

	/**
	 * Set: mostraBotoes.
	 *
	 * @param mostraBotoes the mostra botoes
	 */
	public void setMostraBotoes(boolean mostraBotoes) {
		this.mostraBotoes = mostraBotoes;
	}

	/**
	 * Carrega lista.
	 */
	public void carregaLista() {		
		
		listaPesquisa = new ArrayList<MoedaSistemaSaidaDTO>();
		MoedaSistemaEntradaDTO moedaSistemaEntradaDTO = new MoedaSistemaEntradaDTO();
		moedaSistemaEntradaDTO.setCdTipoLayoutArquivo(getTipoLayoutArquivoFiltro());
		moedaSistemaEntradaDTO.setCdIndicadorEconomicoMoeda(getMoedaIndicePadraoBancoFiltro());
		
		setListaPesquisa(getMoedaSistemaService().listarMoedaSistema(moedaSistemaEntradaDTO));
		setBtoAcionado(true);
	}
	
	/**
	 * Carrega lista sucesso.
	 */
	public void carregaListaSucesso() {		
		try {
			listaPesquisa = new ArrayList<MoedaSistemaSaidaDTO>();
			MoedaSistemaEntradaDTO moedaSistemaEntradaDTO = new MoedaSistemaEntradaDTO();
			moedaSistemaEntradaDTO.setCdTipoLayoutArquivo(getTipoLayoutArquivoFiltro());
			moedaSistemaEntradaDTO.setCdIndicadorEconomicoMoeda(getMoedaIndicePadraoBancoFiltro());
			
			setListaPesquisa(getMoedaSistemaService().listarMoedaSistema(moedaSistemaEntradaDTO));
			
		} catch (PdcAdapterFunctionalException e) {
			if(!StringUtils.right(e.getCode(), 8).equals("PGIT0003")){
				this.setListaPesquisa(new ArrayList<MoedaSistemaSaidaDTO>());
				throw e;
			}
		}
	}
	
	/**
	 * Preenche tipo layout arquivo.
	 *
	 * @return the list< select item>
	 */
	@SuppressWarnings("unchecked")
	public List<SelectItem> preencheTipoLayoutArquivo(){
		try{
			listaTipoLayoutArquivo = new ArrayList<SelectItem>();
			List<TipoLayoutArquivoSaidaDTO> listaTipoLayout = new ArrayList<TipoLayoutArquivoSaidaDTO>();
			TipoLayoutArquivoEntradaDTO tipoLoteEntradaDTO = new TipoLayoutArquivoEntradaDTO();		
			tipoLoteEntradaDTO.setCdSituacaoVinculacaoConta(0);
		
			saidaTipoLayoutArquivoSaidaDTO = comboService.listarTipoLayoutArquivo(tipoLoteEntradaDTO);
			listaTipoLayout = saidaTipoLayoutArquivoSaidaDTO.getOcorrencias();
			listaTipoLayoutArquivoHash.clear();
			for(TipoLayoutArquivoSaidaDTO combo : listaTipoLayout){
				listaTipoLayoutArquivoHash.put(combo.getCdTipoLayoutArquivo(), combo.getDsTipoLayoutArquivo());
				listaTipoLayoutArquivo.add(new SelectItem(combo.getCdTipoLayoutArquivo(),combo.getDsTipoLayoutArquivo()));
			}
		}catch(PdcAdapterFunctionalException e){
			listaTipoLayoutArquivo = new ArrayList<SelectItem>();
		}
		
		return listaTipoLayoutArquivo;
	}
	
	/**
	 * Get: listaTipoLayoutArquivo.
	 *
	 * @return listaTipoLayoutArquivo
	 */
	@SuppressWarnings("unchecked")
	public List<SelectItem> getListaTipoLayoutArquivo() {
				
		return listaTipoLayoutArquivo;

	}
	
	/**
	 * Preenche lista moeda economica.
	 *
	 * @return the list< select item>
	 */
	@SuppressWarnings("unchecked")
	public List<SelectItem> preencheListaMoedaEconomica() {
		try{
			listaMoedaEconomica = new ArrayList<SelectItem>();
			List<MoedaEconomicaSaidaDTO> moedaEconomicaLayout = new ArrayList<MoedaEconomicaSaidaDTO>();
			MoedaEconomicaEntradaDTO tipoMoedaEntradaDTO = new MoedaEconomicaEntradaDTO();		
			tipoMoedaEntradaDTO.setNrOcorrencias(100);
			tipoMoedaEntradaDTO.setCdSituacao(0);
		
			moedaEconomicaLayout = comboService.listarMoedaEconomica(tipoMoedaEntradaDTO);
			listaTipoMoedaEconomicaHash.clear();
			for(MoedaEconomicaSaidaDTO combo : moedaEconomicaLayout){
				listaTipoMoedaEconomicaHash.put(combo.getCdIndicadorEconomico(), combo.getDsIndicadorEconomico());
				listaMoedaEconomica.add(new SelectItem(combo.getCdIndicadorEconomico(),combo.getDsIndicadorEconomico()));
			}
		}catch(PdcAdapterFunctionalException e){
			listaMoedaEconomica = new ArrayList<SelectItem>();
		}
		
		return listaMoedaEconomica;
		
	}
	
	/**
	 * Get: listaMoedaEconomica.
	 *
	 * @return listaMoedaEconomica
	 */
	@SuppressWarnings("unchecked")
	public List<SelectItem> getListaMoedaEconomica() {
		
		return listaMoedaEconomica;
	}

	/**
	 * Set: listaTipoLayoutArquivo.
	 *
	 * @param listaTipoLayoutArquivo the lista tipo layout arquivo
	 */
	public void setListaTipoLayoutArquivo(List<SelectItem> listaTipoLayoutArquivo) {
		this.listaTipoLayoutArquivo = listaTipoLayoutArquivo;
	}

	/**
	 * Get: comboService.
	 *
	 * @return comboService
	 */
	public IComboService getComboService() {
		return comboService;
	}

	/**
	 * Set: comboService.
	 *
	 * @param comboService the combo service
	 */
	public void setComboService(IComboService comboService) {
		this.comboService = comboService;
	}

	/**
	 * Get: moedaSistemaService.
	 *
	 * @return moedaSistemaService
	 */
	public IMoedasSistemaService getMoedaSistemaService() {
		return moedaSistemaService;
	}

	/**
	 * Set: moedaSistemaService.
	 *
	 * @param moedaSistemaService the moeda sistema service
	 */
	public void setMoedaSistemaService(IMoedasSistemaService moedaSistemaService) {
		this.moedaSistemaService = moedaSistemaService;
	}

	/**
	 * Get: tipoLayoutDesc.
	 *
	 * @return tipoLayoutDesc
	 */
	public String getTipoLayoutDesc() {
		return tipoLayoutDesc;
	}

	/**
	 * Set: tipoLayoutDesc.
	 *
	 * @param tipoLayoutDesc the tipo layout desc
	 */
	public void setTipoLayoutDesc(String tipoLayoutDesc) {
		this.tipoLayoutDesc = tipoLayoutDesc;
	}

	/**
	 * Get: tipoLayout.
	 *
	 * @return tipoLayout
	 */
	public String getTipoLayout() {
		return tipoLayout;
	}

	/**
	 * Set: tipoLayout.
	 *
	 * @param tipoLayout the tipo layout
	 */
	public void setTipoLayout(String tipoLayout) {
		this.tipoLayout = tipoLayout;
	}

	/**
	 * Get: incluirTipoLayoutArquivoFiltro.
	 *
	 * @return incluirTipoLayoutArquivoFiltro
	 */
	public Integer getIncluirTipoLayoutArquivoFiltro() {
		return incluirTipoLayoutArquivoFiltro;
	}

	/**
	 * Set: incluirTipoLayoutArquivoFiltro.
	 *
	 * @param incluirTipoLayoutArquivoFiltro the incluir tipo layout arquivo filtro
	 */
	public void setIncluirTipoLayoutArquivoFiltro(
			Integer incluirTipoLayoutArquivoFiltro) {
		this.incluirTipoLayoutArquivoFiltro = incluirTipoLayoutArquivoFiltro;
	}

	/**
	 * Get: itemSelecionadoLista.
	 *
	 * @return itemSelecionadoLista
	 */
	public MoedaSistemaSaidaDTO getItemSelecionadoLista() {
		return itemSelecionadoLista;
	}

	/**
	 * Set: itemSelecionadoLista.
	 *
	 * @param itemSelecionadoLista the item selecionado lista
	 */
	public void setItemSelecionadoLista(MoedaSistemaSaidaDTO itemSelecionadoLista) {
		this.itemSelecionadoLista = itemSelecionadoLista;
	}

	/**
	 * Set: listaMoedaEconomica.
	 *
	 * @param listaMoedaEconomica the lista moeda economica
	 */
	public void setListaMoedaEconomica(List<SelectItem> listaMoedaEconomica) {
		this.listaMoedaEconomica = listaMoedaEconomica;
	}

	/**
	 * Get: incluirMoedaIndicePadraoBancoFiltro.
	 *
	 * @return incluirMoedaIndicePadraoBancoFiltro
	 */
	public Integer getIncluirMoedaIndicePadraoBancoFiltro() {
		return incluirMoedaIndicePadraoBancoFiltro;
	}

	/**
	 * Set: incluirMoedaIndicePadraoBancoFiltro.
	 *
	 * @param incluirMoedaIndicePadraoBancoFiltro the incluir moeda indice padrao banco filtro
	 */
	public void setIncluirMoedaIndicePadraoBancoFiltro(
			Integer incluirMoedaIndicePadraoBancoFiltro) {
		this.incluirMoedaIndicePadraoBancoFiltro = incluirMoedaIndicePadraoBancoFiltro;
	}

	/**
	 * Get: cdIndicadorEconomicoLayout.
	 *
	 * @return cdIndicadorEconomicoLayout
	 */
	public String getCdIndicadorEconomicoLayout() {
		return cdIndicadorEconomicoLayout;
	}

	/**
	 * Set: cdIndicadorEconomicoLayout.
	 *
	 * @param cdIndicadorEconomicoLayout the cd indicador economico layout
	 */
	public void setCdIndicadorEconomicoLayout(String cdIndicadorEconomicoLayout) {
		this.cdIndicadorEconomicoLayout = cdIndicadorEconomicoLayout;
	}

	/**
	 * Get: btoAcionado.
	 *
	 * @return btoAcionado
	 */
	public Boolean getBtoAcionado() {
		return btoAcionado;
	}

	/**
	 * Set: btoAcionado.
	 *
	 * @param btoAcionado the bto acionado
	 */
	public void setBtoAcionado(Boolean btoAcionado) {
		this.btoAcionado = btoAcionado;
	}

	/**
	 * Get: listaTipoLayoutArquivoHash.
	 *
	 * @return listaTipoLayoutArquivoHash
	 */
	public Map<Integer, String> getListaTipoLayoutArquivoHash() {
		return listaTipoLayoutArquivoHash;
	}

	/**
	 * Set lista tipo layout arquivo hash.
	 *
	 * @param listaTipoLayoutArquivoHash the lista tipo layout arquivo hash
	 */
	public void setListaTipoLayoutArquivoHash(
			Map<Integer, String> listaTipoLayoutArquivoHash) {
		this.listaTipoLayoutArquivoHash = listaTipoLayoutArquivoHash;
	}

	/**
	 * Get: listaTipoMoedaEconomicaHash.
	 *
	 * @return listaTipoMoedaEconomicaHash
	 */
	public Map<Integer, String> getListaTipoMoedaEconomicaHash() {
		return listaTipoMoedaEconomicaHash;
	}

	/**
	 * Set lista tipo moeda economica hash.
	 *
	 * @param listaTipoMoedaEconomicaHash the lista tipo moeda economica hash
	 */
	public void setListaTipoMoedaEconomicaHash(
			Map<Integer, String> listaTipoMoedaEconomicaHash) {
		this.listaTipoMoedaEconomicaHash = listaTipoMoedaEconomicaHash;
	}

	/**
	 * Nome: setSaidaTipoLayoutArquivoSaidaDTO
	 *
	 * @exception
	 * @throws
	 * @param saidaTipoLayoutArquivoSaidaDTO
	 */
	public void setSaidaTipoLayoutArquivoSaidaDTO(
			TipoLayoutArquivoSaidaDTO saidaTipoLayoutArquivoSaidaDTO) {
		this.saidaTipoLayoutArquivoSaidaDTO = saidaTipoLayoutArquivoSaidaDTO;
	}

	/**
	 * Nome: getSaidaTipoLayoutArquivoSaidaDTO
	 *
	 * @exception
	 * @throws
	 * @return saidaTipoLayoutArquivoSaidaDTO
	 */
	public TipoLayoutArquivoSaidaDTO getSaidaTipoLayoutArquivoSaidaDTO() {
		return saidaTipoLayoutArquivoSaidaDTO;
	}
	
	
}
