package br.com.bradesco.web.pgit.service.business.manterlimitescontrato.bean;


/**
 * Arquivo criado em 22/09/15.
 */
public class ConsultarLimiteContaSaidaDTO {

    private String codMensagem;

    private String mensagem;

    private String cdUsuarioInclusao;

    private String cdUsuarioInclusaoExterno;

    private String hrInclusaoRegistro;

    private String cdOperacaoCanalInclusao;

    private Integer cdTipoCanalInclusao;
    
    private String dsTipoCanalInclusao;

    private String cdUsuarioManutencao;

    private String cdUsuarioManutencaoExterno;

    private String hrManutencaoRegistro;

    private String cdOperacaoCanalManutencao;

    private Integer cdTipoCanalManutencao;
    
    private String dsTipoCanalManutencao;
    
    public String getUsuarioInclusaoFormatado() {
    	if("".equals(getCdUsuarioInclusao()) || "".equals(getCdUsuarioInclusaoExterno())){
			return getCdUsuarioInclusao() + getCdUsuarioInclusaoExterno();
		}
        return cdUsuarioInclusao + " - " + cdUsuarioInclusaoExterno;
    }
    
    public String getCdOperacaoCanalManutencaoFormatado() {
    	if("0".equals(getCdOperacaoCanalManutencao())){
    		return "";
    	}
    	return getCdOperacaoCanalManutencao();
    }
    
    public String getCdTipoCanalInclusaoFormatado() {
        if(getCdTipoCanalInclusao() == null || getCdTipoCanalInclusao() == 0){
            return "";
        }
        return getCdTipoCanalInclusao().toString() + " - " + getDsTipoCanalInclusao();
    }
    
    public String getCdTipoCanalManutencaoFormatado() {
        if(getCdTipoCanalManutencao() == null || getCdTipoCanalManutencao() == 0){
            return "";
        }
        return getCdTipoCanalManutencao().toString() + " - " + getDsTipoCanalManutencao();
    }
    
    public String getCdOperacaoCanalIncluisaoFormatado() {
        if("0".equals(getCdOperacaoCanalInclusao())){
            return "";
        }
        return getCdOperacaoCanalInclusao();
    }
    
    public String getUsuarioManutencaoFormatado() {
    	if("".equals(getCdUsuarioManutencao()) || "".equals(getCdUsuarioManutencaoExterno())){
			return getCdUsuarioManutencao() + getCdUsuarioManutencaoExterno();
		}
    	return cdUsuarioManutencao + " - " + cdUsuarioManutencaoExterno;
    }

    public void setCodMensagem(String codMensagem) {
        this.codMensagem = codMensagem;
    }

    public String getCodMensagem() {
        return this.codMensagem;
    }

    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }

    public String getMensagem() {
        return this.mensagem;
    }

    public void setCdUsuarioInclusao(String cdUsuarioInclusao) {
        this.cdUsuarioInclusao = cdUsuarioInclusao;
    }

    public String getCdUsuarioInclusao() {
        return this.cdUsuarioInclusao;
    }

    public void setCdUsuarioInclusaoExterno(String cdUsuarioInclusaoExterno) {
        this.cdUsuarioInclusaoExterno = cdUsuarioInclusaoExterno;
    }

    public String getCdUsuarioInclusaoExterno() {
        return this.cdUsuarioInclusaoExterno;
    }

    public void setHrInclusaoRegistro(String hrInclusaoRegistro) {
        this.hrInclusaoRegistro = hrInclusaoRegistro;
    }

    public String getHrInclusaoRegistro() {
        return this.hrInclusaoRegistro;
    }

    public void setCdOperacaoCanalInclusao(String cdOperacaoCanalInclusao) {
        this.cdOperacaoCanalInclusao = cdOperacaoCanalInclusao;
    }

    public String getCdOperacaoCanalInclusao() {
        return this.cdOperacaoCanalInclusao;
    }

    public void setCdTipoCanalInclusao(Integer cdTipoCanalInclusao) {
        this.cdTipoCanalInclusao = cdTipoCanalInclusao;
    }

    public Integer getCdTipoCanalInclusao() {
        return this.cdTipoCanalInclusao;
    }

    public void setCdUsuarioManutencao(String cdUsuarioManutencao) {
        this.cdUsuarioManutencao = cdUsuarioManutencao;
    }

    public String getCdUsuarioManutencao() {
        return this.cdUsuarioManutencao;
    }

    public void setCdUsuarioManutencaoExterno(String cdUsuarioManutencaoExterno) {
        this.cdUsuarioManutencaoExterno = cdUsuarioManutencaoExterno;
    }

    public String getCdUsuarioManutencaoExterno() {
        return this.cdUsuarioManutencaoExterno;
    }

    public void setHrManutencaoRegistro(String hrManutencaoRegistro) {
        this.hrManutencaoRegistro = hrManutencaoRegistro;
    }

    public String getHrManutencaoRegistro() {
        return this.hrManutencaoRegistro;
    }

    public void setCdOperacaoCanalManutencao(String cdOperacaoCanalManutencao) {
        this.cdOperacaoCanalManutencao = cdOperacaoCanalManutencao;
    }

    public String getCdOperacaoCanalManutencao() {
        return this.cdOperacaoCanalManutencao;
    }

    public void setCdTipoCanalManutencao(Integer cdTipoCanalManutencao) {
        this.cdTipoCanalManutencao = cdTipoCanalManutencao;
    }

    public Integer getCdTipoCanalManutencao() {
        return this.cdTipoCanalManutencao;
    }

    /**
     * Nome: getDsTipoCanalInclusao
     *
     * @return dsTipoCanalInclusao
     */
    public String getDsTipoCanalInclusao() {
        return dsTipoCanalInclusao;
    }

    /**
     * Nome: setDsTipoCanalInclusao
     *
     * @param dsTipoCanalInclusao
     */
    public void setDsTipoCanalInclusao(String dsTipoCanalInclusao) {
        this.dsTipoCanalInclusao = dsTipoCanalInclusao;
    }

    /**
     * Nome: getDsTipoCanalManutencao
     *
     * @return dsTipoCanalManutencao
     */
    public String getDsTipoCanalManutencao() {
        return dsTipoCanalManutencao;
    }

    /**
     * Nome: setDsTipoCanalManutencao
     *
     * @param dsTipoCanalManutencao
     */
    public void setDsTipoCanalManutencao(String dsTipoCanalManutencao) {
        this.dsTipoCanalManutencao = dsTipoCanalManutencao;
    }

}