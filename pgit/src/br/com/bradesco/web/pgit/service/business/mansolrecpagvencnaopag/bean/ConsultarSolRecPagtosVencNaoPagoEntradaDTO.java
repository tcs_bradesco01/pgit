/*
 * Nome: br.com.bradesco.web.pgit.service.business.mansolrecpagvencnaopag.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mansolrecpagvencnaopag.bean;

/**
 * Nome: ConsultarSolRecPagtosVencNaoPagoEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarSolRecPagtosVencNaoPagoEntradaDTO{
    
    /** Atributo nrOcorrencias. */
    private Integer nrOcorrencias;
    
    /** Atributo cdFinalidadeRecuperacao. */
    private Integer cdFinalidadeRecuperacao;
    
    /** Atributo cdSelecaoRecuperacao. */
    private Integer cdSelecaoRecuperacao;
    
    /** Atributo cdSolicitacaoPagamento. */
    private Integer cdSolicitacaoPagamento;
    
    /** Atributo cdPessoaJuridicaContrato. */
    private Long cdPessoaJuridicaContrato;
    
    /** Atributo cdTipoContratoNegocio. */
    private Integer cdTipoContratoNegocio;
    
    /** Atributo nrSequenciaContratoNegocio. */
    private Long nrSequenciaContratoNegocio;
    
    /** Atributo cdBancoDebito. */
    private Integer cdBancoDebito;
    
    /** Atributo cdAgenciaDebito. */
    private Integer cdAgenciaDebito;
    
    /** Atributo cdDigitoAgenciaDebito. */
    private Integer cdDigitoAgenciaDebito;
    
    /** Atributo cdContaDebito. */
    private Long cdContaDebito;
    
    /** Atributo cdDigitoContaDebito. */
    private String cdDigitoContaDebito;
    
    /** Atributo cdTipoContaDebito. */
    private Integer cdTipoContaDebito;
    
    /** Atributo dtInicioPesquisa. */
    private String dtInicioPesquisa;
    
    /** Atributo dtFimPesquisa. */
    private String dtFimPesquisa;
    
    /** Atributo cdProduto. */
    private Integer cdProduto;
    
    /** Atributo cdProdutoRelacionado. */
    private Integer cdProdutoRelacionado;
    
    /** Atributo cdSituacaoSolicitacaoPagamento. */
    private Integer cdSituacaoSolicitacaoPagamento;
    
    /** Atributo cdMotivoSolicitacao. */
    private Integer cdMotivoSolicitacao;
	
	/**
	 * Get: cdAgenciaDebito.
	 *
	 * @return cdAgenciaDebito
	 */
	public Integer getCdAgenciaDebito() {
		return cdAgenciaDebito;
	}
	
	/**
	 * Set: cdAgenciaDebito.
	 *
	 * @param cdAgenciaDebito the cd agencia debito
	 */
	public void setCdAgenciaDebito(Integer cdAgenciaDebito) {
		this.cdAgenciaDebito = cdAgenciaDebito;
	}
	
	/**
	 * Get: cdBancoDebito.
	 *
	 * @return cdBancoDebito
	 */
	public Integer getCdBancoDebito() {
		return cdBancoDebito;
	}
	
	/**
	 * Set: cdBancoDebito.
	 *
	 * @param cdBancoDebito the cd banco debito
	 */
	public void setCdBancoDebito(Integer cdBancoDebito) {
		this.cdBancoDebito = cdBancoDebito;
	}
	
	/**
	 * Get: cdContaDebito.
	 *
	 * @return cdContaDebito
	 */
	public Long getCdContaDebito() {
		return cdContaDebito;
	}
	
	/**
	 * Set: cdContaDebito.
	 *
	 * @param cdContaDebito the cd conta debito
	 */
	public void setCdContaDebito(Long cdContaDebito) {
		this.cdContaDebito = cdContaDebito;
	}
	
	/**
	 * Get: cdDigitoAgenciaDebito.
	 *
	 * @return cdDigitoAgenciaDebito
	 */
	public Integer getCdDigitoAgenciaDebito() {
		return cdDigitoAgenciaDebito;
	}
	
	/**
	 * Set: cdDigitoAgenciaDebito.
	 *
	 * @param cdDigitoAgenciaDebito the cd digito agencia debito
	 */
	public void setCdDigitoAgenciaDebito(Integer cdDigitoAgenciaDebito) {
		this.cdDigitoAgenciaDebito = cdDigitoAgenciaDebito;
	}
	
	/**
	 * Get: cdDigitoContaDebito.
	 *
	 * @return cdDigitoContaDebito
	 */
	public String getCdDigitoContaDebito() {
		return cdDigitoContaDebito;
	}
	
	/**
	 * Set: cdDigitoContaDebito.
	 *
	 * @param cdDigitoContaDebito the cd digito conta debito
	 */
	public void setCdDigitoContaDebito(String cdDigitoContaDebito) {
		this.cdDigitoContaDebito = cdDigitoContaDebito;
	}
	
	/**
	 * Get: cdFinalidadeRecuperacao.
	 *
	 * @return cdFinalidadeRecuperacao
	 */
	public Integer getCdFinalidadeRecuperacao() {
		return cdFinalidadeRecuperacao;
	}
	
	/**
	 * Set: cdFinalidadeRecuperacao.
	 *
	 * @param cdFinalidadeRecuperacao the cd finalidade recuperacao
	 */
	public void setCdFinalidadeRecuperacao(Integer cdFinalidadeRecuperacao) {
		this.cdFinalidadeRecuperacao = cdFinalidadeRecuperacao;
	}
	
	/**
	 * Get: cdMotivoSolicitacao.
	 *
	 * @return cdMotivoSolicitacao
	 */
	public Integer getCdMotivoSolicitacao() {
		return cdMotivoSolicitacao;
	}
	
	/**
	 * Set: cdMotivoSolicitacao.
	 *
	 * @param cdMotivoSolicitacao the cd motivo solicitacao
	 */
	public void setCdMotivoSolicitacao(Integer cdMotivoSolicitacao) {
		this.cdMotivoSolicitacao = cdMotivoSolicitacao;
	}
	
	/**
	 * Get: cdPessoaJuridicaContrato.
	 *
	 * @return cdPessoaJuridicaContrato
	 */
	public Long getCdPessoaJuridicaContrato() {
		return cdPessoaJuridicaContrato;
	}
	
	/**
	 * Set: cdPessoaJuridicaContrato.
	 *
	 * @param cdPessoaJuridicaContrato the cd pessoa juridica contrato
	 */
	public void setCdPessoaJuridicaContrato(Long cdPessoaJuridicaContrato) {
		this.cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
	}
	
	/**
	 * Get: cdProduto.
	 *
	 * @return cdProduto
	 */
	public Integer getCdProduto() {
		return cdProduto;
	}
	
	/**
	 * Set: cdProduto.
	 *
	 * @param cdProduto the cd produto
	 */
	public void setCdProduto(Integer cdProduto) {
		this.cdProduto = cdProduto;
	}
	
	/**
	 * Get: cdProdutoRelacionado.
	 *
	 * @return cdProdutoRelacionado
	 */
	public Integer getCdProdutoRelacionado() {
		return cdProdutoRelacionado;
	}
	
	/**
	 * Set: cdProdutoRelacionado.
	 *
	 * @param cdProdutoRelacionado the cd produto relacionado
	 */
	public void setCdProdutoRelacionado(Integer cdProdutoRelacionado) {
		this.cdProdutoRelacionado = cdProdutoRelacionado;
	}
	
	/**
	 * Get: cdSelecaoRecuperacao.
	 *
	 * @return cdSelecaoRecuperacao
	 */
	public Integer getCdSelecaoRecuperacao() {
		return cdSelecaoRecuperacao;
	}
	
	/**
	 * Set: cdSelecaoRecuperacao.
	 *
	 * @param cdSelecaoRecuperacao the cd selecao recuperacao
	 */
	public void setCdSelecaoRecuperacao(Integer cdSelecaoRecuperacao) {
		this.cdSelecaoRecuperacao = cdSelecaoRecuperacao;
	}
	
	/**
	 * Get: cdSituacaoSolicitacaoPagamento.
	 *
	 * @return cdSituacaoSolicitacaoPagamento
	 */
	public Integer getCdSituacaoSolicitacaoPagamento() {
		return cdSituacaoSolicitacaoPagamento;
	}
	
	/**
	 * Set: cdSituacaoSolicitacaoPagamento.
	 *
	 * @param cdSituacaoSolicitacaoPagamento the cd situacao solicitacao pagamento
	 */
	public void setCdSituacaoSolicitacaoPagamento(
			Integer cdSituacaoSolicitacaoPagamento) {
		this.cdSituacaoSolicitacaoPagamento = cdSituacaoSolicitacaoPagamento;
	}
	
	/**
	 * Get: cdSolicitacaoPagamento.
	 *
	 * @return cdSolicitacaoPagamento
	 */
	public Integer getCdSolicitacaoPagamento() {
		return cdSolicitacaoPagamento;
	}
	
	/**
	 * Set: cdSolicitacaoPagamento.
	 *
	 * @param cdSolicitacaoPagamento the cd solicitacao pagamento
	 */
	public void setCdSolicitacaoPagamento(Integer cdSolicitacaoPagamento) {
		this.cdSolicitacaoPagamento = cdSolicitacaoPagamento;
	}
	
	/**
	 * Get: cdTipoContaDebito.
	 *
	 * @return cdTipoContaDebito
	 */
	public Integer getCdTipoContaDebito() {
		return cdTipoContaDebito;
	}
	
	/**
	 * Set: cdTipoContaDebito.
	 *
	 * @param cdTipoContaDebito the cd tipo conta debito
	 */
	public void setCdTipoContaDebito(Integer cdTipoContaDebito) {
		this.cdTipoContaDebito = cdTipoContaDebito;
	}
	
	/**
	 * Get: cdTipoContratoNegocio.
	 *
	 * @return cdTipoContratoNegocio
	 */
	public Integer getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}
	
	/**
	 * Set: cdTipoContratoNegocio.
	 *
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}
	
	/**
	 * Get: dtFimPesquisa.
	 *
	 * @return dtFimPesquisa
	 */
	public String getDtFimPesquisa() {
		return dtFimPesquisa;
	}
	
	/**
	 * Set: dtFimPesquisa.
	 *
	 * @param dtFimPesquisa the dt fim pesquisa
	 */
	public void setDtFimPesquisa(String dtFimPesquisa) {
		this.dtFimPesquisa = dtFimPesquisa;
	}
	
	/**
	 * Get: dtInicioPesquisa.
	 *
	 * @return dtInicioPesquisa
	 */
	public String getDtInicioPesquisa() {
		return dtInicioPesquisa;
	}
	
	/**
	 * Set: dtInicioPesquisa.
	 *
	 * @param dtInicioPesquisa the dt inicio pesquisa
	 */
	public void setDtInicioPesquisa(String dtInicioPesquisa) {
		this.dtInicioPesquisa = dtInicioPesquisa;
	}
	
	/**
	 * Get: nrOcorrencias.
	 *
	 * @return nrOcorrencias
	 */
	public Integer getNrOcorrencias() {
		return nrOcorrencias;
	}
	
	/**
	 * Set: nrOcorrencias.
	 *
	 * @param nrOcorrencias the nr ocorrencias
	 */
	public void setNrOcorrencias(Integer nrOcorrencias) {
		this.nrOcorrencias = nrOcorrencias;
	}
	
	/**
	 * Get: nrSequenciaContratoNegocio.
	 *
	 * @return nrSequenciaContratoNegocio
	 */
	public Long getNrSequenciaContratoNegocio() {
		return nrSequenciaContratoNegocio;
	}
	
	/**
	 * Set: nrSequenciaContratoNegocio.
	 *
	 * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
	 */
	public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}
    
    
}
