/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/implementation.ftl,v $
 * $Id: implementation.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: implementation.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */
 
package br.com.bradesco.web.pgit.service.business.mansolrecpagvencnaopag.impl;

import java.util.ArrayList;
import java.util.List;

import br.com.bradesco.web.pgit.service.business.mansolrecpagvencnaopag.IManSolRecPagVencNaoPagService;
import br.com.bradesco.web.pgit.service.business.mansolrecpagvencnaopag.IManSolRecPagVencNaoPagServiceConstants;
import br.com.bradesco.web.pgit.service.business.mansolrecpagvencnaopag.bean.ConsultarDetalhesSolicitacaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mansolrecpagvencnaopag.bean.ConsultarDetalhesSolicitacaoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mansolrecpagvencnaopag.bean.ConsultarPagtosSolicitacaoRecuperacaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mansolrecpagvencnaopag.bean.ConsultarPagtosSolicitacaoRecuperacaoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mansolrecpagvencnaopag.bean.ConsultarSolRecPagtosVencNaoPagoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mansolrecpagvencnaopag.bean.ConsultarSolRecPagtosVencNaoPagoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mansolrecpagvencnaopag.bean.ExcluirSolRecuperacaoPagtosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mansolrecpagvencnaopag.bean.ExcluirSolRecuperacaoPagtosSaidaDTO;
import br.com.bradesco.web.pgit.service.data.pdc.FactoryAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.conpagtossolrecvennaopago.request.ConsultarPagtosSolicitacaoRecuperacaoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.conpagtossolrecvennaopago.response.ConsultarPagtosSolicitacaoRecuperacaoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.consultardetalhessolicitacao.request.ConsultarDetalhesSolicitacaoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultardetalhessolicitacao.response.ConsultarDetalhesSolicitacaoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.consultarsolrecpagtosvencnaopago.request.ConsultarSolRecPagtosVencNaoPagoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarsolrecpagtosvencnaopago.response.ConsultarSolRecPagtosVencNaoPagoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.excluirsolrecuperacaopagtos.request.ExcluirSolRecuperacaoPagtosRequest;
import br.com.bradesco.web.pgit.service.data.pdc.excluirsolrecuperacaopagtos.response.ExcluirSolRecuperacaoPagtosResponse;
import br.com.bradesco.web.pgit.utils.CpfCnpjUtils;
import br.com.bradesco.web.pgit.utils.PgitUtil;

/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Implementa��o do adaptador: ManSolRecPagVencNaoPag
 * </p>
 * 
 * @comment C�DIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public class ManSolRecPagVencNaoPagServiceImpl implements IManSolRecPagVencNaoPagService {
	
	/** Atributo factoryAdapter. */
	private FactoryAdapter factoryAdapter;
	
    /**
     * Get: factoryAdapter.
     *
     * @return factoryAdapter
     */
    public FactoryAdapter getFactoryAdapter() {
		return factoryAdapter;
	}

	/**
	 * Set: factoryAdapter.
	 *
	 * @param factoryAdapter the factory adapter
	 */
	public void setFactoryAdapter(FactoryAdapter factoryAdapter) {
		this.factoryAdapter = factoryAdapter;
	}

	/**
     * Construtor.
     */
    public ManSolRecPagVencNaoPagServiceImpl() {
       
    }

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.mansolrecpagvencnaopag.IManSolRecPagVencNaoPagService#consultarSolRecPagtosVencNaoPago(br.com.bradesco.web.pgit.service.business.mansolrecpagvencnaopag.bean.ConsultarSolRecPagtosVencNaoPagoEntradaDTO)
	 */
	public List<ConsultarSolRecPagtosVencNaoPagoSaidaDTO> consultarSolRecPagtosVencNaoPago(ConsultarSolRecPagtosVencNaoPagoEntradaDTO entradaDTO) {
	       List<ConsultarSolRecPagtosVencNaoPagoSaidaDTO> listaSaida = new ArrayList<ConsultarSolRecPagtosVencNaoPagoSaidaDTO>();

	        ConsultarSolRecPagtosVencNaoPagoRequest request = new ConsultarSolRecPagtosVencNaoPagoRequest();
	        request.setNrOcorrencias(IManSolRecPagVencNaoPagServiceConstants.NUMERO_OCORRENCIAS_CONSULTAR);
	        request.setCdFinalidadeRecuperacao(PgitUtil.verificaIntegerNulo(entradaDTO.getCdFinalidadeRecuperacao()));
	        request.setCdSelecaoRecuperacao(PgitUtil.verificaIntegerNulo(entradaDTO.getCdSelecaoRecuperacao()));
	        request.setCdSolicitacaoPagamento(PgitUtil.verificaIntegerNulo(entradaDTO.getCdSolicitacaoPagamento()));
	        request.setCdPessoaJuridicaContrato(PgitUtil.verificaLongNulo(entradaDTO.getCdPessoaJuridicaContrato()));
	        request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoContratoNegocio()));
	        request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(entradaDTO.getNrSequenciaContratoNegocio()));
	        request.setCdBancoDebito(PgitUtil.verificaIntegerNulo(entradaDTO.getCdBancoDebito()));
	        request.setCdAgenciaDebito(PgitUtil.verificaIntegerNulo(entradaDTO.getCdAgenciaDebito()));
	        request.setCdDigitoAgenciaDebito(PgitUtil.verificaIntegerNulo(entradaDTO.getCdDigitoAgenciaDebito()));
	        request.setCdContaDebito(PgitUtil.verificaLongNulo(entradaDTO.getCdContaDebito()));
	        request.setCdDigitoContaDebito(PgitUtil.verificaStringNula(entradaDTO.getCdDigitoContaDebito()));
	        request.setCdTipoContaDebito(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoContaDebito()));
	        request.setDtInicioPesquisa(PgitUtil.verificaStringNula(entradaDTO.getDtInicioPesquisa()));
	        request.setDtFimPesquisa(PgitUtil.verificaStringNula(entradaDTO.getDtFimPesquisa()));
	        request.setCdProduto(PgitUtil.verificaIntegerNulo(entradaDTO.getCdProduto()));
	        request.setCdProdutoRelacionado(PgitUtil.verificaIntegerNulo(entradaDTO.getCdProdutoRelacionado()));
	        request.setCdSituacaoSolicitacaoPagamento(PgitUtil.verificaIntegerNulo(entradaDTO.getCdSituacaoSolicitacaoPagamento()));
	        request.setCdMotivoSolicitacao(PgitUtil.verificaIntegerNulo(entradaDTO.getCdMotivoSolicitacao()));

	        ConsultarSolRecPagtosVencNaoPagoResponse response = getFactoryAdapter().getConsultarSolRecPagtosVencNaoPagoPDCAdapter().invokeProcess(request);
	        
	        ConsultarSolRecPagtosVencNaoPagoSaidaDTO saidaDTO;
	        
	        for (int i=0; i<response.getOcorrenciasCount(); i++ ){
	        	saidaDTO = new ConsultarSolRecPagtosVencNaoPagoSaidaDTO();
		        saidaDTO.setCodMensagem(response.getCodMensagem());
		        saidaDTO.setMensagem(response.getMensagem());
		        saidaDTO.setNumeroConsultas(response.getNumeroConsultas());
		        saidaDTO.setCdSolicitacaoPagamento(response.getOcorrencias(i).getCdSolicitacaoPagamento());
		        saidaDTO.setNrSolicitacaoPagamento(response.getOcorrencias(i).getNrSolicitacaoPagamento());
		        saidaDTO.setCdCpfCnpjParticipante(response.getOcorrencias(i).getCdCpfCnpjParticipante());
		        saidaDTO.setCdCpfCnpjParticipanteFormatado(PgitUtil.formatCpfCnpj(response.getOcorrencias(i).getCdCpfCnpjParticipante()));
		        saidaDTO.setDsRazaoSocial(response.getOcorrencias(i).getDsRazaoSocial());
		        saidaDTO.setCdPessoaJuridicaContrato(response.getOcorrencias(i).getCdPessoaJuridicaContrato());
		        saidaDTO.setDsEmpresa(response.getOcorrencias(i).getDsEmpresa());
		        saidaDTO.setCdTipoContratoNegocio(response.getOcorrencias(i).getCdTipoContratoNegocio());
		        saidaDTO.setNrSequenciaContratoNegocio(response.getOcorrencias(i).getNrSequenciaContratoNegocio());
		        saidaDTO.setDsDataSolicitacao(response.getOcorrencias(i).getDsDataSolicitacao());
		        saidaDTO.setDsHoraSolicitacao(response.getOcorrencias(i).getDsHoraSolicitacao());
		        saidaDTO.setCdProdutoServicoOperacao(response.getOcorrencias(i).getCdProdutoServicoOperacao());
		        saidaDTO.setDsResumoProdutoServico(response.getOcorrencias(i).getDsResumoProdutoServico());
		        saidaDTO.setCdProdutoServicoRelacionado(response.getOcorrencias(i).getCdProdutoServicoRelacionado());
		        saidaDTO.setDsResumoServicoRelacionado(response.getOcorrencias(i).getDsResumoServicoRelacionado());
		        saidaDTO.setCdSituacaoSolicitacaoPagamento(response.getOcorrencias(i).getCdSituacaoSolicitacaoPagamento());
		        saidaDTO.setDsSituacaoSolicitacao(response.getOcorrencias(i).getDsSituacaoSolicitacao());
		        saidaDTO.setCdMotivoSolicitacao(response.getOcorrencias(i).getCdMotivoSolicitacao());
		        saidaDTO.setDsMotivoSolicitacao(response.getOcorrencias(i).getDsMotivoSolicitacao());
		        saidaDTO.setDtRecuperacao(response.getOcorrencias(i).getDtRecuperacao());
		        
		        listaSaida.add(saidaDTO);
	        }

	        return listaSaida;
		
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.mansolrecpagvencnaopag.IManSolRecPagVencNaoPagService#excluirSolRecuperacaoPagtos(br.com.bradesco.web.pgit.service.business.mansolrecpagvencnaopag.bean.ExcluirSolRecuperacaoPagtosEntradaDTO)
	 */
	public ExcluirSolRecuperacaoPagtosSaidaDTO excluirSolRecuperacaoPagtos(ExcluirSolRecuperacaoPagtosEntradaDTO entradaDTO) {
		ExcluirSolRecuperacaoPagtosSaidaDTO saidaDTO = new ExcluirSolRecuperacaoPagtosSaidaDTO();
		ExcluirSolRecuperacaoPagtosRequest request = new ExcluirSolRecuperacaoPagtosRequest();
		ExcluirSolRecuperacaoPagtosResponse response = new ExcluirSolRecuperacaoPagtosResponse();
		
		request.setCdSolicitacaoPagamento(PgitUtil.verificaIntegerNulo(entradaDTO.getCdSolicitacaoPagamento()));
		request.setNrSolicitacaoPagamento(PgitUtil.verificaIntegerNulo(entradaDTO.getNrSolicitacaoPagamento()));
		
		response = getFactoryAdapter().getExcluirSolRecuperacaoPagtosPDCAdapter().invokeProcess(request);
		
		saidaDTO.setCodMensagem(response.getCodMensagem());
		saidaDTO.setMensagem(response.getMensagem());
		
		return saidaDTO;
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.mansolrecpagvencnaopag.IManSolRecPagVencNaoPagService#conPagtosSolRecVenNaoPago(br.com.bradesco.web.pgit.service.business.mansolrecpagvencnaopag.bean.ConsultarPagtosSolicitacaoRecuperacaoEntradaDTO)
	 */
	public List<ConsultarPagtosSolicitacaoRecuperacaoSaidaDTO> conPagtosSolRecVenNaoPago(ConsultarPagtosSolicitacaoRecuperacaoEntradaDTO entradaDTO) {
        ConsultarPagtosSolicitacaoRecuperacaoRequest request = new ConsultarPagtosSolicitacaoRecuperacaoRequest();
        request.setNrOcorrencias(IManSolRecPagVencNaoPagServiceConstants.NUMERO_OCORRENCIAS_DETALHAR);
        request.setCdSolicitacaoPagamento(PgitUtil.verificaIntegerNulo(entradaDTO.getCdSolicitacaoPagamento()));
        request.setNrSolicitacaoPagamento(PgitUtil.verificaIntegerNulo(entradaDTO.getNrSolicitacaoPagamento()));
       
       /* request.setCdPessoaJuridicaContrato(PgitUtil.verificaLongNulo(entradaDTO.getCdPessoaJuridicaContrato()));
        request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoContratoNegocio()));
        request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(entradaDTO.getNrSequenciaContratoNegocio()));
        request.setCdControlePagamento(PgitUtil.verificaStringNula(entradaDTO.getCdControlePagamento()));
        request.setCdModalidadePagamento(PgitUtil.verificaIntegerNulo(entradaDTO.getCdModalidadePagamento()));*/

        ConsultarPagtosSolicitacaoRecuperacaoResponse response = getFactoryAdapter().getConPagtosSolRecVenNaoPagoPDCAdapter().invokeProcess(request);
        
        List<ConsultarPagtosSolicitacaoRecuperacaoSaidaDTO> listaSaida = new ArrayList<ConsultarPagtosSolicitacaoRecuperacaoSaidaDTO>();
        
        ConsultarPagtosSolicitacaoRecuperacaoSaidaDTO registro;
        
        for ( int i=0; i<response.getOcorrenciasCount(); i++ ){
        	registro = new ConsultarPagtosSolicitacaoRecuperacaoSaidaDTO();
        	
        	registro.setCodMensagem(response.getCodMensagem());
        	registro.setMensagem(response.getMensagem());

        	registro.setCdCorpoCpfCnpj(response.getOcorrencias(i).getCdCorpoCpfCnpj());
        	registro.setCdFilialCpfCnpj(response.getOcorrencias(i).getCdFilialCpfCnpj());
        	registro.setCdDigitoCpfCnpj(response.getOcorrencias(i).getCdDigitoCpfCnpj());
        	registro.setCdPessoaJuridicaContrato(response.getOcorrencias(i).getCdPessoaJuridicaContrato());
        	registro.setDsRazaoSocial(response.getOcorrencias(i).getDsRazaoSocial());
        	registro.setCdEmpresa(response.getOcorrencias(i).getCdEmpresa());
        	registro.setDsEmpresa(response.getOcorrencias(i).getDsEmpresa());
        	registro.setCdTipoContratoNegocio(response.getOcorrencias(i).getCdTipoContratoNegocio());
        	registro.setDsTipoContrato(response.getOcorrencias(i).getDsTipoContrato());
        	registro.setNrSequenciaContratoNegocio(response.getOcorrencias(i).getNrSequenciaContratoNegocio());
        	registro.setCdProdutoServicoOper(response.getOcorrencias(i).getCdProdutoServicoOper());
        	registro.setDsOperacaoProdutoServico(response.getOcorrencias(i).getDsOperacaoProdutoServico());
        	registro.setCdProdutoServicoRelacionado(response.getOcorrencias(i).getCdProdutoServicoRelacionado());
        	registro.setDsOperacaoProdutoRelacionado(response.getOcorrencias(i).getDsOperacaoProdutoRelacionado());
        	registro.setCdControlePagamento(response.getOcorrencias(i).getCdControlePagamento());
        	registro.setDtCredito(response.getOcorrencias(i).getDtCredito());
        	registro.setVlrEfetivacaoPagamento(response.getOcorrencias(i).getVlrEfetivacaoPagamento());
        	registro.setCdInscricaoFavorecido(response.getOcorrencias(i).getCdInscricaoFavorecido());
	        registro.setDsFavorecido(response.getOcorrencias(i).getDsFavorecido());
	        registro.setCdBancoDebito(response.getOcorrencias(i).getCdBancoDebito());
	        registro.setCdAgenciaDebito(response.getOcorrencias(i).getCdAgenciaDebito());
	        registro.setCdDigitoAgenciaDebito(response.getOcorrencias(i).getCdDigitoAgenciaDebito());
	        registro.setCdContaDebito(response.getOcorrencias(i).getCdContaDebito());
	        registro.setCdDigitoContaDebito(response.getOcorrencias(i).getCdDigitoContaDebito());
	        registro.setCdSituacaoOperPagamento(response.getOcorrencias(i).getCdSituacaoOperPagamento());
	        registro.setDsSituacaoOperacaoPagamento(response.getOcorrencias(i).getDsSituacaoOperacaoPagamento());
	        registro.setCpfCnpjFormatado(CpfCnpjUtils.formatCpfCnpjCompleto(response.getOcorrencias(i).getCdCorpoCpfCnpj(),
	        		response.getOcorrencias(i).getCdFilialCpfCnpj(), response.getOcorrencias(i).getCdDigitoCpfCnpj()));	
	        
	        registro.setContaDebito(PgitUtil.formatBancoAgenciaConta(registro.getCdBancoDebito(), registro.getCdAgenciaDebito(),
	        						registro.getCdDigitoAgenciaDebito(), registro.getCdContaDebito(), registro.getCdDigitoContaDebito(), true));
	        
	        listaSaida.add(registro);
	        
        }      
        
        return listaSaida;
	}
	
	/**
	 * Consultar detalhes solicitacao.
	 *
	 * @param entrada the entrada
	 * @return the consultar detalhes solicitacao saida dto
	 */
	public ConsultarDetalhesSolicitacaoSaidaDTO consultarDetalhesSolicitacao(ConsultarDetalhesSolicitacaoEntradaDTO entrada){
		ConsultarDetalhesSolicitacaoSaidaDTO saida = new ConsultarDetalhesSolicitacaoSaidaDTO();
		ConsultarDetalhesSolicitacaoRequest request = new ConsultarDetalhesSolicitacaoRequest();
		ConsultarDetalhesSolicitacaoResponse response = new ConsultarDetalhesSolicitacaoResponse();

		request.setCdSolicitacaoPagamento(PgitUtil.verificaIntegerNulo(entrada.getCdSolicitacaoPagamento()));
		request.setNrSolicitacaoPagamento(PgitUtil.verificaIntegerNulo(entrada.getNrSolicitacaoPagamento()));
		request.setDsObservacao(PgitUtil.verificaStringNula(entrada.getDsObservacao()));

		response = getFactoryAdapter().getConsultarDetalhesSolicitacaoPDCAdapter().invokeProcess(request);

		saida.setCodMensagem(response.getCodMensagem());
		saida.setMensagem(response.getMensagem());
		saida.setCdBancoEstorno(response.getCdBancoEstorno());
		saida.setDsBancoEstorno(response.getDsBancoEstorno());
		saida.setCdAgenciaEstorno(response.getCdAgenciaEstorno());
		saida.setCdDigitoAgenciaEstorno(response.getCdDigitoAgenciaEstorno());
		saida.setDsAgenciaEstorno(response.getDsAgenciaEstorno());
		saida.setCdContaEstorno(response.getCdContaEstorno());
		saida.setCdDigitoContaEstorno(response.getCdDigitoContaEstorno());
		saida.setDsTipoConta(response.getDsTipoConta());
		saida.setSituacaoSolicitacao(response.getSituacaoSolicitacao());
		saida.setCdMotivoSolicitacao(response.getCdMotivoSolicitacao());
		saida.setDsObservacao(response.getDsObservacao());
		saida.setDsDataSolicitacao(response.getDsDataSolicitacao());
		saida.setDsHoraSolicitacao(response.getDsHoraSolicitacao());
		saida.setDtAutorizacao(response.getDtAutorizacao());
		saida.setHrAutorizacao(response.getHrAutorizacao());
		saida.setCdUsuarioAutorizacao(response.getCdUsuarioAutorizacao());
		saida.setDtPrevistaEstorno(response.getDtPrevistaEstorno());
		saida.setCdValorPrevistoEstorno(response.getCdValorPrevistoEstorno());
        saida.setCdFormaEstrnPagamento(response.getCdFormaEstrnPagamento());
        saida.setDsFormaEstrnPagamento(response.getDsFormaEstrnPagamento());

		return saida;
	}
	

	   
}

