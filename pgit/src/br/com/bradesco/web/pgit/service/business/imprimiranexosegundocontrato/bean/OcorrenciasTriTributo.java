/*
 * Nome: br.com.bradesco.web.pgit.service.business.imprimiranexosegundocontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.imprimiranexosegundocontrato.bean;

import java.math.BigDecimal;

// TODO: Auto-generated Javadoc
/**
 * Nome: OcorrenciasTriTributo
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class OcorrenciasTriTributo {
	
	/** Atributo dsTriTipoTributo. */
	private String dsTriTipoTributo;
    
    /** Atributo dsTriTipoConsultaSaldo. */
    private String dsTriTipoConsultaSaldo;
    
    /** Atributo dsTriTipoProcessamento. */
    private String dsTriTipoProcessamento;
    
    /** Atributo dsTriTipoTratamentoFeriado. */
    private String dsTriTipoTratamentoFeriado;
    
    /** Atributo dsTriTipoRejeicaoEfetivacao. */
    private String dsTriTipoRejeicaoEfetivacao;
    
    /** Atributo dsTriTipoRejeicaoAgendado. */
    private String dsTriTipoRejeicaoAgendado;
    
    /** Atributo cdTriIndicadorValorMaximo. */
    private String cdTriIndicadorValorMaximo;
    
    /** Atributo vlTriMaximoFavorecidoNao. */
    private BigDecimal vlTriMaximoFavorecidoNao;
    
    /** Atributo dsTriPrioridadeDebito. */
    private String dsTriPrioridadeDebito;
    
    /** Atributo cdTriIndicadorValorDia. */
    private String cdTriIndicadorValorDia;
    
    /** Atributo vlTriLimiteDiario. */
    private BigDecimal vlTriLimiteDiario;
    
    /** Atributo cdTriIndicadorValorIndividual. */
    private String cdTriIndicadorValorIndividual;
    
    /** Atributo vlTriLimiteIndividual. */
    private BigDecimal vlTriLimiteIndividual;
    
    /** Atributo cdTriIndicadorQuantidadeFloating. */
    private String cdTriIndicadorQuantidadeFloating;
    
    /** Atributo qttriDiaFloating. */
    private Integer qttriDiaFloating;
    
    /** Atributo cdTriIndicadorCadastroFavorecido. */
    private String cdTriIndicadorCadastroFavorecido;
    
    /** Atributo cdTriUtilizacaoCadastroFavorecido. */
    private String cdTriUtilizacaoCadastroFavorecido;
    
    /** Atributo cdTriIndicadorQuantidadeRepique. */
    private String cdTriIndicadorQuantidadeRepique;
    
    /** Atributo qtTriDiaRepique. */
    private Integer qtTriDiaRepique;
    
    /** The ds tri tipo cons favorecido. */
    private String dsTriTipoConsFavorecido;
    
    /** The cd tri cferi loc. */
    private String cdTriCferiLoc;
    
	/**
	 * Get: dsTriTipoTributo.
	 *
	 * @return dsTriTipoTributo
	 */
	public String getDsTriTipoTributo() {
		return dsTriTipoTributo;
	}
	
	/**
	 * Set: dsTriTipoTributo.
	 *
	 * @param dsTriTipoTributo the ds tri tipo tributo
	 */
	public void setDsTriTipoTributo(String dsTriTipoTributo) {
		this.dsTriTipoTributo = dsTriTipoTributo;
	}
	
	/**
	 * Get: dsTriTipoConsultaSaldo.
	 *
	 * @return dsTriTipoConsultaSaldo
	 */
	public String getDsTriTipoConsultaSaldo() {
		return dsTriTipoConsultaSaldo;
	}
	
	/**
	 * Set: dsTriTipoConsultaSaldo.
	 *
	 * @param dsTriTipoConsultaSaldo the ds tri tipo consulta saldo
	 */
	public void setDsTriTipoConsultaSaldo(String dsTriTipoConsultaSaldo) {
		this.dsTriTipoConsultaSaldo = dsTriTipoConsultaSaldo;
	}
	
	/**
	 * Get: dsTriTipoProcessamento.
	 *
	 * @return dsTriTipoProcessamento
	 */
	public String getDsTriTipoProcessamento() {
		return dsTriTipoProcessamento;
	}
	
	/**
	 * Set: dsTriTipoProcessamento.
	 *
	 * @param dsTriTipoProcessamento the ds tri tipo processamento
	 */
	public void setDsTriTipoProcessamento(String dsTriTipoProcessamento) {
		this.dsTriTipoProcessamento = dsTriTipoProcessamento;
	}
	
	/**
	 * Get: dsTriTipoTratamentoFeriado.
	 *
	 * @return dsTriTipoTratamentoFeriado
	 */
	public String getDsTriTipoTratamentoFeriado() {
		return dsTriTipoTratamentoFeriado;
	}
	
	/**
	 * Set: dsTriTipoTratamentoFeriado.
	 *
	 * @param dsTriTipoTratamentoFeriado the ds tri tipo tratamento feriado
	 */
	public void setDsTriTipoTratamentoFeriado(String dsTriTipoTratamentoFeriado) {
		this.dsTriTipoTratamentoFeriado = dsTriTipoTratamentoFeriado;
	}
	
	/**
	 * Get: dsTriTipoRejeicaoEfetivacao.
	 *
	 * @return dsTriTipoRejeicaoEfetivacao
	 */
	public String getDsTriTipoRejeicaoEfetivacao() {
		return dsTriTipoRejeicaoEfetivacao;
	}
	
	/**
	 * Set: dsTriTipoRejeicaoEfetivacao.
	 *
	 * @param dsTriTipoRejeicaoEfetivacao the ds tri tipo rejeicao efetivacao
	 */
	public void setDsTriTipoRejeicaoEfetivacao(String dsTriTipoRejeicaoEfetivacao) {
		this.dsTriTipoRejeicaoEfetivacao = dsTriTipoRejeicaoEfetivacao;
	}
	
	/**
	 * Get: dsTriTipoRejeicaoAgendado.
	 *
	 * @return dsTriTipoRejeicaoAgendado
	 */
	public String getDsTriTipoRejeicaoAgendado() {
		return dsTriTipoRejeicaoAgendado;
	}
	
	/**
	 * Set: dsTriTipoRejeicaoAgendado.
	 *
	 * @param dsTriTipoRejeicaoAgendado the ds tri tipo rejeicao agendado
	 */
	public void setDsTriTipoRejeicaoAgendado(String dsTriTipoRejeicaoAgendado) {
		this.dsTriTipoRejeicaoAgendado = dsTriTipoRejeicaoAgendado;
	}
	
	/**
	 * Get: cdTriIndicadorValorMaximo.
	 *
	 * @return cdTriIndicadorValorMaximo
	 */
	public String getCdTriIndicadorValorMaximo() {
		return cdTriIndicadorValorMaximo;
	}
	
	/**
	 * Set: cdTriIndicadorValorMaximo.
	 *
	 * @param cdTriIndicadorValorMaximo the cd tri indicador valor maximo
	 */
	public void setCdTriIndicadorValorMaximo(String cdTriIndicadorValorMaximo) {
		this.cdTriIndicadorValorMaximo = cdTriIndicadorValorMaximo;
	}
	
	/**
	 * Get: vlTriMaximoFavorecidoNao.
	 *
	 * @return vlTriMaximoFavorecidoNao
	 */
	public BigDecimal getVlTriMaximoFavorecidoNao() {
		return vlTriMaximoFavorecidoNao;
	}
	
	/**
	 * Set: vlTriMaximoFavorecidoNao.
	 *
	 * @param vlTriMaximoFavorecidoNao the vl tri maximo favorecido nao
	 */
	public void setVlTriMaximoFavorecidoNao(BigDecimal vlTriMaximoFavorecidoNao) {
		this.vlTriMaximoFavorecidoNao = vlTriMaximoFavorecidoNao;
	}
	
	/**
	 * Get: dsTriPrioridadeDebito.
	 *
	 * @return dsTriPrioridadeDebito
	 */
	public String getDsTriPrioridadeDebito() {
		return dsTriPrioridadeDebito;
	}
	
	/**
	 * Set: dsTriPrioridadeDebito.
	 *
	 * @param dsTriPrioridadeDebito the ds tri prioridade debito
	 */
	public void setDsTriPrioridadeDebito(String dsTriPrioridadeDebito) {
		this.dsTriPrioridadeDebito = dsTriPrioridadeDebito;
	}
	
	/**
	 * Get: cdTriIndicadorValorDia.
	 *
	 * @return cdTriIndicadorValorDia
	 */
	public String getCdTriIndicadorValorDia() {
		return cdTriIndicadorValorDia;
	}
	
	/**
	 * Set: cdTriIndicadorValorDia.
	 *
	 * @param cdTriIndicadorValorDia the cd tri indicador valor dia
	 */
	public void setCdTriIndicadorValorDia(String cdTriIndicadorValorDia) {
		this.cdTriIndicadorValorDia = cdTriIndicadorValorDia;
	}
	
	/**
	 * Get: vlTriLimiteDiario.
	 *
	 * @return vlTriLimiteDiario
	 */
	public BigDecimal getVlTriLimiteDiario() {
		return vlTriLimiteDiario;
	}
	
	/**
	 * Set: vlTriLimiteDiario.
	 *
	 * @param vlTriLimiteDiario the vl tri limite diario
	 */
	public void setVlTriLimiteDiario(BigDecimal vlTriLimiteDiario) {
		this.vlTriLimiteDiario = vlTriLimiteDiario;
	}
	
	/**
	 * Get: cdTriIndicadorValorIndividual.
	 *
	 * @return cdTriIndicadorValorIndividual
	 */
	public String getCdTriIndicadorValorIndividual() {
		return cdTriIndicadorValorIndividual;
	}
	
	/**
	 * Set: cdTriIndicadorValorIndividual.
	 *
	 * @param cdTriIndicadorValorIndividual the cd tri indicador valor individual
	 */
	public void setCdTriIndicadorValorIndividual(
			String cdTriIndicadorValorIndividual) {
		this.cdTriIndicadorValorIndividual = cdTriIndicadorValorIndividual;
	}
	
	/**
	 * Get: vlTriLimiteIndividual.
	 *
	 * @return vlTriLimiteIndividual
	 */
	public BigDecimal getVlTriLimiteIndividual() {
		return vlTriLimiteIndividual;
	}
	
	/**
	 * Set: vlTriLimiteIndividual.
	 *
	 * @param vlTriLimiteIndividual the vl tri limite individual
	 */
	public void setVlTriLimiteIndividual(BigDecimal vlTriLimiteIndividual) {
		this.vlTriLimiteIndividual = vlTriLimiteIndividual;
	}
	
	/**
	 * Get: cdTriIndicadorQuantidadeFloating.
	 *
	 * @return cdTriIndicadorQuantidadeFloating
	 */
	public String getCdTriIndicadorQuantidadeFloating() {
		return cdTriIndicadorQuantidadeFloating;
	}
	
	/**
	 * Set: cdTriIndicadorQuantidadeFloating.
	 *
	 * @param cdTriIndicadorQuantidadeFloating the cd tri indicador quantidade floating
	 */
	public void setCdTriIndicadorQuantidadeFloating(
			String cdTriIndicadorQuantidadeFloating) {
		this.cdTriIndicadorQuantidadeFloating = cdTriIndicadorQuantidadeFloating;
	}
	
	/**
	 * Get: qttriDiaFloating.
	 *
	 * @return qttriDiaFloating
	 */
	public Integer getQttriDiaFloating() {
		return qttriDiaFloating;
	}
	
	/**
	 * Set: qttriDiaFloating.
	 *
	 * @param qttriDiaFloating the qttri dia floating
	 */
	public void setQttriDiaFloating(Integer qttriDiaFloating) {
		this.qttriDiaFloating = qttriDiaFloating;
	}
	
	/**
	 * Get: cdTriIndicadorCadastroFavorecido.
	 *
	 * @return cdTriIndicadorCadastroFavorecido
	 */
	public String getCdTriIndicadorCadastroFavorecido() {
		return cdTriIndicadorCadastroFavorecido;
	}
	
	/**
	 * Set: cdTriIndicadorCadastroFavorecido.
	 *
	 * @param cdTriIndicadorCadastroFavorecido the cd tri indicador cadastro favorecido
	 */
	public void setCdTriIndicadorCadastroFavorecido(
			String cdTriIndicadorCadastroFavorecido) {
		this.cdTriIndicadorCadastroFavorecido = cdTriIndicadorCadastroFavorecido;
	}
	
	/**
	 * Get: cdTriUtilizacaoCadastroFavorecido.
	 *
	 * @return cdTriUtilizacaoCadastroFavorecido
	 */
	public String getCdTriUtilizacaoCadastroFavorecido() {
		return cdTriUtilizacaoCadastroFavorecido;
	}
	
	/**
	 * Set: cdTriUtilizacaoCadastroFavorecido.
	 *
	 * @param cdTriUtilizacaoCadastroFavorecido the cd tri utilizacao cadastro favorecido
	 */
	public void setCdTriUtilizacaoCadastroFavorecido(
			String cdTriUtilizacaoCadastroFavorecido) {
		this.cdTriUtilizacaoCadastroFavorecido = cdTriUtilizacaoCadastroFavorecido;
	}
	
	/**
	 * Get: cdTriIndicadorQuantidadeRepique.
	 *
	 * @return cdTriIndicadorQuantidadeRepique
	 */
	public String getCdTriIndicadorQuantidadeRepique() {
		return cdTriIndicadorQuantidadeRepique;
	}
	
	/**
	 * Set: cdTriIndicadorQuantidadeRepique.
	 *
	 * @param cdTriIndicadorQuantidadeRepique the cd tri indicador quantidade repique
	 */
	public void setCdTriIndicadorQuantidadeRepique(
			String cdTriIndicadorQuantidadeRepique) {
		this.cdTriIndicadorQuantidadeRepique = cdTriIndicadorQuantidadeRepique;
	}
	
	/**
	 * Get: qtTriDiaRepique.
	 *
	 * @return qtTriDiaRepique
	 */
	public Integer getQtTriDiaRepique() {
		return qtTriDiaRepique;
	}
	
	/**
	 * Set: qtTriDiaRepique.
	 *
	 * @param qtTriDiaRepique the qt tri dia repique
	 */
	public void setQtTriDiaRepique(Integer qtTriDiaRepique) {
		this.qtTriDiaRepique = qtTriDiaRepique;
	}

	/**
	 * Gets the ds tri tipo cons favorecido.
	 *
	 * @return the dsTriTipoConsFavorecido
	 */
	public String getDsTriTipoConsFavorecido() {
		return dsTriTipoConsFavorecido;
	}

	/**
	 * Sets the ds tri tipo cons favorecido.
	 *
	 * @param dsTriTipoConsFavorecido the dsTriTipoConsFavorecido to set
	 */
	public void setDsTriTipoConsFavorecido(String dsTriTipoConsFavorecido) {
		this.dsTriTipoConsFavorecido = dsTriTipoConsFavorecido;
	}

	/**
	 * Gets the cd tri cferi loc.
	 *
	 * @return the cdTriCferiLoc
	 */
	public String getCdTriCferiLoc() {
		return cdTriCferiLoc;
	}

	/**
	 * Sets the cd tri cferi loc.
	 *
	 * @param cdTriCferiLoc the cdTriCferiLoc to set
	 */
	public void setCdTriCferiLoc(String cdTriCferiLoc) {
		this.cdTriCferiLoc = cdTriCferiLoc;
	}
    
}
