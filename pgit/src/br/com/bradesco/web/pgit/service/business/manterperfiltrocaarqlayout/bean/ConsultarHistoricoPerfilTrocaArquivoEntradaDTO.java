/*
 * Nome: br.com.bradesco.web.pgit.service.business.manterperfiltrocaarqlayout.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.manterperfiltrocaarqlayout.bean;

/**
 * Nome: ConsultarHistoricoPerfilTrocaArquivoEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarHistoricoPerfilTrocaArquivoEntradaDTO{
	
	/** Atributo cdTipoLayoutArquivo. */
	private Integer cdTipoLayoutArquivo;
	
	/** Atributo cdPerfilTrocaArquivo. */
	private Long cdPerfilTrocaArquivo;
	
	/** Atributo dtManutencaoInicio. */
	private String dtManutencaoInicio;
	
	/** Atributo dtManutencaoFim. */
	private String dtManutencaoFim;

	/**
	 * Get: cdTipoLayoutArquivo.
	 *
	 * @return cdTipoLayoutArquivo
	 */
	public Integer getCdTipoLayoutArquivo(){
		return cdTipoLayoutArquivo;
	}

	/**
	 * Set: cdTipoLayoutArquivo.
	 *
	 * @param cdTipoLayoutArquivo the cd tipo layout arquivo
	 */
	public void setCdTipoLayoutArquivo(Integer cdTipoLayoutArquivo){
		this.cdTipoLayoutArquivo = cdTipoLayoutArquivo;
	}

	/**
	 * Get: cdPerfilTrocaArquivo.
	 *
	 * @return cdPerfilTrocaArquivo
	 */
	public Long getCdPerfilTrocaArquivo(){
		return cdPerfilTrocaArquivo;
	}

	/**
	 * Set: cdPerfilTrocaArquivo.
	 *
	 * @param cdPerfilTrocaArquivo the cd perfil troca arquivo
	 */
	public void setCdPerfilTrocaArquivo(Long cdPerfilTrocaArquivo){
		this.cdPerfilTrocaArquivo = cdPerfilTrocaArquivo;
	}

	/**
	 * Get: dtManutencaoInicio.
	 *
	 * @return dtManutencaoInicio
	 */
	public String getDtManutencaoInicio(){
		return dtManutencaoInicio;
	}

	/**
	 * Set: dtManutencaoInicio.
	 *
	 * @param dtManutencaoInicio the dt manutencao inicio
	 */
	public void setDtManutencaoInicio(String dtManutencaoInicio){
		this.dtManutencaoInicio = dtManutencaoInicio;
	}

	/**
	 * Get: dtManutencaoFim.
	 *
	 * @return dtManutencaoFim
	 */
	public String getDtManutencaoFim(){
		return dtManutencaoFim;
	}

	/**
	 * Set: dtManutencaoFim.
	 *
	 * @param dtManutencaoFim the dt manutencao fim
	 */
	public void setDtManutencaoFim(String dtManutencaoFim){
		this.dtManutencaoFim = dtManutencaoFim;
	}
}