/**
 * 
 */
package br.com.bradesco.web.pgit.service.business.mantersolicitacaorastfav.bean;

import java.math.BigDecimal;

/**
 * @author tcordeiro
 * 
 */
public class IncluirSolicRastFavEntradaDTO {
    
	/** Atributo cdPessoaJuridicaContrato. */
	private Long cdPessoaJuridicaContrato;

	/** Atributo cdTipoContratoNegocio. */
	private Integer cdTipoContratoNegocio;

	/** Atributo nrSequenciaContratoNegocio. */
	private Long nrSequenciaContratoNegocio;

	/** Atributo dtInicioRastreamento. */
	private String dtInicioRastreamento;

	/** Atributo dtFimRastreamento. */
	private String dtFimRastreamento;

	/** Atributo cdServico. */
	private Integer cdServico;

	/** Atributo cdModalidade. */
	private Integer cdModalidade;

	/** Atributo cdBanco. */
	private Integer cdBanco;

	/** Atributo cdAgencia. */
	private Integer cdAgencia;

	/** Atributo cdConta. */
	private Long cdConta;

	/** Atributo dgConta. */
	private String dgConta;

	/** Atributo cdIndicadorInclusaoFavorecido. */
	private Integer cdIndicadorInclusaoFavorecido;

	/** Atributo cdIndicadorDestinoRetorno. */
	private Integer cdIndicadorDestinoRetorno;
	
	/** Atributo vlTarifaPadrao. */
	private BigDecimal vlTarifaPadrao;
	
	/** Atributo numPercentualDescTarifa. */
	private BigDecimal numPercentualDescTarifa;
	
	/** Atributo vlrTarifaAtual. */
	private BigDecimal vlrTarifaAtual;

	/** Atributo dsBancoAtualizado. */
	private String dsBancoAtualizado;

	/** Atributo cdDigitoAgenciaAtualizada. */
	private Integer cdDigitoAgenciaAtualizada;

	/** Atributo dsAgenciaAtualizada. */
	private String dsAgenciaAtualizada;

	/** Atributo cdFormaFavorecido. */
	private Integer cdFormaFavorecido;

	/**
	 * Incluir solic rast fav entrada dto.
	 */
	public IncluirSolicRastFavEntradaDTO() {
		super();

	}

	/**
	 * Incluir solic rast fav entrada dto.
	 *
	 * @param cdPessoaJuridicaContrato the cd pessoa juridica contrato
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
	 * @param dtInicioRastreamento the dt inicio rastreamento
	 * @param dtFimRastreamento the dt fim rastreamento
	 * @param cdServico the cd servico
	 * @param cdModalidade the cd modalidade
	 * @param cdBanco the cd banco
	 * @param cdAgencia the cd agencia
	 * @param cdConta the cd conta
	 * @param dgConta the dg conta
	 * @param cdIndicadorInclusaoFavorecido the cd indicador inclusao favorecido
	 * @param cdIndicadorDestinoRetorno the cd indicador destino retorno
	 * @param vlTarifaBonificacao the vl tarifa bonificacao
	 * @param dsBancoAtualizado the ds banco atualizado
	 * @param cdDigitoAgenciaAtualizada the cd digito agencia atualizada
	 * @param dsAgenciaAtualizada the ds agencia atualizada
	 */
	public IncluirSolicRastFavEntradaDTO(Long cdPessoaJuridicaContrato, Integer cdTipoContratoNegocio,
					Long nrSequenciaContratoNegocio, String dtInicioRastreamento, String dtFimRastreamento,
					Integer cdServico, Integer cdModalidade, Integer cdBanco, Integer cdAgencia, Long cdConta,
					String dgConta, Integer cdIndicadorInclusaoFavorecido, Integer cdIndicadorDestinoRetorno,
					BigDecimal vlTarifaBonificacao, String dsBancoAtualizado, Integer cdDigitoAgenciaAtualizada,
					String dsAgenciaAtualizada) {
		super();
		this.cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
		this.dtInicioRastreamento = dtInicioRastreamento;
		this.dtFimRastreamento = dtFimRastreamento;
		this.cdServico = cdServico;
		this.cdModalidade = cdModalidade;
		this.cdBanco = cdBanco;
		this.cdAgencia = cdAgencia;
		this.cdConta = cdConta;
		this.dgConta = dgConta;
		this.cdIndicadorInclusaoFavorecido = cdIndicadorInclusaoFavorecido;
		this.cdIndicadorDestinoRetorno = cdIndicadorDestinoRetorno;
		this.dsBancoAtualizado = dsBancoAtualizado;
		this.cdDigitoAgenciaAtualizada = cdDigitoAgenciaAtualizada;
		this.dsAgenciaAtualizada = dsAgenciaAtualizada;
	}

	/**
	 * Get: banco.
	 *
	 * @return banco
	 */
	public String getBanco() {
		if (dsBancoAtualizado == null) {
			return String.valueOf(cdBanco == null ? "" : cdBanco);
		} else {
			return String.valueOf(cdBanco == null ? "" : cdBanco + " " + dsBancoAtualizado);
		}
	}

	/**
	 * Get: agencia.
	 *
	 * @return agencia
	 */
	public String getAgencia() {
		if (dsAgenciaAtualizada == null) {
			return String.valueOf(cdAgencia == null ? "" : cdAgencia);
		} else {
			return String.valueOf(cdAgencia == null ? "" : cdAgencia + " " + dsAgenciaAtualizada);
		}
	}

	/**
	 * Get: contaDigitoAtualizado.
	 *
	 * @return contaDigitoAtualizado
	 */
	public String getContaDigitoAtualizado() {
		if (this.dgConta == null || this.dgConta.equals("")) {
			return String.valueOf(this.cdConta == null ? "" : this.cdConta);
		} else {
			return String.valueOf(this.cdConta == null ? "" : this.cdConta + "-" + this.dgConta);
		}
	}

	/**
	 * Get: cdAgencia.
	 *
	 * @return cdAgencia
	 */
	public Integer getCdAgencia() {
		return cdAgencia;
	}

	/**
	 * Set: cdAgencia.
	 *
	 * @param cdAgencia the cd agencia
	 */
	public void setCdAgencia(Integer cdAgencia) {
		this.cdAgencia = cdAgencia;
	}

	/**
	 * Get: cdBanco.
	 *
	 * @return cdBanco
	 */
	public Integer getCdBanco() {
		return cdBanco;
	}

	/**
	 * Set: cdBanco.
	 *
	 * @param cdBanco the cd banco
	 */
	public void setCdBanco(Integer cdBanco) {
		this.cdBanco = cdBanco;
	}

	/**
	 * Get: cdConta.
	 *
	 * @return cdConta
	 */
	public Long getCdConta() {
		return cdConta;
	}

	/**
	 * Set: cdConta.
	 *
	 * @param cdConta the cd conta
	 */
	public void setCdConta(Long cdConta) {
		this.cdConta = cdConta;
	}

	/**
	 * Get: cdIndicadorDestinoRetorno.
	 *
	 * @return cdIndicadorDestinoRetorno
	 */
	public Integer getCdIndicadorDestinoRetorno() {
		return cdIndicadorDestinoRetorno;
	}

	/**
	 * Set: cdIndicadorDestinoRetorno.
	 *
	 * @param cdIndicadorDestinoRetorno the cd indicador destino retorno
	 */
	public void setCdIndicadorDestinoRetorno(Integer cdIndicadorDestinoRetorno) {
		this.cdIndicadorDestinoRetorno = cdIndicadorDestinoRetorno;
	}

	/**
	 * Get: cdIndicadorInclusaoFavorecido.
	 *
	 * @return cdIndicadorInclusaoFavorecido
	 */
	public Integer getCdIndicadorInclusaoFavorecido() {
		return cdIndicadorInclusaoFavorecido;
	}

	/**
	 * Set: cdIndicadorInclusaoFavorecido.
	 *
	 * @param cdIndicadorInclusaoFavorecido the cd indicador inclusao favorecido
	 */
	public void setCdIndicadorInclusaoFavorecido(Integer cdIndicadorInclusaoFavorecido) {
		this.cdIndicadorInclusaoFavorecido = cdIndicadorInclusaoFavorecido;
	}

	/**
	 * Get: cdModalidade.
	 *
	 * @return cdModalidade
	 */
	public Integer getCdModalidade() {
		return cdModalidade;
	}

	/**
	 * Set: cdModalidade.
	 *
	 * @param cdModalidade the cd modalidade
	 */
	public void setCdModalidade(Integer cdModalidade) {
		this.cdModalidade = cdModalidade;
	}

	/**
	 * Get: cdPessoaJuridicaContrato.
	 *
	 * @return cdPessoaJuridicaContrato
	 */
	public Long getCdPessoaJuridicaContrato() {
		return cdPessoaJuridicaContrato;
	}

	/**
	 * Set: cdPessoaJuridicaContrato.
	 *
	 * @param cdPessoaJuridicaContrato the cd pessoa juridica contrato
	 */
	public void setCdPessoaJuridicaContrato(Long cdPessoaJuridicaContrato) {
		this.cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
	}

	/**
	 * Get: cdServico.
	 *
	 * @return cdServico
	 */
	public Integer getCdServico() {
		return cdServico;
	}

	/**
	 * Set: cdServico.
	 *
	 * @param cdServico the cd servico
	 */
	public void setCdServico(Integer cdServico) {
		this.cdServico = cdServico;
	}

	/**
	 * Get: cdTipoContratoNegocio.
	 *
	 * @return cdTipoContratoNegocio
	 */
	public Integer getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}

	/**
	 * Set: cdTipoContratoNegocio.
	 *
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}

	/**
	 * Get: dgConta.
	 *
	 * @return dgConta
	 */
	public String getDgConta() {
		return dgConta;
	}

	/**
	 * Set: dgConta.
	 *
	 * @param dgConta the dg conta
	 */
	public void setDgConta(String dgConta) {
		this.dgConta = dgConta;
	}

	/**
	 * Get: dtFimRastreamento.
	 *
	 * @return dtFimRastreamento
	 */
	public String getDtFimRastreamento() {
		return dtFimRastreamento;
	}

	/**
	 * Set: dtFimRastreamento.
	 *
	 * @param dtFimRastreamento the dt fim rastreamento
	 */
	public void setDtFimRastreamento(String dtFimRastreamento) {
		this.dtFimRastreamento = dtFimRastreamento;
	}

	/**
	 * Get: dtInicioRastreamento.
	 *
	 * @return dtInicioRastreamento
	 */
	public String getDtInicioRastreamento() {
		return dtInicioRastreamento;
	}

	/**
	 * Set: dtInicioRastreamento.
	 *
	 * @param dtInicioRastreamento the dt inicio rastreamento
	 */
	public void setDtInicioRastreamento(String dtInicioRastreamento) {
		this.dtInicioRastreamento = dtInicioRastreamento;
	}

	/**
	 * Get: nrSequenciaContratoNegocio.
	 *
	 * @return nrSequenciaContratoNegocio
	 */
	public Long getNrSequenciaContratoNegocio() {
		return nrSequenciaContratoNegocio;
	}

	/**
	 * Set: nrSequenciaContratoNegocio.
	 *
	 * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
	 */
	public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}

	/**
	 * Get: cdDigitoAgenciaAtualizada.
	 *
	 * @return cdDigitoAgenciaAtualizada
	 */
	public Integer getCdDigitoAgenciaAtualizada() {
		return cdDigitoAgenciaAtualizada;
	}

	/**
	 * Set: cdDigitoAgenciaAtualizada.
	 *
	 * @param cdDigitoAgenciaAtualizada the cd digito agencia atualizada
	 */
	public void setCdDigitoAgenciaAtualizada(Integer cdDigitoAgenciaAtualizada) {
		this.cdDigitoAgenciaAtualizada = cdDigitoAgenciaAtualizada;
	}

	/**
	 * Get: dsAgenciaAtualizada.
	 *
	 * @return dsAgenciaAtualizada
	 */
	public String getDsAgenciaAtualizada() {
		return dsAgenciaAtualizada;
	}

	/**
	 * Set: dsAgenciaAtualizada.
	 *
	 * @param dsAgenciaAtualizada the ds agencia atualizada
	 */
	public void setDsAgenciaAtualizada(String dsAgenciaAtualizada) {
		this.dsAgenciaAtualizada = dsAgenciaAtualizada;
	}

	/**
	 * Get: dsBancoAtualizado.
	 *
	 * @return dsBancoAtualizado
	 */
	public String getDsBancoAtualizado() {
		return dsBancoAtualizado;
	}

	/**
	 * Set: dsBancoAtualizado.
	 *
	 * @param dsBancoAtualizado the ds banco atualizado
	 */
	public void setDsBancoAtualizado(String dsBancoAtualizado) {
		this.dsBancoAtualizado = dsBancoAtualizado;
	}

	/**
	 * Get: cdFormaFavorecido.
	 *
	 * @return cdFormaFavorecido
	 */
	public Integer getCdFormaFavorecido() {
		return cdFormaFavorecido;
	}

	/**
	 * Set: cdFormaFavorecido.
	 *
	 * @param cdFormaFavorecido the cd forma favorecido
	 */
	public void setCdFormaFavorecido(Integer cdFormaFavorecido) {
		this.cdFormaFavorecido = cdFormaFavorecido;
	}

	/**
	 * Get: vlTarifaPadrao.
	 *
	 * @return vlTarifaPadrao
	 */
	public BigDecimal getVlTarifaPadrao() {
		return vlTarifaPadrao;
	}

	/**
	 * Set: vlTarifaPadrao.
	 *
	 * @param vlTarifaPadrao the vl tarifa padrao
	 */
	public void setVlTarifaPadrao(BigDecimal vlTarifaPadrao) {
		this.vlTarifaPadrao = vlTarifaPadrao;
	}

	/**
	 * Get: numPercentualDescTarifa.
	 *
	 * @return numPercentualDescTarifa
	 */
	public BigDecimal getNumPercentualDescTarifa() {
		return numPercentualDescTarifa;
	}

	/**
	 * Set: numPercentualDescTarifa.
	 *
	 * @param numPercentualDescTarifa the num percentual desc tarifa
	 */
	public void setNumPercentualDescTarifa(BigDecimal numPercentualDescTarifa) {
		this.numPercentualDescTarifa = numPercentualDescTarifa;
	}

	/**
	 * Get: vlrTarifaAtual.
	 *
	 * @return vlrTarifaAtual
	 */
	public BigDecimal getVlrTarifaAtual() {
		return vlrTarifaAtual;
	}

	/**
	 * Set: vlrTarifaAtual.
	 *
	 * @param vlrTarifaAtual the vlr tarifa atual
	 */
	public void setVlrTarifaAtual(BigDecimal vlrTarifaAtual) {
		this.vlrTarifaAtual = vlrTarifaAtual;
	}

}
