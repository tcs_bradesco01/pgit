/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantercontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantercontrato.bean;

/**
 * Nome: ListarContasRelacionadasHistEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarContasRelacionadasHistEntradaDTO {
	
	/** Atributo cdPessoaJuridicaNegocio. */
	private Long cdPessoaJuridicaNegocio;
	
	/** Atributo cdTipoContratoNegocio. */
	private Integer cdTipoContratoNegocio;
	
	/** Atributo nrSequenciaContratoNegocio. */
	private Long nrSequenciaContratoNegocio;
	
	/** Atributo cdPessoaJuridicaVinculo. */
	private Long cdPessoaJuridicaVinculo;
	
	/** Atributo cdTipoContratoVinculo. */
	private Integer cdTipoContratoVinculo;
	
	/** Atributo nrSequenciaContratoVinculo. */
	private Long nrSequenciaContratoVinculo;
	
	/** Atributo cdTipoVinculoContrato. */
	private Integer cdTipoVinculoContrato;
	
	/** Atributo cdTipoRelacionamentoConta. */
	private Integer cdTipoRelacionamentoConta;
	
	/** Atributo cdBancoRelacionamento. */
	private Integer cdBancoRelacionamento;
	
	/** Atributo cdAgenciaRelacionamento. */
	private Integer cdAgenciaRelacionamento;
	
	/** Atributo cdContaRelacionamento. */
	private Long cdContaRelacionamento;
	
	/** Atributo cdDigitoContaRelacionamento. */
	private String cdDigitoContaRelacionamento;
	
	/** Atributo cdCorpoCpfCnpjParticipante. */
	private Long cdCorpoCpfCnpjParticipante;
	
	/** Atributo cdFilialCpfCnpjParticipante. */
	private Integer cdFilialCpfCnpjParticipante;
	
	/** Atributo cdDigitoCpfCnpjParticipante. */
	private Integer cdDigitoCpfCnpjParticipante;
	
	/** Atributo dtInicioManutencao. */
	private Integer dtInicioManutencao;
	
	/** Atributo dtFimManutencao. */
	private Integer dtFimManutencao;
	
	/**
	 * Get: cdAgenciaRelacionamento.
	 *
	 * @return cdAgenciaRelacionamento
	 */
	public Integer getCdAgenciaRelacionamento() {
		return cdAgenciaRelacionamento;
	}
	
	/**
	 * Set: cdAgenciaRelacionamento.
	 *
	 * @param cdAgenciaRelacionamento the cd agencia relacionamento
	 */
	public void setCdAgenciaRelacionamento(Integer cdAgenciaRelacionamento) {
		this.cdAgenciaRelacionamento = cdAgenciaRelacionamento;
	}
	
	/**
	 * Get: cdBancoRelacionamento.
	 *
	 * @return cdBancoRelacionamento
	 */
	public Integer getCdBancoRelacionamento() {
		return cdBancoRelacionamento;
	}
	
	/**
	 * Set: cdBancoRelacionamento.
	 *
	 * @param cdBancoRelacionamento the cd banco relacionamento
	 */
	public void setCdBancoRelacionamento(Integer cdBancoRelacionamento) {
		this.cdBancoRelacionamento = cdBancoRelacionamento;
	}
	
	/**
	 * Get: cdContaRelacionamento.
	 *
	 * @return cdContaRelacionamento
	 */
	public Long getCdContaRelacionamento() {
		return cdContaRelacionamento;
	}
	
	/**
	 * Set: cdContaRelacionamento.
	 *
	 * @param cdContaRelacionamento the cd conta relacionamento
	 */
	public void setCdContaRelacionamento(Long cdContaRelacionamento) {
		this.cdContaRelacionamento = cdContaRelacionamento;
	}
	
	/**
	 * Get: cdCorpoCpfCnpjParticipante.
	 *
	 * @return cdCorpoCpfCnpjParticipante
	 */
	public Long getCdCorpoCpfCnpjParticipante() {
		return cdCorpoCpfCnpjParticipante;
	}
	
	/**
	 * Set: cdCorpoCpfCnpjParticipante.
	 *
	 * @param cdCorpoCpfCnpjParticipante the cd corpo cpf cnpj participante
	 */
	public void setCdCorpoCpfCnpjParticipante(Long cdCorpoCpfCnpjParticipante) {
		this.cdCorpoCpfCnpjParticipante = cdCorpoCpfCnpjParticipante;
	}
	
	/**
	 * Get: cdDigitoContaRelacionamento.
	 *
	 * @return cdDigitoContaRelacionamento
	 */
	public String getCdDigitoContaRelacionamento() {
		return cdDigitoContaRelacionamento;
	}
	
	/**
	 * Set: cdDigitoContaRelacionamento.
	 *
	 * @param cdDigitoContaRelacionamento the cd digito conta relacionamento
	 */
	public void setCdDigitoContaRelacionamento(String cdDigitoContaRelacionamento) {
		this.cdDigitoContaRelacionamento = cdDigitoContaRelacionamento;
	}
	
	/**
	 * Get: cdDigitoCpfCnpjParticipante.
	 *
	 * @return cdDigitoCpfCnpjParticipante
	 */
	public Integer getCdDigitoCpfCnpjParticipante() {
		return cdDigitoCpfCnpjParticipante;
	}
	
	/**
	 * Set: cdDigitoCpfCnpjParticipante.
	 *
	 * @param cdDigitoCpfCnpjParticipante the cd digito cpf cnpj participante
	 */
	public void setCdDigitoCpfCnpjParticipante(Integer cdDigitoCpfCnpjParticipante) {
		this.cdDigitoCpfCnpjParticipante = cdDigitoCpfCnpjParticipante;
	}
	
	/**
	 * Get: cdFilialCpfCnpjParticipante.
	 *
	 * @return cdFilialCpfCnpjParticipante
	 */
	public Integer getCdFilialCpfCnpjParticipante() {
		return cdFilialCpfCnpjParticipante;
	}
	
	/**
	 * Set: cdFilialCpfCnpjParticipante.
	 *
	 * @param cdFilialCpfCnpjParticipante the cd filial cpf cnpj participante
	 */
	public void setCdFilialCpfCnpjParticipante(Integer cdFilialCpfCnpjParticipante) {
		this.cdFilialCpfCnpjParticipante = cdFilialCpfCnpjParticipante;
	}
	
	/**
	 * Get: cdPessoaJuridicaNegocio.
	 *
	 * @return cdPessoaJuridicaNegocio
	 */
	public Long getCdPessoaJuridicaNegocio() {
		return cdPessoaJuridicaNegocio;
	}
	
	/**
	 * Set: cdPessoaJuridicaNegocio.
	 *
	 * @param cdPessoaJuridicaNegocio the cd pessoa juridica negocio
	 */
	public void setCdPessoaJuridicaNegocio(Long cdPessoaJuridicaNegocio) {
		this.cdPessoaJuridicaNegocio = cdPessoaJuridicaNegocio;
	}
	
	/**
	 * Get: cdPessoaJuridicaVinculo.
	 *
	 * @return cdPessoaJuridicaVinculo
	 */
	public Long getCdPessoaJuridicaVinculo() {
		return cdPessoaJuridicaVinculo;
	}
	
	/**
	 * Set: cdPessoaJuridicaVinculo.
	 *
	 * @param cdPessoaJuridicaVinculo the cd pessoa juridica vinculo
	 */
	public void setCdPessoaJuridicaVinculo(Long cdPessoaJuridicaVinculo) {
		this.cdPessoaJuridicaVinculo = cdPessoaJuridicaVinculo;
	}
	
	/**
	 * Get: cdTipoContratoNegocio.
	 *
	 * @return cdTipoContratoNegocio
	 */
	public Integer getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}
	
	/**
	 * Set: cdTipoContratoNegocio.
	 *
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}
	
	/**
	 * Get: cdTipoContratoVinculo.
	 *
	 * @return cdTipoContratoVinculo
	 */
	public Integer getCdTipoContratoVinculo() {
		return cdTipoContratoVinculo;
	}
	
	/**
	 * Set: cdTipoContratoVinculo.
	 *
	 * @param cdTipoContratoVinculo the cd tipo contrato vinculo
	 */
	public void setCdTipoContratoVinculo(Integer cdTipoContratoVinculo) {
		this.cdTipoContratoVinculo = cdTipoContratoVinculo;
	}
	
	/**
	 * Get: cdTipoRelacionamentoConta.
	 *
	 * @return cdTipoRelacionamentoConta
	 */
	public Integer getCdTipoRelacionamentoConta() {
		return cdTipoRelacionamentoConta;
	}
	
	/**
	 * Set: cdTipoRelacionamentoConta.
	 *
	 * @param cdTipoRelacionamentoConta the cd tipo relacionamento conta
	 */
	public void setCdTipoRelacionamentoConta(Integer cdTipoRelacionamentoConta) {
		this.cdTipoRelacionamentoConta = cdTipoRelacionamentoConta;
	}
	
	/**
	 * Get: cdTipoVinculoContrato.
	 *
	 * @return cdTipoVinculoContrato
	 */
	public Integer getCdTipoVinculoContrato() {
		return cdTipoVinculoContrato;
	}
	
	/**
	 * Set: cdTipoVinculoContrato.
	 *
	 * @param cdTipoVinculoContrato the cd tipo vinculo contrato
	 */
	public void setCdTipoVinculoContrato(Integer cdTipoVinculoContrato) {
		this.cdTipoVinculoContrato = cdTipoVinculoContrato;
	}
	
	/**
	 * Get: dtFimManutencao.
	 *
	 * @return dtFimManutencao
	 */
	public Integer getDtFimManutencao() {
		return dtFimManutencao;
	}
	
	/**
	 * Set: dtFimManutencao.
	 *
	 * @param dtFimManutencao the dt fim manutencao
	 */
	public void setDtFimManutencao(Integer dtFimManutencao) {
		this.dtFimManutencao = dtFimManutencao;
	}
	
	/**
	 * Get: dtInicioManutencao.
	 *
	 * @return dtInicioManutencao
	 */
	public Integer getDtInicioManutencao() {
		return dtInicioManutencao;
	}
	
	/**
	 * Set: dtInicioManutencao.
	 *
	 * @param dtInicioManutencao the dt inicio manutencao
	 */
	public void setDtInicioManutencao(Integer dtInicioManutencao) {
		this.dtInicioManutencao = dtInicioManutencao;
	}
	
	/**
	 * Get: nrSequenciaContratoNegocio.
	 *
	 * @return nrSequenciaContratoNegocio
	 */
	public Long getNrSequenciaContratoNegocio() {
		return nrSequenciaContratoNegocio;
	}
	
	/**
	 * Set: nrSequenciaContratoNegocio.
	 *
	 * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
	 */
	public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}
	
	/**
	 * Get: nrSequenciaContratoVinculo.
	 *
	 * @return nrSequenciaContratoVinculo
	 */
	public Long getNrSequenciaContratoVinculo() {
		return nrSequenciaContratoVinculo;
	}
	
	/**
	 * Set: nrSequenciaContratoVinculo.
	 *
	 * @param nrSequenciaContratoVinculo the nr sequencia contrato vinculo
	 */
	public void setNrSequenciaContratoVinculo(Long nrSequenciaContratoVinculo) {
		this.nrSequenciaContratoVinculo = nrSequenciaContratoVinculo;
	}
	
	
}
