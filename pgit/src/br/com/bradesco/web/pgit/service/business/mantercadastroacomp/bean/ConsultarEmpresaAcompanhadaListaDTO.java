/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantercadastroacomp.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantercadastroacomp.bean;

import br.com.bradesco.web.pgit.utils.CpfCnpjUtils;

/**
 * Nome: ConsultarEmpresaAcompanhadaListaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarEmpresaAcompanhadaListaDTO {
	
	/** Atributo cdCnpjCpf. */
	private Long cdCnpjCpf;
    
    /** Atributo cdFilialCpfCnpj. */
    private Integer cdFilialCpfCnpj;
    
    /** Atributo cdCtrlCpfCnpjSaida. */
    private Integer cdCtrlCpfCnpjSaida;
    
    /** Atributo dsRazaoSocial. */
    private String dsRazaoSocial;
    
    /** Atributo cdAgenciaSaida. */
    private Integer cdAgenciaSaida;
    
    /** Atributo cdContaCorrenteSaida. */
    private Integer cdContaCorrenteSaida;
    
    /** Atributo cdDigitoConta. */
    private String cdDigitoConta;
    
    /** Atributo cdConveCtaSalarial. */
    private Long cdConveCtaSalarial;
    
    /** Atributo cdIndicadorPrevidencia. */
    private String cdIndicadorPrevidencia;
    
    /** Atributo dtInclusao. */
    private String dtInclusao;
    
    /** Atributo hrInclusao. */
    private String hrInclusao;
    
    /** Atributo cdUsuarioInclusao. */
    private String cdUsuarioInclusao;
    
    /** Atributo cdUsuarioInclusaoExterno. */
    private String cdUsuarioInclusaoExterno;
    
    /** Atributo cdOperacaoCanalInclusao. */
    private String cdOperacaoCanalInclusao;
    
    /** Atributo dsTipoCanalInclusao. */
    private String dsTipoCanalInclusao;
    
    /** Atributo dtManutencao. */
    private String dtManutencao;
    
    /** Atributo hrManutencao. */
    private String hrManutencao;
    
    /** Atributo cdUsuarioManutencao. */
    private String cdUsuarioManutencao;
    
    /** Atributo cdUsuarioManutencaoexterno. */
    private String cdUsuarioManutencaoexterno;
    
    /** Atributo cdOperacaoCanalManutencao. */
    private String cdOperacaoCanalManutencao;
    
    /** Atributo dsTipoCanalManutencao. */
    private String dsTipoCanalManutencao;
    
    /** Atributo dsComplementoInclusao. */
    private Integer dsComplementoInclusao;
    
    /** Atributo dsComplementoManutencao. */
    private Integer dsComplementoManutencao;
    
	/**
	 * Get: cdCnpjCpf.
	 *
	 * @return cdCnpjCpf
	 */
	public Long getCdCnpjCpf() {
		return cdCnpjCpf;
	}
	
	/**
	 * Set: cdCnpjCpf.
	 *
	 * @param cdCnpjCpf the cd cnpj cpf
	 */
	public void setCdCnpjCpf(Long cdCnpjCpf) {
		this.cdCnpjCpf = cdCnpjCpf;
	}
	
	/**
	 * Get: cdFilialCpfCnpj.
	 *
	 * @return cdFilialCpfCnpj
	 */
	public Integer getCdFilialCpfCnpj() {
		return cdFilialCpfCnpj;
	}
	
	/**
	 * Set: cdFilialCpfCnpj.
	 *
	 * @param cdFilialCpfCnpj the cd filial cpf cnpj
	 */
	public void setCdFilialCpfCnpj(Integer cdFilialCpfCnpj) {
		this.cdFilialCpfCnpj = cdFilialCpfCnpj;
	}
	
	/**
	 * Get: cdCtrlCpfCnpjSaida.
	 *
	 * @return cdCtrlCpfCnpjSaida
	 */
	public Integer getCdCtrlCpfCnpjSaida() {
		return cdCtrlCpfCnpjSaida;
	}
	
	/**
	 * Set: cdCtrlCpfCnpjSaida.
	 *
	 * @param cdCtrlCpfCnpjSaida the cd ctrl cpf cnpj saida
	 */
	public void setCdCtrlCpfCnpjSaida(Integer cdCtrlCpfCnpjSaida) {
		this.cdCtrlCpfCnpjSaida = cdCtrlCpfCnpjSaida;
	}
	
	/**
	 * Get: dsRazaoSocial.
	 *
	 * @return dsRazaoSocial
	 */
	public String getDsRazaoSocial() {
		return dsRazaoSocial;
	}
	
	/**
	 * Set: dsRazaoSocial.
	 *
	 * @param dsRazaoSocial the ds razao social
	 */
	public void setDsRazaoSocial(String dsRazaoSocial) {
		this.dsRazaoSocial = dsRazaoSocial;
	}
	
	/**
	 * Get: cdAgenciaSaida.
	 *
	 * @return cdAgenciaSaida
	 */
	public Integer getCdAgenciaSaida() {
		return cdAgenciaSaida;
	}
	
	/**
	 * Set: cdAgenciaSaida.
	 *
	 * @param cdAgenciaSaida the cd agencia saida
	 */
	public void setCdAgenciaSaida(Integer cdAgenciaSaida) {
		this.cdAgenciaSaida = cdAgenciaSaida;
	}
	
	/**
	 * Get: cdContaCorrenteSaida.
	 *
	 * @return cdContaCorrenteSaida
	 */
	public Integer getCdContaCorrenteSaida() {
		return cdContaCorrenteSaida;
	}
	
	/**
	 * Set: cdContaCorrenteSaida.
	 *
	 * @param cdContaCorrenteSaida the cd conta corrente saida
	 */
	public void setCdContaCorrenteSaida(Integer cdContaCorrenteSaida) {
		this.cdContaCorrenteSaida = cdContaCorrenteSaida;
	}
	
	/**
	 * Get: cdDigitoConta.
	 *
	 * @return cdDigitoConta
	 */
	public String getCdDigitoConta() {
		return cdDigitoConta;
	}
	
	/**
	 * Set: cdDigitoConta.
	 *
	 * @param cdDigitoConta the cd digito conta
	 */
	public void setCdDigitoConta(String cdDigitoConta) {
		this.cdDigitoConta = cdDigitoConta;
	}
	
	/**
	 * Get: cdConveCtaSalarial.
	 *
	 * @return cdConveCtaSalarial
	 */
	public Long getCdConveCtaSalarial() {
		return cdConveCtaSalarial;
	}
	
	/**
	 * Set: cdConveCtaSalarial.
	 *
	 * @param cdConveCtaSalarial the cd conve cta salarial
	 */
	public void setCdConveCtaSalarial(Long cdConveCtaSalarial) {
		this.cdConveCtaSalarial = cdConveCtaSalarial;
	}
	
	/**
	 * Get: cdIndicadorPrevidencia.
	 *
	 * @return cdIndicadorPrevidencia
	 */
	public String getCdIndicadorPrevidencia() {
		return cdIndicadorPrevidencia;
	}
	
	/**
	 * Set: cdIndicadorPrevidencia.
	 *
	 * @param cdIndicadorPrevidencia the cd indicador previdencia
	 */
	public void setCdIndicadorPrevidencia(String cdIndicadorPrevidencia) {
		this.cdIndicadorPrevidencia = cdIndicadorPrevidencia;
	}
	
	/**
	 * Get: descPrevidencia.
	 *
	 * @return descPrevidencia
	 */
	public String getDescPrevidencia(){
		return getCdIndicadorPrevidencia().equals("S")?"Sim":"N�o";
	}
	
	/**
	 * Get: dtInclusao.
	 *
	 * @return dtInclusao
	 */
	public String getDtInclusao() {
		return dtInclusao!=null?dtInclusao.replaceAll("\\.", "/"):"";
	}
	
	/**
	 * Set: dtInclusao.
	 *
	 * @param dtInclusao the dt inclusao
	 */
	public void setDtInclusao(String dtInclusao) {
		this.dtInclusao = dtInclusao;
	}
	
	/**
	 * Get: hrInclusao.
	 *
	 * @return hrInclusao
	 */
	public String getHrInclusao() {
		return hrInclusao!=null?hrInclusao.replaceAll("\\.", ":"):"";
	}
	
	/**
	 * Set: hrInclusao.
	 *
	 * @param hrInclusao the hr inclusao
	 */
	public void setHrInclusao(String hrInclusao) {
		this.hrInclusao = hrInclusao;
	}
	
	/**
	 * Get: cdUsuarioInclusao.
	 *
	 * @return cdUsuarioInclusao
	 */
	public String getCdUsuarioInclusao() {
		return cdUsuarioInclusao;
	}
	
	/**
	 * Set: cdUsuarioInclusao.
	 *
	 * @param cdUsuarioInclusao the cd usuario inclusao
	 */
	public void setCdUsuarioInclusao(String cdUsuarioInclusao) {
		this.cdUsuarioInclusao = cdUsuarioInclusao;
	}
	
	/**
	 * Get: cdUsuarioInclusaoExterno.
	 *
	 * @return cdUsuarioInclusaoExterno
	 */
	public String getCdUsuarioInclusaoExterno() {
		return cdUsuarioInclusaoExterno;
	}
	
	/**
	 * Set: cdUsuarioInclusaoExterno.
	 *
	 * @param cdUsuarioInclusaoExterno the cd usuario inclusao externo
	 */
	public void setCdUsuarioInclusaoExterno(String cdUsuarioInclusaoExterno) {
		this.cdUsuarioInclusaoExterno = cdUsuarioInclusaoExterno;
	}
	
	/**
	 * Get: cdOperacaoCanalInclusao.
	 *
	 * @return cdOperacaoCanalInclusao
	 */
	public String getCdOperacaoCanalInclusao() {
		return cdOperacaoCanalInclusao;
	}
	
	/**
	 * Set: cdOperacaoCanalInclusao.
	 *
	 * @param cdOperacaoCanalInclusao the cd operacao canal inclusao
	 */
	public void setCdOperacaoCanalInclusao(String cdOperacaoCanalInclusao) {
		this.cdOperacaoCanalInclusao = cdOperacaoCanalInclusao;
	}
	
	/**
	 * Get: dsTipoCanalInclusao.
	 *
	 * @return dsTipoCanalInclusao
	 */
	public String getDsTipoCanalInclusao() {
		return dsTipoCanalInclusao;
	}
	
	/**
	 * Set: dsTipoCanalInclusao.
	 *
	 * @param dsTipoCanalInclusao the ds tipo canal inclusao
	 */
	public void setDsTipoCanalInclusao(String dsTipoCanalInclusao) {
		this.dsTipoCanalInclusao = dsTipoCanalInclusao;
	}
	
	/**
	 * Get: dtManutencao.
	 *
	 * @return dtManutencao
	 */
	public String getDtManutencao() {
		return dtManutencao!=null?dtManutencao.replaceAll("\\.", "/"):"";
	}
	
	/**
	 * Set: dtManutencao.
	 *
	 * @param dtManutencao the dt manutencao
	 */
	public void setDtManutencao(String dtManutencao) {
		this.dtManutencao = dtManutencao;
	}
	
	/**
	 * Get: hrManutencao.
	 *
	 * @return hrManutencao
	 */
	public String getHrManutencao() {
		return hrManutencao!=null?hrManutencao.replaceAll("\\.", ":"):"";
	}
	
	/**
	 * Set: hrManutencao.
	 *
	 * @param hrManutencao the hr manutencao
	 */
	public void setHrManutencao(String hrManutencao) {
		this.hrManutencao = hrManutencao;
	}
	
	/**
	 * Get: cdUsuarioManutencao.
	 *
	 * @return cdUsuarioManutencao
	 */
	public String getCdUsuarioManutencao() {
		return cdUsuarioManutencao;
	}
	
	/**
	 * Set: cdUsuarioManutencao.
	 *
	 * @param cdUsuarioManutencao the cd usuario manutencao
	 */
	public void setCdUsuarioManutencao(String cdUsuarioManutencao) {
		this.cdUsuarioManutencao = cdUsuarioManutencao;
	}
	
	/**
	 * Get: cdUsuarioManutencaoexterno.
	 *
	 * @return cdUsuarioManutencaoexterno
	 */
	public String getCdUsuarioManutencaoexterno() {
		return cdUsuarioManutencaoexterno;
	}
	
	/**
	 * Set: cdUsuarioManutencaoexterno.
	 *
	 * @param cdUsuarioManutencaoexterno the cd usuario manutencaoexterno
	 */
	public void setCdUsuarioManutencaoexterno(String cdUsuarioManutencaoexterno) {
		this.cdUsuarioManutencaoexterno = cdUsuarioManutencaoexterno;
	}
	
	/**
	 * Get: cdOperacaoCanalManutencao.
	 *
	 * @return cdOperacaoCanalManutencao
	 */
	public String getCdOperacaoCanalManutencao() {
		return cdOperacaoCanalManutencao;
	}
	
	/**
	 * Set: cdOperacaoCanalManutencao.
	 *
	 * @param cdOperacaoCanalManutencao the cd operacao canal manutencao
	 */
	public void setCdOperacaoCanalManutencao(String cdOperacaoCanalManutencao) {
		this.cdOperacaoCanalManutencao = cdOperacaoCanalManutencao;
	}
	
	/**
	 * Get: dsTipoCanalManutencao.
	 *
	 * @return dsTipoCanalManutencao
	 */
	public String getDsTipoCanalManutencao() {
		return dsTipoCanalManutencao;
	}
	
	/**
	 * Set: dsTipoCanalManutencao.
	 *
	 * @param dsTipoCanalManutencao the ds tipo canal manutencao
	 */
	public void setDsTipoCanalManutencao(String dsTipoCanalManutencao) {
		this.dsTipoCanalManutencao = dsTipoCanalManutencao;
	}
	
	/**
	 * Get: cnpjFormatado.
	 *
	 * @return cnpjFormatado
	 */
	public String getCnpjFormatado(){
		return CpfCnpjUtils.formatarCpfCnpj(getCdCnpjCpf(), getCdFilialCpfCnpj() ,getCdCtrlCpfCnpjSaida());
	}
	
	/**
	 * Get: contaFormatada.
	 *
	 * @return contaFormatada
	 */
	public String getContaFormatada(){
		return getCdContaCorrenteSaida() + "-" + getCdDigitoConta();
	}
	
	/**
	 * Get: dsComplementoInclusao.
	 *
	 * @return dsComplementoInclusao
	 */
	public Integer getDsComplementoInclusao() {
		return dsComplementoInclusao;
	}
	
	/**
	 * Set: dsComplementoInclusao.
	 *
	 * @param dsComplementoInclusao the ds complemento inclusao
	 */
	public void setDsComplementoInclusao(Integer dsComplementoInclusao) {
		this.dsComplementoInclusao = dsComplementoInclusao;
	}
	
	/**
	 * Get: dsComplementoManutencao.
	 *
	 * @return dsComplementoManutencao
	 */
	public Integer getDsComplementoManutencao() {
		return dsComplementoManutencao;
	}
	
	/**
	 * Set: dsComplementoManutencao.
	 *
	 * @param dsComplementoManutencao the ds complemento manutencao
	 */
	public void setDsComplementoManutencao(Integer dsComplementoManutencao) {
		this.dsComplementoManutencao = dsComplementoManutencao;
	}
}
