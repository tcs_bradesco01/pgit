/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/implementation.ftl,v $
 * $Id: implementation.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: implementation.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */
 
package br.com.bradesco.web.pgit.service.business.manterconenvioarqformaliquidacao.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.bradesco.web.pgit.service.business.manterconenvioarqformaliquidacao.IManterConEnvioArqFormaLiquidacaoService;
import br.com.bradesco.web.pgit.service.business.manterconenvioarqformaliquidacao.IManterConEnvioArqFormaLiquidacaoServiceConstants;
import br.com.bradesco.web.pgit.service.business.manterconenvioarqformaliquidacao.bean.ConsutarContrEnvioFormaLiquidacaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterconenvioarqformaliquidacao.bean.ConsutarContrEnvioFormaLiquidacaoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterconenvioarqformaliquidacao.bean.DetalharContrEnvioFormaLiquidacaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterconenvioarqformaliquidacao.bean.DetalharContrEnvioFormaLiquidacaoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterconenvioarqformaliquidacao.bean.ExcluirContrEnvioArqFormaLiquidacaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterconenvioarqformaliquidacao.bean.ExcluirContrEnvioArqFormaLiquidacaoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterconenvioarqformaliquidacao.bean.IncluirContrEnvioArqFormaLiquidacaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterconenvioarqformaliquidacao.bean.IncluirContrEnvioArqFormaLiquidacaoSaidaDTO;
import br.com.bradesco.web.pgit.service.data.pdc.FactoryAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consutarcontrenvioformaliquidacao.request.ConsutarContrEnvioFormaLiquidacaoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consutarcontrenvioformaliquidacao.response.ConsutarContrEnvioFormaLiquidacaoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.detalharcontrenvioformaliquidacao.request.DetalharContrEnvioFormaLiquidacaoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.detalharcontrenvioformaliquidacao.response.DetalharContrEnvioFormaLiquidacaoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.excluircontrenvioarqformaliquidacao.request.ExcluirContrEnvioArqFormaLiquidacaoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.excluircontrenvioarqformaliquidacao.response.ExcluirContrEnvioArqFormaLiquidacaoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.incluircontrenvioarqformaliquidacao.request.IncluirContrEnvioArqFormaLiquidacaoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.incluircontrenvioarqformaliquidacao.response.IncluirContrEnvioArqFormaLiquidacaoResponse;
import br.com.bradesco.web.pgit.utils.PgitUtil;


/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Implementa��o do adaptador: ManterConEnvioArqFormaLiquidacao
 * </p>
 * 
 * @comment C�DIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public class ManterConEnvioArqFormaLiquidacaoServiceImpl implements IManterConEnvioArqFormaLiquidacaoService {
	
	/** Atributo factoryAdapter. */
	private FactoryAdapter factoryAdapter;
	
	/**
	 * Get: factoryAdapter.
	 *
	 * @return factoryAdapter
	 */
	public FactoryAdapter getFactoryAdapter() {
		return factoryAdapter;
	}
	
	/**
	 * Set: factoryAdapter.
	 *
	 * @param factoryAdapter the factory adapter
	 */
	public void setFactoryAdapter(FactoryAdapter factoryAdapter) {
		this.factoryAdapter = factoryAdapter;
	}
	
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.manterconenvioarqformaliquidacao.IManterConEnvioArqFormaLiquidacaoService#consutarContrEnvioFormaLiquidacao(br.com.bradesco.web.pgit.service.business.manterconenvioarqformaliquidacao.bean.ConsutarContrEnvioFormaLiquidacaoEntradaDTO)
	 */
	public List<ConsutarContrEnvioFormaLiquidacaoSaidaDTO> consutarContrEnvioFormaLiquidacao(ConsutarContrEnvioFormaLiquidacaoEntradaDTO entrada){
		List<ConsutarContrEnvioFormaLiquidacaoSaidaDTO> listaSaida = new ArrayList<ConsutarContrEnvioFormaLiquidacaoSaidaDTO>();
		ConsutarContrEnvioFormaLiquidacaoRequest request = new ConsutarContrEnvioFormaLiquidacaoRequest();
		ConsutarContrEnvioFormaLiquidacaoResponse response = new ConsutarContrEnvioFormaLiquidacaoResponse();

		request.setNrOcorrencias(IManterConEnvioArqFormaLiquidacaoServiceConstants.NUMERO_OCORRENCIAS_CONSULTAR);
		request.setCdFormaLiquidacao(PgitUtil.verificaIntegerNulo(entrada.getCdFormaLiquidacao()));
		request.setNrEnvioFormaLiquidacaoDoc(PgitUtil.verificaIntegerNulo(entrada.getNrEnvioFormaLiquidacaoDoc()));

		response = getFactoryAdapter().getConsutarContrEnvioFormaLiquidacaoPDCAdapter().invokeProcess(request);

		for(int i=0;i<response.getOcorrenicasCount();i++){
			ConsutarContrEnvioFormaLiquidacaoSaidaDTO saida = new ConsutarContrEnvioFormaLiquidacaoSaidaDTO();
			saida.setCodMensagem(response.getCodMensagem());
			saida.setMensagem(response.getMensagem());
			saida.setNumeroLinhas(response.getNumeroLinhas());
			saida.setCdFormaLiquidacao(response.getOcorrenicas(i).getCdFormaLiquidacao());
			saida.setNrEnvioFormaLiquidacaoDoc(response.getOcorrenicas(i).getNrEnvioFormaLiquidacaoDoc());
			saida.setHrEnvioFormaLiquidacaoDoc(StringUtils.left(response.getOcorrenicas(i).getHrEnvioFormaLiquidacaoDoc(), 5).replace(".", ":"));
			saida.setDsFormaLiquidacao(response.getOcorrenicas(i).getDsFormaLiquidacao());
			saida.setFormaLiquidacaoFormatado(PgitUtil.concatenarCampos(saida.getCdFormaLiquidacao(), saida.getDsFormaLiquidacao(), "-"));

			listaSaida.add(saida);
		}

		return listaSaida;
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.manterconenvioarqformaliquidacao.IManterConEnvioArqFormaLiquidacaoService#detalharContrEnvioFormaLiquidacao(br.com.bradesco.web.pgit.service.business.manterconenvioarqformaliquidacao.bean.DetalharContrEnvioFormaLiquidacaoEntradaDTO)
	 */
	public DetalharContrEnvioFormaLiquidacaoSaidaDTO detalharContrEnvioFormaLiquidacao(DetalharContrEnvioFormaLiquidacaoEntradaDTO entrada){
		DetalharContrEnvioFormaLiquidacaoSaidaDTO saida = new DetalharContrEnvioFormaLiquidacaoSaidaDTO();
		DetalharContrEnvioFormaLiquidacaoRequest request = new DetalharContrEnvioFormaLiquidacaoRequest();
		DetalharContrEnvioFormaLiquidacaoResponse response = new DetalharContrEnvioFormaLiquidacaoResponse();

		request.setCdFormaLiquidacao(PgitUtil.verificaIntegerNulo(entrada.getCdFormaLiquidacao()));
		request.setNrEnvioFormaLiquidacaoDoc(PgitUtil.verificaIntegerNulo(entrada.getNrEnvioFormaLiquidacaoDoc()));

		response = getFactoryAdapter().getDetalharContrEnvioFormaLiquidacaoPDCAdapter().invokeProcess(request);

		saida.setCodMensagem(response.getCodMensagem());
		saida.setMensagem(response.getMensagem());
		saida.setCdFormaLiquidacao(response.getCdFormaLiquidacao());
		saida.setNrEnvioFormaLiquidacaoDoc(response.getNrEnvioFormaLiquidacaoDoc());
		saida.setHrEnvioFormaLiquidacaoDoc(StringUtils.left(response.getHrEnvioFormaLiquidacaoDoc(), 5));
		saida.setDsFormaLiquidacao(response.getDsFormaLiquidacao());
		saida.setCdUsuarioInclusao(response.getCdUsuarioInclusao());
		saida.setHrInclusaoRegistro(response.getHrInclusaoRegistro());
		saida.setCdOperacaoCanalInclusao(response.getCdOperacaoCanalInclusao());
		saida.setCdTipoCanalInclusao(response.getCdTipoCanalInclusao());
		saida.setCdUsuarioManutencao(response.getCdUsuarioManutencao());
		saida.setHrManutencaoRegistro(response.getHrManutencaoRegistro());
		saida.setCdOperacaoCanalManutencao(response.getCdOperacaoCanalManutencao());
		saida.setCdTipoCanalManutencao(response.getCdTipoCanalManutencao());
		saida.setDsTipoCanalInclusao(response.getDsCanalInclusao());
		saida.setDsCanalManutencao(response.getDsCanalManutencao());

		return saida;
	}
	
	
	
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.manterconenvioarqformaliquidacao.IManterConEnvioArqFormaLiquidacaoService#excluirContrEnvioArqFormaLiquidacao(br.com.bradesco.web.pgit.service.business.manterconenvioarqformaliquidacao.bean.ExcluirContrEnvioArqFormaLiquidacaoEntradaDTO)
	 */
	public ExcluirContrEnvioArqFormaLiquidacaoSaidaDTO excluirContrEnvioArqFormaLiquidacao(ExcluirContrEnvioArqFormaLiquidacaoEntradaDTO entrada) {
		ExcluirContrEnvioArqFormaLiquidacaoSaidaDTO saida = new ExcluirContrEnvioArqFormaLiquidacaoSaidaDTO();
		ExcluirContrEnvioArqFormaLiquidacaoRequest request = new ExcluirContrEnvioArqFormaLiquidacaoRequest();
		ExcluirContrEnvioArqFormaLiquidacaoResponse response = new ExcluirContrEnvioArqFormaLiquidacaoResponse();

		request.setCdFormaLiquidacao(PgitUtil.verificaIntegerNulo(entrada.getCdFormaLiquidacao()));
		request.setNrEnvioFormaLiquidacaoDoc(PgitUtil.verificaIntegerNulo(entrada.getNrEnvioFormaLiquidacaoDoc()));

		response = getFactoryAdapter().getExcluirContrEnvioArqFormaLiquidacaoPDCAdapter().invokeProcess(request);

		saida.setCodMensagem(response.getCodMensagem());
		saida.setMensagem(response.getMensagem());

		return saida;
	}
	
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.manterconenvioarqformaliquidacao.IManterConEnvioArqFormaLiquidacaoService#incluirContrEnvioArqFormaLiquidacao(br.com.bradesco.web.pgit.service.business.manterconenvioarqformaliquidacao.bean.IncluirContrEnvioArqFormaLiquidacaoEntradaDTO)
	 */
	public IncluirContrEnvioArqFormaLiquidacaoSaidaDTO incluirContrEnvioArqFormaLiquidacao(IncluirContrEnvioArqFormaLiquidacaoEntradaDTO entrada) {
		IncluirContrEnvioArqFormaLiquidacaoSaidaDTO saida = new IncluirContrEnvioArqFormaLiquidacaoSaidaDTO();
		IncluirContrEnvioArqFormaLiquidacaoRequest request = new IncluirContrEnvioArqFormaLiquidacaoRequest();
		IncluirContrEnvioArqFormaLiquidacaoResponse response = new IncluirContrEnvioArqFormaLiquidacaoResponse();

		request.setCdFormaLiquidacao(PgitUtil.verificaIntegerNulo(entrada.getCdFormaLiquidacao()));
		request.setNrEnvioFormaLiquidacaoDoc(PgitUtil.verificaIntegerNulo(entrada.getNrEnvioFormaLiquidacaoDoc()));
		request.setHrEnvioFormaLiquidacaoDoc(PgitUtil.verificaStringNula(entrada.getHrEnvioFormaLiquidacaoDoc()));

		response = getFactoryAdapter().getIncluirContrEnvioArqFormaLiquidacaoPDCAdapter().invokeProcess(request);

		saida.setCodMensagem(response.getCodMensagem());
		saida.setMensagem(response.getMensagem());

		return saida;
	}
	
	
	
	
    
   
    
}

