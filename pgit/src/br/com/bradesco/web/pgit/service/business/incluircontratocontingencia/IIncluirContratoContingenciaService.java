/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/interfaz.ftl,v $
 * $Id: interfaz.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: interfaz.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.incluircontratocontingencia;

import java.util.List;

import br.com.bradesco.web.pgit.service.business.incluircontratocontingencia.bean.ConsultarListaContasInclusaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.incluircontratocontingencia.bean.ConsultarListaContasInclusaoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.incluircontratocontingencia.bean.ConsultarServRelacServperacionalEntradaDTO;
import br.com.bradesco.web.pgit.service.business.incluircontratocontingencia.bean.ConsultarServRelacServperacionalSaidaDTO;
import br.com.bradesco.web.pgit.service.business.incluircontratocontingencia.bean.IncluirContratoPgitEntradaDTO;
import br.com.bradesco.web.pgit.service.business.incluircontratocontingencia.bean.IncluirContratoPgitSaidaDTO;
import br.com.bradesco.web.pgit.service.business.incluircontratocontingencia.bean.ListarDadosCtaConvnContingenciaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.incluircontratocontingencia.bean.ListarDadosCtaConvnContingenciaSaidaDTO;

/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Interface do adaptador: IncluirContratoContingencia
 * </p>
 * 
 * @comment CODIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public interface IIncluirContratoContingenciaService {
	
	/**
	 * Consultar serv relac servperacional.
	 *
	 * @param entradaDTO the entrada dto
	 * @return the list< consultar serv relac servperacional saida dt o>
	 */
	List<ConsultarServRelacServperacionalSaidaDTO> consultarServRelacServperacional (ConsultarServRelacServperacionalEntradaDTO entradaDTO);
	
	/**
	 * Incluir contrato pgit.
	 *
	 * @param entradaDTO the entrada dto
	 * @return the incluir contrato pgit saida dto
	 */
	IncluirContratoPgitSaidaDTO incluirContratoPgit (IncluirContratoPgitEntradaDTO entradaDTO);
	
	/**
	 * Consultar lista contas inclusao.
	 *
	 * @param entradaDTO the entrada dto
	 * @return the list< consultar lista contas inclusao saida dt o>
	 */
	List<ConsultarListaContasInclusaoSaidaDTO> consultarListaContasInclusao(ConsultarListaContasInclusaoEntradaDTO entradaDTO);
	
	/**
	 * Listar dados cta convn contingencia.
	 *
	 * @param entradaDTO the entrada dto
	 * @return the listar dados cta convn contingencia saida dto
	 */
	ListarDadosCtaConvnContingenciaSaidaDTO listarDadosCtaConvnContingencia(ListarDadosCtaConvnContingenciaEntradaDTO entradaDTO);
}