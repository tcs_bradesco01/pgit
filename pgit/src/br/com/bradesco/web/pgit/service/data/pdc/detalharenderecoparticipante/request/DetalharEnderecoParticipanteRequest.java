/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.detalharenderecoparticipante.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class DetalharEnderecoParticipanteRequest.
 * 
 * @version $Revision$ $Date$
 */
public class DetalharEnderecoParticipanteRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdpessoaJuridicaContrato
     */
    private long _cdpessoaJuridicaContrato = 0;

    /**
     * keeps track of state for field: _cdpessoaJuridicaContrato
     */
    private boolean _has_cdpessoaJuridicaContrato;

    /**
     * Field _cdTipoContratoNegocio
     */
    private int _cdTipoContratoNegocio = 0;

    /**
     * keeps track of state for field: _cdTipoContratoNegocio
     */
    private boolean _has_cdTipoContratoNegocio;

    /**
     * Field _nrSequenciaContratoNegocio
     */
    private long _nrSequenciaContratoNegocio = 0;

    /**
     * keeps track of state for field: _nrSequenciaContratoNegocio
     */
    private boolean _has_nrSequenciaContratoNegocio;

    /**
     * Field _cdTipoParticipacaoPessoa
     */
    private int _cdTipoParticipacaoPessoa = 0;

    /**
     * keeps track of state for field: _cdTipoParticipacaoPessoa
     */
    private boolean _has_cdTipoParticipacaoPessoa;

    /**
     * Field _cdCorpoCpfCnpj
     */
    private long _cdCorpoCpfCnpj = 0;

    /**
     * keeps track of state for field: _cdCorpoCpfCnpj
     */
    private boolean _has_cdCorpoCpfCnpj;

    /**
     * Field _cdCnpjContrato
     */
    private int _cdCnpjContrato = 0;

    /**
     * keeps track of state for field: _cdCnpjContrato
     */
    private boolean _has_cdCnpjContrato;

    /**
     * Field _cdCpfCnpjDigito
     */
    private int _cdCpfCnpjDigito = 0;

    /**
     * keeps track of state for field: _cdCpfCnpjDigito
     */
    private boolean _has_cdCpfCnpjDigito;

    /**
     * Field _cdPessoa
     */
    private long _cdPessoa = 0;

    /**
     * keeps track of state for field: _cdPessoa
     */
    private boolean _has_cdPessoa;

    /**
     * Field _cdFinalidadeEnderecoContrato
     */
    private int _cdFinalidadeEnderecoContrato = 0;

    /**
     * keeps track of state for field: _cdFinalidadeEnderecoContrato
     */
    private boolean _has_cdFinalidadeEnderecoContrato;

    /**
     * Field _cdEspeceEndereco
     */
    private int _cdEspeceEndereco = 0;

    /**
     * keeps track of state for field: _cdEspeceEndereco
     */
    private boolean _has_cdEspeceEndereco;

    /**
     * Field _nrSequenciaEnderecoOpcional
     */
    private int _nrSequenciaEnderecoOpcional = 0;

    /**
     * keeps track of state for field: _nrSequenciaEnderecoOpcional
     */
    private boolean _has_nrSequenciaEnderecoOpcional;

    /**
     * Field _cdTipoPessoa
     */
    private java.lang.String _cdTipoPessoa;


      //----------------/
     //- Constructors -/
    //----------------/

    public DetalharEnderecoParticipanteRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharenderecoparticipante.request.DetalharEnderecoParticipanteRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdCnpjContrato
     * 
     */
    public void deleteCdCnpjContrato()
    {
        this._has_cdCnpjContrato= false;
    } //-- void deleteCdCnpjContrato() 

    /**
     * Method deleteCdCorpoCpfCnpj
     * 
     */
    public void deleteCdCorpoCpfCnpj()
    {
        this._has_cdCorpoCpfCnpj= false;
    } //-- void deleteCdCorpoCpfCnpj() 

    /**
     * Method deleteCdCpfCnpjDigito
     * 
     */
    public void deleteCdCpfCnpjDigito()
    {
        this._has_cdCpfCnpjDigito= false;
    } //-- void deleteCdCpfCnpjDigito() 

    /**
     * Method deleteCdEspeceEndereco
     * 
     */
    public void deleteCdEspeceEndereco()
    {
        this._has_cdEspeceEndereco= false;
    } //-- void deleteCdEspeceEndereco() 

    /**
     * Method deleteCdFinalidadeEnderecoContrato
     * 
     */
    public void deleteCdFinalidadeEnderecoContrato()
    {
        this._has_cdFinalidadeEnderecoContrato= false;
    } //-- void deleteCdFinalidadeEnderecoContrato() 

    /**
     * Method deleteCdPessoa
     * 
     */
    public void deleteCdPessoa()
    {
        this._has_cdPessoa= false;
    } //-- void deleteCdPessoa() 

    /**
     * Method deleteCdTipoContratoNegocio
     * 
     */
    public void deleteCdTipoContratoNegocio()
    {
        this._has_cdTipoContratoNegocio= false;
    } //-- void deleteCdTipoContratoNegocio() 

    /**
     * Method deleteCdTipoParticipacaoPessoa
     * 
     */
    public void deleteCdTipoParticipacaoPessoa()
    {
        this._has_cdTipoParticipacaoPessoa= false;
    } //-- void deleteCdTipoParticipacaoPessoa() 

    /**
     * Method deleteCdpessoaJuridicaContrato
     * 
     */
    public void deleteCdpessoaJuridicaContrato()
    {
        this._has_cdpessoaJuridicaContrato= false;
    } //-- void deleteCdpessoaJuridicaContrato() 

    /**
     * Method deleteNrSequenciaContratoNegocio
     * 
     */
    public void deleteNrSequenciaContratoNegocio()
    {
        this._has_nrSequenciaContratoNegocio= false;
    } //-- void deleteNrSequenciaContratoNegocio() 

    /**
     * Method deleteNrSequenciaEnderecoOpcional
     * 
     */
    public void deleteNrSequenciaEnderecoOpcional()
    {
        this._has_nrSequenciaEnderecoOpcional= false;
    } //-- void deleteNrSequenciaEnderecoOpcional() 

    /**
     * Returns the value of field 'cdCnpjContrato'.
     * 
     * @return int
     * @return the value of field 'cdCnpjContrato'.
     */
    public int getCdCnpjContrato()
    {
        return this._cdCnpjContrato;
    } //-- int getCdCnpjContrato() 

    /**
     * Returns the value of field 'cdCorpoCpfCnpj'.
     * 
     * @return long
     * @return the value of field 'cdCorpoCpfCnpj'.
     */
    public long getCdCorpoCpfCnpj()
    {
        return this._cdCorpoCpfCnpj;
    } //-- long getCdCorpoCpfCnpj() 

    /**
     * Returns the value of field 'cdCpfCnpjDigito'.
     * 
     * @return int
     * @return the value of field 'cdCpfCnpjDigito'.
     */
    public int getCdCpfCnpjDigito()
    {
        return this._cdCpfCnpjDigito;
    } //-- int getCdCpfCnpjDigito() 

    /**
     * Returns the value of field 'cdEspeceEndereco'.
     * 
     * @return int
     * @return the value of field 'cdEspeceEndereco'.
     */
    public int getCdEspeceEndereco()
    {
        return this._cdEspeceEndereco;
    } //-- int getCdEspeceEndereco() 

    /**
     * Returns the value of field 'cdFinalidadeEnderecoContrato'.
     * 
     * @return int
     * @return the value of field 'cdFinalidadeEnderecoContrato'.
     */
    public int getCdFinalidadeEnderecoContrato()
    {
        return this._cdFinalidadeEnderecoContrato;
    } //-- int getCdFinalidadeEnderecoContrato() 

    /**
     * Returns the value of field 'cdPessoa'.
     * 
     * @return long
     * @return the value of field 'cdPessoa'.
     */
    public long getCdPessoa()
    {
        return this._cdPessoa;
    } //-- long getCdPessoa() 

    /**
     * Returns the value of field 'cdTipoContratoNegocio'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoNegocio'.
     */
    public int getCdTipoContratoNegocio()
    {
        return this._cdTipoContratoNegocio;
    } //-- int getCdTipoContratoNegocio() 

    /**
     * Returns the value of field 'cdTipoParticipacaoPessoa'.
     * 
     * @return int
     * @return the value of field 'cdTipoParticipacaoPessoa'.
     */
    public int getCdTipoParticipacaoPessoa()
    {
        return this._cdTipoParticipacaoPessoa;
    } //-- int getCdTipoParticipacaoPessoa() 

    /**
     * Returns the value of field 'cdTipoPessoa'.
     * 
     * @return String
     * @return the value of field 'cdTipoPessoa'.
     */
    public java.lang.String getCdTipoPessoa()
    {
        return this._cdTipoPessoa;
    } //-- java.lang.String getCdTipoPessoa() 

    /**
     * Returns the value of field 'cdpessoaJuridicaContrato'.
     * 
     * @return long
     * @return the value of field 'cdpessoaJuridicaContrato'.
     */
    public long getCdpessoaJuridicaContrato()
    {
        return this._cdpessoaJuridicaContrato;
    } //-- long getCdpessoaJuridicaContrato() 

    /**
     * Returns the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @return long
     * @return the value of field 'nrSequenciaContratoNegocio'.
     */
    public long getNrSequenciaContratoNegocio()
    {
        return this._nrSequenciaContratoNegocio;
    } //-- long getNrSequenciaContratoNegocio() 

    /**
     * Returns the value of field 'nrSequenciaEnderecoOpcional'.
     * 
     * @return int
     * @return the value of field 'nrSequenciaEnderecoOpcional'.
     */
    public int getNrSequenciaEnderecoOpcional()
    {
        return this._nrSequenciaEnderecoOpcional;
    } //-- int getNrSequenciaEnderecoOpcional() 

    /**
     * Method hasCdCnpjContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCnpjContrato()
    {
        return this._has_cdCnpjContrato;
    } //-- boolean hasCdCnpjContrato() 

    /**
     * Method hasCdCorpoCpfCnpj
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCorpoCpfCnpj()
    {
        return this._has_cdCorpoCpfCnpj;
    } //-- boolean hasCdCorpoCpfCnpj() 

    /**
     * Method hasCdCpfCnpjDigito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCpfCnpjDigito()
    {
        return this._has_cdCpfCnpjDigito;
    } //-- boolean hasCdCpfCnpjDigito() 

    /**
     * Method hasCdEspeceEndereco
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdEspeceEndereco()
    {
        return this._has_cdEspeceEndereco;
    } //-- boolean hasCdEspeceEndereco() 

    /**
     * Method hasCdFinalidadeEnderecoContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFinalidadeEnderecoContrato()
    {
        return this._has_cdFinalidadeEnderecoContrato;
    } //-- boolean hasCdFinalidadeEnderecoContrato() 

    /**
     * Method hasCdPessoa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoa()
    {
        return this._has_cdPessoa;
    } //-- boolean hasCdPessoa() 

    /**
     * Method hasCdTipoContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoNegocio()
    {
        return this._has_cdTipoContratoNegocio;
    } //-- boolean hasCdTipoContratoNegocio() 

    /**
     * Method hasCdTipoParticipacaoPessoa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoParticipacaoPessoa()
    {
        return this._has_cdTipoParticipacaoPessoa;
    } //-- boolean hasCdTipoParticipacaoPessoa() 

    /**
     * Method hasCdpessoaJuridicaContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdpessoaJuridicaContrato()
    {
        return this._has_cdpessoaJuridicaContrato;
    } //-- boolean hasCdpessoaJuridicaContrato() 

    /**
     * Method hasNrSequenciaContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaContratoNegocio()
    {
        return this._has_nrSequenciaContratoNegocio;
    } //-- boolean hasNrSequenciaContratoNegocio() 

    /**
     * Method hasNrSequenciaEnderecoOpcional
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaEnderecoOpcional()
    {
        return this._has_nrSequenciaEnderecoOpcional;
    } //-- boolean hasNrSequenciaEnderecoOpcional() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdCnpjContrato'.
     * 
     * @param cdCnpjContrato the value of field 'cdCnpjContrato'.
     */
    public void setCdCnpjContrato(int cdCnpjContrato)
    {
        this._cdCnpjContrato = cdCnpjContrato;
        this._has_cdCnpjContrato = true;
    } //-- void setCdCnpjContrato(int) 

    /**
     * Sets the value of field 'cdCorpoCpfCnpj'.
     * 
     * @param cdCorpoCpfCnpj the value of field 'cdCorpoCpfCnpj'.
     */
    public void setCdCorpoCpfCnpj(long cdCorpoCpfCnpj)
    {
        this._cdCorpoCpfCnpj = cdCorpoCpfCnpj;
        this._has_cdCorpoCpfCnpj = true;
    } //-- void setCdCorpoCpfCnpj(long) 

    /**
     * Sets the value of field 'cdCpfCnpjDigito'.
     * 
     * @param cdCpfCnpjDigito the value of field 'cdCpfCnpjDigito'.
     */
    public void setCdCpfCnpjDigito(int cdCpfCnpjDigito)
    {
        this._cdCpfCnpjDigito = cdCpfCnpjDigito;
        this._has_cdCpfCnpjDigito = true;
    } //-- void setCdCpfCnpjDigito(int) 

    /**
     * Sets the value of field 'cdEspeceEndereco'.
     * 
     * @param cdEspeceEndereco the value of field 'cdEspeceEndereco'
     */
    public void setCdEspeceEndereco(int cdEspeceEndereco)
    {
        this._cdEspeceEndereco = cdEspeceEndereco;
        this._has_cdEspeceEndereco = true;
    } //-- void setCdEspeceEndereco(int) 

    /**
     * Sets the value of field 'cdFinalidadeEnderecoContrato'.
     * 
     * @param cdFinalidadeEnderecoContrato the value of field
     * 'cdFinalidadeEnderecoContrato'.
     */
    public void setCdFinalidadeEnderecoContrato(int cdFinalidadeEnderecoContrato)
    {
        this._cdFinalidadeEnderecoContrato = cdFinalidadeEnderecoContrato;
        this._has_cdFinalidadeEnderecoContrato = true;
    } //-- void setCdFinalidadeEnderecoContrato(int) 

    /**
     * Sets the value of field 'cdPessoa'.
     * 
     * @param cdPessoa the value of field 'cdPessoa'.
     */
    public void setCdPessoa(long cdPessoa)
    {
        this._cdPessoa = cdPessoa;
        this._has_cdPessoa = true;
    } //-- void setCdPessoa(long) 

    /**
     * Sets the value of field 'cdTipoContratoNegocio'.
     * 
     * @param cdTipoContratoNegocio the value of field
     * 'cdTipoContratoNegocio'.
     */
    public void setCdTipoContratoNegocio(int cdTipoContratoNegocio)
    {
        this._cdTipoContratoNegocio = cdTipoContratoNegocio;
        this._has_cdTipoContratoNegocio = true;
    } //-- void setCdTipoContratoNegocio(int) 

    /**
     * Sets the value of field 'cdTipoParticipacaoPessoa'.
     * 
     * @param cdTipoParticipacaoPessoa the value of field
     * 'cdTipoParticipacaoPessoa'.
     */
    public void setCdTipoParticipacaoPessoa(int cdTipoParticipacaoPessoa)
    {
        this._cdTipoParticipacaoPessoa = cdTipoParticipacaoPessoa;
        this._has_cdTipoParticipacaoPessoa = true;
    } //-- void setCdTipoParticipacaoPessoa(int) 

    /**
     * Sets the value of field 'cdTipoPessoa'.
     * 
     * @param cdTipoPessoa the value of field 'cdTipoPessoa'.
     */
    public void setCdTipoPessoa(java.lang.String cdTipoPessoa)
    {
        this._cdTipoPessoa = cdTipoPessoa;
    } //-- void setCdTipoPessoa(java.lang.String) 

    /**
     * Sets the value of field 'cdpessoaJuridicaContrato'.
     * 
     * @param cdpessoaJuridicaContrato the value of field
     * 'cdpessoaJuridicaContrato'.
     */
    public void setCdpessoaJuridicaContrato(long cdpessoaJuridicaContrato)
    {
        this._cdpessoaJuridicaContrato = cdpessoaJuridicaContrato;
        this._has_cdpessoaJuridicaContrato = true;
    } //-- void setCdpessoaJuridicaContrato(long) 

    /**
     * Sets the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @param nrSequenciaContratoNegocio the value of field
     * 'nrSequenciaContratoNegocio'.
     */
    public void setNrSequenciaContratoNegocio(long nrSequenciaContratoNegocio)
    {
        this._nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
        this._has_nrSequenciaContratoNegocio = true;
    } //-- void setNrSequenciaContratoNegocio(long) 

    /**
     * Sets the value of field 'nrSequenciaEnderecoOpcional'.
     * 
     * @param nrSequenciaEnderecoOpcional the value of field
     * 'nrSequenciaEnderecoOpcional'.
     */
    public void setNrSequenciaEnderecoOpcional(int nrSequenciaEnderecoOpcional)
    {
        this._nrSequenciaEnderecoOpcional = nrSequenciaEnderecoOpcional;
        this._has_nrSequenciaEnderecoOpcional = true;
    } //-- void setNrSequenciaEnderecoOpcional(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return DetalharEnderecoParticipanteRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.detalharenderecoparticipante.request.DetalharEnderecoParticipanteRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.detalharenderecoparticipante.request.DetalharEnderecoParticipanteRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.detalharenderecoparticipante.request.DetalharEnderecoParticipanteRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharenderecoparticipante.request.DetalharEnderecoParticipanteRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
