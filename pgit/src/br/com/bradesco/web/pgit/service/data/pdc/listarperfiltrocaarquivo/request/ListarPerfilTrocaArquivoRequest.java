/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listarperfiltrocaarquivo.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ListarPerfilTrocaArquivoRequest.
 * 
 * @version $Revision$ $Date$
 */
public class ListarPerfilTrocaArquivoRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _numeroOcorrencias
     */
    private int _numeroOcorrencias = 0;

    /**
     * keeps track of state for field: _numeroOcorrencias
     */
    private boolean _has_numeroOcorrencias;

    /**
     * Field _cdPessoaJuridicaContrato
     */
    private long _cdPessoaJuridicaContrato = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridicaContrato
     */
    private boolean _has_cdPessoaJuridicaContrato;

    /**
     * Field _cdTipoContratoNegocio
     */
    private int _cdTipoContratoNegocio = 0;

    /**
     * keeps track of state for field: _cdTipoContratoNegocio
     */
    private boolean _has_cdTipoContratoNegocio;

    /**
     * Field _nrSequenciaContratoNegocio
     */
    private long _nrSequenciaContratoNegocio = 0;

    /**
     * keeps track of state for field: _nrSequenciaContratoNegocio
     */
    private boolean _has_nrSequenciaContratoNegocio;

    /**
     * Field _cdPessoa
     */
    private long _cdPessoa = 0;

    /**
     * keeps track of state for field: _cdPessoa
     */
    private boolean _has_cdPessoa;


      //----------------/
     //- Constructors -/
    //----------------/

    public ListarPerfilTrocaArquivoRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarperfiltrocaarquivo.request.ListarPerfilTrocaArquivoRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdPessoa
     * 
     */
    public void deleteCdPessoa()
    {
        this._has_cdPessoa= false;
    } //-- void deleteCdPessoa() 

    /**
     * Method deleteCdPessoaJuridicaContrato
     * 
     */
    public void deleteCdPessoaJuridicaContrato()
    {
        this._has_cdPessoaJuridicaContrato= false;
    } //-- void deleteCdPessoaJuridicaContrato() 

    /**
     * Method deleteCdTipoContratoNegocio
     * 
     */
    public void deleteCdTipoContratoNegocio()
    {
        this._has_cdTipoContratoNegocio= false;
    } //-- void deleteCdTipoContratoNegocio() 

    /**
     * Method deleteNrSequenciaContratoNegocio
     * 
     */
    public void deleteNrSequenciaContratoNegocio()
    {
        this._has_nrSequenciaContratoNegocio= false;
    } //-- void deleteNrSequenciaContratoNegocio() 

    /**
     * Method deleteNumeroOcorrencias
     * 
     */
    public void deleteNumeroOcorrencias()
    {
        this._has_numeroOcorrencias= false;
    } //-- void deleteNumeroOcorrencias() 

    /**
     * Returns the value of field 'cdPessoa'.
     * 
     * @return long
     * @return the value of field 'cdPessoa'.
     */
    public long getCdPessoa()
    {
        return this._cdPessoa;
    } //-- long getCdPessoa() 

    /**
     * Returns the value of field 'cdPessoaJuridicaContrato'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridicaContrato'.
     */
    public long getCdPessoaJuridicaContrato()
    {
        return this._cdPessoaJuridicaContrato;
    } //-- long getCdPessoaJuridicaContrato() 

    /**
     * Returns the value of field 'cdTipoContratoNegocio'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoNegocio'.
     */
    public int getCdTipoContratoNegocio()
    {
        return this._cdTipoContratoNegocio;
    } //-- int getCdTipoContratoNegocio() 

    /**
     * Returns the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @return long
     * @return the value of field 'nrSequenciaContratoNegocio'.
     */
    public long getNrSequenciaContratoNegocio()
    {
        return this._nrSequenciaContratoNegocio;
    } //-- long getNrSequenciaContratoNegocio() 

    /**
     * Returns the value of field 'numeroOcorrencias'.
     * 
     * @return int
     * @return the value of field 'numeroOcorrencias'.
     */
    public int getNumeroOcorrencias()
    {
        return this._numeroOcorrencias;
    } //-- int getNumeroOcorrencias() 

    /**
     * Method hasCdPessoa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoa()
    {
        return this._has_cdPessoa;
    } //-- boolean hasCdPessoa() 

    /**
     * Method hasCdPessoaJuridicaContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridicaContrato()
    {
        return this._has_cdPessoaJuridicaContrato;
    } //-- boolean hasCdPessoaJuridicaContrato() 

    /**
     * Method hasCdTipoContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoNegocio()
    {
        return this._has_cdTipoContratoNegocio;
    } //-- boolean hasCdTipoContratoNegocio() 

    /**
     * Method hasNrSequenciaContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaContratoNegocio()
    {
        return this._has_nrSequenciaContratoNegocio;
    } //-- boolean hasNrSequenciaContratoNegocio() 

    /**
     * Method hasNumeroOcorrencias
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNumeroOcorrencias()
    {
        return this._has_numeroOcorrencias;
    } //-- boolean hasNumeroOcorrencias() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdPessoa'.
     * 
     * @param cdPessoa the value of field 'cdPessoa'.
     */
    public void setCdPessoa(long cdPessoa)
    {
        this._cdPessoa = cdPessoa;
        this._has_cdPessoa = true;
    } //-- void setCdPessoa(long) 

    /**
     * Sets the value of field 'cdPessoaJuridicaContrato'.
     * 
     * @param cdPessoaJuridicaContrato the value of field
     * 'cdPessoaJuridicaContrato'.
     */
    public void setCdPessoaJuridicaContrato(long cdPessoaJuridicaContrato)
    {
        this._cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
        this._has_cdPessoaJuridicaContrato = true;
    } //-- void setCdPessoaJuridicaContrato(long) 

    /**
     * Sets the value of field 'cdTipoContratoNegocio'.
     * 
     * @param cdTipoContratoNegocio the value of field
     * 'cdTipoContratoNegocio'.
     */
    public void setCdTipoContratoNegocio(int cdTipoContratoNegocio)
    {
        this._cdTipoContratoNegocio = cdTipoContratoNegocio;
        this._has_cdTipoContratoNegocio = true;
    } //-- void setCdTipoContratoNegocio(int) 

    /**
     * Sets the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @param nrSequenciaContratoNegocio the value of field
     * 'nrSequenciaContratoNegocio'.
     */
    public void setNrSequenciaContratoNegocio(long nrSequenciaContratoNegocio)
    {
        this._nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
        this._has_nrSequenciaContratoNegocio = true;
    } //-- void setNrSequenciaContratoNegocio(long) 

    /**
     * Sets the value of field 'numeroOcorrencias'.
     * 
     * @param numeroOcorrencias the value of field
     * 'numeroOcorrencias'.
     */
    public void setNumeroOcorrencias(int numeroOcorrencias)
    {
        this._numeroOcorrencias = numeroOcorrencias;
        this._has_numeroOcorrencias = true;
    } //-- void setNumeroOcorrencias(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ListarPerfilTrocaArquivoRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listarperfiltrocaarquivo.request.ListarPerfilTrocaArquivoRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listarperfiltrocaarquivo.request.ListarPerfilTrocaArquivoRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listarperfiltrocaarquivo.request.ListarPerfilTrocaArquivoRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarperfiltrocaarquivo.request.ListarPerfilTrocaArquivoRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
