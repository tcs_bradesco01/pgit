/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultaragenciaopetarifapadrao.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.math.BigDecimal;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ConsultarAgenciaOpeTarifaPadraoResponse.
 * 
 * @version $Revision$ $Date$
 */
public class ConsultarAgenciaOpeTarifaPadraoResponse implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _codMensagem
     */
    private java.lang.String _codMensagem;

    /**
     * Field _mensagem
     */
    private java.lang.String _mensagem;

    /**
     * Field _vlTarifaPadrao
     */
    private java.math.BigDecimal _vlTarifaPadrao = new java.math.BigDecimal("0");

    /**
     * Field _cdAgenciaOperadora
     */
    private int _cdAgenciaOperadora = 0;

    /**
     * keeps track of state for field: _cdAgenciaOperadora
     */
    private boolean _has_cdAgenciaOperadora;

    /**
     * Field _dsAgenciaOperadora
     */
    private java.lang.String _dsAgenciaOperadora;

    /**
     * Field _cdIndicadorDescontoBloqueio
     */
    private java.lang.String _cdIndicadorDescontoBloqueio;


      //----------------/
     //- Constructors -/
    //----------------/

    public ConsultarAgenciaOpeTarifaPadraoResponse() 
     {
        super();
        setVlTarifaPadrao(new java.math.BigDecimal("0"));
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultaragenciaopetarifapadrao.response.ConsultarAgenciaOpeTarifaPadraoResponse()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdAgenciaOperadora
     * 
     */
    public void deleteCdAgenciaOperadora()
    {
        this._has_cdAgenciaOperadora= false;
    } //-- void deleteCdAgenciaOperadora() 

    /**
     * Returns the value of field 'cdAgenciaOperadora'.
     * 
     * @return int
     * @return the value of field 'cdAgenciaOperadora'.
     */
    public int getCdAgenciaOperadora()
    {
        return this._cdAgenciaOperadora;
    } //-- int getCdAgenciaOperadora() 

    /**
     * Returns the value of field 'cdIndicadorDescontoBloqueio'.
     * 
     * @return String
     * @return the value of field 'cdIndicadorDescontoBloqueio'.
     */
    public java.lang.String getCdIndicadorDescontoBloqueio()
    {
        return this._cdIndicadorDescontoBloqueio;
    } //-- java.lang.String getCdIndicadorDescontoBloqueio() 

    /**
     * Returns the value of field 'codMensagem'.
     * 
     * @return String
     * @return the value of field 'codMensagem'.
     */
    public java.lang.String getCodMensagem()
    {
        return this._codMensagem;
    } //-- java.lang.String getCodMensagem() 

    /**
     * Returns the value of field 'dsAgenciaOperadora'.
     * 
     * @return String
     * @return the value of field 'dsAgenciaOperadora'.
     */
    public java.lang.String getDsAgenciaOperadora()
    {
        return this._dsAgenciaOperadora;
    } //-- java.lang.String getDsAgenciaOperadora() 

    /**
     * Returns the value of field 'mensagem'.
     * 
     * @return String
     * @return the value of field 'mensagem'.
     */
    public java.lang.String getMensagem()
    {
        return this._mensagem;
    } //-- java.lang.String getMensagem() 

    /**
     * Returns the value of field 'vlTarifaPadrao'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlTarifaPadrao'.
     */
    public java.math.BigDecimal getVlTarifaPadrao()
    {
        return this._vlTarifaPadrao;
    } //-- java.math.BigDecimal getVlTarifaPadrao() 

    /**
     * Method hasCdAgenciaOperadora
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgenciaOperadora()
    {
        return this._has_cdAgenciaOperadora;
    } //-- boolean hasCdAgenciaOperadora() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdAgenciaOperadora'.
     * 
     * @param cdAgenciaOperadora the value of field
     * 'cdAgenciaOperadora'.
     */
    public void setCdAgenciaOperadora(int cdAgenciaOperadora)
    {
        this._cdAgenciaOperadora = cdAgenciaOperadora;
        this._has_cdAgenciaOperadora = true;
    } //-- void setCdAgenciaOperadora(int) 

    /**
     * Sets the value of field 'cdIndicadorDescontoBloqueio'.
     * 
     * @param cdIndicadorDescontoBloqueio the value of field
     * 'cdIndicadorDescontoBloqueio'.
     */
    public void setCdIndicadorDescontoBloqueio(java.lang.String cdIndicadorDescontoBloqueio)
    {
        this._cdIndicadorDescontoBloqueio = cdIndicadorDescontoBloqueio;
    } //-- void setCdIndicadorDescontoBloqueio(java.lang.String) 

    /**
     * Sets the value of field 'codMensagem'.
     * 
     * @param codMensagem the value of field 'codMensagem'.
     */
    public void setCodMensagem(java.lang.String codMensagem)
    {
        this._codMensagem = codMensagem;
    } //-- void setCodMensagem(java.lang.String) 

    /**
     * Sets the value of field 'dsAgenciaOperadora'.
     * 
     * @param dsAgenciaOperadora the value of field
     * 'dsAgenciaOperadora'.
     */
    public void setDsAgenciaOperadora(java.lang.String dsAgenciaOperadora)
    {
        this._dsAgenciaOperadora = dsAgenciaOperadora;
    } //-- void setDsAgenciaOperadora(java.lang.String) 

    /**
     * Sets the value of field 'mensagem'.
     * 
     * @param mensagem the value of field 'mensagem'.
     */
    public void setMensagem(java.lang.String mensagem)
    {
        this._mensagem = mensagem;
    } //-- void setMensagem(java.lang.String) 

    /**
     * Sets the value of field 'vlTarifaPadrao'.
     * 
     * @param vlTarifaPadrao the value of field 'vlTarifaPadrao'.
     */
    public void setVlTarifaPadrao(java.math.BigDecimal vlTarifaPadrao)
    {
        this._vlTarifaPadrao = vlTarifaPadrao;
    } //-- void setVlTarifaPadrao(java.math.BigDecimal) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ConsultarAgenciaOpeTarifaPadraoResponse
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultaragenciaopetarifapadrao.response.ConsultarAgenciaOpeTarifaPadraoResponse unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultaragenciaopetarifapadrao.response.ConsultarAgenciaOpeTarifaPadraoResponse) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultaragenciaopetarifapadrao.response.ConsultarAgenciaOpeTarifaPadraoResponse.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultaragenciaopetarifapadrao.response.ConsultarAgenciaOpeTarifaPadraoResponse unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
