/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantercadcontadestino.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantercadcontadestino.bean;

import java.util.ArrayList;
import java.util.List;

/**
 * Nome: ValidarServicoContratadoEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ValidarServicoContratadoEntradaDTO {

	/** Atributo cdProdutoServico. */
	private Integer cdProdutoServico;
	
	/** Atributo ocorrencias. */
	private List<Integer> ocorrencias = new ArrayList<Integer>();

	/**
	 * Get: cdProdutoServico.
	 *
	 * @return cdProdutoServico
	 */
	public Integer getCdProdutoServico() {
		return cdProdutoServico;
	}

	/**
	 * Set: cdProdutoServico.
	 *
	 * @param cdProdutoServico the cd produto servico
	 */
	public void setCdProdutoServico(Integer cdProdutoServico) {
		this.cdProdutoServico = cdProdutoServico;
	}

	/**
	 * Get: ocorrencias.
	 *
	 * @return ocorrencias
	 */
	public List<Integer> getOcorrencias() {
		return ocorrencias;
	}

	/**
	 * Set: ocorrencias.
	 *
	 * @param ocorrencias the ocorrencias
	 */
	public void setOcorrencias(List<Integer> ocorrencias) {
		this.ocorrencias = ocorrencias;
	}
}