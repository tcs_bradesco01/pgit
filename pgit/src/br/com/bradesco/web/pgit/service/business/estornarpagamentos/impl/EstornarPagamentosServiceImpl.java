/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/implementation.ftl,v $
 * $Id: implementation.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: implementation.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */
 
package br.com.bradesco.web.pgit.service.business.estornarpagamentos.impl;


import java.util.ArrayList;
import java.util.List;

import br.com.bradesco.web.pgit.service.business.estornarpagamentos.IEstornarPagamentosService;
import br.com.bradesco.web.pgit.service.business.estornarpagamentos.IEstornarPagamentosServiceConstants;
import br.com.bradesco.web.pgit.service.business.estornarpagamentos.bean.ConsultarPagtosEstornoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.estornarpagamentos.bean.ConsultarPagtosEstornoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.estornarpagamentos.bean.EstornarPagamentosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.estornarpagamentos.bean.EstornarPagamentosSaidaDTO;
import br.com.bradesco.web.pgit.service.data.pdc.FactoryAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarpagtosestorno.request.ConsultarPagtosEstornoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarpagtosestorno.response.ConsultarPagtosEstornoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.estornarpagamentos.request.EstornarPagamentosRequest;
import br.com.bradesco.web.pgit.service.data.pdc.estornarpagamentos.response.EstornarPagamentosResponse;
import br.com.bradesco.web.pgit.utils.CpfCnpjUtils;
import br.com.bradesco.web.pgit.utils.DateUtils;
import br.com.bradesco.web.pgit.utils.NumberUtils;
import br.com.bradesco.web.pgit.utils.PgitUtil;
import br.com.bradesco.web.pgit.utils.SiteUtil;
import br.com.bradesco.web.pgit.view.converters.FormatarData;


/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Implementa��o do adaptador: EstornarPagamentos
 * </p>
 * 
 * @comment C�DIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public class EstornarPagamentosServiceImpl implements IEstornarPagamentosService {
	
	
	/** Atributo factoryAdapter. */
	private FactoryAdapter factoryAdapter;
	
    /**
     * Get: factoryAdapter.
     *
     * @return factoryAdapter
     */
    public FactoryAdapter getFactoryAdapter() {
		return factoryAdapter;
	}

	/**
	 * Set: factoryAdapter.
	 *
	 * @param factoryAdapter the factory adapter
	 */
	public void setFactoryAdapter(FactoryAdapter factoryAdapter) {
		this.factoryAdapter = factoryAdapter;
	}

	
	
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.estornarpagamentos.IEstornarPagamentosService#estornarPagamentos(br.com.bradesco.web.pgit.service.business.estornarpagamentos.bean.EstornarPagamentosEntradaDTO)
	 */
	public EstornarPagamentosSaidaDTO estornarPagamentos(EstornarPagamentosEntradaDTO entrada){
		EstornarPagamentosSaidaDTO saida = new EstornarPagamentosSaidaDTO();
		EstornarPagamentosRequest request = new EstornarPagamentosRequest();
		EstornarPagamentosResponse response = new EstornarPagamentosResponse();

		request.setCdPrimeiraSolicitacao(PgitUtil.verificaStringNula(entrada.getCdPrimeiraSolicitacao()));
		request.setCdSolicitacaoPagamento(PgitUtil.verificaIntegerNulo(entrada.getCdSolicitacaoPagamento()));
		request.setNrSolicitacaoPagamento(PgitUtil.verificaIntegerNulo(entrada.getNrSolicitacaoPagamento()));
		request.setVlrPrevistoEstornoPagamento(PgitUtil.verificaBigDecimalNulo(entrada.getVlrPrevistoEstornoPagamento()));
		request.setDsObservacaoAutorizacaoEstorno(PgitUtil.verificaStringNula(entrada.getDsObservacaoAutorizacaoEstorno()));
        request.setCdFormaEstrnPagamento(PgitUtil.verificaIntegerNulo(entrada.getCdFormaEstrnPagamento()));
		request.setCdUsuarioAutorizacaoEstorno(PgitUtil.verificaStringNula(entrada.getCdUsuarioAutorizacaoEstorno()));
		request.setNrPagamento(PgitUtil.verificaIntegerNulo(entrada.getNrPagamento()));

		ArrayList<br.com.bradesco.web.pgit.service.data.pdc.estornarpagamentos.request.Ocorrencias> listaOcorrencias = new ArrayList<br.com.bradesco.web.pgit.service.data.pdc.estornarpagamentos.request.Ocorrencias>();
		for (int i=0; i< entrada.getListaOcorrencias().size(); i++){
			br.com.bradesco.web.pgit.service.data.pdc.estornarpagamentos.request.Ocorrencias ocorrencias = new br.com.bradesco.web.pgit.service.data.pdc.estornarpagamentos.request.Ocorrencias();
			ocorrencias.setCdPessoaJuridicaContrato(PgitUtil.verificaLongNulo(entrada.getListaOcorrencias().get(i).getCdPessoaJuridicaContrato()));
			ocorrencias.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(entrada.getListaOcorrencias().get(i).getCdTipoContratoNegocio()));
			ocorrencias.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(entrada.getListaOcorrencias().get(i).getNrSequenciaContratoNegocio()));
			ocorrencias.setCdControlePagamento(PgitUtil.verificaStringNula(entrada.getListaOcorrencias().get(i).getCdControlePagamento()));
			ocorrencias.setDtEfetivacao(PgitUtil.verificaStringNula(entrada.getListaOcorrencias().get(i).getDtEfetivacao()));
			ocorrencias.setCdModalidadePagamento(PgitUtil.verificaIntegerNulo(entrada.getListaOcorrencias().get(i).getCdModalidadePagamento()));
			ocorrencias.setCdValorEstorno(PgitUtil.verificaBigDecimalNulo(entrada.getListaOcorrencias().get(i).getCdValorEstorno()));
			listaOcorrencias.add(ocorrencias);
		}
		request.setOcorrencias((br.com.bradesco.web.pgit.service.data.pdc.estornarpagamentos.request.Ocorrencias[]) listaOcorrencias.toArray(new br.com.bradesco.web.pgit.service.data.pdc.estornarpagamentos.request.Ocorrencias[0]));

		response = getFactoryAdapter().getEstornarPagamentosPDCAdapter().invokeProcess(request);

		saida.setCodMensagem(response.getCodMensagem());
		saida.setMensagem(response.getMensagem());
		saida.setCdSolicitacaoPagamento(response.getCdSolicitacaoPagamento());
		saida.setNrSolicitacaoPagamento(response.getNrSolicitacaoPagamento());
		saida.setDsValorEstornoTotal(response.getDsValorEstornoTotal());

		return saida;
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.estornarpagamentos.IEstornarPagamentosService#consultarPagtosEstorno(br.com.bradesco.web.pgit.service.business.estornarpagamentos.bean.ConsultarPagtosEstornoEntradaDTO)
	 */
	public List<ConsultarPagtosEstornoSaidaDTO> consultarPagtosEstorno(ConsultarPagtosEstornoEntradaDTO consultarPagtosEstornoEntradaDTO) {
		List<ConsultarPagtosEstornoSaidaDTO> listaSaida = new ArrayList<ConsultarPagtosEstornoSaidaDTO>();
		ConsultarPagtosEstornoRequest request = new ConsultarPagtosEstornoRequest();
		ConsultarPagtosEstornoResponse response = new ConsultarPagtosEstornoResponse();

		request.setCdTipoPesquisa(PgitUtil.verificaIntegerNulo(consultarPagtosEstornoEntradaDTO.getCdTipoPesquisa()));
		request.setNumeroOcorrencias(PgitUtil.verificaIntegerNulo(consultarPagtosEstornoEntradaDTO.getNumeroOcorrencias()));
		request.setCdPessoaJuridicaContrato(PgitUtil.verificaLongNulo(consultarPagtosEstornoEntradaDTO.getCdPessoaJuridicaContrato()));
		request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(consultarPagtosEstornoEntradaDTO.getCdTipoContratoNegocio()));
		request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(consultarPagtosEstornoEntradaDTO.getNrSequenciaContratoNegocio()));
		request.setCdBanco(PgitUtil.verificaIntegerNulo(consultarPagtosEstornoEntradaDTO.getCdBanco()));
		request.setCdAgencia(PgitUtil.verificaIntegerNulo(consultarPagtosEstornoEntradaDTO.getCdAgencia()));
		request.setDgAgenciaBancaria(PgitUtil.verificaIntegerNulo(consultarPagtosEstornoEntradaDTO.getDgAgenciaBancaria()));
		request.setCdConta(PgitUtil.verificaLongNulo(consultarPagtosEstornoEntradaDTO.getCdConta()));
		request.setCdDigitoConta(PgitUtil.DIGITO_CONTA);
		request.setNrArquivoRemssaPagamento(PgitUtil.verificaLongNulo(consultarPagtosEstornoEntradaDTO.getNrArquivoRemssaPagamento()));
		request.setCdParticipante(PgitUtil.verificaLongNulo(consultarPagtosEstornoEntradaDTO.getCdParticipante()));
		request.setCdprodutoServicoOperacao(PgitUtil.verificaIntegerNulo(consultarPagtosEstornoEntradaDTO.getCdprodutoServicoOperacao()));
		request.setCdProdutoServicoRelacionado(PgitUtil.verificaIntegerNulo(consultarPagtosEstornoEntradaDTO.getCdProdutoServicoRelacionado()));
		request.setCdControlePagamentoDe(PgitUtil.verificaStringNula(consultarPagtosEstornoEntradaDTO.getCdControlePagamentoDe()));
		request.setVlPagamentoDe(PgitUtil.verificaBigDecimalNulo(consultarPagtosEstornoEntradaDTO.getVlPagamentoDe()));
		request.setVlPagamentoAte(PgitUtil.verificaBigDecimalNulo(consultarPagtosEstornoEntradaDTO.getVlPagamentoAte()));
		request.setCdFavorecidoClientePagador(PgitUtil.verificaLongNulo(consultarPagtosEstornoEntradaDTO.getCdFavorecidoClientePagador()));
		request.setCdInscricaoFavorecido(PgitUtil.verificaLongNulo(consultarPagtosEstornoEntradaDTO.getCdInscricaoFavorecido()));
		request.setCdIdentificacaoInscricaoFavorecido(PgitUtil.verificaIntegerNulo(consultarPagtosEstornoEntradaDTO.getCdIdentificacaoInscricaoFavorecido()));
		request.setDtEstornoInicio(FormatarData.formataDiaMesAnoToPdc(consultarPagtosEstornoEntradaDTO.getDataInicialPagamentoFiltro()));
		request.setDtEstornoFim(FormatarData.formataDiaMesAnoToPdc(consultarPagtosEstornoEntradaDTO.getDataFinalPagamentoFiltro()));
		request.setNumeroOcorrencias(IEstornarPagamentosServiceConstants.NUMERO_OCORRENCIAS_CONSULTAR);

		response = getFactoryAdapter().getConsultarPagtosEstornoPDCAdapter().invokeProcess(request);

		for(int i=0;i<response.getOcorrenciasCount();i++){
			ConsultarPagtosEstornoSaidaDTO saidaDTO = new ConsultarPagtosEstornoSaidaDTO();

			br.com.bradesco.web.pgit.service.data.pdc.consultarpagtosestorno.response.Ocorrencias occur = response.getOcorrencias(i);

			saidaDTO.setCodMensagem(response.getCodMensagem());
			saidaDTO.setMensagem(response.getMensagem());
			saidaDTO.setCorpoCfpCnpj(occur.getCorpoCfpCnpj());
			saidaDTO.setFilialCfpCnpj(occur.getFilialCfpCnpj());
			saidaDTO.setControleCfpCnpj(occur.getControleCfpCnpj());
			saidaDTO.setCdPessoaJuridicaContrato(occur.getCdPessoaJuridicaContrato());
			saidaDTO.setNmRazaoSocial(occur.getNmRazaoSocial());
			saidaDTO.setCdEmpresa(occur.getCdEmpresa());
			saidaDTO.setDsEmpresa(occur.getDsEmpresa());
			saidaDTO.setCdTipoContratoNegocio(occur.getCdTipoContratoNegocio());
			saidaDTO.setNrContrato(occur.getNrContrato());
			saidaDTO.setCdProdutoServicoOperacao(occur.getCdProdutoServicoOperacao());
			saidaDTO.setCdResumoProdutoServico(occur.getCdResumoProdutoServico());
			saidaDTO.setCdProdutoServicoRelacionado(occur.getCdProdutoServicoRelacionado());
			saidaDTO.setDsOperacaoProdutoServico(occur.getDsOperacaoProdutoServico());
			saidaDTO.setCdControlePagamento(occur.getCdControlePagamento());
			saidaDTO.setDtCraditoPagamento(occur.getDtCraditoPagamento());
			saidaDTO.setDtCraditoPagamentoFormatada(DateUtils.formartarDataPDCparaPadraPtBr(saidaDTO.getDtCraditoPagamento(), "dd.MM.yyyy", "dd/MM/yyyy"));
			saidaDTO.setVlrEfetivacaoPagamentoCliente(occur.getVlrEfetivacaoPagamentoCliente());
			saidaDTO.setCdInscricaoFavorecido(occur.getCdInscricaoFavorecido());
			saidaDTO.setDsAbrevFavorecido(occur.getDsAbrevFavorecido());
			saidaDTO.setCdBancoDebito(occur.getCdBancoDebito());
			saidaDTO.setCdAgenciaDebito(occur.getCdAgenciaDebito());
			saidaDTO.setCdDigitoAgenciaDebito(occur.getCdDigitoAgenciaDebito());
			saidaDTO.setCdContaDebito(occur.getCdContaDebito());
			saidaDTO.setCdDigitoContaDebito(occur.getCdDigitoContaDebito());
			saidaDTO.setCdBancoCredito(occur.getCdBancoCredito());
			saidaDTO.setCdAgenciaCredito(occur.getCdAgenciaCredito());
			saidaDTO.setCdDigitoAgenciaCredito(occur.getCdDigitoAgenciaCredito());
			saidaDTO.setCdContaCredito(occur.getCdContaCredito());
			saidaDTO.setCdDigitoContaCredito(occur.getCdDigitoContaCredito());
			saidaDTO.setCdSituacaoOperacaoPagamento(occur.getCdSituacaoOperacaoPagamento());
			saidaDTO.setDsSituacaoOperacaoPagamento(occur.getDsSituacaoOperacaoPagamento());
			saidaDTO.setCdMotivoSituacaoPagamento(occur.getCdMotivoSituacaoPagamento());
			saidaDTO.setDsMotivoSituacaoPagamento(occur.getDsMotivoSituacaoPagamento());
			saidaDTO.setCdTipoCanal(occur.getCdTipoCanal());
			saidaDTO.setDsTipoTela(occur.getDsTipoTela());
			saidaDTO.setDtEfetivacao(occur.getDtEfetivacao());
			saidaDTO.setCdCpfCnpjRecebedor(occur.getCdCpfCnpjRecebedor());
			saidaDTO.setCdFilialRecebedor(occur.getCdFilialRecebedor());
			saidaDTO.setCdDigitoCpfRecebedor(occur.getCdDigitoCpfRecebedor());
			saidaDTO.setDsCodigoTabelaConsulta(occur.getDsCodigoTabelaConsulta());
			saidaDTO.setNrArquivoRemessa(occur.getNrArquivoRemessa());

			//Formatar Valor e Favorecido/Benefici�rio
			saidaDTO.setVlEfetivoPagamentoFormatado(NumberUtils.format(occur.getVlrEfetivacaoPagamentoCliente(),"#,##0.00"));
			saidaDTO.setFavorecidoBeneficiarioFormatado(PgitUtil.concatenarCampos(saidaDTO.getCdInscricaoFavorecido(), saidaDTO.getDsAbrevFavorecido(), "-"));

			 //Formatar Conta Cr�dito/D�bito
			saidaDTO.setBancoAgenciaContaCreditoFormatado(PgitUtil.formatBancoAgenciaConta(saidaDTO.getCdBancoCredito(), saidaDTO.getCdAgenciaCredito(), saidaDTO.getCdDigitoAgenciaCredito(), saidaDTO.getCdContaCredito(), saidaDTO.getCdDigitoContaCredito(), true));
			saidaDTO.setBancoAgenciaContaDebitoFormatado(PgitUtil.formatBancoAgenciaConta(saidaDTO.getCdBancoDebito(), saidaDTO.getCdAgenciaDebito(), saidaDTO.getCdDigitoAgenciaDebito(), saidaDTO.getCdContaDebito(), saidaDTO.getCdDigitoContaDebito(), true));

			//Formatar CPF/CNPJ
			saidaDTO.setCdCpfCnpjFormatado(CpfCnpjUtils.formatCpfCnpjCompleto(occur.getCorpoCfpCnpj(), occur.getFilialCfpCnpj(), occur.getControleCfpCnpj()));

			//Formatar Banco/Ag�ncia/Conta
			saidaDTO.setCdBancoCreditoFormatado(occur.getCdBancoCredito() == 0 ? "" : (SiteUtil.formatNumber(String.valueOf(occur.getCdBancoCredito()), 3)));
    	    saidaDTO.setCdAgenciaCreditoFormatado(occur.getCdAgenciaCredito() == 0 ? "" : (SiteUtil.formatNumber(String.valueOf(occur.getCdAgenciaCredito()), 5)));
    	    saidaDTO.setCdDigitoAgenciaCreditoFormatado(SiteUtil.formatNumber(String.valueOf(occur.getCdDigitoAgenciaCredito()), 2));
    	    saidaDTO.setCdContaCreditoFormatado(SiteUtil.formatNumber(String.valueOf(occur.getCdContaCredito()), 13));
    	    saidaDTO.setCdDigitoContaCreditoFormatado(occur.getCdDigitoContaCredito());
    	    saidaDTO.setAgenciaCreditoFormatada(occur.getCdAgenciaCredito() == 0 ?  "" : (PgitUtil.formatAgencia(occur.getCdAgenciaCredito(), occur.getCdDigitoAgenciaCredito(), false)));
    	    saidaDTO.setContaCreditoFormatada(PgitUtil.formatConta(occur.getCdContaCredito(), occur.getCdDigitoContaCredito(), false));
    	    saidaDTO.setCdFormaLiquidacao(occur.getCdFormaLiquidacao());
    	    
			listaSaida.add(saidaDTO);
		}

		return listaSaida;
	}
}