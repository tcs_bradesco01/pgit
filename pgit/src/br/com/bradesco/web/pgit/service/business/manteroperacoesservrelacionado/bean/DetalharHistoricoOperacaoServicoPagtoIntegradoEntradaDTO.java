/*
 * Nome: br.com.bradesco.web.pgit.service.business.manteroperacoesservrelacionado.bean

 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 10/03/2017
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.manteroperacoesservrelacionado.bean;

/**
 * Nome: DetalharHistoricoOperacaoServicoPagtoIntegradoEntradaDTO
 * <p>
 * Prop�sito:
 * </p>
 * 	
 * @author : todo!
 * 
 * @version :
 */
public class DetalharHistoricoOperacaoServicoPagtoIntegradoEntradaDTO {

    /**
     * Atributo Integer
     */
    private Integer cdProdutoServicoOperacional = null;

    /**
     * Atributo Integer
     */
    private Integer cdProdutoOperacionalRelacionado = null;

    /**
     * Atributo Integer
     */
    private Integer cdOperacaoProdutoServico = null;

    /**
     * Atributo String
     */
    private String hrInclusaoRegistroHistorico = null;

    /**
     * Nome: getCdProdutoServicoOperacional
     * 
     * @return cdProdutoServicoOperacional
     */
    public Integer getCdProdutoServicoOperacional() {
        return cdProdutoServicoOperacional;
    }

    /**
     * Nome: setCdProdutoServicoOperacional
     * 
     * @param cdProdutoServicoOperacional
     */
    public void setCdProdutoServicoOperacional(Integer cdProdutoServicoOperacional) {
        this.cdProdutoServicoOperacional = cdProdutoServicoOperacional;
    }

    /**
     * Nome: getCdProdutoOperacionalRelacionado
     * 
     * @return cdProdutoOperacionalRelacionado
     */
    public Integer getCdProdutoOperacionalRelacionado() {
        return cdProdutoOperacionalRelacionado;
    }

    /**
     * Nome: setCdProdutoOperacionalRelacionado
     * 
     * @param cdProdutoOperacionalRelacionado
     */
    public void setCdProdutoOperacionalRelacionado(Integer cdProdutoOperacionalRelacionado) {
        this.cdProdutoOperacionalRelacionado = cdProdutoOperacionalRelacionado;
    }

    /**
     * Nome: getCdOperacaoProdutoServico
     * 
     * @return cdOperacaoProdutoServico
     */
    public Integer getCdOperacaoProdutoServico() {
        return cdOperacaoProdutoServico;
    }

    /**
     * Nome: setCdOperacaoProdutoServico
     * 
     * @param cdOperacaoProdutoServico
     */
    public void setCdOperacaoProdutoServico(Integer cdOperacaoProdutoServico) {
        this.cdOperacaoProdutoServico = cdOperacaoProdutoServico;
    }

    /**
     * Nome: getHrInclusaoRegistroHistorico
     * 
     * @return hrInclusaoRegistroHistorico
     */
    public String getHrInclusaoRegistroHistorico() {
        return hrInclusaoRegistroHistorico;
    }

    /**
     * Nome: setHrInclusaoRegistroHistorico
     * 
     * @param hrInclusaoRegistroHistorico
     */
    public void setHrInclusaoRegistroHistorico(String hrInclusaoRegistroHistorico) {
        this.hrInclusaoRegistroHistorico = hrInclusaoRegistroHistorico;
    }
}
