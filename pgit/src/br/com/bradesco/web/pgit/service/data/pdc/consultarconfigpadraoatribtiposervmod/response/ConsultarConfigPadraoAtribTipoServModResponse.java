/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultarconfigpadraoatribtiposervmod.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.math.BigDecimal;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ConsultarConfigPadraoAtribTipoServModResponse.
 * 
 * @version $Revision$ $Date$
 */
public class ConsultarConfigPadraoAtribTipoServModResponse implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _codMensagem
     */
    private java.lang.String _codMensagem;

    /**
     * Field _mensagem
     */
    private java.lang.String _mensagem;

    /**
     * Field _cdAcaoNaoVida
     */
    private int _cdAcaoNaoVida = 0;

    /**
     * keeps track of state for field: _cdAcaoNaoVida
     */
    private boolean _has_cdAcaoNaoVida;

    /**
     * Field _cdAcertoDadoRecadastro
     */
    private int _cdAcertoDadoRecadastro = 0;

    /**
     * keeps track of state for field: _cdAcertoDadoRecadastro
     */
    private boolean _has_cdAcertoDadoRecadastro;

    /**
     * Field _cdAgendaDebitoVeiculo
     */
    private int _cdAgendaDebitoVeiculo = 0;

    /**
     * keeps track of state for field: _cdAgendaDebitoVeiculo
     */
    private boolean _has_cdAgendaDebitoVeiculo;

    /**
     * Field _cdAgendaPagamentoVencido
     */
    private int _cdAgendaPagamentoVencido = 0;

    /**
     * keeps track of state for field: _cdAgendaPagamentoVencido
     */
    private boolean _has_cdAgendaPagamentoVencido;

    /**
     * Field _cdAGendaValorMenor
     */
    private int _cdAGendaValorMenor = 0;

    /**
     * keeps track of state for field: _cdAGendaValorMenor
     */
    private boolean _has_cdAGendaValorMenor;

    /**
     * Field _cdAgrupamentoAviso
     */
    private int _cdAgrupamentoAviso = 0;

    /**
     * keeps track of state for field: _cdAgrupamentoAviso
     */
    private boolean _has_cdAgrupamentoAviso;

    /**
     * Field _cdAgrupamentoComprovado
     */
    private int _cdAgrupamentoComprovado = 0;

    /**
     * keeps track of state for field: _cdAgrupamentoComprovado
     */
    private boolean _has_cdAgrupamentoComprovado;

    /**
     * Field _cdAgptoFormularioRecadastro
     */
    private int _cdAgptoFormularioRecadastro = 0;

    /**
     * keeps track of state for field: _cdAgptoFormularioRecadastro
     */
    private boolean _has_cdAgptoFormularioRecadastro;

    /**
     * Field _cdAntecRecadastroBeneficiario
     */
    private int _cdAntecRecadastroBeneficiario = 0;

    /**
     * keeps track of state for field: _cdAntecRecadastroBeneficiari
     */
    private boolean _has_cdAntecRecadastroBeneficiario;

    /**
     * Field _cdAreaReservada
     */
    private int _cdAreaReservada = 0;

    /**
     * keeps track of state for field: _cdAreaReservada
     */
    private boolean _has_cdAreaReservada;

    /**
     * Field _cdBasaeRecadastroBeneficiario
     */
    private int _cdBasaeRecadastroBeneficiario = 0;

    /**
     * keeps track of state for field: _cdBasaeRecadastroBeneficiari
     */
    private boolean _has_cdBasaeRecadastroBeneficiario;

    /**
     * Field _cdBloqueioEmissaoPplta
     */
    private int _cdBloqueioEmissaoPplta = 0;

    /**
     * keeps track of state for field: _cdBloqueioEmissaoPplta
     */
    private boolean _has_cdBloqueioEmissaoPplta;

    /**
     * Field _cdCaptuTituloRegistro
     */
    private int _cdCaptuTituloRegistro = 0;

    /**
     * keeps track of state for field: _cdCaptuTituloRegistro
     */
    private boolean _has_cdCaptuTituloRegistro;

    /**
     * Field _cdCobrancaTarifa
     */
    private int _cdCobrancaTarifa = 0;

    /**
     * keeps track of state for field: _cdCobrancaTarifa
     */
    private boolean _has_cdCobrancaTarifa;

    /**
     * Field _cdConsDebitoVeiculo
     */
    private int _cdConsDebitoVeiculo = 0;

    /**
     * keeps track of state for field: _cdConsDebitoVeiculo
     */
    private boolean _has_cdConsDebitoVeiculo;

    /**
     * Field _cdConsEndereco
     */
    private int _cdConsEndereco = 0;

    /**
     * keeps track of state for field: _cdConsEndereco
     */
    private boolean _has_cdConsEndereco;

    /**
     * Field _cdConsSaldoPagamento
     */
    private int _cdConsSaldoPagamento = 0;

    /**
     * keeps track of state for field: _cdConsSaldoPagamento
     */
    private boolean _has_cdConsSaldoPagamento;

    /**
     * Field _cdContgConsSaldo
     */
    private int _cdContgConsSaldo = 0;

    /**
     * keeps track of state for field: _cdContgConsSaldo
     */
    private boolean _has_cdContgConsSaldo;

    /**
     * Field _cdCreditoNaoUtilizado
     */
    private int _cdCreditoNaoUtilizado = 0;

    /**
     * keeps track of state for field: _cdCreditoNaoUtilizado
     */
    private boolean _has_cdCreditoNaoUtilizado;

    /**
     * Field _cdCriterioEnquaBeneficiario
     */
    private int _cdCriterioEnquaBeneficiario = 0;

    /**
     * keeps track of state for field: _cdCriterioEnquaBeneficiario
     */
    private boolean _has_cdCriterioEnquaBeneficiario;

    /**
     * Field _cdCriterioEnquaRcadt
     */
    private int _cdCriterioEnquaRcadt = 0;

    /**
     * keeps track of state for field: _cdCriterioEnquaRcadt
     */
    private boolean _has_cdCriterioEnquaRcadt;

    /**
     * Field _cdCriterioRstrbTitulo
     */
    private int _cdCriterioRstrbTitulo = 0;

    /**
     * keeps track of state for field: _cdCriterioRstrbTitulo
     */
    private boolean _has_cdCriterioRstrbTitulo;

    /**
     * Field _cdCtciaEspecieBeneficiario
     */
    private int _cdCtciaEspecieBeneficiario = 0;

    /**
     * keeps track of state for field: _cdCtciaEspecieBeneficiario
     */
    private boolean _has_cdCtciaEspecieBeneficiario;

    /**
     * Field _cdCtciaIdBeneficiario
     */
    private int _cdCtciaIdBeneficiario = 0;

    /**
     * keeps track of state for field: _cdCtciaIdBeneficiario
     */
    private boolean _has_cdCtciaIdBeneficiario;

    /**
     * Field _cdCtciaInscricaoFavorecido
     */
    private int _cdCtciaInscricaoFavorecido = 0;

    /**
     * keeps track of state for field: _cdCtciaInscricaoFavorecido
     */
    private boolean _has_cdCtciaInscricaoFavorecido;

    /**
     * Field _cdCtciaProprietarioVeiculo
     */
    private int _cdCtciaProprietarioVeiculo = 0;

    /**
     * keeps track of state for field: _cdCtciaProprietarioVeiculo
     */
    private boolean _has_cdCtciaProprietarioVeiculo;

    /**
     * Field _cdDispzContaCredito
     */
    private int _cdDispzContaCredito = 0;

    /**
     * keeps track of state for field: _cdDispzContaCredito
     */
    private boolean _has_cdDispzContaCredito;

    /**
     * Field _cdDispzDiversosCrrtt
     */
    private int _cdDispzDiversosCrrtt = 0;

    /**
     * keeps track of state for field: _cdDispzDiversosCrrtt
     */
    private boolean _has_cdDispzDiversosCrrtt;

    /**
     * Field _cdDispzDiversaoNao
     */
    private int _cdDispzDiversaoNao = 0;

    /**
     * keeps track of state for field: _cdDispzDiversaoNao
     */
    private boolean _has_cdDispzDiversaoNao;

    /**
     * Field _cdDispzSalarialCrrtt
     */
    private int _cdDispzSalarialCrrtt = 0;

    /**
     * keeps track of state for field: _cdDispzSalarialCrrtt
     */
    private boolean _has_cdDispzSalarialCrrtt;

    /**
     * Field _cdDispzSalarialNao
     */
    private int _cdDispzSalarialNao = 0;

    /**
     * keeps track of state for field: _cdDispzSalarialNao
     */
    private boolean _has_cdDispzSalarialNao;

    /**
     * Field _cdDestinoAviso
     */
    private int _cdDestinoAviso = 0;

    /**
     * keeps track of state for field: _cdDestinoAviso
     */
    private boolean _has_cdDestinoAviso;

    /**
     * Field _cdDestinoComprovante
     */
    private int _cdDestinoComprovante = 0;

    /**
     * keeps track of state for field: _cdDestinoComprovante
     */
    private boolean _has_cdDestinoComprovante;

    /**
     * Field _cdDestinoFormularioRcadt
     */
    private int _cdDestinoFormularioRcadt = 0;

    /**
     * keeps track of state for field: _cdDestinoFormularioRcadt
     */
    private boolean _has_cdDestinoFormularioRcadt;

    /**
     * Field _cdEnvelopeAberto
     */
    private int _cdEnvelopeAberto = 0;

    /**
     * keeps track of state for field: _cdEnvelopeAberto
     */
    private boolean _has_cdEnvelopeAberto;

    /**
     * Field _cdFavorecidoConsPgto
     */
    private int _cdFavorecidoConsPgto = 0;

    /**
     * keeps track of state for field: _cdFavorecidoConsPgto
     */
    private boolean _has_cdFavorecidoConsPgto;

    /**
     * Field _cdFormaAutrzPgto
     */
    private int _cdFormaAutrzPgto = 0;

    /**
     * keeps track of state for field: _cdFormaAutrzPgto
     */
    private boolean _has_cdFormaAutrzPgto;

    /**
     * Field _cdFormaEnvioPagamento
     */
    private int _cdFormaEnvioPagamento = 0;

    /**
     * keeps track of state for field: _cdFormaEnvioPagamento
     */
    private boolean _has_cdFormaEnvioPagamento;

    /**
     * Field _cdFormaExpirCredito
     */
    private int _cdFormaExpirCredito = 0;

    /**
     * keeps track of state for field: _cdFormaExpirCredito
     */
    private boolean _has_cdFormaExpirCredito;

    /**
     * Field _cdFormaManutencao
     */
    private int _cdFormaManutencao = 0;

    /**
     * keeps track of state for field: _cdFormaManutencao
     */
    private boolean _has_cdFormaManutencao;

    /**
     * Field _cdFrasePreCadastro
     */
    private int _cdFrasePreCadastro = 0;

    /**
     * keeps track of state for field: _cdFrasePreCadastro
     */
    private boolean _has_cdFrasePreCadastro;

    /**
     * Field _cdIndicadorAgendaTitulo
     */
    private int _cdIndicadorAgendaTitulo = 0;

    /**
     * keeps track of state for field: _cdIndicadorAgendaTitulo
     */
    private boolean _has_cdIndicadorAgendaTitulo;

    /**
     * Field _cdIndicadorAutrzCliente
     */
    private int _cdIndicadorAutrzCliente = 0;

    /**
     * keeps track of state for field: _cdIndicadorAutrzCliente
     */
    private boolean _has_cdIndicadorAutrzCliente;

    /**
     * Field _cdIndicadorAutrzCompl
     */
    private int _cdIndicadorAutrzCompl = 0;

    /**
     * keeps track of state for field: _cdIndicadorAutrzCompl
     */
    private boolean _has_cdIndicadorAutrzCompl;

    /**
     * Field _cdIndicadorCadOrg
     */
    private int _cdIndicadorCadOrg = 0;

    /**
     * keeps track of state for field: _cdIndicadorCadOrg
     */
    private boolean _has_cdIndicadorCadOrg;

    /**
     * Field _cdIndicadorCadProcd
     */
    private int _cdIndicadorCadProcd = 0;

    /**
     * keeps track of state for field: _cdIndicadorCadProcd
     */
    private boolean _has_cdIndicadorCadProcd;

    /**
     * Field _cdIndicadorCartaoSalarial
     */
    private int _cdIndicadorCartaoSalarial = 0;

    /**
     * keeps track of state for field: _cdIndicadorCartaoSalarial
     */
    private boolean _has_cdIndicadorCartaoSalarial;

    /**
     * Field _cdIndicadorEconomicoReajuste
     */
    private int _cdIndicadorEconomicoReajuste = 0;

    /**
     * keeps track of state for field: _cdIndicadorEconomicoReajuste
     */
    private boolean _has_cdIndicadorEconomicoReajuste;

    /**
     * Field _cdIndicadorExpirCredito
     */
    private int _cdIndicadorExpirCredito = 0;

    /**
     * keeps track of state for field: _cdIndicadorExpirCredito
     */
    private boolean _has_cdIndicadorExpirCredito;

    /**
     * Field _cdIndicadorLctoPgmd
     */
    private int _cdIndicadorLctoPgmd = 0;

    /**
     * keeps track of state for field: _cdIndicadorLctoPgmd
     */
    private boolean _has_cdIndicadorLctoPgmd;

    /**
     * Field _cdIndicadorMensagemPerso
     */
    private int _cdIndicadorMensagemPerso = 0;

    /**
     * keeps track of state for field: _cdIndicadorMensagemPerso
     */
    private boolean _has_cdIndicadorMensagemPerso;

    /**
     * Field _cdLancamentoFuturoCredito
     */
    private int _cdLancamentoFuturoCredito = 0;

    /**
     * keeps track of state for field: _cdLancamentoFuturoCredito
     */
    private boolean _has_cdLancamentoFuturoCredito;

    /**
     * Field _cdLancamentoFuturoDebito
     */
    private int _cdLancamentoFuturoDebito = 0;

    /**
     * keeps track of state for field: _cdLancamentoFuturoDebito
     */
    private boolean _has_cdLancamentoFuturoDebito;

    /**
     * Field _cdLiberacaoLoteProcs
     */
    private int _cdLiberacaoLoteProcs = 0;

    /**
     * keeps track of state for field: _cdLiberacaoLoteProcs
     */
    private boolean _has_cdLiberacaoLoteProcs;

    /**
     * Field _cdManutencaoBaseRcadt
     */
    private int _cdManutencaoBaseRcadt = 0;

    /**
     * keeps track of state for field: _cdManutencaoBaseRcadt
     */
    private boolean _has_cdManutencaoBaseRcadt;

    /**
     * Field _cdMidiaDisponivel
     */
    private int _cdMidiaDisponivel = 0;

    /**
     * keeps track of state for field: _cdMidiaDisponivel
     */
    private boolean _has_cdMidiaDisponivel;

    /**
     * Field _cdMidiaMensagemRcadt
     */
    private int _cdMidiaMensagemRcadt = 0;

    /**
     * keeps track of state for field: _cdMidiaMensagemRcadt
     */
    private boolean _has_cdMidiaMensagemRcadt;

    /**
     * Field _cdMomenAvisoRcadt
     */
    private int _cdMomenAvisoRcadt = 0;

    /**
     * keeps track of state for field: _cdMomenAvisoRcadt
     */
    private boolean _has_cdMomenAvisoRcadt;

    /**
     * Field _cdMomenCreditoEfetivo
     */
    private int _cdMomenCreditoEfetivo = 0;

    /**
     * keeps track of state for field: _cdMomenCreditoEfetivo
     */
    private boolean _has_cdMomenCreditoEfetivo;

    /**
     * Field _cdMomenDebitoPagamento
     */
    private int _cdMomenDebitoPagamento = 0;

    /**
     * keeps track of state for field: _cdMomenDebitoPagamento
     */
    private boolean _has_cdMomenDebitoPagamento;

    /**
     * Field _cdMomenFormulaRcadt
     */
    private int _cdMomenFormulaRcadt = 0;

    /**
     * keeps track of state for field: _cdMomenFormulaRcadt
     */
    private boolean _has_cdMomenFormulaRcadt;

    /**
     * Field _cdMomenProcmPagamento
     */
    private int _cdMomenProcmPagamento = 0;

    /**
     * keeps track of state for field: _cdMomenProcmPagamento
     */
    private boolean _has_cdMomenProcmPagamento;

    /**
     * Field _cdMensagemRcadtMidia
     */
    private int _cdMensagemRcadtMidia = 0;

    /**
     * keeps track of state for field: _cdMensagemRcadtMidia
     */
    private boolean _has_cdMensagemRcadtMidia;

    /**
     * Field _cdPermissaoDebitoOnline
     */
    private int _cdPermissaoDebitoOnline = 0;

    /**
     * keeps track of state for field: _cdPermissaoDebitoOnline
     */
    private boolean _has_cdPermissaoDebitoOnline;

    /**
     * Field _cdNaturezaOperacaoPagamento
     */
    private int _cdNaturezaOperacaoPagamento = 0;

    /**
     * keeps track of state for field: _cdNaturezaOperacaoPagamento
     */
    private boolean _has_cdNaturezaOperacaoPagamento;

    /**
     * Field _cdPerdcAviso
     */
    private int _cdPerdcAviso = 0;

    /**
     * keeps track of state for field: _cdPerdcAviso
     */
    private boolean _has_cdPerdcAviso;

    /**
     * Field _cdPerdcCobrancaTarifa
     */
    private int _cdPerdcCobrancaTarifa = 0;

    /**
     * keeps track of state for field: _cdPerdcCobrancaTarifa
     */
    private boolean _has_cdPerdcCobrancaTarifa;

    /**
     * Field _cdPerdcComprovante
     */
    private int _cdPerdcComprovante = 0;

    /**
     * keeps track of state for field: _cdPerdcComprovante
     */
    private boolean _has_cdPerdcComprovante;

    /**
     * Field _cdPerdcConsVeiculo
     */
    private int _cdPerdcConsVeiculo = 0;

    /**
     * keeps track of state for field: _cdPerdcConsVeiculo
     */
    private boolean _has_cdPerdcConsVeiculo;

    /**
     * Field _cdPerdcEnvioRemessa
     */
    private int _cdPerdcEnvioRemessa = 0;

    /**
     * keeps track of state for field: _cdPerdcEnvioRemessa
     */
    private boolean _has_cdPerdcEnvioRemessa;

    /**
     * Field _cdPerdcManutencaoProcd
     */
    private int _cdPerdcManutencaoProcd = 0;

    /**
     * keeps track of state for field: _cdPerdcManutencaoProcd
     */
    private boolean _has_cdPerdcManutencaoProcd;

    /**
     * Field _cdPagamentoNaoUtil
     */
    private int _cdPagamentoNaoUtil = 0;

    /**
     * keeps track of state for field: _cdPagamentoNaoUtil
     */
    private boolean _has_cdPagamentoNaoUtil;

    /**
     * Field _cdPrincipalEnquaRcadt
     */
    private int _cdPrincipalEnquaRcadt = 0;

    /**
     * keeps track of state for field: _cdPrincipalEnquaRcadt
     */
    private boolean _has_cdPrincipalEnquaRcadt;

    /**
     * Field _cdPriorEfetivacaoPagamento
     */
    private int _cdPriorEfetivacaoPagamento = 0;

    /**
     * keeps track of state for field: _cdPriorEfetivacaoPagamento
     */
    private boolean _has_cdPriorEfetivacaoPagamento;

    /**
     * Field _cdRejeiAgendaLote
     */
    private int _cdRejeiAgendaLote = 0;

    /**
     * keeps track of state for field: _cdRejeiAgendaLote
     */
    private boolean _has_cdRejeiAgendaLote;

    /**
     * Field _cdRejeiEfetivacaoLote
     */
    private int _cdRejeiEfetivacaoLote = 0;

    /**
     * keeps track of state for field: _cdRejeiEfetivacaoLote
     */
    private boolean _has_cdRejeiEfetivacaoLote;

    /**
     * Field _cdRejeiLote
     */
    private int _cdRejeiLote = 0;

    /**
     * keeps track of state for field: _cdRejeiLote
     */
    private boolean _has_cdRejeiLote;

    /**
     * Field _cdRastreabNotaFiscal
     */
    private int _cdRastreabNotaFiscal = 0;

    /**
     * keeps track of state for field: _cdRastreabNotaFiscal
     */
    private boolean _has_cdRastreabNotaFiscal;

    /**
     * Field _cdRastreabTituloTerc
     */
    private int _cdRastreabTituloTerc = 0;

    /**
     * keeps track of state for field: _cdRastreabTituloTerc
     */
    private boolean _has_cdRastreabTituloTerc;

    /**
     * Field _cdTipoCargaRcadt
     */
    private int _cdTipoCargaRcadt = 0;

    /**
     * keeps track of state for field: _cdTipoCargaRcadt
     */
    private boolean _has_cdTipoCargaRcadt;

    /**
     * Field _cdTipoCataoSalarial
     */
    private int _cdTipoCataoSalarial = 0;

    /**
     * keeps track of state for field: _cdTipoCataoSalarial
     */
    private boolean _has_cdTipoCataoSalarial;

    /**
     * Field _cdTipoDataFloat
     */
    private int _cdTipoDataFloat = 0;

    /**
     * keeps track of state for field: _cdTipoDataFloat
     */
    private boolean _has_cdTipoDataFloat;

    /**
     * Field _cdTipoDivergenciaVeiculo
     */
    private int _cdTipoDivergenciaVeiculo = 0;

    /**
     * keeps track of state for field: _cdTipoDivergenciaVeiculo
     */
    private boolean _has_cdTipoDivergenciaVeiculo;

    /**
     * Field _cdTipoIdBenef
     */
    private int _cdTipoIdBenef = 0;

    /**
     * keeps track of state for field: _cdTipoIdBenef
     */
    private boolean _has_cdTipoIdBenef;

    /**
     * Field _cdTipoLayoutArquivo
     */
    private int _cdTipoLayoutArquivo = 0;

    /**
     * keeps track of state for field: _cdTipoLayoutArquivo
     */
    private boolean _has_cdTipoLayoutArquivo;

    /**
     * Field _cdTipoReajusteTarifa
     */
    private int _cdTipoReajusteTarifa = 0;

    /**
     * keeps track of state for field: _cdTipoReajusteTarifa
     */
    private boolean _has_cdTipoReajusteTarifa;

    /**
     * Field _cdTratoContaTransf
     */
    private int _cdTratoContaTransf = 0;

    /**
     * keeps track of state for field: _cdTratoContaTransf
     */
    private boolean _has_cdTratoContaTransf;

    /**
     * Field _cdUtilzFavorecidoCtrl
     */
    private int _cdUtilzFavorecidoCtrl = 0;

    /**
     * keeps track of state for field: _cdUtilzFavorecidoCtrl
     */
    private boolean _has_cdUtilzFavorecidoCtrl;

    /**
     * Field _nrFechamentoApurcTarifa
     */
    private int _nrFechamentoApurcTarifa = 0;

    /**
     * keeps track of state for field: _nrFechamentoApurcTarifa
     */
    private boolean _has_nrFechamentoApurcTarifa;

    /**
     * Field _cdPercentualIndiceReajusteTarifa
     */
    private java.math.BigDecimal _cdPercentualIndiceReajusteTarifa = new java.math.BigDecimal("0");

    /**
     * Field _cdPercentualMaximoInconLote
     */
    private int _cdPercentualMaximoInconLote = 0;

    /**
     * keeps track of state for field: _cdPercentualMaximoInconLote
     */
    private boolean _has_cdPercentualMaximoInconLote;

    /**
     * Field _cdPercentualReducaoTarifaCatlg
     */
    private java.math.BigDecimal _cdPercentualReducaoTarifaCatlg = new java.math.BigDecimal("0");

    /**
     * Field _qtAntecedencia
     */
    private int _qtAntecedencia = 0;

    /**
     * keeps track of state for field: _qtAntecedencia
     */
    private boolean _has_qtAntecedencia;

    /**
     * Field _qtAnteriorVencimentoCompv
     */
    private int _qtAnteriorVencimentoCompv = 0;

    /**
     * keeps track of state for field: _qtAnteriorVencimentoCompv
     */
    private boolean _has_qtAnteriorVencimentoCompv;

    /**
     * Field _qtDiaCobrancaTarifa
     */
    private int _qtDiaCobrancaTarifa = 0;

    /**
     * keeps track of state for field: _qtDiaCobrancaTarifa
     */
    private boolean _has_qtDiaCobrancaTarifa;

    /**
     * Field _qtDiaExpiracao
     */
    private int _qtDiaExpiracao = 0;

    /**
     * keeps track of state for field: _qtDiaExpiracao
     */
    private boolean _has_qtDiaExpiracao;

    /**
     * Field _qtDiaFloatPagamento
     */
    private int _qtDiaFloatPagamento = 0;

    /**
     * keeps track of state for field: _qtDiaFloatPagamento
     */
    private boolean _has_qtDiaFloatPagamento;

    /**
     * Field _qtDiaInativFavorecido
     */
    private int _qtDiaInativFavorecido = 0;

    /**
     * keeps track of state for field: _qtDiaInativFavorecido
     */
    private boolean _has_qtDiaInativFavorecido;

    /**
     * Field _qtDiaRepiqCons
     */
    private int _qtDiaRepiqCons = 0;

    /**
     * keeps track of state for field: _qtDiaRepiqCons
     */
    private boolean _has_qtDiaRepiqCons;

    /**
     * Field _qtEtapaRcadtBeneficiario
     */
    private int _qtEtapaRcadtBeneficiario = 0;

    /**
     * keeps track of state for field: _qtEtapaRcadtBeneficiario
     */
    private boolean _has_qtEtapaRcadtBeneficiario;

    /**
     * Field _qtFaseRcadtBenefiario
     */
    private int _qtFaseRcadtBenefiario = 0;

    /**
     * keeps track of state for field: _qtFaseRcadtBenefiario
     */
    private boolean _has_qtFaseRcadtBenefiario;

    /**
     * Field _qtLimiteLinhas
     */
    private int _qtLimiteLinhas = 0;

    /**
     * keeps track of state for field: _qtLimiteLinhas
     */
    private boolean _has_qtLimiteLinhas;

    /**
     * Field _qtLimiteSolicitacaoCatao
     */
    private int _qtLimiteSolicitacaoCatao = 0;

    /**
     * keeps track of state for field: _qtLimiteSolicitacaoCatao
     */
    private boolean _has_qtLimiteSolicitacaoCatao;

    /**
     * Field _qtMaximaInconLote
     */
    private int _qtMaximaInconLote = 0;

    /**
     * keeps track of state for field: _qtMaximaInconLote
     */
    private boolean _has_qtMaximaInconLote;

    /**
     * Field _qtMaximaTituloVencido
     */
    private int _qtMaximaTituloVencido = 0;

    /**
     * keeps track of state for field: _qtMaximaTituloVencido
     */
    private boolean _has_qtMaximaTituloVencido;

    /**
     * Field _qtMesComprovante
     */
    private int _qtMesComprovante = 0;

    /**
     * keeps track of state for field: _qtMesComprovante
     */
    private boolean _has_qtMesComprovante;

    /**
     * Field _qtMesRcadt
     */
    private int _qtMesRcadt = 0;

    /**
     * keeps track of state for field: _qtMesRcadt
     */
    private boolean _has_qtMesRcadt;

    /**
     * Field _qtMesFaseRcadt
     */
    private int _qtMesFaseRcadt = 0;

    /**
     * keeps track of state for field: _qtMesFaseRcadt
     */
    private boolean _has_qtMesFaseRcadt;

    /**
     * Field _qtMesReajusteTarifa
     */
    private int _qtMesReajusteTarifa = 0;

    /**
     * keeps track of state for field: _qtMesReajusteTarifa
     */
    private boolean _has_qtMesReajusteTarifa;

    /**
     * Field _qtViaAviso
     */
    private int _qtViaAviso = 0;

    /**
     * keeps track of state for field: _qtViaAviso
     */
    private boolean _has_qtViaAviso;

    /**
     * Field _qtViaCobranca
     */
    private int _qtViaCobranca = 0;

    /**
     * keeps track of state for field: _qtViaCobranca
     */
    private boolean _has_qtViaCobranca;

    /**
     * Field _qtViaComprovante
     */
    private int _qtViaComprovante = 0;

    /**
     * keeps track of state for field: _qtViaComprovante
     */
    private boolean _has_qtViaComprovante;

    /**
     * Field _vlFavorecidoNaoCadtr
     */
    private java.math.BigDecimal _vlFavorecidoNaoCadtr = new java.math.BigDecimal("0");

    /**
     * Field _vlLimiteDiaPagamento
     */
    private java.math.BigDecimal _vlLimiteDiaPagamento = new java.math.BigDecimal("0");

    /**
     * Field _vlLimiteIndvdPagamento
     */
    private java.math.BigDecimal _vlLimiteIndvdPagamento = new java.math.BigDecimal("0");

    /**
     * Field _cdIndicadorEmissaoAviso
     */
    private int _cdIndicadorEmissaoAviso = 0;

    /**
     * keeps track of state for field: _cdIndicadorEmissaoAviso
     */
    private boolean _has_cdIndicadorEmissaoAviso;

    /**
     * Field _cdIndicadorRetornoInternet
     */
    private int _cdIndicadorRetornoInternet = 0;

    /**
     * keeps track of state for field: _cdIndicadorRetornoInternet
     */
    private boolean _has_cdIndicadorRetornoInternet;

    /**
     * Field _cdIndicadorListaDebito
     */
    private int _cdIndicadorListaDebito = 0;

    /**
     * keeps track of state for field: _cdIndicadorListaDebito
     */
    private boolean _has_cdIndicadorListaDebito;

    /**
     * Field _cdTipoFormacaoLista
     */
    private int _cdTipoFormacaoLista = 0;

    /**
     * keeps track of state for field: _cdTipoFormacaoLista
     */
    private boolean _has_cdTipoFormacaoLista;

    /**
     * Field _cdTipoConsistenciaLista
     */
    private int _cdTipoConsistenciaLista = 0;

    /**
     * keeps track of state for field: _cdTipoConsistenciaLista
     */
    private boolean _has_cdTipoConsistenciaLista;

    /**
     * Field _cdTipoConsultaComprovante
     */
    private int _cdTipoConsultaComprovante = 0;

    /**
     * keeps track of state for field: _cdTipoConsultaComprovante
     */
    private boolean _has_cdTipoConsultaComprovante;

    /**
     * Field _cdValidacaoNomeFavorecido
     */
    private int _cdValidacaoNomeFavorecido = 0;

    /**
     * keeps track of state for field: _cdValidacaoNomeFavorecido
     */
    private boolean _has_cdValidacaoNomeFavorecido;

    /**
     * Field _cdTipoContaFavorecido
     */
    private int _cdTipoContaFavorecido = 0;

    /**
     * keeps track of state for field: _cdTipoContaFavorecido
     */
    private boolean _has_cdTipoContaFavorecido;

    /**
     * Field _cdIndLancamentoPersonalizado
     */
    private int _cdIndLancamentoPersonalizado = 0;

    /**
     * keeps track of state for field: _cdIndLancamentoPersonalizado
     */
    private boolean _has_cdIndLancamentoPersonalizado;

    /**
     * Field _cdAgendaRastreabilidadeFilial
     */
    private int _cdAgendaRastreabilidadeFilial = 0;

    /**
     * keeps track of state for field: _cdAgendaRastreabilidadeFilia
     */
    private boolean _has_cdAgendaRastreabilidadeFilial;

    /**
     * Field _cdIndicadorAdesaoSacador
     */
    private int _cdIndicadorAdesaoSacador = 0;

    /**
     * keeps track of state for field: _cdIndicadorAdesaoSacador
     */
    private boolean _has_cdIndicadorAdesaoSacador;

    /**
     * Field _cdMeioPagamentoCredito
     */
    private int _cdMeioPagamentoCredito = 0;

    /**
     * keeps track of state for field: _cdMeioPagamentoCredito
     */
    private boolean _has_cdMeioPagamentoCredito;

    /**
     * Field _cdTipoIsncricaoFavorecido
     */
    private int _cdTipoIsncricaoFavorecido = 0;

    /**
     * keeps track of state for field: _cdTipoIsncricaoFavorecido
     */
    private boolean _has_cdTipoIsncricaoFavorecido;

    /**
     * Field _cdIndicadorBancoPostal
     */
    private int _cdIndicadorBancoPostal = 0;

    /**
     * keeps track of state for field: _cdIndicadorBancoPostal
     */
    private boolean _has_cdIndicadorBancoPostal;

    /**
     * Field _cdFormularioContratoCliente
     */
    private int _cdFormularioContratoCliente = 0;

    /**
     * keeps track of state for field: _cdFormularioContratoCliente
     */
    private boolean _has_cdFormularioContratoCliente;

    /**
     * Field _cdConsultaSaldoValorSuperior
     */
    private int _cdConsultaSaldoValorSuperior = 0;

    /**
     * keeps track of state for field: _cdConsultaSaldoValorSuperior
     */
    private boolean _has_cdConsultaSaldoValorSuperior;


      //----------------/
     //- Constructors -/
    //----------------/

    public ConsultarConfigPadraoAtribTipoServModResponse() 
     {
        super();
        setCdPercentualIndiceReajusteTarifa(new java.math.BigDecimal("0"));
        setCdPercentualReducaoTarifaCatlg(new java.math.BigDecimal("0"));
        setVlFavorecidoNaoCadtr(new java.math.BigDecimal("0"));
        setVlLimiteDiaPagamento(new java.math.BigDecimal("0"));
        setVlLimiteIndvdPagamento(new java.math.BigDecimal("0"));
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarconfigpadraoatribtiposervmod.response.ConsultarConfigPadraoAtribTipoServModResponse()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdAGendaValorMenor
     * 
     */
    public void deleteCdAGendaValorMenor()
    {
        this._has_cdAGendaValorMenor= false;
    } //-- void deleteCdAGendaValorMenor() 

    /**
     * Method deleteCdAcaoNaoVida
     * 
     */
    public void deleteCdAcaoNaoVida()
    {
        this._has_cdAcaoNaoVida= false;
    } //-- void deleteCdAcaoNaoVida() 

    /**
     * Method deleteCdAcertoDadoRecadastro
     * 
     */
    public void deleteCdAcertoDadoRecadastro()
    {
        this._has_cdAcertoDadoRecadastro= false;
    } //-- void deleteCdAcertoDadoRecadastro() 

    /**
     * Method deleteCdAgendaDebitoVeiculo
     * 
     */
    public void deleteCdAgendaDebitoVeiculo()
    {
        this._has_cdAgendaDebitoVeiculo= false;
    } //-- void deleteCdAgendaDebitoVeiculo() 

    /**
     * Method deleteCdAgendaPagamentoVencido
     * 
     */
    public void deleteCdAgendaPagamentoVencido()
    {
        this._has_cdAgendaPagamentoVencido= false;
    } //-- void deleteCdAgendaPagamentoVencido() 

    /**
     * Method deleteCdAgendaRastreabilidadeFilial
     * 
     */
    public void deleteCdAgendaRastreabilidadeFilial()
    {
        this._has_cdAgendaRastreabilidadeFilial= false;
    } //-- void deleteCdAgendaRastreabilidadeFilial() 

    /**
     * Method deleteCdAgptoFormularioRecadastro
     * 
     */
    public void deleteCdAgptoFormularioRecadastro()
    {
        this._has_cdAgptoFormularioRecadastro= false;
    } //-- void deleteCdAgptoFormularioRecadastro() 

    /**
     * Method deleteCdAgrupamentoAviso
     * 
     */
    public void deleteCdAgrupamentoAviso()
    {
        this._has_cdAgrupamentoAviso= false;
    } //-- void deleteCdAgrupamentoAviso() 

    /**
     * Method deleteCdAgrupamentoComprovado
     * 
     */
    public void deleteCdAgrupamentoComprovado()
    {
        this._has_cdAgrupamentoComprovado= false;
    } //-- void deleteCdAgrupamentoComprovado() 

    /**
     * Method deleteCdAntecRecadastroBeneficiario
     * 
     */
    public void deleteCdAntecRecadastroBeneficiario()
    {
        this._has_cdAntecRecadastroBeneficiario= false;
    } //-- void deleteCdAntecRecadastroBeneficiario() 

    /**
     * Method deleteCdAreaReservada
     * 
     */
    public void deleteCdAreaReservada()
    {
        this._has_cdAreaReservada= false;
    } //-- void deleteCdAreaReservada() 

    /**
     * Method deleteCdBasaeRecadastroBeneficiario
     * 
     */
    public void deleteCdBasaeRecadastroBeneficiario()
    {
        this._has_cdBasaeRecadastroBeneficiario= false;
    } //-- void deleteCdBasaeRecadastroBeneficiario() 

    /**
     * Method deleteCdBloqueioEmissaoPplta
     * 
     */
    public void deleteCdBloqueioEmissaoPplta()
    {
        this._has_cdBloqueioEmissaoPplta= false;
    } //-- void deleteCdBloqueioEmissaoPplta() 

    /**
     * Method deleteCdCaptuTituloRegistro
     * 
     */
    public void deleteCdCaptuTituloRegistro()
    {
        this._has_cdCaptuTituloRegistro= false;
    } //-- void deleteCdCaptuTituloRegistro() 

    /**
     * Method deleteCdCobrancaTarifa
     * 
     */
    public void deleteCdCobrancaTarifa()
    {
        this._has_cdCobrancaTarifa= false;
    } //-- void deleteCdCobrancaTarifa() 

    /**
     * Method deleteCdConsDebitoVeiculo
     * 
     */
    public void deleteCdConsDebitoVeiculo()
    {
        this._has_cdConsDebitoVeiculo= false;
    } //-- void deleteCdConsDebitoVeiculo() 

    /**
     * Method deleteCdConsEndereco
     * 
     */
    public void deleteCdConsEndereco()
    {
        this._has_cdConsEndereco= false;
    } //-- void deleteCdConsEndereco() 

    /**
     * Method deleteCdConsSaldoPagamento
     * 
     */
    public void deleteCdConsSaldoPagamento()
    {
        this._has_cdConsSaldoPagamento= false;
    } //-- void deleteCdConsSaldoPagamento() 

    /**
     * Method deleteCdConsultaSaldoValorSuperior
     * 
     */
    public void deleteCdConsultaSaldoValorSuperior()
    {
        this._has_cdConsultaSaldoValorSuperior= false;
    } //-- void deleteCdConsultaSaldoValorSuperior() 

    /**
     * Method deleteCdContgConsSaldo
     * 
     */
    public void deleteCdContgConsSaldo()
    {
        this._has_cdContgConsSaldo= false;
    } //-- void deleteCdContgConsSaldo() 

    /**
     * Method deleteCdCreditoNaoUtilizado
     * 
     */
    public void deleteCdCreditoNaoUtilizado()
    {
        this._has_cdCreditoNaoUtilizado= false;
    } //-- void deleteCdCreditoNaoUtilizado() 

    /**
     * Method deleteCdCriterioEnquaBeneficiario
     * 
     */
    public void deleteCdCriterioEnquaBeneficiario()
    {
        this._has_cdCriterioEnquaBeneficiario= false;
    } //-- void deleteCdCriterioEnquaBeneficiario() 

    /**
     * Method deleteCdCriterioEnquaRcadt
     * 
     */
    public void deleteCdCriterioEnquaRcadt()
    {
        this._has_cdCriterioEnquaRcadt= false;
    } //-- void deleteCdCriterioEnquaRcadt() 

    /**
     * Method deleteCdCriterioRstrbTitulo
     * 
     */
    public void deleteCdCriterioRstrbTitulo()
    {
        this._has_cdCriterioRstrbTitulo= false;
    } //-- void deleteCdCriterioRstrbTitulo() 

    /**
     * Method deleteCdCtciaEspecieBeneficiario
     * 
     */
    public void deleteCdCtciaEspecieBeneficiario()
    {
        this._has_cdCtciaEspecieBeneficiario= false;
    } //-- void deleteCdCtciaEspecieBeneficiario() 

    /**
     * Method deleteCdCtciaIdBeneficiario
     * 
     */
    public void deleteCdCtciaIdBeneficiario()
    {
        this._has_cdCtciaIdBeneficiario= false;
    } //-- void deleteCdCtciaIdBeneficiario() 

    /**
     * Method deleteCdCtciaInscricaoFavorecido
     * 
     */
    public void deleteCdCtciaInscricaoFavorecido()
    {
        this._has_cdCtciaInscricaoFavorecido= false;
    } //-- void deleteCdCtciaInscricaoFavorecido() 

    /**
     * Method deleteCdCtciaProprietarioVeiculo
     * 
     */
    public void deleteCdCtciaProprietarioVeiculo()
    {
        this._has_cdCtciaProprietarioVeiculo= false;
    } //-- void deleteCdCtciaProprietarioVeiculo() 

    /**
     * Method deleteCdDestinoAviso
     * 
     */
    public void deleteCdDestinoAviso()
    {
        this._has_cdDestinoAviso= false;
    } //-- void deleteCdDestinoAviso() 

    /**
     * Method deleteCdDestinoComprovante
     * 
     */
    public void deleteCdDestinoComprovante()
    {
        this._has_cdDestinoComprovante= false;
    } //-- void deleteCdDestinoComprovante() 

    /**
     * Method deleteCdDestinoFormularioRcadt
     * 
     */
    public void deleteCdDestinoFormularioRcadt()
    {
        this._has_cdDestinoFormularioRcadt= false;
    } //-- void deleteCdDestinoFormularioRcadt() 

    /**
     * Method deleteCdDispzContaCredito
     * 
     */
    public void deleteCdDispzContaCredito()
    {
        this._has_cdDispzContaCredito= false;
    } //-- void deleteCdDispzContaCredito() 

    /**
     * Method deleteCdDispzDiversaoNao
     * 
     */
    public void deleteCdDispzDiversaoNao()
    {
        this._has_cdDispzDiversaoNao= false;
    } //-- void deleteCdDispzDiversaoNao() 

    /**
     * Method deleteCdDispzDiversosCrrtt
     * 
     */
    public void deleteCdDispzDiversosCrrtt()
    {
        this._has_cdDispzDiversosCrrtt= false;
    } //-- void deleteCdDispzDiversosCrrtt() 

    /**
     * Method deleteCdDispzSalarialCrrtt
     * 
     */
    public void deleteCdDispzSalarialCrrtt()
    {
        this._has_cdDispzSalarialCrrtt= false;
    } //-- void deleteCdDispzSalarialCrrtt() 

    /**
     * Method deleteCdDispzSalarialNao
     * 
     */
    public void deleteCdDispzSalarialNao()
    {
        this._has_cdDispzSalarialNao= false;
    } //-- void deleteCdDispzSalarialNao() 

    /**
     * Method deleteCdEnvelopeAberto
     * 
     */
    public void deleteCdEnvelopeAberto()
    {
        this._has_cdEnvelopeAberto= false;
    } //-- void deleteCdEnvelopeAberto() 

    /**
     * Method deleteCdFavorecidoConsPgto
     * 
     */
    public void deleteCdFavorecidoConsPgto()
    {
        this._has_cdFavorecidoConsPgto= false;
    } //-- void deleteCdFavorecidoConsPgto() 

    /**
     * Method deleteCdFormaAutrzPgto
     * 
     */
    public void deleteCdFormaAutrzPgto()
    {
        this._has_cdFormaAutrzPgto= false;
    } //-- void deleteCdFormaAutrzPgto() 

    /**
     * Method deleteCdFormaEnvioPagamento
     * 
     */
    public void deleteCdFormaEnvioPagamento()
    {
        this._has_cdFormaEnvioPagamento= false;
    } //-- void deleteCdFormaEnvioPagamento() 

    /**
     * Method deleteCdFormaExpirCredito
     * 
     */
    public void deleteCdFormaExpirCredito()
    {
        this._has_cdFormaExpirCredito= false;
    } //-- void deleteCdFormaExpirCredito() 

    /**
     * Method deleteCdFormaManutencao
     * 
     */
    public void deleteCdFormaManutencao()
    {
        this._has_cdFormaManutencao= false;
    } //-- void deleteCdFormaManutencao() 

    /**
     * Method deleteCdFormularioContratoCliente
     * 
     */
    public void deleteCdFormularioContratoCliente()
    {
        this._has_cdFormularioContratoCliente= false;
    } //-- void deleteCdFormularioContratoCliente() 

    /**
     * Method deleteCdFrasePreCadastro
     * 
     */
    public void deleteCdFrasePreCadastro()
    {
        this._has_cdFrasePreCadastro= false;
    } //-- void deleteCdFrasePreCadastro() 

    /**
     * Method deleteCdIndLancamentoPersonalizado
     * 
     */
    public void deleteCdIndLancamentoPersonalizado()
    {
        this._has_cdIndLancamentoPersonalizado= false;
    } //-- void deleteCdIndLancamentoPersonalizado() 

    /**
     * Method deleteCdIndicadorAdesaoSacador
     * 
     */
    public void deleteCdIndicadorAdesaoSacador()
    {
        this._has_cdIndicadorAdesaoSacador= false;
    } //-- void deleteCdIndicadorAdesaoSacador() 

    /**
     * Method deleteCdIndicadorAgendaTitulo
     * 
     */
    public void deleteCdIndicadorAgendaTitulo()
    {
        this._has_cdIndicadorAgendaTitulo= false;
    } //-- void deleteCdIndicadorAgendaTitulo() 

    /**
     * Method deleteCdIndicadorAutrzCliente
     * 
     */
    public void deleteCdIndicadorAutrzCliente()
    {
        this._has_cdIndicadorAutrzCliente= false;
    } //-- void deleteCdIndicadorAutrzCliente() 

    /**
     * Method deleteCdIndicadorAutrzCompl
     * 
     */
    public void deleteCdIndicadorAutrzCompl()
    {
        this._has_cdIndicadorAutrzCompl= false;
    } //-- void deleteCdIndicadorAutrzCompl() 

    /**
     * Method deleteCdIndicadorBancoPostal
     * 
     */
    public void deleteCdIndicadorBancoPostal()
    {
        this._has_cdIndicadorBancoPostal= false;
    } //-- void deleteCdIndicadorBancoPostal() 

    /**
     * Method deleteCdIndicadorCadOrg
     * 
     */
    public void deleteCdIndicadorCadOrg()
    {
        this._has_cdIndicadorCadOrg= false;
    } //-- void deleteCdIndicadorCadOrg() 

    /**
     * Method deleteCdIndicadorCadProcd
     * 
     */
    public void deleteCdIndicadorCadProcd()
    {
        this._has_cdIndicadorCadProcd= false;
    } //-- void deleteCdIndicadorCadProcd() 

    /**
     * Method deleteCdIndicadorCartaoSalarial
     * 
     */
    public void deleteCdIndicadorCartaoSalarial()
    {
        this._has_cdIndicadorCartaoSalarial= false;
    } //-- void deleteCdIndicadorCartaoSalarial() 

    /**
     * Method deleteCdIndicadorEconomicoReajuste
     * 
     */
    public void deleteCdIndicadorEconomicoReajuste()
    {
        this._has_cdIndicadorEconomicoReajuste= false;
    } //-- void deleteCdIndicadorEconomicoReajuste() 

    /**
     * Method deleteCdIndicadorEmissaoAviso
     * 
     */
    public void deleteCdIndicadorEmissaoAviso()
    {
        this._has_cdIndicadorEmissaoAviso= false;
    } //-- void deleteCdIndicadorEmissaoAviso() 

    /**
     * Method deleteCdIndicadorExpirCredito
     * 
     */
    public void deleteCdIndicadorExpirCredito()
    {
        this._has_cdIndicadorExpirCredito= false;
    } //-- void deleteCdIndicadorExpirCredito() 

    /**
     * Method deleteCdIndicadorLctoPgmd
     * 
     */
    public void deleteCdIndicadorLctoPgmd()
    {
        this._has_cdIndicadorLctoPgmd= false;
    } //-- void deleteCdIndicadorLctoPgmd() 

    /**
     * Method deleteCdIndicadorListaDebito
     * 
     */
    public void deleteCdIndicadorListaDebito()
    {
        this._has_cdIndicadorListaDebito= false;
    } //-- void deleteCdIndicadorListaDebito() 

    /**
     * Method deleteCdIndicadorMensagemPerso
     * 
     */
    public void deleteCdIndicadorMensagemPerso()
    {
        this._has_cdIndicadorMensagemPerso= false;
    } //-- void deleteCdIndicadorMensagemPerso() 

    /**
     * Method deleteCdIndicadorRetornoInternet
     * 
     */
    public void deleteCdIndicadorRetornoInternet()
    {
        this._has_cdIndicadorRetornoInternet= false;
    } //-- void deleteCdIndicadorRetornoInternet() 

    /**
     * Method deleteCdLancamentoFuturoCredito
     * 
     */
    public void deleteCdLancamentoFuturoCredito()
    {
        this._has_cdLancamentoFuturoCredito= false;
    } //-- void deleteCdLancamentoFuturoCredito() 

    /**
     * Method deleteCdLancamentoFuturoDebito
     * 
     */
    public void deleteCdLancamentoFuturoDebito()
    {
        this._has_cdLancamentoFuturoDebito= false;
    } //-- void deleteCdLancamentoFuturoDebito() 

    /**
     * Method deleteCdLiberacaoLoteProcs
     * 
     */
    public void deleteCdLiberacaoLoteProcs()
    {
        this._has_cdLiberacaoLoteProcs= false;
    } //-- void deleteCdLiberacaoLoteProcs() 

    /**
     * Method deleteCdManutencaoBaseRcadt
     * 
     */
    public void deleteCdManutencaoBaseRcadt()
    {
        this._has_cdManutencaoBaseRcadt= false;
    } //-- void deleteCdManutencaoBaseRcadt() 

    /**
     * Method deleteCdMeioPagamentoCredito
     * 
     */
    public void deleteCdMeioPagamentoCredito()
    {
        this._has_cdMeioPagamentoCredito= false;
    } //-- void deleteCdMeioPagamentoCredito() 

    /**
     * Method deleteCdMensagemRcadtMidia
     * 
     */
    public void deleteCdMensagemRcadtMidia()
    {
        this._has_cdMensagemRcadtMidia= false;
    } //-- void deleteCdMensagemRcadtMidia() 

    /**
     * Method deleteCdMidiaDisponivel
     * 
     */
    public void deleteCdMidiaDisponivel()
    {
        this._has_cdMidiaDisponivel= false;
    } //-- void deleteCdMidiaDisponivel() 

    /**
     * Method deleteCdMidiaMensagemRcadt
     * 
     */
    public void deleteCdMidiaMensagemRcadt()
    {
        this._has_cdMidiaMensagemRcadt= false;
    } //-- void deleteCdMidiaMensagemRcadt() 

    /**
     * Method deleteCdMomenAvisoRcadt
     * 
     */
    public void deleteCdMomenAvisoRcadt()
    {
        this._has_cdMomenAvisoRcadt= false;
    } //-- void deleteCdMomenAvisoRcadt() 

    /**
     * Method deleteCdMomenCreditoEfetivo
     * 
     */
    public void deleteCdMomenCreditoEfetivo()
    {
        this._has_cdMomenCreditoEfetivo= false;
    } //-- void deleteCdMomenCreditoEfetivo() 

    /**
     * Method deleteCdMomenDebitoPagamento
     * 
     */
    public void deleteCdMomenDebitoPagamento()
    {
        this._has_cdMomenDebitoPagamento= false;
    } //-- void deleteCdMomenDebitoPagamento() 

    /**
     * Method deleteCdMomenFormulaRcadt
     * 
     */
    public void deleteCdMomenFormulaRcadt()
    {
        this._has_cdMomenFormulaRcadt= false;
    } //-- void deleteCdMomenFormulaRcadt() 

    /**
     * Method deleteCdMomenProcmPagamento
     * 
     */
    public void deleteCdMomenProcmPagamento()
    {
        this._has_cdMomenProcmPagamento= false;
    } //-- void deleteCdMomenProcmPagamento() 

    /**
     * Method deleteCdNaturezaOperacaoPagamento
     * 
     */
    public void deleteCdNaturezaOperacaoPagamento()
    {
        this._has_cdNaturezaOperacaoPagamento= false;
    } //-- void deleteCdNaturezaOperacaoPagamento() 

    /**
     * Method deleteCdPagamentoNaoUtil
     * 
     */
    public void deleteCdPagamentoNaoUtil()
    {
        this._has_cdPagamentoNaoUtil= false;
    } //-- void deleteCdPagamentoNaoUtil() 

    /**
     * Method deleteCdPercentualMaximoInconLote
     * 
     */
    public void deleteCdPercentualMaximoInconLote()
    {
        this._has_cdPercentualMaximoInconLote= false;
    } //-- void deleteCdPercentualMaximoInconLote() 

    /**
     * Method deleteCdPerdcAviso
     * 
     */
    public void deleteCdPerdcAviso()
    {
        this._has_cdPerdcAviso= false;
    } //-- void deleteCdPerdcAviso() 

    /**
     * Method deleteCdPerdcCobrancaTarifa
     * 
     */
    public void deleteCdPerdcCobrancaTarifa()
    {
        this._has_cdPerdcCobrancaTarifa= false;
    } //-- void deleteCdPerdcCobrancaTarifa() 

    /**
     * Method deleteCdPerdcComprovante
     * 
     */
    public void deleteCdPerdcComprovante()
    {
        this._has_cdPerdcComprovante= false;
    } //-- void deleteCdPerdcComprovante() 

    /**
     * Method deleteCdPerdcConsVeiculo
     * 
     */
    public void deleteCdPerdcConsVeiculo()
    {
        this._has_cdPerdcConsVeiculo= false;
    } //-- void deleteCdPerdcConsVeiculo() 

    /**
     * Method deleteCdPerdcEnvioRemessa
     * 
     */
    public void deleteCdPerdcEnvioRemessa()
    {
        this._has_cdPerdcEnvioRemessa= false;
    } //-- void deleteCdPerdcEnvioRemessa() 

    /**
     * Method deleteCdPerdcManutencaoProcd
     * 
     */
    public void deleteCdPerdcManutencaoProcd()
    {
        this._has_cdPerdcManutencaoProcd= false;
    } //-- void deleteCdPerdcManutencaoProcd() 

    /**
     * Method deleteCdPermissaoDebitoOnline
     * 
     */
    public void deleteCdPermissaoDebitoOnline()
    {
        this._has_cdPermissaoDebitoOnline= false;
    } //-- void deleteCdPermissaoDebitoOnline() 

    /**
     * Method deleteCdPrincipalEnquaRcadt
     * 
     */
    public void deleteCdPrincipalEnquaRcadt()
    {
        this._has_cdPrincipalEnquaRcadt= false;
    } //-- void deleteCdPrincipalEnquaRcadt() 

    /**
     * Method deleteCdPriorEfetivacaoPagamento
     * 
     */
    public void deleteCdPriorEfetivacaoPagamento()
    {
        this._has_cdPriorEfetivacaoPagamento= false;
    } //-- void deleteCdPriorEfetivacaoPagamento() 

    /**
     * Method deleteCdRastreabNotaFiscal
     * 
     */
    public void deleteCdRastreabNotaFiscal()
    {
        this._has_cdRastreabNotaFiscal= false;
    } //-- void deleteCdRastreabNotaFiscal() 

    /**
     * Method deleteCdRastreabTituloTerc
     * 
     */
    public void deleteCdRastreabTituloTerc()
    {
        this._has_cdRastreabTituloTerc= false;
    } //-- void deleteCdRastreabTituloTerc() 

    /**
     * Method deleteCdRejeiAgendaLote
     * 
     */
    public void deleteCdRejeiAgendaLote()
    {
        this._has_cdRejeiAgendaLote= false;
    } //-- void deleteCdRejeiAgendaLote() 

    /**
     * Method deleteCdRejeiEfetivacaoLote
     * 
     */
    public void deleteCdRejeiEfetivacaoLote()
    {
        this._has_cdRejeiEfetivacaoLote= false;
    } //-- void deleteCdRejeiEfetivacaoLote() 

    /**
     * Method deleteCdRejeiLote
     * 
     */
    public void deleteCdRejeiLote()
    {
        this._has_cdRejeiLote= false;
    } //-- void deleteCdRejeiLote() 

    /**
     * Method deleteCdTipoCargaRcadt
     * 
     */
    public void deleteCdTipoCargaRcadt()
    {
        this._has_cdTipoCargaRcadt= false;
    } //-- void deleteCdTipoCargaRcadt() 

    /**
     * Method deleteCdTipoCataoSalarial
     * 
     */
    public void deleteCdTipoCataoSalarial()
    {
        this._has_cdTipoCataoSalarial= false;
    } //-- void deleteCdTipoCataoSalarial() 

    /**
     * Method deleteCdTipoConsistenciaLista
     * 
     */
    public void deleteCdTipoConsistenciaLista()
    {
        this._has_cdTipoConsistenciaLista= false;
    } //-- void deleteCdTipoConsistenciaLista() 

    /**
     * Method deleteCdTipoConsultaComprovante
     * 
     */
    public void deleteCdTipoConsultaComprovante()
    {
        this._has_cdTipoConsultaComprovante= false;
    } //-- void deleteCdTipoConsultaComprovante() 

    /**
     * Method deleteCdTipoContaFavorecido
     * 
     */
    public void deleteCdTipoContaFavorecido()
    {
        this._has_cdTipoContaFavorecido= false;
    } //-- void deleteCdTipoContaFavorecido() 

    /**
     * Method deleteCdTipoDataFloat
     * 
     */
    public void deleteCdTipoDataFloat()
    {
        this._has_cdTipoDataFloat= false;
    } //-- void deleteCdTipoDataFloat() 

    /**
     * Method deleteCdTipoDivergenciaVeiculo
     * 
     */
    public void deleteCdTipoDivergenciaVeiculo()
    {
        this._has_cdTipoDivergenciaVeiculo= false;
    } //-- void deleteCdTipoDivergenciaVeiculo() 

    /**
     * Method deleteCdTipoFormacaoLista
     * 
     */
    public void deleteCdTipoFormacaoLista()
    {
        this._has_cdTipoFormacaoLista= false;
    } //-- void deleteCdTipoFormacaoLista() 

    /**
     * Method deleteCdTipoIdBenef
     * 
     */
    public void deleteCdTipoIdBenef()
    {
        this._has_cdTipoIdBenef= false;
    } //-- void deleteCdTipoIdBenef() 

    /**
     * Method deleteCdTipoIsncricaoFavorecido
     * 
     */
    public void deleteCdTipoIsncricaoFavorecido()
    {
        this._has_cdTipoIsncricaoFavorecido= false;
    } //-- void deleteCdTipoIsncricaoFavorecido() 

    /**
     * Method deleteCdTipoLayoutArquivo
     * 
     */
    public void deleteCdTipoLayoutArquivo()
    {
        this._has_cdTipoLayoutArquivo= false;
    } //-- void deleteCdTipoLayoutArquivo() 

    /**
     * Method deleteCdTipoReajusteTarifa
     * 
     */
    public void deleteCdTipoReajusteTarifa()
    {
        this._has_cdTipoReajusteTarifa= false;
    } //-- void deleteCdTipoReajusteTarifa() 

    /**
     * Method deleteCdTratoContaTransf
     * 
     */
    public void deleteCdTratoContaTransf()
    {
        this._has_cdTratoContaTransf= false;
    } //-- void deleteCdTratoContaTransf() 

    /**
     * Method deleteCdUtilzFavorecidoCtrl
     * 
     */
    public void deleteCdUtilzFavorecidoCtrl()
    {
        this._has_cdUtilzFavorecidoCtrl= false;
    } //-- void deleteCdUtilzFavorecidoCtrl() 

    /**
     * Method deleteCdValidacaoNomeFavorecido
     * 
     */
    public void deleteCdValidacaoNomeFavorecido()
    {
        this._has_cdValidacaoNomeFavorecido= false;
    } //-- void deleteCdValidacaoNomeFavorecido() 

    /**
     * Method deleteNrFechamentoApurcTarifa
     * 
     */
    public void deleteNrFechamentoApurcTarifa()
    {
        this._has_nrFechamentoApurcTarifa= false;
    } //-- void deleteNrFechamentoApurcTarifa() 

    /**
     * Method deleteQtAntecedencia
     * 
     */
    public void deleteQtAntecedencia()
    {
        this._has_qtAntecedencia= false;
    } //-- void deleteQtAntecedencia() 

    /**
     * Method deleteQtAnteriorVencimentoCompv
     * 
     */
    public void deleteQtAnteriorVencimentoCompv()
    {
        this._has_qtAnteriorVencimentoCompv= false;
    } //-- void deleteQtAnteriorVencimentoCompv() 

    /**
     * Method deleteQtDiaCobrancaTarifa
     * 
     */
    public void deleteQtDiaCobrancaTarifa()
    {
        this._has_qtDiaCobrancaTarifa= false;
    } //-- void deleteQtDiaCobrancaTarifa() 

    /**
     * Method deleteQtDiaExpiracao
     * 
     */
    public void deleteQtDiaExpiracao()
    {
        this._has_qtDiaExpiracao= false;
    } //-- void deleteQtDiaExpiracao() 

    /**
     * Method deleteQtDiaFloatPagamento
     * 
     */
    public void deleteQtDiaFloatPagamento()
    {
        this._has_qtDiaFloatPagamento= false;
    } //-- void deleteQtDiaFloatPagamento() 

    /**
     * Method deleteQtDiaInativFavorecido
     * 
     */
    public void deleteQtDiaInativFavorecido()
    {
        this._has_qtDiaInativFavorecido= false;
    } //-- void deleteQtDiaInativFavorecido() 

    /**
     * Method deleteQtDiaRepiqCons
     * 
     */
    public void deleteQtDiaRepiqCons()
    {
        this._has_qtDiaRepiqCons= false;
    } //-- void deleteQtDiaRepiqCons() 

    /**
     * Method deleteQtEtapaRcadtBeneficiario
     * 
     */
    public void deleteQtEtapaRcadtBeneficiario()
    {
        this._has_qtEtapaRcadtBeneficiario= false;
    } //-- void deleteQtEtapaRcadtBeneficiario() 

    /**
     * Method deleteQtFaseRcadtBenefiario
     * 
     */
    public void deleteQtFaseRcadtBenefiario()
    {
        this._has_qtFaseRcadtBenefiario= false;
    } //-- void deleteQtFaseRcadtBenefiario() 

    /**
     * Method deleteQtLimiteLinhas
     * 
     */
    public void deleteQtLimiteLinhas()
    {
        this._has_qtLimiteLinhas= false;
    } //-- void deleteQtLimiteLinhas() 

    /**
     * Method deleteQtLimiteSolicitacaoCatao
     * 
     */
    public void deleteQtLimiteSolicitacaoCatao()
    {
        this._has_qtLimiteSolicitacaoCatao= false;
    } //-- void deleteQtLimiteSolicitacaoCatao() 

    /**
     * Method deleteQtMaximaInconLote
     * 
     */
    public void deleteQtMaximaInconLote()
    {
        this._has_qtMaximaInconLote= false;
    } //-- void deleteQtMaximaInconLote() 

    /**
     * Method deleteQtMaximaTituloVencido
     * 
     */
    public void deleteQtMaximaTituloVencido()
    {
        this._has_qtMaximaTituloVencido= false;
    } //-- void deleteQtMaximaTituloVencido() 

    /**
     * Method deleteQtMesComprovante
     * 
     */
    public void deleteQtMesComprovante()
    {
        this._has_qtMesComprovante= false;
    } //-- void deleteQtMesComprovante() 

    /**
     * Method deleteQtMesFaseRcadt
     * 
     */
    public void deleteQtMesFaseRcadt()
    {
        this._has_qtMesFaseRcadt= false;
    } //-- void deleteQtMesFaseRcadt() 

    /**
     * Method deleteQtMesRcadt
     * 
     */
    public void deleteQtMesRcadt()
    {
        this._has_qtMesRcadt= false;
    } //-- void deleteQtMesRcadt() 

    /**
     * Method deleteQtMesReajusteTarifa
     * 
     */
    public void deleteQtMesReajusteTarifa()
    {
        this._has_qtMesReajusteTarifa= false;
    } //-- void deleteQtMesReajusteTarifa() 

    /**
     * Method deleteQtViaAviso
     * 
     */
    public void deleteQtViaAviso()
    {
        this._has_qtViaAviso= false;
    } //-- void deleteQtViaAviso() 

    /**
     * Method deleteQtViaCobranca
     * 
     */
    public void deleteQtViaCobranca()
    {
        this._has_qtViaCobranca= false;
    } //-- void deleteQtViaCobranca() 

    /**
     * Method deleteQtViaComprovante
     * 
     */
    public void deleteQtViaComprovante()
    {
        this._has_qtViaComprovante= false;
    } //-- void deleteQtViaComprovante() 

    /**
     * Returns the value of field 'cdAGendaValorMenor'.
     * 
     * @return int
     * @return the value of field 'cdAGendaValorMenor'.
     */
    public int getCdAGendaValorMenor()
    {
        return this._cdAGendaValorMenor;
    } //-- int getCdAGendaValorMenor() 

    /**
     * Returns the value of field 'cdAcaoNaoVida'.
     * 
     * @return int
     * @return the value of field 'cdAcaoNaoVida'.
     */
    public int getCdAcaoNaoVida()
    {
        return this._cdAcaoNaoVida;
    } //-- int getCdAcaoNaoVida() 

    /**
     * Returns the value of field 'cdAcertoDadoRecadastro'.
     * 
     * @return int
     * @return the value of field 'cdAcertoDadoRecadastro'.
     */
    public int getCdAcertoDadoRecadastro()
    {
        return this._cdAcertoDadoRecadastro;
    } //-- int getCdAcertoDadoRecadastro() 

    /**
     * Returns the value of field 'cdAgendaDebitoVeiculo'.
     * 
     * @return int
     * @return the value of field 'cdAgendaDebitoVeiculo'.
     */
    public int getCdAgendaDebitoVeiculo()
    {
        return this._cdAgendaDebitoVeiculo;
    } //-- int getCdAgendaDebitoVeiculo() 

    /**
     * Returns the value of field 'cdAgendaPagamentoVencido'.
     * 
     * @return int
     * @return the value of field 'cdAgendaPagamentoVencido'.
     */
    public int getCdAgendaPagamentoVencido()
    {
        return this._cdAgendaPagamentoVencido;
    } //-- int getCdAgendaPagamentoVencido() 

    /**
     * Returns the value of field 'cdAgendaRastreabilidadeFilial'.
     * 
     * @return int
     * @return the value of field 'cdAgendaRastreabilidadeFilial'.
     */
    public int getCdAgendaRastreabilidadeFilial()
    {
        return this._cdAgendaRastreabilidadeFilial;
    } //-- int getCdAgendaRastreabilidadeFilial() 

    /**
     * Returns the value of field 'cdAgptoFormularioRecadastro'.
     * 
     * @return int
     * @return the value of field 'cdAgptoFormularioRecadastro'.
     */
    public int getCdAgptoFormularioRecadastro()
    {
        return this._cdAgptoFormularioRecadastro;
    } //-- int getCdAgptoFormularioRecadastro() 

    /**
     * Returns the value of field 'cdAgrupamentoAviso'.
     * 
     * @return int
     * @return the value of field 'cdAgrupamentoAviso'.
     */
    public int getCdAgrupamentoAviso()
    {
        return this._cdAgrupamentoAviso;
    } //-- int getCdAgrupamentoAviso() 

    /**
     * Returns the value of field 'cdAgrupamentoComprovado'.
     * 
     * @return int
     * @return the value of field 'cdAgrupamentoComprovado'.
     */
    public int getCdAgrupamentoComprovado()
    {
        return this._cdAgrupamentoComprovado;
    } //-- int getCdAgrupamentoComprovado() 

    /**
     * Returns the value of field 'cdAntecRecadastroBeneficiario'.
     * 
     * @return int
     * @return the value of field 'cdAntecRecadastroBeneficiario'.
     */
    public int getCdAntecRecadastroBeneficiario()
    {
        return this._cdAntecRecadastroBeneficiario;
    } //-- int getCdAntecRecadastroBeneficiario() 

    /**
     * Returns the value of field 'cdAreaReservada'.
     * 
     * @return int
     * @return the value of field 'cdAreaReservada'.
     */
    public int getCdAreaReservada()
    {
        return this._cdAreaReservada;
    } //-- int getCdAreaReservada() 

    /**
     * Returns the value of field 'cdBasaeRecadastroBeneficiario'.
     * 
     * @return int
     * @return the value of field 'cdBasaeRecadastroBeneficiario'.
     */
    public int getCdBasaeRecadastroBeneficiario()
    {
        return this._cdBasaeRecadastroBeneficiario;
    } //-- int getCdBasaeRecadastroBeneficiario() 

    /**
     * Returns the value of field 'cdBloqueioEmissaoPplta'.
     * 
     * @return int
     * @return the value of field 'cdBloqueioEmissaoPplta'.
     */
    public int getCdBloqueioEmissaoPplta()
    {
        return this._cdBloqueioEmissaoPplta;
    } //-- int getCdBloqueioEmissaoPplta() 

    /**
     * Returns the value of field 'cdCaptuTituloRegistro'.
     * 
     * @return int
     * @return the value of field 'cdCaptuTituloRegistro'.
     */
    public int getCdCaptuTituloRegistro()
    {
        return this._cdCaptuTituloRegistro;
    } //-- int getCdCaptuTituloRegistro() 

    /**
     * Returns the value of field 'cdCobrancaTarifa'.
     * 
     * @return int
     * @return the value of field 'cdCobrancaTarifa'.
     */
    public int getCdCobrancaTarifa()
    {
        return this._cdCobrancaTarifa;
    } //-- int getCdCobrancaTarifa() 

    /**
     * Returns the value of field 'cdConsDebitoVeiculo'.
     * 
     * @return int
     * @return the value of field 'cdConsDebitoVeiculo'.
     */
    public int getCdConsDebitoVeiculo()
    {
        return this._cdConsDebitoVeiculo;
    } //-- int getCdConsDebitoVeiculo() 

    /**
     * Returns the value of field 'cdConsEndereco'.
     * 
     * @return int
     * @return the value of field 'cdConsEndereco'.
     */
    public int getCdConsEndereco()
    {
        return this._cdConsEndereco;
    } //-- int getCdConsEndereco() 

    /**
     * Returns the value of field 'cdConsSaldoPagamento'.
     * 
     * @return int
     * @return the value of field 'cdConsSaldoPagamento'.
     */
    public int getCdConsSaldoPagamento()
    {
        return this._cdConsSaldoPagamento;
    } //-- int getCdConsSaldoPagamento() 

    /**
     * Returns the value of field 'cdConsultaSaldoValorSuperior'.
     * 
     * @return int
     * @return the value of field 'cdConsultaSaldoValorSuperior'.
     */
    public int getCdConsultaSaldoValorSuperior()
    {
        return this._cdConsultaSaldoValorSuperior;
    } //-- int getCdConsultaSaldoValorSuperior() 

    /**
     * Returns the value of field 'cdContgConsSaldo'.
     * 
     * @return int
     * @return the value of field 'cdContgConsSaldo'.
     */
    public int getCdContgConsSaldo()
    {
        return this._cdContgConsSaldo;
    } //-- int getCdContgConsSaldo() 

    /**
     * Returns the value of field 'cdCreditoNaoUtilizado'.
     * 
     * @return int
     * @return the value of field 'cdCreditoNaoUtilizado'.
     */
    public int getCdCreditoNaoUtilizado()
    {
        return this._cdCreditoNaoUtilizado;
    } //-- int getCdCreditoNaoUtilizado() 

    /**
     * Returns the value of field 'cdCriterioEnquaBeneficiario'.
     * 
     * @return int
     * @return the value of field 'cdCriterioEnquaBeneficiario'.
     */
    public int getCdCriterioEnquaBeneficiario()
    {
        return this._cdCriterioEnquaBeneficiario;
    } //-- int getCdCriterioEnquaBeneficiario() 

    /**
     * Returns the value of field 'cdCriterioEnquaRcadt'.
     * 
     * @return int
     * @return the value of field 'cdCriterioEnquaRcadt'.
     */
    public int getCdCriterioEnquaRcadt()
    {
        return this._cdCriterioEnquaRcadt;
    } //-- int getCdCriterioEnquaRcadt() 

    /**
     * Returns the value of field 'cdCriterioRstrbTitulo'.
     * 
     * @return int
     * @return the value of field 'cdCriterioRstrbTitulo'.
     */
    public int getCdCriterioRstrbTitulo()
    {
        return this._cdCriterioRstrbTitulo;
    } //-- int getCdCriterioRstrbTitulo() 

    /**
     * Returns the value of field 'cdCtciaEspecieBeneficiario'.
     * 
     * @return int
     * @return the value of field 'cdCtciaEspecieBeneficiario'.
     */
    public int getCdCtciaEspecieBeneficiario()
    {
        return this._cdCtciaEspecieBeneficiario;
    } //-- int getCdCtciaEspecieBeneficiario() 

    /**
     * Returns the value of field 'cdCtciaIdBeneficiario'.
     * 
     * @return int
     * @return the value of field 'cdCtciaIdBeneficiario'.
     */
    public int getCdCtciaIdBeneficiario()
    {
        return this._cdCtciaIdBeneficiario;
    } //-- int getCdCtciaIdBeneficiario() 

    /**
     * Returns the value of field 'cdCtciaInscricaoFavorecido'.
     * 
     * @return int
     * @return the value of field 'cdCtciaInscricaoFavorecido'.
     */
    public int getCdCtciaInscricaoFavorecido()
    {
        return this._cdCtciaInscricaoFavorecido;
    } //-- int getCdCtciaInscricaoFavorecido() 

    /**
     * Returns the value of field 'cdCtciaProprietarioVeiculo'.
     * 
     * @return int
     * @return the value of field 'cdCtciaProprietarioVeiculo'.
     */
    public int getCdCtciaProprietarioVeiculo()
    {
        return this._cdCtciaProprietarioVeiculo;
    } //-- int getCdCtciaProprietarioVeiculo() 

    /**
     * Returns the value of field 'cdDestinoAviso'.
     * 
     * @return int
     * @return the value of field 'cdDestinoAviso'.
     */
    public int getCdDestinoAviso()
    {
        return this._cdDestinoAviso;
    } //-- int getCdDestinoAviso() 

    /**
     * Returns the value of field 'cdDestinoComprovante'.
     * 
     * @return int
     * @return the value of field 'cdDestinoComprovante'.
     */
    public int getCdDestinoComprovante()
    {
        return this._cdDestinoComprovante;
    } //-- int getCdDestinoComprovante() 

    /**
     * Returns the value of field 'cdDestinoFormularioRcadt'.
     * 
     * @return int
     * @return the value of field 'cdDestinoFormularioRcadt'.
     */
    public int getCdDestinoFormularioRcadt()
    {
        return this._cdDestinoFormularioRcadt;
    } //-- int getCdDestinoFormularioRcadt() 

    /**
     * Returns the value of field 'cdDispzContaCredito'.
     * 
     * @return int
     * @return the value of field 'cdDispzContaCredito'.
     */
    public int getCdDispzContaCredito()
    {
        return this._cdDispzContaCredito;
    } //-- int getCdDispzContaCredito() 

    /**
     * Returns the value of field 'cdDispzDiversaoNao'.
     * 
     * @return int
     * @return the value of field 'cdDispzDiversaoNao'.
     */
    public int getCdDispzDiversaoNao()
    {
        return this._cdDispzDiversaoNao;
    } //-- int getCdDispzDiversaoNao() 

    /**
     * Returns the value of field 'cdDispzDiversosCrrtt'.
     * 
     * @return int
     * @return the value of field 'cdDispzDiversosCrrtt'.
     */
    public int getCdDispzDiversosCrrtt()
    {
        return this._cdDispzDiversosCrrtt;
    } //-- int getCdDispzDiversosCrrtt() 

    /**
     * Returns the value of field 'cdDispzSalarialCrrtt'.
     * 
     * @return int
     * @return the value of field 'cdDispzSalarialCrrtt'.
     */
    public int getCdDispzSalarialCrrtt()
    {
        return this._cdDispzSalarialCrrtt;
    } //-- int getCdDispzSalarialCrrtt() 

    /**
     * Returns the value of field 'cdDispzSalarialNao'.
     * 
     * @return int
     * @return the value of field 'cdDispzSalarialNao'.
     */
    public int getCdDispzSalarialNao()
    {
        return this._cdDispzSalarialNao;
    } //-- int getCdDispzSalarialNao() 

    /**
     * Returns the value of field 'cdEnvelopeAberto'.
     * 
     * @return int
     * @return the value of field 'cdEnvelopeAberto'.
     */
    public int getCdEnvelopeAberto()
    {
        return this._cdEnvelopeAberto;
    } //-- int getCdEnvelopeAberto() 

    /**
     * Returns the value of field 'cdFavorecidoConsPgto'.
     * 
     * @return int
     * @return the value of field 'cdFavorecidoConsPgto'.
     */
    public int getCdFavorecidoConsPgto()
    {
        return this._cdFavorecidoConsPgto;
    } //-- int getCdFavorecidoConsPgto() 

    /**
     * Returns the value of field 'cdFormaAutrzPgto'.
     * 
     * @return int
     * @return the value of field 'cdFormaAutrzPgto'.
     */
    public int getCdFormaAutrzPgto()
    {
        return this._cdFormaAutrzPgto;
    } //-- int getCdFormaAutrzPgto() 

    /**
     * Returns the value of field 'cdFormaEnvioPagamento'.
     * 
     * @return int
     * @return the value of field 'cdFormaEnvioPagamento'.
     */
    public int getCdFormaEnvioPagamento()
    {
        return this._cdFormaEnvioPagamento;
    } //-- int getCdFormaEnvioPagamento() 

    /**
     * Returns the value of field 'cdFormaExpirCredito'.
     * 
     * @return int
     * @return the value of field 'cdFormaExpirCredito'.
     */
    public int getCdFormaExpirCredito()
    {
        return this._cdFormaExpirCredito;
    } //-- int getCdFormaExpirCredito() 

    /**
     * Returns the value of field 'cdFormaManutencao'.
     * 
     * @return int
     * @return the value of field 'cdFormaManutencao'.
     */
    public int getCdFormaManutencao()
    {
        return this._cdFormaManutencao;
    } //-- int getCdFormaManutencao() 

    /**
     * Returns the value of field 'cdFormularioContratoCliente'.
     * 
     * @return int
     * @return the value of field 'cdFormularioContratoCliente'.
     */
    public int getCdFormularioContratoCliente()
    {
        return this._cdFormularioContratoCliente;
    } //-- int getCdFormularioContratoCliente() 

    /**
     * Returns the value of field 'cdFrasePreCadastro'.
     * 
     * @return int
     * @return the value of field 'cdFrasePreCadastro'.
     */
    public int getCdFrasePreCadastro()
    {
        return this._cdFrasePreCadastro;
    } //-- int getCdFrasePreCadastro() 

    /**
     * Returns the value of field 'cdIndLancamentoPersonalizado'.
     * 
     * @return int
     * @return the value of field 'cdIndLancamentoPersonalizado'.
     */
    public int getCdIndLancamentoPersonalizado()
    {
        return this._cdIndLancamentoPersonalizado;
    } //-- int getCdIndLancamentoPersonalizado() 

    /**
     * Returns the value of field 'cdIndicadorAdesaoSacador'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorAdesaoSacador'.
     */
    public int getCdIndicadorAdesaoSacador()
    {
        return this._cdIndicadorAdesaoSacador;
    } //-- int getCdIndicadorAdesaoSacador() 

    /**
     * Returns the value of field 'cdIndicadorAgendaTitulo'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorAgendaTitulo'.
     */
    public int getCdIndicadorAgendaTitulo()
    {
        return this._cdIndicadorAgendaTitulo;
    } //-- int getCdIndicadorAgendaTitulo() 

    /**
     * Returns the value of field 'cdIndicadorAutrzCliente'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorAutrzCliente'.
     */
    public int getCdIndicadorAutrzCliente()
    {
        return this._cdIndicadorAutrzCliente;
    } //-- int getCdIndicadorAutrzCliente() 

    /**
     * Returns the value of field 'cdIndicadorAutrzCompl'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorAutrzCompl'.
     */
    public int getCdIndicadorAutrzCompl()
    {
        return this._cdIndicadorAutrzCompl;
    } //-- int getCdIndicadorAutrzCompl() 

    /**
     * Returns the value of field 'cdIndicadorBancoPostal'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorBancoPostal'.
     */
    public int getCdIndicadorBancoPostal()
    {
        return this._cdIndicadorBancoPostal;
    } //-- int getCdIndicadorBancoPostal() 

    /**
     * Returns the value of field 'cdIndicadorCadOrg'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorCadOrg'.
     */
    public int getCdIndicadorCadOrg()
    {
        return this._cdIndicadorCadOrg;
    } //-- int getCdIndicadorCadOrg() 

    /**
     * Returns the value of field 'cdIndicadorCadProcd'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorCadProcd'.
     */
    public int getCdIndicadorCadProcd()
    {
        return this._cdIndicadorCadProcd;
    } //-- int getCdIndicadorCadProcd() 

    /**
     * Returns the value of field 'cdIndicadorCartaoSalarial'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorCartaoSalarial'.
     */
    public int getCdIndicadorCartaoSalarial()
    {
        return this._cdIndicadorCartaoSalarial;
    } //-- int getCdIndicadorCartaoSalarial() 

    /**
     * Returns the value of field 'cdIndicadorEconomicoReajuste'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorEconomicoReajuste'.
     */
    public int getCdIndicadorEconomicoReajuste()
    {
        return this._cdIndicadorEconomicoReajuste;
    } //-- int getCdIndicadorEconomicoReajuste() 

    /**
     * Returns the value of field 'cdIndicadorEmissaoAviso'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorEmissaoAviso'.
     */
    public int getCdIndicadorEmissaoAviso()
    {
        return this._cdIndicadorEmissaoAviso;
    } //-- int getCdIndicadorEmissaoAviso() 

    /**
     * Returns the value of field 'cdIndicadorExpirCredito'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorExpirCredito'.
     */
    public int getCdIndicadorExpirCredito()
    {
        return this._cdIndicadorExpirCredito;
    } //-- int getCdIndicadorExpirCredito() 

    /**
     * Returns the value of field 'cdIndicadorLctoPgmd'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorLctoPgmd'.
     */
    public int getCdIndicadorLctoPgmd()
    {
        return this._cdIndicadorLctoPgmd;
    } //-- int getCdIndicadorLctoPgmd() 

    /**
     * Returns the value of field 'cdIndicadorListaDebito'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorListaDebito'.
     */
    public int getCdIndicadorListaDebito()
    {
        return this._cdIndicadorListaDebito;
    } //-- int getCdIndicadorListaDebito() 

    /**
     * Returns the value of field 'cdIndicadorMensagemPerso'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorMensagemPerso'.
     */
    public int getCdIndicadorMensagemPerso()
    {
        return this._cdIndicadorMensagemPerso;
    } //-- int getCdIndicadorMensagemPerso() 

    /**
     * Returns the value of field 'cdIndicadorRetornoInternet'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorRetornoInternet'.
     */
    public int getCdIndicadorRetornoInternet()
    {
        return this._cdIndicadorRetornoInternet;
    } //-- int getCdIndicadorRetornoInternet() 

    /**
     * Returns the value of field 'cdLancamentoFuturoCredito'.
     * 
     * @return int
     * @return the value of field 'cdLancamentoFuturoCredito'.
     */
    public int getCdLancamentoFuturoCredito()
    {
        return this._cdLancamentoFuturoCredito;
    } //-- int getCdLancamentoFuturoCredito() 

    /**
     * Returns the value of field 'cdLancamentoFuturoDebito'.
     * 
     * @return int
     * @return the value of field 'cdLancamentoFuturoDebito'.
     */
    public int getCdLancamentoFuturoDebito()
    {
        return this._cdLancamentoFuturoDebito;
    } //-- int getCdLancamentoFuturoDebito() 

    /**
     * Returns the value of field 'cdLiberacaoLoteProcs'.
     * 
     * @return int
     * @return the value of field 'cdLiberacaoLoteProcs'.
     */
    public int getCdLiberacaoLoteProcs()
    {
        return this._cdLiberacaoLoteProcs;
    } //-- int getCdLiberacaoLoteProcs() 

    /**
     * Returns the value of field 'cdManutencaoBaseRcadt'.
     * 
     * @return int
     * @return the value of field 'cdManutencaoBaseRcadt'.
     */
    public int getCdManutencaoBaseRcadt()
    {
        return this._cdManutencaoBaseRcadt;
    } //-- int getCdManutencaoBaseRcadt() 

    /**
     * Returns the value of field 'cdMeioPagamentoCredito'.
     * 
     * @return int
     * @return the value of field 'cdMeioPagamentoCredito'.
     */
    public int getCdMeioPagamentoCredito()
    {
        return this._cdMeioPagamentoCredito;
    } //-- int getCdMeioPagamentoCredito() 

    /**
     * Returns the value of field 'cdMensagemRcadtMidia'.
     * 
     * @return int
     * @return the value of field 'cdMensagemRcadtMidia'.
     */
    public int getCdMensagemRcadtMidia()
    {
        return this._cdMensagemRcadtMidia;
    } //-- int getCdMensagemRcadtMidia() 

    /**
     * Returns the value of field 'cdMidiaDisponivel'.
     * 
     * @return int
     * @return the value of field 'cdMidiaDisponivel'.
     */
    public int getCdMidiaDisponivel()
    {
        return this._cdMidiaDisponivel;
    } //-- int getCdMidiaDisponivel() 

    /**
     * Returns the value of field 'cdMidiaMensagemRcadt'.
     * 
     * @return int
     * @return the value of field 'cdMidiaMensagemRcadt'.
     */
    public int getCdMidiaMensagemRcadt()
    {
        return this._cdMidiaMensagemRcadt;
    } //-- int getCdMidiaMensagemRcadt() 

    /**
     * Returns the value of field 'cdMomenAvisoRcadt'.
     * 
     * @return int
     * @return the value of field 'cdMomenAvisoRcadt'.
     */
    public int getCdMomenAvisoRcadt()
    {
        return this._cdMomenAvisoRcadt;
    } //-- int getCdMomenAvisoRcadt() 

    /**
     * Returns the value of field 'cdMomenCreditoEfetivo'.
     * 
     * @return int
     * @return the value of field 'cdMomenCreditoEfetivo'.
     */
    public int getCdMomenCreditoEfetivo()
    {
        return this._cdMomenCreditoEfetivo;
    } //-- int getCdMomenCreditoEfetivo() 

    /**
     * Returns the value of field 'cdMomenDebitoPagamento'.
     * 
     * @return int
     * @return the value of field 'cdMomenDebitoPagamento'.
     */
    public int getCdMomenDebitoPagamento()
    {
        return this._cdMomenDebitoPagamento;
    } //-- int getCdMomenDebitoPagamento() 

    /**
     * Returns the value of field 'cdMomenFormulaRcadt'.
     * 
     * @return int
     * @return the value of field 'cdMomenFormulaRcadt'.
     */
    public int getCdMomenFormulaRcadt()
    {
        return this._cdMomenFormulaRcadt;
    } //-- int getCdMomenFormulaRcadt() 

    /**
     * Returns the value of field 'cdMomenProcmPagamento'.
     * 
     * @return int
     * @return the value of field 'cdMomenProcmPagamento'.
     */
    public int getCdMomenProcmPagamento()
    {
        return this._cdMomenProcmPagamento;
    } //-- int getCdMomenProcmPagamento() 

    /**
     * Returns the value of field 'cdNaturezaOperacaoPagamento'.
     * 
     * @return int
     * @return the value of field 'cdNaturezaOperacaoPagamento'.
     */
    public int getCdNaturezaOperacaoPagamento()
    {
        return this._cdNaturezaOperacaoPagamento;
    } //-- int getCdNaturezaOperacaoPagamento() 

    /**
     * Returns the value of field 'cdPagamentoNaoUtil'.
     * 
     * @return int
     * @return the value of field 'cdPagamentoNaoUtil'.
     */
    public int getCdPagamentoNaoUtil()
    {
        return this._cdPagamentoNaoUtil;
    } //-- int getCdPagamentoNaoUtil() 

    /**
     * Returns the value of field
     * 'cdPercentualIndiceReajusteTarifa'.
     * 
     * @return BigDecimal
     * @return the value of field 'cdPercentualIndiceReajusteTarifa'
     */
    public java.math.BigDecimal getCdPercentualIndiceReajusteTarifa()
    {
        return this._cdPercentualIndiceReajusteTarifa;
    } //-- java.math.BigDecimal getCdPercentualIndiceReajusteTarifa() 

    /**
     * Returns the value of field 'cdPercentualMaximoInconLote'.
     * 
     * @return int
     * @return the value of field 'cdPercentualMaximoInconLote'.
     */
    public int getCdPercentualMaximoInconLote()
    {
        return this._cdPercentualMaximoInconLote;
    } //-- int getCdPercentualMaximoInconLote() 

    /**
     * Returns the value of field 'cdPercentualReducaoTarifaCatlg'.
     * 
     * @return BigDecimal
     * @return the value of field 'cdPercentualReducaoTarifaCatlg'.
     */
    public java.math.BigDecimal getCdPercentualReducaoTarifaCatlg()
    {
        return this._cdPercentualReducaoTarifaCatlg;
    } //-- java.math.BigDecimal getCdPercentualReducaoTarifaCatlg() 

    /**
     * Returns the value of field 'cdPerdcAviso'.
     * 
     * @return int
     * @return the value of field 'cdPerdcAviso'.
     */
    public int getCdPerdcAviso()
    {
        return this._cdPerdcAviso;
    } //-- int getCdPerdcAviso() 

    /**
     * Returns the value of field 'cdPerdcCobrancaTarifa'.
     * 
     * @return int
     * @return the value of field 'cdPerdcCobrancaTarifa'.
     */
    public int getCdPerdcCobrancaTarifa()
    {
        return this._cdPerdcCobrancaTarifa;
    } //-- int getCdPerdcCobrancaTarifa() 

    /**
     * Returns the value of field 'cdPerdcComprovante'.
     * 
     * @return int
     * @return the value of field 'cdPerdcComprovante'.
     */
    public int getCdPerdcComprovante()
    {
        return this._cdPerdcComprovante;
    } //-- int getCdPerdcComprovante() 

    /**
     * Returns the value of field 'cdPerdcConsVeiculo'.
     * 
     * @return int
     * @return the value of field 'cdPerdcConsVeiculo'.
     */
    public int getCdPerdcConsVeiculo()
    {
        return this._cdPerdcConsVeiculo;
    } //-- int getCdPerdcConsVeiculo() 

    /**
     * Returns the value of field 'cdPerdcEnvioRemessa'.
     * 
     * @return int
     * @return the value of field 'cdPerdcEnvioRemessa'.
     */
    public int getCdPerdcEnvioRemessa()
    {
        return this._cdPerdcEnvioRemessa;
    } //-- int getCdPerdcEnvioRemessa() 

    /**
     * Returns the value of field 'cdPerdcManutencaoProcd'.
     * 
     * @return int
     * @return the value of field 'cdPerdcManutencaoProcd'.
     */
    public int getCdPerdcManutencaoProcd()
    {
        return this._cdPerdcManutencaoProcd;
    } //-- int getCdPerdcManutencaoProcd() 

    /**
     * Returns the value of field 'cdPermissaoDebitoOnline'.
     * 
     * @return int
     * @return the value of field 'cdPermissaoDebitoOnline'.
     */
    public int getCdPermissaoDebitoOnline()
    {
        return this._cdPermissaoDebitoOnline;
    } //-- int getCdPermissaoDebitoOnline() 

    /**
     * Returns the value of field 'cdPrincipalEnquaRcadt'.
     * 
     * @return int
     * @return the value of field 'cdPrincipalEnquaRcadt'.
     */
    public int getCdPrincipalEnquaRcadt()
    {
        return this._cdPrincipalEnquaRcadt;
    } //-- int getCdPrincipalEnquaRcadt() 

    /**
     * Returns the value of field 'cdPriorEfetivacaoPagamento'.
     * 
     * @return int
     * @return the value of field 'cdPriorEfetivacaoPagamento'.
     */
    public int getCdPriorEfetivacaoPagamento()
    {
        return this._cdPriorEfetivacaoPagamento;
    } //-- int getCdPriorEfetivacaoPagamento() 

    /**
     * Returns the value of field 'cdRastreabNotaFiscal'.
     * 
     * @return int
     * @return the value of field 'cdRastreabNotaFiscal'.
     */
    public int getCdRastreabNotaFiscal()
    {
        return this._cdRastreabNotaFiscal;
    } //-- int getCdRastreabNotaFiscal() 

    /**
     * Returns the value of field 'cdRastreabTituloTerc'.
     * 
     * @return int
     * @return the value of field 'cdRastreabTituloTerc'.
     */
    public int getCdRastreabTituloTerc()
    {
        return this._cdRastreabTituloTerc;
    } //-- int getCdRastreabTituloTerc() 

    /**
     * Returns the value of field 'cdRejeiAgendaLote'.
     * 
     * @return int
     * @return the value of field 'cdRejeiAgendaLote'.
     */
    public int getCdRejeiAgendaLote()
    {
        return this._cdRejeiAgendaLote;
    } //-- int getCdRejeiAgendaLote() 

    /**
     * Returns the value of field 'cdRejeiEfetivacaoLote'.
     * 
     * @return int
     * @return the value of field 'cdRejeiEfetivacaoLote'.
     */
    public int getCdRejeiEfetivacaoLote()
    {
        return this._cdRejeiEfetivacaoLote;
    } //-- int getCdRejeiEfetivacaoLote() 

    /**
     * Returns the value of field 'cdRejeiLote'.
     * 
     * @return int
     * @return the value of field 'cdRejeiLote'.
     */
    public int getCdRejeiLote()
    {
        return this._cdRejeiLote;
    } //-- int getCdRejeiLote() 

    /**
     * Returns the value of field 'cdTipoCargaRcadt'.
     * 
     * @return int
     * @return the value of field 'cdTipoCargaRcadt'.
     */
    public int getCdTipoCargaRcadt()
    {
        return this._cdTipoCargaRcadt;
    } //-- int getCdTipoCargaRcadt() 

    /**
     * Returns the value of field 'cdTipoCataoSalarial'.
     * 
     * @return int
     * @return the value of field 'cdTipoCataoSalarial'.
     */
    public int getCdTipoCataoSalarial()
    {
        return this._cdTipoCataoSalarial;
    } //-- int getCdTipoCataoSalarial() 

    /**
     * Returns the value of field 'cdTipoConsistenciaLista'.
     * 
     * @return int
     * @return the value of field 'cdTipoConsistenciaLista'.
     */
    public int getCdTipoConsistenciaLista()
    {
        return this._cdTipoConsistenciaLista;
    } //-- int getCdTipoConsistenciaLista() 

    /**
     * Returns the value of field 'cdTipoConsultaComprovante'.
     * 
     * @return int
     * @return the value of field 'cdTipoConsultaComprovante'.
     */
    public int getCdTipoConsultaComprovante()
    {
        return this._cdTipoConsultaComprovante;
    } //-- int getCdTipoConsultaComprovante() 

    /**
     * Returns the value of field 'cdTipoContaFavorecido'.
     * 
     * @return int
     * @return the value of field 'cdTipoContaFavorecido'.
     */
    public int getCdTipoContaFavorecido()
    {
        return this._cdTipoContaFavorecido;
    } //-- int getCdTipoContaFavorecido() 

    /**
     * Returns the value of field 'cdTipoDataFloat'.
     * 
     * @return int
     * @return the value of field 'cdTipoDataFloat'.
     */
    public int getCdTipoDataFloat()
    {
        return this._cdTipoDataFloat;
    } //-- int getCdTipoDataFloat() 

    /**
     * Returns the value of field 'cdTipoDivergenciaVeiculo'.
     * 
     * @return int
     * @return the value of field 'cdTipoDivergenciaVeiculo'.
     */
    public int getCdTipoDivergenciaVeiculo()
    {
        return this._cdTipoDivergenciaVeiculo;
    } //-- int getCdTipoDivergenciaVeiculo() 

    /**
     * Returns the value of field 'cdTipoFormacaoLista'.
     * 
     * @return int
     * @return the value of field 'cdTipoFormacaoLista'.
     */
    public int getCdTipoFormacaoLista()
    {
        return this._cdTipoFormacaoLista;
    } //-- int getCdTipoFormacaoLista() 

    /**
     * Returns the value of field 'cdTipoIdBenef'.
     * 
     * @return int
     * @return the value of field 'cdTipoIdBenef'.
     */
    public int getCdTipoIdBenef()
    {
        return this._cdTipoIdBenef;
    } //-- int getCdTipoIdBenef() 

    /**
     * Returns the value of field 'cdTipoIsncricaoFavorecido'.
     * 
     * @return int
     * @return the value of field 'cdTipoIsncricaoFavorecido'.
     */
    public int getCdTipoIsncricaoFavorecido()
    {
        return this._cdTipoIsncricaoFavorecido;
    } //-- int getCdTipoIsncricaoFavorecido() 

    /**
     * Returns the value of field 'cdTipoLayoutArquivo'.
     * 
     * @return int
     * @return the value of field 'cdTipoLayoutArquivo'.
     */
    public int getCdTipoLayoutArquivo()
    {
        return this._cdTipoLayoutArquivo;
    } //-- int getCdTipoLayoutArquivo() 

    /**
     * Returns the value of field 'cdTipoReajusteTarifa'.
     * 
     * @return int
     * @return the value of field 'cdTipoReajusteTarifa'.
     */
    public int getCdTipoReajusteTarifa()
    {
        return this._cdTipoReajusteTarifa;
    } //-- int getCdTipoReajusteTarifa() 

    /**
     * Returns the value of field 'cdTratoContaTransf'.
     * 
     * @return int
     * @return the value of field 'cdTratoContaTransf'.
     */
    public int getCdTratoContaTransf()
    {
        return this._cdTratoContaTransf;
    } //-- int getCdTratoContaTransf() 

    /**
     * Returns the value of field 'cdUtilzFavorecidoCtrl'.
     * 
     * @return int
     * @return the value of field 'cdUtilzFavorecidoCtrl'.
     */
    public int getCdUtilzFavorecidoCtrl()
    {
        return this._cdUtilzFavorecidoCtrl;
    } //-- int getCdUtilzFavorecidoCtrl() 

    /**
     * Returns the value of field 'cdValidacaoNomeFavorecido'.
     * 
     * @return int
     * @return the value of field 'cdValidacaoNomeFavorecido'.
     */
    public int getCdValidacaoNomeFavorecido()
    {
        return this._cdValidacaoNomeFavorecido;
    } //-- int getCdValidacaoNomeFavorecido() 

    /**
     * Returns the value of field 'codMensagem'.
     * 
     * @return String
     * @return the value of field 'codMensagem'.
     */
    public java.lang.String getCodMensagem()
    {
        return this._codMensagem;
    } //-- java.lang.String getCodMensagem() 

    /**
     * Returns the value of field 'mensagem'.
     * 
     * @return String
     * @return the value of field 'mensagem'.
     */
    public java.lang.String getMensagem()
    {
        return this._mensagem;
    } //-- java.lang.String getMensagem() 

    /**
     * Returns the value of field 'nrFechamentoApurcTarifa'.
     * 
     * @return int
     * @return the value of field 'nrFechamentoApurcTarifa'.
     */
    public int getNrFechamentoApurcTarifa()
    {
        return this._nrFechamentoApurcTarifa;
    } //-- int getNrFechamentoApurcTarifa() 

    /**
     * Returns the value of field 'qtAntecedencia'.
     * 
     * @return int
     * @return the value of field 'qtAntecedencia'.
     */
    public int getQtAntecedencia()
    {
        return this._qtAntecedencia;
    } //-- int getQtAntecedencia() 

    /**
     * Returns the value of field 'qtAnteriorVencimentoCompv'.
     * 
     * @return int
     * @return the value of field 'qtAnteriorVencimentoCompv'.
     */
    public int getQtAnteriorVencimentoCompv()
    {
        return this._qtAnteriorVencimentoCompv;
    } //-- int getQtAnteriorVencimentoCompv() 

    /**
     * Returns the value of field 'qtDiaCobrancaTarifa'.
     * 
     * @return int
     * @return the value of field 'qtDiaCobrancaTarifa'.
     */
    public int getQtDiaCobrancaTarifa()
    {
        return this._qtDiaCobrancaTarifa;
    } //-- int getQtDiaCobrancaTarifa() 

    /**
     * Returns the value of field 'qtDiaExpiracao'.
     * 
     * @return int
     * @return the value of field 'qtDiaExpiracao'.
     */
    public int getQtDiaExpiracao()
    {
        return this._qtDiaExpiracao;
    } //-- int getQtDiaExpiracao() 

    /**
     * Returns the value of field 'qtDiaFloatPagamento'.
     * 
     * @return int
     * @return the value of field 'qtDiaFloatPagamento'.
     */
    public int getQtDiaFloatPagamento()
    {
        return this._qtDiaFloatPagamento;
    } //-- int getQtDiaFloatPagamento() 

    /**
     * Returns the value of field 'qtDiaInativFavorecido'.
     * 
     * @return int
     * @return the value of field 'qtDiaInativFavorecido'.
     */
    public int getQtDiaInativFavorecido()
    {
        return this._qtDiaInativFavorecido;
    } //-- int getQtDiaInativFavorecido() 

    /**
     * Returns the value of field 'qtDiaRepiqCons'.
     * 
     * @return int
     * @return the value of field 'qtDiaRepiqCons'.
     */
    public int getQtDiaRepiqCons()
    {
        return this._qtDiaRepiqCons;
    } //-- int getQtDiaRepiqCons() 

    /**
     * Returns the value of field 'qtEtapaRcadtBeneficiario'.
     * 
     * @return int
     * @return the value of field 'qtEtapaRcadtBeneficiario'.
     */
    public int getQtEtapaRcadtBeneficiario()
    {
        return this._qtEtapaRcadtBeneficiario;
    } //-- int getQtEtapaRcadtBeneficiario() 

    /**
     * Returns the value of field 'qtFaseRcadtBenefiario'.
     * 
     * @return int
     * @return the value of field 'qtFaseRcadtBenefiario'.
     */
    public int getQtFaseRcadtBenefiario()
    {
        return this._qtFaseRcadtBenefiario;
    } //-- int getQtFaseRcadtBenefiario() 

    /**
     * Returns the value of field 'qtLimiteLinhas'.
     * 
     * @return int
     * @return the value of field 'qtLimiteLinhas'.
     */
    public int getQtLimiteLinhas()
    {
        return this._qtLimiteLinhas;
    } //-- int getQtLimiteLinhas() 

    /**
     * Returns the value of field 'qtLimiteSolicitacaoCatao'.
     * 
     * @return int
     * @return the value of field 'qtLimiteSolicitacaoCatao'.
     */
    public int getQtLimiteSolicitacaoCatao()
    {
        return this._qtLimiteSolicitacaoCatao;
    } //-- int getQtLimiteSolicitacaoCatao() 

    /**
     * Returns the value of field 'qtMaximaInconLote'.
     * 
     * @return int
     * @return the value of field 'qtMaximaInconLote'.
     */
    public int getQtMaximaInconLote()
    {
        return this._qtMaximaInconLote;
    } //-- int getQtMaximaInconLote() 

    /**
     * Returns the value of field 'qtMaximaTituloVencido'.
     * 
     * @return int
     * @return the value of field 'qtMaximaTituloVencido'.
     */
    public int getQtMaximaTituloVencido()
    {
        return this._qtMaximaTituloVencido;
    } //-- int getQtMaximaTituloVencido() 

    /**
     * Returns the value of field 'qtMesComprovante'.
     * 
     * @return int
     * @return the value of field 'qtMesComprovante'.
     */
    public int getQtMesComprovante()
    {
        return this._qtMesComprovante;
    } //-- int getQtMesComprovante() 

    /**
     * Returns the value of field 'qtMesFaseRcadt'.
     * 
     * @return int
     * @return the value of field 'qtMesFaseRcadt'.
     */
    public int getQtMesFaseRcadt()
    {
        return this._qtMesFaseRcadt;
    } //-- int getQtMesFaseRcadt() 

    /**
     * Returns the value of field 'qtMesRcadt'.
     * 
     * @return int
     * @return the value of field 'qtMesRcadt'.
     */
    public int getQtMesRcadt()
    {
        return this._qtMesRcadt;
    } //-- int getQtMesRcadt() 

    /**
     * Returns the value of field 'qtMesReajusteTarifa'.
     * 
     * @return int
     * @return the value of field 'qtMesReajusteTarifa'.
     */
    public int getQtMesReajusteTarifa()
    {
        return this._qtMesReajusteTarifa;
    } //-- int getQtMesReajusteTarifa() 

    /**
     * Returns the value of field 'qtViaAviso'.
     * 
     * @return int
     * @return the value of field 'qtViaAviso'.
     */
    public int getQtViaAviso()
    {
        return this._qtViaAviso;
    } //-- int getQtViaAviso() 

    /**
     * Returns the value of field 'qtViaCobranca'.
     * 
     * @return int
     * @return the value of field 'qtViaCobranca'.
     */
    public int getQtViaCobranca()
    {
        return this._qtViaCobranca;
    } //-- int getQtViaCobranca() 

    /**
     * Returns the value of field 'qtViaComprovante'.
     * 
     * @return int
     * @return the value of field 'qtViaComprovante'.
     */
    public int getQtViaComprovante()
    {
        return this._qtViaComprovante;
    } //-- int getQtViaComprovante() 

    /**
     * Returns the value of field 'vlFavorecidoNaoCadtr'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlFavorecidoNaoCadtr'.
     */
    public java.math.BigDecimal getVlFavorecidoNaoCadtr()
    {
        return this._vlFavorecidoNaoCadtr;
    } //-- java.math.BigDecimal getVlFavorecidoNaoCadtr() 

    /**
     * Returns the value of field 'vlLimiteDiaPagamento'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlLimiteDiaPagamento'.
     */
    public java.math.BigDecimal getVlLimiteDiaPagamento()
    {
        return this._vlLimiteDiaPagamento;
    } //-- java.math.BigDecimal getVlLimiteDiaPagamento() 

    /**
     * Returns the value of field 'vlLimiteIndvdPagamento'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlLimiteIndvdPagamento'.
     */
    public java.math.BigDecimal getVlLimiteIndvdPagamento()
    {
        return this._vlLimiteIndvdPagamento;
    } //-- java.math.BigDecimal getVlLimiteIndvdPagamento() 

    /**
     * Method hasCdAGendaValorMenor
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAGendaValorMenor()
    {
        return this._has_cdAGendaValorMenor;
    } //-- boolean hasCdAGendaValorMenor() 

    /**
     * Method hasCdAcaoNaoVida
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAcaoNaoVida()
    {
        return this._has_cdAcaoNaoVida;
    } //-- boolean hasCdAcaoNaoVida() 

    /**
     * Method hasCdAcertoDadoRecadastro
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAcertoDadoRecadastro()
    {
        return this._has_cdAcertoDadoRecadastro;
    } //-- boolean hasCdAcertoDadoRecadastro() 

    /**
     * Method hasCdAgendaDebitoVeiculo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgendaDebitoVeiculo()
    {
        return this._has_cdAgendaDebitoVeiculo;
    } //-- boolean hasCdAgendaDebitoVeiculo() 

    /**
     * Method hasCdAgendaPagamentoVencido
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgendaPagamentoVencido()
    {
        return this._has_cdAgendaPagamentoVencido;
    } //-- boolean hasCdAgendaPagamentoVencido() 

    /**
     * Method hasCdAgendaRastreabilidadeFilial
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgendaRastreabilidadeFilial()
    {
        return this._has_cdAgendaRastreabilidadeFilial;
    } //-- boolean hasCdAgendaRastreabilidadeFilial() 

    /**
     * Method hasCdAgptoFormularioRecadastro
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgptoFormularioRecadastro()
    {
        return this._has_cdAgptoFormularioRecadastro;
    } //-- boolean hasCdAgptoFormularioRecadastro() 

    /**
     * Method hasCdAgrupamentoAviso
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgrupamentoAviso()
    {
        return this._has_cdAgrupamentoAviso;
    } //-- boolean hasCdAgrupamentoAviso() 

    /**
     * Method hasCdAgrupamentoComprovado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgrupamentoComprovado()
    {
        return this._has_cdAgrupamentoComprovado;
    } //-- boolean hasCdAgrupamentoComprovado() 

    /**
     * Method hasCdAntecRecadastroBeneficiario
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAntecRecadastroBeneficiario()
    {
        return this._has_cdAntecRecadastroBeneficiario;
    } //-- boolean hasCdAntecRecadastroBeneficiario() 

    /**
     * Method hasCdAreaReservada
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAreaReservada()
    {
        return this._has_cdAreaReservada;
    } //-- boolean hasCdAreaReservada() 

    /**
     * Method hasCdBasaeRecadastroBeneficiario
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdBasaeRecadastroBeneficiario()
    {
        return this._has_cdBasaeRecadastroBeneficiario;
    } //-- boolean hasCdBasaeRecadastroBeneficiario() 

    /**
     * Method hasCdBloqueioEmissaoPplta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdBloqueioEmissaoPplta()
    {
        return this._has_cdBloqueioEmissaoPplta;
    } //-- boolean hasCdBloqueioEmissaoPplta() 

    /**
     * Method hasCdCaptuTituloRegistro
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCaptuTituloRegistro()
    {
        return this._has_cdCaptuTituloRegistro;
    } //-- boolean hasCdCaptuTituloRegistro() 

    /**
     * Method hasCdCobrancaTarifa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCobrancaTarifa()
    {
        return this._has_cdCobrancaTarifa;
    } //-- boolean hasCdCobrancaTarifa() 

    /**
     * Method hasCdConsDebitoVeiculo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdConsDebitoVeiculo()
    {
        return this._has_cdConsDebitoVeiculo;
    } //-- boolean hasCdConsDebitoVeiculo() 

    /**
     * Method hasCdConsEndereco
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdConsEndereco()
    {
        return this._has_cdConsEndereco;
    } //-- boolean hasCdConsEndereco() 

    /**
     * Method hasCdConsSaldoPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdConsSaldoPagamento()
    {
        return this._has_cdConsSaldoPagamento;
    } //-- boolean hasCdConsSaldoPagamento() 

    /**
     * Method hasCdConsultaSaldoValorSuperior
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdConsultaSaldoValorSuperior()
    {
        return this._has_cdConsultaSaldoValorSuperior;
    } //-- boolean hasCdConsultaSaldoValorSuperior() 

    /**
     * Method hasCdContgConsSaldo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdContgConsSaldo()
    {
        return this._has_cdContgConsSaldo;
    } //-- boolean hasCdContgConsSaldo() 

    /**
     * Method hasCdCreditoNaoUtilizado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCreditoNaoUtilizado()
    {
        return this._has_cdCreditoNaoUtilizado;
    } //-- boolean hasCdCreditoNaoUtilizado() 

    /**
     * Method hasCdCriterioEnquaBeneficiario
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCriterioEnquaBeneficiario()
    {
        return this._has_cdCriterioEnquaBeneficiario;
    } //-- boolean hasCdCriterioEnquaBeneficiario() 

    /**
     * Method hasCdCriterioEnquaRcadt
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCriterioEnquaRcadt()
    {
        return this._has_cdCriterioEnquaRcadt;
    } //-- boolean hasCdCriterioEnquaRcadt() 

    /**
     * Method hasCdCriterioRstrbTitulo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCriterioRstrbTitulo()
    {
        return this._has_cdCriterioRstrbTitulo;
    } //-- boolean hasCdCriterioRstrbTitulo() 

    /**
     * Method hasCdCtciaEspecieBeneficiario
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCtciaEspecieBeneficiario()
    {
        return this._has_cdCtciaEspecieBeneficiario;
    } //-- boolean hasCdCtciaEspecieBeneficiario() 

    /**
     * Method hasCdCtciaIdBeneficiario
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCtciaIdBeneficiario()
    {
        return this._has_cdCtciaIdBeneficiario;
    } //-- boolean hasCdCtciaIdBeneficiario() 

    /**
     * Method hasCdCtciaInscricaoFavorecido
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCtciaInscricaoFavorecido()
    {
        return this._has_cdCtciaInscricaoFavorecido;
    } //-- boolean hasCdCtciaInscricaoFavorecido() 

    /**
     * Method hasCdCtciaProprietarioVeiculo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCtciaProprietarioVeiculo()
    {
        return this._has_cdCtciaProprietarioVeiculo;
    } //-- boolean hasCdCtciaProprietarioVeiculo() 

    /**
     * Method hasCdDestinoAviso
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdDestinoAviso()
    {
        return this._has_cdDestinoAviso;
    } //-- boolean hasCdDestinoAviso() 

    /**
     * Method hasCdDestinoComprovante
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdDestinoComprovante()
    {
        return this._has_cdDestinoComprovante;
    } //-- boolean hasCdDestinoComprovante() 

    /**
     * Method hasCdDestinoFormularioRcadt
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdDestinoFormularioRcadt()
    {
        return this._has_cdDestinoFormularioRcadt;
    } //-- boolean hasCdDestinoFormularioRcadt() 

    /**
     * Method hasCdDispzContaCredito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdDispzContaCredito()
    {
        return this._has_cdDispzContaCredito;
    } //-- boolean hasCdDispzContaCredito() 

    /**
     * Method hasCdDispzDiversaoNao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdDispzDiversaoNao()
    {
        return this._has_cdDispzDiversaoNao;
    } //-- boolean hasCdDispzDiversaoNao() 

    /**
     * Method hasCdDispzDiversosCrrtt
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdDispzDiversosCrrtt()
    {
        return this._has_cdDispzDiversosCrrtt;
    } //-- boolean hasCdDispzDiversosCrrtt() 

    /**
     * Method hasCdDispzSalarialCrrtt
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdDispzSalarialCrrtt()
    {
        return this._has_cdDispzSalarialCrrtt;
    } //-- boolean hasCdDispzSalarialCrrtt() 

    /**
     * Method hasCdDispzSalarialNao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdDispzSalarialNao()
    {
        return this._has_cdDispzSalarialNao;
    } //-- boolean hasCdDispzSalarialNao() 

    /**
     * Method hasCdEnvelopeAberto
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdEnvelopeAberto()
    {
        return this._has_cdEnvelopeAberto;
    } //-- boolean hasCdEnvelopeAberto() 

    /**
     * Method hasCdFavorecidoConsPgto
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFavorecidoConsPgto()
    {
        return this._has_cdFavorecidoConsPgto;
    } //-- boolean hasCdFavorecidoConsPgto() 

    /**
     * Method hasCdFormaAutrzPgto
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFormaAutrzPgto()
    {
        return this._has_cdFormaAutrzPgto;
    } //-- boolean hasCdFormaAutrzPgto() 

    /**
     * Method hasCdFormaEnvioPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFormaEnvioPagamento()
    {
        return this._has_cdFormaEnvioPagamento;
    } //-- boolean hasCdFormaEnvioPagamento() 

    /**
     * Method hasCdFormaExpirCredito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFormaExpirCredito()
    {
        return this._has_cdFormaExpirCredito;
    } //-- boolean hasCdFormaExpirCredito() 

    /**
     * Method hasCdFormaManutencao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFormaManutencao()
    {
        return this._has_cdFormaManutencao;
    } //-- boolean hasCdFormaManutencao() 

    /**
     * Method hasCdFormularioContratoCliente
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFormularioContratoCliente()
    {
        return this._has_cdFormularioContratoCliente;
    } //-- boolean hasCdFormularioContratoCliente() 

    /**
     * Method hasCdFrasePreCadastro
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFrasePreCadastro()
    {
        return this._has_cdFrasePreCadastro;
    } //-- boolean hasCdFrasePreCadastro() 

    /**
     * Method hasCdIndLancamentoPersonalizado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndLancamentoPersonalizado()
    {
        return this._has_cdIndLancamentoPersonalizado;
    } //-- boolean hasCdIndLancamentoPersonalizado() 

    /**
     * Method hasCdIndicadorAdesaoSacador
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorAdesaoSacador()
    {
        return this._has_cdIndicadorAdesaoSacador;
    } //-- boolean hasCdIndicadorAdesaoSacador() 

    /**
     * Method hasCdIndicadorAgendaTitulo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorAgendaTitulo()
    {
        return this._has_cdIndicadorAgendaTitulo;
    } //-- boolean hasCdIndicadorAgendaTitulo() 

    /**
     * Method hasCdIndicadorAutrzCliente
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorAutrzCliente()
    {
        return this._has_cdIndicadorAutrzCliente;
    } //-- boolean hasCdIndicadorAutrzCliente() 

    /**
     * Method hasCdIndicadorAutrzCompl
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorAutrzCompl()
    {
        return this._has_cdIndicadorAutrzCompl;
    } //-- boolean hasCdIndicadorAutrzCompl() 

    /**
     * Method hasCdIndicadorBancoPostal
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorBancoPostal()
    {
        return this._has_cdIndicadorBancoPostal;
    } //-- boolean hasCdIndicadorBancoPostal() 

    /**
     * Method hasCdIndicadorCadOrg
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorCadOrg()
    {
        return this._has_cdIndicadorCadOrg;
    } //-- boolean hasCdIndicadorCadOrg() 

    /**
     * Method hasCdIndicadorCadProcd
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorCadProcd()
    {
        return this._has_cdIndicadorCadProcd;
    } //-- boolean hasCdIndicadorCadProcd() 

    /**
     * Method hasCdIndicadorCartaoSalarial
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorCartaoSalarial()
    {
        return this._has_cdIndicadorCartaoSalarial;
    } //-- boolean hasCdIndicadorCartaoSalarial() 

    /**
     * Method hasCdIndicadorEconomicoReajuste
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorEconomicoReajuste()
    {
        return this._has_cdIndicadorEconomicoReajuste;
    } //-- boolean hasCdIndicadorEconomicoReajuste() 

    /**
     * Method hasCdIndicadorEmissaoAviso
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorEmissaoAviso()
    {
        return this._has_cdIndicadorEmissaoAviso;
    } //-- boolean hasCdIndicadorEmissaoAviso() 

    /**
     * Method hasCdIndicadorExpirCredito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorExpirCredito()
    {
        return this._has_cdIndicadorExpirCredito;
    } //-- boolean hasCdIndicadorExpirCredito() 

    /**
     * Method hasCdIndicadorLctoPgmd
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorLctoPgmd()
    {
        return this._has_cdIndicadorLctoPgmd;
    } //-- boolean hasCdIndicadorLctoPgmd() 

    /**
     * Method hasCdIndicadorListaDebito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorListaDebito()
    {
        return this._has_cdIndicadorListaDebito;
    } //-- boolean hasCdIndicadorListaDebito() 

    /**
     * Method hasCdIndicadorMensagemPerso
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorMensagemPerso()
    {
        return this._has_cdIndicadorMensagemPerso;
    } //-- boolean hasCdIndicadorMensagemPerso() 

    /**
     * Method hasCdIndicadorRetornoInternet
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorRetornoInternet()
    {
        return this._has_cdIndicadorRetornoInternet;
    } //-- boolean hasCdIndicadorRetornoInternet() 

    /**
     * Method hasCdLancamentoFuturoCredito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdLancamentoFuturoCredito()
    {
        return this._has_cdLancamentoFuturoCredito;
    } //-- boolean hasCdLancamentoFuturoCredito() 

    /**
     * Method hasCdLancamentoFuturoDebito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdLancamentoFuturoDebito()
    {
        return this._has_cdLancamentoFuturoDebito;
    } //-- boolean hasCdLancamentoFuturoDebito() 

    /**
     * Method hasCdLiberacaoLoteProcs
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdLiberacaoLoteProcs()
    {
        return this._has_cdLiberacaoLoteProcs;
    } //-- boolean hasCdLiberacaoLoteProcs() 

    /**
     * Method hasCdManutencaoBaseRcadt
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdManutencaoBaseRcadt()
    {
        return this._has_cdManutencaoBaseRcadt;
    } //-- boolean hasCdManutencaoBaseRcadt() 

    /**
     * Method hasCdMeioPagamentoCredito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMeioPagamentoCredito()
    {
        return this._has_cdMeioPagamentoCredito;
    } //-- boolean hasCdMeioPagamentoCredito() 

    /**
     * Method hasCdMensagemRcadtMidia
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMensagemRcadtMidia()
    {
        return this._has_cdMensagemRcadtMidia;
    } //-- boolean hasCdMensagemRcadtMidia() 

    /**
     * Method hasCdMidiaDisponivel
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMidiaDisponivel()
    {
        return this._has_cdMidiaDisponivel;
    } //-- boolean hasCdMidiaDisponivel() 

    /**
     * Method hasCdMidiaMensagemRcadt
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMidiaMensagemRcadt()
    {
        return this._has_cdMidiaMensagemRcadt;
    } //-- boolean hasCdMidiaMensagemRcadt() 

    /**
     * Method hasCdMomenAvisoRcadt
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMomenAvisoRcadt()
    {
        return this._has_cdMomenAvisoRcadt;
    } //-- boolean hasCdMomenAvisoRcadt() 

    /**
     * Method hasCdMomenCreditoEfetivo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMomenCreditoEfetivo()
    {
        return this._has_cdMomenCreditoEfetivo;
    } //-- boolean hasCdMomenCreditoEfetivo() 

    /**
     * Method hasCdMomenDebitoPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMomenDebitoPagamento()
    {
        return this._has_cdMomenDebitoPagamento;
    } //-- boolean hasCdMomenDebitoPagamento() 

    /**
     * Method hasCdMomenFormulaRcadt
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMomenFormulaRcadt()
    {
        return this._has_cdMomenFormulaRcadt;
    } //-- boolean hasCdMomenFormulaRcadt() 

    /**
     * Method hasCdMomenProcmPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMomenProcmPagamento()
    {
        return this._has_cdMomenProcmPagamento;
    } //-- boolean hasCdMomenProcmPagamento() 

    /**
     * Method hasCdNaturezaOperacaoPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdNaturezaOperacaoPagamento()
    {
        return this._has_cdNaturezaOperacaoPagamento;
    } //-- boolean hasCdNaturezaOperacaoPagamento() 

    /**
     * Method hasCdPagamentoNaoUtil
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPagamentoNaoUtil()
    {
        return this._has_cdPagamentoNaoUtil;
    } //-- boolean hasCdPagamentoNaoUtil() 

    /**
     * Method hasCdPercentualMaximoInconLote
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPercentualMaximoInconLote()
    {
        return this._has_cdPercentualMaximoInconLote;
    } //-- boolean hasCdPercentualMaximoInconLote() 

    /**
     * Method hasCdPerdcAviso
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPerdcAviso()
    {
        return this._has_cdPerdcAviso;
    } //-- boolean hasCdPerdcAviso() 

    /**
     * Method hasCdPerdcCobrancaTarifa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPerdcCobrancaTarifa()
    {
        return this._has_cdPerdcCobrancaTarifa;
    } //-- boolean hasCdPerdcCobrancaTarifa() 

    /**
     * Method hasCdPerdcComprovante
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPerdcComprovante()
    {
        return this._has_cdPerdcComprovante;
    } //-- boolean hasCdPerdcComprovante() 

    /**
     * Method hasCdPerdcConsVeiculo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPerdcConsVeiculo()
    {
        return this._has_cdPerdcConsVeiculo;
    } //-- boolean hasCdPerdcConsVeiculo() 

    /**
     * Method hasCdPerdcEnvioRemessa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPerdcEnvioRemessa()
    {
        return this._has_cdPerdcEnvioRemessa;
    } //-- boolean hasCdPerdcEnvioRemessa() 

    /**
     * Method hasCdPerdcManutencaoProcd
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPerdcManutencaoProcd()
    {
        return this._has_cdPerdcManutencaoProcd;
    } //-- boolean hasCdPerdcManutencaoProcd() 

    /**
     * Method hasCdPermissaoDebitoOnline
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPermissaoDebitoOnline()
    {
        return this._has_cdPermissaoDebitoOnline;
    } //-- boolean hasCdPermissaoDebitoOnline() 

    /**
     * Method hasCdPrincipalEnquaRcadt
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPrincipalEnquaRcadt()
    {
        return this._has_cdPrincipalEnquaRcadt;
    } //-- boolean hasCdPrincipalEnquaRcadt() 

    /**
     * Method hasCdPriorEfetivacaoPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPriorEfetivacaoPagamento()
    {
        return this._has_cdPriorEfetivacaoPagamento;
    } //-- boolean hasCdPriorEfetivacaoPagamento() 

    /**
     * Method hasCdRastreabNotaFiscal
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdRastreabNotaFiscal()
    {
        return this._has_cdRastreabNotaFiscal;
    } //-- boolean hasCdRastreabNotaFiscal() 

    /**
     * Method hasCdRastreabTituloTerc
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdRastreabTituloTerc()
    {
        return this._has_cdRastreabTituloTerc;
    } //-- boolean hasCdRastreabTituloTerc() 

    /**
     * Method hasCdRejeiAgendaLote
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdRejeiAgendaLote()
    {
        return this._has_cdRejeiAgendaLote;
    } //-- boolean hasCdRejeiAgendaLote() 

    /**
     * Method hasCdRejeiEfetivacaoLote
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdRejeiEfetivacaoLote()
    {
        return this._has_cdRejeiEfetivacaoLote;
    } //-- boolean hasCdRejeiEfetivacaoLote() 

    /**
     * Method hasCdRejeiLote
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdRejeiLote()
    {
        return this._has_cdRejeiLote;
    } //-- boolean hasCdRejeiLote() 

    /**
     * Method hasCdTipoCargaRcadt
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoCargaRcadt()
    {
        return this._has_cdTipoCargaRcadt;
    } //-- boolean hasCdTipoCargaRcadt() 

    /**
     * Method hasCdTipoCataoSalarial
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoCataoSalarial()
    {
        return this._has_cdTipoCataoSalarial;
    } //-- boolean hasCdTipoCataoSalarial() 

    /**
     * Method hasCdTipoConsistenciaLista
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoConsistenciaLista()
    {
        return this._has_cdTipoConsistenciaLista;
    } //-- boolean hasCdTipoConsistenciaLista() 

    /**
     * Method hasCdTipoConsultaComprovante
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoConsultaComprovante()
    {
        return this._has_cdTipoConsultaComprovante;
    } //-- boolean hasCdTipoConsultaComprovante() 

    /**
     * Method hasCdTipoContaFavorecido
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContaFavorecido()
    {
        return this._has_cdTipoContaFavorecido;
    } //-- boolean hasCdTipoContaFavorecido() 

    /**
     * Method hasCdTipoDataFloat
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoDataFloat()
    {
        return this._has_cdTipoDataFloat;
    } //-- boolean hasCdTipoDataFloat() 

    /**
     * Method hasCdTipoDivergenciaVeiculo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoDivergenciaVeiculo()
    {
        return this._has_cdTipoDivergenciaVeiculo;
    } //-- boolean hasCdTipoDivergenciaVeiculo() 

    /**
     * Method hasCdTipoFormacaoLista
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoFormacaoLista()
    {
        return this._has_cdTipoFormacaoLista;
    } //-- boolean hasCdTipoFormacaoLista() 

    /**
     * Method hasCdTipoIdBenef
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoIdBenef()
    {
        return this._has_cdTipoIdBenef;
    } //-- boolean hasCdTipoIdBenef() 

    /**
     * Method hasCdTipoIsncricaoFavorecido
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoIsncricaoFavorecido()
    {
        return this._has_cdTipoIsncricaoFavorecido;
    } //-- boolean hasCdTipoIsncricaoFavorecido() 

    /**
     * Method hasCdTipoLayoutArquivo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoLayoutArquivo()
    {
        return this._has_cdTipoLayoutArquivo;
    } //-- boolean hasCdTipoLayoutArquivo() 

    /**
     * Method hasCdTipoReajusteTarifa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoReajusteTarifa()
    {
        return this._has_cdTipoReajusteTarifa;
    } //-- boolean hasCdTipoReajusteTarifa() 

    /**
     * Method hasCdTratoContaTransf
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTratoContaTransf()
    {
        return this._has_cdTratoContaTransf;
    } //-- boolean hasCdTratoContaTransf() 

    /**
     * Method hasCdUtilzFavorecidoCtrl
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdUtilzFavorecidoCtrl()
    {
        return this._has_cdUtilzFavorecidoCtrl;
    } //-- boolean hasCdUtilzFavorecidoCtrl() 

    /**
     * Method hasCdValidacaoNomeFavorecido
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdValidacaoNomeFavorecido()
    {
        return this._has_cdValidacaoNomeFavorecido;
    } //-- boolean hasCdValidacaoNomeFavorecido() 

    /**
     * Method hasNrFechamentoApurcTarifa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrFechamentoApurcTarifa()
    {
        return this._has_nrFechamentoApurcTarifa;
    } //-- boolean hasNrFechamentoApurcTarifa() 

    /**
     * Method hasQtAntecedencia
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtAntecedencia()
    {
        return this._has_qtAntecedencia;
    } //-- boolean hasQtAntecedencia() 

    /**
     * Method hasQtAnteriorVencimentoCompv
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtAnteriorVencimentoCompv()
    {
        return this._has_qtAnteriorVencimentoCompv;
    } //-- boolean hasQtAnteriorVencimentoCompv() 

    /**
     * Method hasQtDiaCobrancaTarifa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtDiaCobrancaTarifa()
    {
        return this._has_qtDiaCobrancaTarifa;
    } //-- boolean hasQtDiaCobrancaTarifa() 

    /**
     * Method hasQtDiaExpiracao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtDiaExpiracao()
    {
        return this._has_qtDiaExpiracao;
    } //-- boolean hasQtDiaExpiracao() 

    /**
     * Method hasQtDiaFloatPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtDiaFloatPagamento()
    {
        return this._has_qtDiaFloatPagamento;
    } //-- boolean hasQtDiaFloatPagamento() 

    /**
     * Method hasQtDiaInativFavorecido
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtDiaInativFavorecido()
    {
        return this._has_qtDiaInativFavorecido;
    } //-- boolean hasQtDiaInativFavorecido() 

    /**
     * Method hasQtDiaRepiqCons
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtDiaRepiqCons()
    {
        return this._has_qtDiaRepiqCons;
    } //-- boolean hasQtDiaRepiqCons() 

    /**
     * Method hasQtEtapaRcadtBeneficiario
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtEtapaRcadtBeneficiario()
    {
        return this._has_qtEtapaRcadtBeneficiario;
    } //-- boolean hasQtEtapaRcadtBeneficiario() 

    /**
     * Method hasQtFaseRcadtBenefiario
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtFaseRcadtBenefiario()
    {
        return this._has_qtFaseRcadtBenefiario;
    } //-- boolean hasQtFaseRcadtBenefiario() 

    /**
     * Method hasQtLimiteLinhas
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtLimiteLinhas()
    {
        return this._has_qtLimiteLinhas;
    } //-- boolean hasQtLimiteLinhas() 

    /**
     * Method hasQtLimiteSolicitacaoCatao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtLimiteSolicitacaoCatao()
    {
        return this._has_qtLimiteSolicitacaoCatao;
    } //-- boolean hasQtLimiteSolicitacaoCatao() 

    /**
     * Method hasQtMaximaInconLote
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtMaximaInconLote()
    {
        return this._has_qtMaximaInconLote;
    } //-- boolean hasQtMaximaInconLote() 

    /**
     * Method hasQtMaximaTituloVencido
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtMaximaTituloVencido()
    {
        return this._has_qtMaximaTituloVencido;
    } //-- boolean hasQtMaximaTituloVencido() 

    /**
     * Method hasQtMesComprovante
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtMesComprovante()
    {
        return this._has_qtMesComprovante;
    } //-- boolean hasQtMesComprovante() 

    /**
     * Method hasQtMesFaseRcadt
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtMesFaseRcadt()
    {
        return this._has_qtMesFaseRcadt;
    } //-- boolean hasQtMesFaseRcadt() 

    /**
     * Method hasQtMesRcadt
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtMesRcadt()
    {
        return this._has_qtMesRcadt;
    } //-- boolean hasQtMesRcadt() 

    /**
     * Method hasQtMesReajusteTarifa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtMesReajusteTarifa()
    {
        return this._has_qtMesReajusteTarifa;
    } //-- boolean hasQtMesReajusteTarifa() 

    /**
     * Method hasQtViaAviso
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtViaAviso()
    {
        return this._has_qtViaAviso;
    } //-- boolean hasQtViaAviso() 

    /**
     * Method hasQtViaCobranca
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtViaCobranca()
    {
        return this._has_qtViaCobranca;
    } //-- boolean hasQtViaCobranca() 

    /**
     * Method hasQtViaComprovante
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtViaComprovante()
    {
        return this._has_qtViaComprovante;
    } //-- boolean hasQtViaComprovante() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdAGendaValorMenor'.
     * 
     * @param cdAGendaValorMenor the value of field
     * 'cdAGendaValorMenor'.
     */
    public void setCdAGendaValorMenor(int cdAGendaValorMenor)
    {
        this._cdAGendaValorMenor = cdAGendaValorMenor;
        this._has_cdAGendaValorMenor = true;
    } //-- void setCdAGendaValorMenor(int) 

    /**
     * Sets the value of field 'cdAcaoNaoVida'.
     * 
     * @param cdAcaoNaoVida the value of field 'cdAcaoNaoVida'.
     */
    public void setCdAcaoNaoVida(int cdAcaoNaoVida)
    {
        this._cdAcaoNaoVida = cdAcaoNaoVida;
        this._has_cdAcaoNaoVida = true;
    } //-- void setCdAcaoNaoVida(int) 

    /**
     * Sets the value of field 'cdAcertoDadoRecadastro'.
     * 
     * @param cdAcertoDadoRecadastro the value of field
     * 'cdAcertoDadoRecadastro'.
     */
    public void setCdAcertoDadoRecadastro(int cdAcertoDadoRecadastro)
    {
        this._cdAcertoDadoRecadastro = cdAcertoDadoRecadastro;
        this._has_cdAcertoDadoRecadastro = true;
    } //-- void setCdAcertoDadoRecadastro(int) 

    /**
     * Sets the value of field 'cdAgendaDebitoVeiculo'.
     * 
     * @param cdAgendaDebitoVeiculo the value of field
     * 'cdAgendaDebitoVeiculo'.
     */
    public void setCdAgendaDebitoVeiculo(int cdAgendaDebitoVeiculo)
    {
        this._cdAgendaDebitoVeiculo = cdAgendaDebitoVeiculo;
        this._has_cdAgendaDebitoVeiculo = true;
    } //-- void setCdAgendaDebitoVeiculo(int) 

    /**
     * Sets the value of field 'cdAgendaPagamentoVencido'.
     * 
     * @param cdAgendaPagamentoVencido the value of field
     * 'cdAgendaPagamentoVencido'.
     */
    public void setCdAgendaPagamentoVencido(int cdAgendaPagamentoVencido)
    {
        this._cdAgendaPagamentoVencido = cdAgendaPagamentoVencido;
        this._has_cdAgendaPagamentoVencido = true;
    } //-- void setCdAgendaPagamentoVencido(int) 

    /**
     * Sets the value of field 'cdAgendaRastreabilidadeFilial'.
     * 
     * @param cdAgendaRastreabilidadeFilial the value of field
     * 'cdAgendaRastreabilidadeFilial'.
     */
    public void setCdAgendaRastreabilidadeFilial(int cdAgendaRastreabilidadeFilial)
    {
        this._cdAgendaRastreabilidadeFilial = cdAgendaRastreabilidadeFilial;
        this._has_cdAgendaRastreabilidadeFilial = true;
    } //-- void setCdAgendaRastreabilidadeFilial(int) 

    /**
     * Sets the value of field 'cdAgptoFormularioRecadastro'.
     * 
     * @param cdAgptoFormularioRecadastro the value of field
     * 'cdAgptoFormularioRecadastro'.
     */
    public void setCdAgptoFormularioRecadastro(int cdAgptoFormularioRecadastro)
    {
        this._cdAgptoFormularioRecadastro = cdAgptoFormularioRecadastro;
        this._has_cdAgptoFormularioRecadastro = true;
    } //-- void setCdAgptoFormularioRecadastro(int) 

    /**
     * Sets the value of field 'cdAgrupamentoAviso'.
     * 
     * @param cdAgrupamentoAviso the value of field
     * 'cdAgrupamentoAviso'.
     */
    public void setCdAgrupamentoAviso(int cdAgrupamentoAviso)
    {
        this._cdAgrupamentoAviso = cdAgrupamentoAviso;
        this._has_cdAgrupamentoAviso = true;
    } //-- void setCdAgrupamentoAviso(int) 

    /**
     * Sets the value of field 'cdAgrupamentoComprovado'.
     * 
     * @param cdAgrupamentoComprovado the value of field
     * 'cdAgrupamentoComprovado'.
     */
    public void setCdAgrupamentoComprovado(int cdAgrupamentoComprovado)
    {
        this._cdAgrupamentoComprovado = cdAgrupamentoComprovado;
        this._has_cdAgrupamentoComprovado = true;
    } //-- void setCdAgrupamentoComprovado(int) 

    /**
     * Sets the value of field 'cdAntecRecadastroBeneficiario'.
     * 
     * @param cdAntecRecadastroBeneficiario the value of field
     * 'cdAntecRecadastroBeneficiario'.
     */
    public void setCdAntecRecadastroBeneficiario(int cdAntecRecadastroBeneficiario)
    {
        this._cdAntecRecadastroBeneficiario = cdAntecRecadastroBeneficiario;
        this._has_cdAntecRecadastroBeneficiario = true;
    } //-- void setCdAntecRecadastroBeneficiario(int) 

    /**
     * Sets the value of field 'cdAreaReservada'.
     * 
     * @param cdAreaReservada the value of field 'cdAreaReservada'.
     */
    public void setCdAreaReservada(int cdAreaReservada)
    {
        this._cdAreaReservada = cdAreaReservada;
        this._has_cdAreaReservada = true;
    } //-- void setCdAreaReservada(int) 

    /**
     * Sets the value of field 'cdBasaeRecadastroBeneficiario'.
     * 
     * @param cdBasaeRecadastroBeneficiario the value of field
     * 'cdBasaeRecadastroBeneficiario'.
     */
    public void setCdBasaeRecadastroBeneficiario(int cdBasaeRecadastroBeneficiario)
    {
        this._cdBasaeRecadastroBeneficiario = cdBasaeRecadastroBeneficiario;
        this._has_cdBasaeRecadastroBeneficiario = true;
    } //-- void setCdBasaeRecadastroBeneficiario(int) 

    /**
     * Sets the value of field 'cdBloqueioEmissaoPplta'.
     * 
     * @param cdBloqueioEmissaoPplta the value of field
     * 'cdBloqueioEmissaoPplta'.
     */
    public void setCdBloqueioEmissaoPplta(int cdBloqueioEmissaoPplta)
    {
        this._cdBloqueioEmissaoPplta = cdBloqueioEmissaoPplta;
        this._has_cdBloqueioEmissaoPplta = true;
    } //-- void setCdBloqueioEmissaoPplta(int) 

    /**
     * Sets the value of field 'cdCaptuTituloRegistro'.
     * 
     * @param cdCaptuTituloRegistro the value of field
     * 'cdCaptuTituloRegistro'.
     */
    public void setCdCaptuTituloRegistro(int cdCaptuTituloRegistro)
    {
        this._cdCaptuTituloRegistro = cdCaptuTituloRegistro;
        this._has_cdCaptuTituloRegistro = true;
    } //-- void setCdCaptuTituloRegistro(int) 

    /**
     * Sets the value of field 'cdCobrancaTarifa'.
     * 
     * @param cdCobrancaTarifa the value of field 'cdCobrancaTarifa'
     */
    public void setCdCobrancaTarifa(int cdCobrancaTarifa)
    {
        this._cdCobrancaTarifa = cdCobrancaTarifa;
        this._has_cdCobrancaTarifa = true;
    } //-- void setCdCobrancaTarifa(int) 

    /**
     * Sets the value of field 'cdConsDebitoVeiculo'.
     * 
     * @param cdConsDebitoVeiculo the value of field
     * 'cdConsDebitoVeiculo'.
     */
    public void setCdConsDebitoVeiculo(int cdConsDebitoVeiculo)
    {
        this._cdConsDebitoVeiculo = cdConsDebitoVeiculo;
        this._has_cdConsDebitoVeiculo = true;
    } //-- void setCdConsDebitoVeiculo(int) 

    /**
     * Sets the value of field 'cdConsEndereco'.
     * 
     * @param cdConsEndereco the value of field 'cdConsEndereco'.
     */
    public void setCdConsEndereco(int cdConsEndereco)
    {
        this._cdConsEndereco = cdConsEndereco;
        this._has_cdConsEndereco = true;
    } //-- void setCdConsEndereco(int) 

    /**
     * Sets the value of field 'cdConsSaldoPagamento'.
     * 
     * @param cdConsSaldoPagamento the value of field
     * 'cdConsSaldoPagamento'.
     */
    public void setCdConsSaldoPagamento(int cdConsSaldoPagamento)
    {
        this._cdConsSaldoPagamento = cdConsSaldoPagamento;
        this._has_cdConsSaldoPagamento = true;
    } //-- void setCdConsSaldoPagamento(int) 

    /**
     * Sets the value of field 'cdConsultaSaldoValorSuperior'.
     * 
     * @param cdConsultaSaldoValorSuperior the value of field
     * 'cdConsultaSaldoValorSuperior'.
     */
    public void setCdConsultaSaldoValorSuperior(int cdConsultaSaldoValorSuperior)
    {
        this._cdConsultaSaldoValorSuperior = cdConsultaSaldoValorSuperior;
        this._has_cdConsultaSaldoValorSuperior = true;
    } //-- void setCdConsultaSaldoValorSuperior(int) 

    /**
     * Sets the value of field 'cdContgConsSaldo'.
     * 
     * @param cdContgConsSaldo the value of field 'cdContgConsSaldo'
     */
    public void setCdContgConsSaldo(int cdContgConsSaldo)
    {
        this._cdContgConsSaldo = cdContgConsSaldo;
        this._has_cdContgConsSaldo = true;
    } //-- void setCdContgConsSaldo(int) 

    /**
     * Sets the value of field 'cdCreditoNaoUtilizado'.
     * 
     * @param cdCreditoNaoUtilizado the value of field
     * 'cdCreditoNaoUtilizado'.
     */
    public void setCdCreditoNaoUtilizado(int cdCreditoNaoUtilizado)
    {
        this._cdCreditoNaoUtilizado = cdCreditoNaoUtilizado;
        this._has_cdCreditoNaoUtilizado = true;
    } //-- void setCdCreditoNaoUtilizado(int) 

    /**
     * Sets the value of field 'cdCriterioEnquaBeneficiario'.
     * 
     * @param cdCriterioEnquaBeneficiario the value of field
     * 'cdCriterioEnquaBeneficiario'.
     */
    public void setCdCriterioEnquaBeneficiario(int cdCriterioEnquaBeneficiario)
    {
        this._cdCriterioEnquaBeneficiario = cdCriterioEnquaBeneficiario;
        this._has_cdCriterioEnquaBeneficiario = true;
    } //-- void setCdCriterioEnquaBeneficiario(int) 

    /**
     * Sets the value of field 'cdCriterioEnquaRcadt'.
     * 
     * @param cdCriterioEnquaRcadt the value of field
     * 'cdCriterioEnquaRcadt'.
     */
    public void setCdCriterioEnquaRcadt(int cdCriterioEnquaRcadt)
    {
        this._cdCriterioEnquaRcadt = cdCriterioEnquaRcadt;
        this._has_cdCriterioEnquaRcadt = true;
    } //-- void setCdCriterioEnquaRcadt(int) 

    /**
     * Sets the value of field 'cdCriterioRstrbTitulo'.
     * 
     * @param cdCriterioRstrbTitulo the value of field
     * 'cdCriterioRstrbTitulo'.
     */
    public void setCdCriterioRstrbTitulo(int cdCriterioRstrbTitulo)
    {
        this._cdCriterioRstrbTitulo = cdCriterioRstrbTitulo;
        this._has_cdCriterioRstrbTitulo = true;
    } //-- void setCdCriterioRstrbTitulo(int) 

    /**
     * Sets the value of field 'cdCtciaEspecieBeneficiario'.
     * 
     * @param cdCtciaEspecieBeneficiario the value of field
     * 'cdCtciaEspecieBeneficiario'.
     */
    public void setCdCtciaEspecieBeneficiario(int cdCtciaEspecieBeneficiario)
    {
        this._cdCtciaEspecieBeneficiario = cdCtciaEspecieBeneficiario;
        this._has_cdCtciaEspecieBeneficiario = true;
    } //-- void setCdCtciaEspecieBeneficiario(int) 

    /**
     * Sets the value of field 'cdCtciaIdBeneficiario'.
     * 
     * @param cdCtciaIdBeneficiario the value of field
     * 'cdCtciaIdBeneficiario'.
     */
    public void setCdCtciaIdBeneficiario(int cdCtciaIdBeneficiario)
    {
        this._cdCtciaIdBeneficiario = cdCtciaIdBeneficiario;
        this._has_cdCtciaIdBeneficiario = true;
    } //-- void setCdCtciaIdBeneficiario(int) 

    /**
     * Sets the value of field 'cdCtciaInscricaoFavorecido'.
     * 
     * @param cdCtciaInscricaoFavorecido the value of field
     * 'cdCtciaInscricaoFavorecido'.
     */
    public void setCdCtciaInscricaoFavorecido(int cdCtciaInscricaoFavorecido)
    {
        this._cdCtciaInscricaoFavorecido = cdCtciaInscricaoFavorecido;
        this._has_cdCtciaInscricaoFavorecido = true;
    } //-- void setCdCtciaInscricaoFavorecido(int) 

    /**
     * Sets the value of field 'cdCtciaProprietarioVeiculo'.
     * 
     * @param cdCtciaProprietarioVeiculo the value of field
     * 'cdCtciaProprietarioVeiculo'.
     */
    public void setCdCtciaProprietarioVeiculo(int cdCtciaProprietarioVeiculo)
    {
        this._cdCtciaProprietarioVeiculo = cdCtciaProprietarioVeiculo;
        this._has_cdCtciaProprietarioVeiculo = true;
    } //-- void setCdCtciaProprietarioVeiculo(int) 

    /**
     * Sets the value of field 'cdDestinoAviso'.
     * 
     * @param cdDestinoAviso the value of field 'cdDestinoAviso'.
     */
    public void setCdDestinoAviso(int cdDestinoAviso)
    {
        this._cdDestinoAviso = cdDestinoAviso;
        this._has_cdDestinoAviso = true;
    } //-- void setCdDestinoAviso(int) 

    /**
     * Sets the value of field 'cdDestinoComprovante'.
     * 
     * @param cdDestinoComprovante the value of field
     * 'cdDestinoComprovante'.
     */
    public void setCdDestinoComprovante(int cdDestinoComprovante)
    {
        this._cdDestinoComprovante = cdDestinoComprovante;
        this._has_cdDestinoComprovante = true;
    } //-- void setCdDestinoComprovante(int) 

    /**
     * Sets the value of field 'cdDestinoFormularioRcadt'.
     * 
     * @param cdDestinoFormularioRcadt the value of field
     * 'cdDestinoFormularioRcadt'.
     */
    public void setCdDestinoFormularioRcadt(int cdDestinoFormularioRcadt)
    {
        this._cdDestinoFormularioRcadt = cdDestinoFormularioRcadt;
        this._has_cdDestinoFormularioRcadt = true;
    } //-- void setCdDestinoFormularioRcadt(int) 

    /**
     * Sets the value of field 'cdDispzContaCredito'.
     * 
     * @param cdDispzContaCredito the value of field
     * 'cdDispzContaCredito'.
     */
    public void setCdDispzContaCredito(int cdDispzContaCredito)
    {
        this._cdDispzContaCredito = cdDispzContaCredito;
        this._has_cdDispzContaCredito = true;
    } //-- void setCdDispzContaCredito(int) 

    /**
     * Sets the value of field 'cdDispzDiversaoNao'.
     * 
     * @param cdDispzDiversaoNao the value of field
     * 'cdDispzDiversaoNao'.
     */
    public void setCdDispzDiversaoNao(int cdDispzDiversaoNao)
    {
        this._cdDispzDiversaoNao = cdDispzDiversaoNao;
        this._has_cdDispzDiversaoNao = true;
    } //-- void setCdDispzDiversaoNao(int) 

    /**
     * Sets the value of field 'cdDispzDiversosCrrtt'.
     * 
     * @param cdDispzDiversosCrrtt the value of field
     * 'cdDispzDiversosCrrtt'.
     */
    public void setCdDispzDiversosCrrtt(int cdDispzDiversosCrrtt)
    {
        this._cdDispzDiversosCrrtt = cdDispzDiversosCrrtt;
        this._has_cdDispzDiversosCrrtt = true;
    } //-- void setCdDispzDiversosCrrtt(int) 

    /**
     * Sets the value of field 'cdDispzSalarialCrrtt'.
     * 
     * @param cdDispzSalarialCrrtt the value of field
     * 'cdDispzSalarialCrrtt'.
     */
    public void setCdDispzSalarialCrrtt(int cdDispzSalarialCrrtt)
    {
        this._cdDispzSalarialCrrtt = cdDispzSalarialCrrtt;
        this._has_cdDispzSalarialCrrtt = true;
    } //-- void setCdDispzSalarialCrrtt(int) 

    /**
     * Sets the value of field 'cdDispzSalarialNao'.
     * 
     * @param cdDispzSalarialNao the value of field
     * 'cdDispzSalarialNao'.
     */
    public void setCdDispzSalarialNao(int cdDispzSalarialNao)
    {
        this._cdDispzSalarialNao = cdDispzSalarialNao;
        this._has_cdDispzSalarialNao = true;
    } //-- void setCdDispzSalarialNao(int) 

    /**
     * Sets the value of field 'cdEnvelopeAberto'.
     * 
     * @param cdEnvelopeAberto the value of field 'cdEnvelopeAberto'
     */
    public void setCdEnvelopeAberto(int cdEnvelopeAberto)
    {
        this._cdEnvelopeAberto = cdEnvelopeAberto;
        this._has_cdEnvelopeAberto = true;
    } //-- void setCdEnvelopeAberto(int) 

    /**
     * Sets the value of field 'cdFavorecidoConsPgto'.
     * 
     * @param cdFavorecidoConsPgto the value of field
     * 'cdFavorecidoConsPgto'.
     */
    public void setCdFavorecidoConsPgto(int cdFavorecidoConsPgto)
    {
        this._cdFavorecidoConsPgto = cdFavorecidoConsPgto;
        this._has_cdFavorecidoConsPgto = true;
    } //-- void setCdFavorecidoConsPgto(int) 

    /**
     * Sets the value of field 'cdFormaAutrzPgto'.
     * 
     * @param cdFormaAutrzPgto the value of field 'cdFormaAutrzPgto'
     */
    public void setCdFormaAutrzPgto(int cdFormaAutrzPgto)
    {
        this._cdFormaAutrzPgto = cdFormaAutrzPgto;
        this._has_cdFormaAutrzPgto = true;
    } //-- void setCdFormaAutrzPgto(int) 

    /**
     * Sets the value of field 'cdFormaEnvioPagamento'.
     * 
     * @param cdFormaEnvioPagamento the value of field
     * 'cdFormaEnvioPagamento'.
     */
    public void setCdFormaEnvioPagamento(int cdFormaEnvioPagamento)
    {
        this._cdFormaEnvioPagamento = cdFormaEnvioPagamento;
        this._has_cdFormaEnvioPagamento = true;
    } //-- void setCdFormaEnvioPagamento(int) 

    /**
     * Sets the value of field 'cdFormaExpirCredito'.
     * 
     * @param cdFormaExpirCredito the value of field
     * 'cdFormaExpirCredito'.
     */
    public void setCdFormaExpirCredito(int cdFormaExpirCredito)
    {
        this._cdFormaExpirCredito = cdFormaExpirCredito;
        this._has_cdFormaExpirCredito = true;
    } //-- void setCdFormaExpirCredito(int) 

    /**
     * Sets the value of field 'cdFormaManutencao'.
     * 
     * @param cdFormaManutencao the value of field
     * 'cdFormaManutencao'.
     */
    public void setCdFormaManutencao(int cdFormaManutencao)
    {
        this._cdFormaManutencao = cdFormaManutencao;
        this._has_cdFormaManutencao = true;
    } //-- void setCdFormaManutencao(int) 

    /**
     * Sets the value of field 'cdFormularioContratoCliente'.
     * 
     * @param cdFormularioContratoCliente the value of field
     * 'cdFormularioContratoCliente'.
     */
    public void setCdFormularioContratoCliente(int cdFormularioContratoCliente)
    {
        this._cdFormularioContratoCliente = cdFormularioContratoCliente;
        this._has_cdFormularioContratoCliente = true;
    } //-- void setCdFormularioContratoCliente(int) 

    /**
     * Sets the value of field 'cdFrasePreCadastro'.
     * 
     * @param cdFrasePreCadastro the value of field
     * 'cdFrasePreCadastro'.
     */
    public void setCdFrasePreCadastro(int cdFrasePreCadastro)
    {
        this._cdFrasePreCadastro = cdFrasePreCadastro;
        this._has_cdFrasePreCadastro = true;
    } //-- void setCdFrasePreCadastro(int) 

    /**
     * Sets the value of field 'cdIndLancamentoPersonalizado'.
     * 
     * @param cdIndLancamentoPersonalizado the value of field
     * 'cdIndLancamentoPersonalizado'.
     */
    public void setCdIndLancamentoPersonalizado(int cdIndLancamentoPersonalizado)
    {
        this._cdIndLancamentoPersonalizado = cdIndLancamentoPersonalizado;
        this._has_cdIndLancamentoPersonalizado = true;
    } //-- void setCdIndLancamentoPersonalizado(int) 

    /**
     * Sets the value of field 'cdIndicadorAdesaoSacador'.
     * 
     * @param cdIndicadorAdesaoSacador the value of field
     * 'cdIndicadorAdesaoSacador'.
     */
    public void setCdIndicadorAdesaoSacador(int cdIndicadorAdesaoSacador)
    {
        this._cdIndicadorAdesaoSacador = cdIndicadorAdesaoSacador;
        this._has_cdIndicadorAdesaoSacador = true;
    } //-- void setCdIndicadorAdesaoSacador(int) 

    /**
     * Sets the value of field 'cdIndicadorAgendaTitulo'.
     * 
     * @param cdIndicadorAgendaTitulo the value of field
     * 'cdIndicadorAgendaTitulo'.
     */
    public void setCdIndicadorAgendaTitulo(int cdIndicadorAgendaTitulo)
    {
        this._cdIndicadorAgendaTitulo = cdIndicadorAgendaTitulo;
        this._has_cdIndicadorAgendaTitulo = true;
    } //-- void setCdIndicadorAgendaTitulo(int) 

    /**
     * Sets the value of field 'cdIndicadorAutrzCliente'.
     * 
     * @param cdIndicadorAutrzCliente the value of field
     * 'cdIndicadorAutrzCliente'.
     */
    public void setCdIndicadorAutrzCliente(int cdIndicadorAutrzCliente)
    {
        this._cdIndicadorAutrzCliente = cdIndicadorAutrzCliente;
        this._has_cdIndicadorAutrzCliente = true;
    } //-- void setCdIndicadorAutrzCliente(int) 

    /**
     * Sets the value of field 'cdIndicadorAutrzCompl'.
     * 
     * @param cdIndicadorAutrzCompl the value of field
     * 'cdIndicadorAutrzCompl'.
     */
    public void setCdIndicadorAutrzCompl(int cdIndicadorAutrzCompl)
    {
        this._cdIndicadorAutrzCompl = cdIndicadorAutrzCompl;
        this._has_cdIndicadorAutrzCompl = true;
    } //-- void setCdIndicadorAutrzCompl(int) 

    /**
     * Sets the value of field 'cdIndicadorBancoPostal'.
     * 
     * @param cdIndicadorBancoPostal the value of field
     * 'cdIndicadorBancoPostal'.
     */
    public void setCdIndicadorBancoPostal(int cdIndicadorBancoPostal)
    {
        this._cdIndicadorBancoPostal = cdIndicadorBancoPostal;
        this._has_cdIndicadorBancoPostal = true;
    } //-- void setCdIndicadorBancoPostal(int) 

    /**
     * Sets the value of field 'cdIndicadorCadOrg'.
     * 
     * @param cdIndicadorCadOrg the value of field
     * 'cdIndicadorCadOrg'.
     */
    public void setCdIndicadorCadOrg(int cdIndicadorCadOrg)
    {
        this._cdIndicadorCadOrg = cdIndicadorCadOrg;
        this._has_cdIndicadorCadOrg = true;
    } //-- void setCdIndicadorCadOrg(int) 

    /**
     * Sets the value of field 'cdIndicadorCadProcd'.
     * 
     * @param cdIndicadorCadProcd the value of field
     * 'cdIndicadorCadProcd'.
     */
    public void setCdIndicadorCadProcd(int cdIndicadorCadProcd)
    {
        this._cdIndicadorCadProcd = cdIndicadorCadProcd;
        this._has_cdIndicadorCadProcd = true;
    } //-- void setCdIndicadorCadProcd(int) 

    /**
     * Sets the value of field 'cdIndicadorCartaoSalarial'.
     * 
     * @param cdIndicadorCartaoSalarial the value of field
     * 'cdIndicadorCartaoSalarial'.
     */
    public void setCdIndicadorCartaoSalarial(int cdIndicadorCartaoSalarial)
    {
        this._cdIndicadorCartaoSalarial = cdIndicadorCartaoSalarial;
        this._has_cdIndicadorCartaoSalarial = true;
    } //-- void setCdIndicadorCartaoSalarial(int) 

    /**
     * Sets the value of field 'cdIndicadorEconomicoReajuste'.
     * 
     * @param cdIndicadorEconomicoReajuste the value of field
     * 'cdIndicadorEconomicoReajuste'.
     */
    public void setCdIndicadorEconomicoReajuste(int cdIndicadorEconomicoReajuste)
    {
        this._cdIndicadorEconomicoReajuste = cdIndicadorEconomicoReajuste;
        this._has_cdIndicadorEconomicoReajuste = true;
    } //-- void setCdIndicadorEconomicoReajuste(int) 

    /**
     * Sets the value of field 'cdIndicadorEmissaoAviso'.
     * 
     * @param cdIndicadorEmissaoAviso the value of field
     * 'cdIndicadorEmissaoAviso'.
     */
    public void setCdIndicadorEmissaoAviso(int cdIndicadorEmissaoAviso)
    {
        this._cdIndicadorEmissaoAviso = cdIndicadorEmissaoAviso;
        this._has_cdIndicadorEmissaoAviso = true;
    } //-- void setCdIndicadorEmissaoAviso(int) 

    /**
     * Sets the value of field 'cdIndicadorExpirCredito'.
     * 
     * @param cdIndicadorExpirCredito the value of field
     * 'cdIndicadorExpirCredito'.
     */
    public void setCdIndicadorExpirCredito(int cdIndicadorExpirCredito)
    {
        this._cdIndicadorExpirCredito = cdIndicadorExpirCredito;
        this._has_cdIndicadorExpirCredito = true;
    } //-- void setCdIndicadorExpirCredito(int) 

    /**
     * Sets the value of field 'cdIndicadorLctoPgmd'.
     * 
     * @param cdIndicadorLctoPgmd the value of field
     * 'cdIndicadorLctoPgmd'.
     */
    public void setCdIndicadorLctoPgmd(int cdIndicadorLctoPgmd)
    {
        this._cdIndicadorLctoPgmd = cdIndicadorLctoPgmd;
        this._has_cdIndicadorLctoPgmd = true;
    } //-- void setCdIndicadorLctoPgmd(int) 

    /**
     * Sets the value of field 'cdIndicadorListaDebito'.
     * 
     * @param cdIndicadorListaDebito the value of field
     * 'cdIndicadorListaDebito'.
     */
    public void setCdIndicadorListaDebito(int cdIndicadorListaDebito)
    {
        this._cdIndicadorListaDebito = cdIndicadorListaDebito;
        this._has_cdIndicadorListaDebito = true;
    } //-- void setCdIndicadorListaDebito(int) 

    /**
     * Sets the value of field 'cdIndicadorMensagemPerso'.
     * 
     * @param cdIndicadorMensagemPerso the value of field
     * 'cdIndicadorMensagemPerso'.
     */
    public void setCdIndicadorMensagemPerso(int cdIndicadorMensagemPerso)
    {
        this._cdIndicadorMensagemPerso = cdIndicadorMensagemPerso;
        this._has_cdIndicadorMensagemPerso = true;
    } //-- void setCdIndicadorMensagemPerso(int) 

    /**
     * Sets the value of field 'cdIndicadorRetornoInternet'.
     * 
     * @param cdIndicadorRetornoInternet the value of field
     * 'cdIndicadorRetornoInternet'.
     */
    public void setCdIndicadorRetornoInternet(int cdIndicadorRetornoInternet)
    {
        this._cdIndicadorRetornoInternet = cdIndicadorRetornoInternet;
        this._has_cdIndicadorRetornoInternet = true;
    } //-- void setCdIndicadorRetornoInternet(int) 

    /**
     * Sets the value of field 'cdLancamentoFuturoCredito'.
     * 
     * @param cdLancamentoFuturoCredito the value of field
     * 'cdLancamentoFuturoCredito'.
     */
    public void setCdLancamentoFuturoCredito(int cdLancamentoFuturoCredito)
    {
        this._cdLancamentoFuturoCredito = cdLancamentoFuturoCredito;
        this._has_cdLancamentoFuturoCredito = true;
    } //-- void setCdLancamentoFuturoCredito(int) 

    /**
     * Sets the value of field 'cdLancamentoFuturoDebito'.
     * 
     * @param cdLancamentoFuturoDebito the value of field
     * 'cdLancamentoFuturoDebito'.
     */
    public void setCdLancamentoFuturoDebito(int cdLancamentoFuturoDebito)
    {
        this._cdLancamentoFuturoDebito = cdLancamentoFuturoDebito;
        this._has_cdLancamentoFuturoDebito = true;
    } //-- void setCdLancamentoFuturoDebito(int) 

    /**
     * Sets the value of field 'cdLiberacaoLoteProcs'.
     * 
     * @param cdLiberacaoLoteProcs the value of field
     * 'cdLiberacaoLoteProcs'.
     */
    public void setCdLiberacaoLoteProcs(int cdLiberacaoLoteProcs)
    {
        this._cdLiberacaoLoteProcs = cdLiberacaoLoteProcs;
        this._has_cdLiberacaoLoteProcs = true;
    } //-- void setCdLiberacaoLoteProcs(int) 

    /**
     * Sets the value of field 'cdManutencaoBaseRcadt'.
     * 
     * @param cdManutencaoBaseRcadt the value of field
     * 'cdManutencaoBaseRcadt'.
     */
    public void setCdManutencaoBaseRcadt(int cdManutencaoBaseRcadt)
    {
        this._cdManutencaoBaseRcadt = cdManutencaoBaseRcadt;
        this._has_cdManutencaoBaseRcadt = true;
    } //-- void setCdManutencaoBaseRcadt(int) 

    /**
     * Sets the value of field 'cdMeioPagamentoCredito'.
     * 
     * @param cdMeioPagamentoCredito the value of field
     * 'cdMeioPagamentoCredito'.
     */
    public void setCdMeioPagamentoCredito(int cdMeioPagamentoCredito)
    {
        this._cdMeioPagamentoCredito = cdMeioPagamentoCredito;
        this._has_cdMeioPagamentoCredito = true;
    } //-- void setCdMeioPagamentoCredito(int) 

    /**
     * Sets the value of field 'cdMensagemRcadtMidia'.
     * 
     * @param cdMensagemRcadtMidia the value of field
     * 'cdMensagemRcadtMidia'.
     */
    public void setCdMensagemRcadtMidia(int cdMensagemRcadtMidia)
    {
        this._cdMensagemRcadtMidia = cdMensagemRcadtMidia;
        this._has_cdMensagemRcadtMidia = true;
    } //-- void setCdMensagemRcadtMidia(int) 

    /**
     * Sets the value of field 'cdMidiaDisponivel'.
     * 
     * @param cdMidiaDisponivel the value of field
     * 'cdMidiaDisponivel'.
     */
    public void setCdMidiaDisponivel(int cdMidiaDisponivel)
    {
        this._cdMidiaDisponivel = cdMidiaDisponivel;
        this._has_cdMidiaDisponivel = true;
    } //-- void setCdMidiaDisponivel(int) 

    /**
     * Sets the value of field 'cdMidiaMensagemRcadt'.
     * 
     * @param cdMidiaMensagemRcadt the value of field
     * 'cdMidiaMensagemRcadt'.
     */
    public void setCdMidiaMensagemRcadt(int cdMidiaMensagemRcadt)
    {
        this._cdMidiaMensagemRcadt = cdMidiaMensagemRcadt;
        this._has_cdMidiaMensagemRcadt = true;
    } //-- void setCdMidiaMensagemRcadt(int) 

    /**
     * Sets the value of field 'cdMomenAvisoRcadt'.
     * 
     * @param cdMomenAvisoRcadt the value of field
     * 'cdMomenAvisoRcadt'.
     */
    public void setCdMomenAvisoRcadt(int cdMomenAvisoRcadt)
    {
        this._cdMomenAvisoRcadt = cdMomenAvisoRcadt;
        this._has_cdMomenAvisoRcadt = true;
    } //-- void setCdMomenAvisoRcadt(int) 

    /**
     * Sets the value of field 'cdMomenCreditoEfetivo'.
     * 
     * @param cdMomenCreditoEfetivo the value of field
     * 'cdMomenCreditoEfetivo'.
     */
    public void setCdMomenCreditoEfetivo(int cdMomenCreditoEfetivo)
    {
        this._cdMomenCreditoEfetivo = cdMomenCreditoEfetivo;
        this._has_cdMomenCreditoEfetivo = true;
    } //-- void setCdMomenCreditoEfetivo(int) 

    /**
     * Sets the value of field 'cdMomenDebitoPagamento'.
     * 
     * @param cdMomenDebitoPagamento the value of field
     * 'cdMomenDebitoPagamento'.
     */
    public void setCdMomenDebitoPagamento(int cdMomenDebitoPagamento)
    {
        this._cdMomenDebitoPagamento = cdMomenDebitoPagamento;
        this._has_cdMomenDebitoPagamento = true;
    } //-- void setCdMomenDebitoPagamento(int) 

    /**
     * Sets the value of field 'cdMomenFormulaRcadt'.
     * 
     * @param cdMomenFormulaRcadt the value of field
     * 'cdMomenFormulaRcadt'.
     */
    public void setCdMomenFormulaRcadt(int cdMomenFormulaRcadt)
    {
        this._cdMomenFormulaRcadt = cdMomenFormulaRcadt;
        this._has_cdMomenFormulaRcadt = true;
    } //-- void setCdMomenFormulaRcadt(int) 

    /**
     * Sets the value of field 'cdMomenProcmPagamento'.
     * 
     * @param cdMomenProcmPagamento the value of field
     * 'cdMomenProcmPagamento'.
     */
    public void setCdMomenProcmPagamento(int cdMomenProcmPagamento)
    {
        this._cdMomenProcmPagamento = cdMomenProcmPagamento;
        this._has_cdMomenProcmPagamento = true;
    } //-- void setCdMomenProcmPagamento(int) 

    /**
     * Sets the value of field 'cdNaturezaOperacaoPagamento'.
     * 
     * @param cdNaturezaOperacaoPagamento the value of field
     * 'cdNaturezaOperacaoPagamento'.
     */
    public void setCdNaturezaOperacaoPagamento(int cdNaturezaOperacaoPagamento)
    {
        this._cdNaturezaOperacaoPagamento = cdNaturezaOperacaoPagamento;
        this._has_cdNaturezaOperacaoPagamento = true;
    } //-- void setCdNaturezaOperacaoPagamento(int) 

    /**
     * Sets the value of field 'cdPagamentoNaoUtil'.
     * 
     * @param cdPagamentoNaoUtil the value of field
     * 'cdPagamentoNaoUtil'.
     */
    public void setCdPagamentoNaoUtil(int cdPagamentoNaoUtil)
    {
        this._cdPagamentoNaoUtil = cdPagamentoNaoUtil;
        this._has_cdPagamentoNaoUtil = true;
    } //-- void setCdPagamentoNaoUtil(int) 

    /**
     * Sets the value of field 'cdPercentualIndiceReajusteTarifa'.
     * 
     * @param cdPercentualIndiceReajusteTarifa the value of field
     * 'cdPercentualIndiceReajusteTarifa'.
     */
    public void setCdPercentualIndiceReajusteTarifa(java.math.BigDecimal cdPercentualIndiceReajusteTarifa)
    {
        this._cdPercentualIndiceReajusteTarifa = cdPercentualIndiceReajusteTarifa;
    } //-- void setCdPercentualIndiceReajusteTarifa(java.math.BigDecimal) 

    /**
     * Sets the value of field 'cdPercentualMaximoInconLote'.
     * 
     * @param cdPercentualMaximoInconLote the value of field
     * 'cdPercentualMaximoInconLote'.
     */
    public void setCdPercentualMaximoInconLote(int cdPercentualMaximoInconLote)
    {
        this._cdPercentualMaximoInconLote = cdPercentualMaximoInconLote;
        this._has_cdPercentualMaximoInconLote = true;
    } //-- void setCdPercentualMaximoInconLote(int) 

    /**
     * Sets the value of field 'cdPercentualReducaoTarifaCatlg'.
     * 
     * @param cdPercentualReducaoTarifaCatlg the value of field
     * 'cdPercentualReducaoTarifaCatlg'.
     */
    public void setCdPercentualReducaoTarifaCatlg(java.math.BigDecimal cdPercentualReducaoTarifaCatlg)
    {
        this._cdPercentualReducaoTarifaCatlg = cdPercentualReducaoTarifaCatlg;
    } //-- void setCdPercentualReducaoTarifaCatlg(java.math.BigDecimal) 

    /**
     * Sets the value of field 'cdPerdcAviso'.
     * 
     * @param cdPerdcAviso the value of field 'cdPerdcAviso'.
     */
    public void setCdPerdcAviso(int cdPerdcAviso)
    {
        this._cdPerdcAviso = cdPerdcAviso;
        this._has_cdPerdcAviso = true;
    } //-- void setCdPerdcAviso(int) 

    /**
     * Sets the value of field 'cdPerdcCobrancaTarifa'.
     * 
     * @param cdPerdcCobrancaTarifa the value of field
     * 'cdPerdcCobrancaTarifa'.
     */
    public void setCdPerdcCobrancaTarifa(int cdPerdcCobrancaTarifa)
    {
        this._cdPerdcCobrancaTarifa = cdPerdcCobrancaTarifa;
        this._has_cdPerdcCobrancaTarifa = true;
    } //-- void setCdPerdcCobrancaTarifa(int) 

    /**
     * Sets the value of field 'cdPerdcComprovante'.
     * 
     * @param cdPerdcComprovante the value of field
     * 'cdPerdcComprovante'.
     */
    public void setCdPerdcComprovante(int cdPerdcComprovante)
    {
        this._cdPerdcComprovante = cdPerdcComprovante;
        this._has_cdPerdcComprovante = true;
    } //-- void setCdPerdcComprovante(int) 

    /**
     * Sets the value of field 'cdPerdcConsVeiculo'.
     * 
     * @param cdPerdcConsVeiculo the value of field
     * 'cdPerdcConsVeiculo'.
     */
    public void setCdPerdcConsVeiculo(int cdPerdcConsVeiculo)
    {
        this._cdPerdcConsVeiculo = cdPerdcConsVeiculo;
        this._has_cdPerdcConsVeiculo = true;
    } //-- void setCdPerdcConsVeiculo(int) 

    /**
     * Sets the value of field 'cdPerdcEnvioRemessa'.
     * 
     * @param cdPerdcEnvioRemessa the value of field
     * 'cdPerdcEnvioRemessa'.
     */
    public void setCdPerdcEnvioRemessa(int cdPerdcEnvioRemessa)
    {
        this._cdPerdcEnvioRemessa = cdPerdcEnvioRemessa;
        this._has_cdPerdcEnvioRemessa = true;
    } //-- void setCdPerdcEnvioRemessa(int) 

    /**
     * Sets the value of field 'cdPerdcManutencaoProcd'.
     * 
     * @param cdPerdcManutencaoProcd the value of field
     * 'cdPerdcManutencaoProcd'.
     */
    public void setCdPerdcManutencaoProcd(int cdPerdcManutencaoProcd)
    {
        this._cdPerdcManutencaoProcd = cdPerdcManutencaoProcd;
        this._has_cdPerdcManutencaoProcd = true;
    } //-- void setCdPerdcManutencaoProcd(int) 

    /**
     * Sets the value of field 'cdPermissaoDebitoOnline'.
     * 
     * @param cdPermissaoDebitoOnline the value of field
     * 'cdPermissaoDebitoOnline'.
     */
    public void setCdPermissaoDebitoOnline(int cdPermissaoDebitoOnline)
    {
        this._cdPermissaoDebitoOnline = cdPermissaoDebitoOnline;
        this._has_cdPermissaoDebitoOnline = true;
    } //-- void setCdPermissaoDebitoOnline(int) 

    /**
     * Sets the value of field 'cdPrincipalEnquaRcadt'.
     * 
     * @param cdPrincipalEnquaRcadt the value of field
     * 'cdPrincipalEnquaRcadt'.
     */
    public void setCdPrincipalEnquaRcadt(int cdPrincipalEnquaRcadt)
    {
        this._cdPrincipalEnquaRcadt = cdPrincipalEnquaRcadt;
        this._has_cdPrincipalEnquaRcadt = true;
    } //-- void setCdPrincipalEnquaRcadt(int) 

    /**
     * Sets the value of field 'cdPriorEfetivacaoPagamento'.
     * 
     * @param cdPriorEfetivacaoPagamento the value of field
     * 'cdPriorEfetivacaoPagamento'.
     */
    public void setCdPriorEfetivacaoPagamento(int cdPriorEfetivacaoPagamento)
    {
        this._cdPriorEfetivacaoPagamento = cdPriorEfetivacaoPagamento;
        this._has_cdPriorEfetivacaoPagamento = true;
    } //-- void setCdPriorEfetivacaoPagamento(int) 

    /**
     * Sets the value of field 'cdRastreabNotaFiscal'.
     * 
     * @param cdRastreabNotaFiscal the value of field
     * 'cdRastreabNotaFiscal'.
     */
    public void setCdRastreabNotaFiscal(int cdRastreabNotaFiscal)
    {
        this._cdRastreabNotaFiscal = cdRastreabNotaFiscal;
        this._has_cdRastreabNotaFiscal = true;
    } //-- void setCdRastreabNotaFiscal(int) 

    /**
     * Sets the value of field 'cdRastreabTituloTerc'.
     * 
     * @param cdRastreabTituloTerc the value of field
     * 'cdRastreabTituloTerc'.
     */
    public void setCdRastreabTituloTerc(int cdRastreabTituloTerc)
    {
        this._cdRastreabTituloTerc = cdRastreabTituloTerc;
        this._has_cdRastreabTituloTerc = true;
    } //-- void setCdRastreabTituloTerc(int) 

    /**
     * Sets the value of field 'cdRejeiAgendaLote'.
     * 
     * @param cdRejeiAgendaLote the value of field
     * 'cdRejeiAgendaLote'.
     */
    public void setCdRejeiAgendaLote(int cdRejeiAgendaLote)
    {
        this._cdRejeiAgendaLote = cdRejeiAgendaLote;
        this._has_cdRejeiAgendaLote = true;
    } //-- void setCdRejeiAgendaLote(int) 

    /**
     * Sets the value of field 'cdRejeiEfetivacaoLote'.
     * 
     * @param cdRejeiEfetivacaoLote the value of field
     * 'cdRejeiEfetivacaoLote'.
     */
    public void setCdRejeiEfetivacaoLote(int cdRejeiEfetivacaoLote)
    {
        this._cdRejeiEfetivacaoLote = cdRejeiEfetivacaoLote;
        this._has_cdRejeiEfetivacaoLote = true;
    } //-- void setCdRejeiEfetivacaoLote(int) 

    /**
     * Sets the value of field 'cdRejeiLote'.
     * 
     * @param cdRejeiLote the value of field 'cdRejeiLote'.
     */
    public void setCdRejeiLote(int cdRejeiLote)
    {
        this._cdRejeiLote = cdRejeiLote;
        this._has_cdRejeiLote = true;
    } //-- void setCdRejeiLote(int) 

    /**
     * Sets the value of field 'cdTipoCargaRcadt'.
     * 
     * @param cdTipoCargaRcadt the value of field 'cdTipoCargaRcadt'
     */
    public void setCdTipoCargaRcadt(int cdTipoCargaRcadt)
    {
        this._cdTipoCargaRcadt = cdTipoCargaRcadt;
        this._has_cdTipoCargaRcadt = true;
    } //-- void setCdTipoCargaRcadt(int) 

    /**
     * Sets the value of field 'cdTipoCataoSalarial'.
     * 
     * @param cdTipoCataoSalarial the value of field
     * 'cdTipoCataoSalarial'.
     */
    public void setCdTipoCataoSalarial(int cdTipoCataoSalarial)
    {
        this._cdTipoCataoSalarial = cdTipoCataoSalarial;
        this._has_cdTipoCataoSalarial = true;
    } //-- void setCdTipoCataoSalarial(int) 

    /**
     * Sets the value of field 'cdTipoConsistenciaLista'.
     * 
     * @param cdTipoConsistenciaLista the value of field
     * 'cdTipoConsistenciaLista'.
     */
    public void setCdTipoConsistenciaLista(int cdTipoConsistenciaLista)
    {
        this._cdTipoConsistenciaLista = cdTipoConsistenciaLista;
        this._has_cdTipoConsistenciaLista = true;
    } //-- void setCdTipoConsistenciaLista(int) 

    /**
     * Sets the value of field 'cdTipoConsultaComprovante'.
     * 
     * @param cdTipoConsultaComprovante the value of field
     * 'cdTipoConsultaComprovante'.
     */
    public void setCdTipoConsultaComprovante(int cdTipoConsultaComprovante)
    {
        this._cdTipoConsultaComprovante = cdTipoConsultaComprovante;
        this._has_cdTipoConsultaComprovante = true;
    } //-- void setCdTipoConsultaComprovante(int) 

    /**
     * Sets the value of field 'cdTipoContaFavorecido'.
     * 
     * @param cdTipoContaFavorecido the value of field
     * 'cdTipoContaFavorecido'.
     */
    public void setCdTipoContaFavorecido(int cdTipoContaFavorecido)
    {
        this._cdTipoContaFavorecido = cdTipoContaFavorecido;
        this._has_cdTipoContaFavorecido = true;
    } //-- void setCdTipoContaFavorecido(int) 

    /**
     * Sets the value of field 'cdTipoDataFloat'.
     * 
     * @param cdTipoDataFloat the value of field 'cdTipoDataFloat'.
     */
    public void setCdTipoDataFloat(int cdTipoDataFloat)
    {
        this._cdTipoDataFloat = cdTipoDataFloat;
        this._has_cdTipoDataFloat = true;
    } //-- void setCdTipoDataFloat(int) 

    /**
     * Sets the value of field 'cdTipoDivergenciaVeiculo'.
     * 
     * @param cdTipoDivergenciaVeiculo the value of field
     * 'cdTipoDivergenciaVeiculo'.
     */
    public void setCdTipoDivergenciaVeiculo(int cdTipoDivergenciaVeiculo)
    {
        this._cdTipoDivergenciaVeiculo = cdTipoDivergenciaVeiculo;
        this._has_cdTipoDivergenciaVeiculo = true;
    } //-- void setCdTipoDivergenciaVeiculo(int) 

    /**
     * Sets the value of field 'cdTipoFormacaoLista'.
     * 
     * @param cdTipoFormacaoLista the value of field
     * 'cdTipoFormacaoLista'.
     */
    public void setCdTipoFormacaoLista(int cdTipoFormacaoLista)
    {
        this._cdTipoFormacaoLista = cdTipoFormacaoLista;
        this._has_cdTipoFormacaoLista = true;
    } //-- void setCdTipoFormacaoLista(int) 

    /**
     * Sets the value of field 'cdTipoIdBenef'.
     * 
     * @param cdTipoIdBenef the value of field 'cdTipoIdBenef'.
     */
    public void setCdTipoIdBenef(int cdTipoIdBenef)
    {
        this._cdTipoIdBenef = cdTipoIdBenef;
        this._has_cdTipoIdBenef = true;
    } //-- void setCdTipoIdBenef(int) 

    /**
     * Sets the value of field 'cdTipoIsncricaoFavorecido'.
     * 
     * @param cdTipoIsncricaoFavorecido the value of field
     * 'cdTipoIsncricaoFavorecido'.
     */
    public void setCdTipoIsncricaoFavorecido(int cdTipoIsncricaoFavorecido)
    {
        this._cdTipoIsncricaoFavorecido = cdTipoIsncricaoFavorecido;
        this._has_cdTipoIsncricaoFavorecido = true;
    } //-- void setCdTipoIsncricaoFavorecido(int) 

    /**
     * Sets the value of field 'cdTipoLayoutArquivo'.
     * 
     * @param cdTipoLayoutArquivo the value of field
     * 'cdTipoLayoutArquivo'.
     */
    public void setCdTipoLayoutArquivo(int cdTipoLayoutArquivo)
    {
        this._cdTipoLayoutArquivo = cdTipoLayoutArquivo;
        this._has_cdTipoLayoutArquivo = true;
    } //-- void setCdTipoLayoutArquivo(int) 

    /**
     * Sets the value of field 'cdTipoReajusteTarifa'.
     * 
     * @param cdTipoReajusteTarifa the value of field
     * 'cdTipoReajusteTarifa'.
     */
    public void setCdTipoReajusteTarifa(int cdTipoReajusteTarifa)
    {
        this._cdTipoReajusteTarifa = cdTipoReajusteTarifa;
        this._has_cdTipoReajusteTarifa = true;
    } //-- void setCdTipoReajusteTarifa(int) 

    /**
     * Sets the value of field 'cdTratoContaTransf'.
     * 
     * @param cdTratoContaTransf the value of field
     * 'cdTratoContaTransf'.
     */
    public void setCdTratoContaTransf(int cdTratoContaTransf)
    {
        this._cdTratoContaTransf = cdTratoContaTransf;
        this._has_cdTratoContaTransf = true;
    } //-- void setCdTratoContaTransf(int) 

    /**
     * Sets the value of field 'cdUtilzFavorecidoCtrl'.
     * 
     * @param cdUtilzFavorecidoCtrl the value of field
     * 'cdUtilzFavorecidoCtrl'.
     */
    public void setCdUtilzFavorecidoCtrl(int cdUtilzFavorecidoCtrl)
    {
        this._cdUtilzFavorecidoCtrl = cdUtilzFavorecidoCtrl;
        this._has_cdUtilzFavorecidoCtrl = true;
    } //-- void setCdUtilzFavorecidoCtrl(int) 

    /**
     * Sets the value of field 'cdValidacaoNomeFavorecido'.
     * 
     * @param cdValidacaoNomeFavorecido the value of field
     * 'cdValidacaoNomeFavorecido'.
     */
    public void setCdValidacaoNomeFavorecido(int cdValidacaoNomeFavorecido)
    {
        this._cdValidacaoNomeFavorecido = cdValidacaoNomeFavorecido;
        this._has_cdValidacaoNomeFavorecido = true;
    } //-- void setCdValidacaoNomeFavorecido(int) 

    /**
     * Sets the value of field 'codMensagem'.
     * 
     * @param codMensagem the value of field 'codMensagem'.
     */
    public void setCodMensagem(java.lang.String codMensagem)
    {
        this._codMensagem = codMensagem;
    } //-- void setCodMensagem(java.lang.String) 

    /**
     * Sets the value of field 'mensagem'.
     * 
     * @param mensagem the value of field 'mensagem'.
     */
    public void setMensagem(java.lang.String mensagem)
    {
        this._mensagem = mensagem;
    } //-- void setMensagem(java.lang.String) 

    /**
     * Sets the value of field 'nrFechamentoApurcTarifa'.
     * 
     * @param nrFechamentoApurcTarifa the value of field
     * 'nrFechamentoApurcTarifa'.
     */
    public void setNrFechamentoApurcTarifa(int nrFechamentoApurcTarifa)
    {
        this._nrFechamentoApurcTarifa = nrFechamentoApurcTarifa;
        this._has_nrFechamentoApurcTarifa = true;
    } //-- void setNrFechamentoApurcTarifa(int) 

    /**
     * Sets the value of field 'qtAntecedencia'.
     * 
     * @param qtAntecedencia the value of field 'qtAntecedencia'.
     */
    public void setQtAntecedencia(int qtAntecedencia)
    {
        this._qtAntecedencia = qtAntecedencia;
        this._has_qtAntecedencia = true;
    } //-- void setQtAntecedencia(int) 

    /**
     * Sets the value of field 'qtAnteriorVencimentoCompv'.
     * 
     * @param qtAnteriorVencimentoCompv the value of field
     * 'qtAnteriorVencimentoCompv'.
     */
    public void setQtAnteriorVencimentoCompv(int qtAnteriorVencimentoCompv)
    {
        this._qtAnteriorVencimentoCompv = qtAnteriorVencimentoCompv;
        this._has_qtAnteriorVencimentoCompv = true;
    } //-- void setQtAnteriorVencimentoCompv(int) 

    /**
     * Sets the value of field 'qtDiaCobrancaTarifa'.
     * 
     * @param qtDiaCobrancaTarifa the value of field
     * 'qtDiaCobrancaTarifa'.
     */
    public void setQtDiaCobrancaTarifa(int qtDiaCobrancaTarifa)
    {
        this._qtDiaCobrancaTarifa = qtDiaCobrancaTarifa;
        this._has_qtDiaCobrancaTarifa = true;
    } //-- void setQtDiaCobrancaTarifa(int) 

    /**
     * Sets the value of field 'qtDiaExpiracao'.
     * 
     * @param qtDiaExpiracao the value of field 'qtDiaExpiracao'.
     */
    public void setQtDiaExpiracao(int qtDiaExpiracao)
    {
        this._qtDiaExpiracao = qtDiaExpiracao;
        this._has_qtDiaExpiracao = true;
    } //-- void setQtDiaExpiracao(int) 

    /**
     * Sets the value of field 'qtDiaFloatPagamento'.
     * 
     * @param qtDiaFloatPagamento the value of field
     * 'qtDiaFloatPagamento'.
     */
    public void setQtDiaFloatPagamento(int qtDiaFloatPagamento)
    {
        this._qtDiaFloatPagamento = qtDiaFloatPagamento;
        this._has_qtDiaFloatPagamento = true;
    } //-- void setQtDiaFloatPagamento(int) 

    /**
     * Sets the value of field 'qtDiaInativFavorecido'.
     * 
     * @param qtDiaInativFavorecido the value of field
     * 'qtDiaInativFavorecido'.
     */
    public void setQtDiaInativFavorecido(int qtDiaInativFavorecido)
    {
        this._qtDiaInativFavorecido = qtDiaInativFavorecido;
        this._has_qtDiaInativFavorecido = true;
    } //-- void setQtDiaInativFavorecido(int) 

    /**
     * Sets the value of field 'qtDiaRepiqCons'.
     * 
     * @param qtDiaRepiqCons the value of field 'qtDiaRepiqCons'.
     */
    public void setQtDiaRepiqCons(int qtDiaRepiqCons)
    {
        this._qtDiaRepiqCons = qtDiaRepiqCons;
        this._has_qtDiaRepiqCons = true;
    } //-- void setQtDiaRepiqCons(int) 

    /**
     * Sets the value of field 'qtEtapaRcadtBeneficiario'.
     * 
     * @param qtEtapaRcadtBeneficiario the value of field
     * 'qtEtapaRcadtBeneficiario'.
     */
    public void setQtEtapaRcadtBeneficiario(int qtEtapaRcadtBeneficiario)
    {
        this._qtEtapaRcadtBeneficiario = qtEtapaRcadtBeneficiario;
        this._has_qtEtapaRcadtBeneficiario = true;
    } //-- void setQtEtapaRcadtBeneficiario(int) 

    /**
     * Sets the value of field 'qtFaseRcadtBenefiario'.
     * 
     * @param qtFaseRcadtBenefiario the value of field
     * 'qtFaseRcadtBenefiario'.
     */
    public void setQtFaseRcadtBenefiario(int qtFaseRcadtBenefiario)
    {
        this._qtFaseRcadtBenefiario = qtFaseRcadtBenefiario;
        this._has_qtFaseRcadtBenefiario = true;
    } //-- void setQtFaseRcadtBenefiario(int) 

    /**
     * Sets the value of field 'qtLimiteLinhas'.
     * 
     * @param qtLimiteLinhas the value of field 'qtLimiteLinhas'.
     */
    public void setQtLimiteLinhas(int qtLimiteLinhas)
    {
        this._qtLimiteLinhas = qtLimiteLinhas;
        this._has_qtLimiteLinhas = true;
    } //-- void setQtLimiteLinhas(int) 

    /**
     * Sets the value of field 'qtLimiteSolicitacaoCatao'.
     * 
     * @param qtLimiteSolicitacaoCatao the value of field
     * 'qtLimiteSolicitacaoCatao'.
     */
    public void setQtLimiteSolicitacaoCatao(int qtLimiteSolicitacaoCatao)
    {
        this._qtLimiteSolicitacaoCatao = qtLimiteSolicitacaoCatao;
        this._has_qtLimiteSolicitacaoCatao = true;
    } //-- void setQtLimiteSolicitacaoCatao(int) 

    /**
     * Sets the value of field 'qtMaximaInconLote'.
     * 
     * @param qtMaximaInconLote the value of field
     * 'qtMaximaInconLote'.
     */
    public void setQtMaximaInconLote(int qtMaximaInconLote)
    {
        this._qtMaximaInconLote = qtMaximaInconLote;
        this._has_qtMaximaInconLote = true;
    } //-- void setQtMaximaInconLote(int) 

    /**
     * Sets the value of field 'qtMaximaTituloVencido'.
     * 
     * @param qtMaximaTituloVencido the value of field
     * 'qtMaximaTituloVencido'.
     */
    public void setQtMaximaTituloVencido(int qtMaximaTituloVencido)
    {
        this._qtMaximaTituloVencido = qtMaximaTituloVencido;
        this._has_qtMaximaTituloVencido = true;
    } //-- void setQtMaximaTituloVencido(int) 

    /**
     * Sets the value of field 'qtMesComprovante'.
     * 
     * @param qtMesComprovante the value of field 'qtMesComprovante'
     */
    public void setQtMesComprovante(int qtMesComprovante)
    {
        this._qtMesComprovante = qtMesComprovante;
        this._has_qtMesComprovante = true;
    } //-- void setQtMesComprovante(int) 

    /**
     * Sets the value of field 'qtMesFaseRcadt'.
     * 
     * @param qtMesFaseRcadt the value of field 'qtMesFaseRcadt'.
     */
    public void setQtMesFaseRcadt(int qtMesFaseRcadt)
    {
        this._qtMesFaseRcadt = qtMesFaseRcadt;
        this._has_qtMesFaseRcadt = true;
    } //-- void setQtMesFaseRcadt(int) 

    /**
     * Sets the value of field 'qtMesRcadt'.
     * 
     * @param qtMesRcadt the value of field 'qtMesRcadt'.
     */
    public void setQtMesRcadt(int qtMesRcadt)
    {
        this._qtMesRcadt = qtMesRcadt;
        this._has_qtMesRcadt = true;
    } //-- void setQtMesRcadt(int) 

    /**
     * Sets the value of field 'qtMesReajusteTarifa'.
     * 
     * @param qtMesReajusteTarifa the value of field
     * 'qtMesReajusteTarifa'.
     */
    public void setQtMesReajusteTarifa(int qtMesReajusteTarifa)
    {
        this._qtMesReajusteTarifa = qtMesReajusteTarifa;
        this._has_qtMesReajusteTarifa = true;
    } //-- void setQtMesReajusteTarifa(int) 

    /**
     * Sets the value of field 'qtViaAviso'.
     * 
     * @param qtViaAviso the value of field 'qtViaAviso'.
     */
    public void setQtViaAviso(int qtViaAviso)
    {
        this._qtViaAviso = qtViaAviso;
        this._has_qtViaAviso = true;
    } //-- void setQtViaAviso(int) 

    /**
     * Sets the value of field 'qtViaCobranca'.
     * 
     * @param qtViaCobranca the value of field 'qtViaCobranca'.
     */
    public void setQtViaCobranca(int qtViaCobranca)
    {
        this._qtViaCobranca = qtViaCobranca;
        this._has_qtViaCobranca = true;
    } //-- void setQtViaCobranca(int) 

    /**
     * Sets the value of field 'qtViaComprovante'.
     * 
     * @param qtViaComprovante the value of field 'qtViaComprovante'
     */
    public void setQtViaComprovante(int qtViaComprovante)
    {
        this._qtViaComprovante = qtViaComprovante;
        this._has_qtViaComprovante = true;
    } //-- void setQtViaComprovante(int) 

    /**
     * Sets the value of field 'vlFavorecidoNaoCadtr'.
     * 
     * @param vlFavorecidoNaoCadtr the value of field
     * 'vlFavorecidoNaoCadtr'.
     */
    public void setVlFavorecidoNaoCadtr(java.math.BigDecimal vlFavorecidoNaoCadtr)
    {
        this._vlFavorecidoNaoCadtr = vlFavorecidoNaoCadtr;
    } //-- void setVlFavorecidoNaoCadtr(java.math.BigDecimal) 

    /**
     * Sets the value of field 'vlLimiteDiaPagamento'.
     * 
     * @param vlLimiteDiaPagamento the value of field
     * 'vlLimiteDiaPagamento'.
     */
    public void setVlLimiteDiaPagamento(java.math.BigDecimal vlLimiteDiaPagamento)
    {
        this._vlLimiteDiaPagamento = vlLimiteDiaPagamento;
    } //-- void setVlLimiteDiaPagamento(java.math.BigDecimal) 

    /**
     * Sets the value of field 'vlLimiteIndvdPagamento'.
     * 
     * @param vlLimiteIndvdPagamento the value of field
     * 'vlLimiteIndvdPagamento'.
     */
    public void setVlLimiteIndvdPagamento(java.math.BigDecimal vlLimiteIndvdPagamento)
    {
        this._vlLimiteIndvdPagamento = vlLimiteIndvdPagamento;
    } //-- void setVlLimiteIndvdPagamento(java.math.BigDecimal) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ConsultarConfigPadraoAtribTipoServModResponse
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultarconfigpadraoatribtiposervmod.response.ConsultarConfigPadraoAtribTipoServModResponse unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultarconfigpadraoatribtiposervmod.response.ConsultarConfigPadraoAtribTipoServModResponse) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultarconfigpadraoatribtiposervmod.response.ConsultarConfigPadraoAtribTipoServModResponse.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarconfigpadraoatribtiposervmod.response.ConsultarConfigPadraoAtribTipoServModResponse unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
