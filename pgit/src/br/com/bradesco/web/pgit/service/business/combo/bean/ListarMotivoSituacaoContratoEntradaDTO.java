/*
 * Nome: br.com.bradesco.web.pgit.service.business.combo.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.combo.bean;

/**
 * Nome: ListarMotivoSituacaoContratoEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarMotivoSituacaoContratoEntradaDTO {
	
	/** Atributo cdSituacao. */
	private int cdSituacao;

	/**
	 * Get: cdSituacao.
	 *
	 * @return cdSituacao
	 */
	public int getCdSituacao() {
		return cdSituacao;
	}

	/**
	 * Set: cdSituacao.
	 *
	 * @param cdSituacao the cd situacao
	 */
	public void setCdSituacao(int cdSituacao) {
		this.cdSituacao = cdSituacao;
	}

}
