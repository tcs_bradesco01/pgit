/*
 * Nome: br.com.bradesco.web.pgit.service.business.cadproccontrole.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.cadproccontrole.bean;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Nome: EstatisticaProcessamentoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class EstatisticaProcessamentoSaidaDTO implements Serializable {

	/** Atributo serialVersionUID. */
	private static final long serialVersionUID = 8221994707966387227L;

	/** Atributo cdSistema. */
	private String cdSistema;

    /** Atributo cdProcessoSistema. */
    private Integer cdProcessoSistema;

    /** Atributo idTipoProcessoSIstema. */
    private String idTipoProcessoSIstema;

    /** Atributo hrInicioEstatisticaProcessoInicio. */
    private String hrInicioEstatisticaProcessoInicio;

    /** Atributo hrInicioEstatisticaProcessoFim. */
    private String hrInicioEstatisticaProcessoFim;

    /** Atributo cdSituacaoEstatisticaSistema. */
    private Integer cdSituacaoEstatisticaSistema;

    /** Atributo dsSituacaoEstatisticaSistema. */
    private String dsSituacaoEstatisticaSistema;

    /** Atributo qtRegistroProcessoSistema. */
    private Integer qtRegistroProcessoSistema;

    /** Atributo vlRegistroProcessoSistema. */
    private BigDecimal vlRegistroProcessoSistema;

	/**
	 * Get: cdProcessoSistema.
	 *
	 * @return cdProcessoSistema
	 */
	public Integer getCdProcessoSistema() {
		return cdProcessoSistema;
	}

	/**
	 * Set: cdProcessoSistema.
	 *
	 * @param cdProcessoSistema the cd processo sistema
	 */
	public void setCdProcessoSistema(Integer cdProcessoSistema) {
		this.cdProcessoSistema = cdProcessoSistema;
	}

	/**
	 * Get: cdSistema.
	 *
	 * @return cdSistema
	 */
	public String getCdSistema() {
		return cdSistema;
	}

	/**
	 * Set: cdSistema.
	 *
	 * @param cdSistema the cd sistema
	 */
	public void setCdSistema(String cdSistema) {
		this.cdSistema = cdSistema;
	}

	/**
	 * Get: cdSituacaoEstatisticaSistema.
	 *
	 * @return cdSituacaoEstatisticaSistema
	 */
	public Integer getCdSituacaoEstatisticaSistema() {
		return cdSituacaoEstatisticaSistema;
	}

	/**
	 * Set: cdSituacaoEstatisticaSistema.
	 *
	 * @param cdSituacaoEstatisticaSistema the cd situacao estatistica sistema
	 */
	public void setCdSituacaoEstatisticaSistema(Integer cdSituacaoEstatisticaSistema) {
		this.cdSituacaoEstatisticaSistema = cdSituacaoEstatisticaSistema;
	}

	/**
	 * Get: dsSituacaoEstatisticaSistema.
	 *
	 * @return dsSituacaoEstatisticaSistema
	 */
	public String getDsSituacaoEstatisticaSistema() {
		return dsSituacaoEstatisticaSistema;
	}

	/**
	 * Set: dsSituacaoEstatisticaSistema.
	 *
	 * @param dsSituacaoEstatisticaSistema the ds situacao estatistica sistema
	 */
	public void setDsSituacaoEstatisticaSistema(String dsSituacaoEstatisticaSistema) {
		this.dsSituacaoEstatisticaSistema = dsSituacaoEstatisticaSistema;
	}

	/**
	 * Get: hrInicioEstatisticaProcessoFim.
	 *
	 * @return hrInicioEstatisticaProcessoFim
	 */
	public String getHrInicioEstatisticaProcessoFim() {
		return hrInicioEstatisticaProcessoFim;
	}

	/**
	 * Set: hrInicioEstatisticaProcessoFim.
	 *
	 * @param hrInicioEstatisticaProcessoFim the hr inicio estatistica processo fim
	 */
	public void setHrInicioEstatisticaProcessoFim(
			String hrInicioEstatisticaProcessoFim) {
		this.hrInicioEstatisticaProcessoFim = hrInicioEstatisticaProcessoFim;
	}

	/**
	 * Get: hrInicioEstatisticaProcessoInicio.
	 *
	 * @return hrInicioEstatisticaProcessoInicio
	 */
	public String getHrInicioEstatisticaProcessoInicio() {
		return hrInicioEstatisticaProcessoInicio;
	}

	/**
	 * Set: hrInicioEstatisticaProcessoInicio.
	 *
	 * @param hrInicioEstatisticaProcessoInicio the hr inicio estatistica processo inicio
	 */
	public void setHrInicioEstatisticaProcessoInicio(
			String hrInicioEstatisticaProcessoInicio) {
		this.hrInicioEstatisticaProcessoInicio = hrInicioEstatisticaProcessoInicio;
	}

	/**
	 * Get: idTipoProcessoSIstema.
	 *
	 * @return idTipoProcessoSIstema
	 */
	public String getIdTipoProcessoSIstema() {
		return idTipoProcessoSIstema;
	}

	/**
	 * Set: idTipoProcessoSIstema.
	 *
	 * @param idTipoProcessoSIstema the id tipo processo s istema
	 */
	public void setIdTipoProcessoSIstema(String idTipoProcessoSIstema) {
		this.idTipoProcessoSIstema = idTipoProcessoSIstema;
	}

	/**
	 * Get: qtRegistroProcessoSistema.
	 *
	 * @return qtRegistroProcessoSistema
	 */
	public Integer getQtRegistroProcessoSistema() {
		return qtRegistroProcessoSistema;
	}

	/**
	 * Set: qtRegistroProcessoSistema.
	 *
	 * @param qtRegistroProcessoSistema the qt registro processo sistema
	 */
	public void setQtRegistroProcessoSistema(Integer qtRegistroProcessoSistema) {
		this.qtRegistroProcessoSistema = qtRegistroProcessoSistema;
	}

	/**
	 * Get: vlRegistroProcessoSistema.
	 *
	 * @return vlRegistroProcessoSistema
	 */
	public BigDecimal getVlRegistroProcessoSistema() {
		return vlRegistroProcessoSistema;
	}

	/**
	 * Set: vlRegistroProcessoSistema.
	 *
	 * @param vlRegistroProcessoSistema the vl registro processo sistema
	 */
	public void setVlRegistroProcessoSistema(BigDecimal vlRegistroProcessoSistema) {
		this.vlRegistroProcessoSistema = vlRegistroProcessoSistema;
	}

}
