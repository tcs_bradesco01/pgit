/*
 * Nome: br.com.bradesco.web.pgit.service.business.regsegregacessodados.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.regsegregacessodados.bean;

/**
 * Nome: ExcRegSegregAcessoDadosDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ExcRegSegregAcessoDadosDTO {
	
	/** Atributo linhaSelecionada. */
	private int linhaSelecionada;
	
	/** Atributo codigoExcecao. */
	private String codigoExcecao;
	
	/** Atributo tipoUnidadeOrganizacionalUsuario. */
	private String tipoUnidadeOrganizacionalUsuario;
	
	/** Atributo tipoUnidadeOrganizacionalProprietarioDados. */
	private String tipoUnidadeOrganizacionalProprietarioDados;
	
	/** Atributo tipoExcecao. */
	private String tipoExcecao;
	
	/** Atributo abrangenciaExcecao. */
	private String abrangenciaExcecao;
	
	/**
	 * Get: abrangenciaExcecao.
	 *
	 * @return abrangenciaExcecao
	 */
	public String getAbrangenciaExcecao() {
		return abrangenciaExcecao;
	}
	
	/**
	 * Set: abrangenciaExcecao.
	 *
	 * @param abrangenciaExcecao the abrangencia excecao
	 */
	public void setAbrangenciaExcecao(String abrangenciaExcecao) {
		this.abrangenciaExcecao = abrangenciaExcecao;
	}
	
	/**
	 * Get: codigoExcecao.
	 *
	 * @return codigoExcecao
	 */
	public String getCodigoExcecao() {
		return codigoExcecao;
	}
	
	/**
	 * Set: codigoExcecao.
	 *
	 * @param codigoExcecao the codigo excecao
	 */
	public void setCodigoExcecao(String codigoExcecao) {
		this.codigoExcecao = codigoExcecao;
	}
	
	/**
	 * Get: linhaSelecionada.
	 *
	 * @return linhaSelecionada
	 */
	public int getLinhaSelecionada() {
		return linhaSelecionada;
	}
	
	/**
	 * Set: linhaSelecionada.
	 *
	 * @param linhaSelecionada the linha selecionada
	 */
	public void setLinhaSelecionada(int linhaSelecionada) {
		this.linhaSelecionada = linhaSelecionada;
	}
	
	/**
	 * Get: tipoExcecao.
	 *
	 * @return tipoExcecao
	 */
	public String getTipoExcecao() {
		return tipoExcecao;
	}
	
	/**
	 * Set: tipoExcecao.
	 *
	 * @param tipoExcecao the tipo excecao
	 */
	public void setTipoExcecao(String tipoExcecao) {
		this.tipoExcecao = tipoExcecao;
	}
	
	/**
	 * Get: tipoUnidadeOrganizacionalProprietarioDados.
	 *
	 * @return tipoUnidadeOrganizacionalProprietarioDados
	 */
	public String getTipoUnidadeOrganizacionalProprietarioDados() {
		return tipoUnidadeOrganizacionalProprietarioDados;
	}
	
	/**
	 * Set: tipoUnidadeOrganizacionalProprietarioDados.
	 *
	 * @param tipoUnidadeOrganizacionalProprietarioDados the tipo unidade organizacional proprietario dados
	 */
	public void setTipoUnidadeOrganizacionalProprietarioDados(
			String tipoUnidadeOrganizacionalProprietarioDados) {
		this.tipoUnidadeOrganizacionalProprietarioDados = tipoUnidadeOrganizacionalProprietarioDados;
	}
	
	/**
	 * Get: tipoUnidadeOrganizacionalUsuario.
	 *
	 * @return tipoUnidadeOrganizacionalUsuario
	 */
	public String getTipoUnidadeOrganizacionalUsuario() {
		return tipoUnidadeOrganizacionalUsuario;
	}
	
	/**
	 * Set: tipoUnidadeOrganizacionalUsuario.
	 *
	 * @param tipoUnidadeOrganizacionalUsuario the tipo unidade organizacional usuario
	 */
	public void setTipoUnidadeOrganizacionalUsuario(
			String tipoUnidadeOrganizacionalUsuario) {
		this.tipoUnidadeOrganizacionalUsuario = tipoUnidadeOrganizacionalUsuario;
	}
	
	
	
	
}
