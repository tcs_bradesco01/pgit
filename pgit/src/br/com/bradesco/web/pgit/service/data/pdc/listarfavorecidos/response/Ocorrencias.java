/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listarfavorecidos.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdFavorecidoClientePagador
     */
    private long _cdFavorecidoClientePagador = 0;

    /**
     * keeps track of state for field: _cdFavorecidoClientePagador
     */
    private boolean _has_cdFavorecidoClientePagador;

    /**
     * Field _nmFavorecido
     */
    private java.lang.String _nmFavorecido;

    /**
     * Field _cdTipoFavorecido
     */
    private int _cdTipoFavorecido = 0;

    /**
     * keeps track of state for field: _cdTipoFavorecido
     */
    private boolean _has_cdTipoFavorecido;

    /**
     * Field _dsTipoFavorecido
     */
    private java.lang.String _dsTipoFavorecido;

    /**
     * Field _cdInscricaoFavorecidoCliente
     */
    private long _cdInscricaoFavorecidoCliente = 0;

    /**
     * keeps track of state for field: _cdInscricaoFavorecidoCliente
     */
    private boolean _has_cdInscricaoFavorecidoCliente;

    /**
     * Field _cdTipoInscricaoFavorecido
     */
    private int _cdTipoInscricaoFavorecido = 0;

    /**
     * keeps track of state for field: _cdTipoInscricaoFavorecido
     */
    private boolean _has_cdTipoInscricaoFavorecido;

    /**
     * Field _dsTipoInscricaoFavorecido
     */
    private java.lang.String _dsTipoInscricaoFavorecido;

    /**
     * Field _stFavorecido
     */
    private java.lang.String _stFavorecido;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarfavorecidos.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdFavorecidoClientePagador
     * 
     */
    public void deleteCdFavorecidoClientePagador()
    {
        this._has_cdFavorecidoClientePagador= false;
    } //-- void deleteCdFavorecidoClientePagador() 

    /**
     * Method deleteCdInscricaoFavorecidoCliente
     * 
     */
    public void deleteCdInscricaoFavorecidoCliente()
    {
        this._has_cdInscricaoFavorecidoCliente= false;
    } //-- void deleteCdInscricaoFavorecidoCliente() 

    /**
     * Method deleteCdTipoFavorecido
     * 
     */
    public void deleteCdTipoFavorecido()
    {
        this._has_cdTipoFavorecido= false;
    } //-- void deleteCdTipoFavorecido() 

    /**
     * Method deleteCdTipoInscricaoFavorecido
     * 
     */
    public void deleteCdTipoInscricaoFavorecido()
    {
        this._has_cdTipoInscricaoFavorecido= false;
    } //-- void deleteCdTipoInscricaoFavorecido() 

    /**
     * Returns the value of field 'cdFavorecidoClientePagador'.
     * 
     * @return long
     * @return the value of field 'cdFavorecidoClientePagador'.
     */
    public long getCdFavorecidoClientePagador()
    {
        return this._cdFavorecidoClientePagador;
    } //-- long getCdFavorecidoClientePagador() 

    /**
     * Returns the value of field 'cdInscricaoFavorecidoCliente'.
     * 
     * @return long
     * @return the value of field 'cdInscricaoFavorecidoCliente'.
     */
    public long getCdInscricaoFavorecidoCliente()
    {
        return this._cdInscricaoFavorecidoCliente;
    } //-- long getCdInscricaoFavorecidoCliente() 

    /**
     * Returns the value of field 'cdTipoFavorecido'.
     * 
     * @return int
     * @return the value of field 'cdTipoFavorecido'.
     */
    public int getCdTipoFavorecido()
    {
        return this._cdTipoFavorecido;
    } //-- int getCdTipoFavorecido() 

    /**
     * Returns the value of field 'cdTipoInscricaoFavorecido'.
     * 
     * @return int
     * @return the value of field 'cdTipoInscricaoFavorecido'.
     */
    public int getCdTipoInscricaoFavorecido()
    {
        return this._cdTipoInscricaoFavorecido;
    } //-- int getCdTipoInscricaoFavorecido() 

    /**
     * Returns the value of field 'dsTipoFavorecido'.
     * 
     * @return String
     * @return the value of field 'dsTipoFavorecido'.
     */
    public java.lang.String getDsTipoFavorecido()
    {
        return this._dsTipoFavorecido;
    } //-- java.lang.String getDsTipoFavorecido() 

    /**
     * Returns the value of field 'dsTipoInscricaoFavorecido'.
     * 
     * @return String
     * @return the value of field 'dsTipoInscricaoFavorecido'.
     */
    public java.lang.String getDsTipoInscricaoFavorecido()
    {
        return this._dsTipoInscricaoFavorecido;
    } //-- java.lang.String getDsTipoInscricaoFavorecido() 

    /**
     * Returns the value of field 'nmFavorecido'.
     * 
     * @return String
     * @return the value of field 'nmFavorecido'.
     */
    public java.lang.String getNmFavorecido()
    {
        return this._nmFavorecido;
    } //-- java.lang.String getNmFavorecido() 

    /**
     * Returns the value of field 'stFavorecido'.
     * 
     * @return String
     * @return the value of field 'stFavorecido'.
     */
    public java.lang.String getStFavorecido()
    {
        return this._stFavorecido;
    } //-- java.lang.String getStFavorecido() 

    /**
     * Method hasCdFavorecidoClientePagador
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFavorecidoClientePagador()
    {
        return this._has_cdFavorecidoClientePagador;
    } //-- boolean hasCdFavorecidoClientePagador() 

    /**
     * Method hasCdInscricaoFavorecidoCliente
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdInscricaoFavorecidoCliente()
    {
        return this._has_cdInscricaoFavorecidoCliente;
    } //-- boolean hasCdInscricaoFavorecidoCliente() 

    /**
     * Method hasCdTipoFavorecido
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoFavorecido()
    {
        return this._has_cdTipoFavorecido;
    } //-- boolean hasCdTipoFavorecido() 

    /**
     * Method hasCdTipoInscricaoFavorecido
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoInscricaoFavorecido()
    {
        return this._has_cdTipoInscricaoFavorecido;
    } //-- boolean hasCdTipoInscricaoFavorecido() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdFavorecidoClientePagador'.
     * 
     * @param cdFavorecidoClientePagador the value of field
     * 'cdFavorecidoClientePagador'.
     */
    public void setCdFavorecidoClientePagador(long cdFavorecidoClientePagador)
    {
        this._cdFavorecidoClientePagador = cdFavorecidoClientePagador;
        this._has_cdFavorecidoClientePagador = true;
    } //-- void setCdFavorecidoClientePagador(long) 

    /**
     * Sets the value of field 'cdInscricaoFavorecidoCliente'.
     * 
     * @param cdInscricaoFavorecidoCliente the value of field
     * 'cdInscricaoFavorecidoCliente'.
     */
    public void setCdInscricaoFavorecidoCliente(long cdInscricaoFavorecidoCliente)
    {
        this._cdInscricaoFavorecidoCliente = cdInscricaoFavorecidoCliente;
        this._has_cdInscricaoFavorecidoCliente = true;
    } //-- void setCdInscricaoFavorecidoCliente(long) 

    /**
     * Sets the value of field 'cdTipoFavorecido'.
     * 
     * @param cdTipoFavorecido the value of field 'cdTipoFavorecido'
     */
    public void setCdTipoFavorecido(int cdTipoFavorecido)
    {
        this._cdTipoFavorecido = cdTipoFavorecido;
        this._has_cdTipoFavorecido = true;
    } //-- void setCdTipoFavorecido(int) 

    /**
     * Sets the value of field 'cdTipoInscricaoFavorecido'.
     * 
     * @param cdTipoInscricaoFavorecido the value of field
     * 'cdTipoInscricaoFavorecido'.
     */
    public void setCdTipoInscricaoFavorecido(int cdTipoInscricaoFavorecido)
    {
        this._cdTipoInscricaoFavorecido = cdTipoInscricaoFavorecido;
        this._has_cdTipoInscricaoFavorecido = true;
    } //-- void setCdTipoInscricaoFavorecido(int) 

    /**
     * Sets the value of field 'dsTipoFavorecido'.
     * 
     * @param dsTipoFavorecido the value of field 'dsTipoFavorecido'
     */
    public void setDsTipoFavorecido(java.lang.String dsTipoFavorecido)
    {
        this._dsTipoFavorecido = dsTipoFavorecido;
    } //-- void setDsTipoFavorecido(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoInscricaoFavorecido'.
     * 
     * @param dsTipoInscricaoFavorecido the value of field
     * 'dsTipoInscricaoFavorecido'.
     */
    public void setDsTipoInscricaoFavorecido(java.lang.String dsTipoInscricaoFavorecido)
    {
        this._dsTipoInscricaoFavorecido = dsTipoInscricaoFavorecido;
    } //-- void setDsTipoInscricaoFavorecido(java.lang.String) 

    /**
     * Sets the value of field 'nmFavorecido'.
     * 
     * @param nmFavorecido the value of field 'nmFavorecido'.
     */
    public void setNmFavorecido(java.lang.String nmFavorecido)
    {
        this._nmFavorecido = nmFavorecido;
    } //-- void setNmFavorecido(java.lang.String) 

    /**
     * Sets the value of field 'stFavorecido'.
     * 
     * @param stFavorecido the value of field 'stFavorecido'.
     */
    public void setStFavorecido(java.lang.String stFavorecido)
    {
        this._stFavorecido = stFavorecido;
    } //-- void setStFavorecido(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listarfavorecidos.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listarfavorecidos.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listarfavorecidos.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarfavorecidos.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
