/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/implementation.ftl,v $
 * $Id: implementation.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: implementation.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.teste.impl;

import br.com.bradesco.web.pgit.service.business.teste.IDadosFinanceirosTesteService;
import br.com.bradesco.web.pgit.service.business.teste.bean.DadosFinanceirosTesteDTO;
import br.com.bradesco.web.pgit.service.data.pdc.FactoryAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultardadoscliente.request.ConsultarDadosClienteRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultardadoscliente.response.ConsultarDadosClienteResponse;

/**
 * Nome: DadosFinanceirosTesteServiceImpl
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class DadosFinanceirosTesteServiceImpl implements IDadosFinanceirosTesteService {

	/** Atributo factoryAdapter. */
	private FactoryAdapter factoryAdapter;

    /**
     * Get: factoryAdapter.
     *
     * @return factoryAdapter
     */
    public FactoryAdapter getFactoryAdapter() {
		return factoryAdapter;
	}

	/**
	 * Set: factoryAdapter.
	 *
	 * @param factoryAdapter the factory adapter
	 */
	public void setFactoryAdapter(FactoryAdapter factoryAdapter) {
		this.factoryAdapter = factoryAdapter;
	}

    /**
     * Dados financeiros teste service impl.
     */
    public DadosFinanceirosTesteServiceImpl() {

    }

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.teste.IDadosFinanceirosTesteService#consultar(java.lang.Long, java.lang.Integer, java.lang.Integer, java.lang.Long, java.lang.Integer, java.lang.Integer, java.lang.Integer)
	 */
	public DadosFinanceirosTesteDTO consultar(Long numBaseCnpj, Integer numFilialCnpj, Integer numDigitoCnpj, Long numBaseCpf, Integer numDigitoCpf, Integer dtSist, Integer tipoPesquisaSelecionada) {
		ConsultarDadosClienteRequest request = new ConsultarDadosClienteRequest();

		if(tipoPesquisaSelecionada == 1) {
			request.setCdCorpoCpfCnpj(numBaseCnpj == null ? 0 : numBaseCnpj);
			request.setCdFilialCpfCnpj(numFilialCnpj == null ? 0 : numFilialCnpj);
			request.setCdContratoCpfCnpj(numDigitoCnpj == null ? 0 : numDigitoCnpj);
			request.setDtSistema(dtSist == null ? 0 : dtSist);
		} else {
			request.setCdCorpoCpfCnpj(numBaseCpf == null ? 0 : numBaseCpf);
			request.setCdFilialCpfCnpj(0);
			request.setCdContratoCpfCnpj(numDigitoCpf == null ? 0 : numDigitoCpf);
			request.setDtSistema(dtSist == null ? 0 : dtSist);
		}

		ConsultarDadosClienteResponse response = getFactoryAdapter().getConsultarDadosClientePDCAdapter().invokeProcess(request);

		DadosFinanceirosTesteDTO saida = new DadosFinanceirosTesteDTO();

		saida.setQtdeDoc(response.getQtDoc());
		saida.setValorDoc(response.getVlTotalDoc());
		saida.setQtdeTed(response.getQtTEd());
		saida.setValorTed(response.getVlTotalTed());
		saida.setOrdemPgto(response.getQtOp());
		saida.setValorOrdemPgto(response.getVlTotalOp());
		saida.setQtdeCredConta(response.getQtCodConta());
		saida.setValorCredConta(response.getVlTotalCodConta());
		saida.setQtdeDebConta(response.getQtDebConta());
		saida.setValorDebConta(response.getVlTotalDebConta());
		saida.setQtdeTitBradesco(response.getQtTitBd());
		saida.setValorTitBradesco(response.getVlTotalTitBd());
		saida.setQtdeTitOutroBc(response.getQtTitOutros());
		saida.setValorTitOutroBc(response.getVlTotalTitOutros());
		saida.setQtdeDeposIdent(response.getQtDepIden());
		saida.setValorDeposIdent(response.getVlTotalDepIden());
		saida.setQtdeOrdemCred(response.getQtOrdCred());
		saida.setValorOrdemCred(response.getVlTotalOrdCred());
		saida.setQtdePgtoFornecedor(response.getQtParfor());
		saida.setValorPgtoFornecedor(response.getClTotalPagfor());
		saida.setQtdeDocFolhaPgto(response.getCdQuantidadeSal());
		saida.setValorDocFolhaPgto(response.getVlTotalSalDoc());
		saida.setQtdeTedFolhaPgto(response.getCdQuantidadeSalTed());
		saida.setValorTedFolhaPgto(response.getVlTotalSalTed());
		saida.setQtdeOrdemPgtoFolhaPgto(response.getQtSalOp());
		saida.setValorOrdemPgtoFolhaPgto(response.getVlTotalSalOp());
		saida.setQtdeCredContaFolhaPgto(response.getQtSalConta());
		saida.setValorCredContaFolhaPgto(response.getVlTotalSalConta());
		saida.setQtdeFolhaPgto(response.getQtPagSal());
		saida.setValorFolhaPgto(response.getVlTotalPagSal());
		saida.setQtdeDarf(response.getQtDarf());
		saida.setValorDarf(response.getVlTotalDarf());
		saida.setQtdeGare(response.getQtGare());
		saida.setValorGare(response.getVlTotalGare());
		saida.setQtdeGps(response.getQtGps());
		saida.setValorGps(response.getVlTotalGps());
		saida.setQtdeDebVeiculo(response.getQtDebVei());
		saida.setValorDebVeiculo(response.getVlTotalDebVei());
		saida.setQtdeCodBarra(response.getQtCodBarra());
		saida.setValorCodBarra(response.getVlTotalCodBarra());
		saida.setQtdePgtoEletronico(response.getQtPagBen());
		saida.setValorPgtoEletronico(response.getVlTotalPagBen());

		return saida;
	}
}