/*
 * Nome: br.com.bradesco.web.pgit.service.business.manterconultremessarecebida.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.manterconultremessarecebida.bean;

/**
 * Nome: DetalharUltimoNumeroRemessaSaidaDTO
 * <p>
 * Prop�sito:
 * </p>
 * .
 * 
 * @author : todo!
 * @version :
 */
public class DetalharUltimoNumeroRemessaSaidaDTO {

	private String dsNivelControle;

	private String dsTipoControle;

	private Integer cdTipoLayoutArquivo;

	private String dsTipoLayoutArquivo;

	private Integer cdTipoLoteLayout;

	private String dsTipoLoteLayout;

	private Long nrUltimoNumeroArquivoRemessa;

	private Long nrMaximoArquivoRemessa;

	private String cdCorpoCpfCnpj;

	public String getDsNivelControle() {
		return dsNivelControle;
	}

	public void setDsNivelControle(String dsNivelControle) {
		this.dsNivelControle = dsNivelControle;
	}

	public String getDsTipoControle() {
		return dsTipoControle;
	}

	public void setDsTipoControle(String dsTipoControle) {
		this.dsTipoControle = dsTipoControle;
	}

	public Integer getCdTipoLayoutArquivo() {
		return cdTipoLayoutArquivo;
	}

	public void setCdTipoLayoutArquivo(Integer cdTipoLayoutArquivo) {
		this.cdTipoLayoutArquivo = cdTipoLayoutArquivo;
	}

	public String getDsTipoLayoutArquivo() {
		return dsTipoLayoutArquivo;
	}

	public void setDsTipoLayoutArquivo(String dsTipoLayoutArquivo) {
		this.dsTipoLayoutArquivo = dsTipoLayoutArquivo;
	}

	public Integer getCdTipoLoteLayout() {
		return cdTipoLoteLayout;
	}

	public void setCdTipoLoteLayout(Integer cdTipoLoteLayout) {
		this.cdTipoLoteLayout = cdTipoLoteLayout;
	}

	public String getDsTipoLoteLayout() {
		return dsTipoLoteLayout;
	}

	public void setDsTipoLoteLayout(String dsTipoLoteLayout) {
		this.dsTipoLoteLayout = dsTipoLoteLayout;
	}

	public Long getNrUltimoNumeroArquivoRemessa() {
		return nrUltimoNumeroArquivoRemessa;
	}

	public void setNrUltimoNumeroArquivoRemessa(
			Long nrUltimoNumeroArquivoRemessa) {
		this.nrUltimoNumeroArquivoRemessa = nrUltimoNumeroArquivoRemessa;
	}

	public Long getNrMaximoArquivoRemessa() {
		return nrMaximoArquivoRemessa;
	}

	public void setNrMaximoArquivoRemessa(Long nrMaximoArquivoRemessa) {
		this.nrMaximoArquivoRemessa = nrMaximoArquivoRemessa;
	}

	public String getCdCorpoCpfCnpj() {
		return cdCorpoCpfCnpj;
	}

	public void setCdCorpoCpfCnpj(String cdCorpoCpfCnpj) {
		this.cdCorpoCpfCnpj = cdCorpoCpfCnpj;
	}

}
