/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listartipoacao.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdAcao
     */
    private int _cdAcao = 0;

    /**
     * keeps track of state for field: _cdAcao
     */
    private boolean _has_cdAcao;

    /**
     * Field _dsAcao
     */
    private java.lang.String _dsAcao;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listartipoacao.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdAcao
     * 
     */
    public void deleteCdAcao()
    {
        this._has_cdAcao= false;
    } //-- void deleteCdAcao() 

    /**
     * Returns the value of field 'cdAcao'.
     * 
     * @return int
     * @return the value of field 'cdAcao'.
     */
    public int getCdAcao()
    {
        return this._cdAcao;
    } //-- int getCdAcao() 

    /**
     * Returns the value of field 'dsAcao'.
     * 
     * @return String
     * @return the value of field 'dsAcao'.
     */
    public java.lang.String getDsAcao()
    {
        return this._dsAcao;
    } //-- java.lang.String getDsAcao() 

    /**
     * Method hasCdAcao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAcao()
    {
        return this._has_cdAcao;
    } //-- boolean hasCdAcao() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdAcao'.
     * 
     * @param cdAcao the value of field 'cdAcao'.
     */
    public void setCdAcao(int cdAcao)
    {
        this._cdAcao = cdAcao;
        this._has_cdAcao = true;
    } //-- void setCdAcao(int) 

    /**
     * Sets the value of field 'dsAcao'.
     * 
     * @param dsAcao the value of field 'dsAcao'.
     */
    public void setDsAcao(java.lang.String dsAcao)
    {
        this._dsAcao = dsAcao;
    } //-- void setDsAcao(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listartipoacao.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listartipoacao.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listartipoacao.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listartipoacao.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
