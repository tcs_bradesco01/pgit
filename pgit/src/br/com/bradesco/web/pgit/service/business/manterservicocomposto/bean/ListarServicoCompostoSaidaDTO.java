/*
 * Nome: br.com.bradesco.web.pgit.service.business.manterservicocomposto.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.manterservicocomposto.bean;

import br.com.bradesco.web.pgit.service.data.pdc.listarservicocomposto.response.Ocorrencias;

/**
 * Nome: ListarServicoCompostoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarServicoCompostoSaidaDTO {

    /** Atributo cdServicoCompostoPagamento. */
    private Long cdServicoCompostoPagamento;
    
    /** Atributo dsServicoCompostoPagamento. */
    private String dsServicoCompostoPagamento;
    
    /** Atributo cdTipoServicoCnab. */
    private Integer cdTipoServicoCnab;
    
    /** Atributo dsTipoServicoCnab. */
    private String dsTipoServicoCnab;
    
    /** Atributo cdFormaLancamentoCnab. */
    private Integer cdFormaLancamentoCnab;
    
    /** Atributo dsFormaLancamentoCnab. */
    private String dsFormaLancamentoCnab;
    
    /** Atributo cdFormaLiquidacao. */
    private Integer cdFormaLiquidacao;
    
    /** Atributo dsFormaLiquidacao. */
    private String dsFormaLiquidacao;
    
    

    /**
     * Listar servico composto saida dto.
     */
    public ListarServicoCompostoSaidaDTO() {
		super();
	}

	/**
	 * Listar servico composto saida dto.
	 *
	 * @param cdServicoCompostoPagamento the cd servico composto pagamento
	 * @param dsServicoCompostoPagamento the ds servico composto pagamento
	 * @param cdTipoServicoCnab the cd tipo servico cnab
	 * @param dsTipoServicoCnab the ds tipo servico cnab
	 * @param cdFormaLancamentoCnab the cd forma lancamento cnab
	 * @param dsFormaLancamentoCnab the ds forma lancamento cnab
	 * @param cdFormaLiquidacao the cd forma liquidacao
	 * @param dsFormaLiquidacao the ds forma liquidacao
	 */
	public ListarServicoCompostoSaidaDTO(Long cdServicoCompostoPagamento, String dsServicoCompostoPagamento, Integer cdTipoServicoCnab, String dsTipoServicoCnab, Integer cdFormaLancamentoCnab, String dsFormaLancamentoCnab, Integer cdFormaLiquidacao, String dsFormaLiquidacao) {
		super();
		this.cdServicoCompostoPagamento = cdServicoCompostoPagamento;
		this.dsServicoCompostoPagamento = dsServicoCompostoPagamento;
		this.cdTipoServicoCnab = cdTipoServicoCnab;
		this.dsTipoServicoCnab = dsTipoServicoCnab;
		this.cdFormaLancamentoCnab = cdFormaLancamentoCnab;
		this.dsFormaLancamentoCnab = dsFormaLancamentoCnab;
		this.cdFormaLiquidacao = cdFormaLiquidacao;
		this.dsFormaLiquidacao = dsFormaLiquidacao;
	}


    /**
     * Listar servico composto saida dto.
     *
     * @param saida the saida
     */
    public ListarServicoCompostoSaidaDTO(Ocorrencias saida) {
    	this.cdServicoCompostoPagamento = saida.getCdServicoCompostoPagamento();
        this.dsServicoCompostoPagamento = saida.getDsServicoCompostoPagamento();
        this.cdTipoServicoCnab = saida.getCdTipoServicoCnab();
        this.dsTipoServicoCnab = saida.getDsTipoServicoCnab();
        this.cdFormaLancamentoCnab = saida.getCdFormaLancamentoCnab();
        this.dsFormaLancamentoCnab = saida.getDsFormaLancamentoCnab();
        this.cdFormaLiquidacao = saida.getCdFormaLiquidacao();
        this.dsFormaLiquidacao = saida.getDsFormaLiquidacao();
	}

    
	/**
	 * Get: cdFormaLancamentoCnab.
	 *
	 * @return cdFormaLancamentoCnab
	 */
	public Integer getCdFormaLancamentoCnab() {
		return cdFormaLancamentoCnab;
	}
	
	/**
	 * Set: cdFormaLancamentoCnab.
	 *
	 * @param cdFormaLancamentoCnab the cd forma lancamento cnab
	 */
	public void setCdFormaLancamentoCnab(Integer cdFormaLancamentoCnab) {
		this.cdFormaLancamentoCnab = cdFormaLancamentoCnab;
	}
	
	/**
	 * Get: cdFormaLiquidacao.
	 *
	 * @return cdFormaLiquidacao
	 */
	public Integer getCdFormaLiquidacao() {
		return cdFormaLiquidacao;
	}
	
	/**
	 * Set: cdFormaLiquidacao.
	 *
	 * @param cdFormaLiquidacao the cd forma liquidacao
	 */
	public void setCdFormaLiquidacao(Integer cdFormaLiquidacao) {
		this.cdFormaLiquidacao = cdFormaLiquidacao;
	}
	
	/**
	 * Get: cdServicoCompostoPagamento.
	 *
	 * @return cdServicoCompostoPagamento
	 */
	public Long getCdServicoCompostoPagamento() {
		return cdServicoCompostoPagamento;
	}
	
	/**
	 * Set: cdServicoCompostoPagamento.
	 *
	 * @param cdServicoCompostoPagamento the cd servico composto pagamento
	 */
	public void setCdServicoCompostoPagamento(Long cdServicoCompostoPagamento) {
		this.cdServicoCompostoPagamento = cdServicoCompostoPagamento;
	}
	
	/**
	 * Get: cdTipoServicoCnab.
	 *
	 * @return cdTipoServicoCnab
	 */
	public Integer getCdTipoServicoCnab() {
		return cdTipoServicoCnab;
	}
	
	/**
	 * Set: cdTipoServicoCnab.
	 *
	 * @param cdTipoServicoCnab the cd tipo servico cnab
	 */
	public void setCdTipoServicoCnab(Integer cdTipoServicoCnab) {
		this.cdTipoServicoCnab = cdTipoServicoCnab;
	}
	
	/**
	 * Get: dsFormaLancamentoCnab.
	 *
	 * @return dsFormaLancamentoCnab
	 */
	public String getDsFormaLancamentoCnab() {
		return dsFormaLancamentoCnab;
	}
	
	/**
	 * Set: dsFormaLancamentoCnab.
	 *
	 * @param dsFormaLancamentoCnab the ds forma lancamento cnab
	 */
	public void setDsFormaLancamentoCnab(String dsFormaLancamentoCnab) {
		this.dsFormaLancamentoCnab = dsFormaLancamentoCnab;
	}
	
	/**
	 * Get: dsFormaLiquidacao.
	 *
	 * @return dsFormaLiquidacao
	 */
	public String getDsFormaLiquidacao() {
		return dsFormaLiquidacao;
	}
	
	/**
	 * Set: dsFormaLiquidacao.
	 *
	 * @param dsFormaLiquidacao the ds forma liquidacao
	 */
	public void setDsFormaLiquidacao(String dsFormaLiquidacao) {
		this.dsFormaLiquidacao = dsFormaLiquidacao;
	}
	
	/**
	 * Get: dsServicoCompostoPagamento.
	 *
	 * @return dsServicoCompostoPagamento
	 */
	public String getDsServicoCompostoPagamento() {
		return dsServicoCompostoPagamento;
	}
	
	/**
	 * Set: dsServicoCompostoPagamento.
	 *
	 * @param dsServicoCompostoPagamento the ds servico composto pagamento
	 */
	public void setDsServicoCompostoPagamento(String dsServicoCompostoPagamento) {
		this.dsServicoCompostoPagamento = dsServicoCompostoPagamento;
	}
	
	/**
	 * Get: dsTipoServicoCnab.
	 *
	 * @return dsTipoServicoCnab
	 */
	public String getDsTipoServicoCnab() {
		return dsTipoServicoCnab;
	}
	
	/**
	 * Set: dsTipoServicoCnab.
	 *
	 * @param dsTipoServicoCnab the ds tipo servico cnab
	 */
	public void setDsTipoServicoCnab(String dsTipoServicoCnab) {
		this.dsTipoServicoCnab = dsTipoServicoCnab;
	}
	
	/**
	 * (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof ListarServicoCompostoSaidaDTO)) {
			return false;
		}

		ListarServicoCompostoSaidaDTO other = (ListarServicoCompostoSaidaDTO) obj;
		return this.getCdFormaLancamentoCnab().equals(other.getCdFormaLancamentoCnab())
			&& this.getCdFormaLiquidacao().equals(other.getCdFormaLiquidacao())
			&& this.getCdServicoCompostoPagamento().equals(other.getCdServicoCompostoPagamento())
			&& this.getCdTipoServicoCnab().equals(other.getCdTipoServicoCnab());
	}

	/**
	 * (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		int hashCode = 7;
		hashCode = hashCode + getCdFormaLancamentoCnab().hashCode();
		hashCode = hashCode + getCdFormaLiquidacao().hashCode();
		hashCode = hashCode + getCdServicoCompostoPagamento().hashCode();
		hashCode = hashCode + getCdTipoServicoCnab().hashCode();
		return hashCode;
	}

}
