/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listarenderecoemail.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdCpfCnpjRecebedor
     */
    private long _cdCpfCnpjRecebedor = 0;

    /**
     * keeps track of state for field: _cdCpfCnpjRecebedor
     */
    private boolean _has_cdCpfCnpjRecebedor;

    /**
     * Field _cdFilialCnpjRecebedor
     */
    private int _cdFilialCnpjRecebedor = 0;

    /**
     * keeps track of state for field: _cdFilialCnpjRecebedor
     */
    private boolean _has_cdFilialCnpjRecebedor;

    /**
     * Field _cdControleCpfRecebedor
     */
    private int _cdControleCpfRecebedor = 0;

    /**
     * keeps track of state for field: _cdControleCpfRecebedor
     */
    private boolean _has_cdControleCpfRecebedor;

    /**
     * Field _cdNomeRazao
     */
    private java.lang.String _cdNomeRazao;

    /**
     * Field _dsLogradouroPagador
     */
    private java.lang.String _dsLogradouroPagador;

    /**
     * Field _dsNumeroLogradouroPagador
     */
    private java.lang.String _dsNumeroLogradouroPagador;

    /**
     * Field _dsComplementoLogradouroPagador
     */
    private java.lang.String _dsComplementoLogradouroPagador;

    /**
     * Field _dsBairroClientePagador
     */
    private java.lang.String _dsBairroClientePagador;

    /**
     * Field _dsMunicipioClientePagador
     */
    private java.lang.String _dsMunicipioClientePagador;

    /**
     * Field _cdSiglaUfPagador
     */
    private java.lang.String _cdSiglaUfPagador;

    /**
     * Field _dsPais
     */
    private java.lang.String _dsPais;

    /**
     * Field _cdCepPagador
     */
    private int _cdCepPagador = 0;

    /**
     * keeps track of state for field: _cdCepPagador
     */
    private boolean _has_cdCepPagador;

    /**
     * Field _cdCepComplementoPagador
     */
    private int _cdCepComplementoPagador = 0;

    /**
     * keeps track of state for field: _cdCepComplementoPagador
     */
    private boolean _has_cdCepComplementoPagador;

    /**
     * Field _dsCpfCnpjRecebedor
     */
    private long _dsCpfCnpjRecebedor = 0;

    /**
     * keeps track of state for field: _dsCpfCnpjRecebedor
     */
    private boolean _has_dsCpfCnpjRecebedor;

    /**
     * Field _dsFilialCnpjRecebedor
     */
    private int _dsFilialCnpjRecebedor = 0;

    /**
     * keeps track of state for field: _dsFilialCnpjRecebedor
     */
    private boolean _has_dsFilialCnpjRecebedor;

    /**
     * Field _dsControleCpfRecebedor
     */
    private int _dsControleCpfRecebedor = 0;

    /**
     * keeps track of state for field: _dsControleCpfRecebedor
     */
    private boolean _has_dsControleCpfRecebedor;

    /**
     * Field _dsNomeRazao
     */
    private java.lang.String _dsNomeRazao;

    /**
     * Field _cdEnderecoEletronico
     */
    private java.lang.String _cdEnderecoEletronico;

    /**
     * Field _nrSequenciaEndereco
     */
    private int _nrSequenciaEndereco = 0;

    /**
     * keeps track of state for field: _nrSequenciaEndereco
     */
    private boolean _has_nrSequenciaEndereco;

    /**
     * Field _cdClub
     */
    private long _cdClub = 0;

    /**
     * keeps track of state for field: _cdClub
     */
    private boolean _has_cdClub;

    /**
     * Field _cdTipo
     */
    private java.lang.String _cdTipo;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarenderecoemail.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdCepComplementoPagador
     * 
     */
    public void deleteCdCepComplementoPagador()
    {
        this._has_cdCepComplementoPagador= false;
    } //-- void deleteCdCepComplementoPagador() 

    /**
     * Method deleteCdCepPagador
     * 
     */
    public void deleteCdCepPagador()
    {
        this._has_cdCepPagador= false;
    } //-- void deleteCdCepPagador() 

    /**
     * Method deleteCdClub
     * 
     */
    public void deleteCdClub()
    {
        this._has_cdClub= false;
    } //-- void deleteCdClub() 

    /**
     * Method deleteCdControleCpfRecebedor
     * 
     */
    public void deleteCdControleCpfRecebedor()
    {
        this._has_cdControleCpfRecebedor= false;
    } //-- void deleteCdControleCpfRecebedor() 

    /**
     * Method deleteCdCpfCnpjRecebedor
     * 
     */
    public void deleteCdCpfCnpjRecebedor()
    {
        this._has_cdCpfCnpjRecebedor= false;
    } //-- void deleteCdCpfCnpjRecebedor() 

    /**
     * Method deleteCdFilialCnpjRecebedor
     * 
     */
    public void deleteCdFilialCnpjRecebedor()
    {
        this._has_cdFilialCnpjRecebedor= false;
    } //-- void deleteCdFilialCnpjRecebedor() 

    /**
     * Method deleteDsControleCpfRecebedor
     * 
     */
    public void deleteDsControleCpfRecebedor()
    {
        this._has_dsControleCpfRecebedor= false;
    } //-- void deleteDsControleCpfRecebedor() 

    /**
     * Method deleteDsCpfCnpjRecebedor
     * 
     */
    public void deleteDsCpfCnpjRecebedor()
    {
        this._has_dsCpfCnpjRecebedor= false;
    } //-- void deleteDsCpfCnpjRecebedor() 

    /**
     * Method deleteDsFilialCnpjRecebedor
     * 
     */
    public void deleteDsFilialCnpjRecebedor()
    {
        this._has_dsFilialCnpjRecebedor= false;
    } //-- void deleteDsFilialCnpjRecebedor() 

    /**
     * Method deleteNrSequenciaEndereco
     * 
     */
    public void deleteNrSequenciaEndereco()
    {
        this._has_nrSequenciaEndereco= false;
    } //-- void deleteNrSequenciaEndereco() 

    /**
     * Returns the value of field 'cdCepComplementoPagador'.
     * 
     * @return int
     * @return the value of field 'cdCepComplementoPagador'.
     */
    public int getCdCepComplementoPagador()
    {
        return this._cdCepComplementoPagador;
    } //-- int getCdCepComplementoPagador() 

    /**
     * Returns the value of field 'cdCepPagador'.
     * 
     * @return int
     * @return the value of field 'cdCepPagador'.
     */
    public int getCdCepPagador()
    {
        return this._cdCepPagador;
    } //-- int getCdCepPagador() 

    /**
     * Returns the value of field 'cdClub'.
     * 
     * @return long
     * @return the value of field 'cdClub'.
     */
    public long getCdClub()
    {
        return this._cdClub;
    } //-- long getCdClub() 

    /**
     * Returns the value of field 'cdControleCpfRecebedor'.
     * 
     * @return int
     * @return the value of field 'cdControleCpfRecebedor'.
     */
    public int getCdControleCpfRecebedor()
    {
        return this._cdControleCpfRecebedor;
    } //-- int getCdControleCpfRecebedor() 

    /**
     * Returns the value of field 'cdCpfCnpjRecebedor'.
     * 
     * @return long
     * @return the value of field 'cdCpfCnpjRecebedor'.
     */
    public long getCdCpfCnpjRecebedor()
    {
        return this._cdCpfCnpjRecebedor;
    } //-- long getCdCpfCnpjRecebedor() 

    /**
     * Returns the value of field 'cdEnderecoEletronico'.
     * 
     * @return String
     * @return the value of field 'cdEnderecoEletronico'.
     */
    public java.lang.String getCdEnderecoEletronico()
    {
        return this._cdEnderecoEletronico;
    } //-- java.lang.String getCdEnderecoEletronico() 

    /**
     * Returns the value of field 'cdFilialCnpjRecebedor'.
     * 
     * @return int
     * @return the value of field 'cdFilialCnpjRecebedor'.
     */
    public int getCdFilialCnpjRecebedor()
    {
        return this._cdFilialCnpjRecebedor;
    } //-- int getCdFilialCnpjRecebedor() 

    /**
     * Returns the value of field 'cdNomeRazao'.
     * 
     * @return String
     * @return the value of field 'cdNomeRazao'.
     */
    public java.lang.String getCdNomeRazao()
    {
        return this._cdNomeRazao;
    } //-- java.lang.String getCdNomeRazao() 

    /**
     * Returns the value of field 'cdSiglaUfPagador'.
     * 
     * @return String
     * @return the value of field 'cdSiglaUfPagador'.
     */
    public java.lang.String getCdSiglaUfPagador()
    {
        return this._cdSiglaUfPagador;
    } //-- java.lang.String getCdSiglaUfPagador() 

    /**
     * Returns the value of field 'cdTipo'.
     * 
     * @return String
     * @return the value of field 'cdTipo'.
     */
    public java.lang.String getCdTipo()
    {
        return this._cdTipo;
    } //-- java.lang.String getCdTipo() 

    /**
     * Returns the value of field 'dsBairroClientePagador'.
     * 
     * @return String
     * @return the value of field 'dsBairroClientePagador'.
     */
    public java.lang.String getDsBairroClientePagador()
    {
        return this._dsBairroClientePagador;
    } //-- java.lang.String getDsBairroClientePagador() 

    /**
     * Returns the value of field 'dsComplementoLogradouroPagador'.
     * 
     * @return String
     * @return the value of field 'dsComplementoLogradouroPagador'.
     */
    public java.lang.String getDsComplementoLogradouroPagador()
    {
        return this._dsComplementoLogradouroPagador;
    } //-- java.lang.String getDsComplementoLogradouroPagador() 

    /**
     * Returns the value of field 'dsControleCpfRecebedor'.
     * 
     * @return int
     * @return the value of field 'dsControleCpfRecebedor'.
     */
    public int getDsControleCpfRecebedor()
    {
        return this._dsControleCpfRecebedor;
    } //-- int getDsControleCpfRecebedor() 

    /**
     * Returns the value of field 'dsCpfCnpjRecebedor'.
     * 
     * @return long
     * @return the value of field 'dsCpfCnpjRecebedor'.
     */
    public long getDsCpfCnpjRecebedor()
    {
        return this._dsCpfCnpjRecebedor;
    } //-- long getDsCpfCnpjRecebedor() 

    /**
     * Returns the value of field 'dsFilialCnpjRecebedor'.
     * 
     * @return int
     * @return the value of field 'dsFilialCnpjRecebedor'.
     */
    public int getDsFilialCnpjRecebedor()
    {
        return this._dsFilialCnpjRecebedor;
    } //-- int getDsFilialCnpjRecebedor() 

    /**
     * Returns the value of field 'dsLogradouroPagador'.
     * 
     * @return String
     * @return the value of field 'dsLogradouroPagador'.
     */
    public java.lang.String getDsLogradouroPagador()
    {
        return this._dsLogradouroPagador;
    } //-- java.lang.String getDsLogradouroPagador() 

    /**
     * Returns the value of field 'dsMunicipioClientePagador'.
     * 
     * @return String
     * @return the value of field 'dsMunicipioClientePagador'.
     */
    public java.lang.String getDsMunicipioClientePagador()
    {
        return this._dsMunicipioClientePagador;
    } //-- java.lang.String getDsMunicipioClientePagador() 

    /**
     * Returns the value of field 'dsNomeRazao'.
     * 
     * @return String
     * @return the value of field 'dsNomeRazao'.
     */
    public java.lang.String getDsNomeRazao()
    {
        return this._dsNomeRazao;
    } //-- java.lang.String getDsNomeRazao() 

    /**
     * Returns the value of field 'dsNumeroLogradouroPagador'.
     * 
     * @return String
     * @return the value of field 'dsNumeroLogradouroPagador'.
     */
    public java.lang.String getDsNumeroLogradouroPagador()
    {
        return this._dsNumeroLogradouroPagador;
    } //-- java.lang.String getDsNumeroLogradouroPagador() 

    /**
     * Returns the value of field 'dsPais'.
     * 
     * @return String
     * @return the value of field 'dsPais'.
     */
    public java.lang.String getDsPais()
    {
        return this._dsPais;
    } //-- java.lang.String getDsPais() 

    /**
     * Returns the value of field 'nrSequenciaEndereco'.
     * 
     * @return int
     * @return the value of field 'nrSequenciaEndereco'.
     */
    public int getNrSequenciaEndereco()
    {
        return this._nrSequenciaEndereco;
    } //-- int getNrSequenciaEndereco() 

    /**
     * Method hasCdCepComplementoPagador
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCepComplementoPagador()
    {
        return this._has_cdCepComplementoPagador;
    } //-- boolean hasCdCepComplementoPagador() 

    /**
     * Method hasCdCepPagador
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCepPagador()
    {
        return this._has_cdCepPagador;
    } //-- boolean hasCdCepPagador() 

    /**
     * Method hasCdClub
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdClub()
    {
        return this._has_cdClub;
    } //-- boolean hasCdClub() 

    /**
     * Method hasCdControleCpfRecebedor
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdControleCpfRecebedor()
    {
        return this._has_cdControleCpfRecebedor;
    } //-- boolean hasCdControleCpfRecebedor() 

    /**
     * Method hasCdCpfCnpjRecebedor
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCpfCnpjRecebedor()
    {
        return this._has_cdCpfCnpjRecebedor;
    } //-- boolean hasCdCpfCnpjRecebedor() 

    /**
     * Method hasCdFilialCnpjRecebedor
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFilialCnpjRecebedor()
    {
        return this._has_cdFilialCnpjRecebedor;
    } //-- boolean hasCdFilialCnpjRecebedor() 

    /**
     * Method hasDsControleCpfRecebedor
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasDsControleCpfRecebedor()
    {
        return this._has_dsControleCpfRecebedor;
    } //-- boolean hasDsControleCpfRecebedor() 

    /**
     * Method hasDsCpfCnpjRecebedor
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasDsCpfCnpjRecebedor()
    {
        return this._has_dsCpfCnpjRecebedor;
    } //-- boolean hasDsCpfCnpjRecebedor() 

    /**
     * Method hasDsFilialCnpjRecebedor
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasDsFilialCnpjRecebedor()
    {
        return this._has_dsFilialCnpjRecebedor;
    } //-- boolean hasDsFilialCnpjRecebedor() 

    /**
     * Method hasNrSequenciaEndereco
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaEndereco()
    {
        return this._has_nrSequenciaEndereco;
    } //-- boolean hasNrSequenciaEndereco() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdCepComplementoPagador'.
     * 
     * @param cdCepComplementoPagador the value of field
     * 'cdCepComplementoPagador'.
     */
    public void setCdCepComplementoPagador(int cdCepComplementoPagador)
    {
        this._cdCepComplementoPagador = cdCepComplementoPagador;
        this._has_cdCepComplementoPagador = true;
    } //-- void setCdCepComplementoPagador(int) 

    /**
     * Sets the value of field 'cdCepPagador'.
     * 
     * @param cdCepPagador the value of field 'cdCepPagador'.
     */
    public void setCdCepPagador(int cdCepPagador)
    {
        this._cdCepPagador = cdCepPagador;
        this._has_cdCepPagador = true;
    } //-- void setCdCepPagador(int) 

    /**
     * Sets the value of field 'cdClub'.
     * 
     * @param cdClub the value of field 'cdClub'.
     */
    public void setCdClub(long cdClub)
    {
        this._cdClub = cdClub;
        this._has_cdClub = true;
    } //-- void setCdClub(long) 

    /**
     * Sets the value of field 'cdControleCpfRecebedor'.
     * 
     * @param cdControleCpfRecebedor the value of field
     * 'cdControleCpfRecebedor'.
     */
    public void setCdControleCpfRecebedor(int cdControleCpfRecebedor)
    {
        this._cdControleCpfRecebedor = cdControleCpfRecebedor;
        this._has_cdControleCpfRecebedor = true;
    } //-- void setCdControleCpfRecebedor(int) 

    /**
     * Sets the value of field 'cdCpfCnpjRecebedor'.
     * 
     * @param cdCpfCnpjRecebedor the value of field
     * 'cdCpfCnpjRecebedor'.
     */
    public void setCdCpfCnpjRecebedor(long cdCpfCnpjRecebedor)
    {
        this._cdCpfCnpjRecebedor = cdCpfCnpjRecebedor;
        this._has_cdCpfCnpjRecebedor = true;
    } //-- void setCdCpfCnpjRecebedor(long) 

    /**
     * Sets the value of field 'cdEnderecoEletronico'.
     * 
     * @param cdEnderecoEletronico the value of field
     * 'cdEnderecoEletronico'.
     */
    public void setCdEnderecoEletronico(java.lang.String cdEnderecoEletronico)
    {
        this._cdEnderecoEletronico = cdEnderecoEletronico;
    } //-- void setCdEnderecoEletronico(java.lang.String) 

    /**
     * Sets the value of field 'cdFilialCnpjRecebedor'.
     * 
     * @param cdFilialCnpjRecebedor the value of field
     * 'cdFilialCnpjRecebedor'.
     */
    public void setCdFilialCnpjRecebedor(int cdFilialCnpjRecebedor)
    {
        this._cdFilialCnpjRecebedor = cdFilialCnpjRecebedor;
        this._has_cdFilialCnpjRecebedor = true;
    } //-- void setCdFilialCnpjRecebedor(int) 

    /**
     * Sets the value of field 'cdNomeRazao'.
     * 
     * @param cdNomeRazao the value of field 'cdNomeRazao'.
     */
    public void setCdNomeRazao(java.lang.String cdNomeRazao)
    {
        this._cdNomeRazao = cdNomeRazao;
    } //-- void setCdNomeRazao(java.lang.String) 

    /**
     * Sets the value of field 'cdSiglaUfPagador'.
     * 
     * @param cdSiglaUfPagador the value of field 'cdSiglaUfPagador'
     */
    public void setCdSiglaUfPagador(java.lang.String cdSiglaUfPagador)
    {
        this._cdSiglaUfPagador = cdSiglaUfPagador;
    } //-- void setCdSiglaUfPagador(java.lang.String) 

    /**
     * Sets the value of field 'cdTipo'.
     * 
     * @param cdTipo the value of field 'cdTipo'.
     */
    public void setCdTipo(java.lang.String cdTipo)
    {
        this._cdTipo = cdTipo;
    } //-- void setCdTipo(java.lang.String) 

    /**
     * Sets the value of field 'dsBairroClientePagador'.
     * 
     * @param dsBairroClientePagador the value of field
     * 'dsBairroClientePagador'.
     */
    public void setDsBairroClientePagador(java.lang.String dsBairroClientePagador)
    {
        this._dsBairroClientePagador = dsBairroClientePagador;
    } //-- void setDsBairroClientePagador(java.lang.String) 

    /**
     * Sets the value of field 'dsComplementoLogradouroPagador'.
     * 
     * @param dsComplementoLogradouroPagador the value of field
     * 'dsComplementoLogradouroPagador'.
     */
    public void setDsComplementoLogradouroPagador(java.lang.String dsComplementoLogradouroPagador)
    {
        this._dsComplementoLogradouroPagador = dsComplementoLogradouroPagador;
    } //-- void setDsComplementoLogradouroPagador(java.lang.String) 

    /**
     * Sets the value of field 'dsControleCpfRecebedor'.
     * 
     * @param dsControleCpfRecebedor the value of field
     * 'dsControleCpfRecebedor'.
     */
    public void setDsControleCpfRecebedor(int dsControleCpfRecebedor)
    {
        this._dsControleCpfRecebedor = dsControleCpfRecebedor;
        this._has_dsControleCpfRecebedor = true;
    } //-- void setDsControleCpfRecebedor(int) 

    /**
     * Sets the value of field 'dsCpfCnpjRecebedor'.
     * 
     * @param dsCpfCnpjRecebedor the value of field
     * 'dsCpfCnpjRecebedor'.
     */
    public void setDsCpfCnpjRecebedor(long dsCpfCnpjRecebedor)
    {
        this._dsCpfCnpjRecebedor = dsCpfCnpjRecebedor;
        this._has_dsCpfCnpjRecebedor = true;
    } //-- void setDsCpfCnpjRecebedor(long) 

    /**
     * Sets the value of field 'dsFilialCnpjRecebedor'.
     * 
     * @param dsFilialCnpjRecebedor the value of field
     * 'dsFilialCnpjRecebedor'.
     */
    public void setDsFilialCnpjRecebedor(int dsFilialCnpjRecebedor)
    {
        this._dsFilialCnpjRecebedor = dsFilialCnpjRecebedor;
        this._has_dsFilialCnpjRecebedor = true;
    } //-- void setDsFilialCnpjRecebedor(int) 

    /**
     * Sets the value of field 'dsLogradouroPagador'.
     * 
     * @param dsLogradouroPagador the value of field
     * 'dsLogradouroPagador'.
     */
    public void setDsLogradouroPagador(java.lang.String dsLogradouroPagador)
    {
        this._dsLogradouroPagador = dsLogradouroPagador;
    } //-- void setDsLogradouroPagador(java.lang.String) 

    /**
     * Sets the value of field 'dsMunicipioClientePagador'.
     * 
     * @param dsMunicipioClientePagador the value of field
     * 'dsMunicipioClientePagador'.
     */
    public void setDsMunicipioClientePagador(java.lang.String dsMunicipioClientePagador)
    {
        this._dsMunicipioClientePagador = dsMunicipioClientePagador;
    } //-- void setDsMunicipioClientePagador(java.lang.String) 

    /**
     * Sets the value of field 'dsNomeRazao'.
     * 
     * @param dsNomeRazao the value of field 'dsNomeRazao'.
     */
    public void setDsNomeRazao(java.lang.String dsNomeRazao)
    {
        this._dsNomeRazao = dsNomeRazao;
    } //-- void setDsNomeRazao(java.lang.String) 

    /**
     * Sets the value of field 'dsNumeroLogradouroPagador'.
     * 
     * @param dsNumeroLogradouroPagador the value of field
     * 'dsNumeroLogradouroPagador'.
     */
    public void setDsNumeroLogradouroPagador(java.lang.String dsNumeroLogradouroPagador)
    {
        this._dsNumeroLogradouroPagador = dsNumeroLogradouroPagador;
    } //-- void setDsNumeroLogradouroPagador(java.lang.String) 

    /**
     * Sets the value of field 'dsPais'.
     * 
     * @param dsPais the value of field 'dsPais'.
     */
    public void setDsPais(java.lang.String dsPais)
    {
        this._dsPais = dsPais;
    } //-- void setDsPais(java.lang.String) 

    /**
     * Sets the value of field 'nrSequenciaEndereco'.
     * 
     * @param nrSequenciaEndereco the value of field
     * 'nrSequenciaEndereco'.
     */
    public void setNrSequenciaEndereco(int nrSequenciaEndereco)
    {
        this._nrSequenciaEndereco = nrSequenciaEndereco;
        this._has_nrSequenciaEndereco = true;
    } //-- void setNrSequenciaEndereco(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listarenderecoemail.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listarenderecoemail.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listarenderecoemail.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarenderecoemail.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
