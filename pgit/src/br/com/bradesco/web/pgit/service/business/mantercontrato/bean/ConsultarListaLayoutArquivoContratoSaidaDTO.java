/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantercontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantercontrato.bean;

/**
 * Nome: ConsultarListaLayoutArquivoContratoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarListaLayoutArquivoContratoSaidaDTO {
	
	/** Atributo codMensagem. */
	private String codMensagem;
	
	/** Atributo mensagem. */
	private String mensagem;
	
	/** Atributo numeroLinhas. */
	private int numeroLinhas;
	
	/** Atributo cdTipoLayout. */
	private Integer cdTipoLayout;
	
	/** Atributo dsTipoLayout. */
	private String dsTipoLayout;
	
	/** Atributo cdSituacaoLayoutContrato. */
	private Integer cdSituacaoLayoutContrato;
	
	/** Atributo cdProduto. */
	private Integer cdProduto;
	
	/** Atributo dsProduto. */
	private String dsProduto;
	
	/** Atributo dsSituacaoLayoutContrato. */
	private String dsSituacaoLayoutContrato;
	
	/**
	 * Get: cdProduto.
	 *
	 * @return cdProduto
	 */
	public Integer getCdProduto() {
		return cdProduto;
	}
	
	/**
	 * Set: cdProduto.
	 *
	 * @param cdProduto the cd produto
	 */
	public void setCdProduto(Integer cdProduto) {
		this.cdProduto = cdProduto;
	}
	
	/**
	 * Get: cdTipoLayout.
	 *
	 * @return cdTipoLayout
	 */
	public Integer getCdTipoLayout() {
		return cdTipoLayout;
	}
	
	/**
	 * Set: cdTipoLayout.
	 *
	 * @param cdTipoLayout the cd tipo layout
	 */
	public void setCdTipoLayout(Integer cdTipoLayout) {
		this.cdTipoLayout = cdTipoLayout;
	}
	
	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}
	
	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}
	
	/**
	 * Get: dsProduto.
	 *
	 * @return dsProduto
	 */
	public String getDsProduto() {
		return dsProduto;
	}
	
	/**
	 * Set: dsProduto.
	 *
	 * @param dsProduto the ds produto
	 */
	public void setDsProduto(String dsProduto) {
		this.dsProduto = dsProduto;
	}
	
	/**
	 * Get: dsTipoLayout.
	 *
	 * @return dsTipoLayout
	 */
	public String getDsTipoLayout() {
		return dsTipoLayout;
	}
	
	/**
	 * Set: dsTipoLayout.
	 *
	 * @param dsTipoLayout the ds tipo layout
	 */
	public void setDsTipoLayout(String dsTipoLayout) {
		this.dsTipoLayout = dsTipoLayout;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	/**
	 * Get: numeroLinhas.
	 *
	 * @return numeroLinhas
	 */
	public int getNumeroLinhas() {
		return numeroLinhas;
	}
	
	/**
	 * Set: numeroLinhas.
	 *
	 * @param numeroLinhas the numero linhas
	 */
	public void setNumeroLinhas(int numeroLinhas) {
		this.numeroLinhas = numeroLinhas;
	}
	
	/**
	 * Get: cdSituacaoLayoutContrato.
	 *
	 * @return cdSituacaoLayoutContrato
	 */
	public Integer getCdSituacaoLayoutContrato() {
		return cdSituacaoLayoutContrato;
	}
	
	/**
	 * Set: cdSituacaoLayoutContrato.
	 *
	 * @param cdSituacaoLayoutContrato the cd situacao layout contrato
	 */
	public void setCdSituacaoLayoutContrato(Integer cdSituacaoLayoutContrato) {
		this.cdSituacaoLayoutContrato = cdSituacaoLayoutContrato;
	}
	
	/**
	 * Get: dsSituacaoLayoutContrato.
	 *
	 * @return dsSituacaoLayoutContrato
	 */
	public String getDsSituacaoLayoutContrato() {
		return dsSituacaoLayoutContrato;
	}
	
	/**
	 * Set: dsSituacaoLayoutContrato.
	 *
	 * @param dsSituacaoLayoutContrato the ds situacao layout contrato
	 */
	public void setDsSituacaoLayoutContrato(String dsSituacaoLayoutContrato) {
		this.dsSituacaoLayoutContrato = dsSituacaoLayoutContrato;
	}
	
	
	
}
