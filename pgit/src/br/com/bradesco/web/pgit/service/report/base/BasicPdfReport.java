/*
 * Nome: br.com.bradesco.web.pgit.service.report.base
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.report.base;

import java.io.IOException;
import java.io.OutputStream;
import java.net.URL;

import br.com.bradesco.web.aq.application.util.faces.BradescoFacesUtils;
import br.com.bradesco.web.pgit.service.report.base.exception.ReportException;

import com.lowagie.text.BadElementException;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.pdf.PdfWriter;

/**
 * Nome: BasicPdfReport
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public abstract class BasicPdfReport {

	/** Atributo FONT_NORMAL. */
	protected static final Font FONT_NORMAL = new Font(Font.HELVETICA, 8);

	/** Atributo FONT_BOLD. */
	protected static final Font FONT_BOLD = new Font(Font.HELVETICA, 8, Font.BOLD);

	/**
	 * Popular pdf.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	protected abstract void popularPdf(Document document) throws DocumentException;
	
	/**
	 * Gerar pdf.
	 *
	 * @param outputStream the output stream
	 * @throws DocumentException the document exception
	 */
	public void gerarPdf(OutputStream outputStream) throws DocumentException {
		Document document = new Document(PageSize.A4);

		PdfWriter writer = null;
		try {
			writer = PdfWriter.getInstance(document, outputStream);

			document.open();

			popularPdf(document);

			writer.flush();
		} finally {
			if (document != null && document.isOpen()) {
				document.close();
			}
			if (writer != null) {
				writer.close();
			}
		}
	}

	/**
	 * Get: imagemRelatorio.
	 *
	 * @return imagemRelatorio
	 */
	protected Image getImagemRelatorio() {
		try {
			String caminho = "/images/logorelatorio.gif";
			URL caminhoImagem = BradescoFacesUtils.getServletContext().getResource(caminho);

			if (caminhoImagem == null) {
				throw new ReportException("N�o encontrou a imagem no caminho: " + caminho);
			}

			Image img = Image.getInstance(caminhoImagem);
			img.setAlignment(Element.ALIGN_LEFT);
			img.setWidthPercentage(40);
			return img;
		} catch(IOException e) {
			throw new ReportException(e);
		} catch (BadElementException e) {
			throw new ReportException(e);
		}
	}
}
