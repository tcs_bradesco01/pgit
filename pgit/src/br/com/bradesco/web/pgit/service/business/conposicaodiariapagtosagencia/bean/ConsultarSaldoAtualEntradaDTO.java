/*
 * Nome: br.com.bradesco.web.pgit.service.business.conposicaodiariapagtosagencia.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.conposicaodiariapagtosagencia.bean;

/**
 * Nome: ConsultarSaldoAtualEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarSaldoAtualEntradaDTO{    
    
    /** Atributo cdPessoaJuridicaContrato. */
    private Long cdPessoaJuridicaContrato;
    
    /** Atributo cdTipoContratoNegocio. */
    private Integer cdTipoContratoNegocio;
    
    /** Atributo nrSequenciaContratoNegocio. */
    private Long nrSequenciaContratoNegocio;
    
    /** Atributo cdProdutoServicoOperacao. */
    private Integer cdProdutoServicoOperacao;
    
    /** Atributo cdProdutoServicoRelacionado. */
    private Integer cdProdutoServicoRelacionado;
    
    /** Atributo nrCnpjCpf. */
    private Long nrCnpjCpf;
    
    /** Atributo cdCnpjCpfFilial. */
    private Integer cdCnpjCpfFilial;
    
    /** Atributo cdCnpjCpfDigito. */
    private Integer cdCnpjCpfDigito;
    
    /** Atributo dsComplemento. */
    private String dsComplemento;
    
    /** Atributo cdPessoaContratoDebito. */
    private Long cdPessoaContratoDebito;
    
    /** Atributo cdTipoContratoDebito. */
    private Integer cdTipoContratoDebito;
    
    /** Atributo nrSequenciaContratoDebito. */
    private Long nrSequenciaContratoDebito;
    
    /**
     * Get: cdCnpjCpfDigito.
     *
     * @return cdCnpjCpfDigito
     */
    public Integer getCdCnpjCpfDigito() {
        return cdCnpjCpfDigito;
    }
    
    /**
     * Set: cdCnpjCpfDigito.
     *
     * @param cdCnpjCpfDigito the cd cnpj cpf digito
     */
    public void setCdCnpjCpfDigito(Integer cdCnpjCpfDigito) {
        this.cdCnpjCpfDigito = cdCnpjCpfDigito;
    }
    
    /**
     * Get: cdCnpjCpfFilial.
     *
     * @return cdCnpjCpfFilial
     */
    public Integer getCdCnpjCpfFilial() {
        return cdCnpjCpfFilial;
    }
    
    /**
     * Set: cdCnpjCpfFilial.
     *
     * @param cdCnpjCpfFilial the cd cnpj cpf filial
     */
    public void setCdCnpjCpfFilial(Integer cdCnpjCpfFilial) {
        this.cdCnpjCpfFilial = cdCnpjCpfFilial;
    }
    
    /**
     * Get: cdPessoaContratoDebito.
     *
     * @return cdPessoaContratoDebito
     */
    public Long getCdPessoaContratoDebito() {
        return cdPessoaContratoDebito;
    }
    
    /**
     * Set: cdPessoaContratoDebito.
     *
     * @param cdPessoaContratoDebito the cd pessoa contrato debito
     */
    public void setCdPessoaContratoDebito(Long cdPessoaContratoDebito) {
        this.cdPessoaContratoDebito = cdPessoaContratoDebito;
    }
    
    /**
     * Get: cdPessoaJuridicaContrato.
     *
     * @return cdPessoaJuridicaContrato
     */
    public Long getCdPessoaJuridicaContrato() {
        return cdPessoaJuridicaContrato;
    }
    
    /**
     * Set: cdPessoaJuridicaContrato.
     *
     * @param cdPessoaJuridicaContrato the cd pessoa juridica contrato
     */
    public void setCdPessoaJuridicaContrato(Long cdPessoaJuridicaContrato) {
        this.cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
    }
    
    /**
     * Get: cdProdutoServicoOperacao.
     *
     * @return cdProdutoServicoOperacao
     */
    public Integer getCdProdutoServicoOperacao() {
        return cdProdutoServicoOperacao;
    }
    
    /**
     * Set: cdProdutoServicoOperacao.
     *
     * @param cdProdutoServicoOperacao the cd produto servico operacao
     */
    public void setCdProdutoServicoOperacao(Integer cdProdutoServicoOperacao) {
        this.cdProdutoServicoOperacao = cdProdutoServicoOperacao;
    }
    
    /**
     * Get: cdProdutoServicoRelacionado.
     *
     * @return cdProdutoServicoRelacionado
     */
    public Integer getCdProdutoServicoRelacionado() {
        return cdProdutoServicoRelacionado;
    }
    
    /**
     * Set: cdProdutoServicoRelacionado.
     *
     * @param cdProdutoServicoRelacionado the cd produto servico relacionado
     */
    public void setCdProdutoServicoRelacionado(Integer cdProdutoServicoRelacionado) {
        this.cdProdutoServicoRelacionado = cdProdutoServicoRelacionado;
    }
    
    /**
     * Get: cdTipoContratoDebito.
     *
     * @return cdTipoContratoDebito
     */
    public Integer getCdTipoContratoDebito() {
        return cdTipoContratoDebito;
    }
    
    /**
     * Set: cdTipoContratoDebito.
     *
     * @param cdTipoContratoDebito the cd tipo contrato debito
     */
    public void setCdTipoContratoDebito(Integer cdTipoContratoDebito) {
        this.cdTipoContratoDebito = cdTipoContratoDebito;
    }
    
    /**
     * Get: cdTipoContratoNegocio.
     *
     * @return cdTipoContratoNegocio
     */
    public Integer getCdTipoContratoNegocio() {
        return cdTipoContratoNegocio;
    }
    
    /**
     * Set: cdTipoContratoNegocio.
     *
     * @param cdTipoContratoNegocio the cd tipo contrato negocio
     */
    public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
        this.cdTipoContratoNegocio = cdTipoContratoNegocio;
    }
    
    /**
     * Get: dsComplemento.
     *
     * @return dsComplemento
     */
    public String getDsComplemento() {
        return dsComplemento;
    }
    
    /**
     * Set: dsComplemento.
     *
     * @param dsComplemento the ds complemento
     */
    public void setDsComplemento(String dsComplemento) {
        this.dsComplemento = dsComplemento;
    }
    
    /**
     * Get: nrCnpjCpf.
     *
     * @return nrCnpjCpf
     */
    public Long getNrCnpjCpf() {
        return nrCnpjCpf;
    }
    
    /**
     * Set: nrCnpjCpf.
     *
     * @param nrCnpjCpf the nr cnpj cpf
     */
    public void setNrCnpjCpf(Long nrCnpjCpf) {
        this.nrCnpjCpf = nrCnpjCpf;
    }
    
    /**
     * Get: nrSequenciaContratoDebito.
     *
     * @return nrSequenciaContratoDebito
     */
    public Long getNrSequenciaContratoDebito() {
        return nrSequenciaContratoDebito;
    }
    
    /**
     * Set: nrSequenciaContratoDebito.
     *
     * @param nrSequenciaContratoDebito the nr sequencia contrato debito
     */
    public void setNrSequenciaContratoDebito(Long nrSequenciaContratoDebito) {
        this.nrSequenciaContratoDebito = nrSequenciaContratoDebito;
    }
    
    /**
     * Get: nrSequenciaContratoNegocio.
     *
     * @return nrSequenciaContratoNegocio
     */
    public Long getNrSequenciaContratoNegocio() {
        return nrSequenciaContratoNegocio;
    }
    
    /**
     * Set: nrSequenciaContratoNegocio.
     *
     * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
     */
    public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
        this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
    }    
}

