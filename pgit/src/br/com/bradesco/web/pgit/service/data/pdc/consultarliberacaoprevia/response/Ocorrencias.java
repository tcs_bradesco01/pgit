/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultarliberacaoprevia.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.math.BigDecimal;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _nrCpfCnpj
     */
    private java.lang.String _nrCpfCnpj;

    /**
     * Field _dsRazaoSocial
     */
    private java.lang.String _dsRazaoSocial;

    /**
     * Field _cdPessoaJuridicaContrato
     */
    private long _cdPessoaJuridicaContrato = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridicaContrato
     */
    private boolean _has_cdPessoaJuridicaContrato;

    /**
     * Field _cdTipoPessoaJuridica
     */
    private int _cdTipoPessoaJuridica = 0;

    /**
     * keeps track of state for field: _cdTipoPessoaJuridica
     */
    private boolean _has_cdTipoPessoaJuridica;

    /**
     * Field _nrSequencialContratoNegocio
     */
    private long _nrSequencialContratoNegocio = 0;

    /**
     * keeps track of state for field: _nrSequencialContratoNegocio
     */
    private boolean _has_nrSequencialContratoNegocio;

    /**
     * Field _dsCodigoPessoaJuridica
     */
    private java.lang.String _dsCodigoPessoaJuridica;

    /**
     * Field _cdTipoSolicitacao
     */
    private int _cdTipoSolicitacao = 0;

    /**
     * keeps track of state for field: _cdTipoSolicitacao
     */
    private boolean _has_cdTipoSolicitacao;

    /**
     * Field _nrSolicitacao
     */
    private int _nrSolicitacao = 0;

    /**
     * keeps track of state for field: _nrSolicitacao
     */
    private boolean _has_nrSolicitacao;

    /**
     * Field _dtLiberacao
     */
    private java.lang.String _dtLiberacao;

    /**
     * Field _cdBancoDebito
     */
    private int _cdBancoDebito = 0;

    /**
     * keeps track of state for field: _cdBancoDebito
     */
    private boolean _has_cdBancoDebito;

    /**
     * Field _dsBancoDebito
     */
    private java.lang.String _dsBancoDebito;

    /**
     * Field _cdAgenciaDebito
     */
    private int _cdAgenciaDebito = 0;

    /**
     * keeps track of state for field: _cdAgenciaDebito
     */
    private boolean _has_cdAgenciaDebito;

    /**
     * Field _cdDigitoAgenciaDebito
     */
    private int _cdDigitoAgenciaDebito = 0;

    /**
     * keeps track of state for field: _cdDigitoAgenciaDebito
     */
    private boolean _has_cdDigitoAgenciaDebito;

    /**
     * Field _dsAgenciaDebito
     */
    private java.lang.String _dsAgenciaDebito;

    /**
     * Field _cdContaDebito
     */
    private long _cdContaDebito = 0;

    /**
     * keeps track of state for field: _cdContaDebito
     */
    private boolean _has_cdContaDebito;

    /**
     * Field _cdDigitoContaDebito
     */
    private java.lang.String _cdDigitoContaDebito;

    /**
     * Field _cdTipoContaDebito
     */
    private java.lang.String _cdTipoContaDebito;

    /**
     * Field _situacaoSolicitacao
     */
    private int _situacaoSolicitacao = 0;

    /**
     * keeps track of state for field: _situacaoSolicitacao
     */
    private boolean _has_situacaoSolicitacao;

    /**
     * Field _dsSituacaoSolicitacao
     */
    private java.lang.String _dsSituacaoSolicitacao;

    /**
     * Field _vlAutorizado
     */
    private java.math.BigDecimal _vlAutorizado = new java.math.BigDecimal("0");

    /**
     * Field _vlUtilizado
     */
    private java.math.BigDecimal _vlUtilizado = new java.math.BigDecimal("0");

    /**
     * Field _qtPagamento
     */
    private long _qtPagamento = 0;

    /**
     * keeps track of state for field: _qtPagamento
     */
    private boolean _has_qtPagamento;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
        setVlAutorizado(new java.math.BigDecimal("0"));
        setVlUtilizado(new java.math.BigDecimal("0"));
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarliberacaoprevia.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdAgenciaDebito
     * 
     */
    public void deleteCdAgenciaDebito()
    {
        this._has_cdAgenciaDebito= false;
    } //-- void deleteCdAgenciaDebito() 

    /**
     * Method deleteCdBancoDebito
     * 
     */
    public void deleteCdBancoDebito()
    {
        this._has_cdBancoDebito= false;
    } //-- void deleteCdBancoDebito() 

    /**
     * Method deleteCdContaDebito
     * 
     */
    public void deleteCdContaDebito()
    {
        this._has_cdContaDebito= false;
    } //-- void deleteCdContaDebito() 

    /**
     * Method deleteCdDigitoAgenciaDebito
     * 
     */
    public void deleteCdDigitoAgenciaDebito()
    {
        this._has_cdDigitoAgenciaDebito= false;
    } //-- void deleteCdDigitoAgenciaDebito() 

    /**
     * Method deleteCdPessoaJuridicaContrato
     * 
     */
    public void deleteCdPessoaJuridicaContrato()
    {
        this._has_cdPessoaJuridicaContrato= false;
    } //-- void deleteCdPessoaJuridicaContrato() 

    /**
     * Method deleteCdTipoPessoaJuridica
     * 
     */
    public void deleteCdTipoPessoaJuridica()
    {
        this._has_cdTipoPessoaJuridica= false;
    } //-- void deleteCdTipoPessoaJuridica() 

    /**
     * Method deleteCdTipoSolicitacao
     * 
     */
    public void deleteCdTipoSolicitacao()
    {
        this._has_cdTipoSolicitacao= false;
    } //-- void deleteCdTipoSolicitacao() 

    /**
     * Method deleteNrSequencialContratoNegocio
     * 
     */
    public void deleteNrSequencialContratoNegocio()
    {
        this._has_nrSequencialContratoNegocio= false;
    } //-- void deleteNrSequencialContratoNegocio() 

    /**
     * Method deleteNrSolicitacao
     * 
     */
    public void deleteNrSolicitacao()
    {
        this._has_nrSolicitacao= false;
    } //-- void deleteNrSolicitacao() 

    /**
     * Method deleteQtPagamento
     * 
     */
    public void deleteQtPagamento()
    {
        this._has_qtPagamento= false;
    } //-- void deleteQtPagamento() 

    /**
     * Method deleteSituacaoSolicitacao
     * 
     */
    public void deleteSituacaoSolicitacao()
    {
        this._has_situacaoSolicitacao= false;
    } //-- void deleteSituacaoSolicitacao() 

    /**
     * Returns the value of field 'cdAgenciaDebito'.
     * 
     * @return int
     * @return the value of field 'cdAgenciaDebito'.
     */
    public int getCdAgenciaDebito()
    {
        return this._cdAgenciaDebito;
    } //-- int getCdAgenciaDebito() 

    /**
     * Returns the value of field 'cdBancoDebito'.
     * 
     * @return int
     * @return the value of field 'cdBancoDebito'.
     */
    public int getCdBancoDebito()
    {
        return this._cdBancoDebito;
    } //-- int getCdBancoDebito() 

    /**
     * Returns the value of field 'cdContaDebito'.
     * 
     * @return long
     * @return the value of field 'cdContaDebito'.
     */
    public long getCdContaDebito()
    {
        return this._cdContaDebito;
    } //-- long getCdContaDebito() 

    /**
     * Returns the value of field 'cdDigitoAgenciaDebito'.
     * 
     * @return int
     * @return the value of field 'cdDigitoAgenciaDebito'.
     */
    public int getCdDigitoAgenciaDebito()
    {
        return this._cdDigitoAgenciaDebito;
    } //-- int getCdDigitoAgenciaDebito() 

    /**
     * Returns the value of field 'cdDigitoContaDebito'.
     * 
     * @return String
     * @return the value of field 'cdDigitoContaDebito'.
     */
    public java.lang.String getCdDigitoContaDebito()
    {
        return this._cdDigitoContaDebito;
    } //-- java.lang.String getCdDigitoContaDebito() 

    /**
     * Returns the value of field 'cdPessoaJuridicaContrato'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridicaContrato'.
     */
    public long getCdPessoaJuridicaContrato()
    {
        return this._cdPessoaJuridicaContrato;
    } //-- long getCdPessoaJuridicaContrato() 

    /**
     * Returns the value of field 'cdTipoContaDebito'.
     * 
     * @return String
     * @return the value of field 'cdTipoContaDebito'.
     */
    public java.lang.String getCdTipoContaDebito()
    {
        return this._cdTipoContaDebito;
    } //-- java.lang.String getCdTipoContaDebito() 

    /**
     * Returns the value of field 'cdTipoPessoaJuridica'.
     * 
     * @return int
     * @return the value of field 'cdTipoPessoaJuridica'.
     */
    public int getCdTipoPessoaJuridica()
    {
        return this._cdTipoPessoaJuridica;
    } //-- int getCdTipoPessoaJuridica() 

    /**
     * Returns the value of field 'cdTipoSolicitacao'.
     * 
     * @return int
     * @return the value of field 'cdTipoSolicitacao'.
     */
    public int getCdTipoSolicitacao()
    {
        return this._cdTipoSolicitacao;
    } //-- int getCdTipoSolicitacao() 

    /**
     * Returns the value of field 'dsAgenciaDebito'.
     * 
     * @return String
     * @return the value of field 'dsAgenciaDebito'.
     */
    public java.lang.String getDsAgenciaDebito()
    {
        return this._dsAgenciaDebito;
    } //-- java.lang.String getDsAgenciaDebito() 

    /**
     * Returns the value of field 'dsBancoDebito'.
     * 
     * @return String
     * @return the value of field 'dsBancoDebito'.
     */
    public java.lang.String getDsBancoDebito()
    {
        return this._dsBancoDebito;
    } //-- java.lang.String getDsBancoDebito() 

    /**
     * Returns the value of field 'dsCodigoPessoaJuridica'.
     * 
     * @return String
     * @return the value of field 'dsCodigoPessoaJuridica'.
     */
    public java.lang.String getDsCodigoPessoaJuridica()
    {
        return this._dsCodigoPessoaJuridica;
    } //-- java.lang.String getDsCodigoPessoaJuridica() 

    /**
     * Returns the value of field 'dsRazaoSocial'.
     * 
     * @return String
     * @return the value of field 'dsRazaoSocial'.
     */
    public java.lang.String getDsRazaoSocial()
    {
        return this._dsRazaoSocial;
    } //-- java.lang.String getDsRazaoSocial() 

    /**
     * Returns the value of field 'dsSituacaoSolicitacao'.
     * 
     * @return String
     * @return the value of field 'dsSituacaoSolicitacao'.
     */
    public java.lang.String getDsSituacaoSolicitacao()
    {
        return this._dsSituacaoSolicitacao;
    } //-- java.lang.String getDsSituacaoSolicitacao() 

    /**
     * Returns the value of field 'dtLiberacao'.
     * 
     * @return String
     * @return the value of field 'dtLiberacao'.
     */
    public java.lang.String getDtLiberacao()
    {
        return this._dtLiberacao;
    } //-- java.lang.String getDtLiberacao() 

    /**
     * Returns the value of field 'nrCpfCnpj'.
     * 
     * @return String
     * @return the value of field 'nrCpfCnpj'.
     */
    public java.lang.String getNrCpfCnpj()
    {
        return this._nrCpfCnpj;
    } //-- java.lang.String getNrCpfCnpj() 

    /**
     * Returns the value of field 'nrSequencialContratoNegocio'.
     * 
     * @return long
     * @return the value of field 'nrSequencialContratoNegocio'.
     */
    public long getNrSequencialContratoNegocio()
    {
        return this._nrSequencialContratoNegocio;
    } //-- long getNrSequencialContratoNegocio() 

    /**
     * Returns the value of field 'nrSolicitacao'.
     * 
     * @return int
     * @return the value of field 'nrSolicitacao'.
     */
    public int getNrSolicitacao()
    {
        return this._nrSolicitacao;
    } //-- int getNrSolicitacao() 

    /**
     * Returns the value of field 'qtPagamento'.
     * 
     * @return long
     * @return the value of field 'qtPagamento'.
     */
    public long getQtPagamento()
    {
        return this._qtPagamento;
    } //-- long getQtPagamento() 

    /**
     * Returns the value of field 'situacaoSolicitacao'.
     * 
     * @return int
     * @return the value of field 'situacaoSolicitacao'.
     */
    public int getSituacaoSolicitacao()
    {
        return this._situacaoSolicitacao;
    } //-- int getSituacaoSolicitacao() 

    /**
     * Returns the value of field 'vlAutorizado'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlAutorizado'.
     */
    public java.math.BigDecimal getVlAutorizado()
    {
        return this._vlAutorizado;
    } //-- java.math.BigDecimal getVlAutorizado() 

    /**
     * Returns the value of field 'vlUtilizado'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlUtilizado'.
     */
    public java.math.BigDecimal getVlUtilizado()
    {
        return this._vlUtilizado;
    } //-- java.math.BigDecimal getVlUtilizado() 

    /**
     * Method hasCdAgenciaDebito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgenciaDebito()
    {
        return this._has_cdAgenciaDebito;
    } //-- boolean hasCdAgenciaDebito() 

    /**
     * Method hasCdBancoDebito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdBancoDebito()
    {
        return this._has_cdBancoDebito;
    } //-- boolean hasCdBancoDebito() 

    /**
     * Method hasCdContaDebito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdContaDebito()
    {
        return this._has_cdContaDebito;
    } //-- boolean hasCdContaDebito() 

    /**
     * Method hasCdDigitoAgenciaDebito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdDigitoAgenciaDebito()
    {
        return this._has_cdDigitoAgenciaDebito;
    } //-- boolean hasCdDigitoAgenciaDebito() 

    /**
     * Method hasCdPessoaJuridicaContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridicaContrato()
    {
        return this._has_cdPessoaJuridicaContrato;
    } //-- boolean hasCdPessoaJuridicaContrato() 

    /**
     * Method hasCdTipoPessoaJuridica
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoPessoaJuridica()
    {
        return this._has_cdTipoPessoaJuridica;
    } //-- boolean hasCdTipoPessoaJuridica() 

    /**
     * Method hasCdTipoSolicitacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoSolicitacao()
    {
        return this._has_cdTipoSolicitacao;
    } //-- boolean hasCdTipoSolicitacao() 

    /**
     * Method hasNrSequencialContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequencialContratoNegocio()
    {
        return this._has_nrSequencialContratoNegocio;
    } //-- boolean hasNrSequencialContratoNegocio() 

    /**
     * Method hasNrSolicitacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSolicitacao()
    {
        return this._has_nrSolicitacao;
    } //-- boolean hasNrSolicitacao() 

    /**
     * Method hasQtPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtPagamento()
    {
        return this._has_qtPagamento;
    } //-- boolean hasQtPagamento() 

    /**
     * Method hasSituacaoSolicitacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasSituacaoSolicitacao()
    {
        return this._has_situacaoSolicitacao;
    } //-- boolean hasSituacaoSolicitacao() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdAgenciaDebito'.
     * 
     * @param cdAgenciaDebito the value of field 'cdAgenciaDebito'.
     */
    public void setCdAgenciaDebito(int cdAgenciaDebito)
    {
        this._cdAgenciaDebito = cdAgenciaDebito;
        this._has_cdAgenciaDebito = true;
    } //-- void setCdAgenciaDebito(int) 

    /**
     * Sets the value of field 'cdBancoDebito'.
     * 
     * @param cdBancoDebito the value of field 'cdBancoDebito'.
     */
    public void setCdBancoDebito(int cdBancoDebito)
    {
        this._cdBancoDebito = cdBancoDebito;
        this._has_cdBancoDebito = true;
    } //-- void setCdBancoDebito(int) 

    /**
     * Sets the value of field 'cdContaDebito'.
     * 
     * @param cdContaDebito the value of field 'cdContaDebito'.
     */
    public void setCdContaDebito(long cdContaDebito)
    {
        this._cdContaDebito = cdContaDebito;
        this._has_cdContaDebito = true;
    } //-- void setCdContaDebito(long) 

    /**
     * Sets the value of field 'cdDigitoAgenciaDebito'.
     * 
     * @param cdDigitoAgenciaDebito the value of field
     * 'cdDigitoAgenciaDebito'.
     */
    public void setCdDigitoAgenciaDebito(int cdDigitoAgenciaDebito)
    {
        this._cdDigitoAgenciaDebito = cdDigitoAgenciaDebito;
        this._has_cdDigitoAgenciaDebito = true;
    } //-- void setCdDigitoAgenciaDebito(int) 

    /**
     * Sets the value of field 'cdDigitoContaDebito'.
     * 
     * @param cdDigitoContaDebito the value of field
     * 'cdDigitoContaDebito'.
     */
    public void setCdDigitoContaDebito(java.lang.String cdDigitoContaDebito)
    {
        this._cdDigitoContaDebito = cdDigitoContaDebito;
    } //-- void setCdDigitoContaDebito(java.lang.String) 

    /**
     * Sets the value of field 'cdPessoaJuridicaContrato'.
     * 
     * @param cdPessoaJuridicaContrato the value of field
     * 'cdPessoaJuridicaContrato'.
     */
    public void setCdPessoaJuridicaContrato(long cdPessoaJuridicaContrato)
    {
        this._cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
        this._has_cdPessoaJuridicaContrato = true;
    } //-- void setCdPessoaJuridicaContrato(long) 

    /**
     * Sets the value of field 'cdTipoContaDebito'.
     * 
     * @param cdTipoContaDebito the value of field
     * 'cdTipoContaDebito'.
     */
    public void setCdTipoContaDebito(java.lang.String cdTipoContaDebito)
    {
        this._cdTipoContaDebito = cdTipoContaDebito;
    } //-- void setCdTipoContaDebito(java.lang.String) 

    /**
     * Sets the value of field 'cdTipoPessoaJuridica'.
     * 
     * @param cdTipoPessoaJuridica the value of field
     * 'cdTipoPessoaJuridica'.
     */
    public void setCdTipoPessoaJuridica(int cdTipoPessoaJuridica)
    {
        this._cdTipoPessoaJuridica = cdTipoPessoaJuridica;
        this._has_cdTipoPessoaJuridica = true;
    } //-- void setCdTipoPessoaJuridica(int) 

    /**
     * Sets the value of field 'cdTipoSolicitacao'.
     * 
     * @param cdTipoSolicitacao the value of field
     * 'cdTipoSolicitacao'.
     */
    public void setCdTipoSolicitacao(int cdTipoSolicitacao)
    {
        this._cdTipoSolicitacao = cdTipoSolicitacao;
        this._has_cdTipoSolicitacao = true;
    } //-- void setCdTipoSolicitacao(int) 

    /**
     * Sets the value of field 'dsAgenciaDebito'.
     * 
     * @param dsAgenciaDebito the value of field 'dsAgenciaDebito'.
     */
    public void setDsAgenciaDebito(java.lang.String dsAgenciaDebito)
    {
        this._dsAgenciaDebito = dsAgenciaDebito;
    } //-- void setDsAgenciaDebito(java.lang.String) 

    /**
     * Sets the value of field 'dsBancoDebito'.
     * 
     * @param dsBancoDebito the value of field 'dsBancoDebito'.
     */
    public void setDsBancoDebito(java.lang.String dsBancoDebito)
    {
        this._dsBancoDebito = dsBancoDebito;
    } //-- void setDsBancoDebito(java.lang.String) 

    /**
     * Sets the value of field 'dsCodigoPessoaJuridica'.
     * 
     * @param dsCodigoPessoaJuridica the value of field
     * 'dsCodigoPessoaJuridica'.
     */
    public void setDsCodigoPessoaJuridica(java.lang.String dsCodigoPessoaJuridica)
    {
        this._dsCodigoPessoaJuridica = dsCodigoPessoaJuridica;
    } //-- void setDsCodigoPessoaJuridica(java.lang.String) 

    /**
     * Sets the value of field 'dsRazaoSocial'.
     * 
     * @param dsRazaoSocial the value of field 'dsRazaoSocial'.
     */
    public void setDsRazaoSocial(java.lang.String dsRazaoSocial)
    {
        this._dsRazaoSocial = dsRazaoSocial;
    } //-- void setDsRazaoSocial(java.lang.String) 

    /**
     * Sets the value of field 'dsSituacaoSolicitacao'.
     * 
     * @param dsSituacaoSolicitacao the value of field
     * 'dsSituacaoSolicitacao'.
     */
    public void setDsSituacaoSolicitacao(java.lang.String dsSituacaoSolicitacao)
    {
        this._dsSituacaoSolicitacao = dsSituacaoSolicitacao;
    } //-- void setDsSituacaoSolicitacao(java.lang.String) 

    /**
     * Sets the value of field 'dtLiberacao'.
     * 
     * @param dtLiberacao the value of field 'dtLiberacao'.
     */
    public void setDtLiberacao(java.lang.String dtLiberacao)
    {
        this._dtLiberacao = dtLiberacao;
    } //-- void setDtLiberacao(java.lang.String) 

    /**
     * Sets the value of field 'nrCpfCnpj'.
     * 
     * @param nrCpfCnpj the value of field 'nrCpfCnpj'.
     */
    public void setNrCpfCnpj(java.lang.String nrCpfCnpj)
    {
        this._nrCpfCnpj = nrCpfCnpj;
    } //-- void setNrCpfCnpj(java.lang.String) 

    /**
     * Sets the value of field 'nrSequencialContratoNegocio'.
     * 
     * @param nrSequencialContratoNegocio the value of field
     * 'nrSequencialContratoNegocio'.
     */
    public void setNrSequencialContratoNegocio(long nrSequencialContratoNegocio)
    {
        this._nrSequencialContratoNegocio = nrSequencialContratoNegocio;
        this._has_nrSequencialContratoNegocio = true;
    } //-- void setNrSequencialContratoNegocio(long) 

    /**
     * Sets the value of field 'nrSolicitacao'.
     * 
     * @param nrSolicitacao the value of field 'nrSolicitacao'.
     */
    public void setNrSolicitacao(int nrSolicitacao)
    {
        this._nrSolicitacao = nrSolicitacao;
        this._has_nrSolicitacao = true;
    } //-- void setNrSolicitacao(int) 

    /**
     * Sets the value of field 'qtPagamento'.
     * 
     * @param qtPagamento the value of field 'qtPagamento'.
     */
    public void setQtPagamento(long qtPagamento)
    {
        this._qtPagamento = qtPagamento;
        this._has_qtPagamento = true;
    } //-- void setQtPagamento(long) 

    /**
     * Sets the value of field 'situacaoSolicitacao'.
     * 
     * @param situacaoSolicitacao the value of field
     * 'situacaoSolicitacao'.
     */
    public void setSituacaoSolicitacao(int situacaoSolicitacao)
    {
        this._situacaoSolicitacao = situacaoSolicitacao;
        this._has_situacaoSolicitacao = true;
    } //-- void setSituacaoSolicitacao(int) 

    /**
     * Sets the value of field 'vlAutorizado'.
     * 
     * @param vlAutorizado the value of field 'vlAutorizado'.
     */
    public void setVlAutorizado(java.math.BigDecimal vlAutorizado)
    {
        this._vlAutorizado = vlAutorizado;
    } //-- void setVlAutorizado(java.math.BigDecimal) 

    /**
     * Sets the value of field 'vlUtilizado'.
     * 
     * @param vlUtilizado the value of field 'vlUtilizado'.
     */
    public void setVlUtilizado(java.math.BigDecimal vlUtilizado)
    {
        this._vlUtilizado = vlUtilizado;
    } //-- void setVlUtilizado(java.math.BigDecimal) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultarliberacaoprevia.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultarliberacaoprevia.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultarliberacaoprevia.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarliberacaoprevia.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
