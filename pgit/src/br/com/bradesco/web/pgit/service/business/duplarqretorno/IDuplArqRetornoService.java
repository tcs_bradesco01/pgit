/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/interfaz.ftl,v $
 * $Id: interfaz.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: interfaz.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.duplarqretorno;

import java.util.List;

import br.com.bradesco.web.pgit.service.business.duplarqretorno.bean.AlterarDuplicacaoArqRetornoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.duplarqretorno.bean.AlterarDuplicacaoArqRetornoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.duplarqretorno.bean.DetalharDuplicacaoArqRetornoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.duplarqretorno.bean.DetalharDuplicacaoArqRetornoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.duplarqretorno.bean.DuplicacaoArquivoRetornoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.duplarqretorno.bean.DuplicacaoArquivoRetornoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.duplarqretorno.bean.ExcluirDuplicacaoArqRetornoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.duplarqretorno.bean.ExcluirDuplicacaoArqRetornoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.duplarqretorno.bean.IncluirDuplicacaoArqRetornoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.duplarqretorno.bean.IncluirDuplicacaoArqRetornoSaidaDTO;

/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Interface do adaptador: DuplicacaoArquivoRetorno
 * </p>
 * 
 * @comment CODIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public interface IDuplArqRetornoService {

    /**
     * M�todo de exemplo.
     */
    void sampleDuplicacaoArquivoRetorno();
    
    /**
     * Listar duplicacao arq retorno.
     *
     * @param duplicacaoArquivoRetornoEntradaDTO the duplicacao arquivo retorno entrada dto
     * @return the list< duplicacao arquivo retorno saida dt o>
     */
    List<DuplicacaoArquivoRetornoSaidaDTO> listarDuplicacaoArqRetorno(DuplicacaoArquivoRetornoEntradaDTO duplicacaoArquivoRetornoEntradaDTO);
    
    /**
     * Detalhar duplicacao arq retorno.
     *
     * @param detalharDuplicacaoArqRetornoEntradaDTO the detalhar duplicacao arq retorno entrada dto
     * @return the detalhar duplicacao arq retorno saida dto
     */
    DetalharDuplicacaoArqRetornoSaidaDTO detalharDuplicacaoArqRetorno(DetalharDuplicacaoArqRetornoEntradaDTO detalharDuplicacaoArqRetornoEntradaDTO);
    
    /**
     * Excluir duplicacao arq retorno.
     *
     * @param excluirDuplicacaoArqRetornoEntradaDTO the excluir duplicacao arq retorno entrada dto
     * @return the excluir duplicacao arq retorno saida dto
     */
    ExcluirDuplicacaoArqRetornoSaidaDTO excluirDuplicacaoArqRetorno(ExcluirDuplicacaoArqRetornoEntradaDTO excluirDuplicacaoArqRetornoEntradaDTO);
    
    /**
     * Alterar duplicacao arq retorno.
     *
     * @param alterarDuplicacaoArqRetornoEntradaDTO the alterar duplicacao arq retorno entrada dto
     * @return the alterar duplicacao arq retorno saida dto
     */
    AlterarDuplicacaoArqRetornoSaidaDTO alterarDuplicacaoArqRetorno(AlterarDuplicacaoArqRetornoEntradaDTO alterarDuplicacaoArqRetornoEntradaDTO);
    
    /**
     * Incluir duplicacao arq retorno.
     *
     * @param incluirDuplicacaoArqRetornoEntradaDTO the incluir duplicacao arq retorno entrada dto
     * @return the incluir duplicacao arq retorno saida dto
     */
    IncluirDuplicacaoArqRetornoSaidaDTO incluirDuplicacaoArqRetorno(IncluirDuplicacaoArqRetornoEntradaDTO incluirDuplicacaoArqRetornoEntradaDTO);

}

