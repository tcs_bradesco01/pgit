/*
 * Nome: br.com.bradesco.web.pgit.service.business.conposicaodiariapagtosagencia.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.conposicaodiariapagtosagencia.bean;

/**
 * Nome: DetalharPagamentosEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class DetalharPagamentosEntradaDTO{
    
    /** Atributo nrOcorrencias. */
    private Integer nrOcorrencias;
    
    /** Atributo cdCpfCnpj. */
    private Long cdCpfCnpj;
    
    /** Atributo cdFilialCpfCnpj. */
    private Integer cdFilialCpfCnpj;
    
    /** Atributo cdDigitoCpfCnpj. */
    private Integer cdDigitoCpfCnpj;
    
    /** Atributo cdPessoaJuridicaContrato. */
    private Long cdPessoaJuridicaContrato;
    
    /** Atributo cdTipoContratoNegocio. */
    private Integer cdTipoContratoNegocio;
    
    /** Atributo nrSequenciaContratoNegocio. */
    private Long nrSequenciaContratoNegocio;
    
    /** Atributo cdUnidadeOrganizacional. */
    private Integer cdUnidadeOrganizacional;
    
    /** Atributo cdServico. */
    private Integer cdServico;
    
    /** Atributo cdModalidade. */
    private Integer cdModalidade;
    
    /** Atributo cdBancoDebito. */
    private Integer cdBancoDebito;
    
    /** Atributo cdAgenciaDebito. */
    private Integer cdAgenciaDebito;
    
    /** Atributo cdDigitoAgenciaDebito. */
    private String cdDigitoAgenciaDebito;
    
    /** Atributo cdContaDebito. */
    private Long cdContaDebito;
    
    /** Atributo digitoContaDebito. */
    private String digitoContaDebito;
    
    /** Atributo dsComplemento. */
    private String dsComplemento;
    
    /** Atributo cdPessoaContratoDebito. */
    private Long cdPessoaContratoDebito;
    
    /** Atributo cdTipoContratoDebito. */
    private Integer cdTipoContratoDebito;
    
    /** Atributo nrSequenciaContratoDebito. */
    private Long nrSequenciaContratoDebito;
    
	/**
	 * Get: dsComplemento.
	 *
	 * @return dsComplemento
	 */
	public String getDsComplemento() {
		return dsComplemento;
	}
	
	/**
	 * Set: dsComplemento.
	 *
	 * @param dsComplemento the ds complemento
	 */
	public void setDsComplemento(String dsComplemento) {
		this.dsComplemento = dsComplemento;
	}
	
	/**
	 * Get: cdAgenciaDebito.
	 *
	 * @return cdAgenciaDebito
	 */
	public Integer getCdAgenciaDebito() {
		return cdAgenciaDebito;
	}
	
	/**
	 * Set: cdAgenciaDebito.
	 *
	 * @param cdAgenciaDebito the cd agencia debito
	 */
	public void setCdAgenciaDebito(Integer cdAgenciaDebito) {
		this.cdAgenciaDebito = cdAgenciaDebito;
	}
	
	/**
	 * Get: cdBancoDebito.
	 *
	 * @return cdBancoDebito
	 */
	public Integer getCdBancoDebito() {
		return cdBancoDebito;
	}
	
	/**
	 * Set: cdBancoDebito.
	 *
	 * @param cdBancoDebito the cd banco debito
	 */
	public void setCdBancoDebito(Integer cdBancoDebito) {
		this.cdBancoDebito = cdBancoDebito;
	}
	
	/**
	 * Get: cdContaDebito.
	 *
	 * @return cdContaDebito
	 */
	public Long getCdContaDebito() {
		return cdContaDebito;
	}
	
	/**
	 * Set: cdContaDebito.
	 *
	 * @param cdContaDebito the cd conta debito
	 */
	public void setCdContaDebito(Long cdContaDebito) {
		this.cdContaDebito = cdContaDebito;
	}
	
	/**
	 * Get: cdCpfCnpj.
	 *
	 * @return cdCpfCnpj
	 */
	public Long getCdCpfCnpj() {
		return cdCpfCnpj;
	}
	
	/**
	 * Set: cdCpfCnpj.
	 *
	 * @param cdCpfCnpj the cd cpf cnpj
	 */
	public void setCdCpfCnpj(Long cdCpfCnpj) {
		this.cdCpfCnpj = cdCpfCnpj;
	}
	
	/**
	 * Get: cdDigitoAgenciaDebito.
	 *
	 * @return cdDigitoAgenciaDebito
	 */
	public String getCdDigitoAgenciaDebito() {
		return cdDigitoAgenciaDebito;
	}
	
	/**
	 * Set: cdDigitoAgenciaDebito.
	 *
	 * @param cdDigitoAgenciaDebito the cd digito agencia debito
	 */
	public void setCdDigitoAgenciaDebito(String cdDigitoAgenciaDebito) {
		this.cdDigitoAgenciaDebito = cdDigitoAgenciaDebito;
	}
	
	/**
	 * Get: cdDigitoCpfCnpj.
	 *
	 * @return cdDigitoCpfCnpj
	 */
	public Integer getCdDigitoCpfCnpj() {
		return cdDigitoCpfCnpj;
	}
	
	/**
	 * Set: cdDigitoCpfCnpj.
	 *
	 * @param cdDigitoCpfCnpj the cd digito cpf cnpj
	 */
	public void setCdDigitoCpfCnpj(Integer cdDigitoCpfCnpj) {
		this.cdDigitoCpfCnpj = cdDigitoCpfCnpj;
	}
	
	/**
	 * Get: cdFilialCpfCnpj.
	 *
	 * @return cdFilialCpfCnpj
	 */
	public Integer getCdFilialCpfCnpj() {
		return cdFilialCpfCnpj;
	}
	
	/**
	 * Set: cdFilialCpfCnpj.
	 *
	 * @param cdFilialCpfCnpj the cd filial cpf cnpj
	 */
	public void setCdFilialCpfCnpj(Integer cdFilialCpfCnpj) {
		this.cdFilialCpfCnpj = cdFilialCpfCnpj;
	}
	
	/**
	 * Get: cdModalidade.
	 *
	 * @return cdModalidade
	 */
	public Integer getCdModalidade() {
		return cdModalidade;
	}
	
	/**
	 * Set: cdModalidade.
	 *
	 * @param cdModalidade the cd modalidade
	 */
	public void setCdModalidade(Integer cdModalidade) {
		this.cdModalidade = cdModalidade;
	}
	
	/**
	 * Get: cdPessoaJuridicaContrato.
	 *
	 * @return cdPessoaJuridicaContrato
	 */
	public Long getCdPessoaJuridicaContrato() {
		return cdPessoaJuridicaContrato;
	}
	
	/**
	 * Set: cdPessoaJuridicaContrato.
	 *
	 * @param cdPessoaJuridicaContrato the cd pessoa juridica contrato
	 */
	public void setCdPessoaJuridicaContrato(Long cdPessoaJuridicaContrato) {
		this.cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
	}
	
	/**
	 * Get: cdServico.
	 *
	 * @return cdServico
	 */
	public Integer getCdServico() {
		return cdServico;
	}
	
	/**
	 * Set: cdServico.
	 *
	 * @param cdServico the cd servico
	 */
	public void setCdServico(Integer cdServico) {
		this.cdServico = cdServico;
	}
	
	/**
	 * Get: cdTipoContratoNegocio.
	 *
	 * @return cdTipoContratoNegocio
	 */
	public Integer getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}
	
	/**
	 * Set: cdTipoContratoNegocio.
	 *
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}
	
	/**
	 * Get: cdUnidadeOrganizacional.
	 *
	 * @return cdUnidadeOrganizacional
	 */
	public Integer getCdUnidadeOrganizacional() {
		return cdUnidadeOrganizacional;
	}
	
	/**
	 * Set: cdUnidadeOrganizacional.
	 *
	 * @param cdUnidadeOrganizacional the cd unidade organizacional
	 */
	public void setCdUnidadeOrganizacional(Integer cdUnidadeOrganizacional) {
		this.cdUnidadeOrganizacional = cdUnidadeOrganizacional;
	}
	
	/**
	 * Get: digitoContaDebito.
	 *
	 * @return digitoContaDebito
	 */
	public String getDigitoContaDebito() {
		return digitoContaDebito;
	}
	
	/**
	 * Set: digitoContaDebito.
	 *
	 * @param digitoContaDebito the digito conta debito
	 */
	public void setDigitoContaDebito(String digitoContaDebito) {
		this.digitoContaDebito = digitoContaDebito;
	}
	
	/**
	 * Get: nrOcorrencias.
	 *
	 * @return nrOcorrencias
	 */
	public Integer getNrOcorrencias() {
		return nrOcorrencias;
	}
	
	/**
	 * Set: nrOcorrencias.
	 *
	 * @param nrOcorrencias the nr ocorrencias
	 */
	public void setNrOcorrencias(Integer nrOcorrencias) {
		this.nrOcorrencias = nrOcorrencias;
	}
	
	/**
	 * Get: nrSequenciaContratoNegocio.
	 *
	 * @return nrSequenciaContratoNegocio
	 */
	public Long getNrSequenciaContratoNegocio() {
		return nrSequenciaContratoNegocio;
	}
	
	/**
	 * Set: nrSequenciaContratoNegocio.
	 *
	 * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
	 */
	public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}
	
	/**
	 * Get: cdPessoaContratoDebito.
	 *
	 * @return cdPessoaContratoDebito
	 */
	public Long getCdPessoaContratoDebito() {
		return cdPessoaContratoDebito;
	}
	
	/**
	 * Set: cdPessoaContratoDebito.
	 *
	 * @param cdPessoaContratoDebito the cd pessoa contrato debito
	 */
	public void setCdPessoaContratoDebito(Long cdPessoaContratoDebito) {
		this.cdPessoaContratoDebito = cdPessoaContratoDebito;
	}
	
	/**
	 * Get: cdTipoContratoDebito.
	 *
	 * @return cdTipoContratoDebito
	 */
	public Integer getCdTipoContratoDebito() {
		return cdTipoContratoDebito;
	}
	
	/**
	 * Set: cdTipoContratoDebito.
	 *
	 * @param cdTipoContratoDebito the cd tipo contrato debito
	 */
	public void setCdTipoContratoDebito(Integer cdTipoContratoDebito) {
		this.cdTipoContratoDebito = cdTipoContratoDebito;
	}
	
	/**
	 * Get: nrSequenciaContratoDebito.
	 *
	 * @return nrSequenciaContratoDebito
	 */
	public Long getNrSequenciaContratoDebito() {
		return nrSequenciaContratoDebito;
	}
	
	/**
	 * Set: nrSequenciaContratoDebito.
	 *
	 * @param nrSequenciaContratoDebito the nr sequencia contrato debito
	 */
	public void setNrSequenciaContratoDebito(Long nrSequenciaContratoDebito) {
		this.nrSequenciaContratoDebito = nrSequenciaContratoDebito;
	}
    
	
}


