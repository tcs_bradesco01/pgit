/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantercontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantercontrato.bean;

/**
 * Nome: FinalidadeAlterarContaEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class FinalidadeAlterarContaEntradaDTO {
    
    /** Atributo cdFinalidadeContaPagamento. */
    private Integer cdFinalidadeContaPagamento;
    
    /** Atributo cdTipoAcaoFinalidade. */
    private String cdTipoAcaoFinalidade;
    
	/**
	 * Get: cdFinalidadeContaPagamento.
	 *
	 * @return cdFinalidadeContaPagamento
	 */
	public Integer getCdFinalidadeContaPagamento() {
		return cdFinalidadeContaPagamento;
	}
	
	/**
	 * Set: cdFinalidadeContaPagamento.
	 *
	 * @param cdFinalidadeContaPagamento the cd finalidade conta pagamento
	 */
	public void setCdFinalidadeContaPagamento(Integer cdFinalidadeContaPagamento) {
		this.cdFinalidadeContaPagamento = cdFinalidadeContaPagamento;
	}
	
	/**
	 * Get: cdTipoAcaoFinalidade.
	 *
	 * @return cdTipoAcaoFinalidade
	 */
	public String getCdTipoAcaoFinalidade() {
		return cdTipoAcaoFinalidade;
	}
	
	/**
	 * Set: cdTipoAcaoFinalidade.
	 *
	 * @param cdTipoAcaoFinalidade the cd tipo acao finalidade
	 */
	public void setCdTipoAcaoFinalidade(String cdTipoAcaoFinalidade) {
		this.cdTipoAcaoFinalidade = cdTipoAcaoFinalidade;
	}
    
}
