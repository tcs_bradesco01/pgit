/*
 * Nome: br.com.bradesco.web.pgit.service.business.manterfavorecido.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.manterfavorecido.bean;

/**
 * Nome: ConsultarListaHistoricoFavorecidoEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarListaHistoricoFavorecidoEntradaDTO {

    /** Atributo cdPessoaJuridicaContrato. */
    private Long cdPessoaJuridicaContrato;

    /** Atributo cdTipoContratoNegocio. */
    private Integer cdTipoContratoNegocio;

    /** Atributo nrSequenciaContratoNegocio. */
    private Long nrSequenciaContratoNegocio;

    /** Atributo cdFavorecidoClientePagador. */
    private Long cdFavorecidoClientePagador;

    /** Atributo cdTipoIsncricaoFavorecido. */
    private Integer cdTipoIsncricaoFavorecido;

    /** Atributo cdInscricaoFavorecido. */
    private Long cdInscricaoFavorecido;

    /** Atributo dataInicio. */
    private String dataInicio;

    /** Atributo dataFim. */
    private String dataFim;

    /** Atributo cdPesquisaLista. */
    private Integer cdPesquisaLista;

    /**
     * Get: cdFavorecidoClientePagador.
     *
     * @return cdFavorecidoClientePagador
     */
    public Long getCdFavorecidoClientePagador() {
	return cdFavorecidoClientePagador;
    }

    /**
     * Set: cdFavorecidoClientePagador.
     *
     * @param cdFavorecidoClientePagador the cd favorecido cliente pagador
     */
    public void setCdFavorecidoClientePagador(Long cdFavorecidoClientePagador) {
	this.cdFavorecidoClientePagador = cdFavorecidoClientePagador;
    }

    /**
     * Get: cdPessoaJuridicaContrato.
     *
     * @return cdPessoaJuridicaContrato
     */
    public Long getCdPessoaJuridicaContrato() {
	return cdPessoaJuridicaContrato;
    }

    /**
     * Set: cdPessoaJuridicaContrato.
     *
     * @param cdPessoaJuridicaContrato the cd pessoa juridica contrato
     */
    public void setCdPessoaJuridicaContrato(Long cdPessoaJuridicaContrato) {
	this.cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
    }

    /**
     * Get: cdTipoContratoNegocio.
     *
     * @return cdTipoContratoNegocio
     */
    public Integer getCdTipoContratoNegocio() {
	return cdTipoContratoNegocio;
    }

    /**
     * Set: cdTipoContratoNegocio.
     *
     * @param cdTipoContratoNegocio the cd tipo contrato negocio
     */
    public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
	this.cdTipoContratoNegocio = cdTipoContratoNegocio;
    }

    /**
     * Get: nrSequenciaContratoNegocio.
     *
     * @return nrSequenciaContratoNegocio
     */
    public Long getNrSequenciaContratoNegocio() {
	return nrSequenciaContratoNegocio;
    }

    /**
     * Set: nrSequenciaContratoNegocio.
     *
     * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
     */
    public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
	this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
    }

    /**
     * Get: cdInscricaoFavorecido.
     *
     * @return cdInscricaoFavorecido
     */
    public Long getCdInscricaoFavorecido() {
	return cdInscricaoFavorecido;
    }

    /**
     * Set: cdInscricaoFavorecido.
     *
     * @param cdInscricaoFavorecido the cd inscricao favorecido
     */
    public void setCdInscricaoFavorecido(Long cdInscricaoFavorecido) {
	this.cdInscricaoFavorecido = cdInscricaoFavorecido;
    }

    /**
     * Get: cdPesquisaLista.
     *
     * @return cdPesquisaLista
     */
    public Integer getCdPesquisaLista() {
	return cdPesquisaLista;
    }

    /**
     * Set: cdPesquisaLista.
     *
     * @param cdPesquisaLista the cd pesquisa lista
     */
    public void setCdPesquisaLista(Integer cdPesquisaLista) {
	this.cdPesquisaLista = cdPesquisaLista;
    }

    /**
     * Get: cdTipoIsncricaoFavorecido.
     *
     * @return cdTipoIsncricaoFavorecido
     */
    public Integer getCdTipoIsncricaoFavorecido() {
	return cdTipoIsncricaoFavorecido;
    }

    /**
     * Set: cdTipoIsncricaoFavorecido.
     *
     * @param cdTipoIsncricaoFavorecido the cd tipo isncricao favorecido
     */
    public void setCdTipoIsncricaoFavorecido(Integer cdTipoIsncricaoFavorecido) {
	this.cdTipoIsncricaoFavorecido = cdTipoIsncricaoFavorecido;
    }

    /**
     * Get: dataFim.
     *
     * @return dataFim
     */
    public String getDataFim() {
	return dataFim;
    }

    /**
     * Set: dataFim.
     *
     * @param dataFim the data fim
     */
    public void setDataFim(String dataFim) {
	this.dataFim = dataFim;
    }

    /**
     * Get: dataInicio.
     *
     * @return dataInicio
     */
    public String getDataInicio() {
	return dataInicio;
    }

    /**
     * Set: dataInicio.
     *
     * @param dataInicio the data inicio
     */
    public void setDataInicio(String dataInicio) {
	this.dataInicio = dataInicio;
    }

}
