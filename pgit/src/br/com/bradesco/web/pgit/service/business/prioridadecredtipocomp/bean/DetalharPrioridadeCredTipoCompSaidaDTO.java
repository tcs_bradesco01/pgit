/*
 * Nome: br.com.bradesco.web.pgit.service.business.prioridadecredtipocomp.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.prioridadecredtipocomp.bean;

/**
 * Nome: DetalharPrioridadeCredTipoCompSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class DetalharPrioridadeCredTipoCompSaidaDTO {
	
	/** Atributo codMensagem. */
	private String codMensagem;
	
	/** Atributo mensagem. */
	private String mensagem;
	
	/** Atributo tipoComprimisso. */
	private int tipoComprimisso;
	
	/** Atributo prioridade. */
	private int prioridade;
	
	/** Atributo dataInicioVigencia. */
	private String dataInicioVigencia;
	
	/** Atributo dataFinalVigencia. */
	private String dataFinalVigencia;
	
	/** Atributo dtInicioVigenciaCompromisso. */
	private String dtInicioVigenciaCompromisso;
	
	/** Atributo dtFimVigenciaCompromisso. */
	private String dtFimVigenciaCompromisso;

	
	/*Trilha de Auditoria*/
	/** Atributo dataHoraManutencao. */
	private String dataHoraManutencao;	
		
	/** Atributo complementoManutencao. */
	private String complementoManutencao;
	
	/** Atributo cdCanalManutencao. */
	private int cdCanalManutencao;
	
	/** Atributo dsCanalManutencao. */
	private String dsCanalManutencao;
	
	/** Atributo dataHoraInclusao. */
	private String dataHoraInclusao;	
	
	/** Atributo hrInclusaoRegistro. */
	private String hrInclusaoRegistro;
	
	/** Atributo hrManutencaoRegistro. */
	private String hrManutencaoRegistro;
	
	/** Atributo numOperacaoFluxoInclusao. */
	private String numOperacaoFluxoInclusao;
	
	/** Atributo numOperacaoFluxoManutencao. */
	private String numOperacaoFluxoManutencao;
	
	/** Atributo autenticacaoSegurancaInclusao. */
	private String autenticacaoSegurancaInclusao;
	
	/** Atributo autenticacaoSegurancaManutencao. */
	private String autenticacaoSegurancaManutencao;
	
	/** Atributo cdCanalInclusao. */
	private int cdCanalInclusao;
	
	/** Atributo dsCanalInclusao. */
	private String dsCanalInclusao;
	
	/** Atributo centroCusto. */
	private String centroCusto;
	
	
	
	/**
	 * Get: cdCanalInclusao.
	 *
	 * @return cdCanalInclusao
	 */
	public int getCdCanalInclusao() {
		return cdCanalInclusao;
	}
	
	/**
	 * Set: cdCanalInclusao.
	 *
	 * @param cdCanalInclusao the cd canal inclusao
	 */
	public void setCdCanalInclusao(int cdCanalInclusao) {
		this.cdCanalInclusao = cdCanalInclusao;
	}
	
	/**
	 * Get: cdCanalManutencao.
	 *
	 * @return cdCanalManutencao
	 */
	public int getCdCanalManutencao() {
		return cdCanalManutencao;
	}
	
	/**
	 * Set: cdCanalManutencao.
	 *
	 * @param cdCanalManutencao the cd canal manutencao
	 */
	public void setCdCanalManutencao(int cdCanalManutencao) {
		this.cdCanalManutencao = cdCanalManutencao;
	}

	/**
	 * Get: complementoManutencao.
	 *
	 * @return complementoManutencao
	 */
	public String getComplementoManutencao() {
		return complementoManutencao;
	}
	
	/**
	 * Set: complementoManutencao.
	 *
	 * @param complementoManutencao the complemento manutencao
	 */
	public void setComplementoManutencao(String complementoManutencao) {
		this.complementoManutencao = complementoManutencao;
	}
	
	/**
	 * Get: autenticacaoSegurancaInclusao.
	 *
	 * @return autenticacaoSegurancaInclusao
	 */
	public String getAutenticacaoSegurancaInclusao() {
		return autenticacaoSegurancaInclusao;
	}

	/**
	 * Set: autenticacaoSegurancaInclusao.
	 *
	 * @param autenticacaoSegurancaInclusao the autenticacao seguranca inclusao
	 */
	public void setAutenticacaoSegurancaInclusao(
			String autenticacaoSegurancaInclusao) {
		this.autenticacaoSegurancaInclusao = autenticacaoSegurancaInclusao;
	}
	
	/**
	 * Get: autenticacaoSegurancaManutencao.
	 *
	 * @return autenticacaoSegurancaManutencao
	 */
	public String getAutenticacaoSegurancaManutencao() {
		return autenticacaoSegurancaManutencao;
	}
	
	/**
	 * Set: autenticacaoSegurancaManutencao.
	 *
	 * @param autenticacaoSegurancaManutencao the autenticacao seguranca manutencao
	 */
	public void setAutenticacaoSegurancaManutencao(
			String autenticacaoSegurancaManutencao) {
		this.autenticacaoSegurancaManutencao = autenticacaoSegurancaManutencao;
	}
	
	/**
	 * Get: dataHoraInclusao.
	 *
	 * @return dataHoraInclusao
	 */
	public String getDataHoraInclusao() {
		return dataHoraInclusao;
	}
	
	/**
	 * Set: dataHoraInclusao.
	 *
	 * @param dataHoraInclusao the data hora inclusao
	 */
	public void setDataHoraInclusao(String dataHoraInclusao) {
		this.dataHoraInclusao = dataHoraInclusao;
	}
	
	/**
	 * Get: dataHoraManutencao.
	 *
	 * @return dataHoraManutencao
	 */
	public String getDataHoraManutencao() {
		return dataHoraManutencao;
	}
	
	/**
	 * Set: dataHoraManutencao.
	 *
	 * @param dataHoraManutencao the data hora manutencao
	 */
	public void setDataHoraManutencao(String dataHoraManutencao) {
		this.dataHoraManutencao = dataHoraManutencao;
	}
	
	/**
	 * Get: dsCanalInclusao.
	 *
	 * @return dsCanalInclusao
	 */
	public String getDsCanalInclusao() {
		return dsCanalInclusao;
	}
	
	/**
	 * Set: dsCanalInclusao.
	 *
	 * @param dsCanalInclusao the ds canal inclusao
	 */
	public void setDsCanalInclusao(String dsCanalInclusao) {
		this.dsCanalInclusao = dsCanalInclusao;
	}
	
	/**
	 * Get: dsCanalManutencao.
	 *
	 * @return dsCanalManutencao
	 */
	public String getDsCanalManutencao() {
		return dsCanalManutencao;
	}
	
	/**
	 * Set: dsCanalManutencao.
	 *
	 * @param dsCanalManutencao the ds canal manutencao
	 */
	public void setDsCanalManutencao(String dsCanalManutencao) {
		this.dsCanalManutencao = dsCanalManutencao;
	}

	/**
	 * Get: numOperacaoFluxoInclusao.
	 *
	 * @return numOperacaoFluxoInclusao
	 */
	public String getNumOperacaoFluxoInclusao() {
		return numOperacaoFluxoInclusao;
	}
	
	/**
	 * Set: numOperacaoFluxoInclusao.
	 *
	 * @param numOperacaoFluxoInclusao the num operacao fluxo inclusao
	 */
	public void setNumOperacaoFluxoInclusao(String numOperacaoFluxoInclusao) {
		this.numOperacaoFluxoInclusao = numOperacaoFluxoInclusao;
	}
	
	/**
	 * Get: numOperacaoFluxoManutencao.
	 *
	 * @return numOperacaoFluxoManutencao
	 */
	public String getNumOperacaoFluxoManutencao() {
		return numOperacaoFluxoManutencao;
	}
	
	/**
	 * Set: numOperacaoFluxoManutencao.
	 *
	 * @param numOperacaoFluxoManutencao the num operacao fluxo manutencao
	 */
	public void setNumOperacaoFluxoManutencao(String numOperacaoFluxoManutencao) {
		this.numOperacaoFluxoManutencao = numOperacaoFluxoManutencao;
	}
	
	/**
	 * Get: hrInclusaoRegistro.
	 *
	 * @return hrInclusaoRegistro
	 */
	public String getHrInclusaoRegistro() {
		return hrInclusaoRegistro;
	}
	
	/**
	 * Set: hrInclusaoRegistro.
	 *
	 * @param hrInclusaoRegistro the hr inclusao registro
	 */
	public void setHrInclusaoRegistro(String hrInclusaoRegistro) {
		this.hrInclusaoRegistro = hrInclusaoRegistro;
	}
	
	/**
	 * Get: hrManutencaoRegistro.
	 *
	 * @return hrManutencaoRegistro
	 */
	public String getHrManutencaoRegistro() {
		return hrManutencaoRegistro;
	}
	
	/**
	 * Set: hrManutencaoRegistro.
	 *
	 * @param hrManutencaoRegistro the hr manutencao registro
	 */
	public void setHrManutencaoRegistro(String hrManutencaoRegistro) {
		this.hrManutencaoRegistro = hrManutencaoRegistro;
	}
	
	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}
	
	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	/**
	 * Get: tipoComprimisso.
	 *
	 * @return tipoComprimisso
	 */
	public int getTipoComprimisso() {
		return tipoComprimisso;
	}
	
	/**
	 * Set: tipoComprimisso.
	 *
	 * @param tipoComprimisso the tipo comprimisso
	 */
	public void setTipoComprimisso(int tipoComprimisso) {
		this.tipoComprimisso = tipoComprimisso;
	}
	
	/**
	 * Get: prioridade.
	 *
	 * @return prioridade
	 */
	public int getPrioridade() {
		return prioridade;
	}
	
	/**
	 * Set: prioridade.
	 *
	 * @param prioridade the prioridade
	 */
	public void setPrioridade(int prioridade) {
		this.prioridade = prioridade;
	}
	
	/**
	 * Get: dataInicioVigencia.
	 *
	 * @return dataInicioVigencia
	 */
	public String getDataInicioVigencia() {
		return dataInicioVigencia;
	}
	
	/**
	 * Set: dataInicioVigencia.
	 *
	 * @param dataInicioVigencia the data inicio vigencia
	 */
	public void setDataInicioVigencia(String dataInicioVigencia) {
		this.dataInicioVigencia = dataInicioVigencia;
	}
	
	/**
	 * Get: dataFinalVigencia.
	 *
	 * @return dataFinalVigencia
	 */
	public String getDataFinalVigencia() {
		return dataFinalVigencia;
	}
	
	/**
	 * Set: dataFinalVigencia.
	 *
	 * @param dataFinalVigencia the data final vigencia
	 */
	public void setDataFinalVigencia(String dataFinalVigencia) {
		this.dataFinalVigencia = dataFinalVigencia;
	}
	
	/**
	 * Get: dtFimVigenciaCompromisso.
	 *
	 * @return dtFimVigenciaCompromisso
	 */
	public String getDtFimVigenciaCompromisso() {
		return dtFimVigenciaCompromisso;
	}
	
	/**
	 * Set: dtFimVigenciaCompromisso.
	 *
	 * @param dtFimVigenciaCompromisso the dt fim vigencia compromisso
	 */
	public void setDtFimVigenciaCompromisso(String dtFimVigenciaCompromisso) {
		this.dtFimVigenciaCompromisso = dtFimVigenciaCompromisso;
	}
	
	/**
	 * Get: dtInicioVigenciaCompromisso.
	 *
	 * @return dtInicioVigenciaCompromisso
	 */
	public String getDtInicioVigenciaCompromisso() {
		return dtInicioVigenciaCompromisso;
	}
	
	/**
	 * Set: dtInicioVigenciaCompromisso.
	 *
	 * @param dtInicioVigenciaCompromisso the dt inicio vigencia compromisso
	 */
	public void setDtInicioVigenciaCompromisso(String dtInicioVigenciaCompromisso) {
		this.dtInicioVigenciaCompromisso = dtInicioVigenciaCompromisso;
	}
	
	/**
	 * Get: centroCusto.
	 *
	 * @return centroCusto
	 */
	public String getCentroCusto() {
		return centroCusto;
	}
	
	/**
	 * Set: centroCusto.
	 *
	 * @param centroCusto the centro custo
	 */
	public void setCentroCusto(String centroCusto) {
		this.centroCusto = centroCusto;
	}


}
