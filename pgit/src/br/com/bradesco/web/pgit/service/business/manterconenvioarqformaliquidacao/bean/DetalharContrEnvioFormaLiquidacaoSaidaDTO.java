/*
 * Nome: br.com.bradesco.web.pgit.service.business.manterconenvioarqformaliquidacao.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.manterconenvioarqformaliquidacao.bean;

/**
 * Nome: DetalharContrEnvioFormaLiquidacaoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class DetalharContrEnvioFormaLiquidacaoSaidaDTO{
	
	/** Atributo codMensagem. */
	private String codMensagem;
	
	/** Atributo mensagem. */
	private String mensagem;
	
	/** Atributo cdFormaLiquidacao. */
	private Integer cdFormaLiquidacao;
	
	/** Atributo nrEnvioFormaLiquidacaoDoc. */
	private Integer nrEnvioFormaLiquidacaoDoc;
	
	/** Atributo hrEnvioFormaLiquidacaoDoc. */
	private String hrEnvioFormaLiquidacaoDoc;
	
	/** Atributo dsFormaLiquidacao. */
	private String dsFormaLiquidacao;
	
	/** Atributo cdUsuarioInclusao. */
	private String cdUsuarioInclusao;
	
	/** Atributo hrInclusaoRegistro. */
	private String hrInclusaoRegistro;
	
	/** Atributo cdOperacaoCanalInclusao. */
	private String cdOperacaoCanalInclusao;
	
	/** Atributo cdTipoCanalInclusao. */
	private Integer cdTipoCanalInclusao;
	
	/** Atributo cdUsuarioManutencao. */
	private String cdUsuarioManutencao;
	
	/** Atributo hrManutencaoRegistro. */
	private String hrManutencaoRegistro;
	
	/** Atributo cdOperacaoCanalManutencao. */
	private String cdOperacaoCanalManutencao;
	
	/** Atributo cdTipoCanalManutencao. */
	private Integer cdTipoCanalManutencao;
	
	/** Atributo dsTipoCanalInclusao. */
	private String dsTipoCanalInclusao;
	
	/** Atributo dsCanalManutencao. */
	private String dsCanalManutencao;

	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem(){
		return codMensagem;
	}

	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem){
		this.codMensagem = codMensagem;
	}

	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem(){
		return mensagem;
	}

	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem){
		this.mensagem = mensagem;
	}

	/**
	 * Get: cdFormaLiquidacao.
	 *
	 * @return cdFormaLiquidacao
	 */
	public Integer getCdFormaLiquidacao(){
		return cdFormaLiquidacao;
	}

	/**
	 * Set: cdFormaLiquidacao.
	 *
	 * @param cdFormaLiquidacao the cd forma liquidacao
	 */
	public void setCdFormaLiquidacao(Integer cdFormaLiquidacao){
		this.cdFormaLiquidacao = cdFormaLiquidacao;
	}

	/**
	 * Get: nrEnvioFormaLiquidacaoDoc.
	 *
	 * @return nrEnvioFormaLiquidacaoDoc
	 */
	public Integer getNrEnvioFormaLiquidacaoDoc(){
		return nrEnvioFormaLiquidacaoDoc;
	}

	/**
	 * Set: nrEnvioFormaLiquidacaoDoc.
	 *
	 * @param nrEnvioFormaLiquidacaoDoc the nr envio forma liquidacao doc
	 */
	public void setNrEnvioFormaLiquidacaoDoc(Integer nrEnvioFormaLiquidacaoDoc){
		this.nrEnvioFormaLiquidacaoDoc = nrEnvioFormaLiquidacaoDoc;
	}

	/**
	 * Get: hrEnvioFormaLiquidacaoDoc.
	 *
	 * @return hrEnvioFormaLiquidacaoDoc
	 */
	public String getHrEnvioFormaLiquidacaoDoc(){
		return hrEnvioFormaLiquidacaoDoc;
	}

	/**
	 * Set: hrEnvioFormaLiquidacaoDoc.
	 *
	 * @param hrEnvioFormaLiquidacaoDoc the hr envio forma liquidacao doc
	 */
	public void setHrEnvioFormaLiquidacaoDoc(String hrEnvioFormaLiquidacaoDoc){
		this.hrEnvioFormaLiquidacaoDoc = hrEnvioFormaLiquidacaoDoc;
	}

	/**
	 * Get: dsFormaLiquidacao.
	 *
	 * @return dsFormaLiquidacao
	 */
	public String getDsFormaLiquidacao(){
		return dsFormaLiquidacao;
	}

	/**
	 * Set: dsFormaLiquidacao.
	 *
	 * @param dsFormaLiquidacao the ds forma liquidacao
	 */
	public void setDsFormaLiquidacao(String dsFormaLiquidacao){
		this.dsFormaLiquidacao = dsFormaLiquidacao;
	}

	/**
	 * Get: cdUsuarioInclusao.
	 *
	 * @return cdUsuarioInclusao
	 */
	public String getCdUsuarioInclusao(){
		return cdUsuarioInclusao;
	}

	/**
	 * Set: cdUsuarioInclusao.
	 *
	 * @param cdUsuarioInclusao the cd usuario inclusao
	 */
	public void setCdUsuarioInclusao(String cdUsuarioInclusao){
		this.cdUsuarioInclusao = cdUsuarioInclusao;
	}

	/**
	 * Get: hrInclusaoRegistro.
	 *
	 * @return hrInclusaoRegistro
	 */
	public String getHrInclusaoRegistro(){
		return hrInclusaoRegistro;
	}

	/**
	 * Set: hrInclusaoRegistro.
	 *
	 * @param hrInclusaoRegistro the hr inclusao registro
	 */
	public void setHrInclusaoRegistro(String hrInclusaoRegistro){
		this.hrInclusaoRegistro = hrInclusaoRegistro;
	}

	/**
	 * Get: cdOperacaoCanalInclusao.
	 *
	 * @return cdOperacaoCanalInclusao
	 */
	public String getCdOperacaoCanalInclusao(){
		return cdOperacaoCanalInclusao;
	}

	/**
	 * Set: cdOperacaoCanalInclusao.
	 *
	 * @param cdOperacaoCanalInclusao the cd operacao canal inclusao
	 */
	public void setCdOperacaoCanalInclusao(String cdOperacaoCanalInclusao){
		this.cdOperacaoCanalInclusao = cdOperacaoCanalInclusao;
	}

	/**
	 * Get: cdTipoCanalInclusao.
	 *
	 * @return cdTipoCanalInclusao
	 */
	public Integer getCdTipoCanalInclusao(){
		return cdTipoCanalInclusao;
	}

	/**
	 * Set: cdTipoCanalInclusao.
	 *
	 * @param cdTipoCanalInclusao the cd tipo canal inclusao
	 */
	public void setCdTipoCanalInclusao(Integer cdTipoCanalInclusao){
		this.cdTipoCanalInclusao = cdTipoCanalInclusao;
	}

	/**
	 * Get: cdUsuarioManutencao.
	 *
	 * @return cdUsuarioManutencao
	 */
	public String getCdUsuarioManutencao(){
		return cdUsuarioManutencao;
	}

	/**
	 * Set: cdUsuarioManutencao.
	 *
	 * @param cdUsuarioManutencao the cd usuario manutencao
	 */
	public void setCdUsuarioManutencao(String cdUsuarioManutencao){
		this.cdUsuarioManutencao = cdUsuarioManutencao;
	}

	/**
	 * Get: hrManutencaoRegistro.
	 *
	 * @return hrManutencaoRegistro
	 */
	public String getHrManutencaoRegistro(){
		return hrManutencaoRegistro;
	}

	/**
	 * Set: hrManutencaoRegistro.
	 *
	 * @param hrManutencaoRegistro the hr manutencao registro
	 */
	public void setHrManutencaoRegistro(String hrManutencaoRegistro){
		this.hrManutencaoRegistro = hrManutencaoRegistro;
	}

	/**
	 * Get: cdOperacaoCanalManutencao.
	 *
	 * @return cdOperacaoCanalManutencao
	 */
	public String getCdOperacaoCanalManutencao(){
		return cdOperacaoCanalManutencao;
	}

	/**
	 * Set: cdOperacaoCanalManutencao.
	 *
	 * @param cdOperacaoCanalManutencao the cd operacao canal manutencao
	 */
	public void setCdOperacaoCanalManutencao(String cdOperacaoCanalManutencao){
		this.cdOperacaoCanalManutencao = cdOperacaoCanalManutencao;
	}

	/**
	 * Get: cdTipoCanalManutencao.
	 *
	 * @return cdTipoCanalManutencao
	 */
	public Integer getCdTipoCanalManutencao(){
		return cdTipoCanalManutencao;
	}

	/**
	 * Set: cdTipoCanalManutencao.
	 *
	 * @param cdTipoCanalManutencao the cd tipo canal manutencao
	 */
	public void setCdTipoCanalManutencao(Integer cdTipoCanalManutencao){
		this.cdTipoCanalManutencao = cdTipoCanalManutencao;
	}

	/**
	 * Get: dsTipoCanalInclusao.
	 *
	 * @return dsTipoCanalInclusao
	 */
	public String getDsTipoCanalInclusao() {
	    return dsTipoCanalInclusao;
	}

	/**
	 * Set: dsTipoCanalInclusao.
	 *
	 * @param dsTipoCanalInclusao the ds tipo canal inclusao
	 */
	public void setDsTipoCanalInclusao(String dsTipoCanalInclusao) {
	    this.dsTipoCanalInclusao = dsTipoCanalInclusao;
	}

	/**
	 * Get: dsCanalManutencao.
	 *
	 * @return dsCanalManutencao
	 */
	public String getDsCanalManutencao() {
	    return dsCanalManutencao;
	}

	/**
	 * Set: dsCanalManutencao.
	 *
	 * @param dsCanalManutencao the ds canal manutencao
	 */
	public void setDsCanalManutencao(String dsCanalManutencao) {
	    this.dsCanalManutencao = dsCanalManutencao;
	}
	
	
}