/*
 * =========================================================================
 * 
 * Client:       Bradesco (BR)
 * Project:      Arquitetura Bradesco Canal Internet
 * Development:  GFT Iberia (http://www.gft.com)
 * -------------------------------------------------------------------------
 * Revision - Last:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/constants.ftl,v $
 * $Id: constants.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revision - History:
 * $Log: constants.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.solemissaomovfavorecido;


/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Interface de constantes do adaptador: SolEmissaoMovFavorecido
 * </p>
 * 
 * @comment C�DIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public interface ISolEmissaoMovFavorecidoServiceConstants {
	
	/** Atributo NUMERO_OCORRENCIAS_CONSULTAR. */
	int NUMERO_OCORRENCIAS_CONSULTAR = 100;
	
	/** Atributo NUMERO_OCORRENCIAS_DETALHAR. */
	int NUMERO_OCORRENCIAS_DETALHAR = 70;
	
	/** Atributo NUMERO_REGISTROS_INCLUIR. */
	int NUMERO_REGISTROS_INCLUIR = 0;
	
	/** Atributo CONTROLE_PAGAMENTO_INCLUIR. */
	String CONTROLE_PAGAMENTO_INCLUIR = "0";
	
	/** Atributo TIPO_CANAL_INCLUIR. */
	int TIPO_CANAL_INCLUIR = 0;
	
	/** Atributo CODIGO_TIPO_CONTRATO_EXCLUIR. */
	int CODIGO_TIPO_CONTRATO_EXCLUIR = 0;
}