/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listarsolirelatoriopagamento.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ListarSoliRelatorioPagamentoRequest.
 * 
 * @version $Revision$ $Date$
 */
public class ListarSoliRelatorioPagamentoRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _numeroOcorrencias
     */
    private int _numeroOcorrencias = 0;

    /**
     * keeps track of state for field: _numeroOcorrencias
     */
    private boolean _has_numeroOcorrencias;

    /**
     * Field _cdTipoSolicitacao
     */
    private int _cdTipoSolicitacao = 0;

    /**
     * keeps track of state for field: _cdTipoSolicitacao
     */
    private boolean _has_cdTipoSolicitacao;

    /**
     * Field _dtInicioSolicitacao
     */
    private java.lang.String _dtInicioSolicitacao;

    /**
     * Field _dtFimSolicitacao
     */
    private java.lang.String _dtFimSolicitacao;


      //----------------/
     //- Constructors -/
    //----------------/

    public ListarSoliRelatorioPagamentoRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarsolirelatoriopagamento.request.ListarSoliRelatorioPagamentoRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdTipoSolicitacao
     * 
     */
    public void deleteCdTipoSolicitacao()
    {
        this._has_cdTipoSolicitacao= false;
    } //-- void deleteCdTipoSolicitacao() 

    /**
     * Method deleteNumeroOcorrencias
     * 
     */
    public void deleteNumeroOcorrencias()
    {
        this._has_numeroOcorrencias= false;
    } //-- void deleteNumeroOcorrencias() 

    /**
     * Returns the value of field 'cdTipoSolicitacao'.
     * 
     * @return int
     * @return the value of field 'cdTipoSolicitacao'.
     */
    public int getCdTipoSolicitacao()
    {
        return this._cdTipoSolicitacao;
    } //-- int getCdTipoSolicitacao() 

    /**
     * Returns the value of field 'dtFimSolicitacao'.
     * 
     * @return String
     * @return the value of field 'dtFimSolicitacao'.
     */
    public java.lang.String getDtFimSolicitacao()
    {
        return this._dtFimSolicitacao;
    } //-- java.lang.String getDtFimSolicitacao() 

    /**
     * Returns the value of field 'dtInicioSolicitacao'.
     * 
     * @return String
     * @return the value of field 'dtInicioSolicitacao'.
     */
    public java.lang.String getDtInicioSolicitacao()
    {
        return this._dtInicioSolicitacao;
    } //-- java.lang.String getDtInicioSolicitacao() 

    /**
     * Returns the value of field 'numeroOcorrencias'.
     * 
     * @return int
     * @return the value of field 'numeroOcorrencias'.
     */
    public int getNumeroOcorrencias()
    {
        return this._numeroOcorrencias;
    } //-- int getNumeroOcorrencias() 

    /**
     * Method hasCdTipoSolicitacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoSolicitacao()
    {
        return this._has_cdTipoSolicitacao;
    } //-- boolean hasCdTipoSolicitacao() 

    /**
     * Method hasNumeroOcorrencias
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNumeroOcorrencias()
    {
        return this._has_numeroOcorrencias;
    } //-- boolean hasNumeroOcorrencias() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdTipoSolicitacao'.
     * 
     * @param cdTipoSolicitacao the value of field
     * 'cdTipoSolicitacao'.
     */
    public void setCdTipoSolicitacao(int cdTipoSolicitacao)
    {
        this._cdTipoSolicitacao = cdTipoSolicitacao;
        this._has_cdTipoSolicitacao = true;
    } //-- void setCdTipoSolicitacao(int) 

    /**
     * Sets the value of field 'dtFimSolicitacao'.
     * 
     * @param dtFimSolicitacao the value of field 'dtFimSolicitacao'
     */
    public void setDtFimSolicitacao(java.lang.String dtFimSolicitacao)
    {
        this._dtFimSolicitacao = dtFimSolicitacao;
    } //-- void setDtFimSolicitacao(java.lang.String) 

    /**
     * Sets the value of field 'dtInicioSolicitacao'.
     * 
     * @param dtInicioSolicitacao the value of field
     * 'dtInicioSolicitacao'.
     */
    public void setDtInicioSolicitacao(java.lang.String dtInicioSolicitacao)
    {
        this._dtInicioSolicitacao = dtInicioSolicitacao;
    } //-- void setDtInicioSolicitacao(java.lang.String) 

    /**
     * Sets the value of field 'numeroOcorrencias'.
     * 
     * @param numeroOcorrencias the value of field
     * 'numeroOcorrencias'.
     */
    public void setNumeroOcorrencias(int numeroOcorrencias)
    {
        this._numeroOcorrencias = numeroOcorrencias;
        this._has_numeroOcorrencias = true;
    } //-- void setNumeroOcorrencias(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ListarSoliRelatorioPagamentoRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listarsolirelatoriopagamento.request.ListarSoliRelatorioPagamentoRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listarsolirelatoriopagamento.request.ListarSoliRelatorioPagamentoRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listarsolirelatoriopagamento.request.ListarSoliRelatorioPagamentoRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarsolirelatoriopagamento.request.ListarSoliRelatorioPagamentoRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
