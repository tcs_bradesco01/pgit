/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/implementation.ftl,v $
 * $Id: implementation.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: implementation.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */
 
package br.com.bradesco.web.pgit.service.business.anteciparpostergarpagtosageind.impl;

import java.util.ArrayList;
import java.util.List;

import br.com.bradesco.web.pgit.service.business.anteciparpostergarpagtosageind.IAnteciparPostergarPagtosAgeIndService;
import br.com.bradesco.web.pgit.service.business.anteciparpostergarpagtosageind.IAnteciparPostergarPagtosAgeIndServiceConstants;
import br.com.bradesco.web.pgit.service.business.anteciparpostergarpagtosageind.bean.AnteciparPostergarPagtosIndividuaisEntradaDTO;
import br.com.bradesco.web.pgit.service.business.anteciparpostergarpagtosageind.bean.AnteciparPostergarPagtosIndividuaisSaidaDTO;
import br.com.bradesco.web.pgit.service.business.anteciparpostergarpagtosageind.bean.ConsultarPagtosIndAntPostEntradaDTO;
import br.com.bradesco.web.pgit.service.business.anteciparpostergarpagtosageind.bean.ConsultarPagtosIndAntPostSaidaDTO;
import br.com.bradesco.web.pgit.service.data.pdc.FactoryAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.anteciparpostergarpagtosindividuais.request.AnteciparPostergarPagtosIndividuaisRequest;
import br.com.bradesco.web.pgit.service.data.pdc.anteciparpostergarpagtosindividuais.response.AnteciparPostergarPagtosIndividuaisResponse;
import br.com.bradesco.web.pgit.service.data.pdc.consultarpagtosindantpostergacao.request.ConsultarPagtosIndAntPostergacaoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarpagtosindantpostergacao.response.ConsultarPagtosIndAntPostergacaoResponse;
import br.com.bradesco.web.pgit.utils.CpfCnpjUtils;
import br.com.bradesco.web.pgit.utils.DateUtils;
import br.com.bradesco.web.pgit.utils.PgitUtil;


/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Implementa��o do adaptador: AnteciparPostergarPagtosAgeInd
 * </p>
 * 
 * @comment C�DIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public class AnteciparPostergarPagtosAgeIndServiceImpl implements IAnteciparPostergarPagtosAgeIndService {

	
	/** Atributo factoryAdapter. */
	private FactoryAdapter factoryAdapter;
	
	/**
	 * Get: factoryAdapter.
	 *
	 * @return factoryAdapter
	 */
	public FactoryAdapter getFactoryAdapter() {
		return factoryAdapter;
	}
	
	/**
	 * Set: factoryAdapter.
	 *
	 * @param factoryAdapter the factory adapter
	 */
	public void setFactoryAdapter(FactoryAdapter factoryAdapter) {
		this.factoryAdapter = factoryAdapter;
	}
	
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.anteciparpostergarpagtosageind.IAnteciparPostergarPagtosAgeIndService#consultarPagtosIndAntPostergacao(br.com.bradesco.web.pgit.service.business.anteciparpostergarpagtosageind.bean.ConsultarPagtosIndAntPostEntradaDTO)
	 */
	public List<ConsultarPagtosIndAntPostSaidaDTO> consultarPagtosIndAntPostergacao(ConsultarPagtosIndAntPostEntradaDTO entradaDTO) {
		ConsultarPagtosIndAntPostergacaoRequest request = new ConsultarPagtosIndAntPostergacaoRequest();
		ConsultarPagtosIndAntPostergacaoResponse response = new ConsultarPagtosIndAntPostergacaoResponse();
    	List<ConsultarPagtosIndAntPostSaidaDTO> listaSaida = new ArrayList<ConsultarPagtosIndAntPostSaidaDTO>();
    	
    	request.setCdTipoPesquisa(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoPesquisa()));
    	request.setCdAgendadosPagosNaoPagos(PgitUtil.verificaIntegerNulo(entradaDTO.getCdAgendadosPagosNaoPagos()));
    	request.setNrOcorrencias(PgitUtil.verificaIntegerNulo(entradaDTO.getNrOcorrencias()));
    	request.setCdPessoaJuridicaContrato(PgitUtil.verificaLongNulo(entradaDTO.getCdPessoaJuridicaContrato()));
    	request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoContratoNegocio()));
    	request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(entradaDTO.getNrSequenciaContratoNegocio()));
    	request.setCdBanco(PgitUtil.verificaIntegerNulo(entradaDTO.getCdBanco()));
    	request.setCdAgencia(PgitUtil.verificaIntegerNulo(entradaDTO.getCdAgencia()));
    	request.setCdConta(PgitUtil.verificaLongNulo(entradaDTO.getCdConta()));
    	request.setCdDigitoConta(PgitUtil.DIGITO_CONTA);
    	request.setDtCreditoPagamentoInicio(PgitUtil.verificaStringNula(entradaDTO.getDtCreditoPagamentoInicio()));
    	request.setDtCreditoPagamentoFim(PgitUtil.verificaStringNula(entradaDTO.getDtCreditoPagamentoFim()));
    	request.setNrArquivoRemssaPagamento(PgitUtil.verificaLongNulo(entradaDTO.getNrArquivoRemssaPagamento()));
    	request.setCdParticipante(PgitUtil.verificaLongNulo(entradaDTO.getCdParticipante()));
    	request.setCdProdutoServicoOperacao(PgitUtil.verificaIntegerNulo(entradaDTO.getCdProdutoServicoOperacao()));
    	request.setCdProdutoServicoRelacionado(PgitUtil.verificaIntegerNulo(entradaDTO.getCdProdutoServicoRelacionado()));
    	request.setCdControlePagamentoDe(PgitUtil.verificaStringNula(entradaDTO.getCdControlePagamentoDe()));
    	request.setCdControlePagamentoAte(PgitUtil.verificaStringNula(entradaDTO.getCdControlePagamentoAte()));
    	request.setVlPagamentoDe(PgitUtil.verificaBigDecimalNulo(entradaDTO.getVlPagamentoDe()));
    	request.setVlPagamentoAte(PgitUtil.verificaBigDecimalNulo(entradaDTO.getVlPagamentoAte()));
    	request.setCdSituacaoOperacaoPagamento(PgitUtil.verificaIntegerNulo(entradaDTO.getCdSituacaoOperacaoPagamento()));
    	request.setCdMotivoSituacaoPagamento(PgitUtil.verificaIntegerNulo(entradaDTO.getCdMotivoSituacaoPagamento()));
    	request.setCdBarraDocumento(PgitUtil.verificaStringNula(entradaDTO.getCdBarraDocumento()));
    	request.setCdFavorecidoClientePagador(PgitUtil.verificaLongNulo(entradaDTO.getCdFavorecidoClientePagador()));
    	request.setCdInscricaoFavorecido(PgitUtil.verificaLongNulo(entradaDTO.getCdInscricaoFavorecido()));
    	request.setCdIdentificacaoInscricaoFavorecido(PgitUtil.verificaIntegerNulo(entradaDTO.getCdIdentificacaoInscricaoFavorecido()));
    	request.setCdBancoBeneficiario(PgitUtil.verificaIntegerNulo(entradaDTO.getCdBancoBeneficiario()));
    	request.setCdAgenciaBeneficiario(PgitUtil.verificaIntegerNulo(entradaDTO.getCdAgenciaBeneficiario()));
    	request.setCdContaBeneficiario(PgitUtil.verificaLongNulo(entradaDTO.getCdContaBeneficiario()));
    	request.setCdDigitoContaBeneficiario(PgitUtil.DIGITO_CONTA);
    	request.setNrUnidadeOrganizacional(PgitUtil.verificaIntegerNulo(entradaDTO.getNrUnidadeOrganizacional()));
    	request.setCdBeneficio(PgitUtil.verificaLongNulo(entradaDTO.getCdBeneficio()));
    	request.setCdEspecieBeneficioInss(PgitUtil.verificaIntegerNulo(entradaDTO.getCdEspecieBeneficioInss()));
    	request.setCdClassificacao(PgitUtil.verificaIntegerNulo(entradaDTO.getCdClassificacao()));
    	request.setCdEmpresaContrato(PgitUtil.verificaLongNulo(entradaDTO.getCdEmpresaContrato()));
    	request.setCdTipoConta(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoConta()));
    	request.setNrContrato(PgitUtil.verificaLongNulo(entradaDTO.getNrContrato()));
    	request.setNrOcorrencias(IAnteciparPostergarPagtosAgeIndServiceConstants.NUMERO_OCORRENCIAS_CONSULTAR);
    	request.setCdCpfCnpjParticipante(PgitUtil.verificaLongNulo(entradaDTO.getCdCpfCnpjParticipante()));
    	request.setCdFilialCnpjParticipante(PgitUtil.verificaIntegerNulo(entradaDTO.getCdFilialCnpjParticipante()));
    	request.setCdControleCnpjParticipante(PgitUtil.verificaIntegerNulo(entradaDTO.getCdControleCnpjParticipante()));
    	request.setCdPessoaContratoDebito(PgitUtil.verificaLongNulo(entradaDTO.getCdPessoaContratoDebito()));
    	request.setCdTipoContratoDebito(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoContratoDebito()));
    	request.setNrSequenciaContratoDebito(PgitUtil.verificaLongNulo(entradaDTO.getNrSequenciaContratoDebito()));
    	request.setCdPessoaContratoCredt(PgitUtil.verificaLongNulo(entradaDTO.getCdPessoaContratoCredt()));
    	request.setCdTipoContratoCredt(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoContratoCredt()));
    	request.setNrSequenciaContratoCredt(PgitUtil.verificaLongNulo(entradaDTO.getNrSequenciaContratoCredt()));
    	request.setCdIndiceSimulaPagamento(PgitUtil.verificaIntegerNulo(entradaDTO.getCdIndiceSimulaPagamento()));
    	request.setCdOrigemRecebidoEfetivacao(PgitUtil.verificaIntegerNulo(entradaDTO.getCdOrigemRecebidoEfetivacao()));    
    	request.setCdCpfCnpjParticipante(PgitUtil.verificaLongNulo(entradaDTO.getCdCpfCnpjParticipante()));
    	request.setCdFilialCnpjParticipante(PgitUtil.verificaIntegerNulo(entradaDTO.getCdFilialCnpjParticipante()));
    	request.setCdControleCnpjParticipante(PgitUtil.verificaIntegerNulo(entradaDTO.getCdControleCnpjParticipante()));
    	request.setCdPessoaContratoDebito(PgitUtil.verificaLongNulo(entradaDTO.getCdPessoaContratoDebito()));
    	request.setCdTipoContratoDebito(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoContratoDebito()));
    	request.setNrSequenciaContratoDebito(PgitUtil.verificaLongNulo(entradaDTO.getNrSequenciaContratoDebito()));
    	request.setCdPessoaContratoCredt(PgitUtil.verificaLongNulo(entradaDTO.getCdPessoaContratoCredt()));
    	request.setCdTipoContratoCredt(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoContratoCredt()));
    	request.setNrSequenciaContratoCredt(PgitUtil.verificaLongNulo(entradaDTO.getNrSequenciaContratoCredt()));
    	request.setCdIndiceSimulaPagamento(PgitUtil.verificaIntegerNulo(entradaDTO.getCdIndiceSimulaPagamento()));
    	request.setCdOrigemRecebidoEfetivacao(PgitUtil.verificaIntegerNulo(entradaDTO.getCdOrigemRecebidoEfetivacao()));      	
    	request.setCdTituloPgtoRastreado(0);
    	request.setCdTipoAntecipacao(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoAgendamento()));
    	
    	response = getFactoryAdapter().getConsultarPagtosIndAntPostergacaoPDCAdapter().invokeProcess(request);
    	
    	for(int i=0;i<response.getOcorrenciasCount();i++){
    		ConsultarPagtosIndAntPostSaidaDTO saida = new ConsultarPagtosIndAntPostSaidaDTO();
    		saida.setNrCnpjCpf(response.getOcorrencias(i).getNrCnpjCpf());
    		saida.setNrFilialCnpjCpf(response.getOcorrencias(i).getNrFilialCnpjCpf());
    		saida.setNrDigitoCnpjCpf(response.getOcorrencias(i).getNrDigitoCnpjCpf());
    		saida.setCdPessoaJuridicaContrato(response.getOcorrencias(i).getCdPessoaJuridicaContrato());
    		saida.setDsRazaoSocial(response.getOcorrencias(i).getDsRazaoSocial());
    		saida.setCdEmpresa(response.getOcorrencias(i).getCdEmpresa());
    		saida.setDsEmpresa(response.getOcorrencias(i).getDsEmpresa());
    		saida.setCdTipoContratoNegocio(response.getOcorrencias(i).getCdTipoContratoNegocio());
    		saida.setNrContrato(response.getOcorrencias(i).getNrContrato());
    		saida.setCdProdutoServicoOperacao(response.getOcorrencias(i).getCdProdutoServicoOperacao());
    		saida.setDsResumoProdutoServico(response.getOcorrencias(i).getDsResumoProdutoServico());
    		saida.setCdProdutoServicoRelacionado(response.getOcorrencias(i).getCdProdutoServicoRelacionado());
    		saida.setDsOperacaoProdutoServico(response.getOcorrencias(i).getDsOperacaoProdutoServico());
    		saida.setCdControlePagamento(response.getOcorrencias(i).getCdControlePagamento());
    		saida.setDtCreditoPagamento(DateUtils.formartarDataPDCparaPadraPtBr(response.getOcorrencias(i).getDtCreditoPagamento(), "dd.MM.yyyy", "dd/MM/yyyy"));
    		saida.setVlEfetivoPagamento(response.getOcorrencias(i).getVlEfetivoPagamento());
    		saida.setCdInscricaoFavorecido(response.getOcorrencias(i).getCdInscricaoFavorecido() == 0L ? "" : String.valueOf(response.getOcorrencias(i).getCdInscricaoFavorecido()));
    		saida.setDsFavorecido(response.getOcorrencias(i).getDsFavorecido());
    		saida.setCdBancoDebito(response.getOcorrencias(i).getCdBancoDebito());
    		saida.setCdAgenciaDebito(response.getOcorrencias(i).getCdAgenciaDebito());
    		saida.setCdDigitoAgenciaDebito(response.getOcorrencias(i).getCdDigitoAgenciaDebito());
    		saida.setCdContaDebito(response.getOcorrencias(i).getCdContaDebito());
    		saida.setCdDigitoContaDebito(response.getOcorrencias(i).getCdDigitoContaDebito());
    		saida.setCdBancoCredito(response.getOcorrencias(i).getCdBancoCredito());
    		saida.setCdAgenciaCredito(response.getOcorrencias(i).getCdAgenciaCredito());
    		saida.setCdDigitoAgenciaCredito(response.getOcorrencias(i).getCdDigitoAgenciaCredito());
    		saida.setCdContaCredito(response.getOcorrencias(i).getCdContaCredito());
    		saida.setCdDigitoContaCredito(response.getOcorrencias(i).getCdDigitoContaCredito());
    		saida.setCdSituacaoOperacaoPagamento(response.getOcorrencias(i).getCdSituacaoOperacaoPagamento());
    		saida.setDsSituacaoOperacaoPagamento(response.getOcorrencias(i).getDsSituacaoOperacaoPagamento());
    		saida.setCdMotivoSituacaoPagamento(response.getOcorrencias(i).getCdMotivoSituacaoPagamento());
    		saida.setDsMotivoSituacaoPagamento(response.getOcorrencias(i).getDsMotivoSituacaoPagamento());
    		saida.setCdTipoCanal(response.getOcorrencias(i).getCdTipoCanal());
    		saida.setCdTipoTela(response.getOcorrencias(i).getCdTipoTela());
    		saida.setDsEfetivacaoPagamento(response.getOcorrencias(i).getDsEfetivacaoPagamento());
    		
    		saida.setCpfCnpjFormatado(CpfCnpjUtils.formatCpfCnpjCompleto(response.getOcorrencias(i).getNrCnpjCpf(), response.getOcorrencias(i).getNrFilialCnpjCpf(), response.getOcorrencias(i).getNrDigitoCnpjCpf()));
    		saida.setFavorecidoBeneficiarioFormatado(response.getOcorrencias(i).getDsFavorecido());
    		saida.setBancoAgenciaContaDebitoFormatado(PgitUtil.formatBancoAgenciaConta(response.getOcorrencias(i).getCdBancoDebito(), response.getOcorrencias(i).getCdAgenciaDebito(), response.getOcorrencias(i).getCdDigitoAgenciaDebito(), response.getOcorrencias(i).getCdContaDebito(), response.getOcorrencias(i).getCdDigitoContaDebito(), true));
    		saida.setBancoAgenciaContaCreditoFormatado(PgitUtil.formatBancoAgenciaConta(response.getOcorrencias(i).getCdBancoCredito(), response.getOcorrencias(i).getCdAgenciaCredito(), response.getOcorrencias(i).getCdDigitoAgenciaCredito(), response.getOcorrencias(i).getCdContaCredito(), response.getOcorrencias(i).getCdDigitoContaCredito(), true));

    		saida.setBancoFormatado(PgitUtil.formatBanco(saida.getCdBancoDebito(), false));
    		saida.setAgenciaFormatada(PgitUtil.formatAgencia(saida.getCdAgenciaDebito(), saida.getCdDigitoAgenciaDebito(), false));    	
    		saida.setContaFormatada(PgitUtil.formatConta(saida.getCdContaDebito(), saida.getCdDigitoContaDebito(), false));
    		
    		saida.setBancoCreditoFormatado(PgitUtil.formatBanco(saida.getCdBancoCredito(), false));
    		saida.setAgenciaCreditoFormatada(PgitUtil.formatAgencia(saida.getCdAgenciaCredito(), saida.getCdDigitoAgenciaCredito(), false));
    		saida.setContaCreditoFormatada(PgitUtil.formatConta(saida.getCdContaCredito(), saida.getCdDigitoContaCredito(), false));    		
    		
    		saida.setDsIndicadorPagamento(response.getOcorrencias(i).getDsIndicadorPagamento());
    		
    		saida.setCdIspbPagtoDestino(response.getOcorrencias(i).getCdIspbPagtoDestino());
    		saida.setContaPagtoDestino(response.getOcorrencias(i).getContaPagtoDestino());

    		listaSaida.add(saida);
    	}
    	
    	return listaSaida;
	}
	
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.anteciparpostergarpagtosageind.IAnteciparPostergarPagtosAgeIndService#anteciparPostergarPagtosIndividuais(br.com.bradesco.web.pgit.service.business.anteciparpostergarpagtosageind.bean.AnteciparPostergarPagtosIndividuaisEntradaDTO)
	 */
	public AnteciparPostergarPagtosIndividuaisSaidaDTO anteciparPostergarPagtosIndividuais(AnteciparPostergarPagtosIndividuaisEntradaDTO entradaDTO) {
		
		AnteciparPostergarPagtosIndividuaisSaidaDTO saidaDTO = new AnteciparPostergarPagtosIndividuaisSaidaDTO();
		AnteciparPostergarPagtosIndividuaisRequest request = new AnteciparPostergarPagtosIndividuaisRequest();
		AnteciparPostergarPagtosIndividuaisResponse response = new AnteciparPostergarPagtosIndividuaisResponse();
		
		request.setNumeroConsultas(entradaDTO.getListaOcorrencias().size());
		request.setDtQUitacao(entradaDTO.getDtQuitacao());
		ArrayList<br.com.bradesco.web.pgit.service.data.pdc.anteciparpostergarpagtosindividuais.request.Ocorrencias> lista = new ArrayList<br.com.bradesco.web.pgit.service.data.pdc.anteciparpostergarpagtosindividuais.request.Ocorrencias>();
		
		//V�rias Ocorr�ncias para Antecipa��o
		 for (int i=0; i< entradaDTO.getListaOcorrencias().size(); i++){
			 br.com.bradesco.web.pgit.service.data.pdc.anteciparpostergarpagtosindividuais.request.Ocorrencias ocorrencia = new br.com.bradesco.web.pgit.service.data.pdc.anteciparpostergarpagtosindividuais.request.Ocorrencias();			
			ocorrencia.setCdPessoaJuridicaContrato(PgitUtil.verificaLongNulo(entradaDTO.getListaOcorrencias().get(i).getCdPessoaJuridicaContrato()));
			ocorrencia.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(entradaDTO.getListaOcorrencias().get(i).getCdTipoContratoNegocio()));
			ocorrencia.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(entradaDTO.getListaOcorrencias().get(i).getNrSequenciaContratoNegocio()));
			ocorrencia.setCdProdutoServicoOperacao(PgitUtil.verificaIntegerNulo(entradaDTO.getListaOcorrencias().get(i).getCdProdutoServicoOperacao()));
			ocorrencia.setCdProdutoServicoRelacionado(PgitUtil.verificaIntegerNulo(entradaDTO.getListaOcorrencias().get(i).getCdProdutoServicoRelacionado()));
			ocorrencia.setCdTipoCanal(PgitUtil.verificaIntegerNulo(entradaDTO.getListaOcorrencias().get(i).getCdTipoCanal()));
			ocorrencia.setCdControlePagamento(PgitUtil.verificaStringNula(entradaDTO.getListaOcorrencias().get(i).getCdControlePagamento()));
			ocorrencia.setCdIndicadorAntPosProPagamento(PgitUtil.verificaIntegerNulo(entradaDTO.getListaOcorrencias().get(i).getCdIndicadorAntPosProPagamento()));
			lista.add(ocorrencia);		
		}
				
		request.setOcorrencias((br.com.bradesco.web.pgit.service.data.pdc.anteciparpostergarpagtosindividuais.request.Ocorrencias[]) lista.toArray(new br.com.bradesco.web.pgit.service.data.pdc.anteciparpostergarpagtosindividuais.request.Ocorrencias[0]));		
		response = getFactoryAdapter().getAnteciparPostergarPagtosIndividuaisPDCAdapter().invokeProcess(request);
		
		saidaDTO.setCodMensagem(response.getCodMensagem());
		saidaDTO.setMensagem(response.getMensagem());
		
		return saidaDTO;
    }
}