/*
 * Nome: br.com.bradesco.web.pgit.service.business.manteroperacoesservrelacionado.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 09/03/2017
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.manteroperacoesservrelacionado.bean;

import java.math.BigDecimal;

/**
 * Nome: ConsultarTarifaCatalogoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>
 * 
 * @author : todo!
 * 
 * @version :
 */
public class ConsultarTarifaCatalogoSaidaDTO {

    /**
     * Atributo String
     */
    private String codMensagem = null;

    /**
     * Atributo String
     */
    private String mensagem = null;

    /**
     * Atributo Integer
     */
    private Integer cdCondicaoEconomica = null;

    /**
     * Atributo BigDecimal
     */
    private BigDecimal vrTarifaMinimaNegociada = null;

    /**
     * Atributo BigDecimal
     */
    private BigDecimal vrTarifaMaximaNegociada = null;

    /**
     * Nome: getCodMensagem
     * 
     * @return codMensagem
     */
    public String getCodMensagem() {
        return codMensagem;
    }

    /**
     * Nome: setCodMensagem
     * 
     * @param codMensagem
     */
    public void setCodMensagem(String codMensagem) {
        this.codMensagem = codMensagem;
    }

    /**
     * Nome: getMensagem
     * 
     * @return mensagem
     */
    public String getMensagem() {
        return mensagem;
    }

    /**
     * Nome: setMensagem
     * 
     * @param mensagem
     */
    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }

    /**
     * Nome: getCdCondicaoEconomica
     * 
     * @return cdCondicaoEconomica
     */
    public Integer getCdCondicaoEconomica() {
        return cdCondicaoEconomica;
    }

    /**
     * Nome: setCdCondicaoEconomica
     * 
     * @param cdCondicaoEconomica
     */
    public void setCdCondicaoEconomica(Integer cdCondicaoEconomica) {
        this.cdCondicaoEconomica = cdCondicaoEconomica;
    }

    /**
     * Nome: getVrTarifaMinimaNegociada
     * 
     * @return vrTarifaMinimaNegociada
     */
    public BigDecimal getVrTarifaMinimaNegociada() {
        return vrTarifaMinimaNegociada;
    }

    /**
     * Nome: setVrTarifaMinimaNegociada
     * 
     * @param vrTarifaMinimaNegociada
     */
    public void setVrTarifaMinimaNegociada(BigDecimal vrTarifaMinimaNegociada) {
        this.vrTarifaMinimaNegociada = vrTarifaMinimaNegociada;
    }

    /**
     * Nome: getVrTarifaMaximaNegociada
     * 
     * @return vrTarifaMaximaNegociada
     */
    public BigDecimal getVrTarifaMaximaNegociada() {
        return vrTarifaMaximaNegociada;
    }

    /**
     * Nome: setVrTarifaMaximaNegociada
     * 
     * @param vrTarifaMaximaNegociada
     */
    public void setVrTarifaMaximaNegociada(BigDecimal vrTarifaMaximaNegociada) {
        this.vrTarifaMaximaNegociada = vrTarifaMaximaNegociada;
    }
}
