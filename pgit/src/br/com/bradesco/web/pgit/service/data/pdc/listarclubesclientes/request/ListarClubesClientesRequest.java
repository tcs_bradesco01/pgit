/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listarclubesclientes.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ListarClubesClientesRequest.
 * 
 * @version $Revision$ $Date$
 */
public class ListarClubesClientesRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _nrOcorrencias
     */
    private int _nrOcorrencias = 0;

    /**
     * keeps track of state for field: _nrOcorrencias
     */
    private boolean _has_nrOcorrencias;

    /**
     * Field _cdCorpoCpfCnpj
     */
    private long _cdCorpoCpfCnpj = 0;

    /**
     * keeps track of state for field: _cdCorpoCpfCnpj
     */
    private boolean _has_cdCorpoCpfCnpj;

    /**
     * Field _cdFilialCnpj
     */
    private int _cdFilialCnpj = 0;

    /**
     * keeps track of state for field: _cdFilialCnpj
     */
    private boolean _has_cdFilialCnpj;

    /**
     * Field _cdControleCpfCnpj
     */
    private int _cdControleCpfCnpj = 0;

    /**
     * keeps track of state for field: _cdControleCpfCnpj
     */
    private boolean _has_cdControleCpfCnpj;

    /**
     * Field _cdBanco
     */
    private int _cdBanco = 0;

    /**
     * keeps track of state for field: _cdBanco
     */
    private boolean _has_cdBanco;

    /**
     * Field _cdAgencia
     */
    private int _cdAgencia = 0;

    /**
     * keeps track of state for field: _cdAgencia
     */
    private boolean _has_cdAgencia;

    /**
     * Field _cdDigitoConta
     */
    private java.lang.String _cdDigitoConta;

    /**
     * Field _cdConta
     */
    private long _cdConta = 0;

    /**
     * keeps track of state for field: _cdConta
     */
    private boolean _has_cdConta;

    /**
     * Field _dtNascimentoFundacao
     */
    private java.lang.String _dtNascimentoFundacao;

    /**
     * Field _dsNomeRazao
     */
    private java.lang.String _dsNomeRazao;

    /**
     * Field _cdTipoConta
     */
    private int _cdTipoConta = 0;

    /**
     * keeps track of state for field: _cdTipoConta
     */
    private boolean _has_cdTipoConta;


      //----------------/
     //- Constructors -/
    //----------------/

    public ListarClubesClientesRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarclubesclientes.request.ListarClubesClientesRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdAgencia
     * 
     */
    public void deleteCdAgencia()
    {
        this._has_cdAgencia= false;
    } //-- void deleteCdAgencia() 

    /**
     * Method deleteCdBanco
     * 
     */
    public void deleteCdBanco()
    {
        this._has_cdBanco= false;
    } //-- void deleteCdBanco() 

    /**
     * Method deleteCdConta
     * 
     */
    public void deleteCdConta()
    {
        this._has_cdConta= false;
    } //-- void deleteCdConta() 

    /**
     * Method deleteCdControleCpfCnpj
     * 
     */
    public void deleteCdControleCpfCnpj()
    {
        this._has_cdControleCpfCnpj= false;
    } //-- void deleteCdControleCpfCnpj() 

    /**
     * Method deleteCdCorpoCpfCnpj
     * 
     */
    public void deleteCdCorpoCpfCnpj()
    {
        this._has_cdCorpoCpfCnpj= false;
    } //-- void deleteCdCorpoCpfCnpj() 

    /**
     * Method deleteCdFilialCnpj
     * 
     */
    public void deleteCdFilialCnpj()
    {
        this._has_cdFilialCnpj= false;
    } //-- void deleteCdFilialCnpj() 

    /**
     * Method deleteCdTipoConta
     * 
     */
    public void deleteCdTipoConta()
    {
        this._has_cdTipoConta= false;
    } //-- void deleteCdTipoConta() 

    /**
     * Method deleteNrOcorrencias
     * 
     */
    public void deleteNrOcorrencias()
    {
        this._has_nrOcorrencias= false;
    } //-- void deleteNrOcorrencias() 

    /**
     * Returns the value of field 'cdAgencia'.
     * 
     * @return int
     * @return the value of field 'cdAgencia'.
     */
    public int getCdAgencia()
    {
        return this._cdAgencia;
    } //-- int getCdAgencia() 

    /**
     * Returns the value of field 'cdBanco'.
     * 
     * @return int
     * @return the value of field 'cdBanco'.
     */
    public int getCdBanco()
    {
        return this._cdBanco;
    } //-- int getCdBanco() 

    /**
     * Returns the value of field 'cdConta'.
     * 
     * @return long
     * @return the value of field 'cdConta'.
     */
    public long getCdConta()
    {
        return this._cdConta;
    } //-- long getCdConta() 

    /**
     * Returns the value of field 'cdControleCpfCnpj'.
     * 
     * @return int
     * @return the value of field 'cdControleCpfCnpj'.
     */
    public int getCdControleCpfCnpj()
    {
        return this._cdControleCpfCnpj;
    } //-- int getCdControleCpfCnpj() 

    /**
     * Returns the value of field 'cdCorpoCpfCnpj'.
     * 
     * @return long
     * @return the value of field 'cdCorpoCpfCnpj'.
     */
    public long getCdCorpoCpfCnpj()
    {
        return this._cdCorpoCpfCnpj;
    } //-- long getCdCorpoCpfCnpj() 

    /**
     * Returns the value of field 'cdDigitoConta'.
     * 
     * @return String
     * @return the value of field 'cdDigitoConta'.
     */
    public java.lang.String getCdDigitoConta()
    {
        return this._cdDigitoConta;
    } //-- java.lang.String getCdDigitoConta() 

    /**
     * Returns the value of field 'cdFilialCnpj'.
     * 
     * @return int
     * @return the value of field 'cdFilialCnpj'.
     */
    public int getCdFilialCnpj()
    {
        return this._cdFilialCnpj;
    } //-- int getCdFilialCnpj() 

    /**
     * Returns the value of field 'cdTipoConta'.
     * 
     * @return int
     * @return the value of field 'cdTipoConta'.
     */
    public int getCdTipoConta()
    {
        return this._cdTipoConta;
    } //-- int getCdTipoConta() 

    /**
     * Returns the value of field 'dsNomeRazao'.
     * 
     * @return String
     * @return the value of field 'dsNomeRazao'.
     */
    public java.lang.String getDsNomeRazao()
    {
        return this._dsNomeRazao;
    } //-- java.lang.String getDsNomeRazao() 

    /**
     * Returns the value of field 'dtNascimentoFundacao'.
     * 
     * @return String
     * @return the value of field 'dtNascimentoFundacao'.
     */
    public java.lang.String getDtNascimentoFundacao()
    {
        return this._dtNascimentoFundacao;
    } //-- java.lang.String getDtNascimentoFundacao() 

    /**
     * Returns the value of field 'nrOcorrencias'.
     * 
     * @return int
     * @return the value of field 'nrOcorrencias'.
     */
    public int getNrOcorrencias()
    {
        return this._nrOcorrencias;
    } //-- int getNrOcorrencias() 

    /**
     * Method hasCdAgencia
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgencia()
    {
        return this._has_cdAgencia;
    } //-- boolean hasCdAgencia() 

    /**
     * Method hasCdBanco
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdBanco()
    {
        return this._has_cdBanco;
    } //-- boolean hasCdBanco() 

    /**
     * Method hasCdConta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdConta()
    {
        return this._has_cdConta;
    } //-- boolean hasCdConta() 

    /**
     * Method hasCdControleCpfCnpj
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdControleCpfCnpj()
    {
        return this._has_cdControleCpfCnpj;
    } //-- boolean hasCdControleCpfCnpj() 

    /**
     * Method hasCdCorpoCpfCnpj
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCorpoCpfCnpj()
    {
        return this._has_cdCorpoCpfCnpj;
    } //-- boolean hasCdCorpoCpfCnpj() 

    /**
     * Method hasCdFilialCnpj
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFilialCnpj()
    {
        return this._has_cdFilialCnpj;
    } //-- boolean hasCdFilialCnpj() 

    /**
     * Method hasCdTipoConta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoConta()
    {
        return this._has_cdTipoConta;
    } //-- boolean hasCdTipoConta() 

    /**
     * Method hasNrOcorrencias
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrOcorrencias()
    {
        return this._has_nrOcorrencias;
    } //-- boolean hasNrOcorrencias() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdAgencia'.
     * 
     * @param cdAgencia the value of field 'cdAgencia'.
     */
    public void setCdAgencia(int cdAgencia)
    {
        this._cdAgencia = cdAgencia;
        this._has_cdAgencia = true;
    } //-- void setCdAgencia(int) 

    /**
     * Sets the value of field 'cdBanco'.
     * 
     * @param cdBanco the value of field 'cdBanco'.
     */
    public void setCdBanco(int cdBanco)
    {
        this._cdBanco = cdBanco;
        this._has_cdBanco = true;
    } //-- void setCdBanco(int) 

    /**
     * Sets the value of field 'cdConta'.
     * 
     * @param cdConta the value of field 'cdConta'.
     */
    public void setCdConta(long cdConta)
    {
        this._cdConta = cdConta;
        this._has_cdConta = true;
    } //-- void setCdConta(long) 

    /**
     * Sets the value of field 'cdControleCpfCnpj'.
     * 
     * @param cdControleCpfCnpj the value of field
     * 'cdControleCpfCnpj'.
     */
    public void setCdControleCpfCnpj(int cdControleCpfCnpj)
    {
        this._cdControleCpfCnpj = cdControleCpfCnpj;
        this._has_cdControleCpfCnpj = true;
    } //-- void setCdControleCpfCnpj(int) 

    /**
     * Sets the value of field 'cdCorpoCpfCnpj'.
     * 
     * @param cdCorpoCpfCnpj the value of field 'cdCorpoCpfCnpj'.
     */
    public void setCdCorpoCpfCnpj(long cdCorpoCpfCnpj)
    {
        this._cdCorpoCpfCnpj = cdCorpoCpfCnpj;
        this._has_cdCorpoCpfCnpj = true;
    } //-- void setCdCorpoCpfCnpj(long) 

    /**
     * Sets the value of field 'cdDigitoConta'.
     * 
     * @param cdDigitoConta the value of field 'cdDigitoConta'.
     */
    public void setCdDigitoConta(java.lang.String cdDigitoConta)
    {
        this._cdDigitoConta = cdDigitoConta;
    } //-- void setCdDigitoConta(java.lang.String) 

    /**
     * Sets the value of field 'cdFilialCnpj'.
     * 
     * @param cdFilialCnpj the value of field 'cdFilialCnpj'.
     */
    public void setCdFilialCnpj(int cdFilialCnpj)
    {
        this._cdFilialCnpj = cdFilialCnpj;
        this._has_cdFilialCnpj = true;
    } //-- void setCdFilialCnpj(int) 

    /**
     * Sets the value of field 'cdTipoConta'.
     * 
     * @param cdTipoConta the value of field 'cdTipoConta'.
     */
    public void setCdTipoConta(int cdTipoConta)
    {
        this._cdTipoConta = cdTipoConta;
        this._has_cdTipoConta = true;
    } //-- void setCdTipoConta(int) 

    /**
     * Sets the value of field 'dsNomeRazao'.
     * 
     * @param dsNomeRazao the value of field 'dsNomeRazao'.
     */
    public void setDsNomeRazao(java.lang.String dsNomeRazao)
    {
        this._dsNomeRazao = dsNomeRazao;
    } //-- void setDsNomeRazao(java.lang.String) 

    /**
     * Sets the value of field 'dtNascimentoFundacao'.
     * 
     * @param dtNascimentoFundacao the value of field
     * 'dtNascimentoFundacao'.
     */
    public void setDtNascimentoFundacao(java.lang.String dtNascimentoFundacao)
    {
        this._dtNascimentoFundacao = dtNascimentoFundacao;
    } //-- void setDtNascimentoFundacao(java.lang.String) 

    /**
     * Sets the value of field 'nrOcorrencias'.
     * 
     * @param nrOcorrencias the value of field 'nrOcorrencias'.
     */
    public void setNrOcorrencias(int nrOcorrencias)
    {
        this._nrOcorrencias = nrOcorrencias;
        this._has_nrOcorrencias = true;
    } //-- void setNrOcorrencias(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ListarClubesClientesRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listarclubesclientes.request.ListarClubesClientesRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listarclubesclientes.request.ListarClubesClientesRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listarclubesclientes.request.ListarClubesClientesRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarclubesclientes.request.ListarClubesClientesRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
