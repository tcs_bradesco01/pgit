/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantervinculacaoconveniocontasalario.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantervinculacaoconveniocontasalario.bean;

/**
 * Nome: DetalharVincConvnCtaSalarioSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class DetalharVincConvnCtaSalarioSaidaDTO {

	/** Atributo codMensagem. */
	private String codMensagem;
	
	/** Atributo mensagem. */
	private String mensagem;
	
	/** Atributo cdConveCtaSalarial. */
	private Long cdConveCtaSalarial;
	
	/** Atributo dsConvCtaSalarial. */
	private String dsConvCtaSalarial;
	
	/** Atributo cdSitConvCta. */
	private Integer cdSitConvCta;
	
	/** Atributo cdBcoEmprConvn. */
	private Integer cdBcoEmprConvn;
	
	/** Atributo cdAgenciaEmprConvn. */
	private Integer cdAgenciaEmprConvn;
	
	/** Atributo cdCtaEmprConvn. */
	private Long cdCtaEmprConvn;
	
	/** Atributo cdDigitoEmprConvn. */
	private String cdDigitoEmprConvn;
	
	/** Atributo dtHoraInclusao. */
	private String dtHoraInclusao;
	
	/** Atributo cdUsuarioInclusao. */
	private String cdUsuarioInclusao;
	
	/** Atributo dtInclusao. */
	private String dtInclusao;
	
	/** Atributo hrInclusao. */
	private String hrInclusao;
	
	/** Atributo cdTipoCanalInclusao. */
	private Integer cdTipoCanalInclusao;
	
	/** Atributo dsTipoCanalInclusao. */
	private String dsTipoCanalInclusao;
	
	/** Atributo cdOperacaoFluxoInclusao. */
	private String cdOperacaoFluxoInclusao;
	
	/** Atributo dtManutencao. */
	private String dtManutencao;
	
	/** Atributo hrManutencao. */
	private String hrManutencao;
	
	/** Atributo cdUsuarioManutencao. */
	private String cdUsuarioManutencao;
    
    /** Atributo cdCanalManutencao. */
    private Integer cdCanalManutencao;
    
    /** Atributo dsTipoCanalManutencao. */
    private String dsTipoCanalManutencao;
    
    /** Atributo cdOperacaoFluxoManutencao. */
    private String cdOperacaoFluxoManutencao;

	/**
	 * Get: cdConveCtaSalarial.
	 *
	 * @return cdConveCtaSalarial
	 */
	public Long getCdConveCtaSalarial() {
		return cdConveCtaSalarial;
	}

	/**
	 * Set: cdConveCtaSalarial.
	 *
	 * @param cdConveCtaSalarial the cd conve cta salarial
	 */
	public void setCdConveCtaSalarial(Long cdConveCtaSalarial) {
		this.cdConveCtaSalarial = cdConveCtaSalarial;
	}

	/**
	 * Get: dsConvCtaSalarial.
	 *
	 * @return dsConvCtaSalarial
	 */
	public String getDsConvCtaSalarial() {
		return dsConvCtaSalarial;
	}

	/**
	 * Set: dsConvCtaSalarial.
	 *
	 * @param dsConvCtaSalarial the ds conv cta salarial
	 */
	public void setDsConvCtaSalarial(String dsConvCtaSalarial) {
		this.dsConvCtaSalarial = dsConvCtaSalarial;
	}

	/**
	 * Get: cdSitConvCta.
	 *
	 * @return cdSitConvCta
	 */
	public Integer getCdSitConvCta() {
		return cdSitConvCta;
	}

	/**
	 * Set: cdSitConvCta.
	 *
	 * @param cdSitConvCta the cd sit conv cta
	 */
	public void setCdSitConvCta(Integer cdSitConvCta) {
		this.cdSitConvCta = cdSitConvCta;
	}

	/**
	 * Get: cdBcoEmprConvn.
	 *
	 * @return cdBcoEmprConvn
	 */
	public Integer getCdBcoEmprConvn() {
		return cdBcoEmprConvn;
	}

	/**
	 * Set: cdBcoEmprConvn.
	 *
	 * @param cdBcoEmprConvn the cd bco empr convn
	 */
	public void setCdBcoEmprConvn(Integer cdBcoEmprConvn) {
		this.cdBcoEmprConvn = cdBcoEmprConvn;
	}

	/**
	 * Get: cdAgenciaEmprConvn.
	 *
	 * @return cdAgenciaEmprConvn
	 */
	public Integer getCdAgenciaEmprConvn() {
		return cdAgenciaEmprConvn;
	}

	/**
	 * Set: cdAgenciaEmprConvn.
	 *
	 * @param cdAgenciaEmprConvn the cd agencia empr convn
	 */
	public void setCdAgenciaEmprConvn(Integer cdAgenciaEmprConvn) {
		this.cdAgenciaEmprConvn = cdAgenciaEmprConvn;
	}

	/**
	 * Get: cdCtaEmprConvn.
	 *
	 * @return cdCtaEmprConvn
	 */
	public Long getCdCtaEmprConvn() {
		return cdCtaEmprConvn;
	}

	/**
	 * Set: cdCtaEmprConvn.
	 *
	 * @param cdCtaEmprConvn the cd cta empr convn
	 */
	public void setCdCtaEmprConvn(Long cdCtaEmprConvn) {
		this.cdCtaEmprConvn = cdCtaEmprConvn;
	}

	/**
	 * Get: cdDigitoEmprConvn.
	 *
	 * @return cdDigitoEmprConvn
	 */
	public String getCdDigitoEmprConvn() {
		return cdDigitoEmprConvn;
	}

	/**
	 * Set: cdDigitoEmprConvn.
	 *
	 * @param cdDigitoEmprConvn the cd digito empr convn
	 */
	public void setCdDigitoEmprConvn(String cdDigitoEmprConvn) {
		this.cdDigitoEmprConvn = cdDigitoEmprConvn;
	}

	/**
	 * Get: dtHoraInclusao.
	 *
	 * @return dtHoraInclusao
	 */
	public String getDtHoraInclusao() {
		return dtHoraInclusao;
	}

	/**
	 * Set: dtHoraInclusao.
	 *
	 * @param dtHoraInclusao the dt hora inclusao
	 */
	public void setDtHoraInclusao(String dtHoraInclusao) {
		this.dtHoraInclusao = dtHoraInclusao;
	}

	/**
	 * Get: cdUsuarioInclusao.
	 *
	 * @return cdUsuarioInclusao
	 */
	public String getCdUsuarioInclusao() {
		return cdUsuarioInclusao;
	}

	/**
	 * Set: cdUsuarioInclusao.
	 *
	 * @param cdUsuarioInclusao the cd usuario inclusao
	 */
	public void setCdUsuarioInclusao(String cdUsuarioInclusao) {
		this.cdUsuarioInclusao = cdUsuarioInclusao;
	}

	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}

	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}

	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}

	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	/**
	 * Get: dtInclusao.
	 *
	 * @return dtInclusao
	 */
	public String getDtInclusao() {
		return dtInclusao;
	}

	/**
	 * Set: dtInclusao.
	 *
	 * @param dtInclusao the dt inclusao
	 */
	public void setDtInclusao(String dtInclusao) {
		this.dtInclusao = dtInclusao;
	}

	/**
	 * Get: hrInclusao.
	 *
	 * @return hrInclusao
	 */
	public String getHrInclusao() {
		return hrInclusao;
	}

	/**
	 * Set: hrInclusao.
	 *
	 * @param hrInclusao the hr inclusao
	 */
	public void setHrInclusao(String hrInclusao) {
		this.hrInclusao = hrInclusao;
	}

	/**
	 * Get: cdTipoCanalInclusao.
	 *
	 * @return cdTipoCanalInclusao
	 */
	public Integer getCdTipoCanalInclusao() {
		return cdTipoCanalInclusao;
	}

	/**
	 * Set: cdTipoCanalInclusao.
	 *
	 * @param cdTipoCanalInclusao the cd tipo canal inclusao
	 */
	public void setCdTipoCanalInclusao(Integer cdTipoCanalInclusao) {
		this.cdTipoCanalInclusao = cdTipoCanalInclusao;
	}

	/**
	 * Get: dsTipoCanalInclusao.
	 *
	 * @return dsTipoCanalInclusao
	 */
	public String getDsTipoCanalInclusao() {
		return dsTipoCanalInclusao;
	}

	/**
	 * Set: dsTipoCanalInclusao.
	 *
	 * @param dsTipoCanalInclusao the ds tipo canal inclusao
	 */
	public void setDsTipoCanalInclusao(String dsTipoCanalInclusao) {
		this.dsTipoCanalInclusao = dsTipoCanalInclusao;
	}

	/**
	 * Get: cdOperacaoFluxoInclusao.
	 *
	 * @return cdOperacaoFluxoInclusao
	 */
	public String getCdOperacaoFluxoInclusao() {
		return cdOperacaoFluxoInclusao;
	}

	/**
	 * Set: cdOperacaoFluxoInclusao.
	 *
	 * @param cdOperacaoFluxoInclusao the cd operacao fluxo inclusao
	 */
	public void setCdOperacaoFluxoInclusao(String cdOperacaoFluxoInclusao) {
		this.cdOperacaoFluxoInclusao = cdOperacaoFluxoInclusao;
	}

	/**
	 * Get: dtManutencao.
	 *
	 * @return dtManutencao
	 */
	public String getDtManutencao() {
		return dtManutencao;
	}

	/**
	 * Set: dtManutencao.
	 *
	 * @param dtManutencao the dt manutencao
	 */
	public void setDtManutencao(String dtManutencao) {
		this.dtManutencao = dtManutencao;
	}

	/**
	 * Get: hrManutencao.
	 *
	 * @return hrManutencao
	 */
	public String getHrManutencao() {
		return hrManutencao;
	}

	/**
	 * Set: hrManutencao.
	 *
	 * @param hrManutencao the hr manutencao
	 */
	public void setHrManutencao(String hrManutencao) {
		this.hrManutencao = hrManutencao;
	}

	/**
	 * Get: cdUsuarioManutencao.
	 *
	 * @return cdUsuarioManutencao
	 */
	public String getCdUsuarioManutencao() {
		return cdUsuarioManutencao;
	}

	/**
	 * Set: cdUsuarioManutencao.
	 *
	 * @param cdUsuarioManutencao the cd usuario manutencao
	 */
	public void setCdUsuarioManutencao(String cdUsuarioManutencao) {
		this.cdUsuarioManutencao = cdUsuarioManutencao;
	}

	/**
	 * Get: cdCanalManutencao.
	 *
	 * @return cdCanalManutencao
	 */
	public Integer getCdCanalManutencao() {
		return cdCanalManutencao;
	}

	/**
	 * Set: cdCanalManutencao.
	 *
	 * @param cdCanalManutencao the cd canal manutencao
	 */
	public void setCdCanalManutencao(Integer cdCanalManutencao) {
		this.cdCanalManutencao = cdCanalManutencao;
	}

	/**
	 * Get: dsTipoCanalManutencao.
	 *
	 * @return dsTipoCanalManutencao
	 */
	public String getDsTipoCanalManutencao() {
		return dsTipoCanalManutencao;
	}

	/**
	 * Set: dsTipoCanalManutencao.
	 *
	 * @param dsTipoCanalManutencao the ds tipo canal manutencao
	 */
	public void setDsTipoCanalManutencao(String dsTipoCanalManutencao) {
		this.dsTipoCanalManutencao = dsTipoCanalManutencao;
	}

	/**
	 * Get: cdOperacaoFluxoManutencao.
	 *
	 * @return cdOperacaoFluxoManutencao
	 */
	public String getCdOperacaoFluxoManutencao() {
		return cdOperacaoFluxoManutencao;
	}

	/**
	 * Set: cdOperacaoFluxoManutencao.
	 *
	 * @param cdOperacaoFluxoManutencao the cd operacao fluxo manutencao
	 */
	public void setCdOperacaoFluxoManutencao(String cdOperacaoFluxoManutencao) {
		this.cdOperacaoFluxoManutencao = cdOperacaoFluxoManutencao;
	}

}