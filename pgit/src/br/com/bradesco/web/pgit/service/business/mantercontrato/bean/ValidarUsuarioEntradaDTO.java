package br.com.bradesco.web.pgit.service.business.mantercontrato.bean;

public class ValidarUsuarioEntradaDTO {
	
    private Integer maxOcorrencias;

	public Integer getMaxOcorrencias() {
		return maxOcorrencias;
	}

	public void setMaxOcorrencias(Integer maxOcorrencias) {
		this.maxOcorrencias = maxOcorrencias;
	}

}
