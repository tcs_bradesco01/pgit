/*
 * Nome: br.com.bradesco.web.pgit.service.business.tipoacoescontroladas.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.tipoacoescontroladas.bean;

/**
 * Nome: TipoAcoesControladasDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class TipoAcoesControladasDTO {
	
	/** Atributo linhaSelecionada. */
	private int linhaSelecionada;
	
	/** Atributo codigo. */
	private String codigo;
	
	/** Atributo descricao. */
	private String descricao;
	
	/**
	 * Get: codigo.
	 *
	 * @return codigo
	 */
	public String getCodigo() {
		return codigo;
	}
	
	/**
	 * Set: codigo.
	 *
	 * @param codigo the codigo
	 */
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	
	/**
	 * Get: descricao.
	 *
	 * @return descricao
	 */
	public String getDescricao() {
		return descricao;
	}
	
	/**
	 * Set: descricao.
	 *
	 * @param descricao the descricao
	 */
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	/**
	 * Get: linhaSelecionada.
	 *
	 * @return linhaSelecionada
	 */
	public int getLinhaSelecionada() {
		return linhaSelecionada;
	}
	
	/**
	 * Set: linhaSelecionada.
	 *
	 * @param linhaSelecionada the linha selecionada
	 */
	public void setLinhaSelecionada(int linhaSelecionada) {
		this.linhaSelecionada = linhaSelecionada;
	}
	
	
}
