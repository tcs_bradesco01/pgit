package br.com.bradesco.web.pgit.service.business.anteciparpostergarpagtosagecon.bean;

import java.math.BigDecimal;

// TODO: Auto-generated Javadoc
/**
 * Arquivo criado em 23/05/17.
 *
 * @author : todo!
 * @version :
 */
public class ListarPostergarConsolidadoSolicitacaoOcorrenciasSaidaDTO {

    /** The cd controle pagamento. */
    private String cdControlePagamento;

    /** The vl agendamento pagamento. */
    private BigDecimal vlAgendamentoPagamento;

    /** The complemento recebedor. */
    private String complementoRecebedor;

    /** The cd tipo cta recebedor. */
    private Integer cdTipoCtaRecebedor;

    /** The cd banco recebedor. */
    private Integer cdBancoRecebedor;

    /** The cd agencia recebedor. */
    private Integer cdAgenciaRecebedor;

    /** The cd digito agencia recebedor. */
    private String cdDigitoAgenciaRecebedor;

    /** The cd conta recebedor. */
    private Long cdContaRecebedor;

    /** The cd digito conta recebedor. */
    private String cdDigitoContaRecebedor;
    
    /**
     * Get: bcoAgContaFormatada.
     *
     * @return bcoAgContaFormatada
     */
    public String getBcoAgContaFormatada() {
        if(getCdContaRecebedor () != 0){
            return getCdBancoRecebedor() + " / " + getCdAgenciaRecebedor() + " - " + getCdDigitoAgenciaRecebedor() + " / "
            + getCdContaRecebedor() + " - " + getCdDigitoContaRecebedor();
        }
        
        if(getCdAgenciaRecebedor() != 0 && getCdContaRecebedor() == 0 ){
            return getCdBancoRecebedor() + " / " + getCdAgenciaRecebedor() + " - " + getCdDigitoAgenciaRecebedor();
        }
        
        return "";
    }

    /**
     * Set cd controle pagamento.
     *
     * @param cdControlePagamento the cd controle pagamento
     */
    public void setCdControlePagamento(String cdControlePagamento) {
        this.cdControlePagamento = cdControlePagamento;
    }

    /**
     * Get cd controle pagamento.
     *
     * @return the cd controle pagamento
     */
    public String getCdControlePagamento() {
        return this.cdControlePagamento;
    }

    /**
     * Set vl agendamento pagamento.
     *
     * @param vlAgendamentoPagamento the vl agendamento pagamento
     */
    public void setVlAgendamentoPagamento(BigDecimal vlAgendamentoPagamento) {
        this.vlAgendamentoPagamento = vlAgendamentoPagamento;
    }

    /**
     * Get vl agendamento pagamento.
     *
     * @return the vl agendamento pagamento
     */
    public BigDecimal getVlAgendamentoPagamento() {
        return this.vlAgendamentoPagamento;
    }

    /**
     * Set complemento recebedor.
     *
     * @param complementoRecebedor the complemento recebedor
     */
    public void setComplementoRecebedor(String complementoRecebedor) {
        this.complementoRecebedor = complementoRecebedor;
    }

    /**
     * Get complemento recebedor.
     *
     * @return the complemento recebedor
     */
    public String getComplementoRecebedor() {
        return this.complementoRecebedor;
    }

    /**
     * Set cd tipo cta recebedor.
     *
     * @param cdTipoCtaRecebedor the cd tipo cta recebedor
     */
    public void setCdTipoCtaRecebedor(Integer cdTipoCtaRecebedor) {
        this.cdTipoCtaRecebedor = cdTipoCtaRecebedor;
    }

    /**
     * Get cd tipo cta recebedor.
     *
     * @return the cd tipo cta recebedor
     */
    public Integer getCdTipoCtaRecebedor() {
        return this.cdTipoCtaRecebedor;
    }

    /**
     * Set cd banco recebedor.
     *
     * @param cdBancoRecebedor the cd banco recebedor
     */
    public void setCdBancoRecebedor(Integer cdBancoRecebedor) {
        this.cdBancoRecebedor = cdBancoRecebedor;
    }

    /**
     * Get cd banco recebedor.
     *
     * @return the cd banco recebedor
     */
    public Integer getCdBancoRecebedor() {
        return this.cdBancoRecebedor;
    }

    /**
     * Set cd agencia recebedor.
     *
     * @param cdAgenciaRecebedor the cd agencia recebedor
     */
    public void setCdAgenciaRecebedor(Integer cdAgenciaRecebedor) {
        this.cdAgenciaRecebedor = cdAgenciaRecebedor;
    }

    /**
     * Get cd agencia recebedor.
     *
     * @return the cd agencia recebedor
     */
    public Integer getCdAgenciaRecebedor() {
        return this.cdAgenciaRecebedor;
    }

    /**
     * Set cd digito agencia recebedor.
     *
     * @param cdDigitoAgenciaRecebedor the cd digito agencia recebedor
     */
    public void setCdDigitoAgenciaRecebedor(String cdDigitoAgenciaRecebedor) {
        this.cdDigitoAgenciaRecebedor = cdDigitoAgenciaRecebedor;
    }

    /**
     * Get cd digito agencia recebedor.
     *
     * @return the cd digito agencia recebedor
     */
    public String getCdDigitoAgenciaRecebedor() {
        return this.cdDigitoAgenciaRecebedor;
    }

    /**
     * Set cd conta recebedor.
     *
     * @param cdContaRecebedor the cd conta recebedor
     */
    public void setCdContaRecebedor(Long cdContaRecebedor) {
        this.cdContaRecebedor = cdContaRecebedor;
    }

    /**
     * Get cd conta recebedor.
     *
     * @return the cd conta recebedor
     */
    public Long getCdContaRecebedor() {
        return this.cdContaRecebedor;
    }

    /**
     * Set cd digito conta recebedor.
     *
     * @param cdDigitoContaRecebedor the cd digito conta recebedor
     */
    public void setCdDigitoContaRecebedor(String cdDigitoContaRecebedor) {
        this.cdDigitoContaRecebedor = cdDigitoContaRecebedor;
    }

    /**
     * Get cd digito conta recebedor.
     *
     * @return the cd digito conta recebedor
     */
    public String getCdDigitoContaRecebedor() {
        return this.cdDigitoContaRecebedor;
    }
}