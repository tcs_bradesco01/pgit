/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.conrelacaoconmanutencoes.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class OcorrenciasParticipante.
 * 
 * @version $Revision$ $Date$
 */
public class OcorrenciasParticipante implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _nrCpfCnpjParticipante
     */
    private java.lang.String _nrCpfCnpjParticipante;

    /**
     * Field _nmRazaoSocialParticipante
     */
    private java.lang.String _nmRazaoSocialParticipante;

    /**
     * Field _dsTipoParticipacaoPessoa
     */
    private java.lang.String _dsTipoParticipacaoPessoa;

    /**
     * Field _dsAcaoManutencaoParticipante
     */
    private java.lang.String _dsAcaoManutencaoParticipante;


      //----------------/
     //- Constructors -/
    //----------------/

    public OcorrenciasParticipante() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.conrelacaoconmanutencoes.response.OcorrenciasParticipante()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Returns the value of field 'dsAcaoManutencaoParticipante'.
     * 
     * @return String
     * @return the value of field 'dsAcaoManutencaoParticipante'.
     */
    public java.lang.String getDsAcaoManutencaoParticipante()
    {
        return this._dsAcaoManutencaoParticipante;
    } //-- java.lang.String getDsAcaoManutencaoParticipante() 

    /**
     * Returns the value of field 'dsTipoParticipacaoPessoa'.
     * 
     * @return String
     * @return the value of field 'dsTipoParticipacaoPessoa'.
     */
    public java.lang.String getDsTipoParticipacaoPessoa()
    {
        return this._dsTipoParticipacaoPessoa;
    } //-- java.lang.String getDsTipoParticipacaoPessoa() 

    /**
     * Returns the value of field 'nmRazaoSocialParticipante'.
     * 
     * @return String
     * @return the value of field 'nmRazaoSocialParticipante'.
     */
    public java.lang.String getNmRazaoSocialParticipante()
    {
        return this._nmRazaoSocialParticipante;
    } //-- java.lang.String getNmRazaoSocialParticipante() 

    /**
     * Returns the value of field 'nrCpfCnpjParticipante'.
     * 
     * @return String
     * @return the value of field 'nrCpfCnpjParticipante'.
     */
    public java.lang.String getNrCpfCnpjParticipante()
    {
        return this._nrCpfCnpjParticipante;
    } //-- java.lang.String getNrCpfCnpjParticipante() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'dsAcaoManutencaoParticipante'.
     * 
     * @param dsAcaoManutencaoParticipante the value of field
     * 'dsAcaoManutencaoParticipante'.
     */
    public void setDsAcaoManutencaoParticipante(java.lang.String dsAcaoManutencaoParticipante)
    {
        this._dsAcaoManutencaoParticipante = dsAcaoManutencaoParticipante;
    } //-- void setDsAcaoManutencaoParticipante(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoParticipacaoPessoa'.
     * 
     * @param dsTipoParticipacaoPessoa the value of field
     * 'dsTipoParticipacaoPessoa'.
     */
    public void setDsTipoParticipacaoPessoa(java.lang.String dsTipoParticipacaoPessoa)
    {
        this._dsTipoParticipacaoPessoa = dsTipoParticipacaoPessoa;
    } //-- void setDsTipoParticipacaoPessoa(java.lang.String) 

    /**
     * Sets the value of field 'nmRazaoSocialParticipante'.
     * 
     * @param nmRazaoSocialParticipante the value of field
     * 'nmRazaoSocialParticipante'.
     */
    public void setNmRazaoSocialParticipante(java.lang.String nmRazaoSocialParticipante)
    {
        this._nmRazaoSocialParticipante = nmRazaoSocialParticipante;
    } //-- void setNmRazaoSocialParticipante(java.lang.String) 

    /**
     * Sets the value of field 'nrCpfCnpjParticipante'.
     * 
     * @param nrCpfCnpjParticipante the value of field
     * 'nrCpfCnpjParticipante'.
     */
    public void setNrCpfCnpjParticipante(java.lang.String nrCpfCnpjParticipante)
    {
        this._nrCpfCnpjParticipante = nrCpfCnpjParticipante;
    } //-- void setNrCpfCnpjParticipante(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return OcorrenciasParticipante
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.conrelacaoconmanutencoes.response.OcorrenciasParticipante unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.conrelacaoconmanutencoes.response.OcorrenciasParticipante) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.conrelacaoconmanutencoes.response.OcorrenciasParticipante.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.conrelacaoconmanutencoes.response.OcorrenciasParticipante unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
