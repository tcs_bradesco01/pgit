/*
 * Nome: br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean;

import java.math.BigDecimal;

/**
 * Nome: DetalharManPagtoIndCreditoContaSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class DetalharManPagtoIndCreditoContaSaidaDTO {
	
	
	/** Atributo codMensagem. */
	private String codMensagem;
    
    /** Atributo mensagem. */
    private String mensagem;
    
    /** Atributo dtNascimentoBeneficiario. */
    private String dtNascimentoBeneficiario;
    
    /** Atributo vlDesconto. */
    private BigDecimal vlDesconto;
    
    /** Atributo vlAbatimento. */
    private BigDecimal vlAbatimento;
    
    /** Atributo vlMulta. */
    private BigDecimal vlMulta;
    
    /** Atributo vlMora. */
    private BigDecimal vlMora;
    
    /** Atributo vlDeducao. */
    private BigDecimal vlDeducao;
    
    /** Atributo vlAcrescimo. */
    private BigDecimal vlAcrescimo;
    
    /** Atributo vlImpostoRenda. */
    private BigDecimal vlImpostoRenda;
    
    /** Atributo vlIss. */
    private BigDecimal vlIss;
    
    /** Atributo vlIof. */
    private BigDecimal vlIof;
    
    /** Atributo vlInss. */
    private BigDecimal vlInss;
    
    /** Atributo cdBancoDestino. */
    private Integer cdBancoDestino;
    
    /** Atributo cdAgenciaDestino. */
    private Integer cdAgenciaDestino;
    
    /** Atributo cdDigitoAgenciaDestino. */
    private Integer cdDigitoAgenciaDestino;
    
    /** Atributo cdContaDestino. */
    private Long cdContaDestino;
    
    /** Atributo cdDigitoContaDestino. */
    private String cdDigitoContaDestino;
    
    /** Atributo dsBancoDestino. */
    private String dsBancoDestino;
    
    /** Atributo dsAgenciaDestino. */
    private String dsAgenciaDestino;
    
    /** Atributo dsTipoContaDestino. */
    private String dsTipoContaDestino;
    
    /** Atributo cdIndicadorModalidade. */
    private Integer cdIndicadorModalidade; //campoOculto
    
    /** Atributo dsPagamento. */
    private String dsPagamento;
    
    /** Atributo dsBancoDebito. */
    private String dsBancoDebito;
    
    /** Atributo dsAgenciaDebito. */
    private String dsAgenciaDebito;
    
    /** Atributo dsTipoContaDebito. */
    private String dsTipoContaDebito;
    
    /** Atributo dsContrato. */
    private String dsContrato;
    
    /** Atributo cdSituacaoContrato. */
    private String cdSituacaoContrato;
    
    /** Atributo dtAgendamento. */
    private String dtAgendamento;
    
    /** Atributo vlrAgendamento. */
    private BigDecimal vlrAgendamento;
    
    /** Atributo vlrEfetivacao. */
    private BigDecimal vlrEfetivacao;
    
    /** Atributo cdIndicadorEconomicoMoeda. */
    private Integer cdIndicadorEconomicoMoeda;
    
    /** Atributo qtMoeda. */
    private BigDecimal qtMoeda;
    
    /** Atributo dtVencimento. */
    private String dtVencimento;
    
    /** Atributo dsMensagemPrimeiraLinha. */
    private String dsMensagemPrimeiraLinha;
    
    /** Atributo dsMensagemSegundaLinha. */
    private String dsMensagemSegundaLinha;
    
    /** Atributo dsUsoEmpresa. */
    private String dsUsoEmpresa;
    
    /** Atributo cdSituacaoOperacaoPagamento. */
    private Integer cdSituacaoOperacaoPagamento;
    
    /** Atributo cdMotivoSituacaoPagamento. */
    private Integer cdMotivoSituacaoPagamento; //campoOculto
    
    /** Atributo cdListaDebito. */
    private String cdListaDebito;
    
    /** Atributo cdTipoInscricaoPagador. */
    private Integer cdTipoInscricaoPagador;
    
    /** Atributo nrDocumento. */
    private String nrDocumento;
    
    /** Atributo cdSerieDocumento. */
    private String cdSerieDocumento;
    
    /** Atributo vlDocumento. */
    private BigDecimal vlDocumento;
    
    /** Atributo dtEmissaoDocumento. */
    private String dtEmissaoDocumento;
    
    /** Atributo nrSequenciaArquivoRemessa. */
    private String nrSequenciaArquivoRemessa;
    
    /** Atributo nrLoteArquivoRemessa. */
    private String nrLoteArquivoRemessa;
    
    /** Atributo cdFavorecido. */
    private String cdFavorecido;
    
    /** Atributo dsBancoFavorecido. */
    private String dsBancoFavorecido;
    
    /** Atributo dsAgenciaFavorecido. */
    private String dsAgenciaFavorecido;
    
    /** Atributo cdTipoContaFavorecido. */
    private Integer cdTipoContaFavorecido; //campoOculto
    
    /** Atributo dsTipoContaFavorecido. */
    private String dsTipoContaFavorecido;
    
    /** Atributo dsMotivoSituacao. */
    private String dsMotivoSituacao;
    
    /** Atributo cdTipoManutencao. */
    private Integer cdTipoManutencao; //campoOculto
    
    /** Atributo dsTipoManutencao. */
    private String dsTipoManutencao;
    
    /** Atributo dtInclusao. */
    private String dtInclusao;
    
    /** Atributo hrInclusao. */
    private String hrInclusao;
    
    /** Atributo cdUsuarioInclusaoInterno. */
    private String cdUsuarioInclusaoInterno;
    
    /** Atributo cdUsuarioInclusaoExterno. */
    private String cdUsuarioInclusaoExterno;
    
    /** Atributo cdTipoCanalInclusao. */
    private Integer cdTipoCanalInclusao;
    
    /** Atributo dsTipoCanalInclusao. */
    private String dsTipoCanalInclusao;
    
    /** Atributo cdFluxoInclusao. */
    private String cdFluxoInclusao;
    
    /** Atributo dtManutencao. */
    private String dtManutencao;
    
    /** Atributo hrManutencao. */
    private String hrManutencao;
    
    /** Atributo cdUsuarioManutencaoInterno. */
    private String cdUsuarioManutencaoInterno;
    
    /** Atributo cdUsuarioManutencaoExterno. */
    private String cdUsuarioManutencaoExterno;
    
    /** Atributo cdTipoCanalManutencao. */
    private Integer cdTipoCanalManutencao;
    
    /** Atributo dsTipoCanalManutencao. */
    private String dsTipoCanalManutencao;
    
    /** Atributo cdFluxoManutencao. */
    private String cdFluxoManutencao;
    
    /** Atributo cdTipoDocumento. */
    private int cdTipoDocumento;
    
    /** Atributo dsTipoDocumento. */
    private String dsTipoDocumento;
    
    /** Atributo cdSituacaoTranferenciaAutomatica. */
    private String cdSituacaoTranferenciaAutomatica;
    
    /** Atributo cdStatusTransmissao. */
    private Integer cdStatusTransmissao;
	
	/** Atributo dsStatusTransmissao. */
	private String dsStatusTransmissao;
	
	/** Atributo cdBancoFinal. */
	private Integer cdBancoFinal;
	
	/** Atributo cdAgenciaFinal. */
	private Integer cdAgenciaFinal;
	
	/** Atributo cdDigitoAgenciaFinal. */
	private Integer cdDigitoAgenciaFinal;
	
	/** Atributo cdContaFinal. */
	private Long cdContaFinal;
	
	/** Atributo cdDigitoContaFinal. */
	private String cdDigitoContaFinal;
	
	/** Atributo dtHoraTransmissao. */
	private String dtHoraTransmissao;
	
	/** Atributo dtHoraDevolucao. */
	private String dtHoraDevolucao;
	
	/** Atributo vlEmpresa. */
	private BigDecimal vlEmpresa;
	
	/** Atributo vlLeasing. */
	private BigDecimal vlLeasing;
	
	/** Atributo vlDebitoAut. */
	private BigDecimal vlDebitoAut;
	
	/** Atributo dsMoeda. */
	private String dsMoeda;
	
	/** Atributo bancoDestinoFormatado. */
	private String bancoDestinoFormatado;
	
	/** Atributo agenciaDestinoFormatada. */
	private String agenciaDestinoFormatada;
	
	/** Atributo contaDestinoFormatada. */
	private String contaDestinoFormatada;
	
	/** Atributo dataHoraInclusaoFormatada. */
	private String dataHoraInclusaoFormatada;
	
	/** Atributo usuarioInclusaoFormatado. */
	private String usuarioInclusaoFormatado;
	
	/** Atributo tipoCanalInclusaoFormatado. */
	private String tipoCanalInclusaoFormatado;
	
	/** Atributo complementoInclusaoFormatado. */
	private String complementoInclusaoFormatado;
	
	/** Atributo dataHoraManutencaoFormatada. */
	private String dataHoraManutencaoFormatada;
	
	/** Atributo usuarioManutencaoFormatado. */
	private String usuarioManutencaoFormatado;
	
	/** Atributo tipoCanalManutencaoFormatado. */
	private String tipoCanalManutencaoFormatado;
	
	/** Atributo complementoManutencaoFormatado. */
	private String complementoManutencaoFormatado;
	
	
    
    /**
     * Get: agenciaDestinoFormatada.
     *
     * @return agenciaDestinoFormatada
     */
    public String getAgenciaDestinoFormatada() {
		return agenciaDestinoFormatada;
	}
	
	/**
	 * Set: agenciaDestinoFormatada.
	 *
	 * @param agenciaDestinoFormatada the agencia destino formatada
	 */
	public void setAgenciaDestinoFormatada(String agenciaDestinoFormatada) {
		this.agenciaDestinoFormatada = agenciaDestinoFormatada;
	}
	
	/**
	 * Get: bancoDestinoFormatado.
	 *
	 * @return bancoDestinoFormatado
	 */
	public String getBancoDestinoFormatado() {
		return bancoDestinoFormatado;
	}
	
	/**
	 * Set: bancoDestinoFormatado.
	 *
	 * @param bancoDestinoFormatado the banco destino formatado
	 */
	public void setBancoDestinoFormatado(String bancoDestinoFormatado) {
		this.bancoDestinoFormatado = bancoDestinoFormatado;
	}
	
	/**
	 * Get: contaDestinoFormatada.
	 *
	 * @return contaDestinoFormatada
	 */
	public String getContaDestinoFormatada() {
		return contaDestinoFormatada;
	}
	
	/**
	 * Set: contaDestinoFormatada.
	 *
	 * @param contaDestinoFormatada the conta destino formatada
	 */
	public void setContaDestinoFormatada(String contaDestinoFormatada) {
		this.contaDestinoFormatada = contaDestinoFormatada;
	}
	
	/**
	 * Get: cdAgenciaDestino.
	 *
	 * @return cdAgenciaDestino
	 */
	public Integer getCdAgenciaDestino() {
		return cdAgenciaDestino;
	}
	
	/**
	 * Set: cdAgenciaDestino.
	 *
	 * @param cdAgenciaDestino the cd agencia destino
	 */
	public void setCdAgenciaDestino(Integer cdAgenciaDestino) {
		this.cdAgenciaDestino = cdAgenciaDestino;
	}
	
	/**
	 * Get: cdBancoDestino.
	 *
	 * @return cdBancoDestino
	 */
	public Integer getCdBancoDestino() {
		return cdBancoDestino;
	}
	
	/**
	 * Set: cdBancoDestino.
	 *
	 * @param cdBancoDestino the cd banco destino
	 */
	public void setCdBancoDestino(Integer cdBancoDestino) {
		this.cdBancoDestino = cdBancoDestino;
	}
	
	/**
	 * Get: cdContaDestino.
	 *
	 * @return cdContaDestino
	 */
	public Long getCdContaDestino() {
		return cdContaDestino;
	}
	
	/**
	 * Set: cdContaDestino.
	 *
	 * @param cdContaDestino the cd conta destino
	 */
	public void setCdContaDestino(Long cdContaDestino) {
		this.cdContaDestino = cdContaDestino;
	}
	
	/**
	 * Get: cdDigitoAgenciaDestino.
	 *
	 * @return cdDigitoAgenciaDestino
	 */
	public Integer getCdDigitoAgenciaDestino() {
		return cdDigitoAgenciaDestino;
	}
	
	/**
	 * Set: cdDigitoAgenciaDestino.
	 *
	 * @param cdDigitoAgenciaDestino the cd digito agencia destino
	 */
	public void setCdDigitoAgenciaDestino(Integer cdDigitoAgenciaDestino) {
		this.cdDigitoAgenciaDestino = cdDigitoAgenciaDestino;
	}
	
	/**
	 * Get: cdDigitoContaDestino.
	 *
	 * @return cdDigitoContaDestino
	 */
	public String getCdDigitoContaDestino() {
		return cdDigitoContaDestino;
	}
	
	/**
	 * Set: cdDigitoContaDestino.
	 *
	 * @param cdDigitoContaDestino the cd digito conta destino
	 */
	public void setCdDigitoContaDestino(String cdDigitoContaDestino) {
		this.cdDigitoContaDestino = cdDigitoContaDestino;
	}
	
	/**
	 * Get: cdFavorecido.
	 *
	 * @return cdFavorecido
	 */
	public String getCdFavorecido() {
		return cdFavorecido;
	}
	
	/**
	 * Set: cdFavorecido.
	 *
	 * @param cdFavorecido the cd favorecido
	 */
	public void setCdFavorecido(String cdFavorecido) {
		this.cdFavorecido = cdFavorecido;
	}
	
	/**
	 * Get: cdFluxoInclusao.
	 *
	 * @return cdFluxoInclusao
	 */
	public String getCdFluxoInclusao() {
		return cdFluxoInclusao;
	}
	
	/**
	 * Set: cdFluxoInclusao.
	 *
	 * @param cdFluxoInclusao the cd fluxo inclusao
	 */
	public void setCdFluxoInclusao(String cdFluxoInclusao) {
		this.cdFluxoInclusao = cdFluxoInclusao;
	}
	
	/**
	 * Get: cdFluxoManutencao.
	 *
	 * @return cdFluxoManutencao
	 */
	public String getCdFluxoManutencao() {
		return cdFluxoManutencao;
	}
	
	/**
	 * Set: cdFluxoManutencao.
	 *
	 * @param cdFluxoManutencao the cd fluxo manutencao
	 */
	public void setCdFluxoManutencao(String cdFluxoManutencao) {
		this.cdFluxoManutencao = cdFluxoManutencao;
	}
	
	/**
	 * Get: cdIndicadorEconomicoMoeda.
	 *
	 * @return cdIndicadorEconomicoMoeda
	 */
	public Integer getCdIndicadorEconomicoMoeda() {
		return cdIndicadorEconomicoMoeda;
	}
	
	/**
	 * Set: cdIndicadorEconomicoMoeda.
	 *
	 * @param cdIndicadorEconomicoMoeda the cd indicador economico moeda
	 */
	public void setCdIndicadorEconomicoMoeda(Integer cdIndicadorEconomicoMoeda) {
		this.cdIndicadorEconomicoMoeda = cdIndicadorEconomicoMoeda;
	}
	
	/**
	 * Get: cdIndicadorModalidade.
	 *
	 * @return cdIndicadorModalidade
	 */
	public Integer getCdIndicadorModalidade() {
		return cdIndicadorModalidade;
	}
	
	/**
	 * Set: cdIndicadorModalidade.
	 *
	 * @param cdIndicadorModalidade the cd indicador modalidade
	 */
	public void setCdIndicadorModalidade(Integer cdIndicadorModalidade) {
		this.cdIndicadorModalidade = cdIndicadorModalidade;
	}
	
	/**
	 * Get: cdListaDebito.
	 *
	 * @return cdListaDebito
	 */
	public String getCdListaDebito() {
		return cdListaDebito;
	}
	
	/**
	 * Set: cdListaDebito.
	 *
	 * @param cdListaDebito the cd lista debito
	 */
	public void setCdListaDebito(String cdListaDebito) {
		this.cdListaDebito = cdListaDebito;
	}
	
	/**
	 * Get: cdMotivoSituacaoPagamento.
	 *
	 * @return cdMotivoSituacaoPagamento
	 */
	public Integer getCdMotivoSituacaoPagamento() {
		return cdMotivoSituacaoPagamento;
	}
	
	/**
	 * Set: cdMotivoSituacaoPagamento.
	 *
	 * @param cdMotivoSituacaoPagamento the cd motivo situacao pagamento
	 */
	public void setCdMotivoSituacaoPagamento(Integer cdMotivoSituacaoPagamento) {
		this.cdMotivoSituacaoPagamento = cdMotivoSituacaoPagamento;
	}
	
	/**
	 * Get: cdSerieDocumento.
	 *
	 * @return cdSerieDocumento
	 */
	public String getCdSerieDocumento() {
		return cdSerieDocumento;
	}
	
	/**
	 * Set: cdSerieDocumento.
	 *
	 * @param cdSerieDocumento the cd serie documento
	 */
	public void setCdSerieDocumento(String cdSerieDocumento) {
		this.cdSerieDocumento = cdSerieDocumento;
	}
	
	/**
	 * Get: cdSituacaoContrato.
	 *
	 * @return cdSituacaoContrato
	 */
	public String getCdSituacaoContrato() {
		return cdSituacaoContrato;
	}
	
	/**
	 * Set: cdSituacaoContrato.
	 *
	 * @param cdSituacaoContrato the cd situacao contrato
	 */
	public void setCdSituacaoContrato(String cdSituacaoContrato) {
		this.cdSituacaoContrato = cdSituacaoContrato;
	}
	
	/**
	 * Get: cdSituacaoOperacaoPagamento.
	 *
	 * @return cdSituacaoOperacaoPagamento
	 */
	public Integer getCdSituacaoOperacaoPagamento() {
		return cdSituacaoOperacaoPagamento;
	}
	
	/**
	 * Set: cdSituacaoOperacaoPagamento.
	 *
	 * @param cdSituacaoOperacaoPagamento the cd situacao operacao pagamento
	 */
	public void setCdSituacaoOperacaoPagamento(Integer cdSituacaoOperacaoPagamento) {
		this.cdSituacaoOperacaoPagamento = cdSituacaoOperacaoPagamento;
	}
	
	/**
	 * Get: cdTipoCanalInclusao.
	 *
	 * @return cdTipoCanalInclusao
	 */
	public Integer getCdTipoCanalInclusao() {
		return cdTipoCanalInclusao;
	}
	
	/**
	 * Set: cdTipoCanalInclusao.
	 *
	 * @param cdTipoCanalInclusao the cd tipo canal inclusao
	 */
	public void setCdTipoCanalInclusao(Integer cdTipoCanalInclusao) {
		this.cdTipoCanalInclusao = cdTipoCanalInclusao;
	}
	
	/**
	 * Get: cdTipoCanalManutencao.
	 *
	 * @return cdTipoCanalManutencao
	 */
	public Integer getCdTipoCanalManutencao() {
		return cdTipoCanalManutencao;
	}
	
	/**
	 * Set: cdTipoCanalManutencao.
	 *
	 * @param cdTipoCanalManutencao the cd tipo canal manutencao
	 */
	public void setCdTipoCanalManutencao(Integer cdTipoCanalManutencao) {
		this.cdTipoCanalManutencao = cdTipoCanalManutencao;
	}
	
	/**
	 * Get: cdTipoContaFavorecido.
	 *
	 * @return cdTipoContaFavorecido
	 */
	public Integer getCdTipoContaFavorecido() {
		return cdTipoContaFavorecido;
	}
	
	/**
	 * Set: cdTipoContaFavorecido.
	 *
	 * @param cdTipoContaFavorecido the cd tipo conta favorecido
	 */
	public void setCdTipoContaFavorecido(Integer cdTipoContaFavorecido) {
		this.cdTipoContaFavorecido = cdTipoContaFavorecido;
	}
	
	/**
	 * Get: cdTipoInscricaoPagador.
	 *
	 * @return cdTipoInscricaoPagador
	 */
	public Integer getCdTipoInscricaoPagador() {
		return cdTipoInscricaoPagador;
	}
	
	/**
	 * Set: cdTipoInscricaoPagador.
	 *
	 * @param cdTipoInscricaoPagador the cd tipo inscricao pagador
	 */
	public void setCdTipoInscricaoPagador(Integer cdTipoInscricaoPagador) {
		this.cdTipoInscricaoPagador = cdTipoInscricaoPagador;
	}
	
	/**
	 * Get: cdTipoManutencao.
	 *
	 * @return cdTipoManutencao
	 */
	public Integer getCdTipoManutencao() {
		return cdTipoManutencao;
	}
	
	/**
	 * Set: cdTipoManutencao.
	 *
	 * @param cdTipoManutencao the cd tipo manutencao
	 */
	public void setCdTipoManutencao(Integer cdTipoManutencao) {
		this.cdTipoManutencao = cdTipoManutencao;
	}
	
	/**
	 * Get: cdUsuarioInclusaoExterno.
	 *
	 * @return cdUsuarioInclusaoExterno
	 */
	public String getCdUsuarioInclusaoExterno() {
		return cdUsuarioInclusaoExterno;
	}
	
	/**
	 * Set: cdUsuarioInclusaoExterno.
	 *
	 * @param cdUsuarioInclusaoExterno the cd usuario inclusao externo
	 */
	public void setCdUsuarioInclusaoExterno(String cdUsuarioInclusaoExterno) {
		this.cdUsuarioInclusaoExterno = cdUsuarioInclusaoExterno;
	}
	
	/**
	 * Get: cdUsuarioInclusaoInterno.
	 *
	 * @return cdUsuarioInclusaoInterno
	 */
	public String getCdUsuarioInclusaoInterno() {
		return cdUsuarioInclusaoInterno;
	}
	
	/**
	 * Set: cdUsuarioInclusaoInterno.
	 *
	 * @param cdUsuarioInclusaoInterno the cd usuario inclusao interno
	 */
	public void setCdUsuarioInclusaoInterno(String cdUsuarioInclusaoInterno) {
		this.cdUsuarioInclusaoInterno = cdUsuarioInclusaoInterno;
	}
	
	/**
	 * Get: cdUsuarioManutencaoExterno.
	 *
	 * @return cdUsuarioManutencaoExterno
	 */
	public String getCdUsuarioManutencaoExterno() {
		return cdUsuarioManutencaoExterno;
	}
	
	/**
	 * Set: cdUsuarioManutencaoExterno.
	 *
	 * @param cdUsuarioManutencaoExterno the cd usuario manutencao externo
	 */
	public void setCdUsuarioManutencaoExterno(String cdUsuarioManutencaoExterno) {
		this.cdUsuarioManutencaoExterno = cdUsuarioManutencaoExterno;
	}
	
	/**
	 * Get: cdUsuarioManutencaoInterno.
	 *
	 * @return cdUsuarioManutencaoInterno
	 */
	public String getCdUsuarioManutencaoInterno() {
		return cdUsuarioManutencaoInterno;
	}
	
	/**
	 * Set: cdUsuarioManutencaoInterno.
	 *
	 * @param cdUsuarioManutencaoInterno the cd usuario manutencao interno
	 */
	public void setCdUsuarioManutencaoInterno(String cdUsuarioManutencaoInterno) {
		this.cdUsuarioManutencaoInterno = cdUsuarioManutencaoInterno;
	}
	
	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}
	
	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}
	
	/**
	 * Get: dsAgenciaDebito.
	 *
	 * @return dsAgenciaDebito
	 */
	public String getDsAgenciaDebito() {
		return dsAgenciaDebito;
	}
	
	/**
	 * Set: dsAgenciaDebito.
	 *
	 * @param dsAgenciaDebito the ds agencia debito
	 */
	public void setDsAgenciaDebito(String dsAgenciaDebito) {
		this.dsAgenciaDebito = dsAgenciaDebito;
	}
	
	/**
	 * Get: dsAgenciaDestino.
	 *
	 * @return dsAgenciaDestino
	 */
	public String getDsAgenciaDestino() {
		return dsAgenciaDestino;
	}
	
	/**
	 * Set: dsAgenciaDestino.
	 *
	 * @param dsAgenciaDestino the ds agencia destino
	 */
	public void setDsAgenciaDestino(String dsAgenciaDestino) {
		this.dsAgenciaDestino = dsAgenciaDestino;
	}
	
	/**
	 * Get: dsAgenciaFavorecido.
	 *
	 * @return dsAgenciaFavorecido
	 */
	public String getDsAgenciaFavorecido() {
		return dsAgenciaFavorecido;
	}
	
	/**
	 * Set: dsAgenciaFavorecido.
	 *
	 * @param dsAgenciaFavorecido the ds agencia favorecido
	 */
	public void setDsAgenciaFavorecido(String dsAgenciaFavorecido) {
		this.dsAgenciaFavorecido = dsAgenciaFavorecido;
	}
	
	/**
	 * Get: dsBancoDebito.
	 *
	 * @return dsBancoDebito
	 */
	public String getDsBancoDebito() {
		return dsBancoDebito;
	}
	
	/**
	 * Set: dsBancoDebito.
	 *
	 * @param dsBancoDebito the ds banco debito
	 */
	public void setDsBancoDebito(String dsBancoDebito) {
		this.dsBancoDebito = dsBancoDebito;
	}
	
	/**
	 * Get: dsBancoDestino.
	 *
	 * @return dsBancoDestino
	 */
	public String getDsBancoDestino() {
		return dsBancoDestino;
	}
	
	/**
	 * Set: dsBancoDestino.
	 *
	 * @param dsBancoDestino the ds banco destino
	 */
	public void setDsBancoDestino(String dsBancoDestino) {
		this.dsBancoDestino = dsBancoDestino;
	}
	
	/**
	 * Get: dsBancoFavorecido.
	 *
	 * @return dsBancoFavorecido
	 */
	public String getDsBancoFavorecido() {
		return dsBancoFavorecido;
	}
	
	/**
	 * Set: dsBancoFavorecido.
	 *
	 * @param dsBancoFavorecido the ds banco favorecido
	 */
	public void setDsBancoFavorecido(String dsBancoFavorecido) {
		this.dsBancoFavorecido = dsBancoFavorecido;
	}
	
	/**
	 * Get: dsContrato.
	 *
	 * @return dsContrato
	 */
	public String getDsContrato() {
		return dsContrato;
	}
	
	/**
	 * Set: dsContrato.
	 *
	 * @param dsContrato the ds contrato
	 */
	public void setDsContrato(String dsContrato) {
		this.dsContrato = dsContrato;
	}
	
	/**
	 * Get: dsMensagemPrimeiraLinha.
	 *
	 * @return dsMensagemPrimeiraLinha
	 */
	public String getDsMensagemPrimeiraLinha() {
		return dsMensagemPrimeiraLinha;
	}
	
	/**
	 * Set: dsMensagemPrimeiraLinha.
	 *
	 * @param dsMensagemPrimeiraLinha the ds mensagem primeira linha
	 */
	public void setDsMensagemPrimeiraLinha(String dsMensagemPrimeiraLinha) {
		this.dsMensagemPrimeiraLinha = dsMensagemPrimeiraLinha;
	}
	
	/**
	 * Get: dsMensagemSegundaLinha.
	 *
	 * @return dsMensagemSegundaLinha
	 */
	public String getDsMensagemSegundaLinha() {
		return dsMensagemSegundaLinha;
	}
	
	/**
	 * Set: dsMensagemSegundaLinha.
	 *
	 * @param dsMensagemSegundaLinha the ds mensagem segunda linha
	 */
	public void setDsMensagemSegundaLinha(String dsMensagemSegundaLinha) {
		this.dsMensagemSegundaLinha = dsMensagemSegundaLinha;
	}
	
	/**
	 * Get: dsMotivoSituacao.
	 *
	 * @return dsMotivoSituacao
	 */
	public String getDsMotivoSituacao() {
		return dsMotivoSituacao;
	}
	
	/**
	 * Set: dsMotivoSituacao.
	 *
	 * @param dsMotivoSituacao the ds motivo situacao
	 */
	public void setDsMotivoSituacao(String dsMotivoSituacao) {
		this.dsMotivoSituacao = dsMotivoSituacao;
	}
	
	/**
	 * Get: dsPagamento.
	 *
	 * @return dsPagamento
	 */
	public String getDsPagamento() {
		return dsPagamento;
	}
	
	/**
	 * Set: dsPagamento.
	 *
	 * @param dsPagamento the ds pagamento
	 */
	public void setDsPagamento(String dsPagamento) {
		this.dsPagamento = dsPagamento;
	}
	
	/**
	 * Get: dsTipoCanalInclusao.
	 *
	 * @return dsTipoCanalInclusao
	 */
	public String getDsTipoCanalInclusao() {
		return dsTipoCanalInclusao;
	}
	
	/**
	 * Set: dsTipoCanalInclusao.
	 *
	 * @param dsTipoCanalInclusao the ds tipo canal inclusao
	 */
	public void setDsTipoCanalInclusao(String dsTipoCanalInclusao) {
		this.dsTipoCanalInclusao = dsTipoCanalInclusao;
	}
	
	/**
	 * Get: dsTipoCanalManutencao.
	 *
	 * @return dsTipoCanalManutencao
	 */
	public String getDsTipoCanalManutencao() {
		return dsTipoCanalManutencao;
	}
	
	/**
	 * Set: dsTipoCanalManutencao.
	 *
	 * @param dsTipoCanalManutencao the ds tipo canal manutencao
	 */
	public void setDsTipoCanalManutencao(String dsTipoCanalManutencao) {
		this.dsTipoCanalManutencao = dsTipoCanalManutencao;
	}
	
	/**
	 * Get: dsTipoContaDebito.
	 *
	 * @return dsTipoContaDebito
	 */
	public String getDsTipoContaDebito() {
		return dsTipoContaDebito;
	}
	
	/**
	 * Set: dsTipoContaDebito.
	 *
	 * @param dsTipoContaDebito the ds tipo conta debito
	 */
	public void setDsTipoContaDebito(String dsTipoContaDebito) {
		this.dsTipoContaDebito = dsTipoContaDebito;
	}
	
	/**
	 * Get: dsTipoContaDestino.
	 *
	 * @return dsTipoContaDestino
	 */
	public String getDsTipoContaDestino() {
		return dsTipoContaDestino;
	}
	
	/**
	 * Set: dsTipoContaDestino.
	 *
	 * @param dsTipoContaDestino the ds tipo conta destino
	 */
	public void setDsTipoContaDestino(String dsTipoContaDestino) {
		this.dsTipoContaDestino = dsTipoContaDestino;
	}
	
	/**
	 * Get: dsTipoContaFavorecido.
	 *
	 * @return dsTipoContaFavorecido
	 */
	public String getDsTipoContaFavorecido() {
		return dsTipoContaFavorecido;
	}
	
	/**
	 * Set: dsTipoContaFavorecido.
	 *
	 * @param dsTipoContaFavorecido the ds tipo conta favorecido
	 */
	public void setDsTipoContaFavorecido(String dsTipoContaFavorecido) {
		this.dsTipoContaFavorecido = dsTipoContaFavorecido;
	}
	
	/**
	 * Get: dsTipoManutencao.
	 *
	 * @return dsTipoManutencao
	 */
	public String getDsTipoManutencao() {
		return dsTipoManutencao;
	}
	
	/**
	 * Set: dsTipoManutencao.
	 *
	 * @param dsTipoManutencao the ds tipo manutencao
	 */
	public void setDsTipoManutencao(String dsTipoManutencao) {
		this.dsTipoManutencao = dsTipoManutencao;
	}
	
	/**
	 * Get: dsUsoEmpresa.
	 *
	 * @return dsUsoEmpresa
	 */
	public String getDsUsoEmpresa() {
		return dsUsoEmpresa;
	}
	
	/**
	 * Set: dsUsoEmpresa.
	 *
	 * @param dsUsoEmpresa the ds uso empresa
	 */
	public void setDsUsoEmpresa(String dsUsoEmpresa) {
		this.dsUsoEmpresa = dsUsoEmpresa;
	}
	
	/**
	 * Get: dtAgendamento.
	 *
	 * @return dtAgendamento
	 */
	public String getDtAgendamento() {
		return dtAgendamento;
	}
	
	/**
	 * Set: dtAgendamento.
	 *
	 * @param dtAgendamento the dt agendamento
	 */
	public void setDtAgendamento(String dtAgendamento) {
		this.dtAgendamento = dtAgendamento;
	}
	
	/**
	 * Get: dtEmissaoDocumento.
	 *
	 * @return dtEmissaoDocumento
	 */
	public String getDtEmissaoDocumento() {
		return dtEmissaoDocumento;
	}
	
	/**
	 * Set: dtEmissaoDocumento.
	 *
	 * @param dtEmissaoDocumento the dt emissao documento
	 */
	public void setDtEmissaoDocumento(String dtEmissaoDocumento) {
		this.dtEmissaoDocumento = dtEmissaoDocumento;
	}
	
	/**
	 * Get: dtInclusao.
	 *
	 * @return dtInclusao
	 */
	public String getDtInclusao() {
		return dtInclusao;
	}
	
	/**
	 * Set: dtInclusao.
	 *
	 * @param dtInclusao the dt inclusao
	 */
	public void setDtInclusao(String dtInclusao) {
		this.dtInclusao = dtInclusao;
	}
	
	/**
	 * Get: dtManutencao.
	 *
	 * @return dtManutencao
	 */
	public String getDtManutencao() {
		return dtManutencao;
	}
	
	/**
	 * Set: dtManutencao.
	 *
	 * @param dtManutencao the dt manutencao
	 */
	public void setDtManutencao(String dtManutencao) {
		this.dtManutencao = dtManutencao;
	}
	
	/**
	 * Get: dtNascimentoBeneficiario.
	 *
	 * @return dtNascimentoBeneficiario
	 */
	public String getDtNascimentoBeneficiario() {
		return dtNascimentoBeneficiario;
	}
	
	/**
	 * Set: dtNascimentoBeneficiario.
	 *
	 * @param dtNascimentoBeneficiario the dt nascimento beneficiario
	 */
	public void setDtNascimentoBeneficiario(String dtNascimentoBeneficiario) {
		this.dtNascimentoBeneficiario = dtNascimentoBeneficiario;
	}
	
	/**
	 * Get: dtVencimento.
	 *
	 * @return dtVencimento
	 */
	public String getDtVencimento() {
		return dtVencimento;
	}
	
	/**
	 * Set: dtVencimento.
	 *
	 * @param dtVencimento the dt vencimento
	 */
	public void setDtVencimento(String dtVencimento) {
		this.dtVencimento = dtVencimento;
	}
	
	/**
	 * Get: hrInclusao.
	 *
	 * @return hrInclusao
	 */
	public String getHrInclusao() {
		return hrInclusao;
	}
	
	/**
	 * Set: hrInclusao.
	 *
	 * @param hrInclusao the hr inclusao
	 */
	public void setHrInclusao(String hrInclusao) {
		this.hrInclusao = hrInclusao;
	}
	
	/**
	 * Get: hrManutencao.
	 *
	 * @return hrManutencao
	 */
	public String getHrManutencao() {
		return hrManutencao;
	}
	
	/**
	 * Set: hrManutencao.
	 *
	 * @param hrManutencao the hr manutencao
	 */
	public void setHrManutencao(String hrManutencao) {
		this.hrManutencao = hrManutencao;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	/**
	 * Get: nrDocumento.
	 *
	 * @return nrDocumento
	 */
	public String getNrDocumento() {
		return nrDocumento;
	}
	
	/**
	 * Set: nrDocumento.
	 *
	 * @param nrDocumento the nr documento
	 */
	public void setNrDocumento(String nrDocumento) {
		this.nrDocumento = nrDocumento;
	}
	
	/**
	 * Get: nrLoteArquivoRemessa.
	 *
	 * @return nrLoteArquivoRemessa
	 */
	public String getNrLoteArquivoRemessa() {
		return nrLoteArquivoRemessa;
	}
	
	/**
	 * Set: nrLoteArquivoRemessa.
	 *
	 * @param nrLoteArquivoRemessa the nr lote arquivo remessa
	 */
	public void setNrLoteArquivoRemessa(String nrLoteArquivoRemessa) {
		this.nrLoteArquivoRemessa = nrLoteArquivoRemessa;
	}
	
	/**
	 * Get: nrSequenciaArquivoRemessa.
	 *
	 * @return nrSequenciaArquivoRemessa
	 */
	public String getNrSequenciaArquivoRemessa() {
		return nrSequenciaArquivoRemessa;
	}
	
	/**
	 * Set: nrSequenciaArquivoRemessa.
	 *
	 * @param nrSequenciaArquivoRemessa the nr sequencia arquivo remessa
	 */
	public void setNrSequenciaArquivoRemessa(String nrSequenciaArquivoRemessa) {
		this.nrSequenciaArquivoRemessa = nrSequenciaArquivoRemessa;
	}
	
	/**
	 * Get: qtMoeda.
	 *
	 * @return qtMoeda
	 */
	public BigDecimal getQtMoeda() {
		return qtMoeda;
	}
	
	/**
	 * Set: qtMoeda.
	 *
	 * @param qtMoeda the qt moeda
	 */
	public void setQtMoeda(BigDecimal qtMoeda) {
		this.qtMoeda = qtMoeda;
	}
	
	/**
	 * Get: vlAbatimento.
	 *
	 * @return vlAbatimento
	 */
	public BigDecimal getVlAbatimento() {
		return vlAbatimento;
	}
	
	/**
	 * Set: vlAbatimento.
	 *
	 * @param vlAbatimento the vl abatimento
	 */
	public void setVlAbatimento(BigDecimal vlAbatimento) {
		this.vlAbatimento = vlAbatimento;
	}
	
	/**
	 * Get: vlAcrescimo.
	 *
	 * @return vlAcrescimo
	 */
	public BigDecimal getVlAcrescimo() {
		return vlAcrescimo;
	}
	
	/**
	 * Set: vlAcrescimo.
	 *
	 * @param vlAcrescimo the vl acrescimo
	 */
	public void setVlAcrescimo(BigDecimal vlAcrescimo) {
		this.vlAcrescimo = vlAcrescimo;
	}
	
	/**
	 * Get: vlDeducao.
	 *
	 * @return vlDeducao
	 */
	public BigDecimal getVlDeducao() {
		return vlDeducao;
	}
	
	/**
	 * Set: vlDeducao.
	 *
	 * @param vlDeducao the vl deducao
	 */
	public void setVlDeducao(BigDecimal vlDeducao) {
		this.vlDeducao = vlDeducao;
	}
	
	/**
	 * Get: vlDesconto.
	 *
	 * @return vlDesconto
	 */
	public BigDecimal getVlDesconto() {
		return vlDesconto;
	}
	
	/**
	 * Set: vlDesconto.
	 *
	 * @param vlDesconto the vl desconto
	 */
	public void setVlDesconto(BigDecimal vlDesconto) {
		this.vlDesconto = vlDesconto;
	}
	
	/**
	 * Get: vlDocumento.
	 *
	 * @return vlDocumento
	 */
	public BigDecimal getVlDocumento() {
		return vlDocumento;
	}
	
	/**
	 * Set: vlDocumento.
	 *
	 * @param vlDocumento the vl documento
	 */
	public void setVlDocumento(BigDecimal vlDocumento) {
		this.vlDocumento = vlDocumento;
	}
	
	/**
	 * Get: vlImpostoRenda.
	 *
	 * @return vlImpostoRenda
	 */
	public BigDecimal getVlImpostoRenda() {
		return vlImpostoRenda;
	}
	
	/**
	 * Set: vlImpostoRenda.
	 *
	 * @param vlImpostoRenda the vl imposto renda
	 */
	public void setVlImpostoRenda(BigDecimal vlImpostoRenda) {
		this.vlImpostoRenda = vlImpostoRenda;
	}
	
	/**
	 * Get: vlInss.
	 *
	 * @return vlInss
	 */
	public BigDecimal getVlInss() {
		return vlInss;
	}
	
	/**
	 * Set: vlInss.
	 *
	 * @param vlInss the vl inss
	 */
	public void setVlInss(BigDecimal vlInss) {
		this.vlInss = vlInss;
	}
	
	/**
	 * Get: vlIof.
	 *
	 * @return vlIof
	 */
	public BigDecimal getVlIof() {
		return vlIof;
	}
	
	/**
	 * Set: vlIof.
	 *
	 * @param vlIof the vl iof
	 */
	public void setVlIof(BigDecimal vlIof) {
		this.vlIof = vlIof;
	}
	
	/**
	 * Get: vlIss.
	 *
	 * @return vlIss
	 */
	public BigDecimal getVlIss() {
		return vlIss;
	}
	
	/**
	 * Set: vlIss.
	 *
	 * @param vlIss the vl iss
	 */
	public void setVlIss(BigDecimal vlIss) {
		this.vlIss = vlIss;
	}
	
	/**
	 * Get: vlMora.
	 *
	 * @return vlMora
	 */
	public BigDecimal getVlMora() {
		return vlMora;
	}
	
	/**
	 * Set: vlMora.
	 *
	 * @param vlMora the vl mora
	 */
	public void setVlMora(BigDecimal vlMora) {
		this.vlMora = vlMora;
	}
	
	/**
	 * Get: vlMulta.
	 *
	 * @return vlMulta
	 */
	public BigDecimal getVlMulta() {
		return vlMulta;
	}
	
	/**
	 * Set: vlMulta.
	 *
	 * @param vlMulta the vl multa
	 */
	public void setVlMulta(BigDecimal vlMulta) {
		this.vlMulta = vlMulta;
	}
	
	/**
	 * Get: vlrAgendamento.
	 *
	 * @return vlrAgendamento
	 */
	public BigDecimal getVlrAgendamento() {
		return vlrAgendamento;
	}
	
	/**
	 * Set: vlrAgendamento.
	 *
	 * @param vlrAgendamento the vlr agendamento
	 */
	public void setVlrAgendamento(BigDecimal vlrAgendamento) {
		this.vlrAgendamento = vlrAgendamento;
	}
	
	/**
	 * Get: vlrEfetivacao.
	 *
	 * @return vlrEfetivacao
	 */
	public BigDecimal getVlrEfetivacao() {
		return vlrEfetivacao;
	}
	
	/**
	 * Set: vlrEfetivacao.
	 *
	 * @param vlrEfetivacao the vlr efetivacao
	 */
	public void setVlrEfetivacao(BigDecimal vlrEfetivacao) {
		this.vlrEfetivacao = vlrEfetivacao;
	}
	
	/**
	 * Get: cdTipoDocumento.
	 *
	 * @return cdTipoDocumento
	 */
	public int getCdTipoDocumento() {
		return cdTipoDocumento;
	}
	
	/**
	 * Set: cdTipoDocumento.
	 *
	 * @param cdTipoDocumento the cd tipo documento
	 */
	public void setCdTipoDocumento(int cdTipoDocumento) {
		this.cdTipoDocumento = cdTipoDocumento;
	}
	
	/**
	 * Get: dsTipoDocumento.
	 *
	 * @return dsTipoDocumento
	 */
	public String getDsTipoDocumento() {
		return dsTipoDocumento;
	}
	
	/**
	 * Set: dsTipoDocumento.
	 *
	 * @param dsTipoDocumento the ds tipo documento
	 */
	public void setDsTipoDocumento(String dsTipoDocumento) {
		this.dsTipoDocumento = dsTipoDocumento;
	}
	
	/**
	 * Get: cdAgenciaFinal.
	 *
	 * @return cdAgenciaFinal
	 */
	public Integer getCdAgenciaFinal() {
		return cdAgenciaFinal;
	}
	
	/**
	 * Set: cdAgenciaFinal.
	 *
	 * @param cdAgenciaFinal the cd agencia final
	 */
	public void setCdAgenciaFinal(Integer cdAgenciaFinal) {
		this.cdAgenciaFinal = cdAgenciaFinal;
	}
	
	/**
	 * Get: cdBancoFinal.
	 *
	 * @return cdBancoFinal
	 */
	public Integer getCdBancoFinal() {
		return cdBancoFinal;
	}
	
	/**
	 * Set: cdBancoFinal.
	 *
	 * @param cdBancoFinal the cd banco final
	 */
	public void setCdBancoFinal(Integer cdBancoFinal) {
		this.cdBancoFinal = cdBancoFinal;
	}
	
	/**
	 * Get: cdContaFinal.
	 *
	 * @return cdContaFinal
	 */
	public Long getCdContaFinal() {
		return cdContaFinal;
	}
	
	/**
	 * Set: cdContaFinal.
	 *
	 * @param cdContaFinal the cd conta final
	 */
	public void setCdContaFinal(Long cdContaFinal) {
		this.cdContaFinal = cdContaFinal;
	}
	
	/**
	 * Get: cdDigitoAgenciaFinal.
	 *
	 * @return cdDigitoAgenciaFinal
	 */
	public Integer getCdDigitoAgenciaFinal() {
		return cdDigitoAgenciaFinal;
	}
	
	/**
	 * Set: cdDigitoAgenciaFinal.
	 *
	 * @param cdDigitoAgenciaFinal the cd digito agencia final
	 */
	public void setCdDigitoAgenciaFinal(Integer cdDigitoAgenciaFinal) {
		this.cdDigitoAgenciaFinal = cdDigitoAgenciaFinal;
	}
	
	/**
	 * Get: cdDigitoContaFinal.
	 *
	 * @return cdDigitoContaFinal
	 */
	public String getCdDigitoContaFinal() {
		return cdDigitoContaFinal;
	}
	
	/**
	 * Set: cdDigitoContaFinal.
	 *
	 * @param cdDigitoContaFinal the cd digito conta final
	 */
	public void setCdDigitoContaFinal(String cdDigitoContaFinal) {
		this.cdDigitoContaFinal = cdDigitoContaFinal;
	}
	
	/**
	 * Get: cdStatusTransmissao.
	 *
	 * @return cdStatusTransmissao
	 */
	public Integer getCdStatusTransmissao() {
		return cdStatusTransmissao;
	}
	
	/**
	 * Set: cdStatusTransmissao.
	 *
	 * @param cdStatusTransmissao the cd status transmissao
	 */
	public void setCdStatusTransmissao(Integer cdStatusTransmissao) {
		this.cdStatusTransmissao = cdStatusTransmissao;
	}
	
	/**
	 * Get: dsStatusTransmissao.
	 *
	 * @return dsStatusTransmissao
	 */
	public String getDsStatusTransmissao() {
		return dsStatusTransmissao;
	}
	
	/**
	 * Set: dsStatusTransmissao.
	 *
	 * @param dsStatusTransmissao the ds status transmissao
	 */
	public void setDsStatusTransmissao(String dsStatusTransmissao) {
		this.dsStatusTransmissao = dsStatusTransmissao;
	}
	
	/**
	 * Get: dtHoraDevolucao.
	 *
	 * @return dtHoraDevolucao
	 */
	public String getDtHoraDevolucao() {
		return dtHoraDevolucao;
	}
	
	/**
	 * Set: dtHoraDevolucao.
	 *
	 * @param dtHoraDevolucao the dt hora devolucao
	 */
	public void setDtHoraDevolucao(String dtHoraDevolucao) {
		this.dtHoraDevolucao = dtHoraDevolucao;
	}
	
	/**
	 * Get: dtHoraTransmissao.
	 *
	 * @return dtHoraTransmissao
	 */
	public String getDtHoraTransmissao() {
		return dtHoraTransmissao;
	}
	
	/**
	 * Set: dtHoraTransmissao.
	 *
	 * @param dtHoraTransmissao the dt hora transmissao
	 */
	public void setDtHoraTransmissao(String dtHoraTransmissao) {
		this.dtHoraTransmissao = dtHoraTransmissao;
	}
	
	/**
	 * Get: vlDebitoAut.
	 *
	 * @return vlDebitoAut
	 */
	public BigDecimal getVlDebitoAut() {
		return vlDebitoAut;
	}
	
	/**
	 * Set: vlDebitoAut.
	 *
	 * @param vlDebitoAut the vl debito aut
	 */
	public void setVlDebitoAut(BigDecimal vlDebitoAut) {
		this.vlDebitoAut = vlDebitoAut;
	}
	
	/**
	 * Get: vlEmpresa.
	 *
	 * @return vlEmpresa
	 */
	public BigDecimal getVlEmpresa() {
		return vlEmpresa;
	}
	
	/**
	 * Set: vlEmpresa.
	 *
	 * @param vlEmpresa the vl empresa
	 */
	public void setVlEmpresa(BigDecimal vlEmpresa) {
		this.vlEmpresa = vlEmpresa;
	}
	
	/**
	 * Get: vlLeasing.
	 *
	 * @return vlLeasing
	 */
	public BigDecimal getVlLeasing() {
		return vlLeasing;
	}
	
	/**
	 * Set: vlLeasing.
	 *
	 * @param vlLeasing the vl leasing
	 */
	public void setVlLeasing(BigDecimal vlLeasing) {
		this.vlLeasing = vlLeasing;
	}
	
	/**
	 * Get: complementoInclusaoFormatado.
	 *
	 * @return complementoInclusaoFormatado
	 */
	public String getComplementoInclusaoFormatado() {
		return complementoInclusaoFormatado;
	}
	
	/**
	 * Set: complementoInclusaoFormatado.
	 *
	 * @param complementoInclusaoFormatado the complemento inclusao formatado
	 */
	public void setComplementoInclusaoFormatado(String complementoInclusaoFormatado) {
		this.complementoInclusaoFormatado = complementoInclusaoFormatado;
	}
	
	/**
	 * Get: complementoManutencaoFormatado.
	 *
	 * @return complementoManutencaoFormatado
	 */
	public String getComplementoManutencaoFormatado() {
		return complementoManutencaoFormatado;
	}
	
	/**
	 * Set: complementoManutencaoFormatado.
	 *
	 * @param complementoManutencaoFormatado the complemento manutencao formatado
	 */
	public void setComplementoManutencaoFormatado(
			String complementoManutencaoFormatado) {
		this.complementoManutencaoFormatado = complementoManutencaoFormatado;
	}
	
	/**
	 * Get: dataHoraInclusaoFormatada.
	 *
	 * @return dataHoraInclusaoFormatada
	 */
	public String getDataHoraInclusaoFormatada() {
		return dataHoraInclusaoFormatada;
	}
	
	/**
	 * Set: dataHoraInclusaoFormatada.
	 *
	 * @param dataHoraInclusaoFormatada the data hora inclusao formatada
	 */
	public void setDataHoraInclusaoFormatada(String dataHoraInclusaoFormatada) {
		this.dataHoraInclusaoFormatada = dataHoraInclusaoFormatada;
	}
	
	/**
	 * Get: dataHoraManutencaoFormatada.
	 *
	 * @return dataHoraManutencaoFormatada
	 */
	public String getDataHoraManutencaoFormatada() {
		return dataHoraManutencaoFormatada;
	}
	
	/**
	 * Set: dataHoraManutencaoFormatada.
	 *
	 * @param dataHoraManutencaoFormatada the data hora manutencao formatada
	 */
	public void setDataHoraManutencaoFormatada(String dataHoraManutencaoFormatada) {
		this.dataHoraManutencaoFormatada = dataHoraManutencaoFormatada;
	}
	
	/**
	 * Get: tipoCanalInclusaoFormatado.
	 *
	 * @return tipoCanalInclusaoFormatado
	 */
	public String getTipoCanalInclusaoFormatado() {
		return tipoCanalInclusaoFormatado;
	}
	
	/**
	 * Set: tipoCanalInclusaoFormatado.
	 *
	 * @param tipoCanalInclusaoFormatado the tipo canal inclusao formatado
	 */
	public void setTipoCanalInclusaoFormatado(String tipoCanalInclusaoFormatado) {
		this.tipoCanalInclusaoFormatado = tipoCanalInclusaoFormatado;
	}
	
	/**
	 * Get: tipoCanalManutencaoFormatado.
	 *
	 * @return tipoCanalManutencaoFormatado
	 */
	public String getTipoCanalManutencaoFormatado() {
		return tipoCanalManutencaoFormatado;
	}
	
	/**
	 * Set: tipoCanalManutencaoFormatado.
	 *
	 * @param tipoCanalManutencaoFormatado the tipo canal manutencao formatado
	 */
	public void setTipoCanalManutencaoFormatado(String tipoCanalManutencaoFormatado) {
		this.tipoCanalManutencaoFormatado = tipoCanalManutencaoFormatado;
	}
	
	/**
	 * Get: usuarioInclusaoFormatado.
	 *
	 * @return usuarioInclusaoFormatado
	 */
	public String getUsuarioInclusaoFormatado() {
		return usuarioInclusaoFormatado;
	}
	
	/**
	 * Set: usuarioInclusaoFormatado.
	 *
	 * @param usuarioInclusaoFormatado the usuario inclusao formatado
	 */
	public void setUsuarioInclusaoFormatado(String usuarioInclusaoFormatado) {
		this.usuarioInclusaoFormatado = usuarioInclusaoFormatado;
	}
	
	/**
	 * Get: usuarioManutencaoFormatado.
	 *
	 * @return usuarioManutencaoFormatado
	 */
	public String getUsuarioManutencaoFormatado() {
		return usuarioManutencaoFormatado;
	}
	
	/**
	 * Set: usuarioManutencaoFormatado.
	 *
	 * @param usuarioManutencaoFormatado the usuario manutencao formatado
	 */
	public void setUsuarioManutencaoFormatado(String usuarioManutencaoFormatado) {
		this.usuarioManutencaoFormatado = usuarioManutencaoFormatado;
	}
	
	/**
	 * Get: dsMoeda.
	 *
	 * @return dsMoeda
	 */
	public String getDsMoeda() {
		return dsMoeda;
	}
	
	/**
	 * Set: dsMoeda.
	 *
	 * @param dsMoeda the ds moeda
	 */
	public void setDsMoeda(String dsMoeda) {
		this.dsMoeda = dsMoeda;
	}
	
	/**
	 * Get: cdSituacaoTranferenciaAutomatica.
	 *
	 * @return cdSituacaoTranferenciaAutomatica
	 */
	public String getCdSituacaoTranferenciaAutomatica() {
		return cdSituacaoTranferenciaAutomatica;
	}
	
	/**
	 * Set: cdSituacaoTranferenciaAutomatica.
	 *
	 * @param cdSituacaoTranferenciaAutomatica the cd situacao tranferencia automatica
	 */
	public void setCdSituacaoTranferenciaAutomatica(
			String cdSituacaoTranferenciaAutomatica) {
		this.cdSituacaoTranferenciaAutomatica = cdSituacaoTranferenciaAutomatica;
	}
	
	
	
    
    
}




