/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.validaragencia.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ValidarAgenciaResponse.
 * 
 * @version $Revision$ $Date$
 */
public class ValidarAgenciaResponse implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _codMensagem
     */
    private java.lang.String _codMensagem;

    /**
     * Field _mensagem
     */
    private java.lang.String _mensagem;

    /**
     * Field _cdTipoUnidadeOrganizacional
     */
    private int _cdTipoUnidadeOrganizacional = 0;

    /**
     * keeps track of state for field: _cdTipoUnidadeOrganizacional
     */
    private boolean _has_cdTipoUnidadeOrganizacional;

    /**
     * Field _cdUnidadeOrganizacional
     */
    private int _cdUnidadeOrganizacional = 0;

    /**
     * keeps track of state for field: _cdUnidadeOrganizacional
     */
    private boolean _has_cdUnidadeOrganizacional;

    /**
     * Field _cdIndicadorBloq
     */
    private java.lang.String _cdIndicadorBloq;


      //----------------/
     //- Constructors -/
    //----------------/

    public ValidarAgenciaResponse() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.validaragencia.response.ValidarAgenciaResponse()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdTipoUnidadeOrganizacional
     * 
     */
    public void deleteCdTipoUnidadeOrganizacional()
    {
        this._has_cdTipoUnidadeOrganizacional= false;
    } //-- void deleteCdTipoUnidadeOrganizacional() 

    /**
     * Method deleteCdUnidadeOrganizacional
     * 
     */
    public void deleteCdUnidadeOrganizacional()
    {
        this._has_cdUnidadeOrganizacional= false;
    } //-- void deleteCdUnidadeOrganizacional() 

    /**
     * Returns the value of field 'cdIndicadorBloq'.
     * 
     * @return String
     * @return the value of field 'cdIndicadorBloq'.
     */
    public java.lang.String getCdIndicadorBloq()
    {
        return this._cdIndicadorBloq;
    } //-- java.lang.String getCdIndicadorBloq() 

    /**
     * Returns the value of field 'cdTipoUnidadeOrganizacional'.
     * 
     * @return int
     * @return the value of field 'cdTipoUnidadeOrganizacional'.
     */
    public int getCdTipoUnidadeOrganizacional()
    {
        return this._cdTipoUnidadeOrganizacional;
    } //-- int getCdTipoUnidadeOrganizacional() 

    /**
     * Returns the value of field 'cdUnidadeOrganizacional'.
     * 
     * @return int
     * @return the value of field 'cdUnidadeOrganizacional'.
     */
    public int getCdUnidadeOrganizacional()
    {
        return this._cdUnidadeOrganizacional;
    } //-- int getCdUnidadeOrganizacional() 

    /**
     * Returns the value of field 'codMensagem'.
     * 
     * @return String
     * @return the value of field 'codMensagem'.
     */
    public java.lang.String getCodMensagem()
    {
        return this._codMensagem;
    } //-- java.lang.String getCodMensagem() 

    /**
     * Returns the value of field 'mensagem'.
     * 
     * @return String
     * @return the value of field 'mensagem'.
     */
    public java.lang.String getMensagem()
    {
        return this._mensagem;
    } //-- java.lang.String getMensagem() 

    /**
     * Method hasCdTipoUnidadeOrganizacional
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoUnidadeOrganizacional()
    {
        return this._has_cdTipoUnidadeOrganizacional;
    } //-- boolean hasCdTipoUnidadeOrganizacional() 

    /**
     * Method hasCdUnidadeOrganizacional
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdUnidadeOrganizacional()
    {
        return this._has_cdUnidadeOrganizacional;
    } //-- boolean hasCdUnidadeOrganizacional() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdIndicadorBloq'.
     * 
     * @param cdIndicadorBloq the value of field 'cdIndicadorBloq'.
     */
    public void setCdIndicadorBloq(java.lang.String cdIndicadorBloq)
    {
        this._cdIndicadorBloq = cdIndicadorBloq;
    } //-- void setCdIndicadorBloq(java.lang.String) 

    /**
     * Sets the value of field 'cdTipoUnidadeOrganizacional'.
     * 
     * @param cdTipoUnidadeOrganizacional the value of field
     * 'cdTipoUnidadeOrganizacional'.
     */
    public void setCdTipoUnidadeOrganizacional(int cdTipoUnidadeOrganizacional)
    {
        this._cdTipoUnidadeOrganizacional = cdTipoUnidadeOrganizacional;
        this._has_cdTipoUnidadeOrganizacional = true;
    } //-- void setCdTipoUnidadeOrganizacional(int) 

    /**
     * Sets the value of field 'cdUnidadeOrganizacional'.
     * 
     * @param cdUnidadeOrganizacional the value of field
     * 'cdUnidadeOrganizacional'.
     */
    public void setCdUnidadeOrganizacional(int cdUnidadeOrganizacional)
    {
        this._cdUnidadeOrganizacional = cdUnidadeOrganizacional;
        this._has_cdUnidadeOrganizacional = true;
    } //-- void setCdUnidadeOrganizacional(int) 

    /**
     * Sets the value of field 'codMensagem'.
     * 
     * @param codMensagem the value of field 'codMensagem'.
     */
    public void setCodMensagem(java.lang.String codMensagem)
    {
        this._codMensagem = codMensagem;
    } //-- void setCodMensagem(java.lang.String) 

    /**
     * Sets the value of field 'mensagem'.
     * 
     * @param mensagem the value of field 'mensagem'.
     */
    public void setMensagem(java.lang.String mensagem)
    {
        this._mensagem = mensagem;
    } //-- void setMensagem(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ValidarAgenciaResponse
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.validaragencia.response.ValidarAgenciaResponse unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.validaragencia.response.ValidarAgenciaResponse) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.validaragencia.response.ValidarAgenciaResponse.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.validaragencia.response.ValidarAgenciaResponse unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
