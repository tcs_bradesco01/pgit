/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.detalharprioridadetipocompromisso.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class DetalharPrioridadeTipoCompromissoResponse.
 * 
 * @version $Revision$ $Date$
 */
public class DetalharPrioridadeTipoCompromissoResponse implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _codMensagem
     */
    private java.lang.String _codMensagem;

    /**
     * Field _mensagem
     */
    private java.lang.String _mensagem;

    /**
     * Field _cdCompromissoPrioridadeProduto
     */
    private int _cdCompromissoPrioridadeProduto = 0;

    /**
     * keeps track of state for field:
     * _cdCompromissoPrioridadeProduto
     */
    private boolean _has_cdCompromissoPrioridadeProduto;

    /**
     * Field _centroCustoPrioridadeProduto
     */
    private java.lang.String _centroCustoPrioridadeProduto;

    /**
     * Field _dtInicioVigenciaCompromisso
     */
    private java.lang.String _dtInicioVigenciaCompromisso;

    /**
     * Field _dtFimVigenciaCompromisso
     */
    private java.lang.String _dtFimVigenciaCompromisso;

    /**
     * Field _cdOrdemPrioridadeCompromisso
     */
    private int _cdOrdemPrioridadeCompromisso = 0;

    /**
     * keeps track of state for field: _cdOrdemPrioridadeCompromisso
     */
    private boolean _has_cdOrdemPrioridadeCompromisso;

    /**
     * Field _cdCanalInclusao
     */
    private int _cdCanalInclusao = 0;

    /**
     * keeps track of state for field: _cdCanalInclusao
     */
    private boolean _has_cdCanalInclusao;

    /**
     * Field _dsCanalInclusao
     */
    private java.lang.String _dsCanalInclusao;

    /**
     * Field _cdAutenticacaoSegurancaInclusao
     */
    private java.lang.String _cdAutenticacaoSegurancaInclusao;

    /**
     * Field _nmOperacaoFluxoInclusao
     */
    private java.lang.String _nmOperacaoFluxoInclusao;

    /**
     * Field _hrInclusaoRegistro
     */
    private java.lang.String _hrInclusaoRegistro;

    /**
     * Field _cdCanaManutencao
     */
    private int _cdCanaManutencao = 0;

    /**
     * keeps track of state for field: _cdCanaManutencao
     */
    private boolean _has_cdCanaManutencao;

    /**
     * Field _dsCanaManutencao
     */
    private java.lang.String _dsCanaManutencao;

    /**
     * Field _cdAutenticacaoSegurancaManutencao
     */
    private java.lang.String _cdAutenticacaoSegurancaManutencao;

    /**
     * Field _nmOperacaoFluxoManutencao
     */
    private java.lang.String _nmOperacaoFluxoManutencao;

    /**
     * Field _hrManutencaoRegistro
     */
    private java.lang.String _hrManutencaoRegistro;


      //----------------/
     //- Constructors -/
    //----------------/

    public DetalharPrioridadeTipoCompromissoResponse() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharprioridadetipocompromisso.response.DetalharPrioridadeTipoCompromissoResponse()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdCanaManutencao
     * 
     */
    public void deleteCdCanaManutencao()
    {
        this._has_cdCanaManutencao= false;
    } //-- void deleteCdCanaManutencao() 

    /**
     * Method deleteCdCanalInclusao
     * 
     */
    public void deleteCdCanalInclusao()
    {
        this._has_cdCanalInclusao= false;
    } //-- void deleteCdCanalInclusao() 

    /**
     * Method deleteCdCompromissoPrioridadeProduto
     * 
     */
    public void deleteCdCompromissoPrioridadeProduto()
    {
        this._has_cdCompromissoPrioridadeProduto= false;
    } //-- void deleteCdCompromissoPrioridadeProduto() 

    /**
     * Method deleteCdOrdemPrioridadeCompromisso
     * 
     */
    public void deleteCdOrdemPrioridadeCompromisso()
    {
        this._has_cdOrdemPrioridadeCompromisso= false;
    } //-- void deleteCdOrdemPrioridadeCompromisso() 

    /**
     * Returns the value of field
     * 'cdAutenticacaoSegurancaInclusao'.
     * 
     * @return String
     * @return the value of field 'cdAutenticacaoSegurancaInclusao'.
     */
    public java.lang.String getCdAutenticacaoSegurancaInclusao()
    {
        return this._cdAutenticacaoSegurancaInclusao;
    } //-- java.lang.String getCdAutenticacaoSegurancaInclusao() 

    /**
     * Returns the value of field
     * 'cdAutenticacaoSegurancaManutencao'.
     * 
     * @return String
     * @return the value of field
     * 'cdAutenticacaoSegurancaManutencao'.
     */
    public java.lang.String getCdAutenticacaoSegurancaManutencao()
    {
        return this._cdAutenticacaoSegurancaManutencao;
    } //-- java.lang.String getCdAutenticacaoSegurancaManutencao() 

    /**
     * Returns the value of field 'cdCanaManutencao'.
     * 
     * @return int
     * @return the value of field 'cdCanaManutencao'.
     */
    public int getCdCanaManutencao()
    {
        return this._cdCanaManutencao;
    } //-- int getCdCanaManutencao() 

    /**
     * Returns the value of field 'cdCanalInclusao'.
     * 
     * @return int
     * @return the value of field 'cdCanalInclusao'.
     */
    public int getCdCanalInclusao()
    {
        return this._cdCanalInclusao;
    } //-- int getCdCanalInclusao() 

    /**
     * Returns the value of field 'cdCompromissoPrioridadeProduto'.
     * 
     * @return int
     * @return the value of field 'cdCompromissoPrioridadeProduto'.
     */
    public int getCdCompromissoPrioridadeProduto()
    {
        return this._cdCompromissoPrioridadeProduto;
    } //-- int getCdCompromissoPrioridadeProduto() 

    /**
     * Returns the value of field 'cdOrdemPrioridadeCompromisso'.
     * 
     * @return int
     * @return the value of field 'cdOrdemPrioridadeCompromisso'.
     */
    public int getCdOrdemPrioridadeCompromisso()
    {
        return this._cdOrdemPrioridadeCompromisso;
    } //-- int getCdOrdemPrioridadeCompromisso() 

    /**
     * Returns the value of field 'centroCustoPrioridadeProduto'.
     * 
     * @return String
     * @return the value of field 'centroCustoPrioridadeProduto'.
     */
    public java.lang.String getCentroCustoPrioridadeProduto()
    {
        return this._centroCustoPrioridadeProduto;
    } //-- java.lang.String getCentroCustoPrioridadeProduto() 

    /**
     * Returns the value of field 'codMensagem'.
     * 
     * @return String
     * @return the value of field 'codMensagem'.
     */
    public java.lang.String getCodMensagem()
    {
        return this._codMensagem;
    } //-- java.lang.String getCodMensagem() 

    /**
     * Returns the value of field 'dsCanaManutencao'.
     * 
     * @return String
     * @return the value of field 'dsCanaManutencao'.
     */
    public java.lang.String getDsCanaManutencao()
    {
        return this._dsCanaManutencao;
    } //-- java.lang.String getDsCanaManutencao() 

    /**
     * Returns the value of field 'dsCanalInclusao'.
     * 
     * @return String
     * @return the value of field 'dsCanalInclusao'.
     */
    public java.lang.String getDsCanalInclusao()
    {
        return this._dsCanalInclusao;
    } //-- java.lang.String getDsCanalInclusao() 

    /**
     * Returns the value of field 'dtFimVigenciaCompromisso'.
     * 
     * @return String
     * @return the value of field 'dtFimVigenciaCompromisso'.
     */
    public java.lang.String getDtFimVigenciaCompromisso()
    {
        return this._dtFimVigenciaCompromisso;
    } //-- java.lang.String getDtFimVigenciaCompromisso() 

    /**
     * Returns the value of field 'dtInicioVigenciaCompromisso'.
     * 
     * @return String
     * @return the value of field 'dtInicioVigenciaCompromisso'.
     */
    public java.lang.String getDtInicioVigenciaCompromisso()
    {
        return this._dtInicioVigenciaCompromisso;
    } //-- java.lang.String getDtInicioVigenciaCompromisso() 

    /**
     * Returns the value of field 'hrInclusaoRegistro'.
     * 
     * @return String
     * @return the value of field 'hrInclusaoRegistro'.
     */
    public java.lang.String getHrInclusaoRegistro()
    {
        return this._hrInclusaoRegistro;
    } //-- java.lang.String getHrInclusaoRegistro() 

    /**
     * Returns the value of field 'hrManutencaoRegistro'.
     * 
     * @return String
     * @return the value of field 'hrManutencaoRegistro'.
     */
    public java.lang.String getHrManutencaoRegistro()
    {
        return this._hrManutencaoRegistro;
    } //-- java.lang.String getHrManutencaoRegistro() 

    /**
     * Returns the value of field 'mensagem'.
     * 
     * @return String
     * @return the value of field 'mensagem'.
     */
    public java.lang.String getMensagem()
    {
        return this._mensagem;
    } //-- java.lang.String getMensagem() 

    /**
     * Returns the value of field 'nmOperacaoFluxoInclusao'.
     * 
     * @return String
     * @return the value of field 'nmOperacaoFluxoInclusao'.
     */
    public java.lang.String getNmOperacaoFluxoInclusao()
    {
        return this._nmOperacaoFluxoInclusao;
    } //-- java.lang.String getNmOperacaoFluxoInclusao() 

    /**
     * Returns the value of field 'nmOperacaoFluxoManutencao'.
     * 
     * @return String
     * @return the value of field 'nmOperacaoFluxoManutencao'.
     */
    public java.lang.String getNmOperacaoFluxoManutencao()
    {
        return this._nmOperacaoFluxoManutencao;
    } //-- java.lang.String getNmOperacaoFluxoManutencao() 

    /**
     * Method hasCdCanaManutencao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCanaManutencao()
    {
        return this._has_cdCanaManutencao;
    } //-- boolean hasCdCanaManutencao() 

    /**
     * Method hasCdCanalInclusao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCanalInclusao()
    {
        return this._has_cdCanalInclusao;
    } //-- boolean hasCdCanalInclusao() 

    /**
     * Method hasCdCompromissoPrioridadeProduto
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCompromissoPrioridadeProduto()
    {
        return this._has_cdCompromissoPrioridadeProduto;
    } //-- boolean hasCdCompromissoPrioridadeProduto() 

    /**
     * Method hasCdOrdemPrioridadeCompromisso
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdOrdemPrioridadeCompromisso()
    {
        return this._has_cdOrdemPrioridadeCompromisso;
    } //-- boolean hasCdOrdemPrioridadeCompromisso() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdAutenticacaoSegurancaInclusao'.
     * 
     * @param cdAutenticacaoSegurancaInclusao the value of field
     * 'cdAutenticacaoSegurancaInclusao'.
     */
    public void setCdAutenticacaoSegurancaInclusao(java.lang.String cdAutenticacaoSegurancaInclusao)
    {
        this._cdAutenticacaoSegurancaInclusao = cdAutenticacaoSegurancaInclusao;
    } //-- void setCdAutenticacaoSegurancaInclusao(java.lang.String) 

    /**
     * Sets the value of field 'cdAutenticacaoSegurancaManutencao'.
     * 
     * @param cdAutenticacaoSegurancaManutencao the value of field
     * 'cdAutenticacaoSegurancaManutencao'.
     */
    public void setCdAutenticacaoSegurancaManutencao(java.lang.String cdAutenticacaoSegurancaManutencao)
    {
        this._cdAutenticacaoSegurancaManutencao = cdAutenticacaoSegurancaManutencao;
    } //-- void setCdAutenticacaoSegurancaManutencao(java.lang.String) 

    /**
     * Sets the value of field 'cdCanaManutencao'.
     * 
     * @param cdCanaManutencao the value of field 'cdCanaManutencao'
     */
    public void setCdCanaManutencao(int cdCanaManutencao)
    {
        this._cdCanaManutencao = cdCanaManutencao;
        this._has_cdCanaManutencao = true;
    } //-- void setCdCanaManutencao(int) 

    /**
     * Sets the value of field 'cdCanalInclusao'.
     * 
     * @param cdCanalInclusao the value of field 'cdCanalInclusao'.
     */
    public void setCdCanalInclusao(int cdCanalInclusao)
    {
        this._cdCanalInclusao = cdCanalInclusao;
        this._has_cdCanalInclusao = true;
    } //-- void setCdCanalInclusao(int) 

    /**
     * Sets the value of field 'cdCompromissoPrioridadeProduto'.
     * 
     * @param cdCompromissoPrioridadeProduto the value of field
     * 'cdCompromissoPrioridadeProduto'.
     */
    public void setCdCompromissoPrioridadeProduto(int cdCompromissoPrioridadeProduto)
    {
        this._cdCompromissoPrioridadeProduto = cdCompromissoPrioridadeProduto;
        this._has_cdCompromissoPrioridadeProduto = true;
    } //-- void setCdCompromissoPrioridadeProduto(int) 

    /**
     * Sets the value of field 'cdOrdemPrioridadeCompromisso'.
     * 
     * @param cdOrdemPrioridadeCompromisso the value of field
     * 'cdOrdemPrioridadeCompromisso'.
     */
    public void setCdOrdemPrioridadeCompromisso(int cdOrdemPrioridadeCompromisso)
    {
        this._cdOrdemPrioridadeCompromisso = cdOrdemPrioridadeCompromisso;
        this._has_cdOrdemPrioridadeCompromisso = true;
    } //-- void setCdOrdemPrioridadeCompromisso(int) 

    /**
     * Sets the value of field 'centroCustoPrioridadeProduto'.
     * 
     * @param centroCustoPrioridadeProduto the value of field
     * 'centroCustoPrioridadeProduto'.
     */
    public void setCentroCustoPrioridadeProduto(java.lang.String centroCustoPrioridadeProduto)
    {
        this._centroCustoPrioridadeProduto = centroCustoPrioridadeProduto;
    } //-- void setCentroCustoPrioridadeProduto(java.lang.String) 

    /**
     * Sets the value of field 'codMensagem'.
     * 
     * @param codMensagem the value of field 'codMensagem'.
     */
    public void setCodMensagem(java.lang.String codMensagem)
    {
        this._codMensagem = codMensagem;
    } //-- void setCodMensagem(java.lang.String) 

    /**
     * Sets the value of field 'dsCanaManutencao'.
     * 
     * @param dsCanaManutencao the value of field 'dsCanaManutencao'
     */
    public void setDsCanaManutencao(java.lang.String dsCanaManutencao)
    {
        this._dsCanaManutencao = dsCanaManutencao;
    } //-- void setDsCanaManutencao(java.lang.String) 

    /**
     * Sets the value of field 'dsCanalInclusao'.
     * 
     * @param dsCanalInclusao the value of field 'dsCanalInclusao'.
     */
    public void setDsCanalInclusao(java.lang.String dsCanalInclusao)
    {
        this._dsCanalInclusao = dsCanalInclusao;
    } //-- void setDsCanalInclusao(java.lang.String) 

    /**
     * Sets the value of field 'dtFimVigenciaCompromisso'.
     * 
     * @param dtFimVigenciaCompromisso the value of field
     * 'dtFimVigenciaCompromisso'.
     */
    public void setDtFimVigenciaCompromisso(java.lang.String dtFimVigenciaCompromisso)
    {
        this._dtFimVigenciaCompromisso = dtFimVigenciaCompromisso;
    } //-- void setDtFimVigenciaCompromisso(java.lang.String) 

    /**
     * Sets the value of field 'dtInicioVigenciaCompromisso'.
     * 
     * @param dtInicioVigenciaCompromisso the value of field
     * 'dtInicioVigenciaCompromisso'.
     */
    public void setDtInicioVigenciaCompromisso(java.lang.String dtInicioVigenciaCompromisso)
    {
        this._dtInicioVigenciaCompromisso = dtInicioVigenciaCompromisso;
    } //-- void setDtInicioVigenciaCompromisso(java.lang.String) 

    /**
     * Sets the value of field 'hrInclusaoRegistro'.
     * 
     * @param hrInclusaoRegistro the value of field
     * 'hrInclusaoRegistro'.
     */
    public void setHrInclusaoRegistro(java.lang.String hrInclusaoRegistro)
    {
        this._hrInclusaoRegistro = hrInclusaoRegistro;
    } //-- void setHrInclusaoRegistro(java.lang.String) 

    /**
     * Sets the value of field 'hrManutencaoRegistro'.
     * 
     * @param hrManutencaoRegistro the value of field
     * 'hrManutencaoRegistro'.
     */
    public void setHrManutencaoRegistro(java.lang.String hrManutencaoRegistro)
    {
        this._hrManutencaoRegistro = hrManutencaoRegistro;
    } //-- void setHrManutencaoRegistro(java.lang.String) 

    /**
     * Sets the value of field 'mensagem'.
     * 
     * @param mensagem the value of field 'mensagem'.
     */
    public void setMensagem(java.lang.String mensagem)
    {
        this._mensagem = mensagem;
    } //-- void setMensagem(java.lang.String) 

    /**
     * Sets the value of field 'nmOperacaoFluxoInclusao'.
     * 
     * @param nmOperacaoFluxoInclusao the value of field
     * 'nmOperacaoFluxoInclusao'.
     */
    public void setNmOperacaoFluxoInclusao(java.lang.String nmOperacaoFluxoInclusao)
    {
        this._nmOperacaoFluxoInclusao = nmOperacaoFluxoInclusao;
    } //-- void setNmOperacaoFluxoInclusao(java.lang.String) 

    /**
     * Sets the value of field 'nmOperacaoFluxoManutencao'.
     * 
     * @param nmOperacaoFluxoManutencao the value of field
     * 'nmOperacaoFluxoManutencao'.
     */
    public void setNmOperacaoFluxoManutencao(java.lang.String nmOperacaoFluxoManutencao)
    {
        this._nmOperacaoFluxoManutencao = nmOperacaoFluxoManutencao;
    } //-- void setNmOperacaoFluxoManutencao(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return DetalharPrioridadeTipoCompromissoResponse
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.detalharprioridadetipocompromisso.response.DetalharPrioridadeTipoCompromissoResponse unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.detalharprioridadetipocompromisso.response.DetalharPrioridadeTipoCompromissoResponse) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.detalharprioridadetipocompromisso.response.DetalharPrioridadeTipoCompromissoResponse.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharprioridadetipocompromisso.response.DetalharPrioridadeTipoCompromissoResponse unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
