/*
 * Nome: br.com.bradesco.web.pgit.service.business.manterequiparacaoentrecontratos.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.manterequiparacaoentrecontratos.bean;

/**
 * Nome: ListarServicoEquiparacaoContratoOcorrencias
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarServicoEquiparacaoContratoOcorrencias {
	
	/** Atributo cdProdutoOperacaoRelacionado. */
	private Integer cdProdutoOperacaoRelacionado;
	
	/** Atributo dsProdutoOperRelacionado. */
	private String dsProdutoOperRelacionado;
	
	/** Atributo check. */
	private boolean check;
	
	/**
	 * Get: cdProdutoOperacaoRelacionado.
	 *
	 * @return cdProdutoOperacaoRelacionado
	 */
	public Integer getCdProdutoOperacaoRelacionado() {
		return cdProdutoOperacaoRelacionado;
	}
	
	/**
	 * Set: cdProdutoOperacaoRelacionado.
	 *
	 * @param cdProdutoOperacaoRelacionado the cd produto operacao relacionado
	 */
	public void setCdProdutoOperacaoRelacionado(Integer cdProdutoOperacaoRelacionado) {
		this.cdProdutoOperacaoRelacionado = cdProdutoOperacaoRelacionado;
	}
	
	/**
	 * Get: dsProdutoOperRelacionado.
	 *
	 * @return dsProdutoOperRelacionado
	 */
	public String getDsProdutoOperRelacionado() {
		return dsProdutoOperRelacionado;
	}
	
	/**
	 * Set: dsProdutoOperRelacionado.
	 *
	 * @param dsProdutoOperRelacionado the ds produto oper relacionado
	 */
	public void setDsProdutoOperRelacionado(String dsProdutoOperRelacionado) {
		this.dsProdutoOperRelacionado = dsProdutoOperRelacionado;
	}
	
	/**
	 * Is check.
	 *
	 * @return true, if is check
	 */
	public boolean isCheck() {
		return check;
	}
	
	/**
	 * Set: check.
	 *
	 * @param check the check
	 */
	public void setCheck(boolean check) {
		this.check = check;
	}
		
}
