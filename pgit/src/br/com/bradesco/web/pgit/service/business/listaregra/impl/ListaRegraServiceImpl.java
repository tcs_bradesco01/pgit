/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PilotoIntranet/JavaSource/br/com/bradesco/web/pgit/service/business/listaregra/impl/ListaRegraServiceImpl.java,v $
 * $Id: ListaRegraServiceImpl.java,v 1.2 2009/05/28 12:54:06 cpm.com.br\heslei.silva Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: ListaRegraServiceImpl.java,v $
 * Revision 1.2  2009/05/28 12:54:06  cpm.com.br\heslei.silva
 * *** empty log message ***
 *
 * Revision 1.1  2009/05/22 18:35:57  corporate\marcio.alves
 * Adicionando o servico listaregra para teste do pdc
 *
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */
 
package br.com.bradesco.web.pgit.service.business.listaregra.impl;

import java.util.ArrayList;
import java.util.List;

import br.com.bradesco.web.pgit.service.business.listaregra.IListaRegraService;
import br.com.bradesco.web.pgit.service.business.listaregra.bean.ListaRegraVO;
import br.com.bradesco.web.pgit.service.business.listaregra.exceptions.ListaRegraServiceException;
import br.com.bradesco.web.pgit.service.data.pdc.FactoryAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listaregra.request.ListaRegraRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listaregra.response.ListaRegraResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listaregra.response.Ocorrencia;


/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Implementa��o do adaptador: ListaRegra
 * </p>
 * 
 * @comment C�DIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public class ListaRegraServiceImpl implements IListaRegraService {

	/** Atributo factoryAdapter. */
	private FactoryAdapter factoryAdapter;
	
    /**
	 * Nome: getFactoryAdapter
	 * <p>Prop�sito: M�todo get para o atributo factoryAdapter. </p>
	 * @return FactoryAdapter factoryAdapter
	 */
	public FactoryAdapter getFactoryAdapter() {
		return factoryAdapter;
	}

	/**
	 * Nome: setFactoryAdapter
	 * <p>Prop�sito: M�todo set para o atributo factoryAdapter. </p>
	 * @param FactoryAdapter factoryAdapter
	 */
	public void setFactoryAdapter(FactoryAdapter factoryAdapter) {
		this.factoryAdapter = factoryAdapter;
	}

	/**
     * Construtor.
     */
    public ListaRegraServiceImpl() {
        // TODO: Implementa��o
    }

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.listaregra.IListaRegraService#listaRegras(br.com.bradesco.web.pgit.service.business.listaregra.bean.ListaRegraVO)
	 */
	public List<ListaRegraVO> listaRegras(ListaRegraVO vo) {
		List<ListaRegraVO> lista = new ArrayList<ListaRegraVO>();
		
		ListaRegraRequest request = new ListaRegraRequest();
		ListaRegraResponse response = null;
		
		if( !this.isValid(vo.getIndGestorMaster()) ) {
			throw new ListaRegraServiceException("O campo indGestorMaster � obrigat�rio");		
		}
		
		if( !this.isValid(vo.getTipoConsulta()) ) {
			throw new ListaRegraServiceException("O campo tipoConsulta � obrigat�rio");		
		}
		
		if( !this.isValid(vo.getCodRegraAlcada()) ) {
			throw new ListaRegraServiceException("O campo codRegraAlcada � obrigat�rio");		
		}
		
		if( !this.isValid(vo.getCodSituacao()) ) {
			throw new ListaRegraServiceException("O campo codSituacao � obrigat�rio");		
		}
		
		if( !this.isValid(vo.getCodPessoaJuridica()) ) {
			throw new ListaRegraServiceException("O campo codPessoaJuridica � obrigat�rio");		
		}
		
		if( !this.isValid(vo.getCodDepartamento()) ) {
			throw new ListaRegraServiceException("O campo codDepartamento � obrigat�rio");		
		}
		
		if( !this.isValid(vo.getCodTipoUorg()) ) {
			throw new ListaRegraServiceException("O campo codTipoUorg � obrigat�rio");		
		}
		
		request.setIndGestorMaster(vo.getIndGestorMaster());
		request.setTipoConsulta(vo.getTipoConsulta());
		request.setCodRegraAlcada(vo.getCodRegraAlcada());
		request.setCodSituacao(vo.getCodSituacao());
		request.setDataPesquisaInic(vo.getDataPesquisaInic());
		request.setDataPesquisaFim(vo.getDataPesquisaFim());
		request.setCodPessoaJuridica(vo.getCodPessoaJuridica());
		request.setCodDepartamento(vo.getCodDepartamento());
		
		response = this.factoryAdapter.getListaRegraPDCAdapter().invokeProcess(request);
		
		for( Ocorrencia ocur : response.getOcorrencia() ) {
			lista.add(new ListaRegraVO(ocur));
		}
		
		return lista;
	}
    
	/**
	 * Is valid.
	 *
	 * @param value the value
	 * @return true, if is valid
	 */
	private boolean isValid(Object value) {
		if( value == null ) {
			return false;
		} else if( value instanceof String ) {
			return !"".equals(value);
		} else if( value instanceof Integer ) {
			Integer temp = (Integer)value;
			return temp > -1;
		} else if( value instanceof Long ) {
			Long temp = (Long)value;
			return temp > -1;
		} else {
			return false;
		}
	}
	
}

