/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listarconmanservico.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _hrInclusaoRegistroHistorico
     */
    private java.lang.String _hrInclusaoRegistroHistorico;

    /**
     * Field _cdProduto
     */
    private int _cdProduto = 0;

    /**
     * keeps track of state for field: _cdProduto
     */
    private boolean _has_cdProduto;

    /**
     * Field _dsProduto
     */
    private java.lang.String _dsProduto;

    /**
     * Field _cdModalidade
     */
    private int _cdModalidade = 0;

    /**
     * keeps track of state for field: _cdModalidade
     */
    private boolean _has_cdModalidade;

    /**
     * Field _dsModalidade
     */
    private java.lang.String _dsModalidade;

    /**
     * Field _dtManutencao
     */
    private java.lang.String _dtManutencao;

    /**
     * Field _hrManutencao
     */
    private java.lang.String _hrManutencao;

    /**
     * Field _cdUsuario
     */
    private java.lang.String _cdUsuario;

    /**
     * Field _cdTipoServico
     */
    private int _cdTipoServico = 0;

    /**
     * keeps track of state for field: _cdTipoServico
     */
    private boolean _has_cdTipoServico;

    /**
     * Field _dsTipoManutencao
     */
    private java.lang.String _dsTipoManutencao;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarconmanservico.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdModalidade
     * 
     */
    public void deleteCdModalidade()
    {
        this._has_cdModalidade= false;
    } //-- void deleteCdModalidade() 

    /**
     * Method deleteCdProduto
     * 
     */
    public void deleteCdProduto()
    {
        this._has_cdProduto= false;
    } //-- void deleteCdProduto() 

    /**
     * Method deleteCdTipoServico
     * 
     */
    public void deleteCdTipoServico()
    {
        this._has_cdTipoServico= false;
    } //-- void deleteCdTipoServico() 

    /**
     * Returns the value of field 'cdModalidade'.
     * 
     * @return int
     * @return the value of field 'cdModalidade'.
     */
    public int getCdModalidade()
    {
        return this._cdModalidade;
    } //-- int getCdModalidade() 

    /**
     * Returns the value of field 'cdProduto'.
     * 
     * @return int
     * @return the value of field 'cdProduto'.
     */
    public int getCdProduto()
    {
        return this._cdProduto;
    } //-- int getCdProduto() 

    /**
     * Returns the value of field 'cdTipoServico'.
     * 
     * @return int
     * @return the value of field 'cdTipoServico'.
     */
    public int getCdTipoServico()
    {
        return this._cdTipoServico;
    } //-- int getCdTipoServico() 

    /**
     * Returns the value of field 'cdUsuario'.
     * 
     * @return String
     * @return the value of field 'cdUsuario'.
     */
    public java.lang.String getCdUsuario()
    {
        return this._cdUsuario;
    } //-- java.lang.String getCdUsuario() 

    /**
     * Returns the value of field 'dsModalidade'.
     * 
     * @return String
     * @return the value of field 'dsModalidade'.
     */
    public java.lang.String getDsModalidade()
    {
        return this._dsModalidade;
    } //-- java.lang.String getDsModalidade() 

    /**
     * Returns the value of field 'dsProduto'.
     * 
     * @return String
     * @return the value of field 'dsProduto'.
     */
    public java.lang.String getDsProduto()
    {
        return this._dsProduto;
    } //-- java.lang.String getDsProduto() 

    /**
     * Returns the value of field 'dsTipoManutencao'.
     * 
     * @return String
     * @return the value of field 'dsTipoManutencao'.
     */
    public java.lang.String getDsTipoManutencao()
    {
        return this._dsTipoManutencao;
    } //-- java.lang.String getDsTipoManutencao() 

    /**
     * Returns the value of field 'dtManutencao'.
     * 
     * @return String
     * @return the value of field 'dtManutencao'.
     */
    public java.lang.String getDtManutencao()
    {
        return this._dtManutencao;
    } //-- java.lang.String getDtManutencao() 

    /**
     * Returns the value of field 'hrInclusaoRegistroHistorico'.
     * 
     * @return String
     * @return the value of field 'hrInclusaoRegistroHistorico'.
     */
    public java.lang.String getHrInclusaoRegistroHistorico()
    {
        return this._hrInclusaoRegistroHistorico;
    } //-- java.lang.String getHrInclusaoRegistroHistorico() 

    /**
     * Returns the value of field 'hrManutencao'.
     * 
     * @return String
     * @return the value of field 'hrManutencao'.
     */
    public java.lang.String getHrManutencao()
    {
        return this._hrManutencao;
    } //-- java.lang.String getHrManutencao() 

    /**
     * Method hasCdModalidade
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdModalidade()
    {
        return this._has_cdModalidade;
    } //-- boolean hasCdModalidade() 

    /**
     * Method hasCdProduto
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProduto()
    {
        return this._has_cdProduto;
    } //-- boolean hasCdProduto() 

    /**
     * Method hasCdTipoServico
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoServico()
    {
        return this._has_cdTipoServico;
    } //-- boolean hasCdTipoServico() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdModalidade'.
     * 
     * @param cdModalidade the value of field 'cdModalidade'.
     */
    public void setCdModalidade(int cdModalidade)
    {
        this._cdModalidade = cdModalidade;
        this._has_cdModalidade = true;
    } //-- void setCdModalidade(int) 

    /**
     * Sets the value of field 'cdProduto'.
     * 
     * @param cdProduto the value of field 'cdProduto'.
     */
    public void setCdProduto(int cdProduto)
    {
        this._cdProduto = cdProduto;
        this._has_cdProduto = true;
    } //-- void setCdProduto(int) 

    /**
     * Sets the value of field 'cdTipoServico'.
     * 
     * @param cdTipoServico the value of field 'cdTipoServico'.
     */
    public void setCdTipoServico(int cdTipoServico)
    {
        this._cdTipoServico = cdTipoServico;
        this._has_cdTipoServico = true;
    } //-- void setCdTipoServico(int) 

    /**
     * Sets the value of field 'cdUsuario'.
     * 
     * @param cdUsuario the value of field 'cdUsuario'.
     */
    public void setCdUsuario(java.lang.String cdUsuario)
    {
        this._cdUsuario = cdUsuario;
    } //-- void setCdUsuario(java.lang.String) 

    /**
     * Sets the value of field 'dsModalidade'.
     * 
     * @param dsModalidade the value of field 'dsModalidade'.
     */
    public void setDsModalidade(java.lang.String dsModalidade)
    {
        this._dsModalidade = dsModalidade;
    } //-- void setDsModalidade(java.lang.String) 

    /**
     * Sets the value of field 'dsProduto'.
     * 
     * @param dsProduto the value of field 'dsProduto'.
     */
    public void setDsProduto(java.lang.String dsProduto)
    {
        this._dsProduto = dsProduto;
    } //-- void setDsProduto(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoManutencao'.
     * 
     * @param dsTipoManutencao the value of field 'dsTipoManutencao'
     */
    public void setDsTipoManutencao(java.lang.String dsTipoManutencao)
    {
        this._dsTipoManutencao = dsTipoManutencao;
    } //-- void setDsTipoManutencao(java.lang.String) 

    /**
     * Sets the value of field 'dtManutencao'.
     * 
     * @param dtManutencao the value of field 'dtManutencao'.
     */
    public void setDtManutencao(java.lang.String dtManutencao)
    {
        this._dtManutencao = dtManutencao;
    } //-- void setDtManutencao(java.lang.String) 

    /**
     * Sets the value of field 'hrInclusaoRegistroHistorico'.
     * 
     * @param hrInclusaoRegistroHistorico the value of field
     * 'hrInclusaoRegistroHistorico'.
     */
    public void setHrInclusaoRegistroHistorico(java.lang.String hrInclusaoRegistroHistorico)
    {
        this._hrInclusaoRegistroHistorico = hrInclusaoRegistroHistorico;
    } //-- void setHrInclusaoRegistroHistorico(java.lang.String) 

    /**
     * Sets the value of field 'hrManutencao'.
     * 
     * @param hrManutencao the value of field 'hrManutencao'.
     */
    public void setHrManutencao(java.lang.String hrManutencao)
    {
        this._hrManutencao = hrManutencao;
    } //-- void setHrManutencao(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listarconmanservico.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listarconmanservico.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listarconmanservico.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarconmanservico.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
