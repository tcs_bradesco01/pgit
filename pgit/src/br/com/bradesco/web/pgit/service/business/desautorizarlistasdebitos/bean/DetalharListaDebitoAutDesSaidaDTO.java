/*
 * Nome: br.com.bradesco.web.pgit.service.business.desautorizarlistasdebitos.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.desautorizarlistasdebitos.bean;

import java.math.BigDecimal;

/**
 * Nome: DetalharListaDebitoAutDesSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class DetalharListaDebitoAutDesSaidaDTO {
    
    /** Atributo codMensagem. */
    private String codMensagem;
    
    /** Atributo mensagem. */
    private String mensagem;
    
    /** Atributo cdPessoaJuridicaLista. */
    private Long cdPessoaJuridicaLista;
    
    /** Atributo cdTipoContratoLista. */
    private Integer cdTipoContratoLista;
    
    /** Atributo nrSequenciaContratoLista. */
    private Long nrSequenciaContratoLista;
    
    /** Atributo cdTipoCanalLista. */
    private Integer cdTipoCanalLista;
    
    /** Atributo cdListaDebitoPagamento. */
    private Long cdListaDebitoPagamento;
    
    /** Atributo cdPessoaJuridicaContrato. */
    private Long cdPessoaJuridicaContrato;
    
    /** Atributo cdTipoContratoNegocio. */
    private Integer cdTipoContratoNegocio;
    
    /** Atributo nrSequenciaContratoNegoci. */
    private Long nrSequenciaContratoNegoci;
    
    /** Atributo cdTipoCanal. */
    private Integer cdTipoCanal;
    
    /** Atributo cdControlePagamento. */
    private String cdControlePagamento;
    
    /** Atributo vlPagamento. */
    private BigDecimal vlPagamento;
    
    /** Atributo cdCpfCnpj. */
    private Long cdCpfCnpj;
    
    /** Atributo dsPessoaComplemento. */
    private String dsPessoaComplemento;
    
    /** Atributo cdBanco. */
    private Integer cdBanco;
    
    /** Atributo dsBanco. */
    private String dsBanco;
    
    /** Atributo cdAgencia. */
    private Integer cdAgencia;
    
    /** Atributo cdDigitoAgencia. */
    private Integer cdDigitoAgencia;
    
    /** Atributo dsAgencia. */
    private String dsAgencia;
    
    /** Atributo cdConta. */
    private Long cdConta;
    
    /** Atributo cdDigitoConta. */
    private String cdDigitoConta;
    
    /** Atributo cdSituacaoOperacaoPagamento. */
    private Integer cdSituacaoOperacaoPagamento;
    
    /** Atributo dsAutorizacaoListaDebito. */
    private String dsAutorizacaoListaDebito;
    
    /** Atributo dsSituacaoPagamento. */
    private String dsSituacaoPagamento;
    
    /** Atributo check. */
    private boolean check = false;   
    
    /** Atributo favorecidoFormatado. */
    private String favorecidoFormatado;
    
    /** Atributo contaDebitoFormatado. */
    private String contaDebitoFormatado;
    
	/**
	 * Get: contaDebitoFormatado.
	 *
	 * @return contaDebitoFormatado
	 */
	public String getContaDebitoFormatado() {
		return contaDebitoFormatado;
	}
	
	/**
	 * Set: contaDebitoFormatado.
	 *
	 * @param contaDebitoFormatado the conta debito formatado
	 */
	public void setContaDebitoFormatado(String contaDebitoFormatado) {
		this.contaDebitoFormatado = contaDebitoFormatado;
	}
	
	/**
	 * Get: favorecidoFormatado.
	 *
	 * @return favorecidoFormatado
	 */
	public String getFavorecidoFormatado() {
		return favorecidoFormatado;
	}
	
	/**
	 * Set: favorecidoFormatado.
	 *
	 * @param favorecidoFormatado the favorecido formatado
	 */
	public void setFavorecidoFormatado(String favorecidoFormatado) {
		this.favorecidoFormatado = favorecidoFormatado;
	}
	
	/**
	 * Is check.
	 *
	 * @return true, if is check
	 */
	public boolean isCheck() {
		return check;
	}
	
	/**
	 * Set: check.
	 *
	 * @param check the check
	 */
	public void setCheck(boolean check) {
		this.check = check;
	}
	
	/**
	 * Get: cdAgencia.
	 *
	 * @return cdAgencia
	 */
	public Integer getCdAgencia() {
		return cdAgencia;
	}
	
	/**
	 * Set: cdAgencia.
	 *
	 * @param cdAgencia the cd agencia
	 */
	public void setCdAgencia(Integer cdAgencia) {
		this.cdAgencia = cdAgencia;
	}
	
	/**
	 * Get: cdBanco.
	 *
	 * @return cdBanco
	 */
	public Integer getCdBanco() {
		return cdBanco;
	}
	
	/**
	 * Set: cdBanco.
	 *
	 * @param cdBanco the cd banco
	 */
	public void setCdBanco(Integer cdBanco) {
		this.cdBanco = cdBanco;
	}
	
	/**
	 * Get: cdConta.
	 *
	 * @return cdConta
	 */
	public Long getCdConta() {
		return cdConta;
	}
	
	/**
	 * Set: cdConta.
	 *
	 * @param cdConta the cd conta
	 */
	public void setCdConta(Long cdConta) {
		this.cdConta = cdConta;
	}
	
	/**
	 * Get: cdControlePagamento.
	 *
	 * @return cdControlePagamento
	 */
	public String getCdControlePagamento() {
		return cdControlePagamento;
	}
	
	/**
	 * Set: cdControlePagamento.
	 *
	 * @param cdControlePagamento the cd controle pagamento
	 */
	public void setCdControlePagamento(String cdControlePagamento) {
		this.cdControlePagamento = cdControlePagamento;
	}
	
	/**
	 * Get: cdCpfCnpj.
	 *
	 * @return cdCpfCnpj
	 */
	public Long getCdCpfCnpj() {
		return cdCpfCnpj;
	}
	
	/**
	 * Set: cdCpfCnpj.
	 *
	 * @param cdCpfCnpj the cd cpf cnpj
	 */
	public void setCdCpfCnpj(Long cdCpfCnpj) {
		this.cdCpfCnpj = cdCpfCnpj;
	}
	
	/**
	 * Get: cdDigitoAgencia.
	 *
	 * @return cdDigitoAgencia
	 */
	public Integer getCdDigitoAgencia() {
		return cdDigitoAgencia;
	}
	
	/**
	 * Set: cdDigitoAgencia.
	 *
	 * @param cdDigitoAgencia the cd digito agencia
	 */
	public void setCdDigitoAgencia(Integer cdDigitoAgencia) {
		this.cdDigitoAgencia = cdDigitoAgencia;
	}
	
	/**
	 * Get: cdDigitoConta.
	 *
	 * @return cdDigitoConta
	 */
	public String getCdDigitoConta() {
		return cdDigitoConta;
	}
	
	/**
	 * Set: cdDigitoConta.
	 *
	 * @param cdDigitoConta the cd digito conta
	 */
	public void setCdDigitoConta(String cdDigitoConta) {
		this.cdDigitoConta = cdDigitoConta;
	}
	
	/**
	 * Get: cdListaDebitoPagamento.
	 *
	 * @return cdListaDebitoPagamento
	 */
	public Long getCdListaDebitoPagamento() {
		return cdListaDebitoPagamento;
	}
	
	/**
	 * Set: cdListaDebitoPagamento.
	 *
	 * @param cdListaDebitoPagamento the cd lista debito pagamento
	 */
	public void setCdListaDebitoPagamento(Long cdListaDebitoPagamento) {
		this.cdListaDebitoPagamento = cdListaDebitoPagamento;
	}
	
	/**
	 * Get: cdPessoaJuridicaContrato.
	 *
	 * @return cdPessoaJuridicaContrato
	 */
	public Long getCdPessoaJuridicaContrato() {
		return cdPessoaJuridicaContrato;
	}
	
	/**
	 * Set: cdPessoaJuridicaContrato.
	 *
	 * @param cdPessoaJuridicaContrato the cd pessoa juridica contrato
	 */
	public void setCdPessoaJuridicaContrato(Long cdPessoaJuridicaContrato) {
		this.cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
	}
	
	/**
	 * Get: cdPessoaJuridicaLista.
	 *
	 * @return cdPessoaJuridicaLista
	 */
	public Long getCdPessoaJuridicaLista() {
		return cdPessoaJuridicaLista;
	}
	
	/**
	 * Set: cdPessoaJuridicaLista.
	 *
	 * @param cdPessoaJuridicaLista the cd pessoa juridica lista
	 */
	public void setCdPessoaJuridicaLista(Long cdPessoaJuridicaLista) {
		this.cdPessoaJuridicaLista = cdPessoaJuridicaLista;
	}
	
	/**
	 * Get: cdSituacaoOperacaoPagamento.
	 *
	 * @return cdSituacaoOperacaoPagamento
	 */
	public Integer getCdSituacaoOperacaoPagamento() {
		return cdSituacaoOperacaoPagamento;
	}
	
	/**
	 * Set: cdSituacaoOperacaoPagamento.
	 *
	 * @param cdSituacaoOperacaoPagamento the cd situacao operacao pagamento
	 */
	public void setCdSituacaoOperacaoPagamento(Integer cdSituacaoOperacaoPagamento) {
		this.cdSituacaoOperacaoPagamento = cdSituacaoOperacaoPagamento;
	}
	
	/**
	 * Get: cdTipoCanal.
	 *
	 * @return cdTipoCanal
	 */
	public Integer getCdTipoCanal() {
		return cdTipoCanal;
	}
	
	/**
	 * Set: cdTipoCanal.
	 *
	 * @param cdTipoCanal the cd tipo canal
	 */
	public void setCdTipoCanal(Integer cdTipoCanal) {
		this.cdTipoCanal = cdTipoCanal;
	}
	
	/**
	 * Get: cdTipoCanalLista.
	 *
	 * @return cdTipoCanalLista
	 */
	public Integer getCdTipoCanalLista() {
		return cdTipoCanalLista;
	}
	
	/**
	 * Set: cdTipoCanalLista.
	 *
	 * @param cdTipoCanalLista the cd tipo canal lista
	 */
	public void setCdTipoCanalLista(Integer cdTipoCanalLista) {
		this.cdTipoCanalLista = cdTipoCanalLista;
	}
	
	/**
	 * Get: cdTipoContratoLista.
	 *
	 * @return cdTipoContratoLista
	 */
	public Integer getCdTipoContratoLista() {
		return cdTipoContratoLista;
	}
	
	/**
	 * Set: cdTipoContratoLista.
	 *
	 * @param cdTipoContratoLista the cd tipo contrato lista
	 */
	public void setCdTipoContratoLista(Integer cdTipoContratoLista) {
		this.cdTipoContratoLista = cdTipoContratoLista;
	}
	
	/**
	 * Get: cdTipoContratoNegocio.
	 *
	 * @return cdTipoContratoNegocio
	 */
	public Integer getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}
	
	/**
	 * Set: cdTipoContratoNegocio.
	 *
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}
	
	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}
	
	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}
	
	/**
	 * Get: dsAgencia.
	 *
	 * @return dsAgencia
	 */
	public String getDsAgencia() {
		return dsAgencia;
	}
	
	/**
	 * Set: dsAgencia.
	 *
	 * @param dsAgencia the ds agencia
	 */
	public void setDsAgencia(String dsAgencia) {
		this.dsAgencia = dsAgencia;
	}
	
	/**
	 * Get: dsBanco.
	 *
	 * @return dsBanco
	 */
	public String getDsBanco() {
		return dsBanco;
	}
	
	/**
	 * Set: dsBanco.
	 *
	 * @param dsBanco the ds banco
	 */
	public void setDsBanco(String dsBanco) {
		this.dsBanco = dsBanco;
	}
	
	/**
	 * Get: dsPessoaComplemento.
	 *
	 * @return dsPessoaComplemento
	 */
	public String getDsPessoaComplemento() {
		return dsPessoaComplemento;
	}
	
	/**
	 * Set: dsPessoaComplemento.
	 *
	 * @param dsPessoaComplemento the ds pessoa complemento
	 */
	public void setDsPessoaComplemento(String dsPessoaComplemento) {
		this.dsPessoaComplemento = dsPessoaComplemento;
	}
	
	/**
	 * Get: dsSituacaoPagamento.
	 *
	 * @return dsSituacaoPagamento
	 */
	public String getDsSituacaoPagamento() {
		return dsSituacaoPagamento;
	}
	
	/**
	 * Set: dsSituacaoPagamento.
	 *
	 * @param dsSituacaoPagamento the ds situacao pagamento
	 */
	public void setDsSituacaoPagamento(String dsSituacaoPagamento) {
		this.dsSituacaoPagamento = dsSituacaoPagamento;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	/**
	 * Get: nrSequenciaContratoLista.
	 *
	 * @return nrSequenciaContratoLista
	 */
	public Long getNrSequenciaContratoLista() {
		return nrSequenciaContratoLista;
	}
	
	/**
	 * Set: nrSequenciaContratoLista.
	 *
	 * @param nrSequenciaContratoLista the nr sequencia contrato lista
	 */
	public void setNrSequenciaContratoLista(Long nrSequenciaContratoLista) {
		this.nrSequenciaContratoLista = nrSequenciaContratoLista;
	}
	
	/**
	 * Get: nrSequenciaContratoNegoci.
	 *
	 * @return nrSequenciaContratoNegoci
	 */
	public Long getNrSequenciaContratoNegoci() {
		return nrSequenciaContratoNegoci;
	}
	
	/**
	 * Set: nrSequenciaContratoNegoci.
	 *
	 * @param nrSequenciaContratoNegoci the nr sequencia contrato negoci
	 */
	public void setNrSequenciaContratoNegoci(Long nrSequenciaContratoNegoci) {
		this.nrSequenciaContratoNegoci = nrSequenciaContratoNegoci;
	}
	
	/**
	 * Get: vlPagamento.
	 *
	 * @return vlPagamento
	 */
	public BigDecimal getVlPagamento() {
		return vlPagamento;
	}
	
	/**
	 * Set: vlPagamento.
	 *
	 * @param vlPagamento the vl pagamento
	 */
	public void setVlPagamento(BigDecimal vlPagamento) {
		this.vlPagamento = vlPagamento;
	}
	
	/**
	 * Get: dsAutorizacaoListaDebito.
	 *
	 * @return dsAutorizacaoListaDebito
	 */
	public String getDsAutorizacaoListaDebito() {
		if(dsAutorizacaoListaDebito == null){
			dsAutorizacaoListaDebito = "";
		}
		return dsAutorizacaoListaDebito.toUpperCase();
	}
	
	/**
	 * Set: dsAutorizacaoListaDebito.
	 *
	 * @param dsAutorizacaoListaDebito the ds autorizacao lista debito
	 */
	public void setDsAutorizacaoListaDebito(String dsAutorizacaoListaDebito) {
		this.dsAutorizacaoListaDebito = dsAutorizacaoListaDebito;
	}
 
	
}
