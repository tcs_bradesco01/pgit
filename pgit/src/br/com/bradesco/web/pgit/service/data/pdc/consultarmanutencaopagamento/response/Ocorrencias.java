/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultarmanutencaopagamento.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _dtHoraManutencao
     */
    private java.lang.String _dtHoraManutencao;

    /**
     * Field _cdTipoManutencao
     */
    private int _cdTipoManutencao = 0;

    /**
     * keeps track of state for field: _cdTipoManutencao
     */
    private boolean _has_cdTipoManutencao;

    /**
     * Field _dsTipoManutencao
     */
    private java.lang.String _dsTipoManutencao;

    /**
     * Field _cdTipoCanalLista
     */
    private int _cdTipoCanalLista = 0;

    /**
     * keeps track of state for field: _cdTipoCanalLista
     */
    private boolean _has_cdTipoCanalLista;

    /**
     * Field _dsTipoCanalLista
     */
    private java.lang.String _dsTipoCanalLista;

    /**
     * Field _cdUsuarioManutencao
     */
    private java.lang.String _cdUsuarioManutencao;

    /**
     * Field _dsUsuarioManutencao
     */
    private java.lang.String _dsUsuarioManutencao;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarmanutencaopagamento.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdTipoCanalLista
     * 
     */
    public void deleteCdTipoCanalLista()
    {
        this._has_cdTipoCanalLista= false;
    } //-- void deleteCdTipoCanalLista() 

    /**
     * Method deleteCdTipoManutencao
     * 
     */
    public void deleteCdTipoManutencao()
    {
        this._has_cdTipoManutencao= false;
    } //-- void deleteCdTipoManutencao() 

    /**
     * Returns the value of field 'cdTipoCanalLista'.
     * 
     * @return int
     * @return the value of field 'cdTipoCanalLista'.
     */
    public int getCdTipoCanalLista()
    {
        return this._cdTipoCanalLista;
    } //-- int getCdTipoCanalLista() 

    /**
     * Returns the value of field 'cdTipoManutencao'.
     * 
     * @return int
     * @return the value of field 'cdTipoManutencao'.
     */
    public int getCdTipoManutencao()
    {
        return this._cdTipoManutencao;
    } //-- int getCdTipoManutencao() 

    /**
     * Returns the value of field 'cdUsuarioManutencao'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioManutencao'.
     */
    public java.lang.String getCdUsuarioManutencao()
    {
        return this._cdUsuarioManutencao;
    } //-- java.lang.String getCdUsuarioManutencao() 

    /**
     * Returns the value of field 'dsTipoCanalLista'.
     * 
     * @return String
     * @return the value of field 'dsTipoCanalLista'.
     */
    public java.lang.String getDsTipoCanalLista()
    {
        return this._dsTipoCanalLista;
    } //-- java.lang.String getDsTipoCanalLista() 

    /**
     * Returns the value of field 'dsTipoManutencao'.
     * 
     * @return String
     * @return the value of field 'dsTipoManutencao'.
     */
    public java.lang.String getDsTipoManutencao()
    {
        return this._dsTipoManutencao;
    } //-- java.lang.String getDsTipoManutencao() 

    /**
     * Returns the value of field 'dsUsuarioManutencao'.
     * 
     * @return String
     * @return the value of field 'dsUsuarioManutencao'.
     */
    public java.lang.String getDsUsuarioManutencao()
    {
        return this._dsUsuarioManutencao;
    } //-- java.lang.String getDsUsuarioManutencao() 

    /**
     * Returns the value of field 'dtHoraManutencao'.
     * 
     * @return String
     * @return the value of field 'dtHoraManutencao'.
     */
    public java.lang.String getDtHoraManutencao()
    {
        return this._dtHoraManutencao;
    } //-- java.lang.String getDtHoraManutencao() 

    /**
     * Method hasCdTipoCanalLista
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoCanalLista()
    {
        return this._has_cdTipoCanalLista;
    } //-- boolean hasCdTipoCanalLista() 

    /**
     * Method hasCdTipoManutencao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoManutencao()
    {
        return this._has_cdTipoManutencao;
    } //-- boolean hasCdTipoManutencao() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdTipoCanalLista'.
     * 
     * @param cdTipoCanalLista the value of field 'cdTipoCanalLista'
     */
    public void setCdTipoCanalLista(int cdTipoCanalLista)
    {
        this._cdTipoCanalLista = cdTipoCanalLista;
        this._has_cdTipoCanalLista = true;
    } //-- void setCdTipoCanalLista(int) 

    /**
     * Sets the value of field 'cdTipoManutencao'.
     * 
     * @param cdTipoManutencao the value of field 'cdTipoManutencao'
     */
    public void setCdTipoManutencao(int cdTipoManutencao)
    {
        this._cdTipoManutencao = cdTipoManutencao;
        this._has_cdTipoManutencao = true;
    } //-- void setCdTipoManutencao(int) 

    /**
     * Sets the value of field 'cdUsuarioManutencao'.
     * 
     * @param cdUsuarioManutencao the value of field
     * 'cdUsuarioManutencao'.
     */
    public void setCdUsuarioManutencao(java.lang.String cdUsuarioManutencao)
    {
        this._cdUsuarioManutencao = cdUsuarioManutencao;
    } //-- void setCdUsuarioManutencao(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoCanalLista'.
     * 
     * @param dsTipoCanalLista the value of field 'dsTipoCanalLista'
     */
    public void setDsTipoCanalLista(java.lang.String dsTipoCanalLista)
    {
        this._dsTipoCanalLista = dsTipoCanalLista;
    } //-- void setDsTipoCanalLista(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoManutencao'.
     * 
     * @param dsTipoManutencao the value of field 'dsTipoManutencao'
     */
    public void setDsTipoManutencao(java.lang.String dsTipoManutencao)
    {
        this._dsTipoManutencao = dsTipoManutencao;
    } //-- void setDsTipoManutencao(java.lang.String) 

    /**
     * Sets the value of field 'dsUsuarioManutencao'.
     * 
     * @param dsUsuarioManutencao the value of field
     * 'dsUsuarioManutencao'.
     */
    public void setDsUsuarioManutencao(java.lang.String dsUsuarioManutencao)
    {
        this._dsUsuarioManutencao = dsUsuarioManutencao;
    } //-- void setDsUsuarioManutencao(java.lang.String) 

    /**
     * Sets the value of field 'dtHoraManutencao'.
     * 
     * @param dtHoraManutencao the value of field 'dtHoraManutencao'
     */
    public void setDtHoraManutencao(java.lang.String dtHoraManutencao)
    {
        this._dtHoraManutencao = dtHoraManutencao;
    } //-- void setDtHoraManutencao(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultarmanutencaopagamento.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultarmanutencaopagamento.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultarmanutencaopagamento.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarmanutencaopagamento.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
