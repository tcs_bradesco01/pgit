/*
 * Nome: br.com.bradesco.web.pgit.service.provider.baseintranet.impl
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.provider.baseintranet.impl;

import br.com.bradesco.web.aq.application.log.ILogManager;
import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterException;
import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcConnectorException;
import br.com.bradesco.web.aq.application.security.intranet.service.IAuthenticationService;
import br.com.bradesco.web.aq.application.security.intranet.service.exception.AuthenticationServiceException;
import br.com.bradesco.web.pgit.service.data.pdc.FactoryAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.authenticationservice.request.LogonRequest;
import br.com.bradesco.web.pgit.service.provider.baseintranet.exception.AlterarSenhaException;

/**
 * Nome: AuthenticationServiceImpl
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class AuthenticationServiceImpl implements IAuthenticationService {

	/**
	 * Cont�m a inst�ncia do logManager
	 */
	private ILogManager logManager = null;

	/**
	 * Factory do Adaptador de seguran�a.
	 */
	private FactoryAdapter factoryAdapter = null;

	/**
	 * @see br.com.bradesco.web.aq.application.security.intranet.service.IAuthenticationService#authenticate(java.lang.String,
	 *      java.lang.String)
	 */
	public void authenticate(String userName, String password)
			throws AuthenticationServiceException {
		if (userName == null || userName.trim().equals("")) {
			throw new AuthenticationServiceException("usuario.nulo",
					"O usu�rio n�o pode ser nulo.");
		}
		if (password == null || password.trim().equals("")) {
			throw new AuthenticationServiceException("senha.nulo",
					"A senha n�o pode ser nula.");
		}
		try {
			LogonRequest request = new LogonRequest();
			request.setIdUsuario(userName);
			request.setSenhaAtual(password);
			request.setSenhaNova("");
			request.setSenhaConfirmacao("");
			request.setAtualizarSenha("N");
			getFactoryAdapter().getAuthenticationServicePDCAdapter().invokeProcess(request);
		} catch (PdcConnectorException e) {
			throw new AuthenticationServiceException("pdc.conection.exception",
					"Erro de conex�o com o PDC.", e);
		} catch (PdcAdapterException e) {
			throw new AuthenticationServiceException("pdc.general.exception",
					"Erro ao executar o processo PDC.", e);
		}
	}

	/**
	 * @return the factoryAdapter
	 */
	protected FactoryAdapter getFactoryAdapter() {
		return factoryAdapter;
	}

	/**
	 * @param factoryAdapter
	 *            the factoryAdapter to set
	 */
	public void setFactoryAdapter(FactoryAdapter factoryAdapter) {
		this.factoryAdapter = factoryAdapter;
	}

	/**
	 * @return the logManager
	 */
	public ILogManager getLogManager() {
		return logManager;
	}

	/**
	 * @param logManager
	 *            the logManager to set
	 */
	public void setLogManager(ILogManager logManager) {
		this.logManager = logManager;
	}

	/**
	 * @see br.com.bradesco.web.piloto.service.provider.intranet.IAlterarSenhaService#trocarSenha(java.lang.String,
	 *      java.lang.String, java.lang.String, java.lang.String)
	 */
	public void trocarSenha(String usuario, String senhaAtual,
			String novaSenha, String confirmNovaSenha)
			throws AlterarSenhaException {
		if (usuario == null || usuario.trim().equals("")) {
			throw new AlterarSenhaException("usuario.nulo",
					"O usu�rio n�o pode ser nulo.");
		}
		if (senhaAtual == null || senhaAtual.trim().equals("")) {
			throw new AlterarSenhaException("senha.nulo",
					"A senha n�o pode ser nula.");
		}
		if (novaSenha == null || novaSenha.trim().equals("")) {
			throw new AlterarSenhaException("novaSenha.nulo",
					"O usu�rio n�o pode ser nulo.");
		}
		if (confirmNovaSenha == null || confirmNovaSenha.trim().equals("")) {
			throw new AlterarSenhaException("confirmNovaSenha.nulo",
					"A senha n�o pode ser nula.");
		}
		try {
			LogonRequest request = new LogonRequest();
			request.setIdUsuario(usuario);
			request.setSenhaAtual(senhaAtual);
			request.setSenhaNova(novaSenha);
			request.setSenhaConfirmacao(confirmNovaSenha);
			request.setAtualizarSenha("S");
			getFactoryAdapter().getAuthenticationServicePDCAdapter().invokeProcess(
					request);
		} catch (PdcConnectorException e) {
			throw new AlterarSenhaException("pdc.conection.exception", e,
					"Erro de conex�o com o PDC.");
		} catch (PdcAdapterException e) {
			throw new AlterarSenhaException("pdc.general.exception", e,
					"Erro ao executar o processo PDC.");
		}
	}

}
