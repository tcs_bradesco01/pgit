/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listarsolicitacaorelatoriofavorecidos.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ListarSolicitacaoRelatorioFavorecidosRequest.
 * 
 * @version $Revision$ $Date$
 */
public class ListarSolicitacaoRelatorioFavorecidosRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _numeroOcorrencias
     */
    private int _numeroOcorrencias = 0;

    /**
     * keeps track of state for field: _numeroOcorrencias
     */
    private boolean _has_numeroOcorrencias;

    /**
     * Field _tpSolicitacao
     */
    private int _tpSolicitacao = 0;

    /**
     * keeps track of state for field: _tpSolicitacao
     */
    private boolean _has_tpSolicitacao;

    /**
     * Field _dtInicioSolicitacao
     */
    private java.lang.String _dtInicioSolicitacao;

    /**
     * Field _dtFimSolicitacao
     */
    private java.lang.String _dtFimSolicitacao;


      //----------------/
     //- Constructors -/
    //----------------/

    public ListarSolicitacaoRelatorioFavorecidosRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarsolicitacaorelatoriofavorecidos.request.ListarSolicitacaoRelatorioFavorecidosRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteNumeroOcorrencias
     * 
     */
    public void deleteNumeroOcorrencias()
    {
        this._has_numeroOcorrencias= false;
    } //-- void deleteNumeroOcorrencias() 

    /**
     * Method deleteTpSolicitacao
     * 
     */
    public void deleteTpSolicitacao()
    {
        this._has_tpSolicitacao= false;
    } //-- void deleteTpSolicitacao() 

    /**
     * Returns the value of field 'dtFimSolicitacao'.
     * 
     * @return String
     * @return the value of field 'dtFimSolicitacao'.
     */
    public java.lang.String getDtFimSolicitacao()
    {
        return this._dtFimSolicitacao;
    } //-- java.lang.String getDtFimSolicitacao() 

    /**
     * Returns the value of field 'dtInicioSolicitacao'.
     * 
     * @return String
     * @return the value of field 'dtInicioSolicitacao'.
     */
    public java.lang.String getDtInicioSolicitacao()
    {
        return this._dtInicioSolicitacao;
    } //-- java.lang.String getDtInicioSolicitacao() 

    /**
     * Returns the value of field 'numeroOcorrencias'.
     * 
     * @return int
     * @return the value of field 'numeroOcorrencias'.
     */
    public int getNumeroOcorrencias()
    {
        return this._numeroOcorrencias;
    } //-- int getNumeroOcorrencias() 

    /**
     * Returns the value of field 'tpSolicitacao'.
     * 
     * @return int
     * @return the value of field 'tpSolicitacao'.
     */
    public int getTpSolicitacao()
    {
        return this._tpSolicitacao;
    } //-- int getTpSolicitacao() 

    /**
     * Method hasNumeroOcorrencias
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNumeroOcorrencias()
    {
        return this._has_numeroOcorrencias;
    } //-- boolean hasNumeroOcorrencias() 

    /**
     * Method hasTpSolicitacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasTpSolicitacao()
    {
        return this._has_tpSolicitacao;
    } //-- boolean hasTpSolicitacao() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'dtFimSolicitacao'.
     * 
     * @param dtFimSolicitacao the value of field 'dtFimSolicitacao'
     */
    public void setDtFimSolicitacao(java.lang.String dtFimSolicitacao)
    {
        this._dtFimSolicitacao = dtFimSolicitacao;
    } //-- void setDtFimSolicitacao(java.lang.String) 

    /**
     * Sets the value of field 'dtInicioSolicitacao'.
     * 
     * @param dtInicioSolicitacao the value of field
     * 'dtInicioSolicitacao'.
     */
    public void setDtInicioSolicitacao(java.lang.String dtInicioSolicitacao)
    {
        this._dtInicioSolicitacao = dtInicioSolicitacao;
    } //-- void setDtInicioSolicitacao(java.lang.String) 

    /**
     * Sets the value of field 'numeroOcorrencias'.
     * 
     * @param numeroOcorrencias the value of field
     * 'numeroOcorrencias'.
     */
    public void setNumeroOcorrencias(int numeroOcorrencias)
    {
        this._numeroOcorrencias = numeroOcorrencias;
        this._has_numeroOcorrencias = true;
    } //-- void setNumeroOcorrencias(int) 

    /**
     * Sets the value of field 'tpSolicitacao'.
     * 
     * @param tpSolicitacao the value of field 'tpSolicitacao'.
     */
    public void setTpSolicitacao(int tpSolicitacao)
    {
        this._tpSolicitacao = tpSolicitacao;
        this._has_tpSolicitacao = true;
    } //-- void setTpSolicitacao(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ListarSolicitacaoRelatorioFavorecidosRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listarsolicitacaorelatoriofavorecidos.request.ListarSolicitacaoRelatorioFavorecidosRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listarsolicitacaorelatoriofavorecidos.request.ListarSolicitacaoRelatorioFavorecidosRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listarsolicitacaorelatoriofavorecidos.request.ListarSolicitacaoRelatorioFavorecidosRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarsolicitacaorelatoriofavorecidos.request.ListarSolicitacaoRelatorioFavorecidosRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
