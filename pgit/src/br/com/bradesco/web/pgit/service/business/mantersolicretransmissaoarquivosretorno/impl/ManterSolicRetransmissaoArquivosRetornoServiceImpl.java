/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/implementation.ftl,v $
 * $Id: implementation.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: implementation.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */
 
package br.com.bradesco.web.pgit.service.business.mantersolicretransmissaoarquivosretorno.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import br.com.bradesco.web.pgit.service.business.mantersolicretransmissaoarquivosretorno.IManterSolicRetransmissaoArquivosRetornoService;
import br.com.bradesco.web.pgit.service.business.mantersolicretransmissaoarquivosretorno.IManterSolicRetransmissaoArquivosRetornoServiceConstants;
import br.com.bradesco.web.pgit.service.business.mantersolicretransmissaoarquivosretorno.bean.DetalharSolBaseRetransmissaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicretransmissaoarquivosretorno.bean.DetalharSolBaseRetransmissaoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicretransmissaoarquivosretorno.bean.ExcluirSolBaseRetransmissaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicretransmissaoarquivosretorno.bean.ExcluirSolBaseRetransmissaoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicretransmissaoarquivosretorno.bean.IncluirSolBaseRetransmissaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicretransmissaoarquivosretorno.bean.IncluirSolBaseRetransmissaoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicretransmissaoarquivosretorno.bean.ListarArqRetransmissaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicretransmissaoarquivosretorno.bean.ListarArqRetransmissaoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicretransmissaoarquivosretorno.bean.ListarArquivoRetornoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicretransmissaoarquivosretorno.bean.ListarArquivoRetornoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicretransmissaoarquivosretorno.bean.ListarSolBaseRetransmissaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicretransmissaoarquivosretorno.bean.ListarSolBaseRetransmissaoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicretransmissaoarquivosretorno.bean.OcorrenciasDetalharSolBaseRetransmissaoSaidaDTO;
import br.com.bradesco.web.pgit.service.data.pdc.FactoryAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.detalharsolbaseretransmissao.request.DetalharSolBaseRetransmissaoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.detalharsolbaseretransmissao.response.DetalharSolBaseRetransmissaoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.excluirsolbaseretransmissao.request.ExcluirSolBaseRetransmissaoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.excluirsolbaseretransmissao.response.ExcluirSolBaseRetransmissaoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.incluirsolbaseretransmissao.request.IncluirSolBaseRetransmissaoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.incluirsolbaseretransmissao.response.IncluirSolBaseRetransmissaoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listararqretransmissao.request.ListarArqRetransmissaoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listararqretransmissao.response.ListarArqRetransmissaoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listararquivoretorno.request.ListarArquivoRetornoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listararquivoretorno.response.ListarArquivoRetornoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarsolbaseretransmissao.request.ListarSolBaseRetransmissaoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarsolbaseretransmissao.response.ListarSolBaseRetransmissaoResponse;
import br.com.bradesco.web.pgit.utils.PgitUtil;
import br.com.bradesco.web.pgit.view.converters.FormatarData;


/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Implementa��o do adaptador: ManterSolicRetransmissaoArquivosRetorno
 * </p>
 * 
 * @comment C�DIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public class ManterSolicRetransmissaoArquivosRetornoServiceImpl implements IManterSolicRetransmissaoArquivosRetornoService {
	
	/** Atributo nf. */
	private java.text.NumberFormat nf = java.text.NumberFormat.getInstance( new Locale( "pt_br" ) );
	
	/** Atributo factoryAdapter. */
	private FactoryAdapter factoryAdapter;		
	
    /**
     * Get: factoryAdapter.
     *
     * @return factoryAdapter
     */
    public FactoryAdapter getFactoryAdapter() {
		return factoryAdapter;
	}

	/**
	 * Set: factoryAdapter.
	 *
	 * @param factoryAdapter the factory adapter
	 */
	public void setFactoryAdapter(FactoryAdapter factoryAdapter) {
		this.factoryAdapter = factoryAdapter;
	}

	/**
     * Construtor.
     */
    public ManterSolicRetransmissaoArquivosRetornoServiceImpl() {
    }
    
    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.mantersolicretransmissaoarquivosretorno.IManterSolicRetransmissaoArquivosRetornoService#listarSolBaseRetransmissao(br.com.bradesco.web.pgit.service.business.mantersolicretransmissaoarquivosretorno.bean.ListarSolBaseRetransmissaoEntradaDTO)
     */
    public List<ListarSolBaseRetransmissaoSaidaDTO> listarSolBaseRetransmissao (ListarSolBaseRetransmissaoEntradaDTO entradaDTO){
    	List<ListarSolBaseRetransmissaoSaidaDTO> listaRetorno = new ArrayList<ListarSolBaseRetransmissaoSaidaDTO>();
    	ListarSolBaseRetransmissaoRequest request = new ListarSolBaseRetransmissaoRequest();
    	ListarSolBaseRetransmissaoResponse response = new ListarSolBaseRetransmissaoResponse();
    	    	    	
        request.setCdPessoaJuridicaContrato(PgitUtil.verificaLongNulo(entradaDTO.getCdPessoaJuridicaContrato()));
        request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoContratoNegocio()));
        request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(entradaDTO.getNrSequenciaContratoNegocio()));
        request.setDtSolicitacaoInicio(PgitUtil.verificaStringNula(entradaDTO.getDtSolicitacaoInicio()));
        request.setDtSolicitacaoFim(PgitUtil.verificaStringNula(entradaDTO.getDtSolicitacaoFim()));
        request.setNrSolicitacaoPagamento(PgitUtil.verificaIntegerNulo(entradaDTO.getNrSolicitacaoPagamento()));
        request.setCdSituacaoSolicitacao(PgitUtil.verificaIntegerNulo(entradaDTO.getCdSituacaoSolicitacao()));
        request.setCdOrigemSolicitacao(PgitUtil.verificaIntegerNulo(entradaDTO.getCdOrigemSolicitacao()));        
        request.setNumeroOcorrencias(IManterSolicRetransmissaoArquivosRetornoServiceConstants.NUMERO_OCORRENCIAS_CONSULTAR);                
        
        response = getFactoryAdapter().getListarSolBaseRetransmissaoPDCAdapter().invokeProcess(request);
        
        ListarSolBaseRetransmissaoSaidaDTO saidaDTO;
        for(int i = 0;i<response.getOcorrenciasCount();i++){
        	saidaDTO = new ListarSolBaseRetransmissaoSaidaDTO();
        	
        	saidaDTO.setCodMensagem(response.getCodMensagem());
        	saidaDTO.setMensagem(response.getMensagem());        	
        	saidaDTO.setQtdeRegistros(response.getQtdeRegistros());
        	saidaDTO.setNrSolicitacaoPagamento(response.getOcorrencias(i).getNrSolicitacaoPagamento());
        	saidaDTO.setHrSolicitacaoSolicitacao(response.getOcorrencias(i).getHrSolicitacaoSolicitacao());
        	saidaDTO.setHrSolicitacaoSolicitacaoFormatada(FormatarData.formatarDataTrilha(response.getOcorrencias(i).getHrSolicitacaoSolicitacao()));
        	saidaDTO.setCdSituacaoSolicitacao(response.getOcorrencias(i).getCdSituacaoSolicitacao());
        	saidaDTO.setDsSituacaoSolicitacao(response.getOcorrencias(i).getDsSituacaoSolicitacao());
        	saidaDTO.setResultadoProcessamento(response.getOcorrencias(i).getResultadoProcessamento());
        	saidaDTO.setHrAtendimentoSolicitacao(response.getOcorrencias(i).getHrAtendimentoSolicitacao());
        	saidaDTO.setHrAtendimentoSolicitacaoFormatada(FormatarData.formatarDataTrilha(response.getOcorrencias(i).getHrAtendimentoSolicitacao()));
        	saidaDTO.setQtdeRegistroSolicitacao(response.getOcorrencias(i).getQtdeRegistroSolicitacao());
        	saidaDTO.setQtdeRegistroSolicitacaoFormatado(nf.format(response.getOcorrencias(i).getQtdeRegistroSolicitacao()));
        				            
            listaRetorno.add(saidaDTO);
        }
    	
    	return listaRetorno;    	
    }
    
    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.mantersolicretransmissaoarquivosretorno.IManterSolicRetransmissaoArquivosRetornoService#detalharSolBaseRetransmissao(br.com.bradesco.web.pgit.service.business.mantersolicretransmissaoarquivosretorno.bean.DetalharSolBaseRetransmissaoEntradaDTO)
     */
    public DetalharSolBaseRetransmissaoSaidaDTO detalharSolBaseRetransmissao(DetalharSolBaseRetransmissaoEntradaDTO entradaDTO){
		
    	DetalharSolBaseRetransmissaoRequest request = new DetalharSolBaseRetransmissaoRequest();
    	DetalharSolBaseRetransmissaoResponse response = new DetalharSolBaseRetransmissaoResponse();		
		
		request.setNrSolicitacaoPagamento(PgitUtil.verificaIntegerNulo(entradaDTO.getNrSolicitacaoPagamento()));		
		
		DetalharSolBaseRetransmissaoSaidaDTO saidaDTO = new DetalharSolBaseRetransmissaoSaidaDTO();
		
		response = getFactoryAdapter().getDetalharSolBaseRetransmissaoPDCAdapter().invokeProcess(request);
		
		saidaDTO.setCodMensagem(response.getCodMensagem());
		saidaDTO.setMensagem(response.getMensagem());
		saidaDTO.setNrSolicitacao(response.getNrSolicitacao());
		saidaDTO.setHrSolicitacao(response.getHrSolicitacao());
		saidaDTO.setDsSituacaoRecuperacao(response.getDsSituacaoRecuperacao());
		saidaDTO.setDsMotivoSituacaoRecuperacao(response.getDsMotivoSituacaoRecuperacao());
		saidaDTO.setHrAtendimentoSolicitacao(response.getHrAtendimentoSolicitacao());
		saidaDTO.setQtdeRegistros(response.getQtdeRegistrosOcorrencias());
		saidaDTO.setQtdeRegistrosFormatada(nf.format(response.getQtdeRegistrosOcorrencias()));
		saidaDTO.setVlrTarifaPadrao(response.getVlTarifaPadrao());
		saidaDTO.setDescontoTarifa(response.getNumPercentualDescTarifa());
		saidaDTO.setVlrTarifaAtual(response.getVlrTarifaAtual());
		saidaDTO.setCdCanalInclusao(response.getCdCanalInclusao());
		saidaDTO.setDsCanalInclusao(response.getDsCanalInclusao());
		saidaDTO.setCdAutenticacaoSegurancaInclusao(response.getCdAutenticacaoSegurancaInclusao());
		saidaDTO.setNmOperacaoFluxoInclusao(response.getNmOperacaoFluxoInclusao());
		saidaDTO.setHrInclusaoRegistro(response.getHrInclusaoRegistro());
		saidaDTO.setCdCanalManutencao(response.getCdCanalManutencao());
		saidaDTO.setDsCanalManutencao(response.getDsCanalManutencao());
		saidaDTO.setCdAutenticacaoSegurancaManutencao(response.getCdAutenticacaoSegurancaManutencao());
		saidaDTO.setNmOperacaoFluxoManutencao(response.getNmOperacaoFluxoManutencao());
		saidaDTO.setHrManutencaoRegistro(response.getHrManutencaoRegistro());
		saidaDTO.setNrSolicitacao2(response.getNrSolicitacao2());
			
		saidaDTO.setHrSolicitacaoFormatada(FormatarData.formatarDataTrilha(response.getHrSolicitacao()));
		saidaDTO.setHrAtendimentoSolicitacaoFormatada(FormatarData.formatarDataTrilha(response.getHrAtendimentoSolicitacao()));
		saidaDTO.setHrInclusaoRegistroFormatada(FormatarData.formatarDataTrilha(response.getHrInclusaoRegistro()));
		saidaDTO.setHrManutencaoRegistroFormatada(FormatarData.formatarDataTrilha(response.getHrManutencaoRegistro()));
		
	    
	    List<OcorrenciasDetalharSolBaseRetransmissaoSaidaDTO> listaDetalharSolBaseRetransmissao = new ArrayList<OcorrenciasDetalharSolBaseRetransmissaoSaidaDTO>();
	    OcorrenciasDetalharSolBaseRetransmissaoSaidaDTO ocorrencia;
	    
	    for(int i=0;i<response.getOcorrenciasCount();i++){
	    	ocorrencia = new OcorrenciasDetalharSolBaseRetransmissaoSaidaDTO();
	    		    	
	    	ocorrencia.setCdPessoa(response.getOcorrencias(i).getCdPessoa());
	    	ocorrencia.setCpfCnpjPessoa(response.getOcorrencias(i).getCpfCnpjPessoa());
	    	ocorrencia.setParticipante(response.getOcorrencias(i).getCdPessoa() + "/" + response.getOcorrencias(i).getCpfCnpjPessoa());
	    	ocorrencia.setDsPessoa(response.getOcorrencias(i).getDsPessoa());
	    	ocorrencia.setCdClienteTransferenciaArquivo(response.getOcorrencias(i).getCdClienteTransferenciaArquivo());
	    	ocorrencia.setCdTipoLayoutArquivo(response.getOcorrencias(i).getCdTipoLayoutArquivo());
	    	ocorrencia.setDsTipoLayoutArquivo(response.getOcorrencias(i).getDsTipoLayoutArquivo());
	    	ocorrencia.setNrSequenciaRetorno(response.getOcorrencias(i).getNrSequenciaRetorno());
	    	ocorrencia.setCdOperacao(response.getOcorrencias(i).getCdOperacao());
	    	ocorrencia.setDsArquivoRetorno(response.getOcorrencias(i).getDsArquivoRetorno());
	    	ocorrencia.setHrInclusaoRetorno(response.getOcorrencias(i).getHrInclusaoRetorno());
	    	ocorrencia.setDsSituacaoProcessamento(response.getOcorrencias(i).getDsSituacaoProcessamento());
	    	ocorrencia.setQtdeRegistrosOcorrencias(response.getOcorrencias(i).getQtdeRegistrosOcorrencias());	    	
	    	ocorrencia.setQtdeRegistrosOcorrenciasFormatado(nf.format(response.getOcorrencias(i).getQtdeRegistrosOcorrencias()));
	    	ocorrencia.setVlrRegistros(response.getOcorrencias(i).getVlrRegistros());	    	
	    	ocorrencia.setHrInclusaoRetornoFormatada(FormatarData.formatarDataTrilha(response.getOcorrencias(i).getHrInclusaoRetorno()));	    	
	    	listaDetalharSolBaseRetransmissao.add(ocorrencia);
	    }
	    
	    saidaDTO.setListaDetalharSolBaseRetransmissao(listaDetalharSolBaseRetransmissao);
	    	    
	    return saidaDTO;
	}
    
    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.mantersolicretransmissaoarquivosretorno.IManterSolicRetransmissaoArquivosRetornoService#excluirSolBaseRetransmissao(br.com.bradesco.web.pgit.service.business.mantersolicretransmissaoarquivosretorno.bean.ExcluirSolBaseRetransmissaoEntradaDTO)
     */
    public ExcluirSolBaseRetransmissaoSaidaDTO excluirSolBaseRetransmissao(ExcluirSolBaseRetransmissaoEntradaDTO entradaDTO){
		
    	ExcluirSolBaseRetransmissaoRequest request = new ExcluirSolBaseRetransmissaoRequest();
    	ExcluirSolBaseRetransmissaoResponse response = new ExcluirSolBaseRetransmissaoResponse();
			  
		request.setNrSolicitacaoGeracaoRetorno(PgitUtil.verificaIntegerNulo(entradaDTO.getNrSolicitacaoGeracaoRetorno()));	
		request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoContratoNegocio()));
		request.setCdPessoaJuridicaContrato(PgitUtil.verificaLongNulo(entradaDTO.getCdPessoaJuridicaContrato()));
		request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(entradaDTO.getNrSequenciaContratoNegocio()));
		
		response = getFactoryAdapter().getExcluirSolBaseRetransmissaoPDCAdapter().invokeProcess(request);
		
		ExcluirSolBaseRetransmissaoSaidaDTO saidaDTO = new ExcluirSolBaseRetransmissaoSaidaDTO();	
		
		saidaDTO.setCodMensagem(response.getCodMensagem());
		saidaDTO.setMensagem(response.getMensagem());		
		
		return saidaDTO;
		
	}
    
    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.mantersolicretransmissaoarquivosretorno.IManterSolicRetransmissaoArquivosRetornoService#listarArquivoRetorno(br.com.bradesco.web.pgit.service.business.mantersolicretransmissaoarquivosretorno.bean.ListarArquivoRetornoEntradaDTO)
     */
    public List<ListarArquivoRetornoSaidaDTO> listarArquivoRetorno (ListarArquivoRetornoEntradaDTO entradaDTO){
    	List<ListarArquivoRetornoSaidaDTO> listaRetorno = new ArrayList<ListarArquivoRetornoSaidaDTO>();
    	ListarArquivoRetornoRequest request = new ListarArquivoRetornoRequest();
    	ListarArquivoRetornoResponse response = new ListarArquivoRetornoResponse();
    	
    	request.setCdPessoa(PgitUtil.verificaLongNulo(entradaDTO.getCdPessoa()));
        request.setCdPessoaJuridicaContrato(PgitUtil.verificaLongNulo(entradaDTO.getCdPessoaJuridicaContrato()));
        request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoContratoNegocio()));
        request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(entradaDTO.getNrSequenciaContratoNegocio()));
        request.setDtInicioGeracaoRetorno(PgitUtil.verificaStringNula(entradaDTO.getDtInicioGeracaoRetorno()));
        request.setDtFimGeracaoRetorno(PgitUtil.verificaStringNula(entradaDTO.getDtFimGeracaoRetorno()));
        request.setCdTipoLayoutArquivo(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoLayoutArquivo()));
        request.setNrArquivoRetorno(PgitUtil.verificaLongNulo(entradaDTO.getNrArquivoRetorno()));
        request.setCdSituacaoProcessamentoRetorno(PgitUtil.verificaIntegerNulo(entradaDTO.getCdSituacaoProcessamentoRetorno()));
        request.setNumeroOcorrencias(IManterSolicRetransmissaoArquivosRetornoServiceConstants.NUMERO_OCORRENCIAS_ARQUIVO_RETORNO);
    	
        response = getFactoryAdapter().getListarArquivoRetornoPDCAdapter().invokeProcess(request);
        
        ListarArquivoRetornoSaidaDTO saidaDTO;
        for(int i = 0;i<response.getOcorrenciasCount();i++){
        	saidaDTO = new ListarArquivoRetornoSaidaDTO();
        	
			saidaDTO.setCodMensagem(response.getCodMensagem());
        	saidaDTO.setMensagem(response.getMensagem());
        	saidaDTO.setCdPessoaJuridicaContrato(response.getOcorrencias(i).getCdPessoaJuridicaContrato());
            saidaDTO.setDsPessoaJuridicaContrato(response.getOcorrencias(i).getDsPessoaJuridicaContrato());
            saidaDTO.setCdTipoContratoNegocio(response.getOcorrencias(i).getCdTipoContratoNegocio());
            saidaDTO.setDsTipoContratoNegocio(response.getOcorrencias(i).getDsTipoContratoNegocio());
            saidaDTO.setNrSequenciaContratoNegocio(response.getOcorrencias(i).getNrSequenciaContratoNegocio());
            saidaDTO.setDsContrato(response.getOcorrencias(i).getDsContrato());
            saidaDTO.setCdPessoa(response.getOcorrencias(i).getCdPessoa());
            saidaDTO.setCorpoCpfCnpj(response.getOcorrencias(i).getCorpoCpfCnpj());
            saidaDTO.setDsPessoa(response.getOcorrencias(i).getDsPessoa());
            saidaDTO.setCdTipoLayoutArquivo(response.getOcorrencias(i).getCdTipoLayoutArquivo());
            saidaDTO.setDsTipoLayoutArquivo(response.getOcorrencias(i).getDsTipoLayoutArquivo());
            saidaDTO.setCdClienteTransferenciaArquivo(response.getOcorrencias(i).getCdClienteTransferenciaArquivo());
            saidaDTO.setDsArquivoRetorno(response.getOcorrencias(i).getDsArquivoRetorno());
            saidaDTO.setNrArquivoRetorno(response.getOcorrencias(i).getNrArquivoRetorno());
            saidaDTO.setHrInclusao(response.getOcorrencias(i).getHrInclusao());
            saidaDTO.setDsSituacaoProcessamentoRetorno(response.getOcorrencias(i).getDsSituacaoProcessamentoRetorno());
            saidaDTO.setDsAmbiente(response.getOcorrencias(i).getDsAmbiente());
            saidaDTO.setDsMeioTransmissao(response.getOcorrencias(i).getDsMeioTransmissao());
            saidaDTO.setQtdeRegistroArquivoRetorno(response.getOcorrencias(i).getQtdeRegistroArquivoRetorno());
            saidaDTO.setVlrRegistroArquivoRetorno(response.getOcorrencias(i).getVlrRegistroArquivoRetorno());
            saidaDTO.setHrInclusaoFormatada(FormatarData.formatarDataTrilha((response.getOcorrencias(i).getHrInclusao())));
            saidaDTO.setQtdeRegistroArquivoRetornoFormatada(nf.format((response.getOcorrencias(i).getQtdeRegistroArquivoRetorno())));
            listaRetorno.add(saidaDTO);
        }
    	
    	return listaRetorno;    	
    }
    
    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.mantersolicretransmissaoarquivosretorno.IManterSolicRetransmissaoArquivosRetornoService#listarArquivoRetransmissao(br.com.bradesco.web.pgit.service.business.mantersolicretransmissaoarquivosretorno.bean.ListarArqRetransmissaoEntradaDTO)
     */
    public List<ListarArqRetransmissaoSaidaDTO> listarArquivoRetransmissao (ListarArqRetransmissaoEntradaDTO entradaDTO){
    	List<ListarArqRetransmissaoSaidaDTO> listaRetorno = new ArrayList<ListarArqRetransmissaoSaidaDTO>();
    	ListarArqRetransmissaoRequest request = new ListarArqRetransmissaoRequest();
    	ListarArqRetransmissaoResponse response = new ListarArqRetransmissaoResponse();
    	
    	request.setCdPessoa(PgitUtil.verificaLongNulo(entradaDTO.getCdPessoa()));
    	request.setCdpessoaJuridicaContrato(PgitUtil.verificaLongNulo(entradaDTO.getCdPessoaJuridicaContrato()));
    	request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoContratoNegocio()));
    	request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(entradaDTO.getNrSequenciaContratoNegocio()));
    	request.setDtInicioGeracaoRetorno(PgitUtil.verificaStringNula(entradaDTO.getDtInicioGeracaoRetorno()));
    	request.setDtFimGeracaoRetorno(PgitUtil.verificaStringNula(entradaDTO.getDtFimGeracaoRetorno()));
    	request.setCdTipoLayoutArquivo(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoLayoutArquivo()));
    	request.setNrArquivoRetorno(PgitUtil.verificaLongNulo(entradaDTO.getNrArquivoRetorno()));
    	request.setCdSituacaoProcessamentoRetorno(PgitUtil.verificaIntegerNulo(entradaDTO.getCdSituacaoProcessamentoRetorno()));
    	request.setNrOcorrencias(IManterSolicRetransmissaoArquivosRetornoServiceConstants.NUMERO_OCORRENCIAS_ARQUIVO_RETORNO);
    	
    	response = getFactoryAdapter().getListarArqRetransmissaoPDCAdapter().invokeProcess(request);
    	
    	ListarArqRetransmissaoSaidaDTO saidaDTO;
    	for(int i = 0;i<response.getOcorrenciasCount();i++){
    		saidaDTO = new ListarArqRetransmissaoSaidaDTO();
    		
    		saidaDTO.setCodMensagem(response.getCodMensagem());
    		saidaDTO.setMensagem(response.getMensagem());
    		saidaDTO.setCdPessoaJuridicaContrato(response.getOcorrencias(i).getCdpessoaJuridicaContrato());
    		saidaDTO.setDsPessoaJuridicaContrato(response.getOcorrencias(i).getDsPessoaJuridicaContrato());
    		saidaDTO.setCdTipoContratoNegocio(response.getOcorrencias(i).getCdTipoContratoNegocio());
    		saidaDTO.setDsTipoContratoNegocio(response.getOcorrencias(i).getDsTipoContratoNegocio());
    		saidaDTO.setNrSequenciaContratoNegocio(response.getOcorrencias(i).getNrSequenciaContratoNegocio());
    		saidaDTO.setDsContrato(response.getOcorrencias(i).getDsContrato());
    		saidaDTO.setCdPessoa(response.getOcorrencias(i).getCdPessoa());
    		saidaDTO.setCorpoCpfCnpj(response.getOcorrencias(i).getCpfCnpjPessoa());
    		saidaDTO.setDsPessoa(response.getOcorrencias(i).getDsPessoa());
    		saidaDTO.setCdTipoLayoutArquivo(response.getOcorrencias(i).getCdTipoLayoutArquivo());
    		saidaDTO.setDsTipoLayoutArquivo(response.getOcorrencias(i).getDsTipoLayoutArquivo());
    		saidaDTO.setCdClienteTransferenciaArquivo(response.getOcorrencias(i).getCdClienteTransferenciaArquivo());
    		saidaDTO.setDsArquivoRetorno(response.getOcorrencias(i).getDsArquivoRetorno());
    		saidaDTO.setNrArquivoRetorno(response.getOcorrencias(i).getNrArquivoRetorno());
    		saidaDTO.setHrInclusao(response.getOcorrencias(i).getHrInclusaoRegistro());
    		saidaDTO.setDsSituacaoProcessamentoRetorno(response.getOcorrencias(i).getDsSituacaoProcessamentoRetorno());
    		saidaDTO.setDsAmbiente(response.getOcorrencias(i).getDsAmbiente());
    		saidaDTO.setDsMeioTransmissao(response.getOcorrencias(i).getDsMeioTransmissao());
    		saidaDTO.setQtdeRegistroArquivoRetorno(response.getOcorrencias(i).getQtdeRegistroArquivoRetorno());
    		saidaDTO.setVlrRegistroArquivoRetorno(response.getOcorrencias(i).getVlrRegistroArquivoRetorno());
    		saidaDTO.setHrInclusaoFormatada(FormatarData.formatarDataTrilha((response.getOcorrencias(i).getHrInclusaoRegistro())));
    		saidaDTO.setQtdeRegistroArquivoRetornoFormatada(nf.format((response.getOcorrencias(i).getQtdeRegistroArquivoRetorno())));
    		listaRetorno.add(saidaDTO);
    	}
    	
    	return listaRetorno;    	
    }
    
    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.mantersolicretransmissaoarquivosretorno.IManterSolicRetransmissaoArquivosRetornoService#incluirSolBaseRetransmissao(br.com.bradesco.web.pgit.service.business.mantersolicretransmissaoarquivosretorno.bean.IncluirSolBaseRetransmissaoEntradaDTO)
     */
    public IncluirSolBaseRetransmissaoSaidaDTO incluirSolBaseRetransmissao(IncluirSolBaseRetransmissaoEntradaDTO entradaDTO){    	
    		
    	IncluirSolBaseRetransmissaoSaidaDTO saidaDTO = new IncluirSolBaseRetransmissaoSaidaDTO();
    	IncluirSolBaseRetransmissaoRequest request = new IncluirSolBaseRetransmissaoRequest();
    	IncluirSolBaseRetransmissaoResponse response = new IncluirSolBaseRetransmissaoResponse();
    	
    	request.setCdPessoaJuridicaContrato(PgitUtil.verificaLongNulo(entradaDTO.getCdPessoaJuridicaContrato()));
    	request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoContratoNegocio()));
    	request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(entradaDTO.getNrSequenciaContratoNegocio()));
    	request.setNumeroLinhas(PgitUtil.verificaIntegerNulo(entradaDTO.getNumeroLinhas()));    	
    	request.setVlTarifaPadrao(PgitUtil.verificaBigDecimalNulo(entradaDTO.getVlTarifaPadrao()));
    	request.setNumPercentualDescTarifa(PgitUtil.verificaBigDecimalNulo(entradaDTO.getNumPercentualDescTarifa()));
    	request.setVlrTarifaAtual(PgitUtil.verificaBigDecimalNulo(entradaDTO.getVlrTarifaAtual()));
    	
    	ArrayList<br.com.bradesco.web.pgit.service.data.pdc.incluirsolbaseretransmissao.request.Ocorrencias> lista = new ArrayList<br.com.bradesco.web.pgit.service.data.pdc.incluirsolbaseretransmissao.request.Ocorrencias>();
    	
    	
    	if (!entradaDTO.getListaOcorrenciasIncluirSolBaseRetransmissao().isEmpty()) {
    		for (int i=0; i< entradaDTO.getListaOcorrenciasIncluirSolBaseRetransmissao().size(); i++){
    			br.com.bradesco.web.pgit.service.data.pdc.incluirsolbaseretransmissao.request.Ocorrencias ocorrencia = new br.com.bradesco.web.pgit.service.data.pdc.incluirsolbaseretransmissao.request.Ocorrencias();
    			
    			ocorrencia.setCdClienteTransferenciaArquivo(PgitUtil.verificaLongNulo(entradaDTO.getListaOcorrenciasIncluirSolBaseRetransmissao().get(i).getCdClienteTransferenciaArquivo()));
    			ocorrencia.setCdPessoa(PgitUtil.verificaLongNulo(entradaDTO.getListaOcorrenciasIncluirSolBaseRetransmissao().get(i).getCdPessoa()));
    			ocorrencia.setCdTipoLayoutArquivo(PgitUtil.verificaIntegerNulo(entradaDTO.getListaOcorrenciasIncluirSolBaseRetransmissao().get(i).getCdTipoLayoutArquivo()));
    			ocorrencia.setHrInclusaoArquivoRetorno(PgitUtil.verificaStringNula(entradaDTO.getListaOcorrenciasIncluirSolBaseRetransmissao().get(i).getHrInclusaoArquivoRetorno()));
    			ocorrencia.setNrSequenciaArquivoRetorno(PgitUtil.verificaLongNulo(entradaDTO.getListaOcorrenciasIncluirSolBaseRetransmissao().get(i).getNrSequenciaArquivoRetorno()));
    			ocorrencia.setDsArquivoRetorno(PgitUtil.verificaStringNula(entradaDTO.getListaOcorrenciasIncluirSolBaseRetransmissao().get(i).getDsArquivoRetorno()));
    			lista.add(ocorrencia);		
    		}			
		}else {
			br.com.bradesco.web.pgit.service.data.pdc.incluirsolbaseretransmissao.request.Ocorrencias ocorrencia = new br.com.bradesco.web.pgit.service.data.pdc.incluirsolbaseretransmissao.request.Ocorrencias();
			
			ocorrencia.setCdClienteTransferenciaArquivo(0);
			ocorrencia.setCdPessoa(0l);
			ocorrencia.setCdTipoLayoutArquivo(0);
			ocorrencia.setHrInclusaoArquivoRetorno("");
			ocorrencia.setNrSequenciaArquivoRetorno(0l);
			ocorrencia.setDsArquivoRetorno("");
			lista.add(ocorrencia);
		}

    	request.setOcorrencias((br.com.bradesco.web.pgit.service.data.pdc.incluirsolbaseretransmissao.request.Ocorrencias[]) lista.toArray(new br.com.bradesco.web.pgit.service.data.pdc.incluirsolbaseretransmissao.request.Ocorrencias[0]));
    	response = getFactoryAdapter().getIncluirSolBaseRetransmissaoPDCAdapter().invokeProcess(request);
    		
    	saidaDTO.setCodMensagem(response.getCodMensagem());
    	saidaDTO.setMensagem(response.getMensagem());
    		
    	return saidaDTO;
    }   
}

