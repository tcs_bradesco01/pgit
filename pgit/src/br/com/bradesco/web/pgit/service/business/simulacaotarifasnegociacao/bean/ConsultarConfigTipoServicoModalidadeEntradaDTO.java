/*
 * Nome: br.com.bradesco.web.pgit.service.business.simulacaotarifasnegociacao.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.simulacaotarifasnegociacao.bean;


/**
 * Nome: ConsultarConfigTipoServicoModalidadeEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarConfigTipoServicoModalidadeEntradaDTO {

    private Long cdPessoaJuridicaContrato;

    private Integer cdTipoContratoNegocio;

    private Long nrSequenciaContratoNegocio;

    private Integer cdProdutoServicoOperacao;

    private Integer cdProdutoOperacaoRelacionado;

    private Integer cdParametro;

	
    /**
	 * Nome: getCdPessoaJuridicaContrato
	 *
	 * @exception
	 * @throws
	 * @return cdPessoaJuridicaContrato
	 */
	public Long getCdPessoaJuridicaContrato() {
		return cdPessoaJuridicaContrato;
	}

	/**
	 * Nome: setCdPessoaJuridicaContrato
	 *
	 * @exception
	 * @throws
	 * @param cdPessoaJuridicaContrato
	 */
	public void setCdPessoaJuridicaContrato(Long cdPessoaJuridicaContrato) {
		this.cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
	}

	/**
	 * Nome: getCdTipoContratoNegocio
	 *
	 * @exception
	 * @throws
	 * @return cdTipoContratoNegocio
	 */
	public Integer getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}

	/**
	 * Nome: setCdTipoContratoNegocio
	 *
	 * @exception
	 * @throws
	 * @param cdTipoContratoNegocio
	 */
	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}

	/**
	 * Nome: getNrSequenciaContratoNegocio
	 *
	 * @exception
	 * @throws
	 * @return nrSequenciaContratoNegocio
	 */
	public Long getNrSequenciaContratoNegocio() {
		return nrSequenciaContratoNegocio;
	}

	/**
	 * Nome: setNrSequenciaContratoNegocio
	 *
	 * @exception
	 * @throws
	 * @param nrSequenciaContratoNegocio
	 */
	public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}

	/**
	 * Nome: getCdProdutoServicoOperacao
	 *
	 * @exception
	 * @throws
	 * @return cdProdutoServicoOperacao
	 */
	public Integer getCdProdutoServicoOperacao() {
		return cdProdutoServicoOperacao;
	}

	/**
	 * Nome: setCdProdutoServicoOperacao
	 *
	 * @exception
	 * @throws
	 * @param cdProdutoServicoOperacao
	 */
	public void setCdProdutoServicoOperacao(Integer cdProdutoServicoOperacao) {
		this.cdProdutoServicoOperacao = cdProdutoServicoOperacao;
	}

	/**
	 * Nome: getCdProdutoOperacaoRelacionado
	 *
	 * @exception
	 * @throws
	 * @return cdProdutoOperacaoRelacionado
	 */
	public Integer getCdProdutoOperacaoRelacionado() {
		return cdProdutoOperacaoRelacionado;
	}

	/**
	 * Nome: setCdProdutoOperacaoRelacionado
	 *
	 * @exception
	 * @throws
	 * @param cdProdutoOperacaoRelacionado
	 */
	public void setCdProdutoOperacaoRelacionado(Integer cdProdutoOperacaoRelacionado) {
		this.cdProdutoOperacaoRelacionado = cdProdutoOperacaoRelacionado;
	}

	/**
	 * Nome: getCdParametro
	 *
	 * @exception
	 * @throws
	 * @return cdParametro
	 */
	public Integer getCdParametro() {
		return cdParametro;
	}

	/**
	 * Nome: setCdParametro
	 *
	 * @exception
	 * @throws
	 * @param cdParametro
	 */
	public void setCdParametro(Integer cdParametro) {
		this.cdParametro = cdParametro;
	}

}