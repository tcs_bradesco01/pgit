/*
 * Nome: br.com.bradesco.web.pgit.service.business.manterambienteoperacaocontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.manterambienteoperacaocontrato.bean;

/**
 * Nome: ListarAmbienteEntradaDTO
 * <p>
 * Prop�sito:
 * </p>
 * .
 * 
 * @author : todo!
 * @version :
 */
public class ListarAmbienteEntradaDTO {

	/** Atributo maxOcorrencias. */
	private Integer maxOcorrencias;

	/** Atributo cdPessoaJuridica. */
	private Long cdPessoaJuridica;

	/** Atributo cdTipoContrato. */
	private Integer cdTipoContrato;

	/** Atributo nrSequenciaContrato. */
	private Long nrSequenciaContrato;

	/** Atributo cdPessoa. */
	private Long cdPessoa;

	/** Atributo cdProdutoOperacaoRelacionamento. */
	private Integer cdProdutoOperacaoRelacionamento;

	private String dsAmbiente;

	/**
	 * Get: maxOcorrencias.
	 * 
	 * @return maxOcorrencias
	 */
	public Integer getMaxOcorrencias() {
		return maxOcorrencias;
	}

	/**
	 * Set: maxOcorrencias.
	 * 
	 * @param maxOcorrencias
	 *            the max ocorrencias
	 */
	public void setMaxOcorrencias(Integer maxOcorrencias) {
		this.maxOcorrencias = maxOcorrencias;
	}

	/**
	 * Get: cdPessoaJuridica.
	 * 
	 * @return cdPessoaJuridica
	 */
	public Long getCdPessoaJuridica() {
		return cdPessoaJuridica;
	}

	/**
	 * Set: cdPessoaJuridica.
	 * 
	 * @param cdPessoaJuridica
	 *            the cd pessoa juridica
	 */
	public void setCdPessoaJuridica(Long cdPessoaJuridica) {
		this.cdPessoaJuridica = cdPessoaJuridica;
	}

	/**
	 * Get: cdTipoContrato.
	 * 
	 * @return cdTipoContrato
	 */
	public Integer getCdTipoContrato() {
		return cdTipoContrato;
	}

	/**
	 * Set: cdTipoContrato.
	 * 
	 * @param cdTipoContrato
	 *            the cd tipo contrato
	 */
	public void setCdTipoContrato(Integer cdTipoContrato) {
		this.cdTipoContrato = cdTipoContrato;
	}

	/**
	 * Get: nrSequenciaContrato.
	 * 
	 * @return nrSequenciaContrato
	 */
	public Long getNrSequenciaContrato() {
		return nrSequenciaContrato;
	}

	/**
	 * Set: nrSequenciaContrato.
	 * 
	 * @param nrSequenciaContrato
	 *            the nr sequencia contrato
	 */
	public void setNrSequenciaContrato(Long nrSequenciaContrato) {
		this.nrSequenciaContrato = nrSequenciaContrato;
	}

	/**
	 * Get: cdPessoa.
	 * 
	 * @return cdPessoa
	 */
	public Long getCdPessoa() {
		return cdPessoa;
	}

	/**
	 * Set: cdPessoa.
	 * 
	 * @param cdPessoa
	 *            the cd pessoa
	 */
	public void setCdPessoa(Long cdPessoa) {
		this.cdPessoa = cdPessoa;
	}

	/**
	 * Get: cdProdutoOperacaoRelacionamento.
	 * 
	 * @return cdProdutoOperacaoRelacionamento
	 */
	public Integer getCdProdutoOperacaoRelacionamento() {
		return cdProdutoOperacaoRelacionamento;
	}

	/**
	 * Set: cdProdutoOperacaoRelacionamento.
	 * 
	 * @param cdProdutoOperacaoRelacionamento
	 *            the cd produto operacao relacionamento
	 */
	public void setCdProdutoOperacaoRelacionamento(
			Integer cdProdutoOperacaoRelacionamento) {
		this.cdProdutoOperacaoRelacionamento = cdProdutoOperacaoRelacionamento;
	}

	public String getDsAmbiente() {
		return dsAmbiente;
	}

	public void setDsAmbiente(String dsAmbiente) {
		this.dsAmbiente = dsAmbiente;
	}

}
