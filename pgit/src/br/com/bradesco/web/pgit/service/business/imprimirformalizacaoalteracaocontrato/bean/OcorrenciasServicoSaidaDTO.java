/*
 * Nome: br.com.bradesco.web.pgit.service.business.imprimirformalizacaoalteracaocontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.imprimirformalizacaoalteracaocontrato.bean;

import br.com.bradesco.web.pgit.service.data.pdc.imprimiraditivocontrato.response.OcorrenciasServico2;

/**
 * Nome: OcorrenciasServicoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class OcorrenciasServicoSaidaDTO {
    
    /** Atributo cdAcaoServico. */
    private Integer cdAcaoServico;
    
    /** Atributo cdServicoOperacao. */
    private Integer cdServicoOperacao;
    
    /** Atributo dsServicoOperacao. */
    private String dsServicoOperacao;
    
    /** Atributo cdServicoRelacionado. */
    private Integer cdServicoRelacionado;
    
    /** Atributo dsServicoRelacionado. */
    private String dsServicoRelacionado;
    
	/**
	 * Ocorrencias servico saida dto.
	 */
	public OcorrenciasServicoSaidaDTO() {
		super();
		// TODO Auto-generated constructor stub
	}


	/**
	 * Ocorrencias servico saida dto.
	 *
	 * @param ocorrenciasServico2 the ocorrencias servico2
	 */
	public OcorrenciasServicoSaidaDTO(OcorrenciasServico2 ocorrenciasServico2) {
		super();
		this.cdAcaoServico = ocorrenciasServico2.getCdAcaoServico();
		this.cdServicoOperacao = ocorrenciasServico2.getCdServicoOperacao();
		this.dsServicoOperacao = ocorrenciasServico2.getDsServicoOperacao();
		this.cdServicoRelacionado = ocorrenciasServico2.getCdServicoRelacionado();
		this.dsServicoRelacionado = ocorrenciasServico2.getDsServicoRelacionado();
	}

	/**
	 * Get: cdAcaoServico.
	 *
	 * @return cdAcaoServico
	 */
	public Integer getCdAcaoServico() {
		return cdAcaoServico;
	}

	/**
	 * Set: cdAcaoServico.
	 *
	 * @param cdAcaoServico the cd acao servico
	 */
	public void setCdAcaoServico(Integer cdAcaoServico) {
		this.cdAcaoServico = cdAcaoServico;
	}

	/**
	 * Get: cdServicoOperacao.
	 *
	 * @return cdServicoOperacao
	 */
	public Integer getCdServicoOperacao() {
		return cdServicoOperacao;
	}

	/**
	 * Set: cdServicoOperacao.
	 *
	 * @param cdServicoOperacao the cd servico operacao
	 */
	public void setCdServicoOperacao(Integer cdServicoOperacao) {
		this.cdServicoOperacao = cdServicoOperacao;
	}

	/**
	 * Get: cdServicoRelacionado.
	 *
	 * @return cdServicoRelacionado
	 */
	public Integer getCdServicoRelacionado() {
		return cdServicoRelacionado;
	}

	/**
	 * Set: cdServicoRelacionado.
	 *
	 * @param cdServicoRelacionado the cd servico relacionado
	 */
	public void setCdServicoRelacionado(Integer cdServicoRelacionado) {
		this.cdServicoRelacionado = cdServicoRelacionado;
	}

	/**
	 * Get: dsServicoOperacao.
	 *
	 * @return dsServicoOperacao
	 */
	public String getDsServicoOperacao() {
		return dsServicoOperacao;
	}

	/**
	 * Set: dsServicoOperacao.
	 *
	 * @param dsServicoOperacao the ds servico operacao
	 */
	public void setDsServicoOperacao(String dsServicoOperacao) {
		this.dsServicoOperacao = dsServicoOperacao;
	}

	/**
	 * Get: dsServicoRelacionado.
	 *
	 * @return dsServicoRelacionado
	 */
	public String getDsServicoRelacionado() {
		return dsServicoRelacionado;
	}

	/**
	 * Set: dsServicoRelacionado.
	 *
	 * @param dsServicoRelacionado the ds servico relacionado
	 */
	public void setDsServicoRelacionado(String dsServicoRelacionado) {
		this.dsServicoRelacionado = dsServicoRelacionado;
	}
}
