/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/interfaz.ftl,v $
 * $Id: interfaz.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: interfaz.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.mantermensagemlayout;

import java.util.List;

import br.com.bradesco.web.pgit.service.business.mantermensagemlayout.bean.AlterarMsgLayoutArqRetornoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantermensagemlayout.bean.AlterarMsgLayoutArqRetornoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantermensagemlayout.bean.ConsultarMsgLayoutArqRetornoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantermensagemlayout.bean.ConsultarMsgLayoutArqRetornoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantermensagemlayout.bean.DetalharMsgLayoutArqRetornoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantermensagemlayout.bean.DetalharMsgLayoutArqRetornoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantermensagemlayout.bean.ExcluirMsgLayoutArqRetornoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantermensagemlayout.bean.ExcluirMsgLayoutArqRetornoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantermensagemlayout.bean.IncluirMsgLayoutArqRetornoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantermensagemlayout.bean.IncluirMsgLayoutArqRetornoSaidaDTO;

/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Interface do adaptador: ManterMensagemLayout
 * </p>
 * 
 * @comment CODIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public interface IManterMensagemLayoutService {

    /**
     * Consultar msg layout arq retorno.
     *
     * @param entrada the entrada
     * @return the list< consultar msg layout arq retorno saida dt o>
     */
    List<ConsultarMsgLayoutArqRetornoSaidaDTO> consultarMsgLayoutArqRetorno(ConsultarMsgLayoutArqRetornoEntradaDTO entrada);
    
    /**
     * Incluir msg layout arq retorno.
     *
     * @param entrada the entrada
     * @return the incluir msg layout arq retorno saida dto
     */
    IncluirMsgLayoutArqRetornoSaidaDTO incluirMsgLayoutArqRetorno(IncluirMsgLayoutArqRetornoEntradaDTO entrada);
    
    /**
     * Excluir msg layout arq retorno.
     *
     * @param entrada the entrada
     * @return the excluir msg layout arq retorno saida dto
     */
    ExcluirMsgLayoutArqRetornoSaidaDTO excluirMsgLayoutArqRetorno(ExcluirMsgLayoutArqRetornoEntradaDTO entrada);
    
    /**
     * Alterar msg layout arq retorno.
     *
     * @param entrada the entrada
     * @return the alterar msg layout arq retorno saida dto
     */
    AlterarMsgLayoutArqRetornoSaidaDTO alterarMsgLayoutArqRetorno(AlterarMsgLayoutArqRetornoEntradaDTO entrada);
    
    /**
     * Detalhar msg layout arq retorno.
     *
     * @param entrada the entrada
     * @return the detalhar msg layout arq retorno saida dto
     */
    DetalharMsgLayoutArqRetornoSaidaDTO detalharMsgLayoutArqRetorno(DetalharMsgLayoutArqRetornoEntradaDTO entrada);

}

