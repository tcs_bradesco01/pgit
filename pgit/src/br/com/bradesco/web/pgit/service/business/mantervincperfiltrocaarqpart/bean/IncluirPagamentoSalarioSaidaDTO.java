package br.com.bradesco.web.pgit.service.business.mantervincperfiltrocaarqpart.bean;


/**
 * Arquivo criado em 06/10/15.
 */
public class IncluirPagamentoSalarioSaidaDTO {

    private String codMensagem;

    private String mensagem;

    public void setCodMensagem(String codMensagem) {
        this.codMensagem = codMensagem;
    }

    public String getCodMensagem() {
        return this.codMensagem;
    }

    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }

    public String getMensagem() {
        return this.mensagem;
    }
}