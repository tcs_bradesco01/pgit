/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultarretorno.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ConsultarRetornoRequest.
 * 
 * @version $Revision$ $Date$
 */
public class ConsultarRetornoRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _qtdeOcorrencia
     */
    private int _qtdeOcorrencia = 0;

    /**
     * keeps track of state for field: _qtdeOcorrencia
     */
    private boolean _has_qtdeOcorrencia;

    /**
     * Field _horaRecepcaoInicio
     */
    private java.lang.String _horaRecepcaoInicio;

    /**
     * Field _horaRecepcaoFim
     */
    private java.lang.String _horaRecepcaoFim;

    /**
     * Field _codigoSistemaLegado
     */
    private java.lang.String _codigoSistemaLegado;

    /**
     * Field _cpfCnpj
     */
    private long _cpfCnpj = 0;

    /**
     * keeps track of state for field: _cpfCnpj
     */
    private boolean _has_cpfCnpj;

    /**
     * Field _filial
     */
    private int _filial = 0;

    /**
     * keeps track of state for field: _filial
     */
    private boolean _has_filial;

    /**
     * Field _controle
     */
    private int _controle = 0;

    /**
     * keeps track of state for field: _controle
     */
    private boolean _has_controle;

    /**
     * Field _codigoPerfilComunicacao
     */
    private long _codigoPerfilComunicacao = 0;

    /**
     * keeps track of state for field: _codigoPerfilComunicacao
     */
    private boolean _has_codigoPerfilComunicacao;

    /**
     * Field _numeroArquivoRetorno
     */
    private long _numeroArquivoRetorno = 0;

    /**
     * keeps track of state for field: _numeroArquivoRetorno
     */
    private boolean _has_numeroArquivoRetorno;

    /**
     * Field _codigoTipoRetorno
     */
    private int _codigoTipoRetorno = 0;

    /**
     * keeps track of state for field: _codigoTipoRetorno
     */
    private boolean _has_codigoTipoRetorno;

    /**
     * Field _situacaoGeracaoRetorno
     */
    private int _situacaoGeracaoRetorno = 0;

    /**
     * keeps track of state for field: _situacaoGeracaoRetorno
     */
    private boolean _has_situacaoGeracaoRetorno;


      //----------------/
     //- Constructors -/
    //----------------/

    public ConsultarRetornoRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarretorno.request.ConsultarRetornoRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCodigoPerfilComunicacao
     * 
     */
    public void deleteCodigoPerfilComunicacao()
    {
        this._has_codigoPerfilComunicacao= false;
    } //-- void deleteCodigoPerfilComunicacao() 

    /**
     * Method deleteCodigoTipoRetorno
     * 
     */
    public void deleteCodigoTipoRetorno()
    {
        this._has_codigoTipoRetorno= false;
    } //-- void deleteCodigoTipoRetorno() 

    /**
     * Method deleteControle
     * 
     */
    public void deleteControle()
    {
        this._has_controle= false;
    } //-- void deleteControle() 

    /**
     * Method deleteCpfCnpj
     * 
     */
    public void deleteCpfCnpj()
    {
        this._has_cpfCnpj= false;
    } //-- void deleteCpfCnpj() 

    /**
     * Method deleteFilial
     * 
     */
    public void deleteFilial()
    {
        this._has_filial= false;
    } //-- void deleteFilial() 

    /**
     * Method deleteNumeroArquivoRetorno
     * 
     */
    public void deleteNumeroArquivoRetorno()
    {
        this._has_numeroArquivoRetorno= false;
    } //-- void deleteNumeroArquivoRetorno() 

    /**
     * Method deleteQtdeOcorrencia
     * 
     */
    public void deleteQtdeOcorrencia()
    {
        this._has_qtdeOcorrencia= false;
    } //-- void deleteQtdeOcorrencia() 

    /**
     * Method deleteSituacaoGeracaoRetorno
     * 
     */
    public void deleteSituacaoGeracaoRetorno()
    {
        this._has_situacaoGeracaoRetorno= false;
    } //-- void deleteSituacaoGeracaoRetorno() 

    /**
     * Returns the value of field 'codigoPerfilComunicacao'.
     * 
     * @return long
     * @return the value of field 'codigoPerfilComunicacao'.
     */
    public long getCodigoPerfilComunicacao()
    {
        return this._codigoPerfilComunicacao;
    } //-- long getCodigoPerfilComunicacao() 

    /**
     * Returns the value of field 'codigoSistemaLegado'.
     * 
     * @return String
     * @return the value of field 'codigoSistemaLegado'.
     */
    public java.lang.String getCodigoSistemaLegado()
    {
        return this._codigoSistemaLegado;
    } //-- java.lang.String getCodigoSistemaLegado() 

    /**
     * Returns the value of field 'codigoTipoRetorno'.
     * 
     * @return int
     * @return the value of field 'codigoTipoRetorno'.
     */
    public int getCodigoTipoRetorno()
    {
        return this._codigoTipoRetorno;
    } //-- int getCodigoTipoRetorno() 

    /**
     * Returns the value of field 'controle'.
     * 
     * @return int
     * @return the value of field 'controle'.
     */
    public int getControle()
    {
        return this._controle;
    } //-- int getControle() 

    /**
     * Returns the value of field 'cpfCnpj'.
     * 
     * @return long
     * @return the value of field 'cpfCnpj'.
     */
    public long getCpfCnpj()
    {
        return this._cpfCnpj;
    } //-- long getCpfCnpj() 

    /**
     * Returns the value of field 'filial'.
     * 
     * @return int
     * @return the value of field 'filial'.
     */
    public int getFilial()
    {
        return this._filial;
    } //-- int getFilial() 

    /**
     * Returns the value of field 'horaRecepcaoFim'.
     * 
     * @return String
     * @return the value of field 'horaRecepcaoFim'.
     */
    public java.lang.String getHoraRecepcaoFim()
    {
        return this._horaRecepcaoFim;
    } //-- java.lang.String getHoraRecepcaoFim() 

    /**
     * Returns the value of field 'horaRecepcaoInicio'.
     * 
     * @return String
     * @return the value of field 'horaRecepcaoInicio'.
     */
    public java.lang.String getHoraRecepcaoInicio()
    {
        return this._horaRecepcaoInicio;
    } //-- java.lang.String getHoraRecepcaoInicio() 

    /**
     * Returns the value of field 'numeroArquivoRetorno'.
     * 
     * @return long
     * @return the value of field 'numeroArquivoRetorno'.
     */
    public long getNumeroArquivoRetorno()
    {
        return this._numeroArquivoRetorno;
    } //-- long getNumeroArquivoRetorno() 

    /**
     * Returns the value of field 'qtdeOcorrencia'.
     * 
     * @return int
     * @return the value of field 'qtdeOcorrencia'.
     */
    public int getQtdeOcorrencia()
    {
        return this._qtdeOcorrencia;
    } //-- int getQtdeOcorrencia() 

    /**
     * Returns the value of field 'situacaoGeracaoRetorno'.
     * 
     * @return int
     * @return the value of field 'situacaoGeracaoRetorno'.
     */
    public int getSituacaoGeracaoRetorno()
    {
        return this._situacaoGeracaoRetorno;
    } //-- int getSituacaoGeracaoRetorno() 

    /**
     * Method hasCodigoPerfilComunicacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCodigoPerfilComunicacao()
    {
        return this._has_codigoPerfilComunicacao;
    } //-- boolean hasCodigoPerfilComunicacao() 

    /**
     * Method hasCodigoTipoRetorno
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCodigoTipoRetorno()
    {
        return this._has_codigoTipoRetorno;
    } //-- boolean hasCodigoTipoRetorno() 

    /**
     * Method hasControle
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasControle()
    {
        return this._has_controle;
    } //-- boolean hasControle() 

    /**
     * Method hasCpfCnpj
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCpfCnpj()
    {
        return this._has_cpfCnpj;
    } //-- boolean hasCpfCnpj() 

    /**
     * Method hasFilial
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasFilial()
    {
        return this._has_filial;
    } //-- boolean hasFilial() 

    /**
     * Method hasNumeroArquivoRetorno
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNumeroArquivoRetorno()
    {
        return this._has_numeroArquivoRetorno;
    } //-- boolean hasNumeroArquivoRetorno() 

    /**
     * Method hasQtdeOcorrencia
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtdeOcorrencia()
    {
        return this._has_qtdeOcorrencia;
    } //-- boolean hasQtdeOcorrencia() 

    /**
     * Method hasSituacaoGeracaoRetorno
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasSituacaoGeracaoRetorno()
    {
        return this._has_situacaoGeracaoRetorno;
    } //-- boolean hasSituacaoGeracaoRetorno() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'codigoPerfilComunicacao'.
     * 
     * @param codigoPerfilComunicacao the value of field
     * 'codigoPerfilComunicacao'.
     */
    public void setCodigoPerfilComunicacao(long codigoPerfilComunicacao)
    {
        this._codigoPerfilComunicacao = codigoPerfilComunicacao;
        this._has_codigoPerfilComunicacao = true;
    } //-- void setCodigoPerfilComunicacao(long) 

    /**
     * Sets the value of field 'codigoSistemaLegado'.
     * 
     * @param codigoSistemaLegado the value of field
     * 'codigoSistemaLegado'.
     */
    public void setCodigoSistemaLegado(java.lang.String codigoSistemaLegado)
    {
        this._codigoSistemaLegado = codigoSistemaLegado;
    } //-- void setCodigoSistemaLegado(java.lang.String) 

    /**
     * Sets the value of field 'codigoTipoRetorno'.
     * 
     * @param codigoTipoRetorno the value of field
     * 'codigoTipoRetorno'.
     */
    public void setCodigoTipoRetorno(int codigoTipoRetorno)
    {
        this._codigoTipoRetorno = codigoTipoRetorno;
        this._has_codigoTipoRetorno = true;
    } //-- void setCodigoTipoRetorno(int) 

    /**
     * Sets the value of field 'controle'.
     * 
     * @param controle the value of field 'controle'.
     */
    public void setControle(int controle)
    {
        this._controle = controle;
        this._has_controle = true;
    } //-- void setControle(int) 

    /**
     * Sets the value of field 'cpfCnpj'.
     * 
     * @param cpfCnpj the value of field 'cpfCnpj'.
     */
    public void setCpfCnpj(long cpfCnpj)
    {
        this._cpfCnpj = cpfCnpj;
        this._has_cpfCnpj = true;
    } //-- void setCpfCnpj(long) 

    /**
     * Sets the value of field 'filial'.
     * 
     * @param filial the value of field 'filial'.
     */
    public void setFilial(int filial)
    {
        this._filial = filial;
        this._has_filial = true;
    } //-- void setFilial(int) 

    /**
     * Sets the value of field 'horaRecepcaoFim'.
     * 
     * @param horaRecepcaoFim the value of field 'horaRecepcaoFim'.
     */
    public void setHoraRecepcaoFim(java.lang.String horaRecepcaoFim)
    {
        this._horaRecepcaoFim = horaRecepcaoFim;
    } //-- void setHoraRecepcaoFim(java.lang.String) 

    /**
     * Sets the value of field 'horaRecepcaoInicio'.
     * 
     * @param horaRecepcaoInicio the value of field
     * 'horaRecepcaoInicio'.
     */
    public void setHoraRecepcaoInicio(java.lang.String horaRecepcaoInicio)
    {
        this._horaRecepcaoInicio = horaRecepcaoInicio;
    } //-- void setHoraRecepcaoInicio(java.lang.String) 

    /**
     * Sets the value of field 'numeroArquivoRetorno'.
     * 
     * @param numeroArquivoRetorno the value of field
     * 'numeroArquivoRetorno'.
     */
    public void setNumeroArquivoRetorno(long numeroArquivoRetorno)
    {
        this._numeroArquivoRetorno = numeroArquivoRetorno;
        this._has_numeroArquivoRetorno = true;
    } //-- void setNumeroArquivoRetorno(long) 

    /**
     * Sets the value of field 'qtdeOcorrencia'.
     * 
     * @param qtdeOcorrencia the value of field 'qtdeOcorrencia'.
     */
    public void setQtdeOcorrencia(int qtdeOcorrencia)
    {
        this._qtdeOcorrencia = qtdeOcorrencia;
        this._has_qtdeOcorrencia = true;
    } //-- void setQtdeOcorrencia(int) 

    /**
     * Sets the value of field 'situacaoGeracaoRetorno'.
     * 
     * @param situacaoGeracaoRetorno the value of field
     * 'situacaoGeracaoRetorno'.
     */
    public void setSituacaoGeracaoRetorno(int situacaoGeracaoRetorno)
    {
        this._situacaoGeracaoRetorno = situacaoGeracaoRetorno;
        this._has_situacaoGeracaoRetorno = true;
    } //-- void setSituacaoGeracaoRetorno(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ConsultarRetornoRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultarretorno.request.ConsultarRetornoRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultarretorno.request.ConsultarRetornoRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultarretorno.request.ConsultarRetornoRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarretorno.request.ConsultarRetornoRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
