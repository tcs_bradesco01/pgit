/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listarorgaopagador.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ListarOrgaoPagadorRequest.
 * 
 * @version $Revision$ $Date$
 */
public class ListarOrgaoPagadorRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _maximoOcorrencias
     */
    private int _maximoOcorrencias = 0;

    /**
     * keeps track of state for field: _maximoOcorrencias
     */
    private boolean _has_maximoOcorrencias;

    /**
     * Field _cdOrgaoPagadorOrganizacao
     */
    private int _cdOrgaoPagadorOrganizacao = 0;

    /**
     * keeps track of state for field: _cdOrgaoPagadorOrganizacao
     */
    private boolean _has_cdOrgaoPagadorOrganizacao;

    /**
     * Field _cdTipoUnidadeOrganizacao
     */
    private int _cdTipoUnidadeOrganizacao = 0;

    /**
     * keeps track of state for field: _cdTipoUnidadeOrganizacao
     */
    private boolean _has_cdTipoUnidadeOrganizacao;

    /**
     * Field _cdPessoaJuridica
     */
    private long _cdPessoaJuridica = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridica
     */
    private boolean _has_cdPessoaJuridica;

    /**
     * Field _nrSequenciaUnidadeOrganizacao
     */
    private int _nrSequenciaUnidadeOrganizacao = 0;

    /**
     * keeps track of state for field: _nrSequenciaUnidadeOrganizaca
     */
    private boolean _has_nrSequenciaUnidadeOrganizacao;

    /**
     * Field _cdSituacaoOrgaoPagador
     */
    private int _cdSituacaoOrgaoPagador = 0;

    /**
     * keeps track of state for field: _cdSituacaoOrgaoPagador
     */
    private boolean _has_cdSituacaoOrgaoPagador;

    /**
     * Field _cdTarifaOrgaoPagador
     */
    private int _cdTarifaOrgaoPagador = 0;

    /**
     * keeps track of state for field: _cdTarifaOrgaoPagador
     */
    private boolean _has_cdTarifaOrgaoPagador;


      //----------------/
     //- Constructors -/
    //----------------/

    public ListarOrgaoPagadorRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarorgaopagador.request.ListarOrgaoPagadorRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdOrgaoPagadorOrganizacao
     * 
     */
    public void deleteCdOrgaoPagadorOrganizacao()
    {
        this._has_cdOrgaoPagadorOrganizacao= false;
    } //-- void deleteCdOrgaoPagadorOrganizacao() 

    /**
     * Method deleteCdPessoaJuridica
     * 
     */
    public void deleteCdPessoaJuridica()
    {
        this._has_cdPessoaJuridica= false;
    } //-- void deleteCdPessoaJuridica() 

    /**
     * Method deleteCdSituacaoOrgaoPagador
     * 
     */
    public void deleteCdSituacaoOrgaoPagador()
    {
        this._has_cdSituacaoOrgaoPagador= false;
    } //-- void deleteCdSituacaoOrgaoPagador() 

    /**
     * Method deleteCdTarifaOrgaoPagador
     * 
     */
    public void deleteCdTarifaOrgaoPagador()
    {
        this._has_cdTarifaOrgaoPagador= false;
    } //-- void deleteCdTarifaOrgaoPagador() 

    /**
     * Method deleteCdTipoUnidadeOrganizacao
     * 
     */
    public void deleteCdTipoUnidadeOrganizacao()
    {
        this._has_cdTipoUnidadeOrganizacao= false;
    } //-- void deleteCdTipoUnidadeOrganizacao() 

    /**
     * Method deleteMaximoOcorrencias
     * 
     */
    public void deleteMaximoOcorrencias()
    {
        this._has_maximoOcorrencias= false;
    } //-- void deleteMaximoOcorrencias() 

    /**
     * Method deleteNrSequenciaUnidadeOrganizacao
     * 
     */
    public void deleteNrSequenciaUnidadeOrganizacao()
    {
        this._has_nrSequenciaUnidadeOrganizacao= false;
    } //-- void deleteNrSequenciaUnidadeOrganizacao() 

    /**
     * Returns the value of field 'cdOrgaoPagadorOrganizacao'.
     * 
     * @return int
     * @return the value of field 'cdOrgaoPagadorOrganizacao'.
     */
    public int getCdOrgaoPagadorOrganizacao()
    {
        return this._cdOrgaoPagadorOrganizacao;
    } //-- int getCdOrgaoPagadorOrganizacao() 

    /**
     * Returns the value of field 'cdPessoaJuridica'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridica'.
     */
    public long getCdPessoaJuridica()
    {
        return this._cdPessoaJuridica;
    } //-- long getCdPessoaJuridica() 

    /**
     * Returns the value of field 'cdSituacaoOrgaoPagador'.
     * 
     * @return int
     * @return the value of field 'cdSituacaoOrgaoPagador'.
     */
    public int getCdSituacaoOrgaoPagador()
    {
        return this._cdSituacaoOrgaoPagador;
    } //-- int getCdSituacaoOrgaoPagador() 

    /**
     * Returns the value of field 'cdTarifaOrgaoPagador'.
     * 
     * @return int
     * @return the value of field 'cdTarifaOrgaoPagador'.
     */
    public int getCdTarifaOrgaoPagador()
    {
        return this._cdTarifaOrgaoPagador;
    } //-- int getCdTarifaOrgaoPagador() 

    /**
     * Returns the value of field 'cdTipoUnidadeOrganizacao'.
     * 
     * @return int
     * @return the value of field 'cdTipoUnidadeOrganizacao'.
     */
    public int getCdTipoUnidadeOrganizacao()
    {
        return this._cdTipoUnidadeOrganizacao;
    } //-- int getCdTipoUnidadeOrganizacao() 

    /**
     * Returns the value of field 'maximoOcorrencias'.
     * 
     * @return int
     * @return the value of field 'maximoOcorrencias'.
     */
    public int getMaximoOcorrencias()
    {
        return this._maximoOcorrencias;
    } //-- int getMaximoOcorrencias() 

    /**
     * Returns the value of field 'nrSequenciaUnidadeOrganizacao'.
     * 
     * @return int
     * @return the value of field 'nrSequenciaUnidadeOrganizacao'.
     */
    public int getNrSequenciaUnidadeOrganizacao()
    {
        return this._nrSequenciaUnidadeOrganizacao;
    } //-- int getNrSequenciaUnidadeOrganizacao() 

    /**
     * Method hasCdOrgaoPagadorOrganizacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdOrgaoPagadorOrganizacao()
    {
        return this._has_cdOrgaoPagadorOrganizacao;
    } //-- boolean hasCdOrgaoPagadorOrganizacao() 

    /**
     * Method hasCdPessoaJuridica
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridica()
    {
        return this._has_cdPessoaJuridica;
    } //-- boolean hasCdPessoaJuridica() 

    /**
     * Method hasCdSituacaoOrgaoPagador
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSituacaoOrgaoPagador()
    {
        return this._has_cdSituacaoOrgaoPagador;
    } //-- boolean hasCdSituacaoOrgaoPagador() 

    /**
     * Method hasCdTarifaOrgaoPagador
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTarifaOrgaoPagador()
    {
        return this._has_cdTarifaOrgaoPagador;
    } //-- boolean hasCdTarifaOrgaoPagador() 

    /**
     * Method hasCdTipoUnidadeOrganizacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoUnidadeOrganizacao()
    {
        return this._has_cdTipoUnidadeOrganizacao;
    } //-- boolean hasCdTipoUnidadeOrganizacao() 

    /**
     * Method hasMaximoOcorrencias
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasMaximoOcorrencias()
    {
        return this._has_maximoOcorrencias;
    } //-- boolean hasMaximoOcorrencias() 

    /**
     * Method hasNrSequenciaUnidadeOrganizacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaUnidadeOrganizacao()
    {
        return this._has_nrSequenciaUnidadeOrganizacao;
    } //-- boolean hasNrSequenciaUnidadeOrganizacao() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdOrgaoPagadorOrganizacao'.
     * 
     * @param cdOrgaoPagadorOrganizacao the value of field
     * 'cdOrgaoPagadorOrganizacao'.
     */
    public void setCdOrgaoPagadorOrganizacao(int cdOrgaoPagadorOrganizacao)
    {
        this._cdOrgaoPagadorOrganizacao = cdOrgaoPagadorOrganizacao;
        this._has_cdOrgaoPagadorOrganizacao = true;
    } //-- void setCdOrgaoPagadorOrganizacao(int) 

    /**
     * Sets the value of field 'cdPessoaJuridica'.
     * 
     * @param cdPessoaJuridica the value of field 'cdPessoaJuridica'
     */
    public void setCdPessoaJuridica(long cdPessoaJuridica)
    {
        this._cdPessoaJuridica = cdPessoaJuridica;
        this._has_cdPessoaJuridica = true;
    } //-- void setCdPessoaJuridica(long) 

    /**
     * Sets the value of field 'cdSituacaoOrgaoPagador'.
     * 
     * @param cdSituacaoOrgaoPagador the value of field
     * 'cdSituacaoOrgaoPagador'.
     */
    public void setCdSituacaoOrgaoPagador(int cdSituacaoOrgaoPagador)
    {
        this._cdSituacaoOrgaoPagador = cdSituacaoOrgaoPagador;
        this._has_cdSituacaoOrgaoPagador = true;
    } //-- void setCdSituacaoOrgaoPagador(int) 

    /**
     * Sets the value of field 'cdTarifaOrgaoPagador'.
     * 
     * @param cdTarifaOrgaoPagador the value of field
     * 'cdTarifaOrgaoPagador'.
     */
    public void setCdTarifaOrgaoPagador(int cdTarifaOrgaoPagador)
    {
        this._cdTarifaOrgaoPagador = cdTarifaOrgaoPagador;
        this._has_cdTarifaOrgaoPagador = true;
    } //-- void setCdTarifaOrgaoPagador(int) 

    /**
     * Sets the value of field 'cdTipoUnidadeOrganizacao'.
     * 
     * @param cdTipoUnidadeOrganizacao the value of field
     * 'cdTipoUnidadeOrganizacao'.
     */
    public void setCdTipoUnidadeOrganizacao(int cdTipoUnidadeOrganizacao)
    {
        this._cdTipoUnidadeOrganizacao = cdTipoUnidadeOrganizacao;
        this._has_cdTipoUnidadeOrganizacao = true;
    } //-- void setCdTipoUnidadeOrganizacao(int) 

    /**
     * Sets the value of field 'maximoOcorrencias'.
     * 
     * @param maximoOcorrencias the value of field
     * 'maximoOcorrencias'.
     */
    public void setMaximoOcorrencias(int maximoOcorrencias)
    {
        this._maximoOcorrencias = maximoOcorrencias;
        this._has_maximoOcorrencias = true;
    } //-- void setMaximoOcorrencias(int) 

    /**
     * Sets the value of field 'nrSequenciaUnidadeOrganizacao'.
     * 
     * @param nrSequenciaUnidadeOrganizacao the value of field
     * 'nrSequenciaUnidadeOrganizacao'.
     */
    public void setNrSequenciaUnidadeOrganizacao(int nrSequenciaUnidadeOrganizacao)
    {
        this._nrSequenciaUnidadeOrganizacao = nrSequenciaUnidadeOrganizacao;
        this._has_nrSequenciaUnidadeOrganizacao = true;
    } //-- void setNrSequenciaUnidadeOrganizacao(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ListarOrgaoPagadorRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listarorgaopagador.request.ListarOrgaoPagadorRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listarorgaopagador.request.ListarOrgaoPagadorRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listarorgaopagador.request.ListarOrgaoPagadorRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarorgaopagador.request.ListarOrgaoPagadorRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
