/*
 * Nome: br.com.bradesco.web.pgit.service.business.contrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 19/02/2016
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.contrato.bean;


/**
 * Nome: ImprimirContratoOcorrencias2SaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ImprimirContratoOcorrencias2SaidaDTO {

	/** The cd indicador salario contrato. */
	private String cdIndicadorSalarioContrato = null;
	
    /** The cd indicador modalidade salario. */
    private Integer cdIndicadorModalidadeSalario = null;

    /** The cd produto relacionado salario. */
    private Integer cdProdutoRelacionadoSalario = null;

    /** The ds produto relacionado salario. */
    private String dsProdutoRelacionadoSalario = null;

    /** The cd mome processamento salario. */
    private Integer cdMomeProcessamentoSalario = null;

    /** The ds mome processamento salario. */
    private String dsMomeProcessamentoSalario = null;

    /** The cd pagamento util salario. */
    private Integer cdPagamentoUtilSalario = null;

    /** The ds pagamento util salario. */
    private String dsPagamentoUtilSalario = null;

    /** The nr quantidade dia f loat salario. */
    private Integer nrQuantidadeDiaFLoatSalario = null;

    /** The cd indicador feriado salario. */
    private Integer cdIndicadorFeriadoSalario = null;

    /** The ds indicador feriado salario. */
    private String dsIndicadorFeriadoSalario = null;

    /** The cd indicador repique salario. */
    private String cdIndicadorRepiqueSalario = null;

    /** The nr quantidade repique salario. */
    private Integer nrQuantidadeRepiqueSalario = null;

    /**
     * Instancia um novo ImprimirContratoOcorrencias2SaidaDTO.
     * 
     * @see
     */
    public ImprimirContratoOcorrencias2SaidaDTO() {
        super();
    }

    /**
     * Sets the cd indicador modalidade salario.
     *
     * @param cdIndicadorModalidadeSalario the cd indicador modalidade salario
     */
    public void setCdIndicadorModalidadeSalario(Integer cdIndicadorModalidadeSalario) {
        this.cdIndicadorModalidadeSalario = cdIndicadorModalidadeSalario;
    }

    /**
     * Gets the cd indicador modalidade salario.
     *
     * @return the cd indicador modalidade salario
     */
    public Integer getCdIndicadorModalidadeSalario() {
        return this.cdIndicadorModalidadeSalario;
    }

    /**
     * Sets the cd produto relacionado salario.
     *
     * @param cdProdutoRelacionadoSalario the cd produto relacionado salario
     */
    public void setCdProdutoRelacionadoSalario(Integer cdProdutoRelacionadoSalario) {
        this.cdProdutoRelacionadoSalario = cdProdutoRelacionadoSalario;
    }

    /**
     * Gets the cd produto relacionado salario.
     *
     * @return the cd produto relacionado salario
     */
    public Integer getCdProdutoRelacionadoSalario() {
        return this.cdProdutoRelacionadoSalario;
    }

    /**
     * Sets the ds produto relacionado salario.
     *
     * @param dsProdutoRelacionadoSalario the ds produto relacionado salario
     */
    public void setDsProdutoRelacionadoSalario(String dsProdutoRelacionadoSalario) {
        this.dsProdutoRelacionadoSalario = dsProdutoRelacionadoSalario;
    }

    /**
     * Gets the ds produto relacionado salario.
     *
     * @return the ds produto relacionado salario
     */
    public String getDsProdutoRelacionadoSalario() {
        return this.dsProdutoRelacionadoSalario;
    }

    /**
     * Sets the cd mome processamento salario.
     *
     * @param cdMomeProcessamentoSalario the cd mome processamento salario
     */
    public void setCdMomeProcessamentoSalario(Integer cdMomeProcessamentoSalario) {
        this.cdMomeProcessamentoSalario = cdMomeProcessamentoSalario;
    }

    /**
     * Gets the cd mome processamento salario.
     *
     * @return the cd mome processamento salario
     */
    public Integer getCdMomeProcessamentoSalario() {
        return this.cdMomeProcessamentoSalario;
    }

    /**
     * Sets the ds mome processamento salario.
     *
     * @param dsMomeProcessamentoSalario the ds mome processamento salario
     */
    public void setDsMomeProcessamentoSalario(String dsMomeProcessamentoSalario) {
        this.dsMomeProcessamentoSalario = dsMomeProcessamentoSalario;
    }

    /**
     * Gets the ds mome processamento salario.
     *
     * @return the ds mome processamento salario
     */
    public String getDsMomeProcessamentoSalario() {
        return this.dsMomeProcessamentoSalario;
    }

    /**
     * Sets the cd pagamento util salario.
     *
     * @param cdPagamentoUtilSalario the cd pagamento util salario
     */
    public void setCdPagamentoUtilSalario(Integer cdPagamentoUtilSalario) {
        this.cdPagamentoUtilSalario = cdPagamentoUtilSalario;
    }

    /**
     * Gets the cd pagamento util salario.
     *
     * @return the cd pagamento util salario
     */
    public Integer getCdPagamentoUtilSalario() {
        return this.cdPagamentoUtilSalario;
    }

    /**
     * Sets the ds pagamento util salario.
     *
     * @param dsPagamentoUtilSalario the ds pagamento util salario
     */
    public void setDsPagamentoUtilSalario(String dsPagamentoUtilSalario) {
        this.dsPagamentoUtilSalario = dsPagamentoUtilSalario;
    }

    /**
     * Gets the ds pagamento util salario.
     *
     * @return the ds pagamento util salario
     */
    public String getDsPagamentoUtilSalario() {
        return this.dsPagamentoUtilSalario;
    }

    /**
     * Sets the nr quantidade dia f loat salario.
     *
     * @param nrQuantidadeDiaFLoatSalario the nr quantidade dia f loat salario
     */
    public void setNrQuantidadeDiaFLoatSalario(Integer nrQuantidadeDiaFLoatSalario) {
        this.nrQuantidadeDiaFLoatSalario = nrQuantidadeDiaFLoatSalario;
    }

    /**
     * Gets the nr quantidade dia f loat salario.
     *
     * @return the nr quantidade dia f loat salario
     */
    public Integer getNrQuantidadeDiaFLoatSalario() {
        return this.nrQuantidadeDiaFLoatSalario;
    }

    /**
     * Sets the cd indicador feriado salario.
     *
     * @param cdIndicadorFeriadoSalario the cd indicador feriado salario
     */
    public void setCdIndicadorFeriadoSalario(Integer cdIndicadorFeriadoSalario) {
        this.cdIndicadorFeriadoSalario = cdIndicadorFeriadoSalario;
    }

    /**
     * Gets the cd indicador feriado salario.
     *
     * @return the cd indicador feriado salario
     */
    public Integer getCdIndicadorFeriadoSalario() {
        return this.cdIndicadorFeriadoSalario;
    }

    /**
     * Sets the ds indicador feriado salario.
     *
     * @param dsIndicadorFeriadoSalario the ds indicador feriado salario
     */
    public void setDsIndicadorFeriadoSalario(String dsIndicadorFeriadoSalario) {
        this.dsIndicadorFeriadoSalario = dsIndicadorFeriadoSalario;
    }

    /**
     * Gets the ds indicador feriado salario.
     *
     * @return the ds indicador feriado salario
     */
    public String getDsIndicadorFeriadoSalario() {
        return this.dsIndicadorFeriadoSalario;
    }

    /**
     * Sets the cd indicador repique salario.
     *
     * @param cdIndicadorRepiqueSalario the cd indicador repique salario
     */
    public void setCdIndicadorRepiqueSalario(String cdIndicadorRepiqueSalario) {
        this.cdIndicadorRepiqueSalario = cdIndicadorRepiqueSalario;
    }

    /**
     * Gets the cd indicador repique salario.
     *
     * @return the cd indicador repique salario
     */
    public String getCdIndicadorRepiqueSalario() {
        return this.cdIndicadorRepiqueSalario;
    }

    /**
     * Sets the nr quantidade repique salario.
     *
     * @param nrQuantidadeRepiqueSalario the nr quantidade repique salario
     */
    public void setNrQuantidadeRepiqueSalario(Integer nrQuantidadeRepiqueSalario) {
        this.nrQuantidadeRepiqueSalario = nrQuantidadeRepiqueSalario;
    }

    /**
     * Gets the nr quantidade repique salario.
     *
     * @return the nr quantidade repique salario
     */
    public Integer getNrQuantidadeRepiqueSalario() {
        return this.nrQuantidadeRepiqueSalario;
    }

	/**
	 * Gets the cd indicador salario contrato.
	 *
	 * @return the cd indicador salario contrato
	 */
	public String getCdIndicadorSalarioContrato() {
		return cdIndicadorSalarioContrato;
	}

	/**
	 * Sets the cd indicador salario contrato.
	 *
	 * @param cdIndicadorSalarioContrato the new cd indicador salario contrato
	 */
	public void setCdIndicadorSalarioContrato(String cdIndicadorSalarioContrato) {
		this.cdIndicadorSalarioContrato = cdIndicadorSalarioContrato;
	}
}