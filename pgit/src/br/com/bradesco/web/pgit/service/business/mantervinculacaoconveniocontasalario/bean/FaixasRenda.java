/*
 * Nome: br.com.bradesco.web.pgit.service.business.comun.json.grupos.piramidesalarial
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 13/03/2015
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantervinculacaoconveniocontasalario.bean;

import java.math.BigDecimal;

import br.com.bradesco.web.pgit.utils.NumberUtils;

/**
 * Nome: FaixasRenda
 * <p>
 * Prop�sito:
 * </p>
 * .
 * 
 * @author : todo!
 * @version :
 * @see Gravavel
 */
public class FaixasRenda {

	/** The nu sequencial faixa salarial. */
	private Integer nuSequencialFaixaSalarial  = null;

	/** The vr inicial faixa salarial. */
	private BigDecimal vrInicialFaixaSalarial  = null;

	/** The vr final faixa salarial. */
	private BigDecimal vrFinalFaixaSalarial  = null;

	/** The qt funcionario faixa filial01. */
	private Long qtFuncionarioFaixaFilial01  = null;

	/** The qt funcionario faixa filial02. */
	private Long qtFuncionarioFaixaFilial02  = null;

	/** The qt funcionario faixa filial03. */
	private Long qtFuncionarioFaixaFilial03  = null;

	/** The qt funcionario faixa filial04. */
	private Long qtFuncionarioFaixaFilial04  = null;

	/** The qt funcionario faixa filial05. */
	private Long qtFuncionarioFaixaFilial05  = null;

	/** The qt funcionario faixa filial06. */
	private Long qtFuncionarioFaixaFilial06  = null;

	/** The qt funcionario faixa filial07. */
	private Long qtFuncionarioFaixaFilial07  = null;

	/** The qt funcionario faixa filial08. */
	private Long qtFuncionarioFaixaFilial08  = null;

	/** The qt funcionario faixa filial09. */
	private Long qtFuncionarioFaixaFilial09  = null;

	/** The qt funcionario faixa filial10. */
	private Long qtFuncionarioFaixaFilial10  = null;

	/**
	 * Faixas renda.
	 */
	public FaixasRenda() {
		super();
	}

	/**
	 * Gets the nu sequencial faixa salarial.
	 *
	 * @return the nuSequencialFaixaSalarial
	 */
	public Integer getNuSequencialFaixaSalarial() {
		return nuSequencialFaixaSalarial;
	}

	/**
	 * Sets the nu sequencial faixa salarial.
	 *
	 * @param numSequencialFaixaSalarial the nuSequencialFaixaSalarial to set
	 */
	public void setNuSequencialFaixaSalarial(Integer numSequencialFaixaSalarial) {
		this.nuSequencialFaixaSalarial = numSequencialFaixaSalarial;
	}

	/**
	 * Gets the vr inicial faixa salarial.
	 *
	 * @return the vrInicialFaixaSalarial
	 */
	public BigDecimal getVrInicialFaixaSalarial() {
		return vrInicialFaixaSalarial;
	}

	/**
	 * Sets the vr inicial faixa salarial.
	 *
	 * @param inicialFaixaSalarial the vrInicialFaixaSalarial to set
	 */
	public void setVrInicialFaixaSalarial(BigDecimal inicialFaixaSalarial) {
		this.vrInicialFaixaSalarial = inicialFaixaSalarial;
	}

	/**
	 * Gets the vr final faixa salarial.
	 *
	 * @return the vrFinalFaixaSalarial
	 */
	public BigDecimal getVrFinalFaixaSalarial() {
		return vrFinalFaixaSalarial;
	}

	/**
	 * Sets the vr final faixa salarial.
	 *
	 * @param finalFaixaSalarial the vrFinalFaixaSalarial to set
	 */
	public void setVrFinalFaixaSalarial(BigDecimal finalFaixaSalarial) {
		this.vrFinalFaixaSalarial = finalFaixaSalarial;
	}

	/**
	 * Gets the qt funcionario faixa filial01.
	 *
	 * @return the qtFuncionarioFaixaFilial01
	 */
	public Long getQtFuncionarioFaixaFilial01() {
		return qtFuncionarioFaixaFilial01;
	}

	/**
	 * Sets the qt funcionario faixa filial01.
	 *
	 * @param qtdFuncionarioFaixaFilial01 the qtFuncionarioFaixaFilial01 to set
	 */
	public void setQtFuncionarioFaixaFilial01(Long qtdFuncionarioFaixaFilial01) {
		this.qtFuncionarioFaixaFilial01 = qtdFuncionarioFaixaFilial01;
	}

	/**
	 * Gets the qt funcionario faixa filial02.
	 *
	 * @return the qtFuncionarioFaixaFilial02
	 */
	public Long getQtFuncionarioFaixaFilial02() {
		return qtFuncionarioFaixaFilial02;
	}

	/**
	 * Sets the qt funcionario faixa filial02.
	 *
	 * @param qtdFuncionarioFaixaFilial02 the qtFuncionarioFaixaFilial02 to set
	 */
	public void setQtFuncionarioFaixaFilial02(Long qtdFuncionarioFaixaFilial02) {
		this.qtFuncionarioFaixaFilial02 = qtdFuncionarioFaixaFilial02;
	}

	/**
	 * Gets the qt funcionario faixa filial03.
	 *
	 * @return the qtFuncionarioFaixaFilial03
	 */
	public Long getQtFuncionarioFaixaFilial03() {
		return qtFuncionarioFaixaFilial03;
	}

	/**
	 * Sets the qt funcionario faixa filial03.
	 *
	 * @param qtdFuncionarioFaixaFilial03 the qtFuncionarioFaixaFilial03 to set
	 */
	public void setQtFuncionarioFaixaFilial03(Long qtdFuncionarioFaixaFilial03) {
		this.qtFuncionarioFaixaFilial03 = qtdFuncionarioFaixaFilial03;
	}

	/**
	 * Gets the qt funcionario faixa filial04.
	 *
	 * @return the qtFuncionarioFaixaFilial04
	 */
	public Long getQtFuncionarioFaixaFilial04() {
		return qtFuncionarioFaixaFilial04;
	}

	/**
	 * Sets the qt funcionario faixa filial04.
	 *
	 * @param qtdFuncionarioFaixaFilial04 the qtFuncionarioFaixaFilial04 to set
	 */
	public void setQtFuncionarioFaixaFilial04(Long qtdFuncionarioFaixaFilial04) {
		this.qtFuncionarioFaixaFilial04 = qtdFuncionarioFaixaFilial04;
	}

	/**
	 * Gets the qt funcionario faixa filial05.
	 *
	 * @return the qtFuncionarioFaixaFilial05
	 */
	public Long getQtFuncionarioFaixaFilial05() {
		return qtFuncionarioFaixaFilial05;
	}

	/**
	 * Sets the qt funcionario faixa filial05.
	 *
	 * @param qtdFuncionarioFaixaFilial05 the qtFuncionarioFaixaFilial05 to set
	 */
	public void setQtFuncionarioFaixaFilial05(Long qtdFuncionarioFaixaFilial05) {
		this.qtFuncionarioFaixaFilial05 = qtdFuncionarioFaixaFilial05;
	}

	/**
	 * Gets the qt funcionario faixa filial06.
	 *
	 * @return the qtFuncionarioFaixaFilial06
	 */
	public Long getQtFuncionarioFaixaFilial06() {
		return qtFuncionarioFaixaFilial06;
	}

	/**
	 * Sets the qt funcionario faixa filial06.
	 *
	 * @param qtdFuncionarioFaixaFilial06 the qtFuncionarioFaixaFilial06 to set
	 */
	public void setQtFuncionarioFaixaFilial06(Long qtdFuncionarioFaixaFilial06) {
		this.qtFuncionarioFaixaFilial06 = qtdFuncionarioFaixaFilial06;
	}

	/**
	 * Gets the qt funcionario faixa filial07.
	 *
	 * @return the qtFuncionarioFaixaFilial07
	 */
	public Long getQtFuncionarioFaixaFilial07() {
		return qtFuncionarioFaixaFilial07;
	}

	/**
	 * Sets the qt funcionario faixa filial07.
	 *
	 * @param qtdFuncionarioFaixaFilial07 the qtFuncionarioFaixaFilial07 to set
	 */
	public void setQtFuncionarioFaixaFilial07(Long qtdFuncionarioFaixaFilial07) {
		this.qtFuncionarioFaixaFilial07 = qtdFuncionarioFaixaFilial07;
	}

	/**
	 * Gets the qt funcionario faixa filial08.
	 *
	 * @return the qtFuncionarioFaixaFilial08
	 */
	public Long getQtFuncionarioFaixaFilial08() {
		return qtFuncionarioFaixaFilial08;
	}

	/**
	 * Sets the qt funcionario faixa filial08.
	 *
	 * @param qtdFuncionarioFaixaFilial08 the qtFuncionarioFaixaFilial08 to set
	 */
	public void setQtFuncionarioFaixaFilial08(Long qtdFuncionarioFaixaFilial08) {
		this.qtFuncionarioFaixaFilial08 = qtdFuncionarioFaixaFilial08;
	}

	/**
	 * Gets the qt funcionario faixa filial09.
	 *
	 * @return the qtFuncionarioFaixaFilial09
	 */
	public Long getQtFuncionarioFaixaFilial09() {
		return qtFuncionarioFaixaFilial09;
	}

	/**
	 * Sets the qt funcionario faixa filial09.
	 *
	 * @param qtdFuncionarioFaixaFilial09 the qtFuncionarioFaixaFilial09 to set
	 */
	public void setQtFuncionarioFaixaFilial09(Long qtdFuncionarioFaixaFilial09) {
		this.qtFuncionarioFaixaFilial09 = qtdFuncionarioFaixaFilial09;
	}

	/**
	 * Gets the qt funcionario faixa filial10.
	 *
	 * @return the qtFuncionarioFaixaFilial10
	 */
	public Long getQtFuncionarioFaixaFilial10() {
		return qtFuncionarioFaixaFilial10;
	}

	/**
	 * Sets the qt funcionario faixa filial10.
	 *
	 * @param qtdFuncionarioFaixaFilial10 the qtFuncionarioFaixaFilial10 to set
	 */
	public void setQtFuncionarioFaixaFilial10(Long qtdFuncionarioFaixaFilial10) {
		this.qtFuncionarioFaixaFilial10 = qtdFuncionarioFaixaFilial10;
	}

	/**
	 * Gets the faixa renda formatada.
	 *
	 * @return the faixa renda formatada
	 */
	public String getFaixaRendaFormatada() {
		
		setVrInicialFaixaSalarial(vrInicialFaixaSalarial == null ? BigDecimal.ZERO : vrInicialFaixaSalarial);
		setVrFinalFaixaSalarial(vrFinalFaixaSalarial == null ? BigDecimal.ZERO : vrFinalFaixaSalarial);
		
		String valorInicial = NumberUtils.format(vrInicialFaixaSalarial);
		String valorFinal = NumberUtils.format(vrFinalFaixaSalarial);

		if (vrInicialFaixaSalarial == null || vrInicialFaixaSalarial.compareTo(BigDecimal.ZERO) == 0) {
			return String.format("At� R$ %s", valorFinal);
		}

		if (BigDecimal.ZERO.compareTo(vrFinalFaixaSalarial) == 0
				|| new BigDecimal("999999999999999.99").compareTo(vrFinalFaixaSalarial) == 0) {
			return String.format("Acima de R$ %s", valorInicial);
		}

		return String.format("De R$ %s at� R$ %s", valorInicial, valorFinal);
	}

}