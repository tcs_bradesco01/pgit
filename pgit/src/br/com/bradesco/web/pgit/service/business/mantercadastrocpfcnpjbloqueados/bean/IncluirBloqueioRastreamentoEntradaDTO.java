/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantercadastrocpfcnpjbloqueados.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantercadastrocpfcnpjbloqueados.bean;

/**
 * Nome: IncluirBloqueioRastreamentoEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class IncluirBloqueioRastreamentoEntradaDTO {
	
	/** Atributo cdpessoaJuridicaContrato. */
	private Long cdpessoaJuridicaContrato;
	
	/** Atributo cdTipoContratoNegocio. */
	private Integer cdTipoContratoNegocio;
	
	/** Atributo nrSequenciaContratoNegocio. */
	private Long nrSequenciaContratoNegocio;
	
	/** Atributo cdCpfCnpjBloqueio. */
	private Long cdCpfCnpjBloqueio;
	
	/** Atributo cdFilialCnpjBloqueio. */
	private Integer cdFilialCnpjBloqueio;
	
	/** Atributo cdControleCnpjCpf. */
	private Integer cdControleCnpjCpf;
	
	/**
	 * Get: cdpessoaJuridicaContrato.
	 *
	 * @return cdpessoaJuridicaContrato
	 */
	public Long getCdpessoaJuridicaContrato() {
		return cdpessoaJuridicaContrato;
	}
	
	/**
	 * Set: cdpessoaJuridicaContrato.
	 *
	 * @param cdpessoaJuridicaContrato the cdpessoa juridica contrato
	 */
	public void setCdpessoaJuridicaContrato(Long cdpessoaJuridicaContrato) {
		this.cdpessoaJuridicaContrato = cdpessoaJuridicaContrato;
	}
	
	/**
	 * Get: cdTipoContratoNegocio.
	 *
	 * @return cdTipoContratoNegocio
	 */
	public Integer getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}
	
	/**
	 * Set: cdTipoContratoNegocio.
	 *
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}
	
	/**
	 * Get: nrSequenciaContratoNegocio.
	 *
	 * @return nrSequenciaContratoNegocio
	 */
	public Long getNrSequenciaContratoNegocio() {
		return nrSequenciaContratoNegocio;
	}
	
	/**
	 * Set: nrSequenciaContratoNegocio.
	 *
	 * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
	 */
	public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}
	
	/**
	 * Get: cdCpfCnpjBloqueio.
	 *
	 * @return cdCpfCnpjBloqueio
	 */
	public Long getCdCpfCnpjBloqueio() {
		return cdCpfCnpjBloqueio;
	}
	
	/**
	 * Set: cdCpfCnpjBloqueio.
	 *
	 * @param cdCpfCnpjBloqueio the cd cpf cnpj bloqueio
	 */
	public void setCdCpfCnpjBloqueio(Long cdCpfCnpjBloqueio) {
		this.cdCpfCnpjBloqueio = cdCpfCnpjBloqueio;
	}
	
	/**
	 * Get: cdFilialCnpjBloqueio.
	 *
	 * @return cdFilialCnpjBloqueio
	 */
	public Integer getCdFilialCnpjBloqueio() {
		return cdFilialCnpjBloqueio;
	}
	
	/**
	 * Set: cdFilialCnpjBloqueio.
	 *
	 * @param cdFilialCnpjBloqueio the cd filial cnpj bloqueio
	 */
	public void setCdFilialCnpjBloqueio(Integer cdFilialCnpjBloqueio) {
		this.cdFilialCnpjBloqueio = cdFilialCnpjBloqueio;
	}
	
	/**
	 * Get: cdControleCnpjCpf.
	 *
	 * @return cdControleCnpjCpf
	 */
	public Integer getCdControleCnpjCpf() {
		return cdControleCnpjCpf;
	}
	
	/**
	 * Set: cdControleCnpjCpf.
	 *
	 * @param cdControleCnpjCpf the cd controle cnpj cpf
	 */
	public void setCdControleCnpjCpf(Integer cdControleCnpjCpf) {
		this.cdControleCnpjCpf = cdControleCnpjCpf;
	}
	
	
}
