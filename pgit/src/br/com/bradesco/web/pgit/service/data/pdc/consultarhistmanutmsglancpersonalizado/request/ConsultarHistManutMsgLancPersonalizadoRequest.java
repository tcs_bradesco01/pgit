/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultarhistmanutmsglancpersonalizado.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ConsultarHistManutMsgLancPersonalizadoRequest.
 * 
 * @version $Revision$ $Date$
 */
public class ConsultarHistManutMsgLancPersonalizadoRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdMensagemLinhasExtrato
     */
    private int _cdMensagemLinhasExtrato = 0;

    /**
     * keeps track of state for field: _cdMensagemLinhasExtrato
     */
    private boolean _has_cdMensagemLinhasExtrato;

    /**
     * Field _hrInclusaoRegistroHist
     */
    private java.lang.String _hrInclusaoRegistroHist;

    /**
     * Field _dtInicio
     */
    private java.lang.String _dtInicio;

    /**
     * Field _dtFim
     */
    private java.lang.String _dtFim;

    /**
     * Field _qtOcorrencias
     */
    private int _qtOcorrencias = 0;

    /**
     * keeps track of state for field: _qtOcorrencias
     */
    private boolean _has_qtOcorrencias;


      //----------------/
     //- Constructors -/
    //----------------/

    public ConsultarHistManutMsgLancPersonalizadoRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarhistmanutmsglancpersonalizado.request.ConsultarHistManutMsgLancPersonalizadoRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdMensagemLinhasExtrato
     * 
     */
    public void deleteCdMensagemLinhasExtrato()
    {
        this._has_cdMensagemLinhasExtrato= false;
    } //-- void deleteCdMensagemLinhasExtrato() 

    /**
     * Method deleteQtOcorrencias
     * 
     */
    public void deleteQtOcorrencias()
    {
        this._has_qtOcorrencias= false;
    } //-- void deleteQtOcorrencias() 

    /**
     * Returns the value of field 'cdMensagemLinhasExtrato'.
     * 
     * @return int
     * @return the value of field 'cdMensagemLinhasExtrato'.
     */
    public int getCdMensagemLinhasExtrato()
    {
        return this._cdMensagemLinhasExtrato;
    } //-- int getCdMensagemLinhasExtrato() 

    /**
     * Returns the value of field 'dtFim'.
     * 
     * @return String
     * @return the value of field 'dtFim'.
     */
    public java.lang.String getDtFim()
    {
        return this._dtFim;
    } //-- java.lang.String getDtFim() 

    /**
     * Returns the value of field 'dtInicio'.
     * 
     * @return String
     * @return the value of field 'dtInicio'.
     */
    public java.lang.String getDtInicio()
    {
        return this._dtInicio;
    } //-- java.lang.String getDtInicio() 

    /**
     * Returns the value of field 'hrInclusaoRegistroHist'.
     * 
     * @return String
     * @return the value of field 'hrInclusaoRegistroHist'.
     */
    public java.lang.String getHrInclusaoRegistroHist()
    {
        return this._hrInclusaoRegistroHist;
    } //-- java.lang.String getHrInclusaoRegistroHist() 

    /**
     * Returns the value of field 'qtOcorrencias'.
     * 
     * @return int
     * @return the value of field 'qtOcorrencias'.
     */
    public int getQtOcorrencias()
    {
        return this._qtOcorrencias;
    } //-- int getQtOcorrencias() 

    /**
     * Method hasCdMensagemLinhasExtrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMensagemLinhasExtrato()
    {
        return this._has_cdMensagemLinhasExtrato;
    } //-- boolean hasCdMensagemLinhasExtrato() 

    /**
     * Method hasQtOcorrencias
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtOcorrencias()
    {
        return this._has_qtOcorrencias;
    } //-- boolean hasQtOcorrencias() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdMensagemLinhasExtrato'.
     * 
     * @param cdMensagemLinhasExtrato the value of field
     * 'cdMensagemLinhasExtrato'.
     */
    public void setCdMensagemLinhasExtrato(int cdMensagemLinhasExtrato)
    {
        this._cdMensagemLinhasExtrato = cdMensagemLinhasExtrato;
        this._has_cdMensagemLinhasExtrato = true;
    } //-- void setCdMensagemLinhasExtrato(int) 

    /**
     * Sets the value of field 'dtFim'.
     * 
     * @param dtFim the value of field 'dtFim'.
     */
    public void setDtFim(java.lang.String dtFim)
    {
        this._dtFim = dtFim;
    } //-- void setDtFim(java.lang.String) 

    /**
     * Sets the value of field 'dtInicio'.
     * 
     * @param dtInicio the value of field 'dtInicio'.
     */
    public void setDtInicio(java.lang.String dtInicio)
    {
        this._dtInicio = dtInicio;
    } //-- void setDtInicio(java.lang.String) 

    /**
     * Sets the value of field 'hrInclusaoRegistroHist'.
     * 
     * @param hrInclusaoRegistroHist the value of field
     * 'hrInclusaoRegistroHist'.
     */
    public void setHrInclusaoRegistroHist(java.lang.String hrInclusaoRegistroHist)
    {
        this._hrInclusaoRegistroHist = hrInclusaoRegistroHist;
    } //-- void setHrInclusaoRegistroHist(java.lang.String) 

    /**
     * Sets the value of field 'qtOcorrencias'.
     * 
     * @param qtOcorrencias the value of field 'qtOcorrencias'.
     */
    public void setQtOcorrencias(int qtOcorrencias)
    {
        this._qtOcorrencias = qtOcorrencias;
        this._has_qtOcorrencias = true;
    } //-- void setQtOcorrencias(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ConsultarHistManutMsgLancPersonalizadoRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultarhistmanutmsglancpersonalizado.request.ConsultarHistManutMsgLancPersonalizadoRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultarhistmanutmsglancpersonalizado.request.ConsultarHistManutMsgLancPersonalizadoRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultarhistmanutmsglancpersonalizado.request.ConsultarHistManutMsgLancPersonalizadoRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarhistmanutmsglancpersonalizado.request.ConsultarHistManutMsgLancPersonalizadoRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
