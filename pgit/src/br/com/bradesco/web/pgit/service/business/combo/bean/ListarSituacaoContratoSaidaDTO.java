/*
 * Nome: br.com.bradesco.web.pgit.service.business.combo.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.combo.bean;


/**
 * Nome: ListarSituacaoContratoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarSituacaoContratoSaidaDTO {

	/** Atributo cdSituacaoContrato. */
	private Integer cdSituacaoContrato;
    
    /** Atributo dsSituacaoContrato. */
    private String dsSituacaoContrato;

    /**
     * Listar situacao contrato saida dto.
     *
     * @param cdSituacaoContrato the cd situacao contrato
     * @param dsSituacaoContrato the ds situacao contrato
     */
    public ListarSituacaoContratoSaidaDTO(Integer cdSituacaoContrato, String dsSituacaoContrato) {
		super();
		this.cdSituacaoContrato = cdSituacaoContrato;
		this.dsSituacaoContrato = dsSituacaoContrato;
	}

	/**
	 * Get: cdSituacaoContrato.
	 *
	 * @return cdSituacaoContrato
	 */
	public int getCdSituacaoContrato() {
		return cdSituacaoContrato;
	}
	
	/**
	 * Set: cdSituacaoContrato.
	 *
	 * @param cdSituacaoContrato the cd situacao contrato
	 */
	public void setCdSituacaoContrato(int cdSituacaoContrato) {
		this.cdSituacaoContrato = cdSituacaoContrato;
	}
	
	/**
	 * Get: dsSituacaoContrato.
	 *
	 * @return dsSituacaoContrato
	 */
	public String getDsSituacaoContrato() {
		return dsSituacaoContrato;
	}
	
	/**
	 * Set: dsSituacaoContrato.
	 *
	 * @param dsSituacaoContrato the ds situacao contrato
	 */
	public void setDsSituacaoContrato(String dsSituacaoContrato) {
		this.dsSituacaoContrato = dsSituacaoContrato;
	}
}
