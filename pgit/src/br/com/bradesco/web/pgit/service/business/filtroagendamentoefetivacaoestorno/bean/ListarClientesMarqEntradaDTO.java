/*
 * Nome: br.com.bradesco.web.pgit.service.business.filtroagendamentoefetivacaoestorno.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.filtroagendamentoefetivacaoestorno.bean;

/**
 * Nome: ListarClientesMarqEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarClientesMarqEntradaDTO{
    
    /** Atributo cdCpfCnpj. */
    private Long cdCpfCnpj;
    
    /** Atributo cdFilialCnpj. */
    private Integer cdFilialCnpj;
    
    /** Atributo cdControleCnpj. */
    private Integer cdControleCnpj;
    
    /** Atributo cdBanco. */
    private Integer cdBanco;
    
    /** Atributo cdAgenciaBancaria. */
    private Integer cdAgenciaBancaria;
    
    /** Atributo cdContaBancaria. */
    private Long cdContaBancaria;
    
    /** Atributo dtNascimentoFund. */
    private String dtNascimentoFund;
    
    /** Atributo dsNomeRazao. */
    private String dsNomeRazao;
    
    /** Atributo cdTipoConta. */
    private Integer cdTipoConta;
    
	/**
	 * Get: cdAgenciaBancaria.
	 *
	 * @return cdAgenciaBancaria
	 */
	public Integer getCdAgenciaBancaria() {
		return cdAgenciaBancaria;
	}
	
	/**
	 * Set: cdAgenciaBancaria.
	 *
	 * @param cdAgenciaBancaria the cd agencia bancaria
	 */
	public void setCdAgenciaBancaria(Integer cdAgenciaBancaria) {
		this.cdAgenciaBancaria = cdAgenciaBancaria;
	}
	
	/**
	 * Get: cdBanco.
	 *
	 * @return cdBanco
	 */
	public Integer getCdBanco() {
		return cdBanco;
	}
	
	/**
	 * Set: cdBanco.
	 *
	 * @param cdBanco the cd banco
	 */
	public void setCdBanco(Integer cdBanco) {
		this.cdBanco = cdBanco;
	}
	
	/**
	 * Get: cdContaBancaria.
	 *
	 * @return cdContaBancaria
	 */
	public Long getCdContaBancaria() {
		return cdContaBancaria;
	}
	
	/**
	 * Set: cdContaBancaria.
	 *
	 * @param cdContaBancaria the cd conta bancaria
	 */
	public void setCdContaBancaria(Long cdContaBancaria) {
		this.cdContaBancaria = cdContaBancaria;
	}
	
	/**
	 * Get: cdControleCnpj.
	 *
	 * @return cdControleCnpj
	 */
	public Integer getCdControleCnpj() {
		return cdControleCnpj;
	}
	
	/**
	 * Set: cdControleCnpj.
	 *
	 * @param cdControleCnpj the cd controle cnpj
	 */
	public void setCdControleCnpj(Integer cdControleCnpj) {
		this.cdControleCnpj = cdControleCnpj;
	}
	
	/**
	 * Get: cdCpfCnpj.
	 *
	 * @return cdCpfCnpj
	 */
	public Long getCdCpfCnpj() {
		return cdCpfCnpj;
	}
	
	/**
	 * Set: cdCpfCnpj.
	 *
	 * @param cdCpfCnpj the cd cpf cnpj
	 */
	public void setCdCpfCnpj(Long cdCpfCnpj) {
		this.cdCpfCnpj = cdCpfCnpj;
	}
	
	/**
	 * Get: cdFilialCnpj.
	 *
	 * @return cdFilialCnpj
	 */
	public Integer getCdFilialCnpj() {
		return cdFilialCnpj;
	}
	
	/**
	 * Set: cdFilialCnpj.
	 *
	 * @param cdFilialCnpj the cd filial cnpj
	 */
	public void setCdFilialCnpj(Integer cdFilialCnpj) {
		this.cdFilialCnpj = cdFilialCnpj;
	}
	
	/**
	 * Get: cdTipoConta.
	 *
	 * @return cdTipoConta
	 */
	public Integer getCdTipoConta() {
		return cdTipoConta;
	}
	
	/**
	 * Set: cdTipoConta.
	 *
	 * @param cdTipoConta the cd tipo conta
	 */
	public void setCdTipoConta(Integer cdTipoConta) {
		this.cdTipoConta = cdTipoConta;
	}
	
	/**
	 * Get: dsNomeRazao.
	 *
	 * @return dsNomeRazao
	 */
	public String getDsNomeRazao() {
		return dsNomeRazao;
	}
	
	/**
	 * Set: dsNomeRazao.
	 *
	 * @param dsNomeRazao the ds nome razao
	 */
	public void setDsNomeRazao(String dsNomeRazao) {
		this.dsNomeRazao = dsNomeRazao;
	}
	
	/**
	 * Get: dtNascimentoFund.
	 *
	 * @return dtNascimentoFund
	 */
	public String getDtNascimentoFund() {
		return dtNascimentoFund;
	}
	
	/**
	 * Set: dtNascimentoFund.
	 *
	 * @param dtNascimentoFund the dt nascimento fund
	 */
	public void setDtNascimentoFund(String dtNascimentoFund) {
		this.dtNascimentoFund = dtNascimentoFund;
	}
    
}
