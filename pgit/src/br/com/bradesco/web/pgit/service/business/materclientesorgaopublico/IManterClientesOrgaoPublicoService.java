/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/interfaz.ftl,v $
 * $Id: interfaz.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: interfaz.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.materclientesorgaopublico;

import br.com.bradesco.web.pgit.service.business.materclientesorgaopublico.bean.AlterarParticipantesOrgaoPublicoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.materclientesorgaopublico.bean.AlterarParticipantesOrgaoPublicoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.materclientesorgaopublico.bean.ConsultarUsuarioSoliRelatorioOrgaosPublicosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.materclientesorgaopublico.bean.ConsultarUsuarioSoliRelatorioOrgaosPublicosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.materclientesorgaopublico.bean.DetalharParticipantesOrgaoPublicoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.materclientesorgaopublico.bean.DetalharParticipantesOrgaoPublicoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.materclientesorgaopublico.bean.ExcluirParticipantesOrgaoPublicoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.materclientesorgaopublico.bean.ExcluirParticipantesOrgaoPublicoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.materclientesorgaopublico.bean.ExcluirSoliRelatorioOrgaosPublicosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.materclientesorgaopublico.bean.ExcluirSoliRelatorioOrgaosPublicosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.materclientesorgaopublico.bean.IncluirParticipantesOrgaoPublicoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.materclientesorgaopublico.bean.IncluirParticipantesOrgaoPublicoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.materclientesorgaopublico.bean.IncluirSoliRelatorioOrgaosPublicosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.materclientesorgaopublico.bean.ListarCtasVincPartOrgaoPublicoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.materclientesorgaopublico.bean.ListarCtasVincPartOrgaoPublicoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.materclientesorgaopublico.bean.ListarParticipantesOrgaoPublicoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.materclientesorgaopublico.bean.ListarParticipantesOrgaoPublicoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.materclientesorgaopublico.bean.ListarSoliRelatorioOrgaosPublicosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.materclientesorgaopublico.bean.ListarSoliRelatorioOrgaosPublicosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.materclientesorgaopublico.bean.ValidarContratoPagSalPartOrgaoPublicoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.materclientesorgaopublico.bean.ValidarContratoPagSalPartOrgaoPublicoSaidaDTO;

/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Interface do adaptador: ManterCompTipoServicoFormaLanc
 * </p>
 * 
 * @comment CODIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public interface IManterClientesOrgaoPublicoService {
	
	/**
	 * Listar soli relatorio orgaos publicos.
	 *
	 * @param entrada the entrada
	 * @return the listar soli relatorio orgaos publicos saida dto
	 */
	ListarSoliRelatorioOrgaosPublicosSaidaDTO listarSoliRelatorioOrgaosPublicos(ListarSoliRelatorioOrgaosPublicosEntradaDTO entrada);
	
	/**
	 * Excluir soli relatorio orgaos publicos.
	 *
	 * @param entrada the entrada
	 * @return the excluir soli relatorio orgaos publicos saida dto
	 */
	ExcluirSoliRelatorioOrgaosPublicosSaidaDTO excluirSoliRelatorioOrgaosPublicos(ExcluirSoliRelatorioOrgaosPublicosEntradaDTO entrada);
	
	/**
	 * Consultar usuario soli relatorio orgaos publicos.
	 *
	 * @param entrada the entrada
	 * @return the consultar usuario soli relatorio orgaos publicos saida dto
	 */
	ConsultarUsuarioSoliRelatorioOrgaosPublicosSaidaDTO consultarUsuarioSoliRelatorioOrgaosPublicos(ConsultarUsuarioSoliRelatorioOrgaosPublicosEntradaDTO entrada);
	
	/**
	 * Incluir soli relatorio orgaos publicos saida dto.
	 *
	 * @return the incluir soli relatorio orgaos publicos saida dto
	 */
	IncluirSoliRelatorioOrgaosPublicosSaidaDTO incluirSoliRelatorioOrgaosPublicosSaidaDTO();
	
	/**
	 * Listar participantes orgao publico.
	 *
	 * @param entrada the entrada
	 * @return the listar participantes orgao publico saida dto
	 */
	ListarParticipantesOrgaoPublicoSaidaDTO listarParticipantesOrgaoPublico(ListarParticipantesOrgaoPublicoEntradaDTO entrada);
	
	/**
	 * Detalhar participante orgao publico.
	 *
	 * @param entrada the entrada
	 * @return the detalhar participantes orgao publico saida dto
	 */
	DetalharParticipantesOrgaoPublicoSaidaDTO detalharParticipanteOrgaoPublico(DetalharParticipantesOrgaoPublicoEntradaDTO entrada);
	
	/**
	 * Excluir participante orgao publico.
	 *
	 * @param entrada the entrada
	 * @return the excluir participantes orgao publico saida dto
	 */
	ExcluirParticipantesOrgaoPublicoSaidaDTO excluirParticipanteOrgaoPublico(ExcluirParticipantesOrgaoPublicoEntradaDTO entrada);
	
	/**
	 * Alterar participante orgao publico.
	 *
	 * @param entrada the entrada
	 * @return the alterar participantes orgao publico saida dto
	 */
	AlterarParticipantesOrgaoPublicoSaidaDTO alterarParticipanteOrgaoPublico(AlterarParticipantesOrgaoPublicoEntradaDTO entrada);
	
	/**
	 * Validar participante orgao publico.
	 *
	 * @param entrada the entrada
	 * @return the validar contrato pag sal part orgao publico saida dto
	 */
	ValidarContratoPagSalPartOrgaoPublicoSaidaDTO validarParticipanteOrgaoPublico(ValidarContratoPagSalPartOrgaoPublicoEntradaDTO entrada);
	
	/**
	 * Listar ctas vinc part orgao publico.
	 *
	 * @param entrada the entrada
	 * @return the listar ctas vinc part orgao publico saida dto
	 */
	ListarCtasVincPartOrgaoPublicoSaidaDTO listarCtasVincPartOrgaoPublico(ListarCtasVincPartOrgaoPublicoEntradaDTO entrada);
	
	/**
	 * Incluir participante orgao publico.
	 *
	 * @param entrada the entrada
	 * @return the incluir participantes orgao publico saida dto
	 */
	IncluirParticipantesOrgaoPublicoSaidaDTO incluirParticipanteOrgaoPublico(IncluirParticipantesOrgaoPublicoEntradaDTO entrada);
}

