/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/implementation.ftl,v $
 * $Id: implementation.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: implementation.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */
 
package br.com.bradesco.web.pgit.service.business.manterservicocomposto.impl;

import java.util.ArrayList;
import java.util.List;

import br.com.bradesco.web.pgit.service.business.manterservicocomposto.IManterServicoCompostoService;
import br.com.bradesco.web.pgit.service.business.manterservicocomposto.bean.AlterarServicoCompostoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterservicocomposto.bean.AlterarServicoCompostoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterservicocomposto.bean.DetalharServicoCompostoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterservicocomposto.bean.DetalharServicoCompostoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterservicocomposto.bean.ExcluirServicoCompostoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterservicocomposto.bean.ExcluirServicoCompostoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterservicocomposto.bean.IncluirServicoCompostoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterservicocomposto.bean.IncluirServicoCompostoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterservicocomposto.bean.ListarServicoCompostoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterservicocomposto.bean.ListarServicoCompostoSaidaDTO;
import br.com.bradesco.web.pgit.service.data.pdc.FactoryAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.alterarservicocomposto.request.AlterarServicoCompostoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.alterarservicocomposto.response.AlterarServicoCompostoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.detalharservicocomposto.request.DetalharServicoCompostoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.detalharservicocomposto.response.DetalharServicoCompostoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.excluirservicocomposto.request.ExcluirServicoCompostoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.excluirservicocomposto.response.ExcluirServicoCompostoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.incluirservicocomposto.request.IncluirServicoCompostoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.incluirservicocomposto.response.IncluirServicoCompostoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarservicocomposto.request.ListarServicoCompostoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarservicocomposto.response.ListarServicoCompostoResponse;
import br.com.bradesco.web.pgit.utils.PgitUtil;
import br.com.bradesco.web.pgit.view.converters.FormatarData;



/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Implementa��o do adaptador: ManterServicoComposto
 * </p>
 * 
 * @comment C�DIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public class ManterServicoCompostoServiceImpl implements IManterServicoCompostoService {


	/** Atributo factoryAdapter. */
	private FactoryAdapter factoryAdapter;
	
    /**
     * Construtor.
     */
    public ManterServicoCompostoServiceImpl() {
    	super();
        // TODO: Implementa��o
    }

	/*
	 * Listar
	 */
    /**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.manterservicocomposto.IManterServicoCompostoService#consultarServicoComposto(br.com.bradesco.web.pgit.service.business.manterservicocomposto.bean.ListarServicoCompostoEntradaDTO)
	 */
	public List<ListarServicoCompostoSaidaDTO> consultarServicoComposto(ListarServicoCompostoEntradaDTO entrada){

		ListarServicoCompostoRequest request = new ListarServicoCompostoRequest();

		request.setCdTipoServicoCnab(0);
		request.setCdFormaLiquidacao(0);
		request.setCdFormaLancamentoCnab(PgitUtil.verificaIntegerNulo(entrada.getCdFormaLancamentoCnab()));
		request.setCdServicoCompostoPagamento(PgitUtil.verificaLongNulo(entrada.getCdServicoCompostoPagamento()));
		request.setDsServicoCompostoPagamento(PgitUtil.verificaStringNula(entrada.getDsServicoCompostoPagamento()));
		request.setCdIndLctoFuturo(PgitUtil.verificaIntegerNulo(entrada.getCdIndLctoFuturo()));
		request.setQtDiaLctoFuturo(PgitUtil.verificaIntegerNulo(entrada.getQtDiaLctoFuturo()));
		request.setQtOcorrencias(entrada.getQtOcorrencias());

		ListarServicoCompostoResponse response = getFactoryAdapter().getListarServicoCompostoPDCAdapter().invokeProcess(request);

		List<ListarServicoCompostoSaidaDTO> list = new ArrayList<ListarServicoCompostoSaidaDTO>();

		for (br.com.bradesco.web.pgit.service.data.pdc.listarservicocomposto.response.Ocorrencias saida : response.getOcorrencias()) {
			list.add(new ListarServicoCompostoSaidaDTO(saida));
		}

		return list;    	
    	
    }

	/*
	 * Incluir
	 */
    /**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.manterservicocomposto.IManterServicoCompostoService#incluirServicoComposto(br.com.bradesco.web.pgit.service.business.manterservicocomposto.bean.IncluirServicoCompostoEntradaDTO)
	 */
	public IncluirServicoCompostoSaidaDTO incluirServicoComposto(IncluirServicoCompostoEntradaDTO entrada){

		IncluirServicoCompostoRequest request = new IncluirServicoCompostoRequest();

		request.setCdTipoServicoCnab(0);
		request.setCdFormaLiquidacao(0);
		request.setCdFormaLancamentoCnab(PgitUtil.verificaIntegerNulo(entrada.getCdFormaLancamentoCnab()));
		request.setCdServicoCompostoPagamento(PgitUtil.verificaLongNulo(entrada.getCdServicoCompostoPagamento()));
		request.setDsServicoCompostoPagamento(PgitUtil.verificaStringNula(entrada.getDsServicoCompostoPagamento()));
		request.setCdIndLctoFuturo(PgitUtil.verificaIntegerNulo(entrada.getCdIndLctoFuturo()));
		request.setQtDiaLctoFuturo(PgitUtil.verificaIntegerNulo(entrada.getQtDiaLctoFuturo()));
		request.setQtOcorrencias(PgitUtil.verificaIntegerNulo(entrada.getQtOcorrencias()));

		IncluirServicoCompostoResponse response = getFactoryAdapter().getIncluirServicoCompostoPDCAdapter().invokeProcess(request);

		if (response == null){
			return null;
		}

		return new IncluirServicoCompostoSaidaDTO(response.getCodMensagem(), response.getMensagem());
    	
    }    

	/*
	 * Detalhar
	 */
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.manterservicocomposto.IManterServicoCompostoService#detalharServicoComposto(br.com.bradesco.web.pgit.service.business.manterservicocomposto.bean.DetalharServicoCompostoEntradaDTO)
	 */
	public DetalharServicoCompostoSaidaDTO detalharServicoComposto(DetalharServicoCompostoEntradaDTO entrada) {
		
		DetalharServicoCompostoRequest request = new DetalharServicoCompostoRequest();

		request.setCdTipoServicoCnab(0);
		request.setCdFormaLiquidacao(0);
		request.setCdFormaLancamentoCnab(PgitUtil.verificaIntegerNulo(entrada.getCdFormaLancamentoCnab()));
		request.setCdServicoCompostoPagamento(PgitUtil.verificaLongNulo(entrada.getCdServicoCompostoPagamento()));
		request.setDsServicoCompostoPagamento(PgitUtil.verificaStringNula(entrada.getDsServicoCompostoPagamento()));
		request.setCdIndLctoFuturo(PgitUtil.verificaIntegerNulo(entrada.getCdIndLctoFuturo()));
		request.setQtDiaLctoFuturo(PgitUtil.verificaIntegerNulo(entrada.getQtDiaLctoFuturo()));
		request.setQtOcorrencias(PgitUtil.verificaIntegerNulo(entrada.getQtOcorrencias()));

		DetalharServicoCompostoResponse response = getFactoryAdapter().getDetalharServicoCompostoPDCAdapter().invokeProcess(request);
		
		if (response == null){
			return null;
		}

		DetalharServicoCompostoSaidaDTO saida = new DetalharServicoCompostoSaidaDTO();
        
		saida.setCdAutenticacaoSegurancaInclusao(response.getCdUsuarioInclusao());
		saida.setCdAutenticacaoSegurancaManutencao(response.getCdUsuarioManutencao());
		saida.setCdCanalInclusao(response.getCdTipoCanalInclusao());
		saida.setCdCanalManutencao(response.getCdTipoCanalManutencao());
		saida.setCdFormaLancamentoCnab(response.getCdFormaLancamentoCnab());
		saida.setCdFormaLiquidacao(response.getCdFormaLiquidacao());
		saida.setCdServicoCompostoPagamento(response.getCdServicoCompostoPagamento());
		saida.setCdTipoServicoCnab(response.getCdTipoServicoCnab());
		saida.setDsCanalInclusao(response.getDsTipoCanalInclusao());
		saida.setDsCanalManutencao(response.getDsTipoCanalManutencao());
		saida.setDsFormaLancamentoCnab(response.getDsFormaLancamentoCnab());
		saida.setDsFormaLiquidacao(response.getDsFormaLiquidacao());
		saida.setDsServicoCompostoPagamento(response.getDsServicoCompostoPagamento());
		saida.setDsTipoServicoCnab(response.getDsTipoServicoCnab());
		saida.setHrInclusaoRegistro(FormatarData.formatarDataTrilha(response.getHrInclusaoRegistro()));
		saida.setHrManutencaoRegistro(FormatarData.formatarDataTrilha(response.getHrManutencaoRegistro()));
		saida.setNmOperacaoFluxoInclusao(response.getCdOperacaoCanalInclusao());
		saida.setNmOperacaoFluxoManutencao(response.getCdOperacaoCanalManutencao());
		saida.setQtDiaLctoFuturo(response.getQtDiaLctoFuturo());
		saida.setCdIndLctoFuturo(response.getCdIndLctoFuturo().toUpperCase().equals("SIM")?1:2);
		saida.setDescIndLctoFuturo(response.getCdIndLctoFuturo());
		
		return saida;
	}    
    
	/*
	 * Excluir
	 */
    /**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.manterservicocomposto.IManterServicoCompostoService#excluirServicoComposto(br.com.bradesco.web.pgit.service.business.manterservicocomposto.bean.ExcluirServicoCompostoEntradaDTO)
	 */
	public ExcluirServicoCompostoSaidaDTO excluirServicoComposto(ExcluirServicoCompostoEntradaDTO entrada){

		ExcluirServicoCompostoRequest request = new ExcluirServicoCompostoRequest();

		request.setCdTipoServicoCnab(0);
		request.setCdFormaLiquidacao(0);
		request.setCdFormaLancamentoCnab(PgitUtil.verificaIntegerNulo(entrada.getCdFormaLancamentoCnab()));
		request.setCdServicoCompostoPagamento(PgitUtil.verificaLongNulo(entrada.getCdServicoCompostoPagamento()));
		request.setDsServicoCompostoPagamento(PgitUtil.verificaStringNula(entrada.getDsServicoCompostoPagamento()));
		request.setCdIndLctoFuturo(PgitUtil.verificaIntegerNulo(entrada.getCdIndLctoFuturo()));
		request.setQtDiaLctoFuturo(PgitUtil.verificaIntegerNulo(entrada.getQtDiaLctoFuturo()));
		request.setQtOcorrencias(PgitUtil.verificaIntegerNulo(entrada.getQtOcorrencias()));

		ExcluirServicoCompostoResponse response = getFactoryAdapter().getExcluirServicoCompostoPDCAdapter().invokeProcess(request);

		if (response == null){
			return null;
		}

		return new ExcluirServicoCompostoSaidaDTO(response.getCodMensagem(), response.getMensagem());
    	
    }    
    
    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.manterservicocomposto.IManterServicoCompostoService#alterarServicoComposto(br.com.bradesco.web.pgit.service.business.manterservicocomposto.bean.AlterarServicoCompostoEntradaDTO)
     */
    public AlterarServicoCompostoSaidaDTO alterarServicoComposto(AlterarServicoCompostoEntradaDTO entrada){
    	
    	AlterarServicoCompostoRequest request = new AlterarServicoCompostoRequest();
    	
    	request.setCdTipoServicoCnab(0);
		request.setCdFormaLiquidacao(0);
		request.setCdFormaLancamentoCnab(PgitUtil.verificaIntegerNulo(entrada.getCdFormaLancamentoCnab()));
		request.setCdServicoCompostoPagamento(PgitUtil.verificaLongNulo(entrada.getCdServicoCompostoPagamento()));
		request.setDsServicoCompostoPagamento(PgitUtil.verificaStringNula(entrada.getDsServicoCompostoPagamento()));
		request.setCdIndLctoFuturo(PgitUtil.verificaIntegerNulo(entrada.getCdIndLctoFuturo()));
		request.setQtDiaLctoFuturo(PgitUtil.verificaIntegerNulo(entrada.getQtDiaLctoFuturo()));
		request.setQtOcorrencias(PgitUtil.verificaIntegerNulo(entrada.getQtOcorrencias()));
    	
    	AlterarServicoCompostoResponse response = getFactoryAdapter().getAlterarServicoCompostoPDCAdapter().invokeProcess(request);
    	
    	if (response == null){
    		return null;
    	}
    	
    	return new AlterarServicoCompostoSaidaDTO(response.getCodMensagem(), response.getMensagem());
    	
    }    
    

	/**
	 * Get: factoryAdapter.
	 *
	 * @return factoryAdapter
	 */
	public FactoryAdapter getFactoryAdapter() {
		return factoryAdapter;
	}

	/**
	 * Set: factoryAdapter.
	 *
	 * @param factoryAdapter the factory adapter
	 */
	public void setFactoryAdapter(FactoryAdapter factoryAdapter) {
		this.factoryAdapter = factoryAdapter;
	}    
}

