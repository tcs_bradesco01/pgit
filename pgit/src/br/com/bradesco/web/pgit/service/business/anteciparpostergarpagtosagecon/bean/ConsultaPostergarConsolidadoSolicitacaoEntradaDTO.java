package br.com.bradesco.web.pgit.service.business.anteciparpostergarpagtosagecon.bean;


// TODO: Auto-generated Javadoc
/**
 * Arquivo criado em 23/05/17.
 *
 * @author : todo!
 * @version :
 */
public class ConsultaPostergarConsolidadoSolicitacaoEntradaDTO {

    /** The max ocorrencias. */
    private Integer maxOcorrencias = 70;

    /** The cdpessoa juridica contrato. */
    private Long cdpessoaJuridicaContrato;

    /** The cd tipo contrato negocio. */
    private Integer cdTipoContratoNegocio;

    /** The nr sequencia contrato negocio. */
    private Long nrSequenciaContratoNegocio;

    /** The dt pgto inicial. */
    private String dtPgtoInicial;

    /** The dt pgto final. */
    private String dtPgtoFinal;

    /** The cd situacao solicitacao pagamento. */
    private Integer cdSituacaoSolicitacaoPagamento;

    /** The cd motivo solicitacao. */
    private Integer cdMotivoSolicitacao;

    /**
     * Set max ocorrencias.
     *
     * @param maxOcorrencias the max ocorrencias
     */
    public void setMaxOcorrencias(Integer maxOcorrencias) {
        this.maxOcorrencias = maxOcorrencias;
    }

    /**
     * Get max ocorrencias.
     *
     * @return the max ocorrencias
     */
    public Integer getMaxOcorrencias() {
        return this.maxOcorrencias;
    }

    /**
     * Set cdpessoa juridica contrato.
     *
     * @param cdpessoaJuridicaContrato the cdpessoa juridica contrato
     */
    public void setCdpessoaJuridicaContrato(Long cdpessoaJuridicaContrato) {
        this.cdpessoaJuridicaContrato = cdpessoaJuridicaContrato;
    }

    /**
     * Get cdpessoa juridica contrato.
     *
     * @return the cdpessoa juridica contrato
     */
    public Long getCdpessoaJuridicaContrato() {
        return this.cdpessoaJuridicaContrato;
    }

    /**
     * Set cd tipo contrato negocio.
     *
     * @param cdTipoContratoNegocio the cd tipo contrato negocio
     */
    public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
        this.cdTipoContratoNegocio = cdTipoContratoNegocio;
    }

    /**
     * Get cd tipo contrato negocio.
     *
     * @return the cd tipo contrato negocio
     */
    public Integer getCdTipoContratoNegocio() {
        return this.cdTipoContratoNegocio;
    }

    /**
     * Set nr sequencia contrato negocio.
     *
     * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
     */
    public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
        this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
    }

    /**
     * Get nr sequencia contrato negocio.
     *
     * @return the nr sequencia contrato negocio
     */
    public Long getNrSequenciaContratoNegocio() {
        return this.nrSequenciaContratoNegocio;
    }

    /**
     * Set dt pgto inicial.
     *
     * @param dtPgtoInicial the dt pgto inicial
     */
    public void setDtPgtoInicial(String dtPgtoInicial) {
        this.dtPgtoInicial = dtPgtoInicial;
    }

    /**
     * Get dt pgto inicial.
     *
     * @return the dt pgto inicial
     */
    public String getDtPgtoInicial() {
        return this.dtPgtoInicial;
    }

    /**
     * Set dt pgto final.
     *
     * @param dtPgtoFinal the dt pgto final
     */
    public void setDtPgtoFinal(String dtPgtoFinal) {
        this.dtPgtoFinal = dtPgtoFinal;
    }

    /**
     * Get dt pgto final.
     *
     * @return the dt pgto final
     */
    public String getDtPgtoFinal() {
        return this.dtPgtoFinal;
    }

    /**
     * Set cd situacao solicitacao pagamento.
     *
     * @param cdSituacaoSolicitacaoPagamento the cd situacao solicitacao pagamento
     */
    public void setCdSituacaoSolicitacaoPagamento(Integer cdSituacaoSolicitacaoPagamento) {
        this.cdSituacaoSolicitacaoPagamento = cdSituacaoSolicitacaoPagamento;
    }

    /**
     * Get cd situacao solicitacao pagamento.
     *
     * @return the cd situacao solicitacao pagamento
     */
    public Integer getCdSituacaoSolicitacaoPagamento() {
        return this.cdSituacaoSolicitacaoPagamento;
    }

    /**
     * Set cd motivo solicitacao.
     *
     * @param cdMotivoSolicitacao the cd motivo solicitacao
     */
    public void setCdMotivoSolicitacao(Integer cdMotivoSolicitacao) {
        this.cdMotivoSolicitacao = cdMotivoSolicitacao;
    }

    /**
     * Get cd motivo solicitacao.
     *
     * @return the cd motivo solicitacao
     */
    public Integer getCdMotivoSolicitacao() {
        return this.cdMotivoSolicitacao;
    }
}