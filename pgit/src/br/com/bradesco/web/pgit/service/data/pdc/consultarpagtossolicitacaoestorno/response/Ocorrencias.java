/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultarpagtossolicitacaoestorno.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.math.BigDecimal;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdCorpoCpfCnpj
     */
    private long _cdCorpoCpfCnpj = 0;

    /**
     * keeps track of state for field: _cdCorpoCpfCnpj
     */
    private boolean _has_cdCorpoCpfCnpj;

    /**
     * Field _cdFilialCpfCnpj
     */
    private int _cdFilialCpfCnpj = 0;

    /**
     * keeps track of state for field: _cdFilialCpfCnpj
     */
    private boolean _has_cdFilialCpfCnpj;

    /**
     * Field _cdDigitoCpfCnpj
     */
    private int _cdDigitoCpfCnpj = 0;

    /**
     * keeps track of state for field: _cdDigitoCpfCnpj
     */
    private boolean _has_cdDigitoCpfCnpj;

    /**
     * Field _cdPessoaJuridicaContrato
     */
    private long _cdPessoaJuridicaContrato = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridicaContrato
     */
    private boolean _has_cdPessoaJuridicaContrato;

    /**
     * Field _dsRazaoSocial
     */
    private java.lang.String _dsRazaoSocial;

    /**
     * Field _cdTipoContratoNegocio
     */
    private int _cdTipoContratoNegocio = 0;

    /**
     * keeps track of state for field: _cdTipoContratoNegocio
     */
    private boolean _has_cdTipoContratoNegocio;

    /**
     * Field _nrSequenciaContratoNegocio
     */
    private long _nrSequenciaContratoNegocio = 0;

    /**
     * keeps track of state for field: _nrSequenciaContratoNegocio
     */
    private boolean _has_nrSequenciaContratoNegocio;

    /**
     * Field _cdprodutoServicoOperacao
     */
    private int _cdprodutoServicoOperacao = 0;

    /**
     * keeps track of state for field: _cdprodutoServicoOperacao
     */
    private boolean _has_cdprodutoServicoOperacao;

    /**
     * Field _dsOperacaoProdutoServico
     */
    private java.lang.String _dsOperacaoProdutoServico;

    /**
     * Field _cdProdutoServicoRelacionado
     */
    private int _cdProdutoServicoRelacionado = 0;

    /**
     * keeps track of state for field: _cdProdutoServicoRelacionado
     */
    private boolean _has_cdProdutoServicoRelacionado;

    /**
     * Field _dsOperacaoProdutoRelacionado
     */
    private java.lang.String _dsOperacaoProdutoRelacionado;

    /**
     * Field _cdControlePagamento
     */
    private java.lang.String _cdControlePagamento;

    /**
     * Field _dtCredito
     */
    private java.lang.String _dtCredito;

    /**
     * Field _vlrEfetivacaoPagamento
     */
    private java.math.BigDecimal _vlrEfetivacaoPagamento = new java.math.BigDecimal("0");

    /**
     * Field _cdInscricaoFavorecido
     */
    private long _cdInscricaoFavorecido = 0;

    /**
     * keeps track of state for field: _cdInscricaoFavorecido
     */
    private boolean _has_cdInscricaoFavorecido;

    /**
     * Field _dsFavorecidoCliente
     */
    private java.lang.String _dsFavorecidoCliente;

    /**
     * Field _cdBancoDebito
     */
    private int _cdBancoDebito = 0;

    /**
     * keeps track of state for field: _cdBancoDebito
     */
    private boolean _has_cdBancoDebito;

    /**
     * Field _cdAgenciaDebito
     */
    private int _cdAgenciaDebito = 0;

    /**
     * keeps track of state for field: _cdAgenciaDebito
     */
    private boolean _has_cdAgenciaDebito;

    /**
     * Field _cdDigitoAgenciaDebito
     */
    private int _cdDigitoAgenciaDebito = 0;

    /**
     * keeps track of state for field: _cdDigitoAgenciaDebito
     */
    private boolean _has_cdDigitoAgenciaDebito;

    /**
     * Field _cdContaDebito
     */
    private long _cdContaDebito = 0;

    /**
     * keeps track of state for field: _cdContaDebito
     */
    private boolean _has_cdContaDebito;

    /**
     * Field _cdDigitoContaDebito
     */
    private java.lang.String _cdDigitoContaDebito;

    /**
     * Field _bancoCredito
     */
    private int _bancoCredito = 0;

    /**
     * keeps track of state for field: _bancoCredito
     */
    private boolean _has_bancoCredito;

    /**
     * Field _agenciaCredito
     */
    private int _agenciaCredito = 0;

    /**
     * keeps track of state for field: _agenciaCredito
     */
    private boolean _has_agenciaCredito;

    /**
     * Field _digitoAgenciaCredito
     */
    private int _digitoAgenciaCredito = 0;

    /**
     * keeps track of state for field: _digitoAgenciaCredito
     */
    private boolean _has_digitoAgenciaCredito;

    /**
     * Field _contaCredito
     */
    private long _contaCredito = 0;

    /**
     * keeps track of state for field: _contaCredito
     */
    private boolean _has_contaCredito;

    /**
     * Field _digitoContaCredito
     */
    private java.lang.String _digitoContaCredito;

    /**
     * Field _cdSituacaoOperacaoPagamento
     */
    private int _cdSituacaoOperacaoPagamento = 0;

    /**
     * keeps track of state for field: _cdSituacaoOperacaoPagamento
     */
    private boolean _has_cdSituacaoOperacaoPagamento;

    /**
     * Field _dsSituacaoOperacaoPagamento
     */
    private java.lang.String _dsSituacaoOperacaoPagamento;

    /**
     * Field _dtEfetivacao
     */
    private java.lang.String _dtEfetivacao;

    /**
     * Field _cpfCnpjRecebedor
     */
    private long _cpfCnpjRecebedor = 0;

    /**
     * keeps track of state for field: _cpfCnpjRecebedor
     */
    private boolean _has_cpfCnpjRecebedor;

    /**
     * Field _filialCpfCnpjRecebedor
     */
    private int _filialCpfCnpjRecebedor = 0;

    /**
     * keeps track of state for field: _filialCpfCnpjRecebedor
     */
    private boolean _has_filialCpfCnpjRecebedor;

    /**
     * Field _digitoCpfCnpjRecebedor
     */
    private int _digitoCpfCnpjRecebedor = 0;

    /**
     * keeps track of state for field: _digitoCpfCnpjRecebedor
     */
    private boolean _has_digitoCpfCnpjRecebedor;

    /**
     * Field _cdTipoTela
     */
    private int _cdTipoTela = 0;

    /**
     * keeps track of state for field: _cdTipoTela
     */
    private boolean _has_cdTipoTela;

    /**
     * Field _cdAgendadosPagosNaoPagos
     */
    private int _cdAgendadosPagosNaoPagos = 0;

    /**
     * keeps track of state for field: _cdAgendadosPagosNaoPagos
     */
    private boolean _has_cdAgendadosPagosNaoPagos;

    /**
     * Field _dsMotivoRecusaEstorno
     */
    private java.lang.String _dsMotivoRecusaEstorno;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
        setVlrEfetivacaoPagamento(new java.math.BigDecimal("0"));
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarpagtossolicitacaoestorno.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteAgenciaCredito
     * 
     */
    public void deleteAgenciaCredito()
    {
        this._has_agenciaCredito= false;
    } //-- void deleteAgenciaCredito() 

    /**
     * Method deleteBancoCredito
     * 
     */
    public void deleteBancoCredito()
    {
        this._has_bancoCredito= false;
    } //-- void deleteBancoCredito() 

    /**
     * Method deleteCdAgenciaDebito
     * 
     */
    public void deleteCdAgenciaDebito()
    {
        this._has_cdAgenciaDebito= false;
    } //-- void deleteCdAgenciaDebito() 

    /**
     * Method deleteCdAgendadosPagosNaoPagos
     * 
     */
    public void deleteCdAgendadosPagosNaoPagos()
    {
        this._has_cdAgendadosPagosNaoPagos= false;
    } //-- void deleteCdAgendadosPagosNaoPagos() 

    /**
     * Method deleteCdBancoDebito
     * 
     */
    public void deleteCdBancoDebito()
    {
        this._has_cdBancoDebito= false;
    } //-- void deleteCdBancoDebito() 

    /**
     * Method deleteCdContaDebito
     * 
     */
    public void deleteCdContaDebito()
    {
        this._has_cdContaDebito= false;
    } //-- void deleteCdContaDebito() 

    /**
     * Method deleteCdCorpoCpfCnpj
     * 
     */
    public void deleteCdCorpoCpfCnpj()
    {
        this._has_cdCorpoCpfCnpj= false;
    } //-- void deleteCdCorpoCpfCnpj() 

    /**
     * Method deleteCdDigitoAgenciaDebito
     * 
     */
    public void deleteCdDigitoAgenciaDebito()
    {
        this._has_cdDigitoAgenciaDebito= false;
    } //-- void deleteCdDigitoAgenciaDebito() 

    /**
     * Method deleteCdDigitoCpfCnpj
     * 
     */
    public void deleteCdDigitoCpfCnpj()
    {
        this._has_cdDigitoCpfCnpj= false;
    } //-- void deleteCdDigitoCpfCnpj() 

    /**
     * Method deleteCdFilialCpfCnpj
     * 
     */
    public void deleteCdFilialCpfCnpj()
    {
        this._has_cdFilialCpfCnpj= false;
    } //-- void deleteCdFilialCpfCnpj() 

    /**
     * Method deleteCdInscricaoFavorecido
     * 
     */
    public void deleteCdInscricaoFavorecido()
    {
        this._has_cdInscricaoFavorecido= false;
    } //-- void deleteCdInscricaoFavorecido() 

    /**
     * Method deleteCdPessoaJuridicaContrato
     * 
     */
    public void deleteCdPessoaJuridicaContrato()
    {
        this._has_cdPessoaJuridicaContrato= false;
    } //-- void deleteCdPessoaJuridicaContrato() 

    /**
     * Method deleteCdProdutoServicoRelacionado
     * 
     */
    public void deleteCdProdutoServicoRelacionado()
    {
        this._has_cdProdutoServicoRelacionado= false;
    } //-- void deleteCdProdutoServicoRelacionado() 

    /**
     * Method deleteCdSituacaoOperacaoPagamento
     * 
     */
    public void deleteCdSituacaoOperacaoPagamento()
    {
        this._has_cdSituacaoOperacaoPagamento= false;
    } //-- void deleteCdSituacaoOperacaoPagamento() 

    /**
     * Method deleteCdTipoContratoNegocio
     * 
     */
    public void deleteCdTipoContratoNegocio()
    {
        this._has_cdTipoContratoNegocio= false;
    } //-- void deleteCdTipoContratoNegocio() 

    /**
     * Method deleteCdTipoTela
     * 
     */
    public void deleteCdTipoTela()
    {
        this._has_cdTipoTela= false;
    } //-- void deleteCdTipoTela() 

    /**
     * Method deleteCdprodutoServicoOperacao
     * 
     */
    public void deleteCdprodutoServicoOperacao()
    {
        this._has_cdprodutoServicoOperacao= false;
    } //-- void deleteCdprodutoServicoOperacao() 

    /**
     * Method deleteContaCredito
     * 
     */
    public void deleteContaCredito()
    {
        this._has_contaCredito= false;
    } //-- void deleteContaCredito() 

    /**
     * Method deleteCpfCnpjRecebedor
     * 
     */
    public void deleteCpfCnpjRecebedor()
    {
        this._has_cpfCnpjRecebedor= false;
    } //-- void deleteCpfCnpjRecebedor() 

    /**
     * Method deleteDigitoAgenciaCredito
     * 
     */
    public void deleteDigitoAgenciaCredito()
    {
        this._has_digitoAgenciaCredito= false;
    } //-- void deleteDigitoAgenciaCredito() 

    /**
     * Method deleteDigitoCpfCnpjRecebedor
     * 
     */
    public void deleteDigitoCpfCnpjRecebedor()
    {
        this._has_digitoCpfCnpjRecebedor= false;
    } //-- void deleteDigitoCpfCnpjRecebedor() 

    /**
     * Method deleteFilialCpfCnpjRecebedor
     * 
     */
    public void deleteFilialCpfCnpjRecebedor()
    {
        this._has_filialCpfCnpjRecebedor= false;
    } //-- void deleteFilialCpfCnpjRecebedor() 

    /**
     * Method deleteNrSequenciaContratoNegocio
     * 
     */
    public void deleteNrSequenciaContratoNegocio()
    {
        this._has_nrSequenciaContratoNegocio= false;
    } //-- void deleteNrSequenciaContratoNegocio() 

    /**
     * Returns the value of field 'agenciaCredito'.
     * 
     * @return int
     * @return the value of field 'agenciaCredito'.
     */
    public int getAgenciaCredito()
    {
        return this._agenciaCredito;
    } //-- int getAgenciaCredito() 

    /**
     * Returns the value of field 'bancoCredito'.
     * 
     * @return int
     * @return the value of field 'bancoCredito'.
     */
    public int getBancoCredito()
    {
        return this._bancoCredito;
    } //-- int getBancoCredito() 

    /**
     * Returns the value of field 'cdAgenciaDebito'.
     * 
     * @return int
     * @return the value of field 'cdAgenciaDebito'.
     */
    public int getCdAgenciaDebito()
    {
        return this._cdAgenciaDebito;
    } //-- int getCdAgenciaDebito() 

    /**
     * Returns the value of field 'cdAgendadosPagosNaoPagos'.
     * 
     * @return int
     * @return the value of field 'cdAgendadosPagosNaoPagos'.
     */
    public int getCdAgendadosPagosNaoPagos()
    {
        return this._cdAgendadosPagosNaoPagos;
    } //-- int getCdAgendadosPagosNaoPagos() 

    /**
     * Returns the value of field 'cdBancoDebito'.
     * 
     * @return int
     * @return the value of field 'cdBancoDebito'.
     */
    public int getCdBancoDebito()
    {
        return this._cdBancoDebito;
    } //-- int getCdBancoDebito() 

    /**
     * Returns the value of field 'cdContaDebito'.
     * 
     * @return long
     * @return the value of field 'cdContaDebito'.
     */
    public long getCdContaDebito()
    {
        return this._cdContaDebito;
    } //-- long getCdContaDebito() 

    /**
     * Returns the value of field 'cdControlePagamento'.
     * 
     * @return String
     * @return the value of field 'cdControlePagamento'.
     */
    public java.lang.String getCdControlePagamento()
    {
        return this._cdControlePagamento;
    } //-- java.lang.String getCdControlePagamento() 

    /**
     * Returns the value of field 'cdCorpoCpfCnpj'.
     * 
     * @return long
     * @return the value of field 'cdCorpoCpfCnpj'.
     */
    public long getCdCorpoCpfCnpj()
    {
        return this._cdCorpoCpfCnpj;
    } //-- long getCdCorpoCpfCnpj() 

    /**
     * Returns the value of field 'cdDigitoAgenciaDebito'.
     * 
     * @return int
     * @return the value of field 'cdDigitoAgenciaDebito'.
     */
    public int getCdDigitoAgenciaDebito()
    {
        return this._cdDigitoAgenciaDebito;
    } //-- int getCdDigitoAgenciaDebito() 

    /**
     * Returns the value of field 'cdDigitoContaDebito'.
     * 
     * @return String
     * @return the value of field 'cdDigitoContaDebito'.
     */
    public java.lang.String getCdDigitoContaDebito()
    {
        return this._cdDigitoContaDebito;
    } //-- java.lang.String getCdDigitoContaDebito() 

    /**
     * Returns the value of field 'cdDigitoCpfCnpj'.
     * 
     * @return int
     * @return the value of field 'cdDigitoCpfCnpj'.
     */
    public int getCdDigitoCpfCnpj()
    {
        return this._cdDigitoCpfCnpj;
    } //-- int getCdDigitoCpfCnpj() 

    /**
     * Returns the value of field 'cdFilialCpfCnpj'.
     * 
     * @return int
     * @return the value of field 'cdFilialCpfCnpj'.
     */
    public int getCdFilialCpfCnpj()
    {
        return this._cdFilialCpfCnpj;
    } //-- int getCdFilialCpfCnpj() 

    /**
     * Returns the value of field 'cdInscricaoFavorecido'.
     * 
     * @return long
     * @return the value of field 'cdInscricaoFavorecido'.
     */
    public long getCdInscricaoFavorecido()
    {
        return this._cdInscricaoFavorecido;
    } //-- long getCdInscricaoFavorecido() 

    /**
     * Returns the value of field 'cdPessoaJuridicaContrato'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridicaContrato'.
     */
    public long getCdPessoaJuridicaContrato()
    {
        return this._cdPessoaJuridicaContrato;
    } //-- long getCdPessoaJuridicaContrato() 

    /**
     * Returns the value of field 'cdProdutoServicoRelacionado'.
     * 
     * @return int
     * @return the value of field 'cdProdutoServicoRelacionado'.
     */
    public int getCdProdutoServicoRelacionado()
    {
        return this._cdProdutoServicoRelacionado;
    } //-- int getCdProdutoServicoRelacionado() 

    /**
     * Returns the value of field 'cdSituacaoOperacaoPagamento'.
     * 
     * @return int
     * @return the value of field 'cdSituacaoOperacaoPagamento'.
     */
    public int getCdSituacaoOperacaoPagamento()
    {
        return this._cdSituacaoOperacaoPagamento;
    } //-- int getCdSituacaoOperacaoPagamento() 

    /**
     * Returns the value of field 'cdTipoContratoNegocio'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoNegocio'.
     */
    public int getCdTipoContratoNegocio()
    {
        return this._cdTipoContratoNegocio;
    } //-- int getCdTipoContratoNegocio() 

    /**
     * Returns the value of field 'cdTipoTela'.
     * 
     * @return int
     * @return the value of field 'cdTipoTela'.
     */
    public int getCdTipoTela()
    {
        return this._cdTipoTela;
    } //-- int getCdTipoTela() 

    /**
     * Returns the value of field 'cdprodutoServicoOperacao'.
     * 
     * @return int
     * @return the value of field 'cdprodutoServicoOperacao'.
     */
    public int getCdprodutoServicoOperacao()
    {
        return this._cdprodutoServicoOperacao;
    } //-- int getCdprodutoServicoOperacao() 

    /**
     * Returns the value of field 'contaCredito'.
     * 
     * @return long
     * @return the value of field 'contaCredito'.
     */
    public long getContaCredito()
    {
        return this._contaCredito;
    } //-- long getContaCredito() 

    /**
     * Returns the value of field 'cpfCnpjRecebedor'.
     * 
     * @return long
     * @return the value of field 'cpfCnpjRecebedor'.
     */
    public long getCpfCnpjRecebedor()
    {
        return this._cpfCnpjRecebedor;
    } //-- long getCpfCnpjRecebedor() 

    /**
     * Returns the value of field 'digitoAgenciaCredito'.
     * 
     * @return int
     * @return the value of field 'digitoAgenciaCredito'.
     */
    public int getDigitoAgenciaCredito()
    {
        return this._digitoAgenciaCredito;
    } //-- int getDigitoAgenciaCredito() 

    /**
     * Returns the value of field 'digitoContaCredito'.
     * 
     * @return String
     * @return the value of field 'digitoContaCredito'.
     */
    public java.lang.String getDigitoContaCredito()
    {
        return this._digitoContaCredito;
    } //-- java.lang.String getDigitoContaCredito() 

    /**
     * Returns the value of field 'digitoCpfCnpjRecebedor'.
     * 
     * @return int
     * @return the value of field 'digitoCpfCnpjRecebedor'.
     */
    public int getDigitoCpfCnpjRecebedor()
    {
        return this._digitoCpfCnpjRecebedor;
    } //-- int getDigitoCpfCnpjRecebedor() 

    /**
     * Returns the value of field 'dsFavorecidoCliente'.
     * 
     * @return String
     * @return the value of field 'dsFavorecidoCliente'.
     */
    public java.lang.String getDsFavorecidoCliente()
    {
        return this._dsFavorecidoCliente;
    } //-- java.lang.String getDsFavorecidoCliente() 

    /**
     * Returns the value of field 'dsMotivoRecusaEstorno'.
     * 
     * @return String
     * @return the value of field 'dsMotivoRecusaEstorno'.
     */
    public java.lang.String getDsMotivoRecusaEstorno()
    {
        return this._dsMotivoRecusaEstorno;
    } //-- java.lang.String getDsMotivoRecusaEstorno() 

    /**
     * Returns the value of field 'dsOperacaoProdutoRelacionado'.
     * 
     * @return String
     * @return the value of field 'dsOperacaoProdutoRelacionado'.
     */
    public java.lang.String getDsOperacaoProdutoRelacionado()
    {
        return this._dsOperacaoProdutoRelacionado;
    } //-- java.lang.String getDsOperacaoProdutoRelacionado() 

    /**
     * Returns the value of field 'dsOperacaoProdutoServico'.
     * 
     * @return String
     * @return the value of field 'dsOperacaoProdutoServico'.
     */
    public java.lang.String getDsOperacaoProdutoServico()
    {
        return this._dsOperacaoProdutoServico;
    } //-- java.lang.String getDsOperacaoProdutoServico() 

    /**
     * Returns the value of field 'dsRazaoSocial'.
     * 
     * @return String
     * @return the value of field 'dsRazaoSocial'.
     */
    public java.lang.String getDsRazaoSocial()
    {
        return this._dsRazaoSocial;
    } //-- java.lang.String getDsRazaoSocial() 

    /**
     * Returns the value of field 'dsSituacaoOperacaoPagamento'.
     * 
     * @return String
     * @return the value of field 'dsSituacaoOperacaoPagamento'.
     */
    public java.lang.String getDsSituacaoOperacaoPagamento()
    {
        return this._dsSituacaoOperacaoPagamento;
    } //-- java.lang.String getDsSituacaoOperacaoPagamento() 

    /**
     * Returns the value of field 'dtCredito'.
     * 
     * @return String
     * @return the value of field 'dtCredito'.
     */
    public java.lang.String getDtCredito()
    {
        return this._dtCredito;
    } //-- java.lang.String getDtCredito() 

    /**
     * Returns the value of field 'dtEfetivacao'.
     * 
     * @return String
     * @return the value of field 'dtEfetivacao'.
     */
    public java.lang.String getDtEfetivacao()
    {
        return this._dtEfetivacao;
    } //-- java.lang.String getDtEfetivacao() 

    /**
     * Returns the value of field 'filialCpfCnpjRecebedor'.
     * 
     * @return int
     * @return the value of field 'filialCpfCnpjRecebedor'.
     */
    public int getFilialCpfCnpjRecebedor()
    {
        return this._filialCpfCnpjRecebedor;
    } //-- int getFilialCpfCnpjRecebedor() 

    /**
     * Returns the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @return long
     * @return the value of field 'nrSequenciaContratoNegocio'.
     */
    public long getNrSequenciaContratoNegocio()
    {
        return this._nrSequenciaContratoNegocio;
    } //-- long getNrSequenciaContratoNegocio() 

    /**
     * Returns the value of field 'vlrEfetivacaoPagamento'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlrEfetivacaoPagamento'.
     */
    public java.math.BigDecimal getVlrEfetivacaoPagamento()
    {
        return this._vlrEfetivacaoPagamento;
    } //-- java.math.BigDecimal getVlrEfetivacaoPagamento() 

    /**
     * Method hasAgenciaCredito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasAgenciaCredito()
    {
        return this._has_agenciaCredito;
    } //-- boolean hasAgenciaCredito() 

    /**
     * Method hasBancoCredito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasBancoCredito()
    {
        return this._has_bancoCredito;
    } //-- boolean hasBancoCredito() 

    /**
     * Method hasCdAgenciaDebito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgenciaDebito()
    {
        return this._has_cdAgenciaDebito;
    } //-- boolean hasCdAgenciaDebito() 

    /**
     * Method hasCdAgendadosPagosNaoPagos
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgendadosPagosNaoPagos()
    {
        return this._has_cdAgendadosPagosNaoPagos;
    } //-- boolean hasCdAgendadosPagosNaoPagos() 

    /**
     * Method hasCdBancoDebito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdBancoDebito()
    {
        return this._has_cdBancoDebito;
    } //-- boolean hasCdBancoDebito() 

    /**
     * Method hasCdContaDebito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdContaDebito()
    {
        return this._has_cdContaDebito;
    } //-- boolean hasCdContaDebito() 

    /**
     * Method hasCdCorpoCpfCnpj
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCorpoCpfCnpj()
    {
        return this._has_cdCorpoCpfCnpj;
    } //-- boolean hasCdCorpoCpfCnpj() 

    /**
     * Method hasCdDigitoAgenciaDebito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdDigitoAgenciaDebito()
    {
        return this._has_cdDigitoAgenciaDebito;
    } //-- boolean hasCdDigitoAgenciaDebito() 

    /**
     * Method hasCdDigitoCpfCnpj
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdDigitoCpfCnpj()
    {
        return this._has_cdDigitoCpfCnpj;
    } //-- boolean hasCdDigitoCpfCnpj() 

    /**
     * Method hasCdFilialCpfCnpj
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFilialCpfCnpj()
    {
        return this._has_cdFilialCpfCnpj;
    } //-- boolean hasCdFilialCpfCnpj() 

    /**
     * Method hasCdInscricaoFavorecido
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdInscricaoFavorecido()
    {
        return this._has_cdInscricaoFavorecido;
    } //-- boolean hasCdInscricaoFavorecido() 

    /**
     * Method hasCdPessoaJuridicaContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridicaContrato()
    {
        return this._has_cdPessoaJuridicaContrato;
    } //-- boolean hasCdPessoaJuridicaContrato() 

    /**
     * Method hasCdProdutoServicoRelacionado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdutoServicoRelacionado()
    {
        return this._has_cdProdutoServicoRelacionado;
    } //-- boolean hasCdProdutoServicoRelacionado() 

    /**
     * Method hasCdSituacaoOperacaoPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSituacaoOperacaoPagamento()
    {
        return this._has_cdSituacaoOperacaoPagamento;
    } //-- boolean hasCdSituacaoOperacaoPagamento() 

    /**
     * Method hasCdTipoContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoNegocio()
    {
        return this._has_cdTipoContratoNegocio;
    } //-- boolean hasCdTipoContratoNegocio() 

    /**
     * Method hasCdTipoTela
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoTela()
    {
        return this._has_cdTipoTela;
    } //-- boolean hasCdTipoTela() 

    /**
     * Method hasCdprodutoServicoOperacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdprodutoServicoOperacao()
    {
        return this._has_cdprodutoServicoOperacao;
    } //-- boolean hasCdprodutoServicoOperacao() 

    /**
     * Method hasContaCredito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasContaCredito()
    {
        return this._has_contaCredito;
    } //-- boolean hasContaCredito() 

    /**
     * Method hasCpfCnpjRecebedor
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCpfCnpjRecebedor()
    {
        return this._has_cpfCnpjRecebedor;
    } //-- boolean hasCpfCnpjRecebedor() 

    /**
     * Method hasDigitoAgenciaCredito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasDigitoAgenciaCredito()
    {
        return this._has_digitoAgenciaCredito;
    } //-- boolean hasDigitoAgenciaCredito() 

    /**
     * Method hasDigitoCpfCnpjRecebedor
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasDigitoCpfCnpjRecebedor()
    {
        return this._has_digitoCpfCnpjRecebedor;
    } //-- boolean hasDigitoCpfCnpjRecebedor() 

    /**
     * Method hasFilialCpfCnpjRecebedor
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasFilialCpfCnpjRecebedor()
    {
        return this._has_filialCpfCnpjRecebedor;
    } //-- boolean hasFilialCpfCnpjRecebedor() 

    /**
     * Method hasNrSequenciaContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaContratoNegocio()
    {
        return this._has_nrSequenciaContratoNegocio;
    } //-- boolean hasNrSequenciaContratoNegocio() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'agenciaCredito'.
     * 
     * @param agenciaCredito the value of field 'agenciaCredito'.
     */
    public void setAgenciaCredito(int agenciaCredito)
    {
        this._agenciaCredito = agenciaCredito;
        this._has_agenciaCredito = true;
    } //-- void setAgenciaCredito(int) 

    /**
     * Sets the value of field 'bancoCredito'.
     * 
     * @param bancoCredito the value of field 'bancoCredito'.
     */
    public void setBancoCredito(int bancoCredito)
    {
        this._bancoCredito = bancoCredito;
        this._has_bancoCredito = true;
    } //-- void setBancoCredito(int) 

    /**
     * Sets the value of field 'cdAgenciaDebito'.
     * 
     * @param cdAgenciaDebito the value of field 'cdAgenciaDebito'.
     */
    public void setCdAgenciaDebito(int cdAgenciaDebito)
    {
        this._cdAgenciaDebito = cdAgenciaDebito;
        this._has_cdAgenciaDebito = true;
    } //-- void setCdAgenciaDebito(int) 

    /**
     * Sets the value of field 'cdAgendadosPagosNaoPagos'.
     * 
     * @param cdAgendadosPagosNaoPagos the value of field
     * 'cdAgendadosPagosNaoPagos'.
     */
    public void setCdAgendadosPagosNaoPagos(int cdAgendadosPagosNaoPagos)
    {
        this._cdAgendadosPagosNaoPagos = cdAgendadosPagosNaoPagos;
        this._has_cdAgendadosPagosNaoPagos = true;
    } //-- void setCdAgendadosPagosNaoPagos(int) 

    /**
     * Sets the value of field 'cdBancoDebito'.
     * 
     * @param cdBancoDebito the value of field 'cdBancoDebito'.
     */
    public void setCdBancoDebito(int cdBancoDebito)
    {
        this._cdBancoDebito = cdBancoDebito;
        this._has_cdBancoDebito = true;
    } //-- void setCdBancoDebito(int) 

    /**
     * Sets the value of field 'cdContaDebito'.
     * 
     * @param cdContaDebito the value of field 'cdContaDebito'.
     */
    public void setCdContaDebito(long cdContaDebito)
    {
        this._cdContaDebito = cdContaDebito;
        this._has_cdContaDebito = true;
    } //-- void setCdContaDebito(long) 

    /**
     * Sets the value of field 'cdControlePagamento'.
     * 
     * @param cdControlePagamento the value of field
     * 'cdControlePagamento'.
     */
    public void setCdControlePagamento(java.lang.String cdControlePagamento)
    {
        this._cdControlePagamento = cdControlePagamento;
    } //-- void setCdControlePagamento(java.lang.String) 

    /**
     * Sets the value of field 'cdCorpoCpfCnpj'.
     * 
     * @param cdCorpoCpfCnpj the value of field 'cdCorpoCpfCnpj'.
     */
    public void setCdCorpoCpfCnpj(long cdCorpoCpfCnpj)
    {
        this._cdCorpoCpfCnpj = cdCorpoCpfCnpj;
        this._has_cdCorpoCpfCnpj = true;
    } //-- void setCdCorpoCpfCnpj(long) 

    /**
     * Sets the value of field 'cdDigitoAgenciaDebito'.
     * 
     * @param cdDigitoAgenciaDebito the value of field
     * 'cdDigitoAgenciaDebito'.
     */
    public void setCdDigitoAgenciaDebito(int cdDigitoAgenciaDebito)
    {
        this._cdDigitoAgenciaDebito = cdDigitoAgenciaDebito;
        this._has_cdDigitoAgenciaDebito = true;
    } //-- void setCdDigitoAgenciaDebito(int) 

    /**
     * Sets the value of field 'cdDigitoContaDebito'.
     * 
     * @param cdDigitoContaDebito the value of field
     * 'cdDigitoContaDebito'.
     */
    public void setCdDigitoContaDebito(java.lang.String cdDigitoContaDebito)
    {
        this._cdDigitoContaDebito = cdDigitoContaDebito;
    } //-- void setCdDigitoContaDebito(java.lang.String) 

    /**
     * Sets the value of field 'cdDigitoCpfCnpj'.
     * 
     * @param cdDigitoCpfCnpj the value of field 'cdDigitoCpfCnpj'.
     */
    public void setCdDigitoCpfCnpj(int cdDigitoCpfCnpj)
    {
        this._cdDigitoCpfCnpj = cdDigitoCpfCnpj;
        this._has_cdDigitoCpfCnpj = true;
    } //-- void setCdDigitoCpfCnpj(int) 

    /**
     * Sets the value of field 'cdFilialCpfCnpj'.
     * 
     * @param cdFilialCpfCnpj the value of field 'cdFilialCpfCnpj'.
     */
    public void setCdFilialCpfCnpj(int cdFilialCpfCnpj)
    {
        this._cdFilialCpfCnpj = cdFilialCpfCnpj;
        this._has_cdFilialCpfCnpj = true;
    } //-- void setCdFilialCpfCnpj(int) 

    /**
     * Sets the value of field 'cdInscricaoFavorecido'.
     * 
     * @param cdInscricaoFavorecido the value of field
     * 'cdInscricaoFavorecido'.
     */
    public void setCdInscricaoFavorecido(long cdInscricaoFavorecido)
    {
        this._cdInscricaoFavorecido = cdInscricaoFavorecido;
        this._has_cdInscricaoFavorecido = true;
    } //-- void setCdInscricaoFavorecido(long) 

    /**
     * Sets the value of field 'cdPessoaJuridicaContrato'.
     * 
     * @param cdPessoaJuridicaContrato the value of field
     * 'cdPessoaJuridicaContrato'.
     */
    public void setCdPessoaJuridicaContrato(long cdPessoaJuridicaContrato)
    {
        this._cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
        this._has_cdPessoaJuridicaContrato = true;
    } //-- void setCdPessoaJuridicaContrato(long) 

    /**
     * Sets the value of field 'cdProdutoServicoRelacionado'.
     * 
     * @param cdProdutoServicoRelacionado the value of field
     * 'cdProdutoServicoRelacionado'.
     */
    public void setCdProdutoServicoRelacionado(int cdProdutoServicoRelacionado)
    {
        this._cdProdutoServicoRelacionado = cdProdutoServicoRelacionado;
        this._has_cdProdutoServicoRelacionado = true;
    } //-- void setCdProdutoServicoRelacionado(int) 

    /**
     * Sets the value of field 'cdSituacaoOperacaoPagamento'.
     * 
     * @param cdSituacaoOperacaoPagamento the value of field
     * 'cdSituacaoOperacaoPagamento'.
     */
    public void setCdSituacaoOperacaoPagamento(int cdSituacaoOperacaoPagamento)
    {
        this._cdSituacaoOperacaoPagamento = cdSituacaoOperacaoPagamento;
        this._has_cdSituacaoOperacaoPagamento = true;
    } //-- void setCdSituacaoOperacaoPagamento(int) 

    /**
     * Sets the value of field 'cdTipoContratoNegocio'.
     * 
     * @param cdTipoContratoNegocio the value of field
     * 'cdTipoContratoNegocio'.
     */
    public void setCdTipoContratoNegocio(int cdTipoContratoNegocio)
    {
        this._cdTipoContratoNegocio = cdTipoContratoNegocio;
        this._has_cdTipoContratoNegocio = true;
    } //-- void setCdTipoContratoNegocio(int) 

    /**
     * Sets the value of field 'cdTipoTela'.
     * 
     * @param cdTipoTela the value of field 'cdTipoTela'.
     */
    public void setCdTipoTela(int cdTipoTela)
    {
        this._cdTipoTela = cdTipoTela;
        this._has_cdTipoTela = true;
    } //-- void setCdTipoTela(int) 

    /**
     * Sets the value of field 'cdprodutoServicoOperacao'.
     * 
     * @param cdprodutoServicoOperacao the value of field
     * 'cdprodutoServicoOperacao'.
     */
    public void setCdprodutoServicoOperacao(int cdprodutoServicoOperacao)
    {
        this._cdprodutoServicoOperacao = cdprodutoServicoOperacao;
        this._has_cdprodutoServicoOperacao = true;
    } //-- void setCdprodutoServicoOperacao(int) 

    /**
     * Sets the value of field 'contaCredito'.
     * 
     * @param contaCredito the value of field 'contaCredito'.
     */
    public void setContaCredito(long contaCredito)
    {
        this._contaCredito = contaCredito;
        this._has_contaCredito = true;
    } //-- void setContaCredito(long) 

    /**
     * Sets the value of field 'cpfCnpjRecebedor'.
     * 
     * @param cpfCnpjRecebedor the value of field 'cpfCnpjRecebedor'
     */
    public void setCpfCnpjRecebedor(long cpfCnpjRecebedor)
    {
        this._cpfCnpjRecebedor = cpfCnpjRecebedor;
        this._has_cpfCnpjRecebedor = true;
    } //-- void setCpfCnpjRecebedor(long) 

    /**
     * Sets the value of field 'digitoAgenciaCredito'.
     * 
     * @param digitoAgenciaCredito the value of field
     * 'digitoAgenciaCredito'.
     */
    public void setDigitoAgenciaCredito(int digitoAgenciaCredito)
    {
        this._digitoAgenciaCredito = digitoAgenciaCredito;
        this._has_digitoAgenciaCredito = true;
    } //-- void setDigitoAgenciaCredito(int) 

    /**
     * Sets the value of field 'digitoContaCredito'.
     * 
     * @param digitoContaCredito the value of field
     * 'digitoContaCredito'.
     */
    public void setDigitoContaCredito(java.lang.String digitoContaCredito)
    {
        this._digitoContaCredito = digitoContaCredito;
    } //-- void setDigitoContaCredito(java.lang.String) 

    /**
     * Sets the value of field 'digitoCpfCnpjRecebedor'.
     * 
     * @param digitoCpfCnpjRecebedor the value of field
     * 'digitoCpfCnpjRecebedor'.
     */
    public void setDigitoCpfCnpjRecebedor(int digitoCpfCnpjRecebedor)
    {
        this._digitoCpfCnpjRecebedor = digitoCpfCnpjRecebedor;
        this._has_digitoCpfCnpjRecebedor = true;
    } //-- void setDigitoCpfCnpjRecebedor(int) 

    /**
     * Sets the value of field 'dsFavorecidoCliente'.
     * 
     * @param dsFavorecidoCliente the value of field
     * 'dsFavorecidoCliente'.
     */
    public void setDsFavorecidoCliente(java.lang.String dsFavorecidoCliente)
    {
        this._dsFavorecidoCliente = dsFavorecidoCliente;
    } //-- void setDsFavorecidoCliente(java.lang.String) 

    /**
     * Sets the value of field 'dsMotivoRecusaEstorno'.
     * 
     * @param dsMotivoRecusaEstorno the value of field
     * 'dsMotivoRecusaEstorno'.
     */
    public void setDsMotivoRecusaEstorno(java.lang.String dsMotivoRecusaEstorno)
    {
        this._dsMotivoRecusaEstorno = dsMotivoRecusaEstorno;
    } //-- void setDsMotivoRecusaEstorno(java.lang.String) 

    /**
     * Sets the value of field 'dsOperacaoProdutoRelacionado'.
     * 
     * @param dsOperacaoProdutoRelacionado the value of field
     * 'dsOperacaoProdutoRelacionado'.
     */
    public void setDsOperacaoProdutoRelacionado(java.lang.String dsOperacaoProdutoRelacionado)
    {
        this._dsOperacaoProdutoRelacionado = dsOperacaoProdutoRelacionado;
    } //-- void setDsOperacaoProdutoRelacionado(java.lang.String) 

    /**
     * Sets the value of field 'dsOperacaoProdutoServico'.
     * 
     * @param dsOperacaoProdutoServico the value of field
     * 'dsOperacaoProdutoServico'.
     */
    public void setDsOperacaoProdutoServico(java.lang.String dsOperacaoProdutoServico)
    {
        this._dsOperacaoProdutoServico = dsOperacaoProdutoServico;
    } //-- void setDsOperacaoProdutoServico(java.lang.String) 

    /**
     * Sets the value of field 'dsRazaoSocial'.
     * 
     * @param dsRazaoSocial the value of field 'dsRazaoSocial'.
     */
    public void setDsRazaoSocial(java.lang.String dsRazaoSocial)
    {
        this._dsRazaoSocial = dsRazaoSocial;
    } //-- void setDsRazaoSocial(java.lang.String) 

    /**
     * Sets the value of field 'dsSituacaoOperacaoPagamento'.
     * 
     * @param dsSituacaoOperacaoPagamento the value of field
     * 'dsSituacaoOperacaoPagamento'.
     */
    public void setDsSituacaoOperacaoPagamento(java.lang.String dsSituacaoOperacaoPagamento)
    {
        this._dsSituacaoOperacaoPagamento = dsSituacaoOperacaoPagamento;
    } //-- void setDsSituacaoOperacaoPagamento(java.lang.String) 

    /**
     * Sets the value of field 'dtCredito'.
     * 
     * @param dtCredito the value of field 'dtCredito'.
     */
    public void setDtCredito(java.lang.String dtCredito)
    {
        this._dtCredito = dtCredito;
    } //-- void setDtCredito(java.lang.String) 

    /**
     * Sets the value of field 'dtEfetivacao'.
     * 
     * @param dtEfetivacao the value of field 'dtEfetivacao'.
     */
    public void setDtEfetivacao(java.lang.String dtEfetivacao)
    {
        this._dtEfetivacao = dtEfetivacao;
    } //-- void setDtEfetivacao(java.lang.String) 

    /**
     * Sets the value of field 'filialCpfCnpjRecebedor'.
     * 
     * @param filialCpfCnpjRecebedor the value of field
     * 'filialCpfCnpjRecebedor'.
     */
    public void setFilialCpfCnpjRecebedor(int filialCpfCnpjRecebedor)
    {
        this._filialCpfCnpjRecebedor = filialCpfCnpjRecebedor;
        this._has_filialCpfCnpjRecebedor = true;
    } //-- void setFilialCpfCnpjRecebedor(int) 

    /**
     * Sets the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @param nrSequenciaContratoNegocio the value of field
     * 'nrSequenciaContratoNegocio'.
     */
    public void setNrSequenciaContratoNegocio(long nrSequenciaContratoNegocio)
    {
        this._nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
        this._has_nrSequenciaContratoNegocio = true;
    } //-- void setNrSequenciaContratoNegocio(long) 

    /**
     * Sets the value of field 'vlrEfetivacaoPagamento'.
     * 
     * @param vlrEfetivacaoPagamento the value of field
     * 'vlrEfetivacaoPagamento'.
     */
    public void setVlrEfetivacaoPagamento(java.math.BigDecimal vlrEfetivacaoPagamento)
    {
        this._vlrEfetivacaoPagamento = vlrEfetivacaoPagamento;
    } //-- void setVlrEfetivacaoPagamento(java.math.BigDecimal) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultarpagtossolicitacaoestorno.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultarpagtossolicitacaoestorno.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultarpagtossolicitacaoestorno.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarpagtossolicitacaoestorno.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
