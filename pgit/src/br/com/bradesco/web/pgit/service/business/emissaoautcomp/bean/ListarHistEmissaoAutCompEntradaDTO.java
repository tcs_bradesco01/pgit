/*
 * Nome: br.com.bradesco.web.pgit.service.business.emissaoautcomp.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.emissaoautcomp.bean;

/**
 * Nome: ListarHistEmissaoAutCompEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarHistEmissaoAutCompEntradaDTO {
	
	/** Atributo contratoPssaoJuridContr. */
	private long contratoPssaoJuridContr;
	
	/** Atributo contratoTpoContrNegoc. */
	private int contratoTpoContrNegoc;
	
	/** Atributo contratoSeqContrNegoc. */
	private long contratoSeqContrNegoc;
	
	/** Atributo servicoEmissaoContratado. */
	private int servicoEmissaoContratado;
	
	/** Atributo tipoServico. */
	private int tipoServico;
	
	/** Atributo modalidadeServico. */
	private int modalidadeServico;
	
	/** Atributo tipoRelacionamento. */
	private int tipoRelacionamento;
	
	/** Atributo periodoInicialManutencao. */
	private String periodoInicialManutencao;
	
	/** Atributo periodoFinalManutencao. */
	private String periodoFinalManutencao;
	
	/**
	 * Get: contratoPssaoJuridContr.
	 *
	 * @return contratoPssaoJuridContr
	 */
	public long getContratoPssaoJuridContr() {
		return contratoPssaoJuridContr;
	}
	
	/**
	 * Set: contratoPssaoJuridContr.
	 *
	 * @param contratoPssaoJuridContr the contrato pssao jurid contr
	 */
	public void setContratoPssaoJuridContr(long contratoPssaoJuridContr) {
		this.contratoPssaoJuridContr = contratoPssaoJuridContr;
	}
	
	/**
	 * Get: contratoSeqContrNegoc.
	 *
	 * @return contratoSeqContrNegoc
	 */
	public long getContratoSeqContrNegoc() {
		return contratoSeqContrNegoc;
	}
	
	/**
	 * Set: contratoSeqContrNegoc.
	 *
	 * @param contratoSeqContrNegoc the contrato seq contr negoc
	 */
	public void setContratoSeqContrNegoc(long contratoSeqContrNegoc) {
		this.contratoSeqContrNegoc = contratoSeqContrNegoc;
	}
	
	/**
	 * Get: contratoTpoContrNegoc.
	 *
	 * @return contratoTpoContrNegoc
	 */
	public int getContratoTpoContrNegoc() {
		return contratoTpoContrNegoc;
	}
	
	/**
	 * Set: contratoTpoContrNegoc.
	 *
	 * @param contratoTpoContrNegoc the contrato tpo contr negoc
	 */
	public void setContratoTpoContrNegoc(int contratoTpoContrNegoc) {
		this.contratoTpoContrNegoc = contratoTpoContrNegoc;
	}
	
	/**
	 * Get: modalidadeServico.
	 *
	 * @return modalidadeServico
	 */
	public int getModalidadeServico() {
		return modalidadeServico;
	}
	
	/**
	 * Set: modalidadeServico.
	 *
	 * @param modalidadeServico the modalidade servico
	 */
	public void setModalidadeServico(int modalidadeServico) {
		this.modalidadeServico = modalidadeServico;
	}
	
	/**
	 * Get: periodoFinalManutencao.
	 *
	 * @return periodoFinalManutencao
	 */
	public String getPeriodoFinalManutencao() {
		return periodoFinalManutencao;
	}
	
	/**
	 * Set: periodoFinalManutencao.
	 *
	 * @param periodoFinalManutencao the periodo final manutencao
	 */
	public void setPeriodoFinalManutencao(String periodoFinalManutencao) {
		this.periodoFinalManutencao = periodoFinalManutencao;
	}
	
	/**
	 * Get: periodoInicialManutencao.
	 *
	 * @return periodoInicialManutencao
	 */
	public String getPeriodoInicialManutencao() {
		return periodoInicialManutencao;
	}
	
	/**
	 * Set: periodoInicialManutencao.
	 *
	 * @param periodoInicialManutencao the periodo inicial manutencao
	 */
	public void setPeriodoInicialManutencao(String periodoInicialManutencao) {
		this.periodoInicialManutencao = periodoInicialManutencao;
	}
	
	/**
	 * Get: servicoEmissaoContratado.
	 *
	 * @return servicoEmissaoContratado
	 */
	public int getServicoEmissaoContratado() {
		return servicoEmissaoContratado;
	}
	
	/**
	 * Set: servicoEmissaoContratado.
	 *
	 * @param servicoEmissaoContratado the servico emissao contratado
	 */
	public void setServicoEmissaoContratado(int servicoEmissaoContratado) {
		this.servicoEmissaoContratado = servicoEmissaoContratado;
	}
	
	/**
	 * Get: tipoRelacionamento.
	 *
	 * @return tipoRelacionamento
	 */
	public int getTipoRelacionamento() {
		return tipoRelacionamento;
	}
	
	/**
	 * Set: tipoRelacionamento.
	 *
	 * @param tipoRelacionamento the tipo relacionamento
	 */
	public void setTipoRelacionamento(int tipoRelacionamento) {
		this.tipoRelacionamento = tipoRelacionamento;
	}
	
	/**
	 * Get: tipoServico.
	 *
	 * @return tipoServico
	 */
	public int getTipoServico() {
		return tipoServico;
	}
	
	/**
	 * Set: tipoServico.
	 *
	 * @param tipoServico the tipo servico
	 */
	public void setTipoServico(int tipoServico) {
		this.tipoServico = tipoServico;
	}
	
	
}
