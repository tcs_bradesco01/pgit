/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/interfaz.ftl,v $
 * $Id: interfaz.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: interfaz.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.desautorizarpagamentosindividuais;

import java.util.List;

import br.com.bradesco.web.pgit.service.business.desautorizarpagamentosindividuais.bean.ConsultarPagtosIndAutorizadosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.desautorizarpagamentosindividuais.bean.ConsultarPagtosIndAutorizadosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.desautorizarpagamentosindividuais.bean.DesautorizarPagtosIndividuaisEntradaDTO;
import br.com.bradesco.web.pgit.service.business.desautorizarpagamentosindividuais.bean.DesautorizarPagtosIndividuaisSaidaDTO;

/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Interface do adaptador: DesautorizarPagamentosIndividuais
 * </p>
 * 
 * @comment CODIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public interface IDesautorizarPagamentosIndividuaisService {

    /**
     * M�todo de exemplo.
     */
    List<ConsultarPagtosIndAutorizadosSaidaDTO> consultarPagtosIndAutorizados (ConsultarPagtosIndAutorizadosEntradaDTO entradaDTO);
    
    /**
     * Desautorizar pagtos individuais.
     *
     * @param entradaDTO the entrada dto
     * @return the desautorizar pagtos individuais saida dto
     */
    DesautorizarPagtosIndividuaisSaidaDTO desautorizarPagtosIndividuais (DesautorizarPagtosIndividuaisEntradaDTO entradaDTO);
}

