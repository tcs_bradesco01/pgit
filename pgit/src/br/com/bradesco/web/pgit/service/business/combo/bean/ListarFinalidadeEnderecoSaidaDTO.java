/*
 * Nome: br.com.bradesco.web.pgit.service.business.combo.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.combo.bean;

/**
 * Nome: ListarFinalidadeEnderecoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarFinalidadeEnderecoSaidaDTO {

	
	/** Atributo cdFinalidadeEndereco. */
	private int cdFinalidadeEndereco;
	
	/** Atributo dsFinalidadeEndereco. */
	private String dsFinalidadeEndereco;
	
	/**
	 * Listar finalidade endereco saida dto.
	 *
	 * @param cdFinalidadeEndOpe the cd finalidade end ope
	 * @param dsFinalidadeEndOpe the ds finalidade end ope
	 */
	public ListarFinalidadeEnderecoSaidaDTO(int cdFinalidadeEndOpe,String dsFinalidadeEndOpe){
			this.cdFinalidadeEndereco = cdFinalidadeEndOpe;
			this.dsFinalidadeEndereco = dsFinalidadeEndOpe;
		
	}
	
	/**
	 * Get: cdFinalidadeEndereco.
	 *
	 * @return cdFinalidadeEndereco
	 */
	public int getCdFinalidadeEndereco() {
		return cdFinalidadeEndereco;
	}
	
	/**
	 * Set: cdFinalidadeEndereco.
	 *
	 * @param cdFinalidadeEndOpe the cd finalidade endereco
	 */
	public void setCdFinalidadeEndereco(int cdFinalidadeEndOpe) {
		this.cdFinalidadeEndereco = cdFinalidadeEndOpe;
	}
	
	/**
	 * Get: dsFinalidadeEndereco.
	 *
	 * @return dsFinalidadeEndereco
	 */
	public String getDsFinalidadeEndereco() {
		return dsFinalidadeEndereco;
	}
	
	/**
	 * Set: dsFinalidadeEndereco.
	 *
	 * @param dsFinalidadeEndOpe the ds finalidade endereco
	 */
	public void setDsFinalidadeEndereco(String dsFinalidadeEndOpe) {
		this.dsFinalidadeEndereco = dsFinalidadeEndOpe;
	}
	
}
