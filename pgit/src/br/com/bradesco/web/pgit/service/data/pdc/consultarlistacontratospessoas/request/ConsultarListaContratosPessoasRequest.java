/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultarlistacontratospessoas.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ConsultarListaContratosPessoasRequest.
 * 
 * @version $Revision$ $Date$
 */
public class ConsultarListaContratosPessoasRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _nrOcorrencias
     */
    private int _nrOcorrencias = 0;

    /**
     * keeps track of state for field: _nrOcorrencias
     */
    private boolean _has_nrOcorrencias;

    /**
     * Field _cdClub
     */
    private long _cdClub = 0;

    /**
     * keeps track of state for field: _cdClub
     */
    private boolean _has_cdClub;

    /**
     * Field _cdPessoaJuridicaContrato
     */
    private long _cdPessoaJuridicaContrato = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridicaContrato
     */
    private boolean _has_cdPessoaJuridicaContrato;

    /**
     * Field _cdTipoContratoNegocio
     */
    private int _cdTipoContratoNegocio = 0;

    /**
     * keeps track of state for field: _cdTipoContratoNegocio
     */
    private boolean _has_cdTipoContratoNegocio;

    /**
     * Field _nrSeqContratoNegocio
     */
    private long _nrSeqContratoNegocio = 0;

    /**
     * keeps track of state for field: _nrSeqContratoNegocio
     */
    private boolean _has_nrSeqContratoNegocio;

    /**
     * Field _cdCpfCnpjPssoa
     */
    private long _cdCpfCnpjPssoa = 0;

    /**
     * keeps track of state for field: _cdCpfCnpjPssoa
     */
    private boolean _has_cdCpfCnpjPssoa;

    /**
     * Field _cdFilialCnpjPssoa
     */
    private int _cdFilialCnpjPssoa = 0;

    /**
     * keeps track of state for field: _cdFilialCnpjPssoa
     */
    private boolean _has_cdFilialCnpjPssoa;

    /**
     * Field _cdControleCpfPssoa
     */
    private int _cdControleCpfPssoa = 0;

    /**
     * keeps track of state for field: _cdControleCpfPssoa
     */
    private boolean _has_cdControleCpfPssoa;


      //----------------/
     //- Constructors -/
    //----------------/

    public ConsultarListaContratosPessoasRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarlistacontratospessoas.request.ConsultarListaContratosPessoasRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdClub
     * 
     */
    public void deleteCdClub()
    {
        this._has_cdClub= false;
    } //-- void deleteCdClub() 

    /**
     * Method deleteCdControleCpfPssoa
     * 
     */
    public void deleteCdControleCpfPssoa()
    {
        this._has_cdControleCpfPssoa= false;
    } //-- void deleteCdControleCpfPssoa() 

    /**
     * Method deleteCdCpfCnpjPssoa
     * 
     */
    public void deleteCdCpfCnpjPssoa()
    {
        this._has_cdCpfCnpjPssoa= false;
    } //-- void deleteCdCpfCnpjPssoa() 

    /**
     * Method deleteCdFilialCnpjPssoa
     * 
     */
    public void deleteCdFilialCnpjPssoa()
    {
        this._has_cdFilialCnpjPssoa= false;
    } //-- void deleteCdFilialCnpjPssoa() 

    /**
     * Method deleteCdPessoaJuridicaContrato
     * 
     */
    public void deleteCdPessoaJuridicaContrato()
    {
        this._has_cdPessoaJuridicaContrato= false;
    } //-- void deleteCdPessoaJuridicaContrato() 

    /**
     * Method deleteCdTipoContratoNegocio
     * 
     */
    public void deleteCdTipoContratoNegocio()
    {
        this._has_cdTipoContratoNegocio= false;
    } //-- void deleteCdTipoContratoNegocio() 

    /**
     * Method deleteNrOcorrencias
     * 
     */
    public void deleteNrOcorrencias()
    {
        this._has_nrOcorrencias= false;
    } //-- void deleteNrOcorrencias() 

    /**
     * Method deleteNrSeqContratoNegocio
     * 
     */
    public void deleteNrSeqContratoNegocio()
    {
        this._has_nrSeqContratoNegocio= false;
    } //-- void deleteNrSeqContratoNegocio() 

    /**
     * Returns the value of field 'cdClub'.
     * 
     * @return long
     * @return the value of field 'cdClub'.
     */
    public long getCdClub()
    {
        return this._cdClub;
    } //-- long getCdClub() 

    /**
     * Returns the value of field 'cdControleCpfPssoa'.
     * 
     * @return int
     * @return the value of field 'cdControleCpfPssoa'.
     */
    public int getCdControleCpfPssoa()
    {
        return this._cdControleCpfPssoa;
    } //-- int getCdControleCpfPssoa() 

    /**
     * Returns the value of field 'cdCpfCnpjPssoa'.
     * 
     * @return long
     * @return the value of field 'cdCpfCnpjPssoa'.
     */
    public long getCdCpfCnpjPssoa()
    {
        return this._cdCpfCnpjPssoa;
    } //-- long getCdCpfCnpjPssoa() 

    /**
     * Returns the value of field 'cdFilialCnpjPssoa'.
     * 
     * @return int
     * @return the value of field 'cdFilialCnpjPssoa'.
     */
    public int getCdFilialCnpjPssoa()
    {
        return this._cdFilialCnpjPssoa;
    } //-- int getCdFilialCnpjPssoa() 

    /**
     * Returns the value of field 'cdPessoaJuridicaContrato'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridicaContrato'.
     */
    public long getCdPessoaJuridicaContrato()
    {
        return this._cdPessoaJuridicaContrato;
    } //-- long getCdPessoaJuridicaContrato() 

    /**
     * Returns the value of field 'cdTipoContratoNegocio'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoNegocio'.
     */
    public int getCdTipoContratoNegocio()
    {
        return this._cdTipoContratoNegocio;
    } //-- int getCdTipoContratoNegocio() 

    /**
     * Returns the value of field 'nrOcorrencias'.
     * 
     * @return int
     * @return the value of field 'nrOcorrencias'.
     */
    public int getNrOcorrencias()
    {
        return this._nrOcorrencias;
    } //-- int getNrOcorrencias() 

    /**
     * Returns the value of field 'nrSeqContratoNegocio'.
     * 
     * @return long
     * @return the value of field 'nrSeqContratoNegocio'.
     */
    public long getNrSeqContratoNegocio()
    {
        return this._nrSeqContratoNegocio;
    } //-- long getNrSeqContratoNegocio() 

    /**
     * Method hasCdClub
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdClub()
    {
        return this._has_cdClub;
    } //-- boolean hasCdClub() 

    /**
     * Method hasCdControleCpfPssoa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdControleCpfPssoa()
    {
        return this._has_cdControleCpfPssoa;
    } //-- boolean hasCdControleCpfPssoa() 

    /**
     * Method hasCdCpfCnpjPssoa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCpfCnpjPssoa()
    {
        return this._has_cdCpfCnpjPssoa;
    } //-- boolean hasCdCpfCnpjPssoa() 

    /**
     * Method hasCdFilialCnpjPssoa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFilialCnpjPssoa()
    {
        return this._has_cdFilialCnpjPssoa;
    } //-- boolean hasCdFilialCnpjPssoa() 

    /**
     * Method hasCdPessoaJuridicaContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridicaContrato()
    {
        return this._has_cdPessoaJuridicaContrato;
    } //-- boolean hasCdPessoaJuridicaContrato() 

    /**
     * Method hasCdTipoContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoNegocio()
    {
        return this._has_cdTipoContratoNegocio;
    } //-- boolean hasCdTipoContratoNegocio() 

    /**
     * Method hasNrOcorrencias
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrOcorrencias()
    {
        return this._has_nrOcorrencias;
    } //-- boolean hasNrOcorrencias() 

    /**
     * Method hasNrSeqContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSeqContratoNegocio()
    {
        return this._has_nrSeqContratoNegocio;
    } //-- boolean hasNrSeqContratoNegocio() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdClub'.
     * 
     * @param cdClub the value of field 'cdClub'.
     */
    public void setCdClub(long cdClub)
    {
        this._cdClub = cdClub;
        this._has_cdClub = true;
    } //-- void setCdClub(long) 

    /**
     * Sets the value of field 'cdControleCpfPssoa'.
     * 
     * @param cdControleCpfPssoa the value of field
     * 'cdControleCpfPssoa'.
     */
    public void setCdControleCpfPssoa(int cdControleCpfPssoa)
    {
        this._cdControleCpfPssoa = cdControleCpfPssoa;
        this._has_cdControleCpfPssoa = true;
    } //-- void setCdControleCpfPssoa(int) 

    /**
     * Sets the value of field 'cdCpfCnpjPssoa'.
     * 
     * @param cdCpfCnpjPssoa the value of field 'cdCpfCnpjPssoa'.
     */
    public void setCdCpfCnpjPssoa(long cdCpfCnpjPssoa)
    {
        this._cdCpfCnpjPssoa = cdCpfCnpjPssoa;
        this._has_cdCpfCnpjPssoa = true;
    } //-- void setCdCpfCnpjPssoa(long) 

    /**
     * Sets the value of field 'cdFilialCnpjPssoa'.
     * 
     * @param cdFilialCnpjPssoa the value of field
     * 'cdFilialCnpjPssoa'.
     */
    public void setCdFilialCnpjPssoa(int cdFilialCnpjPssoa)
    {
        this._cdFilialCnpjPssoa = cdFilialCnpjPssoa;
        this._has_cdFilialCnpjPssoa = true;
    } //-- void setCdFilialCnpjPssoa(int) 

    /**
     * Sets the value of field 'cdPessoaJuridicaContrato'.
     * 
     * @param cdPessoaJuridicaContrato the value of field
     * 'cdPessoaJuridicaContrato'.
     */
    public void setCdPessoaJuridicaContrato(long cdPessoaJuridicaContrato)
    {
        this._cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
        this._has_cdPessoaJuridicaContrato = true;
    } //-- void setCdPessoaJuridicaContrato(long) 

    /**
     * Sets the value of field 'cdTipoContratoNegocio'.
     * 
     * @param cdTipoContratoNegocio the value of field
     * 'cdTipoContratoNegocio'.
     */
    public void setCdTipoContratoNegocio(int cdTipoContratoNegocio)
    {
        this._cdTipoContratoNegocio = cdTipoContratoNegocio;
        this._has_cdTipoContratoNegocio = true;
    } //-- void setCdTipoContratoNegocio(int) 

    /**
     * Sets the value of field 'nrOcorrencias'.
     * 
     * @param nrOcorrencias the value of field 'nrOcorrencias'.
     */
    public void setNrOcorrencias(int nrOcorrencias)
    {
        this._nrOcorrencias = nrOcorrencias;
        this._has_nrOcorrencias = true;
    } //-- void setNrOcorrencias(int) 

    /**
     * Sets the value of field 'nrSeqContratoNegocio'.
     * 
     * @param nrSeqContratoNegocio the value of field
     * 'nrSeqContratoNegocio'.
     */
    public void setNrSeqContratoNegocio(long nrSeqContratoNegocio)
    {
        this._nrSeqContratoNegocio = nrSeqContratoNegocio;
        this._has_nrSeqContratoNegocio = true;
    } //-- void setNrSeqContratoNegocio(long) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ConsultarListaContratosPessoasRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultarlistacontratospessoas.request.ConsultarListaContratosPessoasRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultarlistacontratospessoas.request.ConsultarListaContratosPessoasRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultarlistacontratospessoas.request.ConsultarListaContratosPessoasRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarlistacontratospessoas.request.ConsultarListaContratosPessoasRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
