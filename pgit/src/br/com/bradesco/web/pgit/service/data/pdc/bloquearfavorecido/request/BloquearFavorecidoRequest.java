/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.bloquearfavorecido.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;

/**
 * Class BloquearFavorecidoRequest.
 * 
 * @version $Revision$ $Date$
 */
public class BloquearFavorecidoRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdPessoaJuridicaContrato
     */
    private long _cdPessoaJuridicaContrato = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridicaContrato
     */
    private boolean _has_cdPessoaJuridicaContrato;

    /**
     * Field _cdTipoContratoNegocio
     */
    private int _cdTipoContratoNegocio = 0;

    /**
     * keeps track of state for field: _cdTipoContratoNegocio
     */
    private boolean _has_cdTipoContratoNegocio;

    /**
     * Field _nrSequencialContratoNegocio
     */
    private long _nrSequencialContratoNegocio = 0;

    /**
     * keeps track of state for field: _nrSequencialContratoNegocio
     */
    private boolean _has_nrSequencialContratoNegocio;

    /**
     * Field _cdFavoritoClientePagador
     */
    private long _cdFavoritoClientePagador = 0;

    /**
     * keeps track of state for field: _cdFavoritoClientePagador
     */
    private boolean _has_cdFavoritoClientePagador;

    /**
     * Field _cdSituacaoFavorecido
     */
    private int _cdSituacaoFavorecido = 0;

    /**
     * keeps track of state for field: _cdSituacaoFavorecido
     */
    private boolean _has_cdSituacaoFavorecido;

    /**
     * Field _cdMotivoBloqueioFavorecido
     */
    private int _cdMotivoBloqueioFavorecido = 0;

    /**
     * keeps track of state for field: _cdMotivoBloqueioFavorecido
     */
    private boolean _has_cdMotivoBloqueioFavorecido;


      //----------------/
     //- Constructors -/
    //----------------/

    public BloquearFavorecidoRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.bloquearfavorecido.request.BloquearFavorecidoRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdFavoritoClientePagador
     * 
     */
    public void deleteCdFavoritoClientePagador()
    {
        this._has_cdFavoritoClientePagador= false;
    } //-- void deleteCdFavoritoClientePagador() 

    /**
     * Method deleteCdMotivoBloqueioFavorecido
     * 
     */
    public void deleteCdMotivoBloqueioFavorecido()
    {
        this._has_cdMotivoBloqueioFavorecido= false;
    } //-- void deleteCdMotivoBloqueioFavorecido() 

    /**
     * Method deleteCdPessoaJuridicaContrato
     * 
     */
    public void deleteCdPessoaJuridicaContrato()
    {
        this._has_cdPessoaJuridicaContrato= false;
    } //-- void deleteCdPessoaJuridicaContrato() 

    /**
     * Method deleteCdSituacaoFavorecido
     * 
     */
    public void deleteCdSituacaoFavorecido()
    {
        this._has_cdSituacaoFavorecido= false;
    } //-- void deleteCdSituacaoFavorecido() 

    /**
     * Method deleteCdTipoContratoNegocio
     * 
     */
    public void deleteCdTipoContratoNegocio()
    {
        this._has_cdTipoContratoNegocio= false;
    } //-- void deleteCdTipoContratoNegocio() 

    /**
     * Method deleteNrSequencialContratoNegocio
     * 
     */
    public void deleteNrSequencialContratoNegocio()
    {
        this._has_nrSequencialContratoNegocio= false;
    } //-- void deleteNrSequencialContratoNegocio() 

    /**
     * Returns the value of field 'cdFavoritoClientePagador'.
     * 
     * @return long
     * @return the value of field 'cdFavoritoClientePagador'.
     */
    public long getCdFavoritoClientePagador()
    {
        return this._cdFavoritoClientePagador;
    } //-- long getCdFavoritoClientePagador() 

    /**
     * Returns the value of field 'cdMotivoBloqueioFavorecido'.
     * 
     * @return int
     * @return the value of field 'cdMotivoBloqueioFavorecido'.
     */
    public int getCdMotivoBloqueioFavorecido()
    {
        return this._cdMotivoBloqueioFavorecido;
    } //-- int getCdMotivoBloqueioFavorecido() 

    /**
     * Returns the value of field 'cdPessoaJuridicaContrato'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridicaContrato'.
     */
    public long getCdPessoaJuridicaContrato()
    {
        return this._cdPessoaJuridicaContrato;
    } //-- long getCdPessoaJuridicaContrato() 

    /**
     * Returns the value of field 'cdSituacaoFavorecido'.
     * 
     * @return int
     * @return the value of field 'cdSituacaoFavorecido'.
     */
    public int getCdSituacaoFavorecido()
    {
        return this._cdSituacaoFavorecido;
    } //-- int getCdSituacaoFavorecido() 

    /**
     * Returns the value of field 'cdTipoContratoNegocio'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoNegocio'.
     */
    public int getCdTipoContratoNegocio()
    {
        return this._cdTipoContratoNegocio;
    } //-- int getCdTipoContratoNegocio() 

    /**
     * Returns the value of field 'nrSequencialContratoNegocio'.
     * 
     * @return long
     * @return the value of field 'nrSequencialContratoNegocio'.
     */
    public long getNrSequencialContratoNegocio()
    {
        return this._nrSequencialContratoNegocio;
    } //-- long getNrSequencialContratoNegocio() 

    /**
     * Method hasCdFavoritoClientePagador
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFavoritoClientePagador()
    {
        return this._has_cdFavoritoClientePagador;
    } //-- boolean hasCdFavoritoClientePagador() 

    /**
     * Method hasCdMotivoBloqueioFavorecido
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMotivoBloqueioFavorecido()
    {
        return this._has_cdMotivoBloqueioFavorecido;
    } //-- boolean hasCdMotivoBloqueioFavorecido() 

    /**
     * Method hasCdPessoaJuridicaContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridicaContrato()
    {
        return this._has_cdPessoaJuridicaContrato;
    } //-- boolean hasCdPessoaJuridicaContrato() 

    /**
     * Method hasCdSituacaoFavorecido
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSituacaoFavorecido()
    {
        return this._has_cdSituacaoFavorecido;
    } //-- boolean hasCdSituacaoFavorecido() 

    /**
     * Method hasCdTipoContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoNegocio()
    {
        return this._has_cdTipoContratoNegocio;
    } //-- boolean hasCdTipoContratoNegocio() 

    /**
     * Method hasNrSequencialContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequencialContratoNegocio()
    {
        return this._has_nrSequencialContratoNegocio;
    } //-- boolean hasNrSequencialContratoNegocio() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdFavoritoClientePagador'.
     * 
     * @param cdFavoritoClientePagador the value of field
     * 'cdFavoritoClientePagador'.
     */
    public void setCdFavoritoClientePagador(long cdFavoritoClientePagador)
    {
        this._cdFavoritoClientePagador = cdFavoritoClientePagador;
        this._has_cdFavoritoClientePagador = true;
    } //-- void setCdFavoritoClientePagador(long) 

    /**
     * Sets the value of field 'cdMotivoBloqueioFavorecido'.
     * 
     * @param cdMotivoBloqueioFavorecido the value of field
     * 'cdMotivoBloqueioFavorecido'.
     */
    public void setCdMotivoBloqueioFavorecido(int cdMotivoBloqueioFavorecido)
    {
        this._cdMotivoBloqueioFavorecido = cdMotivoBloqueioFavorecido;
        this._has_cdMotivoBloqueioFavorecido = true;
    } //-- void setCdMotivoBloqueioFavorecido(int) 

    /**
     * Sets the value of field 'cdPessoaJuridicaContrato'.
     * 
     * @param cdPessoaJuridicaContrato the value of field
     * 'cdPessoaJuridicaContrato'.
     */
    public void setCdPessoaJuridicaContrato(long cdPessoaJuridicaContrato)
    {
        this._cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
        this._has_cdPessoaJuridicaContrato = true;
    } //-- void setCdPessoaJuridicaContrato(long) 

    /**
     * Sets the value of field 'cdSituacaoFavorecido'.
     * 
     * @param cdSituacaoFavorecido the value of field
     * 'cdSituacaoFavorecido'.
     */
    public void setCdSituacaoFavorecido(int cdSituacaoFavorecido)
    {
        this._cdSituacaoFavorecido = cdSituacaoFavorecido;
        this._has_cdSituacaoFavorecido = true;
    } //-- void setCdSituacaoFavorecido(int) 

    /**
     * Sets the value of field 'cdTipoContratoNegocio'.
     * 
     * @param cdTipoContratoNegocio the value of field
     * 'cdTipoContratoNegocio'.
     */
    public void setCdTipoContratoNegocio(int cdTipoContratoNegocio)
    {
        this._cdTipoContratoNegocio = cdTipoContratoNegocio;
        this._has_cdTipoContratoNegocio = true;
    } //-- void setCdTipoContratoNegocio(int) 

    /**
     * Sets the value of field 'nrSequencialContratoNegocio'.
     * 
     * @param nrSequencialContratoNegocio the value of field
     * 'nrSequencialContratoNegocio'.
     */
    public void setNrSequencialContratoNegocio(long nrSequencialContratoNegocio)
    {
        this._nrSequencialContratoNegocio = nrSequencialContratoNegocio;
        this._has_nrSequencialContratoNegocio = true;
    } //-- void setNrSequencialContratoNegocio(long) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return BloquearFavorecidoRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.bloquearfavorecido.request.BloquearFavorecidoRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.bloquearfavorecido.request.BloquearFavorecidoRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.bloquearfavorecido.request.BloquearFavorecidoRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.bloquearfavorecido.request.BloquearFavorecidoRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
