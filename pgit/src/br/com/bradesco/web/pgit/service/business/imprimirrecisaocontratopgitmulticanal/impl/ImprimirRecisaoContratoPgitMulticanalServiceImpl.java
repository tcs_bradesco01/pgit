/*
 * Nome: br.com.bradesco.web.pgit.service.business.imprimirrecisaocontratopgitmulticanal.impl
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.imprimirrecisaocontratopgitmulticanal.impl;

import java.util.List;

import br.com.bradesco.web.aq.application.util.BradescoCommonServiceFactory;
import br.com.bradesco.web.pgit.service.business.imprimirrecisaocontratopgitmulticanal.IImprimirRecisaoContratoPgitMulticanalService;
import br.com.bradesco.web.pgit.service.business.imprimirrecisaocontratopgitmulticanal.bean.ImprimirRecisaoContratoPgitMulticanalEntradaDTO;
import br.com.bradesco.web.pgit.service.business.imprimirrecisaocontratopgitmulticanal.bean.ImprimirRecisaoContratoPgitMulticanalSaidaDTO;
import br.com.bradesco.web.pgit.service.data.pdc.FactoryAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.imprimirrecisaocontratopgitmulticanal.request.ImprimirRecisaoContratoPgitMulticanalRequest;
import br.com.bradesco.web.pgit.service.data.pdc.imprimirrecisaocontratopgitmulticanal.response.ImprimirRecisaoContratoPgitMulticanalResponse;
import br.com.bradesco.web.pgit.utils.PgitUtil;

/**
 * Nome: ImprimirRecisaoContratoPgitMulticanalServiceImpl
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ImprimirRecisaoContratoPgitMulticanalServiceImpl implements IImprimirRecisaoContratoPgitMulticanalService {

	/** Atributo factoryAdapter. */
	private FactoryAdapter factoryAdapter;

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.imprimirrecisaocontratopgitmulticanal.IImprimirRecisaoContratoPgitMulticanalService#imprimirRecisaoContrato(br.com.bradesco.web.pgit.service.business.imprimirrecisaocontratopgitmulticanal.bean.ImprimirRecisaoContratoPgitMulticanalEntradaDTO)
	 */
	public ImprimirRecisaoContratoPgitMulticanalSaidaDTO imprimirRecisaoContrato(ImprimirRecisaoContratoPgitMulticanalEntradaDTO entrada) {
		ImprimirRecisaoContratoPgitMulticanalRequest request = new ImprimirRecisaoContratoPgitMulticanalRequest();
		request.setCdpessoaJuridicaContrato(PgitUtil.verificaLongNulo(entrada.getCdPessoaJuridicaContrato()));
		request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(entrada.getCdTipoContratoNegocio()));
		request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(entrada.getNrSequenciaContratoNegocio()));
		request.setCdVersao(PgitUtil.verificaIntegerNulo(entrada.getCdVersao()));

		ImprimirRecisaoContratoPgitMulticanalResponse response = getFactoryAdapter().getImprimirRecisaoContratoPgitMulticanalPDCAdapter().invokeProcess(request);

		ImprimirRecisaoContratoPgitMulticanalSaidaDTO saida = new ImprimirRecisaoContratoPgitMulticanalSaidaDTO();

		List listaProtocolos = BradescoCommonServiceFactory.getSessionManager().getProtocols();
		Long protocolo = null;
		if (listaProtocolos != null && !listaProtocolos.isEmpty()) {
			protocolo = Long.valueOf((String)listaProtocolos.get(listaProtocolos.size()-1));
		}
		saida.setProtocolo(protocolo);

		return saida;
	}

	/**
	 * Get: factoryAdapter.
	 *
	 * @return factoryAdapter
	 */
	public FactoryAdapter getFactoryAdapter() {
		return factoryAdapter;
	}

	/**
	 * Set: factoryAdapter.
	 *
	 * @param factoryAdapter the factory adapter
	 */
	public void setFactoryAdapter(FactoryAdapter factoryAdapter) {
		this.factoryAdapter = factoryAdapter;
	}

}