package br.com.bradesco.web.pgit.service.business.mantercontrato.bean;

import java.util.Date;

import br.com.bradesco.web.pgit.view.converters.FormatarData;

public class ConsultarContaDebTarifaSaidaDTO {

    private String codMensagem;
    private String mensagem;
    private String cdUsuarioInclusao;
    private String cdUsuarioInclusaoExterno;
    private String hrInclusaoRegistro;
    private String cdOperacaoCanalInclusao;
    private Integer cdTipoCanalInclusao;
    private String dsTpoCanalInclusao;
    private String cdUsuarioManutencao;
    private String cdUsuarioManutencaoExterno;
    private String hrManutencaoRegistro;
    private String cdOperacaoCanalManutencao;
    private Integer cdTipoCanalManutencao;
    private String dsTipoCanalManutencao;
    
    public String getValidaUsuarioInclusao(){
    	if(cdUsuarioInclusao != null && !"".equals(cdUsuarioInclusao)){
    		return cdUsuarioInclusao;
        }else{
        	return cdUsuarioInclusaoExterno;
        }
    }
    
    public String getValidaUsuarioManutencao(){
    	if(cdUsuarioManutencao != null && !"".equals(cdUsuarioManutencao)){
    		return cdUsuarioManutencao;
    	}else{
    		return cdUsuarioManutencaoExterno;
    	}
    }
    
	public String getCodMensagem() {
		return codMensagem;
	}
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}
	public String getMensagem() {
		return mensagem;
	}
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	public String getCdUsuarioInclusao() {
		return cdUsuarioInclusao;
	}
	public void setCdUsuarioInclusao(String cdUsuarioInclusao) {
		this.cdUsuarioInclusao = cdUsuarioInclusao;
	}
	public String getCdUsuarioInclusaoExterno() {
		return cdUsuarioInclusaoExterno;
	}
	public void setCdUsuarioInclusaoExterno(String cdUsuarioInclusaoExterno) {
		this.cdUsuarioInclusaoExterno = cdUsuarioInclusaoExterno;
	}
	public String getHrInclusaoRegistro() {
		return hrInclusaoRegistro;
	}
	public void setHrInclusaoRegistro(String hrInclusaoRegistro) {
		this.hrInclusaoRegistro = hrInclusaoRegistro;
	}
	public String getCdOperacaoCanalInclusao() {
		return cdOperacaoCanalInclusao;
	}
	public void setCdOperacaoCanalInclusao(String cdOperacaoCanalInclusao) {
		this.cdOperacaoCanalInclusao = cdOperacaoCanalInclusao;
	}
	public Integer getCdTipoCanalInclusao() {
		return cdTipoCanalInclusao;
	}
	public void setCdTipoCanalInclusao(Integer cdTipoCanalInclusao) {
		this.cdTipoCanalInclusao = cdTipoCanalInclusao;
	}
	public String getDsTpoCanalInclusao() {
		return dsTpoCanalInclusao;
	}
	public void setDsTpoCanalInclusao(String dsTpoCanalInclusao) {
		this.dsTpoCanalInclusao = dsTpoCanalInclusao;
	}
	public String getCdUsuarioManutencao() {
		return cdUsuarioManutencao;
	}
	public void setCdUsuarioManutencao(String cdUsuarioManutencao) {
		this.cdUsuarioManutencao = cdUsuarioManutencao;
	}
	public String getCdUsuarioManutencaoExterno() {
		return cdUsuarioManutencaoExterno;
	}
	public void setCdUsuarioManutencaoExterno(String cdUsuarioManutencaoExterno) {
		this.cdUsuarioManutencaoExterno = cdUsuarioManutencaoExterno;
	}
	public String getHrManutencaoRegistro() {
		return hrManutencaoRegistro;
	}
	public void setHrManutencaoRegistro(String hrManutencaoRegistro) {
		this.hrManutencaoRegistro = hrManutencaoRegistro;
	}
	public String getCdOperacaoCanalManutencao() {
		return cdOperacaoCanalManutencao;
	}
	public void setCdOperacaoCanalManutencao(String cdOperacaoCanalManutencao) {
		this.cdOperacaoCanalManutencao = cdOperacaoCanalManutencao;
	}
	public Integer getCdTipoCanalManutencao() {
		return cdTipoCanalManutencao;
	}
	public void setCdTipoCanalManutencao(Integer cdTipoCanalManutencao) {
		this.cdTipoCanalManutencao = cdTipoCanalManutencao;
	}
	public String getDsTipoCanalManutencao() {
		return dsTipoCanalManutencao;
	}
	public void setDsTipoCanalManutencao(String dsTipoCanalManutencao) {
		this.dsTipoCanalManutencao = dsTipoCanalManutencao;
	}

	public String getHoraIncRegistroFormatada() {
		Date horaDate = FormatarData.formataTimestampFromPdc(hrInclusaoRegistro);
		return FormatarData.formataTimestampFromPdc(horaDate);
	}
	
	public String getHoraManutRegistroFormatada() {
		Date horaDate = FormatarData.formataTimestampFromPdc(hrManutencaoRegistro);
		return FormatarData.formataTimestampFromPdc(horaDate);
	}
}