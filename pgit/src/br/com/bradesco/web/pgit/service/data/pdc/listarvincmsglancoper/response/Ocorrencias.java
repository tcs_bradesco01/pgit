/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listarvincmsglancoper.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdMensagemLinhaExtrato
     */
    private int _cdMensagemLinhaExtrato = 0;

    /**
     * keeps track of state for field: _cdMensagemLinhaExtrato
     */
    private boolean _has_cdMensagemLinhaExtrato;

    /**
     * Field _cdIdentificadorLancamentoDebito
     */
    private int _cdIdentificadorLancamentoDebito = 0;

    /**
     * keeps track of state for field:
     * _cdIdentificadorLancamentoDebito
     */
    private boolean _has_cdIdentificadorLancamentoDebito;

    /**
     * Field _dsIdentificadorLancamentoDebito
     */
    private java.lang.String _dsIdentificadorLancamentoDebito;

    /**
     * Field _cdIdentificadorLancamentoCredito
     */
    private int _cdIdentificadorLancamentoCredito = 0;

    /**
     * keeps track of state for field:
     * _cdIdentificadorLancamentoCredito
     */
    private boolean _has_cdIdentificadorLancamentoCredito;

    /**
     * Field _dsIdentificadorLancamentoCredito
     */
    private java.lang.String _dsIdentificadorLancamentoCredito;

    /**
     * Field _cdProdutoServicoOperacao
     */
    private int _cdProdutoServicoOperacao = 0;

    /**
     * keeps track of state for field: _cdProdutoServicoOperacao
     */
    private boolean _has_cdProdutoServicoOperacao;

    /**
     * Field _dsProdutoServicoOperacao
     */
    private java.lang.String _dsProdutoServicoOperacao;

    /**
     * Field _cdProdutoOperacaoRelacionado
     */
    private int _cdProdutoOperacaoRelacionado = 0;

    /**
     * keeps track of state for field: _cdProdutoOperacaoRelacionado
     */
    private boolean _has_cdProdutoOperacaoRelacionado;

    /**
     * Field _dsProdutoOperacaoRelacionado
     */
    private java.lang.String _dsProdutoOperacaoRelacionado;

    /**
     * Field _cdRelacionamentoProduto
     */
    private int _cdRelacionamentoProduto = 0;

    /**
     * keeps track of state for field: _cdRelacionamentoProduto
     */
    private boolean _has_cdRelacionamentoProduto;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarvincmsglancoper.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdIdentificadorLancamentoCredito
     * 
     */
    public void deleteCdIdentificadorLancamentoCredito()
    {
        this._has_cdIdentificadorLancamentoCredito= false;
    } //-- void deleteCdIdentificadorLancamentoCredito() 

    /**
     * Method deleteCdIdentificadorLancamentoDebito
     * 
     */
    public void deleteCdIdentificadorLancamentoDebito()
    {
        this._has_cdIdentificadorLancamentoDebito= false;
    } //-- void deleteCdIdentificadorLancamentoDebito() 

    /**
     * Method deleteCdMensagemLinhaExtrato
     * 
     */
    public void deleteCdMensagemLinhaExtrato()
    {
        this._has_cdMensagemLinhaExtrato= false;
    } //-- void deleteCdMensagemLinhaExtrato() 

    /**
     * Method deleteCdProdutoOperacaoRelacionado
     * 
     */
    public void deleteCdProdutoOperacaoRelacionado()
    {
        this._has_cdProdutoOperacaoRelacionado= false;
    } //-- void deleteCdProdutoOperacaoRelacionado() 

    /**
     * Method deleteCdProdutoServicoOperacao
     * 
     */
    public void deleteCdProdutoServicoOperacao()
    {
        this._has_cdProdutoServicoOperacao= false;
    } //-- void deleteCdProdutoServicoOperacao() 

    /**
     * Method deleteCdRelacionamentoProduto
     * 
     */
    public void deleteCdRelacionamentoProduto()
    {
        this._has_cdRelacionamentoProduto= false;
    } //-- void deleteCdRelacionamentoProduto() 

    /**
     * Returns the value of field
     * 'cdIdentificadorLancamentoCredito'.
     * 
     * @return int
     * @return the value of field 'cdIdentificadorLancamentoCredito'
     */
    public int getCdIdentificadorLancamentoCredito()
    {
        return this._cdIdentificadorLancamentoCredito;
    } //-- int getCdIdentificadorLancamentoCredito() 

    /**
     * Returns the value of field
     * 'cdIdentificadorLancamentoDebito'.
     * 
     * @return int
     * @return the value of field 'cdIdentificadorLancamentoDebito'.
     */
    public int getCdIdentificadorLancamentoDebito()
    {
        return this._cdIdentificadorLancamentoDebito;
    } //-- int getCdIdentificadorLancamentoDebito() 

    /**
     * Returns the value of field 'cdMensagemLinhaExtrato'.
     * 
     * @return int
     * @return the value of field 'cdMensagemLinhaExtrato'.
     */
    public int getCdMensagemLinhaExtrato()
    {
        return this._cdMensagemLinhaExtrato;
    } //-- int getCdMensagemLinhaExtrato() 

    /**
     * Returns the value of field 'cdProdutoOperacaoRelacionado'.
     * 
     * @return int
     * @return the value of field 'cdProdutoOperacaoRelacionado'.
     */
    public int getCdProdutoOperacaoRelacionado()
    {
        return this._cdProdutoOperacaoRelacionado;
    } //-- int getCdProdutoOperacaoRelacionado() 

    /**
     * Returns the value of field 'cdProdutoServicoOperacao'.
     * 
     * @return int
     * @return the value of field 'cdProdutoServicoOperacao'.
     */
    public int getCdProdutoServicoOperacao()
    {
        return this._cdProdutoServicoOperacao;
    } //-- int getCdProdutoServicoOperacao() 

    /**
     * Returns the value of field 'cdRelacionamentoProduto'.
     * 
     * @return int
     * @return the value of field 'cdRelacionamentoProduto'.
     */
    public int getCdRelacionamentoProduto()
    {
        return this._cdRelacionamentoProduto;
    } //-- int getCdRelacionamentoProduto() 

    /**
     * Returns the value of field
     * 'dsIdentificadorLancamentoCredito'.
     * 
     * @return String
     * @return the value of field 'dsIdentificadorLancamentoCredito'
     */
    public java.lang.String getDsIdentificadorLancamentoCredito()
    {
        return this._dsIdentificadorLancamentoCredito;
    } //-- java.lang.String getDsIdentificadorLancamentoCredito() 

    /**
     * Returns the value of field
     * 'dsIdentificadorLancamentoDebito'.
     * 
     * @return String
     * @return the value of field 'dsIdentificadorLancamentoDebito'.
     */
    public java.lang.String getDsIdentificadorLancamentoDebito()
    {
        return this._dsIdentificadorLancamentoDebito;
    } //-- java.lang.String getDsIdentificadorLancamentoDebito() 

    /**
     * Returns the value of field 'dsProdutoOperacaoRelacionado'.
     * 
     * @return String
     * @return the value of field 'dsProdutoOperacaoRelacionado'.
     */
    public java.lang.String getDsProdutoOperacaoRelacionado()
    {
        return this._dsProdutoOperacaoRelacionado;
    } //-- java.lang.String getDsProdutoOperacaoRelacionado() 

    /**
     * Returns the value of field 'dsProdutoServicoOperacao'.
     * 
     * @return String
     * @return the value of field 'dsProdutoServicoOperacao'.
     */
    public java.lang.String getDsProdutoServicoOperacao()
    {
        return this._dsProdutoServicoOperacao;
    } //-- java.lang.String getDsProdutoServicoOperacao() 

    /**
     * Method hasCdIdentificadorLancamentoCredito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIdentificadorLancamentoCredito()
    {
        return this._has_cdIdentificadorLancamentoCredito;
    } //-- boolean hasCdIdentificadorLancamentoCredito() 

    /**
     * Method hasCdIdentificadorLancamentoDebito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIdentificadorLancamentoDebito()
    {
        return this._has_cdIdentificadorLancamentoDebito;
    } //-- boolean hasCdIdentificadorLancamentoDebito() 

    /**
     * Method hasCdMensagemLinhaExtrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMensagemLinhaExtrato()
    {
        return this._has_cdMensagemLinhaExtrato;
    } //-- boolean hasCdMensagemLinhaExtrato() 

    /**
     * Method hasCdProdutoOperacaoRelacionado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdutoOperacaoRelacionado()
    {
        return this._has_cdProdutoOperacaoRelacionado;
    } //-- boolean hasCdProdutoOperacaoRelacionado() 

    /**
     * Method hasCdProdutoServicoOperacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdutoServicoOperacao()
    {
        return this._has_cdProdutoServicoOperacao;
    } //-- boolean hasCdProdutoServicoOperacao() 

    /**
     * Method hasCdRelacionamentoProduto
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdRelacionamentoProduto()
    {
        return this._has_cdRelacionamentoProduto;
    } //-- boolean hasCdRelacionamentoProduto() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdIdentificadorLancamentoCredito'.
     * 
     * @param cdIdentificadorLancamentoCredito the value of field
     * 'cdIdentificadorLancamentoCredito'.
     */
    public void setCdIdentificadorLancamentoCredito(int cdIdentificadorLancamentoCredito)
    {
        this._cdIdentificadorLancamentoCredito = cdIdentificadorLancamentoCredito;
        this._has_cdIdentificadorLancamentoCredito = true;
    } //-- void setCdIdentificadorLancamentoCredito(int) 

    /**
     * Sets the value of field 'cdIdentificadorLancamentoDebito'.
     * 
     * @param cdIdentificadorLancamentoDebito the value of field
     * 'cdIdentificadorLancamentoDebito'.
     */
    public void setCdIdentificadorLancamentoDebito(int cdIdentificadorLancamentoDebito)
    {
        this._cdIdentificadorLancamentoDebito = cdIdentificadorLancamentoDebito;
        this._has_cdIdentificadorLancamentoDebito = true;
    } //-- void setCdIdentificadorLancamentoDebito(int) 

    /**
     * Sets the value of field 'cdMensagemLinhaExtrato'.
     * 
     * @param cdMensagemLinhaExtrato the value of field
     * 'cdMensagemLinhaExtrato'.
     */
    public void setCdMensagemLinhaExtrato(int cdMensagemLinhaExtrato)
    {
        this._cdMensagemLinhaExtrato = cdMensagemLinhaExtrato;
        this._has_cdMensagemLinhaExtrato = true;
    } //-- void setCdMensagemLinhaExtrato(int) 

    /**
     * Sets the value of field 'cdProdutoOperacaoRelacionado'.
     * 
     * @param cdProdutoOperacaoRelacionado the value of field
     * 'cdProdutoOperacaoRelacionado'.
     */
    public void setCdProdutoOperacaoRelacionado(int cdProdutoOperacaoRelacionado)
    {
        this._cdProdutoOperacaoRelacionado = cdProdutoOperacaoRelacionado;
        this._has_cdProdutoOperacaoRelacionado = true;
    } //-- void setCdProdutoOperacaoRelacionado(int) 

    /**
     * Sets the value of field 'cdProdutoServicoOperacao'.
     * 
     * @param cdProdutoServicoOperacao the value of field
     * 'cdProdutoServicoOperacao'.
     */
    public void setCdProdutoServicoOperacao(int cdProdutoServicoOperacao)
    {
        this._cdProdutoServicoOperacao = cdProdutoServicoOperacao;
        this._has_cdProdutoServicoOperacao = true;
    } //-- void setCdProdutoServicoOperacao(int) 

    /**
     * Sets the value of field 'cdRelacionamentoProduto'.
     * 
     * @param cdRelacionamentoProduto the value of field
     * 'cdRelacionamentoProduto'.
     */
    public void setCdRelacionamentoProduto(int cdRelacionamentoProduto)
    {
        this._cdRelacionamentoProduto = cdRelacionamentoProduto;
        this._has_cdRelacionamentoProduto = true;
    } //-- void setCdRelacionamentoProduto(int) 

    /**
     * Sets the value of field 'dsIdentificadorLancamentoCredito'.
     * 
     * @param dsIdentificadorLancamentoCredito the value of field
     * 'dsIdentificadorLancamentoCredito'.
     */
    public void setDsIdentificadorLancamentoCredito(java.lang.String dsIdentificadorLancamentoCredito)
    {
        this._dsIdentificadorLancamentoCredito = dsIdentificadorLancamentoCredito;
    } //-- void setDsIdentificadorLancamentoCredito(java.lang.String) 

    /**
     * Sets the value of field 'dsIdentificadorLancamentoDebito'.
     * 
     * @param dsIdentificadorLancamentoDebito the value of field
     * 'dsIdentificadorLancamentoDebito'.
     */
    public void setDsIdentificadorLancamentoDebito(java.lang.String dsIdentificadorLancamentoDebito)
    {
        this._dsIdentificadorLancamentoDebito = dsIdentificadorLancamentoDebito;
    } //-- void setDsIdentificadorLancamentoDebito(java.lang.String) 

    /**
     * Sets the value of field 'dsProdutoOperacaoRelacionado'.
     * 
     * @param dsProdutoOperacaoRelacionado the value of field
     * 'dsProdutoOperacaoRelacionado'.
     */
    public void setDsProdutoOperacaoRelacionado(java.lang.String dsProdutoOperacaoRelacionado)
    {
        this._dsProdutoOperacaoRelacionado = dsProdutoOperacaoRelacionado;
    } //-- void setDsProdutoOperacaoRelacionado(java.lang.String) 

    /**
     * Sets the value of field 'dsProdutoServicoOperacao'.
     * 
     * @param dsProdutoServicoOperacao the value of field
     * 'dsProdutoServicoOperacao'.
     */
    public void setDsProdutoServicoOperacao(java.lang.String dsProdutoServicoOperacao)
    {
        this._dsProdutoServicoOperacao = dsProdutoServicoOperacao;
    } //-- void setDsProdutoServicoOperacao(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listarvincmsglancoper.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listarvincmsglancoper.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listarvincmsglancoper.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarvincmsglancoper.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
