/*
 * Nome: br.com.bradesco.web.pgit.service.business.autorizarpagamentosconsolidados.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.autorizarpagamentosconsolidados.bean;

import java.math.BigDecimal;

/**
 * Nome: OcorrenciasDetPagConsPendAutSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class OcorrenciasDetPagConsPendAutSaidaDTO {

    /** Atributo nrPagamento. */
    private String nrPagamento;

    /** Atributo vlPagamento. */
    private BigDecimal vlPagamento;

    /** Atributo vlPagamentoFormatado. */
    private String vlPagamentoFormatado;

    /** Atributo cdCnpjCpfFavorecido. */
    private Long cdCnpjCpfFavorecido;

    /** Atributo cdFilialCnpjCpfFavorecido. */
    private Integer cdFilialCnpjCpfFavorecido;

    /** Atributo cdControleCnpjCpfFavorecido. */
    private Integer cdControleCnpjCpfFavorecido;

    /** Atributo dsBeneficio. */
    private String dsBeneficio;

    /** Atributo cdBanco. */
    private Integer cdBanco;

    /** Atributo cdAgencia. */
    private Integer cdAgencia;

    /** Atributo cdDigitoAgencia. */
    private String cdDigitoAgencia;

    /** Atributo cdConta. */
    private Long cdConta;

    /** Atributo cdDigitoConta. */
    private String cdDigitoConta;

    /** Atributo cdTipoCanal. */
    private Integer cdTipoCanal;

    /** Atributo cdFavorecido. */
    private String cdFavorecido;

    /** Atributo cdTipoTela. */
    private Integer cdTipoTela;

    /** Atributo bancoAgenciaContaCreditoFormatado. */
    private String bancoAgenciaContaCreditoFormatado;

    /** Atributo bancoFormatado. */
    private String bancoFormatado;

    /** Atributo agenciaFormatada. */
    private String agenciaFormatada;

    /** Atributo contaFormatada. */
    private String contaFormatada;

    /** Atributo favorecidoBeneficiario. */
    private String favorecidoBeneficiario;

    /** Atributo check. */
    private boolean check;

    /** Atributo dsEfetivacaoPagamento. */
    private String dsEfetivacaoPagamento;
    
	private String dsIndicadorAutorizacao;
	private Long cdLoteInterno;
	private String dsTipoLayout;
	private String dsIndicadorPagamento;
	private String cdIspbPagtoDestino;
	private String contaPagtoDestino;

    /**
     * Get: dsEfetivacaoPagamento.
     *
     * @return dsEfetivacaoPagamento
     */
    public String getDsEfetivacaoPagamento() {
	return dsEfetivacaoPagamento;
    }

    /**
     * Set: dsEfetivacaoPagamento.
     *
     * @param dsEfetivacaoPagamento the ds efetivacao pagamento
     */
    public void setDsEfetivacaoPagamento(String dsEfetivacaoPagamento) {
	this.dsEfetivacaoPagamento = dsEfetivacaoPagamento;
    }

    /**
     * Set: cdDigitoAgencia.
     *
     * @param cdDigitoAgencia the cd digito agencia
     */
    public void setCdDigitoAgencia(String cdDigitoAgencia) {
	this.cdDigitoAgencia = cdDigitoAgencia;
    }

    /**
     * Get: favorecidoBeneficiario.
     *
     * @return favorecidoBeneficiario
     */
    public String getFavorecidoBeneficiario() {
	return favorecidoBeneficiario;
    }

    /**
     * Set: favorecidoBeneficiario.
     *
     * @param favorecidoBeneficiario the favorecido beneficiario
     */
    public void setFavorecidoBeneficiario(String favorecidoBeneficiario) {
	this.favorecidoBeneficiario = favorecidoBeneficiario;
    }

    /**
     * Get: agenciaFormatada.
     *
     * @return agenciaFormatada
     */
    public String getAgenciaFormatada() {
	return agenciaFormatada;
    }

    /**
     * Set: agenciaFormatada.
     *
     * @param agenciaFormatada the agencia formatada
     */
    public void setAgenciaFormatada(String agenciaFormatada) {
	this.agenciaFormatada = agenciaFormatada;
    }

    /**
     * Get: bancoAgenciaContaCreditoFormatado.
     *
     * @return bancoAgenciaContaCreditoFormatado
     */
    public String getBancoAgenciaContaCreditoFormatado() {
	return bancoAgenciaContaCreditoFormatado;
    }

    /**
     * Set: bancoAgenciaContaCreditoFormatado.
     *
     * @param bancoAgenciaContaCreditoFormatado the banco agencia conta credito formatado
     */
    public void setBancoAgenciaContaCreditoFormatado(String bancoAgenciaContaCreditoFormatado) {
	this.bancoAgenciaContaCreditoFormatado = bancoAgenciaContaCreditoFormatado;
    }

    /**
     * Get: bancoFormatado.
     *
     * @return bancoFormatado
     */
    public String getBancoFormatado() {
	return bancoFormatado;
    }

    /**
     * Set: bancoFormatado.
     *
     * @param bancoFormatado the banco formatado
     */
    public void setBancoFormatado(String bancoFormatado) {
	this.bancoFormatado = bancoFormatado;
    }

    /**
     * Get: cdAgencia.
     *
     * @return cdAgencia
     */
    public Integer getCdAgencia() {
	return cdAgencia;
    }

    /**
     * Set: cdAgencia.
     *
     * @param cdAgencia the cd agencia
     */
    public void setCdAgencia(Integer cdAgencia) {
	this.cdAgencia = cdAgencia;
    }

    /**
     * Get: cdBanco.
     *
     * @return cdBanco
     */
    public Integer getCdBanco() {
	return cdBanco;
    }

    /**
     * Set: cdBanco.
     *
     * @param cdBanco the cd banco
     */
    public void setCdBanco(Integer cdBanco) {
	this.cdBanco = cdBanco;
    }

    /**
     * Get: cdCnpjCpfFavorecido.
     *
     * @return cdCnpjCpfFavorecido
     */
    public Long getCdCnpjCpfFavorecido() {
	return cdCnpjCpfFavorecido;
    }

    /**
     * Set: cdCnpjCpfFavorecido.
     *
     * @param cdCnpjCpfFavorecido the cd cnpj cpf favorecido
     */
    public void setCdCnpjCpfFavorecido(Long cdCnpjCpfFavorecido) {
	this.cdCnpjCpfFavorecido = cdCnpjCpfFavorecido;
    }

    /**
     * Get: cdConta.
     *
     * @return cdConta
     */
    public Long getCdConta() {
	return cdConta;
    }

    /**
     * Set: cdConta.
     *
     * @param cdConta the cd conta
     */
    public void setCdConta(Long cdConta) {
	this.cdConta = cdConta;
    }

    /**
     * Get: cdControleCnpjCpfFavorecido.
     *
     * @return cdControleCnpjCpfFavorecido
     */
    public Integer getCdControleCnpjCpfFavorecido() {
	return cdControleCnpjCpfFavorecido;
    }

    /**
     * Set: cdControleCnpjCpfFavorecido.
     *
     * @param cdControleCnpjCpfFavorecido the cd controle cnpj cpf favorecido
     */
    public void setCdControleCnpjCpfFavorecido(Integer cdControleCnpjCpfFavorecido) {
	this.cdControleCnpjCpfFavorecido = cdControleCnpjCpfFavorecido;
    }

   

    /**
     * Get: cdDigitoConta.
     *
     * @return cdDigitoConta
     */
    public String getCdDigitoConta() {
	return cdDigitoConta;
    }

    /**
     * Set: cdDigitoConta.
     *
     * @param cdDigitoConta the cd digito conta
     */
    public void setCdDigitoConta(String cdDigitoConta) {
	this.cdDigitoConta = cdDigitoConta;
    }

    /**
     * Get: cdFavorecido.
     *
     * @return cdFavorecido
     */
    public String getCdFavorecido() {
	return cdFavorecido;
    }

    /**
     * Set: cdFavorecido.
     *
     * @param cdFavorecido the cd favorecido
     */
    public void setCdFavorecido(String cdFavorecido) {
	this.cdFavorecido = cdFavorecido;
    }

    /**
     * Get: cdFilialCnpjCpfFavorecido.
     *
     * @return cdFilialCnpjCpfFavorecido
     */
    public Integer getCdFilialCnpjCpfFavorecido() {
	return cdFilialCnpjCpfFavorecido;
    }

    /**
     * Set: cdFilialCnpjCpfFavorecido.
     *
     * @param cdFilialCnpjCpfFavorecido the cd filial cnpj cpf favorecido
     */
    public void setCdFilialCnpjCpfFavorecido(Integer cdFilialCnpjCpfFavorecido) {
	this.cdFilialCnpjCpfFavorecido = cdFilialCnpjCpfFavorecido;
    }

    /**
     * Get: cdTipoCanal.
     *
     * @return cdTipoCanal
     */
    public Integer getCdTipoCanal() {
	return cdTipoCanal;
    }

    /**
     * Set: cdTipoCanal.
     *
     * @param cdTipoCanal the cd tipo canal
     */
    public void setCdTipoCanal(Integer cdTipoCanal) {
	this.cdTipoCanal = cdTipoCanal;
    }

    /**
     * Get: cdTipoTela.
     *
     * @return cdTipoTela
     */
    public Integer getCdTipoTela() {
	return cdTipoTela;
    }

    /**
     * Set: cdTipoTela.
     *
     * @param cdTipoTela the cd tipo tela
     */
    public void setCdTipoTela(Integer cdTipoTela) {
	this.cdTipoTela = cdTipoTela;
    }

    /**
     * Is check.
     *
     * @return true, if is check
     */
    public boolean isCheck() {
	return check;
    }

    /**
     * Set: check.
     *
     * @param check the check
     */
    public void setCheck(boolean check) {
	this.check = check;
    }

    /**
     * Get: contaFormatada.
     *
     * @return contaFormatada
     */
    public String getContaFormatada() {
	return contaFormatada;
    }

    /**
     * Set: contaFormatada.
     *
     * @param contaFormatada the conta formatada
     */
    public void setContaFormatada(String contaFormatada) {
	this.contaFormatada = contaFormatada;
    }

    /**
     * Get: dsBeneficio.
     *
     * @return dsBeneficio
     */
    public String getDsBeneficio() {
	return dsBeneficio;
    }

    /**
     * Set: dsBeneficio.
     *
     * @param dsBeneficio the ds beneficio
     */
    public void setDsBeneficio(String dsBeneficio) {
	this.dsBeneficio = dsBeneficio;
    }

    /**
     * Get: nrPagamento.
     *
     * @return nrPagamento
     */
    public String getNrPagamento() {
	return nrPagamento;
    }

    /**
     * Set: nrPagamento.
     *
     * @param nrPagamento the nr pagamento
     */
    public void setNrPagamento(String nrPagamento) {
	this.nrPagamento = nrPagamento;
    }

    /**
     * Get: vlPagamento.
     *
     * @return vlPagamento
     */
    public BigDecimal getVlPagamento() {
	return vlPagamento;
    }

    /**
     * Set: vlPagamento.
     *
     * @param vlPagamento the vl pagamento
     */
    public void setVlPagamento(BigDecimal vlPagamento) {
	this.vlPagamento = vlPagamento;
    }

    /**
     * Get: vlPagamentoFormatado.
     *
     * @return vlPagamentoFormatado
     */
    public String getVlPagamentoFormatado() {
	return vlPagamentoFormatado;
    }

    /**
     * Set: vlPagamentoFormatado.
     *
     * @param vlPagamentoFormatado the vl pagamento formatado
     */
    public void setVlPagamentoFormatado(String vlPagamentoFormatado) {
	this.vlPagamentoFormatado = vlPagamentoFormatado;
    }

    /**
     * Get: cdDigitoAgencia.
     *
     * @return cdDigitoAgencia
     */
    public String getCdDigitoAgencia() {
        return cdDigitoAgencia;
    }

	public String getDsIndicadorAutorizacao() {
		return dsIndicadorAutorizacao;
	}

	public void setDsIndicadorAutorizacao(String dsIndicadorAutorizacao) {
		this.dsIndicadorAutorizacao = dsIndicadorAutorizacao;
	}

	public Long getCdLoteInterno() {
		return cdLoteInterno;
	}

	public void setCdLoteInterno(Long cdLoteInterno) {
		this.cdLoteInterno = cdLoteInterno;
	}

	public String getDsTipoLayout() {
		return dsTipoLayout;
	}

	public void setDsTipoLayout(String dsTipoLayout) {
		this.dsTipoLayout = dsTipoLayout;
	}

	public String getDsIndicadorPagamento() {
		return dsIndicadorPagamento;
	}

	public void setDsIndicadorPagamento(String dsIndicadorPagamento) {
		this.dsIndicadorPagamento = dsIndicadorPagamento;
	}

	public String getCdIspbPagtoDestino() {
		return cdIspbPagtoDestino;
	}

	public void setCdIspbPagtoDestino(String cdIspbPagtoDestino) {
		this.cdIspbPagtoDestino = cdIspbPagtoDestino;
	}

	public String getContaPagtoDestino() {
		return contaPagtoDestino;
	}

	public void setContaPagtoDestino(String contaPagtoDestino) {
		this.contaPagtoDestino = contaPagtoDestino;
	}

}
