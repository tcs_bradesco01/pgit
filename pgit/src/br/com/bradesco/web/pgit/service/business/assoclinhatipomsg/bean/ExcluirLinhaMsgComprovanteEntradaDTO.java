/*
 * Nome: br.com.bradesco.web.pgit.service.business.assoclinhatipomsg.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.assoclinhatipomsg.bean;

/**
 * Nome: ExcluirLinhaMsgComprovanteEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ExcluirLinhaMsgComprovanteEntradaDTO {
	
	/** Atributo tipoMensagem. */
	private int tipoMensagem;
	
	/** Atributo prefixo. */
	private String prefixo;
	
	/** Atributo codigoMensagem. */
	private int codigoMensagem;
	
	/** Atributo recurso. */
	private int recurso;
	
	/** Atributo idioma. */
	private int idioma;
	
	/**
	 * Get: codigoMensagem.
	 *
	 * @return codigoMensagem
	 */
	public int getCodigoMensagem() {
		return codigoMensagem;
	}
	
	/**
	 * Set: codigoMensagem.
	 *
	 * @param codigoMensagem the codigo mensagem
	 */
	public void setCodigoMensagem(int codigoMensagem) {
		this.codigoMensagem = codigoMensagem;
	}
	
	/**
	 * Get: idioma.
	 *
	 * @return idioma
	 */
	public int getIdioma() {
		return idioma;
	}
	
	/**
	 * Set: idioma.
	 *
	 * @param idioma the idioma
	 */
	public void setIdioma(int idioma) {
		this.idioma = idioma;
	}
	
	/**
	 * Get: prefixo.
	 *
	 * @return prefixo
	 */
	public String getPrefixo() {
		return prefixo;
	}
	
	/**
	 * Set: prefixo.
	 *
	 * @param prefixo the prefixo
	 */
	public void setPrefixo(String prefixo) {
		this.prefixo = prefixo;
	}
	
	/**
	 * Get: recurso.
	 *
	 * @return recurso
	 */
	public int getRecurso() {
		return recurso;
	}
	
	/**
	 * Set: recurso.
	 *
	 * @param recurso the recurso
	 */
	public void setRecurso(int recurso) {
		this.recurso = recurso;
	}
	
	/**
	 * Get: tipoMensagem.
	 *
	 * @return tipoMensagem
	 */
	public int getTipoMensagem() {
		return tipoMensagem;
	}
	
	/**
	 * Set: tipoMensagem.
	 *
	 * @param tipoMensagem the tipo mensagem
	 */
	public void setTipoMensagem(int tipoMensagem) {
		this.tipoMensagem = tipoMensagem;
	}
	
	
	


}
