/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/interfaz.ftl,v $
 * $Id: interfaz.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: interfaz.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.arquivoretorno;

import java.util.List;

import br.com.bradesco.web.pgit.service.business.arquivorecebido.bean.ComboProdutoDTO;
import br.com.bradesco.web.pgit.service.business.arquivoretorno.bean.ArquivoRetornoDTO;
import br.com.bradesco.web.pgit.service.business.arquivoretorno.bean.InconsistentesRetornoDTO;
import br.com.bradesco.web.pgit.service.business.arquivoretorno.bean.LoteRetornoDTO;
import br.com.bradesco.web.pgit.service.business.arquivoretorno.bean.OcorrenciaRetornoDTO;
import br.com.bradesco.web.pgit.service.business.cmpi0000.bean.ListarClientesDTO;

/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Interface do adaptador: Arquivoremessaretorno
 * </p>
 * 
 * @comment CODIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public interface IArquivoRetornoService {

    /**
     * M�todo de exemplo.
     */
    void sampleArquivoremessaretorno();
    
    /**
     * Lista grid.
     *
     * @param pArquivoRetornoDTO the p arquivo retorno dto
     * @return the list< arquivo retorno dt o>
     */
    List<ArquivoRetornoDTO> listaGrid(ArquivoRetornoDTO pArquivoRetornoDTO);
    
    /**
     * Lista combo produto.
     *
     * @return the list< combo produto dt o>
     */
    List<ComboProdutoDTO> listaComboProduto();
    
    /**
     * Lista lote.
     *
     * @param pLoteDTO the p lote dto
     * @return the list< lote retorno dt o>
     */
    List<LoteRetornoDTO> listaLote(LoteRetornoDTO pLoteDTO);
    
    /**
     * Lista grid clientes.
     *
     * @param pListarClientesDTO the p listar clientes dto
     * @return the list< listar clientes dt o>
     */
    List<ListarClientesDTO> listaGridClientes(ListarClientesDTO pListarClientesDTO);

    /**
     * Lista grid inconsistentes.
     *
     * @param pInconsistentesRetornoDTO the p inconsistentes retorno dto
     * @return the list< inconsistentes retorno dt o>
     */
    List<InconsistentesRetornoDTO> listaGridInconsistentes(InconsistentesRetornoDTO pInconsistentesRetornoDTO); 

    /**
     * Lista ocorrencia retorno.
     *
     * @param pOcorrenciaRetornoDTO the p ocorrencia retorno dto
     * @return the list< ocorrencia retorno dt o>
     */
    List<OcorrenciaRetornoDTO> listaOcorrenciaRetorno(OcorrenciaRetornoDTO pOcorrenciaRetornoDTO);
}