/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/implementation.ftl,v $
 * $Id: implementation.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: implementation.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */
 
package br.com.bradesco.web.pgit.service.business.mantersolicgeracaoarqretbasesistema.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import br.com.bradesco.web.pgit.service.business.mantersolicgeracaoarqretbasesistema.IManterSolicGeracaoArqRetBaseSistemaService;
import br.com.bradesco.web.pgit.service.business.mantersolicgeracaoarqretbasesistema.IManterSolicGeracaoArqRetBaseSistemaServiceConstants;
import br.com.bradesco.web.pgit.service.business.mantersolicgeracaoarqretbasesistema.bean.DetalharSolBaseSistemaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicgeracaoarqretbasesistema.bean.DetalharSolBaseSistemaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicgeracaoarqretbasesistema.bean.ExcluirSolBaseSistemaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicgeracaoarqretbasesistema.bean.ExcluirSolBaseSistemaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicgeracaoarqretbasesistema.bean.IncluirSolBaseSistemaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicgeracaoarqretbasesistema.bean.IncluirSolBaseSistemaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicgeracaoarqretbasesistema.bean.ListarSolBaseSistemaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicgeracaoarqretbasesistema.bean.ListarSolBaseSistemaSaidaDTO;
import br.com.bradesco.web.pgit.service.data.pdc.FactoryAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.detalharsolbasesistema.request.DetalharSolBaseSistemaRequest;
import br.com.bradesco.web.pgit.service.data.pdc.detalharsolbasesistema.response.DetalharSolBaseSistemaResponse;
import br.com.bradesco.web.pgit.service.data.pdc.excluirsolbasesistema.request.ExcluirSolBaseSistemaRequest;
import br.com.bradesco.web.pgit.service.data.pdc.excluirsolbasesistema.response.ExcluirSolBaseSistemaResponse;
import br.com.bradesco.web.pgit.service.data.pdc.incluirsolbasesistema.request.IncluirSolBaseSistemaRequest;
import br.com.bradesco.web.pgit.service.data.pdc.incluirsolbasesistema.response.IncluirSolBaseSistemaResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarsolbasesistema.request.ListarSolBaseSistemaRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarsolbasesistema.response.ListarSolBaseSistemaResponse;
import br.com.bradesco.web.pgit.utils.PgitUtil;
import br.com.bradesco.web.pgit.view.converters.FormatarData;


/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Implementa��o do adaptador: ManterSolicGeracaoArqRetBaseSistema
 * </p>
 * 
 * @comment C�DIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public class ManterSolicGeracaoArqRetBaseSistemaServiceImpl implements IManterSolicGeracaoArqRetBaseSistemaService {
	
	/** Atributo factoryAdapter. */
	private FactoryAdapter factoryAdapter;
	
	/** Atributo nf. */
	private java.text.NumberFormat nf = java.text.NumberFormat.getInstance( new Locale( "pt_br" ) );
	
    /**
     * Get: factoryAdapter.
     *
     * @return factoryAdapter
     */
    public FactoryAdapter getFactoryAdapter() {
		return factoryAdapter;
	}

	/**
	 * Set: factoryAdapter.
	 *
	 * @param factoryAdapter the factory adapter
	 */
	public void setFactoryAdapter(FactoryAdapter factoryAdapter) {
		this.factoryAdapter = factoryAdapter;
	}
    /**
     * Construtor.
     */
    public ManterSolicGeracaoArqRetBaseSistemaServiceImpl() {
    }
    
    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.mantersolicgeracaoarqretbasesistema.IManterSolicGeracaoArqRetBaseSistemaService#listarSolBaseSistema(br.com.bradesco.web.pgit.service.business.mantersolicgeracaoarqretbasesistema.bean.ListarSolBaseSistemaEntradaDTO)
     */
    public List<ListarSolBaseSistemaSaidaDTO> listarSolBaseSistema (ListarSolBaseSistemaEntradaDTO entrada){
    	
		List<ListarSolBaseSistemaSaidaDTO> listaSaida = new ArrayList<ListarSolBaseSistemaSaidaDTO>();
		ListarSolBaseSistemaRequest request = new ListarSolBaseSistemaRequest();
		ListarSolBaseSistemaResponse response = new ListarSolBaseSistemaResponse();
		
		request.setNumeroOcorrencias(IManterSolicGeracaoArqRetBaseSistemaServiceConstants.NUMERO_OCORRENCIAS_CONSULTAR);
		request.setCdPessoaJuridicaContrato(PgitUtil.verificaLongNulo(entrada.getCdPessoaJuridicaContrato()));
		request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(entrada.getCdTipoContratoNegocio()));
		request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(entrada.getNrSequenciaContratoNegocio()));
		request.setDtSolicitacaoInicio(PgitUtil.verificaStringNula(entrada.getDtSolicitacaoInicio()));
		request.setDtSolicitacaoFim(PgitUtil.verificaStringNula(entrada.getDtSolicitacaoFim()));
		request.setNrSolicitacaoPagamento(PgitUtil.verificaIntegerNulo(entrada.getNrSolicitacaoPagamento()));
		request.setCdSituacaoSolicitacao(PgitUtil.verificaIntegerNulo(entrada.getCdSituacaoSolicitacao()));
		request.setCdorigemSolicitacao(PgitUtil.verificaIntegerNulo(entrada.getCdorigemSolicitacao()));
		
		response = getFactoryAdapter().getListarSolBaseSistemaPDCAdapter().invokeProcess(request);
		
		ListarSolBaseSistemaSaidaDTO saida;
		
		for(int i = 0;i<response.getOcorrenciasCount();i++){
			saida = new ListarSolBaseSistemaSaidaDTO();
			saida.setMensagem(response.getMensagem());
			saida.setCodMensagem(response.getCodMensagem());
			
			saida.setCdSituacaoSolicitacao(PgitUtil.verificaIntegerNulo(response.getOcorrencias(i).getCdSituacaoSolicitacao()));
			saida.setDsSituacaoSolicitacao(PgitUtil.verificaStringNula(response.getOcorrencias(i).getDsSituacaoSolicitacao()));
			saida.setHrAtendimentoSolicitacao(FormatarData.formatarDataTrilha(PgitUtil.verificaStringNula(response.getOcorrencias(i).getHrAtendimentoSolicitacao())));
			saida.setHrSolicitacao(PgitUtil.verificaStringNula(response.getOcorrencias(i).getHrSolicitacao()));
			saida.setNrSolicitacaoPagamento(PgitUtil.verificaIntegerNulo(response.getOcorrencias(i).getNrSolicitacaoPagamento()));
			saida.setQtdeRegistroSolicitacao(nf.format(PgitUtil.verificaLongNulo(response.getOcorrencias(i).getQtdeRegistroSolicitacao())));			
			saida.setResultadoProcessamento(PgitUtil.verificaStringNula(response.getOcorrencias(i).getResultadoProcessamento()));			
			saida.setHrSolicitacaoFormatada(FormatarData.formatarDataTrilha(PgitUtil.verificaStringNula(response.getOcorrencias(i).getHrSolicitacao())));
			
			listaSaida.add(saida);
		}	
		
		return listaSaida;
    	
    }
    
    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.mantersolicgeracaoarqretbasesistema.IManterSolicGeracaoArqRetBaseSistemaService#detalharSolBaseSistema(br.com.bradesco.web.pgit.service.business.mantersolicgeracaoarqretbasesistema.bean.DetalharSolBaseSistemaEntradaDTO)
     */
    public DetalharSolBaseSistemaSaidaDTO detalharSolBaseSistema (DetalharSolBaseSistemaEntradaDTO entrada){
    	DetalharSolBaseSistemaRequest request = new DetalharSolBaseSistemaRequest();
    	DetalharSolBaseSistemaResponse response = new DetalharSolBaseSistemaResponse();
    	
    	request.setNrSolicitacaoPagamento(PgitUtil.verificaIntegerNulo(entrada.getNrSolicitacaoPagamento()));
    	
    	response = getFactoryAdapter().getDetalharSolBaseSistemaPDCAdapter().invokeProcess(request);
    	
    	DetalharSolBaseSistemaSaidaDTO saida = new DetalharSolBaseSistemaSaidaDTO();    	
    	
    	saida.setNrSolicitacaoPagamento(PgitUtil.verificaIntegerNulo(response.getNrSolicitacaoPagamento()));
    	saida.setQtdeRegistrosSolicitacao(PgitUtil.verificaLongNulo(response.getQtdeRegistrosSolicitacao()));
    	saida.setResultadoProcessamento(PgitUtil.verificaStringNula(response.getResultadoProcessamento()));
    	saida.setDsCanalManutencao(PgitUtil.verificaStringNula(response.getDsCanalManutencao()));
    	saida.setDsSituacaoSolicitacao(PgitUtil.verificaStringNula(response.getDsSituacaoSolicitacao()));    	
    	saida.setMensagem(PgitUtil.verificaStringNula(response.getMensagem()));
    	saida.setCdBaseSistema(PgitUtil.verificaIntegerNulo(response.getCdBaseSistema()));
    	saida.setCodMensagem(PgitUtil.verificaStringNula(response.getCodMensagem()));
    	saida.setDsBaseSistema(PgitUtil.verificaStringNula(response.getDsBaseSistema()));
    	saida.setNmOperacaoFluxoManutencao(PgitUtil.verificaStringNula(response.getNmOperacaoFluxoManutencao()));
    	saida.setCdCanalManutencao(PgitUtil.verificaIntegerNulo(response.getCdCanalManutencao()));
    	saida.setCdAutenticacaoSegurancaManutencao(PgitUtil.verificaStringNula(response.getCdAutenticacaoSegurancaManutencao()));
    	saida.setCdCanalInclusao(PgitUtil.verificaIntegerNulo(response.getCdCanalInclusao()));
    	saida.setCdAutenticacaoSegurancaInclusao(PgitUtil.verificaStringNula(response.getCdAutenticacaoSegurancaInclusao()));
    	saida.setDsCanalInclusao(PgitUtil.verificaStringNula(response.getDsCanalInclusao()));
    	saida.setNmOperacaoFluxoInlcusao(PgitUtil.verificaStringNula(response.getNmOperacaoFluxoInlcusao()));
    	
    	saida.setVlrTarifaAtual(PgitUtil.verificaBigDecimalNulo(response.getVlrTarifaAtual()));
    	saida.setVlTarifaPadrao(PgitUtil.verificaBigDecimalNulo(response.getVlTarifaPadrao()));
    	saida.setNumPercentualDescTarifa(PgitUtil.verificaBigDecimalNulo(response.getNumPercentualDescTarifa()));
    	
    	saida.setHrInclusaoRegistro(PgitUtil.verificaStringNula(response.getHrInclusaoRegistro()));
    	saida.setHrManutencaoRegistro(PgitUtil.verificaStringNula(response.getHrManutencaoRegistro()));
    	saida.setHrSolicitacao(PgitUtil.verificaStringNula(response.getHrSolicitacao()));
    	saida.setHrAtendimentoSolicitacao(PgitUtil.verificaStringNula(response.getHrAtendimentoSolicitacao()));
    	saida.setHrAtendimentoSolicitacaoFormatada(FormatarData.formatarDataTrilha(PgitUtil.verificaStringNula(response.getHrAtendimentoSolicitacao())));
    	saida.setHrInclusaoRegistroFormatada(FormatarData.formatarDataTrilha(PgitUtil.verificaStringNula(response.getHrInclusaoRegistro())));
    	saida.setHrManutencaoRegistroFormatada(FormatarData.formatarDataTrilha(PgitUtil.verificaStringNula(response.getHrManutencaoRegistro())));
    	saida.setHrSolicitacaoFormatada(FormatarData.formatarDataTrilha(PgitUtil.verificaStringNula(response.getHrSolicitacao())));
    	
		return saida;
    }
    
    
    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.mantersolicgeracaoarqretbasesistema.IManterSolicGeracaoArqRetBaseSistemaService#excluirSolBaseSistema(br.com.bradesco.web.pgit.service.business.mantersolicgeracaoarqretbasesistema.bean.ExcluirSolBaseSistemaEntradaDTO)
     */
    public ExcluirSolBaseSistemaSaidaDTO excluirSolBaseSistema (ExcluirSolBaseSistemaEntradaDTO entrada){
    	
    	ExcluirSolBaseSistemaRequest request = new ExcluirSolBaseSistemaRequest();
    	ExcluirSolBaseSistemaResponse response = new ExcluirSolBaseSistemaResponse();
    	
    	request.setCdPessoaJuridicaContrato(PgitUtil.verificaLongNulo(entrada.getCdPessoaJuridicaContrato()));
    	request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(entrada.getCdTipoContratoNegocio()));
    	request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(entrada.getNrSequenciaContratoNegocio()));
    	request.setNrSolicitacaoGeracaoRetorno(PgitUtil.verificaIntegerNulo(entrada.getNrSolicitacaoGeracaoRetorno()));
    	
    	response = getFactoryAdapter().getExcluirSolBaseSistemaPDCAdapter().invokeProcess(request);
    	
    	ExcluirSolBaseSistemaSaidaDTO saida = new ExcluirSolBaseSistemaSaidaDTO();
    	
    	saida.setCodMensagem(PgitUtil.verificaStringNula(response.getCodMensagem()));
    	saida.setMensagem(PgitUtil.verificaStringNula(response.getMensagem()));
    	
		return saida;    
    }
    
    
    
    
    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.mantersolicgeracaoarqretbasesistema.IManterSolicGeracaoArqRetBaseSistemaService#incluirSolBaseSistema(br.com.bradesco.web.pgit.service.business.mantersolicgeracaoarqretbasesistema.bean.IncluirSolBaseSistemaEntradaDTO)
     */
    public IncluirSolBaseSistemaSaidaDTO incluirSolBaseSistema (IncluirSolBaseSistemaEntradaDTO entrada){
    	
    	IncluirSolBaseSistemaRequest request = new IncluirSolBaseSistemaRequest();
    	IncluirSolBaseSistemaResponse response = new IncluirSolBaseSistemaResponse();
    	IncluirSolBaseSistemaSaidaDTO saida = new IncluirSolBaseSistemaSaidaDTO();
    	
    	request.setCdBaseSistema(PgitUtil.verificaIntegerNulo(entrada.getCdBaseSistema()));
    	request.setCdPessoaJuridicaContrato(PgitUtil.verificaLongNulo(entrada.getCdPessoaJuridicaContrato()));
    	request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(entrada.getCdTipoContratoNegocio()));
    	request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(entrada.getNrSequenciaContratoNegocio()));
    	request.setVlTarifaPadrao(PgitUtil.verificaBigDecimalNulo(entrada.getVlTarifaPadrao()));
    	request.setNumPercentualDescTarifa(PgitUtil.verificaBigDecimalNulo(entrada.getNumPercentualDescTarifa()));
    	request.setVlrTarifaAtual(PgitUtil.verificaBigDecimalNulo(entrada.getVlrTarifaAtual()));
    	
    	response = getFactoryAdapter().getIncluirSolBaseSistemaPDCAdapter().invokeProcess(request);
    	
    	saida.setCodMensagem(PgitUtil.verificaStringNula(response.getCodMensagem()));
    	saida.setMensagem(PgitUtil.verificaStringNula(response.getMensagem()));
    	
    	
    	return saida;
    }
}