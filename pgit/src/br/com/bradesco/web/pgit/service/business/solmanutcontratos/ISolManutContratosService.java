/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/interfaz.ftl,v $
 * $Id: interfaz.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: interfaz.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.solmanutcontratos;

import java.util.List;

import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ConsultarManutencaoMeioTransmissaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ConsultarManutencaoMeioTransmissaoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.solmanutcontratos.bean.CancelarManutencaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.solmanutcontratos.bean.CancelarManutencaoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.solmanutcontratos.bean.ConsultarManutencaoConfigContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.solmanutcontratos.bean.ConsultarManutencaoConfigContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.solmanutcontratos.bean.ConsultarManutencaoContaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.solmanutcontratos.bean.ConsultarManutencaoContaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.solmanutcontratos.bean.ConsultarManutencaoParticipanteEntradaDTO;
import br.com.bradesco.web.pgit.service.business.solmanutcontratos.bean.ConsultarManutencaoParticipanteSaidaDTO;
import br.com.bradesco.web.pgit.service.business.solmanutcontratos.bean.ConsultarManutencaoServicoContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.solmanutcontratos.bean.ConsultarManutencaoServicoContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.solmanutcontratos.bean.ListarManutencaoContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.solmanutcontratos.bean.ListarManutencaoContratoSaidaDTO;

// TODO: Auto-generated Javadoc
/**
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Interface do adaptador: SolManutContratos
 * </p>.
 *
 * @comment CODIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public interface ISolManutContratosService {

    
    /**
     * Detalhar servico contrato.
     *
     * @param consultarManutencaoServicoContratoEntradaDTO the consultar manutencao servico contrato entrada dto
     * @return the consultar manutencao servico contrato saida dto
     */
    ConsultarManutencaoServicoContratoSaidaDTO detalharServicoContrato(ConsultarManutencaoServicoContratoEntradaDTO consultarManutencaoServicoContratoEntradaDTO);
    
    /**
     * Detalhar participante contrato.
     *
     * @param consultarManutencaoParticipanteEntradaDTO the consultar manutencao participante entrada dto
     * @return the consultar manutencao participante saida dto
     */
    ConsultarManutencaoParticipanteSaidaDTO detalharParticipanteContrato(ConsultarManutencaoParticipanteEntradaDTO consultarManutencaoParticipanteEntradaDTO) ;
    
    /**
     * Detalhar conta contrato.
     *
     * @param consultarManutencaoContaEntradaDTO the consultar manutencao conta entrada dto
     * @return the consultar manutencao conta saida dto
     */
    ConsultarManutencaoContaSaidaDTO detalharContaContrato(ConsultarManutencaoContaEntradaDTO consultarManutencaoContaEntradaDTO);
    
    /**
     * Cancelar manutencao.
     *
     * @param cancelarManutencaoEntradaDTO the cancelar manutencao entrada dto
     * @return the cancelar manutencao saida dto
     */
    CancelarManutencaoSaidaDTO cancelarManutencao(CancelarManutencaoEntradaDTO cancelarManutencaoEntradaDTO);
    
    /**
     * Listar manutencao contrato.
     *
     * @param listarManutencaoContratoEntradaDTO the listar manutencao contrato entrada dto
     * @return the list< listar manutencao contrato saida dt o>
     */
    List<ListarManutencaoContratoSaidaDTO> listarManutencaoContrato(ListarManutencaoContratoEntradaDTO listarManutencaoContratoEntradaDTO);

    /**
     * Consultar manutencao meio transmissao.
     *
     * @param entradaDTO the entrada dto
     * @return the consultar manutencao meio transmissao saida dto
     */
    ConsultarManutencaoMeioTransmissaoSaidaDTO consultarManutencaoMeioTransmissao(ConsultarManutencaoMeioTransmissaoEntradaDTO entradaDTO);
    
    /**
     * Consultar manutencao config contrato.
     *
     * @param entradaDTO the entrada dto
     * @return the consultar manutencao config contrato saida dto
     */
    ConsultarManutencaoConfigContratoSaidaDTO consultarManutencaoConfigContrato(ConsultarManutencaoConfigContratoEntradaDTO entradaDTO);
}

