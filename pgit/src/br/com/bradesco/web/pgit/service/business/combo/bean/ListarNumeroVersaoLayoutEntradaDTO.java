/*
 * Nome: br.com.bradesco.web.pgit.service.business.combo.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.combo.bean;

/**
 * Nome: CentroCustoEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarNumeroVersaoLayoutEntradaDTO {

	/** Atributo cdSistema. */
	private Integer numeroCombo;
	

	/**
	 * Nome: setNumeroCombo
	 *
	 * @exception
	 * @throws
	 * @param numeroCombo
	 */
	public void setNumeroCombo(Integer numeroCombo) {
		this.numeroCombo = numeroCombo;
	}

	/**
	 * Nome: getNumeroCombo
	 *
	 * @exception
	 * @throws
	 * @return numeroCombo
	 */
	public Integer getNumeroCombo() {
		return numeroCombo;
	}
	
}
