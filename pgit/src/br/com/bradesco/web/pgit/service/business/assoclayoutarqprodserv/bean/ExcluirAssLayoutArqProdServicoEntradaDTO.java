/*
 * Nome: br.com.bradesco.web.pgit.service.business.assoclayoutarqprodserv.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.assoclayoutarqprodserv.bean;

/**
 * Nome: ExcluirAssLayoutArqProdServicoEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ExcluirAssLayoutArqProdServicoEntradaDTO {

	/** Atributo tipoLayoutArquivo. */
	private int tipoLayoutArquivo;
	
	/** Atributo tipoServico. */
	private int tipoServico;
	
	/** Atributo cdIdentificadorLayoutNegocio. */
	private Integer cdIdentificadorLayoutNegocio;
	
	/**
	 * Get: tipoLayoutArquivo.
	 *
	 * @return tipoLayoutArquivo
	 */
	public int getTipoLayoutArquivo() {
		return tipoLayoutArquivo;
	}
	
	/**
	 * Set: tipoLayoutArquivo.
	 *
	 * @param tipoLayoutArquivo the tipo layout arquivo
	 */
	public void setTipoLayoutArquivo(int tipoLayoutArquivo) {
		this.tipoLayoutArquivo = tipoLayoutArquivo;
	}
	
	/**
	 * Get: tipoServico.
	 *
	 * @return tipoServico
	 */
	public int getTipoServico() {
		return tipoServico;
	}
	
	/**
	 * Set: tipoServico.
	 *
	 * @param tipoServico the tipo servico
	 */
	public void setTipoServico(int tipoServico) {
		this.tipoServico = tipoServico;
	}
	
	/**
	 * Get: cdIdentificadorLayoutNegocio.
	 *
	 * @return cdIdentificadorLayoutNegocio
	 */
	public Integer getCdIdentificadorLayoutNegocio() {
		return cdIdentificadorLayoutNegocio;
	}
	
	/**
	 * Set: cdIdentificadorLayoutNegocio.
	 *
	 * @param cdIdentificadorLayoutNegocio the cd identificador layout negocio
	 */
	public void setCdIdentificadorLayoutNegocio(Integer cdIdentificadorLayoutNegocio) {
		this.cdIdentificadorLayoutNegocio = cdIdentificadorLayoutNegocio;
	}
	
	
	
}
