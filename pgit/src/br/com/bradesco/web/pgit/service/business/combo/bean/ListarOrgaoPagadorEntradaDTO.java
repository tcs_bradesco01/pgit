/*
 * Nome: br.com.bradesco.web.pgit.service.business.combo.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.combo.bean;

/**
 * Nome: ListarOrgaoPagadorEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarOrgaoPagadorEntradaDTO{
    
    /** Atributo maximoOcorrencias. */
    private Integer maximoOcorrencias;
    
    /** Atributo cdOrgaoPagadorOrganizacao. */
    private Integer cdOrgaoPagadorOrganizacao;
    
    /** Atributo cdTipoUnidadeOrganizacao. */
    private Integer cdTipoUnidadeOrganizacao;
    
    /** Atributo cdPessoaJuridica. */
    private Long cdPessoaJuridica;
    
    /** Atributo nrSequenciaUnidadeOrganizacao. */
    private Integer nrSequenciaUnidadeOrganizacao;
    
    /** Atributo cdSituacaoOrgaoPagador. */
    private Integer cdSituacaoOrgaoPagador;
    
    /** Atributo cdTarifaOrgaoPagador. */
    private Integer cdTarifaOrgaoPagador;
    
	/**
	 * Get: cdOrgaoPagadorOrganizacao.
	 *
	 * @return cdOrgaoPagadorOrganizacao
	 */
	public Integer getCdOrgaoPagadorOrganizacao() {
		return cdOrgaoPagadorOrganizacao;
	}
	
	/**
	 * Set: cdOrgaoPagadorOrganizacao.
	 *
	 * @param cdOrgaoPagadorOrganizacao the cd orgao pagador organizacao
	 */
	public void setCdOrgaoPagadorOrganizacao(Integer cdOrgaoPagadorOrganizacao) {
		this.cdOrgaoPagadorOrganizacao = cdOrgaoPagadorOrganizacao;
	}
	
	/**
	 * Get: cdPessoaJuridica.
	 *
	 * @return cdPessoaJuridica
	 */
	public Long getCdPessoaJuridica() {
		return cdPessoaJuridica;
	}
	
	/**
	 * Set: cdPessoaJuridica.
	 *
	 * @param cdPessoaJuridica the cd pessoa juridica
	 */
	public void setCdPessoaJuridica(Long cdPessoaJuridica) {
		this.cdPessoaJuridica = cdPessoaJuridica;
	}
	
	/**
	 * Get: cdSituacaoOrgaoPagador.
	 *
	 * @return cdSituacaoOrgaoPagador
	 */
	public Integer getCdSituacaoOrgaoPagador() {
		return cdSituacaoOrgaoPagador;
	}
	
	/**
	 * Set: cdSituacaoOrgaoPagador.
	 *
	 * @param cdSituacaoOrgaoPagador the cd situacao orgao pagador
	 */
	public void setCdSituacaoOrgaoPagador(Integer cdSituacaoOrgaoPagador) {
		this.cdSituacaoOrgaoPagador = cdSituacaoOrgaoPagador;
	}
	
	/**
	 * Get: cdTarifaOrgaoPagador.
	 *
	 * @return cdTarifaOrgaoPagador
	 */
	public Integer getCdTarifaOrgaoPagador() {
		return cdTarifaOrgaoPagador;
	}
	
	/**
	 * Set: cdTarifaOrgaoPagador.
	 *
	 * @param cdTarifaOrgaoPagador the cd tarifa orgao pagador
	 */
	public void setCdTarifaOrgaoPagador(Integer cdTarifaOrgaoPagador) {
		this.cdTarifaOrgaoPagador = cdTarifaOrgaoPagador;
	}
	
	/**
	 * Get: cdTipoUnidadeOrganizacao.
	 *
	 * @return cdTipoUnidadeOrganizacao
	 */
	public Integer getCdTipoUnidadeOrganizacao() {
		return cdTipoUnidadeOrganizacao;
	}
	
	/**
	 * Set: cdTipoUnidadeOrganizacao.
	 *
	 * @param cdTipoUnidadeOrganizacao the cd tipo unidade organizacao
	 */
	public void setCdTipoUnidadeOrganizacao(Integer cdTipoUnidadeOrganizacao) {
		this.cdTipoUnidadeOrganizacao = cdTipoUnidadeOrganizacao;
	}
	
	/**
	 * Get: maximoOcorrencias.
	 *
	 * @return maximoOcorrencias
	 */
	public Integer getMaximoOcorrencias() {
		return maximoOcorrencias;
	}
	
	/**
	 * Set: maximoOcorrencias.
	 *
	 * @param maximoOcorrencias the maximo ocorrencias
	 */
	public void setMaximoOcorrencias(Integer maximoOcorrencias) {
		this.maximoOcorrencias = maximoOcorrencias;
	}
	
	/**
	 * Get: nrSequenciaUnidadeOrganizacao.
	 *
	 * @return nrSequenciaUnidadeOrganizacao
	 */
	public Integer getNrSequenciaUnidadeOrganizacao() {
		return nrSequenciaUnidadeOrganizacao;
	}
	
	/**
	 * Set: nrSequenciaUnidadeOrganizacao.
	 *
	 * @param nrSequenciaUnidadeOrganizacao the nr sequencia unidade organizacao
	 */
	public void setNrSequenciaUnidadeOrganizacao(
			Integer nrSequenciaUnidadeOrganizacao) {
		this.nrSequenciaUnidadeOrganizacao = nrSequenciaUnidadeOrganizacao;
	}
    
    
}
