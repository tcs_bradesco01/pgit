/*
 * Nome: br.com.bradesco.web.pgit.service.business.manterservicocomposto.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.manterservicocomposto.bean;

/**
 * Nome: IncluirServicoCompostoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class IncluirServicoCompostoSaidaDTO {

	/** Atributo codMensagem. */
	private String codMensagem;
    
    /** Atributo mensagem. */
    private String mensagem;
    
    
	/**
	 * Incluir servico composto saida dto.
	 */
	public IncluirServicoCompostoSaidaDTO() {
		super();
	}
	
	/**
	 * Incluir servico composto saida dto.
	 *
	 * @param codMensagem the cod mensagem
	 * @param mensagem the mensagem
	 */
	public IncluirServicoCompostoSaidaDTO(String codMensagem, String mensagem) {
		super();
		this.codMensagem = codMensagem;
		this.mensagem = mensagem;
	}
	
	
	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}
	
	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
}
