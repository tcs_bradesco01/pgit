/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/interfaz.ftl,v $
 * $Id: interfaz.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: interfaz.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.solrelcontratos;

import java.util.List;

import br.com.bradesco.web.pgit.service.business.solmanutcontratos.bean.DetalharSoliRelatorioPagamentoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.solmanutcontratos.bean.DetalharSoliRelatorioPagamentoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.solmanutcontratos.bean.IncSoliRelatorioPagamentoValidacaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.solmanutcontratos.bean.IncSoliRelatorioPagamentoValidacaoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.solmanutcontratos.bean.IncluirSoliRelatorioPagamentoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.solmanutcontratos.bean.IncluirSoliRelatorioPagamentoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.solmanutcontratos.bean.ListarSoliRelatorioPagamentoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.solmanutcontratos.bean.ListarSoliRelatorioPagamentoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.solrelcontratos.bean.CancelarSolRelContratosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.solrelcontratos.bean.CancelarSolRelContratosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.solrelcontratos.bean.DetalharSolRelContratosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.solrelcontratos.bean.DetalharSolRelContratosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.solrelcontratos.bean.ExcluirSoliRelatorioPagamentoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.solrelcontratos.bean.ExcluirSoliRelatorioPagamentoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.solrelcontratos.bean.IncluirSolRelContratosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.solrelcontratos.bean.IncluirSolRelContratosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.solrelcontratos.bean.ListarSolRelContratosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.solrelcontratos.bean.ListarSolRelContratosSaidaDTO;

/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Interface do adaptador: ManterSolicitacaoRelatoriosContratos
 * </p>
 * 
 * @comment CODIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public interface ISolRelContratosService {

  
	/**
	 * Listar solicitacao relatorio contratos.
	 *
	 * @param listarSolRelContratosEntradaDTO the listar sol rel contratos entrada dto
	 * @return the list< listar sol rel contratos saida dt o>
	 */
	List<ListarSolRelContratosSaidaDTO> listarSolicitacaoRelatorioContratos (ListarSolRelContratosEntradaDTO listarSolRelContratosEntradaDTO);
	
	/**
	 * Detalhar solicitacao relatorio contratos.
	 *
	 * @param detalharSolRelContratosEntradaDTO the detalhar sol rel contratos entrada dto
	 * @return the detalhar sol rel contratos saida dto
	 */
	DetalharSolRelContratosSaidaDTO detalharSolicitacaoRelatorioContratos (DetalharSolRelContratosEntradaDTO detalharSolRelContratosEntradaDTO);
	
	/**
	 * Solicitar relatorio contratos.
	 *
	 * @param incluirSolRelContratosEntradaDTO the incluir sol rel contratos entrada dto
	 * @return the incluir sol rel contratos saida dto
	 */
	IncluirSolRelContratosSaidaDTO solicitarRelatorioContratos (IncluirSolRelContratosEntradaDTO incluirSolRelContratosEntradaDTO);
	
	/**
	 * Cancelar relatorio contratos.
	 *
	 * @param cancelarSolRelContratosEntradaDTO the cancelar sol rel contratos entrada dto
	 * @return the cancelar sol rel contratos saida dto
	 */
	CancelarSolRelContratosSaidaDTO cancelarRelatorioContratos(CancelarSolRelContratosEntradaDTO cancelarSolRelContratosEntradaDTO);

	/**
	 * Listar relatorio pagamento.
	 *
	 * @param entrada the entrada
	 * @return the list< listar soli relatorio pagamento saida dt o>
	 */
	List<ListarSoliRelatorioPagamentoSaidaDTO> listarRelatorioPagamento(ListarSoliRelatorioPagamentoEntradaDTO entrada);
	
    /**
     * Detalhar relatorio pagamento.
     *
     * @param entrada the entrada
     * @return the detalhar soli relatorio pagamento saida dto
     */
    DetalharSoliRelatorioPagamentoSaidaDTO detalharRelatorioPagamento(DetalharSoliRelatorioPagamentoEntradaDTO entrada);
    
    /**
     * Excluir relatorio pagamento.
     *
     * @param entrada the entrada
     * @return the excluir soli relatorio pagamento saida dto
     */
    ExcluirSoliRelatorioPagamentoSaidaDTO  excluirRelatorioPagamento(ExcluirSoliRelatorioPagamentoEntradaDTO entrada);
    
    /**
     * Validar incluir pagamento.
     *
     * @param entrada the entrada
     * @return the inc soli relatorio pagamento validacao saida dto
     */
    IncSoliRelatorioPagamentoValidacaoSaidaDTO validarIncluirPagamento(IncSoliRelatorioPagamentoValidacaoEntradaDTO entrada);
    
    /**
     * Incluir relatorio pagamento.
     *
     * @param entrada the entrada
     * @return the incluir soli relatorio pagamento saida dto
     */
    IncluirSoliRelatorioPagamentoSaidaDTO incluirRelatorioPagamento(IncluirSoliRelatorioPagamentoEntradaDTO entrada);
}

