/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/implementation.ftl,v $
 * $Id: implementation.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: implementation.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */
 
package br.com.bradesco.web.pgit.service.business.moedassistema.impl;

import java.util.ArrayList;
import java.util.List;

import br.com.bradesco.web.pgit.service.business.moedassistema.IMoedasSisServiceConstants;
import br.com.bradesco.web.pgit.service.business.moedassistema.IMoedasSistemaService;
import br.com.bradesco.web.pgit.service.business.moedassistema.bean.MoedaSistemaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.moedassistema.bean.MoedaSistemaSaidaDTO;
import br.com.bradesco.web.pgit.service.data.pdc.FactoryAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.detalharmoedassistema.request.DetalharMoedasSistemaRequest;
import br.com.bradesco.web.pgit.service.data.pdc.detalharmoedassistema.response.DetalharMoedasSistemaResponse;
import br.com.bradesco.web.pgit.service.data.pdc.excluirmoedassistema.request.ExcluirMoedasSistemaRequest;
import br.com.bradesco.web.pgit.service.data.pdc.excluirmoedassistema.response.ExcluirMoedasSistemaResponse;
import br.com.bradesco.web.pgit.service.data.pdc.incluirmoedassistema.request.IncluirMoedasSistemaRequest;
import br.com.bradesco.web.pgit.service.data.pdc.incluirmoedassistema.response.IncluirMoedasSistemaResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarmoedassistema.request.ListarMoedasSistemaRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarmoedassistema.response.ListarMoedasSistemaResponse;


/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Implementa��o do adaptador: ManterMoedasSistema
 * </p>
 * 
 * @comment C�DIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public class MoedasSistemaServiceImpl implements IMoedasSistemaService {
	
	/** Atributo factoryAdapter. */
	private FactoryAdapter factoryAdapter;

    /**
     * Get: factoryAdapter.
     *
     * @return factoryAdapter
     */
    public FactoryAdapter getFactoryAdapter() {
		return factoryAdapter;
	}

	/**
	 * Set: factoryAdapter.
	 *
	 * @param factoryAdapter the factory adapter
	 */
	public void setFactoryAdapter(FactoryAdapter factoryAdapter) {
		this.factoryAdapter = factoryAdapter;
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.moedassistema.IMoedasSistemaService#detalharMoedaSistema(br.com.bradesco.web.pgit.service.business.moedassistema.bean.MoedaSistemaEntradaDTO)
	 */
	public MoedaSistemaSaidaDTO detalharMoedaSistema(MoedaSistemaEntradaDTO moedaSistemaEntradaDTO) {
		MoedaSistemaSaidaDTO moedasSistemaSaidaDTO = new MoedaSistemaSaidaDTO();
		DetalharMoedasSistemaRequest detalharMoedaSistemaRequest = new DetalharMoedasSistemaRequest();
		DetalharMoedasSistemaResponse detalharMoedaSistemaResponse = new DetalharMoedasSistemaResponse();
		
		detalharMoedaSistemaRequest.setCdIndicadorEconomicoMoeda(moedaSistemaEntradaDTO.getCdIndicadorEconomicoMoeda());
		detalharMoedaSistemaRequest.setCdTipoLayoutArquivo(moedaSistemaEntradaDTO.getCdTipoLayoutArquivo());
		detalharMoedaSistemaRequest.setCdIndicadorEconomicoLayout(moedaSistemaEntradaDTO.getCdIndicadorEconomicoLayout());
			
		detalharMoedaSistemaResponse = getFactoryAdapter().getDetalharMoedasSistemaPDCAdapter().invokeProcess(detalharMoedaSistemaRequest);
			
		moedasSistemaSaidaDTO.setCodMensagem(detalharMoedaSistemaResponse.getCodMensagem());
		moedasSistemaSaidaDTO.setMensagem(detalharMoedaSistemaResponse.getMensagem());
		
		moedasSistemaSaidaDTO.setCdTipoLayoutArquivo(detalharMoedaSistemaResponse.getOcorrencias(0).getCdTipoLayoutArquivo());
		moedasSistemaSaidaDTO.setDsTipoLayoutArquivo(detalharMoedaSistemaResponse.getOcorrencias(0).getDsTipoLayoutArquivo());
		moedasSistemaSaidaDTO.setCdIndicadorEconomicoLayout(detalharMoedaSistemaResponse.getOcorrencias(0).getCdIndicadorEconomicoLayout());
		moedasSistemaSaidaDTO.setDsIndicadorEconomicoLayout(detalharMoedaSistemaResponse.getOcorrencias(0).getDsIndicadorEconomicoLayout());
		moedasSistemaSaidaDTO.setCdIndicadorEconomicoMoeda(detalharMoedaSistemaResponse.getOcorrencias(0).getCdIndicadorEconomicoMoeda());
		moedasSistemaSaidaDTO.setDsIndicadorEconomicoMoeda(detalharMoedaSistemaResponse.getOcorrencias(0).getDsIndicadorEconomicoMoeda());
		
		moedasSistemaSaidaDTO.setCdCanalInclusao(detalharMoedaSistemaResponse.getOcorrencias(0).getCdCanalInclusao());
		moedasSistemaSaidaDTO.setDsCanalInclusao(detalharMoedaSistemaResponse.getOcorrencias(0).getDsCanalInclusao());
		moedasSistemaSaidaDTO.setCdAutenticacaoSegurancaInclusao(detalharMoedaSistemaResponse.getOcorrencias(0).getCdAutenSegregacaoInclusao());
		moedasSistemaSaidaDTO.setHrInclusaoRegistro(detalharMoedaSistemaResponse.getOcorrencias(0).getHrInclusaoRegistro());
		moedasSistemaSaidaDTO.setNrOperacaoFluxoInclusao(detalharMoedaSistemaResponse.getOcorrencias(0).getNmOperacaoFluxoInclusao());
		
		moedasSistemaSaidaDTO.setCdCanalManutencao(detalharMoedaSistemaResponse.getOcorrencias(0).getCdCanalManutencao());
		moedasSistemaSaidaDTO.setDsCanalManutencao(detalharMoedaSistemaResponse.getOcorrencias(0).getDsCanalManutencao());
		moedasSistemaSaidaDTO.setCdAutenticacaoSegurancaManutencao(detalharMoedaSistemaResponse.getOcorrencias(0).getCdAutenSegregacaoManutencao());
		moedasSistemaSaidaDTO.setHrManutencaoRegistro(detalharMoedaSistemaResponse.getOcorrencias(0).getHrManutencaoRegistro());
		moedasSistemaSaidaDTO.setNrOperacaoFluxoManutencao(detalharMoedaSistemaResponse.getOcorrencias(0).getNmOperacaoFluxoManutencao());

		return moedasSistemaSaidaDTO;	

	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.moedassistema.IMoedasSistemaService#excluirMoedaSistema(br.com.bradesco.web.pgit.service.business.moedassistema.bean.MoedaSistemaEntradaDTO)
	 */
	public MoedaSistemaSaidaDTO excluirMoedaSistema(MoedaSistemaEntradaDTO moedaSistemaEntradaDTO) {
		MoedaSistemaSaidaDTO moedaSistemaSaidaDTO = new MoedaSistemaSaidaDTO();
		ExcluirMoedasSistemaRequest excluirMoedasSistemaRequest = new ExcluirMoedasSistemaRequest();
		ExcluirMoedasSistemaResponse excluirMoedasSistemaResponse = new ExcluirMoedasSistemaResponse();
		
		excluirMoedasSistemaRequest.setCdIndicadorEconomicoMoeda(moedaSistemaEntradaDTO.getCdIndicadorEconomicoMoeda());
		excluirMoedasSistemaRequest.setCdIndicadorEconomicoLayout(moedaSistemaEntradaDTO.getCdIndicadorEconomicoLayout());
		excluirMoedasSistemaRequest.setCdTipoLayoutArquivo(moedaSistemaEntradaDTO.getCdTipoLayoutArquivo());

		excluirMoedasSistemaResponse = getFactoryAdapter().getExcluirMoedasSistemaPDCAdapter().invokeProcess(excluirMoedasSistemaRequest);

		moedaSistemaSaidaDTO.setCodMensagem(excluirMoedasSistemaResponse.getCodMensagem());
		moedaSistemaSaidaDTO.setMensagem(excluirMoedasSistemaResponse.getMensagem());
			
		return moedaSistemaSaidaDTO;
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.moedassistema.IMoedasSistemaService#incluirMoedaSistema(br.com.bradesco.web.pgit.service.business.moedassistema.bean.MoedaSistemaEntradaDTO)
	 */
	public MoedaSistemaSaidaDTO incluirMoedaSistema(MoedaSistemaEntradaDTO moedaSistemaEntradaDTO) {
		
		MoedaSistemaSaidaDTO moedaSistemaSaidaDTO = new MoedaSistemaSaidaDTO();
		IncluirMoedasSistemaRequest incluirMoedasSistemaRequest = new IncluirMoedasSistemaRequest();
		IncluirMoedasSistemaResponse incluirMoedasSistemaResponse = new IncluirMoedasSistemaResponse();
		
		incluirMoedasSistemaRequest.setCdIndicadorEconomicoMoeda(moedaSistemaEntradaDTO.getCdIndicadorEconomicoMoeda());
		incluirMoedasSistemaRequest.setCdIndicadorEconomicoLayout(moedaSistemaEntradaDTO.getCdIndicadorEconomicoLayout());
		incluirMoedasSistemaRequest.setCdTipoLayoutArquivo(moedaSistemaEntradaDTO.getCdTipoLayoutArquivo());
		
		incluirMoedasSistemaResponse = getFactoryAdapter().getIncluirMoedasSistemaPDCAdapter().invokeProcess(incluirMoedasSistemaRequest);

		moedaSistemaSaidaDTO.setCodMensagem(incluirMoedasSistemaResponse.getCodMensagem());
		moedaSistemaSaidaDTO.setMensagem(incluirMoedasSistemaResponse.getMensagem());
	
		return moedaSistemaSaidaDTO;
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.moedassistema.IMoedasSistemaService#listarMoedaSistema(br.com.bradesco.web.pgit.service.business.moedassistema.bean.MoedaSistemaEntradaDTO)
	 */
	public List<MoedaSistemaSaidaDTO> listarMoedaSistema(MoedaSistemaEntradaDTO moedaSistemaEntradaDTO) {
		List<MoedaSistemaSaidaDTO> listaMoedaSistema = new ArrayList<MoedaSistemaSaidaDTO>();
		ListarMoedasSistemaRequest listarMoedaSistemaRequest = new ListarMoedasSistemaRequest();
		ListarMoedasSistemaResponse listarMoedaSistemaResponse = new ListarMoedasSistemaResponse();
		
		listarMoedaSistemaRequest.setCdIndicadorEconomicoMoeda(moedaSistemaEntradaDTO.getCdIndicadorEconomicoMoeda());
		listarMoedaSistemaRequest.setCdTipoLayoutArquivo(moedaSistemaEntradaDTO.getCdTipoLayoutArquivo());
		listarMoedaSistemaRequest.setQtConsultas(IMoedasSisServiceConstants.QTDE_CONSULTA_LISTAR);
	
		listarMoedaSistemaResponse = getFactoryAdapter().getListarMoedasSistemaPDCAdapter().invokeProcess(listarMoedaSistemaRequest);

		MoedaSistemaSaidaDTO moedaSistemaSaidaDTO;
		for (int i=0; i<listarMoedaSistemaResponse.getOcorrenciasCount();i++){
		
			moedaSistemaSaidaDTO = new MoedaSistemaSaidaDTO();
			moedaSistemaSaidaDTO.setCodMensagem(listarMoedaSistemaResponse.getCodMensagem());
			moedaSistemaSaidaDTO.setMensagem(listarMoedaSistemaResponse.getMensagem());
			moedaSistemaSaidaDTO.setCdIndicadorEconomicoMoeda(listarMoedaSistemaResponse.getOcorrencias(i).getCdIndicadorEconomicoMoeda());
			moedaSistemaSaidaDTO.setDsIndicadorEconomicoMoeda(listarMoedaSistemaResponse.getOcorrencias(i).getDsEconomicoMoeda());
			moedaSistemaSaidaDTO.setCdTipoLayoutArquivo(listarMoedaSistemaResponse.getOcorrencias(i).getCdTipoLayoutArquivo());
			moedaSistemaSaidaDTO.setDsTipoLayoutArquivo(listarMoedaSistemaResponse.getOcorrencias(i).getDsLayoutArquivo());
			moedaSistemaSaidaDTO.setCdIndicadorEconomicoLayout(listarMoedaSistemaResponse.getOcorrencias(i).getCdIndicadorEconomicoLayout());
			moedaSistemaSaidaDTO.setDsIndicadorEconomicoLayout(listarMoedaSistemaResponse.getOcorrencias(i).getCdIndicadorEconomicoLayout());
			listaMoedaSistema.add(moedaSistemaSaidaDTO);
			
		}
		
		return listaMoedaSistema;
	}

	
}

