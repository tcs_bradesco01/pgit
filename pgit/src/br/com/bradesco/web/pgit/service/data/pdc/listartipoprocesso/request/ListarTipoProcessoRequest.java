/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listartipoprocesso.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ListarTipoProcessoRequest.
 * 
 * @version $Revision$ $Date$
 */
public class ListarTipoProcessoRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _nrOcorrencias
     */
    private int _nrOcorrencias = 0;

    /**
     * keeps track of state for field: _nrOcorrencias
     */
    private boolean _has_nrOcorrencias;

    /**
     * Field _cdTipoProcsSistema
     */
    private int _cdTipoProcsSistema = 0;

    /**
     * keeps track of state for field: _cdTipoProcsSistema
     */
    private boolean _has_cdTipoProcsSistema;


      //----------------/
     //- Constructors -/
    //----------------/

    public ListarTipoProcessoRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listartipoprocesso.request.ListarTipoProcessoRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdTipoProcsSistema
     * 
     */
    public void deleteCdTipoProcsSistema()
    {
        this._has_cdTipoProcsSistema= false;
    } //-- void deleteCdTipoProcsSistema() 

    /**
     * Method deleteNrOcorrencias
     * 
     */
    public void deleteNrOcorrencias()
    {
        this._has_nrOcorrencias= false;
    } //-- void deleteNrOcorrencias() 

    /**
     * Returns the value of field 'cdTipoProcsSistema'.
     * 
     * @return int
     * @return the value of field 'cdTipoProcsSistema'.
     */
    public int getCdTipoProcsSistema()
    {
        return this._cdTipoProcsSistema;
    } //-- int getCdTipoProcsSistema() 

    /**
     * Returns the value of field 'nrOcorrencias'.
     * 
     * @return int
     * @return the value of field 'nrOcorrencias'.
     */
    public int getNrOcorrencias()
    {
        return this._nrOcorrencias;
    } //-- int getNrOcorrencias() 

    /**
     * Method hasCdTipoProcsSistema
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoProcsSistema()
    {
        return this._has_cdTipoProcsSistema;
    } //-- boolean hasCdTipoProcsSistema() 

    /**
     * Method hasNrOcorrencias
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrOcorrencias()
    {
        return this._has_nrOcorrencias;
    } //-- boolean hasNrOcorrencias() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdTipoProcsSistema'.
     * 
     * @param cdTipoProcsSistema the value of field
     * 'cdTipoProcsSistema'.
     */
    public void setCdTipoProcsSistema(int cdTipoProcsSistema)
    {
        this._cdTipoProcsSistema = cdTipoProcsSistema;
        this._has_cdTipoProcsSistema = true;
    } //-- void setCdTipoProcsSistema(int) 

    /**
     * Sets the value of field 'nrOcorrencias'.
     * 
     * @param nrOcorrencias the value of field 'nrOcorrencias'.
     */
    public void setNrOcorrencias(int nrOcorrencias)
    {
        this._nrOcorrencias = nrOcorrencias;
        this._has_nrOcorrencias = true;
    } //-- void setNrOcorrencias(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ListarTipoProcessoRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listartipoprocesso.request.ListarTipoProcessoRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listartipoprocesso.request.ListarTipoProcessoRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listartipoprocesso.request.ListarTipoProcessoRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listartipoprocesso.request.ListarTipoProcessoRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
