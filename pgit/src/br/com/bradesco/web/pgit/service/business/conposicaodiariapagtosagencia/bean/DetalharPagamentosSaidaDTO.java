/*
 * Nome: br.com.bradesco.web.pgit.service.business.conposicaodiariapagtosagencia.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.conposicaodiariapagtosagencia.bean;

import java.math.BigDecimal;

/**
 * Nome: DetalharPagamentosSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class DetalharPagamentosSaidaDTO {
    
    /** Atributo codMensagem. */
    private String codMensagem;

    /** Atributo mensagem. */
    private String mensagem;

    /** Atributo numeroConsultas. */
    private Integer numeroConsultas;

    /** Atributo cdCorpoCnpjCpf. */
    private Long cdCorpoCnpjCpf;

    /** Atributo cdFilialCnpjCpf. */
    private Integer cdFilialCnpjCpf;

    /** Atributo cdDigitoCnpjCpf. */
    private Integer cdDigitoCnpjCpf;

    /** Atributo cdPessoaJuridicaContrato. */
    private Long cdPessoaJuridicaContrato;

    /** Atributo dsRazaoSocial. */
    private String dsRazaoSocial;

    /** Atributo cdEmpresa. */
    private Long cdEmpresa;

    /** Atributo dsEmpresa. */
    private String dsEmpresa;

    /** Atributo cdTipoContratoNegocio. */
    private Integer cdTipoContratoNegocio;

    /** Atributo cdNumeroContrato. */
    private Long cdNumeroContrato;

    /** Atributo cdProdutoServicoOperacao. */
    private Integer cdProdutoServicoOperacao;

    /** Atributo dsResumoProdutoServico. */
    private String dsResumoProdutoServico;

    /** Atributo cdProdutoServicoRelacionado. */
    private Integer cdProdutoServicoRelacionado;

    /** Atributo dsOperacaoProdutoServico. */
    private String dsOperacaoProdutoServico;

    /** Atributo cdControlePagamento. */
    private String cdControlePagamento;

    /** Atributo dtCreditoPagamento. */
    private String dtCreditoPagamento;

    /** Atributo vlEfetivacaoPagamentoCliente. */
    private BigDecimal vlEfetivacaoPagamentoCliente;

    /** Atributo cdInscricaoFavorecidoCliente. */
    private Long cdInscricaoFavorecidoCliente;

    /** Atributo dsAbreviacaoFavorecidoCliente. */
    private String dsAbreviacaoFavorecidoCliente;

    /** Atributo cdBancoDebito. */
    private Integer cdBancoDebito;

    /** Atributo cdAgenciaDebito. */
    private Integer cdAgenciaDebito;

    /** Atributo cdDigitoAgenciaDebito. */
    private Integer cdDigitoAgenciaDebito;

    /** Atributo cdContaDebito. */
    private Long cdContaDebito;

    /** Atributo cdDigitoContaDebito. */
    private String cdDigitoContaDebito;

    /** Atributo cdBancoCredito. */
    private Integer cdBancoCredito;

    /** Atributo cdAgenciaCredito. */
    private Integer cdAgenciaCredito;

    /** Atributo cdDigitoAgenciaCredito. */
    private Integer cdDigitoAgenciaCredito;

    /** Atributo cdContaCredito. */
    private Long cdContaCredito;

    /** Atributo cdDigitoContaCredito. */
    private String cdDigitoContaCredito;

    /** Atributo cdSituacaoOperacaoPagamento. */
    private Integer cdSituacaoOperacaoPagamento;

    /** Atributo dsSituacaoOperacaoPagamento. */
    private String dsSituacaoOperacaoPagamento;

    /** Atributo cdMotivoSituacaoPagamento. */
    private Integer cdMotivoSituacaoPagamento;

    /** Atributo dsMotivoSituacaoPagamento. */
    private String dsMotivoSituacaoPagamento;

    /** Atributo cdTipoCanal. */
    private Integer cdTipoCanal;

    /** Atributo cdTipoTela. */
    private Integer cdTipoTela;

    /** Atributo cpfCnpjFormatado. */
    private String cpfCnpjFormatado;

    /** Atributo beneficiarioFavorecido. */
    private String beneficiarioFavorecido;

    /** Atributo dsEfetivacaoPagamento. */
    private String dsEfetivacaoPagamento;

    /** Atributo contaDebitoFormatada. */
    private String contaDebitoFormatada;

    /** Atributo bancoCreditoFormatado. */
    private String bancoCreditoFormatado;

    /** Atributo agenciaCreditoFormatada. */
    private String agenciaCreditoFormatada;

    /** Atributo contaCreditoFormatada. */
    private String contaCreditoFormatada;

    /** Atributo contaDigitoCreditoFormatada. */
    private String contaDigitoCreditoFormatada;

    /** Atributo dsIndicadorPagamento. */
    private String dsIndicadorPagamento;

	private String cdIspbPagtoDestino;
	
	private String contaPagtoDestino;
    /**
     * Get: cdAgenciaCredito.
     *
     * @return cdAgenciaCredito
     */
    public Integer getCdAgenciaCredito() {
	return cdAgenciaCredito;
    }

    /**
     * Set: cdAgenciaCredito.
     *
     * @param cdAgenciaCredito the cd agencia credito
     */
    public void setCdAgenciaCredito(Integer cdAgenciaCredito) {
	this.cdAgenciaCredito = cdAgenciaCredito;
    }

    /**
     * Get: cdAgenciaDebito.
     *
     * @return cdAgenciaDebito
     */
    public Integer getCdAgenciaDebito() {
	return cdAgenciaDebito;
    }

    /**
     * Set: cdAgenciaDebito.
     *
     * @param cdAgenciaDebito the cd agencia debito
     */
    public void setCdAgenciaDebito(Integer cdAgenciaDebito) {
	this.cdAgenciaDebito = cdAgenciaDebito;
    }

    /**
     * Get: cdBancoCredito.
     *
     * @return cdBancoCredito
     */
    public Integer getCdBancoCredito() {
	return cdBancoCredito;
    }

    /**
     * Set: cdBancoCredito.
     *
     * @param cdBancoCredito the cd banco credito
     */
    public void setCdBancoCredito(Integer cdBancoCredito) {
	this.cdBancoCredito = cdBancoCredito;
    }

    /**
     * Get: cdBancoDebito.
     *
     * @return cdBancoDebito
     */
    public Integer getCdBancoDebito() {
	return cdBancoDebito;
    }

    /**
     * Set: cdBancoDebito.
     *
     * @param cdBancoDebito the cd banco debito
     */
    public void setCdBancoDebito(Integer cdBancoDebito) {
	this.cdBancoDebito = cdBancoDebito;
    }

    /**
     * Get: cdContaCredito.
     *
     * @return cdContaCredito
     */
    public Long getCdContaCredito() {
	return cdContaCredito;
    }

    /**
     * Set: cdContaCredito.
     *
     * @param cdContaCredito the cd conta credito
     */
    public void setCdContaCredito(Long cdContaCredito) {
	this.cdContaCredito = cdContaCredito;
    }

    /**
     * Get: cdContaDebito.
     *
     * @return cdContaDebito
     */
    public Long getCdContaDebito() {
	return cdContaDebito;
    }

    /**
     * Set: cdContaDebito.
     *
     * @param cdContaDebito the cd conta debito
     */
    public void setCdContaDebito(Long cdContaDebito) {
	this.cdContaDebito = cdContaDebito;
    }

    /**
     * Get: cdControlePagamento.
     *
     * @return cdControlePagamento
     */
    public String getCdControlePagamento() {
	return cdControlePagamento;
    }

    /**
     * Set: cdControlePagamento.
     *
     * @param cdControlePagamento the cd controle pagamento
     */
    public void setCdControlePagamento(String cdControlePagamento) {
	this.cdControlePagamento = cdControlePagamento;
    }

    /**
     * Get: cdCorpoCnpjCpf.
     *
     * @return cdCorpoCnpjCpf
     */
    public Long getCdCorpoCnpjCpf() {
	return cdCorpoCnpjCpf;
    }

    /**
     * Set: cdCorpoCnpjCpf.
     *
     * @param cdCorpoCnpjCpf the cd corpo cnpj cpf
     */
    public void setCdCorpoCnpjCpf(Long cdCorpoCnpjCpf) {
	this.cdCorpoCnpjCpf = cdCorpoCnpjCpf;
    }

    /**
     * Get: cdDigitoAgenciaCredito.
     *
     * @return cdDigitoAgenciaCredito
     */
    public Integer getCdDigitoAgenciaCredito() {
	return cdDigitoAgenciaCredito;
    }

    /**
     * Set: cdDigitoAgenciaCredito.
     *
     * @param cdDigitoAgenciaCredito the cd digito agencia credito
     */
    public void setCdDigitoAgenciaCredito(Integer cdDigitoAgenciaCredito) {
	this.cdDigitoAgenciaCredito = cdDigitoAgenciaCredito;
    }

    /**
     * Get: cdDigitoAgenciaDebito.
     *
     * @return cdDigitoAgenciaDebito
     */
    public Integer getCdDigitoAgenciaDebito() {
	return cdDigitoAgenciaDebito;
    }

    /**
     * Set: cdDigitoAgenciaDebito.
     *
     * @param cdDigitoAgenciaDebito the cd digito agencia debito
     */
    public void setCdDigitoAgenciaDebito(Integer cdDigitoAgenciaDebito) {
	this.cdDigitoAgenciaDebito = cdDigitoAgenciaDebito;
    }

    /**
     * Get: cdDigitoCnpjCpf.
     *
     * @return cdDigitoCnpjCpf
     */
    public Integer getCdDigitoCnpjCpf() {
	return cdDigitoCnpjCpf;
    }

    /**
     * Set: cdDigitoCnpjCpf.
     *
     * @param cdDigitoCnpjCpf the cd digito cnpj cpf
     */
    public void setCdDigitoCnpjCpf(Integer cdDigitoCnpjCpf) {
	this.cdDigitoCnpjCpf = cdDigitoCnpjCpf;
    }

    /**
     * Get: cdDigitoContaCredito.
     *
     * @return cdDigitoContaCredito
     */
    public String getCdDigitoContaCredito() {
	return cdDigitoContaCredito;
    }

    /**
     * Set: cdDigitoContaCredito.
     *
     * @param cdDigitoContaCredito the cd digito conta credito
     */
    public void setCdDigitoContaCredito(String cdDigitoContaCredito) {
	this.cdDigitoContaCredito = cdDigitoContaCredito;
    }

    /**
     * Get: cdDigitoContaDebito.
     *
     * @return cdDigitoContaDebito
     */
    public String getCdDigitoContaDebito() {
	return cdDigitoContaDebito;
    }

    /**
     * Set: cdDigitoContaDebito.
     *
     * @param cdDigitoContaDebito the cd digito conta debito
     */
    public void setCdDigitoContaDebito(String cdDigitoContaDebito) {
	this.cdDigitoContaDebito = cdDigitoContaDebito;
    }

    /**
     * Get: cdEmpresa.
     *
     * @return cdEmpresa
     */
    public Long getCdEmpresa() {
	return cdEmpresa;
    }

    /**
     * Set: cdEmpresa.
     *
     * @param cdEmpresa the cd empresa
     */
    public void setCdEmpresa(Long cdEmpresa) {
	this.cdEmpresa = cdEmpresa;
    }

    /**
     * Get: cdFilialCnpjCpf.
     *
     * @return cdFilialCnpjCpf
     */
    public Integer getCdFilialCnpjCpf() {
	return cdFilialCnpjCpf;
    }

    /**
     * Set: cdFilialCnpjCpf.
     *
     * @param cdFilialCnpjCpf the cd filial cnpj cpf
     */
    public void setCdFilialCnpjCpf(Integer cdFilialCnpjCpf) {
	this.cdFilialCnpjCpf = cdFilialCnpjCpf;
    }

    /**
     * Get: cdInscricaoFavorecidoCliente.
     *
     * @return cdInscricaoFavorecidoCliente
     */
    public Long getCdInscricaoFavorecidoCliente() {
	return cdInscricaoFavorecidoCliente;
    }

    /**
     * Set: cdInscricaoFavorecidoCliente.
     *
     * @param cdInscricaoFavorecidoCliente the cd inscricao favorecido cliente
     */
    public void setCdInscricaoFavorecidoCliente(Long cdInscricaoFavorecidoCliente) {
	this.cdInscricaoFavorecidoCliente = cdInscricaoFavorecidoCliente;
    }

    /**
     * Get: cdMotivoSituacaoPagamento.
     *
     * @return cdMotivoSituacaoPagamento
     */
    public Integer getCdMotivoSituacaoPagamento() {
	return cdMotivoSituacaoPagamento;
    }

    /**
     * Set: cdMotivoSituacaoPagamento.
     *
     * @param cdMotivoSituacaoPagamento the cd motivo situacao pagamento
     */
    public void setCdMotivoSituacaoPagamento(Integer cdMotivoSituacaoPagamento) {
	this.cdMotivoSituacaoPagamento = cdMotivoSituacaoPagamento;
    }

    /**
     * Get: cdNumeroContrato.
     *
     * @return cdNumeroContrato
     */
    public Long getCdNumeroContrato() {
	return cdNumeroContrato;
    }

    /**
     * Set: cdNumeroContrato.
     *
     * @param cdNumeroContrato the cd numero contrato
     */
    public void setCdNumeroContrato(Long cdNumeroContrato) {
	this.cdNumeroContrato = cdNumeroContrato;
    }

    /**
     * Get: cdPessoaJuridicaContrato.
     *
     * @return cdPessoaJuridicaContrato
     */
    public Long getCdPessoaJuridicaContrato() {
	return cdPessoaJuridicaContrato;
    }

    /**
     * Set: cdPessoaJuridicaContrato.
     *
     * @param cdPessoaJuridicaContrato the cd pessoa juridica contrato
     */
    public void setCdPessoaJuridicaContrato(Long cdPessoaJuridicaContrato) {
	this.cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
    }

    /**
     * Get: cdProdutoServicoOperacao.
     *
     * @return cdProdutoServicoOperacao
     */
    public Integer getCdProdutoServicoOperacao() {
	return cdProdutoServicoOperacao;
    }

    /**
     * Set: cdProdutoServicoOperacao.
     *
     * @param cdProdutoServicoOperacao the cd produto servico operacao
     */
    public void setCdProdutoServicoOperacao(Integer cdProdutoServicoOperacao) {
	this.cdProdutoServicoOperacao = cdProdutoServicoOperacao;
    }

    /**
     * Get: cdProdutoServicoRelacionado.
     *
     * @return cdProdutoServicoRelacionado
     */
    public Integer getCdProdutoServicoRelacionado() {
	return cdProdutoServicoRelacionado;
    }

    /**
     * Set: cdProdutoServicoRelacionado.
     *
     * @param cdProdutoServicoRelacionado the cd produto servico relacionado
     */
    public void setCdProdutoServicoRelacionado(Integer cdProdutoServicoRelacionado) {
	this.cdProdutoServicoRelacionado = cdProdutoServicoRelacionado;
    }

    /**
     * Get: cdSituacaoOperacaoPagamento.
     *
     * @return cdSituacaoOperacaoPagamento
     */
    public Integer getCdSituacaoOperacaoPagamento() {
	return cdSituacaoOperacaoPagamento;
    }

    /**
     * Set: cdSituacaoOperacaoPagamento.
     *
     * @param cdSituacaoOperacaoPagamento the cd situacao operacao pagamento
     */
    public void setCdSituacaoOperacaoPagamento(Integer cdSituacaoOperacaoPagamento) {
	this.cdSituacaoOperacaoPagamento = cdSituacaoOperacaoPagamento;
    }

    /**
     * Get: cdTipoCanal.
     *
     * @return cdTipoCanal
     */
    public Integer getCdTipoCanal() {
	return cdTipoCanal;
    }

    /**
     * Set: cdTipoCanal.
     *
     * @param cdTipoCanal the cd tipo canal
     */
    public void setCdTipoCanal(Integer cdTipoCanal) {
	this.cdTipoCanal = cdTipoCanal;
    }

    /**
     * Get: cdTipoContratoNegocio.
     *
     * @return cdTipoContratoNegocio
     */
    public Integer getCdTipoContratoNegocio() {
	return cdTipoContratoNegocio;
    }

    /**
     * Set: cdTipoContratoNegocio.
     *
     * @param cdTipoContratoNegocio the cd tipo contrato negocio
     */
    public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
	this.cdTipoContratoNegocio = cdTipoContratoNegocio;
    }

    /**
     * Get: cdTipoTela.
     *
     * @return cdTipoTela
     */
    public Integer getCdTipoTela() {
	return cdTipoTela;
    }

    /**
     * Set: cdTipoTela.
     *
     * @param cdTipoTela the cd tipo tela
     */
    public void setCdTipoTela(Integer cdTipoTela) {
	this.cdTipoTela = cdTipoTela;
    }

    /**
     * Get: codMensagem.
     *
     * @return codMensagem
     */
    public String getCodMensagem() {
	return codMensagem;
    }

    /**
     * Set: codMensagem.
     *
     * @param codMensagem the cod mensagem
     */
    public void setCodMensagem(String codMensagem) {
	this.codMensagem = codMensagem;
    }

    /**
     * Get: dsAbreviacaoFavorecidoCliente.
     *
     * @return dsAbreviacaoFavorecidoCliente
     */
    public String getDsAbreviacaoFavorecidoCliente() {
	return dsAbreviacaoFavorecidoCliente;
    }

    /**
     * Set: dsAbreviacaoFavorecidoCliente.
     *
     * @param dsAbreviacaoFavorecidoCliente the ds abreviacao favorecido cliente
     */
    public void setDsAbreviacaoFavorecidoCliente(String dsAbreviacaoFavorecidoCliente) {
	this.dsAbreviacaoFavorecidoCliente = dsAbreviacaoFavorecidoCliente;
    }

    /**
     * Get: dsEmpresa.
     *
     * @return dsEmpresa
     */
    public String getDsEmpresa() {
	return dsEmpresa;
    }

    /**
     * Set: dsEmpresa.
     *
     * @param dsEmpresa the ds empresa
     */
    public void setDsEmpresa(String dsEmpresa) {
	this.dsEmpresa = dsEmpresa;
    }

    /**
     * Get: dsMotivoSituacaoPagamento.
     *
     * @return dsMotivoSituacaoPagamento
     */
    public String getDsMotivoSituacaoPagamento() {
	return dsMotivoSituacaoPagamento;
    }

    /**
     * Set: dsMotivoSituacaoPagamento.
     *
     * @param dsMotivoSituacaoPagamento the ds motivo situacao pagamento
     */
    public void setDsMotivoSituacaoPagamento(String dsMotivoSituacaoPagamento) {
	this.dsMotivoSituacaoPagamento = dsMotivoSituacaoPagamento;
    }

    /**
     * Get: dsOperacaoProdutoServico.
     *
     * @return dsOperacaoProdutoServico
     */
    public String getDsOperacaoProdutoServico() {
	return dsOperacaoProdutoServico;
    }

    /**
     * Set: dsOperacaoProdutoServico.
     *
     * @param dsOperacaoProdutoServico the ds operacao produto servico
     */
    public void setDsOperacaoProdutoServico(String dsOperacaoProdutoServico) {
	this.dsOperacaoProdutoServico = dsOperacaoProdutoServico;
    }

    /**
     * Get: dsRazaoSocial.
     *
     * @return dsRazaoSocial
     */
    public String getDsRazaoSocial() {
	return dsRazaoSocial;
    }

    /**
     * Set: dsRazaoSocial.
     *
     * @param dsRazaoSocial the ds razao social
     */
    public void setDsRazaoSocial(String dsRazaoSocial) {
	this.dsRazaoSocial = dsRazaoSocial;
    }

    /**
     * Get: dsResumoProdutoServico.
     *
     * @return dsResumoProdutoServico
     */
    public String getDsResumoProdutoServico() {
	return dsResumoProdutoServico;
    }

    /**
     * Set: dsResumoProdutoServico.
     *
     * @param dsResumoProdutoServico the ds resumo produto servico
     */
    public void setDsResumoProdutoServico(String dsResumoProdutoServico) {
	this.dsResumoProdutoServico = dsResumoProdutoServico;
    }

    /**
     * Get: dsSituacaoOperacaoPagamento.
     *
     * @return dsSituacaoOperacaoPagamento
     */
    public String getDsSituacaoOperacaoPagamento() {
	return dsSituacaoOperacaoPagamento;
    }

    /**
     * Set: dsSituacaoOperacaoPagamento.
     *
     * @param dsSituacaoOperacaoPagamento the ds situacao operacao pagamento
     */
    public void setDsSituacaoOperacaoPagamento(String dsSituacaoOperacaoPagamento) {
	this.dsSituacaoOperacaoPagamento = dsSituacaoOperacaoPagamento;
    }

    /**
     * Get: dtCreditoPagamento.
     *
     * @return dtCreditoPagamento
     */
    public String getDtCreditoPagamento() {
	return dtCreditoPagamento;
    }

    /**
     * Set: dtCreditoPagamento.
     *
     * @param dtCreditoPagamento the dt credito pagamento
     */
    public void setDtCreditoPagamento(String dtCreditoPagamento) {
	this.dtCreditoPagamento = dtCreditoPagamento;
    }

    /**
     * Get: mensagem.
     *
     * @return mensagem
     */
    public String getMensagem() {
	return mensagem;
    }

    /**
     * Set: mensagem.
     *
     * @param mensagem the mensagem
     */
    public void setMensagem(String mensagem) {
	this.mensagem = mensagem;
    }

    /**
     * Get: numeroConsultas.
     *
     * @return numeroConsultas
     */
    public Integer getNumeroConsultas() {
	return numeroConsultas;
    }

    /**
     * Set: numeroConsultas.
     *
     * @param numeroConsultas the numero consultas
     */
    public void setNumeroConsultas(Integer numeroConsultas) {
	this.numeroConsultas = numeroConsultas;
    }

    /**
     * Get: vlEfetivacaoPagamentoCliente.
     *
     * @return vlEfetivacaoPagamentoCliente
     */
    public BigDecimal getVlEfetivacaoPagamentoCliente() {
	return vlEfetivacaoPagamentoCliente;
    }

    /**
     * Set: vlEfetivacaoPagamentoCliente.
     *
     * @param vlEfetivacaoPagamentoCliente the vl efetivacao pagamento cliente
     */
    public void setVlEfetivacaoPagamentoCliente(BigDecimal vlEfetivacaoPagamentoCliente) {
	this.vlEfetivacaoPagamentoCliente = vlEfetivacaoPagamentoCliente;
    }

    /**
     * Get: cpfCnpjFormatado.
     *
     * @return cpfCnpjFormatado
     */
    public String getCpfCnpjFormatado() {
	return cpfCnpjFormatado;
    }

    /**
     * Set: cpfCnpjFormatado.
     *
     * @param cpfCnpjFormatado the cpf cnpj formatado
     */
    public void setCpfCnpjFormatado(String cpfCnpjFormatado) {
	this.cpfCnpjFormatado = cpfCnpjFormatado;
    }

    /**
     * Get: beneficiarioFavorecido.
     *
     * @return beneficiarioFavorecido
     */
    public String getBeneficiarioFavorecido() {
	return beneficiarioFavorecido;
    }

    /**
     * Set: beneficiarioFavorecido.
     *
     * @param beneficiarioFavorecido the beneficiario favorecido
     */
    public void setBeneficiarioFavorecido(String beneficiarioFavorecido) {
	this.beneficiarioFavorecido = beneficiarioFavorecido;
    }

    /**
     * Get: contaCreditoFormatada.
     *
     * @return contaCreditoFormatada
     */
    public String getContaCreditoFormatada() {
	return contaCreditoFormatada;
    }

    /**
     * Set: contaCreditoFormatada.
     *
     * @param contaCreditoFormatada the conta credito formatada
     */
    public void setContaCreditoFormatada(String contaCreditoFormatada) {
	this.contaCreditoFormatada = contaCreditoFormatada;
    }

    /**
     * Get: agenciaCreditoFormatada.
     *
     * @return agenciaCreditoFormatada
     */
    public String getAgenciaCreditoFormatada() {
	return agenciaCreditoFormatada;
    }

    /**
     * Set: agenciaCreditoFormatada.
     *
     * @param agenciaCreditoFormatada the agencia credito formatada
     */
    public void setAgenciaCreditoFormatada(String agenciaCreditoFormatada) {
	this.agenciaCreditoFormatada = agenciaCreditoFormatada;
    }

    /**
     * Get: contaDebitoFormatada.
     *
     * @return contaDebitoFormatada
     */
    public String getContaDebitoFormatada() {
	return contaDebitoFormatada;
    }

    /**
     * Set: contaDebitoFormatada.
     *
     * @param contaDebitoFormatada the conta debito formatada
     */
    public void setContaDebitoFormatada(String contaDebitoFormatada) {
	this.contaDebitoFormatada = contaDebitoFormatada;
    }

    /**
     * Get: bancoCreditoFormatado.
     *
     * @return bancoCreditoFormatado
     */
    public String getBancoCreditoFormatado() {
	return bancoCreditoFormatado;
    }

    /**
     * Set: bancoCreditoFormatado.
     *
     * @param bancoCreditoFormatado the banco credito formatado
     */
    public void setBancoCreditoFormatado(String bancoCreditoFormatado) {
	this.bancoCreditoFormatado = bancoCreditoFormatado;
    }

    /**
     * Get: contaDigitoCreditoFormatada.
     *
     * @return contaDigitoCreditoFormatada
     */
    public String getContaDigitoCreditoFormatada() {
	return contaDigitoCreditoFormatada;
    }

    /**
     * Set: contaDigitoCreditoFormatada.
     *
     * @param contaDigitoCreditoFormatada the conta digito credito formatada
     */
    public void setContaDigitoCreditoFormatada(String contaDigitoCreditoFormatada) {
	this.contaDigitoCreditoFormatada = contaDigitoCreditoFormatada;
    }

    /**
     * Get: dsEfetivacaoPagamento.
     *
     * @return dsEfetivacaoPagamento
     */
    public String getDsEfetivacaoPagamento() {
	return dsEfetivacaoPagamento;
    }

    /**
     * Set: dsEfetivacaoPagamento.
     *
     * @param dsEfetivacaoPagamento the ds efetivacao pagamento
     */
    public void setDsEfetivacaoPagamento(String dsEfetivacaoPagamento) {
	this.dsEfetivacaoPagamento = dsEfetivacaoPagamento;
    }

    /**
     * Get: dsIndicadorPagamento.
     *
     * @return dsIndicadorPagamento
     */
    public String getDsIndicadorPagamento() {
	return dsIndicadorPagamento;
    }

    /**
     * Set: dsIndicadorPagamento.
     *
     * @param dsIndicadorPagamento the ds indicador pagamento
     */
    public void setDsIndicadorPagamento(String dsIndicadorPagamento) {
	this.dsIndicadorPagamento = dsIndicadorPagamento;
    }

	public String getCdIspbPagtoDestino() {
		return cdIspbPagtoDestino;
	}

	public void setCdIspbPagtoDestino(String cdIspbPagtoDestino) {
		this.cdIspbPagtoDestino = cdIspbPagtoDestino;
	}

	public String getContaPagtoDestino() {
		return contaPagtoDestino;
	}

	public void setContaPagtoDestino(String contaPagtoDestino) {
		this.contaPagtoDestino = contaPagtoDestino;
	}
}