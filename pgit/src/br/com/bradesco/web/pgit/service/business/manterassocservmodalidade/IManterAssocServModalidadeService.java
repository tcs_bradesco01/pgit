/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/interfaz.ftl,v $
 * $Id: interfaz.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: interfaz.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.manterassocservmodalidade;

import java.util.List;

import br.com.bradesco.web.pgit.service.business.manterassocservmodalidade.bean.DetalharAssocServModalidadeEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterassocservmodalidade.bean.DetalharAssocServModalidadeSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterassocservmodalidade.bean.ExcluirAssocServModalidadeEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterassocservmodalidade.bean.ExcluirAssocServModalidadeSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterassocservmodalidade.bean.IncluirAssocServModalidadeEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterassocservmodalidade.bean.IncluirAssocServModalidadeSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterassocservmodalidade.bean.ListarAssocServModalidadeEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterassocservmodalidade.bean.ListarAssocServModalidadeSaidaDTO;

/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Interface do adaptador: ManterAssocServModalidade
 * </p>
 * 
 * @comment CODIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public interface IManterAssocServModalidadeService {

	/**
	 * Listar assoc serv modalidade.
	 *
	 * @param listarAssocServModalidadeEntradaDTO the listar assoc serv modalidade entrada dto
	 * @return the list< listar assoc serv modalidade saida dt o>
	 */
	List<ListarAssocServModalidadeSaidaDTO> listarAssocServModalidade(ListarAssocServModalidadeEntradaDTO listarAssocServModalidadeEntradaDTO);
	
	/**
	 * Detalhar assoc serv modalidade.
	 *
	 * @param detalharAssocServModalidadeEntradaDTO the detalhar assoc serv modalidade entrada dto
	 * @return the detalhar assoc serv modalidade saida dto
	 */
	DetalharAssocServModalidadeSaidaDTO detalharAssocServModalidade(DetalharAssocServModalidadeEntradaDTO detalharAssocServModalidadeEntradaDTO);
	
	/**
	 * Incluir assoc serv modalidade.
	 *
	 * @param entrada the entrada
	 * @return the incluir assoc serv modalidade saida dto
	 */
	IncluirAssocServModalidadeSaidaDTO incluirAssocServModalidade(IncluirAssocServModalidadeEntradaDTO entrada);
	
	/**
	 * Excluir assoc serv modalidade.
	 *
	 * @param excluirAssocServModalidadeEntradaDTO the excluir assoc serv modalidade entrada dto
	 * @return the excluir assoc serv modalidade saida dto
	 */
	ExcluirAssocServModalidadeSaidaDTO excluirAssocServModalidade(ExcluirAssocServModalidadeEntradaDTO excluirAssocServModalidadeEntradaDTO);
}

