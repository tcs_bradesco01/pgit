/*
 * Nome: br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean;

import java.math.BigDecimal;

import br.com.bradesco.web.pgit.utils.NumberUtils;

/**
 * Nome: DetalharPagtoTEDSaidaDTO
 * <p>
 * Prop�sito:
 * </p>
 * .
 * 
 * @author : todo!
 * @version :
 */
public class DetalharPagtoTEDSaidaDTO {

	/** Atributo codMensagem. */
	private String codMensagem;

	/** Atributo mensagem. */
	private String mensagem;

	/** Atributo cdCamaraCentral. */
	private String cdCamaraCentral;

	/** Atributo cdFinalidadeDoc. */
	private String cdFinalidadeDoc; // campoOculto

	/** Atributo cdFinalidadeTed. */
	private String cdFinalidadeTed;

	/** Atributo cdIdentificacaoTransferencia. */
	private String cdIdentificacaoTransferencia;

	/** Atributo cdIdentificacaoTitular. */
	private String cdIdentificacaoTitular;

	/** Atributo vlDesconto. */
	private BigDecimal vlDesconto;

	/** Atributo vlAbatidoCredito. */
	private BigDecimal vlAbatidoCredito;

	/** Atributo vlMultaCredito. */
	private BigDecimal vlMultaCredito;

	/** Atributo vlMoraJuros. */
	private BigDecimal vlMoraJuros;

	/** Atributo vlOutrasDeducoes. */
	private BigDecimal vlOutrasDeducoes;

	/** Atributo vlOutroAcrescimo. */
	private BigDecimal vlOutroAcrescimo;

	/** Atributo vlIrCredito. */
	private BigDecimal vlIrCredito;

	/** Atributo vlIssCredito. */
	private BigDecimal vlIssCredito;

	/** Atributo vlIofCredito. */
	private BigDecimal vlIofCredito;

	/** Atributo vlInssCredito. */
	private BigDecimal vlInssCredito;

	/** Atributo cdIndicadorModalidadePgit. */
	private Integer cdIndicadorModalidadePgit; // campoOculto

	/** Atributo dsPagamento. */
	private String dsPagamento;

	/** Atributo dsBancoDebito. */
	private String dsBancoDebito;

	/** Atributo dsAgenciaDebito. */
	private String dsAgenciaDebito;

	/** Atributo dsTipoContaDebito. */
	private String dsTipoContaDebito;

	/** Atributo dsContrato. */
	private String dsContrato;

	/** Atributo cdSituacaoContrato. */
	private String cdSituacaoContrato;

	/** Atributo dtAgendamento. */
	private String dtAgendamento;

	/** Atributo vlAgendado. */
	private BigDecimal vlAgendado;

	/** Atributo vlEfetivo. */
	private BigDecimal vlEfetivo;

	/** Atributo cdIndicadorEconomicoMoeda. */
	private int cdIndicadorEconomicoMoeda;

	/** Atributo qtMoeda. */
	private BigDecimal qtMoeda;

	/** Atributo dtVencimento. */
	private String dtVencimento;

	/** Atributo dtDevolucaoEstorno. */
	private String dtDevolucaoEstorno;

	/** Atributo dsMensagemPrimeiraLinha. */
	private String dsMensagemPrimeiraLinha;

	/** Atributo dsMensagemSegundaLinha. */
	private String dsMensagemSegundaLinha;

	/** Atributo dsUsoEmpresa. */
	private String dsUsoEmpresa;

	/** Atributo cdSituacaoOperacaoPagamento. */
	private int cdSituacaoOperacaoPagamento;

	/** Atributo cdMotivoSituacaoPagamento. */
	private int cdMotivoSituacaoPagamento; // campoOculto

	/** Atributo cdListaDebito. */
	private String cdListaDebito;

	/** Atributo cdTipoInscricaoFavorecido. */
	private int cdTipoInscricaoFavorecido;

	/** Atributo nrDocumento. */
	private String nrDocumento;

	/** Atributo cdSerieDocumento. */
	private String cdSerieDocumento;

	/** Atributo cdTipoDocumento. */
	private int cdTipoDocumento;

	/** Atributo dsTipoDocumento. */
	private String dsTipoDocumento;

	/** Atributo vlDocumento. */
	private BigDecimal vlDocumento;

	/** Atributo dtEmissaoDocumento. */
	private String dtEmissaoDocumento;

	/** Atributo nrSequenciaArquivoRemessa. */
	private String nrSequenciaArquivoRemessa;

	/** Atributo nrLoteArquivoRemessa. */
	private String nrLoteArquivoRemessa;

	/** Atributo cdFavorecido. */
	private String cdFavorecido;

	/** Atributo dsBancoFavorecido. */
	private String dsBancoFavorecido;

	/** Atributo dsAgenciaFavorecido. */
	private String dsAgenciaFavorecido;

	/** Atributo cdTipoContaFavorecido. */
	private int cdTipoContaFavorecido; // campoOculto

	/** Atributo dsTipoContaFavorecido. */
	private String dsTipoContaFavorecido;

	/** Atributo dsMotivoSituacao. */
	private String dsMotivoSituacao;

	/** Atributo cdTipoManutencao. */
	private int cdTipoManutencao; // campoOculto

	/** Atributo dsTipoManutencao. */
	private String dsTipoManutencao; // campoOculto

	/** Atributo dtInclusao. */
	private String dtInclusao;

	/** Atributo hrInclusao. */
	private String hrInclusao;

	/** Atributo cdUsuarioInclusaoInterno. */
	private String cdUsuarioInclusaoInterno;

	/** Atributo cdUsuarioInclusaoExterno. */
	private String cdUsuarioInclusaoExterno;

	/** Atributo cdTipoCanalInclusao. */
	private int cdTipoCanalInclusao;

	/** Atributo dsTipoCanalInclusao. */
	private String dsTipoCanalInclusao;

	/** Atributo cdFluxoInclusao. */
	private String cdFluxoInclusao;

	/** Atributo dtManutencao. */
	private String dtManutencao;

	/** Atributo hrManutencao. */
	private String hrManutencao;

	/** Atributo cdUsuarioManutencaoInterno. */
	private String cdUsuarioManutencaoInterno;

	/** Atributo cdUsuarioManutencaoExterno. */
	private String cdUsuarioManutencaoExterno;

	/** Atributo cdTipoCanalManutencao. */
	private int cdTipoCanalManutencao;

	/** Atributo dsTipoCanalManutencao. */
	private String dsTipoCanalManutencao;

	/** Atributo cdFluxoManutencao. */
	private String cdFluxoManutencao;

	/** Atributo dsMoeda. */
	private String dsMoeda;

	/** Atributo dataHoraInclusaoFormatada. */
	private String dataHoraInclusaoFormatada;

	/** Atributo usuarioInclusaoFormatado. */
	private String usuarioInclusaoFormatado;

	/** Atributo tipoCanalInclusaoFormatado. */
	private String tipoCanalInclusaoFormatado;

	/** Atributo complementoInclusaoFormatado. */
	private String complementoInclusaoFormatado;

	/** Atributo dataHoraManutencaoFormatada. */
	private String dataHoraManutencaoFormatada;

	/** Atributo usuarioManutencaoFormatado. */
	private String usuarioManutencaoFormatado;

	/** Atributo tipoCanalManutencaoFormatado. */
	private String tipoCanalManutencaoFormatado;

	/** Atributo complementoManutencaoFormatado. */
	private String complementoManutencaoFormatado;

	/** Atributo dsIdentificacaoTransferenciaPagto. */
	private String dsIdentificacaoTransferenciaPagto;

	/** Atributo dsidentificacaoTitularPagto. */
	private String dsidentificacaoTitularPagto;

	/** Atributo dsFinalidadeDocTed. */
	private String dsFinalidadeDocTed;

	/** Atributo cdIndentificadorJudicial. */
	private String cdIndentificadorJudicial;

	// CLIENTE
	/** Atributo cdCpfCnpjCliente. */
	private Long cdCpfCnpjCliente;

	/** Atributo nomeCliente. */
	private String nomeCliente;

	/** Atributo dsPessoaJuridicaContrato. */
	private String dsPessoaJuridicaContrato;

	/** Atributo nrSequenciaContratoNegocio. */
	private String nrSequenciaContratoNegocio;

	/** Atributo cpfCnpjClienteFormatado. */
	private String cpfCnpjClienteFormatado;

	// CONTA DE D�BITO
	/** Atributo cdBancoDebito. */
	private Integer cdBancoDebito;

	/** Atributo cdAgenciaDebito. */
	private Integer cdAgenciaDebito;

	/** Atributo cdDigAgenciaDebto. */
	private String cdDigAgenciaDebto;

	/** Atributo cdContaDebito. */
	private Long cdContaDebito;

	/** Atributo dsDigAgenciaDebito. */
	private String dsDigAgenciaDebito;

	/** Atributo bancoDebitoFormatado. */
	private String bancoDebitoFormatado;

	/** Atributo agenciaDebitoFormatada. */
	private String agenciaDebitoFormatada;

	/** Atributo contaDebitoFormatada. */
	private String contaDebitoFormatada;

	// FAVORECIDO
	/** Atributo nmInscriFavorecido. */
	private String nmInscriFavorecido;

	/** Atributo dsNomeFavorecido. */
	private String dsNomeFavorecido;

	/** Atributo cdBancoCredito. */
	private Integer cdBancoCredito;
	
	/** Atributo dsBancoCredito. */
	private String dsBancoCredito;	
	
	/** Atributo cdIspbPagamento. */
	private String cdIspbPagamento;
	
    /** Atributo dsNomeReclamante. */
    private String dsNomeReclamante = null;
    
    /** Atributo nrProcessoDepositoJudicial. */
    private String nrProcessoDepositoJudicial = null;
    
    /** Atributo nrPisPasep. */
    private Long nrPisPasep = null;	

	/** Atributo cdAgenciaCredito. */
	private Integer cdAgenciaCredito;

	/** Atributo cdDigAgenciaCredito. */
	private String cdDigAgenciaCredito;

	/** Atributo cdContaCredito. */
	private Long cdContaCredito;

	/** Atributo cdDigContaCredito. */
	private String cdDigContaCredito;

	/** Atributo numeroInscricaoFavorecidoFormatado. */
	private String numeroInscricaoFavorecidoFormatado;

	/** Atributo bancoCreditoFormatado. */
	private String bancoCreditoFormatado;

	/** Atributo agenciaCreditoFormatada. */
	private String agenciaCreditoFormatada;

	/** Atributo contaCreditoFormatada. */
	private String contaCreditoFormatada;

	// BENEFICI�RIO
	/** Atributo nmInscriBeneficio. */
	private String nmInscriBeneficio;

	/** Atributo cdTipoInscriBeneficio. */
	private String cdTipoInscriBeneficio;

	/** Atributo cdNomeBeneficio. */
	private String cdNomeBeneficio;

	/** Atributo numeroInscricaoBeneficiarioFormatado. */
	private String numeroInscricaoBeneficiarioFormatado;

	// PAGAMENTO
	/** Atributo dtPagamento. */
	private String dtPagamento;

	/** Atributo cdSituacaoPagamento. */
	private String cdSituacaoPagamento;

	/** Atributo cdIndicadorAuto. */
	private Integer cdIndicadorAuto;

	/** Atributo cdIndicadorSemConsulta. */
	private Integer cdIndicadorSemConsulta;

	/** Atributo dsTipoServicoOperacao. */
	private String dsTipoServicoOperacao;

	/** Atributo dsModalidadeRelacionado. */
	private String dsModalidadeRelacionado;

	/** Atributo cdControlePagamento. */
	private String cdControlePagamento;

	/** Atributo dtFloatingPagamento. */
	private String dtFloatingPagamento;

	/** Atributo dtEfetivFloatPgto. */
	private String dtEfetivFloatPgto;

	/** Atributo vlFloatingPagamento. */
	private BigDecimal vlFloatingPagamento;

	/** Atributo cdOperacaoDcom. */
	private Long cdOperacaoDcom;

	/** Atributo dsSituacaoDcom. */
	private String dsSituacaoDcom;

	/** Atributo cdLoteInterno. */
	private Long cdLoteInterno;
	
	/** Atributo dsIndicadorAutorizacao. */
	private String dsIndicadorAutorizacao;
	
	/** Atributo dsTipoLayout. */
	private String dsTipoLayout;

	/**
	 * Get: cdCamaraCentral.
	 * 
	 * @return cdCamaraCentral
	 */
	public String getCdCamaraCentral() {
		return cdCamaraCentral;
	}

	/**
	 * Set: cdCamaraCentral.
	 * 
	 * @param cdCamaraCentral
	 *            the cd camara central
	 */
	public void setCdCamaraCentral(final String cdCamaraCentral) {
		this.cdCamaraCentral = cdCamaraCentral;
	}

	/**
	 * Get: cdFavorecido.
	 * 
	 * @return cdFavorecido
	 */
	public String getCdFavorecido() {
		return cdFavorecido;
	}

	/**
	 * Set: cdFavorecido.
	 * 
	 * @param cdFavorecido
	 *            the cd favorecido
	 */
	public void setCdFavorecido(final String cdFavorecido) {
		this.cdFavorecido = cdFavorecido;
	}

	/**
	 * Get: cdFinalidadeDoc.
	 * 
	 * @return cdFinalidadeDoc
	 */
	public String getCdFinalidadeDoc() {
		return cdFinalidadeDoc;
	}

	/**
	 * Set: cdFinalidadeDoc.
	 * 
	 * @param cdFinalidadeDoc
	 *            the cd finalidade doc
	 */
	public void setCdFinalidadeDoc(final String cdFinalidadeDoc) {
		this.cdFinalidadeDoc = cdFinalidadeDoc;
	}

	/**
	 * Get: cdFluxoInclusao.
	 * 
	 * @return cdFluxoInclusao
	 */
	public String getCdFluxoInclusao() {
		return cdFluxoInclusao;
	}

	/**
	 * Set: cdFluxoInclusao.
	 * 
	 * @param cdFluxoInclusao
	 *            the cd fluxo inclusao
	 */
	public void setCdFluxoInclusao(final String cdFluxoInclusao) {
		this.cdFluxoInclusao = cdFluxoInclusao;
	}

	/**
	 * Get: cdIdentificacaoTitular.
	 * 
	 * @return cdIdentificacaoTitular
	 */
	public String getCdIdentificacaoTitular() {
		return cdIdentificacaoTitular;
	}

	/**
	 * Set: cdIdentificacaoTitular.
	 * 
	 * @param cdIdentificacaoTitular
	 *            the cd identificacao titular
	 */
	public void setCdIdentificacaoTitular(final String cdIdentificacaoTitular) {
		this.cdIdentificacaoTitular = cdIdentificacaoTitular;
	}

	/**
	 * Get: cdIdentificacaoTransferencia.
	 * 
	 * @return cdIdentificacaoTransferencia
	 */
	public String getCdIdentificacaoTransferencia() {
		return cdIdentificacaoTransferencia;
	}

	/**
	 * Set: cdIdentificacaoTransferencia.
	 * 
	 * @param cdIdentificacaoTransferencia
	 *            the cd identificacao transferencia
	 */
	public void setCdIdentificacaoTransferencia(
			final String cdIdentificacaoTransferencia) {
		this.cdIdentificacaoTransferencia = cdIdentificacaoTransferencia;
	}

	/**
	 * Get: cdIndicadorEconomicoMoeda.
	 * 
	 * @return cdIndicadorEconomicoMoeda
	 */
	public int getCdIndicadorEconomicoMoeda() {
		return cdIndicadorEconomicoMoeda;
	}

	/**
	 * Set: cdIndicadorEconomicoMoeda.
	 * 
	 * @param cdIndicadorEconomicoMoeda
	 *            the cd indicador economico moeda
	 */
	public void setCdIndicadorEconomicoMoeda(final int cdIndicadorEconomicoMoeda) {
		this.cdIndicadorEconomicoMoeda = cdIndicadorEconomicoMoeda;
	}

	/**
	 * Get: cdIndicadorModalidadePgit.
	 * 
	 * @return cdIndicadorModalidadePgit
	 */
	public Integer getCdIndicadorModalidadePgit() {
		return cdIndicadorModalidadePgit;
	}

	/**
	 * Set: cdIndicadorModalidadePgit.
	 * 
	 * @param cdIndicadorModalidadePgit
	 *            the cd indicador modalidade pgit
	 */
	public void setCdIndicadorModalidadePgit(final Integer cdIndicadorModalidadePgit) {
		this.cdIndicadorModalidadePgit = cdIndicadorModalidadePgit;
	}

	/**
	 * Get: cdListaDebito.
	 * 
	 * @return cdListaDebito
	 */
	public String getCdListaDebito() {
		return cdListaDebito;
	}

	/**
	 * Set: cdListaDebito.
	 * 
	 * @param cdListaDebito
	 *            the cd lista debito
	 */
	public void setCdListaDebito(final String cdListaDebito) {
		this.cdListaDebito = cdListaDebito;
	}

	/**
	 * Get: cdMotivoSituacaoPagamento.
	 * 
	 * @return cdMotivoSituacaoPagamento
	 */
	public int getCdMotivoSituacaoPagamento() {
		return cdMotivoSituacaoPagamento;
	}

	/**
	 * Set: cdMotivoSituacaoPagamento.
	 * 
	 * @param cdMotivoSituacaoPagamento
	 *            the cd motivo situacao pagamento
	 */
	public void setCdMotivoSituacaoPagamento(final int cdMotivoSituacaoPagamento) {
		this.cdMotivoSituacaoPagamento = cdMotivoSituacaoPagamento;
	}

	/**
	 * Get: cdSerieDocumento.
	 * 
	 * @return cdSerieDocumento
	 */
	public String getCdSerieDocumento() {
		return cdSerieDocumento;
	}

	/**
	 * Set: cdSerieDocumento.
	 * 
	 * @param cdSerieDocumento
	 *            the cd serie documento
	 */
	public void setCdSerieDocumento(final String cdSerieDocumento) {
		this.cdSerieDocumento = cdSerieDocumento;
	}

	/**
	 * Get: cdSituacaoContrato.
	 * 
	 * @return cdSituacaoContrato
	 */
	public String getCdSituacaoContrato() {
		return cdSituacaoContrato;
	}

	/**
	 * Set: cdSituacaoContrato.
	 * 
	 * @param cdSituacaoContrato
	 *            the cd situacao contrato
	 */
	public void setCdSituacaoContrato(final String cdSituacaoContrato) {
		this.cdSituacaoContrato = cdSituacaoContrato;
	}

	/**
	 * Get: cdSituacaoOperacaoPagamento.
	 * 
	 * @return cdSituacaoOperacaoPagamento
	 */
	public int getCdSituacaoOperacaoPagamento() {
		return cdSituacaoOperacaoPagamento;
	}

	/**
	 * Set: cdSituacaoOperacaoPagamento.
	 * 
	 * @param cdSituacaoOperacaoPagamento
	 *            the cd situacao operacao pagamento
	 */
	public void setCdSituacaoOperacaoPagamento(final int cdSituacaoOperacaoPagamento) {
		this.cdSituacaoOperacaoPagamento = cdSituacaoOperacaoPagamento;
	}

	/**
	 * Get: cdTipoCanalInclusao.
	 * 
	 * @return cdTipoCanalInclusao
	 */
	public int getCdTipoCanalInclusao() {
		return cdTipoCanalInclusao;
	}

	/**
	 * Set: cdTipoCanalInclusao.
	 * 
	 * @param cdTipoCanalInclusao
	 *            the cd tipo canal inclusao
	 */
	public void setCdTipoCanalInclusao(final int cdTipoCanalInclusao) {
		this.cdTipoCanalInclusao = cdTipoCanalInclusao;
	}

	/**
	 * Get: cdTipoCanalManutencao.
	 * 
	 * @return cdTipoCanalManutencao
	 */
	public int getCdTipoCanalManutencao() {
		return cdTipoCanalManutencao;
	}

	/**
	 * Set: cdTipoCanalManutencao.
	 * 
	 * @param cdTipoCanalManutencao
	 *            the cd tipo canal manutencao
	 */
	public void setCdTipoCanalManutencao(final int cdTipoCanalManutencao) {
		this.cdTipoCanalManutencao = cdTipoCanalManutencao;
	}

	/**
	 * Get: cdTipoContaFavorecido.
	 * 
	 * @return cdTipoContaFavorecido
	 */
	public int getCdTipoContaFavorecido() {
		return cdTipoContaFavorecido;
	}

	/**
	 * Set: cdTipoContaFavorecido.
	 * 
	 * @param cdTipoContaFavorecido
	 *            the cd tipo conta favorecido
	 */
	public void setCdTipoContaFavorecido(final int cdTipoContaFavorecido) {
		this.cdTipoContaFavorecido = cdTipoContaFavorecido;
	}

	/**
	 * Get: cdTipoDocumento.
	 * 
	 * @return cdTipoDocumento
	 */
	public int getCdTipoDocumento() {
		return cdTipoDocumento;
	}

	/**
	 * Set: cdTipoDocumento.
	 * 
	 * @param cdTipoDocumento
	 *            the cd tipo documento
	 */
	public void setCdTipoDocumento(final int cdTipoDocumento) {
		this.cdTipoDocumento = cdTipoDocumento;
	}

	/**
	 * Get: cdTipoInscricaoFavorecido.
	 * 
	 * @return cdTipoInscricaoFavorecido
	 */
	public int getCdTipoInscricaoFavorecido() {
		return cdTipoInscricaoFavorecido;
	}

	/**
	 * Set: cdTipoInscricaoFavorecido.
	 * 
	 * @param cdTipoInscricaoPagador
	 *            the cd tipo inscricao favorecido
	 */
	public void setCdTipoInscricaoFavorecido(final int cdTipoInscricaoPagador) {
		this.cdTipoInscricaoFavorecido = cdTipoInscricaoPagador;
	}

	/**
	 * Get: cdTipoManutencao.
	 * 
	 * @return cdTipoManutencao
	 */
	public int getCdTipoManutencao() {
		return cdTipoManutencao;
	}

	/**
	 * Set: cdTipoManutencao.
	 * 
	 * @param cdTipoManutencao
	 *            the cd tipo manutencao
	 */
	public void setCdTipoManutencao(final int cdTipoManutencao) {
		this.cdTipoManutencao = cdTipoManutencao;
	}

	/**
	 * Get: cdUsuarioInclusaoExterno.
	 * 
	 * @return cdUsuarioInclusaoExterno
	 */
	public String getCdUsuarioInclusaoExterno() {
		return cdUsuarioInclusaoExterno;
	}

	/**
	 * Set: cdUsuarioInclusaoExterno.
	 * 
	 * @param cdUsuarioInclusaoExterno
	 *            the cd usuario inclusao externo
	 */
	public void setCdUsuarioInclusaoExterno(final String cdUsuarioInclusaoExterno) {
		this.cdUsuarioInclusaoExterno = cdUsuarioInclusaoExterno;
	}

	/**
	 * Get: cdUsuarioInclusaoInterno.
	 * 
	 * @return cdUsuarioInclusaoInterno
	 */
	public String getCdUsuarioInclusaoInterno() {
		return cdUsuarioInclusaoInterno;
	}

	/**
	 * Set: cdUsuarioInclusaoInterno.
	 * 
	 * @param cdUsuarioInclusaoInterno
	 *            the cd usuario inclusao interno
	 */
	public void setCdUsuarioInclusaoInterno(final String cdUsuarioInclusaoInterno) {
		this.cdUsuarioInclusaoInterno = cdUsuarioInclusaoInterno;
	}

	/**
	 * Get: cdUsuarioManutencaoExterno.
	 * 
	 * @return cdUsuarioManutencaoExterno
	 */
	public String getCdUsuarioManutencaoExterno() {
		return cdUsuarioManutencaoExterno;
	}

	/**
	 * Set: cdUsuarioManutencaoExterno.
	 * 
	 * @param cdUsuarioManutencaoExterno
	 *            the cd usuario manutencao externo
	 */
	public void setCdUsuarioManutencaoExterno(final String cdUsuarioManutencaoExterno) {
		this.cdUsuarioManutencaoExterno = cdUsuarioManutencaoExterno;
	}

	/**
	 * Get: cdUsuarioManutencaoInterno.
	 * 
	 * @return cdUsuarioManutencaoInterno
	 */
	public String getCdUsuarioManutencaoInterno() {
		return cdUsuarioManutencaoInterno;
	}

	/**
	 * Set: cdUsuarioManutencaoInterno.
	 * 
	 * @param cdUsuarioManutencaoInterno
	 *            the cd usuario manutencao interno
	 */
	public void setCdUsuarioManutencaoInterno(final String cdUsuarioManutencaoInterno) {
		this.cdUsuarioManutencaoInterno = cdUsuarioManutencaoInterno;
	}

	/**
	 * Get: codMensagem.
	 * 
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}

	/**
	 * Set: codMensagem.
	 * 
	 * @param codMensagem
	 *            the cod mensagem
	 */
	public void setCodMensagem(final String codMensagem) {
		this.codMensagem = codMensagem;
	}

	/**
	 * Get: dsAgenciaDebito.
	 * 
	 * @return dsAgenciaDebito
	 */
	public String getDsAgenciaDebito() {
		return dsAgenciaDebito;
	}

	/**
	 * Set: dsAgenciaDebito.
	 * 
	 * @param dsAgenciaDebito
	 *            the ds agencia debito
	 */
	public void setDsAgenciaDebito(final String dsAgenciaDebito) {
		this.dsAgenciaDebito = dsAgenciaDebito;
	}

	/**
	 * Get: dsAgenciaFavorecido.
	 * 
	 * @return dsAgenciaFavorecido
	 */
	public String getDsAgenciaFavorecido() {
		return dsAgenciaFavorecido;
	}

	/**
	 * Set: dsAgenciaFavorecido.
	 * 
	 * @param dsAgenciaFavorecido
	 *            the ds agencia favorecido
	 */
	public void setDsAgenciaFavorecido(final String dsAgenciaFavorecido) {
		this.dsAgenciaFavorecido = dsAgenciaFavorecido;
	}

	/**
	 * Get: dtAgendamento.
	 * 
	 * @return dtAgendamento
	 */
	public String getDtAgendamento() {
		return dtAgendamento;
	}

	/**
	 * Set: dtAgendamento.
	 * 
	 * @param dtAgendamento
	 *            the dt agendamento
	 */
	public void setDtAgendamento(final String dtAgendamento) {
		this.dtAgendamento = dtAgendamento;
	}

	/**
	 * Get: dsBancoDebito.
	 * 
	 * @return dsBancoDebito
	 */
	public String getDsBancoDebito() {
		return dsBancoDebito;
	}

	/**
	 * Set: dsBancoDebito.
	 * 
	 * @param dsBancoDebito
	 *            the ds banco debito
	 */
	public void setDsBancoDebito(final String dsBancoDebito) {
		this.dsBancoDebito = dsBancoDebito;
	}

	/**
	 * Get: dsBancoFavorecido.
	 * 
	 * @return dsBancoFavorecido
	 */
	public String getDsBancoFavorecido() {
		return dsBancoFavorecido;
	}

	/**
	 * Set: dsBancoFavorecido.
	 * 
	 * @param dsBancoFavorecido
	 *            the ds banco favorecido
	 */
	public void setDsBancoFavorecido(final String dsBancoFavorecido) {
		this.dsBancoFavorecido = dsBancoFavorecido;
	}

	/**
	 * Get: dsContrato.
	 * 
	 * @return dsContrato
	 */
	public String getDsContrato() {
		return dsContrato;
	}

	/**
	 * Set: dsContrato.
	 * 
	 * @param dsContrato
	 *            the ds contrato
	 */
	public void setDsContrato(final String dsContrato) {
		this.dsContrato = dsContrato;
	}

	/**
	 * Get: cdFluxoManutencao.
	 * 
	 * @return cdFluxoManutencao
	 */
	public String getCdFluxoManutencao() {
		return cdFluxoManutencao;
	}

	/**
	 * Set: cdFluxoManutencao.
	 * 
	 * @param cdFluxoManutencao
	 *            the cd fluxo manutencao
	 */
	public void setCdFluxoManutencao(final String cdFluxoManutencao) {
		this.cdFluxoManutencao = cdFluxoManutencao;
	}

	/**
	 * Get: dsMensagemPrimeiraLinha.
	 * 
	 * @return dsMensagemPrimeiraLinha
	 */
	public String getDsMensagemPrimeiraLinha() {
		return dsMensagemPrimeiraLinha;
	}

	/**
	 * Set: dsMensagemPrimeiraLinha.
	 * 
	 * @param dsMensagemPrimeiraLinha
	 *            the ds mensagem primeira linha
	 */
	public void setDsMensagemPrimeiraLinha(final String dsMensagemPrimeiraLinha) {
		this.dsMensagemPrimeiraLinha = dsMensagemPrimeiraLinha;
	}

	/**
	 * Get: dsMensagemSegundaLinha.
	 * 
	 * @return dsMensagemSegundaLinha
	 */
	public String getDsMensagemSegundaLinha() {
		return dsMensagemSegundaLinha;
	}

	/**
	 * Set: dsMensagemSegundaLinha.
	 * 
	 * @param dsMensagemSegundaLinha
	 *            the ds mensagem segunda linha
	 */
	public void setDsMensagemSegundaLinha(final String dsMensagemSegundaLinha) {
		this.dsMensagemSegundaLinha = dsMensagemSegundaLinha;
	}

	/**
	 * Get: dsMotivoSituacao.
	 * 
	 * @return dsMotivoSituacao
	 */
	public String getDsMotivoSituacao() {
		return dsMotivoSituacao;
	}

	/**
	 * Set: dsMotivoSituacao.
	 * 
	 * @param dsMotivoSituacao
	 *            the ds motivo situacao
	 */
	public void setDsMotivoSituacao(final String dsMotivoSituacao) {
		this.dsMotivoSituacao = dsMotivoSituacao;
	}

	/**
	 * Get: dsPagamento.
	 * 
	 * @return dsPagamento
	 */
	public String getDsPagamento() {
		return dsPagamento;
	}

	/**
	 * Set: dsPagamento.
	 * 
	 * @param dsPagamento
	 *            the ds pagamento
	 */
	public void setDsPagamento(final String dsPagamento) {
		this.dsPagamento = dsPagamento;
	}

	/**
	 * Get: dsTipoCanalInclusao.
	 * 
	 * @return dsTipoCanalInclusao
	 */
	public String getDsTipoCanalInclusao() {
		return dsTipoCanalInclusao;
	}

	/**
	 * Set: dsTipoCanalInclusao.
	 * 
	 * @param dsTipoCanalInclusao
	 *            the ds tipo canal inclusao
	 */
	public void setDsTipoCanalInclusao(final String dsTipoCanalInclusao) {
		this.dsTipoCanalInclusao = dsTipoCanalInclusao;
	}

	/**
	 * Get: dsTipoCanalManutencao.
	 * 
	 * @return dsTipoCanalManutencao
	 */
	public String getDsTipoCanalManutencao() {
		return dsTipoCanalManutencao;
	}

	/**
	 * Set: dsTipoCanalManutencao.
	 * 
	 * @param dsTipoCanalManutencao
	 *            the ds tipo canal manutencao
	 */
	public void setDsTipoCanalManutencao(final String dsTipoCanalManutencao) {
		this.dsTipoCanalManutencao = dsTipoCanalManutencao;
	}

	/**
	 * Get: dsTipoContaDebito.
	 * 
	 * @return dsTipoContaDebito
	 */
	public String getDsTipoContaDebito() {
		return dsTipoContaDebito;
	}

	/**
	 * Set: dsTipoContaDebito.
	 * 
	 * @param dsTipoContaDebito
	 *            the ds tipo conta debito
	 */
	public void setDsTipoContaDebito(final String dsTipoContaDebito) {
		this.dsTipoContaDebito = dsTipoContaDebito;
	}

	/**
	 * Get: dsTipoContaFavorecido.
	 * 
	 * @return dsTipoContaFavorecido
	 */
	public String getDsTipoContaFavorecido() {
		return dsTipoContaFavorecido;
	}

	/**
	 * Set: dsTipoContaFavorecido.
	 * 
	 * @param dsTipoContaFavorecido
	 *            the ds tipo conta favorecido
	 */
	public void setDsTipoContaFavorecido(final String dsTipoContaFavorecido) {
		this.dsTipoContaFavorecido = dsTipoContaFavorecido;
	}

	/**
	 * Get: dsTipoDocumento.
	 * 
	 * @return dsTipoDocumento
	 */
	public String getDsTipoDocumento() {
		return dsTipoDocumento;
	}

	/**
	 * Set: dsTipoDocumento.
	 * 
	 * @param dsTipoDocumento
	 *            the ds tipo documento
	 */
	public void setDsTipoDocumento(final String dsTipoDocumento) {
		this.dsTipoDocumento = dsTipoDocumento;
	}

	/**
	 * Get: dsTipoManutencao.
	 * 
	 * @return dsTipoManutencao
	 */
	public String getDsTipoManutencao() {
		return dsTipoManutencao;
	}

	/**
	 * Set: dsTipoManutencao.
	 * 
	 * @param dsTipoManutencao
	 *            the ds tipo manutencao
	 */
	public void setDsTipoManutencao(final String dsTipoManutencao) {
		this.dsTipoManutencao = dsTipoManutencao;
	}

	/**
	 * Get: dsUsoEmpresa.
	 * 
	 * @return dsUsoEmpresa
	 */
	public String getDsUsoEmpresa() {
		return dsUsoEmpresa;
	}

	/**
	 * Set: dsUsoEmpresa.
	 * 
	 * @param dsUsoEmpresa
	 *            the ds uso empresa
	 */
	public void setDsUsoEmpresa(final String dsUsoEmpresa) {
		this.dsUsoEmpresa = dsUsoEmpresa;
	}

	/**
	 * Get: dtEmissaoDocumento.
	 * 
	 * @return dtEmissaoDocumento
	 */
	public String getDtEmissaoDocumento() {
		return dtEmissaoDocumento;
	}

	/**
	 * Set: dtEmissaoDocumento.
	 * 
	 * @param dtEmissaoDocumento
	 *            the dt emissao documento
	 */
	public void setDtEmissaoDocumento(final String dtEmissaoDocumento) {
		this.dtEmissaoDocumento = dtEmissaoDocumento;
	}

	/**
	 * Get: dtInclusao.
	 * 
	 * @return dtInclusao
	 */
	public String getDtInclusao() {
		return dtInclusao;
	}

	/**
	 * Set: dtInclusao.
	 * 
	 * @param dtInclusao
	 *            the dt inclusao
	 */
	public void setDtInclusao(final String dtInclusao) {
		this.dtInclusao = dtInclusao;
	}

	/**
	 * Get: dtManutencao.
	 * 
	 * @return dtManutencao
	 */
	public String getDtManutencao() {
		return dtManutencao;
	}

	/**
	 * Set: dtManutencao.
	 * 
	 * @param dtManutencao
	 *            the dt manutencao
	 */
	public void setDtManutencao(final String dtManutencao) {
		this.dtManutencao = dtManutencao;
	}

	/**
	 * Get: dtVencimento.
	 * 
	 * @return dtVencimento
	 */
	public String getDtVencimento() {
		return dtVencimento;
	}

	/**
	 * Set: dtVencimento.
	 * 
	 * @param dtVencimento
	 *            the dt vencimento
	 */
	public void setDtVencimento(final String dtVencimento) {
		this.dtVencimento = dtVencimento;
	}

	/**
	 * Get: hrInclusao.
	 * 
	 * @return hrInclusao
	 */
	public String getHrInclusao() {
		return hrInclusao;
	}

	/**
	 * Set: hrInclusao.
	 * 
	 * @param hrInclusao
	 *            the hr inclusao
	 */
	public void setHrInclusao(final String hrInclusao) {
		this.hrInclusao = hrInclusao;
	}

	/**
	 * Get: hrManutencao.
	 * 
	 * @return hrManutencao
	 */
	public String getHrManutencao() {
		return hrManutencao;
	}

	/**
	 * Set: hrManutencao.
	 * 
	 * @param hrManutencao
	 *            the hr manutencao
	 */
	public void setHrManutencao(final String hrManutencao) {
		this.hrManutencao = hrManutencao;
	}

	/**
	 * Get: mensagem.
	 * 
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}

	/**
	 * Set: mensagem.
	 * 
	 * @param mensagem
	 *            the mensagem
	 */
	public void setMensagem(final String mensagem) {
		this.mensagem = mensagem;
	}

	/**
	 * Get: nrDocumento.
	 * 
	 * @return nrDocumento
	 */
	public String getNrDocumento() {
		return nrDocumento;
	}

	/**
	 * Set: nrDocumento.
	 * 
	 * @param nrDocumento
	 *            the nr documento
	 */
	public void setNrDocumento(final String nrDocumento) {
		this.nrDocumento = nrDocumento;
	}

	/**
	 * Get: nrLoteArquivoRemessa.
	 * 
	 * @return nrLoteArquivoRemessa
	 */
	public String getNrLoteArquivoRemessa() {
		return nrLoteArquivoRemessa;
	}

	/**
	 * Set: nrLoteArquivoRemessa.
	 * 
	 * @param nrLoteArquivoRemessa
	 *            the nr lote arquivo remessa
	 */
	public void setNrLoteArquivoRemessa(final String nrLoteArquivoRemessa) {
		this.nrLoteArquivoRemessa = nrLoteArquivoRemessa;
	}

	/**
	 * Get: nrSequenciaArquivoRemessa.
	 * 
	 * @return nrSequenciaArquivoRemessa
	 */
	public String getNrSequenciaArquivoRemessa() {
		return nrSequenciaArquivoRemessa;
	}

	/**
	 * Set: nrSequenciaArquivoRemessa.
	 * 
	 * @param nrSequenciaArquivoRemessa
	 *            the nr sequencia arquivo remessa
	 */
	public void setNrSequenciaArquivoRemessa(final String nrSequenciaArquivoRemessa) {
		this.nrSequenciaArquivoRemessa = nrSequenciaArquivoRemessa;
	}

	/**
	 * Get: qtMoeda.
	 * 
	 * @return qtMoeda
	 */
	public BigDecimal getQtMoeda() {
		return qtMoeda;
	}

	/**
	 * Set: qtMoeda.
	 * 
	 * @param qtMoeda
	 *            the qt moeda
	 */
	public void setQtMoeda(final BigDecimal qtMoeda) {
		this.qtMoeda = qtMoeda;
	}

	/**
	 * Get: vlAgendado.
	 * 
	 * @return vlAgendado
	 */
	public BigDecimal getVlAgendado() {
		return vlAgendado;
	}

	/**
	 * Set: vlAgendado.
	 * 
	 * @param vlAgendado
	 *            the vl agendado
	 */
	public void setVlAgendado(final BigDecimal vlAgendado) {
		this.vlAgendado = vlAgendado;
	}

	/**
	 * Get: vlDesconto.
	 * 
	 * @return vlDesconto
	 */
	public BigDecimal getVlDesconto() {
		return vlDesconto;
	}

	/**
	 * Set: vlDesconto.
	 * 
	 * @param vlDesconto
	 *            the vl desconto
	 */
	public void setVlDesconto(final BigDecimal vlDesconto) {
		this.vlDesconto = vlDesconto;
	}

	/**
	 * Get: vlDocumento.
	 * 
	 * @return vlDocumento
	 */
	public BigDecimal getVlDocumento() {
		return vlDocumento;
	}

	/**
	 * Set: vlDocumento.
	 * 
	 * @param vlDocumento
	 *            the vl documento
	 */
	public void setVlDocumento(final BigDecimal vlDocumento) {
		this.vlDocumento = vlDocumento;
	}

	/**
	 * Get: vlEfetivo.
	 * 
	 * @return vlEfetivo
	 */
	public BigDecimal getVlEfetivo() {
		return vlEfetivo;
	}

	/**
	 * Set: vlEfetivo.
	 * 
	 * @param vlEfetivo
	 *            the vl efetivo
	 */
	public void setVlEfetivo(final BigDecimal vlEfetivo) {
		this.vlEfetivo = vlEfetivo;
	}

	/**
	 * Get: cdFinalidadeTed.
	 * 
	 * @return cdFinalidadeTed
	 */
	public String getCdFinalidadeTed() {
		return cdFinalidadeTed;
	}

	/**
	 * Set: cdFinalidadeTed.
	 * 
	 * @param cdFinalidadeTed
	 *            the cd finalidade ted
	 */
	public void setCdFinalidadeTed(final String cdFinalidadeTed) {
		this.cdFinalidadeTed = cdFinalidadeTed;
	}

	/**
	 * Get: vlAbatidoCredito.
	 * 
	 * @return vlAbatidoCredito
	 */
	public BigDecimal getVlAbatidoCredito() {
		return vlAbatidoCredito;
	}

	/**
	 * Set: vlAbatidoCredito.
	 * 
	 * @param vlAbatidoCredito
	 *            the vl abatido credito
	 */
	public void setVlAbatidoCredito(final BigDecimal vlAbatidoCredito) {
		this.vlAbatidoCredito = vlAbatidoCredito;
	}

	/**
	 * Get: vlInssCredito.
	 * 
	 * @return vlInssCredito
	 */
	public BigDecimal getVlInssCredito() {
		return vlInssCredito;
	}

	/**
	 * Set: vlInssCredito.
	 * 
	 * @param vlInssCredito
	 *            the vl inss credito
	 */
	public void setVlInssCredito(final BigDecimal vlInssCredito) {
		this.vlInssCredito = vlInssCredito;
	}

	/**
	 * Get: vlIofCredito.
	 * 
	 * @return vlIofCredito
	 */
	public BigDecimal getVlIofCredito() {
		return vlIofCredito;
	}

	/**
	 * Set: vlIofCredito.
	 * 
	 * @param vlIofCredito
	 *            the vl iof credito
	 */
	public void setVlIofCredito(final BigDecimal vlIofCredito) {
		this.vlIofCredito = vlIofCredito;
	}

	/**
	 * Get: vlIrCredito.
	 * 
	 * @return vlIrCredito
	 */
	public BigDecimal getVlIrCredito() {
		return vlIrCredito;
	}

	/**
	 * Set: vlIrCredito.
	 * 
	 * @param vlIrCredito
	 *            the vl ir credito
	 */
	public void setVlIrCredito(final BigDecimal vlIrCredito) {
		this.vlIrCredito = vlIrCredito;
	}

	/**
	 * Get: vlIssCredito.
	 * 
	 * @return vlIssCredito
	 */
	public BigDecimal getVlIssCredito() {
		return vlIssCredito;
	}

	/**
	 * Set: vlIssCredito.
	 * 
	 * @param vlIssCredito
	 *            the vl iss credito
	 */
	public void setVlIssCredito(final BigDecimal vlIssCredito) {
		this.vlIssCredito = vlIssCredito;
	}

	/**
	 * Get: vlMoraJuros.
	 * 
	 * @return vlMoraJuros
	 */
	public BigDecimal getVlMoraJuros() {
		return vlMoraJuros;
	}

	/**
	 * Set: vlMoraJuros.
	 * 
	 * @param vlMoraJuros
	 *            the vl mora juros
	 */
	public void setVlMoraJuros(final BigDecimal vlMoraJuros) {
		this.vlMoraJuros = vlMoraJuros;
	}

	/**
	 * Get: vlMultaCredito.
	 * 
	 * @return vlMultaCredito
	 */
	public BigDecimal getVlMultaCredito() {
		return vlMultaCredito;
	}

	/**
	 * Set: vlMultaCredito.
	 * 
	 * @param vlMultaCredito
	 *            the vl multa credito
	 */
	public void setVlMultaCredito(final BigDecimal vlMultaCredito) {
		this.vlMultaCredito = vlMultaCredito;
	}

	/**
	 * Get: vlOutrasDeducoes.
	 * 
	 * @return vlOutrasDeducoes
	 */
	public BigDecimal getVlOutrasDeducoes() {
		return vlOutrasDeducoes;
	}

	/**
	 * Set: vlOutrasDeducoes.
	 * 
	 * @param vlOutrasDeducoes
	 *            the vl outras deducoes
	 */
	public void setVlOutrasDeducoes(final BigDecimal vlOutrasDeducoes) {
		this.vlOutrasDeducoes = vlOutrasDeducoes;
	}

	/**
	 * Get: vlOutroAcrescimo.
	 * 
	 * @return vlOutroAcrescimo
	 */
	public BigDecimal getVlOutroAcrescimo() {
		return vlOutroAcrescimo;
	}

	/**
	 * Set: vlOutroAcrescimo.
	 * 
	 * @param vlOutroAcrescimo
	 *            the vl outro acrescimo
	 */
	public void setVlOutroAcrescimo(final BigDecimal vlOutroAcrescimo) {
		this.vlOutroAcrescimo = vlOutroAcrescimo;
	}

	/**
	 * Get: complementoInclusaoFormatado.
	 * 
	 * @return complementoInclusaoFormatado
	 */
	public String getComplementoInclusaoFormatado() {
		return complementoInclusaoFormatado;
	}

	/**
	 * Set: complementoInclusaoFormatado.
	 * 
	 * @param complementoInclusaoFormatado
	 *            the complemento inclusao formatado
	 */
	public void setComplementoInclusaoFormatado(
			final String complementoInclusaoFormatado) {
		this.complementoInclusaoFormatado = complementoInclusaoFormatado;
	}

	/**
	 * Get: complementoManutencaoFormatado.
	 * 
	 * @return complementoManutencaoFormatado
	 */
	public String getComplementoManutencaoFormatado() {
		return complementoManutencaoFormatado;
	}

	/**
	 * Set: complementoManutencaoFormatado.
	 * 
	 * @param complementoManutencaoFormatado
	 *            the complemento manutencao formatado
	 */
	public void setComplementoManutencaoFormatado(
			final String complementoManutencaoFormatado) {
		this.complementoManutencaoFormatado = complementoManutencaoFormatado;
	}

	/**
	 * Get: dataHoraInclusaoFormatada.
	 * 
	 * @return dataHoraInclusaoFormatada
	 */
	public String getDataHoraInclusaoFormatada() {
		return dataHoraInclusaoFormatada;
	}

	/**
	 * Set: dataHoraInclusaoFormatada.
	 * 
	 * @param dataHoraInclusaoFormatada
	 *            the data hora inclusao formatada
	 */
	public void setDataHoraInclusaoFormatada(final String dataHoraInclusaoFormatada) {
		this.dataHoraInclusaoFormatada = dataHoraInclusaoFormatada;
	}

	/**
	 * Get: dataHoraManutencaoFormatada.
	 * 
	 * @return dataHoraManutencaoFormatada
	 */
	public String getDataHoraManutencaoFormatada() {
		return dataHoraManutencaoFormatada;
	}

	/**
	 * Set: dataHoraManutencaoFormatada.
	 * 
	 * @param dataHoraManutencaoFormatada
	 *            the data hora manutencao formatada
	 */
	public void setDataHoraManutencaoFormatada(
			final String dataHoraManutencaoFormatada) {
		this.dataHoraManutencaoFormatada = dataHoraManutencaoFormatada;
	}

	/**
	 * Get: tipoCanalInclusaoFormatado.
	 * 
	 * @return tipoCanalInclusaoFormatado
	 */
	public String getTipoCanalInclusaoFormatado() {
		return tipoCanalInclusaoFormatado;
	}

	/**
	 * Set: tipoCanalInclusaoFormatado.
	 * 
	 * @param tipoCanalInclusaoFormatado
	 *            the tipo canal inclusao formatado
	 */
	public void setTipoCanalInclusaoFormatado(final String tipoCanalInclusaoFormatado) {
		this.tipoCanalInclusaoFormatado = tipoCanalInclusaoFormatado;
	}

	/**
	 * Get: tipoCanalManutencaoFormatado.
	 * 
	 * @return tipoCanalManutencaoFormatado
	 */
	public String getTipoCanalManutencaoFormatado() {
		return tipoCanalManutencaoFormatado;
	}

	/**
	 * Set: tipoCanalManutencaoFormatado.
	 * 
	 * @param tipoCanalManutencaoFormatado
	 *            the tipo canal manutencao formatado
	 */
	public void setTipoCanalManutencaoFormatado(
			final String tipoCanalManutencaoFormatado) {
		this.tipoCanalManutencaoFormatado = tipoCanalManutencaoFormatado;
	}

	/**
	 * Get: usuarioInclusaoFormatado.
	 * 
	 * @return usuarioInclusaoFormatado
	 */
	public String getUsuarioInclusaoFormatado() {
		return usuarioInclusaoFormatado;
	}

	/**
	 * Set: usuarioInclusaoFormatado.
	 * 
	 * @param usuarioInclusaoFormatado
	 *            the usuario inclusao formatado
	 */
	public void setUsuarioInclusaoFormatado(final String usuarioInclusaoFormatado) {
		this.usuarioInclusaoFormatado = usuarioInclusaoFormatado;
	}

	/**
	 * Get: usuarioManutencaoFormatado.
	 * 
	 * @return usuarioManutencaoFormatado
	 */
	public String getUsuarioManutencaoFormatado() {
		return usuarioManutencaoFormatado;
	}

	/**
	 * Set: usuarioManutencaoFormatado.
	 * 
	 * @param usuarioManutencaoFormatado
	 *            the usuario manutencao formatado
	 */
	public void setUsuarioManutencaoFormatado(final String usuarioManutencaoFormatado) {
		this.usuarioManutencaoFormatado = usuarioManutencaoFormatado;
	}

	/**
	 * Get: dsMoeda.
	 * 
	 * @return dsMoeda
	 */
	public String getDsMoeda() {
		return dsMoeda;
	}

	/**
	 * Set: dsMoeda.
	 * 
	 * @param dsMoeda
	 *            the ds moeda
	 */
	public void setDsMoeda(final String dsMoeda) {
		this.dsMoeda = dsMoeda;
	}

	/**
	 * Get: dsFinalidadeDocTed.
	 * 
	 * @return dsFinalidadeDocTed
	 */
	public String getDsFinalidadeDocTed() {
		return dsFinalidadeDocTed;
	}

	/**
	 * Set: dsFinalidadeDocTed.
	 * 
	 * @param dsFinalidadeDocTed
	 *            the ds finalidade doc ted
	 */
	public void setDsFinalidadeDocTed(final String dsFinalidadeDocTed) {
		this.dsFinalidadeDocTed = dsFinalidadeDocTed;
	}

	/**
	 * Get: dsidentificacaoTitularPagto.
	 * 
	 * @return dsidentificacaoTitularPagto
	 */
	public String getDsidentificacaoTitularPagto() {
		return dsidentificacaoTitularPagto;
	}

	/**
	 * Set: dsidentificacaoTitularPagto.
	 * 
	 * @param dsidentificacaoTitularPagto
	 *            the dsidentificacao titular pagto
	 */
	public void setDsidentificacaoTitularPagto(
			final String dsidentificacaoTitularPagto) {
		this.dsidentificacaoTitularPagto = dsidentificacaoTitularPagto;
	}

	/**
	 * Get: dsIdentificacaoTransferenciaPagto.
	 * 
	 * @return dsIdentificacaoTransferenciaPagto
	 */
	public String getDsIdentificacaoTransferenciaPagto() {
		return dsIdentificacaoTransferenciaPagto;
	}

	/**
	 * Set: dsIdentificacaoTransferenciaPagto.
	 * 
	 * @param dsIdentificacaoTransferenciaPagto
	 *            the ds identificacao transferencia pagto
	 */
	public void setDsIdentificacaoTransferenciaPagto(
			final String dsIdentificacaoTransferenciaPagto) {
		this.dsIdentificacaoTransferenciaPagto = dsIdentificacaoTransferenciaPagto;
	}

	/**
	 * Get: cdIndentificadorJudicial.
	 * 
	 * @return cdIndentificadorJudicial
	 */
	public String getCdIndentificadorJudicial() {
		return cdIndentificadorJudicial;
	}

	/**
	 * Set: cdIndentificadorJudicial.
	 * 
	 * @param cdIndentificadorJudicial
	 *            the cd indentificador judicial
	 */
	public void setCdIndentificadorJudicial(final String cdIndentificadorJudicial) {
		this.cdIndentificadorJudicial = cdIndentificadorJudicial;
	}

	/**
	 * Get: cdAgenciaCredito.
	 * 
	 * @return cdAgenciaCredito
	 */
	public Integer getCdAgenciaCredito() {
		return cdAgenciaCredito;
	}

	/**
	 * Set: cdAgenciaCredito.
	 * 
	 * @param cdAgenciaCredito
	 *            the cd agencia credito
	 */
	public void setCdAgenciaCredito(final Integer cdAgenciaCredito) {
		this.cdAgenciaCredito = cdAgenciaCredito;
	}

	/**
	 * Get: cdAgenciaDebito.
	 * 
	 * @return cdAgenciaDebito
	 */
	public Integer getCdAgenciaDebito() {
		return cdAgenciaDebito;
	}

	/**
	 * Set: cdAgenciaDebito.
	 * 
	 * @param cdAgenciaDebito
	 *            the cd agencia debito
	 */
	public void setCdAgenciaDebito(final Integer cdAgenciaDebito) {
		this.cdAgenciaDebito = cdAgenciaDebito;
	}

	/**
	 * Get: cdBancoCredito.
	 * 
	 * @return cdBancoCredito
	 */
	public Integer getCdBancoCredito() {
		return cdBancoCredito;
	}

	/**
	 * Set: cdBancoCredito.
	 * 
	 * @param cdBancoCredito
	 *            the cd banco credito
	 */
	public void setCdBancoCredito(final Integer cdBancoCredito) {
		this.cdBancoCredito = cdBancoCredito;
	}

	/**
	 * Get: cdBancoDebito.
	 * 
	 * @return cdBancoDebito
	 */
	public Integer getCdBancoDebito() {
		return cdBancoDebito;
	}

	/**
	 * Set: cdBancoDebito.
	 * 
	 * @param cdBancoDebito
	 *            the cd banco debito
	 */
	public void setCdBancoDebito(final Integer cdBancoDebito) {
		this.cdBancoDebito = cdBancoDebito;
	}

	/**
	 * Get: cdContaCredito.
	 * 
	 * @return cdContaCredito
	 */
	public Long getCdContaCredito() {
		return cdContaCredito;
	}

	/**
	 * Set: cdContaCredito.
	 * 
	 * @param cdContaCredito
	 *            the cd conta credito
	 */
	public void setCdContaCredito(final Long cdContaCredito) {
		this.cdContaCredito = cdContaCredito;
	}

	/**
	 * Get: cdContaDebito.
	 * 
	 * @return cdContaDebito
	 */
	public Long getCdContaDebito() {
		return cdContaDebito;
	}

	/**
	 * Set: cdContaDebito.
	 * 
	 * @param cdContaDebito
	 *            the cd conta debito
	 */
	public void setCdContaDebito(final Long cdContaDebito) {
		this.cdContaDebito = cdContaDebito;
	}

	/**
	 * Get: cdControlePagamento.
	 * 
	 * @return cdControlePagamento
	 */
	public String getCdControlePagamento() {
		return cdControlePagamento;
	}

	/**
	 * Set: cdControlePagamento.
	 * 
	 * @param cdControlePagamento
	 *            the cd controle pagamento
	 */
	public void setCdControlePagamento(final String cdControlePagamento) {
		this.cdControlePagamento = cdControlePagamento;
	}

	/**
	 * Get: cdCpfCnpjCliente.
	 * 
	 * @return cdCpfCnpjCliente
	 */
	public Long getCdCpfCnpjCliente() {
		return cdCpfCnpjCliente;
	}

	/**
	 * Set: cdCpfCnpjCliente.
	 * 
	 * @param cdCpfCnpjCliente
	 *            the cd cpf cnpj cliente
	 */
	public void setCdCpfCnpjCliente(final Long cdCpfCnpjCliente) {
		this.cdCpfCnpjCliente = cdCpfCnpjCliente;
	}

	/**
	 * Get: cdDigAgenciaCredito.
	 * 
	 * @return cdDigAgenciaCredito
	 */
	public String getCdDigAgenciaCredito() {
		return cdDigAgenciaCredito;
	}

	/**
	 * Set: cdDigAgenciaCredito.
	 * 
	 * @param cdDigAgenciaCredito
	 *            the cd dig agencia credito
	 */
	public void setCdDigAgenciaCredito(final String cdDigAgenciaCredito) {
		this.cdDigAgenciaCredito = cdDigAgenciaCredito;
	}

	/**
	 * Get: cdDigAgenciaDebto.
	 * 
	 * @return cdDigAgenciaDebto
	 */
	public String getCdDigAgenciaDebto() {
		return cdDigAgenciaDebto;
	}

	/**
	 * Set: cdDigAgenciaDebto.
	 * 
	 * @param cdDigAgenciaDebto
	 *            the cd dig agencia debto
	 */
	public void setCdDigAgenciaDebto(final String cdDigAgenciaDebto) {
		this.cdDigAgenciaDebto = cdDigAgenciaDebto;
	}

	/**
	 * Get: cdDigContaCredito.
	 * 
	 * @return cdDigContaCredito
	 */
	public String getCdDigContaCredito() {
		return cdDigContaCredito;
	}

	/**
	 * Set: cdDigContaCredito.
	 * 
	 * @param cdDigContaCredito
	 *            the cd dig conta credito
	 */
	public void setCdDigContaCredito(final String cdDigContaCredito) {
		this.cdDigContaCredito = cdDigContaCredito;
	}

	/**
	 * Get: cdIndicadorAuto.
	 * 
	 * @return cdIndicadorAuto
	 */
	public Integer getCdIndicadorAuto() {
		return cdIndicadorAuto;
	}

	/**
	 * Set: cdIndicadorAuto.
	 * 
	 * @param cdIndicadorAuto
	 *            the cd indicador auto
	 */
	public void setCdIndicadorAuto(final Integer cdIndicadorAuto) {
		this.cdIndicadorAuto = cdIndicadorAuto;
	}

	/**
	 * Get: cdIndicadorSemConsulta.
	 * 
	 * @return cdIndicadorSemConsulta
	 */
	public Integer getCdIndicadorSemConsulta() {
		return cdIndicadorSemConsulta;
	}

	/**
	 * Set: cdIndicadorSemConsulta.
	 * 
	 * @param cdIndicadorSemConsulta
	 *            the cd indicador sem consulta
	 */
	public void setCdIndicadorSemConsulta(final Integer cdIndicadorSemConsulta) {
		this.cdIndicadorSemConsulta = cdIndicadorSemConsulta;
	}

	/**
	 * Get: cdNomeBeneficio.
	 * 
	 * @return cdNomeBeneficio
	 */
	public String getCdNomeBeneficio() {
		return cdNomeBeneficio;
	}

	/**
	 * Set: cdNomeBeneficio.
	 * 
	 * @param cdNomeBeneficio
	 *            the cd nome beneficio
	 */
	public void setCdNomeBeneficio(final String cdNomeBeneficio) {
		this.cdNomeBeneficio = cdNomeBeneficio;
	}

	/**
	 * Get: cdSituacaoPagamento.
	 * 
	 * @return cdSituacaoPagamento
	 */
	public String getCdSituacaoPagamento() {
		return cdSituacaoPagamento;
	}

	/**
	 * Set: cdSituacaoPagamento.
	 * 
	 * @param cdSituacaoPagamento
	 *            the cd situacao pagamento
	 */
	public void setCdSituacaoPagamento(final String cdSituacaoPagamento) {
		this.cdSituacaoPagamento = cdSituacaoPagamento;
	}

	/**
	 * Get: cdTipoInscriBeneficio.
	 * 
	 * @return cdTipoInscriBeneficio
	 */
	public String getCdTipoInscriBeneficio() {
		return cdTipoInscriBeneficio;
	}

	/**
	 * Set: cdTipoInscriBeneficio.
	 * 
	 * @param cdTipoInscriBeneficio
	 *            the cd tipo inscri beneficio
	 */
	public void setCdTipoInscriBeneficio(final String cdTipoInscriBeneficio) {
		this.cdTipoInscriBeneficio = cdTipoInscriBeneficio;
	}

	/**
	 * Get: dsDigAgenciaDebito.
	 * 
	 * @return dsDigAgenciaDebito
	 */
	public String getDsDigAgenciaDebito() {
		return dsDigAgenciaDebito;
	}

	/**
	 * Set: dsDigAgenciaDebito.
	 * 
	 * @param dsDigAgenciaDebito
	 *            the ds dig agencia debito
	 */
	public void setDsDigAgenciaDebito(final String dsDigAgenciaDebito) {
		this.dsDigAgenciaDebito = dsDigAgenciaDebito;
	}

	/**
	 * Get: dsModalidadeRelacionado.
	 * 
	 * @return dsModalidadeRelacionado
	 */
	public String getDsModalidadeRelacionado() {
		return dsModalidadeRelacionado;
	}

	/**
	 * Set: dsModalidadeRelacionado.
	 * 
	 * @param dsModalidadeRelacionado
	 *            the ds modalidade relacionado
	 */
	public void setDsModalidadeRelacionado(final String dsModalidadeRelacionado) {
		this.dsModalidadeRelacionado = dsModalidadeRelacionado;
	}

	/**
	 * Get: dsNomeFavorecido.
	 * 
	 * @return dsNomeFavorecido
	 */
	public String getDsNomeFavorecido() {
		return dsNomeFavorecido;
	}

	/**
	 * Set: dsNomeFavorecido.
	 * 
	 * @param dsNomeFavorecido
	 *            the ds nome favorecido
	 */
	public void setDsNomeFavorecido(final String dsNomeFavorecido) {
		this.dsNomeFavorecido = dsNomeFavorecido;
	}

	/**
	 * Get: dsPessoaJuridicaContrato.
	 * 
	 * @return dsPessoaJuridicaContrato
	 */
	public String getDsPessoaJuridicaContrato() {
		return dsPessoaJuridicaContrato;
	}

	/**
	 * Set: dsPessoaJuridicaContrato.
	 * 
	 * @param dsPessoaJuridicaContrato
	 *            the ds pessoa juridica contrato
	 */
	public void setDsPessoaJuridicaContrato(final String dsPessoaJuridicaContrato) {
		this.dsPessoaJuridicaContrato = dsPessoaJuridicaContrato;
	}

	/**
	 * Get: dsTipoServicoOperacao.
	 * 
	 * @return dsTipoServicoOperacao
	 */
	public String getDsTipoServicoOperacao() {
		return dsTipoServicoOperacao;
	}

	/**
	 * Set: dsTipoServicoOperacao.
	 * 
	 * @param dsTipoServicoOperacao
	 *            the ds tipo servico operacao
	 */
	public void setDsTipoServicoOperacao(final String dsTipoServicoOperacao) {
		this.dsTipoServicoOperacao = dsTipoServicoOperacao;
	}

	/**
	 * Get: dtPagamento.
	 * 
	 * @return dtPagamento
	 */
	public String getDtPagamento() {
		return dtPagamento;
	}

	/**
	 * Set: dtPagamento.
	 * 
	 * @param dtPagamento
	 *            the dt pagamento
	 */
	public void setDtPagamento(final String dtPagamento) {
		this.dtPagamento = dtPagamento;
	}

	/**
	 * Get: nmInscriBeneficio.
	 * 
	 * @return nmInscriBeneficio
	 */
	public String getNmInscriBeneficio() {
		return nmInscriBeneficio;
	}

	/**
	 * Set: nmInscriBeneficio.
	 * 
	 * @param nmInscriBeneficio
	 *            the nm inscri beneficio
	 */
	public void setNmInscriBeneficio(final String nmInscriBeneficio) {
		this.nmInscriBeneficio = nmInscriBeneficio;
	}

	/**
	 * Get: nmInscriFavorecido.
	 * 
	 * @return nmInscriFavorecido
	 */
	public String getNmInscriFavorecido() {
		return nmInscriFavorecido;
	}

	/**
	 * Set: nmInscriFavorecido.
	 * 
	 * @param nmInscriFavorecido
	 *            the nm inscri favorecido
	 */
	public void setNmInscriFavorecido(final String nmInscriFavorecido) {
		this.nmInscriFavorecido = nmInscriFavorecido;
	}

	/**
	 * Get: nomeCliente.
	 * 
	 * @return nomeCliente
	 */
	public String getNomeCliente() {
		return nomeCliente;
	}

	/**
	 * Set: nomeCliente.
	 * 
	 * @param nomeCliente
	 *            the nome cliente
	 */
	public void setNomeCliente(final String nomeCliente) {
		this.nomeCliente = nomeCliente;
	}

	/**
	 * Get: nrSequenciaContratoNegocio.
	 * 
	 * @return nrSequenciaContratoNegocio
	 */
	public String getNrSequenciaContratoNegocio() {
		return nrSequenciaContratoNegocio;
	}

	/**
	 * Set: nrSequenciaContratoNegocio.
	 * 
	 * @param nrSequenciaContratoNegocio
	 *            the nr sequencia contrato negocio
	 */
	public void setNrSequenciaContratoNegocio(final String nrSequenciaContratoNegocio) {
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}

	/**
	 * Get: agenciaDebitoFormatada.
	 * 
	 * @return agenciaDebitoFormatada
	 */
	public String getAgenciaDebitoFormatada() {
		return agenciaDebitoFormatada;
	}

	/**
	 * Set: agenciaDebitoFormatada.
	 * 
	 * @param agenciaDebitoFormatada
	 *            the agencia debito formatada
	 */
	public void setAgenciaDebitoFormatada(final String agenciaDebitoFormatada) {
		this.agenciaDebitoFormatada = agenciaDebitoFormatada;
	}

	/**
	 * Get: bancoDebitoFormatado.
	 * 
	 * @return bancoDebitoFormatado
	 */
	public String getBancoDebitoFormatado() {
		return bancoDebitoFormatado;
	}

	/**
	 * Set: bancoDebitoFormatado.
	 * 
	 * @param bancoDebitoFormatado
	 *            the banco debito formatado
	 */
	public void setBancoDebitoFormatado(final String bancoDebitoFormatado) {
		this.bancoDebitoFormatado = bancoDebitoFormatado;
	}

	/**
	 * Get: contaDebitoFormatada.
	 * 
	 * @return contaDebitoFormatada
	 */
	public String getContaDebitoFormatada() {
		return contaDebitoFormatada;
	}

	/**
	 * Set: contaDebitoFormatada.
	 * 
	 * @param contaDebitoFormatada
	 *            the conta debito formatada
	 */
	public void setContaDebitoFormatada(final String contaDebitoFormatada) {
		this.contaDebitoFormatada = contaDebitoFormatada;
	}

	/**
	 * Get: numeroInscricaoBeneficiarioFormatado.
	 * 
	 * @return numeroInscricaoBeneficiarioFormatado
	 */
	public String getNumeroInscricaoBeneficiarioFormatado() {
		return numeroInscricaoBeneficiarioFormatado;
	}

	/**
	 * Set: numeroInscricaoBeneficiarioFormatado.
	 * 
	 * @param numeroInscricaoBeneficiarioFormatado
	 *            the numero inscricao beneficiario formatado
	 */
	public void setNumeroInscricaoBeneficiarioFormatado(
			final String numeroInscricaoBeneficiarioFormatado) {
		this.numeroInscricaoBeneficiarioFormatado = numeroInscricaoBeneficiarioFormatado;
	}

	/**
	 * Get: numeroInscricaoFavorecidoFormatado.
	 * 
	 * @return numeroInscricaoFavorecidoFormatado
	 */
	public String getNumeroInscricaoFavorecidoFormatado() {
		return numeroInscricaoFavorecidoFormatado;
	}

	/**
	 * Set: numeroInscricaoFavorecidoFormatado.
	 * 
	 * @param numeroInscricaoFavorecidoFormatado
	 *            the numero inscricao favorecido formatado
	 */
	public void setNumeroInscricaoFavorecidoFormatado(
			final String numeroInscricaoFavorecidoFormatado) {
		this.numeroInscricaoFavorecidoFormatado = numeroInscricaoFavorecidoFormatado;
	}

	/**
	 * Get: cpfCnpjClienteFormatado.
	 * 
	 * @return cpfCnpjClienteFormatado
	 */
	public String getCpfCnpjClienteFormatado() {
		return cpfCnpjClienteFormatado;
	}

	/**
	 * Set: cpfCnpjClienteFormatado.
	 * 
	 * @param cpfCnpjClienteFormatado
	 *            the cpf cnpj cliente formatado
	 */
	public void setCpfCnpjClienteFormatado(final String cpfCnpjClienteFormatado) {
		this.cpfCnpjClienteFormatado = cpfCnpjClienteFormatado;
	}

	/**
	 * Get: agenciaCreditoFormatada.
	 * 
	 * @return agenciaCreditoFormatada
	 */
	public String getAgenciaCreditoFormatada() {
		return agenciaCreditoFormatada;
	}

	/**
	 * Set: agenciaCreditoFormatada.
	 * 
	 * @param agenciaCreditoFormatada
	 *            the agencia credito formatada
	 */
	public void setAgenciaCreditoFormatada(final String agenciaCreditoFormatada) {
		this.agenciaCreditoFormatada = agenciaCreditoFormatada;
	}

	/**
	 * Get: bancoCreditoFormatado.
	 * 
	 * @return bancoCreditoFormatado
	 */
	public String getBancoCreditoFormatado() {
		return bancoCreditoFormatado;
	}

	/**
	 * Set: bancoCreditoFormatado.
	 * 
	 * @param bancoCreditoFormatado
	 *            the banco credito formatado
	 */
	public void setBancoCreditoFormatado(final String bancoCreditoFormatado) {
		this.bancoCreditoFormatado = bancoCreditoFormatado;
	}

	/**
	 * Get: contaCreditoFormatada.
	 * 
	 * @return contaCreditoFormatada
	 */
	public String getContaCreditoFormatada() {
		return contaCreditoFormatada;
	}

	/**
	 * Set: contaCreditoFormatada.
	 * 
	 * @param contaCreditoFormatada
	 *            the conta credito formatada
	 */
	public void setContaCreditoFormatada(final String contaCreditoFormatada) {
		this.contaCreditoFormatada = contaCreditoFormatada;
	}

	/**
	 * Get: dtDevolucaoEstorno.
	 * 
	 * @return dtDevolucaoEstorno
	 */
	public String getDtDevolucaoEstorno() {
		return dtDevolucaoEstorno;
	}

	/**
	 * Set: dtDevolucaoEstorno.
	 * 
	 * @param dtDevolucaoEstorno
	 *            the dt devolucao estorno
	 */
	public void setDtDevolucaoEstorno(final String dtDevolucaoEstorno) {
		this.dtDevolucaoEstorno = dtDevolucaoEstorno;
	}

	/**
	 * Get: dtFloatingPagamento.
	 * 
	 * @return dtFloatingPagamento
	 */
	public String getDtFloatingPagamento() {
		return dtFloatingPagamento;
	}

	/**
	 * Set: dtFloatingPagamento.
	 * 
	 * @param dtFloatingPagamento
	 *            the dt floating pagamento
	 */
	public void setDtFloatingPagamento(final String dtFloatingPagamento) {
		this.dtFloatingPagamento = dtFloatingPagamento;
	}

	/**
	 * Get: dtEfetivFloatPgto.
	 * 
	 * @return dtEfetivFloatPgto
	 */
	public String getDtEfetivFloatPgto() {
		return dtEfetivFloatPgto;
	}

	/**
	 * Set: dtEfetivFloatPgto.
	 * 
	 * @param dtEfetivFloatPgto
	 *            the dt efetiv float pgto
	 */
	public void setDtEfetivFloatPgto(final String dtEfetivFloatPgto) {
		this.dtEfetivFloatPgto = dtEfetivFloatPgto;
	}

	/**
	 * Get: vlFloatingPagamento.
	 * 
	 * @return vlFloatingPagamento
	 */
	public BigDecimal getVlFloatingPagamento() {
		return vlFloatingPagamento;
	}

	/**
	 * Set: vlFloatingPagamento.
	 * 
	 * @param vlFloatingPagamento
	 *            the vl floating pagamento
	 */
	public void setVlFloatingPagamento(final BigDecimal vlFloatingPagamento) {
		this.vlFloatingPagamento = vlFloatingPagamento;
	}

	/**
	 * Get: cdOperacaoDcom.
	 * 
	 * @return cdOperacaoDcom
	 */
	public Long getCdOperacaoDcom() {
		return cdOperacaoDcom;
	}

	/**
	 * Set: cdOperacaoDcom.
	 * 
	 * @param cdOperacaoDcom
	 *            the cd operacao dcom
	 */
	public void setCdOperacaoDcom(final Long cdOperacaoDcom) {
		this.cdOperacaoDcom = cdOperacaoDcom;
	}

	/**
	 * Get: dsSituacaoDcom.
	 * 
	 * @return dsSituacaoDcom
	 */
	public String getDsSituacaoDcom() {
		return dsSituacaoDcom;
	}

	/**
	 * Set: dsSituacaoDcom.
	 * 
	 * @param dsSituacaoDcom
	 *            the ds situacao dcom
	 */
	public void setDsSituacaoDcom(final String dsSituacaoDcom) {
		this.dsSituacaoDcom = dsSituacaoDcom;
	}

	/**
	 * Get: finalidadeTedFormatada.
	 *
	 * @return finalidadeTedFormatada
	 */
	public String getFinalidadeTedFormatada() {
		if (cdFinalidadeTed == null || dsFinalidadeDocTed == null) {
			return "";
		}
		return String.format("%s - %s", cdFinalidadeTed, dsFinalidadeDocTed);
	}

	/**
	 * Get: cdLoteInterno.
	 *
	 * @return the cdLoteInterno
	 */
	public Long getCdLoteInterno() {
		return cdLoteInterno;
	}

	/**
	 * Set: cdLoteInterno.
	 *
	 * @param cdLoteInterno the cdLoteInterno to set
	 */
	public void setCdLoteInterno(final Long cdLoteInterno) {
		this.cdLoteInterno = cdLoteInterno;
	}

	/**
	 * Get: dsIndicadorAutorizacao.
	 *
	 * @return the dsIndicadorAutorizacao
	 */
	public String getDsIndicadorAutorizacao() {
		return dsIndicadorAutorizacao;
	}

	/**
	 * Set: dsIndicadorAutorizacao.
	 *
	 * @param dsIndicadorAutorizacao the dsIndicadorAutorizacao to set
	 */
	public void setDsIndicadorAutorizacao(final String dsIndicadorAutorizacao) {
		this.dsIndicadorAutorizacao = dsIndicadorAutorizacao;
	}

	/**
	 * Get: dsTipoLayout.
	 *
	 * @return dsTipoLayout
	 */
	public String getDsTipoLayout() {
		return dsTipoLayout;
	}

	/**
	 * Set: dsTipoLayout.
	 *
	 * @param dsTipoLayout the ds tipo layout
	 */
	public void setDsTipoLayout(final String dsTipoLayout) {
		this.dsTipoLayout = dsTipoLayout;
	}

	/**
	 * Set: cdIspbPagamento.
	 *
	 * @param cdIspbPagamento the cdIspbPagamento to set
	 */
	public void setCdIspbPagamento(String cdIspbPagamento) {
		this.cdIspbPagamento = cdIspbPagamento;
	}

	/**
	 * Get: cdIspbPagamento.
	 *
	 * @return the cdIspbPagamento
	 */
	public String getCdIspbPagamento() {
		return cdIspbPagamento;
	}

	/**
	 * Set: dsBancoCredito.
	 *
	 * @param dsBancoCredito the dsBancoCredito to set
	 */
	public void setDsBancoCredito(String dsBancoCredito) {
		this.dsBancoCredito = dsBancoCredito;
	}

	/**
	 * Get: dsBancoCredito.
	 *
	 * @return the dsBancoCredito
	 */
	public String getDsBancoCredito() {
		return dsBancoCredito;
	}

    /**
     * Get: dsNomeReclamante.
     *
     * @return dsNomeReclamante
     */
    public String getDsNomeReclamante() {
        return dsNomeReclamante;
    }

    /**
     * Set: dsNomeReclamante.
     *
     * @param dsNomeReclamante the ds nome reclamante
     */
    public void setDsNomeReclamante(String dsNomeReclamante) {
        this.dsNomeReclamante = dsNomeReclamante;
    }

    /**
     * Get: nrProcessoDepositoJudicial.
     *
     * @return nrProcessoDepositoJudicial
     */
    public String getNrProcessoDepositoJudicial() {
        return nrProcessoDepositoJudicial;
    }

    /**
     * Set: nrProcessoDepositoJudicial.
     *
     * @param nrProcessoDepositoJudicial the nr processo deposito judicial
     */
    public void setNrProcessoDepositoJudicial(String nrProcessoDepositoJudicial) {
        this.nrProcessoDepositoJudicial = nrProcessoDepositoJudicial;
    }

    /**
     * Get: nrPisPasep.
     *
     * @return nrPisPasep
     */
    public Long getNrPisPasep() {
        return nrPisPasep;
    }

    /**
     * Set: nrPisPasep.
     *
     * @param nrPisPasep the nr pis pasep
     */
    public void setNrPisPasep(Long nrPisPasep) {
        this.nrPisPasep = nrPisPasep;
    }
    
    /**
     * Is cd finalidade ted deposito judicial.
     *
     * @return true, if is cd finalidade ted deposito judicial
     */
    public boolean isCdFinalidadeTedDepositoJudicial() {
        Integer codigoFinalidadeConvertido = NumberUtils.createInteger(cdFinalidadeTed);
        return codigoFinalidadeConvertido != null && codigoFinalidadeConvertido == 100;
    }

}