/*
 * Nome: br.com.bradesco.web.pgit.service.business.consolconsistenciaprevcredconta.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.consolconsistenciaprevcredconta.bean;

import java.math.BigDecimal;

/**
 * Nome: ConsultarSolicitacaoConsistPreviaSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarSolicitacaoConsistPreviaSaidaDTO {
	
	/** Atributo codMensagem. */
	private String codMensagem;
	
	/** Atributo mensagem. */
	private String mensagem;
	
	/** Atributo numeroConsultas. */
	private Integer numeroConsultas;
	
	/** Atributo cdConsistenciaPreviaPagamento. */
	private Integer cdConsistenciaPreviaPagamento;
	
	/** Atributo cdInscricaoPagador. */
	private Long cdInscricaoPagador;
	
	/** Atributo dsRazaoSocial. */
	private String dsRazaoSocial;
	
	/** Atributo cdPessoaJuridicaContrato. */
	private Long cdPessoaJuridicaContrato;
	
	/** Atributo cdTipoContratoNegocio. */
	private Integer cdTipoContratoNegocio;
	
	/** Atributo nrSequenciaContratoNegocio. */
	private Long nrSequenciaContratoNegocio;
	
	/** Atributo hrInclusaoPagamento. */
	private String hrInclusaoPagamento;
	
	/** Atributo cdSituacaoLotePagamento. */
	private String cdSituacaoLotePagamento;
	
	/** Atributo dtGravacaoLote. */
	private String dtGravacaoLote;
	
	/** Atributo cdTipoLote. */
	private Integer cdTipoLote;
	
	/** Atributo nrLoteArquivoRemessa. */
	private Long nrLoteArquivoRemessa;
	
	/** Atributo cdControleTransacao. */
	private Long cdControleTransacao;
	
	/** Atributo dtProcessoLote. */
	private String dtProcessoLote;
	
	/** Atributo cdVolumeTotal. */
	private Long cdVolumeTotal;
	
	/** Atributo vlTotal. */
	private BigDecimal vlTotal;
	
	/** Atributo cdSituacaoSolicitacao. */
	private Integer cdSituacaoSolicitacao;
    
    /** Atributo dsSituacaoSolicitacao. */
    private String dsSituacaoSolicitacao;
    
    /** Atributo dsSituacaoLotePagamento. */
    private String dsSituacaoLotePagamento;
	
	/**
	 * Get: dsSituacaoLotePagamento.
	 *
	 * @return dsSituacaoLotePagamento
	 */
	public String getDsSituacaoLotePagamento() {
		return dsSituacaoLotePagamento;
	}
	
	/**
	 * Set: dsSituacaoLotePagamento.
	 *
	 * @param dsSituacaoLotePagamento the ds situacao lote pagamento
	 */
	public void setDsSituacaoLotePagamento(String dsSituacaoLotePagamento) {
		this.dsSituacaoLotePagamento = dsSituacaoLotePagamento;
	}
	
	/**
	 * Get: cdConsistenciaPreviaPagamento.
	 *
	 * @return cdConsistenciaPreviaPagamento
	 */
	public Integer getCdConsistenciaPreviaPagamento() {
		return cdConsistenciaPreviaPagamento;
	}
	
	/**
	 * Set: cdConsistenciaPreviaPagamento.
	 *
	 * @param cdConsistenciaPreviaPagamento the cd consistencia previa pagamento
	 */
	public void setCdConsistenciaPreviaPagamento(
			Integer cdConsistenciaPreviaPagamento) {
		this.cdConsistenciaPreviaPagamento = cdConsistenciaPreviaPagamento;
	}
	
	/**
	 * Get: cdControleTransacao.
	 *
	 * @return cdControleTransacao
	 */
	public Long getCdControleTransacao() {
		return cdControleTransacao;
	}
	
	/**
	 * Set: cdControleTransacao.
	 *
	 * @param cdControleTransacao the cd controle transacao
	 */
	public void setCdControleTransacao(Long cdControleTransacao) {
		this.cdControleTransacao = cdControleTransacao;
	}
	
	/**
	 * Get: cdInscricaoPagador.
	 *
	 * @return cdInscricaoPagador
	 */
	public Long getCdInscricaoPagador() {
		return cdInscricaoPagador;
	}
	
	/**
	 * Set: cdInscricaoPagador.
	 *
	 * @param cdInscricaoPagador the cd inscricao pagador
	 */
	public void setCdInscricaoPagador(Long cdInscricaoPagador) {
		this.cdInscricaoPagador = cdInscricaoPagador;
	}
	
	/**
	 * Get: cdPessoaJuridicaContrato.
	 *
	 * @return cdPessoaJuridicaContrato
	 */
	public Long getCdPessoaJuridicaContrato() {
		return cdPessoaJuridicaContrato;
	}
	
	/**
	 * Set: cdPessoaJuridicaContrato.
	 *
	 * @param cdPessoaJuridicaContrato the cd pessoa juridica contrato
	 */
	public void setCdPessoaJuridicaContrato(Long cdPessoaJuridicaContrato) {
		this.cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
	}
	
	/**
	 * Get: cdSituacaoLotePagamento.
	 *
	 * @return cdSituacaoLotePagamento
	 */
	public String getCdSituacaoLotePagamento() {
		return cdSituacaoLotePagamento;
	}
	
	/**
	 * Set: cdSituacaoLotePagamento.
	 *
	 * @param cdSituacaoLotePagamento the cd situacao lote pagamento
	 */
	public void setCdSituacaoLotePagamento(String cdSituacaoLotePagamento) {
		this.cdSituacaoLotePagamento = cdSituacaoLotePagamento;
	}
	
	/**
	 * Get: cdTipoContratoNegocio.
	 *
	 * @return cdTipoContratoNegocio
	 */
	public Integer getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}
	
	/**
	 * Set: cdTipoContratoNegocio.
	 *
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}
	
	/**
	 * Get: cdTipoLote.
	 *
	 * @return cdTipoLote
	 */
	public Integer getCdTipoLote() {
		return cdTipoLote;
	}
	
	/**
	 * Set: cdTipoLote.
	 *
	 * @param cdTipoLote the cd tipo lote
	 */
	public void setCdTipoLote(Integer cdTipoLote) {
		this.cdTipoLote = cdTipoLote;
	}
	
	/**
	 * Get: cdVolumeTotal.
	 *
	 * @return cdVolumeTotal
	 */
	public Long getCdVolumeTotal() {
		return cdVolumeTotal;
	}
	
	/**
	 * Set: cdVolumeTotal.
	 *
	 * @param cdVolumeTotal the cd volume total
	 */
	public void setCdVolumeTotal(Long cdVolumeTotal) {
		this.cdVolumeTotal = cdVolumeTotal;
	}
	
	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}
	
	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}
	
	/**
	 * Get: dsRazaoSocial.
	 *
	 * @return dsRazaoSocial
	 */
	public String getDsRazaoSocial() {
		return dsRazaoSocial;
	}
	
	/**
	 * Set: dsRazaoSocial.
	 *
	 * @param dsRazaoSocial the ds razao social
	 */
	public void setDsRazaoSocial(String dsRazaoSocial) {
		this.dsRazaoSocial = dsRazaoSocial;
	}
	
	/**
	 * Get: dtGravacaoLote.
	 *
	 * @return dtGravacaoLote
	 */
	public String getDtGravacaoLote() {
		return dtGravacaoLote;
	}
	
	/**
	 * Set: dtGravacaoLote.
	 *
	 * @param dtGravacaoLote the dt gravacao lote
	 */
	public void setDtGravacaoLote(String dtGravacaoLote) {
		this.dtGravacaoLote = dtGravacaoLote;
	}
	
	/**
	 * Get: dtProcessoLote.
	 *
	 * @return dtProcessoLote
	 */
	public String getDtProcessoLote() {
		return dtProcessoLote;
	}
	
	/**
	 * Set: dtProcessoLote.
	 *
	 * @param dtProcessoLote the dt processo lote
	 */
	public void setDtProcessoLote(String dtProcessoLote) {
		this.dtProcessoLote = dtProcessoLote;
	}
	
	/**
	 * Get: hrInclusaoPagamento.
	 *
	 * @return hrInclusaoPagamento
	 */
	public String getHrInclusaoPagamento() {
		return hrInclusaoPagamento;
	}
	
	/**
	 * Set: hrInclusaoPagamento.
	 *
	 * @param hrInclusaoPagamento the hr inclusao pagamento
	 */
	public void setHrInclusaoPagamento(String hrInclusaoPagamento) {
		this.hrInclusaoPagamento = hrInclusaoPagamento;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	/**
	 * Get: nrLoteArquivoRemessa.
	 *
	 * @return nrLoteArquivoRemessa
	 */
	public Long getNrLoteArquivoRemessa() {
		return nrLoteArquivoRemessa;
	}
	
	/**
	 * Set: nrLoteArquivoRemessa.
	 *
	 * @param nrLoteArquivoRemessa the nr lote arquivo remessa
	 */
	public void setNrLoteArquivoRemessa(Long nrLoteArquivoRemessa) {
		this.nrLoteArquivoRemessa = nrLoteArquivoRemessa;
	}
	
	/**
	 * Get: nrSequenciaContratoNegocio.
	 *
	 * @return nrSequenciaContratoNegocio
	 */
	public Long getNrSequenciaContratoNegocio() {
		return nrSequenciaContratoNegocio;
	}
	
	/**
	 * Set: nrSequenciaContratoNegocio.
	 *
	 * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
	 */
	public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}
	
	/**
	 * Get: numeroConsultas.
	 *
	 * @return numeroConsultas
	 */
	public Integer getNumeroConsultas() {
		return numeroConsultas;
	}
	
	/**
	 * Set: numeroConsultas.
	 *
	 * @param numeroConsultas the numero consultas
	 */
	public void setNumeroConsultas(Integer numeroConsultas) {
		this.numeroConsultas = numeroConsultas;
	}
	
	/**
	 * Get: vlTotal.
	 *
	 * @return vlTotal
	 */
	public BigDecimal getVlTotal() {
		return vlTotal;
	}
	
	/**
	 * Set: vlTotal.
	 *
	 * @param vlTotal the vl total
	 */
	public void setVlTotal(BigDecimal vlTotal) {
		this.vlTotal = vlTotal;
	}
	
	/**
	 * Get: cdSituacaoSolicitacao.
	 *
	 * @return cdSituacaoSolicitacao
	 */
	public Integer getCdSituacaoSolicitacao() {
		return cdSituacaoSolicitacao;
	}
	
	/**
	 * Set: cdSituacaoSolicitacao.
	 *
	 * @param cdSituacaoSolicitacao the cd situacao solicitacao
	 */
	public void setCdSituacaoSolicitacao(Integer cdSituacaoSolicitacao) {
		this.cdSituacaoSolicitacao = cdSituacaoSolicitacao;
	}
	
	/**
	 * Get: dsSituacaoSolicitacao.
	 *
	 * @return dsSituacaoSolicitacao
	 */
	public String getDsSituacaoSolicitacao() {
		return dsSituacaoSolicitacao;
	}
	
	/**
	 * Set: dsSituacaoSolicitacao.
	 *
	 * @param dsSituacaoSolicitacao the ds situacao solicitacao
	 */
	public void setDsSituacaoSolicitacao(String dsSituacaoSolicitacao) {
		this.dsSituacaoSolicitacao = dsSituacaoSolicitacao;
	}
	
	
}
