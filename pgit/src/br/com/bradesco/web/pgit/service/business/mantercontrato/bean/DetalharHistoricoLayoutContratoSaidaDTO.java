/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantercontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantercontrato.bean;

/**
 * Nome: DetalharHistoricoLayoutContratoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class DetalharHistoricoLayoutContratoSaidaDTO {

	/** Atributo codMensagem. */
	private String codMensagem;
    
    /** Atributo mensagem. */
    private String mensagem;
    
    /** Atributo cdTipoLayoutArquivo. */
    private Integer cdTipoLayoutArquivo;
    
    /** Atributo dsTipoLayoutArquivo. */
    private String dsTipoLayoutArquivo;
    
    /** Atributo cdArquivoRetornoPagamento. */
    private Integer cdArquivoRetornoPagamento;
    
    /** Atributo dsArquivoRetornoPagamento. */
    private String dsArquivoRetornoPagamento;
    
    /** Atributo cdTipoGeracaoRetorno. */
    private Integer cdTipoGeracaoRetorno;
    
    /** Atributo dsTipoGeracaoRetorno. */
    private String dsTipoGeracaoRetorno;
    
    /** Atributo cdTipoMontaRetorno. */
    private Integer cdTipoMontaRetorno;
    
    /** Atributo dsTipoMontaRetorno. */
    private String dsTipoMontaRetorno;
    
    /** Atributo cdTipoOcorRetorno. */
    private Integer cdTipoOcorRetorno;
    
    /** Atributo dsTipoOcorRetorno. */
    private String dsTipoOcorRetorno;
    
    /** Atributo cdTipoOrdemRegistroRetorno. */
    private Integer cdTipoOrdemRegistroRetorno;
    
    /** Atributo dsTipoOrdemRegistroRetorno. */
    private String dsTipoOrdemRegistroRetorno;
    
    /** Atributo cdTipoClassificacaoRetorno. */
    private Integer cdTipoClassificacaoRetorno;
    
    /** Atributo dsTipoClassificacaoRetorno. */
    private String dsTipoClassificacaoRetorno;
    
    /** Atributo cdIndicadorTipoManutencao. */
    private Integer cdIndicadorTipoManutencao;
    
    /** Atributo dsTipoManutencao. */
    private String dsTipoManutencao;
    
    /** Atributo cdUsuarioInclusao. */
    private String cdUsuarioInclusao;
    
    /** Atributo cdUsuarioInclusaoExterno. */
    private String cdUsuarioInclusaoExterno;
    
    /** Atributo hrInclusaoRegistro. */
    private String hrInclusaoRegistro;
    
    /** Atributo cdOperacaoCanalInclusao. */
    private String cdOperacaoCanalInclusao;
    
    /** Atributo cdTipoCanalInclusao. */
    private Integer cdTipoCanalInclusao;
    
    /** Atributo dsCanalInclusao. */
    private String dsCanalInclusao;
    
    /** Atributo cdUsuarioManutencao. */
    private String cdUsuarioManutencao;
    
    /** Atributo cdUsuarioManutencaoExterno. */
    private String cdUsuarioManutencaoExterno;
    
    /** Atributo hrManutencaoRegistro. */
    private String hrManutencaoRegistro;
    
    /** Atributo cdOperacaoCanalManutencao. */
    private String cdOperacaoCanalManutencao;
    
    /** Atributo cdTipoCanalManutencao. */
    private Integer cdTipoCanalManutencao;
    
    /** Atributo dsCanalManutencao. */
    private String dsCanalManutencao;
    
    /** Atributo usuarioInclusaoFormatado. */
    private String usuarioInclusaoFormatado;
    
    /** Atributo usuarioManutencaoFormatado. */
    private String usuarioManutencaoFormatado;
    
    /** Atributo tipoCanalInclusaoFormatado. */
    private String tipoCanalInclusaoFormatado;
    
    /** Atributo tipoCanalManutencaoFormatado. */
    private String tipoCanalManutencaoFormatado;
    
	/**
	 * Get: cdArquivoRetornoPagamento.
	 *
	 * @return cdArquivoRetornoPagamento
	 */
	public Integer getCdArquivoRetornoPagamento() {
		return cdArquivoRetornoPagamento;
	}
	
	/**
	 * Set: cdArquivoRetornoPagamento.
	 *
	 * @param cdArquivoRetornoPagamento the cd arquivo retorno pagamento
	 */
	public void setCdArquivoRetornoPagamento(Integer cdArquivoRetornoPagamento) {
		this.cdArquivoRetornoPagamento = cdArquivoRetornoPagamento;
	}
	
	/**
	 * Get: cdIndicadorTipoManutencao.
	 *
	 * @return cdIndicadorTipoManutencao
	 */
	public Integer getCdIndicadorTipoManutencao() {
		return cdIndicadorTipoManutencao;
	}
	
	/**
	 * Set: cdIndicadorTipoManutencao.
	 *
	 * @param cdIndicadorTipoManutencao the cd indicador tipo manutencao
	 */
	public void setCdIndicadorTipoManutencao(Integer cdIndicadorTipoManutencao) {
		this.cdIndicadorTipoManutencao = cdIndicadorTipoManutencao;
	}
	
	/**
	 * Get: cdOperacaoCanalInclusao.
	 *
	 * @return cdOperacaoCanalInclusao
	 */
	public String getCdOperacaoCanalInclusao() {
		return cdOperacaoCanalInclusao;
	}
	
	/**
	 * Set: cdOperacaoCanalInclusao.
	 *
	 * @param cdOperacaoCanalInclusao the cd operacao canal inclusao
	 */
	public void setCdOperacaoCanalInclusao(String cdOperacaoCanalInclusao) {
		this.cdOperacaoCanalInclusao = cdOperacaoCanalInclusao;
	}
	
	/**
	 * Get: cdOperacaoCanalManutencao.
	 *
	 * @return cdOperacaoCanalManutencao
	 */
	public String getCdOperacaoCanalManutencao() {
		return cdOperacaoCanalManutencao;
	}
	
	/**
	 * Set: cdOperacaoCanalManutencao.
	 *
	 * @param cdOperacaoCanalManutencao the cd operacao canal manutencao
	 */
	public void setCdOperacaoCanalManutencao(String cdOperacaoCanalManutencao) {
		this.cdOperacaoCanalManutencao = cdOperacaoCanalManutencao;
	}
	
	/**
	 * Get: cdTipoCanalInclusao.
	 *
	 * @return cdTipoCanalInclusao
	 */
	public Integer getCdTipoCanalInclusao() {
		return cdTipoCanalInclusao;
	}
	
	/**
	 * Set: cdTipoCanalInclusao.
	 *
	 * @param cdTipoCanalInclusao the cd tipo canal inclusao
	 */
	public void setCdTipoCanalInclusao(Integer cdTipoCanalInclusao) {
		this.cdTipoCanalInclusao = cdTipoCanalInclusao;
	}
	
	/**
	 * Get: cdTipoCanalManutencao.
	 *
	 * @return cdTipoCanalManutencao
	 */
	public Integer getCdTipoCanalManutencao() {
		return cdTipoCanalManutencao;
	}
	
	/**
	 * Set: cdTipoCanalManutencao.
	 *
	 * @param cdTipoCanalManutencao the cd tipo canal manutencao
	 */
	public void setCdTipoCanalManutencao(Integer cdTipoCanalManutencao) {
		this.cdTipoCanalManutencao = cdTipoCanalManutencao;
	}
	
	/**
	 * Get: cdTipoClassificacaoRetorno.
	 *
	 * @return cdTipoClassificacaoRetorno
	 */
	public Integer getCdTipoClassificacaoRetorno() {
		return cdTipoClassificacaoRetorno;
	}
	
	/**
	 * Set: cdTipoClassificacaoRetorno.
	 *
	 * @param cdTipoClassificacaoRetorno the cd tipo classificacao retorno
	 */
	public void setCdTipoClassificacaoRetorno(Integer cdTipoClassificacaoRetorno) {
		this.cdTipoClassificacaoRetorno = cdTipoClassificacaoRetorno;
	}
	
	/**
	 * Get: cdTipoGeracaoRetorno.
	 *
	 * @return cdTipoGeracaoRetorno
	 */
	public Integer getCdTipoGeracaoRetorno() {
		return cdTipoGeracaoRetorno;
	}
	
	/**
	 * Set: cdTipoGeracaoRetorno.
	 *
	 * @param cdTipoGeracaoRetorno the cd tipo geracao retorno
	 */
	public void setCdTipoGeracaoRetorno(Integer cdTipoGeracaoRetorno) {
		this.cdTipoGeracaoRetorno = cdTipoGeracaoRetorno;
	}
	
	/**
	 * Get: cdTipoLayoutArquivo.
	 *
	 * @return cdTipoLayoutArquivo
	 */
	public Integer getCdTipoLayoutArquivo() {
		return cdTipoLayoutArquivo;
	}
	
	/**
	 * Set: cdTipoLayoutArquivo.
	 *
	 * @param cdTipoLayoutArquivo the cd tipo layout arquivo
	 */
	public void setCdTipoLayoutArquivo(Integer cdTipoLayoutArquivo) {
		this.cdTipoLayoutArquivo = cdTipoLayoutArquivo;
	}
	
	/**
	 * Get: cdTipoMontaRetorno.
	 *
	 * @return cdTipoMontaRetorno
	 */
	public Integer getCdTipoMontaRetorno() {
		return cdTipoMontaRetorno;
	}
	
	/**
	 * Set: cdTipoMontaRetorno.
	 *
	 * @param cdTipoMontaRetorno the cd tipo monta retorno
	 */
	public void setCdTipoMontaRetorno(Integer cdTipoMontaRetorno) {
		this.cdTipoMontaRetorno = cdTipoMontaRetorno;
	}
	
	/**
	 * Get: cdTipoOcorRetorno.
	 *
	 * @return cdTipoOcorRetorno
	 */
	public Integer getCdTipoOcorRetorno() {
		return cdTipoOcorRetorno;
	}
	
	/**
	 * Set: cdTipoOcorRetorno.
	 *
	 * @param cdTipoOcorRetorno the cd tipo ocor retorno
	 */
	public void setCdTipoOcorRetorno(Integer cdTipoOcorRetorno) {
		this.cdTipoOcorRetorno = cdTipoOcorRetorno;
	}
	
	/**
	 * Get: cdTipoOrdemRegistroRetorno.
	 *
	 * @return cdTipoOrdemRegistroRetorno
	 */
	public Integer getCdTipoOrdemRegistroRetorno() {
		return cdTipoOrdemRegistroRetorno;
	}
	
	/**
	 * Set: cdTipoOrdemRegistroRetorno.
	 *
	 * @param cdTipoOrdemRegistroRetorno the cd tipo ordem registro retorno
	 */
	public void setCdTipoOrdemRegistroRetorno(Integer cdTipoOrdemRegistroRetorno) {
		this.cdTipoOrdemRegistroRetorno = cdTipoOrdemRegistroRetorno;
	}
	
	/**
	 * Get: cdUsuarioInclusao.
	 *
	 * @return cdUsuarioInclusao
	 */
	public String getCdUsuarioInclusao() {
		return cdUsuarioInclusao;
	}
	
	/**
	 * Set: cdUsuarioInclusao.
	 *
	 * @param cdUsuarioInclusao the cd usuario inclusao
	 */
	public void setCdUsuarioInclusao(String cdUsuarioInclusao) {
		this.cdUsuarioInclusao = cdUsuarioInclusao;
	}
	
	/**
	 * Get: cdUsuarioInclusaoExterno.
	 *
	 * @return cdUsuarioInclusaoExterno
	 */
	public String getCdUsuarioInclusaoExterno() {
		return cdUsuarioInclusaoExterno;
	}
	
	/**
	 * Set: cdUsuarioInclusaoExterno.
	 *
	 * @param cdUsuarioInclusaoExterno the cd usuario inclusao externo
	 */
	public void setCdUsuarioInclusaoExterno(String cdUsuarioInclusaoExterno) {
		this.cdUsuarioInclusaoExterno = cdUsuarioInclusaoExterno;
	}
	
	/**
	 * Get: cdUsuarioManutencao.
	 *
	 * @return cdUsuarioManutencao
	 */
	public String getCdUsuarioManutencao() {
		return cdUsuarioManutencao;
	}
	
	/**
	 * Set: cdUsuarioManutencao.
	 *
	 * @param cdUsuarioManutencao the cd usuario manutencao
	 */
	public void setCdUsuarioManutencao(String cdUsuarioManutencao) {
		this.cdUsuarioManutencao = cdUsuarioManutencao;
	}
	
	/**
	 * Get: cdUsuarioManutencaoExterno.
	 *
	 * @return cdUsuarioManutencaoExterno
	 */
	public String getCdUsuarioManutencaoExterno() {
		return cdUsuarioManutencaoExterno;
	}
	
	/**
	 * Set: cdUsuarioManutencaoExterno.
	 *
	 * @param cdUsuarioManutencaoExterno the cd usuario manutencao externo
	 */
	public void setCdUsuarioManutencaoExterno(String cdUsuarioManutencaoExterno) {
		this.cdUsuarioManutencaoExterno = cdUsuarioManutencaoExterno;
	}
	
	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}
	
	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}
	
	/**
	 * Get: dsArquivoRetornoPagamento.
	 *
	 * @return dsArquivoRetornoPagamento
	 */
	public String getDsArquivoRetornoPagamento() {
		return dsArquivoRetornoPagamento;
	}
	
	/**
	 * Set: dsArquivoRetornoPagamento.
	 *
	 * @param dsArquivoRetornoPagamento the ds arquivo retorno pagamento
	 */
	public void setDsArquivoRetornoPagamento(String dsArquivoRetornoPagamento) {
		this.dsArquivoRetornoPagamento = dsArquivoRetornoPagamento;
	}
	
	/**
	 * Get: dsCanalInclusao.
	 *
	 * @return dsCanalInclusao
	 */
	public String getDsCanalInclusao() {
		return dsCanalInclusao;
	}
	
	/**
	 * Set: dsCanalInclusao.
	 *
	 * @param dsCanalInclusao the ds canal inclusao
	 */
	public void setDsCanalInclusao(String dsCanalInclusao) {
		this.dsCanalInclusao = dsCanalInclusao;
	}
	
	/**
	 * Get: dsCanalManutencao.
	 *
	 * @return dsCanalManutencao
	 */
	public String getDsCanalManutencao() {
		return dsCanalManutencao;
	}
	
	/**
	 * Set: dsCanalManutencao.
	 *
	 * @param dsCanalManutencao the ds canal manutencao
	 */
	public void setDsCanalManutencao(String dsCanalManutencao) {
		this.dsCanalManutencao = dsCanalManutencao;
	}
	
	/**
	 * Get: dsTipoClassificacaoRetorno.
	 *
	 * @return dsTipoClassificacaoRetorno
	 */
	public String getDsTipoClassificacaoRetorno() {
		return dsTipoClassificacaoRetorno;
	}
	
	/**
	 * Set: dsTipoClassificacaoRetorno.
	 *
	 * @param dsTipoClassificacaoRetorno the ds tipo classificacao retorno
	 */
	public void setDsTipoClassificacaoRetorno(String dsTipoClassificacaoRetorno) {
		this.dsTipoClassificacaoRetorno = dsTipoClassificacaoRetorno;
	}
	
	/**
	 * Get: dsTipoGeracaoRetorno.
	 *
	 * @return dsTipoGeracaoRetorno
	 */
	public String getDsTipoGeracaoRetorno() {
		return dsTipoGeracaoRetorno;
	}
	
	/**
	 * Set: dsTipoGeracaoRetorno.
	 *
	 * @param dsTipoGeracaoRetorno the ds tipo geracao retorno
	 */
	public void setDsTipoGeracaoRetorno(String dsTipoGeracaoRetorno) {
		this.dsTipoGeracaoRetorno = dsTipoGeracaoRetorno;
	}
	
	/**
	 * Get: dsTipoLayoutArquivo.
	 *
	 * @return dsTipoLayoutArquivo
	 */
	public String getDsTipoLayoutArquivo() {
		return dsTipoLayoutArquivo;
	}
	
	/**
	 * Set: dsTipoLayoutArquivo.
	 *
	 * @param dsTipoLayoutArquivo the ds tipo layout arquivo
	 */
	public void setDsTipoLayoutArquivo(String dsTipoLayoutArquivo) {
		this.dsTipoLayoutArquivo = dsTipoLayoutArquivo;
	}
	
	/**
	 * Get: dsTipoManutencao.
	 *
	 * @return dsTipoManutencao
	 */
	public String getDsTipoManutencao() {
		return dsTipoManutencao;
	}
	
	/**
	 * Set: dsTipoManutencao.
	 *
	 * @param dsTipoManutencao the ds tipo manutencao
	 */
	public void setDsTipoManutencao(String dsTipoManutencao) {
		this.dsTipoManutencao = dsTipoManutencao;
	}
	
	/**
	 * Get: dsTipoMontaRetorno.
	 *
	 * @return dsTipoMontaRetorno
	 */
	public String getDsTipoMontaRetorno() {
		return dsTipoMontaRetorno;
	}
	
	/**
	 * Set: dsTipoMontaRetorno.
	 *
	 * @param dsTipoMontaRetorno the ds tipo monta retorno
	 */
	public void setDsTipoMontaRetorno(String dsTipoMontaRetorno) {
		this.dsTipoMontaRetorno = dsTipoMontaRetorno;
	}
	
	/**
	 * Get: dsTipoOcorRetorno.
	 *
	 * @return dsTipoOcorRetorno
	 */
	public String getDsTipoOcorRetorno() {
		return dsTipoOcorRetorno;
	}
	
	/**
	 * Set: dsTipoOcorRetorno.
	 *
	 * @param dsTipoOcorRetorno the ds tipo ocor retorno
	 */
	public void setDsTipoOcorRetorno(String dsTipoOcorRetorno) {
		this.dsTipoOcorRetorno = dsTipoOcorRetorno;
	}
	
	/**
	 * Get: dsTipoOrdemRegistroRetorno.
	 *
	 * @return dsTipoOrdemRegistroRetorno
	 */
	public String getDsTipoOrdemRegistroRetorno() {
		return dsTipoOrdemRegistroRetorno;
	}
	
	/**
	 * Set: dsTipoOrdemRegistroRetorno.
	 *
	 * @param dsTipoOrdemRegistroRetorno the ds tipo ordem registro retorno
	 */
	public void setDsTipoOrdemRegistroRetorno(String dsTipoOrdemRegistroRetorno) {
		this.dsTipoOrdemRegistroRetorno = dsTipoOrdemRegistroRetorno;
	}
	
	/**
	 * Get: hrInclusaoRegistro.
	 *
	 * @return hrInclusaoRegistro
	 */
	public String getHrInclusaoRegistro() {
		return hrInclusaoRegistro;
	}
	
	/**
	 * Set: hrInclusaoRegistro.
	 *
	 * @param hrInclusaoRegistro the hr inclusao registro
	 */
	public void setHrInclusaoRegistro(String hrInclusaoRegistro) {
		this.hrInclusaoRegistro = hrInclusaoRegistro;
	}
	
	/**
	 * Get: hrManutencaoRegistro.
	 *
	 * @return hrManutencaoRegistro
	 */
	public String getHrManutencaoRegistro() {
		return hrManutencaoRegistro;
	}
	
	/**
	 * Set: hrManutencaoRegistro.
	 *
	 * @param hrManutencaoRegistro the hr manutencao registro
	 */
	public void setHrManutencaoRegistro(String hrManutencaoRegistro) {
		this.hrManutencaoRegistro = hrManutencaoRegistro;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	/**
	 * Get: usuarioInclusaoFormatado.
	 *
	 * @return usuarioInclusaoFormatado
	 */
	public String getUsuarioInclusaoFormatado() {
		return usuarioInclusaoFormatado;
	}
	
	/**
	 * Set: usuarioInclusaoFormatado.
	 *
	 * @param usuarioInclusaoFormatado the usuario inclusao formatado
	 */
	public void setUsuarioInclusaoFormatado(String usuarioInclusaoFormatado) {
		this.usuarioInclusaoFormatado = usuarioInclusaoFormatado;
	}
	
	/**
	 * Get: usuarioManutencaoFormatado.
	 *
	 * @return usuarioManutencaoFormatado
	 */
	public String getUsuarioManutencaoFormatado() {
		return usuarioManutencaoFormatado;
	}
	
	/**
	 * Set: usuarioManutencaoFormatado.
	 *
	 * @param usuarioManutencaoFormatado the usuario manutencao formatado
	 */
	public void setUsuarioManutencaoFormatado(String usuarioManutencaoFormatado) {
		this.usuarioManutencaoFormatado = usuarioManutencaoFormatado;
	}
	
	/**
	 * Get: tipoCanalInclusaoFormatado.
	 *
	 * @return tipoCanalInclusaoFormatado
	 */
	public String getTipoCanalInclusaoFormatado() {
		return tipoCanalInclusaoFormatado;
	}
	
	/**
	 * Set: tipoCanalInclusaoFormatado.
	 *
	 * @param tipoCanalInclusaoFormatado the tipo canal inclusao formatado
	 */
	public void setTipoCanalInclusaoFormatado(String tipoCanalInclusaoFormatado) {
		this.tipoCanalInclusaoFormatado = tipoCanalInclusaoFormatado;
	}
	
	/**
	 * Get: tipoCanalManutencaoFormatado.
	 *
	 * @return tipoCanalManutencaoFormatado
	 */
	public String getTipoCanalManutencaoFormatado() {
		return tipoCanalManutencaoFormatado;
	}
	
	/**
	 * Set: tipoCanalManutencaoFormatado.
	 *
	 * @param tipoCanalManutencaoFormatado the tipo canal manutencao formatado
	 */
	public void setTipoCanalManutencaoFormatado(String tipoCanalManutencaoFormatado) {
		this.tipoCanalManutencaoFormatado = tipoCanalManutencaoFormatado;
	}
    
    
}
