package br.com.bradesco.web.pgit.service.business.contrato.bean;

/**
 * Nome: NumerosEmissoesPagasEnum
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public enum NumerosEmissoesPagasEnum {

    /** Atributo UMA. */
    UMA(1, "Uma"),
    
    /** Atributo DUAS. */
    DUAS(2, "Duas"),
    
    /** Atributo TRES. */
    TRES(3, "Tr�s"),
    
    /** Atributo TODAS. */
    TODAS(99, "Todas");
    
    /** Atributo numero. */
    private int numero = -1;
    
    /** Atributo descricao. */
    private String descricao = null;

    /**
     * Numeros emissoes pagas enum.
     *
     * @param numero the numero
     * @param descricao the descricao
     */
    private NumerosEmissoesPagasEnum(int numero, String descricao) {
        this.numero = numero;
        this.descricao = descricao;
    }

    /**
     * Get: numero.
     *
     * @return numero
     */
    public int getNumero() {
        return numero;
    }

    /**
     * Get: descricao.
     *
     * @return descricao
     */
    public String getDescricao() {
        return descricao;
    }

    /**
     * Get: enumByNumero.
     *
     * @param numero the numero
     * @return enumByNumero
     */
    public static NumerosEmissoesPagasEnum getEnumByNumero(int numero) {
        for (NumerosEmissoesPagasEnum numerosEmissoesPagasEnum : values()) {
            if (numerosEmissoesPagasEnum.getNumero() == numero) {
                return numerosEmissoesPagasEnum;
            }
        }
        return null;
    }
}
