/*
 * Nome: br.com.bradesco.web.pgit.service.business.combo.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.combo.bean;

/**
 * Nome: TipoSistemaSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class TipoSistemaSaidaDTO {
	
	/** Atributo cdBaseSistema. */
	private Integer cdBaseSistema;
	
	/** Atributo dsBaseSistema. */
	private String dsBaseSistema;
	
	/**
	 * Tipo sistema saida dto.
	 */
	public TipoSistemaSaidaDTO() {
	}
	
	/**
	 * Tipo sistema saida dto.
	 *
	 * @param cdBaseSistema the cd base sistema
	 * @param dsBaseSistema the ds base sistema
	 */
	public TipoSistemaSaidaDTO(Integer cdBaseSistema, String dsBaseSistema) {
		super();
		this.cdBaseSistema = cdBaseSistema;
		this.dsBaseSistema = dsBaseSistema;
	}
	
	/**
	 * Get: cdBaseSistema.
	 *
	 * @return cdBaseSistema
	 */
	public Integer getCdBaseSistema() {
		return cdBaseSistema;
	}
	
	/**
	 * Set: cdBaseSistema.
	 *
	 * @param cdBaseSistema the cd base sistema
	 */
	public void setCdBaseSistema(Integer cdBaseSistema) {
		this.cdBaseSistema = cdBaseSistema;
	}
	
	/**
	 * Get: dsBaseSistema.
	 *
	 * @return dsBaseSistema
	 */
	public String getDsBaseSistema() {
		return dsBaseSistema;
	}
	
	/**
	 * Set: dsBaseSistema.
	 *
	 * @param dsBaseSistema the ds base sistema
	 */
	public void setDsBaseSistema(String dsBaseSistema) {
		this.dsBaseSistema = dsBaseSistema;
	}
	
	

}
