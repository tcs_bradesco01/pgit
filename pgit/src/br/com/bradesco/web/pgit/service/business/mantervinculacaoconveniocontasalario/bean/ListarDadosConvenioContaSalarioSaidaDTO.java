package br.com.bradesco.web.pgit.service.business.mantervinculacaoconveniocontasalario.bean;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.ListarConvenioContaSalarioResponse;

public class ListarDadosConvenioContaSalarioSaidaDTO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2700928756167965322L;
	
	private String codMensagem; 
	private String mensagem; 
	private Integer cdFaiMaxOcorr; 
	private Integer cdTurMaxOcorr;  
	private Integer cdFilMaxOcorr;
	private Integer cdConMaxOcorr; 
	private Integer cdFolMaxOcorr; 
	private Integer cdCocMaxOcorr; 
	private Long cdPssoaJuridCta;
	private Integer cdTipoContrCta; 
	private Long numSequenciaContratoCta;
	private Integer cdInstituicaoBancaria; 
	private Integer cdUnidadeOrganizacionalCusto; 
	private Integer numConta; 
	private Integer numContaDigito; 
	private Integer cdIndicadorNumConve; 
	private Long cdConveCtaSalarial; 
	private BigDecimal vlPagamentoMes; 
	private Integer qtdFuncionarioFlPgto; 
	private BigDecimal vlMediaSalarialMes; 
	private Integer numDiaCredtPrim; 
	private Integer numDiaCredtSeg; 
	private Integer cdIndicadorQuestSalarial; 
	private Integer cdPagamentoSalarialMes; 
	private Integer cdPagamentoSalarialQzena; 
	private Integer cdLocCtaSalarial; 
	private String dsLocCtaSalarial; 
	private Integer dataReftFlPgto; 
	private String codIndicadorFedelize; 
	private String dsEmailEmpresa; 
	private String dsEmailAgencia; 
	private Integer cdIndicadorAltoTurnover; 
	private Integer cdIndicadorOfertaCartaoPrePago; 
	private Integer cdIndicadorAtivacaoAPP; 
	private String dsTextoJustfConc; 
	private String dsTextoJustfRecip;
	private String dsTextoJustfCompl; 
	private Integer cdIndicadorGrpQuest; 
	private Long nroGrupoEconmQuest; 
	private String dsGrupoEconmQuest; 
	private Integer cdUnidadeMeddAg; 
	private String dsAbrevMeddAg; 
	private Integer cdUdistcAgEmpr; 
	private String dsLogradouroEmprQuest; 
	private String dsBairroEmprQuest; 
	private Long cdMunEmprQuest;
	private String dsMuncEmpreQuest; 
	private Integer cdCepEmprQuest; 
	private Integer cdCepComplemento; 
	private Integer cdUfEmpreQuest; 
	private String dsUfEmprQuest; 
	private Long cdAgptoAtividadeQuest;
	private String dsRamoAtividadeQuest; 
	private Integer cdDiscagemDiretaInterEmpr; 
	private Integer cdDiscagemDiretaDistEmpr; 
	private Long numFoneEmprQuest; 
	private String dsSiteEmprQuest;
	private Long cdFuncGerRelacionamento; 
	private String dsGerRelacionamentoQuest; 
	private Integer cdDiscagemDiretaDistGer; 
	private Integer cdDiscagemDiretaDistanGer; 
	private Long numFoneGerQuest;
	private Integer cdSegmentoEmprQuest; 
	private String dsAbrevSegmentoEmpr; 
	private Integer cdIndicadorFlQuest; 
	private String dsIndicadorFlQuest; 
	private BigDecimal pctgmPreNovAnoQuest;
	private Integer cdprefeCsign; 
	private Integer cdprefePabCren; 
	private Integer cdIndicadorPaeCcren; 
	private Integer cdIndicadorAgCcren; 
	private Integer cdIndicadorInstaEstrt; 
	private Integer cdEstrtInstaEmpr;
	private String dsEstruturaInstaEmpr; 
	private Integer cdbancoVincInsta; 
	private Integer cdAgenciaVincInsta; 
	private String dsAgenciaVincInsta; 
	private Integer cdUnidadeMeddInsta; 
	private String dsAbrevInsta; 
	private Integer cdDistcAgenciaInsta; 
	private Integer cdIndicadorVisitaInsta; 
	private String dsIndicadorVisitaInsta; 
	private Long cdFuncGerVisitante;
	private String dsGerVisitanteInsta; 
	private Integer cdUnidadeMeddEspac; 
	private String dsAbrevEspacConcedido; 
	private Integer cdUespacConcedidoInsta; 
	private Integer cdTipoNecesInsta;
	private String dsTipoNecessInsta; 
	private Integer cdIndicadorPerdaRelacionamento; 
	private String dsIndicadorPerdarelacionamento; 
	private Integer cdPagamentoSalCredt;
	private Integer cdPagamentoSalCheque; 
	private Integer cdPagamentoSalDinheiro; 
	private Integer cdPagamentoSalDivers; 
	private String dsPagamentoSalDivers;
	private List<OcorrenciasDTO> ocorrenciasDTOLista; 
	private List<OcorrenciasDTO2> ocorrenciasDTO2Lista; 
	private List<OcorrenciasDTO3> ocorrenciasDTO3Lista; 
	private List<OcorrenciasDTO4> ocorrenciasDT4Lista; 
	private List<OcorrenciasDTO5> ocorrenciasDT5Lista; 
	private List<OcorrenciasDTO6> ocorrenciasDTO6Lista;
	
	public ListarDadosConvenioContaSalarioSaidaDTO() {
		super();
	}

	public ListarDadosConvenioContaSalarioSaidaDTO(
			String codMensagem, String mensagem, Integer cdFaiMaxOcorr,
			Integer cdTurMaxOcorr, Integer cdFilMaxOcorr,
			Integer cdConMaxOcorr, Integer cdFolMaxOcorr,
			Integer cdCocMaxOcorr, Long cdPssoaJuridCta,
			Integer cdTipoContrCta, Long numSequenciaContratoCta,
			Integer cdInstituicaoBancaria,
			Integer cdUnidadeOrganizacionalCusto, Integer numConta,
			Integer numContaDigito, Integer cdIndicadorNumConve,
			Long cdConveCtaSalarial, BigDecimal vlPagamentoMes,
			Integer qtdFuncionarioFlPgto, BigDecimal vlMediaSalarialMes,
			Integer numDiaCredtPrim, Integer numDiaCredtSeg,
			Integer cdIndicadorQuestSalarial, Integer cdPagamentoSalarialMes,
			Integer cdPagamentoSalarialQzena, Integer cdLocCtaSalarial,
			String dsLocCtaSalarial, Integer dataReftFlPgto,
			String codIndicadorFedelize, String dsEmailEmpresa,
			String dsEmailAgencia, Integer cdIndicadorAltoTurnover,
			Integer cdIndicadorOfertaCartaoPrePago,
			Integer cdIndicadorAtivacaoAPP, String dsTextoJustfConc,
			String dsTextoJustfRecip, String dsTextoJustfCompl,
			Integer cdIndicadorGrpQuest, Long nroGrupoEconmQuest,
			String dsGrupoEconmQuest, Integer cdUnidadeMeddAg,
			String dsAbrevMeddAg, Integer cdUdistcAgEmpr,
			String dsLogradouroEmprQuest, String dsBairroEmprQuest,
			Long cdMunEmprQuest, String dsMuncEmpreQuest,
			Integer cdCepEmprQuest, Integer cdCepComplemento,
			Integer cdUfEmpreQuest, String dsUfEmprQuest,
			Long cdAgptoAtividadeQuest, String dsRamoAtividadeQuest,
			Integer cdDiscagemDiretaIntegererEmpr,
			Integer cdDiscagemDiretaDistEmpr, Long numFoneEmprQuest,
			String dsSiteEmprQuest, Long cdFuncGerRelacionamento,
			String dsGerRelacionamentoQuest, Integer cdDiscagemDiretaDistGer,
			Integer cdDiscagemDiretaDistanGer, Long numFoneGerQuest,
			Integer cdSegmentoEmprQuest, String dsAbrevSegmentoEmpr,
			Integer cdIndicadorFlQuest, String dsIndicadorFlQuest,
			BigDecimal pctgmPreNovAnoQuest, Integer cdprefeCsign,
			Integer cdprefePabCren, Integer cdIndicadorPaeCcren,
			Integer cdIndicadorAgCcren, Integer cdIndicadorInstaEstrt,
			Integer cdEstrtInstaEmpr, String dsEstruturaInstaEmpr,
			Integer cdbancoVincInsta, Integer cdAgenciaVincInsta,
			String dsAgenciaVincInsta, Integer cdUnidadeMeddInsta,
			String dsAbrevInsta, Integer cdDistcAgenciaInsta,
			Integer cdIndicadorVisitaInsta, String dsIndicadorVisitaInsta,
			Long cdFuncGerVisitante, String dsGerVisitanteInsta,
			Integer cdUnidadeMeddEspac, String dsAbrevEspacConcedido,
			Integer cdUespacConcedidoInsta, Integer cdTipoNecesInsta,
			String dsTipoNecessInsta, Integer cdIndicadorPerdaRelacionamento,
			String dsIndicadorPerdarelacionamento, Integer cdPagamentoSalCredt,
			Integer cdPagamentoSalCheque, Integer cdPagamentoSalDinheiro,
			Integer cdPagamentoSalDivers, String dsPagamentoSalDivers,
			List<OcorrenciasDTO> ocorrenciasDTOLista,
			List<OcorrenciasDTO2> ocorrenciasDTO2Lista,
			List<OcorrenciasDTO3> ocorrenciasDTO3Lista,
			List<OcorrenciasDTO4> ocorrenciasDT4Lista,
			List<OcorrenciasDTO5> ocorrenciasDT5Lista,
			List<OcorrenciasDTO6> ocorrenciasDTO6Lista) {
		super();
		this.codMensagem = codMensagem;
		this.mensagem = mensagem;
		this.cdFaiMaxOcorr = cdFaiMaxOcorr;
		this.cdTurMaxOcorr = cdTurMaxOcorr;
		this.cdFilMaxOcorr = cdFilMaxOcorr;
		this.cdConMaxOcorr = cdConMaxOcorr;
		this.cdFolMaxOcorr = cdFolMaxOcorr;
		this.cdCocMaxOcorr = cdCocMaxOcorr;
		this.cdPssoaJuridCta = cdPssoaJuridCta;
		this.cdTipoContrCta = cdTipoContrCta;
		this.numSequenciaContratoCta = numSequenciaContratoCta;
		this.cdInstituicaoBancaria = cdInstituicaoBancaria;
		this.cdUnidadeOrganizacionalCusto = cdUnidadeOrganizacionalCusto;
		this.numConta = numConta;
		this.numContaDigito = numContaDigito;
		this.cdIndicadorNumConve = cdIndicadorNumConve;
		this.cdConveCtaSalarial = cdConveCtaSalarial;
		this.vlPagamentoMes = vlPagamentoMes;
		this.qtdFuncionarioFlPgto = qtdFuncionarioFlPgto;
		this.vlMediaSalarialMes = vlMediaSalarialMes;
		this.numDiaCredtPrim = numDiaCredtPrim;
		this.numDiaCredtSeg = numDiaCredtSeg;
		this.cdIndicadorQuestSalarial = cdIndicadorQuestSalarial;
		this.cdPagamentoSalarialMes = cdPagamentoSalarialMes;
		this.cdPagamentoSalarialQzena = cdPagamentoSalarialQzena;
		this.cdLocCtaSalarial = cdLocCtaSalarial;
		this.dsLocCtaSalarial = dsLocCtaSalarial;
		this.dataReftFlPgto = dataReftFlPgto;
		this.codIndicadorFedelize = codIndicadorFedelize;
		this.dsEmailEmpresa = dsEmailEmpresa;
		this.dsEmailAgencia = dsEmailAgencia;
		this.cdIndicadorAltoTurnover = cdIndicadorAltoTurnover;
		this.cdIndicadorOfertaCartaoPrePago = cdIndicadorOfertaCartaoPrePago;
		this.cdIndicadorAtivacaoAPP = cdIndicadorAtivacaoAPP;
		this.dsTextoJustfConc = dsTextoJustfConc;
		this.dsTextoJustfRecip = dsTextoJustfRecip;
		this.dsTextoJustfCompl = dsTextoJustfCompl;
		this.cdIndicadorGrpQuest = cdIndicadorGrpQuest;
		this.nroGrupoEconmQuest = nroGrupoEconmQuest;
		this.dsGrupoEconmQuest = dsGrupoEconmQuest;
		this.cdUnidadeMeddAg = cdUnidadeMeddAg;
		this.dsAbrevMeddAg = dsAbrevMeddAg;
		this.cdUdistcAgEmpr = cdUdistcAgEmpr;
		this.dsLogradouroEmprQuest = dsLogradouroEmprQuest;
		this.dsBairroEmprQuest = dsBairroEmprQuest;
		this.cdMunEmprQuest = cdMunEmprQuest;
		this.dsMuncEmpreQuest = dsMuncEmpreQuest;
		this.cdCepEmprQuest = cdCepEmprQuest;
		this.cdCepComplemento = cdCepComplemento;
		this.cdUfEmpreQuest = cdUfEmpreQuest;
		this.dsUfEmprQuest = dsUfEmprQuest;
		this.cdAgptoAtividadeQuest = cdAgptoAtividadeQuest;
		this.dsRamoAtividadeQuest = dsRamoAtividadeQuest;
		this.cdDiscagemDiretaInterEmpr = cdDiscagemDiretaIntegererEmpr;
		this.cdDiscagemDiretaDistEmpr = cdDiscagemDiretaDistEmpr;
		this.numFoneEmprQuest = numFoneEmprQuest;
		this.dsSiteEmprQuest = dsSiteEmprQuest;
		this.cdFuncGerRelacionamento = cdFuncGerRelacionamento;
		this.dsGerRelacionamentoQuest = dsGerRelacionamentoQuest;
		this.cdDiscagemDiretaDistGer = cdDiscagemDiretaDistGer;
		this.cdDiscagemDiretaDistanGer = cdDiscagemDiretaDistanGer;
		this.numFoneGerQuest = numFoneGerQuest;
		this.cdSegmentoEmprQuest = cdSegmentoEmprQuest;
		this.dsAbrevSegmentoEmpr = dsAbrevSegmentoEmpr;
		this.cdIndicadorFlQuest = cdIndicadorFlQuest;
		this.dsIndicadorFlQuest = dsIndicadorFlQuest;
		this.pctgmPreNovAnoQuest = pctgmPreNovAnoQuest;
		this.cdprefeCsign = cdprefeCsign;
		this.cdprefePabCren = cdprefePabCren;
		this.cdIndicadorPaeCcren = cdIndicadorPaeCcren;
		this.cdIndicadorAgCcren = cdIndicadorAgCcren;
		this.cdIndicadorInstaEstrt = cdIndicadorInstaEstrt;
		this.cdEstrtInstaEmpr = cdEstrtInstaEmpr;
		this.dsEstruturaInstaEmpr = dsEstruturaInstaEmpr;
		this.cdbancoVincInsta = cdbancoVincInsta;
		this.cdAgenciaVincInsta = cdAgenciaVincInsta;
		this.dsAgenciaVincInsta = dsAgenciaVincInsta;
		this.cdUnidadeMeddInsta = cdUnidadeMeddInsta;
		this.dsAbrevInsta = dsAbrevInsta;
		this.cdDistcAgenciaInsta = cdDistcAgenciaInsta;
		this.cdIndicadorVisitaInsta = cdIndicadorVisitaInsta;
		this.dsIndicadorVisitaInsta = dsIndicadorVisitaInsta;
		this.cdFuncGerVisitante = cdFuncGerVisitante;
		this.dsGerVisitanteInsta = dsGerVisitanteInsta;
		this.cdUnidadeMeddEspac = cdUnidadeMeddEspac;
		this.dsAbrevEspacConcedido = dsAbrevEspacConcedido;
		this.cdUespacConcedidoInsta = cdUespacConcedidoInsta;
		this.cdTipoNecesInsta = cdTipoNecesInsta;
		this.dsTipoNecessInsta = dsTipoNecessInsta;
		this.cdIndicadorPerdaRelacionamento = cdIndicadorPerdaRelacionamento;
		this.dsIndicadorPerdarelacionamento = dsIndicadorPerdarelacionamento;
		this.cdPagamentoSalCredt = cdPagamentoSalCredt;
		this.cdPagamentoSalCheque = cdPagamentoSalCheque;
		this.cdPagamentoSalDinheiro = cdPagamentoSalDinheiro;
		this.cdPagamentoSalDivers = cdPagamentoSalDivers;
		this.dsPagamentoSalDivers = dsPagamentoSalDivers;
		this.ocorrenciasDTOLista = ocorrenciasDTOLista;
		this.ocorrenciasDTO2Lista = ocorrenciasDTO2Lista;
		this.ocorrenciasDTO3Lista = ocorrenciasDTO3Lista;
		this.ocorrenciasDT4Lista = ocorrenciasDT4Lista;
		this.ocorrenciasDT5Lista = ocorrenciasDT5Lista;
		this.ocorrenciasDTO6Lista = ocorrenciasDTO6Lista;
	}

	public ListarDadosConvenioContaSalarioSaidaDTO (ListarConvenioContaSalarioResponse response) {
		
		this.codMensagem = response.getCodMensagem();
		this.mensagem = response.getMensagem();
		this.cdFaiMaxOcorr = response.getCdFaiMaxOcorr();
		this.cdTurMaxOcorr = response.getCdTurMaxOcorr();
		this.cdFilMaxOcorr = response.getCdFilMaxOcorr();
		this.cdConMaxOcorr = response.getCdConMaxOcorr();
		this.cdFolMaxOcorr = response.getCdFolMaxOcorr();
		this.cdCocMaxOcorr = response.getCdCocMaxOcorr();
		this.cdPssoaJuridCta = response.getCdPssoaJuridCta();
		this.cdTipoContrCta = response.getCdTipoContrCta();
		this.numSequenciaContratoCta = response.getNumSequenciaContratoCta();
		this.cdInstituicaoBancaria = response.getCdInstituicaoBancaria();
		this.cdUnidadeOrganizacionalCusto = response.getCdUnidadeOrganizacionalCusto();
		this.numConta = response.getNumConta();
		this.numContaDigito = response.getNumContaDigito();
		this.cdIndicadorNumConve = response.getCdIndicadorNumConve();
		this.cdConveCtaSalarial = response.getCdConveCtaSalarial();
		this.vlPagamentoMes = response.getVlPagamentoMes();
		this.qtdFuncionarioFlPgto = response.getQtdFuncionarioFlPgto();
		this.vlMediaSalarialMes = response.getVlMediaSalarialMes();
		this.numDiaCredtPrim = response.getNumDiaCredtPrim();
		this.numDiaCredtSeg = response.getNumDiaCredtSeg();
		this.cdIndicadorQuestSalarial = response.getCdIndicadorQuestSalarial();
		this.cdPagamentoSalarialMes = response.getCdPagamentoSalarialMes();
		this.cdPagamentoSalarialQzena = response.getCdPagamentoSalarialQzena();
		this.cdLocCtaSalarial = response.getCdLocCtaSalarial();
		this.dsLocCtaSalarial = response.getDsLocCtaSalarial();
		this.dataReftFlPgto = response.getDataReftFlPgto();
		this.codIndicadorFedelize = response.getCodIndicadorFedelize();
		this.dsEmailEmpresa = response.getDsEmailEmpresa();
		this.dsEmailAgencia = response.getDsEmailAgencia();
		this.cdIndicadorAltoTurnover = response.getCdIndicadorAltoTurnover();
		this.cdIndicadorOfertaCartaoPrePago = response.getCdIndicadorOfertaCartaoPrePago();
		this.cdIndicadorAtivacaoAPP = response.getCdIndicadorAtivacaoAPP();
		this.dsTextoJustfConc = response.getDsTextoJustfConc();
		this.dsTextoJustfRecip = response.getDsTextoJustfRecip();
		this.dsTextoJustfCompl = response.getDsTextoJustfCompl();
		this.cdIndicadorGrpQuest = response.getCdIndicadorGrpQuest();
		this.nroGrupoEconmQuest = response.getNroGrupoEconmQuest();
		this.dsGrupoEconmQuest = response.getDsGrupoEconmQuest();
		this.cdUnidadeMeddAg = response.getCdUnidadeMeddAg();
		this.dsAbrevMeddAg = response.getDsAbrevMeddAg();
		this.cdUdistcAgEmpr = response.getCdUdistcAgEmpr();
		this.dsLogradouroEmprQuest = response.getDsLogradouroEmprQuest();
		this.dsBairroEmprQuest = response.getDsBairroEmprQuest();
		this.cdMunEmprQuest = response.getCdMunEmprQuest();
		this.dsMuncEmpreQuest = response.getDsMuncEmpreQuest();
		this.cdCepEmprQuest = response.getCdCepEmprQuest();
		this.cdCepComplemento = response.getCdCepComplemento();
		this.cdUfEmpreQuest = response.getCdUfEmpreQuest();
		this.dsUfEmprQuest = response.getDsUfEmprQuest();
		this.cdAgptoAtividadeQuest = response.getCdAgptoAtividadeQuest();
		this.dsRamoAtividadeQuest = response.getDsRamoAtividadeQuest();
		this.cdDiscagemDiretaInterEmpr = response.getCdDiscagemDiretaInterEmpr();
		this.cdDiscagemDiretaDistEmpr = response.getCdDiscagemDiretaDistEmpr();
		this.numFoneEmprQuest = response.getNumFoneEmprQuest();
		this.dsSiteEmprQuest = response.getDsSiteEmprQuest();
		this.cdFuncGerRelacionamento = response.getCdFuncGerRelacionamento();
		this.dsGerRelacionamentoQuest = response.getDsGerRelacionamentoQuest();
		this.cdDiscagemDiretaDistGer = response.getCdDiscagemDiretaDistGer();
		this.cdDiscagemDiretaDistanGer = response.getCdDiscagemDiretaDistanGer();
		this.numFoneGerQuest = response.getNumFoneGerQuest();
		this.cdSegmentoEmprQuest = response.getCdSegmentoEmprQuest();
		this.dsAbrevSegmentoEmpr = response.getDsAbrevSegmentoEmpr();
		this.cdIndicadorFlQuest = response.getCdIndicadorFlQuest();
		this.dsIndicadorFlQuest = response.getDsIndicadorFlQuest();
		this.pctgmPreNovAnoQuest = response.getPctgmPreNovAnoQuest();
		this.cdprefeCsign = response.getCdprefeCsign();
		this.cdprefePabCren = response.getCdprefePabCren();
		this.cdIndicadorPaeCcren = response.getCdIndicadorPaeCcren();
		this.cdIndicadorAgCcren = response.getCdIndicadorAgCcren();
		this.cdIndicadorInstaEstrt = response.getCdEstrtInstaEmpr();
		this.cdEstrtInstaEmpr = response.getCdEstrtInstaEmpr();
		this.dsEstruturaInstaEmpr = response.getDsEstruturaInstaEmpr();
		this.cdbancoVincInsta = response.getCdbancoVincInsta();
		this.cdAgenciaVincInsta = response.getCdAgenciaVincInsta();
		this.dsAgenciaVincInsta = response.getDsAgenciaVincInsta();
		this.cdUnidadeMeddInsta = response.getCdUnidadeMeddInsta();
		this.dsAbrevInsta = response.getDsAbrevInsta();
		this.cdDistcAgenciaInsta = response.getCdDistcAgenciaInsta();
		this.cdIndicadorVisitaInsta = response.getCdIndicadorVisitaInsta();
		this.dsIndicadorVisitaInsta = response.getDsIndicadorVisitaInsta();
		this.cdFuncGerVisitante = response.getCdFuncGerVisitante();
		this.dsGerVisitanteInsta = response.getDsGerVisitanteInsta();
		this.cdUnidadeMeddEspac = response.getCdUnidadeMeddEspac();
		this.dsAbrevEspacConcedido = response.getDsAbrevEspacConcedido();
		this.cdUespacConcedidoInsta = response.getCdUespacConcedidoInsta();
		this.cdTipoNecesInsta = response.getCdTipoNecesInsta();
		this.dsTipoNecessInsta = response.getDsTipoNecessInsta();
		this.cdIndicadorPerdaRelacionamento = response.getCdIndicadorPerdaRelacionamento();
		this.dsIndicadorPerdarelacionamento = response.getDsIndicadorPerdarelacionamento();
		this.cdPagamentoSalCredt = response.getCdPagamentoSalCredt();
		this.cdPagamentoSalCheque = response.getCdPagamentoSalCheque();
		this.cdPagamentoSalDinheiro = response.getCdPagamentoSalDinheiro();
		this.cdPagamentoSalDivers = response.getCdPagamentoSalDivers();
		this.dsPagamentoSalDivers = response.getDsPagamentoSalDivers();
		this.ocorrenciasDTOLista = new OcorrenciasDTO().listarOcorrenciasDTO(response);
		this.ocorrenciasDTO2Lista = new OcorrenciasDTO2().listarOcorrenciasDTO2(response);
		this.ocorrenciasDTO3Lista = new OcorrenciasDTO3().listarOcorrenciasDTO3(response);
		this.ocorrenciasDT4Lista = new OcorrenciasDTO4().listarOcorrenciasDTO4(response);
		this.ocorrenciasDT5Lista = new OcorrenciasDTO5().listarOcorrenciasDTO5(response);
		this.ocorrenciasDTO6Lista = new OcorrenciasDTO6().listarOcorrenciasDTO6(response);
	}
	
	public String getCodMensagem() {
		return codMensagem;
	}

	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}

	public String getMensagem() {
		return mensagem;
	}

	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	public Integer getCdFaiMaxOcorr() {
		return cdFaiMaxOcorr;
	}

	public void setCdFaiMaxOcorr(Integer cdFaiMaxOcorr) {
		this.cdFaiMaxOcorr = cdFaiMaxOcorr;
	}

	public Integer getCdTurMaxOcorr() {
		return cdTurMaxOcorr;
	}

	public void setCdTurMaxOcorr(Integer cdTurMaxOcorr) {
		this.cdTurMaxOcorr = cdTurMaxOcorr;
	}

	public Integer getCdFilMaxOcorr() {
		return cdFilMaxOcorr;
	}

	public void setCdFilMaxOcorr(Integer cdFilMaxOcorr) {
		this.cdFilMaxOcorr = cdFilMaxOcorr;
	}

	public Integer getCdConMaxOcorr() {
		return cdConMaxOcorr;
	}

	public void setCdConMaxOcorr(Integer cdConMaxOcorr) {
		this.cdConMaxOcorr = cdConMaxOcorr;
	}

	public Integer getCdFolMaxOcorr() {
		return cdFolMaxOcorr;
	}

	public void setCdFolMaxOcorr(Integer cdFolMaxOcorr) {
		this.cdFolMaxOcorr = cdFolMaxOcorr;
	}

	public Integer getCdCocMaxOcorr() {
		return cdCocMaxOcorr;
	}

	public void setCdCocMaxOcorr(Integer cdCocMaxOcorr) {
		this.cdCocMaxOcorr = cdCocMaxOcorr;
	}

	public Long getCdPssoaJuridCta() {
		return cdPssoaJuridCta;
	}

	public void setCdPssoaJuridCta(Long cdPssoaJuridCta) {
		this.cdPssoaJuridCta = cdPssoaJuridCta;
	}

	public Integer getCdTipoContrCta() {
		return cdTipoContrCta;
	}

	public void setCdTipoContrCta(Integer cdTipoContrCta) {
		this.cdTipoContrCta = cdTipoContrCta;
	}

	public Long getNumSequenciaContratoCta() {
		return numSequenciaContratoCta;
	}

	public void setNumSequenciaContratoCta(Long numSequenciaContratoCta) {
		this.numSequenciaContratoCta = numSequenciaContratoCta;
	}

	public Integer getCdInstituicaoBancaria() {
		return cdInstituicaoBancaria;
	}

	public void setCdInstituicaoBancaria(Integer cdInstituicaoBancaria) {
		this.cdInstituicaoBancaria = cdInstituicaoBancaria;
	}

	public Integer getCdUnidadeOrganizacionalCusto() {
		return cdUnidadeOrganizacionalCusto;
	}

	public void setCdUnidadeOrganizacionalCusto(Integer cdUnidadeOrganizacionalCusto) {
		this.cdUnidadeOrganizacionalCusto = cdUnidadeOrganizacionalCusto;
	}

	public Integer getNumConta() {
		return numConta;
	}

	public void setNumConta(Integer numConta) {
		this.numConta = numConta;
	}

	public Integer getNumContaDigito() {
		return numContaDigito;
	}

	public void setNumContaDigito(Integer numContaDigito) {
		this.numContaDigito = numContaDigito;
	}

	public Integer getCdIndicadorNumConve() {
		return cdIndicadorNumConve;
	}

	public void setCdIndicadorNumConve(Integer cdIndicadorNumConve) {
		this.cdIndicadorNumConve = cdIndicadorNumConve;
	}

	public Long getCdConveCtaSalarial() {
		return cdConveCtaSalarial;
	}

	public void setCdConveCtaSalarial(Long cdConveCtaSalarial) {
		this.cdConveCtaSalarial = cdConveCtaSalarial;
	}

	public BigDecimal getVlPagamentoMes() {
		return vlPagamentoMes;
	}

	public void setVlPagamentoMes(BigDecimal vlPagamentoMes) {
		this.vlPagamentoMes = vlPagamentoMes;
	}

	public Integer getQtdFuncionarioFlPgto() {
		return qtdFuncionarioFlPgto;
	}

	public void setQtdFuncionarioFlPgto(Integer qtdFuncionarioFlPgto) {
		this.qtdFuncionarioFlPgto = qtdFuncionarioFlPgto;
	}

	public BigDecimal getVlMediaSalarialMes() {
		return vlMediaSalarialMes;
	}

	public void setVlMediaSalarialMes(BigDecimal vlMediaSalarialMes) {
		this.vlMediaSalarialMes = vlMediaSalarialMes;
	}

	public Integer getNumDiaCredtPrim() {
		return numDiaCredtPrim;
	}

	public void setNumDiaCredtPrim(Integer numDiaCredtPrim) {
		this.numDiaCredtPrim = numDiaCredtPrim;
	}

	public Integer getNumDiaCredtSeg() {
		return numDiaCredtSeg;
	}

	public void setNumDiaCredtSeg(Integer numDiaCredtSeg) {
		this.numDiaCredtSeg = numDiaCredtSeg;
	}

	public Integer getCdIndicadorQuestSalarial() {
		return cdIndicadorQuestSalarial;
	}

	public void setCdIndicadorQuestSalarial(Integer cdIndicadorQuestSalarial) {
		this.cdIndicadorQuestSalarial = cdIndicadorQuestSalarial;
	}

	public Integer getCdPagamentoSalarialMes() {
		return cdPagamentoSalarialMes;
	}

	public void setCdPagamentoSalarialMes(Integer cdPagamentoSalarialMes) {
		this.cdPagamentoSalarialMes = cdPagamentoSalarialMes;
	}

	public Integer getCdPagamentoSalarialQzena() {
		return cdPagamentoSalarialQzena;
	}

	public void setCdPagamentoSalarialQzena(Integer cdPagamentoSalarialQzena) {
		this.cdPagamentoSalarialQzena = cdPagamentoSalarialQzena;
	}

	public Integer getCdLocCtaSalarial() {
		return cdLocCtaSalarial;
	}

	public void setCdLocCtaSalarial(Integer cdLocCtaSalarial) {
		this.cdLocCtaSalarial = cdLocCtaSalarial;
	}

	public String getDsLocCtaSalarial() {
		return dsLocCtaSalarial;
	}

	public void setDsLocCtaSalarial(String dsLocCtaSalarial) {
		this.dsLocCtaSalarial = dsLocCtaSalarial;
	}

	public Integer getDataReftFlPgto() {
		return dataReftFlPgto;
	}

	public void setDataReftFlPgto(Integer dataReftFlPgto) {
		this.dataReftFlPgto = dataReftFlPgto;
	}

	public String getCodIndicadorFedelize() {
		return codIndicadorFedelize;
	}

	public void setCodIndicadorFedelize(String codIndicadorFedelize) {
		this.codIndicadorFedelize = codIndicadorFedelize;
	}

	public String getDsEmailEmpresa() {
		return dsEmailEmpresa;
	}

	public void setDsEmailEmpresa(String dsEmailEmpresa) {
		this.dsEmailEmpresa = dsEmailEmpresa;
	}

	public String getDsEmailAgencia() {
		return dsEmailAgencia;
	}

	public void setDsEmailAgencia(String dsEmailAgencia) {
		this.dsEmailAgencia = dsEmailAgencia;
	}

	public Integer getCdIndicadorAltoTurnover() {
		return cdIndicadorAltoTurnover;
	}

	public void setCdIndicadorAltoTurnover(Integer cdIndicadorAltoTurnover) {
		this.cdIndicadorAltoTurnover = cdIndicadorAltoTurnover;
	}

	public Integer getCdIndicadorOfertaCartaoPrePago() {
		return cdIndicadorOfertaCartaoPrePago;
	}

	public void setCdIndicadorOfertaCartaoPrePago(
			Integer cdIndicadorOfertaCartaoPrePago) {
		this.cdIndicadorOfertaCartaoPrePago = cdIndicadorOfertaCartaoPrePago;
	}

	public Integer getCdIndicadorAtivacaoAPP() {
		return cdIndicadorAtivacaoAPP;
	}

	public void setCdIndicadorAtivacaoAPP(Integer cdIndicadorAtivacaoAPP) {
		this.cdIndicadorAtivacaoAPP = cdIndicadorAtivacaoAPP;
	}

	public String getDsTextoJustfConc() {
		return dsTextoJustfConc;
	}

	public void setDsTextoJustfConc(String dsTextoJustfConc) {
		this.dsTextoJustfConc = dsTextoJustfConc;
	}

	public String getDsTextoJustfRecip() {
		return dsTextoJustfRecip;
	}

	public void setDsTextoJustfRecip(String dsTextoJustfRecip) {
		this.dsTextoJustfRecip = dsTextoJustfRecip;
	}

	public String getDsTextoJustfCompl() {
		return dsTextoJustfCompl;
	}

	public void setDsTextoJustfCompl(String dsTextoJustfCompl) {
		this.dsTextoJustfCompl = dsTextoJustfCompl;
	}

	public Integer getCdIndicadorGrpQuest() {
		return cdIndicadorGrpQuest;
	}

	public void setCdIndicadorGrpQuest(Integer cdIndicadorGrpQuest) {
		this.cdIndicadorGrpQuest = cdIndicadorGrpQuest;
	}

	public Long getNroGrupoEconmQuest() {
		return nroGrupoEconmQuest;
	}

	public void setNroGrupoEconmQuest(Long nroGrupoEconmQuest) {
		this.nroGrupoEconmQuest = nroGrupoEconmQuest;
	}

	public String getDsGrupoEconmQuest() {
		return dsGrupoEconmQuest;
	}

	public void setDsGrupoEconmQuest(String dsGrupoEconmQuest) {
		this.dsGrupoEconmQuest = dsGrupoEconmQuest;
	}

	public Integer getCdUnidadeMeddAg() {
		return cdUnidadeMeddAg;
	}

	public void setCdUnidadeMeddAg(Integer cdUnidadeMeddAg) {
		this.cdUnidadeMeddAg = cdUnidadeMeddAg;
	}

	public String getDsAbrevMeddAg() {
		return dsAbrevMeddAg;
	}

	public void setDsAbrevMeddAg(String dsAbrevMeddAg) {
		this.dsAbrevMeddAg = dsAbrevMeddAg;
	}

	public Integer getCdUdistcAgEmpr() {
		return cdUdistcAgEmpr;
	}

	public void setCdUdistcAgEmpr(Integer cdUdistcAgEmpr) {
		this.cdUdistcAgEmpr = cdUdistcAgEmpr;
	}

	public String getDsLogradouroEmprQuest() {
		return dsLogradouroEmprQuest;
	}

	public void setDsLogradouroEmprQuest(String dsLogradouroEmprQuest) {
		this.dsLogradouroEmprQuest = dsLogradouroEmprQuest;
	}

	public String getDsBairroEmprQuest() {
		return dsBairroEmprQuest;
	}

	public void setDsBairroEmprQuest(String dsBairroEmprQuest) {
		this.dsBairroEmprQuest = dsBairroEmprQuest;
	}

	public Long getCdMunEmprQuest() {
		return cdMunEmprQuest;
	}

	public void setCdMunEmprQuest(Long cdMunEmprQuest) {
		this.cdMunEmprQuest = cdMunEmprQuest;
	}

	public String getDsMuncEmpreQuest() {
		return dsMuncEmpreQuest;
	}

	public void setDsMuncEmpreQuest(String dsMuncEmpreQuest) {
		this.dsMuncEmpreQuest = dsMuncEmpreQuest;
	}

	public Integer getCdCepEmprQuest() {
		return cdCepEmprQuest;
	}

	public void setCdCepEmprQuest(Integer cdCepEmprQuest) {
		this.cdCepEmprQuest = cdCepEmprQuest;
	}

	public Integer getCdCepComplemento() {
		return cdCepComplemento;
	}

	public void setCdCepComplemento(Integer cdCepComplemento) {
		this.cdCepComplemento = cdCepComplemento;
	}

	public Integer getCdUfEmpreQuest() {
		return cdUfEmpreQuest;
	}

	public void setCdUfEmpreQuest(Integer cdUfEmpreQuest) {
		this.cdUfEmpreQuest = cdUfEmpreQuest;
	}

	public String getDsUfEmprQuest() {
		return dsUfEmprQuest;
	}

	public void setDsUfEmprQuest(String dsUfEmprQuest) {
		this.dsUfEmprQuest = dsUfEmprQuest;
	}

	public Long getCdAgptoAtividadeQuest() {
		return cdAgptoAtividadeQuest;
	}

	public void setCdAgptoAtividadeQuest(Long cdAgptoAtividadeQuest) {
		this.cdAgptoAtividadeQuest = cdAgptoAtividadeQuest;
	}

	public String getDsRamoAtividadeQuest() {
		return dsRamoAtividadeQuest;
	}

	public void setDsRamoAtividadeQuest(String dsRamoAtividadeQuest) {
		this.dsRamoAtividadeQuest = dsRamoAtividadeQuest;
	}

	public Integer getCdDiscagemDiretaInterEmpr() {
		return cdDiscagemDiretaInterEmpr;
	}

	public void setCdDiscagemDiretaInterEmpr(Integer cdDiscagemDiretaInterEmpr) {
		this.cdDiscagemDiretaInterEmpr = cdDiscagemDiretaInterEmpr;
	}

	public Integer getCdDiscagemDiretaDistEmpr() {
		return cdDiscagemDiretaDistEmpr;
	}

	public void setCdDiscagemDiretaDistEmpr(Integer cdDiscagemDiretaDistEmpr) {
		this.cdDiscagemDiretaDistEmpr = cdDiscagemDiretaDistEmpr;
	}

	public Long getNumFoneEmprQuest() {
		return numFoneEmprQuest;
	}

	public void setNumFoneEmprQuest(Long numFoneEmprQuest) {
		this.numFoneEmprQuest = numFoneEmprQuest;
	}

	public String getDsSiteEmprQuest() {
		return dsSiteEmprQuest;
	}

	public void setDsSiteEmprQuest(String dsSiteEmprQuest) {
		this.dsSiteEmprQuest = dsSiteEmprQuest;
	}

	public Long getCdFuncGerRelacionamento() {
		return cdFuncGerRelacionamento;
	}

	public void setCdFuncGerRelacionamento(Long cdFuncGerRelacionamento) {
		this.cdFuncGerRelacionamento = cdFuncGerRelacionamento;
	}

	public String getDsGerRelacionamentoQuest() {
		return dsGerRelacionamentoQuest;
	}

	public void setDsGerRelacionamentoQuest(String dsGerRelacionamentoQuest) {
		this.dsGerRelacionamentoQuest = dsGerRelacionamentoQuest;
	}

	public Integer getCdDiscagemDiretaDistGer() {
		return cdDiscagemDiretaDistGer;
	}

	public void setCdDiscagemDiretaDistGer(Integer cdDiscagemDiretaDistGer) {
		this.cdDiscagemDiretaDistGer = cdDiscagemDiretaDistGer;
	}

	public Integer getCdDiscagemDiretaDistanGer() {
		return cdDiscagemDiretaDistanGer;
	}

	public void setCdDiscagemDiretaDistanGer(Integer cdDiscagemDiretaDistanGer) {
		this.cdDiscagemDiretaDistanGer = cdDiscagemDiretaDistanGer;
	}

	public Long getNumFoneGerQuest() {
		return numFoneGerQuest;
	}

	public void setNumFoneGerQuest(Long numFoneGerQuest) {
		this.numFoneGerQuest = numFoneGerQuest;
	}

	public Integer getCdSegmentoEmprQuest() {
		return cdSegmentoEmprQuest;
	}

	public void setCdSegmentoEmprQuest(Integer cdSegmentoEmprQuest) {
		this.cdSegmentoEmprQuest = cdSegmentoEmprQuest;
	}

	public String getDsAbrevSegmentoEmpr() {
		return dsAbrevSegmentoEmpr;
	}

	public void setDsAbrevSegmentoEmpr(String dsAbrevSegmentoEmpr) {
		this.dsAbrevSegmentoEmpr = dsAbrevSegmentoEmpr;
	}

	public Integer getCdIndicadorFlQuest() {
		return cdIndicadorFlQuest;
	}

	public void setCdIndicadorFlQuest(Integer cdIndicadorFlQuest) {
		this.cdIndicadorFlQuest = cdIndicadorFlQuest;
	}

	public String getDsIndicadorFlQuest() {
		return dsIndicadorFlQuest;
	}

	public void setDsIndicadorFlQuest(String dsIndicadorFlQuest) {
		this.dsIndicadorFlQuest = dsIndicadorFlQuest;
	}

	public BigDecimal getPctgmPreNovAnoQuest() {
		return pctgmPreNovAnoQuest;
	}

	public void setPctgmPreNovAnoQuest(BigDecimal pctgmPreNovAnoQuest) {
		this.pctgmPreNovAnoQuest = pctgmPreNovAnoQuest;
	}

	public Integer getCdprefeCsign() {
		return cdprefeCsign;
	}

	public void setCdprefeCsign(Integer cdprefeCsign) {
		this.cdprefeCsign = cdprefeCsign;
	}

	public Integer getCdprefePabCren() {
		return cdprefePabCren;
	}

	public void setCdprefePabCren(Integer cdprefePabCren) {
		this.cdprefePabCren = cdprefePabCren;
	}

	public Integer getCdIndicadorPaeCcren() {
		return cdIndicadorPaeCcren;
	}

	public void setCdIndicadorPaeCcren(Integer cdIndicadorPaeCcren) {
		this.cdIndicadorPaeCcren = cdIndicadorPaeCcren;
	}

	public Integer getCdIndicadorAgCcren() {
		return cdIndicadorAgCcren;
	}

	public void setCdIndicadorAgCcren(Integer cdIndicadorAgCcren) {
		this.cdIndicadorAgCcren = cdIndicadorAgCcren;
	}

	public Integer getCdIndicadorInstaEstrt() {
		return cdIndicadorInstaEstrt;
	}

	public void setCdIndicadorInstaEstrt(Integer cdIndicadorInstaEstrt) {
		this.cdIndicadorInstaEstrt = cdIndicadorInstaEstrt;
	}

	public Integer getCdEstrtInstaEmpr() {
		return cdEstrtInstaEmpr;
	}

	public void setCdEstrtInstaEmpr(Integer cdEstrtInstaEmpr) {
		this.cdEstrtInstaEmpr = cdEstrtInstaEmpr;
	}

	public String getDsEstruturaInstaEmpr() {
		return dsEstruturaInstaEmpr;
	}

	public void setDsEstruturaInstaEmpr(String dsEstruturaInstaEmpr) {
		this.dsEstruturaInstaEmpr = dsEstruturaInstaEmpr;
	}

	public Integer getCdbancoVincInsta() {
		return cdbancoVincInsta;
	}

	public void setCdbancoVincInsta(Integer cdbancoVincInsta) {
		this.cdbancoVincInsta = cdbancoVincInsta;
	}

	public Integer getCdAgenciaVincInsta() {
		return cdAgenciaVincInsta;
	}

	public void setCdAgenciaVincInsta(Integer cdAgenciaVincInsta) {
		this.cdAgenciaVincInsta = cdAgenciaVincInsta;
	}

	public String getDsAgenciaVincInsta() {
		return dsAgenciaVincInsta;
	}

	public void setDsAgenciaVincInsta(String dsAgenciaVincInsta) {
		this.dsAgenciaVincInsta = dsAgenciaVincInsta;
	}

	public Integer getCdUnidadeMeddInsta() {
		return cdUnidadeMeddInsta;
	}

	public void setCdUnidadeMeddInsta(Integer cdUnidadeMeddInsta) {
		this.cdUnidadeMeddInsta = cdUnidadeMeddInsta;
	}

	public String getDsAbrevInsta() {
		return dsAbrevInsta;
	}

	public void setDsAbrevInsta(String dsAbrevInsta) {
		this.dsAbrevInsta = dsAbrevInsta;
	}

	public Integer getCdDistcAgenciaInsta() {
		return cdDistcAgenciaInsta;
	}

	public void setCdDistcAgenciaInsta(Integer cdDistcAgenciaInsta) {
		this.cdDistcAgenciaInsta = cdDistcAgenciaInsta;
	}

	public Integer getCdIndicadorVisitaInsta() {
		return cdIndicadorVisitaInsta;
	}

	public void setCdIndicadorVisitaInsta(Integer cdIndicadorVisitaInsta) {
		this.cdIndicadorVisitaInsta = cdIndicadorVisitaInsta;
	}

	public String getDsIndicadorVisitaInsta() {
		return dsIndicadorVisitaInsta;
	}

	public void setDsIndicadorVisitaInsta(String dsIndicadorVisitaInsta) {
		this.dsIndicadorVisitaInsta = dsIndicadorVisitaInsta;
	}

	public Long getCdFuncGerVisitante() {
		return cdFuncGerVisitante;
	}

	public void setCdFuncGerVisitante(Long cdFuncGerVisitante) {
		this.cdFuncGerVisitante = cdFuncGerVisitante;
	}

	public String getDsGerVisitanteInsta() {
		return dsGerVisitanteInsta;
	}

	public void setDsGerVisitanteInsta(String dsGerVisitanteInsta) {
		this.dsGerVisitanteInsta = dsGerVisitanteInsta;
	}

	public Integer getCdUnidadeMeddEspac() {
		return cdUnidadeMeddEspac;
	}

	public void setCdUnidadeMeddEspac(Integer cdUnidadeMeddEspac) {
		this.cdUnidadeMeddEspac = cdUnidadeMeddEspac;
	}

	public String getDsAbrevEspacConcedido() {
		return dsAbrevEspacConcedido;
	}

	public void setDsAbrevEspacConcedido(String dsAbrevEspacConcedido) {
		this.dsAbrevEspacConcedido = dsAbrevEspacConcedido;
	}

	public Integer getCdUespacConcedidoInsta() {
		return cdUespacConcedidoInsta;
	}

	public void setCdUespacConcedidoInsta(Integer cdUespacConcedidoInsta) {
		this.cdUespacConcedidoInsta = cdUespacConcedidoInsta;
	}

	public Integer getCdTipoNecesInsta() {
		return cdTipoNecesInsta;
	}

	public void setCdTipoNecesInsta(Integer cdTipoNecesInsta) {
		this.cdTipoNecesInsta = cdTipoNecesInsta;
	}

	public String getDsTipoNecessInsta() {
		return dsTipoNecessInsta;
	}

	public void setDsTipoNecessInsta(String dsTipoNecessInsta) {
		this.dsTipoNecessInsta = dsTipoNecessInsta;
	}

	public Integer getCdIndicadorPerdaRelacionamento() {
		return cdIndicadorPerdaRelacionamento;
	}

	public void setCdIndicadorPerdaRelacionamento(
			Integer cdIndicadorPerdaRelacionamento) {
		this.cdIndicadorPerdaRelacionamento = cdIndicadorPerdaRelacionamento;
	}

	public String getDsIndicadorPerdarelacionamento() {
		return dsIndicadorPerdarelacionamento;
	}

	public void setDsIndicadorPerdarelacionamento(
			String dsIndicadorPerdarelacionamento) {
		this.dsIndicadorPerdarelacionamento = dsIndicadorPerdarelacionamento;
	}

	public Integer getCdPagamentoSalCredt() {
		return cdPagamentoSalCredt;
	}

	public void setCdPagamentoSalCredt(Integer cdPagamentoSalCredt) {
		this.cdPagamentoSalCredt = cdPagamentoSalCredt;
	}

	public Integer getCdPagamentoSalCheque() {
		return cdPagamentoSalCheque;
	}

	public void setCdPagamentoSalCheque(Integer cdPagamentoSalCheque) {
		this.cdPagamentoSalCheque = cdPagamentoSalCheque;
	}

	public Integer getCdPagamentoSalDinheiro() {
		return cdPagamentoSalDinheiro;
	}

	public void setCdPagamentoSalDinheiro(Integer cdPagamentoSalDinheiro) {
		this.cdPagamentoSalDinheiro = cdPagamentoSalDinheiro;
	}

	public Integer getCdPagamentoSalDivers() {
		return cdPagamentoSalDivers;
	}

	public void setCdPagamentoSalDivers(Integer cdPagamentoSalDivers) {
		this.cdPagamentoSalDivers = cdPagamentoSalDivers;
	}

	public String getDsPagamentoSalDivers() {
		return dsPagamentoSalDivers;
	}

	public void setDsPagamentoSalDivers(String dsPagamentoSalDivers) {
		this.dsPagamentoSalDivers = dsPagamentoSalDivers;
	}

	public List<OcorrenciasDTO> getOcorrenciasDTOLista() {
		return ocorrenciasDTOLista;
	}

	public void setOcorrenciasDTOLista(List<OcorrenciasDTO> ocorrenciasDTOLista) {
		this.ocorrenciasDTOLista = ocorrenciasDTOLista;
	}

	public List<OcorrenciasDTO2> getOcorrenciasDTO2Lista() {
		return ocorrenciasDTO2Lista;
	}

	public void setOcorrenciasDTO2Lista(List<OcorrenciasDTO2> ocorrenciasDTO2Lista) {
		this.ocorrenciasDTO2Lista = ocorrenciasDTO2Lista;
	}

	public List<OcorrenciasDTO3> getOcorrenciasDTO3Lista() {
		return ocorrenciasDTO3Lista;
	}

	public void setOcorrenciasDTO3Lista(List<OcorrenciasDTO3> ocorrenciasDTO3Lista) {
		this.ocorrenciasDTO3Lista = ocorrenciasDTO3Lista;
	}

	public List<OcorrenciasDTO4> getOcorrenciasDT4Lista() {
		return ocorrenciasDT4Lista;
	}

	public void setOcorrenciasDT4Lista(List<OcorrenciasDTO4> ocorrenciasDT4Lista) {
		this.ocorrenciasDT4Lista = ocorrenciasDT4Lista;
	}

	public List<OcorrenciasDTO5> getOcorrenciasDT5Lista() {
		return ocorrenciasDT5Lista;
	}

	public void setOcorrenciasDT5Lista(List<OcorrenciasDTO5> ocorrenciasDT5Lista) {
		this.ocorrenciasDT5Lista = ocorrenciasDT5Lista;
	}

	public List<OcorrenciasDTO6> getOcorrenciasDTO6Lista() {
		return ocorrenciasDTO6Lista;
	}

	public void setOcorrenciasDTO6Lista(List<OcorrenciasDTO6> ocorrenciasDTO6Lista) {
		this.ocorrenciasDTO6Lista = ocorrenciasDTO6Lista;
	}
	
}
