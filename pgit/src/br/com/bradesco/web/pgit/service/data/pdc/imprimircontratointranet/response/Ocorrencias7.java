/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias7.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias7 implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _dsProdutoSalarioInternetBanking
     */
    private java.lang.String _dsProdutoSalarioInternetBanking;

    /**
     * Field _dsServicoSalarioInternetBanking
     */
    private java.lang.String _dsServicoSalarioInternetBanking;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias7() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias7()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Returns the value of field
     * 'dsProdutoSalarioInternetBanking'.
     * 
     * @return String
     * @return the value of field 'dsProdutoSalarioInternetBanking'.
     */
    public java.lang.String getDsProdutoSalarioInternetBanking()
    {
        return this._dsProdutoSalarioInternetBanking;
    } //-- java.lang.String getDsProdutoSalarioInternetBanking() 

    /**
     * Returns the value of field
     * 'dsServicoSalarioInternetBanking'.
     * 
     * @return String
     * @return the value of field 'dsServicoSalarioInternetBanking'.
     */
    public java.lang.String getDsServicoSalarioInternetBanking()
    {
        return this._dsServicoSalarioInternetBanking;
    } //-- java.lang.String getDsServicoSalarioInternetBanking() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'dsProdutoSalarioInternetBanking'.
     * 
     * @param dsProdutoSalarioInternetBanking the value of field
     * 'dsProdutoSalarioInternetBanking'.
     */
    public void setDsProdutoSalarioInternetBanking(java.lang.String dsProdutoSalarioInternetBanking)
    {
        this._dsProdutoSalarioInternetBanking = dsProdutoSalarioInternetBanking;
    } //-- void setDsProdutoSalarioInternetBanking(java.lang.String) 

    /**
     * Sets the value of field 'dsServicoSalarioInternetBanking'.
     * 
     * @param dsServicoSalarioInternetBanking the value of field
     * 'dsServicoSalarioInternetBanking'.
     */
    public void setDsServicoSalarioInternetBanking(java.lang.String dsServicoSalarioInternetBanking)
    {
        this._dsServicoSalarioInternetBanking = dsServicoSalarioInternetBanking;
    } //-- void setDsServicoSalarioInternetBanking(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias7
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias7 unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias7) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias7.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias7 unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
