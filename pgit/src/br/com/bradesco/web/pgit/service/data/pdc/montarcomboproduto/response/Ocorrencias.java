/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.montarcomboproduto.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _codigoProdutoPagamento
     */
    private int _codigoProdutoPagamento = 0;

    /**
     * keeps track of state for field: _codigoProdutoPagamento
     */
    private boolean _has_codigoProdutoPagamento;

    /**
     * Field _codigoCentroCusto
     */
    private java.lang.String _codigoCentroCusto;

    /**
     * Field _descricaoProdutoPagamento
     */
    private java.lang.String _descricaoProdutoPagamento;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.montarcomboproduto.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCodigoProdutoPagamento
     * 
     */
    public void deleteCodigoProdutoPagamento()
    {
        this._has_codigoProdutoPagamento= false;
    } //-- void deleteCodigoProdutoPagamento() 

    /**
     * Returns the value of field 'codigoCentroCusto'.
     * 
     * @return String
     * @return the value of field 'codigoCentroCusto'.
     */
    public java.lang.String getCodigoCentroCusto()
    {
        return this._codigoCentroCusto;
    } //-- java.lang.String getCodigoCentroCusto() 

    /**
     * Returns the value of field 'codigoProdutoPagamento'.
     * 
     * @return int
     * @return the value of field 'codigoProdutoPagamento'.
     */
    public int getCodigoProdutoPagamento()
    {
        return this._codigoProdutoPagamento;
    } //-- int getCodigoProdutoPagamento() 

    /**
     * Returns the value of field 'descricaoProdutoPagamento'.
     * 
     * @return String
     * @return the value of field 'descricaoProdutoPagamento'.
     */
    public java.lang.String getDescricaoProdutoPagamento()
    {
        return this._descricaoProdutoPagamento;
    } //-- java.lang.String getDescricaoProdutoPagamento() 

    /**
     * Method hasCodigoProdutoPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCodigoProdutoPagamento()
    {
        return this._has_codigoProdutoPagamento;
    } //-- boolean hasCodigoProdutoPagamento() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'codigoCentroCusto'.
     * 
     * @param codigoCentroCusto the value of field
     * 'codigoCentroCusto'.
     */
    public void setCodigoCentroCusto(java.lang.String codigoCentroCusto)
    {
        this._codigoCentroCusto = codigoCentroCusto;
    } //-- void setCodigoCentroCusto(java.lang.String) 

    /**
     * Sets the value of field 'codigoProdutoPagamento'.
     * 
     * @param codigoProdutoPagamento the value of field
     * 'codigoProdutoPagamento'.
     */
    public void setCodigoProdutoPagamento(int codigoProdutoPagamento)
    {
        this._codigoProdutoPagamento = codigoProdutoPagamento;
        this._has_codigoProdutoPagamento = true;
    } //-- void setCodigoProdutoPagamento(int) 

    /**
     * Sets the value of field 'descricaoProdutoPagamento'.
     * 
     * @param descricaoProdutoPagamento the value of field
     * 'descricaoProdutoPagamento'.
     */
    public void setDescricaoProdutoPagamento(java.lang.String descricaoProdutoPagamento)
    {
        this._descricaoProdutoPagamento = descricaoProdutoPagamento;
    } //-- void setDescricaoProdutoPagamento(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.montarcomboproduto.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.montarcomboproduto.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.montarcomboproduto.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.montarcomboproduto.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
