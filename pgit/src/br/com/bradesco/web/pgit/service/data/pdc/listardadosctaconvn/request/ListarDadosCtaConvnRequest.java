/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listardadosctaconvn.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ListarDadosCtaConvnRequest.
 * 
 * @version $Revision$ $Date$
 */
public class ListarDadosCtaConvnRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _nrOcorrencias
     */
    private int _nrOcorrencias = 0;

    /**
     * keeps track of state for field: _nrOcorrencias
     */
    private boolean _has_nrOcorrencias;

    /**
     * Field _cdPessoa
     */
    private long _cdPessoa = 0;

    /**
     * keeps track of state for field: _cdPessoa
     */
    private boolean _has_cdPessoa;

    /**
     * Field _cdTipoContratoNegocio
     */
    private int _cdTipoContratoNegocio = 0;

    /**
     * keeps track of state for field: _cdTipoContratoNegocio
     */
    private boolean _has_cdTipoContratoNegocio;

    /**
     * Field _nrSequenciaContratoNegocio
     */
    private long _nrSequenciaContratoNegocio = 0;

    /**
     * keeps track of state for field: _nrSequenciaContratoNegocio
     */
    private boolean _has_nrSequenciaContratoNegocio;

    /**
     * Field _cdClubPessoaRepresentante
     */
    private long _cdClubPessoaRepresentante = 0;

    /**
     * keeps track of state for field: _cdClubPessoaRepresentante
     */
    private boolean _has_cdClubPessoaRepresentante;

    /**
     * Field _cdCpfCnpjRepresentante
     */
    private long _cdCpfCnpjRepresentante = 0;

    /**
     * keeps track of state for field: _cdCpfCnpjRepresentante
     */
    private boolean _has_cdCpfCnpjRepresentante;

    /**
     * Field _cdFilialCnpjRepresentante
     */
    private int _cdFilialCnpjRepresentante = 0;

    /**
     * keeps track of state for field: _cdFilialCnpjRepresentante
     */
    private boolean _has_cdFilialCnpjRepresentante;

    /**
     * Field _cdControleCnpjRepresentante
     */
    private int _cdControleCnpjRepresentante = 0;

    /**
     * keeps track of state for field: _cdControleCnpjRepresentante
     */
    private boolean _has_cdControleCnpjRepresentante;

    /**
     * Field _cdParmConvenio
     */
    private int _cdParmConvenio = 0;

    /**
     * keeps track of state for field: _cdParmConvenio
     */
    private boolean _has_cdParmConvenio;


      //----------------/
     //- Constructors -/
    //----------------/

    public ListarDadosCtaConvnRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listardadosctaconvn.request.ListarDadosCtaConvnRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdClubPessoaRepresentante
     * 
     */
    public void deleteCdClubPessoaRepresentante()
    {
        this._has_cdClubPessoaRepresentante= false;
    } //-- void deleteCdClubPessoaRepresentante() 

    /**
     * Method deleteCdControleCnpjRepresentante
     * 
     */
    public void deleteCdControleCnpjRepresentante()
    {
        this._has_cdControleCnpjRepresentante= false;
    } //-- void deleteCdControleCnpjRepresentante() 

    /**
     * Method deleteCdCpfCnpjRepresentante
     * 
     */
    public void deleteCdCpfCnpjRepresentante()
    {
        this._has_cdCpfCnpjRepresentante= false;
    } //-- void deleteCdCpfCnpjRepresentante() 

    /**
     * Method deleteCdFilialCnpjRepresentante
     * 
     */
    public void deleteCdFilialCnpjRepresentante()
    {
        this._has_cdFilialCnpjRepresentante= false;
    } //-- void deleteCdFilialCnpjRepresentante() 

    /**
     * Method deleteCdParmConvenio
     * 
     */
    public void deleteCdParmConvenio()
    {
        this._has_cdParmConvenio= false;
    } //-- void deleteCdParmConvenio() 

    /**
     * Method deleteCdPessoa
     * 
     */
    public void deleteCdPessoa()
    {
        this._has_cdPessoa= false;
    } //-- void deleteCdPessoa() 

    /**
     * Method deleteCdTipoContratoNegocio
     * 
     */
    public void deleteCdTipoContratoNegocio()
    {
        this._has_cdTipoContratoNegocio= false;
    } //-- void deleteCdTipoContratoNegocio() 

    /**
     * Method deleteNrOcorrencias
     * 
     */
    public void deleteNrOcorrencias()
    {
        this._has_nrOcorrencias= false;
    } //-- void deleteNrOcorrencias() 

    /**
     * Method deleteNrSequenciaContratoNegocio
     * 
     */
    public void deleteNrSequenciaContratoNegocio()
    {
        this._has_nrSequenciaContratoNegocio= false;
    } //-- void deleteNrSequenciaContratoNegocio() 

    /**
     * Returns the value of field 'cdClubPessoaRepresentante'.
     * 
     * @return long
     * @return the value of field 'cdClubPessoaRepresentante'.
     */
    public long getCdClubPessoaRepresentante()
    {
        return this._cdClubPessoaRepresentante;
    } //-- long getCdClubPessoaRepresentante() 

    /**
     * Returns the value of field 'cdControleCnpjRepresentante'.
     * 
     * @return int
     * @return the value of field 'cdControleCnpjRepresentante'.
     */
    public int getCdControleCnpjRepresentante()
    {
        return this._cdControleCnpjRepresentante;
    } //-- int getCdControleCnpjRepresentante() 

    /**
     * Returns the value of field 'cdCpfCnpjRepresentante'.
     * 
     * @return long
     * @return the value of field 'cdCpfCnpjRepresentante'.
     */
    public long getCdCpfCnpjRepresentante()
    {
        return this._cdCpfCnpjRepresentante;
    } //-- long getCdCpfCnpjRepresentante() 

    /**
     * Returns the value of field 'cdFilialCnpjRepresentante'.
     * 
     * @return int
     * @return the value of field 'cdFilialCnpjRepresentante'.
     */
    public int getCdFilialCnpjRepresentante()
    {
        return this._cdFilialCnpjRepresentante;
    } //-- int getCdFilialCnpjRepresentante() 

    /**
     * Returns the value of field 'cdParmConvenio'.
     * 
     * @return int
     * @return the value of field 'cdParmConvenio'.
     */
    public int getCdParmConvenio()
    {
        return this._cdParmConvenio;
    } //-- int getCdParmConvenio() 

    /**
     * Returns the value of field 'cdPessoa'.
     * 
     * @return long
     * @return the value of field 'cdPessoa'.
     */
    public long getCdPessoa()
    {
        return this._cdPessoa;
    } //-- long getCdPessoa() 

    /**
     * Returns the value of field 'cdTipoContratoNegocio'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoNegocio'.
     */
    public int getCdTipoContratoNegocio()
    {
        return this._cdTipoContratoNegocio;
    } //-- int getCdTipoContratoNegocio() 

    /**
     * Returns the value of field 'nrOcorrencias'.
     * 
     * @return int
     * @return the value of field 'nrOcorrencias'.
     */
    public int getNrOcorrencias()
    {
        return this._nrOcorrencias;
    } //-- int getNrOcorrencias() 

    /**
     * Returns the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @return long
     * @return the value of field 'nrSequenciaContratoNegocio'.
     */
    public long getNrSequenciaContratoNegocio()
    {
        return this._nrSequenciaContratoNegocio;
    } //-- long getNrSequenciaContratoNegocio() 

    /**
     * Method hasCdClubPessoaRepresentante
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdClubPessoaRepresentante()
    {
        return this._has_cdClubPessoaRepresentante;
    } //-- boolean hasCdClubPessoaRepresentante() 

    /**
     * Method hasCdControleCnpjRepresentante
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdControleCnpjRepresentante()
    {
        return this._has_cdControleCnpjRepresentante;
    } //-- boolean hasCdControleCnpjRepresentante() 

    /**
     * Method hasCdCpfCnpjRepresentante
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCpfCnpjRepresentante()
    {
        return this._has_cdCpfCnpjRepresentante;
    } //-- boolean hasCdCpfCnpjRepresentante() 

    /**
     * Method hasCdFilialCnpjRepresentante
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFilialCnpjRepresentante()
    {
        return this._has_cdFilialCnpjRepresentante;
    } //-- boolean hasCdFilialCnpjRepresentante() 

    /**
     * Method hasCdParmConvenio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdParmConvenio()
    {
        return this._has_cdParmConvenio;
    } //-- boolean hasCdParmConvenio() 

    /**
     * Method hasCdPessoa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoa()
    {
        return this._has_cdPessoa;
    } //-- boolean hasCdPessoa() 

    /**
     * Method hasCdTipoContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoNegocio()
    {
        return this._has_cdTipoContratoNegocio;
    } //-- boolean hasCdTipoContratoNegocio() 

    /**
     * Method hasNrOcorrencias
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrOcorrencias()
    {
        return this._has_nrOcorrencias;
    } //-- boolean hasNrOcorrencias() 

    /**
     * Method hasNrSequenciaContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaContratoNegocio()
    {
        return this._has_nrSequenciaContratoNegocio;
    } //-- boolean hasNrSequenciaContratoNegocio() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdClubPessoaRepresentante'.
     * 
     * @param cdClubPessoaRepresentante the value of field
     * 'cdClubPessoaRepresentante'.
     */
    public void setCdClubPessoaRepresentante(long cdClubPessoaRepresentante)
    {
        this._cdClubPessoaRepresentante = cdClubPessoaRepresentante;
        this._has_cdClubPessoaRepresentante = true;
    } //-- void setCdClubPessoaRepresentante(long) 

    /**
     * Sets the value of field 'cdControleCnpjRepresentante'.
     * 
     * @param cdControleCnpjRepresentante the value of field
     * 'cdControleCnpjRepresentante'.
     */
    public void setCdControleCnpjRepresentante(int cdControleCnpjRepresentante)
    {
        this._cdControleCnpjRepresentante = cdControleCnpjRepresentante;
        this._has_cdControleCnpjRepresentante = true;
    } //-- void setCdControleCnpjRepresentante(int) 

    /**
     * Sets the value of field 'cdCpfCnpjRepresentante'.
     * 
     * @param cdCpfCnpjRepresentante the value of field
     * 'cdCpfCnpjRepresentante'.
     */
    public void setCdCpfCnpjRepresentante(long cdCpfCnpjRepresentante)
    {
        this._cdCpfCnpjRepresentante = cdCpfCnpjRepresentante;
        this._has_cdCpfCnpjRepresentante = true;
    } //-- void setCdCpfCnpjRepresentante(long) 

    /**
     * Sets the value of field 'cdFilialCnpjRepresentante'.
     * 
     * @param cdFilialCnpjRepresentante the value of field
     * 'cdFilialCnpjRepresentante'.
     */
    public void setCdFilialCnpjRepresentante(int cdFilialCnpjRepresentante)
    {
        this._cdFilialCnpjRepresentante = cdFilialCnpjRepresentante;
        this._has_cdFilialCnpjRepresentante = true;
    } //-- void setCdFilialCnpjRepresentante(int) 

    /**
     * Sets the value of field 'cdParmConvenio'.
     * 
     * @param cdParmConvenio the value of field 'cdParmConvenio'.
     */
    public void setCdParmConvenio(int cdParmConvenio)
    {
        this._cdParmConvenio = cdParmConvenio;
        this._has_cdParmConvenio = true;
    } //-- void setCdParmConvenio(int) 

    /**
     * Sets the value of field 'cdPessoa'.
     * 
     * @param cdPessoa the value of field 'cdPessoa'.
     */
    public void setCdPessoa(long cdPessoa)
    {
        this._cdPessoa = cdPessoa;
        this._has_cdPessoa = true;
    } //-- void setCdPessoa(long) 

    /**
     * Sets the value of field 'cdTipoContratoNegocio'.
     * 
     * @param cdTipoContratoNegocio the value of field
     * 'cdTipoContratoNegocio'.
     */
    public void setCdTipoContratoNegocio(int cdTipoContratoNegocio)
    {
        this._cdTipoContratoNegocio = cdTipoContratoNegocio;
        this._has_cdTipoContratoNegocio = true;
    } //-- void setCdTipoContratoNegocio(int) 

    /**
     * Sets the value of field 'nrOcorrencias'.
     * 
     * @param nrOcorrencias the value of field 'nrOcorrencias'.
     */
    public void setNrOcorrencias(int nrOcorrencias)
    {
        this._nrOcorrencias = nrOcorrencias;
        this._has_nrOcorrencias = true;
    } //-- void setNrOcorrencias(int) 

    /**
     * Sets the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @param nrSequenciaContratoNegocio the value of field
     * 'nrSequenciaContratoNegocio'.
     */
    public void setNrSequenciaContratoNegocio(long nrSequenciaContratoNegocio)
    {
        this._nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
        this._has_nrSequenciaContratoNegocio = true;
    } //-- void setNrSequenciaContratoNegocio(long) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ListarDadosCtaConvnRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listardadosctaconvn.request.ListarDadosCtaConvnRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listardadosctaconvn.request.ListarDadosCtaConvnRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listardadosctaconvn.request.ListarDadosCtaConvnRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listardadosctaconvn.request.ListarDadosCtaConvnRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
