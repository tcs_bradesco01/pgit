/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listardependdiretoria.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdEmpresa
     */
    private int _cdEmpresa = 0;

    /**
     * keeps track of state for field: _cdEmpresa
     */
    private boolean _has_cdEmpresa;

    /**
     * Field _cdDependencia
     */
    private int _cdDependencia = 0;

    /**
     * keeps track of state for field: _cdDependencia
     */
    private boolean _has_cdDependencia;

    /**
     * Field _dsDependencia
     */
    private java.lang.String _dsDependencia;

    /**
     * Field _cdNaturezaDependencia
     */
    private java.lang.String _cdNaturezaDependencia;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listardependdiretoria.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdDependencia
     * 
     */
    public void deleteCdDependencia()
    {
        this._has_cdDependencia= false;
    } //-- void deleteCdDependencia() 

    /**
     * Method deleteCdEmpresa
     * 
     */
    public void deleteCdEmpresa()
    {
        this._has_cdEmpresa= false;
    } //-- void deleteCdEmpresa() 

    /**
     * Returns the value of field 'cdDependencia'.
     * 
     * @return int
     * @return the value of field 'cdDependencia'.
     */
    public int getCdDependencia()
    {
        return this._cdDependencia;
    } //-- int getCdDependencia() 

    /**
     * Returns the value of field 'cdEmpresa'.
     * 
     * @return int
     * @return the value of field 'cdEmpresa'.
     */
    public int getCdEmpresa()
    {
        return this._cdEmpresa;
    } //-- int getCdEmpresa() 

    /**
     * Returns the value of field 'cdNaturezaDependencia'.
     * 
     * @return String
     * @return the value of field 'cdNaturezaDependencia'.
     */
    public java.lang.String getCdNaturezaDependencia()
    {
        return this._cdNaturezaDependencia;
    } //-- java.lang.String getCdNaturezaDependencia() 

    /**
     * Returns the value of field 'dsDependencia'.
     * 
     * @return String
     * @return the value of field 'dsDependencia'.
     */
    public java.lang.String getDsDependencia()
    {
        return this._dsDependencia;
    } //-- java.lang.String getDsDependencia() 

    /**
     * Method hasCdDependencia
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdDependencia()
    {
        return this._has_cdDependencia;
    } //-- boolean hasCdDependencia() 

    /**
     * Method hasCdEmpresa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdEmpresa()
    {
        return this._has_cdEmpresa;
    } //-- boolean hasCdEmpresa() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdDependencia'.
     * 
     * @param cdDependencia the value of field 'cdDependencia'.
     */
    public void setCdDependencia(int cdDependencia)
    {
        this._cdDependencia = cdDependencia;
        this._has_cdDependencia = true;
    } //-- void setCdDependencia(int) 

    /**
     * Sets the value of field 'cdEmpresa'.
     * 
     * @param cdEmpresa the value of field 'cdEmpresa'.
     */
    public void setCdEmpresa(int cdEmpresa)
    {
        this._cdEmpresa = cdEmpresa;
        this._has_cdEmpresa = true;
    } //-- void setCdEmpresa(int) 

    /**
     * Sets the value of field 'cdNaturezaDependencia'.
     * 
     * @param cdNaturezaDependencia the value of field
     * 'cdNaturezaDependencia'.
     */
    public void setCdNaturezaDependencia(java.lang.String cdNaturezaDependencia)
    {
        this._cdNaturezaDependencia = cdNaturezaDependencia;
    } //-- void setCdNaturezaDependencia(java.lang.String) 

    /**
     * Sets the value of field 'dsDependencia'.
     * 
     * @param dsDependencia the value of field 'dsDependencia'.
     */
    public void setDsDependencia(java.lang.String dsDependencia)
    {
        this._dsDependencia = dsDependencia;
    } //-- void setDsDependencia(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listardependdiretoria.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listardependdiretoria.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listardependdiretoria.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listardependdiretoria.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
