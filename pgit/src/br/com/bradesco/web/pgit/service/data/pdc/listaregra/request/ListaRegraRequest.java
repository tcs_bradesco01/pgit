/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id: ListaRegraRequest.java,v 1.4 2009/05/28 12:54:05 cpm.com.br\heslei.silva Exp $
 */

package br.com.bradesco.web.pgit.service.data.pdc.listaregra.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;

/**
 * Class ListaRegraRequest.
 * 
 * @version $Revision: 1.4 $ $Date: 2009/05/28 12:54:05 $
 */
public class ListaRegraRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _indGestorMaster
     */
    private java.lang.String _indGestorMaster;

    /**
     * Field _tipoConsulta
     */
    private int _tipoConsulta = 0;

    /**
     * keeps track of state for field: _tipoConsulta
     */
    private boolean _has_tipoConsulta;

    /**
     * Field _codRegraAlcada
     */
    private int _codRegraAlcada = 0;

    /**
     * keeps track of state for field: _codRegraAlcada
     */
    private boolean _has_codRegraAlcada;

    /**
     * Field _codSituacao
     */
    private int _codSituacao = 0;

    /**
     * keeps track of state for field: _codSituacao
     */
    private boolean _has_codSituacao;

    /**
     * Field _dataPesquisaInic
     */
    private java.lang.String _dataPesquisaInic;

    /**
     * Field _dataPesquisaFim
     */
    private java.lang.String _dataPesquisaFim;

    /**
     * Field _codPessoaJuridica
     */
    private long _codPessoaJuridica = 0;

    /**
     * keeps track of state for field: _codPessoaJuridica
     */
    private boolean _has_codPessoaJuridica;

    /**
     * Field _codDepartamento
     */
    private int _codDepartamento = 0;

    /**
     * keeps track of state for field: _codDepartamento
     */
    private boolean _has_codDepartamento;

  


      //----------------/
     //- Constructors -/
    //----------------/

    public ListaRegraRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listaregra.request.ListaRegraRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCodDepartamento
     * 
     */
    public void deleteCodDepartamento()
    {
        this._has_codDepartamento= false;
    } //-- void deleteCodDepartamento() 

    /**
     * Method deleteCodPessoaJuridica
     * 
     */
    public void deleteCodPessoaJuridica()
    {
        this._has_codPessoaJuridica= false;
    } //-- void deleteCodPessoaJuridica() 

    /**
     * Method deleteCodRegraAlcada
     * 
     */
    public void deleteCodRegraAlcada()
    {
        this._has_codRegraAlcada= false;
    } //-- void deleteCodRegraAlcada() 

    /**
     * Method deleteCodSituacao
     * 
     */
    public void deleteCodSituacao()
    {
        this._has_codSituacao= false;
    } //-- void deleteCodSituacao() 

    
    /**
     * Method deleteTipoConsulta
     * 
     */
    public void deleteTipoConsulta()
    {
        this._has_tipoConsulta= false;
    } //-- void deleteTipoConsulta() 

    /**
     * Returns the value of field 'codDepartamento'.
     * 
     * @return int
     * @return the value of field 'codDepartamento'.
     */
    public int getCodDepartamento()
    {
        return this._codDepartamento;
    } //-- int getCodDepartamento() 

    /**
     * Returns the value of field 'codPessoaJuridica'.
     * 
     * @return long
     * @return the value of field 'codPessoaJuridica'.
     */
    public long getCodPessoaJuridica()
    {
        return this._codPessoaJuridica;
    } //-- long getCodPessoaJuridica() 

    /**
     * Returns the value of field 'codRegraAlcada'.
     * 
     * @return int
     * @return the value of field 'codRegraAlcada'.
     */
    public int getCodRegraAlcada()
    {
        return this._codRegraAlcada;
    } //-- int getCodRegraAlcada() 

    /**
     * Returns the value of field 'codSituacao'.
     * 
     * @return int
     * @return the value of field 'codSituacao'.
     */
    public int getCodSituacao()
    {
        return this._codSituacao;
    } //-- int getCodSituacao() 



    /**
     * Returns the value of field 'dataPesquisaFim'.
     * 
     * @return String
     * @return the value of field 'dataPesquisaFim'.
     */
    public java.lang.String getDataPesquisaFim()
    {
        return this._dataPesquisaFim;
    } //-- java.lang.String getDataPesquisaFim() 

    /**
     * Returns the value of field 'dataPesquisaInic'.
     * 
     * @return String
     * @return the value of field 'dataPesquisaInic'.
     */
    public java.lang.String getDataPesquisaInic()
    {
        return this._dataPesquisaInic;
    } //-- java.lang.String getDataPesquisaInic() 

    /**
     * Returns the value of field 'indGestorMaster'.
     * 
     * @return String
     * @return the value of field 'indGestorMaster'.
     */
    public java.lang.String getIndGestorMaster()
    {
        return this._indGestorMaster;
    } //-- java.lang.String getIndGestorMaster() 

    /**
     * Returns the value of field 'tipoConsulta'.
     * 
     * @return int
     * @return the value of field 'tipoConsulta'.
     */
    public int getTipoConsulta()
    {
        return this._tipoConsulta;
    } //-- int getTipoConsulta() 

    /**
     * Method hasCodDepartamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCodDepartamento()
    {
        return this._has_codDepartamento;
    } //-- boolean hasCodDepartamento() 

    /**
     * Method hasCodPessoaJuridica
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCodPessoaJuridica()
    {
        return this._has_codPessoaJuridica;
    } //-- boolean hasCodPessoaJuridica() 

    /**
     * Method hasCodRegraAlcada
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCodRegraAlcada()
    {
        return this._has_codRegraAlcada;
    } //-- boolean hasCodRegraAlcada() 

    /**
     * Method hasCodSituacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCodSituacao()
    {
        return this._has_codSituacao;
    } //-- boolean hasCodSituacao() 



    /**
     * Method hasTipoConsulta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasTipoConsulta()
    {
        return this._has_tipoConsulta;
    } //-- boolean hasTipoConsulta() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'codDepartamento'.
     * 
     * @param codDepartamento the value of field 'codDepartamento'.
     */
    public void setCodDepartamento(int codDepartamento)
    {
        this._codDepartamento = codDepartamento;
        this._has_codDepartamento = true;
    } //-- void setCodDepartamento(int) 

    /**
     * Sets the value of field 'codPessoaJuridica'.
     * 
     * @param codPessoaJuridica the value of field
     * 'codPessoaJuridica'.
     */
    public void setCodPessoaJuridica(long codPessoaJuridica)
    {
        this._codPessoaJuridica = codPessoaJuridica;
        this._has_codPessoaJuridica = true;
    } //-- void setCodPessoaJuridica(long) 

    /**
     * Sets the value of field 'codRegraAlcada'.
     * 
     * @param codRegraAlcada the value of field 'codRegraAlcada'.
     */
    public void setCodRegraAlcada(int codRegraAlcada)
    {
        this._codRegraAlcada = codRegraAlcada;
        this._has_codRegraAlcada = true;
    } //-- void setCodRegraAlcada(int) 

    /**
     * Sets the value of field 'codSituacao'.
     * 
     * @param codSituacao the value of field 'codSituacao'.
     */
    public void setCodSituacao(int codSituacao)
    {
        this._codSituacao = codSituacao;
        this._has_codSituacao = true;
    } //-- void setCodSituacao(int) 

    /**
     * Sets the value of field 'dataPesquisaFim'.
     * 
     * @param dataPesquisaFim the value of field 'dataPesquisaFim'.
     */
    public void setDataPesquisaFim(java.lang.String dataPesquisaFim)
    {
        this._dataPesquisaFim = dataPesquisaFim;
    } //-- void setDataPesquisaFim(java.lang.String) 

    /**
     * Sets the value of field 'dataPesquisaInic'.
     * 
     * @param dataPesquisaInic the value of field 'dataPesquisaInic'
     */
    public void setDataPesquisaInic(java.lang.String dataPesquisaInic)
    {
        this._dataPesquisaInic = dataPesquisaInic;
    } //-- void setDataPesquisaInic(java.lang.String) 

    /**
     * Sets the value of field 'indGestorMaster'.
     * 
     * @param indGestorMaster the value of field 'indGestorMaster'.
     */
    public void setIndGestorMaster(java.lang.String indGestorMaster)
    {
        this._indGestorMaster = indGestorMaster;
    } //-- void setIndGestorMaster(java.lang.String) 

    /**
     * Sets the value of field 'tipoConsulta'.
     * 
     * @param tipoConsulta the value of field 'tipoConsulta'.
     */
    public void setTipoConsulta(int tipoConsulta)
    {
        this._tipoConsulta = tipoConsulta;
        this._has_tipoConsulta = true;
    } //-- void setTipoConsulta(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ListaRegraRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listaregra.request.ListaRegraRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listaregra.request.ListaRegraRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listaregra.request.ListaRegraRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listaregra.request.ListaRegraRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
