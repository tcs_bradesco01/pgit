/*
 * Nome: br.com.bradesco.web.pgit.service.business.combo.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.combo.bean;


/**
 * Nome: PeriodicidadeSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class PeriodicidadeSaidaDTO {
	
	/** Atributo codMensagem. */
	private String codMensagem;
	
	/** Atributo mensagem. */
	private String mensagem;
	
	/** Atributo cdPeriodicidade. */
	private int cdPeriodicidade;
	
	/** Atributo dsPeriodicidade. */
	private String dsPeriodicidade;
	
	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}
	
	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	/**
	 * Get: cdPeriodicidade.
	 *
	 * @return cdPeriodicidade
	 */
	public int getCdPeriodicidade() {
		return cdPeriodicidade;
	}
	
	/**
	 * Set: cdPeriodicidade.
	 *
	 * @param cdPeriodicidade the cd periodicidade
	 */
	public void setCdPeriodicidade(int cdPeriodicidade) {
		this.cdPeriodicidade = cdPeriodicidade;
	}
	
	/**
	 * Get: dsPeriodicidade.
	 *
	 * @return dsPeriodicidade
	 */
	public String getDsPeriodicidade() {
		return dsPeriodicidade;
	}
	
	/**
	 * Set: dsPeriodicidade.
	 *
	 * @param dsPeriodicidade the ds periodicidade
	 */
	public void setDsPeriodicidade(String dsPeriodicidade) {
		this.dsPeriodicidade = dsPeriodicidade;
	}

	/**
	 * (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof PeriodicidadeSaidaDTO)) {
			return false;
		}

		PeriodicidadeSaidaDTO other = (PeriodicidadeSaidaDTO) obj;
		return this.getCdPeriodicidade() == other.getCdPeriodicidade();
	}

	/**
	 * (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return getCdPeriodicidade();
	}

}
