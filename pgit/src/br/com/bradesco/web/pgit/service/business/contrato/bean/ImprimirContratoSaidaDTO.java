/*
 * Nome: br.com.bradesco.web.pgit.service.business.contrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 19/02/2016
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.contrato.bean;

import java.util.List;

/**
 * Nome: ImprimirContratoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ImprimirContratoSaidaDTO {

    /** The cod mensagem. */
    private String codMensagem = null;

    /** The mensagem. */
    private String mensagem = null;

    /** The cd banco. */
    private Integer cdBanco = null;

    /** The ds banco. */
    private String dsBanco = null;

    /** The cd cpf cnpj. */
    private String cdCpfCnpj = null;

    /** The ds logradouro pagador. */
    private String dsLogradouroPagador = null;

    /** The ds numero logradouro pagador. */
    private String dsNumeroLogradouroPagador = null;

    /** The ds complemento logradouro pagador. */
    private String dsComplementoLogradouroPagador = null;

    /** The ds bairro cliente pagador. */
    private String dsBairroClientePagador = null;

    /** The ds municipio cliente pagador. */
    private String dsMunicipioClientePagador = null;

    /** The cd sigla uf pagador. */
    private String cdSiglaUfPagador = null;

    /** The cd cep pagador. */
    private Integer cdCepPagador = null;

    /** The cd cep complemento pagador. */
    private Integer cdCepComplementoPagador = null;

    /** The cd pessoa juridica contrato. */
    private Long cdPessoaJuridicaContrato = null;

    /** The cd tipo contrato negocio. */
    private Integer cdTipoContratoNegocio = null;

    /** The nr sequencia contrato negocio. */
    private Long nrSequenciaContratoNegocio = null;

    /** The cd agencia gestora. */
    private Integer cdAgenciaGestora = null;

    /** The ds agencia gestora. */
    private String dsAgenciaGestora = null;

    /** The cd forma autorizacao pagamento. */
    private Integer cdFormaAutorizacaoPagamento = null;

    /** The nr aditivo contrato negocio. */
    private Long nrAditivoContratoNegocio = null;

    /** The dt geracao contrato negocio. */
    private String dtGeracaoContratoNegocio = null;

    /** The cd transferencia conta salarial. */
    private String cdTransferenciaContaSalarial = null;

    /** The hr limite transmissao arquivo. */
    private String hrLimiteTransmissaoArquivo = null;

    /** The hr limite pagamento remessa. */
    private String hrLimitePagamentoRemessa = null;

    /** The hr limite superior boleto. */
    private String hrLimiteSuperiorBoleto = null;

    /** The hr limite inferior boleto. */
    private String hrLimiteInferiorBoleto = null;

    /** The hr limite pagamento superior. */
    private String hrLimitePagamentoSuperior = null;

    /** The hr limite pagamento inferior. */
    private String hrLimitePagamentoInferior = null;

    /** The hr limite efetivacao pagamento. */
    private String hrLimiteEfetivacaoPagamento = null;

    /** The hr limite consulta pagamento. */
    private String hrLimiteConsultaPagamento = null;

    /** The hr ciclo efetivacao pagamento. */
    private String hrCicloEfetivacaoPagamento = null;

    /** The nr quantidade limite cobranca impressao. */
    private String nrQuantidadeLimiteCobrancaImpressao = null;

    /** The ds disponibilizacao comprovante banco. */
    private String dsDisponibilizacaoComprovanteBanco = null;

    /** The hr limite apresenta carta. */
    private String hrLimiteApresentaCarta = null;

    /** The nr montante boleto. */
    private String nrMontanteBoleto = null;

    /** The ds frase padrao caput. */
    private String dsFrasePadraoCaput = null;

    /** The ds detalhe modalidade doc. */
    private String dsDetalheModalidadeDoc = null;

    /** The ds detalhe modalidade ted. */
    private String dsDetalheModalidadeTed = null;

    /** The ds detalhe modalidade op. */
    private String dsDetalheModalidadeOp = null;

    /** The cd indicador servico fornecedor. */
    private String cdIndicadorServicoFornecedor = null;

    /** The cd produto servico fornecedor. */
    private Integer cdProdutoServicoFornecedor = null;

    /** The ds produto servico fornecedor. */
    private String dsProdutoServicoFornecedor = null;

     /** The cd indicador servico tributos. */
    private String cdIndicadorServicoTributos = null;

    /** The cd produto servico tributos. */
    private Integer cdProdutoServicoTributos = null;

    /** The ds produto servico tributos. */
    private String dsProdutoServicoTributos = null;

    /** The cd indicador servico salario. */
    private String cdIndicadorServicoSalario = null;

    /** The cd produto servico salario. */
    private Integer cdProdutoServicoSalario = null;

    /** The ds produto servico salario. */
    private String dsProdutoServicoSalario = null;

    /** The ds municipio agencia gestora. */
    private String dsMunicipioAgenciaGestora = null;
    
    /** The ds sigla uf participante. */
    private String dsSiglaUfParticipante = null;

    /** The cd floating servico aplicacao salario. */
    private Integer cdFloatingServicoAplicacaoSalario = null;

    /** The ds floating servico aplicacao salario. */
    private String dsFloatingServicoAplicacaoSalario = null;
    
    /** The cd indicador comprovante salarial. */
    private String cdIndicadorComprovanteSalarial = null; 
    
    /** The cd float servico aplicacao fornecedor. */
    private Integer cdFloatServicoAplicacaoFornecedor = null; 
    
    /** The ds float servico aplicacao fornecedor. */
    private String dsFloatServicoAplicacaoFornecedor = null; 

    /** Atributo ocorrenciasFornecedor. */
    private List<ImprimirContratoOcorrenciasSaidaDTO> ocorrenciasFornecedor = null;
    
    /** Atributo ocorrenciasTributo. */
    private List<ImprimirContratoOcorrencias1SaidaDTO> ocorrenciasTributo = null;
    
    /** Atributo ocorrenciasSalario. */
    private List<ImprimirContratoOcorrencias2SaidaDTO> ocorrenciasSalario = null;

    /** Atributo ocorrenciasFuncionario. */
    private List<ImprimirContratoOcorrencias3SaidaDTO> ocorrenciasFuncionario = null;

    /** Atributo ocorrenciasGuicheCaixa. */
    private List<ImprimirContratoOcorrencias4SaidaDTO> ocorrenciasGuicheCaixa = null;

    /** Atributo ocorrenciasAutoAtendimento. */
    private List<ImprimirContratoOcorrencias5SaidaDTO> ocorrenciasAutoAtendimento = null;

    /** Atributo ocorrenciasFoneFacil. */
    private List<ImprimirContratoOcorrencias6SaidaDTO> ocorrenciasFoneFacil = null;

    /** Atributo ocorrenciasInternet. */
    private List<ImprimirContratoOcorrencias7SaidaDTO> ocorrenciasInternet = null;
    
    /**
     * Instancia um novo ImprimirContratoSaidaDTO.
     *
     * @see
     */
    public ImprimirContratoSaidaDTO() {
        super();
    }

    /**
     * Sets the cod mensagem.
     *
     * @param codMensagem the cod mensagem
     */
    public void setCodMensagem(String codMensagem) {
        this.codMensagem = codMensagem;
    }

    /**
     * Gets the cod mensagem.
     *
     * @return the cod mensagem
     */
    public String getCodMensagem() {
        return this.codMensagem;
    }

    /**
     * Sets the mensagem.
     *
     * @param mensagem the mensagem
     */
    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }

    /**
     * Gets the mensagem.
     *
     * @return the mensagem
     */
    public String getMensagem() {
        return this.mensagem;
    }

    /**
     * Sets the cd banco.
     *
     * @param cdBanco the cd banco
     */
    public void setCdBanco(Integer cdBanco) {
        this.cdBanco = cdBanco;
    }

    /**
     * Gets the cd banco.
     *
     * @return the cd banco
     */
    public Integer getCdBanco() {
        return this.cdBanco;
    }

    /**
     * Sets the ds banco.
     *
     * @param dsBanco the ds banco
     */
    public void setDsBanco(String dsBanco) {
        this.dsBanco = dsBanco;
    }

    /**
     * Gets the ds banco.
     *
     * @return the ds banco
     */
    public String getDsBanco() {
        return this.dsBanco;
    }

    /**
     * Sets the cd cpf cnpj.
     *
     * @param cdCpfCnpj the cd cpf cnpj
     */
    public void setCdCpfCnpj(String cdCpfCnpj) {
        this.cdCpfCnpj = cdCpfCnpj;
    }

    /**
     * Gets the cd cpf cnpj.
     *
     * @return the cd cpf cnpj
     */
    public String getCdCpfCnpj() {
        return this.cdCpfCnpj;
    }

    /**
     * Sets the ds logradouro pagador.
     *
     * @param dsLogradouroPagador the ds logradouro pagador
     */
    public void setDsLogradouroPagador(String dsLogradouroPagador) {
        this.dsLogradouroPagador = dsLogradouroPagador;
    }

    /**
     * Gets the ds logradouro pagador.
     *
     * @return the ds logradouro pagador
     */
    public String getDsLogradouroPagador() {
        return this.dsLogradouroPagador;
    }

    /**
     * Sets the ds numero logradouro pagador.
     *
     * @param dsNumeroLogradouroPagador the ds numero logradouro pagador
     */
    public void setDsNumeroLogradouroPagador(String dsNumeroLogradouroPagador) {
        this.dsNumeroLogradouroPagador = dsNumeroLogradouroPagador;
    }

    /**
     * Gets the ds numero logradouro pagador.
     *
     * @return the ds numero logradouro pagador
     */
    public String getDsNumeroLogradouroPagador() {
        return this.dsNumeroLogradouroPagador;
    }

    /**
     * Sets the ds complemento logradouro pagador.
     *
     * @param dsComplementoLogradouroPagador the ds complemento logradouro pagador
     */
    public void setDsComplementoLogradouroPagador(String dsComplementoLogradouroPagador) {
        this.dsComplementoLogradouroPagador = dsComplementoLogradouroPagador;
    }

    /**
     * Gets the ds complemento logradouro pagador.
     *
     * @return the ds complemento logradouro pagador
     */
    public String getDsComplementoLogradouroPagador() {
        return this.dsComplementoLogradouroPagador;
    }

    /**
     * Sets the ds bairro cliente pagador.
     *
     * @param dsBairroClientePagador the ds bairro cliente pagador
     */
    public void setDsBairroClientePagador(String dsBairroClientePagador) {
        this.dsBairroClientePagador = dsBairroClientePagador;
    }

    /**
     * Gets the ds bairro cliente pagador.
     *
     * @return the ds bairro cliente pagador
     */
    public String getDsBairroClientePagador() {
        return this.dsBairroClientePagador;
    }

    /**
     * Sets the ds municipio cliente pagador.
     *
     * @param dsMunicipioClientePagador the ds municipio cliente pagador
     */
    public void setDsMunicipioClientePagador(String dsMunicipioClientePagador) {
        this.dsMunicipioClientePagador = dsMunicipioClientePagador;
    }

    /**
     * Gets the ds municipio cliente pagador.
     *
     * @return the ds municipio cliente pagador
     */
    public String getDsMunicipioClientePagador() {
        return this.dsMunicipioClientePagador;
    }

    /**
     * Sets the cd sigla uf pagador.
     *
     * @param cdSiglaUfPagador the cd sigla uf pagador
     */
    public void setCdSiglaUfPagador(String cdSiglaUfPagador) {
        this.cdSiglaUfPagador = cdSiglaUfPagador;
    }

    /**
     * Gets the cd sigla uf pagador.
     *
     * @return the cd sigla uf pagador
     */
    public String getCdSiglaUfPagador() {
        return this.cdSiglaUfPagador;
    }

    /**
     * Sets the cd cep pagador.
     *
     * @param cdCepPagador the cd cep pagador
     */
    public void setCdCepPagador(Integer cdCepPagador) {
        this.cdCepPagador = cdCepPagador;
    }

    /**
     * Gets the cd cep pagador.
     *
     * @return the cd cep pagador
     */
    public Integer getCdCepPagador() {
        return this.cdCepPagador;
    }

    /**
     * Sets the cd cep complemento pagador.
     *
     * @param cdCepComplementoPagador the cd cep complemento pagador
     */
    public void setCdCepComplementoPagador(Integer cdCepComplementoPagador) {
        this.cdCepComplementoPagador = cdCepComplementoPagador;
    }

    /**
     * Gets the cd cep complemento pagador.
     *
     * @return the cd cep complemento pagador
     */
    public Integer getCdCepComplementoPagador() {
        return this.cdCepComplementoPagador;
    }

    /**
     * Sets the cd pessoa juridica contrato.
     *
     * @param cdPessoaJuridicaContrato the cd pessoa juridica contrato
     */
    public void setCdPessoaJuridicaContrato(Long cdPessoaJuridicaContrato) {
        this.cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
    }

    /**
     * Gets the cd pessoa juridica contrato.
     *
     * @return the cd pessoa juridica contrato
     */
    public Long getCdPessoaJuridicaContrato() {
        return this.cdPessoaJuridicaContrato;
    }

    /**
     * Sets the cd tipo contrato negocio.
     *
     * @param cdTipoContratoNegocio the cd tipo contrato negocio
     */
    public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
        this.cdTipoContratoNegocio = cdTipoContratoNegocio;
    }

    /**
     * Gets the cd tipo contrato negocio.
     *
     * @return the cd tipo contrato negocio
     */
    public Integer getCdTipoContratoNegocio() {
        return this.cdTipoContratoNegocio;
    }

    /**
     * Sets the nr sequencia contrato negocio.
     *
     * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
     */
    public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
        this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
    }

    /**
     * Gets the nr sequencia contrato negocio.
     *
     * @return the nr sequencia contrato negocio
     */
    public Long getNrSequenciaContratoNegocio() {
        return this.nrSequenciaContratoNegocio;
    }

    /**
     * Sets the cd agencia gestora.
     *
     * @param cdAgenciaGestora the cd agencia gestora
     */
    public void setCdAgenciaGestora(Integer cdAgenciaGestora) {
        this.cdAgenciaGestora = cdAgenciaGestora;
    }

    /**
     * Gets the cd agencia gestora.
     *
     * @return the cd agencia gestora
     */
    public Integer getCdAgenciaGestora() {
        return this.cdAgenciaGestora;
    }

    /**
     * Sets the ds agencia gestora.
     *
     * @param dsAgenciaGestora the ds agencia gestora
     */
    public void setDsAgenciaGestora(String dsAgenciaGestora) {
        this.dsAgenciaGestora = dsAgenciaGestora;
    }

    /**
     * Gets the ds agencia gestora.
     *
     * @return the ds agencia gestora
     */
    public String getDsAgenciaGestora() {
        return this.dsAgenciaGestora;
    }

    /**
     * Sets the cd forma autorizacao pagamento.
     *
     * @param cdFormaAutorizacaoPagamento the cd forma autorizacao pagamento
     */
    public void setCdFormaAutorizacaoPagamento(Integer cdFormaAutorizacaoPagamento) {
        this.cdFormaAutorizacaoPagamento = cdFormaAutorizacaoPagamento;
    }

    /**
     * Gets the cd forma autorizacao pagamento.
     *
     * @return the cd forma autorizacao pagamento
     */
    public Integer getCdFormaAutorizacaoPagamento() {
        return this.cdFormaAutorizacaoPagamento;
    }

    /**
     * Sets the nr aditivo contrato negocio.
     *
     * @param nrAditivoContratoNegocio the nr aditivo contrato negocio
     */
    public void setNrAditivoContratoNegocio(Long nrAditivoContratoNegocio) {
        this.nrAditivoContratoNegocio = nrAditivoContratoNegocio;
    }

    /**
     * Gets the nr aditivo contrato negocio.
     *
     * @return the nr aditivo contrato negocio
     */
    public Long getNrAditivoContratoNegocio() {
        return this.nrAditivoContratoNegocio;
    }

    /**
     * Sets the dt geracao contrato negocio.
     *
     * @param dtGeracaoContratoNegocio the dt geracao contrato negocio
     */
    public void setDtGeracaoContratoNegocio(String dtGeracaoContratoNegocio) {
        this.dtGeracaoContratoNegocio = dtGeracaoContratoNegocio;
    }

    /**
     * Gets the dt geracao contrato negocio.
     *
     * @return the dt geracao contrato negocio
     */
    public String getDtGeracaoContratoNegocio() {
        return this.dtGeracaoContratoNegocio;
    }

    /**
     * Sets the cd transferencia conta salarial.
     *
     * @param cdTransferenciaContaSalarial the cd transferencia conta salarial
     */
    public void setCdTransferenciaContaSalarial(String cdTransferenciaContaSalarial) {
        this.cdTransferenciaContaSalarial = cdTransferenciaContaSalarial;
    }

    /**
     * Gets the cd transferencia conta salarial.
     *
     * @return the cd transferencia conta salarial
     */
    public String getCdTransferenciaContaSalarial() {
        return this.cdTransferenciaContaSalarial;
    }

    /**
     * Sets the hr limite transmissao arquivo.
     *
     * @param hrLimiteTransmissaoArquivo the hr limite transmissao arquivo
     */
    public void setHrLimiteTransmissaoArquivo(String hrLimiteTransmissaoArquivo) {
        this.hrLimiteTransmissaoArquivo = hrLimiteTransmissaoArquivo;
    }

    /**
     * Gets the hr limite transmissao arquivo.
     *
     * @return the hr limite transmissao arquivo
     */
    public String getHrLimiteTransmissaoArquivo() {
        return this.hrLimiteTransmissaoArquivo;
    }

    /**
     * Sets the hr limite pagamento remessa.
     *
     * @param hrLimitePagamentoRemessa the hr limite pagamento remessa
     */
    public void setHrLimitePagamentoRemessa(String hrLimitePagamentoRemessa) {
        this.hrLimitePagamentoRemessa = hrLimitePagamentoRemessa;
    }

    /**
     * Gets the hr limite pagamento remessa.
     *
     * @return the hr limite pagamento remessa
     */
    public String getHrLimitePagamentoRemessa() {
        return this.hrLimitePagamentoRemessa;
    }

    /**
     * Sets the hr limite superior boleto.
     *
     * @param hrLimiteSuperiorBoleto the hr limite superior boleto
     */
    public void setHrLimiteSuperiorBoleto(String hrLimiteSuperiorBoleto) {
        this.hrLimiteSuperiorBoleto = hrLimiteSuperiorBoleto;
    }

    /**
     * Gets the hr limite superior boleto.
     *
     * @return the hr limite superior boleto
     */
    public String getHrLimiteSuperiorBoleto() {
        return this.hrLimiteSuperiorBoleto;
    }

    /**
     * Sets the hr limite inferior boleto.
     *
     * @param hrLimiteInferiorBoleto the hr limite inferior boleto
     */
    public void sethrLimiteInferiorBoleto(String hrLimiteInferiorBoleto) {
        this.hrLimiteInferiorBoleto = hrLimiteInferiorBoleto;
    }

    /**
     * Gets the hr limite inferior boleto.
     *
     * @return the hr limite inferior boleto
     */
    public String gethrLimiteInferiorBoleto() {
        return this.hrLimiteInferiorBoleto;
    }

    /**
     * Sets the hr limite pagamento superior.
     *
     * @param hrLimitePagamentoSuperior the hr limite pagamento superior
     */
    public void setHrLimitePagamentoSuperior(String hrLimitePagamentoSuperior) {
        this.hrLimitePagamentoSuperior = hrLimitePagamentoSuperior;
    }

    /**
     * Gets the hr limite pagamento superior.
     *
     * @return the hr limite pagamento superior
     */
    public String getHrLimitePagamentoSuperior() {
        return this.hrLimitePagamentoSuperior;
    }

    /**
     * Sets the hr limite pagamento inferior.
     *
     * @param hrLimitePagamentoInferior the hr limite pagamento inferior
     */
    public void setHrLimitePagamentoInferior(String hrLimitePagamentoInferior) {
        this.hrLimitePagamentoInferior = hrLimitePagamentoInferior;
    }

    /**
     * Gets the hr limite pagamento inferior.
     *
     * @return the hr limite pagamento inferior
     */
    public String getHrLimitePagamentoInferior() {
        return this.hrLimitePagamentoInferior;
    }

    /**
     * Sets the hr limite efetivacao pagamento.
     *
     * @param hrLimiteEfetivacaoPagamento the hr limite efetivacao pagamento
     */
    public void setHrLimiteEfetivacaoPagamento(String hrLimiteEfetivacaoPagamento) {
        this.hrLimiteEfetivacaoPagamento = hrLimiteEfetivacaoPagamento;
    }

    /**
     * Gets the hr limite efetivacao pagamento.
     *
     * @return the hr limite efetivacao pagamento
     */
    public String getHrLimiteEfetivacaoPagamento() {
        return this.hrLimiteEfetivacaoPagamento;
    }

    /**
     * Sets the hr limite consulta pagamento.
     *
     * @param hrLimiteConsultaPagamento the hr limite consulta pagamento
     */
    public void setHrLimiteConsultaPagamento(String hrLimiteConsultaPagamento) {
        this.hrLimiteConsultaPagamento = hrLimiteConsultaPagamento;
    }

    /**
     * Gets the hr limite consulta pagamento.
     *
     * @return the hr limite consulta pagamento
     */
    public String getHrLimiteConsultaPagamento() {
        return this.hrLimiteConsultaPagamento;
    }

    /**
     * Sets the hr ciclo efetivacao pagamento.
     *
     * @param hrCicloEfetivacaoPagamento the hr ciclo efetivacao pagamento
     */
    public void setHrCicloEfetivacaoPagamento(String hrCicloEfetivacaoPagamento) {
        this.hrCicloEfetivacaoPagamento = hrCicloEfetivacaoPagamento;
    }

    /**
     * Gets the hr ciclo efetivacao pagamento.
     *
     * @return the hr ciclo efetivacao pagamento
     */
    public String getHrCicloEfetivacaoPagamento() {
        return this.hrCicloEfetivacaoPagamento;
    }

    /**
     * Sets the nr quantidade limite cobranca impressao.
     *
     * @param nrQuantidadeLimiteCobrancaImpressao the nr quantidade limite cobranca impressao
     */
    public void setNrQuantidadeLimiteCobrancaImpressao(String nrQuantidadeLimiteCobrancaImpressao) {
        this.nrQuantidadeLimiteCobrancaImpressao = nrQuantidadeLimiteCobrancaImpressao;
    }

    /**
     * Gets the nr quantidade limite cobranca impressao.
     *
     * @return the nr quantidade limite cobranca impressao
     */
    public String getNrQuantidadeLimiteCobrancaImpressao() {
        return this.nrQuantidadeLimiteCobrancaImpressao;
    }

    /**
     * Sets the ds disponibilizacao comprovante banco.
     *
     * @param dsDisponibilizacaoComprovanteBanco the ds disponibilizacao comprovante banco
     */
    public void setDsDisponibilizacaoComprovanteBanco(String dsDisponibilizacaoComprovanteBanco) {
        this.dsDisponibilizacaoComprovanteBanco = dsDisponibilizacaoComprovanteBanco;
    }

    /**
     * Gets the ds disponibilizacao comprovante banco.
     *
     * @return the ds disponibilizacao comprovante banco
     */
    public String getDsDisponibilizacaoComprovanteBanco() {
        return this.dsDisponibilizacaoComprovanteBanco;
    }

    /**
     * Sets the hr limite apresenta carta.
     *
     * @param hrLimiteApresentaCarta the hr limite apresenta carta
     */
    public void setHrLimiteApresentaCarta(String hrLimiteApresentaCarta) {
        this.hrLimiteApresentaCarta = hrLimiteApresentaCarta;
    }

    /**
     * Gets the hr limite apresenta carta.
     *
     * @return the hr limite apresenta carta
     */
    public String getHrLimiteApresentaCarta() {
        return this.hrLimiteApresentaCarta;
    }

    /**
     * Sets the nr montante boleto.
     *
     * @param nrMontanteBoleto the nr montante boleto
     */
    public void setNrMontanteBoleto(String nrMontanteBoleto) {
        this.nrMontanteBoleto = nrMontanteBoleto;
    }

    /**
     * Gets the nr montante boleto.
     *
     * @return the nr montante boleto
     */
    public String getNrMontanteBoleto() {
        return this.nrMontanteBoleto;
    }

    /**
     * Sets the ds frase padrao caput.
     *
     * @param dsFrasePadraoCaput the ds frase padrao caput
     */
    public void setDsFrasePadraoCaput(String dsFrasePadraoCaput) {
        this.dsFrasePadraoCaput = dsFrasePadraoCaput;
    }

    /**
     * Gets the ds frase padrao caput.
     *
     * @return the ds frase padrao caput
     */
    public String getDsFrasePadraoCaput() {
        return this.dsFrasePadraoCaput;
    }

    /**
     * Sets the ds detalhe modalidade doc.
     *
     * @param dsDetalheModalidadeDoc the ds detalhe modalidade doc
     */
    public void setDsDetalheModalidadeDoc(String dsDetalheModalidadeDoc) {
        this.dsDetalheModalidadeDoc = dsDetalheModalidadeDoc;
    }

    /**
     * Gets the ds detalhe modalidade doc.
     *
     * @return the ds detalhe modalidade doc
     */
    public String getDsDetalheModalidadeDoc() {
        return this.dsDetalheModalidadeDoc;
    }

    /**
     * Sets the ds detalhe modalidade ted.
     *
     * @param dsDetalheModalidadeTed the ds detalhe modalidade ted
     */
    public void setDsDetalheModalidadeTed(String dsDetalheModalidadeTed) {
        this.dsDetalheModalidadeTed = dsDetalheModalidadeTed;
    }

    /**
     * Gets the ds detalhe modalidade ted.
     *
     * @return the ds detalhe modalidade ted
     */
    public String getDsDetalheModalidadeTed() {
        return this.dsDetalheModalidadeTed;
    }

    /**
     * Sets the ds detalhe modalidade op.
     *
     * @param dsDetalheModalidadeOp the ds detalhe modalidade op
     */
    public void setDsDetalheModalidadeOp(String dsDetalheModalidadeOp) {
        this.dsDetalheModalidadeOp = dsDetalheModalidadeOp;
    }

    /**
     * Gets the ds detalhe modalidade op.
     *
     * @return the ds detalhe modalidade op
     */
    public String getDsDetalheModalidadeOp() {
        return this.dsDetalheModalidadeOp;
    }

    /**
     * Sets the cd indicador servico fornecedor.
     *
     * @param cdIndicadorServicoFornecedor the cd indicador servico fornecedor
     */
    public void setCdIndicadorServicoFornecedor(String cdIndicadorServicoFornecedor) {
        this.cdIndicadorServicoFornecedor = cdIndicadorServicoFornecedor;
    }

    /**
     * Gets the cd indicador servico fornecedor.
     *
     * @return the cd indicador servico fornecedor
     */
    public String getCdIndicadorServicoFornecedor() {
        return this.cdIndicadorServicoFornecedor;
    }

    /**
     * Sets the cd produto servico fornecedor.
     *
     * @param cdProdutoServicoFornecedor the cd produto servico fornecedor
     */
    public void setCdProdutoServicoFornecedor(Integer cdProdutoServicoFornecedor) {
        this.cdProdutoServicoFornecedor = cdProdutoServicoFornecedor;
    }

    /**
     * Gets the cd produto servico fornecedor.
     *
     * @return the cd produto servico fornecedor
     */
    public Integer getCdProdutoServicoFornecedor() {
        return this.cdProdutoServicoFornecedor;
    }

    /**
     * Sets the ds produto servico fornecedor.
     *
     * @param dsProdutoServicoFornecedor the ds produto servico fornecedor
     */
    public void setDsProdutoServicoFornecedor(String dsProdutoServicoFornecedor) {
        this.dsProdutoServicoFornecedor = dsProdutoServicoFornecedor;
    }

    /**
     * Gets the ds produto servico fornecedor.
     *
     * @return the ds produto servico fornecedor
     */
    public String getDsProdutoServicoFornecedor() {
        return this.dsProdutoServicoFornecedor;
    }

    /**
     * Sets the cd indicador servico tributos.
     *
     * @param cdIndicadorServicoTributos the cd indicador servico tributos
     */
    public void setCdIndicadorServicoTributos(String cdIndicadorServicoTributos) {
        this.cdIndicadorServicoTributos = cdIndicadorServicoTributos;
    }

    /**
     * Gets the cd indicador servico tributos.
     *
     * @return the cd indicador servico tributos
     */
    public String getCdIndicadorServicoTributos() {
        return this.cdIndicadorServicoTributos;
    }

    /**
     * Sets the cd produto servico tributos.
     *
     * @param cdProdutoServicoTributos the cd produto servico tributos
     */
    public void setCdProdutoServicoTributos(Integer cdProdutoServicoTributos) {
        this.cdProdutoServicoTributos = cdProdutoServicoTributos;
    }

    /**
     * Gets the cd produto servico tributos.
     *
     * @return the cd produto servico tributos
     */
    public Integer getCdProdutoServicoTributos() {
        return this.cdProdutoServicoTributos;
    }

    /**
     * Sets the ds produto servico tributos.
     *
     * @param dsProdutoServicoTributos the ds produto servico tributos
     */
    public void setDsProdutoServicoTributos(String dsProdutoServicoTributos) {
        this.dsProdutoServicoTributos = dsProdutoServicoTributos;
    }

    /**
     * Gets the ds produto servico tributos.
     *
     * @return the ds produto servico tributos
     */
    public String getDsProdutoServicoTributos() {
        return this.dsProdutoServicoTributos;
    }

    /**
     * Sets the cd indicador servico salario.
     *
     * @param cdIndicadorServicoSalario the cd indicador servico salario
     */
    public void setCdIndicadorServicoSalario(String cdIndicadorServicoSalario) {
        this.cdIndicadorServicoSalario = cdIndicadorServicoSalario;
    }

    /**
     * Gets the cd indicador servico salario.
     *
     * @return the cd indicador servico salario
     */
    public String getCdIndicadorServicoSalario() {
        return this.cdIndicadorServicoSalario;
    }

    /**
     * Sets the cd produto servico salario.
     *
     * @param cdProdutoServicoSalario the cd produto servico salario
     */
    public void setCdProdutoServicoSalario(Integer cdProdutoServicoSalario) {
        this.cdProdutoServicoSalario = cdProdutoServicoSalario;
    }

    /**
     * Gets the cd produto servico salario.
     *
     * @return the cd produto servico salario
     */
    public Integer getCdProdutoServicoSalario() {
        return this.cdProdutoServicoSalario;
    }

    /**
     * Sets the ds produto servico salario.
     *
     * @param dsProdutoServicoSalario the ds produto servico salario
     */
    public void setDsProdutoServicoSalario(String dsProdutoServicoSalario) {
        this.dsProdutoServicoSalario = dsProdutoServicoSalario;
    }

    /**
     * Gets the ds produto servico salario.
     *
     * @return the ds produto servico salario
     */
    public String getDsProdutoServicoSalario() {
        return this.dsProdutoServicoSalario;
    }

    /**
     * Sets the cd floating servico aplicacao salario.
     *
     * @param cdFloatingServicoAplicacaoSalario the cd floating servico aplicacao salario
     */
    public void setCdFloatingServicoAplicacaoSalario(Integer cdFloatingServicoAplicacaoSalario) {
        this.cdFloatingServicoAplicacaoSalario = cdFloatingServicoAplicacaoSalario;
    }

    /**
     * Gets the cd floating servico aplicacao salario.
     *
     * @return the cd floating servico aplicacao salario
     */
    public Integer getCdFloatingServicoAplicacaoSalario() {
        return this.cdFloatingServicoAplicacaoSalario;
    }

    /**
     * Sets the ds floating servico aplicacao salario.
     *
     * @param dsFloatingServicoAplicacaoSalario the ds floating servico aplicacao salario
     */
    public void setDsFloatingServicoAplicacaoSalario(String dsFloatingServicoAplicacaoSalario) {
        this.dsFloatingServicoAplicacaoSalario = dsFloatingServicoAplicacaoSalario;
    }

    /**
     * Gets the ds floating servico aplicacao salario.
     *
     * @return the ds floating servico aplicacao salario
     */
    public String getDsFloatingServicoAplicacaoSalario() {
        return this.dsFloatingServicoAplicacaoSalario;
    }

	/**
	 * Sets the cd indicador comprovante salarial.
	 *
	 * @param cdIndicadorComprovanteSalarial the cdIndicadorComprovanteSalarial to set
	 */
	public void setCdIndicadorComprovanteSalarial(
			String cdIndicadorComprovanteSalarial) {
		this.cdIndicadorComprovanteSalarial = cdIndicadorComprovanteSalarial;
	}

	/**
	 * Gets the cd indicador comprovante salarial.
	 *
	 * @return the cdIndicadorComprovanteSalarial
	 */
	public String getCdIndicadorComprovanteSalarial() {
		return cdIndicadorComprovanteSalarial;
	}

	/**
	 * Gets the ds municipio agencia gestora.
	 *
	 * @return the ds municipio agencia gestora
	 */
	public String getDsMunicipioAgenciaGestora() {
		return dsMunicipioAgenciaGestora;
	}

	/**
	 * Sets the ds municipio agencia gestora.
	 *
	 * @param dsMunicipioAgenciaGestora the new ds municipio agencia gestora
	 */
	public void setDsMunicipioAgenciaGestora(String dsMunicipioAgenciaGestora) {
		this.dsMunicipioAgenciaGestora = dsMunicipioAgenciaGestora;
	}

	/**
	 * Gets the ds sigla uf participante.
	 *
	 * @return the ds sigla uf participante
	 */
	public String getDsSiglaUfParticipante() {
		return dsSiglaUfParticipante;
	}

	/**
	 * Sets the ds sigla uf participante.
	 *
	 * @param dsSiglaUfParticipante the new ds sigla uf participante
	 */
	public void setDsSiglaUfParticipante(String dsSiglaUfParticipante) {
		this.dsSiglaUfParticipante = dsSiglaUfParticipante;
	}

	/**
	 * Gets the cd float servico aplicacao fornecedor.
	 *
	 * @return the cd float servico aplicacao fornecedor
	 */
	public Integer getCdFloatServicoAplicacaoFornecedor() {
		return cdFloatServicoAplicacaoFornecedor;
	}

	/**
	 * Sets the cd float servico aplicacao fornecedor.
	 *
	 * @param cdFloatServicoAplicacaoFornecedor the new cd float servico aplicacao fornecedor
	 */
	public void setCdFloatServicoAplicacaoFornecedor(
			Integer cdFloatServicoAplicacaoFornecedor) {
		this.cdFloatServicoAplicacaoFornecedor = cdFloatServicoAplicacaoFornecedor;
	}

	/**
	 * Gets the ds float servico aplicacao fornecedor.
	 *
	 * @return the ds float servico aplicacao fornecedor
	 */
	public String getDsFloatServicoAplicacaoFornecedor() {
		return dsFloatServicoAplicacaoFornecedor;
	}

	/**
	 * Sets the ds float servico aplicacao fornecedor.
	 *
	 * @param dsFloatServicoAplicacaoFornecedor the new ds float servico aplicacao fornecedor
	 */
	public void setDsFloatServicoAplicacaoFornecedor(
			String dsFloatServicoAplicacaoFornecedor) {
		this.dsFloatServicoAplicacaoFornecedor = dsFloatServicoAplicacaoFornecedor;
	}

    /**
     * Nome: getOcorrenciasFornecedor.
     *
     * @return ocorrenciasFornecedor
     */
    public List<ImprimirContratoOcorrenciasSaidaDTO> getOcorrenciasFornecedor() {
        return ocorrenciasFornecedor;
    }

    /**
     * Nome: setOcorrenciasFornecedor.
     *
     * @param ocorrenciasFornecedor the ocorrencias fornecedor
     */
    public void setOcorrenciasFornecedor(List<ImprimirContratoOcorrenciasSaidaDTO> ocorrenciasFornecedor) {
        this.ocorrenciasFornecedor = ocorrenciasFornecedor;
    }

    /**
     * Nome: getOcorrenciasTributo.
     *
     * @return ocorrenciasTributo
     */
    public List<ImprimirContratoOcorrencias1SaidaDTO> getOcorrenciasTributo() {
        return ocorrenciasTributo;
    }

    /**
     * Nome: setOcorrenciasTributo.
     *
     * @param ocorrenciasTributo the ocorrencias tributo
     */
    public void setOcorrenciasTributo(List<ImprimirContratoOcorrencias1SaidaDTO> ocorrenciasTributo) {
        this.ocorrenciasTributo = ocorrenciasTributo;
    }

    /**
     * Nome: getOcorrenciasSalario.
     *
     * @return ocorrenciasSalario
     */
    public List<ImprimirContratoOcorrencias2SaidaDTO> getOcorrenciasSalario() {
        return ocorrenciasSalario;
    }

    /**
     * Nome: setOcorrenciasSalario.
     *
     * @param ocorrenciasSalario the ocorrencias salario
     */
    public void setOcorrenciasSalario(List<ImprimirContratoOcorrencias2SaidaDTO> ocorrenciasSalario) {
        this.ocorrenciasSalario = ocorrenciasSalario;
    }

    /**
     * Nome: getOcorrenciasFuncionario.
     *
     * @return ocorrenciasFuncionario
     */
    public List<ImprimirContratoOcorrencias3SaidaDTO> getOcorrenciasFuncionario() {
        return ocorrenciasFuncionario;
    }

    /**
     * Nome: setOcorrenciasFuncionario.
     *
     * @param ocorrenciasFuncionario the ocorrencias funcionario
     */
    public void setOcorrenciasFuncionario(List<ImprimirContratoOcorrencias3SaidaDTO> ocorrenciasFuncionario) {
        this.ocorrenciasFuncionario = ocorrenciasFuncionario;
    }

    /**
     * Nome: getOcorrenciasGuicheCaixa.
     *
     * @return ocorrenciasGuicheCaixa
     */
    public List<ImprimirContratoOcorrencias4SaidaDTO> getOcorrenciasGuicheCaixa() {
        return ocorrenciasGuicheCaixa;
    }

    /**
     * Nome: setOcorrenciasGuicheCaixa.
     *
     * @param ocorrenciasGuicheCaixa the ocorrencias guiche caixa
     */
    public void setOcorrenciasGuicheCaixa(List<ImprimirContratoOcorrencias4SaidaDTO> ocorrenciasGuicheCaixa) {
        this.ocorrenciasGuicheCaixa = ocorrenciasGuicheCaixa;
    }

    /**
     * Nome: getOcorrenciasAutoAtendimento.
     *
     * @return ocorrenciasAutoAtendimento
     */
    public List<ImprimirContratoOcorrencias5SaidaDTO> getOcorrenciasAutoAtendimento() {
        return ocorrenciasAutoAtendimento;
    }

    /**
     * Nome: setOcorrenciasAutoAtendimento.
     *
     * @param ocorrenciasAutoAtendimento the ocorrencias auto atendimento
     */
    public void setOcorrenciasAutoAtendimento(List<ImprimirContratoOcorrencias5SaidaDTO> ocorrenciasAutoAtendimento) {
        this.ocorrenciasAutoAtendimento = ocorrenciasAutoAtendimento;
    }

    /**
     * Nome: getOcorrenciasFoneFacil.
     *
     * @return ocorrenciasFoneFacil
     */
    public List<ImprimirContratoOcorrencias6SaidaDTO> getOcorrenciasFoneFacil() {
        return ocorrenciasFoneFacil;
    }

    /**
     * Nome: setOcorrenciasFoneFacil.
     *
     * @param ocorrenciasFoneFacil the ocorrencias fone facil
     */
    public void setOcorrenciasFoneFacil(List<ImprimirContratoOcorrencias6SaidaDTO> ocorrenciasFoneFacil) {
        this.ocorrenciasFoneFacil = ocorrenciasFoneFacil;
    }

    /**
     * Nome: getOcorrenciasInternet.
     *
     * @return ocorrenciasInternet
     */
    public List<ImprimirContratoOcorrencias7SaidaDTO> getOcorrenciasInternet() {
        return ocorrenciasInternet;
    }

    /**
     * Nome: setOcorrenciasInternet.
     *
     * @param ocorrenciasInternet the ocorrencias internet
     */
    public void setOcorrenciasInternet(List<ImprimirContratoOcorrencias7SaidaDTO> ocorrenciasInternet) {
        this.ocorrenciasInternet = ocorrenciasInternet;
    }
}