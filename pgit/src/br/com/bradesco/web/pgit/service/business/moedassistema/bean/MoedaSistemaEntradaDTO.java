/*
 * Nome: br.com.bradesco.web.pgit.service.business.moedassistema.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.moedassistema.bean;

/**
 * Nome: MoedaSistemaEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class MoedaSistemaEntradaDTO {

	/** Atributo cdIndicadorEconomicoMoeda. */
	private int cdIndicadorEconomicoMoeda;
	
	/** Atributo cdTipoLayoutArquivo. */
	private int cdTipoLayoutArquivo;
	
	/** Atributo cdIndicadorEconomicoLayout. */
	private String cdIndicadorEconomicoLayout;
	
	/**
	 * Get: cdIndicadorEconomicoMoeda.
	 *
	 * @return cdIndicadorEconomicoMoeda
	 */
	public int getCdIndicadorEconomicoMoeda() {
		return cdIndicadorEconomicoMoeda;
	}
	
	/**
	 * Set: cdIndicadorEconomicoMoeda.
	 *
	 * @param cdIndicadorEconomicoMoeda the cd indicador economico moeda
	 */
	public void setCdIndicadorEconomicoMoeda(int cdIndicadorEconomicoMoeda) {
		this.cdIndicadorEconomicoMoeda = cdIndicadorEconomicoMoeda;
	}
	
	/**
	 * Get: cdTipoLayoutArquivo.
	 *
	 * @return cdTipoLayoutArquivo
	 */
	public int getCdTipoLayoutArquivo() {
		return cdTipoLayoutArquivo;
	}
	
	/**
	 * Set: cdTipoLayoutArquivo.
	 *
	 * @param cdTipoLayoutArquivo the cd tipo layout arquivo
	 */
	public void setCdTipoLayoutArquivo(int cdTipoLayoutArquivo) {
		this.cdTipoLayoutArquivo = cdTipoLayoutArquivo;
	}
	
	/**
	 * Get: cdIndicadorEconomicoLayout.
	 *
	 * @return cdIndicadorEconomicoLayout
	 */
	public String getCdIndicadorEconomicoLayout() {
		return cdIndicadorEconomicoLayout;
	}
	
	/**
	 * Set: cdIndicadorEconomicoLayout.
	 *
	 * @param cdIndicadorEconomicoLayout the cd indicador economico layout
	 */
	public void setCdIndicadorEconomicoLayout(String cdIndicadorEconomicoLayout) {
		this.cdIndicadorEconomicoLayout = cdIndicadorEconomicoLayout;
	}
	
	
}
