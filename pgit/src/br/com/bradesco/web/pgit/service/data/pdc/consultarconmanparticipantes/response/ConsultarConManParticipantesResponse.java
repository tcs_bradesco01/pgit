/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultarconmanparticipantes.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ConsultarConManParticipantesResponse.
 * 
 * @version $Revision$ $Date$
 */
public class ConsultarConManParticipantesResponse implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _codMensagem
     */
    private java.lang.String _codMensagem;

    /**
     * Field _mensagem
     */
    private java.lang.String _mensagem;

    /**
     * Field _cdTipoParticipacao
     */
    private int _cdTipoParticipacao = 0;

    /**
     * keeps track of state for field: _cdTipoParticipacao
     */
    private boolean _has_cdTipoParticipacao;

    /**
     * Field _dsTipoParticipacao
     */
    private java.lang.String _dsTipoParticipacao;

    /**
     * Field _cdCpfCnpj
     */
    private long _cdCpfCnpj = 0;

    /**
     * keeps track of state for field: _cdCpfCnpj
     */
    private boolean _has_cdCpfCnpj;

    /**
     * Field _cdFilialCpfCnpj
     */
    private int _cdFilialCpfCnpj = 0;

    /**
     * keeps track of state for field: _cdFilialCpfCnpj
     */
    private boolean _has_cdFilialCpfCnpj;

    /**
     * Field _cdControleCpfCnpj
     */
    private int _cdControleCpfCnpj = 0;

    /**
     * keeps track of state for field: _cdControleCpfCnpj
     */
    private boolean _has_cdControleCpfCnpj;

    /**
     * Field _nome
     */
    private java.lang.String _nome;

    /**
     * Field _cdGrupoEconomico
     */
    private int _cdGrupoEconomico = 0;

    /**
     * keeps track of state for field: _cdGrupoEconomico
     */
    private boolean _has_cdGrupoEconomico;

    /**
     * Field _dsGrupoEconomico
     */
    private java.lang.String _dsGrupoEconomico;

    /**
     * Field _cdAtividadeEconomica
     */
    private int _cdAtividadeEconomica = 0;

    /**
     * keeps track of state for field: _cdAtividadeEconomica
     */
    private boolean _has_cdAtividadeEconomica;

    /**
     * Field _dsAtividadeEconomica
     */
    private java.lang.String _dsAtividadeEconomica;

    /**
     * Field _cdSegCliente
     */
    private int _cdSegCliente = 0;

    /**
     * keeps track of state for field: _cdSegCliente
     */
    private boolean _has_cdSegCliente;

    /**
     * Field _dsSegCliente
     */
    private java.lang.String _dsSegCliente;

    /**
     * Field _cdSsgtoCliente
     */
    private int _cdSsgtoCliente = 0;

    /**
     * keeps track of state for field: _cdSsgtoCliente
     */
    private boolean _has_cdSsgtoCliente;

    /**
     * Field _dsSsgtoCliente
     */
    private java.lang.String _dsSsgtoCliente;

    /**
     * Field _cdIndicadorTipoManutencao
     */
    private int _cdIndicadorTipoManutencao = 0;

    /**
     * keeps track of state for field: _cdIndicadorTipoManutencao
     */
    private boolean _has_cdIndicadorTipoManutencao;

    /**
     * Field _dsIndicadorTipoManutencao
     */
    private java.lang.String _dsIndicadorTipoManutencao;

    /**
     * Field _hrInclusaoRegistro
     */
    private java.lang.String _hrInclusaoRegistro;

    /**
     * Field _cdUsuarioInclusao
     */
    private java.lang.String _cdUsuarioInclusao;

    /**
     * Field _cdUsuarioInclusaoExterno
     */
    private java.lang.String _cdUsuarioInclusaoExterno;

    /**
     * Field _cdCanalInclusao
     */
    private int _cdCanalInclusao = 0;

    /**
     * keeps track of state for field: _cdCanalInclusao
     */
    private boolean _has_cdCanalInclusao;

    /**
     * Field _nmOperacaoFluxoInclusao
     */
    private java.lang.String _nmOperacaoFluxoInclusao;

    /**
     * Field _hrManutencaoRegistro
     */
    private java.lang.String _hrManutencaoRegistro;

    /**
     * Field _cdUsuarioManutencao
     */
    private java.lang.String _cdUsuarioManutencao;

    /**
     * Field _cdUsuarioManutencaoExterno
     */
    private java.lang.String _cdUsuarioManutencaoExterno;

    /**
     * Field _cdCanalManutencao
     */
    private int _cdCanalManutencao = 0;

    /**
     * keeps track of state for field: _cdCanalManutencao
     */
    private boolean _has_cdCanalManutencao;

    /**
     * Field _nrOperacaoFluxoManutencao
     */
    private java.lang.String _nrOperacaoFluxoManutencao;

    /**
     * Field _dsCanalInclusao
     */
    private java.lang.String _dsCanalInclusao;

    /**
     * Field _dsCanalManutencao
     */
    private java.lang.String _dsCanalManutencao;

    /**
     * Field _cdSituacaoParticipanteContrato
     */
    private int _cdSituacaoParticipanteContrato = 0;

    /**
     * keeps track of state for field:
     * _cdSituacaoParticipanteContrato
     */
    private boolean _has_cdSituacaoParticipanteContrato;

    /**
     * Field _dsSituacaoParticipanteContrato
     */
    private java.lang.String _dsSituacaoParticipanteContrato;

    /**
     * Field _cdMotivoSituacaoParticipante
     */
    private int _cdMotivoSituacaoParticipante = 0;

    /**
     * keeps track of state for field: _cdMotivoSituacaoParticipante
     */
    private boolean _has_cdMotivoSituacaoParticipante;

    /**
     * Field _dsMotivoSituacaoParticipante
     */
    private java.lang.String _dsMotivoSituacaoParticipante;

    /**
     * Field _cdClassificacaoAreaParticipante
     */
    private int _cdClassificacaoAreaParticipante = 0;

    /**
     * keeps track of state for field:
     * _cdClassificacaoAreaParticipante
     */
    private boolean _has_cdClassificacaoAreaParticipante;

    /**
     * Field _dsClassificacaoAreaParticipante
     */
    private java.lang.String _dsClassificacaoAreaParticipante;


      //----------------/
     //- Constructors -/
    //----------------/

    public ConsultarConManParticipantesResponse() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarconmanparticipantes.response.ConsultarConManParticipantesResponse()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdAtividadeEconomica
     * 
     */
    public void deleteCdAtividadeEconomica()
    {
        this._has_cdAtividadeEconomica= false;
    } //-- void deleteCdAtividadeEconomica() 

    /**
     * Method deleteCdCanalInclusao
     * 
     */
    public void deleteCdCanalInclusao()
    {
        this._has_cdCanalInclusao= false;
    } //-- void deleteCdCanalInclusao() 

    /**
     * Method deleteCdCanalManutencao
     * 
     */
    public void deleteCdCanalManutencao()
    {
        this._has_cdCanalManutencao= false;
    } //-- void deleteCdCanalManutencao() 

    /**
     * Method deleteCdClassificacaoAreaParticipante
     * 
     */
    public void deleteCdClassificacaoAreaParticipante()
    {
        this._has_cdClassificacaoAreaParticipante= false;
    } //-- void deleteCdClassificacaoAreaParticipante() 

    /**
     * Method deleteCdControleCpfCnpj
     * 
     */
    public void deleteCdControleCpfCnpj()
    {
        this._has_cdControleCpfCnpj= false;
    } //-- void deleteCdControleCpfCnpj() 

    /**
     * Method deleteCdCpfCnpj
     * 
     */
    public void deleteCdCpfCnpj()
    {
        this._has_cdCpfCnpj= false;
    } //-- void deleteCdCpfCnpj() 

    /**
     * Method deleteCdFilialCpfCnpj
     * 
     */
    public void deleteCdFilialCpfCnpj()
    {
        this._has_cdFilialCpfCnpj= false;
    } //-- void deleteCdFilialCpfCnpj() 

    /**
     * Method deleteCdGrupoEconomico
     * 
     */
    public void deleteCdGrupoEconomico()
    {
        this._has_cdGrupoEconomico= false;
    } //-- void deleteCdGrupoEconomico() 

    /**
     * Method deleteCdIndicadorTipoManutencao
     * 
     */
    public void deleteCdIndicadorTipoManutencao()
    {
        this._has_cdIndicadorTipoManutencao= false;
    } //-- void deleteCdIndicadorTipoManutencao() 

    /**
     * Method deleteCdMotivoSituacaoParticipante
     * 
     */
    public void deleteCdMotivoSituacaoParticipante()
    {
        this._has_cdMotivoSituacaoParticipante= false;
    } //-- void deleteCdMotivoSituacaoParticipante() 

    /**
     * Method deleteCdSegCliente
     * 
     */
    public void deleteCdSegCliente()
    {
        this._has_cdSegCliente= false;
    } //-- void deleteCdSegCliente() 

    /**
     * Method deleteCdSituacaoParticipanteContrato
     * 
     */
    public void deleteCdSituacaoParticipanteContrato()
    {
        this._has_cdSituacaoParticipanteContrato= false;
    } //-- void deleteCdSituacaoParticipanteContrato() 

    /**
     * Method deleteCdSsgtoCliente
     * 
     */
    public void deleteCdSsgtoCliente()
    {
        this._has_cdSsgtoCliente= false;
    } //-- void deleteCdSsgtoCliente() 

    /**
     * Method deleteCdTipoParticipacao
     * 
     */
    public void deleteCdTipoParticipacao()
    {
        this._has_cdTipoParticipacao= false;
    } //-- void deleteCdTipoParticipacao() 

    /**
     * Returns the value of field 'cdAtividadeEconomica'.
     * 
     * @return int
     * @return the value of field 'cdAtividadeEconomica'.
     */
    public int getCdAtividadeEconomica()
    {
        return this._cdAtividadeEconomica;
    } //-- int getCdAtividadeEconomica() 

    /**
     * Returns the value of field 'cdCanalInclusao'.
     * 
     * @return int
     * @return the value of field 'cdCanalInclusao'.
     */
    public int getCdCanalInclusao()
    {
        return this._cdCanalInclusao;
    } //-- int getCdCanalInclusao() 

    /**
     * Returns the value of field 'cdCanalManutencao'.
     * 
     * @return int
     * @return the value of field 'cdCanalManutencao'.
     */
    public int getCdCanalManutencao()
    {
        return this._cdCanalManutencao;
    } //-- int getCdCanalManutencao() 

    /**
     * Returns the value of field
     * 'cdClassificacaoAreaParticipante'.
     * 
     * @return int
     * @return the value of field 'cdClassificacaoAreaParticipante'.
     */
    public int getCdClassificacaoAreaParticipante()
    {
        return this._cdClassificacaoAreaParticipante;
    } //-- int getCdClassificacaoAreaParticipante() 

    /**
     * Returns the value of field 'cdControleCpfCnpj'.
     * 
     * @return int
     * @return the value of field 'cdControleCpfCnpj'.
     */
    public int getCdControleCpfCnpj()
    {
        return this._cdControleCpfCnpj;
    } //-- int getCdControleCpfCnpj() 

    /**
     * Returns the value of field 'cdCpfCnpj'.
     * 
     * @return long
     * @return the value of field 'cdCpfCnpj'.
     */
    public long getCdCpfCnpj()
    {
        return this._cdCpfCnpj;
    } //-- long getCdCpfCnpj() 

    /**
     * Returns the value of field 'cdFilialCpfCnpj'.
     * 
     * @return int
     * @return the value of field 'cdFilialCpfCnpj'.
     */
    public int getCdFilialCpfCnpj()
    {
        return this._cdFilialCpfCnpj;
    } //-- int getCdFilialCpfCnpj() 

    /**
     * Returns the value of field 'cdGrupoEconomico'.
     * 
     * @return int
     * @return the value of field 'cdGrupoEconomico'.
     */
    public int getCdGrupoEconomico()
    {
        return this._cdGrupoEconomico;
    } //-- int getCdGrupoEconomico() 

    /**
     * Returns the value of field 'cdIndicadorTipoManutencao'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorTipoManutencao'.
     */
    public int getCdIndicadorTipoManutencao()
    {
        return this._cdIndicadorTipoManutencao;
    } //-- int getCdIndicadorTipoManutencao() 

    /**
     * Returns the value of field 'cdMotivoSituacaoParticipante'.
     * 
     * @return int
     * @return the value of field 'cdMotivoSituacaoParticipante'.
     */
    public int getCdMotivoSituacaoParticipante()
    {
        return this._cdMotivoSituacaoParticipante;
    } //-- int getCdMotivoSituacaoParticipante() 

    /**
     * Returns the value of field 'cdSegCliente'.
     * 
     * @return int
     * @return the value of field 'cdSegCliente'.
     */
    public int getCdSegCliente()
    {
        return this._cdSegCliente;
    } //-- int getCdSegCliente() 

    /**
     * Returns the value of field 'cdSituacaoParticipanteContrato'.
     * 
     * @return int
     * @return the value of field 'cdSituacaoParticipanteContrato'.
     */
    public int getCdSituacaoParticipanteContrato()
    {
        return this._cdSituacaoParticipanteContrato;
    } //-- int getCdSituacaoParticipanteContrato() 

    /**
     * Returns the value of field 'cdSsgtoCliente'.
     * 
     * @return int
     * @return the value of field 'cdSsgtoCliente'.
     */
    public int getCdSsgtoCliente()
    {
        return this._cdSsgtoCliente;
    } //-- int getCdSsgtoCliente() 

    /**
     * Returns the value of field 'cdTipoParticipacao'.
     * 
     * @return int
     * @return the value of field 'cdTipoParticipacao'.
     */
    public int getCdTipoParticipacao()
    {
        return this._cdTipoParticipacao;
    } //-- int getCdTipoParticipacao() 

    /**
     * Returns the value of field 'cdUsuarioInclusao'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioInclusao'.
     */
    public java.lang.String getCdUsuarioInclusao()
    {
        return this._cdUsuarioInclusao;
    } //-- java.lang.String getCdUsuarioInclusao() 

    /**
     * Returns the value of field 'cdUsuarioInclusaoExterno'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioInclusaoExterno'.
     */
    public java.lang.String getCdUsuarioInclusaoExterno()
    {
        return this._cdUsuarioInclusaoExterno;
    } //-- java.lang.String getCdUsuarioInclusaoExterno() 

    /**
     * Returns the value of field 'cdUsuarioManutencao'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioManutencao'.
     */
    public java.lang.String getCdUsuarioManutencao()
    {
        return this._cdUsuarioManutencao;
    } //-- java.lang.String getCdUsuarioManutencao() 

    /**
     * Returns the value of field 'cdUsuarioManutencaoExterno'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioManutencaoExterno'.
     */
    public java.lang.String getCdUsuarioManutencaoExterno()
    {
        return this._cdUsuarioManutencaoExterno;
    } //-- java.lang.String getCdUsuarioManutencaoExterno() 

    /**
     * Returns the value of field 'codMensagem'.
     * 
     * @return String
     * @return the value of field 'codMensagem'.
     */
    public java.lang.String getCodMensagem()
    {
        return this._codMensagem;
    } //-- java.lang.String getCodMensagem() 

    /**
     * Returns the value of field 'dsAtividadeEconomica'.
     * 
     * @return String
     * @return the value of field 'dsAtividadeEconomica'.
     */
    public java.lang.String getDsAtividadeEconomica()
    {
        return this._dsAtividadeEconomica;
    } //-- java.lang.String getDsAtividadeEconomica() 

    /**
     * Returns the value of field 'dsCanalInclusao'.
     * 
     * @return String
     * @return the value of field 'dsCanalInclusao'.
     */
    public java.lang.String getDsCanalInclusao()
    {
        return this._dsCanalInclusao;
    } //-- java.lang.String getDsCanalInclusao() 

    /**
     * Returns the value of field 'dsCanalManutencao'.
     * 
     * @return String
     * @return the value of field 'dsCanalManutencao'.
     */
    public java.lang.String getDsCanalManutencao()
    {
        return this._dsCanalManutencao;
    } //-- java.lang.String getDsCanalManutencao() 

    /**
     * Returns the value of field
     * 'dsClassificacaoAreaParticipante'.
     * 
     * @return String
     * @return the value of field 'dsClassificacaoAreaParticipante'.
     */
    public java.lang.String getDsClassificacaoAreaParticipante()
    {
        return this._dsClassificacaoAreaParticipante;
    } //-- java.lang.String getDsClassificacaoAreaParticipante() 

    /**
     * Returns the value of field 'dsGrupoEconomico'.
     * 
     * @return String
     * @return the value of field 'dsGrupoEconomico'.
     */
    public java.lang.String getDsGrupoEconomico()
    {
        return this._dsGrupoEconomico;
    } //-- java.lang.String getDsGrupoEconomico() 

    /**
     * Returns the value of field 'dsIndicadorTipoManutencao'.
     * 
     * @return String
     * @return the value of field 'dsIndicadorTipoManutencao'.
     */
    public java.lang.String getDsIndicadorTipoManutencao()
    {
        return this._dsIndicadorTipoManutencao;
    } //-- java.lang.String getDsIndicadorTipoManutencao() 

    /**
     * Returns the value of field 'dsMotivoSituacaoParticipante'.
     * 
     * @return String
     * @return the value of field 'dsMotivoSituacaoParticipante'.
     */
    public java.lang.String getDsMotivoSituacaoParticipante()
    {
        return this._dsMotivoSituacaoParticipante;
    } //-- java.lang.String getDsMotivoSituacaoParticipante() 

    /**
     * Returns the value of field 'dsSegCliente'.
     * 
     * @return String
     * @return the value of field 'dsSegCliente'.
     */
    public java.lang.String getDsSegCliente()
    {
        return this._dsSegCliente;
    } //-- java.lang.String getDsSegCliente() 

    /**
     * Returns the value of field 'dsSituacaoParticipanteContrato'.
     * 
     * @return String
     * @return the value of field 'dsSituacaoParticipanteContrato'.
     */
    public java.lang.String getDsSituacaoParticipanteContrato()
    {
        return this._dsSituacaoParticipanteContrato;
    } //-- java.lang.String getDsSituacaoParticipanteContrato() 

    /**
     * Returns the value of field 'dsSsgtoCliente'.
     * 
     * @return String
     * @return the value of field 'dsSsgtoCliente'.
     */
    public java.lang.String getDsSsgtoCliente()
    {
        return this._dsSsgtoCliente;
    } //-- java.lang.String getDsSsgtoCliente() 

    /**
     * Returns the value of field 'dsTipoParticipacao'.
     * 
     * @return String
     * @return the value of field 'dsTipoParticipacao'.
     */
    public java.lang.String getDsTipoParticipacao()
    {
        return this._dsTipoParticipacao;
    } //-- java.lang.String getDsTipoParticipacao() 

    /**
     * Returns the value of field 'hrInclusaoRegistro'.
     * 
     * @return String
     * @return the value of field 'hrInclusaoRegistro'.
     */
    public java.lang.String getHrInclusaoRegistro()
    {
        return this._hrInclusaoRegistro;
    } //-- java.lang.String getHrInclusaoRegistro() 

    /**
     * Returns the value of field 'hrManutencaoRegistro'.
     * 
     * @return String
     * @return the value of field 'hrManutencaoRegistro'.
     */
    public java.lang.String getHrManutencaoRegistro()
    {
        return this._hrManutencaoRegistro;
    } //-- java.lang.String getHrManutencaoRegistro() 

    /**
     * Returns the value of field 'mensagem'.
     * 
     * @return String
     * @return the value of field 'mensagem'.
     */
    public java.lang.String getMensagem()
    {
        return this._mensagem;
    } //-- java.lang.String getMensagem() 

    /**
     * Returns the value of field 'nmOperacaoFluxoInclusao'.
     * 
     * @return String
     * @return the value of field 'nmOperacaoFluxoInclusao'.
     */
    public java.lang.String getNmOperacaoFluxoInclusao()
    {
        return this._nmOperacaoFluxoInclusao;
    } //-- java.lang.String getNmOperacaoFluxoInclusao() 

    /**
     * Returns the value of field 'nome'.
     * 
     * @return String
     * @return the value of field 'nome'.
     */
    public java.lang.String getNome()
    {
        return this._nome;
    } //-- java.lang.String getNome() 

    /**
     * Returns the value of field 'nrOperacaoFluxoManutencao'.
     * 
     * @return String
     * @return the value of field 'nrOperacaoFluxoManutencao'.
     */
    public java.lang.String getNrOperacaoFluxoManutencao()
    {
        return this._nrOperacaoFluxoManutencao;
    } //-- java.lang.String getNrOperacaoFluxoManutencao() 

    /**
     * Method hasCdAtividadeEconomica
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAtividadeEconomica()
    {
        return this._has_cdAtividadeEconomica;
    } //-- boolean hasCdAtividadeEconomica() 

    /**
     * Method hasCdCanalInclusao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCanalInclusao()
    {
        return this._has_cdCanalInclusao;
    } //-- boolean hasCdCanalInclusao() 

    /**
     * Method hasCdCanalManutencao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCanalManutencao()
    {
        return this._has_cdCanalManutencao;
    } //-- boolean hasCdCanalManutencao() 

    /**
     * Method hasCdClassificacaoAreaParticipante
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdClassificacaoAreaParticipante()
    {
        return this._has_cdClassificacaoAreaParticipante;
    } //-- boolean hasCdClassificacaoAreaParticipante() 

    /**
     * Method hasCdControleCpfCnpj
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdControleCpfCnpj()
    {
        return this._has_cdControleCpfCnpj;
    } //-- boolean hasCdControleCpfCnpj() 

    /**
     * Method hasCdCpfCnpj
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCpfCnpj()
    {
        return this._has_cdCpfCnpj;
    } //-- boolean hasCdCpfCnpj() 

    /**
     * Method hasCdFilialCpfCnpj
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFilialCpfCnpj()
    {
        return this._has_cdFilialCpfCnpj;
    } //-- boolean hasCdFilialCpfCnpj() 

    /**
     * Method hasCdGrupoEconomico
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdGrupoEconomico()
    {
        return this._has_cdGrupoEconomico;
    } //-- boolean hasCdGrupoEconomico() 

    /**
     * Method hasCdIndicadorTipoManutencao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorTipoManutencao()
    {
        return this._has_cdIndicadorTipoManutencao;
    } //-- boolean hasCdIndicadorTipoManutencao() 

    /**
     * Method hasCdMotivoSituacaoParticipante
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMotivoSituacaoParticipante()
    {
        return this._has_cdMotivoSituacaoParticipante;
    } //-- boolean hasCdMotivoSituacaoParticipante() 

    /**
     * Method hasCdSegCliente
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSegCliente()
    {
        return this._has_cdSegCliente;
    } //-- boolean hasCdSegCliente() 

    /**
     * Method hasCdSituacaoParticipanteContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSituacaoParticipanteContrato()
    {
        return this._has_cdSituacaoParticipanteContrato;
    } //-- boolean hasCdSituacaoParticipanteContrato() 

    /**
     * Method hasCdSsgtoCliente
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSsgtoCliente()
    {
        return this._has_cdSsgtoCliente;
    } //-- boolean hasCdSsgtoCliente() 

    /**
     * Method hasCdTipoParticipacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoParticipacao()
    {
        return this._has_cdTipoParticipacao;
    } //-- boolean hasCdTipoParticipacao() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdAtividadeEconomica'.
     * 
     * @param cdAtividadeEconomica the value of field
     * 'cdAtividadeEconomica'.
     */
    public void setCdAtividadeEconomica(int cdAtividadeEconomica)
    {
        this._cdAtividadeEconomica = cdAtividadeEconomica;
        this._has_cdAtividadeEconomica = true;
    } //-- void setCdAtividadeEconomica(int) 

    /**
     * Sets the value of field 'cdCanalInclusao'.
     * 
     * @param cdCanalInclusao the value of field 'cdCanalInclusao'.
     */
    public void setCdCanalInclusao(int cdCanalInclusao)
    {
        this._cdCanalInclusao = cdCanalInclusao;
        this._has_cdCanalInclusao = true;
    } //-- void setCdCanalInclusao(int) 

    /**
     * Sets the value of field 'cdCanalManutencao'.
     * 
     * @param cdCanalManutencao the value of field
     * 'cdCanalManutencao'.
     */
    public void setCdCanalManutencao(int cdCanalManutencao)
    {
        this._cdCanalManutencao = cdCanalManutencao;
        this._has_cdCanalManutencao = true;
    } //-- void setCdCanalManutencao(int) 

    /**
     * Sets the value of field 'cdClassificacaoAreaParticipante'.
     * 
     * @param cdClassificacaoAreaParticipante the value of field
     * 'cdClassificacaoAreaParticipante'.
     */
    public void setCdClassificacaoAreaParticipante(int cdClassificacaoAreaParticipante)
    {
        this._cdClassificacaoAreaParticipante = cdClassificacaoAreaParticipante;
        this._has_cdClassificacaoAreaParticipante = true;
    } //-- void setCdClassificacaoAreaParticipante(int) 

    /**
     * Sets the value of field 'cdControleCpfCnpj'.
     * 
     * @param cdControleCpfCnpj the value of field
     * 'cdControleCpfCnpj'.
     */
    public void setCdControleCpfCnpj(int cdControleCpfCnpj)
    {
        this._cdControleCpfCnpj = cdControleCpfCnpj;
        this._has_cdControleCpfCnpj = true;
    } //-- void setCdControleCpfCnpj(int) 

    /**
     * Sets the value of field 'cdCpfCnpj'.
     * 
     * @param cdCpfCnpj the value of field 'cdCpfCnpj'.
     */
    public void setCdCpfCnpj(long cdCpfCnpj)
    {
        this._cdCpfCnpj = cdCpfCnpj;
        this._has_cdCpfCnpj = true;
    } //-- void setCdCpfCnpj(long) 

    /**
     * Sets the value of field 'cdFilialCpfCnpj'.
     * 
     * @param cdFilialCpfCnpj the value of field 'cdFilialCpfCnpj'.
     */
    public void setCdFilialCpfCnpj(int cdFilialCpfCnpj)
    {
        this._cdFilialCpfCnpj = cdFilialCpfCnpj;
        this._has_cdFilialCpfCnpj = true;
    } //-- void setCdFilialCpfCnpj(int) 

    /**
     * Sets the value of field 'cdGrupoEconomico'.
     * 
     * @param cdGrupoEconomico the value of field 'cdGrupoEconomico'
     */
    public void setCdGrupoEconomico(int cdGrupoEconomico)
    {
        this._cdGrupoEconomico = cdGrupoEconomico;
        this._has_cdGrupoEconomico = true;
    } //-- void setCdGrupoEconomico(int) 

    /**
     * Sets the value of field 'cdIndicadorTipoManutencao'.
     * 
     * @param cdIndicadorTipoManutencao the value of field
     * 'cdIndicadorTipoManutencao'.
     */
    public void setCdIndicadorTipoManutencao(int cdIndicadorTipoManutencao)
    {
        this._cdIndicadorTipoManutencao = cdIndicadorTipoManutencao;
        this._has_cdIndicadorTipoManutencao = true;
    } //-- void setCdIndicadorTipoManutencao(int) 

    /**
     * Sets the value of field 'cdMotivoSituacaoParticipante'.
     * 
     * @param cdMotivoSituacaoParticipante the value of field
     * 'cdMotivoSituacaoParticipante'.
     */
    public void setCdMotivoSituacaoParticipante(int cdMotivoSituacaoParticipante)
    {
        this._cdMotivoSituacaoParticipante = cdMotivoSituacaoParticipante;
        this._has_cdMotivoSituacaoParticipante = true;
    } //-- void setCdMotivoSituacaoParticipante(int) 

    /**
     * Sets the value of field 'cdSegCliente'.
     * 
     * @param cdSegCliente the value of field 'cdSegCliente'.
     */
    public void setCdSegCliente(int cdSegCliente)
    {
        this._cdSegCliente = cdSegCliente;
        this._has_cdSegCliente = true;
    } //-- void setCdSegCliente(int) 

    /**
     * Sets the value of field 'cdSituacaoParticipanteContrato'.
     * 
     * @param cdSituacaoParticipanteContrato the value of field
     * 'cdSituacaoParticipanteContrato'.
     */
    public void setCdSituacaoParticipanteContrato(int cdSituacaoParticipanteContrato)
    {
        this._cdSituacaoParticipanteContrato = cdSituacaoParticipanteContrato;
        this._has_cdSituacaoParticipanteContrato = true;
    } //-- void setCdSituacaoParticipanteContrato(int) 

    /**
     * Sets the value of field 'cdSsgtoCliente'.
     * 
     * @param cdSsgtoCliente the value of field 'cdSsgtoCliente'.
     */
    public void setCdSsgtoCliente(int cdSsgtoCliente)
    {
        this._cdSsgtoCliente = cdSsgtoCliente;
        this._has_cdSsgtoCliente = true;
    } //-- void setCdSsgtoCliente(int) 

    /**
     * Sets the value of field 'cdTipoParticipacao'.
     * 
     * @param cdTipoParticipacao the value of field
     * 'cdTipoParticipacao'.
     */
    public void setCdTipoParticipacao(int cdTipoParticipacao)
    {
        this._cdTipoParticipacao = cdTipoParticipacao;
        this._has_cdTipoParticipacao = true;
    } //-- void setCdTipoParticipacao(int) 

    /**
     * Sets the value of field 'cdUsuarioInclusao'.
     * 
     * @param cdUsuarioInclusao the value of field
     * 'cdUsuarioInclusao'.
     */
    public void setCdUsuarioInclusao(java.lang.String cdUsuarioInclusao)
    {
        this._cdUsuarioInclusao = cdUsuarioInclusao;
    } //-- void setCdUsuarioInclusao(java.lang.String) 

    /**
     * Sets the value of field 'cdUsuarioInclusaoExterno'.
     * 
     * @param cdUsuarioInclusaoExterno the value of field
     * 'cdUsuarioInclusaoExterno'.
     */
    public void setCdUsuarioInclusaoExterno(java.lang.String cdUsuarioInclusaoExterno)
    {
        this._cdUsuarioInclusaoExterno = cdUsuarioInclusaoExterno;
    } //-- void setCdUsuarioInclusaoExterno(java.lang.String) 

    /**
     * Sets the value of field 'cdUsuarioManutencao'.
     * 
     * @param cdUsuarioManutencao the value of field
     * 'cdUsuarioManutencao'.
     */
    public void setCdUsuarioManutencao(java.lang.String cdUsuarioManutencao)
    {
        this._cdUsuarioManutencao = cdUsuarioManutencao;
    } //-- void setCdUsuarioManutencao(java.lang.String) 

    /**
     * Sets the value of field 'cdUsuarioManutencaoExterno'.
     * 
     * @param cdUsuarioManutencaoExterno the value of field
     * 'cdUsuarioManutencaoExterno'.
     */
    public void setCdUsuarioManutencaoExterno(java.lang.String cdUsuarioManutencaoExterno)
    {
        this._cdUsuarioManutencaoExterno = cdUsuarioManutencaoExterno;
    } //-- void setCdUsuarioManutencaoExterno(java.lang.String) 

    /**
     * Sets the value of field 'codMensagem'.
     * 
     * @param codMensagem the value of field 'codMensagem'.
     */
    public void setCodMensagem(java.lang.String codMensagem)
    {
        this._codMensagem = codMensagem;
    } //-- void setCodMensagem(java.lang.String) 

    /**
     * Sets the value of field 'dsAtividadeEconomica'.
     * 
     * @param dsAtividadeEconomica the value of field
     * 'dsAtividadeEconomica'.
     */
    public void setDsAtividadeEconomica(java.lang.String dsAtividadeEconomica)
    {
        this._dsAtividadeEconomica = dsAtividadeEconomica;
    } //-- void setDsAtividadeEconomica(java.lang.String) 

    /**
     * Sets the value of field 'dsCanalInclusao'.
     * 
     * @param dsCanalInclusao the value of field 'dsCanalInclusao'.
     */
    public void setDsCanalInclusao(java.lang.String dsCanalInclusao)
    {
        this._dsCanalInclusao = dsCanalInclusao;
    } //-- void setDsCanalInclusao(java.lang.String) 

    /**
     * Sets the value of field 'dsCanalManutencao'.
     * 
     * @param dsCanalManutencao the value of field
     * 'dsCanalManutencao'.
     */
    public void setDsCanalManutencao(java.lang.String dsCanalManutencao)
    {
        this._dsCanalManutencao = dsCanalManutencao;
    } //-- void setDsCanalManutencao(java.lang.String) 

    /**
     * Sets the value of field 'dsClassificacaoAreaParticipante'.
     * 
     * @param dsClassificacaoAreaParticipante the value of field
     * 'dsClassificacaoAreaParticipante'.
     */
    public void setDsClassificacaoAreaParticipante(java.lang.String dsClassificacaoAreaParticipante)
    {
        this._dsClassificacaoAreaParticipante = dsClassificacaoAreaParticipante;
    } //-- void setDsClassificacaoAreaParticipante(java.lang.String) 

    /**
     * Sets the value of field 'dsGrupoEconomico'.
     * 
     * @param dsGrupoEconomico the value of field 'dsGrupoEconomico'
     */
    public void setDsGrupoEconomico(java.lang.String dsGrupoEconomico)
    {
        this._dsGrupoEconomico = dsGrupoEconomico;
    } //-- void setDsGrupoEconomico(java.lang.String) 

    /**
     * Sets the value of field 'dsIndicadorTipoManutencao'.
     * 
     * @param dsIndicadorTipoManutencao the value of field
     * 'dsIndicadorTipoManutencao'.
     */
    public void setDsIndicadorTipoManutencao(java.lang.String dsIndicadorTipoManutencao)
    {
        this._dsIndicadorTipoManutencao = dsIndicadorTipoManutencao;
    } //-- void setDsIndicadorTipoManutencao(java.lang.String) 

    /**
     * Sets the value of field 'dsMotivoSituacaoParticipante'.
     * 
     * @param dsMotivoSituacaoParticipante the value of field
     * 'dsMotivoSituacaoParticipante'.
     */
    public void setDsMotivoSituacaoParticipante(java.lang.String dsMotivoSituacaoParticipante)
    {
        this._dsMotivoSituacaoParticipante = dsMotivoSituacaoParticipante;
    } //-- void setDsMotivoSituacaoParticipante(java.lang.String) 

    /**
     * Sets the value of field 'dsSegCliente'.
     * 
     * @param dsSegCliente the value of field 'dsSegCliente'.
     */
    public void setDsSegCliente(java.lang.String dsSegCliente)
    {
        this._dsSegCliente = dsSegCliente;
    } //-- void setDsSegCliente(java.lang.String) 

    /**
     * Sets the value of field 'dsSituacaoParticipanteContrato'.
     * 
     * @param dsSituacaoParticipanteContrato the value of field
     * 'dsSituacaoParticipanteContrato'.
     */
    public void setDsSituacaoParticipanteContrato(java.lang.String dsSituacaoParticipanteContrato)
    {
        this._dsSituacaoParticipanteContrato = dsSituacaoParticipanteContrato;
    } //-- void setDsSituacaoParticipanteContrato(java.lang.String) 

    /**
     * Sets the value of field 'dsSsgtoCliente'.
     * 
     * @param dsSsgtoCliente the value of field 'dsSsgtoCliente'.
     */
    public void setDsSsgtoCliente(java.lang.String dsSsgtoCliente)
    {
        this._dsSsgtoCliente = dsSsgtoCliente;
    } //-- void setDsSsgtoCliente(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoParticipacao'.
     * 
     * @param dsTipoParticipacao the value of field
     * 'dsTipoParticipacao'.
     */
    public void setDsTipoParticipacao(java.lang.String dsTipoParticipacao)
    {
        this._dsTipoParticipacao = dsTipoParticipacao;
    } //-- void setDsTipoParticipacao(java.lang.String) 

    /**
     * Sets the value of field 'hrInclusaoRegistro'.
     * 
     * @param hrInclusaoRegistro the value of field
     * 'hrInclusaoRegistro'.
     */
    public void setHrInclusaoRegistro(java.lang.String hrInclusaoRegistro)
    {
        this._hrInclusaoRegistro = hrInclusaoRegistro;
    } //-- void setHrInclusaoRegistro(java.lang.String) 

    /**
     * Sets the value of field 'hrManutencaoRegistro'.
     * 
     * @param hrManutencaoRegistro the value of field
     * 'hrManutencaoRegistro'.
     */
    public void setHrManutencaoRegistro(java.lang.String hrManutencaoRegistro)
    {
        this._hrManutencaoRegistro = hrManutencaoRegistro;
    } //-- void setHrManutencaoRegistro(java.lang.String) 

    /**
     * Sets the value of field 'mensagem'.
     * 
     * @param mensagem the value of field 'mensagem'.
     */
    public void setMensagem(java.lang.String mensagem)
    {
        this._mensagem = mensagem;
    } //-- void setMensagem(java.lang.String) 

    /**
     * Sets the value of field 'nmOperacaoFluxoInclusao'.
     * 
     * @param nmOperacaoFluxoInclusao the value of field
     * 'nmOperacaoFluxoInclusao'.
     */
    public void setNmOperacaoFluxoInclusao(java.lang.String nmOperacaoFluxoInclusao)
    {
        this._nmOperacaoFluxoInclusao = nmOperacaoFluxoInclusao;
    } //-- void setNmOperacaoFluxoInclusao(java.lang.String) 

    /**
     * Sets the value of field 'nome'.
     * 
     * @param nome the value of field 'nome'.
     */
    public void setNome(java.lang.String nome)
    {
        this._nome = nome;
    } //-- void setNome(java.lang.String) 

    /**
     * Sets the value of field 'nrOperacaoFluxoManutencao'.
     * 
     * @param nrOperacaoFluxoManutencao the value of field
     * 'nrOperacaoFluxoManutencao'.
     */
    public void setNrOperacaoFluxoManutencao(java.lang.String nrOperacaoFluxoManutencao)
    {
        this._nrOperacaoFluxoManutencao = nrOperacaoFluxoManutencao;
    } //-- void setNrOperacaoFluxoManutencao(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ConsultarConManParticipantesResponse
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultarconmanparticipantes.response.ConsultarConManParticipantesResponse unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultarconmanparticipantes.response.ConsultarConManParticipantesResponse) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultarconmanparticipantes.response.ConsultarConManParticipantesResponse.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarconmanparticipantes.response.ConsultarConManParticipantesResponse unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
