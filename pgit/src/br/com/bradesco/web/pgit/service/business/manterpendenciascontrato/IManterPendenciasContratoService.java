/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/interfaz.ftl,v $
 * $Id: interfaz.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: interfaz.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.manterpendenciascontrato;

import java.util.List;

import br.com.bradesco.web.pgit.service.business.manterpendenciascontrato.bean.ConsultarListaPendenciasContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterpendenciascontrato.bean.ConsultarListaPendenciasContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterpendenciascontrato.bean.ConsultarPendenciaContratoPgitEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterpendenciascontrato.bean.ConsultarPendenciaContratoPgitSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterpendenciascontrato.bean.ReenviarEmailEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterpendenciascontrato.bean.ReenviarEmailSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterpendenciascontrato.bean.RegistrarPendenciaContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterpendenciascontrato.bean.RegistrarPendenciaContratoSaidaDTO;

/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Interface do adaptador: ManterPendenciasContrato
 * </p>
 * 
 * @comment CODIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public interface IManterPendenciasContratoService {
	
	/**
	 * Consultar lista pendencia contrato.
	 *
	 * @param consultarListaPendenciasContratoEntradaDTO the consultar lista pendencias contrato entrada dto
	 * @return the list< consultar lista pendencias contrato saida dt o>
	 */
	List<ConsultarListaPendenciasContratoSaidaDTO> consultarListaPendenciaContrato(ConsultarListaPendenciasContratoEntradaDTO consultarListaPendenciasContratoEntradaDTO);

	/**
	 * Registrar pendencia contrato.
	 *
	 * @param registrarPendenciaContratoEntradaDTO the registrar pendencia contrato entrada dto
	 * @return the registrar pendencia contrato saida dto
	 */
	RegistrarPendenciaContratoSaidaDTO registrarPendenciaContrato(RegistrarPendenciaContratoEntradaDTO registrarPendenciaContratoEntradaDTO);
	
	/**
	 * Consultar pendencia contrato pgit.
	 *
	 * @param consultarPendenciaContratoPgitEntradaDTO the consultar pendencia contrato pgit entrada dto
	 * @return the consultar pendencia contrato pgit saida dto
	 */
	ConsultarPendenciaContratoPgitSaidaDTO consultarPendenciaContratoPgit(ConsultarPendenciaContratoPgitEntradaDTO consultarPendenciaContratoPgitEntradaDTO);
	
	/**
	 * Reenviar email.
	 *
	 * @param entrada the entrada
	 * @return the reenviar email saida dto
	 */
	ReenviarEmailSaidaDTO reenviarEmail(ReenviarEmailEntradaDTO entrada);


}

