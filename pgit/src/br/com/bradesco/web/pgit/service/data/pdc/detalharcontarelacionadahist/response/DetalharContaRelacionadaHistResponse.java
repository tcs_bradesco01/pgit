/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.detalharcontarelacionadahist.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class DetalharContaRelacionadaHistResponse.
 * 
 * @version $Revision$ $Date$
 */
public class DetalharContaRelacionadaHistResponse implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _codMensagem
     */
    private java.lang.String _codMensagem;

    /**
     * Field _mensagem
     */
    private java.lang.String _mensagem;

    /**
     * Field _cdCorpoCpfCnpjParticipante
     */
    private long _cdCorpoCpfCnpjParticipante = 0;

    /**
     * keeps track of state for field: _cdCorpoCpfCnpjParticipante
     */
    private boolean _has_cdCorpoCpfCnpjParticipante;

    /**
     * Field _cdFilialCpfCnpjParticipante
     */
    private int _cdFilialCpfCnpjParticipante = 0;

    /**
     * keeps track of state for field: _cdFilialCpfCnpjParticipante
     */
    private boolean _has_cdFilialCpfCnpjParticipante;

    /**
     * Field _cdDigitoCpfCnpjParticipante
     */
    private int _cdDigitoCpfCnpjParticipante = 0;

    /**
     * keeps track of state for field: _cdDigitoCpfCnpjParticipante
     */
    private boolean _has_cdDigitoCpfCnpjParticipante;

    /**
     * Field _cdBancoRelacionamento
     */
    private int _cdBancoRelacionamento = 0;

    /**
     * keeps track of state for field: _cdBancoRelacionamento
     */
    private boolean _has_cdBancoRelacionamento;

    /**
     * Field _cdAgenciaRelacionamento
     */
    private int _cdAgenciaRelacionamento = 0;

    /**
     * keeps track of state for field: _cdAgenciaRelacionamento
     */
    private boolean _has_cdAgenciaRelacionamento;

    /**
     * Field _cdContaRelacionamento
     */
    private long _cdContaRelacionamento = 0;

    /**
     * keeps track of state for field: _cdContaRelacionamento
     */
    private boolean _has_cdContaRelacionamento;

    /**
     * Field _cdDigitoContaRelacionamento
     */
    private java.lang.String _cdDigitoContaRelacionamento;

    /**
     * Field _cdTipoContaRelacionamento
     */
    private int _cdTipoContaRelacionamento = 0;

    /**
     * keeps track of state for field: _cdTipoContaRelacionamento
     */
    private boolean _has_cdTipoContaRelacionamento;

    /**
     * Field _dsTipoContaRelacionamento
     */
    private java.lang.String _dsTipoContaRelacionamento;

    /**
     * Field _cdIndicadorTipoManutencao
     */
    private int _cdIndicadorTipoManutencao = 0;

    /**
     * keeps track of state for field: _cdIndicadorTipoManutencao
     */
    private boolean _has_cdIndicadorTipoManutencao;

    /**
     * Field _dsTipoManutencao
     */
    private java.lang.String _dsTipoManutencao;

    /**
     * Field _cdUsuarioInclusao
     */
    private java.lang.String _cdUsuarioInclusao;

    /**
     * Field _cdUsuarioInclusaoExterno
     */
    private java.lang.String _cdUsuarioInclusaoExterno;

    /**
     * Field _hrInclusaoRegistro
     */
    private java.lang.String _hrInclusaoRegistro;

    /**
     * Field _cdOperacaoCanalInclusao
     */
    private java.lang.String _cdOperacaoCanalInclusao;

    /**
     * Field _cdTipoCanalInclusao
     */
    private int _cdTipoCanalInclusao = 0;

    /**
     * keeps track of state for field: _cdTipoCanalInclusao
     */
    private boolean _has_cdTipoCanalInclusao;

    /**
     * Field _dsCanalInclusao
     */
    private java.lang.String _dsCanalInclusao;

    /**
     * Field _cdUsuarioManutencao
     */
    private java.lang.String _cdUsuarioManutencao;

    /**
     * Field _cdUsuarioManutencaoExterno
     */
    private java.lang.String _cdUsuarioManutencaoExterno;

    /**
     * Field _hrManutencaoRegistro
     */
    private java.lang.String _hrManutencaoRegistro;

    /**
     * Field _cdOperacaoCanalManutencao
     */
    private java.lang.String _cdOperacaoCanalManutencao;

    /**
     * Field _cdTipoCanalManutencao
     */
    private int _cdTipoCanalManutencao = 0;

    /**
     * keeps track of state for field: _cdTipoCanalManutencao
     */
    private boolean _has_cdTipoCanalManutencao;

    /**
     * Field _dsCanalManutencao
     */
    private java.lang.String _dsCanalManutencao;


      //----------------/
     //- Constructors -/
    //----------------/

    public DetalharContaRelacionadaHistResponse() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharcontarelacionadahist.response.DetalharContaRelacionadaHistResponse()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdAgenciaRelacionamento
     * 
     */
    public void deleteCdAgenciaRelacionamento()
    {
        this._has_cdAgenciaRelacionamento= false;
    } //-- void deleteCdAgenciaRelacionamento() 

    /**
     * Method deleteCdBancoRelacionamento
     * 
     */
    public void deleteCdBancoRelacionamento()
    {
        this._has_cdBancoRelacionamento= false;
    } //-- void deleteCdBancoRelacionamento() 

    /**
     * Method deleteCdContaRelacionamento
     * 
     */
    public void deleteCdContaRelacionamento()
    {
        this._has_cdContaRelacionamento= false;
    } //-- void deleteCdContaRelacionamento() 

    /**
     * Method deleteCdCorpoCpfCnpjParticipante
     * 
     */
    public void deleteCdCorpoCpfCnpjParticipante()
    {
        this._has_cdCorpoCpfCnpjParticipante= false;
    } //-- void deleteCdCorpoCpfCnpjParticipante() 

    /**
     * Method deleteCdDigitoCpfCnpjParticipante
     * 
     */
    public void deleteCdDigitoCpfCnpjParticipante()
    {
        this._has_cdDigitoCpfCnpjParticipante= false;
    } //-- void deleteCdDigitoCpfCnpjParticipante() 

    /**
     * Method deleteCdFilialCpfCnpjParticipante
     * 
     */
    public void deleteCdFilialCpfCnpjParticipante()
    {
        this._has_cdFilialCpfCnpjParticipante= false;
    } //-- void deleteCdFilialCpfCnpjParticipante() 

    /**
     * Method deleteCdIndicadorTipoManutencao
     * 
     */
    public void deleteCdIndicadorTipoManutencao()
    {
        this._has_cdIndicadorTipoManutencao= false;
    } //-- void deleteCdIndicadorTipoManutencao() 

    /**
     * Method deleteCdTipoCanalInclusao
     * 
     */
    public void deleteCdTipoCanalInclusao()
    {
        this._has_cdTipoCanalInclusao= false;
    } //-- void deleteCdTipoCanalInclusao() 

    /**
     * Method deleteCdTipoCanalManutencao
     * 
     */
    public void deleteCdTipoCanalManutencao()
    {
        this._has_cdTipoCanalManutencao= false;
    } //-- void deleteCdTipoCanalManutencao() 

    /**
     * Method deleteCdTipoContaRelacionamento
     * 
     */
    public void deleteCdTipoContaRelacionamento()
    {
        this._has_cdTipoContaRelacionamento= false;
    } //-- void deleteCdTipoContaRelacionamento() 

    /**
     * Returns the value of field 'cdAgenciaRelacionamento'.
     * 
     * @return int
     * @return the value of field 'cdAgenciaRelacionamento'.
     */
    public int getCdAgenciaRelacionamento()
    {
        return this._cdAgenciaRelacionamento;
    } //-- int getCdAgenciaRelacionamento() 

    /**
     * Returns the value of field 'cdBancoRelacionamento'.
     * 
     * @return int
     * @return the value of field 'cdBancoRelacionamento'.
     */
    public int getCdBancoRelacionamento()
    {
        return this._cdBancoRelacionamento;
    } //-- int getCdBancoRelacionamento() 

    /**
     * Returns the value of field 'cdContaRelacionamento'.
     * 
     * @return long
     * @return the value of field 'cdContaRelacionamento'.
     */
    public long getCdContaRelacionamento()
    {
        return this._cdContaRelacionamento;
    } //-- long getCdContaRelacionamento() 

    /**
     * Returns the value of field 'cdCorpoCpfCnpjParticipante'.
     * 
     * @return long
     * @return the value of field 'cdCorpoCpfCnpjParticipante'.
     */
    public long getCdCorpoCpfCnpjParticipante()
    {
        return this._cdCorpoCpfCnpjParticipante;
    } //-- long getCdCorpoCpfCnpjParticipante() 

    /**
     * Returns the value of field 'cdDigitoContaRelacionamento'.
     * 
     * @return String
     * @return the value of field 'cdDigitoContaRelacionamento'.
     */
    public java.lang.String getCdDigitoContaRelacionamento()
    {
        return this._cdDigitoContaRelacionamento;
    } //-- java.lang.String getCdDigitoContaRelacionamento() 

    /**
     * Returns the value of field 'cdDigitoCpfCnpjParticipante'.
     * 
     * @return int
     * @return the value of field 'cdDigitoCpfCnpjParticipante'.
     */
    public int getCdDigitoCpfCnpjParticipante()
    {
        return this._cdDigitoCpfCnpjParticipante;
    } //-- int getCdDigitoCpfCnpjParticipante() 

    /**
     * Returns the value of field 'cdFilialCpfCnpjParticipante'.
     * 
     * @return int
     * @return the value of field 'cdFilialCpfCnpjParticipante'.
     */
    public int getCdFilialCpfCnpjParticipante()
    {
        return this._cdFilialCpfCnpjParticipante;
    } //-- int getCdFilialCpfCnpjParticipante() 

    /**
     * Returns the value of field 'cdIndicadorTipoManutencao'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorTipoManutencao'.
     */
    public int getCdIndicadorTipoManutencao()
    {
        return this._cdIndicadorTipoManutencao;
    } //-- int getCdIndicadorTipoManutencao() 

    /**
     * Returns the value of field 'cdOperacaoCanalInclusao'.
     * 
     * @return String
     * @return the value of field 'cdOperacaoCanalInclusao'.
     */
    public java.lang.String getCdOperacaoCanalInclusao()
    {
        return this._cdOperacaoCanalInclusao;
    } //-- java.lang.String getCdOperacaoCanalInclusao() 

    /**
     * Returns the value of field 'cdOperacaoCanalManutencao'.
     * 
     * @return String
     * @return the value of field 'cdOperacaoCanalManutencao'.
     */
    public java.lang.String getCdOperacaoCanalManutencao()
    {
        return this._cdOperacaoCanalManutencao;
    } //-- java.lang.String getCdOperacaoCanalManutencao() 

    /**
     * Returns the value of field 'cdTipoCanalInclusao'.
     * 
     * @return int
     * @return the value of field 'cdTipoCanalInclusao'.
     */
    public int getCdTipoCanalInclusao()
    {
        return this._cdTipoCanalInclusao;
    } //-- int getCdTipoCanalInclusao() 

    /**
     * Returns the value of field 'cdTipoCanalManutencao'.
     * 
     * @return int
     * @return the value of field 'cdTipoCanalManutencao'.
     */
    public int getCdTipoCanalManutencao()
    {
        return this._cdTipoCanalManutencao;
    } //-- int getCdTipoCanalManutencao() 

    /**
     * Returns the value of field 'cdTipoContaRelacionamento'.
     * 
     * @return int
     * @return the value of field 'cdTipoContaRelacionamento'.
     */
    public int getCdTipoContaRelacionamento()
    {
        return this._cdTipoContaRelacionamento;
    } //-- int getCdTipoContaRelacionamento() 

    /**
     * Returns the value of field 'cdUsuarioInclusao'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioInclusao'.
     */
    public java.lang.String getCdUsuarioInclusao()
    {
        return this._cdUsuarioInclusao;
    } //-- java.lang.String getCdUsuarioInclusao() 

    /**
     * Returns the value of field 'cdUsuarioInclusaoExterno'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioInclusaoExterno'.
     */
    public java.lang.String getCdUsuarioInclusaoExterno()
    {
        return this._cdUsuarioInclusaoExterno;
    } //-- java.lang.String getCdUsuarioInclusaoExterno() 

    /**
     * Returns the value of field 'cdUsuarioManutencao'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioManutencao'.
     */
    public java.lang.String getCdUsuarioManutencao()
    {
        return this._cdUsuarioManutencao;
    } //-- java.lang.String getCdUsuarioManutencao() 

    /**
     * Returns the value of field 'cdUsuarioManutencaoExterno'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioManutencaoExterno'.
     */
    public java.lang.String getCdUsuarioManutencaoExterno()
    {
        return this._cdUsuarioManutencaoExterno;
    } //-- java.lang.String getCdUsuarioManutencaoExterno() 

    /**
     * Returns the value of field 'codMensagem'.
     * 
     * @return String
     * @return the value of field 'codMensagem'.
     */
    public java.lang.String getCodMensagem()
    {
        return this._codMensagem;
    } //-- java.lang.String getCodMensagem() 

    /**
     * Returns the value of field 'dsCanalInclusao'.
     * 
     * @return String
     * @return the value of field 'dsCanalInclusao'.
     */
    public java.lang.String getDsCanalInclusao()
    {
        return this._dsCanalInclusao;
    } //-- java.lang.String getDsCanalInclusao() 

    /**
     * Returns the value of field 'dsCanalManutencao'.
     * 
     * @return String
     * @return the value of field 'dsCanalManutencao'.
     */
    public java.lang.String getDsCanalManutencao()
    {
        return this._dsCanalManutencao;
    } //-- java.lang.String getDsCanalManutencao() 

    /**
     * Returns the value of field 'dsTipoContaRelacionamento'.
     * 
     * @return String
     * @return the value of field 'dsTipoContaRelacionamento'.
     */
    public java.lang.String getDsTipoContaRelacionamento()
    {
        return this._dsTipoContaRelacionamento;
    } //-- java.lang.String getDsTipoContaRelacionamento() 

    /**
     * Returns the value of field 'dsTipoManutencao'.
     * 
     * @return String
     * @return the value of field 'dsTipoManutencao'.
     */
    public java.lang.String getDsTipoManutencao()
    {
        return this._dsTipoManutencao;
    } //-- java.lang.String getDsTipoManutencao() 

    /**
     * Returns the value of field 'hrInclusaoRegistro'.
     * 
     * @return String
     * @return the value of field 'hrInclusaoRegistro'.
     */
    public java.lang.String getHrInclusaoRegistro()
    {
        return this._hrInclusaoRegistro;
    } //-- java.lang.String getHrInclusaoRegistro() 

    /**
     * Returns the value of field 'hrManutencaoRegistro'.
     * 
     * @return String
     * @return the value of field 'hrManutencaoRegistro'.
     */
    public java.lang.String getHrManutencaoRegistro()
    {
        return this._hrManutencaoRegistro;
    } //-- java.lang.String getHrManutencaoRegistro() 

    /**
     * Returns the value of field 'mensagem'.
     * 
     * @return String
     * @return the value of field 'mensagem'.
     */
    public java.lang.String getMensagem()
    {
        return this._mensagem;
    } //-- java.lang.String getMensagem() 

    /**
     * Method hasCdAgenciaRelacionamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgenciaRelacionamento()
    {
        return this._has_cdAgenciaRelacionamento;
    } //-- boolean hasCdAgenciaRelacionamento() 

    /**
     * Method hasCdBancoRelacionamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdBancoRelacionamento()
    {
        return this._has_cdBancoRelacionamento;
    } //-- boolean hasCdBancoRelacionamento() 

    /**
     * Method hasCdContaRelacionamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdContaRelacionamento()
    {
        return this._has_cdContaRelacionamento;
    } //-- boolean hasCdContaRelacionamento() 

    /**
     * Method hasCdCorpoCpfCnpjParticipante
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCorpoCpfCnpjParticipante()
    {
        return this._has_cdCorpoCpfCnpjParticipante;
    } //-- boolean hasCdCorpoCpfCnpjParticipante() 

    /**
     * Method hasCdDigitoCpfCnpjParticipante
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdDigitoCpfCnpjParticipante()
    {
        return this._has_cdDigitoCpfCnpjParticipante;
    } //-- boolean hasCdDigitoCpfCnpjParticipante() 

    /**
     * Method hasCdFilialCpfCnpjParticipante
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFilialCpfCnpjParticipante()
    {
        return this._has_cdFilialCpfCnpjParticipante;
    } //-- boolean hasCdFilialCpfCnpjParticipante() 

    /**
     * Method hasCdIndicadorTipoManutencao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorTipoManutencao()
    {
        return this._has_cdIndicadorTipoManutencao;
    } //-- boolean hasCdIndicadorTipoManutencao() 

    /**
     * Method hasCdTipoCanalInclusao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoCanalInclusao()
    {
        return this._has_cdTipoCanalInclusao;
    } //-- boolean hasCdTipoCanalInclusao() 

    /**
     * Method hasCdTipoCanalManutencao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoCanalManutencao()
    {
        return this._has_cdTipoCanalManutencao;
    } //-- boolean hasCdTipoCanalManutencao() 

    /**
     * Method hasCdTipoContaRelacionamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContaRelacionamento()
    {
        return this._has_cdTipoContaRelacionamento;
    } //-- boolean hasCdTipoContaRelacionamento() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdAgenciaRelacionamento'.
     * 
     * @param cdAgenciaRelacionamento the value of field
     * 'cdAgenciaRelacionamento'.
     */
    public void setCdAgenciaRelacionamento(int cdAgenciaRelacionamento)
    {
        this._cdAgenciaRelacionamento = cdAgenciaRelacionamento;
        this._has_cdAgenciaRelacionamento = true;
    } //-- void setCdAgenciaRelacionamento(int) 

    /**
     * Sets the value of field 'cdBancoRelacionamento'.
     * 
     * @param cdBancoRelacionamento the value of field
     * 'cdBancoRelacionamento'.
     */
    public void setCdBancoRelacionamento(int cdBancoRelacionamento)
    {
        this._cdBancoRelacionamento = cdBancoRelacionamento;
        this._has_cdBancoRelacionamento = true;
    } //-- void setCdBancoRelacionamento(int) 

    /**
     * Sets the value of field 'cdContaRelacionamento'.
     * 
     * @param cdContaRelacionamento the value of field
     * 'cdContaRelacionamento'.
     */
    public void setCdContaRelacionamento(long cdContaRelacionamento)
    {
        this._cdContaRelacionamento = cdContaRelacionamento;
        this._has_cdContaRelacionamento = true;
    } //-- void setCdContaRelacionamento(long) 

    /**
     * Sets the value of field 'cdCorpoCpfCnpjParticipante'.
     * 
     * @param cdCorpoCpfCnpjParticipante the value of field
     * 'cdCorpoCpfCnpjParticipante'.
     */
    public void setCdCorpoCpfCnpjParticipante(long cdCorpoCpfCnpjParticipante)
    {
        this._cdCorpoCpfCnpjParticipante = cdCorpoCpfCnpjParticipante;
        this._has_cdCorpoCpfCnpjParticipante = true;
    } //-- void setCdCorpoCpfCnpjParticipante(long) 

    /**
     * Sets the value of field 'cdDigitoContaRelacionamento'.
     * 
     * @param cdDigitoContaRelacionamento the value of field
     * 'cdDigitoContaRelacionamento'.
     */
    public void setCdDigitoContaRelacionamento(java.lang.String cdDigitoContaRelacionamento)
    {
        this._cdDigitoContaRelacionamento = cdDigitoContaRelacionamento;
    } //-- void setCdDigitoContaRelacionamento(java.lang.String) 

    /**
     * Sets the value of field 'cdDigitoCpfCnpjParticipante'.
     * 
     * @param cdDigitoCpfCnpjParticipante the value of field
     * 'cdDigitoCpfCnpjParticipante'.
     */
    public void setCdDigitoCpfCnpjParticipante(int cdDigitoCpfCnpjParticipante)
    {
        this._cdDigitoCpfCnpjParticipante = cdDigitoCpfCnpjParticipante;
        this._has_cdDigitoCpfCnpjParticipante = true;
    } //-- void setCdDigitoCpfCnpjParticipante(int) 

    /**
     * Sets the value of field 'cdFilialCpfCnpjParticipante'.
     * 
     * @param cdFilialCpfCnpjParticipante the value of field
     * 'cdFilialCpfCnpjParticipante'.
     */
    public void setCdFilialCpfCnpjParticipante(int cdFilialCpfCnpjParticipante)
    {
        this._cdFilialCpfCnpjParticipante = cdFilialCpfCnpjParticipante;
        this._has_cdFilialCpfCnpjParticipante = true;
    } //-- void setCdFilialCpfCnpjParticipante(int) 

    /**
     * Sets the value of field 'cdIndicadorTipoManutencao'.
     * 
     * @param cdIndicadorTipoManutencao the value of field
     * 'cdIndicadorTipoManutencao'.
     */
    public void setCdIndicadorTipoManutencao(int cdIndicadorTipoManutencao)
    {
        this._cdIndicadorTipoManutencao = cdIndicadorTipoManutencao;
        this._has_cdIndicadorTipoManutencao = true;
    } //-- void setCdIndicadorTipoManutencao(int) 

    /**
     * Sets the value of field 'cdOperacaoCanalInclusao'.
     * 
     * @param cdOperacaoCanalInclusao the value of field
     * 'cdOperacaoCanalInclusao'.
     */
    public void setCdOperacaoCanalInclusao(java.lang.String cdOperacaoCanalInclusao)
    {
        this._cdOperacaoCanalInclusao = cdOperacaoCanalInclusao;
    } //-- void setCdOperacaoCanalInclusao(java.lang.String) 

    /**
     * Sets the value of field 'cdOperacaoCanalManutencao'.
     * 
     * @param cdOperacaoCanalManutencao the value of field
     * 'cdOperacaoCanalManutencao'.
     */
    public void setCdOperacaoCanalManutencao(java.lang.String cdOperacaoCanalManutencao)
    {
        this._cdOperacaoCanalManutencao = cdOperacaoCanalManutencao;
    } //-- void setCdOperacaoCanalManutencao(java.lang.String) 

    /**
     * Sets the value of field 'cdTipoCanalInclusao'.
     * 
     * @param cdTipoCanalInclusao the value of field
     * 'cdTipoCanalInclusao'.
     */
    public void setCdTipoCanalInclusao(int cdTipoCanalInclusao)
    {
        this._cdTipoCanalInclusao = cdTipoCanalInclusao;
        this._has_cdTipoCanalInclusao = true;
    } //-- void setCdTipoCanalInclusao(int) 

    /**
     * Sets the value of field 'cdTipoCanalManutencao'.
     * 
     * @param cdTipoCanalManutencao the value of field
     * 'cdTipoCanalManutencao'.
     */
    public void setCdTipoCanalManutencao(int cdTipoCanalManutencao)
    {
        this._cdTipoCanalManutencao = cdTipoCanalManutencao;
        this._has_cdTipoCanalManutencao = true;
    } //-- void setCdTipoCanalManutencao(int) 

    /**
     * Sets the value of field 'cdTipoContaRelacionamento'.
     * 
     * @param cdTipoContaRelacionamento the value of field
     * 'cdTipoContaRelacionamento'.
     */
    public void setCdTipoContaRelacionamento(int cdTipoContaRelacionamento)
    {
        this._cdTipoContaRelacionamento = cdTipoContaRelacionamento;
        this._has_cdTipoContaRelacionamento = true;
    } //-- void setCdTipoContaRelacionamento(int) 

    /**
     * Sets the value of field 'cdUsuarioInclusao'.
     * 
     * @param cdUsuarioInclusao the value of field
     * 'cdUsuarioInclusao'.
     */
    public void setCdUsuarioInclusao(java.lang.String cdUsuarioInclusao)
    {
        this._cdUsuarioInclusao = cdUsuarioInclusao;
    } //-- void setCdUsuarioInclusao(java.lang.String) 

    /**
     * Sets the value of field 'cdUsuarioInclusaoExterno'.
     * 
     * @param cdUsuarioInclusaoExterno the value of field
     * 'cdUsuarioInclusaoExterno'.
     */
    public void setCdUsuarioInclusaoExterno(java.lang.String cdUsuarioInclusaoExterno)
    {
        this._cdUsuarioInclusaoExterno = cdUsuarioInclusaoExterno;
    } //-- void setCdUsuarioInclusaoExterno(java.lang.String) 

    /**
     * Sets the value of field 'cdUsuarioManutencao'.
     * 
     * @param cdUsuarioManutencao the value of field
     * 'cdUsuarioManutencao'.
     */
    public void setCdUsuarioManutencao(java.lang.String cdUsuarioManutencao)
    {
        this._cdUsuarioManutencao = cdUsuarioManutencao;
    } //-- void setCdUsuarioManutencao(java.lang.String) 

    /**
     * Sets the value of field 'cdUsuarioManutencaoExterno'.
     * 
     * @param cdUsuarioManutencaoExterno the value of field
     * 'cdUsuarioManutencaoExterno'.
     */
    public void setCdUsuarioManutencaoExterno(java.lang.String cdUsuarioManutencaoExterno)
    {
        this._cdUsuarioManutencaoExterno = cdUsuarioManutencaoExterno;
    } //-- void setCdUsuarioManutencaoExterno(java.lang.String) 

    /**
     * Sets the value of field 'codMensagem'.
     * 
     * @param codMensagem the value of field 'codMensagem'.
     */
    public void setCodMensagem(java.lang.String codMensagem)
    {
        this._codMensagem = codMensagem;
    } //-- void setCodMensagem(java.lang.String) 

    /**
     * Sets the value of field 'dsCanalInclusao'.
     * 
     * @param dsCanalInclusao the value of field 'dsCanalInclusao'.
     */
    public void setDsCanalInclusao(java.lang.String dsCanalInclusao)
    {
        this._dsCanalInclusao = dsCanalInclusao;
    } //-- void setDsCanalInclusao(java.lang.String) 

    /**
     * Sets the value of field 'dsCanalManutencao'.
     * 
     * @param dsCanalManutencao the value of field
     * 'dsCanalManutencao'.
     */
    public void setDsCanalManutencao(java.lang.String dsCanalManutencao)
    {
        this._dsCanalManutencao = dsCanalManutencao;
    } //-- void setDsCanalManutencao(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoContaRelacionamento'.
     * 
     * @param dsTipoContaRelacionamento the value of field
     * 'dsTipoContaRelacionamento'.
     */
    public void setDsTipoContaRelacionamento(java.lang.String dsTipoContaRelacionamento)
    {
        this._dsTipoContaRelacionamento = dsTipoContaRelacionamento;
    } //-- void setDsTipoContaRelacionamento(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoManutencao'.
     * 
     * @param dsTipoManutencao the value of field 'dsTipoManutencao'
     */
    public void setDsTipoManutencao(java.lang.String dsTipoManutencao)
    {
        this._dsTipoManutencao = dsTipoManutencao;
    } //-- void setDsTipoManutencao(java.lang.String) 

    /**
     * Sets the value of field 'hrInclusaoRegistro'.
     * 
     * @param hrInclusaoRegistro the value of field
     * 'hrInclusaoRegistro'.
     */
    public void setHrInclusaoRegistro(java.lang.String hrInclusaoRegistro)
    {
        this._hrInclusaoRegistro = hrInclusaoRegistro;
    } //-- void setHrInclusaoRegistro(java.lang.String) 

    /**
     * Sets the value of field 'hrManutencaoRegistro'.
     * 
     * @param hrManutencaoRegistro the value of field
     * 'hrManutencaoRegistro'.
     */
    public void setHrManutencaoRegistro(java.lang.String hrManutencaoRegistro)
    {
        this._hrManutencaoRegistro = hrManutencaoRegistro;
    } //-- void setHrManutencaoRegistro(java.lang.String) 

    /**
     * Sets the value of field 'mensagem'.
     * 
     * @param mensagem the value of field 'mensagem'.
     */
    public void setMensagem(java.lang.String mensagem)
    {
        this._mensagem = mensagem;
    } //-- void setMensagem(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return DetalharContaRelacionadaHistResponse
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.detalharcontarelacionadahist.response.DetalharContaRelacionadaHistResponse unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.detalharcontarelacionadahist.response.DetalharContaRelacionadaHistResponse) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.detalharcontarelacionadahist.response.DetalharContaRelacionadaHistResponse.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharcontarelacionadahist.response.DetalharContaRelacionadaHistResponse unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
