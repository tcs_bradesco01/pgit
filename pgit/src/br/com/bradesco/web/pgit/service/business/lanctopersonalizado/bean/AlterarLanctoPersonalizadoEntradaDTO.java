/*
 * Nome: br.com.bradesco.web.pgit.service.business.lanctopersonalizado.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.lanctopersonalizado.bean;

/**
 * Nome: AlterarLanctoPersonalizadoEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class AlterarLanctoPersonalizadoEntradaDTO {
	
	/** Atributo codLancamento. */
	private int codLancamento;
	
	/** Atributo tipoServico. */
	private int tipoServico;
	
	/** Atributo modalidadeServico. */
	private int modalidadeServico;
	
	/** Atributo tipoRelacionamento. */
	private int tipoRelacionamento;
	
	/** Atributo codLancamentoDebito. */
	private int codLancamentoDebito;
	
	/** Atributo codLancamentoCredito. */
	private int codLancamentoCredito;
	
	/** Atributo restricao. */
	private int restricao;
	
	/** Atributo tipoLancamento. */
	private int tipoLancamento;
	
	/**
	 * Get: codLancamentoCredito.
	 *
	 * @return codLancamentoCredito
	 */
	public int getCodLancamentoCredito() {
		return codLancamentoCredito;
	}
	
	/**
	 * Set: codLancamentoCredito.
	 *
	 * @param codLancamentoCredito the cod lancamento credito
	 */
	public void setCodLancamentoCredito(int codLancamentoCredito) {
		this.codLancamentoCredito = codLancamentoCredito;
	}
	
	/**
	 * Get: codLancamentoDebito.
	 *
	 * @return codLancamentoDebito
	 */
	public int getCodLancamentoDebito() {
		return codLancamentoDebito;
	}
	
	/**
	 * Set: codLancamentoDebito.
	 *
	 * @param codLancamentoDebito the cod lancamento debito
	 */
	public void setCodLancamentoDebito(int codLancamentoDebito) {
		this.codLancamentoDebito = codLancamentoDebito;
	}
	
	/**
	 * Get: restricao.
	 *
	 * @return restricao
	 */
	public int getRestricao() {
		return restricao;
	}
	
	/**
	 * Set: restricao.
	 *
	 * @param restricao the restricao
	 */
	public void setRestricao(int restricao) {
		this.restricao = restricao;
	}
	
	/**
	 * Get: tipoLancamento.
	 *
	 * @return tipoLancamento
	 */
	public int getTipoLancamento() {
		return tipoLancamento;
	}
	
	/**
	 * Set: tipoLancamento.
	 *
	 * @param tipoLancamento the tipo lancamento
	 */
	public void setTipoLancamento(int tipoLancamento) {
		this.tipoLancamento = tipoLancamento;
	}
	
	/**
	 * Get: codLancamento.
	 *
	 * @return codLancamento
	 */
	public int getCodLancamento() {
		return codLancamento;
	}
	
	/**
	 * Set: codLancamento.
	 *
	 * @param codLancamento the cod lancamento
	 */
	public void setCodLancamento(int codLancamento) {
		this.codLancamento = codLancamento;
	}
	
	/**
	 * Get: modalidadeServico.
	 *
	 * @return modalidadeServico
	 */
	public int getModalidadeServico() {
		return modalidadeServico;
	}
	
	/**
	 * Set: modalidadeServico.
	 *
	 * @param modalidadeServico the modalidade servico
	 */
	public void setModalidadeServico(int modalidadeServico) {
		this.modalidadeServico = modalidadeServico;
	}
	
	/**
	 * Get: tipoRelacionamento.
	 *
	 * @return tipoRelacionamento
	 */
	public int getTipoRelacionamento() {
		return tipoRelacionamento;
	}
	
	/**
	 * Set: tipoRelacionamento.
	 *
	 * @param tipoRelacionamento the tipo relacionamento
	 */
	public void setTipoRelacionamento(int tipoRelacionamento) {
		this.tipoRelacionamento = tipoRelacionamento;
	}
	
	/**
	 * Get: tipoServico.
	 *
	 * @return tipoServico
	 */
	public int getTipoServico() {
		return tipoServico;
	}
	
	/**
	 * Set: tipoServico.
	 *
	 * @param tipoServico the tipo servico
	 */
	public void setTipoServico(int tipoServico) {
		this.tipoServico = tipoServico;
	}

}
