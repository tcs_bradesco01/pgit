/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/implementation.ftl,v $
 * $Id: implementation.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: implementation.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.arquivoretorno.impl;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterException;
import br.com.bradesco.web.pgit.service.business.arquivorecebido.bean.ComboProdutoDTO;
import br.com.bradesco.web.pgit.service.business.arquivoretorno.IArquivoRetornoService;
import br.com.bradesco.web.pgit.service.business.arquivoretorno.bean.ArquivoRetornoDTO;
import br.com.bradesco.web.pgit.service.business.arquivoretorno.bean.InconsistentesRetornoDTO;
import br.com.bradesco.web.pgit.service.business.arquivoretorno.bean.LoteRetornoDTO;
import br.com.bradesco.web.pgit.service.business.arquivoretorno.bean.OcorrenciaRetornoDTO;
import br.com.bradesco.web.pgit.service.business.cmpi0000.bean.ListarClientesDTO;
import br.com.bradesco.web.pgit.service.data.pdc.FactoryAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarinconsistenciaretorno.IConsultarInconsistenciaRetornoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarinconsistenciaretorno.request.ConsInconsistenciaRetornoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarinconsistenciaretorno.response.ConsInconsistenciaRetornoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.consultarloteretorno.IConsultarLoteRetornoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarloteretorno.request.ConsultarLoteRetornoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarloteretorno.response.ConsultarLoteRetornoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.consultarocorrenciaretorno.IConsultarOcorrenciaRetornoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarocorrenciaretorno.request.ConsOcorrenciaRetornoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarocorrenciaretorno.response.ConsOcorrenciaRetornoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.consultarretorno.IConsultarRetornoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarretorno.request.ConsultarRetornoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarretorno.response.ConsultarRetornoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarclientes.IListarClientesPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarclientes.request.ListarClientesRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarclientes.response.ListarClientesResponse;
import br.com.bradesco.web.pgit.service.data.pdc.montarcomboproduto.IMontarComboProdutoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.montarcomboproduto.request.MontarComboProdutoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.montarcomboproduto.response.MontarComboProdutoResponse;
import br.com.bradesco.web.pgit.utils.PgitUtil;
import br.com.bradesco.web.pgit.utils.SiteUtil;

/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Implementa��o do adaptador: Arquivoremessaretorno
 * </p>
 * 
 * @comment C�DIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public class ArquivoRetornoServiceImpl implements IArquivoRetornoService {

	/** Atributo factoryAdapter. */
	private FactoryAdapter factoryAdapter;

	/**
	 * Construtor.
	 */
	public ArquivoRetornoServiceImpl() {
		// TODO: Implementa��o
	}

	/**
	 * M�todo de exemplo.
	 * 
	 * @see br.com.bradesco.web.pgit.service.business.arquivoretorno.IArquivoremessaretorno#sampleArquivoremessaretorno()
	 */
	public void sampleArquivoremessaretorno() {
		// TODO: Implementa�ao
	}

	/**
	 * Get: factoryAdapter.
	 *
	 * @return factoryAdapter
	 */
	public FactoryAdapter getFactoryAdapter() {
		return factoryAdapter;
	}

	/**
	 * Set: factoryAdapter.
	 *
	 * @param factoryAdapter the factory adapter
	 */
	public void setFactoryAdapter(FactoryAdapter factoryAdapter) {
		this.factoryAdapter = factoryAdapter;
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.arquivoretorno.IArquivoRetornoService#listaGrid(br.com.bradesco.web.pgit.service.business.arquivoretorno.bean.ArquivoRetornoDTO)
	 */
	public List<ArquivoRetornoDTO> listaGrid(ArquivoRetornoDTO pArquivoRemessaRetornoDTO) throws PdcAdapterException {

		List<ArquivoRetornoDTO> lArquivos = new ArrayList<ArquivoRetornoDTO>();
		ConsultarRetornoRequest lRequest = new ConsultarRetornoRequest();
		ConsultarRetornoResponse lResponse = null;

		lRequest.setQtdeOcorrencia(50);

		// PRODUTO
		if (!(SiteUtil.isEmptyOrNull(pArquivoRemessaRetornoDTO.getProduto()))) {
			lRequest.setCodigoSistemaLegado(pArquivoRemessaRetornoDTO.getProduto());
		} else {
			lRequest.setCodigoSistemaLegado("");
		}

		// PERFIL
		if (!(SiteUtil.isEmptyOrNull(pArquivoRemessaRetornoDTO.getPerfil()))) {
			lRequest.setCodigoPerfilComunicacao(new Long(pArquivoRemessaRetornoDTO.getPerfil()));	
		} else {
			lRequest.setCodigoPerfilComunicacao(0);
		}

		// TIPO DE RETORNO
		if (!(SiteUtil.isEmptyOrNull(pArquivoRemessaRetornoDTO.getTipoArquivoRetorno()))) {
			lRequest.setCodigoTipoRetorno(Integer.parseInt(pArquivoRemessaRetornoDTO.getTipoArquivoRetorno()));
		} else {
			lRequest.setCodigoTipoRetorno(0);
		}

		// SEQUENCIA DE RETORNO
		if (!(SiteUtil.isEmptyOrNull(pArquivoRemessaRetornoDTO.getNumeroArquivoRetorno()))) {
			lRequest.setNumeroArquivoRetorno(Integer.parseInt(pArquivoRemessaRetornoDTO.getNumeroArquivoRetorno()));
		} else {
			lRequest.setNumeroArquivoRetorno(0);
		}

		// DATA DE GERACAO
		if (!(SiteUtil.isEmptyOrNull(pArquivoRemessaRetornoDTO.getHoraRecepcaoInicio()))) {
			lRequest.setHoraRecepcaoInicio(pArquivoRemessaRetornoDTO.getHoraRecepcaoInicio());
		} else {
			lRequest.setHoraRecepcaoInicio("");
		}

		if (!(SiteUtil.isEmptyOrNull(pArquivoRemessaRetornoDTO.getHoraRecepcaoFim()))) {
			lRequest.setHoraRecepcaoFim(pArquivoRemessaRetornoDTO.getHoraRecepcaoFim());
		} else {
			lRequest.setHoraRecepcaoFim("");
		}

		// SITUACAO DE RETORNO
		if (!(SiteUtil.isEmptyOrNull(pArquivoRemessaRetornoDTO.getSituacaoGeracaoRetorno()))) {
			lRequest.setSituacaoGeracaoRetorno(Integer.parseInt(pArquivoRemessaRetornoDTO.getSituacaoGeracaoRetorno()));
		} else {
			lRequest.setSituacaoGeracaoRetorno(0);
		}

		// CPF
		if (!(SiteUtil.isEmptyOrNull(pArquivoRemessaRetornoDTO.getCpf_cliente()))) {
			lRequest.setCpfCnpj(new Long(pArquivoRemessaRetornoDTO.getCpf_cliente()));
		} else {
			lRequest.setCpfCnpj(0);
		}

		if (!(SiteUtil.isEmptyOrNull(pArquivoRemessaRetornoDTO.getCpf_filial()))) {
			lRequest.setFilial(Integer.parseInt(pArquivoRemessaRetornoDTO.getCpf_filial()));
		} else {
			lRequest.setFilial(0);
		}

		if (!(SiteUtil.isEmptyOrNull(pArquivoRemessaRetornoDTO.getCpf_controle()))) {
			lRequest.setControle(Integer.parseInt(pArquivoRemessaRetornoDTO.getCpf_controle()));
		} else {
			lRequest.setControle(0);
		}

		// Fazemos a chamada do processo.
		IConsultarRetornoPDCAdapter consultarRetornoPDCAdapter = factoryAdapter.getConsultarRetornoPDCAdapter();
		lResponse = consultarRetornoPDCAdapter.invokeProcess(lRequest);

		ArquivoRetornoDTO lArquivo;
		// Para cada item retornado do processo, adicionamos a lista de
		// retorno.
		for (int i = 0; i < lResponse.getOcorrenciasCount(); i++) {

			lArquivo = new ArquivoRetornoDTO();

			String cpfCliente = String.valueOf(lResponse.getOcorrencias(i).getCpfCnpj());
			String cpfFilial = String.valueOf(lResponse.getOcorrencias(i).getFilialCnpj());
			String cpfControle = String.valueOf(lResponse.getOcorrencias(i).getControle());

			String cpf = "";

			if (lResponse.getOcorrencias(i).getFilialCnpj() == 0) {
				cpf = PgitUtil.formatarCPFComMascara(cpfCliente, cpfControle);
			} else {
				cpf = PgitUtil.formatarCNPJComMascara(cpfCliente, cpfFilial, cpfControle);
			}

			lArquivo.setCpf_completo(cpf);
			lArquivo.setCpf_cliente(cpfCliente);
			lArquivo.setCpf_filial(cpfFilial);
			lArquivo.setCpf_controle(cpfControle);

			lArquivo.setNome(lResponse.getOcorrencias(i).getNomeRazao());
			lArquivo.setProduto(lResponse.getOcorrencias(i).getDescricaoProdutoServico());
			lArquivo.setPerfil(String.valueOf(lResponse.getOcorrencias(i).getCodigoPerfilComunicacao()));

			if (lResponse.getOcorrencias(i).getHoraGeracaoRetorno() != null) {
				try {
					lArquivo.setHoraGeracaoRetorno(SiteUtil.changeStringDateFormat(lResponse.getOcorrencias(i).getHoraGeracaoRetorno(), "yyyy-MM-dd-HH.mm.ss", "dd/MM/yyyy HH:mm:ss"));
				} catch (ParseException e) {
					lArquivo.setHoraGeracaoRetorno("");
				}
			} else {
				lArquivo.setHoraGeracaoRetorno("");
			}

			if (lResponse.getOcorrencias(i).getHoraGeracaoRetorno() != null) {
				lArquivo.setHoraInclusao(lResponse.getOcorrencias(i).getHoraGeracaoRetorno());
			}

			lArquivo.setTipoRetorno(String.valueOf(lResponse.getOcorrencias(i).getCodigoTipoRetorno()));
			lArquivo.setNumeroArquivoRetorno(String.valueOf(lResponse.getOcorrencias(i).getNumeroArquivoRetorno()));

			lArquivo.setSituacaoGeracaoRetorno(String.valueOf(lResponse.getOcorrencias(i).getCodigoSituacaoRetorno()));
			
			lArquivo.setValorRegistroArquivoRetorno(PgitUtil.formatarCampoDeValorMonetario(lResponse.getOcorrencias(i).getValorRegistro()));
			lArquivo.setQtdeOcorrencias(PgitUtil.formatarCampoQuantidade(String.valueOf(lResponse.getOcorrencias(i).getQuantidadeRegistro())));
			lArquivo.setNumeroArquivoRetornoLong(lResponse.getOcorrencias(i).getNumeroArquivoRetorno());

			if (lResponse.getOcorrencias(i).getCodigoBancoDebito() != 0) {
				lArquivo.setBancoDebito(String.valueOf(lResponse.getOcorrencias(i).getCodigoBancoDebito()));
			} else {
				lArquivo.setBancoDebito("");
			}

			if (lResponse.getOcorrencias(i).getAgenciaBancariaDebito() != 0) {
				lArquivo.setAgenciaDebito(String.valueOf(lResponse.getOcorrencias(i).getAgenciaBancariaDebito()));
			} else {
				lArquivo.setAgenciaDebito("");
			}

			if (lResponse.getOcorrencias(i).getContaRazaoDebito() != 0) {
				lArquivo.setRazaoContaDebito(String.valueOf(lResponse.getOcorrencias(i).getContaRazaoDebito()));
			} else {
				lArquivo.setRazaoContaDebito("");
			}

			if (lResponse.getOcorrencias(i).getContaBancariaDebito() != 0) {
				lArquivo.setContaBancaria(String.valueOf(lResponse.getOcorrencias(i).getContaBancariaDebito()));
			} else {
				lArquivo.setContaBancaria("");
			}

			if (lResponse.getOcorrencias(i).getCodigoLancamentoContaDebito() != 0) {
				lArquivo.setLancamentoContaDebito(String.valueOf(lResponse.getOcorrencias(i).getCodigoLancamentoContaDebito()));
			} else {
				lArquivo.setLancamentoContaDebito("");
			}

			lArquivo.setTipoLayout(String.valueOf(lResponse.getOcorrencias(i).getDescricaoTipoLayout()));
			lArquivo.setDescricaoTipoArquivo(lResponse.getOcorrencias(i).getDescricaoTipoRetorno());
			lArquivo.setMeioTransmissaoArquivo(lResponse.getOcorrencias(i).getDescricaoMeioTransmissao());
			lArquivo.setCodigoAmbiente(lResponse.getOcorrencias(i).getDescricaoAmbiente());
			lArquivo.setCodigoSistemaLegado(lResponse.getOcorrencias(i).getCodigoSistemaLegado());
			lArquivo.setPerfil(String.valueOf(lResponse.getOcorrencias(i).getCodigoPerfilComunicacao()));
			lArquivo.setDescricaoSituacao(lResponse.getOcorrencias(i).getDescricaoSituacaoRetorno());
			lArquivo.setTotalRegistros(i);

			lArquivos.add(lArquivo);
		}

		return lArquivos;
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.arquivoretorno.IArquivoRetornoService#listaComboProduto()
	 */
	public List<ComboProdutoDTO> listaComboProduto() throws PdcAdapterException {

		List<ComboProdutoDTO> lArquivos = new ArrayList<ComboProdutoDTO>();
		MontarComboProdutoRequest lRequest = new MontarComboProdutoRequest();
		MontarComboProdutoResponse lResponse = null;

		// Fazemos a chamada do processo.
		IMontarComboProdutoPDCAdapter montarComboProdutoPDCAdapter = factoryAdapter.getMontarComboProdutoPDCAdapter();

		lRequest.setEntrada("");

		lResponse = montarComboProdutoPDCAdapter.invokeProcess(lRequest);

		ComboProdutoDTO lArquivo;

		// Para cada item retornado do processo, adicionamos a lista de
		// retorno.
		for (int i = 0; i < lResponse.getOcorrenciasCount(); i++) {

			lArquivo = new ComboProdutoDTO();
			lArquivo.setCentroCusto(String.valueOf(lResponse.getOcorrencias(i).getCodigoCentroCusto()));
			lArquivo.setDescricaoCentroCusto(lResponse.getOcorrencias(i).getDescricaoProdutoPagamento());
			lArquivo.setCodigoProduto(String.valueOf(lResponse.getOcorrencias(i).getCodigoProdutoPagamento()));
			lArquivos.add(lArquivo);
		}

		return lArquivos;
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.arquivoretorno.IArquivoRetornoService#listaLote(br.com.bradesco.web.pgit.service.business.arquivoretorno.bean.LoteRetornoDTO)
	 */
	public List<LoteRetornoDTO> listaLote(LoteRetornoDTO pLoteDTO) throws PdcAdapterException {

		ConsultarLoteRetornoRequest lRequest = new ConsultarLoteRetornoRequest();
		ConsultarLoteRetornoResponse lResponse = null;
		List<LoteRetornoDTO> listaResultado = null;

		lRequest.setQtdeOcorrencia(100);
		lRequest.setHoraInclusao(pLoteDTO.getHoraInclusao());
		lRequest.setNumeroArquivoRetorno(pLoteDTO.getNumeroArquivoRetorno());
		lRequest.setNumeroLote(new Long(0));
		lRequest.setNumeroSequenciaRegistroRetorno(new Long(0));

		if (pLoteDTO.getCodigoCpfCnpj() != null && !pLoteDTO.getCodigoCpfCnpj().equals("")) {
			lRequest.setCpfCnpj(new Long(pLoteDTO.getCodigoCpfCnpj()));
		} else {
			lRequest.setCpfCnpj(new Long(0));
		}

		if (pLoteDTO.getCodigoFilialCpfCnpj() != null && !pLoteDTO.getCodigoFilialCpfCnpj().equals("")) {
			lRequest.setFilial(Integer.parseInt(pLoteDTO.getCodigoFilialCpfCnpj()));
		} else {
			lRequest.setFilial(0);
		}

		if (pLoteDTO.getCodigoPerfil() != null && !pLoteDTO.getCodigoPerfil().equals("")) {
			lRequest.setCodigoPerfilComunicacao(new Long(pLoteDTO.getCodigoPerfil()));
		} else {
			lRequest.setCodigoPerfilComunicacao(new Long(0));
		}

		if (pLoteDTO.getCodigoSistemaLegado() != null && !pLoteDTO.getCodigoSistemaLegado().equals("")) {
			lRequest.setCodigoSistemaLegado(pLoteDTO.getCodigoSistemaLegado());
		} else {
			lRequest.setCodigoSistemaLegado("");
		}

		IConsultarLoteRetornoPDCAdapter consultarLoteRetornoPDCAdapter = factoryAdapter.getConsultarLoteRetornoPDCAdapter();
		lResponse = consultarLoteRetornoPDCAdapter.invokeProcess(lRequest);
		LoteRetornoDTO loteDTO;
		listaResultado = new ArrayList<LoteRetornoDTO>();
		StringBuffer bcoAgeCtaDebito = null;

		for (int i = 0; i < lResponse.getNumeroLinhas(); i++) {
			loteDTO = new LoteRetornoDTO();

			bcoAgeCtaDebito = new StringBuffer(Integer.toString(lResponse.getOcorrencia(i).getCodigoBancoDebito()));
			bcoAgeCtaDebito = bcoAgeCtaDebito.append("/");
			bcoAgeCtaDebito = bcoAgeCtaDebito.append(lResponse.getOcorrencia(i).getCodigoAgenciaBancariaDebito());
			bcoAgeCtaDebito = bcoAgeCtaDebito.append("/");
			bcoAgeCtaDebito = bcoAgeCtaDebito.append(lResponse.getOcorrencia(i).getContaBancariaDebito());
			loteDTO.setBcoAgeCtaDebito(bcoAgeCtaDebito.toString());
			loteDTO.setModalidadePagto(String.valueOf(lResponse.getOcorrencia(i).getDescricaoModalidadePagamento()));			
			loteDTO.setQtdeRegistros(PgitUtil.formatarCampoQuantidade(String.valueOf(lResponse.getOcorrencia(i).getQuantidadeRegistroLote())));
			loteDTO.setSeqLote(String.valueOf(PgitUtil.formatarCampoQuantidade(String.valueOf(lResponse.getOcorrencia(i).getNumeroLoteRetorno()))));
			loteDTO.setValorRegistros(PgitUtil.formatarCampoDeValorMonetario(lResponse.getOcorrencia(i).getValorRegistroLote()));
			loteDTO.setNumeroLoteRetornoLong(lResponse.getOcorrencia(i).getNumeroLoteRetorno());
			loteDTO.setValorCon(lResponse.getOcorrencia(i).getValoeRegistroConsistidos());
			loteDTO.setValorInc(lResponse.getOcorrencia(i).getValorRegistroIncosistidos());
			loteDTO.setValorTotal(lResponse.getOcorrencia(i).getValorRegistroLote());
			loteDTO.setQuantidadeCon(lResponse.getOcorrencia(i).getQuantidadeRegistroConsistidos());
			loteDTO.setQuantidadeInc(lResponse.getOcorrencia(i).getQuantidadeRegistroInconsistidos());
			loteDTO.setQuantidadeTotal(lResponse.getOcorrencia(i).getQuantidadeRegistroLote());

			loteDTO.setSequencia(i);
			listaResultado.add(loteDTO);

		}
		return listaResultado;
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.arquivoretorno.IArquivoRetornoService#listaGridClientes(br.com.bradesco.web.pgit.service.business.cmpi0000.bean.ListarClientesDTO)
	 */
	public List<ListarClientesDTO> listaGridClientes(ListarClientesDTO pListarClientesDTO) throws PdcAdapterException {
		List<ListarClientesDTO> lListarClientes = new ArrayList<ListarClientesDTO>();
		ListarClientesRequest listarClientesRequest = new ListarClientesRequest();
		ListarClientesResponse listarClientesResponse = new ListarClientesResponse();

		// CPF/CNPJ
		if (!(SiteUtil.isEmptyOrNull(pListarClientesDTO.getCodigoCpfCnpjCliente()))) {
			listarClientesRequest.setCpfCnpj(new Long(pListarClientesDTO.getCodigoCpfCnpjCliente()));
		} else {
			listarClientesRequest.setCpfCnpj(0);
		}

		if (!(SiteUtil.isEmptyOrNull(pListarClientesDTO.getCodigoFilialCnpj()))) {
			listarClientesRequest.setFilial(Integer.parseInt(pListarClientesDTO.getCodigoFilialCnpj()));
		} else {
			listarClientesRequest.setFilial(0);
		}

		if (!(SiteUtil.isEmptyOrNull(pListarClientesDTO.getControleCpfCnpj()))) {
			listarClientesRequest.setControle(Integer.parseInt(pListarClientesDTO.getControleCpfCnpj()));
		} else {
			listarClientesRequest.setControle(0);
		}

		// Nome Raz�o
		if (!(SiteUtil.isEmptyOrNull(pListarClientesDTO.getNomeRazao()))) {
			listarClientesRequest.setNomeRazao(pListarClientesDTO.getNomeRazao());
		} else {
			listarClientesRequest.setNomeRazao("");
		}

		// DataNascimentoFundacao
		if (!(SiteUtil.isEmptyOrNull(pListarClientesDTO.getDataNascimentoFundacao()))) {
			listarClientesRequest.setDataNascimentoFundacao(pListarClientesDTO.getDataNascimentoFundacao());
		} else {
			listarClientesRequest.setDataNascimentoFundacao("");
		}

		// Codigo Banco
		if (!(SiteUtil.isEmptyOrNull(pListarClientesDTO.getCodigoBancoDebito()))) {
			listarClientesRequest.setBanco(Integer.parseInt(pListarClientesDTO.getCodigoBancoDebito()));
		} else {
			listarClientesRequest.setBanco(0);
		}

		// Agencia
		if (!(SiteUtil.isEmptyOrNull(pListarClientesDTO.getAgenciaBancaria()))) {
			listarClientesRequest.setAgencia(Integer.parseInt(pListarClientesDTO.getAgenciaBancaria()));
		} else {
			listarClientesRequest.setAgencia(0);
		}

		// Conta
		if (!(SiteUtil.isEmptyOrNull(pListarClientesDTO.getContaBancaria()))) {
			listarClientesRequest.setConta(Integer.parseInt(pListarClientesDTO.getContaBancaria()));
		} else {
			listarClientesRequest.setConta(0);
		}

		// Fazemos a chamada do processo.
		IListarClientesPDCAdapter listarClientesPDCAdapter = factoryAdapter.getListarClientesPDCAdapter();

		listarClientesResponse = listarClientesPDCAdapter.invokeProcess(listarClientesRequest);

		StringBuffer bcoAgeConta = null;
		// Para cada item retornado do processo, adicionamos a lista de
		// retorno.
		for (int i = 0; i < listarClientesResponse.getNumeroLinhas(); i++) {
			ListarClientesDTO listarClientesDTO = new ListarClientesDTO();

			String cpfCliente = String.valueOf(listarClientesResponse.getOcorrencias(i).getCpfCnpj());
			String cpfFilial = String.valueOf(listarClientesResponse.getOcorrencias(i).getFilial());
			String cpfControle = String.valueOf(listarClientesResponse.getOcorrencias(i).getControle());

			String cpf = "";

			if (listarClientesResponse.getOcorrencias(i).getFilial() == 0) {
				cpf = PgitUtil.formatarCPFComMascara(cpfCliente, cpfControle);
			} else {
				cpf = PgitUtil.formatarCNPJComMascara(cpfCliente, cpfFilial, cpfControle);
			}

			listarClientesDTO.setCpf(cpf);
			listarClientesDTO.setCpfCnpjCliente(cpfCliente);
			listarClientesDTO.setFilialCnpj(cpfFilial);
			listarClientesDTO.setControleCpfCnpj(cpfControle);
			listarClientesDTO.setNomeRazao(listarClientesResponse.getOcorrencias(i).getNomeRazao());
			listarClientesDTO.setDataNascimentoFundacao(listarClientesResponse.getOcorrencias(i).getDataNacscimentoFundacao());
			bcoAgeConta = new StringBuffer();
			bcoAgeConta = bcoAgeConta.append(listarClientesResponse.getOcorrencias(i).getBanco());
			bcoAgeConta = bcoAgeConta.append("/");
			bcoAgeConta = bcoAgeConta.append(listarClientesResponse.getOcorrencias(i).getAgencia());
			bcoAgeConta = bcoAgeConta.append("/");
			bcoAgeConta = bcoAgeConta.append(listarClientesResponse.getOcorrencias(i).getConta());
			listarClientesDTO.setBcoAgeConta(bcoAgeConta.toString());
			listarClientesDTO.setTotalRegistros(i);
			lListarClientes.add(listarClientesDTO);
		}

		return lListarClientes;
	}

	/* CMPI0011 */
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.arquivoretorno.IArquivoRetornoService#listaOcorrenciaRetorno(br.com.bradesco.web.pgit.service.business.arquivoretorno.bean.OcorrenciaRetornoDTO)
	 */
	public List<OcorrenciaRetornoDTO> listaOcorrenciaRetorno(OcorrenciaRetornoDTO pOcorrenciaRetornoDTO) throws PdcAdapterException {
		ConsOcorrenciaRetornoRequest lRequest = new ConsOcorrenciaRetornoRequest();
		ConsOcorrenciaRetornoResponse lResponse = null;
		List<OcorrenciaRetornoDTO> listaResultado = null;

		lRequest.setNumeroOcorrencia(27);
		lRequest.setNumeroArquivoRetorno(pOcorrenciaRetornoDTO.getNumeroArquivoRetorno());
		lRequest.setHoraInclusaoArquivoRetorno(pOcorrenciaRetornoDTO.getHoraInclusao());
		lRequest.setNumeroLoteRetorno(pOcorrenciaRetornoDTO.getNumeroLoteRetorno());

		if (pOcorrenciaRetornoDTO.getSistemaLegado() != null && !pOcorrenciaRetornoDTO.getSistemaLegado().equals("")) {
			lRequest.setCodigoSistemaLegado(pOcorrenciaRetornoDTO.getSistemaLegado());
		} else {
			lRequest.setCodigoSistemaLegado("");
		}

		if (pOcorrenciaRetornoDTO.getCpfCnpj() != null && !pOcorrenciaRetornoDTO.getCpfCnpj().equals("")) {
			String cnpjCpf = new String(pOcorrenciaRetornoDTO.getCpfCnpj());
			lRequest.setCpfCnpj(Long.parseLong(cnpjCpf));
		} else {
			lRequest.setCpfCnpj(0);
		}

		if (pOcorrenciaRetornoDTO.getFilalCnpj() != null && !pOcorrenciaRetornoDTO.getFilalCnpj().equals("")) {
			lRequest.setFilial(Integer.parseInt(pOcorrenciaRetornoDTO.getFilalCnpj()));
		} else {
			lRequest.setFilial(0);
		}

		if (pOcorrenciaRetornoDTO.getNumeroArquivoRetorno() != null && !pOcorrenciaRetornoDTO.getNumeroArquivoRetorno().equals("")) {
			lRequest.setNumeroArquivoRetorno(pOcorrenciaRetornoDTO.getNumeroArquivoRetorno());
		} else {
			lRequest.setNumeroArquivoRetorno(0);
		}

		if (pOcorrenciaRetornoDTO.getNumeroLoteRetorno() != null && !pOcorrenciaRetornoDTO.getNumeroLoteRetorno().equals("")) {
			lRequest.setNumeroLoteRetorno(pOcorrenciaRetornoDTO.getNumeroLoteRetorno());
		} else {
			lRequest.setNumeroLoteRetorno(0);
		}

		if (pOcorrenciaRetornoDTO.getNumeroSequenciaRegistroRetorno() != null && !pOcorrenciaRetornoDTO.getNumeroSequenciaRegistroRetorno().equals("")) {
			lRequest.setNumeroSequenciaRegistro(Long.parseLong(pOcorrenciaRetornoDTO.getNumeroSequenciaRegistroRetorno()));
		} else {
			lRequest.setNumeroSequenciaRegistro(0);
		}

		if (pOcorrenciaRetornoDTO.getCodigoPerfil() != null && !pOcorrenciaRetornoDTO.getCodigoPerfil().equals("")) {
			lRequest.setCodigoPerfilComunicacao(Long.parseLong(pOcorrenciaRetornoDTO.getCodigoPerfil()));
		} else {
			lRequest.setCodigoPerfilComunicacao(0);
		}

		IConsultarOcorrenciaRetornoPDCAdapter consultarOcorrenciaRetornoPDCAdapter = factoryAdapter.getConsultarOcorrenciaRetornoPDCAdapter();
		lResponse = consultarOcorrenciaRetornoPDCAdapter.invokeProcess(lRequest);
		OcorrenciaRetornoDTO ocorrenciaRetornoDTO;
		listaResultado = new ArrayList<OcorrenciaRetornoDTO>();

		for (int i = 0; i < lResponse.getNumeroLinhas(); i++) {
			ocorrenciaRetornoDTO = new OcorrenciaRetornoDTO();
			ocorrenciaRetornoDTO.setTextoMensagem(lResponse.getOcorrencia(i).getTextoMensagem());
			ocorrenciaRetornoDTO.setConteudoEnviado(lResponse.getOcorrencia(i).getConteudoEnviado());
			listaResultado.add(ocorrenciaRetornoDTO);
		}

		return listaResultado;
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.arquivoretorno.IArquivoRetornoService#listaGridInconsistentes(br.com.bradesco.web.pgit.service.business.arquivoretorno.bean.InconsistentesRetornoDTO)
	 */
	public List<InconsistentesRetornoDTO> listaGridInconsistentes(InconsistentesRetornoDTO pInconsistentesRetornoDTO) throws PdcAdapterException {
		ConsInconsistenciaRetornoRequest lRequest = new ConsInconsistenciaRetornoRequest();
		ConsInconsistenciaRetornoResponse lResponse = null;
		List<InconsistentesRetornoDTO> listaResultado = null;

		lRequest.setNumeroOcorrencia(100);
		lRequest.setNumeroLoteRetorno(pInconsistentesRetornoDTO.getNumeroLoteRetorno());
		lRequest.setHoraInclusaoArquivoRetorno(pInconsistentesRetornoDTO.getHoraInclusao());

		if (pInconsistentesRetornoDTO.getCodigoFilialCnpj() != null && !pInconsistentesRetornoDTO.getCodigoFilialCnpj().equals("")) {
			lRequest.setFilial(Integer.parseInt(pInconsistentesRetornoDTO.getCodigoFilialCnpj()));
		} else {
			lRequest.setFilial(0);
		}

		if (pInconsistentesRetornoDTO.getCodigoPerfil() != null && !pInconsistentesRetornoDTO.getCodigoPerfil().equals("")) {
			lRequest.setCodigoPerfilComunicacao(Integer.parseInt(pInconsistentesRetornoDTO.getCodigoPerfil()));
		} else {
			lRequest.setCodigoPerfilComunicacao(0);
		}

		if (pInconsistentesRetornoDTO.getCpfCnpj() != null && !pInconsistentesRetornoDTO.getCpfCnpj().equals("")) {
			lRequest.setCpfCnpj(Integer.parseInt(pInconsistentesRetornoDTO.getCpfCnpj()));
		} else {
			lRequest.setCpfCnpj(0);
		}

		if (pInconsistentesRetornoDTO.getNumeroArquivoRetorno() != null && !pInconsistentesRetornoDTO.getNumeroArquivoRetorno().equals("")) {
			lRequest.setNumeroArquivoRetorno(Integer.parseInt(pInconsistentesRetornoDTO.getNumeroArquivoRetorno()));
		} else {
			lRequest.setNumeroArquivoRetorno(0);
		}

		if (pInconsistentesRetornoDTO.getHoraInclusao() != null && !pInconsistentesRetornoDTO.getHoraInclusao().equals("")) {
			lRequest.setHoraInclusaoArquivoRetorno(pInconsistentesRetornoDTO.getHoraInclusao());
		} else {
			lRequest.setHoraInclusaoArquivoRetorno(pInconsistentesRetornoDTO.getHoraInclusao());
		}

		if (pInconsistentesRetornoDTO.getNumeroSequenciaRetorno() != null && !pInconsistentesRetornoDTO.getNumeroSequenciaRetorno().equals("")) {
			lRequest.setNumeroSequenciaRegistro(Integer.parseInt(pInconsistentesRetornoDTO.getNumeroSequenciaRetorno()));
		} else {
			lRequest.setNumeroSequenciaRegistro(0);
		}

		if (pInconsistentesRetornoDTO.getSistemaLegado() != null && !pInconsistentesRetornoDTO.getSistemaLegado().equals("")) {
			lRequest.setCodigoSistemaLegado(pInconsistentesRetornoDTO.getSistemaLegado());
		} else {
			lRequest.setCodigoSistemaLegado("");
		}

		// Fazemos a chamada do processo.
		IConsultarInconsistenciaRetornoPDCAdapter consultarInconsistenciaPDCAdapter = factoryAdapter.getConsultarInconsistenciaRetornoPDCAdapter();
		lResponse = consultarInconsistenciaPDCAdapter.invokeProcess(lRequest);

		InconsistentesRetornoDTO inconsistentesRetornoDTO;
		listaResultado = new ArrayList<InconsistentesRetornoDTO>();

		for (int i = 0; i < lResponse.getNumeroLinhas(); i++) {
			inconsistentesRetornoDTO = new InconsistentesRetornoDTO();
			inconsistentesRetornoDTO.setLinhaSelecionada(i);
			inconsistentesRetornoDTO.setNumeroSequenciaRetorno(String.valueOf(lResponse.getOcorrencia(i).getNumeroSequencia()));
			inconsistentesRetornoDTO.setControlePagamento(lResponse.getOcorrencia(i).getControlePagamento());
			listaResultado.add(inconsistentesRetornoDTO);
		}

		return listaResultado;
	}
}
