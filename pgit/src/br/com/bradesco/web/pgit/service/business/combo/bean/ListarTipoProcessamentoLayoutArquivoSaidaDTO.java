package br.com.bradesco.web.pgit.service.business.combo.bean;

import java.util.List;

/**
 * Arquivo criado em 06/11/15.
 */
public class ListarTipoProcessamentoLayoutArquivoSaidaDTO {

    private String codMensagem;

    private String mensagem;

    private Integer nrLinhas;

    private List<ListarTipoProcessamentoLayoutArquivoOcorrenciasSaidaDTO> ocorrencias;

    public void setCodMensagem(String codMensagem) {
        this.codMensagem = codMensagem;
    }

    public String getCodMensagem() {
        return this.codMensagem;
    }

    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }

    public String getMensagem() {
        return this.mensagem;
    }

    public void setNrLinhas(Integer nrLinhas) {
        this.nrLinhas = nrLinhas;
    }

    public Integer getNrLinhas() {
        return this.nrLinhas;
    }

    public void setOcorrencias(List<ListarTipoProcessamentoLayoutArquivoOcorrenciasSaidaDTO> ocorrencias) {
        this.ocorrencias = ocorrencias;
    }

    public List<ListarTipoProcessamentoLayoutArquivoOcorrenciasSaidaDTO> getOcorrencias() {
        return this.ocorrencias;
    }
}