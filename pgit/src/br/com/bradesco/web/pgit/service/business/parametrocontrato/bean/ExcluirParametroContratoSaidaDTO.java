package br.com.bradesco.web.pgit.service.business.parametrocontrato.bean;


// TODO: Auto-generated Javadoc
/**
 * Arquivo criado em 22/01/16.
 */
public class ExcluirParametroContratoSaidaDTO {

    /** The cod mensagem. */
    private String codMensagem = null;

    /** The mensagem. */
    private String mensagem = null;

    /**
     * Sets the cod mensagem.
     *
     * @param codMensagem the cod mensagem
     */
    public void setCodMensagem(String codMensagem) {
        this.codMensagem = codMensagem;
    }

    /**
     * Gets the cod mensagem.
     *
     * @return the cod mensagem
     */
    public String getCodMensagem() {
        return this.codMensagem;
    }

    /**
     * Sets the mensagem.
     *
     * @param mensagem the mensagem
     */
    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }

    /**
     * Gets the mensagem.
     *
     * @return the mensagem
     */
    public String getMensagem() {
        return this.mensagem;
    }
}