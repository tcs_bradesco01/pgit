/*
 * Nome: br.com.bradesco.web.pgit.service.business.vincmsglanctopecontratada.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.vincmsglanctopecontratada.bean;

/**
 * Nome: ListarVincMsgLancOperSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarVincMsgLancOperSaidaDTO {
	
	/** Atributo codMensagem. */
	private String codMensagem;
	
	/** Atributo mensagem. */
	private String mensagem;	
	
	/** Atributo codigoHistorico. */
	private int codigoHistorico;
	
	/** Atributo lancDebito. */
	private int lancDebito;
	
	/** Atributo lancCredito. */
	private int lancCredito;
	
	/** Atributo tipoServico. */
	private int tipoServico;
	
	/** Atributo modalidadeServico. */
	private int modalidadeServico;
	
	/** Atributo dsTipoServico. */
	private String dsTipoServico;
	
	/** Atributo dsModalidadeServico. */
	private String dsModalidadeServico;
	
	/** Atributo tipoRelacionamento. */
	private int tipoRelacionamento;
	
	/** Atributo dsLanctCredito. */
	private String dsLanctCredito;
	
	/** Atributo dsLanctDebito. */
	private String dsLanctDebito;
	
	/**
	 * Get: codigoHistorico.
	 *
	 * @return codigoHistorico
	 */
	public int getCodigoHistorico() {
		return codigoHistorico;
	}
	
	/**
	 * Set: codigoHistorico.
	 *
	 * @param codigoHistorico the codigo historico
	 */
	public void setCodigoHistorico(int codigoHistorico) {
		this.codigoHistorico = codigoHistorico;
	}
	
	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}
	
	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}
	
	/**
	 * Get: dsModalidadeServico.
	 *
	 * @return dsModalidadeServico
	 */
	public String getDsModalidadeServico() {
		return dsModalidadeServico;
	}
	
	/**
	 * Set: dsModalidadeServico.
	 *
	 * @param dsModalidadeServico the ds modalidade servico
	 */
	public void setDsModalidadeServico(String dsModalidadeServico) {
		this.dsModalidadeServico = dsModalidadeServico;
	}
	
	/**
	 * Get: dsTipoServico.
	 *
	 * @return dsTipoServico
	 */
	public String getDsTipoServico() {
		return dsTipoServico;
	}
	
	/**
	 * Set: dsTipoServico.
	 *
	 * @param dsTipoServico the ds tipo servico
	 */
	public void setDsTipoServico(String dsTipoServico) {
		this.dsTipoServico = dsTipoServico;
	}
	
	/**
	 * Get: lancCredito.
	 *
	 * @return lancCredito
	 */
	public int getLancCredito() {
		return lancCredito;
	}
	
	/**
	 * Set: lancCredito.
	 *
	 * @param lancCredito the lanc credito
	 */
	public void setLancCredito(int lancCredito) {
		this.lancCredito = lancCredito;
	}
	
	/**
	 * Get: lancDebito.
	 *
	 * @return lancDebito
	 */
	public int getLancDebito() {
		return lancDebito;
	}
	
	/**
	 * Set: lancDebito.
	 *
	 * @param lancDebito the lanc debito
	 */
	public void setLancDebito(int lancDebito) {
		this.lancDebito = lancDebito;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	/**
	 * Get: modalidadeServico.
	 *
	 * @return modalidadeServico
	 */
	public int getModalidadeServico() {
		return modalidadeServico;
	}
	
	/**
	 * Set: modalidadeServico.
	 *
	 * @param modalidadeServico the modalidade servico
	 */
	public void setModalidadeServico(int modalidadeServico) {
		this.modalidadeServico = modalidadeServico;
	}
	
	/**
	 * Get: tipoRelacionamento.
	 *
	 * @return tipoRelacionamento
	 */
	public int getTipoRelacionamento() {
		return tipoRelacionamento;
	}
	
	/**
	 * Set: tipoRelacionamento.
	 *
	 * @param tipoRelacionamento the tipo relacionamento
	 */
	public void setTipoRelacionamento(int tipoRelacionamento) {
		this.tipoRelacionamento = tipoRelacionamento;
	}
	
	/**
	 * Get: tipoServico.
	 *
	 * @return tipoServico
	 */
	public int getTipoServico() {
		return tipoServico;
	}
	
	/**
	 * Set: tipoServico.
	 *
	 * @param tipoServico the tipo servico
	 */
	public void setTipoServico(int tipoServico) {
		this.tipoServico = tipoServico;
	}
	
	/**
	 * Get: dsLanctCredito.
	 *
	 * @return dsLanctCredito
	 */
	public String getDsLanctCredito() {
		return dsLanctCredito;
	}
	
	/**
	 * Set: dsLanctCredito.
	 *
	 * @param dsLanctCredito the ds lanct credito
	 */
	public void setDsLanctCredito(String dsLanctCredito) {
		this.dsLanctCredito = dsLanctCredito;
	}
	
	/**
	 * Get: dsLanctDebito.
	 *
	 * @return dsLanctDebito
	 */
	public String getDsLanctDebito() {
		return dsLanctDebito;
	}
	
	/**
	 * Set: dsLanctDebito.
	 *
	 * @param dsLanctDebito the ds lanct debito
	 */
	public void setDsLanctDebito(String dsLanctDebito) {
		this.dsLanctDebito = dsLanctDebito;
	}
	
}

