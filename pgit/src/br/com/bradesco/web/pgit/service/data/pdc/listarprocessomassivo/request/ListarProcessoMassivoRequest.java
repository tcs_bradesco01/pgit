/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listarprocessomassivo.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ListarProcessoMassivoRequest.
 * 
 * @version $Revision$ $Date$
 */
public class ListarProcessoMassivoRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdSituacao
     */
    private java.lang.String _cdSituacao;

    /**
     * Field _cdTipoProcessoSistema
     */
    private int _cdTipoProcessoSistema = 0;

    /**
     * keeps track of state for field: _cdTipoProcessoSistema
     */
    private boolean _has_cdTipoProcessoSistema;

    /**
     * Field _cdPeriodicidade
     */
    private int _cdPeriodicidade = 0;

    /**
     * keeps track of state for field: _cdPeriodicidade
     */
    private boolean _has_cdPeriodicidade;

    /**
     * Field _qtConsultas
     */
    private int _qtConsultas = 0;

    /**
     * keeps track of state for field: _qtConsultas
     */
    private boolean _has_qtConsultas;


      //----------------/
     //- Constructors -/
    //----------------/

    public ListarProcessoMassivoRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarprocessomassivo.request.ListarProcessoMassivoRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdPeriodicidade
     * 
     */
    public void deleteCdPeriodicidade()
    {
        this._has_cdPeriodicidade= false;
    } //-- void deleteCdPeriodicidade() 

    /**
     * Method deleteCdTipoProcessoSistema
     * 
     */
    public void deleteCdTipoProcessoSistema()
    {
        this._has_cdTipoProcessoSistema= false;
    } //-- void deleteCdTipoProcessoSistema() 

    /**
     * Method deleteQtConsultas
     * 
     */
    public void deleteQtConsultas()
    {
        this._has_qtConsultas= false;
    } //-- void deleteQtConsultas() 

    /**
     * Returns the value of field 'cdPeriodicidade'.
     * 
     * @return int
     * @return the value of field 'cdPeriodicidade'.
     */
    public int getCdPeriodicidade()
    {
        return this._cdPeriodicidade;
    } //-- int getCdPeriodicidade() 

    /**
     * Returns the value of field 'cdSituacao'.
     * 
     * @return String
     * @return the value of field 'cdSituacao'.
     */
    public java.lang.String getCdSituacao()
    {
        return this._cdSituacao;
    } //-- java.lang.String getCdSituacao() 

    /**
     * Returns the value of field 'cdTipoProcessoSistema'.
     * 
     * @return int
     * @return the value of field 'cdTipoProcessoSistema'.
     */
    public int getCdTipoProcessoSistema()
    {
        return this._cdTipoProcessoSistema;
    } //-- int getCdTipoProcessoSistema() 

    /**
     * Returns the value of field 'qtConsultas'.
     * 
     * @return int
     * @return the value of field 'qtConsultas'.
     */
    public int getQtConsultas()
    {
        return this._qtConsultas;
    } //-- int getQtConsultas() 

    /**
     * Method hasCdPeriodicidade
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPeriodicidade()
    {
        return this._has_cdPeriodicidade;
    } //-- boolean hasCdPeriodicidade() 

    /**
     * Method hasCdTipoProcessoSistema
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoProcessoSistema()
    {
        return this._has_cdTipoProcessoSistema;
    } //-- boolean hasCdTipoProcessoSistema() 

    /**
     * Method hasQtConsultas
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtConsultas()
    {
        return this._has_qtConsultas;
    } //-- boolean hasQtConsultas() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdPeriodicidade'.
     * 
     * @param cdPeriodicidade the value of field 'cdPeriodicidade'.
     */
    public void setCdPeriodicidade(int cdPeriodicidade)
    {
        this._cdPeriodicidade = cdPeriodicidade;
        this._has_cdPeriodicidade = true;
    } //-- void setCdPeriodicidade(int) 

    /**
     * Sets the value of field 'cdSituacao'.
     * 
     * @param cdSituacao the value of field 'cdSituacao'.
     */
    public void setCdSituacao(java.lang.String cdSituacao)
    {
        this._cdSituacao = cdSituacao;
    } //-- void setCdSituacao(java.lang.String) 

    /**
     * Sets the value of field 'cdTipoProcessoSistema'.
     * 
     * @param cdTipoProcessoSistema the value of field
     * 'cdTipoProcessoSistema'.
     */
    public void setCdTipoProcessoSistema(int cdTipoProcessoSistema)
    {
        this._cdTipoProcessoSistema = cdTipoProcessoSistema;
        this._has_cdTipoProcessoSistema = true;
    } //-- void setCdTipoProcessoSistema(int) 

    /**
     * Sets the value of field 'qtConsultas'.
     * 
     * @param qtConsultas the value of field 'qtConsultas'.
     */
    public void setQtConsultas(int qtConsultas)
    {
        this._qtConsultas = qtConsultas;
        this._has_qtConsultas = true;
    } //-- void setQtConsultas(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ListarProcessoMassivoRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listarprocessomassivo.request.ListarProcessoMassivoRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listarprocessomassivo.request.ListarProcessoMassivoRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listarprocessomassivo.request.ListarProcessoMassivoRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarprocessomassivo.request.ListarProcessoMassivoRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
