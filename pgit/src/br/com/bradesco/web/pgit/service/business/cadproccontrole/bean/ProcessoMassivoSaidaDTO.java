/*
 * Nome: br.com.bradesco.web.pgit.service.business.cadproccontrole.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.cadproccontrole.bean;

import java.io.Serializable;

/**
 * Nome: ProcessoMassivoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ProcessoMassivoSaidaDTO implements Serializable {

	/** Atributo serialVersionUID. */
	private static final long serialVersionUID = -7691700784855565474L;

	/** Atributo codMensagem. */
	private String codMensagem;

	/** Atributo mensagem. */
	private String mensagem;

	/** Atributo cdSistema. */
	private String cdSistema;

	/** Atributo dsCodigoSistema. */
	private String dsCodigoSistema;

    /** Atributo cdProcessoSistema. */
    private Integer cdProcessoSistema;

    /** Atributo dsProcessoSistema. */
    private String dsProcessoSistema;

    /** Atributo cdTipoProcessoSistema. */
    private Integer cdTipoProcessoSistema;

    /** Atributo dsTipoProcessoSistema. */
    private String dsTipoProcessoSistema;

    /** Atributo cdPeriodicidade. */
    private Integer cdPeriodicidade;

    /** Atributo dsPeriodicidade. */
    private String dsPeriodicidade;

    /** Atributo cdSituacaoProcessoSistema. */
    private Integer cdSituacaoProcessoSistema;

    /** Atributo dsSistemaProcessoSistema. */
    private String dsSistemaProcessoSistema;

    /** Atributo cdNetProcessamentoPgto. */
    private String cdNetProcessamentoPgto;

    /** Atributo cdJobProcessamentoPgto. */
    private String cdJobProcessamentoPgto;
    
    /** Atributo cdCanalInclusao. */
    private Integer cdCanalInclusao;

    /** Atributo dsCanalInclusao. */
    private String dsCanalInclusao;

    /** Atributo cdAutenSegregacaoInclusao. */
    private String cdAutenSegregacaoInclusao;

    /** Atributo nmOperacaoFluxoInclusao. */
    private String nmOperacaoFluxoInclusao;

    /** Atributo hrInclusaoRegistro. */
    private String hrInclusaoRegistro;

    /** Atributo cdCanalManutencao. */
    private Integer cdCanalManutencao;

    /** Atributo dsCanalManutencao. */
    private String dsCanalManutencao;

    /** Atributo cdAutenSegregacaoManutencao. */
    private String cdAutenSegregacaoManutencao;

    /** Atributo nmOperacaoFluxoManutencao. */
    private String nmOperacaoFluxoManutencao;

    /** Atributo hrInclusaoManutencao. */
    private String hrInclusaoManutencao;

	/**
	 * Get: cdJobProcessamentoPgto.
	 *
	 * @return cdJobProcessamentoPgto
	 */
	public String getCdJobProcessamentoPgto() {
		return cdJobProcessamentoPgto;
	}

	/**
	 * Set: cdJobProcessamentoPgto.
	 *
	 * @param cdJobProcessamentoPgto the cd job processamento pgto
	 */
	public void setCdJobProcessamentoPgto(String cdJobProcessamentoPgto) {
		this.cdJobProcessamentoPgto = cdJobProcessamentoPgto;
	}

	/**
	 * Get: cdNetProcessamentoPgto.
	 *
	 * @return cdNetProcessamentoPgto
	 */
	public String getCdNetProcessamentoPgto() {
		return cdNetProcessamentoPgto;
	}

	/**
	 * Set: cdNetProcessamentoPgto.
	 *
	 * @param cdNetProcessamentoPgto the cd net processamento pgto
	 */
	public void setCdNetProcessamentoPgto(String cdNetProcessamentoPgto) {
		this.cdNetProcessamentoPgto = cdNetProcessamentoPgto;
	}

	/**
	 * Get: cdPeriodicidade.
	 *
	 * @return cdPeriodicidade
	 */
	public Integer getCdPeriodicidade() {
		return cdPeriodicidade;
	}

	/**
	 * Set: cdPeriodicidade.
	 *
	 * @param cdPeriodicidade the cd periodicidade
	 */
	public void setCdPeriodicidade(Integer cdPeriodicidade) {
		this.cdPeriodicidade = cdPeriodicidade;
	}

	/**
	 * Get: cdProcessoSistema.
	 *
	 * @return cdProcessoSistema
	 */
	public Integer getCdProcessoSistema() {
		return cdProcessoSistema;
	}

	/**
	 * Set: cdProcessoSistema.
	 *
	 * @param cdProcessoSistema the cd processo sistema
	 */
	public void setCdProcessoSistema(Integer cdProcessoSistema) {
		this.cdProcessoSistema = cdProcessoSistema;
	}

	/**
	 * Get: cdSistema.
	 *
	 * @return cdSistema
	 */
	public String getCdSistema() {
		return cdSistema;
	}

	/**
	 * Set: cdSistema.
	 *
	 * @param cdSistema the cd sistema
	 */
	public void setCdSistema(String cdSistema) {
		this.cdSistema = cdSistema;
	}

	/**
	 * Get: cdSituacaoProcessoSistema.
	 *
	 * @return cdSituacaoProcessoSistema
	 */
	public Integer getCdSituacaoProcessoSistema() {
		return cdSituacaoProcessoSistema;
	}

	/**
	 * Set: cdSituacaoProcessoSistema.
	 *
	 * @param cdSituacaoProcessoSistema the cd situacao processo sistema
	 */
	public void setCdSituacaoProcessoSistema(Integer cdSituacaoProcessoSistema) {
		this.cdSituacaoProcessoSistema = cdSituacaoProcessoSistema;
	}

	/**
	 * Get: cdTipoProcessoSistema.
	 *
	 * @return cdTipoProcessoSistema
	 */
	public Integer getCdTipoProcessoSistema() {
		return cdTipoProcessoSistema;
	}

	/**
	 * Set: cdTipoProcessoSistema.
	 *
	 * @param cdTipoProcessoSistema the cd tipo processo sistema
	 */
	public void setCdTipoProcessoSistema(Integer cdTipoProcessoSistema) {
		this.cdTipoProcessoSistema = cdTipoProcessoSistema;
	}

	/**
	 * Get: dsPeriodicidade.
	 *
	 * @return dsPeriodicidade
	 */
	public String getDsPeriodicidade() {
		return dsPeriodicidade;
	}

	/**
	 * Set: dsPeriodicidade.
	 *
	 * @param dsPeriodicidade the ds periodicidade
	 */
	public void setDsPeriodicidade(String dsPeriodicidade) {
		this.dsPeriodicidade = dsPeriodicidade;
	}

	/**
	 * Get: dsSistemaProcessoSistema.
	 *
	 * @return dsSistemaProcessoSistema
	 */
	public String getDsSistemaProcessoSistema() {
		return dsSistemaProcessoSistema;
	}

	/**
	 * Set: dsSistemaProcessoSistema.
	 *
	 * @param dsSistemaProcessoSistema the ds sistema processo sistema
	 */
	public void setDsSistemaProcessoSistema(String dsSistemaProcessoSistema) {
		this.dsSistemaProcessoSistema = dsSistemaProcessoSistema;
	}

	/**
	 * Get: dsTipoProcessoSistema.
	 *
	 * @return dsTipoProcessoSistema
	 */
	public String getDsTipoProcessoSistema() {
		return dsTipoProcessoSistema;
	}

	/**
	 * Set: dsTipoProcessoSistema.
	 *
	 * @param dsTipoProcessoSistema the ds tipo processo sistema
	 */
	public void setDsTipoProcessoSistema(String dsTipoProcessoSistema) {
		this.dsTipoProcessoSistema = dsTipoProcessoSistema;
	}

	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}

	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}

	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}

	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	/**
	 * Get: cdAutenSegregacaoInclusao.
	 *
	 * @return cdAutenSegregacaoInclusao
	 */
	public String getCdAutenSegregacaoInclusao() {
		return cdAutenSegregacaoInclusao;
	}

	/**
	 * Set: cdAutenSegregacaoInclusao.
	 *
	 * @param cdAutenSegregacaoInclusao the cd auten segregacao inclusao
	 */
	public void setCdAutenSegregacaoInclusao(String cdAutenSegregacaoInclusao) {
		this.cdAutenSegregacaoInclusao = cdAutenSegregacaoInclusao;
	}

	/**
	 * Get: cdAutenSegregacaoManutencao.
	 *
	 * @return cdAutenSegregacaoManutencao
	 */
	public String getCdAutenSegregacaoManutencao() {
		return cdAutenSegregacaoManutencao;
	}

	/**
	 * Set: cdAutenSegregacaoManutencao.
	 *
	 * @param cdAutenSegregacaoManutencao the cd auten segregacao manutencao
	 */
	public void setCdAutenSegregacaoManutencao(String cdAutenSegregacaoManutencao) {
		this.cdAutenSegregacaoManutencao = cdAutenSegregacaoManutencao;
	}

	/**
	 * Get: cdCanalInclusao.
	 *
	 * @return cdCanalInclusao
	 */
	public Integer getCdCanalInclusao() {
		return cdCanalInclusao;
	}

	/**
	 * Set: cdCanalInclusao.
	 *
	 * @param cdCanalInclusao the cd canal inclusao
	 */
	public void setCdCanalInclusao(Integer cdCanalInclusao) {
		this.cdCanalInclusao = cdCanalInclusao;
	}

	/**
	 * Get: cdCanalManutencao.
	 *
	 * @return cdCanalManutencao
	 */
	public Integer getCdCanalManutencao() {
		return cdCanalManutencao;
	}

	/**
	 * Set: cdCanalManutencao.
	 *
	 * @param cdCanalManutencao the cd canal manutencao
	 */
	public void setCdCanalManutencao(Integer cdCanalManutencao) {
		this.cdCanalManutencao = cdCanalManutencao;
	}

	/**
	 * Get: dsCanalInclusao.
	 *
	 * @return dsCanalInclusao
	 */
	public String getDsCanalInclusao() {
		return dsCanalInclusao;
	}

	/**
	 * Set: dsCanalInclusao.
	 *
	 * @param dsCanalInclusao the ds canal inclusao
	 */
	public void setDsCanalInclusao(String dsCanalInclusao) {
		this.dsCanalInclusao = dsCanalInclusao;
	}

	/**
	 * Get: dsCanalManutencao.
	 *
	 * @return dsCanalManutencao
	 */
	public String getDsCanalManutencao() {
		return dsCanalManutencao;
	}

	/**
	 * Set: dsCanalManutencao.
	 *
	 * @param dsCanalManutencao the ds canal manutencao
	 */
	public void setDsCanalManutencao(String dsCanalManutencao) {
		this.dsCanalManutencao = dsCanalManutencao;
	}

	/**
	 * Get: dsCodigoSistema.
	 *
	 * @return dsCodigoSistema
	 */
	public String getDsCodigoSistema() {
		return dsCodigoSistema;
	}

	/**
	 * Set: dsCodigoSistema.
	 *
	 * @param dsCodigoSistema the ds codigo sistema
	 */
	public void setDsCodigoSistema(String dsCodigoSistema) {
		this.dsCodigoSistema = dsCodigoSistema;
	}

	/**
	 * Get: dsProcessoSistema.
	 *
	 * @return dsProcessoSistema
	 */
	public String getDsProcessoSistema() {
		return dsProcessoSistema;
	}

	/**
	 * Set: dsProcessoSistema.
	 *
	 * @param dsProcessoSistema the ds processo sistema
	 */
	public void setDsProcessoSistema(String dsProcessoSistema) {
		this.dsProcessoSistema = dsProcessoSistema;
	}

	/**
	 * Get: hrInclusaoManutencao.
	 *
	 * @return hrInclusaoManutencao
	 */
	public String getHrInclusaoManutencao() {
		return hrInclusaoManutencao;
	}

	/**
	 * Set: hrInclusaoManutencao.
	 *
	 * @param hrInclusaoManutencao the hr inclusao manutencao
	 */
	public void setHrInclusaoManutencao(String hrInclusaoManutencao) {
		this.hrInclusaoManutencao = hrInclusaoManutencao;
	}

	/**
	 * Get: hrInclusaoRegistro.
	 *
	 * @return hrInclusaoRegistro
	 */
	public String getHrInclusaoRegistro() {
		return hrInclusaoRegistro;
	}

	/**
	 * Set: hrInclusaoRegistro.
	 *
	 * @param hrInclusaoRegistro the hr inclusao registro
	 */
	public void setHrInclusaoRegistro(String hrInclusaoRegistro) {
		this.hrInclusaoRegistro = hrInclusaoRegistro;
	}

	/**
	 * Get: nmOperacaoFluxoInclusao.
	 *
	 * @return nmOperacaoFluxoInclusao
	 */
	public String getNmOperacaoFluxoInclusao() {
		return nmOperacaoFluxoInclusao;
	}

	/**
	 * Set: nmOperacaoFluxoInclusao.
	 *
	 * @param nmOperacaoFluxoInclusao the nm operacao fluxo inclusao
	 */
	public void setNmOperacaoFluxoInclusao(String nmOperacaoFluxoInclusao) {
		this.nmOperacaoFluxoInclusao = nmOperacaoFluxoInclusao;
	}

	/**
	 * Get: nmOperacaoFluxoManutencao.
	 *
	 * @return nmOperacaoFluxoManutencao
	 */
	public String getNmOperacaoFluxoManutencao() {
		return nmOperacaoFluxoManutencao;
	}

	/**
	 * Set: nmOperacaoFluxoManutencao.
	 *
	 * @param nmOperacaoFluxoManutencao the nm operacao fluxo manutencao
	 */
	public void setNmOperacaoFluxoManutencao(String nmOperacaoFluxoManutencao) {
		this.nmOperacaoFluxoManutencao = nmOperacaoFluxoManutencao;
	}

	/**
	 * Get: canalInclusao.
	 *
	 * @return canalInclusao
	 */
	public String getCanalInclusao() {
		if (getCdCanalInclusao() != null && getCdCanalInclusao() != 0) {
			return getCdCanalInclusao() + " - " + getDsCanalInclusao();
		}
		return "";
	}
	
	/**
	 * Get: canalManutencao.
	 *
	 * @return canalManutencao
	 */
	public String getCanalManutencao() {
		if (getCdCanalManutencao() != null && getCdCanalManutencao() != 0) {
			return getCdCanalManutencao() + " - " + getDsCanalManutencao();
		}
		return "";
	}

	/**
	 * (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof ProcessoMassivoSaidaDTO)) {
			return false;
		}

		ProcessoMassivoSaidaDTO other = (ProcessoMassivoSaidaDTO) obj;
		return this.getCdSistema().equals(other.getCdSistema())
			&& this.getCdProcessoSistema().equals(other.getCdProcessoSistema());
	}

	/**
	 * (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		int hashCode = 7;
		hashCode = hashCode + getCdSistema().hashCode();
		hashCode = hashCode + getCdProcessoSistema().hashCode();
		return hashCode;
	}
}
