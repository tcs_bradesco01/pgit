/**
 * 
 */
package br.com.bradesco.web.pgit.service.provider.intranet;

import br.com.bradesco.web.pgit.service.provider.intranet.exception.AlterarSenhaException;

/**
 * @author edwin.costa
 * 
 */
public interface IAlterarSenhaService {
	
	/**
	 * Trocar senha.
	 *
	 * @param usuario the usuario
	 * @param senhaAtual the senha atual
	 * @param novaSenha the nova senha
	 * @param confirmNovaSenha the confirm nova senha
	 * @throws AlterarSenhaException the alterar senha exception
	 */
	void trocarSenha(String usuario, String senhaAtual,
			String novaSenha, String confirmNovaSenha)
			throws AlterarSenhaException;
}
