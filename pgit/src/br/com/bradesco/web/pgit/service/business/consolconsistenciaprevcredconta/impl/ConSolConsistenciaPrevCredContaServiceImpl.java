/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/implementation.ftl,v $
 * $Id: implementation.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: implementation.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */
 
package br.com.bradesco.web.pgit.service.business.consolconsistenciaprevcredconta.impl;

import java.util.ArrayList;
import java.util.List;

import br.com.bradesco.web.pgit.service.business.consolconsistenciaprevcredconta.IConSolConsistenciaPrevCredContaService;
import br.com.bradesco.web.pgit.service.business.consolconsistenciaprevcredconta.IConSolConsistenciaPrevCredContaServiceConstants;
import br.com.bradesco.web.pgit.service.business.consolconsistenciaprevcredconta.bean.AprovarSolicitacaoConsistPreviaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consolconsistenciaprevcredconta.bean.AprovarSolicitacaoConsistPreviaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consolconsistenciaprevcredconta.bean.ConsultarSolicitacaoConsistPreviaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consolconsistenciaprevcredconta.bean.ConsultarSolicitacaoConsistPreviaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consolconsistenciaprevcredconta.bean.RejeitarSolicitacaoConsistPreviaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consolconsistenciaprevcredconta.bean.RejeitarSolicitacaoConsistPreviaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consolconsistenciaprevcredconta.bean.ReprocessarSolicitacaoConsistPreviaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consolconsistenciaprevcredconta.bean.ReprocessarSolicitacaoConsistPreviaSaidaDTO;
import br.com.bradesco.web.pgit.service.data.pdc.FactoryAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.aprovarsolicitacaoconsistprevia.request.AprovarSolicitacaoConsistPreviaRequest;
import br.com.bradesco.web.pgit.service.data.pdc.aprovarsolicitacaoconsistprevia.response.AprovarSolicitacaoConsistPreviaResponse;
import br.com.bradesco.web.pgit.service.data.pdc.consultarsolicitacaoconsistprevia.request.ConsultarSolicitacaoConsistPreviaRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarsolicitacaoconsistprevia.response.ConsultarSolicitacaoConsistPreviaResponse;
import br.com.bradesco.web.pgit.service.data.pdc.rejeitarsolicitacaoconsistprevia.request.RejeitarSolicitacaoConsistPreviaRequest;
import br.com.bradesco.web.pgit.service.data.pdc.rejeitarsolicitacaoconsistprevia.response.RejeitarSolicitacaoConsistPreviaResponse;
import br.com.bradesco.web.pgit.service.data.pdc.reprocessarsolicitacaoconsistprevia.request.ReprocessarSolicitacaoConsistPreviaRequest;
import br.com.bradesco.web.pgit.service.data.pdc.reprocessarsolicitacaoconsistprevia.response.ReprocessarSolicitacaoConsistPreviaResponse;
import br.com.bradesco.web.pgit.utils.DateUtils;
import br.com.bradesco.web.pgit.utils.PgitUtil;

/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Implementa��o do adaptador: ConSolConsistenciaPrevCredConta
 * </p>
 * 
 * @comment C�DIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public class ConSolConsistenciaPrevCredContaServiceImpl implements IConSolConsistenciaPrevCredContaService {
	
	/** Atributo factoryAdapter. */
	private FactoryAdapter factoryAdapter;
	
    /**
     * Get: factoryAdapter.
     *
     * @return factoryAdapter
     */
    public FactoryAdapter getFactoryAdapter() {
		return factoryAdapter;
	}

	/**
	 * Set: factoryAdapter.
	 *
	 * @param factoryAdapter the factory adapter
	 */
	public void setFactoryAdapter(FactoryAdapter factoryAdapter) {
		this.factoryAdapter = factoryAdapter;
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.consolconsistenciaprevcredconta.IConSolConsistenciaPrevCredContaService#aprovarSolicitacaoConsistPrevia(br.com.bradesco.web.pgit.service.business.consolconsistenciaprevcredconta.bean.AprovarSolicitacaoConsistPreviaEntradaDTO)
	 */
	public AprovarSolicitacaoConsistPreviaSaidaDTO aprovarSolicitacaoConsistPrevia(AprovarSolicitacaoConsistPreviaEntradaDTO aprovarSolicitacaoConsistPreviaEntradaDTO) {
		
		AprovarSolicitacaoConsistPreviaSaidaDTO aprovarSolicitacaoConsistPreviaSaidaDTO = new AprovarSolicitacaoConsistPreviaSaidaDTO();
		AprovarSolicitacaoConsistPreviaRequest request = new AprovarSolicitacaoConsistPreviaRequest();
		AprovarSolicitacaoConsistPreviaResponse response = new AprovarSolicitacaoConsistPreviaResponse();
		
		request.setCdConsistenciaPreviaPagamento(PgitUtil.verificaIntegerNulo(aprovarSolicitacaoConsistPreviaEntradaDTO.getCdConsistenciaPreviaPagamento()));
		request.setCdPessoaJuridicaContrato(PgitUtil.verificaLongNulo(aprovarSolicitacaoConsistPreviaEntradaDTO.getCdPessoaJuridicaContrato()));
		request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(aprovarSolicitacaoConsistPreviaEntradaDTO.getCdTipoContratoNegocio()));
		request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(aprovarSolicitacaoConsistPreviaEntradaDTO.getNrSequenciaContratoNegocio()));
		
		
		response = getFactoryAdapter().getAprovarSolicitacaoConsistPreviaPDCAdapter().invokeProcess(request);
		
		aprovarSolicitacaoConsistPreviaSaidaDTO.setCdMensagem(response.getCodMensagem());
		aprovarSolicitacaoConsistPreviaSaidaDTO.setMensagem(response.getMensagem());
		
		return aprovarSolicitacaoConsistPreviaSaidaDTO;
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.consolconsistenciaprevcredconta.IConSolConsistenciaPrevCredContaService#rejeitarSolicitacaoConsistPrevia(br.com.bradesco.web.pgit.service.business.consolconsistenciaprevcredconta.bean.RejeitarSolicitacaoConsistPreviaEntradaDTO)
	 */
	public RejeitarSolicitacaoConsistPreviaSaidaDTO rejeitarSolicitacaoConsistPrevia(RejeitarSolicitacaoConsistPreviaEntradaDTO rejeitarSolicitacaoConsistPreviaEntradaDTO) {
		
		RejeitarSolicitacaoConsistPreviaSaidaDTO rejeitarSolicitacaoConsistPreviaSaidaDTO = new RejeitarSolicitacaoConsistPreviaSaidaDTO();
		RejeitarSolicitacaoConsistPreviaRequest request = new RejeitarSolicitacaoConsistPreviaRequest();
		RejeitarSolicitacaoConsistPreviaResponse response = new RejeitarSolicitacaoConsistPreviaResponse();
		
		request.setCdConsistenciaPreviaPagamento(PgitUtil.verificaIntegerNulo(rejeitarSolicitacaoConsistPreviaEntradaDTO.getCdConsistenciaPreviaPagamento()));
		request.setCdPessoaJuridicaContrato(PgitUtil.verificaLongNulo(rejeitarSolicitacaoConsistPreviaEntradaDTO.getCdPessoaJuridicaContrato()));
		request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(rejeitarSolicitacaoConsistPreviaEntradaDTO.getCdTipoContratoNegocio()));
		request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(rejeitarSolicitacaoConsistPreviaEntradaDTO.getNrSequenciaContratoNegocio()));
		
		
		response = getFactoryAdapter().getRejeitarSolicitacaoConsistPreviaPDCAdapter().invokeProcess(request);
		
		rejeitarSolicitacaoConsistPreviaSaidaDTO.setCdMensagem(response.getCodMensagem());
		rejeitarSolicitacaoConsistPreviaSaidaDTO.setMensagem(response.getMensagem());
		
		return rejeitarSolicitacaoConsistPreviaSaidaDTO;
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.consolconsistenciaprevcredconta.IConSolConsistenciaPrevCredContaService#reprocessarSolicitacaoConsistPrevia(br.com.bradesco.web.pgit.service.business.consolconsistenciaprevcredconta.bean.ReprocessarSolicitacaoConsistPreviaEntradaDTO)
	 */
	public ReprocessarSolicitacaoConsistPreviaSaidaDTO reprocessarSolicitacaoConsistPrevia(ReprocessarSolicitacaoConsistPreviaEntradaDTO reprocessarSolicitacaoConsistPreviaEntradaDTO) {
		
		ReprocessarSolicitacaoConsistPreviaSaidaDTO reprocessarSolicitacaoConsistPreviaSaidaDTO = new ReprocessarSolicitacaoConsistPreviaSaidaDTO();
		ReprocessarSolicitacaoConsistPreviaRequest request = new ReprocessarSolicitacaoConsistPreviaRequest();
		ReprocessarSolicitacaoConsistPreviaResponse response = new ReprocessarSolicitacaoConsistPreviaResponse();
		
		request.setCdConsistenciaPreviaPagamento(PgitUtil.verificaIntegerNulo(reprocessarSolicitacaoConsistPreviaEntradaDTO.getCdConsistenciaPreviaPagamento()));
		request.setCdPessoaJuridicaContrato(PgitUtil.verificaLongNulo(reprocessarSolicitacaoConsistPreviaEntradaDTO.getCdPessoaJuridicaContrato()));
		request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(reprocessarSolicitacaoConsistPreviaEntradaDTO.getCdTipoContratoNegocio()));
		request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(reprocessarSolicitacaoConsistPreviaEntradaDTO.getNrSequenciaContratoNegocio()));
		
		
		response = getFactoryAdapter().getReprocessarSolicitacaoConsistPreviaPDCAdapter().invokeProcess(request);
		
		reprocessarSolicitacaoConsistPreviaSaidaDTO.setCdMensagem(response.getCodMensagem());
		reprocessarSolicitacaoConsistPreviaSaidaDTO.setMensagem(response.getMensagem());
		
		return reprocessarSolicitacaoConsistPreviaSaidaDTO;
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.consolconsistenciaprevcredconta.IConSolConsistenciaPrevCredContaService#consultarSolicitacaoConsistPrevia(br.com.bradesco.web.pgit.service.business.consolconsistenciaprevcredconta.bean.ConsultarSolicitacaoConsistPreviaEntradaDTO)
	 */
	public List<ConsultarSolicitacaoConsistPreviaSaidaDTO> consultarSolicitacaoConsistPrevia(ConsultarSolicitacaoConsistPreviaEntradaDTO consultarSolicitacaoConsistPreviaEntradaDTO) {
		List<ConsultarSolicitacaoConsistPreviaSaidaDTO> lista = new ArrayList<ConsultarSolicitacaoConsistPreviaSaidaDTO>();			
    	ConsultarSolicitacaoConsistPreviaRequest request = new ConsultarSolicitacaoConsistPreviaRequest(); 
    	ConsultarSolicitacaoConsistPreviaResponse response = new ConsultarSolicitacaoConsistPreviaResponse();
    	
    	request.setCdAgenciaDebito(PgitUtil.verificaIntegerNulo(consultarSolicitacaoConsistPreviaEntradaDTO.getCdAgenciaDebito()));
    	request.setCdBancoDebito(PgitUtil.verificaIntegerNulo(consultarSolicitacaoConsistPreviaEntradaDTO.getCdBancoDebito()));
    	request.setCdContaDebito(PgitUtil.verificaLongNulo(consultarSolicitacaoConsistPreviaEntradaDTO.getCdContaDebito()));
    	request.setCdPessoaJuridicaContrato(PgitUtil.verificaLongNulo(consultarSolicitacaoConsistPreviaEntradaDTO.getCdPessoaJuridicaContrato()));
    	request.setCdSituacaoLotePagamento(PgitUtil.verificaIntegerNulo(consultarSolicitacaoConsistPreviaEntradaDTO.getCdSituacaoLotePagamento()));
    	request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(consultarSolicitacaoConsistPreviaEntradaDTO.getCdTipoContratoNegocio()));
    	request.setDigitoContaDebito(PgitUtil.DIGITO_CONTA);
    	request.setDtCreditoPagamentoFinal(PgitUtil.verificaStringNula(consultarSolicitacaoConsistPreviaEntradaDTO.getDtCreditoPagamentoFinal()));
    	request.setDtCreditoPagamentoInicial(PgitUtil.verificaStringNula(consultarSolicitacaoConsistPreviaEntradaDTO.getDtCreditoPagamentoInicial()));
    	request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(consultarSolicitacaoConsistPreviaEntradaDTO.getNrSequenciaContratoNegocio()));
    	request.setNrOcorrencias(IConSolConsistenciaPrevCredContaServiceConstants.NUMERO_OCORRENCIAS_CONSULTAR);
    	
    	ConsultarSolicitacaoConsistPreviaSaidaDTO saida;
    	
    	response = getFactoryAdapter().getConsultarSolicitacaoConsistPreviaPDCAdapter().invokeProcess(request);
    	
		for (int i=0; i<response.getOcorrenciasCount();i++){
			saida = new ConsultarSolicitacaoConsistPreviaSaidaDTO();
			
			
	    	saida.setCdConsistenciaPreviaPagamento(response.getOcorrencias(i).getCdConsistenciaPreviaPagamento());
	    	saida.setCdControleTransacao(response.getOcorrencias(i).getCdControleTransacao());
    		saida.setCdInscricaoPagador(response.getOcorrencias(i).getCdInscricaoPagador());
	    	saida.setCdPessoaJuridicaContrato(response.getOcorrencias(i).getCdPessoaJuridicaContrato());
	    	saida.setCdSituacaoLotePagamento(response.getOcorrencias(i).getCdSituacaoLotePagamento());
	    	saida.setCdTipoContratoNegocio(response.getOcorrencias(i).getCdTipoContratoNegocio());
	    	saida.setCdTipoLote(response.getOcorrencias(i).getCdTipoLote());
	    	saida.setCdVolumeTotal(response.getOcorrencias(i).getCdVolumeTotal());
	    	saida.setDsRazaoSocial(response.getOcorrencias(i).getDsRazaoSocial());
	    	saida.setNrLoteArquivoRemessa(response.getOcorrencias(i).getNrLoteArquivoRemessa());
	    	saida.setNrSequenciaContratoNegocio(response.getOcorrencias(i).getNrSequenciaContratoNegocio());
	    	saida.setNumeroConsultas(response.getNumeroConsultas());
	    	saida.setCodMensagem(response.getCodMensagem());
	    	saida.setMensagem(response.getMensagem());
	    	saida.setVlTotal(response.getOcorrencias(i).getVlTotal());
	    	saida.setCdSituacaoSolicitacao(response.getOcorrencias(i).getCdSituacaoSolicitacao());
	    	saida.setDsSituacaoSolicitacao(response.getOcorrencias(i).getDsSituacaoSolicitacao());
	    	saida.setDsSituacaoLotePagamento(response.getOcorrencias(i).getDsSituacaoLotePagamento());
	    	saida.setDtGravacaoLote(DateUtils.trocarSeparadorDeData(response.getOcorrencias(i).getDtGravacaoLote(),".","/"));
			saida.setDtProcessoLote(DateUtils.trocarSeparadorDeData(response.getOcorrencias(i).getDtProcessoLote(),".","/"));
			saida.setHrInclusaoPagamento(DateUtils.formartarDataPDCparaPadraPtBr(response.getOcorrencias(i).getHrInclusaoPagamento(), "yyyy-MM-dd","dd/MM/yyyy"));
			
	    	lista.add(saida);
         }
		
		return lista;
	}
    
}

