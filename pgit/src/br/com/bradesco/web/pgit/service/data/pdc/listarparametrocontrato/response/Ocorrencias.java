/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listarparametrocontrato.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdParametro
     */
    private int _cdParametro = 0;

    /**
     * keeps track of state for field: _cdParametro
     */
    private boolean _has_cdParametro;

    /**
     * Field _dsParametro
     */
    private java.lang.String _dsParametro;

    /**
     * Field _dsParametroContrato
     */
    private java.lang.String _dsParametroContrato;

    /**
     * Field _cdUsuarioInclusao
     */
    private java.lang.String _cdUsuarioInclusao;

    /**
     * Field _hrInclusaoRegistro
     */
    private java.lang.String _hrInclusaoRegistro;

    /**
     * Field _cdTipoCanal
     */
    private int _cdTipoCanal = 0;

    /**
     * keeps track of state for field: _cdTipoCanal
     */
    private boolean _has_cdTipoCanal;

    /**
     * Field _dsTipoCanal
     */
    private java.lang.String _dsTipoCanal;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarparametrocontrato.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdParametro
     * 
     */
    public void deleteCdParametro()
    {
        this._has_cdParametro= false;
    } //-- void deleteCdParametro() 

    /**
     * Method deleteCdTipoCanal
     * 
     */
    public void deleteCdTipoCanal()
    {
        this._has_cdTipoCanal= false;
    } //-- void deleteCdTipoCanal() 

    /**
     * Returns the value of field 'cdParametro'.
     * 
     * @return int
     * @return the value of field 'cdParametro'.
     */
    public int getCdParametro()
    {
        return this._cdParametro;
    } //-- int getCdParametro() 

    /**
     * Returns the value of field 'cdTipoCanal'.
     * 
     * @return int
     * @return the value of field 'cdTipoCanal'.
     */
    public int getCdTipoCanal()
    {
        return this._cdTipoCanal;
    } //-- int getCdTipoCanal() 

    /**
     * Returns the value of field 'cdUsuarioInclusao'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioInclusao'.
     */
    public java.lang.String getCdUsuarioInclusao()
    {
        return this._cdUsuarioInclusao;
    } //-- java.lang.String getCdUsuarioInclusao() 

    /**
     * Returns the value of field 'dsParametro'.
     * 
     * @return String
     * @return the value of field 'dsParametro'.
     */
    public java.lang.String getDsParametro()
    {
        return this._dsParametro;
    } //-- java.lang.String getDsParametro() 

    /**
     * Returns the value of field 'dsParametroContrato'.
     * 
     * @return String
     * @return the value of field 'dsParametroContrato'.
     */
    public java.lang.String getDsParametroContrato()
    {
        return this._dsParametroContrato;
    } //-- java.lang.String getDsParametroContrato() 

    /**
     * Returns the value of field 'dsTipoCanal'.
     * 
     * @return String
     * @return the value of field 'dsTipoCanal'.
     */
    public java.lang.String getDsTipoCanal()
    {
        return this._dsTipoCanal;
    } //-- java.lang.String getDsTipoCanal() 

    /**
     * Returns the value of field 'hrInclusaoRegistro'.
     * 
     * @return String
     * @return the value of field 'hrInclusaoRegistro'.
     */
    public java.lang.String getHrInclusaoRegistro()
    {
        return this._hrInclusaoRegistro;
    } //-- java.lang.String getHrInclusaoRegistro() 

    /**
     * Method hasCdParametro
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdParametro()
    {
        return this._has_cdParametro;
    } //-- boolean hasCdParametro() 

    /**
     * Method hasCdTipoCanal
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoCanal()
    {
        return this._has_cdTipoCanal;
    } //-- boolean hasCdTipoCanal() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdParametro'.
     * 
     * @param cdParametro the value of field 'cdParametro'.
     */
    public void setCdParametro(int cdParametro)
    {
        this._cdParametro = cdParametro;
        this._has_cdParametro = true;
    } //-- void setCdParametro(int) 

    /**
     * Sets the value of field 'cdTipoCanal'.
     * 
     * @param cdTipoCanal the value of field 'cdTipoCanal'.
     */
    public void setCdTipoCanal(int cdTipoCanal)
    {
        this._cdTipoCanal = cdTipoCanal;
        this._has_cdTipoCanal = true;
    } //-- void setCdTipoCanal(int) 

    /**
     * Sets the value of field 'cdUsuarioInclusao'.
     * 
     * @param cdUsuarioInclusao the value of field
     * 'cdUsuarioInclusao'.
     */
    public void setCdUsuarioInclusao(java.lang.String cdUsuarioInclusao)
    {
        this._cdUsuarioInclusao = cdUsuarioInclusao;
    } //-- void setCdUsuarioInclusao(java.lang.String) 

    /**
     * Sets the value of field 'dsParametro'.
     * 
     * @param dsParametro the value of field 'dsParametro'.
     */
    public void setDsParametro(java.lang.String dsParametro)
    {
        this._dsParametro = dsParametro;
    } //-- void setDsParametro(java.lang.String) 

    /**
     * Sets the value of field 'dsParametroContrato'.
     * 
     * @param dsParametroContrato the value of field
     * 'dsParametroContrato'.
     */
    public void setDsParametroContrato(java.lang.String dsParametroContrato)
    {
        this._dsParametroContrato = dsParametroContrato;
    } //-- void setDsParametroContrato(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoCanal'.
     * 
     * @param dsTipoCanal the value of field 'dsTipoCanal'.
     */
    public void setDsTipoCanal(java.lang.String dsTipoCanal)
    {
        this._dsTipoCanal = dsTipoCanal;
    } //-- void setDsTipoCanal(java.lang.String) 

    /**
     * Sets the value of field 'hrInclusaoRegistro'.
     * 
     * @param hrInclusaoRegistro the value of field
     * 'hrInclusaoRegistro'.
     */
    public void setHrInclusaoRegistro(java.lang.String hrInclusaoRegistro)
    {
        this._hrInclusaoRegistro = hrInclusaoRegistro;
    } //-- void setHrInclusaoRegistro(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listarparametrocontrato.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listarparametrocontrato.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listarparametrocontrato.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarparametrocontrato.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
