/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultarestatisticaslote.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _instrucaoMovimento
     */
    private int _instrucaoMovimento = 0;

    /**
     * keeps track of state for field: _instrucaoMovimento
     */
    private boolean _has_instrucaoMovimento;

    /**
     * Field _descricaoMovimento
     */
    private java.lang.String _descricaoMovimento;

    /**
     * Field _quantidadeRegistroConsistente
     */
    private long _quantidadeRegistroConsistente = 0;

    /**
     * keeps track of state for field: _quantidadeRegistroConsistent
     */
    private boolean _has_quantidadeRegistroConsistente;

    /**
     * Field _valorRegistroConsistente
     */
    private double _valorRegistroConsistente = 0;

    /**
     * keeps track of state for field: _valorRegistroConsistente
     */
    private boolean _has_valorRegistroConsistente;

    /**
     * Field _quantidadeRegistroIncosistente
     */
    private long _quantidadeRegistroIncosistente = 0;

    /**
     * keeps track of state for field:
     * _quantidadeRegistroIncosistente
     */
    private boolean _has_quantidadeRegistroIncosistente;

    /**
     * Field _valorRegistroInconsistente
     */
    private double _valorRegistroInconsistente = 0;

    /**
     * keeps track of state for field: _valorRegistroInconsistente
     */
    private boolean _has_valorRegistroInconsistente;

    /**
     * Field _quantidadeRegistros
     */
    private long _quantidadeRegistros = 0;

    /**
     * keeps track of state for field: _quantidadeRegistros
     */
    private boolean _has_quantidadeRegistros;

    /**
     * Field _valorRegistros
     */
    private double _valorRegistros = 0;

    /**
     * keeps track of state for field: _valorRegistros
     */
    private boolean _has_valorRegistros;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarestatisticaslote.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteInstrucaoMovimento
     * 
     */
    public void deleteInstrucaoMovimento()
    {
        this._has_instrucaoMovimento= false;
    } //-- void deleteInstrucaoMovimento() 

    /**
     * Method deleteQuantidadeRegistroConsistente
     * 
     */
    public void deleteQuantidadeRegistroConsistente()
    {
        this._has_quantidadeRegistroConsistente= false;
    } //-- void deleteQuantidadeRegistroConsistente() 

    /**
     * Method deleteQuantidadeRegistroIncosistente
     * 
     */
    public void deleteQuantidadeRegistroIncosistente()
    {
        this._has_quantidadeRegistroIncosistente= false;
    } //-- void deleteQuantidadeRegistroIncosistente() 

    /**
     * Method deleteQuantidadeRegistros
     * 
     */
    public void deleteQuantidadeRegistros()
    {
        this._has_quantidadeRegistros= false;
    } //-- void deleteQuantidadeRegistros() 

    /**
     * Method deleteValorRegistroConsistente
     * 
     */
    public void deleteValorRegistroConsistente()
    {
        this._has_valorRegistroConsistente= false;
    } //-- void deleteValorRegistroConsistente() 

    /**
     * Method deleteValorRegistroInconsistente
     * 
     */
    public void deleteValorRegistroInconsistente()
    {
        this._has_valorRegistroInconsistente= false;
    } //-- void deleteValorRegistroInconsistente() 

    /**
     * Method deleteValorRegistros
     * 
     */
    public void deleteValorRegistros()
    {
        this._has_valorRegistros= false;
    } //-- void deleteValorRegistros() 

    /**
     * Returns the value of field 'descricaoMovimento'.
     * 
     * @return String
     * @return the value of field 'descricaoMovimento'.
     */
    public java.lang.String getDescricaoMovimento()
    {
        return this._descricaoMovimento;
    } //-- java.lang.String getDescricaoMovimento() 

    /**
     * Returns the value of field 'instrucaoMovimento'.
     * 
     * @return int
     * @return the value of field 'instrucaoMovimento'.
     */
    public int getInstrucaoMovimento()
    {
        return this._instrucaoMovimento;
    } //-- int getInstrucaoMovimento() 

    /**
     * Returns the value of field 'quantidadeRegistroConsistente'.
     * 
     * @return long
     * @return the value of field 'quantidadeRegistroConsistente'.
     */
    public long getQuantidadeRegistroConsistente()
    {
        return this._quantidadeRegistroConsistente;
    } //-- long getQuantidadeRegistroConsistente() 

    /**
     * Returns the value of field 'quantidadeRegistroIncosistente'.
     * 
     * @return long
     * @return the value of field 'quantidadeRegistroIncosistente'.
     */
    public long getQuantidadeRegistroIncosistente()
    {
        return this._quantidadeRegistroIncosistente;
    } //-- long getQuantidadeRegistroIncosistente() 

    /**
     * Returns the value of field 'quantidadeRegistros'.
     * 
     * @return long
     * @return the value of field 'quantidadeRegistros'.
     */
    public long getQuantidadeRegistros()
    {
        return this._quantidadeRegistros;
    } //-- long getQuantidadeRegistros() 

    /**
     * Returns the value of field 'valorRegistroConsistente'.
     * 
     * @return double
     * @return the value of field 'valorRegistroConsistente'.
     */
    public double getValorRegistroConsistente()
    {
        return this._valorRegistroConsistente;
    } //-- double getValorRegistroConsistente() 

    /**
     * Returns the value of field 'valorRegistroInconsistente'.
     * 
     * @return double
     * @return the value of field 'valorRegistroInconsistente'.
     */
    public double getValorRegistroInconsistente()
    {
        return this._valorRegistroInconsistente;
    } //-- double getValorRegistroInconsistente() 

    /**
     * Returns the value of field 'valorRegistros'.
     * 
     * @return double
     * @return the value of field 'valorRegistros'.
     */
    public double getValorRegistros()
    {
        return this._valorRegistros;
    } //-- double getValorRegistros() 

    /**
     * Method hasInstrucaoMovimento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasInstrucaoMovimento()
    {
        return this._has_instrucaoMovimento;
    } //-- boolean hasInstrucaoMovimento() 

    /**
     * Method hasQuantidadeRegistroConsistente
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQuantidadeRegistroConsistente()
    {
        return this._has_quantidadeRegistroConsistente;
    } //-- boolean hasQuantidadeRegistroConsistente() 

    /**
     * Method hasQuantidadeRegistroIncosistente
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQuantidadeRegistroIncosistente()
    {
        return this._has_quantidadeRegistroIncosistente;
    } //-- boolean hasQuantidadeRegistroIncosistente() 

    /**
     * Method hasQuantidadeRegistros
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQuantidadeRegistros()
    {
        return this._has_quantidadeRegistros;
    } //-- boolean hasQuantidadeRegistros() 

    /**
     * Method hasValorRegistroConsistente
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasValorRegistroConsistente()
    {
        return this._has_valorRegistroConsistente;
    } //-- boolean hasValorRegistroConsistente() 

    /**
     * Method hasValorRegistroInconsistente
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasValorRegistroInconsistente()
    {
        return this._has_valorRegistroInconsistente;
    } //-- boolean hasValorRegistroInconsistente() 

    /**
     * Method hasValorRegistros
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasValorRegistros()
    {
        return this._has_valorRegistros;
    } //-- boolean hasValorRegistros() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'descricaoMovimento'.
     * 
     * @param descricaoMovimento the value of field
     * 'descricaoMovimento'.
     */
    public void setDescricaoMovimento(java.lang.String descricaoMovimento)
    {
        this._descricaoMovimento = descricaoMovimento;
    } //-- void setDescricaoMovimento(java.lang.String) 

    /**
     * Sets the value of field 'instrucaoMovimento'.
     * 
     * @param instrucaoMovimento the value of field
     * 'instrucaoMovimento'.
     */
    public void setInstrucaoMovimento(int instrucaoMovimento)
    {
        this._instrucaoMovimento = instrucaoMovimento;
        this._has_instrucaoMovimento = true;
    } //-- void setInstrucaoMovimento(int) 

    /**
     * Sets the value of field 'quantidadeRegistroConsistente'.
     * 
     * @param quantidadeRegistroConsistente the value of field
     * 'quantidadeRegistroConsistente'.
     */
    public void setQuantidadeRegistroConsistente(long quantidadeRegistroConsistente)
    {
        this._quantidadeRegistroConsistente = quantidadeRegistroConsistente;
        this._has_quantidadeRegistroConsistente = true;
    } //-- void setQuantidadeRegistroConsistente(long) 

    /**
     * Sets the value of field 'quantidadeRegistroIncosistente'.
     * 
     * @param quantidadeRegistroIncosistente the value of field
     * 'quantidadeRegistroIncosistente'.
     */
    public void setQuantidadeRegistroIncosistente(long quantidadeRegistroIncosistente)
    {
        this._quantidadeRegistroIncosistente = quantidadeRegistroIncosistente;
        this._has_quantidadeRegistroIncosistente = true;
    } //-- void setQuantidadeRegistroIncosistente(long) 

    /**
     * Sets the value of field 'quantidadeRegistros'.
     * 
     * @param quantidadeRegistros the value of field
     * 'quantidadeRegistros'.
     */
    public void setQuantidadeRegistros(long quantidadeRegistros)
    {
        this._quantidadeRegistros = quantidadeRegistros;
        this._has_quantidadeRegistros = true;
    } //-- void setQuantidadeRegistros(long) 

    /**
     * Sets the value of field 'valorRegistroConsistente'.
     * 
     * @param valorRegistroConsistente the value of field
     * 'valorRegistroConsistente'.
     */
    public void setValorRegistroConsistente(double valorRegistroConsistente)
    {
        this._valorRegistroConsistente = valorRegistroConsistente;
        this._has_valorRegistroConsistente = true;
    } //-- void setValorRegistroConsistente(double) 

    /**
     * Sets the value of field 'valorRegistroInconsistente'.
     * 
     * @param valorRegistroInconsistente the value of field
     * 'valorRegistroInconsistente'.
     */
    public void setValorRegistroInconsistente(double valorRegistroInconsistente)
    {
        this._valorRegistroInconsistente = valorRegistroInconsistente;
        this._has_valorRegistroInconsistente = true;
    } //-- void setValorRegistroInconsistente(double) 

    /**
     * Sets the value of field 'valorRegistros'.
     * 
     * @param valorRegistros the value of field 'valorRegistros'.
     */
    public void setValorRegistros(double valorRegistros)
    {
        this._valorRegistros = valorRegistros;
        this._has_valorRegistros = true;
    } //-- void setValorRegistros(double) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultarestatisticaslote.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultarestatisticaslote.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultarestatisticaslote.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarestatisticaslote.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
