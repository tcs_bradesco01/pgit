/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultarmodalidade.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdProdutoOperacaoRelacionada
     */
    private int _cdProdutoOperacaoRelacionada = 0;

    /**
     * keeps track of state for field: _cdProdutoOperacaoRelacionada
     */
    private boolean _has_cdProdutoOperacaoRelacionada;

    /**
     * Field _dsProdutoOperacaoRelacionada
     */
    private java.lang.String _dsProdutoOperacaoRelacionada;

    /**
     * Field _cdRelacionamentoProdutoProduto
     */
    private int _cdRelacionamentoProdutoProduto = 0;

    /**
     * keeps track of state for field:
     * _cdRelacionamentoProdutoProduto
     */
    private boolean _has_cdRelacionamentoProdutoProduto;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarmodalidade.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdProdutoOperacaoRelacionada
     * 
     */
    public void deleteCdProdutoOperacaoRelacionada()
    {
        this._has_cdProdutoOperacaoRelacionada= false;
    } //-- void deleteCdProdutoOperacaoRelacionada() 

    /**
     * Method deleteCdRelacionamentoProdutoProduto
     * 
     */
    public void deleteCdRelacionamentoProdutoProduto()
    {
        this._has_cdRelacionamentoProdutoProduto= false;
    } //-- void deleteCdRelacionamentoProdutoProduto() 

    /**
     * Returns the value of field 'cdProdutoOperacaoRelacionada'.
     * 
     * @return int
     * @return the value of field 'cdProdutoOperacaoRelacionada'.
     */
    public int getCdProdutoOperacaoRelacionada()
    {
        return this._cdProdutoOperacaoRelacionada;
    } //-- int getCdProdutoOperacaoRelacionada() 

    /**
     * Returns the value of field 'cdRelacionamentoProdutoProduto'.
     * 
     * @return int
     * @return the value of field 'cdRelacionamentoProdutoProduto'.
     */
    public int getCdRelacionamentoProdutoProduto()
    {
        return this._cdRelacionamentoProdutoProduto;
    } //-- int getCdRelacionamentoProdutoProduto() 

    /**
     * Returns the value of field 'dsProdutoOperacaoRelacionada'.
     * 
     * @return String
     * @return the value of field 'dsProdutoOperacaoRelacionada'.
     */
    public java.lang.String getDsProdutoOperacaoRelacionada()
    {
        return this._dsProdutoOperacaoRelacionada;
    } //-- java.lang.String getDsProdutoOperacaoRelacionada() 

    /**
     * Method hasCdProdutoOperacaoRelacionada
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdutoOperacaoRelacionada()
    {
        return this._has_cdProdutoOperacaoRelacionada;
    } //-- boolean hasCdProdutoOperacaoRelacionada() 

    /**
     * Method hasCdRelacionamentoProdutoProduto
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdRelacionamentoProdutoProduto()
    {
        return this._has_cdRelacionamentoProdutoProduto;
    } //-- boolean hasCdRelacionamentoProdutoProduto() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdProdutoOperacaoRelacionada'.
     * 
     * @param cdProdutoOperacaoRelacionada the value of field
     * 'cdProdutoOperacaoRelacionada'.
     */
    public void setCdProdutoOperacaoRelacionada(int cdProdutoOperacaoRelacionada)
    {
        this._cdProdutoOperacaoRelacionada = cdProdutoOperacaoRelacionada;
        this._has_cdProdutoOperacaoRelacionada = true;
    } //-- void setCdProdutoOperacaoRelacionada(int) 

    /**
     * Sets the value of field 'cdRelacionamentoProdutoProduto'.
     * 
     * @param cdRelacionamentoProdutoProduto the value of field
     * 'cdRelacionamentoProdutoProduto'.
     */
    public void setCdRelacionamentoProdutoProduto(int cdRelacionamentoProdutoProduto)
    {
        this._cdRelacionamentoProdutoProduto = cdRelacionamentoProdutoProduto;
        this._has_cdRelacionamentoProdutoProduto = true;
    } //-- void setCdRelacionamentoProdutoProduto(int) 

    /**
     * Sets the value of field 'dsProdutoOperacaoRelacionada'.
     * 
     * @param dsProdutoOperacaoRelacionada the value of field
     * 'dsProdutoOperacaoRelacionada'.
     */
    public void setDsProdutoOperacaoRelacionada(java.lang.String dsProdutoOperacaoRelacionada)
    {
        this._dsProdutoOperacaoRelacionada = dsProdutoOperacaoRelacionada;
    } //-- void setDsProdutoOperacaoRelacionada(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultarmodalidade.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultarmodalidade.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultarmodalidade.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarmodalidade.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
