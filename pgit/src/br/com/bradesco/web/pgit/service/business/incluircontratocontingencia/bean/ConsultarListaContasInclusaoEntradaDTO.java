/*
 * Nome: br.com.bradesco.web.pgit.service.business.incluircontratocontingencia.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.incluircontratocontingencia.bean;

/**
 * Nome: ConsultarListaContasInclusaoEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarListaContasInclusaoEntradaDTO {
   
    /** Atributo cdPessoaJuridicaContrato. */
    private Long cdPessoaJuridicaContrato;
    
    /** Atributo cdTipoContratoNegocio. */
    private Integer cdTipoContratoNegocio;
    
    /** Atributo nrSequenciaContratoNegocio. */
    private Long nrSequenciaContratoNegocio;
    
    /** Atributo cdCpfCnpj. */
    private Long cdCpfCnpj;
    
    /** Atributo cdFilialCpfCnpj. */
    private Integer cdFilialCpfCnpj;
    
    /** Atributo cdControleCpfCnpj. */
    private Integer cdControleCpfCnpj;
    
    /** Atributo cdBanco. */
    private Integer cdBanco;
    
    /** Atributo cdAgencia. */
    private Integer cdAgencia;
    
    /** Atributo cdDigitoAgencia. */
    private Integer cdDigitoAgencia;
    
    /** Atributo cdConta. */
    private Long cdConta;
    
    /** Atributo cdDigitoConta. */
    private String cdDigitoConta;
    
    /** Atributo cdTipoConta. */
    private Integer cdTipoConta;  
    
    /** Atributo dsTipoConta. */
    private Integer dsTipoConta;  
    
	/**
	 * Get: cdAgencia.
	 *
	 * @return cdAgencia
	 */
	public Integer getCdAgencia() {
		return cdAgencia;
	}
	
	/**
	 * Set: cdAgencia.
	 *
	 * @param cdAgencia the cd agencia
	 */
	public void setCdAgencia(Integer cdAgencia) {
		this.cdAgencia = cdAgencia;
	}
	
	/**
	 * Get: cdBanco.
	 *
	 * @return cdBanco
	 */
	public Integer getCdBanco() {
		return cdBanco;
	}
	
	/**
	 * Set: cdBanco.
	 *
	 * @param cdBanco the cd banco
	 */
	public void setCdBanco(Integer cdBanco) {
		this.cdBanco = cdBanco;
	}
	
	/**
	 * Get: cdConta.
	 *
	 * @return cdConta
	 */
	public Long getCdConta() {
		return cdConta;
	}
	
	/**
	 * Set: cdConta.
	 *
	 * @param cdConta the cd conta
	 */
	public void setCdConta(Long cdConta) {
		this.cdConta = cdConta;
	}
	
	/**
	 * Get: cdControleCpfCnpj.
	 *
	 * @return cdControleCpfCnpj
	 */
	public Integer getCdControleCpfCnpj() {
		return cdControleCpfCnpj;
	}
	
	/**
	 * Set: cdControleCpfCnpj.
	 *
	 * @param cdControleCpfCnpj the cd controle cpf cnpj
	 */
	public void setCdControleCpfCnpj(Integer cdControleCpfCnpj) {
		this.cdControleCpfCnpj = cdControleCpfCnpj;
	}
	
	/**
	 * Get: cdCpfCnpj.
	 *
	 * @return cdCpfCnpj
	 */
	public Long getCdCpfCnpj() {
		return cdCpfCnpj;
	}
	
	/**
	 * Set: cdCpfCnpj.
	 *
	 * @param cdCpfCnpj the cd cpf cnpj
	 */
	public void setCdCpfCnpj(Long cdCpfCnpj) {
		this.cdCpfCnpj = cdCpfCnpj;
	}
	
	/**
	 * Get: cdDigitoAgencia.
	 *
	 * @return cdDigitoAgencia
	 */
	public Integer getCdDigitoAgencia() {
		return cdDigitoAgencia;
	}
	
	/**
	 * Set: cdDigitoAgencia.
	 *
	 * @param cdDigitoAgencia the cd digito agencia
	 */
	public void setCdDigitoAgencia(Integer cdDigitoAgencia) {
		this.cdDigitoAgencia = cdDigitoAgencia;
	}
	
	/**
	 * Get: cdDigitoConta.
	 *
	 * @return cdDigitoConta
	 */
	public String getCdDigitoConta() {
		return cdDigitoConta;
	}
	
	/**
	 * Set: cdDigitoConta.
	 *
	 * @param cdDigitoConta the cd digito conta
	 */
	public void setCdDigitoConta(String cdDigitoConta) {
		this.cdDigitoConta = cdDigitoConta;
	}
	
	/**
	 * Get: cdFilialCpfCnpj.
	 *
	 * @return cdFilialCpfCnpj
	 */
	public Integer getCdFilialCpfCnpj() {
		return cdFilialCpfCnpj;
	}
	
	/**
	 * Set: cdFilialCpfCnpj.
	 *
	 * @param cdFilialCpfCnpj the cd filial cpf cnpj
	 */
	public void setCdFilialCpfCnpj(Integer cdFilialCpfCnpj) {
		this.cdFilialCpfCnpj = cdFilialCpfCnpj;
	}
	
	/**
	 * Get: cdPessoaJuridicaContrato.
	 *
	 * @return cdPessoaJuridicaContrato
	 */
	public Long getCdPessoaJuridicaContrato() {
		return cdPessoaJuridicaContrato;
	}
	
	/**
	 * Set: cdPessoaJuridicaContrato.
	 *
	 * @param cdPessoaJuridicaContrato the cd pessoa juridica contrato
	 */
	public void setCdPessoaJuridicaContrato(Long cdPessoaJuridicaContrato) {
		this.cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
	}
	
	/**
	 * Get: cdTipoConta.
	 *
	 * @return cdTipoConta
	 */
	public Integer getCdTipoConta() {
		return cdTipoConta;
	}
	
	/**
	 * Set: cdTipoConta.
	 *
	 * @param cdTipoConta the cd tipo conta
	 */
	public void setCdTipoConta(Integer cdTipoConta) {
		this.cdTipoConta = cdTipoConta;
	}
	
	/**
	 * Get: cdTipoContratoNegocio.
	 *
	 * @return cdTipoContratoNegocio
	 */
	public Integer getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}
	
	/**
	 * Set: cdTipoContratoNegocio.
	 *
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}
	
	/**
	 * Get: nrSequenciaContratoNegocio.
	 *
	 * @return nrSequenciaContratoNegocio
	 */
	public Long getNrSequenciaContratoNegocio() {
		return nrSequenciaContratoNegocio;
	}
	
	/**
	 * Set: nrSequenciaContratoNegocio.
	 *
	 * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
	 */
	public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}

	public Integer getDsTipoConta() {
		return dsTipoConta;
	}

	public void setDsTipoConta(Integer dsTipoConta) {
		this.dsTipoConta = dsTipoConta;
	}
	
}
