package br.com.bradesco.web.pgit.service.business.vincmsglanctopecontratada.bean;

public class ListarTiposPagtoEntradaDTO {

	/** Atributo maxOcorrencias. */
	private Integer maxOcorrencias;

	public Integer getMaxOcorrencias() {
		return maxOcorrencias;
	}

	public void setMaxOcorrencias(Integer maxOcorrencias) {
		this.maxOcorrencias = maxOcorrencias;
	}

}
