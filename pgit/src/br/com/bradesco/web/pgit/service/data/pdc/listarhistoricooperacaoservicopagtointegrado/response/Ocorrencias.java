/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listarhistoricooperacaoservicopagtointegrado.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdProdutoServicoOperacional
     */
    private int _cdProdutoServicoOperacional = 0;

    /**
     * keeps track of state for field: _cdProdutoServicoOperacional
     */
    private boolean _has_cdProdutoServicoOperacional;

    /**
     * Field _dsProdutoServicoOperacao
     */
    private java.lang.String _dsProdutoServicoOperacao;

    /**
     * Field _cdProdutoOperacionalRelacionado
     */
    private int _cdProdutoOperacionalRelacionado = 0;

    /**
     * keeps track of state for field:
     * _cdProdutoOperacionalRelacionado
     */
    private boolean _has_cdProdutoOperacionalRelacionado;

    /**
     * Field _dsProdutoOperacaoRelacionado
     */
    private java.lang.String _dsProdutoOperacaoRelacionado;

    /**
     * Field _cdOperacaoProdutoServico
     */
    private int _cdOperacaoProdutoServico = 0;

    /**
     * keeps track of state for field: _cdOperacaoProdutoServico
     */
    private boolean _has_cdOperacaoProdutoServico;

    /**
     * Field _dsOperacaoProdutoServico
     */
    private java.lang.String _dsOperacaoProdutoServico;

    /**
     * Field _hrInclusaoRegistroHistorico
     */
    private java.lang.String _hrInclusaoRegistroHistorico;

    /**
     * Field _cdUsuarioManutencao
     */
    private java.lang.String _cdUsuarioManutencao;

    /**
     * Field _cdUsuarioManutencaoExterno
     */
    private java.lang.String _cdUsuarioManutencaoExterno;

    /**
     * Field _cdIndicadorTipoManutencao
     */
    private int _cdIndicadorTipoManutencao = 0;

    /**
     * keeps track of state for field: _cdIndicadorTipoManutencao
     */
    private boolean _has_cdIndicadorTipoManutencao;

    /**
     * Field _dsIndicadorTipoManutencao
     */
    private java.lang.String _dsIndicadorTipoManutencao;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarhistoricooperacaoservicopagtointegrado.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdIndicadorTipoManutencao
     * 
     */
    public void deleteCdIndicadorTipoManutencao()
    {
        this._has_cdIndicadorTipoManutencao= false;
    } //-- void deleteCdIndicadorTipoManutencao() 

    /**
     * Method deleteCdOperacaoProdutoServico
     * 
     */
    public void deleteCdOperacaoProdutoServico()
    {
        this._has_cdOperacaoProdutoServico= false;
    } //-- void deleteCdOperacaoProdutoServico() 

    /**
     * Method deleteCdProdutoOperacionalRelacionado
     * 
     */
    public void deleteCdProdutoOperacionalRelacionado()
    {
        this._has_cdProdutoOperacionalRelacionado= false;
    } //-- void deleteCdProdutoOperacionalRelacionado() 

    /**
     * Method deleteCdProdutoServicoOperacional
     * 
     */
    public void deleteCdProdutoServicoOperacional()
    {
        this._has_cdProdutoServicoOperacional= false;
    } //-- void deleteCdProdutoServicoOperacional() 

    /**
     * Returns the value of field 'cdIndicadorTipoManutencao'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorTipoManutencao'.
     */
    public int getCdIndicadorTipoManutencao()
    {
        return this._cdIndicadorTipoManutencao;
    } //-- int getCdIndicadorTipoManutencao() 

    /**
     * Returns the value of field 'cdOperacaoProdutoServico'.
     * 
     * @return int
     * @return the value of field 'cdOperacaoProdutoServico'.
     */
    public int getCdOperacaoProdutoServico()
    {
        return this._cdOperacaoProdutoServico;
    } //-- int getCdOperacaoProdutoServico() 

    /**
     * Returns the value of field
     * 'cdProdutoOperacionalRelacionado'.
     * 
     * @return int
     * @return the value of field 'cdProdutoOperacionalRelacionado'.
     */
    public int getCdProdutoOperacionalRelacionado()
    {
        return this._cdProdutoOperacionalRelacionado;
    } //-- int getCdProdutoOperacionalRelacionado() 

    /**
     * Returns the value of field 'cdProdutoServicoOperacional'.
     * 
     * @return int
     * @return the value of field 'cdProdutoServicoOperacional'.
     */
    public int getCdProdutoServicoOperacional()
    {
        return this._cdProdutoServicoOperacional;
    } //-- int getCdProdutoServicoOperacional() 

    /**
     * Returns the value of field 'cdUsuarioManutencao'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioManutencao'.
     */
    public java.lang.String getCdUsuarioManutencao()
    {
        return this._cdUsuarioManutencao;
    } //-- java.lang.String getCdUsuarioManutencao() 

    /**
     * Returns the value of field 'cdUsuarioManutencaoExterno'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioManutencaoExterno'.
     */
    public java.lang.String getCdUsuarioManutencaoExterno()
    {
        return this._cdUsuarioManutencaoExterno;
    } //-- java.lang.String getCdUsuarioManutencaoExterno() 

    /**
     * Returns the value of field 'dsIndicadorTipoManutencao'.
     * 
     * @return String
     * @return the value of field 'dsIndicadorTipoManutencao'.
     */
    public java.lang.String getDsIndicadorTipoManutencao()
    {
        return this._dsIndicadorTipoManutencao;
    } //-- java.lang.String getDsIndicadorTipoManutencao() 

    /**
     * Returns the value of field 'dsOperacaoProdutoServico'.
     * 
     * @return String
     * @return the value of field 'dsOperacaoProdutoServico'.
     */
    public java.lang.String getDsOperacaoProdutoServico()
    {
        return this._dsOperacaoProdutoServico;
    } //-- java.lang.String getDsOperacaoProdutoServico() 

    /**
     * Returns the value of field 'dsProdutoOperacaoRelacionado'.
     * 
     * @return String
     * @return the value of field 'dsProdutoOperacaoRelacionado'.
     */
    public java.lang.String getDsProdutoOperacaoRelacionado()
    {
        return this._dsProdutoOperacaoRelacionado;
    } //-- java.lang.String getDsProdutoOperacaoRelacionado() 

    /**
     * Returns the value of field 'dsProdutoServicoOperacao'.
     * 
     * @return String
     * @return the value of field 'dsProdutoServicoOperacao'.
     */
    public java.lang.String getDsProdutoServicoOperacao()
    {
        return this._dsProdutoServicoOperacao;
    } //-- java.lang.String getDsProdutoServicoOperacao() 

    /**
     * Returns the value of field 'hrInclusaoRegistroHistorico'.
     * 
     * @return String
     * @return the value of field 'hrInclusaoRegistroHistorico'.
     */
    public java.lang.String getHrInclusaoRegistroHistorico()
    {
        return this._hrInclusaoRegistroHistorico;
    } //-- java.lang.String getHrInclusaoRegistroHistorico() 

    /**
     * Method hasCdIndicadorTipoManutencao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorTipoManutencao()
    {
        return this._has_cdIndicadorTipoManutencao;
    } //-- boolean hasCdIndicadorTipoManutencao() 

    /**
     * Method hasCdOperacaoProdutoServico
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdOperacaoProdutoServico()
    {
        return this._has_cdOperacaoProdutoServico;
    } //-- boolean hasCdOperacaoProdutoServico() 

    /**
     * Method hasCdProdutoOperacionalRelacionado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdutoOperacionalRelacionado()
    {
        return this._has_cdProdutoOperacionalRelacionado;
    } //-- boolean hasCdProdutoOperacionalRelacionado() 

    /**
     * Method hasCdProdutoServicoOperacional
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdutoServicoOperacional()
    {
        return this._has_cdProdutoServicoOperacional;
    } //-- boolean hasCdProdutoServicoOperacional() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdIndicadorTipoManutencao'.
     * 
     * @param cdIndicadorTipoManutencao the value of field
     * 'cdIndicadorTipoManutencao'.
     */
    public void setCdIndicadorTipoManutencao(int cdIndicadorTipoManutencao)
    {
        this._cdIndicadorTipoManutencao = cdIndicadorTipoManutencao;
        this._has_cdIndicadorTipoManutencao = true;
    } //-- void setCdIndicadorTipoManutencao(int) 

    /**
     * Sets the value of field 'cdOperacaoProdutoServico'.
     * 
     * @param cdOperacaoProdutoServico the value of field
     * 'cdOperacaoProdutoServico'.
     */
    public void setCdOperacaoProdutoServico(int cdOperacaoProdutoServico)
    {
        this._cdOperacaoProdutoServico = cdOperacaoProdutoServico;
        this._has_cdOperacaoProdutoServico = true;
    } //-- void setCdOperacaoProdutoServico(int) 

    /**
     * Sets the value of field 'cdProdutoOperacionalRelacionado'.
     * 
     * @param cdProdutoOperacionalRelacionado the value of field
     * 'cdProdutoOperacionalRelacionado'.
     */
    public void setCdProdutoOperacionalRelacionado(int cdProdutoOperacionalRelacionado)
    {
        this._cdProdutoOperacionalRelacionado = cdProdutoOperacionalRelacionado;
        this._has_cdProdutoOperacionalRelacionado = true;
    } //-- void setCdProdutoOperacionalRelacionado(int) 

    /**
     * Sets the value of field 'cdProdutoServicoOperacional'.
     * 
     * @param cdProdutoServicoOperacional the value of field
     * 'cdProdutoServicoOperacional'.
     */
    public void setCdProdutoServicoOperacional(int cdProdutoServicoOperacional)
    {
        this._cdProdutoServicoOperacional = cdProdutoServicoOperacional;
        this._has_cdProdutoServicoOperacional = true;
    } //-- void setCdProdutoServicoOperacional(int) 

    /**
     * Sets the value of field 'cdUsuarioManutencao'.
     * 
     * @param cdUsuarioManutencao the value of field
     * 'cdUsuarioManutencao'.
     */
    public void setCdUsuarioManutencao(java.lang.String cdUsuarioManutencao)
    {
        this._cdUsuarioManutencao = cdUsuarioManutencao;
    } //-- void setCdUsuarioManutencao(java.lang.String) 

    /**
     * Sets the value of field 'cdUsuarioManutencaoExterno'.
     * 
     * @param cdUsuarioManutencaoExterno the value of field
     * 'cdUsuarioManutencaoExterno'.
     */
    public void setCdUsuarioManutencaoExterno(java.lang.String cdUsuarioManutencaoExterno)
    {
        this._cdUsuarioManutencaoExterno = cdUsuarioManutencaoExterno;
    } //-- void setCdUsuarioManutencaoExterno(java.lang.String) 

    /**
     * Sets the value of field 'dsIndicadorTipoManutencao'.
     * 
     * @param dsIndicadorTipoManutencao the value of field
     * 'dsIndicadorTipoManutencao'.
     */
    public void setDsIndicadorTipoManutencao(java.lang.String dsIndicadorTipoManutencao)
    {
        this._dsIndicadorTipoManutencao = dsIndicadorTipoManutencao;
    } //-- void setDsIndicadorTipoManutencao(java.lang.String) 

    /**
     * Sets the value of field 'dsOperacaoProdutoServico'.
     * 
     * @param dsOperacaoProdutoServico the value of field
     * 'dsOperacaoProdutoServico'.
     */
    public void setDsOperacaoProdutoServico(java.lang.String dsOperacaoProdutoServico)
    {
        this._dsOperacaoProdutoServico = dsOperacaoProdutoServico;
    } //-- void setDsOperacaoProdutoServico(java.lang.String) 

    /**
     * Sets the value of field 'dsProdutoOperacaoRelacionado'.
     * 
     * @param dsProdutoOperacaoRelacionado the value of field
     * 'dsProdutoOperacaoRelacionado'.
     */
    public void setDsProdutoOperacaoRelacionado(java.lang.String dsProdutoOperacaoRelacionado)
    {
        this._dsProdutoOperacaoRelacionado = dsProdutoOperacaoRelacionado;
    } //-- void setDsProdutoOperacaoRelacionado(java.lang.String) 

    /**
     * Sets the value of field 'dsProdutoServicoOperacao'.
     * 
     * @param dsProdutoServicoOperacao the value of field
     * 'dsProdutoServicoOperacao'.
     */
    public void setDsProdutoServicoOperacao(java.lang.String dsProdutoServicoOperacao)
    {
        this._dsProdutoServicoOperacao = dsProdutoServicoOperacao;
    } //-- void setDsProdutoServicoOperacao(java.lang.String) 

    /**
     * Sets the value of field 'hrInclusaoRegistroHistorico'.
     * 
     * @param hrInclusaoRegistroHistorico the value of field
     * 'hrInclusaoRegistroHistorico'.
     */
    public void setHrInclusaoRegistroHistorico(java.lang.String hrInclusaoRegistroHistorico)
    {
        this._hrInclusaoRegistroHistorico = hrInclusaoRegistroHistorico;
    } //-- void setHrInclusaoRegistroHistorico(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listarhistoricooperacaoservicopagtointegrado.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listarhistoricooperacaoservicopagtointegrado.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listarhistoricooperacaoservicopagtointegrado.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarhistoricooperacaoservicopagtointegrado.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
