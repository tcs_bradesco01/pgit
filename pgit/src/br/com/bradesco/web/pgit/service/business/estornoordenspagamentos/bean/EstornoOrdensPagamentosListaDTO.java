/*
 * Nome: br.com.bradesco.web.pgit.service.business.estornoordenspagamentos.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.estornoordenspagamentos.bean;

import java.math.BigDecimal;

import br.com.bradesco.web.pgit.utils.CpfCnpjUtils;
import br.com.bradesco.web.pgit.utils.NumberUtils;
import br.com.bradesco.web.pgit.utils.PgitUtil;
import br.com.bradesco.web.pgit.utils.SiteUtil;
import br.com.bradesco.web.pgit.view.converters.FormatarData;

/**
 * Nome: EstornoOrdensPagamentosListaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class EstornoOrdensPagamentosListaDTO {

	/** Atributo nrCnpjCpf. */
	private Long nrCnpjCpf;
	
	/** Atributo nrFilialCnpjCpf. */
	private Integer nrFilialCnpjCpf;
	
	/** Atributo nrDigitoCnpjCpf. */
	private Integer nrDigitoCnpjCpf;
	
	/** Atributo cdPessoaJuridicaContrato. */
	private Long cdPessoaJuridicaContrato;
	
	/** Atributo dsRazaoSocial. */
	private String dsRazaoSocial;
	
	/** Atributo cdEmpresa. */
	private Long cdEmpresa;
	
	/** Atributo dsEmpresa. */
	private String dsEmpresa;
	
	/** Atributo cdTipoContratoNegocio. */
	private Integer cdTipoContratoNegocio;
	
	/** Atributo nrContrato. */
	private Long nrContrato;
	
	/** Atributo cdProdutoServicoOperacao. */
	private Integer cdProdutoServicoOperacao;
	
	/** Atributo dsResumoProdutoServico. */
	private String dsResumoProdutoServico;
	
	/** Atributo cdProdutoServicoRelacionado. */
	private Integer cdProdutoServicoRelacionado;
	
	/** Atributo dsOperacaoProdutoServico. */
	private String dsOperacaoProdutoServico;
	
	/** Atributo cdControlePagamento. */
	private String cdControlePagamento;
	
	/** Atributo dtCreditoPagamento. */
	private String dtCreditoPagamento;
	
	/** Atributo vlEfetivoPagamento. */
	private BigDecimal vlEfetivoPagamento;
	
	/** Atributo cdInscricaoFavorecido. */
	private Long cdInscricaoFavorecido;
	
	/** Atributo dsFavorecido. */
	private String dsFavorecido;
	
	/** Atributo cdBancoDebito. */
	private Integer cdBancoDebito;
	
	/** Atributo cdAgenciaDebito. */
	private Integer cdAgenciaDebito;
	
	/** Atributo cdDigitoAgenciaDebito. */
	private Integer cdDigitoAgenciaDebito;
	
	/** Atributo cdContaDebito. */
	private Long cdContaDebito;
	
	/** Atributo cdDigitoContaDebito. */
	private String cdDigitoContaDebito;
	
	/** Atributo cdBancoCredito. */
	private Integer cdBancoCredito;
	
	/** Atributo cdAgenciaCredito. */
	private Integer cdAgenciaCredito;
	
	/** Atributo cdDigitoAgenciaCredito. */
	private Integer cdDigitoAgenciaCredito;
	
	/** Atributo cdContaCredito. */
	private Long cdContaCredito;
	
	/** Atributo cdDigitoContaCredito. */
	private String cdDigitoContaCredito;
	
	/** Atributo cdSituacaoOperacaoPagamento. */
	private Integer cdSituacaoOperacaoPagamento;
	
	/** Atributo dsSituacaoOperacaoPagamento. */
	private String dsSituacaoOperacaoPagamento;
	
	/** Atributo cdMotivoSituacaoPagamento. */
	private Integer cdMotivoSituacaoPagamento;
	
	/** Atributo dsMotivoSituacaoPagamento. */
	private String dsMotivoSituacaoPagamento;
	
	/** Atributo cdTipoCanal. */
	private Integer cdTipoCanal;
	
	/** Atributo cdTipoTela. */
	private Integer cdTipoTela;
	
	/** Atributo dtEfetivacao. */
	private String dtEfetivacao;
	
	/** Atributo cdCpfCnpjRecebedor. */
	private Long cdCpfCnpjRecebedor;
	
	/** Atributo cdFilialRecebedor. */
	private Integer cdFilialRecebedor;
	
	/** Atributo cdDigitoCpfRecebedor. */
	private Integer cdDigitoCpfRecebedor;
	
	/** Atributo dsCododigoTabelaConsulta. */
	private Integer dsCododigoTabelaConsulta;
	
	/** Atributo nrArquivoRemessa. */
	private Long nrArquivoRemessa;
	
	/** Atributo check. */
	private boolean check = false;

	/** Atributo cdFormaLiquidacao. */
	private Integer cdFormaLiquidacao;
	
	/**
	 * Get: nrCnpjCpf.
	 *
	 * @return nrCnpjCpf
	 */
	public Long getNrCnpjCpf() {
		return nrCnpjCpf;
	}

	public Integer getCdFormaLiquidacao() {
		return cdFormaLiquidacao;
	}

	public void setCdFormaLiquidacao(Integer cdFormaLiquidacao) {
		this.cdFormaLiquidacao = cdFormaLiquidacao;
	}

	/**
	 * Set: nrCnpjCpf.
	 *
	 * @param nrCnpjCpf the nr cnpj cpf
	 */
	public void setNrCnpjCpf(Long nrCnpjCpf) {
		this.nrCnpjCpf = nrCnpjCpf;
	}

	/**
	 * Get: nrFilialCnpjCpf.
	 *
	 * @return nrFilialCnpjCpf
	 */
	public Integer getNrFilialCnpjCpf() {
		return nrFilialCnpjCpf;
	}

	/**
	 * Set: nrFilialCnpjCpf.
	 *
	 * @param nrFilialCnpjCpf the nr filial cnpj cpf
	 */
	public void setNrFilialCnpjCpf(Integer nrFilialCnpjCpf) {
		this.nrFilialCnpjCpf = nrFilialCnpjCpf;
	}

	/**
	 * Get: nrDigitoCnpjCpf.
	 *
	 * @return nrDigitoCnpjCpf
	 */
	public Integer getNrDigitoCnpjCpf() {
		return nrDigitoCnpjCpf;
	}

	/**
	 * Set: nrDigitoCnpjCpf.
	 *
	 * @param nrDigitoCnpjCpf the nr digito cnpj cpf
	 */
	public void setNrDigitoCnpjCpf(Integer nrDigitoCnpjCpf) {
		this.nrDigitoCnpjCpf = nrDigitoCnpjCpf;
	}

	/**
	 * Get: cdPessoaJuridicaContrato.
	 *
	 * @return cdPessoaJuridicaContrato
	 */
	public Long getCdPessoaJuridicaContrato() {
		return cdPessoaJuridicaContrato;
	}

	/**
	 * Set: cdPessoaJuridicaContrato.
	 *
	 * @param cdPessoaJuridicaContrato the cd pessoa juridica contrato
	 */
	public void setCdPessoaJuridicaContrato(Long cdPessoaJuridicaContrato) {
		this.cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
	}

	/**
	 * Get: dsRazaoSocial.
	 *
	 * @return dsRazaoSocial
	 */
	public String getDsRazaoSocial() {
		return dsRazaoSocial;
	}

	/**
	 * Set: dsRazaoSocial.
	 *
	 * @param dsRazaoSocial the ds razao social
	 */
	public void setDsRazaoSocial(String dsRazaoSocial) {
		this.dsRazaoSocial = dsRazaoSocial;
	}

	/**
	 * Get: cdEmpresa.
	 *
	 * @return cdEmpresa
	 */
	public Long getCdEmpresa() {
		return cdEmpresa;
	}

	/**
	 * Set: cdEmpresa.
	 *
	 * @param cdEmpresa the cd empresa
	 */
	public void setCdEmpresa(Long cdEmpresa) {
		this.cdEmpresa = cdEmpresa;
	}

	/**
	 * Get: dsEmpresa.
	 *
	 * @return dsEmpresa
	 */
	public String getDsEmpresa() {
		return dsEmpresa;
	}

	/**
	 * Set: dsEmpresa.
	 *
	 * @param dsEmpresa the ds empresa
	 */
	public void setDsEmpresa(String dsEmpresa) {
		this.dsEmpresa = dsEmpresa;
	}

	/**
	 * Get: cdTipoContratoNegocio.
	 *
	 * @return cdTipoContratoNegocio
	 */
	public Integer getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}

	/**
	 * Set: cdTipoContratoNegocio.
	 *
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}

	/**
	 * Get: nrContrato.
	 *
	 * @return nrContrato
	 */
	public Long getNrContrato() {
		return nrContrato;
	}

	/**
	 * Set: nrContrato.
	 *
	 * @param nrContrato the nr contrato
	 */
	public void setNrContrato(Long nrContrato) {
		this.nrContrato = nrContrato;
	}

	/**
	 * Get: cdProdutoServicoOperacao.
	 *
	 * @return cdProdutoServicoOperacao
	 */
	public Integer getCdProdutoServicoOperacao() {
		return cdProdutoServicoOperacao;
	}

	/**
	 * Set: cdProdutoServicoOperacao.
	 *
	 * @param cdProdutoServicoOperacao the cd produto servico operacao
	 */
	public void setCdProdutoServicoOperacao(Integer cdProdutoServicoOperacao) {
		this.cdProdutoServicoOperacao = cdProdutoServicoOperacao;
	}

	/**
	 * Get: dsResumoProdutoServico.
	 *
	 * @return dsResumoProdutoServico
	 */
	public String getDsResumoProdutoServico() {
		return dsResumoProdutoServico;
	}

	/**
	 * Set: dsResumoProdutoServico.
	 *
	 * @param dsResumoProdutoServico the ds resumo produto servico
	 */
	public void setDsResumoProdutoServico(String dsResumoProdutoServico) {
		this.dsResumoProdutoServico = dsResumoProdutoServico;
	}

	/**
	 * Get: cdProdutoServicoRelacionado.
	 *
	 * @return cdProdutoServicoRelacionado
	 */
	public Integer getCdProdutoServicoRelacionado() {
		return cdProdutoServicoRelacionado;
	}

	/**
	 * Set: cdProdutoServicoRelacionado.
	 *
	 * @param cdProdutoServicoRelacionado the cd produto servico relacionado
	 */
	public void setCdProdutoServicoRelacionado(Integer cdProdutoServicoRelacionado) {
		this.cdProdutoServicoRelacionado = cdProdutoServicoRelacionado;
	}

	/**
	 * Get: dsOperacaoProdutoServico.
	 *
	 * @return dsOperacaoProdutoServico
	 */
	public String getDsOperacaoProdutoServico() {
		return dsOperacaoProdutoServico;
	}

	/**
	 * Set: dsOperacaoProdutoServico.
	 *
	 * @param dsOperacaoProdutoServico the ds operacao produto servico
	 */
	public void setDsOperacaoProdutoServico(String dsOperacaoProdutoServico) {
		this.dsOperacaoProdutoServico = dsOperacaoProdutoServico;
	}

	/**
	 * Get: cdControlePagamento.
	 *
	 * @return cdControlePagamento
	 */
	public String getCdControlePagamento() {
		return cdControlePagamento;
	}

	/**
	 * Set: cdControlePagamento.
	 *
	 * @param cdControlePagamento the cd controle pagamento
	 */
	public void setCdControlePagamento(String cdControlePagamento) {
		this.cdControlePagamento = cdControlePagamento;
	}

	/**
	 * Get: dtCreditoPagamento.
	 *
	 * @return dtCreditoPagamento
	 */
	public String getDtCreditoPagamento() {
		return dtCreditoPagamento;
	}

	/**
	 * Set: dtCreditoPagamento.
	 *
	 * @param dtCreditoPagamento the dt credito pagamento
	 */
	public void setDtCreditoPagamento(String dtCreditoPagamento) {
		this.dtCreditoPagamento = dtCreditoPagamento;
	}

	/**
	 * Get: vlEfetivoPagamento.
	 *
	 * @return vlEfetivoPagamento
	 */
	public BigDecimal getVlEfetivoPagamento() {
		return vlEfetivoPagamento;
	}

	/**
	 * Set: vlEfetivoPagamento.
	 *
	 * @param vlEfetivoPagamento the vl efetivo pagamento
	 */
	public void setVlEfetivoPagamento(BigDecimal vlEfetivoPagamento) {
		this.vlEfetivoPagamento = vlEfetivoPagamento;
	}

	/**
	 * Get: cdInscricaoFavorecido.
	 *
	 * @return cdInscricaoFavorecido
	 */
	public Long getCdInscricaoFavorecido() {
		return cdInscricaoFavorecido;
	}

	/**
	 * Set: cdInscricaoFavorecido.
	 *
	 * @param cdInscricaoFavorecido the cd inscricao favorecido
	 */
	public void setCdInscricaoFavorecido(Long cdInscricaoFavorecido) {
		this.cdInscricaoFavorecido = cdInscricaoFavorecido;
	}

	/**
	 * Get: dsFavorecido.
	 *
	 * @return dsFavorecido
	 */
	public String getDsFavorecido() {
		return dsFavorecido;
	}

	/**
	 * Set: dsFavorecido.
	 *
	 * @param dsFavorecido the ds favorecido
	 */
	public void setDsFavorecido(String dsFavorecido) {
		this.dsFavorecido = dsFavorecido;
	}

	/**
	 * Get: cdBancoDebito.
	 *
	 * @return cdBancoDebito
	 */
	public Integer getCdBancoDebito() {
		return cdBancoDebito;
	}

	/**
	 * Set: cdBancoDebito.
	 *
	 * @param cdBancoDebito the cd banco debito
	 */
	public void setCdBancoDebito(Integer cdBancoDebito) {
		this.cdBancoDebito = cdBancoDebito;
	}

	/**
	 * Get: cdAgenciaDebito.
	 *
	 * @return cdAgenciaDebito
	 */
	public Integer getCdAgenciaDebito() {
		return cdAgenciaDebito;
	}

	/**
	 * Set: cdAgenciaDebito.
	 *
	 * @param cdAgenciaDebito the cd agencia debito
	 */
	public void setCdAgenciaDebito(Integer cdAgenciaDebito) {
		this.cdAgenciaDebito = cdAgenciaDebito;
	}

	/**
	 * Get: cdDigitoAgenciaDebito.
	 *
	 * @return cdDigitoAgenciaDebito
	 */
	public Integer getCdDigitoAgenciaDebito() {
		return cdDigitoAgenciaDebito;
	}

	/**
	 * Set: cdDigitoAgenciaDebito.
	 *
	 * @param cdDigitoAgenciaDebito the cd digito agencia debito
	 */
	public void setCdDigitoAgenciaDebito(Integer cdDigitoAgenciaDebito) {
		this.cdDigitoAgenciaDebito = cdDigitoAgenciaDebito;
	}

	/**
	 * Get: cdContaDebito.
	 *
	 * @return cdContaDebito
	 */
	public Long getCdContaDebito() {
		return cdContaDebito;
	}

	/**
	 * Set: cdContaDebito.
	 *
	 * @param cdContaDebito the cd conta debito
	 */
	public void setCdContaDebito(Long cdContaDebito) {
		this.cdContaDebito = cdContaDebito;
	}

	/**
	 * Get: cdDigitoContaDebito.
	 *
	 * @return cdDigitoContaDebito
	 */
	public String getCdDigitoContaDebito() {
		return cdDigitoContaDebito;
	}

	/**
	 * Set: cdDigitoContaDebito.
	 *
	 * @param cdDigitoContaDebito the cd digito conta debito
	 */
	public void setCdDigitoContaDebito(String cdDigitoContaDebito) {
		this.cdDigitoContaDebito = cdDigitoContaDebito;
	}

	/**
	 * Get: cdBancoCredito.
	 *
	 * @return cdBancoCredito
	 */
	public Integer getCdBancoCredito() {
		return cdBancoCredito;
	}

	/**
	 * Set: cdBancoCredito.
	 *
	 * @param cdBancoCredito the cd banco credito
	 */
	public void setCdBancoCredito(Integer cdBancoCredito) {
		this.cdBancoCredito = cdBancoCredito;
	}

	/**
	 * Get: cdAgenciaCredito.
	 *
	 * @return cdAgenciaCredito
	 */
	public Integer getCdAgenciaCredito() {
		return cdAgenciaCredito;
	}

	/**
	 * Set: cdAgenciaCredito.
	 *
	 * @param cdAgenciaCredito the cd agencia credito
	 */
	public void setCdAgenciaCredito(Integer cdAgenciaCredito) {
		this.cdAgenciaCredito = cdAgenciaCredito;
	}

	/**
	 * Get: cdDigitoAgenciaCredito.
	 *
	 * @return cdDigitoAgenciaCredito
	 */
	public Integer getCdDigitoAgenciaCredito() {
		return cdDigitoAgenciaCredito;
	}

	/**
	 * Set: cdDigitoAgenciaCredito.
	 *
	 * @param cdDigitoAgenciaCredito the cd digito agencia credito
	 */
	public void setCdDigitoAgenciaCredito(Integer cdDigitoAgenciaCredito) {
		this.cdDigitoAgenciaCredito = cdDigitoAgenciaCredito;
	}

	/**
	 * Get: cdContaCredito.
	 *
	 * @return cdContaCredito
	 */
	public Long getCdContaCredito() {
		return cdContaCredito;
	}

	/**
	 * Set: cdContaCredito.
	 *
	 * @param cdContaCredito the cd conta credito
	 */
	public void setCdContaCredito(Long cdContaCredito) {
		this.cdContaCredito = cdContaCredito;
	}

	/**
	 * Get: cdDigitoContaCredito.
	 *
	 * @return cdDigitoContaCredito
	 */
	public String getCdDigitoContaCredito() {
		return cdDigitoContaCredito;
	}

	/**
	 * Set: cdDigitoContaCredito.
	 *
	 * @param cdDigitoContaCredito the cd digito conta credito
	 */
	public void setCdDigitoContaCredito(String cdDigitoContaCredito) {
		this.cdDigitoContaCredito = cdDigitoContaCredito;
	}

	/**
	 * Get: cdSituacaoOperacaoPagamento.
	 *
	 * @return cdSituacaoOperacaoPagamento
	 */
	public Integer getCdSituacaoOperacaoPagamento() {
		return cdSituacaoOperacaoPagamento;
	}

	/**
	 * Set: cdSituacaoOperacaoPagamento.
	 *
	 * @param cdSituacaoOperacaoPagamento the cd situacao operacao pagamento
	 */
	public void setCdSituacaoOperacaoPagamento(Integer cdSituacaoOperacaoPagamento) {
		this.cdSituacaoOperacaoPagamento = cdSituacaoOperacaoPagamento;
	}

	/**
	 * Get: dsSituacaoOperacaoPagamento.
	 *
	 * @return dsSituacaoOperacaoPagamento
	 */
	public String getDsSituacaoOperacaoPagamento() {
		return dsSituacaoOperacaoPagamento;
	}

	/**
	 * Set: dsSituacaoOperacaoPagamento.
	 *
	 * @param dsSituacaoOperacaoPagamento the ds situacao operacao pagamento
	 */
	public void setDsSituacaoOperacaoPagamento(String dsSituacaoOperacaoPagamento) {
		this.dsSituacaoOperacaoPagamento = dsSituacaoOperacaoPagamento;
	}

	/**
	 * Get: cdMotivoSituacaoPagamento.
	 *
	 * @return cdMotivoSituacaoPagamento
	 */
	public Integer getCdMotivoSituacaoPagamento() {
		return cdMotivoSituacaoPagamento;
	}

	/**
	 * Set: cdMotivoSituacaoPagamento.
	 *
	 * @param cdMotivoSituacaoPagamento the cd motivo situacao pagamento
	 */
	public void setCdMotivoSituacaoPagamento(Integer cdMotivoSituacaoPagamento) {
		this.cdMotivoSituacaoPagamento = cdMotivoSituacaoPagamento;
	}

	/**
	 * Get: dsMotivoSituacaoPagamento.
	 *
	 * @return dsMotivoSituacaoPagamento
	 */
	public String getDsMotivoSituacaoPagamento() {
		return dsMotivoSituacaoPagamento;
	}

	/**
	 * Set: dsMotivoSituacaoPagamento.
	 *
	 * @param dsMotivoSituacaoPagamento the ds motivo situacao pagamento
	 */
	public void setDsMotivoSituacaoPagamento(String dsMotivoSituacaoPagamento) {
		this.dsMotivoSituacaoPagamento = dsMotivoSituacaoPagamento;
	}

	/**
	 * Get: cdTipoCanal.
	 *
	 * @return cdTipoCanal
	 */
	public Integer getCdTipoCanal() {
		return cdTipoCanal;
	}

	/**
	 * Set: cdTipoCanal.
	 *
	 * @param cdTipoCanal the cd tipo canal
	 */
	public void setCdTipoCanal(Integer cdTipoCanal) {
		this.cdTipoCanal = cdTipoCanal;
	}

	/**
	 * Get: cdTipoTela.
	 *
	 * @return cdTipoTela
	 */
	public Integer getCdTipoTela() {
		return cdTipoTela;
	}

	/**
	 * Set: cdTipoTela.
	 *
	 * @param cdTipoTela the cd tipo tela
	 */
	public void setCdTipoTela(Integer cdTipoTela) {
		this.cdTipoTela = cdTipoTela;
	}

	/**
	 * Get: dtEfetivacao.
	 *
	 * @return dtEfetivacao
	 */
	public String getDtEfetivacao() {
		return dtEfetivacao;
	}

	/**
	 * Set: dtEfetivacao.
	 *
	 * @param dtEfetivacao the dt efetivacao
	 */
	public void setDtEfetivacao(String dtEfetivacao) {
		this.dtEfetivacao = dtEfetivacao;
	}

	/**
	 * Get: cdCpfCnpjRecebedor.
	 *
	 * @return cdCpfCnpjRecebedor
	 */
	public Long getCdCpfCnpjRecebedor() {
		return cdCpfCnpjRecebedor;
	}

	/**
	 * Set: cdCpfCnpjRecebedor.
	 *
	 * @param cdCpfCnpjRecebedor the cd cpf cnpj recebedor
	 */
	public void setCdCpfCnpjRecebedor(Long cdCpfCnpjRecebedor) {
		this.cdCpfCnpjRecebedor = cdCpfCnpjRecebedor;
	}

	/**
	 * Get: cdFilialRecebedor.
	 *
	 * @return cdFilialRecebedor
	 */
	public Integer getCdFilialRecebedor() {
		return cdFilialRecebedor;
	}

	/**
	 * Set: cdFilialRecebedor.
	 *
	 * @param cdFilialRecebedor the cd filial recebedor
	 */
	public void setCdFilialRecebedor(Integer cdFilialRecebedor) {
		this.cdFilialRecebedor = cdFilialRecebedor;
	}

	/**
	 * Get: cdDigitoCpfRecebedor.
	 *
	 * @return cdDigitoCpfRecebedor
	 */
	public Integer getCdDigitoCpfRecebedor() {
		return cdDigitoCpfRecebedor;
	}

	/**
	 * Set: cdDigitoCpfRecebedor.
	 *
	 * @param cdDigitoCpfRecebedor the cd digito cpf recebedor
	 */
	public void setCdDigitoCpfRecebedor(Integer cdDigitoCpfRecebedor) {
		this.cdDigitoCpfRecebedor = cdDigitoCpfRecebedor;
	}

	/**
	 * Get: dsCododigoTabelaConsulta.
	 *
	 * @return dsCododigoTabelaConsulta
	 */
	public Integer getDsCododigoTabelaConsulta() {
		return dsCododigoTabelaConsulta;
	}

	/**
	 * Set: dsCododigoTabelaConsulta.
	 *
	 * @param dsCododigoTabelaConsulta the ds cododigo tabela consulta
	 */
	public void setDsCododigoTabelaConsulta(Integer dsCododigoTabelaConsulta) {
		this.dsCododigoTabelaConsulta = dsCododigoTabelaConsulta;
	}

	/**
	 * Get: nrArquivoRemessa.
	 *
	 * @return nrArquivoRemessa
	 */
	public Long getNrArquivoRemessa() {
		return nrArquivoRemessa;
	}

	/**
	 * Set: nrArquivoRemessa.
	 *
	 * @param nrArquivoRemessa the nr arquivo remessa
	 */
	public void setNrArquivoRemessa(Long nrArquivoRemessa) {
		this.nrArquivoRemessa = nrArquivoRemessa;
	}

	/**
	 * Is check.
	 *
	 * @return true, if is check
	 */
	public boolean isCheck() {
		return check;
	}

	/**
	 * Set: check.
	 *
	 * @param check the check
	 */
	public void setCheck(boolean check) {
		this.check = check;
	}

	/**
	 * Get: cdCpfCnpjFormatado.
	 *
	 * @return cdCpfCnpjFormatado
	 */
	public String getCdCpfCnpjFormatado() {
		return CpfCnpjUtils.formatCpfCnpjCompleto(PgitUtil.verificaLongNulo(getNrCnpjCpf()), PgitUtil.verificaIntegerNulo(getNrFilialCnpjCpf()), PgitUtil.verificaIntegerNulo(getNrDigitoCnpjCpf()));
	}

	/**
	 * Get: dtCreditoPagamentoFormatada.
	 *
	 * @return dtCreditoPagamentoFormatada
	 */
	public String getDtCreditoPagamentoFormatada() {
		return FormatarData.formatarData(PgitUtil.verificaStringNula(getDtCreditoPagamento()));
	}

	/**
	 * Get: vlEfetivoPagamentoFormatado.
	 *
	 * @return vlEfetivoPagamentoFormatado
	 */
	public String getVlEfetivoPagamentoFormatado() {
		return NumberUtils.format(PgitUtil.verificaBigDecimalNulo(getVlEfetivoPagamento()));
	}

	/**
	 * Get: favorecidoBeneficiarioFormatado.
	 *
	 * @return favorecidoBeneficiarioFormatado
	 */
	public String getFavorecidoBeneficiarioFormatado() {
		return PgitUtil.concatenarCampos(PgitUtil.verificaLongNulo(getCdInscricaoFavorecido()), PgitUtil.verificaStringNula(getDsFavorecido()), "-");
	}

	/**
	 * Get: bancoAgenciaContaCreditoFormatado.
	 *
	 * @return bancoAgenciaContaCreditoFormatado
	 */
	public String getBancoAgenciaContaCreditoFormatado() {
		return PgitUtil.formatBancoAgenciaConta(PgitUtil.verificaIntegerNulo(getCdBancoCredito()), PgitUtil.verificaIntegerNulo(getCdAgenciaCredito()), PgitUtil.verificaIntegerNulo(getCdDigitoAgenciaCredito()), PgitUtil.verificaLongNulo(getCdContaCredito()), PgitUtil.verificaStringNula(getCdDigitoContaCredito()), true);
	}

	/**
	 * Get: bancoAgenciaContaDebitoFormatado.
	 *
	 * @return bancoAgenciaContaDebitoFormatado
	 */
	public String getBancoAgenciaContaDebitoFormatado() {
		return PgitUtil.formatBancoAgenciaConta(PgitUtil.verificaIntegerNulo(getCdBancoDebito()), PgitUtil.verificaIntegerNulo(getCdAgenciaDebito()), PgitUtil.verificaIntegerNulo(getCdDigitoAgenciaDebito()), PgitUtil.verificaLongNulo(getCdContaDebito()), PgitUtil.verificaStringNula(getCdDigitoContaDebito()), true);
	}
	
	/**
	 * Get: cdBancoCreditoFormatado.
	 *
	 * @return cdBancoCreditoFormatado
	 */
	public String getCdBancoCreditoFormatado(){
		return getCdBancoCredito() == 0 ? "" : (SiteUtil.formatNumber(String.valueOf(getCdBancoCredito()), 3));
	}
    
	/**
	 * Get: cdAgenciaCreditoFormatado.
	 *
	 * @return cdAgenciaCreditoFormatado
	 */
	public String getCdAgenciaCreditoFormatado(){
    	return getCdAgenciaCredito() == 0 ? "" : (SiteUtil.formatNumber(String.valueOf(getCdAgenciaCredito()), 5));
    }
	
    /**
     * Get: cdDigitoAgenciaCreditoFormatado.
     *
     * @return cdDigitoAgenciaCreditoFormatado
     */
    public String getCdDigitoAgenciaCreditoFormatado(){
    	return SiteUtil.formatNumber(String.valueOf(getCdDigitoAgenciaCredito()), 2);
    }
    
    /**
     * Get: cdContaCreditoFormatado.
     *
     * @return cdContaCreditoFormatado
     */
    public String getCdContaCreditoFormatado(){
    	return SiteUtil.formatNumber(String.valueOf(getCdContaCredito()), 13);
    }
    
    /**
     * Get: cdDigitoContaCreditoFormatado.
     *
     * @return cdDigitoContaCreditoFormatado
     */
    public String getCdDigitoContaCreditoFormatado(){
    	return getCdDigitoContaCredito();
    }
    
    /**
     * Get: agenciaCreditoFormatada.
     *
     * @return agenciaCreditoFormatada
     */
    public String getAgenciaCreditoFormatada(){
    	return getCdAgenciaCredito() == 0 ?  "" : (PgitUtil.formatAgencia(getCdAgenciaCredito(), getCdDigitoAgenciaCredito(), false));
    }
    
    /**
     * Get: contaCreditoFormatada.
     *
     * @return contaCreditoFormatada
     */
    public String getContaCreditoFormatada(){
    	return PgitUtil.formatConta(getCdContaCredito(), getCdDigitoContaCredito(), false);
    }
}