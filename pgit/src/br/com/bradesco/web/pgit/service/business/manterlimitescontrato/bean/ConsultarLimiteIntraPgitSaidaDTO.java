/*
 * Nome: br.com.bradesco.web.pgit.service.business.manterlimitescontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.manterlimitescontrato.bean;
import java.math.BigDecimal;

import org.apache.commons.beanutils.ConvertUtils;

import br.com.bradesco.web.pgit.utils.PgitUtil;
import br.com.bradesco.web.pgit.view.converters.FormatarData;
import br.com.bradesco.web.pgit.view.utils.PgitFacesUtils;

/**
 * Nome: ConsultarLimiteIntraPgitSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarLimiteIntraPgitSaidaDTO{
	
	/** Atributo codMensagem. */
	private String codMensagem;
	
	/** Atributo mensagem. */
	private String mensagem;
	
	/** Atributo numeroLinhas. */
	private Integer numeroLinhas;
	
	/** Atributo cdPessoaJuridicaVinculo. */
	private Long cdPessoaJuridicaVinculo;
	
	/** Atributo cdTipoContratoVinculo. */
	private Integer cdTipoContratoVinculo;
	
	/** Atributo nrSequenciaContratoVinculo. */
	private Long nrSequenciaContratoVinculo;
	
	/** Atributo cdTipoVinculoContrato. */
	private Integer cdTipoVinculoContrato;
	
	/** Atributo cdCpfCnpj. */
	private String cdCpfCnpj;
	
	/** Atributo nmParticipante. */
	private String nmParticipante;
	
	/** Atributo cdBanco. */
	private Integer cdBanco;
	
	/** Atributo dsBanco. */
	private String dsBanco;
	
	/** Atributo cdAgencia. */
	private Integer cdAgencia;
	
	/** Atributo cdDigitoAgencia. */
	private Integer cdDigitoAgencia;
	
	/** Atributo dsAgencia. */
	private String dsAgencia;
	
	/** Atributo cdConta. */
	private Long cdConta;
	
	/** Atributo cdDigitoConta. */
	private String cdDigitoConta;
	
	/** Atributo cdTipoConta. */
	private Integer cdTipoConta;
	
	/** Atributo dsTipoConta. */
	private String dsTipoConta;
	
	/** Atributo dsTipoDebito. */
	private String dsTipoDebito;
	
	/** Atributo dsTipoTarifa. */
	private String dsTipoTarifa;
	
	/** Atributo dsTipoEstorno. */
	private String dsTipoEstorno;
	
	/** Atributo dsAlternativa. */
	private String dsAlternativa;
	
	/** Atributo cdSituacao. */
	private Integer cdSituacao;
	
	/** Atributo dsSituacao. */
	private String dsSituacao;
	
	/** Atributo dsDevolucao. */
	private String dsDevolucao;
	
	/** Atributo vlAdicionalContratoConta. */
	private BigDecimal vlAdicionalContratoConta;
	
	/** Atributo dtInicioValorAdicional. */
	private String dtInicioValorAdicional;
	
	/** Atributo dtFimValorAdicional. */
	private String dtFimValorAdicional;
	
	/** Atributo vlAdcContrCtaTed. */
	private BigDecimal vlAdcContrCtaTed;
    
    /** Atributo dtIniVlrAdicionalTed. */
    private String dtIniVlrAdicionalTed;
    
    /** Atributo dtFimVlrAdicionalTed. */
    private String dtFimVlrAdicionalTed;	
	
	/** Atributo bancoFormatado. */
	private String bancoFormatado;
	
	/** Atributo agenciaFormatada. */
	private String agenciaFormatada;
	
	/** Atributo contaFormatada. */
	private String contaFormatada;
	
	/** Atributo prazoValidade. */
	private String prazoValidade;
	
	/** Atributo prazoValidadeTED. */
	private String prazoValidadeTED;

	/**
	 * Gets the prazo validade formatada.
	 *
	 * @return the prazo validade formatada
	 */
	public String getPrazoValidadeFormatada(){
		if("".equals(getDtInicioValorAdicional()) && !"".equals(getDtFimValorAdicional())){
			return FormatarData.formatarData(getDtInicioValorAdicional());
		}else if(!"".equals(getDtInicioValorAdicional()) && "".equals(getDtFimValorAdicional())){
			return FormatarData.formatarData(getDtFimValorAdicional());
		}
		
		return FormatarData.formatarData(getDtInicioValorAdicional()) + " - " + 
			FormatarData.formatarData(getDtFimValorAdicional());
	}
	
	public String getPrazoValidadeTEDFormatada(){
		if("".equals(getDtIniVlrAdicionalTed()) && !"".equals(getDtFimVlrAdicionalTed())){
			return FormatarData.formatarData(getDtIniVlrAdicionalTed());
		}else if(!"".equals(getDtIniVlrAdicionalTed()) && "".equals(getDtFimVlrAdicionalTed())){
			return FormatarData.formatarData(getDtFimVlrAdicionalTed());
		}
		
		return FormatarData.formatarData(getDtIniVlrAdicionalTed()) + " - " + 
			FormatarData.formatarData(getDtFimVlrAdicionalTed());
	}
	
	/**
	 * Get: prazoValidade.
	 *
	 * @return prazoValidade
	 */
	public String getPrazoValidade() {
	    return prazoValidade;
	}

	/**
	 * Set: prazoValidade.
	 *
	 * @param prazoValidade the prazo validade
	 */
	public void setPrazoValidade(String prazoValidade) {
	    this.prazoValidade = prazoValidade;
	}

	/**
	 * Get: agenciaFormatada.
	 *
	 * @return agenciaFormatada
	 */
	public String getAgenciaFormatada() {
	    return agenciaFormatada;
	}

	/**
	 * Set: agenciaFormatada.
	 *
	 * @param agenciaFormatada the agencia formatada
	 */
	public void setAgenciaFormatada(String agenciaFormatada) {
	    this.agenciaFormatada = agenciaFormatada;
	}

	/**
	 * Get: bancoFormatado.
	 *
	 * @return bancoFormatado
	 */
	public String getBancoFormatado() {
	    return bancoFormatado;
	}

	/**
	 * Set: bancoFormatado.
	 *
	 * @param bancoFormatado the banco formatado
	 */
	public void setBancoFormatado(String bancoFormatado) {
	    this.bancoFormatado = bancoFormatado;
	}

	/**
	 * Get: contaFormatada.
	 *
	 * @return contaFormatada
	 */
	public String getContaFormatada() {
	    return contaFormatada;
	}

	/**
	 * Set: contaFormatada.
	 *
	 * @param contaFormatada the conta formatada
	 */
	public void setContaFormatada(String contaFormatada) {
	    this.contaFormatada = contaFormatada;
	}

	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem(){
		return codMensagem;
	}

	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem){
		this.codMensagem = codMensagem;
	}

	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem(){
		return mensagem;
	}

	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem){
		this.mensagem = mensagem;
	}

	/**
	 * Get: numeroLinhas.
	 *
	 * @return numeroLinhas
	 */
	public Integer getNumeroLinhas(){
		return numeroLinhas;
	}

	/**
	 * Set: numeroLinhas.
	 *
	 * @param numeroLinhas the numero linhas
	 */
	public void setNumeroLinhas(Integer numeroLinhas){
		this.numeroLinhas = numeroLinhas;
	}

	/**
	 * Get: cdPessoaJuridicaVinculo.
	 *
	 * @return cdPessoaJuridicaVinculo
	 */
	public Long getCdPessoaJuridicaVinculo(){
		return cdPessoaJuridicaVinculo;
	}

	/**
	 * Set: cdPessoaJuridicaVinculo.
	 *
	 * @param cdPessoaJuridicaVinculo the cd pessoa juridica vinculo
	 */
	public void setCdPessoaJuridicaVinculo(Long cdPessoaJuridicaVinculo){
		this.cdPessoaJuridicaVinculo = cdPessoaJuridicaVinculo;
	}

	/**
	 * Get: cdTipoContratoVinculo.
	 *
	 * @return cdTipoContratoVinculo
	 */
	public Integer getCdTipoContratoVinculo(){
		return cdTipoContratoVinculo;
	}

	/**
	 * Set: cdTipoContratoVinculo.
	 *
	 * @param cdTipoContratoVinculo the cd tipo contrato vinculo
	 */
	public void setCdTipoContratoVinculo(Integer cdTipoContratoVinculo){
		this.cdTipoContratoVinculo = cdTipoContratoVinculo;
	}

	/**
	 * Get: nrSequenciaContratoVinculo.
	 *
	 * @return nrSequenciaContratoVinculo
	 */
	public Long getNrSequenciaContratoVinculo(){
		return nrSequenciaContratoVinculo;
	}

	/**
	 * Set: nrSequenciaContratoVinculo.
	 *
	 * @param nrSequenciaContratoVinculo the nr sequencia contrato vinculo
	 */
	public void setNrSequenciaContratoVinculo(Long nrSequenciaContratoVinculo){
		this.nrSequenciaContratoVinculo = nrSequenciaContratoVinculo;
	}

	/**
	 * Get: cdTipoVinculoContrato.
	 *
	 * @return cdTipoVinculoContrato
	 */
	public Integer getCdTipoVinculoContrato(){
		return cdTipoVinculoContrato;
	}

	/**
	 * Set: cdTipoVinculoContrato.
	 *
	 * @param cdTipoVinculoContrato the cd tipo vinculo contrato
	 */
	public void setCdTipoVinculoContrato(Integer cdTipoVinculoContrato){
		this.cdTipoVinculoContrato = cdTipoVinculoContrato;
	}

	/**
	 * Get: cdCpfCnpj.
	 *
	 * @return cdCpfCnpj
	 */
	public String getCdCpfCnpj(){
		return cdCpfCnpj;
	}

	/**
	 * Set: cdCpfCnpj.
	 *
	 * @param cdCpfCnpj the cd cpf cnpj
	 */
	public void setCdCpfCnpj(String cdCpfCnpj){
		this.cdCpfCnpj = cdCpfCnpj;
	}

	/**
	 * Get: nmParticipante.
	 *
	 * @return nmParticipante
	 */
	public String getNmParticipante(){
		return nmParticipante;
	}

	/**
	 * Set: nmParticipante.
	 *
	 * @param nmParticipante the nm participante
	 */
	public void setNmParticipante(String nmParticipante){
		this.nmParticipante = nmParticipante;
	}

	/**
	 * Get: cdBanco.
	 *
	 * @return cdBanco
	 */
	public Integer getCdBanco(){
		return cdBanco;
	}

	/**
	 * Set: cdBanco.
	 *
	 * @param cdBanco the cd banco
	 */
	public void setCdBanco(Integer cdBanco){
		this.cdBanco = cdBanco;
	}

	/**
	 * Get: dsBanco.
	 *
	 * @return dsBanco
	 */
	public String getDsBanco(){
		return dsBanco;
	}

	/**
	 * Set: dsBanco.
	 *
	 * @param dsBanco the ds banco
	 */
	public void setDsBanco(String dsBanco){
		this.dsBanco = dsBanco;
	}

	/**
	 * Get: cdAgencia.
	 *
	 * @return cdAgencia
	 */
	public Integer getCdAgencia(){
		return cdAgencia;
	}

	/**
	 * Set: cdAgencia.
	 *
	 * @param cdAgencia the cd agencia
	 */
	public void setCdAgencia(Integer cdAgencia){
		this.cdAgencia = cdAgencia;
	}

	/**
	 * Get: cdDigitoAgencia.
	 *
	 * @return cdDigitoAgencia
	 */
	public Integer getCdDigitoAgencia(){
		return cdDigitoAgencia;
	}

	/**
	 * Set: cdDigitoAgencia.
	 *
	 * @param cdDigitoAgencia the cd digito agencia
	 */
	public void setCdDigitoAgencia(Integer cdDigitoAgencia){
		this.cdDigitoAgencia = cdDigitoAgencia;
	}

	/**
	 * Get: dsAgencia.
	 *
	 * @return dsAgencia
	 */
	public String getDsAgencia(){
		return dsAgencia;
	}

	/**
	 * Set: dsAgencia.
	 *
	 * @param dsAgencia the ds agencia
	 */
	public void setDsAgencia(String dsAgencia){
		this.dsAgencia = dsAgencia;
	}

	/**
	 * Get: cdConta.
	 *
	 * @return cdConta
	 */
	public Long getCdConta(){
		return cdConta;
	}

	/**
	 * Set: cdConta.
	 *
	 * @param cdConta the cd conta
	 */
	public void setCdConta(Long cdConta){
		this.cdConta = cdConta;
	}

	/**
	 * Get: cdDigitoConta.
	 *
	 * @return cdDigitoConta
	 */
	public String getCdDigitoConta(){
		return cdDigitoConta;
	}

	/**
	 * Set: cdDigitoConta.
	 *
	 * @param cdDigitoConta the cd digito conta
	 */
	public void setCdDigitoConta(String cdDigitoConta){
		this.cdDigitoConta = cdDigitoConta;
	}

	/**
	 * Get: cdTipoConta.
	 *
	 * @return cdTipoConta
	 */
	public Integer getCdTipoConta(){
		return cdTipoConta;
	}

	/**
	 * Set: cdTipoConta.
	 *
	 * @param cdTipoConta the cd tipo conta
	 */
	public void setCdTipoConta(Integer cdTipoConta){
		this.cdTipoConta = cdTipoConta;
	}

	/**
	 * Get: dsTipoConta.
	 *
	 * @return dsTipoConta
	 */
	public String getDsTipoConta(){
		return dsTipoConta;
	}

	/**
	 * Set: dsTipoConta.
	 *
	 * @param dsTipoConta the ds tipo conta
	 */
	public void setDsTipoConta(String dsTipoConta){
		this.dsTipoConta = dsTipoConta;
	}

	/**
	 * Get: dsTipoDebito.
	 *
	 * @return dsTipoDebito
	 */
	public String getDsTipoDebito(){
		return dsTipoDebito;
	}

	/**
	 * Set: dsTipoDebito.
	 *
	 * @param dsTipoDebito the ds tipo debito
	 */
	public void setDsTipoDebito(String dsTipoDebito){
		this.dsTipoDebito = dsTipoDebito;
	}

	/**
	 * Get: dsTipoTarifa.
	 *
	 * @return dsTipoTarifa
	 */
	public String getDsTipoTarifa(){
		return dsTipoTarifa;
	}

	/**
	 * Set: dsTipoTarifa.
	 *
	 * @param dsTipoTarifa the ds tipo tarifa
	 */
	public void setDsTipoTarifa(String dsTipoTarifa){
		this.dsTipoTarifa = dsTipoTarifa;
	}

	/**
	 * Get: dsTipoEstorno.
	 *
	 * @return dsTipoEstorno
	 */
	public String getDsTipoEstorno(){
		return dsTipoEstorno;
	}

	/**
	 * Set: dsTipoEstorno.
	 *
	 * @param dsTipoEstorno the ds tipo estorno
	 */
	public void setDsTipoEstorno(String dsTipoEstorno){
		this.dsTipoEstorno = dsTipoEstorno;
	}

	/**
	 * Get: dsAlternativa.
	 *
	 * @return dsAlternativa
	 */
	public String getDsAlternativa(){
		return dsAlternativa;
	}

	/**
	 * Set: dsAlternativa.
	 *
	 * @param dsAlternativa the ds alternativa
	 */
	public void setDsAlternativa(String dsAlternativa){
		this.dsAlternativa = dsAlternativa;
	}

	/**
	 * Get: cdSituacao.
	 *
	 * @return cdSituacao
	 */
	public Integer getCdSituacao(){
		return cdSituacao;
	}

	/**
	 * Set: cdSituacao.
	 *
	 * @param cdSituacao the cd situacao
	 */
	public void setCdSituacao(Integer cdSituacao){
		this.cdSituacao = cdSituacao;
	}

	/**
	 * Get: dsSituacao.
	 *
	 * @return dsSituacao
	 */
	public String getDsSituacao(){
		return dsSituacao;
	}

	/**
	 * Set: dsSituacao.
	 *
	 * @param dsSituacao the ds situacao
	 */
	public void setDsSituacao(String dsSituacao){
		this.dsSituacao = dsSituacao;
	}

	/**
	 * Get: vlAdicionalContratoConta.
	 *
	 * @return vlAdicionalContratoConta
	 */
	public BigDecimal getVlAdicionalContratoConta(){
		return vlAdicionalContratoConta;
	}

	/**
	 * Set: vlAdicionalContratoConta.
	 *
	 * @param vlAdicionalContratoConta the vl adicional contrato conta
	 */
	public void setVlAdicionalContratoConta(BigDecimal vlAdicionalContratoConta){
		this.vlAdicionalContratoConta = vlAdicionalContratoConta;
	}

	/**
	 * Get: dtInicioValorAdicional.
	 *
	 * @return dtInicioValorAdicional
	 */
	public String getDtInicioValorAdicional(){
		return dtInicioValorAdicional;
	}

	/**
	 * Set: dtInicioValorAdicional.
	 *
	 * @param dtInicioValorAdicional the dt inicio valor adicional
	 */
	public void setDtInicioValorAdicional(String dtInicioValorAdicional){
		this.dtInicioValorAdicional = dtInicioValorAdicional;
	}

	/**
	 * Get: dtFimValorAdicional.
	 *
	 * @return dtFimValorAdicional
	 */
	public String getDtFimValorAdicional(){
		return dtFimValorAdicional;
	}

	/**
	 * Set: dtFimValorAdicional.
	 *
	 * @param dtFimValorAdicional the dt fim valor adicional
	 */
	public void setDtFimValorAdicional(String dtFimValorAdicional){
		this.dtFimValorAdicional = dtFimValorAdicional;
	}

	/**
	 * Get: vlAdcContrCtaTed.
	 *
	 * @return vlAdcContrCtaTed
	 */
	public BigDecimal getVlAdcContrCtaTed() {
		return vlAdcContrCtaTed;
	}

	/**
	 * Set: vlAdcContrCtaTed.
	 *
	 * @param vlAdcContrCtaTed the vl adc contr cta ted
	 */
	public void setVlAdcContrCtaTed(BigDecimal vlAdcContrCtaTed) {
		this.vlAdcContrCtaTed = vlAdcContrCtaTed;
	}

	/**
	 * Get: dtIniVlrAdicionalTed.
	 *
	 * @return dtIniVlrAdicionalTed
	 */
	public String getDtIniVlrAdicionalTed() {
		return dtIniVlrAdicionalTed;
	}

	/**
	 * Set: dtIniVlrAdicionalTed.
	 *
	 * @param dtIniVlrAdicionalTed the dt ini vlr adicional ted
	 */
	public void setDtIniVlrAdicionalTed(String dtIniVlrAdicionalTed) {
		this.dtIniVlrAdicionalTed = dtIniVlrAdicionalTed;
	}

	/**
	 * Get: dtFimVlrAdicionalTed.
	 *
	 * @return dtFimVlrAdicionalTed
	 */
	public String getDtFimVlrAdicionalTed() {
		return dtFimVlrAdicionalTed;
	}

	/**
	 * Set: dtFimVlrAdicionalTed.
	 *
	 * @param dtFimVlrAdicionalTed the dt fim vlr adicional ted
	 */
	public void setDtFimVlrAdicionalTed(String dtFimVlrAdicionalTed) {
		this.dtFimVlrAdicionalTed = dtFimVlrAdicionalTed;
	}

	/**
	 * Get: prazoValidadeTED.
	 *
	 * @return prazoValidadeTED
	 */
	public String getPrazoValidadeTED() {
		return prazoValidadeTED;
	}

	/**
	 * Set: prazoValidadeTED.
	 *
	 * @param prazoValidadeTED the prazo validade ted
	 */
	public void setPrazoValidadeTED(String prazoValidadeTED) {
		this.prazoValidadeTED = prazoValidadeTED;
	}

	public String getDsDevolucao() {
		return dsDevolucao;
	}

	public void setDsDevolucao(String dsDevolucao) {
		this.dsDevolucao = dsDevolucao;
	}
}