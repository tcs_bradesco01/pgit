/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultarconfigtiposervicomodalidade.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.math.BigDecimal;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ConsultarConfigTipoServicoModalidadeResponse.
 * 
 * @version $Revision$ $Date$
 */
public class ConsultarConfigTipoServicoModalidadeResponse implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _codMensagem
     */
    private java.lang.String _codMensagem;

    /**
     * Field _mensagem
     */
    private java.lang.String _mensagem;

    /**
     * Field _cdAcaoNaoVida
     */
    private int _cdAcaoNaoVida = 0;

    /**
     * keeps track of state for field: _cdAcaoNaoVida
     */
    private boolean _has_cdAcaoNaoVida;

    /**
     * Field _dsAcaoNaoVida
     */
    private java.lang.String _dsAcaoNaoVida;

    /**
     * Field _cdAcertoDadosRecadastramento
     */
    private int _cdAcertoDadosRecadastramento = 0;

    /**
     * keeps track of state for field: _cdAcertoDadosRecadastramento
     */
    private boolean _has_cdAcertoDadosRecadastramento;

    /**
     * Field _dsAcertoDadosRecadastramento
     */
    private java.lang.String _dsAcertoDadosRecadastramento;

    /**
     * Field _cdAgendamentoDebitoVeiculo
     */
    private int _cdAgendamentoDebitoVeiculo = 0;

    /**
     * keeps track of state for field: _cdAgendamentoDebitoVeiculo
     */
    private boolean _has_cdAgendamentoDebitoVeiculo;

    /**
     * Field _dsAgendamentoDebitoVeiculo
     */
    private java.lang.String _dsAgendamentoDebitoVeiculo;

    /**
     * Field _cdAgendamentoPagamentoVencido
     */
    private int _cdAgendamentoPagamentoVencido = 0;

    /**
     * keeps track of state for field: _cdAgendamentoPagamentoVencid
     */
    private boolean _has_cdAgendamentoPagamentoVencido;

    /**
     * Field _dsAgendamentoPagamentoVencido
     */
    private java.lang.String _dsAgendamentoPagamentoVencido;

    /**
     * Field _cdAgendamentoValorMenor
     */
    private int _cdAgendamentoValorMenor = 0;

    /**
     * keeps track of state for field: _cdAgendamentoValorMenor
     */
    private boolean _has_cdAgendamentoValorMenor;

    /**
     * Field _dsAgendamentoValorMenor
     */
    private java.lang.String _dsAgendamentoValorMenor;

    /**
     * Field _cdAgrupamentoAviso
     */
    private int _cdAgrupamentoAviso = 0;

    /**
     * keeps track of state for field: _cdAgrupamentoAviso
     */
    private boolean _has_cdAgrupamentoAviso;

    /**
     * Field _dsAgrupamentoAviso
     */
    private java.lang.String _dsAgrupamentoAviso;

    /**
     * Field _cdAgrupamentoComprovante
     */
    private int _cdAgrupamentoComprovante = 0;

    /**
     * keeps track of state for field: _cdAgrupamentoComprovante
     */
    private boolean _has_cdAgrupamentoComprovante;

    /**
     * Field _dsAgrupamentoComprovante
     */
    private java.lang.String _dsAgrupamentoComprovante;

    /**
     * Field _cdAgrupamentoFormularioRecadastro
     */
    private int _cdAgrupamentoFormularioRecadastro = 0;

    /**
     * keeps track of state for field:
     * _cdAgrupamentoFormularioRecadastro
     */
    private boolean _has_cdAgrupamentoFormularioRecadastro;

    /**
     * Field _dsAgrupamentoFormularioRecadastro
     */
    private java.lang.String _dsAgrupamentoFormularioRecadastro;

    /**
     * Field _cdAntecipacaoRecadastramentoBeneficiario
     */
    private int _cdAntecipacaoRecadastramentoBeneficiario = 0;

    /**
     * keeps track of state for field:
     * _cdAntecipacaoRecadastramentoBeneficiario
     */
    private boolean _has_cdAntecipacaoRecadastramentoBeneficiario;

    /**
     * Field _dsAntecipacaoRecadastramentoBeneficiario
     */
    private java.lang.String _dsAntecipacaoRecadastramentoBeneficiario;

    /**
     * Field _cdAreaReservada
     */
    private int _cdAreaReservada = 0;

    /**
     * keeps track of state for field: _cdAreaReservada
     */
    private boolean _has_cdAreaReservada;

    /**
     * Field _dsAreaReservada
     */
    private java.lang.String _dsAreaReservada;

    /**
     * Field _cdBaseRecadastramentoBeneficio
     */
    private int _cdBaseRecadastramentoBeneficio = 0;

    /**
     * keeps track of state for field:
     * _cdBaseRecadastramentoBeneficio
     */
    private boolean _has_cdBaseRecadastramentoBeneficio;

    /**
     * Field _dsBaseRecadastramentoBeneficio
     */
    private java.lang.String _dsBaseRecadastramentoBeneficio;

    /**
     * Field _cdBloqueioEmissaoPplta
     */
    private int _cdBloqueioEmissaoPplta = 0;

    /**
     * keeps track of state for field: _cdBloqueioEmissaoPplta
     */
    private boolean _has_cdBloqueioEmissaoPplta;

    /**
     * Field _dsBloqueioEmissaoPapeleta
     */
    private java.lang.String _dsBloqueioEmissaoPapeleta;

    /**
     * Field _cdCaptuTituloRegistro
     */
    private int _cdCaptuTituloRegistro = 0;

    /**
     * keeps track of state for field: _cdCaptuTituloRegistro
     */
    private boolean _has_cdCaptuTituloRegistro;

    /**
     * Field _dsCapturaTituloRegistrado
     */
    private java.lang.String _dsCapturaTituloRegistrado;

    /**
     * Field _cdCobrancaTarifa
     */
    private int _cdCobrancaTarifa = 0;

    /**
     * keeps track of state for field: _cdCobrancaTarifa
     */
    private boolean _has_cdCobrancaTarifa;

    /**
     * Field _dsCobrancaTarifa
     */
    private java.lang.String _dsCobrancaTarifa;

    /**
     * Field _cdConsultaDebitoVeiculo
     */
    private int _cdConsultaDebitoVeiculo = 0;

    /**
     * keeps track of state for field: _cdConsultaDebitoVeiculo
     */
    private boolean _has_cdConsultaDebitoVeiculo;

    /**
     * Field _dsConsultaDebitoVeiculo
     */
    private java.lang.String _dsConsultaDebitoVeiculo;

    /**
     * Field _cdConsultaEndereco
     */
    private int _cdConsultaEndereco = 0;

    /**
     * keeps track of state for field: _cdConsultaEndereco
     */
    private boolean _has_cdConsultaEndereco;

    /**
     * Field _dsConsultaEndereco
     */
    private java.lang.String _dsConsultaEndereco;

    /**
     * Field _cdConsultaSaldoPagamento
     */
    private int _cdConsultaSaldoPagamento = 0;

    /**
     * keeps track of state for field: _cdConsultaSaldoPagamento
     */
    private boolean _has_cdConsultaSaldoPagamento;

    /**
     * Field _dsConsultaSaldoPagamento
     */
    private java.lang.String _dsConsultaSaldoPagamento;

    /**
     * Field _cdContagemConsultaSaldo
     */
    private int _cdContagemConsultaSaldo = 0;

    /**
     * keeps track of state for field: _cdContagemConsultaSaldo
     */
    private boolean _has_cdContagemConsultaSaldo;

    /**
     * Field _dsContagemConsultaSaldo
     */
    private java.lang.String _dsContagemConsultaSaldo;

    /**
     * Field _cdCreditoNaoUtilizado
     */
    private int _cdCreditoNaoUtilizado = 0;

    /**
     * keeps track of state for field: _cdCreditoNaoUtilizado
     */
    private boolean _has_cdCreditoNaoUtilizado;

    /**
     * Field _dsCreditoNaoUtilizado
     */
    private java.lang.String _dsCreditoNaoUtilizado;

    /**
     * Field _cdCriterioEnquandraBeneficio
     */
    private int _cdCriterioEnquandraBeneficio = 0;

    /**
     * keeps track of state for field: _cdCriterioEnquandraBeneficio
     */
    private boolean _has_cdCriterioEnquandraBeneficio;

    /**
     * Field _dsCriterioEnquandraBeneficio
     */
    private java.lang.String _dsCriterioEnquandraBeneficio;

    /**
     * Field _cdCriterioEnquadraRecadastramento
     */
    private int _cdCriterioEnquadraRecadastramento = 0;

    /**
     * keeps track of state for field:
     * _cdCriterioEnquadraRecadastramento
     */
    private boolean _has_cdCriterioEnquadraRecadastramento;

    /**
     * Field _dsCriterioEnquadraRecadastramento
     */
    private java.lang.String _dsCriterioEnquadraRecadastramento;

    /**
     * Field _cdCriterioRstrbTitulo
     */
    private int _cdCriterioRstrbTitulo = 0;

    /**
     * keeps track of state for field: _cdCriterioRstrbTitulo
     */
    private boolean _has_cdCriterioRstrbTitulo;

    /**
     * Field _dsCriterioRastreabilidadeTitulo
     */
    private java.lang.String _dsCriterioRastreabilidadeTitulo;

    /**
     * Field _cdCctciaEspeBeneficio
     */
    private int _cdCctciaEspeBeneficio = 0;

    /**
     * keeps track of state for field: _cdCctciaEspeBeneficio
     */
    private boolean _has_cdCctciaEspeBeneficio;

    /**
     * Field _dsCctciaEspeBeneficio
     */
    private java.lang.String _dsCctciaEspeBeneficio;

    /**
     * Field _cdCctciaIdentificacaoBeneficio
     */
    private int _cdCctciaIdentificacaoBeneficio = 0;

    /**
     * keeps track of state for field:
     * _cdCctciaIdentificacaoBeneficio
     */
    private boolean _has_cdCctciaIdentificacaoBeneficio;

    /**
     * Field _dsCctciaIdentificacaoBeneficio
     */
    private java.lang.String _dsCctciaIdentificacaoBeneficio;

    /**
     * Field _cdCctciaInscricaoFavorecido
     */
    private int _cdCctciaInscricaoFavorecido = 0;

    /**
     * keeps track of state for field: _cdCctciaInscricaoFavorecido
     */
    private boolean _has_cdCctciaInscricaoFavorecido;

    /**
     * Field _dsCctciaInscricaoFavorecido
     */
    private java.lang.String _dsCctciaInscricaoFavorecido;

    /**
     * Field _cdCctciaProprietarioVeculo
     */
    private int _cdCctciaProprietarioVeculo = 0;

    /**
     * keeps track of state for field: _cdCctciaProprietarioVeculo
     */
    private boolean _has_cdCctciaProprietarioVeculo;

    /**
     * Field _dsCctciaProprietarioVeculo
     */
    private java.lang.String _dsCctciaProprietarioVeculo;

    /**
     * Field _cdDisponibilizacaoContaCredito
     */
    private int _cdDisponibilizacaoContaCredito = 0;

    /**
     * keeps track of state for field:
     * _cdDisponibilizacaoContaCredito
     */
    private boolean _has_cdDisponibilizacaoContaCredito;

    /**
     * Field _dsDisponibilizacaoContaCredito
     */
    private java.lang.String _dsDisponibilizacaoContaCredito;

    /**
     * Field _cdDisponibilizacaoDiversoCriterio
     */
    private int _cdDisponibilizacaoDiversoCriterio = 0;

    /**
     * keeps track of state for field:
     * _cdDisponibilizacaoDiversoCriterio
     */
    private boolean _has_cdDisponibilizacaoDiversoCriterio;

    /**
     * Field _dsDisponibilizacaoDiversoCriterio
     */
    private java.lang.String _dsDisponibilizacaoDiversoCriterio;

    /**
     * Field _cdDisponibilizacaoDiversoNao
     */
    private int _cdDisponibilizacaoDiversoNao = 0;

    /**
     * keeps track of state for field: _cdDisponibilizacaoDiversoNao
     */
    private boolean _has_cdDisponibilizacaoDiversoNao;

    /**
     * Field _dsDisponibilizacaoDiversoNao
     */
    private java.lang.String _dsDisponibilizacaoDiversoNao;

    /**
     * Field _cdDisponibilizacaoSalarioCriterio
     */
    private int _cdDisponibilizacaoSalarioCriterio = 0;

    /**
     * keeps track of state for field:
     * _cdDisponibilizacaoSalarioCriterio
     */
    private boolean _has_cdDisponibilizacaoSalarioCriterio;

    /**
     * Field _dsDisponibilizacaoSalarioCriterio
     */
    private java.lang.String _dsDisponibilizacaoSalarioCriterio;

    /**
     * Field _cdDisponibilizacaoSalarioNao
     */
    private int _cdDisponibilizacaoSalarioNao = 0;

    /**
     * keeps track of state for field: _cdDisponibilizacaoSalarioNao
     */
    private boolean _has_cdDisponibilizacaoSalarioNao;

    /**
     * Field _dsDisponibilizacaoSalarioNao
     */
    private java.lang.String _dsDisponibilizacaoSalarioNao;

    /**
     * Field _cdDestinoAviso
     */
    private int _cdDestinoAviso = 0;

    /**
     * keeps track of state for field: _cdDestinoAviso
     */
    private boolean _has_cdDestinoAviso;

    /**
     * Field _dsDestinoAviso
     */
    private java.lang.String _dsDestinoAviso;

    /**
     * Field _cdDestinoComprovante
     */
    private int _cdDestinoComprovante = 0;

    /**
     * keeps track of state for field: _cdDestinoComprovante
     */
    private boolean _has_cdDestinoComprovante;

    /**
     * Field _dsDestinoComprovante
     */
    private java.lang.String _dsDestinoComprovante;

    /**
     * Field _cdDestinoFormularioRecadastramento
     */
    private int _cdDestinoFormularioRecadastramento = 0;

    /**
     * keeps track of state for field:
     * _cdDestinoFormularioRecadastramento
     */
    private boolean _has_cdDestinoFormularioRecadastramento;

    /**
     * Field _dsDestinoFormularioRecadastramento
     */
    private java.lang.String _dsDestinoFormularioRecadastramento;

    /**
     * Field _cdEnvelopeAberto
     */
    private int _cdEnvelopeAberto = 0;

    /**
     * keeps track of state for field: _cdEnvelopeAberto
     */
    private boolean _has_cdEnvelopeAberto;

    /**
     * Field _dsEnvelopeAberto
     */
    private java.lang.String _dsEnvelopeAberto;

    /**
     * Field _cdFavorecidoConsultaPagamento
     */
    private int _cdFavorecidoConsultaPagamento = 0;

    /**
     * keeps track of state for field: _cdFavorecidoConsultaPagament
     */
    private boolean _has_cdFavorecidoConsultaPagamento;

    /**
     * Field _dsFavorecidoConsultaPagamento
     */
    private java.lang.String _dsFavorecidoConsultaPagamento;

    /**
     * Field _cdFormaAutorizacaoPagamento
     */
    private int _cdFormaAutorizacaoPagamento = 0;

    /**
     * keeps track of state for field: _cdFormaAutorizacaoPagamento
     */
    private boolean _has_cdFormaAutorizacaoPagamento;

    /**
     * Field _dsFormaAutorizacaoPagamento
     */
    private java.lang.String _dsFormaAutorizacaoPagamento;

    /**
     * Field _cdFormaEnvioPagamento
     */
    private int _cdFormaEnvioPagamento = 0;

    /**
     * keeps track of state for field: _cdFormaEnvioPagamento
     */
    private boolean _has_cdFormaEnvioPagamento;

    /**
     * Field _dsFormaEnvioPagamento
     */
    private java.lang.String _dsFormaEnvioPagamento;

    /**
     * Field _cdFormaExpiracaoCredito
     */
    private int _cdFormaExpiracaoCredito = 0;

    /**
     * keeps track of state for field: _cdFormaExpiracaoCredito
     */
    private boolean _has_cdFormaExpiracaoCredito;

    /**
     * Field _dsFormaExpiracaoCredito
     */
    private java.lang.String _dsFormaExpiracaoCredito;

    /**
     * Field _cdFormaManutencao
     */
    private int _cdFormaManutencao = 0;

    /**
     * keeps track of state for field: _cdFormaManutencao
     */
    private boolean _has_cdFormaManutencao;

    /**
     * Field _dsFormaManutencao
     */
    private java.lang.String _dsFormaManutencao;

    /**
     * Field _cdFrasePrecadastrada
     */
    private int _cdFrasePrecadastrada = 0;

    /**
     * keeps track of state for field: _cdFrasePrecadastrada
     */
    private boolean _has_cdFrasePrecadastrada;

    /**
     * Field _dsFrasePrecadastrada
     */
    private java.lang.String _dsFrasePrecadastrada;

    /**
     * Field _cdIndicadorAgendaTitulo
     */
    private int _cdIndicadorAgendaTitulo = 0;

    /**
     * keeps track of state for field: _cdIndicadorAgendaTitulo
     */
    private boolean _has_cdIndicadorAgendaTitulo;

    /**
     * Field _dsIndicadorAgendamentoTitulo
     */
    private java.lang.String _dsIndicadorAgendamentoTitulo;

    /**
     * Field _cdIndicadorAutorizacaoCliente
     */
    private int _cdIndicadorAutorizacaoCliente = 0;

    /**
     * keeps track of state for field: _cdIndicadorAutorizacaoClient
     */
    private boolean _has_cdIndicadorAutorizacaoCliente;

    /**
     * Field _dsIndicadorAutorizacaoCliente
     */
    private java.lang.String _dsIndicadorAutorizacaoCliente;

    /**
     * Field _cdIndicadorAutorizacaoComplemento
     */
    private int _cdIndicadorAutorizacaoComplemento = 0;

    /**
     * keeps track of state for field:
     * _cdIndicadorAutorizacaoComplemento
     */
    private boolean _has_cdIndicadorAutorizacaoComplemento;

    /**
     * Field _dsIndicadorAutorizacaoComplemento
     */
    private java.lang.String _dsIndicadorAutorizacaoComplemento;

    /**
     * Field _cdIndicadorCadastroorganizacao
     */
    private int _cdIndicadorCadastroorganizacao = 0;

    /**
     * keeps track of state for field:
     * _cdIndicadorCadastroorganizacao
     */
    private boolean _has_cdIndicadorCadastroorganizacao;

    /**
     * Field _dsIndicadorCadastroorganizacao
     */
    private java.lang.String _dsIndicadorCadastroorganizacao;

    /**
     * Field _cdIndicadorCadastroProcurador
     */
    private int _cdIndicadorCadastroProcurador = 0;

    /**
     * keeps track of state for field: _cdIndicadorCadastroProcurado
     */
    private boolean _has_cdIndicadorCadastroProcurador;

    /**
     * Field _dsIndicadorCadastroProcurador
     */
    private java.lang.String _dsIndicadorCadastroProcurador;

    /**
     * Field _cdIndicadorCartaoSalario
     */
    private int _cdIndicadorCartaoSalario = 0;

    /**
     * keeps track of state for field: _cdIndicadorCartaoSalario
     */
    private boolean _has_cdIndicadorCartaoSalario;

    /**
     * Field _dsIndicadorCartaoSalario
     */
    private java.lang.String _dsIndicadorCartaoSalario;

    /**
     * Field _cdIndicadorEconomicoReajuste
     */
    private int _cdIndicadorEconomicoReajuste = 0;

    /**
     * keeps track of state for field: _cdIndicadorEconomicoReajuste
     */
    private boolean _has_cdIndicadorEconomicoReajuste;

    /**
     * Field _dsIndicadorEconomicoReajuste
     */
    private java.lang.String _dsIndicadorEconomicoReajuste;

    /**
     * Field _cdindicadorExpiraCredito
     */
    private int _cdindicadorExpiraCredito = 0;

    /**
     * keeps track of state for field: _cdindicadorExpiraCredito
     */
    private boolean _has_cdindicadorExpiraCredito;

    /**
     * Field _dsindicadorExpiraCredito
     */
    private java.lang.String _dsindicadorExpiraCredito;

    /**
     * Field _cdindicadorLancamentoProgramado
     */
    private int _cdindicadorLancamentoProgramado = 0;

    /**
     * keeps track of state for field:
     * _cdindicadorLancamentoProgramado
     */
    private boolean _has_cdindicadorLancamentoProgramado;

    /**
     * Field _dsindicadorLancamentoProgramado
     */
    private java.lang.String _dsindicadorLancamentoProgramado;

    /**
     * Field _cdIndicadorMensagemPersonalizada
     */
    private int _cdIndicadorMensagemPersonalizada = 0;

    /**
     * keeps track of state for field:
     * _cdIndicadorMensagemPersonalizada
     */
    private boolean _has_cdIndicadorMensagemPersonalizada;

    /**
     * Field _dsIndicadorMensagemPersonalizada
     */
    private java.lang.String _dsIndicadorMensagemPersonalizada;

    /**
     * Field _cdLancamentoFuturoCredito
     */
    private int _cdLancamentoFuturoCredito = 0;

    /**
     * keeps track of state for field: _cdLancamentoFuturoCredito
     */
    private boolean _has_cdLancamentoFuturoCredito;

    /**
     * Field _dsLancamentoFuturoCredito
     */
    private java.lang.String _dsLancamentoFuturoCredito;

    /**
     * Field _cdLancamentoFuturoDebito
     */
    private int _cdLancamentoFuturoDebito = 0;

    /**
     * keeps track of state for field: _cdLancamentoFuturoDebito
     */
    private boolean _has_cdLancamentoFuturoDebito;

    /**
     * Field _dsLancamentoFuturoDebito
     */
    private java.lang.String _dsLancamentoFuturoDebito;

    /**
     * Field _cdLiberacaoLoteProcs
     */
    private int _cdLiberacaoLoteProcs = 0;

    /**
     * keeps track of state for field: _cdLiberacaoLoteProcs
     */
    private boolean _has_cdLiberacaoLoteProcs;

    /**
     * Field _dsLiberacaoLoteProcessado
     */
    private java.lang.String _dsLiberacaoLoteProcessado;

    /**
     * Field _cdManutencaoBaseRecadastramento
     */
    private int _cdManutencaoBaseRecadastramento = 0;

    /**
     * keeps track of state for field:
     * _cdManutencaoBaseRecadastramento
     */
    private boolean _has_cdManutencaoBaseRecadastramento;

    /**
     * Field _dsManutencaoBaseRecadastramento
     */
    private java.lang.String _dsManutencaoBaseRecadastramento;

    /**
     * Field _cdMidiaDisponivel
     */
    private int _cdMidiaDisponivel = 0;

    /**
     * keeps track of state for field: _cdMidiaDisponivel
     */
    private boolean _has_cdMidiaDisponivel;

    /**
     * Field _dsMidiaDisponivel
     */
    private java.lang.String _dsMidiaDisponivel;

    /**
     * Field _cdMidiaMensagemRecadastramento
     */
    private int _cdMidiaMensagemRecadastramento = 0;

    /**
     * keeps track of state for field:
     * _cdMidiaMensagemRecadastramento
     */
    private boolean _has_cdMidiaMensagemRecadastramento;

    /**
     * Field _dsMidiaMensagemRecadastramento
     */
    private java.lang.String _dsMidiaMensagemRecadastramento;

    /**
     * Field _cdMomentoAvisoRecadastramento
     */
    private int _cdMomentoAvisoRecadastramento = 0;

    /**
     * keeps track of state for field: _cdMomentoAvisoRecadastrament
     */
    private boolean _has_cdMomentoAvisoRecadastramento;

    /**
     * Field _dsMomentoAvisoRecadastramento
     */
    private java.lang.String _dsMomentoAvisoRecadastramento;

    /**
     * Field _cdMomentoCreditoEfetivacao
     */
    private int _cdMomentoCreditoEfetivacao = 0;

    /**
     * keeps track of state for field: _cdMomentoCreditoEfetivacao
     */
    private boolean _has_cdMomentoCreditoEfetivacao;

    /**
     * Field _dsMomentoCreditoEfetivacao
     */
    private java.lang.String _dsMomentoCreditoEfetivacao;

    /**
     * Field _cdMomentoDebitoPagamento
     */
    private int _cdMomentoDebitoPagamento = 0;

    /**
     * keeps track of state for field: _cdMomentoDebitoPagamento
     */
    private boolean _has_cdMomentoDebitoPagamento;

    /**
     * Field _dsMomentoDebitoPagamento
     */
    private java.lang.String _dsMomentoDebitoPagamento;

    /**
     * Field _cdMomentoFormularioRecadastramento
     */
    private int _cdMomentoFormularioRecadastramento = 0;

    /**
     * keeps track of state for field:
     * _cdMomentoFormularioRecadastramento
     */
    private boolean _has_cdMomentoFormularioRecadastramento;

    /**
     * Field _dsMomentoFormularioRecadastramento
     */
    private java.lang.String _dsMomentoFormularioRecadastramento;

    /**
     * Field _cdMomentoProcessamentoPagamento
     */
    private int _cdMomentoProcessamentoPagamento = 0;

    /**
     * keeps track of state for field:
     * _cdMomentoProcessamentoPagamento
     */
    private boolean _has_cdMomentoProcessamentoPagamento;

    /**
     * Field _dsMomentoProcessamentoPagamento
     */
    private java.lang.String _dsMomentoProcessamentoPagamento;

    /**
     * Field _cdMensagemRecadastramentoMidia
     */
    private int _cdMensagemRecadastramentoMidia = 0;

    /**
     * keeps track of state for field:
     * _cdMensagemRecadastramentoMidia
     */
    private boolean _has_cdMensagemRecadastramentoMidia;

    /**
     * Field _dsMensagemRecadastramentoMidia
     */
    private java.lang.String _dsMensagemRecadastramentoMidia;

    /**
     * Field _cdPermissaoDebitoOnline
     */
    private int _cdPermissaoDebitoOnline = 0;

    /**
     * keeps track of state for field: _cdPermissaoDebitoOnline
     */
    private boolean _has_cdPermissaoDebitoOnline;

    /**
     * Field _dsPermissaoDebitoOnline
     */
    private java.lang.String _dsPermissaoDebitoOnline;

    /**
     * Field _cdNaturezaOperacaoPagamento
     */
    private int _cdNaturezaOperacaoPagamento = 0;

    /**
     * keeps track of state for field: _cdNaturezaOperacaoPagamento
     */
    private boolean _has_cdNaturezaOperacaoPagamento;

    /**
     * Field _dsNaturezaOperacaoPagamento
     */
    private java.lang.String _dsNaturezaOperacaoPagamento;

    /**
     * Field _cdPeriodicidadeAviso
     */
    private int _cdPeriodicidadeAviso = 0;

    /**
     * keeps track of state for field: _cdPeriodicidadeAviso
     */
    private boolean _has_cdPeriodicidadeAviso;

    /**
     * Field _dsPeriodicidadeAviso
     */
    private java.lang.String _dsPeriodicidadeAviso;

    /**
     * Field _cdPeriodicidadeCobrancaTarifa
     */
    private int _cdPeriodicidadeCobrancaTarifa = 0;

    /**
     * keeps track of state for field: _cdPeriodicidadeCobrancaTarif
     */
    private boolean _has_cdPeriodicidadeCobrancaTarifa;

    /**
     * Field _dsPeriodicidadeCobrancaTarifa
     */
    private java.lang.String _dsPeriodicidadeCobrancaTarifa;

    /**
     * Field _cdPeriodicidadeComprovante
     */
    private int _cdPeriodicidadeComprovante = 0;

    /**
     * keeps track of state for field: _cdPeriodicidadeComprovante
     */
    private boolean _has_cdPeriodicidadeComprovante;

    /**
     * Field _dsPeriodicidadeComprovante
     */
    private java.lang.String _dsPeriodicidadeComprovante;

    /**
     * Field _cdPeriodicidadeConsultaVeiculo
     */
    private int _cdPeriodicidadeConsultaVeiculo = 0;

    /**
     * keeps track of state for field:
     * _cdPeriodicidadeConsultaVeiculo
     */
    private boolean _has_cdPeriodicidadeConsultaVeiculo;

    /**
     * Field _dsPeriodicidadeConsultaVeiculo
     */
    private java.lang.String _dsPeriodicidadeConsultaVeiculo;

    /**
     * Field _cdPeriodicidadeEnvioRemessa
     */
    private int _cdPeriodicidadeEnvioRemessa = 0;

    /**
     * keeps track of state for field: _cdPeriodicidadeEnvioRemessa
     */
    private boolean _has_cdPeriodicidadeEnvioRemessa;

    /**
     * Field _dsPeriodicidadeEnvioRemessa
     */
    private java.lang.String _dsPeriodicidadeEnvioRemessa;

    /**
     * Field _cdPeriodicidadeManutencaoProcd
     */
    private int _cdPeriodicidadeManutencaoProcd = 0;

    /**
     * keeps track of state for field:
     * _cdPeriodicidadeManutencaoProcd
     */
    private boolean _has_cdPeriodicidadeManutencaoProcd;

    /**
     * Field _dsPeriodicidadeManutencaoProcd
     */
    private java.lang.String _dsPeriodicidadeManutencaoProcd;

    /**
     * Field _cdPagamentoNaoUtil
     */
    private int _cdPagamentoNaoUtil = 0;

    /**
     * keeps track of state for field: _cdPagamentoNaoUtil
     */
    private boolean _has_cdPagamentoNaoUtil;

    /**
     * Field _dsPagamentoNaoUtil
     */
    private java.lang.String _dsPagamentoNaoUtil;

    /**
     * Field _cdPrincipalEnquaRecadastramento
     */
    private int _cdPrincipalEnquaRecadastramento = 0;

    /**
     * keeps track of state for field:
     * _cdPrincipalEnquaRecadastramento
     */
    private boolean _has_cdPrincipalEnquaRecadastramento;

    /**
     * Field _dsPrincipalEnquaRecadastramento
     */
    private java.lang.String _dsPrincipalEnquaRecadastramento;

    /**
     * Field _cdPrioridadeEfetivacaoPagamento
     */
    private int _cdPrioridadeEfetivacaoPagamento = 0;

    /**
     * keeps track of state for field:
     * _cdPrioridadeEfetivacaoPagamento
     */
    private boolean _has_cdPrioridadeEfetivacaoPagamento;

    /**
     * Field _dsPrioridadeEfetivacaoPagamento
     */
    private java.lang.String _dsPrioridadeEfetivacaoPagamento;

    /**
     * Field _cdRejeicaoAgendamentoLote
     */
    private int _cdRejeicaoAgendamentoLote = 0;

    /**
     * keeps track of state for field: _cdRejeicaoAgendamentoLote
     */
    private boolean _has_cdRejeicaoAgendamentoLote;

    /**
     * Field _dsRejeicaoAgendamentoLote
     */
    private java.lang.String _dsRejeicaoAgendamentoLote;

    /**
     * Field _cdRejeicaoEfetivacaoLote
     */
    private int _cdRejeicaoEfetivacaoLote = 0;

    /**
     * keeps track of state for field: _cdRejeicaoEfetivacaoLote
     */
    private boolean _has_cdRejeicaoEfetivacaoLote;

    /**
     * Field _dsRejeicaoEfetivacaoLote
     */
    private java.lang.String _dsRejeicaoEfetivacaoLote;

    /**
     * Field _cdRejeicaoLote
     */
    private int _cdRejeicaoLote = 0;

    /**
     * keeps track of state for field: _cdRejeicaoLote
     */
    private boolean _has_cdRejeicaoLote;

    /**
     * Field _dsRejeicaoLote
     */
    private java.lang.String _dsRejeicaoLote;

    /**
     * Field _cdRastreabNotaFiscal
     */
    private int _cdRastreabNotaFiscal = 0;

    /**
     * keeps track of state for field: _cdRastreabNotaFiscal
     */
    private boolean _has_cdRastreabNotaFiscal;

    /**
     * Field _dsRastreabilidadeNotaFiscal
     */
    private java.lang.String _dsRastreabilidadeNotaFiscal;

    /**
     * Field _cdRastreabTituloTerc
     */
    private int _cdRastreabTituloTerc = 0;

    /**
     * keeps track of state for field: _cdRastreabTituloTerc
     */
    private boolean _has_cdRastreabTituloTerc;

    /**
     * Field _dsRastreabilidadeTituloTerceiro
     */
    private java.lang.String _dsRastreabilidadeTituloTerceiro;

    /**
     * Field _cdTipoCargaRecadastramento
     */
    private int _cdTipoCargaRecadastramento = 0;

    /**
     * keeps track of state for field: _cdTipoCargaRecadastramento
     */
    private boolean _has_cdTipoCargaRecadastramento;

    /**
     * Field _dsTipoCargaRecadastramento
     */
    private java.lang.String _dsTipoCargaRecadastramento;

    /**
     * Field _cdTipoCartaoSalario
     */
    private int _cdTipoCartaoSalario = 0;

    /**
     * keeps track of state for field: _cdTipoCartaoSalario
     */
    private boolean _has_cdTipoCartaoSalario;

    /**
     * Field _dsTipoCartaoSalario
     */
    private java.lang.String _dsTipoCartaoSalario;

    /**
     * Field _cdTipoDataFloating
     */
    private int _cdTipoDataFloating = 0;

    /**
     * keeps track of state for field: _cdTipoDataFloating
     */
    private boolean _has_cdTipoDataFloating;

    /**
     * Field _dsTipoDataFloating
     */
    private java.lang.String _dsTipoDataFloating;

    /**
     * Field _cdTipoDivergenciaVeiculo
     */
    private int _cdTipoDivergenciaVeiculo = 0;

    /**
     * keeps track of state for field: _cdTipoDivergenciaVeiculo
     */
    private boolean _has_cdTipoDivergenciaVeiculo;

    /**
     * Field _dsTipoDivergenciaVeiculo
     */
    private java.lang.String _dsTipoDivergenciaVeiculo;

    /**
     * Field _cdTipoIdentificacaoBeneficio
     */
    private int _cdTipoIdentificacaoBeneficio = 0;

    /**
     * keeps track of state for field: _cdTipoIdentificacaoBeneficio
     */
    private boolean _has_cdTipoIdentificacaoBeneficio;

    /**
     * Field _dsTipoIdentificacaoBeneficio
     */
    private java.lang.String _dsTipoIdentificacaoBeneficio;

    /**
     * Field _cdTipoLayoutArquivo
     */
    private int _cdTipoLayoutArquivo = 0;

    /**
     * keeps track of state for field: _cdTipoLayoutArquivo
     */
    private boolean _has_cdTipoLayoutArquivo;

    /**
     * Field _dsTipoLayoutArquivo
     */
    private java.lang.String _dsTipoLayoutArquivo;

    /**
     * Field _cdTipoReajusteTarifa
     */
    private int _cdTipoReajusteTarifa = 0;

    /**
     * keeps track of state for field: _cdTipoReajusteTarifa
     */
    private boolean _has_cdTipoReajusteTarifa;

    /**
     * Field _dsTipoReajusteTarifa
     */
    private java.lang.String _dsTipoReajusteTarifa;

    /**
     * Field _cdTratoContaTransf
     */
    private int _cdTratoContaTransf = 0;

    /**
     * keeps track of state for field: _cdTratoContaTransf
     */
    private boolean _has_cdTratoContaTransf;

    /**
     * Field _dsTratamentoContaTransferida
     */
    private java.lang.String _dsTratamentoContaTransferida;

    /**
     * Field _cdUtilizacaoFavorecidoControle
     */
    private int _cdUtilizacaoFavorecidoControle = 0;

    /**
     * keeps track of state for field:
     * _cdUtilizacaoFavorecidoControle
     */
    private boolean _has_cdUtilizacaoFavorecidoControle;

    /**
     * Field _dsUtilizacaoFavorecidoControle
     */
    private java.lang.String _dsUtilizacaoFavorecidoControle;

    /**
     * Field _dtEnquaContaSalario
     */
    private java.lang.String _dtEnquaContaSalario;

    /**
     * Field _dtInicioBloqueioPplta
     */
    private java.lang.String _dtInicioBloqueioPplta;

    /**
     * Field _dtInicioRastreabTitulo
     */
    private java.lang.String _dtInicioRastreabTitulo;

    /**
     * Field _dtFimAcertoRecadastramento
     */
    private java.lang.String _dtFimAcertoRecadastramento;

    /**
     * Field _dtFimRecadastramentoBeneficio
     */
    private java.lang.String _dtFimRecadastramentoBeneficio;

    /**
     * Field _dtInicioAcertoRecadastramento
     */
    private java.lang.String _dtInicioAcertoRecadastramento;

    /**
     * Field _dtInicioRecadastramentoBeneficio
     */
    private java.lang.String _dtInicioRecadastramentoBeneficio;

    /**
     * Field _dtLimiteVinculoCarga
     */
    private java.lang.String _dtLimiteVinculoCarga;

    /**
     * Field _nrFechamentoApuracaoTarifa
     */
    private int _nrFechamentoApuracaoTarifa = 0;

    /**
     * keeps track of state for field: _nrFechamentoApuracaoTarifa
     */
    private boolean _has_nrFechamentoApuracaoTarifa;

    /**
     * Field _percentualIndiceReajusteTarifa
     */
    private java.math.BigDecimal _percentualIndiceReajusteTarifa = new java.math.BigDecimal("0");

    /**
     * Field _percentualMaximoInconsistenteLote
     */
    private int _percentualMaximoInconsistenteLote = 0;

    /**
     * keeps track of state for field:
     * _percentualMaximoInconsistenteLote
     */
    private boolean _has_percentualMaximoInconsistenteLote;

    /**
     * Field _periodicidadeTarifaCatalogo
     */
    private java.math.BigDecimal _periodicidadeTarifaCatalogo = new java.math.BigDecimal("0");

    /**
     * Field _quantidadeAntecedencia
     */
    private int _quantidadeAntecedencia = 0;

    /**
     * keeps track of state for field: _quantidadeAntecedencia
     */
    private boolean _has_quantidadeAntecedencia;

    /**
     * Field _quantidadeAnteriorVencimentoComprovante
     */
    private int _quantidadeAnteriorVencimentoComprovante = 0;

    /**
     * keeps track of state for field:
     * _quantidadeAnteriorVencimentoComprovante
     */
    private boolean _has_quantidadeAnteriorVencimentoComprovante;

    /**
     * Field _quantidadeDiaCobrancaTarifa
     */
    private int _quantidadeDiaCobrancaTarifa = 0;

    /**
     * keeps track of state for field: _quantidadeDiaCobrancaTarifa
     */
    private boolean _has_quantidadeDiaCobrancaTarifa;

    /**
     * Field _quantidadeDiaExpiracao
     */
    private int _quantidadeDiaExpiracao = 0;

    /**
     * keeps track of state for field: _quantidadeDiaExpiracao
     */
    private boolean _has_quantidadeDiaExpiracao;

    /**
     * Field _quantidadeDiaFloatingPagamento
     */
    private int _quantidadeDiaFloatingPagamento = 0;

    /**
     * keeps track of state for field:
     * _quantidadeDiaFloatingPagamento
     */
    private boolean _has_quantidadeDiaFloatingPagamento;

    /**
     * Field _quantidadeDiaInatividadeFavorecido
     */
    private int _quantidadeDiaInatividadeFavorecido = 0;

    /**
     * keeps track of state for field:
     * _quantidadeDiaInatividadeFavorecido
     */
    private boolean _has_quantidadeDiaInatividadeFavorecido;

    /**
     * Field _quantidadeDiaRepiqueConsulta
     */
    private int _quantidadeDiaRepiqueConsulta = 0;

    /**
     * keeps track of state for field: _quantidadeDiaRepiqueConsulta
     */
    private boolean _has_quantidadeDiaRepiqueConsulta;

    /**
     * Field _quantidadeEtapaRecadastramentoBeneficio
     */
    private int _quantidadeEtapaRecadastramentoBeneficio = 0;

    /**
     * keeps track of state for field:
     * _quantidadeEtapaRecadastramentoBeneficio
     */
    private boolean _has_quantidadeEtapaRecadastramentoBeneficio;

    /**
     * Field _quantidadeFaseRecadastramentoBeneficio
     */
    private int _quantidadeFaseRecadastramentoBeneficio = 0;

    /**
     * keeps track of state for field:
     * _quantidadeFaseRecadastramentoBeneficio
     */
    private boolean _has_quantidadeFaseRecadastramentoBeneficio;

    /**
     * Field _quantidadeLimiteLinha
     */
    private int _quantidadeLimiteLinha = 0;

    /**
     * keeps track of state for field: _quantidadeLimiteLinha
     */
    private boolean _has_quantidadeLimiteLinha;

    /**
     * Field _quantidadeSolicitacaoCartao
     */
    private int _quantidadeSolicitacaoCartao = 0;

    /**
     * keeps track of state for field: _quantidadeSolicitacaoCartao
     */
    private boolean _has_quantidadeSolicitacaoCartao;

    /**
     * Field _quantidadeMaximaInconsistenteLote
     */
    private int _quantidadeMaximaInconsistenteLote = 0;

    /**
     * keeps track of state for field:
     * _quantidadeMaximaInconsistenteLote
     */
    private boolean _has_quantidadeMaximaInconsistenteLote;

    /**
     * Field _quantidadeMaximaTituloVencido
     */
    private int _quantidadeMaximaTituloVencido = 0;

    /**
     * keeps track of state for field: _quantidadeMaximaTituloVencid
     */
    private boolean _has_quantidadeMaximaTituloVencido;

    /**
     * Field _quantidadeMesComprovante
     */
    private int _quantidadeMesComprovante = 0;

    /**
     * keeps track of state for field: _quantidadeMesComprovante
     */
    private boolean _has_quantidadeMesComprovante;

    /**
     * Field _quantidadeMesEtapaRecadastramento
     */
    private int _quantidadeMesEtapaRecadastramento = 0;

    /**
     * keeps track of state for field:
     * _quantidadeMesEtapaRecadastramento
     */
    private boolean _has_quantidadeMesEtapaRecadastramento;

    /**
     * Field _quantidadeMesFaseRecadastramento
     */
    private int _quantidadeMesFaseRecadastramento = 0;

    /**
     * keeps track of state for field:
     * _quantidadeMesFaseRecadastramento
     */
    private boolean _has_quantidadeMesFaseRecadastramento;

    /**
     * Field _quantidadeMesReajusteTarifa
     */
    private int _quantidadeMesReajusteTarifa = 0;

    /**
     * keeps track of state for field: _quantidadeMesReajusteTarifa
     */
    private boolean _has_quantidadeMesReajusteTarifa;

    /**
     * Field _quantidadeViaAviso
     */
    private int _quantidadeViaAviso = 0;

    /**
     * keeps track of state for field: _quantidadeViaAviso
     */
    private boolean _has_quantidadeViaAviso;

    /**
     * Field _quantidadeViaCombranca
     */
    private int _quantidadeViaCombranca = 0;

    /**
     * keeps track of state for field: _quantidadeViaCombranca
     */
    private boolean _has_quantidadeViaCombranca;

    /**
     * Field _quantidadeViaComprovante
     */
    private int _quantidadeViaComprovante = 0;

    /**
     * keeps track of state for field: _quantidadeViaComprovante
     */
    private boolean _has_quantidadeViaComprovante;

    /**
     * Field _valorFavorecidoNaoCadastrado
     */
    private java.math.BigDecimal _valorFavorecidoNaoCadastrado = new java.math.BigDecimal("0");

    /**
     * Field _valorLimiteDiarioPagamento
     */
    private java.math.BigDecimal _valorLimiteDiarioPagamento = new java.math.BigDecimal("0");

    /**
     * Field _valorLimiteIndividualPagamento
     */
    private java.math.BigDecimal _valorLimiteIndividualPagamento = new java.math.BigDecimal("0");

    /**
     * Field _cdIndicadorEmissaoAviso
     */
    private int _cdIndicadorEmissaoAviso = 0;

    /**
     * keeps track of state for field: _cdIndicadorEmissaoAviso
     */
    private boolean _has_cdIndicadorEmissaoAviso;

    /**
     * Field _dsIndicadorEmissaoAviso
     */
    private java.lang.String _dsIndicadorEmissaoAviso;

    /**
     * Field _cdIndicadorRetornoInternet
     */
    private int _cdIndicadorRetornoInternet = 0;

    /**
     * keeps track of state for field: _cdIndicadorRetornoInternet
     */
    private boolean _has_cdIndicadorRetornoInternet;

    /**
     * Field _dsIndicadorRetornoInternet
     */
    private java.lang.String _dsIndicadorRetornoInternet;

    /**
     * Field _cdIndicadorRetornoSeparado
     */
    private int _cdIndicadorRetornoSeparado = 0;

    /**
     * keeps track of state for field: _cdIndicadorRetornoSeparado
     */
    private boolean _has_cdIndicadorRetornoSeparado;

    /**
     * Field _dsCodigoIndicadorRetornoSeparado
     */
    private java.lang.String _dsCodigoIndicadorRetornoSeparado;

    /**
     * Field _cdIndicadorListaDebito
     */
    private int _cdIndicadorListaDebito = 0;

    /**
     * keeps track of state for field: _cdIndicadorListaDebito
     */
    private boolean _has_cdIndicadorListaDebito;

    /**
     * Field _dsIndicadorListaDebito
     */
    private java.lang.String _dsIndicadorListaDebito;

    /**
     * Field _cdTipoFormacaoLista
     */
    private int _cdTipoFormacaoLista = 0;

    /**
     * keeps track of state for field: _cdTipoFormacaoLista
     */
    private boolean _has_cdTipoFormacaoLista;

    /**
     * Field _dsTipoFormacaoLista
     */
    private java.lang.String _dsTipoFormacaoLista;

    /**
     * Field _cdTipoConsistenciaLista
     */
    private int _cdTipoConsistenciaLista = 0;

    /**
     * keeps track of state for field: _cdTipoConsistenciaLista
     */
    private boolean _has_cdTipoConsistenciaLista;

    /**
     * Field _dsTipoConsistenciaLista
     */
    private java.lang.String _dsTipoConsistenciaLista;

    /**
     * Field _cdTipoConsultaComprovante
     */
    private int _cdTipoConsultaComprovante = 0;

    /**
     * keeps track of state for field: _cdTipoConsultaComprovante
     */
    private boolean _has_cdTipoConsultaComprovante;

    /**
     * Field _dsTipoConsolidacaoComprovante
     */
    private java.lang.String _dsTipoConsolidacaoComprovante;

    /**
     * Field _cdValidacaoNomeFavorecido
     */
    private int _cdValidacaoNomeFavorecido = 0;

    /**
     * keeps track of state for field: _cdValidacaoNomeFavorecido
     */
    private boolean _has_cdValidacaoNomeFavorecido;

    /**
     * Field _dsValidacaoNomeFavorecido
     */
    private java.lang.String _dsValidacaoNomeFavorecido;

    /**
     * Field _cdTipoConsistenciaFavorecido
     */
    private int _cdTipoConsistenciaFavorecido = 0;

    /**
     * keeps track of state for field: _cdTipoConsistenciaFavorecido
     */
    private boolean _has_cdTipoConsistenciaFavorecido;

    /**
     * Field _dsTipoConsistenciaFavorecido
     */
    private java.lang.String _dsTipoConsistenciaFavorecido;

    /**
     * Field _cdIndLancamentoPersonalizado
     */
    private int _cdIndLancamentoPersonalizado = 0;

    /**
     * keeps track of state for field: _cdIndLancamentoPersonalizado
     */
    private boolean _has_cdIndLancamentoPersonalizado;

    /**
     * Field _dsIndLancamentoPersonalizado
     */
    private java.lang.String _dsIndLancamentoPersonalizado;

    /**
     * Field _cdAgendaRastreabilidadeFilial
     */
    private int _cdAgendaRastreabilidadeFilial = 0;

    /**
     * keeps track of state for field: _cdAgendaRastreabilidadeFilia
     */
    private boolean _has_cdAgendaRastreabilidadeFilial;

    /**
     * Field _dsAgendamentoRastFilial
     */
    private java.lang.String _dsAgendamentoRastFilial;

    /**
     * Field _cdIndicadorAdesaoSacador
     */
    private int _cdIndicadorAdesaoSacador = 0;

    /**
     * keeps track of state for field: _cdIndicadorAdesaoSacador
     */
    private boolean _has_cdIndicadorAdesaoSacador;

    /**
     * Field _dsIndicadorAdesaoSacador
     */
    private java.lang.String _dsIndicadorAdesaoSacador;

    /**
     * Field _cdMeioPagamentoCredito
     */
    private int _cdMeioPagamentoCredito = 0;

    /**
     * keeps track of state for field: _cdMeioPagamentoCredito
     */
    private boolean _has_cdMeioPagamentoCredito;

    /**
     * Field _dsMeioPagamentoCredito
     */
    private java.lang.String _dsMeioPagamentoCredito;

    /**
     * Field _cdTipoInscricaoFavorecidoAceita
     */
    private int _cdTipoInscricaoFavorecidoAceita = 0;

    /**
     * keeps track of state for field:
     * _cdTipoInscricaoFavorecidoAceita
     */
    private boolean _has_cdTipoInscricaoFavorecidoAceita;

    /**
     * Field _dsTipoIscricaoFavorecido
     */
    private java.lang.String _dsTipoIscricaoFavorecido;

    /**
     * Field _cdIndicadorBancoPostal
     */
    private int _cdIndicadorBancoPostal = 0;

    /**
     * keeps track of state for field: _cdIndicadorBancoPostal
     */
    private boolean _has_cdIndicadorBancoPostal;

    /**
     * Field _dsIndicadorBancoPostal
     */
    private java.lang.String _dsIndicadorBancoPostal;

    /**
     * Field _cdFormularioContratoCliente
     */
    private int _cdFormularioContratoCliente = 0;

    /**
     * keeps track of state for field: _cdFormularioContratoCliente
     */
    private boolean _has_cdFormularioContratoCliente;

    /**
     * Field _dsCodigoFormularioContratoCliente
     */
    private java.lang.String _dsCodigoFormularioContratoCliente;

    /**
     * Field _dsLocalEmissao
     */
    private java.lang.String _dsLocalEmissao;

    /**
     * Field _cdUsuarioInclusao
     */
    private java.lang.String _cdUsuarioInclusao;

    /**
     * Field _cdUsuarioExternoInclusao
     */
    private java.lang.String _cdUsuarioExternoInclusao;

    /**
     * Field _hrManutencaoRegistroInclusao
     */
    private java.lang.String _hrManutencaoRegistroInclusao;

    /**
     * Field _cdCanalInclusao
     */
    private int _cdCanalInclusao = 0;

    /**
     * keeps track of state for field: _cdCanalInclusao
     */
    private boolean _has_cdCanalInclusao;

    /**
     * Field _dsCanalInclusao
     */
    private java.lang.String _dsCanalInclusao;

    /**
     * Field _nrOperacaoFluxoInclusao
     */
    private java.lang.String _nrOperacaoFluxoInclusao;

    /**
     * Field _cdUsuarioAlteracao
     */
    private java.lang.String _cdUsuarioAlteracao;

    /**
     * Field _cdUsuarioExternoAlteracao
     */
    private java.lang.String _cdUsuarioExternoAlteracao;

    /**
     * Field _hrManutencaoRegistroAlteracao
     */
    private java.lang.String _hrManutencaoRegistroAlteracao;

    /**
     * Field _cdCanalAlteracao
     */
    private int _cdCanalAlteracao = 0;

    /**
     * keeps track of state for field: _cdCanalAlteracao
     */
    private boolean _has_cdCanalAlteracao;

    /**
     * Field _dsCanalAlteracao
     */
    private java.lang.String _dsCanalAlteracao;

    /**
     * Field _nrOperacaoFluxoAlteracao
     */
    private java.lang.String _nrOperacaoFluxoAlteracao;

    /**
     * Field _cdAmbienteServicoContrato
     */
    private java.lang.String _cdAmbienteServicoContrato;

    /**
     * Field _dsAreaReservada2
     */
    private java.lang.String _dsAreaReservada2;

    /**
     * Field _cdConsultaSaldoValorSuperior
     */
    private int _cdConsultaSaldoValorSuperior = 0;

    /**
     * keeps track of state for field: _cdConsultaSaldoValorSuperior
     */
    private boolean _has_cdConsultaSaldoValorSuperior;

    /**
     * Field _dsConsultaSaldoSuperior
     */
    private java.lang.String _dsConsultaSaldoSuperior;


      //----------------/
     //- Constructors -/
    //----------------/

    public ConsultarConfigTipoServicoModalidadeResponse() 
     {
        super();
        setPercentualIndiceReajusteTarifa(new java.math.BigDecimal("0"));
        setPeriodicidadeTarifaCatalogo(new java.math.BigDecimal("0"));
        setValorFavorecidoNaoCadastrado(new java.math.BigDecimal("0"));
        setValorLimiteDiarioPagamento(new java.math.BigDecimal("0"));
        setValorLimiteIndividualPagamento(new java.math.BigDecimal("0"));
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarconfigtiposervicomodalidade.response.ConsultarConfigTipoServicoModalidadeResponse()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdAcaoNaoVida
     * 
     */
    public void deleteCdAcaoNaoVida()
    {
        this._has_cdAcaoNaoVida= false;
    } //-- void deleteCdAcaoNaoVida() 

    /**
     * Method deleteCdAcertoDadosRecadastramento
     * 
     */
    public void deleteCdAcertoDadosRecadastramento()
    {
        this._has_cdAcertoDadosRecadastramento= false;
    } //-- void deleteCdAcertoDadosRecadastramento() 

    /**
     * Method deleteCdAgendaRastreabilidadeFilial
     * 
     */
    public void deleteCdAgendaRastreabilidadeFilial()
    {
        this._has_cdAgendaRastreabilidadeFilial= false;
    } //-- void deleteCdAgendaRastreabilidadeFilial() 

    /**
     * Method deleteCdAgendamentoDebitoVeiculo
     * 
     */
    public void deleteCdAgendamentoDebitoVeiculo()
    {
        this._has_cdAgendamentoDebitoVeiculo= false;
    } //-- void deleteCdAgendamentoDebitoVeiculo() 

    /**
     * Method deleteCdAgendamentoPagamentoVencido
     * 
     */
    public void deleteCdAgendamentoPagamentoVencido()
    {
        this._has_cdAgendamentoPagamentoVencido= false;
    } //-- void deleteCdAgendamentoPagamentoVencido() 

    /**
     * Method deleteCdAgendamentoValorMenor
     * 
     */
    public void deleteCdAgendamentoValorMenor()
    {
        this._has_cdAgendamentoValorMenor= false;
    } //-- void deleteCdAgendamentoValorMenor() 

    /**
     * Method deleteCdAgrupamentoAviso
     * 
     */
    public void deleteCdAgrupamentoAviso()
    {
        this._has_cdAgrupamentoAviso= false;
    } //-- void deleteCdAgrupamentoAviso() 

    /**
     * Method deleteCdAgrupamentoComprovante
     * 
     */
    public void deleteCdAgrupamentoComprovante()
    {
        this._has_cdAgrupamentoComprovante= false;
    } //-- void deleteCdAgrupamentoComprovante() 

    /**
     * Method deleteCdAgrupamentoFormularioRecadastro
     * 
     */
    public void deleteCdAgrupamentoFormularioRecadastro()
    {
        this._has_cdAgrupamentoFormularioRecadastro= false;
    } //-- void deleteCdAgrupamentoFormularioRecadastro() 

    /**
     * Method deleteCdAntecipacaoRecadastramentoBeneficiario
     * 
     */
    public void deleteCdAntecipacaoRecadastramentoBeneficiario()
    {
        this._has_cdAntecipacaoRecadastramentoBeneficiario= false;
    } //-- void deleteCdAntecipacaoRecadastramentoBeneficiario() 

    /**
     * Method deleteCdAreaReservada
     * 
     */
    public void deleteCdAreaReservada()
    {
        this._has_cdAreaReservada= false;
    } //-- void deleteCdAreaReservada() 

    /**
     * Method deleteCdBaseRecadastramentoBeneficio
     * 
     */
    public void deleteCdBaseRecadastramentoBeneficio()
    {
        this._has_cdBaseRecadastramentoBeneficio= false;
    } //-- void deleteCdBaseRecadastramentoBeneficio() 

    /**
     * Method deleteCdBloqueioEmissaoPplta
     * 
     */
    public void deleteCdBloqueioEmissaoPplta()
    {
        this._has_cdBloqueioEmissaoPplta= false;
    } //-- void deleteCdBloqueioEmissaoPplta() 

    /**
     * Method deleteCdCanalAlteracao
     * 
     */
    public void deleteCdCanalAlteracao()
    {
        this._has_cdCanalAlteracao= false;
    } //-- void deleteCdCanalAlteracao() 

    /**
     * Method deleteCdCanalInclusao
     * 
     */
    public void deleteCdCanalInclusao()
    {
        this._has_cdCanalInclusao= false;
    } //-- void deleteCdCanalInclusao() 

    /**
     * Method deleteCdCaptuTituloRegistro
     * 
     */
    public void deleteCdCaptuTituloRegistro()
    {
        this._has_cdCaptuTituloRegistro= false;
    } //-- void deleteCdCaptuTituloRegistro() 

    /**
     * Method deleteCdCctciaEspeBeneficio
     * 
     */
    public void deleteCdCctciaEspeBeneficio()
    {
        this._has_cdCctciaEspeBeneficio= false;
    } //-- void deleteCdCctciaEspeBeneficio() 

    /**
     * Method deleteCdCctciaIdentificacaoBeneficio
     * 
     */
    public void deleteCdCctciaIdentificacaoBeneficio()
    {
        this._has_cdCctciaIdentificacaoBeneficio= false;
    } //-- void deleteCdCctciaIdentificacaoBeneficio() 

    /**
     * Method deleteCdCctciaInscricaoFavorecido
     * 
     */
    public void deleteCdCctciaInscricaoFavorecido()
    {
        this._has_cdCctciaInscricaoFavorecido= false;
    } //-- void deleteCdCctciaInscricaoFavorecido() 

    /**
     * Method deleteCdCctciaProprietarioVeculo
     * 
     */
    public void deleteCdCctciaProprietarioVeculo()
    {
        this._has_cdCctciaProprietarioVeculo= false;
    } //-- void deleteCdCctciaProprietarioVeculo() 

    /**
     * Method deleteCdCobrancaTarifa
     * 
     */
    public void deleteCdCobrancaTarifa()
    {
        this._has_cdCobrancaTarifa= false;
    } //-- void deleteCdCobrancaTarifa() 

    /**
     * Method deleteCdConsultaDebitoVeiculo
     * 
     */
    public void deleteCdConsultaDebitoVeiculo()
    {
        this._has_cdConsultaDebitoVeiculo= false;
    } //-- void deleteCdConsultaDebitoVeiculo() 

    /**
     * Method deleteCdConsultaEndereco
     * 
     */
    public void deleteCdConsultaEndereco()
    {
        this._has_cdConsultaEndereco= false;
    } //-- void deleteCdConsultaEndereco() 

    /**
     * Method deleteCdConsultaSaldoPagamento
     * 
     */
    public void deleteCdConsultaSaldoPagamento()
    {
        this._has_cdConsultaSaldoPagamento= false;
    } //-- void deleteCdConsultaSaldoPagamento() 

    /**
     * Method deleteCdConsultaSaldoValorSuperior
     * 
     */
    public void deleteCdConsultaSaldoValorSuperior()
    {
        this._has_cdConsultaSaldoValorSuperior= false;
    } //-- void deleteCdConsultaSaldoValorSuperior() 

    /**
     * Method deleteCdContagemConsultaSaldo
     * 
     */
    public void deleteCdContagemConsultaSaldo()
    {
        this._has_cdContagemConsultaSaldo= false;
    } //-- void deleteCdContagemConsultaSaldo() 

    /**
     * Method deleteCdCreditoNaoUtilizado
     * 
     */
    public void deleteCdCreditoNaoUtilizado()
    {
        this._has_cdCreditoNaoUtilizado= false;
    } //-- void deleteCdCreditoNaoUtilizado() 

    /**
     * Method deleteCdCriterioEnquadraRecadastramento
     * 
     */
    public void deleteCdCriterioEnquadraRecadastramento()
    {
        this._has_cdCriterioEnquadraRecadastramento= false;
    } //-- void deleteCdCriterioEnquadraRecadastramento() 

    /**
     * Method deleteCdCriterioEnquandraBeneficio
     * 
     */
    public void deleteCdCriterioEnquandraBeneficio()
    {
        this._has_cdCriterioEnquandraBeneficio= false;
    } //-- void deleteCdCriterioEnquandraBeneficio() 

    /**
     * Method deleteCdCriterioRstrbTitulo
     * 
     */
    public void deleteCdCriterioRstrbTitulo()
    {
        this._has_cdCriterioRstrbTitulo= false;
    } //-- void deleteCdCriterioRstrbTitulo() 

    /**
     * Method deleteCdDestinoAviso
     * 
     */
    public void deleteCdDestinoAviso()
    {
        this._has_cdDestinoAviso= false;
    } //-- void deleteCdDestinoAviso() 

    /**
     * Method deleteCdDestinoComprovante
     * 
     */
    public void deleteCdDestinoComprovante()
    {
        this._has_cdDestinoComprovante= false;
    } //-- void deleteCdDestinoComprovante() 

    /**
     * Method deleteCdDestinoFormularioRecadastramento
     * 
     */
    public void deleteCdDestinoFormularioRecadastramento()
    {
        this._has_cdDestinoFormularioRecadastramento= false;
    } //-- void deleteCdDestinoFormularioRecadastramento() 

    /**
     * Method deleteCdDisponibilizacaoContaCredito
     * 
     */
    public void deleteCdDisponibilizacaoContaCredito()
    {
        this._has_cdDisponibilizacaoContaCredito= false;
    } //-- void deleteCdDisponibilizacaoContaCredito() 

    /**
     * Method deleteCdDisponibilizacaoDiversoCriterio
     * 
     */
    public void deleteCdDisponibilizacaoDiversoCriterio()
    {
        this._has_cdDisponibilizacaoDiversoCriterio= false;
    } //-- void deleteCdDisponibilizacaoDiversoCriterio() 

    /**
     * Method deleteCdDisponibilizacaoDiversoNao
     * 
     */
    public void deleteCdDisponibilizacaoDiversoNao()
    {
        this._has_cdDisponibilizacaoDiversoNao= false;
    } //-- void deleteCdDisponibilizacaoDiversoNao() 

    /**
     * Method deleteCdDisponibilizacaoSalarioCriterio
     * 
     */
    public void deleteCdDisponibilizacaoSalarioCriterio()
    {
        this._has_cdDisponibilizacaoSalarioCriterio= false;
    } //-- void deleteCdDisponibilizacaoSalarioCriterio() 

    /**
     * Method deleteCdDisponibilizacaoSalarioNao
     * 
     */
    public void deleteCdDisponibilizacaoSalarioNao()
    {
        this._has_cdDisponibilizacaoSalarioNao= false;
    } //-- void deleteCdDisponibilizacaoSalarioNao() 

    /**
     * Method deleteCdEnvelopeAberto
     * 
     */
    public void deleteCdEnvelopeAberto()
    {
        this._has_cdEnvelopeAberto= false;
    } //-- void deleteCdEnvelopeAberto() 

    /**
     * Method deleteCdFavorecidoConsultaPagamento
     * 
     */
    public void deleteCdFavorecidoConsultaPagamento()
    {
        this._has_cdFavorecidoConsultaPagamento= false;
    } //-- void deleteCdFavorecidoConsultaPagamento() 

    /**
     * Method deleteCdFormaAutorizacaoPagamento
     * 
     */
    public void deleteCdFormaAutorizacaoPagamento()
    {
        this._has_cdFormaAutorizacaoPagamento= false;
    } //-- void deleteCdFormaAutorizacaoPagamento() 

    /**
     * Method deleteCdFormaEnvioPagamento
     * 
     */
    public void deleteCdFormaEnvioPagamento()
    {
        this._has_cdFormaEnvioPagamento= false;
    } //-- void deleteCdFormaEnvioPagamento() 

    /**
     * Method deleteCdFormaExpiracaoCredito
     * 
     */
    public void deleteCdFormaExpiracaoCredito()
    {
        this._has_cdFormaExpiracaoCredito= false;
    } //-- void deleteCdFormaExpiracaoCredito() 

    /**
     * Method deleteCdFormaManutencao
     * 
     */
    public void deleteCdFormaManutencao()
    {
        this._has_cdFormaManutencao= false;
    } //-- void deleteCdFormaManutencao() 

    /**
     * Method deleteCdFormularioContratoCliente
     * 
     */
    public void deleteCdFormularioContratoCliente()
    {
        this._has_cdFormularioContratoCliente= false;
    } //-- void deleteCdFormularioContratoCliente() 

    /**
     * Method deleteCdFrasePrecadastrada
     * 
     */
    public void deleteCdFrasePrecadastrada()
    {
        this._has_cdFrasePrecadastrada= false;
    } //-- void deleteCdFrasePrecadastrada() 

    /**
     * Method deleteCdIndLancamentoPersonalizado
     * 
     */
    public void deleteCdIndLancamentoPersonalizado()
    {
        this._has_cdIndLancamentoPersonalizado= false;
    } //-- void deleteCdIndLancamentoPersonalizado() 

    /**
     * Method deleteCdIndicadorAdesaoSacador
     * 
     */
    public void deleteCdIndicadorAdesaoSacador()
    {
        this._has_cdIndicadorAdesaoSacador= false;
    } //-- void deleteCdIndicadorAdesaoSacador() 

    /**
     * Method deleteCdIndicadorAgendaTitulo
     * 
     */
    public void deleteCdIndicadorAgendaTitulo()
    {
        this._has_cdIndicadorAgendaTitulo= false;
    } //-- void deleteCdIndicadorAgendaTitulo() 

    /**
     * Method deleteCdIndicadorAutorizacaoCliente
     * 
     */
    public void deleteCdIndicadorAutorizacaoCliente()
    {
        this._has_cdIndicadorAutorizacaoCliente= false;
    } //-- void deleteCdIndicadorAutorizacaoCliente() 

    /**
     * Method deleteCdIndicadorAutorizacaoComplemento
     * 
     */
    public void deleteCdIndicadorAutorizacaoComplemento()
    {
        this._has_cdIndicadorAutorizacaoComplemento= false;
    } //-- void deleteCdIndicadorAutorizacaoComplemento() 

    /**
     * Method deleteCdIndicadorBancoPostal
     * 
     */
    public void deleteCdIndicadorBancoPostal()
    {
        this._has_cdIndicadorBancoPostal= false;
    } //-- void deleteCdIndicadorBancoPostal() 

    /**
     * Method deleteCdIndicadorCadastroProcurador
     * 
     */
    public void deleteCdIndicadorCadastroProcurador()
    {
        this._has_cdIndicadorCadastroProcurador= false;
    } //-- void deleteCdIndicadorCadastroProcurador() 

    /**
     * Method deleteCdIndicadorCadastroorganizacao
     * 
     */
    public void deleteCdIndicadorCadastroorganizacao()
    {
        this._has_cdIndicadorCadastroorganizacao= false;
    } //-- void deleteCdIndicadorCadastroorganizacao() 

    /**
     * Method deleteCdIndicadorCartaoSalario
     * 
     */
    public void deleteCdIndicadorCartaoSalario()
    {
        this._has_cdIndicadorCartaoSalario= false;
    } //-- void deleteCdIndicadorCartaoSalario() 

    /**
     * Method deleteCdIndicadorEconomicoReajuste
     * 
     */
    public void deleteCdIndicadorEconomicoReajuste()
    {
        this._has_cdIndicadorEconomicoReajuste= false;
    } //-- void deleteCdIndicadorEconomicoReajuste() 

    /**
     * Method deleteCdIndicadorEmissaoAviso
     * 
     */
    public void deleteCdIndicadorEmissaoAviso()
    {
        this._has_cdIndicadorEmissaoAviso= false;
    } //-- void deleteCdIndicadorEmissaoAviso() 

    /**
     * Method deleteCdIndicadorListaDebito
     * 
     */
    public void deleteCdIndicadorListaDebito()
    {
        this._has_cdIndicadorListaDebito= false;
    } //-- void deleteCdIndicadorListaDebito() 

    /**
     * Method deleteCdIndicadorMensagemPersonalizada
     * 
     */
    public void deleteCdIndicadorMensagemPersonalizada()
    {
        this._has_cdIndicadorMensagemPersonalizada= false;
    } //-- void deleteCdIndicadorMensagemPersonalizada() 

    /**
     * Method deleteCdIndicadorRetornoInternet
     * 
     */
    public void deleteCdIndicadorRetornoInternet()
    {
        this._has_cdIndicadorRetornoInternet= false;
    } //-- void deleteCdIndicadorRetornoInternet() 

    /**
     * Method deleteCdIndicadorRetornoSeparado
     * 
     */
    public void deleteCdIndicadorRetornoSeparado()
    {
        this._has_cdIndicadorRetornoSeparado= false;
    } //-- void deleteCdIndicadorRetornoSeparado() 

    /**
     * Method deleteCdLancamentoFuturoCredito
     * 
     */
    public void deleteCdLancamentoFuturoCredito()
    {
        this._has_cdLancamentoFuturoCredito= false;
    } //-- void deleteCdLancamentoFuturoCredito() 

    /**
     * Method deleteCdLancamentoFuturoDebito
     * 
     */
    public void deleteCdLancamentoFuturoDebito()
    {
        this._has_cdLancamentoFuturoDebito= false;
    } //-- void deleteCdLancamentoFuturoDebito() 

    /**
     * Method deleteCdLiberacaoLoteProcs
     * 
     */
    public void deleteCdLiberacaoLoteProcs()
    {
        this._has_cdLiberacaoLoteProcs= false;
    } //-- void deleteCdLiberacaoLoteProcs() 

    /**
     * Method deleteCdManutencaoBaseRecadastramento
     * 
     */
    public void deleteCdManutencaoBaseRecadastramento()
    {
        this._has_cdManutencaoBaseRecadastramento= false;
    } //-- void deleteCdManutencaoBaseRecadastramento() 

    /**
     * Method deleteCdMeioPagamentoCredito
     * 
     */
    public void deleteCdMeioPagamentoCredito()
    {
        this._has_cdMeioPagamentoCredito= false;
    } //-- void deleteCdMeioPagamentoCredito() 

    /**
     * Method deleteCdMensagemRecadastramentoMidia
     * 
     */
    public void deleteCdMensagemRecadastramentoMidia()
    {
        this._has_cdMensagemRecadastramentoMidia= false;
    } //-- void deleteCdMensagemRecadastramentoMidia() 

    /**
     * Method deleteCdMidiaDisponivel
     * 
     */
    public void deleteCdMidiaDisponivel()
    {
        this._has_cdMidiaDisponivel= false;
    } //-- void deleteCdMidiaDisponivel() 

    /**
     * Method deleteCdMidiaMensagemRecadastramento
     * 
     */
    public void deleteCdMidiaMensagemRecadastramento()
    {
        this._has_cdMidiaMensagemRecadastramento= false;
    } //-- void deleteCdMidiaMensagemRecadastramento() 

    /**
     * Method deleteCdMomentoAvisoRecadastramento
     * 
     */
    public void deleteCdMomentoAvisoRecadastramento()
    {
        this._has_cdMomentoAvisoRecadastramento= false;
    } //-- void deleteCdMomentoAvisoRecadastramento() 

    /**
     * Method deleteCdMomentoCreditoEfetivacao
     * 
     */
    public void deleteCdMomentoCreditoEfetivacao()
    {
        this._has_cdMomentoCreditoEfetivacao= false;
    } //-- void deleteCdMomentoCreditoEfetivacao() 

    /**
     * Method deleteCdMomentoDebitoPagamento
     * 
     */
    public void deleteCdMomentoDebitoPagamento()
    {
        this._has_cdMomentoDebitoPagamento= false;
    } //-- void deleteCdMomentoDebitoPagamento() 

    /**
     * Method deleteCdMomentoFormularioRecadastramento
     * 
     */
    public void deleteCdMomentoFormularioRecadastramento()
    {
        this._has_cdMomentoFormularioRecadastramento= false;
    } //-- void deleteCdMomentoFormularioRecadastramento() 

    /**
     * Method deleteCdMomentoProcessamentoPagamento
     * 
     */
    public void deleteCdMomentoProcessamentoPagamento()
    {
        this._has_cdMomentoProcessamentoPagamento= false;
    } //-- void deleteCdMomentoProcessamentoPagamento() 

    /**
     * Method deleteCdNaturezaOperacaoPagamento
     * 
     */
    public void deleteCdNaturezaOperacaoPagamento()
    {
        this._has_cdNaturezaOperacaoPagamento= false;
    } //-- void deleteCdNaturezaOperacaoPagamento() 

    /**
     * Method deleteCdPagamentoNaoUtil
     * 
     */
    public void deleteCdPagamentoNaoUtil()
    {
        this._has_cdPagamentoNaoUtil= false;
    } //-- void deleteCdPagamentoNaoUtil() 

    /**
     * Method deleteCdPeriodicidadeAviso
     * 
     */
    public void deleteCdPeriodicidadeAviso()
    {
        this._has_cdPeriodicidadeAviso= false;
    } //-- void deleteCdPeriodicidadeAviso() 

    /**
     * Method deleteCdPeriodicidadeCobrancaTarifa
     * 
     */
    public void deleteCdPeriodicidadeCobrancaTarifa()
    {
        this._has_cdPeriodicidadeCobrancaTarifa= false;
    } //-- void deleteCdPeriodicidadeCobrancaTarifa() 

    /**
     * Method deleteCdPeriodicidadeComprovante
     * 
     */
    public void deleteCdPeriodicidadeComprovante()
    {
        this._has_cdPeriodicidadeComprovante= false;
    } //-- void deleteCdPeriodicidadeComprovante() 

    /**
     * Method deleteCdPeriodicidadeConsultaVeiculo
     * 
     */
    public void deleteCdPeriodicidadeConsultaVeiculo()
    {
        this._has_cdPeriodicidadeConsultaVeiculo= false;
    } //-- void deleteCdPeriodicidadeConsultaVeiculo() 

    /**
     * Method deleteCdPeriodicidadeEnvioRemessa
     * 
     */
    public void deleteCdPeriodicidadeEnvioRemessa()
    {
        this._has_cdPeriodicidadeEnvioRemessa= false;
    } //-- void deleteCdPeriodicidadeEnvioRemessa() 

    /**
     * Method deleteCdPeriodicidadeManutencaoProcd
     * 
     */
    public void deleteCdPeriodicidadeManutencaoProcd()
    {
        this._has_cdPeriodicidadeManutencaoProcd= false;
    } //-- void deleteCdPeriodicidadeManutencaoProcd() 

    /**
     * Method deleteCdPermissaoDebitoOnline
     * 
     */
    public void deleteCdPermissaoDebitoOnline()
    {
        this._has_cdPermissaoDebitoOnline= false;
    } //-- void deleteCdPermissaoDebitoOnline() 

    /**
     * Method deleteCdPrincipalEnquaRecadastramento
     * 
     */
    public void deleteCdPrincipalEnquaRecadastramento()
    {
        this._has_cdPrincipalEnquaRecadastramento= false;
    } //-- void deleteCdPrincipalEnquaRecadastramento() 

    /**
     * Method deleteCdPrioridadeEfetivacaoPagamento
     * 
     */
    public void deleteCdPrioridadeEfetivacaoPagamento()
    {
        this._has_cdPrioridadeEfetivacaoPagamento= false;
    } //-- void deleteCdPrioridadeEfetivacaoPagamento() 

    /**
     * Method deleteCdRastreabNotaFiscal
     * 
     */
    public void deleteCdRastreabNotaFiscal()
    {
        this._has_cdRastreabNotaFiscal= false;
    } //-- void deleteCdRastreabNotaFiscal() 

    /**
     * Method deleteCdRastreabTituloTerc
     * 
     */
    public void deleteCdRastreabTituloTerc()
    {
        this._has_cdRastreabTituloTerc= false;
    } //-- void deleteCdRastreabTituloTerc() 

    /**
     * Method deleteCdRejeicaoAgendamentoLote
     * 
     */
    public void deleteCdRejeicaoAgendamentoLote()
    {
        this._has_cdRejeicaoAgendamentoLote= false;
    } //-- void deleteCdRejeicaoAgendamentoLote() 

    /**
     * Method deleteCdRejeicaoEfetivacaoLote
     * 
     */
    public void deleteCdRejeicaoEfetivacaoLote()
    {
        this._has_cdRejeicaoEfetivacaoLote= false;
    } //-- void deleteCdRejeicaoEfetivacaoLote() 

    /**
     * Method deleteCdRejeicaoLote
     * 
     */
    public void deleteCdRejeicaoLote()
    {
        this._has_cdRejeicaoLote= false;
    } //-- void deleteCdRejeicaoLote() 

    /**
     * Method deleteCdTipoCargaRecadastramento
     * 
     */
    public void deleteCdTipoCargaRecadastramento()
    {
        this._has_cdTipoCargaRecadastramento= false;
    } //-- void deleteCdTipoCargaRecadastramento() 

    /**
     * Method deleteCdTipoCartaoSalario
     * 
     */
    public void deleteCdTipoCartaoSalario()
    {
        this._has_cdTipoCartaoSalario= false;
    } //-- void deleteCdTipoCartaoSalario() 

    /**
     * Method deleteCdTipoConsistenciaFavorecido
     * 
     */
    public void deleteCdTipoConsistenciaFavorecido()
    {
        this._has_cdTipoConsistenciaFavorecido= false;
    } //-- void deleteCdTipoConsistenciaFavorecido() 

    /**
     * Method deleteCdTipoConsistenciaLista
     * 
     */
    public void deleteCdTipoConsistenciaLista()
    {
        this._has_cdTipoConsistenciaLista= false;
    } //-- void deleteCdTipoConsistenciaLista() 

    /**
     * Method deleteCdTipoConsultaComprovante
     * 
     */
    public void deleteCdTipoConsultaComprovante()
    {
        this._has_cdTipoConsultaComprovante= false;
    } //-- void deleteCdTipoConsultaComprovante() 

    /**
     * Method deleteCdTipoDataFloating
     * 
     */
    public void deleteCdTipoDataFloating()
    {
        this._has_cdTipoDataFloating= false;
    } //-- void deleteCdTipoDataFloating() 

    /**
     * Method deleteCdTipoDivergenciaVeiculo
     * 
     */
    public void deleteCdTipoDivergenciaVeiculo()
    {
        this._has_cdTipoDivergenciaVeiculo= false;
    } //-- void deleteCdTipoDivergenciaVeiculo() 

    /**
     * Method deleteCdTipoFormacaoLista
     * 
     */
    public void deleteCdTipoFormacaoLista()
    {
        this._has_cdTipoFormacaoLista= false;
    } //-- void deleteCdTipoFormacaoLista() 

    /**
     * Method deleteCdTipoIdentificacaoBeneficio
     * 
     */
    public void deleteCdTipoIdentificacaoBeneficio()
    {
        this._has_cdTipoIdentificacaoBeneficio= false;
    } //-- void deleteCdTipoIdentificacaoBeneficio() 

    /**
     * Method deleteCdTipoInscricaoFavorecidoAceita
     * 
     */
    public void deleteCdTipoInscricaoFavorecidoAceita()
    {
        this._has_cdTipoInscricaoFavorecidoAceita= false;
    } //-- void deleteCdTipoInscricaoFavorecidoAceita() 

    /**
     * Method deleteCdTipoLayoutArquivo
     * 
     */
    public void deleteCdTipoLayoutArquivo()
    {
        this._has_cdTipoLayoutArquivo= false;
    } //-- void deleteCdTipoLayoutArquivo() 

    /**
     * Method deleteCdTipoReajusteTarifa
     * 
     */
    public void deleteCdTipoReajusteTarifa()
    {
        this._has_cdTipoReajusteTarifa= false;
    } //-- void deleteCdTipoReajusteTarifa() 

    /**
     * Method deleteCdTratoContaTransf
     * 
     */
    public void deleteCdTratoContaTransf()
    {
        this._has_cdTratoContaTransf= false;
    } //-- void deleteCdTratoContaTransf() 

    /**
     * Method deleteCdUtilizacaoFavorecidoControle
     * 
     */
    public void deleteCdUtilizacaoFavorecidoControle()
    {
        this._has_cdUtilizacaoFavorecidoControle= false;
    } //-- void deleteCdUtilizacaoFavorecidoControle() 

    /**
     * Method deleteCdValidacaoNomeFavorecido
     * 
     */
    public void deleteCdValidacaoNomeFavorecido()
    {
        this._has_cdValidacaoNomeFavorecido= false;
    } //-- void deleteCdValidacaoNomeFavorecido() 

    /**
     * Method deleteCdindicadorExpiraCredito
     * 
     */
    public void deleteCdindicadorExpiraCredito()
    {
        this._has_cdindicadorExpiraCredito= false;
    } //-- void deleteCdindicadorExpiraCredito() 

    /**
     * Method deleteCdindicadorLancamentoProgramado
     * 
     */
    public void deleteCdindicadorLancamentoProgramado()
    {
        this._has_cdindicadorLancamentoProgramado= false;
    } //-- void deleteCdindicadorLancamentoProgramado() 

    /**
     * Method deleteNrFechamentoApuracaoTarifa
     * 
     */
    public void deleteNrFechamentoApuracaoTarifa()
    {
        this._has_nrFechamentoApuracaoTarifa= false;
    } //-- void deleteNrFechamentoApuracaoTarifa() 

    /**
     * Method deletePercentualMaximoInconsistenteLote
     * 
     */
    public void deletePercentualMaximoInconsistenteLote()
    {
        this._has_percentualMaximoInconsistenteLote= false;
    } //-- void deletePercentualMaximoInconsistenteLote() 

    /**
     * Method deleteQuantidadeAntecedencia
     * 
     */
    public void deleteQuantidadeAntecedencia()
    {
        this._has_quantidadeAntecedencia= false;
    } //-- void deleteQuantidadeAntecedencia() 

    /**
     * Method deleteQuantidadeAnteriorVencimentoComprovante
     * 
     */
    public void deleteQuantidadeAnteriorVencimentoComprovante()
    {
        this._has_quantidadeAnteriorVencimentoComprovante= false;
    } //-- void deleteQuantidadeAnteriorVencimentoComprovante() 

    /**
     * Method deleteQuantidadeDiaCobrancaTarifa
     * 
     */
    public void deleteQuantidadeDiaCobrancaTarifa()
    {
        this._has_quantidadeDiaCobrancaTarifa= false;
    } //-- void deleteQuantidadeDiaCobrancaTarifa() 

    /**
     * Method deleteQuantidadeDiaExpiracao
     * 
     */
    public void deleteQuantidadeDiaExpiracao()
    {
        this._has_quantidadeDiaExpiracao= false;
    } //-- void deleteQuantidadeDiaExpiracao() 

    /**
     * Method deleteQuantidadeDiaFloatingPagamento
     * 
     */
    public void deleteQuantidadeDiaFloatingPagamento()
    {
        this._has_quantidadeDiaFloatingPagamento= false;
    } //-- void deleteQuantidadeDiaFloatingPagamento() 

    /**
     * Method deleteQuantidadeDiaInatividadeFavorecido
     * 
     */
    public void deleteQuantidadeDiaInatividadeFavorecido()
    {
        this._has_quantidadeDiaInatividadeFavorecido= false;
    } //-- void deleteQuantidadeDiaInatividadeFavorecido() 

    /**
     * Method deleteQuantidadeDiaRepiqueConsulta
     * 
     */
    public void deleteQuantidadeDiaRepiqueConsulta()
    {
        this._has_quantidadeDiaRepiqueConsulta= false;
    } //-- void deleteQuantidadeDiaRepiqueConsulta() 

    /**
     * Method deleteQuantidadeEtapaRecadastramentoBeneficio
     * 
     */
    public void deleteQuantidadeEtapaRecadastramentoBeneficio()
    {
        this._has_quantidadeEtapaRecadastramentoBeneficio= false;
    } //-- void deleteQuantidadeEtapaRecadastramentoBeneficio() 

    /**
     * Method deleteQuantidadeFaseRecadastramentoBeneficio
     * 
     */
    public void deleteQuantidadeFaseRecadastramentoBeneficio()
    {
        this._has_quantidadeFaseRecadastramentoBeneficio= false;
    } //-- void deleteQuantidadeFaseRecadastramentoBeneficio() 

    /**
     * Method deleteQuantidadeLimiteLinha
     * 
     */
    public void deleteQuantidadeLimiteLinha()
    {
        this._has_quantidadeLimiteLinha= false;
    } //-- void deleteQuantidadeLimiteLinha() 

    /**
     * Method deleteQuantidadeMaximaInconsistenteLote
     * 
     */
    public void deleteQuantidadeMaximaInconsistenteLote()
    {
        this._has_quantidadeMaximaInconsistenteLote= false;
    } //-- void deleteQuantidadeMaximaInconsistenteLote() 

    /**
     * Method deleteQuantidadeMaximaTituloVencido
     * 
     */
    public void deleteQuantidadeMaximaTituloVencido()
    {
        this._has_quantidadeMaximaTituloVencido= false;
    } //-- void deleteQuantidadeMaximaTituloVencido() 

    /**
     * Method deleteQuantidadeMesComprovante
     * 
     */
    public void deleteQuantidadeMesComprovante()
    {
        this._has_quantidadeMesComprovante= false;
    } //-- void deleteQuantidadeMesComprovante() 

    /**
     * Method deleteQuantidadeMesEtapaRecadastramento
     * 
     */
    public void deleteQuantidadeMesEtapaRecadastramento()
    {
        this._has_quantidadeMesEtapaRecadastramento= false;
    } //-- void deleteQuantidadeMesEtapaRecadastramento() 

    /**
     * Method deleteQuantidadeMesFaseRecadastramento
     * 
     */
    public void deleteQuantidadeMesFaseRecadastramento()
    {
        this._has_quantidadeMesFaseRecadastramento= false;
    } //-- void deleteQuantidadeMesFaseRecadastramento() 

    /**
     * Method deleteQuantidadeMesReajusteTarifa
     * 
     */
    public void deleteQuantidadeMesReajusteTarifa()
    {
        this._has_quantidadeMesReajusteTarifa= false;
    } //-- void deleteQuantidadeMesReajusteTarifa() 

    /**
     * Method deleteQuantidadeSolicitacaoCartao
     * 
     */
    public void deleteQuantidadeSolicitacaoCartao()
    {
        this._has_quantidadeSolicitacaoCartao= false;
    } //-- void deleteQuantidadeSolicitacaoCartao() 

    /**
     * Method deleteQuantidadeViaAviso
     * 
     */
    public void deleteQuantidadeViaAviso()
    {
        this._has_quantidadeViaAviso= false;
    } //-- void deleteQuantidadeViaAviso() 

    /**
     * Method deleteQuantidadeViaCombranca
     * 
     */
    public void deleteQuantidadeViaCombranca()
    {
        this._has_quantidadeViaCombranca= false;
    } //-- void deleteQuantidadeViaCombranca() 

    /**
     * Method deleteQuantidadeViaComprovante
     * 
     */
    public void deleteQuantidadeViaComprovante()
    {
        this._has_quantidadeViaComprovante= false;
    } //-- void deleteQuantidadeViaComprovante() 

    /**
     * Returns the value of field 'cdAcaoNaoVida'.
     * 
     * @return int
     * @return the value of field 'cdAcaoNaoVida'.
     */
    public int getCdAcaoNaoVida()
    {
        return this._cdAcaoNaoVida;
    } //-- int getCdAcaoNaoVida() 

    /**
     * Returns the value of field 'cdAcertoDadosRecadastramento'.
     * 
     * @return int
     * @return the value of field 'cdAcertoDadosRecadastramento'.
     */
    public int getCdAcertoDadosRecadastramento()
    {
        return this._cdAcertoDadosRecadastramento;
    } //-- int getCdAcertoDadosRecadastramento() 

    /**
     * Returns the value of field 'cdAgendaRastreabilidadeFilial'.
     * 
     * @return int
     * @return the value of field 'cdAgendaRastreabilidadeFilial'.
     */
    public int getCdAgendaRastreabilidadeFilial()
    {
        return this._cdAgendaRastreabilidadeFilial;
    } //-- int getCdAgendaRastreabilidadeFilial() 

    /**
     * Returns the value of field 'cdAgendamentoDebitoVeiculo'.
     * 
     * @return int
     * @return the value of field 'cdAgendamentoDebitoVeiculo'.
     */
    public int getCdAgendamentoDebitoVeiculo()
    {
        return this._cdAgendamentoDebitoVeiculo;
    } //-- int getCdAgendamentoDebitoVeiculo() 

    /**
     * Returns the value of field 'cdAgendamentoPagamentoVencido'.
     * 
     * @return int
     * @return the value of field 'cdAgendamentoPagamentoVencido'.
     */
    public int getCdAgendamentoPagamentoVencido()
    {
        return this._cdAgendamentoPagamentoVencido;
    } //-- int getCdAgendamentoPagamentoVencido() 

    /**
     * Returns the value of field 'cdAgendamentoValorMenor'.
     * 
     * @return int
     * @return the value of field 'cdAgendamentoValorMenor'.
     */
    public int getCdAgendamentoValorMenor()
    {
        return this._cdAgendamentoValorMenor;
    } //-- int getCdAgendamentoValorMenor() 

    /**
     * Returns the value of field 'cdAgrupamentoAviso'.
     * 
     * @return int
     * @return the value of field 'cdAgrupamentoAviso'.
     */
    public int getCdAgrupamentoAviso()
    {
        return this._cdAgrupamentoAviso;
    } //-- int getCdAgrupamentoAviso() 

    /**
     * Returns the value of field 'cdAgrupamentoComprovante'.
     * 
     * @return int
     * @return the value of field 'cdAgrupamentoComprovante'.
     */
    public int getCdAgrupamentoComprovante()
    {
        return this._cdAgrupamentoComprovante;
    } //-- int getCdAgrupamentoComprovante() 

    /**
     * Returns the value of field
     * 'cdAgrupamentoFormularioRecadastro'.
     * 
     * @return int
     * @return the value of field
     * 'cdAgrupamentoFormularioRecadastro'.
     */
    public int getCdAgrupamentoFormularioRecadastro()
    {
        return this._cdAgrupamentoFormularioRecadastro;
    } //-- int getCdAgrupamentoFormularioRecadastro() 

    /**
     * Returns the value of field 'cdAmbienteServicoContrato'.
     * 
     * @return String
     * @return the value of field 'cdAmbienteServicoContrato'.
     */
    public java.lang.String getCdAmbienteServicoContrato()
    {
        return this._cdAmbienteServicoContrato;
    } //-- java.lang.String getCdAmbienteServicoContrato() 

    /**
     * Returns the value of field
     * 'cdAntecipacaoRecadastramentoBeneficiario'.
     * 
     * @return int
     * @return the value of field
     * 'cdAntecipacaoRecadastramentoBeneficiario'.
     */
    public int getCdAntecipacaoRecadastramentoBeneficiario()
    {
        return this._cdAntecipacaoRecadastramentoBeneficiario;
    } //-- int getCdAntecipacaoRecadastramentoBeneficiario() 

    /**
     * Returns the value of field 'cdAreaReservada'.
     * 
     * @return int
     * @return the value of field 'cdAreaReservada'.
     */
    public int getCdAreaReservada()
    {
        return this._cdAreaReservada;
    } //-- int getCdAreaReservada() 

    /**
     * Returns the value of field 'cdBaseRecadastramentoBeneficio'.
     * 
     * @return int
     * @return the value of field 'cdBaseRecadastramentoBeneficio'.
     */
    public int getCdBaseRecadastramentoBeneficio()
    {
        return this._cdBaseRecadastramentoBeneficio;
    } //-- int getCdBaseRecadastramentoBeneficio() 

    /**
     * Returns the value of field 'cdBloqueioEmissaoPplta'.
     * 
     * @return int
     * @return the value of field 'cdBloqueioEmissaoPplta'.
     */
    public int getCdBloqueioEmissaoPplta()
    {
        return this._cdBloqueioEmissaoPplta;
    } //-- int getCdBloqueioEmissaoPplta() 

    /**
     * Returns the value of field 'cdCanalAlteracao'.
     * 
     * @return int
     * @return the value of field 'cdCanalAlteracao'.
     */
    public int getCdCanalAlteracao()
    {
        return this._cdCanalAlteracao;
    } //-- int getCdCanalAlteracao() 

    /**
     * Returns the value of field 'cdCanalInclusao'.
     * 
     * @return int
     * @return the value of field 'cdCanalInclusao'.
     */
    public int getCdCanalInclusao()
    {
        return this._cdCanalInclusao;
    } //-- int getCdCanalInclusao() 

    /**
     * Returns the value of field 'cdCaptuTituloRegistro'.
     * 
     * @return int
     * @return the value of field 'cdCaptuTituloRegistro'.
     */
    public int getCdCaptuTituloRegistro()
    {
        return this._cdCaptuTituloRegistro;
    } //-- int getCdCaptuTituloRegistro() 

    /**
     * Returns the value of field 'cdCctciaEspeBeneficio'.
     * 
     * @return int
     * @return the value of field 'cdCctciaEspeBeneficio'.
     */
    public int getCdCctciaEspeBeneficio()
    {
        return this._cdCctciaEspeBeneficio;
    } //-- int getCdCctciaEspeBeneficio() 

    /**
     * Returns the value of field 'cdCctciaIdentificacaoBeneficio'.
     * 
     * @return int
     * @return the value of field 'cdCctciaIdentificacaoBeneficio'.
     */
    public int getCdCctciaIdentificacaoBeneficio()
    {
        return this._cdCctciaIdentificacaoBeneficio;
    } //-- int getCdCctciaIdentificacaoBeneficio() 

    /**
     * Returns the value of field 'cdCctciaInscricaoFavorecido'.
     * 
     * @return int
     * @return the value of field 'cdCctciaInscricaoFavorecido'.
     */
    public int getCdCctciaInscricaoFavorecido()
    {
        return this._cdCctciaInscricaoFavorecido;
    } //-- int getCdCctciaInscricaoFavorecido() 

    /**
     * Returns the value of field 'cdCctciaProprietarioVeculo'.
     * 
     * @return int
     * @return the value of field 'cdCctciaProprietarioVeculo'.
     */
    public int getCdCctciaProprietarioVeculo()
    {
        return this._cdCctciaProprietarioVeculo;
    } //-- int getCdCctciaProprietarioVeculo() 

    /**
     * Returns the value of field 'cdCobrancaTarifa'.
     * 
     * @return int
     * @return the value of field 'cdCobrancaTarifa'.
     */
    public int getCdCobrancaTarifa()
    {
        return this._cdCobrancaTarifa;
    } //-- int getCdCobrancaTarifa() 

    /**
     * Returns the value of field 'cdConsultaDebitoVeiculo'.
     * 
     * @return int
     * @return the value of field 'cdConsultaDebitoVeiculo'.
     */
    public int getCdConsultaDebitoVeiculo()
    {
        return this._cdConsultaDebitoVeiculo;
    } //-- int getCdConsultaDebitoVeiculo() 

    /**
     * Returns the value of field 'cdConsultaEndereco'.
     * 
     * @return int
     * @return the value of field 'cdConsultaEndereco'.
     */
    public int getCdConsultaEndereco()
    {
        return this._cdConsultaEndereco;
    } //-- int getCdConsultaEndereco() 

    /**
     * Returns the value of field 'cdConsultaSaldoPagamento'.
     * 
     * @return int
     * @return the value of field 'cdConsultaSaldoPagamento'.
     */
    public int getCdConsultaSaldoPagamento()
    {
        return this._cdConsultaSaldoPagamento;
    } //-- int getCdConsultaSaldoPagamento() 

    /**
     * Returns the value of field 'cdConsultaSaldoValorSuperior'.
     * 
     * @return int
     * @return the value of field 'cdConsultaSaldoValorSuperior'.
     */
    public int getCdConsultaSaldoValorSuperior()
    {
        return this._cdConsultaSaldoValorSuperior;
    } //-- int getCdConsultaSaldoValorSuperior() 

    /**
     * Returns the value of field 'cdContagemConsultaSaldo'.
     * 
     * @return int
     * @return the value of field 'cdContagemConsultaSaldo'.
     */
    public int getCdContagemConsultaSaldo()
    {
        return this._cdContagemConsultaSaldo;
    } //-- int getCdContagemConsultaSaldo() 

    /**
     * Returns the value of field 'cdCreditoNaoUtilizado'.
     * 
     * @return int
     * @return the value of field 'cdCreditoNaoUtilizado'.
     */
    public int getCdCreditoNaoUtilizado()
    {
        return this._cdCreditoNaoUtilizado;
    } //-- int getCdCreditoNaoUtilizado() 

    /**
     * Returns the value of field
     * 'cdCriterioEnquadraRecadastramento'.
     * 
     * @return int
     * @return the value of field
     * 'cdCriterioEnquadraRecadastramento'.
     */
    public int getCdCriterioEnquadraRecadastramento()
    {
        return this._cdCriterioEnquadraRecadastramento;
    } //-- int getCdCriterioEnquadraRecadastramento() 

    /**
     * Returns the value of field 'cdCriterioEnquandraBeneficio'.
     * 
     * @return int
     * @return the value of field 'cdCriterioEnquandraBeneficio'.
     */
    public int getCdCriterioEnquandraBeneficio()
    {
        return this._cdCriterioEnquandraBeneficio;
    } //-- int getCdCriterioEnquandraBeneficio() 

    /**
     * Returns the value of field 'cdCriterioRstrbTitulo'.
     * 
     * @return int
     * @return the value of field 'cdCriterioRstrbTitulo'.
     */
    public int getCdCriterioRstrbTitulo()
    {
        return this._cdCriterioRstrbTitulo;
    } //-- int getCdCriterioRstrbTitulo() 

    /**
     * Returns the value of field 'cdDestinoAviso'.
     * 
     * @return int
     * @return the value of field 'cdDestinoAviso'.
     */
    public int getCdDestinoAviso()
    {
        return this._cdDestinoAviso;
    } //-- int getCdDestinoAviso() 

    /**
     * Returns the value of field 'cdDestinoComprovante'.
     * 
     * @return int
     * @return the value of field 'cdDestinoComprovante'.
     */
    public int getCdDestinoComprovante()
    {
        return this._cdDestinoComprovante;
    } //-- int getCdDestinoComprovante() 

    /**
     * Returns the value of field
     * 'cdDestinoFormularioRecadastramento'.
     * 
     * @return int
     * @return the value of field
     * 'cdDestinoFormularioRecadastramento'.
     */
    public int getCdDestinoFormularioRecadastramento()
    {
        return this._cdDestinoFormularioRecadastramento;
    } //-- int getCdDestinoFormularioRecadastramento() 

    /**
     * Returns the value of field 'cdDisponibilizacaoContaCredito'.
     * 
     * @return int
     * @return the value of field 'cdDisponibilizacaoContaCredito'.
     */
    public int getCdDisponibilizacaoContaCredito()
    {
        return this._cdDisponibilizacaoContaCredito;
    } //-- int getCdDisponibilizacaoContaCredito() 

    /**
     * Returns the value of field
     * 'cdDisponibilizacaoDiversoCriterio'.
     * 
     * @return int
     * @return the value of field
     * 'cdDisponibilizacaoDiversoCriterio'.
     */
    public int getCdDisponibilizacaoDiversoCriterio()
    {
        return this._cdDisponibilizacaoDiversoCriterio;
    } //-- int getCdDisponibilizacaoDiversoCriterio() 

    /**
     * Returns the value of field 'cdDisponibilizacaoDiversoNao'.
     * 
     * @return int
     * @return the value of field 'cdDisponibilizacaoDiversoNao'.
     */
    public int getCdDisponibilizacaoDiversoNao()
    {
        return this._cdDisponibilizacaoDiversoNao;
    } //-- int getCdDisponibilizacaoDiversoNao() 

    /**
     * Returns the value of field
     * 'cdDisponibilizacaoSalarioCriterio'.
     * 
     * @return int
     * @return the value of field
     * 'cdDisponibilizacaoSalarioCriterio'.
     */
    public int getCdDisponibilizacaoSalarioCriterio()
    {
        return this._cdDisponibilizacaoSalarioCriterio;
    } //-- int getCdDisponibilizacaoSalarioCriterio() 

    /**
     * Returns the value of field 'cdDisponibilizacaoSalarioNao'.
     * 
     * @return int
     * @return the value of field 'cdDisponibilizacaoSalarioNao'.
     */
    public int getCdDisponibilizacaoSalarioNao()
    {
        return this._cdDisponibilizacaoSalarioNao;
    } //-- int getCdDisponibilizacaoSalarioNao() 

    /**
     * Returns the value of field 'cdEnvelopeAberto'.
     * 
     * @return int
     * @return the value of field 'cdEnvelopeAberto'.
     */
    public int getCdEnvelopeAberto()
    {
        return this._cdEnvelopeAberto;
    } //-- int getCdEnvelopeAberto() 

    /**
     * Returns the value of field 'cdFavorecidoConsultaPagamento'.
     * 
     * @return int
     * @return the value of field 'cdFavorecidoConsultaPagamento'.
     */
    public int getCdFavorecidoConsultaPagamento()
    {
        return this._cdFavorecidoConsultaPagamento;
    } //-- int getCdFavorecidoConsultaPagamento() 

    /**
     * Returns the value of field 'cdFormaAutorizacaoPagamento'.
     * 
     * @return int
     * @return the value of field 'cdFormaAutorizacaoPagamento'.
     */
    public int getCdFormaAutorizacaoPagamento()
    {
        return this._cdFormaAutorizacaoPagamento;
    } //-- int getCdFormaAutorizacaoPagamento() 

    /**
     * Returns the value of field 'cdFormaEnvioPagamento'.
     * 
     * @return int
     * @return the value of field 'cdFormaEnvioPagamento'.
     */
    public int getCdFormaEnvioPagamento()
    {
        return this._cdFormaEnvioPagamento;
    } //-- int getCdFormaEnvioPagamento() 

    /**
     * Returns the value of field 'cdFormaExpiracaoCredito'.
     * 
     * @return int
     * @return the value of field 'cdFormaExpiracaoCredito'.
     */
    public int getCdFormaExpiracaoCredito()
    {
        return this._cdFormaExpiracaoCredito;
    } //-- int getCdFormaExpiracaoCredito() 

    /**
     * Returns the value of field 'cdFormaManutencao'.
     * 
     * @return int
     * @return the value of field 'cdFormaManutencao'.
     */
    public int getCdFormaManutencao()
    {
        return this._cdFormaManutencao;
    } //-- int getCdFormaManutencao() 

    /**
     * Returns the value of field 'cdFormularioContratoCliente'.
     * 
     * @return int
     * @return the value of field 'cdFormularioContratoCliente'.
     */
    public int getCdFormularioContratoCliente()
    {
        return this._cdFormularioContratoCliente;
    } //-- int getCdFormularioContratoCliente() 

    /**
     * Returns the value of field 'cdFrasePrecadastrada'.
     * 
     * @return int
     * @return the value of field 'cdFrasePrecadastrada'.
     */
    public int getCdFrasePrecadastrada()
    {
        return this._cdFrasePrecadastrada;
    } //-- int getCdFrasePrecadastrada() 

    /**
     * Returns the value of field 'cdIndLancamentoPersonalizado'.
     * 
     * @return int
     * @return the value of field 'cdIndLancamentoPersonalizado'.
     */
    public int getCdIndLancamentoPersonalizado()
    {
        return this._cdIndLancamentoPersonalizado;
    } //-- int getCdIndLancamentoPersonalizado() 

    /**
     * Returns the value of field 'cdIndicadorAdesaoSacador'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorAdesaoSacador'.
     */
    public int getCdIndicadorAdesaoSacador()
    {
        return this._cdIndicadorAdesaoSacador;
    } //-- int getCdIndicadorAdesaoSacador() 

    /**
     * Returns the value of field 'cdIndicadorAgendaTitulo'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorAgendaTitulo'.
     */
    public int getCdIndicadorAgendaTitulo()
    {
        return this._cdIndicadorAgendaTitulo;
    } //-- int getCdIndicadorAgendaTitulo() 

    /**
     * Returns the value of field 'cdIndicadorAutorizacaoCliente'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorAutorizacaoCliente'.
     */
    public int getCdIndicadorAutorizacaoCliente()
    {
        return this._cdIndicadorAutorizacaoCliente;
    } //-- int getCdIndicadorAutorizacaoCliente() 

    /**
     * Returns the value of field
     * 'cdIndicadorAutorizacaoComplemento'.
     * 
     * @return int
     * @return the value of field
     * 'cdIndicadorAutorizacaoComplemento'.
     */
    public int getCdIndicadorAutorizacaoComplemento()
    {
        return this._cdIndicadorAutorizacaoComplemento;
    } //-- int getCdIndicadorAutorizacaoComplemento() 

    /**
     * Returns the value of field 'cdIndicadorBancoPostal'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorBancoPostal'.
     */
    public int getCdIndicadorBancoPostal()
    {
        return this._cdIndicadorBancoPostal;
    } //-- int getCdIndicadorBancoPostal() 

    /**
     * Returns the value of field 'cdIndicadorCadastroProcurador'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorCadastroProcurador'.
     */
    public int getCdIndicadorCadastroProcurador()
    {
        return this._cdIndicadorCadastroProcurador;
    } //-- int getCdIndicadorCadastroProcurador() 

    /**
     * Returns the value of field 'cdIndicadorCadastroorganizacao'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorCadastroorganizacao'.
     */
    public int getCdIndicadorCadastroorganizacao()
    {
        return this._cdIndicadorCadastroorganizacao;
    } //-- int getCdIndicadorCadastroorganizacao() 

    /**
     * Returns the value of field 'cdIndicadorCartaoSalario'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorCartaoSalario'.
     */
    public int getCdIndicadorCartaoSalario()
    {
        return this._cdIndicadorCartaoSalario;
    } //-- int getCdIndicadorCartaoSalario() 

    /**
     * Returns the value of field 'cdIndicadorEconomicoReajuste'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorEconomicoReajuste'.
     */
    public int getCdIndicadorEconomicoReajuste()
    {
        return this._cdIndicadorEconomicoReajuste;
    } //-- int getCdIndicadorEconomicoReajuste() 

    /**
     * Returns the value of field 'cdIndicadorEmissaoAviso'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorEmissaoAviso'.
     */
    public int getCdIndicadorEmissaoAviso()
    {
        return this._cdIndicadorEmissaoAviso;
    } //-- int getCdIndicadorEmissaoAviso() 

    /**
     * Returns the value of field 'cdIndicadorListaDebito'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorListaDebito'.
     */
    public int getCdIndicadorListaDebito()
    {
        return this._cdIndicadorListaDebito;
    } //-- int getCdIndicadorListaDebito() 

    /**
     * Returns the value of field
     * 'cdIndicadorMensagemPersonalizada'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorMensagemPersonalizada'
     */
    public int getCdIndicadorMensagemPersonalizada()
    {
        return this._cdIndicadorMensagemPersonalizada;
    } //-- int getCdIndicadorMensagemPersonalizada() 

    /**
     * Returns the value of field 'cdIndicadorRetornoInternet'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorRetornoInternet'.
     */
    public int getCdIndicadorRetornoInternet()
    {
        return this._cdIndicadorRetornoInternet;
    } //-- int getCdIndicadorRetornoInternet() 

    /**
     * Returns the value of field 'cdIndicadorRetornoSeparado'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorRetornoSeparado'.
     */
    public int getCdIndicadorRetornoSeparado()
    {
        return this._cdIndicadorRetornoSeparado;
    } //-- int getCdIndicadorRetornoSeparado() 

    /**
     * Returns the value of field 'cdLancamentoFuturoCredito'.
     * 
     * @return int
     * @return the value of field 'cdLancamentoFuturoCredito'.
     */
    public int getCdLancamentoFuturoCredito()
    {
        return this._cdLancamentoFuturoCredito;
    } //-- int getCdLancamentoFuturoCredito() 

    /**
     * Returns the value of field 'cdLancamentoFuturoDebito'.
     * 
     * @return int
     * @return the value of field 'cdLancamentoFuturoDebito'.
     */
    public int getCdLancamentoFuturoDebito()
    {
        return this._cdLancamentoFuturoDebito;
    } //-- int getCdLancamentoFuturoDebito() 

    /**
     * Returns the value of field 'cdLiberacaoLoteProcs'.
     * 
     * @return int
     * @return the value of field 'cdLiberacaoLoteProcs'.
     */
    public int getCdLiberacaoLoteProcs()
    {
        return this._cdLiberacaoLoteProcs;
    } //-- int getCdLiberacaoLoteProcs() 

    /**
     * Returns the value of field
     * 'cdManutencaoBaseRecadastramento'.
     * 
     * @return int
     * @return the value of field 'cdManutencaoBaseRecadastramento'.
     */
    public int getCdManutencaoBaseRecadastramento()
    {
        return this._cdManutencaoBaseRecadastramento;
    } //-- int getCdManutencaoBaseRecadastramento() 

    /**
     * Returns the value of field 'cdMeioPagamentoCredito'.
     * 
     * @return int
     * @return the value of field 'cdMeioPagamentoCredito'.
     */
    public int getCdMeioPagamentoCredito()
    {
        return this._cdMeioPagamentoCredito;
    } //-- int getCdMeioPagamentoCredito() 

    /**
     * Returns the value of field 'cdMensagemRecadastramentoMidia'.
     * 
     * @return int
     * @return the value of field 'cdMensagemRecadastramentoMidia'.
     */
    public int getCdMensagemRecadastramentoMidia()
    {
        return this._cdMensagemRecadastramentoMidia;
    } //-- int getCdMensagemRecadastramentoMidia() 

    /**
     * Returns the value of field 'cdMidiaDisponivel'.
     * 
     * @return int
     * @return the value of field 'cdMidiaDisponivel'.
     */
    public int getCdMidiaDisponivel()
    {
        return this._cdMidiaDisponivel;
    } //-- int getCdMidiaDisponivel() 

    /**
     * Returns the value of field 'cdMidiaMensagemRecadastramento'.
     * 
     * @return int
     * @return the value of field 'cdMidiaMensagemRecadastramento'.
     */
    public int getCdMidiaMensagemRecadastramento()
    {
        return this._cdMidiaMensagemRecadastramento;
    } //-- int getCdMidiaMensagemRecadastramento() 

    /**
     * Returns the value of field 'cdMomentoAvisoRecadastramento'.
     * 
     * @return int
     * @return the value of field 'cdMomentoAvisoRecadastramento'.
     */
    public int getCdMomentoAvisoRecadastramento()
    {
        return this._cdMomentoAvisoRecadastramento;
    } //-- int getCdMomentoAvisoRecadastramento() 

    /**
     * Returns the value of field 'cdMomentoCreditoEfetivacao'.
     * 
     * @return int
     * @return the value of field 'cdMomentoCreditoEfetivacao'.
     */
    public int getCdMomentoCreditoEfetivacao()
    {
        return this._cdMomentoCreditoEfetivacao;
    } //-- int getCdMomentoCreditoEfetivacao() 

    /**
     * Returns the value of field 'cdMomentoDebitoPagamento'.
     * 
     * @return int
     * @return the value of field 'cdMomentoDebitoPagamento'.
     */
    public int getCdMomentoDebitoPagamento()
    {
        return this._cdMomentoDebitoPagamento;
    } //-- int getCdMomentoDebitoPagamento() 

    /**
     * Returns the value of field
     * 'cdMomentoFormularioRecadastramento'.
     * 
     * @return int
     * @return the value of field
     * 'cdMomentoFormularioRecadastramento'.
     */
    public int getCdMomentoFormularioRecadastramento()
    {
        return this._cdMomentoFormularioRecadastramento;
    } //-- int getCdMomentoFormularioRecadastramento() 

    /**
     * Returns the value of field
     * 'cdMomentoProcessamentoPagamento'.
     * 
     * @return int
     * @return the value of field 'cdMomentoProcessamentoPagamento'.
     */
    public int getCdMomentoProcessamentoPagamento()
    {
        return this._cdMomentoProcessamentoPagamento;
    } //-- int getCdMomentoProcessamentoPagamento() 

    /**
     * Returns the value of field 'cdNaturezaOperacaoPagamento'.
     * 
     * @return int
     * @return the value of field 'cdNaturezaOperacaoPagamento'.
     */
    public int getCdNaturezaOperacaoPagamento()
    {
        return this._cdNaturezaOperacaoPagamento;
    } //-- int getCdNaturezaOperacaoPagamento() 

    /**
     * Returns the value of field 'cdPagamentoNaoUtil'.
     * 
     * @return int
     * @return the value of field 'cdPagamentoNaoUtil'.
     */
    public int getCdPagamentoNaoUtil()
    {
        return this._cdPagamentoNaoUtil;
    } //-- int getCdPagamentoNaoUtil() 

    /**
     * Returns the value of field 'cdPeriodicidadeAviso'.
     * 
     * @return int
     * @return the value of field 'cdPeriodicidadeAviso'.
     */
    public int getCdPeriodicidadeAviso()
    {
        return this._cdPeriodicidadeAviso;
    } //-- int getCdPeriodicidadeAviso() 

    /**
     * Returns the value of field 'cdPeriodicidadeCobrancaTarifa'.
     * 
     * @return int
     * @return the value of field 'cdPeriodicidadeCobrancaTarifa'.
     */
    public int getCdPeriodicidadeCobrancaTarifa()
    {
        return this._cdPeriodicidadeCobrancaTarifa;
    } //-- int getCdPeriodicidadeCobrancaTarifa() 

    /**
     * Returns the value of field 'cdPeriodicidadeComprovante'.
     * 
     * @return int
     * @return the value of field 'cdPeriodicidadeComprovante'.
     */
    public int getCdPeriodicidadeComprovante()
    {
        return this._cdPeriodicidadeComprovante;
    } //-- int getCdPeriodicidadeComprovante() 

    /**
     * Returns the value of field 'cdPeriodicidadeConsultaVeiculo'.
     * 
     * @return int
     * @return the value of field 'cdPeriodicidadeConsultaVeiculo'.
     */
    public int getCdPeriodicidadeConsultaVeiculo()
    {
        return this._cdPeriodicidadeConsultaVeiculo;
    } //-- int getCdPeriodicidadeConsultaVeiculo() 

    /**
     * Returns the value of field 'cdPeriodicidadeEnvioRemessa'.
     * 
     * @return int
     * @return the value of field 'cdPeriodicidadeEnvioRemessa'.
     */
    public int getCdPeriodicidadeEnvioRemessa()
    {
        return this._cdPeriodicidadeEnvioRemessa;
    } //-- int getCdPeriodicidadeEnvioRemessa() 

    /**
     * Returns the value of field 'cdPeriodicidadeManutencaoProcd'.
     * 
     * @return int
     * @return the value of field 'cdPeriodicidadeManutencaoProcd'.
     */
    public int getCdPeriodicidadeManutencaoProcd()
    {
        return this._cdPeriodicidadeManutencaoProcd;
    } //-- int getCdPeriodicidadeManutencaoProcd() 

    /**
     * Returns the value of field 'cdPermissaoDebitoOnline'.
     * 
     * @return int
     * @return the value of field 'cdPermissaoDebitoOnline'.
     */
    public int getCdPermissaoDebitoOnline()
    {
        return this._cdPermissaoDebitoOnline;
    } //-- int getCdPermissaoDebitoOnline() 

    /**
     * Returns the value of field
     * 'cdPrincipalEnquaRecadastramento'.
     * 
     * @return int
     * @return the value of field 'cdPrincipalEnquaRecadastramento'.
     */
    public int getCdPrincipalEnquaRecadastramento()
    {
        return this._cdPrincipalEnquaRecadastramento;
    } //-- int getCdPrincipalEnquaRecadastramento() 

    /**
     * Returns the value of field
     * 'cdPrioridadeEfetivacaoPagamento'.
     * 
     * @return int
     * @return the value of field 'cdPrioridadeEfetivacaoPagamento'.
     */
    public int getCdPrioridadeEfetivacaoPagamento()
    {
        return this._cdPrioridadeEfetivacaoPagamento;
    } //-- int getCdPrioridadeEfetivacaoPagamento() 

    /**
     * Returns the value of field 'cdRastreabNotaFiscal'.
     * 
     * @return int
     * @return the value of field 'cdRastreabNotaFiscal'.
     */
    public int getCdRastreabNotaFiscal()
    {
        return this._cdRastreabNotaFiscal;
    } //-- int getCdRastreabNotaFiscal() 

    /**
     * Returns the value of field 'cdRastreabTituloTerc'.
     * 
     * @return int
     * @return the value of field 'cdRastreabTituloTerc'.
     */
    public int getCdRastreabTituloTerc()
    {
        return this._cdRastreabTituloTerc;
    } //-- int getCdRastreabTituloTerc() 

    /**
     * Returns the value of field 'cdRejeicaoAgendamentoLote'.
     * 
     * @return int
     * @return the value of field 'cdRejeicaoAgendamentoLote'.
     */
    public int getCdRejeicaoAgendamentoLote()
    {
        return this._cdRejeicaoAgendamentoLote;
    } //-- int getCdRejeicaoAgendamentoLote() 

    /**
     * Returns the value of field 'cdRejeicaoEfetivacaoLote'.
     * 
     * @return int
     * @return the value of field 'cdRejeicaoEfetivacaoLote'.
     */
    public int getCdRejeicaoEfetivacaoLote()
    {
        return this._cdRejeicaoEfetivacaoLote;
    } //-- int getCdRejeicaoEfetivacaoLote() 

    /**
     * Returns the value of field 'cdRejeicaoLote'.
     * 
     * @return int
     * @return the value of field 'cdRejeicaoLote'.
     */
    public int getCdRejeicaoLote()
    {
        return this._cdRejeicaoLote;
    } //-- int getCdRejeicaoLote() 

    /**
     * Returns the value of field 'cdTipoCargaRecadastramento'.
     * 
     * @return int
     * @return the value of field 'cdTipoCargaRecadastramento'.
     */
    public int getCdTipoCargaRecadastramento()
    {
        return this._cdTipoCargaRecadastramento;
    } //-- int getCdTipoCargaRecadastramento() 

    /**
     * Returns the value of field 'cdTipoCartaoSalario'.
     * 
     * @return int
     * @return the value of field 'cdTipoCartaoSalario'.
     */
    public int getCdTipoCartaoSalario()
    {
        return this._cdTipoCartaoSalario;
    } //-- int getCdTipoCartaoSalario() 

    /**
     * Returns the value of field 'cdTipoConsistenciaFavorecido'.
     * 
     * @return int
     * @return the value of field 'cdTipoConsistenciaFavorecido'.
     */
    public int getCdTipoConsistenciaFavorecido()
    {
        return this._cdTipoConsistenciaFavorecido;
    } //-- int getCdTipoConsistenciaFavorecido() 

    /**
     * Returns the value of field 'cdTipoConsistenciaLista'.
     * 
     * @return int
     * @return the value of field 'cdTipoConsistenciaLista'.
     */
    public int getCdTipoConsistenciaLista()
    {
        return this._cdTipoConsistenciaLista;
    } //-- int getCdTipoConsistenciaLista() 

    /**
     * Returns the value of field 'cdTipoConsultaComprovante'.
     * 
     * @return int
     * @return the value of field 'cdTipoConsultaComprovante'.
     */
    public int getCdTipoConsultaComprovante()
    {
        return this._cdTipoConsultaComprovante;
    } //-- int getCdTipoConsultaComprovante() 

    /**
     * Returns the value of field 'cdTipoDataFloating'.
     * 
     * @return int
     * @return the value of field 'cdTipoDataFloating'.
     */
    public int getCdTipoDataFloating()
    {
        return this._cdTipoDataFloating;
    } //-- int getCdTipoDataFloating() 

    /**
     * Returns the value of field 'cdTipoDivergenciaVeiculo'.
     * 
     * @return int
     * @return the value of field 'cdTipoDivergenciaVeiculo'.
     */
    public int getCdTipoDivergenciaVeiculo()
    {
        return this._cdTipoDivergenciaVeiculo;
    } //-- int getCdTipoDivergenciaVeiculo() 

    /**
     * Returns the value of field 'cdTipoFormacaoLista'.
     * 
     * @return int
     * @return the value of field 'cdTipoFormacaoLista'.
     */
    public int getCdTipoFormacaoLista()
    {
        return this._cdTipoFormacaoLista;
    } //-- int getCdTipoFormacaoLista() 

    /**
     * Returns the value of field 'cdTipoIdentificacaoBeneficio'.
     * 
     * @return int
     * @return the value of field 'cdTipoIdentificacaoBeneficio'.
     */
    public int getCdTipoIdentificacaoBeneficio()
    {
        return this._cdTipoIdentificacaoBeneficio;
    } //-- int getCdTipoIdentificacaoBeneficio() 

    /**
     * Returns the value of field
     * 'cdTipoInscricaoFavorecidoAceita'.
     * 
     * @return int
     * @return the value of field 'cdTipoInscricaoFavorecidoAceita'.
     */
    public int getCdTipoInscricaoFavorecidoAceita()
    {
        return this._cdTipoInscricaoFavorecidoAceita;
    } //-- int getCdTipoInscricaoFavorecidoAceita() 

    /**
     * Returns the value of field 'cdTipoLayoutArquivo'.
     * 
     * @return int
     * @return the value of field 'cdTipoLayoutArquivo'.
     */
    public int getCdTipoLayoutArquivo()
    {
        return this._cdTipoLayoutArquivo;
    } //-- int getCdTipoLayoutArquivo() 

    /**
     * Returns the value of field 'cdTipoReajusteTarifa'.
     * 
     * @return int
     * @return the value of field 'cdTipoReajusteTarifa'.
     */
    public int getCdTipoReajusteTarifa()
    {
        return this._cdTipoReajusteTarifa;
    } //-- int getCdTipoReajusteTarifa() 

    /**
     * Returns the value of field 'cdTratoContaTransf'.
     * 
     * @return int
     * @return the value of field 'cdTratoContaTransf'.
     */
    public int getCdTratoContaTransf()
    {
        return this._cdTratoContaTransf;
    } //-- int getCdTratoContaTransf() 

    /**
     * Returns the value of field 'cdUsuarioAlteracao'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioAlteracao'.
     */
    public java.lang.String getCdUsuarioAlteracao()
    {
        return this._cdUsuarioAlteracao;
    } //-- java.lang.String getCdUsuarioAlteracao() 

    /**
     * Returns the value of field 'cdUsuarioExternoAlteracao'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioExternoAlteracao'.
     */
    public java.lang.String getCdUsuarioExternoAlteracao()
    {
        return this._cdUsuarioExternoAlteracao;
    } //-- java.lang.String getCdUsuarioExternoAlteracao() 

    /**
     * Returns the value of field 'cdUsuarioExternoInclusao'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioExternoInclusao'.
     */
    public java.lang.String getCdUsuarioExternoInclusao()
    {
        return this._cdUsuarioExternoInclusao;
    } //-- java.lang.String getCdUsuarioExternoInclusao() 

    /**
     * Returns the value of field 'cdUsuarioInclusao'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioInclusao'.
     */
    public java.lang.String getCdUsuarioInclusao()
    {
        return this._cdUsuarioInclusao;
    } //-- java.lang.String getCdUsuarioInclusao() 

    /**
     * Returns the value of field 'cdUtilizacaoFavorecidoControle'.
     * 
     * @return int
     * @return the value of field 'cdUtilizacaoFavorecidoControle'.
     */
    public int getCdUtilizacaoFavorecidoControle()
    {
        return this._cdUtilizacaoFavorecidoControle;
    } //-- int getCdUtilizacaoFavorecidoControle() 

    /**
     * Returns the value of field 'cdValidacaoNomeFavorecido'.
     * 
     * @return int
     * @return the value of field 'cdValidacaoNomeFavorecido'.
     */
    public int getCdValidacaoNomeFavorecido()
    {
        return this._cdValidacaoNomeFavorecido;
    } //-- int getCdValidacaoNomeFavorecido() 

    /**
     * Returns the value of field 'cdindicadorExpiraCredito'.
     * 
     * @return int
     * @return the value of field 'cdindicadorExpiraCredito'.
     */
    public int getCdindicadorExpiraCredito()
    {
        return this._cdindicadorExpiraCredito;
    } //-- int getCdindicadorExpiraCredito() 

    /**
     * Returns the value of field
     * 'cdindicadorLancamentoProgramado'.
     * 
     * @return int
     * @return the value of field 'cdindicadorLancamentoProgramado'.
     */
    public int getCdindicadorLancamentoProgramado()
    {
        return this._cdindicadorLancamentoProgramado;
    } //-- int getCdindicadorLancamentoProgramado() 

    /**
     * Returns the value of field 'codMensagem'.
     * 
     * @return String
     * @return the value of field 'codMensagem'.
     */
    public java.lang.String getCodMensagem()
    {
        return this._codMensagem;
    } //-- java.lang.String getCodMensagem() 

    /**
     * Returns the value of field 'dsAcaoNaoVida'.
     * 
     * @return String
     * @return the value of field 'dsAcaoNaoVida'.
     */
    public java.lang.String getDsAcaoNaoVida()
    {
        return this._dsAcaoNaoVida;
    } //-- java.lang.String getDsAcaoNaoVida() 

    /**
     * Returns the value of field 'dsAcertoDadosRecadastramento'.
     * 
     * @return String
     * @return the value of field 'dsAcertoDadosRecadastramento'.
     */
    public java.lang.String getDsAcertoDadosRecadastramento()
    {
        return this._dsAcertoDadosRecadastramento;
    } //-- java.lang.String getDsAcertoDadosRecadastramento() 

    /**
     * Returns the value of field 'dsAgendamentoDebitoVeiculo'.
     * 
     * @return String
     * @return the value of field 'dsAgendamentoDebitoVeiculo'.
     */
    public java.lang.String getDsAgendamentoDebitoVeiculo()
    {
        return this._dsAgendamentoDebitoVeiculo;
    } //-- java.lang.String getDsAgendamentoDebitoVeiculo() 

    /**
     * Returns the value of field 'dsAgendamentoPagamentoVencido'.
     * 
     * @return String
     * @return the value of field 'dsAgendamentoPagamentoVencido'.
     */
    public java.lang.String getDsAgendamentoPagamentoVencido()
    {
        return this._dsAgendamentoPagamentoVencido;
    } //-- java.lang.String getDsAgendamentoPagamentoVencido() 

    /**
     * Returns the value of field 'dsAgendamentoRastFilial'.
     * 
     * @return String
     * @return the value of field 'dsAgendamentoRastFilial'.
     */
    public java.lang.String getDsAgendamentoRastFilial()
    {
        return this._dsAgendamentoRastFilial;
    } //-- java.lang.String getDsAgendamentoRastFilial() 

    /**
     * Returns the value of field 'dsAgendamentoValorMenor'.
     * 
     * @return String
     * @return the value of field 'dsAgendamentoValorMenor'.
     */
    public java.lang.String getDsAgendamentoValorMenor()
    {
        return this._dsAgendamentoValorMenor;
    } //-- java.lang.String getDsAgendamentoValorMenor() 

    /**
     * Returns the value of field 'dsAgrupamentoAviso'.
     * 
     * @return String
     * @return the value of field 'dsAgrupamentoAviso'.
     */
    public java.lang.String getDsAgrupamentoAviso()
    {
        return this._dsAgrupamentoAviso;
    } //-- java.lang.String getDsAgrupamentoAviso() 

    /**
     * Returns the value of field 'dsAgrupamentoComprovante'.
     * 
     * @return String
     * @return the value of field 'dsAgrupamentoComprovante'.
     */
    public java.lang.String getDsAgrupamentoComprovante()
    {
        return this._dsAgrupamentoComprovante;
    } //-- java.lang.String getDsAgrupamentoComprovante() 

    /**
     * Returns the value of field
     * 'dsAgrupamentoFormularioRecadastro'.
     * 
     * @return String
     * @return the value of field
     * 'dsAgrupamentoFormularioRecadastro'.
     */
    public java.lang.String getDsAgrupamentoFormularioRecadastro()
    {
        return this._dsAgrupamentoFormularioRecadastro;
    } //-- java.lang.String getDsAgrupamentoFormularioRecadastro() 

    /**
     * Returns the value of field
     * 'dsAntecipacaoRecadastramentoBeneficiario'.
     * 
     * @return String
     * @return the value of field
     * 'dsAntecipacaoRecadastramentoBeneficiario'.
     */
    public java.lang.String getDsAntecipacaoRecadastramentoBeneficiario()
    {
        return this._dsAntecipacaoRecadastramentoBeneficiario;
    } //-- java.lang.String getDsAntecipacaoRecadastramentoBeneficiario() 

    /**
     * Returns the value of field 'dsAreaReservada'.
     * 
     * @return String
     * @return the value of field 'dsAreaReservada'.
     */
    public java.lang.String getDsAreaReservada()
    {
        return this._dsAreaReservada;
    } //-- java.lang.String getDsAreaReservada() 

    /**
     * Returns the value of field 'dsAreaReservada2'.
     * 
     * @return String
     * @return the value of field 'dsAreaReservada2'.
     */
    public java.lang.String getDsAreaReservada2()
    {
        return this._dsAreaReservada2;
    } //-- java.lang.String getDsAreaReservada2() 

    /**
     * Returns the value of field 'dsBaseRecadastramentoBeneficio'.
     * 
     * @return String
     * @return the value of field 'dsBaseRecadastramentoBeneficio'.
     */
    public java.lang.String getDsBaseRecadastramentoBeneficio()
    {
        return this._dsBaseRecadastramentoBeneficio;
    } //-- java.lang.String getDsBaseRecadastramentoBeneficio() 

    /**
     * Returns the value of field 'dsBloqueioEmissaoPapeleta'.
     * 
     * @return String
     * @return the value of field 'dsBloqueioEmissaoPapeleta'.
     */
    public java.lang.String getDsBloqueioEmissaoPapeleta()
    {
        return this._dsBloqueioEmissaoPapeleta;
    } //-- java.lang.String getDsBloqueioEmissaoPapeleta() 

    /**
     * Returns the value of field 'dsCanalAlteracao'.
     * 
     * @return String
     * @return the value of field 'dsCanalAlteracao'.
     */
    public java.lang.String getDsCanalAlteracao()
    {
        return this._dsCanalAlteracao;
    } //-- java.lang.String getDsCanalAlteracao() 

    /**
     * Returns the value of field 'dsCanalInclusao'.
     * 
     * @return String
     * @return the value of field 'dsCanalInclusao'.
     */
    public java.lang.String getDsCanalInclusao()
    {
        return this._dsCanalInclusao;
    } //-- java.lang.String getDsCanalInclusao() 

    /**
     * Returns the value of field 'dsCapturaTituloRegistrado'.
     * 
     * @return String
     * @return the value of field 'dsCapturaTituloRegistrado'.
     */
    public java.lang.String getDsCapturaTituloRegistrado()
    {
        return this._dsCapturaTituloRegistrado;
    } //-- java.lang.String getDsCapturaTituloRegistrado() 

    /**
     * Returns the value of field 'dsCctciaEspeBeneficio'.
     * 
     * @return String
     * @return the value of field 'dsCctciaEspeBeneficio'.
     */
    public java.lang.String getDsCctciaEspeBeneficio()
    {
        return this._dsCctciaEspeBeneficio;
    } //-- java.lang.String getDsCctciaEspeBeneficio() 

    /**
     * Returns the value of field 'dsCctciaIdentificacaoBeneficio'.
     * 
     * @return String
     * @return the value of field 'dsCctciaIdentificacaoBeneficio'.
     */
    public java.lang.String getDsCctciaIdentificacaoBeneficio()
    {
        return this._dsCctciaIdentificacaoBeneficio;
    } //-- java.lang.String getDsCctciaIdentificacaoBeneficio() 

    /**
     * Returns the value of field 'dsCctciaInscricaoFavorecido'.
     * 
     * @return String
     * @return the value of field 'dsCctciaInscricaoFavorecido'.
     */
    public java.lang.String getDsCctciaInscricaoFavorecido()
    {
        return this._dsCctciaInscricaoFavorecido;
    } //-- java.lang.String getDsCctciaInscricaoFavorecido() 

    /**
     * Returns the value of field 'dsCctciaProprietarioVeculo'.
     * 
     * @return String
     * @return the value of field 'dsCctciaProprietarioVeculo'.
     */
    public java.lang.String getDsCctciaProprietarioVeculo()
    {
        return this._dsCctciaProprietarioVeculo;
    } //-- java.lang.String getDsCctciaProprietarioVeculo() 

    /**
     * Returns the value of field 'dsCobrancaTarifa'.
     * 
     * @return String
     * @return the value of field 'dsCobrancaTarifa'.
     */
    public java.lang.String getDsCobrancaTarifa()
    {
        return this._dsCobrancaTarifa;
    } //-- java.lang.String getDsCobrancaTarifa() 

    /**
     * Returns the value of field
     * 'dsCodigoFormularioContratoCliente'.
     * 
     * @return String
     * @return the value of field
     * 'dsCodigoFormularioContratoCliente'.
     */
    public java.lang.String getDsCodigoFormularioContratoCliente()
    {
        return this._dsCodigoFormularioContratoCliente;
    } //-- java.lang.String getDsCodigoFormularioContratoCliente() 

    /**
     * Returns the value of field
     * 'dsCodigoIndicadorRetornoSeparado'.
     * 
     * @return String
     * @return the value of field 'dsCodigoIndicadorRetornoSeparado'
     */
    public java.lang.String getDsCodigoIndicadorRetornoSeparado()
    {
        return this._dsCodigoIndicadorRetornoSeparado;
    } //-- java.lang.String getDsCodigoIndicadorRetornoSeparado() 

    /**
     * Returns the value of field 'dsConsultaDebitoVeiculo'.
     * 
     * @return String
     * @return the value of field 'dsConsultaDebitoVeiculo'.
     */
    public java.lang.String getDsConsultaDebitoVeiculo()
    {
        return this._dsConsultaDebitoVeiculo;
    } //-- java.lang.String getDsConsultaDebitoVeiculo() 

    /**
     * Returns the value of field 'dsConsultaEndereco'.
     * 
     * @return String
     * @return the value of field 'dsConsultaEndereco'.
     */
    public java.lang.String getDsConsultaEndereco()
    {
        return this._dsConsultaEndereco;
    } //-- java.lang.String getDsConsultaEndereco() 

    /**
     * Returns the value of field 'dsConsultaSaldoPagamento'.
     * 
     * @return String
     * @return the value of field 'dsConsultaSaldoPagamento'.
     */
    public java.lang.String getDsConsultaSaldoPagamento()
    {
        return this._dsConsultaSaldoPagamento;
    } //-- java.lang.String getDsConsultaSaldoPagamento() 

    /**
     * Returns the value of field 'dsConsultaSaldoSuperior'.
     * 
     * @return String
     * @return the value of field 'dsConsultaSaldoSuperior'.
     */
    public java.lang.String getDsConsultaSaldoSuperior()
    {
        return this._dsConsultaSaldoSuperior;
    } //-- java.lang.String getDsConsultaSaldoSuperior() 

    /**
     * Returns the value of field 'dsContagemConsultaSaldo'.
     * 
     * @return String
     * @return the value of field 'dsContagemConsultaSaldo'.
     */
    public java.lang.String getDsContagemConsultaSaldo()
    {
        return this._dsContagemConsultaSaldo;
    } //-- java.lang.String getDsContagemConsultaSaldo() 

    /**
     * Returns the value of field 'dsCreditoNaoUtilizado'.
     * 
     * @return String
     * @return the value of field 'dsCreditoNaoUtilizado'.
     */
    public java.lang.String getDsCreditoNaoUtilizado()
    {
        return this._dsCreditoNaoUtilizado;
    } //-- java.lang.String getDsCreditoNaoUtilizado() 

    /**
     * Returns the value of field
     * 'dsCriterioEnquadraRecadastramento'.
     * 
     * @return String
     * @return the value of field
     * 'dsCriterioEnquadraRecadastramento'.
     */
    public java.lang.String getDsCriterioEnquadraRecadastramento()
    {
        return this._dsCriterioEnquadraRecadastramento;
    } //-- java.lang.String getDsCriterioEnquadraRecadastramento() 

    /**
     * Returns the value of field 'dsCriterioEnquandraBeneficio'.
     * 
     * @return String
     * @return the value of field 'dsCriterioEnquandraBeneficio'.
     */
    public java.lang.String getDsCriterioEnquandraBeneficio()
    {
        return this._dsCriterioEnquandraBeneficio;
    } //-- java.lang.String getDsCriterioEnquandraBeneficio() 

    /**
     * Returns the value of field
     * 'dsCriterioRastreabilidadeTitulo'.
     * 
     * @return String
     * @return the value of field 'dsCriterioRastreabilidadeTitulo'.
     */
    public java.lang.String getDsCriterioRastreabilidadeTitulo()
    {
        return this._dsCriterioRastreabilidadeTitulo;
    } //-- java.lang.String getDsCriterioRastreabilidadeTitulo() 

    /**
     * Returns the value of field 'dsDestinoAviso'.
     * 
     * @return String
     * @return the value of field 'dsDestinoAviso'.
     */
    public java.lang.String getDsDestinoAviso()
    {
        return this._dsDestinoAviso;
    } //-- java.lang.String getDsDestinoAviso() 

    /**
     * Returns the value of field 'dsDestinoComprovante'.
     * 
     * @return String
     * @return the value of field 'dsDestinoComprovante'.
     */
    public java.lang.String getDsDestinoComprovante()
    {
        return this._dsDestinoComprovante;
    } //-- java.lang.String getDsDestinoComprovante() 

    /**
     * Returns the value of field
     * 'dsDestinoFormularioRecadastramento'.
     * 
     * @return String
     * @return the value of field
     * 'dsDestinoFormularioRecadastramento'.
     */
    public java.lang.String getDsDestinoFormularioRecadastramento()
    {
        return this._dsDestinoFormularioRecadastramento;
    } //-- java.lang.String getDsDestinoFormularioRecadastramento() 

    /**
     * Returns the value of field 'dsDisponibilizacaoContaCredito'.
     * 
     * @return String
     * @return the value of field 'dsDisponibilizacaoContaCredito'.
     */
    public java.lang.String getDsDisponibilizacaoContaCredito()
    {
        return this._dsDisponibilizacaoContaCredito;
    } //-- java.lang.String getDsDisponibilizacaoContaCredito() 

    /**
     * Returns the value of field
     * 'dsDisponibilizacaoDiversoCriterio'.
     * 
     * @return String
     * @return the value of field
     * 'dsDisponibilizacaoDiversoCriterio'.
     */
    public java.lang.String getDsDisponibilizacaoDiversoCriterio()
    {
        return this._dsDisponibilizacaoDiversoCriterio;
    } //-- java.lang.String getDsDisponibilizacaoDiversoCriterio() 

    /**
     * Returns the value of field 'dsDisponibilizacaoDiversoNao'.
     * 
     * @return String
     * @return the value of field 'dsDisponibilizacaoDiversoNao'.
     */
    public java.lang.String getDsDisponibilizacaoDiversoNao()
    {
        return this._dsDisponibilizacaoDiversoNao;
    } //-- java.lang.String getDsDisponibilizacaoDiversoNao() 

    /**
     * Returns the value of field
     * 'dsDisponibilizacaoSalarioCriterio'.
     * 
     * @return String
     * @return the value of field
     * 'dsDisponibilizacaoSalarioCriterio'.
     */
    public java.lang.String getDsDisponibilizacaoSalarioCriterio()
    {
        return this._dsDisponibilizacaoSalarioCriterio;
    } //-- java.lang.String getDsDisponibilizacaoSalarioCriterio() 

    /**
     * Returns the value of field 'dsDisponibilizacaoSalarioNao'.
     * 
     * @return String
     * @return the value of field 'dsDisponibilizacaoSalarioNao'.
     */
    public java.lang.String getDsDisponibilizacaoSalarioNao()
    {
        return this._dsDisponibilizacaoSalarioNao;
    } //-- java.lang.String getDsDisponibilizacaoSalarioNao() 

    /**
     * Returns the value of field 'dsEnvelopeAberto'.
     * 
     * @return String
     * @return the value of field 'dsEnvelopeAberto'.
     */
    public java.lang.String getDsEnvelopeAberto()
    {
        return this._dsEnvelopeAberto;
    } //-- java.lang.String getDsEnvelopeAberto() 

    /**
     * Returns the value of field 'dsFavorecidoConsultaPagamento'.
     * 
     * @return String
     * @return the value of field 'dsFavorecidoConsultaPagamento'.
     */
    public java.lang.String getDsFavorecidoConsultaPagamento()
    {
        return this._dsFavorecidoConsultaPagamento;
    } //-- java.lang.String getDsFavorecidoConsultaPagamento() 

    /**
     * Returns the value of field 'dsFormaAutorizacaoPagamento'.
     * 
     * @return String
     * @return the value of field 'dsFormaAutorizacaoPagamento'.
     */
    public java.lang.String getDsFormaAutorizacaoPagamento()
    {
        return this._dsFormaAutorizacaoPagamento;
    } //-- java.lang.String getDsFormaAutorizacaoPagamento() 

    /**
     * Returns the value of field 'dsFormaEnvioPagamento'.
     * 
     * @return String
     * @return the value of field 'dsFormaEnvioPagamento'.
     */
    public java.lang.String getDsFormaEnvioPagamento()
    {
        return this._dsFormaEnvioPagamento;
    } //-- java.lang.String getDsFormaEnvioPagamento() 

    /**
     * Returns the value of field 'dsFormaExpiracaoCredito'.
     * 
     * @return String
     * @return the value of field 'dsFormaExpiracaoCredito'.
     */
    public java.lang.String getDsFormaExpiracaoCredito()
    {
        return this._dsFormaExpiracaoCredito;
    } //-- java.lang.String getDsFormaExpiracaoCredito() 

    /**
     * Returns the value of field 'dsFormaManutencao'.
     * 
     * @return String
     * @return the value of field 'dsFormaManutencao'.
     */
    public java.lang.String getDsFormaManutencao()
    {
        return this._dsFormaManutencao;
    } //-- java.lang.String getDsFormaManutencao() 

    /**
     * Returns the value of field 'dsFrasePrecadastrada'.
     * 
     * @return String
     * @return the value of field 'dsFrasePrecadastrada'.
     */
    public java.lang.String getDsFrasePrecadastrada()
    {
        return this._dsFrasePrecadastrada;
    } //-- java.lang.String getDsFrasePrecadastrada() 

    /**
     * Returns the value of field 'dsIndLancamentoPersonalizado'.
     * 
     * @return String
     * @return the value of field 'dsIndLancamentoPersonalizado'.
     */
    public java.lang.String getDsIndLancamentoPersonalizado()
    {
        return this._dsIndLancamentoPersonalizado;
    } //-- java.lang.String getDsIndLancamentoPersonalizado() 

    /**
     * Returns the value of field 'dsIndicadorAdesaoSacador'.
     * 
     * @return String
     * @return the value of field 'dsIndicadorAdesaoSacador'.
     */
    public java.lang.String getDsIndicadorAdesaoSacador()
    {
        return this._dsIndicadorAdesaoSacador;
    } //-- java.lang.String getDsIndicadorAdesaoSacador() 

    /**
     * Returns the value of field 'dsIndicadorAgendamentoTitulo'.
     * 
     * @return String
     * @return the value of field 'dsIndicadorAgendamentoTitulo'.
     */
    public java.lang.String getDsIndicadorAgendamentoTitulo()
    {
        return this._dsIndicadorAgendamentoTitulo;
    } //-- java.lang.String getDsIndicadorAgendamentoTitulo() 

    /**
     * Returns the value of field 'dsIndicadorAutorizacaoCliente'.
     * 
     * @return String
     * @return the value of field 'dsIndicadorAutorizacaoCliente'.
     */
    public java.lang.String getDsIndicadorAutorizacaoCliente()
    {
        return this._dsIndicadorAutorizacaoCliente;
    } //-- java.lang.String getDsIndicadorAutorizacaoCliente() 

    /**
     * Returns the value of field
     * 'dsIndicadorAutorizacaoComplemento'.
     * 
     * @return String
     * @return the value of field
     * 'dsIndicadorAutorizacaoComplemento'.
     */
    public java.lang.String getDsIndicadorAutorizacaoComplemento()
    {
        return this._dsIndicadorAutorizacaoComplemento;
    } //-- java.lang.String getDsIndicadorAutorizacaoComplemento() 

    /**
     * Returns the value of field 'dsIndicadorBancoPostal'.
     * 
     * @return String
     * @return the value of field 'dsIndicadorBancoPostal'.
     */
    public java.lang.String getDsIndicadorBancoPostal()
    {
        return this._dsIndicadorBancoPostal;
    } //-- java.lang.String getDsIndicadorBancoPostal() 

    /**
     * Returns the value of field 'dsIndicadorCadastroProcurador'.
     * 
     * @return String
     * @return the value of field 'dsIndicadorCadastroProcurador'.
     */
    public java.lang.String getDsIndicadorCadastroProcurador()
    {
        return this._dsIndicadorCadastroProcurador;
    } //-- java.lang.String getDsIndicadorCadastroProcurador() 

    /**
     * Returns the value of field 'dsIndicadorCadastroorganizacao'.
     * 
     * @return String
     * @return the value of field 'dsIndicadorCadastroorganizacao'.
     */
    public java.lang.String getDsIndicadorCadastroorganizacao()
    {
        return this._dsIndicadorCadastroorganizacao;
    } //-- java.lang.String getDsIndicadorCadastroorganizacao() 

    /**
     * Returns the value of field 'dsIndicadorCartaoSalario'.
     * 
     * @return String
     * @return the value of field 'dsIndicadorCartaoSalario'.
     */
    public java.lang.String getDsIndicadorCartaoSalario()
    {
        return this._dsIndicadorCartaoSalario;
    } //-- java.lang.String getDsIndicadorCartaoSalario() 

    /**
     * Returns the value of field 'dsIndicadorEconomicoReajuste'.
     * 
     * @return String
     * @return the value of field 'dsIndicadorEconomicoReajuste'.
     */
    public java.lang.String getDsIndicadorEconomicoReajuste()
    {
        return this._dsIndicadorEconomicoReajuste;
    } //-- java.lang.String getDsIndicadorEconomicoReajuste() 

    /**
     * Returns the value of field 'dsIndicadorEmissaoAviso'.
     * 
     * @return String
     * @return the value of field 'dsIndicadorEmissaoAviso'.
     */
    public java.lang.String getDsIndicadorEmissaoAviso()
    {
        return this._dsIndicadorEmissaoAviso;
    } //-- java.lang.String getDsIndicadorEmissaoAviso() 

    /**
     * Returns the value of field 'dsIndicadorListaDebito'.
     * 
     * @return String
     * @return the value of field 'dsIndicadorListaDebito'.
     */
    public java.lang.String getDsIndicadorListaDebito()
    {
        return this._dsIndicadorListaDebito;
    } //-- java.lang.String getDsIndicadorListaDebito() 

    /**
     * Returns the value of field
     * 'dsIndicadorMensagemPersonalizada'.
     * 
     * @return String
     * @return the value of field 'dsIndicadorMensagemPersonalizada'
     */
    public java.lang.String getDsIndicadorMensagemPersonalizada()
    {
        return this._dsIndicadorMensagemPersonalizada;
    } //-- java.lang.String getDsIndicadorMensagemPersonalizada() 

    /**
     * Returns the value of field 'dsIndicadorRetornoInternet'.
     * 
     * @return String
     * @return the value of field 'dsIndicadorRetornoInternet'.
     */
    public java.lang.String getDsIndicadorRetornoInternet()
    {
        return this._dsIndicadorRetornoInternet;
    } //-- java.lang.String getDsIndicadorRetornoInternet() 

    /**
     * Returns the value of field 'dsLancamentoFuturoCredito'.
     * 
     * @return String
     * @return the value of field 'dsLancamentoFuturoCredito'.
     */
    public java.lang.String getDsLancamentoFuturoCredito()
    {
        return this._dsLancamentoFuturoCredito;
    } //-- java.lang.String getDsLancamentoFuturoCredito() 

    /**
     * Returns the value of field 'dsLancamentoFuturoDebito'.
     * 
     * @return String
     * @return the value of field 'dsLancamentoFuturoDebito'.
     */
    public java.lang.String getDsLancamentoFuturoDebito()
    {
        return this._dsLancamentoFuturoDebito;
    } //-- java.lang.String getDsLancamentoFuturoDebito() 

    /**
     * Returns the value of field 'dsLiberacaoLoteProcessado'.
     * 
     * @return String
     * @return the value of field 'dsLiberacaoLoteProcessado'.
     */
    public java.lang.String getDsLiberacaoLoteProcessado()
    {
        return this._dsLiberacaoLoteProcessado;
    } //-- java.lang.String getDsLiberacaoLoteProcessado() 

    /**
     * Returns the value of field 'dsLocalEmissao'.
     * 
     * @return String
     * @return the value of field 'dsLocalEmissao'.
     */
    public java.lang.String getDsLocalEmissao()
    {
        return this._dsLocalEmissao;
    } //-- java.lang.String getDsLocalEmissao() 

    /**
     * Returns the value of field
     * 'dsManutencaoBaseRecadastramento'.
     * 
     * @return String
     * @return the value of field 'dsManutencaoBaseRecadastramento'.
     */
    public java.lang.String getDsManutencaoBaseRecadastramento()
    {
        return this._dsManutencaoBaseRecadastramento;
    } //-- java.lang.String getDsManutencaoBaseRecadastramento() 

    /**
     * Returns the value of field 'dsMeioPagamentoCredito'.
     * 
     * @return String
     * @return the value of field 'dsMeioPagamentoCredito'.
     */
    public java.lang.String getDsMeioPagamentoCredito()
    {
        return this._dsMeioPagamentoCredito;
    } //-- java.lang.String getDsMeioPagamentoCredito() 

    /**
     * Returns the value of field 'dsMensagemRecadastramentoMidia'.
     * 
     * @return String
     * @return the value of field 'dsMensagemRecadastramentoMidia'.
     */
    public java.lang.String getDsMensagemRecadastramentoMidia()
    {
        return this._dsMensagemRecadastramentoMidia;
    } //-- java.lang.String getDsMensagemRecadastramentoMidia() 

    /**
     * Returns the value of field 'dsMidiaDisponivel'.
     * 
     * @return String
     * @return the value of field 'dsMidiaDisponivel'.
     */
    public java.lang.String getDsMidiaDisponivel()
    {
        return this._dsMidiaDisponivel;
    } //-- java.lang.String getDsMidiaDisponivel() 

    /**
     * Returns the value of field 'dsMidiaMensagemRecadastramento'.
     * 
     * @return String
     * @return the value of field 'dsMidiaMensagemRecadastramento'.
     */
    public java.lang.String getDsMidiaMensagemRecadastramento()
    {
        return this._dsMidiaMensagemRecadastramento;
    } //-- java.lang.String getDsMidiaMensagemRecadastramento() 

    /**
     * Returns the value of field 'dsMomentoAvisoRecadastramento'.
     * 
     * @return String
     * @return the value of field 'dsMomentoAvisoRecadastramento'.
     */
    public java.lang.String getDsMomentoAvisoRecadastramento()
    {
        return this._dsMomentoAvisoRecadastramento;
    } //-- java.lang.String getDsMomentoAvisoRecadastramento() 

    /**
     * Returns the value of field 'dsMomentoCreditoEfetivacao'.
     * 
     * @return String
     * @return the value of field 'dsMomentoCreditoEfetivacao'.
     */
    public java.lang.String getDsMomentoCreditoEfetivacao()
    {
        return this._dsMomentoCreditoEfetivacao;
    } //-- java.lang.String getDsMomentoCreditoEfetivacao() 

    /**
     * Returns the value of field 'dsMomentoDebitoPagamento'.
     * 
     * @return String
     * @return the value of field 'dsMomentoDebitoPagamento'.
     */
    public java.lang.String getDsMomentoDebitoPagamento()
    {
        return this._dsMomentoDebitoPagamento;
    } //-- java.lang.String getDsMomentoDebitoPagamento() 

    /**
     * Returns the value of field
     * 'dsMomentoFormularioRecadastramento'.
     * 
     * @return String
     * @return the value of field
     * 'dsMomentoFormularioRecadastramento'.
     */
    public java.lang.String getDsMomentoFormularioRecadastramento()
    {
        return this._dsMomentoFormularioRecadastramento;
    } //-- java.lang.String getDsMomentoFormularioRecadastramento() 

    /**
     * Returns the value of field
     * 'dsMomentoProcessamentoPagamento'.
     * 
     * @return String
     * @return the value of field 'dsMomentoProcessamentoPagamento'.
     */
    public java.lang.String getDsMomentoProcessamentoPagamento()
    {
        return this._dsMomentoProcessamentoPagamento;
    } //-- java.lang.String getDsMomentoProcessamentoPagamento() 

    /**
     * Returns the value of field 'dsNaturezaOperacaoPagamento'.
     * 
     * @return String
     * @return the value of field 'dsNaturezaOperacaoPagamento'.
     */
    public java.lang.String getDsNaturezaOperacaoPagamento()
    {
        return this._dsNaturezaOperacaoPagamento;
    } //-- java.lang.String getDsNaturezaOperacaoPagamento() 

    /**
     * Returns the value of field 'dsPagamentoNaoUtil'.
     * 
     * @return String
     * @return the value of field 'dsPagamentoNaoUtil'.
     */
    public java.lang.String getDsPagamentoNaoUtil()
    {
        return this._dsPagamentoNaoUtil;
    } //-- java.lang.String getDsPagamentoNaoUtil() 

    /**
     * Returns the value of field 'dsPeriodicidadeAviso'.
     * 
     * @return String
     * @return the value of field 'dsPeriodicidadeAviso'.
     */
    public java.lang.String getDsPeriodicidadeAviso()
    {
        return this._dsPeriodicidadeAviso;
    } //-- java.lang.String getDsPeriodicidadeAviso() 

    /**
     * Returns the value of field 'dsPeriodicidadeCobrancaTarifa'.
     * 
     * @return String
     * @return the value of field 'dsPeriodicidadeCobrancaTarifa'.
     */
    public java.lang.String getDsPeriodicidadeCobrancaTarifa()
    {
        return this._dsPeriodicidadeCobrancaTarifa;
    } //-- java.lang.String getDsPeriodicidadeCobrancaTarifa() 

    /**
     * Returns the value of field 'dsPeriodicidadeComprovante'.
     * 
     * @return String
     * @return the value of field 'dsPeriodicidadeComprovante'.
     */
    public java.lang.String getDsPeriodicidadeComprovante()
    {
        return this._dsPeriodicidadeComprovante;
    } //-- java.lang.String getDsPeriodicidadeComprovante() 

    /**
     * Returns the value of field 'dsPeriodicidadeConsultaVeiculo'.
     * 
     * @return String
     * @return the value of field 'dsPeriodicidadeConsultaVeiculo'.
     */
    public java.lang.String getDsPeriodicidadeConsultaVeiculo()
    {
        return this._dsPeriodicidadeConsultaVeiculo;
    } //-- java.lang.String getDsPeriodicidadeConsultaVeiculo() 

    /**
     * Returns the value of field 'dsPeriodicidadeEnvioRemessa'.
     * 
     * @return String
     * @return the value of field 'dsPeriodicidadeEnvioRemessa'.
     */
    public java.lang.String getDsPeriodicidadeEnvioRemessa()
    {
        return this._dsPeriodicidadeEnvioRemessa;
    } //-- java.lang.String getDsPeriodicidadeEnvioRemessa() 

    /**
     * Returns the value of field 'dsPeriodicidadeManutencaoProcd'.
     * 
     * @return String
     * @return the value of field 'dsPeriodicidadeManutencaoProcd'.
     */
    public java.lang.String getDsPeriodicidadeManutencaoProcd()
    {
        return this._dsPeriodicidadeManutencaoProcd;
    } //-- java.lang.String getDsPeriodicidadeManutencaoProcd() 

    /**
     * Returns the value of field 'dsPermissaoDebitoOnline'.
     * 
     * @return String
     * @return the value of field 'dsPermissaoDebitoOnline'.
     */
    public java.lang.String getDsPermissaoDebitoOnline()
    {
        return this._dsPermissaoDebitoOnline;
    } //-- java.lang.String getDsPermissaoDebitoOnline() 

    /**
     * Returns the value of field
     * 'dsPrincipalEnquaRecadastramento'.
     * 
     * @return String
     * @return the value of field 'dsPrincipalEnquaRecadastramento'.
     */
    public java.lang.String getDsPrincipalEnquaRecadastramento()
    {
        return this._dsPrincipalEnquaRecadastramento;
    } //-- java.lang.String getDsPrincipalEnquaRecadastramento() 

    /**
     * Returns the value of field
     * 'dsPrioridadeEfetivacaoPagamento'.
     * 
     * @return String
     * @return the value of field 'dsPrioridadeEfetivacaoPagamento'.
     */
    public java.lang.String getDsPrioridadeEfetivacaoPagamento()
    {
        return this._dsPrioridadeEfetivacaoPagamento;
    } //-- java.lang.String getDsPrioridadeEfetivacaoPagamento() 

    /**
     * Returns the value of field 'dsRastreabilidadeNotaFiscal'.
     * 
     * @return String
     * @return the value of field 'dsRastreabilidadeNotaFiscal'.
     */
    public java.lang.String getDsRastreabilidadeNotaFiscal()
    {
        return this._dsRastreabilidadeNotaFiscal;
    } //-- java.lang.String getDsRastreabilidadeNotaFiscal() 

    /**
     * Returns the value of field
     * 'dsRastreabilidadeTituloTerceiro'.
     * 
     * @return String
     * @return the value of field 'dsRastreabilidadeTituloTerceiro'.
     */
    public java.lang.String getDsRastreabilidadeTituloTerceiro()
    {
        return this._dsRastreabilidadeTituloTerceiro;
    } //-- java.lang.String getDsRastreabilidadeTituloTerceiro() 

    /**
     * Returns the value of field 'dsRejeicaoAgendamentoLote'.
     * 
     * @return String
     * @return the value of field 'dsRejeicaoAgendamentoLote'.
     */
    public java.lang.String getDsRejeicaoAgendamentoLote()
    {
        return this._dsRejeicaoAgendamentoLote;
    } //-- java.lang.String getDsRejeicaoAgendamentoLote() 

    /**
     * Returns the value of field 'dsRejeicaoEfetivacaoLote'.
     * 
     * @return String
     * @return the value of field 'dsRejeicaoEfetivacaoLote'.
     */
    public java.lang.String getDsRejeicaoEfetivacaoLote()
    {
        return this._dsRejeicaoEfetivacaoLote;
    } //-- java.lang.String getDsRejeicaoEfetivacaoLote() 

    /**
     * Returns the value of field 'dsRejeicaoLote'.
     * 
     * @return String
     * @return the value of field 'dsRejeicaoLote'.
     */
    public java.lang.String getDsRejeicaoLote()
    {
        return this._dsRejeicaoLote;
    } //-- java.lang.String getDsRejeicaoLote() 

    /**
     * Returns the value of field 'dsTipoCargaRecadastramento'.
     * 
     * @return String
     * @return the value of field 'dsTipoCargaRecadastramento'.
     */
    public java.lang.String getDsTipoCargaRecadastramento()
    {
        return this._dsTipoCargaRecadastramento;
    } //-- java.lang.String getDsTipoCargaRecadastramento() 

    /**
     * Returns the value of field 'dsTipoCartaoSalario'.
     * 
     * @return String
     * @return the value of field 'dsTipoCartaoSalario'.
     */
    public java.lang.String getDsTipoCartaoSalario()
    {
        return this._dsTipoCartaoSalario;
    } //-- java.lang.String getDsTipoCartaoSalario() 

    /**
     * Returns the value of field 'dsTipoConsistenciaFavorecido'.
     * 
     * @return String
     * @return the value of field 'dsTipoConsistenciaFavorecido'.
     */
    public java.lang.String getDsTipoConsistenciaFavorecido()
    {
        return this._dsTipoConsistenciaFavorecido;
    } //-- java.lang.String getDsTipoConsistenciaFavorecido() 

    /**
     * Returns the value of field 'dsTipoConsistenciaLista'.
     * 
     * @return String
     * @return the value of field 'dsTipoConsistenciaLista'.
     */
    public java.lang.String getDsTipoConsistenciaLista()
    {
        return this._dsTipoConsistenciaLista;
    } //-- java.lang.String getDsTipoConsistenciaLista() 

    /**
     * Returns the value of field 'dsTipoConsolidacaoComprovante'.
     * 
     * @return String
     * @return the value of field 'dsTipoConsolidacaoComprovante'.
     */
    public java.lang.String getDsTipoConsolidacaoComprovante()
    {
        return this._dsTipoConsolidacaoComprovante;
    } //-- java.lang.String getDsTipoConsolidacaoComprovante() 

    /**
     * Returns the value of field 'dsTipoDataFloating'.
     * 
     * @return String
     * @return the value of field 'dsTipoDataFloating'.
     */
    public java.lang.String getDsTipoDataFloating()
    {
        return this._dsTipoDataFloating;
    } //-- java.lang.String getDsTipoDataFloating() 

    /**
     * Returns the value of field 'dsTipoDivergenciaVeiculo'.
     * 
     * @return String
     * @return the value of field 'dsTipoDivergenciaVeiculo'.
     */
    public java.lang.String getDsTipoDivergenciaVeiculo()
    {
        return this._dsTipoDivergenciaVeiculo;
    } //-- java.lang.String getDsTipoDivergenciaVeiculo() 

    /**
     * Returns the value of field 'dsTipoFormacaoLista'.
     * 
     * @return String
     * @return the value of field 'dsTipoFormacaoLista'.
     */
    public java.lang.String getDsTipoFormacaoLista()
    {
        return this._dsTipoFormacaoLista;
    } //-- java.lang.String getDsTipoFormacaoLista() 

    /**
     * Returns the value of field 'dsTipoIdentificacaoBeneficio'.
     * 
     * @return String
     * @return the value of field 'dsTipoIdentificacaoBeneficio'.
     */
    public java.lang.String getDsTipoIdentificacaoBeneficio()
    {
        return this._dsTipoIdentificacaoBeneficio;
    } //-- java.lang.String getDsTipoIdentificacaoBeneficio() 

    /**
     * Returns the value of field 'dsTipoIscricaoFavorecido'.
     * 
     * @return String
     * @return the value of field 'dsTipoIscricaoFavorecido'.
     */
    public java.lang.String getDsTipoIscricaoFavorecido()
    {
        return this._dsTipoIscricaoFavorecido;
    } //-- java.lang.String getDsTipoIscricaoFavorecido() 

    /**
     * Returns the value of field 'dsTipoLayoutArquivo'.
     * 
     * @return String
     * @return the value of field 'dsTipoLayoutArquivo'.
     */
    public java.lang.String getDsTipoLayoutArquivo()
    {
        return this._dsTipoLayoutArquivo;
    } //-- java.lang.String getDsTipoLayoutArquivo() 

    /**
     * Returns the value of field 'dsTipoReajusteTarifa'.
     * 
     * @return String
     * @return the value of field 'dsTipoReajusteTarifa'.
     */
    public java.lang.String getDsTipoReajusteTarifa()
    {
        return this._dsTipoReajusteTarifa;
    } //-- java.lang.String getDsTipoReajusteTarifa() 

    /**
     * Returns the value of field 'dsTratamentoContaTransferida'.
     * 
     * @return String
     * @return the value of field 'dsTratamentoContaTransferida'.
     */
    public java.lang.String getDsTratamentoContaTransferida()
    {
        return this._dsTratamentoContaTransferida;
    } //-- java.lang.String getDsTratamentoContaTransferida() 

    /**
     * Returns the value of field 'dsUtilizacaoFavorecidoControle'.
     * 
     * @return String
     * @return the value of field 'dsUtilizacaoFavorecidoControle'.
     */
    public java.lang.String getDsUtilizacaoFavorecidoControle()
    {
        return this._dsUtilizacaoFavorecidoControle;
    } //-- java.lang.String getDsUtilizacaoFavorecidoControle() 

    /**
     * Returns the value of field 'dsValidacaoNomeFavorecido'.
     * 
     * @return String
     * @return the value of field 'dsValidacaoNomeFavorecido'.
     */
    public java.lang.String getDsValidacaoNomeFavorecido()
    {
        return this._dsValidacaoNomeFavorecido;
    } //-- java.lang.String getDsValidacaoNomeFavorecido() 

    /**
     * Returns the value of field 'dsindicadorExpiraCredito'.
     * 
     * @return String
     * @return the value of field 'dsindicadorExpiraCredito'.
     */
    public java.lang.String getDsindicadorExpiraCredito()
    {
        return this._dsindicadorExpiraCredito;
    } //-- java.lang.String getDsindicadorExpiraCredito() 

    /**
     * Returns the value of field
     * 'dsindicadorLancamentoProgramado'.
     * 
     * @return String
     * @return the value of field 'dsindicadorLancamentoProgramado'.
     */
    public java.lang.String getDsindicadorLancamentoProgramado()
    {
        return this._dsindicadorLancamentoProgramado;
    } //-- java.lang.String getDsindicadorLancamentoProgramado() 

    /**
     * Returns the value of field 'dtEnquaContaSalario'.
     * 
     * @return String
     * @return the value of field 'dtEnquaContaSalario'.
     */
    public java.lang.String getDtEnquaContaSalario()
    {
        return this._dtEnquaContaSalario;
    } //-- java.lang.String getDtEnquaContaSalario() 

    /**
     * Returns the value of field 'dtFimAcertoRecadastramento'.
     * 
     * @return String
     * @return the value of field 'dtFimAcertoRecadastramento'.
     */
    public java.lang.String getDtFimAcertoRecadastramento()
    {
        return this._dtFimAcertoRecadastramento;
    } //-- java.lang.String getDtFimAcertoRecadastramento() 

    /**
     * Returns the value of field 'dtFimRecadastramentoBeneficio'.
     * 
     * @return String
     * @return the value of field 'dtFimRecadastramentoBeneficio'.
     */
    public java.lang.String getDtFimRecadastramentoBeneficio()
    {
        return this._dtFimRecadastramentoBeneficio;
    } //-- java.lang.String getDtFimRecadastramentoBeneficio() 

    /**
     * Returns the value of field 'dtInicioAcertoRecadastramento'.
     * 
     * @return String
     * @return the value of field 'dtInicioAcertoRecadastramento'.
     */
    public java.lang.String getDtInicioAcertoRecadastramento()
    {
        return this._dtInicioAcertoRecadastramento;
    } //-- java.lang.String getDtInicioAcertoRecadastramento() 

    /**
     * Returns the value of field 'dtInicioBloqueioPplta'.
     * 
     * @return String
     * @return the value of field 'dtInicioBloqueioPplta'.
     */
    public java.lang.String getDtInicioBloqueioPplta()
    {
        return this._dtInicioBloqueioPplta;
    } //-- java.lang.String getDtInicioBloqueioPplta() 

    /**
     * Returns the value of field 'dtInicioRastreabTitulo'.
     * 
     * @return String
     * @return the value of field 'dtInicioRastreabTitulo'.
     */
    public java.lang.String getDtInicioRastreabTitulo()
    {
        return this._dtInicioRastreabTitulo;
    } //-- java.lang.String getDtInicioRastreabTitulo() 

    /**
     * Returns the value of field
     * 'dtInicioRecadastramentoBeneficio'.
     * 
     * @return String
     * @return the value of field 'dtInicioRecadastramentoBeneficio'
     */
    public java.lang.String getDtInicioRecadastramentoBeneficio()
    {
        return this._dtInicioRecadastramentoBeneficio;
    } //-- java.lang.String getDtInicioRecadastramentoBeneficio() 

    /**
     * Returns the value of field 'dtLimiteVinculoCarga'.
     * 
     * @return String
     * @return the value of field 'dtLimiteVinculoCarga'.
     */
    public java.lang.String getDtLimiteVinculoCarga()
    {
        return this._dtLimiteVinculoCarga;
    } //-- java.lang.String getDtLimiteVinculoCarga() 

    /**
     * Returns the value of field 'hrManutencaoRegistroAlteracao'.
     * 
     * @return String
     * @return the value of field 'hrManutencaoRegistroAlteracao'.
     */
    public java.lang.String getHrManutencaoRegistroAlteracao()
    {
        return this._hrManutencaoRegistroAlteracao;
    } //-- java.lang.String getHrManutencaoRegistroAlteracao() 

    /**
     * Returns the value of field 'hrManutencaoRegistroInclusao'.
     * 
     * @return String
     * @return the value of field 'hrManutencaoRegistroInclusao'.
     */
    public java.lang.String getHrManutencaoRegistroInclusao()
    {
        return this._hrManutencaoRegistroInclusao;
    } //-- java.lang.String getHrManutencaoRegistroInclusao() 

    /**
     * Returns the value of field 'mensagem'.
     * 
     * @return String
     * @return the value of field 'mensagem'.
     */
    public java.lang.String getMensagem()
    {
        return this._mensagem;
    } //-- java.lang.String getMensagem() 

    /**
     * Returns the value of field 'nrFechamentoApuracaoTarifa'.
     * 
     * @return int
     * @return the value of field 'nrFechamentoApuracaoTarifa'.
     */
    public int getNrFechamentoApuracaoTarifa()
    {
        return this._nrFechamentoApuracaoTarifa;
    } //-- int getNrFechamentoApuracaoTarifa() 

    /**
     * Returns the value of field 'nrOperacaoFluxoAlteracao'.
     * 
     * @return String
     * @return the value of field 'nrOperacaoFluxoAlteracao'.
     */
    public java.lang.String getNrOperacaoFluxoAlteracao()
    {
        return this._nrOperacaoFluxoAlteracao;
    } //-- java.lang.String getNrOperacaoFluxoAlteracao() 

    /**
     * Returns the value of field 'nrOperacaoFluxoInclusao'.
     * 
     * @return String
     * @return the value of field 'nrOperacaoFluxoInclusao'.
     */
    public java.lang.String getNrOperacaoFluxoInclusao()
    {
        return this._nrOperacaoFluxoInclusao;
    } //-- java.lang.String getNrOperacaoFluxoInclusao() 

    /**
     * Returns the value of field 'percentualIndiceReajusteTarifa'.
     * 
     * @return BigDecimal
     * @return the value of field 'percentualIndiceReajusteTarifa'.
     */
    public java.math.BigDecimal getPercentualIndiceReajusteTarifa()
    {
        return this._percentualIndiceReajusteTarifa;
    } //-- java.math.BigDecimal getPercentualIndiceReajusteTarifa() 

    /**
     * Returns the value of field
     * 'percentualMaximoInconsistenteLote'.
     * 
     * @return int
     * @return the value of field
     * 'percentualMaximoInconsistenteLote'.
     */
    public int getPercentualMaximoInconsistenteLote()
    {
        return this._percentualMaximoInconsistenteLote;
    } //-- int getPercentualMaximoInconsistenteLote() 

    /**
     * Returns the value of field 'periodicidadeTarifaCatalogo'.
     * 
     * @return BigDecimal
     * @return the value of field 'periodicidadeTarifaCatalogo'.
     */
    public java.math.BigDecimal getPeriodicidadeTarifaCatalogo()
    {
        return this._periodicidadeTarifaCatalogo;
    } //-- java.math.BigDecimal getPeriodicidadeTarifaCatalogo() 

    /**
     * Returns the value of field 'quantidadeAntecedencia'.
     * 
     * @return int
     * @return the value of field 'quantidadeAntecedencia'.
     */
    public int getQuantidadeAntecedencia()
    {
        return this._quantidadeAntecedencia;
    } //-- int getQuantidadeAntecedencia() 

    /**
     * Returns the value of field
     * 'quantidadeAnteriorVencimentoComprovante'.
     * 
     * @return int
     * @return the value of field
     * 'quantidadeAnteriorVencimentoComprovante'.
     */
    public int getQuantidadeAnteriorVencimentoComprovante()
    {
        return this._quantidadeAnteriorVencimentoComprovante;
    } //-- int getQuantidadeAnteriorVencimentoComprovante() 

    /**
     * Returns the value of field 'quantidadeDiaCobrancaTarifa'.
     * 
     * @return int
     * @return the value of field 'quantidadeDiaCobrancaTarifa'.
     */
    public int getQuantidadeDiaCobrancaTarifa()
    {
        return this._quantidadeDiaCobrancaTarifa;
    } //-- int getQuantidadeDiaCobrancaTarifa() 

    /**
     * Returns the value of field 'quantidadeDiaExpiracao'.
     * 
     * @return int
     * @return the value of field 'quantidadeDiaExpiracao'.
     */
    public int getQuantidadeDiaExpiracao()
    {
        return this._quantidadeDiaExpiracao;
    } //-- int getQuantidadeDiaExpiracao() 

    /**
     * Returns the value of field 'quantidadeDiaFloatingPagamento'.
     * 
     * @return int
     * @return the value of field 'quantidadeDiaFloatingPagamento'.
     */
    public int getQuantidadeDiaFloatingPagamento()
    {
        return this._quantidadeDiaFloatingPagamento;
    } //-- int getQuantidadeDiaFloatingPagamento() 

    /**
     * Returns the value of field
     * 'quantidadeDiaInatividadeFavorecido'.
     * 
     * @return int
     * @return the value of field
     * 'quantidadeDiaInatividadeFavorecido'.
     */
    public int getQuantidadeDiaInatividadeFavorecido()
    {
        return this._quantidadeDiaInatividadeFavorecido;
    } //-- int getQuantidadeDiaInatividadeFavorecido() 

    /**
     * Returns the value of field 'quantidadeDiaRepiqueConsulta'.
     * 
     * @return int
     * @return the value of field 'quantidadeDiaRepiqueConsulta'.
     */
    public int getQuantidadeDiaRepiqueConsulta()
    {
        return this._quantidadeDiaRepiqueConsulta;
    } //-- int getQuantidadeDiaRepiqueConsulta() 

    /**
     * Returns the value of field
     * 'quantidadeEtapaRecadastramentoBeneficio'.
     * 
     * @return int
     * @return the value of field
     * 'quantidadeEtapaRecadastramentoBeneficio'.
     */
    public int getQuantidadeEtapaRecadastramentoBeneficio()
    {
        return this._quantidadeEtapaRecadastramentoBeneficio;
    } //-- int getQuantidadeEtapaRecadastramentoBeneficio() 

    /**
     * Returns the value of field
     * 'quantidadeFaseRecadastramentoBeneficio'.
     * 
     * @return int
     * @return the value of field
     * 'quantidadeFaseRecadastramentoBeneficio'.
     */
    public int getQuantidadeFaseRecadastramentoBeneficio()
    {
        return this._quantidadeFaseRecadastramentoBeneficio;
    } //-- int getQuantidadeFaseRecadastramentoBeneficio() 

    /**
     * Returns the value of field 'quantidadeLimiteLinha'.
     * 
     * @return int
     * @return the value of field 'quantidadeLimiteLinha'.
     */
    public int getQuantidadeLimiteLinha()
    {
        return this._quantidadeLimiteLinha;
    } //-- int getQuantidadeLimiteLinha() 

    /**
     * Returns the value of field
     * 'quantidadeMaximaInconsistenteLote'.
     * 
     * @return int
     * @return the value of field
     * 'quantidadeMaximaInconsistenteLote'.
     */
    public int getQuantidadeMaximaInconsistenteLote()
    {
        return this._quantidadeMaximaInconsistenteLote;
    } //-- int getQuantidadeMaximaInconsistenteLote() 

    /**
     * Returns the value of field 'quantidadeMaximaTituloVencido'.
     * 
     * @return int
     * @return the value of field 'quantidadeMaximaTituloVencido'.
     */
    public int getQuantidadeMaximaTituloVencido()
    {
        return this._quantidadeMaximaTituloVencido;
    } //-- int getQuantidadeMaximaTituloVencido() 

    /**
     * Returns the value of field 'quantidadeMesComprovante'.
     * 
     * @return int
     * @return the value of field 'quantidadeMesComprovante'.
     */
    public int getQuantidadeMesComprovante()
    {
        return this._quantidadeMesComprovante;
    } //-- int getQuantidadeMesComprovante() 

    /**
     * Returns the value of field
     * 'quantidadeMesEtapaRecadastramento'.
     * 
     * @return int
     * @return the value of field
     * 'quantidadeMesEtapaRecadastramento'.
     */
    public int getQuantidadeMesEtapaRecadastramento()
    {
        return this._quantidadeMesEtapaRecadastramento;
    } //-- int getQuantidadeMesEtapaRecadastramento() 

    /**
     * Returns the value of field
     * 'quantidadeMesFaseRecadastramento'.
     * 
     * @return int
     * @return the value of field 'quantidadeMesFaseRecadastramento'
     */
    public int getQuantidadeMesFaseRecadastramento()
    {
        return this._quantidadeMesFaseRecadastramento;
    } //-- int getQuantidadeMesFaseRecadastramento() 

    /**
     * Returns the value of field 'quantidadeMesReajusteTarifa'.
     * 
     * @return int
     * @return the value of field 'quantidadeMesReajusteTarifa'.
     */
    public int getQuantidadeMesReajusteTarifa()
    {
        return this._quantidadeMesReajusteTarifa;
    } //-- int getQuantidadeMesReajusteTarifa() 

    /**
     * Returns the value of field 'quantidadeSolicitacaoCartao'.
     * 
     * @return int
     * @return the value of field 'quantidadeSolicitacaoCartao'.
     */
    public int getQuantidadeSolicitacaoCartao()
    {
        return this._quantidadeSolicitacaoCartao;
    } //-- int getQuantidadeSolicitacaoCartao() 

    /**
     * Returns the value of field 'quantidadeViaAviso'.
     * 
     * @return int
     * @return the value of field 'quantidadeViaAviso'.
     */
    public int getQuantidadeViaAviso()
    {
        return this._quantidadeViaAviso;
    } //-- int getQuantidadeViaAviso() 

    /**
     * Returns the value of field 'quantidadeViaCombranca'.
     * 
     * @return int
     * @return the value of field 'quantidadeViaCombranca'.
     */
    public int getQuantidadeViaCombranca()
    {
        return this._quantidadeViaCombranca;
    } //-- int getQuantidadeViaCombranca() 

    /**
     * Returns the value of field 'quantidadeViaComprovante'.
     * 
     * @return int
     * @return the value of field 'quantidadeViaComprovante'.
     */
    public int getQuantidadeViaComprovante()
    {
        return this._quantidadeViaComprovante;
    } //-- int getQuantidadeViaComprovante() 

    /**
     * Returns the value of field 'valorFavorecidoNaoCadastrado'.
     * 
     * @return BigDecimal
     * @return the value of field 'valorFavorecidoNaoCadastrado'.
     */
    public java.math.BigDecimal getValorFavorecidoNaoCadastrado()
    {
        return this._valorFavorecidoNaoCadastrado;
    } //-- java.math.BigDecimal getValorFavorecidoNaoCadastrado() 

    /**
     * Returns the value of field 'valorLimiteDiarioPagamento'.
     * 
     * @return BigDecimal
     * @return the value of field 'valorLimiteDiarioPagamento'.
     */
    public java.math.BigDecimal getValorLimiteDiarioPagamento()
    {
        return this._valorLimiteDiarioPagamento;
    } //-- java.math.BigDecimal getValorLimiteDiarioPagamento() 

    /**
     * Returns the value of field 'valorLimiteIndividualPagamento'.
     * 
     * @return BigDecimal
     * @return the value of field 'valorLimiteIndividualPagamento'.
     */
    public java.math.BigDecimal getValorLimiteIndividualPagamento()
    {
        return this._valorLimiteIndividualPagamento;
    } //-- java.math.BigDecimal getValorLimiteIndividualPagamento() 

    /**
     * Method hasCdAcaoNaoVida
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAcaoNaoVida()
    {
        return this._has_cdAcaoNaoVida;
    } //-- boolean hasCdAcaoNaoVida() 

    /**
     * Method hasCdAcertoDadosRecadastramento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAcertoDadosRecadastramento()
    {
        return this._has_cdAcertoDadosRecadastramento;
    } //-- boolean hasCdAcertoDadosRecadastramento() 

    /**
     * Method hasCdAgendaRastreabilidadeFilial
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgendaRastreabilidadeFilial()
    {
        return this._has_cdAgendaRastreabilidadeFilial;
    } //-- boolean hasCdAgendaRastreabilidadeFilial() 

    /**
     * Method hasCdAgendamentoDebitoVeiculo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgendamentoDebitoVeiculo()
    {
        return this._has_cdAgendamentoDebitoVeiculo;
    } //-- boolean hasCdAgendamentoDebitoVeiculo() 

    /**
     * Method hasCdAgendamentoPagamentoVencido
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgendamentoPagamentoVencido()
    {
        return this._has_cdAgendamentoPagamentoVencido;
    } //-- boolean hasCdAgendamentoPagamentoVencido() 

    /**
     * Method hasCdAgendamentoValorMenor
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgendamentoValorMenor()
    {
        return this._has_cdAgendamentoValorMenor;
    } //-- boolean hasCdAgendamentoValorMenor() 

    /**
     * Method hasCdAgrupamentoAviso
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgrupamentoAviso()
    {
        return this._has_cdAgrupamentoAviso;
    } //-- boolean hasCdAgrupamentoAviso() 

    /**
     * Method hasCdAgrupamentoComprovante
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgrupamentoComprovante()
    {
        return this._has_cdAgrupamentoComprovante;
    } //-- boolean hasCdAgrupamentoComprovante() 

    /**
     * Method hasCdAgrupamentoFormularioRecadastro
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgrupamentoFormularioRecadastro()
    {
        return this._has_cdAgrupamentoFormularioRecadastro;
    } //-- boolean hasCdAgrupamentoFormularioRecadastro() 

    /**
     * Method hasCdAntecipacaoRecadastramentoBeneficiario
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAntecipacaoRecadastramentoBeneficiario()
    {
        return this._has_cdAntecipacaoRecadastramentoBeneficiario;
    } //-- boolean hasCdAntecipacaoRecadastramentoBeneficiario() 

    /**
     * Method hasCdAreaReservada
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAreaReservada()
    {
        return this._has_cdAreaReservada;
    } //-- boolean hasCdAreaReservada() 

    /**
     * Method hasCdBaseRecadastramentoBeneficio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdBaseRecadastramentoBeneficio()
    {
        return this._has_cdBaseRecadastramentoBeneficio;
    } //-- boolean hasCdBaseRecadastramentoBeneficio() 

    /**
     * Method hasCdBloqueioEmissaoPplta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdBloqueioEmissaoPplta()
    {
        return this._has_cdBloqueioEmissaoPplta;
    } //-- boolean hasCdBloqueioEmissaoPplta() 

    /**
     * Method hasCdCanalAlteracao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCanalAlteracao()
    {
        return this._has_cdCanalAlteracao;
    } //-- boolean hasCdCanalAlteracao() 

    /**
     * Method hasCdCanalInclusao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCanalInclusao()
    {
        return this._has_cdCanalInclusao;
    } //-- boolean hasCdCanalInclusao() 

    /**
     * Method hasCdCaptuTituloRegistro
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCaptuTituloRegistro()
    {
        return this._has_cdCaptuTituloRegistro;
    } //-- boolean hasCdCaptuTituloRegistro() 

    /**
     * Method hasCdCctciaEspeBeneficio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCctciaEspeBeneficio()
    {
        return this._has_cdCctciaEspeBeneficio;
    } //-- boolean hasCdCctciaEspeBeneficio() 

    /**
     * Method hasCdCctciaIdentificacaoBeneficio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCctciaIdentificacaoBeneficio()
    {
        return this._has_cdCctciaIdentificacaoBeneficio;
    } //-- boolean hasCdCctciaIdentificacaoBeneficio() 

    /**
     * Method hasCdCctciaInscricaoFavorecido
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCctciaInscricaoFavorecido()
    {
        return this._has_cdCctciaInscricaoFavorecido;
    } //-- boolean hasCdCctciaInscricaoFavorecido() 

    /**
     * Method hasCdCctciaProprietarioVeculo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCctciaProprietarioVeculo()
    {
        return this._has_cdCctciaProprietarioVeculo;
    } //-- boolean hasCdCctciaProprietarioVeculo() 

    /**
     * Method hasCdCobrancaTarifa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCobrancaTarifa()
    {
        return this._has_cdCobrancaTarifa;
    } //-- boolean hasCdCobrancaTarifa() 

    /**
     * Method hasCdConsultaDebitoVeiculo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdConsultaDebitoVeiculo()
    {
        return this._has_cdConsultaDebitoVeiculo;
    } //-- boolean hasCdConsultaDebitoVeiculo() 

    /**
     * Method hasCdConsultaEndereco
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdConsultaEndereco()
    {
        return this._has_cdConsultaEndereco;
    } //-- boolean hasCdConsultaEndereco() 

    /**
     * Method hasCdConsultaSaldoPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdConsultaSaldoPagamento()
    {
        return this._has_cdConsultaSaldoPagamento;
    } //-- boolean hasCdConsultaSaldoPagamento() 

    /**
     * Method hasCdConsultaSaldoValorSuperior
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdConsultaSaldoValorSuperior()
    {
        return this._has_cdConsultaSaldoValorSuperior;
    } //-- boolean hasCdConsultaSaldoValorSuperior() 

    /**
     * Method hasCdContagemConsultaSaldo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdContagemConsultaSaldo()
    {
        return this._has_cdContagemConsultaSaldo;
    } //-- boolean hasCdContagemConsultaSaldo() 

    /**
     * Method hasCdCreditoNaoUtilizado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCreditoNaoUtilizado()
    {
        return this._has_cdCreditoNaoUtilizado;
    } //-- boolean hasCdCreditoNaoUtilizado() 

    /**
     * Method hasCdCriterioEnquadraRecadastramento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCriterioEnquadraRecadastramento()
    {
        return this._has_cdCriterioEnquadraRecadastramento;
    } //-- boolean hasCdCriterioEnquadraRecadastramento() 

    /**
     * Method hasCdCriterioEnquandraBeneficio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCriterioEnquandraBeneficio()
    {
        return this._has_cdCriterioEnquandraBeneficio;
    } //-- boolean hasCdCriterioEnquandraBeneficio() 

    /**
     * Method hasCdCriterioRstrbTitulo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCriterioRstrbTitulo()
    {
        return this._has_cdCriterioRstrbTitulo;
    } //-- boolean hasCdCriterioRstrbTitulo() 

    /**
     * Method hasCdDestinoAviso
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdDestinoAviso()
    {
        return this._has_cdDestinoAviso;
    } //-- boolean hasCdDestinoAviso() 

    /**
     * Method hasCdDestinoComprovante
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdDestinoComprovante()
    {
        return this._has_cdDestinoComprovante;
    } //-- boolean hasCdDestinoComprovante() 

    /**
     * Method hasCdDestinoFormularioRecadastramento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdDestinoFormularioRecadastramento()
    {
        return this._has_cdDestinoFormularioRecadastramento;
    } //-- boolean hasCdDestinoFormularioRecadastramento() 

    /**
     * Method hasCdDisponibilizacaoContaCredito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdDisponibilizacaoContaCredito()
    {
        return this._has_cdDisponibilizacaoContaCredito;
    } //-- boolean hasCdDisponibilizacaoContaCredito() 

    /**
     * Method hasCdDisponibilizacaoDiversoCriterio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdDisponibilizacaoDiversoCriterio()
    {
        return this._has_cdDisponibilizacaoDiversoCriterio;
    } //-- boolean hasCdDisponibilizacaoDiversoCriterio() 

    /**
     * Method hasCdDisponibilizacaoDiversoNao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdDisponibilizacaoDiversoNao()
    {
        return this._has_cdDisponibilizacaoDiversoNao;
    } //-- boolean hasCdDisponibilizacaoDiversoNao() 

    /**
     * Method hasCdDisponibilizacaoSalarioCriterio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdDisponibilizacaoSalarioCriterio()
    {
        return this._has_cdDisponibilizacaoSalarioCriterio;
    } //-- boolean hasCdDisponibilizacaoSalarioCriterio() 

    /**
     * Method hasCdDisponibilizacaoSalarioNao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdDisponibilizacaoSalarioNao()
    {
        return this._has_cdDisponibilizacaoSalarioNao;
    } //-- boolean hasCdDisponibilizacaoSalarioNao() 

    /**
     * Method hasCdEnvelopeAberto
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdEnvelopeAberto()
    {
        return this._has_cdEnvelopeAberto;
    } //-- boolean hasCdEnvelopeAberto() 

    /**
     * Method hasCdFavorecidoConsultaPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFavorecidoConsultaPagamento()
    {
        return this._has_cdFavorecidoConsultaPagamento;
    } //-- boolean hasCdFavorecidoConsultaPagamento() 

    /**
     * Method hasCdFormaAutorizacaoPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFormaAutorizacaoPagamento()
    {
        return this._has_cdFormaAutorizacaoPagamento;
    } //-- boolean hasCdFormaAutorizacaoPagamento() 

    /**
     * Method hasCdFormaEnvioPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFormaEnvioPagamento()
    {
        return this._has_cdFormaEnvioPagamento;
    } //-- boolean hasCdFormaEnvioPagamento() 

    /**
     * Method hasCdFormaExpiracaoCredito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFormaExpiracaoCredito()
    {
        return this._has_cdFormaExpiracaoCredito;
    } //-- boolean hasCdFormaExpiracaoCredito() 

    /**
     * Method hasCdFormaManutencao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFormaManutencao()
    {
        return this._has_cdFormaManutencao;
    } //-- boolean hasCdFormaManutencao() 

    /**
     * Method hasCdFormularioContratoCliente
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFormularioContratoCliente()
    {
        return this._has_cdFormularioContratoCliente;
    } //-- boolean hasCdFormularioContratoCliente() 

    /**
     * Method hasCdFrasePrecadastrada
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFrasePrecadastrada()
    {
        return this._has_cdFrasePrecadastrada;
    } //-- boolean hasCdFrasePrecadastrada() 

    /**
     * Method hasCdIndLancamentoPersonalizado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndLancamentoPersonalizado()
    {
        return this._has_cdIndLancamentoPersonalizado;
    } //-- boolean hasCdIndLancamentoPersonalizado() 

    /**
     * Method hasCdIndicadorAdesaoSacador
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorAdesaoSacador()
    {
        return this._has_cdIndicadorAdesaoSacador;
    } //-- boolean hasCdIndicadorAdesaoSacador() 

    /**
     * Method hasCdIndicadorAgendaTitulo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorAgendaTitulo()
    {
        return this._has_cdIndicadorAgendaTitulo;
    } //-- boolean hasCdIndicadorAgendaTitulo() 

    /**
     * Method hasCdIndicadorAutorizacaoCliente
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorAutorizacaoCliente()
    {
        return this._has_cdIndicadorAutorizacaoCliente;
    } //-- boolean hasCdIndicadorAutorizacaoCliente() 

    /**
     * Method hasCdIndicadorAutorizacaoComplemento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorAutorizacaoComplemento()
    {
        return this._has_cdIndicadorAutorizacaoComplemento;
    } //-- boolean hasCdIndicadorAutorizacaoComplemento() 

    /**
     * Method hasCdIndicadorBancoPostal
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorBancoPostal()
    {
        return this._has_cdIndicadorBancoPostal;
    } //-- boolean hasCdIndicadorBancoPostal() 

    /**
     * Method hasCdIndicadorCadastroProcurador
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorCadastroProcurador()
    {
        return this._has_cdIndicadorCadastroProcurador;
    } //-- boolean hasCdIndicadorCadastroProcurador() 

    /**
     * Method hasCdIndicadorCadastroorganizacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorCadastroorganizacao()
    {
        return this._has_cdIndicadorCadastroorganizacao;
    } //-- boolean hasCdIndicadorCadastroorganizacao() 

    /**
     * Method hasCdIndicadorCartaoSalario
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorCartaoSalario()
    {
        return this._has_cdIndicadorCartaoSalario;
    } //-- boolean hasCdIndicadorCartaoSalario() 

    /**
     * Method hasCdIndicadorEconomicoReajuste
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorEconomicoReajuste()
    {
        return this._has_cdIndicadorEconomicoReajuste;
    } //-- boolean hasCdIndicadorEconomicoReajuste() 

    /**
     * Method hasCdIndicadorEmissaoAviso
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorEmissaoAviso()
    {
        return this._has_cdIndicadorEmissaoAviso;
    } //-- boolean hasCdIndicadorEmissaoAviso() 

    /**
     * Method hasCdIndicadorListaDebito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorListaDebito()
    {
        return this._has_cdIndicadorListaDebito;
    } //-- boolean hasCdIndicadorListaDebito() 

    /**
     * Method hasCdIndicadorMensagemPersonalizada
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorMensagemPersonalizada()
    {
        return this._has_cdIndicadorMensagemPersonalizada;
    } //-- boolean hasCdIndicadorMensagemPersonalizada() 

    /**
     * Method hasCdIndicadorRetornoInternet
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorRetornoInternet()
    {
        return this._has_cdIndicadorRetornoInternet;
    } //-- boolean hasCdIndicadorRetornoInternet() 

    /**
     * Method hasCdIndicadorRetornoSeparado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorRetornoSeparado()
    {
        return this._has_cdIndicadorRetornoSeparado;
    } //-- boolean hasCdIndicadorRetornoSeparado() 

    /**
     * Method hasCdLancamentoFuturoCredito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdLancamentoFuturoCredito()
    {
        return this._has_cdLancamentoFuturoCredito;
    } //-- boolean hasCdLancamentoFuturoCredito() 

    /**
     * Method hasCdLancamentoFuturoDebito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdLancamentoFuturoDebito()
    {
        return this._has_cdLancamentoFuturoDebito;
    } //-- boolean hasCdLancamentoFuturoDebito() 

    /**
     * Method hasCdLiberacaoLoteProcs
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdLiberacaoLoteProcs()
    {
        return this._has_cdLiberacaoLoteProcs;
    } //-- boolean hasCdLiberacaoLoteProcs() 

    /**
     * Method hasCdManutencaoBaseRecadastramento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdManutencaoBaseRecadastramento()
    {
        return this._has_cdManutencaoBaseRecadastramento;
    } //-- boolean hasCdManutencaoBaseRecadastramento() 

    /**
     * Method hasCdMeioPagamentoCredito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMeioPagamentoCredito()
    {
        return this._has_cdMeioPagamentoCredito;
    } //-- boolean hasCdMeioPagamentoCredito() 

    /**
     * Method hasCdMensagemRecadastramentoMidia
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMensagemRecadastramentoMidia()
    {
        return this._has_cdMensagemRecadastramentoMidia;
    } //-- boolean hasCdMensagemRecadastramentoMidia() 

    /**
     * Method hasCdMidiaDisponivel
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMidiaDisponivel()
    {
        return this._has_cdMidiaDisponivel;
    } //-- boolean hasCdMidiaDisponivel() 

    /**
     * Method hasCdMidiaMensagemRecadastramento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMidiaMensagemRecadastramento()
    {
        return this._has_cdMidiaMensagemRecadastramento;
    } //-- boolean hasCdMidiaMensagemRecadastramento() 

    /**
     * Method hasCdMomentoAvisoRecadastramento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMomentoAvisoRecadastramento()
    {
        return this._has_cdMomentoAvisoRecadastramento;
    } //-- boolean hasCdMomentoAvisoRecadastramento() 

    /**
     * Method hasCdMomentoCreditoEfetivacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMomentoCreditoEfetivacao()
    {
        return this._has_cdMomentoCreditoEfetivacao;
    } //-- boolean hasCdMomentoCreditoEfetivacao() 

    /**
     * Method hasCdMomentoDebitoPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMomentoDebitoPagamento()
    {
        return this._has_cdMomentoDebitoPagamento;
    } //-- boolean hasCdMomentoDebitoPagamento() 

    /**
     * Method hasCdMomentoFormularioRecadastramento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMomentoFormularioRecadastramento()
    {
        return this._has_cdMomentoFormularioRecadastramento;
    } //-- boolean hasCdMomentoFormularioRecadastramento() 

    /**
     * Method hasCdMomentoProcessamentoPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMomentoProcessamentoPagamento()
    {
        return this._has_cdMomentoProcessamentoPagamento;
    } //-- boolean hasCdMomentoProcessamentoPagamento() 

    /**
     * Method hasCdNaturezaOperacaoPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdNaturezaOperacaoPagamento()
    {
        return this._has_cdNaturezaOperacaoPagamento;
    } //-- boolean hasCdNaturezaOperacaoPagamento() 

    /**
     * Method hasCdPagamentoNaoUtil
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPagamentoNaoUtil()
    {
        return this._has_cdPagamentoNaoUtil;
    } //-- boolean hasCdPagamentoNaoUtil() 

    /**
     * Method hasCdPeriodicidadeAviso
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPeriodicidadeAviso()
    {
        return this._has_cdPeriodicidadeAviso;
    } //-- boolean hasCdPeriodicidadeAviso() 

    /**
     * Method hasCdPeriodicidadeCobrancaTarifa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPeriodicidadeCobrancaTarifa()
    {
        return this._has_cdPeriodicidadeCobrancaTarifa;
    } //-- boolean hasCdPeriodicidadeCobrancaTarifa() 

    /**
     * Method hasCdPeriodicidadeComprovante
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPeriodicidadeComprovante()
    {
        return this._has_cdPeriodicidadeComprovante;
    } //-- boolean hasCdPeriodicidadeComprovante() 

    /**
     * Method hasCdPeriodicidadeConsultaVeiculo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPeriodicidadeConsultaVeiculo()
    {
        return this._has_cdPeriodicidadeConsultaVeiculo;
    } //-- boolean hasCdPeriodicidadeConsultaVeiculo() 

    /**
     * Method hasCdPeriodicidadeEnvioRemessa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPeriodicidadeEnvioRemessa()
    {
        return this._has_cdPeriodicidadeEnvioRemessa;
    } //-- boolean hasCdPeriodicidadeEnvioRemessa() 

    /**
     * Method hasCdPeriodicidadeManutencaoProcd
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPeriodicidadeManutencaoProcd()
    {
        return this._has_cdPeriodicidadeManutencaoProcd;
    } //-- boolean hasCdPeriodicidadeManutencaoProcd() 

    /**
     * Method hasCdPermissaoDebitoOnline
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPermissaoDebitoOnline()
    {
        return this._has_cdPermissaoDebitoOnline;
    } //-- boolean hasCdPermissaoDebitoOnline() 

    /**
     * Method hasCdPrincipalEnquaRecadastramento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPrincipalEnquaRecadastramento()
    {
        return this._has_cdPrincipalEnquaRecadastramento;
    } //-- boolean hasCdPrincipalEnquaRecadastramento() 

    /**
     * Method hasCdPrioridadeEfetivacaoPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPrioridadeEfetivacaoPagamento()
    {
        return this._has_cdPrioridadeEfetivacaoPagamento;
    } //-- boolean hasCdPrioridadeEfetivacaoPagamento() 

    /**
     * Method hasCdRastreabNotaFiscal
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdRastreabNotaFiscal()
    {
        return this._has_cdRastreabNotaFiscal;
    } //-- boolean hasCdRastreabNotaFiscal() 

    /**
     * Method hasCdRastreabTituloTerc
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdRastreabTituloTerc()
    {
        return this._has_cdRastreabTituloTerc;
    } //-- boolean hasCdRastreabTituloTerc() 

    /**
     * Method hasCdRejeicaoAgendamentoLote
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdRejeicaoAgendamentoLote()
    {
        return this._has_cdRejeicaoAgendamentoLote;
    } //-- boolean hasCdRejeicaoAgendamentoLote() 

    /**
     * Method hasCdRejeicaoEfetivacaoLote
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdRejeicaoEfetivacaoLote()
    {
        return this._has_cdRejeicaoEfetivacaoLote;
    } //-- boolean hasCdRejeicaoEfetivacaoLote() 

    /**
     * Method hasCdRejeicaoLote
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdRejeicaoLote()
    {
        return this._has_cdRejeicaoLote;
    } //-- boolean hasCdRejeicaoLote() 

    /**
     * Method hasCdTipoCargaRecadastramento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoCargaRecadastramento()
    {
        return this._has_cdTipoCargaRecadastramento;
    } //-- boolean hasCdTipoCargaRecadastramento() 

    /**
     * Method hasCdTipoCartaoSalario
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoCartaoSalario()
    {
        return this._has_cdTipoCartaoSalario;
    } //-- boolean hasCdTipoCartaoSalario() 

    /**
     * Method hasCdTipoConsistenciaFavorecido
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoConsistenciaFavorecido()
    {
        return this._has_cdTipoConsistenciaFavorecido;
    } //-- boolean hasCdTipoConsistenciaFavorecido() 

    /**
     * Method hasCdTipoConsistenciaLista
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoConsistenciaLista()
    {
        return this._has_cdTipoConsistenciaLista;
    } //-- boolean hasCdTipoConsistenciaLista() 

    /**
     * Method hasCdTipoConsultaComprovante
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoConsultaComprovante()
    {
        return this._has_cdTipoConsultaComprovante;
    } //-- boolean hasCdTipoConsultaComprovante() 

    /**
     * Method hasCdTipoDataFloating
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoDataFloating()
    {
        return this._has_cdTipoDataFloating;
    } //-- boolean hasCdTipoDataFloating() 

    /**
     * Method hasCdTipoDivergenciaVeiculo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoDivergenciaVeiculo()
    {
        return this._has_cdTipoDivergenciaVeiculo;
    } //-- boolean hasCdTipoDivergenciaVeiculo() 

    /**
     * Method hasCdTipoFormacaoLista
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoFormacaoLista()
    {
        return this._has_cdTipoFormacaoLista;
    } //-- boolean hasCdTipoFormacaoLista() 

    /**
     * Method hasCdTipoIdentificacaoBeneficio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoIdentificacaoBeneficio()
    {
        return this._has_cdTipoIdentificacaoBeneficio;
    } //-- boolean hasCdTipoIdentificacaoBeneficio() 

    /**
     * Method hasCdTipoInscricaoFavorecidoAceita
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoInscricaoFavorecidoAceita()
    {
        return this._has_cdTipoInscricaoFavorecidoAceita;
    } //-- boolean hasCdTipoInscricaoFavorecidoAceita() 

    /**
     * Method hasCdTipoLayoutArquivo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoLayoutArquivo()
    {
        return this._has_cdTipoLayoutArquivo;
    } //-- boolean hasCdTipoLayoutArquivo() 

    /**
     * Method hasCdTipoReajusteTarifa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoReajusteTarifa()
    {
        return this._has_cdTipoReajusteTarifa;
    } //-- boolean hasCdTipoReajusteTarifa() 

    /**
     * Method hasCdTratoContaTransf
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTratoContaTransf()
    {
        return this._has_cdTratoContaTransf;
    } //-- boolean hasCdTratoContaTransf() 

    /**
     * Method hasCdUtilizacaoFavorecidoControle
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdUtilizacaoFavorecidoControle()
    {
        return this._has_cdUtilizacaoFavorecidoControle;
    } //-- boolean hasCdUtilizacaoFavorecidoControle() 

    /**
     * Method hasCdValidacaoNomeFavorecido
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdValidacaoNomeFavorecido()
    {
        return this._has_cdValidacaoNomeFavorecido;
    } //-- boolean hasCdValidacaoNomeFavorecido() 

    /**
     * Method hasCdindicadorExpiraCredito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdindicadorExpiraCredito()
    {
        return this._has_cdindicadorExpiraCredito;
    } //-- boolean hasCdindicadorExpiraCredito() 

    /**
     * Method hasCdindicadorLancamentoProgramado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdindicadorLancamentoProgramado()
    {
        return this._has_cdindicadorLancamentoProgramado;
    } //-- boolean hasCdindicadorLancamentoProgramado() 

    /**
     * Method hasNrFechamentoApuracaoTarifa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrFechamentoApuracaoTarifa()
    {
        return this._has_nrFechamentoApuracaoTarifa;
    } //-- boolean hasNrFechamentoApuracaoTarifa() 

    /**
     * Method hasPercentualMaximoInconsistenteLote
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasPercentualMaximoInconsistenteLote()
    {
        return this._has_percentualMaximoInconsistenteLote;
    } //-- boolean hasPercentualMaximoInconsistenteLote() 

    /**
     * Method hasQuantidadeAntecedencia
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQuantidadeAntecedencia()
    {
        return this._has_quantidadeAntecedencia;
    } //-- boolean hasQuantidadeAntecedencia() 

    /**
     * Method hasQuantidadeAnteriorVencimentoComprovante
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQuantidadeAnteriorVencimentoComprovante()
    {
        return this._has_quantidadeAnteriorVencimentoComprovante;
    } //-- boolean hasQuantidadeAnteriorVencimentoComprovante() 

    /**
     * Method hasQuantidadeDiaCobrancaTarifa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQuantidadeDiaCobrancaTarifa()
    {
        return this._has_quantidadeDiaCobrancaTarifa;
    } //-- boolean hasQuantidadeDiaCobrancaTarifa() 

    /**
     * Method hasQuantidadeDiaExpiracao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQuantidadeDiaExpiracao()
    {
        return this._has_quantidadeDiaExpiracao;
    } //-- boolean hasQuantidadeDiaExpiracao() 

    /**
     * Method hasQuantidadeDiaFloatingPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQuantidadeDiaFloatingPagamento()
    {
        return this._has_quantidadeDiaFloatingPagamento;
    } //-- boolean hasQuantidadeDiaFloatingPagamento() 

    /**
     * Method hasQuantidadeDiaInatividadeFavorecido
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQuantidadeDiaInatividadeFavorecido()
    {
        return this._has_quantidadeDiaInatividadeFavorecido;
    } //-- boolean hasQuantidadeDiaInatividadeFavorecido() 

    /**
     * Method hasQuantidadeDiaRepiqueConsulta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQuantidadeDiaRepiqueConsulta()
    {
        return this._has_quantidadeDiaRepiqueConsulta;
    } //-- boolean hasQuantidadeDiaRepiqueConsulta() 

    /**
     * Method hasQuantidadeEtapaRecadastramentoBeneficio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQuantidadeEtapaRecadastramentoBeneficio()
    {
        return this._has_quantidadeEtapaRecadastramentoBeneficio;
    } //-- boolean hasQuantidadeEtapaRecadastramentoBeneficio() 

    /**
     * Method hasQuantidadeFaseRecadastramentoBeneficio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQuantidadeFaseRecadastramentoBeneficio()
    {
        return this._has_quantidadeFaseRecadastramentoBeneficio;
    } //-- boolean hasQuantidadeFaseRecadastramentoBeneficio() 

    /**
     * Method hasQuantidadeLimiteLinha
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQuantidadeLimiteLinha()
    {
        return this._has_quantidadeLimiteLinha;
    } //-- boolean hasQuantidadeLimiteLinha() 

    /**
     * Method hasQuantidadeMaximaInconsistenteLote
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQuantidadeMaximaInconsistenteLote()
    {
        return this._has_quantidadeMaximaInconsistenteLote;
    } //-- boolean hasQuantidadeMaximaInconsistenteLote() 

    /**
     * Method hasQuantidadeMaximaTituloVencido
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQuantidadeMaximaTituloVencido()
    {
        return this._has_quantidadeMaximaTituloVencido;
    } //-- boolean hasQuantidadeMaximaTituloVencido() 

    /**
     * Method hasQuantidadeMesComprovante
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQuantidadeMesComprovante()
    {
        return this._has_quantidadeMesComprovante;
    } //-- boolean hasQuantidadeMesComprovante() 

    /**
     * Method hasQuantidadeMesEtapaRecadastramento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQuantidadeMesEtapaRecadastramento()
    {
        return this._has_quantidadeMesEtapaRecadastramento;
    } //-- boolean hasQuantidadeMesEtapaRecadastramento() 

    /**
     * Method hasQuantidadeMesFaseRecadastramento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQuantidadeMesFaseRecadastramento()
    {
        return this._has_quantidadeMesFaseRecadastramento;
    } //-- boolean hasQuantidadeMesFaseRecadastramento() 

    /**
     * Method hasQuantidadeMesReajusteTarifa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQuantidadeMesReajusteTarifa()
    {
        return this._has_quantidadeMesReajusteTarifa;
    } //-- boolean hasQuantidadeMesReajusteTarifa() 

    /**
     * Method hasQuantidadeSolicitacaoCartao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQuantidadeSolicitacaoCartao()
    {
        return this._has_quantidadeSolicitacaoCartao;
    } //-- boolean hasQuantidadeSolicitacaoCartao() 

    /**
     * Method hasQuantidadeViaAviso
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQuantidadeViaAviso()
    {
        return this._has_quantidadeViaAviso;
    } //-- boolean hasQuantidadeViaAviso() 

    /**
     * Method hasQuantidadeViaCombranca
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQuantidadeViaCombranca()
    {
        return this._has_quantidadeViaCombranca;
    } //-- boolean hasQuantidadeViaCombranca() 

    /**
     * Method hasQuantidadeViaComprovante
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQuantidadeViaComprovante()
    {
        return this._has_quantidadeViaComprovante;
    } //-- boolean hasQuantidadeViaComprovante() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdAcaoNaoVida'.
     * 
     * @param cdAcaoNaoVida the value of field 'cdAcaoNaoVida'.
     */
    public void setCdAcaoNaoVida(int cdAcaoNaoVida)
    {
        this._cdAcaoNaoVida = cdAcaoNaoVida;
        this._has_cdAcaoNaoVida = true;
    } //-- void setCdAcaoNaoVida(int) 

    /**
     * Sets the value of field 'cdAcertoDadosRecadastramento'.
     * 
     * @param cdAcertoDadosRecadastramento the value of field
     * 'cdAcertoDadosRecadastramento'.
     */
    public void setCdAcertoDadosRecadastramento(int cdAcertoDadosRecadastramento)
    {
        this._cdAcertoDadosRecadastramento = cdAcertoDadosRecadastramento;
        this._has_cdAcertoDadosRecadastramento = true;
    } //-- void setCdAcertoDadosRecadastramento(int) 

    /**
     * Sets the value of field 'cdAgendaRastreabilidadeFilial'.
     * 
     * @param cdAgendaRastreabilidadeFilial the value of field
     * 'cdAgendaRastreabilidadeFilial'.
     */
    public void setCdAgendaRastreabilidadeFilial(int cdAgendaRastreabilidadeFilial)
    {
        this._cdAgendaRastreabilidadeFilial = cdAgendaRastreabilidadeFilial;
        this._has_cdAgendaRastreabilidadeFilial = true;
    } //-- void setCdAgendaRastreabilidadeFilial(int) 

    /**
     * Sets the value of field 'cdAgendamentoDebitoVeiculo'.
     * 
     * @param cdAgendamentoDebitoVeiculo the value of field
     * 'cdAgendamentoDebitoVeiculo'.
     */
    public void setCdAgendamentoDebitoVeiculo(int cdAgendamentoDebitoVeiculo)
    {
        this._cdAgendamentoDebitoVeiculo = cdAgendamentoDebitoVeiculo;
        this._has_cdAgendamentoDebitoVeiculo = true;
    } //-- void setCdAgendamentoDebitoVeiculo(int) 

    /**
     * Sets the value of field 'cdAgendamentoPagamentoVencido'.
     * 
     * @param cdAgendamentoPagamentoVencido the value of field
     * 'cdAgendamentoPagamentoVencido'.
     */
    public void setCdAgendamentoPagamentoVencido(int cdAgendamentoPagamentoVencido)
    {
        this._cdAgendamentoPagamentoVencido = cdAgendamentoPagamentoVencido;
        this._has_cdAgendamentoPagamentoVencido = true;
    } //-- void setCdAgendamentoPagamentoVencido(int) 

    /**
     * Sets the value of field 'cdAgendamentoValorMenor'.
     * 
     * @param cdAgendamentoValorMenor the value of field
     * 'cdAgendamentoValorMenor'.
     */
    public void setCdAgendamentoValorMenor(int cdAgendamentoValorMenor)
    {
        this._cdAgendamentoValorMenor = cdAgendamentoValorMenor;
        this._has_cdAgendamentoValorMenor = true;
    } //-- void setCdAgendamentoValorMenor(int) 

    /**
     * Sets the value of field 'cdAgrupamentoAviso'.
     * 
     * @param cdAgrupamentoAviso the value of field
     * 'cdAgrupamentoAviso'.
     */
    public void setCdAgrupamentoAviso(int cdAgrupamentoAviso)
    {
        this._cdAgrupamentoAviso = cdAgrupamentoAviso;
        this._has_cdAgrupamentoAviso = true;
    } //-- void setCdAgrupamentoAviso(int) 

    /**
     * Sets the value of field 'cdAgrupamentoComprovante'.
     * 
     * @param cdAgrupamentoComprovante the value of field
     * 'cdAgrupamentoComprovante'.
     */
    public void setCdAgrupamentoComprovante(int cdAgrupamentoComprovante)
    {
        this._cdAgrupamentoComprovante = cdAgrupamentoComprovante;
        this._has_cdAgrupamentoComprovante = true;
    } //-- void setCdAgrupamentoComprovante(int) 

    /**
     * Sets the value of field 'cdAgrupamentoFormularioRecadastro'.
     * 
     * @param cdAgrupamentoFormularioRecadastro the value of field
     * 'cdAgrupamentoFormularioRecadastro'.
     */
    public void setCdAgrupamentoFormularioRecadastro(int cdAgrupamentoFormularioRecadastro)
    {
        this._cdAgrupamentoFormularioRecadastro = cdAgrupamentoFormularioRecadastro;
        this._has_cdAgrupamentoFormularioRecadastro = true;
    } //-- void setCdAgrupamentoFormularioRecadastro(int) 

    /**
     * Sets the value of field 'cdAmbienteServicoContrato'.
     * 
     * @param cdAmbienteServicoContrato the value of field
     * 'cdAmbienteServicoContrato'.
     */
    public void setCdAmbienteServicoContrato(java.lang.String cdAmbienteServicoContrato)
    {
        this._cdAmbienteServicoContrato = cdAmbienteServicoContrato;
    } //-- void setCdAmbienteServicoContrato(java.lang.String) 

    /**
     * Sets the value of field
     * 'cdAntecipacaoRecadastramentoBeneficiario'.
     * 
     * @param cdAntecipacaoRecadastramentoBeneficiario the value of
     * field 'cdAntecipacaoRecadastramentoBeneficiario'.
     */
    public void setCdAntecipacaoRecadastramentoBeneficiario(int cdAntecipacaoRecadastramentoBeneficiario)
    {
        this._cdAntecipacaoRecadastramentoBeneficiario = cdAntecipacaoRecadastramentoBeneficiario;
        this._has_cdAntecipacaoRecadastramentoBeneficiario = true;
    } //-- void setCdAntecipacaoRecadastramentoBeneficiario(int) 

    /**
     * Sets the value of field 'cdAreaReservada'.
     * 
     * @param cdAreaReservada the value of field 'cdAreaReservada'.
     */
    public void setCdAreaReservada(int cdAreaReservada)
    {
        this._cdAreaReservada = cdAreaReservada;
        this._has_cdAreaReservada = true;
    } //-- void setCdAreaReservada(int) 

    /**
     * Sets the value of field 'cdBaseRecadastramentoBeneficio'.
     * 
     * @param cdBaseRecadastramentoBeneficio the value of field
     * 'cdBaseRecadastramentoBeneficio'.
     */
    public void setCdBaseRecadastramentoBeneficio(int cdBaseRecadastramentoBeneficio)
    {
        this._cdBaseRecadastramentoBeneficio = cdBaseRecadastramentoBeneficio;
        this._has_cdBaseRecadastramentoBeneficio = true;
    } //-- void setCdBaseRecadastramentoBeneficio(int) 

    /**
     * Sets the value of field 'cdBloqueioEmissaoPplta'.
     * 
     * @param cdBloqueioEmissaoPplta the value of field
     * 'cdBloqueioEmissaoPplta'.
     */
    public void setCdBloqueioEmissaoPplta(int cdBloqueioEmissaoPplta)
    {
        this._cdBloqueioEmissaoPplta = cdBloqueioEmissaoPplta;
        this._has_cdBloqueioEmissaoPplta = true;
    } //-- void setCdBloqueioEmissaoPplta(int) 

    /**
     * Sets the value of field 'cdCanalAlteracao'.
     * 
     * @param cdCanalAlteracao the value of field 'cdCanalAlteracao'
     */
    public void setCdCanalAlteracao(int cdCanalAlteracao)
    {
        this._cdCanalAlteracao = cdCanalAlteracao;
        this._has_cdCanalAlteracao = true;
    } //-- void setCdCanalAlteracao(int) 

    /**
     * Sets the value of field 'cdCanalInclusao'.
     * 
     * @param cdCanalInclusao the value of field 'cdCanalInclusao'.
     */
    public void setCdCanalInclusao(int cdCanalInclusao)
    {
        this._cdCanalInclusao = cdCanalInclusao;
        this._has_cdCanalInclusao = true;
    } //-- void setCdCanalInclusao(int) 

    /**
     * Sets the value of field 'cdCaptuTituloRegistro'.
     * 
     * @param cdCaptuTituloRegistro the value of field
     * 'cdCaptuTituloRegistro'.
     */
    public void setCdCaptuTituloRegistro(int cdCaptuTituloRegistro)
    {
        this._cdCaptuTituloRegistro = cdCaptuTituloRegistro;
        this._has_cdCaptuTituloRegistro = true;
    } //-- void setCdCaptuTituloRegistro(int) 

    /**
     * Sets the value of field 'cdCctciaEspeBeneficio'.
     * 
     * @param cdCctciaEspeBeneficio the value of field
     * 'cdCctciaEspeBeneficio'.
     */
    public void setCdCctciaEspeBeneficio(int cdCctciaEspeBeneficio)
    {
        this._cdCctciaEspeBeneficio = cdCctciaEspeBeneficio;
        this._has_cdCctciaEspeBeneficio = true;
    } //-- void setCdCctciaEspeBeneficio(int) 

    /**
     * Sets the value of field 'cdCctciaIdentificacaoBeneficio'.
     * 
     * @param cdCctciaIdentificacaoBeneficio the value of field
     * 'cdCctciaIdentificacaoBeneficio'.
     */
    public void setCdCctciaIdentificacaoBeneficio(int cdCctciaIdentificacaoBeneficio)
    {
        this._cdCctciaIdentificacaoBeneficio = cdCctciaIdentificacaoBeneficio;
        this._has_cdCctciaIdentificacaoBeneficio = true;
    } //-- void setCdCctciaIdentificacaoBeneficio(int) 

    /**
     * Sets the value of field 'cdCctciaInscricaoFavorecido'.
     * 
     * @param cdCctciaInscricaoFavorecido the value of field
     * 'cdCctciaInscricaoFavorecido'.
     */
    public void setCdCctciaInscricaoFavorecido(int cdCctciaInscricaoFavorecido)
    {
        this._cdCctciaInscricaoFavorecido = cdCctciaInscricaoFavorecido;
        this._has_cdCctciaInscricaoFavorecido = true;
    } //-- void setCdCctciaInscricaoFavorecido(int) 

    /**
     * Sets the value of field 'cdCctciaProprietarioVeculo'.
     * 
     * @param cdCctciaProprietarioVeculo the value of field
     * 'cdCctciaProprietarioVeculo'.
     */
    public void setCdCctciaProprietarioVeculo(int cdCctciaProprietarioVeculo)
    {
        this._cdCctciaProprietarioVeculo = cdCctciaProprietarioVeculo;
        this._has_cdCctciaProprietarioVeculo = true;
    } //-- void setCdCctciaProprietarioVeculo(int) 

    /**
     * Sets the value of field 'cdCobrancaTarifa'.
     * 
     * @param cdCobrancaTarifa the value of field 'cdCobrancaTarifa'
     */
    public void setCdCobrancaTarifa(int cdCobrancaTarifa)
    {
        this._cdCobrancaTarifa = cdCobrancaTarifa;
        this._has_cdCobrancaTarifa = true;
    } //-- void setCdCobrancaTarifa(int) 

    /**
     * Sets the value of field 'cdConsultaDebitoVeiculo'.
     * 
     * @param cdConsultaDebitoVeiculo the value of field
     * 'cdConsultaDebitoVeiculo'.
     */
    public void setCdConsultaDebitoVeiculo(int cdConsultaDebitoVeiculo)
    {
        this._cdConsultaDebitoVeiculo = cdConsultaDebitoVeiculo;
        this._has_cdConsultaDebitoVeiculo = true;
    } //-- void setCdConsultaDebitoVeiculo(int) 

    /**
     * Sets the value of field 'cdConsultaEndereco'.
     * 
     * @param cdConsultaEndereco the value of field
     * 'cdConsultaEndereco'.
     */
    public void setCdConsultaEndereco(int cdConsultaEndereco)
    {
        this._cdConsultaEndereco = cdConsultaEndereco;
        this._has_cdConsultaEndereco = true;
    } //-- void setCdConsultaEndereco(int) 

    /**
     * Sets the value of field 'cdConsultaSaldoPagamento'.
     * 
     * @param cdConsultaSaldoPagamento the value of field
     * 'cdConsultaSaldoPagamento'.
     */
    public void setCdConsultaSaldoPagamento(int cdConsultaSaldoPagamento)
    {
        this._cdConsultaSaldoPagamento = cdConsultaSaldoPagamento;
        this._has_cdConsultaSaldoPagamento = true;
    } //-- void setCdConsultaSaldoPagamento(int) 

    /**
     * Sets the value of field 'cdConsultaSaldoValorSuperior'.
     * 
     * @param cdConsultaSaldoValorSuperior the value of field
     * 'cdConsultaSaldoValorSuperior'.
     */
    public void setCdConsultaSaldoValorSuperior(int cdConsultaSaldoValorSuperior)
    {
        this._cdConsultaSaldoValorSuperior = cdConsultaSaldoValorSuperior;
        this._has_cdConsultaSaldoValorSuperior = true;
    } //-- void setCdConsultaSaldoValorSuperior(int) 

    /**
     * Sets the value of field 'cdContagemConsultaSaldo'.
     * 
     * @param cdContagemConsultaSaldo the value of field
     * 'cdContagemConsultaSaldo'.
     */
    public void setCdContagemConsultaSaldo(int cdContagemConsultaSaldo)
    {
        this._cdContagemConsultaSaldo = cdContagemConsultaSaldo;
        this._has_cdContagemConsultaSaldo = true;
    } //-- void setCdContagemConsultaSaldo(int) 

    /**
     * Sets the value of field 'cdCreditoNaoUtilizado'.
     * 
     * @param cdCreditoNaoUtilizado the value of field
     * 'cdCreditoNaoUtilizado'.
     */
    public void setCdCreditoNaoUtilizado(int cdCreditoNaoUtilizado)
    {
        this._cdCreditoNaoUtilizado = cdCreditoNaoUtilizado;
        this._has_cdCreditoNaoUtilizado = true;
    } //-- void setCdCreditoNaoUtilizado(int) 

    /**
     * Sets the value of field 'cdCriterioEnquadraRecadastramento'.
     * 
     * @param cdCriterioEnquadraRecadastramento the value of field
     * 'cdCriterioEnquadraRecadastramento'.
     */
    public void setCdCriterioEnquadraRecadastramento(int cdCriterioEnquadraRecadastramento)
    {
        this._cdCriterioEnquadraRecadastramento = cdCriterioEnquadraRecadastramento;
        this._has_cdCriterioEnquadraRecadastramento = true;
    } //-- void setCdCriterioEnquadraRecadastramento(int) 

    /**
     * Sets the value of field 'cdCriterioEnquandraBeneficio'.
     * 
     * @param cdCriterioEnquandraBeneficio the value of field
     * 'cdCriterioEnquandraBeneficio'.
     */
    public void setCdCriterioEnquandraBeneficio(int cdCriterioEnquandraBeneficio)
    {
        this._cdCriterioEnquandraBeneficio = cdCriterioEnquandraBeneficio;
        this._has_cdCriterioEnquandraBeneficio = true;
    } //-- void setCdCriterioEnquandraBeneficio(int) 

    /**
     * Sets the value of field 'cdCriterioRstrbTitulo'.
     * 
     * @param cdCriterioRstrbTitulo the value of field
     * 'cdCriterioRstrbTitulo'.
     */
    public void setCdCriterioRstrbTitulo(int cdCriterioRstrbTitulo)
    {
        this._cdCriterioRstrbTitulo = cdCriterioRstrbTitulo;
        this._has_cdCriterioRstrbTitulo = true;
    } //-- void setCdCriterioRstrbTitulo(int) 

    /**
     * Sets the value of field 'cdDestinoAviso'.
     * 
     * @param cdDestinoAviso the value of field 'cdDestinoAviso'.
     */
    public void setCdDestinoAviso(int cdDestinoAviso)
    {
        this._cdDestinoAviso = cdDestinoAviso;
        this._has_cdDestinoAviso = true;
    } //-- void setCdDestinoAviso(int) 

    /**
     * Sets the value of field 'cdDestinoComprovante'.
     * 
     * @param cdDestinoComprovante the value of field
     * 'cdDestinoComprovante'.
     */
    public void setCdDestinoComprovante(int cdDestinoComprovante)
    {
        this._cdDestinoComprovante = cdDestinoComprovante;
        this._has_cdDestinoComprovante = true;
    } //-- void setCdDestinoComprovante(int) 

    /**
     * Sets the value of field
     * 'cdDestinoFormularioRecadastramento'.
     * 
     * @param cdDestinoFormularioRecadastramento the value of field
     * 'cdDestinoFormularioRecadastramento'.
     */
    public void setCdDestinoFormularioRecadastramento(int cdDestinoFormularioRecadastramento)
    {
        this._cdDestinoFormularioRecadastramento = cdDestinoFormularioRecadastramento;
        this._has_cdDestinoFormularioRecadastramento = true;
    } //-- void setCdDestinoFormularioRecadastramento(int) 

    /**
     * Sets the value of field 'cdDisponibilizacaoContaCredito'.
     * 
     * @param cdDisponibilizacaoContaCredito the value of field
     * 'cdDisponibilizacaoContaCredito'.
     */
    public void setCdDisponibilizacaoContaCredito(int cdDisponibilizacaoContaCredito)
    {
        this._cdDisponibilizacaoContaCredito = cdDisponibilizacaoContaCredito;
        this._has_cdDisponibilizacaoContaCredito = true;
    } //-- void setCdDisponibilizacaoContaCredito(int) 

    /**
     * Sets the value of field 'cdDisponibilizacaoDiversoCriterio'.
     * 
     * @param cdDisponibilizacaoDiversoCriterio the value of field
     * 'cdDisponibilizacaoDiversoCriterio'.
     */
    public void setCdDisponibilizacaoDiversoCriterio(int cdDisponibilizacaoDiversoCriterio)
    {
        this._cdDisponibilizacaoDiversoCriterio = cdDisponibilizacaoDiversoCriterio;
        this._has_cdDisponibilizacaoDiversoCriterio = true;
    } //-- void setCdDisponibilizacaoDiversoCriterio(int) 

    /**
     * Sets the value of field 'cdDisponibilizacaoDiversoNao'.
     * 
     * @param cdDisponibilizacaoDiversoNao the value of field
     * 'cdDisponibilizacaoDiversoNao'.
     */
    public void setCdDisponibilizacaoDiversoNao(int cdDisponibilizacaoDiversoNao)
    {
        this._cdDisponibilizacaoDiversoNao = cdDisponibilizacaoDiversoNao;
        this._has_cdDisponibilizacaoDiversoNao = true;
    } //-- void setCdDisponibilizacaoDiversoNao(int) 

    /**
     * Sets the value of field 'cdDisponibilizacaoSalarioCriterio'.
     * 
     * @param cdDisponibilizacaoSalarioCriterio the value of field
     * 'cdDisponibilizacaoSalarioCriterio'.
     */
    public void setCdDisponibilizacaoSalarioCriterio(int cdDisponibilizacaoSalarioCriterio)
    {
        this._cdDisponibilizacaoSalarioCriterio = cdDisponibilizacaoSalarioCriterio;
        this._has_cdDisponibilizacaoSalarioCriterio = true;
    } //-- void setCdDisponibilizacaoSalarioCriterio(int) 

    /**
     * Sets the value of field 'cdDisponibilizacaoSalarioNao'.
     * 
     * @param cdDisponibilizacaoSalarioNao the value of field
     * 'cdDisponibilizacaoSalarioNao'.
     */
    public void setCdDisponibilizacaoSalarioNao(int cdDisponibilizacaoSalarioNao)
    {
        this._cdDisponibilizacaoSalarioNao = cdDisponibilizacaoSalarioNao;
        this._has_cdDisponibilizacaoSalarioNao = true;
    } //-- void setCdDisponibilizacaoSalarioNao(int) 

    /**
     * Sets the value of field 'cdEnvelopeAberto'.
     * 
     * @param cdEnvelopeAberto the value of field 'cdEnvelopeAberto'
     */
    public void setCdEnvelopeAberto(int cdEnvelopeAberto)
    {
        this._cdEnvelopeAberto = cdEnvelopeAberto;
        this._has_cdEnvelopeAberto = true;
    } //-- void setCdEnvelopeAberto(int) 

    /**
     * Sets the value of field 'cdFavorecidoConsultaPagamento'.
     * 
     * @param cdFavorecidoConsultaPagamento the value of field
     * 'cdFavorecidoConsultaPagamento'.
     */
    public void setCdFavorecidoConsultaPagamento(int cdFavorecidoConsultaPagamento)
    {
        this._cdFavorecidoConsultaPagamento = cdFavorecidoConsultaPagamento;
        this._has_cdFavorecidoConsultaPagamento = true;
    } //-- void setCdFavorecidoConsultaPagamento(int) 

    /**
     * Sets the value of field 'cdFormaAutorizacaoPagamento'.
     * 
     * @param cdFormaAutorizacaoPagamento the value of field
     * 'cdFormaAutorizacaoPagamento'.
     */
    public void setCdFormaAutorizacaoPagamento(int cdFormaAutorizacaoPagamento)
    {
        this._cdFormaAutorizacaoPagamento = cdFormaAutorizacaoPagamento;
        this._has_cdFormaAutorizacaoPagamento = true;
    } //-- void setCdFormaAutorizacaoPagamento(int) 

    /**
     * Sets the value of field 'cdFormaEnvioPagamento'.
     * 
     * @param cdFormaEnvioPagamento the value of field
     * 'cdFormaEnvioPagamento'.
     */
    public void setCdFormaEnvioPagamento(int cdFormaEnvioPagamento)
    {
        this._cdFormaEnvioPagamento = cdFormaEnvioPagamento;
        this._has_cdFormaEnvioPagamento = true;
    } //-- void setCdFormaEnvioPagamento(int) 

    /**
     * Sets the value of field 'cdFormaExpiracaoCredito'.
     * 
     * @param cdFormaExpiracaoCredito the value of field
     * 'cdFormaExpiracaoCredito'.
     */
    public void setCdFormaExpiracaoCredito(int cdFormaExpiracaoCredito)
    {
        this._cdFormaExpiracaoCredito = cdFormaExpiracaoCredito;
        this._has_cdFormaExpiracaoCredito = true;
    } //-- void setCdFormaExpiracaoCredito(int) 

    /**
     * Sets the value of field 'cdFormaManutencao'.
     * 
     * @param cdFormaManutencao the value of field
     * 'cdFormaManutencao'.
     */
    public void setCdFormaManutencao(int cdFormaManutencao)
    {
        this._cdFormaManutencao = cdFormaManutencao;
        this._has_cdFormaManutencao = true;
    } //-- void setCdFormaManutencao(int) 

    /**
     * Sets the value of field 'cdFormularioContratoCliente'.
     * 
     * @param cdFormularioContratoCliente the value of field
     * 'cdFormularioContratoCliente'.
     */
    public void setCdFormularioContratoCliente(int cdFormularioContratoCliente)
    {
        this._cdFormularioContratoCliente = cdFormularioContratoCliente;
        this._has_cdFormularioContratoCliente = true;
    } //-- void setCdFormularioContratoCliente(int) 

    /**
     * Sets the value of field 'cdFrasePrecadastrada'.
     * 
     * @param cdFrasePrecadastrada the value of field
     * 'cdFrasePrecadastrada'.
     */
    public void setCdFrasePrecadastrada(int cdFrasePrecadastrada)
    {
        this._cdFrasePrecadastrada = cdFrasePrecadastrada;
        this._has_cdFrasePrecadastrada = true;
    } //-- void setCdFrasePrecadastrada(int) 

    /**
     * Sets the value of field 'cdIndLancamentoPersonalizado'.
     * 
     * @param cdIndLancamentoPersonalizado the value of field
     * 'cdIndLancamentoPersonalizado'.
     */
    public void setCdIndLancamentoPersonalizado(int cdIndLancamentoPersonalizado)
    {
        this._cdIndLancamentoPersonalizado = cdIndLancamentoPersonalizado;
        this._has_cdIndLancamentoPersonalizado = true;
    } //-- void setCdIndLancamentoPersonalizado(int) 

    /**
     * Sets the value of field 'cdIndicadorAdesaoSacador'.
     * 
     * @param cdIndicadorAdesaoSacador the value of field
     * 'cdIndicadorAdesaoSacador'.
     */
    public void setCdIndicadorAdesaoSacador(int cdIndicadorAdesaoSacador)
    {
        this._cdIndicadorAdesaoSacador = cdIndicadorAdesaoSacador;
        this._has_cdIndicadorAdesaoSacador = true;
    } //-- void setCdIndicadorAdesaoSacador(int) 

    /**
     * Sets the value of field 'cdIndicadorAgendaTitulo'.
     * 
     * @param cdIndicadorAgendaTitulo the value of field
     * 'cdIndicadorAgendaTitulo'.
     */
    public void setCdIndicadorAgendaTitulo(int cdIndicadorAgendaTitulo)
    {
        this._cdIndicadorAgendaTitulo = cdIndicadorAgendaTitulo;
        this._has_cdIndicadorAgendaTitulo = true;
    } //-- void setCdIndicadorAgendaTitulo(int) 

    /**
     * Sets the value of field 'cdIndicadorAutorizacaoCliente'.
     * 
     * @param cdIndicadorAutorizacaoCliente the value of field
     * 'cdIndicadorAutorizacaoCliente'.
     */
    public void setCdIndicadorAutorizacaoCliente(int cdIndicadorAutorizacaoCliente)
    {
        this._cdIndicadorAutorizacaoCliente = cdIndicadorAutorizacaoCliente;
        this._has_cdIndicadorAutorizacaoCliente = true;
    } //-- void setCdIndicadorAutorizacaoCliente(int) 

    /**
     * Sets the value of field 'cdIndicadorAutorizacaoComplemento'.
     * 
     * @param cdIndicadorAutorizacaoComplemento the value of field
     * 'cdIndicadorAutorizacaoComplemento'.
     */
    public void setCdIndicadorAutorizacaoComplemento(int cdIndicadorAutorizacaoComplemento)
    {
        this._cdIndicadorAutorizacaoComplemento = cdIndicadorAutorizacaoComplemento;
        this._has_cdIndicadorAutorizacaoComplemento = true;
    } //-- void setCdIndicadorAutorizacaoComplemento(int) 

    /**
     * Sets the value of field 'cdIndicadorBancoPostal'.
     * 
     * @param cdIndicadorBancoPostal the value of field
     * 'cdIndicadorBancoPostal'.
     */
    public void setCdIndicadorBancoPostal(int cdIndicadorBancoPostal)
    {
        this._cdIndicadorBancoPostal = cdIndicadorBancoPostal;
        this._has_cdIndicadorBancoPostal = true;
    } //-- void setCdIndicadorBancoPostal(int) 

    /**
     * Sets the value of field 'cdIndicadorCadastroProcurador'.
     * 
     * @param cdIndicadorCadastroProcurador the value of field
     * 'cdIndicadorCadastroProcurador'.
     */
    public void setCdIndicadorCadastroProcurador(int cdIndicadorCadastroProcurador)
    {
        this._cdIndicadorCadastroProcurador = cdIndicadorCadastroProcurador;
        this._has_cdIndicadorCadastroProcurador = true;
    } //-- void setCdIndicadorCadastroProcurador(int) 

    /**
     * Sets the value of field 'cdIndicadorCadastroorganizacao'.
     * 
     * @param cdIndicadorCadastroorganizacao the value of field
     * 'cdIndicadorCadastroorganizacao'.
     */
    public void setCdIndicadorCadastroorganizacao(int cdIndicadorCadastroorganizacao)
    {
        this._cdIndicadorCadastroorganizacao = cdIndicadorCadastroorganizacao;
        this._has_cdIndicadorCadastroorganizacao = true;
    } //-- void setCdIndicadorCadastroorganizacao(int) 

    /**
     * Sets the value of field 'cdIndicadorCartaoSalario'.
     * 
     * @param cdIndicadorCartaoSalario the value of field
     * 'cdIndicadorCartaoSalario'.
     */
    public void setCdIndicadorCartaoSalario(int cdIndicadorCartaoSalario)
    {
        this._cdIndicadorCartaoSalario = cdIndicadorCartaoSalario;
        this._has_cdIndicadorCartaoSalario = true;
    } //-- void setCdIndicadorCartaoSalario(int) 

    /**
     * Sets the value of field 'cdIndicadorEconomicoReajuste'.
     * 
     * @param cdIndicadorEconomicoReajuste the value of field
     * 'cdIndicadorEconomicoReajuste'.
     */
    public void setCdIndicadorEconomicoReajuste(int cdIndicadorEconomicoReajuste)
    {
        this._cdIndicadorEconomicoReajuste = cdIndicadorEconomicoReajuste;
        this._has_cdIndicadorEconomicoReajuste = true;
    } //-- void setCdIndicadorEconomicoReajuste(int) 

    /**
     * Sets the value of field 'cdIndicadorEmissaoAviso'.
     * 
     * @param cdIndicadorEmissaoAviso the value of field
     * 'cdIndicadorEmissaoAviso'.
     */
    public void setCdIndicadorEmissaoAviso(int cdIndicadorEmissaoAviso)
    {
        this._cdIndicadorEmissaoAviso = cdIndicadorEmissaoAviso;
        this._has_cdIndicadorEmissaoAviso = true;
    } //-- void setCdIndicadorEmissaoAviso(int) 

    /**
     * Sets the value of field 'cdIndicadorListaDebito'.
     * 
     * @param cdIndicadorListaDebito the value of field
     * 'cdIndicadorListaDebito'.
     */
    public void setCdIndicadorListaDebito(int cdIndicadorListaDebito)
    {
        this._cdIndicadorListaDebito = cdIndicadorListaDebito;
        this._has_cdIndicadorListaDebito = true;
    } //-- void setCdIndicadorListaDebito(int) 

    /**
     * Sets the value of field 'cdIndicadorMensagemPersonalizada'.
     * 
     * @param cdIndicadorMensagemPersonalizada the value of field
     * 'cdIndicadorMensagemPersonalizada'.
     */
    public void setCdIndicadorMensagemPersonalizada(int cdIndicadorMensagemPersonalizada)
    {
        this._cdIndicadorMensagemPersonalizada = cdIndicadorMensagemPersonalizada;
        this._has_cdIndicadorMensagemPersonalizada = true;
    } //-- void setCdIndicadorMensagemPersonalizada(int) 

    /**
     * Sets the value of field 'cdIndicadorRetornoInternet'.
     * 
     * @param cdIndicadorRetornoInternet the value of field
     * 'cdIndicadorRetornoInternet'.
     */
    public void setCdIndicadorRetornoInternet(int cdIndicadorRetornoInternet)
    {
        this._cdIndicadorRetornoInternet = cdIndicadorRetornoInternet;
        this._has_cdIndicadorRetornoInternet = true;
    } //-- void setCdIndicadorRetornoInternet(int) 

    /**
     * Sets the value of field 'cdIndicadorRetornoSeparado'.
     * 
     * @param cdIndicadorRetornoSeparado the value of field
     * 'cdIndicadorRetornoSeparado'.
     */
    public void setCdIndicadorRetornoSeparado(int cdIndicadorRetornoSeparado)
    {
        this._cdIndicadorRetornoSeparado = cdIndicadorRetornoSeparado;
        this._has_cdIndicadorRetornoSeparado = true;
    } //-- void setCdIndicadorRetornoSeparado(int) 

    /**
     * Sets the value of field 'cdLancamentoFuturoCredito'.
     * 
     * @param cdLancamentoFuturoCredito the value of field
     * 'cdLancamentoFuturoCredito'.
     */
    public void setCdLancamentoFuturoCredito(int cdLancamentoFuturoCredito)
    {
        this._cdLancamentoFuturoCredito = cdLancamentoFuturoCredito;
        this._has_cdLancamentoFuturoCredito = true;
    } //-- void setCdLancamentoFuturoCredito(int) 

    /**
     * Sets the value of field 'cdLancamentoFuturoDebito'.
     * 
     * @param cdLancamentoFuturoDebito the value of field
     * 'cdLancamentoFuturoDebito'.
     */
    public void setCdLancamentoFuturoDebito(int cdLancamentoFuturoDebito)
    {
        this._cdLancamentoFuturoDebito = cdLancamentoFuturoDebito;
        this._has_cdLancamentoFuturoDebito = true;
    } //-- void setCdLancamentoFuturoDebito(int) 

    /**
     * Sets the value of field 'cdLiberacaoLoteProcs'.
     * 
     * @param cdLiberacaoLoteProcs the value of field
     * 'cdLiberacaoLoteProcs'.
     */
    public void setCdLiberacaoLoteProcs(int cdLiberacaoLoteProcs)
    {
        this._cdLiberacaoLoteProcs = cdLiberacaoLoteProcs;
        this._has_cdLiberacaoLoteProcs = true;
    } //-- void setCdLiberacaoLoteProcs(int) 

    /**
     * Sets the value of field 'cdManutencaoBaseRecadastramento'.
     * 
     * @param cdManutencaoBaseRecadastramento the value of field
     * 'cdManutencaoBaseRecadastramento'.
     */
    public void setCdManutencaoBaseRecadastramento(int cdManutencaoBaseRecadastramento)
    {
        this._cdManutencaoBaseRecadastramento = cdManutencaoBaseRecadastramento;
        this._has_cdManutencaoBaseRecadastramento = true;
    } //-- void setCdManutencaoBaseRecadastramento(int) 

    /**
     * Sets the value of field 'cdMeioPagamentoCredito'.
     * 
     * @param cdMeioPagamentoCredito the value of field
     * 'cdMeioPagamentoCredito'.
     */
    public void setCdMeioPagamentoCredito(int cdMeioPagamentoCredito)
    {
        this._cdMeioPagamentoCredito = cdMeioPagamentoCredito;
        this._has_cdMeioPagamentoCredito = true;
    } //-- void setCdMeioPagamentoCredito(int) 

    /**
     * Sets the value of field 'cdMensagemRecadastramentoMidia'.
     * 
     * @param cdMensagemRecadastramentoMidia the value of field
     * 'cdMensagemRecadastramentoMidia'.
     */
    public void setCdMensagemRecadastramentoMidia(int cdMensagemRecadastramentoMidia)
    {
        this._cdMensagemRecadastramentoMidia = cdMensagemRecadastramentoMidia;
        this._has_cdMensagemRecadastramentoMidia = true;
    } //-- void setCdMensagemRecadastramentoMidia(int) 

    /**
     * Sets the value of field 'cdMidiaDisponivel'.
     * 
     * @param cdMidiaDisponivel the value of field
     * 'cdMidiaDisponivel'.
     */
    public void setCdMidiaDisponivel(int cdMidiaDisponivel)
    {
        this._cdMidiaDisponivel = cdMidiaDisponivel;
        this._has_cdMidiaDisponivel = true;
    } //-- void setCdMidiaDisponivel(int) 

    /**
     * Sets the value of field 'cdMidiaMensagemRecadastramento'.
     * 
     * @param cdMidiaMensagemRecadastramento the value of field
     * 'cdMidiaMensagemRecadastramento'.
     */
    public void setCdMidiaMensagemRecadastramento(int cdMidiaMensagemRecadastramento)
    {
        this._cdMidiaMensagemRecadastramento = cdMidiaMensagemRecadastramento;
        this._has_cdMidiaMensagemRecadastramento = true;
    } //-- void setCdMidiaMensagemRecadastramento(int) 

    /**
     * Sets the value of field 'cdMomentoAvisoRecadastramento'.
     * 
     * @param cdMomentoAvisoRecadastramento the value of field
     * 'cdMomentoAvisoRecadastramento'.
     */
    public void setCdMomentoAvisoRecadastramento(int cdMomentoAvisoRecadastramento)
    {
        this._cdMomentoAvisoRecadastramento = cdMomentoAvisoRecadastramento;
        this._has_cdMomentoAvisoRecadastramento = true;
    } //-- void setCdMomentoAvisoRecadastramento(int) 

    /**
     * Sets the value of field 'cdMomentoCreditoEfetivacao'.
     * 
     * @param cdMomentoCreditoEfetivacao the value of field
     * 'cdMomentoCreditoEfetivacao'.
     */
    public void setCdMomentoCreditoEfetivacao(int cdMomentoCreditoEfetivacao)
    {
        this._cdMomentoCreditoEfetivacao = cdMomentoCreditoEfetivacao;
        this._has_cdMomentoCreditoEfetivacao = true;
    } //-- void setCdMomentoCreditoEfetivacao(int) 

    /**
     * Sets the value of field 'cdMomentoDebitoPagamento'.
     * 
     * @param cdMomentoDebitoPagamento the value of field
     * 'cdMomentoDebitoPagamento'.
     */
    public void setCdMomentoDebitoPagamento(int cdMomentoDebitoPagamento)
    {
        this._cdMomentoDebitoPagamento = cdMomentoDebitoPagamento;
        this._has_cdMomentoDebitoPagamento = true;
    } //-- void setCdMomentoDebitoPagamento(int) 

    /**
     * Sets the value of field
     * 'cdMomentoFormularioRecadastramento'.
     * 
     * @param cdMomentoFormularioRecadastramento the value of field
     * 'cdMomentoFormularioRecadastramento'.
     */
    public void setCdMomentoFormularioRecadastramento(int cdMomentoFormularioRecadastramento)
    {
        this._cdMomentoFormularioRecadastramento = cdMomentoFormularioRecadastramento;
        this._has_cdMomentoFormularioRecadastramento = true;
    } //-- void setCdMomentoFormularioRecadastramento(int) 

    /**
     * Sets the value of field 'cdMomentoProcessamentoPagamento'.
     * 
     * @param cdMomentoProcessamentoPagamento the value of field
     * 'cdMomentoProcessamentoPagamento'.
     */
    public void setCdMomentoProcessamentoPagamento(int cdMomentoProcessamentoPagamento)
    {
        this._cdMomentoProcessamentoPagamento = cdMomentoProcessamentoPagamento;
        this._has_cdMomentoProcessamentoPagamento = true;
    } //-- void setCdMomentoProcessamentoPagamento(int) 

    /**
     * Sets the value of field 'cdNaturezaOperacaoPagamento'.
     * 
     * @param cdNaturezaOperacaoPagamento the value of field
     * 'cdNaturezaOperacaoPagamento'.
     */
    public void setCdNaturezaOperacaoPagamento(int cdNaturezaOperacaoPagamento)
    {
        this._cdNaturezaOperacaoPagamento = cdNaturezaOperacaoPagamento;
        this._has_cdNaturezaOperacaoPagamento = true;
    } //-- void setCdNaturezaOperacaoPagamento(int) 

    /**
     * Sets the value of field 'cdPagamentoNaoUtil'.
     * 
     * @param cdPagamentoNaoUtil the value of field
     * 'cdPagamentoNaoUtil'.
     */
    public void setCdPagamentoNaoUtil(int cdPagamentoNaoUtil)
    {
        this._cdPagamentoNaoUtil = cdPagamentoNaoUtil;
        this._has_cdPagamentoNaoUtil = true;
    } //-- void setCdPagamentoNaoUtil(int) 

    /**
     * Sets the value of field 'cdPeriodicidadeAviso'.
     * 
     * @param cdPeriodicidadeAviso the value of field
     * 'cdPeriodicidadeAviso'.
     */
    public void setCdPeriodicidadeAviso(int cdPeriodicidadeAviso)
    {
        this._cdPeriodicidadeAviso = cdPeriodicidadeAviso;
        this._has_cdPeriodicidadeAviso = true;
    } //-- void setCdPeriodicidadeAviso(int) 

    /**
     * Sets the value of field 'cdPeriodicidadeCobrancaTarifa'.
     * 
     * @param cdPeriodicidadeCobrancaTarifa the value of field
     * 'cdPeriodicidadeCobrancaTarifa'.
     */
    public void setCdPeriodicidadeCobrancaTarifa(int cdPeriodicidadeCobrancaTarifa)
    {
        this._cdPeriodicidadeCobrancaTarifa = cdPeriodicidadeCobrancaTarifa;
        this._has_cdPeriodicidadeCobrancaTarifa = true;
    } //-- void setCdPeriodicidadeCobrancaTarifa(int) 

    /**
     * Sets the value of field 'cdPeriodicidadeComprovante'.
     * 
     * @param cdPeriodicidadeComprovante the value of field
     * 'cdPeriodicidadeComprovante'.
     */
    public void setCdPeriodicidadeComprovante(int cdPeriodicidadeComprovante)
    {
        this._cdPeriodicidadeComprovante = cdPeriodicidadeComprovante;
        this._has_cdPeriodicidadeComprovante = true;
    } //-- void setCdPeriodicidadeComprovante(int) 

    /**
     * Sets the value of field 'cdPeriodicidadeConsultaVeiculo'.
     * 
     * @param cdPeriodicidadeConsultaVeiculo the value of field
     * 'cdPeriodicidadeConsultaVeiculo'.
     */
    public void setCdPeriodicidadeConsultaVeiculo(int cdPeriodicidadeConsultaVeiculo)
    {
        this._cdPeriodicidadeConsultaVeiculo = cdPeriodicidadeConsultaVeiculo;
        this._has_cdPeriodicidadeConsultaVeiculo = true;
    } //-- void setCdPeriodicidadeConsultaVeiculo(int) 

    /**
     * Sets the value of field 'cdPeriodicidadeEnvioRemessa'.
     * 
     * @param cdPeriodicidadeEnvioRemessa the value of field
     * 'cdPeriodicidadeEnvioRemessa'.
     */
    public void setCdPeriodicidadeEnvioRemessa(int cdPeriodicidadeEnvioRemessa)
    {
        this._cdPeriodicidadeEnvioRemessa = cdPeriodicidadeEnvioRemessa;
        this._has_cdPeriodicidadeEnvioRemessa = true;
    } //-- void setCdPeriodicidadeEnvioRemessa(int) 

    /**
     * Sets the value of field 'cdPeriodicidadeManutencaoProcd'.
     * 
     * @param cdPeriodicidadeManutencaoProcd the value of field
     * 'cdPeriodicidadeManutencaoProcd'.
     */
    public void setCdPeriodicidadeManutencaoProcd(int cdPeriodicidadeManutencaoProcd)
    {
        this._cdPeriodicidadeManutencaoProcd = cdPeriodicidadeManutencaoProcd;
        this._has_cdPeriodicidadeManutencaoProcd = true;
    } //-- void setCdPeriodicidadeManutencaoProcd(int) 

    /**
     * Sets the value of field 'cdPermissaoDebitoOnline'.
     * 
     * @param cdPermissaoDebitoOnline the value of field
     * 'cdPermissaoDebitoOnline'.
     */
    public void setCdPermissaoDebitoOnline(int cdPermissaoDebitoOnline)
    {
        this._cdPermissaoDebitoOnline = cdPermissaoDebitoOnline;
        this._has_cdPermissaoDebitoOnline = true;
    } //-- void setCdPermissaoDebitoOnline(int) 

    /**
     * Sets the value of field 'cdPrincipalEnquaRecadastramento'.
     * 
     * @param cdPrincipalEnquaRecadastramento the value of field
     * 'cdPrincipalEnquaRecadastramento'.
     */
    public void setCdPrincipalEnquaRecadastramento(int cdPrincipalEnquaRecadastramento)
    {
        this._cdPrincipalEnquaRecadastramento = cdPrincipalEnquaRecadastramento;
        this._has_cdPrincipalEnquaRecadastramento = true;
    } //-- void setCdPrincipalEnquaRecadastramento(int) 

    /**
     * Sets the value of field 'cdPrioridadeEfetivacaoPagamento'.
     * 
     * @param cdPrioridadeEfetivacaoPagamento the value of field
     * 'cdPrioridadeEfetivacaoPagamento'.
     */
    public void setCdPrioridadeEfetivacaoPagamento(int cdPrioridadeEfetivacaoPagamento)
    {
        this._cdPrioridadeEfetivacaoPagamento = cdPrioridadeEfetivacaoPagamento;
        this._has_cdPrioridadeEfetivacaoPagamento = true;
    } //-- void setCdPrioridadeEfetivacaoPagamento(int) 

    /**
     * Sets the value of field 'cdRastreabNotaFiscal'.
     * 
     * @param cdRastreabNotaFiscal the value of field
     * 'cdRastreabNotaFiscal'.
     */
    public void setCdRastreabNotaFiscal(int cdRastreabNotaFiscal)
    {
        this._cdRastreabNotaFiscal = cdRastreabNotaFiscal;
        this._has_cdRastreabNotaFiscal = true;
    } //-- void setCdRastreabNotaFiscal(int) 

    /**
     * Sets the value of field 'cdRastreabTituloTerc'.
     * 
     * @param cdRastreabTituloTerc the value of field
     * 'cdRastreabTituloTerc'.
     */
    public void setCdRastreabTituloTerc(int cdRastreabTituloTerc)
    {
        this._cdRastreabTituloTerc = cdRastreabTituloTerc;
        this._has_cdRastreabTituloTerc = true;
    } //-- void setCdRastreabTituloTerc(int) 

    /**
     * Sets the value of field 'cdRejeicaoAgendamentoLote'.
     * 
     * @param cdRejeicaoAgendamentoLote the value of field
     * 'cdRejeicaoAgendamentoLote'.
     */
    public void setCdRejeicaoAgendamentoLote(int cdRejeicaoAgendamentoLote)
    {
        this._cdRejeicaoAgendamentoLote = cdRejeicaoAgendamentoLote;
        this._has_cdRejeicaoAgendamentoLote = true;
    } //-- void setCdRejeicaoAgendamentoLote(int) 

    /**
     * Sets the value of field 'cdRejeicaoEfetivacaoLote'.
     * 
     * @param cdRejeicaoEfetivacaoLote the value of field
     * 'cdRejeicaoEfetivacaoLote'.
     */
    public void setCdRejeicaoEfetivacaoLote(int cdRejeicaoEfetivacaoLote)
    {
        this._cdRejeicaoEfetivacaoLote = cdRejeicaoEfetivacaoLote;
        this._has_cdRejeicaoEfetivacaoLote = true;
    } //-- void setCdRejeicaoEfetivacaoLote(int) 

    /**
     * Sets the value of field 'cdRejeicaoLote'.
     * 
     * @param cdRejeicaoLote the value of field 'cdRejeicaoLote'.
     */
    public void setCdRejeicaoLote(int cdRejeicaoLote)
    {
        this._cdRejeicaoLote = cdRejeicaoLote;
        this._has_cdRejeicaoLote = true;
    } //-- void setCdRejeicaoLote(int) 

    /**
     * Sets the value of field 'cdTipoCargaRecadastramento'.
     * 
     * @param cdTipoCargaRecadastramento the value of field
     * 'cdTipoCargaRecadastramento'.
     */
    public void setCdTipoCargaRecadastramento(int cdTipoCargaRecadastramento)
    {
        this._cdTipoCargaRecadastramento = cdTipoCargaRecadastramento;
        this._has_cdTipoCargaRecadastramento = true;
    } //-- void setCdTipoCargaRecadastramento(int) 

    /**
     * Sets the value of field 'cdTipoCartaoSalario'.
     * 
     * @param cdTipoCartaoSalario the value of field
     * 'cdTipoCartaoSalario'.
     */
    public void setCdTipoCartaoSalario(int cdTipoCartaoSalario)
    {
        this._cdTipoCartaoSalario = cdTipoCartaoSalario;
        this._has_cdTipoCartaoSalario = true;
    } //-- void setCdTipoCartaoSalario(int) 

    /**
     * Sets the value of field 'cdTipoConsistenciaFavorecido'.
     * 
     * @param cdTipoConsistenciaFavorecido the value of field
     * 'cdTipoConsistenciaFavorecido'.
     */
    public void setCdTipoConsistenciaFavorecido(int cdTipoConsistenciaFavorecido)
    {
        this._cdTipoConsistenciaFavorecido = cdTipoConsistenciaFavorecido;
        this._has_cdTipoConsistenciaFavorecido = true;
    } //-- void setCdTipoConsistenciaFavorecido(int) 

    /**
     * Sets the value of field 'cdTipoConsistenciaLista'.
     * 
     * @param cdTipoConsistenciaLista the value of field
     * 'cdTipoConsistenciaLista'.
     */
    public void setCdTipoConsistenciaLista(int cdTipoConsistenciaLista)
    {
        this._cdTipoConsistenciaLista = cdTipoConsistenciaLista;
        this._has_cdTipoConsistenciaLista = true;
    } //-- void setCdTipoConsistenciaLista(int) 

    /**
     * Sets the value of field 'cdTipoConsultaComprovante'.
     * 
     * @param cdTipoConsultaComprovante the value of field
     * 'cdTipoConsultaComprovante'.
     */
    public void setCdTipoConsultaComprovante(int cdTipoConsultaComprovante)
    {
        this._cdTipoConsultaComprovante = cdTipoConsultaComprovante;
        this._has_cdTipoConsultaComprovante = true;
    } //-- void setCdTipoConsultaComprovante(int) 

    /**
     * Sets the value of field 'cdTipoDataFloating'.
     * 
     * @param cdTipoDataFloating the value of field
     * 'cdTipoDataFloating'.
     */
    public void setCdTipoDataFloating(int cdTipoDataFloating)
    {
        this._cdTipoDataFloating = cdTipoDataFloating;
        this._has_cdTipoDataFloating = true;
    } //-- void setCdTipoDataFloating(int) 

    /**
     * Sets the value of field 'cdTipoDivergenciaVeiculo'.
     * 
     * @param cdTipoDivergenciaVeiculo the value of field
     * 'cdTipoDivergenciaVeiculo'.
     */
    public void setCdTipoDivergenciaVeiculo(int cdTipoDivergenciaVeiculo)
    {
        this._cdTipoDivergenciaVeiculo = cdTipoDivergenciaVeiculo;
        this._has_cdTipoDivergenciaVeiculo = true;
    } //-- void setCdTipoDivergenciaVeiculo(int) 

    /**
     * Sets the value of field 'cdTipoFormacaoLista'.
     * 
     * @param cdTipoFormacaoLista the value of field
     * 'cdTipoFormacaoLista'.
     */
    public void setCdTipoFormacaoLista(int cdTipoFormacaoLista)
    {
        this._cdTipoFormacaoLista = cdTipoFormacaoLista;
        this._has_cdTipoFormacaoLista = true;
    } //-- void setCdTipoFormacaoLista(int) 

    /**
     * Sets the value of field 'cdTipoIdentificacaoBeneficio'.
     * 
     * @param cdTipoIdentificacaoBeneficio the value of field
     * 'cdTipoIdentificacaoBeneficio'.
     */
    public void setCdTipoIdentificacaoBeneficio(int cdTipoIdentificacaoBeneficio)
    {
        this._cdTipoIdentificacaoBeneficio = cdTipoIdentificacaoBeneficio;
        this._has_cdTipoIdentificacaoBeneficio = true;
    } //-- void setCdTipoIdentificacaoBeneficio(int) 

    /**
     * Sets the value of field 'cdTipoInscricaoFavorecidoAceita'.
     * 
     * @param cdTipoInscricaoFavorecidoAceita the value of field
     * 'cdTipoInscricaoFavorecidoAceita'.
     */
    public void setCdTipoInscricaoFavorecidoAceita(int cdTipoInscricaoFavorecidoAceita)
    {
        this._cdTipoInscricaoFavorecidoAceita = cdTipoInscricaoFavorecidoAceita;
        this._has_cdTipoInscricaoFavorecidoAceita = true;
    } //-- void setCdTipoInscricaoFavorecidoAceita(int) 

    /**
     * Sets the value of field 'cdTipoLayoutArquivo'.
     * 
     * @param cdTipoLayoutArquivo the value of field
     * 'cdTipoLayoutArquivo'.
     */
    public void setCdTipoLayoutArquivo(int cdTipoLayoutArquivo)
    {
        this._cdTipoLayoutArquivo = cdTipoLayoutArquivo;
        this._has_cdTipoLayoutArquivo = true;
    } //-- void setCdTipoLayoutArquivo(int) 

    /**
     * Sets the value of field 'cdTipoReajusteTarifa'.
     * 
     * @param cdTipoReajusteTarifa the value of field
     * 'cdTipoReajusteTarifa'.
     */
    public void setCdTipoReajusteTarifa(int cdTipoReajusteTarifa)
    {
        this._cdTipoReajusteTarifa = cdTipoReajusteTarifa;
        this._has_cdTipoReajusteTarifa = true;
    } //-- void setCdTipoReajusteTarifa(int) 

    /**
     * Sets the value of field 'cdTratoContaTransf'.
     * 
     * @param cdTratoContaTransf the value of field
     * 'cdTratoContaTransf'.
     */
    public void setCdTratoContaTransf(int cdTratoContaTransf)
    {
        this._cdTratoContaTransf = cdTratoContaTransf;
        this._has_cdTratoContaTransf = true;
    } //-- void setCdTratoContaTransf(int) 

    /**
     * Sets the value of field 'cdUsuarioAlteracao'.
     * 
     * @param cdUsuarioAlteracao the value of field
     * 'cdUsuarioAlteracao'.
     */
    public void setCdUsuarioAlteracao(java.lang.String cdUsuarioAlteracao)
    {
        this._cdUsuarioAlteracao = cdUsuarioAlteracao;
    } //-- void setCdUsuarioAlteracao(java.lang.String) 

    /**
     * Sets the value of field 'cdUsuarioExternoAlteracao'.
     * 
     * @param cdUsuarioExternoAlteracao the value of field
     * 'cdUsuarioExternoAlteracao'.
     */
    public void setCdUsuarioExternoAlteracao(java.lang.String cdUsuarioExternoAlteracao)
    {
        this._cdUsuarioExternoAlteracao = cdUsuarioExternoAlteracao;
    } //-- void setCdUsuarioExternoAlteracao(java.lang.String) 

    /**
     * Sets the value of field 'cdUsuarioExternoInclusao'.
     * 
     * @param cdUsuarioExternoInclusao the value of field
     * 'cdUsuarioExternoInclusao'.
     */
    public void setCdUsuarioExternoInclusao(java.lang.String cdUsuarioExternoInclusao)
    {
        this._cdUsuarioExternoInclusao = cdUsuarioExternoInclusao;
    } //-- void setCdUsuarioExternoInclusao(java.lang.String) 

    /**
     * Sets the value of field 'cdUsuarioInclusao'.
     * 
     * @param cdUsuarioInclusao the value of field
     * 'cdUsuarioInclusao'.
     */
    public void setCdUsuarioInclusao(java.lang.String cdUsuarioInclusao)
    {
        this._cdUsuarioInclusao = cdUsuarioInclusao;
    } //-- void setCdUsuarioInclusao(java.lang.String) 

    /**
     * Sets the value of field 'cdUtilizacaoFavorecidoControle'.
     * 
     * @param cdUtilizacaoFavorecidoControle the value of field
     * 'cdUtilizacaoFavorecidoControle'.
     */
    public void setCdUtilizacaoFavorecidoControle(int cdUtilizacaoFavorecidoControle)
    {
        this._cdUtilizacaoFavorecidoControle = cdUtilizacaoFavorecidoControle;
        this._has_cdUtilizacaoFavorecidoControle = true;
    } //-- void setCdUtilizacaoFavorecidoControle(int) 

    /**
     * Sets the value of field 'cdValidacaoNomeFavorecido'.
     * 
     * @param cdValidacaoNomeFavorecido the value of field
     * 'cdValidacaoNomeFavorecido'.
     */
    public void setCdValidacaoNomeFavorecido(int cdValidacaoNomeFavorecido)
    {
        this._cdValidacaoNomeFavorecido = cdValidacaoNomeFavorecido;
        this._has_cdValidacaoNomeFavorecido = true;
    } //-- void setCdValidacaoNomeFavorecido(int) 

    /**
     * Sets the value of field 'cdindicadorExpiraCredito'.
     * 
     * @param cdindicadorExpiraCredito the value of field
     * 'cdindicadorExpiraCredito'.
     */
    public void setCdindicadorExpiraCredito(int cdindicadorExpiraCredito)
    {
        this._cdindicadorExpiraCredito = cdindicadorExpiraCredito;
        this._has_cdindicadorExpiraCredito = true;
    } //-- void setCdindicadorExpiraCredito(int) 

    /**
     * Sets the value of field 'cdindicadorLancamentoProgramado'.
     * 
     * @param cdindicadorLancamentoProgramado the value of field
     * 'cdindicadorLancamentoProgramado'.
     */
    public void setCdindicadorLancamentoProgramado(int cdindicadorLancamentoProgramado)
    {
        this._cdindicadorLancamentoProgramado = cdindicadorLancamentoProgramado;
        this._has_cdindicadorLancamentoProgramado = true;
    } //-- void setCdindicadorLancamentoProgramado(int) 

    /**
     * Sets the value of field 'codMensagem'.
     * 
     * @param codMensagem the value of field 'codMensagem'.
     */
    public void setCodMensagem(java.lang.String codMensagem)
    {
        this._codMensagem = codMensagem;
    } //-- void setCodMensagem(java.lang.String) 

    /**
     * Sets the value of field 'dsAcaoNaoVida'.
     * 
     * @param dsAcaoNaoVida the value of field 'dsAcaoNaoVida'.
     */
    public void setDsAcaoNaoVida(java.lang.String dsAcaoNaoVida)
    {
        this._dsAcaoNaoVida = dsAcaoNaoVida;
    } //-- void setDsAcaoNaoVida(java.lang.String) 

    /**
     * Sets the value of field 'dsAcertoDadosRecadastramento'.
     * 
     * @param dsAcertoDadosRecadastramento the value of field
     * 'dsAcertoDadosRecadastramento'.
     */
    public void setDsAcertoDadosRecadastramento(java.lang.String dsAcertoDadosRecadastramento)
    {
        this._dsAcertoDadosRecadastramento = dsAcertoDadosRecadastramento;
    } //-- void setDsAcertoDadosRecadastramento(java.lang.String) 

    /**
     * Sets the value of field 'dsAgendamentoDebitoVeiculo'.
     * 
     * @param dsAgendamentoDebitoVeiculo the value of field
     * 'dsAgendamentoDebitoVeiculo'.
     */
    public void setDsAgendamentoDebitoVeiculo(java.lang.String dsAgendamentoDebitoVeiculo)
    {
        this._dsAgendamentoDebitoVeiculo = dsAgendamentoDebitoVeiculo;
    } //-- void setDsAgendamentoDebitoVeiculo(java.lang.String) 

    /**
     * Sets the value of field 'dsAgendamentoPagamentoVencido'.
     * 
     * @param dsAgendamentoPagamentoVencido the value of field
     * 'dsAgendamentoPagamentoVencido'.
     */
    public void setDsAgendamentoPagamentoVencido(java.lang.String dsAgendamentoPagamentoVencido)
    {
        this._dsAgendamentoPagamentoVencido = dsAgendamentoPagamentoVencido;
    } //-- void setDsAgendamentoPagamentoVencido(java.lang.String) 

    /**
     * Sets the value of field 'dsAgendamentoRastFilial'.
     * 
     * @param dsAgendamentoRastFilial the value of field
     * 'dsAgendamentoRastFilial'.
     */
    public void setDsAgendamentoRastFilial(java.lang.String dsAgendamentoRastFilial)
    {
        this._dsAgendamentoRastFilial = dsAgendamentoRastFilial;
    } //-- void setDsAgendamentoRastFilial(java.lang.String) 

    /**
     * Sets the value of field 'dsAgendamentoValorMenor'.
     * 
     * @param dsAgendamentoValorMenor the value of field
     * 'dsAgendamentoValorMenor'.
     */
    public void setDsAgendamentoValorMenor(java.lang.String dsAgendamentoValorMenor)
    {
        this._dsAgendamentoValorMenor = dsAgendamentoValorMenor;
    } //-- void setDsAgendamentoValorMenor(java.lang.String) 

    /**
     * Sets the value of field 'dsAgrupamentoAviso'.
     * 
     * @param dsAgrupamentoAviso the value of field
     * 'dsAgrupamentoAviso'.
     */
    public void setDsAgrupamentoAviso(java.lang.String dsAgrupamentoAviso)
    {
        this._dsAgrupamentoAviso = dsAgrupamentoAviso;
    } //-- void setDsAgrupamentoAviso(java.lang.String) 

    /**
     * Sets the value of field 'dsAgrupamentoComprovante'.
     * 
     * @param dsAgrupamentoComprovante the value of field
     * 'dsAgrupamentoComprovante'.
     */
    public void setDsAgrupamentoComprovante(java.lang.String dsAgrupamentoComprovante)
    {
        this._dsAgrupamentoComprovante = dsAgrupamentoComprovante;
    } //-- void setDsAgrupamentoComprovante(java.lang.String) 

    /**
     * Sets the value of field 'dsAgrupamentoFormularioRecadastro'.
     * 
     * @param dsAgrupamentoFormularioRecadastro the value of field
     * 'dsAgrupamentoFormularioRecadastro'.
     */
    public void setDsAgrupamentoFormularioRecadastro(java.lang.String dsAgrupamentoFormularioRecadastro)
    {
        this._dsAgrupamentoFormularioRecadastro = dsAgrupamentoFormularioRecadastro;
    } //-- void setDsAgrupamentoFormularioRecadastro(java.lang.String) 

    /**
     * Sets the value of field
     * 'dsAntecipacaoRecadastramentoBeneficiario'.
     * 
     * @param dsAntecipacaoRecadastramentoBeneficiario the value of
     * field 'dsAntecipacaoRecadastramentoBeneficiario'.
     */
    public void setDsAntecipacaoRecadastramentoBeneficiario(java.lang.String dsAntecipacaoRecadastramentoBeneficiario)
    {
        this._dsAntecipacaoRecadastramentoBeneficiario = dsAntecipacaoRecadastramentoBeneficiario;
    } //-- void setDsAntecipacaoRecadastramentoBeneficiario(java.lang.String) 

    /**
     * Sets the value of field 'dsAreaReservada'.
     * 
     * @param dsAreaReservada the value of field 'dsAreaReservada'.
     */
    public void setDsAreaReservada(java.lang.String dsAreaReservada)
    {
        this._dsAreaReservada = dsAreaReservada;
    } //-- void setDsAreaReservada(java.lang.String) 

    /**
     * Sets the value of field 'dsAreaReservada2'.
     * 
     * @param dsAreaReservada2 the value of field 'dsAreaReservada2'
     */
    public void setDsAreaReservada2(java.lang.String dsAreaReservada2)
    {
        this._dsAreaReservada2 = dsAreaReservada2;
    } //-- void setDsAreaReservada2(java.lang.String) 

    /**
     * Sets the value of field 'dsBaseRecadastramentoBeneficio'.
     * 
     * @param dsBaseRecadastramentoBeneficio the value of field
     * 'dsBaseRecadastramentoBeneficio'.
     */
    public void setDsBaseRecadastramentoBeneficio(java.lang.String dsBaseRecadastramentoBeneficio)
    {
        this._dsBaseRecadastramentoBeneficio = dsBaseRecadastramentoBeneficio;
    } //-- void setDsBaseRecadastramentoBeneficio(java.lang.String) 

    /**
     * Sets the value of field 'dsBloqueioEmissaoPapeleta'.
     * 
     * @param dsBloqueioEmissaoPapeleta the value of field
     * 'dsBloqueioEmissaoPapeleta'.
     */
    public void setDsBloqueioEmissaoPapeleta(java.lang.String dsBloqueioEmissaoPapeleta)
    {
        this._dsBloqueioEmissaoPapeleta = dsBloqueioEmissaoPapeleta;
    } //-- void setDsBloqueioEmissaoPapeleta(java.lang.String) 

    /**
     * Sets the value of field 'dsCanalAlteracao'.
     * 
     * @param dsCanalAlteracao the value of field 'dsCanalAlteracao'
     */
    public void setDsCanalAlteracao(java.lang.String dsCanalAlteracao)
    {
        this._dsCanalAlteracao = dsCanalAlteracao;
    } //-- void setDsCanalAlteracao(java.lang.String) 

    /**
     * Sets the value of field 'dsCanalInclusao'.
     * 
     * @param dsCanalInclusao the value of field 'dsCanalInclusao'.
     */
    public void setDsCanalInclusao(java.lang.String dsCanalInclusao)
    {
        this._dsCanalInclusao = dsCanalInclusao;
    } //-- void setDsCanalInclusao(java.lang.String) 

    /**
     * Sets the value of field 'dsCapturaTituloRegistrado'.
     * 
     * @param dsCapturaTituloRegistrado the value of field
     * 'dsCapturaTituloRegistrado'.
     */
    public void setDsCapturaTituloRegistrado(java.lang.String dsCapturaTituloRegistrado)
    {
        this._dsCapturaTituloRegistrado = dsCapturaTituloRegistrado;
    } //-- void setDsCapturaTituloRegistrado(java.lang.String) 

    /**
     * Sets the value of field 'dsCctciaEspeBeneficio'.
     * 
     * @param dsCctciaEspeBeneficio the value of field
     * 'dsCctciaEspeBeneficio'.
     */
    public void setDsCctciaEspeBeneficio(java.lang.String dsCctciaEspeBeneficio)
    {
        this._dsCctciaEspeBeneficio = dsCctciaEspeBeneficio;
    } //-- void setDsCctciaEspeBeneficio(java.lang.String) 

    /**
     * Sets the value of field 'dsCctciaIdentificacaoBeneficio'.
     * 
     * @param dsCctciaIdentificacaoBeneficio the value of field
     * 'dsCctciaIdentificacaoBeneficio'.
     */
    public void setDsCctciaIdentificacaoBeneficio(java.lang.String dsCctciaIdentificacaoBeneficio)
    {
        this._dsCctciaIdentificacaoBeneficio = dsCctciaIdentificacaoBeneficio;
    } //-- void setDsCctciaIdentificacaoBeneficio(java.lang.String) 

    /**
     * Sets the value of field 'dsCctciaInscricaoFavorecido'.
     * 
     * @param dsCctciaInscricaoFavorecido the value of field
     * 'dsCctciaInscricaoFavorecido'.
     */
    public void setDsCctciaInscricaoFavorecido(java.lang.String dsCctciaInscricaoFavorecido)
    {
        this._dsCctciaInscricaoFavorecido = dsCctciaInscricaoFavorecido;
    } //-- void setDsCctciaInscricaoFavorecido(java.lang.String) 

    /**
     * Sets the value of field 'dsCctciaProprietarioVeculo'.
     * 
     * @param dsCctciaProprietarioVeculo the value of field
     * 'dsCctciaProprietarioVeculo'.
     */
    public void setDsCctciaProprietarioVeculo(java.lang.String dsCctciaProprietarioVeculo)
    {
        this._dsCctciaProprietarioVeculo = dsCctciaProprietarioVeculo;
    } //-- void setDsCctciaProprietarioVeculo(java.lang.String) 

    /**
     * Sets the value of field 'dsCobrancaTarifa'.
     * 
     * @param dsCobrancaTarifa the value of field 'dsCobrancaTarifa'
     */
    public void setDsCobrancaTarifa(java.lang.String dsCobrancaTarifa)
    {
        this._dsCobrancaTarifa = dsCobrancaTarifa;
    } //-- void setDsCobrancaTarifa(java.lang.String) 

    /**
     * Sets the value of field 'dsCodigoFormularioContratoCliente'.
     * 
     * @param dsCodigoFormularioContratoCliente the value of field
     * 'dsCodigoFormularioContratoCliente'.
     */
    public void setDsCodigoFormularioContratoCliente(java.lang.String dsCodigoFormularioContratoCliente)
    {
        this._dsCodigoFormularioContratoCliente = dsCodigoFormularioContratoCliente;
    } //-- void setDsCodigoFormularioContratoCliente(java.lang.String) 

    /**
     * Sets the value of field 'dsCodigoIndicadorRetornoSeparado'.
     * 
     * @param dsCodigoIndicadorRetornoSeparado the value of field
     * 'dsCodigoIndicadorRetornoSeparado'.
     */
    public void setDsCodigoIndicadorRetornoSeparado(java.lang.String dsCodigoIndicadorRetornoSeparado)
    {
        this._dsCodigoIndicadorRetornoSeparado = dsCodigoIndicadorRetornoSeparado;
    } //-- void setDsCodigoIndicadorRetornoSeparado(java.lang.String) 

    /**
     * Sets the value of field 'dsConsultaDebitoVeiculo'.
     * 
     * @param dsConsultaDebitoVeiculo the value of field
     * 'dsConsultaDebitoVeiculo'.
     */
    public void setDsConsultaDebitoVeiculo(java.lang.String dsConsultaDebitoVeiculo)
    {
        this._dsConsultaDebitoVeiculo = dsConsultaDebitoVeiculo;
    } //-- void setDsConsultaDebitoVeiculo(java.lang.String) 

    /**
     * Sets the value of field 'dsConsultaEndereco'.
     * 
     * @param dsConsultaEndereco the value of field
     * 'dsConsultaEndereco'.
     */
    public void setDsConsultaEndereco(java.lang.String dsConsultaEndereco)
    {
        this._dsConsultaEndereco = dsConsultaEndereco;
    } //-- void setDsConsultaEndereco(java.lang.String) 

    /**
     * Sets the value of field 'dsConsultaSaldoPagamento'.
     * 
     * @param dsConsultaSaldoPagamento the value of field
     * 'dsConsultaSaldoPagamento'.
     */
    public void setDsConsultaSaldoPagamento(java.lang.String dsConsultaSaldoPagamento)
    {
        this._dsConsultaSaldoPagamento = dsConsultaSaldoPagamento;
    } //-- void setDsConsultaSaldoPagamento(java.lang.String) 

    /**
     * Sets the value of field 'dsConsultaSaldoSuperior'.
     * 
     * @param dsConsultaSaldoSuperior the value of field
     * 'dsConsultaSaldoSuperior'.
     */
    public void setDsConsultaSaldoSuperior(java.lang.String dsConsultaSaldoSuperior)
    {
        this._dsConsultaSaldoSuperior = dsConsultaSaldoSuperior;
    } //-- void setDsConsultaSaldoSuperior(java.lang.String) 

    /**
     * Sets the value of field 'dsContagemConsultaSaldo'.
     * 
     * @param dsContagemConsultaSaldo the value of field
     * 'dsContagemConsultaSaldo'.
     */
    public void setDsContagemConsultaSaldo(java.lang.String dsContagemConsultaSaldo)
    {
        this._dsContagemConsultaSaldo = dsContagemConsultaSaldo;
    } //-- void setDsContagemConsultaSaldo(java.lang.String) 

    /**
     * Sets the value of field 'dsCreditoNaoUtilizado'.
     * 
     * @param dsCreditoNaoUtilizado the value of field
     * 'dsCreditoNaoUtilizado'.
     */
    public void setDsCreditoNaoUtilizado(java.lang.String dsCreditoNaoUtilizado)
    {
        this._dsCreditoNaoUtilizado = dsCreditoNaoUtilizado;
    } //-- void setDsCreditoNaoUtilizado(java.lang.String) 

    /**
     * Sets the value of field 'dsCriterioEnquadraRecadastramento'.
     * 
     * @param dsCriterioEnquadraRecadastramento the value of field
     * 'dsCriterioEnquadraRecadastramento'.
     */
    public void setDsCriterioEnquadraRecadastramento(java.lang.String dsCriterioEnquadraRecadastramento)
    {
        this._dsCriterioEnquadraRecadastramento = dsCriterioEnquadraRecadastramento;
    } //-- void setDsCriterioEnquadraRecadastramento(java.lang.String) 

    /**
     * Sets the value of field 'dsCriterioEnquandraBeneficio'.
     * 
     * @param dsCriterioEnquandraBeneficio the value of field
     * 'dsCriterioEnquandraBeneficio'.
     */
    public void setDsCriterioEnquandraBeneficio(java.lang.String dsCriterioEnquandraBeneficio)
    {
        this._dsCriterioEnquandraBeneficio = dsCriterioEnquandraBeneficio;
    } //-- void setDsCriterioEnquandraBeneficio(java.lang.String) 

    /**
     * Sets the value of field 'dsCriterioRastreabilidadeTitulo'.
     * 
     * @param dsCriterioRastreabilidadeTitulo the value of field
     * 'dsCriterioRastreabilidadeTitulo'.
     */
    public void setDsCriterioRastreabilidadeTitulo(java.lang.String dsCriterioRastreabilidadeTitulo)
    {
        this._dsCriterioRastreabilidadeTitulo = dsCriterioRastreabilidadeTitulo;
    } //-- void setDsCriterioRastreabilidadeTitulo(java.lang.String) 

    /**
     * Sets the value of field 'dsDestinoAviso'.
     * 
     * @param dsDestinoAviso the value of field 'dsDestinoAviso'.
     */
    public void setDsDestinoAviso(java.lang.String dsDestinoAviso)
    {
        this._dsDestinoAviso = dsDestinoAviso;
    } //-- void setDsDestinoAviso(java.lang.String) 

    /**
     * Sets the value of field 'dsDestinoComprovante'.
     * 
     * @param dsDestinoComprovante the value of field
     * 'dsDestinoComprovante'.
     */
    public void setDsDestinoComprovante(java.lang.String dsDestinoComprovante)
    {
        this._dsDestinoComprovante = dsDestinoComprovante;
    } //-- void setDsDestinoComprovante(java.lang.String) 

    /**
     * Sets the value of field
     * 'dsDestinoFormularioRecadastramento'.
     * 
     * @param dsDestinoFormularioRecadastramento the value of field
     * 'dsDestinoFormularioRecadastramento'.
     */
    public void setDsDestinoFormularioRecadastramento(java.lang.String dsDestinoFormularioRecadastramento)
    {
        this._dsDestinoFormularioRecadastramento = dsDestinoFormularioRecadastramento;
    } //-- void setDsDestinoFormularioRecadastramento(java.lang.String) 

    /**
     * Sets the value of field 'dsDisponibilizacaoContaCredito'.
     * 
     * @param dsDisponibilizacaoContaCredito the value of field
     * 'dsDisponibilizacaoContaCredito'.
     */
    public void setDsDisponibilizacaoContaCredito(java.lang.String dsDisponibilizacaoContaCredito)
    {
        this._dsDisponibilizacaoContaCredito = dsDisponibilizacaoContaCredito;
    } //-- void setDsDisponibilizacaoContaCredito(java.lang.String) 

    /**
     * Sets the value of field 'dsDisponibilizacaoDiversoCriterio'.
     * 
     * @param dsDisponibilizacaoDiversoCriterio the value of field
     * 'dsDisponibilizacaoDiversoCriterio'.
     */
    public void setDsDisponibilizacaoDiversoCriterio(java.lang.String dsDisponibilizacaoDiversoCriterio)
    {
        this._dsDisponibilizacaoDiversoCriterio = dsDisponibilizacaoDiversoCriterio;
    } //-- void setDsDisponibilizacaoDiversoCriterio(java.lang.String) 

    /**
     * Sets the value of field 'dsDisponibilizacaoDiversoNao'.
     * 
     * @param dsDisponibilizacaoDiversoNao the value of field
     * 'dsDisponibilizacaoDiversoNao'.
     */
    public void setDsDisponibilizacaoDiversoNao(java.lang.String dsDisponibilizacaoDiversoNao)
    {
        this._dsDisponibilizacaoDiversoNao = dsDisponibilizacaoDiversoNao;
    } //-- void setDsDisponibilizacaoDiversoNao(java.lang.String) 

    /**
     * Sets the value of field 'dsDisponibilizacaoSalarioCriterio'.
     * 
     * @param dsDisponibilizacaoSalarioCriterio the value of field
     * 'dsDisponibilizacaoSalarioCriterio'.
     */
    public void setDsDisponibilizacaoSalarioCriterio(java.lang.String dsDisponibilizacaoSalarioCriterio)
    {
        this._dsDisponibilizacaoSalarioCriterio = dsDisponibilizacaoSalarioCriterio;
    } //-- void setDsDisponibilizacaoSalarioCriterio(java.lang.String) 

    /**
     * Sets the value of field 'dsDisponibilizacaoSalarioNao'.
     * 
     * @param dsDisponibilizacaoSalarioNao the value of field
     * 'dsDisponibilizacaoSalarioNao'.
     */
    public void setDsDisponibilizacaoSalarioNao(java.lang.String dsDisponibilizacaoSalarioNao)
    {
        this._dsDisponibilizacaoSalarioNao = dsDisponibilizacaoSalarioNao;
    } //-- void setDsDisponibilizacaoSalarioNao(java.lang.String) 

    /**
     * Sets the value of field 'dsEnvelopeAberto'.
     * 
     * @param dsEnvelopeAberto the value of field 'dsEnvelopeAberto'
     */
    public void setDsEnvelopeAberto(java.lang.String dsEnvelopeAberto)
    {
        this._dsEnvelopeAberto = dsEnvelopeAberto;
    } //-- void setDsEnvelopeAberto(java.lang.String) 

    /**
     * Sets the value of field 'dsFavorecidoConsultaPagamento'.
     * 
     * @param dsFavorecidoConsultaPagamento the value of field
     * 'dsFavorecidoConsultaPagamento'.
     */
    public void setDsFavorecidoConsultaPagamento(java.lang.String dsFavorecidoConsultaPagamento)
    {
        this._dsFavorecidoConsultaPagamento = dsFavorecidoConsultaPagamento;
    } //-- void setDsFavorecidoConsultaPagamento(java.lang.String) 

    /**
     * Sets the value of field 'dsFormaAutorizacaoPagamento'.
     * 
     * @param dsFormaAutorizacaoPagamento the value of field
     * 'dsFormaAutorizacaoPagamento'.
     */
    public void setDsFormaAutorizacaoPagamento(java.lang.String dsFormaAutorizacaoPagamento)
    {
        this._dsFormaAutorizacaoPagamento = dsFormaAutorizacaoPagamento;
    } //-- void setDsFormaAutorizacaoPagamento(java.lang.String) 

    /**
     * Sets the value of field 'dsFormaEnvioPagamento'.
     * 
     * @param dsFormaEnvioPagamento the value of field
     * 'dsFormaEnvioPagamento'.
     */
    public void setDsFormaEnvioPagamento(java.lang.String dsFormaEnvioPagamento)
    {
        this._dsFormaEnvioPagamento = dsFormaEnvioPagamento;
    } //-- void setDsFormaEnvioPagamento(java.lang.String) 

    /**
     * Sets the value of field 'dsFormaExpiracaoCredito'.
     * 
     * @param dsFormaExpiracaoCredito the value of field
     * 'dsFormaExpiracaoCredito'.
     */
    public void setDsFormaExpiracaoCredito(java.lang.String dsFormaExpiracaoCredito)
    {
        this._dsFormaExpiracaoCredito = dsFormaExpiracaoCredito;
    } //-- void setDsFormaExpiracaoCredito(java.lang.String) 

    /**
     * Sets the value of field 'dsFormaManutencao'.
     * 
     * @param dsFormaManutencao the value of field
     * 'dsFormaManutencao'.
     */
    public void setDsFormaManutencao(java.lang.String dsFormaManutencao)
    {
        this._dsFormaManutencao = dsFormaManutencao;
    } //-- void setDsFormaManutencao(java.lang.String) 

    /**
     * Sets the value of field 'dsFrasePrecadastrada'.
     * 
     * @param dsFrasePrecadastrada the value of field
     * 'dsFrasePrecadastrada'.
     */
    public void setDsFrasePrecadastrada(java.lang.String dsFrasePrecadastrada)
    {
        this._dsFrasePrecadastrada = dsFrasePrecadastrada;
    } //-- void setDsFrasePrecadastrada(java.lang.String) 

    /**
     * Sets the value of field 'dsIndLancamentoPersonalizado'.
     * 
     * @param dsIndLancamentoPersonalizado the value of field
     * 'dsIndLancamentoPersonalizado'.
     */
    public void setDsIndLancamentoPersonalizado(java.lang.String dsIndLancamentoPersonalizado)
    {
        this._dsIndLancamentoPersonalizado = dsIndLancamentoPersonalizado;
    } //-- void setDsIndLancamentoPersonalizado(java.lang.String) 

    /**
     * Sets the value of field 'dsIndicadorAdesaoSacador'.
     * 
     * @param dsIndicadorAdesaoSacador the value of field
     * 'dsIndicadorAdesaoSacador'.
     */
    public void setDsIndicadorAdesaoSacador(java.lang.String dsIndicadorAdesaoSacador)
    {
        this._dsIndicadorAdesaoSacador = dsIndicadorAdesaoSacador;
    } //-- void setDsIndicadorAdesaoSacador(java.lang.String) 

    /**
     * Sets the value of field 'dsIndicadorAgendamentoTitulo'.
     * 
     * @param dsIndicadorAgendamentoTitulo the value of field
     * 'dsIndicadorAgendamentoTitulo'.
     */
    public void setDsIndicadorAgendamentoTitulo(java.lang.String dsIndicadorAgendamentoTitulo)
    {
        this._dsIndicadorAgendamentoTitulo = dsIndicadorAgendamentoTitulo;
    } //-- void setDsIndicadorAgendamentoTitulo(java.lang.String) 

    /**
     * Sets the value of field 'dsIndicadorAutorizacaoCliente'.
     * 
     * @param dsIndicadorAutorizacaoCliente the value of field
     * 'dsIndicadorAutorizacaoCliente'.
     */
    public void setDsIndicadorAutorizacaoCliente(java.lang.String dsIndicadorAutorizacaoCliente)
    {
        this._dsIndicadorAutorizacaoCliente = dsIndicadorAutorizacaoCliente;
    } //-- void setDsIndicadorAutorizacaoCliente(java.lang.String) 

    /**
     * Sets the value of field 'dsIndicadorAutorizacaoComplemento'.
     * 
     * @param dsIndicadorAutorizacaoComplemento the value of field
     * 'dsIndicadorAutorizacaoComplemento'.
     */
    public void setDsIndicadorAutorizacaoComplemento(java.lang.String dsIndicadorAutorizacaoComplemento)
    {
        this._dsIndicadorAutorizacaoComplemento = dsIndicadorAutorizacaoComplemento;
    } //-- void setDsIndicadorAutorizacaoComplemento(java.lang.String) 

    /**
     * Sets the value of field 'dsIndicadorBancoPostal'.
     * 
     * @param dsIndicadorBancoPostal the value of field
     * 'dsIndicadorBancoPostal'.
     */
    public void setDsIndicadorBancoPostal(java.lang.String dsIndicadorBancoPostal)
    {
        this._dsIndicadorBancoPostal = dsIndicadorBancoPostal;
    } //-- void setDsIndicadorBancoPostal(java.lang.String) 

    /**
     * Sets the value of field 'dsIndicadorCadastroProcurador'.
     * 
     * @param dsIndicadorCadastroProcurador the value of field
     * 'dsIndicadorCadastroProcurador'.
     */
    public void setDsIndicadorCadastroProcurador(java.lang.String dsIndicadorCadastroProcurador)
    {
        this._dsIndicadorCadastroProcurador = dsIndicadorCadastroProcurador;
    } //-- void setDsIndicadorCadastroProcurador(java.lang.String) 

    /**
     * Sets the value of field 'dsIndicadorCadastroorganizacao'.
     * 
     * @param dsIndicadorCadastroorganizacao the value of field
     * 'dsIndicadorCadastroorganizacao'.
     */
    public void setDsIndicadorCadastroorganizacao(java.lang.String dsIndicadorCadastroorganizacao)
    {
        this._dsIndicadorCadastroorganizacao = dsIndicadorCadastroorganizacao;
    } //-- void setDsIndicadorCadastroorganizacao(java.lang.String) 

    /**
     * Sets the value of field 'dsIndicadorCartaoSalario'.
     * 
     * @param dsIndicadorCartaoSalario the value of field
     * 'dsIndicadorCartaoSalario'.
     */
    public void setDsIndicadorCartaoSalario(java.lang.String dsIndicadorCartaoSalario)
    {
        this._dsIndicadorCartaoSalario = dsIndicadorCartaoSalario;
    } //-- void setDsIndicadorCartaoSalario(java.lang.String) 

    /**
     * Sets the value of field 'dsIndicadorEconomicoReajuste'.
     * 
     * @param dsIndicadorEconomicoReajuste the value of field
     * 'dsIndicadorEconomicoReajuste'.
     */
    public void setDsIndicadorEconomicoReajuste(java.lang.String dsIndicadorEconomicoReajuste)
    {
        this._dsIndicadorEconomicoReajuste = dsIndicadorEconomicoReajuste;
    } //-- void setDsIndicadorEconomicoReajuste(java.lang.String) 

    /**
     * Sets the value of field 'dsIndicadorEmissaoAviso'.
     * 
     * @param dsIndicadorEmissaoAviso the value of field
     * 'dsIndicadorEmissaoAviso'.
     */
    public void setDsIndicadorEmissaoAviso(java.lang.String dsIndicadorEmissaoAviso)
    {
        this._dsIndicadorEmissaoAviso = dsIndicadorEmissaoAviso;
    } //-- void setDsIndicadorEmissaoAviso(java.lang.String) 

    /**
     * Sets the value of field 'dsIndicadorListaDebito'.
     * 
     * @param dsIndicadorListaDebito the value of field
     * 'dsIndicadorListaDebito'.
     */
    public void setDsIndicadorListaDebito(java.lang.String dsIndicadorListaDebito)
    {
        this._dsIndicadorListaDebito = dsIndicadorListaDebito;
    } //-- void setDsIndicadorListaDebito(java.lang.String) 

    /**
     * Sets the value of field 'dsIndicadorMensagemPersonalizada'.
     * 
     * @param dsIndicadorMensagemPersonalizada the value of field
     * 'dsIndicadorMensagemPersonalizada'.
     */
    public void setDsIndicadorMensagemPersonalizada(java.lang.String dsIndicadorMensagemPersonalizada)
    {
        this._dsIndicadorMensagemPersonalizada = dsIndicadorMensagemPersonalizada;
    } //-- void setDsIndicadorMensagemPersonalizada(java.lang.String) 

    /**
     * Sets the value of field 'dsIndicadorRetornoInternet'.
     * 
     * @param dsIndicadorRetornoInternet the value of field
     * 'dsIndicadorRetornoInternet'.
     */
    public void setDsIndicadorRetornoInternet(java.lang.String dsIndicadorRetornoInternet)
    {
        this._dsIndicadorRetornoInternet = dsIndicadorRetornoInternet;
    } //-- void setDsIndicadorRetornoInternet(java.lang.String) 

    /**
     * Sets the value of field 'dsLancamentoFuturoCredito'.
     * 
     * @param dsLancamentoFuturoCredito the value of field
     * 'dsLancamentoFuturoCredito'.
     */
    public void setDsLancamentoFuturoCredito(java.lang.String dsLancamentoFuturoCredito)
    {
        this._dsLancamentoFuturoCredito = dsLancamentoFuturoCredito;
    } //-- void setDsLancamentoFuturoCredito(java.lang.String) 

    /**
     * Sets the value of field 'dsLancamentoFuturoDebito'.
     * 
     * @param dsLancamentoFuturoDebito the value of field
     * 'dsLancamentoFuturoDebito'.
     */
    public void setDsLancamentoFuturoDebito(java.lang.String dsLancamentoFuturoDebito)
    {
        this._dsLancamentoFuturoDebito = dsLancamentoFuturoDebito;
    } //-- void setDsLancamentoFuturoDebito(java.lang.String) 

    /**
     * Sets the value of field 'dsLiberacaoLoteProcessado'.
     * 
     * @param dsLiberacaoLoteProcessado the value of field
     * 'dsLiberacaoLoteProcessado'.
     */
    public void setDsLiberacaoLoteProcessado(java.lang.String dsLiberacaoLoteProcessado)
    {
        this._dsLiberacaoLoteProcessado = dsLiberacaoLoteProcessado;
    } //-- void setDsLiberacaoLoteProcessado(java.lang.String) 

    /**
     * Sets the value of field 'dsLocalEmissao'.
     * 
     * @param dsLocalEmissao the value of field 'dsLocalEmissao'.
     */
    public void setDsLocalEmissao(java.lang.String dsLocalEmissao)
    {
        this._dsLocalEmissao = dsLocalEmissao;
    } //-- void setDsLocalEmissao(java.lang.String) 

    /**
     * Sets the value of field 'dsManutencaoBaseRecadastramento'.
     * 
     * @param dsManutencaoBaseRecadastramento the value of field
     * 'dsManutencaoBaseRecadastramento'.
     */
    public void setDsManutencaoBaseRecadastramento(java.lang.String dsManutencaoBaseRecadastramento)
    {
        this._dsManutencaoBaseRecadastramento = dsManutencaoBaseRecadastramento;
    } //-- void setDsManutencaoBaseRecadastramento(java.lang.String) 

    /**
     * Sets the value of field 'dsMeioPagamentoCredito'.
     * 
     * @param dsMeioPagamentoCredito the value of field
     * 'dsMeioPagamentoCredito'.
     */
    public void setDsMeioPagamentoCredito(java.lang.String dsMeioPagamentoCredito)
    {
        this._dsMeioPagamentoCredito = dsMeioPagamentoCredito;
    } //-- void setDsMeioPagamentoCredito(java.lang.String) 

    /**
     * Sets the value of field 'dsMensagemRecadastramentoMidia'.
     * 
     * @param dsMensagemRecadastramentoMidia the value of field
     * 'dsMensagemRecadastramentoMidia'.
     */
    public void setDsMensagemRecadastramentoMidia(java.lang.String dsMensagemRecadastramentoMidia)
    {
        this._dsMensagemRecadastramentoMidia = dsMensagemRecadastramentoMidia;
    } //-- void setDsMensagemRecadastramentoMidia(java.lang.String) 

    /**
     * Sets the value of field 'dsMidiaDisponivel'.
     * 
     * @param dsMidiaDisponivel the value of field
     * 'dsMidiaDisponivel'.
     */
    public void setDsMidiaDisponivel(java.lang.String dsMidiaDisponivel)
    {
        this._dsMidiaDisponivel = dsMidiaDisponivel;
    } //-- void setDsMidiaDisponivel(java.lang.String) 

    /**
     * Sets the value of field 'dsMidiaMensagemRecadastramento'.
     * 
     * @param dsMidiaMensagemRecadastramento the value of field
     * 'dsMidiaMensagemRecadastramento'.
     */
    public void setDsMidiaMensagemRecadastramento(java.lang.String dsMidiaMensagemRecadastramento)
    {
        this._dsMidiaMensagemRecadastramento = dsMidiaMensagemRecadastramento;
    } //-- void setDsMidiaMensagemRecadastramento(java.lang.String) 

    /**
     * Sets the value of field 'dsMomentoAvisoRecadastramento'.
     * 
     * @param dsMomentoAvisoRecadastramento the value of field
     * 'dsMomentoAvisoRecadastramento'.
     */
    public void setDsMomentoAvisoRecadastramento(java.lang.String dsMomentoAvisoRecadastramento)
    {
        this._dsMomentoAvisoRecadastramento = dsMomentoAvisoRecadastramento;
    } //-- void setDsMomentoAvisoRecadastramento(java.lang.String) 

    /**
     * Sets the value of field 'dsMomentoCreditoEfetivacao'.
     * 
     * @param dsMomentoCreditoEfetivacao the value of field
     * 'dsMomentoCreditoEfetivacao'.
     */
    public void setDsMomentoCreditoEfetivacao(java.lang.String dsMomentoCreditoEfetivacao)
    {
        this._dsMomentoCreditoEfetivacao = dsMomentoCreditoEfetivacao;
    } //-- void setDsMomentoCreditoEfetivacao(java.lang.String) 

    /**
     * Sets the value of field 'dsMomentoDebitoPagamento'.
     * 
     * @param dsMomentoDebitoPagamento the value of field
     * 'dsMomentoDebitoPagamento'.
     */
    public void setDsMomentoDebitoPagamento(java.lang.String dsMomentoDebitoPagamento)
    {
        this._dsMomentoDebitoPagamento = dsMomentoDebitoPagamento;
    } //-- void setDsMomentoDebitoPagamento(java.lang.String) 

    /**
     * Sets the value of field
     * 'dsMomentoFormularioRecadastramento'.
     * 
     * @param dsMomentoFormularioRecadastramento the value of field
     * 'dsMomentoFormularioRecadastramento'.
     */
    public void setDsMomentoFormularioRecadastramento(java.lang.String dsMomentoFormularioRecadastramento)
    {
        this._dsMomentoFormularioRecadastramento = dsMomentoFormularioRecadastramento;
    } //-- void setDsMomentoFormularioRecadastramento(java.lang.String) 

    /**
     * Sets the value of field 'dsMomentoProcessamentoPagamento'.
     * 
     * @param dsMomentoProcessamentoPagamento the value of field
     * 'dsMomentoProcessamentoPagamento'.
     */
    public void setDsMomentoProcessamentoPagamento(java.lang.String dsMomentoProcessamentoPagamento)
    {
        this._dsMomentoProcessamentoPagamento = dsMomentoProcessamentoPagamento;
    } //-- void setDsMomentoProcessamentoPagamento(java.lang.String) 

    /**
     * Sets the value of field 'dsNaturezaOperacaoPagamento'.
     * 
     * @param dsNaturezaOperacaoPagamento the value of field
     * 'dsNaturezaOperacaoPagamento'.
     */
    public void setDsNaturezaOperacaoPagamento(java.lang.String dsNaturezaOperacaoPagamento)
    {
        this._dsNaturezaOperacaoPagamento = dsNaturezaOperacaoPagamento;
    } //-- void setDsNaturezaOperacaoPagamento(java.lang.String) 

    /**
     * Sets the value of field 'dsPagamentoNaoUtil'.
     * 
     * @param dsPagamentoNaoUtil the value of field
     * 'dsPagamentoNaoUtil'.
     */
    public void setDsPagamentoNaoUtil(java.lang.String dsPagamentoNaoUtil)
    {
        this._dsPagamentoNaoUtil = dsPagamentoNaoUtil;
    } //-- void setDsPagamentoNaoUtil(java.lang.String) 

    /**
     * Sets the value of field 'dsPeriodicidadeAviso'.
     * 
     * @param dsPeriodicidadeAviso the value of field
     * 'dsPeriodicidadeAviso'.
     */
    public void setDsPeriodicidadeAviso(java.lang.String dsPeriodicidadeAviso)
    {
        this._dsPeriodicidadeAviso = dsPeriodicidadeAviso;
    } //-- void setDsPeriodicidadeAviso(java.lang.String) 

    /**
     * Sets the value of field 'dsPeriodicidadeCobrancaTarifa'.
     * 
     * @param dsPeriodicidadeCobrancaTarifa the value of field
     * 'dsPeriodicidadeCobrancaTarifa'.
     */
    public void setDsPeriodicidadeCobrancaTarifa(java.lang.String dsPeriodicidadeCobrancaTarifa)
    {
        this._dsPeriodicidadeCobrancaTarifa = dsPeriodicidadeCobrancaTarifa;
    } //-- void setDsPeriodicidadeCobrancaTarifa(java.lang.String) 

    /**
     * Sets the value of field 'dsPeriodicidadeComprovante'.
     * 
     * @param dsPeriodicidadeComprovante the value of field
     * 'dsPeriodicidadeComprovante'.
     */
    public void setDsPeriodicidadeComprovante(java.lang.String dsPeriodicidadeComprovante)
    {
        this._dsPeriodicidadeComprovante = dsPeriodicidadeComprovante;
    } //-- void setDsPeriodicidadeComprovante(java.lang.String) 

    /**
     * Sets the value of field 'dsPeriodicidadeConsultaVeiculo'.
     * 
     * @param dsPeriodicidadeConsultaVeiculo the value of field
     * 'dsPeriodicidadeConsultaVeiculo'.
     */
    public void setDsPeriodicidadeConsultaVeiculo(java.lang.String dsPeriodicidadeConsultaVeiculo)
    {
        this._dsPeriodicidadeConsultaVeiculo = dsPeriodicidadeConsultaVeiculo;
    } //-- void setDsPeriodicidadeConsultaVeiculo(java.lang.String) 

    /**
     * Sets the value of field 'dsPeriodicidadeEnvioRemessa'.
     * 
     * @param dsPeriodicidadeEnvioRemessa the value of field
     * 'dsPeriodicidadeEnvioRemessa'.
     */
    public void setDsPeriodicidadeEnvioRemessa(java.lang.String dsPeriodicidadeEnvioRemessa)
    {
        this._dsPeriodicidadeEnvioRemessa = dsPeriodicidadeEnvioRemessa;
    } //-- void setDsPeriodicidadeEnvioRemessa(java.lang.String) 

    /**
     * Sets the value of field 'dsPeriodicidadeManutencaoProcd'.
     * 
     * @param dsPeriodicidadeManutencaoProcd the value of field
     * 'dsPeriodicidadeManutencaoProcd'.
     */
    public void setDsPeriodicidadeManutencaoProcd(java.lang.String dsPeriodicidadeManutencaoProcd)
    {
        this._dsPeriodicidadeManutencaoProcd = dsPeriodicidadeManutencaoProcd;
    } //-- void setDsPeriodicidadeManutencaoProcd(java.lang.String) 

    /**
     * Sets the value of field 'dsPermissaoDebitoOnline'.
     * 
     * @param dsPermissaoDebitoOnline the value of field
     * 'dsPermissaoDebitoOnline'.
     */
    public void setDsPermissaoDebitoOnline(java.lang.String dsPermissaoDebitoOnline)
    {
        this._dsPermissaoDebitoOnline = dsPermissaoDebitoOnline;
    } //-- void setDsPermissaoDebitoOnline(java.lang.String) 

    /**
     * Sets the value of field 'dsPrincipalEnquaRecadastramento'.
     * 
     * @param dsPrincipalEnquaRecadastramento the value of field
     * 'dsPrincipalEnquaRecadastramento'.
     */
    public void setDsPrincipalEnquaRecadastramento(java.lang.String dsPrincipalEnquaRecadastramento)
    {
        this._dsPrincipalEnquaRecadastramento = dsPrincipalEnquaRecadastramento;
    } //-- void setDsPrincipalEnquaRecadastramento(java.lang.String) 

    /**
     * Sets the value of field 'dsPrioridadeEfetivacaoPagamento'.
     * 
     * @param dsPrioridadeEfetivacaoPagamento the value of field
     * 'dsPrioridadeEfetivacaoPagamento'.
     */
    public void setDsPrioridadeEfetivacaoPagamento(java.lang.String dsPrioridadeEfetivacaoPagamento)
    {
        this._dsPrioridadeEfetivacaoPagamento = dsPrioridadeEfetivacaoPagamento;
    } //-- void setDsPrioridadeEfetivacaoPagamento(java.lang.String) 

    /**
     * Sets the value of field 'dsRastreabilidadeNotaFiscal'.
     * 
     * @param dsRastreabilidadeNotaFiscal the value of field
     * 'dsRastreabilidadeNotaFiscal'.
     */
    public void setDsRastreabilidadeNotaFiscal(java.lang.String dsRastreabilidadeNotaFiscal)
    {
        this._dsRastreabilidadeNotaFiscal = dsRastreabilidadeNotaFiscal;
    } //-- void setDsRastreabilidadeNotaFiscal(java.lang.String) 

    /**
     * Sets the value of field 'dsRastreabilidadeTituloTerceiro'.
     * 
     * @param dsRastreabilidadeTituloTerceiro the value of field
     * 'dsRastreabilidadeTituloTerceiro'.
     */
    public void setDsRastreabilidadeTituloTerceiro(java.lang.String dsRastreabilidadeTituloTerceiro)
    {
        this._dsRastreabilidadeTituloTerceiro = dsRastreabilidadeTituloTerceiro;
    } //-- void setDsRastreabilidadeTituloTerceiro(java.lang.String) 

    /**
     * Sets the value of field 'dsRejeicaoAgendamentoLote'.
     * 
     * @param dsRejeicaoAgendamentoLote the value of field
     * 'dsRejeicaoAgendamentoLote'.
     */
    public void setDsRejeicaoAgendamentoLote(java.lang.String dsRejeicaoAgendamentoLote)
    {
        this._dsRejeicaoAgendamentoLote = dsRejeicaoAgendamentoLote;
    } //-- void setDsRejeicaoAgendamentoLote(java.lang.String) 

    /**
     * Sets the value of field 'dsRejeicaoEfetivacaoLote'.
     * 
     * @param dsRejeicaoEfetivacaoLote the value of field
     * 'dsRejeicaoEfetivacaoLote'.
     */
    public void setDsRejeicaoEfetivacaoLote(java.lang.String dsRejeicaoEfetivacaoLote)
    {
        this._dsRejeicaoEfetivacaoLote = dsRejeicaoEfetivacaoLote;
    } //-- void setDsRejeicaoEfetivacaoLote(java.lang.String) 

    /**
     * Sets the value of field 'dsRejeicaoLote'.
     * 
     * @param dsRejeicaoLote the value of field 'dsRejeicaoLote'.
     */
    public void setDsRejeicaoLote(java.lang.String dsRejeicaoLote)
    {
        this._dsRejeicaoLote = dsRejeicaoLote;
    } //-- void setDsRejeicaoLote(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoCargaRecadastramento'.
     * 
     * @param dsTipoCargaRecadastramento the value of field
     * 'dsTipoCargaRecadastramento'.
     */
    public void setDsTipoCargaRecadastramento(java.lang.String dsTipoCargaRecadastramento)
    {
        this._dsTipoCargaRecadastramento = dsTipoCargaRecadastramento;
    } //-- void setDsTipoCargaRecadastramento(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoCartaoSalario'.
     * 
     * @param dsTipoCartaoSalario the value of field
     * 'dsTipoCartaoSalario'.
     */
    public void setDsTipoCartaoSalario(java.lang.String dsTipoCartaoSalario)
    {
        this._dsTipoCartaoSalario = dsTipoCartaoSalario;
    } //-- void setDsTipoCartaoSalario(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoConsistenciaFavorecido'.
     * 
     * @param dsTipoConsistenciaFavorecido the value of field
     * 'dsTipoConsistenciaFavorecido'.
     */
    public void setDsTipoConsistenciaFavorecido(java.lang.String dsTipoConsistenciaFavorecido)
    {
        this._dsTipoConsistenciaFavorecido = dsTipoConsistenciaFavorecido;
    } //-- void setDsTipoConsistenciaFavorecido(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoConsistenciaLista'.
     * 
     * @param dsTipoConsistenciaLista the value of field
     * 'dsTipoConsistenciaLista'.
     */
    public void setDsTipoConsistenciaLista(java.lang.String dsTipoConsistenciaLista)
    {
        this._dsTipoConsistenciaLista = dsTipoConsistenciaLista;
    } //-- void setDsTipoConsistenciaLista(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoConsolidacaoComprovante'.
     * 
     * @param dsTipoConsolidacaoComprovante the value of field
     * 'dsTipoConsolidacaoComprovante'.
     */
    public void setDsTipoConsolidacaoComprovante(java.lang.String dsTipoConsolidacaoComprovante)
    {
        this._dsTipoConsolidacaoComprovante = dsTipoConsolidacaoComprovante;
    } //-- void setDsTipoConsolidacaoComprovante(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoDataFloating'.
     * 
     * @param dsTipoDataFloating the value of field
     * 'dsTipoDataFloating'.
     */
    public void setDsTipoDataFloating(java.lang.String dsTipoDataFloating)
    {
        this._dsTipoDataFloating = dsTipoDataFloating;
    } //-- void setDsTipoDataFloating(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoDivergenciaVeiculo'.
     * 
     * @param dsTipoDivergenciaVeiculo the value of field
     * 'dsTipoDivergenciaVeiculo'.
     */
    public void setDsTipoDivergenciaVeiculo(java.lang.String dsTipoDivergenciaVeiculo)
    {
        this._dsTipoDivergenciaVeiculo = dsTipoDivergenciaVeiculo;
    } //-- void setDsTipoDivergenciaVeiculo(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoFormacaoLista'.
     * 
     * @param dsTipoFormacaoLista the value of field
     * 'dsTipoFormacaoLista'.
     */
    public void setDsTipoFormacaoLista(java.lang.String dsTipoFormacaoLista)
    {
        this._dsTipoFormacaoLista = dsTipoFormacaoLista;
    } //-- void setDsTipoFormacaoLista(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoIdentificacaoBeneficio'.
     * 
     * @param dsTipoIdentificacaoBeneficio the value of field
     * 'dsTipoIdentificacaoBeneficio'.
     */
    public void setDsTipoIdentificacaoBeneficio(java.lang.String dsTipoIdentificacaoBeneficio)
    {
        this._dsTipoIdentificacaoBeneficio = dsTipoIdentificacaoBeneficio;
    } //-- void setDsTipoIdentificacaoBeneficio(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoIscricaoFavorecido'.
     * 
     * @param dsTipoIscricaoFavorecido the value of field
     * 'dsTipoIscricaoFavorecido'.
     */
    public void setDsTipoIscricaoFavorecido(java.lang.String dsTipoIscricaoFavorecido)
    {
        this._dsTipoIscricaoFavorecido = dsTipoIscricaoFavorecido;
    } //-- void setDsTipoIscricaoFavorecido(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoLayoutArquivo'.
     * 
     * @param dsTipoLayoutArquivo the value of field
     * 'dsTipoLayoutArquivo'.
     */
    public void setDsTipoLayoutArquivo(java.lang.String dsTipoLayoutArquivo)
    {
        this._dsTipoLayoutArquivo = dsTipoLayoutArquivo;
    } //-- void setDsTipoLayoutArquivo(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoReajusteTarifa'.
     * 
     * @param dsTipoReajusteTarifa the value of field
     * 'dsTipoReajusteTarifa'.
     */
    public void setDsTipoReajusteTarifa(java.lang.String dsTipoReajusteTarifa)
    {
        this._dsTipoReajusteTarifa = dsTipoReajusteTarifa;
    } //-- void setDsTipoReajusteTarifa(java.lang.String) 

    /**
     * Sets the value of field 'dsTratamentoContaTransferida'.
     * 
     * @param dsTratamentoContaTransferida the value of field
     * 'dsTratamentoContaTransferida'.
     */
    public void setDsTratamentoContaTransferida(java.lang.String dsTratamentoContaTransferida)
    {
        this._dsTratamentoContaTransferida = dsTratamentoContaTransferida;
    } //-- void setDsTratamentoContaTransferida(java.lang.String) 

    /**
     * Sets the value of field 'dsUtilizacaoFavorecidoControle'.
     * 
     * @param dsUtilizacaoFavorecidoControle the value of field
     * 'dsUtilizacaoFavorecidoControle'.
     */
    public void setDsUtilizacaoFavorecidoControle(java.lang.String dsUtilizacaoFavorecidoControle)
    {
        this._dsUtilizacaoFavorecidoControle = dsUtilizacaoFavorecidoControle;
    } //-- void setDsUtilizacaoFavorecidoControle(java.lang.String) 

    /**
     * Sets the value of field 'dsValidacaoNomeFavorecido'.
     * 
     * @param dsValidacaoNomeFavorecido the value of field
     * 'dsValidacaoNomeFavorecido'.
     */
    public void setDsValidacaoNomeFavorecido(java.lang.String dsValidacaoNomeFavorecido)
    {
        this._dsValidacaoNomeFavorecido = dsValidacaoNomeFavorecido;
    } //-- void setDsValidacaoNomeFavorecido(java.lang.String) 

    /**
     * Sets the value of field 'dsindicadorExpiraCredito'.
     * 
     * @param dsindicadorExpiraCredito the value of field
     * 'dsindicadorExpiraCredito'.
     */
    public void setDsindicadorExpiraCredito(java.lang.String dsindicadorExpiraCredito)
    {
        this._dsindicadorExpiraCredito = dsindicadorExpiraCredito;
    } //-- void setDsindicadorExpiraCredito(java.lang.String) 

    /**
     * Sets the value of field 'dsindicadorLancamentoProgramado'.
     * 
     * @param dsindicadorLancamentoProgramado the value of field
     * 'dsindicadorLancamentoProgramado'.
     */
    public void setDsindicadorLancamentoProgramado(java.lang.String dsindicadorLancamentoProgramado)
    {
        this._dsindicadorLancamentoProgramado = dsindicadorLancamentoProgramado;
    } //-- void setDsindicadorLancamentoProgramado(java.lang.String) 

    /**
     * Sets the value of field 'dtEnquaContaSalario'.
     * 
     * @param dtEnquaContaSalario the value of field
     * 'dtEnquaContaSalario'.
     */
    public void setDtEnquaContaSalario(java.lang.String dtEnquaContaSalario)
    {
        this._dtEnquaContaSalario = dtEnquaContaSalario;
    } //-- void setDtEnquaContaSalario(java.lang.String) 

    /**
     * Sets the value of field 'dtFimAcertoRecadastramento'.
     * 
     * @param dtFimAcertoRecadastramento the value of field
     * 'dtFimAcertoRecadastramento'.
     */
    public void setDtFimAcertoRecadastramento(java.lang.String dtFimAcertoRecadastramento)
    {
        this._dtFimAcertoRecadastramento = dtFimAcertoRecadastramento;
    } //-- void setDtFimAcertoRecadastramento(java.lang.String) 

    /**
     * Sets the value of field 'dtFimRecadastramentoBeneficio'.
     * 
     * @param dtFimRecadastramentoBeneficio the value of field
     * 'dtFimRecadastramentoBeneficio'.
     */
    public void setDtFimRecadastramentoBeneficio(java.lang.String dtFimRecadastramentoBeneficio)
    {
        this._dtFimRecadastramentoBeneficio = dtFimRecadastramentoBeneficio;
    } //-- void setDtFimRecadastramentoBeneficio(java.lang.String) 

    /**
     * Sets the value of field 'dtInicioAcertoRecadastramento'.
     * 
     * @param dtInicioAcertoRecadastramento the value of field
     * 'dtInicioAcertoRecadastramento'.
     */
    public void setDtInicioAcertoRecadastramento(java.lang.String dtInicioAcertoRecadastramento)
    {
        this._dtInicioAcertoRecadastramento = dtInicioAcertoRecadastramento;
    } //-- void setDtInicioAcertoRecadastramento(java.lang.String) 

    /**
     * Sets the value of field 'dtInicioBloqueioPplta'.
     * 
     * @param dtInicioBloqueioPplta the value of field
     * 'dtInicioBloqueioPplta'.
     */
    public void setDtInicioBloqueioPplta(java.lang.String dtInicioBloqueioPplta)
    {
        this._dtInicioBloqueioPplta = dtInicioBloqueioPplta;
    } //-- void setDtInicioBloqueioPplta(java.lang.String) 

    /**
     * Sets the value of field 'dtInicioRastreabTitulo'.
     * 
     * @param dtInicioRastreabTitulo the value of field
     * 'dtInicioRastreabTitulo'.
     */
    public void setDtInicioRastreabTitulo(java.lang.String dtInicioRastreabTitulo)
    {
        this._dtInicioRastreabTitulo = dtInicioRastreabTitulo;
    } //-- void setDtInicioRastreabTitulo(java.lang.String) 

    /**
     * Sets the value of field 'dtInicioRecadastramentoBeneficio'.
     * 
     * @param dtInicioRecadastramentoBeneficio the value of field
     * 'dtInicioRecadastramentoBeneficio'.
     */
    public void setDtInicioRecadastramentoBeneficio(java.lang.String dtInicioRecadastramentoBeneficio)
    {
        this._dtInicioRecadastramentoBeneficio = dtInicioRecadastramentoBeneficio;
    } //-- void setDtInicioRecadastramentoBeneficio(java.lang.String) 

    /**
     * Sets the value of field 'dtLimiteVinculoCarga'.
     * 
     * @param dtLimiteVinculoCarga the value of field
     * 'dtLimiteVinculoCarga'.
     */
    public void setDtLimiteVinculoCarga(java.lang.String dtLimiteVinculoCarga)
    {
        this._dtLimiteVinculoCarga = dtLimiteVinculoCarga;
    } //-- void setDtLimiteVinculoCarga(java.lang.String) 

    /**
     * Sets the value of field 'hrManutencaoRegistroAlteracao'.
     * 
     * @param hrManutencaoRegistroAlteracao the value of field
     * 'hrManutencaoRegistroAlteracao'.
     */
    public void setHrManutencaoRegistroAlteracao(java.lang.String hrManutencaoRegistroAlteracao)
    {
        this._hrManutencaoRegistroAlteracao = hrManutencaoRegistroAlteracao;
    } //-- void setHrManutencaoRegistroAlteracao(java.lang.String) 

    /**
     * Sets the value of field 'hrManutencaoRegistroInclusao'.
     * 
     * @param hrManutencaoRegistroInclusao the value of field
     * 'hrManutencaoRegistroInclusao'.
     */
    public void setHrManutencaoRegistroInclusao(java.lang.String hrManutencaoRegistroInclusao)
    {
        this._hrManutencaoRegistroInclusao = hrManutencaoRegistroInclusao;
    } //-- void setHrManutencaoRegistroInclusao(java.lang.String) 

    /**
     * Sets the value of field 'mensagem'.
     * 
     * @param mensagem the value of field 'mensagem'.
     */
    public void setMensagem(java.lang.String mensagem)
    {
        this._mensagem = mensagem;
    } //-- void setMensagem(java.lang.String) 

    /**
     * Sets the value of field 'nrFechamentoApuracaoTarifa'.
     * 
     * @param nrFechamentoApuracaoTarifa the value of field
     * 'nrFechamentoApuracaoTarifa'.
     */
    public void setNrFechamentoApuracaoTarifa(int nrFechamentoApuracaoTarifa)
    {
        this._nrFechamentoApuracaoTarifa = nrFechamentoApuracaoTarifa;
        this._has_nrFechamentoApuracaoTarifa = true;
    } //-- void setNrFechamentoApuracaoTarifa(int) 

    /**
     * Sets the value of field 'nrOperacaoFluxoAlteracao'.
     * 
     * @param nrOperacaoFluxoAlteracao the value of field
     * 'nrOperacaoFluxoAlteracao'.
     */
    public void setNrOperacaoFluxoAlteracao(java.lang.String nrOperacaoFluxoAlteracao)
    {
        this._nrOperacaoFluxoAlteracao = nrOperacaoFluxoAlteracao;
    } //-- void setNrOperacaoFluxoAlteracao(java.lang.String) 

    /**
     * Sets the value of field 'nrOperacaoFluxoInclusao'.
     * 
     * @param nrOperacaoFluxoInclusao the value of field
     * 'nrOperacaoFluxoInclusao'.
     */
    public void setNrOperacaoFluxoInclusao(java.lang.String nrOperacaoFluxoInclusao)
    {
        this._nrOperacaoFluxoInclusao = nrOperacaoFluxoInclusao;
    } //-- void setNrOperacaoFluxoInclusao(java.lang.String) 

    /**
     * Sets the value of field 'percentualIndiceReajusteTarifa'.
     * 
     * @param percentualIndiceReajusteTarifa the value of field
     * 'percentualIndiceReajusteTarifa'.
     */
    public void setPercentualIndiceReajusteTarifa(java.math.BigDecimal percentualIndiceReajusteTarifa)
    {
        this._percentualIndiceReajusteTarifa = percentualIndiceReajusteTarifa;
    } //-- void setPercentualIndiceReajusteTarifa(java.math.BigDecimal) 

    /**
     * Sets the value of field 'percentualMaximoInconsistenteLote'.
     * 
     * @param percentualMaximoInconsistenteLote the value of field
     * 'percentualMaximoInconsistenteLote'.
     */
    public void setPercentualMaximoInconsistenteLote(int percentualMaximoInconsistenteLote)
    {
        this._percentualMaximoInconsistenteLote = percentualMaximoInconsistenteLote;
        this._has_percentualMaximoInconsistenteLote = true;
    } //-- void setPercentualMaximoInconsistenteLote(int) 

    /**
     * Sets the value of field 'periodicidadeTarifaCatalogo'.
     * 
     * @param periodicidadeTarifaCatalogo the value of field
     * 'periodicidadeTarifaCatalogo'.
     */
    public void setPeriodicidadeTarifaCatalogo(java.math.BigDecimal periodicidadeTarifaCatalogo)
    {
        this._periodicidadeTarifaCatalogo = periodicidadeTarifaCatalogo;
    } //-- void setPeriodicidadeTarifaCatalogo(java.math.BigDecimal) 

    /**
     * Sets the value of field 'quantidadeAntecedencia'.
     * 
     * @param quantidadeAntecedencia the value of field
     * 'quantidadeAntecedencia'.
     */
    public void setQuantidadeAntecedencia(int quantidadeAntecedencia)
    {
        this._quantidadeAntecedencia = quantidadeAntecedencia;
        this._has_quantidadeAntecedencia = true;
    } //-- void setQuantidadeAntecedencia(int) 

    /**
     * Sets the value of field
     * 'quantidadeAnteriorVencimentoComprovante'.
     * 
     * @param quantidadeAnteriorVencimentoComprovante the value of
     * field 'quantidadeAnteriorVencimentoComprovante'.
     */
    public void setQuantidadeAnteriorVencimentoComprovante(int quantidadeAnteriorVencimentoComprovante)
    {
        this._quantidadeAnteriorVencimentoComprovante = quantidadeAnteriorVencimentoComprovante;
        this._has_quantidadeAnteriorVencimentoComprovante = true;
    } //-- void setQuantidadeAnteriorVencimentoComprovante(int) 

    /**
     * Sets the value of field 'quantidadeDiaCobrancaTarifa'.
     * 
     * @param quantidadeDiaCobrancaTarifa the value of field
     * 'quantidadeDiaCobrancaTarifa'.
     */
    public void setQuantidadeDiaCobrancaTarifa(int quantidadeDiaCobrancaTarifa)
    {
        this._quantidadeDiaCobrancaTarifa = quantidadeDiaCobrancaTarifa;
        this._has_quantidadeDiaCobrancaTarifa = true;
    } //-- void setQuantidadeDiaCobrancaTarifa(int) 

    /**
     * Sets the value of field 'quantidadeDiaExpiracao'.
     * 
     * @param quantidadeDiaExpiracao the value of field
     * 'quantidadeDiaExpiracao'.
     */
    public void setQuantidadeDiaExpiracao(int quantidadeDiaExpiracao)
    {
        this._quantidadeDiaExpiracao = quantidadeDiaExpiracao;
        this._has_quantidadeDiaExpiracao = true;
    } //-- void setQuantidadeDiaExpiracao(int) 

    /**
     * Sets the value of field 'quantidadeDiaFloatingPagamento'.
     * 
     * @param quantidadeDiaFloatingPagamento the value of field
     * 'quantidadeDiaFloatingPagamento'.
     */
    public void setQuantidadeDiaFloatingPagamento(int quantidadeDiaFloatingPagamento)
    {
        this._quantidadeDiaFloatingPagamento = quantidadeDiaFloatingPagamento;
        this._has_quantidadeDiaFloatingPagamento = true;
    } //-- void setQuantidadeDiaFloatingPagamento(int) 

    /**
     * Sets the value of field
     * 'quantidadeDiaInatividadeFavorecido'.
     * 
     * @param quantidadeDiaInatividadeFavorecido the value of field
     * 'quantidadeDiaInatividadeFavorecido'.
     */
    public void setQuantidadeDiaInatividadeFavorecido(int quantidadeDiaInatividadeFavorecido)
    {
        this._quantidadeDiaInatividadeFavorecido = quantidadeDiaInatividadeFavorecido;
        this._has_quantidadeDiaInatividadeFavorecido = true;
    } //-- void setQuantidadeDiaInatividadeFavorecido(int) 

    /**
     * Sets the value of field 'quantidadeDiaRepiqueConsulta'.
     * 
     * @param quantidadeDiaRepiqueConsulta the value of field
     * 'quantidadeDiaRepiqueConsulta'.
     */
    public void setQuantidadeDiaRepiqueConsulta(int quantidadeDiaRepiqueConsulta)
    {
        this._quantidadeDiaRepiqueConsulta = quantidadeDiaRepiqueConsulta;
        this._has_quantidadeDiaRepiqueConsulta = true;
    } //-- void setQuantidadeDiaRepiqueConsulta(int) 

    /**
     * Sets the value of field
     * 'quantidadeEtapaRecadastramentoBeneficio'.
     * 
     * @param quantidadeEtapaRecadastramentoBeneficio the value of
     * field 'quantidadeEtapaRecadastramentoBeneficio'.
     */
    public void setQuantidadeEtapaRecadastramentoBeneficio(int quantidadeEtapaRecadastramentoBeneficio)
    {
        this._quantidadeEtapaRecadastramentoBeneficio = quantidadeEtapaRecadastramentoBeneficio;
        this._has_quantidadeEtapaRecadastramentoBeneficio = true;
    } //-- void setQuantidadeEtapaRecadastramentoBeneficio(int) 

    /**
     * Sets the value of field
     * 'quantidadeFaseRecadastramentoBeneficio'.
     * 
     * @param quantidadeFaseRecadastramentoBeneficio the value of
     * field 'quantidadeFaseRecadastramentoBeneficio'.
     */
    public void setQuantidadeFaseRecadastramentoBeneficio(int quantidadeFaseRecadastramentoBeneficio)
    {
        this._quantidadeFaseRecadastramentoBeneficio = quantidadeFaseRecadastramentoBeneficio;
        this._has_quantidadeFaseRecadastramentoBeneficio = true;
    } //-- void setQuantidadeFaseRecadastramentoBeneficio(int) 

    /**
     * Sets the value of field 'quantidadeLimiteLinha'.
     * 
     * @param quantidadeLimiteLinha the value of field
     * 'quantidadeLimiteLinha'.
     */
    public void setQuantidadeLimiteLinha(int quantidadeLimiteLinha)
    {
        this._quantidadeLimiteLinha = quantidadeLimiteLinha;
        this._has_quantidadeLimiteLinha = true;
    } //-- void setQuantidadeLimiteLinha(int) 

    /**
     * Sets the value of field 'quantidadeMaximaInconsistenteLote'.
     * 
     * @param quantidadeMaximaInconsistenteLote the value of field
     * 'quantidadeMaximaInconsistenteLote'.
     */
    public void setQuantidadeMaximaInconsistenteLote(int quantidadeMaximaInconsistenteLote)
    {
        this._quantidadeMaximaInconsistenteLote = quantidadeMaximaInconsistenteLote;
        this._has_quantidadeMaximaInconsistenteLote = true;
    } //-- void setQuantidadeMaximaInconsistenteLote(int) 

    /**
     * Sets the value of field 'quantidadeMaximaTituloVencido'.
     * 
     * @param quantidadeMaximaTituloVencido the value of field
     * 'quantidadeMaximaTituloVencido'.
     */
    public void setQuantidadeMaximaTituloVencido(int quantidadeMaximaTituloVencido)
    {
        this._quantidadeMaximaTituloVencido = quantidadeMaximaTituloVencido;
        this._has_quantidadeMaximaTituloVencido = true;
    } //-- void setQuantidadeMaximaTituloVencido(int) 

    /**
     * Sets the value of field 'quantidadeMesComprovante'.
     * 
     * @param quantidadeMesComprovante the value of field
     * 'quantidadeMesComprovante'.
     */
    public void setQuantidadeMesComprovante(int quantidadeMesComprovante)
    {
        this._quantidadeMesComprovante = quantidadeMesComprovante;
        this._has_quantidadeMesComprovante = true;
    } //-- void setQuantidadeMesComprovante(int) 

    /**
     * Sets the value of field 'quantidadeMesEtapaRecadastramento'.
     * 
     * @param quantidadeMesEtapaRecadastramento the value of field
     * 'quantidadeMesEtapaRecadastramento'.
     */
    public void setQuantidadeMesEtapaRecadastramento(int quantidadeMesEtapaRecadastramento)
    {
        this._quantidadeMesEtapaRecadastramento = quantidadeMesEtapaRecadastramento;
        this._has_quantidadeMesEtapaRecadastramento = true;
    } //-- void setQuantidadeMesEtapaRecadastramento(int) 

    /**
     * Sets the value of field 'quantidadeMesFaseRecadastramento'.
     * 
     * @param quantidadeMesFaseRecadastramento the value of field
     * 'quantidadeMesFaseRecadastramento'.
     */
    public void setQuantidadeMesFaseRecadastramento(int quantidadeMesFaseRecadastramento)
    {
        this._quantidadeMesFaseRecadastramento = quantidadeMesFaseRecadastramento;
        this._has_quantidadeMesFaseRecadastramento = true;
    } //-- void setQuantidadeMesFaseRecadastramento(int) 

    /**
     * Sets the value of field 'quantidadeMesReajusteTarifa'.
     * 
     * @param quantidadeMesReajusteTarifa the value of field
     * 'quantidadeMesReajusteTarifa'.
     */
    public void setQuantidadeMesReajusteTarifa(int quantidadeMesReajusteTarifa)
    {
        this._quantidadeMesReajusteTarifa = quantidadeMesReajusteTarifa;
        this._has_quantidadeMesReajusteTarifa = true;
    } //-- void setQuantidadeMesReajusteTarifa(int) 

    /**
     * Sets the value of field 'quantidadeSolicitacaoCartao'.
     * 
     * @param quantidadeSolicitacaoCartao the value of field
     * 'quantidadeSolicitacaoCartao'.
     */
    public void setQuantidadeSolicitacaoCartao(int quantidadeSolicitacaoCartao)
    {
        this._quantidadeSolicitacaoCartao = quantidadeSolicitacaoCartao;
        this._has_quantidadeSolicitacaoCartao = true;
    } //-- void setQuantidadeSolicitacaoCartao(int) 

    /**
     * Sets the value of field 'quantidadeViaAviso'.
     * 
     * @param quantidadeViaAviso the value of field
     * 'quantidadeViaAviso'.
     */
    public void setQuantidadeViaAviso(int quantidadeViaAviso)
    {
        this._quantidadeViaAviso = quantidadeViaAviso;
        this._has_quantidadeViaAviso = true;
    } //-- void setQuantidadeViaAviso(int) 

    /**
     * Sets the value of field 'quantidadeViaCombranca'.
     * 
     * @param quantidadeViaCombranca the value of field
     * 'quantidadeViaCombranca'.
     */
    public void setQuantidadeViaCombranca(int quantidadeViaCombranca)
    {
        this._quantidadeViaCombranca = quantidadeViaCombranca;
        this._has_quantidadeViaCombranca = true;
    } //-- void setQuantidadeViaCombranca(int) 

    /**
     * Sets the value of field 'quantidadeViaComprovante'.
     * 
     * @param quantidadeViaComprovante the value of field
     * 'quantidadeViaComprovante'.
     */
    public void setQuantidadeViaComprovante(int quantidadeViaComprovante)
    {
        this._quantidadeViaComprovante = quantidadeViaComprovante;
        this._has_quantidadeViaComprovante = true;
    } //-- void setQuantidadeViaComprovante(int) 

    /**
     * Sets the value of field 'valorFavorecidoNaoCadastrado'.
     * 
     * @param valorFavorecidoNaoCadastrado the value of field
     * 'valorFavorecidoNaoCadastrado'.
     */
    public void setValorFavorecidoNaoCadastrado(java.math.BigDecimal valorFavorecidoNaoCadastrado)
    {
        this._valorFavorecidoNaoCadastrado = valorFavorecidoNaoCadastrado;
    } //-- void setValorFavorecidoNaoCadastrado(java.math.BigDecimal) 

    /**
     * Sets the value of field 'valorLimiteDiarioPagamento'.
     * 
     * @param valorLimiteDiarioPagamento the value of field
     * 'valorLimiteDiarioPagamento'.
     */
    public void setValorLimiteDiarioPagamento(java.math.BigDecimal valorLimiteDiarioPagamento)
    {
        this._valorLimiteDiarioPagamento = valorLimiteDiarioPagamento;
    } //-- void setValorLimiteDiarioPagamento(java.math.BigDecimal) 

    /**
     * Sets the value of field 'valorLimiteIndividualPagamento'.
     * 
     * @param valorLimiteIndividualPagamento the value of field
     * 'valorLimiteIndividualPagamento'.
     */
    public void setValorLimiteIndividualPagamento(java.math.BigDecimal valorLimiteIndividualPagamento)
    {
        this._valorLimiteIndividualPagamento = valorLimiteIndividualPagamento;
    } //-- void setValorLimiteIndividualPagamento(java.math.BigDecimal) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ConsultarConfigTipoServicoModalidadeResponse
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultarconfigtiposervicomodalidade.response.ConsultarConfigTipoServicoModalidadeResponse unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultarconfigtiposervicomodalidade.response.ConsultarConfigTipoServicoModalidadeResponse) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultarconfigtiposervicomodalidade.response.ConsultarConfigTipoServicoModalidadeResponse.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarconfigtiposervicomodalidade.response.ConsultarConfigTipoServicoModalidadeResponse unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
