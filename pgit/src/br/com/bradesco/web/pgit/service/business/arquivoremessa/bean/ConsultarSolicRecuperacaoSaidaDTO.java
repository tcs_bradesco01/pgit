/*
 * Nome: br.com.bradesco.web.pgit.service.business.arquivoremessa.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.arquivoremessa.bean;


/**
 * Nome: ConsultarSolicRecuperacaoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarSolicRecuperacaoSaidaDTO {
		
	
	/** Atributo codMensagem. */
	private String codMensagem;
	
	/** Atributo mensagem. */
	private String mensagem;
	
	/** Atributo numeroLinhas. */
	private Integer numeroLinhas;
	
	/** Atributo nrSolicitacaoRecuperacao. */
	private Integer nrSolicitacaoRecuperacao;
	
	/** Atributo hrSolicitacaoRecuperacao. */
	private String hrSolicitacaoRecuperacao;
	
	/** Atributo cdUsuarioSolicitacao. */
	private String cdUsuarioSolicitacao;
	
	/** Atributo dsSituacaoSolicitacaoRecuperacao. */
	private String dsSituacaoSolicitacaoRecuperacao;
	
	/** Atributo dsDestinoRecuperacao. */
	private String dsDestinoRecuperacao;
	
	/** Atributo dataFormatada. */
	private String dataFormatada;
	
	/**
	 * Get: cdUsuarioSolicitacao.
	 *
	 * @return cdUsuarioSolicitacao
	 */
	public String getCdUsuarioSolicitacao() {
		return cdUsuarioSolicitacao;
	}
	
	/**
	 * Set: cdUsuarioSolicitacao.
	 *
	 * @param cdUsuarioSolicitacao the cd usuario solicitacao
	 */
	public void setCdUsuarioSolicitacao(String cdUsuarioSolicitacao) {
		this.cdUsuarioSolicitacao = cdUsuarioSolicitacao;
	}
	
	/**
	 * Get: dataFormatada.
	 *
	 * @return dataFormatada
	 */
	public String getDataFormatada() {
		return dataFormatada;
	}
	
	/**
	 * Set: dataFormatada.
	 *
	 * @param dataFormatada the data formatada
	 */
	public void setDataFormatada(String dataFormatada) {
		this.dataFormatada = dataFormatada;
	}
	
	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}
	
	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}
	
	/**
	 * Get: dsDestinoRecuperacao.
	 *
	 * @return dsDestinoRecuperacao
	 */
	public String getDsDestinoRecuperacao() {
		return dsDestinoRecuperacao;
	}
	
	/**
	 * Set: dsDestinoRecuperacao.
	 *
	 * @param dsDestinoRecuperacao the ds destino recuperacao
	 */
	public void setDsDestinoRecuperacao(String dsDestinoRecuperacao) {
		this.dsDestinoRecuperacao = dsDestinoRecuperacao;
	}
	
	/**
	 * Get: dsSituacaoSolicitacaoRecuperacao.
	 *
	 * @return dsSituacaoSolicitacaoRecuperacao
	 */
	public String getDsSituacaoSolicitacaoRecuperacao() {
		return dsSituacaoSolicitacaoRecuperacao;
	}
	
	/**
	 * Set: dsSituacaoSolicitacaoRecuperacao.
	 *
	 * @param dsSituacaoSolicitacaoRecuperacao the ds situacao solicitacao recuperacao
	 */
	public void setDsSituacaoSolicitacaoRecuperacao(
			String dsSituacaoSolicitacaoRecuperacao) {
		this.dsSituacaoSolicitacaoRecuperacao = dsSituacaoSolicitacaoRecuperacao;
	}
	
	/**
	 * Get: hrSolicitacaoRecuperacao.
	 *
	 * @return hrSolicitacaoRecuperacao
	 */
	public String getHrSolicitacaoRecuperacao() {
		return hrSolicitacaoRecuperacao;
	}
	
	/**
	 * Set: hrSolicitacaoRecuperacao.
	 *
	 * @param hrSolicitacaoRecuperacao the hr solicitacao recuperacao
	 */
	public void setHrSolicitacaoRecuperacao(String hrSolicitacaoRecuperacao) {
		this.hrSolicitacaoRecuperacao = hrSolicitacaoRecuperacao;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	/**
	 * Get: nrSolicitacaoRecuperacao.
	 *
	 * @return nrSolicitacaoRecuperacao
	 */
	public Integer getNrSolicitacaoRecuperacao() {
		return nrSolicitacaoRecuperacao;
	}
	
	/**
	 * Set: nrSolicitacaoRecuperacao.
	 *
	 * @param nrSolicitacaoRecuperacao the nr solicitacao recuperacao
	 */
	public void setNrSolicitacaoRecuperacao(Integer nrSolicitacaoRecuperacao) {
		this.nrSolicitacaoRecuperacao = nrSolicitacaoRecuperacao;
	}
	
	/**
	 * Get: numeroLinhas.
	 *
	 * @return numeroLinhas
	 */
	public Integer getNumeroLinhas() {
		return numeroLinhas;
	}
	
	/**
	 * Set: numeroLinhas.
	 *
	 * @param numeroLinhas the numero linhas
	 */
	public void setNumeroLinhas(Integer numeroLinhas) {
		this.numeroLinhas = numeroLinhas;
	}
	
	
	

}
