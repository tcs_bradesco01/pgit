/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.detalharcontacomplementar.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class DetalharContaComplementarResponse.
 * 
 * @version $Revision$ $Date$
 */
public class DetalharContaComplementarResponse implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _codMensagem
     */
    private java.lang.String _codMensagem;

    /**
     * Field _mensagem
     */
    private java.lang.String _mensagem;

    /**
     * Field _cdPessoaJuridicaContrato
     */
    private long _cdPessoaJuridicaContrato = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridicaContrato
     */
    private boolean _has_cdPessoaJuridicaContrato;

    /**
     * Field _cdTipoContratoNegocio
     */
    private int _cdTipoContratoNegocio = 0;

    /**
     * keeps track of state for field: _cdTipoContratoNegocio
     */
    private boolean _has_cdTipoContratoNegocio;

    /**
     * Field _nrSequenciaContratoNegocio
     */
    private long _nrSequenciaContratoNegocio = 0;

    /**
     * keeps track of state for field: _nrSequenciaContratoNegocio
     */
    private boolean _has_nrSequenciaContratoNegocio;

    /**
     * Field _cdContaComplementar
     */
    private int _cdContaComplementar = 0;

    /**
     * keeps track of state for field: _cdContaComplementar
     */
    private boolean _has_cdContaComplementar;

    /**
     * Field _cdCorpoCnpjCpf
     */
    private long _cdCorpoCnpjCpf = 0;

    /**
     * keeps track of state for field: _cdCorpoCnpjCpf
     */
    private boolean _has_cdCorpoCnpjCpf;

    /**
     * Field _cdFilialCpfCnpj
     */
    private int _cdFilialCpfCnpj = 0;

    /**
     * keeps track of state for field: _cdFilialCpfCnpj
     */
    private boolean _has_cdFilialCpfCnpj;

    /**
     * Field _cdCtrlCpfCnpj
     */
    private int _cdCtrlCpfCnpj = 0;

    /**
     * keeps track of state for field: _cdCtrlCpfCnpj
     */
    private boolean _has_cdCtrlCpfCnpj;

    /**
     * Field _dsNomeParticipante
     */
    private java.lang.String _dsNomeParticipante;

    /**
     * Field _cdPessoaJuridicaContratoConta
     */
    private long _cdPessoaJuridicaContratoConta = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridicaContratoCont
     */
    private boolean _has_cdPessoaJuridicaContratoConta;

    /**
     * Field _cdTipoContratoConta
     */
    private int _cdTipoContratoConta = 0;

    /**
     * keeps track of state for field: _cdTipoContratoConta
     */
    private boolean _has_cdTipoContratoConta;

    /**
     * Field _nrSequenciaContratoConta
     */
    private long _nrSequenciaContratoConta = 0;

    /**
     * keeps track of state for field: _nrSequenciaContratoConta
     */
    private boolean _has_nrSequenciaContratoConta;

    /**
     * Field _cdBanco
     */
    private int _cdBanco = 0;

    /**
     * keeps track of state for field: _cdBanco
     */
    private boolean _has_cdBanco;

    /**
     * Field _dsBanco
     */
    private java.lang.String _dsBanco;

    /**
     * Field _cdAgencia
     */
    private int _cdAgencia = 0;

    /**
     * keeps track of state for field: _cdAgencia
     */
    private boolean _has_cdAgencia;

    /**
     * Field _dsAgencia
     */
    private java.lang.String _dsAgencia;

    /**
     * Field _cdConta
     */
    private long _cdConta = 0;

    /**
     * keeps track of state for field: _cdConta
     */
    private boolean _has_cdConta;

    /**
     * Field _cdDigitoConta
     */
    private java.lang.String _cdDigitoConta;

    /**
     * Field _cdUsuarioInclusao
     */
    private java.lang.String _cdUsuarioInclusao;

    /**
     * Field _hrInclusaoRegistro
     */
    private java.lang.String _hrInclusaoRegistro;

    /**
     * Field _cdTipoCanalInclusao
     */
    private int _cdTipoCanalInclusao = 0;

    /**
     * keeps track of state for field: _cdTipoCanalInclusao
     */
    private boolean _has_cdTipoCanalInclusao;

    /**
     * Field _cdUsuarioManutencao
     */
    private java.lang.String _cdUsuarioManutencao;

    /**
     * Field _hrManutencaoRegistro
     */
    private java.lang.String _hrManutencaoRegistro;

    /**
     * Field _cdTipoCanalManutencao
     */
    private int _cdTipoCanalManutencao = 0;

    /**
     * keeps track of state for field: _cdTipoCanalManutencao
     */
    private boolean _has_cdTipoCanalManutencao;


      //----------------/
     //- Constructors -/
    //----------------/

    public DetalharContaComplementarResponse() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharcontacomplementar.response.DetalharContaComplementarResponse()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdAgencia
     * 
     */
    public void deleteCdAgencia()
    {
        this._has_cdAgencia= false;
    } //-- void deleteCdAgencia() 

    /**
     * Method deleteCdBanco
     * 
     */
    public void deleteCdBanco()
    {
        this._has_cdBanco= false;
    } //-- void deleteCdBanco() 

    /**
     * Method deleteCdConta
     * 
     */
    public void deleteCdConta()
    {
        this._has_cdConta= false;
    } //-- void deleteCdConta() 

    /**
     * Method deleteCdContaComplementar
     * 
     */
    public void deleteCdContaComplementar()
    {
        this._has_cdContaComplementar= false;
    } //-- void deleteCdContaComplementar() 

    /**
     * Method deleteCdCorpoCnpjCpf
     * 
     */
    public void deleteCdCorpoCnpjCpf()
    {
        this._has_cdCorpoCnpjCpf= false;
    } //-- void deleteCdCorpoCnpjCpf() 

    /**
     * Method deleteCdCtrlCpfCnpj
     * 
     */
    public void deleteCdCtrlCpfCnpj()
    {
        this._has_cdCtrlCpfCnpj= false;
    } //-- void deleteCdCtrlCpfCnpj() 

    /**
     * Method deleteCdFilialCpfCnpj
     * 
     */
    public void deleteCdFilialCpfCnpj()
    {
        this._has_cdFilialCpfCnpj= false;
    } //-- void deleteCdFilialCpfCnpj() 

    /**
     * Method deleteCdPessoaJuridicaContrato
     * 
     */
    public void deleteCdPessoaJuridicaContrato()
    {
        this._has_cdPessoaJuridicaContrato= false;
    } //-- void deleteCdPessoaJuridicaContrato() 

    /**
     * Method deleteCdPessoaJuridicaContratoConta
     * 
     */
    public void deleteCdPessoaJuridicaContratoConta()
    {
        this._has_cdPessoaJuridicaContratoConta= false;
    } //-- void deleteCdPessoaJuridicaContratoConta() 

    /**
     * Method deleteCdTipoCanalInclusao
     * 
     */
    public void deleteCdTipoCanalInclusao()
    {
        this._has_cdTipoCanalInclusao= false;
    } //-- void deleteCdTipoCanalInclusao() 

    /**
     * Method deleteCdTipoCanalManutencao
     * 
     */
    public void deleteCdTipoCanalManutencao()
    {
        this._has_cdTipoCanalManutencao= false;
    } //-- void deleteCdTipoCanalManutencao() 

    /**
     * Method deleteCdTipoContratoConta
     * 
     */
    public void deleteCdTipoContratoConta()
    {
        this._has_cdTipoContratoConta= false;
    } //-- void deleteCdTipoContratoConta() 

    /**
     * Method deleteCdTipoContratoNegocio
     * 
     */
    public void deleteCdTipoContratoNegocio()
    {
        this._has_cdTipoContratoNegocio= false;
    } //-- void deleteCdTipoContratoNegocio() 

    /**
     * Method deleteNrSequenciaContratoConta
     * 
     */
    public void deleteNrSequenciaContratoConta()
    {
        this._has_nrSequenciaContratoConta= false;
    } //-- void deleteNrSequenciaContratoConta() 

    /**
     * Method deleteNrSequenciaContratoNegocio
     * 
     */
    public void deleteNrSequenciaContratoNegocio()
    {
        this._has_nrSequenciaContratoNegocio= false;
    } //-- void deleteNrSequenciaContratoNegocio() 

    /**
     * Returns the value of field 'cdAgencia'.
     * 
     * @return int
     * @return the value of field 'cdAgencia'.
     */
    public int getCdAgencia()
    {
        return this._cdAgencia;
    } //-- int getCdAgencia() 

    /**
     * Returns the value of field 'cdBanco'.
     * 
     * @return int
     * @return the value of field 'cdBanco'.
     */
    public int getCdBanco()
    {
        return this._cdBanco;
    } //-- int getCdBanco() 

    /**
     * Returns the value of field 'cdConta'.
     * 
     * @return long
     * @return the value of field 'cdConta'.
     */
    public long getCdConta()
    {
        return this._cdConta;
    } //-- long getCdConta() 

    /**
     * Returns the value of field 'cdContaComplementar'.
     * 
     * @return int
     * @return the value of field 'cdContaComplementar'.
     */
    public int getCdContaComplementar()
    {
        return this._cdContaComplementar;
    } //-- int getCdContaComplementar() 

    /**
     * Returns the value of field 'cdCorpoCnpjCpf'.
     * 
     * @return long
     * @return the value of field 'cdCorpoCnpjCpf'.
     */
    public long getCdCorpoCnpjCpf()
    {
        return this._cdCorpoCnpjCpf;
    } //-- long getCdCorpoCnpjCpf() 

    /**
     * Returns the value of field 'cdCtrlCpfCnpj'.
     * 
     * @return int
     * @return the value of field 'cdCtrlCpfCnpj'.
     */
    public int getCdCtrlCpfCnpj()
    {
        return this._cdCtrlCpfCnpj;
    } //-- int getCdCtrlCpfCnpj() 

    /**
     * Returns the value of field 'cdDigitoConta'.
     * 
     * @return String
     * @return the value of field 'cdDigitoConta'.
     */
    public java.lang.String getCdDigitoConta()
    {
        return this._cdDigitoConta;
    } //-- java.lang.String getCdDigitoConta() 

    /**
     * Returns the value of field 'cdFilialCpfCnpj'.
     * 
     * @return int
     * @return the value of field 'cdFilialCpfCnpj'.
     */
    public int getCdFilialCpfCnpj()
    {
        return this._cdFilialCpfCnpj;
    } //-- int getCdFilialCpfCnpj() 

    /**
     * Returns the value of field 'cdPessoaJuridicaContrato'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridicaContrato'.
     */
    public long getCdPessoaJuridicaContrato()
    {
        return this._cdPessoaJuridicaContrato;
    } //-- long getCdPessoaJuridicaContrato() 

    /**
     * Returns the value of field 'cdPessoaJuridicaContratoConta'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridicaContratoConta'.
     */
    public long getCdPessoaJuridicaContratoConta()
    {
        return this._cdPessoaJuridicaContratoConta;
    } //-- long getCdPessoaJuridicaContratoConta() 

    /**
     * Returns the value of field 'cdTipoCanalInclusao'.
     * 
     * @return int
     * @return the value of field 'cdTipoCanalInclusao'.
     */
    public int getCdTipoCanalInclusao()
    {
        return this._cdTipoCanalInclusao;
    } //-- int getCdTipoCanalInclusao() 

    /**
     * Returns the value of field 'cdTipoCanalManutencao'.
     * 
     * @return int
     * @return the value of field 'cdTipoCanalManutencao'.
     */
    public int getCdTipoCanalManutencao()
    {
        return this._cdTipoCanalManutencao;
    } //-- int getCdTipoCanalManutencao() 

    /**
     * Returns the value of field 'cdTipoContratoConta'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoConta'.
     */
    public int getCdTipoContratoConta()
    {
        return this._cdTipoContratoConta;
    } //-- int getCdTipoContratoConta() 

    /**
     * Returns the value of field 'cdTipoContratoNegocio'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoNegocio'.
     */
    public int getCdTipoContratoNegocio()
    {
        return this._cdTipoContratoNegocio;
    } //-- int getCdTipoContratoNegocio() 

    /**
     * Returns the value of field 'cdUsuarioInclusao'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioInclusao'.
     */
    public java.lang.String getCdUsuarioInclusao()
    {
        return this._cdUsuarioInclusao;
    } //-- java.lang.String getCdUsuarioInclusao() 

    /**
     * Returns the value of field 'cdUsuarioManutencao'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioManutencao'.
     */
    public java.lang.String getCdUsuarioManutencao()
    {
        return this._cdUsuarioManutencao;
    } //-- java.lang.String getCdUsuarioManutencao() 

    /**
     * Returns the value of field 'codMensagem'.
     * 
     * @return String
     * @return the value of field 'codMensagem'.
     */
    public java.lang.String getCodMensagem()
    {
        return this._codMensagem;
    } //-- java.lang.String getCodMensagem() 

    /**
     * Returns the value of field 'dsAgencia'.
     * 
     * @return String
     * @return the value of field 'dsAgencia'.
     */
    public java.lang.String getDsAgencia()
    {
        return this._dsAgencia;
    } //-- java.lang.String getDsAgencia() 

    /**
     * Returns the value of field 'dsBanco'.
     * 
     * @return String
     * @return the value of field 'dsBanco'.
     */
    public java.lang.String getDsBanco()
    {
        return this._dsBanco;
    } //-- java.lang.String getDsBanco() 

    /**
     * Returns the value of field 'dsNomeParticipante'.
     * 
     * @return String
     * @return the value of field 'dsNomeParticipante'.
     */
    public java.lang.String getDsNomeParticipante()
    {
        return this._dsNomeParticipante;
    } //-- java.lang.String getDsNomeParticipante() 

    /**
     * Returns the value of field 'hrInclusaoRegistro'.
     * 
     * @return String
     * @return the value of field 'hrInclusaoRegistro'.
     */
    public java.lang.String getHrInclusaoRegistro()
    {
        return this._hrInclusaoRegistro;
    } //-- java.lang.String getHrInclusaoRegistro() 

    /**
     * Returns the value of field 'hrManutencaoRegistro'.
     * 
     * @return String
     * @return the value of field 'hrManutencaoRegistro'.
     */
    public java.lang.String getHrManutencaoRegistro()
    {
        return this._hrManutencaoRegistro;
    } //-- java.lang.String getHrManutencaoRegistro() 

    /**
     * Returns the value of field 'mensagem'.
     * 
     * @return String
     * @return the value of field 'mensagem'.
     */
    public java.lang.String getMensagem()
    {
        return this._mensagem;
    } //-- java.lang.String getMensagem() 

    /**
     * Returns the value of field 'nrSequenciaContratoConta'.
     * 
     * @return long
     * @return the value of field 'nrSequenciaContratoConta'.
     */
    public long getNrSequenciaContratoConta()
    {
        return this._nrSequenciaContratoConta;
    } //-- long getNrSequenciaContratoConta() 

    /**
     * Returns the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @return long
     * @return the value of field 'nrSequenciaContratoNegocio'.
     */
    public long getNrSequenciaContratoNegocio()
    {
        return this._nrSequenciaContratoNegocio;
    } //-- long getNrSequenciaContratoNegocio() 

    /**
     * Method hasCdAgencia
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgencia()
    {
        return this._has_cdAgencia;
    } //-- boolean hasCdAgencia() 

    /**
     * Method hasCdBanco
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdBanco()
    {
        return this._has_cdBanco;
    } //-- boolean hasCdBanco() 

    /**
     * Method hasCdConta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdConta()
    {
        return this._has_cdConta;
    } //-- boolean hasCdConta() 

    /**
     * Method hasCdContaComplementar
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdContaComplementar()
    {
        return this._has_cdContaComplementar;
    } //-- boolean hasCdContaComplementar() 

    /**
     * Method hasCdCorpoCnpjCpf
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCorpoCnpjCpf()
    {
        return this._has_cdCorpoCnpjCpf;
    } //-- boolean hasCdCorpoCnpjCpf() 

    /**
     * Method hasCdCtrlCpfCnpj
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCtrlCpfCnpj()
    {
        return this._has_cdCtrlCpfCnpj;
    } //-- boolean hasCdCtrlCpfCnpj() 

    /**
     * Method hasCdFilialCpfCnpj
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFilialCpfCnpj()
    {
        return this._has_cdFilialCpfCnpj;
    } //-- boolean hasCdFilialCpfCnpj() 

    /**
     * Method hasCdPessoaJuridicaContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridicaContrato()
    {
        return this._has_cdPessoaJuridicaContrato;
    } //-- boolean hasCdPessoaJuridicaContrato() 

    /**
     * Method hasCdPessoaJuridicaContratoConta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridicaContratoConta()
    {
        return this._has_cdPessoaJuridicaContratoConta;
    } //-- boolean hasCdPessoaJuridicaContratoConta() 

    /**
     * Method hasCdTipoCanalInclusao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoCanalInclusao()
    {
        return this._has_cdTipoCanalInclusao;
    } //-- boolean hasCdTipoCanalInclusao() 

    /**
     * Method hasCdTipoCanalManutencao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoCanalManutencao()
    {
        return this._has_cdTipoCanalManutencao;
    } //-- boolean hasCdTipoCanalManutencao() 

    /**
     * Method hasCdTipoContratoConta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoConta()
    {
        return this._has_cdTipoContratoConta;
    } //-- boolean hasCdTipoContratoConta() 

    /**
     * Method hasCdTipoContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoNegocio()
    {
        return this._has_cdTipoContratoNegocio;
    } //-- boolean hasCdTipoContratoNegocio() 

    /**
     * Method hasNrSequenciaContratoConta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaContratoConta()
    {
        return this._has_nrSequenciaContratoConta;
    } //-- boolean hasNrSequenciaContratoConta() 

    /**
     * Method hasNrSequenciaContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaContratoNegocio()
    {
        return this._has_nrSequenciaContratoNegocio;
    } //-- boolean hasNrSequenciaContratoNegocio() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdAgencia'.
     * 
     * @param cdAgencia the value of field 'cdAgencia'.
     */
    public void setCdAgencia(int cdAgencia)
    {
        this._cdAgencia = cdAgencia;
        this._has_cdAgencia = true;
    } //-- void setCdAgencia(int) 

    /**
     * Sets the value of field 'cdBanco'.
     * 
     * @param cdBanco the value of field 'cdBanco'.
     */
    public void setCdBanco(int cdBanco)
    {
        this._cdBanco = cdBanco;
        this._has_cdBanco = true;
    } //-- void setCdBanco(int) 

    /**
     * Sets the value of field 'cdConta'.
     * 
     * @param cdConta the value of field 'cdConta'.
     */
    public void setCdConta(long cdConta)
    {
        this._cdConta = cdConta;
        this._has_cdConta = true;
    } //-- void setCdConta(long) 

    /**
     * Sets the value of field 'cdContaComplementar'.
     * 
     * @param cdContaComplementar the value of field
     * 'cdContaComplementar'.
     */
    public void setCdContaComplementar(int cdContaComplementar)
    {
        this._cdContaComplementar = cdContaComplementar;
        this._has_cdContaComplementar = true;
    } //-- void setCdContaComplementar(int) 

    /**
     * Sets the value of field 'cdCorpoCnpjCpf'.
     * 
     * @param cdCorpoCnpjCpf the value of field 'cdCorpoCnpjCpf'.
     */
    public void setCdCorpoCnpjCpf(long cdCorpoCnpjCpf)
    {
        this._cdCorpoCnpjCpf = cdCorpoCnpjCpf;
        this._has_cdCorpoCnpjCpf = true;
    } //-- void setCdCorpoCnpjCpf(long) 

    /**
     * Sets the value of field 'cdCtrlCpfCnpj'.
     * 
     * @param cdCtrlCpfCnpj the value of field 'cdCtrlCpfCnpj'.
     */
    public void setCdCtrlCpfCnpj(int cdCtrlCpfCnpj)
    {
        this._cdCtrlCpfCnpj = cdCtrlCpfCnpj;
        this._has_cdCtrlCpfCnpj = true;
    } //-- void setCdCtrlCpfCnpj(int) 

    /**
     * Sets the value of field 'cdDigitoConta'.
     * 
     * @param cdDigitoConta the value of field 'cdDigitoConta'.
     */
    public void setCdDigitoConta(java.lang.String cdDigitoConta)
    {
        this._cdDigitoConta = cdDigitoConta;
    } //-- void setCdDigitoConta(java.lang.String) 

    /**
     * Sets the value of field 'cdFilialCpfCnpj'.
     * 
     * @param cdFilialCpfCnpj the value of field 'cdFilialCpfCnpj'.
     */
    public void setCdFilialCpfCnpj(int cdFilialCpfCnpj)
    {
        this._cdFilialCpfCnpj = cdFilialCpfCnpj;
        this._has_cdFilialCpfCnpj = true;
    } //-- void setCdFilialCpfCnpj(int) 

    /**
     * Sets the value of field 'cdPessoaJuridicaContrato'.
     * 
     * @param cdPessoaJuridicaContrato the value of field
     * 'cdPessoaJuridicaContrato'.
     */
    public void setCdPessoaJuridicaContrato(long cdPessoaJuridicaContrato)
    {
        this._cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
        this._has_cdPessoaJuridicaContrato = true;
    } //-- void setCdPessoaJuridicaContrato(long) 

    /**
     * Sets the value of field 'cdPessoaJuridicaContratoConta'.
     * 
     * @param cdPessoaJuridicaContratoConta the value of field
     * 'cdPessoaJuridicaContratoConta'.
     */
    public void setCdPessoaJuridicaContratoConta(long cdPessoaJuridicaContratoConta)
    {
        this._cdPessoaJuridicaContratoConta = cdPessoaJuridicaContratoConta;
        this._has_cdPessoaJuridicaContratoConta = true;
    } //-- void setCdPessoaJuridicaContratoConta(long) 

    /**
     * Sets the value of field 'cdTipoCanalInclusao'.
     * 
     * @param cdTipoCanalInclusao the value of field
     * 'cdTipoCanalInclusao'.
     */
    public void setCdTipoCanalInclusao(int cdTipoCanalInclusao)
    {
        this._cdTipoCanalInclusao = cdTipoCanalInclusao;
        this._has_cdTipoCanalInclusao = true;
    } //-- void setCdTipoCanalInclusao(int) 

    /**
     * Sets the value of field 'cdTipoCanalManutencao'.
     * 
     * @param cdTipoCanalManutencao the value of field
     * 'cdTipoCanalManutencao'.
     */
    public void setCdTipoCanalManutencao(int cdTipoCanalManutencao)
    {
        this._cdTipoCanalManutencao = cdTipoCanalManutencao;
        this._has_cdTipoCanalManutencao = true;
    } //-- void setCdTipoCanalManutencao(int) 

    /**
     * Sets the value of field 'cdTipoContratoConta'.
     * 
     * @param cdTipoContratoConta the value of field
     * 'cdTipoContratoConta'.
     */
    public void setCdTipoContratoConta(int cdTipoContratoConta)
    {
        this._cdTipoContratoConta = cdTipoContratoConta;
        this._has_cdTipoContratoConta = true;
    } //-- void setCdTipoContratoConta(int) 

    /**
     * Sets the value of field 'cdTipoContratoNegocio'.
     * 
     * @param cdTipoContratoNegocio the value of field
     * 'cdTipoContratoNegocio'.
     */
    public void setCdTipoContratoNegocio(int cdTipoContratoNegocio)
    {
        this._cdTipoContratoNegocio = cdTipoContratoNegocio;
        this._has_cdTipoContratoNegocio = true;
    } //-- void setCdTipoContratoNegocio(int) 

    /**
     * Sets the value of field 'cdUsuarioInclusao'.
     * 
     * @param cdUsuarioInclusao the value of field
     * 'cdUsuarioInclusao'.
     */
    public void setCdUsuarioInclusao(java.lang.String cdUsuarioInclusao)
    {
        this._cdUsuarioInclusao = cdUsuarioInclusao;
    } //-- void setCdUsuarioInclusao(java.lang.String) 

    /**
     * Sets the value of field 'cdUsuarioManutencao'.
     * 
     * @param cdUsuarioManutencao the value of field
     * 'cdUsuarioManutencao'.
     */
    public void setCdUsuarioManutencao(java.lang.String cdUsuarioManutencao)
    {
        this._cdUsuarioManutencao = cdUsuarioManutencao;
    } //-- void setCdUsuarioManutencao(java.lang.String) 

    /**
     * Sets the value of field 'codMensagem'.
     * 
     * @param codMensagem the value of field 'codMensagem'.
     */
    public void setCodMensagem(java.lang.String codMensagem)
    {
        this._codMensagem = codMensagem;
    } //-- void setCodMensagem(java.lang.String) 

    /**
     * Sets the value of field 'dsAgencia'.
     * 
     * @param dsAgencia the value of field 'dsAgencia'.
     */
    public void setDsAgencia(java.lang.String dsAgencia)
    {
        this._dsAgencia = dsAgencia;
    } //-- void setDsAgencia(java.lang.String) 

    /**
     * Sets the value of field 'dsBanco'.
     * 
     * @param dsBanco the value of field 'dsBanco'.
     */
    public void setDsBanco(java.lang.String dsBanco)
    {
        this._dsBanco = dsBanco;
    } //-- void setDsBanco(java.lang.String) 

    /**
     * Sets the value of field 'dsNomeParticipante'.
     * 
     * @param dsNomeParticipante the value of field
     * 'dsNomeParticipante'.
     */
    public void setDsNomeParticipante(java.lang.String dsNomeParticipante)
    {
        this._dsNomeParticipante = dsNomeParticipante;
    } //-- void setDsNomeParticipante(java.lang.String) 

    /**
     * Sets the value of field 'hrInclusaoRegistro'.
     * 
     * @param hrInclusaoRegistro the value of field
     * 'hrInclusaoRegistro'.
     */
    public void setHrInclusaoRegistro(java.lang.String hrInclusaoRegistro)
    {
        this._hrInclusaoRegistro = hrInclusaoRegistro;
    } //-- void setHrInclusaoRegistro(java.lang.String) 

    /**
     * Sets the value of field 'hrManutencaoRegistro'.
     * 
     * @param hrManutencaoRegistro the value of field
     * 'hrManutencaoRegistro'.
     */
    public void setHrManutencaoRegistro(java.lang.String hrManutencaoRegistro)
    {
        this._hrManutencaoRegistro = hrManutencaoRegistro;
    } //-- void setHrManutencaoRegistro(java.lang.String) 

    /**
     * Sets the value of field 'mensagem'.
     * 
     * @param mensagem the value of field 'mensagem'.
     */
    public void setMensagem(java.lang.String mensagem)
    {
        this._mensagem = mensagem;
    } //-- void setMensagem(java.lang.String) 

    /**
     * Sets the value of field 'nrSequenciaContratoConta'.
     * 
     * @param nrSequenciaContratoConta the value of field
     * 'nrSequenciaContratoConta'.
     */
    public void setNrSequenciaContratoConta(long nrSequenciaContratoConta)
    {
        this._nrSequenciaContratoConta = nrSequenciaContratoConta;
        this._has_nrSequenciaContratoConta = true;
    } //-- void setNrSequenciaContratoConta(long) 

    /**
     * Sets the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @param nrSequenciaContratoNegocio the value of field
     * 'nrSequenciaContratoNegocio'.
     */
    public void setNrSequenciaContratoNegocio(long nrSequenciaContratoNegocio)
    {
        this._nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
        this._has_nrSequenciaContratoNegocio = true;
    } //-- void setNrSequenciaContratoNegocio(long) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return DetalharContaComplementarResponse
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.detalharcontacomplementar.response.DetalharContaComplementarResponse unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.detalharcontacomplementar.response.DetalharContaComplementarResponse) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.detalharcontacomplementar.response.DetalharContaComplementarResponse.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharcontacomplementar.response.DetalharContaComplementarResponse unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
