/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantercontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantercontrato.bean;

/**
 * Nome: ListarTarifasContratoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarTarifasContratoSaidaDTO {
	
	/** Atributo codMensagem. */
	private String codMensagem;
	
	/** Atributo mensagem. */
	private String mensagem;
	
	/** Atributo cdServico. */
	private int cdServico;
	
	/** Atributo dsServico. */
	private String dsServico;
	
	/** Atributo cdModalidade. */
	private int cdModalidade;
	
	/** Atributo dsModalidade. */
	private String dsModalidade;
	
	/** Atributo cdRelacionamentoProduto. */
	private int cdRelacionamentoProduto;
	
	/** Atributo dtInclusaoServico. */
	private String dtInclusaoServico;
	
	/** Atributo cdSituacaoServicoRelacionado. */
	private int cdSituacaoServicoRelacionado;
	
	/** Atributo dtSituacaoServicoRelacionado. */
	private String dtSituacaoServicoRelacionado;
	
	
	/**
	 * Get: cdModalidade.
	 *
	 * @return cdModalidade
	 */
	public int getCdModalidade() {
		return cdModalidade;
	}
	
	/**
	 * Set: cdModalidade.
	 *
	 * @param cdModalidade the cd modalidade
	 */
	public void setCdModalidade(int cdModalidade) {
		this.cdModalidade = cdModalidade;
	}
	
	/**
	 * Get: cdRelacionamentoProduto.
	 *
	 * @return cdRelacionamentoProduto
	 */
	public int getCdRelacionamentoProduto() {
		return cdRelacionamentoProduto;
	}
	
	/**
	 * Set: cdRelacionamentoProduto.
	 *
	 * @param cdRelacionamentoProduto the cd relacionamento produto
	 */
	public void setCdRelacionamentoProduto(int cdRelacionamentoProduto) {
		this.cdRelacionamentoProduto = cdRelacionamentoProduto;
	}
	
	/**
	 * Get: cdServico.
	 *
	 * @return cdServico
	 */
	public int getCdServico() {
		return cdServico;
	}
	
	/**
	 * Set: cdServico.
	 *
	 * @param cdServico the cd servico
	 */
	public void setCdServico(int cdServico) {
		this.cdServico = cdServico;
	}
	
	/**
	 * Get: cdSituacaoServicoRelacionado.
	 *
	 * @return cdSituacaoServicoRelacionado
	 */
	public int getCdSituacaoServicoRelacionado() {
		return cdSituacaoServicoRelacionado;
	}
	
	/**
	 * Set: cdSituacaoServicoRelacionado.
	 *
	 * @param cdSituacaoServicoRelacionado the cd situacao servico relacionado
	 */
	public void setCdSituacaoServicoRelacionado(int cdSituacaoServicoRelacionado) {
		this.cdSituacaoServicoRelacionado = cdSituacaoServicoRelacionado;
	}
	
	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}
	
	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}
	
	/**
	 * Get: dsModalidade.
	 *
	 * @return dsModalidade
	 */
	public String getDsModalidade() {
		return dsModalidade;
	}
	
	/**
	 * Set: dsModalidade.
	 *
	 * @param dsModalidade the ds modalidade
	 */
	public void setDsModalidade(String dsModalidade) {
		this.dsModalidade = dsModalidade;
	}
	
	/**
	 * Get: dsServico.
	 *
	 * @return dsServico
	 */
	public String getDsServico() {
		return dsServico;
	}
	
	/**
	 * Set: dsServico.
	 *
	 * @param dsServico the ds servico
	 */
	public void setDsServico(String dsServico) {
		this.dsServico = dsServico;
	}
	
	/**
	 * Get: dtInclusaoServico.
	 *
	 * @return dtInclusaoServico
	 */
	public String getDtInclusaoServico() {
		return dtInclusaoServico;
	}
	
	/**
	 * Set: dtInclusaoServico.
	 *
	 * @param dtInclusaoServico the dt inclusao servico
	 */
	public void setDtInclusaoServico(String dtInclusaoServico) {
		this.dtInclusaoServico = dtInclusaoServico;
	}
	
	/**
	 * Get: dtSituacaoServicoRelacionado.
	 *
	 * @return dtSituacaoServicoRelacionado
	 */
	public String getDtSituacaoServicoRelacionado() {
		return dtSituacaoServicoRelacionado;
	}
	
	/**
	 * Set: dtSituacaoServicoRelacionado.
	 *
	 * @param dtSituacaoServicoRelacionado the dt situacao servico relacionado
	 */
	public void setDtSituacaoServicoRelacionado(String dtSituacaoServicoRelacionado) {
		this.dtSituacaoServicoRelacionado = dtSituacaoServicoRelacionado;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	
	

}
