/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listarcontasvinculadascontrato.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ListarContasVinculadasContratoRequest.
 * 
 * @version $Revision$ $Date$
 */
public class ListarContasVinculadasContratoRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _nrOcorrencias
     */
    private int _nrOcorrencias = 0;

    /**
     * keeps track of state for field: _nrOcorrencias
     */
    private boolean _has_nrOcorrencias;

    /**
     * Field _cdPessoaJuridicaNegocio
     */
    private long _cdPessoaJuridicaNegocio = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridicaNegocio
     */
    private boolean _has_cdPessoaJuridicaNegocio;

    /**
     * Field _cdTipoContratoNegocio
     */
    private int _cdTipoContratoNegocio = 0;

    /**
     * keeps track of state for field: _cdTipoContratoNegocio
     */
    private boolean _has_cdTipoContratoNegocio;

    /**
     * Field _nrSequenciaContratoNegocio
     */
    private long _nrSequenciaContratoNegocio = 0;

    /**
     * keeps track of state for field: _nrSequenciaContratoNegocio
     */
    private boolean _has_nrSequenciaContratoNegocio;

    /**
     * Field _cdCorpoCfpCnpj
     */
    private long _cdCorpoCfpCnpj = 0;

    /**
     * keeps track of state for field: _cdCorpoCfpCnpj
     */
    private boolean _has_cdCorpoCfpCnpj;

    /**
     * Field _cdControleCpfCnpj
     */
    private int _cdControleCpfCnpj = 0;

    /**
     * keeps track of state for field: _cdControleCpfCnpj
     */
    private boolean _has_cdControleCpfCnpj;

    /**
     * Field _cdDigitoCpfCnpj
     */
    private int _cdDigitoCpfCnpj = 0;

    /**
     * keeps track of state for field: _cdDigitoCpfCnpj
     */
    private boolean _has_cdDigitoCpfCnpj;

    /**
     * Field _cdFinalidade
     */
    private int _cdFinalidade = 0;

    /**
     * keeps track of state for field: _cdFinalidade
     */
    private boolean _has_cdFinalidade;

    /**
     * Field _cdBanco
     */
    private int _cdBanco = 0;

    /**
     * keeps track of state for field: _cdBanco
     */
    private boolean _has_cdBanco;

    /**
     * Field _cdAgencia
     */
    private int _cdAgencia = 0;

    /**
     * keeps track of state for field: _cdAgencia
     */
    private boolean _has_cdAgencia;

    /**
     * Field _cdDigitoAgencia
     */
    private int _cdDigitoAgencia = 0;

    /**
     * keeps track of state for field: _cdDigitoAgencia
     */
    private boolean _has_cdDigitoAgencia;

    /**
     * Field _cdConta
     */
    private long _cdConta = 0;

    /**
     * keeps track of state for field: _cdConta
     */
    private boolean _has_cdConta;

    /**
     * Field _cdDigitoConta
     */
    private java.lang.String _cdDigitoConta;

    /**
     * Field _cdTipoConta
     */
    private int _cdTipoConta = 0;

    /**
     * keeps track of state for field: _cdTipoConta
     */
    private boolean _has_cdTipoConta;


      //----------------/
     //- Constructors -/
    //----------------/

    public ListarContasVinculadasContratoRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarcontasvinculadascontrato.request.ListarContasVinculadasContratoRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdAgencia
     * 
     */
    public void deleteCdAgencia()
    {
        this._has_cdAgencia= false;
    } //-- void deleteCdAgencia() 

    /**
     * Method deleteCdBanco
     * 
     */
    public void deleteCdBanco()
    {
        this._has_cdBanco= false;
    } //-- void deleteCdBanco() 

    /**
     * Method deleteCdConta
     * 
     */
    public void deleteCdConta()
    {
        this._has_cdConta= false;
    } //-- void deleteCdConta() 

    /**
     * Method deleteCdControleCpfCnpj
     * 
     */
    public void deleteCdControleCpfCnpj()
    {
        this._has_cdControleCpfCnpj= false;
    } //-- void deleteCdControleCpfCnpj() 

    /**
     * Method deleteCdCorpoCfpCnpj
     * 
     */
    public void deleteCdCorpoCfpCnpj()
    {
        this._has_cdCorpoCfpCnpj= false;
    } //-- void deleteCdCorpoCfpCnpj() 

    /**
     * Method deleteCdDigitoAgencia
     * 
     */
    public void deleteCdDigitoAgencia()
    {
        this._has_cdDigitoAgencia= false;
    } //-- void deleteCdDigitoAgencia() 

    /**
     * Method deleteCdDigitoCpfCnpj
     * 
     */
    public void deleteCdDigitoCpfCnpj()
    {
        this._has_cdDigitoCpfCnpj= false;
    } //-- void deleteCdDigitoCpfCnpj() 

    /**
     * Method deleteCdFinalidade
     * 
     */
    public void deleteCdFinalidade()
    {
        this._has_cdFinalidade= false;
    } //-- void deleteCdFinalidade() 

    /**
     * Method deleteCdPessoaJuridicaNegocio
     * 
     */
    public void deleteCdPessoaJuridicaNegocio()
    {
        this._has_cdPessoaJuridicaNegocio= false;
    } //-- void deleteCdPessoaJuridicaNegocio() 

    /**
     * Method deleteCdTipoConta
     * 
     */
    public void deleteCdTipoConta()
    {
        this._has_cdTipoConta= false;
    } //-- void deleteCdTipoConta() 

    /**
     * Method deleteCdTipoContratoNegocio
     * 
     */
    public void deleteCdTipoContratoNegocio()
    {
        this._has_cdTipoContratoNegocio= false;
    } //-- void deleteCdTipoContratoNegocio() 

    /**
     * Method deleteNrOcorrencias
     * 
     */
    public void deleteNrOcorrencias()
    {
        this._has_nrOcorrencias= false;
    } //-- void deleteNrOcorrencias() 

    /**
     * Method deleteNrSequenciaContratoNegocio
     * 
     */
    public void deleteNrSequenciaContratoNegocio()
    {
        this._has_nrSequenciaContratoNegocio= false;
    } //-- void deleteNrSequenciaContratoNegocio() 

    /**
     * Returns the value of field 'cdAgencia'.
     * 
     * @return int
     * @return the value of field 'cdAgencia'.
     */
    public int getCdAgencia()
    {
        return this._cdAgencia;
    } //-- int getCdAgencia() 

    /**
     * Returns the value of field 'cdBanco'.
     * 
     * @return int
     * @return the value of field 'cdBanco'.
     */
    public int getCdBanco()
    {
        return this._cdBanco;
    } //-- int getCdBanco() 

    /**
     * Returns the value of field 'cdConta'.
     * 
     * @return long
     * @return the value of field 'cdConta'.
     */
    public long getCdConta()
    {
        return this._cdConta;
    } //-- long getCdConta() 

    /**
     * Returns the value of field 'cdControleCpfCnpj'.
     * 
     * @return int
     * @return the value of field 'cdControleCpfCnpj'.
     */
    public int getCdControleCpfCnpj()
    {
        return this._cdControleCpfCnpj;
    } //-- int getCdControleCpfCnpj() 

    /**
     * Returns the value of field 'cdCorpoCfpCnpj'.
     * 
     * @return long
     * @return the value of field 'cdCorpoCfpCnpj'.
     */
    public long getCdCorpoCfpCnpj()
    {
        return this._cdCorpoCfpCnpj;
    } //-- long getCdCorpoCfpCnpj() 

    /**
     * Returns the value of field 'cdDigitoAgencia'.
     * 
     * @return int
     * @return the value of field 'cdDigitoAgencia'.
     */
    public int getCdDigitoAgencia()
    {
        return this._cdDigitoAgencia;
    } //-- int getCdDigitoAgencia() 

    /**
     * Returns the value of field 'cdDigitoConta'.
     * 
     * @return String
     * @return the value of field 'cdDigitoConta'.
     */
    public java.lang.String getCdDigitoConta()
    {
        return this._cdDigitoConta;
    } //-- java.lang.String getCdDigitoConta() 

    /**
     * Returns the value of field 'cdDigitoCpfCnpj'.
     * 
     * @return int
     * @return the value of field 'cdDigitoCpfCnpj'.
     */
    public int getCdDigitoCpfCnpj()
    {
        return this._cdDigitoCpfCnpj;
    } //-- int getCdDigitoCpfCnpj() 

    /**
     * Returns the value of field 'cdFinalidade'.
     * 
     * @return int
     * @return the value of field 'cdFinalidade'.
     */
    public int getCdFinalidade()
    {
        return this._cdFinalidade;
    } //-- int getCdFinalidade() 

    /**
     * Returns the value of field 'cdPessoaJuridicaNegocio'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridicaNegocio'.
     */
    public long getCdPessoaJuridicaNegocio()
    {
        return this._cdPessoaJuridicaNegocio;
    } //-- long getCdPessoaJuridicaNegocio() 

    /**
     * Returns the value of field 'cdTipoConta'.
     * 
     * @return int
     * @return the value of field 'cdTipoConta'.
     */
    public int getCdTipoConta()
    {
        return this._cdTipoConta;
    } //-- int getCdTipoConta() 

    /**
     * Returns the value of field 'cdTipoContratoNegocio'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoNegocio'.
     */
    public int getCdTipoContratoNegocio()
    {
        return this._cdTipoContratoNegocio;
    } //-- int getCdTipoContratoNegocio() 

    /**
     * Returns the value of field 'nrOcorrencias'.
     * 
     * @return int
     * @return the value of field 'nrOcorrencias'.
     */
    public int getNrOcorrencias()
    {
        return this._nrOcorrencias;
    } //-- int getNrOcorrencias() 

    /**
     * Returns the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @return long
     * @return the value of field 'nrSequenciaContratoNegocio'.
     */
    public long getNrSequenciaContratoNegocio()
    {
        return this._nrSequenciaContratoNegocio;
    } //-- long getNrSequenciaContratoNegocio() 

    /**
     * Method hasCdAgencia
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgencia()
    {
        return this._has_cdAgencia;
    } //-- boolean hasCdAgencia() 

    /**
     * Method hasCdBanco
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdBanco()
    {
        return this._has_cdBanco;
    } //-- boolean hasCdBanco() 

    /**
     * Method hasCdConta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdConta()
    {
        return this._has_cdConta;
    } //-- boolean hasCdConta() 

    /**
     * Method hasCdControleCpfCnpj
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdControleCpfCnpj()
    {
        return this._has_cdControleCpfCnpj;
    } //-- boolean hasCdControleCpfCnpj() 

    /**
     * Method hasCdCorpoCfpCnpj
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCorpoCfpCnpj()
    {
        return this._has_cdCorpoCfpCnpj;
    } //-- boolean hasCdCorpoCfpCnpj() 

    /**
     * Method hasCdDigitoAgencia
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdDigitoAgencia()
    {
        return this._has_cdDigitoAgencia;
    } //-- boolean hasCdDigitoAgencia() 

    /**
     * Method hasCdDigitoCpfCnpj
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdDigitoCpfCnpj()
    {
        return this._has_cdDigitoCpfCnpj;
    } //-- boolean hasCdDigitoCpfCnpj() 

    /**
     * Method hasCdFinalidade
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFinalidade()
    {
        return this._has_cdFinalidade;
    } //-- boolean hasCdFinalidade() 

    /**
     * Method hasCdPessoaJuridicaNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridicaNegocio()
    {
        return this._has_cdPessoaJuridicaNegocio;
    } //-- boolean hasCdPessoaJuridicaNegocio() 

    /**
     * Method hasCdTipoConta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoConta()
    {
        return this._has_cdTipoConta;
    } //-- boolean hasCdTipoConta() 

    /**
     * Method hasCdTipoContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoNegocio()
    {
        return this._has_cdTipoContratoNegocio;
    } //-- boolean hasCdTipoContratoNegocio() 

    /**
     * Method hasNrOcorrencias
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrOcorrencias()
    {
        return this._has_nrOcorrencias;
    } //-- boolean hasNrOcorrencias() 

    /**
     * Method hasNrSequenciaContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaContratoNegocio()
    {
        return this._has_nrSequenciaContratoNegocio;
    } //-- boolean hasNrSequenciaContratoNegocio() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdAgencia'.
     * 
     * @param cdAgencia the value of field 'cdAgencia'.
     */
    public void setCdAgencia(int cdAgencia)
    {
        this._cdAgencia = cdAgencia;
        this._has_cdAgencia = true;
    } //-- void setCdAgencia(int) 

    /**
     * Sets the value of field 'cdBanco'.
     * 
     * @param cdBanco the value of field 'cdBanco'.
     */
    public void setCdBanco(int cdBanco)
    {
        this._cdBanco = cdBanco;
        this._has_cdBanco = true;
    } //-- void setCdBanco(int) 

    /**
     * Sets the value of field 'cdConta'.
     * 
     * @param cdConta the value of field 'cdConta'.
     */
    public void setCdConta(long cdConta)
    {
        this._cdConta = cdConta;
        this._has_cdConta = true;
    } //-- void setCdConta(long) 

    /**
     * Sets the value of field 'cdControleCpfCnpj'.
     * 
     * @param cdControleCpfCnpj the value of field
     * 'cdControleCpfCnpj'.
     */
    public void setCdControleCpfCnpj(int cdControleCpfCnpj)
    {
        this._cdControleCpfCnpj = cdControleCpfCnpj;
        this._has_cdControleCpfCnpj = true;
    } //-- void setCdControleCpfCnpj(int) 

    /**
     * Sets the value of field 'cdCorpoCfpCnpj'.
     * 
     * @param cdCorpoCfpCnpj the value of field 'cdCorpoCfpCnpj'.
     */
    public void setCdCorpoCfpCnpj(long cdCorpoCfpCnpj)
    {
        this._cdCorpoCfpCnpj = cdCorpoCfpCnpj;
        this._has_cdCorpoCfpCnpj = true;
    } //-- void setCdCorpoCfpCnpj(long) 

    /**
     * Sets the value of field 'cdDigitoAgencia'.
     * 
     * @param cdDigitoAgencia the value of field 'cdDigitoAgencia'.
     */
    public void setCdDigitoAgencia(int cdDigitoAgencia)
    {
        this._cdDigitoAgencia = cdDigitoAgencia;
        this._has_cdDigitoAgencia = true;
    } //-- void setCdDigitoAgencia(int) 

    /**
     * Sets the value of field 'cdDigitoConta'.
     * 
     * @param cdDigitoConta the value of field 'cdDigitoConta'.
     */
    public void setCdDigitoConta(java.lang.String cdDigitoConta)
    {
        this._cdDigitoConta = cdDigitoConta;
    } //-- void setCdDigitoConta(java.lang.String) 

    /**
     * Sets the value of field 'cdDigitoCpfCnpj'.
     * 
     * @param cdDigitoCpfCnpj the value of field 'cdDigitoCpfCnpj'.
     */
    public void setCdDigitoCpfCnpj(int cdDigitoCpfCnpj)
    {
        this._cdDigitoCpfCnpj = cdDigitoCpfCnpj;
        this._has_cdDigitoCpfCnpj = true;
    } //-- void setCdDigitoCpfCnpj(int) 

    /**
     * Sets the value of field 'cdFinalidade'.
     * 
     * @param cdFinalidade the value of field 'cdFinalidade'.
     */
    public void setCdFinalidade(int cdFinalidade)
    {
        this._cdFinalidade = cdFinalidade;
        this._has_cdFinalidade = true;
    } //-- void setCdFinalidade(int) 

    /**
     * Sets the value of field 'cdPessoaJuridicaNegocio'.
     * 
     * @param cdPessoaJuridicaNegocio the value of field
     * 'cdPessoaJuridicaNegocio'.
     */
    public void setCdPessoaJuridicaNegocio(long cdPessoaJuridicaNegocio)
    {
        this._cdPessoaJuridicaNegocio = cdPessoaJuridicaNegocio;
        this._has_cdPessoaJuridicaNegocio = true;
    } //-- void setCdPessoaJuridicaNegocio(long) 

    /**
     * Sets the value of field 'cdTipoConta'.
     * 
     * @param cdTipoConta the value of field 'cdTipoConta'.
     */
    public void setCdTipoConta(int cdTipoConta)
    {
        this._cdTipoConta = cdTipoConta;
        this._has_cdTipoConta = true;
    } //-- void setCdTipoConta(int) 

    /**
     * Sets the value of field 'cdTipoContratoNegocio'.
     * 
     * @param cdTipoContratoNegocio the value of field
     * 'cdTipoContratoNegocio'.
     */
    public void setCdTipoContratoNegocio(int cdTipoContratoNegocio)
    {
        this._cdTipoContratoNegocio = cdTipoContratoNegocio;
        this._has_cdTipoContratoNegocio = true;
    } //-- void setCdTipoContratoNegocio(int) 

    /**
     * Sets the value of field 'nrOcorrencias'.
     * 
     * @param nrOcorrencias the value of field 'nrOcorrencias'.
     */
    public void setNrOcorrencias(int nrOcorrencias)
    {
        this._nrOcorrencias = nrOcorrencias;
        this._has_nrOcorrencias = true;
    } //-- void setNrOcorrencias(int) 

    /**
     * Sets the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @param nrSequenciaContratoNegocio the value of field
     * 'nrSequenciaContratoNegocio'.
     */
    public void setNrSequenciaContratoNegocio(long nrSequenciaContratoNegocio)
    {
        this._nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
        this._has_nrSequenciaContratoNegocio = true;
    } //-- void setNrSequenciaContratoNegocio(long) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ListarContasVinculadasContratoRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listarcontasvinculadascontrato.request.ListarContasVinculadasContratoRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listarcontasvinculadascontrato.request.ListarContasVinculadasContratoRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listarcontasvinculadascontrato.request.ListarContasVinculadasContratoRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarcontasvinculadascontrato.request.ListarContasVinculadasContratoRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
