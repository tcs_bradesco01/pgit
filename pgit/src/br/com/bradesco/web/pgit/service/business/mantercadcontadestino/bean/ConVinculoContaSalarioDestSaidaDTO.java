/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantercadcontadestino.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantercadcontadestino.bean;

/**
 * Nome: ConVinculoContaSalarioDestSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConVinculoContaSalarioDestSaidaDTO {
    
    /** Atributo codMensagem. */
    private String codMensagem;
    
    /** Atributo mensagem. */
    private String mensagem;
    
    /** Atributo dsNome. */
    private String dsNome;
    
    /** Atributo nrCpfCnpj. */
    private Long nrCpfCnpj;
    
    /** Atributo cdControleCpfCnpj. */
    private Integer cdControleCpfCnpj;
    
    /** Atributo dgCpfCnpj. */
    private Integer dgCpfCnpj;
    
    /** Atributo cdTipoDestinoCredito. */
    private Integer cdTipoDestinoCredito;
    
    /** Atributo cdSituacaoDestinoCredito. */
    private Integer cdSituacaoDestinoCredito; 
    
    /** Atributo cdUsuarioInclusao. */
    private String cdUsuarioInclusao;
    
    /** Atributo cdUsuarioInclusaoExter. */
    private String cdUsuarioInclusaoExter;
    
    /** Atributo dtInclusao. */
    private String dtInclusao;
    
    /** Atributo hrInclusao. */
    private String hrInclusao;
    
    /** Atributo cdOperCanalInclusao. */
    private String cdOperCanalInclusao;
    
    /** Atributo cdTipoCanalInclusao. */
    private Integer cdTipoCanalInclusao;
    
    /** Atributo dsCanalInclusao. */
    private String dsCanalInclusao;
    
    /** Atributo cdUsuarioManutencao. */
    private String cdUsuarioManutencao;
    
    /** Atributo cdUsuarioManutencaoExter. */
    private String cdUsuarioManutencaoExter;
    
    /** Atributo dtManutencao. */
    private String dtManutencao;
    
    /** Atributo hrManutencao. */
    private String hrManutencao;
    
    /** Atributo cdOperCanalManutencao. */
    private String cdOperCanalManutencao;
    
    /** Atributo cdTipoCanalManutencao. */
    private Integer cdTipoCanalManutencao;
    
    /** Atributo dsCanalManutencao. */
    private String dsCanalManutencao;
    
    /** Atributo dsParticipacaoContrato. */
    private String dsParticipacaoContrato;
    
    /** Atributo dsParticipacaoDestino. */
    private String dsParticipacaoDestino;
   
	/**
	 * Con vinculo conta salario dest saida dto.
	 */
	public ConVinculoContaSalarioDestSaidaDTO() {
		super();
	}
	
	/**
	 * Get: cpfCnpj.
	 *
	 * @return cpfCnpj
	 */
	public String getCpfCnpj() {
		if (cdControleCpfCnpj == 0){
			return nrCpfCnpj == 0 ? "" : nrCpfCnpj + "-" + dgCpfCnpj;
		}
		else{
			return nrCpfCnpj == 0 ? "" : nrCpfCnpj + "/" + cdControleCpfCnpj + "-" + dgCpfCnpj;
		}
	}
	
	/**
	 * Get: dataHoraManutencao.
	 *
	 * @return dataHoraManutencao
	 */
	public String getDataHoraManutencao() {
		return dtManutencao.replace('.', '/') + " " + hrManutencao.replace('.', ':');
	}
	
	/**
	 * Get: dataHoraInclusao.
	 *
	 * @return dataHoraInclusao
	 */
	public String getDataHoraInclusao() {
		return dtInclusao.replace('.', '/') + " " + hrInclusao.replace('.', ':');
	}
	
	/**
	 * Get: tpDescricaoCanalManutencao.
	 *
	 * @return tpDescricaoCanalManutencao
	 */
	public String getTpDescricaoCanalManutencao() {
		return cdTipoCanalManutencao == 0 ? "" : this.cdTipoCanalManutencao + "-" + this.dsCanalManutencao;
	}
	
	/**
	 * Get: tpDescricaoCanalInclusao.
	 *
	 * @return tpDescricaoCanalInclusao
	 */
	public String getTpDescricaoCanalInclusao() {
		return cdTipoCanalInclusao == 0 ? "" : this.cdTipoCanalInclusao + "-" + this.dsCanalInclusao;
	}

	/**
	 * Get: cdSituacaoDestinoCredito.
	 *
	 * @return cdSituacaoDestinoCredito
	 */
	public Integer getCdSituacaoDestinoCredito() {
		return cdSituacaoDestinoCredito;
	}

	/**
	 * Set: cdSituacaoDestinoCredito.
	 *
	 * @param cdSituacaoDestinoCredito the cd situacao destino credito
	 */
	public void setCdSituacaoDestinoCredito(Integer cdSituacaoDestinoCredito) {
		this.cdSituacaoDestinoCredito = cdSituacaoDestinoCredito;
	}

	/**
	 * Get: cdTipoCanalInclusao.
	 *
	 * @return cdTipoCanalInclusao
	 */
	public Integer getCdTipoCanalInclusao() {
		return cdTipoCanalInclusao;
	}

	/**
	 * Set: cdTipoCanalInclusao.
	 *
	 * @param cdTipoCanalInclusao the cd tipo canal inclusao
	 */
	public void setCdTipoCanalInclusao(Integer cdTipoCanalInclusao) {
		this.cdTipoCanalInclusao = cdTipoCanalInclusao;
	}

	/**
	 * Get: cdTipoCanalManutencao.
	 *
	 * @return cdTipoCanalManutencao
	 */
	public Integer getCdTipoCanalManutencao() {
		return cdTipoCanalManutencao;
	}

	/**
	 * Set: cdTipoCanalManutencao.
	 *
	 * @param cdTipoCanalManutencao the cd tipo canal manutencao
	 */
	public void setCdTipoCanalManutencao(Integer cdTipoCanalManutencao) {
		this.cdTipoCanalManutencao = cdTipoCanalManutencao;
	}

	/**
	 * Get: cdTipoDestinoCredito.
	 *
	 * @return cdTipoDestinoCredito
	 */
	public Integer getCdTipoDestinoCredito() {
		return cdTipoDestinoCredito;
	}

	/**
	 * Set: cdTipoDestinoCredito.
	 *
	 * @param cdTipoDestinoCredito the cd tipo destino credito
	 */
	public void setCdTipoDestinoCredito(Integer cdTipoDestinoCredito) {
		this.cdTipoDestinoCredito = cdTipoDestinoCredito;
	}

	/**
	 * Get: cdUsuarioInclusao.
	 *
	 * @return cdUsuarioInclusao
	 */
	public String getCdUsuarioInclusao() {
		return cdUsuarioInclusao.equals("0") ? "" : cdUsuarioInclusao;
	}

	/**
	 * Set: cdUsuarioInclusao.
	 *
	 * @param cdUsuarioInclusao the cd usuario inclusao
	 */
	public void setCdUsuarioInclusao(String cdUsuarioInclusao) {
		this.cdUsuarioInclusao = cdUsuarioInclusao;
	}

	/**
	 * Get: cdUsuarioManutencao.
	 *
	 * @return cdUsuarioManutencao
	 */
	public String getCdUsuarioManutencao() {
		return cdUsuarioManutencao.equals("0") ? "" : cdUsuarioManutencao;
	}

	/**
	 * Set: cdUsuarioManutencao.
	 *
	 * @param cdUsuarioManutencao the cd usuario manutencao
	 */
	public void setCdUsuarioManutencao(String cdUsuarioManutencao) {
		this.cdUsuarioManutencao = cdUsuarioManutencao;
	}

	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}

	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}

	/**
	 * Get: dsCanalInclusao.
	 *
	 * @return dsCanalInclusao
	 */
	public String getDsCanalInclusao() {
		return dsCanalInclusao;
	}

	/**
	 * Set: dsCanalInclusao.
	 *
	 * @param dsCanalInclusao the ds canal inclusao
	 */
	public void setDsCanalInclusao(String dsCanalInclusao) {
		this.dsCanalInclusao = dsCanalInclusao;
	}

	/**
	 * Get: dsCanalManutencao.
	 *
	 * @return dsCanalManutencao
	 */
	public String getDsCanalManutencao() {
		return dsCanalManutencao;
	}

	/**
	 * Set: dsCanalManutencao.
	 *
	 * @param dsCanalManutencao the ds canal manutencao
	 */
	public void setDsCanalManutencao(String dsCanalManutencao) {
		this.dsCanalManutencao = dsCanalManutencao;
	}

	/**
	 * Get: dsParticipacaoContrato.
	 *
	 * @return dsParticipacaoContrato
	 */
	public String getDsParticipacaoContrato() {
		return dsParticipacaoContrato;
	}

	/**
	 * Set: dsParticipacaoContrato.
	 *
	 * @param dsParticipacaoContrato the ds participacao contrato
	 */
	public void setDsParticipacaoContrato(String dsParticipacaoContrato) {
		this.dsParticipacaoContrato = dsParticipacaoContrato;
	}

	/**
	 * Get: dsParticipacaoDestino.
	 *
	 * @return dsParticipacaoDestino
	 */
	public String getDsParticipacaoDestino() {
		return dsParticipacaoDestino;
	}

	/**
	 * Set: dsParticipacaoDestino.
	 *
	 * @param dsParticipacaoDestino the ds participacao destino
	 */
	public void setDsParticipacaoDestino(String dsParticipacaoDestino) {
		this.dsParticipacaoDestino = dsParticipacaoDestino;
	}

	/**
	 * Get: dtInclusao.
	 *
	 * @return dtInclusao
	 */
	public String getDtInclusao() {
		return dtInclusao;
	}

	/**
	 * Set: dtInclusao.
	 *
	 * @param dtInclusao the dt inclusao
	 */
	public void setDtInclusao(String dtInclusao) {
		this.dtInclusao = dtInclusao;
	}

	/**
	 * Get: dtManutencao.
	 *
	 * @return dtManutencao
	 */
	public String getDtManutencao() {
		return dtManutencao;
	}

	/**
	 * Set: dtManutencao.
	 *
	 * @param dtManutencao the dt manutencao
	 */
	public void setDtManutencao(String dtManutencao) {
		this.dtManutencao = dtManutencao;
	}

	/**
	 * Get: hrInclusao.
	 *
	 * @return hrInclusao
	 */
	public String getHrInclusao() {
		return hrInclusao;
	}

	/**
	 * Set: hrInclusao.
	 *
	 * @param hrInclusao the hr inclusao
	 */
	public void setHrInclusao(String hrInclusao) {
		this.hrInclusao = hrInclusao;
	}

	/**
	 * Get: hrManutencao.
	 *
	 * @return hrManutencao
	 */
	public String getHrManutencao() {
		return hrManutencao;
	}

	/**
	 * Set: hrManutencao.
	 *
	 * @param hrManutencao the hr manutencao
	 */
	public void setHrManutencao(String hrManutencao) {
		this.hrManutencao = hrManutencao;
	}

	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}

	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	/**
	 * Get: cdOperCanalInclusao.
	 *
	 * @return cdOperCanalInclusao
	 */
	public String getCdOperCanalInclusao() {
		return cdOperCanalInclusao;
	}

	/**
	 * Set: cdOperCanalInclusao.
	 *
	 * @param cdOperCanalInclusao the cd oper canal inclusao
	 */
	public void setCdOperCanalInclusao(String cdOperCanalInclusao) {
		this.cdOperCanalInclusao = cdOperCanalInclusao;
	}

	/**
	 * Get: cdOperCanalManutencao.
	 *
	 * @return cdOperCanalManutencao
	 */
	public String getCdOperCanalManutencao() {
		return cdOperCanalManutencao;
	}

	/**
	 * Set: cdOperCanalManutencao.
	 *
	 * @param cdOperCanalManutencao the cd oper canal manutencao
	 */
	public void setCdOperCanalManutencao(String cdOperCanalManutencao) {
		this.cdOperCanalManutencao = cdOperCanalManutencao;
	}

	/**
	 * Get: cdUsuarioInclusaoExter.
	 *
	 * @return cdUsuarioInclusaoExter
	 */
	public String getCdUsuarioInclusaoExter() {
		return cdUsuarioInclusaoExter;
	}

	/**
	 * Set: cdUsuarioInclusaoExter.
	 *
	 * @param cdUsuarioInclusaoExter the cd usuario inclusao exter
	 */
	public void setCdUsuarioInclusaoExter(String cdUsuarioInclusaoExter) {
		this.cdUsuarioInclusaoExter = cdUsuarioInclusaoExter;
	}

	/**
	 * Get: cdUsuarioManutencaoExter.
	 *
	 * @return cdUsuarioManutencaoExter
	 */
	public String getCdUsuarioManutencaoExter() {
		return cdUsuarioManutencaoExter;
	}

	/**
	 * Set: cdUsuarioManutencaoExter.
	 *
	 * @param cdUsuarioManutencaoExter the cd usuario manutencao exter
	 */
	public void setCdUsuarioManutencaoExter(String cdUsuarioManutencaoExter) {
		this.cdUsuarioManutencaoExter = cdUsuarioManutencaoExter;
	}

	/**
	 * Get: cdControleCpfCnpj.
	 *
	 * @return cdControleCpfCnpj
	 */
	public Integer getCdControleCpfCnpj() {
		return cdControleCpfCnpj;
	}

	/**
	 * Set: cdControleCpfCnpj.
	 *
	 * @param cdControleCpfCnpj the cd controle cpf cnpj
	 */
	public void setCdControleCpfCnpj(Integer cdControleCpfCnpj) {
		this.cdControleCpfCnpj = cdControleCpfCnpj;
	}

	/**
	 * Get: dgCpfCnpj.
	 *
	 * @return dgCpfCnpj
	 */
	public Integer getDgCpfCnpj() {
		return dgCpfCnpj;
	}

	/**
	 * Set: dgCpfCnpj.
	 *
	 * @param dgCpfCnpj the dg cpf cnpj
	 */
	public void setDgCpfCnpj(Integer dgCpfCnpj) {
		this.dgCpfCnpj = dgCpfCnpj;
	}

	/**
	 * Get: dsNome.
	 *
	 * @return dsNome
	 */
	public String getDsNome() {
		return dsNome;
	}

	/**
	 * Set: dsNome.
	 *
	 * @param dsNome the ds nome
	 */
	public void setDsNome(String dsNome) {
		this.dsNome = dsNome;
	}

	/**
	 * Get: nrCpfCnpj.
	 *
	 * @return nrCpfCnpj
	 */
	public Long getNrCpfCnpj() {
		return nrCpfCnpj;
	}

	/**
	 * Set: nrCpfCnpj.
	 *
	 * @param nrCpfCnpj the nr cpf cnpj
	 */
	public void setNrCpfCnpj(Long nrCpfCnpj) {
		this.nrCpfCnpj = nrCpfCnpj;
	}
}
