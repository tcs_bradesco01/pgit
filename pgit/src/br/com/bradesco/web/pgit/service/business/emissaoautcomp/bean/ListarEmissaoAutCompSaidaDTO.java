/*
 * Nome: br.com.bradesco.web.pgit.service.business.emissaoautcomp.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.emissaoautcomp.bean;

/**
 * Nome: ListarEmissaoAutCompSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarEmissaoAutCompSaidaDTO {

	/** Atributo codigoMensagem. */
	private String codigoMensagem;
	
	/** Atributo mensagem. */
	private String mensagem;	
	
	/** Atributo cdTipoServico. */
	private int cdTipoServico; 
	
	/** Atributo dsTipoServico. */
	private String dsTipoServico;
	
	/** Atributo cdModalidadeServico. */
	private int cdModalidadeServico;
	
	/** Atributo dsModalidadeServico. */
	private String dsModalidadeServico;
	
	/** Atributo tipoRelacionamento. */
	private int tipoRelacionamento;
	
	/** Atributo situacao. */
	private int situacao;
	
	/** Atributo check. */
	private boolean check;
	
	/** Atributo cdSituacaoVinculacaoOperacao. */
	private int cdSituacaoVinculacaoOperacao;
	
	/** Atributo dsSituacaoVinculacaoOperacao. */
	private String dsSituacaoVinculacaoOperacao;
	
	/**
	 * Is check.
	 *
	 * @return true, if is check
	 */
	public boolean isCheck() {
		return check;
	}
	
	/**
	 * Set: check.
	 *
	 * @param check the check
	 */
	public void setCheck(boolean check) {
		this.check = check;
	}
	
	/**
	 * Get: cdModalidadeServico.
	 *
	 * @return cdModalidadeServico
	 */
	public int getCdModalidadeServico() {
		return cdModalidadeServico;
	}
	
	/**
	 * Set: cdModalidadeServico.
	 *
	 * @param cdModalidadeServico the cd modalidade servico
	 */
	public void setCdModalidadeServico(int cdModalidadeServico) {
		this.cdModalidadeServico = cdModalidadeServico;
	}
	
	/**
	 * Get: cdTipoServico.
	 *
	 * @return cdTipoServico
	 */
	public int getCdTipoServico() {
		return cdTipoServico;
	}
	
	/**
	 * Set: cdTipoServico.
	 *
	 * @param cdTipoServico the cd tipo servico
	 */
	public void setCdTipoServico(int cdTipoServico) {
		this.cdTipoServico = cdTipoServico;
	}
	
	/**
	 * Get: codigoMensagem.
	 *
	 * @return codigoMensagem
	 */
	public String getCodigoMensagem() {
		return codigoMensagem;
	}
	
	/**
	 * Set: codigoMensagem.
	 *
	 * @param codigoMensagem the codigo mensagem
	 */
	public void setCodigoMensagem(String codigoMensagem) {
		this.codigoMensagem = codigoMensagem;
	}
	
	/**
	 * Get: dsModalidadeServico.
	 *
	 * @return dsModalidadeServico
	 */
	public String getDsModalidadeServico() {
		return dsModalidadeServico;
	}
	
	/**
	 * Set: dsModalidadeServico.
	 *
	 * @param dsModalidadeServico the ds modalidade servico
	 */
	public void setDsModalidadeServico(String dsModalidadeServico) {
		this.dsModalidadeServico = dsModalidadeServico;
	}
	
	/**
	 * Get: dsTipoServico.
	 *
	 * @return dsTipoServico
	 */
	public String getDsTipoServico() {
		return dsTipoServico;
	}
	
	/**
	 * Set: dsTipoServico.
	 *
	 * @param dsTipoServico the ds tipo servico
	 */
	public void setDsTipoServico(String dsTipoServico) {
		this.dsTipoServico = dsTipoServico;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	/**
	 * Get: situacao.
	 *
	 * @return situacao
	 */
	public int getSituacao() {
		return situacao;
	}
	
	/**
	 * Set: situacao.
	 *
	 * @param situacao the situacao
	 */
	public void setSituacao(int situacao) {
		this.situacao = situacao;
	}
	
	/**
	 * Get: tipoRelacionamento.
	 *
	 * @return tipoRelacionamento
	 */
	public int getTipoRelacionamento() {
		return tipoRelacionamento;
	}
	
	/**
	 * Set: tipoRelacionamento.
	 *
	 * @param tipoRelacionamento the tipo relacionamento
	 */
	public void setTipoRelacionamento(int tipoRelacionamento) {
		this.tipoRelacionamento = tipoRelacionamento;
	}
	
	/**
	 * Get: cdSituacaoVinculacaoOperacao.
	 *
	 * @return cdSituacaoVinculacaoOperacao
	 */
	public int getCdSituacaoVinculacaoOperacao() {
		return cdSituacaoVinculacaoOperacao;
	}
	
	/**
	 * Set: cdSituacaoVinculacaoOperacao.
	 *
	 * @param cdSituacaoVinculacaoOperacao the cd situacao vinculacao operacao
	 */
	public void setCdSituacaoVinculacaoOperacao(int cdSituacaoVinculacaoOperacao) {
		this.cdSituacaoVinculacaoOperacao = cdSituacaoVinculacaoOperacao;
	}
	
	/**
	 * Get: dsSituacaoVinculacaoOperacao.
	 *
	 * @return dsSituacaoVinculacaoOperacao
	 */
	public String getDsSituacaoVinculacaoOperacao() {
		return dsSituacaoVinculacaoOperacao;
	}
	
	/**
	 * Set: dsSituacaoVinculacaoOperacao.
	 *
	 * @param dsSituacaoVinculacaoOperacao the ds situacao vinculacao operacao
	 */
	public void setDsSituacaoVinculacaoOperacao(String dsSituacaoVinculacaoOperacao) {
		this.dsSituacaoVinculacaoOperacao = dsSituacaoVinculacaoOperacao;
	}
	
	
	
}
