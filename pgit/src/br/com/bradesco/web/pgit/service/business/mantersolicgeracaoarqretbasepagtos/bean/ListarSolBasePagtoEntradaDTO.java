/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantersolicgeracaoarqretbasepagtos.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantersolicgeracaoarqretbasepagtos.bean;

/**
 * Nome: ListarSolBasePagtoEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarSolBasePagtoEntradaDTO{
    
    /** Atributo numeroOcorrencias. */
    private Integer numeroOcorrencias;
    
    /** Atributo cdPessoaJuridicaContrato. */
    private Long cdPessoaJuridicaContrato;
    
    /** Atributo cdTipoContratoNegocio. */
    private Integer cdTipoContratoNegocio;
    
    /** Atributo nrSequenciaContratoNegocio. */
    private Long nrSequenciaContratoNegocio;
    
    /** Atributo dtInicioInclusaoRemessa. */
    private String dtInicioInclusaoRemessa;
    
    /** Atributo dtFinalInclusaoRemessa. */
    private String dtFinalInclusaoRemessa;
    
    /** Atributo nrSolicitacaoPagamento. */
    private Integer nrSolicitacaoPagamento;
    
    /** Atributo cdSituacaoSolicitacao. */
    private Integer cdSituacaoSolicitacao;
    
    /** Atributo cdOrigemSolicitacao. */
    private Integer cdOrigemSolicitacao;
    
    
	/**
	 * Get: cdOrigemSolicitacao.
	 *
	 * @return cdOrigemSolicitacao
	 */
	public Integer getCdOrigemSolicitacao() {
		return cdOrigemSolicitacao;
	}
	
	/**
	 * Set: cdOrigemSolicitacao.
	 *
	 * @param cdOrigemSolicitacao the cd origem solicitacao
	 */
	public void setCdOrigemSolicitacao(Integer cdOrigemSolicitacao) {
		this.cdOrigemSolicitacao = cdOrigemSolicitacao;
	}
	
	/**
	 * Get: cdPessoaJuridicaContrato.
	 *
	 * @return cdPessoaJuridicaContrato
	 */
	public Long getCdPessoaJuridicaContrato() {
		return cdPessoaJuridicaContrato;
	}
	
	/**
	 * Set: cdPessoaJuridicaContrato.
	 *
	 * @param cdPessoaJuridicaContrato the cd pessoa juridica contrato
	 */
	public void setCdPessoaJuridicaContrato(Long cdPessoaJuridicaContrato) {
		this.cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
	}
	
	/**
	 * Get: cdSituacaoSolicitacao.
	 *
	 * @return cdSituacaoSolicitacao
	 */
	public Integer getCdSituacaoSolicitacao() {
		return cdSituacaoSolicitacao;
	}
	
	/**
	 * Set: cdSituacaoSolicitacao.
	 *
	 * @param cdSituacaoSolicitacao the cd situacao solicitacao
	 */
	public void setCdSituacaoSolicitacao(Integer cdSituacaoSolicitacao) {
		this.cdSituacaoSolicitacao = cdSituacaoSolicitacao;
	}
	
	/**
	 * Get: cdTipoContratoNegocio.
	 *
	 * @return cdTipoContratoNegocio
	 */
	public Integer getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}
	
	/**
	 * Set: cdTipoContratoNegocio.
	 *
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}
	
	/**
	 * Get: dtFinalInclusaoRemessa.
	 *
	 * @return dtFinalInclusaoRemessa
	 */
	public String getDtFinalInclusaoRemessa() {
		return dtFinalInclusaoRemessa;
	}
	
	/**
	 * Set: dtFinalInclusaoRemessa.
	 *
	 * @param dtFinalInclusaoRemessa the dt final inclusao remessa
	 */
	public void setDtFinalInclusaoRemessa(String dtFinalInclusaoRemessa) {
		this.dtFinalInclusaoRemessa = dtFinalInclusaoRemessa;
	}
	
	/**
	 * Get: dtInicioInclusaoRemessa.
	 *
	 * @return dtInicioInclusaoRemessa
	 */
	public String getDtInicioInclusaoRemessa() {
		return dtInicioInclusaoRemessa;
	}
	
	/**
	 * Set: dtInicioInclusaoRemessa.
	 *
	 * @param dtInicioInclusaoRemessa the dt inicio inclusao remessa
	 */
	public void setDtInicioInclusaoRemessa(String dtInicioInclusaoRemessa) {
		this.dtInicioInclusaoRemessa = dtInicioInclusaoRemessa;
	}
	
	/**
	 * Get: nrSequenciaContratoNegocio.
	 *
	 * @return nrSequenciaContratoNegocio
	 */
	public Long getNrSequenciaContratoNegocio() {
		return nrSequenciaContratoNegocio;
	}
	
	/**
	 * Set: nrSequenciaContratoNegocio.
	 *
	 * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
	 */
	public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}
	
	/**
	 * Get: nrSolicitacaoPagamento.
	 *
	 * @return nrSolicitacaoPagamento
	 */
	public Integer getNrSolicitacaoPagamento() {
		return nrSolicitacaoPagamento;
	}
	
	/**
	 * Set: nrSolicitacaoPagamento.
	 *
	 * @param nrSolicitacaoPagamento the nr solicitacao pagamento
	 */
	public void setNrSolicitacaoPagamento(Integer nrSolicitacaoPagamento) {
		this.nrSolicitacaoPagamento = nrSolicitacaoPagamento;
	}
	
	/**
	 * Get: numeroOcorrencias.
	 *
	 * @return numeroOcorrencias
	 */
	public Integer getNumeroOcorrencias() {
		return numeroOcorrencias;
	}
	
	/**
	 * Set: numeroOcorrencias.
	 *
	 * @param numeroOcorrencias the numero ocorrencias
	 */
	public void setNumeroOcorrencias(Integer numeroOcorrencias) {
		this.numeroOcorrencias = numeroOcorrencias;
	}
    
    
}

