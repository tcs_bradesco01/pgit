/*
 * Nome: br.com.bradesco.web.pgit.service.business.manassocmsglayoutmsgsist.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.manassocmsglayoutmsgsist.bean;

/**
 * Nome: DetalharVinculacaoMsgLayoutSistemaEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class DetalharVinculacaoMsgLayoutSistemaEntradaDTO {
    
    /** Atributo cdTipoLayoutArquivo. */
    private Integer cdTipoLayoutArquivo;

    /** Atributo nrMensagemArquivoRetorno. */
    private Integer nrMensagemArquivoRetorno;

    /** Atributo cdMensagemArquivoRetorno. */
    private String cdMensagemArquivoRetorno;

    /** Atributo cdSistema. */
    private String cdSistema;

    /** Atributo nrEventoMensagemNegocio. */
    private Integer nrEventoMensagemNegocio;

    /** Atributo cdRecursoGeradorMensagem. */
    private Integer cdRecursoGeradorMensagem;

    /** Atributo cdIdiomaTextoMensagem. */
    private Integer cdIdiomaTextoMensagem;

    /**
     * Get: cdTipoLayoutArquivo.
     *
     * @return cdTipoLayoutArquivo
     */
    public Integer getCdTipoLayoutArquivo() {
	return cdTipoLayoutArquivo;
    }

    /**
     * Set: cdTipoLayoutArquivo.
     *
     * @param cdTipoLayoutArquivo the cd tipo layout arquivo
     */
    public void setCdTipoLayoutArquivo(Integer cdTipoLayoutArquivo) {
	this.cdTipoLayoutArquivo = cdTipoLayoutArquivo;
    }

    /**
     * Get: nrMensagemArquivoRetorno.
     *
     * @return nrMensagemArquivoRetorno
     */
    public Integer getNrMensagemArquivoRetorno() {
	return nrMensagemArquivoRetorno;
    }

    /**
     * Set: nrMensagemArquivoRetorno.
     *
     * @param nrMensagemArquivoRetorno the nr mensagem arquivo retorno
     */
    public void setNrMensagemArquivoRetorno(Integer nrMensagemArquivoRetorno) {
	this.nrMensagemArquivoRetorno = nrMensagemArquivoRetorno;
    }

    /**
     * Get: cdSistema.
     *
     * @return cdSistema
     */
    public String getCdSistema() {
	return cdSistema;
    }

    /**
     * Set: cdSistema.
     *
     * @param cdSistema the cd sistema
     */
    public void setCdSistema(String cdSistema) {
	this.cdSistema = cdSistema;
    }

    /**
     * Get: nrEventoMensagemNegocio.
     *
     * @return nrEventoMensagemNegocio
     */
    public Integer getNrEventoMensagemNegocio() {
	return nrEventoMensagemNegocio;
    }

    /**
     * Set: nrEventoMensagemNegocio.
     *
     * @param nrEventoMensagemNegocio the nr evento mensagem negocio
     */
    public void setNrEventoMensagemNegocio(Integer nrEventoMensagemNegocio) {
	this.nrEventoMensagemNegocio = nrEventoMensagemNegocio;
    }

    /**
     * Get: cdRecursoGeradorMensagem.
     *
     * @return cdRecursoGeradorMensagem
     */
    public Integer getCdRecursoGeradorMensagem() {
	return cdRecursoGeradorMensagem;
    }

    /**
     * Set: cdRecursoGeradorMensagem.
     *
     * @param cdRecursoGeradorMensagem the cd recurso gerador mensagem
     */
    public void setCdRecursoGeradorMensagem(Integer cdRecursoGeradorMensagem) {
	this.cdRecursoGeradorMensagem = cdRecursoGeradorMensagem;
    }

    /**
     * Get: cdIdiomaTextoMensagem.
     *
     * @return cdIdiomaTextoMensagem
     */
    public Integer getCdIdiomaTextoMensagem() {
	return cdIdiomaTextoMensagem;
    }

    /**
     * Set: cdIdiomaTextoMensagem.
     *
     * @param cdIdiomaTextoMensagem the cd idioma texto mensagem
     */
    public void setCdIdiomaTextoMensagem(Integer cdIdiomaTextoMensagem) {
	this.cdIdiomaTextoMensagem = cdIdiomaTextoMensagem;
    }

    /**
     * Get: cdMensagemArquivoRetorno.
     *
     * @return cdMensagemArquivoRetorno
     */
    public String getCdMensagemArquivoRetorno() {
	return cdMensagemArquivoRetorno;
    }

    /**
     * Set: cdMensagemArquivoRetorno.
     *
     * @param cdMensagemArquivoRetorno the cd mensagem arquivo retorno
     */
    public void setCdMensagemArquivoRetorno(String cdMensagemArquivoRetorno) {
	this.cdMensagemArquivoRetorno = cdMensagemArquivoRetorno;
    }
}