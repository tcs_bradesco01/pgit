/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/interfaz.ftl,v $
 * $Id: interfaz.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: interfaz.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.concontratosmigrados;

import java.util.List;

import br.com.bradesco.web.pgit.service.business.concontratosmigrados.bean.ConContratoMigradoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.concontratosmigrados.bean.ConContratoMigradoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.concontratosmigrados.bean.DetalharContratoMigradoHsbcEntradaDTO;
import br.com.bradesco.web.pgit.service.business.concontratosmigrados.bean.DetalharContratoMigradoHsbcSaidaDTO;
import br.com.bradesco.web.pgit.service.business.concontratosmigrados.bean.RevertContratoMigradoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.concontratosmigrados.bean.RevertContratoMigradoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ConsultarDadosBasicoContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ConsultarDadosBasicoContratoSaidaDTO;

// TODO: Auto-generated Javadoc
/**
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Interface do adaptador: ConContratosMigrados
 * </p>.
 *
 * @comment CODIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public interface IConContratosMigradosService {
	
	/**
	 * Con contrato migrado.
	 *
	 * @param conContratoMigradoEntradaDTO the con contrato migrado entrada dto
	 * @return the list< con contrato migrado saida dt o>
	 */
	List<ConContratoMigradoSaidaDTO> conContratoMigrado(ConContratoMigradoEntradaDTO conContratoMigradoEntradaDTO);
	
	/**
	 * Consultar dados basico contrato.
	 *
	 * @param consultarDadosBasicoContratoEntradaDTO the consultar dados basico contrato entrada dto
	 * @return the consultar dados basico contrato saida dto
	 */
	ConsultarDadosBasicoContratoSaidaDTO consultarDadosBasicoContrato (ConsultarDadosBasicoContratoEntradaDTO consultarDadosBasicoContratoEntradaDTO);
	
	/**
	 * Revert contrato migrado.
	 *
	 * @param entradaDTO the entrada dto
	 * @return the revert contrato migrado saida dto
	 */
	RevertContratoMigradoSaidaDTO revertContratoMigrado(RevertContratoMigradoEntradaDTO entradaDTO);

	/**
	 * Detalhar contrato migrado hsbc.
	 *
	 * @param entrada the entrada
	 * @return the detalhar contrato migrado hsbc saida dto
	 */
	DetalharContratoMigradoHsbcSaidaDTO detalharContratoMigradoHsbc(DetalharContratoMigradoHsbcEntradaDTO entrada);
}

