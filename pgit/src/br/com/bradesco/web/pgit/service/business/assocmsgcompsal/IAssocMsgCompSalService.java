/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/interfaz.ftl,v $
 * $Id: interfaz.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: interfaz.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.assocmsgcompsal;

import java.util.List;

import br.com.bradesco.web.pgit.service.business.assocmsgcompsal.bean.DetalharAssociacaoContratoMsgEntradaDTO;
import br.com.bradesco.web.pgit.service.business.assocmsgcompsal.bean.DetalharAssociacaoContratoMsgSaidaDTO;
import br.com.bradesco.web.pgit.service.business.assocmsgcompsal.bean.ExcluirAssociacaoContratoMsgEntradaDTO;
import br.com.bradesco.web.pgit.service.business.assocmsgcompsal.bean.ExcluirAssociacaoContratoMsgSaidaDTO;
import br.com.bradesco.web.pgit.service.business.assocmsgcompsal.bean.IncluirAssociacaoContratoMsgEntradaDTO;
import br.com.bradesco.web.pgit.service.business.assocmsgcompsal.bean.IncluirAssociacaoContratoMsgSaidaDTO;
import br.com.bradesco.web.pgit.service.business.assocmsgcompsal.bean.ListarAssociacaoContratoMsgEntradaDTO;
import br.com.bradesco.web.pgit.service.business.assocmsgcompsal.bean.ListarAssociacaoContratoMsgSaidaDTO;

/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Interface do adaptador: ManterAssociacaoMensagemComprovanteSalarial
 * </p>
 * 
 * @comment CODIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public interface IAssocMsgCompSalService {
	
	/**
	 * Listar associacao contrato msg.
	 *
	 * @param listarAssociacaoContratoMsgEntradaDTO the listar associacao contrato msg entrada dto
	 * @return the list< listar associacao contrato msg saida dt o>
	 */
	List<ListarAssociacaoContratoMsgSaidaDTO> listarAssociacaoContratoMsg (ListarAssociacaoContratoMsgEntradaDTO listarAssociacaoContratoMsgEntradaDTO);
	
	/**
	 * Incluir associacao contrato msg.
	 *
	 * @param incluirAssociacaoContratoMsgEntradaDTO the incluir associacao contrato msg entrada dto
	 * @return the incluir associacao contrato msg saida dto
	 */
	IncluirAssociacaoContratoMsgSaidaDTO incluirAssociacaoContratoMsg (IncluirAssociacaoContratoMsgEntradaDTO incluirAssociacaoContratoMsgEntradaDTO);
	
	/**
	 * Excluir associacao contrato msg.
	 *
	 * @param excluirAssociacaoContratoMsgEntradaDTO the excluir associacao contrato msg entrada dto
	 * @return the excluir associacao contrato msg saida dto
	 */
	ExcluirAssociacaoContratoMsgSaidaDTO excluirAssociacaoContratoMsg (ExcluirAssociacaoContratoMsgEntradaDTO excluirAssociacaoContratoMsgEntradaDTO);
	
	/**
	 * Detalhar associacao contrato msg.
	 *
	 * @param detalharAssociacaoContratoMsgEntradaDTO the detalhar associacao contrato msg entrada dto
	 * @return the detalhar associacao contrato msg saida dto
	 */
	DetalharAssociacaoContratoMsgSaidaDTO detalharAssociacaoContratoMsg (DetalharAssociacaoContratoMsgEntradaDTO detalharAssociacaoContratoMsgEntradaDTO);

    /**
     * M�todo de exemplo.
     */
    void sampleManterAssociacaoMensagemComprovanteSalarial();
    
    
}

