package br.com.bradesco.web.pgit.service.business.mantersolicantecipacaoproclotepagamento.bean;

import java.math.BigDecimal;

public class ConsultarLotesIncSolicSaidaDTO {
	private Long nrLoteInterno;
	private Integer cdTipoLayoutArquivo;
	private String dsTipoLayoutArquivo;
	private Integer cdProdutoServicoOperacao;
	private String dsProdutoServicoOperacao;
	private Long qtTotalPagamentoLote;
	private BigDecimal vlTotalPagamentoLote;
	private Integer cdIndicadorLoteApto;
	
	public Long getNrLoteInterno() {
		return nrLoteInterno;
	}
	public void setNrLoteInterno(Long nrLoteInterno) {
		this.nrLoteInterno = nrLoteInterno;
	}
	public Integer getCdTipoLayoutArquivo() {
		return cdTipoLayoutArquivo;
	}
	public void setCdTipoLayoutArquivo(Integer cdTipoLayoutArquivo) {
		this.cdTipoLayoutArquivo = cdTipoLayoutArquivo;
	}
	public String getDsTipoLayoutArquivo() {
		return dsTipoLayoutArquivo;
	}
	public void setDsTipoLayoutArquivo(String dsTipoLayoutArquivo) {
		this.dsTipoLayoutArquivo = dsTipoLayoutArquivo;
	}
	public Integer getCdProdutoServicoOperacao() {
		return cdProdutoServicoOperacao;
	}
	public void setCdProdutoServicoOperacao(Integer cdProdutoServicoOperacao) {
		this.cdProdutoServicoOperacao = cdProdutoServicoOperacao;
	}
	public String getDsProdutoServicoOperacao() {
		return dsProdutoServicoOperacao;
	}
	public void setDsProdutoServicoOperacao(String dsProdutoServicoOperacao) {
		this.dsProdutoServicoOperacao = dsProdutoServicoOperacao;
	}
	public Long getQtTotalPagamentoLote() {
		return qtTotalPagamentoLote;
	}
	public void setQtTotalPagamentoLote(Long qtTotalPagamentoLote) {
		this.qtTotalPagamentoLote = qtTotalPagamentoLote;
	}
	public BigDecimal getVlTotalPagamentoLote() {
		return vlTotalPagamentoLote;
	}
	public void setVlTotalPagamentoLote(BigDecimal vlTotalPagamentoLote) {
		this.vlTotalPagamentoLote = vlTotalPagamentoLote;
	}
	public Integer getCdIndicadorLoteApto() {
		return cdIndicadorLoteApto;
	}
	public void setCdIndicadorLoteApto(Integer cdIndicadorLoteApto) {
		this.cdIndicadorLoteApto = cdIndicadorLoteApto;
	}
}
