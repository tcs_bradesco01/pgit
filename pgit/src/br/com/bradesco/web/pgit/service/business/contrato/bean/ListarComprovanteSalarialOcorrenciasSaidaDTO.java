/*
 * Nome: br.com.bradesco.web.pgit.service.business.contrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 19/02/2016
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.contrato.bean;

/**
 * Nome: ListarComprovanteSalarialOcorrenciasSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarComprovanteSalarialOcorrenciasSaidaDTO {

    /** The cd banco. */
    private Integer cdBanco = null;

    /** The ds banco. */
    private String dsBanco = null;

    /** The cd agencia bancaria. */
    private Integer cdAgenciaBancaria = null;

    /** The cd conta corrente. */
    private Long cdContaCorrente = null;

    /** The cd digito conta. */
    private String cdDigitoConta = null;

    /** The cd cpf cnpj. */
    private String cdCpfCnpj = null;

    /** The cd empresa comprovante salarial. */
    private Long cdEmpresaComprovanteSalarial = null;

    /**
     * Instancia um novo ListarComprovanteSalarialOcorrenciasSaidaDTO
     *
     * @see
     */
    public ListarComprovanteSalarialOcorrenciasSaidaDTO() {
        super();
    }

    /**
     * Sets the cd banco.
     *
     * @param cdBanco the cd banco
     */
    public void setCdBanco(Integer cdBanco) {
        this.cdBanco = cdBanco;
    }

    /**
     * Gets the cd banco.
     *
     * @return the cd banco
     */
    public Integer getCdBanco() {
        return this.cdBanco;
    }

    /**
     * Sets the ds banco.
     *
     * @param dsBanco the ds banco
     */
    public void setDsBanco(String dsBanco) {
        this.dsBanco = dsBanco;
    }

    /**
     * Gets the ds banco.
     *
     * @return the ds banco
     */
    public String getDsBanco() {
        return this.dsBanco;
    }

    /**
     * Sets the cd agencia bancaria.
     *
     * @param cdAgenciaBancaria the cd agencia bancaria
     */
    public void setCdAgenciaBancaria(Integer cdAgenciaBancaria) {
        this.cdAgenciaBancaria = cdAgenciaBancaria;
    }

    /**
     * Gets the cd agencia bancaria.
     *
     * @return the cd agencia bancaria
     */
    public Integer getCdAgenciaBancaria() {
        return this.cdAgenciaBancaria;
    }

    /**
     * Sets the cd conta corrente.
     *
     * @param cdContaCorrente the cd conta corrente
     */
    public void setCdContaCorrente(Long cdContaCorrente) {
        this.cdContaCorrente = cdContaCorrente;
    }

    /**
     * Gets the cd conta corrente.
     *
     * @return the cd conta corrente
     */
    public Long getCdContaCorrente() {
        return this.cdContaCorrente;
    }

    /**
     * Sets the cd digito conta.
     *
     * @param cdDigitoConta the cd digito conta
     */
    public void setCdDigitoConta(String cdDigitoConta) {
        this.cdDigitoConta = cdDigitoConta;
    }

    /**
     * Gets the cd digito conta.
     *
     * @return the cd digito conta
     */
    public String getCdDigitoConta() {
        return this.cdDigitoConta;
    }

    /**
     * Sets the cd cpf cnpj.
     *
     * @param cdCpfCnpj the cd cpf cnpj
     */
    public void setCdCpfCnpj(String cdCpfCnpj) {
        this.cdCpfCnpj = cdCpfCnpj;
    }

    /**
     * Gets the cd cpf cnpj.
     *
     * @return the cd cpf cnpj
     */
    public String getCdCpfCnpj() {
        return this.cdCpfCnpj;
    }

    /**
     * Sets the cd empresa comprovante salarial.
     *
     * @param cdEmpresaComprovanteSalarial the cd empresa comprovante salarial
     */
    public void setCdEmpresaComprovanteSalarial(Long cdEmpresaComprovanteSalarial) {
        this.cdEmpresaComprovanteSalarial = cdEmpresaComprovanteSalarial;
    }

    /**
     * Gets the cd empresa comprovante salarial.
     *
     * @return the cd empresa comprovante salarial
     */
    public Long getCdEmpresaComprovanteSalarial() {
        return this.cdEmpresaComprovanteSalarial;
    }

}