/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantercontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantercontrato.bean;

import java.math.BigDecimal;

/**
 * Nome: AlterarCondReajusteTarifaEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class AlterarCondReajusteTarifaEntradaDTO {
	
	/** Atributo cdPessoaJuridicaNegocio. */
	private long cdPessoaJuridicaNegocio;
	
	/** Atributo cdTipoContratoNegocio. */
	private int cdTipoContratoNegocio;
	
	/** Atributo nrSequenciaContratoNegocio. */
	private long nrSequenciaContratoNegocio;
	
	/** Atributo cdServico. */
	private int cdServico;
	
	/** Atributo cdModalidade. */
	private int cdModalidade;
	
	/** Atributo cdRelacionamentoProduto. */
	private int cdRelacionamentoProduto;
	
	/** Atributo cdTipoReajusteTarifa. */
	private int cdTipoReajusteTarifa;
	
	/** Atributo qtMesReajusteTarifa. */
	private int qtMesReajusteTarifa;
	
	/** Atributo cdIndicadorEconomicoReajuste. */
	private int cdIndicadorEconomicoReajuste;
	
	/** Atributo cdPercentualIndice. */
	private BigDecimal cdPercentualIndice;
	
	/** Atributo cdPercentualFlexibilizacao. */
	private BigDecimal cdPercentualFlexibilizacao;
	
	/**
	 * Get: cdIndicadorEconomicoReajuste.
	 *
	 * @return cdIndicadorEconomicoReajuste
	 */
	public int getCdIndicadorEconomicoReajuste() {
		return cdIndicadorEconomicoReajuste;
	}
	
	/**
	 * Set: cdIndicadorEconomicoReajuste.
	 *
	 * @param cdIndicadorEconomicoReajuste the cd indicador economico reajuste
	 */
	public void setCdIndicadorEconomicoReajuste(int cdIndicadorEconomicoReajuste) {
		this.cdIndicadorEconomicoReajuste = cdIndicadorEconomicoReajuste;
	}
	
	/**
	 * Get: cdModalidade.
	 *
	 * @return cdModalidade
	 */
	public int getCdModalidade() {
		return cdModalidade;
	}
	
	/**
	 * Set: cdModalidade.
	 *
	 * @param cdModalidade the cd modalidade
	 */
	public void setCdModalidade(int cdModalidade) {
		this.cdModalidade = cdModalidade;
	}
	
	/**
	 * Get: cdPercentualFlexibilizacao.
	 *
	 * @return cdPercentualFlexibilizacao
	 */
	public BigDecimal getCdPercentualFlexibilizacao() {
		return cdPercentualFlexibilizacao;
	}
	
	/**
	 * Set: cdPercentualFlexibilizacao.
	 *
	 * @param cdPercentualFlexibilizacao the cd percentual flexibilizacao
	 */
	public void setCdPercentualFlexibilizacao(BigDecimal cdPercentualFlexibilizacao) {
		this.cdPercentualFlexibilizacao = cdPercentualFlexibilizacao;
	}
	
	/**
	 * Get: cdPercentualIndice.
	 *
	 * @return cdPercentualIndice
	 */
	public BigDecimal getCdPercentualIndice() {
		return cdPercentualIndice;
	}
	
	/**
	 * Set: cdPercentualIndice.
	 *
	 * @param cdPercentualIndice the cd percentual indice
	 */
	public void setCdPercentualIndice(BigDecimal cdPercentualIndice) {
		this.cdPercentualIndice = cdPercentualIndice;
	}
	
	/**
	 * Get: cdPessoaJuridicaNegocio.
	 *
	 * @return cdPessoaJuridicaNegocio
	 */
	public long getCdPessoaJuridicaNegocio() {
		return cdPessoaJuridicaNegocio;
	}
	
	/**
	 * Set: cdPessoaJuridicaNegocio.
	 *
	 * @param cdPessoaJuridicaNegocio the cd pessoa juridica negocio
	 */
	public void setCdPessoaJuridicaNegocio(long cdPessoaJuridicaNegocio) {
		this.cdPessoaJuridicaNegocio = cdPessoaJuridicaNegocio;
	}
	
	/**
	 * Get: cdRelacionamentoProduto.
	 *
	 * @return cdRelacionamentoProduto
	 */
	public int getCdRelacionamentoProduto() {
		return cdRelacionamentoProduto;
	}
	
	/**
	 * Set: cdRelacionamentoProduto.
	 *
	 * @param cdRelacionamentoProduto the cd relacionamento produto
	 */
	public void setCdRelacionamentoProduto(int cdRelacionamentoProduto) {
		this.cdRelacionamentoProduto = cdRelacionamentoProduto;
	}
	
	/**
	 * Get: cdServico.
	 *
	 * @return cdServico
	 */
	public int getCdServico() {
		return cdServico;
	}
	
	/**
	 * Set: cdServico.
	 *
	 * @param cdServico the cd servico
	 */
	public void setCdServico(int cdServico) {
		this.cdServico = cdServico;
	}
	
	/**
	 * Get: cdTipoContratoNegocio.
	 *
	 * @return cdTipoContratoNegocio
	 */
	public int getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}
	
	/**
	 * Set: cdTipoContratoNegocio.
	 *
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(int cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}
	
	/**
	 * Get: cdTipoReajusteTarifa.
	 *
	 * @return cdTipoReajusteTarifa
	 */
	public int getCdTipoReajusteTarifa() {
		return cdTipoReajusteTarifa;
	}
	
	/**
	 * Set: cdTipoReajusteTarifa.
	 *
	 * @param cdTipoReajusteTarifa the cd tipo reajuste tarifa
	 */
	public void setCdTipoReajusteTarifa(int cdTipoReajusteTarifa) {
		this.cdTipoReajusteTarifa = cdTipoReajusteTarifa;
	}
	
	/**
	 * Get: nrSequenciaContratoNegocio.
	 *
	 * @return nrSequenciaContratoNegocio
	 */
	public long getNrSequenciaContratoNegocio() {
		return nrSequenciaContratoNegocio;
	}
	
	/**
	 * Set: nrSequenciaContratoNegocio.
	 *
	 * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
	 */
	public void setNrSequenciaContratoNegocio(long nrSequenciaContratoNegocio) {
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}
	
	/**
	 * Get: qtMesReajusteTarifa.
	 *
	 * @return qtMesReajusteTarifa
	 */
	public int getQtMesReajusteTarifa() {
		return qtMesReajusteTarifa;
	}
	
	/**
	 * Set: qtMesReajusteTarifa.
	 *
	 * @param qtMesReajusteTarifa the qt mes reajuste tarifa
	 */
	public void setQtMesReajusteTarifa(int qtMesReajusteTarifa) {
		this.qtMesReajusteTarifa = qtMesReajusteTarifa;
	}
	
	
	

}
