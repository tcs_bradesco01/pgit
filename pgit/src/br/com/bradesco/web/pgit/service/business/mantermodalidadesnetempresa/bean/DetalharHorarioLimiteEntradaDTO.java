/*
 * Nome: br.com.bradesco.web.pgit.service.business.funcoesalcada.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantermodalidadesnetempresa.bean;

// TODO: Auto-generated Javadoc
/**
 * Nome: DetalharFuncoesAlcadaEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class DetalharHorarioLimiteEntradaDTO {
	
	/** The cd modalidade. */
	public int cdModalidade;

	/**
	 * Gets the cd modalidade.
	 *
	 * @return the cdModalidade
	 */
	public int getCdModalidade() {
		return cdModalidade;
	}

	/**
	 * Sets the cd modalidade.
	 *
	 * @param cdModalidade the cdModalidade to set
	 */
	public void setCdModalidade(int cdModalidade) {
		this.cdModalidade = cdModalidade;
	}
}
