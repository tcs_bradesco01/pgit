/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listarhistoricolayoutcontrato.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdArquivoRetornoPagamento
     */
    private int _cdArquivoRetornoPagamento = 0;

    /**
     * keeps track of state for field: _cdArquivoRetornoPagamento
     */
    private boolean _has_cdArquivoRetornoPagamento;

    /**
     * Field _dsArquivoRetornoPagamento
     */
    private java.lang.String _dsArquivoRetornoPagamento;

    /**
     * Field _hrInclusaoManutencao
     */
    private java.lang.String _hrInclusaoManutencao;

    /**
     * Field _cdUsuarioManutencao
     */
    private java.lang.String _cdUsuarioManutencao;

    /**
     * Field _dtManutencao
     */
    private java.lang.String _dtManutencao;

    /**
     * Field _hrManutencao
     */
    private java.lang.String _hrManutencao;

    /**
     * Field _dsTipoManutencao
     */
    private java.lang.String _dsTipoManutencao;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarhistoricolayoutcontrato.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdArquivoRetornoPagamento
     * 
     */
    public void deleteCdArquivoRetornoPagamento()
    {
        this._has_cdArquivoRetornoPagamento= false;
    } //-- void deleteCdArquivoRetornoPagamento() 

    /**
     * Returns the value of field 'cdArquivoRetornoPagamento'.
     * 
     * @return int
     * @return the value of field 'cdArquivoRetornoPagamento'.
     */
    public int getCdArquivoRetornoPagamento()
    {
        return this._cdArquivoRetornoPagamento;
    } //-- int getCdArquivoRetornoPagamento() 

    /**
     * Returns the value of field 'cdUsuarioManutencao'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioManutencao'.
     */
    public java.lang.String getCdUsuarioManutencao()
    {
        return this._cdUsuarioManutencao;
    } //-- java.lang.String getCdUsuarioManutencao() 

    /**
     * Returns the value of field 'dsArquivoRetornoPagamento'.
     * 
     * @return String
     * @return the value of field 'dsArquivoRetornoPagamento'.
     */
    public java.lang.String getDsArquivoRetornoPagamento()
    {
        return this._dsArquivoRetornoPagamento;
    } //-- java.lang.String getDsArquivoRetornoPagamento() 

    /**
     * Returns the value of field 'dsTipoManutencao'.
     * 
     * @return String
     * @return the value of field 'dsTipoManutencao'.
     */
    public java.lang.String getDsTipoManutencao()
    {
        return this._dsTipoManutencao;
    } //-- java.lang.String getDsTipoManutencao() 

    /**
     * Returns the value of field 'dtManutencao'.
     * 
     * @return String
     * @return the value of field 'dtManutencao'.
     */
    public java.lang.String getDtManutencao()
    {
        return this._dtManutencao;
    } //-- java.lang.String getDtManutencao() 

    /**
     * Returns the value of field 'hrInclusaoManutencao'.
     * 
     * @return String
     * @return the value of field 'hrInclusaoManutencao'.
     */
    public java.lang.String getHrInclusaoManutencao()
    {
        return this._hrInclusaoManutencao;
    } //-- java.lang.String getHrInclusaoManutencao() 

    /**
     * Returns the value of field 'hrManutencao'.
     * 
     * @return String
     * @return the value of field 'hrManutencao'.
     */
    public java.lang.String getHrManutencao()
    {
        return this._hrManutencao;
    } //-- java.lang.String getHrManutencao() 

    /**
     * Method hasCdArquivoRetornoPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdArquivoRetornoPagamento()
    {
        return this._has_cdArquivoRetornoPagamento;
    } //-- boolean hasCdArquivoRetornoPagamento() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdArquivoRetornoPagamento'.
     * 
     * @param cdArquivoRetornoPagamento the value of field
     * 'cdArquivoRetornoPagamento'.
     */
    public void setCdArquivoRetornoPagamento(int cdArquivoRetornoPagamento)
    {
        this._cdArquivoRetornoPagamento = cdArquivoRetornoPagamento;
        this._has_cdArquivoRetornoPagamento = true;
    } //-- void setCdArquivoRetornoPagamento(int) 

    /**
     * Sets the value of field 'cdUsuarioManutencao'.
     * 
     * @param cdUsuarioManutencao the value of field
     * 'cdUsuarioManutencao'.
     */
    public void setCdUsuarioManutencao(java.lang.String cdUsuarioManutencao)
    {
        this._cdUsuarioManutencao = cdUsuarioManutencao;
    } //-- void setCdUsuarioManutencao(java.lang.String) 

    /**
     * Sets the value of field 'dsArquivoRetornoPagamento'.
     * 
     * @param dsArquivoRetornoPagamento the value of field
     * 'dsArquivoRetornoPagamento'.
     */
    public void setDsArquivoRetornoPagamento(java.lang.String dsArquivoRetornoPagamento)
    {
        this._dsArquivoRetornoPagamento = dsArquivoRetornoPagamento;
    } //-- void setDsArquivoRetornoPagamento(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoManutencao'.
     * 
     * @param dsTipoManutencao the value of field 'dsTipoManutencao'
     */
    public void setDsTipoManutencao(java.lang.String dsTipoManutencao)
    {
        this._dsTipoManutencao = dsTipoManutencao;
    } //-- void setDsTipoManutencao(java.lang.String) 

    /**
     * Sets the value of field 'dtManutencao'.
     * 
     * @param dtManutencao the value of field 'dtManutencao'.
     */
    public void setDtManutencao(java.lang.String dtManutencao)
    {
        this._dtManutencao = dtManutencao;
    } //-- void setDtManutencao(java.lang.String) 

    /**
     * Sets the value of field 'hrInclusaoManutencao'.
     * 
     * @param hrInclusaoManutencao the value of field
     * 'hrInclusaoManutencao'.
     */
    public void setHrInclusaoManutencao(java.lang.String hrInclusaoManutencao)
    {
        this._hrInclusaoManutencao = hrInclusaoManutencao;
    } //-- void setHrInclusaoManutencao(java.lang.String) 

    /**
     * Sets the value of field 'hrManutencao'.
     * 
     * @param hrManutencao the value of field 'hrManutencao'.
     */
    public void setHrManutencao(java.lang.String hrManutencao)
    {
        this._hrManutencao = hrManutencao;
    } //-- void setHrManutencao(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listarhistoricolayoutcontrato.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listarhistoricolayoutcontrato.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listarhistoricolayoutcontrato.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarhistoricolayoutcontrato.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
