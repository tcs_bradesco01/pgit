/*
 * Nome: br.com.bradesco.web.pgit.service.business.endereco.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.endereco.bean;


/**
 * Nome: DatalharEnderecoParticipanteEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class DatalharEnderecoParticipanteEntradaDTO {

    /** Atributo cdpessoaJuridicaContrato. */
    private Long cdpessoaJuridicaContrato;

    /** Atributo cdTipoContratoNegocio. */
    private Integer cdTipoContratoNegocio;

    /** Atributo nrSequenciaContratoNegocio. */
    private Long nrSequenciaContratoNegocio;

    /** Atributo cdTipoParticipacaoPessoa. */
    private Integer cdTipoParticipacaoPessoa;

    /** Atributo cdCorpoCpfCnpj. */
    private Long cdCorpoCpfCnpj;

    /** Atributo cdCnpjContrato. */
    private Integer cdCnpjContrato;

    /** Atributo cdCpfCnpjDigito. */
    private Integer cdCpfCnpjDigito;

    /** Atributo cdPessoa. */
    private Long cdPessoa;

    /** Atributo cdFinalidadeEnderecoContrato. */
    private Integer cdFinalidadeEnderecoContrato;

    /** Atributo cdEspeceEndereco. */
    private Integer cdEspeceEndereco;

    /** Atributo nrSequenciaEnderecoOpcional. */
    private Integer nrSequenciaEnderecoOpcional;

    /** Atributo cdTipoPessoa. */
    private String cdTipoPessoa;

    /**
     * Set: cdpessoaJuridicaContrato.
     *
     * @param cdpessoaJuridicaContrato the cdpessoa juridica contrato
     */
    public void setCdpessoaJuridicaContrato(Long cdpessoaJuridicaContrato) {
        this.cdpessoaJuridicaContrato = cdpessoaJuridicaContrato;
    }

    /**
     * Get: cdpessoaJuridicaContrato.
     *
     * @return cdpessoaJuridicaContrato
     */
    public Long getCdpessoaJuridicaContrato() {
        return this.cdpessoaJuridicaContrato;
    }

    /**
     * Set: cdTipoContratoNegocio.
     *
     * @param cdTipoContratoNegocio the cd tipo contrato negocio
     */
    public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
        this.cdTipoContratoNegocio = cdTipoContratoNegocio;
    }

    /**
     * Get: cdTipoContratoNegocio.
     *
     * @return cdTipoContratoNegocio
     */
    public Integer getCdTipoContratoNegocio() {
        return this.cdTipoContratoNegocio;
    }

    /**
     * Set: nrSequenciaContratoNegocio.
     *
     * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
     */
    public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
        this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
    }

    /**
     * Get: nrSequenciaContratoNegocio.
     *
     * @return nrSequenciaContratoNegocio
     */
    public Long getNrSequenciaContratoNegocio() {
        return this.nrSequenciaContratoNegocio;
    }

    /**
     * Set: cdTipoParticipacaoPessoa.
     *
     * @param cdTipoParticipacaoPessoa the cd tipo participacao pessoa
     */
    public void setCdTipoParticipacaoPessoa(Integer cdTipoParticipacaoPessoa) {
        this.cdTipoParticipacaoPessoa = cdTipoParticipacaoPessoa;
    }

    /**
     * Get: cdTipoParticipacaoPessoa.
     *
     * @return cdTipoParticipacaoPessoa
     */
    public Integer getCdTipoParticipacaoPessoa() {
        return this.cdTipoParticipacaoPessoa;
    }

    /**
     * Set: cdPessoa.
     *
     * @param cdPessoa the cd pessoa
     */
    public void setCdPessoa(Long cdPessoa) {
        this.cdPessoa = cdPessoa;
    }

    /**
     * Get: cdPessoa.
     *
     * @return cdPessoa
     */
    public Long getCdPessoa() {
        return this.cdPessoa;
    }

    /**
     * Set: cdFinalidadeEnderecoContrato.
     *
     * @param cdFinalidadeEnderecoContrato the cd finalidade endereco contrato
     */
    public void setCdFinalidadeEnderecoContrato(Integer cdFinalidadeEnderecoContrato) {
        this.cdFinalidadeEnderecoContrato = cdFinalidadeEnderecoContrato;
    }

    /**
     * Get: cdFinalidadeEnderecoContrato.
     *
     * @return cdFinalidadeEnderecoContrato
     */
    public Integer getCdFinalidadeEnderecoContrato() {
        return this.cdFinalidadeEnderecoContrato;
    }

	/**
	 * Get: cdCorpoCpfCnpj.
	 *
	 * @return cdCorpoCpfCnpj
	 */
	public Long getCdCorpoCpfCnpj() {
		return cdCorpoCpfCnpj;
	}

	/**
	 * Set: cdCorpoCpfCnpj.
	 *
	 * @param cdCorpoCpfCnpj the cd corpo cpf cnpj
	 */
	public void setCdCorpoCpfCnpj(Long cdCorpoCpfCnpj) {
		this.cdCorpoCpfCnpj = cdCorpoCpfCnpj;
	}

	/**
	 * Get: cdCnpjContrato.
	 *
	 * @return cdCnpjContrato
	 */
	public Integer getCdCnpjContrato() {
		return cdCnpjContrato;
	}

	/**
	 * Set: cdCnpjContrato.
	 *
	 * @param cdCnpjContrato the cd cnpj contrato
	 */
	public void setCdCnpjContrato(Integer cdCnpjContrato) {
		this.cdCnpjContrato = cdCnpjContrato;
	}

	/**
	 * Get: cdCpfCnpjDigito.
	 *
	 * @return cdCpfCnpjDigito
	 */
	public Integer getCdCpfCnpjDigito() {
		return cdCpfCnpjDigito;
	}

	/**
	 * Set: cdCpfCnpjDigito.
	 *
	 * @param cdCpfCnpjDigito the cd cpf cnpj digito
	 */
	public void setCdCpfCnpjDigito(Integer cdCpfCnpjDigito) {
		this.cdCpfCnpjDigito = cdCpfCnpjDigito;
	}

	/**
	 * Get: cdEspeceEndereco.
	 *
	 * @return cdEspeceEndereco
	 */
	public Integer getCdEspeceEndereco() {
		return cdEspeceEndereco;
	}

	/**
	 * Set: cdEspeceEndereco.
	 *
	 * @param cdEspeceEndereco the cd espece endereco
	 */
	public void setCdEspeceEndereco(Integer cdEspeceEndereco) {
		this.cdEspeceEndereco = cdEspeceEndereco;
	}

	/**
	 * Get: nrSequenciaEnderecoOpcional.
	 *
	 * @return nrSequenciaEnderecoOpcional
	 */
	public Integer getNrSequenciaEnderecoOpcional() {
		return nrSequenciaEnderecoOpcional;
	}

	/**
	 * Set: nrSequenciaEnderecoOpcional.
	 *
	 * @param nrSequenciaEnderecoOpcional the nr sequencia endereco opcional
	 */
	public void setNrSequenciaEnderecoOpcional(Integer nrSequenciaEnderecoOpcional) {
		this.nrSequenciaEnderecoOpcional = nrSequenciaEnderecoOpcional;
	}

	/**
	 * Get: cdTipoPessoa.
	 *
	 * @return cdTipoPessoa
	 */
	public String getCdTipoPessoa() {
		return cdTipoPessoa;
	}

	/**
	 * Set: cdTipoPessoa.
	 *
	 * @param cdTipoPessoa the cd tipo pessoa
	 */
	public void setCdTipoPessoa(String cdTipoPessoa) {
		this.cdTipoPessoa = cdTipoPessoa;
	}
}