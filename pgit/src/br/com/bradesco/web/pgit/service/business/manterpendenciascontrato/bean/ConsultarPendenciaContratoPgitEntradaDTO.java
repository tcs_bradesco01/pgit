/*
 * Nome: br.com.bradesco.web.pgit.service.business.manterpendenciascontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.manterpendenciascontrato.bean;

/**
 * Nome: ConsultarPendenciaContratoPgitEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarPendenciaContratoPgitEntradaDTO {
	
	
	/** Atributo codPessoaJuridicaContrato. */
	private Long codPessoaJuridicaContrato;
	
	/** Atributo codTipoContratoNegocio. */
	private Integer codTipoContratoNegocio;
	
	/** Atributo nrSeqContratoNegocio. */
	private Long nrSeqContratoNegocio;
	
	/** Atributo codPendenciaPagamentoIntegrado. */
	private Long codPendenciaPagamentoIntegrado;
	
	/** Atributo nrPendenciaContratoNegocio. */
	private Integer nrPendenciaContratoNegocio;
	
	
	/**
	 * Get: codPendenciaPagamentoIntegrado.
	 *
	 * @return codPendenciaPagamentoIntegrado
	 */
	public Long getCodPendenciaPagamentoIntegrado() {
		return codPendenciaPagamentoIntegrado;
	}
	
	/**
	 * Set: codPendenciaPagamentoIntegrado.
	 *
	 * @param codPendenciaPagamentoIntegrado the cod pendencia pagamento integrado
	 */
	public void setCodPendenciaPagamentoIntegrado(Long codPendenciaPagamentoIntegrado) {
		this.codPendenciaPagamentoIntegrado = codPendenciaPagamentoIntegrado;
	}
	
	/**
	 * Get: codPessoaJuridicaContrato.
	 *
	 * @return codPessoaJuridicaContrato
	 */
	public Long getCodPessoaJuridicaContrato() {
		return codPessoaJuridicaContrato;
	}
	
	/**
	 * Set: codPessoaJuridicaContrato.
	 *
	 * @param codPessoaJuridicaContrato the cod pessoa juridica contrato
	 */
	public void setCodPessoaJuridicaContrato(Long codPessoaJuridicaContrato) {
		this.codPessoaJuridicaContrato = codPessoaJuridicaContrato;
	}
	
	/**
	 * Get: codTipoContratoNegocio.
	 *
	 * @return codTipoContratoNegocio
	 */
	public Integer getCodTipoContratoNegocio() {
		return codTipoContratoNegocio;
	}
	
	/**
	 * Set: codTipoContratoNegocio.
	 *
	 * @param codTipoContratoNegocio the cod tipo contrato negocio
	 */
	public void setCodTipoContratoNegocio(Integer codTipoContratoNegocio) {
		this.codTipoContratoNegocio = codTipoContratoNegocio;
	}
	
	/**
	 * Get: nrPendenciaContratoNegocio.
	 *
	 * @return nrPendenciaContratoNegocio
	 */
	public Integer getNrPendenciaContratoNegocio() {
		return nrPendenciaContratoNegocio;
	}
	
	/**
	 * Set: nrPendenciaContratoNegocio.
	 *
	 * @param nrPendenciaContratoNegocio the nr pendencia contrato negocio
	 */
	public void setNrPendenciaContratoNegocio(Integer nrPendenciaContratoNegocio) {
		this.nrPendenciaContratoNegocio = nrPendenciaContratoNegocio;
	}
	
	/**
	 * Get: nrSeqContratoNegocio.
	 *
	 * @return nrSeqContratoNegocio
	 */
	public Long getNrSeqContratoNegocio() {
		return nrSeqContratoNegocio;
	}
	
	/**
	 * Set: nrSeqContratoNegocio.
	 *
	 * @param nrSeqContratoNegocio the nr seq contrato negocio
	 */
	public void setNrSeqContratoNegocio(Long nrSeqContratoNegocio) {
		this.nrSeqContratoNegocio = nrSeqContratoNegocio;
	}
	
}
