/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/implementation.ftl,v $
 * $Id: implementation.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: implementation.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */
 
package br.com.bradesco.web.pgit.service.business.moedasoperacao.impl;

import java.util.ArrayList;
import java.util.List;

import br.com.bradesco.web.pgit.service.business.moedasoperacao.IMoedasOpeConstants;
import br.com.bradesco.web.pgit.service.business.moedasoperacao.IMoedasOperacaoService;
import br.com.bradesco.web.pgit.service.business.moedasoperacao.bean.DetalharMoedasOperacaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.moedasoperacao.bean.DetalharMoedasOperacaoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.moedasoperacao.bean.ExcluirMoedasOperacaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.moedasoperacao.bean.ExcluirMoedasOperacaoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.moedasoperacao.bean.IncluirMoedasOperacaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.moedasoperacao.bean.IncluirMoedasOperacaoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.moedasoperacao.bean.ListarMoedasOperacaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.moedasoperacao.bean.ListarMoedasOperacaoSaidaDTO;
import br.com.bradesco.web.pgit.service.data.pdc.FactoryAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.detalharmoedasarquivo.request.DetalharMoedasArquivoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.detalharmoedasarquivo.response.DetalharMoedasArquivoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.excluirmoedasarquivo.request.ExcluirMoedasArquivoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.excluirmoedasarquivo.response.ExcluirMoedasArquivoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.incluirmoedasarquivo.request.IncluirMoedasArquivoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.incluirmoedasarquivo.response.IncluirMoedasArquivoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarmoedasarquivo.request.ListarMoedasArquivoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarmoedasarquivo.response.ListarMoedasArquivoResponse;


/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Implementa��o do adaptador: ManterMoedasOperacao
 * </p>
 * 
 * @comment C�DIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public class MoedasOpeServiceImpl implements IMoedasOperacaoService {
	
	/** Atributo factoryAdapter. */
	private FactoryAdapter factoryAdapter;
    /**
     * Construtor.
     */
    public MoedasOpeServiceImpl() {
        // TODO: Implementa��o
    }
  
	/**
	 * Get: factoryAdapter.
	 *
	 * @return factoryAdapter
	 */
	public FactoryAdapter getFactoryAdapter() {
		return factoryAdapter;
	}

	/**
	 * Set: factoryAdapter.
	 *
	 * @param factoryAdapter the factory adapter
	 */
	public void setFactoryAdapter(FactoryAdapter factoryAdapter) {
		this.factoryAdapter = factoryAdapter;
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.moedasoperacao.IMoedasOperacaoService#listarMoedasOperacao(br.com.bradesco.web.pgit.service.business.moedasoperacao.bean.ListarMoedasOperacaoEntradaDTO)
	 */
	public List<ListarMoedasOperacaoSaidaDTO> listarMoedasOperacao(ListarMoedasOperacaoEntradaDTO listarMoedasOperacaoEntradaDTO) {
		
		List<ListarMoedasOperacaoSaidaDTO> listaRetorno = new ArrayList<ListarMoedasOperacaoSaidaDTO>();		
		ListarMoedasArquivoRequest request = new ListarMoedasArquivoRequest();
		ListarMoedasArquivoResponse response = new ListarMoedasArquivoResponse();
		
		request.setCdIndicadorEconomicoMoeda(listarMoedasOperacaoEntradaDTO.getCodigoMoedaIndice());
		request.setCdProdutoOperacaoRelacionado(listarMoedasOperacaoEntradaDTO.getModalidadeServico());
		request.setCdProdutoServicoOperacao(listarMoedasOperacaoEntradaDTO.getTipoServico());
		request.setCdRelacionadoProduto(listarMoedasOperacaoEntradaDTO.getTipoRelacionamento());
		request.setQtConsultas(IMoedasOpeConstants.QTDE_CONSULTAS_LISTAR);
		
		
		response = getFactoryAdapter().getListarMoedasArquivoPDCAdapter().invokeProcess(request);

		ListarMoedasOperacaoSaidaDTO saidaDTO;
		for (int i=0; i<response.getOcorrenciasCount();i++){
			saidaDTO = new ListarMoedasOperacaoSaidaDTO();
			saidaDTO.setCodMensagem(response.getCodMensagem());
			saidaDTO.setMensagem(response.getMensagem());
			saidaDTO.setCdTipoServico(response.getOcorrencias(i).getCdProdutoServicoOperacao());
			saidaDTO.setDsTipoServico(response.getOcorrencias(i).getDsProdutoServicoOperacao());
			saidaDTO.setCdModalidadeServico(response.getOcorrencias(i).getCdProdutoOperacaoRelacionado());
			saidaDTO.setDsModalidadeServico(response.getOcorrencias(i).getDsProdutoOperacaoRelacionado());
			saidaDTO.setCdRelacionadoProduto(response.getOcorrencias(i).getCdRelacionadoProduto());
			saidaDTO.setDsEconomicoMoeda(response.getOcorrencias(i).getDsEconomicoMoeda());
			saidaDTO.setCodigoMoedaIndice(response.getOcorrencias(i).getCdIndicadorEconomicoMoeda());
			listaRetorno.add(saidaDTO);
		}
		
		return listaRetorno;
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.moedasoperacao.IMoedasOperacaoService#detalharMoedasOperacao(br.com.bradesco.web.pgit.service.business.moedasoperacao.bean.DetalharMoedasOperacaoEntradaDTO)
	 */
	public DetalharMoedasOperacaoSaidaDTO detalharMoedasOperacao(DetalharMoedasOperacaoEntradaDTO detalharMoedasOperacaoEntradaDTO) {
			
		DetalharMoedasOperacaoSaidaDTO saidaDTO = new DetalharMoedasOperacaoSaidaDTO();
		
		DetalharMoedasArquivoRequest request = new DetalharMoedasArquivoRequest();
		DetalharMoedasArquivoResponse response = new DetalharMoedasArquivoResponse();
	
		/*PGICW327-CPRODT-SERVC-OPER	Tipo Servi�o
			PGICW327-CPRODT-OPER-RLCDO	Modalidade Servi�o
			PGICW327-CRLCTO-PRODT-PRODT	Tipo Relacionamento (campo oculto)
			PGICW327-CINDCD-ECONM-MOEDA	C�digo Moeda/�ndice
		 */
		
		request.setCdProdutoServicoOperacao(detalharMoedasOperacaoEntradaDTO.getTipoServico());
		request.setCdProdutoServicoRelacionado(detalharMoedasOperacaoEntradaDTO.getModalidadeServico());
		request.setCdControleProduto(detalharMoedasOperacaoEntradaDTO.getTipoRelacionamento());
		request.setCdIndicadorEconomicoMoeda(detalharMoedasOperacaoEntradaDTO.getCodigoMoedaIndice());
		request.setQtConsultas(0);
		
		response = getFactoryAdapter().getDetalharMoedasArquivoPDCAdapter().invokeProcess(request);
	
		saidaDTO = new DetalharMoedasOperacaoSaidaDTO();
		saidaDTO.setCodMensagem(response.getCodMensagem());
		saidaDTO.setMensagem(response.getMensagem());
	
		saidaDTO.setCdTipoServico(response.getOcorrencias(0).getCdProdutoServicoOperacao());
		saidaDTO.setDsTipoServico(response.getOcorrencias(0).getDsProdutoServicoOperacao());
		
		saidaDTO.setCdModalidadeServico(response.getOcorrencias(0).getCdProdutoOperacaoRelacionado());
		saidaDTO.setDsModalidadeServico(response.getOcorrencias(0).getDsProdutoOperacaoRelacionado());
		
		saidaDTO.setCdMoedaIndice(response.getOcorrencias(0).getCdIndicadorEconomicoMoeda());
		saidaDTO.setDsMoedaIndice(response.getOcorrencias(0).getDsIndicadorEconomicoMoeda());
		
		
		
		saidaDTO.setCdCanalInclusao(response.getOcorrencias(0).getCdCanalInclusao());
		saidaDTO.setDsCanalInclusao(response.getOcorrencias(0).getDsCanalInclusao()); 
		saidaDTO.setUsuarioInclusao(response.getOcorrencias(0).getCdAutenSegregacaoInclusao());
		saidaDTO.setComplementoInclusao(response.getOcorrencias(0).getNmOperacaoFluxoInclusao());
		saidaDTO.setDataHoraInclusao(response.getOcorrencias(0).getHrInclusaoRegistro());
		
		
		
		saidaDTO.setCdCanalManutencao(response.getOcorrencias(0).getCdCanalManutencao());
		saidaDTO.setDsCanalManutencao(response.getOcorrencias(0).getDsCanalManutencao()); 
		saidaDTO.setUsuarioManutencao(response.getOcorrencias(0).getCdAutenSegregacaoManutencao());
		saidaDTO.setComplementoManutencao(response.getOcorrencias(0).getNmOperacaoFluxoManutencao());
		saidaDTO.setDataHoraManutencao(response.getOcorrencias(0).getHrManutencaoRegistro());
		
		return saidaDTO;
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.moedasoperacao.IMoedasOperacaoService#excluirMoedasOperacao(br.com.bradesco.web.pgit.service.business.moedasoperacao.bean.ExcluirMoedasOperacaoEntradaDTO)
	 */
	public ExcluirMoedasOperacaoSaidaDTO excluirMoedasOperacao(ExcluirMoedasOperacaoEntradaDTO excluirMoedasOperacaoEntradaDTO) {
		ExcluirMoedasOperacaoSaidaDTO excluirMoedasOperacaoSaidaDTO = new ExcluirMoedasOperacaoSaidaDTO();
		ExcluirMoedasArquivoRequest excluirMoedasArquivoRequest = new ExcluirMoedasArquivoRequest();
		ExcluirMoedasArquivoResponse excluirMoedasArquivoResponse = new ExcluirMoedasArquivoResponse();
	
		excluirMoedasArquivoRequest.setCdProdutoservicoOperacao(excluirMoedasOperacaoEntradaDTO.getTipoServico());
		excluirMoedasArquivoRequest.setCdProdutoOperacaoRelacionado(excluirMoedasOperacaoEntradaDTO.getModalidadeServico());
		excluirMoedasArquivoRequest.setCdRelacionadoProduto(excluirMoedasOperacaoEntradaDTO.getTipoRelacionamento());
		excluirMoedasArquivoRequest.setCdIndicadorEconomicoMoeda(excluirMoedasOperacaoEntradaDTO.getCodigoMoedaIndice());
		excluirMoedasArquivoRequest.setQtConsultas(0);
		
	
		excluirMoedasArquivoResponse = getFactoryAdapter().getExcluirMoedasArquivoPDCAdapter().invokeProcess(excluirMoedasArquivoRequest);
	
		excluirMoedasOperacaoSaidaDTO.setCodMensagem(excluirMoedasArquivoResponse.getCodMensagem());
		excluirMoedasOperacaoSaidaDTO.setMensagem(excluirMoedasArquivoResponse.getMensagem());
	
		return excluirMoedasOperacaoSaidaDTO;
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.moedasoperacao.IMoedasOperacaoService#incluirMoedasOperacao(br.com.bradesco.web.pgit.service.business.moedasoperacao.bean.IncluirMoedasOperacaoEntradaDTO)
	 */
	public IncluirMoedasOperacaoSaidaDTO incluirMoedasOperacao(IncluirMoedasOperacaoEntradaDTO incluirMoedasOperacaoEntradaDTO) {
		
		IncluirMoedasOperacaoSaidaDTO incluirMoedasOperacaoSaidaDTO = new IncluirMoedasOperacaoSaidaDTO();
		IncluirMoedasArquivoRequest incluirMoedasArquivoRequest = new IncluirMoedasArquivoRequest();
		IncluirMoedasArquivoResponse incluirMoedasArquivoResponse = new IncluirMoedasArquivoResponse();
	
		
		incluirMoedasArquivoRequest.setCdProdutoServicoOperacao(incluirMoedasOperacaoEntradaDTO.getTipoServico());
		incluirMoedasArquivoRequest.setCdProdutoOperacaoRelacionado(incluirMoedasOperacaoEntradaDTO.getModalidadeServico());
		incluirMoedasArquivoRequest.setCdRelacionadoProduto(incluirMoedasOperacaoEntradaDTO.getTipoRelacionamento());
		incluirMoedasArquivoRequest.setCdIndicadorEconomicoMoeda(incluirMoedasOperacaoEntradaDTO.getCodigoMoedaIndice());
		 
		incluirMoedasArquivoRequest.setQtConsultas(0);
		
		
		incluirMoedasArquivoResponse = getFactoryAdapter().getIncluirMoedasArquivoPDCAdapter().invokeProcess(incluirMoedasArquivoRequest);
		
		incluirMoedasOperacaoSaidaDTO.setCodMensagem(incluirMoedasArquivoResponse.getCodMensagem());
		incluirMoedasOperacaoSaidaDTO.setMensagem(incluirMoedasArquivoResponse.getMensagem());
	
		return incluirMoedasOperacaoSaidaDTO;
	}
    
}

