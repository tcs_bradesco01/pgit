/*
 * Nome: br.com.bradesco.web.pgit.service.business.msgcompsalarial.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.msgcompsalarial.bean;

/**
 * Nome: ListarMsgComprovanteSalrlEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarMsgComprovanteSalrlEntradaDTO {
	
	/** Atributo cdControleMensagemSalarial. */
	private Integer cdControleMensagemSalarial;	
	
	/** Atributo cdTipoComprovanteSalarial. */
	private Integer cdTipoComprovanteSalarial;
	
        /** Atributo cdTipoMensagemSalarial. */
        private Integer cdTipoMensagemSalarial;
        
        /** Atributo cdRestMensagemSalarial. */
        private Integer cdRestMensagemSalarial;
        
	/**
	 * Get: cdRestMensagemSalarial.
	 *
	 * @return cdRestMensagemSalarial
	 */
	public Integer getCdRestMensagemSalarial() {
	    return cdRestMensagemSalarial;
	}
	
	/**
	 * Set: cdRestMensagemSalarial.
	 *
	 * @param cdRestMensagemSalarial the cd rest mensagem salarial
	 */
	public void setCdRestMensagemSalarial(Integer cdRestMensagemSalarial) {
	    this.cdRestMensagemSalarial = cdRestMensagemSalarial;
	}
	
	/**
	 * Get: cdTipoMensagemSalarial.
	 *
	 * @return cdTipoMensagemSalarial
	 */
	public Integer getCdTipoMensagemSalarial() {
	    return cdTipoMensagemSalarial;
	}
	
	/**
	 * Set: cdTipoMensagemSalarial.
	 *
	 * @param cdTipoMensagemSalarial the cd tipo mensagem salarial
	 */
	public void setCdTipoMensagemSalarial(Integer cdTipoMensagemSalarial) {
	    this.cdTipoMensagemSalarial = cdTipoMensagemSalarial;
	}
	
	/**
	 * Get: cdControleMensagemSalarial.
	 *
	 * @return cdControleMensagemSalarial
	 */
	public Integer getCdControleMensagemSalarial() {
		return cdControleMensagemSalarial;
	}
	
	/**
	 * Set: cdControleMensagemSalarial.
	 *
	 * @param cdControleMensagemSalarial the cd controle mensagem salarial
	 */
	public void setCdControleMensagemSalarial(Integer cdControleMensagemSalarial) {
		this.cdControleMensagemSalarial = cdControleMensagemSalarial;
	}
	
	/**
	 * Get: cdTipoComprovanteSalarial.
	 *
	 * @return cdTipoComprovanteSalarial
	 */
	public Integer getCdTipoComprovanteSalarial() {
		return cdTipoComprovanteSalarial;
	}
	
	/**
	 * Set: cdTipoComprovanteSalarial.
	 *
	 * @param cdTipoComprovanteSalarial the cd tipo comprovante salarial
	 */
	public void setCdTipoComprovanteSalarial(Integer cdTipoComprovanteSalarial) {
		this.cdTipoComprovanteSalarial = cdTipoComprovanteSalarial;
	}

		
}
