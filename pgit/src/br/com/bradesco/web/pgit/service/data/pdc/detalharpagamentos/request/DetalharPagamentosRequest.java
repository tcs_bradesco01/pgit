/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.detalharpagamentos.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class DetalharPagamentosRequest.
 * 
 * @version $Revision$ $Date$
 */
public class DetalharPagamentosRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _nrOcorrencias
     */
    private int _nrOcorrencias = 0;

    /**
     * keeps track of state for field: _nrOcorrencias
     */
    private boolean _has_nrOcorrencias;

    /**
     * Field _cdCpfCnpj
     */
    private long _cdCpfCnpj = 0;

    /**
     * keeps track of state for field: _cdCpfCnpj
     */
    private boolean _has_cdCpfCnpj;

    /**
     * Field _cdFilialCpfCnpj
     */
    private int _cdFilialCpfCnpj = 0;

    /**
     * keeps track of state for field: _cdFilialCpfCnpj
     */
    private boolean _has_cdFilialCpfCnpj;

    /**
     * Field _cdDigitoCpfCnpj
     */
    private int _cdDigitoCpfCnpj = 0;

    /**
     * keeps track of state for field: _cdDigitoCpfCnpj
     */
    private boolean _has_cdDigitoCpfCnpj;

    /**
     * Field _cdPessoaJuridicaContrato
     */
    private long _cdPessoaJuridicaContrato = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridicaContrato
     */
    private boolean _has_cdPessoaJuridicaContrato;

    /**
     * Field _cdTipoContratoNegocio
     */
    private int _cdTipoContratoNegocio = 0;

    /**
     * keeps track of state for field: _cdTipoContratoNegocio
     */
    private boolean _has_cdTipoContratoNegocio;

    /**
     * Field _nrSequenciaContratoNegocio
     */
    private long _nrSequenciaContratoNegocio = 0;

    /**
     * keeps track of state for field: _nrSequenciaContratoNegocio
     */
    private boolean _has_nrSequenciaContratoNegocio;

    /**
     * Field _cdUnidadeOrganizacional
     */
    private int _cdUnidadeOrganizacional = 0;

    /**
     * keeps track of state for field: _cdUnidadeOrganizacional
     */
    private boolean _has_cdUnidadeOrganizacional;

    /**
     * Field _cdServico
     */
    private int _cdServico = 0;

    /**
     * keeps track of state for field: _cdServico
     */
    private boolean _has_cdServico;

    /**
     * Field _cdModalidade
     */
    private int _cdModalidade = 0;

    /**
     * keeps track of state for field: _cdModalidade
     */
    private boolean _has_cdModalidade;

    /**
     * Field _cdBancoDebito
     */
    private int _cdBancoDebito = 0;

    /**
     * keeps track of state for field: _cdBancoDebito
     */
    private boolean _has_cdBancoDebito;

    /**
     * Field _cdAgenciaDebito
     */
    private int _cdAgenciaDebito = 0;

    /**
     * keeps track of state for field: _cdAgenciaDebito
     */
    private boolean _has_cdAgenciaDebito;

    /**
     * Field _cdDigitoAgenciaDebito
     */
    private java.lang.String _cdDigitoAgenciaDebito;

    /**
     * Field _cdContaDebito
     */
    private long _cdContaDebito = 0;

    /**
     * keeps track of state for field: _cdContaDebito
     */
    private boolean _has_cdContaDebito;

    /**
     * Field _digitoContaDebito
     */
    private java.lang.String _digitoContaDebito;

    /**
     * Field _cdPessoaContratoDebito
     */
    private long _cdPessoaContratoDebito = 0;

    /**
     * keeps track of state for field: _cdPessoaContratoDebito
     */
    private boolean _has_cdPessoaContratoDebito;

    /**
     * Field _cdTipoContratoDebito
     */
    private int _cdTipoContratoDebito = 0;

    /**
     * keeps track of state for field: _cdTipoContratoDebito
     */
    private boolean _has_cdTipoContratoDebito;

    /**
     * Field _nrSequenciaContratoDebito
     */
    private long _nrSequenciaContratoDebito = 0;

    /**
     * keeps track of state for field: _nrSequenciaContratoDebito
     */
    private boolean _has_nrSequenciaContratoDebito;

    /**
     * Field _dsComplemento
     */
    private java.lang.String _dsComplemento;


      //----------------/
     //- Constructors -/
    //----------------/

    public DetalharPagamentosRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharpagamentos.request.DetalharPagamentosRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdAgenciaDebito
     * 
     */
    public void deleteCdAgenciaDebito()
    {
        this._has_cdAgenciaDebito= false;
    } //-- void deleteCdAgenciaDebito() 

    /**
     * Method deleteCdBancoDebito
     * 
     */
    public void deleteCdBancoDebito()
    {
        this._has_cdBancoDebito= false;
    } //-- void deleteCdBancoDebito() 

    /**
     * Method deleteCdContaDebito
     * 
     */
    public void deleteCdContaDebito()
    {
        this._has_cdContaDebito= false;
    } //-- void deleteCdContaDebito() 

    /**
     * Method deleteCdCpfCnpj
     * 
     */
    public void deleteCdCpfCnpj()
    {
        this._has_cdCpfCnpj= false;
    } //-- void deleteCdCpfCnpj() 

    /**
     * Method deleteCdDigitoCpfCnpj
     * 
     */
    public void deleteCdDigitoCpfCnpj()
    {
        this._has_cdDigitoCpfCnpj= false;
    } //-- void deleteCdDigitoCpfCnpj() 

    /**
     * Method deleteCdFilialCpfCnpj
     * 
     */
    public void deleteCdFilialCpfCnpj()
    {
        this._has_cdFilialCpfCnpj= false;
    } //-- void deleteCdFilialCpfCnpj() 

    /**
     * Method deleteCdModalidade
     * 
     */
    public void deleteCdModalidade()
    {
        this._has_cdModalidade= false;
    } //-- void deleteCdModalidade() 

    /**
     * Method deleteCdPessoaContratoDebito
     * 
     */
    public void deleteCdPessoaContratoDebito()
    {
        this._has_cdPessoaContratoDebito= false;
    } //-- void deleteCdPessoaContratoDebito() 

    /**
     * Method deleteCdPessoaJuridicaContrato
     * 
     */
    public void deleteCdPessoaJuridicaContrato()
    {
        this._has_cdPessoaJuridicaContrato= false;
    } //-- void deleteCdPessoaJuridicaContrato() 

    /**
     * Method deleteCdServico
     * 
     */
    public void deleteCdServico()
    {
        this._has_cdServico= false;
    } //-- void deleteCdServico() 

    /**
     * Method deleteCdTipoContratoDebito
     * 
     */
    public void deleteCdTipoContratoDebito()
    {
        this._has_cdTipoContratoDebito= false;
    } //-- void deleteCdTipoContratoDebito() 

    /**
     * Method deleteCdTipoContratoNegocio
     * 
     */
    public void deleteCdTipoContratoNegocio()
    {
        this._has_cdTipoContratoNegocio= false;
    } //-- void deleteCdTipoContratoNegocio() 

    /**
     * Method deleteCdUnidadeOrganizacional
     * 
     */
    public void deleteCdUnidadeOrganizacional()
    {
        this._has_cdUnidadeOrganizacional= false;
    } //-- void deleteCdUnidadeOrganizacional() 

    /**
     * Method deleteNrOcorrencias
     * 
     */
    public void deleteNrOcorrencias()
    {
        this._has_nrOcorrencias= false;
    } //-- void deleteNrOcorrencias() 

    /**
     * Method deleteNrSequenciaContratoDebito
     * 
     */
    public void deleteNrSequenciaContratoDebito()
    {
        this._has_nrSequenciaContratoDebito= false;
    } //-- void deleteNrSequenciaContratoDebito() 

    /**
     * Method deleteNrSequenciaContratoNegocio
     * 
     */
    public void deleteNrSequenciaContratoNegocio()
    {
        this._has_nrSequenciaContratoNegocio= false;
    } //-- void deleteNrSequenciaContratoNegocio() 

    /**
     * Returns the value of field 'cdAgenciaDebito'.
     * 
     * @return int
     * @return the value of field 'cdAgenciaDebito'.
     */
    public int getCdAgenciaDebito()
    {
        return this._cdAgenciaDebito;
    } //-- int getCdAgenciaDebito() 

    /**
     * Returns the value of field 'cdBancoDebito'.
     * 
     * @return int
     * @return the value of field 'cdBancoDebito'.
     */
    public int getCdBancoDebito()
    {
        return this._cdBancoDebito;
    } //-- int getCdBancoDebito() 

    /**
     * Returns the value of field 'cdContaDebito'.
     * 
     * @return long
     * @return the value of field 'cdContaDebito'.
     */
    public long getCdContaDebito()
    {
        return this._cdContaDebito;
    } //-- long getCdContaDebito() 

    /**
     * Returns the value of field 'cdCpfCnpj'.
     * 
     * @return long
     * @return the value of field 'cdCpfCnpj'.
     */
    public long getCdCpfCnpj()
    {
        return this._cdCpfCnpj;
    } //-- long getCdCpfCnpj() 

    /**
     * Returns the value of field 'cdDigitoAgenciaDebito'.
     * 
     * @return String
     * @return the value of field 'cdDigitoAgenciaDebito'.
     */
    public java.lang.String getCdDigitoAgenciaDebito()
    {
        return this._cdDigitoAgenciaDebito;
    } //-- java.lang.String getCdDigitoAgenciaDebito() 

    /**
     * Returns the value of field 'cdDigitoCpfCnpj'.
     * 
     * @return int
     * @return the value of field 'cdDigitoCpfCnpj'.
     */
    public int getCdDigitoCpfCnpj()
    {
        return this._cdDigitoCpfCnpj;
    } //-- int getCdDigitoCpfCnpj() 

    /**
     * Returns the value of field 'cdFilialCpfCnpj'.
     * 
     * @return int
     * @return the value of field 'cdFilialCpfCnpj'.
     */
    public int getCdFilialCpfCnpj()
    {
        return this._cdFilialCpfCnpj;
    } //-- int getCdFilialCpfCnpj() 

    /**
     * Returns the value of field 'cdModalidade'.
     * 
     * @return int
     * @return the value of field 'cdModalidade'.
     */
    public int getCdModalidade()
    {
        return this._cdModalidade;
    } //-- int getCdModalidade() 

    /**
     * Returns the value of field 'cdPessoaContratoDebito'.
     * 
     * @return long
     * @return the value of field 'cdPessoaContratoDebito'.
     */
    public long getCdPessoaContratoDebito()
    {
        return this._cdPessoaContratoDebito;
    } //-- long getCdPessoaContratoDebito() 

    /**
     * Returns the value of field 'cdPessoaJuridicaContrato'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridicaContrato'.
     */
    public long getCdPessoaJuridicaContrato()
    {
        return this._cdPessoaJuridicaContrato;
    } //-- long getCdPessoaJuridicaContrato() 

    /**
     * Returns the value of field 'cdServico'.
     * 
     * @return int
     * @return the value of field 'cdServico'.
     */
    public int getCdServico()
    {
        return this._cdServico;
    } //-- int getCdServico() 

    /**
     * Returns the value of field 'cdTipoContratoDebito'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoDebito'.
     */
    public int getCdTipoContratoDebito()
    {
        return this._cdTipoContratoDebito;
    } //-- int getCdTipoContratoDebito() 

    /**
     * Returns the value of field 'cdTipoContratoNegocio'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoNegocio'.
     */
    public int getCdTipoContratoNegocio()
    {
        return this._cdTipoContratoNegocio;
    } //-- int getCdTipoContratoNegocio() 

    /**
     * Returns the value of field 'cdUnidadeOrganizacional'.
     * 
     * @return int
     * @return the value of field 'cdUnidadeOrganizacional'.
     */
    public int getCdUnidadeOrganizacional()
    {
        return this._cdUnidadeOrganizacional;
    } //-- int getCdUnidadeOrganizacional() 

    /**
     * Returns the value of field 'digitoContaDebito'.
     * 
     * @return String
     * @return the value of field 'digitoContaDebito'.
     */
    public java.lang.String getDigitoContaDebito()
    {
        return this._digitoContaDebito;
    } //-- java.lang.String getDigitoContaDebito() 

    /**
     * Returns the value of field 'dsComplemento'.
     * 
     * @return String
     * @return the value of field 'dsComplemento'.
     */
    public java.lang.String getDsComplemento()
    {
        return this._dsComplemento;
    } //-- java.lang.String getDsComplemento() 

    /**
     * Returns the value of field 'nrOcorrencias'.
     * 
     * @return int
     * @return the value of field 'nrOcorrencias'.
     */
    public int getNrOcorrencias()
    {
        return this._nrOcorrencias;
    } //-- int getNrOcorrencias() 

    /**
     * Returns the value of field 'nrSequenciaContratoDebito'.
     * 
     * @return long
     * @return the value of field 'nrSequenciaContratoDebito'.
     */
    public long getNrSequenciaContratoDebito()
    {
        return this._nrSequenciaContratoDebito;
    } //-- long getNrSequenciaContratoDebito() 

    /**
     * Returns the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @return long
     * @return the value of field 'nrSequenciaContratoNegocio'.
     */
    public long getNrSequenciaContratoNegocio()
    {
        return this._nrSequenciaContratoNegocio;
    } //-- long getNrSequenciaContratoNegocio() 

    /**
     * Method hasCdAgenciaDebito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgenciaDebito()
    {
        return this._has_cdAgenciaDebito;
    } //-- boolean hasCdAgenciaDebito() 

    /**
     * Method hasCdBancoDebito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdBancoDebito()
    {
        return this._has_cdBancoDebito;
    } //-- boolean hasCdBancoDebito() 

    /**
     * Method hasCdContaDebito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdContaDebito()
    {
        return this._has_cdContaDebito;
    } //-- boolean hasCdContaDebito() 

    /**
     * Method hasCdCpfCnpj
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCpfCnpj()
    {
        return this._has_cdCpfCnpj;
    } //-- boolean hasCdCpfCnpj() 

    /**
     * Method hasCdDigitoCpfCnpj
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdDigitoCpfCnpj()
    {
        return this._has_cdDigitoCpfCnpj;
    } //-- boolean hasCdDigitoCpfCnpj() 

    /**
     * Method hasCdFilialCpfCnpj
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFilialCpfCnpj()
    {
        return this._has_cdFilialCpfCnpj;
    } //-- boolean hasCdFilialCpfCnpj() 

    /**
     * Method hasCdModalidade
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdModalidade()
    {
        return this._has_cdModalidade;
    } //-- boolean hasCdModalidade() 

    /**
     * Method hasCdPessoaContratoDebito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaContratoDebito()
    {
        return this._has_cdPessoaContratoDebito;
    } //-- boolean hasCdPessoaContratoDebito() 

    /**
     * Method hasCdPessoaJuridicaContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridicaContrato()
    {
        return this._has_cdPessoaJuridicaContrato;
    } //-- boolean hasCdPessoaJuridicaContrato() 

    /**
     * Method hasCdServico
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdServico()
    {
        return this._has_cdServico;
    } //-- boolean hasCdServico() 

    /**
     * Method hasCdTipoContratoDebito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoDebito()
    {
        return this._has_cdTipoContratoDebito;
    } //-- boolean hasCdTipoContratoDebito() 

    /**
     * Method hasCdTipoContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoNegocio()
    {
        return this._has_cdTipoContratoNegocio;
    } //-- boolean hasCdTipoContratoNegocio() 

    /**
     * Method hasCdUnidadeOrganizacional
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdUnidadeOrganizacional()
    {
        return this._has_cdUnidadeOrganizacional;
    } //-- boolean hasCdUnidadeOrganizacional() 

    /**
     * Method hasNrOcorrencias
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrOcorrencias()
    {
        return this._has_nrOcorrencias;
    } //-- boolean hasNrOcorrencias() 

    /**
     * Method hasNrSequenciaContratoDebito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaContratoDebito()
    {
        return this._has_nrSequenciaContratoDebito;
    } //-- boolean hasNrSequenciaContratoDebito() 

    /**
     * Method hasNrSequenciaContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaContratoNegocio()
    {
        return this._has_nrSequenciaContratoNegocio;
    } //-- boolean hasNrSequenciaContratoNegocio() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdAgenciaDebito'.
     * 
     * @param cdAgenciaDebito the value of field 'cdAgenciaDebito'.
     */
    public void setCdAgenciaDebito(int cdAgenciaDebito)
    {
        this._cdAgenciaDebito = cdAgenciaDebito;
        this._has_cdAgenciaDebito = true;
    } //-- void setCdAgenciaDebito(int) 

    /**
     * Sets the value of field 'cdBancoDebito'.
     * 
     * @param cdBancoDebito the value of field 'cdBancoDebito'.
     */
    public void setCdBancoDebito(int cdBancoDebito)
    {
        this._cdBancoDebito = cdBancoDebito;
        this._has_cdBancoDebito = true;
    } //-- void setCdBancoDebito(int) 

    /**
     * Sets the value of field 'cdContaDebito'.
     * 
     * @param cdContaDebito the value of field 'cdContaDebito'.
     */
    public void setCdContaDebito(long cdContaDebito)
    {
        this._cdContaDebito = cdContaDebito;
        this._has_cdContaDebito = true;
    } //-- void setCdContaDebito(long) 

    /**
     * Sets the value of field 'cdCpfCnpj'.
     * 
     * @param cdCpfCnpj the value of field 'cdCpfCnpj'.
     */
    public void setCdCpfCnpj(long cdCpfCnpj)
    {
        this._cdCpfCnpj = cdCpfCnpj;
        this._has_cdCpfCnpj = true;
    } //-- void setCdCpfCnpj(long) 

    /**
     * Sets the value of field 'cdDigitoAgenciaDebito'.
     * 
     * @param cdDigitoAgenciaDebito the value of field
     * 'cdDigitoAgenciaDebito'.
     */
    public void setCdDigitoAgenciaDebito(java.lang.String cdDigitoAgenciaDebito)
    {
        this._cdDigitoAgenciaDebito = cdDigitoAgenciaDebito;
    } //-- void setCdDigitoAgenciaDebito(java.lang.String) 

    /**
     * Sets the value of field 'cdDigitoCpfCnpj'.
     * 
     * @param cdDigitoCpfCnpj the value of field 'cdDigitoCpfCnpj'.
     */
    public void setCdDigitoCpfCnpj(int cdDigitoCpfCnpj)
    {
        this._cdDigitoCpfCnpj = cdDigitoCpfCnpj;
        this._has_cdDigitoCpfCnpj = true;
    } //-- void setCdDigitoCpfCnpj(int) 

    /**
     * Sets the value of field 'cdFilialCpfCnpj'.
     * 
     * @param cdFilialCpfCnpj the value of field 'cdFilialCpfCnpj'.
     */
    public void setCdFilialCpfCnpj(int cdFilialCpfCnpj)
    {
        this._cdFilialCpfCnpj = cdFilialCpfCnpj;
        this._has_cdFilialCpfCnpj = true;
    } //-- void setCdFilialCpfCnpj(int) 

    /**
     * Sets the value of field 'cdModalidade'.
     * 
     * @param cdModalidade the value of field 'cdModalidade'.
     */
    public void setCdModalidade(int cdModalidade)
    {
        this._cdModalidade = cdModalidade;
        this._has_cdModalidade = true;
    } //-- void setCdModalidade(int) 

    /**
     * Sets the value of field 'cdPessoaContratoDebito'.
     * 
     * @param cdPessoaContratoDebito the value of field
     * 'cdPessoaContratoDebito'.
     */
    public void setCdPessoaContratoDebito(long cdPessoaContratoDebito)
    {
        this._cdPessoaContratoDebito = cdPessoaContratoDebito;
        this._has_cdPessoaContratoDebito = true;
    } //-- void setCdPessoaContratoDebito(long) 

    /**
     * Sets the value of field 'cdPessoaJuridicaContrato'.
     * 
     * @param cdPessoaJuridicaContrato the value of field
     * 'cdPessoaJuridicaContrato'.
     */
    public void setCdPessoaJuridicaContrato(long cdPessoaJuridicaContrato)
    {
        this._cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
        this._has_cdPessoaJuridicaContrato = true;
    } //-- void setCdPessoaJuridicaContrato(long) 

    /**
     * Sets the value of field 'cdServico'.
     * 
     * @param cdServico the value of field 'cdServico'.
     */
    public void setCdServico(int cdServico)
    {
        this._cdServico = cdServico;
        this._has_cdServico = true;
    } //-- void setCdServico(int) 

    /**
     * Sets the value of field 'cdTipoContratoDebito'.
     * 
     * @param cdTipoContratoDebito the value of field
     * 'cdTipoContratoDebito'.
     */
    public void setCdTipoContratoDebito(int cdTipoContratoDebito)
    {
        this._cdTipoContratoDebito = cdTipoContratoDebito;
        this._has_cdTipoContratoDebito = true;
    } //-- void setCdTipoContratoDebito(int) 

    /**
     * Sets the value of field 'cdTipoContratoNegocio'.
     * 
     * @param cdTipoContratoNegocio the value of field
     * 'cdTipoContratoNegocio'.
     */
    public void setCdTipoContratoNegocio(int cdTipoContratoNegocio)
    {
        this._cdTipoContratoNegocio = cdTipoContratoNegocio;
        this._has_cdTipoContratoNegocio = true;
    } //-- void setCdTipoContratoNegocio(int) 

    /**
     * Sets the value of field 'cdUnidadeOrganizacional'.
     * 
     * @param cdUnidadeOrganizacional the value of field
     * 'cdUnidadeOrganizacional'.
     */
    public void setCdUnidadeOrganizacional(int cdUnidadeOrganizacional)
    {
        this._cdUnidadeOrganizacional = cdUnidadeOrganizacional;
        this._has_cdUnidadeOrganizacional = true;
    } //-- void setCdUnidadeOrganizacional(int) 

    /**
     * Sets the value of field 'digitoContaDebito'.
     * 
     * @param digitoContaDebito the value of field
     * 'digitoContaDebito'.
     */
    public void setDigitoContaDebito(java.lang.String digitoContaDebito)
    {
        this._digitoContaDebito = digitoContaDebito;
    } //-- void setDigitoContaDebito(java.lang.String) 

    /**
     * Sets the value of field 'dsComplemento'.
     * 
     * @param dsComplemento the value of field 'dsComplemento'.
     */
    public void setDsComplemento(java.lang.String dsComplemento)
    {
        this._dsComplemento = dsComplemento;
    } //-- void setDsComplemento(java.lang.String) 

    /**
     * Sets the value of field 'nrOcorrencias'.
     * 
     * @param nrOcorrencias the value of field 'nrOcorrencias'.
     */
    public void setNrOcorrencias(int nrOcorrencias)
    {
        this._nrOcorrencias = nrOcorrencias;
        this._has_nrOcorrencias = true;
    } //-- void setNrOcorrencias(int) 

    /**
     * Sets the value of field 'nrSequenciaContratoDebito'.
     * 
     * @param nrSequenciaContratoDebito the value of field
     * 'nrSequenciaContratoDebito'.
     */
    public void setNrSequenciaContratoDebito(long nrSequenciaContratoDebito)
    {
        this._nrSequenciaContratoDebito = nrSequenciaContratoDebito;
        this._has_nrSequenciaContratoDebito = true;
    } //-- void setNrSequenciaContratoDebito(long) 

    /**
     * Sets the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @param nrSequenciaContratoNegocio the value of field
     * 'nrSequenciaContratoNegocio'.
     */
    public void setNrSequenciaContratoNegocio(long nrSequenciaContratoNegocio)
    {
        this._nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
        this._has_nrSequenciaContratoNegocio = true;
    } //-- void setNrSequenciaContratoNegocio(long) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return DetalharPagamentosRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.detalharpagamentos.request.DetalharPagamentosRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.detalharpagamentos.request.DetalharPagamentosRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.detalharpagamentos.request.DetalharPagamentosRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharpagamentos.request.DetalharPagamentosRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
