/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultarloteretorno.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencia.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencia implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _numeroLoteRetorno
     */
    private long _numeroLoteRetorno = 0;

    /**
     * keeps track of state for field: _numeroLoteRetorno
     */
    private boolean _has_numeroLoteRetorno;

    /**
     * Field _codigoBancoDebito
     */
    private int _codigoBancoDebito = 0;

    /**
     * keeps track of state for field: _codigoBancoDebito
     */
    private boolean _has_codigoBancoDebito;

    /**
     * Field _codigoAgenciaBancariaDebito
     */
    private int _codigoAgenciaBancariaDebito = 0;

    /**
     * keeps track of state for field: _codigoAgenciaBancariaDebito
     */
    private boolean _has_codigoAgenciaBancariaDebito;

    /**
     * Field _contaBancariaDebito
     */
    private long _contaBancariaDebito = 0;

    /**
     * keeps track of state for field: _contaBancariaDebito
     */
    private boolean _has_contaBancariaDebito;

    /**
     * Field _codigoModalidadePagamento
     */
    private int _codigoModalidadePagamento = 0;

    /**
     * keeps track of state for field: _codigoModalidadePagamento
     */
    private boolean _has_codigoModalidadePagamento;

    /**
     * Field _descricaoModalidadePagamento
     */
    private java.lang.String _descricaoModalidadePagamento;

    /**
     * Field _quantidadeRegistroLote
     */
    private long _quantidadeRegistroLote = 0;

    /**
     * keeps track of state for field: _quantidadeRegistroLote
     */
    private boolean _has_quantidadeRegistroLote;

    /**
     * Field _valorRegistroLote
     */
    private double _valorRegistroLote = 0;

    /**
     * keeps track of state for field: _valorRegistroLote
     */
    private boolean _has_valorRegistroLote;

    /**
     * Field _quantidadeRegistroConsistidos
     */
    private long _quantidadeRegistroConsistidos = 0;

    /**
     * keeps track of state for field: _quantidadeRegistroConsistido
     */
    private boolean _has_quantidadeRegistroConsistidos;

    /**
     * Field _valoeRegistroConsistidos
     */
    private double _valoeRegistroConsistidos = 0;

    /**
     * keeps track of state for field: _valoeRegistroConsistidos
     */
    private boolean _has_valoeRegistroConsistidos;

    /**
     * Field _quantidadeRegistroInconsistidos
     */
    private long _quantidadeRegistroInconsistidos = 0;

    /**
     * keeps track of state for field:
     * _quantidadeRegistroInconsistidos
     */
    private boolean _has_quantidadeRegistroInconsistidos;

    /**
     * Field _valorRegistroIncosistidos
     */
    private double _valorRegistroIncosistidos = 0;

    /**
     * keeps track of state for field: _valorRegistroIncosistidos
     */
    private boolean _has_valorRegistroIncosistidos;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencia() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarloteretorno.response.Ocorrencia()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCodigoAgenciaBancariaDebito
     * 
     */
    public void deleteCodigoAgenciaBancariaDebito()
    {
        this._has_codigoAgenciaBancariaDebito= false;
    } //-- void deleteCodigoAgenciaBancariaDebito() 

    /**
     * Method deleteCodigoBancoDebito
     * 
     */
    public void deleteCodigoBancoDebito()
    {
        this._has_codigoBancoDebito= false;
    } //-- void deleteCodigoBancoDebito() 

    /**
     * Method deleteCodigoModalidadePagamento
     * 
     */
    public void deleteCodigoModalidadePagamento()
    {
        this._has_codigoModalidadePagamento= false;
    } //-- void deleteCodigoModalidadePagamento() 

    /**
     * Method deleteContaBancariaDebito
     * 
     */
    public void deleteContaBancariaDebito()
    {
        this._has_contaBancariaDebito= false;
    } //-- void deleteContaBancariaDebito() 

    /**
     * Method deleteNumeroLoteRetorno
     * 
     */
    public void deleteNumeroLoteRetorno()
    {
        this._has_numeroLoteRetorno= false;
    } //-- void deleteNumeroLoteRetorno() 

    /**
     * Method deleteQuantidadeRegistroConsistidos
     * 
     */
    public void deleteQuantidadeRegistroConsistidos()
    {
        this._has_quantidadeRegistroConsistidos= false;
    } //-- void deleteQuantidadeRegistroConsistidos() 

    /**
     * Method deleteQuantidadeRegistroInconsistidos
     * 
     */
    public void deleteQuantidadeRegistroInconsistidos()
    {
        this._has_quantidadeRegistroInconsistidos= false;
    } //-- void deleteQuantidadeRegistroInconsistidos() 

    /**
     * Method deleteQuantidadeRegistroLote
     * 
     */
    public void deleteQuantidadeRegistroLote()
    {
        this._has_quantidadeRegistroLote= false;
    } //-- void deleteQuantidadeRegistroLote() 

    /**
     * Method deleteValoeRegistroConsistidos
     * 
     */
    public void deleteValoeRegistroConsistidos()
    {
        this._has_valoeRegistroConsistidos= false;
    } //-- void deleteValoeRegistroConsistidos() 

    /**
     * Method deleteValorRegistroIncosistidos
     * 
     */
    public void deleteValorRegistroIncosistidos()
    {
        this._has_valorRegistroIncosistidos= false;
    } //-- void deleteValorRegistroIncosistidos() 

    /**
     * Method deleteValorRegistroLote
     * 
     */
    public void deleteValorRegistroLote()
    {
        this._has_valorRegistroLote= false;
    } //-- void deleteValorRegistroLote() 

    /**
     * Returns the value of field 'codigoAgenciaBancariaDebito'.
     * 
     * @return int
     * @return the value of field 'codigoAgenciaBancariaDebito'.
     */
    public int getCodigoAgenciaBancariaDebito()
    {
        return this._codigoAgenciaBancariaDebito;
    } //-- int getCodigoAgenciaBancariaDebito() 

    /**
     * Returns the value of field 'codigoBancoDebito'.
     * 
     * @return int
     * @return the value of field 'codigoBancoDebito'.
     */
    public int getCodigoBancoDebito()
    {
        return this._codigoBancoDebito;
    } //-- int getCodigoBancoDebito() 

    /**
     * Returns the value of field 'codigoModalidadePagamento'.
     * 
     * @return int
     * @return the value of field 'codigoModalidadePagamento'.
     */
    public int getCodigoModalidadePagamento()
    {
        return this._codigoModalidadePagamento;
    } //-- int getCodigoModalidadePagamento() 

    /**
     * Returns the value of field 'contaBancariaDebito'.
     * 
     * @return long
     * @return the value of field 'contaBancariaDebito'.
     */
    public long getContaBancariaDebito()
    {
        return this._contaBancariaDebito;
    } //-- long getContaBancariaDebito() 

    /**
     * Returns the value of field 'descricaoModalidadePagamento'.
     * 
     * @return String
     * @return the value of field 'descricaoModalidadePagamento'.
     */
    public java.lang.String getDescricaoModalidadePagamento()
    {
        return this._descricaoModalidadePagamento;
    } //-- java.lang.String getDescricaoModalidadePagamento() 

    /**
     * Returns the value of field 'numeroLoteRetorno'.
     * 
     * @return long
     * @return the value of field 'numeroLoteRetorno'.
     */
    public long getNumeroLoteRetorno()
    {
        return this._numeroLoteRetorno;
    } //-- long getNumeroLoteRetorno() 

    /**
     * Returns the value of field 'quantidadeRegistroConsistidos'.
     * 
     * @return long
     * @return the value of field 'quantidadeRegistroConsistidos'.
     */
    public long getQuantidadeRegistroConsistidos()
    {
        return this._quantidadeRegistroConsistidos;
    } //-- long getQuantidadeRegistroConsistidos() 

    /**
     * Returns the value of field
     * 'quantidadeRegistroInconsistidos'.
     * 
     * @return long
     * @return the value of field 'quantidadeRegistroInconsistidos'.
     */
    public long getQuantidadeRegistroInconsistidos()
    {
        return this._quantidadeRegistroInconsistidos;
    } //-- long getQuantidadeRegistroInconsistidos() 

    /**
     * Returns the value of field 'quantidadeRegistroLote'.
     * 
     * @return long
     * @return the value of field 'quantidadeRegistroLote'.
     */
    public long getQuantidadeRegistroLote()
    {
        return this._quantidadeRegistroLote;
    } //-- long getQuantidadeRegistroLote() 

    /**
     * Returns the value of field 'valoeRegistroConsistidos'.
     * 
     * @return double
     * @return the value of field 'valoeRegistroConsistidos'.
     */
    public double getValoeRegistroConsistidos()
    {
        return this._valoeRegistroConsistidos;
    } //-- double getValoeRegistroConsistidos() 

    /**
     * Returns the value of field 'valorRegistroIncosistidos'.
     * 
     * @return double
     * @return the value of field 'valorRegistroIncosistidos'.
     */
    public double getValorRegistroIncosistidos()
    {
        return this._valorRegistroIncosistidos;
    } //-- double getValorRegistroIncosistidos() 

    /**
     * Returns the value of field 'valorRegistroLote'.
     * 
     * @return double
     * @return the value of field 'valorRegistroLote'.
     */
    public double getValorRegistroLote()
    {
        return this._valorRegistroLote;
    } //-- double getValorRegistroLote() 

    /**
     * Method hasCodigoAgenciaBancariaDebito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCodigoAgenciaBancariaDebito()
    {
        return this._has_codigoAgenciaBancariaDebito;
    } //-- boolean hasCodigoAgenciaBancariaDebito() 

    /**
     * Method hasCodigoBancoDebito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCodigoBancoDebito()
    {
        return this._has_codigoBancoDebito;
    } //-- boolean hasCodigoBancoDebito() 

    /**
     * Method hasCodigoModalidadePagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCodigoModalidadePagamento()
    {
        return this._has_codigoModalidadePagamento;
    } //-- boolean hasCodigoModalidadePagamento() 

    /**
     * Method hasContaBancariaDebito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasContaBancariaDebito()
    {
        return this._has_contaBancariaDebito;
    } //-- boolean hasContaBancariaDebito() 

    /**
     * Method hasNumeroLoteRetorno
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNumeroLoteRetorno()
    {
        return this._has_numeroLoteRetorno;
    } //-- boolean hasNumeroLoteRetorno() 

    /**
     * Method hasQuantidadeRegistroConsistidos
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQuantidadeRegistroConsistidos()
    {
        return this._has_quantidadeRegistroConsistidos;
    } //-- boolean hasQuantidadeRegistroConsistidos() 

    /**
     * Method hasQuantidadeRegistroInconsistidos
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQuantidadeRegistroInconsistidos()
    {
        return this._has_quantidadeRegistroInconsistidos;
    } //-- boolean hasQuantidadeRegistroInconsistidos() 

    /**
     * Method hasQuantidadeRegistroLote
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQuantidadeRegistroLote()
    {
        return this._has_quantidadeRegistroLote;
    } //-- boolean hasQuantidadeRegistroLote() 

    /**
     * Method hasValoeRegistroConsistidos
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasValoeRegistroConsistidos()
    {
        return this._has_valoeRegistroConsistidos;
    } //-- boolean hasValoeRegistroConsistidos() 

    /**
     * Method hasValorRegistroIncosistidos
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasValorRegistroIncosistidos()
    {
        return this._has_valorRegistroIncosistidos;
    } //-- boolean hasValorRegistroIncosistidos() 

    /**
     * Method hasValorRegistroLote
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasValorRegistroLote()
    {
        return this._has_valorRegistroLote;
    } //-- boolean hasValorRegistroLote() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'codigoAgenciaBancariaDebito'.
     * 
     * @param codigoAgenciaBancariaDebito the value of field
     * 'codigoAgenciaBancariaDebito'.
     */
    public void setCodigoAgenciaBancariaDebito(int codigoAgenciaBancariaDebito)
    {
        this._codigoAgenciaBancariaDebito = codigoAgenciaBancariaDebito;
        this._has_codigoAgenciaBancariaDebito = true;
    } //-- void setCodigoAgenciaBancariaDebito(int) 

    /**
     * Sets the value of field 'codigoBancoDebito'.
     * 
     * @param codigoBancoDebito the value of field
     * 'codigoBancoDebito'.
     */
    public void setCodigoBancoDebito(int codigoBancoDebito)
    {
        this._codigoBancoDebito = codigoBancoDebito;
        this._has_codigoBancoDebito = true;
    } //-- void setCodigoBancoDebito(int) 

    /**
     * Sets the value of field 'codigoModalidadePagamento'.
     * 
     * @param codigoModalidadePagamento the value of field
     * 'codigoModalidadePagamento'.
     */
    public void setCodigoModalidadePagamento(int codigoModalidadePagamento)
    {
        this._codigoModalidadePagamento = codigoModalidadePagamento;
        this._has_codigoModalidadePagamento = true;
    } //-- void setCodigoModalidadePagamento(int) 

    /**
     * Sets the value of field 'contaBancariaDebito'.
     * 
     * @param contaBancariaDebito the value of field
     * 'contaBancariaDebito'.
     */
    public void setContaBancariaDebito(long contaBancariaDebito)
    {
        this._contaBancariaDebito = contaBancariaDebito;
        this._has_contaBancariaDebito = true;
    } //-- void setContaBancariaDebito(long) 

    /**
     * Sets the value of field 'descricaoModalidadePagamento'.
     * 
     * @param descricaoModalidadePagamento the value of field
     * 'descricaoModalidadePagamento'.
     */
    public void setDescricaoModalidadePagamento(java.lang.String descricaoModalidadePagamento)
    {
        this._descricaoModalidadePagamento = descricaoModalidadePagamento;
    } //-- void setDescricaoModalidadePagamento(java.lang.String) 

    /**
     * Sets the value of field 'numeroLoteRetorno'.
     * 
     * @param numeroLoteRetorno the value of field
     * 'numeroLoteRetorno'.
     */
    public void setNumeroLoteRetorno(long numeroLoteRetorno)
    {
        this._numeroLoteRetorno = numeroLoteRetorno;
        this._has_numeroLoteRetorno = true;
    } //-- void setNumeroLoteRetorno(long) 

    /**
     * Sets the value of field 'quantidadeRegistroConsistidos'.
     * 
     * @param quantidadeRegistroConsistidos the value of field
     * 'quantidadeRegistroConsistidos'.
     */
    public void setQuantidadeRegistroConsistidos(long quantidadeRegistroConsistidos)
    {
        this._quantidadeRegistroConsistidos = quantidadeRegistroConsistidos;
        this._has_quantidadeRegistroConsistidos = true;
    } //-- void setQuantidadeRegistroConsistidos(long) 

    /**
     * Sets the value of field 'quantidadeRegistroInconsistidos'.
     * 
     * @param quantidadeRegistroInconsistidos the value of field
     * 'quantidadeRegistroInconsistidos'.
     */
    public void setQuantidadeRegistroInconsistidos(long quantidadeRegistroInconsistidos)
    {
        this._quantidadeRegistroInconsistidos = quantidadeRegistroInconsistidos;
        this._has_quantidadeRegistroInconsistidos = true;
    } //-- void setQuantidadeRegistroInconsistidos(long) 

    /**
     * Sets the value of field 'quantidadeRegistroLote'.
     * 
     * @param quantidadeRegistroLote the value of field
     * 'quantidadeRegistroLote'.
     */
    public void setQuantidadeRegistroLote(long quantidadeRegistroLote)
    {
        this._quantidadeRegistroLote = quantidadeRegistroLote;
        this._has_quantidadeRegistroLote = true;
    } //-- void setQuantidadeRegistroLote(long) 

    /**
     * Sets the value of field 'valoeRegistroConsistidos'.
     * 
     * @param valoeRegistroConsistidos the value of field
     * 'valoeRegistroConsistidos'.
     */
    public void setValoeRegistroConsistidos(double valoeRegistroConsistidos)
    {
        this._valoeRegistroConsistidos = valoeRegistroConsistidos;
        this._has_valoeRegistroConsistidos = true;
    } //-- void setValoeRegistroConsistidos(double) 

    /**
     * Sets the value of field 'valorRegistroIncosistidos'.
     * 
     * @param valorRegistroIncosistidos the value of field
     * 'valorRegistroIncosistidos'.
     */
    public void setValorRegistroIncosistidos(double valorRegistroIncosistidos)
    {
        this._valorRegistroIncosistidos = valorRegistroIncosistidos;
        this._has_valorRegistroIncosistidos = true;
    } //-- void setValorRegistroIncosistidos(double) 

    /**
     * Sets the value of field 'valorRegistroLote'.
     * 
     * @param valorRegistroLote the value of field
     * 'valorRegistroLote'.
     */
    public void setValorRegistroLote(double valorRegistroLote)
    {
        this._valorRegistroLote = valorRegistroLote;
        this._has_valorRegistroLote = true;
    } //-- void setValorRegistroLote(double) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencia
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultarloteretorno.response.Ocorrencia unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultarloteretorno.response.Ocorrencia) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultarloteretorno.response.Ocorrencia.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarloteretorno.response.Ocorrencia unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
