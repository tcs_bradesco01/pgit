/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/interfaz.ftl,v $
 * $Id: interfaz.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: interfaz.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.bloqueardesbloquearservicocontrato;

import br.com.bradesco.web.pgit.service.business.bloqueardesbloquearservicocontrato.bean.BloquearDesbloquearProdServConSaidaDTO;
import br.com.bradesco.web.pgit.service.business.bloqueardesbloquearservicocontrato.bean.BloquearDesbloquearProdServContEntradaDTO;

/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Interface do adaptador: BloquearDesbloquearServicoContrato
 * </p>
 * 
 * @comment CODIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public interface IBloquearDesbloquearServicoContratoService {

	/**
	 * Bloquear desbloquear prod serv contratos.
	 *
	 * @param entrada the entrada
	 * @return the bloquear desbloquear prod serv con saida dto
	 */
	BloquearDesbloquearProdServConSaidaDTO bloquearDesbloquearProdServContratos (BloquearDesbloquearProdServContEntradaDTO entrada);
}

