/*
 * Nome: br.com.bradesco.web.pgit.service.business.consultasolicitacaoestornoop
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.consultasolicitacaoestornoop;

import br.com.bradesco.web.pgit.service.business.consultasolicitacaoestornoop.bean.ConsultaSolicitacaoEstornoOPEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultasolicitacaoestornoop.bean.ConsultaSolicitacaoEstornoOPSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultasolicitacaoestornoop.bean.ConsultarDetalhesSolicitacaoOPEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultasolicitacaoestornoop.bean.ConsultarDetalhesSolicitacaoOPSaidaDTO;

/**
 * Nome: IConsultaSolicitacaoEstornoOPService
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public interface IConsultaSolicitacaoEstornoOPService {

	/**
	 * Cons solicitacao estorno pagtos op.
	 *
	 * @param entradaDTO the entrada dto
	 * @return the consulta solicitacao estorno op saida dto
	 */
	public ConsultaSolicitacaoEstornoOPSaidaDTO consSolicitacaoEstornoPagtosOP(ConsultaSolicitacaoEstornoOPEntradaDTO entradaDTO);

	/**
	 * Consultar detalhes solicitacao op.
	 *
	 * @param entrada the entrada
	 * @return the consultar detalhes solicitacao op saida dto
	 */
	public ConsultarDetalhesSolicitacaoOPSaidaDTO consultarDetalhesSolicitacaoOP(ConsultarDetalhesSolicitacaoOPEntradaDTO entrada);
}
