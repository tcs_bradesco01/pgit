/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listarmotivosituacaoservcontrato.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdMotivoSituacaoProduto
     */
    private int _cdMotivoSituacaoProduto = 0;

    /**
     * keeps track of state for field: _cdMotivoSituacaoProduto
     */
    private boolean _has_cdMotivoSituacaoProduto;

    /**
     * Field _dsMotivoSituacaoProduto
     */
    private java.lang.String _dsMotivoSituacaoProduto;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarmotivosituacaoservcontrato.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdMotivoSituacaoProduto
     * 
     */
    public void deleteCdMotivoSituacaoProduto()
    {
        this._has_cdMotivoSituacaoProduto= false;
    } //-- void deleteCdMotivoSituacaoProduto() 

    /**
     * Returns the value of field 'cdMotivoSituacaoProduto'.
     * 
     * @return int
     * @return the value of field 'cdMotivoSituacaoProduto'.
     */
    public int getCdMotivoSituacaoProduto()
    {
        return this._cdMotivoSituacaoProduto;
    } //-- int getCdMotivoSituacaoProduto() 

    /**
     * Returns the value of field 'dsMotivoSituacaoProduto'.
     * 
     * @return String
     * @return the value of field 'dsMotivoSituacaoProduto'.
     */
    public java.lang.String getDsMotivoSituacaoProduto()
    {
        return this._dsMotivoSituacaoProduto;
    } //-- java.lang.String getDsMotivoSituacaoProduto() 

    /**
     * Method hasCdMotivoSituacaoProduto
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMotivoSituacaoProduto()
    {
        return this._has_cdMotivoSituacaoProduto;
    } //-- boolean hasCdMotivoSituacaoProduto() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdMotivoSituacaoProduto'.
     * 
     * @param cdMotivoSituacaoProduto the value of field
     * 'cdMotivoSituacaoProduto'.
     */
    public void setCdMotivoSituacaoProduto(int cdMotivoSituacaoProduto)
    {
        this._cdMotivoSituacaoProduto = cdMotivoSituacaoProduto;
        this._has_cdMotivoSituacaoProduto = true;
    } //-- void setCdMotivoSituacaoProduto(int) 

    /**
     * Sets the value of field 'dsMotivoSituacaoProduto'.
     * 
     * @param dsMotivoSituacaoProduto the value of field
     * 'dsMotivoSituacaoProduto'.
     */
    public void setDsMotivoSituacaoProduto(java.lang.String dsMotivoSituacaoProduto)
    {
        this._dsMotivoSituacaoProduto = dsMotivoSituacaoProduto;
    } //-- void setDsMotivoSituacaoProduto(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listarmotivosituacaoservcontrato.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listarmotivosituacaoservcontrato.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listarmotivosituacaoservcontrato.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarmotivosituacaoservcontrato.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
