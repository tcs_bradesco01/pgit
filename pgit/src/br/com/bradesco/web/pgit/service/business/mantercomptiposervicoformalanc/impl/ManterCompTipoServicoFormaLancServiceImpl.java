/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/implementation.ftl,v $
 * $Id: implementation.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: implementation.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */
 
package br.com.bradesco.web.pgit.service.business.mantercomptiposervicoformalanc.impl;

import java.util.ArrayList;
import java.util.List;

import br.com.bradesco.web.pgit.service.business.mantercomptiposervicoformalanc.IManterCompTipoServicoFormaLancService;
import br.com.bradesco.web.pgit.service.business.mantercomptiposervicoformalanc.bean.ConsultarServicoLancamentoCnabEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercomptiposervicoformalanc.bean.ConsultarServicoLancamentoCnabSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercomptiposervicoformalanc.bean.DetalharServicoLancamentoCnabEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercomptiposervicoformalanc.bean.DetalharServicoLancamentoCnabSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercomptiposervicoformalanc.bean.ExcluirServicoLancamentoCnabEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercomptiposervicoformalanc.bean.ExcluirServicoLancamentoCnabSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercomptiposervicoformalanc.bean.IncluirServicoLancamentoCnabEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercomptiposervicoformalanc.bean.IncluirServicoLancamentoCnabSaidaDTO;
import br.com.bradesco.web.pgit.service.data.pdc.FactoryAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarservicolancamentocnab.request.ConsultarServicoLancamentoCnabRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarservicolancamentocnab.response.ConsultarServicoLancamentoCnabResponse;
import br.com.bradesco.web.pgit.service.data.pdc.detalharservicolancamentocnab.request.DetalharServicoLancamentoCnabRequest;
import br.com.bradesco.web.pgit.service.data.pdc.detalharservicolancamentocnab.response.DetalharServicoLancamentoCnabResponse;
import br.com.bradesco.web.pgit.service.data.pdc.excluirservicolancamentocnab.request.ExcluirServicoLancamentoCnabRequest;
import br.com.bradesco.web.pgit.service.data.pdc.excluirservicolancamentocnab.response.ExcluirServicoLancamentoCnabResponse;
import br.com.bradesco.web.pgit.service.data.pdc.incluirservicolancamentocnab.request.IncluirServicoLancamentoCnabRequest;
import br.com.bradesco.web.pgit.service.data.pdc.incluirservicolancamentocnab.response.IncluirServicoLancamentoCnabResponse;
import br.com.bradesco.web.pgit.utils.PgitUtil;


/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Implementa��o do adaptador: ManterCompTipoServicoFormaLanc
 * </p>
 * 
 * @comment C�DIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public class ManterCompTipoServicoFormaLancServiceImpl implements IManterCompTipoServicoFormaLancService {
	
	/** Atributo factoryAdapter. */
	private FactoryAdapter factoryAdapter;
	
	
    /**
     * Get: factoryAdapter.
     *
     * @return factoryAdapter
     */
    public FactoryAdapter getFactoryAdapter() {
		return factoryAdapter;
	}


	/**
	 * Set: factoryAdapter.
	 *
	 * @param factoryAdapter the factory adapter
	 */
	public void setFactoryAdapter(FactoryAdapter factoryAdapter) {
		this.factoryAdapter = factoryAdapter;
	}


	/**
     * Construtor.
     */
    public ManterCompTipoServicoFormaLancServiceImpl() {
        
    }
    
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.mantercomptiposervicoformalanc.IManterCompTipoServicoFormaLancService#consultarServicoLancamentoCnab(br.com.bradesco.web.pgit.service.business.mantercomptiposervicoformalanc.bean.ConsultarServicoLancamentoCnabEntradaDTO)
	 */
	public List<ConsultarServicoLancamentoCnabSaidaDTO> consultarServicoLancamentoCnab(ConsultarServicoLancamentoCnabEntradaDTO entrada){
		List<ConsultarServicoLancamentoCnabSaidaDTO> listaSaida = new ArrayList<ConsultarServicoLancamentoCnabSaidaDTO>();
		ConsultarServicoLancamentoCnabRequest request = new ConsultarServicoLancamentoCnabRequest();
		ConsultarServicoLancamentoCnabResponse response = new ConsultarServicoLancamentoCnabResponse();

		request.setNrOcorrencias(50);
		request.setCdFormaLancamentoCnab(PgitUtil.verificaIntegerNulo(entrada.getCdFormaLancamentoCnab()));
		request.setCdTipoServicoCnab(PgitUtil.verificaIntegerNulo(entrada.getCdTipoServicoCnab()));
		request.setCdFormaLiquidacao(PgitUtil.verificaIntegerNulo(entrada.getCdFormaLiquidacao()));
		request.setCdProdutoServicoOperacao(PgitUtil.verificaIntegerNulo(entrada.getCdProdutoServicoOperacao()));
		request.setCdProdutoOperacaoRelacionado(PgitUtil.verificaIntegerNulo(entrada.getCdProdutoOperacaoRelacionado()));

		response = getFactoryAdapter().getConsultarServicoLancamentoCnabPDCAdapter().invokeProcess(request);

		for(int i=0;i<response.getOcorrenciasCount();i++){
			ConsultarServicoLancamentoCnabSaidaDTO saida = new ConsultarServicoLancamentoCnabSaidaDTO();
			saida.setCodMensagem(response.getCodMensagem());
			saida.setMensagem(response.getMensagem());
			saida.setNumeroLinhas(response.getNumeroLinhas());
			saida.setCdFormaLancamentoCnab(response.getOcorrencias(i).getCdFormaLancamentoCnab());
			saida.setCdTipoServicoCnab(response.getOcorrencias(i).getCdTipoServicoCnab());
			saida.setCdFormaLiquidacao(response.getOcorrencias(i).getCdFormaLiquidacao());
			saida.setCdProdutoServicoOperacao(response.getOcorrencias(i).getCdProdutoServicoOperacao());
			saida.setCdProdutoOperacaoRelacionado(response.getOcorrencias(i).getCdProdutoOperacaoRelacionado());
			
			saida.setDsFormaLancamentoCnab(response.getOcorrencias(i).getDsFormaLancamentoCnab());
			saida.setDsTipoServicoCnab(response.getOcorrencias(i).getDsTipoServicoCnab());
			saida.setDsFormaLiquidacao(response.getOcorrencias(i).getDsFormaLiquidacao());
			saida.setDsProdutoServicoOperacao(response.getOcorrencias(i).getDsProdutoServicoOperacao());        
			saida.setDsProdutoServicoRelacionado(response.getOcorrencias(i).getDsProdutoServicoRelacionado());
			

			listaSaida.add(saida);
		}

		return listaSaida;
	}
    
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.mantercomptiposervicoformalanc.IManterCompTipoServicoFormaLancService#incluirServicoLancamentoCnab(br.com.bradesco.web.pgit.service.business.mantercomptiposervicoformalanc.bean.IncluirServicoLancamentoCnabEntradaDTO)
	 */
	public IncluirServicoLancamentoCnabSaidaDTO incluirServicoLancamentoCnab(IncluirServicoLancamentoCnabEntradaDTO entrada){
		IncluirServicoLancamentoCnabSaidaDTO saida = new IncluirServicoLancamentoCnabSaidaDTO();
		IncluirServicoLancamentoCnabRequest request = new IncluirServicoLancamentoCnabRequest();
		IncluirServicoLancamentoCnabResponse response = new IncluirServicoLancamentoCnabResponse();

		request.setCdFormaLancamentoCnab(PgitUtil.verificaIntegerNulo(entrada.getCdFormaLancamentoCnab()));
		request.setCdTipoServicoCnab(PgitUtil.verificaIntegerNulo(entrada.getCdTipoServicoCnab()));
		request.setCdFormaLiquidacao(PgitUtil.verificaIntegerNulo(entrada.getCdFormaLiquidacao()));
		request.setCdProdutoServicoOperacao(PgitUtil.verificaIntegerNulo(entrada.getCdProdutoServicoOperacao()));
		request.setCdProdutoOperacaoRelacionado(PgitUtil.verificaIntegerNulo(entrada.getCdProdutoOperacaoRelacionado()));

		response = getFactoryAdapter().getIncluirServicoLancamentoCnabPDCAdapter().invokeProcess(request);

		saida.setCodMensagem(response.getCodMensagem());
		saida.setMensagem(response.getMensagem());

		return saida;
	}
	
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.mantercomptiposervicoformalanc.IManterCompTipoServicoFormaLancService#detalharServicoLancamentoCnab(br.com.bradesco.web.pgit.service.business.mantercomptiposervicoformalanc.bean.DetalharServicoLancamentoCnabEntradaDTO)
	 */
	public DetalharServicoLancamentoCnabSaidaDTO detalharServicoLancamentoCnab(DetalharServicoLancamentoCnabEntradaDTO entrada){
		DetalharServicoLancamentoCnabSaidaDTO saida = new DetalharServicoLancamentoCnabSaidaDTO();
		DetalharServicoLancamentoCnabRequest request = new DetalharServicoLancamentoCnabRequest();
		DetalharServicoLancamentoCnabResponse response = new DetalharServicoLancamentoCnabResponse();

		request.setCdFormaLancamentoCnab(PgitUtil.verificaIntegerNulo(entrada.getCdFormaLancamentoCnab()));
		request.setCdTipoServicoCnab(PgitUtil.verificaIntegerNulo(entrada.getCdTipoServicoCnab()));

		response = getFactoryAdapter().getDetalharServicoLancamentoCnabPDCAdapter().invokeProcess(request);

		saida.setCodMensagem(response.getCodMensagem());
		saida.setMensagem(response.getMensagem());
		saida.setCdFormaLancamentoCnab(response.getCdFormaLancamentoCnab());
		saida.setCdTipoServicoCnab(response.getCdTipoServicoCnab());
		saida.setCdFormaLiquidacao(response.getCdFormaLiquidacao());
		saida.setCdProdutoServicoOperacao(response.getCdProdutoServicoOperacao());
		saida.setCdProdutoOperacaoRelacionado(response.getCdProdutoOperacaoRelacionado());
		
		saida.setCdUsuarioInclusao(response.getCdUsuarioInclusao());
		saida.setHrInclusaoRegistro(response.getHrInclusaoRegistro());
		saida.setCdOperacaoCanalInclusao(response.getCdOperacaoCanalInclusao());
		saida.setCdTipoCanalInclusao(response.getCdTipoCanalInclusao());
		saida.setCdUsuarioManutencao(response.getCdUsuarioManutencao());
		saida.setHrManutencaoRegistro(response.getHrManutencaoRegistro());
		saida.setCdOperacaoCanalManutencao(response.getCdOperacaoCanalManutencao());
		saida.setCdTipoCanalManutencao(response.getCdTipoCanalManutencao());
		
		saida.setDsFormaLancamentoCnab(response.getDsFormaLancamentoCnab());
		saida.setDsTipoServicoCnab(response.getDsTipoServicoCnab());
		saida.setDsFormaLiquidacao(response.getDsFormaLiquidacao());
		saida.setDsProdutoServicoOperacao(response.getDsProdutoServicoOperacao());        
		saida.setDsProdutoServicoRelacionado(response.getDsProdutoServicoRelacionado());
		
		saida.setCdRelacionamentoProduto(response.getCdRelacionamentoProduto());
		saida.setDsCanalInclusao(response.getDsCanalInclusao());
		saida.setDsCanalManutencao(response.getDsCanalManutencao());

		return saida;
	}
	
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.mantercomptiposervicoformalanc.IManterCompTipoServicoFormaLancService#excluirServicoLancamentoCnab(br.com.bradesco.web.pgit.service.business.mantercomptiposervicoformalanc.bean.ExcluirServicoLancamentoCnabEntradaDTO)
	 */
	public ExcluirServicoLancamentoCnabSaidaDTO excluirServicoLancamentoCnab(ExcluirServicoLancamentoCnabEntradaDTO entrada){
		ExcluirServicoLancamentoCnabSaidaDTO saida = new ExcluirServicoLancamentoCnabSaidaDTO();
		ExcluirServicoLancamentoCnabRequest request = new ExcluirServicoLancamentoCnabRequest();
		ExcluirServicoLancamentoCnabResponse response = new ExcluirServicoLancamentoCnabResponse();

		request.setCdFormaLancamentoCnab(PgitUtil.verificaIntegerNulo(entrada.getCdFormaLancamentoCnab()));
		request.setCdTipoServicoCnab(PgitUtil.verificaIntegerNulo(entrada.getCdTipoServicoCnab()));	

		response = getFactoryAdapter().getExcluirServicoLancamentoCnabPDCAdapter().invokeProcess(request);

		saida.setCodMensagem(response.getCodMensagem());
		saida.setMensagem(response.getMensagem());

		return saida;
	}
	
	
}

