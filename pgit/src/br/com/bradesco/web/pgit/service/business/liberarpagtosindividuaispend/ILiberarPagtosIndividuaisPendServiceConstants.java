/*
 * =========================================================================
 * 
 * Client:       Bradesco (BR)
 * Project:      Arquitetura Bradesco Canal Internet
 * Development:  GFT Iberia (http://www.gft.com)
 * -------------------------------------------------------------------------
 * Revision - Last:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/constants.ftl,v $
 * $Id: constants.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revision - History:
 * $Log: constants.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.liberarpagtosindividuaispend;


/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Interface de constantes do adaptador: LiberarPagtosIndividuaisPend
 * </p>
 * 
 * @comment C�DIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public interface ILiberarPagtosIndividuaisPendServiceConstants {
	
	/** Atributo NUMERO_OCORRENCIAS_CONSULTAR. */
	int NUMERO_OCORRENCIAS_CONSULTAR = 30;
	
	/** Atributo CODIGO_AGENDAMENTO_PAGO_NAO_PAGO. */
	int CODIGO_AGENDAMENTO_PAGO_NAO_PAGO = 1;
	
	/** Atributo CODIGO_SITUACAO_PAGAMENTO. */
	int CODIGO_SITUACAO_PAGAMENTO = 2;
}