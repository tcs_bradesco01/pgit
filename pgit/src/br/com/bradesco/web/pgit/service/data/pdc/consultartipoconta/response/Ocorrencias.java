/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultartipoconta.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdEmpresa
     */
    private long _cdEmpresa = 0;

    /**
     * keeps track of state for field: _cdEmpresa
     */
    private boolean _has_cdEmpresa;

    /**
     * Field _cdTipo
     */
    private int _cdTipo = 0;

    /**
     * keeps track of state for field: _cdTipo
     */
    private boolean _has_cdTipo;

    /**
     * Field _dsTipo
     */
    private java.lang.String _dsTipo;

    /**
     * Field _nrContrato
     */
    private long _nrContrato = 0;

    /**
     * keeps track of state for field: _nrContrato
     */
    private boolean _has_nrContrato;

    /**
     * Field _auxNumerico
     */
    private long _auxNumerico = 0;

    /**
     * keeps track of state for field: _auxNumerico
     */
    private boolean _has_auxNumerico;

    /**
     * Field _auxAlfanumerico
     */
    private java.lang.String _auxAlfanumerico;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultartipoconta.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteAuxNumerico
     * 
     */
    public void deleteAuxNumerico()
    {
        this._has_auxNumerico= false;
    } //-- void deleteAuxNumerico() 

    /**
     * Method deleteCdEmpresa
     * 
     */
    public void deleteCdEmpresa()
    {
        this._has_cdEmpresa= false;
    } //-- void deleteCdEmpresa() 

    /**
     * Method deleteCdTipo
     * 
     */
    public void deleteCdTipo()
    {
        this._has_cdTipo= false;
    } //-- void deleteCdTipo() 

    /**
     * Method deleteNrContrato
     * 
     */
    public void deleteNrContrato()
    {
        this._has_nrContrato= false;
    } //-- void deleteNrContrato() 

    /**
     * Returns the value of field 'auxAlfanumerico'.
     * 
     * @return String
     * @return the value of field 'auxAlfanumerico'.
     */
    public java.lang.String getAuxAlfanumerico()
    {
        return this._auxAlfanumerico;
    } //-- java.lang.String getAuxAlfanumerico() 

    /**
     * Returns the value of field 'auxNumerico'.
     * 
     * @return long
     * @return the value of field 'auxNumerico'.
     */
    public long getAuxNumerico()
    {
        return this._auxNumerico;
    } //-- long getAuxNumerico() 

    /**
     * Returns the value of field 'cdEmpresa'.
     * 
     * @return long
     * @return the value of field 'cdEmpresa'.
     */
    public long getCdEmpresa()
    {
        return this._cdEmpresa;
    } //-- long getCdEmpresa() 

    /**
     * Returns the value of field 'cdTipo'.
     * 
     * @return int
     * @return the value of field 'cdTipo'.
     */
    public int getCdTipo()
    {
        return this._cdTipo;
    } //-- int getCdTipo() 

    /**
     * Returns the value of field 'dsTipo'.
     * 
     * @return String
     * @return the value of field 'dsTipo'.
     */
    public java.lang.String getDsTipo()
    {
        return this._dsTipo;
    } //-- java.lang.String getDsTipo() 

    /**
     * Returns the value of field 'nrContrato'.
     * 
     * @return long
     * @return the value of field 'nrContrato'.
     */
    public long getNrContrato()
    {
        return this._nrContrato;
    } //-- long getNrContrato() 

    /**
     * Method hasAuxNumerico
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasAuxNumerico()
    {
        return this._has_auxNumerico;
    } //-- boolean hasAuxNumerico() 

    /**
     * Method hasCdEmpresa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdEmpresa()
    {
        return this._has_cdEmpresa;
    } //-- boolean hasCdEmpresa() 

    /**
     * Method hasCdTipo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipo()
    {
        return this._has_cdTipo;
    } //-- boolean hasCdTipo() 

    /**
     * Method hasNrContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrContrato()
    {
        return this._has_nrContrato;
    } //-- boolean hasNrContrato() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'auxAlfanumerico'.
     * 
     * @param auxAlfanumerico the value of field 'auxAlfanumerico'.
     */
    public void setAuxAlfanumerico(java.lang.String auxAlfanumerico)
    {
        this._auxAlfanumerico = auxAlfanumerico;
    } //-- void setAuxAlfanumerico(java.lang.String) 

    /**
     * Sets the value of field 'auxNumerico'.
     * 
     * @param auxNumerico the value of field 'auxNumerico'.
     */
    public void setAuxNumerico(long auxNumerico)
    {
        this._auxNumerico = auxNumerico;
        this._has_auxNumerico = true;
    } //-- void setAuxNumerico(long) 

    /**
     * Sets the value of field 'cdEmpresa'.
     * 
     * @param cdEmpresa the value of field 'cdEmpresa'.
     */
    public void setCdEmpresa(long cdEmpresa)
    {
        this._cdEmpresa = cdEmpresa;
        this._has_cdEmpresa = true;
    } //-- void setCdEmpresa(long) 

    /**
     * Sets the value of field 'cdTipo'.
     * 
     * @param cdTipo the value of field 'cdTipo'.
     */
    public void setCdTipo(int cdTipo)
    {
        this._cdTipo = cdTipo;
        this._has_cdTipo = true;
    } //-- void setCdTipo(int) 

    /**
     * Sets the value of field 'dsTipo'.
     * 
     * @param dsTipo the value of field 'dsTipo'.
     */
    public void setDsTipo(java.lang.String dsTipo)
    {
        this._dsTipo = dsTipo;
    } //-- void setDsTipo(java.lang.String) 

    /**
     * Sets the value of field 'nrContrato'.
     * 
     * @param nrContrato the value of field 'nrContrato'.
     */
    public void setNrContrato(long nrContrato)
    {
        this._nrContrato = nrContrato;
        this._has_nrContrato = true;
    } //-- void setNrContrato(long) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultartipoconta.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultartipoconta.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultartipoconta.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultartipoconta.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
