/*
 * Nome: br.com.bradesco.web.pgit.service.business.endereco.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.endereco.bean;

/**
 * Nome: TipoEnderecoEnum
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public enum TipoEnderecoEnum {

	/** Atributo CORREIO. */
	CORREIO,
	
	/** Atributo EMAIL. */
	EMAIL;
}
