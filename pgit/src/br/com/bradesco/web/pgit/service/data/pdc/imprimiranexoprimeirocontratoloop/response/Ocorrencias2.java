/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.imprimiranexoprimeirocontratoloop.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias2.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias2 implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdCpfCnpjConta
     */
    private java.lang.String _cdCpfCnpjConta;

    /**
     * Field _cdNomeParticipanteConta
     */
    private java.lang.String _cdNomeParticipanteConta;

    /**
     * Field _cdBancoConta
     */
    private int _cdBancoConta = 0;

    /**
     * keeps track of state for field: _cdBancoConta
     */
    private boolean _has_cdBancoConta;

    /**
     * Field _cdAgenciaConta
     */
    private int _cdAgenciaConta = 0;

    /**
     * keeps track of state for field: _cdAgenciaConta
     */
    private boolean _has_cdAgenciaConta;

    /**
     * Field _digitoAgenciaConta
     */
    private java.lang.String _digitoAgenciaConta;

    /**
     * Field _cdContaConta
     */
    private long _cdContaConta = 0;

    /**
     * keeps track of state for field: _cdContaConta
     */
    private boolean _has_cdContaConta;

    /**
     * Field _digitoContaConta
     */
    private java.lang.String _digitoContaConta;

    /**
     * Field _dsTipoConta
     */
    private java.lang.String _dsTipoConta;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias2() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimiranexoprimeirocontratoloop.response.Ocorrencias2()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdAgenciaConta
     * 
     */
    public void deleteCdAgenciaConta()
    {
        this._has_cdAgenciaConta= false;
    } //-- void deleteCdAgenciaConta() 

    /**
     * Method deleteCdBancoConta
     * 
     */
    public void deleteCdBancoConta()
    {
        this._has_cdBancoConta= false;
    } //-- void deleteCdBancoConta() 

    /**
     * Method deleteCdContaConta
     * 
     */
    public void deleteCdContaConta()
    {
        this._has_cdContaConta= false;
    } //-- void deleteCdContaConta() 

    /**
     * Returns the value of field 'cdAgenciaConta'.
     * 
     * @return int
     * @return the value of field 'cdAgenciaConta'.
     */
    public int getCdAgenciaConta()
    {
        return this._cdAgenciaConta;
    } //-- int getCdAgenciaConta() 

    /**
     * Returns the value of field 'cdBancoConta'.
     * 
     * @return int
     * @return the value of field 'cdBancoConta'.
     */
    public int getCdBancoConta()
    {
        return this._cdBancoConta;
    } //-- int getCdBancoConta() 

    /**
     * Returns the value of field 'cdContaConta'.
     * 
     * @return long
     * @return the value of field 'cdContaConta'.
     */
    public long getCdContaConta()
    {
        return this._cdContaConta;
    } //-- long getCdContaConta() 

    /**
     * Returns the value of field 'cdCpfCnpjConta'.
     * 
     * @return String
     * @return the value of field 'cdCpfCnpjConta'.
     */
    public java.lang.String getCdCpfCnpjConta()
    {
        return this._cdCpfCnpjConta;
    } //-- java.lang.String getCdCpfCnpjConta() 

    /**
     * Returns the value of field 'cdNomeParticipanteConta'.
     * 
     * @return String
     * @return the value of field 'cdNomeParticipanteConta'.
     */
    public java.lang.String getCdNomeParticipanteConta()
    {
        return this._cdNomeParticipanteConta;
    } //-- java.lang.String getCdNomeParticipanteConta() 

    /**
     * Returns the value of field 'digitoAgenciaConta'.
     * 
     * @return String
     * @return the value of field 'digitoAgenciaConta'.
     */
    public java.lang.String getDigitoAgenciaConta()
    {
        return this._digitoAgenciaConta;
    } //-- java.lang.String getDigitoAgenciaConta() 

    /**
     * Returns the value of field 'digitoContaConta'.
     * 
     * @return String
     * @return the value of field 'digitoContaConta'.
     */
    public java.lang.String getDigitoContaConta()
    {
        return this._digitoContaConta;
    } //-- java.lang.String getDigitoContaConta() 

    /**
     * Returns the value of field 'dsTipoConta'.
     * 
     * @return String
     * @return the value of field 'dsTipoConta'.
     */
    public java.lang.String getDsTipoConta()
    {
        return this._dsTipoConta;
    } //-- java.lang.String getDsTipoConta() 

    /**
     * Method hasCdAgenciaConta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgenciaConta()
    {
        return this._has_cdAgenciaConta;
    } //-- boolean hasCdAgenciaConta() 

    /**
     * Method hasCdBancoConta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdBancoConta()
    {
        return this._has_cdBancoConta;
    } //-- boolean hasCdBancoConta() 

    /**
     * Method hasCdContaConta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdContaConta()
    {
        return this._has_cdContaConta;
    } //-- boolean hasCdContaConta() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdAgenciaConta'.
     * 
     * @param cdAgenciaConta the value of field 'cdAgenciaConta'.
     */
    public void setCdAgenciaConta(int cdAgenciaConta)
    {
        this._cdAgenciaConta = cdAgenciaConta;
        this._has_cdAgenciaConta = true;
    } //-- void setCdAgenciaConta(int) 

    /**
     * Sets the value of field 'cdBancoConta'.
     * 
     * @param cdBancoConta the value of field 'cdBancoConta'.
     */
    public void setCdBancoConta(int cdBancoConta)
    {
        this._cdBancoConta = cdBancoConta;
        this._has_cdBancoConta = true;
    } //-- void setCdBancoConta(int) 

    /**
     * Sets the value of field 'cdContaConta'.
     * 
     * @param cdContaConta the value of field 'cdContaConta'.
     */
    public void setCdContaConta(long cdContaConta)
    {
        this._cdContaConta = cdContaConta;
        this._has_cdContaConta = true;
    } //-- void setCdContaConta(long) 

    /**
     * Sets the value of field 'cdCpfCnpjConta'.
     * 
     * @param cdCpfCnpjConta the value of field 'cdCpfCnpjConta'.
     */
    public void setCdCpfCnpjConta(java.lang.String cdCpfCnpjConta)
    {
        this._cdCpfCnpjConta = cdCpfCnpjConta;
    } //-- void setCdCpfCnpjConta(java.lang.String) 

    /**
     * Sets the value of field 'cdNomeParticipanteConta'.
     * 
     * @param cdNomeParticipanteConta the value of field
     * 'cdNomeParticipanteConta'.
     */
    public void setCdNomeParticipanteConta(java.lang.String cdNomeParticipanteConta)
    {
        this._cdNomeParticipanteConta = cdNomeParticipanteConta;
    } //-- void setCdNomeParticipanteConta(java.lang.String) 

    /**
     * Sets the value of field 'digitoAgenciaConta'.
     * 
     * @param digitoAgenciaConta the value of field
     * 'digitoAgenciaConta'.
     */
    public void setDigitoAgenciaConta(java.lang.String digitoAgenciaConta)
    {
        this._digitoAgenciaConta = digitoAgenciaConta;
    } //-- void setDigitoAgenciaConta(java.lang.String) 

    /**
     * Sets the value of field 'digitoContaConta'.
     * 
     * @param digitoContaConta the value of field 'digitoContaConta'
     */
    public void setDigitoContaConta(java.lang.String digitoContaConta)
    {
        this._digitoContaConta = digitoContaConta;
    } //-- void setDigitoContaConta(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoConta'.
     * 
     * @param dsTipoConta the value of field 'dsTipoConta'.
     */
    public void setDsTipoConta(java.lang.String dsTipoConta)
    {
        this._dsTipoConta = dsTipoConta;
    } //-- void setDsTipoConta(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias2
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.imprimiranexoprimeirocontratoloop.response.Ocorrencias2 unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.imprimiranexoprimeirocontratoloop.response.Ocorrencias2) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.imprimiranexoprimeirocontratoloop.response.Ocorrencias2.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimiranexoprimeirocontratoloop.response.Ocorrencias2 unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
