/*
 * Nome: br.com.bradesco.web.pgit.service.business.histcomplementar.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.histcomplementar.bean;

/**
 * Nome: ManterHistoricoComplementarDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ManterHistoricoComplementarDTO {

	/** Atributo totalRegistros. */
	private int totalRegistros;
	
	/** Atributo codigo. */
	private String codigo;
	
	/** Atributo descricao. */
	private String descricao;
	
	/** Atributo descricaoResumida. */
	private String descricaoResumida;
	
	/** Atributo prioridadeDebito. */
	private String prioridadeDebito;
	
	/** Atributo centroCusto. */
	private String centroCusto;
	
	/** Atributo horarioLimite. */
	private String horarioLimite;
	
	/** Atributo margemSeguranca. */
	private String margemSeguranca;
	
	/** Atributo tipoMensagem. */
	private String tipoMensagem;
	
	/** Atributo restricao. */
	private String restricao;
	
	/** Atributo codLancamentoDebito. */
	private String codLancamentoDebito;
	
	/** Atributo codLancamentoCredito. */
	private String codLancamentoCredito;
	
	/** Atributo servico. */
	private String servico;
	
	/** Atributo unidadeOperadora. */
	private String unidadeOperadora;
	
	/** Atributo codMensagem. */
	private String codMensagem;
	
	/** Atributo dataManutencao. */
	private String dataManutencao;
	
	/** Atributo responsavel. */
	private String responsavel;
	
	/** Atributo tipoManutencao. */
	private String tipoManutencao;
	
	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}
	
	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}
	
	/**
	 * Get: dataManutencao.
	 *
	 * @return dataManutencao
	 */
	public String getDataManutencao() {
		return dataManutencao;
	}
	
	/**
	 * Set: dataManutencao.
	 *
	 * @param dataManutencao the data manutencao
	 */
	public void setDataManutencao(String dataManutencao) {
		this.dataManutencao = dataManutencao;
	}
	
	/**
	 * Get: responsavel.
	 *
	 * @return responsavel
	 */
	public String getResponsavel() {
		return responsavel;
	}
	
	/**
	 * Set: responsavel.
	 *
	 * @param responsavel the responsavel
	 */
	public void setResponsavel(String responsavel) {
		this.responsavel = responsavel;
	}
	
	/**
	 * Get: tipoManutencao.
	 *
	 * @return tipoManutencao
	 */
	public String getTipoManutencao() {
		return tipoManutencao;
	}
	
	/**
	 * Set: tipoManutencao.
	 *
	 * @param tipoManutencao the tipo manutencao
	 */
	public void setTipoManutencao(String tipoManutencao) {
		this.tipoManutencao = tipoManutencao;
	}
	
	/**
	 * Get: centroCusto.
	 *
	 * @return centroCusto
	 */
	public String getCentroCusto() {
		return centroCusto;
	}
	
	/**
	 * Set: centroCusto.
	 *
	 * @param centroCusto the centro custo
	 */
	public void setCentroCusto(String centroCusto) {
		this.centroCusto = centroCusto;
	}
	
	/**
	 * Get: codigo.
	 *
	 * @return codigo
	 */
	public String getCodigo() {
		return codigo;
	}
	
	/**
	 * Set: codigo.
	 *
	 * @param codigo the codigo
	 */
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	
	/**
	 * Get: descricao.
	 *
	 * @return descricao
	 */
	public String getDescricao() {
		return descricao;
	}
	
	/**
	 * Set: descricao.
	 *
	 * @param descricao the descricao
	 */
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	/**
	 * Get: descricaoResumida.
	 *
	 * @return descricaoResumida
	 */
	public String getDescricaoResumida() {
		return descricaoResumida;
	}
	
	/**
	 * Set: descricaoResumida.
	 *
	 * @param descricaoResumida the descricao resumida
	 */
	public void setDescricaoResumida(String descricaoResumida) {
		this.descricaoResumida = descricaoResumida;
	}
	
	/**
	 * Get: horarioLimite.
	 *
	 * @return horarioLimite
	 */
	public String getHorarioLimite() {
		return horarioLimite;
	}
	
	/**
	 * Set: horarioLimite.
	 *
	 * @param horarioLimite the horario limite
	 */
	public void setHorarioLimite(String horarioLimite) {
		this.horarioLimite = horarioLimite;
	}
	
	/**
	 * Get: margemSeguranca.
	 *
	 * @return margemSeguranca
	 */
	public String getMargemSeguranca() {
		return margemSeguranca;
	}
	
	/**
	 * Set: margemSeguranca.
	 *
	 * @param margemSeguranca the margem seguranca
	 */
	public void setMargemSeguranca(String margemSeguranca) {
		this.margemSeguranca = margemSeguranca;
	}
	
	/**
	 * Get: prioridadeDebito.
	 *
	 * @return prioridadeDebito
	 */
	public String getPrioridadeDebito() {
		return prioridadeDebito;
	}
	
	/**
	 * Set: prioridadeDebito.
	 *
	 * @param prioridadeDebito the prioridade debito
	 */
	public void setPrioridadeDebito(String prioridadeDebito) {
		this.prioridadeDebito = prioridadeDebito;
	}
	
	/**
	 * Get: totalRegistros.
	 *
	 * @return totalRegistros
	 */
	public int getTotalRegistros() {
		return totalRegistros;
	}
	
	/**
	 * Set: totalRegistros.
	 *
	 * @param totalRegistros the total registros
	 */
	public void setTotalRegistros(int totalRegistros) {
		this.totalRegistros = totalRegistros;
	}
	
	/**
	 * Get: codLancamentoCredito.
	 *
	 * @return codLancamentoCredito
	 */
	public String getCodLancamentoCredito() {
		return codLancamentoCredito;
	}
	
	/**
	 * Set: codLancamentoCredito.
	 *
	 * @param codLancamentoCredito the cod lancamento credito
	 */
	public void setCodLancamentoCredito(String codLancamentoCredito) {
		this.codLancamentoCredito = codLancamentoCredito;
	}
	
	/**
	 * Get: codLancamentoDebito.
	 *
	 * @return codLancamentoDebito
	 */
	public String getCodLancamentoDebito() {
		return codLancamentoDebito;
	}
	
	/**
	 * Set: codLancamentoDebito.
	 *
	 * @param codLancamentoDebito the cod lancamento debito
	 */
	public void setCodLancamentoDebito(String codLancamentoDebito) {
		this.codLancamentoDebito = codLancamentoDebito;
	}
	
	/**
	 * Get: restricao.
	 *
	 * @return restricao
	 */
	public String getRestricao() {
		return restricao;
	}
	
	/**
	 * Set: restricao.
	 *
	 * @param restricao the restricao
	 */
	public void setRestricao(String restricao) {
		this.restricao = restricao;
	}
	
	/**
	 * Get: servico.
	 *
	 * @return servico
	 */
	public String getServico() {
		return servico;
	}
	
	/**
	 * Set: servico.
	 *
	 * @param servico the servico
	 */
	public void setServico(String servico) {
		this.servico = servico;
	}
	
	/**
	 * Get: tipoMensagem.
	 *
	 * @return tipoMensagem
	 */
	public String getTipoMensagem() {
		return tipoMensagem;
	}
	
	/**
	 * Set: tipoMensagem.
	 *
	 * @param tipoMensagem the tipo mensagem
	 */
	public void setTipoMensagem(String tipoMensagem) {
		this.tipoMensagem = tipoMensagem;
	}
	
	/**
	 * Get: unidadeOperadora.
	 *
	 * @return unidadeOperadora
	 */
	public String getUnidadeOperadora() {
		return unidadeOperadora;
	}
	
	/**
	 * Set: unidadeOperadora.
	 *
	 * @param unidadeOperadora the unidade operadora
	 */
	public void setUnidadeOperadora(String unidadeOperadora) {
		this.unidadeOperadora = unidadeOperadora;
	}
	
	
	
}
