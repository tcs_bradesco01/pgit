/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.excluirmsgalertagestor.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ExlcuirMsgAlertaGestorRequest.
 * 
 * @version $Revision$ $Date$
 */
public class ExlcuirMsgAlertaGestorRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdCistProcs
     */
    private java.lang.String _cdCistProcs;

    /**
     * Field _cdProcsSistema
     */
    private int _cdProcsSistema = 0;

    /**
     * keeps track of state for field: _cdProcsSistema
     */
    private boolean _has_cdProcsSistema;

    /**
     * Field _nrSeqMensagemAlerta
     */
    private int _nrSeqMensagemAlerta = 0;

    /**
     * keeps track of state for field: _nrSeqMensagemAlerta
     */
    private boolean _has_nrSeqMensagemAlerta;

    /**
     * Field _cdPessoa
     */
    private long _cdPessoa = 0;

    /**
     * keeps track of state for field: _cdPessoa
     */
    private boolean _has_cdPessoa;

    /**
     * Field _cdCist
     */
    private java.lang.String _cdCist;

    /**
     * Field _nrEventoMensagemNegocio
     */
    private int _nrEventoMensagemNegocio = 0;

    /**
     * keeps track of state for field: _nrEventoMensagemNegocio
     */
    private boolean _has_nrEventoMensagemNegocio;

    /**
     * Field _cdRecGedorMensagem
     */
    private int _cdRecGedorMensagem = 0;

    /**
     * keeps track of state for field: _cdRecGedorMensagem
     */
    private boolean _has_cdRecGedorMensagem;

    /**
     * Field _cdIdiomaTextoMensagem
     */
    private int _cdIdiomaTextoMensagem = 0;

    /**
     * keeps track of state for field: _cdIdiomaTextoMensagem
     */
    private boolean _has_cdIdiomaTextoMensagem;

    /**
     * Field _cdIndicadorMeioTransmissao
     */
    private int _cdIndicadorMeioTransmissao = 0;

    /**
     * keeps track of state for field: _cdIndicadorMeioTransmissao
     */
    private boolean _has_cdIndicadorMeioTransmissao;


      //----------------/
     //- Constructors -/
    //----------------/

    public ExlcuirMsgAlertaGestorRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.excluirmsgalertagestor.request.ExlcuirMsgAlertaGestorRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdIdiomaTextoMensagem
     * 
     */
    public void deleteCdIdiomaTextoMensagem()
    {
        this._has_cdIdiomaTextoMensagem= false;
    } //-- void deleteCdIdiomaTextoMensagem() 

    /**
     * Method deleteCdIndicadorMeioTransmissao
     * 
     */
    public void deleteCdIndicadorMeioTransmissao()
    {
        this._has_cdIndicadorMeioTransmissao= false;
    } //-- void deleteCdIndicadorMeioTransmissao() 

    /**
     * Method deleteCdPessoa
     * 
     */
    public void deleteCdPessoa()
    {
        this._has_cdPessoa= false;
    } //-- void deleteCdPessoa() 

    /**
     * Method deleteCdProcsSistema
     * 
     */
    public void deleteCdProcsSistema()
    {
        this._has_cdProcsSistema= false;
    } //-- void deleteCdProcsSistema() 

    /**
     * Method deleteCdRecGedorMensagem
     * 
     */
    public void deleteCdRecGedorMensagem()
    {
        this._has_cdRecGedorMensagem= false;
    } //-- void deleteCdRecGedorMensagem() 

    /**
     * Method deleteNrEventoMensagemNegocio
     * 
     */
    public void deleteNrEventoMensagemNegocio()
    {
        this._has_nrEventoMensagemNegocio= false;
    } //-- void deleteNrEventoMensagemNegocio() 

    /**
     * Method deleteNrSeqMensagemAlerta
     * 
     */
    public void deleteNrSeqMensagemAlerta()
    {
        this._has_nrSeqMensagemAlerta= false;
    } //-- void deleteNrSeqMensagemAlerta() 

    /**
     * Returns the value of field 'cdCist'.
     * 
     * @return String
     * @return the value of field 'cdCist'.
     */
    public java.lang.String getCdCist()
    {
        return this._cdCist;
    } //-- java.lang.String getCdCist() 

    /**
     * Returns the value of field 'cdCistProcs'.
     * 
     * @return String
     * @return the value of field 'cdCistProcs'.
     */
    public java.lang.String getCdCistProcs()
    {
        return this._cdCistProcs;
    } //-- java.lang.String getCdCistProcs() 

    /**
     * Returns the value of field 'cdIdiomaTextoMensagem'.
     * 
     * @return int
     * @return the value of field 'cdIdiomaTextoMensagem'.
     */
    public int getCdIdiomaTextoMensagem()
    {
        return this._cdIdiomaTextoMensagem;
    } //-- int getCdIdiomaTextoMensagem() 

    /**
     * Returns the value of field 'cdIndicadorMeioTransmissao'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorMeioTransmissao'.
     */
    public int getCdIndicadorMeioTransmissao()
    {
        return this._cdIndicadorMeioTransmissao;
    } //-- int getCdIndicadorMeioTransmissao() 

    /**
     * Returns the value of field 'cdPessoa'.
     * 
     * @return long
     * @return the value of field 'cdPessoa'.
     */
    public long getCdPessoa()
    {
        return this._cdPessoa;
    } //-- long getCdPessoa() 

    /**
     * Returns the value of field 'cdProcsSistema'.
     * 
     * @return int
     * @return the value of field 'cdProcsSistema'.
     */
    public int getCdProcsSistema()
    {
        return this._cdProcsSistema;
    } //-- int getCdProcsSistema() 

    /**
     * Returns the value of field 'cdRecGedorMensagem'.
     * 
     * @return int
     * @return the value of field 'cdRecGedorMensagem'.
     */
    public int getCdRecGedorMensagem()
    {
        return this._cdRecGedorMensagem;
    } //-- int getCdRecGedorMensagem() 

    /**
     * Returns the value of field 'nrEventoMensagemNegocio'.
     * 
     * @return int
     * @return the value of field 'nrEventoMensagemNegocio'.
     */
    public int getNrEventoMensagemNegocio()
    {
        return this._nrEventoMensagemNegocio;
    } //-- int getNrEventoMensagemNegocio() 

    /**
     * Returns the value of field 'nrSeqMensagemAlerta'.
     * 
     * @return int
     * @return the value of field 'nrSeqMensagemAlerta'.
     */
    public int getNrSeqMensagemAlerta()
    {
        return this._nrSeqMensagemAlerta;
    } //-- int getNrSeqMensagemAlerta() 

    /**
     * Method hasCdIdiomaTextoMensagem
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIdiomaTextoMensagem()
    {
        return this._has_cdIdiomaTextoMensagem;
    } //-- boolean hasCdIdiomaTextoMensagem() 

    /**
     * Method hasCdIndicadorMeioTransmissao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorMeioTransmissao()
    {
        return this._has_cdIndicadorMeioTransmissao;
    } //-- boolean hasCdIndicadorMeioTransmissao() 

    /**
     * Method hasCdPessoa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoa()
    {
        return this._has_cdPessoa;
    } //-- boolean hasCdPessoa() 

    /**
     * Method hasCdProcsSistema
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProcsSistema()
    {
        return this._has_cdProcsSistema;
    } //-- boolean hasCdProcsSistema() 

    /**
     * Method hasCdRecGedorMensagem
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdRecGedorMensagem()
    {
        return this._has_cdRecGedorMensagem;
    } //-- boolean hasCdRecGedorMensagem() 

    /**
     * Method hasNrEventoMensagemNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrEventoMensagemNegocio()
    {
        return this._has_nrEventoMensagemNegocio;
    } //-- boolean hasNrEventoMensagemNegocio() 

    /**
     * Method hasNrSeqMensagemAlerta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSeqMensagemAlerta()
    {
        return this._has_nrSeqMensagemAlerta;
    } //-- boolean hasNrSeqMensagemAlerta() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdCist'.
     * 
     * @param cdCist the value of field 'cdCist'.
     */
    public void setCdCist(java.lang.String cdCist)
    {
        this._cdCist = cdCist;
    } //-- void setCdCist(java.lang.String) 

    /**
     * Sets the value of field 'cdCistProcs'.
     * 
     * @param cdCistProcs the value of field 'cdCistProcs'.
     */
    public void setCdCistProcs(java.lang.String cdCistProcs)
    {
        this._cdCistProcs = cdCistProcs;
    } //-- void setCdCistProcs(java.lang.String) 

    /**
     * Sets the value of field 'cdIdiomaTextoMensagem'.
     * 
     * @param cdIdiomaTextoMensagem the value of field
     * 'cdIdiomaTextoMensagem'.
     */
    public void setCdIdiomaTextoMensagem(int cdIdiomaTextoMensagem)
    {
        this._cdIdiomaTextoMensagem = cdIdiomaTextoMensagem;
        this._has_cdIdiomaTextoMensagem = true;
    } //-- void setCdIdiomaTextoMensagem(int) 

    /**
     * Sets the value of field 'cdIndicadorMeioTransmissao'.
     * 
     * @param cdIndicadorMeioTransmissao the value of field
     * 'cdIndicadorMeioTransmissao'.
     */
    public void setCdIndicadorMeioTransmissao(int cdIndicadorMeioTransmissao)
    {
        this._cdIndicadorMeioTransmissao = cdIndicadorMeioTransmissao;
        this._has_cdIndicadorMeioTransmissao = true;
    } //-- void setCdIndicadorMeioTransmissao(int) 

    /**
     * Sets the value of field 'cdPessoa'.
     * 
     * @param cdPessoa the value of field 'cdPessoa'.
     */
    public void setCdPessoa(long cdPessoa)
    {
        this._cdPessoa = cdPessoa;
        this._has_cdPessoa = true;
    } //-- void setCdPessoa(long) 

    /**
     * Sets the value of field 'cdProcsSistema'.
     * 
     * @param cdProcsSistema the value of field 'cdProcsSistema'.
     */
    public void setCdProcsSistema(int cdProcsSistema)
    {
        this._cdProcsSistema = cdProcsSistema;
        this._has_cdProcsSistema = true;
    } //-- void setCdProcsSistema(int) 

    /**
     * Sets the value of field 'cdRecGedorMensagem'.
     * 
     * @param cdRecGedorMensagem the value of field
     * 'cdRecGedorMensagem'.
     */
    public void setCdRecGedorMensagem(int cdRecGedorMensagem)
    {
        this._cdRecGedorMensagem = cdRecGedorMensagem;
        this._has_cdRecGedorMensagem = true;
    } //-- void setCdRecGedorMensagem(int) 

    /**
     * Sets the value of field 'nrEventoMensagemNegocio'.
     * 
     * @param nrEventoMensagemNegocio the value of field
     * 'nrEventoMensagemNegocio'.
     */
    public void setNrEventoMensagemNegocio(int nrEventoMensagemNegocio)
    {
        this._nrEventoMensagemNegocio = nrEventoMensagemNegocio;
        this._has_nrEventoMensagemNegocio = true;
    } //-- void setNrEventoMensagemNegocio(int) 

    /**
     * Sets the value of field 'nrSeqMensagemAlerta'.
     * 
     * @param nrSeqMensagemAlerta the value of field
     * 'nrSeqMensagemAlerta'.
     */
    public void setNrSeqMensagemAlerta(int nrSeqMensagemAlerta)
    {
        this._nrSeqMensagemAlerta = nrSeqMensagemAlerta;
        this._has_nrSeqMensagemAlerta = true;
    } //-- void setNrSeqMensagemAlerta(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ExlcuirMsgAlertaGestorRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.excluirmsgalertagestor.request.ExlcuirMsgAlertaGestorRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.excluirmsgalertagestor.request.ExlcuirMsgAlertaGestorRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.excluirmsgalertagestor.request.ExlcuirMsgAlertaGestorRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.excluirmsgalertagestor.request.ExlcuirMsgAlertaGestorRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
