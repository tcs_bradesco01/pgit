/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.detalharlistadebito.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.math.BigDecimal;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdControlePagamento
     */
    private java.lang.String _cdControlePagamento;

    /**
     * Field _vlPagamento
     */
    private java.math.BigDecimal _vlPagamento = new java.math.BigDecimal("0");

    /**
     * Field _cdFavorecidoBeneficiario
     */
    private long _cdFavorecidoBeneficiario = 0;

    /**
     * keeps track of state for field: _cdFavorecidoBeneficiario
     */
    private boolean _has_cdFavorecidoBeneficiario;

    /**
     * Field _dsFavorecidoBeneficiario
     */
    private java.lang.String _dsFavorecidoBeneficiario;

    /**
     * Field _cdBanco
     */
    private int _cdBanco = 0;

    /**
     * keeps track of state for field: _cdBanco
     */
    private boolean _has_cdBanco;

    /**
     * Field _dsBanco
     */
    private java.lang.String _dsBanco;

    /**
     * Field _cdAgencia
     */
    private int _cdAgencia = 0;

    /**
     * keeps track of state for field: _cdAgencia
     */
    private boolean _has_cdAgencia;

    /**
     * Field _cdDigitoAgencia
     */
    private int _cdDigitoAgencia = 0;

    /**
     * keeps track of state for field: _cdDigitoAgencia
     */
    private boolean _has_cdDigitoAgencia;

    /**
     * Field _dsAgencia
     */
    private java.lang.String _dsAgencia;

    /**
     * Field _cdConta
     */
    private long _cdConta = 0;

    /**
     * keeps track of state for field: _cdConta
     */
    private boolean _has_cdConta;

    /**
     * Field _cdDigitoConta
     */
    private java.lang.String _cdDigitoConta;

    /**
     * Field _cdSituacaoOperacaoPagamento
     */
    private int _cdSituacaoOperacaoPagamento = 0;

    /**
     * keeps track of state for field: _cdSituacaoOperacaoPagamento
     */
    private boolean _has_cdSituacaoOperacaoPagamento;

    /**
     * Field _dsSituacaoPagamento
     */
    private java.lang.String _dsSituacaoPagamento;

    /**
     * Field _cdMotivoSituacaoPagamento
     */
    private int _cdMotivoSituacaoPagamento = 0;

    /**
     * keeps track of state for field: _cdMotivoSituacaoPagamento
     */
    private boolean _has_cdMotivoSituacaoPagamento;

    /**
     * Field _dsMotivoPagamento
     */
    private java.lang.String _dsMotivoPagamento;

    /**
     * Field _cdSituacaoPagamentoLista
     */
    private int _cdSituacaoPagamentoLista = 0;

    /**
     * keeps track of state for field: _cdSituacaoPagamentoLista
     */
    private boolean _has_cdSituacaoPagamentoLista;

    /**
     * Field _dsSituacaoPagamentoLista
     */
    private java.lang.String _dsSituacaoPagamentoLista;

    /**
     * Field _cdTipoTela
     */
    private int _cdTipoTela = 0;

    /**
     * keeps track of state for field: _cdTipoTela
     */
    private boolean _has_cdTipoTela;

    /**
     * Field _cdTabelaConsulta
     */
    private int _cdTabelaConsulta = 0;

    /**
     * keeps track of state for field: _cdTabelaConsulta
     */
    private boolean _has_cdTabelaConsulta;

    /**
     * Field _dtEfetivacaoPagamento
     */
    private java.lang.String _dtEfetivacaoPagamento;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
        setVlPagamento(new java.math.BigDecimal("0"));
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharlistadebito.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdAgencia
     * 
     */
    public void deleteCdAgencia()
    {
        this._has_cdAgencia= false;
    } //-- void deleteCdAgencia() 

    /**
     * Method deleteCdBanco
     * 
     */
    public void deleteCdBanco()
    {
        this._has_cdBanco= false;
    } //-- void deleteCdBanco() 

    /**
     * Method deleteCdConta
     * 
     */
    public void deleteCdConta()
    {
        this._has_cdConta= false;
    } //-- void deleteCdConta() 

    /**
     * Method deleteCdDigitoAgencia
     * 
     */
    public void deleteCdDigitoAgencia()
    {
        this._has_cdDigitoAgencia= false;
    } //-- void deleteCdDigitoAgencia() 

    /**
     * Method deleteCdFavorecidoBeneficiario
     * 
     */
    public void deleteCdFavorecidoBeneficiario()
    {
        this._has_cdFavorecidoBeneficiario= false;
    } //-- void deleteCdFavorecidoBeneficiario() 

    /**
     * Method deleteCdMotivoSituacaoPagamento
     * 
     */
    public void deleteCdMotivoSituacaoPagamento()
    {
        this._has_cdMotivoSituacaoPagamento= false;
    } //-- void deleteCdMotivoSituacaoPagamento() 

    /**
     * Method deleteCdSituacaoOperacaoPagamento
     * 
     */
    public void deleteCdSituacaoOperacaoPagamento()
    {
        this._has_cdSituacaoOperacaoPagamento= false;
    } //-- void deleteCdSituacaoOperacaoPagamento() 

    /**
     * Method deleteCdSituacaoPagamentoLista
     * 
     */
    public void deleteCdSituacaoPagamentoLista()
    {
        this._has_cdSituacaoPagamentoLista= false;
    } //-- void deleteCdSituacaoPagamentoLista() 

    /**
     * Method deleteCdTabelaConsulta
     * 
     */
    public void deleteCdTabelaConsulta()
    {
        this._has_cdTabelaConsulta= false;
    } //-- void deleteCdTabelaConsulta() 

    /**
     * Method deleteCdTipoTela
     * 
     */
    public void deleteCdTipoTela()
    {
        this._has_cdTipoTela= false;
    } //-- void deleteCdTipoTela() 

    /**
     * Returns the value of field 'cdAgencia'.
     * 
     * @return int
     * @return the value of field 'cdAgencia'.
     */
    public int getCdAgencia()
    {
        return this._cdAgencia;
    } //-- int getCdAgencia() 

    /**
     * Returns the value of field 'cdBanco'.
     * 
     * @return int
     * @return the value of field 'cdBanco'.
     */
    public int getCdBanco()
    {
        return this._cdBanco;
    } //-- int getCdBanco() 

    /**
     * Returns the value of field 'cdConta'.
     * 
     * @return long
     * @return the value of field 'cdConta'.
     */
    public long getCdConta()
    {
        return this._cdConta;
    } //-- long getCdConta() 

    /**
     * Returns the value of field 'cdControlePagamento'.
     * 
     * @return String
     * @return the value of field 'cdControlePagamento'.
     */
    public java.lang.String getCdControlePagamento()
    {
        return this._cdControlePagamento;
    } //-- java.lang.String getCdControlePagamento() 

    /**
     * Returns the value of field 'cdDigitoAgencia'.
     * 
     * @return int
     * @return the value of field 'cdDigitoAgencia'.
     */
    public int getCdDigitoAgencia()
    {
        return this._cdDigitoAgencia;
    } //-- int getCdDigitoAgencia() 

    /**
     * Returns the value of field 'cdDigitoConta'.
     * 
     * @return String
     * @return the value of field 'cdDigitoConta'.
     */
    public java.lang.String getCdDigitoConta()
    {
        return this._cdDigitoConta;
    } //-- java.lang.String getCdDigitoConta() 

    /**
     * Returns the value of field 'cdFavorecidoBeneficiario'.
     * 
     * @return long
     * @return the value of field 'cdFavorecidoBeneficiario'.
     */
    public long getCdFavorecidoBeneficiario()
    {
        return this._cdFavorecidoBeneficiario;
    } //-- long getCdFavorecidoBeneficiario() 

    /**
     * Returns the value of field 'cdMotivoSituacaoPagamento'.
     * 
     * @return int
     * @return the value of field 'cdMotivoSituacaoPagamento'.
     */
    public int getCdMotivoSituacaoPagamento()
    {
        return this._cdMotivoSituacaoPagamento;
    } //-- int getCdMotivoSituacaoPagamento() 

    /**
     * Returns the value of field 'cdSituacaoOperacaoPagamento'.
     * 
     * @return int
     * @return the value of field 'cdSituacaoOperacaoPagamento'.
     */
    public int getCdSituacaoOperacaoPagamento()
    {
        return this._cdSituacaoOperacaoPagamento;
    } //-- int getCdSituacaoOperacaoPagamento() 

    /**
     * Returns the value of field 'cdSituacaoPagamentoLista'.
     * 
     * @return int
     * @return the value of field 'cdSituacaoPagamentoLista'.
     */
    public int getCdSituacaoPagamentoLista()
    {
        return this._cdSituacaoPagamentoLista;
    } //-- int getCdSituacaoPagamentoLista() 

    /**
     * Returns the value of field 'cdTabelaConsulta'.
     * 
     * @return int
     * @return the value of field 'cdTabelaConsulta'.
     */
    public int getCdTabelaConsulta()
    {
        return this._cdTabelaConsulta;
    } //-- int getCdTabelaConsulta() 

    /**
     * Returns the value of field 'cdTipoTela'.
     * 
     * @return int
     * @return the value of field 'cdTipoTela'.
     */
    public int getCdTipoTela()
    {
        return this._cdTipoTela;
    } //-- int getCdTipoTela() 

    /**
     * Returns the value of field 'dsAgencia'.
     * 
     * @return String
     * @return the value of field 'dsAgencia'.
     */
    public java.lang.String getDsAgencia()
    {
        return this._dsAgencia;
    } //-- java.lang.String getDsAgencia() 

    /**
     * Returns the value of field 'dsBanco'.
     * 
     * @return String
     * @return the value of field 'dsBanco'.
     */
    public java.lang.String getDsBanco()
    {
        return this._dsBanco;
    } //-- java.lang.String getDsBanco() 

    /**
     * Returns the value of field 'dsFavorecidoBeneficiario'.
     * 
     * @return String
     * @return the value of field 'dsFavorecidoBeneficiario'.
     */
    public java.lang.String getDsFavorecidoBeneficiario()
    {
        return this._dsFavorecidoBeneficiario;
    } //-- java.lang.String getDsFavorecidoBeneficiario() 

    /**
     * Returns the value of field 'dsMotivoPagamento'.
     * 
     * @return String
     * @return the value of field 'dsMotivoPagamento'.
     */
    public java.lang.String getDsMotivoPagamento()
    {
        return this._dsMotivoPagamento;
    } //-- java.lang.String getDsMotivoPagamento() 

    /**
     * Returns the value of field 'dsSituacaoPagamento'.
     * 
     * @return String
     * @return the value of field 'dsSituacaoPagamento'.
     */
    public java.lang.String getDsSituacaoPagamento()
    {
        return this._dsSituacaoPagamento;
    } //-- java.lang.String getDsSituacaoPagamento() 

    /**
     * Returns the value of field 'dsSituacaoPagamentoLista'.
     * 
     * @return String
     * @return the value of field 'dsSituacaoPagamentoLista'.
     */
    public java.lang.String getDsSituacaoPagamentoLista()
    {
        return this._dsSituacaoPagamentoLista;
    } //-- java.lang.String getDsSituacaoPagamentoLista() 

    /**
     * Returns the value of field 'dtEfetivacaoPagamento'.
     * 
     * @return String
     * @return the value of field 'dtEfetivacaoPagamento'.
     */
    public java.lang.String getDtEfetivacaoPagamento()
    {
        return this._dtEfetivacaoPagamento;
    } //-- java.lang.String getDtEfetivacaoPagamento() 

    /**
     * Returns the value of field 'vlPagamento'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlPagamento'.
     */
    public java.math.BigDecimal getVlPagamento()
    {
        return this._vlPagamento;
    } //-- java.math.BigDecimal getVlPagamento() 

    /**
     * Method hasCdAgencia
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgencia()
    {
        return this._has_cdAgencia;
    } //-- boolean hasCdAgencia() 

    /**
     * Method hasCdBanco
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdBanco()
    {
        return this._has_cdBanco;
    } //-- boolean hasCdBanco() 

    /**
     * Method hasCdConta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdConta()
    {
        return this._has_cdConta;
    } //-- boolean hasCdConta() 

    /**
     * Method hasCdDigitoAgencia
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdDigitoAgencia()
    {
        return this._has_cdDigitoAgencia;
    } //-- boolean hasCdDigitoAgencia() 

    /**
     * Method hasCdFavorecidoBeneficiario
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFavorecidoBeneficiario()
    {
        return this._has_cdFavorecidoBeneficiario;
    } //-- boolean hasCdFavorecidoBeneficiario() 

    /**
     * Method hasCdMotivoSituacaoPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMotivoSituacaoPagamento()
    {
        return this._has_cdMotivoSituacaoPagamento;
    } //-- boolean hasCdMotivoSituacaoPagamento() 

    /**
     * Method hasCdSituacaoOperacaoPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSituacaoOperacaoPagamento()
    {
        return this._has_cdSituacaoOperacaoPagamento;
    } //-- boolean hasCdSituacaoOperacaoPagamento() 

    /**
     * Method hasCdSituacaoPagamentoLista
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSituacaoPagamentoLista()
    {
        return this._has_cdSituacaoPagamentoLista;
    } //-- boolean hasCdSituacaoPagamentoLista() 

    /**
     * Method hasCdTabelaConsulta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTabelaConsulta()
    {
        return this._has_cdTabelaConsulta;
    } //-- boolean hasCdTabelaConsulta() 

    /**
     * Method hasCdTipoTela
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoTela()
    {
        return this._has_cdTipoTela;
    } //-- boolean hasCdTipoTela() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdAgencia'.
     * 
     * @param cdAgencia the value of field 'cdAgencia'.
     */
    public void setCdAgencia(int cdAgencia)
    {
        this._cdAgencia = cdAgencia;
        this._has_cdAgencia = true;
    } //-- void setCdAgencia(int) 

    /**
     * Sets the value of field 'cdBanco'.
     * 
     * @param cdBanco the value of field 'cdBanco'.
     */
    public void setCdBanco(int cdBanco)
    {
        this._cdBanco = cdBanco;
        this._has_cdBanco = true;
    } //-- void setCdBanco(int) 

    /**
     * Sets the value of field 'cdConta'.
     * 
     * @param cdConta the value of field 'cdConta'.
     */
    public void setCdConta(long cdConta)
    {
        this._cdConta = cdConta;
        this._has_cdConta = true;
    } //-- void setCdConta(long) 

    /**
     * Sets the value of field 'cdControlePagamento'.
     * 
     * @param cdControlePagamento the value of field
     * 'cdControlePagamento'.
     */
    public void setCdControlePagamento(java.lang.String cdControlePagamento)
    {
        this._cdControlePagamento = cdControlePagamento;
    } //-- void setCdControlePagamento(java.lang.String) 

    /**
     * Sets the value of field 'cdDigitoAgencia'.
     * 
     * @param cdDigitoAgencia the value of field 'cdDigitoAgencia'.
     */
    public void setCdDigitoAgencia(int cdDigitoAgencia)
    {
        this._cdDigitoAgencia = cdDigitoAgencia;
        this._has_cdDigitoAgencia = true;
    } //-- void setCdDigitoAgencia(int) 

    /**
     * Sets the value of field 'cdDigitoConta'.
     * 
     * @param cdDigitoConta the value of field 'cdDigitoConta'.
     */
    public void setCdDigitoConta(java.lang.String cdDigitoConta)
    {
        this._cdDigitoConta = cdDigitoConta;
    } //-- void setCdDigitoConta(java.lang.String) 

    /**
     * Sets the value of field 'cdFavorecidoBeneficiario'.
     * 
     * @param cdFavorecidoBeneficiario the value of field
     * 'cdFavorecidoBeneficiario'.
     */
    public void setCdFavorecidoBeneficiario(long cdFavorecidoBeneficiario)
    {
        this._cdFavorecidoBeneficiario = cdFavorecidoBeneficiario;
        this._has_cdFavorecidoBeneficiario = true;
    } //-- void setCdFavorecidoBeneficiario(long) 

    /**
     * Sets the value of field 'cdMotivoSituacaoPagamento'.
     * 
     * @param cdMotivoSituacaoPagamento the value of field
     * 'cdMotivoSituacaoPagamento'.
     */
    public void setCdMotivoSituacaoPagamento(int cdMotivoSituacaoPagamento)
    {
        this._cdMotivoSituacaoPagamento = cdMotivoSituacaoPagamento;
        this._has_cdMotivoSituacaoPagamento = true;
    } //-- void setCdMotivoSituacaoPagamento(int) 

    /**
     * Sets the value of field 'cdSituacaoOperacaoPagamento'.
     * 
     * @param cdSituacaoOperacaoPagamento the value of field
     * 'cdSituacaoOperacaoPagamento'.
     */
    public void setCdSituacaoOperacaoPagamento(int cdSituacaoOperacaoPagamento)
    {
        this._cdSituacaoOperacaoPagamento = cdSituacaoOperacaoPagamento;
        this._has_cdSituacaoOperacaoPagamento = true;
    } //-- void setCdSituacaoOperacaoPagamento(int) 

    /**
     * Sets the value of field 'cdSituacaoPagamentoLista'.
     * 
     * @param cdSituacaoPagamentoLista the value of field
     * 'cdSituacaoPagamentoLista'.
     */
    public void setCdSituacaoPagamentoLista(int cdSituacaoPagamentoLista)
    {
        this._cdSituacaoPagamentoLista = cdSituacaoPagamentoLista;
        this._has_cdSituacaoPagamentoLista = true;
    } //-- void setCdSituacaoPagamentoLista(int) 

    /**
     * Sets the value of field 'cdTabelaConsulta'.
     * 
     * @param cdTabelaConsulta the value of field 'cdTabelaConsulta'
     */
    public void setCdTabelaConsulta(int cdTabelaConsulta)
    {
        this._cdTabelaConsulta = cdTabelaConsulta;
        this._has_cdTabelaConsulta = true;
    } //-- void setCdTabelaConsulta(int) 

    /**
     * Sets the value of field 'cdTipoTela'.
     * 
     * @param cdTipoTela the value of field 'cdTipoTela'.
     */
    public void setCdTipoTela(int cdTipoTela)
    {
        this._cdTipoTela = cdTipoTela;
        this._has_cdTipoTela = true;
    } //-- void setCdTipoTela(int) 

    /**
     * Sets the value of field 'dsAgencia'.
     * 
     * @param dsAgencia the value of field 'dsAgencia'.
     */
    public void setDsAgencia(java.lang.String dsAgencia)
    {
        this._dsAgencia = dsAgencia;
    } //-- void setDsAgencia(java.lang.String) 

    /**
     * Sets the value of field 'dsBanco'.
     * 
     * @param dsBanco the value of field 'dsBanco'.
     */
    public void setDsBanco(java.lang.String dsBanco)
    {
        this._dsBanco = dsBanco;
    } //-- void setDsBanco(java.lang.String) 

    /**
     * Sets the value of field 'dsFavorecidoBeneficiario'.
     * 
     * @param dsFavorecidoBeneficiario the value of field
     * 'dsFavorecidoBeneficiario'.
     */
    public void setDsFavorecidoBeneficiario(java.lang.String dsFavorecidoBeneficiario)
    {
        this._dsFavorecidoBeneficiario = dsFavorecidoBeneficiario;
    } //-- void setDsFavorecidoBeneficiario(java.lang.String) 

    /**
     * Sets the value of field 'dsMotivoPagamento'.
     * 
     * @param dsMotivoPagamento the value of field
     * 'dsMotivoPagamento'.
     */
    public void setDsMotivoPagamento(java.lang.String dsMotivoPagamento)
    {
        this._dsMotivoPagamento = dsMotivoPagamento;
    } //-- void setDsMotivoPagamento(java.lang.String) 

    /**
     * Sets the value of field 'dsSituacaoPagamento'.
     * 
     * @param dsSituacaoPagamento the value of field
     * 'dsSituacaoPagamento'.
     */
    public void setDsSituacaoPagamento(java.lang.String dsSituacaoPagamento)
    {
        this._dsSituacaoPagamento = dsSituacaoPagamento;
    } //-- void setDsSituacaoPagamento(java.lang.String) 

    /**
     * Sets the value of field 'dsSituacaoPagamentoLista'.
     * 
     * @param dsSituacaoPagamentoLista the value of field
     * 'dsSituacaoPagamentoLista'.
     */
    public void setDsSituacaoPagamentoLista(java.lang.String dsSituacaoPagamentoLista)
    {
        this._dsSituacaoPagamentoLista = dsSituacaoPagamentoLista;
    } //-- void setDsSituacaoPagamentoLista(java.lang.String) 

    /**
     * Sets the value of field 'dtEfetivacaoPagamento'.
     * 
     * @param dtEfetivacaoPagamento the value of field
     * 'dtEfetivacaoPagamento'.
     */
    public void setDtEfetivacaoPagamento(java.lang.String dtEfetivacaoPagamento)
    {
        this._dtEfetivacaoPagamento = dtEfetivacaoPagamento;
    } //-- void setDtEfetivacaoPagamento(java.lang.String) 

    /**
     * Sets the value of field 'vlPagamento'.
     * 
     * @param vlPagamento the value of field 'vlPagamento'.
     */
    public void setVlPagamento(java.math.BigDecimal vlPagamento)
    {
        this._vlPagamento = vlPagamento;
    } //-- void setVlPagamento(java.math.BigDecimal) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.detalharlistadebito.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.detalharlistadebito.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.detalharlistadebito.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharlistadebito.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
