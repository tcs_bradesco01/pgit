/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantercontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantercontrato.bean;

/**
 * Nome: ValidarPropostaAndamentoEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ValidarPropostaAndamentoEntradaDTO {
	
	/** Atributo cdpessoaJuridicaContrato. */
	private Long cdpessoaJuridicaContrato;
	
	/** Atributo cdTipoContratoNegocio. */
	private Integer cdTipoContratoNegocio;
	
	/** Atributo nrSequenciaContratoNegocio. */
	private Long nrSequenciaContratoNegocio;
    
    /** Atributo cdCpfCnpjPssoa. */
    private Long cdCpfCnpjPssoa;
    
    /** Atributo cdFilialCnpjPssoa. */
    private Integer cdFilialCnpjPssoa;
    
    /** Atributo cdControleCpfPssoa. */
    private Integer cdControleCpfPssoa;
    
	/**
	 * Get: cdpessoaJuridicaContrato.
	 *
	 * @return cdpessoaJuridicaContrato
	 */
	public Long getCdpessoaJuridicaContrato() {
		return cdpessoaJuridicaContrato;
	}

	/**
	 * Set: cdpessoaJuridicaContrato.
	 *
	 * @param cdpessoaJuridicaContrato the cdpessoa juridica contrato
	 */
	public void setCdpessoaJuridicaContrato(Long cdpessoaJuridicaContrato) {
		this.cdpessoaJuridicaContrato = cdpessoaJuridicaContrato;
	}

	/**
	 * Get: cdTipoContratoNegocio.
	 *
	 * @return cdTipoContratoNegocio
	 */
	public Integer getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}

	/**
	 * Set: cdTipoContratoNegocio.
	 *
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}

	/**
	 * Get: nrSequenciaContratoNegocio.
	 *
	 * @return nrSequenciaContratoNegocio
	 */
	public Long getNrSequenciaContratoNegocio() {
		return nrSequenciaContratoNegocio;
	}

	/**
	 * Set: nrSequenciaContratoNegocio.
	 *
	 * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
	 */
	public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}

	/**
	 * Get: cdCpfCnpjPssoa.
	 *
	 * @return cdCpfCnpjPssoa
	 */
	public Long getCdCpfCnpjPssoa() {
		return cdCpfCnpjPssoa;
	}

	/**
	 * Set: cdCpfCnpjPssoa.
	 *
	 * @param cdCpfCnpjPssoa the cd cpf cnpj pssoa
	 */
	public void setCdCpfCnpjPssoa(Long cdCpfCnpjPssoa) {
		this.cdCpfCnpjPssoa = cdCpfCnpjPssoa;
	}

	/**
	 * Get: cdFilialCnpjPssoa.
	 *
	 * @return cdFilialCnpjPssoa
	 */
	public Integer getCdFilialCnpjPssoa() {
		return cdFilialCnpjPssoa;
	}

	/**
	 * Set: cdFilialCnpjPssoa.
	 *
	 * @param cdFilialCnpjPssoa the cd filial cnpj pssoa
	 */
	public void setCdFilialCnpjPssoa(Integer cdFilialCnpjPssoa) {
		this.cdFilialCnpjPssoa = cdFilialCnpjPssoa;
	}

	/**
	 * Get: cdControleCpfPssoa.
	 *
	 * @return cdControleCpfPssoa
	 */
	public Integer getCdControleCpfPssoa() {
		return cdControleCpfPssoa;
	}

	/**
	 * Set: cdControleCpfPssoa.
	 *
	 * @param cdControleCpfPssoa the cd controle cpf pssoa
	 */
	public void setCdControleCpfPssoa(Integer cdControleCpfPssoa) {
		this.cdControleCpfPssoa = cdControleCpfPssoa;
	}

}
