/*
 * Nome: br.com.bradesco.web.pgit.service.business.solemissaocomppagtoclipag.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.solemissaocomppagtoclipag.bean;

/**
 * Nome: ConsultarAgenciaOpeTarifaPadraoEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarAgenciaOpeTarifaPadraoEntradaDTO {

	/** Atributo cdPessoaJuridicaEmpresa. */
	private Long cdPessoaJuridicaEmpresa;
	
	/** Atributo cdTipoContratoNegocio. */
	private Integer cdTipoContratoNegocio;
	
	/** Atributo nrSequenciaContratoNegocio. */
	private Long nrSequenciaContratoNegocio;
	
	/** Atributo cdProdutoServicoOperacao. */
	private Integer cdProdutoServicoOperacao;
	
	/** Atributo cdProdutoServicoRelacionado. */
	private Integer cdProdutoServicoRelacionado;
	
	/** Atributo cdTipoTarifa. */
	private Integer cdTipoTarifa;

	/**
	 * Get: cdTipoTarifa.
	 *
	 * @return cdTipoTarifa
	 */
	public Integer getCdTipoTarifa() {
		return cdTipoTarifa;
	}

	/**
	 * Set: cdTipoTarifa.
	 *
	 * @param cdTipoTarifa the cd tipo tarifa
	 */
	public void setCdTipoTarifa(Integer cdTipoTarifa) {
		this.cdTipoTarifa = cdTipoTarifa;
	}

	/**
	 * Get: cdPessoaJuridicaEmpresa.
	 *
	 * @return cdPessoaJuridicaEmpresa
	 */
	public Long getCdPessoaJuridicaEmpresa() {
		return cdPessoaJuridicaEmpresa;
	}

	/**
	 * Set: cdPessoaJuridicaEmpresa.
	 *
	 * @param cdPessoaJuridicaEmpresa the cd pessoa juridica empresa
	 */
	public void setCdPessoaJuridicaEmpresa(Long cdPessoaJuridicaEmpresa) {
		this.cdPessoaJuridicaEmpresa = cdPessoaJuridicaEmpresa;
	}

	/**
	 * Get: cdProdutoServicoOperacao.
	 *
	 * @return cdProdutoServicoOperacao
	 */
	public Integer getCdProdutoServicoOperacao() {
		return cdProdutoServicoOperacao;
	}

	/**
	 * Set: cdProdutoServicoOperacao.
	 *
	 * @param cdProdutoServicoOperacao the cd produto servico operacao
	 */
	public void setCdProdutoServicoOperacao(Integer cdProdutoServicoOperacao) {
		this.cdProdutoServicoOperacao = cdProdutoServicoOperacao;
	}

	/**
	 * Get: cdProdutoServicoRelacionado.
	 *
	 * @return cdProdutoServicoRelacionado
	 */
	public Integer getCdProdutoServicoRelacionado() {
		return cdProdutoServicoRelacionado;
	}

	/**
	 * Set: cdProdutoServicoRelacionado.
	 *
	 * @param cdProdutoServicoRelacionado the cd produto servico relacionado
	 */
	public void setCdProdutoServicoRelacionado(
			Integer cdProdutoServicoRelacionado) {
		this.cdProdutoServicoRelacionado = cdProdutoServicoRelacionado;
	}

	/**
	 * Get: cdTipoContratoNegocio.
	 *
	 * @return cdTipoContratoNegocio
	 */
	public Integer getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}

	/**
	 * Set: cdTipoContratoNegocio.
	 *
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}

	/**
	 * Get: nrSequenciaContratoNegocio.
	 *
	 * @return nrSequenciaContratoNegocio
	 */
	public Long getNrSequenciaContratoNegocio() {
		return nrSequenciaContratoNegocio;
	}

	/**
	 * Set: nrSequenciaContratoNegocio.
	 *
	 * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
	 */
	public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}
}
