/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listaraplictvformttipolayoutarqv.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ListarAplictvFormtTipoLayoutArqvRequest.
 * 
 * @version $Revision$ $Date$
 */
public class ListarAplictvFormtTipoLayoutArqvRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _nrOcorrencias
     */
    private int _nrOcorrencias = 0;

    /**
     * keeps track of state for field: _nrOcorrencias
     */
    private boolean _has_nrOcorrencias;

    /**
     * Field _cdSituacaoVinculacaoConta
     */
    private int _cdSituacaoVinculacaoConta = 0;

    /**
     * keeps track of state for field: _cdSituacaoVinculacaoConta
     */
    private boolean _has_cdSituacaoVinculacaoConta;

    /**
     * Field _cdAplictvTransmPgto
     */
    private int _cdAplictvTransmPgto = 0;

    /**
     * keeps track of state for field: _cdAplictvTransmPgto
     */
    private boolean _has_cdAplictvTransmPgto;

    /**
     * Field _cdSerie
     */
    private int _cdSerie = 0;

    /**
     * keeps track of state for field: _cdSerie
     */
    private boolean _has_cdSerie;


      //----------------/
     //- Constructors -/
    //----------------/

    public ListarAplictvFormtTipoLayoutArqvRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listaraplictvformttipolayoutarqv.request.ListarAplictvFormtTipoLayoutArqvRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdAplictvTransmPgto
     * 
     */
    public void deleteCdAplictvTransmPgto()
    {
        this._has_cdAplictvTransmPgto= false;
    } //-- void deleteCdAplictvTransmPgto() 

    /**
     * Method deleteCdSerie
     * 
     */
    public void deleteCdSerie()
    {
        this._has_cdSerie= false;
    } //-- void deleteCdSerie() 

    /**
     * Method deleteCdSituacaoVinculacaoConta
     * 
     */
    public void deleteCdSituacaoVinculacaoConta()
    {
        this._has_cdSituacaoVinculacaoConta= false;
    } //-- void deleteCdSituacaoVinculacaoConta() 

    /**
     * Method deleteNrOcorrencias
     * 
     */
    public void deleteNrOcorrencias()
    {
        this._has_nrOcorrencias= false;
    } //-- void deleteNrOcorrencias() 

    /**
     * Returns the value of field 'cdAplictvTransmPgto'.
     * 
     * @return int
     * @return the value of field 'cdAplictvTransmPgto'.
     */
    public int getCdAplictvTransmPgto()
    {
        return this._cdAplictvTransmPgto;
    } //-- int getCdAplictvTransmPgto() 

    /**
     * Returns the value of field 'cdSerie'.
     * 
     * @return int
     * @return the value of field 'cdSerie'.
     */
    public int getCdSerie()
    {
        return this._cdSerie;
    } //-- int getCdSerie() 

    /**
     * Returns the value of field 'cdSituacaoVinculacaoConta'.
     * 
     * @return int
     * @return the value of field 'cdSituacaoVinculacaoConta'.
     */
    public int getCdSituacaoVinculacaoConta()
    {
        return this._cdSituacaoVinculacaoConta;
    } //-- int getCdSituacaoVinculacaoConta() 

    /**
     * Returns the value of field 'nrOcorrencias'.
     * 
     * @return int
     * @return the value of field 'nrOcorrencias'.
     */
    public int getNrOcorrencias()
    {
        return this._nrOcorrencias;
    } //-- int getNrOcorrencias() 

    /**
     * Method hasCdAplictvTransmPgto
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAplictvTransmPgto()
    {
        return this._has_cdAplictvTransmPgto;
    } //-- boolean hasCdAplictvTransmPgto() 

    /**
     * Method hasCdSerie
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSerie()
    {
        return this._has_cdSerie;
    } //-- boolean hasCdSerie() 

    /**
     * Method hasCdSituacaoVinculacaoConta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSituacaoVinculacaoConta()
    {
        return this._has_cdSituacaoVinculacaoConta;
    } //-- boolean hasCdSituacaoVinculacaoConta() 

    /**
     * Method hasNrOcorrencias
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrOcorrencias()
    {
        return this._has_nrOcorrencias;
    } //-- boolean hasNrOcorrencias() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdAplictvTransmPgto'.
     * 
     * @param cdAplictvTransmPgto the value of field
     * 'cdAplictvTransmPgto'.
     */
    public void setCdAplictvTransmPgto(int cdAplictvTransmPgto)
    {
        this._cdAplictvTransmPgto = cdAplictvTransmPgto;
        this._has_cdAplictvTransmPgto = true;
    } //-- void setCdAplictvTransmPgto(int) 

    /**
     * Sets the value of field 'cdSerie'.
     * 
     * @param cdSerie the value of field 'cdSerie'.
     */
    public void setCdSerie(int cdSerie)
    {
        this._cdSerie = cdSerie;
        this._has_cdSerie = true;
    } //-- void setCdSerie(int) 

    /**
     * Sets the value of field 'cdSituacaoVinculacaoConta'.
     * 
     * @param cdSituacaoVinculacaoConta the value of field
     * 'cdSituacaoVinculacaoConta'.
     */
    public void setCdSituacaoVinculacaoConta(int cdSituacaoVinculacaoConta)
    {
        this._cdSituacaoVinculacaoConta = cdSituacaoVinculacaoConta;
        this._has_cdSituacaoVinculacaoConta = true;
    } //-- void setCdSituacaoVinculacaoConta(int) 

    /**
     * Sets the value of field 'nrOcorrencias'.
     * 
     * @param nrOcorrencias the value of field 'nrOcorrencias'.
     */
    public void setNrOcorrencias(int nrOcorrencias)
    {
        this._nrOcorrencias = nrOcorrencias;
        this._has_nrOcorrencias = true;
    } //-- void setNrOcorrencias(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ListarAplictvFormtTipoLayoutArqvRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listaraplictvformttipolayoutarqv.request.ListarAplictvFormtTipoLayoutArqvRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listaraplictvformttipolayoutarqv.request.ListarAplictvFormtTipoLayoutArqvRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listaraplictvformttipolayoutarqv.request.ListarAplictvFormtTipoLayoutArqvRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listaraplictvformttipolayoutarqv.request.ListarAplictvFormtTipoLayoutArqvRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
