/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantercontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantercontrato.bean;

import java.math.BigDecimal;

/**
 * Nome: IncluirLayoutEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class IncluirLayoutEntradaDTO{
    
    /** Atributo cdPessoaJuridicaNegocio. */
    private Long cdPessoaJuridicaNegocio;
    
    /** Atributo cdTipoContratoNegocio. */
    private Integer cdTipoContratoNegocio;
    
    /** Atributo nrSequenciaContratoNegocio. */
    private Long nrSequenciaContratoNegocio;
    
    /** Atributo cdTipoLayoutArquivo. */
    private Integer cdTipoLayoutArquivo;
    
    /** Atributo cdSituacaoLayoutContrato. */
    private Integer cdSituacaoLayoutContrato;
    
    /** Atributo cdResponsavelCustoEmpresa. */
    private Integer cdResponsavelCustoEmpresa;
    
    /** Atributo percentualCustoOrganizacaoTransmissao. */
    private BigDecimal percentualCustoOrganizacaoTransmissao;
    
    /** Atributo cdMensagemLinhaExtrato. */
    private Integer cdMensagemLinhaExtrato;

    /** Atributo numeroVersaoLayoutArquivo. */
    private Integer numeroVersaoLayoutArquivo;
    
	/**
	 * Get: cdPessoaJuridicaNegocio.
	 *
	 * @return cdPessoaJuridicaNegocio
	 */
	public Long getCdPessoaJuridicaNegocio() {
		return cdPessoaJuridicaNegocio;
	}
	
	/**
	 * Set: cdPessoaJuridicaNegocio.
	 *
	 * @param cdPessoaJuridicaNegocio the cd pessoa juridica negocio
	 */
	public void setCdPessoaJuridicaNegocio(Long cdPessoaJuridicaNegocio) {
		this.cdPessoaJuridicaNegocio = cdPessoaJuridicaNegocio;
	}
	
	/**
	 * Get: cdResponsavelCustoEmpresa.
	 *
	 * @return cdResponsavelCustoEmpresa
	 */
	public Integer getCdResponsavelCustoEmpresa() {
		return cdResponsavelCustoEmpresa;
	}
	
	/**
	 * Set: cdResponsavelCustoEmpresa.
	 *
	 * @param cdResponsavelCustoEmpresa the cd responsavel custo empresa
	 */
	public void setCdResponsavelCustoEmpresa(Integer cdResponsavelCustoEmpresa) {
		this.cdResponsavelCustoEmpresa = cdResponsavelCustoEmpresa;
	}
	
	/**
	 * Get: cdSituacaoLayoutContrato.
	 *
	 * @return cdSituacaoLayoutContrato
	 */
	public Integer getCdSituacaoLayoutContrato() {
		return cdSituacaoLayoutContrato;
	}
	
	/**
	 * Set: cdSituacaoLayoutContrato.
	 *
	 * @param cdSituacaoLayoutContrato the cd situacao layout contrato
	 */
	public void setCdSituacaoLayoutContrato(Integer cdSituacaoLayoutContrato) {
		this.cdSituacaoLayoutContrato = cdSituacaoLayoutContrato;
	}
	
	/**
	 * Get: cdTipoContratoNegocio.
	 *
	 * @return cdTipoContratoNegocio
	 */
	public Integer getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}
	
	/**
	 * Set: cdTipoContratoNegocio.
	 *
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}
	
	/**
	 * Get: cdTipoLayoutArquivo.
	 *
	 * @return cdTipoLayoutArquivo
	 */
	public Integer getCdTipoLayoutArquivo() {
		return cdTipoLayoutArquivo;
	}
	
	/**
	 * Set: cdTipoLayoutArquivo.
	 *
	 * @param cdTipoLayoutArquivo the cd tipo layout arquivo
	 */
	public void setCdTipoLayoutArquivo(Integer cdTipoLayoutArquivo) {
		this.cdTipoLayoutArquivo = cdTipoLayoutArquivo;
	}
	
	/**
	 * Get: nrSequenciaContratoNegocio.
	 *
	 * @return nrSequenciaContratoNegocio
	 */
	public Long getNrSequenciaContratoNegocio() {
		return nrSequenciaContratoNegocio;
	}
	
	/**
	 * Set: nrSequenciaContratoNegocio.
	 *
	 * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
	 */
	public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}
	
	/**
	 * Get: percentualCustoOrganizacaoTransmissao.
	 *
	 * @return percentualCustoOrganizacaoTransmissao
	 */
	public BigDecimal getPercentualCustoOrganizacaoTransmissao() {
		return percentualCustoOrganizacaoTransmissao;
	}
	
	/**
	 * Set: percentualCustoOrganizacaoTransmissao.
	 *
	 * @param percentualCustoOrganizacaoTransmissao the percentual custo organizacao transmissao
	 */
	public void setPercentualCustoOrganizacaoTransmissao(
			BigDecimal percentualCustoOrganizacaoTransmissao) {
		this.percentualCustoOrganizacaoTransmissao = percentualCustoOrganizacaoTransmissao;
	}
	
	/**
	 * Get: cdMensagemLinhaExtrato.
	 *
	 * @return cdMensagemLinhaExtrato
	 */
	public Integer getCdMensagemLinhaExtrato() {
		return cdMensagemLinhaExtrato;
	}
	
	/**
	 * Set: cdMensagemLinhaExtrato.
	 *
	 * @param cdMensagemLinhaExtrato the cd mensagem linha extrato
	 */
	public void setCdMensagemLinhaExtrato(Integer cdMensagemLinhaExtrato) {
		this.cdMensagemLinhaExtrato = cdMensagemLinhaExtrato;
	}

	/**
	 * Nome: setNumeroVersaoLayoutArquivo
	 *
	 * @exception
	 * @throws
	 * @param numeroVersaoLayoutArquivo
	 */
	public void setNumeroVersaoLayoutArquivo(Integer numeroVersaoLayoutArquivo) {
		this.numeroVersaoLayoutArquivo = numeroVersaoLayoutArquivo;
	}

	/**
	 * Nome: getNumeroVersaoLayoutArquivo
	 *
	 * @exception
	 * @throws
	 * @return numeroVersaoLayoutArquivo
	 */
	public Integer getNumeroVersaoLayoutArquivo() {
		return numeroVersaoLayoutArquivo;
	}
	
}