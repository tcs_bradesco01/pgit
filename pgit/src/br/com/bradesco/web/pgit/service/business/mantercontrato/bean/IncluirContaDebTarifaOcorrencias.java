package br.com.bradesco.web.pgit.service.business.mantercontrato.bean;

public class IncluirContaDebTarifaOcorrencias {
	
	 private Integer cdProdutoServicoOperacao;
     private Integer cdProdutoOperacaoRelacionado;
     private Long cdServicoCompostoPagamento;
     private Integer cdOperacaoProdutoServico;
     private Integer cdOperacaoServicoIntegrado;
     private Integer cdNaturezaOperacaoPagamento;
     
	public Integer getCdProdutoServicoOperacao() {
		return cdProdutoServicoOperacao;
	}
	public void setCdProdutoServicoOperacao(Integer cdProdutoServicoOperacao) {
		this.cdProdutoServicoOperacao = cdProdutoServicoOperacao;
	}
	public Integer getCdProdutoOperacaoRelacionado() {
		return cdProdutoOperacaoRelacionado;
	}
	public void setCdProdutoOperacaoRelacionado(Integer cdProdutoOperacaoRelacionado) {
		this.cdProdutoOperacaoRelacionado = cdProdutoOperacaoRelacionado;
	}
	public Long getCdServicoCompostoPagamento() {
		return cdServicoCompostoPagamento;
	}
	public void setCdServicoCompostoPagamento(Long cdServicoCompostoPagamento) {
		this.cdServicoCompostoPagamento = cdServicoCompostoPagamento;
	}
	public Integer getCdOperacaoProdutoServico() {
		return cdOperacaoProdutoServico;
	}
	public void setCdOperacaoProdutoServico(Integer cdOperacaoProdutoServico) {
		this.cdOperacaoProdutoServico = cdOperacaoProdutoServico;
	}
	public Integer getCdOperacaoServicoIntegrado() {
		return cdOperacaoServicoIntegrado;
	}
	public void setCdOperacaoServicoIntegrado(Integer cdOperacaoServicoIntegrado) {
		this.cdOperacaoServicoIntegrado = cdOperacaoServicoIntegrado;
	}
	public Integer getCdNaturezaOperacaoPagamento() {
		return cdNaturezaOperacaoPagamento;
	}
	public void setCdNaturezaOperacaoPagamento(Integer cdNaturezaOperacaoPagamento) {
		this.cdNaturezaOperacaoPagamento = cdNaturezaOperacaoPagamento;
	}

}
