/*
 * Nome: br.com.bradesco.web.pgit.service.business.combo.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.combo.bean;

/**
 * Nome: MotivoSituacaoPendenciaContratoEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class MotivoSituacaoPendenciaContratoEntradaDTO {
	
	/** Atributo cdSituacaoPendenciaContrato. */
	private int cdSituacaoPendenciaContrato;

	/**
	 * Get: cdSituacaoPendenciaContrato.
	 *
	 * @return cdSituacaoPendenciaContrato
	 */
	public int getCdSituacaoPendenciaContrato() {
		return cdSituacaoPendenciaContrato;
	}

	/**
	 * Set: cdSituacaoPendenciaContrato.
	 *
	 * @param cdSituacaoPendenciaContrato the cd situacao pendencia contrato
	 */
	public void setCdSituacaoPendenciaContrato(int cdSituacaoPendenciaContrato) {
		this.cdSituacaoPendenciaContrato = cdSituacaoPendenciaContrato;
	}
	
	
	

}
