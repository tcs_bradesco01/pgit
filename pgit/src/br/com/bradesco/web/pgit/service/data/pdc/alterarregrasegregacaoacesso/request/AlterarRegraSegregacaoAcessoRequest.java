/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.alterarregrasegregacaoacesso.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/


import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;


/**
 * Class AlterarRegraSegregacaoAcessoRequest.
 * 
 * @version $Revision$ $Date$
 */
public class AlterarRegraSegregacaoAcessoRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdRegraAcessoDado
     */
    private int _cdRegraAcessoDado = 0;

    /**
     * keeps track of state for field: _cdRegraAcessoDado
     */
    private boolean _has_cdRegraAcessoDado;

    /**
     * Field _cdTipoUnidadeUsuario
     */
    private int _cdTipoUnidadeUsuario = 0;

    /**
     * keeps track of state for field: _cdTipoUnidadeUsuario
     */
    private boolean _has_cdTipoUnidadeUsuario;

    /**
     * Field _cdTipoUnidadeProprietario
     */
    private int _cdTipoUnidadeProprietario = 0;

    /**
     * keeps track of state for field: _cdTipoUnidadeProprietario
     */
    private boolean _has_cdTipoUnidadeProprietario;

    /**
     * Field _cdIndicadorAcao
     */
    private int _cdIndicadorAcao = 0;

    /**
     * keeps track of state for field: _cdIndicadorAcao
     */
    private boolean _has_cdIndicadorAcao;

    /**
     * Field _cdNivelPermissaoAcesso
     */
    private int _cdNivelPermissaoAcesso = 0;

    /**
     * keeps track of state for field: _cdNivelPermissaoAcesso
     */
    private boolean _has_cdNivelPermissaoAcesso;


      //----------------/
     //- Constructors -/
    //----------------/

    public AlterarRegraSegregacaoAcessoRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.alterarregrasegregacaoacesso.request.AlterarRegraSegregacaoAcessoRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdIndicadorAcao
     * 
     */
    public void deleteCdIndicadorAcao()
    {
        this._has_cdIndicadorAcao= false;
    } //-- void deleteCdIndicadorAcao() 

    /**
     * Method deleteCdNivelPermissaoAcesso
     * 
     */
    public void deleteCdNivelPermissaoAcesso()
    {
        this._has_cdNivelPermissaoAcesso= false;
    } //-- void deleteCdNivelPermissaoAcesso() 

    /**
     * Method deleteCdRegraAcessoDado
     * 
     */
    public void deleteCdRegraAcessoDado()
    {
        this._has_cdRegraAcessoDado= false;
    } //-- void deleteCdRegraAcessoDado() 

    /**
     * Method deleteCdTipoUnidadeProprietario
     * 
     */
    public void deleteCdTipoUnidadeProprietario()
    {
        this._has_cdTipoUnidadeProprietario= false;
    } //-- void deleteCdTipoUnidadeProprietario() 

    /**
     * Method deleteCdTipoUnidadeUsuario
     * 
     */
    public void deleteCdTipoUnidadeUsuario()
    {
        this._has_cdTipoUnidadeUsuario= false;
    } //-- void deleteCdTipoUnidadeUsuario() 

    /**
     * Returns the value of field 'cdIndicadorAcao'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorAcao'.
     */
    public int getCdIndicadorAcao()
    {
        return this._cdIndicadorAcao;
    } //-- int getCdIndicadorAcao() 

    /**
     * Returns the value of field 'cdNivelPermissaoAcesso'.
     * 
     * @return int
     * @return the value of field 'cdNivelPermissaoAcesso'.
     */
    public int getCdNivelPermissaoAcesso()
    {
        return this._cdNivelPermissaoAcesso;
    } //-- int getCdNivelPermissaoAcesso() 

    /**
     * Returns the value of field 'cdRegraAcessoDado'.
     * 
     * @return int
     * @return the value of field 'cdRegraAcessoDado'.
     */
    public int getCdRegraAcessoDado()
    {
        return this._cdRegraAcessoDado;
    } //-- int getCdRegraAcessoDado() 

    /**
     * Returns the value of field 'cdTipoUnidadeProprietario'.
     * 
     * @return int
     * @return the value of field 'cdTipoUnidadeProprietario'.
     */
    public int getCdTipoUnidadeProprietario()
    {
        return this._cdTipoUnidadeProprietario;
    } //-- int getCdTipoUnidadeProprietario() 

    /**
     * Returns the value of field 'cdTipoUnidadeUsuario'.
     * 
     * @return int
     * @return the value of field 'cdTipoUnidadeUsuario'.
     */
    public int getCdTipoUnidadeUsuario()
    {
        return this._cdTipoUnidadeUsuario;
    } //-- int getCdTipoUnidadeUsuario() 

    /**
     * Method hasCdIndicadorAcao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorAcao()
    {
        return this._has_cdIndicadorAcao;
    } //-- boolean hasCdIndicadorAcao() 

    /**
     * Method hasCdNivelPermissaoAcesso
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdNivelPermissaoAcesso()
    {
        return this._has_cdNivelPermissaoAcesso;
    } //-- boolean hasCdNivelPermissaoAcesso() 

    /**
     * Method hasCdRegraAcessoDado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdRegraAcessoDado()
    {
        return this._has_cdRegraAcessoDado;
    } //-- boolean hasCdRegraAcessoDado() 

    /**
     * Method hasCdTipoUnidadeProprietario
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoUnidadeProprietario()
    {
        return this._has_cdTipoUnidadeProprietario;
    } //-- boolean hasCdTipoUnidadeProprietario() 

    /**
     * Method hasCdTipoUnidadeUsuario
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoUnidadeUsuario()
    {
        return this._has_cdTipoUnidadeUsuario;
    } //-- boolean hasCdTipoUnidadeUsuario() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdIndicadorAcao'.
     * 
     * @param cdIndicadorAcao the value of field 'cdIndicadorAcao'.
     */
    public void setCdIndicadorAcao(int cdIndicadorAcao)
    {
        this._cdIndicadorAcao = cdIndicadorAcao;
        this._has_cdIndicadorAcao = true;
    } //-- void setCdIndicadorAcao(int) 

    /**
     * Sets the value of field 'cdNivelPermissaoAcesso'.
     * 
     * @param cdNivelPermissaoAcesso the value of field
     * 'cdNivelPermissaoAcesso'.
     */
    public void setCdNivelPermissaoAcesso(int cdNivelPermissaoAcesso)
    {
        this._cdNivelPermissaoAcesso = cdNivelPermissaoAcesso;
        this._has_cdNivelPermissaoAcesso = true;
    } //-- void setCdNivelPermissaoAcesso(int) 

    /**
     * Sets the value of field 'cdRegraAcessoDado'.
     * 
     * @param cdRegraAcessoDado the value of field
     * 'cdRegraAcessoDado'.
     */
    public void setCdRegraAcessoDado(int cdRegraAcessoDado)
    {
        this._cdRegraAcessoDado = cdRegraAcessoDado;
        this._has_cdRegraAcessoDado = true;
    } //-- void setCdRegraAcessoDado(int) 

    /**
     * Sets the value of field 'cdTipoUnidadeProprietario'.
     * 
     * @param cdTipoUnidadeProprietario the value of field
     * 'cdTipoUnidadeProprietario'.
     */
    public void setCdTipoUnidadeProprietario(int cdTipoUnidadeProprietario)
    {
        this._cdTipoUnidadeProprietario = cdTipoUnidadeProprietario;
        this._has_cdTipoUnidadeProprietario = true;
    } //-- void setCdTipoUnidadeProprietario(int) 

    /**
     * Sets the value of field 'cdTipoUnidadeUsuario'.
     * 
     * @param cdTipoUnidadeUsuario the value of field
     * 'cdTipoUnidadeUsuario'.
     */
    public void setCdTipoUnidadeUsuario(int cdTipoUnidadeUsuario)
    {
        this._cdTipoUnidadeUsuario = cdTipoUnidadeUsuario;
        this._has_cdTipoUnidadeUsuario = true;
    } //-- void setCdTipoUnidadeUsuario(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return AlterarRegraSegregacaoAcessoRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.alterarregrasegregacaoacesso.request.AlterarRegraSegregacaoAcessoRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.alterarregrasegregacaoacesso.request.AlterarRegraSegregacaoAcessoRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.alterarregrasegregacaoacesso.request.AlterarRegraSegregacaoAcessoRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.alterarregrasegregacaoacesso.request.AlterarRegraSegregacaoAcessoRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
