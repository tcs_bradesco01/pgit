/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultarpagamentofavorecido.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.math.BigDecimal;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _corpoCfpCnpj
     */
    private long _corpoCfpCnpj = 0;

    /**
     * keeps track of state for field: _corpoCfpCnpj
     */
    private boolean _has_corpoCfpCnpj;

    /**
     * Field _filialCfpCnpj
     */
    private int _filialCfpCnpj = 0;

    /**
     * keeps track of state for field: _filialCfpCnpj
     */
    private boolean _has_filialCfpCnpj;

    /**
     * Field _controleCfpCnpj
     */
    private int _controleCfpCnpj = 0;

    /**
     * keeps track of state for field: _controleCfpCnpj
     */
    private boolean _has_controleCfpCnpj;

    /**
     * Field _cdPessoaJuridicaContrato
     */
    private long _cdPessoaJuridicaContrato = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridicaContrato
     */
    private boolean _has_cdPessoaJuridicaContrato;

    /**
     * Field _nmRazaoSocial
     */
    private java.lang.String _nmRazaoSocial;

    /**
     * Field _cdEmpresa
     */
    private long _cdEmpresa = 0;

    /**
     * keeps track of state for field: _cdEmpresa
     */
    private boolean _has_cdEmpresa;

    /**
     * Field _dsEmpresa
     */
    private java.lang.String _dsEmpresa;

    /**
     * Field _cdTipoContratoNegocio
     */
    private int _cdTipoContratoNegocio = 0;

    /**
     * keeps track of state for field: _cdTipoContratoNegocio
     */
    private boolean _has_cdTipoContratoNegocio;

    /**
     * Field _nrContrato
     */
    private long _nrContrato = 0;

    /**
     * keeps track of state for field: _nrContrato
     */
    private boolean _has_nrContrato;

    /**
     * Field _cdProdutoServicoOperacao
     */
    private int _cdProdutoServicoOperacao = 0;

    /**
     * keeps track of state for field: _cdProdutoServicoOperacao
     */
    private boolean _has_cdProdutoServicoOperacao;

    /**
     * Field _cdResumoProdutoServico
     */
    private java.lang.String _cdResumoProdutoServico;

    /**
     * Field _cdProdutoServicoRelacionado
     */
    private int _cdProdutoServicoRelacionado = 0;

    /**
     * keeps track of state for field: _cdProdutoServicoRelacionado
     */
    private boolean _has_cdProdutoServicoRelacionado;

    /**
     * Field _dsOperacaoProdutoServico
     */
    private java.lang.String _dsOperacaoProdutoServico;

    /**
     * Field _cdControlePagamento
     */
    private java.lang.String _cdControlePagamento;

    /**
     * Field _dtCraditoPagamento
     */
    private java.lang.String _dtCraditoPagamento;

    /**
     * Field _vlrEfetivacaoPagamentoCliente
     */
    private java.math.BigDecimal _vlrEfetivacaoPagamentoCliente = new java.math.BigDecimal("0");

    /**
     * Field _cdInscricaoFavorecido
     */
    private long _cdInscricaoFavorecido = 0;

    /**
     * keeps track of state for field: _cdInscricaoFavorecido
     */
    private boolean _has_cdInscricaoFavorecido;

    /**
     * Field _dsAbrevFavorecido
     */
    private java.lang.String _dsAbrevFavorecido;

    /**
     * Field _cdBancoDebito
     */
    private int _cdBancoDebito = 0;

    /**
     * keeps track of state for field: _cdBancoDebito
     */
    private boolean _has_cdBancoDebito;

    /**
     * Field _cdAgenciaDebito
     */
    private int _cdAgenciaDebito = 0;

    /**
     * keeps track of state for field: _cdAgenciaDebito
     */
    private boolean _has_cdAgenciaDebito;

    /**
     * Field _cdDigitoAgenciaDebito
     */
    private int _cdDigitoAgenciaDebito = 0;

    /**
     * keeps track of state for field: _cdDigitoAgenciaDebito
     */
    private boolean _has_cdDigitoAgenciaDebito;

    /**
     * Field _cdContaDebito
     */
    private long _cdContaDebito = 0;

    /**
     * keeps track of state for field: _cdContaDebito
     */
    private boolean _has_cdContaDebito;

    /**
     * Field _cdDigitoContaDebito
     */
    private java.lang.String _cdDigitoContaDebito;

    /**
     * Field _cdBancoCredito
     */
    private int _cdBancoCredito = 0;

    /**
     * keeps track of state for field: _cdBancoCredito
     */
    private boolean _has_cdBancoCredito;

    /**
     * Field _cdAgenciaCredito
     */
    private int _cdAgenciaCredito = 0;

    /**
     * keeps track of state for field: _cdAgenciaCredito
     */
    private boolean _has_cdAgenciaCredito;

    /**
     * Field _cdDigitoAgenciaCredito
     */
    private int _cdDigitoAgenciaCredito = 0;

    /**
     * keeps track of state for field: _cdDigitoAgenciaCredito
     */
    private boolean _has_cdDigitoAgenciaCredito;

    /**
     * Field _cdContaCredito
     */
    private long _cdContaCredito = 0;

    /**
     * keeps track of state for field: _cdContaCredito
     */
    private boolean _has_cdContaCredito;

    /**
     * Field _cdDigitoContaCredito
     */
    private java.lang.String _cdDigitoContaCredito;

    /**
     * Field _cdSituacaoOperacaoPagamento
     */
    private int _cdSituacaoOperacaoPagamento = 0;

    /**
     * keeps track of state for field: _cdSituacaoOperacaoPagamento
     */
    private boolean _has_cdSituacaoOperacaoPagamento;

    /**
     * Field _cdMotivoSituacaoPagamento
     */
    private java.lang.String _cdMotivoSituacaoPagamento;

    /**
     * Field _cdTipoCanal
     */
    private int _cdTipoCanal = 0;

    /**
     * keeps track of state for field: _cdTipoCanal
     */
    private boolean _has_cdTipoCanal;

    /**
     * Field _dsTipoTela
     */
    private int _dsTipoTela = 0;

    /**
     * keeps track of state for field: _dsTipoTela
     */
    private boolean _has_dsTipoTela;

    /**
     * Field _dsEfetivacaoPagamento
     */
    private java.lang.String _dsEfetivacaoPagamento;

    /**
     * Field _dsIndicadorPagamento
     */
    private java.lang.String _dsIndicadorPagamento;

    /**
     * Field _cdBancoSalarial
     */
    private int _cdBancoSalarial = 0;

    /**
     * keeps track of state for field: _cdBancoSalarial
     */
    private boolean _has_cdBancoSalarial;

    /**
     * Field _cdAgenciaSalarial
     */
    private int _cdAgenciaSalarial = 0;

    /**
     * keeps track of state for field: _cdAgenciaSalarial
     */
    private boolean _has_cdAgenciaSalarial;

    /**
     * Field _cdDigitoAgenciaSalarial
     */
    private int _cdDigitoAgenciaSalarial = 0;

    /**
     * keeps track of state for field: _cdDigitoAgenciaSalarial
     */
    private boolean _has_cdDigitoAgenciaSalarial;

    /**
     * Field _cdContaSalarial
     */
    private long _cdContaSalarial = 0;

    /**
     * keeps track of state for field: _cdContaSalarial
     */
    private boolean _has_cdContaSalarial;

    /**
     * Field _cdDigitoContaSalarial
     */
    private java.lang.String _cdDigitoContaSalarial;

    /**
     * Field _cdIspbPagtoDestino
     */
    private java.lang.String _cdIspbPagtoDestino;

    /**
     * Field _contaPagtoDestino
     */
    private java.lang.String _contaPagtoDestino = "0";


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
        setVlrEfetivacaoPagamentoCliente(new java.math.BigDecimal("0"));
        setContaPagtoDestino("0");
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarpagamentofavorecido.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdAgenciaCredito
     * 
     */
    public void deleteCdAgenciaCredito()
    {
        this._has_cdAgenciaCredito= false;
    } //-- void deleteCdAgenciaCredito() 

    /**
     * Method deleteCdAgenciaDebito
     * 
     */
    public void deleteCdAgenciaDebito()
    {
        this._has_cdAgenciaDebito= false;
    } //-- void deleteCdAgenciaDebito() 

    /**
     * Method deleteCdAgenciaSalarial
     * 
     */
    public void deleteCdAgenciaSalarial()
    {
        this._has_cdAgenciaSalarial= false;
    } //-- void deleteCdAgenciaSalarial() 

    /**
     * Method deleteCdBancoCredito
     * 
     */
    public void deleteCdBancoCredito()
    {
        this._has_cdBancoCredito= false;
    } //-- void deleteCdBancoCredito() 

    /**
     * Method deleteCdBancoDebito
     * 
     */
    public void deleteCdBancoDebito()
    {
        this._has_cdBancoDebito= false;
    } //-- void deleteCdBancoDebito() 

    /**
     * Method deleteCdBancoSalarial
     * 
     */
    public void deleteCdBancoSalarial()
    {
        this._has_cdBancoSalarial= false;
    } //-- void deleteCdBancoSalarial() 

    /**
     * Method deleteCdContaCredito
     * 
     */
    public void deleteCdContaCredito()
    {
        this._has_cdContaCredito= false;
    } //-- void deleteCdContaCredito() 

    /**
     * Method deleteCdContaDebito
     * 
     */
    public void deleteCdContaDebito()
    {
        this._has_cdContaDebito= false;
    } //-- void deleteCdContaDebito() 

    /**
     * Method deleteCdContaSalarial
     * 
     */
    public void deleteCdContaSalarial()
    {
        this._has_cdContaSalarial= false;
    } //-- void deleteCdContaSalarial() 

    /**
     * Method deleteCdDigitoAgenciaCredito
     * 
     */
    public void deleteCdDigitoAgenciaCredito()
    {
        this._has_cdDigitoAgenciaCredito= false;
    } //-- void deleteCdDigitoAgenciaCredito() 

    /**
     * Method deleteCdDigitoAgenciaDebito
     * 
     */
    public void deleteCdDigitoAgenciaDebito()
    {
        this._has_cdDigitoAgenciaDebito= false;
    } //-- void deleteCdDigitoAgenciaDebito() 

    /**
     * Method deleteCdDigitoAgenciaSalarial
     * 
     */
    public void deleteCdDigitoAgenciaSalarial()
    {
        this._has_cdDigitoAgenciaSalarial= false;
    } //-- void deleteCdDigitoAgenciaSalarial() 

    /**
     * Method deleteCdEmpresa
     * 
     */
    public void deleteCdEmpresa()
    {
        this._has_cdEmpresa= false;
    } //-- void deleteCdEmpresa() 

    /**
     * Method deleteCdInscricaoFavorecido
     * 
     */
    public void deleteCdInscricaoFavorecido()
    {
        this._has_cdInscricaoFavorecido= false;
    } //-- void deleteCdInscricaoFavorecido() 

    /**
     * Method deleteCdPessoaJuridicaContrato
     * 
     */
    public void deleteCdPessoaJuridicaContrato()
    {
        this._has_cdPessoaJuridicaContrato= false;
    } //-- void deleteCdPessoaJuridicaContrato() 

    /**
     * Method deleteCdProdutoServicoOperacao
     * 
     */
    public void deleteCdProdutoServicoOperacao()
    {
        this._has_cdProdutoServicoOperacao= false;
    } //-- void deleteCdProdutoServicoOperacao() 

    /**
     * Method deleteCdProdutoServicoRelacionado
     * 
     */
    public void deleteCdProdutoServicoRelacionado()
    {
        this._has_cdProdutoServicoRelacionado= false;
    } //-- void deleteCdProdutoServicoRelacionado() 

    /**
     * Method deleteCdSituacaoOperacaoPagamento
     * 
     */
    public void deleteCdSituacaoOperacaoPagamento()
    {
        this._has_cdSituacaoOperacaoPagamento= false;
    } //-- void deleteCdSituacaoOperacaoPagamento() 

    /**
     * Method deleteCdTipoCanal
     * 
     */
    public void deleteCdTipoCanal()
    {
        this._has_cdTipoCanal= false;
    } //-- void deleteCdTipoCanal() 

    /**
     * Method deleteCdTipoContratoNegocio
     * 
     */
    public void deleteCdTipoContratoNegocio()
    {
        this._has_cdTipoContratoNegocio= false;
    } //-- void deleteCdTipoContratoNegocio() 

    /**
     * Method deleteControleCfpCnpj
     * 
     */
    public void deleteControleCfpCnpj()
    {
        this._has_controleCfpCnpj= false;
    } //-- void deleteControleCfpCnpj() 

    /**
     * Method deleteCorpoCfpCnpj
     * 
     */
    public void deleteCorpoCfpCnpj()
    {
        this._has_corpoCfpCnpj= false;
    } //-- void deleteCorpoCfpCnpj() 

    /**
     * Method deleteDsTipoTela
     * 
     */
    public void deleteDsTipoTela()
    {
        this._has_dsTipoTela= false;
    } //-- void deleteDsTipoTela() 

    /**
     * Method deleteFilialCfpCnpj
     * 
     */
    public void deleteFilialCfpCnpj()
    {
        this._has_filialCfpCnpj= false;
    } //-- void deleteFilialCfpCnpj() 

    /**
     * Method deleteNrContrato
     * 
     */
    public void deleteNrContrato()
    {
        this._has_nrContrato= false;
    } //-- void deleteNrContrato() 

    /**
     * Returns the value of field 'cdAgenciaCredito'.
     * 
     * @return int
     * @return the value of field 'cdAgenciaCredito'.
     */
    public int getCdAgenciaCredito()
    {
        return this._cdAgenciaCredito;
    } //-- int getCdAgenciaCredito() 

    /**
     * Returns the value of field 'cdAgenciaDebito'.
     * 
     * @return int
     * @return the value of field 'cdAgenciaDebito'.
     */
    public int getCdAgenciaDebito()
    {
        return this._cdAgenciaDebito;
    } //-- int getCdAgenciaDebito() 

    /**
     * Returns the value of field 'cdAgenciaSalarial'.
     * 
     * @return int
     * @return the value of field 'cdAgenciaSalarial'.
     */
    public int getCdAgenciaSalarial()
    {
        return this._cdAgenciaSalarial;
    } //-- int getCdAgenciaSalarial() 

    /**
     * Returns the value of field 'cdBancoCredito'.
     * 
     * @return int
     * @return the value of field 'cdBancoCredito'.
     */
    public int getCdBancoCredito()
    {
        return this._cdBancoCredito;
    } //-- int getCdBancoCredito() 

    /**
     * Returns the value of field 'cdBancoDebito'.
     * 
     * @return int
     * @return the value of field 'cdBancoDebito'.
     */
    public int getCdBancoDebito()
    {
        return this._cdBancoDebito;
    } //-- int getCdBancoDebito() 

    /**
     * Returns the value of field 'cdBancoSalarial'.
     * 
     * @return int
     * @return the value of field 'cdBancoSalarial'.
     */
    public int getCdBancoSalarial()
    {
        return this._cdBancoSalarial;
    } //-- int getCdBancoSalarial() 

    /**
     * Returns the value of field 'cdContaCredito'.
     * 
     * @return long
     * @return the value of field 'cdContaCredito'.
     */
    public long getCdContaCredito()
    {
        return this._cdContaCredito;
    } //-- long getCdContaCredito() 

    /**
     * Returns the value of field 'cdContaDebito'.
     * 
     * @return long
     * @return the value of field 'cdContaDebito'.
     */
    public long getCdContaDebito()
    {
        return this._cdContaDebito;
    } //-- long getCdContaDebito() 

    /**
     * Returns the value of field 'cdContaSalarial'.
     * 
     * @return long
     * @return the value of field 'cdContaSalarial'.
     */
    public long getCdContaSalarial()
    {
        return this._cdContaSalarial;
    } //-- long getCdContaSalarial() 

    /**
     * Returns the value of field 'cdControlePagamento'.
     * 
     * @return String
     * @return the value of field 'cdControlePagamento'.
     */
    public java.lang.String getCdControlePagamento()
    {
        return this._cdControlePagamento;
    } //-- java.lang.String getCdControlePagamento() 

    /**
     * Returns the value of field 'cdDigitoAgenciaCredito'.
     * 
     * @return int
     * @return the value of field 'cdDigitoAgenciaCredito'.
     */
    public int getCdDigitoAgenciaCredito()
    {
        return this._cdDigitoAgenciaCredito;
    } //-- int getCdDigitoAgenciaCredito() 

    /**
     * Returns the value of field 'cdDigitoAgenciaDebito'.
     * 
     * @return int
     * @return the value of field 'cdDigitoAgenciaDebito'.
     */
    public int getCdDigitoAgenciaDebito()
    {
        return this._cdDigitoAgenciaDebito;
    } //-- int getCdDigitoAgenciaDebito() 

    /**
     * Returns the value of field 'cdDigitoAgenciaSalarial'.
     * 
     * @return int
     * @return the value of field 'cdDigitoAgenciaSalarial'.
     */
    public int getCdDigitoAgenciaSalarial()
    {
        return this._cdDigitoAgenciaSalarial;
    } //-- int getCdDigitoAgenciaSalarial() 

    /**
     * Returns the value of field 'cdDigitoContaCredito'.
     * 
     * @return String
     * @return the value of field 'cdDigitoContaCredito'.
     */
    public java.lang.String getCdDigitoContaCredito()
    {
        return this._cdDigitoContaCredito;
    } //-- java.lang.String getCdDigitoContaCredito() 

    /**
     * Returns the value of field 'cdDigitoContaDebito'.
     * 
     * @return String
     * @return the value of field 'cdDigitoContaDebito'.
     */
    public java.lang.String getCdDigitoContaDebito()
    {
        return this._cdDigitoContaDebito;
    } //-- java.lang.String getCdDigitoContaDebito() 

    /**
     * Returns the value of field 'cdDigitoContaSalarial'.
     * 
     * @return String
     * @return the value of field 'cdDigitoContaSalarial'.
     */
    public java.lang.String getCdDigitoContaSalarial()
    {
        return this._cdDigitoContaSalarial;
    } //-- java.lang.String getCdDigitoContaSalarial() 

    /**
     * Returns the value of field 'cdEmpresa'.
     * 
     * @return long
     * @return the value of field 'cdEmpresa'.
     */
    public long getCdEmpresa()
    {
        return this._cdEmpresa;
    } //-- long getCdEmpresa() 

    /**
     * Returns the value of field 'cdInscricaoFavorecido'.
     * 
     * @return long
     * @return the value of field 'cdInscricaoFavorecido'.
     */
    public long getCdInscricaoFavorecido()
    {
        return this._cdInscricaoFavorecido;
    } //-- long getCdInscricaoFavorecido() 

    /**
     * Returns the value of field 'cdIspbPagtoDestino'.
     * 
     * @return String
     * @return the value of field 'cdIspbPagtoDestino'.
     */
    public java.lang.String getCdIspbPagtoDestino()
    {
        return this._cdIspbPagtoDestino;
    } //-- java.lang.String getCdIspbPagtoDestino() 

    /**
     * Returns the value of field 'cdMotivoSituacaoPagamento'.
     * 
     * @return String
     * @return the value of field 'cdMotivoSituacaoPagamento'.
     */
    public java.lang.String getCdMotivoSituacaoPagamento()
    {
        return this._cdMotivoSituacaoPagamento;
    } //-- java.lang.String getCdMotivoSituacaoPagamento() 

    /**
     * Returns the value of field 'cdPessoaJuridicaContrato'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridicaContrato'.
     */
    public long getCdPessoaJuridicaContrato()
    {
        return this._cdPessoaJuridicaContrato;
    } //-- long getCdPessoaJuridicaContrato() 

    /**
     * Returns the value of field 'cdProdutoServicoOperacao'.
     * 
     * @return int
     * @return the value of field 'cdProdutoServicoOperacao'.
     */
    public int getCdProdutoServicoOperacao()
    {
        return this._cdProdutoServicoOperacao;
    } //-- int getCdProdutoServicoOperacao() 

    /**
     * Returns the value of field 'cdProdutoServicoRelacionado'.
     * 
     * @return int
     * @return the value of field 'cdProdutoServicoRelacionado'.
     */
    public int getCdProdutoServicoRelacionado()
    {
        return this._cdProdutoServicoRelacionado;
    } //-- int getCdProdutoServicoRelacionado() 

    /**
     * Returns the value of field 'cdResumoProdutoServico'.
     * 
     * @return String
     * @return the value of field 'cdResumoProdutoServico'.
     */
    public java.lang.String getCdResumoProdutoServico()
    {
        return this._cdResumoProdutoServico;
    } //-- java.lang.String getCdResumoProdutoServico() 

    /**
     * Returns the value of field 'cdSituacaoOperacaoPagamento'.
     * 
     * @return int
     * @return the value of field 'cdSituacaoOperacaoPagamento'.
     */
    public int getCdSituacaoOperacaoPagamento()
    {
        return this._cdSituacaoOperacaoPagamento;
    } //-- int getCdSituacaoOperacaoPagamento() 

    /**
     * Returns the value of field 'cdTipoCanal'.
     * 
     * @return int
     * @return the value of field 'cdTipoCanal'.
     */
    public int getCdTipoCanal()
    {
        return this._cdTipoCanal;
    } //-- int getCdTipoCanal() 

    /**
     * Returns the value of field 'cdTipoContratoNegocio'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoNegocio'.
     */
    public int getCdTipoContratoNegocio()
    {
        return this._cdTipoContratoNegocio;
    } //-- int getCdTipoContratoNegocio() 

    /**
     * Returns the value of field 'contaPagtoDestino'.
     * 
     * @return String
     * @return the value of field 'contaPagtoDestino'.
     */
    public java.lang.String getContaPagtoDestino()
    {
        return this._contaPagtoDestino;
    } //-- java.lang.String getContaPagtoDestino() 

    /**
     * Returns the value of field 'controleCfpCnpj'.
     * 
     * @return int
     * @return the value of field 'controleCfpCnpj'.
     */
    public int getControleCfpCnpj()
    {
        return this._controleCfpCnpj;
    } //-- int getControleCfpCnpj() 

    /**
     * Returns the value of field 'corpoCfpCnpj'.
     * 
     * @return long
     * @return the value of field 'corpoCfpCnpj'.
     */
    public long getCorpoCfpCnpj()
    {
        return this._corpoCfpCnpj;
    } //-- long getCorpoCfpCnpj() 

    /**
     * Returns the value of field 'dsAbrevFavorecido'.
     * 
     * @return String
     * @return the value of field 'dsAbrevFavorecido'.
     */
    public java.lang.String getDsAbrevFavorecido()
    {
        return this._dsAbrevFavorecido;
    } //-- java.lang.String getDsAbrevFavorecido() 

    /**
     * Returns the value of field 'dsEfetivacaoPagamento'.
     * 
     * @return String
     * @return the value of field 'dsEfetivacaoPagamento'.
     */
    public java.lang.String getDsEfetivacaoPagamento()
    {
        return this._dsEfetivacaoPagamento;
    } //-- java.lang.String getDsEfetivacaoPagamento() 

    /**
     * Returns the value of field 'dsEmpresa'.
     * 
     * @return String
     * @return the value of field 'dsEmpresa'.
     */
    public java.lang.String getDsEmpresa()
    {
        return this._dsEmpresa;
    } //-- java.lang.String getDsEmpresa() 

    /**
     * Returns the value of field 'dsIndicadorPagamento'.
     * 
     * @return String
     * @return the value of field 'dsIndicadorPagamento'.
     */
    public java.lang.String getDsIndicadorPagamento()
    {
        return this._dsIndicadorPagamento;
    } //-- java.lang.String getDsIndicadorPagamento() 

    /**
     * Returns the value of field 'dsOperacaoProdutoServico'.
     * 
     * @return String
     * @return the value of field 'dsOperacaoProdutoServico'.
     */
    public java.lang.String getDsOperacaoProdutoServico()
    {
        return this._dsOperacaoProdutoServico;
    } //-- java.lang.String getDsOperacaoProdutoServico() 

    /**
     * Returns the value of field 'dsTipoTela'.
     * 
     * @return int
     * @return the value of field 'dsTipoTela'.
     */
    public int getDsTipoTela()
    {
        return this._dsTipoTela;
    } //-- int getDsTipoTela() 

    /**
     * Returns the value of field 'dtCraditoPagamento'.
     * 
     * @return String
     * @return the value of field 'dtCraditoPagamento'.
     */
    public java.lang.String getDtCraditoPagamento()
    {
        return this._dtCraditoPagamento;
    } //-- java.lang.String getDtCraditoPagamento() 

    /**
     * Returns the value of field 'filialCfpCnpj'.
     * 
     * @return int
     * @return the value of field 'filialCfpCnpj'.
     */
    public int getFilialCfpCnpj()
    {
        return this._filialCfpCnpj;
    } //-- int getFilialCfpCnpj() 

    /**
     * Returns the value of field 'nmRazaoSocial'.
     * 
     * @return String
     * @return the value of field 'nmRazaoSocial'.
     */
    public java.lang.String getNmRazaoSocial()
    {
        return this._nmRazaoSocial;
    } //-- java.lang.String getNmRazaoSocial() 

    /**
     * Returns the value of field 'nrContrato'.
     * 
     * @return long
     * @return the value of field 'nrContrato'.
     */
    public long getNrContrato()
    {
        return this._nrContrato;
    } //-- long getNrContrato() 

    /**
     * Returns the value of field 'vlrEfetivacaoPagamentoCliente'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlrEfetivacaoPagamentoCliente'.
     */
    public java.math.BigDecimal getVlrEfetivacaoPagamentoCliente()
    {
        return this._vlrEfetivacaoPagamentoCliente;
    } //-- java.math.BigDecimal getVlrEfetivacaoPagamentoCliente() 

    /**
     * Method hasCdAgenciaCredito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgenciaCredito()
    {
        return this._has_cdAgenciaCredito;
    } //-- boolean hasCdAgenciaCredito() 

    /**
     * Method hasCdAgenciaDebito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgenciaDebito()
    {
        return this._has_cdAgenciaDebito;
    } //-- boolean hasCdAgenciaDebito() 

    /**
     * Method hasCdAgenciaSalarial
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgenciaSalarial()
    {
        return this._has_cdAgenciaSalarial;
    } //-- boolean hasCdAgenciaSalarial() 

    /**
     * Method hasCdBancoCredito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdBancoCredito()
    {
        return this._has_cdBancoCredito;
    } //-- boolean hasCdBancoCredito() 

    /**
     * Method hasCdBancoDebito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdBancoDebito()
    {
        return this._has_cdBancoDebito;
    } //-- boolean hasCdBancoDebito() 

    /**
     * Method hasCdBancoSalarial
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdBancoSalarial()
    {
        return this._has_cdBancoSalarial;
    } //-- boolean hasCdBancoSalarial() 

    /**
     * Method hasCdContaCredito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdContaCredito()
    {
        return this._has_cdContaCredito;
    } //-- boolean hasCdContaCredito() 

    /**
     * Method hasCdContaDebito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdContaDebito()
    {
        return this._has_cdContaDebito;
    } //-- boolean hasCdContaDebito() 

    /**
     * Method hasCdContaSalarial
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdContaSalarial()
    {
        return this._has_cdContaSalarial;
    } //-- boolean hasCdContaSalarial() 

    /**
     * Method hasCdDigitoAgenciaCredito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdDigitoAgenciaCredito()
    {
        return this._has_cdDigitoAgenciaCredito;
    } //-- boolean hasCdDigitoAgenciaCredito() 

    /**
     * Method hasCdDigitoAgenciaDebito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdDigitoAgenciaDebito()
    {
        return this._has_cdDigitoAgenciaDebito;
    } //-- boolean hasCdDigitoAgenciaDebito() 

    /**
     * Method hasCdDigitoAgenciaSalarial
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdDigitoAgenciaSalarial()
    {
        return this._has_cdDigitoAgenciaSalarial;
    } //-- boolean hasCdDigitoAgenciaSalarial() 

    /**
     * Method hasCdEmpresa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdEmpresa()
    {
        return this._has_cdEmpresa;
    } //-- boolean hasCdEmpresa() 

    /**
     * Method hasCdInscricaoFavorecido
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdInscricaoFavorecido()
    {
        return this._has_cdInscricaoFavorecido;
    } //-- boolean hasCdInscricaoFavorecido() 

    /**
     * Method hasCdPessoaJuridicaContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridicaContrato()
    {
        return this._has_cdPessoaJuridicaContrato;
    } //-- boolean hasCdPessoaJuridicaContrato() 

    /**
     * Method hasCdProdutoServicoOperacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdutoServicoOperacao()
    {
        return this._has_cdProdutoServicoOperacao;
    } //-- boolean hasCdProdutoServicoOperacao() 

    /**
     * Method hasCdProdutoServicoRelacionado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdutoServicoRelacionado()
    {
        return this._has_cdProdutoServicoRelacionado;
    } //-- boolean hasCdProdutoServicoRelacionado() 

    /**
     * Method hasCdSituacaoOperacaoPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSituacaoOperacaoPagamento()
    {
        return this._has_cdSituacaoOperacaoPagamento;
    } //-- boolean hasCdSituacaoOperacaoPagamento() 

    /**
     * Method hasCdTipoCanal
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoCanal()
    {
        return this._has_cdTipoCanal;
    } //-- boolean hasCdTipoCanal() 

    /**
     * Method hasCdTipoContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoNegocio()
    {
        return this._has_cdTipoContratoNegocio;
    } //-- boolean hasCdTipoContratoNegocio() 

    /**
     * Method hasControleCfpCnpj
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasControleCfpCnpj()
    {
        return this._has_controleCfpCnpj;
    } //-- boolean hasControleCfpCnpj() 

    /**
     * Method hasCorpoCfpCnpj
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCorpoCfpCnpj()
    {
        return this._has_corpoCfpCnpj;
    } //-- boolean hasCorpoCfpCnpj() 

    /**
     * Method hasDsTipoTela
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasDsTipoTela()
    {
        return this._has_dsTipoTela;
    } //-- boolean hasDsTipoTela() 

    /**
     * Method hasFilialCfpCnpj
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasFilialCfpCnpj()
    {
        return this._has_filialCfpCnpj;
    } //-- boolean hasFilialCfpCnpj() 

    /**
     * Method hasNrContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrContrato()
    {
        return this._has_nrContrato;
    } //-- boolean hasNrContrato() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdAgenciaCredito'.
     * 
     * @param cdAgenciaCredito the value of field 'cdAgenciaCredito'
     */
    public void setCdAgenciaCredito(int cdAgenciaCredito)
    {
        this._cdAgenciaCredito = cdAgenciaCredito;
        this._has_cdAgenciaCredito = true;
    } //-- void setCdAgenciaCredito(int) 

    /**
     * Sets the value of field 'cdAgenciaDebito'.
     * 
     * @param cdAgenciaDebito the value of field 'cdAgenciaDebito'.
     */
    public void setCdAgenciaDebito(int cdAgenciaDebito)
    {
        this._cdAgenciaDebito = cdAgenciaDebito;
        this._has_cdAgenciaDebito = true;
    } //-- void setCdAgenciaDebito(int) 

    /**
     * Sets the value of field 'cdAgenciaSalarial'.
     * 
     * @param cdAgenciaSalarial the value of field
     * 'cdAgenciaSalarial'.
     */
    public void setCdAgenciaSalarial(int cdAgenciaSalarial)
    {
        this._cdAgenciaSalarial = cdAgenciaSalarial;
        this._has_cdAgenciaSalarial = true;
    } //-- void setCdAgenciaSalarial(int) 

    /**
     * Sets the value of field 'cdBancoCredito'.
     * 
     * @param cdBancoCredito the value of field 'cdBancoCredito'.
     */
    public void setCdBancoCredito(int cdBancoCredito)
    {
        this._cdBancoCredito = cdBancoCredito;
        this._has_cdBancoCredito = true;
    } //-- void setCdBancoCredito(int) 

    /**
     * Sets the value of field 'cdBancoDebito'.
     * 
     * @param cdBancoDebito the value of field 'cdBancoDebito'.
     */
    public void setCdBancoDebito(int cdBancoDebito)
    {
        this._cdBancoDebito = cdBancoDebito;
        this._has_cdBancoDebito = true;
    } //-- void setCdBancoDebito(int) 

    /**
     * Sets the value of field 'cdBancoSalarial'.
     * 
     * @param cdBancoSalarial the value of field 'cdBancoSalarial'.
     */
    public void setCdBancoSalarial(int cdBancoSalarial)
    {
        this._cdBancoSalarial = cdBancoSalarial;
        this._has_cdBancoSalarial = true;
    } //-- void setCdBancoSalarial(int) 

    /**
     * Sets the value of field 'cdContaCredito'.
     * 
     * @param cdContaCredito the value of field 'cdContaCredito'.
     */
    public void setCdContaCredito(long cdContaCredito)
    {
        this._cdContaCredito = cdContaCredito;
        this._has_cdContaCredito = true;
    } //-- void setCdContaCredito(long) 

    /**
     * Sets the value of field 'cdContaDebito'.
     * 
     * @param cdContaDebito the value of field 'cdContaDebito'.
     */
    public void setCdContaDebito(long cdContaDebito)
    {
        this._cdContaDebito = cdContaDebito;
        this._has_cdContaDebito = true;
    } //-- void setCdContaDebito(long) 

    /**
     * Sets the value of field 'cdContaSalarial'.
     * 
     * @param cdContaSalarial the value of field 'cdContaSalarial'.
     */
    public void setCdContaSalarial(long cdContaSalarial)
    {
        this._cdContaSalarial = cdContaSalarial;
        this._has_cdContaSalarial = true;
    } //-- void setCdContaSalarial(long) 

    /**
     * Sets the value of field 'cdControlePagamento'.
     * 
     * @param cdControlePagamento the value of field
     * 'cdControlePagamento'.
     */
    public void setCdControlePagamento(java.lang.String cdControlePagamento)
    {
        this._cdControlePagamento = cdControlePagamento;
    } //-- void setCdControlePagamento(java.lang.String) 

    /**
     * Sets the value of field 'cdDigitoAgenciaCredito'.
     * 
     * @param cdDigitoAgenciaCredito the value of field
     * 'cdDigitoAgenciaCredito'.
     */
    public void setCdDigitoAgenciaCredito(int cdDigitoAgenciaCredito)
    {
        this._cdDigitoAgenciaCredito = cdDigitoAgenciaCredito;
        this._has_cdDigitoAgenciaCredito = true;
    } //-- void setCdDigitoAgenciaCredito(int) 

    /**
     * Sets the value of field 'cdDigitoAgenciaDebito'.
     * 
     * @param cdDigitoAgenciaDebito the value of field
     * 'cdDigitoAgenciaDebito'.
     */
    public void setCdDigitoAgenciaDebito(int cdDigitoAgenciaDebito)
    {
        this._cdDigitoAgenciaDebito = cdDigitoAgenciaDebito;
        this._has_cdDigitoAgenciaDebito = true;
    } //-- void setCdDigitoAgenciaDebito(int) 

    /**
     * Sets the value of field 'cdDigitoAgenciaSalarial'.
     * 
     * @param cdDigitoAgenciaSalarial the value of field
     * 'cdDigitoAgenciaSalarial'.
     */
    public void setCdDigitoAgenciaSalarial(int cdDigitoAgenciaSalarial)
    {
        this._cdDigitoAgenciaSalarial = cdDigitoAgenciaSalarial;
        this._has_cdDigitoAgenciaSalarial = true;
    } //-- void setCdDigitoAgenciaSalarial(int) 

    /**
     * Sets the value of field 'cdDigitoContaCredito'.
     * 
     * @param cdDigitoContaCredito the value of field
     * 'cdDigitoContaCredito'.
     */
    public void setCdDigitoContaCredito(java.lang.String cdDigitoContaCredito)
    {
        this._cdDigitoContaCredito = cdDigitoContaCredito;
    } //-- void setCdDigitoContaCredito(java.lang.String) 

    /**
     * Sets the value of field 'cdDigitoContaDebito'.
     * 
     * @param cdDigitoContaDebito the value of field
     * 'cdDigitoContaDebito'.
     */
    public void setCdDigitoContaDebito(java.lang.String cdDigitoContaDebito)
    {
        this._cdDigitoContaDebito = cdDigitoContaDebito;
    } //-- void setCdDigitoContaDebito(java.lang.String) 

    /**
     * Sets the value of field 'cdDigitoContaSalarial'.
     * 
     * @param cdDigitoContaSalarial the value of field
     * 'cdDigitoContaSalarial'.
     */
    public void setCdDigitoContaSalarial(java.lang.String cdDigitoContaSalarial)
    {
        this._cdDigitoContaSalarial = cdDigitoContaSalarial;
    } //-- void setCdDigitoContaSalarial(java.lang.String) 

    /**
     * Sets the value of field 'cdEmpresa'.
     * 
     * @param cdEmpresa the value of field 'cdEmpresa'.
     */
    public void setCdEmpresa(long cdEmpresa)
    {
        this._cdEmpresa = cdEmpresa;
        this._has_cdEmpresa = true;
    } //-- void setCdEmpresa(long) 

    /**
     * Sets the value of field 'cdInscricaoFavorecido'.
     * 
     * @param cdInscricaoFavorecido the value of field
     * 'cdInscricaoFavorecido'.
     */
    public void setCdInscricaoFavorecido(long cdInscricaoFavorecido)
    {
        this._cdInscricaoFavorecido = cdInscricaoFavorecido;
        this._has_cdInscricaoFavorecido = true;
    } //-- void setCdInscricaoFavorecido(long) 

    /**
     * Sets the value of field 'cdIspbPagtoDestino'.
     * 
     * @param cdIspbPagtoDestino the value of field
     * 'cdIspbPagtoDestino'.
     */
    public void setCdIspbPagtoDestino(java.lang.String cdIspbPagtoDestino)
    {
        this._cdIspbPagtoDestino = cdIspbPagtoDestino;
    } //-- void setCdIspbPagtoDestino(java.lang.String) 

    /**
     * Sets the value of field 'cdMotivoSituacaoPagamento'.
     * 
     * @param cdMotivoSituacaoPagamento the value of field
     * 'cdMotivoSituacaoPagamento'.
     */
    public void setCdMotivoSituacaoPagamento(java.lang.String cdMotivoSituacaoPagamento)
    {
        this._cdMotivoSituacaoPagamento = cdMotivoSituacaoPagamento;
    } //-- void setCdMotivoSituacaoPagamento(java.lang.String) 

    /**
     * Sets the value of field 'cdPessoaJuridicaContrato'.
     * 
     * @param cdPessoaJuridicaContrato the value of field
     * 'cdPessoaJuridicaContrato'.
     */
    public void setCdPessoaJuridicaContrato(long cdPessoaJuridicaContrato)
    {
        this._cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
        this._has_cdPessoaJuridicaContrato = true;
    } //-- void setCdPessoaJuridicaContrato(long) 

    /**
     * Sets the value of field 'cdProdutoServicoOperacao'.
     * 
     * @param cdProdutoServicoOperacao the value of field
     * 'cdProdutoServicoOperacao'.
     */
    public void setCdProdutoServicoOperacao(int cdProdutoServicoOperacao)
    {
        this._cdProdutoServicoOperacao = cdProdutoServicoOperacao;
        this._has_cdProdutoServicoOperacao = true;
    } //-- void setCdProdutoServicoOperacao(int) 

    /**
     * Sets the value of field 'cdProdutoServicoRelacionado'.
     * 
     * @param cdProdutoServicoRelacionado the value of field
     * 'cdProdutoServicoRelacionado'.
     */
    public void setCdProdutoServicoRelacionado(int cdProdutoServicoRelacionado)
    {
        this._cdProdutoServicoRelacionado = cdProdutoServicoRelacionado;
        this._has_cdProdutoServicoRelacionado = true;
    } //-- void setCdProdutoServicoRelacionado(int) 

    /**
     * Sets the value of field 'cdResumoProdutoServico'.
     * 
     * @param cdResumoProdutoServico the value of field
     * 'cdResumoProdutoServico'.
     */
    public void setCdResumoProdutoServico(java.lang.String cdResumoProdutoServico)
    {
        this._cdResumoProdutoServico = cdResumoProdutoServico;
    } //-- void setCdResumoProdutoServico(java.lang.String) 

    /**
     * Sets the value of field 'cdSituacaoOperacaoPagamento'.
     * 
     * @param cdSituacaoOperacaoPagamento the value of field
     * 'cdSituacaoOperacaoPagamento'.
     */
    public void setCdSituacaoOperacaoPagamento(int cdSituacaoOperacaoPagamento)
    {
        this._cdSituacaoOperacaoPagamento = cdSituacaoOperacaoPagamento;
        this._has_cdSituacaoOperacaoPagamento = true;
    } //-- void setCdSituacaoOperacaoPagamento(int) 

    /**
     * Sets the value of field 'cdTipoCanal'.
     * 
     * @param cdTipoCanal the value of field 'cdTipoCanal'.
     */
    public void setCdTipoCanal(int cdTipoCanal)
    {
        this._cdTipoCanal = cdTipoCanal;
        this._has_cdTipoCanal = true;
    } //-- void setCdTipoCanal(int) 

    /**
     * Sets the value of field 'cdTipoContratoNegocio'.
     * 
     * @param cdTipoContratoNegocio the value of field
     * 'cdTipoContratoNegocio'.
     */
    public void setCdTipoContratoNegocio(int cdTipoContratoNegocio)
    {
        this._cdTipoContratoNegocio = cdTipoContratoNegocio;
        this._has_cdTipoContratoNegocio = true;
    } //-- void setCdTipoContratoNegocio(int) 

    /**
     * Sets the value of field 'contaPagtoDestino'.
     * 
     * @param contaPagtoDestino the value of field
     * 'contaPagtoDestino'.
     */
    public void setContaPagtoDestino(java.lang.String contaPagtoDestino)
    {
        this._contaPagtoDestino = contaPagtoDestino;
    } //-- void setContaPagtoDestino(java.lang.String) 

    /**
     * Sets the value of field 'controleCfpCnpj'.
     * 
     * @param controleCfpCnpj the value of field 'controleCfpCnpj'.
     */
    public void setControleCfpCnpj(int controleCfpCnpj)
    {
        this._controleCfpCnpj = controleCfpCnpj;
        this._has_controleCfpCnpj = true;
    } //-- void setControleCfpCnpj(int) 

    /**
     * Sets the value of field 'corpoCfpCnpj'.
     * 
     * @param corpoCfpCnpj the value of field 'corpoCfpCnpj'.
     */
    public void setCorpoCfpCnpj(long corpoCfpCnpj)
    {
        this._corpoCfpCnpj = corpoCfpCnpj;
        this._has_corpoCfpCnpj = true;
    } //-- void setCorpoCfpCnpj(long) 

    /**
     * Sets the value of field 'dsAbrevFavorecido'.
     * 
     * @param dsAbrevFavorecido the value of field
     * 'dsAbrevFavorecido'.
     */
    public void setDsAbrevFavorecido(java.lang.String dsAbrevFavorecido)
    {
        this._dsAbrevFavorecido = dsAbrevFavorecido;
    } //-- void setDsAbrevFavorecido(java.lang.String) 

    /**
     * Sets the value of field 'dsEfetivacaoPagamento'.
     * 
     * @param dsEfetivacaoPagamento the value of field
     * 'dsEfetivacaoPagamento'.
     */
    public void setDsEfetivacaoPagamento(java.lang.String dsEfetivacaoPagamento)
    {
        this._dsEfetivacaoPagamento = dsEfetivacaoPagamento;
    } //-- void setDsEfetivacaoPagamento(java.lang.String) 

    /**
     * Sets the value of field 'dsEmpresa'.
     * 
     * @param dsEmpresa the value of field 'dsEmpresa'.
     */
    public void setDsEmpresa(java.lang.String dsEmpresa)
    {
        this._dsEmpresa = dsEmpresa;
    } //-- void setDsEmpresa(java.lang.String) 

    /**
     * Sets the value of field 'dsIndicadorPagamento'.
     * 
     * @param dsIndicadorPagamento the value of field
     * 'dsIndicadorPagamento'.
     */
    public void setDsIndicadorPagamento(java.lang.String dsIndicadorPagamento)
    {
        this._dsIndicadorPagamento = dsIndicadorPagamento;
    } //-- void setDsIndicadorPagamento(java.lang.String) 

    /**
     * Sets the value of field 'dsOperacaoProdutoServico'.
     * 
     * @param dsOperacaoProdutoServico the value of field
     * 'dsOperacaoProdutoServico'.
     */
    public void setDsOperacaoProdutoServico(java.lang.String dsOperacaoProdutoServico)
    {
        this._dsOperacaoProdutoServico = dsOperacaoProdutoServico;
    } //-- void setDsOperacaoProdutoServico(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoTela'.
     * 
     * @param dsTipoTela the value of field 'dsTipoTela'.
     */
    public void setDsTipoTela(int dsTipoTela)
    {
        this._dsTipoTela = dsTipoTela;
        this._has_dsTipoTela = true;
    } //-- void setDsTipoTela(int) 

    /**
     * Sets the value of field 'dtCraditoPagamento'.
     * 
     * @param dtCraditoPagamento the value of field
     * 'dtCraditoPagamento'.
     */
    public void setDtCraditoPagamento(java.lang.String dtCraditoPagamento)
    {
        this._dtCraditoPagamento = dtCraditoPagamento;
    } //-- void setDtCraditoPagamento(java.lang.String) 

    /**
     * Sets the value of field 'filialCfpCnpj'.
     * 
     * @param filialCfpCnpj the value of field 'filialCfpCnpj'.
     */
    public void setFilialCfpCnpj(int filialCfpCnpj)
    {
        this._filialCfpCnpj = filialCfpCnpj;
        this._has_filialCfpCnpj = true;
    } //-- void setFilialCfpCnpj(int) 

    /**
     * Sets the value of field 'nmRazaoSocial'.
     * 
     * @param nmRazaoSocial the value of field 'nmRazaoSocial'.
     */
    public void setNmRazaoSocial(java.lang.String nmRazaoSocial)
    {
        this._nmRazaoSocial = nmRazaoSocial;
    } //-- void setNmRazaoSocial(java.lang.String) 

    /**
     * Sets the value of field 'nrContrato'.
     * 
     * @param nrContrato the value of field 'nrContrato'.
     */
    public void setNrContrato(long nrContrato)
    {
        this._nrContrato = nrContrato;
        this._has_nrContrato = true;
    } //-- void setNrContrato(long) 

    /**
     * Sets the value of field 'vlrEfetivacaoPagamentoCliente'.
     * 
     * @param vlrEfetivacaoPagamentoCliente the value of field
     * 'vlrEfetivacaoPagamentoCliente'.
     */
    public void setVlrEfetivacaoPagamentoCliente(java.math.BigDecimal vlrEfetivacaoPagamentoCliente)
    {
        this._vlrEfetivacaoPagamentoCliente = vlrEfetivacaoPagamentoCliente;
    } //-- void setVlrEfetivacaoPagamentoCliente(java.math.BigDecimal) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultarpagamentofavorecido.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultarpagamentofavorecido.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultarpagamentofavorecido.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarpagamentofavorecido.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
