/*
 * Nome: br.com.bradesco.web.pgit.service.business.consultas.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 19/10/2016
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.consultas.bean;



/**
 * Nome: ListarTipoRetiradaEntradaDTO
 * <p>
 * Prop�sito:
 * </p>
 * .
 * 
 * @author : todo!
 * @version :
 */
public class ListarTipoRetiradaEntradaDTO {

    /** Atributo maxOcorrencias. */
    private Integer maxOcorrencias = null;

    /** Atributo cdpessoaJuridicaContrato. */
    private Long cdpessoaJuridicaContrato = null;

    /** Atributo cdTipoContratoNegocio. */
    private Integer cdTipoContratoNegocio = null;

    /** Atributo nrSequenciaContratoNegocio. */
    private Long nrSequenciaContratoNegocio = null;

    /** Atributo cdControlePagamento. */
    private String cdControlePagamento = null;

    /** Atributo cdIndentificadorSeqPagamento. */
    private String cdIndentificadorSeqPagamento = null;

    /** Atributo cdProdutoServicoOperacao. */
    private Integer cdProdutoServicoOperacao = null;

    /** Atributo cdProdutoOperacaoRelacionado. */
    private Integer cdProdutoOperacaoRelacionado = null;

    /** Atributo cdBancoPagador. */
    private Integer cdBancoPagador = null;

    /** Atributo cdAgenciaBancariaPagador. */
    private Integer cdAgenciaBancariaPagador = null;

    /** Atributo cdDigitoAgenciaPagador. */
    private String cdDigitoAgenciaPagador = null;

    /** Atributo cdContaBancariaPagador. */
    private Long cdContaBancariaPagador = null;

    /** Atributo cdDigitoContaPagador. */
    private String cdDigitoContaPagador = null;

    /** Atributo cdBancoFavorecido. */
    private Integer cdBancoFavorecido = null;

    /** Atributo cdAgenciaFavorecido. */
    private Integer cdAgenciaFavorecido = null;

    /** Atributo cdDigitoAgenciaFavorecido. */
    private String cdDigitoAgenciaFavorecido = null;

    /** Atributo cdContaFavorecido. */
    private Long cdContaFavorecido = null;

    /** Atributo cdDigitoContaFavorecido. */
    private String cdDigitoContaFavorecido = null;

    /** Atributo dtPagamentoDe. */
    private String dtPagamentoDe = null;

    /** Atributo dtPagamentoAte. */
    private String dtPagamentoAte = null;

    /**
     * Set: maxOcorrencias.
     * 
     * @param maxOcorrencias
     *            the max ocorrencias
     */
    public void setMaxOcorrencias(Integer maxOcorrencias) {
        this.maxOcorrencias = maxOcorrencias;
    }

    /**
     * Get: maxOcorrencias.
     * 
     * @return maxOcorrencias
     */
    public Integer getMaxOcorrencias() {
        return this.maxOcorrencias;
    }

    /**
     * Set: cdpessoaJuridicaContrato.
     * 
     * @param cdpessoaJuridicaContrato
     *            the cdpessoa juridica contrato
     */
    public void setCdpessoaJuridicaContrato(Long cdpessoaJuridicaContrato) {
        this.cdpessoaJuridicaContrato = cdpessoaJuridicaContrato;
    }

    /**
     * Get: cdpessoaJuridicaContrato.
     * 
     * @return cdpessoaJuridicaContrato
     */
    public Long getCdpessoaJuridicaContrato() {
        return this.cdpessoaJuridicaContrato;
    }

    /**
     * Set: cdTipoContratoNegocio.
     * 
     * @param cdTipoContratoNegocio
     *            the cd tipo contrato negocio
     */
    public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
        this.cdTipoContratoNegocio = cdTipoContratoNegocio;
    }

    /**
     * Get: cdTipoContratoNegocio.
     * 
     * @return cdTipoContratoNegocio
     */
    public Integer getCdTipoContratoNegocio() {
        return this.cdTipoContratoNegocio;
    }

    /**
     * Set: nrSequenciaContratoNegocio.
     * 
     * @param nrSequenciaContratoNegocio
     *            the nr sequencia contrato negocio
     */
    public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
        this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
    }

    /**
     * Get: nrSequenciaContratoNegocio.
     * 
     * @return nrSequenciaContratoNegocio
     */
    public Long getNrSequenciaContratoNegocio() {
        return this.nrSequenciaContratoNegocio;
    }

    /**
     * Set: cdControlePagamento.
     * 
     * @param cdControlePagamento
     *            the cd controle pagamento
     */
    public void setCdControlePagamento(String cdControlePagamento) {
        this.cdControlePagamento = cdControlePagamento;
    }

    /**
     * Get: cdControlePagamento.
     * 
     * @return cdControlePagamento
     */
    public String getCdControlePagamento() {
        return this.cdControlePagamento;
    }

    /**
     * Set: cdIndentificadorSeqPagamento.
     * 
     * @param cdIndentificadorSeqPagamento
     *            the cd indentificador seq pagamento
     */
    public void setCdIndentificadorSeqPagamento(String cdIndentificadorSeqPagamento) {
        this.cdIndentificadorSeqPagamento = cdIndentificadorSeqPagamento;
    }

    /**
     * Get: cdIndentificadorSeqPagamento.
     * 
     * @return cdIndentificadorSeqPagamento
     */
    public String getCdIndentificadorSeqPagamento() {
        return this.cdIndentificadorSeqPagamento;
    }

    /**
     * Set: cdProdutoServicoOperacao.
     * 
     * @param cdProdutoServicoOperacao
     *            the cd produto servico operacao
     */
    public void setCdProdutoServicoOperacao(Integer cdProdutoServicoOperacao) {
        this.cdProdutoServicoOperacao = cdProdutoServicoOperacao;
    }

    /**
     * Get: cdProdutoServicoOperacao.
     * 
     * @return cdProdutoServicoOperacao
     */
    public Integer getCdProdutoServicoOperacao() {
        return this.cdProdutoServicoOperacao;
    }

    /**
     * Set: cdProdutoOperacaoRelacionado.
     * 
     * @param cdProdutoOperacaoRelacionado
     *            the cd produto operacao relacionado
     */
    public void setCdProdutoOperacaoRelacionado(Integer cdProdutoOperacaoRelacionado) {
        this.cdProdutoOperacaoRelacionado = cdProdutoOperacaoRelacionado;
    }

    /**
     * Get: cdProdutoOperacaoRelacionado.
     * 
     * @return cdProdutoOperacaoRelacionado
     */
    public Integer getCdProdutoOperacaoRelacionado() {
        return this.cdProdutoOperacaoRelacionado;
    }

    /**
     * Set: cdBancoPagador.
     * 
     * @param cdBancoPagador
     *            the cd banco pagador
     */
    public void setCdBancoPagador(Integer cdBancoPagador) {
        this.cdBancoPagador = cdBancoPagador;
    }

    /**
     * Get: cdBancoPagador.
     * 
     * @return cdBancoPagador
     */
    public Integer getCdBancoPagador() {
        return this.cdBancoPagador;
    }

    /**
     * Set: cdAgenciaBancariaPagador.
     * 
     * @param cdAgenciaBancariaPagador
     *            the cd agencia bancaria pagador
     */
    public void setCdAgenciaBancariaPagador(Integer cdAgenciaBancariaPagador) {
        this.cdAgenciaBancariaPagador = cdAgenciaBancariaPagador;
    }

    /**
     * Get: cdAgenciaBancariaPagador.
     * 
     * @return cdAgenciaBancariaPagador
     */
    public Integer getCdAgenciaBancariaPagador() {
        return this.cdAgenciaBancariaPagador;
    }

    /**
     * Set: cdDigitoAgenciaPagador.
     * 
     * @param cdDigitoAgenciaPagador
     *            the cd digito agencia pagador
     */
    public void setCdDigitoAgenciaPagador(String cdDigitoAgenciaPagador) {
        this.cdDigitoAgenciaPagador = cdDigitoAgenciaPagador;
    }

    /**
     * Get: cdDigitoAgenciaPagador.
     * 
     * @return cdDigitoAgenciaPagador
     */
    public String getCdDigitoAgenciaPagador() {
        return this.cdDigitoAgenciaPagador;
    }

    /**
     * Set: cdContaBancariaPagador.
     * 
     * @param cdContaBancariaPagador
     *            the cd conta bancaria pagador
     */
    public void setCdContaBancariaPagador(Long cdContaBancariaPagador) {
        this.cdContaBancariaPagador = cdContaBancariaPagador;
    }

    /**
     * Get: cdContaBancariaPagador.
     * 
     * @return cdContaBancariaPagador
     */
    public Long getCdContaBancariaPagador() {
        return this.cdContaBancariaPagador;
    }

    /**
     * Set: cdDigitoContaPagador.
     * 
     * @param cdDigitoContaPagador
     *            the cd digito conta pagador
     */
    public void setCdDigitoContaPagador(String cdDigitoContaPagador) {
        this.cdDigitoContaPagador = cdDigitoContaPagador;
    }

    /**
     * Get: cdDigitoContaPagador.
     * 
     * @return cdDigitoContaPagador
     */
    public String getCdDigitoContaPagador() {
        return this.cdDigitoContaPagador;
    }

    /**
     * Set: cdBancoFavorecido.
     * 
     * @param cdBancoFavorecido
     *            the cd banco favorecido
     */
    public void setCdBancoFavorecido(Integer cdBancoFavorecido) {
        this.cdBancoFavorecido = cdBancoFavorecido;
    }

    /**
     * Get: cdBancoFavorecido.
     * 
     * @return cdBancoFavorecido
     */
    public Integer getCdBancoFavorecido() {
        return this.cdBancoFavorecido;
    }

    /**
     * Set: cdAgenciaFavorecido.
     * 
     * @param cdAgenciaFavorecido
     *            the cd agencia favorecido
     */
    public void setCdAgenciaFavorecido(Integer cdAgenciaFavorecido) {
        this.cdAgenciaFavorecido = cdAgenciaFavorecido;
    }

    /**
     * Get: cdAgenciaFavorecido.
     * 
     * @return cdAgenciaFavorecido
     */
    public Integer getCdAgenciaFavorecido() {
        return this.cdAgenciaFavorecido;
    }

    /**
     * Set: cdDigitoAgenciaFavorecido.
     * 
     * @param cdDigitoAgenciaFavorecido
     *            the cd digito agencia favorecido
     */
    public void setCdDigitoAgenciaFavorecido(String cdDigitoAgenciaFavorecido) {
        this.cdDigitoAgenciaFavorecido = cdDigitoAgenciaFavorecido;
    }

    /**
     * Get: cdDigitoAgenciaFavorecido.
     * 
     * @return cdDigitoAgenciaFavorecido
     */
    public String getCdDigitoAgenciaFavorecido() {
        return this.cdDigitoAgenciaFavorecido;
    }

    /**
     * Set: cdContaFavorecido.
     * 
     * @param cdContaFavorecido
     *            the cd conta favorecido
     */
    public void setCdContaFavorecido(Long cdContaFavorecido) {
        this.cdContaFavorecido = cdContaFavorecido;
    }

    /**
     * Get: cdContaFavorecido.
     * 
     * @return cdContaFavorecido
     */
    public Long getCdContaFavorecido() {
        return this.cdContaFavorecido;
    }

    /**
     * Set: cdDigitoContaFavorecido.
     * 
     * @param cdDigitoContaFavorecido
     *            the cd digito conta favorecido
     */
    public void setCdDigitoContaFavorecido(String cdDigitoContaFavorecido) {
        this.cdDigitoContaFavorecido = cdDigitoContaFavorecido;
    }

    /**
     * Get: cdDigitoContaFavorecido.
     * 
     * @return cdDigitoContaFavorecido
     */
    public String getCdDigitoContaFavorecido() {
        return this.cdDigitoContaFavorecido;
    }

    /**
     * Set: dtPagamentoDe.
     * 
     * @param dtPagamentoDe
     *            the dt pagamento de
     */
    public void setDtPagamentoDe(String dtPagamentoDe) {
        this.dtPagamentoDe = dtPagamentoDe;
    }

    /**
     * Get: dtPagamentoDe.
     * 
     * @return dtPagamentoDe
     */
    public String getDtPagamentoDe() {
        return this.dtPagamentoDe;
    }

    /**
     * Set: dtPagamentoAte.
     * 
     * @param dtPagamentoAte
     *            the dt pagamento ate
     */
    public void setDtPagamentoAte(String dtPagamentoAte) {
        this.dtPagamentoAte = dtPagamentoAte;
    }

    /**
     * Get: dtPagamentoAte.
     * 
     * @return dtPagamentoAte
     */
    public String getDtPagamentoAte() {
        return this.dtPagamentoAte;
    }
}