/*
 * Nome: br.com.bradesco.web.pgit.service.business.consultas.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.consultas.bean;

/**
 * Nome: ConsultarEnderecoEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarEnderecoEntradaDTO{
	
	/** Atributo cdTipoPessoa. */
	private String cdTipoPessoa;
	
	/** Atributo cdClub. */
	private Long cdClub;
	
	/** Atributo cdEmpresaContrato. */
	private Long cdEmpresaContrato;

	/**
	 * Get: cdTipoPessoa.
	 *
	 * @return cdTipoPessoa
	 */
	public String getCdTipoPessoa(){
		return cdTipoPessoa;
	}

	/**
	 * Set: cdTipoPessoa.
	 *
	 * @param cdTipoPessoa the cd tipo pessoa
	 */
	public void setCdTipoPessoa(String cdTipoPessoa){
		this.cdTipoPessoa = cdTipoPessoa;
	}

	/**
	 * Get: cdClub.
	 *
	 * @return cdClub
	 */
	public Long getCdClub(){
		return cdClub;
	}

	/**
	 * Set: cdClub.
	 *
	 * @param cdClub the cd club
	 */
	public void setCdClub(Long cdClub){
		this.cdClub = cdClub;
	}

	/**
	 * Get: cdEmpresaContrato.
	 *
	 * @return cdEmpresaContrato
	 */
	public Long getCdEmpresaContrato(){
		return cdEmpresaContrato;
	}

	/**
	 * Set: cdEmpresaContrato.
	 *
	 * @param cdEmpresaContrato the cd empresa contrato
	 */
	public void setCdEmpresaContrato(Long cdEmpresaContrato){
		this.cdEmpresaContrato = cdEmpresaContrato;
	}
}