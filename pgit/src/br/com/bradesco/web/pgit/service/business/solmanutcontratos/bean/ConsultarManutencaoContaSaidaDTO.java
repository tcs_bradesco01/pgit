/*
 * Nome: br.com.bradesco.web.pgit.service.business.solmanutcontratos.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.solmanutcontratos.bean;

/**
 * Nome: ConsultarManutencaoContaSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarManutencaoContaSaidaDTO {
	
    /** Atributo codMensagem. */
    private String codMensagem;
    
    /** Atributo mensagem. */
    private String mensagem;
    
    /** Atributo cdPessoaJuridicaContrato. */
    private Long cdPessoaJuridicaContrato;
    
    /** Atributo cdTipoContratoNegocio. */
    private Integer cdTipoContratoNegocio;
    
    /** Atributo nrSequenciaContratoNegocio. */
    private Long nrSequenciaContratoNegocio;
    
    /** Atributo cdTipoManutencaoContrato. */
    private Integer cdTipoManutencaoContrato;
    
    /** Atributo nrManutencaoContratoNegocio. */
    private Long nrManutencaoContratoNegocio;
    
    /** Atributo cdSituacaoManutencaoContrato. */
    private Integer cdSituacaoManutencaoContrato;
    
    /** Atributo dsSituacaoManutencaoContrato. */
    private String dsSituacaoManutencaoContrato;
    
    /** Atributo cdMotivoManutencaoContrato. */
    private Integer cdMotivoManutencaoContrato;
    
    /** Atributo dsMotivoManutencaoContrato. */
    private String dsMotivoManutencaoContrato;
    
    /** Atributo nrAditivoContratoNegocio. */
    private Long nrAditivoContratoNegocio;
    
    /** Atributo cdIndicadorTipoManutencao. */
    private Integer cdIndicadorTipoManutencao;
    
    /** Atributo dsIndicadorTipoManutencao. */
    private String dsIndicadorTipoManutencao;
    
    /** Atributo dsTipoContaContrato. */
    private String dsTipoContaContrato;
    
    /** Atributo dtVinculo. */
    private String dtVinculo;
    
    /** Atributo cdSituacaoVinculo. */
    private String cdSituacaoVinculo;
    
    /** Atributo cdBanco. */
    private Integer cdBanco;
    
    /** Atributo dsBanco. */
    private String dsBanco;
    
    /** Atributo cdComercialAgenciaContabil. */
    private Integer cdComercialAgenciaContabil;
    
    /** Atributo dsComercialAgenciaContabil. */
    private String dsComercialAgenciaContabil;
    
    /** Atributo nrConta. */
    private Long nrConta;
    
    /** Atributo cdDigitoConta. */
    private String cdDigitoConta;
    
    /** Atributo cdCpfCnpj. */
    private String cdCpfCnpj;
    
    /** Atributo nmPessoa. */
    private String nmPessoa;
    
    /** Atributo hrInclusaoRegistro. */
    private String hrInclusaoRegistro;
    
    /** Atributo hrManutencaoRegistro. */
    private String hrManutencaoRegistro;
    
    /** Atributo cdUsuarioInclusao. */
    private String cdUsuarioInclusao;
    
    /** Atributo cdUsuarioInclusaoExterno. */
    private String cdUsuarioInclusaoExterno;
    
    /** Atributo cdOperacaoCanalInclusao. */
    private String cdOperacaoCanalInclusao;
    
    /** Atributo cdTipoCanalInclusao. */
    private Integer cdTipoCanalInclusao;
    
    /** Atributo dsCanalInclusao. */
    private String dsCanalInclusao;
    
    /** Atributo cdusuarioManutencao. */
    private String cdusuarioManutencao;
    
    /** Atributo cdUsuarioManutencaoExterno. */
    private String cdUsuarioManutencaoExterno;
    
    /** Atributo cdOperacaoCanalManutencao. */
    private String cdOperacaoCanalManutencao;
    
    /** Atributo cdTipoCanalManutencao. */
    private Integer cdTipoCanalManutencao;
    
    /** Atributo dsCanalManutencao. */
    private String dsCanalManutencao;
    
	/**
	 * Get: cdBanco.
	 *
	 * @return cdBanco
	 */
	public Integer getCdBanco() {
		return cdBanco;
	}
	
	/**
	 * Set: cdBanco.
	 *
	 * @param cdBanco the cd banco
	 */
	public void setCdBanco(Integer cdBanco) {
		this.cdBanco = cdBanco;
	}
	
	/**
	 * Get: cdComercialAgenciaContabil.
	 *
	 * @return cdComercialAgenciaContabil
	 */
	public Integer getCdComercialAgenciaContabil() {
		return cdComercialAgenciaContabil;
	}
	
	/**
	 * Set: cdComercialAgenciaContabil.
	 *
	 * @param cdComercialAgenciaContabil the cd comercial agencia contabil
	 */
	public void setCdComercialAgenciaContabil(Integer cdComercialAgenciaContabil) {
		this.cdComercialAgenciaContabil = cdComercialAgenciaContabil;
	}
	
	/**
	 * Get: cdCpfCnpj.
	 *
	 * @return cdCpfCnpj
	 */
	public String getCdCpfCnpj() {
		return cdCpfCnpj;
	}
	
	/**
	 * Set: cdCpfCnpj.
	 *
	 * @param cdCpfCnpj the cd cpf cnpj
	 */
	public void setCdCpfCnpj(String cdCpfCnpj) {
		this.cdCpfCnpj = cdCpfCnpj;
	}
	
	/**
	 * Get: cdDigitoConta.
	 *
	 * @return cdDigitoConta
	 */
	public String getCdDigitoConta() {
		return cdDigitoConta;
	}
	
	/**
	 * Set: cdDigitoConta.
	 *
	 * @param cdDigitoConta the cd digito conta
	 */
	public void setCdDigitoConta(String cdDigitoConta) {
		this.cdDigitoConta = cdDigitoConta;
	}
	
	/**
	 * Get: cdIndicadorTipoManutencao.
	 *
	 * @return cdIndicadorTipoManutencao
	 */
	public Integer getCdIndicadorTipoManutencao() {
		return cdIndicadorTipoManutencao;
	}
	
	/**
	 * Set: cdIndicadorTipoManutencao.
	 *
	 * @param cdIndicadorTipoManutencao the cd indicador tipo manutencao
	 */
	public void setCdIndicadorTipoManutencao(Integer cdIndicadorTipoManutencao) {
		this.cdIndicadorTipoManutencao = cdIndicadorTipoManutencao;
	}
	
	/**
	 * Get: cdMotivoManutencaoContrato.
	 *
	 * @return cdMotivoManutencaoContrato
	 */
	public Integer getCdMotivoManutencaoContrato() {
		return cdMotivoManutencaoContrato;
	}
	
	/**
	 * Set: cdMotivoManutencaoContrato.
	 *
	 * @param cdMotivoManutencaoContrato the cd motivo manutencao contrato
	 */
	public void setCdMotivoManutencaoContrato(Integer cdMotivoManutencaoContrato) {
		this.cdMotivoManutencaoContrato = cdMotivoManutencaoContrato;
	}
	
	/**
	 * Get: cdOperacaoCanalInclusao.
	 *
	 * @return cdOperacaoCanalInclusao
	 */
	public String getCdOperacaoCanalInclusao() {
		return cdOperacaoCanalInclusao;
	}
	
	/**
	 * Set: cdOperacaoCanalInclusao.
	 *
	 * @param cdOperacaoCanalInclusao the cd operacao canal inclusao
	 */
	public void setCdOperacaoCanalInclusao(String cdOperacaoCanalInclusao) {
		this.cdOperacaoCanalInclusao = cdOperacaoCanalInclusao;
	}
	
	/**
	 * Get: cdOperacaoCanalManutencao.
	 *
	 * @return cdOperacaoCanalManutencao
	 */
	public String getCdOperacaoCanalManutencao() {
		return cdOperacaoCanalManutencao;
	}
	
	/**
	 * Set: cdOperacaoCanalManutencao.
	 *
	 * @param cdOperacaoCanalManutencao the cd operacao canal manutencao
	 */
	public void setCdOperacaoCanalManutencao(String cdOperacaoCanalManutencao) {
		this.cdOperacaoCanalManutencao = cdOperacaoCanalManutencao;
	}
	
	/**
	 * Get: cdPessoaJuridicaContrato.
	 *
	 * @return cdPessoaJuridicaContrato
	 */
	public Long getCdPessoaJuridicaContrato() {
		return cdPessoaJuridicaContrato;
	}
	
	/**
	 * Set: cdPessoaJuridicaContrato.
	 *
	 * @param cdPessoaJuridicaContrato the cd pessoa juridica contrato
	 */
	public void setCdPessoaJuridicaContrato(Long cdPessoaJuridicaContrato) {
		this.cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
	}
	
	/**
	 * Get: cdSituacaoManutencaoContrato.
	 *
	 * @return cdSituacaoManutencaoContrato
	 */
	public Integer getCdSituacaoManutencaoContrato() {
		return cdSituacaoManutencaoContrato;
	}
	
	/**
	 * Set: cdSituacaoManutencaoContrato.
	 *
	 * @param cdSituacaoManutencaoContrato the cd situacao manutencao contrato
	 */
	public void setCdSituacaoManutencaoContrato(Integer cdSituacaoManutencaoContrato) {
		this.cdSituacaoManutencaoContrato = cdSituacaoManutencaoContrato;
	}
	
	/**
	 * Get: cdSituacaoVinculo.
	 *
	 * @return cdSituacaoVinculo
	 */
	public String getCdSituacaoVinculo() {
		return cdSituacaoVinculo;
	}
	
	/**
	 * Set: cdSituacaoVinculo.
	 *
	 * @param cdSituacaoVinculo the cd situacao vinculo
	 */
	public void setCdSituacaoVinculo(String cdSituacaoVinculo) {
		this.cdSituacaoVinculo = cdSituacaoVinculo;
	}
	
	/**
	 * Get: cdTipoCanalInclusao.
	 *
	 * @return cdTipoCanalInclusao
	 */
	public Integer getCdTipoCanalInclusao() {
		return cdTipoCanalInclusao;
	}
	
	/**
	 * Set: cdTipoCanalInclusao.
	 *
	 * @param cdTipoCanalInclusao the cd tipo canal inclusao
	 */
	public void setCdTipoCanalInclusao(Integer cdTipoCanalInclusao) {
		this.cdTipoCanalInclusao = cdTipoCanalInclusao;
	}
	
	/**
	 * Get: cdTipoCanalManutencao.
	 *
	 * @return cdTipoCanalManutencao
	 */
	public Integer getCdTipoCanalManutencao() {
		return cdTipoCanalManutencao;
	}
	
	/**
	 * Set: cdTipoCanalManutencao.
	 *
	 * @param cdTipoCanalManutencao the cd tipo canal manutencao
	 */
	public void setCdTipoCanalManutencao(Integer cdTipoCanalManutencao) {
		this.cdTipoCanalManutencao = cdTipoCanalManutencao;
	}
	
	/**
	 * Get: cdTipoContratoNegocio.
	 *
	 * @return cdTipoContratoNegocio
	 */
	public Integer getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}
	
	/**
	 * Set: cdTipoContratoNegocio.
	 *
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}
	
	/**
	 * Get: cdTipoManutencaoContrato.
	 *
	 * @return cdTipoManutencaoContrato
	 */
	public Integer getCdTipoManutencaoContrato() {
		return cdTipoManutencaoContrato;
	}
	
	/**
	 * Set: cdTipoManutencaoContrato.
	 *
	 * @param cdTipoManutencaoContrato the cd tipo manutencao contrato
	 */
	public void setCdTipoManutencaoContrato(Integer cdTipoManutencaoContrato) {
		this.cdTipoManutencaoContrato = cdTipoManutencaoContrato;
	}
	
	/**
	 * Get: cdUsuarioInclusao.
	 *
	 * @return cdUsuarioInclusao
	 */
	public String getCdUsuarioInclusao() {
		return cdUsuarioInclusao;
	}
	
	/**
	 * Set: cdUsuarioInclusao.
	 *
	 * @param cdUsuarioInclusao the cd usuario inclusao
	 */
	public void setCdUsuarioInclusao(String cdUsuarioInclusao) {
		this.cdUsuarioInclusao = cdUsuarioInclusao;
	}
	
	/**
	 * Get: cdUsuarioInclusaoExterno.
	 *
	 * @return cdUsuarioInclusaoExterno
	 */
	public String getCdUsuarioInclusaoExterno() {
		return cdUsuarioInclusaoExterno;
	}
	
	/**
	 * Set: cdUsuarioInclusaoExterno.
	 *
	 * @param cdUsuarioInclusaoExterno the cd usuario inclusao externo
	 */
	public void setCdUsuarioInclusaoExterno(String cdUsuarioInclusaoExterno) {
		this.cdUsuarioInclusaoExterno = cdUsuarioInclusaoExterno;
	}
	
	/**
	 * Get: cdusuarioManutencao.
	 *
	 * @return cdusuarioManutencao
	 */
	public String getCdusuarioManutencao() {
		return cdusuarioManutencao;
	}
	
	/**
	 * Set: cdusuarioManutencao.
	 *
	 * @param cdusuarioManutencao the cdusuario manutencao
	 */
	public void setCdusuarioManutencao(String cdusuarioManutencao) {
		this.cdusuarioManutencao = cdusuarioManutencao;
	}
	
	/**
	 * Get: cdUsuarioManutencaoExterno.
	 *
	 * @return cdUsuarioManutencaoExterno
	 */
	public String getCdUsuarioManutencaoExterno() {
		return cdUsuarioManutencaoExterno;
	}
	
	/**
	 * Set: cdUsuarioManutencaoExterno.
	 *
	 * @param cdUsuarioManutencaoExterno the cd usuario manutencao externo
	 */
	public void setCdUsuarioManutencaoExterno(String cdUsuarioManutencaoExterno) {
		this.cdUsuarioManutencaoExterno = cdUsuarioManutencaoExterno;
	}
	
	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}
	
	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}
	
	/**
	 * Get: dsBanco.
	 *
	 * @return dsBanco
	 */
	public String getDsBanco() {
		return dsBanco;
	}
	
	/**
	 * Set: dsBanco.
	 *
	 * @param dsBanco the ds banco
	 */
	public void setDsBanco(String dsBanco) {
		this.dsBanco = dsBanco;
	}
	
	/**
	 * Get: dsCanalInclusao.
	 *
	 * @return dsCanalInclusao
	 */
	public String getDsCanalInclusao() {
		return dsCanalInclusao;
	}
	
	/**
	 * Set: dsCanalInclusao.
	 *
	 * @param dsCanalInclusao the ds canal inclusao
	 */
	public void setDsCanalInclusao(String dsCanalInclusao) {
		this.dsCanalInclusao = dsCanalInclusao;
	}
	
	/**
	 * Get: dsCanalManutencao.
	 *
	 * @return dsCanalManutencao
	 */
	public String getDsCanalManutencao() {
		return dsCanalManutencao;
	}
	
	/**
	 * Set: dsCanalManutencao.
	 *
	 * @param dsCanalManutencao the ds canal manutencao
	 */
	public void setDsCanalManutencao(String dsCanalManutencao) {
		this.dsCanalManutencao = dsCanalManutencao;
	}
	
	/**
	 * Get: dsComercialAgenciaContabil.
	 *
	 * @return dsComercialAgenciaContabil
	 */
	public String getDsComercialAgenciaContabil() {
		return dsComercialAgenciaContabil;
	}
	
	/**
	 * Set: dsComercialAgenciaContabil.
	 *
	 * @param dsComercialAgenciaContabil the ds comercial agencia contabil
	 */
	public void setDsComercialAgenciaContabil(String dsComercialAgenciaContabil) {
		this.dsComercialAgenciaContabil = dsComercialAgenciaContabil;
	}
	
	/**
	 * Get: dsIndicadorTipoManutencao.
	 *
	 * @return dsIndicadorTipoManutencao
	 */
	public String getDsIndicadorTipoManutencao() {
		return dsIndicadorTipoManutencao;
	}
	
	/**
	 * Set: dsIndicadorTipoManutencao.
	 *
	 * @param dsIndicadorTipoManutencao the ds indicador tipo manutencao
	 */
	public void setDsIndicadorTipoManutencao(String dsIndicadorTipoManutencao) {
		this.dsIndicadorTipoManutencao = dsIndicadorTipoManutencao;
	}
	
	/**
	 * Get: dsMotivoManutencaoContrato.
	 *
	 * @return dsMotivoManutencaoContrato
	 */
	public String getDsMotivoManutencaoContrato() {
		return dsMotivoManutencaoContrato;
	}
	
	/**
	 * Set: dsMotivoManutencaoContrato.
	 *
	 * @param dsMotivoManutencaoContrato the ds motivo manutencao contrato
	 */
	public void setDsMotivoManutencaoContrato(String dsMotivoManutencaoContrato) {
		this.dsMotivoManutencaoContrato = dsMotivoManutencaoContrato;
	}
	
	/**
	 * Get: dsSituacaoManutencaoContrato.
	 *
	 * @return dsSituacaoManutencaoContrato
	 */
	public String getDsSituacaoManutencaoContrato() {
		return dsSituacaoManutencaoContrato;
	}
	
	/**
	 * Set: dsSituacaoManutencaoContrato.
	 *
	 * @param dsSituacaoManutencaoContrato the ds situacao manutencao contrato
	 */
	public void setDsSituacaoManutencaoContrato(String dsSituacaoManutencaoContrato) {
		this.dsSituacaoManutencaoContrato = dsSituacaoManutencaoContrato;
	}
	
	/**
	 * Get: dsTipoContaContrato.
	 *
	 * @return dsTipoContaContrato
	 */
	public String getDsTipoContaContrato() {
		return dsTipoContaContrato;
	}
	
	/**
	 * Set: dsTipoContaContrato.
	 *
	 * @param dsTipoContaContrato the ds tipo conta contrato
	 */
	public void setDsTipoContaContrato(String dsTipoContaContrato) {
		this.dsTipoContaContrato = dsTipoContaContrato;
	}
	
	/**
	 * Get: dtVinculo.
	 *
	 * @return dtVinculo
	 */
	public String getDtVinculo() {
		return dtVinculo;
	}
	
	/**
	 * Set: dtVinculo.
	 *
	 * @param dtVinculo the dt vinculo
	 */
	public void setDtVinculo(String dtVinculo) {
		this.dtVinculo = dtVinculo;
	}
	
	/**
	 * Get: hrInclusaoRegistro.
	 *
	 * @return hrInclusaoRegistro
	 */
	public String getHrInclusaoRegistro() {
		return hrInclusaoRegistro;
	}
	
	/**
	 * Set: hrInclusaoRegistro.
	 *
	 * @param hrInclusaoRegistro the hr inclusao registro
	 */
	public void setHrInclusaoRegistro(String hrInclusaoRegistro) {
		this.hrInclusaoRegistro = hrInclusaoRegistro;
	}
	
	/**
	 * Get: hrManutencaoRegistro.
	 *
	 * @return hrManutencaoRegistro
	 */
	public String getHrManutencaoRegistro() {
		return hrManutencaoRegistro;
	}
	
	/**
	 * Set: hrManutencaoRegistro.
	 *
	 * @param hrManutencaoRegistro the hr manutencao registro
	 */
	public void setHrManutencaoRegistro(String hrManutencaoRegistro) {
		this.hrManutencaoRegistro = hrManutencaoRegistro;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	/**
	 * Get: nmPessoa.
	 *
	 * @return nmPessoa
	 */
	public String getNmPessoa() {
		return nmPessoa;
	}
	
	/**
	 * Set: nmPessoa.
	 *
	 * @param nmPessoa the nm pessoa
	 */
	public void setNmPessoa(String nmPessoa) {
		this.nmPessoa = nmPessoa;
	}
	
	/**
	 * Get: nrAditivoContratoNegocio.
	 *
	 * @return nrAditivoContratoNegocio
	 */
	public Long getNrAditivoContratoNegocio() {
		return nrAditivoContratoNegocio;
	}
	
	/**
	 * Set: nrAditivoContratoNegocio.
	 *
	 * @param nrAditivoContratoNegocio the nr aditivo contrato negocio
	 */
	public void setNrAditivoContratoNegocio(Long nrAditivoContratoNegocio) {
		this.nrAditivoContratoNegocio = nrAditivoContratoNegocio;
	}
	
	/**
	 * Get: nrConta.
	 *
	 * @return nrConta
	 */
	public Long getNrConta() {
		return nrConta;
	}
	
	/**
	 * Set: nrConta.
	 *
	 * @param nrConta the nr conta
	 */
	public void setNrConta(Long nrConta) {
		this.nrConta = nrConta;
	}
	
	/**
	 * Get: nrManutencaoContratoNegocio.
	 *
	 * @return nrManutencaoContratoNegocio
	 */
	public Long getNrManutencaoContratoNegocio() {
		return nrManutencaoContratoNegocio;
	}
	
	/**
	 * Set: nrManutencaoContratoNegocio.
	 *
	 * @param nrManutencaoContratoNegocio the nr manutencao contrato negocio
	 */
	public void setNrManutencaoContratoNegocio(Long nrManutencaoContratoNegocio) {
		this.nrManutencaoContratoNegocio = nrManutencaoContratoNegocio;
	}
	
	/**
	 * Get: nrSequenciaContratoNegocio.
	 *
	 * @return nrSequenciaContratoNegocio
	 */
	public Long getNrSequenciaContratoNegocio() {
		return nrSequenciaContratoNegocio;
	}
	
	/**
	 * Set: nrSequenciaContratoNegocio.
	 *
	 * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
	 */
	public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}
	


}
