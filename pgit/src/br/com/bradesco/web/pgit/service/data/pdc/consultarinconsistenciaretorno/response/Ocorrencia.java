/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultarinconsistenciaretorno.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencia.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencia implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _numeroSequencia
     */
    private long _numeroSequencia = 0;

    /**
     * keeps track of state for field: _numeroSequencia
     */
    private boolean _has_numeroSequencia;

    /**
     * Field _controlePagamento
     */
    private java.lang.String _controlePagamento;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencia() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarinconsistenciaretorno.response.Ocorrencia()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteNumeroSequencia
     * 
     */
    public void deleteNumeroSequencia()
    {
        this._has_numeroSequencia= false;
    } //-- void deleteNumeroSequencia() 

    /**
     * Returns the value of field 'controlePagamento'.
     * 
     * @return String
     * @return the value of field 'controlePagamento'.
     */
    public java.lang.String getControlePagamento()
    {
        return this._controlePagamento;
    } //-- java.lang.String getControlePagamento() 

    /**
     * Returns the value of field 'numeroSequencia'.
     * 
     * @return long
     * @return the value of field 'numeroSequencia'.
     */
    public long getNumeroSequencia()
    {
        return this._numeroSequencia;
    } //-- long getNumeroSequencia() 

    /**
     * Method hasNumeroSequencia
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNumeroSequencia()
    {
        return this._has_numeroSequencia;
    } //-- boolean hasNumeroSequencia() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'controlePagamento'.
     * 
     * @param controlePagamento the value of field
     * 'controlePagamento'.
     */
    public void setControlePagamento(java.lang.String controlePagamento)
    {
        this._controlePagamento = controlePagamento;
    } //-- void setControlePagamento(java.lang.String) 

    /**
     * Sets the value of field 'numeroSequencia'.
     * 
     * @param numeroSequencia the value of field 'numeroSequencia'.
     */
    public void setNumeroSequencia(long numeroSequencia)
    {
        this._numeroSequencia = numeroSequencia;
        this._has_numeroSequencia = true;
    } //-- void setNumeroSequencia(long) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencia
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultarinconsistenciaretorno.response.Ocorrencia unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultarinconsistenciaretorno.response.Ocorrencia) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultarinconsistenciaretorno.response.Ocorrencia.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarinconsistenciaretorno.response.Ocorrencia unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
