/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.imprimiraditivocontrato.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class OcorrenciasParticipante.
 * 
 * @version $Revision$ $Date$
 */
public class OcorrenciasParticipante implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdAcaoParticipante
     */
    private int _cdAcaoParticipante = 0;

    /**
     * keeps track of state for field: _cdAcaoParticipante
     */
    private boolean _has_cdAcaoParticipante;

    /**
     * Field _cdCpfCnpjParticipante
     */
    private java.lang.String _cdCpfCnpjParticipante;

    /**
     * Field _nmParticipante
     */
    private java.lang.String _nmParticipante;


      //----------------/
     //- Constructors -/
    //----------------/

    public OcorrenciasParticipante() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimiraditivocontrato.response.OcorrenciasParticipante()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdAcaoParticipante
     * 
     */
    public void deleteCdAcaoParticipante()
    {
        this._has_cdAcaoParticipante= false;
    } //-- void deleteCdAcaoParticipante() 

    /**
     * Returns the value of field 'cdAcaoParticipante'.
     * 
     * @return int
     * @return the value of field 'cdAcaoParticipante'.
     */
    public int getCdAcaoParticipante()
    {
        return this._cdAcaoParticipante;
    } //-- int getCdAcaoParticipante() 

    /**
     * Returns the value of field 'cdCpfCnpjParticipante'.
     * 
     * @return String
     * @return the value of field 'cdCpfCnpjParticipante'.
     */
    public java.lang.String getCdCpfCnpjParticipante()
    {
        return this._cdCpfCnpjParticipante;
    } //-- java.lang.String getCdCpfCnpjParticipante() 

    /**
     * Returns the value of field 'nmParticipante'.
     * 
     * @return String
     * @return the value of field 'nmParticipante'.
     */
    public java.lang.String getNmParticipante()
    {
        return this._nmParticipante;
    } //-- java.lang.String getNmParticipante() 

    /**
     * Method hasCdAcaoParticipante
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAcaoParticipante()
    {
        return this._has_cdAcaoParticipante;
    } //-- boolean hasCdAcaoParticipante() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdAcaoParticipante'.
     * 
     * @param cdAcaoParticipante the value of field
     * 'cdAcaoParticipante'.
     */
    public void setCdAcaoParticipante(int cdAcaoParticipante)
    {
        this._cdAcaoParticipante = cdAcaoParticipante;
        this._has_cdAcaoParticipante = true;
    } //-- void setCdAcaoParticipante(int) 

    /**
     * Sets the value of field 'cdCpfCnpjParticipante'.
     * 
     * @param cdCpfCnpjParticipante the value of field
     * 'cdCpfCnpjParticipante'.
     */
    public void setCdCpfCnpjParticipante(java.lang.String cdCpfCnpjParticipante)
    {
        this._cdCpfCnpjParticipante = cdCpfCnpjParticipante;
    } //-- void setCdCpfCnpjParticipante(java.lang.String) 

    /**
     * Sets the value of field 'nmParticipante'.
     * 
     * @param nmParticipante the value of field 'nmParticipante'.
     */
    public void setNmParticipante(java.lang.String nmParticipante)
    {
        this._nmParticipante = nmParticipante;
    } //-- void setNmParticipante(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return OcorrenciasParticipante
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.imprimiraditivocontrato.response.OcorrenciasParticipante unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.imprimiraditivocontrato.response.OcorrenciasParticipante) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.imprimiraditivocontrato.response.OcorrenciasParticipante.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimiraditivocontrato.response.OcorrenciasParticipante unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
