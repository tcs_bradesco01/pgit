/*
 * Nome: br.com.bradesco.web.pgit.service.business.conposicaodiariapagtosagencia.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.conposicaodiariapagtosagencia.bean;

/**
 * Nome: DetalharPorContaDebitoEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class DetalharPorContaDebitoEntradaDTO{
    
    /** Atributo nrOcorrencias. */
    private Integer nrOcorrencias;
    
    /** Atributo cdCpfCnpj. */
    private Long cdCpfCnpj;
    
    /** Atributo cdFilialCpfCnpj. */
    private Integer cdFilialCpfCnpj;
    
    /** Atributo cdDigitoCpfCnpj. */
    private Integer cdDigitoCpfCnpj;
    
    /** Atributo cdUnidadeOrganizacional. */
    private Integer cdUnidadeOrganizacional;
    
    /** Atributo cdPessoaJuridicaContrato. */
    private Long cdPessoaJuridicaContrato;
    
    /** Atributo cdTipoContratoNegocio. */
    private Integer cdTipoContratoNegocio;
    
    /** Atributo nrSequenciaContratoNegocio. */
    private Long nrSequenciaContratoNegocio;
    
    /** Atributo cdProdutoServicoOperacao. */
    private Integer cdProdutoServicoOperacao;
    
    /** Atributo cdProdutoServicoRelacionado. */
    private Integer cdProdutoServicoRelacionado;
    
    /** Atributo dsComplemento. */
    private String dsComplemento;
    
	/**
	 * Get: dsComplemento.
	 *
	 * @return dsComplemento
	 */
	public String getDsComplemento() {
		return dsComplemento;
	}
	
	/**
	 * Set: dsComplemento.
	 *
	 * @param dsComplemento the ds complemento
	 */
	public void setDsComplemento(String dsComplemento) {
		this.dsComplemento = dsComplemento;
	}
	
	/**
	 * Get: cdCpfCnpj.
	 *
	 * @return cdCpfCnpj
	 */
	public Long getCdCpfCnpj() {
		return cdCpfCnpj;
	}
	
	/**
	 * Set: cdCpfCnpj.
	 *
	 * @param cdCpfCnpj the cd cpf cnpj
	 */
	public void setCdCpfCnpj(Long cdCpfCnpj) {
		this.cdCpfCnpj = cdCpfCnpj;
	}
	
	/**
	 * Get: cdDigitoCpfCnpj.
	 *
	 * @return cdDigitoCpfCnpj
	 */
	public Integer getCdDigitoCpfCnpj() {
		return cdDigitoCpfCnpj;
	}
	
	/**
	 * Set: cdDigitoCpfCnpj.
	 *
	 * @param cdDigitoCpfCnpj the cd digito cpf cnpj
	 */
	public void setCdDigitoCpfCnpj(Integer cdDigitoCpfCnpj) {
		this.cdDigitoCpfCnpj = cdDigitoCpfCnpj;
	}
	
	/**
	 * Get: cdFilialCpfCnpj.
	 *
	 * @return cdFilialCpfCnpj
	 */
	public Integer getCdFilialCpfCnpj() {
		return cdFilialCpfCnpj;
	}
	
	/**
	 * Set: cdFilialCpfCnpj.
	 *
	 * @param cdFilialCpfCnpj the cd filial cpf cnpj
	 */
	public void setCdFilialCpfCnpj(Integer cdFilialCpfCnpj) {
		this.cdFilialCpfCnpj = cdFilialCpfCnpj;
	}
	
	/**
	 * Get: cdPessoaJuridicaContrato.
	 *
	 * @return cdPessoaJuridicaContrato
	 */
	public Long getCdPessoaJuridicaContrato() {
		return cdPessoaJuridicaContrato;
	}
	
	/**
	 * Set: cdPessoaJuridicaContrato.
	 *
	 * @param cdPessoaJuridicaContrato the cd pessoa juridica contrato
	 */
	public void setCdPessoaJuridicaContrato(Long cdPessoaJuridicaContrato) {
		this.cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
	}
	
	/**
	 * Get: cdProdutoServicoOperacao.
	 *
	 * @return cdProdutoServicoOperacao
	 */
	public Integer getCdProdutoServicoOperacao() {
		return cdProdutoServicoOperacao;
	}
	
	/**
	 * Set: cdProdutoServicoOperacao.
	 *
	 * @param cdProdutoServicoOperacao the cd produto servico operacao
	 */
	public void setCdProdutoServicoOperacao(Integer cdProdutoServicoOperacao) {
		this.cdProdutoServicoOperacao = cdProdutoServicoOperacao;
	}
	
	/**
	 * Get: cdProdutoServicoRelacionado.
	 *
	 * @return cdProdutoServicoRelacionado
	 */
	public Integer getCdProdutoServicoRelacionado() {
		return cdProdutoServicoRelacionado;
	}
	
	/**
	 * Set: cdProdutoServicoRelacionado.
	 *
	 * @param cdProdutoServicoRelacionado the cd produto servico relacionado
	 */
	public void setCdProdutoServicoRelacionado(Integer cdProdutoServicoRelacionado) {
		this.cdProdutoServicoRelacionado = cdProdutoServicoRelacionado;
	}
	
	/**
	 * Get: cdTipoContratoNegocio.
	 *
	 * @return cdTipoContratoNegocio
	 */
	public Integer getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}
	
	/**
	 * Set: cdTipoContratoNegocio.
	 *
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}
	
	/**
	 * Get: cdUnidadeOrganizacional.
	 *
	 * @return cdUnidadeOrganizacional
	 */
	public Integer getCdUnidadeOrganizacional() {
		return cdUnidadeOrganizacional;
	}
	
	/**
	 * Set: cdUnidadeOrganizacional.
	 *
	 * @param cdUnidadeOrganizacional the cd unidade organizacional
	 */
	public void setCdUnidadeOrganizacional(Integer cdUnidadeOrganizacional) {
		this.cdUnidadeOrganizacional = cdUnidadeOrganizacional;
	}
	
	/**
	 * Get: nrOcorrencias.
	 *
	 * @return nrOcorrencias
	 */
	public Integer getNrOcorrencias() {
		return nrOcorrencias;
	}
	
	/**
	 * Set: nrOcorrencias.
	 *
	 * @param nrOcorrencias the nr ocorrencias
	 */
	public void setNrOcorrencias(Integer nrOcorrencias) {
		this.nrOcorrencias = nrOcorrencias;
	}
	
	/**
	 * Get: nrSequenciaContratoNegocio.
	 *
	 * @return nrSequenciaContratoNegocio
	 */
	public Long getNrSequenciaContratoNegocio() {
		return nrSequenciaContratoNegocio;
	}
	
	/**
	 * Set: nrSequenciaContratoNegocio.
	 *
	 * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
	 */
	public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}
    
    
}
  