/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultarsaldoatual.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.math.BigDecimal;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ConsultarSaldoAtualResponse.
 * 
 * @version $Revision$ $Date$
 */
public class ConsultarSaldoAtualResponse implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _codMensagem
     */
    private java.lang.String _codMensagem;

    /**
     * Field _mensagem
     */
    private java.lang.String _mensagem;

    /**
     * Field _dsBancoDebito
     */
    private java.lang.String _dsBancoDebito;

    /**
     * Field _dsAgenciaDebito
     */
    private java.lang.String _dsAgenciaDebito;

    /**
     * Field _dsTipoContaDebito
     */
    private java.lang.String _dsTipoContaDebito;

    /**
     * Field _dsTipoSaldo
     */
    private java.lang.String _dsTipoSaldo;

    /**
     * Field _qtSemSaldo
     */
    private long _qtSemSaldo = 0;

    /**
     * keeps track of state for field: _qtSemSaldo
     */
    private boolean _has_qtSemSaldo;

    /**
     * Field _vlSemSaldo
     */
    private java.math.BigDecimal _vlSemSaldo = new java.math.BigDecimal("0");

    /**
     * Field _qtPendenteSemSaldo
     */
    private long _qtPendenteSemSaldo = 0;

    /**
     * keeps track of state for field: _qtPendenteSemSaldo
     */
    private boolean _has_qtPendenteSemSaldo;

    /**
     * Field _vlPendenteSemSaldo
     */
    private java.math.BigDecimal _vlPendenteSemSaldo = new java.math.BigDecimal("0");

    /**
     * Field _qtAgendadoSemSaldo
     */
    private long _qtAgendadoSemSaldo = 0;

    /**
     * keeps track of state for field: _qtAgendadoSemSaldo
     */
    private boolean _has_qtAgendadoSemSaldo;

    /**
     * Field _vlAgendadoSemSaldo
     */
    private java.math.BigDecimal _vlAgendadoSemSaldo = new java.math.BigDecimal("0");

    /**
     * Field _qtEfetivadoSemSaldo
     */
    private long _qtEfetivadoSemSaldo = 0;

    /**
     * keeps track of state for field: _qtEfetivadoSemSaldo
     */
    private boolean _has_qtEfetivadoSemSaldo;

    /**
     * Field _vlEfetivadoSemSaldo
     */
    private java.math.BigDecimal _vlEfetivadoSemSaldo = new java.math.BigDecimal("0");

    /**
     * Field _vlSaldoLivre
     */
    private java.math.BigDecimal _vlSaldoLivre = new java.math.BigDecimal("0");

    /**
     * Field _vlReservaDisponivel
     */
    private java.math.BigDecimal _vlReservaDisponivel = new java.math.BigDecimal("0");

    /**
     * Field _vlReservaNaoDisponivel
     */
    private java.math.BigDecimal _vlReservaNaoDisponivel = new java.math.BigDecimal("0");

    /**
     * Field _vlTotal
     */
    private java.math.BigDecimal _vlTotal = new java.math.BigDecimal("0");


      //----------------/
     //- Constructors -/
    //----------------/

    public ConsultarSaldoAtualResponse() 
     {
        super();
        setVlSemSaldo(new java.math.BigDecimal("0"));
        setVlPendenteSemSaldo(new java.math.BigDecimal("0"));
        setVlAgendadoSemSaldo(new java.math.BigDecimal("0"));
        setVlEfetivadoSemSaldo(new java.math.BigDecimal("0"));
        setVlSaldoLivre(new java.math.BigDecimal("0"));
        setVlReservaDisponivel(new java.math.BigDecimal("0"));
        setVlReservaNaoDisponivel(new java.math.BigDecimal("0"));
        setVlTotal(new java.math.BigDecimal("0"));
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarsaldoatual.response.ConsultarSaldoAtualResponse()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteQtAgendadoSemSaldo
     * 
     */
    public void deleteQtAgendadoSemSaldo()
    {
        this._has_qtAgendadoSemSaldo= false;
    } //-- void deleteQtAgendadoSemSaldo() 

    /**
     * Method deleteQtEfetivadoSemSaldo
     * 
     */
    public void deleteQtEfetivadoSemSaldo()
    {
        this._has_qtEfetivadoSemSaldo= false;
    } //-- void deleteQtEfetivadoSemSaldo() 

    /**
     * Method deleteQtPendenteSemSaldo
     * 
     */
    public void deleteQtPendenteSemSaldo()
    {
        this._has_qtPendenteSemSaldo= false;
    } //-- void deleteQtPendenteSemSaldo() 

    /**
     * Method deleteQtSemSaldo
     * 
     */
    public void deleteQtSemSaldo()
    {
        this._has_qtSemSaldo= false;
    } //-- void deleteQtSemSaldo() 

    /**
     * Returns the value of field 'codMensagem'.
     * 
     * @return String
     * @return the value of field 'codMensagem'.
     */
    public java.lang.String getCodMensagem()
    {
        return this._codMensagem;
    } //-- java.lang.String getCodMensagem() 

    /**
     * Returns the value of field 'dsAgenciaDebito'.
     * 
     * @return String
     * @return the value of field 'dsAgenciaDebito'.
     */
    public java.lang.String getDsAgenciaDebito()
    {
        return this._dsAgenciaDebito;
    } //-- java.lang.String getDsAgenciaDebito() 

    /**
     * Returns the value of field 'dsBancoDebito'.
     * 
     * @return String
     * @return the value of field 'dsBancoDebito'.
     */
    public java.lang.String getDsBancoDebito()
    {
        return this._dsBancoDebito;
    } //-- java.lang.String getDsBancoDebito() 

    /**
     * Returns the value of field 'dsTipoContaDebito'.
     * 
     * @return String
     * @return the value of field 'dsTipoContaDebito'.
     */
    public java.lang.String getDsTipoContaDebito()
    {
        return this._dsTipoContaDebito;
    } //-- java.lang.String getDsTipoContaDebito() 

    /**
     * Returns the value of field 'dsTipoSaldo'.
     * 
     * @return String
     * @return the value of field 'dsTipoSaldo'.
     */
    public java.lang.String getDsTipoSaldo()
    {
        return this._dsTipoSaldo;
    } //-- java.lang.String getDsTipoSaldo() 

    /**
     * Returns the value of field 'mensagem'.
     * 
     * @return String
     * @return the value of field 'mensagem'.
     */
    public java.lang.String getMensagem()
    {
        return this._mensagem;
    } //-- java.lang.String getMensagem() 

    /**
     * Returns the value of field 'qtAgendadoSemSaldo'.
     * 
     * @return long
     * @return the value of field 'qtAgendadoSemSaldo'.
     */
    public long getQtAgendadoSemSaldo()
    {
        return this._qtAgendadoSemSaldo;
    } //-- long getQtAgendadoSemSaldo() 

    /**
     * Returns the value of field 'qtEfetivadoSemSaldo'.
     * 
     * @return long
     * @return the value of field 'qtEfetivadoSemSaldo'.
     */
    public long getQtEfetivadoSemSaldo()
    {
        return this._qtEfetivadoSemSaldo;
    } //-- long getQtEfetivadoSemSaldo() 

    /**
     * Returns the value of field 'qtPendenteSemSaldo'.
     * 
     * @return long
     * @return the value of field 'qtPendenteSemSaldo'.
     */
    public long getQtPendenteSemSaldo()
    {
        return this._qtPendenteSemSaldo;
    } //-- long getQtPendenteSemSaldo() 

    /**
     * Returns the value of field 'qtSemSaldo'.
     * 
     * @return long
     * @return the value of field 'qtSemSaldo'.
     */
    public long getQtSemSaldo()
    {
        return this._qtSemSaldo;
    } //-- long getQtSemSaldo() 

    /**
     * Returns the value of field 'vlAgendadoSemSaldo'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlAgendadoSemSaldo'.
     */
    public java.math.BigDecimal getVlAgendadoSemSaldo()
    {
        return this._vlAgendadoSemSaldo;
    } //-- java.math.BigDecimal getVlAgendadoSemSaldo() 

    /**
     * Returns the value of field 'vlEfetivadoSemSaldo'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlEfetivadoSemSaldo'.
     */
    public java.math.BigDecimal getVlEfetivadoSemSaldo()
    {
        return this._vlEfetivadoSemSaldo;
    } //-- java.math.BigDecimal getVlEfetivadoSemSaldo() 

    /**
     * Returns the value of field 'vlPendenteSemSaldo'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlPendenteSemSaldo'.
     */
    public java.math.BigDecimal getVlPendenteSemSaldo()
    {
        return this._vlPendenteSemSaldo;
    } //-- java.math.BigDecimal getVlPendenteSemSaldo() 

    /**
     * Returns the value of field 'vlReservaDisponivel'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlReservaDisponivel'.
     */
    public java.math.BigDecimal getVlReservaDisponivel()
    {
        return this._vlReservaDisponivel;
    } //-- java.math.BigDecimal getVlReservaDisponivel() 

    /**
     * Returns the value of field 'vlReservaNaoDisponivel'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlReservaNaoDisponivel'.
     */
    public java.math.BigDecimal getVlReservaNaoDisponivel()
    {
        return this._vlReservaNaoDisponivel;
    } //-- java.math.BigDecimal getVlReservaNaoDisponivel() 

    /**
     * Returns the value of field 'vlSaldoLivre'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlSaldoLivre'.
     */
    public java.math.BigDecimal getVlSaldoLivre()
    {
        return this._vlSaldoLivre;
    } //-- java.math.BigDecimal getVlSaldoLivre() 

    /**
     * Returns the value of field 'vlSemSaldo'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlSemSaldo'.
     */
    public java.math.BigDecimal getVlSemSaldo()
    {
        return this._vlSemSaldo;
    } //-- java.math.BigDecimal getVlSemSaldo() 

    /**
     * Returns the value of field 'vlTotal'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlTotal'.
     */
    public java.math.BigDecimal getVlTotal()
    {
        return this._vlTotal;
    } //-- java.math.BigDecimal getVlTotal() 

    /**
     * Method hasQtAgendadoSemSaldo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtAgendadoSemSaldo()
    {
        return this._has_qtAgendadoSemSaldo;
    } //-- boolean hasQtAgendadoSemSaldo() 

    /**
     * Method hasQtEfetivadoSemSaldo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtEfetivadoSemSaldo()
    {
        return this._has_qtEfetivadoSemSaldo;
    } //-- boolean hasQtEfetivadoSemSaldo() 

    /**
     * Method hasQtPendenteSemSaldo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtPendenteSemSaldo()
    {
        return this._has_qtPendenteSemSaldo;
    } //-- boolean hasQtPendenteSemSaldo() 

    /**
     * Method hasQtSemSaldo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtSemSaldo()
    {
        return this._has_qtSemSaldo;
    } //-- boolean hasQtSemSaldo() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'codMensagem'.
     * 
     * @param codMensagem the value of field 'codMensagem'.
     */
    public void setCodMensagem(java.lang.String codMensagem)
    {
        this._codMensagem = codMensagem;
    } //-- void setCodMensagem(java.lang.String) 

    /**
     * Sets the value of field 'dsAgenciaDebito'.
     * 
     * @param dsAgenciaDebito the value of field 'dsAgenciaDebito'.
     */
    public void setDsAgenciaDebito(java.lang.String dsAgenciaDebito)
    {
        this._dsAgenciaDebito = dsAgenciaDebito;
    } //-- void setDsAgenciaDebito(java.lang.String) 

    /**
     * Sets the value of field 'dsBancoDebito'.
     * 
     * @param dsBancoDebito the value of field 'dsBancoDebito'.
     */
    public void setDsBancoDebito(java.lang.String dsBancoDebito)
    {
        this._dsBancoDebito = dsBancoDebito;
    } //-- void setDsBancoDebito(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoContaDebito'.
     * 
     * @param dsTipoContaDebito the value of field
     * 'dsTipoContaDebito'.
     */
    public void setDsTipoContaDebito(java.lang.String dsTipoContaDebito)
    {
        this._dsTipoContaDebito = dsTipoContaDebito;
    } //-- void setDsTipoContaDebito(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoSaldo'.
     * 
     * @param dsTipoSaldo the value of field 'dsTipoSaldo'.
     */
    public void setDsTipoSaldo(java.lang.String dsTipoSaldo)
    {
        this._dsTipoSaldo = dsTipoSaldo;
    } //-- void setDsTipoSaldo(java.lang.String) 

    /**
     * Sets the value of field 'mensagem'.
     * 
     * @param mensagem the value of field 'mensagem'.
     */
    public void setMensagem(java.lang.String mensagem)
    {
        this._mensagem = mensagem;
    } //-- void setMensagem(java.lang.String) 

    /**
     * Sets the value of field 'qtAgendadoSemSaldo'.
     * 
     * @param qtAgendadoSemSaldo the value of field
     * 'qtAgendadoSemSaldo'.
     */
    public void setQtAgendadoSemSaldo(long qtAgendadoSemSaldo)
    {
        this._qtAgendadoSemSaldo = qtAgendadoSemSaldo;
        this._has_qtAgendadoSemSaldo = true;
    } //-- void setQtAgendadoSemSaldo(long) 

    /**
     * Sets the value of field 'qtEfetivadoSemSaldo'.
     * 
     * @param qtEfetivadoSemSaldo the value of field
     * 'qtEfetivadoSemSaldo'.
     */
    public void setQtEfetivadoSemSaldo(long qtEfetivadoSemSaldo)
    {
        this._qtEfetivadoSemSaldo = qtEfetivadoSemSaldo;
        this._has_qtEfetivadoSemSaldo = true;
    } //-- void setQtEfetivadoSemSaldo(long) 

    /**
     * Sets the value of field 'qtPendenteSemSaldo'.
     * 
     * @param qtPendenteSemSaldo the value of field
     * 'qtPendenteSemSaldo'.
     */
    public void setQtPendenteSemSaldo(long qtPendenteSemSaldo)
    {
        this._qtPendenteSemSaldo = qtPendenteSemSaldo;
        this._has_qtPendenteSemSaldo = true;
    } //-- void setQtPendenteSemSaldo(long) 

    /**
     * Sets the value of field 'qtSemSaldo'.
     * 
     * @param qtSemSaldo the value of field 'qtSemSaldo'.
     */
    public void setQtSemSaldo(long qtSemSaldo)
    {
        this._qtSemSaldo = qtSemSaldo;
        this._has_qtSemSaldo = true;
    } //-- void setQtSemSaldo(long) 

    /**
     * Sets the value of field 'vlAgendadoSemSaldo'.
     * 
     * @param vlAgendadoSemSaldo the value of field
     * 'vlAgendadoSemSaldo'.
     */
    public void setVlAgendadoSemSaldo(java.math.BigDecimal vlAgendadoSemSaldo)
    {
        this._vlAgendadoSemSaldo = vlAgendadoSemSaldo;
    } //-- void setVlAgendadoSemSaldo(java.math.BigDecimal) 

    /**
     * Sets the value of field 'vlEfetivadoSemSaldo'.
     * 
     * @param vlEfetivadoSemSaldo the value of field
     * 'vlEfetivadoSemSaldo'.
     */
    public void setVlEfetivadoSemSaldo(java.math.BigDecimal vlEfetivadoSemSaldo)
    {
        this._vlEfetivadoSemSaldo = vlEfetivadoSemSaldo;
    } //-- void setVlEfetivadoSemSaldo(java.math.BigDecimal) 

    /**
     * Sets the value of field 'vlPendenteSemSaldo'.
     * 
     * @param vlPendenteSemSaldo the value of field
     * 'vlPendenteSemSaldo'.
     */
    public void setVlPendenteSemSaldo(java.math.BigDecimal vlPendenteSemSaldo)
    {
        this._vlPendenteSemSaldo = vlPendenteSemSaldo;
    } //-- void setVlPendenteSemSaldo(java.math.BigDecimal) 

    /**
     * Sets the value of field 'vlReservaDisponivel'.
     * 
     * @param vlReservaDisponivel the value of field
     * 'vlReservaDisponivel'.
     */
    public void setVlReservaDisponivel(java.math.BigDecimal vlReservaDisponivel)
    {
        this._vlReservaDisponivel = vlReservaDisponivel;
    } //-- void setVlReservaDisponivel(java.math.BigDecimal) 

    /**
     * Sets the value of field 'vlReservaNaoDisponivel'.
     * 
     * @param vlReservaNaoDisponivel the value of field
     * 'vlReservaNaoDisponivel'.
     */
    public void setVlReservaNaoDisponivel(java.math.BigDecimal vlReservaNaoDisponivel)
    {
        this._vlReservaNaoDisponivel = vlReservaNaoDisponivel;
    } //-- void setVlReservaNaoDisponivel(java.math.BigDecimal) 

    /**
     * Sets the value of field 'vlSaldoLivre'.
     * 
     * @param vlSaldoLivre the value of field 'vlSaldoLivre'.
     */
    public void setVlSaldoLivre(java.math.BigDecimal vlSaldoLivre)
    {
        this._vlSaldoLivre = vlSaldoLivre;
    } //-- void setVlSaldoLivre(java.math.BigDecimal) 

    /**
     * Sets the value of field 'vlSemSaldo'.
     * 
     * @param vlSemSaldo the value of field 'vlSemSaldo'.
     */
    public void setVlSemSaldo(java.math.BigDecimal vlSemSaldo)
    {
        this._vlSemSaldo = vlSemSaldo;
    } //-- void setVlSemSaldo(java.math.BigDecimal) 

    /**
     * Sets the value of field 'vlTotal'.
     * 
     * @param vlTotal the value of field 'vlTotal'.
     */
    public void setVlTotal(java.math.BigDecimal vlTotal)
    {
        this._vlTotal = vlTotal;
    } //-- void setVlTotal(java.math.BigDecimal) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ConsultarSaldoAtualResponse
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultarsaldoatual.response.ConsultarSaldoAtualResponse unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultarsaldoatual.response.ConsultarSaldoAtualResponse) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultarsaldoatual.response.ConsultarSaldoAtualResponse.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarsaldoatual.response.ConsultarSaldoAtualResponse unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
