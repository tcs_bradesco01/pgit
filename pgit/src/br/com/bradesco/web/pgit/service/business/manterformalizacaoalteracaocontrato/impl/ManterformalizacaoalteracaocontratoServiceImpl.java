/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/implementation.ftl,v $
 * $Id: implementation.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: implementation.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */
 
package br.com.bradesco.web.pgit.service.business.manterformalizacaoalteracaocontrato.impl;

import static br.com.bradesco.web.pgit.utils.PgitUtil.verificaIntegerNulo;
import static br.com.bradesco.web.pgit.utils.PgitUtil.verificaLongNulo;
import static br.com.bradesco.web.pgit.view.converters.FormatarData.formatarDataTrilha;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import br.com.bradesco.web.pgit.service.business.manterformalizacaoalteracaocontrato.IManterformalizacaoalteracaocontratoService;
import br.com.bradesco.web.pgit.service.business.manterformalizacaoalteracaocontrato.bean.ConRelacaoConManutencoesEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterformalizacaoalteracaocontrato.bean.ConRelacaoConManutencoesSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterformalizacaoalteracaocontrato.bean.ConsultarManterFormAltContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterformalizacaoalteracaocontrato.bean.ConsultarManterFormAltContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterformalizacaoalteracaocontrato.bean.DetalharManterFormAltContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterformalizacaoalteracaocontrato.bean.DetalharManterFormAltContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterformalizacaoalteracaocontrato.bean.ExcluirManterFormAltContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterformalizacaoalteracaocontrato.bean.ExcluirManterFormAltContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterformalizacaoalteracaocontrato.bean.IncluirManterFormAltContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterformalizacaoalteracaocontrato.bean.IncluirManterFormAltContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterformalizacaoalteracaocontrato.bean.ParticipantesDTO;
import br.com.bradesco.web.pgit.service.business.manterformalizacaoalteracaocontrato.bean.ParticipantesRelacaoConManutencoesDTO;
import br.com.bradesco.web.pgit.service.business.manterformalizacaoalteracaocontrato.bean.ServicosDTO;
import br.com.bradesco.web.pgit.service.business.manterformalizacaoalteracaocontrato.bean.VinculoDTO;
import br.com.bradesco.web.pgit.service.business.manterformalizacaoalteracaocontrato.bean.VinculoRelacaoConManutencoesDTO;
import br.com.bradesco.web.pgit.service.data.pdc.FactoryAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.conrelacaoconmanutencoes.request.ConRelacaoConManutencoesRequest;
import br.com.bradesco.web.pgit.service.data.pdc.conrelacaoconmanutencoes.response.ConRelacaoConManutencoesResponse;
import br.com.bradesco.web.pgit.service.data.pdc.consultaraditivocontrato.request.ConsultarAditivoContratoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultaraditivocontrato.response.ConsultarAditivoContratoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.detalharaditivocontrato.request.DetalharAditivoContratoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.detalharaditivocontrato.response.DetalharAditivoContratoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.excluiraditivocontrato.request.ExcluirAditivoContratoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.excluiraditivocontrato.response.ExcluirAditivoContratoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.incluiraditivocontrato.request.IncluirAditivoContratoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.incluiraditivocontrato.response.IncluirAditivoContratoResponse;
import br.com.bradesco.web.pgit.utils.PgitUtil;


/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Implementa��o do adaptador: Manterformalizacaoalteracaocontrato
 * </p>
 * 
 * @comment C�DIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public class ManterformalizacaoalteracaocontratoServiceImpl implements IManterformalizacaoalteracaocontratoService {

	/** Atributo factoryAdapter. */
	private FactoryAdapter factoryAdapter;

	/** Atributo TIPO_PARTICIPANTE. */
	private static final int TIPO_PARTICIPANTE = 1;
	
	/** Atributo TIPO_CONTA_VINCULO. */
	private static final int TIPO_CONTA_VINCULO = 2;
	
	/** Atributo TIPO_SERVICO. */
	private static final int TIPO_SERVICO = 3;
	
	/**
	 * Get: factoryAdapter.
	 *
	 * @return factoryAdapter
	 */
	public FactoryAdapter getFactoryAdapter() {
		return factoryAdapter;
	}

	/**
	 * Set: factoryAdapter.
	 *
	 * @param factoryAdapter the factory adapter
	 */
	public void setFactoryAdapter(FactoryAdapter factoryAdapter) {
		this.factoryAdapter = factoryAdapter;
	}

	
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.manterformalizacaoalteracaocontrato.IManterformalizacaoalteracaocontratoService#consultarListaManterFormAltContrato(br.com.bradesco.web.pgit.service.business.manterformalizacaoalteracaocontrato.bean.ConsultarManterFormAltContratoEntradaDTO)
	 */
	public List<ConsultarManterFormAltContratoSaidaDTO> consultarListaManterFormAltContrato(ConsultarManterFormAltContratoEntradaDTO entradaDTO) {
		
		List<ConsultarManterFormAltContratoSaidaDTO>  listaRetorno =  new ArrayList<ConsultarManterFormAltContratoSaidaDTO>();
		ConsultarAditivoContratoRequest request = new ConsultarAditivoContratoRequest();
		ConsultarAditivoContratoResponse response = new ConsultarAditivoContratoResponse();
		
		//TODO Ver os codigos fixos.	
		request.setNumeroOcorrencias(30);
		
		SimpleDateFormat formato2 = new SimpleDateFormat("dd/MM/yyyy");
		SimpleDateFormat formato1 = new SimpleDateFormat("dd.MM.yyyy");
		
		request.setCdPessoaJuridicaContrato(entradaDTO.getCdPessoaJuridicaContrato());
		request.setNrSequenciaContratoNegocio(entradaDTO.getNrSequenciaContratoNegocio());
		request.setCdTipoContratoNegocio(entradaDTO.getCdTipoContratoNegocio());
		request.setCdAcesso(entradaDTO.getCdAcesso());
		
		request.setCdSituacaoAditivoContrato(entradaDTO.getCdSituacaoAditivoContrato());
		request.setDtInicioInclusaoContrato(entradaDTO.getDtInicioInclusaoContrato());
		request.setDtFimInclusaoContrato(entradaDTO.getDtFimInclusaoContrato());
		request.setNrAditivoContratoNegocio(entradaDTO.getNrAditivoContratoNegocio());
		
		response  = getFactoryAdapter().getConsultarAditivoContratoPDCAdapter().invokeProcess(request);
		ConsultarManterFormAltContratoSaidaDTO saidaDTO;
		
		for(int i = 0; i < response.getOcorrenciasCount(); i++){
			saidaDTO =  new ConsultarManterFormAltContratoSaidaDTO();
			
			saidaDTO.setCodMensagem(response.getCodMensagem());
			saidaDTO.setMensagem(response.getMensagem());
			
			
			try{
				saidaDTO.setDtInclusaoAditivoContrato(String.valueOf(formato2.format(formato1.parse(response.getOcorrencias(i).getDtInclusaoAditivoContrato()))));
			}catch(ParseException e){
				saidaDTO.setDtInclusaoAditivoContrato("");
			}
			
			
			saidaDTO.setNrAditivoContratoNegocio(response.getOcorrencias(i).getNrAditivoContratoNegocio());
			saidaDTO.setDsSitucaoAditivoContrato(response.getOcorrencias(i).getDsSitucaoAditivoContrato());
			saidaDTO.setCdSituacaoAditivoContrato(response.getOcorrencias(i).getCdSituacaoAditivoContrato());
			saidaDTO.setHrInclusaoAditivoContrato(response.getOcorrencias(i).getHrInclusaoAditivoContrato());
			
			
			listaRetorno.add(saidaDTO);
		}
		
		return listaRetorno;
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.manterformalizacaoalteracaocontrato.IManterformalizacaoalteracaocontratoService#detalharManterFormAltContratoSaidaDTO(br.com.bradesco.web.pgit.service.business.manterformalizacaoalteracaocontrato.bean.DetalharManterFormAltContratoEntradaDTO)
	 */
	public DetalharManterFormAltContratoSaidaDTO detalharManterFormAltContratoSaidaDTO(
	        DetalharManterFormAltContratoEntradaDTO entradaDTO) {
	
		DetalharManterFormAltContratoSaidaDTO saidaDTO = new DetalharManterFormAltContratoSaidaDTO();
		
		DetalharAditivoContratoRequest request = new DetalharAditivoContratoRequest();
		DetalharAditivoContratoResponse response = new DetalharAditivoContratoResponse();
		
		request.setCdPessoaJuridicaContrato(verificaLongNulo(entradaDTO.getCdPessoaJuridicaContrato()));
		request.setCdTipoContratoNegocio(verificaIntegerNulo(entradaDTO.getCdTipoContratoNegocio()));
		request.setNrAditivoContratoNegocio(verificaLongNulo(entradaDTO.getNrAditivoContratoNegocio()));
		request.setNrSequenciaContratoNegocio(verificaLongNulo(entradaDTO.getNrSequenciaContratoNegocio()));
	
		response = getFactoryAdapter().getDetalharAditivoContratoPDCAdapter().invokeProcess(request);
		
		saidaDTO = new DetalharManterFormAltContratoSaidaDTO();
		saidaDTO.setCodMensagem(response.getCodMensagem());
		saidaDTO.setMensagem(response.getMensagem());
		
		saidaDTO.setCdUsuarioInclusaoExterno(formatarDataTrilha(response.getCdUsuarioInclusaoExterno()));
		saidaDTO.setCdOperacaoCanalInclusao(response.getCdOperacaoCanalInclusao());
		saidaDTO.setCdOperacaoCanalManutencao(response.getCdOperacaoCanalManutencao());
		saidaDTO.setCdTipoCanalInclusaoTrilha(response.getCdTipoCanalInclusaoTrilha());
		saidaDTO.setDsTipoCanalInclusaoTrilha(response.getDsTipoCanalInclusaoTrilha());
		saidaDTO.setHrInclusaoRegistroTrilha(formatarDataTrilha(response.getHrInclusaoRegistroTrilha()));
		saidaDTO.setHrInclusaoRegistro(formatarDataTrilha(response.getHrInclusaoRegistro()));
		
		saidaDTO.setCdTipoCanalManutencao(response.getCdTipoCanalManutencao());
		saidaDTO.setDsTipoCanalManutencao(response.getDsTipoCanalManutencao());
		saidaDTO.setCdUsuarioInclusao(response.getCdUsuarioInclusao());
		saidaDTO.setCdUsuarioManutencao(response.getCdUsuarioManutencao());
		saidaDTO.setCdUsuarioManutencaoExterno(response.getCdUsuarioManutencaoExterno());
		saidaDTO.setHrManutencaoRegistro(formatarDataTrilha(response.getHrManutencaoRegistro()));
		
		
		saidaDTO.setDsSituacaoAditivoContrato(response.getDsSituacaoAditivoContrato());
		saidaDTO.setHrAssinaturaAditivoContrato(formatarDataTrilha(response.getHrAssinaturaAditivoContrato()));
		saidaDTO.setDsMotivoSituacaoAditivo(response.getDsMotivoSituacaoAditivo());
		saidaDTO.setCdIndicadorContratoComl(response.getCdIndicadorContratoComl());
		
        saidaDTO.setCdFormaAutorizacaoPagamento(response.getCdFormaAutorizacaoPagamento());
        saidaDTO.setDsFormaAutorizacaoPagamento(response.getDsFormaAutorizacaoPagamento());
        saidaDTO.setCdUnidadeOrganizacional(response.getCdUnidadeOrganizacional());
        saidaDTO.setDsUnidadeOrganizacional(response.getDsUnidadeOrganizacional());
        saidaDTO.setCdFuncionarioGerenteContrato(response.getCdFuncionarioGerenteContrato());
        saidaDTO.setDsFuncionarioGerenteContrato(response.getDsFuncionarioGerenteContrato());
		
		/*Lista de Participantes*/
		List<ParticipantesDTO> listaParticipantes = new ArrayList<ParticipantesDTO>();
		ParticipantesDTO participantes;
		   for(int i=0;i<response.getNumeroOcorrenciasParticipante();i++){
			   participantes = new ParticipantesDTO(); 
			   
			
			   participantes.setNmRazaoSocialParticipante(response.getOcorrencias(i).getNmRazaoSocialVinc());
			   participantes.setNrCpfCnpjParticipante(response.getOcorrencias(i).getNrCpfCnpjVinc());
			   participantes.setDsAcaoManutencaoParticipante(response.getOcorrencias(i).getDsAcaoManutencaoPerfil());
			   participantes.setDsTipoParticipantePart(response.getOcorrencias(i).getDsTipoParticipantePart());
			   
			   listaParticipantes.add(participantes);
		   }
		 saidaDTO.setListaParticipantes(listaParticipantes); 
		 
		 
		 /*Lista de Servi�os*/
		 List<ServicosDTO> listaServicos = new ArrayList<ServicosDTO>();
		  ServicosDTO servicos;
		  for(int i=0;i<response.getNumeroOcorrenciasServicos();i++){
			   servicos = new ServicosDTO(); 
			   
			   		servicos.setDsProdutoServicoOperacao(response.getOcorrencias2(i).getDsProdutoServicoOperacao());
			   		servicos.setDsProdutoServicoRelacionado(response.getOcorrencias2(i).getDsProdutoServicoRelacionadoPerfil());
			   		servicos.setDsAcaoManutencaoServico(response.getOcorrencias2(i).getDsAcaoManutencaoPerfil());
		
			   listaServicos.add(servicos);
		   }
		saidaDTO.setListaServicos(listaServicos); 
		
		
		/*Lista de Contas-V�nculos*/
 	 List<VinculoDTO> listaVinculos = new ArrayList<VinculoDTO>(); 
	 	VinculoDTO vinculo;
		  for(int i=0;i<response.getNumeroOcorrenciasVinculacao();i++){
   		      vinculo = new VinculoDTO(); 
			 
			  vinculo.setCdBanco(response.getOcorrencias1(i).getCdBanco());
			  vinculo.setCdDigitoConta(response.getOcorrencias1(i).getCdDigitoConta());
			  vinculo.setDsAcaoManutencaoConta(response.getOcorrencias1(i).getDsAcaoManutencaoPerfil());
			  vinculo.setDsAgenciaBancaria(response.getOcorrencias1(i).getDsAgenciaBancaria());
			  vinculo.setDsBanco(response.getOcorrencias1(i).getDsBanco());
			  vinculo.setNmRazaoSocialVinc(response.getOcorrencias1(i).getNmRazaoSocialVinc());
			  vinculo.setCdContaBancaria(response.getOcorrencias1(i).getCdContaBancaria());
			  vinculo.setNrCpfCnpjVinc(response.getOcorrencias1(i).getNrCpfCnpjVinc());
			  vinculo.setCdAgenciaBancaria(response.getOcorrencias1(i).getCdAgenciaBancaria());
			  vinculo.setCdTipoConta(response.getOcorrencias1(i).getCdTipoConta());
			  listaVinculos.add(vinculo);
		   }
		saidaDTO.setListaVinculos(listaVinculos);
		
		saidaDTO.setNumeroOcorrenciasParticipante(verificaIntegerNulo(response.getNumeroOcorrenciasParticipante()));
		saidaDTO.setNumeroOcorrenciasServicos(verificaIntegerNulo(response.getNumeroOcorrenciasServicos()));
		saidaDTO.setNumeroOcorrenciasVinculacao(verificaIntegerNulo(response.getNumeroOcorrenciasVinculacao()));
		
		return saidaDTO;
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.manterformalizacaoalteracaocontrato.IManterformalizacaoalteracaocontratoService#excluirManterFormAltContratoSaidaDTO(br.com.bradesco.web.pgit.service.business.manterformalizacaoalteracaocontrato.bean.ExcluirManterFormAltContratoEntradaDTO)
	 */
	public ExcluirManterFormAltContratoSaidaDTO excluirManterFormAltContratoSaidaDTO(ExcluirManterFormAltContratoEntradaDTO entrada) {
		ExcluirAditivoContratoRequest request =  new ExcluirAditivoContratoRequest();
		request.setCdPessoaJuridicaContrato(PgitUtil.verificaLongNulo(entrada.getCodPessoaJuridica()));
		request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(entrada.getCodTipoContratoNegocio()));
		request.setNrAditivoContratoNegocio(PgitUtil.verificaLongNulo(entrada.getNumAditivoContratoNegocio()));
		request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(entrada.getNumSeqContratoNegocio()));

		ExcluirAditivoContratoResponse response = getFactoryAdapter().getExcluirAditivoContratoPDCAdapter().invokeProcess(request);

		return new ExcluirManterFormAltContratoSaidaDTO(response.getCodMensagem(), response.getMensagem());
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.manterformalizacaoalteracaocontrato.IManterformalizacaoalteracaocontratoService#incluirManterFormAltContratoSaidaDTO(br.com.bradesco.web.pgit.service.business.manterformalizacaoalteracaocontrato.bean.IncluirManterFormAltContratoEntradaDTO)
	 */
	public IncluirManterFormAltContratoSaidaDTO incluirManterFormAltContratoSaidaDTO(IncluirManterFormAltContratoEntradaDTO entradaDTO) {
		
		IncluirManterFormAltContratoSaidaDTO saidaDTO = new IncluirManterFormAltContratoSaidaDTO();
		IncluirAditivoContratoRequest request = new IncluirAditivoContratoRequest();
		IncluirAditivoContratoResponse response = new IncluirAditivoContratoResponse();
		
		request.setCdIndicadorContratoComl(entradaDTO.getCodIndicadorNovoContrato());
		request.setCdPessoaJuridicaContrato(entradaDTO.getCodPessoaJuridicaContrato());
		request.setCdTipoContratoNegocio(entradaDTO.getCodTipoContratoNegocio());
		request.setNrSequenciaContratoNegocio(entradaDTO.getNumSeqContratoNegocio());
		response = getFactoryAdapter().getIncluirAditivoContratoPDCAdapter().invokeProcess(request);
		
		saidaDTO.setCodMensagem(response.getCodMensagem());
		saidaDTO.setMensagem(response.getMensagem()); 
		
		saidaDTO.setCdpessoaJuridicaContrato(response.getCdpessoaJuridicaContrato());
	    saidaDTO.setCdTipoContratoNegocio(response.getCdTipoContratoNegocio());
	    saidaDTO.setNrSequenciaContratoNegocio(response.getNrSequenciaContratoNegocio());
	    saidaDTO.setNrAditivoContratoNegocio(response.getNrAditivoContratoNegocio());
		
		return saidaDTO;
	}
	
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.manterformalizacaoalteracaocontrato.IManterformalizacaoalteracaocontratoService#conRelacaoConManutencoes(br.com.bradesco.web.pgit.service.business.manterformalizacaoalteracaocontrato.bean.ConRelacaoConManutencoesEntradaDTO)
	 */
	public ConRelacaoConManutencoesSaidaDTO conRelacaoConManutencoes(ConRelacaoConManutencoesEntradaDTO entrada){
		ConRelacaoConManutencoesSaidaDTO saida = new ConRelacaoConManutencoesSaidaDTO();
		
		ConRelacaoConManutencoesRequest request = new ConRelacaoConManutencoesRequest();
		ConRelacaoConManutencoesResponse response = new ConRelacaoConManutencoesResponse();
		
		request.setCdPessoaJuridicaContrato(entrada.getCdPessoaJuridicaContrato());
		request.setCdTipoContratoNegocio(entrada.getCdTipoContratoNegocio());
		request.setNrSequenciaContratoNegocio(entrada.getNrSequenciaContratoNegocio());
		
		/*Lista de Participantes*/
		request.setCdTipoManutencaoContrato(TIPO_PARTICIPANTE);
        response = getFactoryAdapter().getConRelacaoConManutencoesPDCAdapter().invokeProcess(request);
		preencherParticipantesRelacaoConManutencoes(saida, response);
		  
        /*Lista de Contas-V�nculos*/
		request.setCdTipoManutencaoContrato(TIPO_CONTA_VINCULO);
        response = getFactoryAdapter().getConRelacaoConManutencoesPDCAdapter().invokeProcess(request);
        preencherContasVinculosRelacaoConManutencoes(saida, response);
        
		 /*Lista de Servi�os*/
        request.setCdTipoManutencaoContrato(TIPO_SERVICO);
        response = getFactoryAdapter().getConRelacaoConManutencoesPDCAdapter().invokeProcess(request);
		preencherServicosRelacaoConManutencoes(saida, response); 
		
		saida.setCodMensagem(response.getCodMensagem());
        saida.setMensagem(response.getMensagem());
        
        saida.setCdFormaAutorizacaoPagamento(response.getCdFormaAutorizacaoPagamento());
        saida.setDsFormaAutorizacaoPagamento(response.getDsFormaAutorizacaoPagamento());
        saida.setCdUnidadeOrganizacional(response.getCdUnidadeOrganizacional());
        saida.setDsUnidadeOrganizacional(response.getDsUnidadeOrganizacional());
        saida.setCdFuncionarioGerenteContrato(response.getCdFuncionarioGerenteContrato());
        saida.setDsFuncionarioGerenteContrato(response.getDsFuncionarioGerenteContrato());
		
		return saida;
	}

    /**
     * Preencher servicos relacao con manutencoes.
     * 
     * @param saida
     *            the saida
     * @param response
     *            the response
     */
    private void preencherServicosRelacaoConManutencoes(ConRelacaoConManutencoesSaidaDTO saida,
        ConRelacaoConManutencoesResponse response) {
        
        List<ServicosDTO> listaServicos = new ArrayList<ServicosDTO>();
        ServicosDTO servicos;
        for (int i = 0; i < response.getNumeroOcorrenciasServico(); i++) {
            servicos = new ServicosDTO();

            servicos.setDsProdutoServicoOperacao(response.getOcorrencias2(i).getDsProdutoServicoOperacao());
            servicos.setDsProdutoServicoRelacionado(response.getOcorrencias2(i).getDsProdutoServicoRelacionadoPerfil());
            servicos.setDsAcaoManutencaoServico(response.getOcorrencias2(i).getDsAcaoManutencaoPerfil());

            listaServicos.add(servicos);
        }
        saida.setListaServicos(listaServicos);
        saida.setNumeroOcorrenciasServicos(response.getNumeroOcorrenciasServico());
    }

    /**
     * Preencher contas vinculos relacao con manutencoes.
     * 
     * @param saida
     *            the saida
     * @param response
     *            the response
     */
    private void preencherContasVinculosRelacaoConManutencoes(ConRelacaoConManutencoesSaidaDTO saida,
        ConRelacaoConManutencoesResponse response) {
        
        List<VinculoRelacaoConManutencoesDTO> listaVinculos = new ArrayList<VinculoRelacaoConManutencoesDTO>();
        VinculoRelacaoConManutencoesDTO vinculo;
        for (int i = 0; i < response.getNumeroOcorrenciasVinculacao(); i++) {
            vinculo = new VinculoRelacaoConManutencoesDTO();
            vinculo.setCdBanco(response.getOcorrencias1(i).getCdBanco());
            vinculo.setCdDigitoConta(response.getOcorrencias1(i).getCdDigitoConta());
            vinculo.setDsAcaoManutencaoConta(response.getOcorrencias1(i).getDsAcaoManutencaoPerfil());
            vinculo.setDsAgencia(response.getOcorrencias1(i).getCdAgencia() + " - "
                + response.getOcorrencias1(i).getDsAgencia());
            vinculo.setDsBanco(response.getOcorrencias1(i).getCdBanco() + " - "
                + response.getOcorrencias1(i).getDsBanco());
            vinculo.setNmRazaoSocialConta(response.getOcorrencias1(i).getNmRazaoSocialConta());
            vinculo.setCdConta(response.getOcorrencias1(i).getCdConta());
            vinculo.setNrCpfCnpjConta(response.getOcorrencias1(i).getNrCpfCnpjConta());
            vinculo.setCdAgencia(response.getOcorrencias1(i).getCdAgencia());
            vinculo.setCdTipoConta(response.getOcorrencias1(i).getCdTipoConta());
            vinculo.setDsConta(response.getOcorrencias1(i).getCdConta() + " - "
                + response.getOcorrencias1(i).getCdDigitoConta());

            listaVinculos.add(vinculo);
        }
        saida.setListaVinculos(listaVinculos);
        saida.setNumeroOcorrenciasVinculacao(response.getNumeroOcorrenciasVinculacao());
    }

    /**
     * Preencher participantes relacao con manutencoes.
     * 
     * @param saida
     *            the saida
     * @param response
     *            the response
     */
    private void preencherParticipantesRelacaoConManutencoes(ConRelacaoConManutencoesSaidaDTO saida,
        ConRelacaoConManutencoesResponse response) {

        List<ParticipantesRelacaoConManutencoesDTO> listaParticipantes =
            new ArrayList<ParticipantesRelacaoConManutencoesDTO>();
        ParticipantesRelacaoConManutencoesDTO participantes;
        for (int i = 0; i < response.getNumeroOcorrenciasParticipante(); i++) {
            participantes = new ParticipantesRelacaoConManutencoesDTO();

            participantes.setNmRazaoSocialParticipante(response.getOcorrencias(i).getNmRazaoSocialConta());
            participantes.setNrCpfCnpjParticipante(response.getOcorrencias(i).getNrCpfCnpjConta());
            participantes.setDsAcaoManutencaoParticipante(response.getOcorrencias(i).getDsAcaoManutencaoPerfil());
            participantes.setDsTipoParticipacaoPessoa(response.getOcorrencias(i).getDsTipoParticipacaoPessoa());

            listaParticipantes.add(participantes);
        }
        saida.setListaParticipantes(listaParticipantes);
        saida.setNumeroOcorrenciasParticipante(response.getNumeroOcorrenciasParticipante());
    }
	
}

