/*
 * Nome: br.com.bradesco.web.pgit.service.business.combo.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.combo.bean;

/**
 * Nome: ListarFinalidadeContaSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarFinalidadeContaSaidaDTO {
	
	/** Atributo cdFinalidadeContaPagamento. */
	private Integer cdFinalidadeContaPagamento;
    
    /** Atributo rsFinalidadeContaPagamento. */
    private String rsFinalidadeContaPagamento;

    /**
     * Listar finalidade conta saida dto.
     *
     * @param cdFinalidadeContaPagamento the cd finalidade conta pagamento
     * @param rsFinalidadeContaPagamento the rs finalidade conta pagamento
     */
    public ListarFinalidadeContaSaidaDTO(Integer cdFinalidadeContaPagamento, String rsFinalidadeContaPagamento) {
		super();
		this.cdFinalidadeContaPagamento = cdFinalidadeContaPagamento;
		this.rsFinalidadeContaPagamento = rsFinalidadeContaPagamento;
	}

	/**
	 * Get: cdFinalidadeContaPagamento.
	 *
	 * @return cdFinalidadeContaPagamento
	 */
	public Integer getCdFinalidadeContaPagamento() {
		return cdFinalidadeContaPagamento;
	}

	/**
	 * Set: cdFinalidadeContaPagamento.
	 *
	 * @param cdFinalidadeContaPagamento the cd finalidade conta pagamento
	 */
	public void setCdFinalidadeContaPagamento(Integer cdFinalidadeContaPagamento) {
		this.cdFinalidadeContaPagamento = cdFinalidadeContaPagamento;
	}

	/**
	 * Get: rsFinalidadeContaPagamento.
	 *
	 * @return rsFinalidadeContaPagamento
	 */
	public String getRsFinalidadeContaPagamento() {
		return rsFinalidadeContaPagamento;
	}

	/**
	 * Set: rsFinalidadeContaPagamento.
	 *
	 * @param rsFinalidadeContaPagamento the rs finalidade conta pagamento
	 */
	public void setRsFinalidadeContaPagamento(String rsFinalidadeContaPagamento) {
		this.rsFinalidadeContaPagamento = rsFinalidadeContaPagamento;
	}


}
