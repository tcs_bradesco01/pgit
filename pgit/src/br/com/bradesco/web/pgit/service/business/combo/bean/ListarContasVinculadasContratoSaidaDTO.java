/*
 * Nome: br.com.bradesco.web.pgit.service.business.combo.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.combo.bean;

/**
 * Nome: ListarContasVinculadasContratoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarContasVinculadasContratoSaidaDTO {
    
    /** Atributo codMensagem. */
    private String codMensagem;
    
    /** Atributo mensagem. */
    private String mensagem;
    
    /** Atributo cdPessoaJuridicaVinculo. */
    private Long cdPessoaJuridicaVinculo;
    
    /** Atributo cdTipoContratoVinculo. */
    private Integer cdTipoContratoVinculo;
    
    /** Atributo nrSequenciaContratoVinculo. */
    private Long nrSequenciaContratoVinculo;
    
    /** Atributo cdTipoVinculoContrato. */
    private Integer cdTipoVinculoContrato;
    
    /** Atributo cdCpfCnpj. */
    private String cdCpfCnpj;
    
    /** Atributo dsNomeParticipante. */
    private String dsNomeParticipante;
    
    /** Atributo cdBanco. */
    private Integer cdBanco;
    
    /** Atributo dsBanco. */
    private String dsBanco;
    
    /** Atributo cdAgencia. */
    private Integer cdAgencia;
    
    /** Atributo cdDigitoAgencia. */
    private Integer cdDigitoAgencia;
    
    /** Atributo dsAgencia. */
    private String dsAgencia;
    
    /** Atributo cdConta. */
    private Long cdConta;
    
    /** Atributo cdDigitoConta. */
    private String cdDigitoConta;
    
    /** Atributo cdTipoConta. */
    private Integer cdTipoConta;
    
    /** Atributo dsTipoConta. */
    private String dsTipoConta;
    
    /** Atributo cdTipoDebito. */
    private String cdTipoDebito;
    
    /** Atributo cdTipoTarifa. */
    private String cdTipoTarifa;
    
    /** Atributo cdTipoEstorno. */
    private String cdTipoEstorno;
    
    /** Atributo dsAlternativa. */
    private String dsAlternativa;
    
    /** Atributo cdParticipante. */
    private Long cdParticipante;
    
    /**
     * Listar contas vinculadas contrato saida dto.
     *
     * @param cdBanco the cd banco
     * @param cdAgencia the cd agencia
     * @param cdDigitoAgencia the cd digito agencia
     * @param cdConta the cd conta
     * @param dsBanco the ds banco
     * @param dsAgencia the ds agencia
     * @param cdDigitoConta the cd digito conta
     * @param cdTipoConta the cd tipo conta
     * @param dsTipoConta the ds tipo conta
     */
    public ListarContasVinculadasContratoSaidaDTO(Integer cdBanco, Integer cdAgencia,Integer cdDigitoAgencia, Long cdConta,
    											   String dsBanco, String dsAgencia, String cdDigitoConta, Integer cdTipoConta, 
    											   String dsTipoConta, String cdCpfCnpj, String dsNomeParticipante,
    											   Long cdPessoaJuridicaVinculo, Integer cdTipoContratoVinculo, Long nrSequenciaContratoVinculo){
    	this.cdBanco = cdBanco;
    	this.cdAgencia = cdAgencia;
    	this.cdDigitoAgencia = cdDigitoAgencia;
    	this.cdConta = cdConta;
    	this.dsBanco = dsBanco;
    	this.dsAgencia = dsAgencia;
    	this.cdDigitoConta = cdDigitoConta;
    	this.cdTipoConta = cdTipoConta;
    	this.dsTipoConta = dsTipoConta;
    	this.cdCpfCnpj = cdCpfCnpj;
    	this.dsNomeParticipante = dsNomeParticipante;
    	this.cdPessoaJuridicaVinculo = cdPessoaJuridicaVinculo;
    	this.cdTipoContratoVinculo = cdTipoContratoVinculo;
    	this.nrSequenciaContratoVinculo = nrSequenciaContratoVinculo;
    }
    
	/**
	 * Get: cdAgencia.
	 *
	 * @return cdAgencia
	 */
	public Integer getCdAgencia() {
		return cdAgencia;
	}
	
	/**
	 * Set: cdAgencia.
	 *
	 * @param cdAgencia the cd agencia
	 */
	public void setCdAgencia(Integer cdAgencia) {
		this.cdAgencia = cdAgencia;
	}
	
	/**
	 * Get: cdBanco.
	 *
	 * @return cdBanco
	 */
	public Integer getCdBanco() {
		return cdBanco;
	}
	
	/**
	 * Set: cdBanco.
	 *
	 * @param cdBanco the cd banco
	 */
	public void setCdBanco(Integer cdBanco) {
		this.cdBanco = cdBanco;
	}
	
	/**
	 * Get: cdConta.
	 *
	 * @return cdConta
	 */
	public Long getCdConta() {
		return cdConta;
	}
	
	/**
	 * Set: cdConta.
	 *
	 * @param cdConta the cd conta
	 */
	public void setCdConta(Long cdConta) {
		this.cdConta = cdConta;
	}
	
	/**
	 * Get: cdCpfCnpj.
	 *
	 * @return cdCpfCnpj
	 */
	public String getCdCpfCnpj() {
		return cdCpfCnpj;
	}
	
	/**
	 * Set: cdCpfCnpj.
	 *
	 * @param cdCpfCnpj the cd cpf cnpj
	 */
	public void setCdCpfCnpj(String cdCpfCnpj) {
		this.cdCpfCnpj = cdCpfCnpj;
	}
	
	/**
	 * Get: cdDigitoAgencia.
	 *
	 * @return cdDigitoAgencia
	 */
	public Integer getCdDigitoAgencia() {
		return cdDigitoAgencia;
	}
	
	/**
	 * Set: cdDigitoAgencia.
	 *
	 * @param cdDigitoAgencia the cd digito agencia
	 */
	public void setCdDigitoAgencia(Integer cdDigitoAgencia) {
		this.cdDigitoAgencia = cdDigitoAgencia;
	}
	
	/**
	 * Get: cdDigitoConta.
	 *
	 * @return cdDigitoConta
	 */
	public String getCdDigitoConta() {
		return cdDigitoConta;
	}
	
	/**
	 * Set: cdDigitoConta.
	 *
	 * @param cdDigitoConta the cd digito conta
	 */
	public void setCdDigitoConta(String cdDigitoConta) {
		this.cdDigitoConta = cdDigitoConta;
	}
	
	/**
	 * Get: cdPessoaJuridicaVinculo.
	 *
	 * @return cdPessoaJuridicaVinculo
	 */
	public Long getCdPessoaJuridicaVinculo() {
		return cdPessoaJuridicaVinculo;
	}
	
	/**
	 * Set: cdPessoaJuridicaVinculo.
	 *
	 * @param cdPessoaJuridicaVinculo the cd pessoa juridica vinculo
	 */
	public void setCdPessoaJuridicaVinculo(Long cdPessoaJuridicaVinculo) {
		this.cdPessoaJuridicaVinculo = cdPessoaJuridicaVinculo;
	}
	
	/**
	 * Get: cdTipoConta.
	 *
	 * @return cdTipoConta
	 */
	public Integer getCdTipoConta() {
		return cdTipoConta;
	}
	
	/**
	 * Set: cdTipoConta.
	 *
	 * @param cdTipoConta the cd tipo conta
	 */
	public void setCdTipoConta(Integer cdTipoConta) {
		this.cdTipoConta = cdTipoConta;
	}
	
	/**
	 * Get: dsTipoConta.
	 *
	 * @return dsTipoConta
	 */
	public String getDsTipoConta() {
		return dsTipoConta;
	}
	
	/**
	 * Set: dsTipoConta.
	 *
	 * @param dsTipoConta the ds tipo conta
	 */
	public void setDsTipoConta(String dsTipoConta) {
		this.dsTipoConta = dsTipoConta;
	}
	
	/**
	 * Get: cdTipoContratoVinculo.
	 *
	 * @return cdTipoContratoVinculo
	 */
	public Integer getCdTipoContratoVinculo() {
		return cdTipoContratoVinculo;
	}
	
	/**
	 * Set: cdTipoContratoVinculo.
	 *
	 * @param cdTipoContratoVinculo the cd tipo contrato vinculo
	 */
	public void setCdTipoContratoVinculo(Integer cdTipoContratoVinculo) {
		this.cdTipoContratoVinculo = cdTipoContratoVinculo;
	}
	
	/**
	 * Get: cdTipoDebito.
	 *
	 * @return cdTipoDebito
	 */
	public String getCdTipoDebito() {
		return cdTipoDebito;
	}
	
	/**
	 * Set: cdTipoDebito.
	 *
	 * @param cdTipoDebito the cd tipo debito
	 */
	public void setCdTipoDebito(String cdTipoDebito) {
		this.cdTipoDebito = cdTipoDebito;
	}
	
	/**
	 * Get: cdTipoEstorno.
	 *
	 * @return cdTipoEstorno
	 */
	public String getCdTipoEstorno() {
		return cdTipoEstorno;
	}
	
	/**
	 * Set: cdTipoEstorno.
	 *
	 * @param cdTipoEstorno the cd tipo estorno
	 */
	public void setCdTipoEstorno(String cdTipoEstorno) {
		this.cdTipoEstorno = cdTipoEstorno;
	}
	
	/**
	 * Get: cdTipoTarifa.
	 *
	 * @return cdTipoTarifa
	 */
	public String getCdTipoTarifa() {
		return cdTipoTarifa;
	}
	
	/**
	 * Set: cdTipoTarifa.
	 *
	 * @param cdTipoTarifa the cd tipo tarifa
	 */
	public void setCdTipoTarifa(String cdTipoTarifa) {
		this.cdTipoTarifa = cdTipoTarifa;
	}
	
	/**
	 * Get: cdTipoVinculoContrato.
	 *
	 * @return cdTipoVinculoContrato
	 */
	public Integer getCdTipoVinculoContrato() {
		return cdTipoVinculoContrato;
	}
	
	/**
	 * Set: cdTipoVinculoContrato.
	 *
	 * @param cdTipoVinculoContrato the cd tipo vinculo contrato
	 */
	public void setCdTipoVinculoContrato(Integer cdTipoVinculoContrato) {
		this.cdTipoVinculoContrato = cdTipoVinculoContrato;
	}
	
	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}
	
	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}
	
	/**
	 * Get: dsAgencia.
	 *
	 * @return dsAgencia
	 */
	public String getDsAgencia() {
		return dsAgencia;
	}
	
	/**
	 * Set: dsAgencia.
	 *
	 * @param dsAgencia the ds agencia
	 */
	public void setDsAgencia(String dsAgencia) {
		this.dsAgencia = dsAgencia;
	}
	
	/**
	 * Get: dsAlternativa.
	 *
	 * @return dsAlternativa
	 */
	public String getDsAlternativa() {
		return dsAlternativa;
	}
	
	/**
	 * Set: dsAlternativa.
	 *
	 * @param dsAlternativa the ds alternativa
	 */
	public void setDsAlternativa(String dsAlternativa) {
		this.dsAlternativa = dsAlternativa;
	}
	
	/**
	 * Get: dsBanco.
	 *
	 * @return dsBanco
	 */
	public String getDsBanco() {
		return dsBanco;
	}
	
	/**
	 * Set: dsBanco.
	 *
	 * @param dsBanco the ds banco
	 */
	public void setDsBanco(String dsBanco) {
		this.dsBanco = dsBanco;
	}
	
	/**
	 * Get: dsNomeParticipante.
	 *
	 * @return dsNomeParticipante
	 */
	public String getDsNomeParticipante() {
		return dsNomeParticipante;
	}
	
	/**
	 * Set: dsNomeParticipante.
	 *
	 * @param dsNomeParticipante the ds nome participante
	 */
	public void setDsNomeParticipante(String dsNomeParticipante) {
		this.dsNomeParticipante = dsNomeParticipante;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	/**
	 * Get: nrSequenciaContratoVinculo.
	 *
	 * @return nrSequenciaContratoVinculo
	 */
	public Long getNrSequenciaContratoVinculo() {
		return nrSequenciaContratoVinculo;
	}
	
	/**
	 * Set: nrSequenciaContratoVinculo.
	 *
	 * @param nrSequenciaContratoVinculo the nr sequencia contrato vinculo
	 */
	public void setNrSequenciaContratoVinculo(Long nrSequenciaContratoVinculo) {
		this.nrSequenciaContratoVinculo = nrSequenciaContratoVinculo;
	}

	/**
	 * Get: cdParticipante.
	 *
	 * @return cdParticipante
	 */
	public Long getCdParticipante() {
	    return cdParticipante;
	}

	/**
	 * Set: cdParticipante.
	 *
	 * @param cdParticipante the cd participante
	 */
	public void setCdParticipante(Long cdParticipante) {
	    this.cdParticipante = cdParticipante;
	}
	
	public String getCdAgenciaDsAgencia() {
		return cdAgencia.toString() + "-" + dsAgencia;
	}
	public String getCdContaCdDigito() {
		return Long.toString(cdConta).trim() + "-" + cdDigitoConta;
	}
}
