/*
 * Nome: br.com.bradesco.web.pgit.service.business.msgalertagestor.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.msgalertagestor.bean;

/**
 * Nome: ListarMsgAlertaGestorSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarMsgAlertaGestorSaidaDTO {
	
	/** Atributo codMensagem. */
	private String codMensagem;
	
	/** Atributo mensagem. */
	private String mensagem;
	
	/** Atributo centroCusto. */
	private String centroCusto;
	
	/** Atributo tipoProcesso. */
	private int tipoProcesso;
	
	/** Atributo dsTipoProcesso. */
	private String dsTipoProcesso;	
	
	/** Atributo cdPessoa. */
	private long cdPessoa;
	
	/** Atributo dsPessoa. */
	private String dsPessoa;
	
	/** Atributo cdCist. */
	private String cdCist;
	
	/** Atributo tipoMensagem. */
	private int tipoMensagem;
	
	/** Atributo dsTipoMensagem. */
	private String dsTipoMensagem;	
	
	/** Atributo recurso. */
	private int recurso;
	
	/** Atributo dsRecurso. */
	private String dsRecurso;	
	
	/** Atributo idioma. */
	private int idioma;
	
	/** Atributo dsIdioma. */
	private String dsIdioma;
	
	/** Atributo nrSeqMensagemAlerta. */
	private int nrSeqMensagemAlerta;
	
	/** Atributo cdProcsSistema. */
	private int cdProcsSistema;
	
	/**
	 * Get: cdProcsSistema.
	 *
	 * @return cdProcsSistema
	 */
	public int getCdProcsSistema() {
		return cdProcsSistema;
	}
	
	/**
	 * Set: cdProcsSistema.
	 *
	 * @param cdProcsSistema the cd procs sistema
	 */
	public void setCdProcsSistema(int cdProcsSistema) {
		this.cdProcsSistema = cdProcsSistema;
	}
	
	/**
	 * Get: nrSeqMensagemAlerta.
	 *
	 * @return nrSeqMensagemAlerta
	 */
	public int getNrSeqMensagemAlerta() {
		return nrSeqMensagemAlerta;
	}
	
	/**
	 * Set: nrSeqMensagemAlerta.
	 *
	 * @param nrSeqMensagemAlerta the nr seq mensagem alerta
	 */
	public void setNrSeqMensagemAlerta(int nrSeqMensagemAlerta) {
		this.nrSeqMensagemAlerta = nrSeqMensagemAlerta;
	}
	
	/**
	 * Get: cdCist.
	 *
	 * @return cdCist
	 */
	public String getCdCist() {
		return cdCist;
	}
	
	/**
	 * Set: cdCist.
	 *
	 * @param cdCist the cd cist
	 */
	public void setCdCist(String cdCist) {
		this.cdCist = cdCist;
	}
	
	/**
	 * Get: cdPessoa.
	 *
	 * @return cdPessoa
	 */
	public long getCdPessoa() {
		return cdPessoa;
	}
	
	/**
	 * Set: cdPessoa.
	 *
	 * @param cdPessoa the cd pessoa
	 */
	public void setCdPessoa(long cdPessoa) {
		this.cdPessoa = cdPessoa;
	}
	
	/**
	 * Get: centroCusto.
	 *
	 * @return centroCusto
	 */
	public String getCentroCusto() {
		return centroCusto;
	}
	
	/**
	 * Set: centroCusto.
	 *
	 * @param centroCusto the centro custo
	 */
	public void setCentroCusto(String centroCusto) {
		this.centroCusto = centroCusto;
	}
	
	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}
	
	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}
	
	/**
	 * Get: dsIdioma.
	 *
	 * @return dsIdioma
	 */
	public String getDsIdioma() {
		return dsIdioma;
	}
	
	/**
	 * Set: dsIdioma.
	 *
	 * @param dsIdioma the ds idioma
	 */
	public void setDsIdioma(String dsIdioma) {
		this.dsIdioma = dsIdioma;
	}
	
	/**
	 * Get: dsPessoa.
	 *
	 * @return dsPessoa
	 */
	public String getDsPessoa() {
		return dsPessoa;
	}
	
	/**
	 * Set: dsPessoa.
	 *
	 * @param dsPessoa the ds pessoa
	 */
	public void setDsPessoa(String dsPessoa) {
		this.dsPessoa = dsPessoa;
	}
	
	/**
	 * Get: dsRecurso.
	 *
	 * @return dsRecurso
	 */
	public String getDsRecurso() {
		return dsRecurso;
	}
	
	/**
	 * Set: dsRecurso.
	 *
	 * @param dsRecurso the ds recurso
	 */
	public void setDsRecurso(String dsRecurso) {
		this.dsRecurso = dsRecurso;
	}
	
	/**
	 * Get: dsTipoMensagem.
	 *
	 * @return dsTipoMensagem
	 */
	public String getDsTipoMensagem() {
		return dsTipoMensagem;
	}
	
	/**
	 * Set: dsTipoMensagem.
	 *
	 * @param dsTipoMensagem the ds tipo mensagem
	 */
	public void setDsTipoMensagem(String dsTipoMensagem) {
		this.dsTipoMensagem = dsTipoMensagem;
	}
	
	/**
	 * Get: dsTipoProcesso.
	 *
	 * @return dsTipoProcesso
	 */
	public String getDsTipoProcesso() {
		return dsTipoProcesso;
	}
	
	/**
	 * Set: dsTipoProcesso.
	 *
	 * @param dsTipoProcesso the ds tipo processo
	 */
	public void setDsTipoProcesso(String dsTipoProcesso) {
		this.dsTipoProcesso = dsTipoProcesso;
	}
	
	/**
	 * Get: idioma.
	 *
	 * @return idioma
	 */
	public int getIdioma() {
		return idioma;
	}
	
	/**
	 * Set: idioma.
	 *
	 * @param idioma the idioma
	 */
	public void setIdioma(int idioma) {
		this.idioma = idioma;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	/**
	 * Get: recurso.
	 *
	 * @return recurso
	 */
	public int getRecurso() {
		return recurso;
	}
	
	/**
	 * Set: recurso.
	 *
	 * @param recurso the recurso
	 */
	public void setRecurso(int recurso) {
		this.recurso = recurso;
	}
	
	/**
	 * Get: tipoMensagem.
	 *
	 * @return tipoMensagem
	 */
	public int getTipoMensagem() {
		return tipoMensagem;
	}
	
	/**
	 * Set: tipoMensagem.
	 *
	 * @param tipoMensagem the tipo mensagem
	 */
	public void setTipoMensagem(int tipoMensagem) {
		this.tipoMensagem = tipoMensagem;
	}
	
	/**
	 * Get: tipoProcesso.
	 *
	 * @return tipoProcesso
	 */
	public int getTipoProcesso() {
		return tipoProcesso;
	}
	
	/**
	 * Set: tipoProcesso.
	 *
	 * @param tipoProcesso the tipo processo
	 */
	public void setTipoProcesso(int tipoProcesso) {
		this.tipoProcesso = tipoProcesso;
	}	
	
	
	


}
