/*
 * Nome: br.com.bradesco.web.pgit.service.business.assoclayoutarqprodserv.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.assoclayoutarqprodserv.bean;

/**
 * Nome: ListarAssLayoutArqProdServicoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarAssLayoutArqProdServicoSaidaDTO {
	
	/** Atributo codMensagem. */
	private String codMensagem;
	
	/** Atributo mensagem. */
	private String mensagem;
	
	/** Atributo cdTipoLayoutArquivo. */
	private Integer cdTipoLayoutArquivo;
	
	/** Atributo dsTipoLayoutArquivo. */
	private String dsTipoLayoutArquivo;
	
	/** Atributo cdTipoServico. */
	private Integer cdTipoServico;
	
	/** Atributo dsTipoServico. */
	private String dsTipoServico;
	
	/** Atributo dsIdentificadorLayoutNegocio. */
	private String dsIdentificadorLayoutNegocio;
	
	/** Atributo cdIdentificadorLayoutNegocio. */
	private Integer cdIdentificadorLayoutNegocio;
	
	/**
	 * Get: cdTipoLayoutArquivo.
	 *
	 * @return cdTipoLayoutArquivo
	 */
	public Integer getCdTipoLayoutArquivo() {
		return cdTipoLayoutArquivo;
	}
	
	/**
	 * Set: cdTipoLayoutArquivo.
	 *
	 * @param cdTipoLayoutArquivo the cd tipo layout arquivo
	 */
	public void setCdTipoLayoutArquivo(Integer cdTipoLayoutArquivo) {
		this.cdTipoLayoutArquivo = cdTipoLayoutArquivo;
	}
	
	/**
	 * Get: cdTipoServico.
	 *
	 * @return cdTipoServico
	 */
	public Integer getCdTipoServico() {
		return cdTipoServico;
	}
	
	/**
	 * Set: cdTipoServico.
	 *
	 * @param cdTipoServico the cd tipo servico
	 */
	public void setCdTipoServico(Integer cdTipoServico) {
		this.cdTipoServico = cdTipoServico;
	}
	
	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}
	
	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}
	
	/**
	 * Get: dsTipoLayoutArquivo.
	 *
	 * @return dsTipoLayoutArquivo
	 */
	public String getDsTipoLayoutArquivo() {
		return dsTipoLayoutArquivo;
	}
	
	/**
	 * Set: dsTipoLayoutArquivo.
	 *
	 * @param dsTipoLayoutArquivo the ds tipo layout arquivo
	 */
	public void setDsTipoLayoutArquivo(String dsTipoLayoutArquivo) {
		this.dsTipoLayoutArquivo = dsTipoLayoutArquivo;
	}
	
	/**
	 * Get: dsTipoServico.
	 *
	 * @return dsTipoServico
	 */
	public String getDsTipoServico() {
		return dsTipoServico;
	}
	
	/**
	 * Set: dsTipoServico.
	 *
	 * @param dsTipoServico the ds tipo servico
	 */
	public void setDsTipoServico(String dsTipoServico) {
		this.dsTipoServico = dsTipoServico;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	/**
	 * (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof ListarAssLayoutArqProdServicoSaidaDTO)) {
			return false;
		}

		ListarAssLayoutArqProdServicoSaidaDTO other = (ListarAssLayoutArqProdServicoSaidaDTO) obj;
		return this.getCdTipoLayoutArquivo().equals(other.getCdTipoLayoutArquivo())
			&& this.getCdTipoServico().equals(other.getCdTipoServico());
	}

	/**
	 * (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		int hashCode = 7;
		hashCode = hashCode + getCdTipoLayoutArquivo().hashCode();
		hashCode = hashCode + getCdTipoServico().hashCode();
		return hashCode;
	}
	
	/**
	 * Get: cdIdentificadorLayoutNegocio.
	 *
	 * @return cdIdentificadorLayoutNegocio
	 */
	public Integer getCdIdentificadorLayoutNegocio() {
		return cdIdentificadorLayoutNegocio;
	}
	
	/**
	 * Set: cdIdentificadorLayoutNegocio.
	 *
	 * @param cdIdentificadorLayoutNegocio the cd identificador layout negocio
	 */
	public void setCdIdentificadorLayoutNegocio(Integer cdIdentificadorLayoutNegocio) {
		this.cdIdentificadorLayoutNegocio = cdIdentificadorLayoutNegocio;
	}
	
	/**
	 * Get: dsIdentificadorLayoutNegocio.
	 *
	 * @return dsIdentificadorLayoutNegocio
	 */
	public String getDsIdentificadorLayoutNegocio() {
		return dsIdentificadorLayoutNegocio;
	}
	
	/**
	 * Set: dsIdentificadorLayoutNegocio.
	 *
	 * @param dsIdentificadorLayoutNegocio the ds identificador layout negocio
	 */
	public void setDsIdentificadorLayoutNegocio(String dsIdentificadorLayoutNegocio) {
		this.dsIdentificadorLayoutNegocio = dsIdentificadorLayoutNegocio;
	}

}
