/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/interfaz.ftl,v $
 * $Id: interfaz.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: interfaz.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.manterlibpreviasemconsaldos;

import java.util.List;

import br.com.bradesco.web.pgit.service.business.manterlibpreviasemconsaldos.bean.AlterarLiberacaoPreviaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterlibpreviasemconsaldos.bean.AlterarLiberacaoPreviaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterlibpreviasemconsaldos.bean.ConsultarDetLiberacaoPreviaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterlibpreviasemconsaldos.bean.ConsultarDetLiberacaoPreviaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterlibpreviasemconsaldos.bean.ConsultarLiberacaoPreviaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterlibpreviasemconsaldos.bean.ConsultarLiberacaoPreviaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterlibpreviasemconsaldos.bean.ExcluirLiberacaoPreviaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterlibpreviasemconsaldos.bean.ExcluirLiberacaoPreviaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterlibpreviasemconsaldos.bean.IncluirLiberacaoPreviaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterlibpreviasemconsaldos.bean.IncluirLiberacaoPreviaSaidaDTO;

/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Interface do adaptador: ManterLibPreviaSemConSaldos
 * </p>
 * 
 * @comment CODIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public interface IManterLibPreviaSemConSaldosService {
	
	/**
	 * Consultar liberacao previa.
	 *
	 * @param entradaDTO the entrada dto
	 * @return the list< consultar liberacao previa saida dt o>
	 */
	List<ConsultarLiberacaoPreviaSaidaDTO> consultarLiberacaoPrevia (ConsultarLiberacaoPreviaEntradaDTO entradaDTO);
	
	/**
	 * Consultar det liberacao previa.
	 *
	 * @param entradaDTO the entrada dto
	 * @return the consultar det liberacao previa saida dto
	 */
	ConsultarDetLiberacaoPreviaSaidaDTO consultarDetLiberacaoPrevia (ConsultarDetLiberacaoPreviaEntradaDTO entradaDTO);
	
	/**
	 * Alterar liberacao previa.
	 *
	 * @param entradaDTO the entrada dto
	 * @return the alterar liberacao previa saida dto
	 */
	AlterarLiberacaoPreviaSaidaDTO alterarLiberacaoPrevia (AlterarLiberacaoPreviaEntradaDTO entradaDTO);
	
	/**
	 * Excluir liberacao previa.
	 *
	 * @param entradaDTO the entrada dto
	 * @return the excluir liberacao previa saida dto
	 */
	ExcluirLiberacaoPreviaSaidaDTO excluirLiberacaoPrevia (ExcluirLiberacaoPreviaEntradaDTO entradaDTO);
	
	/**
	 * Incluir liberacao previa.
	 *
	 * @param entradaDTO the entrada dto
	 * @return the incluir liberacao previa saida dto
	 */
	IncluirLiberacaoPreviaSaidaDTO incluirLiberacaoPrevia (IncluirLiberacaoPreviaEntradaDTO entradaDTO);

}

