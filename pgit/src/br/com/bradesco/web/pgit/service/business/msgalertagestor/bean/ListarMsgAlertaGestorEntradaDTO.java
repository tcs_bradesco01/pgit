/*
 * Nome: br.com.bradesco.web.pgit.service.business.msgalertagestor.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.msgalertagestor.bean;

/**
 * Nome: ListarMsgAlertaGestorEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarMsgAlertaGestorEntradaDTO {
	
	/** Atributo centroCusto. */
	private String centroCusto;
	
	/** Atributo tipoProcesso. */
	private int tipoProcesso;
	
	/** Atributo prefixo. */
	private String prefixo;
	
	/** Atributo tipoMensagem. */
	private int tipoMensagem;
	
	/** Atributo recurso. */
	private int recurso;
	
	/** Atributo idioma. */
	private int idioma;
	
	/** Atributo codPessoa. */
	private String codPessoa;
	
	
	/**
	 * Get: centroCusto.
	 *
	 * @return centroCusto
	 */
	public String getCentroCusto() {
		return centroCusto;
	}
	
	/**
	 * Set: centroCusto.
	 *
	 * @param centroCusto the centro custo
	 */
	public void setCentroCusto(String centroCusto) {
		this.centroCusto = centroCusto;
	}
	
	/**
	 * Get: idioma.
	 *
	 * @return idioma
	 */
	public int getIdioma() {
		return idioma;
	}
	
	/**
	 * Set: idioma.
	 *
	 * @param idioma the idioma
	 */
	public void setIdioma(int idioma) {
		this.idioma = idioma;
	}
	
	/**
	 * Get: prefixo.
	 *
	 * @return prefixo
	 */
	public String getPrefixo() {
		return prefixo;
	}
	
	/**
	 * Set: prefixo.
	 *
	 * @param prefixo the prefixo
	 */
	public void setPrefixo(String prefixo) {
		this.prefixo = prefixo;
	}
	
	/**
	 * Get: recurso.
	 *
	 * @return recurso
	 */
	public int getRecurso() {
		return recurso;
	}
	
	/**
	 * Set: recurso.
	 *
	 * @param recurso the recurso
	 */
	public void setRecurso(int recurso) {
		this.recurso = recurso;
	}
	
	/**
	 * Get: tipoMensagem.
	 *
	 * @return tipoMensagem
	 */
	public int getTipoMensagem() {
		return tipoMensagem;
	}
	
	/**
	 * Set: tipoMensagem.
	 *
	 * @param tipoMensagem the tipo mensagem
	 */
	public void setTipoMensagem(int tipoMensagem) {
		this.tipoMensagem = tipoMensagem;
	}
	
	/**
	 * Get: tipoProcesso.
	 *
	 * @return tipoProcesso
	 */
	public int getTipoProcesso() {
		return tipoProcesso;
	}
	
	/**
	 * Set: tipoProcesso.
	 *
	 * @param tipoProcesso the tipo processo
	 */
	public void setTipoProcesso(int tipoProcesso) {
		this.tipoProcesso = tipoProcesso;
	}
	
	/**
	 * Get: codPessoa.
	 *
	 * @return codPessoa
	 */
	public String getCodPessoa() {
		return codPessoa;
	}
	
	/**
	 * Set: codPessoa.
	 *
	 * @param codPessoa the cod pessoa
	 */
	public void setCodPessoa(String codPessoa) {
		this.codPessoa = codPessoa;
	}
	
	
}
