/*
 * Nome: br.com.bradesco.web.pgit.service.business.combo.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.combo.bean;

/**
 * Nome: ConsultarSituacaoSolicitacaoEstornoEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarSituacaoSolicitacaoEstornoEntradaDTO {
	
	/** Atributo numeroOcorrencias. */
	private Integer numeroOcorrencias;
    
    /** Atributo cdSituacao. */
    private Integer cdSituacao;
    
    /** Atributo cdIndicador. */
    private Integer cdIndicador;
    
	/**
	 * Get: cdSituacao.
	 *
	 * @return cdSituacao
	 */
	public Integer getCdSituacao() {
		return cdSituacao;
	}
	
	/**
	 * Set: cdSituacao.
	 *
	 * @param cdSituacao the cd situacao
	 */
	public void setCdSituacao(Integer cdSituacao) {
		this.cdSituacao = cdSituacao;
	}
	
	/**
	 * Get: numeroOcorrencias.
	 *
	 * @return numeroOcorrencias
	 */
	public Integer getNumeroOcorrencias() {
		return numeroOcorrencias;
	}
	
	/**
	 * Set: numeroOcorrencias.
	 *
	 * @param numeroOcorrencias the numero ocorrencias
	 */
	public void setNumeroOcorrencias(Integer numeroOcorrencias) {
		this.numeroOcorrencias = numeroOcorrencias;
	}
	
	/**
	 * Get: cdIndicador.
	 *
	 * @return cdIndicador
	 */
	public Integer getCdIndicador() {
		return cdIndicador;
	}
	
	/**
	 * Set: cdIndicador.
	 *
	 * @param cdIndicador the cd indicador
	 */
	public void setCdIndicador(Integer cdIndicador) {
		this.cdIndicador = cdIndicador;
	}
}
