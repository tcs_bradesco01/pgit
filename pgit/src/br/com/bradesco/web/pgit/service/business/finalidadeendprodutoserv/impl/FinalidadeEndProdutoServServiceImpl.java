/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/implementation.ftl,v $
 * $Id: implementation.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: implementation.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */
 
package br.com.bradesco.web.pgit.service.business.finalidadeendprodutoserv.impl;

import java.util.ArrayList;
import java.util.List;

import br.com.bradesco.web.pgit.service.business.finalidadeendprodutoserv.IFinalidadeEndProdutoServService;
import br.com.bradesco.web.pgit.service.business.finalidadeendprodutoserv.IFinalidadeEndProdutoServServiceConstants;
import br.com.bradesco.web.pgit.service.business.finalidadeendprodutoserv.bean.DetalharFinalidadeEndOpeEntradaDTO;
import br.com.bradesco.web.pgit.service.business.finalidadeendprodutoserv.bean.DetalharFinalidadeEndOpeSaidaDTO;
import br.com.bradesco.web.pgit.service.business.finalidadeendprodutoserv.bean.ExcluirFinalidadeEndOpeEntradaDTO;
import br.com.bradesco.web.pgit.service.business.finalidadeendprodutoserv.bean.ExcluirFinalidadeEndOpeSaidaDTO;
import br.com.bradesco.web.pgit.service.business.finalidadeendprodutoserv.bean.IncluirFinalidadeEndOpeEntradaDTO;
import br.com.bradesco.web.pgit.service.business.finalidadeendprodutoserv.bean.IncluirFinalidadeEndOpeSaidaDTO;
import br.com.bradesco.web.pgit.service.business.finalidadeendprodutoserv.bean.ListarFinalidadeEndOpeEntradaDTO;
import br.com.bradesco.web.pgit.service.business.finalidadeendprodutoserv.bean.ListarFinalidadeEndOpeSaidaDTO;
import br.com.bradesco.web.pgit.service.data.pdc.FactoryAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.detalharfinalidadeenderecooperacao.request.DetalharFinalidadeEnderecoOperacaoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.detalharfinalidadeenderecooperacao.response.DetalharFinalidadeEnderecoOperacaoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.excluirfinalidadeenderecooperacao.request.ExcluirFinalidadeEnderecoOperacaoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.excluirfinalidadeenderecooperacao.response.ExcluirFinalidadeEnderecoOperacaoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.incluirfinalidadeenderecooperacao.request.IncluirFinalidadeEnderecoOperacaoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.incluirfinalidadeenderecooperacao.response.IncluirFinalidadeEnderecoOperacaoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarfinalidadeenderecooperacao.request.ListarFinalidadeEnderecoOperacaoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarfinalidadeenderecooperacao.response.ListarFinalidadeEnderecoOperacaoResponse;
import br.com.bradesco.web.pgit.utils.PgitUtil;


/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Implementa��o do adaptador: FinalidadeEndProdutoServ
 * </p>
 * 
 * @comment C�DIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public class FinalidadeEndProdutoServServiceImpl implements IFinalidadeEndProdutoServService {

	/** Atributo factoryAdapter. */
	private FactoryAdapter factoryAdapter;

    /**
     * Get: factoryAdapter.
     *
     * @return factoryAdapter
     */
    public FactoryAdapter getFactoryAdapter() {
		return factoryAdapter;
	}

	/**
	 * Set: factoryAdapter.
	 *
	 * @param factoryAdapter the factory adapter
	 */
	public void setFactoryAdapter(FactoryAdapter factoryAdapter) {
		this.factoryAdapter = factoryAdapter;
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.finalidadeendprodutoserv.IFinalidadeEndProdutoServService#listarFinalidadeEnderecoOperacao(br.com.bradesco.web.pgit.service.business.finalidadeendprodutoserv.bean.ListarFinalidadeEndOpeEntradaDTO)
	 */
	public List<ListarFinalidadeEndOpeSaidaDTO> listarFinalidadeEnderecoOperacao(ListarFinalidadeEndOpeEntradaDTO listarFinalidadeEndOpeEntradaDTO) {

		List<ListarFinalidadeEndOpeSaidaDTO> listaRetorno = new ArrayList<ListarFinalidadeEndOpeSaidaDTO>();		
		ListarFinalidadeEnderecoOperacaoRequest request = new ListarFinalidadeEnderecoOperacaoRequest();
		ListarFinalidadeEnderecoOperacaoResponse response = new ListarFinalidadeEnderecoOperacaoResponse();
		
		
		request.setCdProdutoServicoOper(PgitUtil.verificaIntegerNulo(listarFinalidadeEndOpeEntradaDTO.getTipoServico()));
		request.setCdProdutoOperRelacionado(PgitUtil.verificaIntegerNulo(listarFinalidadeEndOpeEntradaDTO.getModalidadeServico()));
		request.setCdRelacionamentoProduto(PgitUtil.verificaIntegerNulo(listarFinalidadeEndOpeEntradaDTO.getTipoRelacionamento()));
		request.setCdFinalidadeEnderecoContrato(PgitUtil.verificaIntegerNulo(listarFinalidadeEndOpeEntradaDTO.getFinalidadeEndereco()));
		request.setCdOperacaoProdutoServico(PgitUtil.verificaIntegerNulo(listarFinalidadeEndOpeEntradaDTO.getCdOperacaoProdutoServico()));
		request.setQtConsultas(IFinalidadeEndProdutoServServiceConstants.QTDE_CONSULTAS_LISTA);
		
		
		response = getFactoryAdapter().getListarFinalidadeEnderecoOperacaoPDCAdapter().invokeProcess(request);

		ListarFinalidadeEndOpeSaidaDTO saidaDTO;
		for (int i=0; i<response.getOcorrenciasCount();i++){
			saidaDTO = new ListarFinalidadeEndOpeSaidaDTO();
			saidaDTO.setCodMensagem(response.getCodMensagem());
			saidaDTO.setCdFinalidadeEndereco(response.getOcorrencias(i).getCdFinalidadeEnderecoContrato());
			saidaDTO.setCdModalidadeServico(response.getOcorrencias(i).getCdProdutoOperRelacionado());
			saidaDTO.setCdTipoServico(response.getOcorrencias(i).getCdProdutoServicoOper());
			saidaDTO.setCdTipoRelacionamento(response.getOcorrencias(i).getCdRelacionamentoProduto());
			saidaDTO.setDsFinalidadeEndereco(response.getOcorrencias(i).getDsFinalidadeEnderecoContrato());
			saidaDTO.setDsModalidadeServico(response.getOcorrencias(i).getDsProdutoOperRelacionado());
			saidaDTO.setDsTipoServico(response.getOcorrencias(i).getDsProdutoServicoOper());	
			saidaDTO.setCdOperacaoProdutoServico(response.getOcorrencias(i).getCdOperacaoProdutoServico());
			saidaDTO.setDsOperacaoProdutoServico(response.getOcorrencias(i).getDsOperacaoProdutoServico());
			listaRetorno.add(saidaDTO);
		}
		
		return listaRetorno;
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.finalidadeendprodutoserv.IFinalidadeEndProdutoServService#detalharFinalidadeEnderecoOperacao(br.com.bradesco.web.pgit.service.business.finalidadeendprodutoserv.bean.DetalharFinalidadeEndOpeEntradaDTO)
	 */
	public DetalharFinalidadeEndOpeSaidaDTO detalharFinalidadeEnderecoOperacao(DetalharFinalidadeEndOpeEntradaDTO detalharFinalidadeEndOpeEntradaDTO) {
		
		DetalharFinalidadeEndOpeSaidaDTO saidaDTO = new DetalharFinalidadeEndOpeSaidaDTO();
		
		DetalharFinalidadeEnderecoOperacaoRequest request = new DetalharFinalidadeEnderecoOperacaoRequest();
		DetalharFinalidadeEnderecoOperacaoResponse response = new DetalharFinalidadeEnderecoOperacaoResponse();
	
		request.setCdProdutoServicoOper(PgitUtil.verificaIntegerNulo(detalharFinalidadeEndOpeEntradaDTO.getTipoServico()));
		request.setCdProdutoOperRelacionado(PgitUtil.verificaIntegerNulo(detalharFinalidadeEndOpeEntradaDTO.getModalidadeServico()));
		request.setCdRelacionamentoProduto(PgitUtil.verificaIntegerNulo(detalharFinalidadeEndOpeEntradaDTO.getTipoRelacionamento()));
		request.setCdFinalidadeEnderecoContrato(PgitUtil.verificaIntegerNulo(detalharFinalidadeEndOpeEntradaDTO.getFinalidadeEndereco()));
		request.setCdOperacaoProdutoServico(PgitUtil.verificaIntegerNulo(detalharFinalidadeEndOpeEntradaDTO.getCdOperacaoProdutoServico()));
				
		response = getFactoryAdapter().getDetalharFinalidadeEnderecoOperacaoPDCAdapter().invokeProcess(request);
	
		saidaDTO.setCodMensagem(response.getCodMensagem());
		saidaDTO.setMensagem(response.getMensagem());
		saidaDTO.setCdTipoServico(response.getCdProdutoServicoOper());
		saidaDTO.setDsTipoServico(response.getDsProdutoServicoOper());
		saidaDTO.setCdModalidadeServico(response.getCdProdutoOperRelacionado());
		saidaDTO.setDsModalidadeServico(response.getDsProdutoOperRelacionado());
		saidaDTO.setCdFinalidadeEndereco(response.getCdFinalidadeEnderecoContrato());
		saidaDTO.setDsFinalidadeEndereco(response.getDsFinalidadeEnderecoContrato());
		saidaDTO.setCdOperacaoProdutoServico(response.getCdOperacaoProdutoServico());
		saidaDTO.setDsOperacaoProdutoServico(response.getDsOperacaoProdutoServico());
		saidaDTO.setCdCanalInclusao(response.getCdCanalInclusao());
		saidaDTO.setDsCanalInclusao(response.getDsCanalInclusao()); 
		saidaDTO.setUsuarioInclusao(response.getCdAutenSegrcInclusao());
		saidaDTO.setComplementoInclusao(response.getNmOperFluxoInclusao());
		saidaDTO.setDataHoraInclusao(response.getHrInclusaoRegistro());
		saidaDTO.setCdCanalManutencao(response.getCdCanalManutencao());
		saidaDTO.setDsCanalManutencao(response.getDsCanalManutencao()); 
		saidaDTO.setUsuarioManutencao(response.getCdAutenSegrcManutencao());
		saidaDTO.setComplementoManutencao(response.getNmOperFluxoManutencao());
		saidaDTO.setDataHoraManutencao(response.getHrManutencaoRegistro());
		saidaDTO.setDsOperacaoProdutoServico(response.getDsOperacaoProdutoServico());
		
		return saidaDTO;
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.finalidadeendprodutoserv.IFinalidadeEndProdutoServService#incluirFinalidadeEnderecoOperacao(br.com.bradesco.web.pgit.service.business.finalidadeendprodutoserv.bean.IncluirFinalidadeEndOpeEntradaDTO)
	 */
	public IncluirFinalidadeEndOpeSaidaDTO incluirFinalidadeEnderecoOperacao(IncluirFinalidadeEndOpeEntradaDTO incluirFinalidadeEndOpeEntradaDTO) {
		IncluirFinalidadeEndOpeSaidaDTO incluirFinalidadeEndOpeSaidaDTO = new IncluirFinalidadeEndOpeSaidaDTO();
		IncluirFinalidadeEnderecoOperacaoRequest  incluirFinalidadeEnderecoOperacaoRequest = new IncluirFinalidadeEnderecoOperacaoRequest();
		IncluirFinalidadeEnderecoOperacaoResponse incluirFinalidadeEnderecoOperacaoResponse = new IncluirFinalidadeEnderecoOperacaoResponse();

		incluirFinalidadeEnderecoOperacaoRequest.setCdProdutoServicoOper(PgitUtil.verificaIntegerNulo(incluirFinalidadeEndOpeEntradaDTO.getTipoServico()));
		incluirFinalidadeEnderecoOperacaoRequest.setCdProdutoOperRelacionado(PgitUtil.verificaIntegerNulo(incluirFinalidadeEndOpeEntradaDTO.getModalidadeServico()));
		incluirFinalidadeEnderecoOperacaoRequest.setCdRelacionamentoProduto(PgitUtil.verificaIntegerNulo(incluirFinalidadeEndOpeEntradaDTO.getTipoRelacionamento()));
		incluirFinalidadeEnderecoOperacaoRequest.setCdFinalidadeEnderecoContrato(PgitUtil.verificaIntegerNulo(incluirFinalidadeEndOpeEntradaDTO.getFinalidadeEndereco()));		 
		incluirFinalidadeEnderecoOperacaoRequest.setCdOperacaoProdutoServico(PgitUtil.verificaIntegerNulo(incluirFinalidadeEndOpeEntradaDTO.getCdOperacaoProdutoServico()));
		
		incluirFinalidadeEnderecoOperacaoResponse = getFactoryAdapter().getIncluirFinalidadeEnderecoOperacaoPDCAdapter().invokeProcess(incluirFinalidadeEnderecoOperacaoRequest);
		
		incluirFinalidadeEndOpeSaidaDTO.setCodMensagem(incluirFinalidadeEnderecoOperacaoResponse.getCodMensagem());
		incluirFinalidadeEndOpeSaidaDTO.setMensagem(incluirFinalidadeEnderecoOperacaoResponse.getMensagem());
	
		return incluirFinalidadeEndOpeSaidaDTO;
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.finalidadeendprodutoserv.IFinalidadeEndProdutoServService#excluirFinalidadeEnderecoOperacao(br.com.bradesco.web.pgit.service.business.finalidadeendprodutoserv.bean.ExcluirFinalidadeEndOpeEntradaDTO)
	 */
	public ExcluirFinalidadeEndOpeSaidaDTO excluirFinalidadeEnderecoOperacao(ExcluirFinalidadeEndOpeEntradaDTO excluirFinalidadeEndOpeEntradaDTO) {
		
		ExcluirFinalidadeEndOpeSaidaDTO excluirFinalidadeEndOpeSaidaDTO = new ExcluirFinalidadeEndOpeSaidaDTO();
		ExcluirFinalidadeEnderecoOperacaoRequest  excluirFinalidadeEnderecoOperacaoRequest = new ExcluirFinalidadeEnderecoOperacaoRequest();
		ExcluirFinalidadeEnderecoOperacaoResponse excluirFinalidadeEnderecoOperacaoResponse = new ExcluirFinalidadeEnderecoOperacaoResponse();

		excluirFinalidadeEnderecoOperacaoRequest.setCdProdutoServicoOper(PgitUtil.verificaIntegerNulo(excluirFinalidadeEndOpeEntradaDTO.getTipoServico()));
		excluirFinalidadeEnderecoOperacaoRequest.setCdProdutoOperRelacionado(PgitUtil.verificaIntegerNulo(excluirFinalidadeEndOpeEntradaDTO.getModalidadeServico()));
		excluirFinalidadeEnderecoOperacaoRequest.setCdRelacionamentoProduto(PgitUtil.verificaIntegerNulo(excluirFinalidadeEndOpeEntradaDTO.getTipoRelacionamento()));
		excluirFinalidadeEnderecoOperacaoRequest.setCdFinalidadeEnderecoContrato(PgitUtil.verificaIntegerNulo(excluirFinalidadeEndOpeEntradaDTO.getFinalidadeEndereco()));		 
		excluirFinalidadeEnderecoOperacaoRequest.setCdOperacaoProdutoServico(PgitUtil.verificaIntegerNulo(excluirFinalidadeEndOpeEntradaDTO.getCdOperacaoProdutoServico()));
		
		excluirFinalidadeEnderecoOperacaoResponse = getFactoryAdapter().getExcluirFinalidadeEnderecoOperacaoPDCAdapter().invokeProcess(excluirFinalidadeEnderecoOperacaoRequest);
		
		excluirFinalidadeEndOpeSaidaDTO.setCodMensagem(excluirFinalidadeEnderecoOperacaoResponse.getCodMensagem());
		excluirFinalidadeEndOpeSaidaDTO.setMensagem(excluirFinalidadeEnderecoOperacaoResponse.getMensagem());
	
		return excluirFinalidadeEndOpeSaidaDTO;
	}
    
}

