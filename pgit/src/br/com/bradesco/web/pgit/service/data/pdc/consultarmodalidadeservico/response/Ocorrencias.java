/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultarmodalidadeservico.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdModalidadePagamentoCliente
     */
    private int _cdModalidadePagamentoCliente = 0;

    /**
     * keeps track of state for field: _cdModalidadePagamentoCliente
     */
    private boolean _has_cdModalidadePagamentoCliente;

    /**
     * Field _dsModalidadePagamentoCliente
     */
    private java.lang.String _dsModalidadePagamentoCliente;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarmodalidadeservico.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdModalidadePagamentoCliente
     * 
     */
    public void deleteCdModalidadePagamentoCliente()
    {
        this._has_cdModalidadePagamentoCliente= false;
    } //-- void deleteCdModalidadePagamentoCliente() 

    /**
     * Returns the value of field 'cdModalidadePagamentoCliente'.
     * 
     * @return int
     * @return the value of field 'cdModalidadePagamentoCliente'.
     */
    public int getCdModalidadePagamentoCliente()
    {
        return this._cdModalidadePagamentoCliente;
    } //-- int getCdModalidadePagamentoCliente() 

    /**
     * Returns the value of field 'dsModalidadePagamentoCliente'.
     * 
     * @return String
     * @return the value of field 'dsModalidadePagamentoCliente'.
     */
    public java.lang.String getDsModalidadePagamentoCliente()
    {
        return this._dsModalidadePagamentoCliente;
    } //-- java.lang.String getDsModalidadePagamentoCliente() 

    /**
     * Method hasCdModalidadePagamentoCliente
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdModalidadePagamentoCliente()
    {
        return this._has_cdModalidadePagamentoCliente;
    } //-- boolean hasCdModalidadePagamentoCliente() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdModalidadePagamentoCliente'.
     * 
     * @param cdModalidadePagamentoCliente the value of field
     * 'cdModalidadePagamentoCliente'.
     */
    public void setCdModalidadePagamentoCliente(int cdModalidadePagamentoCliente)
    {
        this._cdModalidadePagamentoCliente = cdModalidadePagamentoCliente;
        this._has_cdModalidadePagamentoCliente = true;
    } //-- void setCdModalidadePagamentoCliente(int) 

    /**
     * Sets the value of field 'dsModalidadePagamentoCliente'.
     * 
     * @param dsModalidadePagamentoCliente the value of field
     * 'dsModalidadePagamentoCliente'.
     */
    public void setDsModalidadePagamentoCliente(java.lang.String dsModalidadePagamentoCliente)
    {
        this._dsModalidadePagamentoCliente = dsModalidadePagamentoCliente;
    } //-- void setDsModalidadePagamentoCliente(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultarmodalidadeservico.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultarmodalidadeservico.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultarmodalidadeservico.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarmodalidadeservico.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
