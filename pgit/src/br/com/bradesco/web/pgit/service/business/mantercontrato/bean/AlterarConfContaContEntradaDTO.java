/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantercontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantercontrato.bean;

import java.util.ArrayList;
import java.util.List;

/**
 * Nome: AlterarConfContaContEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class AlterarConfContaContEntradaDTO {
	
	/** Atributo cdPessoaJuridicaContrato. */
	private Long cdPessoaJuridicaContrato;
	
	/** Atributo cdTipoContratoNegocio. */
	private int cdTipoContratoNegocio;
	
	/** Atributo nrSequenciaContratoNegocio. */
	private Long nrSequenciaContratoNegocio;
	
	/** Atributo cdPessoaJuridicaVinculo. */
	private Long cdPessoaJuridicaVinculo;
	
	/** Atributo cdTipoContratoNegocioVinculo. */
	private int cdTipoContratoNegocioVinculo;
	
	/** Atributo nrSequenciaContratoVinculo. */
	private Long nrSequenciaContratoVinculo;
	
	/** Atributo cdTipoVinculoContrato. */
	private int cdTipoVinculoContrato;
	
	/** Atributo cdSituacaoVinculacaoConta. */
	private int cdSituacaoVinculacaoConta;
	
	/** Atributo cdMotivoSituacaoConta. */
	private int cdMotivoSituacaoConta;
	
	/** Atributo cdMunicipio. */
	private Long cdMunicipio;
	
	/** Atributo cdMunicipioFeriadoLocal. */
	private int cdMunicipioFeriadoLocal;
	
	private String descApelido;
	
	/** Atributo listaFinalidade. */
	private List<FinalidadeAlterarContaEntradaDTO> listaFinalidade = new ArrayList<FinalidadeAlterarContaEntradaDTO>();	
	
	/**
	 * Get: cdMotivoSituacaoConta.
	 *
	 * @return cdMotivoSituacaoConta
	 */
	public int getCdMotivoSituacaoConta() {
		return cdMotivoSituacaoConta;
	}
	
	/**
	 * Set: cdMotivoSituacaoConta.
	 *
	 * @param cdMotivoSituacaoConta the cd motivo situacao conta
	 */
	public void setCdMotivoSituacaoConta(int cdMotivoSituacaoConta) {
		this.cdMotivoSituacaoConta = cdMotivoSituacaoConta;
	}
	
	/**
	 * Get: cdMunicipio.
	 *
	 * @return cdMunicipio
	 */
	public Long getCdMunicipio() {
		return cdMunicipio;
	}
	
	/**
	 * Set: cdMunicipio.
	 *
	 * @param cdMunicipio the cd municipio
	 */
	public void setCdMunicipio(Long cdMunicipio) {
		this.cdMunicipio = cdMunicipio;
	}
	
	/**
	 * Get: cdMunicipioFeriadoLocal.
	 *
	 * @return cdMunicipioFeriadoLocal
	 */
	public int getCdMunicipioFeriadoLocal() {
		return cdMunicipioFeriadoLocal;
	}
	
	/**
	 * Set: cdMunicipioFeriadoLocal.
	 *
	 * @param cdMunicipioFeriadoLocal the cd municipio feriado local
	 */
	public void setCdMunicipioFeriadoLocal(int cdMunicipioFeriadoLocal) {
		this.cdMunicipioFeriadoLocal = cdMunicipioFeriadoLocal;
	}
	
	/**
	 * Get: cdPessoaJuridicaContrato.
	 *
	 * @return cdPessoaJuridicaContrato
	 */
	public Long getCdPessoaJuridicaContrato() {
		return cdPessoaJuridicaContrato;
	}
	
	/**
	 * Set: cdPessoaJuridicaContrato.
	 *
	 * @param cdPessoaJuridicaContrato the cd pessoa juridica contrato
	 */
	public void setCdPessoaJuridicaContrato(Long cdPessoaJuridicaContrato) {
		this.cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
	}
	
	/**
	 * Get: cdPessoaJuridicaVinculo.
	 *
	 * @return cdPessoaJuridicaVinculo
	 */
	public Long getCdPessoaJuridicaVinculo() {
		return cdPessoaJuridicaVinculo;
	}
	
	/**
	 * Set: cdPessoaJuridicaVinculo.
	 *
	 * @param cdPessoaJuridicaVinculo the cd pessoa juridica vinculo
	 */
	public void setCdPessoaJuridicaVinculo(Long cdPessoaJuridicaVinculo) {
		this.cdPessoaJuridicaVinculo = cdPessoaJuridicaVinculo;
	}
	
	/**
	 * Get: cdSituacaoVinculacaoConta.
	 *
	 * @return cdSituacaoVinculacaoConta
	 */
	public int getCdSituacaoVinculacaoConta() {
		return cdSituacaoVinculacaoConta;
	}
	
	/**
	 * Set: cdSituacaoVinculacaoConta.
	 *
	 * @param cdSituacaoVinculacaoConta the cd situacao vinculacao conta
	 */
	public void setCdSituacaoVinculacaoConta(int cdSituacaoVinculacaoConta) {
		this.cdSituacaoVinculacaoConta = cdSituacaoVinculacaoConta;
	}
	
	/**
	 * Get: cdTipoContratoNegocio.
	 *
	 * @return cdTipoContratoNegocio
	 */
	public int getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}
	
	/**
	 * Set: cdTipoContratoNegocio.
	 *
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(int cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}
	
	/**
	 * Get: cdTipoContratoNegocioVinculo.
	 *
	 * @return cdTipoContratoNegocioVinculo
	 */
	public int getCdTipoContratoNegocioVinculo() {
		return cdTipoContratoNegocioVinculo;
	}
	
	/**
	 * Set: cdTipoContratoNegocioVinculo.
	 *
	 * @param cdTipoContratoNegocioVinculo the cd tipo contrato negocio vinculo
	 */
	public void setCdTipoContratoNegocioVinculo(int cdTipoContratoNegocioVinculo) {
		this.cdTipoContratoNegocioVinculo = cdTipoContratoNegocioVinculo;
	}
	
	/**
	 * Get: cdTipoVinculoContrato.
	 *
	 * @return cdTipoVinculoContrato
	 */
	public int getCdTipoVinculoContrato() {
		return cdTipoVinculoContrato;
	}
	
	/**
	 * Set: cdTipoVinculoContrato.
	 *
	 * @param cdTipoVinculoContrato the cd tipo vinculo contrato
	 */
	public void setCdTipoVinculoContrato(int cdTipoVinculoContrato) {
		this.cdTipoVinculoContrato = cdTipoVinculoContrato;
	}
	
	/**
	 * Get: nrSequenciaContratoNegocio.
	 *
	 * @return nrSequenciaContratoNegocio
	 */
	public Long getNrSequenciaContratoNegocio() {
		return nrSequenciaContratoNegocio;
	}
	
	/**
	 * Set: nrSequenciaContratoNegocio.
	 *
	 * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
	 */
	public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}
	
	/**
	 * Get: nrSequenciaContratoVinculo.
	 *
	 * @return nrSequenciaContratoVinculo
	 */
	public Long getNrSequenciaContratoVinculo() {
		return nrSequenciaContratoVinculo;
	}
	
	/**
	 * Set: nrSequenciaContratoVinculo.
	 *
	 * @param nrSequenciaContratoVinculo the nr sequencia contrato vinculo
	 */
	public void setNrSequenciaContratoVinculo(Long nrSequenciaContratoVinculo) {
		this.nrSequenciaContratoVinculo = nrSequenciaContratoVinculo;
	}
	
	/**
	 * Get: listaFinalidade.
	 *
	 * @return listaFinalidade
	 */
	public List<FinalidadeAlterarContaEntradaDTO> getListaFinalidade() {
		return listaFinalidade;
	}
	
	/**
	 * Set: listaFinalidade.
	 *
	 * @param listaFinalidade the lista finalidade
	 */
	public void setListaFinalidade(
			List<FinalidadeAlterarContaEntradaDTO> listaFinalidade) {
		this.listaFinalidade = listaFinalidade;
	}

	/**
	 * Get: descApelido
	 * @return the descApelido
	 */
	public String getDescApelido() {
		return descApelido;
	}

	/**
	 * Set: descApelido
	 * @param descApelido the descApelido to set
	 */
	public void setDescApelido(String descApelido) {
		this.descApelido = descApelido;
	}
	
}
