/*
 * Nome: br.com.bradesco.web.pgit.service.business.combo.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.combo.bean;

/**
 * Nome: ListarSituacaoSolicitacaoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarSituacaoSolicitacaoSaidaDTO {

	/** Atributo cdSolicitacaoPagamentoIntegrado. */
	private Integer cdSolicitacaoPagamentoIntegrado;
    
    /** Atributo dsTipoSolicitacaoPagamento. */
    private String dsTipoSolicitacaoPagamento;
	
   
	/**
	 * Listar situacao solicitacao saida dto.
	 *
	 * @param cdSolicitacaoPagamentoIntegrado the cd solicitacao pagamento integrado
	 * @param dsTipoSolicitacaoPagamento the ds tipo solicitacao pagamento
	 */
	public ListarSituacaoSolicitacaoSaidaDTO(Integer cdSolicitacaoPagamentoIntegrado, String dsTipoSolicitacaoPagamento) {
		super();
		this.cdSolicitacaoPagamentoIntegrado = cdSolicitacaoPagamentoIntegrado;
		this.dsTipoSolicitacaoPagamento = dsTipoSolicitacaoPagamento;
	}
	
	/**
	 * Get: cdSolicitacaoPagamentoIntegrado.
	 *
	 * @return cdSolicitacaoPagamentoIntegrado
	 */
	public Integer getCdSolicitacaoPagamentoIntegrado() {
		return cdSolicitacaoPagamentoIntegrado;
	}
	
	/**
	 * Set: cdSolicitacaoPagamentoIntegrado.
	 *
	 * @param cdSolicitacaoPagamentoIntegrado the cd solicitacao pagamento integrado
	 */
	public void setCdSolicitacaoPagamentoIntegrado(Integer cdSolicitacaoPagamentoIntegrado) {
		this.cdSolicitacaoPagamentoIntegrado = cdSolicitacaoPagamentoIntegrado;
	}
	
	/**
	 * Get: dsTipoSolicitacaoPagamento.
	 *
	 * @return dsTipoSolicitacaoPagamento
	 */
	public String getDsTipoSolicitacaoPagamento() {
		return dsTipoSolicitacaoPagamento;
	}
	
	/**
	 * Set: dsTipoSolicitacaoPagamento.
	 *
	 * @param dsTipoSolicitacaoPagamento the ds tipo solicitacao pagamento
	 */
	public void setDsTipoSolicitacaoPagamento(String dsTipoSolicitacaoPagamento) {
		this.dsTipoSolicitacaoPagamento = dsTipoSolicitacaoPagamento;
	}
    
    
}
