/*
 * Nome: br.com.bradesco.web.pgit.service.business.estornoordenspagamentos.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.estornoordenspagamentos.bean;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Nome: EstornoOrdensPagamentosEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class EstornoOrdensPagamentosEntradaDTO {
	
	/** Atributo cdTipoPesquisa. */
	private Integer cdTipoPesquisa;
    
    /** Atributo nrOcorrencias. */
    private Integer nrOcorrencias;
    
    /** Atributo cdpessoaJuridicaContrato. */
    private Long cdpessoaJuridicaContrato;
    
    /** Atributo cdTipoContratoNegocio. */
    private Integer cdTipoContratoNegocio;
    
    /** Atributo nrSequenciaContratoNegocio. */
    private Long nrSequenciaContratoNegocio;
    
    /** Atributo cdBanco. */
    private Integer cdBanco;
    
    /** Atributo cdAgencia. */
    private Integer cdAgencia;
    
    /** Atributo dgAgenciaBancaria. */
    private Integer dgAgenciaBancaria;
    
    /** Atributo cdConta. */
    private Long cdConta;
    
    /** Atributo cdDigitoConta. */
    private String cdDigitoConta;
    
    /** Atributo nrArquivoRemssaPagamento. */
    private Long nrArquivoRemssaPagamento;
    
    /** Atributo cdParticipante. */
    private Long cdParticipante;
    
    /** Atributo cdProdutoServicoOperacao. */
    private Integer cdProdutoServicoOperacao;
    
    /** Atributo cdProdutoServicoRelacionado. */
    private Integer cdProdutoServicoRelacionado;
    
    /** Atributo cdControlePagamentoDe. */
    private String cdControlePagamentoDe;
    
    /** Atributo vlPagamentoDe. */
    private BigDecimal vlPagamentoDe;
    
    /** Atributo vlPagamentoAte. */
    private BigDecimal vlPagamentoAte;
    
    /** Atributo cdFavorecidoClientePagador. */
    private Long cdFavorecidoClientePagador;
    
    /** Atributo cdInscricaoFavorecido. */
    private Long cdInscricaoFavorecido;
    
    /** Atributo cdIdentificacaoInscricaoFavorecido. */
    private Integer cdIdentificacaoInscricaoFavorecido;
    
    /** Atributo dtEstornoInicio. */
    private Date dtEstornoInicio;
    
    /** Atributo dtEstornoFim. */
    private Date dtEstornoFim;
    
	/**
	 * Get: cdTipoPesquisa.
	 *
	 * @return cdTipoPesquisa
	 */
	public Integer getCdTipoPesquisa() {
		return cdTipoPesquisa;
	}
	
	/**
	 * Set: cdTipoPesquisa.
	 *
	 * @param cdTipoPesquisa the cd tipo pesquisa
	 */
	public void setCdTipoPesquisa(Integer cdTipoPesquisa) {
		this.cdTipoPesquisa = cdTipoPesquisa;
	}
	
	/**
	 * Get: nrOcorrencias.
	 *
	 * @return nrOcorrencias
	 */
	public Integer getNrOcorrencias() {
		return nrOcorrencias;
	}
	
	/**
	 * Set: nrOcorrencias.
	 *
	 * @param nrOcorrencias the nr ocorrencias
	 */
	public void setNrOcorrencias(Integer nrOcorrencias) {
		this.nrOcorrencias = nrOcorrencias;
	}
	
	/**
	 * Get: cdpessoaJuridicaContrato.
	 *
	 * @return cdpessoaJuridicaContrato
	 */
	public Long getCdpessoaJuridicaContrato() {
		return cdpessoaJuridicaContrato;
	}
	
	/**
	 * Set: cdpessoaJuridicaContrato.
	 *
	 * @param cdpessoaJuridicaContrato the cdpessoa juridica contrato
	 */
	public void setCdpessoaJuridicaContrato(Long cdpessoaJuridicaContrato) {
		this.cdpessoaJuridicaContrato = cdpessoaJuridicaContrato;
	}
	
	/**
	 * Get: cdTipoContratoNegocio.
	 *
	 * @return cdTipoContratoNegocio
	 */
	public Integer getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}
	
	/**
	 * Set: cdTipoContratoNegocio.
	 *
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}
	
	/**
	 * Get: nrSequenciaContratoNegocio.
	 *
	 * @return nrSequenciaContratoNegocio
	 */
	public Long getNrSequenciaContratoNegocio() {
		return nrSequenciaContratoNegocio;
	}
	
	/**
	 * Set: nrSequenciaContratoNegocio.
	 *
	 * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
	 */
	public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}
	
	/**
	 * Get: cdBanco.
	 *
	 * @return cdBanco
	 */
	public Integer getCdBanco() {
		return cdBanco;
	}
	
	/**
	 * Set: cdBanco.
	 *
	 * @param cdBanco the cd banco
	 */
	public void setCdBanco(Integer cdBanco) {
		this.cdBanco = cdBanco;
	}
	
	/**
	 * Get: cdAgencia.
	 *
	 * @return cdAgencia
	 */
	public Integer getCdAgencia() {
		return cdAgencia;
	}
	
	/**
	 * Set: cdAgencia.
	 *
	 * @param cdAgencia the cd agencia
	 */
	public void setCdAgencia(Integer cdAgencia) {
		this.cdAgencia = cdAgencia;
	}
	
	/**
	 * Get: dgAgenciaBancaria.
	 *
	 * @return dgAgenciaBancaria
	 */
	public Integer getDgAgenciaBancaria() {
		return dgAgenciaBancaria;
	}
	
	/**
	 * Set: dgAgenciaBancaria.
	 *
	 * @param dgAgenciaBancaria the dg agencia bancaria
	 */
	public void setDgAgenciaBancaria(Integer dgAgenciaBancaria) {
		this.dgAgenciaBancaria = dgAgenciaBancaria;
	}
	
	/**
	 * Get: cdConta.
	 *
	 * @return cdConta
	 */
	public Long getCdConta() {
		return cdConta;
	}
	
	/**
	 * Set: cdConta.
	 *
	 * @param cdConta the cd conta
	 */
	public void setCdConta(Long cdConta) {
		this.cdConta = cdConta;
	}
	
	/**
	 * Get: cdDigitoConta.
	 *
	 * @return cdDigitoConta
	 */
	public String getCdDigitoConta() {
		return cdDigitoConta;
	}
	
	/**
	 * Set: cdDigitoConta.
	 *
	 * @param cdDigitoConta the cd digito conta
	 */
	public void setCdDigitoConta(String cdDigitoConta) {
		this.cdDigitoConta = cdDigitoConta;
	}
	
	/**
	 * Get: nrArquivoRemssaPagamento.
	 *
	 * @return nrArquivoRemssaPagamento
	 */
	public Long getNrArquivoRemssaPagamento() {
		return nrArquivoRemssaPagamento;
	}
	
	/**
	 * Set: nrArquivoRemssaPagamento.
	 *
	 * @param nrArquivoRemssaPagamento the nr arquivo remssa pagamento
	 */
	public void setNrArquivoRemssaPagamento(Long nrArquivoRemssaPagamento) {
		this.nrArquivoRemssaPagamento = nrArquivoRemssaPagamento;
	}
	
	/**
	 * Get: cdParticipante.
	 *
	 * @return cdParticipante
	 */
	public Long getCdParticipante() {
		return cdParticipante;
	}
	
	/**
	 * Set: cdParticipante.
	 *
	 * @param cdParticipante the cd participante
	 */
	public void setCdParticipante(Long cdParticipante) {
		this.cdParticipante = cdParticipante;
	}
	
	/**
	 * Get: cdProdutoServicoOperacao.
	 *
	 * @return cdProdutoServicoOperacao
	 */
	public Integer getCdProdutoServicoOperacao() {
		return cdProdutoServicoOperacao;
	}
	
	/**
	 * Set: cdProdutoServicoOperacao.
	 *
	 * @param cdProdutoServicoOperacao the cd produto servico operacao
	 */
	public void setCdProdutoServicoOperacao(Integer cdProdutoServicoOperacao) {
		this.cdProdutoServicoOperacao = cdProdutoServicoOperacao;
	}
	
	/**
	 * Get: cdProdutoServicoRelacionado.
	 *
	 * @return cdProdutoServicoRelacionado
	 */
	public Integer getCdProdutoServicoRelacionado() {
		return cdProdutoServicoRelacionado;
	}
	
	/**
	 * Set: cdProdutoServicoRelacionado.
	 *
	 * @param cdProdutoServicoRelacionado the cd produto servico relacionado
	 */
	public void setCdProdutoServicoRelacionado(Integer cdProdutoServicoRelacionado) {
		this.cdProdutoServicoRelacionado = cdProdutoServicoRelacionado;
	}
	
	/**
	 * Get: cdControlePagamentoDe.
	 *
	 * @return cdControlePagamentoDe
	 */
	public String getCdControlePagamentoDe() {
		return cdControlePagamentoDe;
	}
	
	/**
	 * Set: cdControlePagamentoDe.
	 *
	 * @param cdControlePagamentoDe the cd controle pagamento de
	 */
	public void setCdControlePagamentoDe(String cdControlePagamentoDe) {
		this.cdControlePagamentoDe = cdControlePagamentoDe;
	}
	
	/**
	 * Get: vlPagamentoDe.
	 *
	 * @return vlPagamentoDe
	 */
	public BigDecimal getVlPagamentoDe() {
		return vlPagamentoDe;
	}
	
	/**
	 * Set: vlPagamentoDe.
	 *
	 * @param vlPagamentoDe the vl pagamento de
	 */
	public void setVlPagamentoDe(BigDecimal vlPagamentoDe) {
		this.vlPagamentoDe = vlPagamentoDe;
	}
	
	/**
	 * Get: vlPagamentoAte.
	 *
	 * @return vlPagamentoAte
	 */
	public BigDecimal getVlPagamentoAte() {
		return vlPagamentoAte;
	}
	
	/**
	 * Set: vlPagamentoAte.
	 *
	 * @param vlPagamentoAte the vl pagamento ate
	 */
	public void setVlPagamentoAte(BigDecimal vlPagamentoAte) {
		this.vlPagamentoAte = vlPagamentoAte;
	}
	
	/**
	 * Get: cdFavorecidoClientePagador.
	 *
	 * @return cdFavorecidoClientePagador
	 */
	public Long getCdFavorecidoClientePagador() {
		return cdFavorecidoClientePagador;
	}
	
	/**
	 * Set: cdFavorecidoClientePagador.
	 *
	 * @param cdFavorecidoClientePagador the cd favorecido cliente pagador
	 */
	public void setCdFavorecidoClientePagador(Long cdFavorecidoClientePagador) {
		this.cdFavorecidoClientePagador = cdFavorecidoClientePagador;
	}
	
	/**
	 * Get: cdInscricaoFavorecido.
	 *
	 * @return cdInscricaoFavorecido
	 */
	public Long getCdInscricaoFavorecido() {
		return cdInscricaoFavorecido;
	}
	
	/**
	 * Set: cdInscricaoFavorecido.
	 *
	 * @param cdInscricaoFavorecido the cd inscricao favorecido
	 */
	public void setCdInscricaoFavorecido(Long cdInscricaoFavorecido) {
		this.cdInscricaoFavorecido = cdInscricaoFavorecido;
	}
	
	/**
	 * Get: cdIdentificacaoInscricaoFavorecido.
	 *
	 * @return cdIdentificacaoInscricaoFavorecido
	 */
	public Integer getCdIdentificacaoInscricaoFavorecido() {
		return cdIdentificacaoInscricaoFavorecido;
	}
	
	/**
	 * Set: cdIdentificacaoInscricaoFavorecido.
	 *
	 * @param cdIdentificacaoInscricaoFavorecido the cd identificacao inscricao favorecido
	 */
	public void setCdIdentificacaoInscricaoFavorecido(Integer cdIdentificacaoInscricaoFavorecido) {
		this.cdIdentificacaoInscricaoFavorecido = cdIdentificacaoInscricaoFavorecido;
	}
	
	/**
	 * Get: dtEstornoInicio.
	 *
	 * @return dtEstornoInicio
	 */
	public Date getDtEstornoInicio() {
		return dtEstornoInicio;
	}
	
	/**
	 * Set: dtEstornoInicio.
	 *
	 * @param dtEstornoInicio the dt estorno inicio
	 */
	public void setDtEstornoInicio(Date dtEstornoInicio) {
		this.dtEstornoInicio = dtEstornoInicio;
	}
	
	/**
	 * Get: dtEstornoFim.
	 *
	 * @return dtEstornoFim
	 */
	public Date getDtEstornoFim() {
		return dtEstornoFim;
	}
	
	/**
	 * Set: dtEstornoFim.
	 *
	 * @param dtEstornoFim the dt estorno fim
	 */
	public void setDtEstornoFim(Date dtEstornoFim) {
		this.dtEstornoFim = dtEstornoFim;
	}
}