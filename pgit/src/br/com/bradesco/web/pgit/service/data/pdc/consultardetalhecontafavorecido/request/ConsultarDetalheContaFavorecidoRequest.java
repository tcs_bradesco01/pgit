/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultardetalhecontafavorecido.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;

/**
 * Class ConsultarDetalheContaFavorecidoRequest.
 * 
 * @version $Revision$ $Date$
 */
public class ConsultarDetalheContaFavorecidoRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdPessoaJuridicaContrato
     */
    private long _cdPessoaJuridicaContrato = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridicaContrato
     */
    private boolean _has_cdPessoaJuridicaContrato;

    /**
     * Field _cdTipoContratoNegocio
     */
    private int _cdTipoContratoNegocio = 0;

    /**
     * keeps track of state for field: _cdTipoContratoNegocio
     */
    private boolean _has_cdTipoContratoNegocio;

    /**
     * Field _nrSequenciaContratoNegocio
     */
    private long _nrSequenciaContratoNegocio = 0;

    /**
     * keeps track of state for field: _nrSequenciaContratoNegocio
     */
    private boolean _has_nrSequenciaContratoNegocio;

    /**
     * Field _cdFavorecidoClientePagador
     */
    private long _cdFavorecidoClientePagador = 0;

    /**
     * keeps track of state for field: _cdFavorecidoClientePagador
     */
    private boolean _has_cdFavorecidoClientePagador;

    /**
     * Field _nrOcorrenciaContaFavorecido
     */
    private int _nrOcorrenciaContaFavorecido = 0;

    /**
     * keeps track of state for field: _nrOcorrenciaContaFavorecido
     */
    private boolean _has_nrOcorrenciaContaFavorecido;

    /**
     * Field _cdTipoContaFavorecido
     */
    private int _cdTipoContaFavorecido = 0;

    /**
     * keeps track of state for field: _cdTipoContaFavorecido
     */
    private boolean _has_cdTipoContaFavorecido;


      //----------------/
     //- Constructors -/
    //----------------/

    public ConsultarDetalheContaFavorecidoRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultardetalhecontafavorecido.request.ConsultarDetalheContaFavorecidoRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdFavorecidoClientePagador
     * 
     */
    public void deleteCdFavorecidoClientePagador()
    {
        this._has_cdFavorecidoClientePagador= false;
    } //-- void deleteCdFavorecidoClientePagador() 

    /**
     * Method deleteCdPessoaJuridicaContrato
     * 
     */
    public void deleteCdPessoaJuridicaContrato()
    {
        this._has_cdPessoaJuridicaContrato= false;
    } //-- void deleteCdPessoaJuridicaContrato() 

    /**
     * Method deleteCdTipoContaFavorecido
     * 
     */
    public void deleteCdTipoContaFavorecido()
    {
        this._has_cdTipoContaFavorecido= false;
    } //-- void deleteCdTipoContaFavorecido() 

    /**
     * Method deleteCdTipoContratoNegocio
     * 
     */
    public void deleteCdTipoContratoNegocio()
    {
        this._has_cdTipoContratoNegocio= false;
    } //-- void deleteCdTipoContratoNegocio() 

    /**
     * Method deleteNrOcorrenciaContaFavorecido
     * 
     */
    public void deleteNrOcorrenciaContaFavorecido()
    {
        this._has_nrOcorrenciaContaFavorecido= false;
    } //-- void deleteNrOcorrenciaContaFavorecido() 

    /**
     * Method deleteNrSequenciaContratoNegocio
     * 
     */
    public void deleteNrSequenciaContratoNegocio()
    {
        this._has_nrSequenciaContratoNegocio= false;
    } //-- void deleteNrSequenciaContratoNegocio() 

    /**
     * Returns the value of field 'cdFavorecidoClientePagador'.
     * 
     * @return long
     * @return the value of field 'cdFavorecidoClientePagador'.
     */
    public long getCdFavorecidoClientePagador()
    {
        return this._cdFavorecidoClientePagador;
    } //-- long getCdFavorecidoClientePagador() 

    /**
     * Returns the value of field 'cdPessoaJuridicaContrato'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridicaContrato'.
     */
    public long getCdPessoaJuridicaContrato()
    {
        return this._cdPessoaJuridicaContrato;
    } //-- long getCdPessoaJuridicaContrato() 

    /**
     * Returns the value of field 'cdTipoContaFavorecido'.
     * 
     * @return int
     * @return the value of field 'cdTipoContaFavorecido'.
     */
    public int getCdTipoContaFavorecido()
    {
        return this._cdTipoContaFavorecido;
    } //-- int getCdTipoContaFavorecido() 

    /**
     * Returns the value of field 'cdTipoContratoNegocio'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoNegocio'.
     */
    public int getCdTipoContratoNegocio()
    {
        return this._cdTipoContratoNegocio;
    } //-- int getCdTipoContratoNegocio() 

    /**
     * Returns the value of field 'nrOcorrenciaContaFavorecido'.
     * 
     * @return int
     * @return the value of field 'nrOcorrenciaContaFavorecido'.
     */
    public int getNrOcorrenciaContaFavorecido()
    {
        return this._nrOcorrenciaContaFavorecido;
    } //-- int getNrOcorrenciaContaFavorecido() 

    /**
     * Returns the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @return long
     * @return the value of field 'nrSequenciaContratoNegocio'.
     */
    public long getNrSequenciaContratoNegocio()
    {
        return this._nrSequenciaContratoNegocio;
    } //-- long getNrSequenciaContratoNegocio() 

    /**
     * Method hasCdFavorecidoClientePagador
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFavorecidoClientePagador()
    {
        return this._has_cdFavorecidoClientePagador;
    } //-- boolean hasCdFavorecidoClientePagador() 

    /**
     * Method hasCdPessoaJuridicaContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridicaContrato()
    {
        return this._has_cdPessoaJuridicaContrato;
    } //-- boolean hasCdPessoaJuridicaContrato() 

    /**
     * Method hasCdTipoContaFavorecido
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContaFavorecido()
    {
        return this._has_cdTipoContaFavorecido;
    } //-- boolean hasCdTipoContaFavorecido() 

    /**
     * Method hasCdTipoContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoNegocio()
    {
        return this._has_cdTipoContratoNegocio;
    } //-- boolean hasCdTipoContratoNegocio() 

    /**
     * Method hasNrOcorrenciaContaFavorecido
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrOcorrenciaContaFavorecido()
    {
        return this._has_nrOcorrenciaContaFavorecido;
    } //-- boolean hasNrOcorrenciaContaFavorecido() 

    /**
     * Method hasNrSequenciaContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaContratoNegocio()
    {
        return this._has_nrSequenciaContratoNegocio;
    } //-- boolean hasNrSequenciaContratoNegocio() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdFavorecidoClientePagador'.
     * 
     * @param cdFavorecidoClientePagador the value of field
     * 'cdFavorecidoClientePagador'.
     */
    public void setCdFavorecidoClientePagador(long cdFavorecidoClientePagador)
    {
        this._cdFavorecidoClientePagador = cdFavorecidoClientePagador;
        this._has_cdFavorecidoClientePagador = true;
    } //-- void setCdFavorecidoClientePagador(long) 

    /**
     * Sets the value of field 'cdPessoaJuridicaContrato'.
     * 
     * @param cdPessoaJuridicaContrato the value of field
     * 'cdPessoaJuridicaContrato'.
     */
    public void setCdPessoaJuridicaContrato(long cdPessoaJuridicaContrato)
    {
        this._cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
        this._has_cdPessoaJuridicaContrato = true;
    } //-- void setCdPessoaJuridicaContrato(long) 

    /**
     * Sets the value of field 'cdTipoContaFavorecido'.
     * 
     * @param cdTipoContaFavorecido the value of field
     * 'cdTipoContaFavorecido'.
     */
    public void setCdTipoContaFavorecido(int cdTipoContaFavorecido)
    {
        this._cdTipoContaFavorecido = cdTipoContaFavorecido;
        this._has_cdTipoContaFavorecido = true;
    } //-- void setCdTipoContaFavorecido(int) 

    /**
     * Sets the value of field 'cdTipoContratoNegocio'.
     * 
     * @param cdTipoContratoNegocio the value of field
     * 'cdTipoContratoNegocio'.
     */
    public void setCdTipoContratoNegocio(int cdTipoContratoNegocio)
    {
        this._cdTipoContratoNegocio = cdTipoContratoNegocio;
        this._has_cdTipoContratoNegocio = true;
    } //-- void setCdTipoContratoNegocio(int) 

    /**
     * Sets the value of field 'nrOcorrenciaContaFavorecido'.
     * 
     * @param nrOcorrenciaContaFavorecido the value of field
     * 'nrOcorrenciaContaFavorecido'.
     */
    public void setNrOcorrenciaContaFavorecido(int nrOcorrenciaContaFavorecido)
    {
        this._nrOcorrenciaContaFavorecido = nrOcorrenciaContaFavorecido;
        this._has_nrOcorrenciaContaFavorecido = true;
    } //-- void setNrOcorrenciaContaFavorecido(int) 

    /**
     * Sets the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @param nrSequenciaContratoNegocio the value of field
     * 'nrSequenciaContratoNegocio'.
     */
    public void setNrSequenciaContratoNegocio(long nrSequenciaContratoNegocio)
    {
        this._nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
        this._has_nrSequenciaContratoNegocio = true;
    } //-- void setNrSequenciaContratoNegocio(long) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ConsultarDetalheContaFavorecidoRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultardetalhecontafavorecido.request.ConsultarDetalheContaFavorecidoRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultardetalhecontafavorecido.request.ConsultarDetalheContaFavorecidoRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultardetalhecontafavorecido.request.ConsultarDetalheContaFavorecidoRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultardetalhecontafavorecido.request.ConsultarDetalheContaFavorecidoRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
