/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/interfaz.ftl,v $
 * $Id: interfaz.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: interfaz.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.canlibpagtoconsolidadosemconsaldo;

import java.util.List;

import br.com.bradesco.web.pgit.service.business.canlibpagtoconsolidadosemconsaldo.bean.CancelarLiberacaoIntegralEntradaDTO;
import br.com.bradesco.web.pgit.service.business.canlibpagtoconsolidadosemconsaldo.bean.CancelarLiberacaoIntegralSaidaDTO;
import br.com.bradesco.web.pgit.service.business.canlibpagtoconsolidadosemconsaldo.bean.CancelarLiberacaoParcialEntradaDTO;
import br.com.bradesco.web.pgit.service.business.canlibpagtoconsolidadosemconsaldo.bean.CancelarLiberacaoParcialSaidaDTO;
import br.com.bradesco.web.pgit.service.business.canlibpagtoconsolidadosemconsaldo.bean.ConsultarCanLibPagtosConsSemConsSaldoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.canlibpagtoconsolidadosemconsaldo.bean.ConsultarCanLibPagtosConsSemConsSaldoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.canlibpagtoconsolidadosemconsaldo.bean.DetalharCanLibPagtosConsSemConsSaldoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.canlibpagtoconsolidadosemconsaldo.bean.DetalharCanLibPagtosConsSemConsSaldoSaidaDTO;

/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Interface do adaptador: CanLibPagtoConsolidadoSemConSaldo
 * </p>
 * 
 * @comment CODIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public interface ICanLibPagtoConsolidadoSemConSaldoService {
	
	/**
	 * Detalhar can lib pagtos cons sem cons saldo.
	 *
	 * @param entradaDTO the entrada dto
	 * @return the detalhar can lib pagtos cons sem cons saldo saida dto
	 */
	DetalharCanLibPagtosConsSemConsSaldoSaidaDTO detalharCanLibPagtosConsSemConsSaldo(DetalharCanLibPagtosConsSemConsSaldoEntradaDTO entradaDTO);
	
	/**
	 * Cancelar liberacao integral.
	 *
	 * @param entradaDTO the entrada dto
	 * @return the cancelar liberacao integral saida dto
	 */
	CancelarLiberacaoIntegralSaidaDTO cancelarLiberacaoIntegral(CancelarLiberacaoIntegralEntradaDTO entradaDTO);
	
	/**
	 * Cancelar liberacao parcial.
	 *
	 * @param listaEntradaDTO the lista entrada dto
	 * @return the cancelar liberacao parcial saida dto
	 */
	CancelarLiberacaoParcialSaidaDTO cancelarLiberacaoParcial(List<CancelarLiberacaoParcialEntradaDTO> listaEntradaDTO);
	
	/**
	 * Consultar can lib pagtos cons sem cons saldo.
	 *
	 * @param entradaDTO the entrada dto
	 * @return the list< consultar can lib pagtos cons sem cons saldo saida dt o>
	 */
	List<ConsultarCanLibPagtosConsSemConsSaldoSaidaDTO> consultarCanLibPagtosConsSemConsSaldo(ConsultarCanLibPagtosConsSemConsSaldoEntradaDTO entradaDTO);
}

