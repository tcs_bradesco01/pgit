/*
 * Nome: br.com.bradesco.web.pgit.service.business.funcoesalcada.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantermodalidadesnetempresa.bean;

import java.util.List;


// TODO: Auto-generated Javadoc
/**
 * Nome: ListarFuncoesAlcadaSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarHorarioLimiteSaidaDTO {
	
	/** The nr modalidades. */
	private Integer nrModalidades = null;
	
	/** The ocorrencias. */
	private List<ListarHorarioLimiteOcorrenciasSaidaDTO> ocorrencias = null;

	/**
	 * Gets the nr modalidades.
	 *
	 * @return the nrModalidades
	 */
	public Integer getNrModalidades() {
	    return nrModalidades;
	}

	/**
	 * Sets the nr modalidades.
	 *
	 * @param nrModalidades the nrModalidades to set
	 */
	public void setNrModalidades(Integer nrModalidades) {
	    this.nrModalidades = nrModalidades;
	}

	/**
	 * Gets the ocorrencias.
	 *
	 * @return the ocorrencias
	 */
	public List<ListarHorarioLimiteOcorrenciasSaidaDTO> getOcorrencias() {
	    return ocorrencias;
	}

	/**
	 * Sets the ocorrencias.
	 *
	 * @param ocorrencias the ocorrencias to set
	 */
	public void setOcorrencias(
		List<ListarHorarioLimiteOcorrenciasSaidaDTO> ocorrencias) {
	    this.ocorrencias = ocorrencias;
	} 
	
	
}
