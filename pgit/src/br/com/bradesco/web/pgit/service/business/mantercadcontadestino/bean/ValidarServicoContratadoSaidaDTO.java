/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantercadcontadestino.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantercadcontadestino.bean;

/**
 * Nome: ValidarServicoContratadoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ValidarServicoContratadoSaidaDTO {

	/** Atributo mensagem. */
	private String mensagem;
	
	/** Atributo cdMensagem. */
	private String cdMensagem;

	/**
	 * Validar servico contratado saida dto.
	 */
	public ValidarServicoContratadoSaidaDTO() {
		super();
	}

	/**
	 * Validar servico contratado saida dto.
	 *
	 * @param mensagem the mensagem
	 * @param cdMensagem the cd mensagem
	 */
	public ValidarServicoContratadoSaidaDTO(String mensagem, String cdMensagem) {
		super();
		this.mensagem = mensagem;
		this.cdMensagem = cdMensagem;
	}

	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	/**
	 * Get: cdMensagem.
	 *
	 * @return cdMensagem
	 */
	public String getCdMensagem() {
		return cdMensagem;
	}
	
	/**
	 * Set: cdMensagem.
	 *
	 * @param cdMensagem the cd mensagem
	 */
	public void setCdMensagem(String cdMensagem) {
		this.cdMensagem = cdMensagem;
	}
}