/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantersolestornopagto.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantersolestornopagto.bean;

/**
 * Nome: AutorizarSolicitacaoEstornoPatosEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class AutorizarSolicitacaoEstornoPatosEntradaDTO {
	
	/** Atributo cdSolicitacaoPagamento. */
	private Integer cdSolicitacaoPagamento;
	
	/** Atributo nrSolicitacaoPagamento. */
	private Integer nrSolicitacaoPagamento;
	
	/** Atributo dsObservacao. */
	private String dsObservacao;
	
	/** Atributo tipoAcao. */
	private Integer tipoAcao;
	
	/**
	 * Get: cdSolicitacaoPagamento.
	 *
	 * @return cdSolicitacaoPagamento
	 */
	public Integer getCdSolicitacaoPagamento() {
		return cdSolicitacaoPagamento;
	}
	
	/**
	 * Set: cdSolicitacaoPagamento.
	 *
	 * @param cdSolicitacaoPagamento the cd solicitacao pagamento
	 */
	public void setCdSolicitacaoPagamento(Integer cdSolicitacaoPagamento) {
		this.cdSolicitacaoPagamento = cdSolicitacaoPagamento;
	}
	
	/**
	 * Get: dsObservacao.
	 *
	 * @return dsObservacao
	 */
	public String getDsObservacao() {
		return dsObservacao;
	}
	
	/**
	 * Set: dsObservacao.
	 *
	 * @param dsObservacao the ds observacao
	 */
	public void setDsObservacao(String dsObservacao) {
		this.dsObservacao = dsObservacao;
	}
	
	/**
	 * Get: nrSolicitacaoPagamento.
	 *
	 * @return nrSolicitacaoPagamento
	 */
	public Integer getNrSolicitacaoPagamento() {
		return nrSolicitacaoPagamento;
	}
	
	/**
	 * Set: nrSolicitacaoPagamento.
	 *
	 * @param nrSolicitacaoPagamento the nr solicitacao pagamento
	 */
	public void setNrSolicitacaoPagamento(Integer nrSolicitacaoPagamento) {
		this.nrSolicitacaoPagamento = nrSolicitacaoPagamento;
	}
	
	/**
	 * Get: tipoAcao.
	 *
	 * @return tipoAcao
	 */
	public Integer getTipoAcao() {
		return tipoAcao;
	}
	
	/**
	 * Set: tipoAcao.
	 *
	 * @param tipoAcao the tipo acao
	 */
	public void setTipoAcao(Integer tipoAcao) {
		this.tipoAcao = tipoAcao;
	}
	
	
	
	
	

}
