/*
 * Nome: br.com.bradesco.web.pgit.service.business.simulacaotarifasnegociacao.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.simulacaotarifasnegociacao.bean;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Nome: DetalharSimulacaoTarifaNegociacaoEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class DetalharSimulacaoTarifaNegociacaoEntradaDTO {

	
	  /** Atributo vlTarifaTotalPad. */
  	private BigDecimal vlTarifaTotalPad;
	  
  	/** Atributo vlTarifaTotalPro. */
  	private BigDecimal vlTarifaTotalPro;
	  
  	/** Atributo qtOcorrencias. */
  	private Integer qtOcorrencias;
	  
  	/** Atributo ocorrencias. */
  	private List<OcorrenciasDTO> ocorrencias = new ArrayList<OcorrenciasDTO>();
	
	/**
	 * Get: ocorrencias.
	 *
	 * @return ocorrencias
	 */
	public List<OcorrenciasDTO> getOcorrencias() {
		return ocorrencias;
	}
	
	/**
	 * Set: ocorrencias.
	 *
	 * @param ocorrencias the ocorrencias
	 */
	public void setOcorrencias(List<OcorrenciasDTO> ocorrencias) {
		this.ocorrencias = ocorrencias;
	}
	
	/**
	 * Get: qtOcorrencias.
	 *
	 * @return qtOcorrencias
	 */
	public Integer getQtOcorrencias() {
		return qtOcorrencias;
	}
	
	/**
	 * Set: qtOcorrencias.
	 *
	 * @param qtOcorrencias the qt ocorrencias
	 */
	public void setQtOcorrencias(Integer qtOcorrencias) {
		this.qtOcorrencias = qtOcorrencias;
	}
	
	/**
	 * Get: vlTarifaTotalPad.
	 *
	 * @return vlTarifaTotalPad
	 */
	public BigDecimal getVlTarifaTotalPad() {
		return vlTarifaTotalPad;
	}
	
	/**
	 * Set: vlTarifaTotalPad.
	 *
	 * @param vlTarifaTotalPad the vl tarifa total pad
	 */
	public void setVlTarifaTotalPad(BigDecimal vlTarifaTotalPad) {
		this.vlTarifaTotalPad = vlTarifaTotalPad;
	}
	
	/**
	 * Get: vlTarifaTotalPro.
	 *
	 * @return vlTarifaTotalPro
	 */
	public BigDecimal getVlTarifaTotalPro() {
		return vlTarifaTotalPro;
	}
	
	/**
	 * Set: vlTarifaTotalPro.
	 *
	 * @param vlTarifaTotalPro the vl tarifa total pro
	 */
	public void setVlTarifaTotalPro(BigDecimal vlTarifaTotalPro) {
		this.vlTarifaTotalPro = vlTarifaTotalPro;
	}
	  
	  
}
