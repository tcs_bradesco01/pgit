/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listarassversaolayoutloteservico.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ListarAssVersaoLayoutLoteServicoRequest.
 * 
 * @version $Revision$ $Date$
 */
public class ListarAssVersaoLayoutLoteServicoRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdTipoLayoutArquivo
     */
    private int _cdTipoLayoutArquivo = 0;

    /**
     * keeps track of state for field: _cdTipoLayoutArquivo
     */
    private boolean _has_cdTipoLayoutArquivo;

    /**
     * Field _numeroVersaoLayoutArquivo
     */
    private int _numeroVersaoLayoutArquivo = 0;

    /**
     * keeps track of state for field: _numeroVersaoLayoutArquivo
     */
    private boolean _has_numeroVersaoLayoutArquivo;

    /**
     * Field _cdTipoLoteLayout
     */
    private int _cdTipoLoteLayout = 0;

    /**
     * keeps track of state for field: _cdTipoLoteLayout
     */
    private boolean _has_cdTipoLoteLayout;

    /**
     * Field _nrVersaoLoteLayout
     */
    private int _nrVersaoLoteLayout = 0;

    /**
     * keeps track of state for field: _nrVersaoLoteLayout
     */
    private boolean _has_nrVersaoLoteLayout;

    /**
     * Field _cdTipoServicoCnab
     */
    private int _cdTipoServicoCnab = 0;

    /**
     * keeps track of state for field: _cdTipoServicoCnab
     */
    private boolean _has_cdTipoServicoCnab;


      //----------------/
     //- Constructors -/
    //----------------/

    public ListarAssVersaoLayoutLoteServicoRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarassversaolayoutloteservico.request.ListarAssVersaoLayoutLoteServicoRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdTipoLayoutArquivo
     * 
     */
    public void deleteCdTipoLayoutArquivo()
    {
        this._has_cdTipoLayoutArquivo= false;
    } //-- void deleteCdTipoLayoutArquivo() 

    /**
     * Method deleteCdTipoLoteLayout
     * 
     */
    public void deleteCdTipoLoteLayout()
    {
        this._has_cdTipoLoteLayout= false;
    } //-- void deleteCdTipoLoteLayout() 

    /**
     * Method deleteCdTipoServicoCnab
     * 
     */
    public void deleteCdTipoServicoCnab()
    {
        this._has_cdTipoServicoCnab= false;
    } //-- void deleteCdTipoServicoCnab() 

    /**
     * Method deleteNrVersaoLoteLayout
     * 
     */
    public void deleteNrVersaoLoteLayout()
    {
        this._has_nrVersaoLoteLayout= false;
    } //-- void deleteNrVersaoLoteLayout() 

    /**
     * Method deleteNumeroVersaoLayoutArquivo
     * 
     */
    public void deleteNumeroVersaoLayoutArquivo()
    {
        this._has_numeroVersaoLayoutArquivo= false;
    } //-- void deleteNumeroVersaoLayoutArquivo() 

    /**
     * Returns the value of field 'cdTipoLayoutArquivo'.
     * 
     * @return int
     * @return the value of field 'cdTipoLayoutArquivo'.
     */
    public int getCdTipoLayoutArquivo()
    {
        return this._cdTipoLayoutArquivo;
    } //-- int getCdTipoLayoutArquivo() 

    /**
     * Returns the value of field 'cdTipoLoteLayout'.
     * 
     * @return int
     * @return the value of field 'cdTipoLoteLayout'.
     */
    public int getCdTipoLoteLayout()
    {
        return this._cdTipoLoteLayout;
    } //-- int getCdTipoLoteLayout() 

    /**
     * Returns the value of field 'cdTipoServicoCnab'.
     * 
     * @return int
     * @return the value of field 'cdTipoServicoCnab'.
     */
    public int getCdTipoServicoCnab()
    {
        return this._cdTipoServicoCnab;
    } //-- int getCdTipoServicoCnab() 

    /**
     * Returns the value of field 'nrVersaoLoteLayout'.
     * 
     * @return int
     * @return the value of field 'nrVersaoLoteLayout'.
     */
    public int getNrVersaoLoteLayout()
    {
        return this._nrVersaoLoteLayout;
    } //-- int getNrVersaoLoteLayout() 

    /**
     * Returns the value of field 'numeroVersaoLayoutArquivo'.
     * 
     * @return int
     * @return the value of field 'numeroVersaoLayoutArquivo'.
     */
    public int getNumeroVersaoLayoutArquivo()
    {
        return this._numeroVersaoLayoutArquivo;
    } //-- int getNumeroVersaoLayoutArquivo() 

    /**
     * Method hasCdTipoLayoutArquivo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoLayoutArquivo()
    {
        return this._has_cdTipoLayoutArquivo;
    } //-- boolean hasCdTipoLayoutArquivo() 

    /**
     * Method hasCdTipoLoteLayout
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoLoteLayout()
    {
        return this._has_cdTipoLoteLayout;
    } //-- boolean hasCdTipoLoteLayout() 

    /**
     * Method hasCdTipoServicoCnab
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoServicoCnab()
    {
        return this._has_cdTipoServicoCnab;
    } //-- boolean hasCdTipoServicoCnab() 

    /**
     * Method hasNrVersaoLoteLayout
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrVersaoLoteLayout()
    {
        return this._has_nrVersaoLoteLayout;
    } //-- boolean hasNrVersaoLoteLayout() 

    /**
     * Method hasNumeroVersaoLayoutArquivo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNumeroVersaoLayoutArquivo()
    {
        return this._has_numeroVersaoLayoutArquivo;
    } //-- boolean hasNumeroVersaoLayoutArquivo() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdTipoLayoutArquivo'.
     * 
     * @param cdTipoLayoutArquivo the value of field
     * 'cdTipoLayoutArquivo'.
     */
    public void setCdTipoLayoutArquivo(int cdTipoLayoutArquivo)
    {
        this._cdTipoLayoutArquivo = cdTipoLayoutArquivo;
        this._has_cdTipoLayoutArquivo = true;
    } //-- void setCdTipoLayoutArquivo(int) 

    /**
     * Sets the value of field 'cdTipoLoteLayout'.
     * 
     * @param cdTipoLoteLayout the value of field 'cdTipoLoteLayout'
     */
    public void setCdTipoLoteLayout(int cdTipoLoteLayout)
    {
        this._cdTipoLoteLayout = cdTipoLoteLayout;
        this._has_cdTipoLoteLayout = true;
    } //-- void setCdTipoLoteLayout(int) 

    /**
     * Sets the value of field 'cdTipoServicoCnab'.
     * 
     * @param cdTipoServicoCnab the value of field
     * 'cdTipoServicoCnab'.
     */
    public void setCdTipoServicoCnab(int cdTipoServicoCnab)
    {
        this._cdTipoServicoCnab = cdTipoServicoCnab;
        this._has_cdTipoServicoCnab = true;
    } //-- void setCdTipoServicoCnab(int) 

    /**
     * Sets the value of field 'nrVersaoLoteLayout'.
     * 
     * @param nrVersaoLoteLayout the value of field
     * 'nrVersaoLoteLayout'.
     */
    public void setNrVersaoLoteLayout(int nrVersaoLoteLayout)
    {
        this._nrVersaoLoteLayout = nrVersaoLoteLayout;
        this._has_nrVersaoLoteLayout = true;
    } //-- void setNrVersaoLoteLayout(int) 

    /**
     * Sets the value of field 'numeroVersaoLayoutArquivo'.
     * 
     * @param numeroVersaoLayoutArquivo the value of field
     * 'numeroVersaoLayoutArquivo'.
     */
    public void setNumeroVersaoLayoutArquivo(int numeroVersaoLayoutArquivo)
    {
        this._numeroVersaoLayoutArquivo = numeroVersaoLayoutArquivo;
        this._has_numeroVersaoLayoutArquivo = true;
    } //-- void setNumeroVersaoLayoutArquivo(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ListarAssVersaoLayoutLoteServicoRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listarassversaolayoutloteservico.request.ListarAssVersaoLayoutLoteServicoRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listarassversaolayoutloteservico.request.ListarAssVersaoLayoutLoteServicoRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listarassversaolayoutloteservico.request.ListarAssVersaoLayoutLoteServicoRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarassversaolayoutloteservico.request.ListarAssVersaoLayoutLoteServicoRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
