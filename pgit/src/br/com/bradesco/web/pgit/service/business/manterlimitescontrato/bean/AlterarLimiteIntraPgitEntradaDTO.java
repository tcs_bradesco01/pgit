/*
 * Nome: br.com.bradesco.web.pgit.service.business.manterlimitescontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.manterlimitescontrato.bean;
import java.math.BigDecimal;

/**
 * Nome: AlterarLimiteIntraPgitEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class AlterarLimiteIntraPgitEntradaDTO{
	
	/** Atributo cdpessoaJuridicaContrato. */
	private Long cdpessoaJuridicaContrato;
	
	/** Atributo cdTipoContratoNegocio. */
	private Integer cdTipoContratoNegocio;
	
	/** Atributo nrSequenciaContratoNegocio. */
	private Long nrSequenciaContratoNegocio;
	
	/** Atributo cdPessoaJuridicaVinculo. */
	private Long cdPessoaJuridicaVinculo;
	
	/** Atributo cdTipoContratoVinculo. */
	private Integer cdTipoContratoVinculo;
	
	/** Atributo nrSequenciaContratoVinculo. */
	private Long nrSequenciaContratoVinculo;
	
	/** Atributo cdTipoVincContrato. */
	private Integer cdTipoVincContrato;
	
	/** Atributo vlAdicionalContratoConta. */
	private BigDecimal vlAdicionalContratoConta;
	
	/** Atributo dtInicioValorAdicional. */
	private String dtInicioValorAdicional;
	
	/** Atributo dtFimValorAdicional. */
	private String dtFimValorAdicional;
	
	/** Atributo vlAdicContrCtaTed. */
	private BigDecimal vlAdicContrCtaTed;
	
	/** Atributo dtIniVlrAdicionalTed. */
	private String dtIniVlrAdicionalTed;
	
	/** Atributo dtFimVlrAdicionalTed. */
	private String dtFimVlrAdicionalTed;

	/**
	 * Get: cdpessoaJuridicaContrato.
	 *
	 * @return cdpessoaJuridicaContrato
	 */
	public Long getCdpessoaJuridicaContrato(){
		return cdpessoaJuridicaContrato;
	}

	/**
	 * Set: cdpessoaJuridicaContrato.
	 *
	 * @param cdpessoaJuridicaContrato the cdpessoa juridica contrato
	 */
	public void setCdpessoaJuridicaContrato(Long cdpessoaJuridicaContrato){
		this.cdpessoaJuridicaContrato = cdpessoaJuridicaContrato;
	}

	/**
	 * Get: cdTipoContratoNegocio.
	 *
	 * @return cdTipoContratoNegocio
	 */
	public Integer getCdTipoContratoNegocio(){
		return cdTipoContratoNegocio;
	}

	/**
	 * Set: cdTipoContratoNegocio.
	 *
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio){
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}

	/**
	 * Get: nrSequenciaContratoNegocio.
	 *
	 * @return nrSequenciaContratoNegocio
	 */
	public Long getNrSequenciaContratoNegocio(){
		return nrSequenciaContratoNegocio;
	}

	/**
	 * Set: nrSequenciaContratoNegocio.
	 *
	 * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
	 */
	public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio){
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}

	/**
	 * Get: cdPessoaJuridicaVinculo.
	 *
	 * @return cdPessoaJuridicaVinculo
	 */
	public Long getCdPessoaJuridicaVinculo(){
		return cdPessoaJuridicaVinculo;
	}

	/**
	 * Set: cdPessoaJuridicaVinculo.
	 *
	 * @param cdPessoaJuridicaVinculo the cd pessoa juridica vinculo
	 */
	public void setCdPessoaJuridicaVinculo(Long cdPessoaJuridicaVinculo){
		this.cdPessoaJuridicaVinculo = cdPessoaJuridicaVinculo;
	}

	/**
	 * Get: cdTipoContratoVinculo.
	 *
	 * @return cdTipoContratoVinculo
	 */
	public Integer getCdTipoContratoVinculo(){
		return cdTipoContratoVinculo;
	}

	/**
	 * Set: cdTipoContratoVinculo.
	 *
	 * @param cdTipoContratoVinculo the cd tipo contrato vinculo
	 */
	public void setCdTipoContratoVinculo(Integer cdTipoContratoVinculo){
		this.cdTipoContratoVinculo = cdTipoContratoVinculo;
	}

	/**
	 * Get: nrSequenciaContratoVinculo.
	 *
	 * @return nrSequenciaContratoVinculo
	 */
	public Long getNrSequenciaContratoVinculo(){
		return nrSequenciaContratoVinculo;
	}

	/**
	 * Set: nrSequenciaContratoVinculo.
	 *
	 * @param nrSequenciaContratoVinculo the nr sequencia contrato vinculo
	 */
	public void setNrSequenciaContratoVinculo(Long nrSequenciaContratoVinculo){
		this.nrSequenciaContratoVinculo = nrSequenciaContratoVinculo;
	}

	/**
	 * Get: cdTipoVincContrato.
	 *
	 * @return cdTipoVincContrato
	 */
	public Integer getCdTipoVincContrato(){
		return cdTipoVincContrato;
	}

	/**
	 * Set: cdTipoVincContrato.
	 *
	 * @param cdTipoVincContrato the cd tipo vinc contrato
	 */
	public void setCdTipoVincContrato(Integer cdTipoVincContrato){
		this.cdTipoVincContrato = cdTipoVincContrato;
	}

	/**
	 * Get: vlAdicionalContratoConta.
	 *
	 * @return vlAdicionalContratoConta
	 */
	public BigDecimal getVlAdicionalContratoConta(){
		return vlAdicionalContratoConta;
	}

	/**
	 * Set: vlAdicionalContratoConta.
	 *
	 * @param vlAdicionalContratoConta the vl adicional contrato conta
	 */
	public void setVlAdicionalContratoConta(BigDecimal vlAdicionalContratoConta){
		this.vlAdicionalContratoConta = vlAdicionalContratoConta;
	}

	/**
	 * Get: dtInicioValorAdicional.
	 *
	 * @return dtInicioValorAdicional
	 */
	public String getDtInicioValorAdicional(){
		return dtInicioValorAdicional;
	}

	/**
	 * Set: dtInicioValorAdicional.
	 *
	 * @param dtInicioValorAdicional the dt inicio valor adicional
	 */
	public void setDtInicioValorAdicional(String dtInicioValorAdicional){
		this.dtInicioValorAdicional = dtInicioValorAdicional;
	}

	/**
	 * Get: dtFimValorAdicional.
	 *
	 * @return dtFimValorAdicional
	 */
	public String getDtFimValorAdicional(){
		return dtFimValorAdicional;
	}

	/**
	 * Set: dtFimValorAdicional.
	 *
	 * @param dtFimValorAdicional the dt fim valor adicional
	 */
	public void setDtFimValorAdicional(String dtFimValorAdicional){
		this.dtFimValorAdicional = dtFimValorAdicional;
	}

	/**
	 * Get: vlAdicContrCtaTed.
	 *
	 * @return vlAdicContrCtaTed
	 */
	public BigDecimal getVlAdicContrCtaTed() {
		return vlAdicContrCtaTed;
	}

	/**
	 * Set: vlAdicContrCtaTed.
	 *
	 * @param vlAdicContrCtaTed the vl adic contr cta ted
	 */
	public void setVlAdicContrCtaTed(BigDecimal vlAdicContrCtaTed) {
		this.vlAdicContrCtaTed = vlAdicContrCtaTed;
	}

	/**
	 * Get: dtIniVlrAdicionalTed.
	 *
	 * @return dtIniVlrAdicionalTed
	 */
	public String getDtIniVlrAdicionalTed() {
		return dtIniVlrAdicionalTed;
	}

	/**
	 * Set: dtIniVlrAdicionalTed.
	 *
	 * @param dtIniVlrAdicionalTed the dt ini vlr adicional ted
	 */
	public void setDtIniVlrAdicionalTed(String dtIniVlrAdicionalTed) {
		this.dtIniVlrAdicionalTed = dtIniVlrAdicionalTed;
	}

	/**
	 * Get: dtFimVlrAdicionalTed.
	 *
	 * @return dtFimVlrAdicionalTed
	 */
	public String getDtFimVlrAdicionalTed() {
		return dtFimVlrAdicionalTed;
	}

	/**
	 * Set: dtFimVlrAdicionalTed.
	 *
	 * @param dtFimVlrAdicionalTed the dt fim vlr adicional ted
	 */
	public void setDtFimVlrAdicionalTed(String dtFimVlrAdicionalTed) {
		this.dtFimVlrAdicionalTed = dtFimVlrAdicionalTed;
	}
}