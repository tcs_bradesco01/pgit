/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.detalharmanpagtocreditoconta.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.math.BigDecimal;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class DetalharManPagtoCreditoContaResponse.
 * 
 * @version $Revision$ $Date$
 */
public class DetalharManPagtoCreditoContaResponse implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _codMensagem
     */
    private java.lang.String _codMensagem;

    /**
     * Field _mensagem
     */
    private java.lang.String _mensagem;

    /**
     * Field _dtNascimentoBeneficiario
     */
    private java.lang.String _dtNascimentoBeneficiario;

    /**
     * Field _vlDesconto
     */
    private java.math.BigDecimal _vlDesconto = new java.math.BigDecimal("0");

    /**
     * Field _vlAbatimento
     */
    private java.math.BigDecimal _vlAbatimento = new java.math.BigDecimal("0");

    /**
     * Field _vlMulta
     */
    private java.math.BigDecimal _vlMulta = new java.math.BigDecimal("0");

    /**
     * Field _vlMora
     */
    private java.math.BigDecimal _vlMora = new java.math.BigDecimal("0");

    /**
     * Field _vlDeducao
     */
    private java.math.BigDecimal _vlDeducao = new java.math.BigDecimal("0");

    /**
     * Field _vlAcrescimo
     */
    private java.math.BigDecimal _vlAcrescimo = new java.math.BigDecimal("0");

    /**
     * Field _vlImpostoRenda
     */
    private java.math.BigDecimal _vlImpostoRenda = new java.math.BigDecimal("0");

    /**
     * Field _vlIss
     */
    private java.math.BigDecimal _vlIss = new java.math.BigDecimal("0");

    /**
     * Field _vlIof
     */
    private java.math.BigDecimal _vlIof = new java.math.BigDecimal("0");

    /**
     * Field _vlInss
     */
    private java.math.BigDecimal _vlInss = new java.math.BigDecimal("0");

    /**
     * Field _cdBancoDestino
     */
    private int _cdBancoDestino = 0;

    /**
     * keeps track of state for field: _cdBancoDestino
     */
    private boolean _has_cdBancoDestino;

    /**
     * Field _cdAgenciaDestino
     */
    private int _cdAgenciaDestino = 0;

    /**
     * keeps track of state for field: _cdAgenciaDestino
     */
    private boolean _has_cdAgenciaDestino;

    /**
     * Field _cdDigitoAgenciaDestino
     */
    private int _cdDigitoAgenciaDestino = 0;

    /**
     * keeps track of state for field: _cdDigitoAgenciaDestino
     */
    private boolean _has_cdDigitoAgenciaDestino;

    /**
     * Field _cdContaDestino
     */
    private long _cdContaDestino = 0;

    /**
     * keeps track of state for field: _cdContaDestino
     */
    private boolean _has_cdContaDestino;

    /**
     * Field _cdDigitoContaDestino
     */
    private java.lang.String _cdDigitoContaDestino;

    /**
     * Field _dsBancoDestino
     */
    private java.lang.String _dsBancoDestino;

    /**
     * Field _dsAgenciaDestino
     */
    private java.lang.String _dsAgenciaDestino;

    /**
     * Field _dsTipoContaDestino
     */
    private java.lang.String _dsTipoContaDestino;

    /**
     * Field _cdStatusTransmissao
     */
    private int _cdStatusTransmissao = 0;

    /**
     * keeps track of state for field: _cdStatusTransmissao
     */
    private boolean _has_cdStatusTransmissao;

    /**
     * Field _dsStatusTransmissao
     */
    private java.lang.String _dsStatusTransmissao;

    /**
     * Field _cdBancoFinal
     */
    private int _cdBancoFinal = 0;

    /**
     * keeps track of state for field: _cdBancoFinal
     */
    private boolean _has_cdBancoFinal;

    /**
     * Field _cdAgenciaFinal
     */
    private int _cdAgenciaFinal = 0;

    /**
     * keeps track of state for field: _cdAgenciaFinal
     */
    private boolean _has_cdAgenciaFinal;

    /**
     * Field _cdDigitoAgenciaFinal
     */
    private int _cdDigitoAgenciaFinal = 0;

    /**
     * keeps track of state for field: _cdDigitoAgenciaFinal
     */
    private boolean _has_cdDigitoAgenciaFinal;

    /**
     * Field _cdContaFinal
     */
    private long _cdContaFinal = 0;

    /**
     * keeps track of state for field: _cdContaFinal
     */
    private boolean _has_cdContaFinal;

    /**
     * Field _cdDigitoContaFinal
     */
    private java.lang.String _cdDigitoContaFinal;

    /**
     * Field _dsBanco
     */
    private java.lang.String _dsBanco;

    /**
     * Field _dsAgencia
     */
    private java.lang.String _dsAgencia;

    /**
     * Field _dsCodigoTipo
     */
    private java.lang.String _dsCodigoTipo;

    /**
     * Field _dtHoraTransmissao
     */
    private java.lang.String _dtHoraTransmissao;

    /**
     * Field _dtHoraDevolucao
     */
    private java.lang.String _dtHoraDevolucao;

    /**
     * Field _vlEmpresa
     */
    private java.math.BigDecimal _vlEmpresa = new java.math.BigDecimal("0");

    /**
     * Field _vlLeasing
     */
    private java.math.BigDecimal _vlLeasing = new java.math.BigDecimal("0");

    /**
     * Field _vlDebitoAut
     */
    private java.math.BigDecimal _vlDebitoAut = new java.math.BigDecimal("0");

    /**
     * Field _cdSituacaoTranferenciaAutomatica
     */
    private java.lang.String _cdSituacaoTranferenciaAutomatica;

    /**
     * Field _cdIndicadorModalidade
     */
    private int _cdIndicadorModalidade = 0;

    /**
     * keeps track of state for field: _cdIndicadorModalidade
     */
    private boolean _has_cdIndicadorModalidade;

    /**
     * Field _dsPagamento
     */
    private java.lang.String _dsPagamento;

    /**
     * Field _dsBancoDebito
     */
    private java.lang.String _dsBancoDebito;

    /**
     * Field _dsAgenciaDebito
     */
    private java.lang.String _dsAgenciaDebito;

    /**
     * Field _dsTipoContaDebito
     */
    private java.lang.String _dsTipoContaDebito;

    /**
     * Field _dsContrato
     */
    private java.lang.String _dsContrato;

    /**
     * Field _cdSituacaoContrato
     */
    private java.lang.String _cdSituacaoContrato;

    /**
     * Field _dtAgendamento
     */
    private java.lang.String _dtAgendamento;

    /**
     * Field _vlrAgendamento
     */
    private java.math.BigDecimal _vlrAgendamento = new java.math.BigDecimal("0");

    /**
     * Field _vlrEfetivacao
     */
    private java.math.BigDecimal _vlrEfetivacao = new java.math.BigDecimal("0");

    /**
     * Field _cdIndicadorEconomicoMoeda
     */
    private int _cdIndicadorEconomicoMoeda = 0;

    /**
     * keeps track of state for field: _cdIndicadorEconomicoMoeda
     */
    private boolean _has_cdIndicadorEconomicoMoeda;

    /**
     * Field _dsMoeda
     */
    private java.lang.String _dsMoeda;

    /**
     * Field _qtMoeda
     */
    private java.math.BigDecimal _qtMoeda = new java.math.BigDecimal("0");

    /**
     * Field _dtVencimento
     */
    private java.lang.String _dtVencimento;

    /**
     * Field _dsMensagemPrimeiraLinha
     */
    private java.lang.String _dsMensagemPrimeiraLinha;

    /**
     * Field _dsMensagemSegundaLinha
     */
    private java.lang.String _dsMensagemSegundaLinha;

    /**
     * Field _dsUsoEmpresa
     */
    private java.lang.String _dsUsoEmpresa;

    /**
     * Field _cdSituacaoOperacaoPagamento
     */
    private int _cdSituacaoOperacaoPagamento = 0;

    /**
     * keeps track of state for field: _cdSituacaoOperacaoPagamento
     */
    private boolean _has_cdSituacaoOperacaoPagamento;

    /**
     * Field _cdMotivoSituacaoPagamento
     */
    private int _cdMotivoSituacaoPagamento = 0;

    /**
     * keeps track of state for field: _cdMotivoSituacaoPagamento
     */
    private boolean _has_cdMotivoSituacaoPagamento;

    /**
     * Field _cdListaDebito
     */
    private long _cdListaDebito = 0;

    /**
     * keeps track of state for field: _cdListaDebito
     */
    private boolean _has_cdListaDebito;

    /**
     * Field _cdTipoInscricaoPagador
     */
    private int _cdTipoInscricaoPagador = 0;

    /**
     * keeps track of state for field: _cdTipoInscricaoPagador
     */
    private boolean _has_cdTipoInscricaoPagador;

    /**
     * Field _nrDocumento
     */
    private long _nrDocumento = 0;

    /**
     * keeps track of state for field: _nrDocumento
     */
    private boolean _has_nrDocumento;

    /**
     * Field _cdSerieDocumento
     */
    private java.lang.String _cdSerieDocumento;

    /**
     * Field _cdTipoDocumento
     */
    private int _cdTipoDocumento = 0;

    /**
     * keeps track of state for field: _cdTipoDocumento
     */
    private boolean _has_cdTipoDocumento;

    /**
     * Field _dsTipoDocumento
     */
    private java.lang.String _dsTipoDocumento;

    /**
     * Field _vlDocumento
     */
    private java.math.BigDecimal _vlDocumento = new java.math.BigDecimal("0");

    /**
     * Field _dtEmissaoDocumento
     */
    private java.lang.String _dtEmissaoDocumento;

    /**
     * Field _nrSequenciaArquivoRemessa
     */
    private long _nrSequenciaArquivoRemessa = 0;

    /**
     * keeps track of state for field: _nrSequenciaArquivoRemessa
     */
    private boolean _has_nrSequenciaArquivoRemessa;

    /**
     * Field _nrLoteArquivoRemessa
     */
    private long _nrLoteArquivoRemessa = 0;

    /**
     * keeps track of state for field: _nrLoteArquivoRemessa
     */
    private boolean _has_nrLoteArquivoRemessa;

    /**
     * Field _cdFavorecido
     */
    private long _cdFavorecido = 0;

    /**
     * keeps track of state for field: _cdFavorecido
     */
    private boolean _has_cdFavorecido;

    /**
     * Field _dsBancoFavorecido
     */
    private java.lang.String _dsBancoFavorecido;

    /**
     * Field _dsAgenciaFavorecido
     */
    private java.lang.String _dsAgenciaFavorecido;

    /**
     * Field _cdTipoContaFavorecido
     */
    private int _cdTipoContaFavorecido = 0;

    /**
     * keeps track of state for field: _cdTipoContaFavorecido
     */
    private boolean _has_cdTipoContaFavorecido;

    /**
     * Field _dsTipoContaFavorecido
     */
    private java.lang.String _dsTipoContaFavorecido;

    /**
     * Field _dsMotivoSituacao
     */
    private java.lang.String _dsMotivoSituacao;

    /**
     * Field _cdTipoManutencao
     */
    private int _cdTipoManutencao = 0;

    /**
     * keeps track of state for field: _cdTipoManutencao
     */
    private boolean _has_cdTipoManutencao;

    /**
     * Field _dsTipoManutencao
     */
    private java.lang.String _dsTipoManutencao;

    /**
     * Field _dtInclusao
     */
    private java.lang.String _dtInclusao;

    /**
     * Field _hrInclusao
     */
    private java.lang.String _hrInclusao;

    /**
     * Field _cdUsuarioInclusaoInterno
     */
    private java.lang.String _cdUsuarioInclusaoInterno;

    /**
     * Field _cdUsuarioInclusaoExterno
     */
    private java.lang.String _cdUsuarioInclusaoExterno;

    /**
     * Field _cdTipoCanalInclusao
     */
    private int _cdTipoCanalInclusao = 0;

    /**
     * keeps track of state for field: _cdTipoCanalInclusao
     */
    private boolean _has_cdTipoCanalInclusao;

    /**
     * Field _dsTipoCanalInclusao
     */
    private java.lang.String _dsTipoCanalInclusao;

    /**
     * Field _cdFluxoInclusao
     */
    private java.lang.String _cdFluxoInclusao;

    /**
     * Field _dtManutencao
     */
    private java.lang.String _dtManutencao;

    /**
     * Field _hrManutencao
     */
    private java.lang.String _hrManutencao;

    /**
     * Field _cdUsuarioManutencaoInterno
     */
    private java.lang.String _cdUsuarioManutencaoInterno;

    /**
     * Field _cdUsuarioManutencaoExterno
     */
    private java.lang.String _cdUsuarioManutencaoExterno;

    /**
     * Field _cdTipoCanalManutencao
     */
    private int _cdTipoCanalManutencao = 0;

    /**
     * keeps track of state for field: _cdTipoCanalManutencao
     */
    private boolean _has_cdTipoCanalManutencao;

    /**
     * Field _dsTipoCanalManutencao
     */
    private java.lang.String _dsTipoCanalManutencao;

    /**
     * Field _cdFluxoManutencao
     */
    private java.lang.String _cdFluxoManutencao;


      //----------------/
     //- Constructors -/
    //----------------/

    public DetalharManPagtoCreditoContaResponse() 
     {
        super();
        setVlDesconto(new java.math.BigDecimal("0"));
        setVlAbatimento(new java.math.BigDecimal("0"));
        setVlMulta(new java.math.BigDecimal("0"));
        setVlMora(new java.math.BigDecimal("0"));
        setVlDeducao(new java.math.BigDecimal("0"));
        setVlAcrescimo(new java.math.BigDecimal("0"));
        setVlImpostoRenda(new java.math.BigDecimal("0"));
        setVlIss(new java.math.BigDecimal("0"));
        setVlIof(new java.math.BigDecimal("0"));
        setVlInss(new java.math.BigDecimal("0"));
        setVlEmpresa(new java.math.BigDecimal("0"));
        setVlLeasing(new java.math.BigDecimal("0"));
        setVlDebitoAut(new java.math.BigDecimal("0"));
        setVlrAgendamento(new java.math.BigDecimal("0"));
        setVlrEfetivacao(new java.math.BigDecimal("0"));
        setQtMoeda(new java.math.BigDecimal("0"));
        setVlDocumento(new java.math.BigDecimal("0"));
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharmanpagtocreditoconta.response.DetalharManPagtoCreditoContaResponse()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdAgenciaDestino
     * 
     */
    public void deleteCdAgenciaDestino()
    {
        this._has_cdAgenciaDestino= false;
    } //-- void deleteCdAgenciaDestino() 

    /**
     * Method deleteCdAgenciaFinal
     * 
     */
    public void deleteCdAgenciaFinal()
    {
        this._has_cdAgenciaFinal= false;
    } //-- void deleteCdAgenciaFinal() 

    /**
     * Method deleteCdBancoDestino
     * 
     */
    public void deleteCdBancoDestino()
    {
        this._has_cdBancoDestino= false;
    } //-- void deleteCdBancoDestino() 

    /**
     * Method deleteCdBancoFinal
     * 
     */
    public void deleteCdBancoFinal()
    {
        this._has_cdBancoFinal= false;
    } //-- void deleteCdBancoFinal() 

    /**
     * Method deleteCdContaDestino
     * 
     */
    public void deleteCdContaDestino()
    {
        this._has_cdContaDestino= false;
    } //-- void deleteCdContaDestino() 

    /**
     * Method deleteCdContaFinal
     * 
     */
    public void deleteCdContaFinal()
    {
        this._has_cdContaFinal= false;
    } //-- void deleteCdContaFinal() 

    /**
     * Method deleteCdDigitoAgenciaDestino
     * 
     */
    public void deleteCdDigitoAgenciaDestino()
    {
        this._has_cdDigitoAgenciaDestino= false;
    } //-- void deleteCdDigitoAgenciaDestino() 

    /**
     * Method deleteCdDigitoAgenciaFinal
     * 
     */
    public void deleteCdDigitoAgenciaFinal()
    {
        this._has_cdDigitoAgenciaFinal= false;
    } //-- void deleteCdDigitoAgenciaFinal() 

    /**
     * Method deleteCdFavorecido
     * 
     */
    public void deleteCdFavorecido()
    {
        this._has_cdFavorecido= false;
    } //-- void deleteCdFavorecido() 

    /**
     * Method deleteCdIndicadorEconomicoMoeda
     * 
     */
    public void deleteCdIndicadorEconomicoMoeda()
    {
        this._has_cdIndicadorEconomicoMoeda= false;
    } //-- void deleteCdIndicadorEconomicoMoeda() 

    /**
     * Method deleteCdIndicadorModalidade
     * 
     */
    public void deleteCdIndicadorModalidade()
    {
        this._has_cdIndicadorModalidade= false;
    } //-- void deleteCdIndicadorModalidade() 

    /**
     * Method deleteCdListaDebito
     * 
     */
    public void deleteCdListaDebito()
    {
        this._has_cdListaDebito= false;
    } //-- void deleteCdListaDebito() 

    /**
     * Method deleteCdMotivoSituacaoPagamento
     * 
     */
    public void deleteCdMotivoSituacaoPagamento()
    {
        this._has_cdMotivoSituacaoPagamento= false;
    } //-- void deleteCdMotivoSituacaoPagamento() 

    /**
     * Method deleteCdSituacaoOperacaoPagamento
     * 
     */
    public void deleteCdSituacaoOperacaoPagamento()
    {
        this._has_cdSituacaoOperacaoPagamento= false;
    } //-- void deleteCdSituacaoOperacaoPagamento() 

    /**
     * Method deleteCdStatusTransmissao
     * 
     */
    public void deleteCdStatusTransmissao()
    {
        this._has_cdStatusTransmissao= false;
    } //-- void deleteCdStatusTransmissao() 

    /**
     * Method deleteCdTipoCanalInclusao
     * 
     */
    public void deleteCdTipoCanalInclusao()
    {
        this._has_cdTipoCanalInclusao= false;
    } //-- void deleteCdTipoCanalInclusao() 

    /**
     * Method deleteCdTipoCanalManutencao
     * 
     */
    public void deleteCdTipoCanalManutencao()
    {
        this._has_cdTipoCanalManutencao= false;
    } //-- void deleteCdTipoCanalManutencao() 

    /**
     * Method deleteCdTipoContaFavorecido
     * 
     */
    public void deleteCdTipoContaFavorecido()
    {
        this._has_cdTipoContaFavorecido= false;
    } //-- void deleteCdTipoContaFavorecido() 

    /**
     * Method deleteCdTipoDocumento
     * 
     */
    public void deleteCdTipoDocumento()
    {
        this._has_cdTipoDocumento= false;
    } //-- void deleteCdTipoDocumento() 

    /**
     * Method deleteCdTipoInscricaoPagador
     * 
     */
    public void deleteCdTipoInscricaoPagador()
    {
        this._has_cdTipoInscricaoPagador= false;
    } //-- void deleteCdTipoInscricaoPagador() 

    /**
     * Method deleteCdTipoManutencao
     * 
     */
    public void deleteCdTipoManutencao()
    {
        this._has_cdTipoManutencao= false;
    } //-- void deleteCdTipoManutencao() 

    /**
     * Method deleteNrDocumento
     * 
     */
    public void deleteNrDocumento()
    {
        this._has_nrDocumento= false;
    } //-- void deleteNrDocumento() 

    /**
     * Method deleteNrLoteArquivoRemessa
     * 
     */
    public void deleteNrLoteArquivoRemessa()
    {
        this._has_nrLoteArquivoRemessa= false;
    } //-- void deleteNrLoteArquivoRemessa() 

    /**
     * Method deleteNrSequenciaArquivoRemessa
     * 
     */
    public void deleteNrSequenciaArquivoRemessa()
    {
        this._has_nrSequenciaArquivoRemessa= false;
    } //-- void deleteNrSequenciaArquivoRemessa() 

    /**
     * Returns the value of field 'cdAgenciaDestino'.
     * 
     * @return int
     * @return the value of field 'cdAgenciaDestino'.
     */
    public int getCdAgenciaDestino()
    {
        return this._cdAgenciaDestino;
    } //-- int getCdAgenciaDestino() 

    /**
     * Returns the value of field 'cdAgenciaFinal'.
     * 
     * @return int
     * @return the value of field 'cdAgenciaFinal'.
     */
    public int getCdAgenciaFinal()
    {
        return this._cdAgenciaFinal;
    } //-- int getCdAgenciaFinal() 

    /**
     * Returns the value of field 'cdBancoDestino'.
     * 
     * @return int
     * @return the value of field 'cdBancoDestino'.
     */
    public int getCdBancoDestino()
    {
        return this._cdBancoDestino;
    } //-- int getCdBancoDestino() 

    /**
     * Returns the value of field 'cdBancoFinal'.
     * 
     * @return int
     * @return the value of field 'cdBancoFinal'.
     */
    public int getCdBancoFinal()
    {
        return this._cdBancoFinal;
    } //-- int getCdBancoFinal() 

    /**
     * Returns the value of field 'cdContaDestino'.
     * 
     * @return long
     * @return the value of field 'cdContaDestino'.
     */
    public long getCdContaDestino()
    {
        return this._cdContaDestino;
    } //-- long getCdContaDestino() 

    /**
     * Returns the value of field 'cdContaFinal'.
     * 
     * @return long
     * @return the value of field 'cdContaFinal'.
     */
    public long getCdContaFinal()
    {
        return this._cdContaFinal;
    } //-- long getCdContaFinal() 

    /**
     * Returns the value of field 'cdDigitoAgenciaDestino'.
     * 
     * @return int
     * @return the value of field 'cdDigitoAgenciaDestino'.
     */
    public int getCdDigitoAgenciaDestino()
    {
        return this._cdDigitoAgenciaDestino;
    } //-- int getCdDigitoAgenciaDestino() 

    /**
     * Returns the value of field 'cdDigitoAgenciaFinal'.
     * 
     * @return int
     * @return the value of field 'cdDigitoAgenciaFinal'.
     */
    public int getCdDigitoAgenciaFinal()
    {
        return this._cdDigitoAgenciaFinal;
    } //-- int getCdDigitoAgenciaFinal() 

    /**
     * Returns the value of field 'cdDigitoContaDestino'.
     * 
     * @return String
     * @return the value of field 'cdDigitoContaDestino'.
     */
    public java.lang.String getCdDigitoContaDestino()
    {
        return this._cdDigitoContaDestino;
    } //-- java.lang.String getCdDigitoContaDestino() 

    /**
     * Returns the value of field 'cdDigitoContaFinal'.
     * 
     * @return String
     * @return the value of field 'cdDigitoContaFinal'.
     */
    public java.lang.String getCdDigitoContaFinal()
    {
        return this._cdDigitoContaFinal;
    } //-- java.lang.String getCdDigitoContaFinal() 

    /**
     * Returns the value of field 'cdFavorecido'.
     * 
     * @return long
     * @return the value of field 'cdFavorecido'.
     */
    public long getCdFavorecido()
    {
        return this._cdFavorecido;
    } //-- long getCdFavorecido() 

    /**
     * Returns the value of field 'cdFluxoInclusao'.
     * 
     * @return String
     * @return the value of field 'cdFluxoInclusao'.
     */
    public java.lang.String getCdFluxoInclusao()
    {
        return this._cdFluxoInclusao;
    } //-- java.lang.String getCdFluxoInclusao() 

    /**
     * Returns the value of field 'cdFluxoManutencao'.
     * 
     * @return String
     * @return the value of field 'cdFluxoManutencao'.
     */
    public java.lang.String getCdFluxoManutencao()
    {
        return this._cdFluxoManutencao;
    } //-- java.lang.String getCdFluxoManutencao() 

    /**
     * Returns the value of field 'cdIndicadorEconomicoMoeda'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorEconomicoMoeda'.
     */
    public int getCdIndicadorEconomicoMoeda()
    {
        return this._cdIndicadorEconomicoMoeda;
    } //-- int getCdIndicadorEconomicoMoeda() 

    /**
     * Returns the value of field 'cdIndicadorModalidade'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorModalidade'.
     */
    public int getCdIndicadorModalidade()
    {
        return this._cdIndicadorModalidade;
    } //-- int getCdIndicadorModalidade() 

    /**
     * Returns the value of field 'cdListaDebito'.
     * 
     * @return long
     * @return the value of field 'cdListaDebito'.
     */
    public long getCdListaDebito()
    {
        return this._cdListaDebito;
    } //-- long getCdListaDebito() 

    /**
     * Returns the value of field 'cdMotivoSituacaoPagamento'.
     * 
     * @return int
     * @return the value of field 'cdMotivoSituacaoPagamento'.
     */
    public int getCdMotivoSituacaoPagamento()
    {
        return this._cdMotivoSituacaoPagamento;
    } //-- int getCdMotivoSituacaoPagamento() 

    /**
     * Returns the value of field 'cdSerieDocumento'.
     * 
     * @return String
     * @return the value of field 'cdSerieDocumento'.
     */
    public java.lang.String getCdSerieDocumento()
    {
        return this._cdSerieDocumento;
    } //-- java.lang.String getCdSerieDocumento() 

    /**
     * Returns the value of field 'cdSituacaoContrato'.
     * 
     * @return String
     * @return the value of field 'cdSituacaoContrato'.
     */
    public java.lang.String getCdSituacaoContrato()
    {
        return this._cdSituacaoContrato;
    } //-- java.lang.String getCdSituacaoContrato() 

    /**
     * Returns the value of field 'cdSituacaoOperacaoPagamento'.
     * 
     * @return int
     * @return the value of field 'cdSituacaoOperacaoPagamento'.
     */
    public int getCdSituacaoOperacaoPagamento()
    {
        return this._cdSituacaoOperacaoPagamento;
    } //-- int getCdSituacaoOperacaoPagamento() 

    /**
     * Returns the value of field
     * 'cdSituacaoTranferenciaAutomatica'.
     * 
     * @return String
     * @return the value of field 'cdSituacaoTranferenciaAutomatica'
     */
    public java.lang.String getCdSituacaoTranferenciaAutomatica()
    {
        return this._cdSituacaoTranferenciaAutomatica;
    } //-- java.lang.String getCdSituacaoTranferenciaAutomatica() 

    /**
     * Returns the value of field 'cdStatusTransmissao'.
     * 
     * @return int
     * @return the value of field 'cdStatusTransmissao'.
     */
    public int getCdStatusTransmissao()
    {
        return this._cdStatusTransmissao;
    } //-- int getCdStatusTransmissao() 

    /**
     * Returns the value of field 'cdTipoCanalInclusao'.
     * 
     * @return int
     * @return the value of field 'cdTipoCanalInclusao'.
     */
    public int getCdTipoCanalInclusao()
    {
        return this._cdTipoCanalInclusao;
    } //-- int getCdTipoCanalInclusao() 

    /**
     * Returns the value of field 'cdTipoCanalManutencao'.
     * 
     * @return int
     * @return the value of field 'cdTipoCanalManutencao'.
     */
    public int getCdTipoCanalManutencao()
    {
        return this._cdTipoCanalManutencao;
    } //-- int getCdTipoCanalManutencao() 

    /**
     * Returns the value of field 'cdTipoContaFavorecido'.
     * 
     * @return int
     * @return the value of field 'cdTipoContaFavorecido'.
     */
    public int getCdTipoContaFavorecido()
    {
        return this._cdTipoContaFavorecido;
    } //-- int getCdTipoContaFavorecido() 

    /**
     * Returns the value of field 'cdTipoDocumento'.
     * 
     * @return int
     * @return the value of field 'cdTipoDocumento'.
     */
    public int getCdTipoDocumento()
    {
        return this._cdTipoDocumento;
    } //-- int getCdTipoDocumento() 

    /**
     * Returns the value of field 'cdTipoInscricaoPagador'.
     * 
     * @return int
     * @return the value of field 'cdTipoInscricaoPagador'.
     */
    public int getCdTipoInscricaoPagador()
    {
        return this._cdTipoInscricaoPagador;
    } //-- int getCdTipoInscricaoPagador() 

    /**
     * Returns the value of field 'cdTipoManutencao'.
     * 
     * @return int
     * @return the value of field 'cdTipoManutencao'.
     */
    public int getCdTipoManutencao()
    {
        return this._cdTipoManutencao;
    } //-- int getCdTipoManutencao() 

    /**
     * Returns the value of field 'cdUsuarioInclusaoExterno'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioInclusaoExterno'.
     */
    public java.lang.String getCdUsuarioInclusaoExterno()
    {
        return this._cdUsuarioInclusaoExterno;
    } //-- java.lang.String getCdUsuarioInclusaoExterno() 

    /**
     * Returns the value of field 'cdUsuarioInclusaoInterno'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioInclusaoInterno'.
     */
    public java.lang.String getCdUsuarioInclusaoInterno()
    {
        return this._cdUsuarioInclusaoInterno;
    } //-- java.lang.String getCdUsuarioInclusaoInterno() 

    /**
     * Returns the value of field 'cdUsuarioManutencaoExterno'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioManutencaoExterno'.
     */
    public java.lang.String getCdUsuarioManutencaoExterno()
    {
        return this._cdUsuarioManutencaoExterno;
    } //-- java.lang.String getCdUsuarioManutencaoExterno() 

    /**
     * Returns the value of field 'cdUsuarioManutencaoInterno'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioManutencaoInterno'.
     */
    public java.lang.String getCdUsuarioManutencaoInterno()
    {
        return this._cdUsuarioManutencaoInterno;
    } //-- java.lang.String getCdUsuarioManutencaoInterno() 

    /**
     * Returns the value of field 'codMensagem'.
     * 
     * @return String
     * @return the value of field 'codMensagem'.
     */
    public java.lang.String getCodMensagem()
    {
        return this._codMensagem;
    } //-- java.lang.String getCodMensagem() 

    /**
     * Returns the value of field 'dsAgencia'.
     * 
     * @return String
     * @return the value of field 'dsAgencia'.
     */
    public java.lang.String getDsAgencia()
    {
        return this._dsAgencia;
    } //-- java.lang.String getDsAgencia() 

    /**
     * Returns the value of field 'dsAgenciaDebito'.
     * 
     * @return String
     * @return the value of field 'dsAgenciaDebito'.
     */
    public java.lang.String getDsAgenciaDebito()
    {
        return this._dsAgenciaDebito;
    } //-- java.lang.String getDsAgenciaDebito() 

    /**
     * Returns the value of field 'dsAgenciaDestino'.
     * 
     * @return String
     * @return the value of field 'dsAgenciaDestino'.
     */
    public java.lang.String getDsAgenciaDestino()
    {
        return this._dsAgenciaDestino;
    } //-- java.lang.String getDsAgenciaDestino() 

    /**
     * Returns the value of field 'dsAgenciaFavorecido'.
     * 
     * @return String
     * @return the value of field 'dsAgenciaFavorecido'.
     */
    public java.lang.String getDsAgenciaFavorecido()
    {
        return this._dsAgenciaFavorecido;
    } //-- java.lang.String getDsAgenciaFavorecido() 

    /**
     * Returns the value of field 'dsBanco'.
     * 
     * @return String
     * @return the value of field 'dsBanco'.
     */
    public java.lang.String getDsBanco()
    {
        return this._dsBanco;
    } //-- java.lang.String getDsBanco() 

    /**
     * Returns the value of field 'dsBancoDebito'.
     * 
     * @return String
     * @return the value of field 'dsBancoDebito'.
     */
    public java.lang.String getDsBancoDebito()
    {
        return this._dsBancoDebito;
    } //-- java.lang.String getDsBancoDebito() 

    /**
     * Returns the value of field 'dsBancoDestino'.
     * 
     * @return String
     * @return the value of field 'dsBancoDestino'.
     */
    public java.lang.String getDsBancoDestino()
    {
        return this._dsBancoDestino;
    } //-- java.lang.String getDsBancoDestino() 

    /**
     * Returns the value of field 'dsBancoFavorecido'.
     * 
     * @return String
     * @return the value of field 'dsBancoFavorecido'.
     */
    public java.lang.String getDsBancoFavorecido()
    {
        return this._dsBancoFavorecido;
    } //-- java.lang.String getDsBancoFavorecido() 

    /**
     * Returns the value of field 'dsCodigoTipo'.
     * 
     * @return String
     * @return the value of field 'dsCodigoTipo'.
     */
    public java.lang.String getDsCodigoTipo()
    {
        return this._dsCodigoTipo;
    } //-- java.lang.String getDsCodigoTipo() 

    /**
     * Returns the value of field 'dsContrato'.
     * 
     * @return String
     * @return the value of field 'dsContrato'.
     */
    public java.lang.String getDsContrato()
    {
        return this._dsContrato;
    } //-- java.lang.String getDsContrato() 

    /**
     * Returns the value of field 'dsMensagemPrimeiraLinha'.
     * 
     * @return String
     * @return the value of field 'dsMensagemPrimeiraLinha'.
     */
    public java.lang.String getDsMensagemPrimeiraLinha()
    {
        return this._dsMensagemPrimeiraLinha;
    } //-- java.lang.String getDsMensagemPrimeiraLinha() 

    /**
     * Returns the value of field 'dsMensagemSegundaLinha'.
     * 
     * @return String
     * @return the value of field 'dsMensagemSegundaLinha'.
     */
    public java.lang.String getDsMensagemSegundaLinha()
    {
        return this._dsMensagemSegundaLinha;
    } //-- java.lang.String getDsMensagemSegundaLinha() 

    /**
     * Returns the value of field 'dsMoeda'.
     * 
     * @return String
     * @return the value of field 'dsMoeda'.
     */
    public java.lang.String getDsMoeda()
    {
        return this._dsMoeda;
    } //-- java.lang.String getDsMoeda() 

    /**
     * Returns the value of field 'dsMotivoSituacao'.
     * 
     * @return String
     * @return the value of field 'dsMotivoSituacao'.
     */
    public java.lang.String getDsMotivoSituacao()
    {
        return this._dsMotivoSituacao;
    } //-- java.lang.String getDsMotivoSituacao() 

    /**
     * Returns the value of field 'dsPagamento'.
     * 
     * @return String
     * @return the value of field 'dsPagamento'.
     */
    public java.lang.String getDsPagamento()
    {
        return this._dsPagamento;
    } //-- java.lang.String getDsPagamento() 

    /**
     * Returns the value of field 'dsStatusTransmissao'.
     * 
     * @return String
     * @return the value of field 'dsStatusTransmissao'.
     */
    public java.lang.String getDsStatusTransmissao()
    {
        return this._dsStatusTransmissao;
    } //-- java.lang.String getDsStatusTransmissao() 

    /**
     * Returns the value of field 'dsTipoCanalInclusao'.
     * 
     * @return String
     * @return the value of field 'dsTipoCanalInclusao'.
     */
    public java.lang.String getDsTipoCanalInclusao()
    {
        return this._dsTipoCanalInclusao;
    } //-- java.lang.String getDsTipoCanalInclusao() 

    /**
     * Returns the value of field 'dsTipoCanalManutencao'.
     * 
     * @return String
     * @return the value of field 'dsTipoCanalManutencao'.
     */
    public java.lang.String getDsTipoCanalManutencao()
    {
        return this._dsTipoCanalManutencao;
    } //-- java.lang.String getDsTipoCanalManutencao() 

    /**
     * Returns the value of field 'dsTipoContaDebito'.
     * 
     * @return String
     * @return the value of field 'dsTipoContaDebito'.
     */
    public java.lang.String getDsTipoContaDebito()
    {
        return this._dsTipoContaDebito;
    } //-- java.lang.String getDsTipoContaDebito() 

    /**
     * Returns the value of field 'dsTipoContaDestino'.
     * 
     * @return String
     * @return the value of field 'dsTipoContaDestino'.
     */
    public java.lang.String getDsTipoContaDestino()
    {
        return this._dsTipoContaDestino;
    } //-- java.lang.String getDsTipoContaDestino() 

    /**
     * Returns the value of field 'dsTipoContaFavorecido'.
     * 
     * @return String
     * @return the value of field 'dsTipoContaFavorecido'.
     */
    public java.lang.String getDsTipoContaFavorecido()
    {
        return this._dsTipoContaFavorecido;
    } //-- java.lang.String getDsTipoContaFavorecido() 

    /**
     * Returns the value of field 'dsTipoDocumento'.
     * 
     * @return String
     * @return the value of field 'dsTipoDocumento'.
     */
    public java.lang.String getDsTipoDocumento()
    {
        return this._dsTipoDocumento;
    } //-- java.lang.String getDsTipoDocumento() 

    /**
     * Returns the value of field 'dsTipoManutencao'.
     * 
     * @return String
     * @return the value of field 'dsTipoManutencao'.
     */
    public java.lang.String getDsTipoManutencao()
    {
        return this._dsTipoManutencao;
    } //-- java.lang.String getDsTipoManutencao() 

    /**
     * Returns the value of field 'dsUsoEmpresa'.
     * 
     * @return String
     * @return the value of field 'dsUsoEmpresa'.
     */
    public java.lang.String getDsUsoEmpresa()
    {
        return this._dsUsoEmpresa;
    } //-- java.lang.String getDsUsoEmpresa() 

    /**
     * Returns the value of field 'dtAgendamento'.
     * 
     * @return String
     * @return the value of field 'dtAgendamento'.
     */
    public java.lang.String getDtAgendamento()
    {
        return this._dtAgendamento;
    } //-- java.lang.String getDtAgendamento() 

    /**
     * Returns the value of field 'dtEmissaoDocumento'.
     * 
     * @return String
     * @return the value of field 'dtEmissaoDocumento'.
     */
    public java.lang.String getDtEmissaoDocumento()
    {
        return this._dtEmissaoDocumento;
    } //-- java.lang.String getDtEmissaoDocumento() 

    /**
     * Returns the value of field 'dtHoraDevolucao'.
     * 
     * @return String
     * @return the value of field 'dtHoraDevolucao'.
     */
    public java.lang.String getDtHoraDevolucao()
    {
        return this._dtHoraDevolucao;
    } //-- java.lang.String getDtHoraDevolucao() 

    /**
     * Returns the value of field 'dtHoraTransmissao'.
     * 
     * @return String
     * @return the value of field 'dtHoraTransmissao'.
     */
    public java.lang.String getDtHoraTransmissao()
    {
        return this._dtHoraTransmissao;
    } //-- java.lang.String getDtHoraTransmissao() 

    /**
     * Returns the value of field 'dtInclusao'.
     * 
     * @return String
     * @return the value of field 'dtInclusao'.
     */
    public java.lang.String getDtInclusao()
    {
        return this._dtInclusao;
    } //-- java.lang.String getDtInclusao() 

    /**
     * Returns the value of field 'dtManutencao'.
     * 
     * @return String
     * @return the value of field 'dtManutencao'.
     */
    public java.lang.String getDtManutencao()
    {
        return this._dtManutencao;
    } //-- java.lang.String getDtManutencao() 

    /**
     * Returns the value of field 'dtNascimentoBeneficiario'.
     * 
     * @return String
     * @return the value of field 'dtNascimentoBeneficiario'.
     */
    public java.lang.String getDtNascimentoBeneficiario()
    {
        return this._dtNascimentoBeneficiario;
    } //-- java.lang.String getDtNascimentoBeneficiario() 

    /**
     * Returns the value of field 'dtVencimento'.
     * 
     * @return String
     * @return the value of field 'dtVencimento'.
     */
    public java.lang.String getDtVencimento()
    {
        return this._dtVencimento;
    } //-- java.lang.String getDtVencimento() 

    /**
     * Returns the value of field 'hrInclusao'.
     * 
     * @return String
     * @return the value of field 'hrInclusao'.
     */
    public java.lang.String getHrInclusao()
    {
        return this._hrInclusao;
    } //-- java.lang.String getHrInclusao() 

    /**
     * Returns the value of field 'hrManutencao'.
     * 
     * @return String
     * @return the value of field 'hrManutencao'.
     */
    public java.lang.String getHrManutencao()
    {
        return this._hrManutencao;
    } //-- java.lang.String getHrManutencao() 

    /**
     * Returns the value of field 'mensagem'.
     * 
     * @return String
     * @return the value of field 'mensagem'.
     */
    public java.lang.String getMensagem()
    {
        return this._mensagem;
    } //-- java.lang.String getMensagem() 

    /**
     * Returns the value of field 'nrDocumento'.
     * 
     * @return long
     * @return the value of field 'nrDocumento'.
     */
    public long getNrDocumento()
    {
        return this._nrDocumento;
    } //-- long getNrDocumento() 

    /**
     * Returns the value of field 'nrLoteArquivoRemessa'.
     * 
     * @return long
     * @return the value of field 'nrLoteArquivoRemessa'.
     */
    public long getNrLoteArquivoRemessa()
    {
        return this._nrLoteArquivoRemessa;
    } //-- long getNrLoteArquivoRemessa() 

    /**
     * Returns the value of field 'nrSequenciaArquivoRemessa'.
     * 
     * @return long
     * @return the value of field 'nrSequenciaArquivoRemessa'.
     */
    public long getNrSequenciaArquivoRemessa()
    {
        return this._nrSequenciaArquivoRemessa;
    } //-- long getNrSequenciaArquivoRemessa() 

    /**
     * Returns the value of field 'qtMoeda'.
     * 
     * @return BigDecimal
     * @return the value of field 'qtMoeda'.
     */
    public java.math.BigDecimal getQtMoeda()
    {
        return this._qtMoeda;
    } //-- java.math.BigDecimal getQtMoeda() 

    /**
     * Returns the value of field 'vlAbatimento'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlAbatimento'.
     */
    public java.math.BigDecimal getVlAbatimento()
    {
        return this._vlAbatimento;
    } //-- java.math.BigDecimal getVlAbatimento() 

    /**
     * Returns the value of field 'vlAcrescimo'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlAcrescimo'.
     */
    public java.math.BigDecimal getVlAcrescimo()
    {
        return this._vlAcrescimo;
    } //-- java.math.BigDecimal getVlAcrescimo() 

    /**
     * Returns the value of field 'vlDebitoAut'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlDebitoAut'.
     */
    public java.math.BigDecimal getVlDebitoAut()
    {
        return this._vlDebitoAut;
    } //-- java.math.BigDecimal getVlDebitoAut() 

    /**
     * Returns the value of field 'vlDeducao'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlDeducao'.
     */
    public java.math.BigDecimal getVlDeducao()
    {
        return this._vlDeducao;
    } //-- java.math.BigDecimal getVlDeducao() 

    /**
     * Returns the value of field 'vlDesconto'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlDesconto'.
     */
    public java.math.BigDecimal getVlDesconto()
    {
        return this._vlDesconto;
    } //-- java.math.BigDecimal getVlDesconto() 

    /**
     * Returns the value of field 'vlDocumento'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlDocumento'.
     */
    public java.math.BigDecimal getVlDocumento()
    {
        return this._vlDocumento;
    } //-- java.math.BigDecimal getVlDocumento() 

    /**
     * Returns the value of field 'vlEmpresa'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlEmpresa'.
     */
    public java.math.BigDecimal getVlEmpresa()
    {
        return this._vlEmpresa;
    } //-- java.math.BigDecimal getVlEmpresa() 

    /**
     * Returns the value of field 'vlImpostoRenda'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlImpostoRenda'.
     */
    public java.math.BigDecimal getVlImpostoRenda()
    {
        return this._vlImpostoRenda;
    } //-- java.math.BigDecimal getVlImpostoRenda() 

    /**
     * Returns the value of field 'vlInss'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlInss'.
     */
    public java.math.BigDecimal getVlInss()
    {
        return this._vlInss;
    } //-- java.math.BigDecimal getVlInss() 

    /**
     * Returns the value of field 'vlIof'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlIof'.
     */
    public java.math.BigDecimal getVlIof()
    {
        return this._vlIof;
    } //-- java.math.BigDecimal getVlIof() 

    /**
     * Returns the value of field 'vlIss'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlIss'.
     */
    public java.math.BigDecimal getVlIss()
    {
        return this._vlIss;
    } //-- java.math.BigDecimal getVlIss() 

    /**
     * Returns the value of field 'vlLeasing'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlLeasing'.
     */
    public java.math.BigDecimal getVlLeasing()
    {
        return this._vlLeasing;
    } //-- java.math.BigDecimal getVlLeasing() 

    /**
     * Returns the value of field 'vlMora'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlMora'.
     */
    public java.math.BigDecimal getVlMora()
    {
        return this._vlMora;
    } //-- java.math.BigDecimal getVlMora() 

    /**
     * Returns the value of field 'vlMulta'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlMulta'.
     */
    public java.math.BigDecimal getVlMulta()
    {
        return this._vlMulta;
    } //-- java.math.BigDecimal getVlMulta() 

    /**
     * Returns the value of field 'vlrAgendamento'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlrAgendamento'.
     */
    public java.math.BigDecimal getVlrAgendamento()
    {
        return this._vlrAgendamento;
    } //-- java.math.BigDecimal getVlrAgendamento() 

    /**
     * Returns the value of field 'vlrEfetivacao'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlrEfetivacao'.
     */
    public java.math.BigDecimal getVlrEfetivacao()
    {
        return this._vlrEfetivacao;
    } //-- java.math.BigDecimal getVlrEfetivacao() 

    /**
     * Method hasCdAgenciaDestino
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgenciaDestino()
    {
        return this._has_cdAgenciaDestino;
    } //-- boolean hasCdAgenciaDestino() 

    /**
     * Method hasCdAgenciaFinal
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgenciaFinal()
    {
        return this._has_cdAgenciaFinal;
    } //-- boolean hasCdAgenciaFinal() 

    /**
     * Method hasCdBancoDestino
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdBancoDestino()
    {
        return this._has_cdBancoDestino;
    } //-- boolean hasCdBancoDestino() 

    /**
     * Method hasCdBancoFinal
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdBancoFinal()
    {
        return this._has_cdBancoFinal;
    } //-- boolean hasCdBancoFinal() 

    /**
     * Method hasCdContaDestino
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdContaDestino()
    {
        return this._has_cdContaDestino;
    } //-- boolean hasCdContaDestino() 

    /**
     * Method hasCdContaFinal
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdContaFinal()
    {
        return this._has_cdContaFinal;
    } //-- boolean hasCdContaFinal() 

    /**
     * Method hasCdDigitoAgenciaDestino
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdDigitoAgenciaDestino()
    {
        return this._has_cdDigitoAgenciaDestino;
    } //-- boolean hasCdDigitoAgenciaDestino() 

    /**
     * Method hasCdDigitoAgenciaFinal
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdDigitoAgenciaFinal()
    {
        return this._has_cdDigitoAgenciaFinal;
    } //-- boolean hasCdDigitoAgenciaFinal() 

    /**
     * Method hasCdFavorecido
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFavorecido()
    {
        return this._has_cdFavorecido;
    } //-- boolean hasCdFavorecido() 

    /**
     * Method hasCdIndicadorEconomicoMoeda
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorEconomicoMoeda()
    {
        return this._has_cdIndicadorEconomicoMoeda;
    } //-- boolean hasCdIndicadorEconomicoMoeda() 

    /**
     * Method hasCdIndicadorModalidade
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorModalidade()
    {
        return this._has_cdIndicadorModalidade;
    } //-- boolean hasCdIndicadorModalidade() 

    /**
     * Method hasCdListaDebito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdListaDebito()
    {
        return this._has_cdListaDebito;
    } //-- boolean hasCdListaDebito() 

    /**
     * Method hasCdMotivoSituacaoPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMotivoSituacaoPagamento()
    {
        return this._has_cdMotivoSituacaoPagamento;
    } //-- boolean hasCdMotivoSituacaoPagamento() 

    /**
     * Method hasCdSituacaoOperacaoPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSituacaoOperacaoPagamento()
    {
        return this._has_cdSituacaoOperacaoPagamento;
    } //-- boolean hasCdSituacaoOperacaoPagamento() 

    /**
     * Method hasCdStatusTransmissao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdStatusTransmissao()
    {
        return this._has_cdStatusTransmissao;
    } //-- boolean hasCdStatusTransmissao() 

    /**
     * Method hasCdTipoCanalInclusao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoCanalInclusao()
    {
        return this._has_cdTipoCanalInclusao;
    } //-- boolean hasCdTipoCanalInclusao() 

    /**
     * Method hasCdTipoCanalManutencao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoCanalManutencao()
    {
        return this._has_cdTipoCanalManutencao;
    } //-- boolean hasCdTipoCanalManutencao() 

    /**
     * Method hasCdTipoContaFavorecido
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContaFavorecido()
    {
        return this._has_cdTipoContaFavorecido;
    } //-- boolean hasCdTipoContaFavorecido() 

    /**
     * Method hasCdTipoDocumento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoDocumento()
    {
        return this._has_cdTipoDocumento;
    } //-- boolean hasCdTipoDocumento() 

    /**
     * Method hasCdTipoInscricaoPagador
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoInscricaoPagador()
    {
        return this._has_cdTipoInscricaoPagador;
    } //-- boolean hasCdTipoInscricaoPagador() 

    /**
     * Method hasCdTipoManutencao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoManutencao()
    {
        return this._has_cdTipoManutencao;
    } //-- boolean hasCdTipoManutencao() 

    /**
     * Method hasNrDocumento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrDocumento()
    {
        return this._has_nrDocumento;
    } //-- boolean hasNrDocumento() 

    /**
     * Method hasNrLoteArquivoRemessa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrLoteArquivoRemessa()
    {
        return this._has_nrLoteArquivoRemessa;
    } //-- boolean hasNrLoteArquivoRemessa() 

    /**
     * Method hasNrSequenciaArquivoRemessa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaArquivoRemessa()
    {
        return this._has_nrSequenciaArquivoRemessa;
    } //-- boolean hasNrSequenciaArquivoRemessa() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdAgenciaDestino'.
     * 
     * @param cdAgenciaDestino the value of field 'cdAgenciaDestino'
     */
    public void setCdAgenciaDestino(int cdAgenciaDestino)
    {
        this._cdAgenciaDestino = cdAgenciaDestino;
        this._has_cdAgenciaDestino = true;
    } //-- void setCdAgenciaDestino(int) 

    /**
     * Sets the value of field 'cdAgenciaFinal'.
     * 
     * @param cdAgenciaFinal the value of field 'cdAgenciaFinal'.
     */
    public void setCdAgenciaFinal(int cdAgenciaFinal)
    {
        this._cdAgenciaFinal = cdAgenciaFinal;
        this._has_cdAgenciaFinal = true;
    } //-- void setCdAgenciaFinal(int) 

    /**
     * Sets the value of field 'cdBancoDestino'.
     * 
     * @param cdBancoDestino the value of field 'cdBancoDestino'.
     */
    public void setCdBancoDestino(int cdBancoDestino)
    {
        this._cdBancoDestino = cdBancoDestino;
        this._has_cdBancoDestino = true;
    } //-- void setCdBancoDestino(int) 

    /**
     * Sets the value of field 'cdBancoFinal'.
     * 
     * @param cdBancoFinal the value of field 'cdBancoFinal'.
     */
    public void setCdBancoFinal(int cdBancoFinal)
    {
        this._cdBancoFinal = cdBancoFinal;
        this._has_cdBancoFinal = true;
    } //-- void setCdBancoFinal(int) 

    /**
     * Sets the value of field 'cdContaDestino'.
     * 
     * @param cdContaDestino the value of field 'cdContaDestino'.
     */
    public void setCdContaDestino(long cdContaDestino)
    {
        this._cdContaDestino = cdContaDestino;
        this._has_cdContaDestino = true;
    } //-- void setCdContaDestino(long) 

    /**
     * Sets the value of field 'cdContaFinal'.
     * 
     * @param cdContaFinal the value of field 'cdContaFinal'.
     */
    public void setCdContaFinal(long cdContaFinal)
    {
        this._cdContaFinal = cdContaFinal;
        this._has_cdContaFinal = true;
    } //-- void setCdContaFinal(long) 

    /**
     * Sets the value of field 'cdDigitoAgenciaDestino'.
     * 
     * @param cdDigitoAgenciaDestino the value of field
     * 'cdDigitoAgenciaDestino'.
     */
    public void setCdDigitoAgenciaDestino(int cdDigitoAgenciaDestino)
    {
        this._cdDigitoAgenciaDestino = cdDigitoAgenciaDestino;
        this._has_cdDigitoAgenciaDestino = true;
    } //-- void setCdDigitoAgenciaDestino(int) 

    /**
     * Sets the value of field 'cdDigitoAgenciaFinal'.
     * 
     * @param cdDigitoAgenciaFinal the value of field
     * 'cdDigitoAgenciaFinal'.
     */
    public void setCdDigitoAgenciaFinal(int cdDigitoAgenciaFinal)
    {
        this._cdDigitoAgenciaFinal = cdDigitoAgenciaFinal;
        this._has_cdDigitoAgenciaFinal = true;
    } //-- void setCdDigitoAgenciaFinal(int) 

    /**
     * Sets the value of field 'cdDigitoContaDestino'.
     * 
     * @param cdDigitoContaDestino the value of field
     * 'cdDigitoContaDestino'.
     */
    public void setCdDigitoContaDestino(java.lang.String cdDigitoContaDestino)
    {
        this._cdDigitoContaDestino = cdDigitoContaDestino;
    } //-- void setCdDigitoContaDestino(java.lang.String) 

    /**
     * Sets the value of field 'cdDigitoContaFinal'.
     * 
     * @param cdDigitoContaFinal the value of field
     * 'cdDigitoContaFinal'.
     */
    public void setCdDigitoContaFinal(java.lang.String cdDigitoContaFinal)
    {
        this._cdDigitoContaFinal = cdDigitoContaFinal;
    } //-- void setCdDigitoContaFinal(java.lang.String) 

    /**
     * Sets the value of field 'cdFavorecido'.
     * 
     * @param cdFavorecido the value of field 'cdFavorecido'.
     */
    public void setCdFavorecido(long cdFavorecido)
    {
        this._cdFavorecido = cdFavorecido;
        this._has_cdFavorecido = true;
    } //-- void setCdFavorecido(long) 

    /**
     * Sets the value of field 'cdFluxoInclusao'.
     * 
     * @param cdFluxoInclusao the value of field 'cdFluxoInclusao'.
     */
    public void setCdFluxoInclusao(java.lang.String cdFluxoInclusao)
    {
        this._cdFluxoInclusao = cdFluxoInclusao;
    } //-- void setCdFluxoInclusao(java.lang.String) 

    /**
     * Sets the value of field 'cdFluxoManutencao'.
     * 
     * @param cdFluxoManutencao the value of field
     * 'cdFluxoManutencao'.
     */
    public void setCdFluxoManutencao(java.lang.String cdFluxoManutencao)
    {
        this._cdFluxoManutencao = cdFluxoManutencao;
    } //-- void setCdFluxoManutencao(java.lang.String) 

    /**
     * Sets the value of field 'cdIndicadorEconomicoMoeda'.
     * 
     * @param cdIndicadorEconomicoMoeda the value of field
     * 'cdIndicadorEconomicoMoeda'.
     */
    public void setCdIndicadorEconomicoMoeda(int cdIndicadorEconomicoMoeda)
    {
        this._cdIndicadorEconomicoMoeda = cdIndicadorEconomicoMoeda;
        this._has_cdIndicadorEconomicoMoeda = true;
    } //-- void setCdIndicadorEconomicoMoeda(int) 

    /**
     * Sets the value of field 'cdIndicadorModalidade'.
     * 
     * @param cdIndicadorModalidade the value of field
     * 'cdIndicadorModalidade'.
     */
    public void setCdIndicadorModalidade(int cdIndicadorModalidade)
    {
        this._cdIndicadorModalidade = cdIndicadorModalidade;
        this._has_cdIndicadorModalidade = true;
    } //-- void setCdIndicadorModalidade(int) 

    /**
     * Sets the value of field 'cdListaDebito'.
     * 
     * @param cdListaDebito the value of field 'cdListaDebito'.
     */
    public void setCdListaDebito(long cdListaDebito)
    {
        this._cdListaDebito = cdListaDebito;
        this._has_cdListaDebito = true;
    } //-- void setCdListaDebito(long) 

    /**
     * Sets the value of field 'cdMotivoSituacaoPagamento'.
     * 
     * @param cdMotivoSituacaoPagamento the value of field
     * 'cdMotivoSituacaoPagamento'.
     */
    public void setCdMotivoSituacaoPagamento(int cdMotivoSituacaoPagamento)
    {
        this._cdMotivoSituacaoPagamento = cdMotivoSituacaoPagamento;
        this._has_cdMotivoSituacaoPagamento = true;
    } //-- void setCdMotivoSituacaoPagamento(int) 

    /**
     * Sets the value of field 'cdSerieDocumento'.
     * 
     * @param cdSerieDocumento the value of field 'cdSerieDocumento'
     */
    public void setCdSerieDocumento(java.lang.String cdSerieDocumento)
    {
        this._cdSerieDocumento = cdSerieDocumento;
    } //-- void setCdSerieDocumento(java.lang.String) 

    /**
     * Sets the value of field 'cdSituacaoContrato'.
     * 
     * @param cdSituacaoContrato the value of field
     * 'cdSituacaoContrato'.
     */
    public void setCdSituacaoContrato(java.lang.String cdSituacaoContrato)
    {
        this._cdSituacaoContrato = cdSituacaoContrato;
    } //-- void setCdSituacaoContrato(java.lang.String) 

    /**
     * Sets the value of field 'cdSituacaoOperacaoPagamento'.
     * 
     * @param cdSituacaoOperacaoPagamento the value of field
     * 'cdSituacaoOperacaoPagamento'.
     */
    public void setCdSituacaoOperacaoPagamento(int cdSituacaoOperacaoPagamento)
    {
        this._cdSituacaoOperacaoPagamento = cdSituacaoOperacaoPagamento;
        this._has_cdSituacaoOperacaoPagamento = true;
    } //-- void setCdSituacaoOperacaoPagamento(int) 

    /**
     * Sets the value of field 'cdSituacaoTranferenciaAutomatica'.
     * 
     * @param cdSituacaoTranferenciaAutomatica the value of field
     * 'cdSituacaoTranferenciaAutomatica'.
     */
    public void setCdSituacaoTranferenciaAutomatica(java.lang.String cdSituacaoTranferenciaAutomatica)
    {
        this._cdSituacaoTranferenciaAutomatica = cdSituacaoTranferenciaAutomatica;
    } //-- void setCdSituacaoTranferenciaAutomatica(java.lang.String) 

    /**
     * Sets the value of field 'cdStatusTransmissao'.
     * 
     * @param cdStatusTransmissao the value of field
     * 'cdStatusTransmissao'.
     */
    public void setCdStatusTransmissao(int cdStatusTransmissao)
    {
        this._cdStatusTransmissao = cdStatusTransmissao;
        this._has_cdStatusTransmissao = true;
    } //-- void setCdStatusTransmissao(int) 

    /**
     * Sets the value of field 'cdTipoCanalInclusao'.
     * 
     * @param cdTipoCanalInclusao the value of field
     * 'cdTipoCanalInclusao'.
     */
    public void setCdTipoCanalInclusao(int cdTipoCanalInclusao)
    {
        this._cdTipoCanalInclusao = cdTipoCanalInclusao;
        this._has_cdTipoCanalInclusao = true;
    } //-- void setCdTipoCanalInclusao(int) 

    /**
     * Sets the value of field 'cdTipoCanalManutencao'.
     * 
     * @param cdTipoCanalManutencao the value of field
     * 'cdTipoCanalManutencao'.
     */
    public void setCdTipoCanalManutencao(int cdTipoCanalManutencao)
    {
        this._cdTipoCanalManutencao = cdTipoCanalManutencao;
        this._has_cdTipoCanalManutencao = true;
    } //-- void setCdTipoCanalManutencao(int) 

    /**
     * Sets the value of field 'cdTipoContaFavorecido'.
     * 
     * @param cdTipoContaFavorecido the value of field
     * 'cdTipoContaFavorecido'.
     */
    public void setCdTipoContaFavorecido(int cdTipoContaFavorecido)
    {
        this._cdTipoContaFavorecido = cdTipoContaFavorecido;
        this._has_cdTipoContaFavorecido = true;
    } //-- void setCdTipoContaFavorecido(int) 

    /**
     * Sets the value of field 'cdTipoDocumento'.
     * 
     * @param cdTipoDocumento the value of field 'cdTipoDocumento'.
     */
    public void setCdTipoDocumento(int cdTipoDocumento)
    {
        this._cdTipoDocumento = cdTipoDocumento;
        this._has_cdTipoDocumento = true;
    } //-- void setCdTipoDocumento(int) 

    /**
     * Sets the value of field 'cdTipoInscricaoPagador'.
     * 
     * @param cdTipoInscricaoPagador the value of field
     * 'cdTipoInscricaoPagador'.
     */
    public void setCdTipoInscricaoPagador(int cdTipoInscricaoPagador)
    {
        this._cdTipoInscricaoPagador = cdTipoInscricaoPagador;
        this._has_cdTipoInscricaoPagador = true;
    } //-- void setCdTipoInscricaoPagador(int) 

    /**
     * Sets the value of field 'cdTipoManutencao'.
     * 
     * @param cdTipoManutencao the value of field 'cdTipoManutencao'
     */
    public void setCdTipoManutencao(int cdTipoManutencao)
    {
        this._cdTipoManutencao = cdTipoManutencao;
        this._has_cdTipoManutencao = true;
    } //-- void setCdTipoManutencao(int) 

    /**
     * Sets the value of field 'cdUsuarioInclusaoExterno'.
     * 
     * @param cdUsuarioInclusaoExterno the value of field
     * 'cdUsuarioInclusaoExterno'.
     */
    public void setCdUsuarioInclusaoExterno(java.lang.String cdUsuarioInclusaoExterno)
    {
        this._cdUsuarioInclusaoExterno = cdUsuarioInclusaoExterno;
    } //-- void setCdUsuarioInclusaoExterno(java.lang.String) 

    /**
     * Sets the value of field 'cdUsuarioInclusaoInterno'.
     * 
     * @param cdUsuarioInclusaoInterno the value of field
     * 'cdUsuarioInclusaoInterno'.
     */
    public void setCdUsuarioInclusaoInterno(java.lang.String cdUsuarioInclusaoInterno)
    {
        this._cdUsuarioInclusaoInterno = cdUsuarioInclusaoInterno;
    } //-- void setCdUsuarioInclusaoInterno(java.lang.String) 

    /**
     * Sets the value of field 'cdUsuarioManutencaoExterno'.
     * 
     * @param cdUsuarioManutencaoExterno the value of field
     * 'cdUsuarioManutencaoExterno'.
     */
    public void setCdUsuarioManutencaoExterno(java.lang.String cdUsuarioManutencaoExterno)
    {
        this._cdUsuarioManutencaoExterno = cdUsuarioManutencaoExterno;
    } //-- void setCdUsuarioManutencaoExterno(java.lang.String) 

    /**
     * Sets the value of field 'cdUsuarioManutencaoInterno'.
     * 
     * @param cdUsuarioManutencaoInterno the value of field
     * 'cdUsuarioManutencaoInterno'.
     */
    public void setCdUsuarioManutencaoInterno(java.lang.String cdUsuarioManutencaoInterno)
    {
        this._cdUsuarioManutencaoInterno = cdUsuarioManutencaoInterno;
    } //-- void setCdUsuarioManutencaoInterno(java.lang.String) 

    /**
     * Sets the value of field 'codMensagem'.
     * 
     * @param codMensagem the value of field 'codMensagem'.
     */
    public void setCodMensagem(java.lang.String codMensagem)
    {
        this._codMensagem = codMensagem;
    } //-- void setCodMensagem(java.lang.String) 

    /**
     * Sets the value of field 'dsAgencia'.
     * 
     * @param dsAgencia the value of field 'dsAgencia'.
     */
    public void setDsAgencia(java.lang.String dsAgencia)
    {
        this._dsAgencia = dsAgencia;
    } //-- void setDsAgencia(java.lang.String) 

    /**
     * Sets the value of field 'dsAgenciaDebito'.
     * 
     * @param dsAgenciaDebito the value of field 'dsAgenciaDebito'.
     */
    public void setDsAgenciaDebito(java.lang.String dsAgenciaDebito)
    {
        this._dsAgenciaDebito = dsAgenciaDebito;
    } //-- void setDsAgenciaDebito(java.lang.String) 

    /**
     * Sets the value of field 'dsAgenciaDestino'.
     * 
     * @param dsAgenciaDestino the value of field 'dsAgenciaDestino'
     */
    public void setDsAgenciaDestino(java.lang.String dsAgenciaDestino)
    {
        this._dsAgenciaDestino = dsAgenciaDestino;
    } //-- void setDsAgenciaDestino(java.lang.String) 

    /**
     * Sets the value of field 'dsAgenciaFavorecido'.
     * 
     * @param dsAgenciaFavorecido the value of field
     * 'dsAgenciaFavorecido'.
     */
    public void setDsAgenciaFavorecido(java.lang.String dsAgenciaFavorecido)
    {
        this._dsAgenciaFavorecido = dsAgenciaFavorecido;
    } //-- void setDsAgenciaFavorecido(java.lang.String) 

    /**
     * Sets the value of field 'dsBanco'.
     * 
     * @param dsBanco the value of field 'dsBanco'.
     */
    public void setDsBanco(java.lang.String dsBanco)
    {
        this._dsBanco = dsBanco;
    } //-- void setDsBanco(java.lang.String) 

    /**
     * Sets the value of field 'dsBancoDebito'.
     * 
     * @param dsBancoDebito the value of field 'dsBancoDebito'.
     */
    public void setDsBancoDebito(java.lang.String dsBancoDebito)
    {
        this._dsBancoDebito = dsBancoDebito;
    } //-- void setDsBancoDebito(java.lang.String) 

    /**
     * Sets the value of field 'dsBancoDestino'.
     * 
     * @param dsBancoDestino the value of field 'dsBancoDestino'.
     */
    public void setDsBancoDestino(java.lang.String dsBancoDestino)
    {
        this._dsBancoDestino = dsBancoDestino;
    } //-- void setDsBancoDestino(java.lang.String) 

    /**
     * Sets the value of field 'dsBancoFavorecido'.
     * 
     * @param dsBancoFavorecido the value of field
     * 'dsBancoFavorecido'.
     */
    public void setDsBancoFavorecido(java.lang.String dsBancoFavorecido)
    {
        this._dsBancoFavorecido = dsBancoFavorecido;
    } //-- void setDsBancoFavorecido(java.lang.String) 

    /**
     * Sets the value of field 'dsCodigoTipo'.
     * 
     * @param dsCodigoTipo the value of field 'dsCodigoTipo'.
     */
    public void setDsCodigoTipo(java.lang.String dsCodigoTipo)
    {
        this._dsCodigoTipo = dsCodigoTipo;
    } //-- void setDsCodigoTipo(java.lang.String) 

    /**
     * Sets the value of field 'dsContrato'.
     * 
     * @param dsContrato the value of field 'dsContrato'.
     */
    public void setDsContrato(java.lang.String dsContrato)
    {
        this._dsContrato = dsContrato;
    } //-- void setDsContrato(java.lang.String) 

    /**
     * Sets the value of field 'dsMensagemPrimeiraLinha'.
     * 
     * @param dsMensagemPrimeiraLinha the value of field
     * 'dsMensagemPrimeiraLinha'.
     */
    public void setDsMensagemPrimeiraLinha(java.lang.String dsMensagemPrimeiraLinha)
    {
        this._dsMensagemPrimeiraLinha = dsMensagemPrimeiraLinha;
    } //-- void setDsMensagemPrimeiraLinha(java.lang.String) 

    /**
     * Sets the value of field 'dsMensagemSegundaLinha'.
     * 
     * @param dsMensagemSegundaLinha the value of field
     * 'dsMensagemSegundaLinha'.
     */
    public void setDsMensagemSegundaLinha(java.lang.String dsMensagemSegundaLinha)
    {
        this._dsMensagemSegundaLinha = dsMensagemSegundaLinha;
    } //-- void setDsMensagemSegundaLinha(java.lang.String) 

    /**
     * Sets the value of field 'dsMoeda'.
     * 
     * @param dsMoeda the value of field 'dsMoeda'.
     */
    public void setDsMoeda(java.lang.String dsMoeda)
    {
        this._dsMoeda = dsMoeda;
    } //-- void setDsMoeda(java.lang.String) 

    /**
     * Sets the value of field 'dsMotivoSituacao'.
     * 
     * @param dsMotivoSituacao the value of field 'dsMotivoSituacao'
     */
    public void setDsMotivoSituacao(java.lang.String dsMotivoSituacao)
    {
        this._dsMotivoSituacao = dsMotivoSituacao;
    } //-- void setDsMotivoSituacao(java.lang.String) 

    /**
     * Sets the value of field 'dsPagamento'.
     * 
     * @param dsPagamento the value of field 'dsPagamento'.
     */
    public void setDsPagamento(java.lang.String dsPagamento)
    {
        this._dsPagamento = dsPagamento;
    } //-- void setDsPagamento(java.lang.String) 

    /**
     * Sets the value of field 'dsStatusTransmissao'.
     * 
     * @param dsStatusTransmissao the value of field
     * 'dsStatusTransmissao'.
     */
    public void setDsStatusTransmissao(java.lang.String dsStatusTransmissao)
    {
        this._dsStatusTransmissao = dsStatusTransmissao;
    } //-- void setDsStatusTransmissao(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoCanalInclusao'.
     * 
     * @param dsTipoCanalInclusao the value of field
     * 'dsTipoCanalInclusao'.
     */
    public void setDsTipoCanalInclusao(java.lang.String dsTipoCanalInclusao)
    {
        this._dsTipoCanalInclusao = dsTipoCanalInclusao;
    } //-- void setDsTipoCanalInclusao(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoCanalManutencao'.
     * 
     * @param dsTipoCanalManutencao the value of field
     * 'dsTipoCanalManutencao'.
     */
    public void setDsTipoCanalManutencao(java.lang.String dsTipoCanalManutencao)
    {
        this._dsTipoCanalManutencao = dsTipoCanalManutencao;
    } //-- void setDsTipoCanalManutencao(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoContaDebito'.
     * 
     * @param dsTipoContaDebito the value of field
     * 'dsTipoContaDebito'.
     */
    public void setDsTipoContaDebito(java.lang.String dsTipoContaDebito)
    {
        this._dsTipoContaDebito = dsTipoContaDebito;
    } //-- void setDsTipoContaDebito(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoContaDestino'.
     * 
     * @param dsTipoContaDestino the value of field
     * 'dsTipoContaDestino'.
     */
    public void setDsTipoContaDestino(java.lang.String dsTipoContaDestino)
    {
        this._dsTipoContaDestino = dsTipoContaDestino;
    } //-- void setDsTipoContaDestino(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoContaFavorecido'.
     * 
     * @param dsTipoContaFavorecido the value of field
     * 'dsTipoContaFavorecido'.
     */
    public void setDsTipoContaFavorecido(java.lang.String dsTipoContaFavorecido)
    {
        this._dsTipoContaFavorecido = dsTipoContaFavorecido;
    } //-- void setDsTipoContaFavorecido(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoDocumento'.
     * 
     * @param dsTipoDocumento the value of field 'dsTipoDocumento'.
     */
    public void setDsTipoDocumento(java.lang.String dsTipoDocumento)
    {
        this._dsTipoDocumento = dsTipoDocumento;
    } //-- void setDsTipoDocumento(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoManutencao'.
     * 
     * @param dsTipoManutencao the value of field 'dsTipoManutencao'
     */
    public void setDsTipoManutencao(java.lang.String dsTipoManutencao)
    {
        this._dsTipoManutencao = dsTipoManutencao;
    } //-- void setDsTipoManutencao(java.lang.String) 

    /**
     * Sets the value of field 'dsUsoEmpresa'.
     * 
     * @param dsUsoEmpresa the value of field 'dsUsoEmpresa'.
     */
    public void setDsUsoEmpresa(java.lang.String dsUsoEmpresa)
    {
        this._dsUsoEmpresa = dsUsoEmpresa;
    } //-- void setDsUsoEmpresa(java.lang.String) 

    /**
     * Sets the value of field 'dtAgendamento'.
     * 
     * @param dtAgendamento the value of field 'dtAgendamento'.
     */
    public void setDtAgendamento(java.lang.String dtAgendamento)
    {
        this._dtAgendamento = dtAgendamento;
    } //-- void setDtAgendamento(java.lang.String) 

    /**
     * Sets the value of field 'dtEmissaoDocumento'.
     * 
     * @param dtEmissaoDocumento the value of field
     * 'dtEmissaoDocumento'.
     */
    public void setDtEmissaoDocumento(java.lang.String dtEmissaoDocumento)
    {
        this._dtEmissaoDocumento = dtEmissaoDocumento;
    } //-- void setDtEmissaoDocumento(java.lang.String) 

    /**
     * Sets the value of field 'dtHoraDevolucao'.
     * 
     * @param dtHoraDevolucao the value of field 'dtHoraDevolucao'.
     */
    public void setDtHoraDevolucao(java.lang.String dtHoraDevolucao)
    {
        this._dtHoraDevolucao = dtHoraDevolucao;
    } //-- void setDtHoraDevolucao(java.lang.String) 

    /**
     * Sets the value of field 'dtHoraTransmissao'.
     * 
     * @param dtHoraTransmissao the value of field
     * 'dtHoraTransmissao'.
     */
    public void setDtHoraTransmissao(java.lang.String dtHoraTransmissao)
    {
        this._dtHoraTransmissao = dtHoraTransmissao;
    } //-- void setDtHoraTransmissao(java.lang.String) 

    /**
     * Sets the value of field 'dtInclusao'.
     * 
     * @param dtInclusao the value of field 'dtInclusao'.
     */
    public void setDtInclusao(java.lang.String dtInclusao)
    {
        this._dtInclusao = dtInclusao;
    } //-- void setDtInclusao(java.lang.String) 

    /**
     * Sets the value of field 'dtManutencao'.
     * 
     * @param dtManutencao the value of field 'dtManutencao'.
     */
    public void setDtManutencao(java.lang.String dtManutencao)
    {
        this._dtManutencao = dtManutencao;
    } //-- void setDtManutencao(java.lang.String) 

    /**
     * Sets the value of field 'dtNascimentoBeneficiario'.
     * 
     * @param dtNascimentoBeneficiario the value of field
     * 'dtNascimentoBeneficiario'.
     */
    public void setDtNascimentoBeneficiario(java.lang.String dtNascimentoBeneficiario)
    {
        this._dtNascimentoBeneficiario = dtNascimentoBeneficiario;
    } //-- void setDtNascimentoBeneficiario(java.lang.String) 

    /**
     * Sets the value of field 'dtVencimento'.
     * 
     * @param dtVencimento the value of field 'dtVencimento'.
     */
    public void setDtVencimento(java.lang.String dtVencimento)
    {
        this._dtVencimento = dtVencimento;
    } //-- void setDtVencimento(java.lang.String) 

    /**
     * Sets the value of field 'hrInclusao'.
     * 
     * @param hrInclusao the value of field 'hrInclusao'.
     */
    public void setHrInclusao(java.lang.String hrInclusao)
    {
        this._hrInclusao = hrInclusao;
    } //-- void setHrInclusao(java.lang.String) 

    /**
     * Sets the value of field 'hrManutencao'.
     * 
     * @param hrManutencao the value of field 'hrManutencao'.
     */
    public void setHrManutencao(java.lang.String hrManutencao)
    {
        this._hrManutencao = hrManutencao;
    } //-- void setHrManutencao(java.lang.String) 

    /**
     * Sets the value of field 'mensagem'.
     * 
     * @param mensagem the value of field 'mensagem'.
     */
    public void setMensagem(java.lang.String mensagem)
    {
        this._mensagem = mensagem;
    } //-- void setMensagem(java.lang.String) 

    /**
     * Sets the value of field 'nrDocumento'.
     * 
     * @param nrDocumento the value of field 'nrDocumento'.
     */
    public void setNrDocumento(long nrDocumento)
    {
        this._nrDocumento = nrDocumento;
        this._has_nrDocumento = true;
    } //-- void setNrDocumento(long) 

    /**
     * Sets the value of field 'nrLoteArquivoRemessa'.
     * 
     * @param nrLoteArquivoRemessa the value of field
     * 'nrLoteArquivoRemessa'.
     */
    public void setNrLoteArquivoRemessa(long nrLoteArquivoRemessa)
    {
        this._nrLoteArquivoRemessa = nrLoteArquivoRemessa;
        this._has_nrLoteArquivoRemessa = true;
    } //-- void setNrLoteArquivoRemessa(long) 

    /**
     * Sets the value of field 'nrSequenciaArquivoRemessa'.
     * 
     * @param nrSequenciaArquivoRemessa the value of field
     * 'nrSequenciaArquivoRemessa'.
     */
    public void setNrSequenciaArquivoRemessa(long nrSequenciaArquivoRemessa)
    {
        this._nrSequenciaArquivoRemessa = nrSequenciaArquivoRemessa;
        this._has_nrSequenciaArquivoRemessa = true;
    } //-- void setNrSequenciaArquivoRemessa(long) 

    /**
     * Sets the value of field 'qtMoeda'.
     * 
     * @param qtMoeda the value of field 'qtMoeda'.
     */
    public void setQtMoeda(java.math.BigDecimal qtMoeda)
    {
        this._qtMoeda = qtMoeda;
    } //-- void setQtMoeda(java.math.BigDecimal) 

    /**
     * Sets the value of field 'vlAbatimento'.
     * 
     * @param vlAbatimento the value of field 'vlAbatimento'.
     */
    public void setVlAbatimento(java.math.BigDecimal vlAbatimento)
    {
        this._vlAbatimento = vlAbatimento;
    } //-- void setVlAbatimento(java.math.BigDecimal) 

    /**
     * Sets the value of field 'vlAcrescimo'.
     * 
     * @param vlAcrescimo the value of field 'vlAcrescimo'.
     */
    public void setVlAcrescimo(java.math.BigDecimal vlAcrescimo)
    {
        this._vlAcrescimo = vlAcrescimo;
    } //-- void setVlAcrescimo(java.math.BigDecimal) 

    /**
     * Sets the value of field 'vlDebitoAut'.
     * 
     * @param vlDebitoAut the value of field 'vlDebitoAut'.
     */
    public void setVlDebitoAut(java.math.BigDecimal vlDebitoAut)
    {
        this._vlDebitoAut = vlDebitoAut;
    } //-- void setVlDebitoAut(java.math.BigDecimal) 

    /**
     * Sets the value of field 'vlDeducao'.
     * 
     * @param vlDeducao the value of field 'vlDeducao'.
     */
    public void setVlDeducao(java.math.BigDecimal vlDeducao)
    {
        this._vlDeducao = vlDeducao;
    } //-- void setVlDeducao(java.math.BigDecimal) 

    /**
     * Sets the value of field 'vlDesconto'.
     * 
     * @param vlDesconto the value of field 'vlDesconto'.
     */
    public void setVlDesconto(java.math.BigDecimal vlDesconto)
    {
        this._vlDesconto = vlDesconto;
    } //-- void setVlDesconto(java.math.BigDecimal) 

    /**
     * Sets the value of field 'vlDocumento'.
     * 
     * @param vlDocumento the value of field 'vlDocumento'.
     */
    public void setVlDocumento(java.math.BigDecimal vlDocumento)
    {
        this._vlDocumento = vlDocumento;
    } //-- void setVlDocumento(java.math.BigDecimal) 

    /**
     * Sets the value of field 'vlEmpresa'.
     * 
     * @param vlEmpresa the value of field 'vlEmpresa'.
     */
    public void setVlEmpresa(java.math.BigDecimal vlEmpresa)
    {
        this._vlEmpresa = vlEmpresa;
    } //-- void setVlEmpresa(java.math.BigDecimal) 

    /**
     * Sets the value of field 'vlImpostoRenda'.
     * 
     * @param vlImpostoRenda the value of field 'vlImpostoRenda'.
     */
    public void setVlImpostoRenda(java.math.BigDecimal vlImpostoRenda)
    {
        this._vlImpostoRenda = vlImpostoRenda;
    } //-- void setVlImpostoRenda(java.math.BigDecimal) 

    /**
     * Sets the value of field 'vlInss'.
     * 
     * @param vlInss the value of field 'vlInss'.
     */
    public void setVlInss(java.math.BigDecimal vlInss)
    {
        this._vlInss = vlInss;
    } //-- void setVlInss(java.math.BigDecimal) 

    /**
     * Sets the value of field 'vlIof'.
     * 
     * @param vlIof the value of field 'vlIof'.
     */
    public void setVlIof(java.math.BigDecimal vlIof)
    {
        this._vlIof = vlIof;
    } //-- void setVlIof(java.math.BigDecimal) 

    /**
     * Sets the value of field 'vlIss'.
     * 
     * @param vlIss the value of field 'vlIss'.
     */
    public void setVlIss(java.math.BigDecimal vlIss)
    {
        this._vlIss = vlIss;
    } //-- void setVlIss(java.math.BigDecimal) 

    /**
     * Sets the value of field 'vlLeasing'.
     * 
     * @param vlLeasing the value of field 'vlLeasing'.
     */
    public void setVlLeasing(java.math.BigDecimal vlLeasing)
    {
        this._vlLeasing = vlLeasing;
    } //-- void setVlLeasing(java.math.BigDecimal) 

    /**
     * Sets the value of field 'vlMora'.
     * 
     * @param vlMora the value of field 'vlMora'.
     */
    public void setVlMora(java.math.BigDecimal vlMora)
    {
        this._vlMora = vlMora;
    } //-- void setVlMora(java.math.BigDecimal) 

    /**
     * Sets the value of field 'vlMulta'.
     * 
     * @param vlMulta the value of field 'vlMulta'.
     */
    public void setVlMulta(java.math.BigDecimal vlMulta)
    {
        this._vlMulta = vlMulta;
    } //-- void setVlMulta(java.math.BigDecimal) 

    /**
     * Sets the value of field 'vlrAgendamento'.
     * 
     * @param vlrAgendamento the value of field 'vlrAgendamento'.
     */
    public void setVlrAgendamento(java.math.BigDecimal vlrAgendamento)
    {
        this._vlrAgendamento = vlrAgendamento;
    } //-- void setVlrAgendamento(java.math.BigDecimal) 

    /**
     * Sets the value of field 'vlrEfetivacao'.
     * 
     * @param vlrEfetivacao the value of field 'vlrEfetivacao'.
     */
    public void setVlrEfetivacao(java.math.BigDecimal vlrEfetivacao)
    {
        this._vlrEfetivacao = vlrEfetivacao;
    } //-- void setVlrEfetivacao(java.math.BigDecimal) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return DetalharManPagtoCreditoContaResponse
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.detalharmanpagtocreditoconta.response.DetalharManPagtoCreditoContaResponse unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.detalharmanpagtocreditoconta.response.DetalharManPagtoCreditoContaResponse) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.detalharmanpagtocreditoconta.response.DetalharManPagtoCreditoContaResponse.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharmanpagtocreditoconta.response.DetalharManPagtoCreditoContaResponse unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
