/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.bloqueardesbloquearcontacontrato.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;

/**
 * Class BloquearDesbloquearContaContratoRequest.
 * 
 * @version $Revision$ $Date$
 */
public class BloquearDesbloquearContaContratoRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdPessoaJuridicaContrato
     */
    private long _cdPessoaJuridicaContrato = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridicaContrato
     */
    private boolean _has_cdPessoaJuridicaContrato;

    /**
     * Field _cdTipoContratoNegocio
     */
    private int _cdTipoContratoNegocio = 0;

    /**
     * keeps track of state for field: _cdTipoContratoNegocio
     */
    private boolean _has_cdTipoContratoNegocio;

    /**
     * Field _nrSequenciaContratoNegocio
     */
    private long _nrSequenciaContratoNegocio = 0;

    /**
     * keeps track of state for field: _nrSequenciaContratoNegocio
     */
    private boolean _has_nrSequenciaContratoNegocio;

    /**
     * Field _cdBanco
     */
    private int _cdBanco = 0;

    /**
     * keeps track of state for field: _cdBanco
     */
    private boolean _has_cdBanco;

    /**
     * Field _cdTipoContratoConta
     */
    private int _cdTipoContratoConta = 0;

    /**
     * keeps track of state for field: _cdTipoContratoConta
     */
    private boolean _has_cdTipoContratoConta;

    /**
     * Field _cdComercialAgenciaContabil
     */
    private int _cdComercialAgenciaContabil = 0;

    /**
     * keeps track of state for field: _cdComercialAgenciaContabil
     */
    private boolean _has_cdComercialAgenciaContabil;

    /**
     * Field _cdConta
     */
    private long _cdConta = 0;

    /**
     * keeps track of state for field: _cdConta
     */
    private boolean _has_cdConta;

    /**
     * Field _cdDigitoConta
     */
    private int _cdDigitoConta = 0;

    /**
     * keeps track of state for field: _cdDigitoConta
     */
    private boolean _has_cdDigitoConta;

    /**
     * Field _cdSituacaoContrato
     */
    private int _cdSituacaoContrato = 0;

    /**
     * keeps track of state for field: _cdSituacaoContrato
     */
    private boolean _has_cdSituacaoContrato;

    /**
     * Field _cdPessoaJuridicaVinculo
     */
    private long _cdPessoaJuridicaVinculo = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridicaVinculo
     */
    private boolean _has_cdPessoaJuridicaVinculo;

    /**
     * Field _cdTipoContratoVinculo
     */
    private int _cdTipoContratoVinculo = 0;

    /**
     * keeps track of state for field: _cdTipoContratoVinculo
     */
    private boolean _has_cdTipoContratoVinculo;

    /**
     * Field _nrSequenciaContratoVinculo
     */
    private long _nrSequenciaContratoVinculo = 0;

    /**
     * keeps track of state for field: _nrSequenciaContratoVinculo
     */
    private boolean _has_nrSequenciaContratoVinculo;

    /**
     * Field _cdTipoVinculoContrato
     */
    private int _cdTipoVinculoContrato = 0;

    /**
     * keeps track of state for field: _cdTipoVinculoContrato
     */
    private boolean _has_cdTipoVinculoContrato;

    /**
     * Field _cdMotivoBloqueioConta
     */
    private int _cdMotivoBloqueioConta = 0;

    /**
     * keeps track of state for field: _cdMotivoBloqueioConta
     */
    private boolean _has_cdMotivoBloqueioConta;

    /**
     * Field _cdTipoAcao
     */
    private java.lang.String _cdTipoAcao;

    /**
     * Field _dtInicioVinculacaoContrato
     */
    private java.lang.String _dtInicioVinculacaoContrato;


      //----------------/
     //- Constructors -/
    //----------------/

    public BloquearDesbloquearContaContratoRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.bloqueardesbloquearcontacontrato.request.BloquearDesbloquearContaContratoRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdBanco
     * 
     */
    public void deleteCdBanco()
    {
        this._has_cdBanco= false;
    } //-- void deleteCdBanco() 

    /**
     * Method deleteCdComercialAgenciaContabil
     * 
     */
    public void deleteCdComercialAgenciaContabil()
    {
        this._has_cdComercialAgenciaContabil= false;
    } //-- void deleteCdComercialAgenciaContabil() 

    /**
     * Method deleteCdConta
     * 
     */
    public void deleteCdConta()
    {
        this._has_cdConta= false;
    } //-- void deleteCdConta() 

    /**
     * Method deleteCdDigitoConta
     * 
     */
    public void deleteCdDigitoConta()
    {
        this._has_cdDigitoConta= false;
    } //-- void deleteCdDigitoConta() 

    /**
     * Method deleteCdMotivoBloqueioConta
     * 
     */
    public void deleteCdMotivoBloqueioConta()
    {
        this._has_cdMotivoBloqueioConta= false;
    } //-- void deleteCdMotivoBloqueioConta() 

    /**
     * Method deleteCdPessoaJuridicaContrato
     * 
     */
    public void deleteCdPessoaJuridicaContrato()
    {
        this._has_cdPessoaJuridicaContrato= false;
    } //-- void deleteCdPessoaJuridicaContrato() 

    /**
     * Method deleteCdPessoaJuridicaVinculo
     * 
     */
    public void deleteCdPessoaJuridicaVinculo()
    {
        this._has_cdPessoaJuridicaVinculo= false;
    } //-- void deleteCdPessoaJuridicaVinculo() 

    /**
     * Method deleteCdSituacaoContrato
     * 
     */
    public void deleteCdSituacaoContrato()
    {
        this._has_cdSituacaoContrato= false;
    } //-- void deleteCdSituacaoContrato() 

    /**
     * Method deleteCdTipoContratoConta
     * 
     */
    public void deleteCdTipoContratoConta()
    {
        this._has_cdTipoContratoConta= false;
    } //-- void deleteCdTipoContratoConta() 

    /**
     * Method deleteCdTipoContratoNegocio
     * 
     */
    public void deleteCdTipoContratoNegocio()
    {
        this._has_cdTipoContratoNegocio= false;
    } //-- void deleteCdTipoContratoNegocio() 

    /**
     * Method deleteCdTipoContratoVinculo
     * 
     */
    public void deleteCdTipoContratoVinculo()
    {
        this._has_cdTipoContratoVinculo= false;
    } //-- void deleteCdTipoContratoVinculo() 

    /**
     * Method deleteCdTipoVinculoContrato
     * 
     */
    public void deleteCdTipoVinculoContrato()
    {
        this._has_cdTipoVinculoContrato= false;
    } //-- void deleteCdTipoVinculoContrato() 

    /**
     * Method deleteNrSequenciaContratoNegocio
     * 
     */
    public void deleteNrSequenciaContratoNegocio()
    {
        this._has_nrSequenciaContratoNegocio= false;
    } //-- void deleteNrSequenciaContratoNegocio() 

    /**
     * Method deleteNrSequenciaContratoVinculo
     * 
     */
    public void deleteNrSequenciaContratoVinculo()
    {
        this._has_nrSequenciaContratoVinculo= false;
    } //-- void deleteNrSequenciaContratoVinculo() 

    /**
     * Returns the value of field 'cdBanco'.
     * 
     * @return int
     * @return the value of field 'cdBanco'.
     */
    public int getCdBanco()
    {
        return this._cdBanco;
    } //-- int getCdBanco() 

    /**
     * Returns the value of field 'cdComercialAgenciaContabil'.
     * 
     * @return int
     * @return the value of field 'cdComercialAgenciaContabil'.
     */
    public int getCdComercialAgenciaContabil()
    {
        return this._cdComercialAgenciaContabil;
    } //-- int getCdComercialAgenciaContabil() 

    /**
     * Returns the value of field 'cdConta'.
     * 
     * @return long
     * @return the value of field 'cdConta'.
     */
    public long getCdConta()
    {
        return this._cdConta;
    } //-- long getCdConta() 

    /**
     * Returns the value of field 'cdDigitoConta'.
     * 
     * @return int
     * @return the value of field 'cdDigitoConta'.
     */
    public int getCdDigitoConta()
    {
        return this._cdDigitoConta;
    } //-- int getCdDigitoConta() 

    /**
     * Returns the value of field 'cdMotivoBloqueioConta'.
     * 
     * @return int
     * @return the value of field 'cdMotivoBloqueioConta'.
     */
    public int getCdMotivoBloqueioConta()
    {
        return this._cdMotivoBloqueioConta;
    } //-- int getCdMotivoBloqueioConta() 

    /**
     * Returns the value of field 'cdPessoaJuridicaContrato'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridicaContrato'.
     */
    public long getCdPessoaJuridicaContrato()
    {
        return this._cdPessoaJuridicaContrato;
    } //-- long getCdPessoaJuridicaContrato() 

    /**
     * Returns the value of field 'cdPessoaJuridicaVinculo'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridicaVinculo'.
     */
    public long getCdPessoaJuridicaVinculo()
    {
        return this._cdPessoaJuridicaVinculo;
    } //-- long getCdPessoaJuridicaVinculo() 

    /**
     * Returns the value of field 'cdSituacaoContrato'.
     * 
     * @return int
     * @return the value of field 'cdSituacaoContrato'.
     */
    public int getCdSituacaoContrato()
    {
        return this._cdSituacaoContrato;
    } //-- int getCdSituacaoContrato() 

    /**
     * Returns the value of field 'cdTipoAcao'.
     * 
     * @return String
     * @return the value of field 'cdTipoAcao'.
     */
    public java.lang.String getCdTipoAcao()
    {
        return this._cdTipoAcao;
    } //-- java.lang.String getCdTipoAcao() 

    /**
     * Returns the value of field 'cdTipoContratoConta'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoConta'.
     */
    public int getCdTipoContratoConta()
    {
        return this._cdTipoContratoConta;
    } //-- int getCdTipoContratoConta() 

    /**
     * Returns the value of field 'cdTipoContratoNegocio'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoNegocio'.
     */
    public int getCdTipoContratoNegocio()
    {
        return this._cdTipoContratoNegocio;
    } //-- int getCdTipoContratoNegocio() 

    /**
     * Returns the value of field 'cdTipoContratoVinculo'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoVinculo'.
     */
    public int getCdTipoContratoVinculo()
    {
        return this._cdTipoContratoVinculo;
    } //-- int getCdTipoContratoVinculo() 

    /**
     * Returns the value of field 'cdTipoVinculoContrato'.
     * 
     * @return int
     * @return the value of field 'cdTipoVinculoContrato'.
     */
    public int getCdTipoVinculoContrato()
    {
        return this._cdTipoVinculoContrato;
    } //-- int getCdTipoVinculoContrato() 

    /**
     * Returns the value of field 'dtInicioVinculacaoContrato'.
     * 
     * @return String
     * @return the value of field 'dtInicioVinculacaoContrato'.
     */
    public java.lang.String getDtInicioVinculacaoContrato()
    {
        return this._dtInicioVinculacaoContrato;
    } //-- java.lang.String getDtInicioVinculacaoContrato() 

    /**
     * Returns the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @return long
     * @return the value of field 'nrSequenciaContratoNegocio'.
     */
    public long getNrSequenciaContratoNegocio()
    {
        return this._nrSequenciaContratoNegocio;
    } //-- long getNrSequenciaContratoNegocio() 

    /**
     * Returns the value of field 'nrSequenciaContratoVinculo'.
     * 
     * @return long
     * @return the value of field 'nrSequenciaContratoVinculo'.
     */
    public long getNrSequenciaContratoVinculo()
    {
        return this._nrSequenciaContratoVinculo;
    } //-- long getNrSequenciaContratoVinculo() 

    /**
     * Method hasCdBanco
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdBanco()
    {
        return this._has_cdBanco;
    } //-- boolean hasCdBanco() 

    /**
     * Method hasCdComercialAgenciaContabil
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdComercialAgenciaContabil()
    {
        return this._has_cdComercialAgenciaContabil;
    } //-- boolean hasCdComercialAgenciaContabil() 

    /**
     * Method hasCdConta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdConta()
    {
        return this._has_cdConta;
    } //-- boolean hasCdConta() 

    /**
     * Method hasCdDigitoConta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdDigitoConta()
    {
        return this._has_cdDigitoConta;
    } //-- boolean hasCdDigitoConta() 

    /**
     * Method hasCdMotivoBloqueioConta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMotivoBloqueioConta()
    {
        return this._has_cdMotivoBloqueioConta;
    } //-- boolean hasCdMotivoBloqueioConta() 

    /**
     * Method hasCdPessoaJuridicaContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridicaContrato()
    {
        return this._has_cdPessoaJuridicaContrato;
    } //-- boolean hasCdPessoaJuridicaContrato() 

    /**
     * Method hasCdPessoaJuridicaVinculo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridicaVinculo()
    {
        return this._has_cdPessoaJuridicaVinculo;
    } //-- boolean hasCdPessoaJuridicaVinculo() 

    /**
     * Method hasCdSituacaoContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSituacaoContrato()
    {
        return this._has_cdSituacaoContrato;
    } //-- boolean hasCdSituacaoContrato() 

    /**
     * Method hasCdTipoContratoConta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoConta()
    {
        return this._has_cdTipoContratoConta;
    } //-- boolean hasCdTipoContratoConta() 

    /**
     * Method hasCdTipoContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoNegocio()
    {
        return this._has_cdTipoContratoNegocio;
    } //-- boolean hasCdTipoContratoNegocio() 

    /**
     * Method hasCdTipoContratoVinculo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoVinculo()
    {
        return this._has_cdTipoContratoVinculo;
    } //-- boolean hasCdTipoContratoVinculo() 

    /**
     * Method hasCdTipoVinculoContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoVinculoContrato()
    {
        return this._has_cdTipoVinculoContrato;
    } //-- boolean hasCdTipoVinculoContrato() 

    /**
     * Method hasNrSequenciaContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaContratoNegocio()
    {
        return this._has_nrSequenciaContratoNegocio;
    } //-- boolean hasNrSequenciaContratoNegocio() 

    /**
     * Method hasNrSequenciaContratoVinculo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaContratoVinculo()
    {
        return this._has_nrSequenciaContratoVinculo;
    } //-- boolean hasNrSequenciaContratoVinculo() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdBanco'.
     * 
     * @param cdBanco the value of field 'cdBanco'.
     */
    public void setCdBanco(int cdBanco)
    {
        this._cdBanco = cdBanco;
        this._has_cdBanco = true;
    } //-- void setCdBanco(int) 

    /**
     * Sets the value of field 'cdComercialAgenciaContabil'.
     * 
     * @param cdComercialAgenciaContabil the value of field
     * 'cdComercialAgenciaContabil'.
     */
    public void setCdComercialAgenciaContabil(int cdComercialAgenciaContabil)
    {
        this._cdComercialAgenciaContabil = cdComercialAgenciaContabil;
        this._has_cdComercialAgenciaContabil = true;
    } //-- void setCdComercialAgenciaContabil(int) 

    /**
     * Sets the value of field 'cdConta'.
     * 
     * @param cdConta the value of field 'cdConta'.
     */
    public void setCdConta(long cdConta)
    {
        this._cdConta = cdConta;
        this._has_cdConta = true;
    } //-- void setCdConta(long) 

    /**
     * Sets the value of field 'cdDigitoConta'.
     * 
     * @param cdDigitoConta the value of field 'cdDigitoConta'.
     */
    public void setCdDigitoConta(int cdDigitoConta)
    {
        this._cdDigitoConta = cdDigitoConta;
        this._has_cdDigitoConta = true;
    } //-- void setCdDigitoConta(int) 

    /**
     * Sets the value of field 'cdMotivoBloqueioConta'.
     * 
     * @param cdMotivoBloqueioConta the value of field
     * 'cdMotivoBloqueioConta'.
     */
    public void setCdMotivoBloqueioConta(int cdMotivoBloqueioConta)
    {
        this._cdMotivoBloqueioConta = cdMotivoBloqueioConta;
        this._has_cdMotivoBloqueioConta = true;
    } //-- void setCdMotivoBloqueioConta(int) 

    /**
     * Sets the value of field 'cdPessoaJuridicaContrato'.
     * 
     * @param cdPessoaJuridicaContrato the value of field
     * 'cdPessoaJuridicaContrato'.
     */
    public void setCdPessoaJuridicaContrato(long cdPessoaJuridicaContrato)
    {
        this._cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
        this._has_cdPessoaJuridicaContrato = true;
    } //-- void setCdPessoaJuridicaContrato(long) 

    /**
     * Sets the value of field 'cdPessoaJuridicaVinculo'.
     * 
     * @param cdPessoaJuridicaVinculo the value of field
     * 'cdPessoaJuridicaVinculo'.
     */
    public void setCdPessoaJuridicaVinculo(long cdPessoaJuridicaVinculo)
    {
        this._cdPessoaJuridicaVinculo = cdPessoaJuridicaVinculo;
        this._has_cdPessoaJuridicaVinculo = true;
    } //-- void setCdPessoaJuridicaVinculo(long) 

    /**
     * Sets the value of field 'cdSituacaoContrato'.
     * 
     * @param cdSituacaoContrato the value of field
     * 'cdSituacaoContrato'.
     */
    public void setCdSituacaoContrato(int cdSituacaoContrato)
    {
        this._cdSituacaoContrato = cdSituacaoContrato;
        this._has_cdSituacaoContrato = true;
    } //-- void setCdSituacaoContrato(int) 

    /**
     * Sets the value of field 'cdTipoAcao'.
     * 
     * @param cdTipoAcao the value of field 'cdTipoAcao'.
     */
    public void setCdTipoAcao(java.lang.String cdTipoAcao)
    {
        this._cdTipoAcao = cdTipoAcao;
    } //-- void setCdTipoAcao(java.lang.String) 

    /**
     * Sets the value of field 'cdTipoContratoConta'.
     * 
     * @param cdTipoContratoConta the value of field
     * 'cdTipoContratoConta'.
     */
    public void setCdTipoContratoConta(int cdTipoContratoConta)
    {
        this._cdTipoContratoConta = cdTipoContratoConta;
        this._has_cdTipoContratoConta = true;
    } //-- void setCdTipoContratoConta(int) 

    /**
     * Sets the value of field 'cdTipoContratoNegocio'.
     * 
     * @param cdTipoContratoNegocio the value of field
     * 'cdTipoContratoNegocio'.
     */
    public void setCdTipoContratoNegocio(int cdTipoContratoNegocio)
    {
        this._cdTipoContratoNegocio = cdTipoContratoNegocio;
        this._has_cdTipoContratoNegocio = true;
    } //-- void setCdTipoContratoNegocio(int) 

    /**
     * Sets the value of field 'cdTipoContratoVinculo'.
     * 
     * @param cdTipoContratoVinculo the value of field
     * 'cdTipoContratoVinculo'.
     */
    public void setCdTipoContratoVinculo(int cdTipoContratoVinculo)
    {
        this._cdTipoContratoVinculo = cdTipoContratoVinculo;
        this._has_cdTipoContratoVinculo = true;
    } //-- void setCdTipoContratoVinculo(int) 

    /**
     * Sets the value of field 'cdTipoVinculoContrato'.
     * 
     * @param cdTipoVinculoContrato the value of field
     * 'cdTipoVinculoContrato'.
     */
    public void setCdTipoVinculoContrato(int cdTipoVinculoContrato)
    {
        this._cdTipoVinculoContrato = cdTipoVinculoContrato;
        this._has_cdTipoVinculoContrato = true;
    } //-- void setCdTipoVinculoContrato(int) 

    /**
     * Sets the value of field 'dtInicioVinculacaoContrato'.
     * 
     * @param dtInicioVinculacaoContrato the value of field
     * 'dtInicioVinculacaoContrato'.
     */
    public void setDtInicioVinculacaoContrato(java.lang.String dtInicioVinculacaoContrato)
    {
        this._dtInicioVinculacaoContrato = dtInicioVinculacaoContrato;
    } //-- void setDtInicioVinculacaoContrato(java.lang.String) 

    /**
     * Sets the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @param nrSequenciaContratoNegocio the value of field
     * 'nrSequenciaContratoNegocio'.
     */
    public void setNrSequenciaContratoNegocio(long nrSequenciaContratoNegocio)
    {
        this._nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
        this._has_nrSequenciaContratoNegocio = true;
    } //-- void setNrSequenciaContratoNegocio(long) 

    /**
     * Sets the value of field 'nrSequenciaContratoVinculo'.
     * 
     * @param nrSequenciaContratoVinculo the value of field
     * 'nrSequenciaContratoVinculo'.
     */
    public void setNrSequenciaContratoVinculo(long nrSequenciaContratoVinculo)
    {
        this._nrSequenciaContratoVinculo = nrSequenciaContratoVinculo;
        this._has_nrSequenciaContratoVinculo = true;
    } //-- void setNrSequenciaContratoVinculo(long) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return BloquearDesbloquearContaContratoRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.bloqueardesbloquearcontacontrato.request.BloquearDesbloquearContaContratoRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.bloqueardesbloquearcontacontrato.request.BloquearDesbloquearContaContratoRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.bloqueardesbloquearcontacontrato.request.BloquearDesbloquearContaContratoRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.bloqueardesbloquearcontacontrato.request.BloquearDesbloquearContaContratoRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
