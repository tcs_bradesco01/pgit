/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantercontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantercontrato.bean;

import java.util.ArrayList;
import java.util.List;

/**
 * Nome: IncluirRelacContaContEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class IncluirRelacContaContEntradaDTO {
	
	/** Atributo cdPessoaJuridicaContrato. */
	private Long cdPessoaJuridicaContrato;
	
	/** Atributo cdTipoContratoNegocio. */
	private Integer cdTipoContratoNegocio;
	
	/** Atributo nrSequenciaContratoNegocio. */
	private Long nrSequenciaContratoNegocio;
	
	/** Atributo cdTipoVinculoContrato. */
	private Integer cdTipoVinculoContrato;
	
	/** Atributo cdTipoRelacionamentoConta. */
	private Integer cdTipoRelacionamentoConta;
	
	/** Atributo cdPessoaJuridicaRelacionada. */
	private Long cdPessoaJuridicaRelacionada;	
	
	/** Atributo cdTipoContratoRelacionado. */
	private Integer cdTipoContratoRelacionado;
	
	/** Atributo nrSequenciaContratoRelacionado. */
	private Long nrSequenciaContratoRelacionado;
	
	/** Atributo cdFinalidadeContaPagamento. */
	private Integer cdFinalidadeContaPagamento;
	
	private String cdTipoInscricao;
	
	private String cpfCNPJ;
	
	private Long cdPssoa;
	
	/** Atributo ocorrenciasContas. */
	private List<IncluirRelacContaOcorrenciasDTO> ocorrenciasContas = new ArrayList<IncluirRelacContaOcorrenciasDTO>();
	
	/**
	 * Get: cdFinalidadeContaPagamento.
	 *
	 * @return cdFinalidadeContaPagamento
	 */
	public Integer getCdFinalidadeContaPagamento() {
		return cdFinalidadeContaPagamento;
	}
	
	/**
	 * Set: cdFinalidadeContaPagamento.
	 *
	 * @param cdFinalidadeContaPagamento the cd finalidade conta pagamento
	 */
	public void setCdFinalidadeContaPagamento(Integer cdFinalidadeContaPagamento) {
		this.cdFinalidadeContaPagamento = cdFinalidadeContaPagamento;
	}
	
	/**
	 * Get: cdPessoaJuridicaContrato.
	 *
	 * @return cdPessoaJuridicaContrato
	 */
	public Long getCdPessoaJuridicaContrato() {
		return cdPessoaJuridicaContrato;
	}
	
	/**
	 * Set: cdPessoaJuridicaContrato.
	 *
	 * @param cdPessoaJuridicaContrato the cd pessoa juridica contrato
	 */
	public void setCdPessoaJuridicaContrato(Long cdPessoaJuridicaContrato) {
		this.cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
	}
	
	/**
	 * Get: cdPessoaJuridicaRelacionada.
	 *
	 * @return cdPessoaJuridicaRelacionada
	 */
	public Long getCdPessoaJuridicaRelacionada() {
		return cdPessoaJuridicaRelacionada;
	}
	
	/**
	 * Set: cdPessoaJuridicaRelacionada.
	 *
	 * @param cdPessoaJuridicaRelacionada the cd pessoa juridica relacionada
	 */
	public void setCdPessoaJuridicaRelacionada(Long cdPessoaJuridicaRelacionada) {
		this.cdPessoaJuridicaRelacionada = cdPessoaJuridicaRelacionada;
	}
	
	/**
	 * Get: cdTipoContratoNegocio.
	 *
	 * @return cdTipoContratoNegocio
	 */
	public Integer getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}
	
	/**
	 * Set: cdTipoContratoNegocio.
	 *
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}
	
	/**
	 * Get: cdTipoContratoRelacionado.
	 *
	 * @return cdTipoContratoRelacionado
	 */
	public Integer getCdTipoContratoRelacionado() {
		return cdTipoContratoRelacionado;
	}
	
	/**
	 * Set: cdTipoContratoRelacionado.
	 *
	 * @param cdTipoContratoRelacionado the cd tipo contrato relacionado
	 */
	public void setCdTipoContratoRelacionado(Integer cdTipoContratoRelacionado) {
		this.cdTipoContratoRelacionado = cdTipoContratoRelacionado;
	}
	
	/**
	 * Get: cdTipoRelacionamentoConta.
	 *
	 * @return cdTipoRelacionamentoConta
	 */
	public Integer getCdTipoRelacionamentoConta() {
		return cdTipoRelacionamentoConta;
	}
	
	/**
	 * Set: cdTipoRelacionamentoConta.
	 *
	 * @param cdTipoRelacionamentoConta the cd tipo relacionamento conta
	 */
	public void setCdTipoRelacionamentoConta(Integer cdTipoRelacionamentoConta) {
		this.cdTipoRelacionamentoConta = cdTipoRelacionamentoConta;
	}
	
	/**
	 * Get: cdTipoVinculoContrato.
	 *
	 * @return cdTipoVinculoContrato
	 */
	public Integer getCdTipoVinculoContrato() {
		return cdTipoVinculoContrato;
	}
	
	/**
	 * Set: cdTipoVinculoContrato.
	 *
	 * @param cdTipoVinculoContrato the cd tipo vinculo contrato
	 */
	public void setCdTipoVinculoContrato(Integer cdTipoVinculoContrato) {
		this.cdTipoVinculoContrato = cdTipoVinculoContrato;
	}
	
	/**
	 * Get: nrSequenciaContratoNegocio.
	 *
	 * @return nrSequenciaContratoNegocio
	 */
	public Long getNrSequenciaContratoNegocio() {
		return nrSequenciaContratoNegocio;
	}
	
	/**
	 * Set: nrSequenciaContratoNegocio.
	 *
	 * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
	 */
	public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}
	
	/**
	 * Get: nrSequenciaContratoRelacionado.
	 *
	 * @return nrSequenciaContratoRelacionado
	 */
	public Long getNrSequenciaContratoRelacionado() {
		return nrSequenciaContratoRelacionado;
	}
	
	/**
	 * Set: nrSequenciaContratoRelacionado.
	 *
	 * @param nrSequenciaContratoRelacionado the nr sequencia contrato relacionado
	 */
	public void setNrSequenciaContratoRelacionado(
			Long nrSequenciaContratoRelacionado) {
		this.nrSequenciaContratoRelacionado = nrSequenciaContratoRelacionado;
	}
	
	/**
	 * Get: ocorrenciasContas.
	 *
	 * @return ocorrenciasContas
	 */
	public List<IncluirRelacContaOcorrenciasDTO> getOcorrenciasContas() {
		return ocorrenciasContas;
	}
	
	/**
	 * Set: ocorrenciasContas.
	 *
	 * @param ocorrenciasContas the ocorrencias contas
	 */
	public void setOcorrenciasContas(
			List<IncluirRelacContaOcorrenciasDTO> ocorrenciasContas) {
		this.ocorrenciasContas = ocorrenciasContas;
	}

	public String getCdTipoInscricao() {
		return cdTipoInscricao;
	}

	public void setCdTipoInscricao(String cdTipoInscricao) {
		this.cdTipoInscricao = cdTipoInscricao;
	}

	public String getCpfCNPJ() {
		return cpfCNPJ;
	}

	public void setCpfCNPJ(String cpfCNPJ) {
		this.cpfCNPJ = cpfCNPJ;
	}

	public Long getCdPssoa() {
		return cdPssoa;
	}

	public void setCdPssoa(Long cdPssoa) {
		this.cdPssoa = cdPssoa;
	}
	
	
}
