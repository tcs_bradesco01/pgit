/*
 * Nome: br.com.bradesco.web.pgit.service.business.combo.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.combo.bean;

/**
 * Nome: ListarDependenciaDiretoriaSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarDependenciaDiretoriaSaidaDTO {
	
	/** Atributo cdEmpresa. */
	private Integer cdEmpresa;
    
    /** Atributo cdDependencia. */
    private Integer cdDependencia;
    
    /** Atributo dsDependencia. */
    private String dsDependencia;
    
    /** Atributo cdNaturezaDependencia. */
    private String cdNaturezaDependencia;
    
	/**
	 * Listar dependencia diretoria saida dto.
	 *
	 * @param cdEmpresa the cd empresa
	 * @param cdDependencia the cd dependencia
	 * @param dsDependencia the ds dependencia
	 * @param cdNaturezaDependencia the cd natureza dependencia
	 */
	public ListarDependenciaDiretoriaSaidaDTO(Integer cdEmpresa, Integer cdDependencia, String dsDependencia, String cdNaturezaDependencia) {
		super();
		this.cdEmpresa = cdEmpresa;
		this.cdDependencia = cdDependencia;
		this.dsDependencia = dsDependencia;
		this.cdNaturezaDependencia = cdNaturezaDependencia;
	}
	
	/**
	 * Get: cdDependencia.
	 *
	 * @return cdDependencia
	 */
	public Integer getCdDependencia() {
		return cdDependencia;
	}
	
	/**
	 * Set: cdDependencia.
	 *
	 * @param cdDependencia the cd dependencia
	 */
	public void setCdDependencia(Integer cdDependencia) {
		this.cdDependencia = cdDependencia;
	}
	
	/**
	 * Get: cdEmpresa.
	 *
	 * @return cdEmpresa
	 */
	public Integer getCdEmpresa() {
		return cdEmpresa;
	}
	
	/**
	 * Set: cdEmpresa.
	 *
	 * @param cdEmpresa the cd empresa
	 */
	public void setCdEmpresa(Integer cdEmpresa) {
		this.cdEmpresa = cdEmpresa;
	}
	
	/**
	 * Get: dsDependencia.
	 *
	 * @return dsDependencia
	 */
	public String getDsDependencia() {
		return dsDependencia;
	}
	
	/**
	 * Set: dsDependencia.
	 *
	 * @param dsDependencia the ds dependencia
	 */
	public void setDsDependencia(String dsDependencia) {
		this.dsDependencia = dsDependencia;
	}

	/**
	 * Get: cdNaturezaDependencia.
	 *
	 * @return cdNaturezaDependencia
	 */
	public String getCdNaturezaDependencia() {
		return cdNaturezaDependencia;
	}

	/**
	 * Set: cdNaturezaDependencia.
	 *
	 * @param cdNaturezaDependencia the cd natureza dependencia
	 */
	public void setCdNaturezaDependencia(String cdNaturezaDependencia) {
		this.cdNaturezaDependencia = cdNaturezaDependencia;
	}
    
    

}
