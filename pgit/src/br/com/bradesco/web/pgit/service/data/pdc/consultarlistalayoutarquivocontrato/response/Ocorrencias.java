/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultarlistalayoutarquivocontrato.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdTipoLayout
     */
    private int _cdTipoLayout = 0;

    /**
     * keeps track of state for field: _cdTipoLayout
     */
    private boolean _has_cdTipoLayout;

    /**
     * Field _dsTipoLayout
     */
    private java.lang.String _dsTipoLayout;

    /**
     * Field _cdSituacaoLayoutContrato
     */
    private int _cdSituacaoLayoutContrato = 0;

    /**
     * keeps track of state for field: _cdSituacaoLayoutContrato
     */
    private boolean _has_cdSituacaoLayoutContrato;

    /**
     * Field _dsSituacaoLayoutContrato
     */
    private java.lang.String _dsSituacaoLayoutContrato;

    /**
     * Field _cdProduto
     */
    private int _cdProduto = 0;

    /**
     * keeps track of state for field: _cdProduto
     */
    private boolean _has_cdProduto;

    /**
     * Field _dsProduto
     */
    private java.lang.String _dsProduto;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarlistalayoutarquivocontrato.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdProduto
     * 
     */
    public void deleteCdProduto()
    {
        this._has_cdProduto= false;
    } //-- void deleteCdProduto() 

    /**
     * Method deleteCdSituacaoLayoutContrato
     * 
     */
    public void deleteCdSituacaoLayoutContrato()
    {
        this._has_cdSituacaoLayoutContrato= false;
    } //-- void deleteCdSituacaoLayoutContrato() 

    /**
     * Method deleteCdTipoLayout
     * 
     */
    public void deleteCdTipoLayout()
    {
        this._has_cdTipoLayout= false;
    } //-- void deleteCdTipoLayout() 

    /**
     * Returns the value of field 'cdProduto'.
     * 
     * @return int
     * @return the value of field 'cdProduto'.
     */
    public int getCdProduto()
    {
        return this._cdProduto;
    } //-- int getCdProduto() 

    /**
     * Returns the value of field 'cdSituacaoLayoutContrato'.
     * 
     * @return int
     * @return the value of field 'cdSituacaoLayoutContrato'.
     */
    public int getCdSituacaoLayoutContrato()
    {
        return this._cdSituacaoLayoutContrato;
    } //-- int getCdSituacaoLayoutContrato() 

    /**
     * Returns the value of field 'cdTipoLayout'.
     * 
     * @return int
     * @return the value of field 'cdTipoLayout'.
     */
    public int getCdTipoLayout()
    {
        return this._cdTipoLayout;
    } //-- int getCdTipoLayout() 

    /**
     * Returns the value of field 'dsProduto'.
     * 
     * @return String
     * @return the value of field 'dsProduto'.
     */
    public java.lang.String getDsProduto()
    {
        return this._dsProduto;
    } //-- java.lang.String getDsProduto() 

    /**
     * Returns the value of field 'dsSituacaoLayoutContrato'.
     * 
     * @return String
     * @return the value of field 'dsSituacaoLayoutContrato'.
     */
    public java.lang.String getDsSituacaoLayoutContrato()
    {
        return this._dsSituacaoLayoutContrato;
    } //-- java.lang.String getDsSituacaoLayoutContrato() 

    /**
     * Returns the value of field 'dsTipoLayout'.
     * 
     * @return String
     * @return the value of field 'dsTipoLayout'.
     */
    public java.lang.String getDsTipoLayout()
    {
        return this._dsTipoLayout;
    } //-- java.lang.String getDsTipoLayout() 

    /**
     * Method hasCdProduto
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProduto()
    {
        return this._has_cdProduto;
    } //-- boolean hasCdProduto() 

    /**
     * Method hasCdSituacaoLayoutContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSituacaoLayoutContrato()
    {
        return this._has_cdSituacaoLayoutContrato;
    } //-- boolean hasCdSituacaoLayoutContrato() 

    /**
     * Method hasCdTipoLayout
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoLayout()
    {
        return this._has_cdTipoLayout;
    } //-- boolean hasCdTipoLayout() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdProduto'.
     * 
     * @param cdProduto the value of field 'cdProduto'.
     */
    public void setCdProduto(int cdProduto)
    {
        this._cdProduto = cdProduto;
        this._has_cdProduto = true;
    } //-- void setCdProduto(int) 

    /**
     * Sets the value of field 'cdSituacaoLayoutContrato'.
     * 
     * @param cdSituacaoLayoutContrato the value of field
     * 'cdSituacaoLayoutContrato'.
     */
    public void setCdSituacaoLayoutContrato(int cdSituacaoLayoutContrato)
    {
        this._cdSituacaoLayoutContrato = cdSituacaoLayoutContrato;
        this._has_cdSituacaoLayoutContrato = true;
    } //-- void setCdSituacaoLayoutContrato(int) 

    /**
     * Sets the value of field 'cdTipoLayout'.
     * 
     * @param cdTipoLayout the value of field 'cdTipoLayout'.
     */
    public void setCdTipoLayout(int cdTipoLayout)
    {
        this._cdTipoLayout = cdTipoLayout;
        this._has_cdTipoLayout = true;
    } //-- void setCdTipoLayout(int) 

    /**
     * Sets the value of field 'dsProduto'.
     * 
     * @param dsProduto the value of field 'dsProduto'.
     */
    public void setDsProduto(java.lang.String dsProduto)
    {
        this._dsProduto = dsProduto;
    } //-- void setDsProduto(java.lang.String) 

    /**
     * Sets the value of field 'dsSituacaoLayoutContrato'.
     * 
     * @param dsSituacaoLayoutContrato the value of field
     * 'dsSituacaoLayoutContrato'.
     */
    public void setDsSituacaoLayoutContrato(java.lang.String dsSituacaoLayoutContrato)
    {
        this._dsSituacaoLayoutContrato = dsSituacaoLayoutContrato;
    } //-- void setDsSituacaoLayoutContrato(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoLayout'.
     * 
     * @param dsTipoLayout the value of field 'dsTipoLayout'.
     */
    public void setDsTipoLayout(java.lang.String dsTipoLayout)
    {
        this._dsTipoLayout = dsTipoLayout;
    } //-- void setDsTipoLayout(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultarlistalayoutarquivocontrato.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultarlistalayoutarquivocontrato.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultarlistalayoutarquivocontrato.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarlistalayoutarquivocontrato.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
