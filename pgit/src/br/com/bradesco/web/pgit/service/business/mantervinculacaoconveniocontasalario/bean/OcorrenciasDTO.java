package br.com.bradesco.web.pgit.service.business.mantervinculacaoconveniocontasalario.bean;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.ListarConvenioContaSalarioResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.Ocorrencias;

public class OcorrenciasDTO implements Serializable{

	private static final long serialVersionUID = 922781795100090783L;
	
	private Integer nSeqFaixaSalarial; 
	private BigDecimal vlInicialFaixaSalarial; 
	private BigDecimal vlFinalFaixaSalarial;
	private Long qtdFuncionarioFilial01; 
	private Long qtdFuncionarioFilial02; 
	private Long qtdFuncionarioFilial03; 
	private Long qtdFuncionarioFilial04; 
	private Long qtdFuncionarioFilial05; 
	private Long qtdFuncionarioFilial06; 
	private Long qtdFuncionarioFilial07; 
	private Long qtdFuncionarioFilial08; 
	private Long qtdFuncionarioFilial09; 
	private Long qtdFuncionarioFilial10;
	
	public OcorrenciasDTO() {
		super();
	}

	public OcorrenciasDTO(Integer nSeqFaixaSalarial,
			BigDecimal vlInicialFaixaSalarial, BigDecimal vlFinalFaixaSalarial,
			Long qtdFuncionarioFilial01, Long qtdFuncionarioFilial02,
			Long qtdFuncionarioFilial03, Long qtdFuncionarioFilial04,
			Long qtdFuncionarioFilial05, Long qtdFuncionarioFilial06,
			Long qtdFuncionarioFilial07, Long qtdFuncionarioFilial08,
			Long qtdFuncionarioFilial09, Long qtdFuncionarioFilial10) {
		super();
		this.nSeqFaixaSalarial = nSeqFaixaSalarial;
		this.vlInicialFaixaSalarial = vlInicialFaixaSalarial;
		this.vlFinalFaixaSalarial = vlFinalFaixaSalarial;
		this.qtdFuncionarioFilial01 = qtdFuncionarioFilial01;
		this.qtdFuncionarioFilial02 = qtdFuncionarioFilial02;
		this.qtdFuncionarioFilial03 = qtdFuncionarioFilial03;
		this.qtdFuncionarioFilial04 = qtdFuncionarioFilial04;
		this.qtdFuncionarioFilial05 = qtdFuncionarioFilial05;
		this.qtdFuncionarioFilial06 = qtdFuncionarioFilial06;
		this.qtdFuncionarioFilial07 = qtdFuncionarioFilial07;
		this.qtdFuncionarioFilial08 = qtdFuncionarioFilial08;
		this.qtdFuncionarioFilial09 = qtdFuncionarioFilial09;
		this.qtdFuncionarioFilial10 = qtdFuncionarioFilial10;
	}
	
	public List<OcorrenciasDTO> listarOcorrenciasDTO(ListarConvenioContaSalarioResponse response) {
		
		List<OcorrenciasDTO> listaOcorrenciasDTO = new ArrayList<OcorrenciasDTO>();
		OcorrenciasDTO ocorrenciasDTO = new OcorrenciasDTO();
		
		for(Ocorrencias retorno : response.getOcorrencias()){
			ocorrenciasDTO.setnSeqFaixaSalarial(retorno.getNSeqFaixaSalarial());
			ocorrenciasDTO.setVlInicialFaixaSalarial(retorno.getVlInicialFaixaSalarial());
			ocorrenciasDTO.setVlFinalFaixaSalarial(retorno.getVlFinalFaixaSalarial());
			ocorrenciasDTO.setQtdFuncionarioFilial01(retorno.getQtdFuncionarioFilial01());
			ocorrenciasDTO.setQtdFuncionarioFilial02(retorno.getQtdFuncionarioFilial02());
			ocorrenciasDTO.setQtdFuncionarioFilial03(retorno.getQtdFuncionarioFilial03());
			ocorrenciasDTO.setQtdFuncionarioFilial04(retorno.getQtdFuncionarioFilial04());
			ocorrenciasDTO.setQtdFuncionarioFilial05(retorno.getQtdFuncionarioFilial05());
			ocorrenciasDTO.setQtdFuncionarioFilial06(retorno.getQtdFuncionarioFilial06());
			ocorrenciasDTO.setQtdFuncionarioFilial07(retorno.getQtdFuncionarioFilial07());
			ocorrenciasDTO.setQtdFuncionarioFilial08(retorno.getQtdFuncionarioFilial08());
			ocorrenciasDTO.setQtdFuncionarioFilial09(retorno.getQtdFuncionarioFilial09());
			ocorrenciasDTO.setQtdFuncionarioFilial10(retorno.getQtdFuncionarioFilial10());
			listaOcorrenciasDTO.add(ocorrenciasDTO);
		}
		return listaOcorrenciasDTO;
	}

	public Integer getnSeqFaixaSalarial() {
		return nSeqFaixaSalarial;
	}

	public void setnSeqFaixaSalarial(Integer nSeqFaixaSalarial) {
		this.nSeqFaixaSalarial = nSeqFaixaSalarial;
	}

	public BigDecimal getVlInicialFaixaSalarial() {
		return vlInicialFaixaSalarial;
	}

	public void setVlInicialFaixaSalarial(BigDecimal vlInicialFaixaSalarial) {
		this.vlInicialFaixaSalarial = vlInicialFaixaSalarial;
	}

	public BigDecimal getVlFinalFaixaSalarial() {
		return vlFinalFaixaSalarial;
	}

	public void setVlFinalFaixaSalarial(BigDecimal vlFinalFaixaSalarial) {
		this.vlFinalFaixaSalarial = vlFinalFaixaSalarial;
	}

	public Long getQtdFuncionarioFilial01() {
		return qtdFuncionarioFilial01;
	}

	public void setQtdFuncionarioFilial01(Long qtdFuncionarioFilial01) {
		this.qtdFuncionarioFilial01 = qtdFuncionarioFilial01;
	}

	public Long getQtdFuncionarioFilial02() {
		return qtdFuncionarioFilial02;
	}

	public void setQtdFuncionarioFilial02(Long qtdFuncionarioFilial02) {
		this.qtdFuncionarioFilial02 = qtdFuncionarioFilial02;
	}

	public Long getQtdFuncionarioFilial03() {
		return qtdFuncionarioFilial03;
	}

	public void setQtdFuncionarioFilial03(Long qtdFuncionarioFilial03) {
		this.qtdFuncionarioFilial03 = qtdFuncionarioFilial03;
	}

	public Long getQtdFuncionarioFilial04() {
		return qtdFuncionarioFilial04;
	}

	public void setQtdFuncionarioFilial04(Long qtdFuncionarioFilial04) {
		this.qtdFuncionarioFilial04 = qtdFuncionarioFilial04;
	}

	public Long getQtdFuncionarioFilial05() {
		return qtdFuncionarioFilial05;
	}

	public void setQtdFuncionarioFilial05(Long qtdFuncionarioFilial05) {
		this.qtdFuncionarioFilial05 = qtdFuncionarioFilial05;
	}

	public Long getQtdFuncionarioFilial06() {
		return qtdFuncionarioFilial06;
	}

	public void setQtdFuncionarioFilial06(Long qtdFuncionarioFilial06) {
		this.qtdFuncionarioFilial06 = qtdFuncionarioFilial06;
	}

	public Long getQtdFuncionarioFilial07() {
		return qtdFuncionarioFilial07;
	}

	public void setQtdFuncionarioFilial07(Long qtdFuncionarioFilial07) {
		this.qtdFuncionarioFilial07 = qtdFuncionarioFilial07;
	}

	public Long getQtdFuncionarioFilial08() {
		return qtdFuncionarioFilial08;
	}

	public void setQtdFuncionarioFilial08(Long qtdFuncionarioFilial08) {
		this.qtdFuncionarioFilial08 = qtdFuncionarioFilial08;
	}

	public Long getQtdFuncionarioFilial09() {
		return qtdFuncionarioFilial09;
	}

	public void setQtdFuncionarioFilial09(Long qtdFuncionarioFilial09) {
		this.qtdFuncionarioFilial09 = qtdFuncionarioFilial09;
	}

	public Long getQtdFuncionarioFilial10() {
		return qtdFuncionarioFilial10;
	}

	public void setQtdFuncionarioFilial10(Long qtdFuncionarioFilial10) {
		this.qtdFuncionarioFilial10 = qtdFuncionarioFilial10;
	}
	
}
