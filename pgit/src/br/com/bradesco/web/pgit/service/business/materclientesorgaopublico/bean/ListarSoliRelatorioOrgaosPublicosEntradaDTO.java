/*
 * Nome: br.com.bradesco.web.pgit.service.business.materclientesorgaopublico.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.materclientesorgaopublico.bean;

/**
 * Nome: ListarSoliRelatorioOrgaosPublicosEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarSoliRelatorioOrgaosPublicosEntradaDTO{
	
	/** Atributo numeroOcorrencias. */
	private Integer numeroOcorrencias;
	
	/** Atributo dtInicioSolicitacao. */
	private String dtInicioSolicitacao;
	
	/** Atributo dtFimSolicitacao. */
	private String dtFimSolicitacao;
	
	
	/**
	 * Set: numeroOcorrencias.
	 *
	 * @param numeroOcorrencias the numero ocorrencias
	 */
	public void setNumeroOcorrencias(Integer numeroOcorrencias) {
		this.numeroOcorrencias = numeroOcorrencias;
	}
	
	/**
	 * Get: numeroOcorrencias.
	 *
	 * @return numeroOcorrencias
	 */
	public Integer getNumeroOcorrencias() {
		return numeroOcorrencias;
	}
	
	/**
	 * Set: dtInicioSolicitacao.
	 *
	 * @param dtInicioSolicitacao the dt inicio solicitacao
	 */
	public void setDtInicioSolicitacao(String dtInicioSolicitacao) {
		this.dtInicioSolicitacao = dtInicioSolicitacao;
	}
	
	/**
	 * Get: dtInicioSolicitacao.
	 *
	 * @return dtInicioSolicitacao
	 */
	public String getDtInicioSolicitacao() {
		return dtInicioSolicitacao;
	}
	
	/**
	 * Set: dtFimSolicitacao.
	 *
	 * @param dtFimSolicitacao the dt fim solicitacao
	 */
	public void setDtFimSolicitacao(String dtFimSolicitacao) {
		this.dtFimSolicitacao = dtFimSolicitacao;
	}
	
	/**
	 * Get: dtFimSolicitacao.
	 *
	 * @return dtFimSolicitacao
	 */
	public String getDtFimSolicitacao() {
		return dtFimSolicitacao;
	}
}