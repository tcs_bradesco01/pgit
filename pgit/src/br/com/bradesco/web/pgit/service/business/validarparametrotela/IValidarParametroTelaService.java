/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/interfaz.ftl,v $
 * $Id: interfaz.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: interfaz.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.validarparametrotela;

import java.util.List;

import br.com.bradesco.web.pgit.service.business.incluircontratocontingencia.bean.ConsultarListaContasInclusaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.incluircontratocontingencia.bean.ConsultarListaContasInclusaoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.incluircontratocontingencia.bean.ConsultarServRelacServperacionalEntradaDTO;
import br.com.bradesco.web.pgit.service.business.incluircontratocontingencia.bean.ConsultarServRelacServperacionalSaidaDTO;
import br.com.bradesco.web.pgit.service.business.incluircontratocontingencia.bean.IncluirContratoPgitEntradaDTO;
import br.com.bradesco.web.pgit.service.business.incluircontratocontingencia.bean.IncluirContratoPgitSaidaDTO;
import br.com.bradesco.web.pgit.service.business.incluircontratocontingencia.bean.ListarDadosCtaConvnContingenciaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.incluircontratocontingencia.bean.ListarDadosCtaConvnContingenciaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.validarparametrotela.bean.ValidarParametroTelaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.validarparametrotela.bean.ValidarParametroTelaSaidaDTO;

/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Interface do adaptador: IncluirContratoContingencia
 * </p>
 * 
 * @comment CODIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public interface IValidarParametroTelaService {
	
	/**
	 * Listar validar parametro tela.
	 *
	 * @param entradaDTO the entrada dto
	 * @return the validar parametro tela saida dto
	 */
	public ValidarParametroTelaSaidaDTO listarValidarParametroTela(ValidarParametroTelaEntradaDTO entradaDTO);
}