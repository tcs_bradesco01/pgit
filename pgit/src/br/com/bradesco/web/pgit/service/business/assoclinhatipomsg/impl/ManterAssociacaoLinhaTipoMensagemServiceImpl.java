/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/implementation.ftl,v $
 * $Id: implementation.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: implementation.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */
 
package br.com.bradesco.web.pgit.service.business.assoclinhatipomsg.impl;

import java.util.ArrayList;
import java.util.List;

import br.com.bradesco.web.pgit.service.business.assoclinhatipomsg.IAssocLinTipMsgServiceConstants;
import br.com.bradesco.web.pgit.service.business.assoclinhatipomsg.IAssocLinhaTipoMsgService;
import br.com.bradesco.web.pgit.service.business.assoclinhatipomsg.bean.AlterarLinhaMsgComprovanteEntradaDTO;
import br.com.bradesco.web.pgit.service.business.assoclinhatipomsg.bean.AlterarLinhaMsgComprovanteSaidaDTO;
import br.com.bradesco.web.pgit.service.business.assoclinhatipomsg.bean.DetalharLinhaMsgComprovanteEntradaDTO;
import br.com.bradesco.web.pgit.service.business.assoclinhatipomsg.bean.DetalharLinhaMsgComprovanteSaidaDTO;
import br.com.bradesco.web.pgit.service.business.assoclinhatipomsg.bean.ExcluirLinhaMsgComprovanteEntradaDTO;
import br.com.bradesco.web.pgit.service.business.assoclinhatipomsg.bean.ExcluirLinhaMsgComprovanteSaidaDTO;
import br.com.bradesco.web.pgit.service.business.assoclinhatipomsg.bean.IncluirLinhaMsgComprovanteEntradaDTO;
import br.com.bradesco.web.pgit.service.business.assoclinhatipomsg.bean.IncluirLinhaMsgComprovanteSaidaDTO;
import br.com.bradesco.web.pgit.service.business.assoclinhatipomsg.bean.ListarLinhaMsgComprovanteEntradaDTO;
import br.com.bradesco.web.pgit.service.business.assoclinhatipomsg.bean.ListarLinhaMsgComprovanteSaidaDTO;
import br.com.bradesco.web.pgit.service.business.tipocomprovante.bean.ConsultarDescricaoMensagemEntradaDTO;
import br.com.bradesco.web.pgit.service.business.tipocomprovante.bean.ConsultarDescricaoMensagemSaidaDTO;
import br.com.bradesco.web.pgit.service.data.pdc.FactoryAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.alterarlinhamsgcomprovante.request.AlterarLinhaMsgComprovanteRequest;
import br.com.bradesco.web.pgit.service.data.pdc.alterarlinhamsgcomprovante.response.AlterarLinhaMsgComprovanteResponse;
import br.com.bradesco.web.pgit.service.data.pdc.consultardescricaomensagem.request.ConsultarDescricaoMensagemRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultardescricaomensagem.response.ConsultarDescricaoMensagemResponse;
import br.com.bradesco.web.pgit.service.data.pdc.consultardetalhelinhamensagem.request.ConsultarDetalheLinhaMensagemRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultardetalhelinhamensagem.response.ConsultarDetalheLinhaMensagemResponse;
import br.com.bradesco.web.pgit.service.data.pdc.excluirlinhamsgcomprovante.request.ExcluirLinhaMsgComprovanteRequest;
import br.com.bradesco.web.pgit.service.data.pdc.excluirlinhamsgcomprovante.response.ExcluirLinhaMsgComprovanteResponse;
import br.com.bradesco.web.pgit.service.data.pdc.incluirlinhamsgcomprovante.request.IncluirLinhaMsgComprovanteRequest;
import br.com.bradesco.web.pgit.service.data.pdc.incluirlinhamsgcomprovante.response.IncluirLinhaMsgComprovanteResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarlinhamsgcomprovante.request.ListarLinhaMsgComprovanteRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarlinhamsgcomprovante.response.ListarLinhaMsgComprovanteResponse;
import br.com.bradesco.web.pgit.utils.PgitUtil;


/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Implementa��o do adaptador: ManterAssociacaoLinhaTipoMensagem
 * </p>
 * 
 * @comment C�DIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public class ManterAssociacaoLinhaTipoMensagemServiceImpl implements IAssocLinhaTipoMsgService {
	
	/** Atributo factoryAdapter. */
	private FactoryAdapter factoryAdapter;

    /**
     * Construtor.
     */
    public ManterAssociacaoLinhaTipoMensagemServiceImpl() {
        // TODO: Implementa��o
    }
    
    /**
     * M�todo de exemplo.
     *
     * @see br.com.bradesco.web.pgit.service.business.assoclinhatipomsg.IManterAssociacaoLinhaTipoMensagem#sampleManterAssociacaoLinhaTipoMensagem()
     */
    public void sampleManterAssociacaoLinhaTipoMensagem() {
        // TODO: Implementa�ao
    }

	/**
	 * Get: factoryAdapter.
	 *
	 * @return factoryAdapter
	 */
	public FactoryAdapter getFactoryAdapter() {
		return factoryAdapter;
	}

	/**
	 * Set: factoryAdapter.
	 *
	 * @param factoryAdapter the factory adapter
	 */
	public void setFactoryAdapter(FactoryAdapter factoryAdapter) {
		this.factoryAdapter = factoryAdapter;
	}
    
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.assoclinhatipomsg.IAssocLinhaTipoMsgService#listarLinhaMsgComprovante(br.com.bradesco.web.pgit.service.business.assoclinhatipomsg.bean.ListarLinhaMsgComprovanteEntradaDTO)
	 */
	public List<ListarLinhaMsgComprovanteSaidaDTO> listarLinhaMsgComprovante (ListarLinhaMsgComprovanteEntradaDTO listarLinhaMsgComprovanteEntradaDTO) {
		
		List<ListarLinhaMsgComprovanteSaidaDTO> listaRetorno = new ArrayList<ListarLinhaMsgComprovanteSaidaDTO>();
		ListarLinhaMsgComprovanteRequest request = new ListarLinhaMsgComprovanteRequest();
		ListarLinhaMsgComprovanteResponse response = new ListarLinhaMsgComprovanteResponse();
		
		request.setCdControleMensagemSalarial(listarLinhaMsgComprovanteEntradaDTO.getTipoMensagem());
		request.setCdSistema(listarLinhaMsgComprovanteEntradaDTO.getCentroCusto());
		request.setNrEvendoMensagemNegocio(0);
		request.setCdReciboGeradorMensagem(0);
		request.setCdIdiomaTextoMensagem(0);
		request.setNumeroOcorrencias(IAssocLinTipMsgServiceConstants.QTDE_CONSULTAS_LISTAR);
		
		response = getFactoryAdapter().getListarLinhaMsgComprovantePDCAdapter().invokeProcess(request);

		ListarLinhaMsgComprovanteSaidaDTO saidaDTO;
		for (int i=0; i<response.getOcorrenciasCount();i++){
			saidaDTO = new ListarLinhaMsgComprovanteSaidaDTO();
			saidaDTO.setCodMensagem(response.getCodMensagem());
			saidaDTO.setMensagem(response.getMensagem());
			saidaDTO.setCdTipoMensagem(response.getOcorrencias(i).getCdControleMensagemSalarial());
			saidaDTO.setDsTipoMensagem(response.getOcorrencias(i).getDsControleMensagemSalarial());
			saidaDTO.setPrefixo(response.getOcorrencias(i).getCdSistema());
			saidaDTO.setCodigoLinhaMsgComprovante(response.getOcorrencias(i).getNrEventoMensagemNegocio());			
			saidaDTO.setDsMensagemComprovante(response.getOcorrencias(i).getDsEventoMensagemNegocio());			
			saidaDTO.setCdRecurso(response.getOcorrencias(i).getCdReciboGeradorMensagem());
			saidaDTO.setDsRecurso(response.getOcorrencias(i).getDsReciboGeradorMensagem());			
			saidaDTO.setCdIdioma(response.getOcorrencias(i).getCdIdiomaTextoMensagem());
			saidaDTO.setDsIdioma(response.getOcorrencias(i).getDsIdiomaTextoMensagem());
			saidaDTO.setOrdemLinha(response.getOcorrencias(i).getNrOrdemLinhaMensagem());
			listaRetorno.add(saidaDTO);
			 
		}
		
		return listaRetorno;
	}
	
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.assoclinhatipomsg.IAssocLinhaTipoMsgService#incluirLinhaMsgComprovante(br.com.bradesco.web.pgit.service.business.assoclinhatipomsg.bean.IncluirLinhaMsgComprovanteEntradaDTO)
	 */
	public IncluirLinhaMsgComprovanteSaidaDTO incluirLinhaMsgComprovante (IncluirLinhaMsgComprovanteEntradaDTO entrada) {
		IncluirLinhaMsgComprovanteSaidaDTO saidaDto = new IncluirLinhaMsgComprovanteSaidaDTO();
		IncluirLinhaMsgComprovanteRequest request = new IncluirLinhaMsgComprovanteRequest();
		IncluirLinhaMsgComprovanteResponse response = new IncluirLinhaMsgComprovanteResponse();

		request.setCdControleMensagemSalarial(entrada.getTipoMensagem());
		request.setCdSistema(entrada.getPrefixo());
		request.setNrEventoMensagemNegocio(entrada.getCodigoMensagem());
		request.setCdReciboGeradorMensagem(entrada.getRecurso());
		request.setCdIdiomaTextoMensagem(entrada.getIdioma());
		request.setNrOrdemLinhaMensagem(PgitUtil.verificaLongNulo(entrada.getOrdemLinhaMensagem()));

		response = getFactoryAdapter().getIncluirLinhaMsgComprovantePDCAdapter().invokeProcess(request);

		saidaDto.setCodMensagem(response.getCodMensagem());
		saidaDto.setMensagem(response.getMensagem());

		return saidaDto;
	}
	
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.assoclinhatipomsg.IAssocLinhaTipoMsgService#alterarLinhaMsgComprovante(br.com.bradesco.web.pgit.service.business.assoclinhatipomsg.bean.AlterarLinhaMsgComprovanteEntradaDTO)
	 */
	public AlterarLinhaMsgComprovanteSaidaDTO alterarLinhaMsgComprovante (AlterarLinhaMsgComprovanteEntradaDTO alterarLinhaMsgComprovanteEntradaDTO) {
		
		AlterarLinhaMsgComprovanteSaidaDTO alterarLinhaMsgComprovanteSaidaDTO = new AlterarLinhaMsgComprovanteSaidaDTO();
		AlterarLinhaMsgComprovanteRequest request = new AlterarLinhaMsgComprovanteRequest();
		AlterarLinhaMsgComprovanteResponse response = new AlterarLinhaMsgComprovanteResponse();
		
		request.setCdControleMensagemSalarial(alterarLinhaMsgComprovanteEntradaDTO.getTipoMensagem());
		request.setCdSistema(alterarLinhaMsgComprovanteEntradaDTO.getPrefixo());
		request.setNrEventoMensagemNegocio(alterarLinhaMsgComprovanteEntradaDTO.getCodigoMensagem());
		request.setCdReciboGeradorMensagem(alterarLinhaMsgComprovanteEntradaDTO.getRecurso());
		request.setCdIdiomaTextoMensagem(alterarLinhaMsgComprovanteEntradaDTO.getIdioma());
		request.setNrOrdemLinhaMensagem(alterarLinhaMsgComprovanteEntradaDTO.getOrdemLinhaMensagem());
		
		response = getFactoryAdapter().getAlterarLinhaMsgComprovantePDCAdapter().invokeProcess(request);
				
		alterarLinhaMsgComprovanteSaidaDTO.setCodMensagem(response.getCodMensagem());
		alterarLinhaMsgComprovanteSaidaDTO.setMensagem(response.getMensagem());
					
		
		return alterarLinhaMsgComprovanteSaidaDTO;
		
	}
	
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.assoclinhatipomsg.IAssocLinhaTipoMsgService#excluirLinhaMsgComprovante(br.com.bradesco.web.pgit.service.business.assoclinhatipomsg.bean.ExcluirLinhaMsgComprovanteEntradaDTO)
	 */
	public ExcluirLinhaMsgComprovanteSaidaDTO excluirLinhaMsgComprovante (ExcluirLinhaMsgComprovanteEntradaDTO excluirLinhaMsgComprovanteEntradaDTO) {
		
		ExcluirLinhaMsgComprovanteSaidaDTO excluirLinhaMsgComprovanteSaidaDTO = new ExcluirLinhaMsgComprovanteSaidaDTO();
		ExcluirLinhaMsgComprovanteRequest excluirLinhaMsgComprovanteRequest = new ExcluirLinhaMsgComprovanteRequest();
		ExcluirLinhaMsgComprovanteResponse excluirLinhaMsgComprovanteResponse = new ExcluirLinhaMsgComprovanteResponse();
		
		excluirLinhaMsgComprovanteRequest.setCdControleMensagemSalarial(excluirLinhaMsgComprovanteEntradaDTO.getTipoMensagem());
		excluirLinhaMsgComprovanteRequest.setCdSistema(excluirLinhaMsgComprovanteEntradaDTO.getPrefixo());
		excluirLinhaMsgComprovanteRequest.setNrEventoMensagemNegocio(excluirLinhaMsgComprovanteEntradaDTO.getCodigoMensagem());
		excluirLinhaMsgComprovanteRequest.setCdReciboGeradorMensagem(excluirLinhaMsgComprovanteEntradaDTO.getRecurso());
		excluirLinhaMsgComprovanteRequest.setCdIdiomaTextoMensagem(excluirLinhaMsgComprovanteEntradaDTO.getIdioma());
		excluirLinhaMsgComprovanteRequest.setNumeroOcorrencias(0);
		
		excluirLinhaMsgComprovanteResponse = getFactoryAdapter().getExcluirLinhaMsgComprovantePDCAdapter().invokeProcess(excluirLinhaMsgComprovanteRequest);
		
		excluirLinhaMsgComprovanteSaidaDTO.setCodMensagem(excluirLinhaMsgComprovanteResponse.getCodMensagem());
		excluirLinhaMsgComprovanteSaidaDTO.setMensagem(excluirLinhaMsgComprovanteResponse.getMensagem());
		
		
		return excluirLinhaMsgComprovanteSaidaDTO;
	}
	
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.assoclinhatipomsg.IAssocLinhaTipoMsgService#detalharLinhaMsgComprovante(br.com.bradesco.web.pgit.service.business.assoclinhatipomsg.bean.DetalharLinhaMsgComprovanteEntradaDTO)
	 */
	public DetalharLinhaMsgComprovanteSaidaDTO detalharLinhaMsgComprovante (DetalharLinhaMsgComprovanteEntradaDTO detalharLinhaMsgComprovanteEntradaDTO) {
		
		DetalharLinhaMsgComprovanteSaidaDTO saidaDTO = new DetalharLinhaMsgComprovanteSaidaDTO();
		ConsultarDetalheLinhaMensagemRequest request = new ConsultarDetalheLinhaMensagemRequest();
		ConsultarDetalheLinhaMensagemResponse response = new ConsultarDetalheLinhaMensagemResponse();
		
		request.setCdControleMensagemSalarial(detalharLinhaMsgComprovanteEntradaDTO.getTipoMensagem());
		request.setCdSistema(detalharLinhaMsgComprovanteEntradaDTO.getPrefixo());
		request.setNrEventoMensagemNegocio(detalharLinhaMsgComprovanteEntradaDTO.getCodigoMensagem());
		request.setCdReciboGeradorMensagem(detalharLinhaMsgComprovanteEntradaDTO.getRecurso());
		request.setCdIdiomaTextoMensagem(detalharLinhaMsgComprovanteEntradaDTO.getIdioma());
		request.setQtConsultas(0);
		
		response = getFactoryAdapter().getConsultarDetalheLinhaMensagemPDCAdapter().invokeProcess(request);
		
		saidaDTO = new DetalharLinhaMsgComprovanteSaidaDTO();
		saidaDTO.setCodMensagem(response.getCodMensagem());
		saidaDTO.setMensagem(response.getMensagem());
		saidaDTO.setCdTipoMensagem(response.getOcorrencias(0).getCdControleMensagemSalarial());
		saidaDTO.setDsTipoMensagem(response.getOcorrencias(0).getDsControleMensagemSalarial());
		saidaDTO.setPrefixo(response.getOcorrencias(0).getCdSistema());
		saidaDTO.setCdcodigoMensagem(response.getOcorrencias(0).getNrEventoMensagemNegocio());
		saidaDTO.setDsCodigoMensagem(response.getOcorrencias(0).getDsEventoMensagemNegocio());
		saidaDTO.setCdRecurso(response.getOcorrencias(0).getCdReciboGeradorMensagem());
		saidaDTO.setDsRecurso(response.getOcorrencias(0).getDsReciboGeradorMensagem());
		saidaDTO.setCdIdioma(response.getOcorrencias(0).getCdIdiomaTextoMensagem());
		saidaDTO.setDsIdioma(response.getOcorrencias(0).getDsIdiomaTextoMensagem());
		saidaDTO.setOrdemLinhaMensagem(response.getOcorrencias(0).getNrOrdemLinhaMensagem());
		saidaDTO.setCdTipoCanalInclusao(response.getOcorrencias(0).getCdCanalInclusao());
		saidaDTO.setDsTipoCanalInclusao(response.getOcorrencias(0).getDsCanalInclusao());
		saidaDTO.setUsuarioInclusao(response.getOcorrencias(0).getCdAutenticacaoSegurancaInclusao());
		saidaDTO.setComplementoInclusao(response.getOcorrencias(0).getNrOperacaoFluxoInclusao());
		saidaDTO.setDataHoraInclusao(response.getOcorrencias(0).getHrInclusaoRegistro());
		saidaDTO.setCdTipoCanalManutencao(response.getOcorrencias(0).getCdCanalManutencao());
		saidaDTO.setDsTipoCanalManutencao(response.getOcorrencias(0).getDsCanalManutencao());
		saidaDTO.setUsuarioManutencao(response.getOcorrencias(0).getCdAutenticacaoSegurancaManutencao());
		saidaDTO.setComplementoManutencao(response.getOcorrencias(0).getNrOperacaoFluxoManutencao());
		saidaDTO.setDataHoraManutencao(response.getOcorrencias(0).getHrManutencaoRegistro());
		
		return saidaDTO;
	}
	
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.assoclinhatipomsg.IAssocLinhaTipoMsgService#consultarDescricaoMensagem(br.com.bradesco.web.pgit.service.business.tipocomprovante.bean.ConsultarDescricaoMensagemEntradaDTO)
	 */
	public ConsultarDescricaoMensagemSaidaDTO consultarDescricaoMensagem (ConsultarDescricaoMensagemEntradaDTO consultarDescricaoMensagemEntradaDTO){
		
		ConsultarDescricaoMensagemSaidaDTO saida = new ConsultarDescricaoMensagemSaidaDTO();
		ConsultarDescricaoMensagemRequest request = new ConsultarDescricaoMensagemRequest();
		ConsultarDescricaoMensagemResponse response = new ConsultarDescricaoMensagemResponse();
		
		request.setCdIdiomaTextoMensagem(consultarDescricaoMensagemEntradaDTO.getCdIdiomaTextoMensagem());
		request.setCdRecursoGeradorMensagem(consultarDescricaoMensagemEntradaDTO.getCdRecursoGeradorMensagem());
		request.setCdSistema(consultarDescricaoMensagemEntradaDTO.getCdSistema());
		request.setNrEventoMensagemNegocio(consultarDescricaoMensagemEntradaDTO.getNrEventoMensagemNegocio());
		
		response = getFactoryAdapter().getConsultarDescricaoMensagemPDCAdapter().invokeProcess(request);
		
		saida = new ConsultarDescricaoMensagemSaidaDTO();
		saida.setCodMensagem(response.getCodMensagem());
		saida.setDsEventoMensagemNegocio(response.getDsEventoMensagemNegocio());
		saida.setMensagem(response.getMensagem());
		saida.setNrEventoMensagemNegocio(response.getNrEventoMensagemNegocio());
		
		return saida;
		
	}
    
}

