/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/interfaz.ftl,v $
 * $Id: interfaz.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: interfaz.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.registrarassinaturaformaltcontrato;

import java.util.List;

import br.com.bradesco.web.pgit.service.business.registrarassinaturaformaltcontrato.bean.ConsultarAditivoContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.registrarassinaturaformaltcontrato.bean.ConsultarAditivoContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.registrarassinaturaformaltcontrato.bean.RegistrarAssinaturaContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.registrarassinaturaformaltcontrato.bean.RegistrarAssinaturaContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.registrarassinaturaformaltcontrato.bean.RegistrarFormalizacaoManContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.registrarassinaturaformaltcontrato.bean.RegistrarFormalizacaoManContratoSaidaDTO;

/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Interface do adaptador: RegistrarAssinaturaFormAltContrato
 * </p>
 * 
 * @comment CODIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public interface IRegistrarAssinaturaFormAltContratoService {
	
	/**
	 * Registrar assinatura contrato.
	 *
	 * @param entrada the entrada
	 * @return the registrar assinatura contrato saida dto
	 */
	RegistrarAssinaturaContratoSaidaDTO registrarAssinaturaContrato(RegistrarAssinaturaContratoEntradaDTO entrada);
	
	/**
	 * Consultar aditivo contrato.
	 *
	 * @param entradaDTO the entrada dto
	 * @return the list< consultar aditivo contrato saida dt o>
	 */
	List<ConsultarAditivoContratoSaidaDTO> consultarAditivoContrato(ConsultarAditivoContratoEntradaDTO  entradaDTO);
	
	/**
	 * Registrar formalizacao man contrato.
	 *
	 * @param entrada the entrada
	 * @return the registrar formalizacao man contrato saida dto
	 */
	RegistrarFormalizacaoManContratoSaidaDTO registrarFormalizacaoManContrato (RegistrarFormalizacaoManContratoEntradaDTO entrada);
}

