/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consutarcontrenvioformaliquidacao.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ConsutarContrEnvioFormaLiquidacaoRequest.
 * 
 * @version $Revision$ $Date$
 */
public class ConsutarContrEnvioFormaLiquidacaoRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _nrOcorrencias
     */
    private int _nrOcorrencias = 0;

    /**
     * keeps track of state for field: _nrOcorrencias
     */
    private boolean _has_nrOcorrencias;

    /**
     * Field _cdFormaLiquidacao
     */
    private int _cdFormaLiquidacao = 0;

    /**
     * keeps track of state for field: _cdFormaLiquidacao
     */
    private boolean _has_cdFormaLiquidacao;

    /**
     * Field _nrEnvioFormaLiquidacaoDoc
     */
    private int _nrEnvioFormaLiquidacaoDoc = 0;

    /**
     * keeps track of state for field: _nrEnvioFormaLiquidacaoDoc
     */
    private boolean _has_nrEnvioFormaLiquidacaoDoc;


      //----------------/
     //- Constructors -/
    //----------------/

    public ConsutarContrEnvioFormaLiquidacaoRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consutarcontrenvioformaliquidacao.request.ConsutarContrEnvioFormaLiquidacaoRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdFormaLiquidacao
     * 
     */
    public void deleteCdFormaLiquidacao()
    {
        this._has_cdFormaLiquidacao= false;
    } //-- void deleteCdFormaLiquidacao() 

    /**
     * Method deleteNrEnvioFormaLiquidacaoDoc
     * 
     */
    public void deleteNrEnvioFormaLiquidacaoDoc()
    {
        this._has_nrEnvioFormaLiquidacaoDoc= false;
    } //-- void deleteNrEnvioFormaLiquidacaoDoc() 

    /**
     * Method deleteNrOcorrencias
     * 
     */
    public void deleteNrOcorrencias()
    {
        this._has_nrOcorrencias= false;
    } //-- void deleteNrOcorrencias() 

    /**
     * Returns the value of field 'cdFormaLiquidacao'.
     * 
     * @return int
     * @return the value of field 'cdFormaLiquidacao'.
     */
    public int getCdFormaLiquidacao()
    {
        return this._cdFormaLiquidacao;
    } //-- int getCdFormaLiquidacao() 

    /**
     * Returns the value of field 'nrEnvioFormaLiquidacaoDoc'.
     * 
     * @return int
     * @return the value of field 'nrEnvioFormaLiquidacaoDoc'.
     */
    public int getNrEnvioFormaLiquidacaoDoc()
    {
        return this._nrEnvioFormaLiquidacaoDoc;
    } //-- int getNrEnvioFormaLiquidacaoDoc() 

    /**
     * Returns the value of field 'nrOcorrencias'.
     * 
     * @return int
     * @return the value of field 'nrOcorrencias'.
     */
    public int getNrOcorrencias()
    {
        return this._nrOcorrencias;
    } //-- int getNrOcorrencias() 

    /**
     * Method hasCdFormaLiquidacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFormaLiquidacao()
    {
        return this._has_cdFormaLiquidacao;
    } //-- boolean hasCdFormaLiquidacao() 

    /**
     * Method hasNrEnvioFormaLiquidacaoDoc
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrEnvioFormaLiquidacaoDoc()
    {
        return this._has_nrEnvioFormaLiquidacaoDoc;
    } //-- boolean hasNrEnvioFormaLiquidacaoDoc() 

    /**
     * Method hasNrOcorrencias
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrOcorrencias()
    {
        return this._has_nrOcorrencias;
    } //-- boolean hasNrOcorrencias() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdFormaLiquidacao'.
     * 
     * @param cdFormaLiquidacao the value of field
     * 'cdFormaLiquidacao'.
     */
    public void setCdFormaLiquidacao(int cdFormaLiquidacao)
    {
        this._cdFormaLiquidacao = cdFormaLiquidacao;
        this._has_cdFormaLiquidacao = true;
    } //-- void setCdFormaLiquidacao(int) 

    /**
     * Sets the value of field 'nrEnvioFormaLiquidacaoDoc'.
     * 
     * @param nrEnvioFormaLiquidacaoDoc the value of field
     * 'nrEnvioFormaLiquidacaoDoc'.
     */
    public void setNrEnvioFormaLiquidacaoDoc(int nrEnvioFormaLiquidacaoDoc)
    {
        this._nrEnvioFormaLiquidacaoDoc = nrEnvioFormaLiquidacaoDoc;
        this._has_nrEnvioFormaLiquidacaoDoc = true;
    } //-- void setNrEnvioFormaLiquidacaoDoc(int) 

    /**
     * Sets the value of field 'nrOcorrencias'.
     * 
     * @param nrOcorrencias the value of field 'nrOcorrencias'.
     */
    public void setNrOcorrencias(int nrOcorrencias)
    {
        this._nrOcorrencias = nrOcorrencias;
        this._has_nrOcorrencias = true;
    } //-- void setNrOcorrencias(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ConsutarContrEnvioFormaLiquidacaoRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consutarcontrenvioformaliquidacao.request.ConsutarContrEnvioFormaLiquidacaoRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consutarcontrenvioformaliquidacao.request.ConsutarContrEnvioFormaLiquidacaoRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consutarcontrenvioformaliquidacao.request.ConsutarContrEnvioFormaLiquidacaoRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consutarcontrenvioformaliquidacao.request.ConsutarContrEnvioFormaLiquidacaoRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
