/*
 * Nome: br.com.bradesco.web.pgit.service.business.solrecpagamentosconsulta.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.solrecpagamentosconsulta.bean;

import java.math.BigDecimal;

/**
 * Nome: SolicitarRecuparacaoPagtosConsultaEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class SolicitarRecuparacaoPagtosConsultaEntradaDTO {
	
	
	 	/** Atributo cdFinalidadeRecuperacao. */
	 	private Integer cdFinalidadeRecuperacao;
	    
    	/** Atributo cdSelecaoRecuperacao. */
    	private Integer cdSelecaoRecuperacao;
	    
    	/** Atributo cdSolicitacaoPagamento. */
    	private Integer cdSolicitacaoPagamento;
	    
    	/** Atributo cdPessoaJuridicaContrato. */
    	private Long cdPessoaJuridicaContrato;
	    
    	/** Atributo cdTipoContratoNegocio. */
    	private Integer cdTipoContratoNegocio;
	    
    	/** Atributo nrSequenciaContratoNegocio. */
    	private Long nrSequenciaContratoNegocio;
	    
    	/** Atributo cdBancoDebito. */
    	private Integer cdBancoDebito;
	    
    	/** Atributo cdAgenciaDebito. */
    	private Integer cdAgenciaDebito;
	    
    	/** Atributo cdContaDebito. */
    	private Long cdContaDebito;
	    
    	/** Atributo cdDigitoContaDebito. */
    	private String cdDigitoContaDebito;
	    
    	/** Atributo cdTipoContaDebito. */
    	private Integer cdTipoContaDebito;
	    
    	/** Atributo dtInicioPeriodoPesquisa. */
    	private String dtInicioPeriodoPesquisa;
	    
    	/** Atributo dtFimPeriodoPesquisa. */
    	private String dtFimPeriodoPesquisa;
	    
    	/** Atributo cdProduto. */
    	private Integer cdProduto;
	    
    	/** Atributo cdProdutoRelacionado. */
    	private Integer cdProdutoRelacionado;
	    
    	/** Atributo nrSequenciaArquivoRemessa. */
    	private Long nrSequenciaArquivoRemessa;
	    
    	/** Atributo cdPessoa. */
    	private Long cdPessoa;
	    
    	/** Atributo cdControlePagamentoInicial. */
    	private String cdControlePagamentoInicial;
	    
    	/** Atributo cdControlePagamentoFinal. */
    	private String cdControlePagamentoFinal;
	    
    	/** Atributo vlrInicio. */
    	private BigDecimal vlrInicio;
	    
    	/** Atributo vlFim. */
    	private BigDecimal vlFim;
	    
    	/** Atributo cdSituacaoPagamento. */
    	private Integer cdSituacaoPagamento;
	    
    	/** Atributo cdMotivoSituacaoPagamento. */
    	private Integer cdMotivoSituacaoPagamento;
	    
    	/** Atributo linhaDigitavel. */
    	private String linhaDigitavel;
	    
    	/** Atributo cdFavorecidoCliente. */
    	private Long cdFavorecidoCliente;
	    
    	/** Atributo cdTipoInscricaoRecebedor. */
    	private Integer cdTipoInscricaoRecebedor;
	    
    	/** Atributo cdInscricaoRecebedor. */
    	private Long cdInscricaoRecebedor;
	    
    	/** Atributo cdCpfCnpjRecebedor. */
    	private Long cdCpfCnpjRecebedor;
	    
    	/** Atributo cdFilialCnpjRecebedor. */
    	private Integer cdFilialCnpjRecebedor;
	    
    	/** Atributo cdControleCpfRecebedor. */
    	private Integer cdControleCpfRecebedor;
	    
    	/** Atributo cdInscricaoFavorecido. */
    	private Long cdInscricaoFavorecido;
	    
    	/** Atributo cdBancoCredito. */
    	private Integer cdBancoCredito;
	    
    	/** Atributo cdAgenciaCredito. */
    	private Integer cdAgenciaCredito;
	    
    	/** Atributo cdContaCredito. */
    	private Long cdContaCredito;
	    
    	/** Atributo digitoContaCredito. */
    	private String digitoContaCredito;
	    
    	/** Atributo cdTipoContaCredito. */
    	private Integer cdTipoContaCredito;
	    
    	/** Atributo dtRecuperacaoPagamento. */
    	private String dtRecuperacaoPagamento;
	    
    	/** Atributo dtNovoVencimentoPagamento. */
    	private String dtNovoVencimentoPagamento;
	    
    	/** Atributo vlTarifaPadrao. */
    	private BigDecimal vlTarifaPadrao;
	    
    	/** Atributo vlTarifaNegociacao. */
    	private BigDecimal vlTarifaNegociacao;
	    
    	/** Atributo percentualBonificacaoTarifa. */
    	private BigDecimal percentualBonificacaoTarifa;
	    
     	private Long contaSalario;
    	private Integer bancoSalario;
    	private Integer bancoPgtoDestino;
    	private Integer agenciaSalario;
    	private String digitoSalario;
    	private Integer destinoPagamento;
    	private Long contaPgtoDestino;
    	private String ispbPgtoDestino;
    	private Long loteInterno;
	    
		/**
		 * Get: cdAgenciaCredito.
		 *
		 * @return cdAgenciaCredito
		 */
		public Integer getCdAgenciaCredito() {
			return cdAgenciaCredito;
		}
		
		/**
		 * Set: cdAgenciaCredito.
		 *
		 * @param cdAgenciaCredito the cd agencia credito
		 */
		public void setCdAgenciaCredito(Integer cdAgenciaCredito) {
			this.cdAgenciaCredito = cdAgenciaCredito;
		}
		
		/**
		 * Get: cdAgenciaDebito.
		 *
		 * @return cdAgenciaDebito
		 */
		public Integer getCdAgenciaDebito() {
			return cdAgenciaDebito;
		}
		
		/**
		 * Set: cdAgenciaDebito.
		 *
		 * @param cdAgenciaDebito the cd agencia debito
		 */
		public void setCdAgenciaDebito(Integer cdAgenciaDebito) {
			this.cdAgenciaDebito = cdAgenciaDebito;
		}
		
		/**
		 * Get: cdBancoCredito.
		 *
		 * @return cdBancoCredito
		 */
		public Integer getCdBancoCredito() {
			return cdBancoCredito;
		}
		
		/**
		 * Set: cdBancoCredito.
		 *
		 * @param cdBancoCredito the cd banco credito
		 */
		public void setCdBancoCredito(Integer cdBancoCredito) {
			this.cdBancoCredito = cdBancoCredito;
		}
		
		/**
		 * Get: cdBancoDebito.
		 *
		 * @return cdBancoDebito
		 */
		public Integer getCdBancoDebito() {
			return cdBancoDebito;
		}
		
		/**
		 * Set: cdBancoDebito.
		 *
		 * @param cdBancoDebito the cd banco debito
		 */
		public void setCdBancoDebito(Integer cdBancoDebito) {
			this.cdBancoDebito = cdBancoDebito;
		}
		
		/**
		 * Get: cdContaCredito.
		 *
		 * @return cdContaCredito
		 */
		public Long getCdContaCredito() {
			return cdContaCredito;
		}
		
		/**
		 * Set: cdContaCredito.
		 *
		 * @param cdContaCredito the cd conta credito
		 */
		public void setCdContaCredito(Long cdContaCredito) {
			this.cdContaCredito = cdContaCredito;
		}
		
		/**
		 * Get: cdContaDebito.
		 *
		 * @return cdContaDebito
		 */
		public Long getCdContaDebito() {
			return cdContaDebito;
		}
		
		/**
		 * Set: cdContaDebito.
		 *
		 * @param cdContaDebito the cd conta debito
		 */
		public void setCdContaDebito(Long cdContaDebito) {
			this.cdContaDebito = cdContaDebito;
		}
		
		/**
		 * Get: cdControleCpfRecebedor.
		 *
		 * @return cdControleCpfRecebedor
		 */
		public Integer getCdControleCpfRecebedor() {
			return cdControleCpfRecebedor;
		}
		
		/**
		 * Set: cdControleCpfRecebedor.
		 *
		 * @param cdControleCpfRecebedor the cd controle cpf recebedor
		 */
		public void setCdControleCpfRecebedor(Integer cdControleCpfRecebedor) {
			this.cdControleCpfRecebedor = cdControleCpfRecebedor;
		}
		
		/**
		 * Get: cdControlePagamentoFinal.
		 *
		 * @return cdControlePagamentoFinal
		 */
		public String getCdControlePagamentoFinal() {
			return cdControlePagamentoFinal;
		}
		
		/**
		 * Set: cdControlePagamentoFinal.
		 *
		 * @param cdControlePagamentoFinal the cd controle pagamento final
		 */
		public void setCdControlePagamentoFinal(String cdControlePagamentoFinal) {
			this.cdControlePagamentoFinal = cdControlePagamentoFinal;
		}
		
		/**
		 * Get: cdControlePagamentoInicial.
		 *
		 * @return cdControlePagamentoInicial
		 */
		public String getCdControlePagamentoInicial() {
			return cdControlePagamentoInicial;
		}
		
		/**
		 * Set: cdControlePagamentoInicial.
		 *
		 * @param cdControlePagamentoInicial the cd controle pagamento inicial
		 */
		public void setCdControlePagamentoInicial(String cdControlePagamentoInicial) {
			this.cdControlePagamentoInicial = cdControlePagamentoInicial;
		}
		
		/**
		 * Get: cdCpfCnpjRecebedor.
		 *
		 * @return cdCpfCnpjRecebedor
		 */
		public Long getCdCpfCnpjRecebedor() {
			return cdCpfCnpjRecebedor;
		}
		
		/**
		 * Set: cdCpfCnpjRecebedor.
		 *
		 * @param cdCpfCnpjRecebedor the cd cpf cnpj recebedor
		 */
		public void setCdCpfCnpjRecebedor(Long cdCpfCnpjRecebedor) {
			this.cdCpfCnpjRecebedor = cdCpfCnpjRecebedor;
		}
		
		/**
		 * Get: cdDigitoContaDebito.
		 *
		 * @return cdDigitoContaDebito
		 */
		public String getCdDigitoContaDebito() {
			return cdDigitoContaDebito;
		}
		
		/**
		 * Set: cdDigitoContaDebito.
		 *
		 * @param cdDigitoContaDebito the cd digito conta debito
		 */
		public void setCdDigitoContaDebito(String cdDigitoContaDebito) {
			this.cdDigitoContaDebito = cdDigitoContaDebito;
		}
		
		/**
		 * Get: cdFavorecidoCliente.
		 *
		 * @return cdFavorecidoCliente
		 */
		public Long getCdFavorecidoCliente() {
			return cdFavorecidoCliente;
		}
		
		/**
		 * Set: cdFavorecidoCliente.
		 *
		 * @param cdFavorecidoCliente the cd favorecido cliente
		 */
		public void setCdFavorecidoCliente(Long cdFavorecidoCliente) {
			this.cdFavorecidoCliente = cdFavorecidoCliente;
		}
		
		/**
		 * Get: cdFilialCnpjRecebedor.
		 *
		 * @return cdFilialCnpjRecebedor
		 */
		public Integer getCdFilialCnpjRecebedor() {
			return cdFilialCnpjRecebedor;
		}
		
		/**
		 * Set: cdFilialCnpjRecebedor.
		 *
		 * @param cdFilialCnpjRecebedor the cd filial cnpj recebedor
		 */
		public void setCdFilialCnpjRecebedor(Integer cdFilialCnpjRecebedor) {
			this.cdFilialCnpjRecebedor = cdFilialCnpjRecebedor;
		}
		
		/**
		 * Get: cdFinalidadeRecuperacao.
		 *
		 * @return cdFinalidadeRecuperacao
		 */
		public Integer getCdFinalidadeRecuperacao() {
			return cdFinalidadeRecuperacao;
		}
		
		/**
		 * Set: cdFinalidadeRecuperacao.
		 *
		 * @param cdFinalidadeRecuperacao the cd finalidade recuperacao
		 */
		public void setCdFinalidadeRecuperacao(Integer cdFinalidadeRecuperacao) {
			this.cdFinalidadeRecuperacao = cdFinalidadeRecuperacao;
		}
		
		/**
		 * Get: cdInscricaoFavorecido.
		 *
		 * @return cdInscricaoFavorecido
		 */
		public Long getCdInscricaoFavorecido() {
			return cdInscricaoFavorecido;
		}
		
		/**
		 * Set: cdInscricaoFavorecido.
		 *
		 * @param cdInscricaoFavorecido the cd inscricao favorecido
		 */
		public void setCdInscricaoFavorecido(Long cdInscricaoFavorecido) {
			this.cdInscricaoFavorecido = cdInscricaoFavorecido;
		}
		
		/**
		 * Get: cdInscricaoRecebedor.
		 *
		 * @return cdInscricaoRecebedor
		 */
		public Long getCdInscricaoRecebedor() {
			return cdInscricaoRecebedor;
		}
		
		/**
		 * Set: cdInscricaoRecebedor.
		 *
		 * @param cdInscricaoRecebedor the cd inscricao recebedor
		 */
		public void setCdInscricaoRecebedor(Long cdInscricaoRecebedor) {
			this.cdInscricaoRecebedor = cdInscricaoRecebedor;
		}
		
		/**
		 * Get: cdMotivoSituacaoPagamento.
		 *
		 * @return cdMotivoSituacaoPagamento
		 */
		public Integer getCdMotivoSituacaoPagamento() {
			return cdMotivoSituacaoPagamento;
		}
		
		/**
		 * Set: cdMotivoSituacaoPagamento.
		 *
		 * @param cdMotivoSituacaoPagamento the cd motivo situacao pagamento
		 */
		public void setCdMotivoSituacaoPagamento(Integer cdMotivoSituacaoPagamento) {
			this.cdMotivoSituacaoPagamento = cdMotivoSituacaoPagamento;
		}
		
		/**
		 * Get: cdPessoa.
		 *
		 * @return cdPessoa
		 */
		public Long getCdPessoa() {
			return cdPessoa;
		}
		
		/**
		 * Set: cdPessoa.
		 *
		 * @param cdPessoa the cd pessoa
		 */
		public void setCdPessoa(Long cdPessoa) {
			this.cdPessoa = cdPessoa;
		}
		
		/**
		 * Get: cdPessoaJuridicaContrato.
		 *
		 * @return cdPessoaJuridicaContrato
		 */
		public Long getCdPessoaJuridicaContrato() {
			return cdPessoaJuridicaContrato;
		}
		
		/**
		 * Set: cdPessoaJuridicaContrato.
		 *
		 * @param cdPessoaJuridicaContrato the cd pessoa juridica contrato
		 */
		public void setCdPessoaJuridicaContrato(Long cdPessoaJuridicaContrato) {
			this.cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
		}
		
		/**
		 * Get: cdProduto.
		 *
		 * @return cdProduto
		 */
		public Integer getCdProduto() {
			return cdProduto;
		}
		
		/**
		 * Set: cdProduto.
		 *
		 * @param cdProduto the cd produto
		 */
		public void setCdProduto(Integer cdProduto) {
			this.cdProduto = cdProduto;
		}
		
		/**
		 * Get: cdProdutoRelacionado.
		 *
		 * @return cdProdutoRelacionado
		 */
		public Integer getCdProdutoRelacionado() {
			return cdProdutoRelacionado;
		}
		
		/**
		 * Set: cdProdutoRelacionado.
		 *
		 * @param cdProdutoRelacionado the cd produto relacionado
		 */
		public void setCdProdutoRelacionado(Integer cdProdutoRelacionado) {
			this.cdProdutoRelacionado = cdProdutoRelacionado;
		}
		
		/**
		 * Get: cdSelecaoRecuperacao.
		 *
		 * @return cdSelecaoRecuperacao
		 */
		public Integer getCdSelecaoRecuperacao() {
			return cdSelecaoRecuperacao;
		}
		
		/**
		 * Set: cdSelecaoRecuperacao.
		 *
		 * @param cdSelecaoRecuperacao the cd selecao recuperacao
		 */
		public void setCdSelecaoRecuperacao(Integer cdSelecaoRecuperacao) {
			this.cdSelecaoRecuperacao = cdSelecaoRecuperacao;
		}
		
		/**
		 * Get: cdSituacaoPagamento.
		 *
		 * @return cdSituacaoPagamento
		 */
		public Integer getCdSituacaoPagamento() {
			return cdSituacaoPagamento;
		}
		
		/**
		 * Set: cdSituacaoPagamento.
		 *
		 * @param cdSituacaoPagamento the cd situacao pagamento
		 */
		public void setCdSituacaoPagamento(Integer cdSituacaoPagamento) {
			this.cdSituacaoPagamento = cdSituacaoPagamento;
		}
		
		/**
		 * Get: cdSolicitacaoPagamento.
		 *
		 * @return cdSolicitacaoPagamento
		 */
		public Integer getCdSolicitacaoPagamento() {
			return cdSolicitacaoPagamento;
		}
		
		/**
		 * Set: cdSolicitacaoPagamento.
		 *
		 * @param cdSolicitacaoPagamento the cd solicitacao pagamento
		 */
		public void setCdSolicitacaoPagamento(Integer cdSolicitacaoPagamento) {
			this.cdSolicitacaoPagamento = cdSolicitacaoPagamento;
		}
		
		/**
		 * Get: cdTipoContaCredito.
		 *
		 * @return cdTipoContaCredito
		 */
		public Integer getCdTipoContaCredito() {
			return cdTipoContaCredito;
		}
		
		/**
		 * Set: cdTipoContaCredito.
		 *
		 * @param cdTipoContaCredito the cd tipo conta credito
		 */
		public void setCdTipoContaCredito(Integer cdTipoContaCredito) {
			this.cdTipoContaCredito = cdTipoContaCredito;
		}
		
		/**
		 * Get: cdTipoContaDebito.
		 *
		 * @return cdTipoContaDebito
		 */
		public Integer getCdTipoContaDebito() {
			return cdTipoContaDebito;
		}
		
		/**
		 * Set: cdTipoContaDebito.
		 *
		 * @param cdTipoContaDebito the cd tipo conta debito
		 */
		public void setCdTipoContaDebito(Integer cdTipoContaDebito) {
			this.cdTipoContaDebito = cdTipoContaDebito;
		}
		
		/**
		 * Get: cdTipoContratoNegocio.
		 *
		 * @return cdTipoContratoNegocio
		 */
		public Integer getCdTipoContratoNegocio() {
			return cdTipoContratoNegocio;
		}
		
		/**
		 * Set: cdTipoContratoNegocio.
		 *
		 * @param cdTipoContratoNegocio the cd tipo contrato negocio
		 */
		public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
			this.cdTipoContratoNegocio = cdTipoContratoNegocio;
		}
		
		/**
		 * Get: cdTipoInscricaoRecebedor.
		 *
		 * @return cdTipoInscricaoRecebedor
		 */
		public Integer getCdTipoInscricaoRecebedor() {
			return cdTipoInscricaoRecebedor;
		}
		
		/**
		 * Set: cdTipoInscricaoRecebedor.
		 *
		 * @param cdTipoInscricaoRecebedor the cd tipo inscricao recebedor
		 */
		public void setCdTipoInscricaoRecebedor(Integer cdTipoInscricaoRecebedor) {
			this.cdTipoInscricaoRecebedor = cdTipoInscricaoRecebedor;
		}
		
		/**
		 * Get: digitoContaCredito.
		 *
		 * @return digitoContaCredito
		 */
		public String getDigitoContaCredito() {
			return digitoContaCredito;
		}
		
		/**
		 * Set: digitoContaCredito.
		 *
		 * @param digitoContaCredito the digito conta credito
		 */
		public void setDigitoContaCredito(String digitoContaCredito) {
			this.digitoContaCredito = digitoContaCredito;
		}
		
		/**
		 * Get: dtFimPeriodoPesquisa.
		 *
		 * @return dtFimPeriodoPesquisa
		 */
		public String getDtFimPeriodoPesquisa() {
			return dtFimPeriodoPesquisa;
		}
		
		/**
		 * Set: dtFimPeriodoPesquisa.
		 *
		 * @param dtFimPeriodoPesquisa the dt fim periodo pesquisa
		 */
		public void setDtFimPeriodoPesquisa(String dtFimPeriodoPesquisa) {
			this.dtFimPeriodoPesquisa = dtFimPeriodoPesquisa;
		}
		
		/**
		 * Get: dtInicioPeriodoPesquisa.
		 *
		 * @return dtInicioPeriodoPesquisa
		 */
		public String getDtInicioPeriodoPesquisa() {
			return dtInicioPeriodoPesquisa;
		}
		
		/**
		 * Set: dtInicioPeriodoPesquisa.
		 *
		 * @param dtInicioPeriodoPesquisa the dt inicio periodo pesquisa
		 */
		public void setDtInicioPeriodoPesquisa(String dtInicioPeriodoPesquisa) {
			this.dtInicioPeriodoPesquisa = dtInicioPeriodoPesquisa;
		}
		
		/**
		 * Get: dtNovoVencimentoPagamento.
		 *
		 * @return dtNovoVencimentoPagamento
		 */
		public String getDtNovoVencimentoPagamento() {
			return dtNovoVencimentoPagamento;
		}
		
		/**
		 * Set: dtNovoVencimentoPagamento.
		 *
		 * @param dtNovoVencimentoPagamento the dt novo vencimento pagamento
		 */
		public void setDtNovoVencimentoPagamento(String dtNovoVencimentoPagamento) {
			this.dtNovoVencimentoPagamento = dtNovoVencimentoPagamento;
		}
		
		/**
		 * Get: dtRecuperacaoPagamento.
		 *
		 * @return dtRecuperacaoPagamento
		 */
		public String getDtRecuperacaoPagamento() {
			return dtRecuperacaoPagamento;
		}
		
		/**
		 * Set: dtRecuperacaoPagamento.
		 *
		 * @param dtRecuperacaoPagamento the dt recuperacao pagamento
		 */
		public void setDtRecuperacaoPagamento(String dtRecuperacaoPagamento) {
			this.dtRecuperacaoPagamento = dtRecuperacaoPagamento;
		}
		
		/**
		 * Get: linhaDigitavel.
		 *
		 * @return linhaDigitavel
		 */
		public String getLinhaDigitavel() {
			return linhaDigitavel;
		}
		
		/**
		 * Set: linhaDigitavel.
		 *
		 * @param linhaDigitavel the linha digitavel
		 */
		public void setLinhaDigitavel(String linhaDigitavel) {
			this.linhaDigitavel = linhaDigitavel;
		}
		
		/**
		 * Get: nrSequenciaArquivoRemessa.
		 *
		 * @return nrSequenciaArquivoRemessa
		 */
		public Long getNrSequenciaArquivoRemessa() {
			return nrSequenciaArquivoRemessa;
		}
		
		/**
		 * Set: nrSequenciaArquivoRemessa.
		 *
		 * @param nrSequenciaArquivoRemessa the nr sequencia arquivo remessa
		 */
		public void setNrSequenciaArquivoRemessa(Long nrSequenciaArquivoRemessa) {
			this.nrSequenciaArquivoRemessa = nrSequenciaArquivoRemessa;
		}
		
		/**
		 * Get: nrSequenciaContratoNegocio.
		 *
		 * @return nrSequenciaContratoNegocio
		 */
		public Long getNrSequenciaContratoNegocio() {
			return nrSequenciaContratoNegocio;
		}
		
		/**
		 * Set: nrSequenciaContratoNegocio.
		 *
		 * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
		 */
		public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
			this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
		}
		
		/**
		 * Get: percentualBonificacaoTarifa.
		 *
		 * @return percentualBonificacaoTarifa
		 */
		public BigDecimal getPercentualBonificacaoTarifa() {
			return percentualBonificacaoTarifa;
		}
		
		/**
		 * Set: percentualBonificacaoTarifa.
		 *
		 * @param percentualBonificacaoTarifa the percentual bonificacao tarifa
		 */
		public void setPercentualBonificacaoTarifa(
				BigDecimal percentualBonificacaoTarifa) {
			this.percentualBonificacaoTarifa = percentualBonificacaoTarifa;
		}
		
		/**
		 * Get: vlFim.
		 *
		 * @return vlFim
		 */
		public BigDecimal getVlFim() {
			return vlFim;
		}
		
		/**
		 * Set: vlFim.
		 *
		 * @param vlFim the vl fim
		 */
		public void setVlFim(BigDecimal vlFim) {
			this.vlFim = vlFim;
		}
		
		/**
		 * Get: vlrInicio.
		 *
		 * @return vlrInicio
		 */
		public BigDecimal getVlrInicio() {
			return vlrInicio;
		}
		
		/**
		 * Set: vlrInicio.
		 *
		 * @param vlrInicio the vlr inicio
		 */
		public void setVlrInicio(BigDecimal vlrInicio) {
			this.vlrInicio = vlrInicio;
		}
		
		/**
		 * Get: vlTarifaNegociacao.
		 *
		 * @return vlTarifaNegociacao
		 */
		public BigDecimal getVlTarifaNegociacao() {
			return vlTarifaNegociacao;
		}
		
		/**
		 * Set: vlTarifaNegociacao.
		 *
		 * @param vlTarifaNegociacao the vl tarifa negociacao
		 */
		public void setVlTarifaNegociacao(BigDecimal vlTarifaNegociacao) {
			this.vlTarifaNegociacao = vlTarifaNegociacao;
		}
		
		/**
		 * Get: vlTarifaPadrao.
		 *
		 * @return vlTarifaPadrao
		 */
		public BigDecimal getVlTarifaPadrao() {
			return vlTarifaPadrao;
		}
		
		/**
		 * Set: vlTarifaPadrao.
		 *
		 * @param vlTarifaPadrao the vl tarifa padrao
		 */
		public void setVlTarifaPadrao(BigDecimal vlTarifaPadrao) {
			this.vlTarifaPadrao = vlTarifaPadrao;
		}

		public Long getContaSalario() {
			return contaSalario;
		}

		public void setContaSalario(Long contaSalario) {
			this.contaSalario = contaSalario;
		}

		public Integer getBancoSalario() {
			return bancoSalario;
		}

		public void setBancoSalario(Integer bancoSalario) {
			this.bancoSalario = bancoSalario;
		}

		public Integer getAgenciaSalario() {
			return agenciaSalario;
		}

		public void setAgenciaSalario(Integer agenciaSalario) {
			this.agenciaSalario = agenciaSalario;
		}

		public String getDigitoSalario() {
			return digitoSalario;
		}

		public void setDigitoSalario(String digitoSalario) {
			this.digitoSalario = digitoSalario;
		}

		public Integer getDestinoPagamento() {
			return destinoPagamento;
		}

		public void setDestinoPagamento(Integer destinoPagamento) {
			this.destinoPagamento = destinoPagamento;
		}

		public Long getContaPgtoDestino() {
			return contaPgtoDestino;
		}

		public void setContaPgtoDestino(Long contaPgtoDestino) {
			this.contaPgtoDestino = contaPgtoDestino;
		}

		public String getIspbPgtoDestino() {
			return ispbPgtoDestino;
		}

		public void setIspbPgtoDestino(String ispbPgtoDestino) {
			this.ispbPgtoDestino = ispbPgtoDestino;
		}

		public Long getLoteInterno() {
			return loteInterno;
		}

		public void setLoteInterno(Long loteInterno) {
			this.loteInterno = loteInterno;
		}

		public Integer getBancoPgtoDestino() {
			return bancoPgtoDestino;
		}

		public void setBancoPgtoDestino(Integer bancoPgtoDestino) {
			this.bancoPgtoDestino = bancoPgtoDestino;
		}
	    
	    

}
