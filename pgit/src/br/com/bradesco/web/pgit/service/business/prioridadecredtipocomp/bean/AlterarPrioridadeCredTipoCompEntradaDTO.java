/*
 * Nome: br.com.bradesco.web.pgit.service.business.prioridadecredtipocomp.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.prioridadecredtipocomp.bean;

/**
 * Nome: AlterarPrioridadeCredTipoCompEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class AlterarPrioridadeCredTipoCompEntradaDTO {

	/** Atributo tipoCompromisso. */
	private int tipoCompromisso;
	
	/** Atributo prioridade. */
	private int prioridade;
	
	/** Atributo dataFimVigencia. */
	private String dataFimVigencia;
	
	/** Atributo dataInicioVigencia. */
	private String dataInicioVigencia;
	
	/** Atributo centroCustoPrioridadeProduto. */
	private String centroCustoPrioridadeProduto;		

	/**
	 * Get: centroCustoPrioridadeProduto.
	 *
	 * @return centroCustoPrioridadeProduto
	 */
	public String getCentroCustoPrioridadeProduto() {
	    return centroCustoPrioridadeProduto;
	}

	/**
	 * Set: centroCustoPrioridadeProduto.
	 *
	 * @param centroCustoPrioridadeProduto the centro custo prioridade produto
	 */
	public void setCentroCustoPrioridadeProduto(String centroCustoPrioridadeProduto) {
	    this.centroCustoPrioridadeProduto = centroCustoPrioridadeProduto;
	}

	/**
	 * Get: dataFimVigencia.
	 *
	 * @return dataFimVigencia
	 */
	public String getDataFimVigencia() {
		return dataFimVigencia;
	}

	/**
	 * Set: dataFimVigencia.
	 *
	 * @param dataFimVigencia the data fim vigencia
	 */
	public void setDataFimVigencia(String dataFimVigencia) {
		this.dataFimVigencia = dataFimVigencia;
	}

	/**
	 * Get: dataInicioVigencia.
	 *
	 * @return dataInicioVigencia
	 */
	public String getDataInicioVigencia() {
		return dataInicioVigencia;
	}

	/**
	 * Set: dataInicioVigencia.
	 *
	 * @param dataInicioVigencia the data inicio vigencia
	 */
	public void setDataInicioVigencia(String dataInicioVigencia) {
		this.dataInicioVigencia = dataInicioVigencia;
	}

	/**
	 * Get: tipoCompromisso.
	 *
	 * @return tipoCompromisso
	 */
	public int getTipoCompromisso() {
		return tipoCompromisso;
	}

	/**
	 * Set: tipoCompromisso.
	 *
	 * @param tipoCompromisso the tipo compromisso
	 */
	public void setTipoCompromisso(int tipoCompromisso) {
		this.tipoCompromisso = tipoCompromisso;
	}

	/**
	 * Get: prioridade.
	 *
	 * @return prioridade
	 */
	public int getPrioridade() {
		return prioridade;
	}

	/**
	 * Set: prioridade.
	 *
	 * @param prioridade the prioridade
	 */
	public void setPrioridade(int prioridade) {
		this.prioridade = prioridade;
	}
	
	

	
	
}
