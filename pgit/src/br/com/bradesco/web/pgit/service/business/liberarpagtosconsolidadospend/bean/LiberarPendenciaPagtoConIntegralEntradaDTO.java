/*
 * Nome: br.com.bradesco.web.pgit.service.business.liberarpagtosconsolidadospend.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.liberarpagtosconsolidadospend.bean;

/**
 * Nome: LiberarPendenciaPagtoConIntegralEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class LiberarPendenciaPagtoConIntegralEntradaDTO {
    
    /** Atributo nrOcorrencias. */
    private Integer nrOcorrencias;
    
    /** Atributo cdPessoaJuridica. */
    private Long cdPessoaJuridica;
    
    /** Atributo cdTipoContrato. */
    private Integer cdTipoContrato;
    
    /** Atributo nrSequenciaContrato. */
    private Long nrSequenciaContrato;
    
    /** Atributo cdCnpjCliente. */
    private Long cdCnpjCliente;
    
    /** Atributo cdCnpjFilial. */
    private Integer cdCnpjFilial;
    
    /** Atributo cdCnpjDigito. */
    private Integer cdCnpjDigito;
    
    /** Atributo nrRemessa. */
    private Long nrRemessa;
    
    /** Atributo dtCredito. */
    private String dtCredito;
    
    /** Atributo cdPessoaJuridicaDebito. */
    private Long cdPessoaJuridicaDebito;
    
    /** Atributo cdTipoContratoDebito. */
    private Integer cdTipoContratoDebito;
    
    /** Atributo nrSequenciaContratoDebito. */
    private Long nrSequenciaContratoDebito;
    
    /** Atributo cdProdutoServicoOperacao. */
    private Integer cdProdutoServicoOperacao;
    
    /** Atributo cdProdutoServicoRelacionado. */
    private Integer cdProdutoServicoRelacionado;
    
    /** Atributo cdSituacaoOperacaoPagamento. */
    private Integer cdSituacaoOperacaoPagamento;
    
    /** Atributo cdMotivoPendencia. */
    private Integer cdMotivoPendencia;
    
	/**
	 * Get: cdCnpjCliente.
	 *
	 * @return cdCnpjCliente
	 */
	public Long getCdCnpjCliente() {
		return cdCnpjCliente;
	}
	
	/**
	 * Set: cdCnpjCliente.
	 *
	 * @param cdCnpjCliente the cd cnpj cliente
	 */
	public void setCdCnpjCliente(Long cdCnpjCliente) {
		this.cdCnpjCliente = cdCnpjCliente;
	}
	
	/**
	 * Get: cdCnpjDigito.
	 *
	 * @return cdCnpjDigito
	 */
	public Integer getCdCnpjDigito() {
		return cdCnpjDigito;
	}
	
	/**
	 * Set: cdCnpjDigito.
	 *
	 * @param cdCnpjDigito the cd cnpj digito
	 */
	public void setCdCnpjDigito(Integer cdCnpjDigito) {
		this.cdCnpjDigito = cdCnpjDigito;
	}
	
	/**
	 * Get: cdCnpjFilial.
	 *
	 * @return cdCnpjFilial
	 */
	public Integer getCdCnpjFilial() {
		return cdCnpjFilial;
	}
	
	/**
	 * Set: cdCnpjFilial.
	 *
	 * @param cdCnpjFilial the cd cnpj filial
	 */
	public void setCdCnpjFilial(Integer cdCnpjFilial) {
		this.cdCnpjFilial = cdCnpjFilial;
	}
	
	/**
	 * Get: cdMotivoPendencia.
	 *
	 * @return cdMotivoPendencia
	 */
	public Integer getCdMotivoPendencia() {
		return cdMotivoPendencia;
	}
	
	/**
	 * Set: cdMotivoPendencia.
	 *
	 * @param cdMotivoPendencia the cd motivo pendencia
	 */
	public void setCdMotivoPendencia(Integer cdMotivoPendencia) {
		this.cdMotivoPendencia = cdMotivoPendencia;
	}
	
	/**
	 * Get: cdPessoaJuridica.
	 *
	 * @return cdPessoaJuridica
	 */
	public Long getCdPessoaJuridica() {
		return cdPessoaJuridica;
	}
	
	/**
	 * Set: cdPessoaJuridica.
	 *
	 * @param cdPessoaJuridica the cd pessoa juridica
	 */
	public void setCdPessoaJuridica(Long cdPessoaJuridica) {
		this.cdPessoaJuridica = cdPessoaJuridica;
	}
	
	/**
	 * Get: cdPessoaJuridicaDebito.
	 *
	 * @return cdPessoaJuridicaDebito
	 */
	public Long getCdPessoaJuridicaDebito() {
		return cdPessoaJuridicaDebito;
	}
	
	/**
	 * Set: cdPessoaJuridicaDebito.
	 *
	 * @param cdPessoaJuridicaDebito the cd pessoa juridica debito
	 */
	public void setCdPessoaJuridicaDebito(Long cdPessoaJuridicaDebito) {
		this.cdPessoaJuridicaDebito = cdPessoaJuridicaDebito;
	}
	
	/**
	 * Get: cdProdutoServicoOperacao.
	 *
	 * @return cdProdutoServicoOperacao
	 */
	public Integer getCdProdutoServicoOperacao() {
		return cdProdutoServicoOperacao;
	}
	
	/**
	 * Set: cdProdutoServicoOperacao.
	 *
	 * @param cdProdutoServicoOperacao the cd produto servico operacao
	 */
	public void setCdProdutoServicoOperacao(Integer cdProdutoServicoOperacao) {
		this.cdProdutoServicoOperacao = cdProdutoServicoOperacao;
	}
	
	/**
	 * Get: cdProdutoServicoRelacionado.
	 *
	 * @return cdProdutoServicoRelacionado
	 */
	public Integer getCdProdutoServicoRelacionado() {
		return cdProdutoServicoRelacionado;
	}
	
	/**
	 * Set: cdProdutoServicoRelacionado.
	 *
	 * @param cdProdutoServicoRelacionado the cd produto servico relacionado
	 */
	public void setCdProdutoServicoRelacionado(Integer cdProdutoServicoRelacionado) {
		this.cdProdutoServicoRelacionado = cdProdutoServicoRelacionado;
	}
	
	/**
	 * Get: cdSituacaoOperacaoPagamento.
	 *
	 * @return cdSituacaoOperacaoPagamento
	 */
	public Integer getCdSituacaoOperacaoPagamento() {
		return cdSituacaoOperacaoPagamento;
	}
	
	/**
	 * Set: cdSituacaoOperacaoPagamento.
	 *
	 * @param cdSituacaoOperacaoPagamento the cd situacao operacao pagamento
	 */
	public void setCdSituacaoOperacaoPagamento(Integer cdSituacaoOperacaoPagamento) {
		this.cdSituacaoOperacaoPagamento = cdSituacaoOperacaoPagamento;
	}
	
	/**
	 * Get: cdTipoContrato.
	 *
	 * @return cdTipoContrato
	 */
	public Integer getCdTipoContrato() {
		return cdTipoContrato;
	}
	
	/**
	 * Set: cdTipoContrato.
	 *
	 * @param cdTipoContrato the cd tipo contrato
	 */
	public void setCdTipoContrato(Integer cdTipoContrato) {
		this.cdTipoContrato = cdTipoContrato;
	}
	
	/**
	 * Get: cdTipoContratoDebito.
	 *
	 * @return cdTipoContratoDebito
	 */
	public Integer getCdTipoContratoDebito() {
		return cdTipoContratoDebito;
	}
	
	/**
	 * Set: cdTipoContratoDebito.
	 *
	 * @param cdTipoContratoDebito the cd tipo contrato debito
	 */
	public void setCdTipoContratoDebito(Integer cdTipoContratoDebito) {
		this.cdTipoContratoDebito = cdTipoContratoDebito;
	}
	
	/**
	 * Get: dtCredito.
	 *
	 * @return dtCredito
	 */
	public String getDtCredito() {
		return dtCredito;
	}
	
	/**
	 * Set: dtCredito.
	 *
	 * @param dtCredito the dt credito
	 */
	public void setDtCredito(String dtCredito) {
		this.dtCredito = dtCredito;
	}
	
	/**
	 * Get: nrOcorrencias.
	 *
	 * @return nrOcorrencias
	 */
	public Integer getNrOcorrencias() {
		return nrOcorrencias;
	}
	
	/**
	 * Set: nrOcorrencias.
	 *
	 * @param nrOcorrencias the nr ocorrencias
	 */
	public void setNrOcorrencias(Integer nrOcorrencias) {
		this.nrOcorrencias = nrOcorrencias;
	}
	
	/**
	 * Get: nrRemessa.
	 *
	 * @return nrRemessa
	 */
	public Long getNrRemessa() {
		return nrRemessa;
	}
	
	/**
	 * Set: nrRemessa.
	 *
	 * @param nrRemessa the nr remessa
	 */
	public void setNrRemessa(Long nrRemessa) {
		this.nrRemessa = nrRemessa;
	}
	
	/**
	 * Get: nrSequenciaContrato.
	 *
	 * @return nrSequenciaContrato
	 */
	public Long getNrSequenciaContrato() {
		return nrSequenciaContrato;
	}
	
	/**
	 * Set: nrSequenciaContrato.
	 *
	 * @param nrSequenciaContrato the nr sequencia contrato
	 */
	public void setNrSequenciaContrato(Long nrSequenciaContrato) {
		this.nrSequenciaContrato = nrSequenciaContrato;
	}
	
	/**
	 * Get: nrSequenciaContratoDebito.
	 *
	 * @return nrSequenciaContratoDebito
	 */
	public Long getNrSequenciaContratoDebito() {
		return nrSequenciaContratoDebito;
	}
	
	/**
	 * Set: nrSequenciaContratoDebito.
	 *
	 * @param nrSequenciaContratoDebito the nr sequencia contrato debito
	 */
	public void setNrSequenciaContratoDebito(Long nrSequenciaContratoDebito) {
		this.nrSequenciaContratoDebito = nrSequenciaContratoDebito;
	}
    
    
}
