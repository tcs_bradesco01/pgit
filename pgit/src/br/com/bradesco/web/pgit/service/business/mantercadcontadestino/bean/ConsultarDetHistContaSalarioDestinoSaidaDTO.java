/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantercadcontadestino.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantercadcontadestino.bean;

import br.com.bradesco.web.pgit.service.data.pdc.consultardethistcontasalariodestino.response.Ocorrencias;

/**
 * Nome: ConsultarDetHistContaSalarioDestinoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarDetHistContaSalarioDestinoSaidaDTO {
  
     /** Atributo tpManutencao. */
     private String tpManutencao;
     
     /** Atributo cdBancoDestino. */
     private Integer cdBancoDestino;
     
     /** Atributo dsBancoDestino. */
     private String dsBancoDestino;
     
     /** Atributo cdAgenciaDestino. */
     private Integer cdAgenciaDestino;
     
     /** Atributo dgAgenciaDestino. */
     private Integer dgAgenciaDestino;
     
     /** Atributo cdContaBancariaDestino. */
     private Long cdContaBancariaDestino;
     
     /** Atributo dgContaDestino. */
     private String dgContaDestino;
     
     /** Atributo dsAgenciaDestino. */
     private String dsAgenciaDestino;
     
     /** Atributo cdTipoContaDestino. */
     private String cdTipoContaDestino;
     
     /** Atributo cdUsuarioInclusao. */
     private String cdUsuarioInclusao;
     
     /** Atributo cdUsuarioInclusaoExter. */
     private String cdUsuarioInclusaoExter;
     
     /** Atributo dtInclusao. */
     private String dtInclusao;
     
     /** Atributo hrInclusao. */
     private String hrInclusao;
     
     /** Atributo cdOperCanalInclusao. */
     private String cdOperCanalInclusao;
     
     /** Atributo cdTipoCanalInclusao. */
     private Integer cdTipoCanalInclusao;
     
     /** Atributo dsCanalInclusao. */
     private String dsCanalInclusao;
     
     /** Atributo cdUsuarioManutencao. */
     private String cdUsuarioManutencao;
     
     /** Atributo cdUsuarioManutencaoExter. */
     private String cdUsuarioManutencaoExter;
     
     /** Atributo dtMantuencao. */
     private String dtMantuencao;
     
     /** Atributo hrManutencao. */
     private String hrManutencao;
     
     /** Atributo cdOperCanalManutencao. */
     private String cdOperCanalManutencao;
     
     /** Atributo cdTipoCanalManutencao. */
     private Integer cdTipoCanalManutencao;
     
     /** Atributo dsCanalManutencao. */
     private String dsCanalManutencao;
     
	/**
	 * Consultar det hist conta salario destino saida dto.
	 */
	public ConsultarDetHistContaSalarioDestinoSaidaDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	
	/**
	 * Consultar det hist conta salario destino saida dto.
	 *
	 * @param ocorrencia the ocorrencia
	 */
	public ConsultarDetHistContaSalarioDestinoSaidaDTO(Ocorrencias ocorrencia) {
		this.tpManutencao = ocorrencia.getTpManutencao();
		this.cdBancoDestino = ocorrencia.getCdBancoDestino();
		this.dsBancoDestino = ocorrencia.getDsBancoDestino();
		this.cdAgenciaDestino = ocorrencia.getCdAgenciaDestino();
		this.dgAgenciaDestino = ocorrencia.getDgAgenciaDestino();
		this.cdContaBancariaDestino = ocorrencia.getCdContaBancariaDestino();
		this.dgContaDestino = ocorrencia.getDgContaDestino();
		this.dsAgenciaDestino = ocorrencia.getDsAgenciaDestino();
		this.cdTipoContaDestino = ocorrencia.getCdTipoContaDestino();
		this.cdUsuarioInclusao = ocorrencia.getCdUsuarioInclusao();
		this.cdUsuarioInclusaoExter = ocorrencia.getCdUsuarioInclusaoExter();
		this.dtInclusao = ocorrencia.getDtInclusao();
		this.hrInclusao = ocorrencia.getHrInclusao();
		this.cdOperCanalInclusao = ocorrencia.getCdOperCanalInclusao();
		this.cdTipoCanalInclusao = ocorrencia.getCdTipoCanalInclusao();
		this.dsCanalInclusao = ocorrencia.getDsCanalInclusao();
		this.cdUsuarioManutencao = ocorrencia.getCdUsuarioManutencao();
		this.cdUsuarioManutencaoExter = ocorrencia.getCdUsuarioManutencaoExter();
		this.dtMantuencao = ocorrencia.getDtMantuencao();
		this.hrManutencao = ocorrencia.getHrManutencao();
		this.cdOperCanalManutencao = ocorrencia.getCdOperCanalManutencao();
		this.cdTipoCanalManutencao = ocorrencia.getCdTipoCanalManutencao();
		this.dsCanalManutencao = ocorrencia.getDsCanalManutencao();
	}

	/**
	 * Get: bancoDestino.
	 *
	 * @return bancoDestino
	 */
	public String getBancoDestino() {
		return dsBancoDestino.equals("") ? String.valueOf(cdBancoDestino) : cdBancoDestino == 0 ? "" : cdBancoDestino + " " + dsBancoDestino;
	}
	
	/**
	 * Get: agenciaDestino.
	 *
	 * @return agenciaDestino
	 */
	public String getAgenciaDestino() {
		if (dgAgenciaDestino != null && dsAgenciaDestino != null && !dsAgenciaDestino.trim().equals("")) {
			return cdAgenciaDestino == 0 ? "" : cdAgenciaDestino + "-" + dgAgenciaDestino + " " + dsAgenciaDestino;
		} else if (dgAgenciaDestino == null && dsAgenciaDestino != null && !dsAgenciaDestino.trim().equals("")){
			return cdAgenciaDestino == 0 ? "" : cdAgenciaDestino + " " + dsAgenciaDestino;
		} else if (dgAgenciaDestino != null ){
			return cdAgenciaDestino == 0 ? "" : cdAgenciaDestino + "-" + dgAgenciaDestino;
		} else{
			return String.valueOf(cdAgenciaDestino);
		}
	}
	
	/**
	 * Get: contaDigitoDestino.
	 *
	 * @return contaDigitoDestino
	 */
	public String getContaDigitoDestino() {
		if (dgContaDestino != null && !dgContaDestino.trim().equals("")) {
			return cdContaBancariaDestino == 0 ? "" : cdContaBancariaDestino + "-" +  this.dgContaDestino;
		}else {
			return String.valueOf(cdContaBancariaDestino);
		}
	}
	
	/**
	 * Get: dataHoraInclusao.
	 *
	 * @return dataHoraInclusao
	 */
	public String getDataHoraInclusao() {
		return dtInclusao.replace('.', '/') + " "+ hrInclusao.replace('.', ':');
	}
	
	/**
	 * Get: dataHoraManutencao.
	 *
	 * @return dataHoraManutencao
	 */
	public String getDataHoraManutencao() {
		return dtMantuencao.replace('.', '/') + " " + hrManutencao.replace('.', ':');
	}
	
	/**
	 * Get: tpDescricaoCanalManutencao.
	 *
	 * @return tpDescricaoCanalManutencao
	 */
	public String getTpDescricaoCanalManutencao() {
		return this.cdTipoCanalManutencao == 0 ? this.dsCanalManutencao : this.cdTipoCanalManutencao + "-" + this.dsCanalManutencao;
	}
	
	/**
	 * Get: tpDescricaoCanalInclusao.
	 *
	 * @return tpDescricaoCanalInclusao
	 */
	public String getTpDescricaoCanalInclusao() {
		return this.cdTipoCanalInclusao == 0 ? this.dsCanalInclusao : this.cdTipoCanalInclusao + "-" + this.dsCanalInclusao;
	}

	/**
	 * Get: usuarioInclusao.
	 *
	 * @return usuarioInclusao
	 */
	public String getUsuarioInclusao() {
		return cdUsuarioInclusao.trim().equals("0")  ? "" : cdUsuarioInclusao;
	}
	
	/**
	 * Get: usuarioManutencao.
	 *
	 * @return usuarioManutencao
	 */
	public String getUsuarioManutencao() {
		return cdUsuarioManutencao.trim().equals("0")  ? "" : cdUsuarioManutencao;
	}
	
	/**
	 * Get: cdOperCanalInclusao.
	 *
	 * @return cdOperCanalInclusao
	 */
	public String getCdOperCanalInclusao() {
		return cdOperCanalInclusao;
	}

	/**
	 * Set: cdOperCanalInclusao.
	 *
	 * @param cdOperCanalInclusao the cd oper canal inclusao
	 */
	public void setCdOperCanalInclusao(String cdOperCanalInclusao) {
		this.cdOperCanalInclusao = cdOperCanalInclusao;
	}

	/**
	 * Get: cdOperCanalManutencao.
	 *
	 * @return cdOperCanalManutencao
	 */
	public String getCdOperCanalManutencao() {
		return cdOperCanalManutencao;
	}

	/**
	 * Set: cdOperCanalManutencao.
	 *
	 * @param cdOperCanalManutencao the cd oper canal manutencao
	 */
	public void setCdOperCanalManutencao(String cdOperCanalManutencao) {
		this.cdOperCanalManutencao = cdOperCanalManutencao;
	}

	/**
	 * Get: cdTipoCanalInclusao.
	 *
	 * @return cdTipoCanalInclusao
	 */
	public Integer getCdTipoCanalInclusao() {
		return cdTipoCanalInclusao;
	}

	/**
	 * Set: cdTipoCanalInclusao.
	 *
	 * @param cdTipoCanalInclusao the cd tipo canal inclusao
	 */
	public void setCdTipoCanalInclusao(Integer cdTipoCanalInclusao) {
		this.cdTipoCanalInclusao = cdTipoCanalInclusao;
	}

	/**
	 * Get: cdTipoCanalManutencao.
	 *
	 * @return cdTipoCanalManutencao
	 */
	public Integer getCdTipoCanalManutencao() {
		return cdTipoCanalManutencao;
	}

	/**
	 * Set: cdTipoCanalManutencao.
	 *
	 * @param cdTipoCanalManutencao the cd tipo canal manutencao
	 */
	public void setCdTipoCanalManutencao(Integer cdTipoCanalManutencao) {
		this.cdTipoCanalManutencao = cdTipoCanalManutencao;
	}

	/**
	 * Get: cdUsuarioInclusao.
	 *
	 * @return cdUsuarioInclusao
	 */
	public String getCdUsuarioInclusao() {
		return cdUsuarioInclusao;
	}

	/**
	 * Set: cdUsuarioInclusao.
	 *
	 * @param cdUsuarioInclusao the cd usuario inclusao
	 */
	public void setCdUsuarioInclusao(String cdUsuarioInclusao) {
		this.cdUsuarioInclusao = cdUsuarioInclusao;
	}

	/**
	 * Get: cdUsuarioInclusaoExter.
	 *
	 * @return cdUsuarioInclusaoExter
	 */
	public String getCdUsuarioInclusaoExter() {
		return cdUsuarioInclusaoExter;
	}

	/**
	 * Set: cdUsuarioInclusaoExter.
	 *
	 * @param cdUsuarioInclusaoExter the cd usuario inclusao exter
	 */
	public void setCdUsuarioInclusaoExter(String cdUsuarioInclusaoExter) {
		this.cdUsuarioInclusaoExter = cdUsuarioInclusaoExter;
	}

	/**
	 * Get: cdUsuarioManutencao.
	 *
	 * @return cdUsuarioManutencao
	 */
	public String getCdUsuarioManutencao() {
		return cdUsuarioManutencao;
	}

	/**
	 * Set: cdUsuarioManutencao.
	 *
	 * @param cdUsuarioManutencao the cd usuario manutencao
	 */
	public void setCdUsuarioManutencao(String cdUsuarioManutencao) {
		this.cdUsuarioManutencao = cdUsuarioManutencao;
	}

	/**
	 * Get: cdUsuarioManutencaoExter.
	 *
	 * @return cdUsuarioManutencaoExter
	 */
	public String getCdUsuarioManutencaoExter() {
		return cdUsuarioManutencaoExter;
	}

	/**
	 * Set: cdUsuarioManutencaoExter.
	 *
	 * @param cdUsuarioManutencaoExter the cd usuario manutencao exter
	 */
	public void setCdUsuarioManutencaoExter(String cdUsuarioManutencaoExter) {
		this.cdUsuarioManutencaoExter = cdUsuarioManutencaoExter;
	}

	/**
	 * Get: dsCanalInclusao.
	 *
	 * @return dsCanalInclusao
	 */
	public String getDsCanalInclusao() {
		return dsCanalInclusao;
	}

	/**
	 * Set: dsCanalInclusao.
	 *
	 * @param dsCanalInclusao the ds canal inclusao
	 */
	public void setDsCanalInclusao(String dsCanalInclusao) {
		this.dsCanalInclusao = dsCanalInclusao;
	}

	/**
	 * Get: dsCanalManutencao.
	 *
	 * @return dsCanalManutencao
	 */
	public String getDsCanalManutencao() {
		return dsCanalManutencao;
	}

	/**
	 * Set: dsCanalManutencao.
	 *
	 * @param dsCanalManutencao the ds canal manutencao
	 */
	public void setDsCanalManutencao(String dsCanalManutencao) {
		this.dsCanalManutencao = dsCanalManutencao;
	}

	/**
	 * Get: dtInclusao.
	 *
	 * @return dtInclusao
	 */
	public String getDtInclusao() {
		return dtInclusao;
	}

	/**
	 * Set: dtInclusao.
	 *
	 * @param dtInclusao the dt inclusao
	 */
	public void setDtInclusao(String dtInclusao) {
		this.dtInclusao = dtInclusao;
	}

	/**
	 * Get: hrInclusao.
	 *
	 * @return hrInclusao
	 */
	public String getHrInclusao() {
		return hrInclusao;
	}

	/**
	 * Set: hrInclusao.
	 *
	 * @param hrInclusao the hr inclusao
	 */
	public void setHrInclusao(String hrInclusao) {
		this.hrInclusao = hrInclusao;
	}

	/**
	 * Get: hrManutencao.
	 *
	 * @return hrManutencao
	 */
	public String getHrManutencao() {
		return hrManutencao;
	}

	/**
	 * Set: hrManutencao.
	 *
	 * @param hrManutencao the hr manutencao
	 */
	public void setHrManutencao(String hrManutencao) {
		this.hrManutencao = hrManutencao;
	}

	/**
	 * Get: tpManutencao.
	 *
	 * @return tpManutencao
	 */
	public String getTpManutencao() {
		return tpManutencao;
	}

	/**
	 * Set: tpManutencao.
	 *
	 * @param tpManutencao the tp manutencao
	 */
	public void setTpManutencao(String tpManutencao) {
		this.tpManutencao = tpManutencao;
	}

	/**
	 * Get: cdAgenciaDestino.
	 *
	 * @return cdAgenciaDestino
	 */
	public Integer getCdAgenciaDestino() {
		return cdAgenciaDestino;
	}

	/**
	 * Set: cdAgenciaDestino.
	 *
	 * @param cdAgenciaDestino the cd agencia destino
	 */
	public void setCdAgenciaDestino(Integer cdAgenciaDestino) {
		this.cdAgenciaDestino = cdAgenciaDestino;
	}

	/**
	 * Get: cdBancoDestino.
	 *
	 * @return cdBancoDestino
	 */
	public Integer getCdBancoDestino() {
		return cdBancoDestino;
	}

	/**
	 * Set: cdBancoDestino.
	 *
	 * @param cdBancoDestino the cd banco destino
	 */
	public void setCdBancoDestino(Integer cdBancoDestino) {
		this.cdBancoDestino = cdBancoDestino;
	}

	/**
	 * Get: cdContaBancariaDestino.
	 *
	 * @return cdContaBancariaDestino
	 */
	public Long getCdContaBancariaDestino() {
		return cdContaBancariaDestino;
	}

	/**
	 * Set: cdContaBancariaDestino.
	 *
	 * @param cdContaBancariaDestino the cd conta bancaria destino
	 */
	public void setCdContaBancariaDestino(Long cdContaBancariaDestino) {
		this.cdContaBancariaDestino = cdContaBancariaDestino;
	}

	/**
	 * Get: cdTipoContaDestino.
	 *
	 * @return cdTipoContaDestino
	 */
	public String getCdTipoContaDestino() {
		return cdTipoContaDestino;
	}

	/**
	 * Set: cdTipoContaDestino.
	 *
	 * @param cdTipoContaDestino the cd tipo conta destino
	 */
	public void setCdTipoContaDestino(String cdTipoContaDestino) {
		this.cdTipoContaDestino = cdTipoContaDestino;
	}

	/**
	 * Get: dgAgenciaDestino.
	 *
	 * @return dgAgenciaDestino
	 */
	public Integer getDgAgenciaDestino() {
		return dgAgenciaDestino;
	}

	/**
	 * Set: dgAgenciaDestino.
	 *
	 * @param dgAgenciaDestino the dg agencia destino
	 */
	public void setDgAgenciaDestino(Integer dgAgenciaDestino) {
		this.dgAgenciaDestino = dgAgenciaDestino;
	}

	/**
	 * Get: dgContaDestino.
	 *
	 * @return dgContaDestino
	 */
	public String getDgContaDestino() {
		return dgContaDestino;
	}

	/**
	 * Set: dgContaDestino.
	 *
	 * @param dgContaDestino the dg conta destino
	 */
	public void setDgContaDestino(String dgContaDestino) {
		this.dgContaDestino = dgContaDestino;
	}

	/**
	 * Get: dsAgenciaDestino.
	 *
	 * @return dsAgenciaDestino
	 */
	public String getDsAgenciaDestino() {
		return dsAgenciaDestino;
	}

	/**
	 * Set: dsAgenciaDestino.
	 *
	 * @param dsAgenciaDestino the ds agencia destino
	 */
	public void setDsAgenciaDestino(String dsAgenciaDestino) {
		this.dsAgenciaDestino = dsAgenciaDestino;
	}

	/**
	 * Get: dsBancoDestino.
	 *
	 * @return dsBancoDestino
	 */
	public String getDsBancoDestino() {
		return dsBancoDestino;
	}

	/**
	 * Set: dsBancoDestino.
	 *
	 * @param dsBancoDestino the ds banco destino
	 */
	public void setDsBancoDestino(String dsBancoDestino) {
		this.dsBancoDestino = dsBancoDestino;
	}

	/**
	 * Get: dtMantuencao.
	 *
	 * @return dtMantuencao
	 */
	public String getDtMantuencao() {
		return dtMantuencao;
	}

	/**
	 * Set: dtMantuencao.
	 *
	 * @param dtMantuencao the dt mantuencao
	 */
	public void setDtMantuencao(String dtMantuencao) {
		this.dtMantuencao = dtMantuencao;
	}
}
