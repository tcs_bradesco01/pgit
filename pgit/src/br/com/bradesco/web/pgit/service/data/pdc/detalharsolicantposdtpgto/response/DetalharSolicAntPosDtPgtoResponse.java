/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.detalharsolicantposdtpgto.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.math.BigDecimal;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class DetalharSolicAntPosDtPgtoResponse.
 * 
 * @version $Revision$ $Date$
 */
public class DetalharSolicAntPosDtPgtoResponse implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _codMensagem
     */
    private java.lang.String _codMensagem;

    /**
     * Field _mensagem
     */
    private java.lang.String _mensagem;

    /**
     * Field _cdpessoaJuridicaContrato
     */
    private long _cdpessoaJuridicaContrato = 0;

    /**
     * keeps track of state for field: _cdpessoaJuridicaContrato
     */
    private boolean _has_cdpessoaJuridicaContrato;

    /**
     * Field _cdTipoContratoNegocio
     */
    private int _cdTipoContratoNegocio = 0;

    /**
     * keeps track of state for field: _cdTipoContratoNegocio
     */
    private boolean _has_cdTipoContratoNegocio;

    /**
     * Field _nrSequenciaContratoNegocio
     */
    private long _nrSequenciaContratoNegocio = 0;

    /**
     * keeps track of state for field: _nrSequenciaContratoNegocio
     */
    private boolean _has_nrSequenciaContratoNegocio;

    /**
     * Field _cdSolicitacaoPagamentoIntegrado
     */
    private int _cdSolicitacaoPagamentoIntegrado = 0;

    /**
     * keeps track of state for field:
     * _cdSolicitacaoPagamentoIntegrado
     */
    private boolean _has_cdSolicitacaoPagamentoIntegrado;

    /**
     * Field _nrSolicitacaoPagamentoIntegrado
     */
    private int _nrSolicitacaoPagamentoIntegrado = 0;

    /**
     * keeps track of state for field:
     * _nrSolicitacaoPagamentoIntegrado
     */
    private boolean _has_nrSolicitacaoPagamentoIntegrado;

    /**
     * Field _dsSituacaoSolicitaoPgto
     */
    private java.lang.String _dsSituacaoSolicitaoPgto;

    /**
     * Field _dsMotivoSolicitacao
     */
    private java.lang.String _dsMotivoSolicitacao;

    /**
     * Field _dsSolicitacao
     */
    private java.lang.String _dsSolicitacao;

    /**
     * Field _hrSolicitacao
     */
    private java.lang.String _hrSolicitacao;

    /**
     * Field _dtNovaDataAgenda
     */
    private java.lang.String _dtNovaDataAgenda;

    /**
     * Field _cdIndicadorAntecipPosterg
     */
    private int _cdIndicadorAntecipPosterg = 0;

    /**
     * keeps track of state for field: _cdIndicadorAntecipPosterg
     */
    private boolean _has_cdIndicadorAntecipPosterg;

    /**
     * Field _dsTipoSolicitacaoAntecipacaoPostergacao
     */
    private java.lang.String _dsTipoSolicitacaoAntecipacaoPostergacao;

    /**
     * Field _nrLoteInterno
     */
    private long _nrLoteInterno = 0;

    /**
     * keeps track of state for field: _nrLoteInterno
     */
    private boolean _has_nrLoteInterno;

    /**
     * Field _dsTipoLayoutArquivo
     */
    private java.lang.String _dsTipoLayoutArquivo;

    /**
     * Field _dtPgtoInicial
     */
    private java.lang.String _dtPgtoInicial;

    /**
     * Field _dtPgtoFinal
     */
    private java.lang.String _dtPgtoFinal;

    /**
     * Field _cdBancoDebito
     */
    private int _cdBancoDebito = 0;

    /**
     * keeps track of state for field: _cdBancoDebito
     */
    private boolean _has_cdBancoDebito;

    /**
     * Field _cdAgenciaDebito
     */
    private int _cdAgenciaDebito = 0;

    /**
     * keeps track of state for field: _cdAgenciaDebito
     */
    private boolean _has_cdAgenciaDebito;

    /**
     * Field _cdContaDebito
     */
    private long _cdContaDebito = 0;

    /**
     * keeps track of state for field: _cdContaDebito
     */
    private boolean _has_cdContaDebito;

    /**
     * Field _dsProdutoServicoOperacao
     */
    private java.lang.String _dsProdutoServicoOperacao;

    /**
     * Field _dsModalidadePgtoCliente
     */
    private java.lang.String _dsModalidadePgtoCliente;

    /**
     * Field _qtdeTotalPagtoPrevistoSoltc
     */
    private long _qtdeTotalPagtoPrevistoSoltc = 0;

    /**
     * keeps track of state for field: _qtdeTotalPagtoPrevistoSoltc
     */
    private boolean _has_qtdeTotalPagtoPrevistoSoltc;

    /**
     * Field _vlrTotPagtoPrevistoSolicitacao
     */
    private java.math.BigDecimal _vlrTotPagtoPrevistoSolicitacao = new java.math.BigDecimal("0");

    /**
     * Field _qtTotalPgtoEfetivadoSolicitacao
     */
    private long _qtTotalPgtoEfetivadoSolicitacao = 0;

    /**
     * keeps track of state for field:
     * _qtTotalPgtoEfetivadoSolicitacao
     */
    private boolean _has_qtTotalPgtoEfetivadoSolicitacao;

    /**
     * Field _vlTotalPgtoEfetuadoSolicitacao
     */
    private java.math.BigDecimal _vlTotalPgtoEfetuadoSolicitacao = new java.math.BigDecimal("0");

    /**
     * Field _cdCanalInclusao
     */
    private int _cdCanalInclusao = 0;

    /**
     * keeps track of state for field: _cdCanalInclusao
     */
    private boolean _has_cdCanalInclusao;

    /**
     * Field _dsCanalInclusao
     */
    private java.lang.String _dsCanalInclusao;

    /**
     * Field _cdAutenticacaoSegurancaInclusao
     */
    private java.lang.String _cdAutenticacaoSegurancaInclusao;

    /**
     * Field _nmOperacaoFluxoInclusao
     */
    private java.lang.String _nmOperacaoFluxoInclusao;

    /**
     * Field _hrInclusaoRegistro
     */
    private java.lang.String _hrInclusaoRegistro;

    /**
     * Field _cdCanalManutencao
     */
    private int _cdCanalManutencao = 0;

    /**
     * keeps track of state for field: _cdCanalManutencao
     */
    private boolean _has_cdCanalManutencao;

    /**
     * Field _dsCanalManutencao
     */
    private java.lang.String _dsCanalManutencao;

    /**
     * Field _cdAutenticacaoSegurancaManutencao
     */
    private java.lang.String _cdAutenticacaoSegurancaManutencao;

    /**
     * Field _nmOperacaoFluxoManutencao
     */
    private java.lang.String _nmOperacaoFluxoManutencao;

    /**
     * Field _hrManutencaoRegistro
     */
    private java.lang.String _hrManutencaoRegistro;


      //----------------/
     //- Constructors -/
    //----------------/

    public DetalharSolicAntPosDtPgtoResponse() 
     {
        super();
        setVlrTotPagtoPrevistoSolicitacao(new java.math.BigDecimal("0"));
        setVlTotalPgtoEfetuadoSolicitacao(new java.math.BigDecimal("0"));
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharsolicantposdtpgto.response.DetalharSolicAntPosDtPgtoResponse()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdAgenciaDebito
     * 
     */
    public void deleteCdAgenciaDebito()
    {
        this._has_cdAgenciaDebito= false;
    } //-- void deleteCdAgenciaDebito() 

    /**
     * Method deleteCdBancoDebito
     * 
     */
    public void deleteCdBancoDebito()
    {
        this._has_cdBancoDebito= false;
    } //-- void deleteCdBancoDebito() 

    /**
     * Method deleteCdCanalInclusao
     * 
     */
    public void deleteCdCanalInclusao()
    {
        this._has_cdCanalInclusao= false;
    } //-- void deleteCdCanalInclusao() 

    /**
     * Method deleteCdCanalManutencao
     * 
     */
    public void deleteCdCanalManutencao()
    {
        this._has_cdCanalManutencao= false;
    } //-- void deleteCdCanalManutencao() 

    /**
     * Method deleteCdContaDebito
     * 
     */
    public void deleteCdContaDebito()
    {
        this._has_cdContaDebito= false;
    } //-- void deleteCdContaDebito() 

    /**
     * Method deleteCdIndicadorAntecipPosterg
     * 
     */
    public void deleteCdIndicadorAntecipPosterg()
    {
        this._has_cdIndicadorAntecipPosterg= false;
    } //-- void deleteCdIndicadorAntecipPosterg() 

    /**
     * Method deleteCdSolicitacaoPagamentoIntegrado
     * 
     */
    public void deleteCdSolicitacaoPagamentoIntegrado()
    {
        this._has_cdSolicitacaoPagamentoIntegrado= false;
    } //-- void deleteCdSolicitacaoPagamentoIntegrado() 

    /**
     * Method deleteCdTipoContratoNegocio
     * 
     */
    public void deleteCdTipoContratoNegocio()
    {
        this._has_cdTipoContratoNegocio= false;
    } //-- void deleteCdTipoContratoNegocio() 

    /**
     * Method deleteCdpessoaJuridicaContrato
     * 
     */
    public void deleteCdpessoaJuridicaContrato()
    {
        this._has_cdpessoaJuridicaContrato= false;
    } //-- void deleteCdpessoaJuridicaContrato() 

    /**
     * Method deleteNrLoteInterno
     * 
     */
    public void deleteNrLoteInterno()
    {
        this._has_nrLoteInterno= false;
    } //-- void deleteNrLoteInterno() 

    /**
     * Method deleteNrSequenciaContratoNegocio
     * 
     */
    public void deleteNrSequenciaContratoNegocio()
    {
        this._has_nrSequenciaContratoNegocio= false;
    } //-- void deleteNrSequenciaContratoNegocio() 

    /**
     * Method deleteNrSolicitacaoPagamentoIntegrado
     * 
     */
    public void deleteNrSolicitacaoPagamentoIntegrado()
    {
        this._has_nrSolicitacaoPagamentoIntegrado= false;
    } //-- void deleteNrSolicitacaoPagamentoIntegrado() 

    /**
     * Method deleteQtTotalPgtoEfetivadoSolicitacao
     * 
     */
    public void deleteQtTotalPgtoEfetivadoSolicitacao()
    {
        this._has_qtTotalPgtoEfetivadoSolicitacao= false;
    } //-- void deleteQtTotalPgtoEfetivadoSolicitacao() 

    /**
     * Method deleteQtdeTotalPagtoPrevistoSoltc
     * 
     */
    public void deleteQtdeTotalPagtoPrevistoSoltc()
    {
        this._has_qtdeTotalPagtoPrevistoSoltc= false;
    } //-- void deleteQtdeTotalPagtoPrevistoSoltc() 

    /**
     * Returns the value of field 'cdAgenciaDebito'.
     * 
     * @return int
     * @return the value of field 'cdAgenciaDebito'.
     */
    public int getCdAgenciaDebito()
    {
        return this._cdAgenciaDebito;
    } //-- int getCdAgenciaDebito() 

    /**
     * Returns the value of field
     * 'cdAutenticacaoSegurancaInclusao'.
     * 
     * @return String
     * @return the value of field 'cdAutenticacaoSegurancaInclusao'.
     */
    public java.lang.String getCdAutenticacaoSegurancaInclusao()
    {
        return this._cdAutenticacaoSegurancaInclusao;
    } //-- java.lang.String getCdAutenticacaoSegurancaInclusao() 

    /**
     * Returns the value of field
     * 'cdAutenticacaoSegurancaManutencao'.
     * 
     * @return String
     * @return the value of field
     * 'cdAutenticacaoSegurancaManutencao'.
     */
    public java.lang.String getCdAutenticacaoSegurancaManutencao()
    {
        return this._cdAutenticacaoSegurancaManutencao;
    } //-- java.lang.String getCdAutenticacaoSegurancaManutencao() 

    /**
     * Returns the value of field 'cdBancoDebito'.
     * 
     * @return int
     * @return the value of field 'cdBancoDebito'.
     */
    public int getCdBancoDebito()
    {
        return this._cdBancoDebito;
    } //-- int getCdBancoDebito() 

    /**
     * Returns the value of field 'cdCanalInclusao'.
     * 
     * @return int
     * @return the value of field 'cdCanalInclusao'.
     */
    public int getCdCanalInclusao()
    {
        return this._cdCanalInclusao;
    } //-- int getCdCanalInclusao() 

    /**
     * Returns the value of field 'cdCanalManutencao'.
     * 
     * @return int
     * @return the value of field 'cdCanalManutencao'.
     */
    public int getCdCanalManutencao()
    {
        return this._cdCanalManutencao;
    } //-- int getCdCanalManutencao() 

    /**
     * Returns the value of field 'cdContaDebito'.
     * 
     * @return long
     * @return the value of field 'cdContaDebito'.
     */
    public long getCdContaDebito()
    {
        return this._cdContaDebito;
    } //-- long getCdContaDebito() 

    /**
     * Returns the value of field 'cdIndicadorAntecipPosterg'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorAntecipPosterg'.
     */
    public int getCdIndicadorAntecipPosterg()
    {
        return this._cdIndicadorAntecipPosterg;
    } //-- int getCdIndicadorAntecipPosterg() 

    /**
     * Returns the value of field
     * 'cdSolicitacaoPagamentoIntegrado'.
     * 
     * @return int
     * @return the value of field 'cdSolicitacaoPagamentoIntegrado'.
     */
    public int getCdSolicitacaoPagamentoIntegrado()
    {
        return this._cdSolicitacaoPagamentoIntegrado;
    } //-- int getCdSolicitacaoPagamentoIntegrado() 

    /**
     * Returns the value of field 'cdTipoContratoNegocio'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoNegocio'.
     */
    public int getCdTipoContratoNegocio()
    {
        return this._cdTipoContratoNegocio;
    } //-- int getCdTipoContratoNegocio() 

    /**
     * Returns the value of field 'cdpessoaJuridicaContrato'.
     * 
     * @return long
     * @return the value of field 'cdpessoaJuridicaContrato'.
     */
    public long getCdpessoaJuridicaContrato()
    {
        return this._cdpessoaJuridicaContrato;
    } //-- long getCdpessoaJuridicaContrato() 

    /**
     * Returns the value of field 'codMensagem'.
     * 
     * @return String
     * @return the value of field 'codMensagem'.
     */
    public java.lang.String getCodMensagem()
    {
        return this._codMensagem;
    } //-- java.lang.String getCodMensagem() 

    /**
     * Returns the value of field 'dsCanalInclusao'.
     * 
     * @return String
     * @return the value of field 'dsCanalInclusao'.
     */
    public java.lang.String getDsCanalInclusao()
    {
        return this._dsCanalInclusao;
    } //-- java.lang.String getDsCanalInclusao() 

    /**
     * Returns the value of field 'dsCanalManutencao'.
     * 
     * @return String
     * @return the value of field 'dsCanalManutencao'.
     */
    public java.lang.String getDsCanalManutencao()
    {
        return this._dsCanalManutencao;
    } //-- java.lang.String getDsCanalManutencao() 

    /**
     * Returns the value of field 'dsModalidadePgtoCliente'.
     * 
     * @return String
     * @return the value of field 'dsModalidadePgtoCliente'.
     */
    public java.lang.String getDsModalidadePgtoCliente()
    {
        return this._dsModalidadePgtoCliente;
    } //-- java.lang.String getDsModalidadePgtoCliente() 

    /**
     * Returns the value of field 'dsMotivoSolicitacao'.
     * 
     * @return String
     * @return the value of field 'dsMotivoSolicitacao'.
     */
    public java.lang.String getDsMotivoSolicitacao()
    {
        return this._dsMotivoSolicitacao;
    } //-- java.lang.String getDsMotivoSolicitacao() 

    /**
     * Returns the value of field 'dsProdutoServicoOperacao'.
     * 
     * @return String
     * @return the value of field 'dsProdutoServicoOperacao'.
     */
    public java.lang.String getDsProdutoServicoOperacao()
    {
        return this._dsProdutoServicoOperacao;
    } //-- java.lang.String getDsProdutoServicoOperacao() 

    /**
     * Returns the value of field 'dsSituacaoSolicitaoPgto'.
     * 
     * @return String
     * @return the value of field 'dsSituacaoSolicitaoPgto'.
     */
    public java.lang.String getDsSituacaoSolicitaoPgto()
    {
        return this._dsSituacaoSolicitaoPgto;
    } //-- java.lang.String getDsSituacaoSolicitaoPgto() 

    /**
     * Returns the value of field 'dsSolicitacao'.
     * 
     * @return String
     * @return the value of field 'dsSolicitacao'.
     */
    public java.lang.String getDsSolicitacao()
    {
        return this._dsSolicitacao;
    } //-- java.lang.String getDsSolicitacao() 

    /**
     * Returns the value of field 'dsTipoLayoutArquivo'.
     * 
     * @return String
     * @return the value of field 'dsTipoLayoutArquivo'.
     */
    public java.lang.String getDsTipoLayoutArquivo()
    {
        return this._dsTipoLayoutArquivo;
    } //-- java.lang.String getDsTipoLayoutArquivo() 

    /**
     * Returns the value of field
     * 'dsTipoSolicitacaoAntecipacaoPostergacao'.
     * 
     * @return String
     * @return the value of field
     * 'dsTipoSolicitacaoAntecipacaoPostergacao'.
     */
    public java.lang.String getDsTipoSolicitacaoAntecipacaoPostergacao()
    {
        return this._dsTipoSolicitacaoAntecipacaoPostergacao;
    } //-- java.lang.String getDsTipoSolicitacaoAntecipacaoPostergacao() 

    /**
     * Returns the value of field 'dtNovaDataAgenda'.
     * 
     * @return String
     * @return the value of field 'dtNovaDataAgenda'.
     */
    public java.lang.String getDtNovaDataAgenda()
    {
        return this._dtNovaDataAgenda;
    } //-- java.lang.String getDtNovaDataAgenda() 

    /**
     * Returns the value of field 'dtPgtoFinal'.
     * 
     * @return String
     * @return the value of field 'dtPgtoFinal'.
     */
    public java.lang.String getDtPgtoFinal()
    {
        return this._dtPgtoFinal;
    } //-- java.lang.String getDtPgtoFinal() 

    /**
     * Returns the value of field 'dtPgtoInicial'.
     * 
     * @return String
     * @return the value of field 'dtPgtoInicial'.
     */
    public java.lang.String getDtPgtoInicial()
    {
        return this._dtPgtoInicial;
    } //-- java.lang.String getDtPgtoInicial() 

    /**
     * Returns the value of field 'hrInclusaoRegistro'.
     * 
     * @return String
     * @return the value of field 'hrInclusaoRegistro'.
     */
    public java.lang.String getHrInclusaoRegistro()
    {
        return this._hrInclusaoRegistro;
    } //-- java.lang.String getHrInclusaoRegistro() 

    /**
     * Returns the value of field 'hrManutencaoRegistro'.
     * 
     * @return String
     * @return the value of field 'hrManutencaoRegistro'.
     */
    public java.lang.String getHrManutencaoRegistro()
    {
        return this._hrManutencaoRegistro;
    } //-- java.lang.String getHrManutencaoRegistro() 

    /**
     * Returns the value of field 'hrSolicitacao'.
     * 
     * @return String
     * @return the value of field 'hrSolicitacao'.
     */
    public java.lang.String getHrSolicitacao()
    {
        return this._hrSolicitacao;
    } //-- java.lang.String getHrSolicitacao() 

    /**
     * Returns the value of field 'mensagem'.
     * 
     * @return String
     * @return the value of field 'mensagem'.
     */
    public java.lang.String getMensagem()
    {
        return this._mensagem;
    } //-- java.lang.String getMensagem() 

    /**
     * Returns the value of field 'nmOperacaoFluxoInclusao'.
     * 
     * @return String
     * @return the value of field 'nmOperacaoFluxoInclusao'.
     */
    public java.lang.String getNmOperacaoFluxoInclusao()
    {
        return this._nmOperacaoFluxoInclusao;
    } //-- java.lang.String getNmOperacaoFluxoInclusao() 

    /**
     * Returns the value of field 'nmOperacaoFluxoManutencao'.
     * 
     * @return String
     * @return the value of field 'nmOperacaoFluxoManutencao'.
     */
    public java.lang.String getNmOperacaoFluxoManutencao()
    {
        return this._nmOperacaoFluxoManutencao;
    } //-- java.lang.String getNmOperacaoFluxoManutencao() 

    /**
     * Returns the value of field 'nrLoteInterno'.
     * 
     * @return long
     * @return the value of field 'nrLoteInterno'.
     */
    public long getNrLoteInterno()
    {
        return this._nrLoteInterno;
    } //-- long getNrLoteInterno() 

    /**
     * Returns the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @return long
     * @return the value of field 'nrSequenciaContratoNegocio'.
     */
    public long getNrSequenciaContratoNegocio()
    {
        return this._nrSequenciaContratoNegocio;
    } //-- long getNrSequenciaContratoNegocio() 

    /**
     * Returns the value of field
     * 'nrSolicitacaoPagamentoIntegrado'.
     * 
     * @return int
     * @return the value of field 'nrSolicitacaoPagamentoIntegrado'.
     */
    public int getNrSolicitacaoPagamentoIntegrado()
    {
        return this._nrSolicitacaoPagamentoIntegrado;
    } //-- int getNrSolicitacaoPagamentoIntegrado() 

    /**
     * Returns the value of field
     * 'qtTotalPgtoEfetivadoSolicitacao'.
     * 
     * @return long
     * @return the value of field 'qtTotalPgtoEfetivadoSolicitacao'.
     */
    public long getQtTotalPgtoEfetivadoSolicitacao()
    {
        return this._qtTotalPgtoEfetivadoSolicitacao;
    } //-- long getQtTotalPgtoEfetivadoSolicitacao() 

    /**
     * Returns the value of field 'qtdeTotalPagtoPrevistoSoltc'.
     * 
     * @return long
     * @return the value of field 'qtdeTotalPagtoPrevistoSoltc'.
     */
    public long getQtdeTotalPagtoPrevistoSoltc()
    {
        return this._qtdeTotalPagtoPrevistoSoltc;
    } //-- long getQtdeTotalPagtoPrevistoSoltc() 

    /**
     * Returns the value of field 'vlTotalPgtoEfetuadoSolicitacao'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlTotalPgtoEfetuadoSolicitacao'.
     */
    public java.math.BigDecimal getVlTotalPgtoEfetuadoSolicitacao()
    {
        return this._vlTotalPgtoEfetuadoSolicitacao;
    } //-- java.math.BigDecimal getVlTotalPgtoEfetuadoSolicitacao() 

    /**
     * Returns the value of field 'vlrTotPagtoPrevistoSolicitacao'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlrTotPagtoPrevistoSolicitacao'.
     */
    public java.math.BigDecimal getVlrTotPagtoPrevistoSolicitacao()
    {
        return this._vlrTotPagtoPrevistoSolicitacao;
    } //-- java.math.BigDecimal getVlrTotPagtoPrevistoSolicitacao() 

    /**
     * Method hasCdAgenciaDebito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgenciaDebito()
    {
        return this._has_cdAgenciaDebito;
    } //-- boolean hasCdAgenciaDebito() 

    /**
     * Method hasCdBancoDebito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdBancoDebito()
    {
        return this._has_cdBancoDebito;
    } //-- boolean hasCdBancoDebito() 

    /**
     * Method hasCdCanalInclusao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCanalInclusao()
    {
        return this._has_cdCanalInclusao;
    } //-- boolean hasCdCanalInclusao() 

    /**
     * Method hasCdCanalManutencao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCanalManutencao()
    {
        return this._has_cdCanalManutencao;
    } //-- boolean hasCdCanalManutencao() 

    /**
     * Method hasCdContaDebito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdContaDebito()
    {
        return this._has_cdContaDebito;
    } //-- boolean hasCdContaDebito() 

    /**
     * Method hasCdIndicadorAntecipPosterg
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorAntecipPosterg()
    {
        return this._has_cdIndicadorAntecipPosterg;
    } //-- boolean hasCdIndicadorAntecipPosterg() 

    /**
     * Method hasCdSolicitacaoPagamentoIntegrado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSolicitacaoPagamentoIntegrado()
    {
        return this._has_cdSolicitacaoPagamentoIntegrado;
    } //-- boolean hasCdSolicitacaoPagamentoIntegrado() 

    /**
     * Method hasCdTipoContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoNegocio()
    {
        return this._has_cdTipoContratoNegocio;
    } //-- boolean hasCdTipoContratoNegocio() 

    /**
     * Method hasCdpessoaJuridicaContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdpessoaJuridicaContrato()
    {
        return this._has_cdpessoaJuridicaContrato;
    } //-- boolean hasCdpessoaJuridicaContrato() 

    /**
     * Method hasNrLoteInterno
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrLoteInterno()
    {
        return this._has_nrLoteInterno;
    } //-- boolean hasNrLoteInterno() 

    /**
     * Method hasNrSequenciaContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaContratoNegocio()
    {
        return this._has_nrSequenciaContratoNegocio;
    } //-- boolean hasNrSequenciaContratoNegocio() 

    /**
     * Method hasNrSolicitacaoPagamentoIntegrado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSolicitacaoPagamentoIntegrado()
    {
        return this._has_nrSolicitacaoPagamentoIntegrado;
    } //-- boolean hasNrSolicitacaoPagamentoIntegrado() 

    /**
     * Method hasQtTotalPgtoEfetivadoSolicitacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtTotalPgtoEfetivadoSolicitacao()
    {
        return this._has_qtTotalPgtoEfetivadoSolicitacao;
    } //-- boolean hasQtTotalPgtoEfetivadoSolicitacao() 

    /**
     * Method hasQtdeTotalPagtoPrevistoSoltc
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtdeTotalPagtoPrevistoSoltc()
    {
        return this._has_qtdeTotalPagtoPrevistoSoltc;
    } //-- boolean hasQtdeTotalPagtoPrevistoSoltc() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdAgenciaDebito'.
     * 
     * @param cdAgenciaDebito the value of field 'cdAgenciaDebito'.
     */
    public void setCdAgenciaDebito(int cdAgenciaDebito)
    {
        this._cdAgenciaDebito = cdAgenciaDebito;
        this._has_cdAgenciaDebito = true;
    } //-- void setCdAgenciaDebito(int) 

    /**
     * Sets the value of field 'cdAutenticacaoSegurancaInclusao'.
     * 
     * @param cdAutenticacaoSegurancaInclusao the value of field
     * 'cdAutenticacaoSegurancaInclusao'.
     */
    public void setCdAutenticacaoSegurancaInclusao(java.lang.String cdAutenticacaoSegurancaInclusao)
    {
        this._cdAutenticacaoSegurancaInclusao = cdAutenticacaoSegurancaInclusao;
    } //-- void setCdAutenticacaoSegurancaInclusao(java.lang.String) 

    /**
     * Sets the value of field 'cdAutenticacaoSegurancaManutencao'.
     * 
     * @param cdAutenticacaoSegurancaManutencao the value of field
     * 'cdAutenticacaoSegurancaManutencao'.
     */
    public void setCdAutenticacaoSegurancaManutencao(java.lang.String cdAutenticacaoSegurancaManutencao)
    {
        this._cdAutenticacaoSegurancaManutencao = cdAutenticacaoSegurancaManutencao;
    } //-- void setCdAutenticacaoSegurancaManutencao(java.lang.String) 

    /**
     * Sets the value of field 'cdBancoDebito'.
     * 
     * @param cdBancoDebito the value of field 'cdBancoDebito'.
     */
    public void setCdBancoDebito(int cdBancoDebito)
    {
        this._cdBancoDebito = cdBancoDebito;
        this._has_cdBancoDebito = true;
    } //-- void setCdBancoDebito(int) 

    /**
     * Sets the value of field 'cdCanalInclusao'.
     * 
     * @param cdCanalInclusao the value of field 'cdCanalInclusao'.
     */
    public void setCdCanalInclusao(int cdCanalInclusao)
    {
        this._cdCanalInclusao = cdCanalInclusao;
        this._has_cdCanalInclusao = true;
    } //-- void setCdCanalInclusao(int) 

    /**
     * Sets the value of field 'cdCanalManutencao'.
     * 
     * @param cdCanalManutencao the value of field
     * 'cdCanalManutencao'.
     */
    public void setCdCanalManutencao(int cdCanalManutencao)
    {
        this._cdCanalManutencao = cdCanalManutencao;
        this._has_cdCanalManutencao = true;
    } //-- void setCdCanalManutencao(int) 

    /**
     * Sets the value of field 'cdContaDebito'.
     * 
     * @param cdContaDebito the value of field 'cdContaDebito'.
     */
    public void setCdContaDebito(long cdContaDebito)
    {
        this._cdContaDebito = cdContaDebito;
        this._has_cdContaDebito = true;
    } //-- void setCdContaDebito(long) 

    /**
     * Sets the value of field 'cdIndicadorAntecipPosterg'.
     * 
     * @param cdIndicadorAntecipPosterg the value of field
     * 'cdIndicadorAntecipPosterg'.
     */
    public void setCdIndicadorAntecipPosterg(int cdIndicadorAntecipPosterg)
    {
        this._cdIndicadorAntecipPosterg = cdIndicadorAntecipPosterg;
        this._has_cdIndicadorAntecipPosterg = true;
    } //-- void setCdIndicadorAntecipPosterg(int) 

    /**
     * Sets the value of field 'cdSolicitacaoPagamentoIntegrado'.
     * 
     * @param cdSolicitacaoPagamentoIntegrado the value of field
     * 'cdSolicitacaoPagamentoIntegrado'.
     */
    public void setCdSolicitacaoPagamentoIntegrado(int cdSolicitacaoPagamentoIntegrado)
    {
        this._cdSolicitacaoPagamentoIntegrado = cdSolicitacaoPagamentoIntegrado;
        this._has_cdSolicitacaoPagamentoIntegrado = true;
    } //-- void setCdSolicitacaoPagamentoIntegrado(int) 

    /**
     * Sets the value of field 'cdTipoContratoNegocio'.
     * 
     * @param cdTipoContratoNegocio the value of field
     * 'cdTipoContratoNegocio'.
     */
    public void setCdTipoContratoNegocio(int cdTipoContratoNegocio)
    {
        this._cdTipoContratoNegocio = cdTipoContratoNegocio;
        this._has_cdTipoContratoNegocio = true;
    } //-- void setCdTipoContratoNegocio(int) 

    /**
     * Sets the value of field 'cdpessoaJuridicaContrato'.
     * 
     * @param cdpessoaJuridicaContrato the value of field
     * 'cdpessoaJuridicaContrato'.
     */
    public void setCdpessoaJuridicaContrato(long cdpessoaJuridicaContrato)
    {
        this._cdpessoaJuridicaContrato = cdpessoaJuridicaContrato;
        this._has_cdpessoaJuridicaContrato = true;
    } //-- void setCdpessoaJuridicaContrato(long) 

    /**
     * Sets the value of field 'codMensagem'.
     * 
     * @param codMensagem the value of field 'codMensagem'.
     */
    public void setCodMensagem(java.lang.String codMensagem)
    {
        this._codMensagem = codMensagem;
    } //-- void setCodMensagem(java.lang.String) 

    /**
     * Sets the value of field 'dsCanalInclusao'.
     * 
     * @param dsCanalInclusao the value of field 'dsCanalInclusao'.
     */
    public void setDsCanalInclusao(java.lang.String dsCanalInclusao)
    {
        this._dsCanalInclusao = dsCanalInclusao;
    } //-- void setDsCanalInclusao(java.lang.String) 

    /**
     * Sets the value of field 'dsCanalManutencao'.
     * 
     * @param dsCanalManutencao the value of field
     * 'dsCanalManutencao'.
     */
    public void setDsCanalManutencao(java.lang.String dsCanalManutencao)
    {
        this._dsCanalManutencao = dsCanalManutencao;
    } //-- void setDsCanalManutencao(java.lang.String) 

    /**
     * Sets the value of field 'dsModalidadePgtoCliente'.
     * 
     * @param dsModalidadePgtoCliente the value of field
     * 'dsModalidadePgtoCliente'.
     */
    public void setDsModalidadePgtoCliente(java.lang.String dsModalidadePgtoCliente)
    {
        this._dsModalidadePgtoCliente = dsModalidadePgtoCliente;
    } //-- void setDsModalidadePgtoCliente(java.lang.String) 

    /**
     * Sets the value of field 'dsMotivoSolicitacao'.
     * 
     * @param dsMotivoSolicitacao the value of field
     * 'dsMotivoSolicitacao'.
     */
    public void setDsMotivoSolicitacao(java.lang.String dsMotivoSolicitacao)
    {
        this._dsMotivoSolicitacao = dsMotivoSolicitacao;
    } //-- void setDsMotivoSolicitacao(java.lang.String) 

    /**
     * Sets the value of field 'dsProdutoServicoOperacao'.
     * 
     * @param dsProdutoServicoOperacao the value of field
     * 'dsProdutoServicoOperacao'.
     */
    public void setDsProdutoServicoOperacao(java.lang.String dsProdutoServicoOperacao)
    {
        this._dsProdutoServicoOperacao = dsProdutoServicoOperacao;
    } //-- void setDsProdutoServicoOperacao(java.lang.String) 

    /**
     * Sets the value of field 'dsSituacaoSolicitaoPgto'.
     * 
     * @param dsSituacaoSolicitaoPgto the value of field
     * 'dsSituacaoSolicitaoPgto'.
     */
    public void setDsSituacaoSolicitaoPgto(java.lang.String dsSituacaoSolicitaoPgto)
    {
        this._dsSituacaoSolicitaoPgto = dsSituacaoSolicitaoPgto;
    } //-- void setDsSituacaoSolicitaoPgto(java.lang.String) 

    /**
     * Sets the value of field 'dsSolicitacao'.
     * 
     * @param dsSolicitacao the value of field 'dsSolicitacao'.
     */
    public void setDsSolicitacao(java.lang.String dsSolicitacao)
    {
        this._dsSolicitacao = dsSolicitacao;
    } //-- void setDsSolicitacao(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoLayoutArquivo'.
     * 
     * @param dsTipoLayoutArquivo the value of field
     * 'dsTipoLayoutArquivo'.
     */
    public void setDsTipoLayoutArquivo(java.lang.String dsTipoLayoutArquivo)
    {
        this._dsTipoLayoutArquivo = dsTipoLayoutArquivo;
    } //-- void setDsTipoLayoutArquivo(java.lang.String) 

    /**
     * Sets the value of field
     * 'dsTipoSolicitacaoAntecipacaoPostergacao'.
     * 
     * @param dsTipoSolicitacaoAntecipacaoPostergacao the value of
     * field 'dsTipoSolicitacaoAntecipacaoPostergacao'.
     */
    public void setDsTipoSolicitacaoAntecipacaoPostergacao(java.lang.String dsTipoSolicitacaoAntecipacaoPostergacao)
    {
        this._dsTipoSolicitacaoAntecipacaoPostergacao = dsTipoSolicitacaoAntecipacaoPostergacao;
    } //-- void setDsTipoSolicitacaoAntecipacaoPostergacao(java.lang.String) 

    /**
     * Sets the value of field 'dtNovaDataAgenda'.
     * 
     * @param dtNovaDataAgenda the value of field 'dtNovaDataAgenda'
     */
    public void setDtNovaDataAgenda(java.lang.String dtNovaDataAgenda)
    {
        this._dtNovaDataAgenda = dtNovaDataAgenda;
    } //-- void setDtNovaDataAgenda(java.lang.String) 

    /**
     * Sets the value of field 'dtPgtoFinal'.
     * 
     * @param dtPgtoFinal the value of field 'dtPgtoFinal'.
     */
    public void setDtPgtoFinal(java.lang.String dtPgtoFinal)
    {
        this._dtPgtoFinal = dtPgtoFinal;
    } //-- void setDtPgtoFinal(java.lang.String) 

    /**
     * Sets the value of field 'dtPgtoInicial'.
     * 
     * @param dtPgtoInicial the value of field 'dtPgtoInicial'.
     */
    public void setDtPgtoInicial(java.lang.String dtPgtoInicial)
    {
        this._dtPgtoInicial = dtPgtoInicial;
    } //-- void setDtPgtoInicial(java.lang.String) 

    /**
     * Sets the value of field 'hrInclusaoRegistro'.
     * 
     * @param hrInclusaoRegistro the value of field
     * 'hrInclusaoRegistro'.
     */
    public void setHrInclusaoRegistro(java.lang.String hrInclusaoRegistro)
    {
        this._hrInclusaoRegistro = hrInclusaoRegistro;
    } //-- void setHrInclusaoRegistro(java.lang.String) 

    /**
     * Sets the value of field 'hrManutencaoRegistro'.
     * 
     * @param hrManutencaoRegistro the value of field
     * 'hrManutencaoRegistro'.
     */
    public void setHrManutencaoRegistro(java.lang.String hrManutencaoRegistro)
    {
        this._hrManutencaoRegistro = hrManutencaoRegistro;
    } //-- void setHrManutencaoRegistro(java.lang.String) 

    /**
     * Sets the value of field 'hrSolicitacao'.
     * 
     * @param hrSolicitacao the value of field 'hrSolicitacao'.
     */
    public void setHrSolicitacao(java.lang.String hrSolicitacao)
    {
        this._hrSolicitacao = hrSolicitacao;
    } //-- void setHrSolicitacao(java.lang.String) 

    /**
     * Sets the value of field 'mensagem'.
     * 
     * @param mensagem the value of field 'mensagem'.
     */
    public void setMensagem(java.lang.String mensagem)
    {
        this._mensagem = mensagem;
    } //-- void setMensagem(java.lang.String) 

    /**
     * Sets the value of field 'nmOperacaoFluxoInclusao'.
     * 
     * @param nmOperacaoFluxoInclusao the value of field
     * 'nmOperacaoFluxoInclusao'.
     */
    public void setNmOperacaoFluxoInclusao(java.lang.String nmOperacaoFluxoInclusao)
    {
        this._nmOperacaoFluxoInclusao = nmOperacaoFluxoInclusao;
    } //-- void setNmOperacaoFluxoInclusao(java.lang.String) 

    /**
     * Sets the value of field 'nmOperacaoFluxoManutencao'.
     * 
     * @param nmOperacaoFluxoManutencao the value of field
     * 'nmOperacaoFluxoManutencao'.
     */
    public void setNmOperacaoFluxoManutencao(java.lang.String nmOperacaoFluxoManutencao)
    {
        this._nmOperacaoFluxoManutencao = nmOperacaoFluxoManutencao;
    } //-- void setNmOperacaoFluxoManutencao(java.lang.String) 

    /**
     * Sets the value of field 'nrLoteInterno'.
     * 
     * @param nrLoteInterno the value of field 'nrLoteInterno'.
     */
    public void setNrLoteInterno(long nrLoteInterno)
    {
        this._nrLoteInterno = nrLoteInterno;
        this._has_nrLoteInterno = true;
    } //-- void setNrLoteInterno(long) 

    /**
     * Sets the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @param nrSequenciaContratoNegocio the value of field
     * 'nrSequenciaContratoNegocio'.
     */
    public void setNrSequenciaContratoNegocio(long nrSequenciaContratoNegocio)
    {
        this._nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
        this._has_nrSequenciaContratoNegocio = true;
    } //-- void setNrSequenciaContratoNegocio(long) 

    /**
     * Sets the value of field 'nrSolicitacaoPagamentoIntegrado'.
     * 
     * @param nrSolicitacaoPagamentoIntegrado the value of field
     * 'nrSolicitacaoPagamentoIntegrado'.
     */
    public void setNrSolicitacaoPagamentoIntegrado(int nrSolicitacaoPagamentoIntegrado)
    {
        this._nrSolicitacaoPagamentoIntegrado = nrSolicitacaoPagamentoIntegrado;
        this._has_nrSolicitacaoPagamentoIntegrado = true;
    } //-- void setNrSolicitacaoPagamentoIntegrado(int) 

    /**
     * Sets the value of field 'qtTotalPgtoEfetivadoSolicitacao'.
     * 
     * @param qtTotalPgtoEfetivadoSolicitacao the value of field
     * 'qtTotalPgtoEfetivadoSolicitacao'.
     */
    public void setQtTotalPgtoEfetivadoSolicitacao(long qtTotalPgtoEfetivadoSolicitacao)
    {
        this._qtTotalPgtoEfetivadoSolicitacao = qtTotalPgtoEfetivadoSolicitacao;
        this._has_qtTotalPgtoEfetivadoSolicitacao = true;
    } //-- void setQtTotalPgtoEfetivadoSolicitacao(long) 

    /**
     * Sets the value of field 'qtdeTotalPagtoPrevistoSoltc'.
     * 
     * @param qtdeTotalPagtoPrevistoSoltc the value of field
     * 'qtdeTotalPagtoPrevistoSoltc'.
     */
    public void setQtdeTotalPagtoPrevistoSoltc(long qtdeTotalPagtoPrevistoSoltc)
    {
        this._qtdeTotalPagtoPrevistoSoltc = qtdeTotalPagtoPrevistoSoltc;
        this._has_qtdeTotalPagtoPrevistoSoltc = true;
    } //-- void setQtdeTotalPagtoPrevistoSoltc(long) 

    /**
     * Sets the value of field 'vlTotalPgtoEfetuadoSolicitacao'.
     * 
     * @param vlTotalPgtoEfetuadoSolicitacao the value of field
     * 'vlTotalPgtoEfetuadoSolicitacao'.
     */
    public void setVlTotalPgtoEfetuadoSolicitacao(java.math.BigDecimal vlTotalPgtoEfetuadoSolicitacao)
    {
        this._vlTotalPgtoEfetuadoSolicitacao = vlTotalPgtoEfetuadoSolicitacao;
    } //-- void setVlTotalPgtoEfetuadoSolicitacao(java.math.BigDecimal) 

    /**
     * Sets the value of field 'vlrTotPagtoPrevistoSolicitacao'.
     * 
     * @param vlrTotPagtoPrevistoSolicitacao the value of field
     * 'vlrTotPagtoPrevistoSolicitacao'.
     */
    public void setVlrTotPagtoPrevistoSolicitacao(java.math.BigDecimal vlrTotPagtoPrevistoSolicitacao)
    {
        this._vlrTotPagtoPrevistoSolicitacao = vlrTotPagtoPrevistoSolicitacao;
    } //-- void setVlrTotPagtoPrevistoSolicitacao(java.math.BigDecimal) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return DetalharSolicAntPosDtPgtoResponse
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.detalharsolicantposdtpgto.response.DetalharSolicAntPosDtPgtoResponse unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.detalharsolicantposdtpgto.response.DetalharSolicAntPosDtPgtoResponse) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.detalharsolicantposdtpgto.response.DetalharSolicAntPosDtPgtoResponse.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharsolicantposdtpgto.response.DetalharSolicAntPosDtPgtoResponse unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
