package br.com.bradesco.web.pgit.service.business.assretlayarqproser.bean;


/**
 * Arquivo criado em 09/02/15.
 */
public class ExcluirAssVersaoLayoutLoteServicoSaidaDTO {

    private String codMensagem;

    private String mensagem;

    public void setCodMensagem(String codMensagem) {
        this.codMensagem = codMensagem;
    }

    public String getCodMensagem() {
        return this.codMensagem;
    }

    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }

    public String getMensagem() {
        return this.mensagem;
    }
}