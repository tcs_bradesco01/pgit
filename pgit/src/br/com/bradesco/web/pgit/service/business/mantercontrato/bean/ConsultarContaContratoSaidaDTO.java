/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantercontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantercontrato.bean;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Nome: ConsultarContaContratoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarContaContratoSaidaDTO {
	
	/** Atributo listaFinalidade. */
	private List<FinalidadeSaidaDTO> listaFinalidade = new ArrayList<FinalidadeSaidaDTO>();
	
	/** Atributo codMensagem. */
	private String codMensagem;
	
	/** Atributo mensagem. */
	private String mensagem;
	
	/** Atributo cdSituacaoVinculacaoConta. */
	private int cdSituacaoVinculacaoConta;
	
	/** Atributo dsSituacaoVinculacaoConta. */
	private String dsSituacaoVinculacaoConta;
	
	/** Atributo cdMotivoSituacaoConta. */
	private int cdMotivoSituacaoConta;
	
	/** Atributo dsMotivoSituacaoConta. */
	private String dsMotivoSituacaoConta;
	
	/** Atributo cdMunicipio. */
	private Long cdMunicipio;
	
	/** Atributo dsMunicipio. */
	private String dsMunicipio;
	
	/** Atributo cdMunicipioFeriadoLocal. */
	private int cdMunicipioFeriadoLocal;
	
	/** Atributo cdUsuarioInclusao. */
	private String cdUsuarioInclusao;
	
	/** Atributo cdUsuarioInclusaoExterno. */
	private String cdUsuarioInclusaoExterno;
	
	/** Atributo hrInclusaoRegistro. */
	private String hrInclusaoRegistro;
	
	/** Atributo cdOperacaoCanalInclusao. */
	private String cdOperacaoCanalInclusao;
	
	/** Atributo cdTipoCanalInclusao. */
	private int cdTipoCanalInclusao;
	
	/** Atributo cdUsuarioManutencao. */
	private String cdUsuarioManutencao;
	
	/** Atributo cdUsuarioManutencaoExterno. */
	private String cdUsuarioManutencaoExterno;
	
	/** Atributo hrManutencaoRegistro. */
	private String hrManutencaoRegistro;
	
	/** Atributo cdOperacaoCanalManutencao. */
	private String cdOperacaoCanalManutencao;
	
	/** Atributo cdTipoCanalManutencao. */
	private int cdTipoCanalManutencao;
	
	/** Atributo cdUf. */
	private int cdUf;
	
	/** Atributo dsUf. */
	private String dsUf;
	
	/** Atributo cdSiglaUf. */
	private String cdSiglaUf;
	
	/** Atributo dsTipoCanalInclusao. */
	private String dsTipoCanalInclusao;
	
	/** Atributo dsTipoCanalManutencao. */
	private String dsTipoCanalManutencao;
	
	/** Atributo vlAdicionalContratoConta. */
	private BigDecimal vlAdicionalContratoConta;
    
    /** Atributo dtInicioValorAdicional. */
    private String dtInicioValorAdicional;
    
    /** Atributo dtFimValorAdicional. */
    private String dtFimValorAdicional;
    
    /** Atributo vlAdicContrCtaTed. */
    private BigDecimal vlAdicContrCtaTed;
    
    /** Atributo dtIniVlrAdicionalTed. */
    private String dtIniVlrAdicionalTed;
    
    /** Atributo dtFimVlrAdicionalTed. */
    private String dtFimVlrAdicionalTed;
    
    private String descApelido;
	
	/**
	 * Get: cdMotivoSituacaoConta.
	 *
	 * @return cdMotivoSituacaoConta
	 */
	public int getCdMotivoSituacaoConta() {
		return cdMotivoSituacaoConta;
	}
	
	/**
	 * Set: cdMotivoSituacaoConta.
	 *
	 * @param cdMotivoSituacaoConta the cd motivo situacao conta
	 */
	public void setCdMotivoSituacaoConta(int cdMotivoSituacaoConta) {
		this.cdMotivoSituacaoConta = cdMotivoSituacaoConta;
	}

	/**
	 * Get: cdMunicipio.
	 *
	 * @return cdMunicipio
	 */
	public Long getCdMunicipio() {
		return cdMunicipio;
	}
	
	/**
	 * Set: cdMunicipio.
	 *
	 * @param cdMunicipio the cd municipio
	 */
	public void setCdMunicipio(Long cdMunicipio) {
		this.cdMunicipio = cdMunicipio;
	}
	
	/**
	 * Get: cdMunicipioFeriadoLocal.
	 *
	 * @return cdMunicipioFeriadoLocal
	 */
	public int getCdMunicipioFeriadoLocal() {
		return cdMunicipioFeriadoLocal;
	}
	
	/**
	 * Set: cdMunicipioFeriadoLocal.
	 *
	 * @param cdMunicipioFeriadoLocal the cd municipio feriado local
	 */
	public void setCdMunicipioFeriadoLocal(int cdMunicipioFeriadoLocal) {
		this.cdMunicipioFeriadoLocal = cdMunicipioFeriadoLocal;
	}
	
	/**
	 * Get: cdOperacaoCanalInclusao.
	 *
	 * @return cdOperacaoCanalInclusao
	 */
	public String getCdOperacaoCanalInclusao() {
		return cdOperacaoCanalInclusao;
	}
	
	/**
	 * Set: cdOperacaoCanalInclusao.
	 *
	 * @param cdOperacaoCanalInclusao the cd operacao canal inclusao
	 */
	public void setCdOperacaoCanalInclusao(String cdOperacaoCanalInclusao) {
		this.cdOperacaoCanalInclusao = cdOperacaoCanalInclusao;
	}
	
	/**
	 * Get: cdOperacaoCanalManutencao.
	 *
	 * @return cdOperacaoCanalManutencao
	 */
	public String getCdOperacaoCanalManutencao() {
		return cdOperacaoCanalManutencao;
	}
	
	/**
	 * Set: cdOperacaoCanalManutencao.
	 *
	 * @param cdOperacaoCanalManutencao the cd operacao canal manutencao
	 */
	public void setCdOperacaoCanalManutencao(String cdOperacaoCanalManutencao) {
		this.cdOperacaoCanalManutencao = cdOperacaoCanalManutencao;
	}
	
	/**
	 * Get: cdSituacaoVinculacaoConta.
	 *
	 * @return cdSituacaoVinculacaoConta
	 */
	public int getCdSituacaoVinculacaoConta() {
		return cdSituacaoVinculacaoConta;
	}
	
	/**
	 * Set: cdSituacaoVinculacaoConta.
	 *
	 * @param cdSituacaoVinculacaoConta the cd situacao vinculacao conta
	 */
	public void setCdSituacaoVinculacaoConta(int cdSituacaoVinculacaoConta) {
		this.cdSituacaoVinculacaoConta = cdSituacaoVinculacaoConta;
	}
	
	/**
	 * Get: cdTipoCanalInclusao.
	 *
	 * @return cdTipoCanalInclusao
	 */
	public int getCdTipoCanalInclusao() {
		return cdTipoCanalInclusao;
	}
	
	/**
	 * Set: cdTipoCanalInclusao.
	 *
	 * @param cdTipoCanalInclusao the cd tipo canal inclusao
	 */
	public void setCdTipoCanalInclusao(int cdTipoCanalInclusao) {
		this.cdTipoCanalInclusao = cdTipoCanalInclusao;
	}
	
	/**
	 * Get: cdTipoCanalManutencao.
	 *
	 * @return cdTipoCanalManutencao
	 */
	public int getCdTipoCanalManutencao() {
		return cdTipoCanalManutencao;
	}
	
	/**
	 * Set: cdTipoCanalManutencao.
	 *
	 * @param cdTipoCanalManutencao the cd tipo canal manutencao
	 */
	public void setCdTipoCanalManutencao(int cdTipoCanalManutencao) {
		this.cdTipoCanalManutencao = cdTipoCanalManutencao;
	}
	
	/**
	 * Get: cdUsuarioInclusao.
	 *
	 * @return cdUsuarioInclusao
	 */
	public String getCdUsuarioInclusao() {
		return cdUsuarioInclusao;
	}
	
	/**
	 * Set: cdUsuarioInclusao.
	 *
	 * @param cdUsuarioInclusao the cd usuario inclusao
	 */
	public void setCdUsuarioInclusao(String cdUsuarioInclusao) {
		this.cdUsuarioInclusao = cdUsuarioInclusao;
	}
	
	/**
	 * Get: cdUsuarioInclusaoExterno.
	 *
	 * @return cdUsuarioInclusaoExterno
	 */
	public String getCdUsuarioInclusaoExterno() {
		return cdUsuarioInclusaoExterno;
	}
	
	/**
	 * Set: cdUsuarioInclusaoExterno.
	 *
	 * @param cdUsuarioInclusaoExterno the cd usuario inclusao externo
	 */
	public void setCdUsuarioInclusaoExterno(String cdUsuarioInclusaoExterno) {
		this.cdUsuarioInclusaoExterno = cdUsuarioInclusaoExterno;
	}
	
	/**
	 * Get: cdUsuarioManutencao.
	 *
	 * @return cdUsuarioManutencao
	 */
	public String getCdUsuarioManutencao() {
		return cdUsuarioManutencao;
	}
	
	/**
	 * Set: cdUsuarioManutencao.
	 *
	 * @param cdUsuarioManutencao the cd usuario manutencao
	 */
	public void setCdUsuarioManutencao(String cdUsuarioManutencao) {
		this.cdUsuarioManutencao = cdUsuarioManutencao;
	}
	
	/**
	 * Get: cdUsuarioManutencaoExterno.
	 *
	 * @return cdUsuarioManutencaoExterno
	 */
	public String getCdUsuarioManutencaoExterno() {
		return cdUsuarioManutencaoExterno;
	}
	
	/**
	 * Set: cdUsuarioManutencaoExterno.
	 *
	 * @param cdUsuarioManutencaoExterno the cd usuario manutencao externo
	 */
	public void setCdUsuarioManutencaoExterno(String cdUsuarioManutencaoExterno) {
		this.cdUsuarioManutencaoExterno = cdUsuarioManutencaoExterno;
	}
	
	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}
	
	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}

	/**
	 * Get: dsMotivoSituacaoConta.
	 *
	 * @return dsMotivoSituacaoConta
	 */
	public String getDsMotivoSituacaoConta() {
		return dsMotivoSituacaoConta;
	}
	
	/**
	 * Set: dsMotivoSituacaoConta.
	 *
	 * @param dsMotivoSituacaoConta the ds motivo situacao conta
	 */
	public void setDsMotivoSituacaoConta(String dsMotivoSituacaoConta) {
		this.dsMotivoSituacaoConta = dsMotivoSituacaoConta;
	}
	
	/**
	 * Get: dsMunicipio.
	 *
	 * @return dsMunicipio
	 */
	public String getDsMunicipio() {
		return dsMunicipio;
	}
	
	/**
	 * Set: dsMunicipio.
	 *
	 * @param dsMunicipio the ds municipio
	 */
	public void setDsMunicipio(String dsMunicipio) {
		this.dsMunicipio = dsMunicipio;
	}
	
	/**
	 * Get: dsSituacaoVinculacaoConta.
	 *
	 * @return dsSituacaoVinculacaoConta
	 */
	public String getDsSituacaoVinculacaoConta() {
		return dsSituacaoVinculacaoConta;
	}
	
	/**
	 * Set: dsSituacaoVinculacaoConta.
	 *
	 * @param dsSituacaoVinculacaoConta the ds situacao vinculacao conta
	 */
	public void setDsSituacaoVinculacaoConta(String dsSituacaoVinculacaoConta) {
		this.dsSituacaoVinculacaoConta = dsSituacaoVinculacaoConta;
	}
	
	/**
	 * Get: hrInclusaoRegistro.
	 *
	 * @return hrInclusaoRegistro
	 */
	public String getHrInclusaoRegistro() {
		return hrInclusaoRegistro;
	}
	
	/**
	 * Set: hrInclusaoRegistro.
	 *
	 * @param hrInclusaoRegistro the hr inclusao registro
	 */
	public void setHrInclusaoRegistro(String hrInclusaoRegistro) {
		this.hrInclusaoRegistro = hrInclusaoRegistro;
	}
	
	/**
	 * Get: hrManutencaoRegistro.
	 *
	 * @return hrManutencaoRegistro
	 */
	public String getHrManutencaoRegistro() {
		return hrManutencaoRegistro;
	}
	
	/**
	 * Set: hrManutencaoRegistro.
	 *
	 * @param hrManutencaoRegistro the hr manutencao registro
	 */
	public void setHrManutencaoRegistro(String hrManutencaoRegistro) {
		this.hrManutencaoRegistro = hrManutencaoRegistro;
	}
	
	/**
	 * Get: listaFinalidade.
	 *
	 * @return listaFinalidade
	 */
	public List<FinalidadeSaidaDTO> getListaFinalidade() {
		return listaFinalidade;
	}
	
	/**
	 * Set: listaFinalidade.
	 *
	 * @param listaFinalidade the lista finalidade
	 */
	public void setListaFinalidade(List<FinalidadeSaidaDTO> listaFinalidade) {
		this.listaFinalidade = listaFinalidade;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	/**
	 * Get: cdSiglaUf.
	 *
	 * @return cdSiglaUf
	 */
	public String getCdSiglaUf() {
		return cdSiglaUf;
	}
	
	/**
	 * Set: cdSiglaUf.
	 *
	 * @param cdSiglaUf the cd sigla uf
	 */
	public void setCdSiglaUf(String cdSiglaUf) {
		this.cdSiglaUf = cdSiglaUf;
	}
	
	/**
	 * Get: cdUf.
	 *
	 * @return cdUf
	 */
	public int getCdUf() {
		return cdUf;
	}
	
	/**
	 * Set: cdUf.
	 *
	 * @param cdUf the cd uf
	 */
	public void setCdUf(int cdUf) {
		this.cdUf = cdUf;
	}
	
	/**
	 * Get: dsTipoCanalInclusao.
	 *
	 * @return dsTipoCanalInclusao
	 */
	public String getDsTipoCanalInclusao() {
		return dsTipoCanalInclusao;
	}
	
	/**
	 * Set: dsTipoCanalInclusao.
	 *
	 * @param dsTipoCanalInclusao the ds tipo canal inclusao
	 */
	public void setDsTipoCanalInclusao(String dsTipoCanalInclusao) {
		this.dsTipoCanalInclusao = dsTipoCanalInclusao;
	}
	
	/**
	 * Get: dsTipoCanalManutencao.
	 *
	 * @return dsTipoCanalManutencao
	 */
	public String getDsTipoCanalManutencao() {
		return dsTipoCanalManutencao;
	}
	
	/**
	 * Set: dsTipoCanalManutencao.
	 *
	 * @param dsTipoCanalManutencao the ds tipo canal manutencao
	 */
	public void setDsTipoCanalManutencao(String dsTipoCanalManutencao) {
		this.dsTipoCanalManutencao = dsTipoCanalManutencao;
	}
	
	/**
	 * Get: dsUf.
	 *
	 * @return dsUf
	 */
	public String getDsUf() {
		return dsUf;
	}
	
	/**
	 * Set: dsUf.
	 *
	 * @param dsUf the ds uf
	 */
	public void setDsUf(String dsUf) {
		this.dsUf = dsUf;
	}
	
	/**
	 * Get: vlAdicionalContratoConta.
	 *
	 * @return vlAdicionalContratoConta
	 */
	public BigDecimal getVlAdicionalContratoConta() {
		return vlAdicionalContratoConta;
	}
	
	/**
	 * Set: vlAdicionalContratoConta.
	 *
	 * @param vlAdicionalContratoConta the vl adicional contrato conta
	 */
	public void setVlAdicionalContratoConta(BigDecimal vlAdicionalContratoConta) {
		this.vlAdicionalContratoConta = vlAdicionalContratoConta;
	}
	
	/**
	 * Get: dtInicioValorAdicional.
	 *
	 * @return dtInicioValorAdicional
	 */
	public String getDtInicioValorAdicional() {
		return dtInicioValorAdicional;
	}
	
	/**
	 * Set: dtInicioValorAdicional.
	 *
	 * @param dtInicioValorAdicional the dt inicio valor adicional
	 */
	public void setDtInicioValorAdicional(String dtInicioValorAdicional) {
		this.dtInicioValorAdicional = dtInicioValorAdicional;
	}
	
	/**
	 * Get: dtFimValorAdicional.
	 *
	 * @return dtFimValorAdicional
	 */
	public String getDtFimValorAdicional() {
		return dtFimValorAdicional;
	}
	
	/**
	 * Set: dtFimValorAdicional.
	 *
	 * @param dtFimValorAdicional the dt fim valor adicional
	 */
	public void setDtFimValorAdicional(String dtFimValorAdicional) {
		this.dtFimValorAdicional = dtFimValorAdicional;
	}
	
	/**
	 * Get: vlAdicContrCtaTed.
	 *
	 * @return vlAdicContrCtaTed
	 */
	public BigDecimal getVlAdicContrCtaTed() {
		return vlAdicContrCtaTed;
	}
	
	/**
	 * Set: vlAdicContrCtaTed.
	 *
	 * @param vlAdicContrCtaTed the vl adic contr cta ted
	 */
	public void setVlAdicContrCtaTed(BigDecimal vlAdicContrCtaTed) {
		this.vlAdicContrCtaTed = vlAdicContrCtaTed;
	}
	
	/**
	 * Get: dtIniVlrAdicionalTed.
	 *
	 * @return dtIniVlrAdicionalTed
	 */
	public String getDtIniVlrAdicionalTed() {
		return dtIniVlrAdicionalTed;
	}
	
	/**
	 * Set: dtIniVlrAdicionalTed.
	 *
	 * @param dtIniVlrAdicionalTed the dt ini vlr adicional ted
	 */
	public void setDtIniVlrAdicionalTed(String dtIniVlrAdicionalTed) {
		this.dtIniVlrAdicionalTed = dtIniVlrAdicionalTed;
	}
	
	/**
	 * Get: dtFimVlrAdicionalTed.
	 *
	 * @return dtFimVlrAdicionalTed
	 */
	public String getDtFimVlrAdicionalTed() {
		return dtFimVlrAdicionalTed;
	}
	
	/**
	 * Set: dtFimVlrAdicionalTed.
	 *
	 * @param dtFimVlrAdicionalTed the dt fim vlr adicional ted
	 */
	public void setDtFimVlrAdicionalTed(String dtFimVlrAdicionalTed) {
		this.dtFimVlrAdicionalTed = dtFimVlrAdicionalTed;
	}

	/**
	 * Get: descApelido
	 * @return the descApelido
	 */
	public String getDescApelido() {
		return descApelido;
	}

	/**
	 * Set: descApelido
	 * @param descApelido the descApelido to set
	 */
	public void setDescApelido(String descApelido) {
		this.descApelido = descApelido;
	}
	
}