/*
 * Nome: br.com.bradesco.web.pgit.service.business.manterformalizacaoalteracaocontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.manterformalizacaoalteracaocontrato.bean;

/**
 * Nome: ExcluirManterFormAltContratoEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ExcluirManterFormAltContratoEntradaDTO {

	/** Atributo codPessoaJuridica. */
	private Long codPessoaJuridica;

	/** Atributo codTipoContratoNegocio. */
	private Integer codTipoContratoNegocio;

	/** Atributo numSeqContratoNegocio. */
	private Long numSeqContratoNegocio;

	/** Atributo numAditivoContratoNegocio. */
	private Long numAditivoContratoNegocio;

	/**
	 * Get: codPessoaJuridica.
	 *
	 * @return codPessoaJuridica
	 */
	public Long getCodPessoaJuridica() {
		return codPessoaJuridica;
	}

	/**
	 * Set: codPessoaJuridica.
	 *
	 * @param codPessoaJuridica the cod pessoa juridica
	 */
	public void setCodPessoaJuridica(Long codPessoaJuridica) {
		this.codPessoaJuridica = codPessoaJuridica;
	}

	/**
	 * Get: codTipoContratoNegocio.
	 *
	 * @return codTipoContratoNegocio
	 */
	public Integer getCodTipoContratoNegocio() {
		return codTipoContratoNegocio;
	}

	/**
	 * Set: codTipoContratoNegocio.
	 *
	 * @param codTipoContratoNegocio the cod tipo contrato negocio
	 */
	public void setCodTipoContratoNegocio(Integer codTipoContratoNegocio) {
		this.codTipoContratoNegocio = codTipoContratoNegocio;
	}

	/**
	 * Get: numSeqContratoNegocio.
	 *
	 * @return numSeqContratoNegocio
	 */
	public Long getNumSeqContratoNegocio() {
		return numSeqContratoNegocio;
	}

	/**
	 * Set: numSeqContratoNegocio.
	 *
	 * @param numSeqContratoNegocio the num seq contrato negocio
	 */
	public void setNumSeqContratoNegocio(Long numSeqContratoNegocio) {
		this.numSeqContratoNegocio = numSeqContratoNegocio;
	}

	/**
	 * Get: numAditivoContratoNegocio.
	 *
	 * @return numAditivoContratoNegocio
	 */
	public Long getNumAditivoContratoNegocio() {
		return numAditivoContratoNegocio;
	}

	/**
	 * Set: numAditivoContratoNegocio.
	 *
	 * @param numAditivoContratoNegocio the num aditivo contrato negocio
	 */
	public void setNumAditivoContratoNegocio(Long numAditivoContratoNegocio) {
		this.numAditivoContratoNegocio = numAditivoContratoNegocio;
	}
}