/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listaraplicativoformatacaoarquivo.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdAplicacaoTransmicaoPagamento
     */
    private int _cdAplicacaoTransmicaoPagamento = 0;

    /**
     * keeps track of state for field:
     * _cdAplicacaoTransmicaoPagamento
     */
    private boolean _has_cdAplicacaoTransmicaoPagamento;

    /**
     * Field _dsAplicacaoTransmicaoPagamento
     */
    private java.lang.String _dsAplicacaoTransmicaoPagamento;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listaraplicativoformatacaoarquivo.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdAplicacaoTransmicaoPagamento
     * 
     */
    public void deleteCdAplicacaoTransmicaoPagamento()
    {
        this._has_cdAplicacaoTransmicaoPagamento= false;
    } //-- void deleteCdAplicacaoTransmicaoPagamento() 

    /**
     * Returns the value of field 'cdAplicacaoTransmicaoPagamento'.
     * 
     * @return int
     * @return the value of field 'cdAplicacaoTransmicaoPagamento'.
     */
    public int getCdAplicacaoTransmicaoPagamento()
    {
        return this._cdAplicacaoTransmicaoPagamento;
    } //-- int getCdAplicacaoTransmicaoPagamento() 

    /**
     * Returns the value of field 'dsAplicacaoTransmicaoPagamento'.
     * 
     * @return String
     * @return the value of field 'dsAplicacaoTransmicaoPagamento'.
     */
    public java.lang.String getDsAplicacaoTransmicaoPagamento()
    {
        return this._dsAplicacaoTransmicaoPagamento;
    } //-- java.lang.String getDsAplicacaoTransmicaoPagamento() 

    /**
     * Method hasCdAplicacaoTransmicaoPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAplicacaoTransmicaoPagamento()
    {
        return this._has_cdAplicacaoTransmicaoPagamento;
    } //-- boolean hasCdAplicacaoTransmicaoPagamento() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdAplicacaoTransmicaoPagamento'.
     * 
     * @param cdAplicacaoTransmicaoPagamento the value of field
     * 'cdAplicacaoTransmicaoPagamento'.
     */
    public void setCdAplicacaoTransmicaoPagamento(int cdAplicacaoTransmicaoPagamento)
    {
        this._cdAplicacaoTransmicaoPagamento = cdAplicacaoTransmicaoPagamento;
        this._has_cdAplicacaoTransmicaoPagamento = true;
    } //-- void setCdAplicacaoTransmicaoPagamento(int) 

    /**
     * Sets the value of field 'dsAplicacaoTransmicaoPagamento'.
     * 
     * @param dsAplicacaoTransmicaoPagamento the value of field
     * 'dsAplicacaoTransmicaoPagamento'.
     */
    public void setDsAplicacaoTransmicaoPagamento(java.lang.String dsAplicacaoTransmicaoPagamento)
    {
        this._dsAplicacaoTransmicaoPagamento = dsAplicacaoTransmicaoPagamento;
    } //-- void setDsAplicacaoTransmicaoPagamento(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listaraplicativoformatacaoarquivo.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listaraplicativoformatacaoarquivo.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listaraplicativoformatacaoarquivo.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listaraplicativoformatacaoarquivo.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
