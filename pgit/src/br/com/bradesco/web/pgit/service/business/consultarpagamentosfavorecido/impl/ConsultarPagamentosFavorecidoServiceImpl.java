/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/implementation.ftl,v $
 * $Id: implementation.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: implementation.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */
 
package br.com.bradesco.web.pgit.service.business.consultarpagamentosfavorecido.impl;

import static br.com.bradesco.web.pgit.utils.PgitUtil.concatenarCampos;
import static br.com.bradesco.web.pgit.utils.PgitUtil.formatBancoAgenciaConta;
import static br.com.bradesco.web.pgit.utils.PgitUtil.verificaIntegerNulo;
import static br.com.bradesco.web.pgit.utils.PgitUtil.verificaLongNulo;
import static br.com.bradesco.web.pgit.utils.PgitUtil.verificaStringNula;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import br.com.bradesco.web.pgit.service.business.consultarpagamentosfavorecido.IConsultarPagamentosFavorecidoService;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosfavorecido.IConsultarPagamentosFavorecidoServiceConstants;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosfavorecido.bean.ConsultarPagamentoFavorecidoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosfavorecido.bean.ConsultarPagamentoFavorecidoSaidaDTO;
import br.com.bradesco.web.pgit.service.data.pdc.FactoryAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarpagamentofavorecido.request.ConsultarPagamentoFavorecidoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarpagamentofavorecido.response.ConsultarPagamentoFavorecidoResponse;
import br.com.bradesco.web.pgit.utils.CpfCnpjUtils;
import br.com.bradesco.web.pgit.utils.NumberUtils;
import br.com.bradesco.web.pgit.utils.PgitUtil;


/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Implementa��o do adaptador: ConsultarPagamentosFavorecido
 * </p>
 * 
 * @comment C�DIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public class ConsultarPagamentosFavorecidoServiceImpl implements IConsultarPagamentosFavorecidoService {

	/** Atributo factoryAdapter. */
	private FactoryAdapter factoryAdapter;
	
	/**
	 * Get: factoryAdapter.
	 *
	 * @return factoryAdapter
	 */
	public FactoryAdapter getFactoryAdapter() {
		return factoryAdapter;
	}

	/**
	 * Set: factoryAdapter.
	 *
	 * @param factoryAdapter the factory adapter
	 */
	public void setFactoryAdapter(FactoryAdapter factoryAdapter) {
		this.factoryAdapter = factoryAdapter;
	}

	/**
     * Construtor.
     */
    public ConsultarPagamentosFavorecidoServiceImpl() {
        // TODO: Implementa��o
    }
    
    /**
     * M�todo de exemplo.
     *
     * @see br.com.bradesco.web.pgit.service.business.consultarpagamentosfavorecido.IConsultarPagamentosFavorecido#sampleConsultarPagamentosFavorecido()
     */
    public void sampleConsultarPagamentosFavorecido() {
        // TODO: Implementa�ao
    }
    
    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.consultarpagamentosfavorecido.IConsultarPagamentosFavorecidoService#consultarPagamentoFavorecido(br.com.bradesco.web.pgit.service.business.consultarpagamentosfavorecido.bean.ConsultarPagamentoFavorecidoEntradaDTO)
     */
    public List<ConsultarPagamentoFavorecidoSaidaDTO> consultarPagamentoFavorecido (ConsultarPagamentoFavorecidoEntradaDTO entradaDTO){
    	List<ConsultarPagamentoFavorecidoSaidaDTO> listaSaida = new ArrayList<ConsultarPagamentoFavorecidoSaidaDTO>();
    	ConsultarPagamentoFavorecidoRequest request = new ConsultarPagamentoFavorecidoRequest();
    	ConsultarPagamentoFavorecidoResponse response = new ConsultarPagamentoFavorecidoResponse();
    	
        request.setAgePagoNaoPago(PgitUtil.verificaIntegerNulo(entradaDTO.getAgePagoNaoPago()));
        request.setNumeroOcorrencias(IConsultarPagamentosFavorecidoServiceConstants.NUMERO_OCORRENCIAS_CONSULTAR);       
        request.setDtCreditoPagamentoInicial(verificaStringNula(entradaDTO.getDtCreditoPagamentoInicial()));
        request.setDtCreditoPagamentoFinal(verificaStringNula(entradaDTO.getDtCreditoPagamentoFinal()));
        request.setCdProdutoServicoOperacao(verificaIntegerNulo(entradaDTO.getCdProdutoServicoOperacao()));
        request.setCdProdutoServicoRelacionado(verificaIntegerNulo(entradaDTO.getCdProdutoServicoRelacionado()));
        request.setCdFavorecidoClientePagador(verificaLongNulo(entradaDTO.getCdFavorecidoClientePagador()));
        request.setCdInscricaoFavorecidoCliente(verificaLongNulo(entradaDTO.getCdInscricaoFavorecidoCliente()));
        request.setCdIdentificacaoInscricaoFavorecido(verificaIntegerNulo(entradaDTO.getCdIdentificacaoInscricaoFavorecido()));
        request.setCdBanco(verificaIntegerNulo(entradaDTO.getCdBanco()));
        request.setCdAgencia(verificaIntegerNulo(entradaDTO.getCdAgencia()));
        request.setCdConta(verificaLongNulo(entradaDTO.getCdConta()));
        request.setCdDigitoConta(PgitUtil.DIGITO_CONTA);
        request.setNrUnidadeOrganizacional(verificaIntegerNulo(entradaDTO.getNrUnidadeOrganizacional()));
        request.setCdBeneficiario(verificaLongNulo(entradaDTO.getCdBeneficiario()));
        request.setCdEspecieBeneficioINSS(verificaIntegerNulo(entradaDTO.getCdEspecieBeneficioINSS()));
        request.setCdClassificacao("1");
        request.setCdEmpresaContrato(verificaLongNulo(entradaDTO.getCdEmpresaContrato()));
        request.setCdTipoConta(verificaIntegerNulo(entradaDTO.getCdTipoConta()));
        request.setNrContrato(verificaLongNulo(entradaDTO.getNrContrato()));        
        request.setCdBancoSalarial(verificaIntegerNulo(entradaDTO.getCdBancoSalarial()));
        request.setCdAgencaSalarial(verificaIntegerNulo(entradaDTO.getCdAgencaSalarial()));
        request.setCdContaSalarial(verificaLongNulo(entradaDTO.getCdContaSalarial()));
		if("".equals(PgitUtil.verificaStringNula(entradaDTO.getCdIspbPagtoDestino()))){
			request.setCdIspbPagtoDestino("");
		}else{
			request.setCdIspbPagtoDestino(PgitUtil.preencherZerosAEsquerda(entradaDTO.getCdIspbPagtoDestino(), 8));
		}
		request.setContaPagtoDestino(PgitUtil.preencherZerosAEsquerda(entradaDTO.getContaPagtoDestino(), 20));

        
        
        response = getFactoryAdapter().getConsultarPagamentoFavorecidoPDCAdapter().invokeProcess(request);
        
        ConsultarPagamentoFavorecidoSaidaDTO saidaDTO;
 	    SimpleDateFormat formato1 = new SimpleDateFormat("dd.MM.yyyy");
	    SimpleDateFormat formato2 = new SimpleDateFormat("dd/MM/yyyy");
        
        for(int i=0;i<response.getOcorrenciasCount(); i++){
        	saidaDTO = new ConsultarPagamentoFavorecidoSaidaDTO();
            saidaDTO.setCodMensagem(response.getCodMensagem());
            saidaDTO.setMensagem(response.getMensagem());
            saidaDTO.setCorpoCfpCnpj(response.getOcorrencias(i).getCorpoCfpCnpj());
            saidaDTO.setFilialCfpCnpj(response.getOcorrencias(i).getFilialCfpCnpj());
            saidaDTO.setControleCfpCnpj(response.getOcorrencias(i).getControleCfpCnpj());
            saidaDTO.setCdPessoaJuridicaContrato(response.getOcorrencias(i).getCdPessoaJuridicaContrato());
            saidaDTO.setNmRazaoSocial(response.getOcorrencias(i).getNmRazaoSocial());
            saidaDTO.setCdEmpresa(response.getOcorrencias(i).getCdEmpresa());
            saidaDTO.setDsEmpresa(response.getOcorrencias(i).getDsEmpresa());
            saidaDTO.setCdTipoContratoNegocio(response.getOcorrencias(i).getCdTipoContratoNegocio());
            saidaDTO.setNrContrato(response.getOcorrencias(i).getNrContrato());
            saidaDTO.setCdProdutoServicoOperacao(response.getOcorrencias(i).getCdProdutoServicoOperacao());
            saidaDTO.setCdResumoProdutoServico(response.getOcorrencias(i).getCdResumoProdutoServico());
            saidaDTO.setCdProdutoServicoRelacionado(response.getOcorrencias(i).getCdProdutoServicoRelacionado());
            saidaDTO.setDsOperacaoProdutoServico(response.getOcorrencias(i).getDsOperacaoProdutoServico());
            saidaDTO.setCdControlePagamento(response.getOcorrencias(i).getCdControlePagamento());
            saidaDTO.setDtCraditoPagamento(response.getOcorrencias(i).getDtCraditoPagamento());
            saidaDTO.setVlrEfetivacaoPagamentoCliente(response.getOcorrencias(i).getVlrEfetivacaoPagamentoCliente());
            saidaDTO.setCdInscricaoFavorecido(response.getOcorrencias(i).getCdInscricaoFavorecido());
            saidaDTO.setDsAbrevFavorecido(response.getOcorrencias(i).getDsAbrevFavorecido());
            saidaDTO.setCdBancoDebito(response.getOcorrencias(i).getCdBancoDebito());
            saidaDTO.setCdAgenciaDebito(response.getOcorrencias(i).getCdAgenciaDebito());
            saidaDTO.setCdDigitoAgenciaDebito(response.getOcorrencias(i).getCdDigitoAgenciaDebito());
            saidaDTO.setCdContaDebito(response.getOcorrencias(i).getCdContaDebito());
            saidaDTO.setCdDigitoContaDebito(response.getOcorrencias(i).getCdDigitoContaDebito());
            saidaDTO.setCdBancoCredito(response.getOcorrencias(i).getCdBancoCredito());
            saidaDTO.setCdAgenciaCredito(response.getOcorrencias(i).getCdAgenciaCredito());
            saidaDTO.setCdDigitoAgenciaCredito(response.getOcorrencias(i).getCdDigitoAgenciaCredito());
            saidaDTO.setCdContaCredito(response.getOcorrencias(i).getCdContaCredito());
            saidaDTO.setCdDigitoContaCredito(response.getOcorrencias(i).getCdDigitoContaCredito());
            saidaDTO.setCdSituacaoOperacaoPagamento(response.getOcorrencias(i).getCdSituacaoOperacaoPagamento());
            saidaDTO.setCdMotivoSituacaoPagamento(response.getOcorrencias(i).getCdMotivoSituacaoPagamento());
            saidaDTO.setCdTipoCanal(response.getOcorrencias(i).getCdTipoCanal());
            saidaDTO.setDsTipoTela(response.getOcorrencias(i).getDsTipoTela());
            saidaDTO.setDsEfetivacaoPagamento(response.getOcorrencias(i).getDsEfetivacaoPagamento());
            
            saidaDTO.setCdBancoSalarial(response.getOcorrencias(i).getCdBancoSalarial());          
            saidaDTO.setCdAgenciaSalarial(response.getOcorrencias(i).getCdAgenciaSalarial());        
            saidaDTO.setCdDigitoAgenciaSalarial(response.getOcorrencias(i).getCdDigitoAgenciaSalarial());  
            saidaDTO.setCdContaSalarial(response.getOcorrencias(i).getCdContaSalarial());          
            saidaDTO.setCdDigitoContaSalarial(response.getOcorrencias(i).getCdDigitoContaSalarial());    
            
			saidaDTO.setCpfCnpjFormatado(CpfCnpjUtils.formatCpfCnpjCompleto(
					response.getOcorrencias(i).getCorpoCfpCnpj(), 
					response.getOcorrencias(i).getFilialCfpCnpj(), 
					response.getOcorrencias(i).getControleCfpCnpj()));
			saidaDTO.setFavorecidoBeneficiarioFormatado(concatenarCampos(response.getOcorrencias(i)
					.getCdInscricaoFavorecido(), response.getOcorrencias(i).getDsAbrevFavorecido(), "-"));
			saidaDTO.setBancoAgenciaContaDebitoFormatado(formatBancoAgenciaConta(saidaDTO.getCdBancoDebito(),
					saidaDTO.getCdAgenciaDebito(), saidaDTO.getCdDigitoAgenciaDebito(), saidaDTO
					.getCdContaDebito(), saidaDTO.getCdDigitoContaDebito(), true));
			
			String cdContaPagamentoDestino = response.getOcorrencias(i).getContaPagtoDestino();
			if(cdContaPagamentoDestino != null && !"".equals(cdContaPagamentoDestino) && !"0".equals(cdContaPagamentoDestino)){
				saidaDTO.setBancoAgenciaContaCreditoFormatado(PgitUtil.formatBancoIspbConta(response.getOcorrencias(i).getCdBancoCredito(), 
						response.getOcorrencias(i).getCdIspbPagtoDestino(), cdContaPagamentoDestino, true));
			}else{
				saidaDTO.setBancoAgenciaContaCreditoFormatado(formatBancoAgenciaConta(saidaDTO.getCdBancoCredito(),
						saidaDTO.getCdAgenciaCredito(), saidaDTO.getCdDigitoAgenciaCredito(), saidaDTO
						.getCdContaCredito(), saidaDTO.getCdDigitoContaCredito(), true));			
			}
			saidaDTO.setBancoDebitoFormatado(PgitUtil.formatBanco(response
					.getOcorrencias(i).getCdBancoDebito(), false));
			saidaDTO.setAgenciaDebitoFormatada(PgitUtil.formatAgencia(response.getOcorrencias(i).getCdAgenciaDebito(), 
					response.getOcorrencias(i).getCdDigitoAgenciaDebito(), false));
			saidaDTO.setContaDebitoFormatada(PgitUtil.formatConta(response.getOcorrencias(i).getCdContaDebito(), 
					response.getOcorrencias(i).getCdDigitoContaDebito(), false));

			saidaDTO.setBancoCreditoFormatado(PgitUtil.formatBanco(response.getOcorrencias(i)
					.getCdBancoCredito(), false));
			saidaDTO.setAgenciaCreditoFormatada(PgitUtil.formatAgencia(response.getOcorrencias(i)
					.getCdAgenciaCredito(), response.getOcorrencias(i).getCdDigitoAgenciaCredito(), false));
			saidaDTO.setContaCreditoFormatada(PgitUtil.formatConta(response.getOcorrencias(i).getCdContaCredito(), 
					response.getOcorrencias(i).getCdDigitoContaCredito(), false));
            try {
				saidaDTO.setDtFormatada(formato2.format(formato1.parse(response.getOcorrencias(i).getDtCraditoPagamento())));
			} catch (IndexOutOfBoundsException e) {

			} catch (java.text.ParseException e) {
				saidaDTO.setDtFormatada("");
			}
			saidaDTO.setVlPagamentoFormatado(NumberUtils.format(response.getOcorrencias(i).getVlrEfetivacaoPagamentoCliente(),"#,##0.00"));
			saidaDTO.setDsIndicadorPagamento(response.getOcorrencias(i).getDsIndicadorPagamento());
			saidaDTO.setCdIspbPagtoDestino(response.getOcorrencias(i).getCdIspbPagtoDestino());
			saidaDTO.setContaPagtoDestino(response.getOcorrencias(i).getContaPagtoDestino());
				
            listaSaida.add(saidaDTO);
        }
    	
    	return listaSaida;
    }
}

