/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/implementation.ftl,v $
 * $Id: implementation.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: implementation.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */
 
package br.com.bradesco.web.pgit.service.business.autorizarlistasdebitos.impl;

import java.util.ArrayList;
import java.util.List;

import br.com.bradesco.web.aq.application.error.i18n.MessageHelperUtils;
import br.com.bradesco.web.pgit.service.business.autorizarlistasdebitos.IAutorizarListasDebitosService;
import br.com.bradesco.web.pgit.service.business.autorizarlistasdebitos.IAutorizarListasDebitosServiceConstants;
import br.com.bradesco.web.pgit.service.business.autorizarlistasdebitos.bean.AutDesListaDebitoIntegralEntradaDTO;
import br.com.bradesco.web.pgit.service.business.autorizarlistasdebitos.bean.AutDesListaDebitoIntegralSaidaDTO;
import br.com.bradesco.web.pgit.service.business.autorizarlistasdebitos.bean.AutDesListaDebitoParcialEntradaDTO;
import br.com.bradesco.web.pgit.service.business.autorizarlistasdebitos.bean.AutDesListaDebitoParcialSaidaDTO;
import br.com.bradesco.web.pgit.service.business.autorizarlistasdebitos.bean.ConsultarListaDebitoAutDesEntradaDTO;
import br.com.bradesco.web.pgit.service.business.autorizarlistasdebitos.bean.ConsultarListaDebitoAutDesSaidaDTO;
import br.com.bradesco.web.pgit.service.business.autorizarlistasdebitos.bean.DetalharListaDebitoAutDesEntradaDTO;
import br.com.bradesco.web.pgit.service.business.autorizarlistasdebitos.bean.DetalharListaDebitoAutDesSaidaDTO;
import br.com.bradesco.web.pgit.service.data.pdc.FactoryAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.autdeslistadebitointegral.request.AutDesListaDebitoIntegralRequest;
import br.com.bradesco.web.pgit.service.data.pdc.autdeslistadebitointegral.response.AutDesListaDebitoIntegralResponse;
import br.com.bradesco.web.pgit.service.data.pdc.autdeslistadebitoparcial.request.AutDesListaDebitoParcialRequest;
import br.com.bradesco.web.pgit.service.data.pdc.autdeslistadebitoparcial.response.AutDesListaDebitoParcialResponse;
import br.com.bradesco.web.pgit.service.data.pdc.consultarlistadebitoautdes.request.ConsultarListaDebitoAutDesRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarlistadebitoautdes.response.ConsultarListaDebitoAutDesResponse;
import br.com.bradesco.web.pgit.service.data.pdc.detalharlistadebitoautdes.request.DetalharListaDebitoAutDesRequest;
import br.com.bradesco.web.pgit.service.data.pdc.detalharlistadebitoautdes.response.DetalharListaDebitoAutDesResponse;
import br.com.bradesco.web.pgit.utils.CpfCnpjUtils;
import br.com.bradesco.web.pgit.utils.PgitUtil;
import br.com.bradesco.web.pgit.utils.SiteUtil;
import br.com.bradesco.web.pgit.view.converters.FormatarData;


/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Implementa��o do adaptador: AutorizarListasDebitos
 * </p>
 * 
 * @comment C�DIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public class AutorizarListasDebitosServiceImpl implements IAutorizarListasDebitosService {
	
	/** Atributo factoryAdapter. */
	private FactoryAdapter factoryAdapter;
	
    /**
     * Get: factoryAdapter.
     *
     * @return factoryAdapter
     */
    public FactoryAdapter getFactoryAdapter() {
		return factoryAdapter;
	}

	/**
	 * Set: factoryAdapter.
	 *
	 * @param factoryAdapter the factory adapter
	 */
	public void setFactoryAdapter(FactoryAdapter factoryAdapter) {
		this.factoryAdapter = factoryAdapter;
	}
	
   /**
    * (non-Javadoc)
    * @see br.com.bradesco.web.pgit.service.business.autorizarlistasdebitos.IAutorizarListasDebitosService#consultarListaDebitoAutDes(br.com.bradesco.web.pgit.service.business.autorizarlistasdebitos.bean.ConsultarListaDebitoAutDesEntradaDTO)
    */
   public List<ConsultarListaDebitoAutDesSaidaDTO> consultarListaDebitoAutDes (ConsultarListaDebitoAutDesEntradaDTO entradaDTO){
    	List<ConsultarListaDebitoAutDesSaidaDTO> listaRetorno = new ArrayList<ConsultarListaDebitoAutDesSaidaDTO>();
    	ConsultarListaDebitoAutDesRequest request = new ConsultarListaDebitoAutDesRequest();
    	ConsultarListaDebitoAutDesResponse response = new ConsultarListaDebitoAutDesResponse();
    	
        request.setCdTipoPesquisa(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoPesquisa()));
        request.setCdPessoaParticipacaoContrato(PgitUtil.verificaLongNulo(entradaDTO.getCdPessoaParticipacaoContrato()));
        request.setCdPessoaJuridicaContrato(PgitUtil.verificaLongNulo(entradaDTO.getCdPessoaJuridicaContrato()));
        request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoContratoNegocio()));
        request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(entradaDTO.getNrSequenciaContratoNegocio()));
        request.setDtCreditoPagamentoInicial(PgitUtil.verificaStringNula(entradaDTO.getDtCreditoPagamentoInicial()));
        request.setDtCreditoPagamentoFinal(PgitUtil.verificaStringNula(entradaDTO.getDtCreditoPagamentoFinal()));
        request.setCdListaDebitoPagamentoInicial(PgitUtil.verificaLongNulo(entradaDTO.getCdListaDebitoPagamentoInicial()));
        request.setCdListaDebitoPagamentoFinal(PgitUtil.verificaLongNulo(entradaDTO.getCdListaDebitoPagamentoFinal()));
        request.setCdBanco(PgitUtil.verificaIntegerNulo(entradaDTO.getCdBanco()));
        request.setCdAgencia(PgitUtil.verificaIntegerNulo(entradaDTO.getCdAgencua()));
        request.setCdDigitoAgencia(PgitUtil.verificaIntegerNulo(entradaDTO.getCdDigitoAgencia()));
        request.setCdConta(PgitUtil.verificaLongNulo(entradaDTO.getCdConta()));
        request.setCdDigitoConta(PgitUtil.DIGITO_CONTA);
        request.setCdProdutoServicoOperacao(PgitUtil.verificaIntegerNulo(entradaDTO.getCdProdutoServicoOperacao()));
        request.setCdProdutoServicoRelacionado(PgitUtil.verificaIntegerNulo(entradaDTO.getCdProdutoServicoRelacionado()));
        request.setCdTipoOperacao(IAutorizarListasDebitosServiceConstants.NUMERO_TIPO_OPERACAO_AUTORIZAR);
        request.setNrOcorrencias(IAutorizarListasDebitosServiceConstants.NUMERO_OCORRENCIAS_CONSULTAR);
        request.setCdTipoOperacao(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoOperacao()));
        request.setCdServicoCompostoPagamento(PgitUtil.verificaLongNulo(entradaDTO.getCdServicoCompostoPagamento()));
        
        response = getFactoryAdapter().getConsultarListaDebitoAutDesPDCAdapter().invokeProcess(request);
        
        ConsultarListaDebitoAutDesSaidaDTO saidaDTO;
        for(int i=0;i<response.getOcorrenciasCount();i++){
        	saidaDTO = new ConsultarListaDebitoAutDesSaidaDTO();
            saidaDTO.setCodMensagem(response.getCodMensagem());
            saidaDTO.setMensagem(response.getMensagem());
            saidaDTO.setNrCpfCnpj(response.getOcorrencias(i).getNrCpfCnpj());
            saidaDTO.setDsPessoaEmpresa(response.getOcorrencias(i).getDsPessoaEmpresa());
            saidaDTO.setCdPessoaJuridicaContrato(response.getOcorrencias(i).getCdPessoaJuridicaContrato());
            saidaDTO.setCdTipoContratoNegocio(response.getOcorrencias(i).getCdTipoContratoNegocio());
            saidaDTO.setNrSequenciaContratoNegocio(response.getOcorrencias(i).getNrSequenciaContratoNegocio());
            saidaDTO.setDsTipoContratoNegocio(response.getOcorrencias(i).getDsTipoContratoNegocio());
            saidaDTO.setCdCanal(response.getOcorrencias(i).getCdCanal());
            saidaDTO.setCdListaDebitoPagamento(response.getOcorrencias(i).getCdListaDebitoPagamento());
            saidaDTO.setCdSituacaoListaDebito(response.getOcorrencias(i).getCdSituacaoListaDebito());
            saidaDTO.setDsSituacaoListaDebito(response.getOcorrencias(i).getDsSituacaoListaDebito());
            saidaDTO.setDtPrevistaPagamento(response.getOcorrencias(i).getDtPrevistaPagamento());
            saidaDTO.setDtPrevistaPagamentoFormatada(FormatarData.formatarDataFromPdc(response.getOcorrencias(i).getDtPrevistaPagamento()));
            saidaDTO.setCdProdutoServicoOperacao(response.getOcorrencias(i).getCdProdutoServicoOperacao());
            saidaDTO.setDsResumoProdutoServico(response.getOcorrencias(i).getDsResumoProdutoServico());
            saidaDTO.setCdProdutoServicoRelacionado(response.getOcorrencias(i).getCdProdutoServicoRelacionado());
            saidaDTO.setDsProdutoServicoRelacionado(response.getOcorrencias(i).getDsProdutoServicoRelacionado());
            saidaDTO.setCdBanco(response.getOcorrencias(i).getCdBanco());
            saidaDTO.setDsBanco(response.getOcorrencias(i).getDsBanco());
            saidaDTO.setCdAgencia(response.getOcorrencias(i).getCdAgencia());
            saidaDTO.setCdDigitoAgencia(response.getOcorrencias(i).getCdDigitoAgencia());
            saidaDTO.setDsAgencia(response.getOcorrencias(i).getDsAgencia());
            saidaDTO.setCdConta(response.getOcorrencias(i).getCdConta());
            saidaDTO.setCdDigitoConta(response.getOcorrencias(i).getCdDigitoConta());
            saidaDTO.setCdTipoConta(response.getOcorrencias(i).getCdTipoConta());
            saidaDTO.setDsTipoConta(response.getOcorrencias(i).getDsTipoConta());
            saidaDTO.setQtPagamentoAutorizado(response.getOcorrencias(i).getQtPagamentoAutorizado());
            saidaDTO.setVlPagamentoAutorizado(response.getOcorrencias(i).getVlPagamentoAutorizado());
            saidaDTO.setQtPagamentoDesautorizado(response.getOcorrencias(i).getQtPagamentoDesautorizado());
            saidaDTO.setVlPagamentoDesautorizado(response.getOcorrencias(i).getVlPagamentoDesautorizado());
            saidaDTO.setQtPagamentoEfetivado(response.getOcorrencias(i).getQtPagamentoEfetivado());
            saidaDTO.setVlPagamentoEfetivado(response.getOcorrencias(i).getVlPagamentoEfetivado());
            saidaDTO.setQtPagamentoNaoEfetivado(response.getOcorrencias(i).getQtPagamentoNaoEfetivado());
            saidaDTO.setVlPagamentoNaoEfetivado(response.getOcorrencias(i).getVlPagamentoNaoEfetivado());
            saidaDTO.setQtPagamentoTotal(response.getOcorrencias(i).getQtPagamentoTotal());
            saidaDTO.setVlPagamentoTotal(response.getOcorrencias(i).getVlPagamentoTotal());
            saidaDTO.setDsEmpresa(response.getOcorrencias(i).getDsEmpresa());
            saidaDTO.setCdServicoCompostoPagamento(response.getOcorrencias(i).getCdServicoCompostoPagamento());
            
            saidaDTO.setContaDebitoFormatado(PgitUtil.formatBancoAgenciaConta(saidaDTO.getCdBanco(), saidaDTO.getCdAgencia(), saidaDTO.getCdDigitoAgencia(), saidaDTO.getCdConta(), saidaDTO.getCdDigitoConta(), true));
            
            saidaDTO.setNrCpfCnpjFormatado(SiteUtil.formatNumber(saidaDTO.getNrCpfCnpj(), 15));
        	if(!saidaDTO.getNrCpfCnpjFormatado().equals("000000000000000")){
        		if(saidaDTO.getNrCpfCnpjFormatado().substring(9, 13).equals("0000")){
        			saidaDTO.setNrCpfCnpjFormatado(CpfCnpjUtils.formatCpfCnpj(saidaDTO.getNrCpfCnpjFormatado(), 1));
        		}else{
        			saidaDTO.setNrCpfCnpjFormatado(CpfCnpjUtils.formatCpfCnpj(saidaDTO.getNrCpfCnpjFormatado(), 2));
        		}
        	}else{
        		saidaDTO.setNrCpfCnpjFormatado("");
        	}
            
            listaRetorno.add(saidaDTO);
        }
        
    	return listaRetorno;
    }
    
    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.autorizarlistasdebitos.IAutorizarListasDebitosService#detalharListaDebitoAutDes(br.com.bradesco.web.pgit.service.business.autorizarlistasdebitos.bean.DetalharListaDebitoAutDesEntradaDTO)
     */
    public List<DetalharListaDebitoAutDesSaidaDTO> detalharListaDebitoAutDes (DetalharListaDebitoAutDesEntradaDTO entradaDTO){
    	List<DetalharListaDebitoAutDesSaidaDTO> listaRetorno = new ArrayList<DetalharListaDebitoAutDesSaidaDTO>();
    	DetalharListaDebitoAutDesRequest request = new DetalharListaDebitoAutDesRequest();
    	DetalharListaDebitoAutDesResponse response = new DetalharListaDebitoAutDesResponse();
    	
        request.setNrOcorrencias(IAutorizarListasDebitosServiceConstants.NUMERO_OCORRENCIAS_CONSULTAR);
        request.setCdPessoaJuridicaContrato(PgitUtil.verificaLongNulo(entradaDTO.getCdPessoaJuridicaContrato()));
        request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoContratoNegocio()));
        request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(entradaDTO.getNrSequenciaContratoNegocio()));
        request.setCdTipoCanal(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoCanal()));
        request.setCdListaDebitoPagamento(PgitUtil.verificaLongNulo(entradaDTO.getCdListaDebitoPagamento()));
        request.setCdTipoOperacao(IAutorizarListasDebitosServiceConstants.NUMERO_TIPO_OPERACAO_AUTORIZAR);
        
        response = getFactoryAdapter().getDetalharListaDebitoAutDesPDCAdapter().invokeProcess(request);
        
        DetalharListaDebitoAutDesSaidaDTO saidaDTO;
        for(int i=0;i<response.getOcorrenciasCount();i++){
        	saidaDTO = new DetalharListaDebitoAutDesSaidaDTO();
            saidaDTO.setCodMensagem(response.getCodMensagem());
            saidaDTO.setMensagem(response.getMensagem());
            saidaDTO.setCdPessoaJuridicaLista(response.getOcorrencias(i).getCdPessoaJuridicaLista());
            saidaDTO.setCdTipoContratoLista(response.getOcorrencias(i).getCdTipoContratoLista());
            saidaDTO.setNrSequenciaContratoLista(response.getOcorrencias(i).getNrSequenciaContratoLista());
            saidaDTO.setCdTipoCanalLista(response.getOcorrencias(i).getCdTipoCanalLista());
            saidaDTO.setCdListaDebitoPagamento(response.getOcorrencias(i).getCdListaDebitoPagamento());
            saidaDTO.setCdPessoaJuridicaContrato(response.getOcorrencias(i).getCdPessoaJuridicaContrato());
            saidaDTO.setCdTipoContratoNegocio(response.getOcorrencias(i).getCdTipoContratoNegocio());
            saidaDTO.setNrSequenciaContratoNegoci(response.getOcorrencias(i).getNrSequenciaContratoNegoci());
            saidaDTO.setCdTipoCanal(response.getOcorrencias(i).getCdTipoCanal());
            saidaDTO.setCdControlePagamento(response.getOcorrencias(i).getCdControlePagamento());
            saidaDTO.setVlPagamento(response.getOcorrencias(i).getVlPagamento());
            saidaDTO.setCdCpfCnpj(response.getOcorrencias(i).getCdCpfCnpj());
            saidaDTO.setDsPessoaComplemento(response.getOcorrencias(i).getDsPessoaComplemento());
            saidaDTO.setCdBanco(response.getOcorrencias(i).getCdBanco());
            saidaDTO.setDsBanco(response.getOcorrencias(i).getDsBanco());
            saidaDTO.setCdAgencia(response.getOcorrencias(i).getCdAgencia());
            saidaDTO.setCdDigitoAgencia(response.getOcorrencias(i).getCdDigitoAgencia());
            saidaDTO.setDsAgencia(response.getOcorrencias(i).getDsAgencia());
            saidaDTO.setCdConta(response.getOcorrencias(i).getCdConta());
            saidaDTO.setCdDigitoConta(response.getOcorrencias(i).getCdDigitoConta());
            saidaDTO.setCdSituacaoOperacaoPagamento(response.getOcorrencias(i).getCdSituacaoOperacaoPagamento());
            saidaDTO.setDsAutorizacaoListaDebito(response.getOcorrencias(i).getCdSituacaoOperacaoPagamento() == 1 ? MessageHelperUtils.getI18nMessage("label_autorizado") : response.getOcorrencias(i).getCdSituacaoOperacaoPagamento() ==2? MessageHelperUtils.getI18nMessage("label_desautorizado") : "" );
            saidaDTO.setDsSituacaoPagamento(response.getOcorrencias(i).getDsSituacaoPagamento());
            
            saidaDTO.setFavorecidoFormatado(PgitUtil.concatenarCampos(saidaDTO.getCdCpfCnpj(), saidaDTO.getDsPessoaComplemento()));
            saidaDTO.setContaDebitoFormatado(PgitUtil.formatBancoAgenciaConta(saidaDTO.getCdBanco(), saidaDTO.getCdAgencia(), saidaDTO.getCdDigitoAgencia(), saidaDTO.getCdConta(), saidaDTO.getCdDigitoConta(), true));
            
            listaRetorno.add(saidaDTO);       	
        }
    	return listaRetorno;
    }
    
    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.autorizarlistasdebitos.IAutorizarListasDebitosService#autDesListaDebitoParcial(java.util.List)
     */
    public AutDesListaDebitoParcialSaidaDTO autDesListaDebitoParcial (List<AutDesListaDebitoParcialEntradaDTO> listaEntrada){
    	AutDesListaDebitoParcialSaidaDTO saida = new AutDesListaDebitoParcialSaidaDTO();
    	AutDesListaDebitoParcialRequest request = new AutDesListaDebitoParcialRequest();
    	AutDesListaDebitoParcialResponse response = new AutDesListaDebitoParcialResponse();
    	
    	request.setQtRegistros(listaEntrada.size());
    	request.setCdTipoOperacao(IAutorizarListasDebitosServiceConstants.NUMERO_TIPO_OPERACAO_AUTORIZAR);
    	ArrayList<br.com.bradesco.web.pgit.service.data.pdc.autdeslistadebitoparcial.request.Ocorrencias> ocorrencias = new ArrayList<br.com.bradesco.web.pgit.service.data.pdc.autdeslistadebitoparcial.request.Ocorrencias>();
    	br.com.bradesco.web.pgit.service.data.pdc.autdeslistadebitoparcial.request.Ocorrencias ocorrencia;
    	
    	for(int i=0;i<listaEntrada.size();i++){
    		ocorrencia = new br.com.bradesco.web.pgit.service.data.pdc.autdeslistadebitoparcial.request.Ocorrencias();
    	    ocorrencia.setCdPessoaJuridicaLista(PgitUtil.verificaLongNulo(listaEntrada.get(i).getCdPessoaJuridicaLista()));
    	    ocorrencia.setCdTipoContratoLista(PgitUtil.verificaIntegerNulo(listaEntrada.get(i).getCdTipoContratoLista()));
    	    ocorrencia.setNrSequenciaContratoLista(PgitUtil.verificaLongNulo(listaEntrada.get(i).getNrSequenciaContratoLista()));
    	    ocorrencia.setCdTipoCanalLista(PgitUtil.verificaIntegerNulo(listaEntrada.get(i).getCdTipoCanalLista()));
    	    ocorrencia.setCdListaDebitoPagamento(PgitUtil.verificaLongNulo(listaEntrada.get(i).getCdListaDebitoPagamento()));
    	    ocorrencia.setCdPessoaJuridicaContrato(PgitUtil.verificaLongNulo(listaEntrada.get(i).getCdPessoaJuridicaContrato()));
    	    ocorrencia.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(listaEntrada.get(i).getCdTipoContratoNegocio()));
    	    ocorrencia.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(listaEntrada.get(i).getNrSequenciaContratoNegocio()));
    	    ocorrencia.setCdTipoCanal(PgitUtil.verificaIntegerNulo(listaEntrada.get(i).getCdTipoCanal()));
    	    ocorrencia.setCdControlePagamento(PgitUtil.verificaStringNula(listaEntrada.get(i).getCdControlePagamento()));
    	    ocorrencia.setVlPagamento(PgitUtil.verificaBigDecimalNulo(listaEntrada.get(i).getVlPagamento()));
    	    
    	    ocorrencias.add(ocorrencia);
    	}
    	
    	request.setOcorrencias(ocorrencias.toArray(new br.com.bradesco.web.pgit.service.data.pdc.autdeslistadebitoparcial.request.Ocorrencias[0]));
    	
    	response = getFactoryAdapter().getAutDesListaDebitoParcialPDCAdapter().invokeProcess(request);
    	
    	saida.setCodMensagem(response.getCodMensagem());
    	saida.setMensagem(response.getMensagem());
    	
    	return saida;
    }
    
    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.autorizarlistasdebitos.IAutorizarListasDebitosService#autDesListaDebitoIntegral(br.com.bradesco.web.pgit.service.business.autorizarlistasdebitos.bean.AutDesListaDebitoIntegralEntradaDTO)
     */
    public AutDesListaDebitoIntegralSaidaDTO autDesListaDebitoIntegral (AutDesListaDebitoIntegralEntradaDTO entradaDTO){
    	AutDesListaDebitoIntegralSaidaDTO saidaDTO = new AutDesListaDebitoIntegralSaidaDTO();
    	AutDesListaDebitoIntegralRequest request = new AutDesListaDebitoIntegralRequest();
    	AutDesListaDebitoIntegralResponse response = new AutDesListaDebitoIntegralResponse();
    	
    	request.setCdTipoOperacao(IAutorizarListasDebitosServiceConstants.NUMERO_TIPO_OPERACAO_AUTORIZAR);
    	request.setCdPessoaJuridicaContrato(PgitUtil.verificaLongNulo(entradaDTO.getCdPessoaJuridicaContrato()));
    	request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoContratoNegocio()));
    	request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(entradaDTO.getNrSequenciaContratoNegocio()));
    	request.setCdTipoCanal(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoCanal()));
    	request.setCdListaDebitoPagamento(PgitUtil.verificaLongNulo(entradaDTO.getCdListaDebitoPagamento()));
    	
    	response = getFactoryAdapter().getAutDesListaDebitoIntegralPDCAdapter().invokeProcess(request);
    	
    	saidaDTO.setCodMensagem(response.getCodMensagem());
    	saidaDTO.setMensagem(response.getMensagem());
    	
    	return saidaDTO;
    }
}