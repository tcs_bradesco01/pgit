/*
 * Nome: br.com.bradesco.web.pgit.service.business.combo.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.combo.bean;

/**
 * Nome: ListarClasseRamoAtvddSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarClasseRamoAtvddSaidaDTO {
    
    /** Atributo codMensagem. */
    private String codMensagem;
    
    /** Atributo mensagem. */
    private String mensagem;    
    
    /** Atributo cdClassAtividade. */
    private String cdClassAtividade;
    
    /** Atributo cdRamoAtividade. */
    private Integer cdRamoAtividade;
    
    /** Atributo dsRamoAtividade. */
    private String dsRamoAtividade;
    
    /** Atributo classeRamoFormatado. */
    private String classeRamoFormatado;
    
    /**
     * Get: classeRamoFormatado.
     *
     * @return classeRamoFormatado
     */
    public String getClasseRamoFormatado() {
        return classeRamoFormatado;
    }
    
    /**
     * Set: classeRamoFormatado.
     *
     * @param classeRamoFormatado the classe ramo formatado
     */
    public void setClasseRamoFormatado(String classeRamoFormatado) {
        this.classeRamoFormatado = classeRamoFormatado;
    }
    
    /**
     * Get: cdClassAtividade.
     *
     * @return cdClassAtividade
     */
    public String getCdClassAtividade() {
        return cdClassAtividade;
    }
    
    /**
     * Set: cdClassAtividade.
     *
     * @param cdClassAtividade the cd class atividade
     */
    public void setCdClassAtividade(String cdClassAtividade) {
        this.cdClassAtividade = cdClassAtividade;
    }
    
    /**
     * Get: cdRamoAtividade.
     *
     * @return cdRamoAtividade
     */
    public Integer getCdRamoAtividade() {
        return cdRamoAtividade;
    }
    
    /**
     * Set: cdRamoAtividade.
     *
     * @param cdRamoAtividade the cd ramo atividade
     */
    public void setCdRamoAtividade(Integer cdRamoAtividade) {
        this.cdRamoAtividade = cdRamoAtividade;
    }
    
    /**
     * Get: codMensagem.
     *
     * @return codMensagem
     */
    public String getCodMensagem() {
        return codMensagem;
    }
    
    /**
     * Set: codMensagem.
     *
     * @param codMensagem the cod mensagem
     */
    public void setCodMensagem(String codMensagem) {
        this.codMensagem = codMensagem;
    }
    
    /**
     * Get: dsRamoAtividade.
     *
     * @return dsRamoAtividade
     */
    public String getDsRamoAtividade() {
        return dsRamoAtividade;
    }
    
    /**
     * Set: dsRamoAtividade.
     *
     * @param dsRamoAtividade the ds ramo atividade
     */
    public void setDsRamoAtividade(String dsRamoAtividade) {
        this.dsRamoAtividade = dsRamoAtividade;
    }
    
    /**
     * Get: mensagem.
     *
     * @return mensagem
     */
    public String getMensagem() {
        return mensagem;
    }
    
    /**
     * Set: mensagem.
     *
     * @param mensagem the mensagem
     */
    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }
    
    
}
