/**
 * Nome: br.com.bradesco.web.pgit.service.business.solrelempresasacomp.impl
 * Compilador: JDK 1.5
 * Prop�sito: INSERIR O PROP�SITO DAS CLASSES DO PACOTE
 * Data da cria��o: <dd/MM/yyyy>
 * Par�metros de compila��o: -d
 */ 
package br.com.bradesco.web.pgit.service.business.solrelempresasacomp.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import br.com.bradesco.web.pgit.service.business.geral.MensagemSaida;
import br.com.bradesco.web.pgit.service.business.solrelempresasacomp.ISolRelEmpresasAcompService;
import br.com.bradesco.web.pgit.service.business.solrelempresasacomp.ISolrelempresasacompServiceConstants;
import br.com.bradesco.web.pgit.service.business.solrelempresasacomp.bean.ExcluirSoliRelatorioEmpresaAcompanhadaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.solrelempresasacomp.bean.IncluirSoliRelatorioEmpresaAcompanhadaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.solrelempresasacomp.bean.IncluirSoliRelatorioEmpresaAcompanhadaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.solrelempresasacomp.bean.SolicitacaoRelatorioEmpresasAcompOcorrencia;
import br.com.bradesco.web.pgit.service.data.pdc.FactoryAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.excluirsolirelatorioempresaacompanhada.request.ExcluirSoliRelatorioEmpresaAcompanhadaRequest;
import br.com.bradesco.web.pgit.service.data.pdc.excluirsolirelatorioempresaacompanhada.response.ExcluirSoliRelatorioEmpresaAcompanhadaResponse;
import br.com.bradesco.web.pgit.service.data.pdc.incluirsolirelatorioempresaacompanhada.request.IncluirSoliRelatorioEmpresaAcompanhadaRequest;
import br.com.bradesco.web.pgit.service.data.pdc.incluirsolirelatorioempresaacompanhada.response.IncluirSoliRelatorioEmpresaAcompanhadaResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarsolirelatorioempresaacompanhada.request.ListarSoliRelatorioEmpresaAcompanhadaRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarsolirelatorioempresaacompanhada.response.ListarSoliRelatorioEmpresaAcompanhadaResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarsolirelatorioempresaacompanhada.response.Ocorrencias;
import br.com.bradesco.web.pgit.utils.PgitUtil;
import br.com.bradesco.web.pgit.view.converters.FormatarData;

/**
 * Nome: SolrelempresasacompServiceImpl
 * <p>
 * Prop�sito: Implementa��o do adaptador Solrelempresasacomp
 * </p>
 * 
 * @author CPM Braxis / TI Melhorias - Arquitetura
 * @version 1.0
 * @see ISolRelEmpresasAcompService
 */
public class SolRelEmpresasAcompServiceImpl implements ISolRelEmpresasAcompService {

	/** Atributo factoryAdapter. */
	private FactoryAdapter factoryAdapter; 

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.solrelempresasacomp.ISolRelEmpresasAcompService#listarSolicitacoes(java.util.Date, java.util.Date)
	 */
	public List<SolicitacaoRelatorioEmpresasAcompOcorrencia> listarSolicitacoes(Date dataInicial, Date dataFinal) {
		ListarSoliRelatorioEmpresaAcompanhadaRequest request = new ListarSoliRelatorioEmpresaAcompanhadaRequest();
		request.setMaxOcorrencias(ISolrelempresasacompServiceConstants.MAX_OCORRENCIAS_50);
		request.setDtInicio(FormatarData.formataDiaMesAnoToPdc(dataInicial));
		request.setDtFim(FormatarData.formataDiaMesAnoToPdc(dataFinal));

		ListarSoliRelatorioEmpresaAcompanhadaResponse response = factoryAdapter.getListarSoliRelatorioEmpresaAcompanhadaPDCAdapter().invokeProcess(request);

		List<SolicitacaoRelatorioEmpresasAcompOcorrencia> solicitacoes = new ArrayList<SolicitacaoRelatorioEmpresasAcompOcorrencia>();
		for (Ocorrencias solicitacao : response.getOcorrencias()) {
			solicitacoes.add(new SolicitacaoRelatorioEmpresasAcompOcorrencia(solicitacao));
		}

		return solicitacoes;
	}
	
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.solrelempresasacomp.ISolRelEmpresasAcompService#incluirSolicitacoes(br.com.bradesco.web.pgit.service.business.solrelempresasacomp.bean.IncluirSoliRelatorioEmpresaAcompanhadaEntradaDTO)
	 */
	public IncluirSoliRelatorioEmpresaAcompanhadaSaidaDTO incluirSolicitacoes(IncluirSoliRelatorioEmpresaAcompanhadaEntradaDTO entrada) {
		IncluirSoliRelatorioEmpresaAcompanhadaRequest request = new IncluirSoliRelatorioEmpresaAcompanhadaRequest();
		IncluirSoliRelatorioEmpresaAcompanhadaSaidaDTO objSaida = new IncluirSoliRelatorioEmpresaAcompanhadaSaidaDTO();
		
		request.setCdTipoAcesso(PgitUtil.verificaIntegerNulo(entrada.getCdTipoAcesso()));
		request.setCdUsuarioInclusao(PgitUtil.verificaStringNula(entrada.getCdUsuarioInclusao()));
		request.setHrSolicitacaoPagamento(PgitUtil.verificaStringNula(entrada.getHrSolicitacaoPagamento()));
		
		IncluirSoliRelatorioEmpresaAcompanhadaResponse response = factoryAdapter.getIncluirSoliRelatorioEmpresaAcompanhadaPDCAdapter().invokeProcess(request);
		
		objSaida.setCodMensagem(response.getCodMensagem());
		objSaida.setMensagem(response.getMensagem());
		objSaida.setCdUsuarioInclusao(response.getCdUsuarioInclusao());
		objSaida.setDtPagtoIntegrado(response.getDtPagtoIntegrado());
		objSaida.setDtHoraPagamentoIntegrado(response.getDtHoraPagamentoIntegrado());
		
		return objSaida;
	}
	
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.solrelempresasacomp.ISolRelEmpresasAcompService#excluirSolicitacoes(br.com.bradesco.web.pgit.service.business.solrelempresasacomp.bean.ExcluirSoliRelatorioEmpresaAcompanhadaEntradaDTO)
	 */
	public MensagemSaida excluirSolicitacoes(ExcluirSoliRelatorioEmpresaAcompanhadaEntradaDTO entrada) {
		ExcluirSoliRelatorioEmpresaAcompanhadaRequest request = new ExcluirSoliRelatorioEmpresaAcompanhadaRequest();
		
		request.setCdSolicitacaoPagamentoIntegrado(PgitUtil.verificaIntegerNulo(entrada.getCdSolicitacaoPagamentoIntegrado()));
		request.setNrSolicitacaoPagamentoIntegrado(PgitUtil.verificaIntegerNulo(entrada.getNrSolicitacaoPagamentoIntegrado()));
        
		ExcluirSoliRelatorioEmpresaAcompanhadaResponse response = factoryAdapter.getExcluirSoliRelatorioEmpresaAcompanhadaPDCAdapter().invokeProcess(request);
		            
		return new MensagemSaida(response.getCodMensagem(),response.getMensagem());
	}

	/**
	 * Get: factoryAdapter.
	 *
	 * @return factoryAdapter
	 */
	public FactoryAdapter getFactoryAdapter() {
		return factoryAdapter;
	}

	/**
	 * Set: factoryAdapter.
	 *
	 * @param factoryAdapter the factory adapter
	 */
	public void setFactoryAdapter(FactoryAdapter factoryAdapter) {
		this.factoryAdapter = factoryAdapter;
	}
}