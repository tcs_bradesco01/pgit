/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.registrarpendenciacontratopgit.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class RegistrarPendenciaContratoPgitRequest.
 * 
 * @version $Revision$ $Date$
 */
public class RegistrarPendenciaContratoPgitRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdPessoaJuridicaContrato
     */
    private long _cdPessoaJuridicaContrato = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridicaContrato
     */
    private boolean _has_cdPessoaJuridicaContrato;

    /**
     * Field _cdTipoContratoNegocio
     */
    private int _cdTipoContratoNegocio = 0;

    /**
     * keeps track of state for field: _cdTipoContratoNegocio
     */
    private boolean _has_cdTipoContratoNegocio;

    /**
     * Field _nrSequenciaContratoNegocio
     */
    private long _nrSequenciaContratoNegocio = 0;

    /**
     * keeps track of state for field: _nrSequenciaContratoNegocio
     */
    private boolean _has_nrSequenciaContratoNegocio;

    /**
     * Field _cdPendenciaPagamentoIntegrado
     */
    private long _cdPendenciaPagamentoIntegrado = 0;

    /**
     * keeps track of state for field: _cdPendenciaPagamentoIntegrad
     */
    private boolean _has_cdPendenciaPagamentoIntegrado;

    /**
     * Field _nrPendenciaPagamentoIntegrado
     */
    private long _nrPendenciaPagamentoIntegrado = 0;

    /**
     * keeps track of state for field: _nrPendenciaPagamentoIntegrad
     */
    private boolean _has_nrPendenciaPagamentoIntegrado;

    /**
     * Field _cdSituacaoPendenciaContrato
     */
    private int _cdSituacaoPendenciaContrato = 0;

    /**
     * keeps track of state for field: _cdSituacaoPendenciaContrato
     */
    private boolean _has_cdSituacaoPendenciaContrato;

    /**
     * Field _cdTipoBaixaPendencia
     */
    private int _cdTipoBaixaPendencia = 0;

    /**
     * keeps track of state for field: _cdTipoBaixaPendencia
     */
    private boolean _has_cdTipoBaixaPendencia;

    /**
     * Field _dtAtendimentoPendenciaContrato
     */
    private java.lang.String _dtAtendimentoPendenciaContrato;

    /**
     * Field _cdMotivoBaixaPendencia
     */
    private int _cdMotivoBaixaPendencia = 0;

    /**
     * keeps track of state for field: _cdMotivoBaixaPendencia
     */
    private boolean _has_cdMotivoBaixaPendencia;


      //----------------/
     //- Constructors -/
    //----------------/

    public RegistrarPendenciaContratoPgitRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.registrarpendenciacontratopgit.request.RegistrarPendenciaContratoPgitRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdMotivoBaixaPendencia
     * 
     */
    public void deleteCdMotivoBaixaPendencia()
    {
        this._has_cdMotivoBaixaPendencia= false;
    } //-- void deleteCdMotivoBaixaPendencia() 

    /**
     * Method deleteCdPendenciaPagamentoIntegrado
     * 
     */
    public void deleteCdPendenciaPagamentoIntegrado()
    {
        this._has_cdPendenciaPagamentoIntegrado= false;
    } //-- void deleteCdPendenciaPagamentoIntegrado() 

    /**
     * Method deleteCdPessoaJuridicaContrato
     * 
     */
    public void deleteCdPessoaJuridicaContrato()
    {
        this._has_cdPessoaJuridicaContrato= false;
    } //-- void deleteCdPessoaJuridicaContrato() 

    /**
     * Method deleteCdSituacaoPendenciaContrato
     * 
     */
    public void deleteCdSituacaoPendenciaContrato()
    {
        this._has_cdSituacaoPendenciaContrato= false;
    } //-- void deleteCdSituacaoPendenciaContrato() 

    /**
     * Method deleteCdTipoBaixaPendencia
     * 
     */
    public void deleteCdTipoBaixaPendencia()
    {
        this._has_cdTipoBaixaPendencia= false;
    } //-- void deleteCdTipoBaixaPendencia() 

    /**
     * Method deleteCdTipoContratoNegocio
     * 
     */
    public void deleteCdTipoContratoNegocio()
    {
        this._has_cdTipoContratoNegocio= false;
    } //-- void deleteCdTipoContratoNegocio() 

    /**
     * Method deleteNrPendenciaPagamentoIntegrado
     * 
     */
    public void deleteNrPendenciaPagamentoIntegrado()
    {
        this._has_nrPendenciaPagamentoIntegrado= false;
    } //-- void deleteNrPendenciaPagamentoIntegrado() 

    /**
     * Method deleteNrSequenciaContratoNegocio
     * 
     */
    public void deleteNrSequenciaContratoNegocio()
    {
        this._has_nrSequenciaContratoNegocio= false;
    } //-- void deleteNrSequenciaContratoNegocio() 

    /**
     * Returns the value of field 'cdMotivoBaixaPendencia'.
     * 
     * @return int
     * @return the value of field 'cdMotivoBaixaPendencia'.
     */
    public int getCdMotivoBaixaPendencia()
    {
        return this._cdMotivoBaixaPendencia;
    } //-- int getCdMotivoBaixaPendencia() 

    /**
     * Returns the value of field 'cdPendenciaPagamentoIntegrado'.
     * 
     * @return long
     * @return the value of field 'cdPendenciaPagamentoIntegrado'.
     */
    public long getCdPendenciaPagamentoIntegrado()
    {
        return this._cdPendenciaPagamentoIntegrado;
    } //-- long getCdPendenciaPagamentoIntegrado() 

    /**
     * Returns the value of field 'cdPessoaJuridicaContrato'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridicaContrato'.
     */
    public long getCdPessoaJuridicaContrato()
    {
        return this._cdPessoaJuridicaContrato;
    } //-- long getCdPessoaJuridicaContrato() 

    /**
     * Returns the value of field 'cdSituacaoPendenciaContrato'.
     * 
     * @return int
     * @return the value of field 'cdSituacaoPendenciaContrato'.
     */
    public int getCdSituacaoPendenciaContrato()
    {
        return this._cdSituacaoPendenciaContrato;
    } //-- int getCdSituacaoPendenciaContrato() 

    /**
     * Returns the value of field 'cdTipoBaixaPendencia'.
     * 
     * @return int
     * @return the value of field 'cdTipoBaixaPendencia'.
     */
    public int getCdTipoBaixaPendencia()
    {
        return this._cdTipoBaixaPendencia;
    } //-- int getCdTipoBaixaPendencia() 

    /**
     * Returns the value of field 'cdTipoContratoNegocio'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoNegocio'.
     */
    public int getCdTipoContratoNegocio()
    {
        return this._cdTipoContratoNegocio;
    } //-- int getCdTipoContratoNegocio() 

    /**
     * Returns the value of field 'dtAtendimentoPendenciaContrato'.
     * 
     * @return String
     * @return the value of field 'dtAtendimentoPendenciaContrato'.
     */
    public java.lang.String getDtAtendimentoPendenciaContrato()
    {
        return this._dtAtendimentoPendenciaContrato;
    } //-- java.lang.String getDtAtendimentoPendenciaContrato() 

    /**
     * Returns the value of field 'nrPendenciaPagamentoIntegrado'.
     * 
     * @return long
     * @return the value of field 'nrPendenciaPagamentoIntegrado'.
     */
    public long getNrPendenciaPagamentoIntegrado()
    {
        return this._nrPendenciaPagamentoIntegrado;
    } //-- long getNrPendenciaPagamentoIntegrado() 

    /**
     * Returns the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @return long
     * @return the value of field 'nrSequenciaContratoNegocio'.
     */
    public long getNrSequenciaContratoNegocio()
    {
        return this._nrSequenciaContratoNegocio;
    } //-- long getNrSequenciaContratoNegocio() 

    /**
     * Method hasCdMotivoBaixaPendencia
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMotivoBaixaPendencia()
    {
        return this._has_cdMotivoBaixaPendencia;
    } //-- boolean hasCdMotivoBaixaPendencia() 

    /**
     * Method hasCdPendenciaPagamentoIntegrado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPendenciaPagamentoIntegrado()
    {
        return this._has_cdPendenciaPagamentoIntegrado;
    } //-- boolean hasCdPendenciaPagamentoIntegrado() 

    /**
     * Method hasCdPessoaJuridicaContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridicaContrato()
    {
        return this._has_cdPessoaJuridicaContrato;
    } //-- boolean hasCdPessoaJuridicaContrato() 

    /**
     * Method hasCdSituacaoPendenciaContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSituacaoPendenciaContrato()
    {
        return this._has_cdSituacaoPendenciaContrato;
    } //-- boolean hasCdSituacaoPendenciaContrato() 

    /**
     * Method hasCdTipoBaixaPendencia
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoBaixaPendencia()
    {
        return this._has_cdTipoBaixaPendencia;
    } //-- boolean hasCdTipoBaixaPendencia() 

    /**
     * Method hasCdTipoContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoNegocio()
    {
        return this._has_cdTipoContratoNegocio;
    } //-- boolean hasCdTipoContratoNegocio() 

    /**
     * Method hasNrPendenciaPagamentoIntegrado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrPendenciaPagamentoIntegrado()
    {
        return this._has_nrPendenciaPagamentoIntegrado;
    } //-- boolean hasNrPendenciaPagamentoIntegrado() 

    /**
     * Method hasNrSequenciaContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaContratoNegocio()
    {
        return this._has_nrSequenciaContratoNegocio;
    } //-- boolean hasNrSequenciaContratoNegocio() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdMotivoBaixaPendencia'.
     * 
     * @param cdMotivoBaixaPendencia the value of field
     * 'cdMotivoBaixaPendencia'.
     */
    public void setCdMotivoBaixaPendencia(int cdMotivoBaixaPendencia)
    {
        this._cdMotivoBaixaPendencia = cdMotivoBaixaPendencia;
        this._has_cdMotivoBaixaPendencia = true;
    } //-- void setCdMotivoBaixaPendencia(int) 

    /**
     * Sets the value of field 'cdPendenciaPagamentoIntegrado'.
     * 
     * @param cdPendenciaPagamentoIntegrado the value of field
     * 'cdPendenciaPagamentoIntegrado'.
     */
    public void setCdPendenciaPagamentoIntegrado(long cdPendenciaPagamentoIntegrado)
    {
        this._cdPendenciaPagamentoIntegrado = cdPendenciaPagamentoIntegrado;
        this._has_cdPendenciaPagamentoIntegrado = true;
    } //-- void setCdPendenciaPagamentoIntegrado(long) 

    /**
     * Sets the value of field 'cdPessoaJuridicaContrato'.
     * 
     * @param cdPessoaJuridicaContrato the value of field
     * 'cdPessoaJuridicaContrato'.
     */
    public void setCdPessoaJuridicaContrato(long cdPessoaJuridicaContrato)
    {
        this._cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
        this._has_cdPessoaJuridicaContrato = true;
    } //-- void setCdPessoaJuridicaContrato(long) 

    /**
     * Sets the value of field 'cdSituacaoPendenciaContrato'.
     * 
     * @param cdSituacaoPendenciaContrato the value of field
     * 'cdSituacaoPendenciaContrato'.
     */
    public void setCdSituacaoPendenciaContrato(int cdSituacaoPendenciaContrato)
    {
        this._cdSituacaoPendenciaContrato = cdSituacaoPendenciaContrato;
        this._has_cdSituacaoPendenciaContrato = true;
    } //-- void setCdSituacaoPendenciaContrato(int) 

    /**
     * Sets the value of field 'cdTipoBaixaPendencia'.
     * 
     * @param cdTipoBaixaPendencia the value of field
     * 'cdTipoBaixaPendencia'.
     */
    public void setCdTipoBaixaPendencia(int cdTipoBaixaPendencia)
    {
        this._cdTipoBaixaPendencia = cdTipoBaixaPendencia;
        this._has_cdTipoBaixaPendencia = true;
    } //-- void setCdTipoBaixaPendencia(int) 

    /**
     * Sets the value of field 'cdTipoContratoNegocio'.
     * 
     * @param cdTipoContratoNegocio the value of field
     * 'cdTipoContratoNegocio'.
     */
    public void setCdTipoContratoNegocio(int cdTipoContratoNegocio)
    {
        this._cdTipoContratoNegocio = cdTipoContratoNegocio;
        this._has_cdTipoContratoNegocio = true;
    } //-- void setCdTipoContratoNegocio(int) 

    /**
     * Sets the value of field 'dtAtendimentoPendenciaContrato'.
     * 
     * @param dtAtendimentoPendenciaContrato the value of field
     * 'dtAtendimentoPendenciaContrato'.
     */
    public void setDtAtendimentoPendenciaContrato(java.lang.String dtAtendimentoPendenciaContrato)
    {
        this._dtAtendimentoPendenciaContrato = dtAtendimentoPendenciaContrato;
    } //-- void setDtAtendimentoPendenciaContrato(java.lang.String) 

    /**
     * Sets the value of field 'nrPendenciaPagamentoIntegrado'.
     * 
     * @param nrPendenciaPagamentoIntegrado the value of field
     * 'nrPendenciaPagamentoIntegrado'.
     */
    public void setNrPendenciaPagamentoIntegrado(long nrPendenciaPagamentoIntegrado)
    {
        this._nrPendenciaPagamentoIntegrado = nrPendenciaPagamentoIntegrado;
        this._has_nrPendenciaPagamentoIntegrado = true;
    } //-- void setNrPendenciaPagamentoIntegrado(long) 

    /**
     * Sets the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @param nrSequenciaContratoNegocio the value of field
     * 'nrSequenciaContratoNegocio'.
     */
    public void setNrSequenciaContratoNegocio(long nrSequenciaContratoNegocio)
    {
        this._nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
        this._has_nrSequenciaContratoNegocio = true;
    } //-- void setNrSequenciaContratoNegocio(long) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return RegistrarPendenciaContratoPgitRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.registrarpendenciacontratopgit.request.RegistrarPendenciaContratoPgitRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.registrarpendenciacontratopgit.request.RegistrarPendenciaContratoPgitRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.registrarpendenciacontratopgit.request.RegistrarPendenciaContratoPgitRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.registrarpendenciacontratopgit.request.RegistrarPendenciaContratoPgitRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
