/*
 * =========================================================================
 * 
 * Client:       Bradesco (BR)
 * Project:      Arquitetura Bradesco Canal Internet
 * Development:  GFT Iberia (http://www.gft.com)
 * -------------------------------------------------------------------------
 * Revision - Last:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/constants.ftl,v $
 * $Id: constants.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revision - History:
 * $Log: constants.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.consultas;


/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Interface de constantes do adaptador: Consultas
 * </p>
 * 
 * @comment C�DIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public interface IConsultasServiceConstants {
	
	/** Atributo NUMERO_OCORRENCIAS_CONSULTAR_ENDERECO. */
	int NUMERO_OCORRENCIAS_CONSULTAR_ENDERECO = 50;
	
	/** Atributo NUMERO_OCORRENCIAS_CONSULTAR_EMAIL. */
	int NUMERO_OCORRENCIAS_CONSULTAR_EMAIL = 50;
	
	/** Atributo NUMERO_OCORRENCIAS_LISTAR_TIPO_RETIRADA. */
	int NUMERO_OCORRENCIAS_LISTAR_TIPO_RETIRADA = 50;

	/** Atributo CODIGO_PESSOA_JURIDICA_BRAD. */
	long CODIGO_PESSOA_JURIDICA_BRAD = 2269651;
	
	/** Atributo CODIGO_TIPO_CONTRATO_PGIT. */
	int CODIGO_TIPO_CONTRATO_PGIT = 21;
}

