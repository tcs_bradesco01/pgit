/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.incluirequiparacaocontrato.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdTipoSelecao
     */
    private int _cdTipoSelecao;

    /**
     * keeps track of state for field: _cdTipoSelecao
     */
    private boolean _has_cdTipoSelecao;

    /**
     * Field _cdPessoaJuridicaContrato1
     */
    private long _cdPessoaJuridicaContrato1;

    /**
     * keeps track of state for field: _cdPessoaJuridicaContrato1
     */
    private boolean _has_cdPessoaJuridicaContrato1;

    /**
     * Field _cdTipoContratoNegocio1
     */
    private int _cdTipoContratoNegocio1;

    /**
     * keeps track of state for field: _cdTipoContratoNegocio1
     */
    private boolean _has_cdTipoContratoNegocio1;

    /**
     * Field _nrSequenciaContratoNegocio1
     */
    private long _nrSequenciaContratoNegocio1;

    /**
     * keeps track of state for field: _nrSequenciaContratoNegocio1
     */
    private boolean _has_nrSequenciaContratoNegocio1;

    /**
     * Field _cdPessoaJuridicaContrato2
     */
    private long _cdPessoaJuridicaContrato2;

    /**
     * keeps track of state for field: _cdPessoaJuridicaContrato2
     */
    private boolean _has_cdPessoaJuridicaContrato2;

    /**
     * Field _cdTipoContratoNegocio2
     */
    private int _cdTipoContratoNegocio2;

    /**
     * keeps track of state for field: _cdTipoContratoNegocio2
     */
    private boolean _has_cdTipoContratoNegocio2;

    /**
     * Field _nrSequenciaContratoNegocio2
     */
    private long _nrSequenciaContratoNegocio2;

    /**
     * keeps track of state for field: _nrSequenciaContratoNegocio2
     */
    private boolean _has_nrSequenciaContratoNegocio2;

    /**
     * Field _cdProdutoServicoOperacao
     */
    private int _cdProdutoServicoOperacao;

    /**
     * keeps track of state for field: _cdProdutoServicoOperacao
     */
    private boolean _has_cdProdutoServicoOperacao;

    /**
     * Field _cdProdutoOperacaoRelacionado
     */
    private int _cdProdutoOperacaoRelacionado;

    /**
     * keeps track of state for field: _cdProdutoOperacaoRelacionado
     */
    private boolean _has_cdProdutoOperacaoRelacionado;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.incluirequiparacaocontrato.request.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdPessoaJuridicaContrato1
     * 
     */
    public void deleteCdPessoaJuridicaContrato1()
    {
        this._has_cdPessoaJuridicaContrato1= false;
    } //-- void deleteCdPessoaJuridicaContrato1() 

    /**
     * Method deleteCdPessoaJuridicaContrato2
     * 
     */
    public void deleteCdPessoaJuridicaContrato2()
    {
        this._has_cdPessoaJuridicaContrato2= false;
    } //-- void deleteCdPessoaJuridicaContrato2() 

    /**
     * Method deleteCdProdutoOperacaoRelacionado
     * 
     */
    public void deleteCdProdutoOperacaoRelacionado()
    {
        this._has_cdProdutoOperacaoRelacionado= false;
    } //-- void deleteCdProdutoOperacaoRelacionado() 

    /**
     * Method deleteCdProdutoServicoOperacao
     * 
     */
    public void deleteCdProdutoServicoOperacao()
    {
        this._has_cdProdutoServicoOperacao= false;
    } //-- void deleteCdProdutoServicoOperacao() 

    /**
     * Method deleteCdTipoContratoNegocio1
     * 
     */
    public void deleteCdTipoContratoNegocio1()
    {
        this._has_cdTipoContratoNegocio1= false;
    } //-- void deleteCdTipoContratoNegocio1() 

    /**
     * Method deleteCdTipoContratoNegocio2
     * 
     */
    public void deleteCdTipoContratoNegocio2()
    {
        this._has_cdTipoContratoNegocio2= false;
    } //-- void deleteCdTipoContratoNegocio2() 

    /**
     * Method deleteCdTipoSelecao
     * 
     */
    public void deleteCdTipoSelecao()
    {
        this._has_cdTipoSelecao= false;
    } //-- void deleteCdTipoSelecao() 

    /**
     * Method deleteNrSequenciaContratoNegocio1
     * 
     */
    public void deleteNrSequenciaContratoNegocio1()
    {
        this._has_nrSequenciaContratoNegocio1= false;
    } //-- void deleteNrSequenciaContratoNegocio1() 

    /**
     * Method deleteNrSequenciaContratoNegocio2
     * 
     */
    public void deleteNrSequenciaContratoNegocio2()
    {
        this._has_nrSequenciaContratoNegocio2= false;
    } //-- void deleteNrSequenciaContratoNegocio2() 

    /**
     * Returns the value of field 'cdPessoaJuridicaContrato1'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridicaContrato1'.
     */
    public long getCdPessoaJuridicaContrato1()
    {
        return this._cdPessoaJuridicaContrato1;
    } //-- long getCdPessoaJuridicaContrato1() 

    /**
     * Returns the value of field 'cdPessoaJuridicaContrato2'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridicaContrato2'.
     */
    public long getCdPessoaJuridicaContrato2()
    {
        return this._cdPessoaJuridicaContrato2;
    } //-- long getCdPessoaJuridicaContrato2() 

    /**
     * Returns the value of field 'cdProdutoOperacaoRelacionado'.
     * 
     * @return int
     * @return the value of field 'cdProdutoOperacaoRelacionado'.
     */
    public int getCdProdutoOperacaoRelacionado()
    {
        return this._cdProdutoOperacaoRelacionado;
    } //-- int getCdProdutoOperacaoRelacionado() 

    /**
     * Returns the value of field 'cdProdutoServicoOperacao'.
     * 
     * @return int
     * @return the value of field 'cdProdutoServicoOperacao'.
     */
    public int getCdProdutoServicoOperacao()
    {
        return this._cdProdutoServicoOperacao;
    } //-- int getCdProdutoServicoOperacao() 

    /**
     * Returns the value of field 'cdTipoContratoNegocio1'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoNegocio1'.
     */
    public int getCdTipoContratoNegocio1()
    {
        return this._cdTipoContratoNegocio1;
    } //-- int getCdTipoContratoNegocio1() 

    /**
     * Returns the value of field 'cdTipoContratoNegocio2'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoNegocio2'.
     */
    public int getCdTipoContratoNegocio2()
    {
        return this._cdTipoContratoNegocio2;
    } //-- int getCdTipoContratoNegocio2() 

    /**
     * Returns the value of field 'cdTipoSelecao'.
     * 
     * @return int
     * @return the value of field 'cdTipoSelecao'.
     */
    public int getCdTipoSelecao()
    {
        return this._cdTipoSelecao;
    } //-- int getCdTipoSelecao() 

    /**
     * Returns the value of field 'nrSequenciaContratoNegocio1'.
     * 
     * @return long
     * @return the value of field 'nrSequenciaContratoNegocio1'.
     */
    public long getNrSequenciaContratoNegocio1()
    {
        return this._nrSequenciaContratoNegocio1;
    } //-- long getNrSequenciaContratoNegocio1() 

    /**
     * Returns the value of field 'nrSequenciaContratoNegocio2'.
     * 
     * @return long
     * @return the value of field 'nrSequenciaContratoNegocio2'.
     */
    public long getNrSequenciaContratoNegocio2()
    {
        return this._nrSequenciaContratoNegocio2;
    } //-- long getNrSequenciaContratoNegocio2() 

    /**
     * Method hasCdPessoaJuridicaContrato1
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridicaContrato1()
    {
        return this._has_cdPessoaJuridicaContrato1;
    } //-- boolean hasCdPessoaJuridicaContrato1() 

    /**
     * Method hasCdPessoaJuridicaContrato2
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridicaContrato2()
    {
        return this._has_cdPessoaJuridicaContrato2;
    } //-- boolean hasCdPessoaJuridicaContrato2() 

    /**
     * Method hasCdProdutoOperacaoRelacionado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdutoOperacaoRelacionado()
    {
        return this._has_cdProdutoOperacaoRelacionado;
    } //-- boolean hasCdProdutoOperacaoRelacionado() 

    /**
     * Method hasCdProdutoServicoOperacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdutoServicoOperacao()
    {
        return this._has_cdProdutoServicoOperacao;
    } //-- boolean hasCdProdutoServicoOperacao() 

    /**
     * Method hasCdTipoContratoNegocio1
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoNegocio1()
    {
        return this._has_cdTipoContratoNegocio1;
    } //-- boolean hasCdTipoContratoNegocio1() 

    /**
     * Method hasCdTipoContratoNegocio2
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoNegocio2()
    {
        return this._has_cdTipoContratoNegocio2;
    } //-- boolean hasCdTipoContratoNegocio2() 

    /**
     * Method hasCdTipoSelecao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoSelecao()
    {
        return this._has_cdTipoSelecao;
    } //-- boolean hasCdTipoSelecao() 

    /**
     * Method hasNrSequenciaContratoNegocio1
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaContratoNegocio1()
    {
        return this._has_nrSequenciaContratoNegocio1;
    } //-- boolean hasNrSequenciaContratoNegocio1() 

    /**
     * Method hasNrSequenciaContratoNegocio2
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaContratoNegocio2()
    {
        return this._has_nrSequenciaContratoNegocio2;
    } //-- boolean hasNrSequenciaContratoNegocio2() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdPessoaJuridicaContrato1'.
     * 
     * @param cdPessoaJuridicaContrato1 the value of field
     * 'cdPessoaJuridicaContrato1'.
     */
    public void setCdPessoaJuridicaContrato1(long cdPessoaJuridicaContrato1)
    {
        this._cdPessoaJuridicaContrato1 = cdPessoaJuridicaContrato1;
        this._has_cdPessoaJuridicaContrato1 = true;
    } //-- void setCdPessoaJuridicaContrato1(long) 

    /**
     * Sets the value of field 'cdPessoaJuridicaContrato2'.
     * 
     * @param cdPessoaJuridicaContrato2 the value of field
     * 'cdPessoaJuridicaContrato2'.
     */
    public void setCdPessoaJuridicaContrato2(long cdPessoaJuridicaContrato2)
    {
        this._cdPessoaJuridicaContrato2 = cdPessoaJuridicaContrato2;
        this._has_cdPessoaJuridicaContrato2 = true;
    } //-- void setCdPessoaJuridicaContrato2(long) 

    /**
     * Sets the value of field 'cdProdutoOperacaoRelacionado'.
     * 
     * @param cdProdutoOperacaoRelacionado the value of field
     * 'cdProdutoOperacaoRelacionado'.
     */
    public void setCdProdutoOperacaoRelacionado(int cdProdutoOperacaoRelacionado)
    {
        this._cdProdutoOperacaoRelacionado = cdProdutoOperacaoRelacionado;
        this._has_cdProdutoOperacaoRelacionado = true;
    } //-- void setCdProdutoOperacaoRelacionado(int) 

    /**
     * Sets the value of field 'cdProdutoServicoOperacao'.
     * 
     * @param cdProdutoServicoOperacao the value of field
     * 'cdProdutoServicoOperacao'.
     */
    public void setCdProdutoServicoOperacao(int cdProdutoServicoOperacao)
    {
        this._cdProdutoServicoOperacao = cdProdutoServicoOperacao;
        this._has_cdProdutoServicoOperacao = true;
    } //-- void setCdProdutoServicoOperacao(int) 

    /**
     * Sets the value of field 'cdTipoContratoNegocio1'.
     * 
     * @param cdTipoContratoNegocio1 the value of field
     * 'cdTipoContratoNegocio1'.
     */
    public void setCdTipoContratoNegocio1(int cdTipoContratoNegocio1)
    {
        this._cdTipoContratoNegocio1 = cdTipoContratoNegocio1;
        this._has_cdTipoContratoNegocio1 = true;
    } //-- void setCdTipoContratoNegocio1(int) 

    /**
     * Sets the value of field 'cdTipoContratoNegocio2'.
     * 
     * @param cdTipoContratoNegocio2 the value of field
     * 'cdTipoContratoNegocio2'.
     */
    public void setCdTipoContratoNegocio2(int cdTipoContratoNegocio2)
    {
        this._cdTipoContratoNegocio2 = cdTipoContratoNegocio2;
        this._has_cdTipoContratoNegocio2 = true;
    } //-- void setCdTipoContratoNegocio2(int) 

    /**
     * Sets the value of field 'cdTipoSelecao'.
     * 
     * @param cdTipoSelecao the value of field 'cdTipoSelecao'.
     */
    public void setCdTipoSelecao(int cdTipoSelecao)
    {
        this._cdTipoSelecao = cdTipoSelecao;
        this._has_cdTipoSelecao = true;
    } //-- void setCdTipoSelecao(int) 

    /**
     * Sets the value of field 'nrSequenciaContratoNegocio1'.
     * 
     * @param nrSequenciaContratoNegocio1 the value of field
     * 'nrSequenciaContratoNegocio1'.
     */
    public void setNrSequenciaContratoNegocio1(long nrSequenciaContratoNegocio1)
    {
        this._nrSequenciaContratoNegocio1 = nrSequenciaContratoNegocio1;
        this._has_nrSequenciaContratoNegocio1 = true;
    } //-- void setNrSequenciaContratoNegocio1(long) 

    /**
     * Sets the value of field 'nrSequenciaContratoNegocio2'.
     * 
     * @param nrSequenciaContratoNegocio2 the value of field
     * 'nrSequenciaContratoNegocio2'.
     */
    public void setNrSequenciaContratoNegocio2(long nrSequenciaContratoNegocio2)
    {
        this._nrSequenciaContratoNegocio2 = nrSequenciaContratoNegocio2;
        this._has_nrSequenciaContratoNegocio2 = true;
    } //-- void setNrSequenciaContratoNegocio2(long) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.incluirequiparacaocontrato.request.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.incluirequiparacaocontrato.request.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.incluirequiparacaocontrato.request.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.incluirequiparacaocontrato.request.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
