/*
 * Nome: br.com.bradesco.web.pgit.service.report.contrato.data
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 05/02/2016
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.report.mantercontrato.contrato.data;

// TODO: Auto-generated Javadoc
/**
 * Nome: AnexoFornecedores
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class AnexoFornecedores {

    /** Atributo aplicacaoFloatingSim. */
    private String aplicacaoFloatingSim = null;

    /** Atributo aplicacaoFloatingNao. */
    private String aplicacaoFloatingNao = null;
    
    /** Atributo tipoAplicacao. */
    private String tipoAplicacao = null;
    
    /** Atributo hrLimiteSuperiorBoleto. */
    private String hrLimiteSuperiorBoleto = null;
    
    /** Atributo hrLimiteInferiorBoleto. */
    private String hrLimiteInferiorBoleto = null;
    
    /** The hr limite pagamento superior. */
    private String hrLimitePagamentoSuperior = null;
    
    /** The hr limite pagamento inferior. */
    private String hrLimitePagamentoInferior = null;
    
    /** The nr montante boleto. */
    private String nrMontanteBoleto = null; 

    /** Atributo modalidadeCreditoConta. */
    private ModalidadeCondicao modalidadeCreditoConta = null;

    /** Atributo modalidadeTed. */
    private ModalidadeCondicao modalidadeTed = null;

    /** Atributo modalidadeDoc. */
    private ModalidadeCondicao modalidadeDoc = null;

    /** Atributo modalidadeBoletosBradesco. */
    private ModalidadeCondicao modalidadeBoletosBradesco = null;

    /** Atributo modalidadeBoletosOutros. */
    private ModalidadeCondicao modalidadeBoletosOutros = null;

    /** Atributo modalidadeOrdemPagamento. */
    private ModalidadeCondicao modalidadeOrdemPagamento = null;

    /** Atributo modalidadeRastreamentoBoletos. */
    private ModalidadeCondicao modalidadeRastreamentoBoletos = null;

    /**
     * Instancia um novo AnexoFornecedores.
     *
     * @see
     */
    public AnexoFornecedores() {
        super();
    }

    /**
     * Nome: getAplicacaoFloatingSim.
     *
     * @return aplicacaoFloatingSim
     */
    public String getAplicacaoFloatingSim() {
        return aplicacaoFloatingSim;
    }

    /**
     * Nome: setAplicacaoFloatingSim.
     *
     * @param aplicacaoFloatingSim the aplicacao floating sim
     */
    public void setAplicacaoFloatingSim(String aplicacaoFloatingSim) {
        this.aplicacaoFloatingSim = aplicacaoFloatingSim;
    }

    /**
     * Nome: getAplicacaoFloatingNao.
     *
     * @return aplicacaoFloatingNao
     */
    public String getAplicacaoFloatingNao() {
        return aplicacaoFloatingNao;
    }

    /**
     * Nome: setAplicacaoFloatingNao.
     *
     * @param aplicacaoFloatingNao the aplicacao floating nao
     */
    public void setAplicacaoFloatingNao(String aplicacaoFloatingNao) {
        this.aplicacaoFloatingNao = aplicacaoFloatingNao;
    }

    /**
     * Nome: getModalidadeCreditoConta.
     *
     * @return modalidadeCreditoConta
     */
    public ModalidadeCondicao getModalidadeCreditoConta() {
        return modalidadeCreditoConta;
    }

    /**
     * Nome: setModalidadeCreditoConta.
     *
     * @param modalidadeCreditoConta the modalidade credito conta
     */
    public void setModalidadeCreditoConta(ModalidadeCondicao modalidadeCreditoConta) {
        this.modalidadeCreditoConta = modalidadeCreditoConta;
    }

    /**
     * Nome: getModalidadeTed.
     *
     * @return modalidadeTed
     */
    public ModalidadeCondicao getModalidadeTed() {
        return modalidadeTed;
    }

    /**
     * Nome: setModalidadeTed.
     *
     * @param modalidadeTed the modalidade ted
     */
    public void setModalidadeTed(ModalidadeCondicao modalidadeTed) {
        this.modalidadeTed = modalidadeTed;
    }

    /**
     * Nome: getModalidadeDoc.
     *
     * @return modalidadeDoc
     */
    public ModalidadeCondicao getModalidadeDoc() {
        return modalidadeDoc;
    }

    /**
     * Nome: setModalidadeDoc.
     *
     * @param modalidadeDoc the modalidade doc
     */
    public void setModalidadeDoc(ModalidadeCondicao modalidadeDoc) {
        this.modalidadeDoc = modalidadeDoc;
    }

    /**
     * Nome: getModalidadeBoletosBradesco.
     *
     * @return modalidadeBoletosBradesco
     */
    public ModalidadeCondicao getModalidadeBoletosBradesco() {
        return modalidadeBoletosBradesco;
    }

    /**
     * Nome: setModalidadeBoletosBradesco.
     *
     * @param modalidadeBoletosBradesco the modalidade boletos bradesco
     */
    public void setModalidadeBoletosBradesco(ModalidadeCondicao modalidadeBoletosBradesco) {
        this.modalidadeBoletosBradesco = modalidadeBoletosBradesco;
    }

    /**
     * Nome: getModalidadeBoletosOutros.
     *
     * @return modalidadeBoletosOutros
     */
    public ModalidadeCondicao getModalidadeBoletosOutros() {
        return modalidadeBoletosOutros;
    }

    /**
     * Nome: setModalidadeBoletosOutros.
     *
     * @param modalidadeBoletosOutros the modalidade boletos outros
     */
    public void setModalidadeBoletosOutros(ModalidadeCondicao modalidadeBoletosOutros) {
        this.modalidadeBoletosOutros = modalidadeBoletosOutros;
    }

    /**
     * Nome: getModalidadeOrdemPagamento.
     *
     * @return modalidadeOrdemPagamento
     */
    public ModalidadeCondicao getModalidadeOrdemPagamento() {
        return modalidadeOrdemPagamento;
    }

    /**
     * Nome: setModalidadeOrdemPagamento.
     *
     * @param modalidadeOrdemPagamento the modalidade ordem pagamento
     */
    public void setModalidadeOrdemPagamento(ModalidadeCondicao modalidadeOrdemPagamento) {
        this.modalidadeOrdemPagamento = modalidadeOrdemPagamento;
    }

    /**
     * Nome: getModalidadeRastreamentoBoletos.
     *
     * @return modalidadeRastreamentoBoletos
     */
    public ModalidadeCondicao getModalidadeRastreamentoBoletos() {
        return modalidadeRastreamentoBoletos;
    }

    /**
     * Nome: setModalidadeRastreamentoBoletos.
     *
     * @param modalidadeRastreamentoBoletos the modalidade rastreamento boletos
     */
    public void setModalidadeRastreamentoBoletos(ModalidadeCondicao modalidadeRastreamentoBoletos) {
        this.modalidadeRastreamentoBoletos = modalidadeRastreamentoBoletos;
    }

	/**
	 * Sets the tipo aplicacao.
	 *
	 * @param tipoAplicacao the tipoAplicacao to set
	 */
	public void setTipoAplicacao(String tipoAplicacao) {
		this.tipoAplicacao = tipoAplicacao;
	}

	/**
	 * Gets the tipo aplicacao.
	 *
	 * @return the tipoAplicacao
	 */
	public String getTipoAplicacao() {
		return tipoAplicacao;
	}

	/**
	 * Gets the hr limite pagamento superior.
	 *
	 * @return the hr limite pagamento superior
	 */
	public String getHrLimitePagamentoSuperior() {
		return hrLimitePagamentoSuperior;
	}

	/**
	 * Sets the hr limite pagamento superior.
	 *
	 * @param hrLimitePagamentoSuperior the new hr limite pagamento superior
	 */
	public void setHrLimitePagamentoSuperior(String hrLimitePagamentoSuperior) {
		this.hrLimitePagamentoSuperior = hrLimitePagamentoSuperior;
	}

	/**
	 * Gets the hr limite pagamento inferior.
	 *
	 * @return the hr limite pagamento inferior
	 */
	public String getHrLimitePagamentoInferior() {
		return hrLimitePagamentoInferior;
	}

	/**
	 * Sets the hr limite pagamento inferior.
	 *
	 * @param hrLimitePagamentoInferior the new hr limite pagamento inferior
	 */
	public void setHrLimitePagamentoInferior(String hrLimitePagamentoInferior) {
		this.hrLimitePagamentoInferior = hrLimitePagamentoInferior;
	}

	/**
	 * Gets the nr montante boleto.
	 *
	 * @return the nr montante boleto
	 */
	public String getNrMontanteBoleto() {
		return nrMontanteBoleto;
	}

	/**
	 * Sets the nr montante boleto.
	 *
	 * @param nrMontanteBoleto the new nr montante boleto
	 */
	public void setNrMontanteBoleto(String nrMontanteBoleto) {
		this.nrMontanteBoleto = nrMontanteBoleto;
	}

    /**
     * Nome: setHrLimiteSuperiorBoleto.
     *
     * @param hrLimiteSuperiorBoleto the hr limite superior boleto
     */
    public void setHrLimiteSuperiorBoleto(String hrLimiteSuperiorBoleto) {
        this.hrLimiteSuperiorBoleto = hrLimiteSuperiorBoleto;
    }

    /**
     * Nome: getHrLimiteSuperiorBoleto.
     *
     * @return hrLimiteSuperiorBoleto
     */
    public String getHrLimiteSuperiorBoleto() {
        return hrLimiteSuperiorBoleto;
    }

    /**
     * Nome: sethrLimiteInferiorBoleto.
     *
     * @param hrLimiteInferiorBoleto the hr limite inferno boleto
     */
    public void sethrLimiteInferiorBoleto(String hrLimiteInferiorBoleto) {
        this.hrLimiteInferiorBoleto = hrLimiteInferiorBoleto;
    }

    /**
     * Nome: gethrLimiteInferiorBoleto.
     *
     * @return hrLimiteInferiorBoleto
     */
    public String gethrLimiteInferiorBoleto() {
        return hrLimiteInferiorBoleto;
    }
}
