/**
 * Nome: br.com.bradesco.web.pgit.service.business.mantersolicitacaoexclusaolotepagamentosservice.impl
 * Compilador: JDK 1.5
 * Prop�sito: INSERIR O PROP�SITO DAS CLASSES DO PACOTE
 * Data da cria��o: <dd/MM/yyyy>
 * Par�metros de compila��o: -d
 */
package br.com.bradesco.web.pgit.service.business.mantersolicitacaoexclusaolotepagamentosservice.impl;

import static br.com.bradesco.web.pgit.utils.PgitUtil.verificaBigDecimalNulo;
import static br.com.bradesco.web.pgit.utils.PgitUtil.verificaIntegerNulo;
import static br.com.bradesco.web.pgit.utils.PgitUtil.verificaLongNulo;
import static br.com.bradesco.web.pgit.utils.PgitUtil.verificaStringNula;
import static br.com.bradesco.web.pgit.view.converters.FormatarData.formataData;
import static br.com.bradesco.web.pgit.view.converters.FormatarData.formataDiaMesAnoToPdc;
import static br.com.bradesco.web.pgit.view.converters.FormatarData.formataTimestampFromPdc;

import java.util.ArrayList;

import org.apache.commons.lang.StringUtils;

import br.com.bradesco.web.pgit.service.business.lotepagamentos.bean.ConsultarLotePagamentosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.lotepagamentos.bean.ConsultarLotePagamentosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.lotepagamentos.bean.DetalharLotePagamentosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.lotepagamentos.bean.DetalharLotePagamentosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.lotepagamentos.bean.ExcluirLotePagamentosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.lotepagamentos.bean.ExcluirLotePagamentosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.lotepagamentos.bean.IncluirLotePagamentosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.lotepagamentos.bean.IncluirLotePagamentosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.lotepagamentos.bean.OcorrenciasLotePagamentosDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicitacaoexclusaolotepagamentosservice.
IManterSolicitacaoExclusaoLotePagamentosService;
import br.com.bradesco.web.pgit.service.business.mantersolicitacaoexclusaolotepagamentosservice.
bean.ConsultarOcorrenciasSolicLoteEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicitacaoexclusaolotepagamentosservice.
bean.ConsultarOcorrenciasSolicLoteSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicitacaoexclusaolotepagamentosservice.
bean.ConsultarQtdeValorPgtoPrevistoSolicEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicitacaoexclusaolotepagamentosservice.
bean.ConsultarQtdeValorPgtoPrevistoSolicSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicitacaoexclusaolotepagamentosservice.
bean.OcorrenciasSolicLoteDTO;
import br.com.bradesco.web.pgit.service.data.pdc.FactoryAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarocorrenciassoliclote.
request.ConsultarOcorrenciasSolicLoteRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarocorrenciassoliclote.
response.ConsultarOcorrenciasSolicLoteResponse;
import br.com.bradesco.web.pgit.service.data.pdc.consultarqtdevalorpgtoprevistosolic.
request.ConsultarQtdeValorPgtoPrevistoSolicRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarqtdevalorpgtoprevistosolic.
response.ConsultarQtdeValorPgtoPrevistoSolicResponse;
import br.com.bradesco.web.pgit.service.data.pdc.consultarsolicexclusaolotepgto.
request.ConsultarSolicExclusaoLotePgtoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarsolicexclusaolotepgto.
response.ConsultarSolicExclusaoLotePgtoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.detalharsolicexclusaolotepgto.
request.DetalharSolicExclusaoLotePgtoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.detalharsolicexclusaolotepgto.
response.DetalharSolicExclusaoLotePgtoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.excluirsolicexclusaolotepgto.
request.ExcluirSolicExclusaoLotePgtoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.excluirsolicexclusaolotepgto.
response.ExcluirSolicExclusaoLotePgtoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.incluirsolicexclusaolotepgto.
request.IncluirSolicExclusaoLotePgtoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.incluirsolicexclusaolotepgto.
response.IncluirSolicExclusaoLotePgtoResponse;
import br.com.bradesco.web.pgit.utils.PgitUtil;

/**
 * Nome: ManterSolicitacaoExclusaoLotePagamentosServiceServiceImpl
 * <p>
 * Prop�sito: Implementa��o do adaptador ManterSolicitacaoExclusaoLotePagamentosService
 * </p>
 * 
 * @author CPM Braxis / TI Melhorias - Arquitetura
 * @version 1.0
 * @see IManterSolicitacaoExclusaoLotePagamentosService
 */
public class ManterSolicitacaoExclusaoLotePagamentosServiceImpl implements
    IManterSolicitacaoExclusaoLotePagamentosService {

    /**
     * Atributo int
     */
    private static final int QTDE_MAXIMA_OCORRENCIAS = 40;
    /**
     * Atributo FactoryAdapter
     */
    private FactoryAdapter factoryAdapter = null;

    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.mantersolicitacaoexclusaolotepagamentosservice.
     * IManterSolicitacaoExclusaoLotePagamentosService#consultarSolicitacaoExclusaoLotePagamentos(br.com.bradesco.
     * web.pgit.service.business.lotepagamentos.bean.ConsultarLotePagamentosEntradaDTO)
     * @param entrada
     */
    public ConsultarLotePagamentosSaidaDTO consultarSolicitacaoExclusaoLotePagamentos(
        ConsultarLotePagamentosEntradaDTO entrada) {

        ConsultarLotePagamentosSaidaDTO saida = new ConsultarLotePagamentosSaidaDTO();
        ConsultarSolicExclusaoLotePgtoRequest request = new ConsultarSolicExclusaoLotePgtoRequest();
        ConsultarSolicExclusaoLotePgtoResponse response = null;

        request.setNrOcorrencias(QTDE_MAXIMA_OCORRENCIAS);
        request.setCdpessoaJuridicaContrato(verificaLongNulo(entrada.getCdpessoaJuridicaContrato()));
        request.setCdTipoContratoNegocio(verificaIntegerNulo(entrada.getCdTipoContratoNegocio()));
        request.setNrSequenciaContratoNegocio(verificaLongNulo(entrada.getNrSequenciaContratoNegocio()));
        request.setNrLoteInterno(verificaLongNulo(entrada.getNrLoteInterno()));
        request.setCdTipoLayoutArquivo(verificaIntegerNulo(entrada.getCdTipoLayoutArquivo()));
        request.setDtPgtoInicial(verificaStringNula(entrada.getDtPgtoInicial()));
        request.setDtPgtoFinal(verificaStringNula(entrada.getDtPgtoFinal()));
        request.setCdSituacaoSolicitacaoPagamento(verificaIntegerNulo(entrada.getCdSituacaoSolicitacaoPagamento()));
        request.setCdMotivoSolicitacao(verificaIntegerNulo(entrada.getCdMotivoSolicitacao()));

        response = getFactoryAdapter().getConsultarSolicExclusaoLotePgtoPDCAdapter().invokeProcess(request);

        saida.setCodMensagem(response.getCodMensagem());
        saida.setMensagem(response.getMensagem());
        saida.setNumeroLinhas(response.getNumeroLinhas());

        saida.setOcorrencias(new ArrayList<OcorrenciasLotePagamentosDTO>());

        for (int i = 0; i < response.getOcorrenciasCount(); i++) {
            OcorrenciasLotePagamentosDTO ocorrencia = new OcorrenciasLotePagamentosDTO();

            ocorrencia.setCdSolicitacaoPagamentoIntegrado(response.getOcorrencias(i)
                .getCdSolicitacaoPagamentoIntegrado());
            ocorrencia.setNrSolicitacaoPagamentoIntegrado(response.getOcorrencias(i)
                .getNrSolicitacaoPagamentoIntegrado());
            ocorrencia.setCdpessoaJuridicaContrato(response.getOcorrencias(i).getCdPessoaJuridicaContrato());
            ocorrencia.setCdTipoContratoNegocio(response.getOcorrencias(i).getCdTipoContratoNegocio());
            ocorrencia.setNrSequenciaContratoNegocio(response.getOcorrencias(i).getNrSequenciaContratoNegocio());
            ocorrencia.setNrLoteInterno(response.getOcorrencias(i).getNrLoteInterno());
            ocorrencia.setCdTipoLayoutArquivo(response.getOcorrencias(i).getCdTipoLayoutArquivo());
            ocorrencia.setDsTipoLayoutArquivo(response.getOcorrencias(i).getDsTipoLayoutArquivo());
            ocorrencia
                .setCdSituacaoSolicitacaoPagamento(response.getOcorrencias(i).getCdSituacaoSolicitacaoPagamento());
            ocorrencia.setDsSituacaoSolicitaoPgto(response.getOcorrencias(i).getDsSituacaoSolicitaoPgto());
            ocorrencia.setCdMotivoSolicitacao(response.getOcorrencias(i).getCdMotivoSolicitacao());
            ocorrencia.setDsMotivoSolicitacao(response.getOcorrencias(i).getDsMotivoSolicitacao());
            ocorrencia.setDsEmpresa(response.getOcorrencias(i).getDsEmpresa());
            ocorrencia.setDsSolicitacao(response.getOcorrencias(i).getDsSolicitacao());
            ocorrencia.setHrSolicitacao(response.getOcorrencias(i).getHrSolicitacao());
            ocorrencia.setDsProdutoServicoOperacao(response.getOcorrencias(i).getDsProdutoServicoOperacao());
            ocorrencia.setDataHoraFormatada(PgitUtil.concatenarCampos(response.getOcorrencias(i).getDsSolicitacao(),
                response.getOcorrencias(i).getHrSolicitacao()));

            saida.getOcorrencias().add(ocorrencia);
        }

        return saida;
    }

    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.mantersolicitacaoexclusaolotepagamentosservice.
     * IManterSolicitacaoExclusaoLotePagamentosService#detalharSolicitacaoExclusaoLotePagamentos(br.com.bradesco.web.
     * pgit.service.business.lotepagamentos.bean.DetalharLotePagamentosEntradaDTO)
     * @param entrada
     */
    public DetalharLotePagamentosSaidaDTO detalharSolicitacaoExclusaoLotePagamentos(
        DetalharLotePagamentosEntradaDTO entrada) {

        DetalharLotePagamentosSaidaDTO saida = new DetalharLotePagamentosSaidaDTO();
        DetalharSolicExclusaoLotePgtoRequest request = new DetalharSolicExclusaoLotePgtoRequest();
        DetalharSolicExclusaoLotePgtoResponse response = null;

        request.setCdSolicitacaoPagamentoIntegrado(PgitUtil.verificaIntegerNulo(entrada
            .getCdSolicitacaoPagamentoIntegrado()));
        request.setNrSolicitacaoPagamentoIntegrado(PgitUtil.verificaIntegerNulo(entrada
            .getNrSolicitacaoPagamentoIntegrado()));

        response = getFactoryAdapter().getDetalharSolicExclusaoLotePgtoPDCAdapter().invokeProcess(request);

        saida.setCodMensagem(response.getCodMensagem());
        saida.setMensagem(response.getMensagem());
        saida.setCdpessoaJuridicaContrato(response.getCdpessoaJuridicaContrato());
        saida.setCdTipoContratoNegocio(response.getCdTipoContratoNegocio());
        saida.setNrSequenciaContratoNegocio(response.getNrSequenciaContratoNegocio());
        saida.setCdSolicitacaoPagamentoIntegrado(response.getCdSolicitacaoPagamentoIntegrado());
        saida.setNrSolicitacaoPagamentoIntegrado(response.getNrSolicitacaoPagamentoIntegrado());
        saida.setDsSituacaoSolicitaoPgto(response.getDsSituacaoSolicitaoPgto());
        saida.setDsMotivoSolicitacao(response.getDsMotivoSolicitacao());
        saida.setDsSolicitacao(response.getDsSolicitacao());
        saida.setHrSolicitacao(response.getHrSolicitacao());
        saida.setFormatDataHora(response.getDsSolicitacao() + response.getHrSolicitacao());
        if(saida.getDsSituacaoSolicitaoPgto().toUpperCase().equals("PENDENTE")){
        	saida.setFormatDataHora(null);        	
        }    
        if(StringUtils.isNotEmpty(saida.getFormatDataHora())){
        	saida.setFormatDataHora(String.format("%s - %s", response.getDsSolicitacao(), response.getHrSolicitacao()));	
        }
        
        saida.setNrLoteInterno(response.getNrLoteInterno());
        saida.setDsTipoLayoutArquivo(response.getDsTipoLayoutArquivo());
        saida.setDtPgtoInicial(response.getDtPgtoInicial());
        saida.setDtPgtoFinal(response.getDtPgtoFinal());
        saida.setCdBancoDebito(response.getCdBancoDebito());
        saida.setCdAgenciaDebito(response.getCdAgenciaDebito());
        saida.setCdContaDebito(response.getCdContaDebito());
        saida.setDsProdutoServicoOperacao(response.getDsProdutoServicoOperacao());
        saida.setDsModalidadePgtoCliente(response.getDsModalidadePgtoCliente());

        saida.setQtdeTotalPagtoPrevistoSoltc(response.getQtdeTotalPagtoPrevistoSoltc());
        saida.setVlrTotPagtoPrevistoSolicitacao(response.getVlrTotPagtoPrevistoSolicitacao());

        saida.setQtTotalPgtoEfetivadoSolicitacao(response.getQtTotalPgtoEfetivadoSolicitacao());
        saida.setVlTotalPgtoEfetuadoSolicitacao(response.getVlTotalPgtoEfetuadoSolicitacao());
        saida.setCdCanalInclusao(response.getCdCanalInclusao());
        saida.setDsCanalInclusao(response.getDsCanalInclusao());
        saida.setCdAutenticacaoSegurancaInclusao(response.getCdAutenticacaoSegurancaInclusao());
        saida.setHrInclusaoRegistro(formataTimestampFromPdc(formataData(response.getHrInclusaoRegistro(),
            "yyyy-MM-dd-HH.mm.ss")));
        saida.setCdCanalManutencao(response.getCdCanalManutencao());
        saida.setDsCanalManutencao(response.getDsCanalManutencao());
        saida.setCdAutenticacaoSegurancaManutencao(response.getCdAutenticacaoSegurancaManutencao());
        saida.setNmOperacaoFluxoManutencao(response.getNmOperacaoFluxoManutencao());
        saida.setHrManutencaoRegistro(formataTimestampFromPdc(formataData(response.getHrManutencaoRegistro(),
            "yyyy-MM-dd-HH.mm.ss")));

        return saida;
    }

    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.mantersolicitacaoexclusaolotepagamentosservice.
     * IManterSolicitacaoExclusaoLotePagamentosService#consultarOcorrenciasSolicLote(br.com.bradesco.web.pgit.service
     * .business.mantersolicitacaoexclusaolotepagamentosservice.bean.ConsultarOcorrenciasSolicLoteEntradaDTO)
     * @param entrada
     */
    public ConsultarOcorrenciasSolicLoteSaidaDTO consultarOcorrenciasSolicLote(
        ConsultarOcorrenciasSolicLoteEntradaDTO entrada) {

        ConsultarOcorrenciasSolicLoteSaidaDTO saida = new ConsultarOcorrenciasSolicLoteSaidaDTO();
        ConsultarOcorrenciasSolicLoteRequest request = new ConsultarOcorrenciasSolicLoteRequest();
        ConsultarOcorrenciasSolicLoteResponse response = null;

        request.setNrOcorrencias(QTDE_MAXIMA_OCORRENCIAS);
        request.setCdSolicitacaoPagamentoIntegrado(entrada.getCdSolicitacaoPagamentoIntegrado());
        request.setNrSolicitacaoPagamentoIntegrado(entrada.getNrSolicitacaoPagamentoIntegrado());

        response = getFactoryAdapter().getConsultarOcorrenciasSolicLotePDCAdapter().invokeProcess(request);

        saida.setCodMensagem(response.getCodMensagem());
        saida.setMensagem(response.getMensagem());
        saida.setNumeroLinhas(response.getNumeroLinhas());
        saida.setOcorrencias(new ArrayList<OcorrenciasSolicLoteDTO>());

        for (int i = 0; i < response.getOcorrenciasCount(); i++) {
            OcorrenciasSolicLoteDTO ocorrencia = new OcorrenciasSolicLoteDTO();

            ocorrencia.setCdCpfCnpjPagador(response.getOcorrencias(i).getCdCpfCnpjPagador());
            ocorrencia.setCdFilialCpfCnpjPagador(response.getOcorrencias(i).getCdFilialCpfCnpjPagador());
            ocorrencia.setCdTipoCtaRecebedor(response.getOcorrencias(i).getCdTipoCtaRecebedor());
            ocorrencia.setDsRazaoSocial(response.getOcorrencias(i).getDsRazaoSocial());
            ocorrencia.setDsTipoServico(response.getOcorrencias(i).getDsTipoServico());
            ocorrencia.setDsModalidade(response.getOcorrencias(i).getDsModalidade());
            ocorrencia.setCdControlePagamento(response.getOcorrencias(i).getCdControlePagamento());
            ocorrencia.setDsPagto(response.getOcorrencias(i).getDsPagto());
            ocorrencia.setVlPagto(response.getOcorrencias(i).getVlPagto());
            ocorrencia.setDsRazaoSocialFav(response.getOcorrencias(i).getDsRazaoSocialFav());
            ocorrencia.setCdBancoDebito(response.getOcorrencias(i).getCdBancoDebito());
            ocorrencia.setCdAgenciaDebito(response.getOcorrencias(i).getCdAgenciaDebito());
            ocorrencia.setCdContaDebito(response.getOcorrencias(i).getCdContaDebito());
            ocorrencia.setCdBcoRecebedor(response.getOcorrencias(i).getCdBcoRecebedor());
            ocorrencia.setCdAgenciaRecebedor(response.getOcorrencias(i).getCdAgenciaRecebedor());
            ocorrencia.setCdContaRecebedor(response.getOcorrencias(i).getCdContaRecebedor());
            ocorrencia.setDsMotivoErro(response.getOcorrencias(i).getDsMotivoErro());

            saida.getOcorrencias().add(ocorrencia);
        }

        return saida;
    }

    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.mantersolicitacaoexclusaolotepagamentosservice.
     * IManterSolicitacaoExclusaoLotePagamentosService#excluirSolicitacaoExclusaoLotePagamentos(br.com.bradesco.
     * web.pgit.service.business.lotepagamentos.bean.ExcluirLotePagamentosEntradaDTO)
     * @param entrada
     */
    public ExcluirLotePagamentosSaidaDTO excluirSolicitacaoExclusaoLotePagamentos(
        ExcluirLotePagamentosEntradaDTO entrada) {

        ExcluirLotePagamentosSaidaDTO saida = new ExcluirLotePagamentosSaidaDTO();
        ExcluirSolicExclusaoLotePgtoRequest request = new ExcluirSolicExclusaoLotePgtoRequest();
        ExcluirSolicExclusaoLotePgtoResponse response = null;

        request.setCdSolicitacaoPagamentoIntegrado(entrada.getCdSolicitacaoPagamentoIntegrado());
        request.setNrSolicitacaoPagamentoIntegrado(entrada.getNrSolicitacaoPagamentoIntegrado());

        response = getFactoryAdapter().getExcluirSolicExclusaoLotePgtoPDCAdapter().invokeProcess(request);

        saida.setCodMensagem(response.getCodMensagem());
        saida.setMensagem(response.getMensagem());

        return saida;
    }

    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.mantersolicitacaoexclusaolotepagamentosservice.
     * IManterSolicitacaoExclusaoLotePagamentosService#incluirSolicitacaoExclusaoLotePagamentos(br.com.bradesco.web.
     * pgit.service.business.lotepagamentos.bean.IncluirLotePagamentosEntradaDTO)
     * @param entrada
     */
    public IncluirLotePagamentosSaidaDTO incluirSolicitacaoExclusaoLotePagamentos(
        IncluirLotePagamentosEntradaDTO entrada) {

        IncluirLotePagamentosSaidaDTO saida = new IncluirLotePagamentosSaidaDTO();
        IncluirSolicExclusaoLotePgtoRequest request = new IncluirSolicExclusaoLotePgtoRequest();
        IncluirSolicExclusaoLotePgtoResponse response = null;

        request.setCdpessoaJuridicaContrato(verificaLongNulo(entrada.getCdpessoaJuridicaContrato()));
        request.setCdTipoContratoNegocio(verificaIntegerNulo(entrada.getCdTipoContratoNegocio()));
        request.setNrSequenciaContratoNegocio(verificaLongNulo(entrada.getNrSequenciaContratoNegocio()));
        request.setNrLoteInterno(verificaLongNulo(entrada.getNrLoteInterno()));
        request.setCdTipoLayoutArquivo(verificaIntegerNulo(entrada.getCdTipoLayoutArquivo()));
        request.setDtPgtoInicial(verificaStringNula(entrada.getDtPgtoInicial()));
        request.setDtPgtoFinal(verificaStringNula(entrada.getDtPgtoFinal()));
        request.setCdProdutoServicoOperacao(verificaIntegerNulo(entrada.getCdProdutoServicoOperacao()));
        request.setCdProdutoServicoRelacionado(verificaIntegerNulo(entrada.getCdProdutoServicoRelacionado()));
        request.setCdBancoDebito(verificaIntegerNulo(entrada.getCdBancoDebito()));
        request.setCdAgenciaDebito(verificaIntegerNulo(entrada.getCdAgenciaDebito()));
        request.setCdContaDebito(verificaLongNulo(entrada.getCdContaDebito()));
        request.setQtdeTotalPagtoPrevistoSoltc(verificaLongNulo(entrada.getQtdeTotalPagtoPrevistoSoltc()));
        request.setVlrTotPagtoPrevistoSolicitacao(verificaBigDecimalNulo(entrada.getVlrTotPagtoPrevistoSolicitacao()));
        request.setCdTipoInscricaoPagador(verificaIntegerNulo(entrada.getCdTipoInscricaoPagador()));
        request.setCdCpfCnpjPagador(verificaLongNulo(entrada.getCdCpfCnpjPagador()));
        request.setCdFilialCpfCnpjPagador(verificaIntegerNulo(entrada.getCdFilialCpfCnpjPagador()));
        request.setCdControleCpfCnpjPagador(verificaIntegerNulo(entrada.getCdControleCpfCnpjPagador()));
        request.setCdTipoCtaRecebedor(verificaIntegerNulo(entrada.getCdTipoCtaRecebedor()));
        request.setCdBcoRecebedor(verificaIntegerNulo(entrada.getCdBcoRecebedor()));
        request.setCdAgenciaRecebedor(verificaIntegerNulo(entrada.getCdAgenciaRecebedor()));
        request.setCdContaRecebedor(verificaLongNulo(entrada.getCdContaRecebedor()));
        request.setCdTipoInscricaoRecebedor(verificaIntegerNulo(entrada.getCdTipoInscricaoRecebedor()));
        request.setCdCpfCnpjRecebedor(PgitUtil.verificaLongNulo(entrada.getCdCpfCnpjRecebedor()));
        request.setCdFilialCnpjRecebedor(verificaIntegerNulo(entrada.getCdFilialCnpjRecebedor()));
        request.setCdControleCpfRecebedor(verificaIntegerNulo(entrada.getCdControleCpfRecebedor()));

        response = getFactoryAdapter().getIncluirSolicExclusaoLotePgtoPDCAdapter().invokeProcess(request);

        saida.setCodMensagem(response.getCodMensagem());
        saida.setMensagem(response.getMensagem());

        return saida;
    }

    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.mantersolicitacaoexclusaolotepagamentosservice.
     * IManterSolicitacaoExclusaoLotePagamentosService#consultarQtdeValorPgtoPrevistoSolic(br.com.bradesco.web.pgit.
     * service.business.mantersolicitacaoexclusaolotepagamentosservice.bean.
     * ConsultarQtdeValorPgtoPrevistoSolicEntradaDTO)
     * @param entrada
     */
    public ConsultarQtdeValorPgtoPrevistoSolicSaidaDTO consultarQtdeValorPgtoPrevistoSolic(
        ConsultarQtdeValorPgtoPrevistoSolicEntradaDTO entrada) {

        ConsultarQtdeValorPgtoPrevistoSolicSaidaDTO saida = new ConsultarQtdeValorPgtoPrevistoSolicSaidaDTO();
        ConsultarQtdeValorPgtoPrevistoSolicRequest request = new ConsultarQtdeValorPgtoPrevistoSolicRequest();
        ConsultarQtdeValorPgtoPrevistoSolicResponse response = null;

        request.setCdSolicitacaoPagamentoIntegrado(verificaIntegerNulo(entrada.getCdSolicitacaoPagamentoIntegrado()));
        request.setCdpessoaJuridicaContrato(verificaLongNulo(entrada.getCdpessoaJuridicaContrato()));
        request.setCdTipoContratoNegocio(verificaIntegerNulo(entrada.getCdTipoContratoNegocio()));
        request.setNrSequenciaContratoNegocio(verificaLongNulo(entrada.getNrSequenciaContratoNegocio()));
        request.setNrLoteInterno(verificaLongNulo(entrada.getNrLoteInterno()));
        request.setCdTipoLayoutArquivo(verificaIntegerNulo(entrada.getCdTipoLayoutArquivo()));
        request.setDtPgtoInicial(formataDiaMesAnoToPdc(entrada.getDtPgtoInicial()));
        request.setDtPgtoFinal(formataDiaMesAnoToPdc(entrada.getDtPgtoFinal()));
        request.setCdProdutoServicoOperacao(verificaIntegerNulo(entrada.getCdProdutoServicoOperacao()));
        request.setCdProdutoServicoRelacionado(verificaIntegerNulo(entrada.getCdProdutoServicoRelacionado()));
        request.setCdBancoDebito(verificaIntegerNulo(entrada.getCdBancoDebito()));
        request.setCdAgenciaDebito(verificaIntegerNulo(entrada.getCdAgenciaDebito()));
        request.setCdContaDebito(verificaLongNulo(entrada.getCdContaDebito()));
        request.setCdTipoInscricaoPagador(verificaIntegerNulo(entrada.getCdTipoInscricaoPagador()));
        request.setCdCpfCnpjPagador(verificaLongNulo(entrada.getCdCpfCnpjPagador()));
        request.setCdFilialCpfCnpjPagador(verificaIntegerNulo(entrada.getCdFilialCpfCnpjPagador()));
        request.setCdControleCpfCnpjPagador(verificaIntegerNulo(entrada.getCdControleCpfCnpjPagador()));
        request.setCdTipoCtaRecebedor(verificaIntegerNulo(entrada.getCdTipoCtaRecebedor()));
        request.setCdBcoRecebedor(verificaIntegerNulo(entrada.getCdBcoRecebedor()));
        request.setCdAgenciaRecebedor(verificaIntegerNulo(entrada.getCdAgenciaRecebedor()));
        request.setCdContaRecebedor(verificaLongNulo(entrada.getCdContaRecebedor()));
        request.setCdTipoInscricaoRecebedor(verificaIntegerNulo(entrada.getCdTipoInscricaoRecebedor()));
        request.setCdCpfCnpjRecebedor(verificaLongNulo(entrada.getCdCpfCnpjRecebedor()));
        request.setCdFilialCnpjRecebedor(verificaIntegerNulo(entrada.getCdFilialCnpjRecebedor()));
        request.setCdControleCpfRecebedor(verificaIntegerNulo(entrada.getCdControleCpfRecebedor()));

        response = getFactoryAdapter().getConsultarQtdeValorPgtoPrevistoSolicPDCAdapter().invokeProcess(request);

        saida.setCodMensagem(response.getCodMensagem());
        saida.setMensagem(response.getMensagem());
        saida.setQtdeTotalPagtoPrevistoSoltc(response.getQtdeTotalPagtoPrevistoSoltc());
        saida.setVlrTotPagtoPrevistoSolicitacao(response.getVlrTotPagtoPrevistoSolicitacao());

        return saida;
    }
    
    /**
     * Nome: getFactoryAdapter
     * 
     * @return
     */
    public FactoryAdapter getFactoryAdapter() {
        return factoryAdapter;
    }

    /**
     * Nome: setFactoryAdapter
     * 
     * @param factoryAdapter
     */
    public void setFactoryAdapter(FactoryAdapter factoryAdapter) {
        this.factoryAdapter = factoryAdapter;
    }
}