/*
 * Nome: br.com.bradesco.web.pgit.service.business.tipocomprovante.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.tipocomprovante.bean;

/**
 * Nome: ExcluirTipoComprovanteEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ExcluirTipoComprovanteEntradaDTO {
	
	/** Atributo cdTipoComprovanteSalarial. */
	private int cdTipoComprovanteSalarial;

	/**
	 * Get: cdTipoComprovanteSalarial.
	 *
	 * @return cdTipoComprovanteSalarial
	 */
	public int getCdTipoComprovanteSalarial() {
		return cdTipoComprovanteSalarial;
	}

	/**
	 * Set: cdTipoComprovanteSalarial.
	 *
	 * @param cdTipoComprovanteSalarial the cd tipo comprovante salarial
	 */
	public void setCdTipoComprovanteSalarial(int cdTipoComprovanteSalarial) {
		this.cdTipoComprovanteSalarial = cdTipoComprovanteSalarial;
	}
	
	

}
