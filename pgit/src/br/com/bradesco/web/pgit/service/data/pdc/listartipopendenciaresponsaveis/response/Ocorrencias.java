/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listartipopendenciaresponsaveis.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdPendenciaPagamentoIntegrado
     */
    private long _cdPendenciaPagamentoIntegrado = 0;

    /**
     * keeps track of state for field: _cdPendenciaPagamentoIntegrad
     */
    private boolean _has_cdPendenciaPagamentoIntegrado;

    /**
     * Field _dsTipoUnidadeOrganizacional
     */
    private java.lang.String _dsTipoUnidadeOrganizacional;

    /**
     * Field _dsPendenciaPagamentoIntegrado
     */
    private java.lang.String _dsPendenciaPagamentoIntegrado;

    /**
     * Field _cdTipoBaixaPendencia
     */
    private int _cdTipoBaixaPendencia = 0;

    /**
     * keeps track of state for field: _cdTipoBaixaPendencia
     */
    private boolean _has_cdTipoBaixaPendencia;

    /**
     * Field _cdIndicadorRespostaPagamento
     */
    private int _cdIndicadorRespostaPagamento = 0;

    /**
     * keeps track of state for field: _cdIndicadorRespostaPagamento
     */
    private boolean _has_cdIndicadorRespostaPagamento;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listartipopendenciaresponsaveis.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdIndicadorRespostaPagamento
     * 
     */
    public void deleteCdIndicadorRespostaPagamento()
    {
        this._has_cdIndicadorRespostaPagamento= false;
    } //-- void deleteCdIndicadorRespostaPagamento() 

    /**
     * Method deleteCdPendenciaPagamentoIntegrado
     * 
     */
    public void deleteCdPendenciaPagamentoIntegrado()
    {
        this._has_cdPendenciaPagamentoIntegrado= false;
    } //-- void deleteCdPendenciaPagamentoIntegrado() 

    /**
     * Method deleteCdTipoBaixaPendencia
     * 
     */
    public void deleteCdTipoBaixaPendencia()
    {
        this._has_cdTipoBaixaPendencia= false;
    } //-- void deleteCdTipoBaixaPendencia() 

    /**
     * Returns the value of field 'cdIndicadorRespostaPagamento'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorRespostaPagamento'.
     */
    public int getCdIndicadorRespostaPagamento()
    {
        return this._cdIndicadorRespostaPagamento;
    } //-- int getCdIndicadorRespostaPagamento() 

    /**
     * Returns the value of field 'cdPendenciaPagamentoIntegrado'.
     * 
     * @return long
     * @return the value of field 'cdPendenciaPagamentoIntegrado'.
     */
    public long getCdPendenciaPagamentoIntegrado()
    {
        return this._cdPendenciaPagamentoIntegrado;
    } //-- long getCdPendenciaPagamentoIntegrado() 

    /**
     * Returns the value of field 'cdTipoBaixaPendencia'.
     * 
     * @return int
     * @return the value of field 'cdTipoBaixaPendencia'.
     */
    public int getCdTipoBaixaPendencia()
    {
        return this._cdTipoBaixaPendencia;
    } //-- int getCdTipoBaixaPendencia() 

    /**
     * Returns the value of field 'dsPendenciaPagamentoIntegrado'.
     * 
     * @return String
     * @return the value of field 'dsPendenciaPagamentoIntegrado'.
     */
    public java.lang.String getDsPendenciaPagamentoIntegrado()
    {
        return this._dsPendenciaPagamentoIntegrado;
    } //-- java.lang.String getDsPendenciaPagamentoIntegrado() 

    /**
     * Returns the value of field 'dsTipoUnidadeOrganizacional'.
     * 
     * @return String
     * @return the value of field 'dsTipoUnidadeOrganizacional'.
     */
    public java.lang.String getDsTipoUnidadeOrganizacional()
    {
        return this._dsTipoUnidadeOrganizacional;
    } //-- java.lang.String getDsTipoUnidadeOrganizacional() 

    /**
     * Method hasCdIndicadorRespostaPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorRespostaPagamento()
    {
        return this._has_cdIndicadorRespostaPagamento;
    } //-- boolean hasCdIndicadorRespostaPagamento() 

    /**
     * Method hasCdPendenciaPagamentoIntegrado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPendenciaPagamentoIntegrado()
    {
        return this._has_cdPendenciaPagamentoIntegrado;
    } //-- boolean hasCdPendenciaPagamentoIntegrado() 

    /**
     * Method hasCdTipoBaixaPendencia
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoBaixaPendencia()
    {
        return this._has_cdTipoBaixaPendencia;
    } //-- boolean hasCdTipoBaixaPendencia() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdIndicadorRespostaPagamento'.
     * 
     * @param cdIndicadorRespostaPagamento the value of field
     * 'cdIndicadorRespostaPagamento'.
     */
    public void setCdIndicadorRespostaPagamento(int cdIndicadorRespostaPagamento)
    {
        this._cdIndicadorRespostaPagamento = cdIndicadorRespostaPagamento;
        this._has_cdIndicadorRespostaPagamento = true;
    } //-- void setCdIndicadorRespostaPagamento(int) 

    /**
     * Sets the value of field 'cdPendenciaPagamentoIntegrado'.
     * 
     * @param cdPendenciaPagamentoIntegrado the value of field
     * 'cdPendenciaPagamentoIntegrado'.
     */
    public void setCdPendenciaPagamentoIntegrado(long cdPendenciaPagamentoIntegrado)
    {
        this._cdPendenciaPagamentoIntegrado = cdPendenciaPagamentoIntegrado;
        this._has_cdPendenciaPagamentoIntegrado = true;
    } //-- void setCdPendenciaPagamentoIntegrado(long) 

    /**
     * Sets the value of field 'cdTipoBaixaPendencia'.
     * 
     * @param cdTipoBaixaPendencia the value of field
     * 'cdTipoBaixaPendencia'.
     */
    public void setCdTipoBaixaPendencia(int cdTipoBaixaPendencia)
    {
        this._cdTipoBaixaPendencia = cdTipoBaixaPendencia;
        this._has_cdTipoBaixaPendencia = true;
    } //-- void setCdTipoBaixaPendencia(int) 

    /**
     * Sets the value of field 'dsPendenciaPagamentoIntegrado'.
     * 
     * @param dsPendenciaPagamentoIntegrado the value of field
     * 'dsPendenciaPagamentoIntegrado'.
     */
    public void setDsPendenciaPagamentoIntegrado(java.lang.String dsPendenciaPagamentoIntegrado)
    {
        this._dsPendenciaPagamentoIntegrado = dsPendenciaPagamentoIntegrado;
    } //-- void setDsPendenciaPagamentoIntegrado(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoUnidadeOrganizacional'.
     * 
     * @param dsTipoUnidadeOrganizacional the value of field
     * 'dsTipoUnidadeOrganizacional'.
     */
    public void setDsTipoUnidadeOrganizacional(java.lang.String dsTipoUnidadeOrganizacional)
    {
        this._dsTipoUnidadeOrganizacional = dsTipoUnidadeOrganizacional;
    } //-- void setDsTipoUnidadeOrganizacional(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listartipopendenciaresponsaveis.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listartipopendenciaresponsaveis.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listartipopendenciaresponsaveis.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listartipopendenciaresponsaveis.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
