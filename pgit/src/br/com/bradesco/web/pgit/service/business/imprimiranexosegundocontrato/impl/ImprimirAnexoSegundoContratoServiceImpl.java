/**
 * Nome: br.com.bradesco.web.pgit.service.business.imprimiranexosegundocontrato.impl
 * Compilador: JDK 1.5
 * Prop�sito: INSERIR O PROP�SITO DAS CLASSES DO PACOTE
 * Data da cria��o: <dd/MM/yyyy>
 * Par�metros de compila��o: -d
 */
package br.com.bradesco.web.pgit.service.business.imprimiranexosegundocontrato.impl;

import java.util.ArrayList;
import java.util.List;

import br.com.bradesco.web.pgit.service.business.imprimiranexosegundocontrato.IImprimirAnexoSegundoContratoService;
import br.com.bradesco.web.pgit.service.business.imprimiranexosegundocontrato.bean.ImprimirAnexoSegundoContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.imprimiranexosegundocontrato.bean.ImprimirAnexoSegundoContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.imprimiranexosegundocontrato.bean.OcorrenciasAvisoFormularios;
import br.com.bradesco.web.pgit.service.business.imprimiranexosegundocontrato.bean.OcorrenciasAvisos;
import br.com.bradesco.web.pgit.service.business.imprimiranexosegundocontrato.bean.OcorrenciasComprovanteSalariosDiversos;
import br.com.bradesco.web.pgit.service.business.imprimiranexosegundocontrato.bean.OcorrenciasComprovantes;
import br.com.bradesco.web.pgit.service.business.imprimiranexosegundocontrato.bean.OcorrenciasCreditos;
import br.com.bradesco.web.pgit.service.business.imprimiranexosegundocontrato.bean.OcorrenciasModBeneficios;
import br.com.bradesco.web.pgit.service.business.imprimiranexosegundocontrato.bean.OcorrenciasModFornecedores;
import br.com.bradesco.web.pgit.service.business.imprimiranexosegundocontrato.bean.OcorrenciasModRecadastro;
import br.com.bradesco.web.pgit.service.business.imprimiranexosegundocontrato.bean.OcorrenciasModSalarial;
import br.com.bradesco.web.pgit.service.business.imprimiranexosegundocontrato.bean.OcorrenciasModTributos;
import br.com.bradesco.web.pgit.service.business.imprimiranexosegundocontrato.bean.OcorrenciasModularidades;
import br.com.bradesco.web.pgit.service.business.imprimiranexosegundocontrato.bean.OcorrenciasOperacoes;
import br.com.bradesco.web.pgit.service.business.imprimiranexosegundocontrato.bean.OcorrenciasServicoPagamentos;
import br.com.bradesco.web.pgit.service.business.imprimiranexosegundocontrato.bean.OcorrenciasTitulos;
import br.com.bradesco.web.pgit.service.business.imprimiranexosegundocontrato.bean.OcorrenciasTriTributo;
import br.com.bradesco.web.pgit.service.data.pdc.FactoryAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.request.ImprimirAnexoSegundoContratoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.ImprimirAnexoSegundoContratoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias10;

// TODO: Auto-generated Javadoc
/**
 * Nome: ImprimirAnexoSegundoContratoServiceImpl
 * <p>
 * Prop�sito: Implementa��o do adaptador ImprimirAnexoSegundoContrato
 * </p>.
 *
 * @author CPM Braxis / TI Melhorias - Arquitetura
 * @version 1.0
 * @see IImprimirAnexoSegundoContratoService
 */
public class ImprimirAnexoSegundoContratoServiceImpl implements IImprimirAnexoSegundoContratoService {

    /** Atributo factoryAdapter. */
    private FactoryAdapter factoryAdapter;

    /**
     * (non-Javadoc).
     *
     * @param entrada the entrada
     * @return the imprimir anexo segundo contrato saida dto
     * @see br.com.bradesco.web.pgit.service.business.imprimiranexosegundocontrato.
     * IImprimirAnexoSegundoContratoService#imprimirAnexoPrimeiroContrato(br.com.bradesco.web.pgit.service.
     * business.imprimiranexosegundocontrato.bean.ImprimirAnexoSegundoContratoEntradaDTO)
     */
    public ImprimirAnexoSegundoContratoSaidaDTO imprimirAnexoPrimeiroContrato(
        ImprimirAnexoSegundoContratoEntradaDTO entrada) {
        ImprimirAnexoSegundoContratoRequest request = new ImprimirAnexoSegundoContratoRequest();
        request.setCdPessoaJuridicaContrato(entrada.getCdPessoaJuridicaContrato());
        request.setCdTipoContratoNegocio(entrada.getCdTipoContratoNegocio());
        request.setNrSequenciaContratoNegocio(entrada.getNrSequenciaContratoNegocio());

        ImprimirAnexoSegundoContratoResponse response =
            getFactoryAdapter().getImprimirAnexoSegundoContratoPDCAdapter().invokeProcess(request);

        ImprimirAnexoSegundoContratoSaidaDTO saida = new ImprimirAnexoSegundoContratoSaidaDTO();

        popularFornecedor(saida, response);
        popularTributos(saida, response);
        popularSalarios(saida, response);
        popularBeneficiarios(saida, response);

        // Emiss�o de aviso de movimenta��o
        saida.setCdIndicadorAviso(response.getCdIndicadorAviso());
        saida.setCdIndicadorAvisoPagador(response.getCdIndicadorAvisoPagador());
        saida.setCdIndicadorAvisoFavorecido(response.getCdIndicadorAvisoFavorecido());

        // Emiss�o de comprovante de pagamento
        saida.setCdIndicadorComplemento(response.getCdIndicadorComplemento());
        saida.setCdIndicadorComplementoPagador(response.getCdIndicadorComplementoPagador());
        saida.setCdIndicadorComplementoFavorecido(response.getCdIndicadorComplementoFavorecido());
        saida.setCdIndicadorComplementoSalarial(response.getCdIndicadorComplementoSalarial());
        saida.setCdIndicadorComplementoDivergente(response.getCdIndicadorComplementoDivergente());

        // Cadastro de Favorecidos
        saida.setCdIndcadorCadastroFavorecido(response.getCdIndcadorCadastroFavorecido());

        popularRecadastroBeneficiarios(saida, response);

        // ?????????

        // Servi�o: Pagamento de Fornecedores / Pagamento de Tributos / Pagamento de Salarios / Pagamento de Beneficios
        OcorrenciasServicoPagamentos servicoPagamentos = null;
        List<OcorrenciasServicoPagamentos> listaServicoPagamento = new ArrayList<OcorrenciasServicoPagamentos>();

        for (int i = 0; i < response.getOcorrencias6Count(); i++) {
            servicoPagamentos = new OcorrenciasServicoPagamentos();

            servicoPagamentos.setCdServicoIndicadorListaDebito(response.getOcorrencias6(i)
                .getCdServicoIndicadorListaDebito());
            servicoPagamentos.setDsServicoTipoServico(response.getOcorrencias6(i).getDsServicoTipoServico());
            servicoPagamentos.setDsFormaEnvioPagamento(response.getOcorrencias6(i).getDsFormaEnvioPagamento());
            servicoPagamentos.setDsServicoFormaAutorizacaoPagamento(response.getOcorrencias6(i)
                .getDsServicoFormaAutorizacaoPagamento());
            servicoPagamentos.setDsIndicadorGeradorRetorno(response.getOcorrencias6(i).getDsIndicadorGeradorRetorno());
            servicoPagamentos.setDsServicoIndicadorRetornoSeparado(response.getOcorrencias6(i)
                .getDsServicoIndicadorRetornoSeparado());
            servicoPagamentos.setDsServicoListaDebito(response.getOcorrencias6(i).getDsServicoListaDebito());
            servicoPagamentos.setDsServicoTratamentoListaDebito(response.getOcorrencias6(i)
                .getDsServicoTratamentoListaDebito());
            servicoPagamentos.setDsServicoPreAutorizacaoCliente(response.getOcorrencias6(i)
                .getDsServicoPreAutorizacaoCliente());
            servicoPagamentos.setDsServicoAutorizacaoComplementoAgencia(response.getOcorrencias6(i)
                .getDsServicoAutorizacaoComplementoAgencia());
            
            servicoPagamentos.setDsServicoTipoDataFloat(response.getOcorrencias6(i).getDsServicoTipoDataFloat());
            servicoPagamentos.setCdServicoFloating(response.getOcorrencias6(i).getCdServicoFloating());
            servicoPagamentos.setCdServicoTipoConsolidado(response.getOcorrencias6(i).getCdServicoTipoConsolidado());
            
            listaServicoPagamento.add(servicoPagamentos);
        }
        saida.setListaServicoPagamento(listaServicoPagamento);

        // Cr�dito em Conta (P/ Pagamentos de Fornecedores, Sal�rios e Beneficios)
        OcorrenciasCreditos creditos = null;
        List<OcorrenciasCreditos> listaCreditos = new ArrayList<OcorrenciasCreditos>();

        for (int i = 0; i < response.getOcorrencias11Count(); i++) {
            creditos = new OcorrenciasCreditos();

            creditos.setDsCretipoServico(response.getOcorrencias11(i).getDsCretipoServico());
            creditos.setDsCreTipoConsultaSaldo(response.getOcorrencias11(i).getDsCreTipoConsultaSaldo());
            creditos.setDsCreTipoProcessamento(response.getOcorrencias11(i).getDsCreTipoProcessamento());
            creditos.setDsCreTipoTratamentoFeriado(response.getOcorrencias11(i).getDsCreTipoTratamentoFeriado());
            creditos.setDsCreTipoRejeicaoEfetivacao(response.getOcorrencias11(i).getDsCreTipoRejeicaoEfetivacao());
            creditos.setDsCreTipoRejeicaoAgendada(response.getOcorrencias11(i).getDsCreTipoRejeicaoAgendada());
            creditos
                .setCdCreIndicadorPercentualMaximo(response.getOcorrencias11(i).getCdCreIndicadorPercentualMaximo());
            creditos.setPcCreMaximoInconsistente(response.getOcorrencias11(i).getPcCreMaximoInconsistente());
            creditos
                .setCdCreIndicadorQuantidadeMaxima(response.getOcorrencias11(i).getCdCreIndicadorQuantidadeMaxima());
            creditos.setQtCreMaximaInconsistencia(response.getOcorrencias11(i).getQtCreMaximaInconsistencia());
            creditos.setCdCreIndicadorValorMaximo(response.getOcorrencias11(i).getCdCreIndicadorValorMaximo());
            creditos.setVlCreMaximoFavorecidoNao(response.getOcorrencias11(i).getVlCreMaximoFavorecidoNao());
            creditos.setDsCrePrioridadeDebito(response.getOcorrencias11(i).getDsCrePrioridadeDebito());
            creditos.setCdCreIndicadorValorDia(response.getOcorrencias11(i).getCdCreIndicadorValorDia());
            creditos.setVlCreLimiteDiario(response.getOcorrencias11(i).getVlCreLimiteDiario());
            creditos.setCdCreIndicadorValorIndividual(response.getOcorrencias11(i).getCdCreIndicadorValorIndividual());
            creditos.setVlCreLimiteIndividual(response.getOcorrencias11(i).getVlCreLimiteIndividual());
            creditos.setCdCreIndicadorCadastroFavorecido(response.getOcorrencias11(i)
                .getCdCreIndicadorCadastroFavorecido());
            creditos.setCdCreUtilizacaoCadastroFavorecido(response.getOcorrencias11(i)
                .getCdCreUtilizacaoCadastroFavorecido());
            creditos.setCdCreIndicadorQuantidadeRepique(response.getOcorrencias11(i)
                .getCdCreIndicadorQuantidadeRepique());
            creditos.setQtCreDiaRepique(response.getOcorrencias11(i).getQtCreDiaRepique());
            creditos.setCdCreIndicadorQuantidadeFloating(response.getOcorrencias11(i)
                .getCdCreIndicadorQuantidadeFloating());
            creditos.setQtCreDiaFloating(response.getOcorrencias11(i).getQtCreDiaFloating());
            creditos.setDsCreTipoConsistenciaFavorecido(response.getOcorrencias11(i)
                .getDsCreTipoConsistenciaFavorecido());
            
            creditos.setCdCreCferiLoc(response.getOcorrencias11(i).getCdCreCferiLoc());
            creditos.setCdCreContratoTransf(response.getOcorrencias11(i).getCdCreContratoTransf());
            creditos.setCdLctoPgmd(response.getOcorrencias11(i).getCdLctoPgmd());

            listaCreditos.add(creditos);
        }
        saida.setListaCreditos(listaCreditos);

        // Titulos Bradesco / Titulos Outros Banco
        OcorrenciasTitulos titulos = null;
        List<OcorrenciasTitulos> listaTitulos = new ArrayList<OcorrenciasTitulos>();

        for (int i = 0; i < response.getOcorrencias10Count(); i++) {
            Ocorrencias10 ocorrencias10 = response.getOcorrencias10(i);

            titulos = new OcorrenciasTitulos();
            titulos.setDsFtiTipoTitulo(ocorrencias10.getDsFtiTipoTitulo());
            titulos.setDsFtiTipoConsultaSaldo(ocorrencias10.getDsFtiTipoConsultaSaldo());
            titulos.setDsFtiTipoProcessamento(ocorrencias10.getDsFtiTipoProcessamento());
            titulos.setDsFtiTipoTratamentoFeriado(ocorrencias10.getDsFtiTipoTratamentoFeriado());
            titulos.setDsFtiTipoRejeicaoEfetivacao(ocorrencias10.getDsFtiTipoRejeicaoEfetivacao());
            titulos.setDsFtiTipoRejeicaoAgendamento(ocorrencias10.getDsFtiTipoRejeicaoAgendamento());
            titulos.setCdFtiIndicadorPercentualMaximo(ocorrencias10.getCdFtiIndicadorPercentualMaximo());
            titulos.setPcFtiMaximoInconsistente(ocorrencias10.getPcFtiMaximoInconsistente());
            titulos.setCdFtiIndicadorQuantidadeMaxima(ocorrencias10.getCdFtiIndicadorQuantidadeMaxima());
            titulos.setQtFtiMaximaInconsistencia(ocorrencias10.getQtFtiMaximaInconsistencia());
            titulos.setCdFtiIndicadorValorMaximo(ocorrencias10.getCdFtiIndicadorValorMaximo());
            titulos.setVlFtiMaximoFavorecidoNao(ocorrencias10.getVlFtiMaximoFavorecidoNao());
            titulos.setDsFtiPrioridadeDebito(ocorrencias10.getDsFtiPrioridadeDebito());
            titulos.setCdFtiIndicadorValorDia(ocorrencias10.getCdFtiIndicadorValorDia());
            titulos.setVlFtiLimiteDiario(ocorrencias10.getVlFtiLimiteDiario());
            titulos.setCdFtiIndicadorValorIndividual(ocorrencias10.getCdFtiIndicadorValorIndividual());
            titulos.setVlFtiLimiteIndividual(ocorrencias10.getVlFtiLimiteIndividual());
            titulos.setCdFtiIndicadorQuantidadeFloating(ocorrencias10.getCdFtiIndicadorQuantidadeFloating());
            titulos.setQtFtiDiaFloating(ocorrencias10.getQtFtiDiaFloating());
            titulos.setCdFtiIndicadorCadastroFavorecido(ocorrencias10.getCdFtiIndicadorCadastroFavorecido());
            titulos.setCdFtiUtilizadoFavorecido(ocorrencias10.getCdFtiUtilizadoFavorecido());
            titulos.setCdFtiIndicadorQuantidadeRepique(ocorrencias10.getCdFtiIndicadorQuantidadeRepique());
            titulos.setQtFtiDiaRepique(ocorrencias10.getQtFtiDiaRepique());
            titulos.setDsTIpoConsultaSaldoSuperior(ocorrencias10.getDsTIpoConsultaSaldoSuperior());
            titulos.setFtiPagamentoVencido(ocorrencias10.getFtiPagamentoVencido());
            titulos.setFtiPagamentoMenor(ocorrencias10.getFtiPagamentoMenor());
            titulos.setFtiDsTipoConsFavorecido(ocorrencias10.getFtiDsTipoConsFavorecido());
            titulos.setFtiCferiLoc(ocorrencias10.getFtiCferiLoc());
            titulos.setFtiQuantidadeMaxVencido(ocorrencias10.getFtiQuantidadeMaxVencido());
            
            listaTitulos.add(titulos);
        }

        saida.setListaTitulos(listaTitulos);

        // DOC / TED / Debito em Conta / Ordem de Credito / Deposito Identificado
        OcorrenciasModularidades modularidades = null;
        List<OcorrenciasModularidades> listaModularidades = new ArrayList<OcorrenciasModularidades>();

        for (int i = 0; i < response.getOcorrencias13Count(); i++) {
            modularidades = new OcorrenciasModularidades();

            modularidades.setDsModTipoServico(response.getOcorrencias13(i).getDsModTipoServico());
            modularidades.setDsModTipoModalidade(response.getOcorrencias13(i).getDsModTipoModalidade());

            modularidades.setDsModTipoConsultaSaldo(response.getOcorrencias13(i).getDsModTipoConsultaSaldo());
            modularidades.setDsModTipoProcessamento(response.getOcorrencias13(i).getDsModTipoProcessamento());
            modularidades.setDsModTipoTratamentoFeriado(response.getOcorrencias13(i).getDsModTipoTratamentoFeriado());
            modularidades.setDsModTipoRejeicaoEfetivacao(response.getOcorrencias13(i).getDsModTipoRejeicaoEfetivacao());
            modularidades.setDsModTipoRejeicaoAgendado(response.getOcorrencias13(i).getDsModTipoRejeicaoAgendado());
            modularidades.setCdModIndicadorPercentualMaximo(response.getOcorrencias13(i)
                .getCdModIndicadorPercentualMaximo());
            modularidades.setCdModIndicadorPercentualMaximoInconsistente(response.getOcorrencias13(i)
                .getCdModIndicadorPercentualMaximoInconsistente());
            modularidades.setCdModIndicadorQuantidadeMaxima(response.getOcorrencias13(i)
                .getCdModIndicadorQuantidadeMaxima());
            modularidades.setQtModMaximoInconsistencia(response.getOcorrencias13(i).getQtModMaximoInconsistencia());
            modularidades.setCdModIndicadorValorMaximo(response.getOcorrencias13(i).getCdModIndicadorValorMaximo());
            modularidades.setVlModMaximoFavorecidoNao(response.getOcorrencias13(i).getVlModMaximoFavorecidoNao());
            modularidades.setDsModPrioridadeDebito(response.getOcorrencias13(i).getDsModPrioridadeDebito());
            modularidades.setCdModIndicadorValorDia(response.getOcorrencias13(i).getCdModIndicadorValorDia());
            modularidades.setVlModLimiteDiario(response.getOcorrencias13(i).getVlModLimiteDiario());
            modularidades.setCdModIndicadorValorIndividual(response.getOcorrencias13(i)
                .getCdModIndicadorValorIndividual());
            modularidades.setVlModLimiteIndividual(response.getOcorrencias13(i).getVlModLimiteIndividual());
            modularidades.setCdModIndicadorQuantidadeFloating(response.getOcorrencias13(i)
                .getCdModIndicadorQuantidadeFloating());
            modularidades.setQtModDiaFloating(response.getOcorrencias13(i).getQtModDiaFloating());
            modularidades.setCdModIndicadorCadastroFavorecido(response.getOcorrencias13(i)
                .getCdModIndicadorCadastroFavorecido());
            modularidades.setCdModUtilizacaoCadastroFavorecido(response.getOcorrencias13(i)
                .getCdModUtilizacaoCadastroFavorecido());
            modularidades.setCdModIndicadorQuantidadeRepique(response.getOcorrencias13(i)
                .getCdModIndicadorQuantidadeRepique());
            modularidades.setQtModDiaRepique(response.getOcorrencias13(i).getQtModDiaRepique());
            modularidades.setDsModalidadeCferiLoc(response.getOcorrencias13(i).getDsModalidadeCferiLoc());
            modularidades.setDsModTipoConsFavorecido(response.getOcorrencias13(i).getDsModTipoConsFavorecido());

            listaModularidades.add(modularidades);
        }
        saida.setListaModularidades(listaModularidades);

        // Ordem de Pagamento
        OcorrenciasOperacoes ordemPagamento = null;
        List<OcorrenciasOperacoes> listaOrdemPagamento = new ArrayList<OcorrenciasOperacoes>();

        for (int i = 0; i < response.getOcorrencias12Count(); i++) {
            ordemPagamento = new OcorrenciasOperacoes();

            ordemPagamento.setDsOpgTipoServico(response.getOcorrencias12(i).getDsOpgTipoServico());
            ordemPagamento.setDsOpgTipoConsultaSaldo(response.getOcorrencias12(i).getDsOpgTipoConsultaSaldo());
            ordemPagamento.setDsOpgTipoProcessamento(response.getOcorrencias12(i).getDsOpgTipoProcessamento());
            ordemPagamento.setDsOpgTipoTratamentoFeriado(response.getOcorrencias12(i).getDsOpgTipoTratamentoFeriado());
            ordemPagamento
                .setDsOpgTipoRejeicaoEfetivacao(response.getOcorrencias12(i).getDsOpgTipoRejeicaoEfetivacao());
            ordemPagamento.setDsOpgTiporejeicaoAgendado(response.getOcorrencias12(i).getDsOpgTiporejeicaoAgendado());
            ordemPagamento.setCdOpgIndicadorPercentualMaximo(response.getOcorrencias12(i)
                .getCdOpgIndicadorPercentualMaximo());
            ordemPagamento.setPcOpgMaximoInconsistencia(response.getOcorrencias12(i).getPcOpgMaximoInconsistencia());
            ordemPagamento.setCdOpgIndicadorQuantidadeMaximo(response.getOcorrencias12(i)
                .getCdOpgIndicadorQuantidadeMaximo());
            ordemPagamento.setQtOpgMaximoInconsistencia(response.getOcorrencias12(i).getQtOpgMaximoInconsistencia());
            ordemPagamento.setCdOpgIndicadorValorDia(response.getOcorrencias12(i).getCdOpgIndicadorValorDia());
            ordemPagamento.setVlOpgLimiteDiario(response.getOcorrencias12(i).getVlOpgLimiteDiario());
            ordemPagamento.setCdOpgIndicadorValorIndividual(response.getOcorrencias12(i)
                .getCdOpgIndicadorValorIndividual());
            ordemPagamento.setVlOpgLimiteIndividual(response.getOcorrencias12(i).getVlOpgLimiteIndividual());
            ordemPagamento.setCdOpgIndicadorQuantidadeFloating(response.getOcorrencias12(i)
                .getCdOpgIndicadorQuantidadeFloating());
            ordemPagamento.setQtOpgDiaFloating(response.getOcorrencias12(i).getQtOpgDiaFloating());
            ordemPagamento.setDsOpgPrioridadeDebito(response.getOcorrencias12(i).getDsOpgPrioridadeDebito());
            ordemPagamento.setCdOpgIndicadorValorMaximo(response.getOcorrencias12(i).getCdOpgIndicadorValorMaximo());
            ordemPagamento.setVlOpgMaximoFavorecidoNao(response.getOcorrencias12(i).getVlOpgMaximoFavorecidoNao());
            ordemPagamento.setCdOpgIndicadorCadastroFavorecido(response.getOcorrencias12(i)
                .getCdOpgIndicadorCadastroFavorecido());
            ordemPagamento.setCdOpgUtilizacaoCadastroFavorecido(response.getOcorrencias12(i)
                .getCdOpgUtilizacaoCadastroFavorecido());
            ordemPagamento.setDsOpgOcorrenciasDebito(response.getOcorrencias12(i).getDsOpgOcorrenciasDebito());
            ordemPagamento.setDsOpgTipoInscricao(response.getOcorrencias12(i).getDsOpgTipoInscricao());
            ordemPagamento.setCdOpgIndicadorQuantidadeRepique(response.getOcorrencias12(i)
                .getCdOpgIndicadorQuantidadeRepique());
            ordemPagamento.setQtOpgDiaRepique(response.getOcorrencias12(i).getQtOpgDiaRepique());
            ordemPagamento.setQtOpgDiaExpiracao(response.getOcorrencias12(i).getQtOpgDiaExpiracao());
           ordemPagamento.setDsTipoConsFavorecido(response.getOcorrencias12(i).getDsTipoConsFavorecido());
           ordemPagamento.setCdFeriLoc(response.getOcorrencias12(i).getCdFeriLoc());
           ordemPagamento.setoPgEmissao(response.getOcorrencias12(i).getOPgEmissao());

            listaOrdemPagamento.add(ordemPagamento);
        }
        saida.setListaOperacoes(listaOrdemPagamento);

        // Rastreamento
        saida.setDsFraTipoRastreabilidade(response.getDsFraTipoRastreabilidade());
        saida.setDsFraRastreabilidadeTerceiro(response.getDsFraRastreabilidadeTerceiro());
        saida.setDsFraRastreabilidadeNota(response.getDsFraRastreabilidadeNota());
        saida.setDsFraCaptacaoTitulo(response.getDsFraCaptacaoTitulo());
        saida.setDsFraAgendaCliente(response.getDsFraAgendaCliente());
        saida.setDsFraAgendaFilial(response.getDsFraAgendaFilial());
        saida.setDsFraBloqueioEmissao(response.getDsFraBloqueioEmissao());
        saida.setDtFraInicioBloqueio(response.getDtFraInicioBloqueio());
        saida.setDtFraInicioRastreabilidade(response.getDtFraInicioRastreabilidade());
        saida.setDsFraAdesaoSacado(response.getDsFraAdesaoSacado());

        // Debito de Veiculos/GARE/DARF/GPS/C�digo de Barras
        OcorrenciasTriTributo debitoVeiculos = null;
        List<OcorrenciasTriTributo> listaDebitoVeiculos = new ArrayList<OcorrenciasTriTributo>();

        for (int i = 0; i < response.getOcorrencias14Count(); i++) {
            debitoVeiculos = new OcorrenciasTriTributo();

            debitoVeiculos.setDsTriTipoTributo(response.getOcorrencias14(i).getDsTriTipoTributo());
            debitoVeiculos.setDsTriTipoConsultaSaldo(response.getOcorrencias14(i).getDsTriTipoConsultaSaldo());
            debitoVeiculos.setDsTriTipoProcessamento(response.getOcorrencias14(i).getDsTriTipoProcessamento());
            debitoVeiculos.setDsTriTipoTratamentoFeriado(response.getOcorrencias14(i).getDsTriTipoTratamentoFeriado());
            debitoVeiculos
                .setDsTriTipoRejeicaoEfetivacao(response.getOcorrencias14(i).getDsTriTipoRejeicaoEfetivacao());
            debitoVeiculos.setDsTriTipoRejeicaoAgendado(response.getOcorrencias14(i).getDsTriTipoRejeicaoAgendado());
            debitoVeiculos.setCdTriIndicadorValorMaximo(response.getOcorrencias14(i).getCdTriIndicadorValorMaximo());
            debitoVeiculos.setVlTriMaximoFavorecidoNao(response.getOcorrencias14(i).getVlTriMaximoFavorecidoNao());
            debitoVeiculos.setDsTriPrioridadeDebito(response.getOcorrencias14(i).getDsTriPrioridadeDebito());
            debitoVeiculos.setCdTriIndicadorValorDia(response.getOcorrencias14(i).getCdTriIndicadorValorDia());
            debitoVeiculos.setVlTriLimiteDiario(response.getOcorrencias14(i).getVlTriLimiteDiario());
            debitoVeiculos.setCdTriIndicadorValorIndividual(response.getOcorrencias14(i)
                .getCdTriIndicadorValorIndividual());
            debitoVeiculos.setVlTriLimiteIndividual(response.getOcorrencias14(i).getVlTriLimiteIndividual());
            debitoVeiculos.setCdTriIndicadorQuantidadeFloating(response.getOcorrencias14(i)
                .getCdTriIndicadorQuantidadeFloating());
            debitoVeiculos.setQttriDiaFloating(response.getOcorrencias14(i).getQttriDiaFloating());
            debitoVeiculos.setCdTriIndicadorCadastroFavorecido(response.getOcorrencias14(i)
                .getCdTriIndicadorCadastroFavorecido());
            debitoVeiculos.setCdTriUtilizacaoCadastroFavorecido(response.getOcorrencias14(i)
                .getCdTriUtilizacaoCadastroFavorecido());
            debitoVeiculos.setCdTriIndicadorQuantidadeRepique(response.getOcorrencias14(i)
                .getCdTriIndicadorQuantidadeRepique());
            debitoVeiculos.setQtTriDiaRepique(response.getOcorrencias14(i).getQtTriDiaRepique());
            
            debitoVeiculos.setDsTriTipoConsFavorecido(response.getOcorrencias14(i).getDsTriTipoConsFavorecido());
            debitoVeiculos.setCdTriCferiLoc(response.getOcorrencias14(i).getCdTriCferiLoc());

            listaDebitoVeiculos.add(debitoVeiculos);

        }
        saida.setListaTriTributos(listaDebitoVeiculos);

        // Comprova��o de vida
        saida.setDsBprTipoAcao(response.getDsBprTipoAcao());
        saida.setQtBprMesProvisorio(response.getQtBprMesProvisorio());
        saida.setCdBprIndicadorEmissaoAviso(response.getCdBprIndicadorEmissaoAviso());
        saida.setQtBprDiaAnteriorAviso(response.getQtBprDiaAnteriorAviso());
        saida.setQtBprDiaAnteriorVencimento(response.getQtBprDiaAnteriorVencimento());
        saida.setCdBprUtilizadoMensagemPersonalizada(response.getCdBprUtilizadoMensagemPersonalizada());
        saida.setDsBprDestino(response.getDsBprDestino());
        saida.setDsBprCadastroEnderecoUtilizado(response.getDsBprCadastroEnderecoUtilizado());

        // Servi�o: Aviso Movimenta��o Favorecido/Aviso Movimenta��o Pagador
        OcorrenciasAvisos avisos = null;
        List<OcorrenciasAvisos> listaAvisos = new ArrayList<OcorrenciasAvisos>();

        for (int i = 0; i < response.getOcorrencias7Count(); i++) {
            avisos = new OcorrenciasAvisos();

            avisos.setDsAvisoTipoAviso(response.getOcorrencias7(i).getDsAvisoTipoAviso());
            avisos.setDsAvisoPeriodicidadeEmissao(response.getOcorrencias7(i).getDsAvisoPeriodicidadeEmissao());
            avisos.setDsAvisoDestino(response.getOcorrencias7(i).getDsAvisoDestino());
            avisos.setDsAvisoPermissaoCorrespondenciaAbertura(response.getOcorrencias7(i)
                .getDsAvisoPermissaoCorrespondenciaAbertura());
            avisos.setDsAvisoDemontracaoInformacaoReservada(response.getOcorrencias7(i)
                .getDsAvisoDemontracaoInformacaoReservada());
            avisos.setCdAvisoQuantidadeViasEmitir(response.getOcorrencias7(i).getCdAvisoQuantidadeViasEmitir());
            avisos.setDsAvisoAgrupamentoCorrespondencia(response.getOcorrencias7(i)
                .getDsAvisoAgrupamentoCorrespondencia());

            listaAvisos.add(avisos);
        }
        saida.setListaAvisos(listaAvisos);

        // Comprovante Pagamento Favorecido /Comprovante Pagamento pagador
        OcorrenciasComprovantes comprovantes = null;
        List<OcorrenciasComprovantes> listaComprovantes = new ArrayList<OcorrenciasComprovantes>();

        for (int i = 0; i < response.getOcorrencias8Count(); i++) {
            comprovantes = new OcorrenciasComprovantes();

            comprovantes.setCdComprovanteTipoComprovante(response.getOcorrencias8(i).getCdComprovanteTipoComprovante());
            comprovantes.setDsComprovantePeriodicidadeEmissao(response.getOcorrencias8(i)
                .getDsComprovantePeriodicidadeEmissao());
            comprovantes.setDsComprovanteDestino(response.getOcorrencias8(i).getDsComprovanteDestino());
            comprovantes.setDsComprovantePermissaoCorrespondenteAbertura(response.getOcorrencias8(i)
                .getDsComprovantePermissaoCorrespondenteAbertura());
            comprovantes.setDsDemonstrativoInformacaoReservada(response.getOcorrencias8(i)
                .getDsDemonstrativoInformacaoReservada());
            comprovantes.setQtViasEmitir(response.getOcorrencias8(i).getQtViasEmitir());
            comprovantes.setDsAgrupamentoCorrespondente(response.getOcorrencias8(i).getDsAgrupamentoCorrespondente());

            listaComprovantes.add(comprovantes);
        }
        saida.setListaComprovantes(listaComprovantes);

        // Servi�o: ComprovanteSalarial /Comprovante diversos
        OcorrenciasComprovanteSalariosDiversos comprovantesSalariosDiversos = null;
        List<OcorrenciasComprovanteSalariosDiversos> listaComprovanteSalarioDiversos =
            new ArrayList<OcorrenciasComprovanteSalariosDiversos>();

        for (int i = 0; i < response.getOcorrencias9Count(); i++) {
            comprovantesSalariosDiversos = new OcorrenciasComprovanteSalariosDiversos();

            comprovantesSalariosDiversos.setDsCsdTipoComprovante(response.getOcorrencias9(i).getDsCsdTipoComprovante());
            comprovantesSalariosDiversos.setDsCsdTipoRejeicaoLote(response.getOcorrencias9(i)
                .getDsCsdTipoRejeicaoLote());
            comprovantesSalariosDiversos.setDsCsdMeioDisponibilizacaoCorrentista(response.getOcorrencias9(i)
                .getDsCsdMeioDisponibilizacaoCorrentista());
            comprovantesSalariosDiversos.setDsCsdMediaDisponibilidadeCorrentista(response.getOcorrencias9(i)
                .getDsCsdMediaDisponibilidadeCorrentista());
            comprovantesSalariosDiversos.setDsCsdMediaDisponibilidadeNumeroCorrentistas(response.getOcorrencias9(i)
                .getDsCsdMediaDisponibilidadeNumeroCorrentistas());
            comprovantesSalariosDiversos.setDsCsdDestino(response.getOcorrencias9(i).getDsCsdDestino());
            comprovantesSalariosDiversos.setQtCsdMesEmissao(response.getOcorrencias9(i).getQtCsdMesEmissao());
            comprovantesSalariosDiversos.setDsCsdCadastroConservadorEndereco(response.getOcorrencias9(i)
                .getDsCsdCadastroConservadorEndereco());
            comprovantesSalariosDiversos.setCdCsdQuantidadeViasEmitir(response.getOcorrencias9(i)
                .getCdCsdQuantidadeViasEmitir());

            listaComprovanteSalarioDiversos.add(comprovantesSalariosDiversos);
        }
        saida.setListaComprovanteSalarioDiversos(listaComprovanteSalarioDiversos);

        // Servi�o: Cadastro de Favorecido
        saida.setCdIndcadorCadastroFavorecido(response.getCdIndcadorCadastroFavorecido());
        saida.setDsCadastroFormaManutencao(response.getDsCadastroFormaManutencao());
        saida.setCdCadastroQuantidadeDiasInativos(response.getCdCadastroQuantidadeDiasInativos());
        saida.setDsCadastroTipoConsistenciaInscricao(response.getDsCadastroTipoConsistenciaInscricao());

        // Servi�o: Recadastramento de Beneficiarios
        saida.setDsServicoRecadastro(response.getDsServicoRecadastro());
        saida.setDsRecTipoIdentificacaoBeneficio(response.getDsRecTipoIdentificacaoBeneficio());
        saida.setDsRecTipoConsistenciaIdentificacao(response.getDsRecTipoConsistenciaIdentificacao());
        saida.setDsRecCondicaoEnquadramento(response.getDsRecCondicaoEnquadramento());
        saida.setDsRecCriterioPrincipalEnquadramento(response.getDsRecCriterioPrincipalEnquadramento());
        saida.setDsRecTipoCriterioCompostoEnquadramento(response.getDsRecTipoCriterioCompostoEnquadramento());
        saida.setQtRecEtapaRecadastramento(response.getQtRecEtapaRecadastramento());
        saida.setQtRecFaseEtapaRecadastramento(response.getQtRecFaseEtapaRecadastramento());
        saida.setDtRecInicioRecadastramento(response.getDtRecInicioRecadastramento());
        saida.setDtRecFinalRecadastramento(response.getDtRecFinalRecadastramento());
        saida.setDsRecGeradorRetornoInternet(response.getDsRecGeradorRetornoInternet());

        // Aviso de Recadastramento /Formul�rio de Recadastramento
        OcorrenciasAvisoFormularios avisoFormularios = null;
        List<OcorrenciasAvisoFormularios> listaAvisoFormularios = new ArrayList<OcorrenciasAvisoFormularios>();

        for (int i = 0; i < response.getOcorrencias15Count(); i++) {
            avisoFormularios = new OcorrenciasAvisoFormularios();

            avisoFormularios.setDsRecTipoModalidade(response.getOcorrencias15(i).getDsRecTipoModalidade());
            avisoFormularios.setDsRecMomentoEnvio(response.getOcorrencias15(i).getDsRecMomentoEnvio());
            avisoFormularios.setDsRecDestino(response.getOcorrencias15(i).getDsRecDestino());
            avisoFormularios.setDsRecCadastroEnderecoUtilizado(response.getOcorrencias15(i)
                .getDsRecCadastroEnderecoUtilizado());
            avisoFormularios.setQtRecDiaAvisoAntecipado(response.getOcorrencias15(i).getQtRecDiaAvisoAntecipado());
            avisoFormularios.setDsRecPermissaoAgrupamento(response.getOcorrencias15(i).getDsRecPermissaoAgrupamento());

            listaAvisoFormularios.add(avisoFormularios);
        }
        saida.setListaAvisoFormularios(listaAvisoFormularios);

        return saida;
    }

    /**
     * Popular recadastro beneficiarios.
     * 
     * @param saida
     *            the saida
     * @param response
     *            the response
     */
    private void popularRecadastroBeneficiarios(ImprimirAnexoSegundoContratoSaidaDTO saida,
        ImprimirAnexoSegundoContratoResponse response) {
        saida.setDsServicoRecadastro(response.getDsServicoRecadastro());
        saida.setQtModalidadeRecadastro(response.getQtModalidadeRecadastro());

        List<OcorrenciasModRecadastro> listaRecadastros = new ArrayList<OcorrenciasModRecadastro>();
        for (int i = 0; i < response.getOcorrencias5Count(); i++) {
            String cdModalidadeRecadastro = response.getOcorrencias5(i).getCdModalidadeRecadastro();

            if (isModalidadeValida(cdModalidadeRecadastro)) {
                listaRecadastros.add(new OcorrenciasModRecadastro(cdModalidadeRecadastro));
            }
        }

        saida.setListaModRecadastro(listaRecadastros);
    }

    /**
     * Popular beneficiarios.
     * 
     * @param saida
     *            the saida
     * @param response
     *            the response
     */
    private void popularBeneficiarios(ImprimirAnexoSegundoContratoSaidaDTO saida,
        ImprimirAnexoSegundoContratoResponse response) {
        saida.setCdServicoBeneficio(response.getCdServicoBeneficio());
        saida.setQtModalidadeBeneficio(response.getQtModalidadeBeneficio());
        saida.setDsBeneficioInformadoAgenciaConta(response.getDsBeneficioInformadoAgenciaConta());
        saida.setDsBeneficioTipoIdentificacaoBeneficio(response.getDsBeneficioTipoIdentificacaoBeneficio());
        saida.setDsBeneficioUtilizacaoCadastroOrganizacao(response.getDsBeneficioUtilizacaoCadastroOrganizacao());
        saida.setDsBeneficioUtilizacaoCadastroProcuradores(response.getDsBeneficioUtilizacaoCadastroProcuradores());
        saida.setDsBeneficioPossuiExpiracaoCredito(response.getDsBeneficioPossuiExpiracaoCredito());
        saida.setCdBeneficioQuantidadeDiasExpiracaoCredito(response.getCdBeneficioQuantidadeDiasExpiracaoCredito());
        saida.setDsBprTipoAcao(response.getDsBprTipoAcao());
        saida.setQtBprMesProvisorio(response.getQtBprMesProvisorio());
        saida.setCdBprIndicadorEmissaoAviso(response.getCdBprIndicadorEmissaoAviso());
        saida.setQtBprDiaAnteriorAviso(response.getQtBprDiaAnteriorAviso());
        saida.setQtBprDiaAnteriorVencimento(response.getQtBprDiaAnteriorVencimento());
        saida.setCdBprUtilizadoMensagemPersonalizada(response.getCdBprUtilizadoMensagemPersonalizada());
        saida.setDsBprDestino(response.getDsBprDestino());
        saida.setDsBprCadastroEnderecoUtilizado(response.getDsBprCadastroEnderecoUtilizado());

        List<OcorrenciasModBeneficios> listaBeneficios = new ArrayList<OcorrenciasModBeneficios>();
        for (int i = 0; i < response.getOcorrencias4Count(); i++) {
            String cdModalidadeBeneficio = response.getOcorrencias4(i).getCdModalidadeBeneficio();

            if (isModalidadeValida(cdModalidadeBeneficio)) {
                listaBeneficios.add(new OcorrenciasModBeneficios(cdModalidadeBeneficio));
            }
        }

        saida.setListaModBeneficio(listaBeneficios);
    }

    /**
     * Popular salarios.
     * 
     * @param saida
     *            the saida
     * @param response
     *            the response
     */
    private void popularSalarios(ImprimirAnexoSegundoContratoSaidaDTO saida,
        ImprimirAnexoSegundoContratoResponse response) {
        saida.setCdServicoSalarial(response.getCdServicoSalarial());
        saida.setQtModalidadeSalario(response.getQtModalidadeSalario());
        saida.setDsSalarioEmissaoAntecipadoCartao(response.getDsSalarioEmissaoAntecipadoCartao());
        saida.setCdSalarioQuantidadeLimiteSolicitacaoCartao(response.getCdSalarioQuantidadeLimiteSolicitacaoCartao());
        saida.setDsSalarioAberturaContaExpressa(response.getDsSalarioAberturaContaExpressa());

        List<OcorrenciasModSalarial> listaSalarios = new ArrayList<OcorrenciasModSalarial>();
        for (int i = 0; i < response.getOcorrencias3Count(); i++) {
            String cdModalidadeSalarial = response.getOcorrencias3(i).getCdModalidadeSalarial();

            if (isModalidadeValida(cdModalidadeSalarial)) {
                listaSalarios.add(new OcorrenciasModSalarial(cdModalidadeSalarial));
            }
        }

        saida.setListaModSalario(listaSalarios);
    }

    /**
     * Popular tributos.
     * 
     * @param saida
     *            the saida
     * @param response
     *            the response
     */
    private void popularTributos(ImprimirAnexoSegundoContratoSaidaDTO saida,
        ImprimirAnexoSegundoContratoResponse response) {
        saida.setCdServicoTributos(response.getCdServicoTributos());
        saida.setQtModalidadeTributos(response.getQtModalidadeTributos());
        saida.setDsTibutoAgendadoDebitoVeicular(response.getDsTibutoAgendadoDebitoVeicular());
        saida.setDsTdvTipoConsultaProposta(response.getDsTdvTipoConsultaProposta());
        saida.setDsTdvTipoTratamentoValor(response.getDsTdvTipoTratamentoValor());

        List<OcorrenciasModTributos> listaTributos = new ArrayList<OcorrenciasModTributos>();
        for (int i = 0; i < response.getOcorrencias2Count(); i++) {
            String cdModalidadeTributos = response.getOcorrencias2(i).getCdModalidadeTributos();

            if (isModalidadeValida(cdModalidadeTributos)) {
                listaTributos.add(new OcorrenciasModTributos(cdModalidadeTributos));
            }
        }

        saida.setListaModTributos(listaTributos);
    }

    /**
     * Popular fornecedor.
     * 
     * @param saida
     *            the saida
     * @param response
     *            the response
     */
    private void popularFornecedor(ImprimirAnexoSegundoContratoSaidaDTO saida,
        ImprimirAnexoSegundoContratoResponse response) {
        saida.setCdServicoFornecedor(response.getCdServicoFornecedor());
        saida.setQtModalidadeFornecedor(response.getQtModalidadeFornecedor());

        List<OcorrenciasModFornecedores> listaFornecedores = new ArrayList<OcorrenciasModFornecedores>();
        for (int i = 0; i < response.getOcorrencias1Count(); i++) {
            String dsModalidadeFornecedor = response.getOcorrencias1(i).getDsModalidadeFornecedor();

            if (isModalidadeValida(dsModalidadeFornecedor)) {
                listaFornecedores.add(new OcorrenciasModFornecedores(dsModalidadeFornecedor));
            }
        }

        saida.setListModFornecedores(listaFornecedores);
    }

    /**
     * Is modalidade valida.
     * 
     * @param modalidade
     *            the modalidade
     * @return true, if is modalidade valida
     */
    private boolean isModalidadeValida(String modalidade) {
        return modalidade != null && !modalidade.trim().equals("");
    }

    /**
     * Get: factoryAdapter.
     * 
     * @return factoryAdapter
     */
    public FactoryAdapter getFactoryAdapter() {
        return factoryAdapter;
    }

    /**
     * Set: factoryAdapter.
     * 
     * @param factoryAdapter
     *            the factory adapter
     */
    public void setFactoryAdapter(FactoryAdapter factoryAdapter) {
        this.factoryAdapter = factoryAdapter;
    }

}