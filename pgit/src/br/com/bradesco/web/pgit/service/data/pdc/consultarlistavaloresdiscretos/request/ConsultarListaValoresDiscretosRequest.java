/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultarlistavaloresdiscretos.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ConsultarListaValoresDiscretosRequest.
 * 
 * @version $Revision$ $Date$
 */
public class ConsultarListaValoresDiscretosRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _numeroCombo
     */
    private int _numeroCombo = 0;

    /**
     * keeps track of state for field: _numeroCombo
     */
    private boolean _has_numeroCombo;


      //----------------/
     //- Constructors -/
    //----------------/

    public ConsultarListaValoresDiscretosRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarlistavaloresdiscretos.request.ConsultarListaValoresDiscretosRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteNumeroCombo
     * 
     */
    public void deleteNumeroCombo()
    {
        this._has_numeroCombo= false;
    } //-- void deleteNumeroCombo() 

    /**
     * Returns the value of field 'numeroCombo'.
     * 
     * @return int
     * @return the value of field 'numeroCombo'.
     */
    public int getNumeroCombo()
    {
        return this._numeroCombo;
    } //-- int getNumeroCombo() 

    /**
     * Method hasNumeroCombo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNumeroCombo()
    {
        return this._has_numeroCombo;
    } //-- boolean hasNumeroCombo() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'numeroCombo'.
     * 
     * @param numeroCombo the value of field 'numeroCombo'.
     */
    public void setNumeroCombo(int numeroCombo)
    {
        this._numeroCombo = numeroCombo;
        this._has_numeroCombo = true;
    } //-- void setNumeroCombo(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ConsultarListaValoresDiscretosRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultarlistavaloresdiscretos.request.ConsultarListaValoresDiscretosRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultarlistavaloresdiscretos.request.ConsultarListaValoresDiscretosRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultarlistavaloresdiscretos.request.ConsultarListaValoresDiscretosRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarlistavaloresdiscretos.request.ConsultarListaValoresDiscretosRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
