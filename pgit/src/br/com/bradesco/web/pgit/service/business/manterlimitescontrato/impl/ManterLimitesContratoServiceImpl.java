/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/implementation.ftl,v $
 * $Id: implementation.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: implementation.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */
 
package br.com.bradesco.web.pgit.service.business.manterlimitescontrato.impl;

import static br.com.bradesco.web.pgit.utils.PgitUtil.verificaIntegerNulo;
import static br.com.bradesco.web.pgit.utils.PgitUtil.verificaLongNulo;
import static br.com.bradesco.web.pgit.utils.PgitUtil.verificaStringNula;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import br.com.bradesco.web.aq.application.error.i18n.MessageHelperUtils;
import br.com.bradesco.web.pgit.service.business.manterlimitescontrato.IManterLimitesContratoService;
import br.com.bradesco.web.pgit.service.business.manterlimitescontrato.bean.AlterarLimDIaOpePgitEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterlimitescontrato.bean.AlterarLimDIaOpePgitSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterlimitescontrato.bean.AlterarLimiteIntraPgitEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterlimitescontrato.bean.AlterarLimiteIntraPgitSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterlimitescontrato.bean.ConsultarLimDiarioUtilizadoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterlimitescontrato.bean.ConsultarLimDiarioUtilizadoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterlimitescontrato.bean.ConsultarLimiteContaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterlimitescontrato.bean.ConsultarLimiteContaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterlimitescontrato.bean.ConsultarLimiteHistoricoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterlimitescontrato.bean.ConsultarLimiteHistoricoOcorrenciasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterlimitescontrato.bean.ConsultarLimiteHistoricoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterlimitescontrato.bean.ConsultarLimiteIntraPgitEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterlimitescontrato.bean.ConsultarLimiteIntraPgitSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterlimitescontrato.bean.ConsultarListaServLimDiaIndvEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterlimitescontrato.bean.ConsultarListaServLimDiaIndvSaidaDTO;
import br.com.bradesco.web.pgit.service.data.pdc.FactoryAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.alterarlimdiaopepgit.request.AlterarLimDIaOpePgitRequest;
import br.com.bradesco.web.pgit.service.data.pdc.alterarlimdiaopepgit.response.AlterarLimDIaOpePgitResponse;
import br.com.bradesco.web.pgit.service.data.pdc.alterarlimiteintrapgit.request.AlterarLimiteIntraPgitRequest;
import br.com.bradesco.web.pgit.service.data.pdc.alterarlimiteintrapgit.response.AlterarLimiteIntraPgitResponse;
import br.com.bradesco.web.pgit.service.data.pdc.consultarlimdiarioutilizado.request.ConsultarLimDiarioUtilizadoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarlimdiarioutilizado.response.ConsultarLimDiarioUtilizadoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.consultarlimiteconta.request.ConsultarLimiteContaRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarlimiteconta.response.ConsultarLimiteContaResponse;
import br.com.bradesco.web.pgit.service.data.pdc.consultarlimitehistorico.request.ConsultarLimiteHistoricoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarlimitehistorico.response.ConsultarLimiteHistoricoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.consultarlimiteintrapgit.request.ConsultarLimiteIntraPgitRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarlimiteintrapgit.response.ConsultarLimiteIntraPgitResponse;
import br.com.bradesco.web.pgit.service.data.pdc.consultarlistaservlimdiaindv.request.ConsultarListaServLimDiaIndvRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarlistaservlimdiaindv.response.ConsultarListaServLimDiaIndvResponse;
import br.com.bradesco.web.pgit.utils.PgitUtil;
import br.com.bradesco.web.pgit.view.converters.FormatarData;
import br.com.bradesco.web.pgit.view.utils.PgitFacesUtils;


/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Implementa��o do adaptador: ManterLimitesContrato
 * </p>
 * 
 * @comment C�DIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public class ManterLimitesContratoServiceImpl implements IManterLimitesContratoService {

	/** Atributo factoryAdapter. */
	private FactoryAdapter factoryAdapter;

	/**
	 * Get: factoryAdapter.
	 *
	 * @return factoryAdapter
	 */
	public FactoryAdapter getFactoryAdapter() {
		return factoryAdapter;
	}

	/**
	 * Set: factoryAdapter.
	 *
	 * @param factoryAdapter the factory adapter
	 */
	public void setFactoryAdapter(FactoryAdapter factoryAdapter) {
		this.factoryAdapter = factoryAdapter;
	}
    
    /**
     * Construtor.
     */
    public ManterLimitesContratoServiceImpl() {
        // TODO: Implementa��o
    }
    
    /**
     * M�todo de exemplo.
     *
     * @see br.com.bradesco.web.pgit.service.business.manterlimitescontrato.IManterLimitesContrato#sampleManterLimitesContrato()
     */
    public void sampleManterLimitesContrato() {
        // TODO: Implementa�ao
    }
    
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.manterlimitescontrato.IManterLimitesContratoService#consultarLimiteIntraPgit(br.com.bradesco.web.pgit.service.business.manterlimitescontrato.bean.ConsultarLimiteIntraPgitEntradaDTO)
	 */
	public List<ConsultarLimiteIntraPgitSaidaDTO> consultarLimiteIntraPgit(ConsultarLimiteIntraPgitEntradaDTO entrada){
		List<ConsultarLimiteIntraPgitSaidaDTO> listaSaida = new ArrayList<ConsultarLimiteIntraPgitSaidaDTO>();
		ConsultarLimiteIntraPgitRequest request = new ConsultarLimiteIntraPgitRequest();
		ConsultarLimiteIntraPgitResponse response = new ConsultarLimiteIntraPgitResponse();

		request.setNrOcorrencias(50);
		request.setCdPessoaJuridicaNegocio(PgitUtil.verificaLongNulo(entrada.getCdPessoaJuridicaNegocio()));
		request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(entrada.getCdTipoContratoNegocio()));
		request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(entrada.getNrSequenciaContratoNegocio()));
		request.setCdCorpoCpfCnpj(PgitUtil.verificaLongNulo(entrada.getCdCorpoCpfCnpj()));
		request.setCdControleCpfCnpj(PgitUtil.verificaIntegerNulo(entrada.getCdControleCpfCnpj()));
		request.setCdDigitoCpfCnpj(PgitUtil.verificaIntegerNulo(entrada.getCdDigitoCpfCnpj()));
		request.setCdFinalidade(PgitUtil.verificaIntegerNulo(entrada.getCdFinalidade()));
		request.setCdBanco(PgitUtil.verificaIntegerNulo(entrada.getCdBanco()));
		request.setCdAgencia(PgitUtil.verificaIntegerNulo(entrada.getCdAgencia()));
		request.setCdDigitoAgencia(PgitUtil.verificaIntegerNulo(entrada.getCdDigitoAgencia()));
		request.setCdConta(PgitUtil.verificaLongNulo(entrada.getCdConta()));
		request.setCdDigitoConta(PgitUtil.verificaStringNula(entrada.getCdDigitoConta()));
		request.setCdTipoConta(PgitUtil.verificaIntegerNulo(entrada.getCdTipoConta()));

		response = getFactoryAdapter().getConsultarLimiteIntraPgitPDCAdapter().invokeProcess(request);

		for(int i=0;i<response.getOcorrenciasCount();i++){
			ConsultarLimiteIntraPgitSaidaDTO saida = new ConsultarLimiteIntraPgitSaidaDTO();
			saida.setCodMensagem(response.getCodMensagem());
			saida.setMensagem(response.getMensagem());
			saida.setNumeroLinhas(response.getNumeroLinhas());
			saida.setCdPessoaJuridicaVinculo(response.getOcorrencias(i).getCdPessoaJuridicaVinculo());
			saida.setCdTipoContratoVinculo(response.getOcorrencias(i).getCdTipoContratoVinculo());
			saida.setNrSequenciaContratoVinculo(response.getOcorrencias(i).getNrSequenciaContratoVinculo());
			saida.setCdTipoVinculoContrato(response.getOcorrencias(i).getCdTipoVinculoContrato());
			saida.setCdCpfCnpj(response.getOcorrencias(i).getCdCpfCnpj());
			saida.setNmParticipante(response.getOcorrencias(i).getNmParticipante());
			saida.setCdBanco(response.getOcorrencias(i).getCdBanco());
			saida.setDsBanco(response.getOcorrencias(i).getDsBanco());
			saida.setCdAgencia(response.getOcorrencias(i).getCdAgencia());
			saida.setCdDigitoAgencia(response.getOcorrencias(i).getCdDigitoAgencia());
			saida.setDsAgencia(response.getOcorrencias(i).getDsAgencia());
			saida.setCdConta(response.getOcorrencias(i).getCdConta());
			saida.setCdDigitoConta(response.getOcorrencias(i).getCdDigitoConta());
			saida.setCdTipoConta(response.getOcorrencias(i).getCdTipoConta());
			saida.setDsTipoConta(response.getOcorrencias(i).getDsTipoConta());
			saida.setDsTipoDebito(response.getOcorrencias(i).getDsTipoDebito());
			saida.setDsTipoTarifa(response.getOcorrencias(i).getDsTipoTarifa());
			saida.setDsTipoEstorno(response.getOcorrencias(i).getDsTipoEstorno());
			saida.setDsAlternativa(response.getOcorrencias(i).getDsAlternativa());
			saida.setCdSituacao(response.getOcorrencias(i).getCdSituacao());
			saida.setDsSituacao(response.getOcorrencias(i).getDsSituacao());
			saida.setDsDevolucao(response.getOcorrencias(i).getDsDevolucao());
			saida.setVlAdicionalContratoConta(response.getOcorrencias(i).getVlAdicionalContratoConta());
			saida.setDtInicioValorAdicional(response.getOcorrencias(i).getDtInicioValorAdicional());
			saida.setDtFimValorAdicional(response.getOcorrencias(i).getDtFimValorAdicional());
			saida.setVlAdcContrCtaTed(response.getOcorrencias(i).getVlAdcContrCtaTed());
			saida.setDtIniVlrAdicionalTed(response.getOcorrencias(i).getDtIniVlrAdicionalTed());
			saida.setDtFimVlrAdicionalTed(response.getOcorrencias(i).getDtFimVlrAdicionalTed());

			saida.setBancoFormatado(PgitUtil.formatBanco(saida.getCdBanco(), saida.getDsBanco(), false));
			saida.setAgenciaFormatada(PgitUtil.formatAgencia(saida.getCdAgencia(), null , saida.getDsAgencia(), false));
			
			StringBuffer fieldFinalidade = new StringBuffer();
			
			if (saida.getDsDevolucao().equals("SIM")) {
				fieldFinalidade.append(MessageHelperUtils.getI18nMessage("conContasManterContrato_grid_devolu��o"));
			}

			if (saida.getDsTipoEstorno().equals("SIM")) {
				if (fieldFinalidade.length() > 0) {
					fieldFinalidade.append(" - ");
				}

				fieldFinalidade.append(MessageHelperUtils.getI18nMessage("conContasManterContrato_grid_estorno"));
			}
			
//			saida.setDsFinalidade(fieldFinalidade.toString());
			
			saida.setContaFormatada(PgitUtil.formatConta(saida.getCdConta(), saida.getCdDigitoConta(), false));
			
			if(PgitUtil.verificaZero(saida.getVlAdicionalContratoConta()) != null){
			    saida.setPrazoValidade(PgitUtil.concatenarCampos(FormatarData.formatarData(saida.getDtInicioValorAdicional()), FormatarData.formatarData(saida.getDtFimValorAdicional())));
			}else{
			    saida.setPrazoValidade("");
			}
			
			if(PgitUtil.verificaZero(saida.getVlAdcContrCtaTed()) != null){
			    saida.setPrazoValidadeTED(PgitUtil.concatenarCampos(FormatarData.formatarData(saida.getDtIniVlrAdicionalTed()), FormatarData.formatarData(saida.getDtFimVlrAdicionalTed())));
			}else{
			    saida.setPrazoValidadeTED("");
			}
			
			listaSaida.add(saida);
		}

		return listaSaida;
	}
	
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.manterlimitescontrato.IManterLimitesContratoService#consultarListaServLimDiaIndv(br.com.bradesco.web.pgit.service.business.manterlimitescontrato.bean.ConsultarListaServLimDiaIndvEntradaDTO)
	 */
	public List<ConsultarListaServLimDiaIndvSaidaDTO> consultarListaServLimDiaIndv(ConsultarListaServLimDiaIndvEntradaDTO entrada){
		List<ConsultarListaServLimDiaIndvSaidaDTO> listaSaida = new ArrayList<ConsultarListaServLimDiaIndvSaidaDTO>();
		ConsultarListaServLimDiaIndvRequest request = new ConsultarListaServLimDiaIndvRequest();
		ConsultarListaServLimDiaIndvResponse response = new ConsultarListaServLimDiaIndvResponse();

		request.setNrOcorrencias(50);
		request.setCdpessoaJuridicaContrato(PgitUtil.verificaLongNulo(entrada.getCdpessoaJuridicaContrato()));
		request.setCdTipoContrato(PgitUtil.verificaIntegerNulo(entrada.getCdTipoContrato()));
		request.setNrSequenciaContrato(PgitUtil.verificaLongNulo(entrada.getNrSequenciaContrato()));
		request.setCdProdutoServicoOperacao(PgitUtil.verificaIntegerNulo(entrada.getCdProdutoServicoOperacao()));
		request.setCdProdutoOperacaoRelacionado(PgitUtil.verificaIntegerNulo(entrada.getCdProdutoOperacaoRelacionado()));

		response = getFactoryAdapter().getConsultarListaServLimDiaIndvPDCAdapter().invokeProcess(request);

		for(int i=0;i<response.getOcorrenciasCount();i++){
			ConsultarListaServLimDiaIndvSaidaDTO saida = new ConsultarListaServLimDiaIndvSaidaDTO();
			saida.setCodMensagem(response.getCodMensagem());
			saida.setMensagem(response.getMensagem());
			saida.setNumeroLinhas(response.getNumeroLinhas());
			saida.setCdParametroTela(response.getOcorrencias(i).getCdParametroTela());
			saida.setCdProdutoServicoOperacao(response.getOcorrencias(i).getCdProdutoServicoOperacao());
			//saida.setDsProdutoServicoOperacao(response.getOcorrencias(i).getDsProdutoServicoOperacao());
			saida.setCdProdutoOperacaoRelacionado(response.getOcorrencias(i).getCdProdutoOperacaoRelacionado());
			//saida.setDsProdutoOperacaoRelacionado(response.getOcorrencias(i).getDsProdutoOperacaoRelacionado());
			saida.setCdRelacionamentoProduto(response.getOcorrencias(i).getCdRelacionamentoProduto());
			saida.setDtInclusaoServico(response.getOcorrencias(i).getDtInclusaoServico());
			saida.setDsServico(response.getOcorrencias(i).getDsServico());
			saida.setDsMadalidade(response.getOcorrencias(i).getDsMadalidade());
			saida.setVlLimiteIndvdPagamento(response.getOcorrencias(i).getVlLimiteIndvdPagamento());
			saida.setVlLimiteDiaPagamento(response.getOcorrencias(i).getVlLimiteDiaPagamento());

			listaSaida.add(saida);
		}
		
		return listaSaida;
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.manterlimitescontrato.IManterLimitesContratoService#alterarLimiteIntraPgit(br.com.bradesco.web.pgit.service.business.manterlimitescontrato.bean.AlterarLimiteIntraPgitEntradaDTO)
	 */
	public AlterarLimiteIntraPgitSaidaDTO alterarLimiteIntraPgit(AlterarLimiteIntraPgitEntradaDTO entrada){
		AlterarLimiteIntraPgitSaidaDTO saida = new AlterarLimiteIntraPgitSaidaDTO();
		AlterarLimiteIntraPgitRequest request = new AlterarLimiteIntraPgitRequest();
		AlterarLimiteIntraPgitResponse response = new AlterarLimiteIntraPgitResponse();

		request.setCdpessoaJuridicaContrato(PgitUtil.verificaLongNulo(entrada.getCdpessoaJuridicaContrato()));
		request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(entrada.getCdTipoContratoNegocio()));
		request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(entrada.getNrSequenciaContratoNegocio()));
		request.setCdPessoaJuridicaVinculo(PgitUtil.verificaLongNulo(entrada.getCdPessoaJuridicaVinculo()));
		request.setCdTipoContratoVinculo(PgitUtil.verificaIntegerNulo(entrada.getCdTipoContratoVinculo()));
		request.setNrSequenciaContratoVinculo(PgitUtil.verificaLongNulo(entrada.getNrSequenciaContratoVinculo()));
		request.setCdTipoVincContrato(PgitUtil.verificaIntegerNulo(entrada.getCdTipoVincContrato()));
		request.setVlAdicionalContratoConta(PgitUtil.verificaBigDecimalNulo(entrada.getVlAdicionalContratoConta()));
		request.setDtInicioValorAdicional(PgitUtil.verificaStringNula(entrada.getDtInicioValorAdicional()));
		request.setDtFimValorAdicional(PgitUtil.verificaStringNula(entrada.getDtFimValorAdicional()));
		request.setVlAdicContrCtaTed(PgitUtil.verificaBigDecimalNulo(entrada.getVlAdicContrCtaTed()));
		request.setDtIniVlrAdicionalTed(PgitUtil.verificaStringNula(entrada.getDtIniVlrAdicionalTed()));
		request.setDtFimVlrAdicionalTed(PgitUtil.verificaStringNula(entrada.getDtFimVlrAdicionalTed()));
		
		response = getFactoryAdapter().getAlterarLimiteIntraPgitPDCAdapter().invokeProcess(request);

		saida.setCodMensagem(response.getCodMensagem());
		saida.setMensagem(response.getMensagem());

		return saida;
	}
	
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.manterlimitescontrato.IManterLimitesContratoService#alterarLimDIaOpePgit(br.com.bradesco.web.pgit.service.business.manterlimitescontrato.bean.AlterarLimDIaOpePgitEntradaDTO)
	 */
	public AlterarLimDIaOpePgitSaidaDTO alterarLimDIaOpePgit(AlterarLimDIaOpePgitEntradaDTO entrada){
		AlterarLimDIaOpePgitSaidaDTO saida = new AlterarLimDIaOpePgitSaidaDTO();
		AlterarLimDIaOpePgitRequest request = new AlterarLimDIaOpePgitRequest();
		AlterarLimDIaOpePgitResponse response = new AlterarLimDIaOpePgitResponse();

		request.setCdpessoaJuridicaContrato(PgitUtil.verificaLongNulo(entrada.getCdpessoaJuridicaContrato()));
		request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(entrada.getCdTipoContratoNegocio()));
		request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(entrada.getNrSequenciaContratoNegocio()));
		request.setCdProdutoServicoOperacao(PgitUtil.verificaIntegerNulo(entrada.getCdProdutoServicoOperacao()));
		request.setCdProdutoOperacaoRelacionado(PgitUtil.verificaIntegerNulo(entrada.getCdProdutoOperacaoRelacionado()));
		request.setVlLimiteDiaPagamento(PgitUtil.verificaBigDecimalNulo(entrada.getVlLimiteDiaPagamento()));
		request.setVlLimiteIndvdPagamento(PgitUtil.verificaBigDecimalNulo(entrada.getVlLimiteIndvdPagamento()));
		request.setCdusuario(PgitUtil.verificaStringNula(entrada.getCdusuario()));
		request.setCdUsuarioExterno(PgitUtil.verificaStringNula(entrada.getCdUsuarioExterno()));
		request.setCdCanal(PgitUtil.verificaIntegerNulo(entrada.getCdCanal()));
		request.setNmOperacaoFluxo(PgitUtil.verificaStringNula(entrada.getNmOperacaoFluxo()));
		request.setCdEmpresaOperante(PgitUtil.verificaLongNulo(entrada.getCdEmpresaOperante()));
		request.setCdDependenteOperante(PgitUtil.verificaIntegerNulo(entrada.getCdDependenteOperante()));

		response = getFactoryAdapter().getAlterarLimDIaOpePgitPDCAdapter().invokeProcess(request);

		saida.setCodMensagem(response.getCodMensagem());
		saida.setMensagem(response.getMensagem());

		return saida;
	}
	
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.manterlimitescontrato.IManterLimitesContratoService#consultarLimDiarioUtilizado(br.com.bradesco.web.pgit.service.business.manterlimitescontrato.bean.ConsultarLimDiarioUtilizadoEntradaDTO)
	 */
	public List<ConsultarLimDiarioUtilizadoSaidaDTO> consultarLimDiarioUtilizado(ConsultarLimDiarioUtilizadoEntradaDTO entrada){
		List<ConsultarLimDiarioUtilizadoSaidaDTO> listaSaida = new ArrayList<ConsultarLimDiarioUtilizadoSaidaDTO>();
		ConsultarLimDiarioUtilizadoRequest request = new ConsultarLimDiarioUtilizadoRequest();
		ConsultarLimDiarioUtilizadoResponse response = new ConsultarLimDiarioUtilizadoResponse();

		request.setNrOcorrencias(50);
		request.setCdpessoaJuridicaContrato(PgitUtil.verificaLongNulo(entrada.getCdpessoaJuridicaContrato()));
		request.setCdTipoContrato(PgitUtil.verificaIntegerNulo(entrada.getCdTipoContrato()));
		request.setNrSequenciaContrato(PgitUtil.verificaLongNulo(entrada.getNrSequenciaContrato()));
		request.setCdProdutoServicoOperacao(PgitUtil.verificaIntegerNulo(entrada.getCdProdutoServicoOperacao()));
		request.setCdProdutoOperacaoRelacionado(PgitUtil.verificaIntegerNulo(entrada.getCdProdutoOperacaoRelacionado()));

		response = getFactoryAdapter().getConsultarLimDiarioUtilizadoPDCAdapter().invokeProcess(request);

		for(int i=0;i<response.getOcorrenciasCount();i++){
			ConsultarLimDiarioUtilizadoSaidaDTO saida = new ConsultarLimDiarioUtilizadoSaidaDTO();
			saida.setCodMensagem(response.getCodMensagem());
			saida.setMensagem(response.getMensagem());			
			saida.setDtAgendamentoPagamento(response.getOcorrencias(i).getDtAgendamentoPagamento());
			saida.setVlLimiteDiaUtilizado(response.getOcorrencias(i).getVlLimiteDiaUtilizado());

			saida.setDtAgendamentoFormatada(FormatarData.formatarData(saida.getDtAgendamentoPagamento()));
			
			listaSaida.add(saida);
		}

		return listaSaida;
	}
	

	public ConsultarLimiteContaSaidaDTO consultarLimiteConta(ConsultarLimiteContaEntradaDTO entrada){
		ConsultarLimiteContaRequest request = new ConsultarLimiteContaRequest();

		request.setCdpessoaJuridicaContrato(PgitUtil.verificaLongNulo(entrada.getCdpessoaJuridicaContrato()));
		request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(entrada.getCdTipoContratoNegocio()));
		request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(entrada.getNrSequenciaContratoNegocio()));
		request.setCdPessoaJuridicaVinculo(PgitUtil.verificaLongNulo(entrada.getCdPessoaJuridicaVinculo()));
		request.setCdTipoContratoVinculo(PgitUtil.verificaIntegerNulo(entrada.getCdTipoContratoVinculo()));
		request.setNrSequenciaContratoVinculo(PgitUtil.verificaLongNulo(entrada.getNrSequenciaContratoVinculo()));
		request.setCdTipoVincContrato(PgitUtil.verificaIntegerNulo(entrada.getCdTipoVincContrato()));
    
		ConsultarLimiteContaResponse response = getFactoryAdapter()
				.getConsultarLimiteContaPDCAdapter().invokeProcess(request);
    
		ConsultarLimiteContaSaidaDTO saida = new ConsultarLimiteContaSaidaDTO();
		saida.setCodMensagem(response.getCodMensagem());
		saida.setMensagem(response.getMensagem());
		saida.setCdUsuarioInclusao(response.getCdUsuarioInclusao());
		saida.setCdUsuarioInclusaoExterno(response.getCdUsuarioInclusaoExterno());
		saida.setCdOperacaoCanalInclusao(response.getCdOperacaoCanalInclusao());
		saida.setCdTipoCanalInclusao(response.getCdTipoCanalInclusao());
		saida.setCdUsuarioManutencao(response.getCdUsuarioManutencao());
		saida.setCdUsuarioManutencaoExterno(response.getCdUsuarioManutencaoExterno());
		saida.setCdOperacaoCanalManutencao(response.getCdOperacaoCanalManutencao());
		saida.setCdTipoCanalManutencao(response.getCdTipoCanalManutencao());
		saida.setDsTipoCanalInclusao(response.getDsTipoCanalInclusao());
		saida.setDsTipoCanalManutencao(response.getDsTipoCanalManutencao());

		saida.setHrInclusaoRegistro(FormatarData.formatarDataTrilha(response.getHrInclusaoRegistro()));
		saida.setHrManutencaoRegistro(FormatarData.formatarDataTrilha(response.getHrManutencaoRegistro()));

		
		return saida;
    }
    

    public ConsultarLimiteHistoricoSaidaDTO consultarLimiteHistorico(ConsultarLimiteHistoricoEntradaDTO entrada){
    	ConsultarLimiteHistoricoRequest request = new ConsultarLimiteHistoricoRequest();
    	
    	request.setMaxOcorrencias(50);
    	request.setCdpessoaJuridicaContrato(verificaLongNulo(entrada.getCdpessoaJuridicaContrato()));
    	request.setCdTipoContratoNegocio(verificaIntegerNulo(entrada.getCdTipoContratoNegocio()));
    	request.setNrSequenciaContratoNegocio(verificaLongNulo(entrada.getNrSequenciaContratoNegocio()));
    	request.setCdPessoaJuridicaVinculo(verificaLongNulo(entrada.getCdPessoaJuridicaVinculo()));
    	request.setCdTipoContratoVinculo(verificaIntegerNulo(entrada.getCdTipoContratoVinculo()));
    	request.setNrSequenciaContratoVinculo(verificaLongNulo(entrada.getNrSequenciaContratoVinculo()));
    	request.setCdTipoVincContrato(verificaIntegerNulo(entrada.getCdTipoVincContrato()));
    	request.setDtInicioManutencao(verificaStringNula(entrada.getDtInicioManutencao()));
        request.setDtFimManutencao(verificaStringNula(entrada.getDtFimManutencao()));

		ConsultarLimiteHistoricoResponse response = getFactoryAdapter()
				.getConsultarLimiteHistoricoPDCAdapter().invokeProcess(request);
		
		SimpleDateFormat formato1 = new SimpleDateFormat("yyyy-MM-dd-HH.mm.ss");
		SimpleDateFormat formato2 = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		ConsultarLimiteHistoricoSaidaDTO saida = new ConsultarLimiteHistoricoSaidaDTO();
    	
    	saida.setCodMensagem(response.getCodMensagem());
    	saida.setMensagem(response.getMensagem());
    	saida.setNumeroConsulta(response.getNumeroConsulta());

        List<ConsultarLimiteHistoricoOcorrenciasSaidaDTO> listOcorrencias = 
        	new ArrayList<ConsultarLimiteHistoricoOcorrenciasSaidaDTO>(response.getOcorrenciasCount());
        
        for(int i=0;i<response.getOcorrenciasCount();i++){
        	ConsultarLimiteHistoricoOcorrenciasSaidaDTO ocorrencia = new ConsultarLimiteHistoricoOcorrenciasSaidaDTO();
			
        	ocorrencia.setVlAdicionalContratoConta(response.getOcorrencias(i).getVlAdicionalContratoConta());
            ocorrencia.setDtInicioValorAdicional(response.getOcorrencias(i).getDtInicioValorAdicional());
            ocorrencia.setDtFimValorAdicional(response.getOcorrencias(i).getDtFimValorAdicional());
            ocorrencia.setVlAdicContrCtaTed(response.getOcorrencias(i).getVlAdicContrCtaTed());
            ocorrencia.setDtIniVlrAdicionalTed(response.getOcorrencias(i).getDtIniVlrAdicionalTed());
            ocorrencia.setDtFimVlrAdicionalTed(response.getOcorrencias(i).getDtFimVlrAdicionalTed());
            ocorrencia.setCdUsuarioInclusao(response.getOcorrencias(i).getCdUsuarioInclusao());
            ocorrencia.setCdUsuarioInclusaoExterno(response.getOcorrencias(i).getCdUsuarioInclusaoExterno());
            ocorrencia.setHrInclusaoRegistro(response.getOcorrencias(i).getHrInclusaoRegistro());
            ocorrencia.setCdOperacaoCanalInclusao(response.getOcorrencias(i).getCdOperacaoCanalInclusao());
            ocorrencia.setCdTipoCanalInclusao(response.getOcorrencias(i).getCdTipoCanalInclusao());
            ocorrencia.setCdUsuarioManutencao(response.getOcorrencias(i).getCdUsuarioManutencao());
            ocorrencia.setCdUsuarioManutencaoExterno(response.getOcorrencias(i).getCdUsuarioManutencaoExterno());
            ocorrencia.setHrManutencaoRegistro(response.getOcorrencias(i).getHrManutencaoRegistro());
            ocorrencia.setCdOperacaoCanalManutencao(response.getOcorrencias(i).getCdOperacaoCanalManutencao());
            ocorrencia.setCdTipoCanalManutencao(response.getOcorrencias(i).getCdTipoCanalManutencao());
            ocorrencia.setDsTipoCanalInclusao(response.getOcorrencias(i).getDsTipoCanalInclusao());
            ocorrencia.setDsTipoCanalManutencao(response.getOcorrencias(i).getDsTipoCanalManutencao());
        	
            try {
            	String dataHora = formato2.format(formato1.parse(response.getOcorrencias(i).getHrInclusaoRegistro()));

            	ocorrencia.setHora(dataHora.substring(10));
            	ocorrencia.setData(dataHora.substring(0, 10));
            	ocorrencia.setHrInclusaoRegistro(dataHora);
            	
            	String dataHoraManutencao = formato2.format(formato1.parse(
            			response.getOcorrencias(i).getHrManutencaoRegistro()));
            	
            	ocorrencia.setHoraManutencaoRegistro(dataHoraManutencao.substring(10));
            	ocorrencia.setDataManutencaoRegistro(dataHoraManutencao.substring(0, 10));
            	ocorrencia.setHrManutencaoRegistro(dataHoraManutencao);


            } catch (IndexOutOfBoundsException e) {

            } catch (java.text.ParseException e) {
            	ocorrencia.setHora("");
            	ocorrencia.setData("");
            	ocorrencia.setHrInclusaoRegistro("");
            	ocorrencia.setHrManutencaoRegistro("");
            }
			listOcorrencias.add(ocorrencia);
		}
        saida.setOcorrencias(listOcorrencias);
        
        return saida;
    }
}

