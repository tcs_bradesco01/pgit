/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listartiporetirada.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ListarTipoRetiradaRequest.
 * 
 * @version $Revision$ $Date$
 */
public class ListarTipoRetiradaRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _maxOcorrencias
     */
    private int _maxOcorrencias = 0;

    /**
     * keeps track of state for field: _maxOcorrencias
     */
    private boolean _has_maxOcorrencias;

    /**
     * Field _cdpessoaJuridicaContrato
     */
    private long _cdpessoaJuridicaContrato = 0;

    /**
     * keeps track of state for field: _cdpessoaJuridicaContrato
     */
    private boolean _has_cdpessoaJuridicaContrato;

    /**
     * Field _cdTipoContratoNegocio
     */
    private int _cdTipoContratoNegocio = 0;

    /**
     * keeps track of state for field: _cdTipoContratoNegocio
     */
    private boolean _has_cdTipoContratoNegocio;

    /**
     * Field _nrSequenciaContratoNegocio
     */
    private long _nrSequenciaContratoNegocio = 0;

    /**
     * keeps track of state for field: _nrSequenciaContratoNegocio
     */
    private boolean _has_nrSequenciaContratoNegocio;

    /**
     * Field _cdControlePagamento
     */
    private java.lang.String _cdControlePagamento;

    /**
     * Field _cdIndentificadorSeqPagamento
     */
    private java.lang.String _cdIndentificadorSeqPagamento;

    /**
     * Field _cdProdutoServicoOperacao
     */
    private int _cdProdutoServicoOperacao = 0;

    /**
     * keeps track of state for field: _cdProdutoServicoOperacao
     */
    private boolean _has_cdProdutoServicoOperacao;

    /**
     * Field _cdProdutoOperacaoRelacionado
     */
    private int _cdProdutoOperacaoRelacionado = 0;

    /**
     * keeps track of state for field: _cdProdutoOperacaoRelacionado
     */
    private boolean _has_cdProdutoOperacaoRelacionado;

    /**
     * Field _cdBancoPagador
     */
    private int _cdBancoPagador = 0;

    /**
     * keeps track of state for field: _cdBancoPagador
     */
    private boolean _has_cdBancoPagador;

    /**
     * Field _cdAgenciaBancariaPagador
     */
    private int _cdAgenciaBancariaPagador = 0;

    /**
     * keeps track of state for field: _cdAgenciaBancariaPagador
     */
    private boolean _has_cdAgenciaBancariaPagador;

    /**
     * Field _cdDigitoAgenciaPagador
     */
    private java.lang.String _cdDigitoAgenciaPagador;

    /**
     * Field _cdContaBancariaPagador
     */
    private long _cdContaBancariaPagador = 0;

    /**
     * keeps track of state for field: _cdContaBancariaPagador
     */
    private boolean _has_cdContaBancariaPagador;

    /**
     * Field _cdDigitoContaPagador
     */
    private java.lang.String _cdDigitoContaPagador;

    /**
     * Field _cdBancoFavorecido
     */
    private int _cdBancoFavorecido = 0;

    /**
     * keeps track of state for field: _cdBancoFavorecido
     */
    private boolean _has_cdBancoFavorecido;

    /**
     * Field _cdAgenciaFavorecido
     */
    private int _cdAgenciaFavorecido = 0;

    /**
     * keeps track of state for field: _cdAgenciaFavorecido
     */
    private boolean _has_cdAgenciaFavorecido;

    /**
     * Field _cdDigitoAgenciaFavorecido
     */
    private java.lang.String _cdDigitoAgenciaFavorecido;

    /**
     * Field _cdContaFavorecido
     */
    private long _cdContaFavorecido = 0;

    /**
     * keeps track of state for field: _cdContaFavorecido
     */
    private boolean _has_cdContaFavorecido;

    /**
     * Field _cdDigitoContaFavorecido
     */
    private java.lang.String _cdDigitoContaFavorecido;

    /**
     * Field _dtPagamentoDe
     */
    private java.lang.String _dtPagamentoDe;

    /**
     * Field _dtPagamentoAte
     */
    private java.lang.String _dtPagamentoAte;


      //----------------/
     //- Constructors -/
    //----------------/

    public ListarTipoRetiradaRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listartiporetirada.request.ListarTipoRetiradaRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdAgenciaBancariaPagador
     * 
     */
    public void deleteCdAgenciaBancariaPagador()
    {
        this._has_cdAgenciaBancariaPagador= false;
    } //-- void deleteCdAgenciaBancariaPagador() 

    /**
     * Method deleteCdAgenciaFavorecido
     * 
     */
    public void deleteCdAgenciaFavorecido()
    {
        this._has_cdAgenciaFavorecido= false;
    } //-- void deleteCdAgenciaFavorecido() 

    /**
     * Method deleteCdBancoFavorecido
     * 
     */
    public void deleteCdBancoFavorecido()
    {
        this._has_cdBancoFavorecido= false;
    } //-- void deleteCdBancoFavorecido() 

    /**
     * Method deleteCdBancoPagador
     * 
     */
    public void deleteCdBancoPagador()
    {
        this._has_cdBancoPagador= false;
    } //-- void deleteCdBancoPagador() 

    /**
     * Method deleteCdContaBancariaPagador
     * 
     */
    public void deleteCdContaBancariaPagador()
    {
        this._has_cdContaBancariaPagador= false;
    } //-- void deleteCdContaBancariaPagador() 

    /**
     * Method deleteCdContaFavorecido
     * 
     */
    public void deleteCdContaFavorecido()
    {
        this._has_cdContaFavorecido= false;
    } //-- void deleteCdContaFavorecido() 

    /**
     * Method deleteCdProdutoOperacaoRelacionado
     * 
     */
    public void deleteCdProdutoOperacaoRelacionado()
    {
        this._has_cdProdutoOperacaoRelacionado= false;
    } //-- void deleteCdProdutoOperacaoRelacionado() 

    /**
     * Method deleteCdProdutoServicoOperacao
     * 
     */
    public void deleteCdProdutoServicoOperacao()
    {
        this._has_cdProdutoServicoOperacao= false;
    } //-- void deleteCdProdutoServicoOperacao() 

    /**
     * Method deleteCdTipoContratoNegocio
     * 
     */
    public void deleteCdTipoContratoNegocio()
    {
        this._has_cdTipoContratoNegocio= false;
    } //-- void deleteCdTipoContratoNegocio() 

    /**
     * Method deleteCdpessoaJuridicaContrato
     * 
     */
    public void deleteCdpessoaJuridicaContrato()
    {
        this._has_cdpessoaJuridicaContrato= false;
    } //-- void deleteCdpessoaJuridicaContrato() 

    /**
     * Method deleteMaxOcorrencias
     * 
     */
    public void deleteMaxOcorrencias()
    {
        this._has_maxOcorrencias= false;
    } //-- void deleteMaxOcorrencias() 

    /**
     * Method deleteNrSequenciaContratoNegocio
     * 
     */
    public void deleteNrSequenciaContratoNegocio()
    {
        this._has_nrSequenciaContratoNegocio= false;
    } //-- void deleteNrSequenciaContratoNegocio() 

    /**
     * Returns the value of field 'cdAgenciaBancariaPagador'.
     * 
     * @return int
     * @return the value of field 'cdAgenciaBancariaPagador'.
     */
    public int getCdAgenciaBancariaPagador()
    {
        return this._cdAgenciaBancariaPagador;
    } //-- int getCdAgenciaBancariaPagador() 

    /**
     * Returns the value of field 'cdAgenciaFavorecido'.
     * 
     * @return int
     * @return the value of field 'cdAgenciaFavorecido'.
     */
    public int getCdAgenciaFavorecido()
    {
        return this._cdAgenciaFavorecido;
    } //-- int getCdAgenciaFavorecido() 

    /**
     * Returns the value of field 'cdBancoFavorecido'.
     * 
     * @return int
     * @return the value of field 'cdBancoFavorecido'.
     */
    public int getCdBancoFavorecido()
    {
        return this._cdBancoFavorecido;
    } //-- int getCdBancoFavorecido() 

    /**
     * Returns the value of field 'cdBancoPagador'.
     * 
     * @return int
     * @return the value of field 'cdBancoPagador'.
     */
    public int getCdBancoPagador()
    {
        return this._cdBancoPagador;
    } //-- int getCdBancoPagador() 

    /**
     * Returns the value of field 'cdContaBancariaPagador'.
     * 
     * @return long
     * @return the value of field 'cdContaBancariaPagador'.
     */
    public long getCdContaBancariaPagador()
    {
        return this._cdContaBancariaPagador;
    } //-- long getCdContaBancariaPagador() 

    /**
     * Returns the value of field 'cdContaFavorecido'.
     * 
     * @return long
     * @return the value of field 'cdContaFavorecido'.
     */
    public long getCdContaFavorecido()
    {
        return this._cdContaFavorecido;
    } //-- long getCdContaFavorecido() 

    /**
     * Returns the value of field 'cdControlePagamento'.
     * 
     * @return String
     * @return the value of field 'cdControlePagamento'.
     */
    public java.lang.String getCdControlePagamento()
    {
        return this._cdControlePagamento;
    } //-- java.lang.String getCdControlePagamento() 

    /**
     * Returns the value of field 'cdDigitoAgenciaFavorecido'.
     * 
     * @return String
     * @return the value of field 'cdDigitoAgenciaFavorecido'.
     */
    public java.lang.String getCdDigitoAgenciaFavorecido()
    {
        return this._cdDigitoAgenciaFavorecido;
    } //-- java.lang.String getCdDigitoAgenciaFavorecido() 

    /**
     * Returns the value of field 'cdDigitoAgenciaPagador'.
     * 
     * @return String
     * @return the value of field 'cdDigitoAgenciaPagador'.
     */
    public java.lang.String getCdDigitoAgenciaPagador()
    {
        return this._cdDigitoAgenciaPagador;
    } //-- java.lang.String getCdDigitoAgenciaPagador() 

    /**
     * Returns the value of field 'cdDigitoContaFavorecido'.
     * 
     * @return String
     * @return the value of field 'cdDigitoContaFavorecido'.
     */
    public java.lang.String getCdDigitoContaFavorecido()
    {
        return this._cdDigitoContaFavorecido;
    } //-- java.lang.String getCdDigitoContaFavorecido() 

    /**
     * Returns the value of field 'cdDigitoContaPagador'.
     * 
     * @return String
     * @return the value of field 'cdDigitoContaPagador'.
     */
    public java.lang.String getCdDigitoContaPagador()
    {
        return this._cdDigitoContaPagador;
    } //-- java.lang.String getCdDigitoContaPagador() 

    /**
     * Returns the value of field 'cdIndentificadorSeqPagamento'.
     * 
     * @return String
     * @return the value of field 'cdIndentificadorSeqPagamento'.
     */
    public java.lang.String getCdIndentificadorSeqPagamento()
    {
        return this._cdIndentificadorSeqPagamento;
    } //-- java.lang.String getCdIndentificadorSeqPagamento() 

    /**
     * Returns the value of field 'cdProdutoOperacaoRelacionado'.
     * 
     * @return int
     * @return the value of field 'cdProdutoOperacaoRelacionado'.
     */
    public int getCdProdutoOperacaoRelacionado()
    {
        return this._cdProdutoOperacaoRelacionado;
    } //-- int getCdProdutoOperacaoRelacionado() 

    /**
     * Returns the value of field 'cdProdutoServicoOperacao'.
     * 
     * @return int
     * @return the value of field 'cdProdutoServicoOperacao'.
     */
    public int getCdProdutoServicoOperacao()
    {
        return this._cdProdutoServicoOperacao;
    } //-- int getCdProdutoServicoOperacao() 

    /**
     * Returns the value of field 'cdTipoContratoNegocio'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoNegocio'.
     */
    public int getCdTipoContratoNegocio()
    {
        return this._cdTipoContratoNegocio;
    } //-- int getCdTipoContratoNegocio() 

    /**
     * Returns the value of field 'cdpessoaJuridicaContrato'.
     * 
     * @return long
     * @return the value of field 'cdpessoaJuridicaContrato'.
     */
    public long getCdpessoaJuridicaContrato()
    {
        return this._cdpessoaJuridicaContrato;
    } //-- long getCdpessoaJuridicaContrato() 

    /**
     * Returns the value of field 'dtPagamentoAte'.
     * 
     * @return String
     * @return the value of field 'dtPagamentoAte'.
     */
    public java.lang.String getDtPagamentoAte()
    {
        return this._dtPagamentoAte;
    } //-- java.lang.String getDtPagamentoAte() 

    /**
     * Returns the value of field 'dtPagamentoDe'.
     * 
     * @return String
     * @return the value of field 'dtPagamentoDe'.
     */
    public java.lang.String getDtPagamentoDe()
    {
        return this._dtPagamentoDe;
    } //-- java.lang.String getDtPagamentoDe() 

    /**
     * Returns the value of field 'maxOcorrencias'.
     * 
     * @return int
     * @return the value of field 'maxOcorrencias'.
     */
    public int getMaxOcorrencias()
    {
        return this._maxOcorrencias;
    } //-- int getMaxOcorrencias() 

    /**
     * Returns the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @return long
     * @return the value of field 'nrSequenciaContratoNegocio'.
     */
    public long getNrSequenciaContratoNegocio()
    {
        return this._nrSequenciaContratoNegocio;
    } //-- long getNrSequenciaContratoNegocio() 

    /**
     * Method hasCdAgenciaBancariaPagador
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgenciaBancariaPagador()
    {
        return this._has_cdAgenciaBancariaPagador;
    } //-- boolean hasCdAgenciaBancariaPagador() 

    /**
     * Method hasCdAgenciaFavorecido
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgenciaFavorecido()
    {
        return this._has_cdAgenciaFavorecido;
    } //-- boolean hasCdAgenciaFavorecido() 

    /**
     * Method hasCdBancoFavorecido
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdBancoFavorecido()
    {
        return this._has_cdBancoFavorecido;
    } //-- boolean hasCdBancoFavorecido() 

    /**
     * Method hasCdBancoPagador
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdBancoPagador()
    {
        return this._has_cdBancoPagador;
    } //-- boolean hasCdBancoPagador() 

    /**
     * Method hasCdContaBancariaPagador
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdContaBancariaPagador()
    {
        return this._has_cdContaBancariaPagador;
    } //-- boolean hasCdContaBancariaPagador() 

    /**
     * Method hasCdContaFavorecido
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdContaFavorecido()
    {
        return this._has_cdContaFavorecido;
    } //-- boolean hasCdContaFavorecido() 

    /**
     * Method hasCdProdutoOperacaoRelacionado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdutoOperacaoRelacionado()
    {
        return this._has_cdProdutoOperacaoRelacionado;
    } //-- boolean hasCdProdutoOperacaoRelacionado() 

    /**
     * Method hasCdProdutoServicoOperacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdutoServicoOperacao()
    {
        return this._has_cdProdutoServicoOperacao;
    } //-- boolean hasCdProdutoServicoOperacao() 

    /**
     * Method hasCdTipoContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoNegocio()
    {
        return this._has_cdTipoContratoNegocio;
    } //-- boolean hasCdTipoContratoNegocio() 

    /**
     * Method hasCdpessoaJuridicaContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdpessoaJuridicaContrato()
    {
        return this._has_cdpessoaJuridicaContrato;
    } //-- boolean hasCdpessoaJuridicaContrato() 

    /**
     * Method hasMaxOcorrencias
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasMaxOcorrencias()
    {
        return this._has_maxOcorrencias;
    } //-- boolean hasMaxOcorrencias() 

    /**
     * Method hasNrSequenciaContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaContratoNegocio()
    {
        return this._has_nrSequenciaContratoNegocio;
    } //-- boolean hasNrSequenciaContratoNegocio() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdAgenciaBancariaPagador'.
     * 
     * @param cdAgenciaBancariaPagador the value of field
     * 'cdAgenciaBancariaPagador'.
     */
    public void setCdAgenciaBancariaPagador(int cdAgenciaBancariaPagador)
    {
        this._cdAgenciaBancariaPagador = cdAgenciaBancariaPagador;
        this._has_cdAgenciaBancariaPagador = true;
    } //-- void setCdAgenciaBancariaPagador(int) 

    /**
     * Sets the value of field 'cdAgenciaFavorecido'.
     * 
     * @param cdAgenciaFavorecido the value of field
     * 'cdAgenciaFavorecido'.
     */
    public void setCdAgenciaFavorecido(int cdAgenciaFavorecido)
    {
        this._cdAgenciaFavorecido = cdAgenciaFavorecido;
        this._has_cdAgenciaFavorecido = true;
    } //-- void setCdAgenciaFavorecido(int) 

    /**
     * Sets the value of field 'cdBancoFavorecido'.
     * 
     * @param cdBancoFavorecido the value of field
     * 'cdBancoFavorecido'.
     */
    public void setCdBancoFavorecido(int cdBancoFavorecido)
    {
        this._cdBancoFavorecido = cdBancoFavorecido;
        this._has_cdBancoFavorecido = true;
    } //-- void setCdBancoFavorecido(int) 

    /**
     * Sets the value of field 'cdBancoPagador'.
     * 
     * @param cdBancoPagador the value of field 'cdBancoPagador'.
     */
    public void setCdBancoPagador(int cdBancoPagador)
    {
        this._cdBancoPagador = cdBancoPagador;
        this._has_cdBancoPagador = true;
    } //-- void setCdBancoPagador(int) 

    /**
     * Sets the value of field 'cdContaBancariaPagador'.
     * 
     * @param cdContaBancariaPagador the value of field
     * 'cdContaBancariaPagador'.
     */
    public void setCdContaBancariaPagador(long cdContaBancariaPagador)
    {
        this._cdContaBancariaPagador = cdContaBancariaPagador;
        this._has_cdContaBancariaPagador = true;
    } //-- void setCdContaBancariaPagador(long) 

    /**
     * Sets the value of field 'cdContaFavorecido'.
     * 
     * @param cdContaFavorecido the value of field
     * 'cdContaFavorecido'.
     */
    public void setCdContaFavorecido(long cdContaFavorecido)
    {
        this._cdContaFavorecido = cdContaFavorecido;
        this._has_cdContaFavorecido = true;
    } //-- void setCdContaFavorecido(long) 

    /**
     * Sets the value of field 'cdControlePagamento'.
     * 
     * @param cdControlePagamento the value of field
     * 'cdControlePagamento'.
     */
    public void setCdControlePagamento(java.lang.String cdControlePagamento)
    {
        this._cdControlePagamento = cdControlePagamento;
    } //-- void setCdControlePagamento(java.lang.String) 

    /**
     * Sets the value of field 'cdDigitoAgenciaFavorecido'.
     * 
     * @param cdDigitoAgenciaFavorecido the value of field
     * 'cdDigitoAgenciaFavorecido'.
     */
    public void setCdDigitoAgenciaFavorecido(java.lang.String cdDigitoAgenciaFavorecido)
    {
        this._cdDigitoAgenciaFavorecido = cdDigitoAgenciaFavorecido;
    } //-- void setCdDigitoAgenciaFavorecido(java.lang.String) 

    /**
     * Sets the value of field 'cdDigitoAgenciaPagador'.
     * 
     * @param cdDigitoAgenciaPagador the value of field
     * 'cdDigitoAgenciaPagador'.
     */
    public void setCdDigitoAgenciaPagador(java.lang.String cdDigitoAgenciaPagador)
    {
        this._cdDigitoAgenciaPagador = cdDigitoAgenciaPagador;
    } //-- void setCdDigitoAgenciaPagador(java.lang.String) 

    /**
     * Sets the value of field 'cdDigitoContaFavorecido'.
     * 
     * @param cdDigitoContaFavorecido the value of field
     * 'cdDigitoContaFavorecido'.
     */
    public void setCdDigitoContaFavorecido(java.lang.String cdDigitoContaFavorecido)
    {
        this._cdDigitoContaFavorecido = cdDigitoContaFavorecido;
    } //-- void setCdDigitoContaFavorecido(java.lang.String) 

    /**
     * Sets the value of field 'cdDigitoContaPagador'.
     * 
     * @param cdDigitoContaPagador the value of field
     * 'cdDigitoContaPagador'.
     */
    public void setCdDigitoContaPagador(java.lang.String cdDigitoContaPagador)
    {
        this._cdDigitoContaPagador = cdDigitoContaPagador;
    } //-- void setCdDigitoContaPagador(java.lang.String) 

    /**
     * Sets the value of field 'cdIndentificadorSeqPagamento'.
     * 
     * @param cdIndentificadorSeqPagamento the value of field
     * 'cdIndentificadorSeqPagamento'.
     */
    public void setCdIndentificadorSeqPagamento(java.lang.String cdIndentificadorSeqPagamento)
    {
        this._cdIndentificadorSeqPagamento = cdIndentificadorSeqPagamento;
    } //-- void setCdIndentificadorSeqPagamento(java.lang.String) 

    /**
     * Sets the value of field 'cdProdutoOperacaoRelacionado'.
     * 
     * @param cdProdutoOperacaoRelacionado the value of field
     * 'cdProdutoOperacaoRelacionado'.
     */
    public void setCdProdutoOperacaoRelacionado(int cdProdutoOperacaoRelacionado)
    {
        this._cdProdutoOperacaoRelacionado = cdProdutoOperacaoRelacionado;
        this._has_cdProdutoOperacaoRelacionado = true;
    } //-- void setCdProdutoOperacaoRelacionado(int) 

    /**
     * Sets the value of field 'cdProdutoServicoOperacao'.
     * 
     * @param cdProdutoServicoOperacao the value of field
     * 'cdProdutoServicoOperacao'.
     */
    public void setCdProdutoServicoOperacao(int cdProdutoServicoOperacao)
    {
        this._cdProdutoServicoOperacao = cdProdutoServicoOperacao;
        this._has_cdProdutoServicoOperacao = true;
    } //-- void setCdProdutoServicoOperacao(int) 

    /**
     * Sets the value of field 'cdTipoContratoNegocio'.
     * 
     * @param cdTipoContratoNegocio the value of field
     * 'cdTipoContratoNegocio'.
     */
    public void setCdTipoContratoNegocio(int cdTipoContratoNegocio)
    {
        this._cdTipoContratoNegocio = cdTipoContratoNegocio;
        this._has_cdTipoContratoNegocio = true;
    } //-- void setCdTipoContratoNegocio(int) 

    /**
     * Sets the value of field 'cdpessoaJuridicaContrato'.
     * 
     * @param cdpessoaJuridicaContrato the value of field
     * 'cdpessoaJuridicaContrato'.
     */
    public void setCdpessoaJuridicaContrato(long cdpessoaJuridicaContrato)
    {
        this._cdpessoaJuridicaContrato = cdpessoaJuridicaContrato;
        this._has_cdpessoaJuridicaContrato = true;
    } //-- void setCdpessoaJuridicaContrato(long) 

    /**
     * Sets the value of field 'dtPagamentoAte'.
     * 
     * @param dtPagamentoAte the value of field 'dtPagamentoAte'.
     */
    public void setDtPagamentoAte(java.lang.String dtPagamentoAte)
    {
        this._dtPagamentoAte = dtPagamentoAte;
    } //-- void setDtPagamentoAte(java.lang.String) 

    /**
     * Sets the value of field 'dtPagamentoDe'.
     * 
     * @param dtPagamentoDe the value of field 'dtPagamentoDe'.
     */
    public void setDtPagamentoDe(java.lang.String dtPagamentoDe)
    {
        this._dtPagamentoDe = dtPagamentoDe;
    } //-- void setDtPagamentoDe(java.lang.String) 

    /**
     * Sets the value of field 'maxOcorrencias'.
     * 
     * @param maxOcorrencias the value of field 'maxOcorrencias'.
     */
    public void setMaxOcorrencias(int maxOcorrencias)
    {
        this._maxOcorrencias = maxOcorrencias;
        this._has_maxOcorrencias = true;
    } //-- void setMaxOcorrencias(int) 

    /**
     * Sets the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @param nrSequenciaContratoNegocio the value of field
     * 'nrSequenciaContratoNegocio'.
     */
    public void setNrSequenciaContratoNegocio(long nrSequenciaContratoNegocio)
    {
        this._nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
        this._has_nrSequenciaContratoNegocio = true;
    } //-- void setNrSequenciaContratoNegocio(long) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ListarTipoRetiradaRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listartiporetirada.request.ListarTipoRetiradaRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listartiporetirada.request.ListarTipoRetiradaRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listartiporetirada.request.ListarTipoRetiradaRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listartiporetirada.request.ListarTipoRetiradaRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
