/*
 * Nome: br.com.bradesco.web.pgit.service.business.contrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 19/02/2016
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.contrato.bean;


/**
 * Nome: ImprimirContratoOcorrencias1SaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ImprimirContratoOcorrencias1SaidaDTO {

	/** The cd indicador tributo contrato. */
	private String cdIndicadorTributoContrato = null;
	
    /** The cd indicador modalidade tributos. */
    private Integer cdIndicadorModalidadeTributos = null;

    /** The cd produto relacionado tributo. */
    private Integer cdProdutoRelacionadoTributo = null;

    /** The ds produto relacionado tributo. */
    private String dsProdutoRelacionadoTributo = null;

    /** The cd momento processamento tributos. */
    private Integer cdMomentoProcessamentoTributos = null;

    /** The ds momento processamento tributos. */
    private String dsMomentoProcessamentoTributos = null;

    /** The cd indicador feriado tributos. */
    private Integer cdIndicadorFeriadoTributos = null;

    /** The ds indicador feriado tributos. */
    private String dsIndicadorFeriadoTributos = null;

    /**
     * Instancia um novo ImprimirContratoOcorrencias1SaidaDTO.
     * 
     * @see
     */
    public ImprimirContratoOcorrencias1SaidaDTO() {
        super();
    }

    /**
     * Sets the cd indicador modalidade tributos.
     *
     * @param cdIndicadorModalidadeTributos the cd indicador modalidade tributos
     */
    public void setCdIndicadorModalidadeTributos(Integer cdIndicadorModalidadeTributos) {
        this.cdIndicadorModalidadeTributos = cdIndicadorModalidadeTributos;
    }

    /**
     * Gets the cd indicador modalidade tributos.
     *
     * @return the cd indicador modalidade tributos
     */
    public Integer getCdIndicadorModalidadeTributos() {
        return this.cdIndicadorModalidadeTributos;
    }

    /**
     * Sets the cd produto relacionado tributo.
     *
     * @param cdProdutoRelacionadoTributo the cd produto relacionado tributo
     */
    public void setCdProdutoRelacionadoTributo(Integer cdProdutoRelacionadoTributo) {
        this.cdProdutoRelacionadoTributo = cdProdutoRelacionadoTributo;
    }

    /**
     * Gets the cd produto relacionado tributo.
     *
     * @return the cd produto relacionado tributo
     */
    public Integer getCdProdutoRelacionadoTributo() {
        return this.cdProdutoRelacionadoTributo;
    }

    /**
     * Sets the ds produto relacionado tributo.
     *
     * @param dsProdutoRelacionadoTributo the ds produto relacionado tributo
     */
    public void setDsProdutoRelacionadoTributo(String dsProdutoRelacionadoTributo) {
        this.dsProdutoRelacionadoTributo = dsProdutoRelacionadoTributo;
    }

    /**
     * Gets the ds produto relacionado tributo.
     *
     * @return the ds produto relacionado tributo
     */
    public String getDsProdutoRelacionadoTributo() {
        return this.dsProdutoRelacionadoTributo;
    }

    /**
     * Sets the cd momento processamento tributos.
     *
     * @param cdMomentoProcessamentoTributos the cd momento processamento tributos
     */
    public void setCdMomentoProcessamentoTributos(Integer cdMomentoProcessamentoTributos) {
        this.cdMomentoProcessamentoTributos = cdMomentoProcessamentoTributos;
    }

    /**
     * Gets the cd momento processamento tributos.
     *
     * @return the cd momento processamento tributos
     */
    public Integer getCdMomentoProcessamentoTributos() {
        return this.cdMomentoProcessamentoTributos;
    }

    /**
     * Sets the ds momento processamento tributos.
     *
     * @param dsMomentoProcessamentoTributos the ds momento processamento tributos
     */
    public void setDsMomentoProcessamentoTributos(String dsMomentoProcessamentoTributos) {
        this.dsMomentoProcessamentoTributos = dsMomentoProcessamentoTributos;
    }

    /**
     * Gets the ds momento processamento tributos.
     *
     * @return the ds momento processamento tributos
     */
    public String getDsMomentoProcessamentoTributos() {
        return this.dsMomentoProcessamentoTributos;
    }

    /**
     * Sets the cd indicador feriado tributos.
     *
     * @param cdIndicadorFeriadoTributos the cd indicador feriado tributos
     */
    public void setCdIndicadorFeriadoTributos(Integer cdIndicadorFeriadoTributos) {
        this.cdIndicadorFeriadoTributos = cdIndicadorFeriadoTributos;
    }

    /**
     * Gets the cd indicador feriado tributos.
     *
     * @return the cd indicador feriado tributos
     */
    public Integer getCdIndicadorFeriadoTributos() {
        return this.cdIndicadorFeriadoTributos;
    }

    /**
     * Sets the ds indicador feriado tributos.
     *
     * @param dsIndicadorFeriadoTributos the ds indicador feriado tributos
     */
    public void setDsIndicadorFeriadoTributos(String dsIndicadorFeriadoTributos) {
        this.dsIndicadorFeriadoTributos = dsIndicadorFeriadoTributos;
    }

    /**
     * Gets the ds indicador feriado tributos.
     *
     * @return the ds indicador feriado tributos
     */
    public String getDsIndicadorFeriadoTributos() {
        return this.dsIndicadorFeriadoTributos;
    }

	/**
	 * Gets the cd indicador tributo contrato.
	 *
	 * @return the cd indicador tributo contrato
	 */
	public String getCdIndicadorTributoContrato() {
		return cdIndicadorTributoContrato;
	}

	/**
	 * Sets the cd indicador tributo contrato.
	 *
	 * @param cdIndicadorTributoContrato the new cd indicador tributo contrato
	 */
	public void setCdIndicadorTributoContrato(String cdIndicadorTributoContrato) {
		this.cdIndicadorTributoContrato = cdIndicadorTributoContrato;
	}
}