/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantercontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantercontrato.bean;

/**
 * Nome: ListarAgregadoEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarAgregadoEntradaDTO {

	/** Atributo cdCorpoCpfCnpj. */
	private Long cdCorpoCpfCnpj;
	
	/** Atributo cdControleCpfCnpj. */
	private Integer cdControleCpfCnpj;
	
	/** Atributo cdFilialCpnj. */
	private Integer cdFilialCpnj;

	/**
	 * Get: cdCorpoCpfCnpj.
	 *
	 * @return cdCorpoCpfCnpj
	 */
	public Long getCdCorpoCpfCnpj() {
		return cdCorpoCpfCnpj;
	}
	
	/**
	 * Set: cdCorpoCpfCnpj.
	 *
	 * @param cdCorpoCpfCnpj the cd corpo cpf cnpj
	 */
	public void setCdCorpoCpfCnpj(Long cdCorpoCpfCnpj) {
		this.cdCorpoCpfCnpj = cdCorpoCpfCnpj;
	}
	
	/**
	 * Get: cdControleCpfCnpj.
	 *
	 * @return cdControleCpfCnpj
	 */
	public Integer getCdControleCpfCnpj() {
		return cdControleCpfCnpj;
	}
	
	/**
	 * Set: cdControleCpfCnpj.
	 *
	 * @param cdControleCpfCnpj the cd controle cpf cnpj
	 */
	public void setCdControleCpfCnpj(Integer cdControleCpfCnpj) {
		this.cdControleCpfCnpj = cdControleCpfCnpj;
	}
	
	/**
	 * Get: cdFilialCpnj.
	 *
	 * @return cdFilialCpnj
	 */
	public Integer getCdFilialCpnj() {
		return cdFilialCpnj;
	}
	
	/**
	 * Set: cdFilialCpnj.
	 *
	 * @param cdFilialCpnj the cd filial cpnj
	 */
	public void setCdFilialCpnj(Integer cdFilialCpnj) {
		this.cdFilialCpnj = cdFilialCpnj;
	}
}