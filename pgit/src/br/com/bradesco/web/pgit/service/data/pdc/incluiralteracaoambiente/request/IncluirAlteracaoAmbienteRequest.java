/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.incluiralteracaoambiente.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class IncluirAlteracaoAmbienteRequest.
 * 
 * @version $Revision$ $Date$
 */
public class IncluirAlteracaoAmbienteRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdPessoaJuridica
     */
    private long _cdPessoaJuridica = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridica
     */
    private boolean _has_cdPessoaJuridica;

    /**
     * Field _cdTipoContratoNegocio
     */
    private int _cdTipoContratoNegocio = 0;

    /**
     * keeps track of state for field: _cdTipoContratoNegocio
     */
    private boolean _has_cdTipoContratoNegocio;

    /**
     * Field _nrSequenciaContrato
     */
    private long _nrSequenciaContrato = 0;

    /**
     * keeps track of state for field: _nrSequenciaContrato
     */
    private boolean _has_nrSequenciaContrato;

    /**
     * Field _cdServico
     */
    private int _cdServico = 0;

    /**
     * keeps track of state for field: _cdServico
     */
    private boolean _has_cdServico;

    /**
     * Field _cdModalidade
     */
    private int _cdModalidade = 0;

    /**
     * keeps track of state for field: _cdModalidade
     */
    private boolean _has_cdModalidade;

    /**
     * Field _cdRelacionamentoProduto
     */
    private int _cdRelacionamentoProduto = 0;

    /**
     * keeps track of state for field: _cdRelacionamentoProduto
     */
    private boolean _has_cdRelacionamentoProduto;

    /**
     * Field _cdFormaMudanca
     */
    private int _cdFormaMudanca = 0;

    /**
     * keeps track of state for field: _cdFormaMudanca
     */
    private boolean _has_cdFormaMudanca;

    /**
     * Field _dtMudancao
     */
    private java.lang.String _dtMudancao;

    /**
     * Field _cdIndicadorArmazenamentoInformacao
     */
    private int _cdIndicadorArmazenamentoInformacao = 0;

    /**
     * keeps track of state for field:
     * _cdIndicadorArmazenamentoInformacao
     */
    private boolean _has_cdIndicadorArmazenamentoInformacao;

    /**
     * Field _cdAmbienteAtivoSolicitacao
     */
    private java.lang.String _cdAmbienteAtivoSolicitacao;


      //----------------/
     //- Constructors -/
    //----------------/

    public IncluirAlteracaoAmbienteRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.incluiralteracaoambiente.request.IncluirAlteracaoAmbienteRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdFormaMudanca
     * 
     */
    public void deleteCdFormaMudanca()
    {
        this._has_cdFormaMudanca= false;
    } //-- void deleteCdFormaMudanca() 

    /**
     * Method deleteCdIndicadorArmazenamentoInformacao
     * 
     */
    public void deleteCdIndicadorArmazenamentoInformacao()
    {
        this._has_cdIndicadorArmazenamentoInformacao= false;
    } //-- void deleteCdIndicadorArmazenamentoInformacao() 

    /**
     * Method deleteCdModalidade
     * 
     */
    public void deleteCdModalidade()
    {
        this._has_cdModalidade= false;
    } //-- void deleteCdModalidade() 

    /**
     * Method deleteCdPessoaJuridica
     * 
     */
    public void deleteCdPessoaJuridica()
    {
        this._has_cdPessoaJuridica= false;
    } //-- void deleteCdPessoaJuridica() 

    /**
     * Method deleteCdRelacionamentoProduto
     * 
     */
    public void deleteCdRelacionamentoProduto()
    {
        this._has_cdRelacionamentoProduto= false;
    } //-- void deleteCdRelacionamentoProduto() 

    /**
     * Method deleteCdServico
     * 
     */
    public void deleteCdServico()
    {
        this._has_cdServico= false;
    } //-- void deleteCdServico() 

    /**
     * Method deleteCdTipoContratoNegocio
     * 
     */
    public void deleteCdTipoContratoNegocio()
    {
        this._has_cdTipoContratoNegocio= false;
    } //-- void deleteCdTipoContratoNegocio() 

    /**
     * Method deleteNrSequenciaContrato
     * 
     */
    public void deleteNrSequenciaContrato()
    {
        this._has_nrSequenciaContrato= false;
    } //-- void deleteNrSequenciaContrato() 

    /**
     * Returns the value of field 'cdAmbienteAtivoSolicitacao'.
     * 
     * @return String
     * @return the value of field 'cdAmbienteAtivoSolicitacao'.
     */
    public java.lang.String getCdAmbienteAtivoSolicitacao()
    {
        return this._cdAmbienteAtivoSolicitacao;
    } //-- java.lang.String getCdAmbienteAtivoSolicitacao() 

    /**
     * Returns the value of field 'cdFormaMudanca'.
     * 
     * @return int
     * @return the value of field 'cdFormaMudanca'.
     */
    public int getCdFormaMudanca()
    {
        return this._cdFormaMudanca;
    } //-- int getCdFormaMudanca() 

    /**
     * Returns the value of field
     * 'cdIndicadorArmazenamentoInformacao'.
     * 
     * @return int
     * @return the value of field
     * 'cdIndicadorArmazenamentoInformacao'.
     */
    public int getCdIndicadorArmazenamentoInformacao()
    {
        return this._cdIndicadorArmazenamentoInformacao;
    } //-- int getCdIndicadorArmazenamentoInformacao() 

    /**
     * Returns the value of field 'cdModalidade'.
     * 
     * @return int
     * @return the value of field 'cdModalidade'.
     */
    public int getCdModalidade()
    {
        return this._cdModalidade;
    } //-- int getCdModalidade() 

    /**
     * Returns the value of field 'cdPessoaJuridica'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridica'.
     */
    public long getCdPessoaJuridica()
    {
        return this._cdPessoaJuridica;
    } //-- long getCdPessoaJuridica() 

    /**
     * Returns the value of field 'cdRelacionamentoProduto'.
     * 
     * @return int
     * @return the value of field 'cdRelacionamentoProduto'.
     */
    public int getCdRelacionamentoProduto()
    {
        return this._cdRelacionamentoProduto;
    } //-- int getCdRelacionamentoProduto() 

    /**
     * Returns the value of field 'cdServico'.
     * 
     * @return int
     * @return the value of field 'cdServico'.
     */
    public int getCdServico()
    {
        return this._cdServico;
    } //-- int getCdServico() 

    /**
     * Returns the value of field 'cdTipoContratoNegocio'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoNegocio'.
     */
    public int getCdTipoContratoNegocio()
    {
        return this._cdTipoContratoNegocio;
    } //-- int getCdTipoContratoNegocio() 

    /**
     * Returns the value of field 'dtMudancao'.
     * 
     * @return String
     * @return the value of field 'dtMudancao'.
     */
    public java.lang.String getDtMudancao()
    {
        return this._dtMudancao;
    } //-- java.lang.String getDtMudancao() 

    /**
     * Returns the value of field 'nrSequenciaContrato'.
     * 
     * @return long
     * @return the value of field 'nrSequenciaContrato'.
     */
    public long getNrSequenciaContrato()
    {
        return this._nrSequenciaContrato;
    } //-- long getNrSequenciaContrato() 

    /**
     * Method hasCdFormaMudanca
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFormaMudanca()
    {
        return this._has_cdFormaMudanca;
    } //-- boolean hasCdFormaMudanca() 

    /**
     * Method hasCdIndicadorArmazenamentoInformacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorArmazenamentoInformacao()
    {
        return this._has_cdIndicadorArmazenamentoInformacao;
    } //-- boolean hasCdIndicadorArmazenamentoInformacao() 

    /**
     * Method hasCdModalidade
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdModalidade()
    {
        return this._has_cdModalidade;
    } //-- boolean hasCdModalidade() 

    /**
     * Method hasCdPessoaJuridica
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridica()
    {
        return this._has_cdPessoaJuridica;
    } //-- boolean hasCdPessoaJuridica() 

    /**
     * Method hasCdRelacionamentoProduto
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdRelacionamentoProduto()
    {
        return this._has_cdRelacionamentoProduto;
    } //-- boolean hasCdRelacionamentoProduto() 

    /**
     * Method hasCdServico
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdServico()
    {
        return this._has_cdServico;
    } //-- boolean hasCdServico() 

    /**
     * Method hasCdTipoContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoNegocio()
    {
        return this._has_cdTipoContratoNegocio;
    } //-- boolean hasCdTipoContratoNegocio() 

    /**
     * Method hasNrSequenciaContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaContrato()
    {
        return this._has_nrSequenciaContrato;
    } //-- boolean hasNrSequenciaContrato() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdAmbienteAtivoSolicitacao'.
     * 
     * @param cdAmbienteAtivoSolicitacao the value of field
     * 'cdAmbienteAtivoSolicitacao'.
     */
    public void setCdAmbienteAtivoSolicitacao(java.lang.String cdAmbienteAtivoSolicitacao)
    {
        this._cdAmbienteAtivoSolicitacao = cdAmbienteAtivoSolicitacao;
    } //-- void setCdAmbienteAtivoSolicitacao(java.lang.String) 

    /**
     * Sets the value of field 'cdFormaMudanca'.
     * 
     * @param cdFormaMudanca the value of field 'cdFormaMudanca'.
     */
    public void setCdFormaMudanca(int cdFormaMudanca)
    {
        this._cdFormaMudanca = cdFormaMudanca;
        this._has_cdFormaMudanca = true;
    } //-- void setCdFormaMudanca(int) 

    /**
     * Sets the value of field
     * 'cdIndicadorArmazenamentoInformacao'.
     * 
     * @param cdIndicadorArmazenamentoInformacao the value of field
     * 'cdIndicadorArmazenamentoInformacao'.
     */
    public void setCdIndicadorArmazenamentoInformacao(int cdIndicadorArmazenamentoInformacao)
    {
        this._cdIndicadorArmazenamentoInformacao = cdIndicadorArmazenamentoInformacao;
        this._has_cdIndicadorArmazenamentoInformacao = true;
    } //-- void setCdIndicadorArmazenamentoInformacao(int) 

    /**
     * Sets the value of field 'cdModalidade'.
     * 
     * @param cdModalidade the value of field 'cdModalidade'.
     */
    public void setCdModalidade(int cdModalidade)
    {
        this._cdModalidade = cdModalidade;
        this._has_cdModalidade = true;
    } //-- void setCdModalidade(int) 

    /**
     * Sets the value of field 'cdPessoaJuridica'.
     * 
     * @param cdPessoaJuridica the value of field 'cdPessoaJuridica'
     */
    public void setCdPessoaJuridica(long cdPessoaJuridica)
    {
        this._cdPessoaJuridica = cdPessoaJuridica;
        this._has_cdPessoaJuridica = true;
    } //-- void setCdPessoaJuridica(long) 

    /**
     * Sets the value of field 'cdRelacionamentoProduto'.
     * 
     * @param cdRelacionamentoProduto the value of field
     * 'cdRelacionamentoProduto'.
     */
    public void setCdRelacionamentoProduto(int cdRelacionamentoProduto)
    {
        this._cdRelacionamentoProduto = cdRelacionamentoProduto;
        this._has_cdRelacionamentoProduto = true;
    } //-- void setCdRelacionamentoProduto(int) 

    /**
     * Sets the value of field 'cdServico'.
     * 
     * @param cdServico the value of field 'cdServico'.
     */
    public void setCdServico(int cdServico)
    {
        this._cdServico = cdServico;
        this._has_cdServico = true;
    } //-- void setCdServico(int) 

    /**
     * Sets the value of field 'cdTipoContratoNegocio'.
     * 
     * @param cdTipoContratoNegocio the value of field
     * 'cdTipoContratoNegocio'.
     */
    public void setCdTipoContratoNegocio(int cdTipoContratoNegocio)
    {
        this._cdTipoContratoNegocio = cdTipoContratoNegocio;
        this._has_cdTipoContratoNegocio = true;
    } //-- void setCdTipoContratoNegocio(int) 

    /**
     * Sets the value of field 'dtMudancao'.
     * 
     * @param dtMudancao the value of field 'dtMudancao'.
     */
    public void setDtMudancao(java.lang.String dtMudancao)
    {
        this._dtMudancao = dtMudancao;
    } //-- void setDtMudancao(java.lang.String) 

    /**
     * Sets the value of field 'nrSequenciaContrato'.
     * 
     * @param nrSequenciaContrato the value of field
     * 'nrSequenciaContrato'.
     */
    public void setNrSequenciaContrato(long nrSequenciaContrato)
    {
        this._nrSequenciaContrato = nrSequenciaContrato;
        this._has_nrSequenciaContrato = true;
    } //-- void setNrSequenciaContrato(long) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return IncluirAlteracaoAmbienteRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.incluiralteracaoambiente.request.IncluirAlteracaoAmbienteRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.incluiralteracaoambiente.request.IncluirAlteracaoAmbienteRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.incluiralteracaoambiente.request.IncluirAlteracaoAmbienteRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.incluiralteracaoambiente.request.IncluirAlteracaoAmbienteRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
