/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.imprimiraditivocontrato.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _nrAditivoContratoNegocio
     */
    private long _nrAditivoContratoNegocio = 0;

    /**
     * keeps track of state for field: _nrAditivoContratoNegocio
     */
    private boolean _has_nrAditivoContratoNegocio;

    /**
     * Field _cdSituacaoAditivoContrato
     */
    private int _cdSituacaoAditivoContrato = 0;

    /**
     * keeps track of state for field: _cdSituacaoAditivoContrato
     */
    private boolean _has_cdSituacaoAditivoContrato;

    /**
     * Field _dsSituacaoAditivoContrato
     */
    private java.lang.String _dsSituacaoAditivoContrato;

    /**
     * Field _cdMotivoSituacaoAditivo
     */
    private int _cdMotivoSituacaoAditivo = 0;

    /**
     * keeps track of state for field: _cdMotivoSituacaoAditivo
     */
    private boolean _has_cdMotivoSituacaoAditivo;

    /**
     * Field _dsMotivoSituacaoAditivo
     */
    private java.lang.String _dsMotivoSituacaoAditivo;

    /**
     * Field _hrAssinaturaAditivoContrato
     */
    private java.lang.String _hrAssinaturaAditivoContrato;

    /**
     * Field _cdIndicadorContratoComl
     */
    private int _cdIndicadorContratoComl = 0;

    /**
     * keeps track of state for field: _cdIndicadorContratoComl
     */
    private boolean _has_cdIndicadorContratoComl;

    /**
     * Field _cdUsuarioInclusao
     */
    private java.lang.String _cdUsuarioInclusao;

    /**
     * Field _cdUsuarioInclusaoExterno
     */
    private java.lang.String _cdUsuarioInclusaoExterno;

    /**
     * Field _hrInclusaoRegistro
     */
    private java.lang.String _hrInclusaoRegistro;

    /**
     * Field _cdOperacaoCanalInclusao
     */
    private java.lang.String _cdOperacaoCanalInclusao;

    /**
     * Field _cdTipoCanalInclusao
     */
    private int _cdTipoCanalInclusao = 0;

    /**
     * keeps track of state for field: _cdTipoCanalInclusao
     */
    private boolean _has_cdTipoCanalInclusao;

    /**
     * Field _dsTipoCanalInclusao
     */
    private java.lang.String _dsTipoCanalInclusao;

    /**
     * Field _cdUsuarioManutencao
     */
    private java.lang.String _cdUsuarioManutencao;

    /**
     * Field _cdUsuarioManutencaoExterno
     */
    private java.lang.String _cdUsuarioManutencaoExterno;

    /**
     * Field _hrManutencaoRegistro
     */
    private java.lang.String _hrManutencaoRegistro;

    /**
     * Field _cdOperacaoCanalManutencao
     */
    private java.lang.String _cdOperacaoCanalManutencao;

    /**
     * Field _cdTipoCanalManutencao
     */
    private int _cdTipoCanalManutencao = 0;

    /**
     * keeps track of state for field: _cdTipoCanalManutencao
     */
    private boolean _has_cdTipoCanalManutencao;

    /**
     * Field _dsTipoCanalManutencao
     */
    private java.lang.String _dsTipoCanalManutencao;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimiraditivocontrato.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdIndicadorContratoComl
     * 
     */
    public void deleteCdIndicadorContratoComl()
    {
        this._has_cdIndicadorContratoComl= false;
    } //-- void deleteCdIndicadorContratoComl() 

    /**
     * Method deleteCdMotivoSituacaoAditivo
     * 
     */
    public void deleteCdMotivoSituacaoAditivo()
    {
        this._has_cdMotivoSituacaoAditivo= false;
    } //-- void deleteCdMotivoSituacaoAditivo() 

    /**
     * Method deleteCdSituacaoAditivoContrato
     * 
     */
    public void deleteCdSituacaoAditivoContrato()
    {
        this._has_cdSituacaoAditivoContrato= false;
    } //-- void deleteCdSituacaoAditivoContrato() 

    /**
     * Method deleteCdTipoCanalInclusao
     * 
     */
    public void deleteCdTipoCanalInclusao()
    {
        this._has_cdTipoCanalInclusao= false;
    } //-- void deleteCdTipoCanalInclusao() 

    /**
     * Method deleteCdTipoCanalManutencao
     * 
     */
    public void deleteCdTipoCanalManutencao()
    {
        this._has_cdTipoCanalManutencao= false;
    } //-- void deleteCdTipoCanalManutencao() 

    /**
     * Method deleteNrAditivoContratoNegocio
     * 
     */
    public void deleteNrAditivoContratoNegocio()
    {
        this._has_nrAditivoContratoNegocio= false;
    } //-- void deleteNrAditivoContratoNegocio() 

    /**
     * Returns the value of field 'cdIndicadorContratoComl'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorContratoComl'.
     */
    public int getCdIndicadorContratoComl()
    {
        return this._cdIndicadorContratoComl;
    } //-- int getCdIndicadorContratoComl() 

    /**
     * Returns the value of field 'cdMotivoSituacaoAditivo'.
     * 
     * @return int
     * @return the value of field 'cdMotivoSituacaoAditivo'.
     */
    public int getCdMotivoSituacaoAditivo()
    {
        return this._cdMotivoSituacaoAditivo;
    } //-- int getCdMotivoSituacaoAditivo() 

    /**
     * Returns the value of field 'cdOperacaoCanalInclusao'.
     * 
     * @return String
     * @return the value of field 'cdOperacaoCanalInclusao'.
     */
    public java.lang.String getCdOperacaoCanalInclusao()
    {
        return this._cdOperacaoCanalInclusao;
    } //-- java.lang.String getCdOperacaoCanalInclusao() 

    /**
     * Returns the value of field 'cdOperacaoCanalManutencao'.
     * 
     * @return String
     * @return the value of field 'cdOperacaoCanalManutencao'.
     */
    public java.lang.String getCdOperacaoCanalManutencao()
    {
        return this._cdOperacaoCanalManutencao;
    } //-- java.lang.String getCdOperacaoCanalManutencao() 

    /**
     * Returns the value of field 'cdSituacaoAditivoContrato'.
     * 
     * @return int
     * @return the value of field 'cdSituacaoAditivoContrato'.
     */
    public int getCdSituacaoAditivoContrato()
    {
        return this._cdSituacaoAditivoContrato;
    } //-- int getCdSituacaoAditivoContrato() 

    /**
     * Returns the value of field 'cdTipoCanalInclusao'.
     * 
     * @return int
     * @return the value of field 'cdTipoCanalInclusao'.
     */
    public int getCdTipoCanalInclusao()
    {
        return this._cdTipoCanalInclusao;
    } //-- int getCdTipoCanalInclusao() 

    /**
     * Returns the value of field 'cdTipoCanalManutencao'.
     * 
     * @return int
     * @return the value of field 'cdTipoCanalManutencao'.
     */
    public int getCdTipoCanalManutencao()
    {
        return this._cdTipoCanalManutencao;
    } //-- int getCdTipoCanalManutencao() 

    /**
     * Returns the value of field 'cdUsuarioInclusao'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioInclusao'.
     */
    public java.lang.String getCdUsuarioInclusao()
    {
        return this._cdUsuarioInclusao;
    } //-- java.lang.String getCdUsuarioInclusao() 

    /**
     * Returns the value of field 'cdUsuarioInclusaoExterno'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioInclusaoExterno'.
     */
    public java.lang.String getCdUsuarioInclusaoExterno()
    {
        return this._cdUsuarioInclusaoExterno;
    } //-- java.lang.String getCdUsuarioInclusaoExterno() 

    /**
     * Returns the value of field 'cdUsuarioManutencao'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioManutencao'.
     */
    public java.lang.String getCdUsuarioManutencao()
    {
        return this._cdUsuarioManutencao;
    } //-- java.lang.String getCdUsuarioManutencao() 

    /**
     * Returns the value of field 'cdUsuarioManutencaoExterno'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioManutencaoExterno'.
     */
    public java.lang.String getCdUsuarioManutencaoExterno()
    {
        return this._cdUsuarioManutencaoExterno;
    } //-- java.lang.String getCdUsuarioManutencaoExterno() 

    /**
     * Returns the value of field 'dsMotivoSituacaoAditivo'.
     * 
     * @return String
     * @return the value of field 'dsMotivoSituacaoAditivo'.
     */
    public java.lang.String getDsMotivoSituacaoAditivo()
    {
        return this._dsMotivoSituacaoAditivo;
    } //-- java.lang.String getDsMotivoSituacaoAditivo() 

    /**
     * Returns the value of field 'dsSituacaoAditivoContrato'.
     * 
     * @return String
     * @return the value of field 'dsSituacaoAditivoContrato'.
     */
    public java.lang.String getDsSituacaoAditivoContrato()
    {
        return this._dsSituacaoAditivoContrato;
    } //-- java.lang.String getDsSituacaoAditivoContrato() 

    /**
     * Returns the value of field 'dsTipoCanalInclusao'.
     * 
     * @return String
     * @return the value of field 'dsTipoCanalInclusao'.
     */
    public java.lang.String getDsTipoCanalInclusao()
    {
        return this._dsTipoCanalInclusao;
    } //-- java.lang.String getDsTipoCanalInclusao() 

    /**
     * Returns the value of field 'dsTipoCanalManutencao'.
     * 
     * @return String
     * @return the value of field 'dsTipoCanalManutencao'.
     */
    public java.lang.String getDsTipoCanalManutencao()
    {
        return this._dsTipoCanalManutencao;
    } //-- java.lang.String getDsTipoCanalManutencao() 

    /**
     * Returns the value of field 'hrAssinaturaAditivoContrato'.
     * 
     * @return String
     * @return the value of field 'hrAssinaturaAditivoContrato'.
     */
    public java.lang.String getHrAssinaturaAditivoContrato()
    {
        return this._hrAssinaturaAditivoContrato;
    } //-- java.lang.String getHrAssinaturaAditivoContrato() 

    /**
     * Returns the value of field 'hrInclusaoRegistro'.
     * 
     * @return String
     * @return the value of field 'hrInclusaoRegistro'.
     */
    public java.lang.String getHrInclusaoRegistro()
    {
        return this._hrInclusaoRegistro;
    } //-- java.lang.String getHrInclusaoRegistro() 

    /**
     * Returns the value of field 'hrManutencaoRegistro'.
     * 
     * @return String
     * @return the value of field 'hrManutencaoRegistro'.
     */
    public java.lang.String getHrManutencaoRegistro()
    {
        return this._hrManutencaoRegistro;
    } //-- java.lang.String getHrManutencaoRegistro() 

    /**
     * Returns the value of field 'nrAditivoContratoNegocio'.
     * 
     * @return long
     * @return the value of field 'nrAditivoContratoNegocio'.
     */
    public long getNrAditivoContratoNegocio()
    {
        return this._nrAditivoContratoNegocio;
    } //-- long getNrAditivoContratoNegocio() 

    /**
     * Method hasCdIndicadorContratoComl
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorContratoComl()
    {
        return this._has_cdIndicadorContratoComl;
    } //-- boolean hasCdIndicadorContratoComl() 

    /**
     * Method hasCdMotivoSituacaoAditivo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMotivoSituacaoAditivo()
    {
        return this._has_cdMotivoSituacaoAditivo;
    } //-- boolean hasCdMotivoSituacaoAditivo() 

    /**
     * Method hasCdSituacaoAditivoContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSituacaoAditivoContrato()
    {
        return this._has_cdSituacaoAditivoContrato;
    } //-- boolean hasCdSituacaoAditivoContrato() 

    /**
     * Method hasCdTipoCanalInclusao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoCanalInclusao()
    {
        return this._has_cdTipoCanalInclusao;
    } //-- boolean hasCdTipoCanalInclusao() 

    /**
     * Method hasCdTipoCanalManutencao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoCanalManutencao()
    {
        return this._has_cdTipoCanalManutencao;
    } //-- boolean hasCdTipoCanalManutencao() 

    /**
     * Method hasNrAditivoContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrAditivoContratoNegocio()
    {
        return this._has_nrAditivoContratoNegocio;
    } //-- boolean hasNrAditivoContratoNegocio() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdIndicadorContratoComl'.
     * 
     * @param cdIndicadorContratoComl the value of field
     * 'cdIndicadorContratoComl'.
     */
    public void setCdIndicadorContratoComl(int cdIndicadorContratoComl)
    {
        this._cdIndicadorContratoComl = cdIndicadorContratoComl;
        this._has_cdIndicadorContratoComl = true;
    } //-- void setCdIndicadorContratoComl(int) 

    /**
     * Sets the value of field 'cdMotivoSituacaoAditivo'.
     * 
     * @param cdMotivoSituacaoAditivo the value of field
     * 'cdMotivoSituacaoAditivo'.
     */
    public void setCdMotivoSituacaoAditivo(int cdMotivoSituacaoAditivo)
    {
        this._cdMotivoSituacaoAditivo = cdMotivoSituacaoAditivo;
        this._has_cdMotivoSituacaoAditivo = true;
    } //-- void setCdMotivoSituacaoAditivo(int) 

    /**
     * Sets the value of field 'cdOperacaoCanalInclusao'.
     * 
     * @param cdOperacaoCanalInclusao the value of field
     * 'cdOperacaoCanalInclusao'.
     */
    public void setCdOperacaoCanalInclusao(java.lang.String cdOperacaoCanalInclusao)
    {
        this._cdOperacaoCanalInclusao = cdOperacaoCanalInclusao;
    } //-- void setCdOperacaoCanalInclusao(java.lang.String) 

    /**
     * Sets the value of field 'cdOperacaoCanalManutencao'.
     * 
     * @param cdOperacaoCanalManutencao the value of field
     * 'cdOperacaoCanalManutencao'.
     */
    public void setCdOperacaoCanalManutencao(java.lang.String cdOperacaoCanalManutencao)
    {
        this._cdOperacaoCanalManutencao = cdOperacaoCanalManutencao;
    } //-- void setCdOperacaoCanalManutencao(java.lang.String) 

    /**
     * Sets the value of field 'cdSituacaoAditivoContrato'.
     * 
     * @param cdSituacaoAditivoContrato the value of field
     * 'cdSituacaoAditivoContrato'.
     */
    public void setCdSituacaoAditivoContrato(int cdSituacaoAditivoContrato)
    {
        this._cdSituacaoAditivoContrato = cdSituacaoAditivoContrato;
        this._has_cdSituacaoAditivoContrato = true;
    } //-- void setCdSituacaoAditivoContrato(int) 

    /**
     * Sets the value of field 'cdTipoCanalInclusao'.
     * 
     * @param cdTipoCanalInclusao the value of field
     * 'cdTipoCanalInclusao'.
     */
    public void setCdTipoCanalInclusao(int cdTipoCanalInclusao)
    {
        this._cdTipoCanalInclusao = cdTipoCanalInclusao;
        this._has_cdTipoCanalInclusao = true;
    } //-- void setCdTipoCanalInclusao(int) 

    /**
     * Sets the value of field 'cdTipoCanalManutencao'.
     * 
     * @param cdTipoCanalManutencao the value of field
     * 'cdTipoCanalManutencao'.
     */
    public void setCdTipoCanalManutencao(int cdTipoCanalManutencao)
    {
        this._cdTipoCanalManutencao = cdTipoCanalManutencao;
        this._has_cdTipoCanalManutencao = true;
    } //-- void setCdTipoCanalManutencao(int) 

    /**
     * Sets the value of field 'cdUsuarioInclusao'.
     * 
     * @param cdUsuarioInclusao the value of field
     * 'cdUsuarioInclusao'.
     */
    public void setCdUsuarioInclusao(java.lang.String cdUsuarioInclusao)
    {
        this._cdUsuarioInclusao = cdUsuarioInclusao;
    } //-- void setCdUsuarioInclusao(java.lang.String) 

    /**
     * Sets the value of field 'cdUsuarioInclusaoExterno'.
     * 
     * @param cdUsuarioInclusaoExterno the value of field
     * 'cdUsuarioInclusaoExterno'.
     */
    public void setCdUsuarioInclusaoExterno(java.lang.String cdUsuarioInclusaoExterno)
    {
        this._cdUsuarioInclusaoExterno = cdUsuarioInclusaoExterno;
    } //-- void setCdUsuarioInclusaoExterno(java.lang.String) 

    /**
     * Sets the value of field 'cdUsuarioManutencao'.
     * 
     * @param cdUsuarioManutencao the value of field
     * 'cdUsuarioManutencao'.
     */
    public void setCdUsuarioManutencao(java.lang.String cdUsuarioManutencao)
    {
        this._cdUsuarioManutencao = cdUsuarioManutencao;
    } //-- void setCdUsuarioManutencao(java.lang.String) 

    /**
     * Sets the value of field 'cdUsuarioManutencaoExterno'.
     * 
     * @param cdUsuarioManutencaoExterno the value of field
     * 'cdUsuarioManutencaoExterno'.
     */
    public void setCdUsuarioManutencaoExterno(java.lang.String cdUsuarioManutencaoExterno)
    {
        this._cdUsuarioManutencaoExterno = cdUsuarioManutencaoExterno;
    } //-- void setCdUsuarioManutencaoExterno(java.lang.String) 

    /**
     * Sets the value of field 'dsMotivoSituacaoAditivo'.
     * 
     * @param dsMotivoSituacaoAditivo the value of field
     * 'dsMotivoSituacaoAditivo'.
     */
    public void setDsMotivoSituacaoAditivo(java.lang.String dsMotivoSituacaoAditivo)
    {
        this._dsMotivoSituacaoAditivo = dsMotivoSituacaoAditivo;
    } //-- void setDsMotivoSituacaoAditivo(java.lang.String) 

    /**
     * Sets the value of field 'dsSituacaoAditivoContrato'.
     * 
     * @param dsSituacaoAditivoContrato the value of field
     * 'dsSituacaoAditivoContrato'.
     */
    public void setDsSituacaoAditivoContrato(java.lang.String dsSituacaoAditivoContrato)
    {
        this._dsSituacaoAditivoContrato = dsSituacaoAditivoContrato;
    } //-- void setDsSituacaoAditivoContrato(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoCanalInclusao'.
     * 
     * @param dsTipoCanalInclusao the value of field
     * 'dsTipoCanalInclusao'.
     */
    public void setDsTipoCanalInclusao(java.lang.String dsTipoCanalInclusao)
    {
        this._dsTipoCanalInclusao = dsTipoCanalInclusao;
    } //-- void setDsTipoCanalInclusao(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoCanalManutencao'.
     * 
     * @param dsTipoCanalManutencao the value of field
     * 'dsTipoCanalManutencao'.
     */
    public void setDsTipoCanalManutencao(java.lang.String dsTipoCanalManutencao)
    {
        this._dsTipoCanalManutencao = dsTipoCanalManutencao;
    } //-- void setDsTipoCanalManutencao(java.lang.String) 

    /**
     * Sets the value of field 'hrAssinaturaAditivoContrato'.
     * 
     * @param hrAssinaturaAditivoContrato the value of field
     * 'hrAssinaturaAditivoContrato'.
     */
    public void setHrAssinaturaAditivoContrato(java.lang.String hrAssinaturaAditivoContrato)
    {
        this._hrAssinaturaAditivoContrato = hrAssinaturaAditivoContrato;
    } //-- void setHrAssinaturaAditivoContrato(java.lang.String) 

    /**
     * Sets the value of field 'hrInclusaoRegistro'.
     * 
     * @param hrInclusaoRegistro the value of field
     * 'hrInclusaoRegistro'.
     */
    public void setHrInclusaoRegistro(java.lang.String hrInclusaoRegistro)
    {
        this._hrInclusaoRegistro = hrInclusaoRegistro;
    } //-- void setHrInclusaoRegistro(java.lang.String) 

    /**
     * Sets the value of field 'hrManutencaoRegistro'.
     * 
     * @param hrManutencaoRegistro the value of field
     * 'hrManutencaoRegistro'.
     */
    public void setHrManutencaoRegistro(java.lang.String hrManutencaoRegistro)
    {
        this._hrManutencaoRegistro = hrManutencaoRegistro;
    } //-- void setHrManutencaoRegistro(java.lang.String) 

    /**
     * Sets the value of field 'nrAditivoContratoNegocio'.
     * 
     * @param nrAditivoContratoNegocio the value of field
     * 'nrAditivoContratoNegocio'.
     */
    public void setNrAditivoContratoNegocio(long nrAditivoContratoNegocio)
    {
        this._nrAditivoContratoNegocio = nrAditivoContratoNegocio;
        this._has_nrAditivoContratoNegocio = true;
    } //-- void setNrAditivoContratoNegocio(long) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.imprimiraditivocontrato.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.imprimiraditivocontrato.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.imprimiraditivocontrato.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimiraditivocontrato.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
