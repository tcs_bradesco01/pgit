/*
 * Nome: br.com.bradesco.web.pgit.service.business.combo.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.combo.bean;

/**
 * Nome: ListarTipoMensagemEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarTipoMensagemEntradaDTO {

	/** Atributo cdControleMensagemSalarial. */
	private Integer cdControleMensagemSalarial;
	
	/** Atributo cdTipoComprovanteSalarial. */
	private Integer cdTipoComprovanteSalarial;	
        
        /** Atributo cdTipoMensagemSalarial. */
        private Integer cdTipoMensagemSalarial;
        
        /** Atributo cdRestMensagemSalarial. */
        private Integer cdRestMensagemSalarial;
	
	/** Atributo numeroOcorrencias. */
	private Integer numeroOcorrencias;
	
	/**
	 * Listar tipo mensagem entrada dto.
	 */
	public ListarTipoMensagemEntradaDTO() {
		super();
	}
	
	/**
	 * Listar tipo mensagem entrada dto.
	 *
	 * @param cdControleMensagemSalarial the cd controle mensagem salarial
	 * @param cdTipoComprovanteSalarial the cd tipo comprovante salarial
	 * @param cdTipoMensagemSalarial the cd tipo mensagem salarial
	 * @param cdRestMensagemComprovante the cd rest mensagem comprovante
	 * @param numeroOcorrencias the numero ocorrencias
	 */
	public ListarTipoMensagemEntradaDTO(Integer cdControleMensagemSalarial, Integer cdTipoComprovanteSalarial, Integer cdTipoMensagemSalarial, Integer cdRestMensagemComprovante, Integer numeroOcorrencias) {
		super();
		this.cdControleMensagemSalarial = cdControleMensagemSalarial;
		this.cdTipoComprovanteSalarial = cdTipoComprovanteSalarial;
		this.cdTipoMensagemSalarial = cdTipoMensagemSalarial;
		this.cdRestMensagemSalarial = cdRestMensagemComprovante;
		this.numeroOcorrencias = numeroOcorrencias;
	}

	/**
	 * Get: cdControleMensagemSalarial.
	 *
	 * @return cdControleMensagemSalarial
	 */
	public Integer getCdControleMensagemSalarial() {
		return cdControleMensagemSalarial;
	}
	
	/**
	 * Set: cdControleMensagemSalarial.
	 *
	 * @param cdControleMensagemSalarial the cd controle mensagem salarial
	 */
	public void setCdControleMensagemSalarial(Integer cdControleMensagemSalarial) {
		this.cdControleMensagemSalarial = cdControleMensagemSalarial;
	}
	
	/**
	 * Get: cdTipoComprovanteSalarial.
	 *
	 * @return cdTipoComprovanteSalarial
	 */
	public Integer getCdTipoComprovanteSalarial() {
		return cdTipoComprovanteSalarial;
	}
	
	/**
	 * Set: cdTipoComprovanteSalarial.
	 *
	 * @param cdTipoComprovanteSalarial the cd tipo comprovante salarial
	 */
	public void setCdTipoComprovanteSalarial(Integer cdTipoComprovanteSalarial) {
		this.cdTipoComprovanteSalarial = cdTipoComprovanteSalarial;
	}
	
	/**
	 * Get: numeroOcorrencias.
	 *
	 * @return numeroOcorrencias
	 */
	public Integer getNumeroOcorrencias() {
		return numeroOcorrencias;
	}
	
	/**
	 * Set: numeroOcorrencias.
	 *
	 * @param numeroOcorrencias the numero ocorrencias
	 */
	public void setNumeroOcorrencias(Integer numeroOcorrencias) {
		this.numeroOcorrencias = numeroOcorrencias;
	}

	/**
	 * Get: cdRestMensagemSalarial.
	 *
	 * @return cdRestMensagemSalarial
	 */
	public Integer getCdRestMensagemSalarial() {
	    return cdRestMensagemSalarial;
	}

	/**
	 * Set: cdRestMensagemSalarial.
	 *
	 * @param cdRestMensagemSalarial the cd rest mensagem salarial
	 */
	public void setCdRestMensagemSalarial(Integer cdRestMensagemSalarial) {
	    this.cdRestMensagemSalarial = cdRestMensagemSalarial;
	}

	/**
	 * Get: cdTipoMensagemSalarial.
	 *
	 * @return cdTipoMensagemSalarial
	 */
	public Integer getCdTipoMensagemSalarial() {
	    return cdTipoMensagemSalarial;
	}

	/**
	 * Set: cdTipoMensagemSalarial.
	 *
	 * @param cdTipoMensagemSalarial the cd tipo mensagem salarial
	 */
	public void setCdTipoMensagemSalarial(Integer cdTipoMensagemSalarial) {
	    this.cdTipoMensagemSalarial = cdTipoMensagemSalarial;
	}
	
	
	
	
	 

}
