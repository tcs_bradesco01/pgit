/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/interfaz.ftl,v $
 * $Id: interfaz.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: interfaz.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.estornarpagamentos;

import java.util.List;

import br.com.bradesco.web.pgit.service.business.estornarpagamentos.bean.ConsultarPagtosEstornoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.estornarpagamentos.bean.ConsultarPagtosEstornoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.estornarpagamentos.bean.EstornarPagamentosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.estornarpagamentos.bean.EstornarPagamentosSaidaDTO;


/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Interface do adaptador: EstornarPagamentos
 * </p>
 * 
 * @comment CODIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public interface IEstornarPagamentosService {
	
	 /**
 	 * Estornar pagamentos.
 	 *
 	 * @param entradaDTO the entrada dto
 	 * @return the estornar pagamentos saida dto
 	 */
 	EstornarPagamentosSaidaDTO estornarPagamentos (EstornarPagamentosEntradaDTO entradaDTO);
	 
 	/**
 	 * Consultar pagtos estorno.
 	 *
 	 * @param consultarPagtosEstornoEntradaDTO the consultar pagtos estorno entrada dto
 	 * @return the list< consultar pagtos estorno saida dt o>
 	 */
 	List<ConsultarPagtosEstornoSaidaDTO> consultarPagtosEstorno(ConsultarPagtosEstornoEntradaDTO consultarPagtosEstornoEntradaDTO);

}

