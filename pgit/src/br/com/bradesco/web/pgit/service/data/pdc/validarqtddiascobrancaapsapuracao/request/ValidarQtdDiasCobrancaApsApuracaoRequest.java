/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.validarqtddiascobrancaapsapuracao.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ValidarQtdDiasCobrancaApsApuracaoRequest.
 * 
 * @version $Revision$ $Date$
 */
public class ValidarQtdDiasCobrancaApsApuracaoRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdPeriodicidadeCobrancaTarifa
     */
    private int _cdPeriodicidadeCobrancaTarifa = 0;

    /**
     * keeps track of state for field: _cdPeriodicidadeCobrancaTarif
     */
    private boolean _has_cdPeriodicidadeCobrancaTarifa;

    /**
     * Field _qtDiaCobrancaTarifa
     */
    private int _qtDiaCobrancaTarifa = 0;

    /**
     * keeps track of state for field: _qtDiaCobrancaTarifa
     */
    private boolean _has_qtDiaCobrancaTarifa;


      //----------------/
     //- Constructors -/
    //----------------/

    public ValidarQtdDiasCobrancaApsApuracaoRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.validarqtddiascobrancaapsapuracao.request.ValidarQtdDiasCobrancaApsApuracaoRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdPeriodicidadeCobrancaTarifa
     * 
     */
    public void deleteCdPeriodicidadeCobrancaTarifa()
    {
        this._has_cdPeriodicidadeCobrancaTarifa= false;
    } //-- void deleteCdPeriodicidadeCobrancaTarifa() 

    /**
     * Method deleteQtDiaCobrancaTarifa
     * 
     */
    public void deleteQtDiaCobrancaTarifa()
    {
        this._has_qtDiaCobrancaTarifa= false;
    } //-- void deleteQtDiaCobrancaTarifa() 

    /**
     * Returns the value of field 'cdPeriodicidadeCobrancaTarifa'.
     * 
     * @return int
     * @return the value of field 'cdPeriodicidadeCobrancaTarifa'.
     */
    public int getCdPeriodicidadeCobrancaTarifa()
    {
        return this._cdPeriodicidadeCobrancaTarifa;
    } //-- int getCdPeriodicidadeCobrancaTarifa() 

    /**
     * Returns the value of field 'qtDiaCobrancaTarifa'.
     * 
     * @return int
     * @return the value of field 'qtDiaCobrancaTarifa'.
     */
    public int getQtDiaCobrancaTarifa()
    {
        return this._qtDiaCobrancaTarifa;
    } //-- int getQtDiaCobrancaTarifa() 

    /**
     * Method hasCdPeriodicidadeCobrancaTarifa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPeriodicidadeCobrancaTarifa()
    {
        return this._has_cdPeriodicidadeCobrancaTarifa;
    } //-- boolean hasCdPeriodicidadeCobrancaTarifa() 

    /**
     * Method hasQtDiaCobrancaTarifa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtDiaCobrancaTarifa()
    {
        return this._has_qtDiaCobrancaTarifa;
    } //-- boolean hasQtDiaCobrancaTarifa() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdPeriodicidadeCobrancaTarifa'.
     * 
     * @param cdPeriodicidadeCobrancaTarifa the value of field
     * 'cdPeriodicidadeCobrancaTarifa'.
     */
    public void setCdPeriodicidadeCobrancaTarifa(int cdPeriodicidadeCobrancaTarifa)
    {
        this._cdPeriodicidadeCobrancaTarifa = cdPeriodicidadeCobrancaTarifa;
        this._has_cdPeriodicidadeCobrancaTarifa = true;
    } //-- void setCdPeriodicidadeCobrancaTarifa(int) 

    /**
     * Sets the value of field 'qtDiaCobrancaTarifa'.
     * 
     * @param qtDiaCobrancaTarifa the value of field
     * 'qtDiaCobrancaTarifa'.
     */
    public void setQtDiaCobrancaTarifa(int qtDiaCobrancaTarifa)
    {
        this._qtDiaCobrancaTarifa = qtDiaCobrancaTarifa;
        this._has_qtDiaCobrancaTarifa = true;
    } //-- void setQtDiaCobrancaTarifa(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ValidarQtdDiasCobrancaApsApuracaoRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.validarqtddiascobrancaapsapuracao.request.ValidarQtdDiasCobrancaApsApuracaoRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.validarqtddiascobrancaapsapuracao.request.ValidarQtdDiasCobrancaApsApuracaoRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.validarqtddiascobrancaapsapuracao.request.ValidarQtdDiasCobrancaApsApuracaoRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.validarqtddiascobrancaapsapuracao.request.ValidarQtdDiasCobrancaApsApuracaoRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
