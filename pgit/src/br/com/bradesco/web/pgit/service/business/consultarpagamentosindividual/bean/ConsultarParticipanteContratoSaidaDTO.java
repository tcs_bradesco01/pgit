/*
 * Nome: br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean;

import br.com.bradesco.web.pgit.utils.CpfCnpjUtils;

/**
 * Nome: ConsultarParticipanteContratoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarParticipanteContratoSaidaDTO {

	/** Atributo cdCpfCnpjParticipante. */
	private Long cdCpfCnpjParticipante;
	
	/** Atributo cdFilialCnpjParticipante. */
	private Integer cdFilialCnpjParticipante;
	
	/** Atributo cdControleCpfParticipante. */
	private Integer cdControleCpfParticipante;
	
	/** Atributo cdParticipante. */
	private Long cdParticipante;
	
	/** Atributo dsParticipante. */
	private String dsParticipante;
	
	/** Atributo dsTipoParticipacao. */
	private String dsTipoParticipacao;
	
	/** Atributo dsSituacaoParticipacao. */
	private String dsSituacaoParticipacao;
	
	/**
	 * Get: cdControleCpfParticipante.
	 *
	 * @return cdControleCpfParticipante
	 */
	public Integer getCdControleCpfParticipante() {
		return cdControleCpfParticipante;
	}
	
	/**
	 * Set: cdControleCpfParticipante.
	 *
	 * @param cdControleCpfParticipante the cd controle cpf participante
	 */
	public void setCdControleCpfParticipante(Integer cdControleCpfParticipante) {
		this.cdControleCpfParticipante = cdControleCpfParticipante;
	}
	
	/**
	 * Get: cdCpfCnpjParticipante.
	 *
	 * @return cdCpfCnpjParticipante
	 */
	public Long getCdCpfCnpjParticipante() {
		return cdCpfCnpjParticipante;
	}
	
	/**
	 * Set: cdCpfCnpjParticipante.
	 *
	 * @param cdCpfCnpjParticipante the cd cpf cnpj participante
	 */
	public void setCdCpfCnpjParticipante(Long cdCpfCnpjParticipante) {
		this.cdCpfCnpjParticipante = cdCpfCnpjParticipante;
	}
	
	/**
	 * Get: cdFilialCnpjParticipante.
	 *
	 * @return cdFilialCnpjParticipante
	 */
	public Integer getCdFilialCnpjParticipante() {
		return cdFilialCnpjParticipante;
	}
	
	/**
	 * Set: cdFilialCnpjParticipante.
	 *
	 * @param cdFilialCnpjParticipante the cd filial cnpj participante
	 */
	public void setCdFilialCnpjParticipante(Integer cdFilialCnpjParticipante) {
		this.cdFilialCnpjParticipante = cdFilialCnpjParticipante;
	}
	
	/**
	 * Get: cdParticipante.
	 *
	 * @return cdParticipante
	 */
	public Long getCdParticipante() {
		return cdParticipante;
	}
	
	/**
	 * Set: cdParticipante.
	 *
	 * @param cdParticipante the cd participante
	 */
	public void setCdParticipante(Long cdParticipante) {
		this.cdParticipante = cdParticipante;
	}
	
	/**
	 * Get: dsParticipante.
	 *
	 * @return dsParticipante
	 */
	public String getDsParticipante() {
		return dsParticipante;
	}
	
	/**
	 * Set: dsParticipante.
	 *
	 * @param dsParticipante the ds participante
	 */
	public void setDsParticipante(String dsParticipante) {
		this.dsParticipante = dsParticipante;
	}
	
	/**
	 * Get: dsSituacaoParticipacao.
	 *
	 * @return dsSituacaoParticipacao
	 */
	public String getDsSituacaoParticipacao() {
		return dsSituacaoParticipacao;
	}
	
	/**
	 * Set: dsSituacaoParticipacao.
	 *
	 * @param dsSituacaoParticipacao the ds situacao participacao
	 */
	public void setDsSituacaoParticipacao(String dsSituacaoParticipacao) {
		this.dsSituacaoParticipacao = dsSituacaoParticipacao;
	}
	
	/**
	 * Get: dsTipoParticipacao.
	 *
	 * @return dsTipoParticipacao
	 */
	public String getDsTipoParticipacao() {
		return dsTipoParticipacao;
	}
	
	/**
	 * Set: dsTipoParticipacao.
	 *
	 * @param dsTipoParticipacao the ds tipo participacao
	 */
	public void setDsTipoParticipacao(String dsTipoParticipacao) {
		this.dsTipoParticipacao = dsTipoParticipacao;
	}
	
	/**
	 * Get: cpfCnpj.
	 *
	 * @return cpfCnpj
	 */
	public String getCpfCnpj(){
		
		return CpfCnpjUtils.formatarCpfCnpj(getCdCpfCnpjParticipante(), getCdFilialCnpjParticipante(), getCdControleCpfParticipante());
		
	}
    
}
