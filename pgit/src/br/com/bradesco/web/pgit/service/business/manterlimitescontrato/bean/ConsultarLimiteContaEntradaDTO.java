package br.com.bradesco.web.pgit.service.business.manterlimitescontrato.bean;


/**
 * Arquivo criado em 22/09/15.
 */
public class ConsultarLimiteContaEntradaDTO {

    private Long cdpessoaJuridicaContrato;

    private Integer cdTipoContratoNegocio;

    private Long nrSequenciaContratoNegocio;

    private Long cdPessoaJuridicaVinculo;

    private Integer cdTipoContratoVinculo;

    private Long nrSequenciaContratoVinculo;

    private Integer cdTipoVincContrato;

    public void setCdpessoaJuridicaContrato(Long cdpessoaJuridicaContrato) {
        this.cdpessoaJuridicaContrato = cdpessoaJuridicaContrato;
    }

    public Long getCdpessoaJuridicaContrato() {
        return this.cdpessoaJuridicaContrato;
    }

    public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
        this.cdTipoContratoNegocio = cdTipoContratoNegocio;
    }

    public Integer getCdTipoContratoNegocio() {
        return this.cdTipoContratoNegocio;
    }

    public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
        this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
    }

    public Long getNrSequenciaContratoNegocio() {
        return this.nrSequenciaContratoNegocio;
    }

    public void setCdPessoaJuridicaVinculo(Long cdPessoaJuridicaVinculo) {
        this.cdPessoaJuridicaVinculo = cdPessoaJuridicaVinculo;
    }

    public Long getCdPessoaJuridicaVinculo() {
        return this.cdPessoaJuridicaVinculo;
    }

    public void setCdTipoContratoVinculo(Integer cdTipoContratoVinculo) {
        this.cdTipoContratoVinculo = cdTipoContratoVinculo;
    }

    public Integer getCdTipoContratoVinculo() {
        return this.cdTipoContratoVinculo;
    }

    public void setNrSequenciaContratoVinculo(Long nrSequenciaContratoVinculo) {
        this.nrSequenciaContratoVinculo = nrSequenciaContratoVinculo;
    }

    public Long getNrSequenciaContratoVinculo() {
        return this.nrSequenciaContratoVinculo;
    }

    public void setCdTipoVincContrato(Integer cdTipoVincContrato) {
        this.cdTipoVincContrato = cdTipoVincContrato;
    }

    public Integer getCdTipoVincContrato() {
        return this.cdTipoVincContrato;
    }
}