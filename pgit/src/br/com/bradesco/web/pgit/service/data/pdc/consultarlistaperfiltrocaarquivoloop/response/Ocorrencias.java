/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultarlistaperfiltrocaarquivoloop.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdTipoLayoutArquivo
     */
    private int _cdTipoLayoutArquivo = 0;

    /**
     * keeps track of state for field: _cdTipoLayoutArquivo
     */
    private boolean _has_cdTipoLayoutArquivo;

    /**
     * Field _cdPerfilTrocaArquivo
     */
    private long _cdPerfilTrocaArquivo = 0;

    /**
     * keeps track of state for field: _cdPerfilTrocaArquivo
     */
    private boolean _has_cdPerfilTrocaArquivo;

    /**
     * Field _dsTipoLayoutArquivo
     */
    private java.lang.String _dsTipoLayoutArquivo;

    /**
     * Field _dsLayoutProprio
     */
    private java.lang.String _dsLayoutProprio;

    /**
     * Field _cdProdutoServicoOperacao
     */
    private int _cdProdutoServicoOperacao = 0;

    /**
     * keeps track of state for field: _cdProdutoServicoOperacao
     */
    private boolean _has_cdProdutoServicoOperacao;

    /**
     * Field _cdProdutoOperacaoRelacionado
     */
    private int _cdProdutoOperacaoRelacionado = 0;

    /**
     * keeps track of state for field: _cdProdutoOperacaoRelacionado
     */
    private boolean _has_cdProdutoOperacaoRelacionado;

    /**
     * Field _cdRelacionamentoProduto
     */
    private int _cdRelacionamentoProduto = 0;

    /**
     * keeps track of state for field: _cdRelacionamentoProduto
     */
    private boolean _has_cdRelacionamentoProduto;

    /**
     * Field _dsProdutoServico
     */
    private java.lang.String _dsProdutoServico;

    /**
     * Field _cdAplicacaoTransmicaoPagamento
     */
    private int _cdAplicacaoTransmicaoPagamento = 0;

    /**
     * keeps track of state for field:
     * _cdAplicacaoTransmicaoPagamento
     */
    private boolean _has_cdAplicacaoTransmicaoPagamento;

    /**
     * Field _dsAplicacaoTransmicaoPagamento
     */
    private java.lang.String _dsAplicacaoTransmicaoPagamento;

    /**
     * Field _cdMeioPrincipalRemessa
     */
    private int _cdMeioPrincipalRemessa = 0;

    /**
     * keeps track of state for field: _cdMeioPrincipalRemessa
     */
    private boolean _has_cdMeioPrincipalRemessa;

    /**
     * Field _dsMeioPrincRemss
     */
    private java.lang.String _dsMeioPrincRemss;

    /**
     * Field _cdmeioAlternativoRemessa
     */
    private int _cdmeioAlternativoRemessa = 0;

    /**
     * keeps track of state for field: _cdmeioAlternativoRemessa
     */
    private boolean _has_cdmeioAlternativoRemessa;

    /**
     * Field _dsMeioAltrnRemss
     */
    private java.lang.String _dsMeioAltrnRemss;

    /**
     * Field _cdMeioPrincipalRetorno
     */
    private int _cdMeioPrincipalRetorno = 0;

    /**
     * keeps track of state for field: _cdMeioPrincipalRetorno
     */
    private boolean _has_cdMeioPrincipalRetorno;

    /**
     * Field _dsMeioPrincRetor
     */
    private java.lang.String _dsMeioPrincRetor;

    /**
     * Field _cdMeioAlternativoRetorno
     */
    private int _cdMeioAlternativoRetorno = 0;

    /**
     * keeps track of state for field: _cdMeioAlternativoRetorno
     */
    private boolean _has_cdMeioAlternativoRetorno;

    /**
     * Field _dsMeioAltrnRetor
     */
    private java.lang.String _dsMeioAltrnRetor;

    /**
     * Field _cdPessoaJuridicaParceiro
     */
    private long _cdPessoaJuridicaParceiro = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridicaParceiro
     */
    private boolean _has_cdPessoaJuridicaParceiro;

    /**
     * Field _dsRzScialPcero
     */
    private java.lang.String _dsRzScialPcero;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarlistaperfiltrocaarquivoloop.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdAplicacaoTransmicaoPagamento
     * 
     */
    public void deleteCdAplicacaoTransmicaoPagamento()
    {
        this._has_cdAplicacaoTransmicaoPagamento= false;
    } //-- void deleteCdAplicacaoTransmicaoPagamento() 

    /**
     * Method deleteCdMeioAlternativoRetorno
     * 
     */
    public void deleteCdMeioAlternativoRetorno()
    {
        this._has_cdMeioAlternativoRetorno= false;
    } //-- void deleteCdMeioAlternativoRetorno() 

    /**
     * Method deleteCdMeioPrincipalRemessa
     * 
     */
    public void deleteCdMeioPrincipalRemessa()
    {
        this._has_cdMeioPrincipalRemessa= false;
    } //-- void deleteCdMeioPrincipalRemessa() 

    /**
     * Method deleteCdMeioPrincipalRetorno
     * 
     */
    public void deleteCdMeioPrincipalRetorno()
    {
        this._has_cdMeioPrincipalRetorno= false;
    } //-- void deleteCdMeioPrincipalRetorno() 

    /**
     * Method deleteCdPerfilTrocaArquivo
     * 
     */
    public void deleteCdPerfilTrocaArquivo()
    {
        this._has_cdPerfilTrocaArquivo= false;
    } //-- void deleteCdPerfilTrocaArquivo() 

    /**
     * Method deleteCdPessoaJuridicaParceiro
     * 
     */
    public void deleteCdPessoaJuridicaParceiro()
    {
        this._has_cdPessoaJuridicaParceiro= false;
    } //-- void deleteCdPessoaJuridicaParceiro() 

    /**
     * Method deleteCdProdutoOperacaoRelacionado
     * 
     */
    public void deleteCdProdutoOperacaoRelacionado()
    {
        this._has_cdProdutoOperacaoRelacionado= false;
    } //-- void deleteCdProdutoOperacaoRelacionado() 

    /**
     * Method deleteCdProdutoServicoOperacao
     * 
     */
    public void deleteCdProdutoServicoOperacao()
    {
        this._has_cdProdutoServicoOperacao= false;
    } //-- void deleteCdProdutoServicoOperacao() 

    /**
     * Method deleteCdRelacionamentoProduto
     * 
     */
    public void deleteCdRelacionamentoProduto()
    {
        this._has_cdRelacionamentoProduto= false;
    } //-- void deleteCdRelacionamentoProduto() 

    /**
     * Method deleteCdTipoLayoutArquivo
     * 
     */
    public void deleteCdTipoLayoutArquivo()
    {
        this._has_cdTipoLayoutArquivo= false;
    } //-- void deleteCdTipoLayoutArquivo() 

    /**
     * Method deleteCdmeioAlternativoRemessa
     * 
     */
    public void deleteCdmeioAlternativoRemessa()
    {
        this._has_cdmeioAlternativoRemessa= false;
    } //-- void deleteCdmeioAlternativoRemessa() 

    /**
     * Returns the value of field 'cdAplicacaoTransmicaoPagamento'.
     * 
     * @return int
     * @return the value of field 'cdAplicacaoTransmicaoPagamento'.
     */
    public int getCdAplicacaoTransmicaoPagamento()
    {
        return this._cdAplicacaoTransmicaoPagamento;
    } //-- int getCdAplicacaoTransmicaoPagamento() 

    /**
     * Returns the value of field 'cdMeioAlternativoRetorno'.
     * 
     * @return int
     * @return the value of field 'cdMeioAlternativoRetorno'.
     */
    public int getCdMeioAlternativoRetorno()
    {
        return this._cdMeioAlternativoRetorno;
    } //-- int getCdMeioAlternativoRetorno() 

    /**
     * Returns the value of field 'cdMeioPrincipalRemessa'.
     * 
     * @return int
     * @return the value of field 'cdMeioPrincipalRemessa'.
     */
    public int getCdMeioPrincipalRemessa()
    {
        return this._cdMeioPrincipalRemessa;
    } //-- int getCdMeioPrincipalRemessa() 

    /**
     * Returns the value of field 'cdMeioPrincipalRetorno'.
     * 
     * @return int
     * @return the value of field 'cdMeioPrincipalRetorno'.
     */
    public int getCdMeioPrincipalRetorno()
    {
        return this._cdMeioPrincipalRetorno;
    } //-- int getCdMeioPrincipalRetorno() 

    /**
     * Returns the value of field 'cdPerfilTrocaArquivo'.
     * 
     * @return long
     * @return the value of field 'cdPerfilTrocaArquivo'.
     */
    public long getCdPerfilTrocaArquivo()
    {
        return this._cdPerfilTrocaArquivo;
    } //-- long getCdPerfilTrocaArquivo() 

    /**
     * Returns the value of field 'cdPessoaJuridicaParceiro'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridicaParceiro'.
     */
    public long getCdPessoaJuridicaParceiro()
    {
        return this._cdPessoaJuridicaParceiro;
    } //-- long getCdPessoaJuridicaParceiro() 

    /**
     * Returns the value of field 'cdProdutoOperacaoRelacionado'.
     * 
     * @return int
     * @return the value of field 'cdProdutoOperacaoRelacionado'.
     */
    public int getCdProdutoOperacaoRelacionado()
    {
        return this._cdProdutoOperacaoRelacionado;
    } //-- int getCdProdutoOperacaoRelacionado() 

    /**
     * Returns the value of field 'cdProdutoServicoOperacao'.
     * 
     * @return int
     * @return the value of field 'cdProdutoServicoOperacao'.
     */
    public int getCdProdutoServicoOperacao()
    {
        return this._cdProdutoServicoOperacao;
    } //-- int getCdProdutoServicoOperacao() 

    /**
     * Returns the value of field 'cdRelacionamentoProduto'.
     * 
     * @return int
     * @return the value of field 'cdRelacionamentoProduto'.
     */
    public int getCdRelacionamentoProduto()
    {
        return this._cdRelacionamentoProduto;
    } //-- int getCdRelacionamentoProduto() 

    /**
     * Returns the value of field 'cdTipoLayoutArquivo'.
     * 
     * @return int
     * @return the value of field 'cdTipoLayoutArquivo'.
     */
    public int getCdTipoLayoutArquivo()
    {
        return this._cdTipoLayoutArquivo;
    } //-- int getCdTipoLayoutArquivo() 

    /**
     * Returns the value of field 'cdmeioAlternativoRemessa'.
     * 
     * @return int
     * @return the value of field 'cdmeioAlternativoRemessa'.
     */
    public int getCdmeioAlternativoRemessa()
    {
        return this._cdmeioAlternativoRemessa;
    } //-- int getCdmeioAlternativoRemessa() 

    /**
     * Returns the value of field 'dsAplicacaoTransmicaoPagamento'.
     * 
     * @return String
     * @return the value of field 'dsAplicacaoTransmicaoPagamento'.
     */
    public java.lang.String getDsAplicacaoTransmicaoPagamento()
    {
        return this._dsAplicacaoTransmicaoPagamento;
    } //-- java.lang.String getDsAplicacaoTransmicaoPagamento() 

    /**
     * Returns the value of field 'dsLayoutProprio'.
     * 
     * @return String
     * @return the value of field 'dsLayoutProprio'.
     */
    public java.lang.String getDsLayoutProprio()
    {
        return this._dsLayoutProprio;
    } //-- java.lang.String getDsLayoutProprio() 

    /**
     * Returns the value of field 'dsMeioAltrnRemss'.
     * 
     * @return String
     * @return the value of field 'dsMeioAltrnRemss'.
     */
    public java.lang.String getDsMeioAltrnRemss()
    {
        return this._dsMeioAltrnRemss;
    } //-- java.lang.String getDsMeioAltrnRemss() 

    /**
     * Returns the value of field 'dsMeioAltrnRetor'.
     * 
     * @return String
     * @return the value of field 'dsMeioAltrnRetor'.
     */
    public java.lang.String getDsMeioAltrnRetor()
    {
        return this._dsMeioAltrnRetor;
    } //-- java.lang.String getDsMeioAltrnRetor() 

    /**
     * Returns the value of field 'dsMeioPrincRemss'.
     * 
     * @return String
     * @return the value of field 'dsMeioPrincRemss'.
     */
    public java.lang.String getDsMeioPrincRemss()
    {
        return this._dsMeioPrincRemss;
    } //-- java.lang.String getDsMeioPrincRemss() 

    /**
     * Returns the value of field 'dsMeioPrincRetor'.
     * 
     * @return String
     * @return the value of field 'dsMeioPrincRetor'.
     */
    public java.lang.String getDsMeioPrincRetor()
    {
        return this._dsMeioPrincRetor;
    } //-- java.lang.String getDsMeioPrincRetor() 

    /**
     * Returns the value of field 'dsProdutoServico'.
     * 
     * @return String
     * @return the value of field 'dsProdutoServico'.
     */
    public java.lang.String getDsProdutoServico()
    {
        return this._dsProdutoServico;
    } //-- java.lang.String getDsProdutoServico() 

    /**
     * Returns the value of field 'dsRzScialPcero'.
     * 
     * @return String
     * @return the value of field 'dsRzScialPcero'.
     */
    public java.lang.String getDsRzScialPcero()
    {
        return this._dsRzScialPcero;
    } //-- java.lang.String getDsRzScialPcero() 

    /**
     * Returns the value of field 'dsTipoLayoutArquivo'.
     * 
     * @return String
     * @return the value of field 'dsTipoLayoutArquivo'.
     */
    public java.lang.String getDsTipoLayoutArquivo()
    {
        return this._dsTipoLayoutArquivo;
    } //-- java.lang.String getDsTipoLayoutArquivo() 

    /**
     * Method hasCdAplicacaoTransmicaoPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAplicacaoTransmicaoPagamento()
    {
        return this._has_cdAplicacaoTransmicaoPagamento;
    } //-- boolean hasCdAplicacaoTransmicaoPagamento() 

    /**
     * Method hasCdMeioAlternativoRetorno
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMeioAlternativoRetorno()
    {
        return this._has_cdMeioAlternativoRetorno;
    } //-- boolean hasCdMeioAlternativoRetorno() 

    /**
     * Method hasCdMeioPrincipalRemessa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMeioPrincipalRemessa()
    {
        return this._has_cdMeioPrincipalRemessa;
    } //-- boolean hasCdMeioPrincipalRemessa() 

    /**
     * Method hasCdMeioPrincipalRetorno
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMeioPrincipalRetorno()
    {
        return this._has_cdMeioPrincipalRetorno;
    } //-- boolean hasCdMeioPrincipalRetorno() 

    /**
     * Method hasCdPerfilTrocaArquivo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPerfilTrocaArquivo()
    {
        return this._has_cdPerfilTrocaArquivo;
    } //-- boolean hasCdPerfilTrocaArquivo() 

    /**
     * Method hasCdPessoaJuridicaParceiro
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridicaParceiro()
    {
        return this._has_cdPessoaJuridicaParceiro;
    } //-- boolean hasCdPessoaJuridicaParceiro() 

    /**
     * Method hasCdProdutoOperacaoRelacionado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdutoOperacaoRelacionado()
    {
        return this._has_cdProdutoOperacaoRelacionado;
    } //-- boolean hasCdProdutoOperacaoRelacionado() 

    /**
     * Method hasCdProdutoServicoOperacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdutoServicoOperacao()
    {
        return this._has_cdProdutoServicoOperacao;
    } //-- boolean hasCdProdutoServicoOperacao() 

    /**
     * Method hasCdRelacionamentoProduto
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdRelacionamentoProduto()
    {
        return this._has_cdRelacionamentoProduto;
    } //-- boolean hasCdRelacionamentoProduto() 

    /**
     * Method hasCdTipoLayoutArquivo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoLayoutArquivo()
    {
        return this._has_cdTipoLayoutArquivo;
    } //-- boolean hasCdTipoLayoutArquivo() 

    /**
     * Method hasCdmeioAlternativoRemessa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdmeioAlternativoRemessa()
    {
        return this._has_cdmeioAlternativoRemessa;
    } //-- boolean hasCdmeioAlternativoRemessa() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdAplicacaoTransmicaoPagamento'.
     * 
     * @param cdAplicacaoTransmicaoPagamento the value of field
     * 'cdAplicacaoTransmicaoPagamento'.
     */
    public void setCdAplicacaoTransmicaoPagamento(int cdAplicacaoTransmicaoPagamento)
    {
        this._cdAplicacaoTransmicaoPagamento = cdAplicacaoTransmicaoPagamento;
        this._has_cdAplicacaoTransmicaoPagamento = true;
    } //-- void setCdAplicacaoTransmicaoPagamento(int) 

    /**
     * Sets the value of field 'cdMeioAlternativoRetorno'.
     * 
     * @param cdMeioAlternativoRetorno the value of field
     * 'cdMeioAlternativoRetorno'.
     */
    public void setCdMeioAlternativoRetorno(int cdMeioAlternativoRetorno)
    {
        this._cdMeioAlternativoRetorno = cdMeioAlternativoRetorno;
        this._has_cdMeioAlternativoRetorno = true;
    } //-- void setCdMeioAlternativoRetorno(int) 

    /**
     * Sets the value of field 'cdMeioPrincipalRemessa'.
     * 
     * @param cdMeioPrincipalRemessa the value of field
     * 'cdMeioPrincipalRemessa'.
     */
    public void setCdMeioPrincipalRemessa(int cdMeioPrincipalRemessa)
    {
        this._cdMeioPrincipalRemessa = cdMeioPrincipalRemessa;
        this._has_cdMeioPrincipalRemessa = true;
    } //-- void setCdMeioPrincipalRemessa(int) 

    /**
     * Sets the value of field 'cdMeioPrincipalRetorno'.
     * 
     * @param cdMeioPrincipalRetorno the value of field
     * 'cdMeioPrincipalRetorno'.
     */
    public void setCdMeioPrincipalRetorno(int cdMeioPrincipalRetorno)
    {
        this._cdMeioPrincipalRetorno = cdMeioPrincipalRetorno;
        this._has_cdMeioPrincipalRetorno = true;
    } //-- void setCdMeioPrincipalRetorno(int) 

    /**
     * Sets the value of field 'cdPerfilTrocaArquivo'.
     * 
     * @param cdPerfilTrocaArquivo the value of field
     * 'cdPerfilTrocaArquivo'.
     */
    public void setCdPerfilTrocaArquivo(long cdPerfilTrocaArquivo)
    {
        this._cdPerfilTrocaArquivo = cdPerfilTrocaArquivo;
        this._has_cdPerfilTrocaArquivo = true;
    } //-- void setCdPerfilTrocaArquivo(long) 

    /**
     * Sets the value of field 'cdPessoaJuridicaParceiro'.
     * 
     * @param cdPessoaJuridicaParceiro the value of field
     * 'cdPessoaJuridicaParceiro'.
     */
    public void setCdPessoaJuridicaParceiro(long cdPessoaJuridicaParceiro)
    {
        this._cdPessoaJuridicaParceiro = cdPessoaJuridicaParceiro;
        this._has_cdPessoaJuridicaParceiro = true;
    } //-- void setCdPessoaJuridicaParceiro(long) 

    /**
     * Sets the value of field 'cdProdutoOperacaoRelacionado'.
     * 
     * @param cdProdutoOperacaoRelacionado the value of field
     * 'cdProdutoOperacaoRelacionado'.
     */
    public void setCdProdutoOperacaoRelacionado(int cdProdutoOperacaoRelacionado)
    {
        this._cdProdutoOperacaoRelacionado = cdProdutoOperacaoRelacionado;
        this._has_cdProdutoOperacaoRelacionado = true;
    } //-- void setCdProdutoOperacaoRelacionado(int) 

    /**
     * Sets the value of field 'cdProdutoServicoOperacao'.
     * 
     * @param cdProdutoServicoOperacao the value of field
     * 'cdProdutoServicoOperacao'.
     */
    public void setCdProdutoServicoOperacao(int cdProdutoServicoOperacao)
    {
        this._cdProdutoServicoOperacao = cdProdutoServicoOperacao;
        this._has_cdProdutoServicoOperacao = true;
    } //-- void setCdProdutoServicoOperacao(int) 

    /**
     * Sets the value of field 'cdRelacionamentoProduto'.
     * 
     * @param cdRelacionamentoProduto the value of field
     * 'cdRelacionamentoProduto'.
     */
    public void setCdRelacionamentoProduto(int cdRelacionamentoProduto)
    {
        this._cdRelacionamentoProduto = cdRelacionamentoProduto;
        this._has_cdRelacionamentoProduto = true;
    } //-- void setCdRelacionamentoProduto(int) 

    /**
     * Sets the value of field 'cdTipoLayoutArquivo'.
     * 
     * @param cdTipoLayoutArquivo the value of field
     * 'cdTipoLayoutArquivo'.
     */
    public void setCdTipoLayoutArquivo(int cdTipoLayoutArquivo)
    {
        this._cdTipoLayoutArquivo = cdTipoLayoutArquivo;
        this._has_cdTipoLayoutArquivo = true;
    } //-- void setCdTipoLayoutArquivo(int) 

    /**
     * Sets the value of field 'cdmeioAlternativoRemessa'.
     * 
     * @param cdmeioAlternativoRemessa the value of field
     * 'cdmeioAlternativoRemessa'.
     */
    public void setCdmeioAlternativoRemessa(int cdmeioAlternativoRemessa)
    {
        this._cdmeioAlternativoRemessa = cdmeioAlternativoRemessa;
        this._has_cdmeioAlternativoRemessa = true;
    } //-- void setCdmeioAlternativoRemessa(int) 

    /**
     * Sets the value of field 'dsAplicacaoTransmicaoPagamento'.
     * 
     * @param dsAplicacaoTransmicaoPagamento the value of field
     * 'dsAplicacaoTransmicaoPagamento'.
     */
    public void setDsAplicacaoTransmicaoPagamento(java.lang.String dsAplicacaoTransmicaoPagamento)
    {
        this._dsAplicacaoTransmicaoPagamento = dsAplicacaoTransmicaoPagamento;
    } //-- void setDsAplicacaoTransmicaoPagamento(java.lang.String) 

    /**
     * Sets the value of field 'dsLayoutProprio'.
     * 
     * @param dsLayoutProprio the value of field 'dsLayoutProprio'.
     */
    public void setDsLayoutProprio(java.lang.String dsLayoutProprio)
    {
        this._dsLayoutProprio = dsLayoutProprio;
    } //-- void setDsLayoutProprio(java.lang.String) 

    /**
     * Sets the value of field 'dsMeioAltrnRemss'.
     * 
     * @param dsMeioAltrnRemss the value of field 'dsMeioAltrnRemss'
     */
    public void setDsMeioAltrnRemss(java.lang.String dsMeioAltrnRemss)
    {
        this._dsMeioAltrnRemss = dsMeioAltrnRemss;
    } //-- void setDsMeioAltrnRemss(java.lang.String) 

    /**
     * Sets the value of field 'dsMeioAltrnRetor'.
     * 
     * @param dsMeioAltrnRetor the value of field 'dsMeioAltrnRetor'
     */
    public void setDsMeioAltrnRetor(java.lang.String dsMeioAltrnRetor)
    {
        this._dsMeioAltrnRetor = dsMeioAltrnRetor;
    } //-- void setDsMeioAltrnRetor(java.lang.String) 

    /**
     * Sets the value of field 'dsMeioPrincRemss'.
     * 
     * @param dsMeioPrincRemss the value of field 'dsMeioPrincRemss'
     */
    public void setDsMeioPrincRemss(java.lang.String dsMeioPrincRemss)
    {
        this._dsMeioPrincRemss = dsMeioPrincRemss;
    } //-- void setDsMeioPrincRemss(java.lang.String) 

    /**
     * Sets the value of field 'dsMeioPrincRetor'.
     * 
     * @param dsMeioPrincRetor the value of field 'dsMeioPrincRetor'
     */
    public void setDsMeioPrincRetor(java.lang.String dsMeioPrincRetor)
    {
        this._dsMeioPrincRetor = dsMeioPrincRetor;
    } //-- void setDsMeioPrincRetor(java.lang.String) 

    /**
     * Sets the value of field 'dsProdutoServico'.
     * 
     * @param dsProdutoServico the value of field 'dsProdutoServico'
     */
    public void setDsProdutoServico(java.lang.String dsProdutoServico)
    {
        this._dsProdutoServico = dsProdutoServico;
    } //-- void setDsProdutoServico(java.lang.String) 

    /**
     * Sets the value of field 'dsRzScialPcero'.
     * 
     * @param dsRzScialPcero the value of field 'dsRzScialPcero'.
     */
    public void setDsRzScialPcero(java.lang.String dsRzScialPcero)
    {
        this._dsRzScialPcero = dsRzScialPcero;
    } //-- void setDsRzScialPcero(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoLayoutArquivo'.
     * 
     * @param dsTipoLayoutArquivo the value of field
     * 'dsTipoLayoutArquivo'.
     */
    public void setDsTipoLayoutArquivo(java.lang.String dsTipoLayoutArquivo)
    {
        this._dsTipoLayoutArquivo = dsTipoLayoutArquivo;
    } //-- void setDsTipoLayoutArquivo(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultarlistaperfiltrocaarquivoloop.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultarlistaperfiltrocaarquivoloop.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultarlistaperfiltrocaarquivoloop.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarlistaperfiltrocaarquivoloop.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
