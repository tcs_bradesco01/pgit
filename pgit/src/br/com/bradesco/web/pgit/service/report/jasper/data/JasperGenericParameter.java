/*
 * Nome: br.com.bradesco.web.pgit.service.report.jasper.data
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 05/02/2016
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.report.jasper.data;

/**
 * Nome: JasperGenericParameter
 * <p>
 * Prop�sito:
 * </p>
 * 	
 * @author : todo!
 * 
 * @version :
 */
public interface JasperGenericParameter {

    Object value(String property);
}
