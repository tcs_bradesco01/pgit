/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantersolicgeracaoarqretbasepagtos.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantersolicgeracaoarqretbasepagtos.bean;

import java.math.BigDecimal;

/**
 * Nome: DetalharSolBasePagtoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class DetalharSolBasePagtoSaidaDTO {
	
	/** Atributo codMensagem. */
	private String codMensagem;
	
	/** Atributo mensagem. */
	private String mensagem;
	
	/** Atributo nrSolicitacaoPagamento. */
	private Integer nrSolicitacaoPagamento;
	
	/** Atributo hrSolicitacaoPagamento. */
	private String hrSolicitacaoPagamento;
	
	/** Atributo dsSituacaoSolicitacao. */
	private String dsSituacaoSolicitacao;
	
	/** Atributo resultadoProcessamento. */
	private String resultadoProcessamento;
	
	/** Atributo hrAtendimentoSolicitacao. */
	private String hrAtendimentoSolicitacao;
	
	/** Atributo qtdeRegistrosSolicitacao. */
	private Long qtdeRegistrosSolicitacao;
	
	/** Atributo dtInicioPagamento. */
	private String dtInicioPagamento;
	
	/** Atributo dtFimPagamento. */
	private String dtFimPagamento;
	
	/** Atributo cdServico. */
	private String cdServico;
	
	/** Atributo cdServicoRelacionado. */
	private String cdServicoRelacionado;
	
	/** Atributo cdBancoDebito. */
	private Integer cdBancoDebito;
	
	/** Atributo cdAgenciaDebito. */
	private Integer cdAgenciaDebito;
	
	/** Atributo digitoAgenciaDebito. */
	private Integer digitoAgenciaDebito;
	
	/** Atributo cdContaDebito. */
	private Long cdContaDebito;
	
	/** Atributo digitoContaDebito. */
	private String digitoContaDebito;
	
	/** Atributo indicadorPagos. */
	private String indicadorPagos;
	
	/** Atributo indicadorNaoPagos. */
	private String indicadorNaoPagos;
	
	/** Atributo indicadorAgendados. */
	private String indicadorAgendados;
	
	/** Atributo cdOrigem. */
	private Integer cdOrigem;
	
	/** Atributo cdDestino. */
	private Integer cdDestino;
	
	/** Atributo dsFavorecido. */
	private String dsFavorecido;
	
	/** Atributo cdFavorecido. */
	private Long cdFavorecido;
	
	/** Atributo cdInscricaoFavorecido. */
	private Long cdInscricaoFavorecido;
	
	/** Atributo cdTipoInscricaoFavorecido. */
	private String cdTipoInscricaoFavorecido;
	
	/** Atributo nmMinemonicoFavorecido. */
	private String nmMinemonicoFavorecido;
	
	/** Atributo vlTarifaPadrao. */
	private BigDecimal vlTarifaPadrao;
	
	/** Atributo numPercentualDescTarifa. */
	private BigDecimal numPercentualDescTarifa;
	
	/** Atributo vlrTarifaAtual. */
	private BigDecimal vlrTarifaAtual;
	
	/** Atributo cdCananInclusao. */
	private Integer cdCananInclusao;
	
	/** Atributo dsCanalInclusao. */
	private String dsCanalInclusao;
	
	/** Atributo cdAutenticacaoSegurancaInclusao. */
	private String cdAutenticacaoSegurancaInclusao;
	
	/** Atributo nmOperacaoFluxoInclusao. */
	private String nmOperacaoFluxoInclusao;
	
	/** Atributo hrInclusaoRegistro. */
	private String hrInclusaoRegistro;
	
	/** Atributo cdCanalManutencao. */
	private Integer cdCanalManutencao;
	
	/** Atributo dsCanalManutencao. */
	private String dsCanalManutencao;
	
	/** Atributo cdAutenticacaoSegurancaManutencao. */
	private String cdAutenticacaoSegurancaManutencao;
	
	/** Atributo nmOperacaoFluxoManutencao. */
	private String nmOperacaoFluxoManutencao;
	
	/** Atributo hrManutencaoRegistro. */
	private String hrManutencaoRegistro;
	
	/** Atributo hrSolicitacaoPagamentoFormatada. */
	private String hrSolicitacaoPagamentoFormatada;
	
	/** Atributo hrAtendimentoSolicitacaoFormatada. */
	private String hrAtendimentoSolicitacaoFormatada;
	
	/** Atributo hrInclusaoRegistroFormatada. */
	private String hrInclusaoRegistroFormatada;
	
	/** Atributo hrManutencaoRegistroFormatada. */
	private String hrManutencaoRegistroFormatada;
	
	/** Atributo contaDebitoFormatado. */
	private String contaDebitoFormatado;

	/**
	 * Get: cdAgenciaDebito.
	 *
	 * @return cdAgenciaDebito
	 */
	public Integer getCdAgenciaDebito() {
		return cdAgenciaDebito;
	}

	/**
	 * Set: cdAgenciaDebito.
	 *
	 * @param cdAgenciaDebito the cd agencia debito
	 */
	public void setCdAgenciaDebito(Integer cdAgenciaDebito) {
		this.cdAgenciaDebito = cdAgenciaDebito;
	}

	/**
	 * Get: cdAutenticacaoSegurancaInclusao.
	 *
	 * @return cdAutenticacaoSegurancaInclusao
	 */
	public String getCdAutenticacaoSegurancaInclusao() {
		return cdAutenticacaoSegurancaInclusao;
	}

	/**
	 * Set: cdAutenticacaoSegurancaInclusao.
	 *
	 * @param cdAutenticacaoSegurancaInclusao the cd autenticacao seguranca inclusao
	 */
	public void setCdAutenticacaoSegurancaInclusao(
			String cdAutenticacaoSegurancaInclusao) {
		this.cdAutenticacaoSegurancaInclusao = cdAutenticacaoSegurancaInclusao;
	}

	/**
	 * Get: cdAutenticacaoSegurancaManutencao.
	 *
	 * @return cdAutenticacaoSegurancaManutencao
	 */
	public String getCdAutenticacaoSegurancaManutencao() {
		return cdAutenticacaoSegurancaManutencao;
	}

	/**
	 * Set: cdAutenticacaoSegurancaManutencao.
	 *
	 * @param cdAutenticacaoSegurancaManutencao the cd autenticacao seguranca manutencao
	 */
	public void setCdAutenticacaoSegurancaManutencao(
			String cdAutenticacaoSegurancaManutencao) {
		this.cdAutenticacaoSegurancaManutencao = cdAutenticacaoSegurancaManutencao;
	}

	/**
	 * Get: cdBancoDebito.
	 *
	 * @return cdBancoDebito
	 */
	public Integer getCdBancoDebito() {
		return cdBancoDebito;
	}

	/**
	 * Set: cdBancoDebito.
	 *
	 * @param cdBancoDebito the cd banco debito
	 */
	public void setCdBancoDebito(Integer cdBancoDebito) {
		this.cdBancoDebito = cdBancoDebito;
	}

	/**
	 * Get: cdCanalManutencao.
	 *
	 * @return cdCanalManutencao
	 */
	public Integer getCdCanalManutencao() {
		return cdCanalManutencao;
	}

	/**
	 * Set: cdCanalManutencao.
	 *
	 * @param cdCanalManutencao the cd canal manutencao
	 */
	public void setCdCanalManutencao(Integer cdCanalManutencao) {
		this.cdCanalManutencao = cdCanalManutencao;
	}

	/**
	 * Get: cdCananInclusao.
	 *
	 * @return cdCananInclusao
	 */
	public Integer getCdCananInclusao() {
		return cdCananInclusao;
	}

	/**
	 * Set: cdCananInclusao.
	 *
	 * @param cdCananInclusao the cd canan inclusao
	 */
	public void setCdCananInclusao(Integer cdCananInclusao) {
		this.cdCananInclusao = cdCananInclusao;
	}

	/**
	 * Get: cdContaDebito.
	 *
	 * @return cdContaDebito
	 */
	public Long getCdContaDebito() {
		return cdContaDebito;
	}

	/**
	 * Set: cdContaDebito.
	 *
	 * @param cdContaDebito the cd conta debito
	 */
	public void setCdContaDebito(Long cdContaDebito) {
		this.cdContaDebito = cdContaDebito;
	}

	/**
	 * Get: cdDestino.
	 *
	 * @return cdDestino
	 */
	public Integer getCdDestino() {
		return cdDestino;
	}

	/**
	 * Set: cdDestino.
	 *
	 * @param cdDestino the cd destino
	 */
	public void setCdDestino(Integer cdDestino) {
		this.cdDestino = cdDestino;
	}

	/**
	 * Get: cdFavorecido.
	 *
	 * @return cdFavorecido
	 */
	public Long getCdFavorecido() {
		return cdFavorecido;
	}

	/**
	 * Set: cdFavorecido.
	 *
	 * @param cdFavorecido the cd favorecido
	 */
	public void setCdFavorecido(Long cdFavorecido) {
		this.cdFavorecido = cdFavorecido;
	}

	/**
	 * Get: cdInscricaoFavorecido.
	 *
	 * @return cdInscricaoFavorecido
	 */
	public Long getCdInscricaoFavorecido() {
		return cdInscricaoFavorecido;
	}

	/**
	 * Set: cdInscricaoFavorecido.
	 *
	 * @param cdInscricaoFavorecido the cd inscricao favorecido
	 */
	public void setCdInscricaoFavorecido(Long cdInscricaoFavorecido) {
		this.cdInscricaoFavorecido = cdInscricaoFavorecido;
	}

	/**
	 * Get: cdOrigem.
	 *
	 * @return cdOrigem
	 */
	public Integer getCdOrigem() {
		return cdOrigem;
	}

	/**
	 * Set: cdOrigem.
	 *
	 * @param cdOrigem the cd origem
	 */
	public void setCdOrigem(Integer cdOrigem) {
		this.cdOrigem = cdOrigem;
	}

	/**
	 * Get: cdServico.
	 *
	 * @return cdServico
	 */
	public String getCdServico() {
		return cdServico;
	}

	/**
	 * Set: cdServico.
	 *
	 * @param cdServico the cd servico
	 */
	public void setCdServico(String cdServico) {
		this.cdServico = cdServico;
	}

	/**
	 * Get: cdServicoRelacionado.
	 *
	 * @return cdServicoRelacionado
	 */
	public String getCdServicoRelacionado() {
		return cdServicoRelacionado;
	}

	/**
	 * Set: cdServicoRelacionado.
	 *
	 * @param cdServicoRelacionado the cd servico relacionado
	 */
	public void setCdServicoRelacionado(String cdServicoRelacionado) {
		this.cdServicoRelacionado = cdServicoRelacionado;
	}

	/**
	 * Get: cdTipoInscricaoFavorecido.
	 *
	 * @return cdTipoInscricaoFavorecido
	 */
	public String getCdTipoInscricaoFavorecido() {
		return cdTipoInscricaoFavorecido;
	}

	/**
	 * Set: cdTipoInscricaoFavorecido.
	 *
	 * @param cdTipoInscricaoFavorecido the cd tipo inscricao favorecido
	 */
	public void setCdTipoInscricaoFavorecido(String cdTipoInscricaoFavorecido) {
		this.cdTipoInscricaoFavorecido = cdTipoInscricaoFavorecido;
	}

	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}

	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}

	/**
	 * Get: digitoAgenciaDebito.
	 *
	 * @return digitoAgenciaDebito
	 */
	public Integer getDigitoAgenciaDebito() {
		return digitoAgenciaDebito;
	}

	/**
	 * Set: digitoAgenciaDebito.
	 *
	 * @param digitoAgenciaDebito the digito agencia debito
	 */
	public void setDigitoAgenciaDebito(Integer digitoAgenciaDebito) {
		this.digitoAgenciaDebito = digitoAgenciaDebito;
	}

	/**
	 * Get: digitoContaDebito.
	 *
	 * @return digitoContaDebito
	 */
	public String getDigitoContaDebito() {
		return digitoContaDebito;
	}

	/**
	 * Set: digitoContaDebito.
	 *
	 * @param digitoContaDebito the digito conta debito
	 */
	public void setDigitoContaDebito(String digitoContaDebito) {
		this.digitoContaDebito = digitoContaDebito;
	}

	/**
	 * Get: dsCanalInclusao.
	 *
	 * @return dsCanalInclusao
	 */
	public String getDsCanalInclusao() {
		return dsCanalInclusao;
	}

	/**
	 * Set: dsCanalInclusao.
	 *
	 * @param dsCanalInclusao the ds canal inclusao
	 */
	public void setDsCanalInclusao(String dsCanalInclusao) {
		this.dsCanalInclusao = dsCanalInclusao;
	}

	/**
	 * Get: dsCanalManutencao.
	 *
	 * @return dsCanalManutencao
	 */
	public String getDsCanalManutencao() {
		return dsCanalManutencao;
	}

	/**
	 * Set: dsCanalManutencao.
	 *
	 * @param dsCanalManutencao the ds canal manutencao
	 */
	public void setDsCanalManutencao(String dsCanalManutencao) {
		this.dsCanalManutencao = dsCanalManutencao;
	}

	/**
	 * Get: dsFavorecido.
	 *
	 * @return dsFavorecido
	 */
	public String getDsFavorecido() {
		return dsFavorecido;
	}

	/**
	 * Set: dsFavorecido.
	 *
	 * @param dsFavorecido the ds favorecido
	 */
	public void setDsFavorecido(String dsFavorecido) {
		this.dsFavorecido = dsFavorecido;
	}

	/**
	 * Get: dsSituacaoSolicitacao.
	 *
	 * @return dsSituacaoSolicitacao
	 */
	public String getDsSituacaoSolicitacao() {
		return dsSituacaoSolicitacao;
	}

	/**
	 * Set: dsSituacaoSolicitacao.
	 *
	 * @param dsSituacaoSolicitacao the ds situacao solicitacao
	 */
	public void setDsSituacaoSolicitacao(String dsSituacaoSolicitacao) {
		this.dsSituacaoSolicitacao = dsSituacaoSolicitacao;
	}

	/**
	 * Get: dtFimPagamento.
	 *
	 * @return dtFimPagamento
	 */
	public String getDtFimPagamento() {
		return dtFimPagamento;
	}

	/**
	 * Set: dtFimPagamento.
	 *
	 * @param dtFimPagamento the dt fim pagamento
	 */
	public void setDtFimPagamento(String dtFimPagamento) {
		this.dtFimPagamento = dtFimPagamento;
	}

	/**
	 * Get: dtInicioPagamento.
	 *
	 * @return dtInicioPagamento
	 */
	public String getDtInicioPagamento() {
		return dtInicioPagamento;
	}

	/**
	 * Set: dtInicioPagamento.
	 *
	 * @param dtInicioPagamento the dt inicio pagamento
	 */
	public void setDtInicioPagamento(String dtInicioPagamento) {
		this.dtInicioPagamento = dtInicioPagamento;
	}

	/**
	 * Get: hrAtendimentoSolicitacao.
	 *
	 * @return hrAtendimentoSolicitacao
	 */
	public String getHrAtendimentoSolicitacao() {
		return hrAtendimentoSolicitacao;
	}

	/**
	 * Set: hrAtendimentoSolicitacao.
	 *
	 * @param hrAtendimentoSolicitacao the hr atendimento solicitacao
	 */
	public void setHrAtendimentoSolicitacao(String hrAtendimentoSolicitacao) {
		this.hrAtendimentoSolicitacao = hrAtendimentoSolicitacao;
	}

	/**
	 * Get: hrInclusaoRegistro.
	 *
	 * @return hrInclusaoRegistro
	 */
	public String getHrInclusaoRegistro() {
		return hrInclusaoRegistro;
	}

	/**
	 * Set: hrInclusaoRegistro.
	 *
	 * @param hrInclusaoRegistro the hr inclusao registro
	 */
	public void setHrInclusaoRegistro(String hrInclusaoRegistro) {
		this.hrInclusaoRegistro = hrInclusaoRegistro;
	}

	/**
	 * Get: hrManutencaoRegistro.
	 *
	 * @return hrManutencaoRegistro
	 */
	public String getHrManutencaoRegistro() {
		return hrManutencaoRegistro;
	}

	/**
	 * Set: hrManutencaoRegistro.
	 *
	 * @param hrManutencaoRegistro the hr manutencao registro
	 */
	public void setHrManutencaoRegistro(String hrManutencaoRegistro) {
		this.hrManutencaoRegistro = hrManutencaoRegistro;
	}

	/**
	 * Get: hrSolicitacaoPagamento.
	 *
	 * @return hrSolicitacaoPagamento
	 */
	public String getHrSolicitacaoPagamento() {
		return hrSolicitacaoPagamento;
	}

	/**
	 * Set: hrSolicitacaoPagamento.
	 *
	 * @param hrSolicitacaoPagamento the hr solicitacao pagamento
	 */
	public void setHrSolicitacaoPagamento(String hrSolicitacaoPagamento) {
		this.hrSolicitacaoPagamento = hrSolicitacaoPagamento;
	}

	/**
	 * Get: indicadorAgendados.
	 *
	 * @return indicadorAgendados
	 */
	public String getIndicadorAgendados() {
		return indicadorAgendados;
	}

	/**
	 * Set: indicadorAgendados.
	 *
	 * @param indicadorAgendados the indicador agendados
	 */
	public void setIndicadorAgendados(String indicadorAgendados) {
		this.indicadorAgendados = indicadorAgendados;
	}

	/**
	 * Get: indicadorNaoPagos.
	 *
	 * @return indicadorNaoPagos
	 */
	public String getIndicadorNaoPagos() {
		return indicadorNaoPagos;
	}

	/**
	 * Set: indicadorNaoPagos.
	 *
	 * @param indicadorNaoPagos the indicador nao pagos
	 */
	public void setIndicadorNaoPagos(String indicadorNaoPagos) {
		this.indicadorNaoPagos = indicadorNaoPagos;
	}

	/**
	 * Get: indicadorPagos.
	 *
	 * @return indicadorPagos
	 */
	public String getIndicadorPagos() {
		return indicadorPagos;
	}

	/**
	 * Set: indicadorPagos.
	 *
	 * @param indicadorPagos the indicador pagos
	 */
	public void setIndicadorPagos(String indicadorPagos) {
		this.indicadorPagos = indicadorPagos;
	}

	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}

	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	/**
	 * Get: nmMinemonicoFavorecido.
	 *
	 * @return nmMinemonicoFavorecido
	 */
	public String getNmMinemonicoFavorecido() {
		return nmMinemonicoFavorecido;
	}

	/**
	 * Set: nmMinemonicoFavorecido.
	 *
	 * @param nmMinemonicoFavorecido the nm minemonico favorecido
	 */
	public void setNmMinemonicoFavorecido(String nmMinemonicoFavorecido) {
		this.nmMinemonicoFavorecido = nmMinemonicoFavorecido;
	}

	/**
	 * Get: nmOperacaoFluxoInclusao.
	 *
	 * @return nmOperacaoFluxoInclusao
	 */
	public String getNmOperacaoFluxoInclusao() {
		return nmOperacaoFluxoInclusao;
	}

	/**
	 * Set: nmOperacaoFluxoInclusao.
	 *
	 * @param nmOperacaoFluxoInclusao the nm operacao fluxo inclusao
	 */
	public void setNmOperacaoFluxoInclusao(String nmOperacaoFluxoInclusao) {
		this.nmOperacaoFluxoInclusao = nmOperacaoFluxoInclusao;
	}

	/**
	 * Get: nmOperacaoFluxoManutencao.
	 *
	 * @return nmOperacaoFluxoManutencao
	 */
	public String getNmOperacaoFluxoManutencao() {
		return nmOperacaoFluxoManutencao;
	}

	/**
	 * Set: nmOperacaoFluxoManutencao.
	 *
	 * @param nmOperacaoFluxoManutencao the nm operacao fluxo manutencao
	 */
	public void setNmOperacaoFluxoManutencao(String nmOperacaoFluxoManutencao) {
		this.nmOperacaoFluxoManutencao = nmOperacaoFluxoManutencao;
	}

	/**
	 * Get: nrSolicitacaoPagamento.
	 *
	 * @return nrSolicitacaoPagamento
	 */
	public Integer getNrSolicitacaoPagamento() {
		return nrSolicitacaoPagamento;
	}

	/**
	 * Set: nrSolicitacaoPagamento.
	 *
	 * @param nrSolicitacaoPagamento the nr solicitacao pagamento
	 */
	public void setNrSolicitacaoPagamento(Integer nrSolicitacaoPagamento) {
		this.nrSolicitacaoPagamento = nrSolicitacaoPagamento;
	}

	/**
	 * Get: qtdeRegistrosSolicitacao.
	 *
	 * @return qtdeRegistrosSolicitacao
	 */
	public Long getQtdeRegistrosSolicitacao() {
		return qtdeRegistrosSolicitacao;
	}

	/**
	 * Set: qtdeRegistrosSolicitacao.
	 *
	 * @param qtdeRegistrosSolicitacao the qtde registros solicitacao
	 */
	public void setQtdeRegistrosSolicitacao(Long qtdeRegistrosSolicitacao) {
		this.qtdeRegistrosSolicitacao = qtdeRegistrosSolicitacao;
	}

	/**
	 * Get: resultadoProcessamento.
	 *
	 * @return resultadoProcessamento
	 */
	public String getResultadoProcessamento() {
		return resultadoProcessamento;
	}

	/**
	 * Set: resultadoProcessamento.
	 *
	 * @param resultadoProcessamento the resultado processamento
	 */
	public void setResultadoProcessamento(String resultadoProcessamento) {
		this.resultadoProcessamento = resultadoProcessamento;
	}


	/**
	 * Get: vlrTarifaAtual.
	 *
	 * @return vlrTarifaAtual
	 */
	public BigDecimal getVlrTarifaAtual() {
		return vlrTarifaAtual;
	}

	/**
	 * Set: vlrTarifaAtual.
	 *
	 * @param vlrTarifaAtual the vlr tarifa atual
	 */
	public void setVlrTarifaAtual(BigDecimal vlrTarifaAtual) {
		this.vlrTarifaAtual = vlrTarifaAtual;
	}

	/**
	 * Get: hrSolicitacaoPagamentoFormatada.
	 *
	 * @return hrSolicitacaoPagamentoFormatada
	 */
	public String getHrSolicitacaoPagamentoFormatada() {
		return hrSolicitacaoPagamentoFormatada;
	}

	/**
	 * Set: hrSolicitacaoPagamentoFormatada.
	 *
	 * @param hrSolicitacaoPagamentoFormatada the hr solicitacao pagamento formatada
	 */
	public void setHrSolicitacaoPagamentoFormatada(
			String hrSolicitacaoPagamentoFormatada) {
		this.hrSolicitacaoPagamentoFormatada = hrSolicitacaoPagamentoFormatada;
	}

	/**
	 * Get: hrAtendimentoSolicitacaoFormatada.
	 *
	 * @return hrAtendimentoSolicitacaoFormatada
	 */
	public String getHrAtendimentoSolicitacaoFormatada() {
		return hrAtendimentoSolicitacaoFormatada;
	}

	/**
	 * Set: hrAtendimentoSolicitacaoFormatada.
	 *
	 * @param hrAtendimentoSolicitacaoFormatada the hr atendimento solicitacao formatada
	 */
	public void setHrAtendimentoSolicitacaoFormatada(
			String hrAtendimentoSolicitacaoFormatada) {
		this.hrAtendimentoSolicitacaoFormatada = hrAtendimentoSolicitacaoFormatada;
	}

	/**
	 * Get: hrInclusaoRegistroFormatada.
	 *
	 * @return hrInclusaoRegistroFormatada
	 */
	public String getHrInclusaoRegistroFormatada() {
		return hrInclusaoRegistroFormatada;
	}

	/**
	 * Set: hrInclusaoRegistroFormatada.
	 *
	 * @param hrInclusaoRegistroFormatada the hr inclusao registro formatada
	 */
	public void setHrInclusaoRegistroFormatada(
			String hrInclusaoRegistroFormatada) {
		this.hrInclusaoRegistroFormatada = hrInclusaoRegistroFormatada;
	}

	/**
	 * Get: hrManutencaoRegistroFormatada.
	 *
	 * @return hrManutencaoRegistroFormatada
	 */
	public String getHrManutencaoRegistroFormatada() {
		return hrManutencaoRegistroFormatada;
	}

	/**
	 * Set: hrManutencaoRegistroFormatada.
	 *
	 * @param hrManutencaoRegistroFormatada the hr manutencao registro formatada
	 */
	public void setHrManutencaoRegistroFormatada(
			String hrManutencaoRegistroFormatada) {
		this.hrManutencaoRegistroFormatada = hrManutencaoRegistroFormatada;
	}

	/**
	 * Get: contaDebitoFormatado.
	 *
	 * @return contaDebitoFormatado
	 */
	public String getContaDebitoFormatado() {
		return contaDebitoFormatado;
	}

	/**
	 * Set: contaDebitoFormatado.
	 *
	 * @param contaDebitoFormatado the conta debito formatado
	 */
	public void setContaDebitoFormatado(String contaDebitoFormatado) {
		this.contaDebitoFormatado = contaDebitoFormatado;
	}

	/**
	 * Get: tipoCanalFormatado.
	 *
	 * @return tipoCanalFormatado
	 */
	public String getTipoCanalFormatado() {
		if (cdCanalManutencao != null && cdCanalManutencao == 0) {
			return "";
		}

		StringBuilder tipoCanal = new StringBuilder();
		tipoCanal.append(cdCanalManutencao);
		tipoCanal.append(" - ");
		tipoCanal.append(dsCanalManutencao);
		return tipoCanal.toString();
	}

	/**
	 * Get: vlTarifaPadrao.
	 *
	 * @return vlTarifaPadrao
	 */
	public BigDecimal getVlTarifaPadrao() {
		return vlTarifaPadrao;
	}

	/**
	 * Set: vlTarifaPadrao.
	 *
	 * @param vlTarifaPadrao the vl tarifa padrao
	 */
	public void setVlTarifaPadrao(BigDecimal vlTarifaPadrao) {
		this.vlTarifaPadrao = vlTarifaPadrao;
	}

	/**
	 * Get: numPercentualDescTarifa.
	 *
	 * @return numPercentualDescTarifa
	 */
	public BigDecimal getNumPercentualDescTarifa() {
		return numPercentualDescTarifa;
	}

	/**
	 * Set: numPercentualDescTarifa.
	 *
	 * @param numPercentualDescTarifa the num percentual desc tarifa
	 */
	public void setNumPercentualDescTarifa(BigDecimal numPercentualDescTarifa) {
		this.numPercentualDescTarifa = numPercentualDescTarifa;
	}
}