/*
 * Nome: br.com.bradesco.web.pgit.service.business.combo.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.combo.bean;

import br.com.bradesco.web.pgit.service.data.pdc.listartipoconta.response.Ocorrencias;

/**
 * Nome: ListarTipoContaSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarTipoContaSaidaDTO {
    
    /** Atributo cdTipoConta. */
    private Integer cdTipoConta;
    
    /** Atributo dsTipoConta. */
    private String dsTipoConta;
    
	/**
	 * Listar tipo conta saida dto.
	 *
	 * @param ocorrencia the ocorrencia
	 */
	public ListarTipoContaSaidaDTO(Ocorrencias ocorrencia) {
		this.cdTipoConta = ocorrencia.getCdTipoConta();
		this.dsTipoConta = ocorrencia.getDsTipoConta();
	}

	/**
	 * Listar tipo conta saida dto.
	 */
	public ListarTipoContaSaidaDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * Get: cdTipoConta.
	 *
	 * @return cdTipoConta
	 */
	public Integer getCdTipoConta() {
		return cdTipoConta;
	}

	/**
	 * Set: cdTipoConta.
	 *
	 * @param cdTipoConta the cd tipo conta
	 */
	public void setCdTipoConta(Integer cdTipoConta) {
		this.cdTipoConta = cdTipoConta;
	}

	/**
	 * Get: dsTipoConta.
	 *
	 * @return dsTipoConta
	 */
	public String getDsTipoConta() {
		return dsTipoConta;
	}

	/**
	 * Set: dsTipoConta.
	 *
	 * @param dsTipoConta the ds tipo conta
	 */
	public void setDsTipoConta(String dsTipoConta) {
		this.dsTipoConta = dsTipoConta;
	}
}
