package br.com.bradesco.web.pgit.service.business.vincmsglanctopecontratada.bean;

import java.util.List;

public class DetalharVincMsgLancOperSaidaDTO {

	/** Atributo codMensagem. */
	private String codMensagem;

	/** Atributo mensagem. */
	private String mensagem;

	/** Atributo cdPessoaJuridicaContrato. */
	private Long cdPessoaJuridicaContrato;

	/** Atributo cdTipoContratoNegocio. */
	private Integer cdTipoContratoNegocio;

	/** Atributo nrSequenciaContratoNegocio. */
	private Long nrSequenciaContratoNegocio;

	/** Atributo cdMensagemLinhaExtrato. */
	private Integer cdMensagemLinhaExtrato;

	/** Atributo cdIdentificadorLancamentoDebito. */
	private Integer cdIdentificadorLancamentoDebito;

	/** Atributo dsIdentificadorLancamentoDebito. */
	private String dsIdentificadorLancamentoDebito;

	/** Atributo cdIdentificadorLancamentoCredito. */
	private Integer cdIdentificadorLancamentoCredito;

	/** Atributo dsIdentificadorLancamentoCredito. */
	private String dsIdentificadorLancamentoCredito;

	/** Atributo cdTipoMensagemExtrato. */
	private Integer cdTipoMensagemExtrato;

	/** Atributo cdIndicadorRestContrato. */
	private Integer cdIndicadorRestContrato;

	/** Atributo cdProdutoServicoOperacao. */
	private Integer cdProdutoServicoOperacao;

	/** Atributo dsProdutoServicoOperacao. */
	private String dsProdutoServicoOperacao;

	/** Atributo cdProdutoOperacaoRelacionado. */
	private Integer cdProdutoOperacaoRelacionado;

	/** Atributo dsProdutoOperacaoRelacionado. */
	private String dsProdutoOperacaoRelacionado;

	/** Atributo cdRelacionamentoProdutoProduto. */
	private Integer cdRelacionamentoProdutoProduto;

	/** Atributo cdCanalInclusao. */
	private Integer cdCanalInclusao;

	/** Atributo dsCanalInclusao. */
	private String dsCanalInclusao;

	/** Atributo cdAutenticacaoSegurancaInclusao. */
	private String cdAutenticacaoSegurancaInclusao;

	/** Atributo nmOperadorFluxoInclusao. */
	private String nmOperadorFluxoInclusao;

	/** Atributo hrInclusaoRegistro. */
	private String hrInclusaoRegistro;

	/** Atributo cdCanalManutencao. */
	private Integer cdCanalManutencao;

	/** Atributo dsCanalManutencao. */
	private String dsCanalManutencao;

	/** Atributo cdAutenticacaoSegurancaManutencao. */
	private String cdAutenticacaoSegurancaManutencao;

	/** Atributo nmOperadorFluxoManutencao. */
	private String nmOperadorFluxoManutencao;

	/** Atributo hrManutencaoRegistro. */
	private String hrManutencaoRegistro;

	/** Atributo nrOcorrencias. */
	private Integer nrOcorrencias;
	
	/** Atributo ocorrencias. */
	private List<DetalharVincMsgLancOperOcorrenciasSaidaDTO> ocorrencias;
	
	public String getCodMensagem() {
		return codMensagem;
	}

	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}

	public String getMensagem() {
		return mensagem;
	}

	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	public Long getCdPessoaJuridicaContrato() {
		return cdPessoaJuridicaContrato;
	}

	public void setCdPessoaJuridicaContrato(Long cdPessoaJuridicaContrato) {
		this.cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
	}

	public Integer getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}

	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}

	public Long getNrSequenciaContratoNegocio() {
		return nrSequenciaContratoNegocio;
	}

	public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}

	public Integer getCdMensagemLinhaExtrato() {
		return cdMensagemLinhaExtrato;
	}

	public void setCdMensagemLinhaExtrato(Integer cdMensagemLinhaExtrato) {
		this.cdMensagemLinhaExtrato = cdMensagemLinhaExtrato;
	}

	public Integer getCdIdentificadorLancamentoDebito() {
		return cdIdentificadorLancamentoDebito;
	}

	public void setCdIdentificadorLancamentoDebito(
			Integer cdIdentificadorLancamentoDebito) {
		this.cdIdentificadorLancamentoDebito = cdIdentificadorLancamentoDebito;
	}

	public String getDsIdentificadorLancamentoDebito() {
		return dsIdentificadorLancamentoDebito;
	}

	public void setDsIdentificadorLancamentoDebito(
			String dsIdentificadorLancamentoDebito) {
		this.dsIdentificadorLancamentoDebito = dsIdentificadorLancamentoDebito;
	}

	public Integer getCdIdentificadorLancamentoCredito() {
		return cdIdentificadorLancamentoCredito;
	}

	public void setCdIdentificadorLancamentoCredito(
			Integer cdIdentificadorLancamentoCredito) {
		this.cdIdentificadorLancamentoCredito = cdIdentificadorLancamentoCredito;
	}

	public String getDsIdentificadorLancamentoCredito() {
		return dsIdentificadorLancamentoCredito;
	}

	public void setDsIdentificadorLancamentoCredito(
			String dsIdentificadorLancamentoCredito) {
		this.dsIdentificadorLancamentoCredito = dsIdentificadorLancamentoCredito;
	}

	public Integer getCdTipoMensagemExtrato() {
		return cdTipoMensagemExtrato;
	}

	public void setCdTipoMensagemExtrato(Integer cdTipoMensagemExtrato) {
		this.cdTipoMensagemExtrato = cdTipoMensagemExtrato;
	}

	public Integer getCdIndicadorRestContrato() {
		return cdIndicadorRestContrato;
	}

	public void setCdIndicadorRestContrato(Integer cdIndicadorRestContrato) {
		this.cdIndicadorRestContrato = cdIndicadorRestContrato;
	}

	public Integer getCdProdutoServicoOperacao() {
		return cdProdutoServicoOperacao;
	}

	public void setCdProdutoServicoOperacao(Integer cdProdutoServicoOperacao) {
		this.cdProdutoServicoOperacao = cdProdutoServicoOperacao;
	}

	public String getDsProdutoServicoOperacao() {
		return dsProdutoServicoOperacao;
	}

	public void setDsProdutoServicoOperacao(String dsProdutoServicoOperacao) {
		this.dsProdutoServicoOperacao = dsProdutoServicoOperacao;
	}

	public Integer getCdProdutoOperacaoRelacionado() {
		return cdProdutoOperacaoRelacionado;
	}

	public void setCdProdutoOperacaoRelacionado(
			Integer cdProdutoOperacaoRelacionado) {
		this.cdProdutoOperacaoRelacionado = cdProdutoOperacaoRelacionado;
	}

	public String getDsProdutoOperacaoRelacionado() {
		return dsProdutoOperacaoRelacionado;
	}

	public void setDsProdutoOperacaoRelacionado(
			String dsProdutoOperacaoRelacionado) {
		this.dsProdutoOperacaoRelacionado = dsProdutoOperacaoRelacionado;
	}

	public Integer getCdRelacionamentoProdutoProduto() {
		return cdRelacionamentoProdutoProduto;
	}

	public void setCdRelacionamentoProdutoProduto(
			Integer cdRelacionamentoProdutoProduto) {
		this.cdRelacionamentoProdutoProduto = cdRelacionamentoProdutoProduto;
	}

	public Integer getCdCanalInclusao() {
		return cdCanalInclusao;
	}

	public void setCdCanalInclusao(Integer cdCanalInclusao) {
		this.cdCanalInclusao = cdCanalInclusao;
	}

	public String getDsCanalInclusao() {
		return dsCanalInclusao;
	}

	public void setDsCanalInclusao(String dsCanalInclusao) {
		this.dsCanalInclusao = dsCanalInclusao;
	}

	public String getCdAutenticacaoSegurancaInclusao() {
		return cdAutenticacaoSegurancaInclusao;
	}

	public void setCdAutenticacaoSegurancaInclusao(
			String cdAutenticacaoSegurancaInclusao) {
		this.cdAutenticacaoSegurancaInclusao = cdAutenticacaoSegurancaInclusao;
	}

	public String getNmOperadorFluxoInclusao() {
		return nmOperadorFluxoInclusao;
	}

	public void setNmOperadorFluxoInclusao(String nmOperadorFluxoInclusao) {
		this.nmOperadorFluxoInclusao = nmOperadorFluxoInclusao;
	}

	public String getHrInclusaoRegistro() {
		return hrInclusaoRegistro;
	}

	public void setHrInclusaoRegistro(String hrInclusaoRegistro) {
		this.hrInclusaoRegistro = hrInclusaoRegistro;
	}

	public Integer getCdCanalManutencao() {
		return cdCanalManutencao;
	}

	public void setCdCanalManutencao(Integer cdCanalManutencao) {
		this.cdCanalManutencao = cdCanalManutencao;
	}

	public String getDsCanalManutencao() {
		return dsCanalManutencao;
	}

	public void setDsCanalManutencao(String dsCanalManutencao) {
		this.dsCanalManutencao = dsCanalManutencao;
	}

	public String getCdAutenticacaoSegurancaManutencao() {
		return cdAutenticacaoSegurancaManutencao;
	}

	public void setCdAutenticacaoSegurancaManutencao(
			String cdAutenticacaoSegurancaManutencao) {
		this.cdAutenticacaoSegurancaManutencao = cdAutenticacaoSegurancaManutencao;
	}

	public String getNmOperadorFluxoManutencao() {
		return nmOperadorFluxoManutencao;
	}

	public void setNmOperadorFluxoManutencao(String nmOperadorFluxoManutencao) {
		this.nmOperadorFluxoManutencao = nmOperadorFluxoManutencao;
	}

	public String getHrManutencaoRegistro() {
		return hrManutencaoRegistro;
	}

	public void setHrManutencaoRegistro(String hrManutencaoRegistro) {
		this.hrManutencaoRegistro = hrManutencaoRegistro;
	}

	public Integer getNrOcorrencias() {
		return nrOcorrencias;
	}

	public void setNrOcorrencias(Integer nrOcorrencias) {
		this.nrOcorrencias = nrOcorrencias;
	}

	public List<DetalharVincMsgLancOperOcorrenciasSaidaDTO> getOcorrencias() {
		return ocorrencias;
	}

	public void setOcorrencias(
			List<DetalharVincMsgLancOperOcorrenciasSaidaDTO> ocorrencias) {
		this.ocorrencias = ocorrencias;
	}

}
