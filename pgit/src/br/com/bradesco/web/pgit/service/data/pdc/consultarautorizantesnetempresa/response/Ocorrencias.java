/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultarautorizantesnetempresa.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _qtdAutorizadoPagamento
     */
    private java.lang.String _qtdAutorizadoPagamento;

    /**
     * Field _dtHoraManutencao
     */
    private java.lang.String _dtHoraManutencao;

    /**
     * Field _cdTipocpfcnpj
     */
    private int _cdTipocpfcnpj = 0;

    /**
     * keeps track of state for field: _cdTipocpfcnpj
     */
    private boolean _has_cdTipocpfcnpj;

    /**
     * Field _cdCpfCnpj
     */
    private java.lang.String _cdCpfCnpj;

    /**
     * Field _dsAutorizante
     */
    private java.lang.String _dsAutorizante;

    /**
     * Field _cdAcaoEvento
     */
    private int _cdAcaoEvento = 0;

    /**
     * keeps track of state for field: _cdAcaoEvento
     */
    private boolean _has_cdAcaoEvento;

    /**
     * Field _dsAcaoEvento
     */
    private java.lang.String _dsAcaoEvento;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarautorizantesnetempresa.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdAcaoEvento
     * 
     */
    public void deleteCdAcaoEvento()
    {
        this._has_cdAcaoEvento= false;
    } //-- void deleteCdAcaoEvento() 

    /**
     * Method deleteCdTipocpfcnpj
     * 
     */
    public void deleteCdTipocpfcnpj()
    {
        this._has_cdTipocpfcnpj= false;
    } //-- void deleteCdTipocpfcnpj() 

    /**
     * Returns the value of field 'cdAcaoEvento'.
     * 
     * @return int
     * @return the value of field 'cdAcaoEvento'.
     */
    public int getCdAcaoEvento()
    {
        return this._cdAcaoEvento;
    } //-- int getCdAcaoEvento() 

    /**
     * Returns the value of field 'cdCpfCnpj'.
     * 
     * @return String
     * @return the value of field 'cdCpfCnpj'.
     */
    public java.lang.String getCdCpfCnpj()
    {
        return this._cdCpfCnpj;
    } //-- java.lang.String getCdCpfCnpj() 

    /**
     * Returns the value of field 'cdTipocpfcnpj'.
     * 
     * @return int
     * @return the value of field 'cdTipocpfcnpj'.
     */
    public int getCdTipocpfcnpj()
    {
        return this._cdTipocpfcnpj;
    } //-- int getCdTipocpfcnpj() 

    /**
     * Returns the value of field 'dsAcaoEvento'.
     * 
     * @return String
     * @return the value of field 'dsAcaoEvento'.
     */
    public java.lang.String getDsAcaoEvento()
    {
        return this._dsAcaoEvento;
    } //-- java.lang.String getDsAcaoEvento() 

    /**
     * Returns the value of field 'dsAutorizante'.
     * 
     * @return String
     * @return the value of field 'dsAutorizante'.
     */
    public java.lang.String getDsAutorizante()
    {
        return this._dsAutorizante;
    } //-- java.lang.String getDsAutorizante() 

    /**
     * Returns the value of field 'dtHoraManutencao'.
     * 
     * @return String
     * @return the value of field 'dtHoraManutencao'.
     */
    public java.lang.String getDtHoraManutencao()
    {
        return this._dtHoraManutencao;
    } //-- java.lang.String getDtHoraManutencao() 

    /**
     * Returns the value of field 'qtdAutorizadoPagamento'.
     * 
     * @return String
     * @return the value of field 'qtdAutorizadoPagamento'.
     */
    public java.lang.String getQtdAutorizadoPagamento()
    {
        return this._qtdAutorizadoPagamento;
    } //-- java.lang.String getQtdAutorizadoPagamento() 

    /**
     * Method hasCdAcaoEvento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAcaoEvento()
    {
        return this._has_cdAcaoEvento;
    } //-- boolean hasCdAcaoEvento() 

    /**
     * Method hasCdTipocpfcnpj
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipocpfcnpj()
    {
        return this._has_cdTipocpfcnpj;
    } //-- boolean hasCdTipocpfcnpj() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdAcaoEvento'.
     * 
     * @param cdAcaoEvento the value of field 'cdAcaoEvento'.
     */
    public void setCdAcaoEvento(int cdAcaoEvento)
    {
        this._cdAcaoEvento = cdAcaoEvento;
        this._has_cdAcaoEvento = true;
    } //-- void setCdAcaoEvento(int) 

    /**
     * Sets the value of field 'cdCpfCnpj'.
     * 
     * @param cdCpfCnpj the value of field 'cdCpfCnpj'.
     */
    public void setCdCpfCnpj(java.lang.String cdCpfCnpj)
    {
        this._cdCpfCnpj = cdCpfCnpj;
    } //-- void setCdCpfCnpj(java.lang.String) 

    /**
     * Sets the value of field 'cdTipocpfcnpj'.
     * 
     * @param cdTipocpfcnpj the value of field 'cdTipocpfcnpj'.
     */
    public void setCdTipocpfcnpj(int cdTipocpfcnpj)
    {
        this._cdTipocpfcnpj = cdTipocpfcnpj;
        this._has_cdTipocpfcnpj = true;
    } //-- void setCdTipocpfcnpj(int) 

    /**
     * Sets the value of field 'dsAcaoEvento'.
     * 
     * @param dsAcaoEvento the value of field 'dsAcaoEvento'.
     */
    public void setDsAcaoEvento(java.lang.String dsAcaoEvento)
    {
        this._dsAcaoEvento = dsAcaoEvento;
    } //-- void setDsAcaoEvento(java.lang.String) 

    /**
     * Sets the value of field 'dsAutorizante'.
     * 
     * @param dsAutorizante the value of field 'dsAutorizante'.
     */
    public void setDsAutorizante(java.lang.String dsAutorizante)
    {
        this._dsAutorizante = dsAutorizante;
    } //-- void setDsAutorizante(java.lang.String) 

    /**
     * Sets the value of field 'dtHoraManutencao'.
     * 
     * @param dtHoraManutencao the value of field 'dtHoraManutencao'
     */
    public void setDtHoraManutencao(java.lang.String dtHoraManutencao)
    {
        this._dtHoraManutencao = dtHoraManutencao;
    } //-- void setDtHoraManutencao(java.lang.String) 

    /**
     * Sets the value of field 'qtdAutorizadoPagamento'.
     * 
     * @param qtdAutorizadoPagamento the value of field
     * 'qtdAutorizadoPagamento'.
     */
    public void setQtdAutorizadoPagamento(java.lang.String qtdAutorizadoPagamento)
    {
        this._qtdAutorizadoPagamento = qtdAutorizadoPagamento;
    } //-- void setQtdAutorizadoPagamento(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultarautorizantesnetempresa.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultarautorizantesnetempresa.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultarautorizantesnetempresa.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarautorizantesnetempresa.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
