/*
 * Nome: br.com.bradesco.web.pgit.service.business.manterservicorelacionados.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.manterservicorelacionados.bean;

/**
 * Nome: DetalharServicoRelacionadoEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class DetalharServicoRelacionadoEntradaDTO {
	
	
	/** Atributo codTipoServico. */
	private int codTipoServico;
	
	/** Atributo codModalidadeServico. */
	private int codModalidadeServico;
	
	/** Atributo codTipoRelacionamento. */
	private int codTipoRelacionamento;
	//private int codServicoComposto;
	//private int codNaturezaServico;
	
	
	
	/**
	 * Get: codModalidadeServico.
	 *
	 * @return codModalidadeServico
	 */
	public int getCodModalidadeServico() {
		return codModalidadeServico;
	}
	
	/**
	 * Set: codModalidadeServico.
	 *
	 * @param codModalidadeServico the cod modalidade servico
	 */
	public void setCodModalidadeServico(int codModalidadeServico) {
		this.codModalidadeServico = codModalidadeServico;
	}
	
	/**
	 * Get: codTipoRelacionamento.
	 *
	 * @return codTipoRelacionamento
	 */
	public int getCodTipoRelacionamento() {
		return codTipoRelacionamento;
	}
	
	/**
	 * Set: codTipoRelacionamento.
	 *
	 * @param codTipoRelacionamento the cod tipo relacionamento
	 */
	public void setCodTipoRelacionamento(int codTipoRelacionamento) {
		this.codTipoRelacionamento = codTipoRelacionamento;
	}
	
	/**
	 * Get: codTipoServico.
	 *
	 * @return codTipoServico
	 */
	public int getCodTipoServico() {
		return codTipoServico;
	}
	
	/**
	 * Set: codTipoServico.
	 *
	 * @param codTipoServico the cod tipo servico
	 */
	public void setCodTipoServico(int codTipoServico) {
		this.codTipoServico = codTipoServico;
	}

	
	

}
