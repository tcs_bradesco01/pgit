/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantercontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantercontrato.bean;

/**
 * Nome: ListarContasVincContSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarContasVincContSaidaDTO {
	
	/** Atributo codMensagem. */
	private String codMensagem;
	
	/** Atributo mensagem. */
	private String mensagem;
	
	/** Atributo cdPessoaJuridicaVinculo. */
	private Long cdPessoaJuridicaVinculo;
	
	/** Atributo cdTipoContratoVinculo. */
	private Integer cdTipoContratoVinculo;
	
	/** Atributo nrSequenciaContratoVinculo. */
	private Long nrSequenciaContratoVinculo;
	
	/** Atributo cdTipoVinculoContrato. */
	private Integer cdTipoVinculoContrato;
	
	/** Atributo cdCpfCnpj. */
	private String cdCpfCnpj;
	
	/** Atributo nomeParticipante. */
	private String nomeParticipante;
	
	/** Atributo cdBanco. */
	private Integer cdBanco;
	
	/** Atributo dsBanco. */
	private String dsBanco;
	
	/** Atributo cdAgencia. */
	private Integer cdAgencia;
	
	/** Atributo dsAgencia. */
	private String dsAgencia;
	
	/** Atributo cdConta. */
	private Long cdConta;
	
	/** Atributo cdDigitoConta. */
	private String cdDigitoConta;
	
	/** Atributo cdTipoConta. */
	private Integer cdTipoConta;
	
	/** Atributo dsTipoConta. */
	private String dsTipoConta;
	
	/** Atributo cdTipoDebito. */
	private String cdTipoDebito;
	
	/** Atributo cdTipoTarifa. */
	private String cdTipoTarifa;
	
	/** Atributo cdTipoEstorno. */
	private String cdTipoEstorno;
	
	/** Atributo dsAlternativa. */
	private String dsAlternativa;
	
	/** Atributo dsFinalidade. */
	private String dsFinalidade;
	
	/** Atributo cdDigitoAgencia. */
	private Integer cdDigitoAgencia;
	
	/** Atributo dsBancoFormatado. */
	private String dsBancoFormatado;
	
	/** Atributo dsAgenciaFormatada. */
	private String dsAgenciaFormatada;
	
	/** Atributo dsContaFormatada. */
	private String dsContaFormatada;
	
	/** Atributo tipoContaFormatada. */
	private String tipoContaFormatada;
	
	/** Atributo cdParticipante. */
	private Long cdParticipante;
	
	/** Atributo cdSituacao. */
	private Integer cdSituacao;
	
	/** Atributo dsSituacao. */
	private String dsSituacao;
	
	/** Atributo cdSituacaoVinculacaoConta. */
	private Integer cdSituacaoVinculacaoConta;
	
	/** Atributo dsSituacaoVinculacaoConta. */
	private String dsSituacaoVinculacaoConta;
		
	/** Atributo check. */
	private boolean check;
	
	/** Atributo dsDevolucao. */
	private String dsDevolucao;
	
	//Campos utilizado em Desbloquear Contas;
	/** Atributo contaDebito. */
	private String contaDebito;
	
	private String dsEstornoDevolucao;

	/**
	 * Get: cdAgencia.
	 *
	 * @return cdAgencia
	 */
	public Integer getCdAgencia() {
		return cdAgencia;
	}

	/**
	 * Set: cdAgencia.
	 *
	 * @param cdAgencia the cd agencia
	 */
	public void setCdAgencia(Integer cdAgencia) {
		this.cdAgencia = cdAgencia;
	}

	/**
	 * Get: cdBanco.
	 *
	 * @return cdBanco
	 */
	public Integer getCdBanco() {
		return cdBanco;
	}

	/**
	 * Set: cdBanco.
	 *
	 * @param cdBanco the cd banco
	 */
	public void setCdBanco(Integer cdBanco) {
		this.cdBanco = cdBanco;
	}

	/**
	 * Get: cdConta.
	 *
	 * @return cdConta
	 */
	public Long getCdConta() {
		return cdConta;
	}

	/**
	 * Set: cdConta.
	 *
	 * @param cdConta the cd conta
	 */
	public void setCdConta(Long cdConta) {
		this.cdConta = cdConta;
	}

	/**
	 * Get: cdCpfCnpj.
	 *
	 * @return cdCpfCnpj
	 */
	public String getCdCpfCnpj() {
		return cdCpfCnpj;
	}

	/**
	 * Set: cdCpfCnpj.
	 *
	 * @param cdCpfCnpj the cd cpf cnpj
	 */
	public void setCdCpfCnpj(String cdCpfCnpj) {
		this.cdCpfCnpj = cdCpfCnpj;
	}

	/**
	 * Get: cdDigitoConta.
	 *
	 * @return cdDigitoConta
	 */
	public String getCdDigitoConta() {
		return cdDigitoConta;
	}

	/**
	 * Set: cdDigitoConta.
	 *
	 * @param cdDigitoConta the cd digito conta
	 */
	public void setCdDigitoConta(String cdDigitoConta) {
		this.cdDigitoConta = cdDigitoConta;
	}

	/**
	 * Get: cdPessoaJuridicaVinculo.
	 *
	 * @return cdPessoaJuridicaVinculo
	 */
	public Long getCdPessoaJuridicaVinculo() {
		return cdPessoaJuridicaVinculo;
	}

	/**
	 * Set: cdPessoaJuridicaVinculo.
	 *
	 * @param cdPessoaJuridicaVinculo the cd pessoa juridica vinculo
	 */
	public void setCdPessoaJuridicaVinculo(Long cdPessoaJuridicaVinculo) {
		this.cdPessoaJuridicaVinculo = cdPessoaJuridicaVinculo;
	}

	/**
	 * Get: cdTipoConta.
	 *
	 * @return cdTipoConta
	 */
	public Integer getCdTipoConta() {
		return cdTipoConta;
	}

	/**
	 * Set: cdTipoConta.
	 *
	 * @param cdTipoConta the cd tipo conta
	 */
	public void setCdTipoConta(Integer cdTipoConta) {
		this.cdTipoConta = cdTipoConta;
	}

	/**
	 * Get: dsTipoConta.
	 *
	 * @return dsTipoConta
	 */
	public String getDsTipoConta() {
		return dsTipoConta;
	}

	/**
	 * Set: dsTipoConta.
	 *
	 * @param dsTipoConta the ds tipo conta
	 */
	public void setDsTipoConta(String dsTipoConta) {
		this.dsTipoConta = dsTipoConta;
	}

	/**
	 * Get: cdTipoContratoVinculo.
	 *
	 * @return cdTipoContratoVinculo
	 */
	public Integer getCdTipoContratoVinculo() {
		return cdTipoContratoVinculo;
	}

	/**
	 * Set: cdTipoContratoVinculo.
	 *
	 * @param cdTipoContratoVinculo the cd tipo contrato vinculo
	 */
	public void setCdTipoContratoVinculo(Integer cdTipoContratoVinculo) {
		this.cdTipoContratoVinculo = cdTipoContratoVinculo;
	}

	/**
	 * Get: cdTipoDebito.
	 *
	 * @return cdTipoDebito
	 */
	public String getCdTipoDebito() {
		return cdTipoDebito;
	}

	/**
	 * Set: cdTipoDebito.
	 *
	 * @param cdTipoDebito the cd tipo debito
	 */
	public void setCdTipoDebito(String cdTipoDebito) {
		this.cdTipoDebito = cdTipoDebito;
	}

	/**
	 * Get: cdTipoEstorno.
	 *
	 * @return cdTipoEstorno
	 */
	public String getCdTipoEstorno() {
		return cdTipoEstorno;
	}

	/**
	 * Set: cdTipoEstorno.
	 *
	 * @param cdTipoEstorno the cd tipo estorno
	 */
	public void setCdTipoEstorno(String cdTipoEstorno) {
		this.cdTipoEstorno = cdTipoEstorno;
	}

	/**
	 * Get: cdTipoTarifa.
	 *
	 * @return cdTipoTarifa
	 */
	public String getCdTipoTarifa() {
		return cdTipoTarifa;
	}

	/**
	 * Set: cdTipoTarifa.
	 *
	 * @param cdTipoTarifa the cd tipo tarifa
	 */
	public void setCdTipoTarifa(String cdTipoTarifa) {
		this.cdTipoTarifa = cdTipoTarifa;
	}

	/**
	 * Get: cdTipoVinculoContrato.
	 *
	 * @return cdTipoVinculoContrato
	 */
	public Integer getCdTipoVinculoContrato() {
		return cdTipoVinculoContrato;
	}

	/**
	 * Set: cdTipoVinculoContrato.
	 *
	 * @param cdTipoVinculoContrato the cd tipo vinculo contrato
	 */
	public void setCdTipoVinculoContrato(Integer cdTipoVinculoContrato) {
		this.cdTipoVinculoContrato = cdTipoVinculoContrato;
	}

	/**
	 * Is check.
	 *
	 * @return true, if is check
	 */
	public boolean isCheck() {
		return check;
	}

	/**
	 * Set: check.
	 *
	 * @param check the check
	 */
	public void setCheck(boolean check) {
		this.check = check;
	}

	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}

	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}

	/**
	 * Get: contaDebito.
	 *
	 * @return contaDebito
	 */
	public String getContaDebito() {
		return contaDebito;
	}

	/**
	 * Set: contaDebito.
	 *
	 * @param contaDebito the conta debito
	 */
	public void setContaDebito(String contaDebito) {
		this.contaDebito = contaDebito;
	}

	/**
	 * Get: dsAgencia.
	 *
	 * @return dsAgencia
	 */
	public String getDsAgencia() {
		return dsAgencia;
	}

	/**
	 * Set: dsAgencia.
	 *
	 * @param dsAgencia the ds agencia
	 */
	public void setDsAgencia(String dsAgencia) {
		this.dsAgencia = dsAgencia;
	}

	/**
	 * Get: dsAlternativa.
	 *
	 * @return dsAlternativa
	 */
	public String getDsAlternativa() {
		return dsAlternativa;
	}

	/**
	 * Set: dsAlternativa.
	 *
	 * @param dsAlternativa the ds alternativa
	 */
	public void setDsAlternativa(String dsAlternativa) {
		this.dsAlternativa = dsAlternativa;
	}

	/**
	 * Get: dsBanco.
	 *
	 * @return dsBanco
	 */
	public String getDsBanco() {
		return dsBanco;
	}

	/**
	 * Set: dsBanco.
	 *
	 * @param dsBanco the ds banco
	 */
	public void setDsBanco(String dsBanco) {
		this.dsBanco = dsBanco;
	}

	/**
	 * Get: dsFinalidade.
	 *
	 * @return dsFinalidade
	 */
	public String getDsFinalidade() {
		return dsFinalidade;
	}

	/**
	 * Set: dsFinalidade.
	 *
	 * @param dsFinalidade the ds finalidade
	 */
	public void setDsFinalidade(String dsFinalidade) {
		this.dsFinalidade = dsFinalidade;
	}

	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}

	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	/**
	 * Get: nomeParticipante.
	 *
	 * @return nomeParticipante
	 */
	public String getNomeParticipante() {
		return nomeParticipante;
	}

	/**
	 * Set: nomeParticipante.
	 *
	 * @param nomeParticipante the nome participante
	 */
	public void setNomeParticipante(String nomeParticipante) {
		this.nomeParticipante = nomeParticipante;
	}

	/**
	 * Get: nrSequenciaContratoVinculo.
	 *
	 * @return nrSequenciaContratoVinculo
	 */
	public Long getNrSequenciaContratoVinculo() {
		return nrSequenciaContratoVinculo;
	}

	/**
	 * Set: nrSequenciaContratoVinculo.
	 *
	 * @param nrSequenciaContratoVinculo the nr sequencia contrato vinculo
	 */
	public void setNrSequenciaContratoVinculo(Long nrSequenciaContratoVinculo) {
		this.nrSequenciaContratoVinculo = nrSequenciaContratoVinculo;
	}

	/**
	 * Get: cdDigitoAgencia.
	 *
	 * @return cdDigitoAgencia
	 */
	public Integer getCdDigitoAgencia() {
		return cdDigitoAgencia;
	}

	/**
	 * Set: cdDigitoAgencia.
	 *
	 * @param cdDigitoAgencia the cd digito agencia
	 */
	public void setCdDigitoAgencia(Integer cdDigitoAgencia) {
		this.cdDigitoAgencia = cdDigitoAgencia;
	}

	/**
	 * Get: dsAgenciaFormatada.
	 *
	 * @return dsAgenciaFormatada
	 */
	public String getDsAgenciaFormatada() {
		return dsAgenciaFormatada;
	}

	/**
	 * Set: dsAgenciaFormatada.
	 *
	 * @param dsAgenciaFormatada the ds agencia formatada
	 */
	public void setDsAgenciaFormatada(String dsAgenciaFormatada) {
		this.dsAgenciaFormatada = dsAgenciaFormatada;
	}

	/**
	 * Get: dsBancoFormatado.
	 *
	 * @return dsBancoFormatado
	 */
	public String getDsBancoFormatado() {
		return dsBancoFormatado;
	}

	/**
	 * Set: dsBancoFormatado.
	 *
	 * @param dsBancoFormatado the ds banco formatado
	 */
	public void setDsBancoFormatado(String dsBancoFormatado) {
		this.dsBancoFormatado = dsBancoFormatado;
	}

	/**
	 * Get: dsContaFormatada.
	 *
	 * @return dsContaFormatada
	 */
	public String getDsContaFormatada() {
		return dsContaFormatada;
	}

	/**
	 * Set: dsContaFormatada.
	 *
	 * @param dsContaFormatada the ds conta formatada
	 */
	public void setDsContaFormatada(String dsContaFormatada) {
		this.dsContaFormatada = dsContaFormatada;
	}

	/**
	 * Get: tipoContaFormatada.
	 *
	 * @return tipoContaFormatada
	 */
	public String getTipoContaFormatada() {
		return tipoContaFormatada;
	}

	/**
	 * Set: tipoContaFormatada.
	 *
	 * @param tipoContaFormatada the tipo conta formatada
	 */
	public void setTipoContaFormatada(String tipoContaFormatada) {
		this.tipoContaFormatada = tipoContaFormatada;
	}

	/**
	 * Get: cdParticipante.
	 *
	 * @return cdParticipante
	 */
	public Long getCdParticipante() {
	    return cdParticipante;
	}

	/**
	 * Set: cdParticipante.
	 *
	 * @param cdParticipante the cd participante
	 */
	public void setCdParticipante(Long cdParticipante) {
	    this.cdParticipante = cdParticipante;
	}

	/**
	 * Get: cdSituacao.
	 *
	 * @return cdSituacao
	 */
	public Integer getCdSituacao() {
		return cdSituacao;
	}

	/**
	 * Set: cdSituacao.
	 *
	 * @param cdSituacao the cd situacao
	 */
	public void setCdSituacao(Integer cdSituacao) {
		this.cdSituacao = cdSituacao;
	}

	/**
	 * Get: dsSituacao.
	 *
	 * @return dsSituacao
	 */
	public String getDsSituacao() {
		return dsSituacao;
	}

	/**
	 * Set: dsSituacao.
	 *
	 * @param dsSituacao the ds situacao
	 */
	public void setDsSituacao(String dsSituacao) {
		this.dsSituacao = dsSituacao;
	}

	/**
	 * Get: cdSituacaoVinculacaoConta.
	 *
	 * @return cdSituacaoVinculacaoConta
	 */
	public Integer getCdSituacaoVinculacaoConta() {
		return cdSituacaoVinculacaoConta;
	}

	/**
	 * Set: cdSituacaoVinculacaoConta.
	 *
	 * @param cdSituacaoVinculacaoConta the cd situacao vinculacao conta
	 */
	public void setCdSituacaoVinculacaoConta(Integer cdSituacaoVinculacaoConta) {
		this.cdSituacaoVinculacaoConta = cdSituacaoVinculacaoConta;
	}

	/**
	 * Get: dsSituacaoVinculacaoConta.
	 *
	 * @return dsSituacaoVinculacaoConta
	 */
	public String getDsSituacaoVinculacaoConta() {
		return dsSituacaoVinculacaoConta;
	}

	/**
	 * Set: dsSituacaoVinculacaoConta.
	 *
	 * @param dsSituacaoVinculacaoConta the ds situacao vinculacao conta
	 */
	public void setDsSituacaoVinculacaoConta(String dsSituacaoVinculacaoConta) {
		this.dsSituacaoVinculacaoConta = dsSituacaoVinculacaoConta;
	}

	public String getDsDevolucao() {
		return dsDevolucao;
	}

	public void setDsDevolucao(String dsDevolucao) {
		this.dsDevolucao = dsDevolucao;
	}

	public String getDsEstornoDevolucao() {
		return dsEstornoDevolucao;
	}

	public void setDsEstornoDevolucao(String dsEstornoDevolucao) {
		this.dsEstornoDevolucao = dsEstornoDevolucao;
	}

	

}
