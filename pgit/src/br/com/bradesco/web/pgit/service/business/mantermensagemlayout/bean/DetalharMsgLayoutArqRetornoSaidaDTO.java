/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantermensagemlayout.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantermensagemlayout.bean;

/**
 * Nome: DetalharMsgLayoutArqRetornoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class DetalharMsgLayoutArqRetornoSaidaDTO{
	
	/** Atributo codMensagem. */
	private String codMensagem;
	
	/** Atributo mensagem. */
	private String mensagem;
	
	/** Atributo cdTipoLayoutArquivo. */
	private Integer cdTipoLayoutArquivo;
	
	/** Atributo dsTipoLayoutArquivo. */
	private String dsTipoLayoutArquivo;
	
	/** Atributo cdMensagemArquivoRetorno. */
	private String cdMensagemArquivoRetorno;
	
	/** Atributo cdTipoMensagemRetorno. */
	private Integer cdTipoMensagemRetorno;
	
	/** Atributo nrPosicaoInicialMensagem. */
	private Integer nrPosicaoInicialMensagem;
	
	/** Atributo nrPosicaoFinalMensagem. */
	private Integer nrPosicaoFinalMensagem;
	
	/** Atributo cdNivelMensagemLayout. */
	private Integer cdNivelMensagemLayout;
	
	/** Atributo dsMensagemLayoutRetorno. */
	private String dsMensagemLayoutRetorno;
	
	/** Atributo cdCanalInclusao. */
	private Integer cdCanalInclusao;
	
	/** Atributo dsCanalInclusao. */
	private String dsCanalInclusao;
	
	/** Atributo cdAutenticacaoSegurancaInclusao. */
	private String cdAutenticacaoSegurancaInclusao;
	
	/** Atributo nmOperacaoFluxoInclusao. */
	private String nmOperacaoFluxoInclusao;
	
	/** Atributo hrInclusaoRegistro. */
	private String hrInclusaoRegistro;
	
	/** Atributo cdCanalManutencao. */
	private Integer cdCanalManutencao;
	
	/** Atributo dsCanalManutencao. */
	private String dsCanalManutencao;
	
	/** Atributo cdAutenticacaoSegurancaManutencao. */
	private String cdAutenticacaoSegurancaManutencao;
	
	/** Atributo nmOperacaoFluxoManutencao. */
	private String nmOperacaoFluxoManutencao;
	
	/** Atributo hrManutencaoRegistro. */
	private String hrManutencaoRegistro;

	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem(){
		return codMensagem;
	}

	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem){
		this.codMensagem = codMensagem;
	}

	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem(){
		return mensagem;
	}

	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem){
		this.mensagem = mensagem;
	}

	/**
	 * Get: cdTipoLayoutArquivo.
	 *
	 * @return cdTipoLayoutArquivo
	 */
	public Integer getCdTipoLayoutArquivo(){
		return cdTipoLayoutArquivo;
	}

	/**
	 * Set: cdTipoLayoutArquivo.
	 *
	 * @param cdTipoLayoutArquivo the cd tipo layout arquivo
	 */
	public void setCdTipoLayoutArquivo(Integer cdTipoLayoutArquivo){
		this.cdTipoLayoutArquivo = cdTipoLayoutArquivo;
	}

	/**
	 * Get: cdMensagemArquivoRetorno.
	 *
	 * @return cdMensagemArquivoRetorno
	 */
	public String getCdMensagemArquivoRetorno(){
		return cdMensagemArquivoRetorno;
	}

	/**
	 * Set: cdMensagemArquivoRetorno.
	 *
	 * @param cdMensagemArquivoRetorno the cd mensagem arquivo retorno
	 */
	public void setCdMensagemArquivoRetorno(String cdMensagemArquivoRetorno){
		this.cdMensagemArquivoRetorno = cdMensagemArquivoRetorno;
	}

	/**
	 * Get: cdTipoMensagemRetorno.
	 *
	 * @return cdTipoMensagemRetorno
	 */
	public Integer getCdTipoMensagemRetorno(){
		return cdTipoMensagemRetorno;
	}

	/**
	 * Set: cdTipoMensagemRetorno.
	 *
	 * @param cdTipoMensagemRetorno the cd tipo mensagem retorno
	 */
	public void setCdTipoMensagemRetorno(Integer cdTipoMensagemRetorno){
		this.cdTipoMensagemRetorno = cdTipoMensagemRetorno;
	}

	/**
	 * Get: nrPosicaoInicialMensagem.
	 *
	 * @return nrPosicaoInicialMensagem
	 */
	public Integer getNrPosicaoInicialMensagem(){
		return nrPosicaoInicialMensagem;
	}

	/**
	 * Set: nrPosicaoInicialMensagem.
	 *
	 * @param nrPosicaoInicialMensagem the nr posicao inicial mensagem
	 */
	public void setNrPosicaoInicialMensagem(Integer nrPosicaoInicialMensagem){
		this.nrPosicaoInicialMensagem = nrPosicaoInicialMensagem;
	}

	/**
	 * Get: nrPosicaoFinalMensagem.
	 *
	 * @return nrPosicaoFinalMensagem
	 */
	public Integer getNrPosicaoFinalMensagem(){
		return nrPosicaoFinalMensagem;
	}

	/**
	 * Set: nrPosicaoFinalMensagem.
	 *
	 * @param nrPosicaoFinalMensagem the nr posicao final mensagem
	 */
	public void setNrPosicaoFinalMensagem(Integer nrPosicaoFinalMensagem){
		this.nrPosicaoFinalMensagem = nrPosicaoFinalMensagem;
	}

	/**
	 * Get: cdNivelMensagemLayout.
	 *
	 * @return cdNivelMensagemLayout
	 */
	public Integer getCdNivelMensagemLayout(){
		return cdNivelMensagemLayout;
	}

	/**
	 * Set: cdNivelMensagemLayout.
	 *
	 * @param cdNivelMensagemLayout the cd nivel mensagem layout
	 */
	public void setCdNivelMensagemLayout(Integer cdNivelMensagemLayout){
		this.cdNivelMensagemLayout = cdNivelMensagemLayout;
	}

	/**
	 * Get: dsMensagemLayoutRetorno.
	 *
	 * @return dsMensagemLayoutRetorno
	 */
	public String getDsMensagemLayoutRetorno(){
		return dsMensagemLayoutRetorno;
	}

	/**
	 * Set: dsMensagemLayoutRetorno.
	 *
	 * @param dsMensagemLayoutRetorno the ds mensagem layout retorno
	 */
	public void setDsMensagemLayoutRetorno(String dsMensagemLayoutRetorno){
		this.dsMensagemLayoutRetorno = dsMensagemLayoutRetorno;
	}

	/**
	 * Get: cdCanalInclusao.
	 *
	 * @return cdCanalInclusao
	 */
	public Integer getCdCanalInclusao(){
		return cdCanalInclusao;
	}

	/**
	 * Set: cdCanalInclusao.
	 *
	 * @param cdCanalInclusao the cd canal inclusao
	 */
	public void setCdCanalInclusao(Integer cdCanalInclusao){
		this.cdCanalInclusao = cdCanalInclusao;
	}

	/**
	 * Get: dsCanalInclusao.
	 *
	 * @return dsCanalInclusao
	 */
	public String getDsCanalInclusao(){
		return dsCanalInclusao;
	}

	/**
	 * Set: dsCanalInclusao.
	 *
	 * @param dsCanalInclusao the ds canal inclusao
	 */
	public void setDsCanalInclusao(String dsCanalInclusao){
		this.dsCanalInclusao = dsCanalInclusao;
	}

	/**
	 * Get: cdAutenticacaoSegurancaInclusao.
	 *
	 * @return cdAutenticacaoSegurancaInclusao
	 */
	public String getCdAutenticacaoSegurancaInclusao(){
		return cdAutenticacaoSegurancaInclusao;
	}

	/**
	 * Set: cdAutenticacaoSegurancaInclusao.
	 *
	 * @param cdAutenticacaoSegurancaInclusao the cd autenticacao seguranca inclusao
	 */
	public void setCdAutenticacaoSegurancaInclusao(String cdAutenticacaoSegurancaInclusao){
		this.cdAutenticacaoSegurancaInclusao = cdAutenticacaoSegurancaInclusao;
	}

	/**
	 * Get: nmOperacaoFluxoInclusao.
	 *
	 * @return nmOperacaoFluxoInclusao
	 */
	public String getNmOperacaoFluxoInclusao(){
		return nmOperacaoFluxoInclusao;
	}

	/**
	 * Set: nmOperacaoFluxoInclusao.
	 *
	 * @param nmOperacaoFluxoInclusao the nm operacao fluxo inclusao
	 */
	public void setNmOperacaoFluxoInclusao(String nmOperacaoFluxoInclusao){
		this.nmOperacaoFluxoInclusao = nmOperacaoFluxoInclusao;
	}

	/**
	 * Get: hrInclusaoRegistro.
	 *
	 * @return hrInclusaoRegistro
	 */
	public String getHrInclusaoRegistro(){
		return hrInclusaoRegistro;
	}

	/**
	 * Set: hrInclusaoRegistro.
	 *
	 * @param hrInclusaoRegistro the hr inclusao registro
	 */
	public void setHrInclusaoRegistro(String hrInclusaoRegistro){
		this.hrInclusaoRegistro = hrInclusaoRegistro;
	}

	/**
	 * Get: cdCanalManutencao.
	 *
	 * @return cdCanalManutencao
	 */
	public Integer getCdCanalManutencao(){
		return cdCanalManutencao;
	}

	/**
	 * Set: cdCanalManutencao.
	 *
	 * @param cdCanalManutencao the cd canal manutencao
	 */
	public void setCdCanalManutencao(Integer cdCanalManutencao){
		this.cdCanalManutencao = cdCanalManutencao;
	}

	/**
	 * Get: dsCanalManutencao.
	 *
	 * @return dsCanalManutencao
	 */
	public String getDsCanalManutencao(){
		return dsCanalManutencao;
	}

	/**
	 * Set: dsCanalManutencao.
	 *
	 * @param dsCanalManutencao the ds canal manutencao
	 */
	public void setDsCanalManutencao(String dsCanalManutencao){
		this.dsCanalManutencao = dsCanalManutencao;
	}

	/**
	 * Get: cdAutenticacaoSegurancaManutencao.
	 *
	 * @return cdAutenticacaoSegurancaManutencao
	 */
	public String getCdAutenticacaoSegurancaManutencao(){
		return cdAutenticacaoSegurancaManutencao;
	}

	/**
	 * Set: cdAutenticacaoSegurancaManutencao.
	 *
	 * @param cdAutenticacaoSegurancaManutencao the cd autenticacao seguranca manutencao
	 */
	public void setCdAutenticacaoSegurancaManutencao(String cdAutenticacaoSegurancaManutencao){
		this.cdAutenticacaoSegurancaManutencao = cdAutenticacaoSegurancaManutencao;
	}

	/**
	 * Get: nmOperacaoFluxoManutencao.
	 *
	 * @return nmOperacaoFluxoManutencao
	 */
	public String getNmOperacaoFluxoManutencao(){
		return nmOperacaoFluxoManutencao;
	}

	/**
	 * Set: nmOperacaoFluxoManutencao.
	 *
	 * @param nmOperacaoFluxoManutencao the nm operacao fluxo manutencao
	 */
	public void setNmOperacaoFluxoManutencao(String nmOperacaoFluxoManutencao){
		this.nmOperacaoFluxoManutencao = nmOperacaoFluxoManutencao;
	}

	/**
	 * Get: hrManutencaoRegistro.
	 *
	 * @return hrManutencaoRegistro
	 */
	public String getHrManutencaoRegistro(){
		return hrManutencaoRegistro;
	}

	/**
	 * Set: hrManutencaoRegistro.
	 *
	 * @param hrManutencaoRegistro the hr manutencao registro
	 */
	public void setHrManutencaoRegistro(String hrManutencaoRegistro){
		this.hrManutencaoRegistro = hrManutencaoRegistro;
	}

	/**
	 * Get: dsTipoLayoutArquivo.
	 *
	 * @return dsTipoLayoutArquivo
	 */
	public String getDsTipoLayoutArquivo() {
		return dsTipoLayoutArquivo;
	}

	/**
	 * Set: dsTipoLayoutArquivo.
	 *
	 * @param dsTipoLayoutArquivo the ds tipo layout arquivo
	 */
	public void setDsTipoLayoutArquivo(String dsTipoLayoutArquivo) {
		this.dsTipoLayoutArquivo = dsTipoLayoutArquivo;
	}
	
	
}