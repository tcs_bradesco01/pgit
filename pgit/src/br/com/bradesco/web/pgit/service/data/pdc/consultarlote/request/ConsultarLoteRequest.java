/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultarlote.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ConsultarLoteRequest.
 * 
 * @version $Revision$ $Date$
 */
public class ConsultarLoteRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _maximoOcorrencia
     */
    private int _maximoOcorrencia = 0;

    /**
     * keeps track of state for field: _maximoOcorrencia
     */
    private boolean _has_maximoOcorrencia;

    /**
     * Field _codSistemaLegado
     */
    private java.lang.String _codSistemaLegado;

    /**
     * Field _cpfCnpj
     */
    private long _cpfCnpj = 0;

    /**
     * keeps track of state for field: _cpfCnpj
     */
    private boolean _has_cpfCnpj;

    /**
     * Field _codFilialCnpj
     */
    private int _codFilialCnpj = 0;

    /**
     * keeps track of state for field: _codFilialCnpj
     */
    private boolean _has_codFilialCnpj;

    /**
     * Field _controleCnpj
     */
    private int _controleCnpj = 0;

    /**
     * keeps track of state for field: _controleCnpj
     */
    private boolean _has_controleCnpj;

    /**
     * Field _codPerfil
     */
    private long _codPerfil = 0;

    /**
     * keeps track of state for field: _codPerfil
     */
    private boolean _has_codPerfil;

    /**
     * Field _numArqRemessa
     */
    private long _numArqRemessa = 0;

    /**
     * keeps track of state for field: _numArqRemessa
     */
    private boolean _has_numArqRemessa;

    /**
     * Field _hrInclusaoRemessa
     */
    private java.lang.String _hrInclusaoRemessa;

    /**
     * Field _numLoteRemessa
     */
    private long _numLoteRemessa = 0;

    /**
     * keeps track of state for field: _numLoteRemessa
     */
    private boolean _has_numLoteRemessa;

    /**
     * Field _numSeqRegistroRemessa
     */
    private long _numSeqRegistroRemessa = 0;

    /**
     * keeps track of state for field: _numSeqRegistroRemessa
     */
    private boolean _has_numSeqRegistroRemessa;

    /**
     * Field _tipoPesquisa
     */
    private java.lang.String _tipoPesquisa;


      //----------------/
     //- Constructors -/
    //----------------/

    public ConsultarLoteRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarlote.request.ConsultarLoteRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCodFilialCnpj
     * 
     */
    public void deleteCodFilialCnpj()
    {
        this._has_codFilialCnpj= false;
    } //-- void deleteCodFilialCnpj() 

    /**
     * Method deleteCodPerfil
     * 
     */
    public void deleteCodPerfil()
    {
        this._has_codPerfil= false;
    } //-- void deleteCodPerfil() 

    /**
     * Method deleteControleCnpj
     * 
     */
    public void deleteControleCnpj()
    {
        this._has_controleCnpj= false;
    } //-- void deleteControleCnpj() 

    /**
     * Method deleteCpfCnpj
     * 
     */
    public void deleteCpfCnpj()
    {
        this._has_cpfCnpj= false;
    } //-- void deleteCpfCnpj() 

    /**
     * Method deleteMaximoOcorrencia
     * 
     */
    public void deleteMaximoOcorrencia()
    {
        this._has_maximoOcorrencia= false;
    } //-- void deleteMaximoOcorrencia() 

    /**
     * Method deleteNumArqRemessa
     * 
     */
    public void deleteNumArqRemessa()
    {
        this._has_numArqRemessa= false;
    } //-- void deleteNumArqRemessa() 

    /**
     * Method deleteNumLoteRemessa
     * 
     */
    public void deleteNumLoteRemessa()
    {
        this._has_numLoteRemessa= false;
    } //-- void deleteNumLoteRemessa() 

    /**
     * Method deleteNumSeqRegistroRemessa
     * 
     */
    public void deleteNumSeqRegistroRemessa()
    {
        this._has_numSeqRegistroRemessa= false;
    } //-- void deleteNumSeqRegistroRemessa() 

    /**
     * Returns the value of field 'codFilialCnpj'.
     * 
     * @return int
     * @return the value of field 'codFilialCnpj'.
     */
    public int getCodFilialCnpj()
    {
        return this._codFilialCnpj;
    } //-- int getCodFilialCnpj() 

    /**
     * Returns the value of field 'codPerfil'.
     * 
     * @return long
     * @return the value of field 'codPerfil'.
     */
    public long getCodPerfil()
    {
        return this._codPerfil;
    } //-- long getCodPerfil() 

    /**
     * Returns the value of field 'codSistemaLegado'.
     * 
     * @return String
     * @return the value of field 'codSistemaLegado'.
     */
    public java.lang.String getCodSistemaLegado()
    {
        return this._codSistemaLegado;
    } //-- java.lang.String getCodSistemaLegado() 

    /**
     * Returns the value of field 'controleCnpj'.
     * 
     * @return int
     * @return the value of field 'controleCnpj'.
     */
    public int getControleCnpj()
    {
        return this._controleCnpj;
    } //-- int getControleCnpj() 

    /**
     * Returns the value of field 'cpfCnpj'.
     * 
     * @return long
     * @return the value of field 'cpfCnpj'.
     */
    public long getCpfCnpj()
    {
        return this._cpfCnpj;
    } //-- long getCpfCnpj() 

    /**
     * Returns the value of field 'hrInclusaoRemessa'.
     * 
     * @return String
     * @return the value of field 'hrInclusaoRemessa'.
     */
    public java.lang.String getHrInclusaoRemessa()
    {
        return this._hrInclusaoRemessa;
    } //-- java.lang.String getHrInclusaoRemessa() 

    /**
     * Returns the value of field 'maximoOcorrencia'.
     * 
     * @return int
     * @return the value of field 'maximoOcorrencia'.
     */
    public int getMaximoOcorrencia()
    {
        return this._maximoOcorrencia;
    } //-- int getMaximoOcorrencia() 

    /**
     * Returns the value of field 'numArqRemessa'.
     * 
     * @return long
     * @return the value of field 'numArqRemessa'.
     */
    public long getNumArqRemessa()
    {
        return this._numArqRemessa;
    } //-- long getNumArqRemessa() 

    /**
     * Returns the value of field 'numLoteRemessa'.
     * 
     * @return long
     * @return the value of field 'numLoteRemessa'.
     */
    public long getNumLoteRemessa()
    {
        return this._numLoteRemessa;
    } //-- long getNumLoteRemessa() 

    /**
     * Returns the value of field 'numSeqRegistroRemessa'.
     * 
     * @return long
     * @return the value of field 'numSeqRegistroRemessa'.
     */
    public long getNumSeqRegistroRemessa()
    {
        return this._numSeqRegistroRemessa;
    } //-- long getNumSeqRegistroRemessa() 

    /**
     * Returns the value of field 'tipoPesquisa'.
     * 
     * @return String
     * @return the value of field 'tipoPesquisa'.
     */
    public java.lang.String getTipoPesquisa()
    {
        return this._tipoPesquisa;
    } //-- java.lang.String getTipoPesquisa() 

    /**
     * Method hasCodFilialCnpj
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCodFilialCnpj()
    {
        return this._has_codFilialCnpj;
    } //-- boolean hasCodFilialCnpj() 

    /**
     * Method hasCodPerfil
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCodPerfil()
    {
        return this._has_codPerfil;
    } //-- boolean hasCodPerfil() 

    /**
     * Method hasControleCnpj
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasControleCnpj()
    {
        return this._has_controleCnpj;
    } //-- boolean hasControleCnpj() 

    /**
     * Method hasCpfCnpj
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCpfCnpj()
    {
        return this._has_cpfCnpj;
    } //-- boolean hasCpfCnpj() 

    /**
     * Method hasMaximoOcorrencia
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasMaximoOcorrencia()
    {
        return this._has_maximoOcorrencia;
    } //-- boolean hasMaximoOcorrencia() 

    /**
     * Method hasNumArqRemessa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNumArqRemessa()
    {
        return this._has_numArqRemessa;
    } //-- boolean hasNumArqRemessa() 

    /**
     * Method hasNumLoteRemessa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNumLoteRemessa()
    {
        return this._has_numLoteRemessa;
    } //-- boolean hasNumLoteRemessa() 

    /**
     * Method hasNumSeqRegistroRemessa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNumSeqRegistroRemessa()
    {
        return this._has_numSeqRegistroRemessa;
    } //-- boolean hasNumSeqRegistroRemessa() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'codFilialCnpj'.
     * 
     * @param codFilialCnpj the value of field 'codFilialCnpj'.
     */
    public void setCodFilialCnpj(int codFilialCnpj)
    {
        this._codFilialCnpj = codFilialCnpj;
        this._has_codFilialCnpj = true;
    } //-- void setCodFilialCnpj(int) 

    /**
     * Sets the value of field 'codPerfil'.
     * 
     * @param codPerfil the value of field 'codPerfil'.
     */
    public void setCodPerfil(long codPerfil)
    {
        this._codPerfil = codPerfil;
        this._has_codPerfil = true;
    } //-- void setCodPerfil(long) 

    /**
     * Sets the value of field 'codSistemaLegado'.
     * 
     * @param codSistemaLegado the value of field 'codSistemaLegado'
     */
    public void setCodSistemaLegado(java.lang.String codSistemaLegado)
    {
        this._codSistemaLegado = codSistemaLegado;
    } //-- void setCodSistemaLegado(java.lang.String) 

    /**
     * Sets the value of field 'controleCnpj'.
     * 
     * @param controleCnpj the value of field 'controleCnpj'.
     */
    public void setControleCnpj(int controleCnpj)
    {
        this._controleCnpj = controleCnpj;
        this._has_controleCnpj = true;
    } //-- void setControleCnpj(int) 

    /**
     * Sets the value of field 'cpfCnpj'.
     * 
     * @param cpfCnpj the value of field 'cpfCnpj'.
     */
    public void setCpfCnpj(long cpfCnpj)
    {
        this._cpfCnpj = cpfCnpj;
        this._has_cpfCnpj = true;
    } //-- void setCpfCnpj(long) 

    /**
     * Sets the value of field 'hrInclusaoRemessa'.
     * 
     * @param hrInclusaoRemessa the value of field
     * 'hrInclusaoRemessa'.
     */
    public void setHrInclusaoRemessa(java.lang.String hrInclusaoRemessa)
    {
        this._hrInclusaoRemessa = hrInclusaoRemessa;
    } //-- void setHrInclusaoRemessa(java.lang.String) 

    /**
     * Sets the value of field 'maximoOcorrencia'.
     * 
     * @param maximoOcorrencia the value of field 'maximoOcorrencia'
     */
    public void setMaximoOcorrencia(int maximoOcorrencia)
    {
        this._maximoOcorrencia = maximoOcorrencia;
        this._has_maximoOcorrencia = true;
    } //-- void setMaximoOcorrencia(int) 

    /**
     * Sets the value of field 'numArqRemessa'.
     * 
     * @param numArqRemessa the value of field 'numArqRemessa'.
     */
    public void setNumArqRemessa(long numArqRemessa)
    {
        this._numArqRemessa = numArqRemessa;
        this._has_numArqRemessa = true;
    } //-- void setNumArqRemessa(long) 

    /**
     * Sets the value of field 'numLoteRemessa'.
     * 
     * @param numLoteRemessa the value of field 'numLoteRemessa'.
     */
    public void setNumLoteRemessa(long numLoteRemessa)
    {
        this._numLoteRemessa = numLoteRemessa;
        this._has_numLoteRemessa = true;
    } //-- void setNumLoteRemessa(long) 

    /**
     * Sets the value of field 'numSeqRegistroRemessa'.
     * 
     * @param numSeqRegistroRemessa the value of field
     * 'numSeqRegistroRemessa'.
     */
    public void setNumSeqRegistroRemessa(long numSeqRegistroRemessa)
    {
        this._numSeqRegistroRemessa = numSeqRegistroRemessa;
        this._has_numSeqRegistroRemessa = true;
    } //-- void setNumSeqRegistroRemessa(long) 

    /**
     * Sets the value of field 'tipoPesquisa'.
     * 
     * @param tipoPesquisa the value of field 'tipoPesquisa'.
     */
    public void setTipoPesquisa(java.lang.String tipoPesquisa)
    {
        this._tipoPesquisa = tipoPesquisa;
    } //-- void setTipoPesquisa(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ConsultarLoteRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultarlote.request.ConsultarLoteRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultarlote.request.ConsultarLoteRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultarlote.request.ConsultarLoteRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarlote.request.ConsultarLoteRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
