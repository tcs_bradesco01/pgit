package br.com.bradesco.web.pgit.service.business.mantervinculacaoconveniocontasalario.bean;

import java.util.ArrayList;
import java.util.List;

public class ListarVincConvnCtaSalarioSaidaDTO {
	
	private String indicadorConvenioNovo;
	private String usuarioInclusao;
	private String horarioInclusao;
	private String descCanalInclusao;
	private String descCanalManutencao;
	private int tipoCanalInclusao;
	private int tipoCanalManutencao;
	private String usuarioManutencao;
	private String horarioManutencao;
	private int numOcorrencias;
	
	
	private List<ListarVincConvnCtaSalarioOcorrenciasDTO> listaOcorrencias = new ArrayList<ListarVincConvnCtaSalarioOcorrenciasDTO>();
	public int getNumOcorrencias() {
		return numOcorrencias;
	}
	public void setNumOcorrencias(int numOcorrencias) {
		this.numOcorrencias = numOcorrencias;
	}
	public List<ListarVincConvnCtaSalarioOcorrenciasDTO> getListaOcorrencias() {
		return listaOcorrencias;
	}
	public void setListaOcorrencias(
			List<ListarVincConvnCtaSalarioOcorrenciasDTO> listaOcorrencias) {
		this.listaOcorrencias = listaOcorrencias;
	}
	public String getIndicadorConvenioNovo() {
		return indicadorConvenioNovo;
	}
	public void setIndicadorConvenioNovo(String indicadorConvenioNovo) {
		this.indicadorConvenioNovo = indicadorConvenioNovo;
	}
	public String getUsuarioInclusao() {
		return usuarioInclusao;
	}
	public void setUsuarioInclusao(String usuarioInclusao) {
		this.usuarioInclusao = usuarioInclusao;
	}
	public String getHorarioInclusao() {
		return horarioInclusao;
	}
	public void setHorarioInclusao(String horarioInclusao) {
		this.horarioInclusao = horarioInclusao;
	}
	public String getDescCanalInclusao() {
		return descCanalInclusao;
	}
	public void setDescCanalInclusao(String descCanalInclusao) {
		this.descCanalInclusao = descCanalInclusao;
	}
	public String getDescCanalManutencao() {
		return descCanalManutencao;
	}
	public void setDescCanalManutencao(String descCanalManutencao) {
		this.descCanalManutencao = descCanalManutencao;
	}
	public int getTipoCanalInclusao() {
		return tipoCanalInclusao;
	}
	public void setTipoCanalInclusao(int tipoCanalInclusao) {
		this.tipoCanalInclusao = tipoCanalInclusao;
	}
	public int getTipoCanalManutencao() {
		return tipoCanalManutencao;
	}
	public void setTipoCanalManutencao(int tipoCanalManutencao) {
		this.tipoCanalManutencao = tipoCanalManutencao;
	}
	public String getUsuarioManutencao() {
		return usuarioManutencao;
	}
	public void setUsuarioManutencao(String usuarioManutencao) {
		this.usuarioManutencao = usuarioManutencao;
	}
	public String getHorarioManutencao() {
		return horarioManutencao;
	}
	public void setHorarioManutencao(String horarioManutencao) {
		this.horarioManutencao = horarioManutencao;
	}
	
	
	
	
	

}
