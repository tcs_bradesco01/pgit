/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listarmotivosituacaovinccontrato.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdMotivoSituacaoContrato
     */
    private int _cdMotivoSituacaoContrato = 0;

    /**
     * keeps track of state for field: _cdMotivoSituacaoContrato
     */
    private boolean _has_cdMotivoSituacaoContrato;

    /**
     * Field _dsMotivoSituacaoContrato
     */
    private java.lang.String _dsMotivoSituacaoContrato;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarmotivosituacaovinccontrato.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdMotivoSituacaoContrato
     * 
     */
    public void deleteCdMotivoSituacaoContrato()
    {
        this._has_cdMotivoSituacaoContrato= false;
    } //-- void deleteCdMotivoSituacaoContrato() 

    /**
     * Returns the value of field 'cdMotivoSituacaoContrato'.
     * 
     * @return int
     * @return the value of field 'cdMotivoSituacaoContrato'.
     */
    public int getCdMotivoSituacaoContrato()
    {
        return this._cdMotivoSituacaoContrato;
    } //-- int getCdMotivoSituacaoContrato() 

    /**
     * Returns the value of field 'dsMotivoSituacaoContrato'.
     * 
     * @return String
     * @return the value of field 'dsMotivoSituacaoContrato'.
     */
    public java.lang.String getDsMotivoSituacaoContrato()
    {
        return this._dsMotivoSituacaoContrato;
    } //-- java.lang.String getDsMotivoSituacaoContrato() 

    /**
     * Method hasCdMotivoSituacaoContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMotivoSituacaoContrato()
    {
        return this._has_cdMotivoSituacaoContrato;
    } //-- boolean hasCdMotivoSituacaoContrato() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdMotivoSituacaoContrato'.
     * 
     * @param cdMotivoSituacaoContrato the value of field
     * 'cdMotivoSituacaoContrato'.
     */
    public void setCdMotivoSituacaoContrato(int cdMotivoSituacaoContrato)
    {
        this._cdMotivoSituacaoContrato = cdMotivoSituacaoContrato;
        this._has_cdMotivoSituacaoContrato = true;
    } //-- void setCdMotivoSituacaoContrato(int) 

    /**
     * Sets the value of field 'dsMotivoSituacaoContrato'.
     * 
     * @param dsMotivoSituacaoContrato the value of field
     * 'dsMotivoSituacaoContrato'.
     */
    public void setDsMotivoSituacaoContrato(java.lang.String dsMotivoSituacaoContrato)
    {
        this._dsMotivoSituacaoContrato = dsMotivoSituacaoContrato;
    } //-- void setDsMotivoSituacaoContrato(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listarmotivosituacaovinccontrato.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listarmotivosituacaovinccontrato.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listarmotivosituacaovinccontrato.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarmotivosituacaovinccontrato.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
