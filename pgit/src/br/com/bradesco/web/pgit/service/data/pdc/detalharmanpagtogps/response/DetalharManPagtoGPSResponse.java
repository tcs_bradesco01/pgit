/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.detalharmanpagtogps.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.math.BigDecimal;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class DetalharManPagtoGPSResponse.
 * 
 * @version $Revision$ $Date$
 */
public class DetalharManPagtoGPSResponse implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _codMensagem
     */
    private java.lang.String _codMensagem;

    /**
     * Field _mensagem
     */
    private java.lang.String _mensagem;

    /**
     * Field _cdDddContribuinte
     */
    private int _cdDddContribuinte = 0;

    /**
     * keeps track of state for field: _cdDddContribuinte
     */
    private boolean _has_cdDddContribuinte;

    /**
     * Field _cdTelContribuinte
     */
    private java.lang.String _cdTelContribuinte;

    /**
     * Field _cdInss
     */
    private int _cdInss = 0;

    /**
     * keeps track of state for field: _cdInss
     */
    private boolean _has_cdInss;

    /**
     * Field _dsMesAnoCompt
     */
    private int _dsMesAnoCompt = 0;

    /**
     * keeps track of state for field: _dsMesAnoCompt
     */
    private boolean _has_dsMesAnoCompt;

    /**
     * Field _vlPrincipal
     */
    private java.math.BigDecimal _vlPrincipal = new java.math.BigDecimal("0");

    /**
     * Field _vlOutraEntidade
     */
    private java.math.BigDecimal _vlOutraEntidade = new java.math.BigDecimal("0");

    /**
     * Field _vlAbatimento
     */
    private java.math.BigDecimal _vlAbatimento = new java.math.BigDecimal("0");

    /**
     * Field _vlMulta
     */
    private java.math.BigDecimal _vlMulta = new java.math.BigDecimal("0");

    /**
     * Field _vlMora
     */
    private java.math.BigDecimal _vlMora = new java.math.BigDecimal("0");

    /**
     * Field _vlTotal
     */
    private java.math.BigDecimal _vlTotal = new java.math.BigDecimal("0");

    /**
     * Field _cdIndicadorModalidadePgit
     */
    private int _cdIndicadorModalidadePgit = 0;

    /**
     * keeps track of state for field: _cdIndicadorModalidadePgit
     */
    private boolean _has_cdIndicadorModalidadePgit;

    /**
     * Field _dsPagamento
     */
    private java.lang.String _dsPagamento;

    /**
     * Field _dsBancoDebito
     */
    private java.lang.String _dsBancoDebito;

    /**
     * Field _dsAgenciaDebito
     */
    private java.lang.String _dsAgenciaDebito;

    /**
     * Field _dsTipoContaDebito
     */
    private java.lang.String _dsTipoContaDebito;

    /**
     * Field _dsContrato
     */
    private java.lang.String _dsContrato;

    /**
     * Field _cdSituacaoContrato
     */
    private java.lang.String _cdSituacaoContrato;

    /**
     * Field _dtAgendamento
     */
    private java.lang.String _dtAgendamento;

    /**
     * Field _vlAgendado
     */
    private java.math.BigDecimal _vlAgendado = new java.math.BigDecimal("0");

    /**
     * Field _vlEfetivo
     */
    private java.math.BigDecimal _vlEfetivo = new java.math.BigDecimal("0");

    /**
     * Field _cdIndicadorEconomicoMoeda
     */
    private int _cdIndicadorEconomicoMoeda = 0;

    /**
     * keeps track of state for field: _cdIndicadorEconomicoMoeda
     */
    private boolean _has_cdIndicadorEconomicoMoeda;

    /**
     * Field _dsMoeda
     */
    private java.lang.String _dsMoeda;

    /**
     * Field _qtMoeda
     */
    private java.math.BigDecimal _qtMoeda = new java.math.BigDecimal("0");

    /**
     * Field _dtVencimento
     */
    private java.lang.String _dtVencimento;

    /**
     * Field _dsMensagemPrimeiraLinha
     */
    private java.lang.String _dsMensagemPrimeiraLinha;

    /**
     * Field _dsMensagemSegundaLinha
     */
    private java.lang.String _dsMensagemSegundaLinha;

    /**
     * Field _dsUsoEmpresa
     */
    private java.lang.String _dsUsoEmpresa;

    /**
     * Field _cdSituacaoOperacaoPagamento
     */
    private int _cdSituacaoOperacaoPagamento = 0;

    /**
     * keeps track of state for field: _cdSituacaoOperacaoPagamento
     */
    private boolean _has_cdSituacaoOperacaoPagamento;

    /**
     * Field _cdMotivoSituacaoPagamento
     */
    private int _cdMotivoSituacaoPagamento = 0;

    /**
     * keeps track of state for field: _cdMotivoSituacaoPagamento
     */
    private boolean _has_cdMotivoSituacaoPagamento;

    /**
     * Field _cdListaDebito
     */
    private long _cdListaDebito = 0;

    /**
     * keeps track of state for field: _cdListaDebito
     */
    private boolean _has_cdListaDebito;

    /**
     * Field _cdTipoIsncricaoFavorecido
     */
    private int _cdTipoIsncricaoFavorecido = 0;

    /**
     * keeps track of state for field: _cdTipoIsncricaoFavorecido
     */
    private boolean _has_cdTipoIsncricaoFavorecido;

    /**
     * Field _nrDocumento
     */
    private long _nrDocumento = 0;

    /**
     * keeps track of state for field: _nrDocumento
     */
    private boolean _has_nrDocumento;

    /**
     * Field _cdSerieDocumento
     */
    private java.lang.String _cdSerieDocumento;

    /**
     * Field _cdTipoDocumento
     */
    private int _cdTipoDocumento = 0;

    /**
     * keeps track of state for field: _cdTipoDocumento
     */
    private boolean _has_cdTipoDocumento;

    /**
     * Field _dsTipoDocumento
     */
    private java.lang.String _dsTipoDocumento;

    /**
     * Field _vlDocumento
     */
    private java.math.BigDecimal _vlDocumento = new java.math.BigDecimal("0");

    /**
     * Field _dtEmissaoDocumento
     */
    private java.lang.String _dtEmissaoDocumento;

    /**
     * Field _nrSequenciaArquivoRemessa
     */
    private long _nrSequenciaArquivoRemessa = 0;

    /**
     * keeps track of state for field: _nrSequenciaArquivoRemessa
     */
    private boolean _has_nrSequenciaArquivoRemessa;

    /**
     * Field _nrLoteArquivoRemessa
     */
    private long _nrLoteArquivoRemessa = 0;

    /**
     * keeps track of state for field: _nrLoteArquivoRemessa
     */
    private boolean _has_nrLoteArquivoRemessa;

    /**
     * Field _cdFavorecido
     */
    private long _cdFavorecido = 0;

    /**
     * keeps track of state for field: _cdFavorecido
     */
    private boolean _has_cdFavorecido;

    /**
     * Field _dsBancoFavorecido
     */
    private java.lang.String _dsBancoFavorecido;

    /**
     * Field _dsAgenciaFavorecido
     */
    private java.lang.String _dsAgenciaFavorecido;

    /**
     * Field _cdTipoContaFavorecido
     */
    private int _cdTipoContaFavorecido = 0;

    /**
     * keeps track of state for field: _cdTipoContaFavorecido
     */
    private boolean _has_cdTipoContaFavorecido;

    /**
     * Field _dsTipoContaFavorecido
     */
    private java.lang.String _dsTipoContaFavorecido;

    /**
     * Field _dsMotivoSituacao
     */
    private java.lang.String _dsMotivoSituacao;

    /**
     * Field _cdTipoManutencao
     */
    private int _cdTipoManutencao = 0;

    /**
     * keeps track of state for field: _cdTipoManutencao
     */
    private boolean _has_cdTipoManutencao;

    /**
     * Field _dsTipoManutencao
     */
    private java.lang.String _dsTipoManutencao;

    /**
     * Field _dsIndicadorAutorizacao
     */
    private java.lang.String _dsIndicadorAutorizacao;

    /**
     * Field _nrLoteInterno
     */
    private long _nrLoteInterno = 0;

    /**
     * keeps track of state for field: _nrLoteInterno
     */
    private boolean _has_nrLoteInterno;

    /**
     * Field _dsTipoLayout
     */
    private java.lang.String _dsTipoLayout;

    /**
     * Field _dtInclusao
     */
    private java.lang.String _dtInclusao;

    /**
     * Field _hrInclusao
     */
    private java.lang.String _hrInclusao;

    /**
     * Field _cdUsuarioInclusaoInterno
     */
    private java.lang.String _cdUsuarioInclusaoInterno;

    /**
     * Field _cdUsuarioInclusaoExterno
     */
    private java.lang.String _cdUsuarioInclusaoExterno;

    /**
     * Field _cdTipoCanalInclusao
     */
    private int _cdTipoCanalInclusao = 0;

    /**
     * keeps track of state for field: _cdTipoCanalInclusao
     */
    private boolean _has_cdTipoCanalInclusao;

    /**
     * Field _dsTipoCanalInclusao
     */
    private java.lang.String _dsTipoCanalInclusao;

    /**
     * Field _cdFluxoInclusao
     */
    private java.lang.String _cdFluxoInclusao;

    /**
     * Field _dtManutencao
     */
    private java.lang.String _dtManutencao;

    /**
     * Field _hrManutencao
     */
    private java.lang.String _hrManutencao;

    /**
     * Field _cdUsuarioManutencaoInterno
     */
    private java.lang.String _cdUsuarioManutencaoInterno;

    /**
     * Field _cdUsuarioManutencaoExterno
     */
    private java.lang.String _cdUsuarioManutencaoExterno;

    /**
     * Field _cdTipoCanalManutencao
     */
    private int _cdTipoCanalManutencao = 0;

    /**
     * keeps track of state for field: _cdTipoCanalManutencao
     */
    private boolean _has_cdTipoCanalManutencao;

    /**
     * Field _dsTipoCanalManutencao
     */
    private java.lang.String _dsTipoCanalManutencao;

    /**
     * Field _cdFluxoManutencao
     */
    private java.lang.String _cdFluxoManutencao;

    /**
     * Field _cdCpfCnpjCliente
     */
    private long _cdCpfCnpjCliente = 0;

    /**
     * keeps track of state for field: _cdCpfCnpjCliente
     */
    private boolean _has_cdCpfCnpjCliente;

    /**
     * Field _nomeCliente
     */
    private java.lang.String _nomeCliente;

    /**
     * Field _cdBancoDebito
     */
    private int _cdBancoDebito = 0;

    /**
     * keeps track of state for field: _cdBancoDebito
     */
    private boolean _has_cdBancoDebito;

    /**
     * Field _cdAgenciaDebito
     */
    private int _cdAgenciaDebito = 0;

    /**
     * keeps track of state for field: _cdAgenciaDebito
     */
    private boolean _has_cdAgenciaDebito;

    /**
     * Field _cdDigAgenciaDebto
     */
    private java.lang.String _cdDigAgenciaDebto;

    /**
     * Field _cdContaDebito
     */
    private long _cdContaDebito = 0;

    /**
     * keeps track of state for field: _cdContaDebito
     */
    private boolean _has_cdContaDebito;

    /**
     * Field _dsDigAgenciaDebito
     */
    private java.lang.String _dsDigAgenciaDebito;

    /**
     * Field _nmInscriFavorecido
     */
    private long _nmInscriFavorecido = 0;

    /**
     * keeps track of state for field: _nmInscriFavorecido
     */
    private boolean _has_nmInscriFavorecido;

    /**
     * Field _dsNomeFavorecido
     */
    private java.lang.String _dsNomeFavorecido;

    /**
     * Field _cdBancoCredito
     */
    private int _cdBancoCredito = 0;

    /**
     * keeps track of state for field: _cdBancoCredito
     */
    private boolean _has_cdBancoCredito;

    /**
     * Field _cdAgenciaCredito
     */
    private int _cdAgenciaCredito = 0;

    /**
     * keeps track of state for field: _cdAgenciaCredito
     */
    private boolean _has_cdAgenciaCredito;

    /**
     * Field _cdDigAgenciaCredito
     */
    private java.lang.String _cdDigAgenciaCredito;

    /**
     * Field _cdContaCredito
     */
    private long _cdContaCredito = 0;

    /**
     * keeps track of state for field: _cdContaCredito
     */
    private boolean _has_cdContaCredito;

    /**
     * Field _cdDigContaCredito
     */
    private java.lang.String _cdDigContaCredito;

    /**
     * Field _dtPagamento
     */
    private java.lang.String _dtPagamento;

    /**
     * Field _cdSituacaoPagamento
     */
    private java.lang.String _cdSituacaoPagamento;

    /**
     * Field _cdIndicadorAuto
     */
    private int _cdIndicadorAuto = 0;

    /**
     * keeps track of state for field: _cdIndicadorAuto
     */
    private boolean _has_cdIndicadorAuto;

    /**
     * Field _cdIndicadorSemConsulta
     */
    private int _cdIndicadorSemConsulta = 0;

    /**
     * keeps track of state for field: _cdIndicadorSemConsulta
     */
    private boolean _has_cdIndicadorSemConsulta;

    /**
     * Field _dsPessoaJuridicaContrato
     */
    private java.lang.String _dsPessoaJuridicaContrato;

    /**
     * Field _nrSequenciaContratoNegocio
     */
    private long _nrSequenciaContratoNegocio = 0;

    /**
     * keeps track of state for field: _nrSequenciaContratoNegocio
     */
    private boolean _has_nrSequenciaContratoNegocio;

    /**
     * Field _dsTipoServicoOperacao
     */
    private java.lang.String _dsTipoServicoOperacao;

    /**
     * Field _dsModalidadeRelacionado
     */
    private java.lang.String _dsModalidadeRelacionado;

    /**
     * Field _cdControlePagamento
     */
    private java.lang.String _cdControlePagamento;

    /**
     * Field _dtDevolucaoEstorno
     */
    private java.lang.String _dtDevolucaoEstorno;

    /**
     * Field _vlFloatingPagamento
     */
    private java.math.BigDecimal _vlFloatingPagamento = new java.math.BigDecimal("0");

    /**
     * Field _dtFloatPgto
     */
    private java.lang.String _dtFloatPgto;

    /**
     * Field _dtEfetivacaoFloatPgto
     */
    private java.lang.String _dtEfetivacaoFloatPgto;


      //----------------/
     //- Constructors -/
    //----------------/

    public DetalharManPagtoGPSResponse() 
     {
        super();
        setVlPrincipal(new java.math.BigDecimal("0"));
        setVlOutraEntidade(new java.math.BigDecimal("0"));
        setVlAbatimento(new java.math.BigDecimal("0"));
        setVlMulta(new java.math.BigDecimal("0"));
        setVlMora(new java.math.BigDecimal("0"));
        setVlTotal(new java.math.BigDecimal("0"));
        setVlAgendado(new java.math.BigDecimal("0"));
        setVlEfetivo(new java.math.BigDecimal("0"));
        setQtMoeda(new java.math.BigDecimal("0"));
        setVlDocumento(new java.math.BigDecimal("0"));
        setVlFloatingPagamento(new java.math.BigDecimal("0"));
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharmanpagtogps.response.DetalharManPagtoGPSResponse()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdAgenciaCredito
     * 
     */
    public void deleteCdAgenciaCredito()
    {
        this._has_cdAgenciaCredito= false;
    } //-- void deleteCdAgenciaCredito() 

    /**
     * Method deleteCdAgenciaDebito
     * 
     */
    public void deleteCdAgenciaDebito()
    {
        this._has_cdAgenciaDebito= false;
    } //-- void deleteCdAgenciaDebito() 

    /**
     * Method deleteCdBancoCredito
     * 
     */
    public void deleteCdBancoCredito()
    {
        this._has_cdBancoCredito= false;
    } //-- void deleteCdBancoCredito() 

    /**
     * Method deleteCdBancoDebito
     * 
     */
    public void deleteCdBancoDebito()
    {
        this._has_cdBancoDebito= false;
    } //-- void deleteCdBancoDebito() 

    /**
     * Method deleteCdContaCredito
     * 
     */
    public void deleteCdContaCredito()
    {
        this._has_cdContaCredito= false;
    } //-- void deleteCdContaCredito() 

    /**
     * Method deleteCdContaDebito
     * 
     */
    public void deleteCdContaDebito()
    {
        this._has_cdContaDebito= false;
    } //-- void deleteCdContaDebito() 

    /**
     * Method deleteCdCpfCnpjCliente
     * 
     */
    public void deleteCdCpfCnpjCliente()
    {
        this._has_cdCpfCnpjCliente= false;
    } //-- void deleteCdCpfCnpjCliente() 

    /**
     * Method deleteCdDddContribuinte
     * 
     */
    public void deleteCdDddContribuinte()
    {
        this._has_cdDddContribuinte= false;
    } //-- void deleteCdDddContribuinte() 

    /**
     * Method deleteCdFavorecido
     * 
     */
    public void deleteCdFavorecido()
    {
        this._has_cdFavorecido= false;
    } //-- void deleteCdFavorecido() 

    /**
     * Method deleteCdIndicadorAuto
     * 
     */
    public void deleteCdIndicadorAuto()
    {
        this._has_cdIndicadorAuto= false;
    } //-- void deleteCdIndicadorAuto() 

    /**
     * Method deleteCdIndicadorEconomicoMoeda
     * 
     */
    public void deleteCdIndicadorEconomicoMoeda()
    {
        this._has_cdIndicadorEconomicoMoeda= false;
    } //-- void deleteCdIndicadorEconomicoMoeda() 

    /**
     * Method deleteCdIndicadorModalidadePgit
     * 
     */
    public void deleteCdIndicadorModalidadePgit()
    {
        this._has_cdIndicadorModalidadePgit= false;
    } //-- void deleteCdIndicadorModalidadePgit() 

    /**
     * Method deleteCdIndicadorSemConsulta
     * 
     */
    public void deleteCdIndicadorSemConsulta()
    {
        this._has_cdIndicadorSemConsulta= false;
    } //-- void deleteCdIndicadorSemConsulta() 

    /**
     * Method deleteCdInss
     * 
     */
    public void deleteCdInss()
    {
        this._has_cdInss= false;
    } //-- void deleteCdInss() 

    /**
     * Method deleteCdListaDebito
     * 
     */
    public void deleteCdListaDebito()
    {
        this._has_cdListaDebito= false;
    } //-- void deleteCdListaDebito() 

    /**
     * Method deleteCdMotivoSituacaoPagamento
     * 
     */
    public void deleteCdMotivoSituacaoPagamento()
    {
        this._has_cdMotivoSituacaoPagamento= false;
    } //-- void deleteCdMotivoSituacaoPagamento() 

    /**
     * Method deleteCdSituacaoOperacaoPagamento
     * 
     */
    public void deleteCdSituacaoOperacaoPagamento()
    {
        this._has_cdSituacaoOperacaoPagamento= false;
    } //-- void deleteCdSituacaoOperacaoPagamento() 

    /**
     * Method deleteCdTipoCanalInclusao
     * 
     */
    public void deleteCdTipoCanalInclusao()
    {
        this._has_cdTipoCanalInclusao= false;
    } //-- void deleteCdTipoCanalInclusao() 

    /**
     * Method deleteCdTipoCanalManutencao
     * 
     */
    public void deleteCdTipoCanalManutencao()
    {
        this._has_cdTipoCanalManutencao= false;
    } //-- void deleteCdTipoCanalManutencao() 

    /**
     * Method deleteCdTipoContaFavorecido
     * 
     */
    public void deleteCdTipoContaFavorecido()
    {
        this._has_cdTipoContaFavorecido= false;
    } //-- void deleteCdTipoContaFavorecido() 

    /**
     * Method deleteCdTipoDocumento
     * 
     */
    public void deleteCdTipoDocumento()
    {
        this._has_cdTipoDocumento= false;
    } //-- void deleteCdTipoDocumento() 

    /**
     * Method deleteCdTipoIsncricaoFavorecido
     * 
     */
    public void deleteCdTipoIsncricaoFavorecido()
    {
        this._has_cdTipoIsncricaoFavorecido= false;
    } //-- void deleteCdTipoIsncricaoFavorecido() 

    /**
     * Method deleteCdTipoManutencao
     * 
     */
    public void deleteCdTipoManutencao()
    {
        this._has_cdTipoManutencao= false;
    } //-- void deleteCdTipoManutencao() 

    /**
     * Method deleteDsMesAnoCompt
     * 
     */
    public void deleteDsMesAnoCompt()
    {
        this._has_dsMesAnoCompt= false;
    } //-- void deleteDsMesAnoCompt() 

    /**
     * Method deleteNmInscriFavorecido
     * 
     */
    public void deleteNmInscriFavorecido()
    {
        this._has_nmInscriFavorecido= false;
    } //-- void deleteNmInscriFavorecido() 

    /**
     * Method deleteNrDocumento
     * 
     */
    public void deleteNrDocumento()
    {
        this._has_nrDocumento= false;
    } //-- void deleteNrDocumento() 

    /**
     * Method deleteNrLoteArquivoRemessa
     * 
     */
    public void deleteNrLoteArquivoRemessa()
    {
        this._has_nrLoteArquivoRemessa= false;
    } //-- void deleteNrLoteArquivoRemessa() 

    /**
     * Method deleteNrLoteInterno
     * 
     */
    public void deleteNrLoteInterno()
    {
        this._has_nrLoteInterno= false;
    } //-- void deleteNrLoteInterno() 

    /**
     * Method deleteNrSequenciaArquivoRemessa
     * 
     */
    public void deleteNrSequenciaArquivoRemessa()
    {
        this._has_nrSequenciaArquivoRemessa= false;
    } //-- void deleteNrSequenciaArquivoRemessa() 

    /**
     * Method deleteNrSequenciaContratoNegocio
     * 
     */
    public void deleteNrSequenciaContratoNegocio()
    {
        this._has_nrSequenciaContratoNegocio= false;
    } //-- void deleteNrSequenciaContratoNegocio() 

    /**
     * Returns the value of field 'cdAgenciaCredito'.
     * 
     * @return int
     * @return the value of field 'cdAgenciaCredito'.
     */
    public int getCdAgenciaCredito()
    {
        return this._cdAgenciaCredito;
    } //-- int getCdAgenciaCredito() 

    /**
     * Returns the value of field 'cdAgenciaDebito'.
     * 
     * @return int
     * @return the value of field 'cdAgenciaDebito'.
     */
    public int getCdAgenciaDebito()
    {
        return this._cdAgenciaDebito;
    } //-- int getCdAgenciaDebito() 

    /**
     * Returns the value of field 'cdBancoCredito'.
     * 
     * @return int
     * @return the value of field 'cdBancoCredito'.
     */
    public int getCdBancoCredito()
    {
        return this._cdBancoCredito;
    } //-- int getCdBancoCredito() 

    /**
     * Returns the value of field 'cdBancoDebito'.
     * 
     * @return int
     * @return the value of field 'cdBancoDebito'.
     */
    public int getCdBancoDebito()
    {
        return this._cdBancoDebito;
    } //-- int getCdBancoDebito() 

    /**
     * Returns the value of field 'cdContaCredito'.
     * 
     * @return long
     * @return the value of field 'cdContaCredito'.
     */
    public long getCdContaCredito()
    {
        return this._cdContaCredito;
    } //-- long getCdContaCredito() 

    /**
     * Returns the value of field 'cdContaDebito'.
     * 
     * @return long
     * @return the value of field 'cdContaDebito'.
     */
    public long getCdContaDebito()
    {
        return this._cdContaDebito;
    } //-- long getCdContaDebito() 

    /**
     * Returns the value of field 'cdControlePagamento'.
     * 
     * @return String
     * @return the value of field 'cdControlePagamento'.
     */
    public java.lang.String getCdControlePagamento()
    {
        return this._cdControlePagamento;
    } //-- java.lang.String getCdControlePagamento() 

    /**
     * Returns the value of field 'cdCpfCnpjCliente'.
     * 
     * @return long
     * @return the value of field 'cdCpfCnpjCliente'.
     */
    public long getCdCpfCnpjCliente()
    {
        return this._cdCpfCnpjCliente;
    } //-- long getCdCpfCnpjCliente() 

    /**
     * Returns the value of field 'cdDddContribuinte'.
     * 
     * @return int
     * @return the value of field 'cdDddContribuinte'.
     */
    public int getCdDddContribuinte()
    {
        return this._cdDddContribuinte;
    } //-- int getCdDddContribuinte() 

    /**
     * Returns the value of field 'cdDigAgenciaCredito'.
     * 
     * @return String
     * @return the value of field 'cdDigAgenciaCredito'.
     */
    public java.lang.String getCdDigAgenciaCredito()
    {
        return this._cdDigAgenciaCredito;
    } //-- java.lang.String getCdDigAgenciaCredito() 

    /**
     * Returns the value of field 'cdDigAgenciaDebto'.
     * 
     * @return String
     * @return the value of field 'cdDigAgenciaDebto'.
     */
    public java.lang.String getCdDigAgenciaDebto()
    {
        return this._cdDigAgenciaDebto;
    } //-- java.lang.String getCdDigAgenciaDebto() 

    /**
     * Returns the value of field 'cdDigContaCredito'.
     * 
     * @return String
     * @return the value of field 'cdDigContaCredito'.
     */
    public java.lang.String getCdDigContaCredito()
    {
        return this._cdDigContaCredito;
    } //-- java.lang.String getCdDigContaCredito() 

    /**
     * Returns the value of field 'cdFavorecido'.
     * 
     * @return long
     * @return the value of field 'cdFavorecido'.
     */
    public long getCdFavorecido()
    {
        return this._cdFavorecido;
    } //-- long getCdFavorecido() 

    /**
     * Returns the value of field 'cdFluxoInclusao'.
     * 
     * @return String
     * @return the value of field 'cdFluxoInclusao'.
     */
    public java.lang.String getCdFluxoInclusao()
    {
        return this._cdFluxoInclusao;
    } //-- java.lang.String getCdFluxoInclusao() 

    /**
     * Returns the value of field 'cdFluxoManutencao'.
     * 
     * @return String
     * @return the value of field 'cdFluxoManutencao'.
     */
    public java.lang.String getCdFluxoManutencao()
    {
        return this._cdFluxoManutencao;
    } //-- java.lang.String getCdFluxoManutencao() 

    /**
     * Returns the value of field 'cdIndicadorAuto'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorAuto'.
     */
    public int getCdIndicadorAuto()
    {
        return this._cdIndicadorAuto;
    } //-- int getCdIndicadorAuto() 

    /**
     * Returns the value of field 'cdIndicadorEconomicoMoeda'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorEconomicoMoeda'.
     */
    public int getCdIndicadorEconomicoMoeda()
    {
        return this._cdIndicadorEconomicoMoeda;
    } //-- int getCdIndicadorEconomicoMoeda() 

    /**
     * Returns the value of field 'cdIndicadorModalidadePgit'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorModalidadePgit'.
     */
    public int getCdIndicadorModalidadePgit()
    {
        return this._cdIndicadorModalidadePgit;
    } //-- int getCdIndicadorModalidadePgit() 

    /**
     * Returns the value of field 'cdIndicadorSemConsulta'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorSemConsulta'.
     */
    public int getCdIndicadorSemConsulta()
    {
        return this._cdIndicadorSemConsulta;
    } //-- int getCdIndicadorSemConsulta() 

    /**
     * Returns the value of field 'cdInss'.
     * 
     * @return int
     * @return the value of field 'cdInss'.
     */
    public int getCdInss()
    {
        return this._cdInss;
    } //-- int getCdInss() 

    /**
     * Returns the value of field 'cdListaDebito'.
     * 
     * @return long
     * @return the value of field 'cdListaDebito'.
     */
    public long getCdListaDebito()
    {
        return this._cdListaDebito;
    } //-- long getCdListaDebito() 

    /**
     * Returns the value of field 'cdMotivoSituacaoPagamento'.
     * 
     * @return int
     * @return the value of field 'cdMotivoSituacaoPagamento'.
     */
    public int getCdMotivoSituacaoPagamento()
    {
        return this._cdMotivoSituacaoPagamento;
    } //-- int getCdMotivoSituacaoPagamento() 

    /**
     * Returns the value of field 'cdSerieDocumento'.
     * 
     * @return String
     * @return the value of field 'cdSerieDocumento'.
     */
    public java.lang.String getCdSerieDocumento()
    {
        return this._cdSerieDocumento;
    } //-- java.lang.String getCdSerieDocumento() 

    /**
     * Returns the value of field 'cdSituacaoContrato'.
     * 
     * @return String
     * @return the value of field 'cdSituacaoContrato'.
     */
    public java.lang.String getCdSituacaoContrato()
    {
        return this._cdSituacaoContrato;
    } //-- java.lang.String getCdSituacaoContrato() 

    /**
     * Returns the value of field 'cdSituacaoOperacaoPagamento'.
     * 
     * @return int
     * @return the value of field 'cdSituacaoOperacaoPagamento'.
     */
    public int getCdSituacaoOperacaoPagamento()
    {
        return this._cdSituacaoOperacaoPagamento;
    } //-- int getCdSituacaoOperacaoPagamento() 

    /**
     * Returns the value of field 'cdSituacaoPagamento'.
     * 
     * @return String
     * @return the value of field 'cdSituacaoPagamento'.
     */
    public java.lang.String getCdSituacaoPagamento()
    {
        return this._cdSituacaoPagamento;
    } //-- java.lang.String getCdSituacaoPagamento() 

    /**
     * Returns the value of field 'cdTelContribuinte'.
     * 
     * @return String
     * @return the value of field 'cdTelContribuinte'.
     */
    public java.lang.String getCdTelContribuinte()
    {
        return this._cdTelContribuinte;
    } //-- java.lang.String getCdTelContribuinte() 

    /**
     * Returns the value of field 'cdTipoCanalInclusao'.
     * 
     * @return int
     * @return the value of field 'cdTipoCanalInclusao'.
     */
    public int getCdTipoCanalInclusao()
    {
        return this._cdTipoCanalInclusao;
    } //-- int getCdTipoCanalInclusao() 

    /**
     * Returns the value of field 'cdTipoCanalManutencao'.
     * 
     * @return int
     * @return the value of field 'cdTipoCanalManutencao'.
     */
    public int getCdTipoCanalManutencao()
    {
        return this._cdTipoCanalManutencao;
    } //-- int getCdTipoCanalManutencao() 

    /**
     * Returns the value of field 'cdTipoContaFavorecido'.
     * 
     * @return int
     * @return the value of field 'cdTipoContaFavorecido'.
     */
    public int getCdTipoContaFavorecido()
    {
        return this._cdTipoContaFavorecido;
    } //-- int getCdTipoContaFavorecido() 

    /**
     * Returns the value of field 'cdTipoDocumento'.
     * 
     * @return int
     * @return the value of field 'cdTipoDocumento'.
     */
    public int getCdTipoDocumento()
    {
        return this._cdTipoDocumento;
    } //-- int getCdTipoDocumento() 

    /**
     * Returns the value of field 'cdTipoIsncricaoFavorecido'.
     * 
     * @return int
     * @return the value of field 'cdTipoIsncricaoFavorecido'.
     */
    public int getCdTipoIsncricaoFavorecido()
    {
        return this._cdTipoIsncricaoFavorecido;
    } //-- int getCdTipoIsncricaoFavorecido() 

    /**
     * Returns the value of field 'cdTipoManutencao'.
     * 
     * @return int
     * @return the value of field 'cdTipoManutencao'.
     */
    public int getCdTipoManutencao()
    {
        return this._cdTipoManutencao;
    } //-- int getCdTipoManutencao() 

    /**
     * Returns the value of field 'cdUsuarioInclusaoExterno'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioInclusaoExterno'.
     */
    public java.lang.String getCdUsuarioInclusaoExterno()
    {
        return this._cdUsuarioInclusaoExterno;
    } //-- java.lang.String getCdUsuarioInclusaoExterno() 

    /**
     * Returns the value of field 'cdUsuarioInclusaoInterno'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioInclusaoInterno'.
     */
    public java.lang.String getCdUsuarioInclusaoInterno()
    {
        return this._cdUsuarioInclusaoInterno;
    } //-- java.lang.String getCdUsuarioInclusaoInterno() 

    /**
     * Returns the value of field 'cdUsuarioManutencaoExterno'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioManutencaoExterno'.
     */
    public java.lang.String getCdUsuarioManutencaoExterno()
    {
        return this._cdUsuarioManutencaoExterno;
    } //-- java.lang.String getCdUsuarioManutencaoExterno() 

    /**
     * Returns the value of field 'cdUsuarioManutencaoInterno'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioManutencaoInterno'.
     */
    public java.lang.String getCdUsuarioManutencaoInterno()
    {
        return this._cdUsuarioManutencaoInterno;
    } //-- java.lang.String getCdUsuarioManutencaoInterno() 

    /**
     * Returns the value of field 'codMensagem'.
     * 
     * @return String
     * @return the value of field 'codMensagem'.
     */
    public java.lang.String getCodMensagem()
    {
        return this._codMensagem;
    } //-- java.lang.String getCodMensagem() 

    /**
     * Returns the value of field 'dsAgenciaDebito'.
     * 
     * @return String
     * @return the value of field 'dsAgenciaDebito'.
     */
    public java.lang.String getDsAgenciaDebito()
    {
        return this._dsAgenciaDebito;
    } //-- java.lang.String getDsAgenciaDebito() 

    /**
     * Returns the value of field 'dsAgenciaFavorecido'.
     * 
     * @return String
     * @return the value of field 'dsAgenciaFavorecido'.
     */
    public java.lang.String getDsAgenciaFavorecido()
    {
        return this._dsAgenciaFavorecido;
    } //-- java.lang.String getDsAgenciaFavorecido() 

    /**
     * Returns the value of field 'dsBancoDebito'.
     * 
     * @return String
     * @return the value of field 'dsBancoDebito'.
     */
    public java.lang.String getDsBancoDebito()
    {
        return this._dsBancoDebito;
    } //-- java.lang.String getDsBancoDebito() 

    /**
     * Returns the value of field 'dsBancoFavorecido'.
     * 
     * @return String
     * @return the value of field 'dsBancoFavorecido'.
     */
    public java.lang.String getDsBancoFavorecido()
    {
        return this._dsBancoFavorecido;
    } //-- java.lang.String getDsBancoFavorecido() 

    /**
     * Returns the value of field 'dsContrato'.
     * 
     * @return String
     * @return the value of field 'dsContrato'.
     */
    public java.lang.String getDsContrato()
    {
        return this._dsContrato;
    } //-- java.lang.String getDsContrato() 

    /**
     * Returns the value of field 'dsDigAgenciaDebito'.
     * 
     * @return String
     * @return the value of field 'dsDigAgenciaDebito'.
     */
    public java.lang.String getDsDigAgenciaDebito()
    {
        return this._dsDigAgenciaDebito;
    } //-- java.lang.String getDsDigAgenciaDebito() 

    /**
     * Returns the value of field 'dsIndicadorAutorizacao'.
     * 
     * @return String
     * @return the value of field 'dsIndicadorAutorizacao'.
     */
    public java.lang.String getDsIndicadorAutorizacao()
    {
        return this._dsIndicadorAutorizacao;
    } //-- java.lang.String getDsIndicadorAutorizacao() 

    /**
     * Returns the value of field 'dsMensagemPrimeiraLinha'.
     * 
     * @return String
     * @return the value of field 'dsMensagemPrimeiraLinha'.
     */
    public java.lang.String getDsMensagemPrimeiraLinha()
    {
        return this._dsMensagemPrimeiraLinha;
    } //-- java.lang.String getDsMensagemPrimeiraLinha() 

    /**
     * Returns the value of field 'dsMensagemSegundaLinha'.
     * 
     * @return String
     * @return the value of field 'dsMensagemSegundaLinha'.
     */
    public java.lang.String getDsMensagemSegundaLinha()
    {
        return this._dsMensagemSegundaLinha;
    } //-- java.lang.String getDsMensagemSegundaLinha() 

    /**
     * Returns the value of field 'dsMesAnoCompt'.
     * 
     * @return int
     * @return the value of field 'dsMesAnoCompt'.
     */
    public int getDsMesAnoCompt()
    {
        return this._dsMesAnoCompt;
    } //-- int getDsMesAnoCompt() 

    /**
     * Returns the value of field 'dsModalidadeRelacionado'.
     * 
     * @return String
     * @return the value of field 'dsModalidadeRelacionado'.
     */
    public java.lang.String getDsModalidadeRelacionado()
    {
        return this._dsModalidadeRelacionado;
    } //-- java.lang.String getDsModalidadeRelacionado() 

    /**
     * Returns the value of field 'dsMoeda'.
     * 
     * @return String
     * @return the value of field 'dsMoeda'.
     */
    public java.lang.String getDsMoeda()
    {
        return this._dsMoeda;
    } //-- java.lang.String getDsMoeda() 

    /**
     * Returns the value of field 'dsMotivoSituacao'.
     * 
     * @return String
     * @return the value of field 'dsMotivoSituacao'.
     */
    public java.lang.String getDsMotivoSituacao()
    {
        return this._dsMotivoSituacao;
    } //-- java.lang.String getDsMotivoSituacao() 

    /**
     * Returns the value of field 'dsNomeFavorecido'.
     * 
     * @return String
     * @return the value of field 'dsNomeFavorecido'.
     */
    public java.lang.String getDsNomeFavorecido()
    {
        return this._dsNomeFavorecido;
    } //-- java.lang.String getDsNomeFavorecido() 

    /**
     * Returns the value of field 'dsPagamento'.
     * 
     * @return String
     * @return the value of field 'dsPagamento'.
     */
    public java.lang.String getDsPagamento()
    {
        return this._dsPagamento;
    } //-- java.lang.String getDsPagamento() 

    /**
     * Returns the value of field 'dsPessoaJuridicaContrato'.
     * 
     * @return String
     * @return the value of field 'dsPessoaJuridicaContrato'.
     */
    public java.lang.String getDsPessoaJuridicaContrato()
    {
        return this._dsPessoaJuridicaContrato;
    } //-- java.lang.String getDsPessoaJuridicaContrato() 

    /**
     * Returns the value of field 'dsTipoCanalInclusao'.
     * 
     * @return String
     * @return the value of field 'dsTipoCanalInclusao'.
     */
    public java.lang.String getDsTipoCanalInclusao()
    {
        return this._dsTipoCanalInclusao;
    } //-- java.lang.String getDsTipoCanalInclusao() 

    /**
     * Returns the value of field 'dsTipoCanalManutencao'.
     * 
     * @return String
     * @return the value of field 'dsTipoCanalManutencao'.
     */
    public java.lang.String getDsTipoCanalManutencao()
    {
        return this._dsTipoCanalManutencao;
    } //-- java.lang.String getDsTipoCanalManutencao() 

    /**
     * Returns the value of field 'dsTipoContaDebito'.
     * 
     * @return String
     * @return the value of field 'dsTipoContaDebito'.
     */
    public java.lang.String getDsTipoContaDebito()
    {
        return this._dsTipoContaDebito;
    } //-- java.lang.String getDsTipoContaDebito() 

    /**
     * Returns the value of field 'dsTipoContaFavorecido'.
     * 
     * @return String
     * @return the value of field 'dsTipoContaFavorecido'.
     */
    public java.lang.String getDsTipoContaFavorecido()
    {
        return this._dsTipoContaFavorecido;
    } //-- java.lang.String getDsTipoContaFavorecido() 

    /**
     * Returns the value of field 'dsTipoDocumento'.
     * 
     * @return String
     * @return the value of field 'dsTipoDocumento'.
     */
    public java.lang.String getDsTipoDocumento()
    {
        return this._dsTipoDocumento;
    } //-- java.lang.String getDsTipoDocumento() 

    /**
     * Returns the value of field 'dsTipoLayout'.
     * 
     * @return String
     * @return the value of field 'dsTipoLayout'.
     */
    public java.lang.String getDsTipoLayout()
    {
        return this._dsTipoLayout;
    } //-- java.lang.String getDsTipoLayout() 

    /**
     * Returns the value of field 'dsTipoManutencao'.
     * 
     * @return String
     * @return the value of field 'dsTipoManutencao'.
     */
    public java.lang.String getDsTipoManutencao()
    {
        return this._dsTipoManutencao;
    } //-- java.lang.String getDsTipoManutencao() 

    /**
     * Returns the value of field 'dsTipoServicoOperacao'.
     * 
     * @return String
     * @return the value of field 'dsTipoServicoOperacao'.
     */
    public java.lang.String getDsTipoServicoOperacao()
    {
        return this._dsTipoServicoOperacao;
    } //-- java.lang.String getDsTipoServicoOperacao() 

    /**
     * Returns the value of field 'dsUsoEmpresa'.
     * 
     * @return String
     * @return the value of field 'dsUsoEmpresa'.
     */
    public java.lang.String getDsUsoEmpresa()
    {
        return this._dsUsoEmpresa;
    } //-- java.lang.String getDsUsoEmpresa() 

    /**
     * Returns the value of field 'dtAgendamento'.
     * 
     * @return String
     * @return the value of field 'dtAgendamento'.
     */
    public java.lang.String getDtAgendamento()
    {
        return this._dtAgendamento;
    } //-- java.lang.String getDtAgendamento() 

    /**
     * Returns the value of field 'dtDevolucaoEstorno'.
     * 
     * @return String
     * @return the value of field 'dtDevolucaoEstorno'.
     */
    public java.lang.String getDtDevolucaoEstorno()
    {
        return this._dtDevolucaoEstorno;
    } //-- java.lang.String getDtDevolucaoEstorno() 

    /**
     * Returns the value of field 'dtEfetivacaoFloatPgto'.
     * 
     * @return String
     * @return the value of field 'dtEfetivacaoFloatPgto'.
     */
    public java.lang.String getDtEfetivacaoFloatPgto()
    {
        return this._dtEfetivacaoFloatPgto;
    } //-- java.lang.String getDtEfetivacaoFloatPgto() 

    /**
     * Returns the value of field 'dtEmissaoDocumento'.
     * 
     * @return String
     * @return the value of field 'dtEmissaoDocumento'.
     */
    public java.lang.String getDtEmissaoDocumento()
    {
        return this._dtEmissaoDocumento;
    } //-- java.lang.String getDtEmissaoDocumento() 

    /**
     * Returns the value of field 'dtFloatPgto'.
     * 
     * @return String
     * @return the value of field 'dtFloatPgto'.
     */
    public java.lang.String getDtFloatPgto()
    {
        return this._dtFloatPgto;
    } //-- java.lang.String getDtFloatPgto() 

    /**
     * Returns the value of field 'dtInclusao'.
     * 
     * @return String
     * @return the value of field 'dtInclusao'.
     */
    public java.lang.String getDtInclusao()
    {
        return this._dtInclusao;
    } //-- java.lang.String getDtInclusao() 

    /**
     * Returns the value of field 'dtManutencao'.
     * 
     * @return String
     * @return the value of field 'dtManutencao'.
     */
    public java.lang.String getDtManutencao()
    {
        return this._dtManutencao;
    } //-- java.lang.String getDtManutencao() 

    /**
     * Returns the value of field 'dtPagamento'.
     * 
     * @return String
     * @return the value of field 'dtPagamento'.
     */
    public java.lang.String getDtPagamento()
    {
        return this._dtPagamento;
    } //-- java.lang.String getDtPagamento() 

    /**
     * Returns the value of field 'dtVencimento'.
     * 
     * @return String
     * @return the value of field 'dtVencimento'.
     */
    public java.lang.String getDtVencimento()
    {
        return this._dtVencimento;
    } //-- java.lang.String getDtVencimento() 

    /**
     * Returns the value of field 'hrInclusao'.
     * 
     * @return String
     * @return the value of field 'hrInclusao'.
     */
    public java.lang.String getHrInclusao()
    {
        return this._hrInclusao;
    } //-- java.lang.String getHrInclusao() 

    /**
     * Returns the value of field 'hrManutencao'.
     * 
     * @return String
     * @return the value of field 'hrManutencao'.
     */
    public java.lang.String getHrManutencao()
    {
        return this._hrManutencao;
    } //-- java.lang.String getHrManutencao() 

    /**
     * Returns the value of field 'mensagem'.
     * 
     * @return String
     * @return the value of field 'mensagem'.
     */
    public java.lang.String getMensagem()
    {
        return this._mensagem;
    } //-- java.lang.String getMensagem() 

    /**
     * Returns the value of field 'nmInscriFavorecido'.
     * 
     * @return long
     * @return the value of field 'nmInscriFavorecido'.
     */
    public long getNmInscriFavorecido()
    {
        return this._nmInscriFavorecido;
    } //-- long getNmInscriFavorecido() 

    /**
     * Returns the value of field 'nomeCliente'.
     * 
     * @return String
     * @return the value of field 'nomeCliente'.
     */
    public java.lang.String getNomeCliente()
    {
        return this._nomeCliente;
    } //-- java.lang.String getNomeCliente() 

    /**
     * Returns the value of field 'nrDocumento'.
     * 
     * @return long
     * @return the value of field 'nrDocumento'.
     */
    public long getNrDocumento()
    {
        return this._nrDocumento;
    } //-- long getNrDocumento() 

    /**
     * Returns the value of field 'nrLoteArquivoRemessa'.
     * 
     * @return long
     * @return the value of field 'nrLoteArquivoRemessa'.
     */
    public long getNrLoteArquivoRemessa()
    {
        return this._nrLoteArquivoRemessa;
    } //-- long getNrLoteArquivoRemessa() 

    /**
     * Returns the value of field 'nrLoteInterno'.
     * 
     * @return long
     * @return the value of field 'nrLoteInterno'.
     */
    public long getNrLoteInterno()
    {
        return this._nrLoteInterno;
    } //-- long getNrLoteInterno() 

    /**
     * Returns the value of field 'nrSequenciaArquivoRemessa'.
     * 
     * @return long
     * @return the value of field 'nrSequenciaArquivoRemessa'.
     */
    public long getNrSequenciaArquivoRemessa()
    {
        return this._nrSequenciaArquivoRemessa;
    } //-- long getNrSequenciaArquivoRemessa() 

    /**
     * Returns the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @return long
     * @return the value of field 'nrSequenciaContratoNegocio'.
     */
    public long getNrSequenciaContratoNegocio()
    {
        return this._nrSequenciaContratoNegocio;
    } //-- long getNrSequenciaContratoNegocio() 

    /**
     * Returns the value of field 'qtMoeda'.
     * 
     * @return BigDecimal
     * @return the value of field 'qtMoeda'.
     */
    public java.math.BigDecimal getQtMoeda()
    {
        return this._qtMoeda;
    } //-- java.math.BigDecimal getQtMoeda() 

    /**
     * Returns the value of field 'vlAbatimento'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlAbatimento'.
     */
    public java.math.BigDecimal getVlAbatimento()
    {
        return this._vlAbatimento;
    } //-- java.math.BigDecimal getVlAbatimento() 

    /**
     * Returns the value of field 'vlAgendado'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlAgendado'.
     */
    public java.math.BigDecimal getVlAgendado()
    {
        return this._vlAgendado;
    } //-- java.math.BigDecimal getVlAgendado() 

    /**
     * Returns the value of field 'vlDocumento'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlDocumento'.
     */
    public java.math.BigDecimal getVlDocumento()
    {
        return this._vlDocumento;
    } //-- java.math.BigDecimal getVlDocumento() 

    /**
     * Returns the value of field 'vlEfetivo'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlEfetivo'.
     */
    public java.math.BigDecimal getVlEfetivo()
    {
        return this._vlEfetivo;
    } //-- java.math.BigDecimal getVlEfetivo() 

    /**
     * Returns the value of field 'vlFloatingPagamento'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlFloatingPagamento'.
     */
    public java.math.BigDecimal getVlFloatingPagamento()
    {
        return this._vlFloatingPagamento;
    } //-- java.math.BigDecimal getVlFloatingPagamento() 

    /**
     * Returns the value of field 'vlMora'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlMora'.
     */
    public java.math.BigDecimal getVlMora()
    {
        return this._vlMora;
    } //-- java.math.BigDecimal getVlMora() 

    /**
     * Returns the value of field 'vlMulta'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlMulta'.
     */
    public java.math.BigDecimal getVlMulta()
    {
        return this._vlMulta;
    } //-- java.math.BigDecimal getVlMulta() 

    /**
     * Returns the value of field 'vlOutraEntidade'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlOutraEntidade'.
     */
    public java.math.BigDecimal getVlOutraEntidade()
    {
        return this._vlOutraEntidade;
    } //-- java.math.BigDecimal getVlOutraEntidade() 

    /**
     * Returns the value of field 'vlPrincipal'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlPrincipal'.
     */
    public java.math.BigDecimal getVlPrincipal()
    {
        return this._vlPrincipal;
    } //-- java.math.BigDecimal getVlPrincipal() 

    /**
     * Returns the value of field 'vlTotal'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlTotal'.
     */
    public java.math.BigDecimal getVlTotal()
    {
        return this._vlTotal;
    } //-- java.math.BigDecimal getVlTotal() 

    /**
     * Method hasCdAgenciaCredito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgenciaCredito()
    {
        return this._has_cdAgenciaCredito;
    } //-- boolean hasCdAgenciaCredito() 

    /**
     * Method hasCdAgenciaDebito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgenciaDebito()
    {
        return this._has_cdAgenciaDebito;
    } //-- boolean hasCdAgenciaDebito() 

    /**
     * Method hasCdBancoCredito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdBancoCredito()
    {
        return this._has_cdBancoCredito;
    } //-- boolean hasCdBancoCredito() 

    /**
     * Method hasCdBancoDebito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdBancoDebito()
    {
        return this._has_cdBancoDebito;
    } //-- boolean hasCdBancoDebito() 

    /**
     * Method hasCdContaCredito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdContaCredito()
    {
        return this._has_cdContaCredito;
    } //-- boolean hasCdContaCredito() 

    /**
     * Method hasCdContaDebito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdContaDebito()
    {
        return this._has_cdContaDebito;
    } //-- boolean hasCdContaDebito() 

    /**
     * Method hasCdCpfCnpjCliente
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCpfCnpjCliente()
    {
        return this._has_cdCpfCnpjCliente;
    } //-- boolean hasCdCpfCnpjCliente() 

    /**
     * Method hasCdDddContribuinte
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdDddContribuinte()
    {
        return this._has_cdDddContribuinte;
    } //-- boolean hasCdDddContribuinte() 

    /**
     * Method hasCdFavorecido
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFavorecido()
    {
        return this._has_cdFavorecido;
    } //-- boolean hasCdFavorecido() 

    /**
     * Method hasCdIndicadorAuto
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorAuto()
    {
        return this._has_cdIndicadorAuto;
    } //-- boolean hasCdIndicadorAuto() 

    /**
     * Method hasCdIndicadorEconomicoMoeda
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorEconomicoMoeda()
    {
        return this._has_cdIndicadorEconomicoMoeda;
    } //-- boolean hasCdIndicadorEconomicoMoeda() 

    /**
     * Method hasCdIndicadorModalidadePgit
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorModalidadePgit()
    {
        return this._has_cdIndicadorModalidadePgit;
    } //-- boolean hasCdIndicadorModalidadePgit() 

    /**
     * Method hasCdIndicadorSemConsulta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorSemConsulta()
    {
        return this._has_cdIndicadorSemConsulta;
    } //-- boolean hasCdIndicadorSemConsulta() 

    /**
     * Method hasCdInss
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdInss()
    {
        return this._has_cdInss;
    } //-- boolean hasCdInss() 

    /**
     * Method hasCdListaDebito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdListaDebito()
    {
        return this._has_cdListaDebito;
    } //-- boolean hasCdListaDebito() 

    /**
     * Method hasCdMotivoSituacaoPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMotivoSituacaoPagamento()
    {
        return this._has_cdMotivoSituacaoPagamento;
    } //-- boolean hasCdMotivoSituacaoPagamento() 

    /**
     * Method hasCdSituacaoOperacaoPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSituacaoOperacaoPagamento()
    {
        return this._has_cdSituacaoOperacaoPagamento;
    } //-- boolean hasCdSituacaoOperacaoPagamento() 

    /**
     * Method hasCdTipoCanalInclusao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoCanalInclusao()
    {
        return this._has_cdTipoCanalInclusao;
    } //-- boolean hasCdTipoCanalInclusao() 

    /**
     * Method hasCdTipoCanalManutencao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoCanalManutencao()
    {
        return this._has_cdTipoCanalManutencao;
    } //-- boolean hasCdTipoCanalManutencao() 

    /**
     * Method hasCdTipoContaFavorecido
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContaFavorecido()
    {
        return this._has_cdTipoContaFavorecido;
    } //-- boolean hasCdTipoContaFavorecido() 

    /**
     * Method hasCdTipoDocumento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoDocumento()
    {
        return this._has_cdTipoDocumento;
    } //-- boolean hasCdTipoDocumento() 

    /**
     * Method hasCdTipoIsncricaoFavorecido
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoIsncricaoFavorecido()
    {
        return this._has_cdTipoIsncricaoFavorecido;
    } //-- boolean hasCdTipoIsncricaoFavorecido() 

    /**
     * Method hasCdTipoManutencao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoManutencao()
    {
        return this._has_cdTipoManutencao;
    } //-- boolean hasCdTipoManutencao() 

    /**
     * Method hasDsMesAnoCompt
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasDsMesAnoCompt()
    {
        return this._has_dsMesAnoCompt;
    } //-- boolean hasDsMesAnoCompt() 

    /**
     * Method hasNmInscriFavorecido
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNmInscriFavorecido()
    {
        return this._has_nmInscriFavorecido;
    } //-- boolean hasNmInscriFavorecido() 

    /**
     * Method hasNrDocumento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrDocumento()
    {
        return this._has_nrDocumento;
    } //-- boolean hasNrDocumento() 

    /**
     * Method hasNrLoteArquivoRemessa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrLoteArquivoRemessa()
    {
        return this._has_nrLoteArquivoRemessa;
    } //-- boolean hasNrLoteArquivoRemessa() 

    /**
     * Method hasNrLoteInterno
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrLoteInterno()
    {
        return this._has_nrLoteInterno;
    } //-- boolean hasNrLoteInterno() 

    /**
     * Method hasNrSequenciaArquivoRemessa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaArquivoRemessa()
    {
        return this._has_nrSequenciaArquivoRemessa;
    } //-- boolean hasNrSequenciaArquivoRemessa() 

    /**
     * Method hasNrSequenciaContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaContratoNegocio()
    {
        return this._has_nrSequenciaContratoNegocio;
    } //-- boolean hasNrSequenciaContratoNegocio() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdAgenciaCredito'.
     * 
     * @param cdAgenciaCredito the value of field 'cdAgenciaCredito'
     */
    public void setCdAgenciaCredito(int cdAgenciaCredito)
    {
        this._cdAgenciaCredito = cdAgenciaCredito;
        this._has_cdAgenciaCredito = true;
    } //-- void setCdAgenciaCredito(int) 

    /**
     * Sets the value of field 'cdAgenciaDebito'.
     * 
     * @param cdAgenciaDebito the value of field 'cdAgenciaDebito'.
     */
    public void setCdAgenciaDebito(int cdAgenciaDebito)
    {
        this._cdAgenciaDebito = cdAgenciaDebito;
        this._has_cdAgenciaDebito = true;
    } //-- void setCdAgenciaDebito(int) 

    /**
     * Sets the value of field 'cdBancoCredito'.
     * 
     * @param cdBancoCredito the value of field 'cdBancoCredito'.
     */
    public void setCdBancoCredito(int cdBancoCredito)
    {
        this._cdBancoCredito = cdBancoCredito;
        this._has_cdBancoCredito = true;
    } //-- void setCdBancoCredito(int) 

    /**
     * Sets the value of field 'cdBancoDebito'.
     * 
     * @param cdBancoDebito the value of field 'cdBancoDebito'.
     */
    public void setCdBancoDebito(int cdBancoDebito)
    {
        this._cdBancoDebito = cdBancoDebito;
        this._has_cdBancoDebito = true;
    } //-- void setCdBancoDebito(int) 

    /**
     * Sets the value of field 'cdContaCredito'.
     * 
     * @param cdContaCredito the value of field 'cdContaCredito'.
     */
    public void setCdContaCredito(long cdContaCredito)
    {
        this._cdContaCredito = cdContaCredito;
        this._has_cdContaCredito = true;
    } //-- void setCdContaCredito(long) 

    /**
     * Sets the value of field 'cdContaDebito'.
     * 
     * @param cdContaDebito the value of field 'cdContaDebito'.
     */
    public void setCdContaDebito(long cdContaDebito)
    {
        this._cdContaDebito = cdContaDebito;
        this._has_cdContaDebito = true;
    } //-- void setCdContaDebito(long) 

    /**
     * Sets the value of field 'cdControlePagamento'.
     * 
     * @param cdControlePagamento the value of field
     * 'cdControlePagamento'.
     */
    public void setCdControlePagamento(java.lang.String cdControlePagamento)
    {
        this._cdControlePagamento = cdControlePagamento;
    } //-- void setCdControlePagamento(java.lang.String) 

    /**
     * Sets the value of field 'cdCpfCnpjCliente'.
     * 
     * @param cdCpfCnpjCliente the value of field 'cdCpfCnpjCliente'
     */
    public void setCdCpfCnpjCliente(long cdCpfCnpjCliente)
    {
        this._cdCpfCnpjCliente = cdCpfCnpjCliente;
        this._has_cdCpfCnpjCliente = true;
    } //-- void setCdCpfCnpjCliente(long) 

    /**
     * Sets the value of field 'cdDddContribuinte'.
     * 
     * @param cdDddContribuinte the value of field
     * 'cdDddContribuinte'.
     */
    public void setCdDddContribuinte(int cdDddContribuinte)
    {
        this._cdDddContribuinte = cdDddContribuinte;
        this._has_cdDddContribuinte = true;
    } //-- void setCdDddContribuinte(int) 

    /**
     * Sets the value of field 'cdDigAgenciaCredito'.
     * 
     * @param cdDigAgenciaCredito the value of field
     * 'cdDigAgenciaCredito'.
     */
    public void setCdDigAgenciaCredito(java.lang.String cdDigAgenciaCredito)
    {
        this._cdDigAgenciaCredito = cdDigAgenciaCredito;
    } //-- void setCdDigAgenciaCredito(java.lang.String) 

    /**
     * Sets the value of field 'cdDigAgenciaDebto'.
     * 
     * @param cdDigAgenciaDebto the value of field
     * 'cdDigAgenciaDebto'.
     */
    public void setCdDigAgenciaDebto(java.lang.String cdDigAgenciaDebto)
    {
        this._cdDigAgenciaDebto = cdDigAgenciaDebto;
    } //-- void setCdDigAgenciaDebto(java.lang.String) 

    /**
     * Sets the value of field 'cdDigContaCredito'.
     * 
     * @param cdDigContaCredito the value of field
     * 'cdDigContaCredito'.
     */
    public void setCdDigContaCredito(java.lang.String cdDigContaCredito)
    {
        this._cdDigContaCredito = cdDigContaCredito;
    } //-- void setCdDigContaCredito(java.lang.String) 

    /**
     * Sets the value of field 'cdFavorecido'.
     * 
     * @param cdFavorecido the value of field 'cdFavorecido'.
     */
    public void setCdFavorecido(long cdFavorecido)
    {
        this._cdFavorecido = cdFavorecido;
        this._has_cdFavorecido = true;
    } //-- void setCdFavorecido(long) 

    /**
     * Sets the value of field 'cdFluxoInclusao'.
     * 
     * @param cdFluxoInclusao the value of field 'cdFluxoInclusao'.
     */
    public void setCdFluxoInclusao(java.lang.String cdFluxoInclusao)
    {
        this._cdFluxoInclusao = cdFluxoInclusao;
    } //-- void setCdFluxoInclusao(java.lang.String) 

    /**
     * Sets the value of field 'cdFluxoManutencao'.
     * 
     * @param cdFluxoManutencao the value of field
     * 'cdFluxoManutencao'.
     */
    public void setCdFluxoManutencao(java.lang.String cdFluxoManutencao)
    {
        this._cdFluxoManutencao = cdFluxoManutencao;
    } //-- void setCdFluxoManutencao(java.lang.String) 

    /**
     * Sets the value of field 'cdIndicadorAuto'.
     * 
     * @param cdIndicadorAuto the value of field 'cdIndicadorAuto'.
     */
    public void setCdIndicadorAuto(int cdIndicadorAuto)
    {
        this._cdIndicadorAuto = cdIndicadorAuto;
        this._has_cdIndicadorAuto = true;
    } //-- void setCdIndicadorAuto(int) 

    /**
     * Sets the value of field 'cdIndicadorEconomicoMoeda'.
     * 
     * @param cdIndicadorEconomicoMoeda the value of field
     * 'cdIndicadorEconomicoMoeda'.
     */
    public void setCdIndicadorEconomicoMoeda(int cdIndicadorEconomicoMoeda)
    {
        this._cdIndicadorEconomicoMoeda = cdIndicadorEconomicoMoeda;
        this._has_cdIndicadorEconomicoMoeda = true;
    } //-- void setCdIndicadorEconomicoMoeda(int) 

    /**
     * Sets the value of field 'cdIndicadorModalidadePgit'.
     * 
     * @param cdIndicadorModalidadePgit the value of field
     * 'cdIndicadorModalidadePgit'.
     */
    public void setCdIndicadorModalidadePgit(int cdIndicadorModalidadePgit)
    {
        this._cdIndicadorModalidadePgit = cdIndicadorModalidadePgit;
        this._has_cdIndicadorModalidadePgit = true;
    } //-- void setCdIndicadorModalidadePgit(int) 

    /**
     * Sets the value of field 'cdIndicadorSemConsulta'.
     * 
     * @param cdIndicadorSemConsulta the value of field
     * 'cdIndicadorSemConsulta'.
     */
    public void setCdIndicadorSemConsulta(int cdIndicadorSemConsulta)
    {
        this._cdIndicadorSemConsulta = cdIndicadorSemConsulta;
        this._has_cdIndicadorSemConsulta = true;
    } //-- void setCdIndicadorSemConsulta(int) 

    /**
     * Sets the value of field 'cdInss'.
     * 
     * @param cdInss the value of field 'cdInss'.
     */
    public void setCdInss(int cdInss)
    {
        this._cdInss = cdInss;
        this._has_cdInss = true;
    } //-- void setCdInss(int) 

    /**
     * Sets the value of field 'cdListaDebito'.
     * 
     * @param cdListaDebito the value of field 'cdListaDebito'.
     */
    public void setCdListaDebito(long cdListaDebito)
    {
        this._cdListaDebito = cdListaDebito;
        this._has_cdListaDebito = true;
    } //-- void setCdListaDebito(long) 

    /**
     * Sets the value of field 'cdMotivoSituacaoPagamento'.
     * 
     * @param cdMotivoSituacaoPagamento the value of field
     * 'cdMotivoSituacaoPagamento'.
     */
    public void setCdMotivoSituacaoPagamento(int cdMotivoSituacaoPagamento)
    {
        this._cdMotivoSituacaoPagamento = cdMotivoSituacaoPagamento;
        this._has_cdMotivoSituacaoPagamento = true;
    } //-- void setCdMotivoSituacaoPagamento(int) 

    /**
     * Sets the value of field 'cdSerieDocumento'.
     * 
     * @param cdSerieDocumento the value of field 'cdSerieDocumento'
     */
    public void setCdSerieDocumento(java.lang.String cdSerieDocumento)
    {
        this._cdSerieDocumento = cdSerieDocumento;
    } //-- void setCdSerieDocumento(java.lang.String) 

    /**
     * Sets the value of field 'cdSituacaoContrato'.
     * 
     * @param cdSituacaoContrato the value of field
     * 'cdSituacaoContrato'.
     */
    public void setCdSituacaoContrato(java.lang.String cdSituacaoContrato)
    {
        this._cdSituacaoContrato = cdSituacaoContrato;
    } //-- void setCdSituacaoContrato(java.lang.String) 

    /**
     * Sets the value of field 'cdSituacaoOperacaoPagamento'.
     * 
     * @param cdSituacaoOperacaoPagamento the value of field
     * 'cdSituacaoOperacaoPagamento'.
     */
    public void setCdSituacaoOperacaoPagamento(int cdSituacaoOperacaoPagamento)
    {
        this._cdSituacaoOperacaoPagamento = cdSituacaoOperacaoPagamento;
        this._has_cdSituacaoOperacaoPagamento = true;
    } //-- void setCdSituacaoOperacaoPagamento(int) 

    /**
     * Sets the value of field 'cdSituacaoPagamento'.
     * 
     * @param cdSituacaoPagamento the value of field
     * 'cdSituacaoPagamento'.
     */
    public void setCdSituacaoPagamento(java.lang.String cdSituacaoPagamento)
    {
        this._cdSituacaoPagamento = cdSituacaoPagamento;
    } //-- void setCdSituacaoPagamento(java.lang.String) 

    /**
     * Sets the value of field 'cdTelContribuinte'.
     * 
     * @param cdTelContribuinte the value of field
     * 'cdTelContribuinte'.
     */
    public void setCdTelContribuinte(java.lang.String cdTelContribuinte)
    {
        this._cdTelContribuinte = cdTelContribuinte;
    } //-- void setCdTelContribuinte(java.lang.String) 

    /**
     * Sets the value of field 'cdTipoCanalInclusao'.
     * 
     * @param cdTipoCanalInclusao the value of field
     * 'cdTipoCanalInclusao'.
     */
    public void setCdTipoCanalInclusao(int cdTipoCanalInclusao)
    {
        this._cdTipoCanalInclusao = cdTipoCanalInclusao;
        this._has_cdTipoCanalInclusao = true;
    } //-- void setCdTipoCanalInclusao(int) 

    /**
     * Sets the value of field 'cdTipoCanalManutencao'.
     * 
     * @param cdTipoCanalManutencao the value of field
     * 'cdTipoCanalManutencao'.
     */
    public void setCdTipoCanalManutencao(int cdTipoCanalManutencao)
    {
        this._cdTipoCanalManutencao = cdTipoCanalManutencao;
        this._has_cdTipoCanalManutencao = true;
    } //-- void setCdTipoCanalManutencao(int) 

    /**
     * Sets the value of field 'cdTipoContaFavorecido'.
     * 
     * @param cdTipoContaFavorecido the value of field
     * 'cdTipoContaFavorecido'.
     */
    public void setCdTipoContaFavorecido(int cdTipoContaFavorecido)
    {
        this._cdTipoContaFavorecido = cdTipoContaFavorecido;
        this._has_cdTipoContaFavorecido = true;
    } //-- void setCdTipoContaFavorecido(int) 

    /**
     * Sets the value of field 'cdTipoDocumento'.
     * 
     * @param cdTipoDocumento the value of field 'cdTipoDocumento'.
     */
    public void setCdTipoDocumento(int cdTipoDocumento)
    {
        this._cdTipoDocumento = cdTipoDocumento;
        this._has_cdTipoDocumento = true;
    } //-- void setCdTipoDocumento(int) 

    /**
     * Sets the value of field 'cdTipoIsncricaoFavorecido'.
     * 
     * @param cdTipoIsncricaoFavorecido the value of field
     * 'cdTipoIsncricaoFavorecido'.
     */
    public void setCdTipoIsncricaoFavorecido(int cdTipoIsncricaoFavorecido)
    {
        this._cdTipoIsncricaoFavorecido = cdTipoIsncricaoFavorecido;
        this._has_cdTipoIsncricaoFavorecido = true;
    } //-- void setCdTipoIsncricaoFavorecido(int) 

    /**
     * Sets the value of field 'cdTipoManutencao'.
     * 
     * @param cdTipoManutencao the value of field 'cdTipoManutencao'
     */
    public void setCdTipoManutencao(int cdTipoManutencao)
    {
        this._cdTipoManutencao = cdTipoManutencao;
        this._has_cdTipoManutencao = true;
    } //-- void setCdTipoManutencao(int) 

    /**
     * Sets the value of field 'cdUsuarioInclusaoExterno'.
     * 
     * @param cdUsuarioInclusaoExterno the value of field
     * 'cdUsuarioInclusaoExterno'.
     */
    public void setCdUsuarioInclusaoExterno(java.lang.String cdUsuarioInclusaoExterno)
    {
        this._cdUsuarioInclusaoExterno = cdUsuarioInclusaoExterno;
    } //-- void setCdUsuarioInclusaoExterno(java.lang.String) 

    /**
     * Sets the value of field 'cdUsuarioInclusaoInterno'.
     * 
     * @param cdUsuarioInclusaoInterno the value of field
     * 'cdUsuarioInclusaoInterno'.
     */
    public void setCdUsuarioInclusaoInterno(java.lang.String cdUsuarioInclusaoInterno)
    {
        this._cdUsuarioInclusaoInterno = cdUsuarioInclusaoInterno;
    } //-- void setCdUsuarioInclusaoInterno(java.lang.String) 

    /**
     * Sets the value of field 'cdUsuarioManutencaoExterno'.
     * 
     * @param cdUsuarioManutencaoExterno the value of field
     * 'cdUsuarioManutencaoExterno'.
     */
    public void setCdUsuarioManutencaoExterno(java.lang.String cdUsuarioManutencaoExterno)
    {
        this._cdUsuarioManutencaoExterno = cdUsuarioManutencaoExterno;
    } //-- void setCdUsuarioManutencaoExterno(java.lang.String) 

    /**
     * Sets the value of field 'cdUsuarioManutencaoInterno'.
     * 
     * @param cdUsuarioManutencaoInterno the value of field
     * 'cdUsuarioManutencaoInterno'.
     */
    public void setCdUsuarioManutencaoInterno(java.lang.String cdUsuarioManutencaoInterno)
    {
        this._cdUsuarioManutencaoInterno = cdUsuarioManutencaoInterno;
    } //-- void setCdUsuarioManutencaoInterno(java.lang.String) 

    /**
     * Sets the value of field 'codMensagem'.
     * 
     * @param codMensagem the value of field 'codMensagem'.
     */
    public void setCodMensagem(java.lang.String codMensagem)
    {
        this._codMensagem = codMensagem;
    } //-- void setCodMensagem(java.lang.String) 

    /**
     * Sets the value of field 'dsAgenciaDebito'.
     * 
     * @param dsAgenciaDebito the value of field 'dsAgenciaDebito'.
     */
    public void setDsAgenciaDebito(java.lang.String dsAgenciaDebito)
    {
        this._dsAgenciaDebito = dsAgenciaDebito;
    } //-- void setDsAgenciaDebito(java.lang.String) 

    /**
     * Sets the value of field 'dsAgenciaFavorecido'.
     * 
     * @param dsAgenciaFavorecido the value of field
     * 'dsAgenciaFavorecido'.
     */
    public void setDsAgenciaFavorecido(java.lang.String dsAgenciaFavorecido)
    {
        this._dsAgenciaFavorecido = dsAgenciaFavorecido;
    } //-- void setDsAgenciaFavorecido(java.lang.String) 

    /**
     * Sets the value of field 'dsBancoDebito'.
     * 
     * @param dsBancoDebito the value of field 'dsBancoDebito'.
     */
    public void setDsBancoDebito(java.lang.String dsBancoDebito)
    {
        this._dsBancoDebito = dsBancoDebito;
    } //-- void setDsBancoDebito(java.lang.String) 

    /**
     * Sets the value of field 'dsBancoFavorecido'.
     * 
     * @param dsBancoFavorecido the value of field
     * 'dsBancoFavorecido'.
     */
    public void setDsBancoFavorecido(java.lang.String dsBancoFavorecido)
    {
        this._dsBancoFavorecido = dsBancoFavorecido;
    } //-- void setDsBancoFavorecido(java.lang.String) 

    /**
     * Sets the value of field 'dsContrato'.
     * 
     * @param dsContrato the value of field 'dsContrato'.
     */
    public void setDsContrato(java.lang.String dsContrato)
    {
        this._dsContrato = dsContrato;
    } //-- void setDsContrato(java.lang.String) 

    /**
     * Sets the value of field 'dsDigAgenciaDebito'.
     * 
     * @param dsDigAgenciaDebito the value of field
     * 'dsDigAgenciaDebito'.
     */
    public void setDsDigAgenciaDebito(java.lang.String dsDigAgenciaDebito)
    {
        this._dsDigAgenciaDebito = dsDigAgenciaDebito;
    } //-- void setDsDigAgenciaDebito(java.lang.String) 

    /**
     * Sets the value of field 'dsIndicadorAutorizacao'.
     * 
     * @param dsIndicadorAutorizacao the value of field
     * 'dsIndicadorAutorizacao'.
     */
    public void setDsIndicadorAutorizacao(java.lang.String dsIndicadorAutorizacao)
    {
        this._dsIndicadorAutorizacao = dsIndicadorAutorizacao;
    } //-- void setDsIndicadorAutorizacao(java.lang.String) 

    /**
     * Sets the value of field 'dsMensagemPrimeiraLinha'.
     * 
     * @param dsMensagemPrimeiraLinha the value of field
     * 'dsMensagemPrimeiraLinha'.
     */
    public void setDsMensagemPrimeiraLinha(java.lang.String dsMensagemPrimeiraLinha)
    {
        this._dsMensagemPrimeiraLinha = dsMensagemPrimeiraLinha;
    } //-- void setDsMensagemPrimeiraLinha(java.lang.String) 

    /**
     * Sets the value of field 'dsMensagemSegundaLinha'.
     * 
     * @param dsMensagemSegundaLinha the value of field
     * 'dsMensagemSegundaLinha'.
     */
    public void setDsMensagemSegundaLinha(java.lang.String dsMensagemSegundaLinha)
    {
        this._dsMensagemSegundaLinha = dsMensagemSegundaLinha;
    } //-- void setDsMensagemSegundaLinha(java.lang.String) 

    /**
     * Sets the value of field 'dsMesAnoCompt'.
     * 
     * @param dsMesAnoCompt the value of field 'dsMesAnoCompt'.
     */
    public void setDsMesAnoCompt(int dsMesAnoCompt)
    {
        this._dsMesAnoCompt = dsMesAnoCompt;
        this._has_dsMesAnoCompt = true;
    } //-- void setDsMesAnoCompt(int) 

    /**
     * Sets the value of field 'dsModalidadeRelacionado'.
     * 
     * @param dsModalidadeRelacionado the value of field
     * 'dsModalidadeRelacionado'.
     */
    public void setDsModalidadeRelacionado(java.lang.String dsModalidadeRelacionado)
    {
        this._dsModalidadeRelacionado = dsModalidadeRelacionado;
    } //-- void setDsModalidadeRelacionado(java.lang.String) 

    /**
     * Sets the value of field 'dsMoeda'.
     * 
     * @param dsMoeda the value of field 'dsMoeda'.
     */
    public void setDsMoeda(java.lang.String dsMoeda)
    {
        this._dsMoeda = dsMoeda;
    } //-- void setDsMoeda(java.lang.String) 

    /**
     * Sets the value of field 'dsMotivoSituacao'.
     * 
     * @param dsMotivoSituacao the value of field 'dsMotivoSituacao'
     */
    public void setDsMotivoSituacao(java.lang.String dsMotivoSituacao)
    {
        this._dsMotivoSituacao = dsMotivoSituacao;
    } //-- void setDsMotivoSituacao(java.lang.String) 

    /**
     * Sets the value of field 'dsNomeFavorecido'.
     * 
     * @param dsNomeFavorecido the value of field 'dsNomeFavorecido'
     */
    public void setDsNomeFavorecido(java.lang.String dsNomeFavorecido)
    {
        this._dsNomeFavorecido = dsNomeFavorecido;
    } //-- void setDsNomeFavorecido(java.lang.String) 

    /**
     * Sets the value of field 'dsPagamento'.
     * 
     * @param dsPagamento the value of field 'dsPagamento'.
     */
    public void setDsPagamento(java.lang.String dsPagamento)
    {
        this._dsPagamento = dsPagamento;
    } //-- void setDsPagamento(java.lang.String) 

    /**
     * Sets the value of field 'dsPessoaJuridicaContrato'.
     * 
     * @param dsPessoaJuridicaContrato the value of field
     * 'dsPessoaJuridicaContrato'.
     */
    public void setDsPessoaJuridicaContrato(java.lang.String dsPessoaJuridicaContrato)
    {
        this._dsPessoaJuridicaContrato = dsPessoaJuridicaContrato;
    } //-- void setDsPessoaJuridicaContrato(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoCanalInclusao'.
     * 
     * @param dsTipoCanalInclusao the value of field
     * 'dsTipoCanalInclusao'.
     */
    public void setDsTipoCanalInclusao(java.lang.String dsTipoCanalInclusao)
    {
        this._dsTipoCanalInclusao = dsTipoCanalInclusao;
    } //-- void setDsTipoCanalInclusao(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoCanalManutencao'.
     * 
     * @param dsTipoCanalManutencao the value of field
     * 'dsTipoCanalManutencao'.
     */
    public void setDsTipoCanalManutencao(java.lang.String dsTipoCanalManutencao)
    {
        this._dsTipoCanalManutencao = dsTipoCanalManutencao;
    } //-- void setDsTipoCanalManutencao(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoContaDebito'.
     * 
     * @param dsTipoContaDebito the value of field
     * 'dsTipoContaDebito'.
     */
    public void setDsTipoContaDebito(java.lang.String dsTipoContaDebito)
    {
        this._dsTipoContaDebito = dsTipoContaDebito;
    } //-- void setDsTipoContaDebito(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoContaFavorecido'.
     * 
     * @param dsTipoContaFavorecido the value of field
     * 'dsTipoContaFavorecido'.
     */
    public void setDsTipoContaFavorecido(java.lang.String dsTipoContaFavorecido)
    {
        this._dsTipoContaFavorecido = dsTipoContaFavorecido;
    } //-- void setDsTipoContaFavorecido(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoDocumento'.
     * 
     * @param dsTipoDocumento the value of field 'dsTipoDocumento'.
     */
    public void setDsTipoDocumento(java.lang.String dsTipoDocumento)
    {
        this._dsTipoDocumento = dsTipoDocumento;
    } //-- void setDsTipoDocumento(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoLayout'.
     * 
     * @param dsTipoLayout the value of field 'dsTipoLayout'.
     */
    public void setDsTipoLayout(java.lang.String dsTipoLayout)
    {
        this._dsTipoLayout = dsTipoLayout;
    } //-- void setDsTipoLayout(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoManutencao'.
     * 
     * @param dsTipoManutencao the value of field 'dsTipoManutencao'
     */
    public void setDsTipoManutencao(java.lang.String dsTipoManutencao)
    {
        this._dsTipoManutencao = dsTipoManutencao;
    } //-- void setDsTipoManutencao(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoServicoOperacao'.
     * 
     * @param dsTipoServicoOperacao the value of field
     * 'dsTipoServicoOperacao'.
     */
    public void setDsTipoServicoOperacao(java.lang.String dsTipoServicoOperacao)
    {
        this._dsTipoServicoOperacao = dsTipoServicoOperacao;
    } //-- void setDsTipoServicoOperacao(java.lang.String) 

    /**
     * Sets the value of field 'dsUsoEmpresa'.
     * 
     * @param dsUsoEmpresa the value of field 'dsUsoEmpresa'.
     */
    public void setDsUsoEmpresa(java.lang.String dsUsoEmpresa)
    {
        this._dsUsoEmpresa = dsUsoEmpresa;
    } //-- void setDsUsoEmpresa(java.lang.String) 

    /**
     * Sets the value of field 'dtAgendamento'.
     * 
     * @param dtAgendamento the value of field 'dtAgendamento'.
     */
    public void setDtAgendamento(java.lang.String dtAgendamento)
    {
        this._dtAgendamento = dtAgendamento;
    } //-- void setDtAgendamento(java.lang.String) 

    /**
     * Sets the value of field 'dtDevolucaoEstorno'.
     * 
     * @param dtDevolucaoEstorno the value of field
     * 'dtDevolucaoEstorno'.
     */
    public void setDtDevolucaoEstorno(java.lang.String dtDevolucaoEstorno)
    {
        this._dtDevolucaoEstorno = dtDevolucaoEstorno;
    } //-- void setDtDevolucaoEstorno(java.lang.String) 

    /**
     * Sets the value of field 'dtEfetivacaoFloatPgto'.
     * 
     * @param dtEfetivacaoFloatPgto the value of field
     * 'dtEfetivacaoFloatPgto'.
     */
    public void setDtEfetivacaoFloatPgto(java.lang.String dtEfetivacaoFloatPgto)
    {
        this._dtEfetivacaoFloatPgto = dtEfetivacaoFloatPgto;
    } //-- void setDtEfetivacaoFloatPgto(java.lang.String) 

    /**
     * Sets the value of field 'dtEmissaoDocumento'.
     * 
     * @param dtEmissaoDocumento the value of field
     * 'dtEmissaoDocumento'.
     */
    public void setDtEmissaoDocumento(java.lang.String dtEmissaoDocumento)
    {
        this._dtEmissaoDocumento = dtEmissaoDocumento;
    } //-- void setDtEmissaoDocumento(java.lang.String) 

    /**
     * Sets the value of field 'dtFloatPgto'.
     * 
     * @param dtFloatPgto the value of field 'dtFloatPgto'.
     */
    public void setDtFloatPgto(java.lang.String dtFloatPgto)
    {
        this._dtFloatPgto = dtFloatPgto;
    } //-- void setDtFloatPgto(java.lang.String) 

    /**
     * Sets the value of field 'dtInclusao'.
     * 
     * @param dtInclusao the value of field 'dtInclusao'.
     */
    public void setDtInclusao(java.lang.String dtInclusao)
    {
        this._dtInclusao = dtInclusao;
    } //-- void setDtInclusao(java.lang.String) 

    /**
     * Sets the value of field 'dtManutencao'.
     * 
     * @param dtManutencao the value of field 'dtManutencao'.
     */
    public void setDtManutencao(java.lang.String dtManutencao)
    {
        this._dtManutencao = dtManutencao;
    } //-- void setDtManutencao(java.lang.String) 

    /**
     * Sets the value of field 'dtPagamento'.
     * 
     * @param dtPagamento the value of field 'dtPagamento'.
     */
    public void setDtPagamento(java.lang.String dtPagamento)
    {
        this._dtPagamento = dtPagamento;
    } //-- void setDtPagamento(java.lang.String) 

    /**
     * Sets the value of field 'dtVencimento'.
     * 
     * @param dtVencimento the value of field 'dtVencimento'.
     */
    public void setDtVencimento(java.lang.String dtVencimento)
    {
        this._dtVencimento = dtVencimento;
    } //-- void setDtVencimento(java.lang.String) 

    /**
     * Sets the value of field 'hrInclusao'.
     * 
     * @param hrInclusao the value of field 'hrInclusao'.
     */
    public void setHrInclusao(java.lang.String hrInclusao)
    {
        this._hrInclusao = hrInclusao;
    } //-- void setHrInclusao(java.lang.String) 

    /**
     * Sets the value of field 'hrManutencao'.
     * 
     * @param hrManutencao the value of field 'hrManutencao'.
     */
    public void setHrManutencao(java.lang.String hrManutencao)
    {
        this._hrManutencao = hrManutencao;
    } //-- void setHrManutencao(java.lang.String) 

    /**
     * Sets the value of field 'mensagem'.
     * 
     * @param mensagem the value of field 'mensagem'.
     */
    public void setMensagem(java.lang.String mensagem)
    {
        this._mensagem = mensagem;
    } //-- void setMensagem(java.lang.String) 

    /**
     * Sets the value of field 'nmInscriFavorecido'.
     * 
     * @param nmInscriFavorecido the value of field
     * 'nmInscriFavorecido'.
     */
    public void setNmInscriFavorecido(long nmInscriFavorecido)
    {
        this._nmInscriFavorecido = nmInscriFavorecido;
        this._has_nmInscriFavorecido = true;
    } //-- void setNmInscriFavorecido(long) 

    /**
     * Sets the value of field 'nomeCliente'.
     * 
     * @param nomeCliente the value of field 'nomeCliente'.
     */
    public void setNomeCliente(java.lang.String nomeCliente)
    {
        this._nomeCliente = nomeCliente;
    } //-- void setNomeCliente(java.lang.String) 

    /**
     * Sets the value of field 'nrDocumento'.
     * 
     * @param nrDocumento the value of field 'nrDocumento'.
     */
    public void setNrDocumento(long nrDocumento)
    {
        this._nrDocumento = nrDocumento;
        this._has_nrDocumento = true;
    } //-- void setNrDocumento(long) 

    /**
     * Sets the value of field 'nrLoteArquivoRemessa'.
     * 
     * @param nrLoteArquivoRemessa the value of field
     * 'nrLoteArquivoRemessa'.
     */
    public void setNrLoteArquivoRemessa(long nrLoteArquivoRemessa)
    {
        this._nrLoteArquivoRemessa = nrLoteArquivoRemessa;
        this._has_nrLoteArquivoRemessa = true;
    } //-- void setNrLoteArquivoRemessa(long) 

    /**
     * Sets the value of field 'nrLoteInterno'.
     * 
     * @param nrLoteInterno the value of field 'nrLoteInterno'.
     */
    public void setNrLoteInterno(long nrLoteInterno)
    {
        this._nrLoteInterno = nrLoteInterno;
        this._has_nrLoteInterno = true;
    } //-- void setNrLoteInterno(long) 

    /**
     * Sets the value of field 'nrSequenciaArquivoRemessa'.
     * 
     * @param nrSequenciaArquivoRemessa the value of field
     * 'nrSequenciaArquivoRemessa'.
     */
    public void setNrSequenciaArquivoRemessa(long nrSequenciaArquivoRemessa)
    {
        this._nrSequenciaArquivoRemessa = nrSequenciaArquivoRemessa;
        this._has_nrSequenciaArquivoRemessa = true;
    } //-- void setNrSequenciaArquivoRemessa(long) 

    /**
     * Sets the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @param nrSequenciaContratoNegocio the value of field
     * 'nrSequenciaContratoNegocio'.
     */
    public void setNrSequenciaContratoNegocio(long nrSequenciaContratoNegocio)
    {
        this._nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
        this._has_nrSequenciaContratoNegocio = true;
    } //-- void setNrSequenciaContratoNegocio(long) 

    /**
     * Sets the value of field 'qtMoeda'.
     * 
     * @param qtMoeda the value of field 'qtMoeda'.
     */
    public void setQtMoeda(java.math.BigDecimal qtMoeda)
    {
        this._qtMoeda = qtMoeda;
    } //-- void setQtMoeda(java.math.BigDecimal) 

    /**
     * Sets the value of field 'vlAbatimento'.
     * 
     * @param vlAbatimento the value of field 'vlAbatimento'.
     */
    public void setVlAbatimento(java.math.BigDecimal vlAbatimento)
    {
        this._vlAbatimento = vlAbatimento;
    } //-- void setVlAbatimento(java.math.BigDecimal) 

    /**
     * Sets the value of field 'vlAgendado'.
     * 
     * @param vlAgendado the value of field 'vlAgendado'.
     */
    public void setVlAgendado(java.math.BigDecimal vlAgendado)
    {
        this._vlAgendado = vlAgendado;
    } //-- void setVlAgendado(java.math.BigDecimal) 

    /**
     * Sets the value of field 'vlDocumento'.
     * 
     * @param vlDocumento the value of field 'vlDocumento'.
     */
    public void setVlDocumento(java.math.BigDecimal vlDocumento)
    {
        this._vlDocumento = vlDocumento;
    } //-- void setVlDocumento(java.math.BigDecimal) 

    /**
     * Sets the value of field 'vlEfetivo'.
     * 
     * @param vlEfetivo the value of field 'vlEfetivo'.
     */
    public void setVlEfetivo(java.math.BigDecimal vlEfetivo)
    {
        this._vlEfetivo = vlEfetivo;
    } //-- void setVlEfetivo(java.math.BigDecimal) 

    /**
     * Sets the value of field 'vlFloatingPagamento'.
     * 
     * @param vlFloatingPagamento the value of field
     * 'vlFloatingPagamento'.
     */
    public void setVlFloatingPagamento(java.math.BigDecimal vlFloatingPagamento)
    {
        this._vlFloatingPagamento = vlFloatingPagamento;
    } //-- void setVlFloatingPagamento(java.math.BigDecimal) 

    /**
     * Sets the value of field 'vlMora'.
     * 
     * @param vlMora the value of field 'vlMora'.
     */
    public void setVlMora(java.math.BigDecimal vlMora)
    {
        this._vlMora = vlMora;
    } //-- void setVlMora(java.math.BigDecimal) 

    /**
     * Sets the value of field 'vlMulta'.
     * 
     * @param vlMulta the value of field 'vlMulta'.
     */
    public void setVlMulta(java.math.BigDecimal vlMulta)
    {
        this._vlMulta = vlMulta;
    } //-- void setVlMulta(java.math.BigDecimal) 

    /**
     * Sets the value of field 'vlOutraEntidade'.
     * 
     * @param vlOutraEntidade the value of field 'vlOutraEntidade'.
     */
    public void setVlOutraEntidade(java.math.BigDecimal vlOutraEntidade)
    {
        this._vlOutraEntidade = vlOutraEntidade;
    } //-- void setVlOutraEntidade(java.math.BigDecimal) 

    /**
     * Sets the value of field 'vlPrincipal'.
     * 
     * @param vlPrincipal the value of field 'vlPrincipal'.
     */
    public void setVlPrincipal(java.math.BigDecimal vlPrincipal)
    {
        this._vlPrincipal = vlPrincipal;
    } //-- void setVlPrincipal(java.math.BigDecimal) 

    /**
     * Sets the value of field 'vlTotal'.
     * 
     * @param vlTotal the value of field 'vlTotal'.
     */
    public void setVlTotal(java.math.BigDecimal vlTotal)
    {
        this._vlTotal = vlTotal;
    } //-- void setVlTotal(java.math.BigDecimal) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return DetalharManPagtoGPSResponse
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.detalharmanpagtogps.response.DetalharManPagtoGPSResponse unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.detalharmanpagtogps.response.DetalharManPagtoGPSResponse) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.detalharmanpagtogps.response.DetalharManPagtoGPSResponse.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharmanpagtogps.response.DetalharManPagtoGPSResponse unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
