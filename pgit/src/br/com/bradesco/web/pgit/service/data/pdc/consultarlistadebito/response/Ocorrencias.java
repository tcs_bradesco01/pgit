/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultarlistadebito.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.math.BigDecimal;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _numeroCpfCnpj
     */
    private java.lang.String _numeroCpfCnpj;

    /**
     * Field _dsPessoaEmpresa
     */
    private java.lang.String _dsPessoaEmpresa;

    /**
     * Field _cdPessoaJuridicaContrato
     */
    private long _cdPessoaJuridicaContrato = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridicaContrato
     */
    private boolean _has_cdPessoaJuridicaContrato;

    /**
     * Field _dsEmpresa
     */
    private java.lang.String _dsEmpresa;

    /**
     * Field _cdTipoContratoNegocio
     */
    private int _cdTipoContratoNegocio = 0;

    /**
     * keeps track of state for field: _cdTipoContratoNegocio
     */
    private boolean _has_cdTipoContratoNegocio;

    /**
     * Field _nrSequenciaContratoNegocio
     */
    private long _nrSequenciaContratoNegocio = 0;

    /**
     * keeps track of state for field: _nrSequenciaContratoNegocio
     */
    private boolean _has_nrSequenciaContratoNegocio;

    /**
     * Field _dsTipoContratoNegocio
     */
    private java.lang.String _dsTipoContratoNegocio;

    /**
     * Field _cdListaDebitoPagamento
     */
    private long _cdListaDebitoPagamento = 0;

    /**
     * keeps track of state for field: _cdListaDebitoPagamento
     */
    private boolean _has_cdListaDebitoPagamento;

    /**
     * Field _cdSituacaoListaDebito
     */
    private int _cdSituacaoListaDebito = 0;

    /**
     * keeps track of state for field: _cdSituacaoListaDebito
     */
    private boolean _has_cdSituacaoListaDebito;

    /**
     * Field _dsSituacaoListaDebito
     */
    private java.lang.String _dsSituacaoListaDebito;

    /**
     * Field _dtPrevistaPagamento
     */
    private java.lang.String _dtPrevistaPagamento;

    /**
     * Field _cdProdutoServicoOperacao
     */
    private int _cdProdutoServicoOperacao = 0;

    /**
     * keeps track of state for field: _cdProdutoServicoOperacao
     */
    private boolean _has_cdProdutoServicoOperacao;

    /**
     * Field _dsResumoProdutoServico
     */
    private java.lang.String _dsResumoProdutoServico;

    /**
     * Field _cdProdutoServicoRelacionado
     */
    private int _cdProdutoServicoRelacionado = 0;

    /**
     * keeps track of state for field: _cdProdutoServicoRelacionado
     */
    private boolean _has_cdProdutoServicoRelacionado;

    /**
     * Field _dsProdutoServicoRelacionado
     */
    private java.lang.String _dsProdutoServicoRelacionado;

    /**
     * Field _cdServicoCompostoPagamento
     */
    private long _cdServicoCompostoPagamento = 0;

    /**
     * keeps track of state for field: _cdServicoCompostoPagamento
     */
    private boolean _has_cdServicoCompostoPagamento;

    /**
     * Field _cdBanco
     */
    private int _cdBanco = 0;

    /**
     * keeps track of state for field: _cdBanco
     */
    private boolean _has_cdBanco;

    /**
     * Field _dsBanco
     */
    private java.lang.String _dsBanco;

    /**
     * Field _cdAgencia
     */
    private int _cdAgencia = 0;

    /**
     * keeps track of state for field: _cdAgencia
     */
    private boolean _has_cdAgencia;

    /**
     * Field _cdDigitoAgencia
     */
    private int _cdDigitoAgencia = 0;

    /**
     * keeps track of state for field: _cdDigitoAgencia
     */
    private boolean _has_cdDigitoAgencia;

    /**
     * Field _dsAgencia
     */
    private java.lang.String _dsAgencia;

    /**
     * Field _cdConta
     */
    private long _cdConta = 0;

    /**
     * keeps track of state for field: _cdConta
     */
    private boolean _has_cdConta;

    /**
     * Field _cdDigitoConta
     */
    private java.lang.String _cdDigitoConta;

    /**
     * Field _cdTipoConta
     */
    private int _cdTipoConta = 0;

    /**
     * keeps track of state for field: _cdTipoConta
     */
    private boolean _has_cdTipoConta;

    /**
     * Field _dsTipoConta
     */
    private java.lang.String _dsTipoConta;

    /**
     * Field _qtPagamentoAutorizado
     */
    private long _qtPagamentoAutorizado = 0;

    /**
     * keeps track of state for field: _qtPagamentoAutorizado
     */
    private boolean _has_qtPagamentoAutorizado;

    /**
     * Field _vlPagamentoAutorizado
     */
    private java.math.BigDecimal _vlPagamentoAutorizado = new java.math.BigDecimal("0");

    /**
     * Field _qtPagamentoDesautorizado
     */
    private long _qtPagamentoDesautorizado = 0;

    /**
     * keeps track of state for field: _qtPagamentoDesautorizado
     */
    private boolean _has_qtPagamentoDesautorizado;

    /**
     * Field _vlPagamentoDesautorizado
     */
    private java.math.BigDecimal _vlPagamentoDesautorizado = new java.math.BigDecimal("0");

    /**
     * Field _qtPagamendoEfetivado
     */
    private long _qtPagamendoEfetivado = 0;

    /**
     * keeps track of state for field: _qtPagamendoEfetivado
     */
    private boolean _has_qtPagamendoEfetivado;

    /**
     * Field _vlPagamentoEfetivado
     */
    private java.math.BigDecimal _vlPagamentoEfetivado = new java.math.BigDecimal("0");

    /**
     * Field _qtPagamentoNaoEfetivado
     */
    private long _qtPagamentoNaoEfetivado = 0;

    /**
     * keeps track of state for field: _qtPagamentoNaoEfetivado
     */
    private boolean _has_qtPagamentoNaoEfetivado;

    /**
     * Field _vlPagamentoNaoEfetivado
     */
    private java.math.BigDecimal _vlPagamentoNaoEfetivado = new java.math.BigDecimal("0");

    /**
     * Field _qtTotalPagamento
     */
    private long _qtTotalPagamento = 0;

    /**
     * keeps track of state for field: _qtTotalPagamento
     */
    private boolean _has_qtTotalPagamento;

    /**
     * Field _vlTotalPagamentos
     */
    private java.math.BigDecimal _vlTotalPagamentos = new java.math.BigDecimal("0");


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
        setVlPagamentoAutorizado(new java.math.BigDecimal("0"));
        setVlPagamentoDesautorizado(new java.math.BigDecimal("0"));
        setVlPagamentoEfetivado(new java.math.BigDecimal("0"));
        setVlPagamentoNaoEfetivado(new java.math.BigDecimal("0"));
        setVlTotalPagamentos(new java.math.BigDecimal("0"));
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarlistadebito.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdAgencia
     * 
     */
    public void deleteCdAgencia()
    {
        this._has_cdAgencia= false;
    } //-- void deleteCdAgencia() 

    /**
     * Method deleteCdBanco
     * 
     */
    public void deleteCdBanco()
    {
        this._has_cdBanco= false;
    } //-- void deleteCdBanco() 

    /**
     * Method deleteCdConta
     * 
     */
    public void deleteCdConta()
    {
        this._has_cdConta= false;
    } //-- void deleteCdConta() 

    /**
     * Method deleteCdDigitoAgencia
     * 
     */
    public void deleteCdDigitoAgencia()
    {
        this._has_cdDigitoAgencia= false;
    } //-- void deleteCdDigitoAgencia() 

    /**
     * Method deleteCdListaDebitoPagamento
     * 
     */
    public void deleteCdListaDebitoPagamento()
    {
        this._has_cdListaDebitoPagamento= false;
    } //-- void deleteCdListaDebitoPagamento() 

    /**
     * Method deleteCdPessoaJuridicaContrato
     * 
     */
    public void deleteCdPessoaJuridicaContrato()
    {
        this._has_cdPessoaJuridicaContrato= false;
    } //-- void deleteCdPessoaJuridicaContrato() 

    /**
     * Method deleteCdProdutoServicoOperacao
     * 
     */
    public void deleteCdProdutoServicoOperacao()
    {
        this._has_cdProdutoServicoOperacao= false;
    } //-- void deleteCdProdutoServicoOperacao() 

    /**
     * Method deleteCdProdutoServicoRelacionado
     * 
     */
    public void deleteCdProdutoServicoRelacionado()
    {
        this._has_cdProdutoServicoRelacionado= false;
    } //-- void deleteCdProdutoServicoRelacionado() 

    /**
     * Method deleteCdServicoCompostoPagamento
     * 
     */
    public void deleteCdServicoCompostoPagamento()
    {
        this._has_cdServicoCompostoPagamento= false;
    } //-- void deleteCdServicoCompostoPagamento() 

    /**
     * Method deleteCdSituacaoListaDebito
     * 
     */
    public void deleteCdSituacaoListaDebito()
    {
        this._has_cdSituacaoListaDebito= false;
    } //-- void deleteCdSituacaoListaDebito() 

    /**
     * Method deleteCdTipoConta
     * 
     */
    public void deleteCdTipoConta()
    {
        this._has_cdTipoConta= false;
    } //-- void deleteCdTipoConta() 

    /**
     * Method deleteCdTipoContratoNegocio
     * 
     */
    public void deleteCdTipoContratoNegocio()
    {
        this._has_cdTipoContratoNegocio= false;
    } //-- void deleteCdTipoContratoNegocio() 

    /**
     * Method deleteNrSequenciaContratoNegocio
     * 
     */
    public void deleteNrSequenciaContratoNegocio()
    {
        this._has_nrSequenciaContratoNegocio= false;
    } //-- void deleteNrSequenciaContratoNegocio() 

    /**
     * Method deleteQtPagamendoEfetivado
     * 
     */
    public void deleteQtPagamendoEfetivado()
    {
        this._has_qtPagamendoEfetivado= false;
    } //-- void deleteQtPagamendoEfetivado() 

    /**
     * Method deleteQtPagamentoAutorizado
     * 
     */
    public void deleteQtPagamentoAutorizado()
    {
        this._has_qtPagamentoAutorizado= false;
    } //-- void deleteQtPagamentoAutorizado() 

    /**
     * Method deleteQtPagamentoDesautorizado
     * 
     */
    public void deleteQtPagamentoDesautorizado()
    {
        this._has_qtPagamentoDesautorizado= false;
    } //-- void deleteQtPagamentoDesautorizado() 

    /**
     * Method deleteQtPagamentoNaoEfetivado
     * 
     */
    public void deleteQtPagamentoNaoEfetivado()
    {
        this._has_qtPagamentoNaoEfetivado= false;
    } //-- void deleteQtPagamentoNaoEfetivado() 

    /**
     * Method deleteQtTotalPagamento
     * 
     */
    public void deleteQtTotalPagamento()
    {
        this._has_qtTotalPagamento= false;
    } //-- void deleteQtTotalPagamento() 

    /**
     * Returns the value of field 'cdAgencia'.
     * 
     * @return int
     * @return the value of field 'cdAgencia'.
     */
    public int getCdAgencia()
    {
        return this._cdAgencia;
    } //-- int getCdAgencia() 

    /**
     * Returns the value of field 'cdBanco'.
     * 
     * @return int
     * @return the value of field 'cdBanco'.
     */
    public int getCdBanco()
    {
        return this._cdBanco;
    } //-- int getCdBanco() 

    /**
     * Returns the value of field 'cdConta'.
     * 
     * @return long
     * @return the value of field 'cdConta'.
     */
    public long getCdConta()
    {
        return this._cdConta;
    } //-- long getCdConta() 

    /**
     * Returns the value of field 'cdDigitoAgencia'.
     * 
     * @return int
     * @return the value of field 'cdDigitoAgencia'.
     */
    public int getCdDigitoAgencia()
    {
        return this._cdDigitoAgencia;
    } //-- int getCdDigitoAgencia() 

    /**
     * Returns the value of field 'cdDigitoConta'.
     * 
     * @return String
     * @return the value of field 'cdDigitoConta'.
     */
    public java.lang.String getCdDigitoConta()
    {
        return this._cdDigitoConta;
    } //-- java.lang.String getCdDigitoConta() 

    /**
     * Returns the value of field 'cdListaDebitoPagamento'.
     * 
     * @return long
     * @return the value of field 'cdListaDebitoPagamento'.
     */
    public long getCdListaDebitoPagamento()
    {
        return this._cdListaDebitoPagamento;
    } //-- long getCdListaDebitoPagamento() 

    /**
     * Returns the value of field 'cdPessoaJuridicaContrato'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridicaContrato'.
     */
    public long getCdPessoaJuridicaContrato()
    {
        return this._cdPessoaJuridicaContrato;
    } //-- long getCdPessoaJuridicaContrato() 

    /**
     * Returns the value of field 'cdProdutoServicoOperacao'.
     * 
     * @return int
     * @return the value of field 'cdProdutoServicoOperacao'.
     */
    public int getCdProdutoServicoOperacao()
    {
        return this._cdProdutoServicoOperacao;
    } //-- int getCdProdutoServicoOperacao() 

    /**
     * Returns the value of field 'cdProdutoServicoRelacionado'.
     * 
     * @return int
     * @return the value of field 'cdProdutoServicoRelacionado'.
     */
    public int getCdProdutoServicoRelacionado()
    {
        return this._cdProdutoServicoRelacionado;
    } //-- int getCdProdutoServicoRelacionado() 

    /**
     * Returns the value of field 'cdServicoCompostoPagamento'.
     * 
     * @return long
     * @return the value of field 'cdServicoCompostoPagamento'.
     */
    public long getCdServicoCompostoPagamento()
    {
        return this._cdServicoCompostoPagamento;
    } //-- long getCdServicoCompostoPagamento() 

    /**
     * Returns the value of field 'cdSituacaoListaDebito'.
     * 
     * @return int
     * @return the value of field 'cdSituacaoListaDebito'.
     */
    public int getCdSituacaoListaDebito()
    {
        return this._cdSituacaoListaDebito;
    } //-- int getCdSituacaoListaDebito() 

    /**
     * Returns the value of field 'cdTipoConta'.
     * 
     * @return int
     * @return the value of field 'cdTipoConta'.
     */
    public int getCdTipoConta()
    {
        return this._cdTipoConta;
    } //-- int getCdTipoConta() 

    /**
     * Returns the value of field 'cdTipoContratoNegocio'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoNegocio'.
     */
    public int getCdTipoContratoNegocio()
    {
        return this._cdTipoContratoNegocio;
    } //-- int getCdTipoContratoNegocio() 

    /**
     * Returns the value of field 'dsAgencia'.
     * 
     * @return String
     * @return the value of field 'dsAgencia'.
     */
    public java.lang.String getDsAgencia()
    {
        return this._dsAgencia;
    } //-- java.lang.String getDsAgencia() 

    /**
     * Returns the value of field 'dsBanco'.
     * 
     * @return String
     * @return the value of field 'dsBanco'.
     */
    public java.lang.String getDsBanco()
    {
        return this._dsBanco;
    } //-- java.lang.String getDsBanco() 

    /**
     * Returns the value of field 'dsEmpresa'.
     * 
     * @return String
     * @return the value of field 'dsEmpresa'.
     */
    public java.lang.String getDsEmpresa()
    {
        return this._dsEmpresa;
    } //-- java.lang.String getDsEmpresa() 

    /**
     * Returns the value of field 'dsPessoaEmpresa'.
     * 
     * @return String
     * @return the value of field 'dsPessoaEmpresa'.
     */
    public java.lang.String getDsPessoaEmpresa()
    {
        return this._dsPessoaEmpresa;
    } //-- java.lang.String getDsPessoaEmpresa() 

    /**
     * Returns the value of field 'dsProdutoServicoRelacionado'.
     * 
     * @return String
     * @return the value of field 'dsProdutoServicoRelacionado'.
     */
    public java.lang.String getDsProdutoServicoRelacionado()
    {
        return this._dsProdutoServicoRelacionado;
    } //-- java.lang.String getDsProdutoServicoRelacionado() 

    /**
     * Returns the value of field 'dsResumoProdutoServico'.
     * 
     * @return String
     * @return the value of field 'dsResumoProdutoServico'.
     */
    public java.lang.String getDsResumoProdutoServico()
    {
        return this._dsResumoProdutoServico;
    } //-- java.lang.String getDsResumoProdutoServico() 

    /**
     * Returns the value of field 'dsSituacaoListaDebito'.
     * 
     * @return String
     * @return the value of field 'dsSituacaoListaDebito'.
     */
    public java.lang.String getDsSituacaoListaDebito()
    {
        return this._dsSituacaoListaDebito;
    } //-- java.lang.String getDsSituacaoListaDebito() 

    /**
     * Returns the value of field 'dsTipoConta'.
     * 
     * @return String
     * @return the value of field 'dsTipoConta'.
     */
    public java.lang.String getDsTipoConta()
    {
        return this._dsTipoConta;
    } //-- java.lang.String getDsTipoConta() 

    /**
     * Returns the value of field 'dsTipoContratoNegocio'.
     * 
     * @return String
     * @return the value of field 'dsTipoContratoNegocio'.
     */
    public java.lang.String getDsTipoContratoNegocio()
    {
        return this._dsTipoContratoNegocio;
    } //-- java.lang.String getDsTipoContratoNegocio() 

    /**
     * Returns the value of field 'dtPrevistaPagamento'.
     * 
     * @return String
     * @return the value of field 'dtPrevistaPagamento'.
     */
    public java.lang.String getDtPrevistaPagamento()
    {
        return this._dtPrevistaPagamento;
    } //-- java.lang.String getDtPrevistaPagamento() 

    /**
     * Returns the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @return long
     * @return the value of field 'nrSequenciaContratoNegocio'.
     */
    public long getNrSequenciaContratoNegocio()
    {
        return this._nrSequenciaContratoNegocio;
    } //-- long getNrSequenciaContratoNegocio() 

    /**
     * Returns the value of field 'numeroCpfCnpj'.
     * 
     * @return String
     * @return the value of field 'numeroCpfCnpj'.
     */
    public java.lang.String getNumeroCpfCnpj()
    {
        return this._numeroCpfCnpj;
    } //-- java.lang.String getNumeroCpfCnpj() 

    /**
     * Returns the value of field 'qtPagamendoEfetivado'.
     * 
     * @return long
     * @return the value of field 'qtPagamendoEfetivado'.
     */
    public long getQtPagamendoEfetivado()
    {
        return this._qtPagamendoEfetivado;
    } //-- long getQtPagamendoEfetivado() 

    /**
     * Returns the value of field 'qtPagamentoAutorizado'.
     * 
     * @return long
     * @return the value of field 'qtPagamentoAutorizado'.
     */
    public long getQtPagamentoAutorizado()
    {
        return this._qtPagamentoAutorizado;
    } //-- long getQtPagamentoAutorizado() 

    /**
     * Returns the value of field 'qtPagamentoDesautorizado'.
     * 
     * @return long
     * @return the value of field 'qtPagamentoDesautorizado'.
     */
    public long getQtPagamentoDesautorizado()
    {
        return this._qtPagamentoDesautorizado;
    } //-- long getQtPagamentoDesautorizado() 

    /**
     * Returns the value of field 'qtPagamentoNaoEfetivado'.
     * 
     * @return long
     * @return the value of field 'qtPagamentoNaoEfetivado'.
     */
    public long getQtPagamentoNaoEfetivado()
    {
        return this._qtPagamentoNaoEfetivado;
    } //-- long getQtPagamentoNaoEfetivado() 

    /**
     * Returns the value of field 'qtTotalPagamento'.
     * 
     * @return long
     * @return the value of field 'qtTotalPagamento'.
     */
    public long getQtTotalPagamento()
    {
        return this._qtTotalPagamento;
    } //-- long getQtTotalPagamento() 

    /**
     * Returns the value of field 'vlPagamentoAutorizado'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlPagamentoAutorizado'.
     */
    public java.math.BigDecimal getVlPagamentoAutorizado()
    {
        return this._vlPagamentoAutorizado;
    } //-- java.math.BigDecimal getVlPagamentoAutorizado() 

    /**
     * Returns the value of field 'vlPagamentoDesautorizado'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlPagamentoDesautorizado'.
     */
    public java.math.BigDecimal getVlPagamentoDesautorizado()
    {
        return this._vlPagamentoDesautorizado;
    } //-- java.math.BigDecimal getVlPagamentoDesautorizado() 

    /**
     * Returns the value of field 'vlPagamentoEfetivado'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlPagamentoEfetivado'.
     */
    public java.math.BigDecimal getVlPagamentoEfetivado()
    {
        return this._vlPagamentoEfetivado;
    } //-- java.math.BigDecimal getVlPagamentoEfetivado() 

    /**
     * Returns the value of field 'vlPagamentoNaoEfetivado'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlPagamentoNaoEfetivado'.
     */
    public java.math.BigDecimal getVlPagamentoNaoEfetivado()
    {
        return this._vlPagamentoNaoEfetivado;
    } //-- java.math.BigDecimal getVlPagamentoNaoEfetivado() 

    /**
     * Returns the value of field 'vlTotalPagamentos'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlTotalPagamentos'.
     */
    public java.math.BigDecimal getVlTotalPagamentos()
    {
        return this._vlTotalPagamentos;
    } //-- java.math.BigDecimal getVlTotalPagamentos() 

    /**
     * Method hasCdAgencia
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgencia()
    {
        return this._has_cdAgencia;
    } //-- boolean hasCdAgencia() 

    /**
     * Method hasCdBanco
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdBanco()
    {
        return this._has_cdBanco;
    } //-- boolean hasCdBanco() 

    /**
     * Method hasCdConta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdConta()
    {
        return this._has_cdConta;
    } //-- boolean hasCdConta() 

    /**
     * Method hasCdDigitoAgencia
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdDigitoAgencia()
    {
        return this._has_cdDigitoAgencia;
    } //-- boolean hasCdDigitoAgencia() 

    /**
     * Method hasCdListaDebitoPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdListaDebitoPagamento()
    {
        return this._has_cdListaDebitoPagamento;
    } //-- boolean hasCdListaDebitoPagamento() 

    /**
     * Method hasCdPessoaJuridicaContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridicaContrato()
    {
        return this._has_cdPessoaJuridicaContrato;
    } //-- boolean hasCdPessoaJuridicaContrato() 

    /**
     * Method hasCdProdutoServicoOperacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdutoServicoOperacao()
    {
        return this._has_cdProdutoServicoOperacao;
    } //-- boolean hasCdProdutoServicoOperacao() 

    /**
     * Method hasCdProdutoServicoRelacionado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdutoServicoRelacionado()
    {
        return this._has_cdProdutoServicoRelacionado;
    } //-- boolean hasCdProdutoServicoRelacionado() 

    /**
     * Method hasCdServicoCompostoPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdServicoCompostoPagamento()
    {
        return this._has_cdServicoCompostoPagamento;
    } //-- boolean hasCdServicoCompostoPagamento() 

    /**
     * Method hasCdSituacaoListaDebito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSituacaoListaDebito()
    {
        return this._has_cdSituacaoListaDebito;
    } //-- boolean hasCdSituacaoListaDebito() 

    /**
     * Method hasCdTipoConta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoConta()
    {
        return this._has_cdTipoConta;
    } //-- boolean hasCdTipoConta() 

    /**
     * Method hasCdTipoContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoNegocio()
    {
        return this._has_cdTipoContratoNegocio;
    } //-- boolean hasCdTipoContratoNegocio() 

    /**
     * Method hasNrSequenciaContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaContratoNegocio()
    {
        return this._has_nrSequenciaContratoNegocio;
    } //-- boolean hasNrSequenciaContratoNegocio() 

    /**
     * Method hasQtPagamendoEfetivado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtPagamendoEfetivado()
    {
        return this._has_qtPagamendoEfetivado;
    } //-- boolean hasQtPagamendoEfetivado() 

    /**
     * Method hasQtPagamentoAutorizado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtPagamentoAutorizado()
    {
        return this._has_qtPagamentoAutorizado;
    } //-- boolean hasQtPagamentoAutorizado() 

    /**
     * Method hasQtPagamentoDesautorizado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtPagamentoDesautorizado()
    {
        return this._has_qtPagamentoDesautorizado;
    } //-- boolean hasQtPagamentoDesautorizado() 

    /**
     * Method hasQtPagamentoNaoEfetivado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtPagamentoNaoEfetivado()
    {
        return this._has_qtPagamentoNaoEfetivado;
    } //-- boolean hasQtPagamentoNaoEfetivado() 

    /**
     * Method hasQtTotalPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtTotalPagamento()
    {
        return this._has_qtTotalPagamento;
    } //-- boolean hasQtTotalPagamento() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdAgencia'.
     * 
     * @param cdAgencia the value of field 'cdAgencia'.
     */
    public void setCdAgencia(int cdAgencia)
    {
        this._cdAgencia = cdAgencia;
        this._has_cdAgencia = true;
    } //-- void setCdAgencia(int) 

    /**
     * Sets the value of field 'cdBanco'.
     * 
     * @param cdBanco the value of field 'cdBanco'.
     */
    public void setCdBanco(int cdBanco)
    {
        this._cdBanco = cdBanco;
        this._has_cdBanco = true;
    } //-- void setCdBanco(int) 

    /**
     * Sets the value of field 'cdConta'.
     * 
     * @param cdConta the value of field 'cdConta'.
     */
    public void setCdConta(long cdConta)
    {
        this._cdConta = cdConta;
        this._has_cdConta = true;
    } //-- void setCdConta(long) 

    /**
     * Sets the value of field 'cdDigitoAgencia'.
     * 
     * @param cdDigitoAgencia the value of field 'cdDigitoAgencia'.
     */
    public void setCdDigitoAgencia(int cdDigitoAgencia)
    {
        this._cdDigitoAgencia = cdDigitoAgencia;
        this._has_cdDigitoAgencia = true;
    } //-- void setCdDigitoAgencia(int) 

    /**
     * Sets the value of field 'cdDigitoConta'.
     * 
     * @param cdDigitoConta the value of field 'cdDigitoConta'.
     */
    public void setCdDigitoConta(java.lang.String cdDigitoConta)
    {
        this._cdDigitoConta = cdDigitoConta;
    } //-- void setCdDigitoConta(java.lang.String) 

    /**
     * Sets the value of field 'cdListaDebitoPagamento'.
     * 
     * @param cdListaDebitoPagamento the value of field
     * 'cdListaDebitoPagamento'.
     */
    public void setCdListaDebitoPagamento(long cdListaDebitoPagamento)
    {
        this._cdListaDebitoPagamento = cdListaDebitoPagamento;
        this._has_cdListaDebitoPagamento = true;
    } //-- void setCdListaDebitoPagamento(long) 

    /**
     * Sets the value of field 'cdPessoaJuridicaContrato'.
     * 
     * @param cdPessoaJuridicaContrato the value of field
     * 'cdPessoaJuridicaContrato'.
     */
    public void setCdPessoaJuridicaContrato(long cdPessoaJuridicaContrato)
    {
        this._cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
        this._has_cdPessoaJuridicaContrato = true;
    } //-- void setCdPessoaJuridicaContrato(long) 

    /**
     * Sets the value of field 'cdProdutoServicoOperacao'.
     * 
     * @param cdProdutoServicoOperacao the value of field
     * 'cdProdutoServicoOperacao'.
     */
    public void setCdProdutoServicoOperacao(int cdProdutoServicoOperacao)
    {
        this._cdProdutoServicoOperacao = cdProdutoServicoOperacao;
        this._has_cdProdutoServicoOperacao = true;
    } //-- void setCdProdutoServicoOperacao(int) 

    /**
     * Sets the value of field 'cdProdutoServicoRelacionado'.
     * 
     * @param cdProdutoServicoRelacionado the value of field
     * 'cdProdutoServicoRelacionado'.
     */
    public void setCdProdutoServicoRelacionado(int cdProdutoServicoRelacionado)
    {
        this._cdProdutoServicoRelacionado = cdProdutoServicoRelacionado;
        this._has_cdProdutoServicoRelacionado = true;
    } //-- void setCdProdutoServicoRelacionado(int) 

    /**
     * Sets the value of field 'cdServicoCompostoPagamento'.
     * 
     * @param cdServicoCompostoPagamento the value of field
     * 'cdServicoCompostoPagamento'.
     */
    public void setCdServicoCompostoPagamento(long cdServicoCompostoPagamento)
    {
        this._cdServicoCompostoPagamento = cdServicoCompostoPagamento;
        this._has_cdServicoCompostoPagamento = true;
    } //-- void setCdServicoCompostoPagamento(long) 

    /**
     * Sets the value of field 'cdSituacaoListaDebito'.
     * 
     * @param cdSituacaoListaDebito the value of field
     * 'cdSituacaoListaDebito'.
     */
    public void setCdSituacaoListaDebito(int cdSituacaoListaDebito)
    {
        this._cdSituacaoListaDebito = cdSituacaoListaDebito;
        this._has_cdSituacaoListaDebito = true;
    } //-- void setCdSituacaoListaDebito(int) 

    /**
     * Sets the value of field 'cdTipoConta'.
     * 
     * @param cdTipoConta the value of field 'cdTipoConta'.
     */
    public void setCdTipoConta(int cdTipoConta)
    {
        this._cdTipoConta = cdTipoConta;
        this._has_cdTipoConta = true;
    } //-- void setCdTipoConta(int) 

    /**
     * Sets the value of field 'cdTipoContratoNegocio'.
     * 
     * @param cdTipoContratoNegocio the value of field
     * 'cdTipoContratoNegocio'.
     */
    public void setCdTipoContratoNegocio(int cdTipoContratoNegocio)
    {
        this._cdTipoContratoNegocio = cdTipoContratoNegocio;
        this._has_cdTipoContratoNegocio = true;
    } //-- void setCdTipoContratoNegocio(int) 

    /**
     * Sets the value of field 'dsAgencia'.
     * 
     * @param dsAgencia the value of field 'dsAgencia'.
     */
    public void setDsAgencia(java.lang.String dsAgencia)
    {
        this._dsAgencia = dsAgencia;
    } //-- void setDsAgencia(java.lang.String) 

    /**
     * Sets the value of field 'dsBanco'.
     * 
     * @param dsBanco the value of field 'dsBanco'.
     */
    public void setDsBanco(java.lang.String dsBanco)
    {
        this._dsBanco = dsBanco;
    } //-- void setDsBanco(java.lang.String) 

    /**
     * Sets the value of field 'dsEmpresa'.
     * 
     * @param dsEmpresa the value of field 'dsEmpresa'.
     */
    public void setDsEmpresa(java.lang.String dsEmpresa)
    {
        this._dsEmpresa = dsEmpresa;
    } //-- void setDsEmpresa(java.lang.String) 

    /**
     * Sets the value of field 'dsPessoaEmpresa'.
     * 
     * @param dsPessoaEmpresa the value of field 'dsPessoaEmpresa'.
     */
    public void setDsPessoaEmpresa(java.lang.String dsPessoaEmpresa)
    {
        this._dsPessoaEmpresa = dsPessoaEmpresa;
    } //-- void setDsPessoaEmpresa(java.lang.String) 

    /**
     * Sets the value of field 'dsProdutoServicoRelacionado'.
     * 
     * @param dsProdutoServicoRelacionado the value of field
     * 'dsProdutoServicoRelacionado'.
     */
    public void setDsProdutoServicoRelacionado(java.lang.String dsProdutoServicoRelacionado)
    {
        this._dsProdutoServicoRelacionado = dsProdutoServicoRelacionado;
    } //-- void setDsProdutoServicoRelacionado(java.lang.String) 

    /**
     * Sets the value of field 'dsResumoProdutoServico'.
     * 
     * @param dsResumoProdutoServico the value of field
     * 'dsResumoProdutoServico'.
     */
    public void setDsResumoProdutoServico(java.lang.String dsResumoProdutoServico)
    {
        this._dsResumoProdutoServico = dsResumoProdutoServico;
    } //-- void setDsResumoProdutoServico(java.lang.String) 

    /**
     * Sets the value of field 'dsSituacaoListaDebito'.
     * 
     * @param dsSituacaoListaDebito the value of field
     * 'dsSituacaoListaDebito'.
     */
    public void setDsSituacaoListaDebito(java.lang.String dsSituacaoListaDebito)
    {
        this._dsSituacaoListaDebito = dsSituacaoListaDebito;
    } //-- void setDsSituacaoListaDebito(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoConta'.
     * 
     * @param dsTipoConta the value of field 'dsTipoConta'.
     */
    public void setDsTipoConta(java.lang.String dsTipoConta)
    {
        this._dsTipoConta = dsTipoConta;
    } //-- void setDsTipoConta(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoContratoNegocio'.
     * 
     * @param dsTipoContratoNegocio the value of field
     * 'dsTipoContratoNegocio'.
     */
    public void setDsTipoContratoNegocio(java.lang.String dsTipoContratoNegocio)
    {
        this._dsTipoContratoNegocio = dsTipoContratoNegocio;
    } //-- void setDsTipoContratoNegocio(java.lang.String) 

    /**
     * Sets the value of field 'dtPrevistaPagamento'.
     * 
     * @param dtPrevistaPagamento the value of field
     * 'dtPrevistaPagamento'.
     */
    public void setDtPrevistaPagamento(java.lang.String dtPrevistaPagamento)
    {
        this._dtPrevistaPagamento = dtPrevistaPagamento;
    } //-- void setDtPrevistaPagamento(java.lang.String) 

    /**
     * Sets the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @param nrSequenciaContratoNegocio the value of field
     * 'nrSequenciaContratoNegocio'.
     */
    public void setNrSequenciaContratoNegocio(long nrSequenciaContratoNegocio)
    {
        this._nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
        this._has_nrSequenciaContratoNegocio = true;
    } //-- void setNrSequenciaContratoNegocio(long) 

    /**
     * Sets the value of field 'numeroCpfCnpj'.
     * 
     * @param numeroCpfCnpj the value of field 'numeroCpfCnpj'.
     */
    public void setNumeroCpfCnpj(java.lang.String numeroCpfCnpj)
    {
        this._numeroCpfCnpj = numeroCpfCnpj;
    } //-- void setNumeroCpfCnpj(java.lang.String) 

    /**
     * Sets the value of field 'qtPagamendoEfetivado'.
     * 
     * @param qtPagamendoEfetivado the value of field
     * 'qtPagamendoEfetivado'.
     */
    public void setQtPagamendoEfetivado(long qtPagamendoEfetivado)
    {
        this._qtPagamendoEfetivado = qtPagamendoEfetivado;
        this._has_qtPagamendoEfetivado = true;
    } //-- void setQtPagamendoEfetivado(long) 

    /**
     * Sets the value of field 'qtPagamentoAutorizado'.
     * 
     * @param qtPagamentoAutorizado the value of field
     * 'qtPagamentoAutorizado'.
     */
    public void setQtPagamentoAutorizado(long qtPagamentoAutorizado)
    {
        this._qtPagamentoAutorizado = qtPagamentoAutorizado;
        this._has_qtPagamentoAutorizado = true;
    } //-- void setQtPagamentoAutorizado(long) 

    /**
     * Sets the value of field 'qtPagamentoDesautorizado'.
     * 
     * @param qtPagamentoDesautorizado the value of field
     * 'qtPagamentoDesautorizado'.
     */
    public void setQtPagamentoDesautorizado(long qtPagamentoDesautorizado)
    {
        this._qtPagamentoDesautorizado = qtPagamentoDesautorizado;
        this._has_qtPagamentoDesautorizado = true;
    } //-- void setQtPagamentoDesautorizado(long) 

    /**
     * Sets the value of field 'qtPagamentoNaoEfetivado'.
     * 
     * @param qtPagamentoNaoEfetivado the value of field
     * 'qtPagamentoNaoEfetivado'.
     */
    public void setQtPagamentoNaoEfetivado(long qtPagamentoNaoEfetivado)
    {
        this._qtPagamentoNaoEfetivado = qtPagamentoNaoEfetivado;
        this._has_qtPagamentoNaoEfetivado = true;
    } //-- void setQtPagamentoNaoEfetivado(long) 

    /**
     * Sets the value of field 'qtTotalPagamento'.
     * 
     * @param qtTotalPagamento the value of field 'qtTotalPagamento'
     */
    public void setQtTotalPagamento(long qtTotalPagamento)
    {
        this._qtTotalPagamento = qtTotalPagamento;
        this._has_qtTotalPagamento = true;
    } //-- void setQtTotalPagamento(long) 

    /**
     * Sets the value of field 'vlPagamentoAutorizado'.
     * 
     * @param vlPagamentoAutorizado the value of field
     * 'vlPagamentoAutorizado'.
     */
    public void setVlPagamentoAutorizado(java.math.BigDecimal vlPagamentoAutorizado)
    {
        this._vlPagamentoAutorizado = vlPagamentoAutorizado;
    } //-- void setVlPagamentoAutorizado(java.math.BigDecimal) 

    /**
     * Sets the value of field 'vlPagamentoDesautorizado'.
     * 
     * @param vlPagamentoDesautorizado the value of field
     * 'vlPagamentoDesautorizado'.
     */
    public void setVlPagamentoDesautorizado(java.math.BigDecimal vlPagamentoDesautorizado)
    {
        this._vlPagamentoDesautorizado = vlPagamentoDesautorizado;
    } //-- void setVlPagamentoDesautorizado(java.math.BigDecimal) 

    /**
     * Sets the value of field 'vlPagamentoEfetivado'.
     * 
     * @param vlPagamentoEfetivado the value of field
     * 'vlPagamentoEfetivado'.
     */
    public void setVlPagamentoEfetivado(java.math.BigDecimal vlPagamentoEfetivado)
    {
        this._vlPagamentoEfetivado = vlPagamentoEfetivado;
    } //-- void setVlPagamentoEfetivado(java.math.BigDecimal) 

    /**
     * Sets the value of field 'vlPagamentoNaoEfetivado'.
     * 
     * @param vlPagamentoNaoEfetivado the value of field
     * 'vlPagamentoNaoEfetivado'.
     */
    public void setVlPagamentoNaoEfetivado(java.math.BigDecimal vlPagamentoNaoEfetivado)
    {
        this._vlPagamentoNaoEfetivado = vlPagamentoNaoEfetivado;
    } //-- void setVlPagamentoNaoEfetivado(java.math.BigDecimal) 

    /**
     * Sets the value of field 'vlTotalPagamentos'.
     * 
     * @param vlTotalPagamentos the value of field
     * 'vlTotalPagamentos'.
     */
    public void setVlTotalPagamentos(java.math.BigDecimal vlTotalPagamentos)
    {
        this._vlTotalPagamentos = vlTotalPagamentos;
    } //-- void setVlTotalPagamentos(java.math.BigDecimal) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultarlistadebito.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultarlistadebito.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultarlistadebito.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarlistadebito.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
