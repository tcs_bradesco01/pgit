/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantersolicretransmissaoarquivosretorno.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantersolicretransmissaoarquivosretorno.bean;

import java.math.BigDecimal;

/**
 * Nome: OcorrenciasDetalharSolBaseRetransmissaoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class OcorrenciasDetalharSolBaseRetransmissaoSaidaDTO {
	
	/** Atributo cdPessoa. */
	private Long cdPessoa;
	
	/** Atributo cpfCnpjPessoa. */
	private String cpfCnpjPessoa;
	
	/** Atributo dsPessoa. */
	private String dsPessoa;
	
	/** Atributo cdClienteTransferenciaArquivo. */
	private Long cdClienteTransferenciaArquivo;
	
	/** Atributo cdTipoLayoutArquivo. */
	private Integer cdTipoLayoutArquivo;
	
	/** Atributo dsTipoLayoutArquivo. */
	private String dsTipoLayoutArquivo;
	
	/** Atributo nrSequenciaRetorno. */
	private Long nrSequenciaRetorno;
	
	/** Atributo cdOperacao. */
	private String cdOperacao;
	
	/** Atributo dsArquivoRetorno. */
	private String dsArquivoRetorno;
	
	/** Atributo hrInclusaoRetorno. */
	private String hrInclusaoRetorno;
	
	/** Atributo dsSituacaoProcessamento. */
	private String dsSituacaoProcessamento;
	
	/** Atributo qtdeRegistrosOcorrencias. */
	private Long qtdeRegistrosOcorrencias;
	
	/** Atributo vlrRegistros. */
	private BigDecimal vlrRegistros;
	
	/** Atributo hrInclusaoRetornoFormatada. */
	private String hrInclusaoRetornoFormatada;	
	
	/** Atributo participante. */
	private String participante;
	
	/** Atributo qtdeRegistrosOcorrenciasFormatado. */
	private String qtdeRegistrosOcorrenciasFormatado;
	
	/**
	 * Get: cdClienteTransferenciaArquivo.
	 *
	 * @return cdClienteTransferenciaArquivo
	 */
	public Long getCdClienteTransferenciaArquivo() {
		return cdClienteTransferenciaArquivo;
	}
	
	/**
	 * Set: cdClienteTransferenciaArquivo.
	 *
	 * @param cdClienteTransferenciaArquivo the cd cliente transferencia arquivo
	 */
	public void setCdClienteTransferenciaArquivo(Long cdClienteTransferenciaArquivo) {
		this.cdClienteTransferenciaArquivo = cdClienteTransferenciaArquivo;
	}
	
	/**
	 * Get: cdOperacao.
	 *
	 * @return cdOperacao
	 */
	public String getCdOperacao() {
		return cdOperacao;
	}
	
	/**
	 * Set: cdOperacao.
	 *
	 * @param cdOperacao the cd operacao
	 */
	public void setCdOperacao(String cdOperacao) {
		this.cdOperacao = cdOperacao;
	}
	
	/**
	 * Get: cdPessoa.
	 *
	 * @return cdPessoa
	 */
	public Long getCdPessoa() {
		return cdPessoa;
	}
	
	/**
	 * Set: cdPessoa.
	 *
	 * @param cdPessoa the cd pessoa
	 */
	public void setCdPessoa(Long cdPessoa) {
		this.cdPessoa = cdPessoa;
	}
	
	/**
	 * Get: cdTipoLayoutArquivo.
	 *
	 * @return cdTipoLayoutArquivo
	 */
	public Integer getCdTipoLayoutArquivo() {
		return cdTipoLayoutArquivo;
	}
	
	/**
	 * Set: cdTipoLayoutArquivo.
	 *
	 * @param cdTipoLayoutArquivo the cd tipo layout arquivo
	 */
	public void setCdTipoLayoutArquivo(Integer cdTipoLayoutArquivo) {
		this.cdTipoLayoutArquivo = cdTipoLayoutArquivo;
	}
	
	/**
	 * Get: cpfCnpjPessoa.
	 *
	 * @return cpfCnpjPessoa
	 */
	public String getCpfCnpjPessoa() {
		return cpfCnpjPessoa;
	}
	
	/**
	 * Set: cpfCnpjPessoa.
	 *
	 * @param cpfCnpjPessoa the cpf cnpj pessoa
	 */
	public void setCpfCnpjPessoa(String cpfCnpjPessoa) {
		this.cpfCnpjPessoa = cpfCnpjPessoa;
	}
	
	/**
	 * Get: dsArquivoRetorno.
	 *
	 * @return dsArquivoRetorno
	 */
	public String getDsArquivoRetorno() {
		return dsArquivoRetorno;
	}
	
	/**
	 * Set: dsArquivoRetorno.
	 *
	 * @param dsArquivoRetorno the ds arquivo retorno
	 */
	public void setDsArquivoRetorno(String dsArquivoRetorno) {
		this.dsArquivoRetorno = dsArquivoRetorno;
	}
	
	/**
	 * Get: dsPessoa.
	 *
	 * @return dsPessoa
	 */
	public String getDsPessoa() {
		return dsPessoa;
	}
	
	/**
	 * Set: dsPessoa.
	 *
	 * @param dsPessoa the ds pessoa
	 */
	public void setDsPessoa(String dsPessoa) {
		this.dsPessoa = dsPessoa;
	}
	
	/**
	 * Get: dsSituacaoProcessamento.
	 *
	 * @return dsSituacaoProcessamento
	 */
	public String getDsSituacaoProcessamento() {
		return dsSituacaoProcessamento;
	}
	
	/**
	 * Set: dsSituacaoProcessamento.
	 *
	 * @param dsSituacaoProcessamento the ds situacao processamento
	 */
	public void setDsSituacaoProcessamento(String dsSituacaoProcessamento) {
		this.dsSituacaoProcessamento = dsSituacaoProcessamento;
	}

	/**
	 * Get: dsTipoLayoutArquivo.
	 *
	 * @return dsTipoLayoutArquivo
	 */
	public String getDsTipoLayoutArquivo() {
		return dsTipoLayoutArquivo;
	}
	
	/**
	 * Set: dsTipoLayoutArquivo.
	 *
	 * @param dsTipoLayoutArquivo the ds tipo layout arquivo
	 */
	public void setDsTipoLayoutArquivo(String dsTipoLayoutArquivo) {
		this.dsTipoLayoutArquivo = dsTipoLayoutArquivo;
	}
	
	/**
	 * Get: hrInclusaoRetorno.
	 *
	 * @return hrInclusaoRetorno
	 */
	public String getHrInclusaoRetorno() {
		return hrInclusaoRetorno;
	}
	
	/**
	 * Set: hrInclusaoRetorno.
	 *
	 * @param hrInclusaoRetorno the hr inclusao retorno
	 */
	public void setHrInclusaoRetorno(String hrInclusaoRetorno) {
		this.hrInclusaoRetorno = hrInclusaoRetorno;
	}
	
	/**
	 * Get: hrInclusaoRetornoFormatada.
	 *
	 * @return hrInclusaoRetornoFormatada
	 */
	public String getHrInclusaoRetornoFormatada() {
		return hrInclusaoRetornoFormatada;
	}
	
	/**
	 * Set: hrInclusaoRetornoFormatada.
	 *
	 * @param hrInclusaoRetornoFormatada the hr inclusao retorno formatada
	 */
	public void setHrInclusaoRetornoFormatada(String hrInclusaoRetornoFormatada) {
		this.hrInclusaoRetornoFormatada = hrInclusaoRetornoFormatada;
	}
	
	/**
	 * Get: nrSequenciaRetorno.
	 *
	 * @return nrSequenciaRetorno
	 */
	public Long getNrSequenciaRetorno() {
		return nrSequenciaRetorno;
	}
	
	/**
	 * Set: nrSequenciaRetorno.
	 *
	 * @param nrSequenciaRetorno the nr sequencia retorno
	 */
	public void setNrSequenciaRetorno(Long nrSequenciaRetorno) {
		this.nrSequenciaRetorno = nrSequenciaRetorno;
	}
	
	/**
	 * Get: vlrRegistros.
	 *
	 * @return vlrRegistros
	 */
	public BigDecimal getVlrRegistros() {
		return vlrRegistros;
	}
	
	/**
	 * Set: vlrRegistros.
	 *
	 * @param vlrRegistros the vlr registros
	 */
	public void setVlrRegistros(BigDecimal vlrRegistros) {
		this.vlrRegistros = vlrRegistros;
	}
	
	/**
	 * Get: qtdeRegistrosOcorrencias.
	 *
	 * @return qtdeRegistrosOcorrencias
	 */
	public Long getQtdeRegistrosOcorrencias() {
		return qtdeRegistrosOcorrencias;
	}
	
	/**
	 * Set: qtdeRegistrosOcorrencias.
	 *
	 * @param qtdeRegistrosOcorrencias the qtde registros ocorrencias
	 */
	public void setQtdeRegistrosOcorrencias(Long qtdeRegistrosOcorrencias) {
		this.qtdeRegistrosOcorrencias = qtdeRegistrosOcorrencias;
	}
	
	/**
	 * Get: participante.
	 *
	 * @return participante
	 */
	public String getParticipante() {
		return participante;
	}
	
	/**
	 * Set: participante.
	 *
	 * @param participante the participante
	 */
	public void setParticipante(String participante) {
		this.participante = participante;
	}
	
	/**
	 * Get: qtdeRegistrosOcorrenciasFormatado.
	 *
	 * @return qtdeRegistrosOcorrenciasFormatado
	 */
	public String getQtdeRegistrosOcorrenciasFormatado() {
		return qtdeRegistrosOcorrenciasFormatado;
	}
	
	/**
	 * Set: qtdeRegistrosOcorrenciasFormatado.
	 *
	 * @param qtdeRegistrosOcorrenciasFormatado the qtde registros ocorrencias formatado
	 */
	public void setQtdeRegistrosOcorrenciasFormatado(
			String qtdeRegistrosOcorrenciasFormatado) {
		this.qtdeRegistrosOcorrenciasFormatado = qtdeRegistrosOcorrenciasFormatado;
	}
}
