/*
 * Nome: br.com.bradesco.web.pgit.service.business.combo.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.combo.bean;

/**
 * Nome: ListarSituacaoAditivoContratoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarSituacaoAditivoContratoSaidaDTO {

	
	/** Atributo cdSituacao. */
	private int cdSituacao;
	
	/** Atributo dsSituacao. */
	private String dsSituacao;
	
	/**
	 * Listar situacao aditivo contrato saida dto.
	 *
	 * @param cdSituacao the cd situacao
	 * @param dsSituacao the ds situacao
	 */
	public ListarSituacaoAditivoContratoSaidaDTO(int cdSituacao, String dsSituacao){
		
		this.cdSituacao = cdSituacao;
		this.dsSituacao = dsSituacao;
		
	}

	/**
	 * Get: cdSituacao.
	 *
	 * @return cdSituacao
	 */
	public int getCdSituacao() {
		return cdSituacao;
	}

	/**
	 * Set: cdSituacao.
	 *
	 * @param cdSituacao the cd situacao
	 */
	public void setCdSituacao(int cdSituacao) {
		this.cdSituacao = cdSituacao;
	}

	/**
	 * Get: dsSituacao.
	 *
	 * @return dsSituacao
	 */
	public String getDsSituacao() {
		return dsSituacao;
	}

	/**
	 * Set: dsSituacao.
	 *
	 * @param dsSituacao the ds situacao
	 */
	public void setDsSituacao(String dsSituacao) {
		this.dsSituacao = dsSituacao;
	}
	
	
}
