/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultardetalhevinculocontasalariodestino.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ConsultarDetalheVinculoContaSalarioDestinoRequest.
 * 
 * @version $Revision$ $Date$
 */
public class ConsultarDetalheVinculoContaSalarioDestinoRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdPessoaJuridNegocio
     */
    private long _cdPessoaJuridNegocio = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridNegocio
     */
    private boolean _has_cdPessoaJuridNegocio;

    /**
     * Field _cdTipoContratoNegocio
     */
    private int _cdTipoContratoNegocio = 0;

    /**
     * keeps track of state for field: _cdTipoContratoNegocio
     */
    private boolean _has_cdTipoContratoNegocio;

    /**
     * Field _nrSeqContratoNegocio
     */
    private long _nrSeqContratoNegocio = 0;

    /**
     * keeps track of state for field: _nrSeqContratoNegocio
     */
    private boolean _has_nrSeqContratoNegocio;

    /**
     * Field _cdPessoaJuridVinc
     */
    private long _cdPessoaJuridVinc = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridVinc
     */
    private boolean _has_cdPessoaJuridVinc;

    /**
     * Field _cdTipoContratoVinc
     */
    private int _cdTipoContratoVinc = 0;

    /**
     * keeps track of state for field: _cdTipoContratoVinc
     */
    private boolean _has_cdTipoContratoVinc;

    /**
     * Field _nrSeqContratoVinc
     */
    private long _nrSeqContratoVinc = 0;

    /**
     * keeps track of state for field: _nrSeqContratoVinc
     */
    private boolean _has_nrSeqContratoVinc;

    /**
     * Field _cdTipoVinculoContrato
     */
    private int _cdTipoVinculoContrato = 0;

    /**
     * keeps track of state for field: _cdTipoVinculoContrato
     */
    private boolean _has_cdTipoVinculoContrato;


      //----------------/
     //- Constructors -/
    //----------------/

    public ConsultarDetalheVinculoContaSalarioDestinoRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultardetalhevinculocontasalariodestino.request.ConsultarDetalheVinculoContaSalarioDestinoRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdPessoaJuridNegocio
     * 
     */
    public void deleteCdPessoaJuridNegocio()
    {
        this._has_cdPessoaJuridNegocio= false;
    } //-- void deleteCdPessoaJuridNegocio() 

    /**
     * Method deleteCdPessoaJuridVinc
     * 
     */
    public void deleteCdPessoaJuridVinc()
    {
        this._has_cdPessoaJuridVinc= false;
    } //-- void deleteCdPessoaJuridVinc() 

    /**
     * Method deleteCdTipoContratoNegocio
     * 
     */
    public void deleteCdTipoContratoNegocio()
    {
        this._has_cdTipoContratoNegocio= false;
    } //-- void deleteCdTipoContratoNegocio() 

    /**
     * Method deleteCdTipoContratoVinc
     * 
     */
    public void deleteCdTipoContratoVinc()
    {
        this._has_cdTipoContratoVinc= false;
    } //-- void deleteCdTipoContratoVinc() 

    /**
     * Method deleteCdTipoVinculoContrato
     * 
     */
    public void deleteCdTipoVinculoContrato()
    {
        this._has_cdTipoVinculoContrato= false;
    } //-- void deleteCdTipoVinculoContrato() 

    /**
     * Method deleteNrSeqContratoNegocio
     * 
     */
    public void deleteNrSeqContratoNegocio()
    {
        this._has_nrSeqContratoNegocio= false;
    } //-- void deleteNrSeqContratoNegocio() 

    /**
     * Method deleteNrSeqContratoVinc
     * 
     */
    public void deleteNrSeqContratoVinc()
    {
        this._has_nrSeqContratoVinc= false;
    } //-- void deleteNrSeqContratoVinc() 

    /**
     * Returns the value of field 'cdPessoaJuridNegocio'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridNegocio'.
     */
    public long getCdPessoaJuridNegocio()
    {
        return this._cdPessoaJuridNegocio;
    } //-- long getCdPessoaJuridNegocio() 

    /**
     * Returns the value of field 'cdPessoaJuridVinc'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridVinc'.
     */
    public long getCdPessoaJuridVinc()
    {
        return this._cdPessoaJuridVinc;
    } //-- long getCdPessoaJuridVinc() 

    /**
     * Returns the value of field 'cdTipoContratoNegocio'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoNegocio'.
     */
    public int getCdTipoContratoNegocio()
    {
        return this._cdTipoContratoNegocio;
    } //-- int getCdTipoContratoNegocio() 

    /**
     * Returns the value of field 'cdTipoContratoVinc'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoVinc'.
     */
    public int getCdTipoContratoVinc()
    {
        return this._cdTipoContratoVinc;
    } //-- int getCdTipoContratoVinc() 

    /**
     * Returns the value of field 'cdTipoVinculoContrato'.
     * 
     * @return int
     * @return the value of field 'cdTipoVinculoContrato'.
     */
    public int getCdTipoVinculoContrato()
    {
        return this._cdTipoVinculoContrato;
    } //-- int getCdTipoVinculoContrato() 

    /**
     * Returns the value of field 'nrSeqContratoNegocio'.
     * 
     * @return long
     * @return the value of field 'nrSeqContratoNegocio'.
     */
    public long getNrSeqContratoNegocio()
    {
        return this._nrSeqContratoNegocio;
    } //-- long getNrSeqContratoNegocio() 

    /**
     * Returns the value of field 'nrSeqContratoVinc'.
     * 
     * @return long
     * @return the value of field 'nrSeqContratoVinc'.
     */
    public long getNrSeqContratoVinc()
    {
        return this._nrSeqContratoVinc;
    } //-- long getNrSeqContratoVinc() 

    /**
     * Method hasCdPessoaJuridNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridNegocio()
    {
        return this._has_cdPessoaJuridNegocio;
    } //-- boolean hasCdPessoaJuridNegocio() 

    /**
     * Method hasCdPessoaJuridVinc
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridVinc()
    {
        return this._has_cdPessoaJuridVinc;
    } //-- boolean hasCdPessoaJuridVinc() 

    /**
     * Method hasCdTipoContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoNegocio()
    {
        return this._has_cdTipoContratoNegocio;
    } //-- boolean hasCdTipoContratoNegocio() 

    /**
     * Method hasCdTipoContratoVinc
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoVinc()
    {
        return this._has_cdTipoContratoVinc;
    } //-- boolean hasCdTipoContratoVinc() 

    /**
     * Method hasCdTipoVinculoContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoVinculoContrato()
    {
        return this._has_cdTipoVinculoContrato;
    } //-- boolean hasCdTipoVinculoContrato() 

    /**
     * Method hasNrSeqContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSeqContratoNegocio()
    {
        return this._has_nrSeqContratoNegocio;
    } //-- boolean hasNrSeqContratoNegocio() 

    /**
     * Method hasNrSeqContratoVinc
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSeqContratoVinc()
    {
        return this._has_nrSeqContratoVinc;
    } //-- boolean hasNrSeqContratoVinc() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdPessoaJuridNegocio'.
     * 
     * @param cdPessoaJuridNegocio the value of field
     * 'cdPessoaJuridNegocio'.
     */
    public void setCdPessoaJuridNegocio(long cdPessoaJuridNegocio)
    {
        this._cdPessoaJuridNegocio = cdPessoaJuridNegocio;
        this._has_cdPessoaJuridNegocio = true;
    } //-- void setCdPessoaJuridNegocio(long) 

    /**
     * Sets the value of field 'cdPessoaJuridVinc'.
     * 
     * @param cdPessoaJuridVinc the value of field
     * 'cdPessoaJuridVinc'.
     */
    public void setCdPessoaJuridVinc(long cdPessoaJuridVinc)
    {
        this._cdPessoaJuridVinc = cdPessoaJuridVinc;
        this._has_cdPessoaJuridVinc = true;
    } //-- void setCdPessoaJuridVinc(long) 

    /**
     * Sets the value of field 'cdTipoContratoNegocio'.
     * 
     * @param cdTipoContratoNegocio the value of field
     * 'cdTipoContratoNegocio'.
     */
    public void setCdTipoContratoNegocio(int cdTipoContratoNegocio)
    {
        this._cdTipoContratoNegocio = cdTipoContratoNegocio;
        this._has_cdTipoContratoNegocio = true;
    } //-- void setCdTipoContratoNegocio(int) 

    /**
     * Sets the value of field 'cdTipoContratoVinc'.
     * 
     * @param cdTipoContratoVinc the value of field
     * 'cdTipoContratoVinc'.
     */
    public void setCdTipoContratoVinc(int cdTipoContratoVinc)
    {
        this._cdTipoContratoVinc = cdTipoContratoVinc;
        this._has_cdTipoContratoVinc = true;
    } //-- void setCdTipoContratoVinc(int) 

    /**
     * Sets the value of field 'cdTipoVinculoContrato'.
     * 
     * @param cdTipoVinculoContrato the value of field
     * 'cdTipoVinculoContrato'.
     */
    public void setCdTipoVinculoContrato(int cdTipoVinculoContrato)
    {
        this._cdTipoVinculoContrato = cdTipoVinculoContrato;
        this._has_cdTipoVinculoContrato = true;
    } //-- void setCdTipoVinculoContrato(int) 

    /**
     * Sets the value of field 'nrSeqContratoNegocio'.
     * 
     * @param nrSeqContratoNegocio the value of field
     * 'nrSeqContratoNegocio'.
     */
    public void setNrSeqContratoNegocio(long nrSeqContratoNegocio)
    {
        this._nrSeqContratoNegocio = nrSeqContratoNegocio;
        this._has_nrSeqContratoNegocio = true;
    } //-- void setNrSeqContratoNegocio(long) 

    /**
     * Sets the value of field 'nrSeqContratoVinc'.
     * 
     * @param nrSeqContratoVinc the value of field
     * 'nrSeqContratoVinc'.
     */
    public void setNrSeqContratoVinc(long nrSeqContratoVinc)
    {
        this._nrSeqContratoVinc = nrSeqContratoVinc;
        this._has_nrSeqContratoVinc = true;
    } //-- void setNrSeqContratoVinc(long) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ConsultarDetalheVinculoContaSalarioDestinoRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultardetalhevinculocontasalariodestino.request.ConsultarDetalheVinculoContaSalarioDestinoRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultardetalhevinculocontasalariodestino.request.ConsultarDetalheVinculoContaSalarioDestinoRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultardetalhevinculocontasalariodestino.request.ConsultarDetalheVinculoContaSalarioDestinoRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultardetalhevinculocontasalariodestino.request.ConsultarDetalheVinculoContaSalarioDestinoRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
