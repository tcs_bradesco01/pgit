/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/implementation.ftl,v $
 * $Id: implementation.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: implementation.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.anteciparpostergarpagtosagecon.impl;

import static br.com.bradesco.web.pgit.utils.PgitUtil.verificaIntegerNulo;
import static br.com.bradesco.web.pgit.utils.PgitUtil.verificaLongNulo;
import static br.com.bradesco.web.pgit.utils.PgitUtil.verificaStringNula;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.apache.commons.lang.StringUtils;

import br.com.bradesco.web.pgit.service.business.anteciparpostergarpagtosagecon.IAnteciparPostergarPagtosAgeConService;
import br.com.bradesco.web.pgit.service.business.anteciparpostergarpagtosagecon.IAnteciparPostergarPagtosAgeConServiceConstants;
import br.com.bradesco.web.pgit.service.business.anteciparpostergarpagtosagecon.bean.AntPostergarPagtosIntegralEntradaDTO;
import br.com.bradesco.web.pgit.service.business.anteciparpostergarpagtosagecon.bean.AntPostergarPagtosIntegralSaidaDTO;
import br.com.bradesco.web.pgit.service.business.anteciparpostergarpagtosagecon.bean.AntPostergarPagtosParcialEntradaDTO;
import br.com.bradesco.web.pgit.service.business.anteciparpostergarpagtosagecon.bean.AntPostergarPagtosParcialSaidaDTO;
import br.com.bradesco.web.pgit.service.business.anteciparpostergarpagtosagecon.bean.ConsultaPostergarConsolidadoSolicitacaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.anteciparpostergarpagtosagecon.bean.ConsultaPostergarConsolidadoSolicitacaoOcorrenciasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.anteciparpostergarpagtosagecon.bean.ConsultaPostergarConsolidadoSolicitacaoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.anteciparpostergarpagtosagecon.bean.ConsultarPagtosConsAntPostergacaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.anteciparpostergarpagtosagecon.bean.ConsultarPagtosConsAntPostergacaoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.anteciparpostergarpagtosagecon.bean.DetalharAntPostergarPagtosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.anteciparpostergarpagtosagecon.bean.DetalharAntPostergarPagtosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.anteciparpostergarpagtosagecon.bean.DetalhePostergarConsolidadoSolicitacaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.anteciparpostergarpagtosagecon.bean.DetalhePostergarConsolidadoSolicitacaoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.anteciparpostergarpagtosagecon.bean.ExcluirPostergarConsolidadoSolicitacaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.anteciparpostergarpagtosagecon.bean.ExcluirPostergarConsolidadoSolicitacaoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.anteciparpostergarpagtosagecon.bean.ListarPostergarConsolidadoSolicitacaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.anteciparpostergarpagtosagecon.bean.ListarPostergarConsolidadoSolicitacaoOcorrenciasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.anteciparpostergarpagtosagecon.bean.ListarPostergarConsolidadoSolicitacaoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.anteciparpostergarpagtosagecon.bean.OcorrenciasDetalharAntPostergarPagtosSaidaDTO;
import br.com.bradesco.web.pgit.service.data.pdc.FactoryAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.antpostergarpagtosintegral.request.AntPostergarPagtosIntegralRequest;
import br.com.bradesco.web.pgit.service.data.pdc.antpostergarpagtosintegral.response.AntPostergarPagtosIntegralResponse;
import br.com.bradesco.web.pgit.service.data.pdc.antpostergarpagtosparcial.request.AntPostergarPagtosParcialRequest;
import br.com.bradesco.web.pgit.service.data.pdc.antpostergarpagtosparcial.response.AntPostergarPagtosParcialResponse;
import br.com.bradesco.web.pgit.service.data.pdc.consultapostergarconsolidadosolicitacao.request.ConsultaPostergarConsolidadoSolicitacaoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultapostergarconsolidadosolicitacao.response.ConsultaPostergarConsolidadoSolicitacaoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.consultarpagtosconsantpostergacao.request.ConsultarPagtosConsAntPostergacaoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarpagtosconsantpostergacao.response.ConsultarPagtosConsAntPostergacaoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.detalharantpostergarpagtos.request.DetalharAntPostergarPagtosRequest;
import br.com.bradesco.web.pgit.service.data.pdc.detalharantpostergarpagtos.response.DetalharAntPostergarPagtosResponse;
import br.com.bradesco.web.pgit.service.data.pdc.detalhepostergarconsolidadosolicitacao.request.DetalhePostergarConsolidadoSolicitacaoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.detalhepostergarconsolidadosolicitacao.response.DetalhePostergarConsolidadoSolicitacaoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.excluirpostergarconsolidadosolicitacao.request.ExcluirPostergarConsolidadoSolicitacaoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.excluirpostergarconsolidadosolicitacao.response.ExcluirPostergarConsolidadoSolicitacaoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarpostergarconsolidadosolicitacao.request.ListarPostergarConsolidadoSolicitacaoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarpostergarconsolidadosolicitacao.response.ListarPostergarConsolidadoSolicitacaoResponse;
import br.com.bradesco.web.pgit.utils.CpfCnpjUtils;
import br.com.bradesco.web.pgit.utils.PgitUtil;
import br.com.bradesco.web.pgit.view.converters.FormatarData;

// TODO: Auto-generated Javadoc
/**
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Implementa��o do adaptador: AnteciparPostergarPagtosAgeCon
 * </p>.
 *
 * @comment C�DIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public class AnteciparPostergarPagtosAgeConServiceImpl implements IAnteciparPostergarPagtosAgeConService {
    
    /** Atributo nf. */
    private java.text.NumberFormat nf = java.text.NumberFormat.getInstance(new Locale("pt_br"));

    /** Atributo factoryAdapter. */
    private FactoryAdapter factoryAdapter;

    /**
     * (non-Javadoc).
     *
     * @param entrada the entrada
     * @return the list< consultar pagtos cons ant postergacao saida dt o>
     * @see br.com.bradesco.web.pgit.service.business.anteciparpostergarpagtosagecon.IAnteciparPostergarPagtosAgeConService#consultarPagtosConsAntPostergacao(br.com.bradesco.web.pgit.service.business.anteciparpostergarpagtosagecon.bean.ConsultarPagtosConsAntPostergacaoEntradaDTO)
     */
    public List<ConsultarPagtosConsAntPostergacaoSaidaDTO> consultarPagtosConsAntPostergacao(ConsultarPagtosConsAntPostergacaoEntradaDTO entrada) {
	ConsultarPagtosConsAntPostergacaoRequest request = new ConsultarPagtosConsAntPostergacaoRequest();
	List<ConsultarPagtosConsAntPostergacaoSaidaDTO> listaSaida = new ArrayList<ConsultarPagtosConsAntPostergacaoSaidaDTO>();

	request.setCdAgencia(PgitUtil.verificaIntegerNulo(entrada.getCdAgencia()));
	request.setCdAgendamentoPagosNaoPagos(PgitUtil.verificaIntegerNulo(entrada.getCdAgendamentoPagosNaoPagos()));
	request.setCdBanco(PgitUtil.verificaIntegerNulo(entrada.getCdBanco()));
	request.setCdConta(PgitUtil.verificaLongNulo(entrada.getCdConta()));
	request.setCdDigitoConta(PgitUtil.DIGITO_CONTA);
	request.setCdMotivoSituacaoPagamento(PgitUtil.verificaIntegerNulo(entrada.getCdMotivoSituacaoPagamento()));
	request.setCdPessoaJuridica(PgitUtil.verificaLongNulo(entrada.getCdPessoaJuridica()));
	request.setCdProdutoServicoOperacao(PgitUtil.verificaIntegerNulo(entrada.getCdProdutoServicoOperacao()));
	request.setCdProdutoServicoRelacionado(PgitUtil.verificaIntegerNulo(entrada.getCdProdutoServicoRelacionado()));
	request.setCdSituacaoOperacaoPagamento(PgitUtil.verificaIntegerNulo(entrada.getCdSituacaoOperacaoPagamento()));
	request.setCdTipoContrato(PgitUtil.verificaIntegerNulo(entrada.getCdTipoContrato()));
	request.setCdTipoPesquisa(PgitUtil.verificaIntegerNulo(entrada.getCdTipoPesquisa()));
	request.setDtFinalPagamento(PgitUtil.verificaStringNula(entrada.getDtFinalPagamento()));
	request.setDtInicialPagamento(PgitUtil.verificaStringNula(entrada.getDtInicialPagamento()));
	request.setNrOcorrencias(IAnteciparPostergarPagtosAgeConServiceConstants.NUMERO_OCORRENCIAS_CONSULTA);
	request.setNrRemessa(PgitUtil.verificaLongNulo(entrada.getNrRemessa()));
	request.setNrSequencialContrato(PgitUtil.verificaLongNulo(entrada.getNrSequencialContrato()));
	request.setCdTipoAntecipacao(PgitUtil.verificaIntegerNulo(entrada.getCdTipoAgendamento()));

	ConsultarPagtosConsAntPostergacaoResponse response = getFactoryAdapter().getConsultarPagtosConsAntPostergacaoPDCAdapter().invokeProcess(
		request);

	for (int i = 0; i < response.getOcorrenciasCount(); i++) {
	    ConsultarPagtosConsAntPostergacaoSaidaDTO saida = new ConsultarPagtosConsAntPostergacaoSaidaDTO();

	    saida.setCdAgencia(response.getOcorrencias(i).getCdAgencia());
	    saida.setCdBanco(response.getOcorrencias(i).getCdBanco());
	    saida.setCdConta(response.getOcorrencias(i).getCdConta());
	    saida.setCdDigitoAgencia(response.getOcorrencias(i).getCdDigitoAgencia());
	    saida.setCdDigitoCnpjCpf(response.getOcorrencias(i).getCdDigitoCnpjCpf());
	    saida.setCdDigitoConta(response.getOcorrencias(i).getCdDigitoConta());
	    saida.setCdPessoaJuridica(response.getOcorrencias(i).getCdPessoaJuridica());
	    saida.setCdProdutoServicoOperacao(response.getOcorrencias(i).getCdProdutoServicoOperacao());
	    saida.setCdProdutoServicoRelacionado(response.getOcorrencias(i).getCdProdutoServicoRelacionado());
	    saida.setCdSituacaoOperacaoPagamento(response.getOcorrencias(i).getCdSituacaoOperacaoPagamento());
	    saida.setDsSituacaoOperacaoPagamento(response.getOcorrencias(i).getDsSituacaoOperacaoPagamento());
	    saida.setCdTipoContrato(response.getOcorrencias(i).getCdTipoContrato());
	    saida.setCodMensagem(response.getCodMensagem());
	    saida.setDsOperacaoProdutoServico(response.getOcorrencias(i).getDsOperacaoProdutoServico());
	    saida.setDsRazaoSocial(response.getOcorrencias(i).getDsRazaoSocial());
	    saida.setDsResumoProdutoServico(response.getOcorrencias(i).getDsResumoProdutoServico());

	    saida
		    .setDtPagamento(response.getOcorrencias(i).getDtPagamento() != null && !response.getOcorrencias(i).getDtPagamento().equals("") ? FormatarData
			    .formatarData(response.getOcorrencias(i).getDtPagamento())
			    : "");

	    saida.setMensagem(response.getMensagem());
	    saida.setNrCnpjCpf(response.getOcorrencias(i).getNrCnpjCpf());
	    saida.setNrConsultas(response.getNrConsultas());
	    saida.setNrContrato(response.getOcorrencias(i).getNrContrato());
	    saida.setNrFilialCnpjCpf(response.getOcorrencias(i).getNrFilialCnpjCpf());
	    saida.setNrRemessa(response.getOcorrencias(i).getNrRemessa());
	    saida.setQtPagamento(response.getOcorrencias(i).getQtPagamento());
	    saida.setQtPagamentoFormatado(nf.format(new java.math.BigDecimal(response.getOcorrencias(i).getQtPagamento())));
	    saida.setVlPagamento(response.getOcorrencias(i).getVlEfetivacaoPagamentoCliente());
	    saida.setCpfCnpjFormatado(CpfCnpjUtils
		    .formatCpfCnpjCompleto(saida.getNrCnpjCpf(), saida.getNrFilialCnpjCpf(), saida.getCdDigitoCnpjCpf()));
	    saida.setContaFormatada(PgitUtil.formatBancoAgenciaConta(saida.getCdBanco(), saida.getCdAgencia(), saida.getCdDigitoAgencia(), saida
		    .getCdConta(), saida.getCdDigitoConta(), true));

	    saida.setCdPessoaContratoDebito(response.getOcorrencias(i).getCdPessoaContratoDebito());
	    saida.setCdTipoContratoDebito(response.getOcorrencias(i).getCdTipoContratoDebito());
	    saida.setNrSequenciaContratoDebito(response.getOcorrencias(i).getNrSequenciaContratoDebito());

	    listaSaida.add(saida);
	}

	return listaSaida;

    }

    /**
     * (non-Javadoc).
     *
     * @param entradaDTO the entrada dto
     * @return the detalhar ant postergar pagtos saida dto
     * @see br.com.bradesco.web.pgit.service.business.anteciparpostergarpagtosagecon.IAnteciparPostergarPagtosAgeConService#detalharAntPostergarPagtos(br.com.bradesco.web.pgit.service.business.anteciparpostergarpagtosagecon.bean.DetalharAntPostergarPagtosEntradaDTO)
     */
    public DetalharAntPostergarPagtosSaidaDTO detalharAntPostergarPagtos(DetalharAntPostergarPagtosEntradaDTO entradaDTO) {

	DetalharAntPostergarPagtosResponse response = new DetalharAntPostergarPagtosResponse();
	DetalharAntPostergarPagtosRequest request = new DetalharAntPostergarPagtosRequest();
	DetalharAntPostergarPagtosSaidaDTO saida = new DetalharAntPostergarPagtosSaidaDTO();

	request.setNrOcorrencias(IAnteciparPostergarPagtosAgeConServiceConstants.NUMERO_OCORRENCIAS_DETALHAR);
	request.setCdPessoaJuridicaContrato(PgitUtil.verificaLongNulo(entradaDTO.getCdPessoaJuridicaContrato()));
	request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoContratoNegocio()));
	request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(entradaDTO.getNrSequenciaContratoNegocio()));
	request.setNrCnpjCpf(PgitUtil.verificaLongNulo(entradaDTO.getNrCnpjCpf()));
	request.setNrFilialcnpjCpf(PgitUtil.verificaIntegerNulo(entradaDTO.getNrFilialcnpjCpf()));
	request.setNrControleCnpjCpf(PgitUtil.verificaIntegerNulo(entradaDTO.getNrControleCnpjCpf()));
	request.setNrArquivoRemessaPagamento(PgitUtil.verificaLongNulo(entradaDTO.getNrArquivoRemessaPagamento()));
	request.setDtCreditoPagamento(PgitUtil.verificaStringNula(entradaDTO.getDtCreditoPagamento()));
	request.setCdBanco(PgitUtil.verificaIntegerNulo(entradaDTO.getCdBanco()));
	request.setCdAgencia(PgitUtil.verificaIntegerNulo(entradaDTO.getCdAgencia()));
	request.setCdConta(PgitUtil.verificaLongNulo(entradaDTO.getCdConta()));
	request.setCdDigitoConta(PgitUtil.complementaDigitoConta(entradaDTO.getCdDigitoConta()));
	request.setCdProdutoServicoOperacao(PgitUtil.verificaIntegerNulo(entradaDTO.getCdProdutoServicoOperacao()));
	request.setCdProdutoServicoRelacionado(PgitUtil.verificaIntegerNulo(entradaDTO.getCdProdutoServicoRelacionado()));
	request.setCdSituacaoOperacaoPagamento(PgitUtil.verificaIntegerNulo(entradaDTO.getCdSituacaoOperacaoPagamento()));
	request.setCdAgendadoPagaoNaoPago(PgitUtil.verificaIntegerNulo(entradaDTO.getCdAgendadoPagaoNaoPago()));
	request.setCdPessoaContratoDebito(PgitUtil.verificaLongNulo(entradaDTO.getCdPessoaContratoDebito()));
	request.setCdTipoContratoDebito(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoContratoDebito()));
	request.setNrSequenciaContratoDebito(PgitUtil.verificaLongNulo(entradaDTO.getNrSequenciaContratoDebito()));
	request.setCdTituloPgtoRastreado(0);
	request.setCdIndicadorAutorizacaoPagador(PgitUtil.verificaIntegerNulo(entradaDTO.getCdIndicadorAutorizacaoPagador()));
	request.setNrLoteInternoPagamento(PgitUtil.verificaLongNulo(entradaDTO.getNrLoteInternoPagamento()));

	response = getFactoryAdapter().getDetalharAntPostergarPagtosPDCAdapter().invokeProcess(request);
	
	saida.setCodMensagem(response.getCodMensagem());
	saida.setMensagem(response.getMensagem());
	saida.setNumeroConsultas(response.getNumeroConsultas());
	saida.setDsBancoDebito(response.getDsBancoDebito());
	saida.setDsAgenciaDebito(response.getDsAgenciaDebito());
	saida.setDsTipoContaDebito(response.getDsTipoContaDebito());
	saida.setDsContrato(response.getDsContrato());
	saida.setDsSituacaoContrato(response.getDsSituacaoContrato());
	saida.setDsContrato(response.getDsContrato());
	saida.setDsSituacaoContrato(response.getDsSituacaoContrato());
	saida.setNomeCliente(response.getNmCliente());
	saida.setListaDetalharAntPostergarPagtos(new ArrayList<OcorrenciasDetalharAntPostergarPagtosSaidaDTO>());
	for (int i = 0; i < response.getOcorrenciasCount(); i++) {
	    OcorrenciasDetalharAntPostergarPagtosSaidaDTO o = new OcorrenciasDetalharAntPostergarPagtosSaidaDTO();
	    o.setNrPagamento(response.getOcorrencias(i).getNrPagamento());
	    o.setVlPagamento(response.getOcorrencias(i).getVlPagamento());
	    o.setCdCnpjCpfFavorecido(response.getOcorrencias(i).getCdCnpjCpfFavorecido());
	    o.setCdFilialCnpjCpfFavorecido(response.getOcorrencias(i).getCdFilialCnpjCpfFavorecido());
	    o.setCdControleCnpjCpfFavorecido(response.getOcorrencias(i).getCdControleCnpjCpfFavorecido());
	    o.setCdFavorecido(response.getOcorrencias(i).getCdFavorecido() == 0L ? "" : String.valueOf(response.getOcorrencias(i).getCdFavorecido()));
	    o.setDsBeneficio(response.getOcorrencias(i).getDsBeneficio());
	    o.setCdBanco(response.getOcorrencias(i).getCdBanco());
	    o.setCdAgencia(response.getOcorrencias(i).getCdAgencia());
	    o.setCdDigitoAgencia(response.getOcorrencias(i).getCdDigitoAgencia());
	    o.setCdConta(response.getOcorrencias(i).getCdConta());
	    o.setCdDigitoConta(response.getOcorrencias(i).getCdDigitoConta());
	    o.setCdTipoCanal(response.getOcorrencias(i).getCdTipoCanal());
	    o.setCdTipoTela(response.getOcorrencias(i).getCdTipoTela());
	    o.setFavorecidoBeneficiario(response.getOcorrencias(i).getDsBeneficio());

	    o.setBancoFormatado(PgitUtil.formatBanco(response.getOcorrencias(i).getCdBanco(), false));
	    o.setAgenciaFormatada(PgitUtil.formatAgencia(response.getOcorrencias(i).getCdAgencia(), response.getOcorrencias(i).getCdDigitoAgencia(),
		    false));
	    o.setContaFormatada(PgitUtil.formatConta(response.getOcorrencias(i).getCdConta(), response.getOcorrencias(i).getCdDigitoConta(), false));
	    o.setContaCreditoFormatada(PgitUtil.formatBancoAgenciaConta(response.getOcorrencias(i).getCdBanco(), response.getOcorrencias(i)
		    .getCdAgencia(), response.getOcorrencias(i).getCdDigitoAgencia(), response.getOcorrencias(i).getCdConta(), response
		    .getOcorrencias(i).getCdDigitoConta(), true));
	    
	    o.setDsEfetivacaoPagamento(response.getOcorrencias(i).getDsEfetivacaoPagamento());
	    o.setCdIspbPagtoDestino(response.getOcorrencias(i).getCdIspbPagtoDestino());
	    o.setContaPagtoDestino(response.getOcorrencias(i).getContaPagtoDestino());		
	    
	    saida.getListaDetalharAntPostergarPagtos().add(o);
	}

	return saida;
    }

    /**
     * (non-Javadoc).
     *
     * @param entradaDTO the entrada dto
     * @return the ant postergar pagtos integral saida dto
     * @see br.com.bradesco.web.pgit.service.business.anteciparpostergarpagtosagecon.IAnteciparPostergarPagtosAgeConService#antPostergarPagtosIntegral(br.com.bradesco.web.pgit.service.business.anteciparpostergarpagtosagecon.bean.AntPostergarPagtosIntegralEntradaDTO)
     */
    public AntPostergarPagtosIntegralSaidaDTO antPostergarPagtosIntegral(AntPostergarPagtosIntegralEntradaDTO entradaDTO) {
        AntPostergarPagtosIntegralSaidaDTO saidaDTO = new AntPostergarPagtosIntegralSaidaDTO();
        AntPostergarPagtosIntegralRequest request = new AntPostergarPagtosIntegralRequest();
        AntPostergarPagtosIntegralResponse response = new AntPostergarPagtosIntegralResponse();

        request.setDtQuitacao(PgitUtil.verificaStringNula(entradaDTO.getDtQuitacao()));
        request.setCdPessoaJuridicaContrato(PgitUtil.verificaLongNulo(entradaDTO.getCdPessoaJuridicaContrato()));
        request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoContratoNegocio()));
        request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(entradaDTO.getNrSequenciaContratoNegocio()));
        request.setCdCorpoCpfCnpj(PgitUtil.verificaLongNulo(entradaDTO.getCdCorpoCpfCnpj()));
        request.setCdCnpjFilial(PgitUtil.verificaIntegerNulo(entradaDTO.getCdCnpjFilial()));
        request.setCdDigitoCpfCnpj(StringUtils.leftPad(PgitUtil.verificaStringNula(entradaDTO.getCdDigitoCpfCnpj()), 2,"0"));
        request.setDtPagamento(PgitUtil.verificaStringNula(entradaDTO.getDtPagamento()));
        request.setNrArquivoRemessaPagamento(PgitUtil.verificaLongNulo(entradaDTO.getNrArquivoRemessaPagamento()));
        request.setCdBanco(PgitUtil.verificaIntegerNulo(entradaDTO.getCdBanco()));
        request.setCdAgenciaBancaria(PgitUtil.verificaIntegerNulo(entradaDTO.getCdAgenciaBancaria()));
        request.setCdContaBancaria(PgitUtil.verificaLongNulo(entradaDTO.getCdContaBancaria()));
        request.setCdDigitoContaBancaria(PgitUtil.complementaDigitoConta(entradaDTO.getCdDigitoContaBancaria()));
        request.setCdProdutoServicoOperacao(PgitUtil.verificaIntegerNulo(entradaDTO.getCdProdutoServicoOperacao()));
        request.setCdProdutoServicoRelacionado(PgitUtil
            .verificaIntegerNulo(entradaDTO.getCdProdutoServicoRelacionado()));
        request.setCdSituacaoOperacaoPagamento(PgitUtil
            .verificaIntegerNulo(entradaDTO.getCdSituacaoOperacaoPagamento()));
        request.setCdMotivoSituacaoPagamento(PgitUtil.verificaIntegerNulo(entradaDTO.getCdMotivoSituacaoPagamento()));
        request.setCdPessoaContratoDebito(PgitUtil.verificaLongNulo(entradaDTO.getCdPessoaContratoDebito()));
        request.setCdTipoContratoDebito(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoContratoDebito()));
        request.setNrSequenciaContratoDebito(PgitUtil.verificaLongNulo(entradaDTO.getNrSequenciaContratoDebito()));
        request.setCdIndicadorAntPosProPagamento(PgitUtil.verificaIntegerNulo(entradaDTO
            .getCdIndicadorAntPosProPagamento()));

        response = getFactoryAdapter().getAntPostergarPagtosIntegralPDCAdapter().invokeProcess(request);

        while ("PGIT0009".equals(response.getCodMensagem())) {
            response = getFactoryAdapter().getAntPostergarPagtosIntegralPDCAdapter().invokeProcess(request);
        }

        saidaDTO.setCodMensagem(response.getCodMensagem());
        saidaDTO.setMensagem(response.getMensagem());
        return saidaDTO;
    }

    /**
     * (non-Javadoc).
     *
     * @param entradaDTO the entrada dto
     * @return the ant postergar pagtos parcial saida dto
     * @see br.com.bradesco.web.pgit.service.business.anteciparpostergarpagtosagecon.IAnteciparPostergarPagtosAgeConService#antPostergarPagtosParcial(br.com.bradesco.web.pgit.service.business.anteciparpostergarpagtosagecon.bean.AntPostergarPagtosParcialEntradaDTO)
     */
    public AntPostergarPagtosParcialSaidaDTO antPostergarPagtosParcial(AntPostergarPagtosParcialEntradaDTO entradaDTO) {

	AntPostergarPagtosParcialSaidaDTO saidaDTO = new AntPostergarPagtosParcialSaidaDTO();
	AntPostergarPagtosParcialRequest request = new AntPostergarPagtosParcialRequest();
	AntPostergarPagtosParcialResponse response = new AntPostergarPagtosParcialResponse();

	request.setNumeroConsultas(entradaDTO.getListaOcorrencias().size());
	request.setDtQuitacao(FormatarData.formatarDataToPdc(entradaDTO.getDtQuitacao()));

	ArrayList<br.com.bradesco.web.pgit.service.data.pdc.antpostergarpagtosparcial.request.Ocorrencias> lista = new ArrayList<br.com.bradesco.web.pgit.service.data.pdc.antpostergarpagtosparcial.request.Ocorrencias>();
	br.com.bradesco.web.pgit.service.data.pdc.antpostergarpagtosparcial.request.Ocorrencias ocorrencia;

	// V�rias Ocorr�ncias para Antecipa��o
	for (int i = 0; i < entradaDTO.getListaOcorrencias().size(); i++) {
	    ocorrencia = new br.com.bradesco.web.pgit.service.data.pdc.antpostergarpagtosparcial.request.Ocorrencias();
	    ocorrencia.setCdPessoaJuridicaContrato(PgitUtil.verificaLongNulo(entradaDTO.getListaOcorrencias().get(i).getCdPessoaJuridicaContrato()));
	    ocorrencia.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(entradaDTO.getListaOcorrencias().get(i).getCdTipoContratoNegocio()));
	    ocorrencia.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(entradaDTO.getListaOcorrencias().get(i)
		    .getNrSequenciaContratoNegocio()));
	    ocorrencia.setCdProdutoServicoOperacao(PgitUtil
		    .verificaIntegerNulo(entradaDTO.getListaOcorrencias().get(i).getCdProdutoServicoOperacao()));
	    ocorrencia.setCdProdutoServicoRelacionado(PgitUtil.verificaIntegerNulo(entradaDTO.getListaOcorrencias().get(i)
		    .getCdProdutoServicoRelacionado()));
	    ocorrencia.setCdTipoCanal(PgitUtil.verificaIntegerNulo(entradaDTO.getListaOcorrencias().get(i).getCdTipoCanal()));
	    ocorrencia.setCdControlePagamento(PgitUtil.verificaStringNula(entradaDTO.getListaOcorrencias().get(i).getCdControlePagamento()));
	    ocorrencia.setCdIndicadorAntPosProPagamento(PgitUtil.verificaIntegerNulo(entradaDTO.getListaOcorrencias().get(i)
		    .getCdIndicadorAntPosProPagamento()));
	    lista.add(ocorrencia);
	}

	request.setOcorrencias((br.com.bradesco.web.pgit.service.data.pdc.antpostergarpagtosparcial.request.Ocorrencias[]) lista
		.toArray(new br.com.bradesco.web.pgit.service.data.pdc.antpostergarpagtosparcial.request.Ocorrencias[0]));
	response = getFactoryAdapter().getAntPostergarPagtosParcialPDCAdapter().invokeProcess(request);

	saidaDTO.setCodMensagem(response.getCodMensagem());
	saidaDTO.setMensagem(response.getMensagem());

	return saidaDTO;
    }

    /**
     * (non-Javadoc).
     *
     * @param entradaDTO the entrada dto
     * @return the listar postergar consolidado solicitacao saida dto
     * @see br.com.bradesco.web.pgit.service.business.anteciparpostergarpagtosagecon.IAnteciparPostergarPagtosAgeConService#listarPostergarConsolidadoSolicitacao(br.com.bradesco.web.pgit.service.business.anteciparpostergarpagtosagecon.bean.ListarPostergarConsolidadoSolicitacaoEntradaDTO)
     */
    public ListarPostergarConsolidadoSolicitacaoSaidaDTO listarPostergarConsolidadoSolicitacao(
        ListarPostergarConsolidadoSolicitacaoEntradaDTO entradaDTO) {
        
        ListarPostergarConsolidadoSolicitacaoRequest request =  new ListarPostergarConsolidadoSolicitacaoRequest();
        
        request.setMaxOcorrencias(verificaIntegerNulo(entradaDTO.getMaxOcorrencias()));
        request.setCdSolicitacaoPagamentoIntegrado(verificaIntegerNulo(entradaDTO.getCdSolicitacaoPagamentoIntegrado()));
        request.setNrSolicitacaoPagamentoIntegrado(verificaIntegerNulo(entradaDTO.getNrSolicitacaoPagamentoIntegrado()));
        request.setCdpessoaJuridicaContrato(verificaLongNulo(entradaDTO.getCdpessoaJuridicaContrato()));
        request.setCdTipoContratoNegocio(verificaIntegerNulo(entradaDTO.getCdTipoContratoNegocio()));
        request.setNrSequenciaContratoNegocio(verificaLongNulo(entradaDTO.getNrSequenciaContratoNegocio()));
        
        ListarPostergarConsolidadoSolicitacaoResponse response = 
            getFactoryAdapter().getListarPostergarConsolidadoSolicitacaoPDCAdapter().invokeProcess(request);
        
        ListarPostergarConsolidadoSolicitacaoSaidaDTO saidaDto = new ListarPostergarConsolidadoSolicitacaoSaidaDTO();
        
        saidaDto.setCodMensagem(response.getCodMensagem());
        saidaDto.setMensagem(response.getMensagem());
        saidaDto.setNrLinhas(response.getNrLinhas());
        
        List<ListarPostergarConsolidadoSolicitacaoOcorrenciasSaidaDTO> ocorrencias = 
            new ArrayList<ListarPostergarConsolidadoSolicitacaoOcorrenciasSaidaDTO>(response.getNrLinhas());
        
        
        for (int i = 0; i < response.getNrLinhas(); i++) {
            
            br.com.bradesco.web.pgit.service.data.pdc.listarpostergarconsolidadosolicitacao.response.Ocorrencias item = 
                response.getOcorrencias(i);
            
            ListarPostergarConsolidadoSolicitacaoOcorrenciasSaidaDTO ocorrencia = 
                new ListarPostergarConsolidadoSolicitacaoOcorrenciasSaidaDTO();
            
            ocorrencia.setCdControlePagamento(item.getCdControlePagamento());     
            ocorrencia.setVlAgendamentoPagamento(item.getVlAgendamentoPagamento());  
            ocorrencia.setComplementoRecebedor(item.getComplementoRecebedor());    
            ocorrencia.setCdTipoCtaRecebedor(item.getCdTipoCtaRecebedor());      
            ocorrencia.setCdBancoRecebedor(item.getCdBancoRecebedor());        
            ocorrencia.setCdAgenciaRecebedor(item.getCdAgenciaRecebedor());      
            ocorrencia.setCdDigitoAgenciaRecebedor(item.getCdDigitoAgenciaRecebedor());
            ocorrencia.setCdContaRecebedor(item.getCdContaRecebedor());
            ocorrencia.setCdDigitoContaRecebedor(item.getCdDigitoContaRecebedor());
            
            ocorrencias.add(ocorrencia);
        }

        saidaDto.setOcorrencias(ocorrencias);
        
        return saidaDto;
    }
    
    /**
     * (non-Javadoc).
     *
     * @param entradaDTO the entrada dto
     * @return the excluir postergar consolidado solicitacao saida dto
     * @see br.com.bradesco.web.pgit.service.business.anteciparpostergarpagtosagecon.IAnteciparPostergarPagtosAgeConService#excluirPostergarConsolidadoSolicitacao(br.com.bradesco.web.pgit.service.business.anteciparpostergarpagtosagecon.bean.ExcluirPostergarConsolidadoSolicitacaoEntradaDTO)
     */
    public ExcluirPostergarConsolidadoSolicitacaoSaidaDTO excluirPostergarConsolidadoSolicitacao(
        ExcluirPostergarConsolidadoSolicitacaoEntradaDTO entradaDTO) {
        
        ExcluirPostergarConsolidadoSolicitacaoRequest request =  new ExcluirPostergarConsolidadoSolicitacaoRequest();
        
        request.setCdSolicitacaoPagamentoIntegrado(verificaIntegerNulo(entradaDTO.getCdSolicitacaoPagamentoIntegrado()));
        request.setNrSolicitacaoPagamentoIntegrado(verificaIntegerNulo(entradaDTO.getNrSolicitacaoPagamentoIntegrado()));

        ExcluirPostergarConsolidadoSolicitacaoResponse response = 
            getFactoryAdapter().getExcluirPostergarConsolidadoSolicitacaoPDCAdapter().invokeProcess(request);
        
        ExcluirPostergarConsolidadoSolicitacaoSaidaDTO saidaDto = new ExcluirPostergarConsolidadoSolicitacaoSaidaDTO();
        
        saidaDto.setCodMensagem(response.getCodMensagem());
        saidaDto.setMensagem(response.getMensagem());
        
        return saidaDto;
    }
    
    /**
     * (non-Javadoc).
     *
     * @param entradaDTO the entrada dto
     * @return the consulta postergar consolidado solicitacao saida dto
     * @see br.com.bradesco.web.pgit.service.business.anteciparpostergarpagtosagecon.IAnteciparPostergarPagtosAgeConService#consultaPostergarConsolidadoSolicitacao(br.com.bradesco.web.pgit.service.business.anteciparpostergarpagtosagecon.bean.ConsultaPostergarConsolidadoSolicitacaoEntradaDTO)
     */
    public ConsultaPostergarConsolidadoSolicitacaoSaidaDTO consultaPostergarConsolidadoSolicitacao(
        ConsultaPostergarConsolidadoSolicitacaoEntradaDTO entradaDTO) {

        ConsultaPostergarConsolidadoSolicitacaoRequest request =  new ConsultaPostergarConsolidadoSolicitacaoRequest();
     
        request.setMaxOcorrencias(verificaIntegerNulo(entradaDTO.getMaxOcorrencias()));
        request.setCdpessoaJuridicaContrato(verificaLongNulo(entradaDTO.getCdpessoaJuridicaContrato()));
        request.setCdTipoContratoNegocio(verificaIntegerNulo(entradaDTO.getCdTipoContratoNegocio()));
        request.setNrSequenciaContratoNegocio(verificaLongNulo(entradaDTO.getNrSequenciaContratoNegocio()));
        request.setDtPgtoInicial(verificaStringNula(entradaDTO.getDtPgtoInicial()));
        request.setDtPgtoFinal(verificaStringNula(entradaDTO.getDtPgtoFinal()));
        request.setCdSituacaoSolicitacaoPagamento(verificaIntegerNulo(entradaDTO.getCdSituacaoSolicitacaoPagamento()));
        request.setCdMotivoSolicitacao(verificaIntegerNulo(entradaDTO.getCdMotivoSolicitacao()));
        
        ConsultaPostergarConsolidadoSolicitacaoResponse response = 
            getFactoryAdapter().getConsultaPostergarConsolidadoSolicitacaoPDCAdapter().invokeProcess(request);
        
        ConsultaPostergarConsolidadoSolicitacaoSaidaDTO saidaDto = new ConsultaPostergarConsolidadoSolicitacaoSaidaDTO();
        
        saidaDto.setCodMensagem(response.getCodMensagem());
        saidaDto.setMensagem(response.getMensagem());
        saidaDto.setNrLinhas(response.getNrLinhas());
        
        List<ConsultaPostergarConsolidadoSolicitacaoOcorrenciasSaidaDTO> ocorrencias = 
            new ArrayList<ConsultaPostergarConsolidadoSolicitacaoOcorrenciasSaidaDTO>(response.getNrLinhas());
        
        
        for (int i = 0; i < response.getNrLinhas(); i++) {
            
            br.com.bradesco.web.pgit.service.data.pdc.consultapostergarconsolidadosolicitacao.response.Ocorrencias item = 
                response.getOcorrencias(i);
            
            ConsultaPostergarConsolidadoSolicitacaoOcorrenciasSaidaDTO ocorrencia = 
                new ConsultaPostergarConsolidadoSolicitacaoOcorrenciasSaidaDTO();

            ocorrencia.setCdSolicitacaoPagamentoIntegrado(item.getCdSolicitacaoPagamentoIntegrado());
            ocorrencia.setNrSolicitacaoPagamentoIntegrado(item.getNrSolicitacaoPagamentoIntegrado());
            ocorrencia.setCdpessoaJuridicaContrato(item.getCdpessoaJuridicaContrato());
            ocorrencia.setDsPessoaJuridicaContrato(item.getDsPessoaJuridicaContrato());
            ocorrencia.setCdTipoContratoNegocio(item.getCdTipoContratoNegocio());
            ocorrencia.setNrSequenciaContratoNegocio(item.getNrSequenciaContratoNegocio());
            ocorrencia.setCdSituacaoSolicitacaoPagamento(item.getCdSituacaoSolicitacaoPagamento());
            ocorrencia.setDsSituacaoSolicitacaoPagamento(item.getDsSituacaoSolicitacaoPagamento());
            ocorrencia.setCdMotivoSolicitacao(item.getCdMotivoSolicitacao());
            ocorrencia.setDsMotivoSolicitacao(item.getDsMotivoSolicitacao());
            ocorrencia.setDtInclusaoSolicitacao(item.getDtInclusaoSolicitacao());
            ocorrencia.setHrInclusaoSolicitacao(item.getHrInclusaoSolicitacao());
            ocorrencia.setDsTipoSolicitacao(item.getDsTipoSolicitacao());
            ocorrencia.setNrArquivoRemessa(item.getNrArquivoRemessa());
            ocorrencia.setCdProdutoServicoOperacao(item.getCdProdutoServicoOperacao());
            ocorrencia.setDsProdutoServicoOperacao(item.getDsProdutoServicoOperacao());
            ocorrencia.setCdProdutoOperacaoRelacionado(item.getCdProdutoOperacaoRelacionado());
            ocorrencia.setDsProdutoOperacaoRelacionado(item.getDsProdutoOperacaoRelacionado());
 
            ocorrencias.add(ocorrencia);
        }

        saidaDto.setOcorrencias(ocorrencias);
        
        return saidaDto;
    }
    
    /**
     * (non-Javadoc).
     *
     * @param entradaDTO the entrada dto
     * @return the detalhe postergar consolidado solicitacao saida dto
     * @see br.com.bradesco.web.pgit.service.business.anteciparpostergarpagtosagecon.IAnteciparPostergarPagtosAgeConService#detalhePostergarConsolidadoSolicitacao(br.com.bradesco.web.pgit.service.business.anteciparpostergarpagtosagecon.bean.DetalhePostergarConsolidadoSolicitacaoEntradaDTO)
     */
    public DetalhePostergarConsolidadoSolicitacaoSaidaDTO detalhePostergarConsolidadoSolicitacao(
        DetalhePostergarConsolidadoSolicitacaoEntradaDTO entradaDTO) {
        
        DetalhePostergarConsolidadoSolicitacaoRequest request =  new DetalhePostergarConsolidadoSolicitacaoRequest();
        
        request.setCdSolicitacaoPagamentoIntegrado(verificaIntegerNulo(entradaDTO.getCdSolicitacaoPagamentoIntegrado()));
        request.setNrSolicitacaoPagamentoIntegrado(verificaIntegerNulo(entradaDTO.getNrSolicitacaoPagamentoIntegrado()));

        DetalhePostergarConsolidadoSolicitacaoResponse response = 
            getFactoryAdapter().getDetalhePostergarConsolidadoSolicitacaoPDCAdapter().invokeProcess(request);
        
        DetalhePostergarConsolidadoSolicitacaoSaidaDTO saidaDto = new DetalhePostergarConsolidadoSolicitacaoSaidaDTO();
        
        saidaDto.setCodMensagem(response.getCodMensagem());
        saidaDto.setMensagem(response.getMensagem());
        
        saidaDto.setCdSolicitacaoPagamentoIntegrado(response.getCdSolicitacaoPagamentoIntegrado());
        saidaDto.setNrSolicitacaoPagamentoIntegrado(response.getNrSolicitacaoPagamentoIntegrado());
        saidaDto.setCdpessoaJuridicaContrato(response.getCdpessoaJuridicaContrato());
        saidaDto.setCdTipoContratoNegocio(response.getCdTipoContratoNegocio());
        saidaDto.setNrSequenciaContratoNegocio(response.getNrSequenciaContratoNegocio());
        saidaDto.setCdSituacaoSolicitacaoPagamento(response.getCdSituacaoSolicitacaoPagamento());
        saidaDto.setDsSituacaoSolicitacaoPagamento(response.getDsSituacaoSolicitacaoPagamento());
        saidaDto.setCdMotivoSolicitacao(response.getCdMotivoSolicitacao());
        saidaDto.setDsMotivoSolicitacao(response.getDsMotivoSolicitacao());
        saidaDto.setDsTipoSolicitacao(response.getDsTipoSolicitacao());
        saidaDto.setDtAgendamentoPagamento(response.getDtAgendamentoPagamento());
        saidaDto.setDtPrevistaAgendaPagamento(response.getDtPrevistaAgendaPagamento());
        saidaDto.setCdProdutoServicoOperacao(response.getCdProdutoServicoOperacao());
        saidaDto.setDsProdutoServicoOperacao(response.getDsProdutoServicoOperacao());
        saidaDto.setCdProdutoOperacaoRelacionado(response.getCdProdutoOperacaoRelacionado());
        saidaDto.setDsProdutoOperRelacionado(response.getDsProdutoOperRelacionado());
        saidaDto.setNrArquivoRemssaPagamento(response.getNrArquivoRemssaPagamento());
        saidaDto.setCdSituacaoPagamentoCliente(response.getCdSituacaoPagamentoCliente());
        saidaDto.setDsSituacaoPagamentoCliente(response.getDsSituacaoPagamentoCliente());
        saidaDto.setCdTipoContaPagador(response.getCdTipoContaPagador());
        saidaDto.setDsTipoContaPagador(response.getDsTipoContaPagador());
        saidaDto.setCdBancoPagador(response.getCdBancoPagador());
        saidaDto.setDsBancoPagador(response.getDsBancoPagador());
        saidaDto.setCdAgenciaBancariaPagador(response.getCdAgenciaBancariaPagador());
        saidaDto.setDsAgenciaBancariaPagador(response.getDsAgenciaBancariaPagador());
        saidaDto.setCdDigitoAgenciaPagador(response.getCdDigitoAgenciaPagador());
        saidaDto.setCdContaBancariaPagador(response.getCdContaBancariaPagador());
        saidaDto.setCdDigitoContaPagador(response.getCdDigitoContaPagador());
        saidaDto.setCdTipoInscricaoPagador(response.getCdTipoInscricaoPagador());
        saidaDto.setCdCpfCnpjPagador(response.getCdCpfCnpjPagador());
        saidaDto.setCdFilialCpfCnpjPagador(response.getCdFilialCpfCnpjPagador());
        saidaDto.setCdControleCpfCnpjPagador(response.getCdControleCpfCnpjPagador());
        saidaDto.setDsComplementoPagador(response.getDsComplementoPagador());
        saidaDto.setQtdPagamentoPrevtSolicitacao(response.getQtdPagamentoPrevtSolicitacao() == 0 ? null :  response.getQtdPagamentoPrevtSolicitacao());
        saidaDto.setQtdPagamentoEfetvSolicitacao(response.getQtdPagamentoEfetvSolicitacao() == 0 ? null :  response.getQtdPagamentoEfetvSolicitacao());
        saidaDto.setQtdePagamentosProcessados(response.getQtdePagamentosProcessados() == 0 ? null :  response.getQtdePagamentosProcessados());
        saidaDto.setVlPagamentoPrevtSolicitacao(response.getVlPagamentoPrevtSolicitacao() == new BigDecimal(0) ? null : response.getVlPagamentoPrevtSolicitacao());
        saidaDto.setVlPagamentoEfetivoSolicitacao(response.getVlPagamentoEfetivoSolicitacao() == new BigDecimal(0) ? null :  response.getVlPagamentoEfetivoSolicitacao());
        saidaDto.setVlrPagamentosProcessados(response.getVlrPagamentosProcessados() == new BigDecimal(0) ? null :  response.getVlrPagamentosProcessados() );
        saidaDto.setDtAtendimentoSolicitacaoPagamento(response.getDtAtendimentoSolicitacaoPagamento());
        saidaDto.setHrAtendimentoSolicitacaoPagamento(response.getHrAtendimentoSolicitacaoPagamento());
        saidaDto.setCdCanalInclusao(response.getCdCanalInclusao());
        saidaDto.setDsCanalInclusao(response.getDsCanalInclusao());
        saidaDto.setCdAutenticacaoSegurancaInclusao(response.getCdAutenticacaoSegurancaInclusao());
        saidaDto.setNrOperacaoFluxoInclusao(response.getNrOperacaoFluxoInclusao());
        saidaDto.setCdCanalManutencao(response.getCdCanalManutencao());
        saidaDto.setDsCanalManutencao(response.getDsCanalManutencao());
        saidaDto.setCdAutenticacaoSegurancaManutencao(response.getCdAutenticacaoSegurancaManutencao());
        saidaDto.setNmOperacaoFluxoManutencao(response.getNmOperacaoFluxoManutencao());
        
        
        
        SimpleDateFormat formato1 = new SimpleDateFormat("yyyy-MM-dd-HH.mm.ss");
        SimpleDateFormat formato2 = new SimpleDateFormat("dd/MM/yyyy - HH:mm:ss");
       
        try {
            saidaDto.setHrInclusaoRegistro(formato2.format(formato1.parse(response.getHrInclusaoRegistro())));
            saidaDto.setHrManutencaoRegistro(formato2.format(formato1.parse(response.getHrManutencaoRegistro())));
        } catch (ParseException e) {
           
            e.printStackTrace();
        }
        
        return saidaDto;
    }

    /**
     * Get: factoryAdapter.
     *
     * @return factoryAdapter
     */
    public FactoryAdapter getFactoryAdapter() {
	return factoryAdapter;
    }

    /**
     * Set: factoryAdapter.
     *
     * @param factoryAdapter the factory adapter
     */
    public void setFactoryAdapter(FactoryAdapter factoryAdapter) {
	this.factoryAdapter = factoryAdapter;
    }

}