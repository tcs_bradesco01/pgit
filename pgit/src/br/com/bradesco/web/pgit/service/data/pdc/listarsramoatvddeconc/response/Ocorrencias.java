/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listarsramoatvddeconc.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdSubRamoAtividade
     */
    private int _cdSubRamoAtividade = 0;

    /**
     * keeps track of state for field: _cdSubRamoAtividade
     */
    private boolean _has_cdSubRamoAtividade;

    /**
     * Field _cdAtividadeEconomica
     */
    private int _cdAtividadeEconomica = 0;

    /**
     * keeps track of state for field: _cdAtividadeEconomica
     */
    private boolean _has_cdAtividadeEconomica;

    /**
     * Field _dsAtividadeEconomica
     */
    private java.lang.String _dsAtividadeEconomica;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarsramoatvddeconc.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdAtividadeEconomica
     * 
     */
    public void deleteCdAtividadeEconomica()
    {
        this._has_cdAtividadeEconomica= false;
    } //-- void deleteCdAtividadeEconomica() 

    /**
     * Method deleteCdSubRamoAtividade
     * 
     */
    public void deleteCdSubRamoAtividade()
    {
        this._has_cdSubRamoAtividade= false;
    } //-- void deleteCdSubRamoAtividade() 

    /**
     * Returns the value of field 'cdAtividadeEconomica'.
     * 
     * @return int
     * @return the value of field 'cdAtividadeEconomica'.
     */
    public int getCdAtividadeEconomica()
    {
        return this._cdAtividadeEconomica;
    } //-- int getCdAtividadeEconomica() 

    /**
     * Returns the value of field 'cdSubRamoAtividade'.
     * 
     * @return int
     * @return the value of field 'cdSubRamoAtividade'.
     */
    public int getCdSubRamoAtividade()
    {
        return this._cdSubRamoAtividade;
    } //-- int getCdSubRamoAtividade() 

    /**
     * Returns the value of field 'dsAtividadeEconomica'.
     * 
     * @return String
     * @return the value of field 'dsAtividadeEconomica'.
     */
    public java.lang.String getDsAtividadeEconomica()
    {
        return this._dsAtividadeEconomica;
    } //-- java.lang.String getDsAtividadeEconomica() 

    /**
     * Method hasCdAtividadeEconomica
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAtividadeEconomica()
    {
        return this._has_cdAtividadeEconomica;
    } //-- boolean hasCdAtividadeEconomica() 

    /**
     * Method hasCdSubRamoAtividade
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSubRamoAtividade()
    {
        return this._has_cdSubRamoAtividade;
    } //-- boolean hasCdSubRamoAtividade() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdAtividadeEconomica'.
     * 
     * @param cdAtividadeEconomica the value of field
     * 'cdAtividadeEconomica'.
     */
    public void setCdAtividadeEconomica(int cdAtividadeEconomica)
    {
        this._cdAtividadeEconomica = cdAtividadeEconomica;
        this._has_cdAtividadeEconomica = true;
    } //-- void setCdAtividadeEconomica(int) 

    /**
     * Sets the value of field 'cdSubRamoAtividade'.
     * 
     * @param cdSubRamoAtividade the value of field
     * 'cdSubRamoAtividade'.
     */
    public void setCdSubRamoAtividade(int cdSubRamoAtividade)
    {
        this._cdSubRamoAtividade = cdSubRamoAtividade;
        this._has_cdSubRamoAtividade = true;
    } //-- void setCdSubRamoAtividade(int) 

    /**
     * Sets the value of field 'dsAtividadeEconomica'.
     * 
     * @param dsAtividadeEconomica the value of field
     * 'dsAtividadeEconomica'.
     */
    public void setDsAtividadeEconomica(java.lang.String dsAtividadeEconomica)
    {
        this._dsAtividadeEconomica = dsAtividadeEconomica;
    } //-- void setDsAtividadeEconomica(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listarsramoatvddeconc.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listarsramoatvddeconc.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listarsramoatvddeconc.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarsramoatvddeconc.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
