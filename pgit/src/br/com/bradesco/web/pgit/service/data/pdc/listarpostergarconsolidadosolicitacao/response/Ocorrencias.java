/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listarpostergarconsolidadosolicitacao.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.math.BigDecimal;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdControlePagamento
     */
    private java.lang.String _cdControlePagamento;

    /**
     * Field _vlAgendamentoPagamento
     */
    private java.math.BigDecimal _vlAgendamentoPagamento = new java.math.BigDecimal("0");

    /**
     * Field _complementoRecebedor
     */
    private java.lang.String _complementoRecebedor;

    /**
     * Field _cdTipoCtaRecebedor
     */
    private int _cdTipoCtaRecebedor = 0;

    /**
     * keeps track of state for field: _cdTipoCtaRecebedor
     */
    private boolean _has_cdTipoCtaRecebedor;

    /**
     * Field _cdBancoRecebedor
     */
    private int _cdBancoRecebedor = 0;

    /**
     * keeps track of state for field: _cdBancoRecebedor
     */
    private boolean _has_cdBancoRecebedor;

    /**
     * Field _cdAgenciaRecebedor
     */
    private int _cdAgenciaRecebedor = 0;

    /**
     * keeps track of state for field: _cdAgenciaRecebedor
     */
    private boolean _has_cdAgenciaRecebedor;

    /**
     * Field _cdDigitoAgenciaRecebedor
     */
    private java.lang.String _cdDigitoAgenciaRecebedor;

    /**
     * Field _cdContaRecebedor
     */
    private long _cdContaRecebedor = 0;

    /**
     * keeps track of state for field: _cdContaRecebedor
     */
    private boolean _has_cdContaRecebedor;

    /**
     * Field _cdDigitoContaRecebedor
     */
    private java.lang.String _cdDigitoContaRecebedor;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
        setVlAgendamentoPagamento(new java.math.BigDecimal("0"));
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarpostergarconsolidadosolicitacao.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdAgenciaRecebedor
     * 
     */
    public void deleteCdAgenciaRecebedor()
    {
        this._has_cdAgenciaRecebedor= false;
    } //-- void deleteCdAgenciaRecebedor() 

    /**
     * Method deleteCdBancoRecebedor
     * 
     */
    public void deleteCdBancoRecebedor()
    {
        this._has_cdBancoRecebedor= false;
    } //-- void deleteCdBancoRecebedor() 

    /**
     * Method deleteCdContaRecebedor
     * 
     */
    public void deleteCdContaRecebedor()
    {
        this._has_cdContaRecebedor= false;
    } //-- void deleteCdContaRecebedor() 

    /**
     * Method deleteCdTipoCtaRecebedor
     * 
     */
    public void deleteCdTipoCtaRecebedor()
    {
        this._has_cdTipoCtaRecebedor= false;
    } //-- void deleteCdTipoCtaRecebedor() 

    /**
     * Returns the value of field 'cdAgenciaRecebedor'.
     * 
     * @return int
     * @return the value of field 'cdAgenciaRecebedor'.
     */
    public int getCdAgenciaRecebedor()
    {
        return this._cdAgenciaRecebedor;
    } //-- int getCdAgenciaRecebedor() 

    /**
     * Returns the value of field 'cdBancoRecebedor'.
     * 
     * @return int
     * @return the value of field 'cdBancoRecebedor'.
     */
    public int getCdBancoRecebedor()
    {
        return this._cdBancoRecebedor;
    } //-- int getCdBancoRecebedor() 

    /**
     * Returns the value of field 'cdContaRecebedor'.
     * 
     * @return long
     * @return the value of field 'cdContaRecebedor'.
     */
    public long getCdContaRecebedor()
    {
        return this._cdContaRecebedor;
    } //-- long getCdContaRecebedor() 

    /**
     * Returns the value of field 'cdControlePagamento'.
     * 
     * @return String
     * @return the value of field 'cdControlePagamento'.
     */
    public java.lang.String getCdControlePagamento()
    {
        return this._cdControlePagamento;
    } //-- java.lang.String getCdControlePagamento() 

    /**
     * Returns the value of field 'cdDigitoAgenciaRecebedor'.
     * 
     * @return String
     * @return the value of field 'cdDigitoAgenciaRecebedor'.
     */
    public java.lang.String getCdDigitoAgenciaRecebedor()
    {
        return this._cdDigitoAgenciaRecebedor;
    } //-- java.lang.String getCdDigitoAgenciaRecebedor() 

    /**
     * Returns the value of field 'cdDigitoContaRecebedor'.
     * 
     * @return String
     * @return the value of field 'cdDigitoContaRecebedor'.
     */
    public java.lang.String getCdDigitoContaRecebedor()
    {
        return this._cdDigitoContaRecebedor;
    } //-- java.lang.String getCdDigitoContaRecebedor() 

    /**
     * Returns the value of field 'cdTipoCtaRecebedor'.
     * 
     * @return int
     * @return the value of field 'cdTipoCtaRecebedor'.
     */
    public int getCdTipoCtaRecebedor()
    {
        return this._cdTipoCtaRecebedor;
    } //-- int getCdTipoCtaRecebedor() 

    /**
     * Returns the value of field 'complementoRecebedor'.
     * 
     * @return String
     * @return the value of field 'complementoRecebedor'.
     */
    public java.lang.String getComplementoRecebedor()
    {
        return this._complementoRecebedor;
    } //-- java.lang.String getComplementoRecebedor() 

    /**
     * Returns the value of field 'vlAgendamentoPagamento'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlAgendamentoPagamento'.
     */
    public java.math.BigDecimal getVlAgendamentoPagamento()
    {
        return this._vlAgendamentoPagamento;
    } //-- java.math.BigDecimal getVlAgendamentoPagamento() 

    /**
     * Method hasCdAgenciaRecebedor
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgenciaRecebedor()
    {
        return this._has_cdAgenciaRecebedor;
    } //-- boolean hasCdAgenciaRecebedor() 

    /**
     * Method hasCdBancoRecebedor
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdBancoRecebedor()
    {
        return this._has_cdBancoRecebedor;
    } //-- boolean hasCdBancoRecebedor() 

    /**
     * Method hasCdContaRecebedor
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdContaRecebedor()
    {
        return this._has_cdContaRecebedor;
    } //-- boolean hasCdContaRecebedor() 

    /**
     * Method hasCdTipoCtaRecebedor
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoCtaRecebedor()
    {
        return this._has_cdTipoCtaRecebedor;
    } //-- boolean hasCdTipoCtaRecebedor() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdAgenciaRecebedor'.
     * 
     * @param cdAgenciaRecebedor the value of field
     * 'cdAgenciaRecebedor'.
     */
    public void setCdAgenciaRecebedor(int cdAgenciaRecebedor)
    {
        this._cdAgenciaRecebedor = cdAgenciaRecebedor;
        this._has_cdAgenciaRecebedor = true;
    } //-- void setCdAgenciaRecebedor(int) 

    /**
     * Sets the value of field 'cdBancoRecebedor'.
     * 
     * @param cdBancoRecebedor the value of field 'cdBancoRecebedor'
     */
    public void setCdBancoRecebedor(int cdBancoRecebedor)
    {
        this._cdBancoRecebedor = cdBancoRecebedor;
        this._has_cdBancoRecebedor = true;
    } //-- void setCdBancoRecebedor(int) 

    /**
     * Sets the value of field 'cdContaRecebedor'.
     * 
     * @param cdContaRecebedor the value of field 'cdContaRecebedor'
     */
    public void setCdContaRecebedor(long cdContaRecebedor)
    {
        this._cdContaRecebedor = cdContaRecebedor;
        this._has_cdContaRecebedor = true;
    } //-- void setCdContaRecebedor(long) 

    /**
     * Sets the value of field 'cdControlePagamento'.
     * 
     * @param cdControlePagamento the value of field
     * 'cdControlePagamento'.
     */
    public void setCdControlePagamento(java.lang.String cdControlePagamento)
    {
        this._cdControlePagamento = cdControlePagamento;
    } //-- void setCdControlePagamento(java.lang.String) 

    /**
     * Sets the value of field 'cdDigitoAgenciaRecebedor'.
     * 
     * @param cdDigitoAgenciaRecebedor the value of field
     * 'cdDigitoAgenciaRecebedor'.
     */
    public void setCdDigitoAgenciaRecebedor(java.lang.String cdDigitoAgenciaRecebedor)
    {
        this._cdDigitoAgenciaRecebedor = cdDigitoAgenciaRecebedor;
    } //-- void setCdDigitoAgenciaRecebedor(java.lang.String) 

    /**
     * Sets the value of field 'cdDigitoContaRecebedor'.
     * 
     * @param cdDigitoContaRecebedor the value of field
     * 'cdDigitoContaRecebedor'.
     */
    public void setCdDigitoContaRecebedor(java.lang.String cdDigitoContaRecebedor)
    {
        this._cdDigitoContaRecebedor = cdDigitoContaRecebedor;
    } //-- void setCdDigitoContaRecebedor(java.lang.String) 

    /**
     * Sets the value of field 'cdTipoCtaRecebedor'.
     * 
     * @param cdTipoCtaRecebedor the value of field
     * 'cdTipoCtaRecebedor'.
     */
    public void setCdTipoCtaRecebedor(int cdTipoCtaRecebedor)
    {
        this._cdTipoCtaRecebedor = cdTipoCtaRecebedor;
        this._has_cdTipoCtaRecebedor = true;
    } //-- void setCdTipoCtaRecebedor(int) 

    /**
     * Sets the value of field 'complementoRecebedor'.
     * 
     * @param complementoRecebedor the value of field
     * 'complementoRecebedor'.
     */
    public void setComplementoRecebedor(java.lang.String complementoRecebedor)
    {
        this._complementoRecebedor = complementoRecebedor;
    } //-- void setComplementoRecebedor(java.lang.String) 

    /**
     * Sets the value of field 'vlAgendamentoPagamento'.
     * 
     * @param vlAgendamentoPagamento the value of field
     * 'vlAgendamentoPagamento'.
     */
    public void setVlAgendamentoPagamento(java.math.BigDecimal vlAgendamentoPagamento)
    {
        this._vlAgendamentoPagamento = vlAgendamentoPagamento;
    } //-- void setVlAgendamentoPagamento(java.math.BigDecimal) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listarpostergarconsolidadosolicitacao.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listarpostergarconsolidadosolicitacao.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listarpostergarconsolidadosolicitacao.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarpostergarconsolidadosolicitacao.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
