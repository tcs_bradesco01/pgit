package br.com.bradesco.web.pgit.service.business.mantercontrato.bean;

import java.util.ArrayList;
import java.util.List;

/**
 * Nome: IncluirRepresentanteEntradaDTO
 * <p>
 * Propůsito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class IncluirRepresentanteEntradaDTO {

	/** Atributo cdpessoaJuridicaContrato. */
	private Long cdpessoaJuridicaContrato;
	
	/** Atributo cdTipoContratoNegocio. */
	private Integer cdTipoContratoNegocio;
	
	/** Atributo nrSequenciaContratoNegocio. */
	private Long nrSequenciaContratoNegocio;
	
	/** Atributo nrManutencaoContratoNegocio. */
	private Long nrManutencaoContratoNegocio;
	
	/** Atributo cdProdutoServicoOperacao. */
	private Integer cdProdutoServicoOperacao;
	
	/** Atributo cdProdutoOperacaoRelacionado. */
	private Integer cdProdutoOperacaoRelacionado;
	
	/** Atributo cdRelacionamentoProdutoProduto. */
	private Integer cdRelacionamentoProdutoProduto;
	
	/** Atributo cdFinalidadeItemAditivo. */
	private Integer cdFinalidadeItemAditivo;
	
	/** Atributo cdEmpresaOperante. */
	private Long cdEmpresaOperante;
	
	/** Atributo cdDependenciaOperante. */
	private Integer cdDependenciaOperante;
	
	/** Atributo cdUsuario. */
	private String cdUsuario;
	
	/** Atributo nmOperacaoFluxo. */
	private String nmOperacaoFluxo;
	
	/** Atributo cdCanal. */
	private Integer cdCanal;
	
	/** Atributo cdMidiaComprovanteCorrentista. */
	private Integer cdMidiaComprovanteCorrentista;
	
	/** Atributo dsMidiaComprovanteCorrentista. */
	private String dsMidiaComprovanteCorrentista;
	
	/** Atributo qtFuncionarioEmpresaPagadora. */
	private Long qtFuncionarioEmpresaPagadora;
	
	/** Atributo qtViaComprovante. */
	private Integer qtViaComprovante;
	
	/** Atributo qtViaCobradaComprovante. */
	private Integer qtViaCobradaComprovante;
	
	/** Atributo dsViaCobradaComprovante. */
	private String dsViaCobradaComprovante;

	/** Atributo cdIndicadorFormaContratacao. */
	private Integer cdIndicadorFormaContratacao;
	
	/** Atributo qtdeTotalItens. */
	private Integer qtdeTotalItens;
	
	/** Atributo ocorrencias. */
	private List<ListarRepresentanteOcorrenciaDTO> ocorrencias = new ArrayList<ListarRepresentanteOcorrenciaDTO>();

	/**
	 * @return the cdpessoaJuridicaContrato
	 */
	public Long getCdpessoaJuridicaContrato() {
		return cdpessoaJuridicaContrato;
	}

	/**
	 * @param cdpessoaJuridicaContrato the cdpessoaJuridicaContrato to set
	 */
	public void setCdpessoaJuridicaContrato(Long cdpessoaJuridicaContrato) {
		this.cdpessoaJuridicaContrato = cdpessoaJuridicaContrato;
	}

	/**
	 * @return the cdTipoContratoNegocio
	 */
	public Integer getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}

	/**
	 * @param cdTipoContratoNegocio the cdTipoContratoNegocio to set
	 */
	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}

	/**
	 * @return the nrSequenciaContratoNegocio
	 */
	public Long getNrSequenciaContratoNegocio() {
		return nrSequenciaContratoNegocio;
	}

	/**
	 * @param nrSequenciaContratoNegocio the nrSequenciaContratoNegocio to set
	 */
	public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}

	/**
	 * @return the nrManutencaoContratoNegocio
	 */
	public Long getNrManutencaoContratoNegocio() {
		return nrManutencaoContratoNegocio;
	}

	/**
	 * @param nrManutencaoContratoNegocio the nrManutencaoContratoNegocio to set
	 */
	public void setNrManutencaoContratoNegocio(Long nrManutencaoContratoNegocio) {
		this.nrManutencaoContratoNegocio = nrManutencaoContratoNegocio;
	}

	/**
	 * @return the cdProdutoServicoOperacao
	 */
	public Integer getCdProdutoServicoOperacao() {
		return cdProdutoServicoOperacao;
	}

	/**
	 * @param cdProdutoServicoOperacao the cdProdutoServicoOperacao to set
	 */
	public void setCdProdutoServicoOperacao(Integer cdProdutoServicoOperacao) {
		this.cdProdutoServicoOperacao = cdProdutoServicoOperacao;
	}

	/**
	 * @return the cdProdutoOperacaoRelacionado
	 */
	public Integer getCdProdutoOperacaoRelacionado() {
		return cdProdutoOperacaoRelacionado;
	}

	/**
	 * @param cdProdutoOperacaoRelacionado the cdProdutoOperacaoRelacionado to set
	 */
	public void setCdProdutoOperacaoRelacionado(Integer cdProdutoOperacaoRelacionado) {
		this.cdProdutoOperacaoRelacionado = cdProdutoOperacaoRelacionado;
	}

	/**
	 * @return the cdRelacionamentoProdutoProduto
	 */
	public Integer getCdRelacionamentoProdutoProduto() {
		return cdRelacionamentoProdutoProduto;
	}

	/**
	 * @param cdRelacionamentoProdutoProduto the cdRelacionamentoProdutoProduto to set
	 */
	public void setCdRelacionamentoProdutoProduto(
			Integer cdRelacionamentoProdutoProduto) {
		this.cdRelacionamentoProdutoProduto = cdRelacionamentoProdutoProduto;
	}

	/**
	 * @return the cdFinalidadeItemAditivo
	 */
	public Integer getCdFinalidadeItemAditivo() {
		return cdFinalidadeItemAditivo;
	}

	/**
	 * @param cdFinalidadeItemAditivo the cdFinalidadeItemAditivo to set
	 */
	public void setCdFinalidadeItemAditivo(Integer cdFinalidadeItemAditivo) {
		this.cdFinalidadeItemAditivo = cdFinalidadeItemAditivo;
	}

	/**
	 * @return the cdEmpresaOperante
	 */
	public Long getCdEmpresaOperante() {
		return cdEmpresaOperante;
	}

	/**
	 * @param cdEmpresaOperante the cdEmpresaOperante to set
	 */
	public void setCdEmpresaOperante(Long cdEmpresaOperante) {
		this.cdEmpresaOperante = cdEmpresaOperante;
	}

	/**
	 * @return the cdDependenciaOperante
	 */
	public Integer getCdDependenciaOperante() {
		return cdDependenciaOperante;
	}

	/**
	 * @param cdDependenciaOperante the cdDependenciaOperante to set
	 */
	public void setCdDependenciaOperante(Integer cdDependenciaOperante) {
		this.cdDependenciaOperante = cdDependenciaOperante;
	}

	/**
	 * @return the cdUsuario
	 */
	public String getCdUsuario() {
		return cdUsuario;
	}

	/**
	 * @param cdUsuario the cdUsuario to set
	 */
	public void setCdUsuario(String cdUsuario) {
		this.cdUsuario = cdUsuario;
	}

	/**
	 * @return the nmOperacaoFluxo
	 */
	public String getNmOperacaoFluxo() {
		return nmOperacaoFluxo;
	}

	/**
	 * @param nmOperacaoFluxo the nmOperacaoFluxo to set
	 */
	public void setNmOperacaoFluxo(String nmOperacaoFluxo) {
		this.nmOperacaoFluxo = nmOperacaoFluxo;
	}

	/**
	 * @return the cdCanal
	 */
	public Integer getCdCanal() {
		return cdCanal;
	}

	/**
	 * @param cdCanal the cdCanal to set
	 */
	public void setCdCanal(Integer cdCanal) {
		this.cdCanal = cdCanal;
	}

	/**
	 * @return the cdMidiaComprovanteCorrentista
	 */
	public Integer getCdMidiaComprovanteCorrentista() {
		return cdMidiaComprovanteCorrentista;
	}

	/**
	 * @param cdMidiaComprovanteCorrentista the cdMidiaComprovanteCorrentista to set
	 */
	public void setCdMidiaComprovanteCorrentista(
			Integer cdMidiaComprovanteCorrentista) {
		this.cdMidiaComprovanteCorrentista = cdMidiaComprovanteCorrentista;
	}

	/**
	 * @return the dsMidiaComprovanteCorrentista
	 */
	public String getDsMidiaComprovanteCorrentista() {
		return dsMidiaComprovanteCorrentista;
	}

	/**
	 * @param dsMidiaComprovanteCorrentista the dsMidiaComprovanteCorrentista to set
	 */
	public void setDsMidiaComprovanteCorrentista(
			String dsMidiaComprovanteCorrentista) {
		this.dsMidiaComprovanteCorrentista = dsMidiaComprovanteCorrentista;
	}

	/**
	 * @return the qtFuncionarioEmpresaPagadora
	 */
	public Long getQtFuncionarioEmpresaPagadora() {
		return qtFuncionarioEmpresaPagadora;
	}

	/**
	 * @param qtFuncionarioEmpresaPagadora the qtFuncionarioEmpresaPagadora to set
	 */
	public void setQtFuncionarioEmpresaPagadora(Long qtFuncionarioEmpresaPagadora) {
		this.qtFuncionarioEmpresaPagadora = qtFuncionarioEmpresaPagadora;
	}

	/**
	 * @return the qtViaComprovante
	 */
	public Integer getQtViaComprovante() {
		return qtViaComprovante;
	}

	/**
	 * @param qtViaComprovante the qtViaComprovante to set
	 */
	public void setQtViaComprovante(Integer qtViaComprovante) {
		this.qtViaComprovante = qtViaComprovante;
	}

	/**
	 * @return the qtViaCobradaComprovante
	 */
	public Integer getQtViaCobradaComprovante() {
		return qtViaCobradaComprovante;
	}

	/**
	 * @param qtViaCobradaComprovante the qtViaCobradaComprovante to set
	 */
	public void setQtViaCobradaComprovante(Integer qtViaCobradaComprovante) {
		this.qtViaCobradaComprovante = qtViaCobradaComprovante;
	}

	/**
	 * @return the dsViaCobradaComprovante
	 */
	public String getDsViaCobradaComprovante() {
		return dsViaCobradaComprovante;
	}

	/**
	 * @param dsViaCobradaComprovante the dsViaCobradaComprovante to set
	 */
	public void setDsViaCobradaComprovante(String dsViaCobradaComprovante) {
		this.dsViaCobradaComprovante = dsViaCobradaComprovante;
	}

	/**
	 * @return the cdIndicadorFormaContratacao
	 */
	public Integer getCdIndicadorFormaContratacao() {
		return cdIndicadorFormaContratacao;
	}

	/**
	 * @param cdIndicadorFormaContratacao the cdIndicadorFormaContratacao to set
	 */
	public void setCdIndicadorFormaContratacao(Integer cdIndicadorFormaContratacao) {
		this.cdIndicadorFormaContratacao = cdIndicadorFormaContratacao;
	}

	/**
	 * @return the qtdeTotalItens
	 */
	public Integer getQtdeTotalItens() {
		return qtdeTotalItens;
	}

	/**
	 * @param qtdeTotalItens the qtdeTotalItens to set
	 */
	public void setQtdeTotalItens(Integer qtdeTotalItens) {
		this.qtdeTotalItens = qtdeTotalItens;
	}

	/**
	 * @return the ocorrencias
	 */
	public List<ListarRepresentanteOcorrenciaDTO> getOcorrencias() {
		return ocorrencias;
	}

	/**
	 * @param ocorrencias the ocorrencias to set
	 */
	public void setOcorrencias(List<ListarRepresentanteOcorrenciaDTO> ocorrencias) {
		this.ocorrencias = ocorrencias;
	}
}
