/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.alterarcondreajustetarifa.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/


import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;


/**
 * Class AlterarCondReajusteTarifaRequest.
 * 
 * @version $Revision$ $Date$
 */
public class AlterarCondReajusteTarifaRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdPessoaJuridicaNegocio
     */
    private long _cdPessoaJuridicaNegocio = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridicaNegocio
     */
    private boolean _has_cdPessoaJuridicaNegocio;

    /**
     * Field _cdTipoContratoNegocio
     */
    private int _cdTipoContratoNegocio = 0;

    /**
     * keeps track of state for field: _cdTipoContratoNegocio
     */
    private boolean _has_cdTipoContratoNegocio;

    /**
     * Field _nrSequenciaContratoNegocio
     */
    private long _nrSequenciaContratoNegocio = 0;

    /**
     * keeps track of state for field: _nrSequenciaContratoNegocio
     */
    private boolean _has_nrSequenciaContratoNegocio;

    /**
     * Field _cdServico
     */
    private int _cdServico = 0;

    /**
     * keeps track of state for field: _cdServico
     */
    private boolean _has_cdServico;

    /**
     * Field _cdModalidade
     */
    private int _cdModalidade = 0;

    /**
     * keeps track of state for field: _cdModalidade
     */
    private boolean _has_cdModalidade;

    /**
     * Field _cdRelacionamentoProduto
     */
    private int _cdRelacionamentoProduto = 0;

    /**
     * keeps track of state for field: _cdRelacionamentoProduto
     */
    private boolean _has_cdRelacionamentoProduto;

    /**
     * Field _cdTipoReajusteTarifa
     */
    private int _cdTipoReajusteTarifa = 0;

    /**
     * keeps track of state for field: _cdTipoReajusteTarifa
     */
    private boolean _has_cdTipoReajusteTarifa;

    /**
     * Field _qtMesReajusteTarifa
     */
    private int _qtMesReajusteTarifa = 0;

    /**
     * keeps track of state for field: _qtMesReajusteTarifa
     */
    private boolean _has_qtMesReajusteTarifa;

    /**
     * Field _cdIndicadorEconomicoReajuste
     */
    private int _cdIndicadorEconomicoReajuste = 0;

    /**
     * keeps track of state for field: _cdIndicadorEconomicoReajuste
     */
    private boolean _has_cdIndicadorEconomicoReajuste;

    /**
     * Field _cdPercentualReajusteTarifa
     */
    private java.math.BigDecimal _cdPercentualReajusteTarifa = new java.math.BigDecimal("0");

    /**
     * Field _cdPercentualTarifaCatalogada
     */
    private java.math.BigDecimal _cdPercentualTarifaCatalogada = new java.math.BigDecimal("0");


      //----------------/
     //- Constructors -/
    //----------------/

    public AlterarCondReajusteTarifaRequest() 
     {
        super();
        setCdPercentualReajusteTarifa(new java.math.BigDecimal("0"));
        setCdPercentualTarifaCatalogada(new java.math.BigDecimal("0"));
    } //-- br.com.bradesco.web.pgit.service.data.pdc.alterarcondreajustetarifa.request.AlterarCondReajusteTarifaRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdIndicadorEconomicoReajuste
     * 
     */
    public void deleteCdIndicadorEconomicoReajuste()
    {
        this._has_cdIndicadorEconomicoReajuste= false;
    } //-- void deleteCdIndicadorEconomicoReajuste() 

    /**
     * Method deleteCdModalidade
     * 
     */
    public void deleteCdModalidade()
    {
        this._has_cdModalidade= false;
    } //-- void deleteCdModalidade() 

    /**
     * Method deleteCdPessoaJuridicaNegocio
     * 
     */
    public void deleteCdPessoaJuridicaNegocio()
    {
        this._has_cdPessoaJuridicaNegocio= false;
    } //-- void deleteCdPessoaJuridicaNegocio() 

    /**
     * Method deleteCdRelacionamentoProduto
     * 
     */
    public void deleteCdRelacionamentoProduto()
    {
        this._has_cdRelacionamentoProduto= false;
    } //-- void deleteCdRelacionamentoProduto() 

    /**
     * Method deleteCdServico
     * 
     */
    public void deleteCdServico()
    {
        this._has_cdServico= false;
    } //-- void deleteCdServico() 

    /**
     * Method deleteCdTipoContratoNegocio
     * 
     */
    public void deleteCdTipoContratoNegocio()
    {
        this._has_cdTipoContratoNegocio= false;
    } //-- void deleteCdTipoContratoNegocio() 

    /**
     * Method deleteCdTipoReajusteTarifa
     * 
     */
    public void deleteCdTipoReajusteTarifa()
    {
        this._has_cdTipoReajusteTarifa= false;
    } //-- void deleteCdTipoReajusteTarifa() 

    /**
     * Method deleteNrSequenciaContratoNegocio
     * 
     */
    public void deleteNrSequenciaContratoNegocio()
    {
        this._has_nrSequenciaContratoNegocio= false;
    } //-- void deleteNrSequenciaContratoNegocio() 

    /**
     * Method deleteQtMesReajusteTarifa
     * 
     */
    public void deleteQtMesReajusteTarifa()
    {
        this._has_qtMesReajusteTarifa= false;
    } //-- void deleteQtMesReajusteTarifa() 

    /**
     * Returns the value of field 'cdIndicadorEconomicoReajuste'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorEconomicoReajuste'.
     */
    public int getCdIndicadorEconomicoReajuste()
    {
        return this._cdIndicadorEconomicoReajuste;
    } //-- int getCdIndicadorEconomicoReajuste() 

    /**
     * Returns the value of field 'cdModalidade'.
     * 
     * @return int
     * @return the value of field 'cdModalidade'.
     */
    public int getCdModalidade()
    {
        return this._cdModalidade;
    } //-- int getCdModalidade() 

    /**
     * Returns the value of field 'cdPercentualReajusteTarifa'.
     * 
     * @return BigDecimal
     * @return the value of field 'cdPercentualReajusteTarifa'.
     */
    public java.math.BigDecimal getCdPercentualReajusteTarifa()
    {
        return this._cdPercentualReajusteTarifa;
    } //-- java.math.BigDecimal getCdPercentualReajusteTarifa() 

    /**
     * Returns the value of field 'cdPercentualTarifaCatalogada'.
     * 
     * @return BigDecimal
     * @return the value of field 'cdPercentualTarifaCatalogada'.
     */
    public java.math.BigDecimal getCdPercentualTarifaCatalogada()
    {
        return this._cdPercentualTarifaCatalogada;
    } //-- java.math.BigDecimal getCdPercentualTarifaCatalogada() 

    /**
     * Returns the value of field 'cdPessoaJuridicaNegocio'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridicaNegocio'.
     */
    public long getCdPessoaJuridicaNegocio()
    {
        return this._cdPessoaJuridicaNegocio;
    } //-- long getCdPessoaJuridicaNegocio() 

    /**
     * Returns the value of field 'cdRelacionamentoProduto'.
     * 
     * @return int
     * @return the value of field 'cdRelacionamentoProduto'.
     */
    public int getCdRelacionamentoProduto()
    {
        return this._cdRelacionamentoProduto;
    } //-- int getCdRelacionamentoProduto() 

    /**
     * Returns the value of field 'cdServico'.
     * 
     * @return int
     * @return the value of field 'cdServico'.
     */
    public int getCdServico()
    {
        return this._cdServico;
    } //-- int getCdServico() 

    /**
     * Returns the value of field 'cdTipoContratoNegocio'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoNegocio'.
     */
    public int getCdTipoContratoNegocio()
    {
        return this._cdTipoContratoNegocio;
    } //-- int getCdTipoContratoNegocio() 

    /**
     * Returns the value of field 'cdTipoReajusteTarifa'.
     * 
     * @return int
     * @return the value of field 'cdTipoReajusteTarifa'.
     */
    public int getCdTipoReajusteTarifa()
    {
        return this._cdTipoReajusteTarifa;
    } //-- int getCdTipoReajusteTarifa() 

    /**
     * Returns the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @return long
     * @return the value of field 'nrSequenciaContratoNegocio'.
     */
    public long getNrSequenciaContratoNegocio()
    {
        return this._nrSequenciaContratoNegocio;
    } //-- long getNrSequenciaContratoNegocio() 

    /**
     * Returns the value of field 'qtMesReajusteTarifa'.
     * 
     * @return int
     * @return the value of field 'qtMesReajusteTarifa'.
     */
    public int getQtMesReajusteTarifa()
    {
        return this._qtMesReajusteTarifa;
    } //-- int getQtMesReajusteTarifa() 

    /**
     * Method hasCdIndicadorEconomicoReajuste
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorEconomicoReajuste()
    {
        return this._has_cdIndicadorEconomicoReajuste;
    } //-- boolean hasCdIndicadorEconomicoReajuste() 

    /**
     * Method hasCdModalidade
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdModalidade()
    {
        return this._has_cdModalidade;
    } //-- boolean hasCdModalidade() 

    /**
     * Method hasCdPessoaJuridicaNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridicaNegocio()
    {
        return this._has_cdPessoaJuridicaNegocio;
    } //-- boolean hasCdPessoaJuridicaNegocio() 

    /**
     * Method hasCdRelacionamentoProduto
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdRelacionamentoProduto()
    {
        return this._has_cdRelacionamentoProduto;
    } //-- boolean hasCdRelacionamentoProduto() 

    /**
     * Method hasCdServico
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdServico()
    {
        return this._has_cdServico;
    } //-- boolean hasCdServico() 

    /**
     * Method hasCdTipoContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoNegocio()
    {
        return this._has_cdTipoContratoNegocio;
    } //-- boolean hasCdTipoContratoNegocio() 

    /**
     * Method hasCdTipoReajusteTarifa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoReajusteTarifa()
    {
        return this._has_cdTipoReajusteTarifa;
    } //-- boolean hasCdTipoReajusteTarifa() 

    /**
     * Method hasNrSequenciaContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaContratoNegocio()
    {
        return this._has_nrSequenciaContratoNegocio;
    } //-- boolean hasNrSequenciaContratoNegocio() 

    /**
     * Method hasQtMesReajusteTarifa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtMesReajusteTarifa()
    {
        return this._has_qtMesReajusteTarifa;
    } //-- boolean hasQtMesReajusteTarifa() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdIndicadorEconomicoReajuste'.
     * 
     * @param cdIndicadorEconomicoReajuste the value of field
     * 'cdIndicadorEconomicoReajuste'.
     */
    public void setCdIndicadorEconomicoReajuste(int cdIndicadorEconomicoReajuste)
    {
        this._cdIndicadorEconomicoReajuste = cdIndicadorEconomicoReajuste;
        this._has_cdIndicadorEconomicoReajuste = true;
    } //-- void setCdIndicadorEconomicoReajuste(int) 

    /**
     * Sets the value of field 'cdModalidade'.
     * 
     * @param cdModalidade the value of field 'cdModalidade'.
     */
    public void setCdModalidade(int cdModalidade)
    {
        this._cdModalidade = cdModalidade;
        this._has_cdModalidade = true;
    } //-- void setCdModalidade(int) 

    /**
     * Sets the value of field 'cdPercentualReajusteTarifa'.
     * 
     * @param cdPercentualReajusteTarifa the value of field
     * 'cdPercentualReajusteTarifa'.
     */
    public void setCdPercentualReajusteTarifa(java.math.BigDecimal cdPercentualReajusteTarifa)
    {
        this._cdPercentualReajusteTarifa = cdPercentualReajusteTarifa;
    } //-- void setCdPercentualReajusteTarifa(java.math.BigDecimal) 

    /**
     * Sets the value of field 'cdPercentualTarifaCatalogada'.
     * 
     * @param cdPercentualTarifaCatalogada the value of field
     * 'cdPercentualTarifaCatalogada'.
     */
    public void setCdPercentualTarifaCatalogada(java.math.BigDecimal cdPercentualTarifaCatalogada)
    {
        this._cdPercentualTarifaCatalogada = cdPercentualTarifaCatalogada;
    } //-- void setCdPercentualTarifaCatalogada(java.math.BigDecimal) 

    /**
     * Sets the value of field 'cdPessoaJuridicaNegocio'.
     * 
     * @param cdPessoaJuridicaNegocio the value of field
     * 'cdPessoaJuridicaNegocio'.
     */
    public void setCdPessoaJuridicaNegocio(long cdPessoaJuridicaNegocio)
    {
        this._cdPessoaJuridicaNegocio = cdPessoaJuridicaNegocio;
        this._has_cdPessoaJuridicaNegocio = true;
    } //-- void setCdPessoaJuridicaNegocio(long) 

    /**
     * Sets the value of field 'cdRelacionamentoProduto'.
     * 
     * @param cdRelacionamentoProduto the value of field
     * 'cdRelacionamentoProduto'.
     */
    public void setCdRelacionamentoProduto(int cdRelacionamentoProduto)
    {
        this._cdRelacionamentoProduto = cdRelacionamentoProduto;
        this._has_cdRelacionamentoProduto = true;
    } //-- void setCdRelacionamentoProduto(int) 

    /**
     * Sets the value of field 'cdServico'.
     * 
     * @param cdServico the value of field 'cdServico'.
     */
    public void setCdServico(int cdServico)
    {
        this._cdServico = cdServico;
        this._has_cdServico = true;
    } //-- void setCdServico(int) 

    /**
     * Sets the value of field 'cdTipoContratoNegocio'.
     * 
     * @param cdTipoContratoNegocio the value of field
     * 'cdTipoContratoNegocio'.
     */
    public void setCdTipoContratoNegocio(int cdTipoContratoNegocio)
    {
        this._cdTipoContratoNegocio = cdTipoContratoNegocio;
        this._has_cdTipoContratoNegocio = true;
    } //-- void setCdTipoContratoNegocio(int) 

    /**
     * Sets the value of field 'cdTipoReajusteTarifa'.
     * 
     * @param cdTipoReajusteTarifa the value of field
     * 'cdTipoReajusteTarifa'.
     */
    public void setCdTipoReajusteTarifa(int cdTipoReajusteTarifa)
    {
        this._cdTipoReajusteTarifa = cdTipoReajusteTarifa;
        this._has_cdTipoReajusteTarifa = true;
    } //-- void setCdTipoReajusteTarifa(int) 

    /**
     * Sets the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @param nrSequenciaContratoNegocio the value of field
     * 'nrSequenciaContratoNegocio'.
     */
    public void setNrSequenciaContratoNegocio(long nrSequenciaContratoNegocio)
    {
        this._nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
        this._has_nrSequenciaContratoNegocio = true;
    } //-- void setNrSequenciaContratoNegocio(long) 

    /**
     * Sets the value of field 'qtMesReajusteTarifa'.
     * 
     * @param qtMesReajusteTarifa the value of field
     * 'qtMesReajusteTarifa'.
     */
    public void setQtMesReajusteTarifa(int qtMesReajusteTarifa)
    {
        this._qtMesReajusteTarifa = qtMesReajusteTarifa;
        this._has_qtMesReajusteTarifa = true;
    } //-- void setQtMesReajusteTarifa(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return AlterarCondReajusteTarifaRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.alterarcondreajustetarifa.request.AlterarCondReajusteTarifaRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.alterarcondreajustetarifa.request.AlterarCondReajusteTarifaRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.alterarcondreajustetarifa.request.AlterarCondReajusteTarifaRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.alterarcondreajustetarifa.request.AlterarCondReajusteTarifaRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
