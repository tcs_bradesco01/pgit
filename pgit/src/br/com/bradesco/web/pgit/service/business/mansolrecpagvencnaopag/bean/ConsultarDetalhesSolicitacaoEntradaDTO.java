/*
 * Nome: br.com.bradesco.web.pgit.service.business.mansolrecpagvencnaopag.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mansolrecpagvencnaopag.bean;

/**
 * Nome: ConsultarDetalhesSolicitacaoEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarDetalhesSolicitacaoEntradaDTO{
	
	/** Atributo cdSolicitacaoPagamento. */
	private Integer cdSolicitacaoPagamento;
	
	/** Atributo nrSolicitacaoPagamento. */
	private Integer nrSolicitacaoPagamento;
	
	/** Atributo dsObservacao. */
	private String dsObservacao;
	
	/** Atributo cdTipoAcao. */
	private Integer cdTipoAcao;

	/**
	 * Get: cdTipoAcao.
	 *
	 * @return cdTipoAcao
	 */
	public Integer getCdTipoAcao() {
	    return cdTipoAcao;
	}

	/**
	 * Set: cdTipoAcao.
	 *
	 * @param cdTipoAcao the cd tipo acao
	 */
	public void setCdTipoAcao(Integer cdTipoAcao) {
	    this.cdTipoAcao = cdTipoAcao;
	}

	/**
	 * Get: cdSolicitacaoPagamento.
	 *
	 * @return cdSolicitacaoPagamento
	 */
	public Integer getCdSolicitacaoPagamento(){
		return cdSolicitacaoPagamento;
	}

	/**
	 * Set: cdSolicitacaoPagamento.
	 *
	 * @param cdSolicitacaoPagamento the cd solicitacao pagamento
	 */
	public void setCdSolicitacaoPagamento(Integer cdSolicitacaoPagamento){
		this.cdSolicitacaoPagamento = cdSolicitacaoPagamento;
	}

	/**
	 * Get: nrSolicitacaoPagamento.
	 *
	 * @return nrSolicitacaoPagamento
	 */
	public Integer getNrSolicitacaoPagamento(){
		return nrSolicitacaoPagamento;
	}

	/**
	 * Set: nrSolicitacaoPagamento.
	 *
	 * @param nrSolicitacaoPagamento the nr solicitacao pagamento
	 */
	public void setNrSolicitacaoPagamento(Integer nrSolicitacaoPagamento){
		this.nrSolicitacaoPagamento = nrSolicitacaoPagamento;
	}

	/**
	 * Get: dsObservacao.
	 *
	 * @return dsObservacao
	 */
	public String getDsObservacao(){
		return dsObservacao;
	}

	/**
	 * Set: dsObservacao.
	 *
	 * @param dsObservacao the ds observacao
	 */
	public void setDsObservacao(String dsObservacao){
		this.dsObservacao = dsObservacao;
	}
}