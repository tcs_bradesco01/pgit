/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias1.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias1 implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdIndicadorModalidadeTributos
     */
    private int _cdIndicadorModalidadeTributos = 0;

    /**
     * keeps track of state for field: _cdIndicadorModalidadeTributo
     */
    private boolean _has_cdIndicadorModalidadeTributos;

    /**
     * Field _cdIndicadorTributoContrato
     */
    private java.lang.String _cdIndicadorTributoContrato;

    /**
     * Field _cdProdutoRelacionadoTributo
     */
    private int _cdProdutoRelacionadoTributo = 0;

    /**
     * keeps track of state for field: _cdProdutoRelacionadoTributo
     */
    private boolean _has_cdProdutoRelacionadoTributo;

    /**
     * Field _dsProdutoRelacionadoTributo
     */
    private java.lang.String _dsProdutoRelacionadoTributo;

    /**
     * Field _cdMomentoProcessamentoTributos
     */
    private int _cdMomentoProcessamentoTributos = 0;

    /**
     * keeps track of state for field:
     * _cdMomentoProcessamentoTributos
     */
    private boolean _has_cdMomentoProcessamentoTributos;

    /**
     * Field _dsMomentoProcessamentoTributos
     */
    private java.lang.String _dsMomentoProcessamentoTributos;

    /**
     * Field _cdIndicadorFeriadoTributos
     */
    private int _cdIndicadorFeriadoTributos = 0;

    /**
     * keeps track of state for field: _cdIndicadorFeriadoTributos
     */
    private boolean _has_cdIndicadorFeriadoTributos;

    /**
     * Field _dsIndicadorFeriadoTributos
     */
    private java.lang.String _dsIndicadorFeriadoTributos;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias1() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias1()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdIndicadorFeriadoTributos
     * 
     */
    public void deleteCdIndicadorFeriadoTributos()
    {
        this._has_cdIndicadorFeriadoTributos= false;
    } //-- void deleteCdIndicadorFeriadoTributos() 

    /**
     * Method deleteCdIndicadorModalidadeTributos
     * 
     */
    public void deleteCdIndicadorModalidadeTributos()
    {
        this._has_cdIndicadorModalidadeTributos= false;
    } //-- void deleteCdIndicadorModalidadeTributos() 

    /**
     * Method deleteCdMomentoProcessamentoTributos
     * 
     */
    public void deleteCdMomentoProcessamentoTributos()
    {
        this._has_cdMomentoProcessamentoTributos= false;
    } //-- void deleteCdMomentoProcessamentoTributos() 

    /**
     * Method deleteCdProdutoRelacionadoTributo
     * 
     */
    public void deleteCdProdutoRelacionadoTributo()
    {
        this._has_cdProdutoRelacionadoTributo= false;
    } //-- void deleteCdProdutoRelacionadoTributo() 

    /**
     * Returns the value of field 'cdIndicadorFeriadoTributos'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorFeriadoTributos'.
     */
    public int getCdIndicadorFeriadoTributos()
    {
        return this._cdIndicadorFeriadoTributos;
    } //-- int getCdIndicadorFeriadoTributos() 

    /**
     * Returns the value of field 'cdIndicadorModalidadeTributos'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorModalidadeTributos'.
     */
    public int getCdIndicadorModalidadeTributos()
    {
        return this._cdIndicadorModalidadeTributos;
    } //-- int getCdIndicadorModalidadeTributos() 

    /**
     * Returns the value of field 'cdIndicadorTributoContrato'.
     * 
     * @return String
     * @return the value of field 'cdIndicadorTributoContrato'.
     */
    public java.lang.String getCdIndicadorTributoContrato()
    {
        return this._cdIndicadorTributoContrato;
    } //-- java.lang.String getCdIndicadorTributoContrato() 

    /**
     * Returns the value of field 'cdMomentoProcessamentoTributos'.
     * 
     * @return int
     * @return the value of field 'cdMomentoProcessamentoTributos'.
     */
    public int getCdMomentoProcessamentoTributos()
    {
        return this._cdMomentoProcessamentoTributos;
    } //-- int getCdMomentoProcessamentoTributos() 

    /**
     * Returns the value of field 'cdProdutoRelacionadoTributo'.
     * 
     * @return int
     * @return the value of field 'cdProdutoRelacionadoTributo'.
     */
    public int getCdProdutoRelacionadoTributo()
    {
        return this._cdProdutoRelacionadoTributo;
    } //-- int getCdProdutoRelacionadoTributo() 

    /**
     * Returns the value of field 'dsIndicadorFeriadoTributos'.
     * 
     * @return String
     * @return the value of field 'dsIndicadorFeriadoTributos'.
     */
    public java.lang.String getDsIndicadorFeriadoTributos()
    {
        return this._dsIndicadorFeriadoTributos;
    } //-- java.lang.String getDsIndicadorFeriadoTributos() 

    /**
     * Returns the value of field 'dsMomentoProcessamentoTributos'.
     * 
     * @return String
     * @return the value of field 'dsMomentoProcessamentoTributos'.
     */
    public java.lang.String getDsMomentoProcessamentoTributos()
    {
        return this._dsMomentoProcessamentoTributos;
    } //-- java.lang.String getDsMomentoProcessamentoTributos() 

    /**
     * Returns the value of field 'dsProdutoRelacionadoTributo'.
     * 
     * @return String
     * @return the value of field 'dsProdutoRelacionadoTributo'.
     */
    public java.lang.String getDsProdutoRelacionadoTributo()
    {
        return this._dsProdutoRelacionadoTributo;
    } //-- java.lang.String getDsProdutoRelacionadoTributo() 

    /**
     * Method hasCdIndicadorFeriadoTributos
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorFeriadoTributos()
    {
        return this._has_cdIndicadorFeriadoTributos;
    } //-- boolean hasCdIndicadorFeriadoTributos() 

    /**
     * Method hasCdIndicadorModalidadeTributos
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorModalidadeTributos()
    {
        return this._has_cdIndicadorModalidadeTributos;
    } //-- boolean hasCdIndicadorModalidadeTributos() 

    /**
     * Method hasCdMomentoProcessamentoTributos
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMomentoProcessamentoTributos()
    {
        return this._has_cdMomentoProcessamentoTributos;
    } //-- boolean hasCdMomentoProcessamentoTributos() 

    /**
     * Method hasCdProdutoRelacionadoTributo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdutoRelacionadoTributo()
    {
        return this._has_cdProdutoRelacionadoTributo;
    } //-- boolean hasCdProdutoRelacionadoTributo() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdIndicadorFeriadoTributos'.
     * 
     * @param cdIndicadorFeriadoTributos the value of field
     * 'cdIndicadorFeriadoTributos'.
     */
    public void setCdIndicadorFeriadoTributos(int cdIndicadorFeriadoTributos)
    {
        this._cdIndicadorFeriadoTributos = cdIndicadorFeriadoTributos;
        this._has_cdIndicadorFeriadoTributos = true;
    } //-- void setCdIndicadorFeriadoTributos(int) 

    /**
     * Sets the value of field 'cdIndicadorModalidadeTributos'.
     * 
     * @param cdIndicadorModalidadeTributos the value of field
     * 'cdIndicadorModalidadeTributos'.
     */
    public void setCdIndicadorModalidadeTributos(int cdIndicadorModalidadeTributos)
    {
        this._cdIndicadorModalidadeTributos = cdIndicadorModalidadeTributos;
        this._has_cdIndicadorModalidadeTributos = true;
    } //-- void setCdIndicadorModalidadeTributos(int) 

    /**
     * Sets the value of field 'cdIndicadorTributoContrato'.
     * 
     * @param cdIndicadorTributoContrato the value of field
     * 'cdIndicadorTributoContrato'.
     */
    public void setCdIndicadorTributoContrato(java.lang.String cdIndicadorTributoContrato)
    {
        this._cdIndicadorTributoContrato = cdIndicadorTributoContrato;
    } //-- void setCdIndicadorTributoContrato(java.lang.String) 

    /**
     * Sets the value of field 'cdMomentoProcessamentoTributos'.
     * 
     * @param cdMomentoProcessamentoTributos the value of field
     * 'cdMomentoProcessamentoTributos'.
     */
    public void setCdMomentoProcessamentoTributos(int cdMomentoProcessamentoTributos)
    {
        this._cdMomentoProcessamentoTributos = cdMomentoProcessamentoTributos;
        this._has_cdMomentoProcessamentoTributos = true;
    } //-- void setCdMomentoProcessamentoTributos(int) 

    /**
     * Sets the value of field 'cdProdutoRelacionadoTributo'.
     * 
     * @param cdProdutoRelacionadoTributo the value of field
     * 'cdProdutoRelacionadoTributo'.
     */
    public void setCdProdutoRelacionadoTributo(int cdProdutoRelacionadoTributo)
    {
        this._cdProdutoRelacionadoTributo = cdProdutoRelacionadoTributo;
        this._has_cdProdutoRelacionadoTributo = true;
    } //-- void setCdProdutoRelacionadoTributo(int) 

    /**
     * Sets the value of field 'dsIndicadorFeriadoTributos'.
     * 
     * @param dsIndicadorFeriadoTributos the value of field
     * 'dsIndicadorFeriadoTributos'.
     */
    public void setDsIndicadorFeriadoTributos(java.lang.String dsIndicadorFeriadoTributos)
    {
        this._dsIndicadorFeriadoTributos = dsIndicadorFeriadoTributos;
    } //-- void setDsIndicadorFeriadoTributos(java.lang.String) 

    /**
     * Sets the value of field 'dsMomentoProcessamentoTributos'.
     * 
     * @param dsMomentoProcessamentoTributos the value of field
     * 'dsMomentoProcessamentoTributos'.
     */
    public void setDsMomentoProcessamentoTributos(java.lang.String dsMomentoProcessamentoTributos)
    {
        this._dsMomentoProcessamentoTributos = dsMomentoProcessamentoTributos;
    } //-- void setDsMomentoProcessamentoTributos(java.lang.String) 

    /**
     * Sets the value of field 'dsProdutoRelacionadoTributo'.
     * 
     * @param dsProdutoRelacionadoTributo the value of field
     * 'dsProdutoRelacionadoTributo'.
     */
    public void setDsProdutoRelacionadoTributo(java.lang.String dsProdutoRelacionadoTributo)
    {
        this._dsProdutoRelacionadoTributo = dsProdutoRelacionadoTributo;
    } //-- void setDsProdutoRelacionadoTributo(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias1
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias1 unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias1) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias1.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias1 unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
