/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/implementation.ftl,v $
 * $Id: implementation.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: implementation.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */
 
package br.com.bradesco.web.pgit.service.business.mantersolestornopagto.impl;

import java.util.ArrayList;
import java.util.List;

import br.com.bradesco.web.pgit.service.business.mansolrecpagvencnaopag.bean.ConsultarDetalhesSolicitacaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mansolrecpagvencnaopag.bean.ConsultarDetalhesSolicitacaoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolestornopagto.IManterSolEstornoPagtoService;
import br.com.bradesco.web.pgit.service.business.mantersolestornopagto.IManterSolEstornoPagtoServiceConstants;
import br.com.bradesco.web.pgit.service.business.mantersolestornopagto.bean.AutorizarSolicitacaoEstornoPatosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolestornopagto.bean.AutorizarSolicitacaoEstornoPatosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolestornopagto.bean.ConsultarPagtosSolicitacaoEstornoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolestornopagto.bean.ConsultarPagtosSolicitacaoEstornoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolestornopagto.bean.ConsultarSolicitacaoEstornoPagtosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolestornopagto.bean.ConsultarSolicitacaoEstornoPagtosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolestornopagto.bean.ExcluirSolicitacaoEstornoPagtosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolestornopagto.bean.ExcluirSolicitacaoEstornoPagtosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolestornopagto.bean.OcorrenciasConsultarPagtosSolicitacaoEstornoDTO;
import br.com.bradesco.web.pgit.service.data.pdc.FactoryAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.autorizarsolicitacaoestornopatos.request.AutorizarSolicitacaoEstornoPatosRequest;
import br.com.bradesco.web.pgit.service.data.pdc.autorizarsolicitacaoestornopatos.response.AutorizarSolicitacaoEstornoPatosResponse;
import br.com.bradesco.web.pgit.service.data.pdc.consultardetalhessolicitacao.request.ConsultarDetalhesSolicitacaoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultardetalhessolicitacao.response.ConsultarDetalhesSolicitacaoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.consultarpagtossolicitacaoestorno.request.ConsultarPagtosSolicitacaoEstornoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarpagtossolicitacaoestorno.response.ConsultarPagtosSolicitacaoEstornoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.consultarsolicitacaoestornopagtos.request.ConsultarSolicitacaoEstornoPagtosRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarsolicitacaoestornopagtos.response.ConsultarSolicitacaoEstornoPagtosResponse;
import br.com.bradesco.web.pgit.service.data.pdc.excluirsolicitacaoestornopagtos.request.ExcluirSolicitacaoEstornoPagtosRequest;
import br.com.bradesco.web.pgit.service.data.pdc.excluirsolicitacaoestornopagtos.response.ExcluirSolicitacaoEstornoPagtosResponse;
import br.com.bradesco.web.pgit.utils.CpfCnpjUtils;
import br.com.bradesco.web.pgit.utils.PgitUtil;

/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Implementa��o do adaptador: ManterSolEstornoPagto
 * </p>
 * 
 * @comment C�DIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public class ManterSolEstornoPagtoServiceImpl implements IManterSolEstornoPagtoService {
	
	/** Atributo factoryAdapter. */
	private FactoryAdapter factoryAdapter;
    
	/**
	 * Get: factoryAdapter.
	 *
	 * @return factoryAdapter
	 */
	public FactoryAdapter getFactoryAdapter() {
		return factoryAdapter;
	}
	
	/**
	 * Set: factoryAdapter.
	 *
	 * @param factoryAdapter the factory adapter
	 */
	public void setFactoryAdapter(FactoryAdapter factoryAdapter) {
		this.factoryAdapter = factoryAdapter;
	}
	
	
	
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.mantersolestornopagto.IManterSolEstornoPagtoService#consultarPagtosSolicitacaoEstorno(br.com.bradesco.web.pgit.service.business.mantersolestornopagto.bean.ConsultarPagtosSolicitacaoEstornoEntradaDTO)
	 */
	public ConsultarPagtosSolicitacaoEstornoSaidaDTO consultarPagtosSolicitacaoEstorno(ConsultarPagtosSolicitacaoEstornoEntradaDTO entrada){
		ConsultarPagtosSolicitacaoEstornoSaidaDTO saida = new ConsultarPagtosSolicitacaoEstornoSaidaDTO();
		ConsultarPagtosSolicitacaoEstornoRequest request = new ConsultarPagtosSolicitacaoEstornoRequest();
		ConsultarPagtosSolicitacaoEstornoResponse response = new ConsultarPagtosSolicitacaoEstornoResponse();

		request.setNumeroOcorrencias(IManterSolEstornoPagtoServiceConstants.NUMERO_OCORRENCIAS_CONSULTAR2);
		request.setCdSolicitacaoPagamentoIntegrado(PgitUtil.verificaIntegerNulo(entrada.getCdSolicitacaoPagamentoIntegrado()));
		request.setNrSolicitacaoPagamentoIntegrado(PgitUtil.verificaIntegerNulo(entrada.getNrSolicitacaoPagamentoIntegrado()));

		response = getFactoryAdapter().getConsultarPagtosSolicitacaoEstornoPDCAdapter().invokeProcess(request);

		saida.setCodMensagem(response.getCodMensagem());
		saida.setMensagem(response.getMensagem());
		saida.setNumeroLinhas(response.getNumeroLinhas());

		List<OcorrenciasConsultarPagtosSolicitacaoEstornoDTO> listaOcorrencias = new ArrayList<OcorrenciasConsultarPagtosSolicitacaoEstornoDTO>();
		for(int i=0;i<response.getOcorrenciasCount();i++){
			OcorrenciasConsultarPagtosSolicitacaoEstornoDTO ocorrencias = new OcorrenciasConsultarPagtosSolicitacaoEstornoDTO();
			ocorrencias.setCdCorpoCpfCnpj(response.getOcorrencias(i).getCdCorpoCpfCnpj());
			ocorrencias.setCdFilialCpfCnpj(response.getOcorrencias(i).getCdFilialCpfCnpj());
			ocorrencias.setCdDigitoCpfCnpj(response.getOcorrencias(i).getCdDigitoCpfCnpj());
			ocorrencias.setCdPessoaJuridicaContrato(response.getOcorrencias(i).getCdPessoaJuridicaContrato());
			ocorrencias.setDsRazaoSocial(response.getOcorrencias(i).getDsRazaoSocial());
			ocorrencias.setCdTipoContratoNegocio(response.getOcorrencias(i).getCdTipoContratoNegocio());
			ocorrencias.setNrSequenciaContratoNegocio(response.getOcorrencias(i).getNrSequenciaContratoNegocio());
			ocorrencias.setCdprodutoServicoOperacao(response.getOcorrencias(i).getCdprodutoServicoOperacao());
			ocorrencias.setDsOperacaoProdutoServico(response.getOcorrencias(i).getDsOperacaoProdutoServico());
			ocorrencias.setCdProdutoServicoRelacionado(response.getOcorrencias(i).getCdProdutoServicoRelacionado());
			ocorrencias.setDsOperacaoProdutoRelacionado(response.getOcorrencias(i).getDsOperacaoProdutoRelacionado());
			ocorrencias.setCdControlePagamento(response.getOcorrencias(i).getCdControlePagamento());
			ocorrencias.setDtCredito(response.getOcorrencias(i).getDtCredito());
			ocorrencias.setVlrEfetivacaoPagamento(response.getOcorrencias(i).getVlrEfetivacaoPagamento());
			ocorrencias.setCdInscricaoFavorecido(response.getOcorrencias(i).getCdInscricaoFavorecido());
			ocorrencias.setDsFavorecidoCliente(response.getOcorrencias(i).getDsFavorecidoCliente());
			ocorrencias.setCdBancoDebito(response.getOcorrencias(i).getCdBancoDebito());
			ocorrencias.setCdAgenciaDebito(response.getOcorrencias(i).getCdAgenciaDebito());
			ocorrencias.setCdDigitoAgenciaDebito(response.getOcorrencias(i).getCdDigitoAgenciaDebito());
			ocorrencias.setCdContaDebito(response.getOcorrencias(i).getCdContaDebito());
			ocorrencias.setCdDigitoContaDebito(response.getOcorrencias(i).getCdDigitoContaDebito());
			ocorrencias.setBancoCredito(response.getOcorrencias(i).getBancoCredito());
			ocorrencias.setAgenciaCredito(response.getOcorrencias(i).getAgenciaCredito());
			ocorrencias.setDigitoAgenciaCredito(response.getOcorrencias(i).getDigitoAgenciaCredito());
			ocorrencias.setContaCredito(response.getOcorrencias(i).getContaCredito());
			ocorrencias.setDigitoContaCredito(response.getOcorrencias(i).getDigitoContaCredito());
			ocorrencias.setCdSituacaoOperacaoPagamento(response.getOcorrencias(i).getCdSituacaoOperacaoPagamento());
			ocorrencias.setDsSituacaoOperacaoPagamento(response.getOcorrencias(i).getDsSituacaoOperacaoPagamento());
			ocorrencias.setDtEfetivacao(response.getOcorrencias(i).getDtEfetivacao());
			ocorrencias.setCpfCnpjRecebedor(response.getOcorrencias(i).getCpfCnpjRecebedor());
			ocorrencias.setFilialCpfCnpjRecebedor(response.getOcorrencias(i).getFilialCpfCnpjRecebedor());
			ocorrencias.setDigitoCpfCnpjRecebedor(response.getOcorrencias(i).getDigitoCpfCnpjRecebedor());
			ocorrencias.setCdTipoTela(response.getOcorrencias(i).getCdTipoTela());
			ocorrencias.setCdAgendadosPagosNaoPagos(response.getOcorrencias(i).getCdAgendadosPagosNaoPagos());
			ocorrencias.setFavorecidoBeneficiarioFormatado(PgitUtil.concatenarCampos(ocorrencias.getCdInscricaoFavorecido(), ocorrencias.getDsFavorecidoCliente()));
			ocorrencias.setDsMotivoRecusaEstorno(response.getOcorrencias(i).getDsMotivoRecusaEstorno());
			
			ocorrencias.setAgenciaCreditoFormatada(PgitUtil.formatAgencia(response.getOcorrencias(i).getAgenciaCredito(), response.getOcorrencias(i).getDigitoAgenciaCredito(), false));
			ocorrencias.setContaCreditoFormatada(PgitUtil.formatConta(response.getOcorrencias(i).getContaCredito(), response.getOcorrencias(i).getDigitoContaCredito(), false));
			ocorrencias.setBancoAgenciaContaDebitoFormatado(PgitUtil.formatBancoAgenciaConta(response.getOcorrencias(i).getCdBancoDebito(), response.getOcorrencias(i).getCdAgenciaDebito(), response.getOcorrencias(i).getCdDigitoAgenciaDebito(), response.getOcorrencias(i).getCdContaDebito(), response.getOcorrencias(i).getCdDigitoContaDebito(), false));
			//solicitado via email por AllaAllan Henrique Scarabelot Poletto Enviado: ter�a-feira,05/07/2011, formatar a coluna "Favorecido" com os valores de cr�dito
			ocorrencias.setBancoAgenciaContaFavorecidoFormatada(PgitUtil.formatBancoAgenciaConta(response.getOcorrencias(i).getBancoCredito(), response.getOcorrencias(i).getAgenciaCredito(), response.getOcorrencias(i).getDigitoAgenciaCredito(), response.getOcorrencias(i).getContaCredito(), response.getOcorrencias(i).getDigitoContaCredito(), false));
			ocorrencias.setCpfCnpjFormatado(CpfCnpjUtils.formatCpfCnpjCompleto(response.getOcorrencias(i).getCdCorpoCpfCnpj(), response.getOcorrencias(i).getCdFilialCpfCnpj(), response.getOcorrencias(i).getCdDigitoCpfCnpj()));
			
			listaOcorrencias.add(ocorrencias);
			
			
		}
		saida.setListaOcorrencias(listaOcorrencias);

		return saida;
	}
	
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.mantersolestornopagto.IManterSolEstornoPagtoService#consultarSolicitacaoEstornoPagtos(br.com.bradesco.web.pgit.service.business.mantersolestornopagto.bean.ConsultarSolicitacaoEstornoPagtosEntradaDTO)
	 */
	public List<ConsultarSolicitacaoEstornoPagtosSaidaDTO> consultarSolicitacaoEstornoPagtos (ConsultarSolicitacaoEstornoPagtosEntradaDTO entradaDTO){
		ConsultarSolicitacaoEstornoPagtosRequest request = new ConsultarSolicitacaoEstornoPagtosRequest();
		ConsultarSolicitacaoEstornoPagtosResponse response = new ConsultarSolicitacaoEstornoPagtosResponse();
		List<ConsultarSolicitacaoEstornoPagtosSaidaDTO> listaSaida = new ArrayList <ConsultarSolicitacaoEstornoPagtosSaidaDTO>();

		request.setCdMotivoSituacaoSolicitacao(PgitUtil.verificaIntegerNulo(entradaDTO.getCdMotivoSituacaoSolicitacao()));
		request.setCdPessoaJuridicaContrato(PgitUtil.verificaLongNulo(entradaDTO.getCdPessoaJuridicaContrato()));
		request.setCdModalidadePagamentoCliente(PgitUtil.verificaIntegerNulo(entradaDTO.getCdModalidadePagamentoCliente()));
		request.setCdSituacaoSolicitacaoPagamento(PgitUtil.verificaIntegerNulo(entradaDTO.getCdSituacaoSolicitacaoPagamento()));		
		request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoContratoNegocio()));
		request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(entradaDTO.getNrSequenciaContratoNegocio()));
		request.setDtSolicitacaoFim(PgitUtil.verificaStringNula(entradaDTO.getDtSolicitacaoFinal()));
		request.setDtSolicitacaoInicio(PgitUtil.verificaStringNula(entradaDTO.getDtSolicitacaoInicio()));
		request.setNumeroOcorrencias(IManterSolEstornoPagtoServiceConstants.NUMERO_OCORRENCIAS_CONSULTAR);
		
		response = getFactoryAdapter().getConsultarSolicitacaoEstornoPagtosPDCAdapter().invokeProcess(request);
		
		ConsultarSolicitacaoEstornoPagtosSaidaDTO saida;
		for(int i = 0; i < response.getOcorrenciasCount(); i++){
			saida = new ConsultarSolicitacaoEstornoPagtosSaidaDTO();


			saida.setCdPessoaJuridicaContrato(response.getOcorrencias(i).getCdPessoaJuridicaContrato());
			saida.setCdSolicitacaoPagamento(response.getOcorrencias(i).getCdSolicitacaoPagamento());
			saida.setCdSituacaoSolicitacao(response.getOcorrencias(i).getCdSituacaoSolicitacao());
			saida.setCdTipoContratoNegocio(response.getOcorrencias(i).getCdTipoContratoNegocio());
			saida.setNrSequenciaContratoNegocio(response.getOcorrencias(i).getNrSequenciaContratoNegocio());
			saida.setNrSolicitacaoPagamentoIntegrado(response.getOcorrencias(i).getNrSolicitacaoPagamentoIntegrado());
			
			saida.setCdCpfCnpjParticipante(response.getOcorrencias(i).getCdCpfCnpjParticipante());
			saida.setCdPessoaCompleto(response.getOcorrencias(i).getCdPessoaCompleto());
			saida.setDsSituacaoSolicitante(response.getOcorrencias(i).getDsSituacaoSolicitante());
			saida.setCdMotivoSituacaoSolicitante(response.getOcorrencias(i).getCdMotivoSituacaoSolicitante());
			saida.setDsMotivoSituacaoSolicitante(response.getOcorrencias(i).getDsMotivoSituacaoSolicitante());
			saida.setDsDataSolicitacao(response.getOcorrencias(i).getDsDataSolicitacao());
			saida.setDsHoraSolicitacao(response.getOcorrencias(i).getDsHoraSolicitacao());
			saida.setCdModalidade(response.getOcorrencias(i).getCdModalidade());
			saida.setDsModalidade(response.getOcorrencias(i).getDsModalidade());
	        saida.setCdFormaEstrnPagamento(response.getOcorrencias(i).getCdFormaEstrnPagamento());
	        saida.setDsFormaEstrnPagamento(response.getOcorrencias(i).getDsFormaEstrnPagamento());
			
			listaSaida.add(saida);
		}
		
		return listaSaida;
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.mantersolestornopagto.IManterSolEstornoPagtoService#excluirSolicitacaoEstornoPagtos(br.com.bradesco.web.pgit.service.business.mantersolestornopagto.bean.ExcluirSolicitacaoEstornoPagtosEntradaDTO)
	 */
	public ExcluirSolicitacaoEstornoPagtosSaidaDTO excluirSolicitacaoEstornoPagtos (ExcluirSolicitacaoEstornoPagtosEntradaDTO entradaDTO){
		ExcluirSolicitacaoEstornoPagtosRequest request = new ExcluirSolicitacaoEstornoPagtosRequest();
		ExcluirSolicitacaoEstornoPagtosResponse response = new ExcluirSolicitacaoEstornoPagtosResponse();
		
		/*request.setCdFuncao(PgitUtil.verificaIntegerNulo(entradaDTO.getCdFuncao()));
		request.setCdSolicitacaoEstorno(PgitUtil.verificaIntegerNulo(entradaDTO.getCdSolicitacaoEstorno()));
		request.setNrSolicitacaoEstorno(PgitUtil.verificaIntegerNulo(entradaDTO.getNrSolicitacaoEstorno()));
		request.setObservacao(PgitUtil.verificaStringNula(entradaDTO.getObservacao()));*/
		
		request.setCdSolicitacaoPagamento(PgitUtil.verificaIntegerNulo(entradaDTO.getCdSolicitacaoPagamento()));
		request.setNrSolicitacaoPagamento(PgitUtil.verificaIntegerNulo(entradaDTO.getNrSolicitacaoPagamento()));
		request.setDsObservacao(PgitUtil.verificaStringNula(entradaDTO.getDsObservacao()));
		request.setCdTipoAcao(PgitUtil.verificaIntegerNulo(entradaDTO.getTipoAcao()));
		
		response = getFactoryAdapter().getExcluirSolicitacaoEstornoPagtosPDCAdapter().invokeProcess(request);
		
		ExcluirSolicitacaoEstornoPagtosSaidaDTO saida;
		saida = new ExcluirSolicitacaoEstornoPagtosSaidaDTO();
		
		saida.setCodMensagem(response.getCodMensagem());
		saida.setMensagem(response.getMensagem());
		
		return saida;		
	}
	
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.mantersolestornopagto.IManterSolEstornoPagtoService#autorizarSolicitacaoEstornoPatos(br.com.bradesco.web.pgit.service.business.mantersolestornopagto.bean.AutorizarSolicitacaoEstornoPatosEntradaDTO)
	 */
	public AutorizarSolicitacaoEstornoPatosSaidaDTO autorizarSolicitacaoEstornoPatos (AutorizarSolicitacaoEstornoPatosEntradaDTO entradaDTO){
		AutorizarSolicitacaoEstornoPatosRequest request = new AutorizarSolicitacaoEstornoPatosRequest();
		AutorizarSolicitacaoEstornoPatosResponse response = new AutorizarSolicitacaoEstornoPatosResponse();
		
		/*request.setCdFuncao(PgitUtil.verificaIntegerNulo(entradaDTO.getCdFuncao()));
		request.setCdSolicitacaoPaga(PgitUtil.verificaIntegerNulo(entradaDTO.getCdSolicitacaoEstorno()));
		request.setNrSolicitacaoEstorno(PgitUtil.verificaIntegerNulo(entradaDTO.getNrSolicitacaoEstorno()));
		request.setObservacao(PgitUtil.verificaStringNula(entradaDTO.getObservacao()));*/
		
		request.setCdSolicitacaoPagamento(PgitUtil.verificaIntegerNulo(entradaDTO.getCdSolicitacaoPagamento()));
		request.setNrSolicitacaoPagamento(PgitUtil.verificaIntegerNulo(entradaDTO.getNrSolicitacaoPagamento()));
		request.setDsObservacao(PgitUtil.verificaStringNula(entradaDTO.getDsObservacao()));
		request.setCdTipoAcao(PgitUtil.verificaIntegerNulo(entradaDTO.getTipoAcao()));
		
		response = getFactoryAdapter().getAutorizarSolicitacaoEstornoPatosPDCAdapter().invokeProcess(request);
		
		AutorizarSolicitacaoEstornoPatosSaidaDTO saida;
		saida = new AutorizarSolicitacaoEstornoPatosSaidaDTO();
		
		saida.setCodMensagem(response.getCodMensagem());
		saida.setMensagem(response.getMensagem());
		
		
		return saida;
	}
	
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.mantersolestornopagto.IManterSolEstornoPagtoService#consultarDetalhesSolicitacao(br.com.bradesco.web.pgit.service.business.mansolrecpagvencnaopag.bean.ConsultarDetalhesSolicitacaoEntradaDTO)
	 */
	public ConsultarDetalhesSolicitacaoSaidaDTO consultarDetalhesSolicitacao(ConsultarDetalhesSolicitacaoEntradaDTO entrada){
		ConsultarDetalhesSolicitacaoSaidaDTO saida = new ConsultarDetalhesSolicitacaoSaidaDTO();
		ConsultarDetalhesSolicitacaoRequest request = new ConsultarDetalhesSolicitacaoRequest();
		ConsultarDetalhesSolicitacaoResponse response = new ConsultarDetalhesSolicitacaoResponse();

		request.setCdSolicitacaoPagamento(PgitUtil.verificaIntegerNulo(entrada.getCdSolicitacaoPagamento()));
		request.setNrSolicitacaoPagamento(PgitUtil.verificaIntegerNulo(entrada.getNrSolicitacaoPagamento()));
		request.setDsObservacao(PgitUtil.verificaStringNula(entrada.getDsObservacao()));
		request.setCdTipoAcao(PgitUtil.verificaIntegerNulo(entrada.getCdTipoAcao()));

		response = getFactoryAdapter().getConsultarDetalhesSolicitacaoPDCAdapter().invokeProcess(request);

		saida.setCodMensagem(response.getCodMensagem());
		saida.setMensagem(response.getMensagem());
		saida.setCdBancoEstorno(response.getCdBancoEstorno());
		saida.setDsBancoEstorno(response.getDsBancoEstorno());
		saida.setCdAgenciaEstorno(response.getCdAgenciaEstorno());
		saida.setCdDigitoAgenciaEstorno(response.getCdDigitoAgenciaEstorno());
		saida.setDsAgenciaEstorno(response.getDsAgenciaEstorno());
		saida.setCdContaEstorno(response.getCdContaEstorno());
		saida.setCdDigitoContaEstorno(response.getCdDigitoContaEstorno());
		saida.setDsTipoConta(response.getDsTipoConta());
		saida.setSituacaoSolicitacao(response.getSituacaoSolicitacao());
		saida.setCdMotivoSolicitacao(response.getCdMotivoSolicitacao());
		saida.setDsObservacao(response.getDsObservacao());
		saida.setDsDataSolicitacao(response.getDsDataSolicitacao());
		saida.setDsHoraSolicitacao(response.getDsHoraSolicitacao());
		saida.setDtAutorizacao(response.getDtAutorizacao());
		saida.setHrAutorizacao(response.getHrAutorizacao());
		saida.setCdUsuarioAutorizacao(response.getCdUsuarioAutorizacao());
		saida.setDtPrevistaEstorno(response.getDtPrevistaEstorno());
		saida.setCdValorPrevistoEstorno(response.getCdValorPrevistoEstorno());
		saida.setCdFormaEstrnPagamento(response.getCdFormaEstrnPagamento());
		saida.setDsFormaEstrnPagamento(response.getDsFormaEstrnPagamento());
		saida.setDtInclusao(response.getDtInclusao());
	    saida.setHrInclusao(response.getHrInclusao());
	    saida.setCdUsuarioInclusao(response.getCdUsuarioInclusao());
	    saida.setCdTipoCanalInclusao(response.getCdTipoCanalInclusao());
	    saida.setDsTipoCanalInclusao(response.getDsTipoCanalInclusao());
	    saida.setCdOperacaoFluxoInclusao(response.getCdOperacaoFluxoInclusao());
	    saida.setDtManutencao(response.getDtManutencao());
	    saida.setHrManutencao(response.getHrManutencao());
	    saida.setCdUsuarioManutencao(response.getCdUsuarioManutencao());
	    saida.setCdCanalManutencao(response.getCdCanalManutencao());
	    saida.setDsTipoCanalManutencao(response.getDsTipoCanalManutencao());
	    saida.setCdOperacaoFluxoManutencao(response.getCdOperacaoFluxoManutencao());

		return saida;
	}
	
	
	
}

