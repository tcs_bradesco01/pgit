/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listartiporespons.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdTipoResponsavelOrganizacao
     */
    private int _cdTipoResponsavelOrganizacao = 0;

    /**
     * keeps track of state for field: _cdTipoResponsavelOrganizacao
     */
    private boolean _has_cdTipoResponsavelOrganizacao;

    /**
     * Field _dsTipoResponsavelOrganizacao
     */
    private java.lang.String _dsTipoResponsavelOrganizacao;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listartiporespons.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdTipoResponsavelOrganizacao
     * 
     */
    public void deleteCdTipoResponsavelOrganizacao()
    {
        this._has_cdTipoResponsavelOrganizacao= false;
    } //-- void deleteCdTipoResponsavelOrganizacao() 

    /**
     * Returns the value of field 'cdTipoResponsavelOrganizacao'.
     * 
     * @return int
     * @return the value of field 'cdTipoResponsavelOrganizacao'.
     */
    public int getCdTipoResponsavelOrganizacao()
    {
        return this._cdTipoResponsavelOrganizacao;
    } //-- int getCdTipoResponsavelOrganizacao() 

    /**
     * Returns the value of field 'dsTipoResponsavelOrganizacao'.
     * 
     * @return String
     * @return the value of field 'dsTipoResponsavelOrganizacao'.
     */
    public java.lang.String getDsTipoResponsavelOrganizacao()
    {
        return this._dsTipoResponsavelOrganizacao;
    } //-- java.lang.String getDsTipoResponsavelOrganizacao() 

    /**
     * Method hasCdTipoResponsavelOrganizacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoResponsavelOrganizacao()
    {
        return this._has_cdTipoResponsavelOrganizacao;
    } //-- boolean hasCdTipoResponsavelOrganizacao() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdTipoResponsavelOrganizacao'.
     * 
     * @param cdTipoResponsavelOrganizacao the value of field
     * 'cdTipoResponsavelOrganizacao'.
     */
    public void setCdTipoResponsavelOrganizacao(int cdTipoResponsavelOrganizacao)
    {
        this._cdTipoResponsavelOrganizacao = cdTipoResponsavelOrganizacao;
        this._has_cdTipoResponsavelOrganizacao = true;
    } //-- void setCdTipoResponsavelOrganizacao(int) 

    /**
     * Sets the value of field 'dsTipoResponsavelOrganizacao'.
     * 
     * @param dsTipoResponsavelOrganizacao the value of field
     * 'dsTipoResponsavelOrganizacao'.
     */
    public void setDsTipoResponsavelOrganizacao(java.lang.String dsTipoResponsavelOrganizacao)
    {
        this._dsTipoResponsavelOrganizacao = dsTipoResponsavelOrganizacao;
    } //-- void setDsTipoResponsavelOrganizacao(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listartiporespons.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listartiporespons.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listartiporespons.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listartiporespons.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
