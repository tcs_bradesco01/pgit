/*
 * Nome: br.com.bradesco.web.pgit.service.business.combo.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.combo.bean;

/**
 * Nome: ListarServicosEmissaoEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarServicosEmissaoEntradaDTO {

	/** Atributo numOcorrencias. */
	private Integer numOcorrencias;
	
	/** Atributo cdPessoaJuridicaContrato. */
	private Long cdPessoaJuridicaContrato;
	
	/** Atributo cdTipoContratoNegocio. */
	private Integer cdTipoContratoNegocio;
	
	/** Atributo numSequenciaContratoNegocio. */
	private Long numSequenciaContratoNegocio;

	/**
	 * Get: numOcorrencias.
	 *
	 * @return numOcorrencias
	 */
	public Integer getNumOcorrencias() {
		return numOcorrencias;
	}
	
	/**
	 * Set: numOcorrencias.
	 *
	 * @param numOcorrencias the num ocorrencias
	 */
	public void setNumOcorrencias(Integer numOcorrencias) {
		this.numOcorrencias = numOcorrencias;
	}
	
	/**
	 * Get: cdPessoaJuridicaContrato.
	 *
	 * @return cdPessoaJuridicaContrato
	 */
	public Long getCdPessoaJuridicaContrato() {
		return cdPessoaJuridicaContrato;
	}
	
	/**
	 * Set: cdPessoaJuridicaContrato.
	 *
	 * @param cdPessoaJuridicaContrato the cd pessoa juridica contrato
	 */
	public void setCdPessoaJuridicaContrato(Long cdPessoaJuridicaContrato) {
		this.cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
	}
	
	/**
	 * Get: cdTipoContratoNegocio.
	 *
	 * @return cdTipoContratoNegocio
	 */
	public Integer getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}
	
	/**
	 * Set: cdTipoContratoNegocio.
	 *
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}
	
	/**
	 * Get: numSequenciaContratoNegocio.
	 *
	 * @return numSequenciaContratoNegocio
	 */
	public Long getNumSequenciaContratoNegocio() {
		return numSequenciaContratoNegocio;
	}
	
	/**
	 * Set: numSequenciaContratoNegocio.
	 *
	 * @param numSequenciaContratoNegocio the num sequencia contrato negocio
	 */
	public void setNumSequenciaContratoNegocio(Long numSequenciaContratoNegocio) {
		this.numSequenciaContratoNegocio = numSequenciaContratoNegocio;
	}
}