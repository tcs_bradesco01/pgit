/*
 * Nome: br.com.bradesco.web.pgit.service.business.manterambienteoperacaocontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.manterambienteoperacaocontrato.bean;

import br.com.bradesco.web.pgit.utils.CpfCnpjUtils;

/**
 * Nome: ConsultarSolicitacaoAmbientePartcSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarSolicitacaoAmbientePartcSaidaDTO {

	/** Atributo codMensagem. */
	private String codMensagem;
	
	/** Atributo mensagem. */
	private String mensagem;
	
	/** Atributo dsServico. */
	private String dsServico;
	
	/** Atributo dsModalidade. */
	private String dsModalidade;
	
	/** Atributo cdTipoMudanca. */
	private String cdTipoMudanca;
	
	/** Atributo cdFormaEfetivacao. */
	private String cdFormaEfetivacao;
	
	/** Atributo dtProgramada. */
	private String dtProgramada;
	
	/** Atributo cdSituacao. */
	private String cdSituacao;
	
	/** Atributo cdManterOperacao. */
	private String cdManterOperacao;
	
	/** Atributo cdUsuarioInclusao. */
	private String cdUsuarioInclusao;
	
	/** Atributo dtInclusao. */
	private String dtInclusao;
	
	/** Atributo hrInclusao. */
	private String hrInclusao;
	
	/** Atributo cdOperacaoCanalInclusao. */
	private String cdOperacaoCanalInclusao;
	
	/** Atributo cdTipoCanalInclusao. */
	private int cdTipoCanalInclusao;
	
	/** Atributo dsCanalInclusao. */
	private String dsCanalInclusao;
	
	/** Atributo cdUsuarioManutencao. */
	private String cdUsuarioManutencao;
	
	/** Atributo dtManutencao. */
	private String dtManutencao;
	
	/** Atributo hrManutencao. */
	private String hrManutencao;
	
	/** Atributo cdOperacaoCanalManutencao. */
	private String cdOperacaoCanalManutencao;
	
	/** Atributo cdTipoCanalManutencao. */
	private Integer cdTipoCanalManutencao;
	
	/** Atributo dsCanalManutencao. */
	private String dsCanalManutencao;
	
	/** Atributo cdServico. */
	private Integer cdServico;
	
	/** Atributo cdModalidade. */
	private Integer cdModalidade;
	
	/** Atributo cdMotivoSituacao. */
	private Integer cdMotivoSituacao;
	
	/** Atributo dsMotivoSituacao. */
	private String dsMotivoSituacao;
	
	/** Atributo cdTipoParticipacaoPessoa. */
	private Integer cdTipoParticipacaoPessoa;
	
	/** Atributo cdPessoa. */
	private Long cdPessoa;
	
	/** Atributo cdFilialParticipante. */
	private Integer cdFilialParticipante;
	
	/** Atributo cdControlePArticipante. */
	private Integer cdControlePArticipante;
	
	/** Atributo dsNomeRazaoParticipante. */
	private String dsNomeRazaoParticipante;
	
	/** Atributo cdCnpjParticipante. */
	private Long cdCnpjParticipante;

	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}

	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}

	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}

	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	/**
	 * Get: dsServico.
	 *
	 * @return dsServico
	 */
	public String getDsServico() {
		return dsServico;
	}

	/**
	 * Set: dsServico.
	 *
	 * @param dsServico the ds servico
	 */
	public void setDsServico(String dsServico) {
		this.dsServico = dsServico;
	}

	/**
	 * Get: dsModalidade.
	 *
	 * @return dsModalidade
	 */
	public String getDsModalidade() {
		return dsModalidade;
	}

	/**
	 * Set: dsModalidade.
	 *
	 * @param dsModalidade the ds modalidade
	 */
	public void setDsModalidade(String dsModalidade) {
		this.dsModalidade = dsModalidade;
	}

	/**
	 * Get: cdTipoMudanca.
	 *
	 * @return cdTipoMudanca
	 */
	public String getCdTipoMudanca() {
		return cdTipoMudanca;
	}

	/**
	 * Set: cdTipoMudanca.
	 *
	 * @param cdTipoMudanca the cd tipo mudanca
	 */
	public void setCdTipoMudanca(String cdTipoMudanca) {
		this.cdTipoMudanca = cdTipoMudanca;
	}

	/**
	 * Get: cdFormaEfetivacao.
	 *
	 * @return cdFormaEfetivacao
	 */
	public String getCdFormaEfetivacao() {
		return cdFormaEfetivacao;
	}

	/**
	 * Set: cdFormaEfetivacao.
	 *
	 * @param cdFormaEfetivacao the cd forma efetivacao
	 */
	public void setCdFormaEfetivacao(String cdFormaEfetivacao) {
		this.cdFormaEfetivacao = cdFormaEfetivacao;
	}

	/**
	 * Get: dtProgramada.
	 *
	 * @return dtProgramada
	 */
	public String getDtProgramada() {
		return dtProgramada;
	}

	/**
	 * Set: dtProgramada.
	 *
	 * @param dtProgramada the dt programada
	 */
	public void setDtProgramada(String dtProgramada) {
		this.dtProgramada = dtProgramada;
	}

	/**
	 * Get: cdSituacao.
	 *
	 * @return cdSituacao
	 */
	public String getCdSituacao() {
		return cdSituacao;
	}

	/**
	 * Set: cdSituacao.
	 *
	 * @param cdSituacao the cd situacao
	 */
	public void setCdSituacao(String cdSituacao) {
		this.cdSituacao = cdSituacao;
	}

	/**
	 * Get: cdManterOperacao.
	 *
	 * @return cdManterOperacao
	 */
	public String getCdManterOperacao() {
		return cdManterOperacao;
	}

	/**
	 * Set: cdManterOperacao.
	 *
	 * @param cdManterOperacao the cd manter operacao
	 */
	public void setCdManterOperacao(String cdManterOperacao) {
		this.cdManterOperacao = cdManterOperacao;
	}

	/**
	 * Get: cdUsuarioInclusao.
	 *
	 * @return cdUsuarioInclusao
	 */
	public String getCdUsuarioInclusao() {
		return cdUsuarioInclusao;
	}

	/**
	 * Set: cdUsuarioInclusao.
	 *
	 * @param cdUsuarioInclusao the cd usuario inclusao
	 */
	public void setCdUsuarioInclusao(String cdUsuarioInclusao) {
		this.cdUsuarioInclusao = cdUsuarioInclusao;
	}

	/**
	 * Get: dtInclusao.
	 *
	 * @return dtInclusao
	 */
	public String getDtInclusao() {
		return dtInclusao;
	}

	/**
	 * Set: dtInclusao.
	 *
	 * @param dtInclusao the dt inclusao
	 */
	public void setDtInclusao(String dtInclusao) {
		this.dtInclusao = dtInclusao;
	}

	/**
	 * Get: hrInclusao.
	 *
	 * @return hrInclusao
	 */
	public String getHrInclusao() {
		return hrInclusao;
	}

	/**
	 * Set: hrInclusao.
	 *
	 * @param hrInclusao the hr inclusao
	 */
	public void setHrInclusao(String hrInclusao) {
		this.hrInclusao = hrInclusao;
	}

	/**
	 * Get: cdOperacaoCanalInclusao.
	 *
	 * @return cdOperacaoCanalInclusao
	 */
	public String getCdOperacaoCanalInclusao() {
		return cdOperacaoCanalInclusao;
	}

	/**
	 * Set: cdOperacaoCanalInclusao.
	 *
	 * @param cdOperacaoCanalInclusao the cd operacao canal inclusao
	 */
	public void setCdOperacaoCanalInclusao(String cdOperacaoCanalInclusao) {
		this.cdOperacaoCanalInclusao = cdOperacaoCanalInclusao;
	}

	/**
	 * Get: cdTipoCanalInclusao.
	 *
	 * @return cdTipoCanalInclusao
	 */
	public int getCdTipoCanalInclusao() {
		return cdTipoCanalInclusao;
	}

	/**
	 * Set: cdTipoCanalInclusao.
	 *
	 * @param cdTipoCanalInclusao the cd tipo canal inclusao
	 */
	public void setCdTipoCanalInclusao(int cdTipoCanalInclusao) {
		this.cdTipoCanalInclusao = cdTipoCanalInclusao;
	}

	/**
	 * Get: dsCanalInclusao.
	 *
	 * @return dsCanalInclusao
	 */
	public String getDsCanalInclusao() {
		return dsCanalInclusao;
	}

	/**
	 * Set: dsCanalInclusao.
	 *
	 * @param dsCanalInclusao the ds canal inclusao
	 */
	public void setDsCanalInclusao(String dsCanalInclusao) {
		this.dsCanalInclusao = dsCanalInclusao;
	}

	/**
	 * Get: cdUsuarioManutencao.
	 *
	 * @return cdUsuarioManutencao
	 */
	public String getCdUsuarioManutencao() {
		return cdUsuarioManutencao;
	}

	/**
	 * Set: cdUsuarioManutencao.
	 *
	 * @param cdUsuarioManutencao the cd usuario manutencao
	 */
	public void setCdUsuarioManutencao(String cdUsuarioManutencao) {
		this.cdUsuarioManutencao = cdUsuarioManutencao;
	}

	/**
	 * Get: dtManutencao.
	 *
	 * @return dtManutencao
	 */
	public String getDtManutencao() {
		return dtManutencao;
	}

	/**
	 * Set: dtManutencao.
	 *
	 * @param dtManutencao the dt manutencao
	 */
	public void setDtManutencao(String dtManutencao) {
		this.dtManutencao = dtManutencao;
	}

	/**
	 * Get: hrManutencao.
	 *
	 * @return hrManutencao
	 */
	public String getHrManutencao() {
		return hrManutencao;
	}

	/**
	 * Set: hrManutencao.
	 *
	 * @param hrManutencao the hr manutencao
	 */
	public void setHrManutencao(String hrManutencao) {
		this.hrManutencao = hrManutencao;
	}

	/**
	 * Get: cdOperacaoCanalManutencao.
	 *
	 * @return cdOperacaoCanalManutencao
	 */
	public String getCdOperacaoCanalManutencao() {
		return cdOperacaoCanalManutencao;
	}

	/**
	 * Set: cdOperacaoCanalManutencao.
	 *
	 * @param cdOperacaoCanalManutencao the cd operacao canal manutencao
	 */
	public void setCdOperacaoCanalManutencao(String cdOperacaoCanalManutencao) {
		this.cdOperacaoCanalManutencao = cdOperacaoCanalManutencao;
	}

	/**
	 * Get: cdTipoCanalManutencao.
	 *
	 * @return cdTipoCanalManutencao
	 */
	public Integer getCdTipoCanalManutencao() {
		return cdTipoCanalManutencao;
	}

	/**
	 * Set: cdTipoCanalManutencao.
	 *
	 * @param cdTipoCanalManutencao the cd tipo canal manutencao
	 */
	public void setCdTipoCanalManutencao(Integer cdTipoCanalManutencao) {
		this.cdTipoCanalManutencao = cdTipoCanalManutencao;
	}

	/**
	 * Get: dsCanalManutencao.
	 *
	 * @return dsCanalManutencao
	 */
	public String getDsCanalManutencao() {
		return dsCanalManutencao;
	}

	/**
	 * Set: dsCanalManutencao.
	 *
	 * @param dsCanalManutencao the ds canal manutencao
	 */
	public void setDsCanalManutencao(String dsCanalManutencao) {
		this.dsCanalManutencao = dsCanalManutencao;
	}

	/**
	 * Get: cdServico.
	 *
	 * @return cdServico
	 */
	public Integer getCdServico() {
		return cdServico;
	}

	/**
	 * Set: cdServico.
	 *
	 * @param cdServico the cd servico
	 */
	public void setCdServico(Integer cdServico) {
		this.cdServico = cdServico;
	}

	/**
	 * Get: cdModalidade.
	 *
	 * @return cdModalidade
	 */
	public Integer getCdModalidade() {
		return cdModalidade;
	}

	/**
	 * Set: cdModalidade.
	 *
	 * @param cdModalidade the cd modalidade
	 */
	public void setCdModalidade(Integer cdModalidade) {
		this.cdModalidade = cdModalidade;
	}

	/**
	 * Get: cdMotivoSituacao.
	 *
	 * @return cdMotivoSituacao
	 */
	public Integer getCdMotivoSituacao() {
		return cdMotivoSituacao;
	}

	/**
	 * Set: cdMotivoSituacao.
	 *
	 * @param cdMotivoSituacao the cd motivo situacao
	 */
	public void setCdMotivoSituacao(Integer cdMotivoSituacao) {
		this.cdMotivoSituacao = cdMotivoSituacao;
	}

	/**
	 * Get: dsMotivoSituacao.
	 *
	 * @return dsMotivoSituacao
	 */
	public String getDsMotivoSituacao() {
		return dsMotivoSituacao;
	}

	/**
	 * Set: dsMotivoSituacao.
	 *
	 * @param dsMotivoSituacao the ds motivo situacao
	 */
	public void setDsMotivoSituacao(String dsMotivoSituacao) {
		this.dsMotivoSituacao = dsMotivoSituacao;
	}

	/**
	 * Get: cdTipoParticipacaoPessoa.
	 *
	 * @return cdTipoParticipacaoPessoa
	 */
	public Integer getCdTipoParticipacaoPessoa() {
		return cdTipoParticipacaoPessoa;
	}

	/**
	 * Set: cdTipoParticipacaoPessoa.
	 *
	 * @param cdTipoParticipacaoPessoa the cd tipo participacao pessoa
	 */
	public void setCdTipoParticipacaoPessoa(Integer cdTipoParticipacaoPessoa) {
		this.cdTipoParticipacaoPessoa = cdTipoParticipacaoPessoa;
	}

	/**
	 * Get: cdPessoa.
	 *
	 * @return cdPessoa
	 */
	public Long getCdPessoa() {
		return cdPessoa;
	}

	/**
	 * Set: cdPessoa.
	 *
	 * @param cdPessoa the cd pessoa
	 */
	public void setCdPessoa(Long cdPessoa) {
		this.cdPessoa = cdPessoa;
	}

	/**
	 * Get: cdFilialParticipante.
	 *
	 * @return cdFilialParticipante
	 */
	public Integer getCdFilialParticipante() {
		return cdFilialParticipante;
	}

	/**
	 * Set: cdFilialParticipante.
	 *
	 * @param cdFilialParticipante the cd filial participante
	 */
	public void setCdFilialParticipante(Integer cdFilialParticipante) {
		this.cdFilialParticipante = cdFilialParticipante;
	}

	/**
	 * Get: cdControlePArticipante.
	 *
	 * @return cdControlePArticipante
	 */
	public Integer getCdControlePArticipante() {
		return cdControlePArticipante;
	}

	/**
	 * Set: cdControlePArticipante.
	 *
	 * @param cdControlePArticipante the cd controle p articipante
	 */
	public void setCdControlePArticipante(Integer cdControlePArticipante) {
		this.cdControlePArticipante = cdControlePArticipante;
	}

	/**
	 * Get: dsNomeRazaoParticipante.
	 *
	 * @return dsNomeRazaoParticipante
	 */
	public String getDsNomeRazaoParticipante() {
		return dsNomeRazaoParticipante;
	}

	/**
	 * Set: dsNomeRazaoParticipante.
	 *
	 * @param dsNomeRazaoParticipante the ds nome razao participante
	 */
	public void setDsNomeRazaoParticipante(String dsNomeRazaoParticipante) {
		this.dsNomeRazaoParticipante = dsNomeRazaoParticipante;
	}

	/**
	 * Get: cpfCnpjFormatado.
	 *
	 * @return cpfCnpjFormatado
	 */
	public String getCpfCnpjFormatado() {
		return CpfCnpjUtils.formatCpfCnpjCompleto(cdCnpjParticipante,
				cdFilialParticipante, cdControlePArticipante);
	}

	/**
	 * Get: cdCnpjParticipante.
	 *
	 * @return cdCnpjParticipante
	 */
	public Long getCdCnpjParticipante() {
		return cdCnpjParticipante;
	}

	/**
	 * Set: cdCnpjParticipante.
	 *
	 * @param cdCnpjParticipante the cd cnpj participante
	 */
	public void setCdCnpjParticipante(Long cdCnpjParticipante) {
		this.cdCnpjParticipante = cdCnpjParticipante;
	}
}
