/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/interfaz.ftl,v $
 * $Id: interfaz.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: interfaz.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.manassocmsglayoutmsgsist;

import java.util.List;

import br.com.bradesco.web.pgit.service.business.manassocmsglayoutmsgsist.bean.ConsultarVinculacaoMsgLayoutSistemaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manassocmsglayoutmsgsist.bean.ConsultarVinculacaoMsgLayoutSistemaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manassocmsglayoutmsgsist.bean.DetalharVinculacaoMsgLayoutSistemaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manassocmsglayoutmsgsist.bean.DetalharVinculacaoMsgLayoutSistemaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manassocmsglayoutmsgsist.bean.ExcluirVinculacaoMsgLayoutSistemaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manassocmsglayoutmsgsist.bean.ExcluirVinculacaoMsgLayoutSistemaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manassocmsglayoutmsgsist.bean.IncluirVinculacaoMsgLayoutSistemaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manassocmsglayoutmsgsist.bean.IncluirVinculacaoMsgLayoutSistemaSaidaDTO;

/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Interface do adaptador: ManAssocMsgLayoutMsgSist
 * </p>
 * 
 * @comment CODIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public interface IManAssocMsgLayoutMsgSistService {
	
	/**
	 * Detalhar vinculacao msg layout sistema.
	 *
	 * @param entrada the entrada
	 * @return the detalhar vinculacao msg layout sistema saida dto
	 */
	DetalharVinculacaoMsgLayoutSistemaSaidaDTO detalharVinculacaoMsgLayoutSistema(DetalharVinculacaoMsgLayoutSistemaEntradaDTO entrada);
	
	/**
	 * Excluir vinculacao msg layout sistema.
	 *
	 * @param entrada the entrada
	 * @return the excluir vinculacao msg layout sistema saida dto
	 */
	ExcluirVinculacaoMsgLayoutSistemaSaidaDTO excluirVinculacaoMsgLayoutSistema(ExcluirVinculacaoMsgLayoutSistemaEntradaDTO entrada);
	
	/**
	 * Incluir vinculacao msg layout sistema.
	 *
	 * @param entrada the entrada
	 * @return the incluir vinculacao msg layout sistema saida dto
	 */
	IncluirVinculacaoMsgLayoutSistemaSaidaDTO incluirVinculacaoMsgLayoutSistema(IncluirVinculacaoMsgLayoutSistemaEntradaDTO entrada);
	
	/**
	 * Consultar vinculacao msg layout sistema.
	 *
	 * @param entrada the entrada
	 * @return the list< consultar vinculacao msg layout sistema saida dt o>
	 */
	List<ConsultarVinculacaoMsgLayoutSistemaSaidaDTO> consultarVinculacaoMsgLayoutSistema(ConsultarVinculacaoMsgLayoutSistemaEntradaDTO entrada);
}

