/**
 * 
 */
package br.com.bradesco.web.pgit.service.business.mantersolicitacaorastfav.bean;

import br.com.bradesco.web.pgit.service.data.pdc.consultarlistasolicitacaorastreamento.response.Ocorrencias;

/**
 * @author tcordeiro
 *
 */
public class ManterSolicRastFavSaidaDTO {
	
	/** Atributo nrSolicitacaoPagamento. */
	private Integer nrSolicitacaoPagamento;
    
    /** Atributo dtSolicitacao. */
    private String dtSolicitacao;
    
    /** Atributo hrSolicitacao. */
    private String hrSolicitacao;
    
    /** Atributo cdSituacaoSolicitacao. */
    private Integer cdSituacaoSolicitacao;
    
    /** Atributo dsSituacao. */
    private String dsSituacao;
    
    /** Atributo cdSolicitacaoPagamento. */
    private Integer cdSolicitacaoPagamento;
    
    
	/**
	 * Manter solic rast fav saida dto.
	 */
	public ManterSolicRastFavSaidaDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * Manter solic rast fav saida dto.
	 *
	 * @param saida the saida
	 */
	public ManterSolicRastFavSaidaDTO(Ocorrencias saida) {
		this.nrSolicitacaoPagamento = saida.getNrSolicitacaoPagamento();
	    this.dtSolicitacao = saida.getDtSolicitacao();
	    this.hrSolicitacao = saida.getHrSolicitacao();
	    this.cdSituacaoSolicitacao = saida.getCdSituacaoSolicitacao();
	    this.dsSituacao = saida.getDsSituacaoSolicitacao();
	    this.cdSolicitacaoPagamento = saida.getCdSolicitacaoPagamento();
	}
	
	/**
	 * Get: cdSituacaoSolicitacao.
	 *
	 * @return cdSituacaoSolicitacao
	 */
	public Integer getCdSituacaoSolicitacao() {
		return cdSituacaoSolicitacao;
	}
	
	/**
	 * Set: cdSituacaoSolicitacao.
	 *
	 * @param cdSituacaoSolicitacao the cd situacao solicitacao
	 */
	public void setCdSituacaoSolicitacao(Integer cdSituacaoSolicitacao) {
		this.cdSituacaoSolicitacao = cdSituacaoSolicitacao;
	}
	
	/**
	 * Get: dsSituacao.
	 *
	 * @return dsSituacao
	 */
	public String getDsSituacao() {
		return dsSituacao;
	}
	
	/**
	 * Set: dsSituacao.
	 *
	 * @param dsSituacao the ds situacao
	 */
	public void setDsSituacao(String dsSituacao) {
		this.dsSituacao = dsSituacao;
	}
	
	/**
	 * Get: hrSolicitacao.
	 *
	 * @return hrSolicitacao
	 */
	public String getHrSolicitacao() {
		return hrSolicitacao;
	}
	
	/**
	 * Set: hrSolicitacao.
	 *
	 * @param hrSolicitacao the hr solicitacao
	 */
	public void setHrSolicitacao(String hrSolicitacao) {
		this.hrSolicitacao = hrSolicitacao;
	}
	
	/**
	 * Get: nrSolicitacaoPagamento.
	 *
	 * @return nrSolicitacaoPagamento
	 */
	public Integer getNrSolicitacaoPagamento() {
		return nrSolicitacaoPagamento;
	}
	
	/**
	 * Set: nrSolicitacaoPagamento.
	 *
	 * @param nrSolicitacaoPagamento the nr solicitacao pagamento
	 */
	public void setNrSolicitacaoPagamento(Integer nrSolicitacaoPagamento) {
		this.nrSolicitacaoPagamento = nrSolicitacaoPagamento;
	}

	/**
	 * Get: dtSolicitacaoFormatado.
	 *
	 * @return dtSolicitacaoFormatado
	 */
	public String getDtSolicitacaoFormatado() {
		return dtSolicitacao.replace('.', '/');
	}
	
	/**
	 * Get: dtSolicitacao.
	 *
	 * @return dtSolicitacao
	 */
	public String getDtSolicitacao() {
		return dtSolicitacao;
	}

	/**
	 * Set: dtSolicitacao.
	 *
	 * @param dtSolicitacao the dt solicitacao
	 */
	public void setDtSolicitacao(String dtSolicitacao) {
		this.dtSolicitacao = dtSolicitacao;
	}

	/**
	 * Get: cdSolicitacaoPagamento.
	 *
	 * @return cdSolicitacaoPagamento
	 */
	public Integer getCdSolicitacaoPagamento() {
		return cdSolicitacaoPagamento;
	}

	/**
	 * Set: cdSolicitacaoPagamento.
	 *
	 * @param cdSolicitacaoPagamento the cd solicitacao pagamento
	 */
	public void setCdSolicitacaoPagamento(Integer cdSolicitacaoPagamento) {
		this.cdSolicitacaoPagamento = cdSolicitacaoPagamento;
	}


}
