/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.detalharaditivocontrato.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _nrCpfCnpjVinc
     */
    private java.lang.String _nrCpfCnpjVinc;

    /**
     * Field _nmRazaoSocialVinc
     */
    private java.lang.String _nmRazaoSocialVinc;

    /**
     * Field _dsTipoParticipantePart
     */
    private java.lang.String _dsTipoParticipantePart;

    /**
     * Field _dsAcaoManutencaoPerfil
     */
    private java.lang.String _dsAcaoManutencaoPerfil;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharaditivocontrato.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Returns the value of field 'dsAcaoManutencaoPerfil'.
     * 
     * @return String
     * @return the value of field 'dsAcaoManutencaoPerfil'.
     */
    public java.lang.String getDsAcaoManutencaoPerfil()
    {
        return this._dsAcaoManutencaoPerfil;
    } //-- java.lang.String getDsAcaoManutencaoPerfil() 

    /**
     * Returns the value of field 'dsTipoParticipantePart'.
     * 
     * @return String
     * @return the value of field 'dsTipoParticipantePart'.
     */
    public java.lang.String getDsTipoParticipantePart()
    {
        return this._dsTipoParticipantePart;
    } //-- java.lang.String getDsTipoParticipantePart() 

    /**
     * Returns the value of field 'nmRazaoSocialVinc'.
     * 
     * @return String
     * @return the value of field 'nmRazaoSocialVinc'.
     */
    public java.lang.String getNmRazaoSocialVinc()
    {
        return this._nmRazaoSocialVinc;
    } //-- java.lang.String getNmRazaoSocialVinc() 

    /**
     * Returns the value of field 'nrCpfCnpjVinc'.
     * 
     * @return String
     * @return the value of field 'nrCpfCnpjVinc'.
     */
    public java.lang.String getNrCpfCnpjVinc()
    {
        return this._nrCpfCnpjVinc;
    } //-- java.lang.String getNrCpfCnpjVinc() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'dsAcaoManutencaoPerfil'.
     * 
     * @param dsAcaoManutencaoPerfil the value of field
     * 'dsAcaoManutencaoPerfil'.
     */
    public void setDsAcaoManutencaoPerfil(java.lang.String dsAcaoManutencaoPerfil)
    {
        this._dsAcaoManutencaoPerfil = dsAcaoManutencaoPerfil;
    } //-- void setDsAcaoManutencaoPerfil(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoParticipantePart'.
     * 
     * @param dsTipoParticipantePart the value of field
     * 'dsTipoParticipantePart'.
     */
    public void setDsTipoParticipantePart(java.lang.String dsTipoParticipantePart)
    {
        this._dsTipoParticipantePart = dsTipoParticipantePart;
    } //-- void setDsTipoParticipantePart(java.lang.String) 

    /**
     * Sets the value of field 'nmRazaoSocialVinc'.
     * 
     * @param nmRazaoSocialVinc the value of field
     * 'nmRazaoSocialVinc'.
     */
    public void setNmRazaoSocialVinc(java.lang.String nmRazaoSocialVinc)
    {
        this._nmRazaoSocialVinc = nmRazaoSocialVinc;
    } //-- void setNmRazaoSocialVinc(java.lang.String) 

    /**
     * Sets the value of field 'nrCpfCnpjVinc'.
     * 
     * @param nrCpfCnpjVinc the value of field 'nrCpfCnpjVinc'.
     */
    public void setNrCpfCnpjVinc(java.lang.String nrCpfCnpjVinc)
    {
        this._nrCpfCnpjVinc = nrCpfCnpjVinc;
    } //-- void setNrCpfCnpjVinc(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.detalharaditivocontrato.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.detalharaditivocontrato.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.detalharaditivocontrato.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharaditivocontrato.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
