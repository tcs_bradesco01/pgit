/*
 * Nome: br.com.bradesco.web.pgit.service.business.msgcompsalarial.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.msgcompsalarial.bean;

/**
 * Nome: DetalharMsgComprovanteSalrlSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class DetalharMsgComprovanteSalrlSaidaDTO {
	
	/** Atributo codigoTipoMensagem. */
	private int codigoTipoMensagem;
	
	/** Atributo cdTipoComprovante. */
	private int cdTipoComprovante;
	
	/** Atributo dsTipoComprovante. */
	private String dsTipoComprovante;
	
	/** Atributo cdNivelMensagem. */
	private int cdNivelMensagem;
	
	/** Atributo descricao. */
	private String descricao;
	
	/** Atributo cdRestricao. */
	private int cdRestricao;
	
	/** Atributo dataHoraManutencao. */
	private String dataHoraManutencao;	
	
	/** Atributo usuarioManutencao. */
	private String usuarioManutencao;	
	
	/** Atributo complementoManutencao. */
	private String complementoManutencao;
	
	/** Atributo cdCanalManutencao. */
	private int cdCanalManutencao;
	
	/** Atributo dsCanalManutencao. */
	private String dsCanalManutencao;
	
	/** Atributo dataHoraInclusao. */
	private String dataHoraInclusao;	
	
	/** Atributo usuarioInclusao. */
	private String usuarioInclusao;
	
	/** Atributo complementoInclusao. */
	private String complementoInclusao;
	
	/** Atributo cdCanalInclusao. */
	private int cdCanalInclusao;
	
	/** Atributo dsCanalInclusao. */
	private String dsCanalInclusao;
	
	/** Atributo codMensagem. */
	private String codMensagem;
	
	/** Atributo mensagem. */
	private String mensagem;

	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}
	
	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	/**
	 * Get: cdCanalInclusao.
	 *
	 * @return cdCanalInclusao
	 */
	public int getCdCanalInclusao() {
		return cdCanalInclusao;
	}
	
	/**
	 * Set: cdCanalInclusao.
	 *
	 * @param cdCanalInclusao the cd canal inclusao
	 */
	public void setCdCanalInclusao(int cdCanalInclusao) {
		this.cdCanalInclusao = cdCanalInclusao;
	}
	
	/**
	 * Get: cdCanalManutencao.
	 *
	 * @return cdCanalManutencao
	 */
	public int getCdCanalManutencao() {
		return cdCanalManutencao;
	}
	
	/**
	 * Set: cdCanalManutencao.
	 *
	 * @param cdCanalManutencao the cd canal manutencao
	 */
	public void setCdCanalManutencao(int cdCanalManutencao) {
		this.cdCanalManutencao = cdCanalManutencao;
	}
	
	/**
	 * Get: cdNivelMensagem.
	 *
	 * @return cdNivelMensagem
	 */
	public int getCdNivelMensagem() {
		return cdNivelMensagem;
	}
	
	/**
	 * Set: cdNivelMensagem.
	 *
	 * @param cdNivelMensagem the cd nivel mensagem
	 */
	public void setCdNivelMensagem(int cdNivelMensagem) {
		this.cdNivelMensagem = cdNivelMensagem;
	}
	
	/**
	 * Get: cdRestricao.
	 *
	 * @return cdRestricao
	 */
	public int getCdRestricao() {
		return cdRestricao;
	}
	
	/**
	 * Set: cdRestricao.
	 *
	 * @param cdRestricao the cd restricao
	 */
	public void setCdRestricao(int cdRestricao) {
		this.cdRestricao = cdRestricao;
	}
	
	/**
	 * Get: cdTipoComprovante.
	 *
	 * @return cdTipoComprovante
	 */
	public int getCdTipoComprovante() {
		return cdTipoComprovante;
	}
	
	/**
	 * Set: cdTipoComprovante.
	 *
	 * @param cdTipoComprovante the cd tipo comprovante
	 */
	public void setCdTipoComprovante(int cdTipoComprovante) {
		this.cdTipoComprovante = cdTipoComprovante;
	}
	
	/**
	 * Get: codigoTipoMensagem.
	 *
	 * @return codigoTipoMensagem
	 */
	public int getCodigoTipoMensagem() {
		return codigoTipoMensagem;
	}
	
	/**
	 * Set: codigoTipoMensagem.
	 *
	 * @param codigoTipoMensagem the codigo tipo mensagem
	 */
	public void setCodigoTipoMensagem(int codigoTipoMensagem) {
		this.codigoTipoMensagem = codigoTipoMensagem;
	}
	
	/**
	 * Get: complementoInclusao.
	 *
	 * @return complementoInclusao
	 */
	public String getComplementoInclusao() {
		return complementoInclusao;
	}
	
	/**
	 * Set: complementoInclusao.
	 *
	 * @param complementoInclusao the complemento inclusao
	 */
	public void setComplementoInclusao(String complementoInclusao) {
		this.complementoInclusao = complementoInclusao;
	}
	
	/**
	 * Get: complementoManutencao.
	 *
	 * @return complementoManutencao
	 */
	public String getComplementoManutencao() {
		return complementoManutencao;
	}
	
	/**
	 * Set: complementoManutencao.
	 *
	 * @param complementoManutencao the complemento manutencao
	 */
	public void setComplementoManutencao(String complementoManutencao) {
		this.complementoManutencao = complementoManutencao;
	}
	
	/**
	 * Get: dataHoraInclusao.
	 *
	 * @return dataHoraInclusao
	 */
	public String getDataHoraInclusao() {
		return dataHoraInclusao;
	}
	
	/**
	 * Set: dataHoraInclusao.
	 *
	 * @param dataHoraInclusao the data hora inclusao
	 */
	public void setDataHoraInclusao(String dataHoraInclusao) {
		this.dataHoraInclusao = dataHoraInclusao;
	}
	
	/**
	 * Get: dataHoraManutencao.
	 *
	 * @return dataHoraManutencao
	 */
	public String getDataHoraManutencao() {
		return dataHoraManutencao;
	}
	
	/**
	 * Set: dataHoraManutencao.
	 *
	 * @param dataHoraManutencao the data hora manutencao
	 */
	public void setDataHoraManutencao(String dataHoraManutencao) {
		this.dataHoraManutencao = dataHoraManutencao;
	}
	
	/**
	 * Get: descricao.
	 *
	 * @return descricao
	 */
	public String getDescricao() {
		return descricao;
	}
	
	/**
	 * Set: descricao.
	 *
	 * @param descricao the descricao
	 */
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	/**
	 * Get: dsCanalInclusao.
	 *
	 * @return dsCanalInclusao
	 */
	public String getDsCanalInclusao() {
		return dsCanalInclusao;
	}
	
	/**
	 * Set: dsCanalInclusao.
	 *
	 * @param dsCanalInclusao the ds canal inclusao
	 */
	public void setDsCanalInclusao(String dsCanalInclusao) {
		this.dsCanalInclusao = dsCanalInclusao;
	}
	
	/**
	 * Get: dsCanalManutencao.
	 *
	 * @return dsCanalManutencao
	 */
	public String getDsCanalManutencao() {
		return dsCanalManutencao;
	}
	
	/**
	 * Set: dsCanalManutencao.
	 *
	 * @param dsCanalManutencao the ds canal manutencao
	 */
	public void setDsCanalManutencao(String dsCanalManutencao) {
		this.dsCanalManutencao = dsCanalManutencao;
	}
	
	/**
	 * Get: dsTipoComprovante.
	 *
	 * @return dsTipoComprovante
	 */
	public String getDsTipoComprovante() {
		return dsTipoComprovante;
	}
	
	/**
	 * Set: dsTipoComprovante.
	 *
	 * @param dsTipoComprovante the ds tipo comprovante
	 */
	public void setDsTipoComprovante(String dsTipoComprovante) {
		this.dsTipoComprovante = dsTipoComprovante;
	}
	
	/**
	 * Get: usuarioInclusao.
	 *
	 * @return usuarioInclusao
	 */
	public String getUsuarioInclusao() {
		return usuarioInclusao;
	}
	
	/**
	 * Set: usuarioInclusao.
	 *
	 * @param usuarioInclusao the usuario inclusao
	 */
	public void setUsuarioInclusao(String usuarioInclusao) {
		this.usuarioInclusao = usuarioInclusao;
	}
	
	/**
	 * Get: usuarioManutencao.
	 *
	 * @return usuarioManutencao
	 */
	public String getUsuarioManutencao() {
		return usuarioManutencao;
	}
	
	/**
	 * Set: usuarioManutencao.
	 *
	 * @param usuarioManutencao the usuario manutencao
	 */
	public void setUsuarioManutencao(String usuarioManutencao) {
		this.usuarioManutencao = usuarioManutencao;
	}


}
