/*
 * Nome: br.com.bradesco.web.pgit.service.business.manterservicocomposto.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.manterservicocomposto.bean;

import br.com.bradesco.web.pgit.service.data.pdc.detalharservicocomposto.response.Ocorrencias;

/**
 * Nome: DetalharServicoCompostoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class DetalharServicoCompostoSaidaDTO {
	
    /** Atributo cdServicoCompostoPagamento. */
    private Long cdServicoCompostoPagamento;
    
    /** Atributo dsServicoCompostoPagamento. */
    private String dsServicoCompostoPagamento;
    
    /** Atributo cdTipoServicoCnab. */
    private Integer cdTipoServicoCnab;
    
    /** Atributo dsTipoServicoCnab. */
    private String dsTipoServicoCnab;
    
    /** Atributo cdFormaLancamentoCnab. */
    private Integer cdFormaLancamentoCnab;
    
    /** Atributo dsFormaLancamentoCnab. */
    private String dsFormaLancamentoCnab;
    
    /** Atributo cdFormaLiquidacao. */
    private Integer cdFormaLiquidacao;
    
    /** Atributo dsFormaLiquidacao. */
    private String dsFormaLiquidacao;
    
    /** Atributo cdCanalInclusao. */
    private Integer cdCanalInclusao;
    
    /** Atributo dsCanalInclusao. */
    private String dsCanalInclusao;
    
    /** Atributo cdAutenticacaoSegurancaInclusao. */
    private String cdAutenticacaoSegurancaInclusao;
    
    /** Atributo nmOperacaoFluxoInclusao. */
    private String nmOperacaoFluxoInclusao;
    
    /** Atributo hrInclusaoRegistro. */
    private String hrInclusaoRegistro;
    
    /** Atributo cdCanalManutencao. */
    private Integer cdCanalManutencao;
    
    /** Atributo dsCanalManutencao. */
    private String dsCanalManutencao;
    
    /** Atributo cdAutenticacaoSegurancaManutencao. */
    private String cdAutenticacaoSegurancaManutencao;
    
    /** Atributo nmOperacaoFluxoManutencao. */
    private String nmOperacaoFluxoManutencao;
    
    /** Atributo hrManutencaoRegistro. */
    private String hrManutencaoRegistro;
    
    /** Atributo qtDiaLctoFuturo. */
    private Integer qtDiaLctoFuturo;
    
    /** Atributo cdIndLctoFuturo. */
    private Integer cdIndLctoFuturo;
    
    /** Atributo descIndLctoFuturo. */
    private String descIndLctoFuturo;
    
    
    /**
     * Detalhar servico composto saida dto.
     */
    public DetalharServicoCompostoSaidaDTO() {
		super();
	}

	/**
	 * Detalhar servico composto saida dto.
	 *
	 * @param saida the saida
	 */
	public DetalharServicoCompostoSaidaDTO(Ocorrencias saida) {
    	this.cdServicoCompostoPagamento = saida.getCdServicoCompostoPagamento();
        this.dsServicoCompostoPagamento = saida.getDsServicoCompostoPagamento();
        this.cdTipoServicoCnab = saida.getCdTipoServicoCnab();
        this.dsTipoServicoCnab = saida.getDsTipoServicoCnab();
        this.cdFormaLancamentoCnab = saida.getCdFormaLancamentoCnab();
        this.dsFormaLancamentoCnab = saida.getDsFormaLancamentoCnab();
        this.cdFormaLiquidacao = saida.getCdFormaLiquidacao();
        this.dsFormaLiquidacao = saida.getDsFormaLiquidacao();
        
    	this.cdCanalInclusao = saida.getCdCanalInclusao();
        this.dsCanalInclusao = saida.getDsCanalInclusao();
        this.cdAutenticacaoSegurancaInclusao = saida.getCdAutenticacaoSegurancaInclusao();
        this.nmOperacaoFluxoInclusao = saida.getNmOperacaoFluxoInclusao();
        this.hrInclusaoRegistro = saida.getHrInclusaoRegistro();
        this.cdCanalManutencao = saida.getCdCanalManutencao();
        this.dsCanalManutencao = saida.getDsCanalManutencao();
        this.cdAutenticacaoSegurancaManutencao = saida.getCdAutenticacaoSegurancaManutencao();
        this.nmOperacaoFluxoManutencao = saida.getNmOperacaoFluxoManutencao();
        this.hrManutencaoRegistro = saida.getHrManutencaoRegistro();        
	}
    
	/**
	 * Get: cdAutenticacaoSegurancaInclusao.
	 *
	 * @return cdAutenticacaoSegurancaInclusao
	 */
	public String getCdAutenticacaoSegurancaInclusao() {
		return cdAutenticacaoSegurancaInclusao;
	}
	
	/**
	 * Set: cdAutenticacaoSegurancaInclusao.
	 *
	 * @param cdAutenticacaoSegurancaInclusao the cd autenticacao seguranca inclusao
	 */
	public void setCdAutenticacaoSegurancaInclusao(
			String cdAutenticacaoSegurancaInclusao) {
		this.cdAutenticacaoSegurancaInclusao = cdAutenticacaoSegurancaInclusao;
	}
	
	/**
	 * Get: cdAutenticacaoSegurancaManutencao.
	 *
	 * @return cdAutenticacaoSegurancaManutencao
	 */
	public String getCdAutenticacaoSegurancaManutencao() {
		return cdAutenticacaoSegurancaManutencao;
	}
	
	/**
	 * Set: cdAutenticacaoSegurancaManutencao.
	 *
	 * @param cdAutenticacaoSegurancaManutencao the cd autenticacao seguranca manutencao
	 */
	public void setCdAutenticacaoSegurancaManutencao(
			String cdAutenticacaoSegurancaManutencao) {
		this.cdAutenticacaoSegurancaManutencao = cdAutenticacaoSegurancaManutencao;
	}
	
	/**
	 * Get: cdCanalInclusao.
	 *
	 * @return cdCanalInclusao
	 */
	public Integer getCdCanalInclusao() {
		return cdCanalInclusao;
	}
	
	/**
	 * Set: cdCanalInclusao.
	 *
	 * @param cdCanalInclusao the cd canal inclusao
	 */
	public void setCdCanalInclusao(Integer cdCanalInclusao) {
		this.cdCanalInclusao = cdCanalInclusao;
	}
	
	/**
	 * Get: cdCanalManutencao.
	 *
	 * @return cdCanalManutencao
	 */
	public Integer getCdCanalManutencao() {
		return cdCanalManutencao;
	}
	
	/**
	 * Set: cdCanalManutencao.
	 *
	 * @param cdCanalManutencao the cd canal manutencao
	 */
	public void setCdCanalManutencao(Integer cdCanalManutencao) {
		this.cdCanalManutencao = cdCanalManutencao;
	}
	
	/**
	 * Get: cdFormaLancamentoCnab.
	 *
	 * @return cdFormaLancamentoCnab
	 */
	public Integer getCdFormaLancamentoCnab() {
		return cdFormaLancamentoCnab;
	}
	
	/**
	 * Set: cdFormaLancamentoCnab.
	 *
	 * @param cdFormaLancamentoCnab the cd forma lancamento cnab
	 */
	public void setCdFormaLancamentoCnab(Integer cdFormaLancamentoCnab) {
		this.cdFormaLancamentoCnab = cdFormaLancamentoCnab;
	}
	
	/**
	 * Get: cdFormaLiquidacao.
	 *
	 * @return cdFormaLiquidacao
	 */
	public Integer getCdFormaLiquidacao() {
		return cdFormaLiquidacao;
	}
	
	/**
	 * Set: cdFormaLiquidacao.
	 *
	 * @param cdFormaLiquidacao the cd forma liquidacao
	 */
	public void setCdFormaLiquidacao(Integer cdFormaLiquidacao) {
		this.cdFormaLiquidacao = cdFormaLiquidacao;
	}
	
	/**
	 * Get: cdServicoCompostoPagamento.
	 *
	 * @return cdServicoCompostoPagamento
	 */
	public Long getCdServicoCompostoPagamento() {
		return cdServicoCompostoPagamento;
	}
	
	/**
	 * Set: cdServicoCompostoPagamento.
	 *
	 * @param cdServicoCompostoPagamento the cd servico composto pagamento
	 */
	public void setCdServicoCompostoPagamento(Long cdServicoCompostoPagamento) {
		this.cdServicoCompostoPagamento = cdServicoCompostoPagamento;
	}
	
	/**
	 * Get: cdTipoServicoCnab.
	 *
	 * @return cdTipoServicoCnab
	 */
	public Integer getCdTipoServicoCnab() {
		return cdTipoServicoCnab;
	}
	
	/**
	 * Set: cdTipoServicoCnab.
	 *
	 * @param cdTipoServicoCnab the cd tipo servico cnab
	 */
	public void setCdTipoServicoCnab(Integer cdTipoServicoCnab) {
		this.cdTipoServicoCnab = cdTipoServicoCnab;
	}
	
	/**
	 * Get: dsCanalInclusao.
	 *
	 * @return dsCanalInclusao
	 */
	public String getDsCanalInclusao() {
		return dsCanalInclusao;
	}
	
	/**
	 * Set: dsCanalInclusao.
	 *
	 * @param dsCanalInclusao the ds canal inclusao
	 */
	public void setDsCanalInclusao(String dsCanalInclusao) {
		this.dsCanalInclusao = dsCanalInclusao;
	}
	
	/**
	 * Get: dsCanalManutencao.
	 *
	 * @return dsCanalManutencao
	 */
	public String getDsCanalManutencao() {
		return dsCanalManutencao;
	}
	
	/**
	 * Set: dsCanalManutencao.
	 *
	 * @param dsCanalManutencao the ds canal manutencao
	 */
	public void setDsCanalManutencao(String dsCanalManutencao) {
		this.dsCanalManutencao = dsCanalManutencao;
	}
	
	/**
	 * Get: dsFormaLancamentoCnab.
	 *
	 * @return dsFormaLancamentoCnab
	 */
	public String getDsFormaLancamentoCnab() {
		return dsFormaLancamentoCnab;
	}
	
	/**
	 * Set: dsFormaLancamentoCnab.
	 *
	 * @param dsFormaLancamentoCnab the ds forma lancamento cnab
	 */
	public void setDsFormaLancamentoCnab(String dsFormaLancamentoCnab) {
		this.dsFormaLancamentoCnab = dsFormaLancamentoCnab;
	}
	
	/**
	 * Get: dsFormaLiquidacao.
	 *
	 * @return dsFormaLiquidacao
	 */
	public String getDsFormaLiquidacao() {
		return dsFormaLiquidacao;
	}
	
	/**
	 * Set: dsFormaLiquidacao.
	 *
	 * @param dsFormaLiquidacao the ds forma liquidacao
	 */
	public void setDsFormaLiquidacao(String dsFormaLiquidacao) {
		this.dsFormaLiquidacao = dsFormaLiquidacao;
	}
	
	/**
	 * Get: dsServicoCompostoPagamento.
	 *
	 * @return dsServicoCompostoPagamento
	 */
	public String getDsServicoCompostoPagamento() {
		return dsServicoCompostoPagamento;
	}
	
	/**
	 * Set: dsServicoCompostoPagamento.
	 *
	 * @param dsServicoCompostoPagamento the ds servico composto pagamento
	 */
	public void setDsServicoCompostoPagamento(String dsServicoCompostoPagamento) {
		this.dsServicoCompostoPagamento = dsServicoCompostoPagamento;
	}
	
	/**
	 * Get: dsTipoServicoCnab.
	 *
	 * @return dsTipoServicoCnab
	 */
	public String getDsTipoServicoCnab() {
		return dsTipoServicoCnab;
	}
	
	/**
	 * Set: dsTipoServicoCnab.
	 *
	 * @param dsTipoServicoCnab the ds tipo servico cnab
	 */
	public void setDsTipoServicoCnab(String dsTipoServicoCnab) {
		this.dsTipoServicoCnab = dsTipoServicoCnab;
	}
	
	/**
	 * Get: hrInclusaoRegistro.
	 *
	 * @return hrInclusaoRegistro
	 */
	public String getHrInclusaoRegistro() {
		return hrInclusaoRegistro;
	}
	
	/**
	 * Set: hrInclusaoRegistro.
	 *
	 * @param hrInclusaoRegistro the hr inclusao registro
	 */
	public void setHrInclusaoRegistro(String hrInclusaoRegistro) {
		this.hrInclusaoRegistro = hrInclusaoRegistro;
	}
	
	/**
	 * Get: hrManutencaoRegistro.
	 *
	 * @return hrManutencaoRegistro
	 */
	public String getHrManutencaoRegistro() {
		return hrManutencaoRegistro;
	}
	
	/**
	 * Set: hrManutencaoRegistro.
	 *
	 * @param hrManutencaoRegistro the hr manutencao registro
	 */
	public void setHrManutencaoRegistro(String hrManutencaoRegistro) {
		this.hrManutencaoRegistro = hrManutencaoRegistro;
	}
	
	/**
	 * Get: nmOperacaoFluxoInclusao.
	 *
	 * @return nmOperacaoFluxoInclusao
	 */
	public String getNmOperacaoFluxoInclusao() {
		return nmOperacaoFluxoInclusao;
	}
	
	/**
	 * Set: nmOperacaoFluxoInclusao.
	 *
	 * @param nmOperacaoFluxoInclusao the nm operacao fluxo inclusao
	 */
	public void setNmOperacaoFluxoInclusao(String nmOperacaoFluxoInclusao) {
		this.nmOperacaoFluxoInclusao = nmOperacaoFluxoInclusao;
	}
	
	/**
	 * Get: nmOperacaoFluxoManutencao.
	 *
	 * @return nmOperacaoFluxoManutencao
	 */
	public String getNmOperacaoFluxoManutencao() {
		return nmOperacaoFluxoManutencao;
	}
	
	/**
	 * Set: nmOperacaoFluxoManutencao.
	 *
	 * @param nmOperacaoFluxoManutencao the nm operacao fluxo manutencao
	 */
	public void setNmOperacaoFluxoManutencao(String nmOperacaoFluxoManutencao) {
		this.nmOperacaoFluxoManutencao = nmOperacaoFluxoManutencao;
	}
	
	/**
	 * Get: qtDiaLctoFuturo.
	 *
	 * @return qtDiaLctoFuturo
	 */
	public Integer getQtDiaLctoFuturo() {
		return qtDiaLctoFuturo;
	}
	
	/**
	 * Set: qtDiaLctoFuturo.
	 *
	 * @param qtDiaLctoFuturo the qt dia lcto futuro
	 */
	public void setQtDiaLctoFuturo(Integer qtDiaLctoFuturo) {
		this.qtDiaLctoFuturo = qtDiaLctoFuturo;
	}
	
	/**
	 * Get: descIndLctoFuturo.
	 *
	 * @return descIndLctoFuturo
	 */
	public String getDescIndLctoFuturo() {
		return descIndLctoFuturo;
	}

	/**
	 * Set: descIndLctoFuturo.
	 *
	 * @param descIndLctoFuturo the desc ind lcto futuro
	 */
	public void setDescIndLctoFuturo(String descIndLctoFuturo) {
		this.descIndLctoFuturo = descIndLctoFuturo;
	}

	/**
	 * Get: cdIndLctoFuturo.
	 *
	 * @return cdIndLctoFuturo
	 */
	public Integer getCdIndLctoFuturo() {
		return cdIndLctoFuturo;
	}

	/**
	 * Set: cdIndLctoFuturo.
	 *
	 * @param cdIndLctoFuturo the cd ind lcto futuro
	 */
	public void setCdIndLctoFuturo(Integer cdIndLctoFuturo) {
		this.cdIndLctoFuturo = cdIndLctoFuturo;
	}
}
