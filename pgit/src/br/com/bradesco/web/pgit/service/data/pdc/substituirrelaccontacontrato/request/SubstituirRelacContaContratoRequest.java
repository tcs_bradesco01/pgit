/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.substituirrelaccontacontrato.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.util.Enumeration;
import java.util.Vector;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class SubstituirRelacContaContratoRequest.
 * 
 * @version $Revision$ $Date$
 */
public class SubstituirRelacContaContratoRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdPessoaJuridicaContrato
     */
    private long _cdPessoaJuridicaContrato = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridicaContrato
     */
    private boolean _has_cdPessoaJuridicaContrato;

    /**
     * Field _cdTipoContratoNegocio
     */
    private int _cdTipoContratoNegocio = 0;

    /**
     * keeps track of state for field: _cdTipoContratoNegocio
     */
    private boolean _has_cdTipoContratoNegocio;

    /**
     * Field _nrSequencialContratoNegocio
     */
    private long _nrSequencialContratoNegocio = 0;

    /**
     * keeps track of state for field: _nrSequencialContratoNegocio
     */
    private boolean _has_nrSequencialContratoNegocio;

    /**
     * Field _cdTipoVinculoContrato
     */
    private int _cdTipoVinculoContrato = 0;

    /**
     * keeps track of state for field: _cdTipoVinculoContrato
     */
    private boolean _has_cdTipoVinculoContrato;

    /**
     * Field _cdTipoRelacionamentoConta
     */
    private int _cdTipoRelacionamentoConta = 0;

    /**
     * keeps track of state for field: _cdTipoRelacionamentoConta
     */
    private boolean _has_cdTipoRelacionamentoConta;

    /**
     * Field _cdPessoaJuridicaRelacionada
     */
    private long _cdPessoaJuridicaRelacionada = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridicaRelacionada
     */
    private boolean _has_cdPessoaJuridicaRelacionada;

    /**
     * Field _cdTipoContratoRelacionado
     */
    private int _cdTipoContratoRelacionado = 0;

    /**
     * keeps track of state for field: _cdTipoContratoRelacionado
     */
    private boolean _has_cdTipoContratoRelacionado;

    /**
     * Field _nrSequenciaContratoRelacionado
     */
    private long _nrSequenciaContratoRelacionado = 0;

    /**
     * keeps track of state for field:
     * _nrSequenciaContratoRelacionado
     */
    private boolean _has_nrSequenciaContratoRelacionado;

    /**
     * Field _cdFinalidadeContaPagamento
     */
    private int _cdFinalidadeContaPagamento = 0;

    /**
     * keeps track of state for field: _cdFinalidadeContaPagamento
     */
    private boolean _has_cdFinalidadeContaPagamento;

    /**
     * Field _cdTipoInscricao
     */
    private java.lang.String _cdTipoInscricao;

    /**
     * Field _cpfCNPJ
     */
    private java.lang.String _cpfCNPJ;

    /**
     * Field _cdPssoa
     */
    private long _cdPssoa = 0;

    /**
     * keeps track of state for field: _cdPssoa
     */
    private boolean _has_cdPssoa;

    /**
     * Field _numeroOcorrencias
     */
    private int _numeroOcorrencias = 0;

    /**
     * keeps track of state for field: _numeroOcorrencias
     */
    private boolean _has_numeroOcorrencias;

    /**
     * Field _ocorrenciasList
     */
    private java.util.Vector _ocorrenciasList;


      //----------------/
     //- Constructors -/
    //----------------/

    public SubstituirRelacContaContratoRequest() 
     {
        super();
        _ocorrenciasList = new Vector();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.substituirrelaccontacontrato.request.SubstituirRelacContaContratoRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method addOcorrencias
     * 
     * 
     * 
     * @param vOcorrencias
     */
    public void addOcorrencias(br.com.bradesco.web.pgit.service.data.pdc.substituirrelaccontacontrato.request.Ocorrencias vOcorrencias)
        throws java.lang.IndexOutOfBoundsException
    {
        if (!(_ocorrenciasList.size() < 50)) {
            throw new IndexOutOfBoundsException("addOcorrencias has a maximum of 50");
        }
        _ocorrenciasList.addElement(vOcorrencias);
    } //-- void addOcorrencias(br.com.bradesco.web.pgit.service.data.pdc.substituirrelaccontacontrato.request.Ocorrencias) 

    /**
     * Method addOcorrencias
     * 
     * 
     * 
     * @param index
     * @param vOcorrencias
     */
    public void addOcorrencias(int index, br.com.bradesco.web.pgit.service.data.pdc.substituirrelaccontacontrato.request.Ocorrencias vOcorrencias)
        throws java.lang.IndexOutOfBoundsException
    {
        if (!(_ocorrenciasList.size() < 50)) {
            throw new IndexOutOfBoundsException("addOcorrencias has a maximum of 50");
        }
        _ocorrenciasList.insertElementAt(vOcorrencias, index);
    } //-- void addOcorrencias(int, br.com.bradesco.web.pgit.service.data.pdc.substituirrelaccontacontrato.request.Ocorrencias) 

    /**
     * Method deleteCdFinalidadeContaPagamento
     * 
     */
    public void deleteCdFinalidadeContaPagamento()
    {
        this._has_cdFinalidadeContaPagamento= false;
    } //-- void deleteCdFinalidadeContaPagamento() 

    /**
     * Method deleteCdPessoaJuridicaContrato
     * 
     */
    public void deleteCdPessoaJuridicaContrato()
    {
        this._has_cdPessoaJuridicaContrato= false;
    } //-- void deleteCdPessoaJuridicaContrato() 

    /**
     * Method deleteCdPessoaJuridicaRelacionada
     * 
     */
    public void deleteCdPessoaJuridicaRelacionada()
    {
        this._has_cdPessoaJuridicaRelacionada= false;
    } //-- void deleteCdPessoaJuridicaRelacionada() 

    /**
     * Method deleteCdPssoa
     * 
     */
    public void deleteCdPssoa()
    {
        this._has_cdPssoa= false;
    } //-- void deleteCdPssoa() 

    /**
     * Method deleteCdTipoContratoNegocio
     * 
     */
    public void deleteCdTipoContratoNegocio()
    {
        this._has_cdTipoContratoNegocio= false;
    } //-- void deleteCdTipoContratoNegocio() 

    /**
     * Method deleteCdTipoContratoRelacionado
     * 
     */
    public void deleteCdTipoContratoRelacionado()
    {
        this._has_cdTipoContratoRelacionado= false;
    } //-- void deleteCdTipoContratoRelacionado() 

    /**
     * Method deleteCdTipoRelacionamentoConta
     * 
     */
    public void deleteCdTipoRelacionamentoConta()
    {
        this._has_cdTipoRelacionamentoConta= false;
    } //-- void deleteCdTipoRelacionamentoConta() 

    /**
     * Method deleteCdTipoVinculoContrato
     * 
     */
    public void deleteCdTipoVinculoContrato()
    {
        this._has_cdTipoVinculoContrato= false;
    } //-- void deleteCdTipoVinculoContrato() 

    /**
     * Method deleteNrSequenciaContratoRelacionado
     * 
     */
    public void deleteNrSequenciaContratoRelacionado()
    {
        this._has_nrSequenciaContratoRelacionado= false;
    } //-- void deleteNrSequenciaContratoRelacionado() 

    /**
     * Method deleteNrSequencialContratoNegocio
     * 
     */
    public void deleteNrSequencialContratoNegocio()
    {
        this._has_nrSequencialContratoNegocio= false;
    } //-- void deleteNrSequencialContratoNegocio() 

    /**
     * Method deleteNumeroOcorrencias
     * 
     */
    public void deleteNumeroOcorrencias()
    {
        this._has_numeroOcorrencias= false;
    } //-- void deleteNumeroOcorrencias() 

    /**
     * Method enumerateOcorrencias
     * 
     * 
     * 
     * @return Enumeration
     */
    public java.util.Enumeration enumerateOcorrencias()
    {
        return _ocorrenciasList.elements();
    } //-- java.util.Enumeration enumerateOcorrencias() 

    /**
     * Returns the value of field 'cdFinalidadeContaPagamento'.
     * 
     * @return int
     * @return the value of field 'cdFinalidadeContaPagamento'.
     */
    public int getCdFinalidadeContaPagamento()
    {
        return this._cdFinalidadeContaPagamento;
    } //-- int getCdFinalidadeContaPagamento() 

    /**
     * Returns the value of field 'cdPessoaJuridicaContrato'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridicaContrato'.
     */
    public long getCdPessoaJuridicaContrato()
    {
        return this._cdPessoaJuridicaContrato;
    } //-- long getCdPessoaJuridicaContrato() 

    /**
     * Returns the value of field 'cdPessoaJuridicaRelacionada'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridicaRelacionada'.
     */
    public long getCdPessoaJuridicaRelacionada()
    {
        return this._cdPessoaJuridicaRelacionada;
    } //-- long getCdPessoaJuridicaRelacionada() 

    /**
     * Returns the value of field 'cdPssoa'.
     * 
     * @return long
     * @return the value of field 'cdPssoa'.
     */
    public long getCdPssoa()
    {
        return this._cdPssoa;
    } //-- long getCdPssoa() 

    /**
     * Returns the value of field 'cdTipoContratoNegocio'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoNegocio'.
     */
    public int getCdTipoContratoNegocio()
    {
        return this._cdTipoContratoNegocio;
    } //-- int getCdTipoContratoNegocio() 

    /**
     * Returns the value of field 'cdTipoContratoRelacionado'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoRelacionado'.
     */
    public int getCdTipoContratoRelacionado()
    {
        return this._cdTipoContratoRelacionado;
    } //-- int getCdTipoContratoRelacionado() 

    /**
     * Returns the value of field 'cdTipoInscricao'.
     * 
     * @return String
     * @return the value of field 'cdTipoInscricao'.
     */
    public java.lang.String getCdTipoInscricao()
    {
        return this._cdTipoInscricao;
    } //-- java.lang.String getCdTipoInscricao() 

    /**
     * Returns the value of field 'cdTipoRelacionamentoConta'.
     * 
     * @return int
     * @return the value of field 'cdTipoRelacionamentoConta'.
     */
    public int getCdTipoRelacionamentoConta()
    {
        return this._cdTipoRelacionamentoConta;
    } //-- int getCdTipoRelacionamentoConta() 

    /**
     * Returns the value of field 'cdTipoVinculoContrato'.
     * 
     * @return int
     * @return the value of field 'cdTipoVinculoContrato'.
     */
    public int getCdTipoVinculoContrato()
    {
        return this._cdTipoVinculoContrato;
    } //-- int getCdTipoVinculoContrato() 

    /**
     * Returns the value of field 'cpfCNPJ'.
     * 
     * @return String
     * @return the value of field 'cpfCNPJ'.
     */
    public java.lang.String getCpfCNPJ()
    {
        return this._cpfCNPJ;
    } //-- java.lang.String getCpfCNPJ() 

    /**
     * Returns the value of field 'nrSequenciaContratoRelacionado'.
     * 
     * @return long
     * @return the value of field 'nrSequenciaContratoRelacionado'.
     */
    public long getNrSequenciaContratoRelacionado()
    {
        return this._nrSequenciaContratoRelacionado;
    } //-- long getNrSequenciaContratoRelacionado() 

    /**
     * Returns the value of field 'nrSequencialContratoNegocio'.
     * 
     * @return long
     * @return the value of field 'nrSequencialContratoNegocio'.
     */
    public long getNrSequencialContratoNegocio()
    {
        return this._nrSequencialContratoNegocio;
    } //-- long getNrSequencialContratoNegocio() 

    /**
     * Returns the value of field 'numeroOcorrencias'.
     * 
     * @return int
     * @return the value of field 'numeroOcorrencias'.
     */
    public int getNumeroOcorrencias()
    {
        return this._numeroOcorrencias;
    } //-- int getNumeroOcorrencias() 

    /**
     * Method getOcorrencias
     * 
     * 
     * 
     * @param index
     * @return Ocorrencias
     */
    public br.com.bradesco.web.pgit.service.data.pdc.substituirrelaccontacontrato.request.Ocorrencias getOcorrencias(int index)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index >= _ocorrenciasList.size())) {
            throw new IndexOutOfBoundsException("getOcorrencias: Index value '"+index+"' not in range [0.."+(_ocorrenciasList.size() - 1) + "]");
        }
        
        return (br.com.bradesco.web.pgit.service.data.pdc.substituirrelaccontacontrato.request.Ocorrencias) _ocorrenciasList.elementAt(index);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.substituirrelaccontacontrato.request.Ocorrencias getOcorrencias(int) 

    /**
     * Method getOcorrencias
     * 
     * 
     * 
     * @return Ocorrencias
     */
    public br.com.bradesco.web.pgit.service.data.pdc.substituirrelaccontacontrato.request.Ocorrencias[] getOcorrencias()
    {
        int size = _ocorrenciasList.size();
        br.com.bradesco.web.pgit.service.data.pdc.substituirrelaccontacontrato.request.Ocorrencias[] mArray = new br.com.bradesco.web.pgit.service.data.pdc.substituirrelaccontacontrato.request.Ocorrencias[size];
        for (int index = 0; index < size; index++) {
            mArray[index] = (br.com.bradesco.web.pgit.service.data.pdc.substituirrelaccontacontrato.request.Ocorrencias) _ocorrenciasList.elementAt(index);
        }
        return mArray;
    } //-- br.com.bradesco.web.pgit.service.data.pdc.substituirrelaccontacontrato.request.Ocorrencias[] getOcorrencias() 

    /**
     * Method getOcorrenciasCount
     * 
     * 
     * 
     * @return int
     */
    public int getOcorrenciasCount()
    {
        return _ocorrenciasList.size();
    } //-- int getOcorrenciasCount() 

    /**
     * Method hasCdFinalidadeContaPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFinalidadeContaPagamento()
    {
        return this._has_cdFinalidadeContaPagamento;
    } //-- boolean hasCdFinalidadeContaPagamento() 

    /**
     * Method hasCdPessoaJuridicaContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridicaContrato()
    {
        return this._has_cdPessoaJuridicaContrato;
    } //-- boolean hasCdPessoaJuridicaContrato() 

    /**
     * Method hasCdPessoaJuridicaRelacionada
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridicaRelacionada()
    {
        return this._has_cdPessoaJuridicaRelacionada;
    } //-- boolean hasCdPessoaJuridicaRelacionada() 

    /**
     * Method hasCdPssoa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPssoa()
    {
        return this._has_cdPssoa;
    } //-- boolean hasCdPssoa() 

    /**
     * Method hasCdTipoContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoNegocio()
    {
        return this._has_cdTipoContratoNegocio;
    } //-- boolean hasCdTipoContratoNegocio() 

    /**
     * Method hasCdTipoContratoRelacionado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoRelacionado()
    {
        return this._has_cdTipoContratoRelacionado;
    } //-- boolean hasCdTipoContratoRelacionado() 

    /**
     * Method hasCdTipoRelacionamentoConta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoRelacionamentoConta()
    {
        return this._has_cdTipoRelacionamentoConta;
    } //-- boolean hasCdTipoRelacionamentoConta() 

    /**
     * Method hasCdTipoVinculoContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoVinculoContrato()
    {
        return this._has_cdTipoVinculoContrato;
    } //-- boolean hasCdTipoVinculoContrato() 

    /**
     * Method hasNrSequenciaContratoRelacionado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaContratoRelacionado()
    {
        return this._has_nrSequenciaContratoRelacionado;
    } //-- boolean hasNrSequenciaContratoRelacionado() 

    /**
     * Method hasNrSequencialContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequencialContratoNegocio()
    {
        return this._has_nrSequencialContratoNegocio;
    } //-- boolean hasNrSequencialContratoNegocio() 

    /**
     * Method hasNumeroOcorrencias
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNumeroOcorrencias()
    {
        return this._has_numeroOcorrencias;
    } //-- boolean hasNumeroOcorrencias() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Method removeAllOcorrencias
     * 
     */
    public void removeAllOcorrencias()
    {
        _ocorrenciasList.removeAllElements();
    } //-- void removeAllOcorrencias() 

    /**
     * Method removeOcorrencias
     * 
     * 
     * 
     * @param index
     * @return Ocorrencias
     */
    public br.com.bradesco.web.pgit.service.data.pdc.substituirrelaccontacontrato.request.Ocorrencias removeOcorrencias(int index)
    {
        java.lang.Object obj = _ocorrenciasList.elementAt(index);
        _ocorrenciasList.removeElementAt(index);
        return (br.com.bradesco.web.pgit.service.data.pdc.substituirrelaccontacontrato.request.Ocorrencias) obj;
    } //-- br.com.bradesco.web.pgit.service.data.pdc.substituirrelaccontacontrato.request.Ocorrencias removeOcorrencias(int) 

    /**
     * Sets the value of field 'cdFinalidadeContaPagamento'.
     * 
     * @param cdFinalidadeContaPagamento the value of field
     * 'cdFinalidadeContaPagamento'.
     */
    public void setCdFinalidadeContaPagamento(int cdFinalidadeContaPagamento)
    {
        this._cdFinalidadeContaPagamento = cdFinalidadeContaPagamento;
        this._has_cdFinalidadeContaPagamento = true;
    } //-- void setCdFinalidadeContaPagamento(int) 

    /**
     * Sets the value of field 'cdPessoaJuridicaContrato'.
     * 
     * @param cdPessoaJuridicaContrato the value of field
     * 'cdPessoaJuridicaContrato'.
     */
    public void setCdPessoaJuridicaContrato(long cdPessoaJuridicaContrato)
    {
        this._cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
        this._has_cdPessoaJuridicaContrato = true;
    } //-- void setCdPessoaJuridicaContrato(long) 

    /**
     * Sets the value of field 'cdPessoaJuridicaRelacionada'.
     * 
     * @param cdPessoaJuridicaRelacionada the value of field
     * 'cdPessoaJuridicaRelacionada'.
     */
    public void setCdPessoaJuridicaRelacionada(long cdPessoaJuridicaRelacionada)
    {
        this._cdPessoaJuridicaRelacionada = cdPessoaJuridicaRelacionada;
        this._has_cdPessoaJuridicaRelacionada = true;
    } //-- void setCdPessoaJuridicaRelacionada(long) 

    /**
     * Sets the value of field 'cdPssoa'.
     * 
     * @param cdPssoa the value of field 'cdPssoa'.
     */
    public void setCdPssoa(long cdPssoa)
    {
        this._cdPssoa = cdPssoa;
        this._has_cdPssoa = true;
    } //-- void setCdPssoa(long) 

    /**
     * Sets the value of field 'cdTipoContratoNegocio'.
     * 
     * @param cdTipoContratoNegocio the value of field
     * 'cdTipoContratoNegocio'.
     */
    public void setCdTipoContratoNegocio(int cdTipoContratoNegocio)
    {
        this._cdTipoContratoNegocio = cdTipoContratoNegocio;
        this._has_cdTipoContratoNegocio = true;
    } //-- void setCdTipoContratoNegocio(int) 

    /**
     * Sets the value of field 'cdTipoContratoRelacionado'.
     * 
     * @param cdTipoContratoRelacionado the value of field
     * 'cdTipoContratoRelacionado'.
     */
    public void setCdTipoContratoRelacionado(int cdTipoContratoRelacionado)
    {
        this._cdTipoContratoRelacionado = cdTipoContratoRelacionado;
        this._has_cdTipoContratoRelacionado = true;
    } //-- void setCdTipoContratoRelacionado(int) 

    /**
     * Sets the value of field 'cdTipoInscricao'.
     * 
     * @param cdTipoInscricao the value of field 'cdTipoInscricao'.
     */
    public void setCdTipoInscricao(java.lang.String cdTipoInscricao)
    {
        this._cdTipoInscricao = cdTipoInscricao;
    } //-- void setCdTipoInscricao(java.lang.String) 

    /**
     * Sets the value of field 'cdTipoRelacionamentoConta'.
     * 
     * @param cdTipoRelacionamentoConta the value of field
     * 'cdTipoRelacionamentoConta'.
     */
    public void setCdTipoRelacionamentoConta(int cdTipoRelacionamentoConta)
    {
        this._cdTipoRelacionamentoConta = cdTipoRelacionamentoConta;
        this._has_cdTipoRelacionamentoConta = true;
    } //-- void setCdTipoRelacionamentoConta(int) 

    /**
     * Sets the value of field 'cdTipoVinculoContrato'.
     * 
     * @param cdTipoVinculoContrato the value of field
     * 'cdTipoVinculoContrato'.
     */
    public void setCdTipoVinculoContrato(int cdTipoVinculoContrato)
    {
        this._cdTipoVinculoContrato = cdTipoVinculoContrato;
        this._has_cdTipoVinculoContrato = true;
    } //-- void setCdTipoVinculoContrato(int) 

    /**
     * Sets the value of field 'cpfCNPJ'.
     * 
     * @param cpfCNPJ the value of field 'cpfCNPJ'.
     */
    public void setCpfCNPJ(java.lang.String cpfCNPJ)
    {
        this._cpfCNPJ = cpfCNPJ;
    } //-- void setCpfCNPJ(java.lang.String) 

    /**
     * Sets the value of field 'nrSequenciaContratoRelacionado'.
     * 
     * @param nrSequenciaContratoRelacionado the value of field
     * 'nrSequenciaContratoRelacionado'.
     */
    public void setNrSequenciaContratoRelacionado(long nrSequenciaContratoRelacionado)
    {
        this._nrSequenciaContratoRelacionado = nrSequenciaContratoRelacionado;
        this._has_nrSequenciaContratoRelacionado = true;
    } //-- void setNrSequenciaContratoRelacionado(long) 

    /**
     * Sets the value of field 'nrSequencialContratoNegocio'.
     * 
     * @param nrSequencialContratoNegocio the value of field
     * 'nrSequencialContratoNegocio'.
     */
    public void setNrSequencialContratoNegocio(long nrSequencialContratoNegocio)
    {
        this._nrSequencialContratoNegocio = nrSequencialContratoNegocio;
        this._has_nrSequencialContratoNegocio = true;
    } //-- void setNrSequencialContratoNegocio(long) 

    /**
     * Sets the value of field 'numeroOcorrencias'.
     * 
     * @param numeroOcorrencias the value of field
     * 'numeroOcorrencias'.
     */
    public void setNumeroOcorrencias(int numeroOcorrencias)
    {
        this._numeroOcorrencias = numeroOcorrencias;
        this._has_numeroOcorrencias = true;
    } //-- void setNumeroOcorrencias(int) 

    /**
     * Method setOcorrencias
     * 
     * 
     * 
     * @param index
     * @param vOcorrencias
     */
    public void setOcorrencias(int index, br.com.bradesco.web.pgit.service.data.pdc.substituirrelaccontacontrato.request.Ocorrencias vOcorrencias)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index >= _ocorrenciasList.size())) {
            throw new IndexOutOfBoundsException("setOcorrencias: Index value '"+index+"' not in range [0.." + (_ocorrenciasList.size() - 1) + "]");
        }
        if (!(index < 50)) {
            throw new IndexOutOfBoundsException("setOcorrencias has a maximum of 50");
        }
        _ocorrenciasList.setElementAt(vOcorrencias, index);
    } //-- void setOcorrencias(int, br.com.bradesco.web.pgit.service.data.pdc.substituirrelaccontacontrato.request.Ocorrencias) 

    /**
     * Method setOcorrencias
     * 
     * 
     * 
     * @param ocorrenciasArray
     */
    public void setOcorrencias(br.com.bradesco.web.pgit.service.data.pdc.substituirrelaccontacontrato.request.Ocorrencias[] ocorrenciasArray)
    {
        //-- copy array
        _ocorrenciasList.removeAllElements();
        for (int i = 0; i < ocorrenciasArray.length; i++) {
            _ocorrenciasList.addElement(ocorrenciasArray[i]);
        }
    } //-- void setOcorrencias(br.com.bradesco.web.pgit.service.data.pdc.substituirrelaccontacontrato.request.Ocorrencias) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return SubstituirRelacContaContratoRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.substituirrelaccontacontrato.request.SubstituirRelacContaContratoRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.substituirrelaccontacontrato.request.SubstituirRelacContaContratoRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.substituirrelaccontacontrato.request.SubstituirRelacContaContratoRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.substituirrelaccontacontrato.request.SubstituirRelacContaContratoRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
