/*
 * Nome: br.com.bradesco.web.pgit.service.business.solmanutcontratos.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.solmanutcontratos.bean;

/**
 * Nome: ListarManutencaoContratoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarManutencaoContratoSaidaDTO {

	/** Atributo cdIndicadorTipoManutencao. */
	private int cdIndicadorTipoManutencao;	
	
	/** Atributo cdManutencao. */
	private long cdManutencao;
	
	/** Atributo cdSituacaoManutencaoContrato. */
	private int cdSituacaoManutencaoContrato;
	
	/** Atributo cdTipoManutencaoContrato. */
	private int cdTipoManutencaoContrato;
	
	/** Atributo dsIndicadorTipoManutencao. */
	private String dsIndicadorTipoManutencao;
	
	/** Atributo dsManutencaoContrato. */
	private String dsManutencaoContrato;
	
	/** Atributo dsSituacaoManutencaoContrato. */
	private String dsSituacaoManutencaoContrato;
	
	/** Atributo dsTipoManutencaoContrato. */
	private String dsTipoManutencaoContrato;
	
	/** Atributo hrManutencaoRegistro. */
	private String hrManutencaoRegistro;
	
	
	/**
	 * Get: cdIndicadorTipoManutencao.
	 *
	 * @return cdIndicadorTipoManutencao
	 */
	public int getCdIndicadorTipoManutencao() {
		return cdIndicadorTipoManutencao;
	}
	
	/**
	 * Set: cdIndicadorTipoManutencao.
	 *
	 * @param cdIndicadorTipoManutencao the cd indicador tipo manutencao
	 */
	public void setCdIndicadorTipoManutencao(int cdIndicadorTipoManutencao) {
		this.cdIndicadorTipoManutencao = cdIndicadorTipoManutencao;
	}
	
	/**
	 * Get: cdManutencao.
	 *
	 * @return cdManutencao
	 */
	public long getCdManutencao() {
		return cdManutencao;
	}
	
	/**
	 * Set: cdManutencao.
	 *
	 * @param cdManutencao the cd manutencao
	 */
	public void setCdManutencao(long cdManutencao) {
		this.cdManutencao = cdManutencao;
	}
	
	/**
	 * Get: cdSituacaoManutencaoContrato.
	 *
	 * @return cdSituacaoManutencaoContrato
	 */
	public int getCdSituacaoManutencaoContrato() {
		return cdSituacaoManutencaoContrato;
	}
	
	/**
	 * Set: cdSituacaoManutencaoContrato.
	 *
	 * @param cdSituacaoManutencaoContrato the cd situacao manutencao contrato
	 */
	public void setCdSituacaoManutencaoContrato(int cdSituacaoManutencaoContrato) {
		this.cdSituacaoManutencaoContrato = cdSituacaoManutencaoContrato;
	}
	
	/**
	 * Get: cdTipoManutencaoContrato.
	 *
	 * @return cdTipoManutencaoContrato
	 */
	public int getCdTipoManutencaoContrato() {
		return cdTipoManutencaoContrato;
	}
	
	/**
	 * Set: cdTipoManutencaoContrato.
	 *
	 * @param cdTipoManutencaoContrato the cd tipo manutencao contrato
	 */
	public void setCdTipoManutencaoContrato(int cdTipoManutencaoContrato) {
		this.cdTipoManutencaoContrato = cdTipoManutencaoContrato;
	}
	
	/**
	 * Get: dsIndicadorTipoManutencao.
	 *
	 * @return dsIndicadorTipoManutencao
	 */
	public String getDsIndicadorTipoManutencao() {
		return dsIndicadorTipoManutencao;
	}
	
	/**
	 * Set: dsIndicadorTipoManutencao.
	 *
	 * @param dsIndicadorTipoManutencao the ds indicador tipo manutencao
	 */
	public void setDsIndicadorTipoManutencao(String dsIndicadorTipoManutencao) {
		this.dsIndicadorTipoManutencao = dsIndicadorTipoManutencao;
	}
	
	/**
	 * Get: dsManutencaoContrato.
	 *
	 * @return dsManutencaoContrato
	 */
	public String getDsManutencaoContrato() {
		return dsManutencaoContrato;
	}
	
	/**
	 * Set: dsManutencaoContrato.
	 *
	 * @param dsManutencaoContrato the ds manutencao contrato
	 */
	public void setDsManutencaoContrato(String dsManutencaoContrato) {
		this.dsManutencaoContrato = dsManutencaoContrato;
	}
	
	/**
	 * Get: dsSituacaoManutencaoContrato.
	 *
	 * @return dsSituacaoManutencaoContrato
	 */
	public String getDsSituacaoManutencaoContrato() {
		return dsSituacaoManutencaoContrato;
	}
	
	/**
	 * Set: dsSituacaoManutencaoContrato.
	 *
	 * @param dsSituacaoManutencaoContrato the ds situacao manutencao contrato
	 */
	public void setDsSituacaoManutencaoContrato(String dsSituacaoManutencaoContrato) {
		this.dsSituacaoManutencaoContrato = dsSituacaoManutencaoContrato;
	}
	
	/**
	 * Get: dsTipoManutencaoContrato.
	 *
	 * @return dsTipoManutencaoContrato
	 */
	public String getDsTipoManutencaoContrato() {
		return dsTipoManutencaoContrato;
	}
	
	/**
	 * Set: dsTipoManutencaoContrato.
	 *
	 * @param dsTipoManutencaoContrato the ds tipo manutencao contrato
	 */
	public void setDsTipoManutencaoContrato(String dsTipoManutencaoContrato) {
		this.dsTipoManutencaoContrato = dsTipoManutencaoContrato;
	}
	
	/**
	 * Get: hrManutencaoRegistro.
	 *
	 * @return hrManutencaoRegistro
	 */
	public String getHrManutencaoRegistro() {
		return hrManutencaoRegistro;
	}
	
	/**
	 * Set: hrManutencaoRegistro.
	 *
	 * @param hrManutencaoRegistro the hr manutencao registro
	 */
	public void setHrManutencaoRegistro(String hrManutencaoRegistro) {
		this.hrManutencaoRegistro = hrManutencaoRegistro;
	}
	
	
	
}
