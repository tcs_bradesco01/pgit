/*
 * Nome: br.com.bradesco.web.pgit.service.business.geral
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.geral;

import br.com.bradesco.web.pgit.utils.PgitUtil;


/**
 * Arquivo criado em 06/12/11.
 */
public class MensagemSaida {

	/** Atributo codMensagem. */
	private String codMensagem;

	/** Atributo mensagem. */
	private String mensagem;

	/**
	 * Mensagem saida.
	 *
	 * @param codMensagem the cod mensagem
	 * @param mensagem the mensagem
	 */
	public MensagemSaida(String codMensagem, String mensagem) {
		super();
		this.codMensagem = codMensagem;
		this.mensagem = mensagem;
	}
	
	/**
	 * Mensagem saida.
	 */
	public MensagemSaida() {
		super();
	}

	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}

	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}

	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}

	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	/**
	 * Get: mensagemFormatada.
	 *
	 * @return mensagemFormatada
	 */
	public String getMensagemFormatada() {
		if (PgitUtil.isStringVazio(codMensagem) || PgitUtil.isStringVazio(mensagem)) {
			return "";
		}
		
		return String.format("(%s) %s", codMensagem, mensagem);
	}
}