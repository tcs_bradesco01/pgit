/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.detalhartiposervmodcontrato.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;

/**
 * Class DetalharTipoServModContratoResponse.
 * 
 * @version $Revision$ $Date$
 */
public class DetalharTipoServModContratoResponse implements java.io.Serializable {


      /**
	 * 
	 */
	private static final long serialVersionUID = 1150985148737363126L;

	//--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _codMensagem
     */
    private java.lang.String _codMensagem;

    /**
     * Field _mensagem
     */
    private java.lang.String _mensagem;

    /**
     * Field _cdAcaoNaoVida
     */
    private int _cdAcaoNaoVida = 0;

    /**
     * keeps track of state for field: _cdAcaoNaoVida
     */
    private boolean _has_cdAcaoNaoVida;

    /**
     * Field _dsAcaoNaoVida
     */
    private java.lang.String _dsAcaoNaoVida;

    /**
     * Field _cdAcertoDadoRecadastro
     */
    private int _cdAcertoDadoRecadastro = 0;

    /**
     * keeps track of state for field: _cdAcertoDadoRecadastro
     */
    private boolean _has_cdAcertoDadoRecadastro;

    /**
     * Field _dsAcertoDadoRecadastro
     */
    private java.lang.String _dsAcertoDadoRecadastro;

    /**
     * Field _cdAgendaDebitoVeiculo
     */
    private int _cdAgendaDebitoVeiculo = 0;

    /**
     * keeps track of state for field: _cdAgendaDebitoVeiculo
     */
    private boolean _has_cdAgendaDebitoVeiculo;

    /**
     * Field _dsAgendaDebitoVeiculo
     */
    private java.lang.String _dsAgendaDebitoVeiculo;

    /**
     * Field _cdAgendaPagamentoVencido
     */
    private int _cdAgendaPagamentoVencido = 0;

    /**
     * keeps track of state for field: _cdAgendaPagamentoVencido
     */
    private boolean _has_cdAgendaPagamentoVencido;

    /**
     * Field _dsAgendaPagamentoVencido
     */
    private java.lang.String _dsAgendaPagamentoVencido;

    /**
     * Field _cdAgendaValorMenor
     */
    private int _cdAgendaValorMenor = 0;

    /**
     * keeps track of state for field: _cdAgendaValorMenor
     */
    private boolean _has_cdAgendaValorMenor;

    /**
     * Field _dsAgendaValorMenor
     */
    private java.lang.String _dsAgendaValorMenor;

    /**
     * Field _cdAgrupamentoAviso
     */
    private int _cdAgrupamentoAviso = 0;

    /**
     * keeps track of state for field: _cdAgrupamentoAviso
     */
    private boolean _has_cdAgrupamentoAviso;

    /**
     * Field _dsAgrupamentoAviso
     */
    private java.lang.String _dsAgrupamentoAviso;

    /**
     * Field _cdAgrupamentoComprovado
     */
    private int _cdAgrupamentoComprovado = 0;

    /**
     * keeps track of state for field: _cdAgrupamentoComprovado
     */
    private boolean _has_cdAgrupamentoComprovado;

    /**
     * Field _dsAgrupamentoComprovado
     */
    private java.lang.String _dsAgrupamentoComprovado;

    /**
     * Field _cdAgrupamentoFormularioRecadastro
     */
    private int _cdAgrupamentoFormularioRecadastro = 0;

    /**
     * keeps track of state for field:
     * _cdAgrupamentoFormularioRecadastro
     */
    private boolean _has_cdAgrupamentoFormularioRecadastro;

    /**
     * Field _dsAgrupamentoFormularioRecadastro
     */
    private java.lang.String _dsAgrupamentoFormularioRecadastro;

    /**
     * Field _cdAntecRecadastroBeneficio
     */
    private int _cdAntecRecadastroBeneficio = 0;

    /**
     * keeps track of state for field: _cdAntecRecadastroBeneficio
     */
    private boolean _has_cdAntecRecadastroBeneficio;

    /**
     * Field _dsAntecRecadastroBeneficio
     */
    private java.lang.String _dsAntecRecadastroBeneficio;

    /**
     * Field _cdAreaReservada
     */
    private int _cdAreaReservada = 0;

    /**
     * keeps track of state for field: _cdAreaReservada
     */
    private boolean _has_cdAreaReservada;

    /**
     * Field _dsAreaReservada
     */
    private java.lang.String _dsAreaReservada;

    /**
     * Field _cdBaseRecadastroBeneficio
     */
    private int _cdBaseRecadastroBeneficio = 0;

    /**
     * keeps track of state for field: _cdBaseRecadastroBeneficio
     */
    private boolean _has_cdBaseRecadastroBeneficio;

    /**
     * Field _dsBaseRecadastroBeneficio
     */
    private java.lang.String _dsBaseRecadastroBeneficio;

    /**
     * Field _cdBloqueioEmissaoPplta
     */
    private int _cdBloqueioEmissaoPplta = 0;

    /**
     * keeps track of state for field: _cdBloqueioEmissaoPplta
     */
    private boolean _has_cdBloqueioEmissaoPplta;

    /**
     * Field _dsBloqueioEmissaoPplta
     */
    private java.lang.String _dsBloqueioEmissaoPplta;

    /**
     * Field _cdCapituloTituloRegistro
     */
    private int _cdCapituloTituloRegistro = 0;

    /**
     * keeps track of state for field: _cdCapituloTituloRegistro
     */
    private boolean _has_cdCapituloTituloRegistro;

    /**
     * Field _dsCapituloTituloRegistro
     */
    private java.lang.String _dsCapituloTituloRegistro;

    /**
     * Field _cdCobrancaTarifa
     */
    private int _cdCobrancaTarifa = 0;

    /**
     * keeps track of state for field: _cdCobrancaTarifa
     */
    private boolean _has_cdCobrancaTarifa;

    /**
     * Field _dsCobrancaTarifa
     */
    private java.lang.String _dsCobrancaTarifa;

    /**
     * Field _cdConsDebitoVeiculo
     */
    private int _cdConsDebitoVeiculo = 0;

    /**
     * keeps track of state for field: _cdConsDebitoVeiculo
     */
    private boolean _has_cdConsDebitoVeiculo;

    /**
     * Field _dsConsDebitoVeiculo
     */
    private java.lang.String _dsConsDebitoVeiculo;

    /**
     * Field _cdConsEndereco
     */
    private int _cdConsEndereco = 0;

    /**
     * keeps track of state for field: _cdConsEndereco
     */
    private boolean _has_cdConsEndereco;

    /**
     * Field _dsConsEndereco
     */
    private java.lang.String _dsConsEndereco;

    /**
     * Field _cdConsSaldoPagamento
     */
    private int _cdConsSaldoPagamento = 0;

    /**
     * keeps track of state for field: _cdConsSaldoPagamento
     */
    private boolean _has_cdConsSaldoPagamento;

    /**
     * Field _dsConsSaldoPagamento
     */
    private java.lang.String _dsConsSaldoPagamento;

    /**
     * Field _cdContagemConsSaudo
     */
    private int _cdContagemConsSaudo = 0;

    /**
     * keeps track of state for field: _cdContagemConsSaudo
     */
    private boolean _has_cdContagemConsSaudo;

    /**
     * Field _dsContagemConsSaudo
     */
    private java.lang.String _dsContagemConsSaudo;

    /**
     * Field _cdCreditoNaoUtilizado
     */
    private int _cdCreditoNaoUtilizado = 0;

    /**
     * keeps track of state for field: _cdCreditoNaoUtilizado
     */
    private boolean _has_cdCreditoNaoUtilizado;

    /**
     * Field _dsCreditoNaoUtilizado
     */
    private java.lang.String _dsCreditoNaoUtilizado;

    /**
     * Field _cdCriterioEnquaBeneficio
     */
    private int _cdCriterioEnquaBeneficio = 0;

    /**
     * keeps track of state for field: _cdCriterioEnquaBeneficio
     */
    private boolean _has_cdCriterioEnquaBeneficio;

    /**
     * Field _dsCriterioEnquaBeneficio
     */
    private java.lang.String _dsCriterioEnquaBeneficio;

    /**
     * Field _cdCriterioEnquaRecadastro
     */
    private int _cdCriterioEnquaRecadastro = 0;

    /**
     * keeps track of state for field: _cdCriterioEnquaRecadastro
     */
    private boolean _has_cdCriterioEnquaRecadastro;

    /**
     * Field _dsCriterioEnquaRecadastro
     */
    private java.lang.String _dsCriterioEnquaRecadastro;

    /**
     * Field _cdCriterioRastreabilidadeTitulo
     */
    private int _cdCriterioRastreabilidadeTitulo = 0;

    /**
     * keeps track of state for field:
     * _cdCriterioRastreabilidadeTitulo
     */
    private boolean _has_cdCriterioRastreabilidadeTitulo;

    /**
     * Field _dsCriterioRastreabilidadeTitulo
     */
    private java.lang.String _dsCriterioRastreabilidadeTitulo;

    /**
     * Field _cdCtciaEspecieBeneficio
     */
    private int _cdCtciaEspecieBeneficio = 0;

    /**
     * keeps track of state for field: _cdCtciaEspecieBeneficio
     */
    private boolean _has_cdCtciaEspecieBeneficio;

    /**
     * Field _dsCtciaEspecieBeneficio
     */
    private java.lang.String _dsCtciaEspecieBeneficio;

    /**
     * Field _cdCtciaIdentificacaoBeneficio
     */
    private int _cdCtciaIdentificacaoBeneficio = 0;

    /**
     * keeps track of state for field: _cdCtciaIdentificacaoBenefici
     */
    private boolean _has_cdCtciaIdentificacaoBeneficio;

    /**
     * Field _dsCtciaIdentificacaoBeneficio
     */
    private java.lang.String _dsCtciaIdentificacaoBeneficio;

    /**
     * Field _cdCtciaInscricaoFavorecido
     */
    private int _cdCtciaInscricaoFavorecido = 0;

    /**
     * keeps track of state for field: _cdCtciaInscricaoFavorecido
     */
    private boolean _has_cdCtciaInscricaoFavorecido;

    /**
     * Field _dsCtciaInscricaoFavorecido
     */
    private java.lang.String _dsCtciaInscricaoFavorecido;

    /**
     * Field _cdCtciaProprietarioVeiculo
     */
    private int _cdCtciaProprietarioVeiculo = 0;

    /**
     * keeps track of state for field: _cdCtciaProprietarioVeiculo
     */
    private boolean _has_cdCtciaProprietarioVeiculo;

    /**
     * Field _dsCtciaProprietarioVeiculo
     */
    private java.lang.String _dsCtciaProprietarioVeiculo;

    /**
     * Field _cdDispzContaCredito
     */
    private int _cdDispzContaCredito = 0;

    /**
     * keeps track of state for field: _cdDispzContaCredito
     */
    private boolean _has_cdDispzContaCredito;

    /**
     * Field _dsDispzContaCredito
     */
    private java.lang.String _dsDispzContaCredito;

    /**
     * Field _cdDispzDiversarCrrtt
     */
    private int _cdDispzDiversarCrrtt = 0;

    /**
     * keeps track of state for field: _cdDispzDiversarCrrtt
     */
    private boolean _has_cdDispzDiversarCrrtt;

    /**
     * Field _dsDispzDiversarCrrtt
     */
    private java.lang.String _dsDispzDiversarCrrtt;

    /**
     * Field _cdDispzDiversasNao
     */
    private int _cdDispzDiversasNao = 0;

    /**
     * keeps track of state for field: _cdDispzDiversasNao
     */
    private boolean _has_cdDispzDiversasNao;

    /**
     * Field _dsDispzDiversasNao
     */
    private java.lang.String _dsDispzDiversasNao;

    /**
     * Field _cdDispzSalarioCrrtt
     */
    private int _cdDispzSalarioCrrtt = 0;

    /**
     * keeps track of state for field: _cdDispzSalarioCrrtt
     */
    private boolean _has_cdDispzSalarioCrrtt;

    /**
     * Field _dsDispzSalarioCrrtt
     */
    private java.lang.String _dsDispzSalarioCrrtt;

    /**
     * Field _cdDispzSalarioNao
     */
    private int _cdDispzSalarioNao = 0;

    /**
     * keeps track of state for field: _cdDispzSalarioNao
     */
    private boolean _has_cdDispzSalarioNao;

    /**
     * Field _dsDispzSalarioNao
     */
    private java.lang.String _dsDispzSalarioNao;

    /**
     * Field _cdDestinoAviso
     */
    private int _cdDestinoAviso = 0;

    /**
     * keeps track of state for field: _cdDestinoAviso
     */
    private boolean _has_cdDestinoAviso;

    /**
     * Field _dsDestinoAviso
     */
    private java.lang.String _dsDestinoAviso;

    /**
     * Field _cdDestinoComprovante
     */
    private int _cdDestinoComprovante = 0;

    /**
     * keeps track of state for field: _cdDestinoComprovante
     */
    private boolean _has_cdDestinoComprovante;

    /**
     * Field _dsDestinoComprovante
     */
    private java.lang.String _dsDestinoComprovante;

    /**
     * Field _cdDestinoFormularioRecadastro
     */
    private int _cdDestinoFormularioRecadastro = 0;

    /**
     * keeps track of state for field: _cdDestinoFormularioRecadastr
     */
    private boolean _has_cdDestinoFormularioRecadastro;

    /**
     * Field _dsDestinoFormularioRecadastro
     */
    private java.lang.String _dsDestinoFormularioRecadastro;

    /**
     * Field _cdEnvelopeAberto
     */
    private int _cdEnvelopeAberto = 0;

    /**
     * keeps track of state for field: _cdEnvelopeAberto
     */
    private boolean _has_cdEnvelopeAberto;

    /**
     * Field _dsEnvelopeAberto
     */
    private java.lang.String _dsEnvelopeAberto;

    /**
     * Field _cdFavorecidoConsPagamento
     */
    private int _cdFavorecidoConsPagamento = 0;

    /**
     * keeps track of state for field: _cdFavorecidoConsPagamento
     */
    private boolean _has_cdFavorecidoConsPagamento;

    /**
     * Field _dsFavorecidoConsPagamento
     */
    private java.lang.String _dsFavorecidoConsPagamento;

    /**
     * Field _cdFormaAutorizacaoPagamento
     */
    private int _cdFormaAutorizacaoPagamento = 0;

    /**
     * keeps track of state for field: _cdFormaAutorizacaoPagamento
     */
    private boolean _has_cdFormaAutorizacaoPagamento;

    /**
     * Field _dsFormaAutorizacaoPagamento
     */
    private java.lang.String _dsFormaAutorizacaoPagamento;

    /**
     * Field _cdFormaEnvioPagamento
     */
    private int _cdFormaEnvioPagamento = 0;

    /**
     * keeps track of state for field: _cdFormaEnvioPagamento
     */
    private boolean _has_cdFormaEnvioPagamento;

    /**
     * Field _dsFormaEnvioPagamento
     */
    private java.lang.String _dsFormaEnvioPagamento;

    /**
     * Field _cdFormaExpiracaoCredito
     */
    private int _cdFormaExpiracaoCredito = 0;

    /**
     * keeps track of state for field: _cdFormaExpiracaoCredito
     */
    private boolean _has_cdFormaExpiracaoCredito;

    /**
     * Field _dsFormaExpiracaoCredito
     */
    private java.lang.String _dsFormaExpiracaoCredito;

    /**
     * Field _cdFormaManutencao
     */
    private int _cdFormaManutencao = 0;

    /**
     * keeps track of state for field: _cdFormaManutencao
     */
    private boolean _has_cdFormaManutencao;

    /**
     * Field _dsFormaManutencao
     */
    private java.lang.String _dsFormaManutencao;

    /**
     * Field _cdFrasePreCadastro
     */
    private int _cdFrasePreCadastro = 0;

    /**
     * keeps track of state for field: _cdFrasePreCadastro
     */
    private boolean _has_cdFrasePreCadastro;

    /**
     * Field _dsFrasePreCadastro
     */
    private java.lang.String _dsFrasePreCadastro;

    /**
     * Field _cdIndicadorAgendaTitulo
     */
    private int _cdIndicadorAgendaTitulo = 0;

    /**
     * keeps track of state for field: _cdIndicadorAgendaTitulo
     */
    private boolean _has_cdIndicadorAgendaTitulo;

    /**
     * Field _dsIndicadorAgendaTitulo
     */
    private java.lang.String _dsIndicadorAgendaTitulo;

    /**
     * Field _cdIndicadorAutorizacaoCliente
     */
    private int _cdIndicadorAutorizacaoCliente = 0;

    /**
     * keeps track of state for field: _cdIndicadorAutorizacaoClient
     */
    private boolean _has_cdIndicadorAutorizacaoCliente;

    /**
     * Field _dsIndicadorAutorizacaoCliente
     */
    private java.lang.String _dsIndicadorAutorizacaoCliente;

    /**
     * Field _cdIndicadorAutorizacaoComplemento
     */
    private int _cdIndicadorAutorizacaoComplemento = 0;

    /**
     * keeps track of state for field:
     * _cdIndicadorAutorizacaoComplemento
     */
    private boolean _has_cdIndicadorAutorizacaoComplemento;

    /**
     * Field _dsIndicadorAutorizacaoComplemento
     */
    private java.lang.String _dsIndicadorAutorizacaoComplemento;

    /**
     * Field _cdIndicadorCadastroOrg
     */
    private int _cdIndicadorCadastroOrg = 0;

    /**
     * keeps track of state for field: _cdIndicadorCadastroOrg
     */
    private boolean _has_cdIndicadorCadastroOrg;

    /**
     * Field _dsIndicadorCadastroOrg
     */
    private java.lang.String _dsIndicadorCadastroOrg;

    /**
     * Field _cdIndicadorCadastroProcd
     */
    private int _cdIndicadorCadastroProcd = 0;

    /**
     * keeps track of state for field: _cdIndicadorCadastroProcd
     */
    private boolean _has_cdIndicadorCadastroProcd;

    /**
     * Field _dsIndicadorCadastroProcd
     */
    private java.lang.String _dsIndicadorCadastroProcd;

    /**
     * Field _cdIndicadorCataoSalario
     */
    private int _cdIndicadorCataoSalario = 0;

    /**
     * keeps track of state for field: _cdIndicadorCataoSalario
     */
    private boolean _has_cdIndicadorCataoSalario;

    /**
     * Field _dsIndicadorCataoSalario
     */
    private java.lang.String _dsIndicadorCataoSalario;

    /**
     * Field _cdIndicadorEconomicoReajuste
     */
    private int _cdIndicadorEconomicoReajuste = 0;

    /**
     * keeps track of state for field: _cdIndicadorEconomicoReajuste
     */
    private boolean _has_cdIndicadorEconomicoReajuste;

    /**
     * Field _dsIndicadorEconomicoReajuste
     */
    private java.lang.String _dsIndicadorEconomicoReajuste;

    /**
     * Field _cdIndicadorExpiracaoCredito
     */
    private int _cdIndicadorExpiracaoCredito = 0;

    /**
     * keeps track of state for field: _cdIndicadorExpiracaoCredito
     */
    private boolean _has_cdIndicadorExpiracaoCredito;

    /**
     * Field _dsIndicadorExpiracaoCredito
     */
    private java.lang.String _dsIndicadorExpiracaoCredito;

    /**
     * Field _cdIndicadorLancamentoPagamento
     */
    private int _cdIndicadorLancamentoPagamento = 0;

    /**
     * keeps track of state for field:
     * _cdIndicadorLancamentoPagamento
     */
    private boolean _has_cdIndicadorLancamentoPagamento;

    /**
     * Field _dsIndicadorLancamentoPagamento
     */
    private java.lang.String _dsIndicadorLancamentoPagamento;

    /**
     * Field _cdIndicadorMensagemPerso
     */
    private int _cdIndicadorMensagemPerso = 0;

    /**
     * keeps track of state for field: _cdIndicadorMensagemPerso
     */
    private boolean _has_cdIndicadorMensagemPerso;

    /**
     * Field _dsIndicadorMensagemPerso
     */
    private java.lang.String _dsIndicadorMensagemPerso;

    /**
     * Field _cdLancamentoFuturoCredito
     */
    private int _cdLancamentoFuturoCredito = 0;

    /**
     * keeps track of state for field: _cdLancamentoFuturoCredito
     */
    private boolean _has_cdLancamentoFuturoCredito;

    /**
     * Field _dsLancamentoFuturoCredito
     */
    private java.lang.String _dsLancamentoFuturoCredito;

    /**
     * Field _cdLancamentoFuturoDebito
     */
    private int _cdLancamentoFuturoDebito = 0;

    /**
     * keeps track of state for field: _cdLancamentoFuturoDebito
     */
    private boolean _has_cdLancamentoFuturoDebito;

    /**
     * Field _dsLancamentoFuturoDebito
     */
    private java.lang.String _dsLancamentoFuturoDebito;

    /**
     * Field _cdLiberacaoLoteProcesso
     */
    private int _cdLiberacaoLoteProcesso = 0;

    /**
     * keeps track of state for field: _cdLiberacaoLoteProcesso
     */
    private boolean _has_cdLiberacaoLoteProcesso;

    /**
     * Field _dsLiberacaoLoteProcesso
     */
    private java.lang.String _dsLiberacaoLoteProcesso;

    /**
     * Field _cdManutencaoBaseRecadastro
     */
    private int _cdManutencaoBaseRecadastro = 0;

    /**
     * keeps track of state for field: _cdManutencaoBaseRecadastro
     */
    private boolean _has_cdManutencaoBaseRecadastro;

    /**
     * Field _dsManutencaoBaseRecadastro
     */
    private java.lang.String _dsManutencaoBaseRecadastro;

    /**
     * Field _cdMidiaDisponivel
     */
    private int _cdMidiaDisponivel = 0;

    /**
     * keeps track of state for field: _cdMidiaDisponivel
     */
    private boolean _has_cdMidiaDisponivel;

    /**
     * Field _dsMidiaDisponivel
     */
    private java.lang.String _dsMidiaDisponivel;

    /**
     * Field _cdMidiaMensagemRecadastro
     */
    private int _cdMidiaMensagemRecadastro = 0;

    /**
     * keeps track of state for field: _cdMidiaMensagemRecadastro
     */
    private boolean _has_cdMidiaMensagemRecadastro;

    /**
     * Field _dsMidiaMensagemRecadastro
     */
    private java.lang.String _dsMidiaMensagemRecadastro;

    /**
     * Field _cdMomentoAvisoRacadastro
     */
    private int _cdMomentoAvisoRacadastro = 0;

    /**
     * keeps track of state for field: _cdMomentoAvisoRacadastro
     */
    private boolean _has_cdMomentoAvisoRacadastro;

    /**
     * Field _dsMomentoAvisoRacadastro
     */
    private java.lang.String _dsMomentoAvisoRacadastro;

    /**
     * Field _cdMomentoCreditoEfetivacao
     */
    private int _cdMomentoCreditoEfetivacao = 0;

    /**
     * keeps track of state for field: _cdMomentoCreditoEfetivacao
     */
    private boolean _has_cdMomentoCreditoEfetivacao;

    /**
     * Field _dsMomentoCreditoEfetivacao
     */
    private java.lang.String _dsMomentoCreditoEfetivacao;

    /**
     * Field _cdMomentoDebitoPagamento
     */
    private int _cdMomentoDebitoPagamento = 0;

    /**
     * keeps track of state for field: _cdMomentoDebitoPagamento
     */
    private boolean _has_cdMomentoDebitoPagamento;

    /**
     * Field _dsMomentoDebitoPagamento
     */
    private java.lang.String _dsMomentoDebitoPagamento;

    /**
     * Field _cdMomentoFormularioRecadastro
     */
    private int _cdMomentoFormularioRecadastro = 0;

    /**
     * keeps track of state for field: _cdMomentoFormularioRecadastr
     */
    private boolean _has_cdMomentoFormularioRecadastro;

    /**
     * Field _dsMomentoFormularioRecadastro
     */
    private java.lang.String _dsMomentoFormularioRecadastro;

    /**
     * Field _cdMomentoProcessamentoPagamento
     */
    private int _cdMomentoProcessamentoPagamento = 0;

    /**
     * keeps track of state for field:
     * _cdMomentoProcessamentoPagamento
     */
    private boolean _has_cdMomentoProcessamentoPagamento;

    /**
     * Field _dsMomentoProcessamentoPagamento
     */
    private java.lang.String _dsMomentoProcessamentoPagamento;

    /**
     * Field _cdMensagemRecadastroMidia
     */
    private int _cdMensagemRecadastroMidia = 0;

    /**
     * keeps track of state for field: _cdMensagemRecadastroMidia
     */
    private boolean _has_cdMensagemRecadastroMidia;

    /**
     * Field _dsMensagemRecadastroMidia
     */
    private java.lang.String _dsMensagemRecadastroMidia;

    /**
     * Field _cdPermissaoDebitoOnline
     */
    private int _cdPermissaoDebitoOnline = 0;

    /**
     * keeps track of state for field: _cdPermissaoDebitoOnline
     */
    private boolean _has_cdPermissaoDebitoOnline;

    /**
     * Field _dsPermissaoDebitoOnline
     */
    private java.lang.String _dsPermissaoDebitoOnline;

    /**
     * Field _cdNaturezaOperacaoPagamento
     */
    private int _cdNaturezaOperacaoPagamento = 0;

    /**
     * keeps track of state for field: _cdNaturezaOperacaoPagamento
     */
    private boolean _has_cdNaturezaOperacaoPagamento;

    /**
     * Field _dsNaturezaOperacaoPagamento
     */
    private java.lang.String _dsNaturezaOperacaoPagamento;

    /**
     * Field _cdPeriodicidadeAviso
     */
    private int _cdPeriodicidadeAviso = 0;

    /**
     * keeps track of state for field: _cdPeriodicidadeAviso
     */
    private boolean _has_cdPeriodicidadeAviso;

    /**
     * Field _dsPeriodicidadeAviso
     */
    private java.lang.String _dsPeriodicidadeAviso;

    /**
     * Field _cdPerdcCobrancaTarifa
     */
    private int _cdPerdcCobrancaTarifa = 0;

    /**
     * keeps track of state for field: _cdPerdcCobrancaTarifa
     */
    private boolean _has_cdPerdcCobrancaTarifa;

    /**
     * Field _dsPerdcCobrancaTarifa
     */
    private java.lang.String _dsPerdcCobrancaTarifa;

    /**
     * Field _cdPerdcComprovante
     */
    private int _cdPerdcComprovante = 0;

    /**
     * keeps track of state for field: _cdPerdcComprovante
     */
    private boolean _has_cdPerdcComprovante;

    /**
     * Field _dsPerdcComprovante
     */
    private java.lang.String _dsPerdcComprovante;

    /**
     * Field _cdPerdcConsultaVeiculo
     */
    private int _cdPerdcConsultaVeiculo = 0;

    /**
     * keeps track of state for field: _cdPerdcConsultaVeiculo
     */
    private boolean _has_cdPerdcConsultaVeiculo;

    /**
     * Field _dsPerdcConsultaVeiculo
     */
    private java.lang.String _dsPerdcConsultaVeiculo;

    /**
     * Field _cdPerdcEnvioRemessa
     */
    private int _cdPerdcEnvioRemessa = 0;

    /**
     * keeps track of state for field: _cdPerdcEnvioRemessa
     */
    private boolean _has_cdPerdcEnvioRemessa;

    /**
     * Field _dsPerdcEnvioRemessa
     */
    private java.lang.String _dsPerdcEnvioRemessa;

    /**
     * Field _cdPerdcManutencaoProcd
     */
    private int _cdPerdcManutencaoProcd = 0;

    /**
     * keeps track of state for field: _cdPerdcManutencaoProcd
     */
    private boolean _has_cdPerdcManutencaoProcd;

    /**
     * Field _dsPerdcManutencaoProcd
     */
    private java.lang.String _dsPerdcManutencaoProcd;

    /**
     * Field _cdPagamentoNaoUtilizado
     */
    private int _cdPagamentoNaoUtilizado = 0;

    /**
     * keeps track of state for field: _cdPagamentoNaoUtilizado
     */
    private boolean _has_cdPagamentoNaoUtilizado;

    /**
     * Field _dsPagamentoNaoUtilizado
     */
    private java.lang.String _dsPagamentoNaoUtilizado;

    /**
     * Field _cdPrincipalEnquaRecadastro
     */
    private int _cdPrincipalEnquaRecadastro = 0;

    /**
     * keeps track of state for field: _cdPrincipalEnquaRecadastro
     */
    private boolean _has_cdPrincipalEnquaRecadastro;

    /**
     * Field _dsPrincipalEnquaRecadastro
     */
    private java.lang.String _dsPrincipalEnquaRecadastro;

    /**
     * Field _cdPrioridadeEfetivacaoPagamento
     */
    private int _cdPrioridadeEfetivacaoPagamento = 0;

    /**
     * keeps track of state for field:
     * _cdPrioridadeEfetivacaoPagamento
     */
    private boolean _has_cdPrioridadeEfetivacaoPagamento;

    /**
     * Field _dsPrioridadeEfetivacaoPagamento
     */
    private java.lang.String _dsPrioridadeEfetivacaoPagamento;

    /**
     * Field _cdRejeicaoAgendaLote
     */
    private int _cdRejeicaoAgendaLote = 0;

    /**
     * keeps track of state for field: _cdRejeicaoAgendaLote
     */
    private boolean _has_cdRejeicaoAgendaLote;

    /**
     * Field _dsRejeicaoAgendaLote
     */
    private java.lang.String _dsRejeicaoAgendaLote;

    /**
     * Field _cdRejeicaoEfetivacaoLote
     */
    private int _cdRejeicaoEfetivacaoLote = 0;

    /**
     * keeps track of state for field: _cdRejeicaoEfetivacaoLote
     */
    private boolean _has_cdRejeicaoEfetivacaoLote;

    /**
     * Field _dsRejeicaoEfetivacaoLote
     */
    private java.lang.String _dsRejeicaoEfetivacaoLote;

    /**
     * Field _cdRejeicaoLote
     */
    private int _cdRejeicaoLote = 0;

    /**
     * keeps track of state for field: _cdRejeicaoLote
     */
    private boolean _has_cdRejeicaoLote;

    /**
     * Field _dsRejeicaoLote
     */
    private java.lang.String _dsRejeicaoLote;

    /**
     * Field _cdRastreabilidadeNotaFiscal
     */
    private int _cdRastreabilidadeNotaFiscal = 0;

    /**
     * keeps track of state for field: _cdRastreabilidadeNotaFiscal
     */
    private boolean _has_cdRastreabilidadeNotaFiscal;

    /**
     * Field _dsRastreabilidadeNotaFiscal
     */
    private java.lang.String _dsRastreabilidadeNotaFiscal;

    /**
     * Field _cdRastreabilidadeTituloTerceiro
     */
    private int _cdRastreabilidadeTituloTerceiro = 0;

    /**
     * keeps track of state for field:
     * _cdRastreabilidadeTituloTerceiro
     */
    private boolean _has_cdRastreabilidadeTituloTerceiro;

    /**
     * Field _dsRastreabilidadeTituloTerceiro
     */
    private java.lang.String _dsRastreabilidadeTituloTerceiro;

    /**
     * Field _cdTipoCargaRecadastro
     */
    private int _cdTipoCargaRecadastro = 0;

    /**
     * keeps track of state for field: _cdTipoCargaRecadastro
     */
    private boolean _has_cdTipoCargaRecadastro;

    /**
     * Field _dsTipoCargaRecadastro
     */
    private java.lang.String _dsTipoCargaRecadastro;

    /**
     * Field _cdTipoCataoSalario
     */
    private int _cdTipoCataoSalario = 0;

    /**
     * keeps track of state for field: _cdTipoCataoSalario
     */
    private boolean _has_cdTipoCataoSalario;

    /**
     * Field _dsTipoCataoSalario
     */
    private java.lang.String _dsTipoCataoSalario;

    /**
     * Field _cdTipoDataFloat
     */
    private int _cdTipoDataFloat = 0;

    /**
     * keeps track of state for field: _cdTipoDataFloat
     */
    private boolean _has_cdTipoDataFloat;

    /**
     * Field _dsTipoDataFloat
     */
    private java.lang.String _dsTipoDataFloat;

    /**
     * Field _cdTipoDivergenciaVeiculo
     */
    private int _cdTipoDivergenciaVeiculo = 0;

    /**
     * keeps track of state for field: _cdTipoDivergenciaVeiculo
     */
    private boolean _has_cdTipoDivergenciaVeiculo;

    /**
     * Field _dsTipoDivergenciaVeiculo
     */
    private java.lang.String _dsTipoDivergenciaVeiculo;

    /**
     * Field _cdTipoIdBeneficio
     */
    private int _cdTipoIdBeneficio = 0;

    /**
     * keeps track of state for field: _cdTipoIdBeneficio
     */
    private boolean _has_cdTipoIdBeneficio;

    /**
     * Field _dsTipoIdBeneficio
     */
    private java.lang.String _dsTipoIdBeneficio;

    /**
     * Field _cdTipoLayoutArquivo
     */
    private int _cdTipoLayoutArquivo = 0;

    /**
     * keeps track of state for field: _cdTipoLayoutArquivo
     */
    private boolean _has_cdTipoLayoutArquivo;

    /**
     * Field _dsTipoLayoutArquivo
     */
    private java.lang.String _dsTipoLayoutArquivo;

    /**
     * Field _cdTipoReajusteTarifa
     */
    private int _cdTipoReajusteTarifa = 0;

    /**
     * keeps track of state for field: _cdTipoReajusteTarifa
     */
    private boolean _has_cdTipoReajusteTarifa;

    /**
     * Field _dsTipoReajusteTarifa
     */
    private java.lang.String _dsTipoReajusteTarifa;

    /**
     * Field _cdContratoContaTransferencia
     */
    private int _cdContratoContaTransferencia = 0;

    /**
     * keeps track of state for field: _cdContratoContaTransferencia
     */
    private boolean _has_cdContratoContaTransferencia;

    /**
     * Field _dsContratoContaTransferencia
     */
    private java.lang.String _dsContratoContaTransferencia;

    /**
     * Field _cdUtilizacaoFavorecidoControle
     */
    private int _cdUtilizacaoFavorecidoControle = 0;

    /**
     * keeps track of state for field:
     * _cdUtilizacaoFavorecidoControle
     */
    private boolean _has_cdUtilizacaoFavorecidoControle;

    /**
     * Field _dsUtilizacaoFavorecidoControle
     */
    private java.lang.String _dsUtilizacaoFavorecidoControle;

    /**
     * Field _dtEnquaContaSalario
     */
    private java.lang.String _dtEnquaContaSalario;

    /**
     * Field _dtInicioBloqueioPplta
     */
    private java.lang.String _dtInicioBloqueioPplta;

    /**
     * Field _dtInicioRastreabilidadeTitulo
     */
    private java.lang.String _dtInicioRastreabilidadeTitulo;

    /**
     * Field _dtFimAcertoRecadastro
     */
    private java.lang.String _dtFimAcertoRecadastro;

    /**
     * Field _dtFimRecadastroBeneficio
     */
    private java.lang.String _dtFimRecadastroBeneficio;

    /**
     * Field _dtinicioAcertoRecadastro
     */
    private java.lang.String _dtinicioAcertoRecadastro;

    /**
     * Field _dtInicioRecadastroBeneficio
     */
    private java.lang.String _dtInicioRecadastroBeneficio;

    /**
     * Field _dtLimiteVinculoCarga
     */
    private java.lang.String _dtLimiteVinculoCarga;

    /**
     * Field _nrFechamentoApuracaoTarifa
     */
    private int _nrFechamentoApuracaoTarifa = 0;

    /**
     * keeps track of state for field: _nrFechamentoApuracaoTarifa
     */
    private boolean _has_nrFechamentoApuracaoTarifa;

    /**
     * Field _cdPercentualIndicadorReajusteTarifa
     */
    private java.math.BigDecimal _cdPercentualIndicadorReajusteTarifa = new java.math.BigDecimal("0");

    /**
     * Field _cdPercentualMaximoInconLote
     */
    private int _cdPercentualMaximoInconLote = 0;

    /**
     * keeps track of state for field: _cdPercentualMaximoInconLote
     */
    private boolean _has_cdPercentualMaximoInconLote;

    /**
     * Field _cdPercentualReducaoTarifaCatalogo
     */
    private java.math.BigDecimal _cdPercentualReducaoTarifaCatalogo = new java.math.BigDecimal("0");

    /**
     * Field _qtAntecedencia
     */
    private int _qtAntecedencia = 0;

    /**
     * keeps track of state for field: _qtAntecedencia
     */
    private boolean _has_qtAntecedencia;

    /**
     * Field _qtAnteriorVencimentoComprovado
     */
    private int _qtAnteriorVencimentoComprovado = 0;

    /**
     * keeps track of state for field:
     * _qtAnteriorVencimentoComprovado
     */
    private boolean _has_qtAnteriorVencimentoComprovado;

    /**
     * Field _qtDiaCobrancaTarifa
     */
    private int _qtDiaCobrancaTarifa = 0;

    /**
     * keeps track of state for field: _qtDiaCobrancaTarifa
     */
    private boolean _has_qtDiaCobrancaTarifa;

    /**
     * Field _qtDiaExpiracao
     */
    private int _qtDiaExpiracao = 0;

    /**
     * keeps track of state for field: _qtDiaExpiracao
     */
    private boolean _has_qtDiaExpiracao;

    /**
     * Field _cdDiaFloatPagamento
     */
    private int _cdDiaFloatPagamento = 0;

    /**
     * keeps track of state for field: _cdDiaFloatPagamento
     */
    private boolean _has_cdDiaFloatPagamento;

    /**
     * Field _qtDiaInatividadeFavorecido
     */
    private int _qtDiaInatividadeFavorecido = 0;

    /**
     * keeps track of state for field: _qtDiaInatividadeFavorecido
     */
    private boolean _has_qtDiaInatividadeFavorecido;

    /**
     * Field _qtDiaRepiqConsulta
     */
    private int _qtDiaRepiqConsulta = 0;

    /**
     * keeps track of state for field: _qtDiaRepiqConsulta
     */
    private boolean _has_qtDiaRepiqConsulta;

    /**
     * Field _qtEtapasRecadastroBeneficio
     */
    private int _qtEtapasRecadastroBeneficio = 0;

    /**
     * keeps track of state for field: _qtEtapasRecadastroBeneficio
     */
    private boolean _has_qtEtapasRecadastroBeneficio;

    /**
     * Field _qtFaseRecadastroBeneficio
     */
    private int _qtFaseRecadastroBeneficio = 0;

    /**
     * keeps track of state for field: _qtFaseRecadastroBeneficio
     */
    private boolean _has_qtFaseRecadastroBeneficio;

    /**
     * Field _qtLimiteLinha
     */
    private int _qtLimiteLinha = 0;

    /**
     * keeps track of state for field: _qtLimiteLinha
     */
    private boolean _has_qtLimiteLinha;

    /**
     * Field _qtLimiteSolicitacaoCatao
     */
    private int _qtLimiteSolicitacaoCatao = 0;

    /**
     * keeps track of state for field: _qtLimiteSolicitacaoCatao
     */
    private boolean _has_qtLimiteSolicitacaoCatao;

    /**
     * Field _qtMaximaInconLote
     */
    private int _qtMaximaInconLote = 0;

    /**
     * keeps track of state for field: _qtMaximaInconLote
     */
    private boolean _has_qtMaximaInconLote;

    /**
     * Field _qtMaximaTituloVencido
     */
    private int _qtMaximaTituloVencido = 0;

    /**
     * keeps track of state for field: _qtMaximaTituloVencido
     */
    private boolean _has_qtMaximaTituloVencido;

    /**
     * Field _qtMesComprovante
     */
    private int _qtMesComprovante = 0;

    /**
     * keeps track of state for field: _qtMesComprovante
     */
    private boolean _has_qtMesComprovante;

    /**
     * Field _qtMesEtapaRecadastro
     */
    private int _qtMesEtapaRecadastro = 0;

    /**
     * keeps track of state for field: _qtMesEtapaRecadastro
     */
    private boolean _has_qtMesEtapaRecadastro;

    /**
     * Field _qtMesFaseRecadastro
     */
    private int _qtMesFaseRecadastro = 0;

    /**
     * keeps track of state for field: _qtMesFaseRecadastro
     */
    private boolean _has_qtMesFaseRecadastro;

    /**
     * Field _qtMesReajusteTarifa
     */
    private int _qtMesReajusteTarifa = 0;

    /**
     * keeps track of state for field: _qtMesReajusteTarifa
     */
    private boolean _has_qtMesReajusteTarifa;

    /**
     * Field _qtViaAviso
     */
    private int _qtViaAviso = 0;

    /**
     * keeps track of state for field: _qtViaAviso
     */
    private boolean _has_qtViaAviso;

    /**
     * Field _qtViaCobranca
     */
    private int _qtViaCobranca = 0;

    /**
     * keeps track of state for field: _qtViaCobranca
     */
    private boolean _has_qtViaCobranca;

    /**
     * Field _qtViaComprovante
     */
    private int _qtViaComprovante = 0;

    /**
     * keeps track of state for field: _qtViaComprovante
     */
    private boolean _has_qtViaComprovante;

    /**
     * Field _vlFavorecidoNaoCadastro
     */
    private java.math.BigDecimal _vlFavorecidoNaoCadastro = new java.math.BigDecimal("0");

    /**
     * Field _vlLimiteDiaPagamento
     */
    private java.math.BigDecimal _vlLimiteDiaPagamento = new java.math.BigDecimal("0");

    /**
     * Field _vlLimiteIndividualPagamento
     */
    private java.math.BigDecimal _vlLimiteIndividualPagamento = new java.math.BigDecimal("0");

    /**
     * Field _cdIndicadorEmissaoAviso
     */
    private int _cdIndicadorEmissaoAviso = 0;

    /**
     * keeps track of state for field: _cdIndicadorEmissaoAviso
     */
    private boolean _has_cdIndicadorEmissaoAviso;

    /**
     * Field _dsIndicadorEmissaoAviso
     */
    private java.lang.String _dsIndicadorEmissaoAviso;

    /**
     * Field _cdIndicadorRetornoInternet
     */
    private int _cdIndicadorRetornoInternet = 0;

    /**
     * keeps track of state for field: _cdIndicadorRetornoInternet
     */
    private boolean _has_cdIndicadorRetornoInternet;

    /**
     * Field _dsIndicadorRetornoInternet
     */
    private java.lang.String _dsIndicadorRetornoInternet;

    /**
     * Field _cdIndicadorRetornoSeparado
     */
    private int _cdIndicadorRetornoSeparado = 0;

    /**
     * keeps track of state for field: _cdIndicadorRetornoSeparado
     */
    private boolean _has_cdIndicadorRetornoSeparado;

    /**
     * Field _dsCodigoIndicadorRetornoSeparado
     */
    private java.lang.String _dsCodigoIndicadorRetornoSeparado;

    /**
     * Field _cdIndicadorListaDebito
     */
    private int _cdIndicadorListaDebito = 0;

    /**
     * keeps track of state for field: _cdIndicadorListaDebito
     */
    private boolean _has_cdIndicadorListaDebito;

    /**
     * Field _dsIndicadorListaDebito
     */
    private java.lang.String _dsIndicadorListaDebito;

    /**
     * Field _cdTipoFormacaoLista
     */
    private int _cdTipoFormacaoLista = 0;

    /**
     * keeps track of state for field: _cdTipoFormacaoLista
     */
    private boolean _has_cdTipoFormacaoLista;

    /**
     * Field _dsTipoFormacaoLista
     */
    private java.lang.String _dsTipoFormacaoLista;

    /**
     * Field _cdTipoConsistenciaLista
     */
    private int _cdTipoConsistenciaLista = 0;

    /**
     * keeps track of state for field: _cdTipoConsistenciaLista
     */
    private boolean _has_cdTipoConsistenciaLista;

    /**
     * Field _dsTipoConsistenciaLista
     */
    private java.lang.String _dsTipoConsistenciaLista;

    /**
     * Field _cdTipoConsultaComprovante
     */
    private int _cdTipoConsultaComprovante = 0;

    /**
     * keeps track of state for field: _cdTipoConsultaComprovante
     */
    private boolean _has_cdTipoConsultaComprovante;

    /**
     * Field _dsTipoConsolidacaoComprovante
     */
    private java.lang.String _dsTipoConsolidacaoComprovante;

    /**
     * Field _cdValidacaoNomeFavorecido
     */
    private int _cdValidacaoNomeFavorecido = 0;

    /**
     * keeps track of state for field: _cdValidacaoNomeFavorecido
     */
    private boolean _has_cdValidacaoNomeFavorecido;

    /**
     * Field _dsValidacaoNomeFavorecido
     */
    private java.lang.String _dsValidacaoNomeFavorecido;

    /**
     * Field _cdTipoContaFavorecido
     */
    private int _cdTipoContaFavorecido = 0;

    /**
     * keeps track of state for field: _cdTipoContaFavorecido
     */
    private boolean _has_cdTipoContaFavorecido;

    /**
     * Field _dsTipoConsistenciaFavorecido
     */
    private java.lang.String _dsTipoConsistenciaFavorecido;

    /**
     * Field _cdIndLancamentoPersonalizado
     */
    private int _cdIndLancamentoPersonalizado = 0;

    /**
     * keeps track of state for field: _cdIndLancamentoPersonalizado
     */
    private boolean _has_cdIndLancamentoPersonalizado;

    /**
     * Field _dsIndLancamentoPersonalizado
     */
    private java.lang.String _dsIndLancamentoPersonalizado;

    /**
     * Field _cdAgendaRastreabilidadeFilial
     */
    private int _cdAgendaRastreabilidadeFilial = 0;

    /**
     * keeps track of state for field: _cdAgendaRastreabilidadeFilia
     */
    private boolean _has_cdAgendaRastreabilidadeFilial;

    /**
     * Field _dsAgendamentoRastFilial
     */
    private java.lang.String _dsAgendamentoRastFilial;

    /**
     * Field _cdIndicadorAdesaoSacador
     */
    private int _cdIndicadorAdesaoSacador = 0;

    /**
     * keeps track of state for field: _cdIndicadorAdesaoSacador
     */
    private boolean _has_cdIndicadorAdesaoSacador;

    /**
     * Field _dsIndicadorAdesaoSacador
     */
    private java.lang.String _dsIndicadorAdesaoSacador;

    /**
     * Field _cdMeioPagamentoCredito
     */
    private int _cdMeioPagamentoCredito = 0;

    /**
     * keeps track of state for field: _cdMeioPagamentoCredito
     */
    private boolean _has_cdMeioPagamentoCredito;

    /**
     * Field _dsMeioPagamentoCredito
     */
    private java.lang.String _dsMeioPagamentoCredito;

    /**
     * Field _cdTipoIsncricaoFavorecido
     */
    private int _cdTipoIsncricaoFavorecido = 0;

    /**
     * keeps track of state for field: _cdTipoIsncricaoFavorecido
     */
    private boolean _has_cdTipoIsncricaoFavorecido;

    /**
     * Field _dsTipoIscricaoFavorecido
     */
    private java.lang.String _dsTipoIscricaoFavorecido;

    /**
     * Field _cdIndicadorBancoPostal
     */
    private int _cdIndicadorBancoPostal = 0;

    /**
     * keeps track of state for field: _cdIndicadorBancoPostal
     */
    private boolean _has_cdIndicadorBancoPostal;

    /**
     * Field _dsIndicadorBancoPostal
     */
    private java.lang.String _dsIndicadorBancoPostal;

    /**
     * Field _cdFormularioContratoCliente
     */
    private int _cdFormularioContratoCliente = 0;

    /**
     * keeps track of state for field: _cdFormularioContratoCliente
     */
    private boolean _has_cdFormularioContratoCliente;

    /**
     * Field _dsCodigoFormularioContratoCliente
     */
    private java.lang.String _dsCodigoFormularioContratoCliente;

    /**
     * Field _dsLocalEmissao
     */
    private java.lang.String _dsLocalEmissao;

    /**
     * Field _cdUsuarioInclusao
     */
    private java.lang.String _cdUsuarioInclusao;

    /**
     * Field _cdUsuarioExternoInclusao
     */
    private java.lang.String _cdUsuarioExternoInclusao;

    /**
     * Field _hrManutencaoRegistroInclusao
     */
    private java.lang.String _hrManutencaoRegistroInclusao;

    /**
     * Field _cdCanalInclusao
     */
    private int _cdCanalInclusao = 0;

    /**
     * keeps track of state for field: _cdCanalInclusao
     */
    private boolean _has_cdCanalInclusao;

    /**
     * Field _dsCanalInclusao
     */
    private java.lang.String _dsCanalInclusao;

    /**
     * Field _nmOperacaoFluxoInclusao
     */
    private java.lang.String _nmOperacaoFluxoInclusao;

    /**
     * Field _cdUsuarioAlteracao
     */
    private java.lang.String _cdUsuarioAlteracao;

    /**
     * Field _cdUsuarioExternoAlteracao
     */
    private java.lang.String _cdUsuarioExternoAlteracao;

    /**
     * Field _hrManutencaoRegistroAlteracao
     */
    private java.lang.String _hrManutencaoRegistroAlteracao;

    /**
     * Field _cdCanalAlteracao
     */
    private int _cdCanalAlteracao = 0;

    /**
     * keeps track of state for field: _cdCanalAlteracao
     */
    private boolean _has_cdCanalAlteracao;

    /**
     * Field _dsCanalAlteracao
     */
    private java.lang.String _dsCanalAlteracao;

    /**
     * Field _nmOperacaoFluxoAlteracao
     */
    private java.lang.String _nmOperacaoFluxoAlteracao;

    /**
     * Field _cdAmbienteServicoContrato
     */
    private java.lang.String _cdAmbienteServicoContrato;

    /**
     * Field _dsAreaResrd
     */
    private java.lang.String _dsAreaResrd;

    /**
     * Field _cdConsultaSaldoValorSuperior
     */
    private int _cdConsultaSaldoValorSuperior = 0;

    /**
     * keeps track of state for field: _cdConsultaSaldoValorSuperior
     */
    private boolean _has_cdConsultaSaldoValorSuperior;

    /**
     * Field _dsConsultaSaldoSuperior
     */
    private java.lang.String _dsConsultaSaldoSuperior;

    /**
     * Field _cdIndicadorFeriadoLocal
     */
    private int _cdIndicadorFeriadoLocal = 0;

    /**
     * keeps track of state for field: _cdIndicadorFeriadoLocal
     */
    private boolean _has_cdIndicadorFeriadoLocal;

    /**
     * Field _dsCodigoIndFeriadoLocal
     */
    private java.lang.String _dsCodigoIndFeriadoLocal;

    /**
     * Field _cdIndicadorSegundaLinha
     */
    private int _cdIndicadorSegundaLinha = 0;

    /**
     * keeps track of state for field: _cdIndicadorSegundaLinha
     */
    private boolean _has_cdIndicadorSegundaLinha;

    /**
     * Field _dsIndicadorSegundaLinha
     */
    private java.lang.String _dsIndicadorSegundaLinha;

    /**
     * Field _cdFloatServicoContrato
     */
    private int _cdFloatServicoContrato = 0;

    /**
     * keeps track of state for field: _cdFloatServicoContrato
     */
    private boolean _has_cdFloatServicoContrato;

    /**
     * Field _dsFloatServicoContrato
     */
    private java.lang.String _dsFloatServicoContrato;

    /**
     * Field _qtDiaUtilPgto
     */
    private int _qtDiaUtilPgto = 0;

    /**
     * keeps track of state for field: _qtDiaUtilPgto
     */
    private boolean _has_qtDiaUtilPgto;

    /**
     * Field _cdExigeAutFilial
     */
    private int _cdExigeAutFilial = 0;

    /**
     * keeps track of state for field: _cdExigeAutFilial
     */
    private boolean _has_cdExigeAutFilial;

    /**
     * Field _dsExigeFilial
     */
    private java.lang.String _dsExigeFilial;

    /**
     * Field _cdPreenchimentoLancamentoPersonalizado
     */
    private int _cdPreenchimentoLancamentoPersonalizado = 0;

    /**
     * keeps track of state for field:
     * _cdPreenchimentoLancamentoPersonalizado
     */
    private boolean _has_cdPreenchimentoLancamentoPersonalizado;

    /**
     * Field _dsPreenchimentoLancamentoPersonalizado
     */
    private java.lang.String _dsPreenchimentoLancamentoPersonalizado;

    /**
     * Field _cdIndicadorAgendaGrade
     */
    private int _cdIndicadorAgendaGrade = 0;

    /**
     * keeps track of state for field: _cdIndicadorAgendaGrade
     */
    private boolean _has_cdIndicadorAgendaGrade;

    /**
     * Field _dsIndicadorAgendaGrade
     */
    private java.lang.String _dsIndicadorAgendaGrade;

    /**
     * Field _cdTituloDdaRetorno
     */
    private int _cdTituloDdaRetorno = 0;

    /**
     * keeps track of state for field: _cdTituloDdaRetorno
     */
    private boolean _has_cdTituloDdaRetorno;

    /**
     * Field _dsTituloDdaRetorno
     */
    private java.lang.String _dsTituloDdaRetorno;

    /**
     * Field _cdIndicadorUtilizaMora
     */
    private int _cdIndicadorUtilizaMora = 0;

    /**
     * keeps track of state for field: _cdIndicadorUtilizaMora
     */
    private boolean _has_cdIndicadorUtilizaMora;

    /**
     * Field _dsIndicadorUtilizaMora
     */
    private java.lang.String _dsIndicadorUtilizaMora;

    /**
     * Field _vlPercentualDiferencaTolerada
     */
    private java.math.BigDecimal _vlPercentualDiferencaTolerada = new java.math.BigDecimal("0");

    /**
     * Field _cdConsistenciaCpfCnpjBenefAvalNpc
     */
    private int _cdConsistenciaCpfCnpjBenefAvalNpc = 0;

    /**
     * keeps track of state for field:
     * _cdConsistenciaCpfCnpjBenefAvalNpc
     */
    private boolean _has_cdConsistenciaCpfCnpjBenefAvalNpc;

    /**
     * Field _dsConsistenciaCpfCnpjBenefAvalNpc
     */
    private java.lang.String _dsConsistenciaCpfCnpjBenefAvalNpc;

    /**
     * Field _dsOrigemIndicador
     */
    private int _dsOrigemIndicador = 0;

    /**
     * keeps track of state for field: _dsOrigemIndicador
     */
    private boolean _has_dsOrigemIndicador;

    /**
     * Field _dsIndicador
     */
    private java.lang.String _dsIndicador;

    /**
     * Field _cdIndicadorTipoRetornoInternet
     */
    private int _cdIndicadorTipoRetornoInternet = 0;

    /**
     * keeps track of state for field:
     * _cdIndicadorTipoRetornoInternet
     */
    private boolean _has_cdIndicadorTipoRetornoInternet;

    /**
     * Field _dsIndicadorTipoRetornoInternet
     */
    private java.lang.String _dsIndicadorTipoRetornoInternet;


      //----------------/
     //- Constructors -/
    //----------------/

    public DetalharTipoServModContratoResponse() 
     {
        super();
        setCdPercentualIndicadorReajusteTarifa(new java.math.BigDecimal("0"));
        setCdPercentualReducaoTarifaCatalogo(new java.math.BigDecimal("0"));
        setVlFavorecidoNaoCadastro(new java.math.BigDecimal("0"));
        setVlLimiteDiaPagamento(new java.math.BigDecimal("0"));
        setVlLimiteIndividualPagamento(new java.math.BigDecimal("0"));
        setVlPercentualDiferencaTolerada(new java.math.BigDecimal("0"));
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalhartiposervmodcontrato.response.DetalharTipoServModContratoResponse()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdAcaoNaoVida
     * 
     */
    public void deleteCdAcaoNaoVida()
    {
        this._has_cdAcaoNaoVida= false;
    } //-- void deleteCdAcaoNaoVida() 

    /**
     * Method deleteCdAcertoDadoRecadastro
     * 
     */
    public void deleteCdAcertoDadoRecadastro()
    {
        this._has_cdAcertoDadoRecadastro= false;
    } //-- void deleteCdAcertoDadoRecadastro() 

    /**
     * Method deleteCdAgendaDebitoVeiculo
     * 
     */
    public void deleteCdAgendaDebitoVeiculo()
    {
        this._has_cdAgendaDebitoVeiculo= false;
    } //-- void deleteCdAgendaDebitoVeiculo() 

    /**
     * Method deleteCdAgendaPagamentoVencido
     * 
     */
    public void deleteCdAgendaPagamentoVencido()
    {
        this._has_cdAgendaPagamentoVencido= false;
    } //-- void deleteCdAgendaPagamentoVencido() 

    /**
     * Method deleteCdAgendaRastreabilidadeFilial
     * 
     */
    public void deleteCdAgendaRastreabilidadeFilial()
    {
        this._has_cdAgendaRastreabilidadeFilial= false;
    } //-- void deleteCdAgendaRastreabilidadeFilial() 

    /**
     * Method deleteCdAgendaValorMenor
     * 
     */
    public void deleteCdAgendaValorMenor()
    {
        this._has_cdAgendaValorMenor= false;
    } //-- void deleteCdAgendaValorMenor() 

    /**
     * Method deleteCdAgrupamentoAviso
     * 
     */
    public void deleteCdAgrupamentoAviso()
    {
        this._has_cdAgrupamentoAviso= false;
    } //-- void deleteCdAgrupamentoAviso() 

    /**
     * Method deleteCdAgrupamentoComprovado
     * 
     */
    public void deleteCdAgrupamentoComprovado()
    {
        this._has_cdAgrupamentoComprovado= false;
    } //-- void deleteCdAgrupamentoComprovado() 

    /**
     * Method deleteCdAgrupamentoFormularioRecadastro
     * 
     */
    public void deleteCdAgrupamentoFormularioRecadastro()
    {
        this._has_cdAgrupamentoFormularioRecadastro= false;
    } //-- void deleteCdAgrupamentoFormularioRecadastro() 

    /**
     * Method deleteCdAntecRecadastroBeneficio
     * 
     */
    public void deleteCdAntecRecadastroBeneficio()
    {
        this._has_cdAntecRecadastroBeneficio= false;
    } //-- void deleteCdAntecRecadastroBeneficio() 

    /**
     * Method deleteCdAreaReservada
     * 
     */
    public void deleteCdAreaReservada()
    {
        this._has_cdAreaReservada= false;
    } //-- void deleteCdAreaReservada() 

    /**
     * Method deleteCdBaseRecadastroBeneficio
     * 
     */
    public void deleteCdBaseRecadastroBeneficio()
    {
        this._has_cdBaseRecadastroBeneficio= false;
    } //-- void deleteCdBaseRecadastroBeneficio() 

    /**
     * Method deleteCdBloqueioEmissaoPplta
     * 
     */
    public void deleteCdBloqueioEmissaoPplta()
    {
        this._has_cdBloqueioEmissaoPplta= false;
    } //-- void deleteCdBloqueioEmissaoPplta() 

    /**
     * Method deleteCdCanalAlteracao
     * 
     */
    public void deleteCdCanalAlteracao()
    {
        this._has_cdCanalAlteracao= false;
    } //-- void deleteCdCanalAlteracao() 

    /**
     * Method deleteCdCanalInclusao
     * 
     */
    public void deleteCdCanalInclusao()
    {
        this._has_cdCanalInclusao= false;
    } //-- void deleteCdCanalInclusao() 

    /**
     * Method deleteCdCapituloTituloRegistro
     * 
     */
    public void deleteCdCapituloTituloRegistro()
    {
        this._has_cdCapituloTituloRegistro= false;
    } //-- void deleteCdCapituloTituloRegistro() 

    /**
     * Method deleteCdCobrancaTarifa
     * 
     */
    public void deleteCdCobrancaTarifa()
    {
        this._has_cdCobrancaTarifa= false;
    } //-- void deleteCdCobrancaTarifa() 

    /**
     * Method deleteCdConsDebitoVeiculo
     * 
     */
    public void deleteCdConsDebitoVeiculo()
    {
        this._has_cdConsDebitoVeiculo= false;
    } //-- void deleteCdConsDebitoVeiculo() 

    /**
     * Method deleteCdConsEndereco
     * 
     */
    public void deleteCdConsEndereco()
    {
        this._has_cdConsEndereco= false;
    } //-- void deleteCdConsEndereco() 

    /**
     * Method deleteCdConsSaldoPagamento
     * 
     */
    public void deleteCdConsSaldoPagamento()
    {
        this._has_cdConsSaldoPagamento= false;
    } //-- void deleteCdConsSaldoPagamento() 

    /**
     * Method deleteCdConsistenciaCpfCnpjBenefAvalNpc
     * 
     */
    public void deleteCdConsistenciaCpfCnpjBenefAvalNpc()
    {
        this._has_cdConsistenciaCpfCnpjBenefAvalNpc= false;
    } //-- void deleteCdConsistenciaCpfCnpjBenefAvalNpc() 

    /**
     * Method deleteCdConsultaSaldoValorSuperior
     * 
     */
    public void deleteCdConsultaSaldoValorSuperior()
    {
        this._has_cdConsultaSaldoValorSuperior= false;
    } //-- void deleteCdConsultaSaldoValorSuperior() 

    /**
     * Method deleteCdContagemConsSaudo
     * 
     */
    public void deleteCdContagemConsSaudo()
    {
        this._has_cdContagemConsSaudo= false;
    } //-- void deleteCdContagemConsSaudo() 

    /**
     * Method deleteCdContratoContaTransferencia
     * 
     */
    public void deleteCdContratoContaTransferencia()
    {
        this._has_cdContratoContaTransferencia= false;
    } //-- void deleteCdContratoContaTransferencia() 

    /**
     * Method deleteCdCreditoNaoUtilizado
     * 
     */
    public void deleteCdCreditoNaoUtilizado()
    {
        this._has_cdCreditoNaoUtilizado= false;
    } //-- void deleteCdCreditoNaoUtilizado() 

    /**
     * Method deleteCdCriterioEnquaBeneficio
     * 
     */
    public void deleteCdCriterioEnquaBeneficio()
    {
        this._has_cdCriterioEnquaBeneficio= false;
    } //-- void deleteCdCriterioEnquaBeneficio() 

    /**
     * Method deleteCdCriterioEnquaRecadastro
     * 
     */
    public void deleteCdCriterioEnquaRecadastro()
    {
        this._has_cdCriterioEnquaRecadastro= false;
    } //-- void deleteCdCriterioEnquaRecadastro() 

    /**
     * Method deleteCdCriterioRastreabilidadeTitulo
     * 
     */
    public void deleteCdCriterioRastreabilidadeTitulo()
    {
        this._has_cdCriterioRastreabilidadeTitulo= false;
    } //-- void deleteCdCriterioRastreabilidadeTitulo() 

    /**
     * Method deleteCdCtciaEspecieBeneficio
     * 
     */
    public void deleteCdCtciaEspecieBeneficio()
    {
        this._has_cdCtciaEspecieBeneficio= false;
    } //-- void deleteCdCtciaEspecieBeneficio() 

    /**
     * Method deleteCdCtciaIdentificacaoBeneficio
     * 
     */
    public void deleteCdCtciaIdentificacaoBeneficio()
    {
        this._has_cdCtciaIdentificacaoBeneficio= false;
    } //-- void deleteCdCtciaIdentificacaoBeneficio() 

    /**
     * Method deleteCdCtciaInscricaoFavorecido
     * 
     */
    public void deleteCdCtciaInscricaoFavorecido()
    {
        this._has_cdCtciaInscricaoFavorecido= false;
    } //-- void deleteCdCtciaInscricaoFavorecido() 

    /**
     * Method deleteCdCtciaProprietarioVeiculo
     * 
     */
    public void deleteCdCtciaProprietarioVeiculo()
    {
        this._has_cdCtciaProprietarioVeiculo= false;
    } //-- void deleteCdCtciaProprietarioVeiculo() 

    /**
     * Method deleteCdDestinoAviso
     * 
     */
    public void deleteCdDestinoAviso()
    {
        this._has_cdDestinoAviso= false;
    } //-- void deleteCdDestinoAviso() 

    /**
     * Method deleteCdDestinoComprovante
     * 
     */
    public void deleteCdDestinoComprovante()
    {
        this._has_cdDestinoComprovante= false;
    } //-- void deleteCdDestinoComprovante() 

    /**
     * Method deleteCdDestinoFormularioRecadastro
     * 
     */
    public void deleteCdDestinoFormularioRecadastro()
    {
        this._has_cdDestinoFormularioRecadastro= false;
    } //-- void deleteCdDestinoFormularioRecadastro() 

    /**
     * Method deleteCdDiaFloatPagamento
     * 
     */
    public void deleteCdDiaFloatPagamento()
    {
        this._has_cdDiaFloatPagamento= false;
    } //-- void deleteCdDiaFloatPagamento() 

    /**
     * Method deleteCdDispzContaCredito
     * 
     */
    public void deleteCdDispzContaCredito()
    {
        this._has_cdDispzContaCredito= false;
    } //-- void deleteCdDispzContaCredito() 

    /**
     * Method deleteCdDispzDiversarCrrtt
     * 
     */
    public void deleteCdDispzDiversarCrrtt()
    {
        this._has_cdDispzDiversarCrrtt= false;
    } //-- void deleteCdDispzDiversarCrrtt() 

    /**
     * Method deleteCdDispzDiversasNao
     * 
     */
    public void deleteCdDispzDiversasNao()
    {
        this._has_cdDispzDiversasNao= false;
    } //-- void deleteCdDispzDiversasNao() 

    /**
     * Method deleteCdDispzSalarioCrrtt
     * 
     */
    public void deleteCdDispzSalarioCrrtt()
    {
        this._has_cdDispzSalarioCrrtt= false;
    } //-- void deleteCdDispzSalarioCrrtt() 

    /**
     * Method deleteCdDispzSalarioNao
     * 
     */
    public void deleteCdDispzSalarioNao()
    {
        this._has_cdDispzSalarioNao= false;
    } //-- void deleteCdDispzSalarioNao() 

    /**
     * Method deleteCdEnvelopeAberto
     * 
     */
    public void deleteCdEnvelopeAberto()
    {
        this._has_cdEnvelopeAberto= false;
    } //-- void deleteCdEnvelopeAberto() 

    /**
     * Method deleteCdExigeAutFilial
     * 
     */
    public void deleteCdExigeAutFilial()
    {
        this._has_cdExigeAutFilial= false;
    } //-- void deleteCdExigeAutFilial() 

    /**
     * Method deleteCdFavorecidoConsPagamento
     * 
     */
    public void deleteCdFavorecidoConsPagamento()
    {
        this._has_cdFavorecidoConsPagamento= false;
    } //-- void deleteCdFavorecidoConsPagamento() 

    /**
     * Method deleteCdFloatServicoContrato
     * 
     */
    public void deleteCdFloatServicoContrato()
    {
        this._has_cdFloatServicoContrato= false;
    } //-- void deleteCdFloatServicoContrato() 

    /**
     * Method deleteCdFormaAutorizacaoPagamento
     * 
     */
    public void deleteCdFormaAutorizacaoPagamento()
    {
        this._has_cdFormaAutorizacaoPagamento= false;
    } //-- void deleteCdFormaAutorizacaoPagamento() 

    /**
     * Method deleteCdFormaEnvioPagamento
     * 
     */
    public void deleteCdFormaEnvioPagamento()
    {
        this._has_cdFormaEnvioPagamento= false;
    } //-- void deleteCdFormaEnvioPagamento() 

    /**
     * Method deleteCdFormaExpiracaoCredito
     * 
     */
    public void deleteCdFormaExpiracaoCredito()
    {
        this._has_cdFormaExpiracaoCredito= false;
    } //-- void deleteCdFormaExpiracaoCredito() 

    /**
     * Method deleteCdFormaManutencao
     * 
     */
    public void deleteCdFormaManutencao()
    {
        this._has_cdFormaManutencao= false;
    } //-- void deleteCdFormaManutencao() 

    /**
     * Method deleteCdFormularioContratoCliente
     * 
     */
    public void deleteCdFormularioContratoCliente()
    {
        this._has_cdFormularioContratoCliente= false;
    } //-- void deleteCdFormularioContratoCliente() 

    /**
     * Method deleteCdFrasePreCadastro
     * 
     */
    public void deleteCdFrasePreCadastro()
    {
        this._has_cdFrasePreCadastro= false;
    } //-- void deleteCdFrasePreCadastro() 

    /**
     * Method deleteCdIndLancamentoPersonalizado
     * 
     */
    public void deleteCdIndLancamentoPersonalizado()
    {
        this._has_cdIndLancamentoPersonalizado= false;
    } //-- void deleteCdIndLancamentoPersonalizado() 

    /**
     * Method deleteCdIndicadorAdesaoSacador
     * 
     */
    public void deleteCdIndicadorAdesaoSacador()
    {
        this._has_cdIndicadorAdesaoSacador= false;
    } //-- void deleteCdIndicadorAdesaoSacador() 

    /**
     * Method deleteCdIndicadorAgendaGrade
     * 
     */
    public void deleteCdIndicadorAgendaGrade()
    {
        this._has_cdIndicadorAgendaGrade= false;
    } //-- void deleteCdIndicadorAgendaGrade() 

    /**
     * Method deleteCdIndicadorAgendaTitulo
     * 
     */
    public void deleteCdIndicadorAgendaTitulo()
    {
        this._has_cdIndicadorAgendaTitulo= false;
    } //-- void deleteCdIndicadorAgendaTitulo() 

    /**
     * Method deleteCdIndicadorAutorizacaoCliente
     * 
     */
    public void deleteCdIndicadorAutorizacaoCliente()
    {
        this._has_cdIndicadorAutorizacaoCliente= false;
    } //-- void deleteCdIndicadorAutorizacaoCliente() 

    /**
     * Method deleteCdIndicadorAutorizacaoComplemento
     * 
     */
    public void deleteCdIndicadorAutorizacaoComplemento()
    {
        this._has_cdIndicadorAutorizacaoComplemento= false;
    } //-- void deleteCdIndicadorAutorizacaoComplemento() 

    /**
     * Method deleteCdIndicadorBancoPostal
     * 
     */
    public void deleteCdIndicadorBancoPostal()
    {
        this._has_cdIndicadorBancoPostal= false;
    } //-- void deleteCdIndicadorBancoPostal() 

    /**
     * Method deleteCdIndicadorCadastroOrg
     * 
     */
    public void deleteCdIndicadorCadastroOrg()
    {
        this._has_cdIndicadorCadastroOrg= false;
    } //-- void deleteCdIndicadorCadastroOrg() 

    /**
     * Method deleteCdIndicadorCadastroProcd
     * 
     */
    public void deleteCdIndicadorCadastroProcd()
    {
        this._has_cdIndicadorCadastroProcd= false;
    } //-- void deleteCdIndicadorCadastroProcd() 

    /**
     * Method deleteCdIndicadorCataoSalario
     * 
     */
    public void deleteCdIndicadorCataoSalario()
    {
        this._has_cdIndicadorCataoSalario= false;
    } //-- void deleteCdIndicadorCataoSalario() 

    /**
     * Method deleteCdIndicadorEconomicoReajuste
     * 
     */
    public void deleteCdIndicadorEconomicoReajuste()
    {
        this._has_cdIndicadorEconomicoReajuste= false;
    } //-- void deleteCdIndicadorEconomicoReajuste() 

    /**
     * Method deleteCdIndicadorEmissaoAviso
     * 
     */
    public void deleteCdIndicadorEmissaoAviso()
    {
        this._has_cdIndicadorEmissaoAviso= false;
    } //-- void deleteCdIndicadorEmissaoAviso() 

    /**
     * Method deleteCdIndicadorExpiracaoCredito
     * 
     */
    public void deleteCdIndicadorExpiracaoCredito()
    {
        this._has_cdIndicadorExpiracaoCredito= false;
    } //-- void deleteCdIndicadorExpiracaoCredito() 

    /**
     * Method deleteCdIndicadorFeriadoLocal
     * 
     */
    public void deleteCdIndicadorFeriadoLocal()
    {
        this._has_cdIndicadorFeriadoLocal= false;
    } //-- void deleteCdIndicadorFeriadoLocal() 

    /**
     * Method deleteCdIndicadorLancamentoPagamento
     * 
     */
    public void deleteCdIndicadorLancamentoPagamento()
    {
        this._has_cdIndicadorLancamentoPagamento= false;
    } //-- void deleteCdIndicadorLancamentoPagamento() 

    /**
     * Method deleteCdIndicadorListaDebito
     * 
     */
    public void deleteCdIndicadorListaDebito()
    {
        this._has_cdIndicadorListaDebito= false;
    } //-- void deleteCdIndicadorListaDebito() 

    /**
     * Method deleteCdIndicadorMensagemPerso
     * 
     */
    public void deleteCdIndicadorMensagemPerso()
    {
        this._has_cdIndicadorMensagemPerso= false;
    } //-- void deleteCdIndicadorMensagemPerso() 

    /**
     * Method deleteCdIndicadorRetornoInternet
     * 
     */
    public void deleteCdIndicadorRetornoInternet()
    {
        this._has_cdIndicadorRetornoInternet= false;
    } //-- void deleteCdIndicadorRetornoInternet() 

    /**
     * Method deleteCdIndicadorRetornoSeparado
     * 
     */
    public void deleteCdIndicadorRetornoSeparado()
    {
        this._has_cdIndicadorRetornoSeparado= false;
    } //-- void deleteCdIndicadorRetornoSeparado() 

    /**
     * Method deleteCdIndicadorSegundaLinha
     * 
     */
    public void deleteCdIndicadorSegundaLinha()
    {
        this._has_cdIndicadorSegundaLinha= false;
    } //-- void deleteCdIndicadorSegundaLinha() 

    /**
     * Method deleteCdIndicadorTipoRetornoInternet
     * 
     */
    public void deleteCdIndicadorTipoRetornoInternet()
    {
        this._has_cdIndicadorTipoRetornoInternet= false;
    } //-- void deleteCdIndicadorTipoRetornoInternet() 

    /**
     * Method deleteCdIndicadorUtilizaMora
     * 
     */
    public void deleteCdIndicadorUtilizaMora()
    {
        this._has_cdIndicadorUtilizaMora= false;
    } //-- void deleteCdIndicadorUtilizaMora() 

    /**
     * Method deleteCdLancamentoFuturoCredito
     * 
     */
    public void deleteCdLancamentoFuturoCredito()
    {
        this._has_cdLancamentoFuturoCredito= false;
    } //-- void deleteCdLancamentoFuturoCredito() 

    /**
     * Method deleteCdLancamentoFuturoDebito
     * 
     */
    public void deleteCdLancamentoFuturoDebito()
    {
        this._has_cdLancamentoFuturoDebito= false;
    } //-- void deleteCdLancamentoFuturoDebito() 

    /**
     * Method deleteCdLiberacaoLoteProcesso
     * 
     */
    public void deleteCdLiberacaoLoteProcesso()
    {
        this._has_cdLiberacaoLoteProcesso= false;
    } //-- void deleteCdLiberacaoLoteProcesso() 

    /**
     * Method deleteCdManutencaoBaseRecadastro
     * 
     */
    public void deleteCdManutencaoBaseRecadastro()
    {
        this._has_cdManutencaoBaseRecadastro= false;
    } //-- void deleteCdManutencaoBaseRecadastro() 

    /**
     * Method deleteCdMeioPagamentoCredito
     * 
     */
    public void deleteCdMeioPagamentoCredito()
    {
        this._has_cdMeioPagamentoCredito= false;
    } //-- void deleteCdMeioPagamentoCredito() 

    /**
     * Method deleteCdMensagemRecadastroMidia
     * 
     */
    public void deleteCdMensagemRecadastroMidia()
    {
        this._has_cdMensagemRecadastroMidia= false;
    } //-- void deleteCdMensagemRecadastroMidia() 

    /**
     * Method deleteCdMidiaDisponivel
     * 
     */
    public void deleteCdMidiaDisponivel()
    {
        this._has_cdMidiaDisponivel= false;
    } //-- void deleteCdMidiaDisponivel() 

    /**
     * Method deleteCdMidiaMensagemRecadastro
     * 
     */
    public void deleteCdMidiaMensagemRecadastro()
    {
        this._has_cdMidiaMensagemRecadastro= false;
    } //-- void deleteCdMidiaMensagemRecadastro() 

    /**
     * Method deleteCdMomentoAvisoRacadastro
     * 
     */
    public void deleteCdMomentoAvisoRacadastro()
    {
        this._has_cdMomentoAvisoRacadastro= false;
    } //-- void deleteCdMomentoAvisoRacadastro() 

    /**
     * Method deleteCdMomentoCreditoEfetivacao
     * 
     */
    public void deleteCdMomentoCreditoEfetivacao()
    {
        this._has_cdMomentoCreditoEfetivacao= false;
    } //-- void deleteCdMomentoCreditoEfetivacao() 

    /**
     * Method deleteCdMomentoDebitoPagamento
     * 
     */
    public void deleteCdMomentoDebitoPagamento()
    {
        this._has_cdMomentoDebitoPagamento= false;
    } //-- void deleteCdMomentoDebitoPagamento() 

    /**
     * Method deleteCdMomentoFormularioRecadastro
     * 
     */
    public void deleteCdMomentoFormularioRecadastro()
    {
        this._has_cdMomentoFormularioRecadastro= false;
    } //-- void deleteCdMomentoFormularioRecadastro() 

    /**
     * Method deleteCdMomentoProcessamentoPagamento
     * 
     */
    public void deleteCdMomentoProcessamentoPagamento()
    {
        this._has_cdMomentoProcessamentoPagamento= false;
    } //-- void deleteCdMomentoProcessamentoPagamento() 

    /**
     * Method deleteCdNaturezaOperacaoPagamento
     * 
     */
    public void deleteCdNaturezaOperacaoPagamento()
    {
        this._has_cdNaturezaOperacaoPagamento= false;
    } //-- void deleteCdNaturezaOperacaoPagamento() 

    /**
     * Method deleteCdPagamentoNaoUtilizado
     * 
     */
    public void deleteCdPagamentoNaoUtilizado()
    {
        this._has_cdPagamentoNaoUtilizado= false;
    } //-- void deleteCdPagamentoNaoUtilizado() 

    /**
     * Method deleteCdPercentualMaximoInconLote
     * 
     */
    public void deleteCdPercentualMaximoInconLote()
    {
        this._has_cdPercentualMaximoInconLote= false;
    } //-- void deleteCdPercentualMaximoInconLote() 

    /**
     * Method deleteCdPerdcCobrancaTarifa
     * 
     */
    public void deleteCdPerdcCobrancaTarifa()
    {
        this._has_cdPerdcCobrancaTarifa= false;
    } //-- void deleteCdPerdcCobrancaTarifa() 

    /**
     * Method deleteCdPerdcComprovante
     * 
     */
    public void deleteCdPerdcComprovante()
    {
        this._has_cdPerdcComprovante= false;
    } //-- void deleteCdPerdcComprovante() 

    /**
     * Method deleteCdPerdcConsultaVeiculo
     * 
     */
    public void deleteCdPerdcConsultaVeiculo()
    {
        this._has_cdPerdcConsultaVeiculo= false;
    } //-- void deleteCdPerdcConsultaVeiculo() 

    /**
     * Method deleteCdPerdcEnvioRemessa
     * 
     */
    public void deleteCdPerdcEnvioRemessa()
    {
        this._has_cdPerdcEnvioRemessa= false;
    } //-- void deleteCdPerdcEnvioRemessa() 

    /**
     * Method deleteCdPerdcManutencaoProcd
     * 
     */
    public void deleteCdPerdcManutencaoProcd()
    {
        this._has_cdPerdcManutencaoProcd= false;
    } //-- void deleteCdPerdcManutencaoProcd() 

    /**
     * Method deleteCdPeriodicidadeAviso
     * 
     */
    public void deleteCdPeriodicidadeAviso()
    {
        this._has_cdPeriodicidadeAviso= false;
    } //-- void deleteCdPeriodicidadeAviso() 

    /**
     * Method deleteCdPermissaoDebitoOnline
     * 
     */
    public void deleteCdPermissaoDebitoOnline()
    {
        this._has_cdPermissaoDebitoOnline= false;
    } //-- void deleteCdPermissaoDebitoOnline() 

    /**
     * Method deleteCdPreenchimentoLancamentoPersonalizado
     * 
     */
    public void deleteCdPreenchimentoLancamentoPersonalizado()
    {
        this._has_cdPreenchimentoLancamentoPersonalizado= false;
    } //-- void deleteCdPreenchimentoLancamentoPersonalizado() 

    /**
     * Method deleteCdPrincipalEnquaRecadastro
     * 
     */
    public void deleteCdPrincipalEnquaRecadastro()
    {
        this._has_cdPrincipalEnquaRecadastro= false;
    } //-- void deleteCdPrincipalEnquaRecadastro() 

    /**
     * Method deleteCdPrioridadeEfetivacaoPagamento
     * 
     */
    public void deleteCdPrioridadeEfetivacaoPagamento()
    {
        this._has_cdPrioridadeEfetivacaoPagamento= false;
    } //-- void deleteCdPrioridadeEfetivacaoPagamento() 

    /**
     * Method deleteCdRastreabilidadeNotaFiscal
     * 
     */
    public void deleteCdRastreabilidadeNotaFiscal()
    {
        this._has_cdRastreabilidadeNotaFiscal= false;
    } //-- void deleteCdRastreabilidadeNotaFiscal() 

    /**
     * Method deleteCdRastreabilidadeTituloTerceiro
     * 
     */
    public void deleteCdRastreabilidadeTituloTerceiro()
    {
        this._has_cdRastreabilidadeTituloTerceiro= false;
    } //-- void deleteCdRastreabilidadeTituloTerceiro() 

    /**
     * Method deleteCdRejeicaoAgendaLote
     * 
     */
    public void deleteCdRejeicaoAgendaLote()
    {
        this._has_cdRejeicaoAgendaLote= false;
    } //-- void deleteCdRejeicaoAgendaLote() 

    /**
     * Method deleteCdRejeicaoEfetivacaoLote
     * 
     */
    public void deleteCdRejeicaoEfetivacaoLote()
    {
        this._has_cdRejeicaoEfetivacaoLote= false;
    } //-- void deleteCdRejeicaoEfetivacaoLote() 

    /**
     * Method deleteCdRejeicaoLote
     * 
     */
    public void deleteCdRejeicaoLote()
    {
        this._has_cdRejeicaoLote= false;
    } //-- void deleteCdRejeicaoLote() 

    /**
     * Method deleteCdTipoCargaRecadastro
     * 
     */
    public void deleteCdTipoCargaRecadastro()
    {
        this._has_cdTipoCargaRecadastro= false;
    } //-- void deleteCdTipoCargaRecadastro() 

    /**
     * Method deleteCdTipoCataoSalario
     * 
     */
    public void deleteCdTipoCataoSalario()
    {
        this._has_cdTipoCataoSalario= false;
    } //-- void deleteCdTipoCataoSalario() 

    /**
     * Method deleteCdTipoConsistenciaLista
     * 
     */
    public void deleteCdTipoConsistenciaLista()
    {
        this._has_cdTipoConsistenciaLista= false;
    } //-- void deleteCdTipoConsistenciaLista() 

    /**
     * Method deleteCdTipoConsultaComprovante
     * 
     */
    public void deleteCdTipoConsultaComprovante()
    {
        this._has_cdTipoConsultaComprovante= false;
    } //-- void deleteCdTipoConsultaComprovante() 

    /**
     * Method deleteCdTipoContaFavorecido
     * 
     */
    public void deleteCdTipoContaFavorecido()
    {
        this._has_cdTipoContaFavorecido= false;
    } //-- void deleteCdTipoContaFavorecido() 

    /**
     * Method deleteCdTipoDataFloat
     * 
     */
    public void deleteCdTipoDataFloat()
    {
        this._has_cdTipoDataFloat= false;
    } //-- void deleteCdTipoDataFloat() 

    /**
     * Method deleteCdTipoDivergenciaVeiculo
     * 
     */
    public void deleteCdTipoDivergenciaVeiculo()
    {
        this._has_cdTipoDivergenciaVeiculo= false;
    } //-- void deleteCdTipoDivergenciaVeiculo() 

    /**
     * Method deleteCdTipoFormacaoLista
     * 
     */
    public void deleteCdTipoFormacaoLista()
    {
        this._has_cdTipoFormacaoLista= false;
    } //-- void deleteCdTipoFormacaoLista() 

    /**
     * Method deleteCdTipoIdBeneficio
     * 
     */
    public void deleteCdTipoIdBeneficio()
    {
        this._has_cdTipoIdBeneficio= false;
    } //-- void deleteCdTipoIdBeneficio() 

    /**
     * Method deleteCdTipoIsncricaoFavorecido
     * 
     */
    public void deleteCdTipoIsncricaoFavorecido()
    {
        this._has_cdTipoIsncricaoFavorecido= false;
    } //-- void deleteCdTipoIsncricaoFavorecido() 

    /**
     * Method deleteCdTipoLayoutArquivo
     * 
     */
    public void deleteCdTipoLayoutArquivo()
    {
        this._has_cdTipoLayoutArquivo= false;
    } //-- void deleteCdTipoLayoutArquivo() 

    /**
     * Method deleteCdTipoReajusteTarifa
     * 
     */
    public void deleteCdTipoReajusteTarifa()
    {
        this._has_cdTipoReajusteTarifa= false;
    } //-- void deleteCdTipoReajusteTarifa() 

    /**
     * Method deleteCdTituloDdaRetorno
     * 
     */
    public void deleteCdTituloDdaRetorno()
    {
        this._has_cdTituloDdaRetorno= false;
    } //-- void deleteCdTituloDdaRetorno() 

    /**
     * Method deleteCdUtilizacaoFavorecidoControle
     * 
     */
    public void deleteCdUtilizacaoFavorecidoControle()
    {
        this._has_cdUtilizacaoFavorecidoControle= false;
    } //-- void deleteCdUtilizacaoFavorecidoControle() 

    /**
     * Method deleteCdValidacaoNomeFavorecido
     * 
     */
    public void deleteCdValidacaoNomeFavorecido()
    {
        this._has_cdValidacaoNomeFavorecido= false;
    } //-- void deleteCdValidacaoNomeFavorecido() 

    /**
     * Method deleteDsOrigemIndicador
     * 
     */
    public void deleteDsOrigemIndicador()
    {
        this._has_dsOrigemIndicador= false;
    } //-- void deleteDsOrigemIndicador() 

    /**
     * Method deleteNrFechamentoApuracaoTarifa
     * 
     */
    public void deleteNrFechamentoApuracaoTarifa()
    {
        this._has_nrFechamentoApuracaoTarifa= false;
    } //-- void deleteNrFechamentoApuracaoTarifa() 

    /**
     * Method deleteQtAntecedencia
     * 
     */
    public void deleteQtAntecedencia()
    {
        this._has_qtAntecedencia= false;
    } //-- void deleteQtAntecedencia() 

    /**
     * Method deleteQtAnteriorVencimentoComprovado
     * 
     */
    public void deleteQtAnteriorVencimentoComprovado()
    {
        this._has_qtAnteriorVencimentoComprovado= false;
    } //-- void deleteQtAnteriorVencimentoComprovado() 

    /**
     * Method deleteQtDiaCobrancaTarifa
     * 
     */
    public void deleteQtDiaCobrancaTarifa()
    {
        this._has_qtDiaCobrancaTarifa= false;
    } //-- void deleteQtDiaCobrancaTarifa() 

    /**
     * Method deleteQtDiaExpiracao
     * 
     */
    public void deleteQtDiaExpiracao()
    {
        this._has_qtDiaExpiracao= false;
    } //-- void deleteQtDiaExpiracao() 

    /**
     * Method deleteQtDiaInatividadeFavorecido
     * 
     */
    public void deleteQtDiaInatividadeFavorecido()
    {
        this._has_qtDiaInatividadeFavorecido= false;
    } //-- void deleteQtDiaInatividadeFavorecido() 

    /**
     * Method deleteQtDiaRepiqConsulta
     * 
     */
    public void deleteQtDiaRepiqConsulta()
    {
        this._has_qtDiaRepiqConsulta= false;
    } //-- void deleteQtDiaRepiqConsulta() 

    /**
     * Method deleteQtDiaUtilPgto
     * 
     */
    public void deleteQtDiaUtilPgto()
    {
        this._has_qtDiaUtilPgto= false;
    } //-- void deleteQtDiaUtilPgto() 

    /**
     * Method deleteQtEtapasRecadastroBeneficio
     * 
     */
    public void deleteQtEtapasRecadastroBeneficio()
    {
        this._has_qtEtapasRecadastroBeneficio= false;
    } //-- void deleteQtEtapasRecadastroBeneficio() 

    /**
     * Method deleteQtFaseRecadastroBeneficio
     * 
     */
    public void deleteQtFaseRecadastroBeneficio()
    {
        this._has_qtFaseRecadastroBeneficio= false;
    } //-- void deleteQtFaseRecadastroBeneficio() 

    /**
     * Method deleteQtLimiteLinha
     * 
     */
    public void deleteQtLimiteLinha()
    {
        this._has_qtLimiteLinha= false;
    } //-- void deleteQtLimiteLinha() 

    /**
     * Method deleteQtLimiteSolicitacaoCatao
     * 
     */
    public void deleteQtLimiteSolicitacaoCatao()
    {
        this._has_qtLimiteSolicitacaoCatao= false;
    } //-- void deleteQtLimiteSolicitacaoCatao() 

    /**
     * Method deleteQtMaximaInconLote
     * 
     */
    public void deleteQtMaximaInconLote()
    {
        this._has_qtMaximaInconLote= false;
    } //-- void deleteQtMaximaInconLote() 

    /**
     * Method deleteQtMaximaTituloVencido
     * 
     */
    public void deleteQtMaximaTituloVencido()
    {
        this._has_qtMaximaTituloVencido= false;
    } //-- void deleteQtMaximaTituloVencido() 

    /**
     * Method deleteQtMesComprovante
     * 
     */
    public void deleteQtMesComprovante()
    {
        this._has_qtMesComprovante= false;
    } //-- void deleteQtMesComprovante() 

    /**
     * Method deleteQtMesEtapaRecadastro
     * 
     */
    public void deleteQtMesEtapaRecadastro()
    {
        this._has_qtMesEtapaRecadastro= false;
    } //-- void deleteQtMesEtapaRecadastro() 

    /**
     * Method deleteQtMesFaseRecadastro
     * 
     */
    public void deleteQtMesFaseRecadastro()
    {
        this._has_qtMesFaseRecadastro= false;
    } //-- void deleteQtMesFaseRecadastro() 

    /**
     * Method deleteQtMesReajusteTarifa
     * 
     */
    public void deleteQtMesReajusteTarifa()
    {
        this._has_qtMesReajusteTarifa= false;
    } //-- void deleteQtMesReajusteTarifa() 

    /**
     * Method deleteQtViaAviso
     * 
     */
    public void deleteQtViaAviso()
    {
        this._has_qtViaAviso= false;
    } //-- void deleteQtViaAviso() 

    /**
     * Method deleteQtViaCobranca
     * 
     */
    public void deleteQtViaCobranca()
    {
        this._has_qtViaCobranca= false;
    } //-- void deleteQtViaCobranca() 

    /**
     * Method deleteQtViaComprovante
     * 
     */
    public void deleteQtViaComprovante()
    {
        this._has_qtViaComprovante= false;
    } //-- void deleteQtViaComprovante() 

    /**
     * Returns the value of field 'cdAcaoNaoVida'.
     * 
     * @return int
     * @return the value of field 'cdAcaoNaoVida'.
     */
    public int getCdAcaoNaoVida()
    {
        return this._cdAcaoNaoVida;
    } //-- int getCdAcaoNaoVida() 

    /**
     * Returns the value of field 'cdAcertoDadoRecadastro'.
     * 
     * @return int
     * @return the value of field 'cdAcertoDadoRecadastro'.
     */
    public int getCdAcertoDadoRecadastro()
    {
        return this._cdAcertoDadoRecadastro;
    } //-- int getCdAcertoDadoRecadastro() 

    /**
     * Returns the value of field 'cdAgendaDebitoVeiculo'.
     * 
     * @return int
     * @return the value of field 'cdAgendaDebitoVeiculo'.
     */
    public int getCdAgendaDebitoVeiculo()
    {
        return this._cdAgendaDebitoVeiculo;
    } //-- int getCdAgendaDebitoVeiculo() 

    /**
     * Returns the value of field 'cdAgendaPagamentoVencido'.
     * 
     * @return int
     * @return the value of field 'cdAgendaPagamentoVencido'.
     */
    public int getCdAgendaPagamentoVencido()
    {
        return this._cdAgendaPagamentoVencido;
    } //-- int getCdAgendaPagamentoVencido() 

    /**
     * Returns the value of field 'cdAgendaRastreabilidadeFilial'.
     * 
     * @return int
     * @return the value of field 'cdAgendaRastreabilidadeFilial'.
     */
    public int getCdAgendaRastreabilidadeFilial()
    {
        return this._cdAgendaRastreabilidadeFilial;
    } //-- int getCdAgendaRastreabilidadeFilial() 

    /**
     * Returns the value of field 'cdAgendaValorMenor'.
     * 
     * @return int
     * @return the value of field 'cdAgendaValorMenor'.
     */
    public int getCdAgendaValorMenor()
    {
        return this._cdAgendaValorMenor;
    } //-- int getCdAgendaValorMenor() 

    /**
     * Returns the value of field 'cdAgrupamentoAviso'.
     * 
     * @return int
     * @return the value of field 'cdAgrupamentoAviso'.
     */
    public int getCdAgrupamentoAviso()
    {
        return this._cdAgrupamentoAviso;
    } //-- int getCdAgrupamentoAviso() 

    /**
     * Returns the value of field 'cdAgrupamentoComprovado'.
     * 
     * @return int
     * @return the value of field 'cdAgrupamentoComprovado'.
     */
    public int getCdAgrupamentoComprovado()
    {
        return this._cdAgrupamentoComprovado;
    } //-- int getCdAgrupamentoComprovado() 

    /**
     * Returns the value of field
     * 'cdAgrupamentoFormularioRecadastro'.
     * 
     * @return int
     * @return the value of field
     * 'cdAgrupamentoFormularioRecadastro'.
     */
    public int getCdAgrupamentoFormularioRecadastro()
    {
        return this._cdAgrupamentoFormularioRecadastro;
    } //-- int getCdAgrupamentoFormularioRecadastro() 

    /**
     * Returns the value of field 'cdAmbienteServicoContrato'.
     * 
     * @return String
     * @return the value of field 'cdAmbienteServicoContrato'.
     */
    public java.lang.String getCdAmbienteServicoContrato()
    {
        return this._cdAmbienteServicoContrato;
    } //-- java.lang.String getCdAmbienteServicoContrato() 

    /**
     * Returns the value of field 'cdAntecRecadastroBeneficio'.
     * 
     * @return int
     * @return the value of field 'cdAntecRecadastroBeneficio'.
     */
    public int getCdAntecRecadastroBeneficio()
    {
        return this._cdAntecRecadastroBeneficio;
    } //-- int getCdAntecRecadastroBeneficio() 

    /**
     * Returns the value of field 'cdAreaReservada'.
     * 
     * @return int
     * @return the value of field 'cdAreaReservada'.
     */
    public int getCdAreaReservada()
    {
        return this._cdAreaReservada;
    } //-- int getCdAreaReservada() 

    /**
     * Returns the value of field 'cdBaseRecadastroBeneficio'.
     * 
     * @return int
     * @return the value of field 'cdBaseRecadastroBeneficio'.
     */
    public int getCdBaseRecadastroBeneficio()
    {
        return this._cdBaseRecadastroBeneficio;
    } //-- int getCdBaseRecadastroBeneficio() 

    /**
     * Returns the value of field 'cdBloqueioEmissaoPplta'.
     * 
     * @return int
     * @return the value of field 'cdBloqueioEmissaoPplta'.
     */
    public int getCdBloqueioEmissaoPplta()
    {
        return this._cdBloqueioEmissaoPplta;
    } //-- int getCdBloqueioEmissaoPplta() 

    /**
     * Returns the value of field 'cdCanalAlteracao'.
     * 
     * @return int
     * @return the value of field 'cdCanalAlteracao'.
     */
    public int getCdCanalAlteracao()
    {
        return this._cdCanalAlteracao;
    } //-- int getCdCanalAlteracao() 

    /**
     * Returns the value of field 'cdCanalInclusao'.
     * 
     * @return int
     * @return the value of field 'cdCanalInclusao'.
     */
    public int getCdCanalInclusao()
    {
        return this._cdCanalInclusao;
    } //-- int getCdCanalInclusao() 

    /**
     * Returns the value of field 'cdCapituloTituloRegistro'.
     * 
     * @return int
     * @return the value of field 'cdCapituloTituloRegistro'.
     */
    public int getCdCapituloTituloRegistro()
    {
        return this._cdCapituloTituloRegistro;
    } //-- int getCdCapituloTituloRegistro() 

    /**
     * Returns the value of field 'cdCobrancaTarifa'.
     * 
     * @return int
     * @return the value of field 'cdCobrancaTarifa'.
     */
    public int getCdCobrancaTarifa()
    {
        return this._cdCobrancaTarifa;
    } //-- int getCdCobrancaTarifa() 

    /**
     * Returns the value of field 'cdConsDebitoVeiculo'.
     * 
     * @return int
     * @return the value of field 'cdConsDebitoVeiculo'.
     */
    public int getCdConsDebitoVeiculo()
    {
        return this._cdConsDebitoVeiculo;
    } //-- int getCdConsDebitoVeiculo() 

    /**
     * Returns the value of field 'cdConsEndereco'.
     * 
     * @return int
     * @return the value of field 'cdConsEndereco'.
     */
    public int getCdConsEndereco()
    {
        return this._cdConsEndereco;
    } //-- int getCdConsEndereco() 

    /**
     * Returns the value of field 'cdConsSaldoPagamento'.
     * 
     * @return int
     * @return the value of field 'cdConsSaldoPagamento'.
     */
    public int getCdConsSaldoPagamento()
    {
        return this._cdConsSaldoPagamento;
    } //-- int getCdConsSaldoPagamento() 

    /**
     * Returns the value of field
     * 'cdConsistenciaCpfCnpjBenefAvalNpc'.
     * 
     * @return int
     * @return the value of field
     * 'cdConsistenciaCpfCnpjBenefAvalNpc'.
     */
    public int getCdConsistenciaCpfCnpjBenefAvalNpc()
    {
        return this._cdConsistenciaCpfCnpjBenefAvalNpc;
    } //-- int getCdConsistenciaCpfCnpjBenefAvalNpc() 

    /**
     * Returns the value of field 'cdConsultaSaldoValorSuperior'.
     * 
     * @return int
     * @return the value of field 'cdConsultaSaldoValorSuperior'.
     */
    public int getCdConsultaSaldoValorSuperior()
    {
        return this._cdConsultaSaldoValorSuperior;
    } //-- int getCdConsultaSaldoValorSuperior() 

    /**
     * Returns the value of field 'cdContagemConsSaudo'.
     * 
     * @return int
     * @return the value of field 'cdContagemConsSaudo'.
     */
    public int getCdContagemConsSaudo()
    {
        return this._cdContagemConsSaudo;
    } //-- int getCdContagemConsSaudo() 

    /**
     * Returns the value of field 'cdContratoContaTransferencia'.
     * 
     * @return int
     * @return the value of field 'cdContratoContaTransferencia'.
     */
    public int getCdContratoContaTransferencia()
    {
        return this._cdContratoContaTransferencia;
    } //-- int getCdContratoContaTransferencia() 

    /**
     * Returns the value of field 'cdCreditoNaoUtilizado'.
     * 
     * @return int
     * @return the value of field 'cdCreditoNaoUtilizado'.
     */
    public int getCdCreditoNaoUtilizado()
    {
        return this._cdCreditoNaoUtilizado;
    } //-- int getCdCreditoNaoUtilizado() 

    /**
     * Returns the value of field 'cdCriterioEnquaBeneficio'.
     * 
     * @return int
     * @return the value of field 'cdCriterioEnquaBeneficio'.
     */
    public int getCdCriterioEnquaBeneficio()
    {
        return this._cdCriterioEnquaBeneficio;
    } //-- int getCdCriterioEnquaBeneficio() 

    /**
     * Returns the value of field 'cdCriterioEnquaRecadastro'.
     * 
     * @return int
     * @return the value of field 'cdCriterioEnquaRecadastro'.
     */
    public int getCdCriterioEnquaRecadastro()
    {
        return this._cdCriterioEnquaRecadastro;
    } //-- int getCdCriterioEnquaRecadastro() 

    /**
     * Returns the value of field
     * 'cdCriterioRastreabilidadeTitulo'.
     * 
     * @return int
     * @return the value of field 'cdCriterioRastreabilidadeTitulo'.
     */
    public int getCdCriterioRastreabilidadeTitulo()
    {
        return this._cdCriterioRastreabilidadeTitulo;
    } //-- int getCdCriterioRastreabilidadeTitulo() 

    /**
     * Returns the value of field 'cdCtciaEspecieBeneficio'.
     * 
     * @return int
     * @return the value of field 'cdCtciaEspecieBeneficio'.
     */
    public int getCdCtciaEspecieBeneficio()
    {
        return this._cdCtciaEspecieBeneficio;
    } //-- int getCdCtciaEspecieBeneficio() 

    /**
     * Returns the value of field 'cdCtciaIdentificacaoBeneficio'.
     * 
     * @return int
     * @return the value of field 'cdCtciaIdentificacaoBeneficio'.
     */
    public int getCdCtciaIdentificacaoBeneficio()
    {
        return this._cdCtciaIdentificacaoBeneficio;
    } //-- int getCdCtciaIdentificacaoBeneficio() 

    /**
     * Returns the value of field 'cdCtciaInscricaoFavorecido'.
     * 
     * @return int
     * @return the value of field 'cdCtciaInscricaoFavorecido'.
     */
    public int getCdCtciaInscricaoFavorecido()
    {
        return this._cdCtciaInscricaoFavorecido;
    } //-- int getCdCtciaInscricaoFavorecido() 

    /**
     * Returns the value of field 'cdCtciaProprietarioVeiculo'.
     * 
     * @return int
     * @return the value of field 'cdCtciaProprietarioVeiculo'.
     */
    public int getCdCtciaProprietarioVeiculo()
    {
        return this._cdCtciaProprietarioVeiculo;
    } //-- int getCdCtciaProprietarioVeiculo() 

    /**
     * Returns the value of field 'cdDestinoAviso'.
     * 
     * @return int
     * @return the value of field 'cdDestinoAviso'.
     */
    public int getCdDestinoAviso()
    {
        return this._cdDestinoAviso;
    } //-- int getCdDestinoAviso() 

    /**
     * Returns the value of field 'cdDestinoComprovante'.
     * 
     * @return int
     * @return the value of field 'cdDestinoComprovante'.
     */
    public int getCdDestinoComprovante()
    {
        return this._cdDestinoComprovante;
    } //-- int getCdDestinoComprovante() 

    /**
     * Returns the value of field 'cdDestinoFormularioRecadastro'.
     * 
     * @return int
     * @return the value of field 'cdDestinoFormularioRecadastro'.
     */
    public int getCdDestinoFormularioRecadastro()
    {
        return this._cdDestinoFormularioRecadastro;
    } //-- int getCdDestinoFormularioRecadastro() 

    /**
     * Returns the value of field 'cdDiaFloatPagamento'.
     * 
     * @return int
     * @return the value of field 'cdDiaFloatPagamento'.
     */
    public int getCdDiaFloatPagamento()
    {
        return this._cdDiaFloatPagamento;
    } //-- int getCdDiaFloatPagamento() 

    /**
     * Returns the value of field 'cdDispzContaCredito'.
     * 
     * @return int
     * @return the value of field 'cdDispzContaCredito'.
     */
    public int getCdDispzContaCredito()
    {
        return this._cdDispzContaCredito;
    } //-- int getCdDispzContaCredito() 

    /**
     * Returns the value of field 'cdDispzDiversarCrrtt'.
     * 
     * @return int
     * @return the value of field 'cdDispzDiversarCrrtt'.
     */
    public int getCdDispzDiversarCrrtt()
    {
        return this._cdDispzDiversarCrrtt;
    } //-- int getCdDispzDiversarCrrtt() 

    /**
     * Returns the value of field 'cdDispzDiversasNao'.
     * 
     * @return int
     * @return the value of field 'cdDispzDiversasNao'.
     */
    public int getCdDispzDiversasNao()
    {
        return this._cdDispzDiversasNao;
    } //-- int getCdDispzDiversasNao() 

    /**
     * Returns the value of field 'cdDispzSalarioCrrtt'.
     * 
     * @return int
     * @return the value of field 'cdDispzSalarioCrrtt'.
     */
    public int getCdDispzSalarioCrrtt()
    {
        return this._cdDispzSalarioCrrtt;
    } //-- int getCdDispzSalarioCrrtt() 

    /**
     * Returns the value of field 'cdDispzSalarioNao'.
     * 
     * @return int
     * @return the value of field 'cdDispzSalarioNao'.
     */
    public int getCdDispzSalarioNao()
    {
        return this._cdDispzSalarioNao;
    } //-- int getCdDispzSalarioNao() 

    /**
     * Returns the value of field 'cdEnvelopeAberto'.
     * 
     * @return int
     * @return the value of field 'cdEnvelopeAberto'.
     */
    public int getCdEnvelopeAberto()
    {
        return this._cdEnvelopeAberto;
    } //-- int getCdEnvelopeAberto() 

    /**
     * Returns the value of field 'cdExigeAutFilial'.
     * 
     * @return int
     * @return the value of field 'cdExigeAutFilial'.
     */
    public int getCdExigeAutFilial()
    {
        return this._cdExigeAutFilial;
    } //-- int getCdExigeAutFilial() 

    /**
     * Returns the value of field 'cdFavorecidoConsPagamento'.
     * 
     * @return int
     * @return the value of field 'cdFavorecidoConsPagamento'.
     */
    public int getCdFavorecidoConsPagamento()
    {
        return this._cdFavorecidoConsPagamento;
    } //-- int getCdFavorecidoConsPagamento() 

    /**
     * Returns the value of field 'cdFloatServicoContrato'.
     * 
     * @return int
     * @return the value of field 'cdFloatServicoContrato'.
     */
    public int getCdFloatServicoContrato()
    {
        return this._cdFloatServicoContrato;
    } //-- int getCdFloatServicoContrato() 

    /**
     * Returns the value of field 'cdFormaAutorizacaoPagamento'.
     * 
     * @return int
     * @return the value of field 'cdFormaAutorizacaoPagamento'.
     */
    public int getCdFormaAutorizacaoPagamento()
    {
        return this._cdFormaAutorizacaoPagamento;
    } //-- int getCdFormaAutorizacaoPagamento() 

    /**
     * Returns the value of field 'cdFormaEnvioPagamento'.
     * 
     * @return int
     * @return the value of field 'cdFormaEnvioPagamento'.
     */
    public int getCdFormaEnvioPagamento()
    {
        return this._cdFormaEnvioPagamento;
    } //-- int getCdFormaEnvioPagamento() 

    /**
     * Returns the value of field 'cdFormaExpiracaoCredito'.
     * 
     * @return int
     * @return the value of field 'cdFormaExpiracaoCredito'.
     */
    public int getCdFormaExpiracaoCredito()
    {
        return this._cdFormaExpiracaoCredito;
    } //-- int getCdFormaExpiracaoCredito() 

    /**
     * Returns the value of field 'cdFormaManutencao'.
     * 
     * @return int
     * @return the value of field 'cdFormaManutencao'.
     */
    public int getCdFormaManutencao()
    {
        return this._cdFormaManutencao;
    } //-- int getCdFormaManutencao() 

    /**
     * Returns the value of field 'cdFormularioContratoCliente'.
     * 
     * @return int
     * @return the value of field 'cdFormularioContratoCliente'.
     */
    public int getCdFormularioContratoCliente()
    {
        return this._cdFormularioContratoCliente;
    } //-- int getCdFormularioContratoCliente() 

    /**
     * Returns the value of field 'cdFrasePreCadastro'.
     * 
     * @return int
     * @return the value of field 'cdFrasePreCadastro'.
     */
    public int getCdFrasePreCadastro()
    {
        return this._cdFrasePreCadastro;
    } //-- int getCdFrasePreCadastro() 

    /**
     * Returns the value of field 'cdIndLancamentoPersonalizado'.
     * 
     * @return int
     * @return the value of field 'cdIndLancamentoPersonalizado'.
     */
    public int getCdIndLancamentoPersonalizado()
    {
        return this._cdIndLancamentoPersonalizado;
    } //-- int getCdIndLancamentoPersonalizado() 

    /**
     * Returns the value of field 'cdIndicadorAdesaoSacador'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorAdesaoSacador'.
     */
    public int getCdIndicadorAdesaoSacador()
    {
        return this._cdIndicadorAdesaoSacador;
    } //-- int getCdIndicadorAdesaoSacador() 

    /**
     * Returns the value of field 'cdIndicadorAgendaGrade'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorAgendaGrade'.
     */
    public int getCdIndicadorAgendaGrade()
    {
        return this._cdIndicadorAgendaGrade;
    } //-- int getCdIndicadorAgendaGrade() 

    /**
     * Returns the value of field 'cdIndicadorAgendaTitulo'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorAgendaTitulo'.
     */
    public int getCdIndicadorAgendaTitulo()
    {
        return this._cdIndicadorAgendaTitulo;
    } //-- int getCdIndicadorAgendaTitulo() 

    /**
     * Returns the value of field 'cdIndicadorAutorizacaoCliente'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorAutorizacaoCliente'.
     */
    public int getCdIndicadorAutorizacaoCliente()
    {
        return this._cdIndicadorAutorizacaoCliente;
    } //-- int getCdIndicadorAutorizacaoCliente() 

    /**
     * Returns the value of field
     * 'cdIndicadorAutorizacaoComplemento'.
     * 
     * @return int
     * @return the value of field
     * 'cdIndicadorAutorizacaoComplemento'.
     */
    public int getCdIndicadorAutorizacaoComplemento()
    {
        return this._cdIndicadorAutorizacaoComplemento;
    } //-- int getCdIndicadorAutorizacaoComplemento() 

    /**
     * Returns the value of field 'cdIndicadorBancoPostal'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorBancoPostal'.
     */
    public int getCdIndicadorBancoPostal()
    {
        return this._cdIndicadorBancoPostal;
    } //-- int getCdIndicadorBancoPostal() 

    /**
     * Returns the value of field 'cdIndicadorCadastroOrg'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorCadastroOrg'.
     */
    public int getCdIndicadorCadastroOrg()
    {
        return this._cdIndicadorCadastroOrg;
    } //-- int getCdIndicadorCadastroOrg() 

    /**
     * Returns the value of field 'cdIndicadorCadastroProcd'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorCadastroProcd'.
     */
    public int getCdIndicadorCadastroProcd()
    {
        return this._cdIndicadorCadastroProcd;
    } //-- int getCdIndicadorCadastroProcd() 

    /**
     * Returns the value of field 'cdIndicadorCataoSalario'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorCataoSalario'.
     */
    public int getCdIndicadorCataoSalario()
    {
        return this._cdIndicadorCataoSalario;
    } //-- int getCdIndicadorCataoSalario() 

    /**
     * Returns the value of field 'cdIndicadorEconomicoReajuste'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorEconomicoReajuste'.
     */
    public int getCdIndicadorEconomicoReajuste()
    {
        return this._cdIndicadorEconomicoReajuste;
    } //-- int getCdIndicadorEconomicoReajuste() 

    /**
     * Returns the value of field 'cdIndicadorEmissaoAviso'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorEmissaoAviso'.
     */
    public int getCdIndicadorEmissaoAviso()
    {
        return this._cdIndicadorEmissaoAviso;
    } //-- int getCdIndicadorEmissaoAviso() 

    /**
     * Returns the value of field 'cdIndicadorExpiracaoCredito'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorExpiracaoCredito'.
     */
    public int getCdIndicadorExpiracaoCredito()
    {
        return this._cdIndicadorExpiracaoCredito;
    } //-- int getCdIndicadorExpiracaoCredito() 

    /**
     * Returns the value of field 'cdIndicadorFeriadoLocal'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorFeriadoLocal'.
     */
    public int getCdIndicadorFeriadoLocal()
    {
        return this._cdIndicadorFeriadoLocal;
    } //-- int getCdIndicadorFeriadoLocal() 

    /**
     * Returns the value of field 'cdIndicadorLancamentoPagamento'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorLancamentoPagamento'.
     */
    public int getCdIndicadorLancamentoPagamento()
    {
        return this._cdIndicadorLancamentoPagamento;
    } //-- int getCdIndicadorLancamentoPagamento() 

    /**
     * Returns the value of field 'cdIndicadorListaDebito'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorListaDebito'.
     */
    public int getCdIndicadorListaDebito()
    {
        return this._cdIndicadorListaDebito;
    } //-- int getCdIndicadorListaDebito() 

    /**
     * Returns the value of field 'cdIndicadorMensagemPerso'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorMensagemPerso'.
     */
    public int getCdIndicadorMensagemPerso()
    {
        return this._cdIndicadorMensagemPerso;
    } //-- int getCdIndicadorMensagemPerso() 

    /**
     * Returns the value of field 'cdIndicadorRetornoInternet'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorRetornoInternet'.
     */
    public int getCdIndicadorRetornoInternet()
    {
        return this._cdIndicadorRetornoInternet;
    } //-- int getCdIndicadorRetornoInternet() 

    /**
     * Returns the value of field 'cdIndicadorRetornoSeparado'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorRetornoSeparado'.
     */
    public int getCdIndicadorRetornoSeparado()
    {
        return this._cdIndicadorRetornoSeparado;
    } //-- int getCdIndicadorRetornoSeparado() 

    /**
     * Returns the value of field 'cdIndicadorSegundaLinha'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorSegundaLinha'.
     */
    public int getCdIndicadorSegundaLinha()
    {
        return this._cdIndicadorSegundaLinha;
    } //-- int getCdIndicadorSegundaLinha() 

    /**
     * Returns the value of field 'cdIndicadorTipoRetornoInternet'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorTipoRetornoInternet'.
     */
    public int getCdIndicadorTipoRetornoInternet()
    {
        return this._cdIndicadorTipoRetornoInternet;
    } //-- int getCdIndicadorTipoRetornoInternet() 

    /**
     * Returns the value of field 'cdIndicadorUtilizaMora'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorUtilizaMora'.
     */
    public int getCdIndicadorUtilizaMora()
    {
        return this._cdIndicadorUtilizaMora;
    } //-- int getCdIndicadorUtilizaMora() 

    /**
     * Returns the value of field 'cdLancamentoFuturoCredito'.
     * 
     * @return int
     * @return the value of field 'cdLancamentoFuturoCredito'.
     */
    public int getCdLancamentoFuturoCredito()
    {
        return this._cdLancamentoFuturoCredito;
    } //-- int getCdLancamentoFuturoCredito() 

    /**
     * Returns the value of field 'cdLancamentoFuturoDebito'.
     * 
     * @return int
     * @return the value of field 'cdLancamentoFuturoDebito'.
     */
    public int getCdLancamentoFuturoDebito()
    {
        return this._cdLancamentoFuturoDebito;
    } //-- int getCdLancamentoFuturoDebito() 

    /**
     * Returns the value of field 'cdLiberacaoLoteProcesso'.
     * 
     * @return int
     * @return the value of field 'cdLiberacaoLoteProcesso'.
     */
    public int getCdLiberacaoLoteProcesso()
    {
        return this._cdLiberacaoLoteProcesso;
    } //-- int getCdLiberacaoLoteProcesso() 

    /**
     * Returns the value of field 'cdManutencaoBaseRecadastro'.
     * 
     * @return int
     * @return the value of field 'cdManutencaoBaseRecadastro'.
     */
    public int getCdManutencaoBaseRecadastro()
    {
        return this._cdManutencaoBaseRecadastro;
    } //-- int getCdManutencaoBaseRecadastro() 

    /**
     * Returns the value of field 'cdMeioPagamentoCredito'.
     * 
     * @return int
     * @return the value of field 'cdMeioPagamentoCredito'.
     */
    public int getCdMeioPagamentoCredito()
    {
        return this._cdMeioPagamentoCredito;
    } //-- int getCdMeioPagamentoCredito() 

    /**
     * Returns the value of field 'cdMensagemRecadastroMidia'.
     * 
     * @return int
     * @return the value of field 'cdMensagemRecadastroMidia'.
     */
    public int getCdMensagemRecadastroMidia()
    {
        return this._cdMensagemRecadastroMidia;
    } //-- int getCdMensagemRecadastroMidia() 

    /**
     * Returns the value of field 'cdMidiaDisponivel'.
     * 
     * @return int
     * @return the value of field 'cdMidiaDisponivel'.
     */
    public int getCdMidiaDisponivel()
    {
        return this._cdMidiaDisponivel;
    } //-- int getCdMidiaDisponivel() 

    /**
     * Returns the value of field 'cdMidiaMensagemRecadastro'.
     * 
     * @return int
     * @return the value of field 'cdMidiaMensagemRecadastro'.
     */
    public int getCdMidiaMensagemRecadastro()
    {
        return this._cdMidiaMensagemRecadastro;
    } //-- int getCdMidiaMensagemRecadastro() 

    /**
     * Returns the value of field 'cdMomentoAvisoRacadastro'.
     * 
     * @return int
     * @return the value of field 'cdMomentoAvisoRacadastro'.
     */
    public int getCdMomentoAvisoRacadastro()
    {
        return this._cdMomentoAvisoRacadastro;
    } //-- int getCdMomentoAvisoRacadastro() 

    /**
     * Returns the value of field 'cdMomentoCreditoEfetivacao'.
     * 
     * @return int
     * @return the value of field 'cdMomentoCreditoEfetivacao'.
     */
    public int getCdMomentoCreditoEfetivacao()
    {
        return this._cdMomentoCreditoEfetivacao;
    } //-- int getCdMomentoCreditoEfetivacao() 

    /**
     * Returns the value of field 'cdMomentoDebitoPagamento'.
     * 
     * @return int
     * @return the value of field 'cdMomentoDebitoPagamento'.
     */
    public int getCdMomentoDebitoPagamento()
    {
        return this._cdMomentoDebitoPagamento;
    } //-- int getCdMomentoDebitoPagamento() 

    /**
     * Returns the value of field 'cdMomentoFormularioRecadastro'.
     * 
     * @return int
     * @return the value of field 'cdMomentoFormularioRecadastro'.
     */
    public int getCdMomentoFormularioRecadastro()
    {
        return this._cdMomentoFormularioRecadastro;
    } //-- int getCdMomentoFormularioRecadastro() 

    /**
     * Returns the value of field
     * 'cdMomentoProcessamentoPagamento'.
     * 
     * @return int
     * @return the value of field 'cdMomentoProcessamentoPagamento'.
     */
    public int getCdMomentoProcessamentoPagamento()
    {
        return this._cdMomentoProcessamentoPagamento;
    } //-- int getCdMomentoProcessamentoPagamento() 

    /**
     * Returns the value of field 'cdNaturezaOperacaoPagamento'.
     * 
     * @return int
     * @return the value of field 'cdNaturezaOperacaoPagamento'.
     */
    public int getCdNaturezaOperacaoPagamento()
    {
        return this._cdNaturezaOperacaoPagamento;
    } //-- int getCdNaturezaOperacaoPagamento() 

    /**
     * Returns the value of field 'cdPagamentoNaoUtilizado'.
     * 
     * @return int
     * @return the value of field 'cdPagamentoNaoUtilizado'.
     */
    public int getCdPagamentoNaoUtilizado()
    {
        return this._cdPagamentoNaoUtilizado;
    } //-- int getCdPagamentoNaoUtilizado() 

    /**
     * Returns the value of field
     * 'cdPercentualIndicadorReajusteTarifa'.
     * 
     * @return BigDecimal
     * @return the value of field
     * 'cdPercentualIndicadorReajusteTarifa'.
     */
    public java.math.BigDecimal getCdPercentualIndicadorReajusteTarifa()
    {
        return this._cdPercentualIndicadorReajusteTarifa;
    } //-- java.math.BigDecimal getCdPercentualIndicadorReajusteTarifa() 

    /**
     * Returns the value of field 'cdPercentualMaximoInconLote'.
     * 
     * @return int
     * @return the value of field 'cdPercentualMaximoInconLote'.
     */
    public int getCdPercentualMaximoInconLote()
    {
        return this._cdPercentualMaximoInconLote;
    } //-- int getCdPercentualMaximoInconLote() 

    /**
     * Returns the value of field
     * 'cdPercentualReducaoTarifaCatalogo'.
     * 
     * @return BigDecimal
     * @return the value of field
     * 'cdPercentualReducaoTarifaCatalogo'.
     */
    public java.math.BigDecimal getCdPercentualReducaoTarifaCatalogo()
    {
        return this._cdPercentualReducaoTarifaCatalogo;
    } //-- java.math.BigDecimal getCdPercentualReducaoTarifaCatalogo() 

    /**
     * Returns the value of field 'cdPerdcCobrancaTarifa'.
     * 
     * @return int
     * @return the value of field 'cdPerdcCobrancaTarifa'.
     */
    public int getCdPerdcCobrancaTarifa()
    {
        return this._cdPerdcCobrancaTarifa;
    } //-- int getCdPerdcCobrancaTarifa() 

    /**
     * Returns the value of field 'cdPerdcComprovante'.
     * 
     * @return int
     * @return the value of field 'cdPerdcComprovante'.
     */
    public int getCdPerdcComprovante()
    {
        return this._cdPerdcComprovante;
    } //-- int getCdPerdcComprovante() 

    /**
     * Returns the value of field 'cdPerdcConsultaVeiculo'.
     * 
     * @return int
     * @return the value of field 'cdPerdcConsultaVeiculo'.
     */
    public int getCdPerdcConsultaVeiculo()
    {
        return this._cdPerdcConsultaVeiculo;
    } //-- int getCdPerdcConsultaVeiculo() 

    /**
     * Returns the value of field 'cdPerdcEnvioRemessa'.
     * 
     * @return int
     * @return the value of field 'cdPerdcEnvioRemessa'.
     */
    public int getCdPerdcEnvioRemessa()
    {
        return this._cdPerdcEnvioRemessa;
    } //-- int getCdPerdcEnvioRemessa() 

    /**
     * Returns the value of field 'cdPerdcManutencaoProcd'.
     * 
     * @return int
     * @return the value of field 'cdPerdcManutencaoProcd'.
     */
    public int getCdPerdcManutencaoProcd()
    {
        return this._cdPerdcManutencaoProcd;
    } //-- int getCdPerdcManutencaoProcd() 

    /**
     * Returns the value of field 'cdPeriodicidadeAviso'.
     * 
     * @return int
     * @return the value of field 'cdPeriodicidadeAviso'.
     */
    public int getCdPeriodicidadeAviso()
    {
        return this._cdPeriodicidadeAviso;
    } //-- int getCdPeriodicidadeAviso() 

    /**
     * Returns the value of field 'cdPermissaoDebitoOnline'.
     * 
     * @return int
     * @return the value of field 'cdPermissaoDebitoOnline'.
     */
    public int getCdPermissaoDebitoOnline()
    {
        return this._cdPermissaoDebitoOnline;
    } //-- int getCdPermissaoDebitoOnline() 

    /**
     * Returns the value of field
     * 'cdPreenchimentoLancamentoPersonalizado'.
     * 
     * @return int
     * @return the value of field
     * 'cdPreenchimentoLancamentoPersonalizado'.
     */
    public int getCdPreenchimentoLancamentoPersonalizado()
    {
        return this._cdPreenchimentoLancamentoPersonalizado;
    } //-- int getCdPreenchimentoLancamentoPersonalizado() 

    /**
     * Returns the value of field 'cdPrincipalEnquaRecadastro'.
     * 
     * @return int
     * @return the value of field 'cdPrincipalEnquaRecadastro'.
     */
    public int getCdPrincipalEnquaRecadastro()
    {
        return this._cdPrincipalEnquaRecadastro;
    } //-- int getCdPrincipalEnquaRecadastro() 

    /**
     * Returns the value of field
     * 'cdPrioridadeEfetivacaoPagamento'.
     * 
     * @return int
     * @return the value of field 'cdPrioridadeEfetivacaoPagamento'.
     */
    public int getCdPrioridadeEfetivacaoPagamento()
    {
        return this._cdPrioridadeEfetivacaoPagamento;
    } //-- int getCdPrioridadeEfetivacaoPagamento() 

    /**
     * Returns the value of field 'cdRastreabilidadeNotaFiscal'.
     * 
     * @return int
     * @return the value of field 'cdRastreabilidadeNotaFiscal'.
     */
    public int getCdRastreabilidadeNotaFiscal()
    {
        return this._cdRastreabilidadeNotaFiscal;
    } //-- int getCdRastreabilidadeNotaFiscal() 

    /**
     * Returns the value of field
     * 'cdRastreabilidadeTituloTerceiro'.
     * 
     * @return int
     * @return the value of field 'cdRastreabilidadeTituloTerceiro'.
     */
    public int getCdRastreabilidadeTituloTerceiro()
    {
        return this._cdRastreabilidadeTituloTerceiro;
    } //-- int getCdRastreabilidadeTituloTerceiro() 

    /**
     * Returns the value of field 'cdRejeicaoAgendaLote'.
     * 
     * @return int
     * @return the value of field 'cdRejeicaoAgendaLote'.
     */
    public int getCdRejeicaoAgendaLote()
    {
        return this._cdRejeicaoAgendaLote;
    } //-- int getCdRejeicaoAgendaLote() 

    /**
     * Returns the value of field 'cdRejeicaoEfetivacaoLote'.
     * 
     * @return int
     * @return the value of field 'cdRejeicaoEfetivacaoLote'.
     */
    public int getCdRejeicaoEfetivacaoLote()
    {
        return this._cdRejeicaoEfetivacaoLote;
    } //-- int getCdRejeicaoEfetivacaoLote() 

    /**
     * Returns the value of field 'cdRejeicaoLote'.
     * 
     * @return int
     * @return the value of field 'cdRejeicaoLote'.
     */
    public int getCdRejeicaoLote()
    {
        return this._cdRejeicaoLote;
    } //-- int getCdRejeicaoLote() 

    /**
     * Returns the value of field 'cdTipoCargaRecadastro'.
     * 
     * @return int
     * @return the value of field 'cdTipoCargaRecadastro'.
     */
    public int getCdTipoCargaRecadastro()
    {
        return this._cdTipoCargaRecadastro;
    } //-- int getCdTipoCargaRecadastro() 

    /**
     * Returns the value of field 'cdTipoCataoSalario'.
     * 
     * @return int
     * @return the value of field 'cdTipoCataoSalario'.
     */
    public int getCdTipoCataoSalario()
    {
        return this._cdTipoCataoSalario;
    } //-- int getCdTipoCataoSalario() 

    /**
     * Returns the value of field 'cdTipoConsistenciaLista'.
     * 
     * @return int
     * @return the value of field 'cdTipoConsistenciaLista'.
     */
    public int getCdTipoConsistenciaLista()
    {
        return this._cdTipoConsistenciaLista;
    } //-- int getCdTipoConsistenciaLista() 

    /**
     * Returns the value of field 'cdTipoConsultaComprovante'.
     * 
     * @return int
     * @return the value of field 'cdTipoConsultaComprovante'.
     */
    public int getCdTipoConsultaComprovante()
    {
        return this._cdTipoConsultaComprovante;
    } //-- int getCdTipoConsultaComprovante() 

    /**
     * Returns the value of field 'cdTipoContaFavorecido'.
     * 
     * @return int
     * @return the value of field 'cdTipoContaFavorecido'.
     */
    public int getCdTipoContaFavorecido()
    {
        return this._cdTipoContaFavorecido;
    } //-- int getCdTipoContaFavorecido() 

    /**
     * Returns the value of field 'cdTipoDataFloat'.
     * 
     * @return int
     * @return the value of field 'cdTipoDataFloat'.
     */
    public int getCdTipoDataFloat()
    {
        return this._cdTipoDataFloat;
    } //-- int getCdTipoDataFloat() 

    /**
     * Returns the value of field 'cdTipoDivergenciaVeiculo'.
     * 
     * @return int
     * @return the value of field 'cdTipoDivergenciaVeiculo'.
     */
    public int getCdTipoDivergenciaVeiculo()
    {
        return this._cdTipoDivergenciaVeiculo;
    } //-- int getCdTipoDivergenciaVeiculo() 

    /**
     * Returns the value of field 'cdTipoFormacaoLista'.
     * 
     * @return int
     * @return the value of field 'cdTipoFormacaoLista'.
     */
    public int getCdTipoFormacaoLista()
    {
        return this._cdTipoFormacaoLista;
    } //-- int getCdTipoFormacaoLista() 

    /**
     * Returns the value of field 'cdTipoIdBeneficio'.
     * 
     * @return int
     * @return the value of field 'cdTipoIdBeneficio'.
     */
    public int getCdTipoIdBeneficio()
    {
        return this._cdTipoIdBeneficio;
    } //-- int getCdTipoIdBeneficio() 

    /**
     * Returns the value of field 'cdTipoIsncricaoFavorecido'.
     * 
     * @return int
     * @return the value of field 'cdTipoIsncricaoFavorecido'.
     */
    public int getCdTipoIsncricaoFavorecido()
    {
        return this._cdTipoIsncricaoFavorecido;
    } //-- int getCdTipoIsncricaoFavorecido() 

    /**
     * Returns the value of field 'cdTipoLayoutArquivo'.
     * 
     * @return int
     * @return the value of field 'cdTipoLayoutArquivo'.
     */
    public int getCdTipoLayoutArquivo()
    {
        return this._cdTipoLayoutArquivo;
    } //-- int getCdTipoLayoutArquivo() 

    /**
     * Returns the value of field 'cdTipoReajusteTarifa'.
     * 
     * @return int
     * @return the value of field 'cdTipoReajusteTarifa'.
     */
    public int getCdTipoReajusteTarifa()
    {
        return this._cdTipoReajusteTarifa;
    } //-- int getCdTipoReajusteTarifa() 

    /**
     * Returns the value of field 'cdTituloDdaRetorno'.
     * 
     * @return int
     * @return the value of field 'cdTituloDdaRetorno'.
     */
    public int getCdTituloDdaRetorno()
    {
        return this._cdTituloDdaRetorno;
    } //-- int getCdTituloDdaRetorno() 

    /**
     * Returns the value of field 'cdUsuarioAlteracao'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioAlteracao'.
     */
    public java.lang.String getCdUsuarioAlteracao()
    {
        return this._cdUsuarioAlteracao;
    } //-- java.lang.String getCdUsuarioAlteracao() 

    /**
     * Returns the value of field 'cdUsuarioExternoAlteracao'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioExternoAlteracao'.
     */
    public java.lang.String getCdUsuarioExternoAlteracao()
    {
        return this._cdUsuarioExternoAlteracao;
    } //-- java.lang.String getCdUsuarioExternoAlteracao() 

    /**
     * Returns the value of field 'cdUsuarioExternoInclusao'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioExternoInclusao'.
     */
    public java.lang.String getCdUsuarioExternoInclusao()
    {
        return this._cdUsuarioExternoInclusao;
    } //-- java.lang.String getCdUsuarioExternoInclusao() 

    /**
     * Returns the value of field 'cdUsuarioInclusao'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioInclusao'.
     */
    public java.lang.String getCdUsuarioInclusao()
    {
        return this._cdUsuarioInclusao;
    } //-- java.lang.String getCdUsuarioInclusao() 

    /**
     * Returns the value of field 'cdUtilizacaoFavorecidoControle'.
     * 
     * @return int
     * @return the value of field 'cdUtilizacaoFavorecidoControle'.
     */
    public int getCdUtilizacaoFavorecidoControle()
    {
        return this._cdUtilizacaoFavorecidoControle;
    } //-- int getCdUtilizacaoFavorecidoControle() 

    /**
     * Returns the value of field 'cdValidacaoNomeFavorecido'.
     * 
     * @return int
     * @return the value of field 'cdValidacaoNomeFavorecido'.
     */
    public int getCdValidacaoNomeFavorecido()
    {
        return this._cdValidacaoNomeFavorecido;
    } //-- int getCdValidacaoNomeFavorecido() 

    /**
     * Returns the value of field 'codMensagem'.
     * 
     * @return String
     * @return the value of field 'codMensagem'.
     */
    public java.lang.String getCodMensagem()
    {
        return this._codMensagem;
    } //-- java.lang.String getCodMensagem() 

    /**
     * Returns the value of field 'dsAcaoNaoVida'.
     * 
     * @return String
     * @return the value of field 'dsAcaoNaoVida'.
     */
    public java.lang.String getDsAcaoNaoVida()
    {
        return this._dsAcaoNaoVida;
    } //-- java.lang.String getDsAcaoNaoVida() 

    /**
     * Returns the value of field 'dsAcertoDadoRecadastro'.
     * 
     * @return String
     * @return the value of field 'dsAcertoDadoRecadastro'.
     */
    public java.lang.String getDsAcertoDadoRecadastro()
    {
        return this._dsAcertoDadoRecadastro;
    } //-- java.lang.String getDsAcertoDadoRecadastro() 

    /**
     * Returns the value of field 'dsAgendaDebitoVeiculo'.
     * 
     * @return String
     * @return the value of field 'dsAgendaDebitoVeiculo'.
     */
    public java.lang.String getDsAgendaDebitoVeiculo()
    {
        return this._dsAgendaDebitoVeiculo;
    } //-- java.lang.String getDsAgendaDebitoVeiculo() 

    /**
     * Returns the value of field 'dsAgendaPagamentoVencido'.
     * 
     * @return String
     * @return the value of field 'dsAgendaPagamentoVencido'.
     */
    public java.lang.String getDsAgendaPagamentoVencido()
    {
        return this._dsAgendaPagamentoVencido;
    } //-- java.lang.String getDsAgendaPagamentoVencido() 

    /**
     * Returns the value of field 'dsAgendaValorMenor'.
     * 
     * @return String
     * @return the value of field 'dsAgendaValorMenor'.
     */
    public java.lang.String getDsAgendaValorMenor()
    {
        return this._dsAgendaValorMenor;
    } //-- java.lang.String getDsAgendaValorMenor() 

    /**
     * Returns the value of field 'dsAgendamentoRastFilial'.
     * 
     * @return String
     * @return the value of field 'dsAgendamentoRastFilial'.
     */
    public java.lang.String getDsAgendamentoRastFilial()
    {
        return this._dsAgendamentoRastFilial;
    } //-- java.lang.String getDsAgendamentoRastFilial() 

    /**
     * Returns the value of field 'dsAgrupamentoAviso'.
     * 
     * @return String
     * @return the value of field 'dsAgrupamentoAviso'.
     */
    public java.lang.String getDsAgrupamentoAviso()
    {
        return this._dsAgrupamentoAviso;
    } //-- java.lang.String getDsAgrupamentoAviso() 

    /**
     * Returns the value of field 'dsAgrupamentoComprovado'.
     * 
     * @return String
     * @return the value of field 'dsAgrupamentoComprovado'.
     */
    public java.lang.String getDsAgrupamentoComprovado()
    {
        return this._dsAgrupamentoComprovado;
    } //-- java.lang.String getDsAgrupamentoComprovado() 

    /**
     * Returns the value of field
     * 'dsAgrupamentoFormularioRecadastro'.
     * 
     * @return String
     * @return the value of field
     * 'dsAgrupamentoFormularioRecadastro'.
     */
    public java.lang.String getDsAgrupamentoFormularioRecadastro()
    {
        return this._dsAgrupamentoFormularioRecadastro;
    } //-- java.lang.String getDsAgrupamentoFormularioRecadastro() 

    /**
     * Returns the value of field 'dsAntecRecadastroBeneficio'.
     * 
     * @return String
     * @return the value of field 'dsAntecRecadastroBeneficio'.
     */
    public java.lang.String getDsAntecRecadastroBeneficio()
    {
        return this._dsAntecRecadastroBeneficio;
    } //-- java.lang.String getDsAntecRecadastroBeneficio() 

    /**
     * Returns the value of field 'dsAreaReservada'.
     * 
     * @return String
     * @return the value of field 'dsAreaReservada'.
     */
    public java.lang.String getDsAreaReservada()
    {
        return this._dsAreaReservada;
    } //-- java.lang.String getDsAreaReservada() 

    /**
     * Returns the value of field 'dsAreaResrd'.
     * 
     * @return String
     * @return the value of field 'dsAreaResrd'.
     */
    public java.lang.String getDsAreaResrd()
    {
        return this._dsAreaResrd;
    } //-- java.lang.String getDsAreaResrd() 

    /**
     * Returns the value of field 'dsBaseRecadastroBeneficio'.
     * 
     * @return String
     * @return the value of field 'dsBaseRecadastroBeneficio'.
     */
    public java.lang.String getDsBaseRecadastroBeneficio()
    {
        return this._dsBaseRecadastroBeneficio;
    } //-- java.lang.String getDsBaseRecadastroBeneficio() 

    /**
     * Returns the value of field 'dsBloqueioEmissaoPplta'.
     * 
     * @return String
     * @return the value of field 'dsBloqueioEmissaoPplta'.
     */
    public java.lang.String getDsBloqueioEmissaoPplta()
    {
        return this._dsBloqueioEmissaoPplta;
    } //-- java.lang.String getDsBloqueioEmissaoPplta() 

    /**
     * Returns the value of field 'dsCanalAlteracao'.
     * 
     * @return String
     * @return the value of field 'dsCanalAlteracao'.
     */
    public java.lang.String getDsCanalAlteracao()
    {
        return this._dsCanalAlteracao;
    } //-- java.lang.String getDsCanalAlteracao() 

    /**
     * Returns the value of field 'dsCanalInclusao'.
     * 
     * @return String
     * @return the value of field 'dsCanalInclusao'.
     */
    public java.lang.String getDsCanalInclusao()
    {
        return this._dsCanalInclusao;
    } //-- java.lang.String getDsCanalInclusao() 

    /**
     * Returns the value of field 'dsCapituloTituloRegistro'.
     * 
     * @return String
     * @return the value of field 'dsCapituloTituloRegistro'.
     */
    public java.lang.String getDsCapituloTituloRegistro()
    {
        return this._dsCapituloTituloRegistro;
    } //-- java.lang.String getDsCapituloTituloRegistro() 

    /**
     * Returns the value of field 'dsCobrancaTarifa'.
     * 
     * @return String
     * @return the value of field 'dsCobrancaTarifa'.
     */
    public java.lang.String getDsCobrancaTarifa()
    {
        return this._dsCobrancaTarifa;
    } //-- java.lang.String getDsCobrancaTarifa() 

    /**
     * Returns the value of field
     * 'dsCodigoFormularioContratoCliente'.
     * 
     * @return String
     * @return the value of field
     * 'dsCodigoFormularioContratoCliente'.
     */
    public java.lang.String getDsCodigoFormularioContratoCliente()
    {
        return this._dsCodigoFormularioContratoCliente;
    } //-- java.lang.String getDsCodigoFormularioContratoCliente() 

    /**
     * Returns the value of field 'dsCodigoIndFeriadoLocal'.
     * 
     * @return String
     * @return the value of field 'dsCodigoIndFeriadoLocal'.
     */
    public java.lang.String getDsCodigoIndFeriadoLocal()
    {
        return this._dsCodigoIndFeriadoLocal;
    } //-- java.lang.String getDsCodigoIndFeriadoLocal() 

    /**
     * Returns the value of field
     * 'dsCodigoIndicadorRetornoSeparado'.
     * 
     * @return String
     * @return the value of field 'dsCodigoIndicadorRetornoSeparado'
     */
    public java.lang.String getDsCodigoIndicadorRetornoSeparado()
    {
        return this._dsCodigoIndicadorRetornoSeparado;
    } //-- java.lang.String getDsCodigoIndicadorRetornoSeparado() 

    /**
     * Returns the value of field 'dsConsDebitoVeiculo'.
     * 
     * @return String
     * @return the value of field 'dsConsDebitoVeiculo'.
     */
    public java.lang.String getDsConsDebitoVeiculo()
    {
        return this._dsConsDebitoVeiculo;
    } //-- java.lang.String getDsConsDebitoVeiculo() 

    /**
     * Returns the value of field 'dsConsEndereco'.
     * 
     * @return String
     * @return the value of field 'dsConsEndereco'.
     */
    public java.lang.String getDsConsEndereco()
    {
        return this._dsConsEndereco;
    } //-- java.lang.String getDsConsEndereco() 

    /**
     * Returns the value of field 'dsConsSaldoPagamento'.
     * 
     * @return String
     * @return the value of field 'dsConsSaldoPagamento'.
     */
    public java.lang.String getDsConsSaldoPagamento()
    {
        return this._dsConsSaldoPagamento;
    } //-- java.lang.String getDsConsSaldoPagamento() 

    /**
     * Returns the value of field
     * 'dsConsistenciaCpfCnpjBenefAvalNpc'.
     * 
     * @return String
     * @return the value of field
     * 'dsConsistenciaCpfCnpjBenefAvalNpc'.
     */
    public java.lang.String getDsConsistenciaCpfCnpjBenefAvalNpc()
    {
        return this._dsConsistenciaCpfCnpjBenefAvalNpc;
    } //-- java.lang.String getDsConsistenciaCpfCnpjBenefAvalNpc() 

    /**
     * Returns the value of field 'dsConsultaSaldoSuperior'.
     * 
     * @return String
     * @return the value of field 'dsConsultaSaldoSuperior'.
     */
    public java.lang.String getDsConsultaSaldoSuperior()
    {
        return this._dsConsultaSaldoSuperior;
    } //-- java.lang.String getDsConsultaSaldoSuperior() 

    /**
     * Returns the value of field 'dsContagemConsSaudo'.
     * 
     * @return String
     * @return the value of field 'dsContagemConsSaudo'.
     */
    public java.lang.String getDsContagemConsSaudo()
    {
        return this._dsContagemConsSaudo;
    } //-- java.lang.String getDsContagemConsSaudo() 

    /**
     * Returns the value of field 'dsContratoContaTransferencia'.
     * 
     * @return String
     * @return the value of field 'dsContratoContaTransferencia'.
     */
    public java.lang.String getDsContratoContaTransferencia()
    {
        return this._dsContratoContaTransferencia;
    } //-- java.lang.String getDsContratoContaTransferencia() 

    /**
     * Returns the value of field 'dsCreditoNaoUtilizado'.
     * 
     * @return String
     * @return the value of field 'dsCreditoNaoUtilizado'.
     */
    public java.lang.String getDsCreditoNaoUtilizado()
    {
        return this._dsCreditoNaoUtilizado;
    } //-- java.lang.String getDsCreditoNaoUtilizado() 

    /**
     * Returns the value of field 'dsCriterioEnquaBeneficio'.
     * 
     * @return String
     * @return the value of field 'dsCriterioEnquaBeneficio'.
     */
    public java.lang.String getDsCriterioEnquaBeneficio()
    {
        return this._dsCriterioEnquaBeneficio;
    } //-- java.lang.String getDsCriterioEnquaBeneficio() 

    /**
     * Returns the value of field 'dsCriterioEnquaRecadastro'.
     * 
     * @return String
     * @return the value of field 'dsCriterioEnquaRecadastro'.
     */
    public java.lang.String getDsCriterioEnquaRecadastro()
    {
        return this._dsCriterioEnquaRecadastro;
    } //-- java.lang.String getDsCriterioEnquaRecadastro() 

    /**
     * Returns the value of field
     * 'dsCriterioRastreabilidadeTitulo'.
     * 
     * @return String
     * @return the value of field 'dsCriterioRastreabilidadeTitulo'.
     */
    public java.lang.String getDsCriterioRastreabilidadeTitulo()
    {
        return this._dsCriterioRastreabilidadeTitulo;
    } //-- java.lang.String getDsCriterioRastreabilidadeTitulo() 

    /**
     * Returns the value of field 'dsCtciaEspecieBeneficio'.
     * 
     * @return String
     * @return the value of field 'dsCtciaEspecieBeneficio'.
     */
    public java.lang.String getDsCtciaEspecieBeneficio()
    {
        return this._dsCtciaEspecieBeneficio;
    } //-- java.lang.String getDsCtciaEspecieBeneficio() 

    /**
     * Returns the value of field 'dsCtciaIdentificacaoBeneficio'.
     * 
     * @return String
     * @return the value of field 'dsCtciaIdentificacaoBeneficio'.
     */
    public java.lang.String getDsCtciaIdentificacaoBeneficio()
    {
        return this._dsCtciaIdentificacaoBeneficio;
    } //-- java.lang.String getDsCtciaIdentificacaoBeneficio() 

    /**
     * Returns the value of field 'dsCtciaInscricaoFavorecido'.
     * 
     * @return String
     * @return the value of field 'dsCtciaInscricaoFavorecido'.
     */
    public java.lang.String getDsCtciaInscricaoFavorecido()
    {
        return this._dsCtciaInscricaoFavorecido;
    } //-- java.lang.String getDsCtciaInscricaoFavorecido() 

    /**
     * Returns the value of field 'dsCtciaProprietarioVeiculo'.
     * 
     * @return String
     * @return the value of field 'dsCtciaProprietarioVeiculo'.
     */
    public java.lang.String getDsCtciaProprietarioVeiculo()
    {
        return this._dsCtciaProprietarioVeiculo;
    } //-- java.lang.String getDsCtciaProprietarioVeiculo() 

    /**
     * Returns the value of field 'dsDestinoAviso'.
     * 
     * @return String
     * @return the value of field 'dsDestinoAviso'.
     */
    public java.lang.String getDsDestinoAviso()
    {
        return this._dsDestinoAviso;
    } //-- java.lang.String getDsDestinoAviso() 

    /**
     * Returns the value of field 'dsDestinoComprovante'.
     * 
     * @return String
     * @return the value of field 'dsDestinoComprovante'.
     */
    public java.lang.String getDsDestinoComprovante()
    {
        return this._dsDestinoComprovante;
    } //-- java.lang.String getDsDestinoComprovante() 

    /**
     * Returns the value of field 'dsDestinoFormularioRecadastro'.
     * 
     * @return String
     * @return the value of field 'dsDestinoFormularioRecadastro'.
     */
    public java.lang.String getDsDestinoFormularioRecadastro()
    {
        return this._dsDestinoFormularioRecadastro;
    } //-- java.lang.String getDsDestinoFormularioRecadastro() 

    /**
     * Returns the value of field 'dsDispzContaCredito'.
     * 
     * @return String
     * @return the value of field 'dsDispzContaCredito'.
     */
    public java.lang.String getDsDispzContaCredito()
    {
        return this._dsDispzContaCredito;
    } //-- java.lang.String getDsDispzContaCredito() 

    /**
     * Returns the value of field 'dsDispzDiversarCrrtt'.
     * 
     * @return String
     * @return the value of field 'dsDispzDiversarCrrtt'.
     */
    public java.lang.String getDsDispzDiversarCrrtt()
    {
        return this._dsDispzDiversarCrrtt;
    } //-- java.lang.String getDsDispzDiversarCrrtt() 

    /**
     * Returns the value of field 'dsDispzDiversasNao'.
     * 
     * @return String
     * @return the value of field 'dsDispzDiversasNao'.
     */
    public java.lang.String getDsDispzDiversasNao()
    {
        return this._dsDispzDiversasNao;
    } //-- java.lang.String getDsDispzDiversasNao() 

    /**
     * Returns the value of field 'dsDispzSalarioCrrtt'.
     * 
     * @return String
     * @return the value of field 'dsDispzSalarioCrrtt'.
     */
    public java.lang.String getDsDispzSalarioCrrtt()
    {
        return this._dsDispzSalarioCrrtt;
    } //-- java.lang.String getDsDispzSalarioCrrtt() 

    /**
     * Returns the value of field 'dsDispzSalarioNao'.
     * 
     * @return String
     * @return the value of field 'dsDispzSalarioNao'.
     */
    public java.lang.String getDsDispzSalarioNao()
    {
        return this._dsDispzSalarioNao;
    } //-- java.lang.String getDsDispzSalarioNao() 

    /**
     * Returns the value of field 'dsEnvelopeAberto'.
     * 
     * @return String
     * @return the value of field 'dsEnvelopeAberto'.
     */
    public java.lang.String getDsEnvelopeAberto()
    {
        return this._dsEnvelopeAberto;
    } //-- java.lang.String getDsEnvelopeAberto() 

    /**
     * Returns the value of field 'dsExigeFilial'.
     * 
     * @return String
     * @return the value of field 'dsExigeFilial'.
     */
    public java.lang.String getDsExigeFilial()
    {
        return this._dsExigeFilial;
    } //-- java.lang.String getDsExigeFilial() 

    /**
     * Returns the value of field 'dsFavorecidoConsPagamento'.
     * 
     * @return String
     * @return the value of field 'dsFavorecidoConsPagamento'.
     */
    public java.lang.String getDsFavorecidoConsPagamento()
    {
        return this._dsFavorecidoConsPagamento;
    } //-- java.lang.String getDsFavorecidoConsPagamento() 

    /**
     * Returns the value of field 'dsFloatServicoContrato'.
     * 
     * @return String
     * @return the value of field 'dsFloatServicoContrato'.
     */
    public java.lang.String getDsFloatServicoContrato()
    {
        return this._dsFloatServicoContrato;
    } //-- java.lang.String getDsFloatServicoContrato() 

    /**
     * Returns the value of field 'dsFormaAutorizacaoPagamento'.
     * 
     * @return String
     * @return the value of field 'dsFormaAutorizacaoPagamento'.
     */
    public java.lang.String getDsFormaAutorizacaoPagamento()
    {
        return this._dsFormaAutorizacaoPagamento;
    } //-- java.lang.String getDsFormaAutorizacaoPagamento() 

    /**
     * Returns the value of field 'dsFormaEnvioPagamento'.
     * 
     * @return String
     * @return the value of field 'dsFormaEnvioPagamento'.
     */
    public java.lang.String getDsFormaEnvioPagamento()
    {
        return this._dsFormaEnvioPagamento;
    } //-- java.lang.String getDsFormaEnvioPagamento() 

    /**
     * Returns the value of field 'dsFormaExpiracaoCredito'.
     * 
     * @return String
     * @return the value of field 'dsFormaExpiracaoCredito'.
     */
    public java.lang.String getDsFormaExpiracaoCredito()
    {
        return this._dsFormaExpiracaoCredito;
    } //-- java.lang.String getDsFormaExpiracaoCredito() 

    /**
     * Returns the value of field 'dsFormaManutencao'.
     * 
     * @return String
     * @return the value of field 'dsFormaManutencao'.
     */
    public java.lang.String getDsFormaManutencao()
    {
        return this._dsFormaManutencao;
    } //-- java.lang.String getDsFormaManutencao() 

    /**
     * Returns the value of field 'dsFrasePreCadastro'.
     * 
     * @return String
     * @return the value of field 'dsFrasePreCadastro'.
     */
    public java.lang.String getDsFrasePreCadastro()
    {
        return this._dsFrasePreCadastro;
    } //-- java.lang.String getDsFrasePreCadastro() 

    /**
     * Returns the value of field 'dsIndLancamentoPersonalizado'.
     * 
     * @return String
     * @return the value of field 'dsIndLancamentoPersonalizado'.
     */
    public java.lang.String getDsIndLancamentoPersonalizado()
    {
        return this._dsIndLancamentoPersonalizado;
    } //-- java.lang.String getDsIndLancamentoPersonalizado() 

    /**
     * Returns the value of field 'dsIndicador'.
     * 
     * @return String
     * @return the value of field 'dsIndicador'.
     */
    public java.lang.String getDsIndicador()
    {
        return this._dsIndicador;
    } //-- java.lang.String getDsIndicador() 

    /**
     * Returns the value of field 'dsIndicadorAdesaoSacador'.
     * 
     * @return String
     * @return the value of field 'dsIndicadorAdesaoSacador'.
     */
    public java.lang.String getDsIndicadorAdesaoSacador()
    {
        return this._dsIndicadorAdesaoSacador;
    } //-- java.lang.String getDsIndicadorAdesaoSacador() 

    /**
     * Returns the value of field 'dsIndicadorAgendaGrade'.
     * 
     * @return String
     * @return the value of field 'dsIndicadorAgendaGrade'.
     */
    public java.lang.String getDsIndicadorAgendaGrade()
    {
        return this._dsIndicadorAgendaGrade;
    } //-- java.lang.String getDsIndicadorAgendaGrade() 

    /**
     * Returns the value of field 'dsIndicadorAgendaTitulo'.
     * 
     * @return String
     * @return the value of field 'dsIndicadorAgendaTitulo'.
     */
    public java.lang.String getDsIndicadorAgendaTitulo()
    {
        return this._dsIndicadorAgendaTitulo;
    } //-- java.lang.String getDsIndicadorAgendaTitulo() 

    /**
     * Returns the value of field 'dsIndicadorAutorizacaoCliente'.
     * 
     * @return String
     * @return the value of field 'dsIndicadorAutorizacaoCliente'.
     */
    public java.lang.String getDsIndicadorAutorizacaoCliente()
    {
        return this._dsIndicadorAutorizacaoCliente;
    } //-- java.lang.String getDsIndicadorAutorizacaoCliente() 

    /**
     * Returns the value of field
     * 'dsIndicadorAutorizacaoComplemento'.
     * 
     * @return String
     * @return the value of field
     * 'dsIndicadorAutorizacaoComplemento'.
     */
    public java.lang.String getDsIndicadorAutorizacaoComplemento()
    {
        return this._dsIndicadorAutorizacaoComplemento;
    } //-- java.lang.String getDsIndicadorAutorizacaoComplemento() 

    /**
     * Returns the value of field 'dsIndicadorBancoPostal'.
     * 
     * @return String
     * @return the value of field 'dsIndicadorBancoPostal'.
     */
    public java.lang.String getDsIndicadorBancoPostal()
    {
        return this._dsIndicadorBancoPostal;
    } //-- java.lang.String getDsIndicadorBancoPostal() 

    /**
     * Returns the value of field 'dsIndicadorCadastroOrg'.
     * 
     * @return String
     * @return the value of field 'dsIndicadorCadastroOrg'.
     */
    public java.lang.String getDsIndicadorCadastroOrg()
    {
        return this._dsIndicadorCadastroOrg;
    } //-- java.lang.String getDsIndicadorCadastroOrg() 

    /**
     * Returns the value of field 'dsIndicadorCadastroProcd'.
     * 
     * @return String
     * @return the value of field 'dsIndicadorCadastroProcd'.
     */
    public java.lang.String getDsIndicadorCadastroProcd()
    {
        return this._dsIndicadorCadastroProcd;
    } //-- java.lang.String getDsIndicadorCadastroProcd() 

    /**
     * Returns the value of field 'dsIndicadorCataoSalario'.
     * 
     * @return String
     * @return the value of field 'dsIndicadorCataoSalario'.
     */
    public java.lang.String getDsIndicadorCataoSalario()
    {
        return this._dsIndicadorCataoSalario;
    } //-- java.lang.String getDsIndicadorCataoSalario() 

    /**
     * Returns the value of field 'dsIndicadorEconomicoReajuste'.
     * 
     * @return String
     * @return the value of field 'dsIndicadorEconomicoReajuste'.
     */
    public java.lang.String getDsIndicadorEconomicoReajuste()
    {
        return this._dsIndicadorEconomicoReajuste;
    } //-- java.lang.String getDsIndicadorEconomicoReajuste() 

    /**
     * Returns the value of field 'dsIndicadorEmissaoAviso'.
     * 
     * @return String
     * @return the value of field 'dsIndicadorEmissaoAviso'.
     */
    public java.lang.String getDsIndicadorEmissaoAviso()
    {
        return this._dsIndicadorEmissaoAviso;
    } //-- java.lang.String getDsIndicadorEmissaoAviso() 

    /**
     * Returns the value of field 'dsIndicadorExpiracaoCredito'.
     * 
     * @return String
     * @return the value of field 'dsIndicadorExpiracaoCredito'.
     */
    public java.lang.String getDsIndicadorExpiracaoCredito()
    {
        return this._dsIndicadorExpiracaoCredito;
    } //-- java.lang.String getDsIndicadorExpiracaoCredito() 

    /**
     * Returns the value of field 'dsIndicadorLancamentoPagamento'.
     * 
     * @return String
     * @return the value of field 'dsIndicadorLancamentoPagamento'.
     */
    public java.lang.String getDsIndicadorLancamentoPagamento()
    {
        return this._dsIndicadorLancamentoPagamento;
    } //-- java.lang.String getDsIndicadorLancamentoPagamento() 

    /**
     * Returns the value of field 'dsIndicadorListaDebito'.
     * 
     * @return String
     * @return the value of field 'dsIndicadorListaDebito'.
     */
    public java.lang.String getDsIndicadorListaDebito()
    {
        return this._dsIndicadorListaDebito;
    } //-- java.lang.String getDsIndicadorListaDebito() 

    /**
     * Returns the value of field 'dsIndicadorMensagemPerso'.
     * 
     * @return String
     * @return the value of field 'dsIndicadorMensagemPerso'.
     */
    public java.lang.String getDsIndicadorMensagemPerso()
    {
        return this._dsIndicadorMensagemPerso;
    } //-- java.lang.String getDsIndicadorMensagemPerso() 

    /**
     * Returns the value of field 'dsIndicadorRetornoInternet'.
     * 
     * @return String
     * @return the value of field 'dsIndicadorRetornoInternet'.
     */
    public java.lang.String getDsIndicadorRetornoInternet()
    {
        return this._dsIndicadorRetornoInternet;
    } //-- java.lang.String getDsIndicadorRetornoInternet() 

    /**
     * Returns the value of field 'dsIndicadorSegundaLinha'.
     * 
     * @return String
     * @return the value of field 'dsIndicadorSegundaLinha'.
     */
    public java.lang.String getDsIndicadorSegundaLinha()
    {
        return this._dsIndicadorSegundaLinha;
    } //-- java.lang.String getDsIndicadorSegundaLinha() 

    /**
     * Returns the value of field 'dsIndicadorTipoRetornoInternet'.
     * 
     * @return String
     * @return the value of field 'dsIndicadorTipoRetornoInternet'.
     */
    public java.lang.String getDsIndicadorTipoRetornoInternet()
    {
        return this._dsIndicadorTipoRetornoInternet;
    } //-- java.lang.String getDsIndicadorTipoRetornoInternet() 

    /**
     * Returns the value of field 'dsIndicadorUtilizaMora'.
     * 
     * @return String
     * @return the value of field 'dsIndicadorUtilizaMora'.
     */
    public java.lang.String getDsIndicadorUtilizaMora()
    {
        return this._dsIndicadorUtilizaMora;
    } //-- java.lang.String getDsIndicadorUtilizaMora() 

    /**
     * Returns the value of field 'dsLancamentoFuturoCredito'.
     * 
     * @return String
     * @return the value of field 'dsLancamentoFuturoCredito'.
     */
    public java.lang.String getDsLancamentoFuturoCredito()
    {
        return this._dsLancamentoFuturoCredito;
    } //-- java.lang.String getDsLancamentoFuturoCredito() 

    /**
     * Returns the value of field 'dsLancamentoFuturoDebito'.
     * 
     * @return String
     * @return the value of field 'dsLancamentoFuturoDebito'.
     */
    public java.lang.String getDsLancamentoFuturoDebito()
    {
        return this._dsLancamentoFuturoDebito;
    } //-- java.lang.String getDsLancamentoFuturoDebito() 

    /**
     * Returns the value of field 'dsLiberacaoLoteProcesso'.
     * 
     * @return String
     * @return the value of field 'dsLiberacaoLoteProcesso'.
     */
    public java.lang.String getDsLiberacaoLoteProcesso()
    {
        return this._dsLiberacaoLoteProcesso;
    } //-- java.lang.String getDsLiberacaoLoteProcesso() 

    /**
     * Returns the value of field 'dsLocalEmissao'.
     * 
     * @return String
     * @return the value of field 'dsLocalEmissao'.
     */
    public java.lang.String getDsLocalEmissao()
    {
        return this._dsLocalEmissao;
    } //-- java.lang.String getDsLocalEmissao() 

    /**
     * Returns the value of field 'dsManutencaoBaseRecadastro'.
     * 
     * @return String
     * @return the value of field 'dsManutencaoBaseRecadastro'.
     */
    public java.lang.String getDsManutencaoBaseRecadastro()
    {
        return this._dsManutencaoBaseRecadastro;
    } //-- java.lang.String getDsManutencaoBaseRecadastro() 

    /**
     * Returns the value of field 'dsMeioPagamentoCredito'.
     * 
     * @return String
     * @return the value of field 'dsMeioPagamentoCredito'.
     */
    public java.lang.String getDsMeioPagamentoCredito()
    {
        return this._dsMeioPagamentoCredito;
    } //-- java.lang.String getDsMeioPagamentoCredito() 

    /**
     * Returns the value of field 'dsMensagemRecadastroMidia'.
     * 
     * @return String
     * @return the value of field 'dsMensagemRecadastroMidia'.
     */
    public java.lang.String getDsMensagemRecadastroMidia()
    {
        return this._dsMensagemRecadastroMidia;
    } //-- java.lang.String getDsMensagemRecadastroMidia() 

    /**
     * Returns the value of field 'dsMidiaDisponivel'.
     * 
     * @return String
     * @return the value of field 'dsMidiaDisponivel'.
     */
    public java.lang.String getDsMidiaDisponivel()
    {
        return this._dsMidiaDisponivel;
    } //-- java.lang.String getDsMidiaDisponivel() 

    /**
     * Returns the value of field 'dsMidiaMensagemRecadastro'.
     * 
     * @return String
     * @return the value of field 'dsMidiaMensagemRecadastro'.
     */
    public java.lang.String getDsMidiaMensagemRecadastro()
    {
        return this._dsMidiaMensagemRecadastro;
    } //-- java.lang.String getDsMidiaMensagemRecadastro() 

    /**
     * Returns the value of field 'dsMomentoAvisoRacadastro'.
     * 
     * @return String
     * @return the value of field 'dsMomentoAvisoRacadastro'.
     */
    public java.lang.String getDsMomentoAvisoRacadastro()
    {
        return this._dsMomentoAvisoRacadastro;
    } //-- java.lang.String getDsMomentoAvisoRacadastro() 

    /**
     * Returns the value of field 'dsMomentoCreditoEfetivacao'.
     * 
     * @return String
     * @return the value of field 'dsMomentoCreditoEfetivacao'.
     */
    public java.lang.String getDsMomentoCreditoEfetivacao()
    {
        return this._dsMomentoCreditoEfetivacao;
    } //-- java.lang.String getDsMomentoCreditoEfetivacao() 

    /**
     * Returns the value of field 'dsMomentoDebitoPagamento'.
     * 
     * @return String
     * @return the value of field 'dsMomentoDebitoPagamento'.
     */
    public java.lang.String getDsMomentoDebitoPagamento()
    {
        return this._dsMomentoDebitoPagamento;
    } //-- java.lang.String getDsMomentoDebitoPagamento() 

    /**
     * Returns the value of field 'dsMomentoFormularioRecadastro'.
     * 
     * @return String
     * @return the value of field 'dsMomentoFormularioRecadastro'.
     */
    public java.lang.String getDsMomentoFormularioRecadastro()
    {
        return this._dsMomentoFormularioRecadastro;
    } //-- java.lang.String getDsMomentoFormularioRecadastro() 

    /**
     * Returns the value of field
     * 'dsMomentoProcessamentoPagamento'.
     * 
     * @return String
     * @return the value of field 'dsMomentoProcessamentoPagamento'.
     */
    public java.lang.String getDsMomentoProcessamentoPagamento()
    {
        return this._dsMomentoProcessamentoPagamento;
    } //-- java.lang.String getDsMomentoProcessamentoPagamento() 

    /**
     * Returns the value of field 'dsNaturezaOperacaoPagamento'.
     * 
     * @return String
     * @return the value of field 'dsNaturezaOperacaoPagamento'.
     */
    public java.lang.String getDsNaturezaOperacaoPagamento()
    {
        return this._dsNaturezaOperacaoPagamento;
    } //-- java.lang.String getDsNaturezaOperacaoPagamento() 

    /**
     * Returns the value of field 'dsOrigemIndicador'.
     * 
     * @return int
     * @return the value of field 'dsOrigemIndicador'.
     */
    public int getDsOrigemIndicador()
    {
        return this._dsOrigemIndicador;
    } //-- int getDsOrigemIndicador() 

    /**
     * Returns the value of field 'dsPagamentoNaoUtilizado'.
     * 
     * @return String
     * @return the value of field 'dsPagamentoNaoUtilizado'.
     */
    public java.lang.String getDsPagamentoNaoUtilizado()
    {
        return this._dsPagamentoNaoUtilizado;
    } //-- java.lang.String getDsPagamentoNaoUtilizado() 

    /**
     * Returns the value of field 'dsPerdcCobrancaTarifa'.
     * 
     * @return String
     * @return the value of field 'dsPerdcCobrancaTarifa'.
     */
    public java.lang.String getDsPerdcCobrancaTarifa()
    {
        return this._dsPerdcCobrancaTarifa;
    } //-- java.lang.String getDsPerdcCobrancaTarifa() 

    /**
     * Returns the value of field 'dsPerdcComprovante'.
     * 
     * @return String
     * @return the value of field 'dsPerdcComprovante'.
     */
    public java.lang.String getDsPerdcComprovante()
    {
        return this._dsPerdcComprovante;
    } //-- java.lang.String getDsPerdcComprovante() 

    /**
     * Returns the value of field 'dsPerdcConsultaVeiculo'.
     * 
     * @return String
     * @return the value of field 'dsPerdcConsultaVeiculo'.
     */
    public java.lang.String getDsPerdcConsultaVeiculo()
    {
        return this._dsPerdcConsultaVeiculo;
    } //-- java.lang.String getDsPerdcConsultaVeiculo() 

    /**
     * Returns the value of field 'dsPerdcEnvioRemessa'.
     * 
     * @return String
     * @return the value of field 'dsPerdcEnvioRemessa'.
     */
    public java.lang.String getDsPerdcEnvioRemessa()
    {
        return this._dsPerdcEnvioRemessa;
    } //-- java.lang.String getDsPerdcEnvioRemessa() 

    /**
     * Returns the value of field 'dsPerdcManutencaoProcd'.
     * 
     * @return String
     * @return the value of field 'dsPerdcManutencaoProcd'.
     */
    public java.lang.String getDsPerdcManutencaoProcd()
    {
        return this._dsPerdcManutencaoProcd;
    } //-- java.lang.String getDsPerdcManutencaoProcd() 

    /**
     * Returns the value of field 'dsPeriodicidadeAviso'.
     * 
     * @return String
     * @return the value of field 'dsPeriodicidadeAviso'.
     */
    public java.lang.String getDsPeriodicidadeAviso()
    {
        return this._dsPeriodicidadeAviso;
    } //-- java.lang.String getDsPeriodicidadeAviso() 

    /**
     * Returns the value of field 'dsPermissaoDebitoOnline'.
     * 
     * @return String
     * @return the value of field 'dsPermissaoDebitoOnline'.
     */
    public java.lang.String getDsPermissaoDebitoOnline()
    {
        return this._dsPermissaoDebitoOnline;
    } //-- java.lang.String getDsPermissaoDebitoOnline() 

    /**
     * Returns the value of field
     * 'dsPreenchimentoLancamentoPersonalizado'.
     * 
     * @return String
     * @return the value of field
     * 'dsPreenchimentoLancamentoPersonalizado'.
     */
    public java.lang.String getDsPreenchimentoLancamentoPersonalizado()
    {
        return this._dsPreenchimentoLancamentoPersonalizado;
    } //-- java.lang.String getDsPreenchimentoLancamentoPersonalizado() 

    /**
     * Returns the value of field 'dsPrincipalEnquaRecadastro'.
     * 
     * @return String
     * @return the value of field 'dsPrincipalEnquaRecadastro'.
     */
    public java.lang.String getDsPrincipalEnquaRecadastro()
    {
        return this._dsPrincipalEnquaRecadastro;
    } //-- java.lang.String getDsPrincipalEnquaRecadastro() 

    /**
     * Returns the value of field
     * 'dsPrioridadeEfetivacaoPagamento'.
     * 
     * @return String
     * @return the value of field 'dsPrioridadeEfetivacaoPagamento'.
     */
    public java.lang.String getDsPrioridadeEfetivacaoPagamento()
    {
        return this._dsPrioridadeEfetivacaoPagamento;
    } //-- java.lang.String getDsPrioridadeEfetivacaoPagamento() 

    /**
     * Returns the value of field 'dsRastreabilidadeNotaFiscal'.
     * 
     * @return String
     * @return the value of field 'dsRastreabilidadeNotaFiscal'.
     */
    public java.lang.String getDsRastreabilidadeNotaFiscal()
    {
        return this._dsRastreabilidadeNotaFiscal;
    } //-- java.lang.String getDsRastreabilidadeNotaFiscal() 

    /**
     * Returns the value of field
     * 'dsRastreabilidadeTituloTerceiro'.
     * 
     * @return String
     * @return the value of field 'dsRastreabilidadeTituloTerceiro'.
     */
    public java.lang.String getDsRastreabilidadeTituloTerceiro()
    {
        return this._dsRastreabilidadeTituloTerceiro;
    } //-- java.lang.String getDsRastreabilidadeTituloTerceiro() 

    /**
     * Returns the value of field 'dsRejeicaoAgendaLote'.
     * 
     * @return String
     * @return the value of field 'dsRejeicaoAgendaLote'.
     */
    public java.lang.String getDsRejeicaoAgendaLote()
    {
        return this._dsRejeicaoAgendaLote;
    } //-- java.lang.String getDsRejeicaoAgendaLote() 

    /**
     * Returns the value of field 'dsRejeicaoEfetivacaoLote'.
     * 
     * @return String
     * @return the value of field 'dsRejeicaoEfetivacaoLote'.
     */
    public java.lang.String getDsRejeicaoEfetivacaoLote()
    {
        return this._dsRejeicaoEfetivacaoLote;
    } //-- java.lang.String getDsRejeicaoEfetivacaoLote() 

    /**
     * Returns the value of field 'dsRejeicaoLote'.
     * 
     * @return String
     * @return the value of field 'dsRejeicaoLote'.
     */
    public java.lang.String getDsRejeicaoLote()
    {
        return this._dsRejeicaoLote;
    } //-- java.lang.String getDsRejeicaoLote() 

    /**
     * Returns the value of field 'dsTipoCargaRecadastro'.
     * 
     * @return String
     * @return the value of field 'dsTipoCargaRecadastro'.
     */
    public java.lang.String getDsTipoCargaRecadastro()
    {
        return this._dsTipoCargaRecadastro;
    } //-- java.lang.String getDsTipoCargaRecadastro() 

    /**
     * Returns the value of field 'dsTipoCataoSalario'.
     * 
     * @return String
     * @return the value of field 'dsTipoCataoSalario'.
     */
    public java.lang.String getDsTipoCataoSalario()
    {
        return this._dsTipoCataoSalario;
    } //-- java.lang.String getDsTipoCataoSalario() 

    /**
     * Returns the value of field 'dsTipoConsistenciaFavorecido'.
     * 
     * @return String
     * @return the value of field 'dsTipoConsistenciaFavorecido'.
     */
    public java.lang.String getDsTipoConsistenciaFavorecido()
    {
        return this._dsTipoConsistenciaFavorecido;
    } //-- java.lang.String getDsTipoConsistenciaFavorecido() 

    /**
     * Returns the value of field 'dsTipoConsistenciaLista'.
     * 
     * @return String
     * @return the value of field 'dsTipoConsistenciaLista'.
     */
    public java.lang.String getDsTipoConsistenciaLista()
    {
        return this._dsTipoConsistenciaLista;
    } //-- java.lang.String getDsTipoConsistenciaLista() 

    /**
     * Returns the value of field 'dsTipoConsolidacaoComprovante'.
     * 
     * @return String
     * @return the value of field 'dsTipoConsolidacaoComprovante'.
     */
    public java.lang.String getDsTipoConsolidacaoComprovante()
    {
        return this._dsTipoConsolidacaoComprovante;
    } //-- java.lang.String getDsTipoConsolidacaoComprovante() 

    /**
     * Returns the value of field 'dsTipoDataFloat'.
     * 
     * @return String
     * @return the value of field 'dsTipoDataFloat'.
     */
    public java.lang.String getDsTipoDataFloat()
    {
        return this._dsTipoDataFloat;
    } //-- java.lang.String getDsTipoDataFloat() 

    /**
     * Returns the value of field 'dsTipoDivergenciaVeiculo'.
     * 
     * @return String
     * @return the value of field 'dsTipoDivergenciaVeiculo'.
     */
    public java.lang.String getDsTipoDivergenciaVeiculo()
    {
        return this._dsTipoDivergenciaVeiculo;
    } //-- java.lang.String getDsTipoDivergenciaVeiculo() 

    /**
     * Returns the value of field 'dsTipoFormacaoLista'.
     * 
     * @return String
     * @return the value of field 'dsTipoFormacaoLista'.
     */
    public java.lang.String getDsTipoFormacaoLista()
    {
        return this._dsTipoFormacaoLista;
    } //-- java.lang.String getDsTipoFormacaoLista() 

    /**
     * Returns the value of field 'dsTipoIdBeneficio'.
     * 
     * @return String
     * @return the value of field 'dsTipoIdBeneficio'.
     */
    public java.lang.String getDsTipoIdBeneficio()
    {
        return this._dsTipoIdBeneficio;
    } //-- java.lang.String getDsTipoIdBeneficio() 

    /**
     * Returns the value of field 'dsTipoIscricaoFavorecido'.
     * 
     * @return String
     * @return the value of field 'dsTipoIscricaoFavorecido'.
     */
    public java.lang.String getDsTipoIscricaoFavorecido()
    {
        return this._dsTipoIscricaoFavorecido;
    } //-- java.lang.String getDsTipoIscricaoFavorecido() 

    /**
     * Returns the value of field 'dsTipoLayoutArquivo'.
     * 
     * @return String
     * @return the value of field 'dsTipoLayoutArquivo'.
     */
    public java.lang.String getDsTipoLayoutArquivo()
    {
        return this._dsTipoLayoutArquivo;
    } //-- java.lang.String getDsTipoLayoutArquivo() 

    /**
     * Returns the value of field 'dsTipoReajusteTarifa'.
     * 
     * @return String
     * @return the value of field 'dsTipoReajusteTarifa'.
     */
    public java.lang.String getDsTipoReajusteTarifa()
    {
        return this._dsTipoReajusteTarifa;
    } //-- java.lang.String getDsTipoReajusteTarifa() 

    /**
     * Returns the value of field 'dsTituloDdaRetorno'.
     * 
     * @return String
     * @return the value of field 'dsTituloDdaRetorno'.
     */
    public java.lang.String getDsTituloDdaRetorno()
    {
        return this._dsTituloDdaRetorno;
    } //-- java.lang.String getDsTituloDdaRetorno() 

    /**
     * Returns the value of field 'dsUtilizacaoFavorecidoControle'.
     * 
     * @return String
     * @return the value of field 'dsUtilizacaoFavorecidoControle'.
     */
    public java.lang.String getDsUtilizacaoFavorecidoControle()
    {
        return this._dsUtilizacaoFavorecidoControle;
    } //-- java.lang.String getDsUtilizacaoFavorecidoControle() 

    /**
     * Returns the value of field 'dsValidacaoNomeFavorecido'.
     * 
     * @return String
     * @return the value of field 'dsValidacaoNomeFavorecido'.
     */
    public java.lang.String getDsValidacaoNomeFavorecido()
    {
        return this._dsValidacaoNomeFavorecido;
    } //-- java.lang.String getDsValidacaoNomeFavorecido() 

    /**
     * Returns the value of field 'dtEnquaContaSalario'.
     * 
     * @return String
     * @return the value of field 'dtEnquaContaSalario'.
     */
    public java.lang.String getDtEnquaContaSalario()
    {
        return this._dtEnquaContaSalario;
    } //-- java.lang.String getDtEnquaContaSalario() 

    /**
     * Returns the value of field 'dtFimAcertoRecadastro'.
     * 
     * @return String
     * @return the value of field 'dtFimAcertoRecadastro'.
     */
    public java.lang.String getDtFimAcertoRecadastro()
    {
        return this._dtFimAcertoRecadastro;
    } //-- java.lang.String getDtFimAcertoRecadastro() 

    /**
     * Returns the value of field 'dtFimRecadastroBeneficio'.
     * 
     * @return String
     * @return the value of field 'dtFimRecadastroBeneficio'.
     */
    public java.lang.String getDtFimRecadastroBeneficio()
    {
        return this._dtFimRecadastroBeneficio;
    } //-- java.lang.String getDtFimRecadastroBeneficio() 

    /**
     * Returns the value of field 'dtInicioBloqueioPplta'.
     * 
     * @return String
     * @return the value of field 'dtInicioBloqueioPplta'.
     */
    public java.lang.String getDtInicioBloqueioPplta()
    {
        return this._dtInicioBloqueioPplta;
    } //-- java.lang.String getDtInicioBloqueioPplta() 

    /**
     * Returns the value of field 'dtInicioRastreabilidadeTitulo'.
     * 
     * @return String
     * @return the value of field 'dtInicioRastreabilidadeTitulo'.
     */
    public java.lang.String getDtInicioRastreabilidadeTitulo()
    {
        return this._dtInicioRastreabilidadeTitulo;
    } //-- java.lang.String getDtInicioRastreabilidadeTitulo() 

    /**
     * Returns the value of field 'dtInicioRecadastroBeneficio'.
     * 
     * @return String
     * @return the value of field 'dtInicioRecadastroBeneficio'.
     */
    public java.lang.String getDtInicioRecadastroBeneficio()
    {
        return this._dtInicioRecadastroBeneficio;
    } //-- java.lang.String getDtInicioRecadastroBeneficio() 

    /**
     * Returns the value of field 'dtLimiteVinculoCarga'.
     * 
     * @return String
     * @return the value of field 'dtLimiteVinculoCarga'.
     */
    public java.lang.String getDtLimiteVinculoCarga()
    {
        return this._dtLimiteVinculoCarga;
    } //-- java.lang.String getDtLimiteVinculoCarga() 

    /**
     * Returns the value of field 'dtinicioAcertoRecadastro'.
     * 
     * @return String
     * @return the value of field 'dtinicioAcertoRecadastro'.
     */
    public java.lang.String getDtinicioAcertoRecadastro()
    {
        return this._dtinicioAcertoRecadastro;
    } //-- java.lang.String getDtinicioAcertoRecadastro() 

    /**
     * Returns the value of field 'hrManutencaoRegistroAlteracao'.
     * 
     * @return String
     * @return the value of field 'hrManutencaoRegistroAlteracao'.
     */
    public java.lang.String getHrManutencaoRegistroAlteracao()
    {
        return this._hrManutencaoRegistroAlteracao;
    } //-- java.lang.String getHrManutencaoRegistroAlteracao() 

    /**
     * Returns the value of field 'hrManutencaoRegistroInclusao'.
     * 
     * @return String
     * @return the value of field 'hrManutencaoRegistroInclusao'.
     */
    public java.lang.String getHrManutencaoRegistroInclusao()
    {
        return this._hrManutencaoRegistroInclusao;
    } //-- java.lang.String getHrManutencaoRegistroInclusao() 

    /**
     * Returns the value of field 'mensagem'.
     * 
     * @return String
     * @return the value of field 'mensagem'.
     */
    public java.lang.String getMensagem()
    {
        return this._mensagem;
    } //-- java.lang.String getMensagem() 

    /**
     * Returns the value of field 'nmOperacaoFluxoAlteracao'.
     * 
     * @return String
     * @return the value of field 'nmOperacaoFluxoAlteracao'.
     */
    public java.lang.String getNmOperacaoFluxoAlteracao()
    {
        return this._nmOperacaoFluxoAlteracao;
    } //-- java.lang.String getNmOperacaoFluxoAlteracao() 

    /**
     * Returns the value of field 'nmOperacaoFluxoInclusao'.
     * 
     * @return String
     * @return the value of field 'nmOperacaoFluxoInclusao'.
     */
    public java.lang.String getNmOperacaoFluxoInclusao()
    {
        return this._nmOperacaoFluxoInclusao;
    } //-- java.lang.String getNmOperacaoFluxoInclusao() 

    /**
     * Returns the value of field 'nrFechamentoApuracaoTarifa'.
     * 
     * @return int
     * @return the value of field 'nrFechamentoApuracaoTarifa'.
     */
    public int getNrFechamentoApuracaoTarifa()
    {
        return this._nrFechamentoApuracaoTarifa;
    } //-- int getNrFechamentoApuracaoTarifa() 

    /**
     * Returns the value of field 'qtAntecedencia'.
     * 
     * @return int
     * @return the value of field 'qtAntecedencia'.
     */
    public int getQtAntecedencia()
    {
        return this._qtAntecedencia;
    } //-- int getQtAntecedencia() 

    /**
     * Returns the value of field 'qtAnteriorVencimentoComprovado'.
     * 
     * @return int
     * @return the value of field 'qtAnteriorVencimentoComprovado'.
     */
    public int getQtAnteriorVencimentoComprovado()
    {
        return this._qtAnteriorVencimentoComprovado;
    } //-- int getQtAnteriorVencimentoComprovado() 

    /**
     * Returns the value of field 'qtDiaCobrancaTarifa'.
     * 
     * @return int
     * @return the value of field 'qtDiaCobrancaTarifa'.
     */
    public int getQtDiaCobrancaTarifa()
    {
        return this._qtDiaCobrancaTarifa;
    } //-- int getQtDiaCobrancaTarifa() 

    /**
     * Returns the value of field 'qtDiaExpiracao'.
     * 
     * @return int
     * @return the value of field 'qtDiaExpiracao'.
     */
    public int getQtDiaExpiracao()
    {
        return this._qtDiaExpiracao;
    } //-- int getQtDiaExpiracao() 

    /**
     * Returns the value of field 'qtDiaInatividadeFavorecido'.
     * 
     * @return int
     * @return the value of field 'qtDiaInatividadeFavorecido'.
     */
    public int getQtDiaInatividadeFavorecido()
    {
        return this._qtDiaInatividadeFavorecido;
    } //-- int getQtDiaInatividadeFavorecido() 

    /**
     * Returns the value of field 'qtDiaRepiqConsulta'.
     * 
     * @return int
     * @return the value of field 'qtDiaRepiqConsulta'.
     */
    public int getQtDiaRepiqConsulta()
    {
        return this._qtDiaRepiqConsulta;
    } //-- int getQtDiaRepiqConsulta() 

    /**
     * Returns the value of field 'qtDiaUtilPgto'.
     * 
     * @return int
     * @return the value of field 'qtDiaUtilPgto'.
     */
    public int getQtDiaUtilPgto()
    {
        return this._qtDiaUtilPgto;
    } //-- int getQtDiaUtilPgto() 

    /**
     * Returns the value of field 'qtEtapasRecadastroBeneficio'.
     * 
     * @return int
     * @return the value of field 'qtEtapasRecadastroBeneficio'.
     */
    public int getQtEtapasRecadastroBeneficio()
    {
        return this._qtEtapasRecadastroBeneficio;
    } //-- int getQtEtapasRecadastroBeneficio() 

    /**
     * Returns the value of field 'qtFaseRecadastroBeneficio'.
     * 
     * @return int
     * @return the value of field 'qtFaseRecadastroBeneficio'.
     */
    public int getQtFaseRecadastroBeneficio()
    {
        return this._qtFaseRecadastroBeneficio;
    } //-- int getQtFaseRecadastroBeneficio() 

    /**
     * Returns the value of field 'qtLimiteLinha'.
     * 
     * @return int
     * @return the value of field 'qtLimiteLinha'.
     */
    public int getQtLimiteLinha()
    {
        return this._qtLimiteLinha;
    } //-- int getQtLimiteLinha() 

    /**
     * Returns the value of field 'qtLimiteSolicitacaoCatao'.
     * 
     * @return int
     * @return the value of field 'qtLimiteSolicitacaoCatao'.
     */
    public int getQtLimiteSolicitacaoCatao()
    {
        return this._qtLimiteSolicitacaoCatao;
    } //-- int getQtLimiteSolicitacaoCatao() 

    /**
     * Returns the value of field 'qtMaximaInconLote'.
     * 
     * @return int
     * @return the value of field 'qtMaximaInconLote'.
     */
    public int getQtMaximaInconLote()
    {
        return this._qtMaximaInconLote;
    } //-- int getQtMaximaInconLote() 

    /**
     * Returns the value of field 'qtMaximaTituloVencido'.
     * 
     * @return int
     * @return the value of field 'qtMaximaTituloVencido'.
     */
    public int getQtMaximaTituloVencido()
    {
        return this._qtMaximaTituloVencido;
    } //-- int getQtMaximaTituloVencido() 

    /**
     * Returns the value of field 'qtMesComprovante'.
     * 
     * @return int
     * @return the value of field 'qtMesComprovante'.
     */
    public int getQtMesComprovante()
    {
        return this._qtMesComprovante;
    } //-- int getQtMesComprovante() 

    /**
     * Returns the value of field 'qtMesEtapaRecadastro'.
     * 
     * @return int
     * @return the value of field 'qtMesEtapaRecadastro'.
     */
    public int getQtMesEtapaRecadastro()
    {
        return this._qtMesEtapaRecadastro;
    } //-- int getQtMesEtapaRecadastro() 

    /**
     * Returns the value of field 'qtMesFaseRecadastro'.
     * 
     * @return int
     * @return the value of field 'qtMesFaseRecadastro'.
     */
    public int getQtMesFaseRecadastro()
    {
        return this._qtMesFaseRecadastro;
    } //-- int getQtMesFaseRecadastro() 

    /**
     * Returns the value of field 'qtMesReajusteTarifa'.
     * 
     * @return int
     * @return the value of field 'qtMesReajusteTarifa'.
     */
    public int getQtMesReajusteTarifa()
    {
        return this._qtMesReajusteTarifa;
    } //-- int getQtMesReajusteTarifa() 

    /**
     * Returns the value of field 'qtViaAviso'.
     * 
     * @return int
     * @return the value of field 'qtViaAviso'.
     */
    public int getQtViaAviso()
    {
        return this._qtViaAviso;
    } //-- int getQtViaAviso() 

    /**
     * Returns the value of field 'qtViaCobranca'.
     * 
     * @return int
     * @return the value of field 'qtViaCobranca'.
     */
    public int getQtViaCobranca()
    {
        return this._qtViaCobranca;
    } //-- int getQtViaCobranca() 

    /**
     * Returns the value of field 'qtViaComprovante'.
     * 
     * @return int
     * @return the value of field 'qtViaComprovante'.
     */
    public int getQtViaComprovante()
    {
        return this._qtViaComprovante;
    } //-- int getQtViaComprovante() 

    /**
     * Returns the value of field 'vlFavorecidoNaoCadastro'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlFavorecidoNaoCadastro'.
     */
    public java.math.BigDecimal getVlFavorecidoNaoCadastro()
    {
        return this._vlFavorecidoNaoCadastro;
    } //-- java.math.BigDecimal getVlFavorecidoNaoCadastro() 

    /**
     * Returns the value of field 'vlLimiteDiaPagamento'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlLimiteDiaPagamento'.
     */
    public java.math.BigDecimal getVlLimiteDiaPagamento()
    {
        return this._vlLimiteDiaPagamento;
    } //-- java.math.BigDecimal getVlLimiteDiaPagamento() 

    /**
     * Returns the value of field 'vlLimiteIndividualPagamento'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlLimiteIndividualPagamento'.
     */
    public java.math.BigDecimal getVlLimiteIndividualPagamento()
    {
        return this._vlLimiteIndividualPagamento;
    } //-- java.math.BigDecimal getVlLimiteIndividualPagamento() 

    /**
     * Returns the value of field 'vlPercentualDiferencaTolerada'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlPercentualDiferencaTolerada'.
     */
    public java.math.BigDecimal getVlPercentualDiferencaTolerada()
    {
        return this._vlPercentualDiferencaTolerada;
    } //-- java.math.BigDecimal getVlPercentualDiferencaTolerada() 

    /**
     * Method hasCdAcaoNaoVida
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAcaoNaoVida()
    {
        return this._has_cdAcaoNaoVida;
    } //-- boolean hasCdAcaoNaoVida() 

    /**
     * Method hasCdAcertoDadoRecadastro
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAcertoDadoRecadastro()
    {
        return this._has_cdAcertoDadoRecadastro;
    } //-- boolean hasCdAcertoDadoRecadastro() 

    /**
     * Method hasCdAgendaDebitoVeiculo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgendaDebitoVeiculo()
    {
        return this._has_cdAgendaDebitoVeiculo;
    } //-- boolean hasCdAgendaDebitoVeiculo() 

    /**
     * Method hasCdAgendaPagamentoVencido
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgendaPagamentoVencido()
    {
        return this._has_cdAgendaPagamentoVencido;
    } //-- boolean hasCdAgendaPagamentoVencido() 

    /**
     * Method hasCdAgendaRastreabilidadeFilial
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgendaRastreabilidadeFilial()
    {
        return this._has_cdAgendaRastreabilidadeFilial;
    } //-- boolean hasCdAgendaRastreabilidadeFilial() 

    /**
     * Method hasCdAgendaValorMenor
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgendaValorMenor()
    {
        return this._has_cdAgendaValorMenor;
    } //-- boolean hasCdAgendaValorMenor() 

    /**
     * Method hasCdAgrupamentoAviso
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgrupamentoAviso()
    {
        return this._has_cdAgrupamentoAviso;
    } //-- boolean hasCdAgrupamentoAviso() 

    /**
     * Method hasCdAgrupamentoComprovado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgrupamentoComprovado()
    {
        return this._has_cdAgrupamentoComprovado;
    } //-- boolean hasCdAgrupamentoComprovado() 

    /**
     * Method hasCdAgrupamentoFormularioRecadastro
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgrupamentoFormularioRecadastro()
    {
        return this._has_cdAgrupamentoFormularioRecadastro;
    } //-- boolean hasCdAgrupamentoFormularioRecadastro() 

    /**
     * Method hasCdAntecRecadastroBeneficio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAntecRecadastroBeneficio()
    {
        return this._has_cdAntecRecadastroBeneficio;
    } //-- boolean hasCdAntecRecadastroBeneficio() 

    /**
     * Method hasCdAreaReservada
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAreaReservada()
    {
        return this._has_cdAreaReservada;
    } //-- boolean hasCdAreaReservada() 

    /**
     * Method hasCdBaseRecadastroBeneficio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdBaseRecadastroBeneficio()
    {
        return this._has_cdBaseRecadastroBeneficio;
    } //-- boolean hasCdBaseRecadastroBeneficio() 

    /**
     * Method hasCdBloqueioEmissaoPplta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdBloqueioEmissaoPplta()
    {
        return this._has_cdBloqueioEmissaoPplta;
    } //-- boolean hasCdBloqueioEmissaoPplta() 

    /**
     * Method hasCdCanalAlteracao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCanalAlteracao()
    {
        return this._has_cdCanalAlteracao;
    } //-- boolean hasCdCanalAlteracao() 

    /**
     * Method hasCdCanalInclusao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCanalInclusao()
    {
        return this._has_cdCanalInclusao;
    } //-- boolean hasCdCanalInclusao() 

    /**
     * Method hasCdCapituloTituloRegistro
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCapituloTituloRegistro()
    {
        return this._has_cdCapituloTituloRegistro;
    } //-- boolean hasCdCapituloTituloRegistro() 

    /**
     * Method hasCdCobrancaTarifa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCobrancaTarifa()
    {
        return this._has_cdCobrancaTarifa;
    } //-- boolean hasCdCobrancaTarifa() 

    /**
     * Method hasCdConsDebitoVeiculo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdConsDebitoVeiculo()
    {
        return this._has_cdConsDebitoVeiculo;
    } //-- boolean hasCdConsDebitoVeiculo() 

    /**
     * Method hasCdConsEndereco
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdConsEndereco()
    {
        return this._has_cdConsEndereco;
    } //-- boolean hasCdConsEndereco() 

    /**
     * Method hasCdConsSaldoPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdConsSaldoPagamento()
    {
        return this._has_cdConsSaldoPagamento;
    } //-- boolean hasCdConsSaldoPagamento() 

    /**
     * Method hasCdConsistenciaCpfCnpjBenefAvalNpc
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdConsistenciaCpfCnpjBenefAvalNpc()
    {
        return this._has_cdConsistenciaCpfCnpjBenefAvalNpc;
    } //-- boolean hasCdConsistenciaCpfCnpjBenefAvalNpc() 

    /**
     * Method hasCdConsultaSaldoValorSuperior
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdConsultaSaldoValorSuperior()
    {
        return this._has_cdConsultaSaldoValorSuperior;
    } //-- boolean hasCdConsultaSaldoValorSuperior() 

    /**
     * Method hasCdContagemConsSaudo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdContagemConsSaudo()
    {
        return this._has_cdContagemConsSaudo;
    } //-- boolean hasCdContagemConsSaudo() 

    /**
     * Method hasCdContratoContaTransferencia
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdContratoContaTransferencia()
    {
        return this._has_cdContratoContaTransferencia;
    } //-- boolean hasCdContratoContaTransferencia() 

    /**
     * Method hasCdCreditoNaoUtilizado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCreditoNaoUtilizado()
    {
        return this._has_cdCreditoNaoUtilizado;
    } //-- boolean hasCdCreditoNaoUtilizado() 

    /**
     * Method hasCdCriterioEnquaBeneficio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCriterioEnquaBeneficio()
    {
        return this._has_cdCriterioEnquaBeneficio;
    } //-- boolean hasCdCriterioEnquaBeneficio() 

    /**
     * Method hasCdCriterioEnquaRecadastro
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCriterioEnquaRecadastro()
    {
        return this._has_cdCriterioEnquaRecadastro;
    } //-- boolean hasCdCriterioEnquaRecadastro() 

    /**
     * Method hasCdCriterioRastreabilidadeTitulo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCriterioRastreabilidadeTitulo()
    {
        return this._has_cdCriterioRastreabilidadeTitulo;
    } //-- boolean hasCdCriterioRastreabilidadeTitulo() 

    /**
     * Method hasCdCtciaEspecieBeneficio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCtciaEspecieBeneficio()
    {
        return this._has_cdCtciaEspecieBeneficio;
    } //-- boolean hasCdCtciaEspecieBeneficio() 

    /**
     * Method hasCdCtciaIdentificacaoBeneficio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCtciaIdentificacaoBeneficio()
    {
        return this._has_cdCtciaIdentificacaoBeneficio;
    } //-- boolean hasCdCtciaIdentificacaoBeneficio() 

    /**
     * Method hasCdCtciaInscricaoFavorecido
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCtciaInscricaoFavorecido()
    {
        return this._has_cdCtciaInscricaoFavorecido;
    } //-- boolean hasCdCtciaInscricaoFavorecido() 

    /**
     * Method hasCdCtciaProprietarioVeiculo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCtciaProprietarioVeiculo()
    {
        return this._has_cdCtciaProprietarioVeiculo;
    } //-- boolean hasCdCtciaProprietarioVeiculo() 

    /**
     * Method hasCdDestinoAviso
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdDestinoAviso()
    {
        return this._has_cdDestinoAviso;
    } //-- boolean hasCdDestinoAviso() 

    /**
     * Method hasCdDestinoComprovante
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdDestinoComprovante()
    {
        return this._has_cdDestinoComprovante;
    } //-- boolean hasCdDestinoComprovante() 

    /**
     * Method hasCdDestinoFormularioRecadastro
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdDestinoFormularioRecadastro()
    {
        return this._has_cdDestinoFormularioRecadastro;
    } //-- boolean hasCdDestinoFormularioRecadastro() 

    /**
     * Method hasCdDiaFloatPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdDiaFloatPagamento()
    {
        return this._has_cdDiaFloatPagamento;
    } //-- boolean hasCdDiaFloatPagamento() 

    /**
     * Method hasCdDispzContaCredito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdDispzContaCredito()
    {
        return this._has_cdDispzContaCredito;
    } //-- boolean hasCdDispzContaCredito() 

    /**
     * Method hasCdDispzDiversarCrrtt
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdDispzDiversarCrrtt()
    {
        return this._has_cdDispzDiversarCrrtt;
    } //-- boolean hasCdDispzDiversarCrrtt() 

    /**
     * Method hasCdDispzDiversasNao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdDispzDiversasNao()
    {
        return this._has_cdDispzDiversasNao;
    } //-- boolean hasCdDispzDiversasNao() 

    /**
     * Method hasCdDispzSalarioCrrtt
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdDispzSalarioCrrtt()
    {
        return this._has_cdDispzSalarioCrrtt;
    } //-- boolean hasCdDispzSalarioCrrtt() 

    /**
     * Method hasCdDispzSalarioNao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdDispzSalarioNao()
    {
        return this._has_cdDispzSalarioNao;
    } //-- boolean hasCdDispzSalarioNao() 

    /**
     * Method hasCdEnvelopeAberto
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdEnvelopeAberto()
    {
        return this._has_cdEnvelopeAberto;
    } //-- boolean hasCdEnvelopeAberto() 

    /**
     * Method hasCdExigeAutFilial
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdExigeAutFilial()
    {
        return this._has_cdExigeAutFilial;
    } //-- boolean hasCdExigeAutFilial() 

    /**
     * Method hasCdFavorecidoConsPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFavorecidoConsPagamento()
    {
        return this._has_cdFavorecidoConsPagamento;
    } //-- boolean hasCdFavorecidoConsPagamento() 

    /**
     * Method hasCdFloatServicoContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFloatServicoContrato()
    {
        return this._has_cdFloatServicoContrato;
    } //-- boolean hasCdFloatServicoContrato() 

    /**
     * Method hasCdFormaAutorizacaoPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFormaAutorizacaoPagamento()
    {
        return this._has_cdFormaAutorizacaoPagamento;
    } //-- boolean hasCdFormaAutorizacaoPagamento() 

    /**
     * Method hasCdFormaEnvioPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFormaEnvioPagamento()
    {
        return this._has_cdFormaEnvioPagamento;
    } //-- boolean hasCdFormaEnvioPagamento() 

    /**
     * Method hasCdFormaExpiracaoCredito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFormaExpiracaoCredito()
    {
        return this._has_cdFormaExpiracaoCredito;
    } //-- boolean hasCdFormaExpiracaoCredito() 

    /**
     * Method hasCdFormaManutencao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFormaManutencao()
    {
        return this._has_cdFormaManutencao;
    } //-- boolean hasCdFormaManutencao() 

    /**
     * Method hasCdFormularioContratoCliente
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFormularioContratoCliente()
    {
        return this._has_cdFormularioContratoCliente;
    } //-- boolean hasCdFormularioContratoCliente() 

    /**
     * Method hasCdFrasePreCadastro
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFrasePreCadastro()
    {
        return this._has_cdFrasePreCadastro;
    } //-- boolean hasCdFrasePreCadastro() 

    /**
     * Method hasCdIndLancamentoPersonalizado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndLancamentoPersonalizado()
    {
        return this._has_cdIndLancamentoPersonalizado;
    } //-- boolean hasCdIndLancamentoPersonalizado() 

    /**
     * Method hasCdIndicadorAdesaoSacador
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorAdesaoSacador()
    {
        return this._has_cdIndicadorAdesaoSacador;
    } //-- boolean hasCdIndicadorAdesaoSacador() 

    /**
     * Method hasCdIndicadorAgendaGrade
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorAgendaGrade()
    {
        return this._has_cdIndicadorAgendaGrade;
    } //-- boolean hasCdIndicadorAgendaGrade() 

    /**
     * Method hasCdIndicadorAgendaTitulo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorAgendaTitulo()
    {
        return this._has_cdIndicadorAgendaTitulo;
    } //-- boolean hasCdIndicadorAgendaTitulo() 

    /**
     * Method hasCdIndicadorAutorizacaoCliente
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorAutorizacaoCliente()
    {
        return this._has_cdIndicadorAutorizacaoCliente;
    } //-- boolean hasCdIndicadorAutorizacaoCliente() 

    /**
     * Method hasCdIndicadorAutorizacaoComplemento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorAutorizacaoComplemento()
    {
        return this._has_cdIndicadorAutorizacaoComplemento;
    } //-- boolean hasCdIndicadorAutorizacaoComplemento() 

    /**
     * Method hasCdIndicadorBancoPostal
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorBancoPostal()
    {
        return this._has_cdIndicadorBancoPostal;
    } //-- boolean hasCdIndicadorBancoPostal() 

    /**
     * Method hasCdIndicadorCadastroOrg
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorCadastroOrg()
    {
        return this._has_cdIndicadorCadastroOrg;
    } //-- boolean hasCdIndicadorCadastroOrg() 

    /**
     * Method hasCdIndicadorCadastroProcd
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorCadastroProcd()
    {
        return this._has_cdIndicadorCadastroProcd;
    } //-- boolean hasCdIndicadorCadastroProcd() 

    /**
     * Method hasCdIndicadorCataoSalario
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorCataoSalario()
    {
        return this._has_cdIndicadorCataoSalario;
    } //-- boolean hasCdIndicadorCataoSalario() 

    /**
     * Method hasCdIndicadorEconomicoReajuste
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorEconomicoReajuste()
    {
        return this._has_cdIndicadorEconomicoReajuste;
    } //-- boolean hasCdIndicadorEconomicoReajuste() 

    /**
     * Method hasCdIndicadorEmissaoAviso
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorEmissaoAviso()
    {
        return this._has_cdIndicadorEmissaoAviso;
    } //-- boolean hasCdIndicadorEmissaoAviso() 

    /**
     * Method hasCdIndicadorExpiracaoCredito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorExpiracaoCredito()
    {
        return this._has_cdIndicadorExpiracaoCredito;
    } //-- boolean hasCdIndicadorExpiracaoCredito() 

    /**
     * Method hasCdIndicadorFeriadoLocal
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorFeriadoLocal()
    {
        return this._has_cdIndicadorFeriadoLocal;
    } //-- boolean hasCdIndicadorFeriadoLocal() 

    /**
     * Method hasCdIndicadorLancamentoPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorLancamentoPagamento()
    {
        return this._has_cdIndicadorLancamentoPagamento;
    } //-- boolean hasCdIndicadorLancamentoPagamento() 

    /**
     * Method hasCdIndicadorListaDebito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorListaDebito()
    {
        return this._has_cdIndicadorListaDebito;
    } //-- boolean hasCdIndicadorListaDebito() 

    /**
     * Method hasCdIndicadorMensagemPerso
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorMensagemPerso()
    {
        return this._has_cdIndicadorMensagemPerso;
    } //-- boolean hasCdIndicadorMensagemPerso() 

    /**
     * Method hasCdIndicadorRetornoInternet
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorRetornoInternet()
    {
        return this._has_cdIndicadorRetornoInternet;
    } //-- boolean hasCdIndicadorRetornoInternet() 

    /**
     * Method hasCdIndicadorRetornoSeparado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorRetornoSeparado()
    {
        return this._has_cdIndicadorRetornoSeparado;
    } //-- boolean hasCdIndicadorRetornoSeparado() 

    /**
     * Method hasCdIndicadorSegundaLinha
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorSegundaLinha()
    {
        return this._has_cdIndicadorSegundaLinha;
    } //-- boolean hasCdIndicadorSegundaLinha() 

    /**
     * Method hasCdIndicadorTipoRetornoInternet
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorTipoRetornoInternet()
    {
        return this._has_cdIndicadorTipoRetornoInternet;
    } //-- boolean hasCdIndicadorTipoRetornoInternet() 

    /**
     * Method hasCdIndicadorUtilizaMora
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorUtilizaMora()
    {
        return this._has_cdIndicadorUtilizaMora;
    } //-- boolean hasCdIndicadorUtilizaMora() 

    /**
     * Method hasCdLancamentoFuturoCredito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdLancamentoFuturoCredito()
    {
        return this._has_cdLancamentoFuturoCredito;
    } //-- boolean hasCdLancamentoFuturoCredito() 

    /**
     * Method hasCdLancamentoFuturoDebito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdLancamentoFuturoDebito()
    {
        return this._has_cdLancamentoFuturoDebito;
    } //-- boolean hasCdLancamentoFuturoDebito() 

    /**
     * Method hasCdLiberacaoLoteProcesso
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdLiberacaoLoteProcesso()
    {
        return this._has_cdLiberacaoLoteProcesso;
    } //-- boolean hasCdLiberacaoLoteProcesso() 

    /**
     * Method hasCdManutencaoBaseRecadastro
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdManutencaoBaseRecadastro()
    {
        return this._has_cdManutencaoBaseRecadastro;
    } //-- boolean hasCdManutencaoBaseRecadastro() 

    /**
     * Method hasCdMeioPagamentoCredito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMeioPagamentoCredito()
    {
        return this._has_cdMeioPagamentoCredito;
    } //-- boolean hasCdMeioPagamentoCredito() 

    /**
     * Method hasCdMensagemRecadastroMidia
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMensagemRecadastroMidia()
    {
        return this._has_cdMensagemRecadastroMidia;
    } //-- boolean hasCdMensagemRecadastroMidia() 

    /**
     * Method hasCdMidiaDisponivel
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMidiaDisponivel()
    {
        return this._has_cdMidiaDisponivel;
    } //-- boolean hasCdMidiaDisponivel() 

    /**
     * Method hasCdMidiaMensagemRecadastro
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMidiaMensagemRecadastro()
    {
        return this._has_cdMidiaMensagemRecadastro;
    } //-- boolean hasCdMidiaMensagemRecadastro() 

    /**
     * Method hasCdMomentoAvisoRacadastro
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMomentoAvisoRacadastro()
    {
        return this._has_cdMomentoAvisoRacadastro;
    } //-- boolean hasCdMomentoAvisoRacadastro() 

    /**
     * Method hasCdMomentoCreditoEfetivacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMomentoCreditoEfetivacao()
    {
        return this._has_cdMomentoCreditoEfetivacao;
    } //-- boolean hasCdMomentoCreditoEfetivacao() 

    /**
     * Method hasCdMomentoDebitoPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMomentoDebitoPagamento()
    {
        return this._has_cdMomentoDebitoPagamento;
    } //-- boolean hasCdMomentoDebitoPagamento() 

    /**
     * Method hasCdMomentoFormularioRecadastro
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMomentoFormularioRecadastro()
    {
        return this._has_cdMomentoFormularioRecadastro;
    } //-- boolean hasCdMomentoFormularioRecadastro() 

    /**
     * Method hasCdMomentoProcessamentoPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMomentoProcessamentoPagamento()
    {
        return this._has_cdMomentoProcessamentoPagamento;
    } //-- boolean hasCdMomentoProcessamentoPagamento() 

    /**
     * Method hasCdNaturezaOperacaoPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdNaturezaOperacaoPagamento()
    {
        return this._has_cdNaturezaOperacaoPagamento;
    } //-- boolean hasCdNaturezaOperacaoPagamento() 

    /**
     * Method hasCdPagamentoNaoUtilizado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPagamentoNaoUtilizado()
    {
        return this._has_cdPagamentoNaoUtilizado;
    } //-- boolean hasCdPagamentoNaoUtilizado() 

    /**
     * Method hasCdPercentualMaximoInconLote
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPercentualMaximoInconLote()
    {
        return this._has_cdPercentualMaximoInconLote;
    } //-- boolean hasCdPercentualMaximoInconLote() 

    /**
     * Method hasCdPerdcCobrancaTarifa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPerdcCobrancaTarifa()
    {
        return this._has_cdPerdcCobrancaTarifa;
    } //-- boolean hasCdPerdcCobrancaTarifa() 

    /**
     * Method hasCdPerdcComprovante
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPerdcComprovante()
    {
        return this._has_cdPerdcComprovante;
    } //-- boolean hasCdPerdcComprovante() 

    /**
     * Method hasCdPerdcConsultaVeiculo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPerdcConsultaVeiculo()
    {
        return this._has_cdPerdcConsultaVeiculo;
    } //-- boolean hasCdPerdcConsultaVeiculo() 

    /**
     * Method hasCdPerdcEnvioRemessa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPerdcEnvioRemessa()
    {
        return this._has_cdPerdcEnvioRemessa;
    } //-- boolean hasCdPerdcEnvioRemessa() 

    /**
     * Method hasCdPerdcManutencaoProcd
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPerdcManutencaoProcd()
    {
        return this._has_cdPerdcManutencaoProcd;
    } //-- boolean hasCdPerdcManutencaoProcd() 

    /**
     * Method hasCdPeriodicidadeAviso
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPeriodicidadeAviso()
    {
        return this._has_cdPeriodicidadeAviso;
    } //-- boolean hasCdPeriodicidadeAviso() 

    /**
     * Method hasCdPermissaoDebitoOnline
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPermissaoDebitoOnline()
    {
        return this._has_cdPermissaoDebitoOnline;
    } //-- boolean hasCdPermissaoDebitoOnline() 

    /**
     * Method hasCdPreenchimentoLancamentoPersonalizado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPreenchimentoLancamentoPersonalizado()
    {
        return this._has_cdPreenchimentoLancamentoPersonalizado;
    } //-- boolean hasCdPreenchimentoLancamentoPersonalizado() 

    /**
     * Method hasCdPrincipalEnquaRecadastro
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPrincipalEnquaRecadastro()
    {
        return this._has_cdPrincipalEnquaRecadastro;
    } //-- boolean hasCdPrincipalEnquaRecadastro() 

    /**
     * Method hasCdPrioridadeEfetivacaoPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPrioridadeEfetivacaoPagamento()
    {
        return this._has_cdPrioridadeEfetivacaoPagamento;
    } //-- boolean hasCdPrioridadeEfetivacaoPagamento() 

    /**
     * Method hasCdRastreabilidadeNotaFiscal
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdRastreabilidadeNotaFiscal()
    {
        return this._has_cdRastreabilidadeNotaFiscal;
    } //-- boolean hasCdRastreabilidadeNotaFiscal() 

    /**
     * Method hasCdRastreabilidadeTituloTerceiro
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdRastreabilidadeTituloTerceiro()
    {
        return this._has_cdRastreabilidadeTituloTerceiro;
    } //-- boolean hasCdRastreabilidadeTituloTerceiro() 

    /**
     * Method hasCdRejeicaoAgendaLote
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdRejeicaoAgendaLote()
    {
        return this._has_cdRejeicaoAgendaLote;
    } //-- boolean hasCdRejeicaoAgendaLote() 

    /**
     * Method hasCdRejeicaoEfetivacaoLote
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdRejeicaoEfetivacaoLote()
    {
        return this._has_cdRejeicaoEfetivacaoLote;
    } //-- boolean hasCdRejeicaoEfetivacaoLote() 

    /**
     * Method hasCdRejeicaoLote
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdRejeicaoLote()
    {
        return this._has_cdRejeicaoLote;
    } //-- boolean hasCdRejeicaoLote() 

    /**
     * Method hasCdTipoCargaRecadastro
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoCargaRecadastro()
    {
        return this._has_cdTipoCargaRecadastro;
    } //-- boolean hasCdTipoCargaRecadastro() 

    /**
     * Method hasCdTipoCataoSalario
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoCataoSalario()
    {
        return this._has_cdTipoCataoSalario;
    } //-- boolean hasCdTipoCataoSalario() 

    /**
     * Method hasCdTipoConsistenciaLista
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoConsistenciaLista()
    {
        return this._has_cdTipoConsistenciaLista;
    } //-- boolean hasCdTipoConsistenciaLista() 

    /**
     * Method hasCdTipoConsultaComprovante
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoConsultaComprovante()
    {
        return this._has_cdTipoConsultaComprovante;
    } //-- boolean hasCdTipoConsultaComprovante() 

    /**
     * Method hasCdTipoContaFavorecido
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContaFavorecido()
    {
        return this._has_cdTipoContaFavorecido;
    } //-- boolean hasCdTipoContaFavorecido() 

    /**
     * Method hasCdTipoDataFloat
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoDataFloat()
    {
        return this._has_cdTipoDataFloat;
    } //-- boolean hasCdTipoDataFloat() 

    /**
     * Method hasCdTipoDivergenciaVeiculo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoDivergenciaVeiculo()
    {
        return this._has_cdTipoDivergenciaVeiculo;
    } //-- boolean hasCdTipoDivergenciaVeiculo() 

    /**
     * Method hasCdTipoFormacaoLista
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoFormacaoLista()
    {
        return this._has_cdTipoFormacaoLista;
    } //-- boolean hasCdTipoFormacaoLista() 

    /**
     * Method hasCdTipoIdBeneficio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoIdBeneficio()
    {
        return this._has_cdTipoIdBeneficio;
    } //-- boolean hasCdTipoIdBeneficio() 

    /**
     * Method hasCdTipoIsncricaoFavorecido
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoIsncricaoFavorecido()
    {
        return this._has_cdTipoIsncricaoFavorecido;
    } //-- boolean hasCdTipoIsncricaoFavorecido() 

    /**
     * Method hasCdTipoLayoutArquivo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoLayoutArquivo()
    {
        return this._has_cdTipoLayoutArquivo;
    } //-- boolean hasCdTipoLayoutArquivo() 

    /**
     * Method hasCdTipoReajusteTarifa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoReajusteTarifa()
    {
        return this._has_cdTipoReajusteTarifa;
    } //-- boolean hasCdTipoReajusteTarifa() 

    /**
     * Method hasCdTituloDdaRetorno
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTituloDdaRetorno()
    {
        return this._has_cdTituloDdaRetorno;
    } //-- boolean hasCdTituloDdaRetorno() 

    /**
     * Method hasCdUtilizacaoFavorecidoControle
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdUtilizacaoFavorecidoControle()
    {
        return this._has_cdUtilizacaoFavorecidoControle;
    } //-- boolean hasCdUtilizacaoFavorecidoControle() 

    /**
     * Method hasCdValidacaoNomeFavorecido
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdValidacaoNomeFavorecido()
    {
        return this._has_cdValidacaoNomeFavorecido;
    } //-- boolean hasCdValidacaoNomeFavorecido() 

    /**
     * Method hasDsOrigemIndicador
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasDsOrigemIndicador()
    {
        return this._has_dsOrigemIndicador;
    } //-- boolean hasDsOrigemIndicador() 

    /**
     * Method hasNrFechamentoApuracaoTarifa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrFechamentoApuracaoTarifa()
    {
        return this._has_nrFechamentoApuracaoTarifa;
    } //-- boolean hasNrFechamentoApuracaoTarifa() 

    /**
     * Method hasQtAntecedencia
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtAntecedencia()
    {
        return this._has_qtAntecedencia;
    } //-- boolean hasQtAntecedencia() 

    /**
     * Method hasQtAnteriorVencimentoComprovado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtAnteriorVencimentoComprovado()
    {
        return this._has_qtAnteriorVencimentoComprovado;
    } //-- boolean hasQtAnteriorVencimentoComprovado() 

    /**
     * Method hasQtDiaCobrancaTarifa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtDiaCobrancaTarifa()
    {
        return this._has_qtDiaCobrancaTarifa;
    } //-- boolean hasQtDiaCobrancaTarifa() 

    /**
     * Method hasQtDiaExpiracao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtDiaExpiracao()
    {
        return this._has_qtDiaExpiracao;
    } //-- boolean hasQtDiaExpiracao() 

    /**
     * Method hasQtDiaInatividadeFavorecido
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtDiaInatividadeFavorecido()
    {
        return this._has_qtDiaInatividadeFavorecido;
    } //-- boolean hasQtDiaInatividadeFavorecido() 

    /**
     * Method hasQtDiaRepiqConsulta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtDiaRepiqConsulta()
    {
        return this._has_qtDiaRepiqConsulta;
    } //-- boolean hasQtDiaRepiqConsulta() 

    /**
     * Method hasQtDiaUtilPgto
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtDiaUtilPgto()
    {
        return this._has_qtDiaUtilPgto;
    } //-- boolean hasQtDiaUtilPgto() 

    /**
     * Method hasQtEtapasRecadastroBeneficio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtEtapasRecadastroBeneficio()
    {
        return this._has_qtEtapasRecadastroBeneficio;
    } //-- boolean hasQtEtapasRecadastroBeneficio() 

    /**
     * Method hasQtFaseRecadastroBeneficio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtFaseRecadastroBeneficio()
    {
        return this._has_qtFaseRecadastroBeneficio;
    } //-- boolean hasQtFaseRecadastroBeneficio() 

    /**
     * Method hasQtLimiteLinha
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtLimiteLinha()
    {
        return this._has_qtLimiteLinha;
    } //-- boolean hasQtLimiteLinha() 

    /**
     * Method hasQtLimiteSolicitacaoCatao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtLimiteSolicitacaoCatao()
    {
        return this._has_qtLimiteSolicitacaoCatao;
    } //-- boolean hasQtLimiteSolicitacaoCatao() 

    /**
     * Method hasQtMaximaInconLote
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtMaximaInconLote()
    {
        return this._has_qtMaximaInconLote;
    } //-- boolean hasQtMaximaInconLote() 

    /**
     * Method hasQtMaximaTituloVencido
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtMaximaTituloVencido()
    {
        return this._has_qtMaximaTituloVencido;
    } //-- boolean hasQtMaximaTituloVencido() 

    /**
     * Method hasQtMesComprovante
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtMesComprovante()
    {
        return this._has_qtMesComprovante;
    } //-- boolean hasQtMesComprovante() 

    /**
     * Method hasQtMesEtapaRecadastro
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtMesEtapaRecadastro()
    {
        return this._has_qtMesEtapaRecadastro;
    } //-- boolean hasQtMesEtapaRecadastro() 

    /**
     * Method hasQtMesFaseRecadastro
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtMesFaseRecadastro()
    {
        return this._has_qtMesFaseRecadastro;
    } //-- boolean hasQtMesFaseRecadastro() 

    /**
     * Method hasQtMesReajusteTarifa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtMesReajusteTarifa()
    {
        return this._has_qtMesReajusteTarifa;
    } //-- boolean hasQtMesReajusteTarifa() 

    /**
     * Method hasQtViaAviso
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtViaAviso()
    {
        return this._has_qtViaAviso;
    } //-- boolean hasQtViaAviso() 

    /**
     * Method hasQtViaCobranca
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtViaCobranca()
    {
        return this._has_qtViaCobranca;
    } //-- boolean hasQtViaCobranca() 

    /**
     * Method hasQtViaComprovante
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtViaComprovante()
    {
        return this._has_qtViaComprovante;
    } //-- boolean hasQtViaComprovante() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdAcaoNaoVida'.
     * 
     * @param cdAcaoNaoVida the value of field 'cdAcaoNaoVida'.
     */
    public void setCdAcaoNaoVida(int cdAcaoNaoVida)
    {
        this._cdAcaoNaoVida = cdAcaoNaoVida;
        this._has_cdAcaoNaoVida = true;
    } //-- void setCdAcaoNaoVida(int) 

    /**
     * Sets the value of field 'cdAcertoDadoRecadastro'.
     * 
     * @param cdAcertoDadoRecadastro the value of field
     * 'cdAcertoDadoRecadastro'.
     */
    public void setCdAcertoDadoRecadastro(int cdAcertoDadoRecadastro)
    {
        this._cdAcertoDadoRecadastro = cdAcertoDadoRecadastro;
        this._has_cdAcertoDadoRecadastro = true;
    } //-- void setCdAcertoDadoRecadastro(int) 

    /**
     * Sets the value of field 'cdAgendaDebitoVeiculo'.
     * 
     * @param cdAgendaDebitoVeiculo the value of field
     * 'cdAgendaDebitoVeiculo'.
     */
    public void setCdAgendaDebitoVeiculo(int cdAgendaDebitoVeiculo)
    {
        this._cdAgendaDebitoVeiculo = cdAgendaDebitoVeiculo;
        this._has_cdAgendaDebitoVeiculo = true;
    } //-- void setCdAgendaDebitoVeiculo(int) 

    /**
     * Sets the value of field 'cdAgendaPagamentoVencido'.
     * 
     * @param cdAgendaPagamentoVencido the value of field
     * 'cdAgendaPagamentoVencido'.
     */
    public void setCdAgendaPagamentoVencido(int cdAgendaPagamentoVencido)
    {
        this._cdAgendaPagamentoVencido = cdAgendaPagamentoVencido;
        this._has_cdAgendaPagamentoVencido = true;
    } //-- void setCdAgendaPagamentoVencido(int) 

    /**
     * Sets the value of field 'cdAgendaRastreabilidadeFilial'.
     * 
     * @param cdAgendaRastreabilidadeFilial the value of field
     * 'cdAgendaRastreabilidadeFilial'.
     */
    public void setCdAgendaRastreabilidadeFilial(int cdAgendaRastreabilidadeFilial)
    {
        this._cdAgendaRastreabilidadeFilial = cdAgendaRastreabilidadeFilial;
        this._has_cdAgendaRastreabilidadeFilial = true;
    } //-- void setCdAgendaRastreabilidadeFilial(int) 

    /**
     * Sets the value of field 'cdAgendaValorMenor'.
     * 
     * @param cdAgendaValorMenor the value of field
     * 'cdAgendaValorMenor'.
     */
    public void setCdAgendaValorMenor(int cdAgendaValorMenor)
    {
        this._cdAgendaValorMenor = cdAgendaValorMenor;
        this._has_cdAgendaValorMenor = true;
    } //-- void setCdAgendaValorMenor(int) 

    /**
     * Sets the value of field 'cdAgrupamentoAviso'.
     * 
     * @param cdAgrupamentoAviso the value of field
     * 'cdAgrupamentoAviso'.
     */
    public void setCdAgrupamentoAviso(int cdAgrupamentoAviso)
    {
        this._cdAgrupamentoAviso = cdAgrupamentoAviso;
        this._has_cdAgrupamentoAviso = true;
    } //-- void setCdAgrupamentoAviso(int) 

    /**
     * Sets the value of field 'cdAgrupamentoComprovado'.
     * 
     * @param cdAgrupamentoComprovado the value of field
     * 'cdAgrupamentoComprovado'.
     */
    public void setCdAgrupamentoComprovado(int cdAgrupamentoComprovado)
    {
        this._cdAgrupamentoComprovado = cdAgrupamentoComprovado;
        this._has_cdAgrupamentoComprovado = true;
    } //-- void setCdAgrupamentoComprovado(int) 

    /**
     * Sets the value of field 'cdAgrupamentoFormularioRecadastro'.
     * 
     * @param cdAgrupamentoFormularioRecadastro the value of field
     * 'cdAgrupamentoFormularioRecadastro'.
     */
    public void setCdAgrupamentoFormularioRecadastro(int cdAgrupamentoFormularioRecadastro)
    {
        this._cdAgrupamentoFormularioRecadastro = cdAgrupamentoFormularioRecadastro;
        this._has_cdAgrupamentoFormularioRecadastro = true;
    } //-- void setCdAgrupamentoFormularioRecadastro(int) 

    /**
     * Sets the value of field 'cdAmbienteServicoContrato'.
     * 
     * @param cdAmbienteServicoContrato the value of field
     * 'cdAmbienteServicoContrato'.
     */
    public void setCdAmbienteServicoContrato(java.lang.String cdAmbienteServicoContrato)
    {
        this._cdAmbienteServicoContrato = cdAmbienteServicoContrato;
    } //-- void setCdAmbienteServicoContrato(java.lang.String) 

    /**
     * Sets the value of field 'cdAntecRecadastroBeneficio'.
     * 
     * @param cdAntecRecadastroBeneficio the value of field
     * 'cdAntecRecadastroBeneficio'.
     */
    public void setCdAntecRecadastroBeneficio(int cdAntecRecadastroBeneficio)
    {
        this._cdAntecRecadastroBeneficio = cdAntecRecadastroBeneficio;
        this._has_cdAntecRecadastroBeneficio = true;
    } //-- void setCdAntecRecadastroBeneficio(int) 

    /**
     * Sets the value of field 'cdAreaReservada'.
     * 
     * @param cdAreaReservada the value of field 'cdAreaReservada'.
     */
    public void setCdAreaReservada(int cdAreaReservada)
    {
        this._cdAreaReservada = cdAreaReservada;
        this._has_cdAreaReservada = true;
    } //-- void setCdAreaReservada(int) 

    /**
     * Sets the value of field 'cdBaseRecadastroBeneficio'.
     * 
     * @param cdBaseRecadastroBeneficio the value of field
     * 'cdBaseRecadastroBeneficio'.
     */
    public void setCdBaseRecadastroBeneficio(int cdBaseRecadastroBeneficio)
    {
        this._cdBaseRecadastroBeneficio = cdBaseRecadastroBeneficio;
        this._has_cdBaseRecadastroBeneficio = true;
    } //-- void setCdBaseRecadastroBeneficio(int) 

    /**
     * Sets the value of field 'cdBloqueioEmissaoPplta'.
     * 
     * @param cdBloqueioEmissaoPplta the value of field
     * 'cdBloqueioEmissaoPplta'.
     */
    public void setCdBloqueioEmissaoPplta(int cdBloqueioEmissaoPplta)
    {
        this._cdBloqueioEmissaoPplta = cdBloqueioEmissaoPplta;
        this._has_cdBloqueioEmissaoPplta = true;
    } //-- void setCdBloqueioEmissaoPplta(int) 

    /**
     * Sets the value of field 'cdCanalAlteracao'.
     * 
     * @param cdCanalAlteracao the value of field 'cdCanalAlteracao'
     */
    public void setCdCanalAlteracao(int cdCanalAlteracao)
    {
        this._cdCanalAlteracao = cdCanalAlteracao;
        this._has_cdCanalAlteracao = true;
    } //-- void setCdCanalAlteracao(int) 

    /**
     * Sets the value of field 'cdCanalInclusao'.
     * 
     * @param cdCanalInclusao the value of field 'cdCanalInclusao'.
     */
    public void setCdCanalInclusao(int cdCanalInclusao)
    {
        this._cdCanalInclusao = cdCanalInclusao;
        this._has_cdCanalInclusao = true;
    } //-- void setCdCanalInclusao(int) 

    /**
     * Sets the value of field 'cdCapituloTituloRegistro'.
     * 
     * @param cdCapituloTituloRegistro the value of field
     * 'cdCapituloTituloRegistro'.
     */
    public void setCdCapituloTituloRegistro(int cdCapituloTituloRegistro)
    {
        this._cdCapituloTituloRegistro = cdCapituloTituloRegistro;
        this._has_cdCapituloTituloRegistro = true;
    } //-- void setCdCapituloTituloRegistro(int) 

    /**
     * Sets the value of field 'cdCobrancaTarifa'.
     * 
     * @param cdCobrancaTarifa the value of field 'cdCobrancaTarifa'
     */
    public void setCdCobrancaTarifa(int cdCobrancaTarifa)
    {
        this._cdCobrancaTarifa = cdCobrancaTarifa;
        this._has_cdCobrancaTarifa = true;
    } //-- void setCdCobrancaTarifa(int) 

    /**
     * Sets the value of field 'cdConsDebitoVeiculo'.
     * 
     * @param cdConsDebitoVeiculo the value of field
     * 'cdConsDebitoVeiculo'.
     */
    public void setCdConsDebitoVeiculo(int cdConsDebitoVeiculo)
    {
        this._cdConsDebitoVeiculo = cdConsDebitoVeiculo;
        this._has_cdConsDebitoVeiculo = true;
    } //-- void setCdConsDebitoVeiculo(int) 

    /**
     * Sets the value of field 'cdConsEndereco'.
     * 
     * @param cdConsEndereco the value of field 'cdConsEndereco'.
     */
    public void setCdConsEndereco(int cdConsEndereco)
    {
        this._cdConsEndereco = cdConsEndereco;
        this._has_cdConsEndereco = true;
    } //-- void setCdConsEndereco(int) 

    /**
     * Sets the value of field 'cdConsSaldoPagamento'.
     * 
     * @param cdConsSaldoPagamento the value of field
     * 'cdConsSaldoPagamento'.
     */
    public void setCdConsSaldoPagamento(int cdConsSaldoPagamento)
    {
        this._cdConsSaldoPagamento = cdConsSaldoPagamento;
        this._has_cdConsSaldoPagamento = true;
    } //-- void setCdConsSaldoPagamento(int) 

    /**
     * Sets the value of field 'cdConsistenciaCpfCnpjBenefAvalNpc'.
     * 
     * @param cdConsistenciaCpfCnpjBenefAvalNpc the value of field
     * 'cdConsistenciaCpfCnpjBenefAvalNpc'.
     */
    public void setCdConsistenciaCpfCnpjBenefAvalNpc(int cdConsistenciaCpfCnpjBenefAvalNpc)
    {
        this._cdConsistenciaCpfCnpjBenefAvalNpc = cdConsistenciaCpfCnpjBenefAvalNpc;
        this._has_cdConsistenciaCpfCnpjBenefAvalNpc = true;
    } //-- void setCdConsistenciaCpfCnpjBenefAvalNpc(int) 

    /**
     * Sets the value of field 'cdConsultaSaldoValorSuperior'.
     * 
     * @param cdConsultaSaldoValorSuperior the value of field
     * 'cdConsultaSaldoValorSuperior'.
     */
    public void setCdConsultaSaldoValorSuperior(int cdConsultaSaldoValorSuperior)
    {
        this._cdConsultaSaldoValorSuperior = cdConsultaSaldoValorSuperior;
        this._has_cdConsultaSaldoValorSuperior = true;
    } //-- void setCdConsultaSaldoValorSuperior(int) 

    /**
     * Sets the value of field 'cdContagemConsSaudo'.
     * 
     * @param cdContagemConsSaudo the value of field
     * 'cdContagemConsSaudo'.
     */
    public void setCdContagemConsSaudo(int cdContagemConsSaudo)
    {
        this._cdContagemConsSaudo = cdContagemConsSaudo;
        this._has_cdContagemConsSaudo = true;
    } //-- void setCdContagemConsSaudo(int) 

    /**
     * Sets the value of field 'cdContratoContaTransferencia'.
     * 
     * @param cdContratoContaTransferencia the value of field
     * 'cdContratoContaTransferencia'.
     */
    public void setCdContratoContaTransferencia(int cdContratoContaTransferencia)
    {
        this._cdContratoContaTransferencia = cdContratoContaTransferencia;
        this._has_cdContratoContaTransferencia = true;
    } //-- void setCdContratoContaTransferencia(int) 

    /**
     * Sets the value of field 'cdCreditoNaoUtilizado'.
     * 
     * @param cdCreditoNaoUtilizado the value of field
     * 'cdCreditoNaoUtilizado'.
     */
    public void setCdCreditoNaoUtilizado(int cdCreditoNaoUtilizado)
    {
        this._cdCreditoNaoUtilizado = cdCreditoNaoUtilizado;
        this._has_cdCreditoNaoUtilizado = true;
    } //-- void setCdCreditoNaoUtilizado(int) 

    /**
     * Sets the value of field 'cdCriterioEnquaBeneficio'.
     * 
     * @param cdCriterioEnquaBeneficio the value of field
     * 'cdCriterioEnquaBeneficio'.
     */
    public void setCdCriterioEnquaBeneficio(int cdCriterioEnquaBeneficio)
    {
        this._cdCriterioEnquaBeneficio = cdCriterioEnquaBeneficio;
        this._has_cdCriterioEnquaBeneficio = true;
    } //-- void setCdCriterioEnquaBeneficio(int) 

    /**
     * Sets the value of field 'cdCriterioEnquaRecadastro'.
     * 
     * @param cdCriterioEnquaRecadastro the value of field
     * 'cdCriterioEnquaRecadastro'.
     */
    public void setCdCriterioEnquaRecadastro(int cdCriterioEnquaRecadastro)
    {
        this._cdCriterioEnquaRecadastro = cdCriterioEnquaRecadastro;
        this._has_cdCriterioEnquaRecadastro = true;
    } //-- void setCdCriterioEnquaRecadastro(int) 

    /**
     * Sets the value of field 'cdCriterioRastreabilidadeTitulo'.
     * 
     * @param cdCriterioRastreabilidadeTitulo the value of field
     * 'cdCriterioRastreabilidadeTitulo'.
     */
    public void setCdCriterioRastreabilidadeTitulo(int cdCriterioRastreabilidadeTitulo)
    {
        this._cdCriterioRastreabilidadeTitulo = cdCriterioRastreabilidadeTitulo;
        this._has_cdCriterioRastreabilidadeTitulo = true;
    } //-- void setCdCriterioRastreabilidadeTitulo(int) 

    /**
     * Sets the value of field 'cdCtciaEspecieBeneficio'.
     * 
     * @param cdCtciaEspecieBeneficio the value of field
     * 'cdCtciaEspecieBeneficio'.
     */
    public void setCdCtciaEspecieBeneficio(int cdCtciaEspecieBeneficio)
    {
        this._cdCtciaEspecieBeneficio = cdCtciaEspecieBeneficio;
        this._has_cdCtciaEspecieBeneficio = true;
    } //-- void setCdCtciaEspecieBeneficio(int) 

    /**
     * Sets the value of field 'cdCtciaIdentificacaoBeneficio'.
     * 
     * @param cdCtciaIdentificacaoBeneficio the value of field
     * 'cdCtciaIdentificacaoBeneficio'.
     */
    public void setCdCtciaIdentificacaoBeneficio(int cdCtciaIdentificacaoBeneficio)
    {
        this._cdCtciaIdentificacaoBeneficio = cdCtciaIdentificacaoBeneficio;
        this._has_cdCtciaIdentificacaoBeneficio = true;
    } //-- void setCdCtciaIdentificacaoBeneficio(int) 

    /**
     * Sets the value of field 'cdCtciaInscricaoFavorecido'.
     * 
     * @param cdCtciaInscricaoFavorecido the value of field
     * 'cdCtciaInscricaoFavorecido'.
     */
    public void setCdCtciaInscricaoFavorecido(int cdCtciaInscricaoFavorecido)
    {
        this._cdCtciaInscricaoFavorecido = cdCtciaInscricaoFavorecido;
        this._has_cdCtciaInscricaoFavorecido = true;
    } //-- void setCdCtciaInscricaoFavorecido(int) 

    /**
     * Sets the value of field 'cdCtciaProprietarioVeiculo'.
     * 
     * @param cdCtciaProprietarioVeiculo the value of field
     * 'cdCtciaProprietarioVeiculo'.
     */
    public void setCdCtciaProprietarioVeiculo(int cdCtciaProprietarioVeiculo)
    {
        this._cdCtciaProprietarioVeiculo = cdCtciaProprietarioVeiculo;
        this._has_cdCtciaProprietarioVeiculo = true;
    } //-- void setCdCtciaProprietarioVeiculo(int) 

    /**
     * Sets the value of field 'cdDestinoAviso'.
     * 
     * @param cdDestinoAviso the value of field 'cdDestinoAviso'.
     */
    public void setCdDestinoAviso(int cdDestinoAviso)
    {
        this._cdDestinoAviso = cdDestinoAviso;
        this._has_cdDestinoAviso = true;
    } //-- void setCdDestinoAviso(int) 

    /**
     * Sets the value of field 'cdDestinoComprovante'.
     * 
     * @param cdDestinoComprovante the value of field
     * 'cdDestinoComprovante'.
     */
    public void setCdDestinoComprovante(int cdDestinoComprovante)
    {
        this._cdDestinoComprovante = cdDestinoComprovante;
        this._has_cdDestinoComprovante = true;
    } //-- void setCdDestinoComprovante(int) 

    /**
     * Sets the value of field 'cdDestinoFormularioRecadastro'.
     * 
     * @param cdDestinoFormularioRecadastro the value of field
     * 'cdDestinoFormularioRecadastro'.
     */
    public void setCdDestinoFormularioRecadastro(int cdDestinoFormularioRecadastro)
    {
        this._cdDestinoFormularioRecadastro = cdDestinoFormularioRecadastro;
        this._has_cdDestinoFormularioRecadastro = true;
    } //-- void setCdDestinoFormularioRecadastro(int) 

    /**
     * Sets the value of field 'cdDiaFloatPagamento'.
     * 
     * @param cdDiaFloatPagamento the value of field
     * 'cdDiaFloatPagamento'.
     */
    public void setCdDiaFloatPagamento(int cdDiaFloatPagamento)
    {
        this._cdDiaFloatPagamento = cdDiaFloatPagamento;
        this._has_cdDiaFloatPagamento = true;
    } //-- void setCdDiaFloatPagamento(int) 

    /**
     * Sets the value of field 'cdDispzContaCredito'.
     * 
     * @param cdDispzContaCredito the value of field
     * 'cdDispzContaCredito'.
     */
    public void setCdDispzContaCredito(int cdDispzContaCredito)
    {
        this._cdDispzContaCredito = cdDispzContaCredito;
        this._has_cdDispzContaCredito = true;
    } //-- void setCdDispzContaCredito(int) 

    /**
     * Sets the value of field 'cdDispzDiversarCrrtt'.
     * 
     * @param cdDispzDiversarCrrtt the value of field
     * 'cdDispzDiversarCrrtt'.
     */
    public void setCdDispzDiversarCrrtt(int cdDispzDiversarCrrtt)
    {
        this._cdDispzDiversarCrrtt = cdDispzDiversarCrrtt;
        this._has_cdDispzDiversarCrrtt = true;
    } //-- void setCdDispzDiversarCrrtt(int) 

    /**
     * Sets the value of field 'cdDispzDiversasNao'.
     * 
     * @param cdDispzDiversasNao the value of field
     * 'cdDispzDiversasNao'.
     */
    public void setCdDispzDiversasNao(int cdDispzDiversasNao)
    {
        this._cdDispzDiversasNao = cdDispzDiversasNao;
        this._has_cdDispzDiversasNao = true;
    } //-- void setCdDispzDiversasNao(int) 

    /**
     * Sets the value of field 'cdDispzSalarioCrrtt'.
     * 
     * @param cdDispzSalarioCrrtt the value of field
     * 'cdDispzSalarioCrrtt'.
     */
    public void setCdDispzSalarioCrrtt(int cdDispzSalarioCrrtt)
    {
        this._cdDispzSalarioCrrtt = cdDispzSalarioCrrtt;
        this._has_cdDispzSalarioCrrtt = true;
    } //-- void setCdDispzSalarioCrrtt(int) 

    /**
     * Sets the value of field 'cdDispzSalarioNao'.
     * 
     * @param cdDispzSalarioNao the value of field
     * 'cdDispzSalarioNao'.
     */
    public void setCdDispzSalarioNao(int cdDispzSalarioNao)
    {
        this._cdDispzSalarioNao = cdDispzSalarioNao;
        this._has_cdDispzSalarioNao = true;
    } //-- void setCdDispzSalarioNao(int) 

    /**
     * Sets the value of field 'cdEnvelopeAberto'.
     * 
     * @param cdEnvelopeAberto the value of field 'cdEnvelopeAberto'
     */
    public void setCdEnvelopeAberto(int cdEnvelopeAberto)
    {
        this._cdEnvelopeAberto = cdEnvelopeAberto;
        this._has_cdEnvelopeAberto = true;
    } //-- void setCdEnvelopeAberto(int) 

    /**
     * Sets the value of field 'cdExigeAutFilial'.
     * 
     * @param cdExigeAutFilial the value of field 'cdExigeAutFilial'
     */
    public void setCdExigeAutFilial(int cdExigeAutFilial)
    {
        this._cdExigeAutFilial = cdExigeAutFilial;
        this._has_cdExigeAutFilial = true;
    } //-- void setCdExigeAutFilial(int) 

    /**
     * Sets the value of field 'cdFavorecidoConsPagamento'.
     * 
     * @param cdFavorecidoConsPagamento the value of field
     * 'cdFavorecidoConsPagamento'.
     */
    public void setCdFavorecidoConsPagamento(int cdFavorecidoConsPagamento)
    {
        this._cdFavorecidoConsPagamento = cdFavorecidoConsPagamento;
        this._has_cdFavorecidoConsPagamento = true;
    } //-- void setCdFavorecidoConsPagamento(int) 

    /**
     * Sets the value of field 'cdFloatServicoContrato'.
     * 
     * @param cdFloatServicoContrato the value of field
     * 'cdFloatServicoContrato'.
     */
    public void setCdFloatServicoContrato(int cdFloatServicoContrato)
    {
        this._cdFloatServicoContrato = cdFloatServicoContrato;
        this._has_cdFloatServicoContrato = true;
    } //-- void setCdFloatServicoContrato(int) 

    /**
     * Sets the value of field 'cdFormaAutorizacaoPagamento'.
     * 
     * @param cdFormaAutorizacaoPagamento the value of field
     * 'cdFormaAutorizacaoPagamento'.
     */
    public void setCdFormaAutorizacaoPagamento(int cdFormaAutorizacaoPagamento)
    {
        this._cdFormaAutorizacaoPagamento = cdFormaAutorizacaoPagamento;
        this._has_cdFormaAutorizacaoPagamento = true;
    } //-- void setCdFormaAutorizacaoPagamento(int) 

    /**
     * Sets the value of field 'cdFormaEnvioPagamento'.
     * 
     * @param cdFormaEnvioPagamento the value of field
     * 'cdFormaEnvioPagamento'.
     */
    public void setCdFormaEnvioPagamento(int cdFormaEnvioPagamento)
    {
        this._cdFormaEnvioPagamento = cdFormaEnvioPagamento;
        this._has_cdFormaEnvioPagamento = true;
    } //-- void setCdFormaEnvioPagamento(int) 

    /**
     * Sets the value of field 'cdFormaExpiracaoCredito'.
     * 
     * @param cdFormaExpiracaoCredito the value of field
     * 'cdFormaExpiracaoCredito'.
     */
    public void setCdFormaExpiracaoCredito(int cdFormaExpiracaoCredito)
    {
        this._cdFormaExpiracaoCredito = cdFormaExpiracaoCredito;
        this._has_cdFormaExpiracaoCredito = true;
    } //-- void setCdFormaExpiracaoCredito(int) 

    /**
     * Sets the value of field 'cdFormaManutencao'.
     * 
     * @param cdFormaManutencao the value of field
     * 'cdFormaManutencao'.
     */
    public void setCdFormaManutencao(int cdFormaManutencao)
    {
        this._cdFormaManutencao = cdFormaManutencao;
        this._has_cdFormaManutencao = true;
    } //-- void setCdFormaManutencao(int) 

    /**
     * Sets the value of field 'cdFormularioContratoCliente'.
     * 
     * @param cdFormularioContratoCliente the value of field
     * 'cdFormularioContratoCliente'.
     */
    public void setCdFormularioContratoCliente(int cdFormularioContratoCliente)
    {
        this._cdFormularioContratoCliente = cdFormularioContratoCliente;
        this._has_cdFormularioContratoCliente = true;
    } //-- void setCdFormularioContratoCliente(int) 

    /**
     * Sets the value of field 'cdFrasePreCadastro'.
     * 
     * @param cdFrasePreCadastro the value of field
     * 'cdFrasePreCadastro'.
     */
    public void setCdFrasePreCadastro(int cdFrasePreCadastro)
    {
        this._cdFrasePreCadastro = cdFrasePreCadastro;
        this._has_cdFrasePreCadastro = true;
    } //-- void setCdFrasePreCadastro(int) 

    /**
     * Sets the value of field 'cdIndLancamentoPersonalizado'.
     * 
     * @param cdIndLancamentoPersonalizado the value of field
     * 'cdIndLancamentoPersonalizado'.
     */
    public void setCdIndLancamentoPersonalizado(int cdIndLancamentoPersonalizado)
    {
        this._cdIndLancamentoPersonalizado = cdIndLancamentoPersonalizado;
        this._has_cdIndLancamentoPersonalizado = true;
    } //-- void setCdIndLancamentoPersonalizado(int) 

    /**
     * Sets the value of field 'cdIndicadorAdesaoSacador'.
     * 
     * @param cdIndicadorAdesaoSacador the value of field
     * 'cdIndicadorAdesaoSacador'.
     */
    public void setCdIndicadorAdesaoSacador(int cdIndicadorAdesaoSacador)
    {
        this._cdIndicadorAdesaoSacador = cdIndicadorAdesaoSacador;
        this._has_cdIndicadorAdesaoSacador = true;
    } //-- void setCdIndicadorAdesaoSacador(int) 

    /**
     * Sets the value of field 'cdIndicadorAgendaGrade'.
     * 
     * @param cdIndicadorAgendaGrade the value of field
     * 'cdIndicadorAgendaGrade'.
     */
    public void setCdIndicadorAgendaGrade(int cdIndicadorAgendaGrade)
    {
        this._cdIndicadorAgendaGrade = cdIndicadorAgendaGrade;
        this._has_cdIndicadorAgendaGrade = true;
    } //-- void setCdIndicadorAgendaGrade(int) 

    /**
     * Sets the value of field 'cdIndicadorAgendaTitulo'.
     * 
     * @param cdIndicadorAgendaTitulo the value of field
     * 'cdIndicadorAgendaTitulo'.
     */
    public void setCdIndicadorAgendaTitulo(int cdIndicadorAgendaTitulo)
    {
        this._cdIndicadorAgendaTitulo = cdIndicadorAgendaTitulo;
        this._has_cdIndicadorAgendaTitulo = true;
    } //-- void setCdIndicadorAgendaTitulo(int) 

    /**
     * Sets the value of field 'cdIndicadorAutorizacaoCliente'.
     * 
     * @param cdIndicadorAutorizacaoCliente the value of field
     * 'cdIndicadorAutorizacaoCliente'.
     */
    public void setCdIndicadorAutorizacaoCliente(int cdIndicadorAutorizacaoCliente)
    {
        this._cdIndicadorAutorizacaoCliente = cdIndicadorAutorizacaoCliente;
        this._has_cdIndicadorAutorizacaoCliente = true;
    } //-- void setCdIndicadorAutorizacaoCliente(int) 

    /**
     * Sets the value of field 'cdIndicadorAutorizacaoComplemento'.
     * 
     * @param cdIndicadorAutorizacaoComplemento the value of field
     * 'cdIndicadorAutorizacaoComplemento'.
     */
    public void setCdIndicadorAutorizacaoComplemento(int cdIndicadorAutorizacaoComplemento)
    {
        this._cdIndicadorAutorizacaoComplemento = cdIndicadorAutorizacaoComplemento;
        this._has_cdIndicadorAutorizacaoComplemento = true;
    } //-- void setCdIndicadorAutorizacaoComplemento(int) 

    /**
     * Sets the value of field 'cdIndicadorBancoPostal'.
     * 
     * @param cdIndicadorBancoPostal the value of field
     * 'cdIndicadorBancoPostal'.
     */
    public void setCdIndicadorBancoPostal(int cdIndicadorBancoPostal)
    {
        this._cdIndicadorBancoPostal = cdIndicadorBancoPostal;
        this._has_cdIndicadorBancoPostal = true;
    } //-- void setCdIndicadorBancoPostal(int) 

    /**
     * Sets the value of field 'cdIndicadorCadastroOrg'.
     * 
     * @param cdIndicadorCadastroOrg the value of field
     * 'cdIndicadorCadastroOrg'.
     */
    public void setCdIndicadorCadastroOrg(int cdIndicadorCadastroOrg)
    {
        this._cdIndicadorCadastroOrg = cdIndicadorCadastroOrg;
        this._has_cdIndicadorCadastroOrg = true;
    } //-- void setCdIndicadorCadastroOrg(int) 

    /**
     * Sets the value of field 'cdIndicadorCadastroProcd'.
     * 
     * @param cdIndicadorCadastroProcd the value of field
     * 'cdIndicadorCadastroProcd'.
     */
    public void setCdIndicadorCadastroProcd(int cdIndicadorCadastroProcd)
    {
        this._cdIndicadorCadastroProcd = cdIndicadorCadastroProcd;
        this._has_cdIndicadorCadastroProcd = true;
    } //-- void setCdIndicadorCadastroProcd(int) 

    /**
     * Sets the value of field 'cdIndicadorCataoSalario'.
     * 
     * @param cdIndicadorCataoSalario the value of field
     * 'cdIndicadorCataoSalario'.
     */
    public void setCdIndicadorCataoSalario(int cdIndicadorCataoSalario)
    {
        this._cdIndicadorCataoSalario = cdIndicadorCataoSalario;
        this._has_cdIndicadorCataoSalario = true;
    } //-- void setCdIndicadorCataoSalario(int) 

    /**
     * Sets the value of field 'cdIndicadorEconomicoReajuste'.
     * 
     * @param cdIndicadorEconomicoReajuste the value of field
     * 'cdIndicadorEconomicoReajuste'.
     */
    public void setCdIndicadorEconomicoReajuste(int cdIndicadorEconomicoReajuste)
    {
        this._cdIndicadorEconomicoReajuste = cdIndicadorEconomicoReajuste;
        this._has_cdIndicadorEconomicoReajuste = true;
    } //-- void setCdIndicadorEconomicoReajuste(int) 

    /**
     * Sets the value of field 'cdIndicadorEmissaoAviso'.
     * 
     * @param cdIndicadorEmissaoAviso the value of field
     * 'cdIndicadorEmissaoAviso'.
     */
    public void setCdIndicadorEmissaoAviso(int cdIndicadorEmissaoAviso)
    {
        this._cdIndicadorEmissaoAviso = cdIndicadorEmissaoAviso;
        this._has_cdIndicadorEmissaoAviso = true;
    } //-- void setCdIndicadorEmissaoAviso(int) 

    /**
     * Sets the value of field 'cdIndicadorExpiracaoCredito'.
     * 
     * @param cdIndicadorExpiracaoCredito the value of field
     * 'cdIndicadorExpiracaoCredito'.
     */
    public void setCdIndicadorExpiracaoCredito(int cdIndicadorExpiracaoCredito)
    {
        this._cdIndicadorExpiracaoCredito = cdIndicadorExpiracaoCredito;
        this._has_cdIndicadorExpiracaoCredito = true;
    } //-- void setCdIndicadorExpiracaoCredito(int) 

    /**
     * Sets the value of field 'cdIndicadorFeriadoLocal'.
     * 
     * @param cdIndicadorFeriadoLocal the value of field
     * 'cdIndicadorFeriadoLocal'.
     */
    public void setCdIndicadorFeriadoLocal(int cdIndicadorFeriadoLocal)
    {
        this._cdIndicadorFeriadoLocal = cdIndicadorFeriadoLocal;
        this._has_cdIndicadorFeriadoLocal = true;
    } //-- void setCdIndicadorFeriadoLocal(int) 

    /**
     * Sets the value of field 'cdIndicadorLancamentoPagamento'.
     * 
     * @param cdIndicadorLancamentoPagamento the value of field
     * 'cdIndicadorLancamentoPagamento'.
     */
    public void setCdIndicadorLancamentoPagamento(int cdIndicadorLancamentoPagamento)
    {
        this._cdIndicadorLancamentoPagamento = cdIndicadorLancamentoPagamento;
        this._has_cdIndicadorLancamentoPagamento = true;
    } //-- void setCdIndicadorLancamentoPagamento(int) 

    /**
     * Sets the value of field 'cdIndicadorListaDebito'.
     * 
     * @param cdIndicadorListaDebito the value of field
     * 'cdIndicadorListaDebito'.
     */
    public void setCdIndicadorListaDebito(int cdIndicadorListaDebito)
    {
        this._cdIndicadorListaDebito = cdIndicadorListaDebito;
        this._has_cdIndicadorListaDebito = true;
    } //-- void setCdIndicadorListaDebito(int) 

    /**
     * Sets the value of field 'cdIndicadorMensagemPerso'.
     * 
     * @param cdIndicadorMensagemPerso the value of field
     * 'cdIndicadorMensagemPerso'.
     */
    public void setCdIndicadorMensagemPerso(int cdIndicadorMensagemPerso)
    {
        this._cdIndicadorMensagemPerso = cdIndicadorMensagemPerso;
        this._has_cdIndicadorMensagemPerso = true;
    } //-- void setCdIndicadorMensagemPerso(int) 

    /**
     * Sets the value of field 'cdIndicadorRetornoInternet'.
     * 
     * @param cdIndicadorRetornoInternet the value of field
     * 'cdIndicadorRetornoInternet'.
     */
    public void setCdIndicadorRetornoInternet(int cdIndicadorRetornoInternet)
    {
        this._cdIndicadorRetornoInternet = cdIndicadorRetornoInternet;
        this._has_cdIndicadorRetornoInternet = true;
    } //-- void setCdIndicadorRetornoInternet(int) 

    /**
     * Sets the value of field 'cdIndicadorRetornoSeparado'.
     * 
     * @param cdIndicadorRetornoSeparado the value of field
     * 'cdIndicadorRetornoSeparado'.
     */
    public void setCdIndicadorRetornoSeparado(int cdIndicadorRetornoSeparado)
    {
        this._cdIndicadorRetornoSeparado = cdIndicadorRetornoSeparado;
        this._has_cdIndicadorRetornoSeparado = true;
    } //-- void setCdIndicadorRetornoSeparado(int) 

    /**
     * Sets the value of field 'cdIndicadorSegundaLinha'.
     * 
     * @param cdIndicadorSegundaLinha the value of field
     * 'cdIndicadorSegundaLinha'.
     */
    public void setCdIndicadorSegundaLinha(int cdIndicadorSegundaLinha)
    {
        this._cdIndicadorSegundaLinha = cdIndicadorSegundaLinha;
        this._has_cdIndicadorSegundaLinha = true;
    } //-- void setCdIndicadorSegundaLinha(int) 

    /**
     * Sets the value of field 'cdIndicadorTipoRetornoInternet'.
     * 
     * @param cdIndicadorTipoRetornoInternet the value of field
     * 'cdIndicadorTipoRetornoInternet'.
     */
    public void setCdIndicadorTipoRetornoInternet(int cdIndicadorTipoRetornoInternet)
    {
        this._cdIndicadorTipoRetornoInternet = cdIndicadorTipoRetornoInternet;
        this._has_cdIndicadorTipoRetornoInternet = true;
    } //-- void setCdIndicadorTipoRetornoInternet(int) 

    /**
     * Sets the value of field 'cdIndicadorUtilizaMora'.
     * 
     * @param cdIndicadorUtilizaMora the value of field
     * 'cdIndicadorUtilizaMora'.
     */
    public void setCdIndicadorUtilizaMora(int cdIndicadorUtilizaMora)
    {
        this._cdIndicadorUtilizaMora = cdIndicadorUtilizaMora;
        this._has_cdIndicadorUtilizaMora = true;
    } //-- void setCdIndicadorUtilizaMora(int) 

    /**
     * Sets the value of field 'cdLancamentoFuturoCredito'.
     * 
     * @param cdLancamentoFuturoCredito the value of field
     * 'cdLancamentoFuturoCredito'.
     */
    public void setCdLancamentoFuturoCredito(int cdLancamentoFuturoCredito)
    {
        this._cdLancamentoFuturoCredito = cdLancamentoFuturoCredito;
        this._has_cdLancamentoFuturoCredito = true;
    } //-- void setCdLancamentoFuturoCredito(int) 

    /**
     * Sets the value of field 'cdLancamentoFuturoDebito'.
     * 
     * @param cdLancamentoFuturoDebito the value of field
     * 'cdLancamentoFuturoDebito'.
     */
    public void setCdLancamentoFuturoDebito(int cdLancamentoFuturoDebito)
    {
        this._cdLancamentoFuturoDebito = cdLancamentoFuturoDebito;
        this._has_cdLancamentoFuturoDebito = true;
    } //-- void setCdLancamentoFuturoDebito(int) 

    /**
     * Sets the value of field 'cdLiberacaoLoteProcesso'.
     * 
     * @param cdLiberacaoLoteProcesso the value of field
     * 'cdLiberacaoLoteProcesso'.
     */
    public void setCdLiberacaoLoteProcesso(int cdLiberacaoLoteProcesso)
    {
        this._cdLiberacaoLoteProcesso = cdLiberacaoLoteProcesso;
        this._has_cdLiberacaoLoteProcesso = true;
    } //-- void setCdLiberacaoLoteProcesso(int) 

    /**
     * Sets the value of field 'cdManutencaoBaseRecadastro'.
     * 
     * @param cdManutencaoBaseRecadastro the value of field
     * 'cdManutencaoBaseRecadastro'.
     */
    public void setCdManutencaoBaseRecadastro(int cdManutencaoBaseRecadastro)
    {
        this._cdManutencaoBaseRecadastro = cdManutencaoBaseRecadastro;
        this._has_cdManutencaoBaseRecadastro = true;
    } //-- void setCdManutencaoBaseRecadastro(int) 

    /**
     * Sets the value of field 'cdMeioPagamentoCredito'.
     * 
     * @param cdMeioPagamentoCredito the value of field
     * 'cdMeioPagamentoCredito'.
     */
    public void setCdMeioPagamentoCredito(int cdMeioPagamentoCredito)
    {
        this._cdMeioPagamentoCredito = cdMeioPagamentoCredito;
        this._has_cdMeioPagamentoCredito = true;
    } //-- void setCdMeioPagamentoCredito(int) 

    /**
     * Sets the value of field 'cdMensagemRecadastroMidia'.
     * 
     * @param cdMensagemRecadastroMidia the value of field
     * 'cdMensagemRecadastroMidia'.
     */
    public void setCdMensagemRecadastroMidia(int cdMensagemRecadastroMidia)
    {
        this._cdMensagemRecadastroMidia = cdMensagemRecadastroMidia;
        this._has_cdMensagemRecadastroMidia = true;
    } //-- void setCdMensagemRecadastroMidia(int) 

    /**
     * Sets the value of field 'cdMidiaDisponivel'.
     * 
     * @param cdMidiaDisponivel the value of field
     * 'cdMidiaDisponivel'.
     */
    public void setCdMidiaDisponivel(int cdMidiaDisponivel)
    {
        this._cdMidiaDisponivel = cdMidiaDisponivel;
        this._has_cdMidiaDisponivel = true;
    } //-- void setCdMidiaDisponivel(int) 

    /**
     * Sets the value of field 'cdMidiaMensagemRecadastro'.
     * 
     * @param cdMidiaMensagemRecadastro the value of field
     * 'cdMidiaMensagemRecadastro'.
     */
    public void setCdMidiaMensagemRecadastro(int cdMidiaMensagemRecadastro)
    {
        this._cdMidiaMensagemRecadastro = cdMidiaMensagemRecadastro;
        this._has_cdMidiaMensagemRecadastro = true;
    } //-- void setCdMidiaMensagemRecadastro(int) 

    /**
     * Sets the value of field 'cdMomentoAvisoRacadastro'.
     * 
     * @param cdMomentoAvisoRacadastro the value of field
     * 'cdMomentoAvisoRacadastro'.
     */
    public void setCdMomentoAvisoRacadastro(int cdMomentoAvisoRacadastro)
    {
        this._cdMomentoAvisoRacadastro = cdMomentoAvisoRacadastro;
        this._has_cdMomentoAvisoRacadastro = true;
    } //-- void setCdMomentoAvisoRacadastro(int) 

    /**
     * Sets the value of field 'cdMomentoCreditoEfetivacao'.
     * 
     * @param cdMomentoCreditoEfetivacao the value of field
     * 'cdMomentoCreditoEfetivacao'.
     */
    public void setCdMomentoCreditoEfetivacao(int cdMomentoCreditoEfetivacao)
    {
        this._cdMomentoCreditoEfetivacao = cdMomentoCreditoEfetivacao;
        this._has_cdMomentoCreditoEfetivacao = true;
    } //-- void setCdMomentoCreditoEfetivacao(int) 

    /**
     * Sets the value of field 'cdMomentoDebitoPagamento'.
     * 
     * @param cdMomentoDebitoPagamento the value of field
     * 'cdMomentoDebitoPagamento'.
     */
    public void setCdMomentoDebitoPagamento(int cdMomentoDebitoPagamento)
    {
        this._cdMomentoDebitoPagamento = cdMomentoDebitoPagamento;
        this._has_cdMomentoDebitoPagamento = true;
    } //-- void setCdMomentoDebitoPagamento(int) 

    /**
     * Sets the value of field 'cdMomentoFormularioRecadastro'.
     * 
     * @param cdMomentoFormularioRecadastro the value of field
     * 'cdMomentoFormularioRecadastro'.
     */
    public void setCdMomentoFormularioRecadastro(int cdMomentoFormularioRecadastro)
    {
        this._cdMomentoFormularioRecadastro = cdMomentoFormularioRecadastro;
        this._has_cdMomentoFormularioRecadastro = true;
    } //-- void setCdMomentoFormularioRecadastro(int) 

    /**
     * Sets the value of field 'cdMomentoProcessamentoPagamento'.
     * 
     * @param cdMomentoProcessamentoPagamento the value of field
     * 'cdMomentoProcessamentoPagamento'.
     */
    public void setCdMomentoProcessamentoPagamento(int cdMomentoProcessamentoPagamento)
    {
        this._cdMomentoProcessamentoPagamento = cdMomentoProcessamentoPagamento;
        this._has_cdMomentoProcessamentoPagamento = true;
    } //-- void setCdMomentoProcessamentoPagamento(int) 

    /**
     * Sets the value of field 'cdNaturezaOperacaoPagamento'.
     * 
     * @param cdNaturezaOperacaoPagamento the value of field
     * 'cdNaturezaOperacaoPagamento'.
     */
    public void setCdNaturezaOperacaoPagamento(int cdNaturezaOperacaoPagamento)
    {
        this._cdNaturezaOperacaoPagamento = cdNaturezaOperacaoPagamento;
        this._has_cdNaturezaOperacaoPagamento = true;
    } //-- void setCdNaturezaOperacaoPagamento(int) 

    /**
     * Sets the value of field 'cdPagamentoNaoUtilizado'.
     * 
     * @param cdPagamentoNaoUtilizado the value of field
     * 'cdPagamentoNaoUtilizado'.
     */
    public void setCdPagamentoNaoUtilizado(int cdPagamentoNaoUtilizado)
    {
        this._cdPagamentoNaoUtilizado = cdPagamentoNaoUtilizado;
        this._has_cdPagamentoNaoUtilizado = true;
    } //-- void setCdPagamentoNaoUtilizado(int) 

    /**
     * Sets the value of field
     * 'cdPercentualIndicadorReajusteTarifa'.
     * 
     * @param cdPercentualIndicadorReajusteTarifa the value of
     * field 'cdPercentualIndicadorReajusteTarifa'.
     */
    public void setCdPercentualIndicadorReajusteTarifa(java.math.BigDecimal cdPercentualIndicadorReajusteTarifa)
    {
        this._cdPercentualIndicadorReajusteTarifa = cdPercentualIndicadorReajusteTarifa;
    } //-- void setCdPercentualIndicadorReajusteTarifa(java.math.BigDecimal) 

    /**
     * Sets the value of field 'cdPercentualMaximoInconLote'.
     * 
     * @param cdPercentualMaximoInconLote the value of field
     * 'cdPercentualMaximoInconLote'.
     */
    public void setCdPercentualMaximoInconLote(int cdPercentualMaximoInconLote)
    {
        this._cdPercentualMaximoInconLote = cdPercentualMaximoInconLote;
        this._has_cdPercentualMaximoInconLote = true;
    } //-- void setCdPercentualMaximoInconLote(int) 

    /**
     * Sets the value of field 'cdPercentualReducaoTarifaCatalogo'.
     * 
     * @param cdPercentualReducaoTarifaCatalogo the value of field
     * 'cdPercentualReducaoTarifaCatalogo'.
     */
    public void setCdPercentualReducaoTarifaCatalogo(java.math.BigDecimal cdPercentualReducaoTarifaCatalogo)
    {
        this._cdPercentualReducaoTarifaCatalogo = cdPercentualReducaoTarifaCatalogo;
    } //-- void setCdPercentualReducaoTarifaCatalogo(java.math.BigDecimal) 

    /**
     * Sets the value of field 'cdPerdcCobrancaTarifa'.
     * 
     * @param cdPerdcCobrancaTarifa the value of field
     * 'cdPerdcCobrancaTarifa'.
     */
    public void setCdPerdcCobrancaTarifa(int cdPerdcCobrancaTarifa)
    {
        this._cdPerdcCobrancaTarifa = cdPerdcCobrancaTarifa;
        this._has_cdPerdcCobrancaTarifa = true;
    } //-- void setCdPerdcCobrancaTarifa(int) 

    /**
     * Sets the value of field 'cdPerdcComprovante'.
     * 
     * @param cdPerdcComprovante the value of field
     * 'cdPerdcComprovante'.
     */
    public void setCdPerdcComprovante(int cdPerdcComprovante)
    {
        this._cdPerdcComprovante = cdPerdcComprovante;
        this._has_cdPerdcComprovante = true;
    } //-- void setCdPerdcComprovante(int) 

    /**
     * Sets the value of field 'cdPerdcConsultaVeiculo'.
     * 
     * @param cdPerdcConsultaVeiculo the value of field
     * 'cdPerdcConsultaVeiculo'.
     */
    public void setCdPerdcConsultaVeiculo(int cdPerdcConsultaVeiculo)
    {
        this._cdPerdcConsultaVeiculo = cdPerdcConsultaVeiculo;
        this._has_cdPerdcConsultaVeiculo = true;
    } //-- void setCdPerdcConsultaVeiculo(int) 

    /**
     * Sets the value of field 'cdPerdcEnvioRemessa'.
     * 
     * @param cdPerdcEnvioRemessa the value of field
     * 'cdPerdcEnvioRemessa'.
     */
    public void setCdPerdcEnvioRemessa(int cdPerdcEnvioRemessa)
    {
        this._cdPerdcEnvioRemessa = cdPerdcEnvioRemessa;
        this._has_cdPerdcEnvioRemessa = true;
    } //-- void setCdPerdcEnvioRemessa(int) 

    /**
     * Sets the value of field 'cdPerdcManutencaoProcd'.
     * 
     * @param cdPerdcManutencaoProcd the value of field
     * 'cdPerdcManutencaoProcd'.
     */
    public void setCdPerdcManutencaoProcd(int cdPerdcManutencaoProcd)
    {
        this._cdPerdcManutencaoProcd = cdPerdcManutencaoProcd;
        this._has_cdPerdcManutencaoProcd = true;
    } //-- void setCdPerdcManutencaoProcd(int) 

    /**
     * Sets the value of field 'cdPeriodicidadeAviso'.
     * 
     * @param cdPeriodicidadeAviso the value of field
     * 'cdPeriodicidadeAviso'.
     */
    public void setCdPeriodicidadeAviso(int cdPeriodicidadeAviso)
    {
        this._cdPeriodicidadeAviso = cdPeriodicidadeAviso;
        this._has_cdPeriodicidadeAviso = true;
    } //-- void setCdPeriodicidadeAviso(int) 

    /**
     * Sets the value of field 'cdPermissaoDebitoOnline'.
     * 
     * @param cdPermissaoDebitoOnline the value of field
     * 'cdPermissaoDebitoOnline'.
     */
    public void setCdPermissaoDebitoOnline(int cdPermissaoDebitoOnline)
    {
        this._cdPermissaoDebitoOnline = cdPermissaoDebitoOnline;
        this._has_cdPermissaoDebitoOnline = true;
    } //-- void setCdPermissaoDebitoOnline(int) 

    /**
     * Sets the value of field
     * 'cdPreenchimentoLancamentoPersonalizado'.
     * 
     * @param cdPreenchimentoLancamentoPersonalizado the value of
     * field 'cdPreenchimentoLancamentoPersonalizado'.
     */
    public void setCdPreenchimentoLancamentoPersonalizado(int cdPreenchimentoLancamentoPersonalizado)
    {
        this._cdPreenchimentoLancamentoPersonalizado = cdPreenchimentoLancamentoPersonalizado;
        this._has_cdPreenchimentoLancamentoPersonalizado = true;
    } //-- void setCdPreenchimentoLancamentoPersonalizado(int) 

    /**
     * Sets the value of field 'cdPrincipalEnquaRecadastro'.
     * 
     * @param cdPrincipalEnquaRecadastro the value of field
     * 'cdPrincipalEnquaRecadastro'.
     */
    public void setCdPrincipalEnquaRecadastro(int cdPrincipalEnquaRecadastro)
    {
        this._cdPrincipalEnquaRecadastro = cdPrincipalEnquaRecadastro;
        this._has_cdPrincipalEnquaRecadastro = true;
    } //-- void setCdPrincipalEnquaRecadastro(int) 

    /**
     * Sets the value of field 'cdPrioridadeEfetivacaoPagamento'.
     * 
     * @param cdPrioridadeEfetivacaoPagamento the value of field
     * 'cdPrioridadeEfetivacaoPagamento'.
     */
    public void setCdPrioridadeEfetivacaoPagamento(int cdPrioridadeEfetivacaoPagamento)
    {
        this._cdPrioridadeEfetivacaoPagamento = cdPrioridadeEfetivacaoPagamento;
        this._has_cdPrioridadeEfetivacaoPagamento = true;
    } //-- void setCdPrioridadeEfetivacaoPagamento(int) 

    /**
     * Sets the value of field 'cdRastreabilidadeNotaFiscal'.
     * 
     * @param cdRastreabilidadeNotaFiscal the value of field
     * 'cdRastreabilidadeNotaFiscal'.
     */
    public void setCdRastreabilidadeNotaFiscal(int cdRastreabilidadeNotaFiscal)
    {
        this._cdRastreabilidadeNotaFiscal = cdRastreabilidadeNotaFiscal;
        this._has_cdRastreabilidadeNotaFiscal = true;
    } //-- void setCdRastreabilidadeNotaFiscal(int) 

    /**
     * Sets the value of field 'cdRastreabilidadeTituloTerceiro'.
     * 
     * @param cdRastreabilidadeTituloTerceiro the value of field
     * 'cdRastreabilidadeTituloTerceiro'.
     */
    public void setCdRastreabilidadeTituloTerceiro(int cdRastreabilidadeTituloTerceiro)
    {
        this._cdRastreabilidadeTituloTerceiro = cdRastreabilidadeTituloTerceiro;
        this._has_cdRastreabilidadeTituloTerceiro = true;
    } //-- void setCdRastreabilidadeTituloTerceiro(int) 

    /**
     * Sets the value of field 'cdRejeicaoAgendaLote'.
     * 
     * @param cdRejeicaoAgendaLote the value of field
     * 'cdRejeicaoAgendaLote'.
     */
    public void setCdRejeicaoAgendaLote(int cdRejeicaoAgendaLote)
    {
        this._cdRejeicaoAgendaLote = cdRejeicaoAgendaLote;
        this._has_cdRejeicaoAgendaLote = true;
    } //-- void setCdRejeicaoAgendaLote(int) 

    /**
     * Sets the value of field 'cdRejeicaoEfetivacaoLote'.
     * 
     * @param cdRejeicaoEfetivacaoLote the value of field
     * 'cdRejeicaoEfetivacaoLote'.
     */
    public void setCdRejeicaoEfetivacaoLote(int cdRejeicaoEfetivacaoLote)
    {
        this._cdRejeicaoEfetivacaoLote = cdRejeicaoEfetivacaoLote;
        this._has_cdRejeicaoEfetivacaoLote = true;
    } //-- void setCdRejeicaoEfetivacaoLote(int) 

    /**
     * Sets the value of field 'cdRejeicaoLote'.
     * 
     * @param cdRejeicaoLote the value of field 'cdRejeicaoLote'.
     */
    public void setCdRejeicaoLote(int cdRejeicaoLote)
    {
        this._cdRejeicaoLote = cdRejeicaoLote;
        this._has_cdRejeicaoLote = true;
    } //-- void setCdRejeicaoLote(int) 

    /**
     * Sets the value of field 'cdTipoCargaRecadastro'.
     * 
     * @param cdTipoCargaRecadastro the value of field
     * 'cdTipoCargaRecadastro'.
     */
    public void setCdTipoCargaRecadastro(int cdTipoCargaRecadastro)
    {
        this._cdTipoCargaRecadastro = cdTipoCargaRecadastro;
        this._has_cdTipoCargaRecadastro = true;
    } //-- void setCdTipoCargaRecadastro(int) 

    /**
     * Sets the value of field 'cdTipoCataoSalario'.
     * 
     * @param cdTipoCataoSalario the value of field
     * 'cdTipoCataoSalario'.
     */
    public void setCdTipoCataoSalario(int cdTipoCataoSalario)
    {
        this._cdTipoCataoSalario = cdTipoCataoSalario;
        this._has_cdTipoCataoSalario = true;
    } //-- void setCdTipoCataoSalario(int) 

    /**
     * Sets the value of field 'cdTipoConsistenciaLista'.
     * 
     * @param cdTipoConsistenciaLista the value of field
     * 'cdTipoConsistenciaLista'.
     */
    public void setCdTipoConsistenciaLista(int cdTipoConsistenciaLista)
    {
        this._cdTipoConsistenciaLista = cdTipoConsistenciaLista;
        this._has_cdTipoConsistenciaLista = true;
    } //-- void setCdTipoConsistenciaLista(int) 

    /**
     * Sets the value of field 'cdTipoConsultaComprovante'.
     * 
     * @param cdTipoConsultaComprovante the value of field
     * 'cdTipoConsultaComprovante'.
     */
    public void setCdTipoConsultaComprovante(int cdTipoConsultaComprovante)
    {
        this._cdTipoConsultaComprovante = cdTipoConsultaComprovante;
        this._has_cdTipoConsultaComprovante = true;
    } //-- void setCdTipoConsultaComprovante(int) 

    /**
     * Sets the value of field 'cdTipoContaFavorecido'.
     * 
     * @param cdTipoContaFavorecido the value of field
     * 'cdTipoContaFavorecido'.
     */
    public void setCdTipoContaFavorecido(int cdTipoContaFavorecido)
    {
        this._cdTipoContaFavorecido = cdTipoContaFavorecido;
        this._has_cdTipoContaFavorecido = true;
    } //-- void setCdTipoContaFavorecido(int) 

    /**
     * Sets the value of field 'cdTipoDataFloat'.
     * 
     * @param cdTipoDataFloat the value of field 'cdTipoDataFloat'.
     */
    public void setCdTipoDataFloat(int cdTipoDataFloat)
    {
        this._cdTipoDataFloat = cdTipoDataFloat;
        this._has_cdTipoDataFloat = true;
    } //-- void setCdTipoDataFloat(int) 

    /**
     * Sets the value of field 'cdTipoDivergenciaVeiculo'.
     * 
     * @param cdTipoDivergenciaVeiculo the value of field
     * 'cdTipoDivergenciaVeiculo'.
     */
    public void setCdTipoDivergenciaVeiculo(int cdTipoDivergenciaVeiculo)
    {
        this._cdTipoDivergenciaVeiculo = cdTipoDivergenciaVeiculo;
        this._has_cdTipoDivergenciaVeiculo = true;
    } //-- void setCdTipoDivergenciaVeiculo(int) 

    /**
     * Sets the value of field 'cdTipoFormacaoLista'.
     * 
     * @param cdTipoFormacaoLista the value of field
     * 'cdTipoFormacaoLista'.
     */
    public void setCdTipoFormacaoLista(int cdTipoFormacaoLista)
    {
        this._cdTipoFormacaoLista = cdTipoFormacaoLista;
        this._has_cdTipoFormacaoLista = true;
    } //-- void setCdTipoFormacaoLista(int) 

    /**
     * Sets the value of field 'cdTipoIdBeneficio'.
     * 
     * @param cdTipoIdBeneficio the value of field
     * 'cdTipoIdBeneficio'.
     */
    public void setCdTipoIdBeneficio(int cdTipoIdBeneficio)
    {
        this._cdTipoIdBeneficio = cdTipoIdBeneficio;
        this._has_cdTipoIdBeneficio = true;
    } //-- void setCdTipoIdBeneficio(int) 

    /**
     * Sets the value of field 'cdTipoIsncricaoFavorecido'.
     * 
     * @param cdTipoIsncricaoFavorecido the value of field
     * 'cdTipoIsncricaoFavorecido'.
     */
    public void setCdTipoIsncricaoFavorecido(int cdTipoIsncricaoFavorecido)
    {
        this._cdTipoIsncricaoFavorecido = cdTipoIsncricaoFavorecido;
        this._has_cdTipoIsncricaoFavorecido = true;
    } //-- void setCdTipoIsncricaoFavorecido(int) 

    /**
     * Sets the value of field 'cdTipoLayoutArquivo'.
     * 
     * @param cdTipoLayoutArquivo the value of field
     * 'cdTipoLayoutArquivo'.
     */
    public void setCdTipoLayoutArquivo(int cdTipoLayoutArquivo)
    {
        this._cdTipoLayoutArquivo = cdTipoLayoutArquivo;
        this._has_cdTipoLayoutArquivo = true;
    } //-- void setCdTipoLayoutArquivo(int) 

    /**
     * Sets the value of field 'cdTipoReajusteTarifa'.
     * 
     * @param cdTipoReajusteTarifa the value of field
     * 'cdTipoReajusteTarifa'.
     */
    public void setCdTipoReajusteTarifa(int cdTipoReajusteTarifa)
    {
        this._cdTipoReajusteTarifa = cdTipoReajusteTarifa;
        this._has_cdTipoReajusteTarifa = true;
    } //-- void setCdTipoReajusteTarifa(int) 

    /**
     * Sets the value of field 'cdTituloDdaRetorno'.
     * 
     * @param cdTituloDdaRetorno the value of field
     * 'cdTituloDdaRetorno'.
     */
    public void setCdTituloDdaRetorno(int cdTituloDdaRetorno)
    {
        this._cdTituloDdaRetorno = cdTituloDdaRetorno;
        this._has_cdTituloDdaRetorno = true;
    } //-- void setCdTituloDdaRetorno(int) 

    /**
     * Sets the value of field 'cdUsuarioAlteracao'.
     * 
     * @param cdUsuarioAlteracao the value of field
     * 'cdUsuarioAlteracao'.
     */
    public void setCdUsuarioAlteracao(java.lang.String cdUsuarioAlteracao)
    {
        this._cdUsuarioAlteracao = cdUsuarioAlteracao;
    } //-- void setCdUsuarioAlteracao(java.lang.String) 

    /**
     * Sets the value of field 'cdUsuarioExternoAlteracao'.
     * 
     * @param cdUsuarioExternoAlteracao the value of field
     * 'cdUsuarioExternoAlteracao'.
     */
    public void setCdUsuarioExternoAlteracao(java.lang.String cdUsuarioExternoAlteracao)
    {
        this._cdUsuarioExternoAlteracao = cdUsuarioExternoAlteracao;
    } //-- void setCdUsuarioExternoAlteracao(java.lang.String) 

    /**
     * Sets the value of field 'cdUsuarioExternoInclusao'.
     * 
     * @param cdUsuarioExternoInclusao the value of field
     * 'cdUsuarioExternoInclusao'.
     */
    public void setCdUsuarioExternoInclusao(java.lang.String cdUsuarioExternoInclusao)
    {
        this._cdUsuarioExternoInclusao = cdUsuarioExternoInclusao;
    } //-- void setCdUsuarioExternoInclusao(java.lang.String) 

    /**
     * Sets the value of field 'cdUsuarioInclusao'.
     * 
     * @param cdUsuarioInclusao the value of field
     * 'cdUsuarioInclusao'.
     */
    public void setCdUsuarioInclusao(java.lang.String cdUsuarioInclusao)
    {
        this._cdUsuarioInclusao = cdUsuarioInclusao;
    } //-- void setCdUsuarioInclusao(java.lang.String) 

    /**
     * Sets the value of field 'cdUtilizacaoFavorecidoControle'.
     * 
     * @param cdUtilizacaoFavorecidoControle the value of field
     * 'cdUtilizacaoFavorecidoControle'.
     */
    public void setCdUtilizacaoFavorecidoControle(int cdUtilizacaoFavorecidoControle)
    {
        this._cdUtilizacaoFavorecidoControle = cdUtilizacaoFavorecidoControle;
        this._has_cdUtilizacaoFavorecidoControle = true;
    } //-- void setCdUtilizacaoFavorecidoControle(int) 

    /**
     * Sets the value of field 'cdValidacaoNomeFavorecido'.
     * 
     * @param cdValidacaoNomeFavorecido the value of field
     * 'cdValidacaoNomeFavorecido'.
     */
    public void setCdValidacaoNomeFavorecido(int cdValidacaoNomeFavorecido)
    {
        this._cdValidacaoNomeFavorecido = cdValidacaoNomeFavorecido;
        this._has_cdValidacaoNomeFavorecido = true;
    } //-- void setCdValidacaoNomeFavorecido(int) 

    /**
     * Sets the value of field 'codMensagem'.
     * 
     * @param codMensagem the value of field 'codMensagem'.
     */
    public void setCodMensagem(java.lang.String codMensagem)
    {
        this._codMensagem = codMensagem;
    } //-- void setCodMensagem(java.lang.String) 

    /**
     * Sets the value of field 'dsAcaoNaoVida'.
     * 
     * @param dsAcaoNaoVida the value of field 'dsAcaoNaoVida'.
     */
    public void setDsAcaoNaoVida(java.lang.String dsAcaoNaoVida)
    {
        this._dsAcaoNaoVida = dsAcaoNaoVida;
    } //-- void setDsAcaoNaoVida(java.lang.String) 

    /**
     * Sets the value of field 'dsAcertoDadoRecadastro'.
     * 
     * @param dsAcertoDadoRecadastro the value of field
     * 'dsAcertoDadoRecadastro'.
     */
    public void setDsAcertoDadoRecadastro(java.lang.String dsAcertoDadoRecadastro)
    {
        this._dsAcertoDadoRecadastro = dsAcertoDadoRecadastro;
    } //-- void setDsAcertoDadoRecadastro(java.lang.String) 

    /**
     * Sets the value of field 'dsAgendaDebitoVeiculo'.
     * 
     * @param dsAgendaDebitoVeiculo the value of field
     * 'dsAgendaDebitoVeiculo'.
     */
    public void setDsAgendaDebitoVeiculo(java.lang.String dsAgendaDebitoVeiculo)
    {
        this._dsAgendaDebitoVeiculo = dsAgendaDebitoVeiculo;
    } //-- void setDsAgendaDebitoVeiculo(java.lang.String) 

    /**
     * Sets the value of field 'dsAgendaPagamentoVencido'.
     * 
     * @param dsAgendaPagamentoVencido the value of field
     * 'dsAgendaPagamentoVencido'.
     */
    public void setDsAgendaPagamentoVencido(java.lang.String dsAgendaPagamentoVencido)
    {
        this._dsAgendaPagamentoVencido = dsAgendaPagamentoVencido;
    } //-- void setDsAgendaPagamentoVencido(java.lang.String) 

    /**
     * Sets the value of field 'dsAgendaValorMenor'.
     * 
     * @param dsAgendaValorMenor the value of field
     * 'dsAgendaValorMenor'.
     */
    public void setDsAgendaValorMenor(java.lang.String dsAgendaValorMenor)
    {
        this._dsAgendaValorMenor = dsAgendaValorMenor;
    } //-- void setDsAgendaValorMenor(java.lang.String) 

    /**
     * Sets the value of field 'dsAgendamentoRastFilial'.
     * 
     * @param dsAgendamentoRastFilial the value of field
     * 'dsAgendamentoRastFilial'.
     */
    public void setDsAgendamentoRastFilial(java.lang.String dsAgendamentoRastFilial)
    {
        this._dsAgendamentoRastFilial = dsAgendamentoRastFilial;
    } //-- void setDsAgendamentoRastFilial(java.lang.String) 

    /**
     * Sets the value of field 'dsAgrupamentoAviso'.
     * 
     * @param dsAgrupamentoAviso the value of field
     * 'dsAgrupamentoAviso'.
     */
    public void setDsAgrupamentoAviso(java.lang.String dsAgrupamentoAviso)
    {
        this._dsAgrupamentoAviso = dsAgrupamentoAviso;
    } //-- void setDsAgrupamentoAviso(java.lang.String) 

    /**
     * Sets the value of field 'dsAgrupamentoComprovado'.
     * 
     * @param dsAgrupamentoComprovado the value of field
     * 'dsAgrupamentoComprovado'.
     */
    public void setDsAgrupamentoComprovado(java.lang.String dsAgrupamentoComprovado)
    {
        this._dsAgrupamentoComprovado = dsAgrupamentoComprovado;
    } //-- void setDsAgrupamentoComprovado(java.lang.String) 

    /**
     * Sets the value of field 'dsAgrupamentoFormularioRecadastro'.
     * 
     * @param dsAgrupamentoFormularioRecadastro the value of field
     * 'dsAgrupamentoFormularioRecadastro'.
     */
    public void setDsAgrupamentoFormularioRecadastro(java.lang.String dsAgrupamentoFormularioRecadastro)
    {
        this._dsAgrupamentoFormularioRecadastro = dsAgrupamentoFormularioRecadastro;
    } //-- void setDsAgrupamentoFormularioRecadastro(java.lang.String) 

    /**
     * Sets the value of field 'dsAntecRecadastroBeneficio'.
     * 
     * @param dsAntecRecadastroBeneficio the value of field
     * 'dsAntecRecadastroBeneficio'.
     */
    public void setDsAntecRecadastroBeneficio(java.lang.String dsAntecRecadastroBeneficio)
    {
        this._dsAntecRecadastroBeneficio = dsAntecRecadastroBeneficio;
    } //-- void setDsAntecRecadastroBeneficio(java.lang.String) 

    /**
     * Sets the value of field 'dsAreaReservada'.
     * 
     * @param dsAreaReservada the value of field 'dsAreaReservada'.
     */
    public void setDsAreaReservada(java.lang.String dsAreaReservada)
    {
        this._dsAreaReservada = dsAreaReservada;
    } //-- void setDsAreaReservada(java.lang.String) 

    /**
     * Sets the value of field 'dsAreaResrd'.
     * 
     * @param dsAreaResrd the value of field 'dsAreaResrd'.
     */
    public void setDsAreaResrd(java.lang.String dsAreaResrd)
    {
        this._dsAreaResrd = dsAreaResrd;
    } //-- void setDsAreaResrd(java.lang.String) 

    /**
     * Sets the value of field 'dsBaseRecadastroBeneficio'.
     * 
     * @param dsBaseRecadastroBeneficio the value of field
     * 'dsBaseRecadastroBeneficio'.
     */
    public void setDsBaseRecadastroBeneficio(java.lang.String dsBaseRecadastroBeneficio)
    {
        this._dsBaseRecadastroBeneficio = dsBaseRecadastroBeneficio;
    } //-- void setDsBaseRecadastroBeneficio(java.lang.String) 

    /**
     * Sets the value of field 'dsBloqueioEmissaoPplta'.
     * 
     * @param dsBloqueioEmissaoPplta the value of field
     * 'dsBloqueioEmissaoPplta'.
     */
    public void setDsBloqueioEmissaoPplta(java.lang.String dsBloqueioEmissaoPplta)
    {
        this._dsBloqueioEmissaoPplta = dsBloqueioEmissaoPplta;
    } //-- void setDsBloqueioEmissaoPplta(java.lang.String) 

    /**
     * Sets the value of field 'dsCanalAlteracao'.
     * 
     * @param dsCanalAlteracao the value of field 'dsCanalAlteracao'
     */
    public void setDsCanalAlteracao(java.lang.String dsCanalAlteracao)
    {
        this._dsCanalAlteracao = dsCanalAlteracao;
    } //-- void setDsCanalAlteracao(java.lang.String) 

    /**
     * Sets the value of field 'dsCanalInclusao'.
     * 
     * @param dsCanalInclusao the value of field 'dsCanalInclusao'.
     */
    public void setDsCanalInclusao(java.lang.String dsCanalInclusao)
    {
        this._dsCanalInclusao = dsCanalInclusao;
    } //-- void setDsCanalInclusao(java.lang.String) 

    /**
     * Sets the value of field 'dsCapituloTituloRegistro'.
     * 
     * @param dsCapituloTituloRegistro the value of field
     * 'dsCapituloTituloRegistro'.
     */
    public void setDsCapituloTituloRegistro(java.lang.String dsCapituloTituloRegistro)
    {
        this._dsCapituloTituloRegistro = dsCapituloTituloRegistro;
    } //-- void setDsCapituloTituloRegistro(java.lang.String) 

    /**
     * Sets the value of field 'dsCobrancaTarifa'.
     * 
     * @param dsCobrancaTarifa the value of field 'dsCobrancaTarifa'
     */
    public void setDsCobrancaTarifa(java.lang.String dsCobrancaTarifa)
    {
        this._dsCobrancaTarifa = dsCobrancaTarifa;
    } //-- void setDsCobrancaTarifa(java.lang.String) 

    /**
     * Sets the value of field 'dsCodigoFormularioContratoCliente'.
     * 
     * @param dsCodigoFormularioContratoCliente the value of field
     * 'dsCodigoFormularioContratoCliente'.
     */
    public void setDsCodigoFormularioContratoCliente(java.lang.String dsCodigoFormularioContratoCliente)
    {
        this._dsCodigoFormularioContratoCliente = dsCodigoFormularioContratoCliente;
    } //-- void setDsCodigoFormularioContratoCliente(java.lang.String) 

    /**
     * Sets the value of field 'dsCodigoIndFeriadoLocal'.
     * 
     * @param dsCodigoIndFeriadoLocal the value of field
     * 'dsCodigoIndFeriadoLocal'.
     */
    public void setDsCodigoIndFeriadoLocal(java.lang.String dsCodigoIndFeriadoLocal)
    {
        this._dsCodigoIndFeriadoLocal = dsCodigoIndFeriadoLocal;
    } //-- void setDsCodigoIndFeriadoLocal(java.lang.String) 

    /**
     * Sets the value of field 'dsCodigoIndicadorRetornoSeparado'.
     * 
     * @param dsCodigoIndicadorRetornoSeparado the value of field
     * 'dsCodigoIndicadorRetornoSeparado'.
     */
    public void setDsCodigoIndicadorRetornoSeparado(java.lang.String dsCodigoIndicadorRetornoSeparado)
    {
        this._dsCodigoIndicadorRetornoSeparado = dsCodigoIndicadorRetornoSeparado;
    } //-- void setDsCodigoIndicadorRetornoSeparado(java.lang.String) 

    /**
     * Sets the value of field 'dsConsDebitoVeiculo'.
     * 
     * @param dsConsDebitoVeiculo the value of field
     * 'dsConsDebitoVeiculo'.
     */
    public void setDsConsDebitoVeiculo(java.lang.String dsConsDebitoVeiculo)
    {
        this._dsConsDebitoVeiculo = dsConsDebitoVeiculo;
    } //-- void setDsConsDebitoVeiculo(java.lang.String) 

    /**
     * Sets the value of field 'dsConsEndereco'.
     * 
     * @param dsConsEndereco the value of field 'dsConsEndereco'.
     */
    public void setDsConsEndereco(java.lang.String dsConsEndereco)
    {
        this._dsConsEndereco = dsConsEndereco;
    } //-- void setDsConsEndereco(java.lang.String) 

    /**
     * Sets the value of field 'dsConsSaldoPagamento'.
     * 
     * @param dsConsSaldoPagamento the value of field
     * 'dsConsSaldoPagamento'.
     */
    public void setDsConsSaldoPagamento(java.lang.String dsConsSaldoPagamento)
    {
        this._dsConsSaldoPagamento = dsConsSaldoPagamento;
    } //-- void setDsConsSaldoPagamento(java.lang.String) 

    /**
     * Sets the value of field 'dsConsistenciaCpfCnpjBenefAvalNpc'.
     * 
     * @param dsConsistenciaCpfCnpjBenefAvalNpc the value of field
     * 'dsConsistenciaCpfCnpjBenefAvalNpc'.
     */
    public void setDsConsistenciaCpfCnpjBenefAvalNpc(java.lang.String dsConsistenciaCpfCnpjBenefAvalNpc)
    {
        this._dsConsistenciaCpfCnpjBenefAvalNpc = dsConsistenciaCpfCnpjBenefAvalNpc;
    } //-- void setDsConsistenciaCpfCnpjBenefAvalNpc(java.lang.String) 

    /**
     * Sets the value of field 'dsConsultaSaldoSuperior'.
     * 
     * @param dsConsultaSaldoSuperior the value of field
     * 'dsConsultaSaldoSuperior'.
     */
    public void setDsConsultaSaldoSuperior(java.lang.String dsConsultaSaldoSuperior)
    {
        this._dsConsultaSaldoSuperior = dsConsultaSaldoSuperior;
    } //-- void setDsConsultaSaldoSuperior(java.lang.String) 

    /**
     * Sets the value of field 'dsContagemConsSaudo'.
     * 
     * @param dsContagemConsSaudo the value of field
     * 'dsContagemConsSaudo'.
     */
    public void setDsContagemConsSaudo(java.lang.String dsContagemConsSaudo)
    {
        this._dsContagemConsSaudo = dsContagemConsSaudo;
    } //-- void setDsContagemConsSaudo(java.lang.String) 

    /**
     * Sets the value of field 'dsContratoContaTransferencia'.
     * 
     * @param dsContratoContaTransferencia the value of field
     * 'dsContratoContaTransferencia'.
     */
    public void setDsContratoContaTransferencia(java.lang.String dsContratoContaTransferencia)
    {
        this._dsContratoContaTransferencia = dsContratoContaTransferencia;
    } //-- void setDsContratoContaTransferencia(java.lang.String) 

    /**
     * Sets the value of field 'dsCreditoNaoUtilizado'.
     * 
     * @param dsCreditoNaoUtilizado the value of field
     * 'dsCreditoNaoUtilizado'.
     */
    public void setDsCreditoNaoUtilizado(java.lang.String dsCreditoNaoUtilizado)
    {
        this._dsCreditoNaoUtilizado = dsCreditoNaoUtilizado;
    } //-- void setDsCreditoNaoUtilizado(java.lang.String) 

    /**
     * Sets the value of field 'dsCriterioEnquaBeneficio'.
     * 
     * @param dsCriterioEnquaBeneficio the value of field
     * 'dsCriterioEnquaBeneficio'.
     */
    public void setDsCriterioEnquaBeneficio(java.lang.String dsCriterioEnquaBeneficio)
    {
        this._dsCriterioEnquaBeneficio = dsCriterioEnquaBeneficio;
    } //-- void setDsCriterioEnquaBeneficio(java.lang.String) 

    /**
     * Sets the value of field 'dsCriterioEnquaRecadastro'.
     * 
     * @param dsCriterioEnquaRecadastro the value of field
     * 'dsCriterioEnquaRecadastro'.
     */
    public void setDsCriterioEnquaRecadastro(java.lang.String dsCriterioEnquaRecadastro)
    {
        this._dsCriterioEnquaRecadastro = dsCriterioEnquaRecadastro;
    } //-- void setDsCriterioEnquaRecadastro(java.lang.String) 

    /**
     * Sets the value of field 'dsCriterioRastreabilidadeTitulo'.
     * 
     * @param dsCriterioRastreabilidadeTitulo the value of field
     * 'dsCriterioRastreabilidadeTitulo'.
     */
    public void setDsCriterioRastreabilidadeTitulo(java.lang.String dsCriterioRastreabilidadeTitulo)
    {
        this._dsCriterioRastreabilidadeTitulo = dsCriterioRastreabilidadeTitulo;
    } //-- void setDsCriterioRastreabilidadeTitulo(java.lang.String) 

    /**
     * Sets the value of field 'dsCtciaEspecieBeneficio'.
     * 
     * @param dsCtciaEspecieBeneficio the value of field
     * 'dsCtciaEspecieBeneficio'.
     */
    public void setDsCtciaEspecieBeneficio(java.lang.String dsCtciaEspecieBeneficio)
    {
        this._dsCtciaEspecieBeneficio = dsCtciaEspecieBeneficio;
    } //-- void setDsCtciaEspecieBeneficio(java.lang.String) 

    /**
     * Sets the value of field 'dsCtciaIdentificacaoBeneficio'.
     * 
     * @param dsCtciaIdentificacaoBeneficio the value of field
     * 'dsCtciaIdentificacaoBeneficio'.
     */
    public void setDsCtciaIdentificacaoBeneficio(java.lang.String dsCtciaIdentificacaoBeneficio)
    {
        this._dsCtciaIdentificacaoBeneficio = dsCtciaIdentificacaoBeneficio;
    } //-- void setDsCtciaIdentificacaoBeneficio(java.lang.String) 

    /**
     * Sets the value of field 'dsCtciaInscricaoFavorecido'.
     * 
     * @param dsCtciaInscricaoFavorecido the value of field
     * 'dsCtciaInscricaoFavorecido'.
     */
    public void setDsCtciaInscricaoFavorecido(java.lang.String dsCtciaInscricaoFavorecido)
    {
        this._dsCtciaInscricaoFavorecido = dsCtciaInscricaoFavorecido;
    } //-- void setDsCtciaInscricaoFavorecido(java.lang.String) 

    /**
     * Sets the value of field 'dsCtciaProprietarioVeiculo'.
     * 
     * @param dsCtciaProprietarioVeiculo the value of field
     * 'dsCtciaProprietarioVeiculo'.
     */
    public void setDsCtciaProprietarioVeiculo(java.lang.String dsCtciaProprietarioVeiculo)
    {
        this._dsCtciaProprietarioVeiculo = dsCtciaProprietarioVeiculo;
    } //-- void setDsCtciaProprietarioVeiculo(java.lang.String) 

    /**
     * Sets the value of field 'dsDestinoAviso'.
     * 
     * @param dsDestinoAviso the value of field 'dsDestinoAviso'.
     */
    public void setDsDestinoAviso(java.lang.String dsDestinoAviso)
    {
        this._dsDestinoAviso = dsDestinoAviso;
    } //-- void setDsDestinoAviso(java.lang.String) 

    /**
     * Sets the value of field 'dsDestinoComprovante'.
     * 
     * @param dsDestinoComprovante the value of field
     * 'dsDestinoComprovante'.
     */
    public void setDsDestinoComprovante(java.lang.String dsDestinoComprovante)
    {
        this._dsDestinoComprovante = dsDestinoComprovante;
    } //-- void setDsDestinoComprovante(java.lang.String) 

    /**
     * Sets the value of field 'dsDestinoFormularioRecadastro'.
     * 
     * @param dsDestinoFormularioRecadastro the value of field
     * 'dsDestinoFormularioRecadastro'.
     */
    public void setDsDestinoFormularioRecadastro(java.lang.String dsDestinoFormularioRecadastro)
    {
        this._dsDestinoFormularioRecadastro = dsDestinoFormularioRecadastro;
    } //-- void setDsDestinoFormularioRecadastro(java.lang.String) 

    /**
     * Sets the value of field 'dsDispzContaCredito'.
     * 
     * @param dsDispzContaCredito the value of field
     * 'dsDispzContaCredito'.
     */
    public void setDsDispzContaCredito(java.lang.String dsDispzContaCredito)
    {
        this._dsDispzContaCredito = dsDispzContaCredito;
    } //-- void setDsDispzContaCredito(java.lang.String) 

    /**
     * Sets the value of field 'dsDispzDiversarCrrtt'.
     * 
     * @param dsDispzDiversarCrrtt the value of field
     * 'dsDispzDiversarCrrtt'.
     */
    public void setDsDispzDiversarCrrtt(java.lang.String dsDispzDiversarCrrtt)
    {
        this._dsDispzDiversarCrrtt = dsDispzDiversarCrrtt;
    } //-- void setDsDispzDiversarCrrtt(java.lang.String) 

    /**
     * Sets the value of field 'dsDispzDiversasNao'.
     * 
     * @param dsDispzDiversasNao the value of field
     * 'dsDispzDiversasNao'.
     */
    public void setDsDispzDiversasNao(java.lang.String dsDispzDiversasNao)
    {
        this._dsDispzDiversasNao = dsDispzDiversasNao;
    } //-- void setDsDispzDiversasNao(java.lang.String) 

    /**
     * Sets the value of field 'dsDispzSalarioCrrtt'.
     * 
     * @param dsDispzSalarioCrrtt the value of field
     * 'dsDispzSalarioCrrtt'.
     */
    public void setDsDispzSalarioCrrtt(java.lang.String dsDispzSalarioCrrtt)
    {
        this._dsDispzSalarioCrrtt = dsDispzSalarioCrrtt;
    } //-- void setDsDispzSalarioCrrtt(java.lang.String) 

    /**
     * Sets the value of field 'dsDispzSalarioNao'.
     * 
     * @param dsDispzSalarioNao the value of field
     * 'dsDispzSalarioNao'.
     */
    public void setDsDispzSalarioNao(java.lang.String dsDispzSalarioNao)
    {
        this._dsDispzSalarioNao = dsDispzSalarioNao;
    } //-- void setDsDispzSalarioNao(java.lang.String) 

    /**
     * Sets the value of field 'dsEnvelopeAberto'.
     * 
     * @param dsEnvelopeAberto the value of field 'dsEnvelopeAberto'
     */
    public void setDsEnvelopeAberto(java.lang.String dsEnvelopeAberto)
    {
        this._dsEnvelopeAberto = dsEnvelopeAberto;
    } //-- void setDsEnvelopeAberto(java.lang.String) 

    /**
     * Sets the value of field 'dsExigeFilial'.
     * 
     * @param dsExigeFilial the value of field 'dsExigeFilial'.
     */
    public void setDsExigeFilial(java.lang.String dsExigeFilial)
    {
        this._dsExigeFilial = dsExigeFilial;
    } //-- void setDsExigeFilial(java.lang.String) 

    /**
     * Sets the value of field 'dsFavorecidoConsPagamento'.
     * 
     * @param dsFavorecidoConsPagamento the value of field
     * 'dsFavorecidoConsPagamento'.
     */
    public void setDsFavorecidoConsPagamento(java.lang.String dsFavorecidoConsPagamento)
    {
        this._dsFavorecidoConsPagamento = dsFavorecidoConsPagamento;
    } //-- void setDsFavorecidoConsPagamento(java.lang.String) 

    /**
     * Sets the value of field 'dsFloatServicoContrato'.
     * 
     * @param dsFloatServicoContrato the value of field
     * 'dsFloatServicoContrato'.
     */
    public void setDsFloatServicoContrato(java.lang.String dsFloatServicoContrato)
    {
        this._dsFloatServicoContrato = dsFloatServicoContrato;
    } //-- void setDsFloatServicoContrato(java.lang.String) 

    /**
     * Sets the value of field 'dsFormaAutorizacaoPagamento'.
     * 
     * @param dsFormaAutorizacaoPagamento the value of field
     * 'dsFormaAutorizacaoPagamento'.
     */
    public void setDsFormaAutorizacaoPagamento(java.lang.String dsFormaAutorizacaoPagamento)
    {
        this._dsFormaAutorizacaoPagamento = dsFormaAutorizacaoPagamento;
    } //-- void setDsFormaAutorizacaoPagamento(java.lang.String) 

    /**
     * Sets the value of field 'dsFormaEnvioPagamento'.
     * 
     * @param dsFormaEnvioPagamento the value of field
     * 'dsFormaEnvioPagamento'.
     */
    public void setDsFormaEnvioPagamento(java.lang.String dsFormaEnvioPagamento)
    {
        this._dsFormaEnvioPagamento = dsFormaEnvioPagamento;
    } //-- void setDsFormaEnvioPagamento(java.lang.String) 

    /**
     * Sets the value of field 'dsFormaExpiracaoCredito'.
     * 
     * @param dsFormaExpiracaoCredito the value of field
     * 'dsFormaExpiracaoCredito'.
     */
    public void setDsFormaExpiracaoCredito(java.lang.String dsFormaExpiracaoCredito)
    {
        this._dsFormaExpiracaoCredito = dsFormaExpiracaoCredito;
    } //-- void setDsFormaExpiracaoCredito(java.lang.String) 

    /**
     * Sets the value of field 'dsFormaManutencao'.
     * 
     * @param dsFormaManutencao the value of field
     * 'dsFormaManutencao'.
     */
    public void setDsFormaManutencao(java.lang.String dsFormaManutencao)
    {
        this._dsFormaManutencao = dsFormaManutencao;
    } //-- void setDsFormaManutencao(java.lang.String) 

    /**
     * Sets the value of field 'dsFrasePreCadastro'.
     * 
     * @param dsFrasePreCadastro the value of field
     * 'dsFrasePreCadastro'.
     */
    public void setDsFrasePreCadastro(java.lang.String dsFrasePreCadastro)
    {
        this._dsFrasePreCadastro = dsFrasePreCadastro;
    } //-- void setDsFrasePreCadastro(java.lang.String) 

    /**
     * Sets the value of field 'dsIndLancamentoPersonalizado'.
     * 
     * @param dsIndLancamentoPersonalizado the value of field
     * 'dsIndLancamentoPersonalizado'.
     */
    public void setDsIndLancamentoPersonalizado(java.lang.String dsIndLancamentoPersonalizado)
    {
        this._dsIndLancamentoPersonalizado = dsIndLancamentoPersonalizado;
    } //-- void setDsIndLancamentoPersonalizado(java.lang.String) 

    /**
     * Sets the value of field 'dsIndicador'.
     * 
     * @param dsIndicador the value of field 'dsIndicador'.
     */
    public void setDsIndicador(java.lang.String dsIndicador)
    {
        this._dsIndicador = dsIndicador;
    } //-- void setDsIndicador(java.lang.String) 

    /**
     * Sets the value of field 'dsIndicadorAdesaoSacador'.
     * 
     * @param dsIndicadorAdesaoSacador the value of field
     * 'dsIndicadorAdesaoSacador'.
     */
    public void setDsIndicadorAdesaoSacador(java.lang.String dsIndicadorAdesaoSacador)
    {
        this._dsIndicadorAdesaoSacador = dsIndicadorAdesaoSacador;
    } //-- void setDsIndicadorAdesaoSacador(java.lang.String) 

    /**
     * Sets the value of field 'dsIndicadorAgendaGrade'.
     * 
     * @param dsIndicadorAgendaGrade the value of field
     * 'dsIndicadorAgendaGrade'.
     */
    public void setDsIndicadorAgendaGrade(java.lang.String dsIndicadorAgendaGrade)
    {
        this._dsIndicadorAgendaGrade = dsIndicadorAgendaGrade;
    } //-- void setDsIndicadorAgendaGrade(java.lang.String) 

    /**
     * Sets the value of field 'dsIndicadorAgendaTitulo'.
     * 
     * @param dsIndicadorAgendaTitulo the value of field
     * 'dsIndicadorAgendaTitulo'.
     */
    public void setDsIndicadorAgendaTitulo(java.lang.String dsIndicadorAgendaTitulo)
    {
        this._dsIndicadorAgendaTitulo = dsIndicadorAgendaTitulo;
    } //-- void setDsIndicadorAgendaTitulo(java.lang.String) 

    /**
     * Sets the value of field 'dsIndicadorAutorizacaoCliente'.
     * 
     * @param dsIndicadorAutorizacaoCliente the value of field
     * 'dsIndicadorAutorizacaoCliente'.
     */
    public void setDsIndicadorAutorizacaoCliente(java.lang.String dsIndicadorAutorizacaoCliente)
    {
        this._dsIndicadorAutorizacaoCliente = dsIndicadorAutorizacaoCliente;
    } //-- void setDsIndicadorAutorizacaoCliente(java.lang.String) 

    /**
     * Sets the value of field 'dsIndicadorAutorizacaoComplemento'.
     * 
     * @param dsIndicadorAutorizacaoComplemento the value of field
     * 'dsIndicadorAutorizacaoComplemento'.
     */
    public void setDsIndicadorAutorizacaoComplemento(java.lang.String dsIndicadorAutorizacaoComplemento)
    {
        this._dsIndicadorAutorizacaoComplemento = dsIndicadorAutorizacaoComplemento;
    } //-- void setDsIndicadorAutorizacaoComplemento(java.lang.String) 

    /**
     * Sets the value of field 'dsIndicadorBancoPostal'.
     * 
     * @param dsIndicadorBancoPostal the value of field
     * 'dsIndicadorBancoPostal'.
     */
    public void setDsIndicadorBancoPostal(java.lang.String dsIndicadorBancoPostal)
    {
        this._dsIndicadorBancoPostal = dsIndicadorBancoPostal;
    } //-- void setDsIndicadorBancoPostal(java.lang.String) 

    /**
     * Sets the value of field 'dsIndicadorCadastroOrg'.
     * 
     * @param dsIndicadorCadastroOrg the value of field
     * 'dsIndicadorCadastroOrg'.
     */
    public void setDsIndicadorCadastroOrg(java.lang.String dsIndicadorCadastroOrg)
    {
        this._dsIndicadorCadastroOrg = dsIndicadorCadastroOrg;
    } //-- void setDsIndicadorCadastroOrg(java.lang.String) 

    /**
     * Sets the value of field 'dsIndicadorCadastroProcd'.
     * 
     * @param dsIndicadorCadastroProcd the value of field
     * 'dsIndicadorCadastroProcd'.
     */
    public void setDsIndicadorCadastroProcd(java.lang.String dsIndicadorCadastroProcd)
    {
        this._dsIndicadorCadastroProcd = dsIndicadorCadastroProcd;
    } //-- void setDsIndicadorCadastroProcd(java.lang.String) 

    /**
     * Sets the value of field 'dsIndicadorCataoSalario'.
     * 
     * @param dsIndicadorCataoSalario the value of field
     * 'dsIndicadorCataoSalario'.
     */
    public void setDsIndicadorCataoSalario(java.lang.String dsIndicadorCataoSalario)
    {
        this._dsIndicadorCataoSalario = dsIndicadorCataoSalario;
    } //-- void setDsIndicadorCataoSalario(java.lang.String) 

    /**
     * Sets the value of field 'dsIndicadorEconomicoReajuste'.
     * 
     * @param dsIndicadorEconomicoReajuste the value of field
     * 'dsIndicadorEconomicoReajuste'.
     */
    public void setDsIndicadorEconomicoReajuste(java.lang.String dsIndicadorEconomicoReajuste)
    {
        this._dsIndicadorEconomicoReajuste = dsIndicadorEconomicoReajuste;
    } //-- void setDsIndicadorEconomicoReajuste(java.lang.String) 

    /**
     * Sets the value of field 'dsIndicadorEmissaoAviso'.
     * 
     * @param dsIndicadorEmissaoAviso the value of field
     * 'dsIndicadorEmissaoAviso'.
     */
    public void setDsIndicadorEmissaoAviso(java.lang.String dsIndicadorEmissaoAviso)
    {
        this._dsIndicadorEmissaoAviso = dsIndicadorEmissaoAviso;
    } //-- void setDsIndicadorEmissaoAviso(java.lang.String) 

    /**
     * Sets the value of field 'dsIndicadorExpiracaoCredito'.
     * 
     * @param dsIndicadorExpiracaoCredito the value of field
     * 'dsIndicadorExpiracaoCredito'.
     */
    public void setDsIndicadorExpiracaoCredito(java.lang.String dsIndicadorExpiracaoCredito)
    {
        this._dsIndicadorExpiracaoCredito = dsIndicadorExpiracaoCredito;
    } //-- void setDsIndicadorExpiracaoCredito(java.lang.String) 

    /**
     * Sets the value of field 'dsIndicadorLancamentoPagamento'.
     * 
     * @param dsIndicadorLancamentoPagamento the value of field
     * 'dsIndicadorLancamentoPagamento'.
     */
    public void setDsIndicadorLancamentoPagamento(java.lang.String dsIndicadorLancamentoPagamento)
    {
        this._dsIndicadorLancamentoPagamento = dsIndicadorLancamentoPagamento;
    } //-- void setDsIndicadorLancamentoPagamento(java.lang.String) 

    /**
     * Sets the value of field 'dsIndicadorListaDebito'.
     * 
     * @param dsIndicadorListaDebito the value of field
     * 'dsIndicadorListaDebito'.
     */
    public void setDsIndicadorListaDebito(java.lang.String dsIndicadorListaDebito)
    {
        this._dsIndicadorListaDebito = dsIndicadorListaDebito;
    } //-- void setDsIndicadorListaDebito(java.lang.String) 

    /**
     * Sets the value of field 'dsIndicadorMensagemPerso'.
     * 
     * @param dsIndicadorMensagemPerso the value of field
     * 'dsIndicadorMensagemPerso'.
     */
    public void setDsIndicadorMensagemPerso(java.lang.String dsIndicadorMensagemPerso)
    {
        this._dsIndicadorMensagemPerso = dsIndicadorMensagemPerso;
    } //-- void setDsIndicadorMensagemPerso(java.lang.String) 

    /**
     * Sets the value of field 'dsIndicadorRetornoInternet'.
     * 
     * @param dsIndicadorRetornoInternet the value of field
     * 'dsIndicadorRetornoInternet'.
     */
    public void setDsIndicadorRetornoInternet(java.lang.String dsIndicadorRetornoInternet)
    {
        this._dsIndicadorRetornoInternet = dsIndicadorRetornoInternet;
    } //-- void setDsIndicadorRetornoInternet(java.lang.String) 

    /**
     * Sets the value of field 'dsIndicadorSegundaLinha'.
     * 
     * @param dsIndicadorSegundaLinha the value of field
     * 'dsIndicadorSegundaLinha'.
     */
    public void setDsIndicadorSegundaLinha(java.lang.String dsIndicadorSegundaLinha)
    {
        this._dsIndicadorSegundaLinha = dsIndicadorSegundaLinha;
    } //-- void setDsIndicadorSegundaLinha(java.lang.String) 

    /**
     * Sets the value of field 'dsIndicadorTipoRetornoInternet'.
     * 
     * @param dsIndicadorTipoRetornoInternet the value of field
     * 'dsIndicadorTipoRetornoInternet'.
     */
    public void setDsIndicadorTipoRetornoInternet(java.lang.String dsIndicadorTipoRetornoInternet)
    {
        this._dsIndicadorTipoRetornoInternet = dsIndicadorTipoRetornoInternet;
    } //-- void setDsIndicadorTipoRetornoInternet(java.lang.String) 

    /**
     * Sets the value of field 'dsIndicadorUtilizaMora'.
     * 
     * @param dsIndicadorUtilizaMora the value of field
     * 'dsIndicadorUtilizaMora'.
     */
    public void setDsIndicadorUtilizaMora(java.lang.String dsIndicadorUtilizaMora)
    {
        this._dsIndicadorUtilizaMora = dsIndicadorUtilizaMora;
    } //-- void setDsIndicadorUtilizaMora(java.lang.String) 

    /**
     * Sets the value of field 'dsLancamentoFuturoCredito'.
     * 
     * @param dsLancamentoFuturoCredito the value of field
     * 'dsLancamentoFuturoCredito'.
     */
    public void setDsLancamentoFuturoCredito(java.lang.String dsLancamentoFuturoCredito)
    {
        this._dsLancamentoFuturoCredito = dsLancamentoFuturoCredito;
    } //-- void setDsLancamentoFuturoCredito(java.lang.String) 

    /**
     * Sets the value of field 'dsLancamentoFuturoDebito'.
     * 
     * @param dsLancamentoFuturoDebito the value of field
     * 'dsLancamentoFuturoDebito'.
     */
    public void setDsLancamentoFuturoDebito(java.lang.String dsLancamentoFuturoDebito)
    {
        this._dsLancamentoFuturoDebito = dsLancamentoFuturoDebito;
    } //-- void setDsLancamentoFuturoDebito(java.lang.String) 

    /**
     * Sets the value of field 'dsLiberacaoLoteProcesso'.
     * 
     * @param dsLiberacaoLoteProcesso the value of field
     * 'dsLiberacaoLoteProcesso'.
     */
    public void setDsLiberacaoLoteProcesso(java.lang.String dsLiberacaoLoteProcesso)
    {
        this._dsLiberacaoLoteProcesso = dsLiberacaoLoteProcesso;
    } //-- void setDsLiberacaoLoteProcesso(java.lang.String) 

    /**
     * Sets the value of field 'dsLocalEmissao'.
     * 
     * @param dsLocalEmissao the value of field 'dsLocalEmissao'.
     */
    public void setDsLocalEmissao(java.lang.String dsLocalEmissao)
    {
        this._dsLocalEmissao = dsLocalEmissao;
    } //-- void setDsLocalEmissao(java.lang.String) 

    /**
     * Sets the value of field 'dsManutencaoBaseRecadastro'.
     * 
     * @param dsManutencaoBaseRecadastro the value of field
     * 'dsManutencaoBaseRecadastro'.
     */
    public void setDsManutencaoBaseRecadastro(java.lang.String dsManutencaoBaseRecadastro)
    {
        this._dsManutencaoBaseRecadastro = dsManutencaoBaseRecadastro;
    } //-- void setDsManutencaoBaseRecadastro(java.lang.String) 

    /**
     * Sets the value of field 'dsMeioPagamentoCredito'.
     * 
     * @param dsMeioPagamentoCredito the value of field
     * 'dsMeioPagamentoCredito'.
     */
    public void setDsMeioPagamentoCredito(java.lang.String dsMeioPagamentoCredito)
    {
        this._dsMeioPagamentoCredito = dsMeioPagamentoCredito;
    } //-- void setDsMeioPagamentoCredito(java.lang.String) 

    /**
     * Sets the value of field 'dsMensagemRecadastroMidia'.
     * 
     * @param dsMensagemRecadastroMidia the value of field
     * 'dsMensagemRecadastroMidia'.
     */
    public void setDsMensagemRecadastroMidia(java.lang.String dsMensagemRecadastroMidia)
    {
        this._dsMensagemRecadastroMidia = dsMensagemRecadastroMidia;
    } //-- void setDsMensagemRecadastroMidia(java.lang.String) 

    /**
     * Sets the value of field 'dsMidiaDisponivel'.
     * 
     * @param dsMidiaDisponivel the value of field
     * 'dsMidiaDisponivel'.
     */
    public void setDsMidiaDisponivel(java.lang.String dsMidiaDisponivel)
    {
        this._dsMidiaDisponivel = dsMidiaDisponivel;
    } //-- void setDsMidiaDisponivel(java.lang.String) 

    /**
     * Sets the value of field 'dsMidiaMensagemRecadastro'.
     * 
     * @param dsMidiaMensagemRecadastro the value of field
     * 'dsMidiaMensagemRecadastro'.
     */
    public void setDsMidiaMensagemRecadastro(java.lang.String dsMidiaMensagemRecadastro)
    {
        this._dsMidiaMensagemRecadastro = dsMidiaMensagemRecadastro;
    } //-- void setDsMidiaMensagemRecadastro(java.lang.String) 

    /**
     * Sets the value of field 'dsMomentoAvisoRacadastro'.
     * 
     * @param dsMomentoAvisoRacadastro the value of field
     * 'dsMomentoAvisoRacadastro'.
     */
    public void setDsMomentoAvisoRacadastro(java.lang.String dsMomentoAvisoRacadastro)
    {
        this._dsMomentoAvisoRacadastro = dsMomentoAvisoRacadastro;
    } //-- void setDsMomentoAvisoRacadastro(java.lang.String) 

    /**
     * Sets the value of field 'dsMomentoCreditoEfetivacao'.
     * 
     * @param dsMomentoCreditoEfetivacao the value of field
     * 'dsMomentoCreditoEfetivacao'.
     */
    public void setDsMomentoCreditoEfetivacao(java.lang.String dsMomentoCreditoEfetivacao)
    {
        this._dsMomentoCreditoEfetivacao = dsMomentoCreditoEfetivacao;
    } //-- void setDsMomentoCreditoEfetivacao(java.lang.String) 

    /**
     * Sets the value of field 'dsMomentoDebitoPagamento'.
     * 
     * @param dsMomentoDebitoPagamento the value of field
     * 'dsMomentoDebitoPagamento'.
     */
    public void setDsMomentoDebitoPagamento(java.lang.String dsMomentoDebitoPagamento)
    {
        this._dsMomentoDebitoPagamento = dsMomentoDebitoPagamento;
    } //-- void setDsMomentoDebitoPagamento(java.lang.String) 

    /**
     * Sets the value of field 'dsMomentoFormularioRecadastro'.
     * 
     * @param dsMomentoFormularioRecadastro the value of field
     * 'dsMomentoFormularioRecadastro'.
     */
    public void setDsMomentoFormularioRecadastro(java.lang.String dsMomentoFormularioRecadastro)
    {
        this._dsMomentoFormularioRecadastro = dsMomentoFormularioRecadastro;
    } //-- void setDsMomentoFormularioRecadastro(java.lang.String) 

    /**
     * Sets the value of field 'dsMomentoProcessamentoPagamento'.
     * 
     * @param dsMomentoProcessamentoPagamento the value of field
     * 'dsMomentoProcessamentoPagamento'.
     */
    public void setDsMomentoProcessamentoPagamento(java.lang.String dsMomentoProcessamentoPagamento)
    {
        this._dsMomentoProcessamentoPagamento = dsMomentoProcessamentoPagamento;
    } //-- void setDsMomentoProcessamentoPagamento(java.lang.String) 

    /**
     * Sets the value of field 'dsNaturezaOperacaoPagamento'.
     * 
     * @param dsNaturezaOperacaoPagamento the value of field
     * 'dsNaturezaOperacaoPagamento'.
     */
    public void setDsNaturezaOperacaoPagamento(java.lang.String dsNaturezaOperacaoPagamento)
    {
        this._dsNaturezaOperacaoPagamento = dsNaturezaOperacaoPagamento;
    } //-- void setDsNaturezaOperacaoPagamento(java.lang.String) 

    /**
     * Sets the value of field 'dsOrigemIndicador'.
     * 
     * @param dsOrigemIndicador the value of field
     * 'dsOrigemIndicador'.
     */
    public void setDsOrigemIndicador(int dsOrigemIndicador)
    {
        this._dsOrigemIndicador = dsOrigemIndicador;
        this._has_dsOrigemIndicador = true;
    } //-- void setDsOrigemIndicador(int) 

    /**
     * Sets the value of field 'dsPagamentoNaoUtilizado'.
     * 
     * @param dsPagamentoNaoUtilizado the value of field
     * 'dsPagamentoNaoUtilizado'.
     */
    public void setDsPagamentoNaoUtilizado(java.lang.String dsPagamentoNaoUtilizado)
    {
        this._dsPagamentoNaoUtilizado = dsPagamentoNaoUtilizado;
    } //-- void setDsPagamentoNaoUtilizado(java.lang.String) 

    /**
     * Sets the value of field 'dsPerdcCobrancaTarifa'.
     * 
     * @param dsPerdcCobrancaTarifa the value of field
     * 'dsPerdcCobrancaTarifa'.
     */
    public void setDsPerdcCobrancaTarifa(java.lang.String dsPerdcCobrancaTarifa)
    {
        this._dsPerdcCobrancaTarifa = dsPerdcCobrancaTarifa;
    } //-- void setDsPerdcCobrancaTarifa(java.lang.String) 

    /**
     * Sets the value of field 'dsPerdcComprovante'.
     * 
     * @param dsPerdcComprovante the value of field
     * 'dsPerdcComprovante'.
     */
    public void setDsPerdcComprovante(java.lang.String dsPerdcComprovante)
    {
        this._dsPerdcComprovante = dsPerdcComprovante;
    } //-- void setDsPerdcComprovante(java.lang.String) 

    /**
     * Sets the value of field 'dsPerdcConsultaVeiculo'.
     * 
     * @param dsPerdcConsultaVeiculo the value of field
     * 'dsPerdcConsultaVeiculo'.
     */
    public void setDsPerdcConsultaVeiculo(java.lang.String dsPerdcConsultaVeiculo)
    {
        this._dsPerdcConsultaVeiculo = dsPerdcConsultaVeiculo;
    } //-- void setDsPerdcConsultaVeiculo(java.lang.String) 

    /**
     * Sets the value of field 'dsPerdcEnvioRemessa'.
     * 
     * @param dsPerdcEnvioRemessa the value of field
     * 'dsPerdcEnvioRemessa'.
     */
    public void setDsPerdcEnvioRemessa(java.lang.String dsPerdcEnvioRemessa)
    {
        this._dsPerdcEnvioRemessa = dsPerdcEnvioRemessa;
    } //-- void setDsPerdcEnvioRemessa(java.lang.String) 

    /**
     * Sets the value of field 'dsPerdcManutencaoProcd'.
     * 
     * @param dsPerdcManutencaoProcd the value of field
     * 'dsPerdcManutencaoProcd'.
     */
    public void setDsPerdcManutencaoProcd(java.lang.String dsPerdcManutencaoProcd)
    {
        this._dsPerdcManutencaoProcd = dsPerdcManutencaoProcd;
    } //-- void setDsPerdcManutencaoProcd(java.lang.String) 

    /**
     * Sets the value of field 'dsPeriodicidadeAviso'.
     * 
     * @param dsPeriodicidadeAviso the value of field
     * 'dsPeriodicidadeAviso'.
     */
    public void setDsPeriodicidadeAviso(java.lang.String dsPeriodicidadeAviso)
    {
        this._dsPeriodicidadeAviso = dsPeriodicidadeAviso;
    } //-- void setDsPeriodicidadeAviso(java.lang.String) 

    /**
     * Sets the value of field 'dsPermissaoDebitoOnline'.
     * 
     * @param dsPermissaoDebitoOnline the value of field
     * 'dsPermissaoDebitoOnline'.
     */
    public void setDsPermissaoDebitoOnline(java.lang.String dsPermissaoDebitoOnline)
    {
        this._dsPermissaoDebitoOnline = dsPermissaoDebitoOnline;
    } //-- void setDsPermissaoDebitoOnline(java.lang.String) 

    /**
     * Sets the value of field
     * 'dsPreenchimentoLancamentoPersonalizado'.
     * 
     * @param dsPreenchimentoLancamentoPersonalizado the value of
     * field 'dsPreenchimentoLancamentoPersonalizado'.
     */
    public void setDsPreenchimentoLancamentoPersonalizado(java.lang.String dsPreenchimentoLancamentoPersonalizado)
    {
        this._dsPreenchimentoLancamentoPersonalizado = dsPreenchimentoLancamentoPersonalizado;
    } //-- void setDsPreenchimentoLancamentoPersonalizado(java.lang.String) 

    /**
     * Sets the value of field 'dsPrincipalEnquaRecadastro'.
     * 
     * @param dsPrincipalEnquaRecadastro the value of field
     * 'dsPrincipalEnquaRecadastro'.
     */
    public void setDsPrincipalEnquaRecadastro(java.lang.String dsPrincipalEnquaRecadastro)
    {
        this._dsPrincipalEnquaRecadastro = dsPrincipalEnquaRecadastro;
    } //-- void setDsPrincipalEnquaRecadastro(java.lang.String) 

    /**
     * Sets the value of field 'dsPrioridadeEfetivacaoPagamento'.
     * 
     * @param dsPrioridadeEfetivacaoPagamento the value of field
     * 'dsPrioridadeEfetivacaoPagamento'.
     */
    public void setDsPrioridadeEfetivacaoPagamento(java.lang.String dsPrioridadeEfetivacaoPagamento)
    {
        this._dsPrioridadeEfetivacaoPagamento = dsPrioridadeEfetivacaoPagamento;
    } //-- void setDsPrioridadeEfetivacaoPagamento(java.lang.String) 

    /**
     * Sets the value of field 'dsRastreabilidadeNotaFiscal'.
     * 
     * @param dsRastreabilidadeNotaFiscal the value of field
     * 'dsRastreabilidadeNotaFiscal'.
     */
    public void setDsRastreabilidadeNotaFiscal(java.lang.String dsRastreabilidadeNotaFiscal)
    {
        this._dsRastreabilidadeNotaFiscal = dsRastreabilidadeNotaFiscal;
    } //-- void setDsRastreabilidadeNotaFiscal(java.lang.String) 

    /**
     * Sets the value of field 'dsRastreabilidadeTituloTerceiro'.
     * 
     * @param dsRastreabilidadeTituloTerceiro the value of field
     * 'dsRastreabilidadeTituloTerceiro'.
     */
    public void setDsRastreabilidadeTituloTerceiro(java.lang.String dsRastreabilidadeTituloTerceiro)
    {
        this._dsRastreabilidadeTituloTerceiro = dsRastreabilidadeTituloTerceiro;
    } //-- void setDsRastreabilidadeTituloTerceiro(java.lang.String) 

    /**
     * Sets the value of field 'dsRejeicaoAgendaLote'.
     * 
     * @param dsRejeicaoAgendaLote the value of field
     * 'dsRejeicaoAgendaLote'.
     */
    public void setDsRejeicaoAgendaLote(java.lang.String dsRejeicaoAgendaLote)
    {
        this._dsRejeicaoAgendaLote = dsRejeicaoAgendaLote;
    } //-- void setDsRejeicaoAgendaLote(java.lang.String) 

    /**
     * Sets the value of field 'dsRejeicaoEfetivacaoLote'.
     * 
     * @param dsRejeicaoEfetivacaoLote the value of field
     * 'dsRejeicaoEfetivacaoLote'.
     */
    public void setDsRejeicaoEfetivacaoLote(java.lang.String dsRejeicaoEfetivacaoLote)
    {
        this._dsRejeicaoEfetivacaoLote = dsRejeicaoEfetivacaoLote;
    } //-- void setDsRejeicaoEfetivacaoLote(java.lang.String) 

    /**
     * Sets the value of field 'dsRejeicaoLote'.
     * 
     * @param dsRejeicaoLote the value of field 'dsRejeicaoLote'.
     */
    public void setDsRejeicaoLote(java.lang.String dsRejeicaoLote)
    {
        this._dsRejeicaoLote = dsRejeicaoLote;
    } //-- void setDsRejeicaoLote(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoCargaRecadastro'.
     * 
     * @param dsTipoCargaRecadastro the value of field
     * 'dsTipoCargaRecadastro'.
     */
    public void setDsTipoCargaRecadastro(java.lang.String dsTipoCargaRecadastro)
    {
        this._dsTipoCargaRecadastro = dsTipoCargaRecadastro;
    } //-- void setDsTipoCargaRecadastro(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoCataoSalario'.
     * 
     * @param dsTipoCataoSalario the value of field
     * 'dsTipoCataoSalario'.
     */
    public void setDsTipoCataoSalario(java.lang.String dsTipoCataoSalario)
    {
        this._dsTipoCataoSalario = dsTipoCataoSalario;
    } //-- void setDsTipoCataoSalario(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoConsistenciaFavorecido'.
     * 
     * @param dsTipoConsistenciaFavorecido the value of field
     * 'dsTipoConsistenciaFavorecido'.
     */
    public void setDsTipoConsistenciaFavorecido(java.lang.String dsTipoConsistenciaFavorecido)
    {
        this._dsTipoConsistenciaFavorecido = dsTipoConsistenciaFavorecido;
    } //-- void setDsTipoConsistenciaFavorecido(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoConsistenciaLista'.
     * 
     * @param dsTipoConsistenciaLista the value of field
     * 'dsTipoConsistenciaLista'.
     */
    public void setDsTipoConsistenciaLista(java.lang.String dsTipoConsistenciaLista)
    {
        this._dsTipoConsistenciaLista = dsTipoConsistenciaLista;
    } //-- void setDsTipoConsistenciaLista(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoConsolidacaoComprovante'.
     * 
     * @param dsTipoConsolidacaoComprovante the value of field
     * 'dsTipoConsolidacaoComprovante'.
     */
    public void setDsTipoConsolidacaoComprovante(java.lang.String dsTipoConsolidacaoComprovante)
    {
        this._dsTipoConsolidacaoComprovante = dsTipoConsolidacaoComprovante;
    } //-- void setDsTipoConsolidacaoComprovante(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoDataFloat'.
     * 
     * @param dsTipoDataFloat the value of field 'dsTipoDataFloat'.
     */
    public void setDsTipoDataFloat(java.lang.String dsTipoDataFloat)
    {
        this._dsTipoDataFloat = dsTipoDataFloat;
    } //-- void setDsTipoDataFloat(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoDivergenciaVeiculo'.
     * 
     * @param dsTipoDivergenciaVeiculo the value of field
     * 'dsTipoDivergenciaVeiculo'.
     */
    public void setDsTipoDivergenciaVeiculo(java.lang.String dsTipoDivergenciaVeiculo)
    {
        this._dsTipoDivergenciaVeiculo = dsTipoDivergenciaVeiculo;
    } //-- void setDsTipoDivergenciaVeiculo(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoFormacaoLista'.
     * 
     * @param dsTipoFormacaoLista the value of field
     * 'dsTipoFormacaoLista'.
     */
    public void setDsTipoFormacaoLista(java.lang.String dsTipoFormacaoLista)
    {
        this._dsTipoFormacaoLista = dsTipoFormacaoLista;
    } //-- void setDsTipoFormacaoLista(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoIdBeneficio'.
     * 
     * @param dsTipoIdBeneficio the value of field
     * 'dsTipoIdBeneficio'.
     */
    public void setDsTipoIdBeneficio(java.lang.String dsTipoIdBeneficio)
    {
        this._dsTipoIdBeneficio = dsTipoIdBeneficio;
    } //-- void setDsTipoIdBeneficio(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoIscricaoFavorecido'.
     * 
     * @param dsTipoIscricaoFavorecido the value of field
     * 'dsTipoIscricaoFavorecido'.
     */
    public void setDsTipoIscricaoFavorecido(java.lang.String dsTipoIscricaoFavorecido)
    {
        this._dsTipoIscricaoFavorecido = dsTipoIscricaoFavorecido;
    } //-- void setDsTipoIscricaoFavorecido(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoLayoutArquivo'.
     * 
     * @param dsTipoLayoutArquivo the value of field
     * 'dsTipoLayoutArquivo'.
     */
    public void setDsTipoLayoutArquivo(java.lang.String dsTipoLayoutArquivo)
    {
        this._dsTipoLayoutArquivo = dsTipoLayoutArquivo;
    } //-- void setDsTipoLayoutArquivo(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoReajusteTarifa'.
     * 
     * @param dsTipoReajusteTarifa the value of field
     * 'dsTipoReajusteTarifa'.
     */
    public void setDsTipoReajusteTarifa(java.lang.String dsTipoReajusteTarifa)
    {
        this._dsTipoReajusteTarifa = dsTipoReajusteTarifa;
    } //-- void setDsTipoReajusteTarifa(java.lang.String) 

    /**
     * Sets the value of field 'dsTituloDdaRetorno'.
     * 
     * @param dsTituloDdaRetorno the value of field
     * 'dsTituloDdaRetorno'.
     */
    public void setDsTituloDdaRetorno(java.lang.String dsTituloDdaRetorno)
    {
        this._dsTituloDdaRetorno = dsTituloDdaRetorno;
    } //-- void setDsTituloDdaRetorno(java.lang.String) 

    /**
     * Sets the value of field 'dsUtilizacaoFavorecidoControle'.
     * 
     * @param dsUtilizacaoFavorecidoControle the value of field
     * 'dsUtilizacaoFavorecidoControle'.
     */
    public void setDsUtilizacaoFavorecidoControle(java.lang.String dsUtilizacaoFavorecidoControle)
    {
        this._dsUtilizacaoFavorecidoControle = dsUtilizacaoFavorecidoControle;
    } //-- void setDsUtilizacaoFavorecidoControle(java.lang.String) 

    /**
     * Sets the value of field 'dsValidacaoNomeFavorecido'.
     * 
     * @param dsValidacaoNomeFavorecido the value of field
     * 'dsValidacaoNomeFavorecido'.
     */
    public void setDsValidacaoNomeFavorecido(java.lang.String dsValidacaoNomeFavorecido)
    {
        this._dsValidacaoNomeFavorecido = dsValidacaoNomeFavorecido;
    } //-- void setDsValidacaoNomeFavorecido(java.lang.String) 

    /**
     * Sets the value of field 'dtEnquaContaSalario'.
     * 
     * @param dtEnquaContaSalario the value of field
     * 'dtEnquaContaSalario'.
     */
    public void setDtEnquaContaSalario(java.lang.String dtEnquaContaSalario)
    {
        this._dtEnquaContaSalario = dtEnquaContaSalario;
    } //-- void setDtEnquaContaSalario(java.lang.String) 

    /**
     * Sets the value of field 'dtFimAcertoRecadastro'.
     * 
     * @param dtFimAcertoRecadastro the value of field
     * 'dtFimAcertoRecadastro'.
     */
    public void setDtFimAcertoRecadastro(java.lang.String dtFimAcertoRecadastro)
    {
        this._dtFimAcertoRecadastro = dtFimAcertoRecadastro;
    } //-- void setDtFimAcertoRecadastro(java.lang.String) 

    /**
     * Sets the value of field 'dtFimRecadastroBeneficio'.
     * 
     * @param dtFimRecadastroBeneficio the value of field
     * 'dtFimRecadastroBeneficio'.
     */
    public void setDtFimRecadastroBeneficio(java.lang.String dtFimRecadastroBeneficio)
    {
        this._dtFimRecadastroBeneficio = dtFimRecadastroBeneficio;
    } //-- void setDtFimRecadastroBeneficio(java.lang.String) 

    /**
     * Sets the value of field 'dtInicioBloqueioPplta'.
     * 
     * @param dtInicioBloqueioPplta the value of field
     * 'dtInicioBloqueioPplta'.
     */
    public void setDtInicioBloqueioPplta(java.lang.String dtInicioBloqueioPplta)
    {
        this._dtInicioBloqueioPplta = dtInicioBloqueioPplta;
    } //-- void setDtInicioBloqueioPplta(java.lang.String) 

    /**
     * Sets the value of field 'dtInicioRastreabilidadeTitulo'.
     * 
     * @param dtInicioRastreabilidadeTitulo the value of field
     * 'dtInicioRastreabilidadeTitulo'.
     */
    public void setDtInicioRastreabilidadeTitulo(java.lang.String dtInicioRastreabilidadeTitulo)
    {
        this._dtInicioRastreabilidadeTitulo = dtInicioRastreabilidadeTitulo;
    } //-- void setDtInicioRastreabilidadeTitulo(java.lang.String) 

    /**
     * Sets the value of field 'dtInicioRecadastroBeneficio'.
     * 
     * @param dtInicioRecadastroBeneficio the value of field
     * 'dtInicioRecadastroBeneficio'.
     */
    public void setDtInicioRecadastroBeneficio(java.lang.String dtInicioRecadastroBeneficio)
    {
        this._dtInicioRecadastroBeneficio = dtInicioRecadastroBeneficio;
    } //-- void setDtInicioRecadastroBeneficio(java.lang.String) 

    /**
     * Sets the value of field 'dtLimiteVinculoCarga'.
     * 
     * @param dtLimiteVinculoCarga the value of field
     * 'dtLimiteVinculoCarga'.
     */
    public void setDtLimiteVinculoCarga(java.lang.String dtLimiteVinculoCarga)
    {
        this._dtLimiteVinculoCarga = dtLimiteVinculoCarga;
    } //-- void setDtLimiteVinculoCarga(java.lang.String) 

    /**
     * Sets the value of field 'dtinicioAcertoRecadastro'.
     * 
     * @param dtinicioAcertoRecadastro the value of field
     * 'dtinicioAcertoRecadastro'.
     */
    public void setDtinicioAcertoRecadastro(java.lang.String dtinicioAcertoRecadastro)
    {
        this._dtinicioAcertoRecadastro = dtinicioAcertoRecadastro;
    } //-- void setDtinicioAcertoRecadastro(java.lang.String) 

    /**
     * Sets the value of field 'hrManutencaoRegistroAlteracao'.
     * 
     * @param hrManutencaoRegistroAlteracao the value of field
     * 'hrManutencaoRegistroAlteracao'.
     */
    public void setHrManutencaoRegistroAlteracao(java.lang.String hrManutencaoRegistroAlteracao)
    {
        this._hrManutencaoRegistroAlteracao = hrManutencaoRegistroAlteracao;
    } //-- void setHrManutencaoRegistroAlteracao(java.lang.String) 

    /**
     * Sets the value of field 'hrManutencaoRegistroInclusao'.
     * 
     * @param hrManutencaoRegistroInclusao the value of field
     * 'hrManutencaoRegistroInclusao'.
     */
    public void setHrManutencaoRegistroInclusao(java.lang.String hrManutencaoRegistroInclusao)
    {
        this._hrManutencaoRegistroInclusao = hrManutencaoRegistroInclusao;
    } //-- void setHrManutencaoRegistroInclusao(java.lang.String) 

    /**
     * Sets the value of field 'mensagem'.
     * 
     * @param mensagem the value of field 'mensagem'.
     */
    public void setMensagem(java.lang.String mensagem)
    {
        this._mensagem = mensagem;
    } //-- void setMensagem(java.lang.String) 

    /**
     * Sets the value of field 'nmOperacaoFluxoAlteracao'.
     * 
     * @param nmOperacaoFluxoAlteracao the value of field
     * 'nmOperacaoFluxoAlteracao'.
     */
    public void setNmOperacaoFluxoAlteracao(java.lang.String nmOperacaoFluxoAlteracao)
    {
        this._nmOperacaoFluxoAlteracao = nmOperacaoFluxoAlteracao;
    } //-- void setNmOperacaoFluxoAlteracao(java.lang.String) 

    /**
     * Sets the value of field 'nmOperacaoFluxoInclusao'.
     * 
     * @param nmOperacaoFluxoInclusao the value of field
     * 'nmOperacaoFluxoInclusao'.
     */
    public void setNmOperacaoFluxoInclusao(java.lang.String nmOperacaoFluxoInclusao)
    {
        this._nmOperacaoFluxoInclusao = nmOperacaoFluxoInclusao;
    } //-- void setNmOperacaoFluxoInclusao(java.lang.String) 

    /**
     * Sets the value of field 'nrFechamentoApuracaoTarifa'.
     * 
     * @param nrFechamentoApuracaoTarifa the value of field
     * 'nrFechamentoApuracaoTarifa'.
     */
    public void setNrFechamentoApuracaoTarifa(int nrFechamentoApuracaoTarifa)
    {
        this._nrFechamentoApuracaoTarifa = nrFechamentoApuracaoTarifa;
        this._has_nrFechamentoApuracaoTarifa = true;
    } //-- void setNrFechamentoApuracaoTarifa(int) 

    /**
     * Sets the value of field 'qtAntecedencia'.
     * 
     * @param qtAntecedencia the value of field 'qtAntecedencia'.
     */
    public void setQtAntecedencia(int qtAntecedencia)
    {
        this._qtAntecedencia = qtAntecedencia;
        this._has_qtAntecedencia = true;
    } //-- void setQtAntecedencia(int) 

    /**
     * Sets the value of field 'qtAnteriorVencimentoComprovado'.
     * 
     * @param qtAnteriorVencimentoComprovado the value of field
     * 'qtAnteriorVencimentoComprovado'.
     */
    public void setQtAnteriorVencimentoComprovado(int qtAnteriorVencimentoComprovado)
    {
        this._qtAnteriorVencimentoComprovado = qtAnteriorVencimentoComprovado;
        this._has_qtAnteriorVencimentoComprovado = true;
    } //-- void setQtAnteriorVencimentoComprovado(int) 

    /**
     * Sets the value of field 'qtDiaCobrancaTarifa'.
     * 
     * @param qtDiaCobrancaTarifa the value of field
     * 'qtDiaCobrancaTarifa'.
     */
    public void setQtDiaCobrancaTarifa(int qtDiaCobrancaTarifa)
    {
        this._qtDiaCobrancaTarifa = qtDiaCobrancaTarifa;
        this._has_qtDiaCobrancaTarifa = true;
    } //-- void setQtDiaCobrancaTarifa(int) 

    /**
     * Sets the value of field 'qtDiaExpiracao'.
     * 
     * @param qtDiaExpiracao the value of field 'qtDiaExpiracao'.
     */
    public void setQtDiaExpiracao(int qtDiaExpiracao)
    {
        this._qtDiaExpiracao = qtDiaExpiracao;
        this._has_qtDiaExpiracao = true;
    } //-- void setQtDiaExpiracao(int) 

    /**
     * Sets the value of field 'qtDiaInatividadeFavorecido'.
     * 
     * @param qtDiaInatividadeFavorecido the value of field
     * 'qtDiaInatividadeFavorecido'.
     */
    public void setQtDiaInatividadeFavorecido(int qtDiaInatividadeFavorecido)
    {
        this._qtDiaInatividadeFavorecido = qtDiaInatividadeFavorecido;
        this._has_qtDiaInatividadeFavorecido = true;
    } //-- void setQtDiaInatividadeFavorecido(int) 

    /**
     * Sets the value of field 'qtDiaRepiqConsulta'.
     * 
     * @param qtDiaRepiqConsulta the value of field
     * 'qtDiaRepiqConsulta'.
     */
    public void setQtDiaRepiqConsulta(int qtDiaRepiqConsulta)
    {
        this._qtDiaRepiqConsulta = qtDiaRepiqConsulta;
        this._has_qtDiaRepiqConsulta = true;
    } //-- void setQtDiaRepiqConsulta(int) 

    /**
     * Sets the value of field 'qtDiaUtilPgto'.
     * 
     * @param qtDiaUtilPgto the value of field 'qtDiaUtilPgto'.
     */
    public void setQtDiaUtilPgto(int qtDiaUtilPgto)
    {
        this._qtDiaUtilPgto = qtDiaUtilPgto;
        this._has_qtDiaUtilPgto = true;
    } //-- void setQtDiaUtilPgto(int) 

    /**
     * Sets the value of field 'qtEtapasRecadastroBeneficio'.
     * 
     * @param qtEtapasRecadastroBeneficio the value of field
     * 'qtEtapasRecadastroBeneficio'.
     */
    public void setQtEtapasRecadastroBeneficio(int qtEtapasRecadastroBeneficio)
    {
        this._qtEtapasRecadastroBeneficio = qtEtapasRecadastroBeneficio;
        this._has_qtEtapasRecadastroBeneficio = true;
    } //-- void setQtEtapasRecadastroBeneficio(int) 

    /**
     * Sets the value of field 'qtFaseRecadastroBeneficio'.
     * 
     * @param qtFaseRecadastroBeneficio the value of field
     * 'qtFaseRecadastroBeneficio'.
     */
    public void setQtFaseRecadastroBeneficio(int qtFaseRecadastroBeneficio)
    {
        this._qtFaseRecadastroBeneficio = qtFaseRecadastroBeneficio;
        this._has_qtFaseRecadastroBeneficio = true;
    } //-- void setQtFaseRecadastroBeneficio(int) 

    /**
     * Sets the value of field 'qtLimiteLinha'.
     * 
     * @param qtLimiteLinha the value of field 'qtLimiteLinha'.
     */
    public void setQtLimiteLinha(int qtLimiteLinha)
    {
        this._qtLimiteLinha = qtLimiteLinha;
        this._has_qtLimiteLinha = true;
    } //-- void setQtLimiteLinha(int) 

    /**
     * Sets the value of field 'qtLimiteSolicitacaoCatao'.
     * 
     * @param qtLimiteSolicitacaoCatao the value of field
     * 'qtLimiteSolicitacaoCatao'.
     */
    public void setQtLimiteSolicitacaoCatao(int qtLimiteSolicitacaoCatao)
    {
        this._qtLimiteSolicitacaoCatao = qtLimiteSolicitacaoCatao;
        this._has_qtLimiteSolicitacaoCatao = true;
    } //-- void setQtLimiteSolicitacaoCatao(int) 

    /**
     * Sets the value of field 'qtMaximaInconLote'.
     * 
     * @param qtMaximaInconLote the value of field
     * 'qtMaximaInconLote'.
     */
    public void setQtMaximaInconLote(int qtMaximaInconLote)
    {
        this._qtMaximaInconLote = qtMaximaInconLote;
        this._has_qtMaximaInconLote = true;
    } //-- void setQtMaximaInconLote(int) 

    /**
     * Sets the value of field 'qtMaximaTituloVencido'.
     * 
     * @param qtMaximaTituloVencido the value of field
     * 'qtMaximaTituloVencido'.
     */
    public void setQtMaximaTituloVencido(int qtMaximaTituloVencido)
    {
        this._qtMaximaTituloVencido = qtMaximaTituloVencido;
        this._has_qtMaximaTituloVencido = true;
    } //-- void setQtMaximaTituloVencido(int) 

    /**
     * Sets the value of field 'qtMesComprovante'.
     * 
     * @param qtMesComprovante the value of field 'qtMesComprovante'
     */
    public void setQtMesComprovante(int qtMesComprovante)
    {
        this._qtMesComprovante = qtMesComprovante;
        this._has_qtMesComprovante = true;
    } //-- void setQtMesComprovante(int) 

    /**
     * Sets the value of field 'qtMesEtapaRecadastro'.
     * 
     * @param qtMesEtapaRecadastro the value of field
     * 'qtMesEtapaRecadastro'.
     */
    public void setQtMesEtapaRecadastro(int qtMesEtapaRecadastro)
    {
        this._qtMesEtapaRecadastro = qtMesEtapaRecadastro;
        this._has_qtMesEtapaRecadastro = true;
    } //-- void setQtMesEtapaRecadastro(int) 

    /**
     * Sets the value of field 'qtMesFaseRecadastro'.
     * 
     * @param qtMesFaseRecadastro the value of field
     * 'qtMesFaseRecadastro'.
     */
    public void setQtMesFaseRecadastro(int qtMesFaseRecadastro)
    {
        this._qtMesFaseRecadastro = qtMesFaseRecadastro;
        this._has_qtMesFaseRecadastro = true;
    } //-- void setQtMesFaseRecadastro(int) 

    /**
     * Sets the value of field 'qtMesReajusteTarifa'.
     * 
     * @param qtMesReajusteTarifa the value of field
     * 'qtMesReajusteTarifa'.
     */
    public void setQtMesReajusteTarifa(int qtMesReajusteTarifa)
    {
        this._qtMesReajusteTarifa = qtMesReajusteTarifa;
        this._has_qtMesReajusteTarifa = true;
    } //-- void setQtMesReajusteTarifa(int) 

    /**
     * Sets the value of field 'qtViaAviso'.
     * 
     * @param qtViaAviso the value of field 'qtViaAviso'.
     */
    public void setQtViaAviso(int qtViaAviso)
    {
        this._qtViaAviso = qtViaAviso;
        this._has_qtViaAviso = true;
    } //-- void setQtViaAviso(int) 

    /**
     * Sets the value of field 'qtViaCobranca'.
     * 
     * @param qtViaCobranca the value of field 'qtViaCobranca'.
     */
    public void setQtViaCobranca(int qtViaCobranca)
    {
        this._qtViaCobranca = qtViaCobranca;
        this._has_qtViaCobranca = true;
    } //-- void setQtViaCobranca(int) 

    /**
     * Sets the value of field 'qtViaComprovante'.
     * 
     * @param qtViaComprovante the value of field 'qtViaComprovante'
     */
    public void setQtViaComprovante(int qtViaComprovante)
    {
        this._qtViaComprovante = qtViaComprovante;
        this._has_qtViaComprovante = true;
    } //-- void setQtViaComprovante(int) 

    /**
     * Sets the value of field 'vlFavorecidoNaoCadastro'.
     * 
     * @param vlFavorecidoNaoCadastro the value of field
     * 'vlFavorecidoNaoCadastro'.
     */
    public void setVlFavorecidoNaoCadastro(java.math.BigDecimal vlFavorecidoNaoCadastro)
    {
        this._vlFavorecidoNaoCadastro = vlFavorecidoNaoCadastro;
    } //-- void setVlFavorecidoNaoCadastro(java.math.BigDecimal) 

    /**
     * Sets the value of field 'vlLimiteDiaPagamento'.
     * 
     * @param vlLimiteDiaPagamento the value of field
     * 'vlLimiteDiaPagamento'.
     */
    public void setVlLimiteDiaPagamento(java.math.BigDecimal vlLimiteDiaPagamento)
    {
        this._vlLimiteDiaPagamento = vlLimiteDiaPagamento;
    } //-- void setVlLimiteDiaPagamento(java.math.BigDecimal) 

    /**
     * Sets the value of field 'vlLimiteIndividualPagamento'.
     * 
     * @param vlLimiteIndividualPagamento the value of field
     * 'vlLimiteIndividualPagamento'.
     */
    public void setVlLimiteIndividualPagamento(java.math.BigDecimal vlLimiteIndividualPagamento)
    {
        this._vlLimiteIndividualPagamento = vlLimiteIndividualPagamento;
    } //-- void setVlLimiteIndividualPagamento(java.math.BigDecimal) 

    /**
     * Sets the value of field 'vlPercentualDiferencaTolerada'.
     * 
     * @param vlPercentualDiferencaTolerada the value of field
     * 'vlPercentualDiferencaTolerada'.
     */
    public void setVlPercentualDiferencaTolerada(java.math.BigDecimal vlPercentualDiferencaTolerada)
    {
        this._vlPercentualDiferencaTolerada = vlPercentualDiferencaTolerada;
    } //-- void setVlPercentualDiferencaTolerada(java.math.BigDecimal) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return DetalharTipoServModContratoResponse
     */
    
    
    public static br.com.bradesco.web.pgit.service.data.pdc.detalhartiposervmodcontrato.response.DetalharTipoServModContratoResponse unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.detalhartiposervmodcontrato.response.DetalharTipoServModContratoResponse) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.detalhartiposervmodcontrato.response.DetalharTipoServModContratoResponse.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalhartiposervmodcontrato.response.DetalharTipoServModContratoResponse unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 




}
