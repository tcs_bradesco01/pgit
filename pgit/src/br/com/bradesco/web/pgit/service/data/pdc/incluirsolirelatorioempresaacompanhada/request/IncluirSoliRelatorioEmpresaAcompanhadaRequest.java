/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.incluirsolirelatorioempresaacompanhada.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class IncluirSoliRelatorioEmpresaAcompanhadaRequest.
 * 
 * @version $Revision$ $Date$
 */
public class IncluirSoliRelatorioEmpresaAcompanhadaRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdTipoAcesso
     */
    private int _cdTipoAcesso = 0;

    /**
     * keeps track of state for field: _cdTipoAcesso
     */
    private boolean _has_cdTipoAcesso;

    /**
     * Field _hrSolicitacaoPagamento
     */
    private java.lang.String _hrSolicitacaoPagamento;

    /**
     * Field _cdUsuarioInclusao
     */
    private java.lang.String _cdUsuarioInclusao;


      //----------------/
     //- Constructors -/
    //----------------/

    public IncluirSoliRelatorioEmpresaAcompanhadaRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.incluirsolirelatorioempresaacompanhada.request.IncluirSoliRelatorioEmpresaAcompanhadaRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdTipoAcesso
     * 
     */
    public void deleteCdTipoAcesso()
    {
        this._has_cdTipoAcesso= false;
    } //-- void deleteCdTipoAcesso() 

    /**
     * Returns the value of field 'cdTipoAcesso'.
     * 
     * @return int
     * @return the value of field 'cdTipoAcesso'.
     */
    public int getCdTipoAcesso()
    {
        return this._cdTipoAcesso;
    } //-- int getCdTipoAcesso() 

    /**
     * Returns the value of field 'cdUsuarioInclusao'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioInclusao'.
     */
    public java.lang.String getCdUsuarioInclusao()
    {
        return this._cdUsuarioInclusao;
    } //-- java.lang.String getCdUsuarioInclusao() 

    /**
     * Returns the value of field 'hrSolicitacaoPagamento'.
     * 
     * @return String
     * @return the value of field 'hrSolicitacaoPagamento'.
     */
    public java.lang.String getHrSolicitacaoPagamento()
    {
        return this._hrSolicitacaoPagamento;
    } //-- java.lang.String getHrSolicitacaoPagamento() 

    /**
     * Method hasCdTipoAcesso
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoAcesso()
    {
        return this._has_cdTipoAcesso;
    } //-- boolean hasCdTipoAcesso() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdTipoAcesso'.
     * 
     * @param cdTipoAcesso the value of field 'cdTipoAcesso'.
     */
    public void setCdTipoAcesso(int cdTipoAcesso)
    {
        this._cdTipoAcesso = cdTipoAcesso;
        this._has_cdTipoAcesso = true;
    } //-- void setCdTipoAcesso(int) 

    /**
     * Sets the value of field 'cdUsuarioInclusao'.
     * 
     * @param cdUsuarioInclusao the value of field
     * 'cdUsuarioInclusao'.
     */
    public void setCdUsuarioInclusao(java.lang.String cdUsuarioInclusao)
    {
        this._cdUsuarioInclusao = cdUsuarioInclusao;
    } //-- void setCdUsuarioInclusao(java.lang.String) 

    /**
     * Sets the value of field 'hrSolicitacaoPagamento'.
     * 
     * @param hrSolicitacaoPagamento the value of field
     * 'hrSolicitacaoPagamento'.
     */
    public void setHrSolicitacaoPagamento(java.lang.String hrSolicitacaoPagamento)
    {
        this._hrSolicitacaoPagamento = hrSolicitacaoPagamento;
    } //-- void setHrSolicitacaoPagamento(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return IncluirSoliRelatorioEmpresaAcompanhadaRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.incluirsolirelatorioempresaacompanhada.request.IncluirSoliRelatorioEmpresaAcompanhadaRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.incluirsolirelatorioempresaacompanhada.request.IncluirSoliRelatorioEmpresaAcompanhadaRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.incluirsolirelatorioempresaacompanhada.request.IncluirSoliRelatorioEmpresaAcompanhadaRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.incluirsolirelatorioempresaacompanhada.request.IncluirSoliRelatorioEmpresaAcompanhadaRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
