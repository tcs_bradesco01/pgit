/*
 * Nome: br.com.bradesco.web.pgit.service.business.regsegregacessodados.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.regsegregacessodados.bean;

/**
 * Nome: AlterarRegraSegregacaoEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class AlterarRegraSegregacaoEntradaDTO {
	
	/** Atributo tipoUnidadeOrganizacionalUsuario. */
	private int tipoUnidadeOrganizacionalUsuario;
	
	/** Atributo tipoUnidadeOrganizacionalProprietarioDados. */
	private int tipoUnidadeOrganizacionalProprietarioDados;
	
	/** Atributo tipoAcao. */
	private int tipoAcao;
	
	/** Atributo nivelPermissaoAcessoDados. */
	private int nivelPermissaoAcessoDados;
	
	/** Atributo codigoRegra. */
	private int codigoRegra;
	
	
	/**
	 * Get: nivelPermissaoAcessoDados.
	 *
	 * @return nivelPermissaoAcessoDados
	 */
	public int getNivelPermissaoAcessoDados() {
		return nivelPermissaoAcessoDados;
	}
	
	/**
	 * Set: nivelPermissaoAcessoDados.
	 *
	 * @param cdNivelPermissaoAcessoDados the nivel permissao acesso dados
	 */
	public void setNivelPermissaoAcessoDados(int cdNivelPermissaoAcessoDados) {
		this.nivelPermissaoAcessoDados = cdNivelPermissaoAcessoDados;
	}
	
	/**
	 * Get: tipoAcao.
	 *
	 * @return tipoAcao
	 */
	public int getTipoAcao() {
		return tipoAcao;
	}
	
	
	
	
	/**
	 * Set: tipoAcao.
	 *
	 * @param cdTipoAcao the tipo acao
	 */
	public void setTipoAcao(int cdTipoAcao) {
		this.tipoAcao = cdTipoAcao;
	}
	
	/**
	 * Get: tipoUnidadeOrganizacionalProprietarioDados.
	 *
	 * @return tipoUnidadeOrganizacionalProprietarioDados
	 */
	public int getTipoUnidadeOrganizacionalProprietarioDados() {
		return tipoUnidadeOrganizacionalProprietarioDados;
	}
	
	/**
	 * Set: tipoUnidadeOrganizacionalProprietarioDados.
	 *
	 * @param cdTipoUnidadeOrganizacionalProprietarioDados the tipo unidade organizacional proprietario dados
	 */
	public void setTipoUnidadeOrganizacionalProprietarioDados(
			int cdTipoUnidadeOrganizacionalProprietarioDados) {
		this.tipoUnidadeOrganizacionalProprietarioDados = cdTipoUnidadeOrganizacionalProprietarioDados;
	}
	
	/**
	 * Get: tipoUnidadeOrganizacionalUsuario.
	 *
	 * @return tipoUnidadeOrganizacionalUsuario
	 */
	public int getTipoUnidadeOrganizacionalUsuario() {
		return tipoUnidadeOrganizacionalUsuario;
	}
	
	/**
	 * Set: tipoUnidadeOrganizacionalUsuario.
	 *
	 * @param cdTipoUnidadeOrganizacionalUsuario the tipo unidade organizacional usuario
	 */
	public void setTipoUnidadeOrganizacionalUsuario(
			int cdTipoUnidadeOrganizacionalUsuario) {
		this.tipoUnidadeOrganizacionalUsuario = cdTipoUnidadeOrganizacionalUsuario;
	}
	
	/**
	 * Get: codigoRegra.
	 *
	 * @return codigoRegra
	 */
	public int getCodigoRegra() {
		return codigoRegra;
	}
	
	/**
	 * Set: codigoRegra.
	 *
	 * @param codigoRegra the codigo regra
	 */
	public void setCodigoRegra(int codigoRegra) {
		this.codigoRegra = codigoRegra;
	}
	
	
	


}
