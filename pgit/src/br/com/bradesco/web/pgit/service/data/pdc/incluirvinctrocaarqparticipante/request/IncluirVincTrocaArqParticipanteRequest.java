/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.incluirvinctrocaarqparticipante.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class IncluirVincTrocaArqParticipanteRequest.
 * 
 * @version $Revision$ $Date$
 */
public class IncluirVincTrocaArqParticipanteRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdTipoLayoutArquivo
     */
    private int _cdTipoLayoutArquivo = 0;

    /**
     * keeps track of state for field: _cdTipoLayoutArquivo
     */
    private boolean _has_cdTipoLayoutArquivo;

    /**
     * Field _cdPerfilTrocaArquivo
     */
    private long _cdPerfilTrocaArquivo = 0;

    /**
     * keeps track of state for field: _cdPerfilTrocaArquivo
     */
    private boolean _has_cdPerfilTrocaArquivo;

    /**
     * Field _cdPessoaJuridicaContrato
     */
    private long _cdPessoaJuridicaContrato = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridicaContrato
     */
    private boolean _has_cdPessoaJuridicaContrato;

    /**
     * Field _cdTipoContratoNegocio
     */
    private int _cdTipoContratoNegocio = 0;

    /**
     * keeps track of state for field: _cdTipoContratoNegocio
     */
    private boolean _has_cdTipoContratoNegocio;

    /**
     * Field _nrSequenciaContratoNegocio
     */
    private long _nrSequenciaContratoNegocio = 0;

    /**
     * keeps track of state for field: _nrSequenciaContratoNegocio
     */
    private boolean _has_nrSequenciaContratoNegocio;

    /**
     * Field _cdTipoParticipacaoPessoa
     */
    private int _cdTipoParticipacaoPessoa = 0;

    /**
     * keeps track of state for field: _cdTipoParticipacaoPessoa
     */
    private boolean _has_cdTipoParticipacaoPessoa;

    /**
     * Field _cdPessoa
     */
    private long _cdPessoa = 0;

    /**
     * keeps track of state for field: _cdPessoa
     */
    private boolean _has_cdPessoa;

    /**
     * Field _cdCorpoCpfCnpj
     */
    private long _cdCorpoCpfCnpj = 0;

    /**
     * keeps track of state for field: _cdCorpoCpfCnpj
     */
    private boolean _has_cdCorpoCpfCnpj;

    /**
     * Field _nrFilialCnpj
     */
    private int _nrFilialCnpj = 0;

    /**
     * keeps track of state for field: _nrFilialCnpj
     */
    private boolean _has_nrFilialCnpj;

    /**
     * Field _cdDigitoCpfCnpj
     */
    private java.lang.String _cdDigitoCpfCnpj;

    /**
     * Field _cdTipoLayoutMae
     */
    private int _cdTipoLayoutMae = 0;

    /**
     * keeps track of state for field: _cdTipoLayoutMae
     */
    private boolean _has_cdTipoLayoutMae;

    /**
     * Field _cdPerfilTrocaMae
     */
    private long _cdPerfilTrocaMae = 0;

    /**
     * keeps track of state for field: _cdPerfilTrocaMae
     */
    private boolean _has_cdPerfilTrocaMae;

    /**
     * Field _cdPessoaJuridicaMae
     */
    private long _cdPessoaJuridicaMae = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridicaMae
     */
    private boolean _has_cdPessoaJuridicaMae;

    /**
     * Field _cdTipoContratoMae
     */
    private int _cdTipoContratoMae = 0;

    /**
     * keeps track of state for field: _cdTipoContratoMae
     */
    private boolean _has_cdTipoContratoMae;

    /**
     * Field _nrSequencialContratoMae
     */
    private long _nrSequencialContratoMae = 0;

    /**
     * keeps track of state for field: _nrSequencialContratoMae
     */
    private boolean _has_nrSequencialContratoMae;

    /**
     * Field _cdTipoParticipanteMae
     */
    private int _cdTipoParticipanteMae = 0;

    /**
     * keeps track of state for field: _cdTipoParticipanteMae
     */
    private boolean _has_cdTipoParticipanteMae;

    /**
     * Field _cdCpessoaMae
     */
    private long _cdCpessoaMae = 0;

    /**
     * keeps track of state for field: _cdCpessoaMae
     */
    private boolean _has_cdCpessoaMae;

    /**
     * Field _cdCpfCnpjMae
     */
    private long _cdCpfCnpjMae = 0;

    /**
     * keeps track of state for field: _cdCpfCnpjMae
     */
    private boolean _has_cdCpfCnpjMae;

    /**
     * Field _cdCnpjFilialMae
     */
    private int _cdCnpjFilialMae = 0;

    /**
     * keeps track of state for field: _cdCnpjFilialMae
     */
    private boolean _has_cdCnpjFilialMae;

    /**
     * Field _cdDigitoCpfCnpjMae
     */
    private java.lang.String _cdDigitoCpfCnpjMae;


      //----------------/
     //- Constructors -/
    //----------------/

    public IncluirVincTrocaArqParticipanteRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.incluirvinctrocaarqparticipante.request.IncluirVincTrocaArqParticipanteRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdCnpjFilialMae
     * 
     */
    public void deleteCdCnpjFilialMae()
    {
        this._has_cdCnpjFilialMae= false;
    } //-- void deleteCdCnpjFilialMae() 

    /**
     * Method deleteCdCorpoCpfCnpj
     * 
     */
    public void deleteCdCorpoCpfCnpj()
    {
        this._has_cdCorpoCpfCnpj= false;
    } //-- void deleteCdCorpoCpfCnpj() 

    /**
     * Method deleteCdCpessoaMae
     * 
     */
    public void deleteCdCpessoaMae()
    {
        this._has_cdCpessoaMae= false;
    } //-- void deleteCdCpessoaMae() 

    /**
     * Method deleteCdCpfCnpjMae
     * 
     */
    public void deleteCdCpfCnpjMae()
    {
        this._has_cdCpfCnpjMae= false;
    } //-- void deleteCdCpfCnpjMae() 

    /**
     * Method deleteCdPerfilTrocaArquivo
     * 
     */
    public void deleteCdPerfilTrocaArquivo()
    {
        this._has_cdPerfilTrocaArquivo= false;
    } //-- void deleteCdPerfilTrocaArquivo() 

    /**
     * Method deleteCdPerfilTrocaMae
     * 
     */
    public void deleteCdPerfilTrocaMae()
    {
        this._has_cdPerfilTrocaMae= false;
    } //-- void deleteCdPerfilTrocaMae() 

    /**
     * Method deleteCdPessoa
     * 
     */
    public void deleteCdPessoa()
    {
        this._has_cdPessoa= false;
    } //-- void deleteCdPessoa() 

    /**
     * Method deleteCdPessoaJuridicaContrato
     * 
     */
    public void deleteCdPessoaJuridicaContrato()
    {
        this._has_cdPessoaJuridicaContrato= false;
    } //-- void deleteCdPessoaJuridicaContrato() 

    /**
     * Method deleteCdPessoaJuridicaMae
     * 
     */
    public void deleteCdPessoaJuridicaMae()
    {
        this._has_cdPessoaJuridicaMae= false;
    } //-- void deleteCdPessoaJuridicaMae() 

    /**
     * Method deleteCdTipoContratoMae
     * 
     */
    public void deleteCdTipoContratoMae()
    {
        this._has_cdTipoContratoMae= false;
    } //-- void deleteCdTipoContratoMae() 

    /**
     * Method deleteCdTipoContratoNegocio
     * 
     */
    public void deleteCdTipoContratoNegocio()
    {
        this._has_cdTipoContratoNegocio= false;
    } //-- void deleteCdTipoContratoNegocio() 

    /**
     * Method deleteCdTipoLayoutArquivo
     * 
     */
    public void deleteCdTipoLayoutArquivo()
    {
        this._has_cdTipoLayoutArquivo= false;
    } //-- void deleteCdTipoLayoutArquivo() 

    /**
     * Method deleteCdTipoLayoutMae
     * 
     */
    public void deleteCdTipoLayoutMae()
    {
        this._has_cdTipoLayoutMae= false;
    } //-- void deleteCdTipoLayoutMae() 

    /**
     * Method deleteCdTipoParticipacaoPessoa
     * 
     */
    public void deleteCdTipoParticipacaoPessoa()
    {
        this._has_cdTipoParticipacaoPessoa= false;
    } //-- void deleteCdTipoParticipacaoPessoa() 

    /**
     * Method deleteCdTipoParticipanteMae
     * 
     */
    public void deleteCdTipoParticipanteMae()
    {
        this._has_cdTipoParticipanteMae= false;
    } //-- void deleteCdTipoParticipanteMae() 

    /**
     * Method deleteNrFilialCnpj
     * 
     */
    public void deleteNrFilialCnpj()
    {
        this._has_nrFilialCnpj= false;
    } //-- void deleteNrFilialCnpj() 

    /**
     * Method deleteNrSequenciaContratoNegocio
     * 
     */
    public void deleteNrSequenciaContratoNegocio()
    {
        this._has_nrSequenciaContratoNegocio= false;
    } //-- void deleteNrSequenciaContratoNegocio() 

    /**
     * Method deleteNrSequencialContratoMae
     * 
     */
    public void deleteNrSequencialContratoMae()
    {
        this._has_nrSequencialContratoMae= false;
    } //-- void deleteNrSequencialContratoMae() 

    /**
     * Returns the value of field 'cdCnpjFilialMae'.
     * 
     * @return int
     * @return the value of field 'cdCnpjFilialMae'.
     */
    public int getCdCnpjFilialMae()
    {
        return this._cdCnpjFilialMae;
    } //-- int getCdCnpjFilialMae() 

    /**
     * Returns the value of field 'cdCorpoCpfCnpj'.
     * 
     * @return long
     * @return the value of field 'cdCorpoCpfCnpj'.
     */
    public long getCdCorpoCpfCnpj()
    {
        return this._cdCorpoCpfCnpj;
    } //-- long getCdCorpoCpfCnpj() 

    /**
     * Returns the value of field 'cdCpessoaMae'.
     * 
     * @return long
     * @return the value of field 'cdCpessoaMae'.
     */
    public long getCdCpessoaMae()
    {
        return this._cdCpessoaMae;
    } //-- long getCdCpessoaMae() 

    /**
     * Returns the value of field 'cdCpfCnpjMae'.
     * 
     * @return long
     * @return the value of field 'cdCpfCnpjMae'.
     */
    public long getCdCpfCnpjMae()
    {
        return this._cdCpfCnpjMae;
    } //-- long getCdCpfCnpjMae() 

    /**
     * Returns the value of field 'cdDigitoCpfCnpj'.
     * 
     * @return String
     * @return the value of field 'cdDigitoCpfCnpj'.
     */
    public java.lang.String getCdDigitoCpfCnpj()
    {
        return this._cdDigitoCpfCnpj;
    } //-- java.lang.String getCdDigitoCpfCnpj() 

    /**
     * Returns the value of field 'cdDigitoCpfCnpjMae'.
     * 
     * @return String
     * @return the value of field 'cdDigitoCpfCnpjMae'.
     */
    public java.lang.String getCdDigitoCpfCnpjMae()
    {
        return this._cdDigitoCpfCnpjMae;
    } //-- java.lang.String getCdDigitoCpfCnpjMae() 

    /**
     * Returns the value of field 'cdPerfilTrocaArquivo'.
     * 
     * @return long
     * @return the value of field 'cdPerfilTrocaArquivo'.
     */
    public long getCdPerfilTrocaArquivo()
    {
        return this._cdPerfilTrocaArquivo;
    } //-- long getCdPerfilTrocaArquivo() 

    /**
     * Returns the value of field 'cdPerfilTrocaMae'.
     * 
     * @return long
     * @return the value of field 'cdPerfilTrocaMae'.
     */
    public long getCdPerfilTrocaMae()
    {
        return this._cdPerfilTrocaMae;
    } //-- long getCdPerfilTrocaMae() 

    /**
     * Returns the value of field 'cdPessoa'.
     * 
     * @return long
     * @return the value of field 'cdPessoa'.
     */
    public long getCdPessoa()
    {
        return this._cdPessoa;
    } //-- long getCdPessoa() 

    /**
     * Returns the value of field 'cdPessoaJuridicaContrato'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridicaContrato'.
     */
    public long getCdPessoaJuridicaContrato()
    {
        return this._cdPessoaJuridicaContrato;
    } //-- long getCdPessoaJuridicaContrato() 

    /**
     * Returns the value of field 'cdPessoaJuridicaMae'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridicaMae'.
     */
    public long getCdPessoaJuridicaMae()
    {
        return this._cdPessoaJuridicaMae;
    } //-- long getCdPessoaJuridicaMae() 

    /**
     * Returns the value of field 'cdTipoContratoMae'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoMae'.
     */
    public int getCdTipoContratoMae()
    {
        return this._cdTipoContratoMae;
    } //-- int getCdTipoContratoMae() 

    /**
     * Returns the value of field 'cdTipoContratoNegocio'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoNegocio'.
     */
    public int getCdTipoContratoNegocio()
    {
        return this._cdTipoContratoNegocio;
    } //-- int getCdTipoContratoNegocio() 

    /**
     * Returns the value of field 'cdTipoLayoutArquivo'.
     * 
     * @return int
     * @return the value of field 'cdTipoLayoutArquivo'.
     */
    public int getCdTipoLayoutArquivo()
    {
        return this._cdTipoLayoutArquivo;
    } //-- int getCdTipoLayoutArquivo() 

    /**
     * Returns the value of field 'cdTipoLayoutMae'.
     * 
     * @return int
     * @return the value of field 'cdTipoLayoutMae'.
     */
    public int getCdTipoLayoutMae()
    {
        return this._cdTipoLayoutMae;
    } //-- int getCdTipoLayoutMae() 

    /**
     * Returns the value of field 'cdTipoParticipacaoPessoa'.
     * 
     * @return int
     * @return the value of field 'cdTipoParticipacaoPessoa'.
     */
    public int getCdTipoParticipacaoPessoa()
    {
        return this._cdTipoParticipacaoPessoa;
    } //-- int getCdTipoParticipacaoPessoa() 

    /**
     * Returns the value of field 'cdTipoParticipanteMae'.
     * 
     * @return int
     * @return the value of field 'cdTipoParticipanteMae'.
     */
    public int getCdTipoParticipanteMae()
    {
        return this._cdTipoParticipanteMae;
    } //-- int getCdTipoParticipanteMae() 

    /**
     * Returns the value of field 'nrFilialCnpj'.
     * 
     * @return int
     * @return the value of field 'nrFilialCnpj'.
     */
    public int getNrFilialCnpj()
    {
        return this._nrFilialCnpj;
    } //-- int getNrFilialCnpj() 

    /**
     * Returns the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @return long
     * @return the value of field 'nrSequenciaContratoNegocio'.
     */
    public long getNrSequenciaContratoNegocio()
    {
        return this._nrSequenciaContratoNegocio;
    } //-- long getNrSequenciaContratoNegocio() 

    /**
     * Returns the value of field 'nrSequencialContratoMae'.
     * 
     * @return long
     * @return the value of field 'nrSequencialContratoMae'.
     */
    public long getNrSequencialContratoMae()
    {
        return this._nrSequencialContratoMae;
    } //-- long getNrSequencialContratoMae() 

    /**
     * Method hasCdCnpjFilialMae
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCnpjFilialMae()
    {
        return this._has_cdCnpjFilialMae;
    } //-- boolean hasCdCnpjFilialMae() 

    /**
     * Method hasCdCorpoCpfCnpj
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCorpoCpfCnpj()
    {
        return this._has_cdCorpoCpfCnpj;
    } //-- boolean hasCdCorpoCpfCnpj() 

    /**
     * Method hasCdCpessoaMae
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCpessoaMae()
    {
        return this._has_cdCpessoaMae;
    } //-- boolean hasCdCpessoaMae() 

    /**
     * Method hasCdCpfCnpjMae
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCpfCnpjMae()
    {
        return this._has_cdCpfCnpjMae;
    } //-- boolean hasCdCpfCnpjMae() 

    /**
     * Method hasCdPerfilTrocaArquivo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPerfilTrocaArquivo()
    {
        return this._has_cdPerfilTrocaArquivo;
    } //-- boolean hasCdPerfilTrocaArquivo() 

    /**
     * Method hasCdPerfilTrocaMae
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPerfilTrocaMae()
    {
        return this._has_cdPerfilTrocaMae;
    } //-- boolean hasCdPerfilTrocaMae() 

    /**
     * Method hasCdPessoa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoa()
    {
        return this._has_cdPessoa;
    } //-- boolean hasCdPessoa() 

    /**
     * Method hasCdPessoaJuridicaContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridicaContrato()
    {
        return this._has_cdPessoaJuridicaContrato;
    } //-- boolean hasCdPessoaJuridicaContrato() 

    /**
     * Method hasCdPessoaJuridicaMae
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridicaMae()
    {
        return this._has_cdPessoaJuridicaMae;
    } //-- boolean hasCdPessoaJuridicaMae() 

    /**
     * Method hasCdTipoContratoMae
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoMae()
    {
        return this._has_cdTipoContratoMae;
    } //-- boolean hasCdTipoContratoMae() 

    /**
     * Method hasCdTipoContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoNegocio()
    {
        return this._has_cdTipoContratoNegocio;
    } //-- boolean hasCdTipoContratoNegocio() 

    /**
     * Method hasCdTipoLayoutArquivo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoLayoutArquivo()
    {
        return this._has_cdTipoLayoutArquivo;
    } //-- boolean hasCdTipoLayoutArquivo() 

    /**
     * Method hasCdTipoLayoutMae
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoLayoutMae()
    {
        return this._has_cdTipoLayoutMae;
    } //-- boolean hasCdTipoLayoutMae() 

    /**
     * Method hasCdTipoParticipacaoPessoa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoParticipacaoPessoa()
    {
        return this._has_cdTipoParticipacaoPessoa;
    } //-- boolean hasCdTipoParticipacaoPessoa() 

    /**
     * Method hasCdTipoParticipanteMae
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoParticipanteMae()
    {
        return this._has_cdTipoParticipanteMae;
    } //-- boolean hasCdTipoParticipanteMae() 

    /**
     * Method hasNrFilialCnpj
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrFilialCnpj()
    {
        return this._has_nrFilialCnpj;
    } //-- boolean hasNrFilialCnpj() 

    /**
     * Method hasNrSequenciaContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaContratoNegocio()
    {
        return this._has_nrSequenciaContratoNegocio;
    } //-- boolean hasNrSequenciaContratoNegocio() 

    /**
     * Method hasNrSequencialContratoMae
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequencialContratoMae()
    {
        return this._has_nrSequencialContratoMae;
    } //-- boolean hasNrSequencialContratoMae() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdCnpjFilialMae'.
     * 
     * @param cdCnpjFilialMae the value of field 'cdCnpjFilialMae'.
     */
    public void setCdCnpjFilialMae(int cdCnpjFilialMae)
    {
        this._cdCnpjFilialMae = cdCnpjFilialMae;
        this._has_cdCnpjFilialMae = true;
    } //-- void setCdCnpjFilialMae(int) 

    /**
     * Sets the value of field 'cdCorpoCpfCnpj'.
     * 
     * @param cdCorpoCpfCnpj the value of field 'cdCorpoCpfCnpj'.
     */
    public void setCdCorpoCpfCnpj(long cdCorpoCpfCnpj)
    {
        this._cdCorpoCpfCnpj = cdCorpoCpfCnpj;
        this._has_cdCorpoCpfCnpj = true;
    } //-- void setCdCorpoCpfCnpj(long) 

    /**
     * Sets the value of field 'cdCpessoaMae'.
     * 
     * @param cdCpessoaMae the value of field 'cdCpessoaMae'.
     */
    public void setCdCpessoaMae(long cdCpessoaMae)
    {
        this._cdCpessoaMae = cdCpessoaMae;
        this._has_cdCpessoaMae = true;
    } //-- void setCdCpessoaMae(long) 

    /**
     * Sets the value of field 'cdCpfCnpjMae'.
     * 
     * @param cdCpfCnpjMae the value of field 'cdCpfCnpjMae'.
     */
    public void setCdCpfCnpjMae(long cdCpfCnpjMae)
    {
        this._cdCpfCnpjMae = cdCpfCnpjMae;
        this._has_cdCpfCnpjMae = true;
    } //-- void setCdCpfCnpjMae(long) 

    /**
     * Sets the value of field 'cdDigitoCpfCnpj'.
     * 
     * @param cdDigitoCpfCnpj the value of field 'cdDigitoCpfCnpj'.
     */
    public void setCdDigitoCpfCnpj(java.lang.String cdDigitoCpfCnpj)
    {
        this._cdDigitoCpfCnpj = cdDigitoCpfCnpj;
    } //-- void setCdDigitoCpfCnpj(java.lang.String) 

    /**
     * Sets the value of field 'cdDigitoCpfCnpjMae'.
     * 
     * @param cdDigitoCpfCnpjMae the value of field
     * 'cdDigitoCpfCnpjMae'.
     */
    public void setCdDigitoCpfCnpjMae(java.lang.String cdDigitoCpfCnpjMae)
    {
        this._cdDigitoCpfCnpjMae = cdDigitoCpfCnpjMae;
    } //-- void setCdDigitoCpfCnpjMae(java.lang.String) 

    /**
     * Sets the value of field 'cdPerfilTrocaArquivo'.
     * 
     * @param cdPerfilTrocaArquivo the value of field
     * 'cdPerfilTrocaArquivo'.
     */
    public void setCdPerfilTrocaArquivo(long cdPerfilTrocaArquivo)
    {
        this._cdPerfilTrocaArquivo = cdPerfilTrocaArquivo;
        this._has_cdPerfilTrocaArquivo = true;
    } //-- void setCdPerfilTrocaArquivo(long) 

    /**
     * Sets the value of field 'cdPerfilTrocaMae'.
     * 
     * @param cdPerfilTrocaMae the value of field 'cdPerfilTrocaMae'
     */
    public void setCdPerfilTrocaMae(long cdPerfilTrocaMae)
    {
        this._cdPerfilTrocaMae = cdPerfilTrocaMae;
        this._has_cdPerfilTrocaMae = true;
    } //-- void setCdPerfilTrocaMae(long) 

    /**
     * Sets the value of field 'cdPessoa'.
     * 
     * @param cdPessoa the value of field 'cdPessoa'.
     */
    public void setCdPessoa(long cdPessoa)
    {
        this._cdPessoa = cdPessoa;
        this._has_cdPessoa = true;
    } //-- void setCdPessoa(long) 

    /**
     * Sets the value of field 'cdPessoaJuridicaContrato'.
     * 
     * @param cdPessoaJuridicaContrato the value of field
     * 'cdPessoaJuridicaContrato'.
     */
    public void setCdPessoaJuridicaContrato(long cdPessoaJuridicaContrato)
    {
        this._cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
        this._has_cdPessoaJuridicaContrato = true;
    } //-- void setCdPessoaJuridicaContrato(long) 

    /**
     * Sets the value of field 'cdPessoaJuridicaMae'.
     * 
     * @param cdPessoaJuridicaMae the value of field
     * 'cdPessoaJuridicaMae'.
     */
    public void setCdPessoaJuridicaMae(long cdPessoaJuridicaMae)
    {
        this._cdPessoaJuridicaMae = cdPessoaJuridicaMae;
        this._has_cdPessoaJuridicaMae = true;
    } //-- void setCdPessoaJuridicaMae(long) 

    /**
     * Sets the value of field 'cdTipoContratoMae'.
     * 
     * @param cdTipoContratoMae the value of field
     * 'cdTipoContratoMae'.
     */
    public void setCdTipoContratoMae(int cdTipoContratoMae)
    {
        this._cdTipoContratoMae = cdTipoContratoMae;
        this._has_cdTipoContratoMae = true;
    } //-- void setCdTipoContratoMae(int) 

    /**
     * Sets the value of field 'cdTipoContratoNegocio'.
     * 
     * @param cdTipoContratoNegocio the value of field
     * 'cdTipoContratoNegocio'.
     */
    public void setCdTipoContratoNegocio(int cdTipoContratoNegocio)
    {
        this._cdTipoContratoNegocio = cdTipoContratoNegocio;
        this._has_cdTipoContratoNegocio = true;
    } //-- void setCdTipoContratoNegocio(int) 

    /**
     * Sets the value of field 'cdTipoLayoutArquivo'.
     * 
     * @param cdTipoLayoutArquivo the value of field
     * 'cdTipoLayoutArquivo'.
     */
    public void setCdTipoLayoutArquivo(int cdTipoLayoutArquivo)
    {
        this._cdTipoLayoutArquivo = cdTipoLayoutArquivo;
        this._has_cdTipoLayoutArquivo = true;
    } //-- void setCdTipoLayoutArquivo(int) 

    /**
     * Sets the value of field 'cdTipoLayoutMae'.
     * 
     * @param cdTipoLayoutMae the value of field 'cdTipoLayoutMae'.
     */
    public void setCdTipoLayoutMae(int cdTipoLayoutMae)
    {
        this._cdTipoLayoutMae = cdTipoLayoutMae;
        this._has_cdTipoLayoutMae = true;
    } //-- void setCdTipoLayoutMae(int) 

    /**
     * Sets the value of field 'cdTipoParticipacaoPessoa'.
     * 
     * @param cdTipoParticipacaoPessoa the value of field
     * 'cdTipoParticipacaoPessoa'.
     */
    public void setCdTipoParticipacaoPessoa(int cdTipoParticipacaoPessoa)
    {
        this._cdTipoParticipacaoPessoa = cdTipoParticipacaoPessoa;
        this._has_cdTipoParticipacaoPessoa = true;
    } //-- void setCdTipoParticipacaoPessoa(int) 

    /**
     * Sets the value of field 'cdTipoParticipanteMae'.
     * 
     * @param cdTipoParticipanteMae the value of field
     * 'cdTipoParticipanteMae'.
     */
    public void setCdTipoParticipanteMae(int cdTipoParticipanteMae)
    {
        this._cdTipoParticipanteMae = cdTipoParticipanteMae;
        this._has_cdTipoParticipanteMae = true;
    } //-- void setCdTipoParticipanteMae(int) 

    /**
     * Sets the value of field 'nrFilialCnpj'.
     * 
     * @param nrFilialCnpj the value of field 'nrFilialCnpj'.
     */
    public void setNrFilialCnpj(int nrFilialCnpj)
    {
        this._nrFilialCnpj = nrFilialCnpj;
        this._has_nrFilialCnpj = true;
    } //-- void setNrFilialCnpj(int) 

    /**
     * Sets the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @param nrSequenciaContratoNegocio the value of field
     * 'nrSequenciaContratoNegocio'.
     */
    public void setNrSequenciaContratoNegocio(long nrSequenciaContratoNegocio)
    {
        this._nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
        this._has_nrSequenciaContratoNegocio = true;
    } //-- void setNrSequenciaContratoNegocio(long) 

    /**
     * Sets the value of field 'nrSequencialContratoMae'.
     * 
     * @param nrSequencialContratoMae the value of field
     * 'nrSequencialContratoMae'.
     */
    public void setNrSequencialContratoMae(long nrSequencialContratoMae)
    {
        this._nrSequencialContratoMae = nrSequencialContratoMae;
        this._has_nrSequencialContratoMae = true;
    } //-- void setNrSequencialContratoMae(long) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return IncluirVincTrocaArqParticipanteRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.incluirvinctrocaarqparticipante.request.IncluirVincTrocaArqParticipanteRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.incluirvinctrocaarqparticipante.request.IncluirVincTrocaArqParticipanteRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.incluirvinctrocaarqparticipante.request.IncluirVincTrocaArqParticipanteRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.incluirvinctrocaarqparticipante.request.IncluirVincTrocaArqParticipanteRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
