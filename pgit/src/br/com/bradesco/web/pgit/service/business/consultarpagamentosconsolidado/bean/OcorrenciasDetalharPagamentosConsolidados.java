/*
 * Nome: br.com.bradesco.web.pgit.service.business.consultarpagamentosconsolidado.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.consultarpagamentosconsolidado.bean;

import java.math.BigDecimal;

/**
 * Nome: OcorrenciasDetalharPagamentosConsolidados
 * <p>
 * Prop�sito:
 * </p>
 * .
 * 
 * @author : todo!
 * @version :
 */
public class OcorrenciasDetalharPagamentosConsolidados {

	/** Atributo nrPagamento. */
	private String nrPagamento;

	/** Atributo vlPagamento. */
	private BigDecimal vlPagamento;

	/** Atributo vlPagamentoFormatado. */
	private String vlPagamentoFormatado;

	/** Atributo cdCnpjCpfFavorecido. */
	private Long cdCnpjCpfFavorecido;

	/** Atributo cdFilialCnpjCpfFavorecido. */
	private Integer cdFilialCnpjCpfFavorecido;

	/** Atributo cdControleCnpjCpfFavorecido. */
	private Integer cdControleCnpjCpfFavorecido;

	/** Atributo dsBeneficio. */
	private String dsBeneficio;

	/** Atributo cdBanco. */
	private Integer cdBanco;

	/** Atributo cdAgencia. */
	private Integer cdAgencia;

	/** Atributo cdDigitoAgencia. */
	private String cdDigitoAgencia;

	/** Atributo cdConta. */
	private Long cdConta;

	/** Atributo cdDigitoConta. */
	private String cdDigitoConta;

	/** Atributo cdTipoCanal. */
	private Integer cdTipoCanal;

	/** Atributo cdFavorecido. */
	private Long cdFavorecido;

	/** Atributo cdTipoTela. */
	private Integer cdTipoTela;

	/** Atributo bancoAgenciaContaCreditoFormatado. */
	private String bancoAgenciaContaCreditoFormatado;

	/** Atributo bancoFormatado. */
	private String bancoFormatado;

	/** Atributo agenciaFormatada. */
	private String agenciaFormatada;

	/** Atributo contaFormatada. */
	private String contaFormatada;

	/** Atributo favorecidoBeneficiarioFormatado. */
	private String favorecidoBeneficiarioFormatado;

	/** Atributo check. */
	private boolean check;

	/** Atributo dsEfetivacaoPagamento. */
	private String dsEfetivacaoPagamento;

	private String dsIndicadorAutorizacao;
	private Long cdLoteInterno;
	private String dsTipoLayout;
	private String dsIndicadorPagamento;
	private String cdIspbPagtoDestino;
	private String contaPagtoDestino;

	public String getNrPagamento() {
		return nrPagamento;
	}

	public void setNrPagamento(String nrPagamento) {
		this.nrPagamento = nrPagamento;
	}

	public BigDecimal getVlPagamento() {
		return vlPagamento;
	}

	public void setVlPagamento(BigDecimal vlPagamento) {
		this.vlPagamento = vlPagamento;
	}

	public String getVlPagamentoFormatado() {
		return vlPagamentoFormatado;
	}

	public void setVlPagamentoFormatado(String vlPagamentoFormatado) {
		this.vlPagamentoFormatado = vlPagamentoFormatado;
	}

	public Long getCdCnpjCpfFavorecido() {
		return cdCnpjCpfFavorecido;
	}

	public void setCdCnpjCpfFavorecido(Long cdCnpjCpfFavorecido) {
		this.cdCnpjCpfFavorecido = cdCnpjCpfFavorecido;
	}

	public Integer getCdFilialCnpjCpfFavorecido() {
		return cdFilialCnpjCpfFavorecido;
	}

	public void setCdFilialCnpjCpfFavorecido(Integer cdFilialCnpjCpfFavorecido) {
		this.cdFilialCnpjCpfFavorecido = cdFilialCnpjCpfFavorecido;
	}

	public Integer getCdControleCnpjCpfFavorecido() {
		return cdControleCnpjCpfFavorecido;
	}

	public void setCdControleCnpjCpfFavorecido(
			Integer cdControleCnpjCpfFavorecido) {
		this.cdControleCnpjCpfFavorecido = cdControleCnpjCpfFavorecido;
	}

	public String getDsBeneficio() {
		return dsBeneficio;
	}

	public void setDsBeneficio(String dsBeneficio) {
		this.dsBeneficio = dsBeneficio;
	}

	public Integer getCdBanco() {
		return cdBanco;
	}

	public void setCdBanco(Integer cdBanco) {
		this.cdBanco = cdBanco;
	}

	public Integer getCdAgencia() {
		return cdAgencia;
	}

	public void setCdAgencia(Integer cdAgencia) {
		this.cdAgencia = cdAgencia;
	}

	public String getCdDigitoAgencia() {
		return cdDigitoAgencia;
	}

	public void setCdDigitoAgencia(String cdDigitoAgencia) {
		this.cdDigitoAgencia = cdDigitoAgencia;
	}

	public Long getCdConta() {
		return cdConta;
	}

	public void setCdConta(Long cdConta) {
		this.cdConta = cdConta;
	}

	public String getCdDigitoConta() {
		return cdDigitoConta;
	}

	public void setCdDigitoConta(String cdDigitoConta) {
		this.cdDigitoConta = cdDigitoConta;
	}

	public Integer getCdTipoCanal() {
		return cdTipoCanal;
	}

	public void setCdTipoCanal(Integer cdTipoCanal) {
		this.cdTipoCanal = cdTipoCanal;
	}

	public Long getCdFavorecido() {
		return cdFavorecido;
	}

	public void setCdFavorecido(Long cdFavorecido) {
		this.cdFavorecido = cdFavorecido;
	}

	public Integer getCdTipoTela() {
		return cdTipoTela;
	}

	public void setCdTipoTela(Integer cdTipoTela) {
		this.cdTipoTela = cdTipoTela;
	}

	public String getBancoAgenciaContaCreditoFormatado() {
		return bancoAgenciaContaCreditoFormatado;
	}

	public void setBancoAgenciaContaCreditoFormatado(
			String bancoAgenciaContaCreditoFormatado) {
		this.bancoAgenciaContaCreditoFormatado = bancoAgenciaContaCreditoFormatado;
	}

	public String getBancoFormatado() {
		return bancoFormatado;
	}

	public void setBancoFormatado(String bancoFormatado) {
		this.bancoFormatado = bancoFormatado;
	}

	public String getAgenciaFormatada() {
		return agenciaFormatada;
	}

	public void setAgenciaFormatada(String agenciaFormatada) {
		this.agenciaFormatada = agenciaFormatada;
	}

	public String getContaFormatada() {
		return contaFormatada;
	}

	public void setContaFormatada(String contaFormatada) {
		this.contaFormatada = contaFormatada;
	}

	public String getFavorecidoBeneficiarioFormatado() {
		return favorecidoBeneficiarioFormatado;
	}

	public void setFavorecidoBeneficiarioFormatado(
			String favorecidoBeneficiarioFormatado) {
		this.favorecidoBeneficiarioFormatado = favorecidoBeneficiarioFormatado;
	}

	public boolean isCheck() {
		return check;
	}

	public void setCheck(boolean check) {
		this.check = check;
	}

	public String getDsEfetivacaoPagamento() {
		return dsEfetivacaoPagamento;
	}

	public void setDsEfetivacaoPagamento(String dsEfetivacaoPagamento) {
		this.dsEfetivacaoPagamento = dsEfetivacaoPagamento;
	}

	public String getDsIndicadorAutorizacao() {
		return dsIndicadorAutorizacao;
	}

	public void setDsIndicadorAutorizacao(String dsIndicadorAutorizacao) {
		this.dsIndicadorAutorizacao = dsIndicadorAutorizacao;
	}

	public Long getCdLoteInterno() {
		return cdLoteInterno;
	}

	public void setCdLoteInterno(Long cdLoteInterno) {
		this.cdLoteInterno = cdLoteInterno;
	}

	public String getDsTipoLayout() {
		return dsTipoLayout;
	}

	public void setDsTipoLayout(String dsTipoLayout) {
		this.dsTipoLayout = dsTipoLayout;
	}

    /**
     * Nome: getDsIndicadorPagamento
     *
     * @return dsIndicadorPagamento
     */
    public String getDsIndicadorPagamento() {
        return dsIndicadorPagamento;
    }

    /**
     * Nome: setDsIndicadorPagamento
     *
     * @param dsIndicadorPagamento
     */
    public void setDsIndicadorPagamento(String dsIndicadorPagamento) {
        this.dsIndicadorPagamento = dsIndicadorPagamento;
    }

	public String getCdIspbPagtoDestino() {
		return cdIspbPagtoDestino;
	}

	public void setCdIspbPagtoDestino(String cdIspbPagtoDestino) {
		this.cdIspbPagtoDestino = cdIspbPagtoDestino;
	}

	public String getContaPagtoDestino() {
		return contaPagtoDestino;
	}

	public void setContaPagtoDestino(String contaPagtoDestino) {
		this.contaPagtoDestino = contaPagtoDestino;
	}
	
}