/*
 * Nome: br.com.bradesco.web.pgit.service.report.contrato.data
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 05/02/2016
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.report.mantercontrato.contrato.data;

/**
 * Nome: ModalidadeCondicao
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ModalidadeCondicao {

    /** Atributo flagModalidade. */
    private String flagModalidade = null;
    
    /** Atributo tituloModalidade. */
    private String tituloModalidade = null;

    /** Atributo flagProcessamentoCiclico. */
    private String flagProcessamentoCiclico = null;

    /** Atributo flagProcessamentoDiario. */
    private String flagProcessamentoDiario = null;

    /** Atributo flagPagDiaNaoUtilAntecipa. */
    private String flagPagDiaNaoUtilAntecipa = null;

    /** Atributo flagPagDiaNaoUtilPosterga. */
    private String flagPagDiaNaoUtilPosterga = null;

    /** Atributo flagPagFeriadoAcata. */
    private String flagPagFeriadoAcata = null;

    /** Atributo flagPagFeriadoAcataReagendamento. */
    private String flagPagFeriadoAcataReagendamento = null;

    /** Atributo flagPagFeriadoAntecipa. */
    private String flagPagFeriadoAntecipa = null;

    /** Atributo flagPagFeriadoPosterga. */
    private String flagPagFeriadoPosterga = null;

    /** Atributo flagPagFeriadoRejeita. */
    private String flagPagFeriadoRejeita = null;

    /** Atributo qtdeDiasFloating. */
    private String qtdeDiasFloating = null;

    /** Atributo qtdeDiasRepique. */
    private String qtdeDiasRepique = null;

    /** Atributo diasRepiqueSim. */
    private String diasRepiqueSim = null;

    /**
     * Instancia um novo ModalidadeCondicao
     * 
     * @see
     */
    public ModalidadeCondicao() {
        super();
    }

    /** Atributo diasRepiqueNao. */
    private String diasRepiqueNao = null;

    /**
     * Nome: getFlagModalidade.
     *
     * @return flagModalidade
     */
    public String getFlagModalidade() {
        return flagModalidade;
    }

    /**
     * Nome: setFlagModalidade.
     *
     * @param flagModalidade the flag modalidade
     */
    public void setFlagModalidade(String flagModalidade) {
        this.flagModalidade = flagModalidade;
    }

    /**
     * Nome: getTituloModalidade.
     *
     * @return tituloModalidade
     */
    public String getTituloModalidade() {
        return tituloModalidade;
    }

    /**
     * Nome: setTituloModalidade.
     *
     * @param tituloModalidade the titulo modalidade
     */
    public void setTituloModalidade(String tituloModalidade) {
        this.tituloModalidade = tituloModalidade;
    }

    /**
     * Nome: getFlagProcessamentoCiclico.
     *
     * @return flagProcessamentoCiclico
     */
    public String getFlagProcessamentoCiclico() {
        return flagProcessamentoCiclico;
    }

    /**
     * Nome: setFlagProcessamentoCiclico.
     *
     * @param flagProcessamentoCiclico the flag processamento ciclico
     */
    public void setFlagProcessamentoCiclico(String flagProcessamentoCiclico) {
        this.flagProcessamentoCiclico = flagProcessamentoCiclico;
    }

    /**
     * Nome: getFlagProcessamentoDiario.
     *
     * @return flagProcessamentoDiario
     */
    public String getFlagProcessamentoDiario() {
        return flagProcessamentoDiario;
    }

    /**
     * Nome: setFlagProcessamentoDiario.
     *
     * @param flagProcessamentoDiario the flag processamento diario
     */
    public void setFlagProcessamentoDiario(String flagProcessamentoDiario) {
        this.flagProcessamentoDiario = flagProcessamentoDiario;
    }

    /**
     * Nome: getFlagPagDiaNaoUtilAntecipa.
     *
     * @return flagPagDiaNaoUtilAntecipa
     */
    public String getFlagPagDiaNaoUtilAntecipa() {
        return flagPagDiaNaoUtilAntecipa;
    }

    /**
     * Nome: setFlagPagDiaNaoUtilAntecipa.
     *
     * @param flagPagDiaNaoUtilAntecipa the flag pag dia nao util antecipa
     */
    public void setFlagPagDiaNaoUtilAntecipa(String flagPagDiaNaoUtilAntecipa) {
        this.flagPagDiaNaoUtilAntecipa = flagPagDiaNaoUtilAntecipa;
    }

    /**
     * Nome: getFlagPagDiaNaoUtilPosterga.
     *
     * @return flagPagDiaNaoUtilPosterga
     */
    public String getFlagPagDiaNaoUtilPosterga() {
        return flagPagDiaNaoUtilPosterga;
    }

    /**
     * Nome: setFlagPagDiaNaoUtilPosterga.
     *
     * @param flagPagDiaNaoUtilPosterga the flag pag dia nao util posterga
     */
    public void setFlagPagDiaNaoUtilPosterga(String flagPagDiaNaoUtilPosterga) {
        this.flagPagDiaNaoUtilPosterga = flagPagDiaNaoUtilPosterga;
    }

    /**
     * Nome: getFlagPagFeriadoAcata.
     *
     * @return flagPagFeriadoAcata
     */
    public String getFlagPagFeriadoAcata() {
        return flagPagFeriadoAcata;
    }

    /**
     * Nome: setFlagPagFeriadoAcata.
     *
     * @param flagPagFeriadoAcata the flag pag feriado acata
     */
    public void setFlagPagFeriadoAcata(String flagPagFeriadoAcata) {
        this.flagPagFeriadoAcata = flagPagFeriadoAcata;
    }

    /**
     * Nome: getFlagPagFeriadoAcataReagendamento.
     *
     * @return flagPagFeriadoAcataReagendamento
     */
    public String getFlagPagFeriadoAcataReagendamento() {
        return flagPagFeriadoAcataReagendamento;
    }

    /**
     * Nome: setFlagPagFeriadoAcataReagendamento.
     *
     * @param flagPagFeriadoAcataReagendamento the flag pag feriado acata reagendamento
     */
    public void setFlagPagFeriadoAcataReagendamento(String flagPagFeriadoAcataReagendamento) {
        this.flagPagFeriadoAcataReagendamento = flagPagFeriadoAcataReagendamento;
    }

    /**
     * Nome: getFlagPagFeriadoAntecipa.
     *
     * @return flagPagFeriadoAntecipa
     */
    public String getFlagPagFeriadoAntecipa() {
        return flagPagFeriadoAntecipa;
    }

    /**
     * Nome: setFlagPagFeriadoAntecipa.
     *
     * @param flagPagFeriadoAntecipa the flag pag feriado antecipa
     */
    public void setFlagPagFeriadoAntecipa(String flagPagFeriadoAntecipa) {
        this.flagPagFeriadoAntecipa = flagPagFeriadoAntecipa;
    }

    /**
     * Nome: getFlagPagFeriadoPosterga.
     *
     * @return flagPagFeriadoPosterga
     */
    public String getFlagPagFeriadoPosterga() {
        return flagPagFeriadoPosterga;
    }

    /**
     * Nome: setFlagPagFeriadoPosterga.
     *
     * @param flagPagFeriadoPosterga the flag pag feriado posterga
     */
    public void setFlagPagFeriadoPosterga(String flagPagFeriadoPosterga) {
        this.flagPagFeriadoPosterga = flagPagFeriadoPosterga;
    }

    /**
     * Nome: getQtdeDiasFloating.
     *
     * @return qtdeDiasFloating
     */
    public String getQtdeDiasFloating() {
        return qtdeDiasFloating;
    }

    /**
     * Nome: setQtdeDiasFloating.
     *
     * @param qtdeDiasFloating the qtde dias floating
     */
    public void setQtdeDiasFloating(String qtdeDiasFloating) {
        this.qtdeDiasFloating = qtdeDiasFloating;
    }

    /**
     * Nome: getFlagPagFeriadoRejeita.
     *
     * @return flagPagFeriadoRejeita
     */
    public String getFlagPagFeriadoRejeita() {
        return flagPagFeriadoRejeita;
    }

    /**
     * Nome: setFlagPagFeriadoRejeita.
     *
     * @param flagPagFeriadoRejeita the flag pag feriado rejeita
     */
    public void setFlagPagFeriadoRejeita(String flagPagFeriadoRejeita) {
        this.flagPagFeriadoRejeita = flagPagFeriadoRejeita;
    }

    /**
     * Nome: getQtdeDiasRepique.
     *
     * @return qtdeDiasRepique
     */
    public String getQtdeDiasRepique() {
        return qtdeDiasRepique;
    }

    /**
     * Nome: setQtdeDiasRepique.
     *
     * @param qtdeDiasRepique the qtde dias repique
     */
    public void setQtdeDiasRepique(String qtdeDiasRepique) {
        this.qtdeDiasRepique = qtdeDiasRepique;
    }

    /**
     * Nome: getDiasRepiqueSim.
     *
     * @return diasRepiqueSim
     */
    public String getDiasRepiqueSim() {
        return diasRepiqueSim;
    }

    /**
     * Nome: setDiasRepiqueSim.
     *
     * @param diasRepiqueSim the dias repique sim
     */
    public void setDiasRepiqueSim(String diasRepiqueSim) {
        this.diasRepiqueSim = diasRepiqueSim;
    }

    /**
     * Nome: getDiasRepiqueNao.
     *
     * @return diasRepiqueNao
     */
    public String getDiasRepiqueNao() {
        return diasRepiqueNao;
    }

    /**
     * Nome: setDiasRepiqueNao.
     *
     * @param diasRepiqueNao the dias repique nao
     */
    public void setDiasRepiqueNao(String diasRepiqueNao) {
        this.diasRepiqueNao = diasRepiqueNao;
    }
}
