/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PilotoIntranet/JavaSource/br/com/bradesco/web/pgit/service/business/confrontodependencia/impl/ConfrontoDepServiceImpl.java,v $
 * $Id: ConfrontoDepServiceImpl.java,v 1.2 2009/05/13 19:39:29 cpm.com.br\heslei.silva Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: ConfrontoDepServiceImpl.java,v $
 * Revision 1.2  2009/05/13 19:39:29  cpm.com.br\heslei.silva
 * voltando para vers�o do branch 2.0
 *
 * Revision 1.1.2.1  2009/05/05 14:24:44  corporate\marcio.alves
 * Adicionando pagina��o ao piloto intranet
 *
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */
 
package br.com.bradesco.web.pgit.service.business.confrontodep.impl;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import br.com.bradesco.web.aq.application.error.BradescoApplicationException;
import br.com.bradesco.web.pgit.service.business.confrontodep.IConfrontoDepService;
import br.com.bradesco.web.pgit.service.business.confrontodep.bean.ConfrontoDepVO;
import br.com.bradesco.web.pgit.service.data.pdc.FactoryAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarconfrontodependencia.request.ListConfDependenciaRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarconfrontodependencia.response.ListConfDependResponse;
import br.com.bradesco.web.pgit.utils.SiteUtil;


/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Implementa��o do adaptador: ConfrontoDependencia
 * </p>
 * 
 * @comment C�DIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public class ConfrontoDepServiceImpl implements IConfrontoDepService {

	/** Atributo factoryAdapter. */
	private FactoryAdapter factoryAdapter;
	
    /**
     * Construtor.
     */
    public ConfrontoDepServiceImpl() {
        // TODO: Implementa��o
    }
    
    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.confrontodep.IConfrontoDepService#listarConfrontoDependencia(br.com.bradesco.web.pgit.service.business.confrontodep.bean.ConfrontoDepVO)
     */
    public List listarConfrontoDependencia(ConfrontoDepVO pConfrontoDependenciaVO) {
        List lConfrontos = new ArrayList();
        ListConfDependenciaRequest lRequest = new ListConfDependenciaRequest();
        ListConfDependResponse lResponse = null;
        
        lRequest.setMaxOcorrencia(50);

        // Verifica se o campos de entrada � vazio.
        // Se sim, passamos o valor padr�o
        // Se n�o, passamos o valor da entrada:

        try {
        	if (!(SiteUtil.isEmptyOrNull(pConfrontoDependenciaVO.getDtReferenciaDe()))) {
                lRequest.setDtReferenciaDe(
                        SiteUtil.changeStringDateFormat(pConfrontoDependenciaVO.getDtReferenciaDe(), "dd/MM/yyyy",
                                "dd.MM.yyyy"));
            } else {
                lRequest.setDtReferenciaDe("");
            }

            if (!(SiteUtil.isEmptyOrNull(pConfrontoDependenciaVO.getDtReferenciaAte()))) {
                lRequest.setDtReferenciaAte(
                        SiteUtil.changeStringDateFormat(pConfrontoDependenciaVO.getDtReferenciaAte(), "dd/MM/yyyy",
                                "dd.MM.yyyy"));
            } else {
                lRequest.setDtReferenciaAte("");
            }
        } catch (ParseException e) {
			throw new BradescoApplicationException(e);
		}

        if (!(SiteUtil.isEmptyOrNull(pConfrontoDependenciaVO.getCodEmpresa()))) {
            lRequest.setCodEmpresa(Integer.valueOf(pConfrontoDependenciaVO.getCodEmpresa()).intValue());
        } else {
            lRequest.setCodEmpresa(0);
        }

        if (!(SiteUtil.isEmptyOrNull(pConfrontoDependenciaVO.getCodDependencia()))) {
            lRequest.setCodDependencia(Integer.valueOf(pConfrontoDependenciaVO.getCodDependencia()).intValue());
        } else {
            lRequest.setCodDependencia(0);
        }
        
        // verificando se tipo de conta foi definido...
        if (pConfrontoDependenciaVO.getTipoConta() != null && !pConfrontoDependenciaVO.getTipoConta().equals("")) {
        	
        	lRequest.setTipoConta(Integer.parseInt(pConfrontoDependenciaVO.getTipoConta()));
			
		}
        
        // Fazemos a chamada do processo.
        //lResponse = getFactoryAdapter().getListarConfrontoDependenciaPDCAdapter().invokeProcess(lRequest);

        // Para cada item retornado do processo, adicionamos a lista de retorno.
        for (int i = 0; i < lResponse.getOcorrenciaCount(); i++) {
            
            if (!SiteUtil.isEmptyOrNull(lResponse.getOcorrencia(i).getDtContabil())
                    && !lResponse.getOcorrencia(i).getDtContabil().equals("0")) {
                
                ConfrontoDepVO lConfronto = new ConfrontoDepVO();
                lConfronto.setCdConta(String.valueOf(lResponse.getOcorrencia(i).getCdConta()));
                lConfronto.setCodTipoSaldo(String.valueOf(lResponse.getOcorrencia(i).getCodTipoSaldo()));
                lConfronto.setDescConta(lResponse.getOcorrencia(i).getDescConta());
                lConfronto.setVlSaldoContabil(String.valueOf(lResponse.getOcorrencia(i).getVlSaldoContabil()));
                lConfronto.setVlSaldoDiferenca(String.valueOf(lResponse.getOcorrencia(i).getVlSaldoDiferenca()));
                lConfronto.setVlSaldoProduto(String.valueOf(lResponse.getOcorrencia(i).getVlSaldoProduto()));
                lConfronto.setDtContabil(lResponse.getOcorrencia(i).getDtContabil());
                
                lConfrontos.add(lConfronto);            
            }
        }
        return lConfrontos;
    }

	/**
	 * Get: factoryAdapter.
	 *
	 * @return factoryAdapter
	 */
	public FactoryAdapter getFactoryAdapter() {
		return factoryAdapter;
	}

	/**
	 * Set: factoryAdapter.
	 *
	 * @param factoryAdapter the factory adapter
	 */
	public void setFactoryAdapter(FactoryAdapter factoryAdapter) {
		this.factoryAdapter = factoryAdapter;
	}
}

