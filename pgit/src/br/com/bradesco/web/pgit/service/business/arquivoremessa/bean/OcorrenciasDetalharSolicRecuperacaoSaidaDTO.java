/*
 * Nome: br.com.bradesco.web.pgit.service.business.arquivoremessa.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.arquivoremessa.bean;

import java.math.BigDecimal;

/**
 * Nome: OcorrenciasDetalharSolicRecuperacaoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class OcorrenciasDetalharSolicRecuperacaoSaidaDTO {
	
	/** Atributo cdPessoa. */
	private Long cdPessoa;
	
	/** Atributo cdCnpjCpf. */
	private String cdCnpjCpf;
	
	/** Atributo dsPessoa. */
	private String dsPessoa;
	
	/** Atributo cdTipoLayoutArquivo. */
	private Integer cdTipoLayoutArquivo;
	
	/** Atributo dsTipoLayoutArquivo. */
	private String dsTipoLayoutArquivo;
	
	/** Atributo cdClienteTransferenciaArquivo. */
	private Long cdClienteTransferenciaArquivo;
	
	/** Atributo nrSequenciaRemessa. */
	private Long nrSequenciaRemessa;
	
	/** Atributo hrInclusaoRemessa. */
	private String hrInclusaoRemessa;
	
	/** Atributo dsSituacaoProcessamento. */
	private String dsSituacaoProcessamento;
	
	/** Atributo dsCondicaoProcessamento. */
	private String dsCondicaoProcessamento;
	
	/** Atributo dsMeioTransmissao. */
	private String dsMeioTransmissao;
	
	/** Atributo qtdeRegistro. */
	private String qtdeRegistro;
	
	/** Atributo vlrRegistro. */
	private BigDecimal vlrRegistro;
	
	/** Atributo dataFormatada. */
	private String dataFormatada;
	
	
	
	
	
	/**
	 * Get: dataFormatada.
	 *
	 * @return dataFormatada
	 */
	public String getDataFormatada() {
		return dataFormatada;
	}
	
	
	/**
	 * Set: dataFormatada.
	 *
	 * @param dataFormatada the data formatada
	 */
	public void setDataFormatada(String dataFormatada) {
		this.dataFormatada = dataFormatada;
	}
	
	/**
	 * Get: cdClienteTransferenciaArquivo.
	 *
	 * @return cdClienteTransferenciaArquivo
	 */
	public Long getCdClienteTransferenciaArquivo() {
		return cdClienteTransferenciaArquivo;
	}
	
	/**
	 * Set: cdClienteTransferenciaArquivo.
	 *
	 * @param cdClienteTransferenciaArquivo the cd cliente transferencia arquivo
	 */
	public void setCdClienteTransferenciaArquivo(Long cdClienteTransferenciaArquivo) {
		this.cdClienteTransferenciaArquivo = cdClienteTransferenciaArquivo;
	}
	
	/**
	 * Get: cdCnpjCpf.
	 *
	 * @return cdCnpjCpf
	 */
	public String getCdCnpjCpf() {
		return cdCnpjCpf;
	}
	
	/**
	 * Set: cdCnpjCpf.
	 *
	 * @param cdCnpjCpf the cd cnpj cpf
	 */
	public void setCdCnpjCpf(String cdCnpjCpf) {
		this.cdCnpjCpf = cdCnpjCpf;
	}
	
	/**
	 * Get: cdPessoa.
	 *
	 * @return cdPessoa
	 */
	public Long getCdPessoa() {
		return cdPessoa;
	}
	
	/**
	 * Set: cdPessoa.
	 *
	 * @param cdPessoa the cd pessoa
	 */
	public void setCdPessoa(Long cdPessoa) {
		this.cdPessoa = cdPessoa;
	}
	
	/**
	 * Get: cdTipoLayoutArquivo.
	 *
	 * @return cdTipoLayoutArquivo
	 */
	public Integer getCdTipoLayoutArquivo() {
		return cdTipoLayoutArquivo;
	}
	
	/**
	 * Set: cdTipoLayoutArquivo.
	 *
	 * @param cdTipoLayoutArquivo the cd tipo layout arquivo
	 */
	public void setCdTipoLayoutArquivo(Integer cdTipoLayoutArquivo) {
		this.cdTipoLayoutArquivo = cdTipoLayoutArquivo;
	}
	
	/**
	 * Get: dsCondicaoProcessamento.
	 *
	 * @return dsCondicaoProcessamento
	 */
	public String getDsCondicaoProcessamento() {
		return dsCondicaoProcessamento;
	}
	
	/**
	 * Set: dsCondicaoProcessamento.
	 *
	 * @param dsCondicaoProcessamento the ds condicao processamento
	 */
	public void setDsCondicaoProcessamento(String dsCondicaoProcessamento) {
		this.dsCondicaoProcessamento = dsCondicaoProcessamento;
	}
	
	/**
	 * Get: dsMeioTransmissao.
	 *
	 * @return dsMeioTransmissao
	 */
	public String getDsMeioTransmissao() {
		return dsMeioTransmissao;
	}
	
	/**
	 * Set: dsMeioTransmissao.
	 *
	 * @param dsMeioTransmissao the ds meio transmissao
	 */
	public void setDsMeioTransmissao(String dsMeioTransmissao) {
		this.dsMeioTransmissao = dsMeioTransmissao;
	}
	
	/**
	 * Get: dsPessoa.
	 *
	 * @return dsPessoa
	 */
	public String getDsPessoa() {
		return dsPessoa;
	}
	
	/**
	 * Set: dsPessoa.
	 *
	 * @param dsPessoa the ds pessoa
	 */
	public void setDsPessoa(String dsPessoa) {
		this.dsPessoa = dsPessoa;
	}
	
	/**
	 * Get: dsSituacaoProcessamento.
	 *
	 * @return dsSituacaoProcessamento
	 */
	public String getDsSituacaoProcessamento() {
		return dsSituacaoProcessamento;
	}
	
	/**
	 * Set: dsSituacaoProcessamento.
	 *
	 * @param dsSituacaoProcessamento the ds situacao processamento
	 */
	public void setDsSituacaoProcessamento(String dsSituacaoProcessamento) {
		this.dsSituacaoProcessamento = dsSituacaoProcessamento;
	}
	
	/**
	 * Get: dsTipoLayoutArquivo.
	 *
	 * @return dsTipoLayoutArquivo
	 */
	public String getDsTipoLayoutArquivo() {
		return dsTipoLayoutArquivo;
	}
	
	/**
	 * Set: dsTipoLayoutArquivo.
	 *
	 * @param dsTipoLayoutArquivo the ds tipo layout arquivo
	 */
	public void setDsTipoLayoutArquivo(String dsTipoLayoutArquivo) {
		this.dsTipoLayoutArquivo = dsTipoLayoutArquivo;
	}
	
	/**
	 * Get: hrInclusaoRemessa.
	 *
	 * @return hrInclusaoRemessa
	 */
	public String getHrInclusaoRemessa() {
		return hrInclusaoRemessa;
	}
	
	/**
	 * Set: hrInclusaoRemessa.
	 *
	 * @param hrInclusaoRemessa the hr inclusao remessa
	 */
	public void setHrInclusaoRemessa(String hrInclusaoRemessa) {
		this.hrInclusaoRemessa = hrInclusaoRemessa;
	}
	
	/**
	 * Get: nrSequenciaRemessa.
	 *
	 * @return nrSequenciaRemessa
	 */
	public Long getNrSequenciaRemessa() {
		return nrSequenciaRemessa;
	}
	
	/**
	 * Set: nrSequenciaRemessa.
	 *
	 * @param nrSequenciaRemessa the nr sequencia remessa
	 */
	public void setNrSequenciaRemessa(Long nrSequenciaRemessa) {
		this.nrSequenciaRemessa = nrSequenciaRemessa;
	}
	
	/**
	 * Get: qtdeRegistro.
	 *
	 * @return qtdeRegistro
	 */
	public String getQtdeRegistro() {
		return qtdeRegistro;
	}
	
	/**
	 * Set: qtdeRegistro.
	 *
	 * @param qtdeRegistro the qtde registro
	 */
	public void setQtdeRegistro(String qtdeRegistro) {
		this.qtdeRegistro = qtdeRegistro;
	}
	
	/**
	 * Get: vlrRegistro.
	 *
	 * @return vlrRegistro
	 */
	public BigDecimal getVlrRegistro() {
		return vlrRegistro;
	}
	
	/**
	 * Set: vlrRegistro.
	 *
	 * @param vlrRegistro the vlr registro
	 */
	public void setVlrRegistro(BigDecimal vlrRegistro) {
		this.vlrRegistro = vlrRegistro;
	}
	
	
	
	

}
