/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.alteraroperacaoservicopagtointegrado.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.math.BigDecimal;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class AlterarOperacaoServicoPagtoIntegradoRequest.
 * 
 * @version $Revision$ $Date$
 */
public class AlterarOperacaoServicoPagtoIntegradoRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdProdutoServicoOperacional
     */
    private int _cdProdutoServicoOperacional = 0;

    /**
     * keeps track of state for field: _cdProdutoServicoOperacional
     */
    private boolean _has_cdProdutoServicoOperacional;

    /**
     * Field _cdProdutoOperacionalRelacionado
     */
    private int _cdProdutoOperacionalRelacionado = 0;

    /**
     * keeps track of state for field:
     * _cdProdutoOperacionalRelacionado
     */
    private boolean _has_cdProdutoOperacionalRelacionado;

    /**
     * Field _cdOperacaoProdutoServico
     */
    private int _cdOperacaoProdutoServico = 0;

    /**
     * keeps track of state for field: _cdOperacaoProdutoServico
     */
    private boolean _has_cdOperacaoProdutoServico;

    /**
     * Field _cdOperacaoServicoPagamentoIntegrado
     */
    private int _cdOperacaoServicoPagamentoIntegrado = 0;

    /**
     * keeps track of state for field:
     * _cdOperacaoServicoPagamentoIntegrado
     */
    private boolean _has_cdOperacaoServicoPagamentoIntegrado;

    /**
     * Field _cdNaturezaOperacaoPagamento
     */
    private int _cdNaturezaOperacaoPagamento = 0;

    /**
     * keeps track of state for field: _cdNaturezaOperacaoPagamento
     */
    private boolean _has_cdNaturezaOperacaoPagamento;

    /**
     * Field _cdTipoAlcadaTarifa
     */
    private int _cdTipoAlcadaTarifa = 0;

    /**
     * keeps track of state for field: _cdTipoAlcadaTarifa
     */
    private boolean _has_cdTipoAlcadaTarifa;

    /**
     * Field _vrAlcadaTarifaAgencia
     */
    private java.math.BigDecimal _vrAlcadaTarifaAgencia = new java.math.BigDecimal("0");

    /**
     * Field _pcAlcadaTarifaAgencia
     */
    private java.math.BigDecimal _pcAlcadaTarifaAgencia = new java.math.BigDecimal("0");


      //----------------/
     //- Constructors -/
    //----------------/

    public AlterarOperacaoServicoPagtoIntegradoRequest() 
     {
        super();
        setVrAlcadaTarifaAgencia(new java.math.BigDecimal("0"));
        setPcAlcadaTarifaAgencia(new java.math.BigDecimal("0"));
    } //-- br.com.bradesco.web.pgit.service.data.pdc.alteraroperacaoservicopagtointegrado.request.AlterarOperacaoServicoPagtoIntegradoRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdNaturezaOperacaoPagamento
     * 
     */
    public void deleteCdNaturezaOperacaoPagamento()
    {
        this._has_cdNaturezaOperacaoPagamento= false;
    } //-- void deleteCdNaturezaOperacaoPagamento() 

    /**
     * Method deleteCdOperacaoProdutoServico
     * 
     */
    public void deleteCdOperacaoProdutoServico()
    {
        this._has_cdOperacaoProdutoServico= false;
    } //-- void deleteCdOperacaoProdutoServico() 

    /**
     * Method deleteCdOperacaoServicoPagamentoIntegrado
     * 
     */
    public void deleteCdOperacaoServicoPagamentoIntegrado()
    {
        this._has_cdOperacaoServicoPagamentoIntegrado= false;
    } //-- void deleteCdOperacaoServicoPagamentoIntegrado() 

    /**
     * Method deleteCdProdutoOperacionalRelacionado
     * 
     */
    public void deleteCdProdutoOperacionalRelacionado()
    {
        this._has_cdProdutoOperacionalRelacionado= false;
    } //-- void deleteCdProdutoOperacionalRelacionado() 

    /**
     * Method deleteCdProdutoServicoOperacional
     * 
     */
    public void deleteCdProdutoServicoOperacional()
    {
        this._has_cdProdutoServicoOperacional= false;
    } //-- void deleteCdProdutoServicoOperacional() 

    /**
     * Method deleteCdTipoAlcadaTarifa
     * 
     */
    public void deleteCdTipoAlcadaTarifa()
    {
        this._has_cdTipoAlcadaTarifa= false;
    } //-- void deleteCdTipoAlcadaTarifa() 

    /**
     * Returns the value of field 'cdNaturezaOperacaoPagamento'.
     * 
     * @return int
     * @return the value of field 'cdNaturezaOperacaoPagamento'.
     */
    public int getCdNaturezaOperacaoPagamento()
    {
        return this._cdNaturezaOperacaoPagamento;
    } //-- int getCdNaturezaOperacaoPagamento() 

    /**
     * Returns the value of field 'cdOperacaoProdutoServico'.
     * 
     * @return int
     * @return the value of field 'cdOperacaoProdutoServico'.
     */
    public int getCdOperacaoProdutoServico()
    {
        return this._cdOperacaoProdutoServico;
    } //-- int getCdOperacaoProdutoServico() 

    /**
     * Returns the value of field
     * 'cdOperacaoServicoPagamentoIntegrado'.
     * 
     * @return int
     * @return the value of field
     * 'cdOperacaoServicoPagamentoIntegrado'.
     */
    public int getCdOperacaoServicoPagamentoIntegrado()
    {
        return this._cdOperacaoServicoPagamentoIntegrado;
    } //-- int getCdOperacaoServicoPagamentoIntegrado() 

    /**
     * Returns the value of field
     * 'cdProdutoOperacionalRelacionado'.
     * 
     * @return int
     * @return the value of field 'cdProdutoOperacionalRelacionado'.
     */
    public int getCdProdutoOperacionalRelacionado()
    {
        return this._cdProdutoOperacionalRelacionado;
    } //-- int getCdProdutoOperacionalRelacionado() 

    /**
     * Returns the value of field 'cdProdutoServicoOperacional'.
     * 
     * @return int
     * @return the value of field 'cdProdutoServicoOperacional'.
     */
    public int getCdProdutoServicoOperacional()
    {
        return this._cdProdutoServicoOperacional;
    } //-- int getCdProdutoServicoOperacional() 

    /**
     * Returns the value of field 'cdTipoAlcadaTarifa'.
     * 
     * @return int
     * @return the value of field 'cdTipoAlcadaTarifa'.
     */
    public int getCdTipoAlcadaTarifa()
    {
        return this._cdTipoAlcadaTarifa;
    } //-- int getCdTipoAlcadaTarifa() 

    /**
     * Returns the value of field 'pcAlcadaTarifaAgencia'.
     * 
     * @return BigDecimal
     * @return the value of field 'pcAlcadaTarifaAgencia'.
     */
    public java.math.BigDecimal getPcAlcadaTarifaAgencia()
    {
        return this._pcAlcadaTarifaAgencia;
    } //-- java.math.BigDecimal getPcAlcadaTarifaAgencia() 

    /**
     * Returns the value of field 'vrAlcadaTarifaAgencia'.
     * 
     * @return BigDecimal
     * @return the value of field 'vrAlcadaTarifaAgencia'.
     */
    public java.math.BigDecimal getVrAlcadaTarifaAgencia()
    {
        return this._vrAlcadaTarifaAgencia;
    } //-- java.math.BigDecimal getVrAlcadaTarifaAgencia() 

    /**
     * Method hasCdNaturezaOperacaoPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdNaturezaOperacaoPagamento()
    {
        return this._has_cdNaturezaOperacaoPagamento;
    } //-- boolean hasCdNaturezaOperacaoPagamento() 

    /**
     * Method hasCdOperacaoProdutoServico
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdOperacaoProdutoServico()
    {
        return this._has_cdOperacaoProdutoServico;
    } //-- boolean hasCdOperacaoProdutoServico() 

    /**
     * Method hasCdOperacaoServicoPagamentoIntegrado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdOperacaoServicoPagamentoIntegrado()
    {
        return this._has_cdOperacaoServicoPagamentoIntegrado;
    } //-- boolean hasCdOperacaoServicoPagamentoIntegrado() 

    /**
     * Method hasCdProdutoOperacionalRelacionado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdutoOperacionalRelacionado()
    {
        return this._has_cdProdutoOperacionalRelacionado;
    } //-- boolean hasCdProdutoOperacionalRelacionado() 

    /**
     * Method hasCdProdutoServicoOperacional
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdutoServicoOperacional()
    {
        return this._has_cdProdutoServicoOperacional;
    } //-- boolean hasCdProdutoServicoOperacional() 

    /**
     * Method hasCdTipoAlcadaTarifa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoAlcadaTarifa()
    {
        return this._has_cdTipoAlcadaTarifa;
    } //-- boolean hasCdTipoAlcadaTarifa() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdNaturezaOperacaoPagamento'.
     * 
     * @param cdNaturezaOperacaoPagamento the value of field
     * 'cdNaturezaOperacaoPagamento'.
     */
    public void setCdNaturezaOperacaoPagamento(int cdNaturezaOperacaoPagamento)
    {
        this._cdNaturezaOperacaoPagamento = cdNaturezaOperacaoPagamento;
        this._has_cdNaturezaOperacaoPagamento = true;
    } //-- void setCdNaturezaOperacaoPagamento(int) 

    /**
     * Sets the value of field 'cdOperacaoProdutoServico'.
     * 
     * @param cdOperacaoProdutoServico the value of field
     * 'cdOperacaoProdutoServico'.
     */
    public void setCdOperacaoProdutoServico(int cdOperacaoProdutoServico)
    {
        this._cdOperacaoProdutoServico = cdOperacaoProdutoServico;
        this._has_cdOperacaoProdutoServico = true;
    } //-- void setCdOperacaoProdutoServico(int) 

    /**
     * Sets the value of field
     * 'cdOperacaoServicoPagamentoIntegrado'.
     * 
     * @param cdOperacaoServicoPagamentoIntegrado the value of
     * field 'cdOperacaoServicoPagamentoIntegrado'.
     */
    public void setCdOperacaoServicoPagamentoIntegrado(int cdOperacaoServicoPagamentoIntegrado)
    {
        this._cdOperacaoServicoPagamentoIntegrado = cdOperacaoServicoPagamentoIntegrado;
        this._has_cdOperacaoServicoPagamentoIntegrado = true;
    } //-- void setCdOperacaoServicoPagamentoIntegrado(int) 

    /**
     * Sets the value of field 'cdProdutoOperacionalRelacionado'.
     * 
     * @param cdProdutoOperacionalRelacionado the value of field
     * 'cdProdutoOperacionalRelacionado'.
     */
    public void setCdProdutoOperacionalRelacionado(int cdProdutoOperacionalRelacionado)
    {
        this._cdProdutoOperacionalRelacionado = cdProdutoOperacionalRelacionado;
        this._has_cdProdutoOperacionalRelacionado = true;
    } //-- void setCdProdutoOperacionalRelacionado(int) 

    /**
     * Sets the value of field 'cdProdutoServicoOperacional'.
     * 
     * @param cdProdutoServicoOperacional the value of field
     * 'cdProdutoServicoOperacional'.
     */
    public void setCdProdutoServicoOperacional(int cdProdutoServicoOperacional)
    {
        this._cdProdutoServicoOperacional = cdProdutoServicoOperacional;
        this._has_cdProdutoServicoOperacional = true;
    } //-- void setCdProdutoServicoOperacional(int) 

    /**
     * Sets the value of field 'cdTipoAlcadaTarifa'.
     * 
     * @param cdTipoAlcadaTarifa the value of field
     * 'cdTipoAlcadaTarifa'.
     */
    public void setCdTipoAlcadaTarifa(int cdTipoAlcadaTarifa)
    {
        this._cdTipoAlcadaTarifa = cdTipoAlcadaTarifa;
        this._has_cdTipoAlcadaTarifa = true;
    } //-- void setCdTipoAlcadaTarifa(int) 

    /**
     * Sets the value of field 'pcAlcadaTarifaAgencia'.
     * 
     * @param pcAlcadaTarifaAgencia the value of field
     * 'pcAlcadaTarifaAgencia'.
     */
    public void setPcAlcadaTarifaAgencia(java.math.BigDecimal pcAlcadaTarifaAgencia)
    {
        this._pcAlcadaTarifaAgencia = pcAlcadaTarifaAgencia;
    } //-- void setPcAlcadaTarifaAgencia(java.math.BigDecimal) 

    /**
     * Sets the value of field 'vrAlcadaTarifaAgencia'.
     * 
     * @param vrAlcadaTarifaAgencia the value of field
     * 'vrAlcadaTarifaAgencia'.
     */
    public void setVrAlcadaTarifaAgencia(java.math.BigDecimal vrAlcadaTarifaAgencia)
    {
        this._vrAlcadaTarifaAgencia = vrAlcadaTarifaAgencia;
    } //-- void setVrAlcadaTarifaAgencia(java.math.BigDecimal) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return AlterarOperacaoServicoPagtoIntegradoRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.alteraroperacaoservicopagtointegrado.request.AlterarOperacaoServicoPagtoIntegradoRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.alteraroperacaoservicopagtointegrado.request.AlterarOperacaoServicoPagtoIntegradoRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.alteraroperacaoservicopagtointegrado.request.AlterarOperacaoServicoPagtoIntegradoRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.alteraroperacaoservicopagtointegrado.request.AlterarOperacaoServicoPagtoIntegradoRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
