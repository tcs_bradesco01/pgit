/*
 * Nome: br.com.bradesco.web.pgit.service.business.imprimir.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.imprimir.bean;

/**
 * Nome: ImpdiaadSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ImpdiaadSaidaDTO{
	
	/** Atributo codMensagem. */
	private String codMensagem;
	
	/** Atributo mensagem. */
	private String mensagem;
	
	/** Atributo tamanhoDocumento. */
	private Integer tamanhoDocumento;
	
	/** Atributo tipoDocumento. */
	private String tipoDocumento;

	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem(){
		return codMensagem;
	}

	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem){
		this.codMensagem = codMensagem;
	}

	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem(){
		return mensagem;
	}

	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem){
		this.mensagem = mensagem;
	}

	/**
	 * Get: tamanhoDocumento.
	 *
	 * @return tamanhoDocumento
	 */
	public Integer getTamanhoDocumento(){
		return tamanhoDocumento;
	}

	/**
	 * Set: tamanhoDocumento.
	 *
	 * @param tamanhoDocumento the tamanho documento
	 */
	public void setTamanhoDocumento(Integer tamanhoDocumento){
		this.tamanhoDocumento = tamanhoDocumento;
	}

	/**
	 * Get: tipoDocumento.
	 *
	 * @return tipoDocumento
	 */
	public String getTipoDocumento(){
		return tipoDocumento;
	}

	/**
	 * Set: tipoDocumento.
	 *
	 * @param tipoDocumento the tipo documento
	 */
	public void setTipoDocumento(String tipoDocumento){
		this.tipoDocumento = tipoDocumento;
	}
}