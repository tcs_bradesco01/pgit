/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/interfaz.ftl,v $
 * $Id: interfaz.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: interfaz.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.mantercontroleultimoretornogerado;

import java.util.List;

import br.com.bradesco.web.pgit.service.business.mantercontroleultimoretornogerado.bean.AlterarUltimoNumeroRetornoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontroleultimoretornogerado.bean.AlterarUltimoNumeroRetornoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontroleultimoretornogerado.bean.ConsultarUltimoNumeroRetornoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontroleultimoretornogerado.bean.ConsultarUltimoNumeroRetornoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontroleultimoretornogerado.bean.ListarUltimoNumeroRetornoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontroleultimoretornogerado.bean.ListarUltimoNumeroRetornoSaidaDTO;

/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Interface do adaptador: ManterControleUltimoRetornoGerado
 * </p>
 * 
 * @comment CODIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public interface IManterControleUltimoRetornoGeradoService {

	/**
	 * Alterar ultimo numero retorno.
	 *
	 * @param entrada the entrada
	 * @return the alterar ultimo numero retorno saida dto
	 */
	AlterarUltimoNumeroRetornoSaidaDTO alterarUltimoNumeroRetorno (AlterarUltimoNumeroRetornoEntradaDTO entrada);
	
	/**
	 * Consultar ultimo numero retorno.
	 *
	 * @param entrada the entrada
	 * @return the consultar ultimo numero retorno saida dto
	 */
	ConsultarUltimoNumeroRetornoSaidaDTO consultarUltimoNumeroRetorno (ConsultarUltimoNumeroRetornoEntradaDTO entrada);
	
	/**
	 * Listar ultimo numero retorno.
	 *
	 * @param entrada the entrada
	 * @return the list< listar ultimo numero retorno saida dt o>
	 */
	List<ListarUltimoNumeroRetornoSaidaDTO> listarUltimoNumeroRetorno (ListarUltimoNumeroRetornoEntradaDTO entrada);

}

