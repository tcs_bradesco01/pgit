/*
 * Nome: br.com.bradesco.web.pgit.service.business.arquivoremessa.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.arquivoremessa.bean;

/**
 * Nome: ConsultarArquivoRecuperacaoEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarArquivoRecuperacaoEntradaDTO {
	
	/** Atributo nrOcorrencias. */
	private Integer nrOcorrencias;
    
    /** Atributo cdPessoa. */
    private Long cdPessoa;
    
    /** Atributo cdPessoaJuridicaContrato. */
    private Long cdPessoaJuridicaContrato;
    
    /** Atributo cdTipoContratoNegocio. */
    private Integer cdTipoContratoNegocio;
    
    /** Atributo nrSequenciaContratoNegocio. */
    private Long nrSequenciaContratoNegocio;
    
    /** Atributo dtInicioInclusaoRemessa. */
    private String dtInicioInclusaoRemessa;
    
    /** Atributo dtFinalInclusaoRemessa. */
    private String dtFinalInclusaoRemessa;
    
    /** Atributo cdTipoLayoutArquivo. */
    private Integer cdTipoLayoutArquivo;
    
    /** Atributo nrArquivoRemessa. */
    private Long nrArquivoRemessa;
    
    /** Atributo cdSituacaoProcessamentoRemessa. */
    private Integer cdSituacaoProcessamentoRemessa;
    
    /** Atributo cdCondicaoProcessamentoRemessa. */
    private Integer cdCondicaoProcessamentoRemessa;
    
	/**
	 * Get: cdCondicaoProcessamentoRemessa.
	 *
	 * @return cdCondicaoProcessamentoRemessa
	 */
	public Integer getCdCondicaoProcessamentoRemessa() {
		return cdCondicaoProcessamentoRemessa;
	}
	
	/**
	 * Set: cdCondicaoProcessamentoRemessa.
	 *
	 * @param cdCondicaoProcessamentoRemessa the cd condicao processamento remessa
	 */
	public void setCdCondicaoProcessamentoRemessa(
			Integer cdCondicaoProcessamentoRemessa) {
		this.cdCondicaoProcessamentoRemessa = cdCondicaoProcessamentoRemessa;
	}
	
	/**
	 * Get: cdPessoa.
	 *
	 * @return cdPessoa
	 */
	public Long getCdPessoa() {
		return cdPessoa;
	}
	
	/**
	 * Set: cdPessoa.
	 *
	 * @param cdPessoa the cd pessoa
	 */
	public void setCdPessoa(Long cdPessoa) {
		this.cdPessoa = cdPessoa;
	}
	
	/**
	 * Get: cdPessoaJuridicaContrato.
	 *
	 * @return cdPessoaJuridicaContrato
	 */
	public Long getCdPessoaJuridicaContrato() {
		return cdPessoaJuridicaContrato;
	}
	
	/**
	 * Set: cdPessoaJuridicaContrato.
	 *
	 * @param cdPessoaJuridicaContrato the cd pessoa juridica contrato
	 */
	public void setCdPessoaJuridicaContrato(Long cdPessoaJuridicaContrato) {
		this.cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
	}
	
	/**
	 * Get: cdSituacaoProcessamentoRemessa.
	 *
	 * @return cdSituacaoProcessamentoRemessa
	 */
	public Integer getCdSituacaoProcessamentoRemessa() {
		return cdSituacaoProcessamentoRemessa;
	}
	
	/**
	 * Set: cdSituacaoProcessamentoRemessa.
	 *
	 * @param cdSituacaoProcessamentoRemessa the cd situacao processamento remessa
	 */
	public void setCdSituacaoProcessamentoRemessa(
			Integer cdSituacaoProcessamentoRemessa) {
		this.cdSituacaoProcessamentoRemessa = cdSituacaoProcessamentoRemessa;
	}
	
	/**
	 * Get: cdTipoContratoNegocio.
	 *
	 * @return cdTipoContratoNegocio
	 */
	public Integer getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}
	
	/**
	 * Set: cdTipoContratoNegocio.
	 *
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}
	
	/**
	 * Get: cdTipoLayoutArquivo.
	 *
	 * @return cdTipoLayoutArquivo
	 */
	public Integer getCdTipoLayoutArquivo() {
		return cdTipoLayoutArquivo;
	}
	
	/**
	 * Set: cdTipoLayoutArquivo.
	 *
	 * @param cdTipoLayoutArquivo the cd tipo layout arquivo
	 */
	public void setCdTipoLayoutArquivo(Integer cdTipoLayoutArquivo) {
		this.cdTipoLayoutArquivo = cdTipoLayoutArquivo;
	}
	
	/**
	 * Get: dtFinalInclusaoRemessa.
	 *
	 * @return dtFinalInclusaoRemessa
	 */
	public String getDtFinalInclusaoRemessa() {
		return dtFinalInclusaoRemessa;
	}
	
	/**
	 * Set: dtFinalInclusaoRemessa.
	 *
	 * @param dtFinalInclusaoRemessa the dt final inclusao remessa
	 */
	public void setDtFinalInclusaoRemessa(String dtFinalInclusaoRemessa) {
		this.dtFinalInclusaoRemessa = dtFinalInclusaoRemessa;
	}
	
	/**
	 * Get: dtInicioInclusaoRemessa.
	 *
	 * @return dtInicioInclusaoRemessa
	 */
	public String getDtInicioInclusaoRemessa() {
		return dtInicioInclusaoRemessa;
	}
	
	/**
	 * Set: dtInicioInclusaoRemessa.
	 *
	 * @param dtInicioInclusaoRemessa the dt inicio inclusao remessa
	 */
	public void setDtInicioInclusaoRemessa(String dtInicioInclusaoRemessa) {
		this.dtInicioInclusaoRemessa = dtInicioInclusaoRemessa;
	}
	
	/**
	 * Get: nrArquivoRemessa.
	 *
	 * @return nrArquivoRemessa
	 */
	public Long getNrArquivoRemessa() {
		return nrArquivoRemessa;
	}
	
	/**
	 * Set: nrArquivoRemessa.
	 *
	 * @param nrArquivoRemessa the nr arquivo remessa
	 */
	public void setNrArquivoRemessa(Long nrArquivoRemessa) {
		this.nrArquivoRemessa = nrArquivoRemessa;
	}
	
	/**
	 * Get: nrOcorrencias.
	 *
	 * @return nrOcorrencias
	 */
	public Integer getNrOcorrencias() {
		return nrOcorrencias;
	}
	
	/**
	 * Set: nrOcorrencias.
	 *
	 * @param nrOcorrencias the nr ocorrencias
	 */
	public void setNrOcorrencias(Integer nrOcorrencias) {
		this.nrOcorrencias = nrOcorrencias;
	}
	
	/**
	 * Get: nrSequenciaContratoNegocio.
	 *
	 * @return nrSequenciaContratoNegocio
	 */
	public Long getNrSequenciaContratoNegocio() {
		return nrSequenciaContratoNegocio;
	}
	
	/**
	 * Set: nrSequenciaContratoNegocio.
	 *
	 * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
	 */
	public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}
    
	
    
    
    
    
    
    

}
