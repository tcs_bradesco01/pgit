/*
 * Nome: br.com.bradesco.web.pgit.service.business.manterlibpreviasemconsaldos.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.manterlibpreviasemconsaldos.bean;

/**
 * Nome: ConsultarDetLiberacaoPreviaSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarDetLiberacaoPreviaSaidaDTO {
	
	/** Atributo codMensagem. */
	private String codMensagem;
	
	/** Atributo mensagem. */
	private String mensagem;
	
	/** Atributo dtInclusao. */
	private String dtInclusao;
	
	/** Atributo hrInclusao. */
	private String hrInclusao;
	
	/** Atributo cdUsuarioInclusao. */
	private String cdUsuarioInclusao;
	
	/** Atributo cdCanalInclusao. */
	private Integer cdCanalInclusao;
	
	/** Atributo dsCanalInclusao. */
	private String dsCanalInclusao;
	
	/** Atributo cdFluxoInclusao. */
	private String cdFluxoInclusao;
	
	/** Atributo dtManutencao. */
	private String dtManutencao;
	
	/** Atributo hrManutencao. */
	private String hrManutencao;
	
	/** Atributo cdUsuarioManutencao. */
	private String cdUsuarioManutencao;
	
	/** Atributo cdCanalManutencao. */
	private Integer cdCanalManutencao;
	
	/** Atributo dsCanalManutencao. */
	private String dsCanalManutencao;
	
	/** Atributo cdFluxoManutencao. */
	private String cdFluxoManutencao;
	
	/** Atributo dataHoraInclusaoFormatada. */
	private String dataHoraInclusaoFormatada;
	
	/** Atributo tipoCanalInclusaoFormatado. */
	private String tipoCanalInclusaoFormatado;
	
	/** Atributo complementoInclusaoFormatado. */
	private String complementoInclusaoFormatado;
	
	/** Atributo dataHoraManutencaoFormatada. */
	private String dataHoraManutencaoFormatada;
	
	/** Atributo tipoCanalManutencaoFormatado. */
	private String tipoCanalManutencaoFormatado;
	
	/** Atributo complementoManutencaoFormatado. */
	private String complementoManutencaoFormatado;
	
	/** Atributo motivoSolicitacao. */
	private String motivoSolicitacao;
	
	/**
	 * Get: motivoSolicitacao.
	 *
	 * @return motivoSolicitacao
	 */
	public String getMotivoSolicitacao() {
		return motivoSolicitacao;
	}
	
	/**
	 * Set: motivoSolicitacao.
	 *
	 * @param motivoSolicitacao the motivo solicitacao
	 */
	public void setMotivoSolicitacao(String motivoSolicitacao) {
		this.motivoSolicitacao = motivoSolicitacao;
	}
	
	/**
	 * Get: complementoInclusaoFormatado.
	 *
	 * @return complementoInclusaoFormatado
	 */
	public String getComplementoInclusaoFormatado() {
		return complementoInclusaoFormatado;
	}
	
	/**
	 * Set: complementoInclusaoFormatado.
	 *
	 * @param complementoInclusaoFormatado the complemento inclusao formatado
	 */
	public void setComplementoInclusaoFormatado(String complementoInclusaoFormatado) {
		this.complementoInclusaoFormatado = complementoInclusaoFormatado;
	}
	
	/**
	 * Get: complementoManutencaoFormatado.
	 *
	 * @return complementoManutencaoFormatado
	 */
	public String getComplementoManutencaoFormatado() {
		return complementoManutencaoFormatado;
	}
	
	/**
	 * Set: complementoManutencaoFormatado.
	 *
	 * @param complementoManutencaoFormatado the complemento manutencao formatado
	 */
	public void setComplementoManutencaoFormatado(
			String complementoManutencaoFormatado) {
		this.complementoManutencaoFormatado = complementoManutencaoFormatado;
	}
	
	/**
	 * Get: dataHoraInclusaoFormatada.
	 *
	 * @return dataHoraInclusaoFormatada
	 */
	public String getDataHoraInclusaoFormatada() {
		return dataHoraInclusaoFormatada;
	}
	
	/**
	 * Set: dataHoraInclusaoFormatada.
	 *
	 * @param dataHoraInclusaoFormatada the data hora inclusao formatada
	 */
	public void setDataHoraInclusaoFormatada(String dataHoraInclusaoFormatada) {
		this.dataHoraInclusaoFormatada = dataHoraInclusaoFormatada;
	}
	
	/**
	 * Get: dataHoraManutencaoFormatada.
	 *
	 * @return dataHoraManutencaoFormatada
	 */
	public String getDataHoraManutencaoFormatada() {
		return dataHoraManutencaoFormatada;
	}
	
	/**
	 * Set: dataHoraManutencaoFormatada.
	 *
	 * @param dataHoraManutencaoFormatada the data hora manutencao formatada
	 */
	public void setDataHoraManutencaoFormatada(String dataHoraManutencaoFormatada) {
		this.dataHoraManutencaoFormatada = dataHoraManutencaoFormatada;
	}
	
	/**
	 * Get: tipoCanalInclusaoFormatado.
	 *
	 * @return tipoCanalInclusaoFormatado
	 */
	public String getTipoCanalInclusaoFormatado() {
		return tipoCanalInclusaoFormatado;
	}
	
	/**
	 * Set: tipoCanalInclusaoFormatado.
	 *
	 * @param tipoCanalInclusaoFormatado the tipo canal inclusao formatado
	 */
	public void setTipoCanalInclusaoFormatado(String tipoCanalInclusaoFormatado) {
		this.tipoCanalInclusaoFormatado = tipoCanalInclusaoFormatado;
	}
	
	/**
	 * Get: tipoCanalManutencaoFormatado.
	 *
	 * @return tipoCanalManutencaoFormatado
	 */
	public String getTipoCanalManutencaoFormatado() {
		return tipoCanalManutencaoFormatado;
	}
	
	/**
	 * Set: tipoCanalManutencaoFormatado.
	 *
	 * @param tipoCanalManutencaoFormatado the tipo canal manutencao formatado
	 */
	public void setTipoCanalManutencaoFormatado(String tipoCanalManutencaoFormatado) {
		this.tipoCanalManutencaoFormatado = tipoCanalManutencaoFormatado;
	}

	/**
	 * Get: cdCanalInclusao.
	 *
	 * @return cdCanalInclusao
	 */
	public Integer getCdCanalInclusao() {
		return cdCanalInclusao;
	}
	
	/**
	 * Set: cdCanalInclusao.
	 *
	 * @param cdCanalInclusao the cd canal inclusao
	 */
	public void setCdCanalInclusao(Integer cdCanalInclusao) {
		this.cdCanalInclusao = cdCanalInclusao;
	}
	
	/**
	 * Get: cdCanalManutencao.
	 *
	 * @return cdCanalManutencao
	 */
	public Integer getCdCanalManutencao() {
		return cdCanalManutencao;
	}
	
	/**
	 * Set: cdCanalManutencao.
	 *
	 * @param cdCanalManutencao the cd canal manutencao
	 */
	public void setCdCanalManutencao(Integer cdCanalManutencao) {
		this.cdCanalManutencao = cdCanalManutencao;
	}
	
	/**
	 * Get: cdFluxoInclusao.
	 *
	 * @return cdFluxoInclusao
	 */
	public String getCdFluxoInclusao() {
		return cdFluxoInclusao;
	}
	
	/**
	 * Set: cdFluxoInclusao.
	 *
	 * @param cdFluxoInclusao the cd fluxo inclusao
	 */
	public void setCdFluxoInclusao(String cdFluxoInclusao) {
		this.cdFluxoInclusao = cdFluxoInclusao;
	}
	
	/**
	 * Get: cdFluxoManutencao.
	 *
	 * @return cdFluxoManutencao
	 */
	public String getCdFluxoManutencao() {
		return cdFluxoManutencao;
	}
	
	/**
	 * Set: cdFluxoManutencao.
	 *
	 * @param cdFluxoManutencao the cd fluxo manutencao
	 */
	public void setCdFluxoManutencao(String cdFluxoManutencao) {
		this.cdFluxoManutencao = cdFluxoManutencao;
	}
	
	/**
	 * Get: cdUsuarioInclusao.
	 *
	 * @return cdUsuarioInclusao
	 */
	public String getCdUsuarioInclusao() {
		return cdUsuarioInclusao;
	}
	
	/**
	 * Set: cdUsuarioInclusao.
	 *
	 * @param cdUsuarioInclusao the cd usuario inclusao
	 */
	public void setCdUsuarioInclusao(String cdUsuarioInclusao) {
		this.cdUsuarioInclusao = cdUsuarioInclusao;
	}
	
	/**
	 * Get: cdUsuarioManutencao.
	 *
	 * @return cdUsuarioManutencao
	 */
	public String getCdUsuarioManutencao() {
		return cdUsuarioManutencao;
	}
	
	/**
	 * Set: cdUsuarioManutencao.
	 *
	 * @param cdUsuarioManutencao the cd usuario manutencao
	 */
	public void setCdUsuarioManutencao(String cdUsuarioManutencao) {
		this.cdUsuarioManutencao = cdUsuarioManutencao;
	}
	
	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}
	
	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}
	
	/**
	 * Get: dsCanalInclusao.
	 *
	 * @return dsCanalInclusao
	 */
	public String getDsCanalInclusao() {
		return dsCanalInclusao;
	}
	
	/**
	 * Set: dsCanalInclusao.
	 *
	 * @param dsCanalInclusao the ds canal inclusao
	 */
	public void setDsCanalInclusao(String dsCanalInclusao) {
		this.dsCanalInclusao = dsCanalInclusao;
	}
	
	/**
	 * Get: dsCanalManutencao.
	 *
	 * @return dsCanalManutencao
	 */
	public String getDsCanalManutencao() {
		return dsCanalManutencao;
	}
	
	/**
	 * Set: dsCanalManutencao.
	 *
	 * @param dsCanalManutencao the ds canal manutencao
	 */
	public void setDsCanalManutencao(String dsCanalManutencao) {
		this.dsCanalManutencao = dsCanalManutencao;
	}
	
	/**
	 * Get: dtInclusao.
	 *
	 * @return dtInclusao
	 */
	public String getDtInclusao() {
		return dtInclusao;
	}
	
	/**
	 * Set: dtInclusao.
	 *
	 * @param dtInclusao the dt inclusao
	 */
	public void setDtInclusao(String dtInclusao) {
		this.dtInclusao = dtInclusao;
	}
	
	/**
	 * Get: dtManutencao.
	 *
	 * @return dtManutencao
	 */
	public String getDtManutencao() {
		return dtManutencao;
	}
	
	/**
	 * Set: dtManutencao.
	 *
	 * @param dtManutencao the dt manutencao
	 */
	public void setDtManutencao(String dtManutencao) {
		this.dtManutencao = dtManutencao;
	}
	
	/**
	 * Get: hrInclusao.
	 *
	 * @return hrInclusao
	 */
	public String getHrInclusao() {
		return hrInclusao;
	}
	
	/**
	 * Set: hrInclusao.
	 *
	 * @param hrInclusao the hr inclusao
	 */
	public void setHrInclusao(String hrInclusao) {
		this.hrInclusao = hrInclusao;
	}
	
	/**
	 * Get: hrManutencao.
	 *
	 * @return hrManutencao
	 */
	public String getHrManutencao() {
		return hrManutencao;
	}
	
	/**
	 * Set: hrManutencao.
	 *
	 * @param hrManutencao the hr manutencao
	 */
	public void setHrManutencao(String hrManutencao) {
		this.hrManutencao = hrManutencao;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	

}


       