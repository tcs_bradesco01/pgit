/*
 * Nome: br.com.bradesco.web.pgit.service.business.manassocmsglayoutmsgsist.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.manassocmsglayoutmsgsist.bean;

/**
 * Nome: ConsultarVinculacaoMsgLayoutSistemaSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarVinculacaoMsgLayoutSistemaSaidaDTO {
    
    /** Atributo codMensagem. */
    private String codMensagem;

    /** Atributo mensagem. */
    private String mensagem;

    /** Atributo numeroLinhas. */
    private Integer numeroLinhas;

    /** Atributo cdTipoLayoutArquivo. */
    private Integer cdTipoLayoutArquivo;

    /** Atributo dsTipoLayoutArquivo. */
    private String dsTipoLayoutArquivo;

    /** Atributo nrMensagemArquivoRetorno. */
    private Integer nrMensagemArquivoRetorno;

    /** Atributo cdMensagemArquivoRetorno. */
    private String cdMensagemArquivoRetorno;

    /** Atributo dsMensagemLayoutRetorno. */
    private String dsMensagemLayoutRetorno;

    /** Atributo cdSistema. */
    private String cdSistema;

    /** Atributo dsCodigoSistema. */
    private String dsCodigoSistema;

    /** Atributo nrEventoMensagemNegocio. */
    private Integer nrEventoMensagemNegocio;

    /** Atributo dsEventoMensagemNegocio. */
    private String dsEventoMensagemNegocio;

    /** Atributo cdRecursoGeradorMensagem. */
    private Integer cdRecursoGeradorMensagem;

    /** Atributo dsRecGedorMensagem. */
    private String dsRecGedorMensagem;

    /** Atributo cdIdiomaTextoMensagem. */
    private Integer cdIdiomaTextoMensagem;

    /** Atributo dsIdiomaTextoMensagem. */
    private String dsIdiomaTextoMensagem;

    /** Atributo tipoLayoutArquivoFormatado. */
    private String tipoLayoutArquivoFormatado;

    /** Atributo mensagemFormatada. */
    private String mensagemFormatada;
    
    /** Atributo centroCustoFormatado. */
    private String centroCustoFormatado;

    /** Atributo eventoFormatado. */
    private String eventoFormatado;

    /** Atributo recursoFormatado. */
    private String recursoFormatado;

    /**
     * Get: codMensagem.
     *
     * @return codMensagem
     */
    public String getCodMensagem() {
	return codMensagem;
    }

    /**
     * Set: codMensagem.
     *
     * @param codMensagem the cod mensagem
     */
    public void setCodMensagem(String codMensagem) {
	this.codMensagem = codMensagem;
    }

    /**
     * Get: mensagem.
     *
     * @return mensagem
     */
    public String getMensagem() {
	return mensagem;
    }

    /**
     * Set: mensagem.
     *
     * @param mensagem the mensagem
     */
    public void setMensagem(String mensagem) {
	this.mensagem = mensagem;
    }

    /**
     * Get: numeroLinhas.
     *
     * @return numeroLinhas
     */
    public Integer getNumeroLinhas() {
	return numeroLinhas;
    }

    /**
     * Set: numeroLinhas.
     *
     * @param numeroLinhas the numero linhas
     */
    public void setNumeroLinhas(Integer numeroLinhas) {
	this.numeroLinhas = numeroLinhas;
    }

    /**
     * Get: cdTipoLayoutArquivo.
     *
     * @return cdTipoLayoutArquivo
     */
    public Integer getCdTipoLayoutArquivo() {
	return cdTipoLayoutArquivo;
    }

    /**
     * Set: cdTipoLayoutArquivo.
     *
     * @param cdTipoLayoutArquivo the cd tipo layout arquivo
     */
    public void setCdTipoLayoutArquivo(Integer cdTipoLayoutArquivo) {
	this.cdTipoLayoutArquivo = cdTipoLayoutArquivo;
    }

    /**
     * Get: dsTipoLayoutArquivo.
     *
     * @return dsTipoLayoutArquivo
     */
    public String getDsTipoLayoutArquivo() {
	return dsTipoLayoutArquivo;
    }

    /**
     * Set: dsTipoLayoutArquivo.
     *
     * @param dsTipoLayoutArquivo the ds tipo layout arquivo
     */
    public void setDsTipoLayoutArquivo(String dsTipoLayoutArquivo) {
	this.dsTipoLayoutArquivo = dsTipoLayoutArquivo;
    }

    /**
     * Get: cdMensagemArquivoRetorno.
     *
     * @return cdMensagemArquivoRetorno
     */
    public String getCdMensagemArquivoRetorno() {
	return cdMensagemArquivoRetorno;
    }

    /**
     * Set: cdMensagemArquivoRetorno.
     *
     * @param cdMensagemArquivoRetorno the cd mensagem arquivo retorno
     */
    public void setCdMensagemArquivoRetorno(String cdMensagemArquivoRetorno) {
	this.cdMensagemArquivoRetorno = cdMensagemArquivoRetorno;
    }

    /**
     * Get: dsMensagemLayoutRetorno.
     *
     * @return dsMensagemLayoutRetorno
     */
    public String getDsMensagemLayoutRetorno() {
	return dsMensagemLayoutRetorno;
    }

    /**
     * Set: dsMensagemLayoutRetorno.
     *
     * @param dsMensagemLayoutRetorno the ds mensagem layout retorno
     */
    public void setDsMensagemLayoutRetorno(String dsMensagemLayoutRetorno) {
	this.dsMensagemLayoutRetorno = dsMensagemLayoutRetorno;
    }

    /**
     * Get: nrEventoMensagemNegocio.
     *
     * @return nrEventoMensagemNegocio
     */
    public Integer getNrEventoMensagemNegocio() {
	return nrEventoMensagemNegocio;
    }

    /**
     * Set: nrEventoMensagemNegocio.
     *
     * @param nrEventoMensagemNegocio the nr evento mensagem negocio
     */
    public void setNrEventoMensagemNegocio(Integer nrEventoMensagemNegocio) {
	this.nrEventoMensagemNegocio = nrEventoMensagemNegocio;
    }

    /**
     * Get: dsEventoMensagemNegocio.
     *
     * @return dsEventoMensagemNegocio
     */
    public String getDsEventoMensagemNegocio() {
	return dsEventoMensagemNegocio;
    }

    /**
     * Set: dsEventoMensagemNegocio.
     *
     * @param dsEventoMensagemNegocio the ds evento mensagem negocio
     */
    public void setDsEventoMensagemNegocio(String dsEventoMensagemNegocio) {
	this.dsEventoMensagemNegocio = dsEventoMensagemNegocio;
    }

    /**
     * Get: cdRecursoGeradorMensagem.
     *
     * @return cdRecursoGeradorMensagem
     */
    public Integer getCdRecursoGeradorMensagem() {
	return cdRecursoGeradorMensagem;
    }

    /**
     * Set: cdRecursoGeradorMensagem.
     *
     * @param cdRecursoGeradorMensagem the cd recurso gerador mensagem
     */
    public void setCdRecursoGeradorMensagem(Integer cdRecursoGeradorMensagem) {
	this.cdRecursoGeradorMensagem = cdRecursoGeradorMensagem;
    }

    /**
     * Get: dsRecGedorMensagem.
     *
     * @return dsRecGedorMensagem
     */
    public String getDsRecGedorMensagem() {
	return dsRecGedorMensagem;
    }

    /**
     * Set: dsRecGedorMensagem.
     *
     * @param dsRecGedorMensagem the ds rec gedor mensagem
     */
    public void setDsRecGedorMensagem(String dsRecGedorMensagem) {
	this.dsRecGedorMensagem = dsRecGedorMensagem;
    }

    /**
     * Get: cdIdiomaTextoMensagem.
     *
     * @return cdIdiomaTextoMensagem
     */
    public Integer getCdIdiomaTextoMensagem() {
	return cdIdiomaTextoMensagem;
    }

    /**
     * Set: cdIdiomaTextoMensagem.
     *
     * @param cdIdiomaTextoMensagem the cd idioma texto mensagem
     */
    public void setCdIdiomaTextoMensagem(Integer cdIdiomaTextoMensagem) {
	this.cdIdiomaTextoMensagem = cdIdiomaTextoMensagem;
    }

    /**
     * Get: dsIdiomaTextoMensagem.
     *
     * @return dsIdiomaTextoMensagem
     */
    public String getDsIdiomaTextoMensagem() {
	return dsIdiomaTextoMensagem;
    }

    /**
     * Set: dsIdiomaTextoMensagem.
     *
     * @param dsIdiomaTextoMensagem the ds idioma texto mensagem
     */
    public void setDsIdiomaTextoMensagem(String dsIdiomaTextoMensagem) {
	this.dsIdiomaTextoMensagem = dsIdiomaTextoMensagem;
    }

    /**
     * Get: eventoFormatado.
     *
     * @return eventoFormatado
     */
    public String getEventoFormatado() {
	return eventoFormatado;
    }

    /**
     * Set: eventoFormatado.
     *
     * @param eventoFormatado the evento formatado
     */
    public void setEventoFormatado(String eventoFormatado) {
	this.eventoFormatado = eventoFormatado;
    }

    /**
     * Get: mensagemFormatada.
     *
     * @return mensagemFormatada
     */
    public String getMensagemFormatada() {
	return mensagemFormatada;
    }

    /**
     * Set: mensagemFormatada.
     *
     * @param mensagemFormatada the mensagem formatada
     */
    public void setMensagemFormatada(String mensagemFormatada) {
	this.mensagemFormatada = mensagemFormatada;
    }

    /**
     * Get: recursoFormatado.
     *
     * @return recursoFormatado
     */
    public String getRecursoFormatado() {
	return recursoFormatado;
    }

    /**
     * Set: recursoFormatado.
     *
     * @param recursoFormatado the recurso formatado
     */
    public void setRecursoFormatado(String recursoFormatado) {
	this.recursoFormatado = recursoFormatado;
    }

    /**
     * Get: tipoLayoutArquivoFormatado.
     *
     * @return tipoLayoutArquivoFormatado
     */
    public String getTipoLayoutArquivoFormatado() {
	return tipoLayoutArquivoFormatado;
    }

    /**
     * Set: tipoLayoutArquivoFormatado.
     *
     * @param tipoLayoutArquivoFormatado the tipo layout arquivo formatado
     */
    public void setTipoLayoutArquivoFormatado(String tipoLayoutArquivoFormatado) {
	this.tipoLayoutArquivoFormatado = tipoLayoutArquivoFormatado;
    }

    /**
     * Get: cdSistema.
     *
     * @return cdSistema
     */
    public String getCdSistema() {
        return cdSistema;
    }

    /**
     * Set: cdSistema.
     *
     * @param cdSistema the cd sistema
     */
    public void setCdSistema(String cdSistema) {
        this.cdSistema = cdSistema;
    }

    /**
     * Get: dsCodigoSistema.
     *
     * @return dsCodigoSistema
     */
    public String getDsCodigoSistema() {
        return dsCodigoSistema;
    }

    /**
     * Set: dsCodigoSistema.
     *
     * @param dsCodigoSistema the ds codigo sistema
     */
    public void setDsCodigoSistema(String dsCodigoSistema) {
        this.dsCodigoSistema = dsCodigoSistema;
    }

    /**
     * Get: nrMensagemArquivoRetorno.
     *
     * @return nrMensagemArquivoRetorno
     */
    public Integer getNrMensagemArquivoRetorno() {
        return nrMensagemArquivoRetorno;
    }

    /**
     * Set: nrMensagemArquivoRetorno.
     *
     * @param nrMensagemArquivoRetorno the nr mensagem arquivo retorno
     */
    public void setNrMensagemArquivoRetorno(Integer nrMensagemArquivoRetorno) {
        this.nrMensagemArquivoRetorno = nrMensagemArquivoRetorno;
    }

    /**
     * Get: centroCustoFormatado.
     *
     * @return centroCustoFormatado
     */
    public String getCentroCustoFormatado() {
        return centroCustoFormatado;
    }

    /**
     * Set: centroCustoFormatado.
     *
     * @param centroCustoFormatado the centro custo formatado
     */
    public void setCentroCustoFormatado(String centroCustoFormatado) {
        this.centroCustoFormatado = centroCustoFormatado;
    }
}