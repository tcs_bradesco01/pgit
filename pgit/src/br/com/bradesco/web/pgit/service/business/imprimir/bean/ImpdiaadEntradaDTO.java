/*
 * Nome: br.com.bradesco.web.pgit.service.business.imprimir.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.imprimir.bean;

/**
 * Nome: ImpdiaadEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ImpdiaadEntradaDTO{
	
	/** Atributo idDocumentoGerado. */
	private Long idDocumentoGerado;
	
	/** Atributo paginaDocumento. */
	private Integer paginaDocumento;
	
	/** Atributo tipoImpressao. */
	private String tipoImpressao;

	/**
	 * Get: idDocumentoGerado.
	 *
	 * @return idDocumentoGerado
	 */
	public Long getIdDocumentoGerado(){
		return idDocumentoGerado;
	}

	/**
	 * Set: idDocumentoGerado.
	 *
	 * @param idDocumentoGerado the id documento gerado
	 */
	public void setIdDocumentoGerado(Long idDocumentoGerado){
		this.idDocumentoGerado = idDocumentoGerado;
	}

	/**
	 * Get: paginaDocumento.
	 *
	 * @return paginaDocumento
	 */
	public Integer getPaginaDocumento(){
		return paginaDocumento;
	}

	/**
	 * Set: paginaDocumento.
	 *
	 * @param paginaDocumento the pagina documento
	 */
	public void setPaginaDocumento(Integer paginaDocumento){
		this.paginaDocumento = paginaDocumento;
	}

	/**
	 * Get: tipoImpressao.
	 *
	 * @return tipoImpressao
	 */
	public String getTipoImpressao(){
		return tipoImpressao;
	}

	/**
	 * Set: tipoImpressao.
	 *
	 * @param tipoImpressao the tipo impressao
	 */
	public void setTipoImpressao(String tipoImpressao){
		this.tipoImpressao = tipoImpressao;
	}
}