/*
 * Nome: br.com.bradesco.web.pgit.service.business.combo.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.combo.bean;

/**
 * Nome: ListarMotivoSituacaoContratoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarMotivoSituacaoContratoSaidaDTO {

	/** Atributo cdMotivoSituacaoContrato. */
	private Integer cdMotivoSituacaoContrato;
    
    /** Atributo dsMotivoSituacaoContrato. */
    private String dsMotivoSituacaoContrato;
    
    /**
     * Listar motivo situacao contrato saida dto.
     *
     * @param cdMotivoSituacaoContrato the cd motivo situacao contrato
     * @param dsMotivoSituacaoContrato the ds motivo situacao contrato
     */
    public ListarMotivoSituacaoContratoSaidaDTO(Integer cdMotivoSituacaoContrato, String dsMotivoSituacaoContrato) {
		super();
		this.cdMotivoSituacaoContrato = cdMotivoSituacaoContrato;
		this.dsMotivoSituacaoContrato = dsMotivoSituacaoContrato;
	}
    
	/**
	 * Get: cdMotivoSituacaoContrato.
	 *
	 * @return cdMotivoSituacaoContrato
	 */
	public Integer getCdMotivoSituacaoContrato() {
		return cdMotivoSituacaoContrato;
	}
	
	/**
	 * Set: cdMotivoSituacaoContrato.
	 *
	 * @param cdMotivoSituacaoContrato the cd motivo situacao contrato
	 */
	public void setCdMotivoSituacaoContrato(Integer cdMotivoSituacaoContrato) {
		this.cdMotivoSituacaoContrato = cdMotivoSituacaoContrato;
	}
	
	/**
	 * Get: dsMotivoSituacaoContrato.
	 *
	 * @return dsMotivoSituacaoContrato
	 */
	public String getDsMotivoSituacaoContrato() {
		return dsMotivoSituacaoContrato;
	}
	
	/**
	 * Set: dsMotivoSituacaoContrato.
	 *
	 * @param dsMotivoSituacaoContrato the ds motivo situacao contrato
	 */
	public void setDsMotivoSituacaoContrato(String dsMotivoSituacaoContrato) {
		this.dsMotivoSituacaoContrato = dsMotivoSituacaoContrato;
	}
}
