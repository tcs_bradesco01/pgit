/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.detalharcnpjficticio.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class DetalharCnpjFicticioResponse.
 * 
 * @version $Revision$ $Date$
 */
public class DetalharCnpjFicticioResponse implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _codMensagem
     */
    private java.lang.String _codMensagem;

    /**
     * Field _mensagem
     */
    private java.lang.String _mensagem;

    /**
     * Field _cdContratoPerfil
     */
    private long _cdContratoPerfil = 0;

    /**
     * keeps track of state for field: _cdContratoPerfil
     */
    private boolean _has_cdContratoPerfil;

    /**
     * Field _cdCnpjCpf
     */
    private long _cdCnpjCpf = 0;

    /**
     * keeps track of state for field: _cdCnpjCpf
     */
    private boolean _has_cdCnpjCpf;

    /**
     * Field _cdFilialCpfCnpj
     */
    private int _cdFilialCpfCnpj = 0;

    /**
     * keeps track of state for field: _cdFilialCpfCnpj
     */
    private boolean _has_cdFilialCpfCnpj;

    /**
     * Field _cdCtrlCpfCnpj
     */
    private int _cdCtrlCpfCnpj = 0;

    /**
     * keeps track of state for field: _cdCtrlCpfCnpj
     */
    private boolean _has_cdCtrlCpfCnpj;

    /**
     * Field _dsRazaoSocial
     */
    private java.lang.String _dsRazaoSocial;

    /**
     * Field _cdUsuarioInclusao
     */
    private java.lang.String _cdUsuarioInclusao;

    /**
     * Field _cdUsuarioInclusaoExterno
     */
    private java.lang.String _cdUsuarioInclusaoExterno;

    /**
     * Field _dtInclusaoRegistro
     */
    private java.lang.String _dtInclusaoRegistro;

    /**
     * Field _hrInclusaoRegistro
     */
    private java.lang.String _hrInclusaoRegistro;

    /**
     * Field _cdOperacaoCanalInclusao
     */
    private java.lang.String _cdOperacaoCanalInclusao;

    /**
     * Field _cdTipoCanalInclusao
     */
    private int _cdTipoCanalInclusao = 0;

    /**
     * keeps track of state for field: _cdTipoCanalInclusao
     */
    private boolean _has_cdTipoCanalInclusao;

    /**
     * Field _dsTipoCanalInclusao
     */
    private java.lang.String _dsTipoCanalInclusao;

    /**
     * Field _cdUsuarioManutencao
     */
    private java.lang.String _cdUsuarioManutencao;

    /**
     * Field _cdUsuarioManutencaoexterno
     */
    private java.lang.String _cdUsuarioManutencaoexterno;

    /**
     * Field _dtManutencaoRegistro
     */
    private java.lang.String _dtManutencaoRegistro;

    /**
     * Field _hrManutencaoRegistro
     */
    private java.lang.String _hrManutencaoRegistro;

    /**
     * Field _cdOperacaoCanalManutencao
     */
    private java.lang.String _cdOperacaoCanalManutencao;

    /**
     * Field _cdTipoCanalManutencao
     */
    private int _cdTipoCanalManutencao = 0;

    /**
     * keeps track of state for field: _cdTipoCanalManutencao
     */
    private boolean _has_cdTipoCanalManutencao;

    /**
     * Field _dsTipoCanalManutencao
     */
    private java.lang.String _dsTipoCanalManutencao;


      //----------------/
     //- Constructors -/
    //----------------/

    public DetalharCnpjFicticioResponse() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharcnpjficticio.response.DetalharCnpjFicticioResponse()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdCnpjCpf
     * 
     */
    public void deleteCdCnpjCpf()
    {
        this._has_cdCnpjCpf= false;
    } //-- void deleteCdCnpjCpf() 

    /**
     * Method deleteCdContratoPerfil
     * 
     */
    public void deleteCdContratoPerfil()
    {
        this._has_cdContratoPerfil= false;
    } //-- void deleteCdContratoPerfil() 

    /**
     * Method deleteCdCtrlCpfCnpj
     * 
     */
    public void deleteCdCtrlCpfCnpj()
    {
        this._has_cdCtrlCpfCnpj= false;
    } //-- void deleteCdCtrlCpfCnpj() 

    /**
     * Method deleteCdFilialCpfCnpj
     * 
     */
    public void deleteCdFilialCpfCnpj()
    {
        this._has_cdFilialCpfCnpj= false;
    } //-- void deleteCdFilialCpfCnpj() 

    /**
     * Method deleteCdTipoCanalInclusao
     * 
     */
    public void deleteCdTipoCanalInclusao()
    {
        this._has_cdTipoCanalInclusao= false;
    } //-- void deleteCdTipoCanalInclusao() 

    /**
     * Method deleteCdTipoCanalManutencao
     * 
     */
    public void deleteCdTipoCanalManutencao()
    {
        this._has_cdTipoCanalManutencao= false;
    } //-- void deleteCdTipoCanalManutencao() 

    /**
     * Returns the value of field 'cdCnpjCpf'.
     * 
     * @return long
     * @return the value of field 'cdCnpjCpf'.
     */
    public long getCdCnpjCpf()
    {
        return this._cdCnpjCpf;
    } //-- long getCdCnpjCpf() 

    /**
     * Returns the value of field 'cdContratoPerfil'.
     * 
     * @return long
     * @return the value of field 'cdContratoPerfil'.
     */
    public long getCdContratoPerfil()
    {
        return this._cdContratoPerfil;
    } //-- long getCdContratoPerfil() 

    /**
     * Returns the value of field 'cdCtrlCpfCnpj'.
     * 
     * @return int
     * @return the value of field 'cdCtrlCpfCnpj'.
     */
    public int getCdCtrlCpfCnpj()
    {
        return this._cdCtrlCpfCnpj;
    } //-- int getCdCtrlCpfCnpj() 

    /**
     * Returns the value of field 'cdFilialCpfCnpj'.
     * 
     * @return int
     * @return the value of field 'cdFilialCpfCnpj'.
     */
    public int getCdFilialCpfCnpj()
    {
        return this._cdFilialCpfCnpj;
    } //-- int getCdFilialCpfCnpj() 

    /**
     * Returns the value of field 'cdOperacaoCanalInclusao'.
     * 
     * @return String
     * @return the value of field 'cdOperacaoCanalInclusao'.
     */
    public java.lang.String getCdOperacaoCanalInclusao()
    {
        return this._cdOperacaoCanalInclusao;
    } //-- java.lang.String getCdOperacaoCanalInclusao() 

    /**
     * Returns the value of field 'cdOperacaoCanalManutencao'.
     * 
     * @return String
     * @return the value of field 'cdOperacaoCanalManutencao'.
     */
    public java.lang.String getCdOperacaoCanalManutencao()
    {
        return this._cdOperacaoCanalManutencao;
    } //-- java.lang.String getCdOperacaoCanalManutencao() 

    /**
     * Returns the value of field 'cdTipoCanalInclusao'.
     * 
     * @return int
     * @return the value of field 'cdTipoCanalInclusao'.
     */
    public int getCdTipoCanalInclusao()
    {
        return this._cdTipoCanalInclusao;
    } //-- int getCdTipoCanalInclusao() 

    /**
     * Returns the value of field 'cdTipoCanalManutencao'.
     * 
     * @return int
     * @return the value of field 'cdTipoCanalManutencao'.
     */
    public int getCdTipoCanalManutencao()
    {
        return this._cdTipoCanalManutencao;
    } //-- int getCdTipoCanalManutencao() 

    /**
     * Returns the value of field 'cdUsuarioInclusao'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioInclusao'.
     */
    public java.lang.String getCdUsuarioInclusao()
    {
        return this._cdUsuarioInclusao;
    } //-- java.lang.String getCdUsuarioInclusao() 

    /**
     * Returns the value of field 'cdUsuarioInclusaoExterno'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioInclusaoExterno'.
     */
    public java.lang.String getCdUsuarioInclusaoExterno()
    {
        return this._cdUsuarioInclusaoExterno;
    } //-- java.lang.String getCdUsuarioInclusaoExterno() 

    /**
     * Returns the value of field 'cdUsuarioManutencao'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioManutencao'.
     */
    public java.lang.String getCdUsuarioManutencao()
    {
        return this._cdUsuarioManutencao;
    } //-- java.lang.String getCdUsuarioManutencao() 

    /**
     * Returns the value of field 'cdUsuarioManutencaoexterno'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioManutencaoexterno'.
     */
    public java.lang.String getCdUsuarioManutencaoexterno()
    {
        return this._cdUsuarioManutencaoexterno;
    } //-- java.lang.String getCdUsuarioManutencaoexterno() 

    /**
     * Returns the value of field 'codMensagem'.
     * 
     * @return String
     * @return the value of field 'codMensagem'.
     */
    public java.lang.String getCodMensagem()
    {
        return this._codMensagem;
    } //-- java.lang.String getCodMensagem() 

    /**
     * Returns the value of field 'dsRazaoSocial'.
     * 
     * @return String
     * @return the value of field 'dsRazaoSocial'.
     */
    public java.lang.String getDsRazaoSocial()
    {
        return this._dsRazaoSocial;
    } //-- java.lang.String getDsRazaoSocial() 

    /**
     * Returns the value of field 'dsTipoCanalInclusao'.
     * 
     * @return String
     * @return the value of field 'dsTipoCanalInclusao'.
     */
    public java.lang.String getDsTipoCanalInclusao()
    {
        return this._dsTipoCanalInclusao;
    } //-- java.lang.String getDsTipoCanalInclusao() 

    /**
     * Returns the value of field 'dsTipoCanalManutencao'.
     * 
     * @return String
     * @return the value of field 'dsTipoCanalManutencao'.
     */
    public java.lang.String getDsTipoCanalManutencao()
    {
        return this._dsTipoCanalManutencao;
    } //-- java.lang.String getDsTipoCanalManutencao() 

    /**
     * Returns the value of field 'dtInclusaoRegistro'.
     * 
     * @return String
     * @return the value of field 'dtInclusaoRegistro'.
     */
    public java.lang.String getDtInclusaoRegistro()
    {
        return this._dtInclusaoRegistro;
    } //-- java.lang.String getDtInclusaoRegistro() 

    /**
     * Returns the value of field 'dtManutencaoRegistro'.
     * 
     * @return String
     * @return the value of field 'dtManutencaoRegistro'.
     */
    public java.lang.String getDtManutencaoRegistro()
    {
        return this._dtManutencaoRegistro;
    } //-- java.lang.String getDtManutencaoRegistro() 

    /**
     * Returns the value of field 'hrInclusaoRegistro'.
     * 
     * @return String
     * @return the value of field 'hrInclusaoRegistro'.
     */
    public java.lang.String getHrInclusaoRegistro()
    {
        return this._hrInclusaoRegistro;
    } //-- java.lang.String getHrInclusaoRegistro() 

    /**
     * Returns the value of field 'hrManutencaoRegistro'.
     * 
     * @return String
     * @return the value of field 'hrManutencaoRegistro'.
     */
    public java.lang.String getHrManutencaoRegistro()
    {
        return this._hrManutencaoRegistro;
    } //-- java.lang.String getHrManutencaoRegistro() 

    /**
     * Returns the value of field 'mensagem'.
     * 
     * @return String
     * @return the value of field 'mensagem'.
     */
    public java.lang.String getMensagem()
    {
        return this._mensagem;
    } //-- java.lang.String getMensagem() 

    /**
     * Method hasCdCnpjCpf
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCnpjCpf()
    {
        return this._has_cdCnpjCpf;
    } //-- boolean hasCdCnpjCpf() 

    /**
     * Method hasCdContratoPerfil
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdContratoPerfil()
    {
        return this._has_cdContratoPerfil;
    } //-- boolean hasCdContratoPerfil() 

    /**
     * Method hasCdCtrlCpfCnpj
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCtrlCpfCnpj()
    {
        return this._has_cdCtrlCpfCnpj;
    } //-- boolean hasCdCtrlCpfCnpj() 

    /**
     * Method hasCdFilialCpfCnpj
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFilialCpfCnpj()
    {
        return this._has_cdFilialCpfCnpj;
    } //-- boolean hasCdFilialCpfCnpj() 

    /**
     * Method hasCdTipoCanalInclusao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoCanalInclusao()
    {
        return this._has_cdTipoCanalInclusao;
    } //-- boolean hasCdTipoCanalInclusao() 

    /**
     * Method hasCdTipoCanalManutencao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoCanalManutencao()
    {
        return this._has_cdTipoCanalManutencao;
    } //-- boolean hasCdTipoCanalManutencao() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdCnpjCpf'.
     * 
     * @param cdCnpjCpf the value of field 'cdCnpjCpf'.
     */
    public void setCdCnpjCpf(long cdCnpjCpf)
    {
        this._cdCnpjCpf = cdCnpjCpf;
        this._has_cdCnpjCpf = true;
    } //-- void setCdCnpjCpf(long) 

    /**
     * Sets the value of field 'cdContratoPerfil'.
     * 
     * @param cdContratoPerfil the value of field 'cdContratoPerfil'
     */
    public void setCdContratoPerfil(long cdContratoPerfil)
    {
        this._cdContratoPerfil = cdContratoPerfil;
        this._has_cdContratoPerfil = true;
    } //-- void setCdContratoPerfil(long) 

    /**
     * Sets the value of field 'cdCtrlCpfCnpj'.
     * 
     * @param cdCtrlCpfCnpj the value of field 'cdCtrlCpfCnpj'.
     */
    public void setCdCtrlCpfCnpj(int cdCtrlCpfCnpj)
    {
        this._cdCtrlCpfCnpj = cdCtrlCpfCnpj;
        this._has_cdCtrlCpfCnpj = true;
    } //-- void setCdCtrlCpfCnpj(int) 

    /**
     * Sets the value of field 'cdFilialCpfCnpj'.
     * 
     * @param cdFilialCpfCnpj the value of field 'cdFilialCpfCnpj'.
     */
    public void setCdFilialCpfCnpj(int cdFilialCpfCnpj)
    {
        this._cdFilialCpfCnpj = cdFilialCpfCnpj;
        this._has_cdFilialCpfCnpj = true;
    } //-- void setCdFilialCpfCnpj(int) 

    /**
     * Sets the value of field 'cdOperacaoCanalInclusao'.
     * 
     * @param cdOperacaoCanalInclusao the value of field
     * 'cdOperacaoCanalInclusao'.
     */
    public void setCdOperacaoCanalInclusao(java.lang.String cdOperacaoCanalInclusao)
    {
        this._cdOperacaoCanalInclusao = cdOperacaoCanalInclusao;
    } //-- void setCdOperacaoCanalInclusao(java.lang.String) 

    /**
     * Sets the value of field 'cdOperacaoCanalManutencao'.
     * 
     * @param cdOperacaoCanalManutencao the value of field
     * 'cdOperacaoCanalManutencao'.
     */
    public void setCdOperacaoCanalManutencao(java.lang.String cdOperacaoCanalManutencao)
    {
        this._cdOperacaoCanalManutencao = cdOperacaoCanalManutencao;
    } //-- void setCdOperacaoCanalManutencao(java.lang.String) 

    /**
     * Sets the value of field 'cdTipoCanalInclusao'.
     * 
     * @param cdTipoCanalInclusao the value of field
     * 'cdTipoCanalInclusao'.
     */
    public void setCdTipoCanalInclusao(int cdTipoCanalInclusao)
    {
        this._cdTipoCanalInclusao = cdTipoCanalInclusao;
        this._has_cdTipoCanalInclusao = true;
    } //-- void setCdTipoCanalInclusao(int) 

    /**
     * Sets the value of field 'cdTipoCanalManutencao'.
     * 
     * @param cdTipoCanalManutencao the value of field
     * 'cdTipoCanalManutencao'.
     */
    public void setCdTipoCanalManutencao(int cdTipoCanalManutencao)
    {
        this._cdTipoCanalManutencao = cdTipoCanalManutencao;
        this._has_cdTipoCanalManutencao = true;
    } //-- void setCdTipoCanalManutencao(int) 

    /**
     * Sets the value of field 'cdUsuarioInclusao'.
     * 
     * @param cdUsuarioInclusao the value of field
     * 'cdUsuarioInclusao'.
     */
    public void setCdUsuarioInclusao(java.lang.String cdUsuarioInclusao)
    {
        this._cdUsuarioInclusao = cdUsuarioInclusao;
    } //-- void setCdUsuarioInclusao(java.lang.String) 

    /**
     * Sets the value of field 'cdUsuarioInclusaoExterno'.
     * 
     * @param cdUsuarioInclusaoExterno the value of field
     * 'cdUsuarioInclusaoExterno'.
     */
    public void setCdUsuarioInclusaoExterno(java.lang.String cdUsuarioInclusaoExterno)
    {
        this._cdUsuarioInclusaoExterno = cdUsuarioInclusaoExterno;
    } //-- void setCdUsuarioInclusaoExterno(java.lang.String) 

    /**
     * Sets the value of field 'cdUsuarioManutencao'.
     * 
     * @param cdUsuarioManutencao the value of field
     * 'cdUsuarioManutencao'.
     */
    public void setCdUsuarioManutencao(java.lang.String cdUsuarioManutencao)
    {
        this._cdUsuarioManutencao = cdUsuarioManutencao;
    } //-- void setCdUsuarioManutencao(java.lang.String) 

    /**
     * Sets the value of field 'cdUsuarioManutencaoexterno'.
     * 
     * @param cdUsuarioManutencaoexterno the value of field
     * 'cdUsuarioManutencaoexterno'.
     */
    public void setCdUsuarioManutencaoexterno(java.lang.String cdUsuarioManutencaoexterno)
    {
        this._cdUsuarioManutencaoexterno = cdUsuarioManutencaoexterno;
    } //-- void setCdUsuarioManutencaoexterno(java.lang.String) 

    /**
     * Sets the value of field 'codMensagem'.
     * 
     * @param codMensagem the value of field 'codMensagem'.
     */
    public void setCodMensagem(java.lang.String codMensagem)
    {
        this._codMensagem = codMensagem;
    } //-- void setCodMensagem(java.lang.String) 

    /**
     * Sets the value of field 'dsRazaoSocial'.
     * 
     * @param dsRazaoSocial the value of field 'dsRazaoSocial'.
     */
    public void setDsRazaoSocial(java.lang.String dsRazaoSocial)
    {
        this._dsRazaoSocial = dsRazaoSocial;
    } //-- void setDsRazaoSocial(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoCanalInclusao'.
     * 
     * @param dsTipoCanalInclusao the value of field
     * 'dsTipoCanalInclusao'.
     */
    public void setDsTipoCanalInclusao(java.lang.String dsTipoCanalInclusao)
    {
        this._dsTipoCanalInclusao = dsTipoCanalInclusao;
    } //-- void setDsTipoCanalInclusao(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoCanalManutencao'.
     * 
     * @param dsTipoCanalManutencao the value of field
     * 'dsTipoCanalManutencao'.
     */
    public void setDsTipoCanalManutencao(java.lang.String dsTipoCanalManutencao)
    {
        this._dsTipoCanalManutencao = dsTipoCanalManutencao;
    } //-- void setDsTipoCanalManutencao(java.lang.String) 

    /**
     * Sets the value of field 'dtInclusaoRegistro'.
     * 
     * @param dtInclusaoRegistro the value of field
     * 'dtInclusaoRegistro'.
     */
    public void setDtInclusaoRegistro(java.lang.String dtInclusaoRegistro)
    {
        this._dtInclusaoRegistro = dtInclusaoRegistro;
    } //-- void setDtInclusaoRegistro(java.lang.String) 

    /**
     * Sets the value of field 'dtManutencaoRegistro'.
     * 
     * @param dtManutencaoRegistro the value of field
     * 'dtManutencaoRegistro'.
     */
    public void setDtManutencaoRegistro(java.lang.String dtManutencaoRegistro)
    {
        this._dtManutencaoRegistro = dtManutencaoRegistro;
    } //-- void setDtManutencaoRegistro(java.lang.String) 

    /**
     * Sets the value of field 'hrInclusaoRegistro'.
     * 
     * @param hrInclusaoRegistro the value of field
     * 'hrInclusaoRegistro'.
     */
    public void setHrInclusaoRegistro(java.lang.String hrInclusaoRegistro)
    {
        this._hrInclusaoRegistro = hrInclusaoRegistro;
    } //-- void setHrInclusaoRegistro(java.lang.String) 

    /**
     * Sets the value of field 'hrManutencaoRegistro'.
     * 
     * @param hrManutencaoRegistro the value of field
     * 'hrManutencaoRegistro'.
     */
    public void setHrManutencaoRegistro(java.lang.String hrManutencaoRegistro)
    {
        this._hrManutencaoRegistro = hrManutencaoRegistro;
    } //-- void setHrManutencaoRegistro(java.lang.String) 

    /**
     * Sets the value of field 'mensagem'.
     * 
     * @param mensagem the value of field 'mensagem'.
     */
    public void setMensagem(java.lang.String mensagem)
    {
        this._mensagem = mensagem;
    } //-- void setMensagem(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return DetalharCnpjFicticioResponse
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.detalharcnpjficticio.response.DetalharCnpjFicticioResponse unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.detalharcnpjficticio.response.DetalharCnpjFicticioResponse) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.detalharcnpjficticio.response.DetalharCnpjFicticioResponse.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharcnpjficticio.response.DetalharCnpjFicticioResponse unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
