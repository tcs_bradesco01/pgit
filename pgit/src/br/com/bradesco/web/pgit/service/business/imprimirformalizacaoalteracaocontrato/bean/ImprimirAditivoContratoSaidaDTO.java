/*
 * Nome: br.com.bradesco.web.pgit.service.business.imprimirformalizacaoalteracaocontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.imprimirformalizacaoalteracaocontrato.bean;

import java.util.List;

import br.com.bradesco.web.pgit.service.data.pdc.imprimiraditivocontrato.response.ImprimirAditivoContratoResponse;

/**
 * Nome: ImprimirAditivoContratoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ImprimirAditivoContratoSaidaDTO {
    
    /** Atributo nrAditivo. */
    private Long nrAditivo;
    
    /** Atributo dtAditivo. */
    private String dtAditivo;
    
    /** Atributo cpfCnpjCliente. */
    private String cpfCnpjCliente;
    
    /** Atributo nomeCliente. */
    private String nomeCliente;
    
    /** Atributo cdPessoaEmpresa. */
    private Long cdPessoaEmpresa;
    
    /** Atributo cdCpfCnpjEmpresa. */
    private String cdCpfCnpjEmpresa;
    
    /** Atributo nmEmpresa. */
    private String nmEmpresa;
    
    /** Atributo cdAgenciaGestorContrato. */
    private Integer cdAgenciaGestorContrato;
    
    /** Atributo digitoAgenciaGestor. */
    private Integer digitoAgenciaGestor;
    
    /** Atributo dsAgenciaGestor. */
    private String dsAgenciaGestor;
    
    /** Atributo cdContaAgenciaGestor. */
    private Long cdContaAgenciaGestor;
    
    /** Atributo digitoContaGestor. */
    private String digitoContaGestor;
    
    /** Atributo cdCepAgencia. */
    private Integer cdCepAgencia;
    
    /** Atributo cdComplementoCepAgencia. */
    private Integer cdComplementoCepAgencia;
    
    /** Atributo logradouroAgencia. */
    private String logradouroAgencia;
    
    /** Atributo nrLogradouroAgencia. */
    private String nrLogradouroAgencia;
    
    /** Atributo complementoAgencia. */
    private String complementoAgencia;
    
    /** Atributo bairroAgencia. */
    private String bairroAgencia;
    
    /** Atributo cidadeAgencia. */
    private String cidadeAgencia;
    
    /** Atributo siglaUfAgencia. */
    private String siglaUfAgencia;
    
    /** Atributo cdCep. */
    private Integer cdCep;
    
    /** Atributo cdCepComplemento. */
    private Integer cdCepComplemento;
    
    /** Atributo dsLogradouroPessoa. */
    private String dsLogradouroPessoa;
    
    /** Atributo dsLogradouroNumero. */
    private String dsLogradouroNumero;
    
    /** Atributo dsComplementoEndereco. */
    private String dsComplementoEndereco;
    
    /** Atributo dsBairroEndereco. */
    private String dsBairroEndereco;
    
    /** Atributo dsCidadeEndereco. */
    private String dsCidadeEndereco;
    
    /** Atributo cdSiglaUf. */
    private String cdSiglaUf;
    
    /** Atributo nrAnexo. */
    private Integer nrAnexo;
    
    /** Atributo dsServicoContrato. */
    private List<String> dsServicoContrato;
    
    /** Atributo impressaoParticipante. */
    private String impressaoParticipante;
    
    /** Atributo qtdeParticipante. */
    private Integer qtdeParticipante;
    
    /** Atributo ocorrenciasParticipante. */
    private List<OcorrenciasParticipanteSaidaDTO> ocorrenciasParticipante;
    
    /** Atributo impressaoConta. */
    private String impressaoConta;
    
    /** Atributo qtdeConta. */
    private Integer qtdeConta;
    
    /** Atributo ocorrenciasConta. */
    private List<OcorrenciasContaSaidaDTO> ocorrenciasConta;
    
    /** Atributo impressaoSeervico. */
    private String impressaoSeervico;
    
    /** Atributo qtdeServico. */
    private Integer qtdeServico;
    
    /** Atributo ocorrenciasServico. */
    private List<OcorrenciasServicoSaidaDTO> ocorrenciasServico;
    
	/**
	 * Imprimir aditivo contrato saida dto.
	 */
	public ImprimirAditivoContratoSaidaDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * Imprimir aditivo contrato saida dto.
	 *
	 * @param response the response
	 * @param dsServicoContrato the ds servico contrato
	 * @param ocorrenciasParticipante the ocorrencias participante
	 * @param ocorrenciasConta the ocorrencias conta
	 * @param ocorrenciasServico the ocorrencias servico
	 */
	public ImprimirAditivoContratoSaidaDTO(ImprimirAditivoContratoResponse response, List<String> dsServicoContrato, List<OcorrenciasParticipanteSaidaDTO> ocorrenciasParticipante, List<OcorrenciasContaSaidaDTO> ocorrenciasConta, List<OcorrenciasServicoSaidaDTO> ocorrenciasServico) {
		super();
		this.nrAditivo = response.getNrAditivo();
		this.dtAditivo = response.getDtAditivo();
		this.cpfCnpjCliente = response.getCpfCnpjCliente();
		this.nomeCliente = response.getNomeCliente();
		this.cdPessoaEmpresa = response.getCdPessoaEmpresa();
		this.cdCpfCnpjEmpresa = response.getCdCpfCnpjEmpresa();
		this.nmEmpresa = response.getNmEmpresa();
		this.cdAgenciaGestorContrato = response.getCdAgenciaGestorContrato();
		this.digitoAgenciaGestor = response.getDigitoAgenciaGestor();
		this.dsAgenciaGestor = response.getDsAgenciaGestor();
		this.cdContaAgenciaGestor = response.getCdContaAgenciaGestor();
		this.digitoContaGestor = response.getDigitoContaGestor();
		this.cdCepAgencia = response.getCdCepAgencia();
		this.cdComplementoCepAgencia = response.getCdComplementoCepAgencia();
		this.logradouroAgencia = response.getLogradouroAgencia();
		this.nrLogradouroAgencia = response.getNrLogradouroAgencia();
		this.complementoAgencia = response.getComplementoAgencia();
		this.bairroAgencia = response.getBairroAgencia();
		this.cidadeAgencia = response.getCidadeAgencia();
		this.siglaUfAgencia = response.getSiglaUfAgencia();
		this.cdCep = response.getCdCep();
		this.cdCepComplemento = response.getCdCepComplemento();
		this.dsLogradouroPessoa = response.getDsLogradouroPessoa();
		this.dsLogradouroNumero = response.getDsLogradouroNumero();
		this.dsComplementoEndereco = response.getDsComplementoEndereco();
		this.dsBairroEndereco = response.getDsBairroEndereco();
		this.dsCidadeEndereco = response.getDsCidadeEndereco();
		this.cdSiglaUf = response.getCdSiglaUf();
		this.nrAnexo = response.getNrAnexo();
		this.dsServicoContrato = dsServicoContrato;
		this.impressaoParticipante = response.getImpressaoParticipante();
		this.qtdeParticipante = response.getQtdeParticipante();
		this.ocorrenciasParticipante = ocorrenciasParticipante;
		this.impressaoConta = response.getImpressaoConta();
		this.qtdeConta = response.getQtdeConta();
		this.ocorrenciasConta = ocorrenciasConta;
		this.impressaoSeervico = response.getImpressaoSeervico();
		this.qtdeServico = response.getQtdeServico();
		this.ocorrenciasServico = ocorrenciasServico;
	}

	/**
	 * Get: bairroAgencia.
	 *
	 * @return bairroAgencia
	 */
	public String getBairroAgencia() {
		return bairroAgencia;
	}

	/**
	 * Set: bairroAgencia.
	 *
	 * @param bairroAgencia the bairro agencia
	 */
	public void setBairroAgencia(String bairroAgencia) {
		this.bairroAgencia = bairroAgencia;
	}

	/**
	 * Get: cdAgenciaGestorContrato.
	 *
	 * @return cdAgenciaGestorContrato
	 */
	public Integer getCdAgenciaGestorContrato() {
		return cdAgenciaGestorContrato;
	}

	/**
	 * Set: cdAgenciaGestorContrato.
	 *
	 * @param cdAgenciaGestorContrato the cd agencia gestor contrato
	 */
	public void setCdAgenciaGestorContrato(Integer cdAgenciaGestorContrato) {
		this.cdAgenciaGestorContrato = cdAgenciaGestorContrato;
	}

	/**
	 * Get: cdCep.
	 *
	 * @return cdCep
	 */
	public Integer getCdCep() {
		return cdCep;
	}

	/**
	 * Set: cdCep.
	 *
	 * @param cdCep the cd cep
	 */
	public void setCdCep(Integer cdCep) {
		this.cdCep = cdCep;
	}

	/**
	 * Get: cdCepAgencia.
	 *
	 * @return cdCepAgencia
	 */
	public Integer getCdCepAgencia() {
		return cdCepAgencia;
	}

	/**
	 * Set: cdCepAgencia.
	 *
	 * @param cdCepAgencia the cd cep agencia
	 */
	public void setCdCepAgencia(Integer cdCepAgencia) {
		this.cdCepAgencia = cdCepAgencia;
	}

	/**
	 * Get: cdCepComplemento.
	 *
	 * @return cdCepComplemento
	 */
	public Integer getCdCepComplemento() {
		return cdCepComplemento;
	}

	/**
	 * Set: cdCepComplemento.
	 *
	 * @param cdCepComplemento the cd cep complemento
	 */
	public void setCdCepComplemento(Integer cdCepComplemento) {
		this.cdCepComplemento = cdCepComplemento;
	}

	/**
	 * Get: cdComplementoCepAgencia.
	 *
	 * @return cdComplementoCepAgencia
	 */
	public Integer getCdComplementoCepAgencia() {
		return cdComplementoCepAgencia;
	}

	/**
	 * Set: cdComplementoCepAgencia.
	 *
	 * @param cdComplementoCepAgencia the cd complemento cep agencia
	 */
	public void setCdComplementoCepAgencia(Integer cdComplementoCepAgencia) {
		this.cdComplementoCepAgencia = cdComplementoCepAgencia;
	}

	/**
	 * Get: cdContaAgenciaGestor.
	 *
	 * @return cdContaAgenciaGestor
	 */
	public Long getCdContaAgenciaGestor() {
		return cdContaAgenciaGestor;
	}

	/**
	 * Set: cdContaAgenciaGestor.
	 *
	 * @param cdContaAgenciaGestor the cd conta agencia gestor
	 */
	public void setCdContaAgenciaGestor(Long cdContaAgenciaGestor) {
		this.cdContaAgenciaGestor = cdContaAgenciaGestor;
	}

	/**
	 * Get: cdCpfCnpjEmpresa.
	 *
	 * @return cdCpfCnpjEmpresa
	 */
	public String getCdCpfCnpjEmpresa() {
		return cdCpfCnpjEmpresa;
	}

	/**
	 * Set: cdCpfCnpjEmpresa.
	 *
	 * @param cdCpfCnpjEmpresa the cd cpf cnpj empresa
	 */
	public void setCdCpfCnpjEmpresa(String cdCpfCnpjEmpresa) {
		this.cdCpfCnpjEmpresa = cdCpfCnpjEmpresa;
	}

	/**
	 * Get: cdPessoaEmpresa.
	 *
	 * @return cdPessoaEmpresa
	 */
	public Long getCdPessoaEmpresa() {
		return cdPessoaEmpresa;
	}

	/**
	 * Set: cdPessoaEmpresa.
	 *
	 * @param cdPessoaEmpresa the cd pessoa empresa
	 */
	public void setCdPessoaEmpresa(Long cdPessoaEmpresa) {
		this.cdPessoaEmpresa = cdPessoaEmpresa;
	}

	/**
	 * Get: cdSiglaUf.
	 *
	 * @return cdSiglaUf
	 */
	public String getCdSiglaUf() {
		return cdSiglaUf;
	}

	/**
	 * Set: cdSiglaUf.
	 *
	 * @param cdSiglaUf the cd sigla uf
	 */
	public void setCdSiglaUf(String cdSiglaUf) {
		this.cdSiglaUf = cdSiglaUf;
	}

	/**
	 * Get: cidadeAgencia.
	 *
	 * @return cidadeAgencia
	 */
	public String getCidadeAgencia() {
		return cidadeAgencia;
	}

	/**
	 * Set: cidadeAgencia.
	 *
	 * @param cidadeAgencia the cidade agencia
	 */
	public void setCidadeAgencia(String cidadeAgencia) {
		this.cidadeAgencia = cidadeAgencia;
	}

	/**
	 * Get: complementoAgencia.
	 *
	 * @return complementoAgencia
	 */
	public String getComplementoAgencia() {
		return complementoAgencia;
	}

	/**
	 * Set: complementoAgencia.
	 *
	 * @param complementoAgencia the complemento agencia
	 */
	public void setComplementoAgencia(String complementoAgencia) {
		this.complementoAgencia = complementoAgencia;
	}

	/**
	 * Get: cpfCnpjCliente.
	 *
	 * @return cpfCnpjCliente
	 */
	public String getCpfCnpjCliente() {
		return cpfCnpjCliente;
	}

	/**
	 * Set: cpfCnpjCliente.
	 *
	 * @param cpfCnpjCliente the cpf cnpj cliente
	 */
	public void setCpfCnpjCliente(String cpfCnpjCliente) {
		this.cpfCnpjCliente = cpfCnpjCliente;
	}

	/**
	 * Get: digitoAgenciaGestor.
	 *
	 * @return digitoAgenciaGestor
	 */
	public Integer getDigitoAgenciaGestor() {
		return digitoAgenciaGestor;
	}

	/**
	 * Set: digitoAgenciaGestor.
	 *
	 * @param digitoAgenciaGestor the digito agencia gestor
	 */
	public void setDigitoAgenciaGestor(Integer digitoAgenciaGestor) {
		this.digitoAgenciaGestor = digitoAgenciaGestor;
	}

	/**
	 * Get: digitoContaGestor.
	 *
	 * @return digitoContaGestor
	 */
	public String getDigitoContaGestor() {
		return digitoContaGestor;
	}

	/**
	 * Set: digitoContaGestor.
	 *
	 * @param digitoContaGestor the digito conta gestor
	 */
	public void setDigitoContaGestor(String digitoContaGestor) {
		this.digitoContaGestor = digitoContaGestor;
	}

	/**
	 * Get: dsAgenciaGestor.
	 *
	 * @return dsAgenciaGestor
	 */
	public String getDsAgenciaGestor() {
		return dsAgenciaGestor;
	}

	/**
	 * Set: dsAgenciaGestor.
	 *
	 * @param dsAgenciaGestor the ds agencia gestor
	 */
	public void setDsAgenciaGestor(String dsAgenciaGestor) {
		this.dsAgenciaGestor = dsAgenciaGestor;
	}

	/**
	 * Get: dsBairroEndereco.
	 *
	 * @return dsBairroEndereco
	 */
	public String getDsBairroEndereco() {
		return dsBairroEndereco;
	}

	/**
	 * Set: dsBairroEndereco.
	 *
	 * @param dsBairroEndereco the ds bairro endereco
	 */
	public void setDsBairroEndereco(String dsBairroEndereco) {
		this.dsBairroEndereco = dsBairroEndereco;
	}

	/**
	 * Get: dsCidadeEndereco.
	 *
	 * @return dsCidadeEndereco
	 */
	public String getDsCidadeEndereco() {
		return dsCidadeEndereco;
	}

	/**
	 * Set: dsCidadeEndereco.
	 *
	 * @param dsCidadeEndereco the ds cidade endereco
	 */
	public void setDsCidadeEndereco(String dsCidadeEndereco) {
		this.dsCidadeEndereco = dsCidadeEndereco;
	}

	/**
	 * Get: dsComplementoEndereco.
	 *
	 * @return dsComplementoEndereco
	 */
	public String getDsComplementoEndereco() {
		return dsComplementoEndereco;
	}

	/**
	 * Set: dsComplementoEndereco.
	 *
	 * @param dsComplementoEndereco the ds complemento endereco
	 */
	public void setDsComplementoEndereco(String dsComplementoEndereco) {
		this.dsComplementoEndereco = dsComplementoEndereco;
	}

	/**
	 * Get: dsLogradouroNumero.
	 *
	 * @return dsLogradouroNumero
	 */
	public String getDsLogradouroNumero() {
		return dsLogradouroNumero;
	}

	/**
	 * Set: dsLogradouroNumero.
	 *
	 * @param dsLogradouroNumero the ds logradouro numero
	 */
	public void setDsLogradouroNumero(String dsLogradouroNumero) {
		this.dsLogradouroNumero = dsLogradouroNumero;
	}

	/**
	 * Get: dsLogradouroPessoa.
	 *
	 * @return dsLogradouroPessoa
	 */
	public String getDsLogradouroPessoa() {
		return dsLogradouroPessoa;
	}

	/**
	 * Set: dsLogradouroPessoa.
	 *
	 * @param dsLogradouroPessoa the ds logradouro pessoa
	 */
	public void setDsLogradouroPessoa(String dsLogradouroPessoa) {
		this.dsLogradouroPessoa = dsLogradouroPessoa;
	}

	/**
	 * Get: dsServicoContrato.
	 *
	 * @return dsServicoContrato
	 */
	public List<String> getDsServicoContrato() {
		return dsServicoContrato;
	}

	/**
	 * Set: dsServicoContrato.
	 *
	 * @param dsServicoContrato the ds servico contrato
	 */
	public void setDsServicoContrato(List<String> dsServicoContrato) {
		this.dsServicoContrato = dsServicoContrato;
	}

	/**
	 * Get: dtAditivo.
	 *
	 * @return dtAditivo
	 */
	public String getDtAditivo() {
		return dtAditivo;
	}

	/**
	 * Set: dtAditivo.
	 *
	 * @param dtAditivo the dt aditivo
	 */
	public void setDtAditivo(String dtAditivo) {
		this.dtAditivo = dtAditivo;
	}

	/**
	 * Get: impressaoConta.
	 *
	 * @return impressaoConta
	 */
	public String getImpressaoConta() {
		return impressaoConta;
	}

	/**
	 * Set: impressaoConta.
	 *
	 * @param impressaoConta the impressao conta
	 */
	public void setImpressaoConta(String impressaoConta) {
		this.impressaoConta = impressaoConta;
	}

	/**
	 * Get: impressaoParticipante.
	 *
	 * @return impressaoParticipante
	 */
	public String getImpressaoParticipante() {
		return impressaoParticipante;
	}

	/**
	 * Set: impressaoParticipante.
	 *
	 * @param impressaoParticipante the impressao participante
	 */
	public void setImpressaoParticipante(String impressaoParticipante) {
		this.impressaoParticipante = impressaoParticipante;
	}

	/**
	 * Get: impressaoSeervico.
	 *
	 * @return impressaoSeervico
	 */
	public String getImpressaoSeervico() {
		return impressaoSeervico;
	}

	/**
	 * Set: impressaoSeervico.
	 *
	 * @param impressaoSeervico the impressao seervico
	 */
	public void setImpressaoSeervico(String impressaoSeervico) {
		this.impressaoSeervico = impressaoSeervico;
	}

	/**
	 * Get: logradouroAgencia.
	 *
	 * @return logradouroAgencia
	 */
	public String getLogradouroAgencia() {
		return logradouroAgencia;
	}

	/**
	 * Set: logradouroAgencia.
	 *
	 * @param logradouroAgencia the logradouro agencia
	 */
	public void setLogradouroAgencia(String logradouroAgencia) {
		this.logradouroAgencia = logradouroAgencia;
	}

	/**
	 * Get: nmEmpresa.
	 *
	 * @return nmEmpresa
	 */
	public String getNmEmpresa() {
		return nmEmpresa;
	}

	/**
	 * Set: nmEmpresa.
	 *
	 * @param nmEmpresa the nm empresa
	 */
	public void setNmEmpresa(String nmEmpresa) {
		this.nmEmpresa = nmEmpresa;
	}

	/**
	 * Get: nomeCliente.
	 *
	 * @return nomeCliente
	 */
	public String getNomeCliente() {
		return nomeCliente;
	}

	/**
	 * Set: nomeCliente.
	 *
	 * @param nomeCliente the nome cliente
	 */
	public void setNomeCliente(String nomeCliente) {
		this.nomeCliente = nomeCliente;
	}

	/**
	 * Get: nrAditivo.
	 *
	 * @return nrAditivo
	 */
	public Long getNrAditivo() {
		return nrAditivo;
	}

	/**
	 * Set: nrAditivo.
	 *
	 * @param nrAditivo the nr aditivo
	 */
	public void setNrAditivo(Long nrAditivo) {
		this.nrAditivo = nrAditivo;
	}

	/**
	 * Get: nrAnexo.
	 *
	 * @return nrAnexo
	 */
	public Integer getNrAnexo() {
		return nrAnexo;
	}

	/**
	 * Set: nrAnexo.
	 *
	 * @param nrAnexo the nr anexo
	 */
	public void setNrAnexo(Integer nrAnexo) {
		this.nrAnexo = nrAnexo;
	}

	/**
	 * Get: nrLogradouroAgencia.
	 *
	 * @return nrLogradouroAgencia
	 */
	public String getNrLogradouroAgencia() {
		return nrLogradouroAgencia;
	}

	/**
	 * Set: nrLogradouroAgencia.
	 *
	 * @param nrLogradouroAgencia the nr logradouro agencia
	 */
	public void setNrLogradouroAgencia(String nrLogradouroAgencia) {
		this.nrLogradouroAgencia = nrLogradouroAgencia;
	}

	/**
	 * Get: ocorrenciasConta.
	 *
	 * @return ocorrenciasConta
	 */
	public List<OcorrenciasContaSaidaDTO> getOcorrenciasConta() {
		return ocorrenciasConta;
	}

	/**
	 * Set: ocorrenciasConta.
	 *
	 * @param ocorrenciasConta the ocorrencias conta
	 */
	public void setOcorrenciasConta(List<OcorrenciasContaSaidaDTO> ocorrenciasConta) {
		this.ocorrenciasConta = ocorrenciasConta;
	}

	/**
	 * Get: ocorrenciasParticipante.
	 *
	 * @return ocorrenciasParticipante
	 */
	public List<OcorrenciasParticipanteSaidaDTO> getOcorrenciasParticipante() {
		return ocorrenciasParticipante;
	}

	/**
	 * Set: ocorrenciasParticipante.
	 *
	 * @param ocorrenciasParticipante the ocorrencias participante
	 */
	public void setOcorrenciasParticipante(
			List<OcorrenciasParticipanteSaidaDTO> ocorrenciasParticipante) {
		this.ocorrenciasParticipante = ocorrenciasParticipante;
	}

	/**
	 * Get: ocorrenciasServico.
	 *
	 * @return ocorrenciasServico
	 */
	public List<OcorrenciasServicoSaidaDTO> getOcorrenciasServico() {
		return ocorrenciasServico;
	}

	/**
	 * Set: ocorrenciasServico.
	 *
	 * @param ocorrenciasServico the ocorrencias servico
	 */
	public void setOcorrenciasServico(
			List<OcorrenciasServicoSaidaDTO> ocorrenciasServico) {
		this.ocorrenciasServico = ocorrenciasServico;
	}

	/**
	 * Get: qtdeConta.
	 *
	 * @return qtdeConta
	 */
	public Integer getQtdeConta() {
		return qtdeConta;
	}

	/**
	 * Set: qtdeConta.
	 *
	 * @param qtdeConta the qtde conta
	 */
	public void setQtdeConta(Integer qtdeConta) {
		this.qtdeConta = qtdeConta;
	}

	/**
	 * Get: qtdeParticipante.
	 *
	 * @return qtdeParticipante
	 */
	public Integer getQtdeParticipante() {
		return qtdeParticipante;
	}

	/**
	 * Set: qtdeParticipante.
	 *
	 * @param qtdeParticipante the qtde participante
	 */
	public void setQtdeParticipante(Integer qtdeParticipante) {
		this.qtdeParticipante = qtdeParticipante;
	}

	/**
	 * Get: qtdeServico.
	 *
	 * @return qtdeServico
	 */
	public Integer getQtdeServico() {
		return qtdeServico;
	}

	/**
	 * Set: qtdeServico.
	 *
	 * @param qtdeServico the qtde servico
	 */
	public void setQtdeServico(Integer qtdeServico) {
		this.qtdeServico = qtdeServico;
	}

	/**
	 * Get: siglaUfAgencia.
	 *
	 * @return siglaUfAgencia
	 */
	public String getSiglaUfAgencia() {
		return siglaUfAgencia;
	}

	/**
	 * Set: siglaUfAgencia.
	 *
	 * @param siglaUfAgencia the sigla uf agencia
	 */
	public void setSiglaUfAgencia(String siglaUfAgencia) {
		this.siglaUfAgencia = siglaUfAgencia;
	}
}
