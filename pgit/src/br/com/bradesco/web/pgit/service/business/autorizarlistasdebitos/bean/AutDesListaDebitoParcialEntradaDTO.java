/*
 * Nome: br.com.bradesco.web.pgit.service.business.autorizarlistasdebitos.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.autorizarlistasdebitos.bean;

import java.math.BigDecimal;

/**
 * Nome: AutDesListaDebitoParcialEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class AutDesListaDebitoParcialEntradaDTO {

    /** Atributo cdPessoaJuridicaLista. */
    private Long cdPessoaJuridicaLista;
    
    /** Atributo cdTipoContratoLista. */
    private Integer cdTipoContratoLista;
    
    /** Atributo nrSequenciaContratoLista. */
    private Long nrSequenciaContratoLista;
    
    /** Atributo cdTipoCanalLista. */
    private Integer cdTipoCanalLista;
    
    /** Atributo cdListaDebitoPagamento. */
    private Long cdListaDebitoPagamento;
    
    /** Atributo cdPessoaJuridicaContrato. */
    private Long cdPessoaJuridicaContrato;
    
    /** Atributo cdTipoContratoNegocio. */
    private Integer cdTipoContratoNegocio;
    
    /** Atributo nrSequenciaContratoNegocio. */
    private Long nrSequenciaContratoNegocio;
    
    /** Atributo cdTipoCanal. */
    private Integer cdTipoCanal;
    
    /** Atributo cdControlePagamento. */
    private String cdControlePagamento;
    
    /** Atributo vlPagamento. */
    private BigDecimal vlPagamento;
    
	/**
	 * Get: cdControlePagamento.
	 *
	 * @return cdControlePagamento
	 */
	public String getCdControlePagamento() {
		return cdControlePagamento;
	}
	
	/**
	 * Set: cdControlePagamento.
	 *
	 * @param cdControlePagamento the cd controle pagamento
	 */
	public void setCdControlePagamento(String cdControlePagamento) {
		this.cdControlePagamento = cdControlePagamento;
	}
	
	/**
	 * Get: cdListaDebitoPagamento.
	 *
	 * @return cdListaDebitoPagamento
	 */
	public Long getCdListaDebitoPagamento() {
		return cdListaDebitoPagamento;
	}
	
	/**
	 * Set: cdListaDebitoPagamento.
	 *
	 * @param cdListaDebitoPagamento the cd lista debito pagamento
	 */
	public void setCdListaDebitoPagamento(Long cdListaDebitoPagamento) {
		this.cdListaDebitoPagamento = cdListaDebitoPagamento;
	}
	
	/**
	 * Get: cdPessoaJuridicaContrato.
	 *
	 * @return cdPessoaJuridicaContrato
	 */
	public Long getCdPessoaJuridicaContrato() {
		return cdPessoaJuridicaContrato;
	}
	
	/**
	 * Set: cdPessoaJuridicaContrato.
	 *
	 * @param cdPessoaJuridicaContrato the cd pessoa juridica contrato
	 */
	public void setCdPessoaJuridicaContrato(Long cdPessoaJuridicaContrato) {
		this.cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
	}
	
	/**
	 * Get: cdPessoaJuridicaLista.
	 *
	 * @return cdPessoaJuridicaLista
	 */
	public Long getCdPessoaJuridicaLista() {
		return cdPessoaJuridicaLista;
	}
	
	/**
	 * Set: cdPessoaJuridicaLista.
	 *
	 * @param cdPessoaJuridicaLista the cd pessoa juridica lista
	 */
	public void setCdPessoaJuridicaLista(Long cdPessoaJuridicaLista) {
		this.cdPessoaJuridicaLista = cdPessoaJuridicaLista;
	}
	
	/**
	 * Get: cdTipoCanal.
	 *
	 * @return cdTipoCanal
	 */
	public Integer getCdTipoCanal() {
		return cdTipoCanal;
	}
	
	/**
	 * Set: cdTipoCanal.
	 *
	 * @param cdTipoCanal the cd tipo canal
	 */
	public void setCdTipoCanal(Integer cdTipoCanal) {
		this.cdTipoCanal = cdTipoCanal;
	}
	
	/**
	 * Get: cdTipoCanalLista.
	 *
	 * @return cdTipoCanalLista
	 */
	public Integer getCdTipoCanalLista() {
		return cdTipoCanalLista;
	}
	
	/**
	 * Set: cdTipoCanalLista.
	 *
	 * @param cdTipoCanalLista the cd tipo canal lista
	 */
	public void setCdTipoCanalLista(Integer cdTipoCanalLista) {
		this.cdTipoCanalLista = cdTipoCanalLista;
	}
	
	/**
	 * Get: cdTipoContratoLista.
	 *
	 * @return cdTipoContratoLista
	 */
	public Integer getCdTipoContratoLista() {
		return cdTipoContratoLista;
	}
	
	/**
	 * Set: cdTipoContratoLista.
	 *
	 * @param cdTipoContratoLista the cd tipo contrato lista
	 */
	public void setCdTipoContratoLista(Integer cdTipoContratoLista) {
		this.cdTipoContratoLista = cdTipoContratoLista;
	}
	
	/**
	 * Get: cdTipoContratoNegocio.
	 *
	 * @return cdTipoContratoNegocio
	 */
	public Integer getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}
	
	/**
	 * Set: cdTipoContratoNegocio.
	 *
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}
	
	/**
	 * Get: nrSequenciaContratoLista.
	 *
	 * @return nrSequenciaContratoLista
	 */
	public Long getNrSequenciaContratoLista() {
		return nrSequenciaContratoLista;
	}
	
	/**
	 * Set: nrSequenciaContratoLista.
	 *
	 * @param nrSequenciaContratoLista the nr sequencia contrato lista
	 */
	public void setNrSequenciaContratoLista(Long nrSequenciaContratoLista) {
		this.nrSequenciaContratoLista = nrSequenciaContratoLista;
	}
	
	/**
	 * Get: nrSequenciaContratoNegocio.
	 *
	 * @return nrSequenciaContratoNegocio
	 */
	public Long getNrSequenciaContratoNegocio() {
		return nrSequenciaContratoNegocio;
	}
	
	/**
	 * Set: nrSequenciaContratoNegocio.
	 *
	 * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
	 */
	public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}
	
	/**
	 * Get: vlPagamento.
	 *
	 * @return vlPagamento
	 */
	public BigDecimal getVlPagamento() {
		return vlPagamento;
	}
	
	/**
	 * Set: vlPagamento.
	 *
	 * @param vlPagamento the vl pagamento
	 */
	public void setVlPagamento(BigDecimal vlPagamento) {
		this.vlPagamento = vlPagamento;
	}
    
    
}
