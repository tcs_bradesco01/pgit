/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listarperiodicidade.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdPeriodicidade
     */
    private int _cdPeriodicidade = 0;

    /**
     * keeps track of state for field: _cdPeriodicidade
     */
    private boolean _has_cdPeriodicidade;

    /**
     * Field _dsPeriodicidade
     */
    private java.lang.String _dsPeriodicidade;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarperiodicidade.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdPeriodicidade
     * 
     */
    public void deleteCdPeriodicidade()
    {
        this._has_cdPeriodicidade= false;
    } //-- void deleteCdPeriodicidade() 

    /**
     * Returns the value of field 'cdPeriodicidade'.
     * 
     * @return int
     * @return the value of field 'cdPeriodicidade'.
     */
    public int getCdPeriodicidade()
    {
        return this._cdPeriodicidade;
    } //-- int getCdPeriodicidade() 

    /**
     * Returns the value of field 'dsPeriodicidade'.
     * 
     * @return String
     * @return the value of field 'dsPeriodicidade'.
     */
    public java.lang.String getDsPeriodicidade()
    {
        return this._dsPeriodicidade;
    } //-- java.lang.String getDsPeriodicidade() 

    /**
     * Method hasCdPeriodicidade
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPeriodicidade()
    {
        return this._has_cdPeriodicidade;
    } //-- boolean hasCdPeriodicidade() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdPeriodicidade'.
     * 
     * @param cdPeriodicidade the value of field 'cdPeriodicidade'.
     */
    public void setCdPeriodicidade(int cdPeriodicidade)
    {
        this._cdPeriodicidade = cdPeriodicidade;
        this._has_cdPeriodicidade = true;
    } //-- void setCdPeriodicidade(int) 

    /**
     * Sets the value of field 'dsPeriodicidade'.
     * 
     * @param dsPeriodicidade the value of field 'dsPeriodicidade'.
     */
    public void setDsPeriodicidade(java.lang.String dsPeriodicidade)
    {
        this._dsPeriodicidade = dsPeriodicidade;
    } //-- void setDsPeriodicidade(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listarperiodicidade.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listarperiodicidade.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listarperiodicidade.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarperiodicidade.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
