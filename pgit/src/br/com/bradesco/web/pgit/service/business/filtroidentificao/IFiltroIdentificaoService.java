/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/interfaz.ftl,v $
 * $Id: interfaz.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: interfaz.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.filtroidentificao;

import java.util.List;

import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasEntradaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaContratosPessoasEntradaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaContratosPessoasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaFuncionarioBradescoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaFuncionarioBradescoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ListarClubesClientesSaidaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ListarContratosPgitEntradaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ListarContratosPgitSaidaDTO;

/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Interface do adaptador: Filtroidentificao
 * </p>
 * 
 * @comment CODIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public interface IFiltroIdentificaoService {

	/**
	 * Pesquisar clientes.
	 *
	 * @param entrada the entrada
	 * @return the list< consultar lista cliente pessoas saida dt o>
	 */
	List<ConsultarListaClientePessoasSaidaDTO> pesquisarClientes(ConsultarListaClientePessoasEntradaDTO entrada);
	
	/**
	 * Pesquisar contratos.
	 *
	 * @param entrada the entrada
	 * @return the list< consultar lista contratos pessoas saida dt o>
	 */
	List<ConsultarListaContratosPessoasSaidaDTO> pesquisarContratos(ConsultarListaContratosPessoasEntradaDTO entrada);
	
	/**
	 * Pesquisar funcionarios.
	 *
	 * @param entrada the entrada
	 * @return the list< consultar lista funcionario bradesco saida dt o>
	 */
	List<ConsultarListaFuncionarioBradescoSaidaDTO> pesquisarFuncionarios(ConsultarListaFuncionarioBradescoEntradaDTO entrada);
	
	/**
	 * Detalhar contrato origem.
	 *
	 * @param entrada the entrada
	 * @return the consultar lista contratos pessoas saida dto
	 */
	ConsultarListaContratosPessoasSaidaDTO detalharContratoOrigem(ConsultarListaContratosPessoasEntradaDTO entrada);
	
	/**
	 * Pesquisar manutencao contrato.
	 *
	 * @param entrada the entrada
	 * @return the list< listar contratos pgit saida dt o>
	 */
	List<ListarContratosPgitSaidaDTO> pesquisarManutencaoContrato(ListarContratosPgitEntradaDTO entrada);

	/**
	 * Listar clubes clientes.
	 *
	 * @param entrada the entrada
	 * @return the list< listar clubes clientes saida dt o>
	 */
	List<ListarClubesClientesSaidaDTO> listarClubesClientes(ConsultarListaClientePessoasEntradaDTO entrada);
	
	/**
	 * Listar contratos renegociacao.
	 *
	 * @param entrada the entrada
	 * @return the list< listar contratos pgit saida dt o>
	 */
	List<ListarContratosPgitSaidaDTO> listarContratosRenegociacao(ListarContratosPgitEntradaDTO entrada);
}