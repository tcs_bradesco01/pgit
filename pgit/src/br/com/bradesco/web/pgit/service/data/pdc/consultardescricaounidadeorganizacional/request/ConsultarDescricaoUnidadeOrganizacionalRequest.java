/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultardescricaounidadeorganizacional.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ConsultarDescricaoUnidadeOrganizacionalRequest.
 * 
 * @version $Revision$ $Date$
 */
public class ConsultarDescricaoUnidadeOrganizacionalRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdPessoaJuridica
     */
    private long _cdPessoaJuridica = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridica
     */
    private boolean _has_cdPessoaJuridica;

    /**
     * Field _nrSequenciaUnidadeOrganizacional
     */
    private int _nrSequenciaUnidadeOrganizacional = 0;

    /**
     * keeps track of state for field:
     * _nrSequenciaUnidadeOrganizacional
     */
    private boolean _has_nrSequenciaUnidadeOrganizacional;

    /**
     * Field _cdTipoUnidadeOrganizacional
     */
    private int _cdTipoUnidadeOrganizacional = 0;

    /**
     * keeps track of state for field: _cdTipoUnidadeOrganizacional
     */
    private boolean _has_cdTipoUnidadeOrganizacional;


      //----------------/
     //- Constructors -/
    //----------------/

    public ConsultarDescricaoUnidadeOrganizacionalRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultardescricaounidadeorganizacional.request.ConsultarDescricaoUnidadeOrganizacionalRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdPessoaJuridica
     * 
     */
    public void deleteCdPessoaJuridica()
    {
        this._has_cdPessoaJuridica= false;
    } //-- void deleteCdPessoaJuridica() 

    /**
     * Method deleteCdTipoUnidadeOrganizacional
     * 
     */
    public void deleteCdTipoUnidadeOrganizacional()
    {
        this._has_cdTipoUnidadeOrganizacional= false;
    } //-- void deleteCdTipoUnidadeOrganizacional() 

    /**
     * Method deleteNrSequenciaUnidadeOrganizacional
     * 
     */
    public void deleteNrSequenciaUnidadeOrganizacional()
    {
        this._has_nrSequenciaUnidadeOrganizacional= false;
    } //-- void deleteNrSequenciaUnidadeOrganizacional() 

    /**
     * Returns the value of field 'cdPessoaJuridica'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridica'.
     */
    public long getCdPessoaJuridica()
    {
        return this._cdPessoaJuridica;
    } //-- long getCdPessoaJuridica() 

    /**
     * Returns the value of field 'cdTipoUnidadeOrganizacional'.
     * 
     * @return int
     * @return the value of field 'cdTipoUnidadeOrganizacional'.
     */
    public int getCdTipoUnidadeOrganizacional()
    {
        return this._cdTipoUnidadeOrganizacional;
    } //-- int getCdTipoUnidadeOrganizacional() 

    /**
     * Returns the value of field
     * 'nrSequenciaUnidadeOrganizacional'.
     * 
     * @return int
     * @return the value of field 'nrSequenciaUnidadeOrganizacional'
     */
    public int getNrSequenciaUnidadeOrganizacional()
    {
        return this._nrSequenciaUnidadeOrganizacional;
    } //-- int getNrSequenciaUnidadeOrganizacional() 

    /**
     * Method hasCdPessoaJuridica
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridica()
    {
        return this._has_cdPessoaJuridica;
    } //-- boolean hasCdPessoaJuridica() 

    /**
     * Method hasCdTipoUnidadeOrganizacional
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoUnidadeOrganizacional()
    {
        return this._has_cdTipoUnidadeOrganizacional;
    } //-- boolean hasCdTipoUnidadeOrganizacional() 

    /**
     * Method hasNrSequenciaUnidadeOrganizacional
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaUnidadeOrganizacional()
    {
        return this._has_nrSequenciaUnidadeOrganizacional;
    } //-- boolean hasNrSequenciaUnidadeOrganizacional() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdPessoaJuridica'.
     * 
     * @param cdPessoaJuridica the value of field 'cdPessoaJuridica'
     */
    public void setCdPessoaJuridica(long cdPessoaJuridica)
    {
        this._cdPessoaJuridica = cdPessoaJuridica;
        this._has_cdPessoaJuridica = true;
    } //-- void setCdPessoaJuridica(long) 

    /**
     * Sets the value of field 'cdTipoUnidadeOrganizacional'.
     * 
     * @param cdTipoUnidadeOrganizacional the value of field
     * 'cdTipoUnidadeOrganizacional'.
     */
    public void setCdTipoUnidadeOrganizacional(int cdTipoUnidadeOrganizacional)
    {
        this._cdTipoUnidadeOrganizacional = cdTipoUnidadeOrganizacional;
        this._has_cdTipoUnidadeOrganizacional = true;
    } //-- void setCdTipoUnidadeOrganizacional(int) 

    /**
     * Sets the value of field 'nrSequenciaUnidadeOrganizacional'.
     * 
     * @param nrSequenciaUnidadeOrganizacional the value of field
     * 'nrSequenciaUnidadeOrganizacional'.
     */
    public void setNrSequenciaUnidadeOrganizacional(int nrSequenciaUnidadeOrganizacional)
    {
        this._nrSequenciaUnidadeOrganizacional = nrSequenciaUnidadeOrganizacional;
        this._has_nrSequenciaUnidadeOrganizacional = true;
    } //-- void setNrSequenciaUnidadeOrganizacional(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ConsultarDescricaoUnidadeOrganizacionalRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultardescricaounidadeorganizacional.request.ConsultarDescricaoUnidadeOrganizacionalRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultardescricaounidadeorganizacional.request.ConsultarDescricaoUnidadeOrganizacionalRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultardescricaounidadeorganizacional.request.ConsultarDescricaoUnidadeOrganizacionalRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultardescricaounidadeorganizacional.request.ConsultarDescricaoUnidadeOrganizacionalRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
