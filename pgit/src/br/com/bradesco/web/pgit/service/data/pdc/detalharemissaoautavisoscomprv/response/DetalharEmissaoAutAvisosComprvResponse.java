/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.detalharemissaoautavisoscomprv.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class DetalharEmissaoAutAvisosComprvResponse.
 * 
 * @version $Revision$ $Date$
 */
public class DetalharEmissaoAutAvisosComprvResponse implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _codMensagem
     */
    private java.lang.String _codMensagem;

    /**
     * Field _mensagem
     */
    private java.lang.String _mensagem;

    /**
     * Field _cdProdutoServicoVinculo
     */
    private int _cdProdutoServicoVinculo = 0;

    /**
     * keeps track of state for field: _cdProdutoServicoVinculo
     */
    private boolean _has_cdProdutoServicoVinculo;

    /**
     * Field _dsProdutoServicoVinculo
     */
    private java.lang.String _dsProdutoServicoVinculo;

    /**
     * Field _cdProdutoServioOperacao
     */
    private int _cdProdutoServioOperacao = 0;

    /**
     * keeps track of state for field: _cdProdutoServioOperacao
     */
    private boolean _has_cdProdutoServioOperacao;

    /**
     * Field _dsProdutoServicoOperacao
     */
    private java.lang.String _dsProdutoServicoOperacao;

    /**
     * Field _cdProdutoOperacaoRelacionado
     */
    private int _cdProdutoOperacaoRelacionado = 0;

    /**
     * keeps track of state for field: _cdProdutoOperacaoRelacionado
     */
    private boolean _has_cdProdutoOperacaoRelacionado;

    /**
     * Field _dsProdutoOperacaoRelacionado
     */
    private java.lang.String _dsProdutoOperacaoRelacionado;

    /**
     * Field _cdRelacionamentoProduto
     */
    private int _cdRelacionamentoProduto = 0;

    /**
     * keeps track of state for field: _cdRelacionamentoProduto
     */
    private boolean _has_cdRelacionamentoProduto;

    /**
     * Field _cdSituacaoVinculacaoOperacao
     */
    private int _cdSituacaoVinculacaoOperacao = 0;

    /**
     * keeps track of state for field: _cdSituacaoVinculacaoOperacao
     */
    private boolean _has_cdSituacaoVinculacaoOperacao;

    /**
     * Field _dsSituacaoVinculacaoOperacao
     */
    private java.lang.String _dsSituacaoVinculacaoOperacao;

    /**
     * Field _cdIndicadorTipoManutencao
     */
    private int _cdIndicadorTipoManutencao = 0;

    /**
     * keeps track of state for field: _cdIndicadorTipoManutencao
     */
    private boolean _has_cdIndicadorTipoManutencao;

    /**
     * Field _cdCanalInclusao
     */
    private int _cdCanalInclusao = 0;

    /**
     * keeps track of state for field: _cdCanalInclusao
     */
    private boolean _has_cdCanalInclusao;

    /**
     * Field _dsCanalInclusao
     */
    private java.lang.String _dsCanalInclusao;

    /**
     * Field _cdAutenSegregacaoInclusao
     */
    private java.lang.String _cdAutenSegregacaoInclusao;

    /**
     * Field _nmOperacaoFluxoInclusao
     */
    private java.lang.String _nmOperacaoFluxoInclusao;

    /**
     * Field _hrInclusaoRegistro
     */
    private java.lang.String _hrInclusaoRegistro;

    /**
     * Field _cdCanalManutencao
     */
    private int _cdCanalManutencao = 0;

    /**
     * keeps track of state for field: _cdCanalManutencao
     */
    private boolean _has_cdCanalManutencao;

    /**
     * Field _dsCanalManutencao
     */
    private java.lang.String _dsCanalManutencao;

    /**
     * Field _cdAutenSegregacaoManutencao
     */
    private java.lang.String _cdAutenSegregacaoManutencao;

    /**
     * Field _nmOperacaoFluxoManutencao
     */
    private java.lang.String _nmOperacaoFluxoManutencao;

    /**
     * Field _hrManutencaoRegistro
     */
    private java.lang.String _hrManutencaoRegistro;


      //----------------/
     //- Constructors -/
    //----------------/

    public DetalharEmissaoAutAvisosComprvResponse() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharemissaoautavisoscomprv.response.DetalharEmissaoAutAvisosComprvResponse()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdCanalInclusao
     * 
     */
    public void deleteCdCanalInclusao()
    {
        this._has_cdCanalInclusao= false;
    } //-- void deleteCdCanalInclusao() 

    /**
     * Method deleteCdCanalManutencao
     * 
     */
    public void deleteCdCanalManutencao()
    {
        this._has_cdCanalManutencao= false;
    } //-- void deleteCdCanalManutencao() 

    /**
     * Method deleteCdIndicadorTipoManutencao
     * 
     */
    public void deleteCdIndicadorTipoManutencao()
    {
        this._has_cdIndicadorTipoManutencao= false;
    } //-- void deleteCdIndicadorTipoManutencao() 

    /**
     * Method deleteCdProdutoOperacaoRelacionado
     * 
     */
    public void deleteCdProdutoOperacaoRelacionado()
    {
        this._has_cdProdutoOperacaoRelacionado= false;
    } //-- void deleteCdProdutoOperacaoRelacionado() 

    /**
     * Method deleteCdProdutoServicoVinculo
     * 
     */
    public void deleteCdProdutoServicoVinculo()
    {
        this._has_cdProdutoServicoVinculo= false;
    } //-- void deleteCdProdutoServicoVinculo() 

    /**
     * Method deleteCdProdutoServioOperacao
     * 
     */
    public void deleteCdProdutoServioOperacao()
    {
        this._has_cdProdutoServioOperacao= false;
    } //-- void deleteCdProdutoServioOperacao() 

    /**
     * Method deleteCdRelacionamentoProduto
     * 
     */
    public void deleteCdRelacionamentoProduto()
    {
        this._has_cdRelacionamentoProduto= false;
    } //-- void deleteCdRelacionamentoProduto() 

    /**
     * Method deleteCdSituacaoVinculacaoOperacao
     * 
     */
    public void deleteCdSituacaoVinculacaoOperacao()
    {
        this._has_cdSituacaoVinculacaoOperacao= false;
    } //-- void deleteCdSituacaoVinculacaoOperacao() 

    /**
     * Returns the value of field 'cdAutenSegregacaoInclusao'.
     * 
     * @return String
     * @return the value of field 'cdAutenSegregacaoInclusao'.
     */
    public java.lang.String getCdAutenSegregacaoInclusao()
    {
        return this._cdAutenSegregacaoInclusao;
    } //-- java.lang.String getCdAutenSegregacaoInclusao() 

    /**
     * Returns the value of field 'cdAutenSegregacaoManutencao'.
     * 
     * @return String
     * @return the value of field 'cdAutenSegregacaoManutencao'.
     */
    public java.lang.String getCdAutenSegregacaoManutencao()
    {
        return this._cdAutenSegregacaoManutencao;
    } //-- java.lang.String getCdAutenSegregacaoManutencao() 

    /**
     * Returns the value of field 'cdCanalInclusao'.
     * 
     * @return int
     * @return the value of field 'cdCanalInclusao'.
     */
    public int getCdCanalInclusao()
    {
        return this._cdCanalInclusao;
    } //-- int getCdCanalInclusao() 

    /**
     * Returns the value of field 'cdCanalManutencao'.
     * 
     * @return int
     * @return the value of field 'cdCanalManutencao'.
     */
    public int getCdCanalManutencao()
    {
        return this._cdCanalManutencao;
    } //-- int getCdCanalManutencao() 

    /**
     * Returns the value of field 'cdIndicadorTipoManutencao'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorTipoManutencao'.
     */
    public int getCdIndicadorTipoManutencao()
    {
        return this._cdIndicadorTipoManutencao;
    } //-- int getCdIndicadorTipoManutencao() 

    /**
     * Returns the value of field 'cdProdutoOperacaoRelacionado'.
     * 
     * @return int
     * @return the value of field 'cdProdutoOperacaoRelacionado'.
     */
    public int getCdProdutoOperacaoRelacionado()
    {
        return this._cdProdutoOperacaoRelacionado;
    } //-- int getCdProdutoOperacaoRelacionado() 

    /**
     * Returns the value of field 'cdProdutoServicoVinculo'.
     * 
     * @return int
     * @return the value of field 'cdProdutoServicoVinculo'.
     */
    public int getCdProdutoServicoVinculo()
    {
        return this._cdProdutoServicoVinculo;
    } //-- int getCdProdutoServicoVinculo() 

    /**
     * Returns the value of field 'cdProdutoServioOperacao'.
     * 
     * @return int
     * @return the value of field 'cdProdutoServioOperacao'.
     */
    public int getCdProdutoServioOperacao()
    {
        return this._cdProdutoServioOperacao;
    } //-- int getCdProdutoServioOperacao() 

    /**
     * Returns the value of field 'cdRelacionamentoProduto'.
     * 
     * @return int
     * @return the value of field 'cdRelacionamentoProduto'.
     */
    public int getCdRelacionamentoProduto()
    {
        return this._cdRelacionamentoProduto;
    } //-- int getCdRelacionamentoProduto() 

    /**
     * Returns the value of field 'cdSituacaoVinculacaoOperacao'.
     * 
     * @return int
     * @return the value of field 'cdSituacaoVinculacaoOperacao'.
     */
    public int getCdSituacaoVinculacaoOperacao()
    {
        return this._cdSituacaoVinculacaoOperacao;
    } //-- int getCdSituacaoVinculacaoOperacao() 

    /**
     * Returns the value of field 'codMensagem'.
     * 
     * @return String
     * @return the value of field 'codMensagem'.
     */
    public java.lang.String getCodMensagem()
    {
        return this._codMensagem;
    } //-- java.lang.String getCodMensagem() 

    /**
     * Returns the value of field 'dsCanalInclusao'.
     * 
     * @return String
     * @return the value of field 'dsCanalInclusao'.
     */
    public java.lang.String getDsCanalInclusao()
    {
        return this._dsCanalInclusao;
    } //-- java.lang.String getDsCanalInclusao() 

    /**
     * Returns the value of field 'dsCanalManutencao'.
     * 
     * @return String
     * @return the value of field 'dsCanalManutencao'.
     */
    public java.lang.String getDsCanalManutencao()
    {
        return this._dsCanalManutencao;
    } //-- java.lang.String getDsCanalManutencao() 

    /**
     * Returns the value of field 'dsProdutoOperacaoRelacionado'.
     * 
     * @return String
     * @return the value of field 'dsProdutoOperacaoRelacionado'.
     */
    public java.lang.String getDsProdutoOperacaoRelacionado()
    {
        return this._dsProdutoOperacaoRelacionado;
    } //-- java.lang.String getDsProdutoOperacaoRelacionado() 

    /**
     * Returns the value of field 'dsProdutoServicoOperacao'.
     * 
     * @return String
     * @return the value of field 'dsProdutoServicoOperacao'.
     */
    public java.lang.String getDsProdutoServicoOperacao()
    {
        return this._dsProdutoServicoOperacao;
    } //-- java.lang.String getDsProdutoServicoOperacao() 

    /**
     * Returns the value of field 'dsProdutoServicoVinculo'.
     * 
     * @return String
     * @return the value of field 'dsProdutoServicoVinculo'.
     */
    public java.lang.String getDsProdutoServicoVinculo()
    {
        return this._dsProdutoServicoVinculo;
    } //-- java.lang.String getDsProdutoServicoVinculo() 

    /**
     * Returns the value of field 'dsSituacaoVinculacaoOperacao'.
     * 
     * @return String
     * @return the value of field 'dsSituacaoVinculacaoOperacao'.
     */
    public java.lang.String getDsSituacaoVinculacaoOperacao()
    {
        return this._dsSituacaoVinculacaoOperacao;
    } //-- java.lang.String getDsSituacaoVinculacaoOperacao() 

    /**
     * Returns the value of field 'hrInclusaoRegistro'.
     * 
     * @return String
     * @return the value of field 'hrInclusaoRegistro'.
     */
    public java.lang.String getHrInclusaoRegistro()
    {
        return this._hrInclusaoRegistro;
    } //-- java.lang.String getHrInclusaoRegistro() 

    /**
     * Returns the value of field 'hrManutencaoRegistro'.
     * 
     * @return String
     * @return the value of field 'hrManutencaoRegistro'.
     */
    public java.lang.String getHrManutencaoRegistro()
    {
        return this._hrManutencaoRegistro;
    } //-- java.lang.String getHrManutencaoRegistro() 

    /**
     * Returns the value of field 'mensagem'.
     * 
     * @return String
     * @return the value of field 'mensagem'.
     */
    public java.lang.String getMensagem()
    {
        return this._mensagem;
    } //-- java.lang.String getMensagem() 

    /**
     * Returns the value of field 'nmOperacaoFluxoInclusao'.
     * 
     * @return String
     * @return the value of field 'nmOperacaoFluxoInclusao'.
     */
    public java.lang.String getNmOperacaoFluxoInclusao()
    {
        return this._nmOperacaoFluxoInclusao;
    } //-- java.lang.String getNmOperacaoFluxoInclusao() 

    /**
     * Returns the value of field 'nmOperacaoFluxoManutencao'.
     * 
     * @return String
     * @return the value of field 'nmOperacaoFluxoManutencao'.
     */
    public java.lang.String getNmOperacaoFluxoManutencao()
    {
        return this._nmOperacaoFluxoManutencao;
    } //-- java.lang.String getNmOperacaoFluxoManutencao() 

    /**
     * Method hasCdCanalInclusao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCanalInclusao()
    {
        return this._has_cdCanalInclusao;
    } //-- boolean hasCdCanalInclusao() 

    /**
     * Method hasCdCanalManutencao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCanalManutencao()
    {
        return this._has_cdCanalManutencao;
    } //-- boolean hasCdCanalManutencao() 

    /**
     * Method hasCdIndicadorTipoManutencao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorTipoManutencao()
    {
        return this._has_cdIndicadorTipoManutencao;
    } //-- boolean hasCdIndicadorTipoManutencao() 

    /**
     * Method hasCdProdutoOperacaoRelacionado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdutoOperacaoRelacionado()
    {
        return this._has_cdProdutoOperacaoRelacionado;
    } //-- boolean hasCdProdutoOperacaoRelacionado() 

    /**
     * Method hasCdProdutoServicoVinculo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdutoServicoVinculo()
    {
        return this._has_cdProdutoServicoVinculo;
    } //-- boolean hasCdProdutoServicoVinculo() 

    /**
     * Method hasCdProdutoServioOperacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdutoServioOperacao()
    {
        return this._has_cdProdutoServioOperacao;
    } //-- boolean hasCdProdutoServioOperacao() 

    /**
     * Method hasCdRelacionamentoProduto
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdRelacionamentoProduto()
    {
        return this._has_cdRelacionamentoProduto;
    } //-- boolean hasCdRelacionamentoProduto() 

    /**
     * Method hasCdSituacaoVinculacaoOperacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSituacaoVinculacaoOperacao()
    {
        return this._has_cdSituacaoVinculacaoOperacao;
    } //-- boolean hasCdSituacaoVinculacaoOperacao() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdAutenSegregacaoInclusao'.
     * 
     * @param cdAutenSegregacaoInclusao the value of field
     * 'cdAutenSegregacaoInclusao'.
     */
    public void setCdAutenSegregacaoInclusao(java.lang.String cdAutenSegregacaoInclusao)
    {
        this._cdAutenSegregacaoInclusao = cdAutenSegregacaoInclusao;
    } //-- void setCdAutenSegregacaoInclusao(java.lang.String) 

    /**
     * Sets the value of field 'cdAutenSegregacaoManutencao'.
     * 
     * @param cdAutenSegregacaoManutencao the value of field
     * 'cdAutenSegregacaoManutencao'.
     */
    public void setCdAutenSegregacaoManutencao(java.lang.String cdAutenSegregacaoManutencao)
    {
        this._cdAutenSegregacaoManutencao = cdAutenSegregacaoManutencao;
    } //-- void setCdAutenSegregacaoManutencao(java.lang.String) 

    /**
     * Sets the value of field 'cdCanalInclusao'.
     * 
     * @param cdCanalInclusao the value of field 'cdCanalInclusao'.
     */
    public void setCdCanalInclusao(int cdCanalInclusao)
    {
        this._cdCanalInclusao = cdCanalInclusao;
        this._has_cdCanalInclusao = true;
    } //-- void setCdCanalInclusao(int) 

    /**
     * Sets the value of field 'cdCanalManutencao'.
     * 
     * @param cdCanalManutencao the value of field
     * 'cdCanalManutencao'.
     */
    public void setCdCanalManutencao(int cdCanalManutencao)
    {
        this._cdCanalManutencao = cdCanalManutencao;
        this._has_cdCanalManutencao = true;
    } //-- void setCdCanalManutencao(int) 

    /**
     * Sets the value of field 'cdIndicadorTipoManutencao'.
     * 
     * @param cdIndicadorTipoManutencao the value of field
     * 'cdIndicadorTipoManutencao'.
     */
    public void setCdIndicadorTipoManutencao(int cdIndicadorTipoManutencao)
    {
        this._cdIndicadorTipoManutencao = cdIndicadorTipoManutencao;
        this._has_cdIndicadorTipoManutencao = true;
    } //-- void setCdIndicadorTipoManutencao(int) 

    /**
     * Sets the value of field 'cdProdutoOperacaoRelacionado'.
     * 
     * @param cdProdutoOperacaoRelacionado the value of field
     * 'cdProdutoOperacaoRelacionado'.
     */
    public void setCdProdutoOperacaoRelacionado(int cdProdutoOperacaoRelacionado)
    {
        this._cdProdutoOperacaoRelacionado = cdProdutoOperacaoRelacionado;
        this._has_cdProdutoOperacaoRelacionado = true;
    } //-- void setCdProdutoOperacaoRelacionado(int) 

    /**
     * Sets the value of field 'cdProdutoServicoVinculo'.
     * 
     * @param cdProdutoServicoVinculo the value of field
     * 'cdProdutoServicoVinculo'.
     */
    public void setCdProdutoServicoVinculo(int cdProdutoServicoVinculo)
    {
        this._cdProdutoServicoVinculo = cdProdutoServicoVinculo;
        this._has_cdProdutoServicoVinculo = true;
    } //-- void setCdProdutoServicoVinculo(int) 

    /**
     * Sets the value of field 'cdProdutoServioOperacao'.
     * 
     * @param cdProdutoServioOperacao the value of field
     * 'cdProdutoServioOperacao'.
     */
    public void setCdProdutoServioOperacao(int cdProdutoServioOperacao)
    {
        this._cdProdutoServioOperacao = cdProdutoServioOperacao;
        this._has_cdProdutoServioOperacao = true;
    } //-- void setCdProdutoServioOperacao(int) 

    /**
     * Sets the value of field 'cdRelacionamentoProduto'.
     * 
     * @param cdRelacionamentoProduto the value of field
     * 'cdRelacionamentoProduto'.
     */
    public void setCdRelacionamentoProduto(int cdRelacionamentoProduto)
    {
        this._cdRelacionamentoProduto = cdRelacionamentoProduto;
        this._has_cdRelacionamentoProduto = true;
    } //-- void setCdRelacionamentoProduto(int) 

    /**
     * Sets the value of field 'cdSituacaoVinculacaoOperacao'.
     * 
     * @param cdSituacaoVinculacaoOperacao the value of field
     * 'cdSituacaoVinculacaoOperacao'.
     */
    public void setCdSituacaoVinculacaoOperacao(int cdSituacaoVinculacaoOperacao)
    {
        this._cdSituacaoVinculacaoOperacao = cdSituacaoVinculacaoOperacao;
        this._has_cdSituacaoVinculacaoOperacao = true;
    } //-- void setCdSituacaoVinculacaoOperacao(int) 

    /**
     * Sets the value of field 'codMensagem'.
     * 
     * @param codMensagem the value of field 'codMensagem'.
     */
    public void setCodMensagem(java.lang.String codMensagem)
    {
        this._codMensagem = codMensagem;
    } //-- void setCodMensagem(java.lang.String) 

    /**
     * Sets the value of field 'dsCanalInclusao'.
     * 
     * @param dsCanalInclusao the value of field 'dsCanalInclusao'.
     */
    public void setDsCanalInclusao(java.lang.String dsCanalInclusao)
    {
        this._dsCanalInclusao = dsCanalInclusao;
    } //-- void setDsCanalInclusao(java.lang.String) 

    /**
     * Sets the value of field 'dsCanalManutencao'.
     * 
     * @param dsCanalManutencao the value of field
     * 'dsCanalManutencao'.
     */
    public void setDsCanalManutencao(java.lang.String dsCanalManutencao)
    {
        this._dsCanalManutencao = dsCanalManutencao;
    } //-- void setDsCanalManutencao(java.lang.String) 

    /**
     * Sets the value of field 'dsProdutoOperacaoRelacionado'.
     * 
     * @param dsProdutoOperacaoRelacionado the value of field
     * 'dsProdutoOperacaoRelacionado'.
     */
    public void setDsProdutoOperacaoRelacionado(java.lang.String dsProdutoOperacaoRelacionado)
    {
        this._dsProdutoOperacaoRelacionado = dsProdutoOperacaoRelacionado;
    } //-- void setDsProdutoOperacaoRelacionado(java.lang.String) 

    /**
     * Sets the value of field 'dsProdutoServicoOperacao'.
     * 
     * @param dsProdutoServicoOperacao the value of field
     * 'dsProdutoServicoOperacao'.
     */
    public void setDsProdutoServicoOperacao(java.lang.String dsProdutoServicoOperacao)
    {
        this._dsProdutoServicoOperacao = dsProdutoServicoOperacao;
    } //-- void setDsProdutoServicoOperacao(java.lang.String) 

    /**
     * Sets the value of field 'dsProdutoServicoVinculo'.
     * 
     * @param dsProdutoServicoVinculo the value of field
     * 'dsProdutoServicoVinculo'.
     */
    public void setDsProdutoServicoVinculo(java.lang.String dsProdutoServicoVinculo)
    {
        this._dsProdutoServicoVinculo = dsProdutoServicoVinculo;
    } //-- void setDsProdutoServicoVinculo(java.lang.String) 

    /**
     * Sets the value of field 'dsSituacaoVinculacaoOperacao'.
     * 
     * @param dsSituacaoVinculacaoOperacao the value of field
     * 'dsSituacaoVinculacaoOperacao'.
     */
    public void setDsSituacaoVinculacaoOperacao(java.lang.String dsSituacaoVinculacaoOperacao)
    {
        this._dsSituacaoVinculacaoOperacao = dsSituacaoVinculacaoOperacao;
    } //-- void setDsSituacaoVinculacaoOperacao(java.lang.String) 

    /**
     * Sets the value of field 'hrInclusaoRegistro'.
     * 
     * @param hrInclusaoRegistro the value of field
     * 'hrInclusaoRegistro'.
     */
    public void setHrInclusaoRegistro(java.lang.String hrInclusaoRegistro)
    {
        this._hrInclusaoRegistro = hrInclusaoRegistro;
    } //-- void setHrInclusaoRegistro(java.lang.String) 

    /**
     * Sets the value of field 'hrManutencaoRegistro'.
     * 
     * @param hrManutencaoRegistro the value of field
     * 'hrManutencaoRegistro'.
     */
    public void setHrManutencaoRegistro(java.lang.String hrManutencaoRegistro)
    {
        this._hrManutencaoRegistro = hrManutencaoRegistro;
    } //-- void setHrManutencaoRegistro(java.lang.String) 

    /**
     * Sets the value of field 'mensagem'.
     * 
     * @param mensagem the value of field 'mensagem'.
     */
    public void setMensagem(java.lang.String mensagem)
    {
        this._mensagem = mensagem;
    } //-- void setMensagem(java.lang.String) 

    /**
     * Sets the value of field 'nmOperacaoFluxoInclusao'.
     * 
     * @param nmOperacaoFluxoInclusao the value of field
     * 'nmOperacaoFluxoInclusao'.
     */
    public void setNmOperacaoFluxoInclusao(java.lang.String nmOperacaoFluxoInclusao)
    {
        this._nmOperacaoFluxoInclusao = nmOperacaoFluxoInclusao;
    } //-- void setNmOperacaoFluxoInclusao(java.lang.String) 

    /**
     * Sets the value of field 'nmOperacaoFluxoManutencao'.
     * 
     * @param nmOperacaoFluxoManutencao the value of field
     * 'nmOperacaoFluxoManutencao'.
     */
    public void setNmOperacaoFluxoManutencao(java.lang.String nmOperacaoFluxoManutencao)
    {
        this._nmOperacaoFluxoManutencao = nmOperacaoFluxoManutencao;
    } //-- void setNmOperacaoFluxoManutencao(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return DetalharEmissaoAutAvisosComprvResponse
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.detalharemissaoautavisoscomprv.response.DetalharEmissaoAutAvisosComprvResponse unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.detalharemissaoautavisoscomprv.response.DetalharEmissaoAutAvisosComprvResponse) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.detalharemissaoautavisoscomprv.response.DetalharEmissaoAutAvisosComprvResponse.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharemissaoautavisoscomprv.response.DetalharEmissaoAutAvisosComprvResponse unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
