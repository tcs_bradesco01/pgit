/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.detalharsolbasesistema.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.math.BigDecimal;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class DetalharSolBaseSistemaResponse.
 * 
 * @version $Revision$ $Date$
 */
public class DetalharSolBaseSistemaResponse implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _codMensagem
     */
    private java.lang.String _codMensagem;

    /**
     * Field _mensagem
     */
    private java.lang.String _mensagem;

    /**
     * Field _nrSolicitacaoPagamento
     */
    private int _nrSolicitacaoPagamento = 0;

    /**
     * keeps track of state for field: _nrSolicitacaoPagamento
     */
    private boolean _has_nrSolicitacaoPagamento;

    /**
     * Field _hrSolicitacao
     */
    private java.lang.String _hrSolicitacao;

    /**
     * Field _dsSituacaoSolicitacao
     */
    private java.lang.String _dsSituacaoSolicitacao;

    /**
     * Field _resultadoProcessamento
     */
    private java.lang.String _resultadoProcessamento;

    /**
     * Field _hrAtendimentoSolicitacao
     */
    private java.lang.String _hrAtendimentoSolicitacao;

    /**
     * Field _qtdeRegistrosSolicitacao
     */
    private long _qtdeRegistrosSolicitacao = 0;

    /**
     * keeps track of state for field: _qtdeRegistrosSolicitacao
     */
    private boolean _has_qtdeRegistrosSolicitacao;

    /**
     * Field _cdBaseSistema
     */
    private int _cdBaseSistema = 0;

    /**
     * keeps track of state for field: _cdBaseSistema
     */
    private boolean _has_cdBaseSistema;

    /**
     * Field _dsBaseSistema
     */
    private java.lang.String _dsBaseSistema;

    /**
     * Field _vlTarifaPadrao
     */
    private java.math.BigDecimal _vlTarifaPadrao = new java.math.BigDecimal("0");

    /**
     * Field _numPercentualDescTarifa
     */
    private java.math.BigDecimal _numPercentualDescTarifa = new java.math.BigDecimal("0");

    /**
     * Field _vlrTarifaAtual
     */
    private java.math.BigDecimal _vlrTarifaAtual = new java.math.BigDecimal("0");

    /**
     * Field _cdCanalInclusao
     */
    private int _cdCanalInclusao = 0;

    /**
     * keeps track of state for field: _cdCanalInclusao
     */
    private boolean _has_cdCanalInclusao;

    /**
     * Field _dsCanalInclusao
     */
    private java.lang.String _dsCanalInclusao;

    /**
     * Field _cdAutenticacaoSegurancaInclusao
     */
    private java.lang.String _cdAutenticacaoSegurancaInclusao;

    /**
     * Field _nmOperacaoFluxoInlcusao
     */
    private java.lang.String _nmOperacaoFluxoInlcusao;

    /**
     * Field _hrInclusaoRegistro
     */
    private java.lang.String _hrInclusaoRegistro;

    /**
     * Field _cdCanalManutencao
     */
    private int _cdCanalManutencao = 0;

    /**
     * keeps track of state for field: _cdCanalManutencao
     */
    private boolean _has_cdCanalManutencao;

    /**
     * Field _dsCanalManutencao
     */
    private java.lang.String _dsCanalManutencao;

    /**
     * Field _cdAutenticacaoSegurancaManutencao
     */
    private java.lang.String _cdAutenticacaoSegurancaManutencao;

    /**
     * Field _nmOperacaoFluxoManutencao
     */
    private java.lang.String _nmOperacaoFluxoManutencao;

    /**
     * Field _hrManutencaoRegistro
     */
    private java.lang.String _hrManutencaoRegistro;


      //----------------/
     //- Constructors -/
    //----------------/

    public DetalharSolBaseSistemaResponse() 
     {
        super();
        setVlTarifaPadrao(new java.math.BigDecimal("0"));
        setNumPercentualDescTarifa(new java.math.BigDecimal("0"));
        setVlrTarifaAtual(new java.math.BigDecimal("0"));
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharsolbasesistema.response.DetalharSolBaseSistemaResponse()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdBaseSistema
     * 
     */
    public void deleteCdBaseSistema()
    {
        this._has_cdBaseSistema= false;
    } //-- void deleteCdBaseSistema() 

    /**
     * Method deleteCdCanalInclusao
     * 
     */
    public void deleteCdCanalInclusao()
    {
        this._has_cdCanalInclusao= false;
    } //-- void deleteCdCanalInclusao() 

    /**
     * Method deleteCdCanalManutencao
     * 
     */
    public void deleteCdCanalManutencao()
    {
        this._has_cdCanalManutencao= false;
    } //-- void deleteCdCanalManutencao() 

    /**
     * Method deleteNrSolicitacaoPagamento
     * 
     */
    public void deleteNrSolicitacaoPagamento()
    {
        this._has_nrSolicitacaoPagamento= false;
    } //-- void deleteNrSolicitacaoPagamento() 

    /**
     * Method deleteQtdeRegistrosSolicitacao
     * 
     */
    public void deleteQtdeRegistrosSolicitacao()
    {
        this._has_qtdeRegistrosSolicitacao= false;
    } //-- void deleteQtdeRegistrosSolicitacao() 

    /**
     * Returns the value of field
     * 'cdAutenticacaoSegurancaInclusao'.
     * 
     * @return String
     * @return the value of field 'cdAutenticacaoSegurancaInclusao'.
     */
    public java.lang.String getCdAutenticacaoSegurancaInclusao()
    {
        return this._cdAutenticacaoSegurancaInclusao;
    } //-- java.lang.String getCdAutenticacaoSegurancaInclusao() 

    /**
     * Returns the value of field
     * 'cdAutenticacaoSegurancaManutencao'.
     * 
     * @return String
     * @return the value of field
     * 'cdAutenticacaoSegurancaManutencao'.
     */
    public java.lang.String getCdAutenticacaoSegurancaManutencao()
    {
        return this._cdAutenticacaoSegurancaManutencao;
    } //-- java.lang.String getCdAutenticacaoSegurancaManutencao() 

    /**
     * Returns the value of field 'cdBaseSistema'.
     * 
     * @return int
     * @return the value of field 'cdBaseSistema'.
     */
    public int getCdBaseSistema()
    {
        return this._cdBaseSistema;
    } //-- int getCdBaseSistema() 

    /**
     * Returns the value of field 'cdCanalInclusao'.
     * 
     * @return int
     * @return the value of field 'cdCanalInclusao'.
     */
    public int getCdCanalInclusao()
    {
        return this._cdCanalInclusao;
    } //-- int getCdCanalInclusao() 

    /**
     * Returns the value of field 'cdCanalManutencao'.
     * 
     * @return int
     * @return the value of field 'cdCanalManutencao'.
     */
    public int getCdCanalManutencao()
    {
        return this._cdCanalManutencao;
    } //-- int getCdCanalManutencao() 

    /**
     * Returns the value of field 'codMensagem'.
     * 
     * @return String
     * @return the value of field 'codMensagem'.
     */
    public java.lang.String getCodMensagem()
    {
        return this._codMensagem;
    } //-- java.lang.String getCodMensagem() 

    /**
     * Returns the value of field 'dsBaseSistema'.
     * 
     * @return String
     * @return the value of field 'dsBaseSistema'.
     */
    public java.lang.String getDsBaseSistema()
    {
        return this._dsBaseSistema;
    } //-- java.lang.String getDsBaseSistema() 

    /**
     * Returns the value of field 'dsCanalInclusao'.
     * 
     * @return String
     * @return the value of field 'dsCanalInclusao'.
     */
    public java.lang.String getDsCanalInclusao()
    {
        return this._dsCanalInclusao;
    } //-- java.lang.String getDsCanalInclusao() 

    /**
     * Returns the value of field 'dsCanalManutencao'.
     * 
     * @return String
     * @return the value of field 'dsCanalManutencao'.
     */
    public java.lang.String getDsCanalManutencao()
    {
        return this._dsCanalManutencao;
    } //-- java.lang.String getDsCanalManutencao() 

    /**
     * Returns the value of field 'dsSituacaoSolicitacao'.
     * 
     * @return String
     * @return the value of field 'dsSituacaoSolicitacao'.
     */
    public java.lang.String getDsSituacaoSolicitacao()
    {
        return this._dsSituacaoSolicitacao;
    } //-- java.lang.String getDsSituacaoSolicitacao() 

    /**
     * Returns the value of field 'hrAtendimentoSolicitacao'.
     * 
     * @return String
     * @return the value of field 'hrAtendimentoSolicitacao'.
     */
    public java.lang.String getHrAtendimentoSolicitacao()
    {
        return this._hrAtendimentoSolicitacao;
    } //-- java.lang.String getHrAtendimentoSolicitacao() 

    /**
     * Returns the value of field 'hrInclusaoRegistro'.
     * 
     * @return String
     * @return the value of field 'hrInclusaoRegistro'.
     */
    public java.lang.String getHrInclusaoRegistro()
    {
        return this._hrInclusaoRegistro;
    } //-- java.lang.String getHrInclusaoRegistro() 

    /**
     * Returns the value of field 'hrManutencaoRegistro'.
     * 
     * @return String
     * @return the value of field 'hrManutencaoRegistro'.
     */
    public java.lang.String getHrManutencaoRegistro()
    {
        return this._hrManutencaoRegistro;
    } //-- java.lang.String getHrManutencaoRegistro() 

    /**
     * Returns the value of field 'hrSolicitacao'.
     * 
     * @return String
     * @return the value of field 'hrSolicitacao'.
     */
    public java.lang.String getHrSolicitacao()
    {
        return this._hrSolicitacao;
    } //-- java.lang.String getHrSolicitacao() 

    /**
     * Returns the value of field 'mensagem'.
     * 
     * @return String
     * @return the value of field 'mensagem'.
     */
    public java.lang.String getMensagem()
    {
        return this._mensagem;
    } //-- java.lang.String getMensagem() 

    /**
     * Returns the value of field 'nmOperacaoFluxoInlcusao'.
     * 
     * @return String
     * @return the value of field 'nmOperacaoFluxoInlcusao'.
     */
    public java.lang.String getNmOperacaoFluxoInlcusao()
    {
        return this._nmOperacaoFluxoInlcusao;
    } //-- java.lang.String getNmOperacaoFluxoInlcusao() 

    /**
     * Returns the value of field 'nmOperacaoFluxoManutencao'.
     * 
     * @return String
     * @return the value of field 'nmOperacaoFluxoManutencao'.
     */
    public java.lang.String getNmOperacaoFluxoManutencao()
    {
        return this._nmOperacaoFluxoManutencao;
    } //-- java.lang.String getNmOperacaoFluxoManutencao() 

    /**
     * Returns the value of field 'nrSolicitacaoPagamento'.
     * 
     * @return int
     * @return the value of field 'nrSolicitacaoPagamento'.
     */
    public int getNrSolicitacaoPagamento()
    {
        return this._nrSolicitacaoPagamento;
    } //-- int getNrSolicitacaoPagamento() 

    /**
     * Returns the value of field 'numPercentualDescTarifa'.
     * 
     * @return BigDecimal
     * @return the value of field 'numPercentualDescTarifa'.
     */
    public java.math.BigDecimal getNumPercentualDescTarifa()
    {
        return this._numPercentualDescTarifa;
    } //-- java.math.BigDecimal getNumPercentualDescTarifa() 

    /**
     * Returns the value of field 'qtdeRegistrosSolicitacao'.
     * 
     * @return long
     * @return the value of field 'qtdeRegistrosSolicitacao'.
     */
    public long getQtdeRegistrosSolicitacao()
    {
        return this._qtdeRegistrosSolicitacao;
    } //-- long getQtdeRegistrosSolicitacao() 

    /**
     * Returns the value of field 'resultadoProcessamento'.
     * 
     * @return String
     * @return the value of field 'resultadoProcessamento'.
     */
    public java.lang.String getResultadoProcessamento()
    {
        return this._resultadoProcessamento;
    } //-- java.lang.String getResultadoProcessamento() 

    /**
     * Returns the value of field 'vlTarifaPadrao'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlTarifaPadrao'.
     */
    public java.math.BigDecimal getVlTarifaPadrao()
    {
        return this._vlTarifaPadrao;
    } //-- java.math.BigDecimal getVlTarifaPadrao() 

    /**
     * Returns the value of field 'vlrTarifaAtual'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlrTarifaAtual'.
     */
    public java.math.BigDecimal getVlrTarifaAtual()
    {
        return this._vlrTarifaAtual;
    } //-- java.math.BigDecimal getVlrTarifaAtual() 

    /**
     * Method hasCdBaseSistema
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdBaseSistema()
    {
        return this._has_cdBaseSistema;
    } //-- boolean hasCdBaseSistema() 

    /**
     * Method hasCdCanalInclusao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCanalInclusao()
    {
        return this._has_cdCanalInclusao;
    } //-- boolean hasCdCanalInclusao() 

    /**
     * Method hasCdCanalManutencao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCanalManutencao()
    {
        return this._has_cdCanalManutencao;
    } //-- boolean hasCdCanalManutencao() 

    /**
     * Method hasNrSolicitacaoPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSolicitacaoPagamento()
    {
        return this._has_nrSolicitacaoPagamento;
    } //-- boolean hasNrSolicitacaoPagamento() 

    /**
     * Method hasQtdeRegistrosSolicitacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtdeRegistrosSolicitacao()
    {
        return this._has_qtdeRegistrosSolicitacao;
    } //-- boolean hasQtdeRegistrosSolicitacao() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdAutenticacaoSegurancaInclusao'.
     * 
     * @param cdAutenticacaoSegurancaInclusao the value of field
     * 'cdAutenticacaoSegurancaInclusao'.
     */
    public void setCdAutenticacaoSegurancaInclusao(java.lang.String cdAutenticacaoSegurancaInclusao)
    {
        this._cdAutenticacaoSegurancaInclusao = cdAutenticacaoSegurancaInclusao;
    } //-- void setCdAutenticacaoSegurancaInclusao(java.lang.String) 

    /**
     * Sets the value of field 'cdAutenticacaoSegurancaManutencao'.
     * 
     * @param cdAutenticacaoSegurancaManutencao the value of field
     * 'cdAutenticacaoSegurancaManutencao'.
     */
    public void setCdAutenticacaoSegurancaManutencao(java.lang.String cdAutenticacaoSegurancaManutencao)
    {
        this._cdAutenticacaoSegurancaManutencao = cdAutenticacaoSegurancaManutencao;
    } //-- void setCdAutenticacaoSegurancaManutencao(java.lang.String) 

    /**
     * Sets the value of field 'cdBaseSistema'.
     * 
     * @param cdBaseSistema the value of field 'cdBaseSistema'.
     */
    public void setCdBaseSistema(int cdBaseSistema)
    {
        this._cdBaseSistema = cdBaseSistema;
        this._has_cdBaseSistema = true;
    } //-- void setCdBaseSistema(int) 

    /**
     * Sets the value of field 'cdCanalInclusao'.
     * 
     * @param cdCanalInclusao the value of field 'cdCanalInclusao'.
     */
    public void setCdCanalInclusao(int cdCanalInclusao)
    {
        this._cdCanalInclusao = cdCanalInclusao;
        this._has_cdCanalInclusao = true;
    } //-- void setCdCanalInclusao(int) 

    /**
     * Sets the value of field 'cdCanalManutencao'.
     * 
     * @param cdCanalManutencao the value of field
     * 'cdCanalManutencao'.
     */
    public void setCdCanalManutencao(int cdCanalManutencao)
    {
        this._cdCanalManutencao = cdCanalManutencao;
        this._has_cdCanalManutencao = true;
    } //-- void setCdCanalManutencao(int) 

    /**
     * Sets the value of field 'codMensagem'.
     * 
     * @param codMensagem the value of field 'codMensagem'.
     */
    public void setCodMensagem(java.lang.String codMensagem)
    {
        this._codMensagem = codMensagem;
    } //-- void setCodMensagem(java.lang.String) 

    /**
     * Sets the value of field 'dsBaseSistema'.
     * 
     * @param dsBaseSistema the value of field 'dsBaseSistema'.
     */
    public void setDsBaseSistema(java.lang.String dsBaseSistema)
    {
        this._dsBaseSistema = dsBaseSistema;
    } //-- void setDsBaseSistema(java.lang.String) 

    /**
     * Sets the value of field 'dsCanalInclusao'.
     * 
     * @param dsCanalInclusao the value of field 'dsCanalInclusao'.
     */
    public void setDsCanalInclusao(java.lang.String dsCanalInclusao)
    {
        this._dsCanalInclusao = dsCanalInclusao;
    } //-- void setDsCanalInclusao(java.lang.String) 

    /**
     * Sets the value of field 'dsCanalManutencao'.
     * 
     * @param dsCanalManutencao the value of field
     * 'dsCanalManutencao'.
     */
    public void setDsCanalManutencao(java.lang.String dsCanalManutencao)
    {
        this._dsCanalManutencao = dsCanalManutencao;
    } //-- void setDsCanalManutencao(java.lang.String) 

    /**
     * Sets the value of field 'dsSituacaoSolicitacao'.
     * 
     * @param dsSituacaoSolicitacao the value of field
     * 'dsSituacaoSolicitacao'.
     */
    public void setDsSituacaoSolicitacao(java.lang.String dsSituacaoSolicitacao)
    {
        this._dsSituacaoSolicitacao = dsSituacaoSolicitacao;
    } //-- void setDsSituacaoSolicitacao(java.lang.String) 

    /**
     * Sets the value of field 'hrAtendimentoSolicitacao'.
     * 
     * @param hrAtendimentoSolicitacao the value of field
     * 'hrAtendimentoSolicitacao'.
     */
    public void setHrAtendimentoSolicitacao(java.lang.String hrAtendimentoSolicitacao)
    {
        this._hrAtendimentoSolicitacao = hrAtendimentoSolicitacao;
    } //-- void setHrAtendimentoSolicitacao(java.lang.String) 

    /**
     * Sets the value of field 'hrInclusaoRegistro'.
     * 
     * @param hrInclusaoRegistro the value of field
     * 'hrInclusaoRegistro'.
     */
    public void setHrInclusaoRegistro(java.lang.String hrInclusaoRegistro)
    {
        this._hrInclusaoRegistro = hrInclusaoRegistro;
    } //-- void setHrInclusaoRegistro(java.lang.String) 

    /**
     * Sets the value of field 'hrManutencaoRegistro'.
     * 
     * @param hrManutencaoRegistro the value of field
     * 'hrManutencaoRegistro'.
     */
    public void setHrManutencaoRegistro(java.lang.String hrManutencaoRegistro)
    {
        this._hrManutencaoRegistro = hrManutencaoRegistro;
    } //-- void setHrManutencaoRegistro(java.lang.String) 

    /**
     * Sets the value of field 'hrSolicitacao'.
     * 
     * @param hrSolicitacao the value of field 'hrSolicitacao'.
     */
    public void setHrSolicitacao(java.lang.String hrSolicitacao)
    {
        this._hrSolicitacao = hrSolicitacao;
    } //-- void setHrSolicitacao(java.lang.String) 

    /**
     * Sets the value of field 'mensagem'.
     * 
     * @param mensagem the value of field 'mensagem'.
     */
    public void setMensagem(java.lang.String mensagem)
    {
        this._mensagem = mensagem;
    } //-- void setMensagem(java.lang.String) 

    /**
     * Sets the value of field 'nmOperacaoFluxoInlcusao'.
     * 
     * @param nmOperacaoFluxoInlcusao the value of field
     * 'nmOperacaoFluxoInlcusao'.
     */
    public void setNmOperacaoFluxoInlcusao(java.lang.String nmOperacaoFluxoInlcusao)
    {
        this._nmOperacaoFluxoInlcusao = nmOperacaoFluxoInlcusao;
    } //-- void setNmOperacaoFluxoInlcusao(java.lang.String) 

    /**
     * Sets the value of field 'nmOperacaoFluxoManutencao'.
     * 
     * @param nmOperacaoFluxoManutencao the value of field
     * 'nmOperacaoFluxoManutencao'.
     */
    public void setNmOperacaoFluxoManutencao(java.lang.String nmOperacaoFluxoManutencao)
    {
        this._nmOperacaoFluxoManutencao = nmOperacaoFluxoManutencao;
    } //-- void setNmOperacaoFluxoManutencao(java.lang.String) 

    /**
     * Sets the value of field 'nrSolicitacaoPagamento'.
     * 
     * @param nrSolicitacaoPagamento the value of field
     * 'nrSolicitacaoPagamento'.
     */
    public void setNrSolicitacaoPagamento(int nrSolicitacaoPagamento)
    {
        this._nrSolicitacaoPagamento = nrSolicitacaoPagamento;
        this._has_nrSolicitacaoPagamento = true;
    } //-- void setNrSolicitacaoPagamento(int) 

    /**
     * Sets the value of field 'numPercentualDescTarifa'.
     * 
     * @param numPercentualDescTarifa the value of field
     * 'numPercentualDescTarifa'.
     */
    public void setNumPercentualDescTarifa(java.math.BigDecimal numPercentualDescTarifa)
    {
        this._numPercentualDescTarifa = numPercentualDescTarifa;
    } //-- void setNumPercentualDescTarifa(java.math.BigDecimal) 

    /**
     * Sets the value of field 'qtdeRegistrosSolicitacao'.
     * 
     * @param qtdeRegistrosSolicitacao the value of field
     * 'qtdeRegistrosSolicitacao'.
     */
    public void setQtdeRegistrosSolicitacao(long qtdeRegistrosSolicitacao)
    {
        this._qtdeRegistrosSolicitacao = qtdeRegistrosSolicitacao;
        this._has_qtdeRegistrosSolicitacao = true;
    } //-- void setQtdeRegistrosSolicitacao(long) 

    /**
     * Sets the value of field 'resultadoProcessamento'.
     * 
     * @param resultadoProcessamento the value of field
     * 'resultadoProcessamento'.
     */
    public void setResultadoProcessamento(java.lang.String resultadoProcessamento)
    {
        this._resultadoProcessamento = resultadoProcessamento;
    } //-- void setResultadoProcessamento(java.lang.String) 

    /**
     * Sets the value of field 'vlTarifaPadrao'.
     * 
     * @param vlTarifaPadrao the value of field 'vlTarifaPadrao'.
     */
    public void setVlTarifaPadrao(java.math.BigDecimal vlTarifaPadrao)
    {
        this._vlTarifaPadrao = vlTarifaPadrao;
    } //-- void setVlTarifaPadrao(java.math.BigDecimal) 

    /**
     * Sets the value of field 'vlrTarifaAtual'.
     * 
     * @param vlrTarifaAtual the value of field 'vlrTarifaAtual'.
     */
    public void setVlrTarifaAtual(java.math.BigDecimal vlrTarifaAtual)
    {
        this._vlrTarifaAtual = vlrTarifaAtual;
    } //-- void setVlrTarifaAtual(java.math.BigDecimal) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return DetalharSolBaseSistemaResponse
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.detalharsolbasesistema.response.DetalharSolBaseSistemaResponse unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.detalharsolbasesistema.response.DetalharSolBaseSistemaResponse) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.detalharsolbasesistema.response.DetalharSolBaseSistemaResponse.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharsolbasesistema.response.DetalharSolBaseSistemaResponse unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
