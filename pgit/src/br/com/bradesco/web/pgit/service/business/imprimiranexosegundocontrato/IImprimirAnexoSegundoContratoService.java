/**
 * Nome: br.com.bradesco.web.pgit.service.business.imprimiranexosegundocontrato
 * Compilador: JDK 1.5
 * Prop�sito: INSERIR O PROP�SITO DAS CLASSES DO PACOTE
 * Data da cria��o: <dd/MM/yyyy>
 * Par�metros de compila��o: -d
 */
package br.com.bradesco.web.pgit.service.business.imprimiranexosegundocontrato;

import br.com.bradesco.web.pgit.service.business.imprimiranexosegundocontrato.bean.ImprimirAnexoSegundoContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.imprimiranexosegundocontrato.bean.ImprimirAnexoSegundoContratoSaidaDTO;


/**
 * Nome: IImprimirAnexoSegundoContratoService
 * <p>
 * Prop�sito: Interface do adaptador ImprimirAnexoSegundoContrato
 * </p>
 * 
 * @author CPM Braxis / TI Melhorias - Arquitetura
 * @version 1.0
 */
public interface IImprimirAnexoSegundoContratoService {

    /**
     * M�todo de exemplo
     */
    public ImprimirAnexoSegundoContratoSaidaDTO imprimirAnexoPrimeiroContrato(ImprimirAnexoSegundoContratoEntradaDTO entrada);
}