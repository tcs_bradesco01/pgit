/*
 * Nome: br.com.bradesco.web.pgit.service.business.filtroidentificao.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.filtroidentificao.bean;

import br.com.bradesco.web.pgit.service.data.pdc.consultarlistacontratospessoas.response.Ocorrencias;

/**
 * Nome: ConsultarListaContratosPessoasSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarListaContratosPessoasSaidaDTO {
	
	/** Atributo codMensagem. */
	private String codMensagem;
	
	/** Atributo mensagem. */
	private String mensagem;
	
	/** Atributo cdPessoaJuridicaContrato. */
	private Long cdPessoaJuridicaContrato;
	
	/** Atributo dsPessoaJuridicaContrato. */
	private String dsPessoaJuridicaContrato;
	
	/** Atributo cdTipoParticipacaoPessoa. */
	private Integer cdTipoParticipacaoPessoa;
	
	/** Atributo dsTipoParticipacaoPessoa. */
	private String dsTipoParticipacaoPessoa;
	
	/** Atributo cdTipoContratoNegocio. */
	private Integer cdTipoContratoNegocio;
	
	/** Atributo dsTipoContratoNegocio. */
	private String dsTipoContratoNegocio;
	
	/** Atributo nrSeqContratoNegocio. */
	private Long nrSeqContratoNegocio;
	
	/** Atributo dsContrato. */
	private String dsContrato;
	
	/** Atributo cdSituacaoContratoNegocio. */
	private Integer cdSituacaoContratoNegocio;
	
	/** Atributo dsSituacaoContratoNegocio. */
	private String dsSituacaoContratoNegocio;
	
	/** Atributo cdCpfCnpjParticipante. */
	private Long cdCpfCnpjParticipante;
	
	/** Atributo cdFilialCnpjParticipante. */
	private Integer cdFilialCnpjParticipante;
	
	/** Atributo cdControleCpfCnpjParticipante. */
	private Integer cdControleCpfCnpjParticipante;
	
	/** Atributo cdPessoaParticipante. */
	private Long cdPessoaParticipante;
	
	/** Atributo dsPessoaParticipante. */
	private String dsPessoaParticipante;
	
	/** Atributo cnpjOuCpfFormatado. */
	private String cnpjOuCpfFormatado;

	/**
	 * Consultar lista contratos pessoas saida dto.
	 */
	public ConsultarListaContratosPessoasSaidaDTO() {
		super();
	}

	/**
	 * Consultar lista contratos pessoas saida dto.
	 *
	 * @param saida the saida
	 */
	public ConsultarListaContratosPessoasSaidaDTO(Ocorrencias saida) {
		super();
		
		this.cdPessoaJuridicaContrato = saida.getCdPessoaJuridicaContrato();
	    this.dsPessoaJuridicaContrato = saida.getDsPessoaJuridicaContrato();
	    this.cdTipoParticipacaoPessoa = saida.getCdTipoParticipacaoPessoa();
	    this.dsTipoParticipacaoPessoa = saida.getDsTipoParticipacaoPessoa();
	    this.cdTipoContratoNegocio = saida.getCdTipoContratoNegocio();
	    this.dsTipoContratoNegocio = saida.getDsTipoContratoNegocio();
	    this.nrSeqContratoNegocio = saida.getNrSeqContratoNegocio();
	    
	    //Ap�s o email do dia 30/03/2011 - 18:26 - Andr� Fernandes Vieira o pdc ficou na vers�o seis e esses campos foram exclui�dos. 
	   /*his.cdSituacaoParticipacaoContrato = saida.getCdSituacaoParticipacaoContrato();
	    this.dsSituacaoParticipacaoContrato = saida.getDsSituacaoParticipacaoContrato();
	    this.dtInicioParticipacaoContrato = saida.getDtInicioParticipacaoContrato();
	    this.dtFimPessoaContrato = saida.getDtFimPessoaContrato();*/
	    this.dsContrato = saida.getDsContrato();
	    this.cdSituacaoContratoNegocio = saida.getCdSituacaoContratoNegocio(); 
	    this.dsSituacaoContratoNegocio = saida.getDsSituacaoContratoNegocio();

	}
	
    /**
     * Get: descricaoPessoaJuridicaFormatada.
     *
     * @return descricaoPessoaJuridicaFormatada
     */
    public String getDescricaoPessoaJuridicaFormatada(){
    	StringBuilder descricaoFormatada = new StringBuilder();
    	descricaoFormatada.append(cdPessoaJuridicaContrato);
    	descricaoFormatada.append(" - ");
    	descricaoFormatada.append(dsPessoaJuridicaContrato);
    	
    	return descricaoFormatada.toString();
    }

	/**
	 * Get: cdPessoaJuridicaContrato.
	 *
	 * @return cdPessoaJuridicaContrato
	 */
	public Long getCdPessoaJuridicaContrato() {
		return cdPessoaJuridicaContrato;
	}

	/**
	 * Set: cdPessoaJuridicaContrato.
	 *
	 * @param cdPessoaJuridicaContrato the cd pessoa juridica contrato
	 */
	public void setCdPessoaJuridicaContrato(Long cdPessoaJuridicaContrato) {
		this.cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
	}

	/**
	 * Get: cdSituacaoContratoNegocio.
	 *
	 * @return cdSituacaoContratoNegocio
	 */
	public Integer getCdSituacaoContratoNegocio() {
		return cdSituacaoContratoNegocio;
	}

	/**
	 * Set: cdSituacaoContratoNegocio.
	 *
	 * @param cdSituacaoContratoNegocio the cd situacao contrato negocio
	 */
	public void setCdSituacaoContratoNegocio(Integer cdSituacaoContratoNegocio) {
		this.cdSituacaoContratoNegocio = cdSituacaoContratoNegocio;
	}

	/**
	 * Get: cdTipoContratoNegocio.
	 *
	 * @return cdTipoContratoNegocio
	 */
	public Integer getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}

	/**
	 * Set: cdTipoContratoNegocio.
	 *
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}

	/**
	 * Get: cdTipoParticipacaoPessoa.
	 *
	 * @return cdTipoParticipacaoPessoa
	 */
	public Integer getCdTipoParticipacaoPessoa() {
		return cdTipoParticipacaoPessoa;
	}

	/**
	 * Set: cdTipoParticipacaoPessoa.
	 *
	 * @param cdTipoParticipacaoPessoa the cd tipo participacao pessoa
	 */
	public void setCdTipoParticipacaoPessoa(Integer cdTipoParticipacaoPessoa) {
		this.cdTipoParticipacaoPessoa = cdTipoParticipacaoPessoa;
	}

	/**
	 * Get: dsContrato.
	 *
	 * @return dsContrato
	 */
	public String getDsContrato() {
		return dsContrato;
	}

	/**
	 * Set: dsContrato.
	 *
	 * @param dsContrato the ds contrato
	 */
	public void setDsContrato(String dsContrato) {
		this.dsContrato = dsContrato;
	}

	/**
	 * Get: dsPessoaJuridicaContrato.
	 *
	 * @return dsPessoaJuridicaContrato
	 */
	public String getDsPessoaJuridicaContrato() {
		return dsPessoaJuridicaContrato;
	}

	/**
	 * Set: dsPessoaJuridicaContrato.
	 *
	 * @param dsPessoaJuridicaContrato the ds pessoa juridica contrato
	 */
	public void setDsPessoaJuridicaContrato(String dsPessoaJuridicaContrato) {
		this.dsPessoaJuridicaContrato = dsPessoaJuridicaContrato;
	}

	/**
	 * Get: dsSituacaoContratoNegocio.
	 *
	 * @return dsSituacaoContratoNegocio
	 */
	public String getDsSituacaoContratoNegocio() {
		return dsSituacaoContratoNegocio;
	}

	/**
	 * Set: dsSituacaoContratoNegocio.
	 *
	 * @param dsSituacaoContratoNegocio the ds situacao contrato negocio
	 */
	public void setDsSituacaoContratoNegocio(String dsSituacaoContratoNegocio) {
		this.dsSituacaoContratoNegocio = dsSituacaoContratoNegocio;
	}

	/**
	 * Get: dsTipoContratoNegocio.
	 *
	 * @return dsTipoContratoNegocio
	 */
	public String getDsTipoContratoNegocio() {
		return dsTipoContratoNegocio;
	}

	/**
	 * Set: dsTipoContratoNegocio.
	 *
	 * @param dsTipoContratoNegocio the ds tipo contrato negocio
	 */
	public void setDsTipoContratoNegocio(String dsTipoContratoNegocio) {
		this.dsTipoContratoNegocio = dsTipoContratoNegocio;
	}

	/**
	 * Get: dsTipoParticipacaoPessoa.
	 *
	 * @return dsTipoParticipacaoPessoa
	 */
	public String getDsTipoParticipacaoPessoa() {
		return dsTipoParticipacaoPessoa;
	}

	/**
	 * Set: dsTipoParticipacaoPessoa.
	 *
	 * @param dsTipoParticipacaoPessoa the ds tipo participacao pessoa
	 */
	public void setDsTipoParticipacaoPessoa(String dsTipoParticipacaoPessoa) {
		this.dsTipoParticipacaoPessoa = dsTipoParticipacaoPessoa;
	}

	/**
	 * Get: nrSeqContratoNegocio.
	 *
	 * @return nrSeqContratoNegocio
	 */
	public Long getNrSeqContratoNegocio() {
		return nrSeqContratoNegocio;
	}

	/**
	 * Set: nrSeqContratoNegocio.
	 *
	 * @param nrSeqContratoNegocio the nr seq contrato negocio
	 */
	public void setNrSeqContratoNegocio(Long nrSeqContratoNegocio) {
		this.nrSeqContratoNegocio = nrSeqContratoNegocio;
	}

	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}

	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}

	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}

	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	/**
	 * Get: cdControleCpfCnpjParticipante.
	 *
	 * @return cdControleCpfCnpjParticipante
	 */
	public Integer getCdControleCpfCnpjParticipante() {
		return cdControleCpfCnpjParticipante;
	}

	/**
	 * Set: cdControleCpfCnpjParticipante.
	 *
	 * @param cdControleCpfCnpjParticipante the cd controle cpf cnpj participante
	 */
	public void setCdControleCpfCnpjParticipante(
			Integer cdControleCpfCnpjParticipante) {
		this.cdControleCpfCnpjParticipante = cdControleCpfCnpjParticipante;
	}

	/**
	 * Get: cdCpfCnpjParticipante.
	 *
	 * @return cdCpfCnpjParticipante
	 */
	public Long getCdCpfCnpjParticipante() {
		return cdCpfCnpjParticipante;
	}

	/**
	 * Set: cdCpfCnpjParticipante.
	 *
	 * @param cdCpfCnpjParticipante the cd cpf cnpj participante
	 */
	public void setCdCpfCnpjParticipante(Long cdCpfCnpjParticipante) {
		this.cdCpfCnpjParticipante = cdCpfCnpjParticipante;
	}

	/**
	 * Get: cdFilialCnpjParticipante.
	 *
	 * @return cdFilialCnpjParticipante
	 */
	public Integer getCdFilialCnpjParticipante() {
		return cdFilialCnpjParticipante;
	}

	/**
	 * Set: cdFilialCnpjParticipante.
	 *
	 * @param cdFilialCnpjParticipante the cd filial cnpj participante
	 */
	public void setCdFilialCnpjParticipante(Integer cdFilialCnpjParticipante) {
		this.cdFilialCnpjParticipante = cdFilialCnpjParticipante;
	}

	/**
	 * Get: cdPessoaParticipante.
	 *
	 * @return cdPessoaParticipante
	 */
	public Long getCdPessoaParticipante() {
		return cdPessoaParticipante;
	}

	/**
	 * Set: cdPessoaParticipante.
	 *
	 * @param cdPessoaParticipante the cd pessoa participante
	 */
	public void setCdPessoaParticipante(Long cdPessoaParticipante) {
		this.cdPessoaParticipante = cdPessoaParticipante;
	}

	/**
	 * Get: dsPessoaParticipante.
	 *
	 * @return dsPessoaParticipante
	 */
	public String getDsPessoaParticipante() {
		return dsPessoaParticipante;
	}

	/**
	 * Set: dsPessoaParticipante.
	 *
	 * @param dsPessoaParticipante the ds pessoa participante
	 */
	public void setDsPessoaParticipante(String dsPessoaParticipante) {
		this.dsPessoaParticipante = dsPessoaParticipante;
	}

	/**
	 * Get: cnpjOuCpfFormatado.
	 *
	 * @return cnpjOuCpfFormatado
	 */
	public String getCnpjOuCpfFormatado() {
		return cnpjOuCpfFormatado;
	}

	/**
	 * Set: cnpjOuCpfFormatado.
	 *
	 * @param cnpjOuCpfFormatado the cnpj ou cpf formatado
	 */
	public void setCnpjOuCpfFormatado(String cnpjOuCpfFormatado) {
		this.cnpjOuCpfFormatado = cnpjOuCpfFormatado;
	}

}
