/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultarsituacaosolicitacaoestorno.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _codigo
     */
    private int _codigo = 0;

    /**
     * keeps track of state for field: _codigo
     */
    private boolean _has_codigo;

    /**
     * Field _descricao
     */
    private java.lang.String _descricao;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarsituacaosolicitacaoestorno.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCodigo
     * 
     */
    public void deleteCodigo()
    {
        this._has_codigo= false;
    } //-- void deleteCodigo() 

    /**
     * Returns the value of field 'codigo'.
     * 
     * @return int
     * @return the value of field 'codigo'.
     */
    public int getCodigo()
    {
        return this._codigo;
    } //-- int getCodigo() 

    /**
     * Returns the value of field 'descricao'.
     * 
     * @return String
     * @return the value of field 'descricao'.
     */
    public java.lang.String getDescricao()
    {
        return this._descricao;
    } //-- java.lang.String getDescricao() 

    /**
     * Method hasCodigo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCodigo()
    {
        return this._has_codigo;
    } //-- boolean hasCodigo() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'codigo'.
     * 
     * @param codigo the value of field 'codigo'.
     */
    public void setCodigo(int codigo)
    {
        this._codigo = codigo;
        this._has_codigo = true;
    } //-- void setCodigo(int) 

    /**
     * Sets the value of field 'descricao'.
     * 
     * @param descricao the value of field 'descricao'.
     */
    public void setDescricao(java.lang.String descricao)
    {
        this._descricao = descricao;
    } //-- void setDescricao(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultarsituacaosolicitacaoestorno.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultarsituacaosolicitacaoestorno.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultarsituacaosolicitacaoestorno.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarsituacaosolicitacaoestorno.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
