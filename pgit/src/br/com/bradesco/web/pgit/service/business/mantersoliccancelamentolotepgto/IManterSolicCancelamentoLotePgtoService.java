/**
 * Nome: br.com.bradesco.web.pgit.service.business.mantersoliccancelamentolotepgto
 * Compilador: JDK 1.5
 * Prop�sito: INSERIR O PROP�SITO DAS CLASSES DO PACOTE
 * Data da cria��o: <dd/MM/yyyy>
 * Par�metros de compila��o: -d
 */
package br.com.bradesco.web.pgit.service.business.mantersoliccancelamentolotepgto;

import br.com.bradesco.web.pgit.service.business.lotepagamentos.bean.ConsultarLotePagamentosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.lotepagamentos.bean.ConsultarLotePagamentosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.lotepagamentos.bean.DetalharLotePagamentosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.lotepagamentos.bean.DetalharLotePagamentosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.lotepagamentos.bean.ExcluirLotePagamentosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.lotepagamentos.bean.ExcluirLotePagamentosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.lotepagamentos.bean.IncluirLotePagamentosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.lotepagamentos.bean.IncluirLotePagamentosSaidaDTO;

/**
 * Nome: IManterSolicCancelamentoLotePgtoService
 * <p>
 * Prop�sito: Interface do adaptador ManterSolicCancelamentoLotePgto
 * </p>
 * 
 * @author CPM Braxis / TI Melhorias - Arquitetura
 * @version 1.0
 */
public interface IManterSolicCancelamentoLotePgtoService {

	public ConsultarLotePagamentosSaidaDTO consultarSolicCancelamentoLotePgto(
			ConsultarLotePagamentosEntradaDTO entrada);

	public IncluirLotePagamentosSaidaDTO incluirSolicCancelamentoLotePgto(
			IncluirLotePagamentosEntradaDTO entrada);

	public DetalharLotePagamentosSaidaDTO detalharSolicCancelamentoLotePgto(
			DetalharLotePagamentosEntradaDTO entrada);

	public ExcluirLotePagamentosSaidaDTO excluirSolicCancelamentoLotePgto(
			ExcluirLotePagamentosEntradaDTO entrada);
}