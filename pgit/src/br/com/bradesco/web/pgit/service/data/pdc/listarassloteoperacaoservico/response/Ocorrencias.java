/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listarassloteoperacaoservico.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdTipoLayoutArquivo
     */
    private int _cdTipoLayoutArquivo = 0;

    /**
     * keeps track of state for field: _cdTipoLayoutArquivo
     */
    private boolean _has_cdTipoLayoutArquivo;

    /**
     * Field _dsTipoLayoutArquivo
     */
    private java.lang.String _dsTipoLayoutArquivo;

    /**
     * Field _cdTipoLoteLayout
     */
    private int _cdTipoLoteLayout = 0;

    /**
     * keeps track of state for field: _cdTipoLoteLayout
     */
    private boolean _has_cdTipoLoteLayout;

    /**
     * Field _dsTipoLoteLayout
     */
    private java.lang.String _dsTipoLoteLayout;

    /**
     * Field _cdProdutoServicoOper
     */
    private int _cdProdutoServicoOper = 0;

    /**
     * keeps track of state for field: _cdProdutoServicoOper
     */
    private boolean _has_cdProdutoServicoOper;

    /**
     * Field _dsProdutoServicoOper
     */
    private java.lang.String _dsProdutoServicoOper;

    /**
     * Field _cdProdutoOperRelacionado
     */
    private int _cdProdutoOperRelacionado = 0;

    /**
     * keeps track of state for field: _cdProdutoOperRelacionado
     */
    private boolean _has_cdProdutoOperRelacionado;

    /**
     * Field _dsProdutoOperRelacionado
     */
    private java.lang.String _dsProdutoOperRelacionado;

    /**
     * Field _cdRelacionadoProduto
     */
    private int _cdRelacionadoProduto = 0;

    /**
     * keeps track of state for field: _cdRelacionadoProduto
     */
    private boolean _has_cdRelacionadoProduto;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarassloteoperacaoservico.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdProdutoOperRelacionado
     * 
     */
    public void deleteCdProdutoOperRelacionado()
    {
        this._has_cdProdutoOperRelacionado= false;
    } //-- void deleteCdProdutoOperRelacionado() 

    /**
     * Method deleteCdProdutoServicoOper
     * 
     */
    public void deleteCdProdutoServicoOper()
    {
        this._has_cdProdutoServicoOper= false;
    } //-- void deleteCdProdutoServicoOper() 

    /**
     * Method deleteCdRelacionadoProduto
     * 
     */
    public void deleteCdRelacionadoProduto()
    {
        this._has_cdRelacionadoProduto= false;
    } //-- void deleteCdRelacionadoProduto() 

    /**
     * Method deleteCdTipoLayoutArquivo
     * 
     */
    public void deleteCdTipoLayoutArquivo()
    {
        this._has_cdTipoLayoutArquivo= false;
    } //-- void deleteCdTipoLayoutArquivo() 

    /**
     * Method deleteCdTipoLoteLayout
     * 
     */
    public void deleteCdTipoLoteLayout()
    {
        this._has_cdTipoLoteLayout= false;
    } //-- void deleteCdTipoLoteLayout() 

    /**
     * Returns the value of field 'cdProdutoOperRelacionado'.
     * 
     * @return int
     * @return the value of field 'cdProdutoOperRelacionado'.
     */
    public int getCdProdutoOperRelacionado()
    {
        return this._cdProdutoOperRelacionado;
    } //-- int getCdProdutoOperRelacionado() 

    /**
     * Returns the value of field 'cdProdutoServicoOper'.
     * 
     * @return int
     * @return the value of field 'cdProdutoServicoOper'.
     */
    public int getCdProdutoServicoOper()
    {
        return this._cdProdutoServicoOper;
    } //-- int getCdProdutoServicoOper() 

    /**
     * Returns the value of field 'cdRelacionadoProduto'.
     * 
     * @return int
     * @return the value of field 'cdRelacionadoProduto'.
     */
    public int getCdRelacionadoProduto()
    {
        return this._cdRelacionadoProduto;
    } //-- int getCdRelacionadoProduto() 

    /**
     * Returns the value of field 'cdTipoLayoutArquivo'.
     * 
     * @return int
     * @return the value of field 'cdTipoLayoutArquivo'.
     */
    public int getCdTipoLayoutArquivo()
    {
        return this._cdTipoLayoutArquivo;
    } //-- int getCdTipoLayoutArquivo() 

    /**
     * Returns the value of field 'cdTipoLoteLayout'.
     * 
     * @return int
     * @return the value of field 'cdTipoLoteLayout'.
     */
    public int getCdTipoLoteLayout()
    {
        return this._cdTipoLoteLayout;
    } //-- int getCdTipoLoteLayout() 

    /**
     * Returns the value of field 'dsProdutoOperRelacionado'.
     * 
     * @return String
     * @return the value of field 'dsProdutoOperRelacionado'.
     */
    public java.lang.String getDsProdutoOperRelacionado()
    {
        return this._dsProdutoOperRelacionado;
    } //-- java.lang.String getDsProdutoOperRelacionado() 

    /**
     * Returns the value of field 'dsProdutoServicoOper'.
     * 
     * @return String
     * @return the value of field 'dsProdutoServicoOper'.
     */
    public java.lang.String getDsProdutoServicoOper()
    {
        return this._dsProdutoServicoOper;
    } //-- java.lang.String getDsProdutoServicoOper() 

    /**
     * Returns the value of field 'dsTipoLayoutArquivo'.
     * 
     * @return String
     * @return the value of field 'dsTipoLayoutArquivo'.
     */
    public java.lang.String getDsTipoLayoutArquivo()
    {
        return this._dsTipoLayoutArquivo;
    } //-- java.lang.String getDsTipoLayoutArquivo() 

    /**
     * Returns the value of field 'dsTipoLoteLayout'.
     * 
     * @return String
     * @return the value of field 'dsTipoLoteLayout'.
     */
    public java.lang.String getDsTipoLoteLayout()
    {
        return this._dsTipoLoteLayout;
    } //-- java.lang.String getDsTipoLoteLayout() 

    /**
     * Method hasCdProdutoOperRelacionado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdutoOperRelacionado()
    {
        return this._has_cdProdutoOperRelacionado;
    } //-- boolean hasCdProdutoOperRelacionado() 

    /**
     * Method hasCdProdutoServicoOper
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdutoServicoOper()
    {
        return this._has_cdProdutoServicoOper;
    } //-- boolean hasCdProdutoServicoOper() 

    /**
     * Method hasCdRelacionadoProduto
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdRelacionadoProduto()
    {
        return this._has_cdRelacionadoProduto;
    } //-- boolean hasCdRelacionadoProduto() 

    /**
     * Method hasCdTipoLayoutArquivo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoLayoutArquivo()
    {
        return this._has_cdTipoLayoutArquivo;
    } //-- boolean hasCdTipoLayoutArquivo() 

    /**
     * Method hasCdTipoLoteLayout
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoLoteLayout()
    {
        return this._has_cdTipoLoteLayout;
    } //-- boolean hasCdTipoLoteLayout() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdProdutoOperRelacionado'.
     * 
     * @param cdProdutoOperRelacionado the value of field
     * 'cdProdutoOperRelacionado'.
     */
    public void setCdProdutoOperRelacionado(int cdProdutoOperRelacionado)
    {
        this._cdProdutoOperRelacionado = cdProdutoOperRelacionado;
        this._has_cdProdutoOperRelacionado = true;
    } //-- void setCdProdutoOperRelacionado(int) 

    /**
     * Sets the value of field 'cdProdutoServicoOper'.
     * 
     * @param cdProdutoServicoOper the value of field
     * 'cdProdutoServicoOper'.
     */
    public void setCdProdutoServicoOper(int cdProdutoServicoOper)
    {
        this._cdProdutoServicoOper = cdProdutoServicoOper;
        this._has_cdProdutoServicoOper = true;
    } //-- void setCdProdutoServicoOper(int) 

    /**
     * Sets the value of field 'cdRelacionadoProduto'.
     * 
     * @param cdRelacionadoProduto the value of field
     * 'cdRelacionadoProduto'.
     */
    public void setCdRelacionadoProduto(int cdRelacionadoProduto)
    {
        this._cdRelacionadoProduto = cdRelacionadoProduto;
        this._has_cdRelacionadoProduto = true;
    } //-- void setCdRelacionadoProduto(int) 

    /**
     * Sets the value of field 'cdTipoLayoutArquivo'.
     * 
     * @param cdTipoLayoutArquivo the value of field
     * 'cdTipoLayoutArquivo'.
     */
    public void setCdTipoLayoutArquivo(int cdTipoLayoutArquivo)
    {
        this._cdTipoLayoutArquivo = cdTipoLayoutArquivo;
        this._has_cdTipoLayoutArquivo = true;
    } //-- void setCdTipoLayoutArquivo(int) 

    /**
     * Sets the value of field 'cdTipoLoteLayout'.
     * 
     * @param cdTipoLoteLayout the value of field 'cdTipoLoteLayout'
     */
    public void setCdTipoLoteLayout(int cdTipoLoteLayout)
    {
        this._cdTipoLoteLayout = cdTipoLoteLayout;
        this._has_cdTipoLoteLayout = true;
    } //-- void setCdTipoLoteLayout(int) 

    /**
     * Sets the value of field 'dsProdutoOperRelacionado'.
     * 
     * @param dsProdutoOperRelacionado the value of field
     * 'dsProdutoOperRelacionado'.
     */
    public void setDsProdutoOperRelacionado(java.lang.String dsProdutoOperRelacionado)
    {
        this._dsProdutoOperRelacionado = dsProdutoOperRelacionado;
    } //-- void setDsProdutoOperRelacionado(java.lang.String) 

    /**
     * Sets the value of field 'dsProdutoServicoOper'.
     * 
     * @param dsProdutoServicoOper the value of field
     * 'dsProdutoServicoOper'.
     */
    public void setDsProdutoServicoOper(java.lang.String dsProdutoServicoOper)
    {
        this._dsProdutoServicoOper = dsProdutoServicoOper;
    } //-- void setDsProdutoServicoOper(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoLayoutArquivo'.
     * 
     * @param dsTipoLayoutArquivo the value of field
     * 'dsTipoLayoutArquivo'.
     */
    public void setDsTipoLayoutArquivo(java.lang.String dsTipoLayoutArquivo)
    {
        this._dsTipoLayoutArquivo = dsTipoLayoutArquivo;
    } //-- void setDsTipoLayoutArquivo(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoLoteLayout'.
     * 
     * @param dsTipoLoteLayout the value of field 'dsTipoLoteLayout'
     */
    public void setDsTipoLoteLayout(java.lang.String dsTipoLoteLayout)
    {
        this._dsTipoLoteLayout = dsTipoLoteLayout;
    } //-- void setDsTipoLoteLayout(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listarassloteoperacaoservico.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listarassloteoperacaoservico.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listarassloteoperacaoservico.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarassloteoperacaoservico.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
