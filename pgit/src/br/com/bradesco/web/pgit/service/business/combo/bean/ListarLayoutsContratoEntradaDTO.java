package br.com.bradesco.web.pgit.service.business.combo.bean;


// TODO: Auto-generated Javadoc
/**
 * Arquivo criado em 22/02/16.
 */
public class ListarLayoutsContratoEntradaDTO {

    /** The max ocorrencias. */
    private Integer maxOcorrencias;

    /** The cdpessoa juridica contrato. */
    private Long cdpessoaJuridicaContrato;

    /** The cd tipo contrato. */
    private Integer cdTipoContrato;

    /** The nr sequencia contrato negocio. */
    private Long nrSequenciaContratoNegocio;

    /** The cd produto servico operacao. */
    private Integer cdProdutoServicoOperacao;

    /** The cd produto operacao relacionado. */
    private Integer cdProdutoOperacaoRelacionado;

    /**
     * Set max ocorrencias.
     *
     * @param maxOcorrencias the max ocorrencias
     */
    public void setMaxOcorrencias(Integer maxOcorrencias) {
        this.maxOcorrencias = maxOcorrencias;
    }

    /**
     * Get max ocorrencias.
     *
     * @return the max ocorrencias
     */
    public Integer getMaxOcorrencias() {
        return this.maxOcorrencias;
    }

    /**
     * Set cdpessoa juridica contrato.
     *
     * @param cdpessoaJuridicaContrato the cdpessoa juridica contrato
     */
    public void setCdpessoaJuridicaContrato(Long cdpessoaJuridicaContrato) {
        this.cdpessoaJuridicaContrato = cdpessoaJuridicaContrato;
    }

    /**
     * Get cdpessoa juridica contrato.
     *
     * @return the cdpessoa juridica contrato
     */
    public Long getCdpessoaJuridicaContrato() {
        return this.cdpessoaJuridicaContrato;
    }

    /**
     * Set cd tipo contrato.
     *
     * @param cdTipoContrato the cd tipo contrato
     */
    public void setCdTipoContrato(Integer cdTipoContrato) {
        this.cdTipoContrato = cdTipoContrato;
    }

    /**
     * Get cd tipo contrato.
     *
     * @return the cd tipo contrato
     */
    public Integer getCdTipoContrato() {
        return this.cdTipoContrato;
    }

    /**
     * Set nr sequencia contrato negocio.
     *
     * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
     */
    public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
        this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
    }

    /**
     * Get nr sequencia contrato negocio.
     *
     * @return the nr sequencia contrato negocio
     */
    public Long getNrSequenciaContratoNegocio() {
        return this.nrSequenciaContratoNegocio;
    }

    /**
     * Set cd produto servico operacao.
     *
     * @param cdProdutoServicoOperacao the cd produto servico operacao
     */
    public void setCdProdutoServicoOperacao(Integer cdProdutoServicoOperacao) {
        this.cdProdutoServicoOperacao = cdProdutoServicoOperacao;
    }

    /**
     * Get cd produto servico operacao.
     *
     * @return the cd produto servico operacao
     */
    public Integer getCdProdutoServicoOperacao() {
        return this.cdProdutoServicoOperacao;
    }

    /**
     * Set cd produto operacao relacionado.
     *
     * @param cdProdutoOperacaoRelacionado the cd produto operacao relacionado
     */
    public void setCdProdutoOperacaoRelacionado(Integer cdProdutoOperacaoRelacionado) {
        this.cdProdutoOperacaoRelacionado = cdProdutoOperacaoRelacionado;
    }

    /**
     * Get cd produto operacao relacionado.
     *
     * @return the cd produto operacao relacionado
     */
    public Integer getCdProdutoOperacaoRelacionado() {
        return this.cdProdutoOperacaoRelacionado;
    }
}