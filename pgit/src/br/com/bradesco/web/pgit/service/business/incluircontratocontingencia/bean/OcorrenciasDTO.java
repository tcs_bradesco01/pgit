/*
 * Nome: br.com.bradesco.web.pgit.service.business.incluircontratocontingencia.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.incluircontratocontingencia.bean;

/**
 * Nome: OcorrenciasDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class OcorrenciasDTO {
	
	/** Atributo cdProduto. */
	private Integer cdProduto;

	/**
	 * Ocorrencias dto.
	 *
	 * @param cdProduto the cd produto
	 */
	public OcorrenciasDTO(Integer cdProduto){
		this.cdProduto = cdProduto;
	}
	
	/**
	 * Get: cdProduto.
	 *
	 * @return cdProduto
	 */
	public Integer getCdProduto() {
		return cdProduto;
	}

	/**
	 * Set: cdProduto.
	 *
	 * @param cdProduto the cd produto
	 */
	public void setCdProduto(Integer cdProduto) {
		this.cdProduto = cdProduto;
	}
}