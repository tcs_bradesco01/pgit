/**
 * Nome: br.com.bradesco.web.pgit.service.business.imprimiranexoprimeirocontrato.exceptions
 * Compilador: JDK 1.5
 * Prop�sito: INSERIR O PROP�SITO DAS CLASSES DO PACOTE
 * Data da cria��o: <dd/MM/yyyy>
 * Par�metros de compila��o: -d
 */
package br.com.bradesco.web.pgit.service.business.imprimiranexoprimeirocontrato.exceptions;

import br.com.bradesco.web.aq.application.error.BradescoApplicationException;

/**
 * Nome: ImprimirAnexoPrimeiroContratoServiceException
 * <p>
 * Prop�sito: Classe de exce��o do adaptador ImprimirAnexoPrimeiroContrato
 * </p>
 * 
 * @author CPM Braxis / TI Melhorias - Arquitetura
 * @version 1.0
 * @see BradescoApplicationException
 */
public class ImprimirAnexoPrimeiroContratoServiceException extends BradescoApplicationException  {

}