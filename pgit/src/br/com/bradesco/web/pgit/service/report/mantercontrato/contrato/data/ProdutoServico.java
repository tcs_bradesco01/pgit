/*
 * Nome: br.com.bradesco.web.pgit.service.report.contrato.data
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 05/02/2016
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.report.mantercontrato.contrato.data;

/**
 * Nome: ProdutoServico
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ProdutoServico {

    /** Atributo produto. */
    private String produto = null;

    /** Atributo servico. */
    private String servico = null;

    /**
     * Instancia um novo ProdutoServico
     *
     * @see
     */
    public ProdutoServico() {
        super();
    }

    /**
     * Nome: getProduto.
     *
     * @return produto
     */
    public String getProduto() {
        return produto;
    }

    /**
     * Nome: setProduto.
     *
     * @param produto the produto
     */
    public void setProduto(String produto) {
        this.produto = produto;
    }

    /**
     * Nome: getServico.
     *
     * @return servico
     */
    public String getServico() {
        return servico;
    }

    /**
     * Nome: setServico.
     *
     * @param servico the servico
     */
    public void setServico(String servico) {
        this.servico = servico;
    }
}
