/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.detalharsolicrecuperacao.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.math.BigDecimal;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdPessoa
     */
    private long _cdPessoa = 0;

    /**
     * keeps track of state for field: _cdPessoa
     */
    private boolean _has_cdPessoa;

    /**
     * Field _cdCnpjCpf
     */
    private java.lang.String _cdCnpjCpf;

    /**
     * Field _dsPessoa
     */
    private java.lang.String _dsPessoa;

    /**
     * Field _cdTipoLayoutArquivo
     */
    private int _cdTipoLayoutArquivo = 0;

    /**
     * keeps track of state for field: _cdTipoLayoutArquivo
     */
    private boolean _has_cdTipoLayoutArquivo;

    /**
     * Field _dsTipoLayoutArquivo
     */
    private java.lang.String _dsTipoLayoutArquivo;

    /**
     * Field _cdClienteTransferenciaArquivo
     */
    private long _cdClienteTransferenciaArquivo = 0;

    /**
     * keeps track of state for field: _cdClienteTransferenciaArquiv
     */
    private boolean _has_cdClienteTransferenciaArquivo;

    /**
     * Field _nrSequenciaRemessa
     */
    private long _nrSequenciaRemessa = 0;

    /**
     * keeps track of state for field: _nrSequenciaRemessa
     */
    private boolean _has_nrSequenciaRemessa;

    /**
     * Field _hrInclusaoRemessa
     */
    private java.lang.String _hrInclusaoRemessa;

    /**
     * Field _dsSituacaoProcessamento
     */
    private java.lang.String _dsSituacaoProcessamento;

    /**
     * Field _dsCondicaoProcessamento
     */
    private java.lang.String _dsCondicaoProcessamento;

    /**
     * Field _dsMeioTransmissao
     */
    private java.lang.String _dsMeioTransmissao;

    /**
     * Field _qtdeRegistro
     */
    private long _qtdeRegistro = 0;

    /**
     * keeps track of state for field: _qtdeRegistro
     */
    private boolean _has_qtdeRegistro;

    /**
     * Field _vlrRegistro
     */
    private java.math.BigDecimal _vlrRegistro = new java.math.BigDecimal("0");


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
        setVlrRegistro(new java.math.BigDecimal("0"));
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharsolicrecuperacao.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdClienteTransferenciaArquivo
     * 
     */
    public void deleteCdClienteTransferenciaArquivo()
    {
        this._has_cdClienteTransferenciaArquivo= false;
    } //-- void deleteCdClienteTransferenciaArquivo() 

    /**
     * Method deleteCdPessoa
     * 
     */
    public void deleteCdPessoa()
    {
        this._has_cdPessoa= false;
    } //-- void deleteCdPessoa() 

    /**
     * Method deleteCdTipoLayoutArquivo
     * 
     */
    public void deleteCdTipoLayoutArquivo()
    {
        this._has_cdTipoLayoutArquivo= false;
    } //-- void deleteCdTipoLayoutArquivo() 

    /**
     * Method deleteNrSequenciaRemessa
     * 
     */
    public void deleteNrSequenciaRemessa()
    {
        this._has_nrSequenciaRemessa= false;
    } //-- void deleteNrSequenciaRemessa() 

    /**
     * Method deleteQtdeRegistro
     * 
     */
    public void deleteQtdeRegistro()
    {
        this._has_qtdeRegistro= false;
    } //-- void deleteQtdeRegistro() 

    /**
     * Returns the value of field 'cdClienteTransferenciaArquivo'.
     * 
     * @return long
     * @return the value of field 'cdClienteTransferenciaArquivo'.
     */
    public long getCdClienteTransferenciaArquivo()
    {
        return this._cdClienteTransferenciaArquivo;
    } //-- long getCdClienteTransferenciaArquivo() 

    /**
     * Returns the value of field 'cdCnpjCpf'.
     * 
     * @return String
     * @return the value of field 'cdCnpjCpf'.
     */
    public java.lang.String getCdCnpjCpf()
    {
        return this._cdCnpjCpf;
    } //-- java.lang.String getCdCnpjCpf() 

    /**
     * Returns the value of field 'cdPessoa'.
     * 
     * @return long
     * @return the value of field 'cdPessoa'.
     */
    public long getCdPessoa()
    {
        return this._cdPessoa;
    } //-- long getCdPessoa() 

    /**
     * Returns the value of field 'cdTipoLayoutArquivo'.
     * 
     * @return int
     * @return the value of field 'cdTipoLayoutArquivo'.
     */
    public int getCdTipoLayoutArquivo()
    {
        return this._cdTipoLayoutArquivo;
    } //-- int getCdTipoLayoutArquivo() 

    /**
     * Returns the value of field 'dsCondicaoProcessamento'.
     * 
     * @return String
     * @return the value of field 'dsCondicaoProcessamento'.
     */
    public java.lang.String getDsCondicaoProcessamento()
    {
        return this._dsCondicaoProcessamento;
    } //-- java.lang.String getDsCondicaoProcessamento() 

    /**
     * Returns the value of field 'dsMeioTransmissao'.
     * 
     * @return String
     * @return the value of field 'dsMeioTransmissao'.
     */
    public java.lang.String getDsMeioTransmissao()
    {
        return this._dsMeioTransmissao;
    } //-- java.lang.String getDsMeioTransmissao() 

    /**
     * Returns the value of field 'dsPessoa'.
     * 
     * @return String
     * @return the value of field 'dsPessoa'.
     */
    public java.lang.String getDsPessoa()
    {
        return this._dsPessoa;
    } //-- java.lang.String getDsPessoa() 

    /**
     * Returns the value of field 'dsSituacaoProcessamento'.
     * 
     * @return String
     * @return the value of field 'dsSituacaoProcessamento'.
     */
    public java.lang.String getDsSituacaoProcessamento()
    {
        return this._dsSituacaoProcessamento;
    } //-- java.lang.String getDsSituacaoProcessamento() 

    /**
     * Returns the value of field 'dsTipoLayoutArquivo'.
     * 
     * @return String
     * @return the value of field 'dsTipoLayoutArquivo'.
     */
    public java.lang.String getDsTipoLayoutArquivo()
    {
        return this._dsTipoLayoutArquivo;
    } //-- java.lang.String getDsTipoLayoutArquivo() 

    /**
     * Returns the value of field 'hrInclusaoRemessa'.
     * 
     * @return String
     * @return the value of field 'hrInclusaoRemessa'.
     */
    public java.lang.String getHrInclusaoRemessa()
    {
        return this._hrInclusaoRemessa;
    } //-- java.lang.String getHrInclusaoRemessa() 

    /**
     * Returns the value of field 'nrSequenciaRemessa'.
     * 
     * @return long
     * @return the value of field 'nrSequenciaRemessa'.
     */
    public long getNrSequenciaRemessa()
    {
        return this._nrSequenciaRemessa;
    } //-- long getNrSequenciaRemessa() 

    /**
     * Returns the value of field 'qtdeRegistro'.
     * 
     * @return long
     * @return the value of field 'qtdeRegistro'.
     */
    public long getQtdeRegistro()
    {
        return this._qtdeRegistro;
    } //-- long getQtdeRegistro() 

    /**
     * Returns the value of field 'vlrRegistro'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlrRegistro'.
     */
    public java.math.BigDecimal getVlrRegistro()
    {
        return this._vlrRegistro;
    } //-- java.math.BigDecimal getVlrRegistro() 

    /**
     * Method hasCdClienteTransferenciaArquivo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdClienteTransferenciaArquivo()
    {
        return this._has_cdClienteTransferenciaArquivo;
    } //-- boolean hasCdClienteTransferenciaArquivo() 

    /**
     * Method hasCdPessoa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoa()
    {
        return this._has_cdPessoa;
    } //-- boolean hasCdPessoa() 

    /**
     * Method hasCdTipoLayoutArquivo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoLayoutArquivo()
    {
        return this._has_cdTipoLayoutArquivo;
    } //-- boolean hasCdTipoLayoutArquivo() 

    /**
     * Method hasNrSequenciaRemessa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaRemessa()
    {
        return this._has_nrSequenciaRemessa;
    } //-- boolean hasNrSequenciaRemessa() 

    /**
     * Method hasQtdeRegistro
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtdeRegistro()
    {
        return this._has_qtdeRegistro;
    } //-- boolean hasQtdeRegistro() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdClienteTransferenciaArquivo'.
     * 
     * @param cdClienteTransferenciaArquivo the value of field
     * 'cdClienteTransferenciaArquivo'.
     */
    public void setCdClienteTransferenciaArquivo(long cdClienteTransferenciaArquivo)
    {
        this._cdClienteTransferenciaArquivo = cdClienteTransferenciaArquivo;
        this._has_cdClienteTransferenciaArquivo = true;
    } //-- void setCdClienteTransferenciaArquivo(long) 

    /**
     * Sets the value of field 'cdCnpjCpf'.
     * 
     * @param cdCnpjCpf the value of field 'cdCnpjCpf'.
     */
    public void setCdCnpjCpf(java.lang.String cdCnpjCpf)
    {
        this._cdCnpjCpf = cdCnpjCpf;
    } //-- void setCdCnpjCpf(java.lang.String) 

    /**
     * Sets the value of field 'cdPessoa'.
     * 
     * @param cdPessoa the value of field 'cdPessoa'.
     */
    public void setCdPessoa(long cdPessoa)
    {
        this._cdPessoa = cdPessoa;
        this._has_cdPessoa = true;
    } //-- void setCdPessoa(long) 

    /**
     * Sets the value of field 'cdTipoLayoutArquivo'.
     * 
     * @param cdTipoLayoutArquivo the value of field
     * 'cdTipoLayoutArquivo'.
     */
    public void setCdTipoLayoutArquivo(int cdTipoLayoutArquivo)
    {
        this._cdTipoLayoutArquivo = cdTipoLayoutArquivo;
        this._has_cdTipoLayoutArquivo = true;
    } //-- void setCdTipoLayoutArquivo(int) 

    /**
     * Sets the value of field 'dsCondicaoProcessamento'.
     * 
     * @param dsCondicaoProcessamento the value of field
     * 'dsCondicaoProcessamento'.
     */
    public void setDsCondicaoProcessamento(java.lang.String dsCondicaoProcessamento)
    {
        this._dsCondicaoProcessamento = dsCondicaoProcessamento;
    } //-- void setDsCondicaoProcessamento(java.lang.String) 

    /**
     * Sets the value of field 'dsMeioTransmissao'.
     * 
     * @param dsMeioTransmissao the value of field
     * 'dsMeioTransmissao'.
     */
    public void setDsMeioTransmissao(java.lang.String dsMeioTransmissao)
    {
        this._dsMeioTransmissao = dsMeioTransmissao;
    } //-- void setDsMeioTransmissao(java.lang.String) 

    /**
     * Sets the value of field 'dsPessoa'.
     * 
     * @param dsPessoa the value of field 'dsPessoa'.
     */
    public void setDsPessoa(java.lang.String dsPessoa)
    {
        this._dsPessoa = dsPessoa;
    } //-- void setDsPessoa(java.lang.String) 

    /**
     * Sets the value of field 'dsSituacaoProcessamento'.
     * 
     * @param dsSituacaoProcessamento the value of field
     * 'dsSituacaoProcessamento'.
     */
    public void setDsSituacaoProcessamento(java.lang.String dsSituacaoProcessamento)
    {
        this._dsSituacaoProcessamento = dsSituacaoProcessamento;
    } //-- void setDsSituacaoProcessamento(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoLayoutArquivo'.
     * 
     * @param dsTipoLayoutArquivo the value of field
     * 'dsTipoLayoutArquivo'.
     */
    public void setDsTipoLayoutArquivo(java.lang.String dsTipoLayoutArquivo)
    {
        this._dsTipoLayoutArquivo = dsTipoLayoutArquivo;
    } //-- void setDsTipoLayoutArquivo(java.lang.String) 

    /**
     * Sets the value of field 'hrInclusaoRemessa'.
     * 
     * @param hrInclusaoRemessa the value of field
     * 'hrInclusaoRemessa'.
     */
    public void setHrInclusaoRemessa(java.lang.String hrInclusaoRemessa)
    {
        this._hrInclusaoRemessa = hrInclusaoRemessa;
    } //-- void setHrInclusaoRemessa(java.lang.String) 

    /**
     * Sets the value of field 'nrSequenciaRemessa'.
     * 
     * @param nrSequenciaRemessa the value of field
     * 'nrSequenciaRemessa'.
     */
    public void setNrSequenciaRemessa(long nrSequenciaRemessa)
    {
        this._nrSequenciaRemessa = nrSequenciaRemessa;
        this._has_nrSequenciaRemessa = true;
    } //-- void setNrSequenciaRemessa(long) 

    /**
     * Sets the value of field 'qtdeRegistro'.
     * 
     * @param qtdeRegistro the value of field 'qtdeRegistro'.
     */
    public void setQtdeRegistro(long qtdeRegistro)
    {
        this._qtdeRegistro = qtdeRegistro;
        this._has_qtdeRegistro = true;
    } //-- void setQtdeRegistro(long) 

    /**
     * Sets the value of field 'vlrRegistro'.
     * 
     * @param vlrRegistro the value of field 'vlrRegistro'.
     */
    public void setVlrRegistro(java.math.BigDecimal vlrRegistro)
    {
        this._vlrRegistro = vlrRegistro;
    } //-- void setVlrRegistro(java.math.BigDecimal) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.detalharsolicrecuperacao.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.detalharsolicrecuperacao.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.detalharsolicrecuperacao.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharsolicrecuperacao.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
