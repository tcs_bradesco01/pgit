/*
 * Nome: br.com.bradesco.web.pgit.service.business.manteroperacoesservrelacionado.bean

 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 10/03/2017
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.manteroperacoesservrelacionado.bean;

import static br.com.bradesco.web.pgit.utils.SiteUtil.isEmptyOrNull;
import static br.com.bradesco.web.pgit.utils.SiteUtil.not;

import java.math.BigDecimal;

import br.com.bradesco.web.pgit.utils.PgitUtil;
import br.com.bradesco.web.pgit.view.converters.FormatarData;

/**
 * Nome: DetalharHistoricoOperacaoServicoPagtoIntegradoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>
 * 	
 * @author : todo!
 * 
 * @version :
 */
public class DetalharHistoricoOperacaoServicoPagtoIntegradoSaidaDTO {

    /**
     * Atributo String
     */
    private String codMensagem = null;

    /**
     * Atributo String
     */
    private String mensagem = null;

    /**
     * Atributo Integer
     */
    private Integer cdOperacaoServicoPagamentoIntegrado = null;

    /**
     * Atributo Integer
     */
    private Integer cdNaturezaOperacaoPagamento = null;

    /**
     * Atributo String
     */
    private String dsNaturezaOperacaoPagamento = null;

    /**
     * Atributo Integer
     */
    private Integer cdTipoCondcTarifa = null;

    /**
     * Atributo String
     */
    private String dsTipoCondcTarifa = null;

    /**
     * Atributo BigDecimal
     */
    private BigDecimal vrAlcadaTarifaAgencia = null;

    /**
     * Atributo BigDecimal
     */
    private BigDecimal pcAlcadaTarifaAgencia = null;

    /**
     * Atributo String
     */
    private String cdOperCanalInclusao = null;

    /**
     * Atributo Integer
     */
    private Integer cdTipoCanalInclusao = null;

    /**
     * Atributo String
     */
    private String dsTipoCanalInclusao = null;

    /**
     * Atributo String
     */
    private String cdUsuarioInclusao = null;

    /**
     * Atributo String
     */
    private String cdUsuarioInclusaoExterno = null;

    /**
     * Atributo String
     */
    private String hrInclusaoRegistro = null;

    /**
     * Atributo String
     */
    private String cdOperacaoCanalManutencao = null;

    /**
     * Atributo Integer
     */
    private Integer cdTipoCanalManutencao = null;

    /**
     * Atributo String
     */
    private String dsTipoCanalManutencao = null;

    /**
     * Atributo String
     */
    private String cdUsuarioManutencao = null;

    /**
     * Atributo String
     */
    private String cdUsuarioManutencaoExterno = null;

    /**
     * Atributo String
     */
    private String hrManutencaoRegistro = null;

    /**
     * Nome: getCodMensagem
     * 
     * @return codMensagem
     */
    public String getCodMensagem() {
        return codMensagem;
    }

    /**
     * Nome: setCodMensagem
     * 
     * @param codMensagem
     */
    public void setCodMensagem(String codMensagem) {
        this.codMensagem = codMensagem;
    }

    /**
     * Nome: getMensagem
     * 
     * @return mensagem
     */
    public String getMensagem() {
        return mensagem;
    }

    /**
     * Nome: setMensagem
     * 
     * @param mensagem
     */
    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }

    /**
     * Nome: getCdOperacaoServicoPagamentoIntegrado
     * 
     * @return cdOperacaoServicoPagamentoIntegrado
     */
    public Integer getCdOperacaoServicoPagamentoIntegrado() {
        return cdOperacaoServicoPagamentoIntegrado;
    }

    /**
     * Nome: setCdOperacaoServicoPagamentoIntegrado
     * 
     * @param cdOperacaoServicoPagamentoIntegrado
     */
    public void setCdOperacaoServicoPagamentoIntegrado(Integer cdOperacaoServicoPagamentoIntegrado) {
        this.cdOperacaoServicoPagamentoIntegrado = cdOperacaoServicoPagamentoIntegrado;
    }

    /**
     * Nome: getCdNaturezaOperacaoPagamento
     * 
     * @return cdNaturezaOperacaoPagamento
     */
    public Integer getCdNaturezaOperacaoPagamento() {
        return cdNaturezaOperacaoPagamento;
    }

    /**
     * Nome: setCdNaturezaOperacaoPagamento
     * 
     * @param cdNaturezaOperacaoPagamento
     */
    public void setCdNaturezaOperacaoPagamento(Integer cdNaturezaOperacaoPagamento) {
        this.cdNaturezaOperacaoPagamento = cdNaturezaOperacaoPagamento;
    }

    /**
     * Nome: getDsNaturezaOperacaoPagamento
     * 
     * @return dsNaturezaOperacaoPagamento
     */
    public String getDsNaturezaOperacaoPagamento() {
        return dsNaturezaOperacaoPagamento;
    }

    /**
     * Nome: setDsNaturezaOperacaoPagamento
     * 
     * @param dsNaturezaOperacaoPagamento
     */
    public void setDsNaturezaOperacaoPagamento(String dsNaturezaOperacaoPagamento) {
        this.dsNaturezaOperacaoPagamento = dsNaturezaOperacaoPagamento;
    }

    /**
     * Nome: getCdTipoCondcTarifa
     * 
     * @return cdTipoCondcTarifa
     */
    public Integer getCdTipoCondcTarifa() {
        return cdTipoCondcTarifa;
    }

    /**
     * Nome: setCdTipoCondcTarifa
     * 
     * @param cdTipoCondcTarifa
     */
    public void setCdTipoCondcTarifa(Integer cdTipoCondcTarifa) {
        this.cdTipoCondcTarifa = cdTipoCondcTarifa;
    }

    /**
     * Nome: getDsTipoCondcTarifa
     * 
     * @return dsTipoCondcTarifa
     */
    public String getDsTipoCondcTarifa() {
        return dsTipoCondcTarifa;
    }

    /**
     * Nome: setDsTipoCondcTarifa
     * 
     * @param dsTipoCondcTarifa
     */
    public void setDsTipoCondcTarifa(String dsTipoCondcTarifa) {
        this.dsTipoCondcTarifa = dsTipoCondcTarifa;
    }

    /**
     * Nome: getVrAlcadaTarifaAgencia
     * 
     * @return vrAlcadaTarifaAgencia
     */
    public BigDecimal getVrAlcadaTarifaAgencia() {
        return vrAlcadaTarifaAgencia;
    }

    /**
     * Nome: setVrAlcadaTarifaAgencia
     * 
     * @param vrAlcadaTarifaAgencia
     */
    public void setVrAlcadaTarifaAgencia(BigDecimal vrAlcadaTarifaAgencia) {
        this.vrAlcadaTarifaAgencia = vrAlcadaTarifaAgencia;
    }

    /**
     * Nome: getPcAlcadaTarifaAgencia
     * 
     * @return pcAlcadaTarifaAgencia
     */
    public BigDecimal getPcAlcadaTarifaAgencia() {
        return pcAlcadaTarifaAgencia;
    }

    /**
     * Nome: setPcAlcadaTarifaAgencia
     * 
     * @param pcAlcadaTarifaAgencia
     */
    public void setPcAlcadaTarifaAgencia(BigDecimal pcAlcadaTarifaAgencia) {
        this.pcAlcadaTarifaAgencia = pcAlcadaTarifaAgencia;
    }

    /**
     * Nome: getCdOperCanalInclusao
     * 
     * @return cdOperCanalInclusao
     */
    public String getCdOperCanalInclusao() {
        return cdOperCanalInclusao;
    }

    /**
     * Nome: setCdOperCanalInclusao
     * 
     * @param cdOperCanalInclusao
     */
    public void setCdOperCanalInclusao(String cdOperCanalInclusao) {
        this.cdOperCanalInclusao = cdOperCanalInclusao;
    }

    /**
     * Nome: getCdTipoCanalInclusao
     * 
     * @return cdTipoCanalInclusao
     */
    public Integer getCdTipoCanalInclusao() {
        return cdTipoCanalInclusao;
    }

    /**
     * Nome: setCdTipoCanalInclusao
     * 
     * @param cdTipoCanalInclusao
     */
    public void setCdTipoCanalInclusao(Integer cdTipoCanalInclusao) {
        this.cdTipoCanalInclusao = cdTipoCanalInclusao;
    }

    /**
     * Nome: getDsTipoCanalInclusao
     * 
     * @return dsTipoCanalInclusao
     */
    public String getDsTipoCanalInclusao() {
        return dsTipoCanalInclusao;
    }

    /**
     * Nome: setDsTipoCanalInclusao
     * 
     * @param dsTipoCanalInclusao
     */
    public void setDsTipoCanalInclusao(String dsTipoCanalInclusao) {
        this.dsTipoCanalInclusao = dsTipoCanalInclusao;
    }

    /**
     * Nome: getCdUsuarioInclusao
     * 
     * @return cdUsuarioInclusao
     */
    public String getCdUsuarioInclusao() {
        return cdUsuarioInclusao;
    }

    /**
     * Nome: setCdUsuarioInclusao
     * 
     * @param cdUsuarioInclusao
     */
    public void setCdUsuarioInclusao(String cdUsuarioInclusao) {
        this.cdUsuarioInclusao = cdUsuarioInclusao;
    }

    /**
     * Nome: getCdUsuarioInclusaoExterno
     * 
     * @return cdUsuarioInclusaoExterno
     */
    public String getCdUsuarioInclusaoExterno() {
        return cdUsuarioInclusaoExterno;
    }

    /**
     * Nome: setCdUsuarioInclusaoExterno
     * 
     * @param cdUsuarioInclusaoExterno
     */
    public void setCdUsuarioInclusaoExterno(String cdUsuarioInclusaoExterno) {
        this.cdUsuarioInclusaoExterno = cdUsuarioInclusaoExterno;
    }

    /**
     * Nome: getHrInclusaoRegistro
     * 
     * @return hrInclusaoRegistro
     */
    public String getHrInclusaoRegistro() {
        return hrInclusaoRegistro;
    }

    /**
     * Nome: setHrInclusaoRegistro
     * 
     * @param hrInclusaoRegistro
     */
    public void setHrInclusaoRegistro(String hrInclusaoRegistro) {
        this.hrInclusaoRegistro = hrInclusaoRegistro;
    }

    /**
     * Nome: getCdOperacaoCanalManutencao
     * 
     * @return cdOperacaoCanalManutencao
     */
    public String getCdOperacaoCanalManutencao() {
        return cdOperacaoCanalManutencao;
    }

    /**
     * Nome: setCdOperacaoCanalManutencao
     * 
     * @param cdOperacaoCanalManutencao
     */
    public void setCdOperacaoCanalManutencao(String cdOperacaoCanalManutencao) {
        this.cdOperacaoCanalManutencao = cdOperacaoCanalManutencao;
    }

    /**
     * Nome: getCdTipoCanalManutencao
     * 
     * @return cdTipoCanalManutencao
     */
    public Integer getCdTipoCanalManutencao() {
        return cdTipoCanalManutencao;
    }

    /**
     * Nome: setCdTipoCanalManutencao
     * 
     * @param cdTipoCanalManutencao
     */
    public void setCdTipoCanalManutencao(Integer cdTipoCanalManutencao) {
        this.cdTipoCanalManutencao = cdTipoCanalManutencao;
    }

    /**
     * Nome: getDsTipoCanalManutencao
     * 
     * @return dsTipoCanalManutencao
     */
    public String getDsTipoCanalManutencao() {
        return dsTipoCanalManutencao;
    }

    /**
     * Nome: setDsTipoCanalManutencao
     * 
     * @param dsTipoCanalManutencao
     */
    public void setDsTipoCanalManutencao(String dsTipoCanalManutencao) {
        this.dsTipoCanalManutencao = dsTipoCanalManutencao;
    }

    /**
     * Nome: getCdUsuarioManutencao
     * 
     * @return cdUsuarioManutencao
     */
    public String getCdUsuarioManutencao() {
        return cdUsuarioManutencao;
    }

    /**
     * Nome: setCdUsuarioManutencao
     * 
     * @param cdUsuarioManutencao
     */
    public void setCdUsuarioManutencao(String cdUsuarioManutencao) {
        this.cdUsuarioManutencao = cdUsuarioManutencao;
    }

    /**
     * Nome: getCdUsuarioManutencaoExterno
     * 
     * @return cdUsuarioManutencaoExterno
     */
    public String getCdUsuarioManutencaoExterno() {
        return cdUsuarioManutencaoExterno;
    }

    /**
     * Nome: setCdUsuarioManutencaoExterno
     * 
     * @param cdUsuarioManutencaoExterno
     */
    public void setCdUsuarioManutencaoExterno(String cdUsuarioManutencaoExterno) {
        this.cdUsuarioManutencaoExterno = cdUsuarioManutencaoExterno;
    }

    /**
     * Nome: getHrManutencaoRegistro
     * 
     * @return hrManutencaoRegistro
     */
    public String getHrManutencaoRegistro() {
        return hrManutencaoRegistro;
    }

    /**
     * Nome: setHrManutencaoRegistro
     * 
     * @param hrManutencaoRegistro
     */
    public void setHrManutencaoRegistro(String hrManutencaoRegistro) {
        this.hrManutencaoRegistro = hrManutencaoRegistro;
    }
    
    /**
     * Nome: getPercentualAlcadaTarifaAgenciaFormatado
     * 
     * @return
     */
    public String getPercentualAlcadaTarifaAgenciaFormatado(){
        return PgitUtil.formatNumberPercent(getPcAlcadaTarifaAgencia());
    }
    
    /**
     * Nome: getDataHoraManutencaoRegistroFormatada
     * 
     * @return
     */
    public String getDataHoraManutencaoRegistroFormatada() {
        return FormatarData.formatarDataTrilha(getHrManutencaoRegistro());
    }
    
    /**
     * Nome: getDataHoraInclusaoRegistroFormatada
     * 
     * @return
     */
    public String getDataHoraInclusaoRegistroFormatada() {
        return FormatarData.formatarDataTrilha(getHrInclusaoRegistro());
    }
    
    /**
     * Nome: getUsuarioInclusao
     * 
     * @return
     */
    public String getUsuarioInclusao() {
        if(getCdUsuarioInclusao().equals("")){
            return getCdUsuarioInclusaoExterno();
        }
        
        return getCdUsuarioInclusao();
    }
    
    /**
     * Nome: getUsuarioManutencao
     * 
     * @return
     */
    public String getUsuarioManutencao() {
        if(getCdUsuarioManutencao().equals("")){
            return getCdUsuarioManutencaoExterno();
        }
        
        return getCdUsuarioManutencao();
    }
    
    /**
     * Nome: getTipoCanalInclusaoFormatado
     * 
     * @return
     */
    public String getTipoCanalInclusaoFormatado(){
        if(not(isEmptyOrNull(getDsTipoCanalInclusao()))){
            return getCdTipoCanalInclusao() + " - " + getDsTipoCanalInclusao();
        }
        
        return "";
    }
    
    /**
     * Nome: getTipoCanalIManutencaoFormatado
     * 
     * @return
     */
    public String getTipoCanalManutencaoFormatado(){
        if(not(isEmptyOrNull(getDsTipoCanalManutencao()))){
            return getCdTipoCanalManutencao() + " - " + getDsTipoCanalManutencao();
        }
        
        return "";
    }
}
