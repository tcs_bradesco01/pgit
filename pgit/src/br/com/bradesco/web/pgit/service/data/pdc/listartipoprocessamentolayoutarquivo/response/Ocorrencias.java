/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listartipoprocessamentolayoutarquivo.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdCombo
     */
    private int _cdCombo = 0;

    /**
     * keeps track of state for field: _cdCombo
     */
    private boolean _has_cdCombo;

    /**
     * Field _dsCombo
     */
    private java.lang.String _dsCombo;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listartipoprocessamentolayoutarquivo.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdCombo
     * 
     */
    public void deleteCdCombo()
    {
        this._has_cdCombo= false;
    } //-- void deleteCdCombo() 

    /**
     * Returns the value of field 'cdCombo'.
     * 
     * @return int
     * @return the value of field 'cdCombo'.
     */
    public int getCdCombo()
    {
        return this._cdCombo;
    } //-- int getCdCombo() 

    /**
     * Returns the value of field 'dsCombo'.
     * 
     * @return String
     * @return the value of field 'dsCombo'.
     */
    public java.lang.String getDsCombo()
    {
        return this._dsCombo;
    } //-- java.lang.String getDsCombo() 

    /**
     * Method hasCdCombo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCombo()
    {
        return this._has_cdCombo;
    } //-- boolean hasCdCombo() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdCombo'.
     * 
     * @param cdCombo the value of field 'cdCombo'.
     */
    public void setCdCombo(int cdCombo)
    {
        this._cdCombo = cdCombo;
        this._has_cdCombo = true;
    } //-- void setCdCombo(int) 

    /**
     * Sets the value of field 'dsCombo'.
     * 
     * @param dsCombo the value of field 'dsCombo'.
     */
    public void setDsCombo(java.lang.String dsCombo)
    {
        this._dsCombo = dsCombo;
    } //-- void setDsCombo(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listartipoprocessamentolayoutarquivo.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listartipoprocessamentolayoutarquivo.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listartipoprocessamentolayoutarquivo.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listartipoprocessamentolayoutarquivo.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
