/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultardadosbasicocontrato.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.math.BigDecimal;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ConsultarDadosBasicoContratoResponse.
 * 
 * @version $Revision$ $Date$
 */
public class ConsultarDadosBasicoContratoResponse implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _codMensagem
     */
    private java.lang.String _codMensagem;

    /**
     * Field _mensagem
     */
    private java.lang.String _mensagem;

    /**
     * Field _cdGrupoEconomicoMaster
     */
    private long _cdGrupoEconomicoMaster = 0;

    /**
     * keeps track of state for field: _cdGrupoEconomicoMaster
     */
    private boolean _has_cdGrupoEconomicoMaster;

    /**
     * Field _dsGrupoEconomicoMaster
     */
    private java.lang.String _dsGrupoEconomicoMaster;

    /**
     * Field _cdAtividadeEconomicaMaster
     */
    private int _cdAtividadeEconomicaMaster = 0;

    /**
     * keeps track of state for field: _cdAtividadeEconomicaMaster
     */
    private boolean _has_cdAtividadeEconomicaMaster;

    /**
     * Field _dsAtividadeEconomicaMaster
     */
    private java.lang.String _dsAtividadeEconomicaMaster;

    /**
     * Field _cdSegmentoParticipanteMaster
     */
    private int _cdSegmentoParticipanteMaster = 0;

    /**
     * keeps track of state for field: _cdSegmentoParticipanteMaster
     */
    private boolean _has_cdSegmentoParticipanteMaster;

    /**
     * Field _dsSegmentoParticipanteMaster
     */
    private java.lang.String _dsSegmentoParticipanteMaster;

    /**
     * Field _cdSubSegmentoMaster
     */
    private int _cdSubSegmentoMaster = 0;

    /**
     * keeps track of state for field: _cdSubSegmentoMaster
     */
    private boolean _has_cdSubSegmentoMaster;

    /**
     * Field _dsSubSegmentoMaster
     */
    private java.lang.String _dsSubSegmentoMaster;

    /**
     * Field _dsContratoNegocio
     */
    private java.lang.String _dsContratoNegocio;

    /**
     * Field _cdIdioma
     */
    private int _cdIdioma = 0;

    /**
     * keeps track of state for field: _cdIdioma
     */
    private boolean _has_cdIdioma;

    /**
     * Field _dsIdioma
     */
    private java.lang.String _dsIdioma;

    /**
     * Field _cdMoeda
     */
    private int _cdMoeda = 0;

    /**
     * keeps track of state for field: _cdMoeda
     */
    private boolean _has_cdMoeda;

    /**
     * Field _dsMoeda
     */
    private java.lang.String _dsMoeda;

    /**
     * Field _cdOrigem
     */
    private int _cdOrigem = 0;

    /**
     * keeps track of state for field: _cdOrigem
     */
    private boolean _has_cdOrigem;

    /**
     * Field _deOrigem
     */
    private java.lang.String _deOrigem;

    /**
     * Field _indicadorAditivo
     */
    private java.lang.String _indicadorAditivo;

    /**
     * Field _cdSituacaoContrato
     */
    private int _cdSituacaoContrato = 0;

    /**
     * keeps track of state for field: _cdSituacaoContrato
     */
    private boolean _has_cdSituacaoContrato;

    /**
     * Field _dsSituacaoContrato
     */
    private java.lang.String _dsSituacaoContrato;

    /**
     * Field _cdSituacaoMotivoContrato
     */
    private int _cdSituacaoMotivoContrato = 0;

    /**
     * keeps track of state for field: _cdSituacaoMotivoContrato
     */
    private boolean _has_cdSituacaoMotivoContrato;

    /**
     * Field _dsSituacaoMotivoContrato
     */
    private java.lang.String _dsSituacaoMotivoContrato;

    /**
     * Field _cdSetorContrato
     */
    private int _cdSetorContrato = 0;

    /**
     * keeps track of state for field: _cdSetorContrato
     */
    private boolean _has_cdSetorContrato;

    /**
     * Field _dsSetorContrato
     */
    private java.lang.String _dsSetorContrato;

    /**
     * Field _dtCadastroContrato
     */
    private java.lang.String _dtCadastroContrato;

    /**
     * Field _hrCadastroContrato
     */
    private java.lang.String _hrCadastroContrato;

    /**
     * Field _dtVigenciaContrato
     */
    private java.lang.String _dtVigenciaContrato;

    /**
     * Field _hrVigenciaContrato
     */
    private java.lang.String _hrVigenciaContrato;

    /**
     * Field _nrComercialContrato
     */
    private java.lang.String _nrComercialContrato;

    /**
     * Field _cdAcaoRelacionamento
     */
    private long _cdAcaoRelacionamento = 0;

    /**
     * keeps track of state for field: _cdAcaoRelacionamento
     */
    private boolean _has_cdAcaoRelacionamento;

    /**
     * Field _tipoParticipacaoContrato
     */
    private java.lang.String _tipoParticipacaoContrato;

    /**
     * Field _dtAssinaturaContratoNegocio
     */
    private java.lang.String _dtAssinaturaContratoNegocio;

    /**
     * Field _hrAssinaturaContratoNegocio
     */
    private java.lang.String _hrAssinaturaContratoNegocio;

    /**
     * Field _cdIndicadorEncerramentoContrato
     */
    private int _cdIndicadorEncerramentoContrato = 0;

    /**
     * keeps track of state for field:
     * _cdIndicadorEncerramentoContrato
     */
    private boolean _has_cdIndicadorEncerramentoContrato;

    /**
     * Field _dsindicadorEncerramentoContrato
     */
    private java.lang.String _dsindicadorEncerramentoContrato;

    /**
     * Field _cdUnidadeOperacionalContrato
     */
    private int _cdUnidadeOperacionalContrato = 0;

    /**
     * keeps track of state for field: _cdUnidadeOperacionalContrato
     */
    private boolean _has_cdUnidadeOperacionalContrato;

    /**
     * Field _nrUnidadeOperacionalContrato
     */
    private java.lang.String _nrUnidadeOperacionalContrato;

    /**
     * Field _cdUnidadeContabilContrato
     */
    private int _cdUnidadeContabilContrato = 0;

    /**
     * keeps track of state for field: _cdUnidadeContabilContrato
     */
    private boolean _has_cdUnidadeContabilContrato;

    /**
     * Field _nrUnidadeContabilContrato
     */
    private java.lang.String _nrUnidadeContabilContrato;

    /**
     * Field _cdUnidadeGestoraContrato
     */
    private int _cdUnidadeGestoraContrato = 0;

    /**
     * keeps track of state for field: _cdUnidadeGestoraContrato
     */
    private boolean _has_cdUnidadeGestoraContrato;

    /**
     * Field _nrUnidadeGestoraContrato
     */
    private java.lang.String _nrUnidadeGestoraContrato;

    /**
     * Field _cdFuncionarioBradesco
     */
    private long _cdFuncionarioBradesco = 0;

    /**
     * keeps track of state for field: _cdFuncionarioBradesco
     */
    private boolean _has_cdFuncionarioBradesco;

    /**
     * Field _nomeFuncionarioBradesco
     */
    private java.lang.String _nomeFuncionarioBradesco;

    /**
     * Field _cdPessoaJuridica
     */
    private long _cdPessoaJuridica = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridica
     */
    private boolean _has_cdPessoaJuridica;

    /**
     * Field _nrSequenciaUnidadeOrganizacional
     */
    private int _nrSequenciaUnidadeOrganizacional = 0;

    /**
     * keeps track of state for field:
     * _nrSequenciaUnidadeOrganizacional
     */
    private boolean _has_nrSequenciaUnidadeOrganizacional;

    /**
     * Field _cdPessoaJuridicaProposta
     */
    private long _cdPessoaJuridicaProposta = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridicaProposta
     */
    private boolean _has_cdPessoaJuridicaProposta;

    /**
     * Field _cdTipoContratoProposta
     */
    private int _cdTipoContratoProposta = 0;

    /**
     * keeps track of state for field: _cdTipoContratoProposta
     */
    private boolean _has_cdTipoContratoProposta;

    /**
     * Field _nrContratoProposta
     */
    private long _nrContratoProposta = 0;

    /**
     * keeps track of state for field: _nrContratoProposta
     */
    private boolean _has_nrContratoProposta;

    /**
     * Field _dsPessoaJuridicaProposta
     */
    private java.lang.String _dsPessoaJuridicaProposta;

    /**
     * Field _dsTipoContratoProposta
     */
    private java.lang.String _dsTipoContratoProposta;

    /**
     * Field _vlSaldoVirtualTeste
     */
    private java.math.BigDecimal _vlSaldoVirtualTeste = new java.math.BigDecimal("0");

    /**
     * Field _cdPropostaJuridicaPagamentoSalario
     */
    private long _cdPropostaJuridicaPagamentoSalario = 0;

    /**
     * keeps track of state for field:
     * _cdPropostaJuridicaPagamentoSalario
     */
    private boolean _has_cdPropostaJuridicaPagamentoSalario;

    /**
     * Field _cdTipoContratoPagamentoSalario
     */
    private int _cdTipoContratoPagamentoSalario = 0;

    /**
     * keeps track of state for field:
     * _cdTipoContratoPagamentoSalario
     */
    private boolean _has_cdTipoContratoPagamentoSalario;

    /**
     * Field _nrSequenciaContratoPagamentoSalario
     */
    private long _nrSequenciaContratoPagamentoSalario = 0;

    /**
     * keeps track of state for field:
     * _nrSequenciaContratoPagamentoSalario
     */
    private boolean _has_nrSequenciaContratoPagamentoSalario;

    /**
     * Field _cdPropostaJuridicaOutros
     */
    private long _cdPropostaJuridicaOutros = 0;

    /**
     * keeps track of state for field: _cdPropostaJuridicaOutros
     */
    private boolean _has_cdPropostaJuridicaOutros;

    /**
     * Field _cdTipoContratoOutros
     */
    private int _cdTipoContratoOutros = 0;

    /**
     * keeps track of state for field: _cdTipoContratoOutros
     */
    private boolean _has_cdTipoContratoOutros;

    /**
     * Field _nrSequenciaContratoOutros
     */
    private long _nrSequenciaContratoOutros = 0;

    /**
     * keeps track of state for field: _nrSequenciaContratoOutros
     */
    private boolean _has_nrSequenciaContratoOutros;

    /**
     * Field _cdFormaAutorizacaoPagamento
     */
    private int _cdFormaAutorizacaoPagamento = 0;

    /**
     * keeps track of state for field: _cdFormaAutorizacaoPagamento
     */
    private boolean _has_cdFormaAutorizacaoPagamento;

    /**
     * Field _dsFormaAutorizacaoPagamento
     */
    private java.lang.String _dsFormaAutorizacaoPagamento;

    /**
     * Field _dtInclusao
     */
    private java.lang.String _dtInclusao;

    /**
     * Field _hrInclusao
     */
    private java.lang.String _hrInclusao;

    /**
     * Field _cdUsuarioInclusao
     */
    private java.lang.String _cdUsuarioInclusao;

    /**
     * Field _cdTipoCanalInclusao
     */
    private int _cdTipoCanalInclusao = 0;

    /**
     * keeps track of state for field: _cdTipoCanalInclusao
     */
    private boolean _has_cdTipoCanalInclusao;

    /**
     * Field _dsTipoCanalInclusao
     */
    private java.lang.String _dsTipoCanalInclusao;

    /**
     * Field _operacaoFluxoInclusao
     */
    private java.lang.String _operacaoFluxoInclusao;

    /**
     * Field _dtManutencao
     */
    private java.lang.String _dtManutencao;

    /**
     * Field _hrManutencao
     */
    private java.lang.String _hrManutencao;

    /**
     * Field _cdUsuarioManutencao
     */
    private java.lang.String _cdUsuarioManutencao;

    /**
     * Field _cdTipoCanalManutencao
     */
    private int _cdTipoCanalManutencao = 0;

    /**
     * keeps track of state for field: _cdTipoCanalManutencao
     */
    private boolean _has_cdTipoCanalManutencao;

    /**
     * Field _dsTipoCanalManutencao
     */
    private java.lang.String _dsTipoCanalManutencao;

    /**
     * Field _operacaoFluxoManutencao
     */
    private java.lang.String _operacaoFluxoManutencao;


      //----------------/
     //- Constructors -/
    //----------------/

    public ConsultarDadosBasicoContratoResponse() 
     {
        super();
        setVlSaldoVirtualTeste(new java.math.BigDecimal("0"));
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultardadosbasicocontrato.response.ConsultarDadosBasicoContratoResponse()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdAcaoRelacionamento
     * 
     */
    public void deleteCdAcaoRelacionamento()
    {
        this._has_cdAcaoRelacionamento= false;
    } //-- void deleteCdAcaoRelacionamento() 

    /**
     * Method deleteCdAtividadeEconomicaMaster
     * 
     */
    public void deleteCdAtividadeEconomicaMaster()
    {
        this._has_cdAtividadeEconomicaMaster= false;
    } //-- void deleteCdAtividadeEconomicaMaster() 

    /**
     * Method deleteCdFormaAutorizacaoPagamento
     * 
     */
    public void deleteCdFormaAutorizacaoPagamento()
    {
        this._has_cdFormaAutorizacaoPagamento= false;
    } //-- void deleteCdFormaAutorizacaoPagamento() 

    /**
     * Method deleteCdFuncionarioBradesco
     * 
     */
    public void deleteCdFuncionarioBradesco()
    {
        this._has_cdFuncionarioBradesco= false;
    } //-- void deleteCdFuncionarioBradesco() 

    /**
     * Method deleteCdGrupoEconomicoMaster
     * 
     */
    public void deleteCdGrupoEconomicoMaster()
    {
        this._has_cdGrupoEconomicoMaster= false;
    } //-- void deleteCdGrupoEconomicoMaster() 

    /**
     * Method deleteCdIdioma
     * 
     */
    public void deleteCdIdioma()
    {
        this._has_cdIdioma= false;
    } //-- void deleteCdIdioma() 

    /**
     * Method deleteCdIndicadorEncerramentoContrato
     * 
     */
    public void deleteCdIndicadorEncerramentoContrato()
    {
        this._has_cdIndicadorEncerramentoContrato= false;
    } //-- void deleteCdIndicadorEncerramentoContrato() 

    /**
     * Method deleteCdMoeda
     * 
     */
    public void deleteCdMoeda()
    {
        this._has_cdMoeda= false;
    } //-- void deleteCdMoeda() 

    /**
     * Method deleteCdOrigem
     * 
     */
    public void deleteCdOrigem()
    {
        this._has_cdOrigem= false;
    } //-- void deleteCdOrigem() 

    /**
     * Method deleteCdPessoaJuridica
     * 
     */
    public void deleteCdPessoaJuridica()
    {
        this._has_cdPessoaJuridica= false;
    } //-- void deleteCdPessoaJuridica() 

    /**
     * Method deleteCdPessoaJuridicaProposta
     * 
     */
    public void deleteCdPessoaJuridicaProposta()
    {
        this._has_cdPessoaJuridicaProposta= false;
    } //-- void deleteCdPessoaJuridicaProposta() 

    /**
     * Method deleteCdPropostaJuridicaOutros
     * 
     */
    public void deleteCdPropostaJuridicaOutros()
    {
        this._has_cdPropostaJuridicaOutros= false;
    } //-- void deleteCdPropostaJuridicaOutros() 

    /**
     * Method deleteCdPropostaJuridicaPagamentoSalario
     * 
     */
    public void deleteCdPropostaJuridicaPagamentoSalario()
    {
        this._has_cdPropostaJuridicaPagamentoSalario= false;
    } //-- void deleteCdPropostaJuridicaPagamentoSalario() 

    /**
     * Method deleteCdSegmentoParticipanteMaster
     * 
     */
    public void deleteCdSegmentoParticipanteMaster()
    {
        this._has_cdSegmentoParticipanteMaster= false;
    } //-- void deleteCdSegmentoParticipanteMaster() 

    /**
     * Method deleteCdSetorContrato
     * 
     */
    public void deleteCdSetorContrato()
    {
        this._has_cdSetorContrato= false;
    } //-- void deleteCdSetorContrato() 

    /**
     * Method deleteCdSituacaoContrato
     * 
     */
    public void deleteCdSituacaoContrato()
    {
        this._has_cdSituacaoContrato= false;
    } //-- void deleteCdSituacaoContrato() 

    /**
     * Method deleteCdSituacaoMotivoContrato
     * 
     */
    public void deleteCdSituacaoMotivoContrato()
    {
        this._has_cdSituacaoMotivoContrato= false;
    } //-- void deleteCdSituacaoMotivoContrato() 

    /**
     * Method deleteCdSubSegmentoMaster
     * 
     */
    public void deleteCdSubSegmentoMaster()
    {
        this._has_cdSubSegmentoMaster= false;
    } //-- void deleteCdSubSegmentoMaster() 

    /**
     * Method deleteCdTipoCanalInclusao
     * 
     */
    public void deleteCdTipoCanalInclusao()
    {
        this._has_cdTipoCanalInclusao= false;
    } //-- void deleteCdTipoCanalInclusao() 

    /**
     * Method deleteCdTipoCanalManutencao
     * 
     */
    public void deleteCdTipoCanalManutencao()
    {
        this._has_cdTipoCanalManutencao= false;
    } //-- void deleteCdTipoCanalManutencao() 

    /**
     * Method deleteCdTipoContratoOutros
     * 
     */
    public void deleteCdTipoContratoOutros()
    {
        this._has_cdTipoContratoOutros= false;
    } //-- void deleteCdTipoContratoOutros() 

    /**
     * Method deleteCdTipoContratoPagamentoSalario
     * 
     */
    public void deleteCdTipoContratoPagamentoSalario()
    {
        this._has_cdTipoContratoPagamentoSalario= false;
    } //-- void deleteCdTipoContratoPagamentoSalario() 

    /**
     * Method deleteCdTipoContratoProposta
     * 
     */
    public void deleteCdTipoContratoProposta()
    {
        this._has_cdTipoContratoProposta= false;
    } //-- void deleteCdTipoContratoProposta() 

    /**
     * Method deleteCdUnidadeContabilContrato
     * 
     */
    public void deleteCdUnidadeContabilContrato()
    {
        this._has_cdUnidadeContabilContrato= false;
    } //-- void deleteCdUnidadeContabilContrato() 

    /**
     * Method deleteCdUnidadeGestoraContrato
     * 
     */
    public void deleteCdUnidadeGestoraContrato()
    {
        this._has_cdUnidadeGestoraContrato= false;
    } //-- void deleteCdUnidadeGestoraContrato() 

    /**
     * Method deleteCdUnidadeOperacionalContrato
     * 
     */
    public void deleteCdUnidadeOperacionalContrato()
    {
        this._has_cdUnidadeOperacionalContrato= false;
    } //-- void deleteCdUnidadeOperacionalContrato() 

    /**
     * Method deleteNrContratoProposta
     * 
     */
    public void deleteNrContratoProposta()
    {
        this._has_nrContratoProposta= false;
    } //-- void deleteNrContratoProposta() 

    /**
     * Method deleteNrSequenciaContratoOutros
     * 
     */
    public void deleteNrSequenciaContratoOutros()
    {
        this._has_nrSequenciaContratoOutros= false;
    } //-- void deleteNrSequenciaContratoOutros() 

    /**
     * Method deleteNrSequenciaContratoPagamentoSalario
     * 
     */
    public void deleteNrSequenciaContratoPagamentoSalario()
    {
        this._has_nrSequenciaContratoPagamentoSalario= false;
    } //-- void deleteNrSequenciaContratoPagamentoSalario() 

    /**
     * Method deleteNrSequenciaUnidadeOrganizacional
     * 
     */
    public void deleteNrSequenciaUnidadeOrganizacional()
    {
        this._has_nrSequenciaUnidadeOrganizacional= false;
    } //-- void deleteNrSequenciaUnidadeOrganizacional() 

    /**
     * Returns the value of field 'cdAcaoRelacionamento'.
     * 
     * @return long
     * @return the value of field 'cdAcaoRelacionamento'.
     */
    public long getCdAcaoRelacionamento()
    {
        return this._cdAcaoRelacionamento;
    } //-- long getCdAcaoRelacionamento() 

    /**
     * Returns the value of field 'cdAtividadeEconomicaMaster'.
     * 
     * @return int
     * @return the value of field 'cdAtividadeEconomicaMaster'.
     */
    public int getCdAtividadeEconomicaMaster()
    {
        return this._cdAtividadeEconomicaMaster;
    } //-- int getCdAtividadeEconomicaMaster() 

    /**
     * Returns the value of field 'cdFormaAutorizacaoPagamento'.
     * 
     * @return int
     * @return the value of field 'cdFormaAutorizacaoPagamento'.
     */
    public int getCdFormaAutorizacaoPagamento()
    {
        return this._cdFormaAutorizacaoPagamento;
    } //-- int getCdFormaAutorizacaoPagamento() 

    /**
     * Returns the value of field 'cdFuncionarioBradesco'.
     * 
     * @return long
     * @return the value of field 'cdFuncionarioBradesco'.
     */
    public long getCdFuncionarioBradesco()
    {
        return this._cdFuncionarioBradesco;
    } //-- long getCdFuncionarioBradesco() 

    /**
     * Returns the value of field 'cdGrupoEconomicoMaster'.
     * 
     * @return long
     * @return the value of field 'cdGrupoEconomicoMaster'.
     */
    public long getCdGrupoEconomicoMaster()
    {
        return this._cdGrupoEconomicoMaster;
    } //-- long getCdGrupoEconomicoMaster() 

    /**
     * Returns the value of field 'cdIdioma'.
     * 
     * @return int
     * @return the value of field 'cdIdioma'.
     */
    public int getCdIdioma()
    {
        return this._cdIdioma;
    } //-- int getCdIdioma() 

    /**
     * Returns the value of field
     * 'cdIndicadorEncerramentoContrato'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorEncerramentoContrato'.
     */
    public int getCdIndicadorEncerramentoContrato()
    {
        return this._cdIndicadorEncerramentoContrato;
    } //-- int getCdIndicadorEncerramentoContrato() 

    /**
     * Returns the value of field 'cdMoeda'.
     * 
     * @return int
     * @return the value of field 'cdMoeda'.
     */
    public int getCdMoeda()
    {
        return this._cdMoeda;
    } //-- int getCdMoeda() 

    /**
     * Returns the value of field 'cdOrigem'.
     * 
     * @return int
     * @return the value of field 'cdOrigem'.
     */
    public int getCdOrigem()
    {
        return this._cdOrigem;
    } //-- int getCdOrigem() 

    /**
     * Returns the value of field 'cdPessoaJuridica'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridica'.
     */
    public long getCdPessoaJuridica()
    {
        return this._cdPessoaJuridica;
    } //-- long getCdPessoaJuridica() 

    /**
     * Returns the value of field 'cdPessoaJuridicaProposta'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridicaProposta'.
     */
    public long getCdPessoaJuridicaProposta()
    {
        return this._cdPessoaJuridicaProposta;
    } //-- long getCdPessoaJuridicaProposta() 

    /**
     * Returns the value of field 'cdPropostaJuridicaOutros'.
     * 
     * @return long
     * @return the value of field 'cdPropostaJuridicaOutros'.
     */
    public long getCdPropostaJuridicaOutros()
    {
        return this._cdPropostaJuridicaOutros;
    } //-- long getCdPropostaJuridicaOutros() 

    /**
     * Returns the value of field
     * 'cdPropostaJuridicaPagamentoSalario'.
     * 
     * @return long
     * @return the value of field
     * 'cdPropostaJuridicaPagamentoSalario'.
     */
    public long getCdPropostaJuridicaPagamentoSalario()
    {
        return this._cdPropostaJuridicaPagamentoSalario;
    } //-- long getCdPropostaJuridicaPagamentoSalario() 

    /**
     * Returns the value of field 'cdSegmentoParticipanteMaster'.
     * 
     * @return int
     * @return the value of field 'cdSegmentoParticipanteMaster'.
     */
    public int getCdSegmentoParticipanteMaster()
    {
        return this._cdSegmentoParticipanteMaster;
    } //-- int getCdSegmentoParticipanteMaster() 

    /**
     * Returns the value of field 'cdSetorContrato'.
     * 
     * @return int
     * @return the value of field 'cdSetorContrato'.
     */
    public int getCdSetorContrato()
    {
        return this._cdSetorContrato;
    } //-- int getCdSetorContrato() 

    /**
     * Returns the value of field 'cdSituacaoContrato'.
     * 
     * @return int
     * @return the value of field 'cdSituacaoContrato'.
     */
    public int getCdSituacaoContrato()
    {
        return this._cdSituacaoContrato;
    } //-- int getCdSituacaoContrato() 

    /**
     * Returns the value of field 'cdSituacaoMotivoContrato'.
     * 
     * @return int
     * @return the value of field 'cdSituacaoMotivoContrato'.
     */
    public int getCdSituacaoMotivoContrato()
    {
        return this._cdSituacaoMotivoContrato;
    } //-- int getCdSituacaoMotivoContrato() 

    /**
     * Returns the value of field 'cdSubSegmentoMaster'.
     * 
     * @return int
     * @return the value of field 'cdSubSegmentoMaster'.
     */
    public int getCdSubSegmentoMaster()
    {
        return this._cdSubSegmentoMaster;
    } //-- int getCdSubSegmentoMaster() 

    /**
     * Returns the value of field 'cdTipoCanalInclusao'.
     * 
     * @return int
     * @return the value of field 'cdTipoCanalInclusao'.
     */
    public int getCdTipoCanalInclusao()
    {
        return this._cdTipoCanalInclusao;
    } //-- int getCdTipoCanalInclusao() 

    /**
     * Returns the value of field 'cdTipoCanalManutencao'.
     * 
     * @return int
     * @return the value of field 'cdTipoCanalManutencao'.
     */
    public int getCdTipoCanalManutencao()
    {
        return this._cdTipoCanalManutencao;
    } //-- int getCdTipoCanalManutencao() 

    /**
     * Returns the value of field 'cdTipoContratoOutros'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoOutros'.
     */
    public int getCdTipoContratoOutros()
    {
        return this._cdTipoContratoOutros;
    } //-- int getCdTipoContratoOutros() 

    /**
     * Returns the value of field 'cdTipoContratoPagamentoSalario'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoPagamentoSalario'.
     */
    public int getCdTipoContratoPagamentoSalario()
    {
        return this._cdTipoContratoPagamentoSalario;
    } //-- int getCdTipoContratoPagamentoSalario() 

    /**
     * Returns the value of field 'cdTipoContratoProposta'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoProposta'.
     */
    public int getCdTipoContratoProposta()
    {
        return this._cdTipoContratoProposta;
    } //-- int getCdTipoContratoProposta() 

    /**
     * Returns the value of field 'cdUnidadeContabilContrato'.
     * 
     * @return int
     * @return the value of field 'cdUnidadeContabilContrato'.
     */
    public int getCdUnidadeContabilContrato()
    {
        return this._cdUnidadeContabilContrato;
    } //-- int getCdUnidadeContabilContrato() 

    /**
     * Returns the value of field 'cdUnidadeGestoraContrato'.
     * 
     * @return int
     * @return the value of field 'cdUnidadeGestoraContrato'.
     */
    public int getCdUnidadeGestoraContrato()
    {
        return this._cdUnidadeGestoraContrato;
    } //-- int getCdUnidadeGestoraContrato() 

    /**
     * Returns the value of field 'cdUnidadeOperacionalContrato'.
     * 
     * @return int
     * @return the value of field 'cdUnidadeOperacionalContrato'.
     */
    public int getCdUnidadeOperacionalContrato()
    {
        return this._cdUnidadeOperacionalContrato;
    } //-- int getCdUnidadeOperacionalContrato() 

    /**
     * Returns the value of field 'cdUsuarioInclusao'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioInclusao'.
     */
    public java.lang.String getCdUsuarioInclusao()
    {
        return this._cdUsuarioInclusao;
    } //-- java.lang.String getCdUsuarioInclusao() 

    /**
     * Returns the value of field 'cdUsuarioManutencao'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioManutencao'.
     */
    public java.lang.String getCdUsuarioManutencao()
    {
        return this._cdUsuarioManutencao;
    } //-- java.lang.String getCdUsuarioManutencao() 

    /**
     * Returns the value of field 'codMensagem'.
     * 
     * @return String
     * @return the value of field 'codMensagem'.
     */
    public java.lang.String getCodMensagem()
    {
        return this._codMensagem;
    } //-- java.lang.String getCodMensagem() 

    /**
     * Returns the value of field 'deOrigem'.
     * 
     * @return String
     * @return the value of field 'deOrigem'.
     */
    public java.lang.String getDeOrigem()
    {
        return this._deOrigem;
    } //-- java.lang.String getDeOrigem() 

    /**
     * Returns the value of field 'dsAtividadeEconomicaMaster'.
     * 
     * @return String
     * @return the value of field 'dsAtividadeEconomicaMaster'.
     */
    public java.lang.String getDsAtividadeEconomicaMaster()
    {
        return this._dsAtividadeEconomicaMaster;
    } //-- java.lang.String getDsAtividadeEconomicaMaster() 

    /**
     * Returns the value of field 'dsContratoNegocio'.
     * 
     * @return String
     * @return the value of field 'dsContratoNegocio'.
     */
    public java.lang.String getDsContratoNegocio()
    {
        return this._dsContratoNegocio;
    } //-- java.lang.String getDsContratoNegocio() 

    /**
     * Returns the value of field 'dsFormaAutorizacaoPagamento'.
     * 
     * @return String
     * @return the value of field 'dsFormaAutorizacaoPagamento'.
     */
    public java.lang.String getDsFormaAutorizacaoPagamento()
    {
        return this._dsFormaAutorizacaoPagamento;
    } //-- java.lang.String getDsFormaAutorizacaoPagamento() 

    /**
     * Returns the value of field 'dsGrupoEconomicoMaster'.
     * 
     * @return String
     * @return the value of field 'dsGrupoEconomicoMaster'.
     */
    public java.lang.String getDsGrupoEconomicoMaster()
    {
        return this._dsGrupoEconomicoMaster;
    } //-- java.lang.String getDsGrupoEconomicoMaster() 

    /**
     * Returns the value of field 'dsIdioma'.
     * 
     * @return String
     * @return the value of field 'dsIdioma'.
     */
    public java.lang.String getDsIdioma()
    {
        return this._dsIdioma;
    } //-- java.lang.String getDsIdioma() 

    /**
     * Returns the value of field 'dsMoeda'.
     * 
     * @return String
     * @return the value of field 'dsMoeda'.
     */
    public java.lang.String getDsMoeda()
    {
        return this._dsMoeda;
    } //-- java.lang.String getDsMoeda() 

    /**
     * Returns the value of field 'dsPessoaJuridicaProposta'.
     * 
     * @return String
     * @return the value of field 'dsPessoaJuridicaProposta'.
     */
    public java.lang.String getDsPessoaJuridicaProposta()
    {
        return this._dsPessoaJuridicaProposta;
    } //-- java.lang.String getDsPessoaJuridicaProposta() 

    /**
     * Returns the value of field 'dsSegmentoParticipanteMaster'.
     * 
     * @return String
     * @return the value of field 'dsSegmentoParticipanteMaster'.
     */
    public java.lang.String getDsSegmentoParticipanteMaster()
    {
        return this._dsSegmentoParticipanteMaster;
    } //-- java.lang.String getDsSegmentoParticipanteMaster() 

    /**
     * Returns the value of field 'dsSetorContrato'.
     * 
     * @return String
     * @return the value of field 'dsSetorContrato'.
     */
    public java.lang.String getDsSetorContrato()
    {
        return this._dsSetorContrato;
    } //-- java.lang.String getDsSetorContrato() 

    /**
     * Returns the value of field 'dsSituacaoContrato'.
     * 
     * @return String
     * @return the value of field 'dsSituacaoContrato'.
     */
    public java.lang.String getDsSituacaoContrato()
    {
        return this._dsSituacaoContrato;
    } //-- java.lang.String getDsSituacaoContrato() 

    /**
     * Returns the value of field 'dsSituacaoMotivoContrato'.
     * 
     * @return String
     * @return the value of field 'dsSituacaoMotivoContrato'.
     */
    public java.lang.String getDsSituacaoMotivoContrato()
    {
        return this._dsSituacaoMotivoContrato;
    } //-- java.lang.String getDsSituacaoMotivoContrato() 

    /**
     * Returns the value of field 'dsSubSegmentoMaster'.
     * 
     * @return String
     * @return the value of field 'dsSubSegmentoMaster'.
     */
    public java.lang.String getDsSubSegmentoMaster()
    {
        return this._dsSubSegmentoMaster;
    } //-- java.lang.String getDsSubSegmentoMaster() 

    /**
     * Returns the value of field 'dsTipoCanalInclusao'.
     * 
     * @return String
     * @return the value of field 'dsTipoCanalInclusao'.
     */
    public java.lang.String getDsTipoCanalInclusao()
    {
        return this._dsTipoCanalInclusao;
    } //-- java.lang.String getDsTipoCanalInclusao() 

    /**
     * Returns the value of field 'dsTipoCanalManutencao'.
     * 
     * @return String
     * @return the value of field 'dsTipoCanalManutencao'.
     */
    public java.lang.String getDsTipoCanalManutencao()
    {
        return this._dsTipoCanalManutencao;
    } //-- java.lang.String getDsTipoCanalManutencao() 

    /**
     * Returns the value of field 'dsTipoContratoProposta'.
     * 
     * @return String
     * @return the value of field 'dsTipoContratoProposta'.
     */
    public java.lang.String getDsTipoContratoProposta()
    {
        return this._dsTipoContratoProposta;
    } //-- java.lang.String getDsTipoContratoProposta() 

    /**
     * Returns the value of field
     * 'dsindicadorEncerramentoContrato'.
     * 
     * @return String
     * @return the value of field 'dsindicadorEncerramentoContrato'.
     */
    public java.lang.String getDsindicadorEncerramentoContrato()
    {
        return this._dsindicadorEncerramentoContrato;
    } //-- java.lang.String getDsindicadorEncerramentoContrato() 

    /**
     * Returns the value of field 'dtAssinaturaContratoNegocio'.
     * 
     * @return String
     * @return the value of field 'dtAssinaturaContratoNegocio'.
     */
    public java.lang.String getDtAssinaturaContratoNegocio()
    {
        return this._dtAssinaturaContratoNegocio;
    } //-- java.lang.String getDtAssinaturaContratoNegocio() 

    /**
     * Returns the value of field 'dtCadastroContrato'.
     * 
     * @return String
     * @return the value of field 'dtCadastroContrato'.
     */
    public java.lang.String getDtCadastroContrato()
    {
        return this._dtCadastroContrato;
    } //-- java.lang.String getDtCadastroContrato() 

    /**
     * Returns the value of field 'dtInclusao'.
     * 
     * @return String
     * @return the value of field 'dtInclusao'.
     */
    public java.lang.String getDtInclusao()
    {
        return this._dtInclusao;
    } //-- java.lang.String getDtInclusao() 

    /**
     * Returns the value of field 'dtManutencao'.
     * 
     * @return String
     * @return the value of field 'dtManutencao'.
     */
    public java.lang.String getDtManutencao()
    {
        return this._dtManutencao;
    } //-- java.lang.String getDtManutencao() 

    /**
     * Returns the value of field 'dtVigenciaContrato'.
     * 
     * @return String
     * @return the value of field 'dtVigenciaContrato'.
     */
    public java.lang.String getDtVigenciaContrato()
    {
        return this._dtVigenciaContrato;
    } //-- java.lang.String getDtVigenciaContrato() 

    /**
     * Returns the value of field 'hrAssinaturaContratoNegocio'.
     * 
     * @return String
     * @return the value of field 'hrAssinaturaContratoNegocio'.
     */
    public java.lang.String getHrAssinaturaContratoNegocio()
    {
        return this._hrAssinaturaContratoNegocio;
    } //-- java.lang.String getHrAssinaturaContratoNegocio() 

    /**
     * Returns the value of field 'hrCadastroContrato'.
     * 
     * @return String
     * @return the value of field 'hrCadastroContrato'.
     */
    public java.lang.String getHrCadastroContrato()
    {
        return this._hrCadastroContrato;
    } //-- java.lang.String getHrCadastroContrato() 

    /**
     * Returns the value of field 'hrInclusao'.
     * 
     * @return String
     * @return the value of field 'hrInclusao'.
     */
    public java.lang.String getHrInclusao()
    {
        return this._hrInclusao;
    } //-- java.lang.String getHrInclusao() 

    /**
     * Returns the value of field 'hrManutencao'.
     * 
     * @return String
     * @return the value of field 'hrManutencao'.
     */
    public java.lang.String getHrManutencao()
    {
        return this._hrManutencao;
    } //-- java.lang.String getHrManutencao() 

    /**
     * Returns the value of field 'hrVigenciaContrato'.
     * 
     * @return String
     * @return the value of field 'hrVigenciaContrato'.
     */
    public java.lang.String getHrVigenciaContrato()
    {
        return this._hrVigenciaContrato;
    } //-- java.lang.String getHrVigenciaContrato() 

    /**
     * Returns the value of field 'indicadorAditivo'.
     * 
     * @return String
     * @return the value of field 'indicadorAditivo'.
     */
    public java.lang.String getIndicadorAditivo()
    {
        return this._indicadorAditivo;
    } //-- java.lang.String getIndicadorAditivo() 

    /**
     * Returns the value of field 'mensagem'.
     * 
     * @return String
     * @return the value of field 'mensagem'.
     */
    public java.lang.String getMensagem()
    {
        return this._mensagem;
    } //-- java.lang.String getMensagem() 

    /**
     * Returns the value of field 'nomeFuncionarioBradesco'.
     * 
     * @return String
     * @return the value of field 'nomeFuncionarioBradesco'.
     */
    public java.lang.String getNomeFuncionarioBradesco()
    {
        return this._nomeFuncionarioBradesco;
    } //-- java.lang.String getNomeFuncionarioBradesco() 

    /**
     * Returns the value of field 'nrComercialContrato'.
     * 
     * @return String
     * @return the value of field 'nrComercialContrato'.
     */
    public java.lang.String getNrComercialContrato()
    {
        return this._nrComercialContrato;
    } //-- java.lang.String getNrComercialContrato() 

    /**
     * Returns the value of field 'nrContratoProposta'.
     * 
     * @return long
     * @return the value of field 'nrContratoProposta'.
     */
    public long getNrContratoProposta()
    {
        return this._nrContratoProposta;
    } //-- long getNrContratoProposta() 

    /**
     * Returns the value of field 'nrSequenciaContratoOutros'.
     * 
     * @return long
     * @return the value of field 'nrSequenciaContratoOutros'.
     */
    public long getNrSequenciaContratoOutros()
    {
        return this._nrSequenciaContratoOutros;
    } //-- long getNrSequenciaContratoOutros() 

    /**
     * Returns the value of field
     * 'nrSequenciaContratoPagamentoSalario'.
     * 
     * @return long
     * @return the value of field
     * 'nrSequenciaContratoPagamentoSalario'.
     */
    public long getNrSequenciaContratoPagamentoSalario()
    {
        return this._nrSequenciaContratoPagamentoSalario;
    } //-- long getNrSequenciaContratoPagamentoSalario() 

    /**
     * Returns the value of field
     * 'nrSequenciaUnidadeOrganizacional'.
     * 
     * @return int
     * @return the value of field 'nrSequenciaUnidadeOrganizacional'
     */
    public int getNrSequenciaUnidadeOrganizacional()
    {
        return this._nrSequenciaUnidadeOrganizacional;
    } //-- int getNrSequenciaUnidadeOrganizacional() 

    /**
     * Returns the value of field 'nrUnidadeContabilContrato'.
     * 
     * @return String
     * @return the value of field 'nrUnidadeContabilContrato'.
     */
    public java.lang.String getNrUnidadeContabilContrato()
    {
        return this._nrUnidadeContabilContrato;
    } //-- java.lang.String getNrUnidadeContabilContrato() 

    /**
     * Returns the value of field 'nrUnidadeGestoraContrato'.
     * 
     * @return String
     * @return the value of field 'nrUnidadeGestoraContrato'.
     */
    public java.lang.String getNrUnidadeGestoraContrato()
    {
        return this._nrUnidadeGestoraContrato;
    } //-- java.lang.String getNrUnidadeGestoraContrato() 

    /**
     * Returns the value of field 'nrUnidadeOperacionalContrato'.
     * 
     * @return String
     * @return the value of field 'nrUnidadeOperacionalContrato'.
     */
    public java.lang.String getNrUnidadeOperacionalContrato()
    {
        return this._nrUnidadeOperacionalContrato;
    } //-- java.lang.String getNrUnidadeOperacionalContrato() 

    /**
     * Returns the value of field 'operacaoFluxoInclusao'.
     * 
     * @return String
     * @return the value of field 'operacaoFluxoInclusao'.
     */
    public java.lang.String getOperacaoFluxoInclusao()
    {
        return this._operacaoFluxoInclusao;
    } //-- java.lang.String getOperacaoFluxoInclusao() 

    /**
     * Returns the value of field 'operacaoFluxoManutencao'.
     * 
     * @return String
     * @return the value of field 'operacaoFluxoManutencao'.
     */
    public java.lang.String getOperacaoFluxoManutencao()
    {
        return this._operacaoFluxoManutencao;
    } //-- java.lang.String getOperacaoFluxoManutencao() 

    /**
     * Returns the value of field 'tipoParticipacaoContrato'.
     * 
     * @return String
     * @return the value of field 'tipoParticipacaoContrato'.
     */
    public java.lang.String getTipoParticipacaoContrato()
    {
        return this._tipoParticipacaoContrato;
    } //-- java.lang.String getTipoParticipacaoContrato() 

    /**
     * Returns the value of field 'vlSaldoVirtualTeste'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlSaldoVirtualTeste'.
     */
    public java.math.BigDecimal getVlSaldoVirtualTeste()
    {
        return this._vlSaldoVirtualTeste;
    } //-- java.math.BigDecimal getVlSaldoVirtualTeste() 

    /**
     * Method hasCdAcaoRelacionamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAcaoRelacionamento()
    {
        return this._has_cdAcaoRelacionamento;
    } //-- boolean hasCdAcaoRelacionamento() 

    /**
     * Method hasCdAtividadeEconomicaMaster
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAtividadeEconomicaMaster()
    {
        return this._has_cdAtividadeEconomicaMaster;
    } //-- boolean hasCdAtividadeEconomicaMaster() 

    /**
     * Method hasCdFormaAutorizacaoPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFormaAutorizacaoPagamento()
    {
        return this._has_cdFormaAutorizacaoPagamento;
    } //-- boolean hasCdFormaAutorizacaoPagamento() 

    /**
     * Method hasCdFuncionarioBradesco
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFuncionarioBradesco()
    {
        return this._has_cdFuncionarioBradesco;
    } //-- boolean hasCdFuncionarioBradesco() 

    /**
     * Method hasCdGrupoEconomicoMaster
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdGrupoEconomicoMaster()
    {
        return this._has_cdGrupoEconomicoMaster;
    } //-- boolean hasCdGrupoEconomicoMaster() 

    /**
     * Method hasCdIdioma
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIdioma()
    {
        return this._has_cdIdioma;
    } //-- boolean hasCdIdioma() 

    /**
     * Method hasCdIndicadorEncerramentoContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorEncerramentoContrato()
    {
        return this._has_cdIndicadorEncerramentoContrato;
    } //-- boolean hasCdIndicadorEncerramentoContrato() 

    /**
     * Method hasCdMoeda
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMoeda()
    {
        return this._has_cdMoeda;
    } //-- boolean hasCdMoeda() 

    /**
     * Method hasCdOrigem
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdOrigem()
    {
        return this._has_cdOrigem;
    } //-- boolean hasCdOrigem() 

    /**
     * Method hasCdPessoaJuridica
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridica()
    {
        return this._has_cdPessoaJuridica;
    } //-- boolean hasCdPessoaJuridica() 

    /**
     * Method hasCdPessoaJuridicaProposta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridicaProposta()
    {
        return this._has_cdPessoaJuridicaProposta;
    } //-- boolean hasCdPessoaJuridicaProposta() 

    /**
     * Method hasCdPropostaJuridicaOutros
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPropostaJuridicaOutros()
    {
        return this._has_cdPropostaJuridicaOutros;
    } //-- boolean hasCdPropostaJuridicaOutros() 

    /**
     * Method hasCdPropostaJuridicaPagamentoSalario
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPropostaJuridicaPagamentoSalario()
    {
        return this._has_cdPropostaJuridicaPagamentoSalario;
    } //-- boolean hasCdPropostaJuridicaPagamentoSalario() 

    /**
     * Method hasCdSegmentoParticipanteMaster
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSegmentoParticipanteMaster()
    {
        return this._has_cdSegmentoParticipanteMaster;
    } //-- boolean hasCdSegmentoParticipanteMaster() 

    /**
     * Method hasCdSetorContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSetorContrato()
    {
        return this._has_cdSetorContrato;
    } //-- boolean hasCdSetorContrato() 

    /**
     * Method hasCdSituacaoContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSituacaoContrato()
    {
        return this._has_cdSituacaoContrato;
    } //-- boolean hasCdSituacaoContrato() 

    /**
     * Method hasCdSituacaoMotivoContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSituacaoMotivoContrato()
    {
        return this._has_cdSituacaoMotivoContrato;
    } //-- boolean hasCdSituacaoMotivoContrato() 

    /**
     * Method hasCdSubSegmentoMaster
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSubSegmentoMaster()
    {
        return this._has_cdSubSegmentoMaster;
    } //-- boolean hasCdSubSegmentoMaster() 

    /**
     * Method hasCdTipoCanalInclusao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoCanalInclusao()
    {
        return this._has_cdTipoCanalInclusao;
    } //-- boolean hasCdTipoCanalInclusao() 

    /**
     * Method hasCdTipoCanalManutencao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoCanalManutencao()
    {
        return this._has_cdTipoCanalManutencao;
    } //-- boolean hasCdTipoCanalManutencao() 

    /**
     * Method hasCdTipoContratoOutros
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoOutros()
    {
        return this._has_cdTipoContratoOutros;
    } //-- boolean hasCdTipoContratoOutros() 

    /**
     * Method hasCdTipoContratoPagamentoSalario
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoPagamentoSalario()
    {
        return this._has_cdTipoContratoPagamentoSalario;
    } //-- boolean hasCdTipoContratoPagamentoSalario() 

    /**
     * Method hasCdTipoContratoProposta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoProposta()
    {
        return this._has_cdTipoContratoProposta;
    } //-- boolean hasCdTipoContratoProposta() 

    /**
     * Method hasCdUnidadeContabilContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdUnidadeContabilContrato()
    {
        return this._has_cdUnidadeContabilContrato;
    } //-- boolean hasCdUnidadeContabilContrato() 

    /**
     * Method hasCdUnidadeGestoraContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdUnidadeGestoraContrato()
    {
        return this._has_cdUnidadeGestoraContrato;
    } //-- boolean hasCdUnidadeGestoraContrato() 

    /**
     * Method hasCdUnidadeOperacionalContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdUnidadeOperacionalContrato()
    {
        return this._has_cdUnidadeOperacionalContrato;
    } //-- boolean hasCdUnidadeOperacionalContrato() 

    /**
     * Method hasNrContratoProposta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrContratoProposta()
    {
        return this._has_nrContratoProposta;
    } //-- boolean hasNrContratoProposta() 

    /**
     * Method hasNrSequenciaContratoOutros
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaContratoOutros()
    {
        return this._has_nrSequenciaContratoOutros;
    } //-- boolean hasNrSequenciaContratoOutros() 

    /**
     * Method hasNrSequenciaContratoPagamentoSalario
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaContratoPagamentoSalario()
    {
        return this._has_nrSequenciaContratoPagamentoSalario;
    } //-- boolean hasNrSequenciaContratoPagamentoSalario() 

    /**
     * Method hasNrSequenciaUnidadeOrganizacional
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaUnidadeOrganizacional()
    {
        return this._has_nrSequenciaUnidadeOrganizacional;
    } //-- boolean hasNrSequenciaUnidadeOrganizacional() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdAcaoRelacionamento'.
     * 
     * @param cdAcaoRelacionamento the value of field
     * 'cdAcaoRelacionamento'.
     */
    public void setCdAcaoRelacionamento(long cdAcaoRelacionamento)
    {
        this._cdAcaoRelacionamento = cdAcaoRelacionamento;
        this._has_cdAcaoRelacionamento = true;
    } //-- void setCdAcaoRelacionamento(long) 

    /**
     * Sets the value of field 'cdAtividadeEconomicaMaster'.
     * 
     * @param cdAtividadeEconomicaMaster the value of field
     * 'cdAtividadeEconomicaMaster'.
     */
    public void setCdAtividadeEconomicaMaster(int cdAtividadeEconomicaMaster)
    {
        this._cdAtividadeEconomicaMaster = cdAtividadeEconomicaMaster;
        this._has_cdAtividadeEconomicaMaster = true;
    } //-- void setCdAtividadeEconomicaMaster(int) 

    /**
     * Sets the value of field 'cdFormaAutorizacaoPagamento'.
     * 
     * @param cdFormaAutorizacaoPagamento the value of field
     * 'cdFormaAutorizacaoPagamento'.
     */
    public void setCdFormaAutorizacaoPagamento(int cdFormaAutorizacaoPagamento)
    {
        this._cdFormaAutorizacaoPagamento = cdFormaAutorizacaoPagamento;
        this._has_cdFormaAutorizacaoPagamento = true;
    } //-- void setCdFormaAutorizacaoPagamento(int) 

    /**
     * Sets the value of field 'cdFuncionarioBradesco'.
     * 
     * @param cdFuncionarioBradesco the value of field
     * 'cdFuncionarioBradesco'.
     */
    public void setCdFuncionarioBradesco(long cdFuncionarioBradesco)
    {
        this._cdFuncionarioBradesco = cdFuncionarioBradesco;
        this._has_cdFuncionarioBradesco = true;
    } //-- void setCdFuncionarioBradesco(long) 

    /**
     * Sets the value of field 'cdGrupoEconomicoMaster'.
     * 
     * @param cdGrupoEconomicoMaster the value of field
     * 'cdGrupoEconomicoMaster'.
     */
    public void setCdGrupoEconomicoMaster(long cdGrupoEconomicoMaster)
    {
        this._cdGrupoEconomicoMaster = cdGrupoEconomicoMaster;
        this._has_cdGrupoEconomicoMaster = true;
    } //-- void setCdGrupoEconomicoMaster(long) 

    /**
     * Sets the value of field 'cdIdioma'.
     * 
     * @param cdIdioma the value of field 'cdIdioma'.
     */
    public void setCdIdioma(int cdIdioma)
    {
        this._cdIdioma = cdIdioma;
        this._has_cdIdioma = true;
    } //-- void setCdIdioma(int) 

    /**
     * Sets the value of field 'cdIndicadorEncerramentoContrato'.
     * 
     * @param cdIndicadorEncerramentoContrato the value of field
     * 'cdIndicadorEncerramentoContrato'.
     */
    public void setCdIndicadorEncerramentoContrato(int cdIndicadorEncerramentoContrato)
    {
        this._cdIndicadorEncerramentoContrato = cdIndicadorEncerramentoContrato;
        this._has_cdIndicadorEncerramentoContrato = true;
    } //-- void setCdIndicadorEncerramentoContrato(int) 

    /**
     * Sets the value of field 'cdMoeda'.
     * 
     * @param cdMoeda the value of field 'cdMoeda'.
     */
    public void setCdMoeda(int cdMoeda)
    {
        this._cdMoeda = cdMoeda;
        this._has_cdMoeda = true;
    } //-- void setCdMoeda(int) 

    /**
     * Sets the value of field 'cdOrigem'.
     * 
     * @param cdOrigem the value of field 'cdOrigem'.
     */
    public void setCdOrigem(int cdOrigem)
    {
        this._cdOrigem = cdOrigem;
        this._has_cdOrigem = true;
    } //-- void setCdOrigem(int) 

    /**
     * Sets the value of field 'cdPessoaJuridica'.
     * 
     * @param cdPessoaJuridica the value of field 'cdPessoaJuridica'
     */
    public void setCdPessoaJuridica(long cdPessoaJuridica)
    {
        this._cdPessoaJuridica = cdPessoaJuridica;
        this._has_cdPessoaJuridica = true;
    } //-- void setCdPessoaJuridica(long) 

    /**
     * Sets the value of field 'cdPessoaJuridicaProposta'.
     * 
     * @param cdPessoaJuridicaProposta the value of field
     * 'cdPessoaJuridicaProposta'.
     */
    public void setCdPessoaJuridicaProposta(long cdPessoaJuridicaProposta)
    {
        this._cdPessoaJuridicaProposta = cdPessoaJuridicaProposta;
        this._has_cdPessoaJuridicaProposta = true;
    } //-- void setCdPessoaJuridicaProposta(long) 

    /**
     * Sets the value of field 'cdPropostaJuridicaOutros'.
     * 
     * @param cdPropostaJuridicaOutros the value of field
     * 'cdPropostaJuridicaOutros'.
     */
    public void setCdPropostaJuridicaOutros(long cdPropostaJuridicaOutros)
    {
        this._cdPropostaJuridicaOutros = cdPropostaJuridicaOutros;
        this._has_cdPropostaJuridicaOutros = true;
    } //-- void setCdPropostaJuridicaOutros(long) 

    /**
     * Sets the value of field
     * 'cdPropostaJuridicaPagamentoSalario'.
     * 
     * @param cdPropostaJuridicaPagamentoSalario the value of field
     * 'cdPropostaJuridicaPagamentoSalario'.
     */
    public void setCdPropostaJuridicaPagamentoSalario(long cdPropostaJuridicaPagamentoSalario)
    {
        this._cdPropostaJuridicaPagamentoSalario = cdPropostaJuridicaPagamentoSalario;
        this._has_cdPropostaJuridicaPagamentoSalario = true;
    } //-- void setCdPropostaJuridicaPagamentoSalario(long) 

    /**
     * Sets the value of field 'cdSegmentoParticipanteMaster'.
     * 
     * @param cdSegmentoParticipanteMaster the value of field
     * 'cdSegmentoParticipanteMaster'.
     */
    public void setCdSegmentoParticipanteMaster(int cdSegmentoParticipanteMaster)
    {
        this._cdSegmentoParticipanteMaster = cdSegmentoParticipanteMaster;
        this._has_cdSegmentoParticipanteMaster = true;
    } //-- void setCdSegmentoParticipanteMaster(int) 

    /**
     * Sets the value of field 'cdSetorContrato'.
     * 
     * @param cdSetorContrato the value of field 'cdSetorContrato'.
     */
    public void setCdSetorContrato(int cdSetorContrato)
    {
        this._cdSetorContrato = cdSetorContrato;
        this._has_cdSetorContrato = true;
    } //-- void setCdSetorContrato(int) 

    /**
     * Sets the value of field 'cdSituacaoContrato'.
     * 
     * @param cdSituacaoContrato the value of field
     * 'cdSituacaoContrato'.
     */
    public void setCdSituacaoContrato(int cdSituacaoContrato)
    {
        this._cdSituacaoContrato = cdSituacaoContrato;
        this._has_cdSituacaoContrato = true;
    } //-- void setCdSituacaoContrato(int) 

    /**
     * Sets the value of field 'cdSituacaoMotivoContrato'.
     * 
     * @param cdSituacaoMotivoContrato the value of field
     * 'cdSituacaoMotivoContrato'.
     */
    public void setCdSituacaoMotivoContrato(int cdSituacaoMotivoContrato)
    {
        this._cdSituacaoMotivoContrato = cdSituacaoMotivoContrato;
        this._has_cdSituacaoMotivoContrato = true;
    } //-- void setCdSituacaoMotivoContrato(int) 

    /**
     * Sets the value of field 'cdSubSegmentoMaster'.
     * 
     * @param cdSubSegmentoMaster the value of field
     * 'cdSubSegmentoMaster'.
     */
    public void setCdSubSegmentoMaster(int cdSubSegmentoMaster)
    {
        this._cdSubSegmentoMaster = cdSubSegmentoMaster;
        this._has_cdSubSegmentoMaster = true;
    } //-- void setCdSubSegmentoMaster(int) 

    /**
     * Sets the value of field 'cdTipoCanalInclusao'.
     * 
     * @param cdTipoCanalInclusao the value of field
     * 'cdTipoCanalInclusao'.
     */
    public void setCdTipoCanalInclusao(int cdTipoCanalInclusao)
    {
        this._cdTipoCanalInclusao = cdTipoCanalInclusao;
        this._has_cdTipoCanalInclusao = true;
    } //-- void setCdTipoCanalInclusao(int) 

    /**
     * Sets the value of field 'cdTipoCanalManutencao'.
     * 
     * @param cdTipoCanalManutencao the value of field
     * 'cdTipoCanalManutencao'.
     */
    public void setCdTipoCanalManutencao(int cdTipoCanalManutencao)
    {
        this._cdTipoCanalManutencao = cdTipoCanalManutencao;
        this._has_cdTipoCanalManutencao = true;
    } //-- void setCdTipoCanalManutencao(int) 

    /**
     * Sets the value of field 'cdTipoContratoOutros'.
     * 
     * @param cdTipoContratoOutros the value of field
     * 'cdTipoContratoOutros'.
     */
    public void setCdTipoContratoOutros(int cdTipoContratoOutros)
    {
        this._cdTipoContratoOutros = cdTipoContratoOutros;
        this._has_cdTipoContratoOutros = true;
    } //-- void setCdTipoContratoOutros(int) 

    /**
     * Sets the value of field 'cdTipoContratoPagamentoSalario'.
     * 
     * @param cdTipoContratoPagamentoSalario the value of field
     * 'cdTipoContratoPagamentoSalario'.
     */
    public void setCdTipoContratoPagamentoSalario(int cdTipoContratoPagamentoSalario)
    {
        this._cdTipoContratoPagamentoSalario = cdTipoContratoPagamentoSalario;
        this._has_cdTipoContratoPagamentoSalario = true;
    } //-- void setCdTipoContratoPagamentoSalario(int) 

    /**
     * Sets the value of field 'cdTipoContratoProposta'.
     * 
     * @param cdTipoContratoProposta the value of field
     * 'cdTipoContratoProposta'.
     */
    public void setCdTipoContratoProposta(int cdTipoContratoProposta)
    {
        this._cdTipoContratoProposta = cdTipoContratoProposta;
        this._has_cdTipoContratoProposta = true;
    } //-- void setCdTipoContratoProposta(int) 

    /**
     * Sets the value of field 'cdUnidadeContabilContrato'.
     * 
     * @param cdUnidadeContabilContrato the value of field
     * 'cdUnidadeContabilContrato'.
     */
    public void setCdUnidadeContabilContrato(int cdUnidadeContabilContrato)
    {
        this._cdUnidadeContabilContrato = cdUnidadeContabilContrato;
        this._has_cdUnidadeContabilContrato = true;
    } //-- void setCdUnidadeContabilContrato(int) 

    /**
     * Sets the value of field 'cdUnidadeGestoraContrato'.
     * 
     * @param cdUnidadeGestoraContrato the value of field
     * 'cdUnidadeGestoraContrato'.
     */
    public void setCdUnidadeGestoraContrato(int cdUnidadeGestoraContrato)
    {
        this._cdUnidadeGestoraContrato = cdUnidadeGestoraContrato;
        this._has_cdUnidadeGestoraContrato = true;
    } //-- void setCdUnidadeGestoraContrato(int) 

    /**
     * Sets the value of field 'cdUnidadeOperacionalContrato'.
     * 
     * @param cdUnidadeOperacionalContrato the value of field
     * 'cdUnidadeOperacionalContrato'.
     */
    public void setCdUnidadeOperacionalContrato(int cdUnidadeOperacionalContrato)
    {
        this._cdUnidadeOperacionalContrato = cdUnidadeOperacionalContrato;
        this._has_cdUnidadeOperacionalContrato = true;
    } //-- void setCdUnidadeOperacionalContrato(int) 

    /**
     * Sets the value of field 'cdUsuarioInclusao'.
     * 
     * @param cdUsuarioInclusao the value of field
     * 'cdUsuarioInclusao'.
     */
    public void setCdUsuarioInclusao(java.lang.String cdUsuarioInclusao)
    {
        this._cdUsuarioInclusao = cdUsuarioInclusao;
    } //-- void setCdUsuarioInclusao(java.lang.String) 

    /**
     * Sets the value of field 'cdUsuarioManutencao'.
     * 
     * @param cdUsuarioManutencao the value of field
     * 'cdUsuarioManutencao'.
     */
    public void setCdUsuarioManutencao(java.lang.String cdUsuarioManutencao)
    {
        this._cdUsuarioManutencao = cdUsuarioManutencao;
    } //-- void setCdUsuarioManutencao(java.lang.String) 

    /**
     * Sets the value of field 'codMensagem'.
     * 
     * @param codMensagem the value of field 'codMensagem'.
     */
    public void setCodMensagem(java.lang.String codMensagem)
    {
        this._codMensagem = codMensagem;
    } //-- void setCodMensagem(java.lang.String) 

    /**
     * Sets the value of field 'deOrigem'.
     * 
     * @param deOrigem the value of field 'deOrigem'.
     */
    public void setDeOrigem(java.lang.String deOrigem)
    {
        this._deOrigem = deOrigem;
    } //-- void setDeOrigem(java.lang.String) 

    /**
     * Sets the value of field 'dsAtividadeEconomicaMaster'.
     * 
     * @param dsAtividadeEconomicaMaster the value of field
     * 'dsAtividadeEconomicaMaster'.
     */
    public void setDsAtividadeEconomicaMaster(java.lang.String dsAtividadeEconomicaMaster)
    {
        this._dsAtividadeEconomicaMaster = dsAtividadeEconomicaMaster;
    } //-- void setDsAtividadeEconomicaMaster(java.lang.String) 

    /**
     * Sets the value of field 'dsContratoNegocio'.
     * 
     * @param dsContratoNegocio the value of field
     * 'dsContratoNegocio'.
     */
    public void setDsContratoNegocio(java.lang.String dsContratoNegocio)
    {
        this._dsContratoNegocio = dsContratoNegocio;
    } //-- void setDsContratoNegocio(java.lang.String) 

    /**
     * Sets the value of field 'dsFormaAutorizacaoPagamento'.
     * 
     * @param dsFormaAutorizacaoPagamento the value of field
     * 'dsFormaAutorizacaoPagamento'.
     */
    public void setDsFormaAutorizacaoPagamento(java.lang.String dsFormaAutorizacaoPagamento)
    {
        this._dsFormaAutorizacaoPagamento = dsFormaAutorizacaoPagamento;
    } //-- void setDsFormaAutorizacaoPagamento(java.lang.String) 

    /**
     * Sets the value of field 'dsGrupoEconomicoMaster'.
     * 
     * @param dsGrupoEconomicoMaster the value of field
     * 'dsGrupoEconomicoMaster'.
     */
    public void setDsGrupoEconomicoMaster(java.lang.String dsGrupoEconomicoMaster)
    {
        this._dsGrupoEconomicoMaster = dsGrupoEconomicoMaster;
    } //-- void setDsGrupoEconomicoMaster(java.lang.String) 

    /**
     * Sets the value of field 'dsIdioma'.
     * 
     * @param dsIdioma the value of field 'dsIdioma'.
     */
    public void setDsIdioma(java.lang.String dsIdioma)
    {
        this._dsIdioma = dsIdioma;
    } //-- void setDsIdioma(java.lang.String) 

    /**
     * Sets the value of field 'dsMoeda'.
     * 
     * @param dsMoeda the value of field 'dsMoeda'.
     */
    public void setDsMoeda(java.lang.String dsMoeda)
    {
        this._dsMoeda = dsMoeda;
    } //-- void setDsMoeda(java.lang.String) 

    /**
     * Sets the value of field 'dsPessoaJuridicaProposta'.
     * 
     * @param dsPessoaJuridicaProposta the value of field
     * 'dsPessoaJuridicaProposta'.
     */
    public void setDsPessoaJuridicaProposta(java.lang.String dsPessoaJuridicaProposta)
    {
        this._dsPessoaJuridicaProposta = dsPessoaJuridicaProposta;
    } //-- void setDsPessoaJuridicaProposta(java.lang.String) 

    /**
     * Sets the value of field 'dsSegmentoParticipanteMaster'.
     * 
     * @param dsSegmentoParticipanteMaster the value of field
     * 'dsSegmentoParticipanteMaster'.
     */
    public void setDsSegmentoParticipanteMaster(java.lang.String dsSegmentoParticipanteMaster)
    {
        this._dsSegmentoParticipanteMaster = dsSegmentoParticipanteMaster;
    } //-- void setDsSegmentoParticipanteMaster(java.lang.String) 

    /**
     * Sets the value of field 'dsSetorContrato'.
     * 
     * @param dsSetorContrato the value of field 'dsSetorContrato'.
     */
    public void setDsSetorContrato(java.lang.String dsSetorContrato)
    {
        this._dsSetorContrato = dsSetorContrato;
    } //-- void setDsSetorContrato(java.lang.String) 

    /**
     * Sets the value of field 'dsSituacaoContrato'.
     * 
     * @param dsSituacaoContrato the value of field
     * 'dsSituacaoContrato'.
     */
    public void setDsSituacaoContrato(java.lang.String dsSituacaoContrato)
    {
        this._dsSituacaoContrato = dsSituacaoContrato;
    } //-- void setDsSituacaoContrato(java.lang.String) 

    /**
     * Sets the value of field 'dsSituacaoMotivoContrato'.
     * 
     * @param dsSituacaoMotivoContrato the value of field
     * 'dsSituacaoMotivoContrato'.
     */
    public void setDsSituacaoMotivoContrato(java.lang.String dsSituacaoMotivoContrato)
    {
        this._dsSituacaoMotivoContrato = dsSituacaoMotivoContrato;
    } //-- void setDsSituacaoMotivoContrato(java.lang.String) 

    /**
     * Sets the value of field 'dsSubSegmentoMaster'.
     * 
     * @param dsSubSegmentoMaster the value of field
     * 'dsSubSegmentoMaster'.
     */
    public void setDsSubSegmentoMaster(java.lang.String dsSubSegmentoMaster)
    {
        this._dsSubSegmentoMaster = dsSubSegmentoMaster;
    } //-- void setDsSubSegmentoMaster(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoCanalInclusao'.
     * 
     * @param dsTipoCanalInclusao the value of field
     * 'dsTipoCanalInclusao'.
     */
    public void setDsTipoCanalInclusao(java.lang.String dsTipoCanalInclusao)
    {
        this._dsTipoCanalInclusao = dsTipoCanalInclusao;
    } //-- void setDsTipoCanalInclusao(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoCanalManutencao'.
     * 
     * @param dsTipoCanalManutencao the value of field
     * 'dsTipoCanalManutencao'.
     */
    public void setDsTipoCanalManutencao(java.lang.String dsTipoCanalManutencao)
    {
        this._dsTipoCanalManutencao = dsTipoCanalManutencao;
    } //-- void setDsTipoCanalManutencao(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoContratoProposta'.
     * 
     * @param dsTipoContratoProposta the value of field
     * 'dsTipoContratoProposta'.
     */
    public void setDsTipoContratoProposta(java.lang.String dsTipoContratoProposta)
    {
        this._dsTipoContratoProposta = dsTipoContratoProposta;
    } //-- void setDsTipoContratoProposta(java.lang.String) 

    /**
     * Sets the value of field 'dsindicadorEncerramentoContrato'.
     * 
     * @param dsindicadorEncerramentoContrato the value of field
     * 'dsindicadorEncerramentoContrato'.
     */
    public void setDsindicadorEncerramentoContrato(java.lang.String dsindicadorEncerramentoContrato)
    {
        this._dsindicadorEncerramentoContrato = dsindicadorEncerramentoContrato;
    } //-- void setDsindicadorEncerramentoContrato(java.lang.String) 

    /**
     * Sets the value of field 'dtAssinaturaContratoNegocio'.
     * 
     * @param dtAssinaturaContratoNegocio the value of field
     * 'dtAssinaturaContratoNegocio'.
     */
    public void setDtAssinaturaContratoNegocio(java.lang.String dtAssinaturaContratoNegocio)
    {
        this._dtAssinaturaContratoNegocio = dtAssinaturaContratoNegocio;
    } //-- void setDtAssinaturaContratoNegocio(java.lang.String) 

    /**
     * Sets the value of field 'dtCadastroContrato'.
     * 
     * @param dtCadastroContrato the value of field
     * 'dtCadastroContrato'.
     */
    public void setDtCadastroContrato(java.lang.String dtCadastroContrato)
    {
        this._dtCadastroContrato = dtCadastroContrato;
    } //-- void setDtCadastroContrato(java.lang.String) 

    /**
     * Sets the value of field 'dtInclusao'.
     * 
     * @param dtInclusao the value of field 'dtInclusao'.
     */
    public void setDtInclusao(java.lang.String dtInclusao)
    {
        this._dtInclusao = dtInclusao;
    } //-- void setDtInclusao(java.lang.String) 

    /**
     * Sets the value of field 'dtManutencao'.
     * 
     * @param dtManutencao the value of field 'dtManutencao'.
     */
    public void setDtManutencao(java.lang.String dtManutencao)
    {
        this._dtManutencao = dtManutencao;
    } //-- void setDtManutencao(java.lang.String) 

    /**
     * Sets the value of field 'dtVigenciaContrato'.
     * 
     * @param dtVigenciaContrato the value of field
     * 'dtVigenciaContrato'.
     */
    public void setDtVigenciaContrato(java.lang.String dtVigenciaContrato)
    {
        this._dtVigenciaContrato = dtVigenciaContrato;
    } //-- void setDtVigenciaContrato(java.lang.String) 

    /**
     * Sets the value of field 'hrAssinaturaContratoNegocio'.
     * 
     * @param hrAssinaturaContratoNegocio the value of field
     * 'hrAssinaturaContratoNegocio'.
     */
    public void setHrAssinaturaContratoNegocio(java.lang.String hrAssinaturaContratoNegocio)
    {
        this._hrAssinaturaContratoNegocio = hrAssinaturaContratoNegocio;
    } //-- void setHrAssinaturaContratoNegocio(java.lang.String) 

    /**
     * Sets the value of field 'hrCadastroContrato'.
     * 
     * @param hrCadastroContrato the value of field
     * 'hrCadastroContrato'.
     */
    public void setHrCadastroContrato(java.lang.String hrCadastroContrato)
    {
        this._hrCadastroContrato = hrCadastroContrato;
    } //-- void setHrCadastroContrato(java.lang.String) 

    /**
     * Sets the value of field 'hrInclusao'.
     * 
     * @param hrInclusao the value of field 'hrInclusao'.
     */
    public void setHrInclusao(java.lang.String hrInclusao)
    {
        this._hrInclusao = hrInclusao;
    } //-- void setHrInclusao(java.lang.String) 

    /**
     * Sets the value of field 'hrManutencao'.
     * 
     * @param hrManutencao the value of field 'hrManutencao'.
     */
    public void setHrManutencao(java.lang.String hrManutencao)
    {
        this._hrManutencao = hrManutencao;
    } //-- void setHrManutencao(java.lang.String) 

    /**
     * Sets the value of field 'hrVigenciaContrato'.
     * 
     * @param hrVigenciaContrato the value of field
     * 'hrVigenciaContrato'.
     */
    public void setHrVigenciaContrato(java.lang.String hrVigenciaContrato)
    {
        this._hrVigenciaContrato = hrVigenciaContrato;
    } //-- void setHrVigenciaContrato(java.lang.String) 

    /**
     * Sets the value of field 'indicadorAditivo'.
     * 
     * @param indicadorAditivo the value of field 'indicadorAditivo'
     */
    public void setIndicadorAditivo(java.lang.String indicadorAditivo)
    {
        this._indicadorAditivo = indicadorAditivo;
    } //-- void setIndicadorAditivo(java.lang.String) 

    /**
     * Sets the value of field 'mensagem'.
     * 
     * @param mensagem the value of field 'mensagem'.
     */
    public void setMensagem(java.lang.String mensagem)
    {
        this._mensagem = mensagem;
    } //-- void setMensagem(java.lang.String) 

    /**
     * Sets the value of field 'nomeFuncionarioBradesco'.
     * 
     * @param nomeFuncionarioBradesco the value of field
     * 'nomeFuncionarioBradesco'.
     */
    public void setNomeFuncionarioBradesco(java.lang.String nomeFuncionarioBradesco)
    {
        this._nomeFuncionarioBradesco = nomeFuncionarioBradesco;
    } //-- void setNomeFuncionarioBradesco(java.lang.String) 

    /**
     * Sets the value of field 'nrComercialContrato'.
     * 
     * @param nrComercialContrato the value of field
     * 'nrComercialContrato'.
     */
    public void setNrComercialContrato(java.lang.String nrComercialContrato)
    {
        this._nrComercialContrato = nrComercialContrato;
    } //-- void setNrComercialContrato(java.lang.String) 

    /**
     * Sets the value of field 'nrContratoProposta'.
     * 
     * @param nrContratoProposta the value of field
     * 'nrContratoProposta'.
     */
    public void setNrContratoProposta(long nrContratoProposta)
    {
        this._nrContratoProposta = nrContratoProposta;
        this._has_nrContratoProposta = true;
    } //-- void setNrContratoProposta(long) 

    /**
     * Sets the value of field 'nrSequenciaContratoOutros'.
     * 
     * @param nrSequenciaContratoOutros the value of field
     * 'nrSequenciaContratoOutros'.
     */
    public void setNrSequenciaContratoOutros(long nrSequenciaContratoOutros)
    {
        this._nrSequenciaContratoOutros = nrSequenciaContratoOutros;
        this._has_nrSequenciaContratoOutros = true;
    } //-- void setNrSequenciaContratoOutros(long) 

    /**
     * Sets the value of field
     * 'nrSequenciaContratoPagamentoSalario'.
     * 
     * @param nrSequenciaContratoPagamentoSalario the value of
     * field 'nrSequenciaContratoPagamentoSalario'.
     */
    public void setNrSequenciaContratoPagamentoSalario(long nrSequenciaContratoPagamentoSalario)
    {
        this._nrSequenciaContratoPagamentoSalario = nrSequenciaContratoPagamentoSalario;
        this._has_nrSequenciaContratoPagamentoSalario = true;
    } //-- void setNrSequenciaContratoPagamentoSalario(long) 

    /**
     * Sets the value of field 'nrSequenciaUnidadeOrganizacional'.
     * 
     * @param nrSequenciaUnidadeOrganizacional the value of field
     * 'nrSequenciaUnidadeOrganizacional'.
     */
    public void setNrSequenciaUnidadeOrganizacional(int nrSequenciaUnidadeOrganizacional)
    {
        this._nrSequenciaUnidadeOrganizacional = nrSequenciaUnidadeOrganizacional;
        this._has_nrSequenciaUnidadeOrganizacional = true;
    } //-- void setNrSequenciaUnidadeOrganizacional(int) 

    /**
     * Sets the value of field 'nrUnidadeContabilContrato'.
     * 
     * @param nrUnidadeContabilContrato the value of field
     * 'nrUnidadeContabilContrato'.
     */
    public void setNrUnidadeContabilContrato(java.lang.String nrUnidadeContabilContrato)
    {
        this._nrUnidadeContabilContrato = nrUnidadeContabilContrato;
    } //-- void setNrUnidadeContabilContrato(java.lang.String) 

    /**
     * Sets the value of field 'nrUnidadeGestoraContrato'.
     * 
     * @param nrUnidadeGestoraContrato the value of field
     * 'nrUnidadeGestoraContrato'.
     */
    public void setNrUnidadeGestoraContrato(java.lang.String nrUnidadeGestoraContrato)
    {
        this._nrUnidadeGestoraContrato = nrUnidadeGestoraContrato;
    } //-- void setNrUnidadeGestoraContrato(java.lang.String) 

    /**
     * Sets the value of field 'nrUnidadeOperacionalContrato'.
     * 
     * @param nrUnidadeOperacionalContrato the value of field
     * 'nrUnidadeOperacionalContrato'.
     */
    public void setNrUnidadeOperacionalContrato(java.lang.String nrUnidadeOperacionalContrato)
    {
        this._nrUnidadeOperacionalContrato = nrUnidadeOperacionalContrato;
    } //-- void setNrUnidadeOperacionalContrato(java.lang.String) 

    /**
     * Sets the value of field 'operacaoFluxoInclusao'.
     * 
     * @param operacaoFluxoInclusao the value of field
     * 'operacaoFluxoInclusao'.
     */
    public void setOperacaoFluxoInclusao(java.lang.String operacaoFluxoInclusao)
    {
        this._operacaoFluxoInclusao = operacaoFluxoInclusao;
    } //-- void setOperacaoFluxoInclusao(java.lang.String) 

    /**
     * Sets the value of field 'operacaoFluxoManutencao'.
     * 
     * @param operacaoFluxoManutencao the value of field
     * 'operacaoFluxoManutencao'.
     */
    public void setOperacaoFluxoManutencao(java.lang.String operacaoFluxoManutencao)
    {
        this._operacaoFluxoManutencao = operacaoFluxoManutencao;
    } //-- void setOperacaoFluxoManutencao(java.lang.String) 

    /**
     * Sets the value of field 'tipoParticipacaoContrato'.
     * 
     * @param tipoParticipacaoContrato the value of field
     * 'tipoParticipacaoContrato'.
     */
    public void setTipoParticipacaoContrato(java.lang.String tipoParticipacaoContrato)
    {
        this._tipoParticipacaoContrato = tipoParticipacaoContrato;
    } //-- void setTipoParticipacaoContrato(java.lang.String) 

    /**
     * Sets the value of field 'vlSaldoVirtualTeste'.
     * 
     * @param vlSaldoVirtualTeste the value of field
     * 'vlSaldoVirtualTeste'.
     */
    public void setVlSaldoVirtualTeste(java.math.BigDecimal vlSaldoVirtualTeste)
    {
        this._vlSaldoVirtualTeste = vlSaldoVirtualTeste;
    } //-- void setVlSaldoVirtualTeste(java.math.BigDecimal) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ConsultarDadosBasicoContratoResponse
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultardadosbasicocontrato.response.ConsultarDadosBasicoContratoResponse unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultardadosbasicocontrato.response.ConsultarDadosBasicoContratoResponse) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultardadosbasicocontrato.response.ConsultarDadosBasicoContratoResponse.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultardadosbasicocontrato.response.ConsultarDadosBasicoContratoResponse unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
