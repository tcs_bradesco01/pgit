/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantercontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantercontrato.bean;

/**
 * Nome: ListarConManAmbPartEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarConManAmbPartEntradaDTO {
	
	/** Atributo maxOcorrencias. */
	private Integer maxOcorrencias;
	
	/** Atributo cdPessoaJuridica. */
	private Long cdPessoaJuridica;
	
	/** Atributo cdTipoContrato. */
	private Integer cdTipoContrato;
	
	/** Atributo nrSequenciaContrato. */
	private Long nrSequenciaContrato;
	
	/** Atributo cdPessoa. */
	private Long cdPessoa;
	
	/** Atributo cdProdutoOperacaoRelacionamento. */
	private Integer cdProdutoOperacaoRelacionamento;
	
	/** Atributo dtInicioManutencao. */
	private String dtInicioManutencao;
	
	/** Atributo dtFimManutencao. */
	private String dtFimManutencao;

	/**
	 * Get: maxOcorrencias.
	 *
	 * @return maxOcorrencias
	 */
	public Integer getMaxOcorrencias() {
		return maxOcorrencias;
	}

	/**
	 * Get: cdPessoaJuridica.
	 *
	 * @return cdPessoaJuridica
	 */
	public Long getCdPessoaJuridica() {
		return cdPessoaJuridica;
	}

	/**
	 * Get: cdTipoContrato.
	 *
	 * @return cdTipoContrato
	 */
	public Integer getCdTipoContrato() {
		return cdTipoContrato;
	}

	/**
	 * Get: nrSequenciaContrato.
	 *
	 * @return nrSequenciaContrato
	 */
	public Long getNrSequenciaContrato() {
		return nrSequenciaContrato;
	}

	/**
	 * Get: cdPessoa.
	 *
	 * @return cdPessoa
	 */
	public Long getCdPessoa() {
		return cdPessoa;
	}

	/**
	 * Get: cdProdutoOperacaoRelacionamento.
	 *
	 * @return cdProdutoOperacaoRelacionamento
	 */
	public Integer getCdProdutoOperacaoRelacionamento() {
		return cdProdutoOperacaoRelacionamento;
	}

	/**
	 * Get: dtInicioManutencao.
	 *
	 * @return dtInicioManutencao
	 */
	public String getDtInicioManutencao() {
		return dtInicioManutencao;
	}

	/**
	 * Get: dtFimManutencao.
	 *
	 * @return dtFimManutencao
	 */
	public String getDtFimManutencao() {
		return dtFimManutencao;
	}

	/**
	 * Set: maxOcorrencias.
	 *
	 * @param maxOcorrencias the max ocorrencias
	 */
	public void setMaxOcorrencias(Integer maxOcorrencias) {
		this.maxOcorrencias = maxOcorrencias;
	}

	/**
	 * Set: cdPessoaJuridica.
	 *
	 * @param cdPessoaJuridica the cd pessoa juridica
	 */
	public void setCdPessoaJuridica(Long cdPessoaJuridica) {
		this.cdPessoaJuridica = cdPessoaJuridica;
	}

	/**
	 * Set: cdTipoContrato.
	 *
	 * @param cdTipoContrato the cd tipo contrato
	 */
	public void setCdTipoContrato(Integer cdTipoContrato) {
		this.cdTipoContrato = cdTipoContrato;
	}

	/**
	 * Set: nrSequenciaContrato.
	 *
	 * @param nrSequenciaContrato the nr sequencia contrato
	 */
	public void setNrSequenciaContrato(Long nrSequenciaContrato) {
		this.nrSequenciaContrato = nrSequenciaContrato;
	}

	/**
	 * Set: cdPessoa.
	 *
	 * @param cdPessoa the cd pessoa
	 */
	public void setCdPessoa(Long cdPessoa) {
		this.cdPessoa = cdPessoa;
	}

	/**
	 * Set: cdProdutoOperacaoRelacionamento.
	 *
	 * @param cdProdutoOperacaoRelacionamento the cd produto operacao relacionamento
	 */
	public void setCdProdutoOperacaoRelacionamento(
			Integer cdProdutoOperacaoRelacionamento) {
		this.cdProdutoOperacaoRelacionamento = cdProdutoOperacaoRelacionamento;
	}

	/**
	 * Set: dtInicioManutencao.
	 *
	 * @param dtInicioManutencao the dt inicio manutencao
	 */
	public void setDtInicioManutencao(String dtInicioManutencao) {
		this.dtInicioManutencao = dtInicioManutencao;
	}

	/**
	 * Set: dtFimManutencao.
	 *
	 * @param dtFimManutencao the dt fim manutencao
	 */
	public void setDtFimManutencao(String dtFimManutencao) {
		this.dtFimManutencao = dtFimManutencao;
	}

}
