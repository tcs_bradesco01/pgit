/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.alterartiporetornolayout.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class AlterarTipoRetornoLayoutRequest.
 * 
 * @version $Revision$ $Date$
 */
public class AlterarTipoRetornoLayoutRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdPessoaJuridicaContrato
     */
    private long _cdPessoaJuridicaContrato = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridicaContrato
     */
    private boolean _has_cdPessoaJuridicaContrato;

    /**
     * Field _cdTipoContratoNegocio
     */
    private int _cdTipoContratoNegocio = 0;

    /**
     * keeps track of state for field: _cdTipoContratoNegocio
     */
    private boolean _has_cdTipoContratoNegocio;

    /**
     * Field _nrSequenciaContratoNegocio
     */
    private long _nrSequenciaContratoNegocio = 0;

    /**
     * keeps track of state for field: _nrSequenciaContratoNegocio
     */
    private boolean _has_nrSequenciaContratoNegocio;

    /**
     * Field _cdTipoLayoutArquivo
     */
    private int _cdTipoLayoutArquivo = 0;

    /**
     * keeps track of state for field: _cdTipoLayoutArquivo
     */
    private boolean _has_cdTipoLayoutArquivo;

    /**
     * Field _cdArquivoRetornoPagamento
     */
    private int _cdArquivoRetornoPagamento = 0;

    /**
     * keeps track of state for field: _cdArquivoRetornoPagamento
     */
    private boolean _has_cdArquivoRetornoPagamento;

    /**
     * Field _cdTipoGeracaoRetorno
     */
    private int _cdTipoGeracaoRetorno = 0;

    /**
     * keeps track of state for field: _cdTipoGeracaoRetorno
     */
    private boolean _has_cdTipoGeracaoRetorno;

    /**
     * Field _cdTipoMontaRetorno
     */
    private int _cdTipoMontaRetorno = 0;

    /**
     * keeps track of state for field: _cdTipoMontaRetorno
     */
    private boolean _has_cdTipoMontaRetorno;

    /**
     * Field _cdTipoOcorrenciaRetorno
     */
    private int _cdTipoOcorrenciaRetorno = 0;

    /**
     * keeps track of state for field: _cdTipoOcorrenciaRetorno
     */
    private boolean _has_cdTipoOcorrenciaRetorno;

    /**
     * Field _cdTipoClassificacaoRetorno
     */
    private int _cdTipoClassificacaoRetorno = 0;

    /**
     * keeps track of state for field: _cdTipoClassificacaoRetorno
     */
    private boolean _has_cdTipoClassificacaoRetorno;

    /**
     * Field _cdTipoOrdemRegistroRetorno
     */
    private int _cdTipoOrdemRegistroRetorno = 0;

    /**
     * keeps track of state for field: _cdTipoOrdemRegistroRetorno
     */
    private boolean _has_cdTipoOrdemRegistroRetorno;


      //----------------/
     //- Constructors -/
    //----------------/

    public AlterarTipoRetornoLayoutRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.alterartiporetornolayout.request.AlterarTipoRetornoLayoutRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdArquivoRetornoPagamento
     * 
     */
    public void deleteCdArquivoRetornoPagamento()
    {
        this._has_cdArquivoRetornoPagamento= false;
    } //-- void deleteCdArquivoRetornoPagamento() 

    /**
     * Method deleteCdPessoaJuridicaContrato
     * 
     */
    public void deleteCdPessoaJuridicaContrato()
    {
        this._has_cdPessoaJuridicaContrato= false;
    } //-- void deleteCdPessoaJuridicaContrato() 

    /**
     * Method deleteCdTipoClassificacaoRetorno
     * 
     */
    public void deleteCdTipoClassificacaoRetorno()
    {
        this._has_cdTipoClassificacaoRetorno= false;
    } //-- void deleteCdTipoClassificacaoRetorno() 

    /**
     * Method deleteCdTipoContratoNegocio
     * 
     */
    public void deleteCdTipoContratoNegocio()
    {
        this._has_cdTipoContratoNegocio= false;
    } //-- void deleteCdTipoContratoNegocio() 

    /**
     * Method deleteCdTipoGeracaoRetorno
     * 
     */
    public void deleteCdTipoGeracaoRetorno()
    {
        this._has_cdTipoGeracaoRetorno= false;
    } //-- void deleteCdTipoGeracaoRetorno() 

    /**
     * Method deleteCdTipoLayoutArquivo
     * 
     */
    public void deleteCdTipoLayoutArquivo()
    {
        this._has_cdTipoLayoutArquivo= false;
    } //-- void deleteCdTipoLayoutArquivo() 

    /**
     * Method deleteCdTipoMontaRetorno
     * 
     */
    public void deleteCdTipoMontaRetorno()
    {
        this._has_cdTipoMontaRetorno= false;
    } //-- void deleteCdTipoMontaRetorno() 

    /**
     * Method deleteCdTipoOcorrenciaRetorno
     * 
     */
    public void deleteCdTipoOcorrenciaRetorno()
    {
        this._has_cdTipoOcorrenciaRetorno= false;
    } //-- void deleteCdTipoOcorrenciaRetorno() 

    /**
     * Method deleteCdTipoOrdemRegistroRetorno
     * 
     */
    public void deleteCdTipoOrdemRegistroRetorno()
    {
        this._has_cdTipoOrdemRegistroRetorno= false;
    } //-- void deleteCdTipoOrdemRegistroRetorno() 

    /**
     * Method deleteNrSequenciaContratoNegocio
     * 
     */
    public void deleteNrSequenciaContratoNegocio()
    {
        this._has_nrSequenciaContratoNegocio= false;
    } //-- void deleteNrSequenciaContratoNegocio() 

    /**
     * Returns the value of field 'cdArquivoRetornoPagamento'.
     * 
     * @return int
     * @return the value of field 'cdArquivoRetornoPagamento'.
     */
    public int getCdArquivoRetornoPagamento()
    {
        return this._cdArquivoRetornoPagamento;
    } //-- int getCdArquivoRetornoPagamento() 

    /**
     * Returns the value of field 'cdPessoaJuridicaContrato'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridicaContrato'.
     */
    public long getCdPessoaJuridicaContrato()
    {
        return this._cdPessoaJuridicaContrato;
    } //-- long getCdPessoaJuridicaContrato() 

    /**
     * Returns the value of field 'cdTipoClassificacaoRetorno'.
     * 
     * @return int
     * @return the value of field 'cdTipoClassificacaoRetorno'.
     */
    public int getCdTipoClassificacaoRetorno()
    {
        return this._cdTipoClassificacaoRetorno;
    } //-- int getCdTipoClassificacaoRetorno() 

    /**
     * Returns the value of field 'cdTipoContratoNegocio'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoNegocio'.
     */
    public int getCdTipoContratoNegocio()
    {
        return this._cdTipoContratoNegocio;
    } //-- int getCdTipoContratoNegocio() 

    /**
     * Returns the value of field 'cdTipoGeracaoRetorno'.
     * 
     * @return int
     * @return the value of field 'cdTipoGeracaoRetorno'.
     */
    public int getCdTipoGeracaoRetorno()
    {
        return this._cdTipoGeracaoRetorno;
    } //-- int getCdTipoGeracaoRetorno() 

    /**
     * Returns the value of field 'cdTipoLayoutArquivo'.
     * 
     * @return int
     * @return the value of field 'cdTipoLayoutArquivo'.
     */
    public int getCdTipoLayoutArquivo()
    {
        return this._cdTipoLayoutArquivo;
    } //-- int getCdTipoLayoutArquivo() 

    /**
     * Returns the value of field 'cdTipoMontaRetorno'.
     * 
     * @return int
     * @return the value of field 'cdTipoMontaRetorno'.
     */
    public int getCdTipoMontaRetorno()
    {
        return this._cdTipoMontaRetorno;
    } //-- int getCdTipoMontaRetorno() 

    /**
     * Returns the value of field 'cdTipoOcorrenciaRetorno'.
     * 
     * @return int
     * @return the value of field 'cdTipoOcorrenciaRetorno'.
     */
    public int getCdTipoOcorrenciaRetorno()
    {
        return this._cdTipoOcorrenciaRetorno;
    } //-- int getCdTipoOcorrenciaRetorno() 

    /**
     * Returns the value of field 'cdTipoOrdemRegistroRetorno'.
     * 
     * @return int
     * @return the value of field 'cdTipoOrdemRegistroRetorno'.
     */
    public int getCdTipoOrdemRegistroRetorno()
    {
        return this._cdTipoOrdemRegistroRetorno;
    } //-- int getCdTipoOrdemRegistroRetorno() 

    /**
     * Returns the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @return long
     * @return the value of field 'nrSequenciaContratoNegocio'.
     */
    public long getNrSequenciaContratoNegocio()
    {
        return this._nrSequenciaContratoNegocio;
    } //-- long getNrSequenciaContratoNegocio() 

    /**
     * Method hasCdArquivoRetornoPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdArquivoRetornoPagamento()
    {
        return this._has_cdArquivoRetornoPagamento;
    } //-- boolean hasCdArquivoRetornoPagamento() 

    /**
     * Method hasCdPessoaJuridicaContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridicaContrato()
    {
        return this._has_cdPessoaJuridicaContrato;
    } //-- boolean hasCdPessoaJuridicaContrato() 

    /**
     * Method hasCdTipoClassificacaoRetorno
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoClassificacaoRetorno()
    {
        return this._has_cdTipoClassificacaoRetorno;
    } //-- boolean hasCdTipoClassificacaoRetorno() 

    /**
     * Method hasCdTipoContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoNegocio()
    {
        return this._has_cdTipoContratoNegocio;
    } //-- boolean hasCdTipoContratoNegocio() 

    /**
     * Method hasCdTipoGeracaoRetorno
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoGeracaoRetorno()
    {
        return this._has_cdTipoGeracaoRetorno;
    } //-- boolean hasCdTipoGeracaoRetorno() 

    /**
     * Method hasCdTipoLayoutArquivo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoLayoutArquivo()
    {
        return this._has_cdTipoLayoutArquivo;
    } //-- boolean hasCdTipoLayoutArquivo() 

    /**
     * Method hasCdTipoMontaRetorno
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoMontaRetorno()
    {
        return this._has_cdTipoMontaRetorno;
    } //-- boolean hasCdTipoMontaRetorno() 

    /**
     * Method hasCdTipoOcorrenciaRetorno
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoOcorrenciaRetorno()
    {
        return this._has_cdTipoOcorrenciaRetorno;
    } //-- boolean hasCdTipoOcorrenciaRetorno() 

    /**
     * Method hasCdTipoOrdemRegistroRetorno
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoOrdemRegistroRetorno()
    {
        return this._has_cdTipoOrdemRegistroRetorno;
    } //-- boolean hasCdTipoOrdemRegistroRetorno() 

    /**
     * Method hasNrSequenciaContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaContratoNegocio()
    {
        return this._has_nrSequenciaContratoNegocio;
    } //-- boolean hasNrSequenciaContratoNegocio() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdArquivoRetornoPagamento'.
     * 
     * @param cdArquivoRetornoPagamento the value of field
     * 'cdArquivoRetornoPagamento'.
     */
    public void setCdArquivoRetornoPagamento(int cdArquivoRetornoPagamento)
    {
        this._cdArquivoRetornoPagamento = cdArquivoRetornoPagamento;
        this._has_cdArquivoRetornoPagamento = true;
    } //-- void setCdArquivoRetornoPagamento(int) 

    /**
     * Sets the value of field 'cdPessoaJuridicaContrato'.
     * 
     * @param cdPessoaJuridicaContrato the value of field
     * 'cdPessoaJuridicaContrato'.
     */
    public void setCdPessoaJuridicaContrato(long cdPessoaJuridicaContrato)
    {
        this._cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
        this._has_cdPessoaJuridicaContrato = true;
    } //-- void setCdPessoaJuridicaContrato(long) 

    /**
     * Sets the value of field 'cdTipoClassificacaoRetorno'.
     * 
     * @param cdTipoClassificacaoRetorno the value of field
     * 'cdTipoClassificacaoRetorno'.
     */
    public void setCdTipoClassificacaoRetorno(int cdTipoClassificacaoRetorno)
    {
        this._cdTipoClassificacaoRetorno = cdTipoClassificacaoRetorno;
        this._has_cdTipoClassificacaoRetorno = true;
    } //-- void setCdTipoClassificacaoRetorno(int) 

    /**
     * Sets the value of field 'cdTipoContratoNegocio'.
     * 
     * @param cdTipoContratoNegocio the value of field
     * 'cdTipoContratoNegocio'.
     */
    public void setCdTipoContratoNegocio(int cdTipoContratoNegocio)
    {
        this._cdTipoContratoNegocio = cdTipoContratoNegocio;
        this._has_cdTipoContratoNegocio = true;
    } //-- void setCdTipoContratoNegocio(int) 

    /**
     * Sets the value of field 'cdTipoGeracaoRetorno'.
     * 
     * @param cdTipoGeracaoRetorno the value of field
     * 'cdTipoGeracaoRetorno'.
     */
    public void setCdTipoGeracaoRetorno(int cdTipoGeracaoRetorno)
    {
        this._cdTipoGeracaoRetorno = cdTipoGeracaoRetorno;
        this._has_cdTipoGeracaoRetorno = true;
    } //-- void setCdTipoGeracaoRetorno(int) 

    /**
     * Sets the value of field 'cdTipoLayoutArquivo'.
     * 
     * @param cdTipoLayoutArquivo the value of field
     * 'cdTipoLayoutArquivo'.
     */
    public void setCdTipoLayoutArquivo(int cdTipoLayoutArquivo)
    {
        this._cdTipoLayoutArquivo = cdTipoLayoutArquivo;
        this._has_cdTipoLayoutArquivo = true;
    } //-- void setCdTipoLayoutArquivo(int) 

    /**
     * Sets the value of field 'cdTipoMontaRetorno'.
     * 
     * @param cdTipoMontaRetorno the value of field
     * 'cdTipoMontaRetorno'.
     */
    public void setCdTipoMontaRetorno(int cdTipoMontaRetorno)
    {
        this._cdTipoMontaRetorno = cdTipoMontaRetorno;
        this._has_cdTipoMontaRetorno = true;
    } //-- void setCdTipoMontaRetorno(int) 

    /**
     * Sets the value of field 'cdTipoOcorrenciaRetorno'.
     * 
     * @param cdTipoOcorrenciaRetorno the value of field
     * 'cdTipoOcorrenciaRetorno'.
     */
    public void setCdTipoOcorrenciaRetorno(int cdTipoOcorrenciaRetorno)
    {
        this._cdTipoOcorrenciaRetorno = cdTipoOcorrenciaRetorno;
        this._has_cdTipoOcorrenciaRetorno = true;
    } //-- void setCdTipoOcorrenciaRetorno(int) 

    /**
     * Sets the value of field 'cdTipoOrdemRegistroRetorno'.
     * 
     * @param cdTipoOrdemRegistroRetorno the value of field
     * 'cdTipoOrdemRegistroRetorno'.
     */
    public void setCdTipoOrdemRegistroRetorno(int cdTipoOrdemRegistroRetorno)
    {
        this._cdTipoOrdemRegistroRetorno = cdTipoOrdemRegistroRetorno;
        this._has_cdTipoOrdemRegistroRetorno = true;
    } //-- void setCdTipoOrdemRegistroRetorno(int) 

    /**
     * Sets the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @param nrSequenciaContratoNegocio the value of field
     * 'nrSequenciaContratoNegocio'.
     */
    public void setNrSequenciaContratoNegocio(long nrSequenciaContratoNegocio)
    {
        this._nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
        this._has_nrSequenciaContratoNegocio = true;
    } //-- void setNrSequenciaContratoNegocio(long) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return AlterarTipoRetornoLayoutRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.alterartiporetornolayout.request.AlterarTipoRetornoLayoutRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.alterartiporetornolayout.request.AlterarTipoRetornoLayoutRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.alterartiporetornolayout.request.AlterarTipoRetornoLayoutRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.alterartiporetornolayout.request.AlterarTipoRetornoLayoutRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
