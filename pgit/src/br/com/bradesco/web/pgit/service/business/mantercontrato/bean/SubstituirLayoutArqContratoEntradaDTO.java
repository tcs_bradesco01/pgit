/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantercontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantercontrato.bean;

import java.math.BigDecimal;

/**
 * Nome: SubstituirLayoutArqContratoEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class SubstituirLayoutArqContratoEntradaDTO {
	
	/** Atributo cdPessoaJuridicaContrato. */
	private Long cdPessoaJuridicaContrato;
	
	/** Atributo cdTipoContratoNegocio. */
	private Integer cdTipoContratoNegocio;
	
	/** Atributo nrSequenciaContratoNegocio. */
	private Long nrSequenciaContratoNegocio;
	
	/** Atributo cdTipoLayoutArquivo. */
	private Integer cdTipoLayoutArquivo;
	
	/** Atributo cdTipoLayoutArquivoNovo. */
	private Integer cdTipoLayoutArquivoNovo;
	
	/** Atributo cdSistema. */
	private String cdSistema;
	
	/** Atributo cdAplicacaoTransmissaoArquivo. */
	private Integer cdAplicacaoTransmissaoArquivo;
	
	/** Atributo cdSerieAplicacaoTransmissao. */
	private String cdSerieAplicacaoTransmissao;
	
	/** Atributo cdNivelControleRemessa. */
	private Integer cdNivelControleRemessa;
	
	/** Atributo cdControleNumeroRemessa. */
	private Integer cdControleNumeroRemessa;
	
	/** Atributo cdPerdcContagemRemessa. */
	private Integer cdPerdcContagemRemessa;
	
	/** Atributo nrMaximoContagemRemessa. */
	private Long nrMaximoContagemRemessa;
	
	/** Atributo cdRejeicaoAcolhimentoRemessa. */
	private Integer cdRejeicaoAcolhimentoRemessa;
	
	/** Atributo cdPercentualRejeicaoRemessa. */
	private BigDecimal cdPercentualRejeicaoRemessa;
	
	/** Atributo cdMeioPrincipalRetorno. */
	private Integer cdMeioPrincipalRetorno;
	
	/** Atributo cdMeioAlternativoRetorno. */
	private Integer cdMeioAlternativoRetorno;
	
	/** Atributo cdmeioAlternativoRemessa. */
	private Integer cdmeioAlternativoRemessa;
	
	/** Atributo cdMeioPrincipalRemessa. */
	private Integer cdMeioPrincipalRemessa;
	
	/** Atributo dsNomeCliente. */
	private String dsNomeCliente;
	
	/** Atributo dsNomeArquivoRemessa. */
	private String dsNomeArquivoRemessa;
    
    /** Atributo dsNomeArquivoRetorno. */
    private String dsNomeArquivoRetorno;	
    
    /** Atributo dsEmpresaTratamentoArquivo. */
    private String dsEmpresaTratamentoArquivo;
	
	/**
	 * Get: cdAplicacaoTransmissaoArquivo.
	 *
	 * @return cdAplicacaoTransmissaoArquivo
	 */
	public Integer getCdAplicacaoTransmissaoArquivo() {
		return cdAplicacaoTransmissaoArquivo;
	}
	
	/**
	 * Set: cdAplicacaoTransmissaoArquivo.
	 *
	 * @param cdAplicacaoTransmissaoArquivo the cd aplicacao transmissao arquivo
	 */
	public void setCdAplicacaoTransmissaoArquivo(
			Integer cdAplicacaoTransmissaoArquivo) {
		this.cdAplicacaoTransmissaoArquivo = cdAplicacaoTransmissaoArquivo;
	}
	
	/**
	 * Get: cdControleNumeroRemessa.
	 *
	 * @return cdControleNumeroRemessa
	 */
	public Integer getCdControleNumeroRemessa() {
		return cdControleNumeroRemessa;
	}
	
	/**
	 * Set: cdControleNumeroRemessa.
	 *
	 * @param cdControleNumeroRemessa the cd controle numero remessa
	 */
	public void setCdControleNumeroRemessa(Integer cdControleNumeroRemessa) {
		this.cdControleNumeroRemessa = cdControleNumeroRemessa;
	}
	
	/**
	 * Get: cdmeioAlternativoRemessa.
	 *
	 * @return cdmeioAlternativoRemessa
	 */
	public Integer getCdmeioAlternativoRemessa() {
		return cdmeioAlternativoRemessa;
	}
	
	/**
	 * Set: cdmeioAlternativoRemessa.
	 *
	 * @param cdmeioAlternativoRemessa the cdmeio alternativo remessa
	 */
	public void setCdmeioAlternativoRemessa(Integer cdmeioAlternativoRemessa) {
		this.cdmeioAlternativoRemessa = cdmeioAlternativoRemessa;
	}
	
	/**
	 * Get: cdMeioAlternativoRetorno.
	 *
	 * @return cdMeioAlternativoRetorno
	 */
	public Integer getCdMeioAlternativoRetorno() {
		return cdMeioAlternativoRetorno;
	}
	
	/**
	 * Set: cdMeioAlternativoRetorno.
	 *
	 * @param cdMeioAlternativoRetorno the cd meio alternativo retorno
	 */
	public void setCdMeioAlternativoRetorno(Integer cdMeioAlternativoRetorno) {
		this.cdMeioAlternativoRetorno = cdMeioAlternativoRetorno;
	}
	
	/**
	 * Get: cdMeioPrincipalRemessa.
	 *
	 * @return cdMeioPrincipalRemessa
	 */
	public Integer getCdMeioPrincipalRemessa() {
		return cdMeioPrincipalRemessa;
	}
	
	/**
	 * Set: cdMeioPrincipalRemessa.
	 *
	 * @param cdMeioPrincipalRemessa the cd meio principal remessa
	 */
	public void setCdMeioPrincipalRemessa(Integer cdMeioPrincipalRemessa) {
		this.cdMeioPrincipalRemessa = cdMeioPrincipalRemessa;
	}
	
	/**
	 * Get: cdMeioPrincipalRetorno.
	 *
	 * @return cdMeioPrincipalRetorno
	 */
	public Integer getCdMeioPrincipalRetorno() {
		return cdMeioPrincipalRetorno;
	}
	
	/**
	 * Set: cdMeioPrincipalRetorno.
	 *
	 * @param cdMeioPrincipalRetorno the cd meio principal retorno
	 */
	public void setCdMeioPrincipalRetorno(Integer cdMeioPrincipalRetorno) {
		this.cdMeioPrincipalRetorno = cdMeioPrincipalRetorno;
	}
	
	/**
	 * Get: cdNivelControleRemessa.
	 *
	 * @return cdNivelControleRemessa
	 */
	public Integer getCdNivelControleRemessa() {
		return cdNivelControleRemessa;
	}
	
	/**
	 * Set: cdNivelControleRemessa.
	 *
	 * @param cdNivelControleRemessa the cd nivel controle remessa
	 */
	public void setCdNivelControleRemessa(Integer cdNivelControleRemessa) {
		this.cdNivelControleRemessa = cdNivelControleRemessa;
	}
	
	/**
	 * Get: cdPercentualRejeicaoRemessa.
	 *
	 * @return cdPercentualRejeicaoRemessa
	 */
	public BigDecimal getCdPercentualRejeicaoRemessa() {
		return cdPercentualRejeicaoRemessa;
	}
	
	/**
	 * Set: cdPercentualRejeicaoRemessa.
	 *
	 * @param cdPercentualRejeicaoRemessa the cd percentual rejeicao remessa
	 */
	public void setCdPercentualRejeicaoRemessa(
			BigDecimal cdPercentualRejeicaoRemessa) {
		this.cdPercentualRejeicaoRemessa = cdPercentualRejeicaoRemessa;
	}
	
	/**
	 * Get: cdPerdcContagemRemessa.
	 *
	 * @return cdPerdcContagemRemessa
	 */
	public Integer getCdPerdcContagemRemessa() {
		return cdPerdcContagemRemessa;
	}
	
	/**
	 * Set: cdPerdcContagemRemessa.
	 *
	 * @param cdPerdcContagemRemessa the cd perdc contagem remessa
	 */
	public void setCdPerdcContagemRemessa(Integer cdPerdcContagemRemessa) {
		this.cdPerdcContagemRemessa = cdPerdcContagemRemessa;
	}
	
	/**
	 * Get: cdPessoaJuridicaContrato.
	 *
	 * @return cdPessoaJuridicaContrato
	 */
	public Long getCdPessoaJuridicaContrato() {
		return cdPessoaJuridicaContrato;
	}
	
	/**
	 * Set: cdPessoaJuridicaContrato.
	 *
	 * @param cdPessoaJuridicaContrato the cd pessoa juridica contrato
	 */
	public void setCdPessoaJuridicaContrato(Long cdPessoaJuridicaContrato) {
		this.cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
	}
	
	/**
	 * Get: cdRejeicaoAcolhimentoRemessa.
	 *
	 * @return cdRejeicaoAcolhimentoRemessa
	 */
	public Integer getCdRejeicaoAcolhimentoRemessa() {
		return cdRejeicaoAcolhimentoRemessa;
	}
	
	/**
	 * Set: cdRejeicaoAcolhimentoRemessa.
	 *
	 * @param cdRejeicaoAcolhimentoRemessa the cd rejeicao acolhimento remessa
	 */
	public void setCdRejeicaoAcolhimentoRemessa(Integer cdRejeicaoAcolhimentoRemessa) {
		this.cdRejeicaoAcolhimentoRemessa = cdRejeicaoAcolhimentoRemessa;
	}
	
	/**
	 * Get: cdSerieAplicacaoTransmissao.
	 *
	 * @return cdSerieAplicacaoTransmissao
	 */
	public String getCdSerieAplicacaoTransmissao() {
		return cdSerieAplicacaoTransmissao;
	}
	
	/**
	 * Set: cdSerieAplicacaoTransmissao.
	 *
	 * @param cdSerieAplicacaoTransmissao the cd serie aplicacao transmissao
	 */
	public void setCdSerieAplicacaoTransmissao(String cdSerieAplicacaoTransmissao) {
		this.cdSerieAplicacaoTransmissao = cdSerieAplicacaoTransmissao;
	}
	
	/**
	 * Get: cdSistema.
	 *
	 * @return cdSistema
	 */
	public String getCdSistema() {
		return cdSistema;
	}
	
	/**
	 * Set: cdSistema.
	 *
	 * @param cdSistema the cd sistema
	 */
	public void setCdSistema(String cdSistema) {
		this.cdSistema = cdSistema;
	}
	
	/**
	 * Get: cdTipoContratoNegocio.
	 *
	 * @return cdTipoContratoNegocio
	 */
	public Integer getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}
	
	/**
	 * Set: cdTipoContratoNegocio.
	 *
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}
	
	/**
	 * Get: cdTipoLayoutArquivo.
	 *
	 * @return cdTipoLayoutArquivo
	 */
	public Integer getCdTipoLayoutArquivo() {
		return cdTipoLayoutArquivo;
	}
	
	/**
	 * Set: cdTipoLayoutArquivo.
	 *
	 * @param cdTipoLayoutArquivo the cd tipo layout arquivo
	 */
	public void setCdTipoLayoutArquivo(Integer cdTipoLayoutArquivo) {
		this.cdTipoLayoutArquivo = cdTipoLayoutArquivo;
	}
	
	/**
	 * Get: cdTipoLayoutArquivoNovo.
	 *
	 * @return cdTipoLayoutArquivoNovo
	 */
	public Integer getCdTipoLayoutArquivoNovo() {
		return cdTipoLayoutArquivoNovo;
	}
	
	/**
	 * Set: cdTipoLayoutArquivoNovo.
	 *
	 * @param cdTipoLayoutArquivoNovo the cd tipo layout arquivo novo
	 */
	public void setCdTipoLayoutArquivoNovo(Integer cdTipoLayoutArquivoNovo) {
		this.cdTipoLayoutArquivoNovo = cdTipoLayoutArquivoNovo;
	}
	
	/**
	 * Get: dsNomeCliente.
	 *
	 * @return dsNomeCliente
	 */
	public String getDsNomeCliente() {
		return dsNomeCliente;
	}
	
	/**
	 * Set: dsNomeCliente.
	 *
	 * @param dsNomeCliente the ds nome cliente
	 */
	public void setDsNomeCliente(String dsNomeCliente) {
		this.dsNomeCliente = dsNomeCliente;
	}
	
	/**
	 * Get: nrMaximoContagemRemessa.
	 *
	 * @return nrMaximoContagemRemessa
	 */
	public Long getNrMaximoContagemRemessa() {
		return nrMaximoContagemRemessa;
	}
	
	/**
	 * Set: nrMaximoContagemRemessa.
	 *
	 * @param nrMaximoContagemRemessa the nr maximo contagem remessa
	 */
	public void setNrMaximoContagemRemessa(Long nrMaximoContagemRemessa) {
		this.nrMaximoContagemRemessa = nrMaximoContagemRemessa;
	}
	
	/**
	 * Get: nrSequenciaContratoNegocio.
	 *
	 * @return nrSequenciaContratoNegocio
	 */
	public Long getNrSequenciaContratoNegocio() {
		return nrSequenciaContratoNegocio;
	}
	
	/**
	 * Set: nrSequenciaContratoNegocio.
	 *
	 * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
	 */
	public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}
	
	/**
	 * Get: dsNomeArquivoRemessa.
	 *
	 * @return dsNomeArquivoRemessa
	 */
	public String getDsNomeArquivoRemessa() {
		return dsNomeArquivoRemessa;
	}
	
	/**
	 * Set: dsNomeArquivoRemessa.
	 *
	 * @param dsNomeArquivoRemessa the ds nome arquivo remessa
	 */
	public void setDsNomeArquivoRemessa(String dsNomeArquivoRemessa) {
		this.dsNomeArquivoRemessa = dsNomeArquivoRemessa;
	}
	
	/**
	 * Get: dsNomeArquivoRetorno.
	 *
	 * @return dsNomeArquivoRetorno
	 */
	public String getDsNomeArquivoRetorno() {
		return dsNomeArquivoRetorno;
	}
	
	/**
	 * Set: dsNomeArquivoRetorno.
	 *
	 * @param dsNomeArquivoRetorno the ds nome arquivo retorno
	 */
	public void setDsNomeArquivoRetorno(String dsNomeArquivoRetorno) {
		this.dsNomeArquivoRetorno = dsNomeArquivoRetorno;
	}
	
	/**
	 * Get: dsEmpresaTratamentoArquivo.
	 *
	 * @return dsEmpresaTratamentoArquivo
	 */
	public String getDsEmpresaTratamentoArquivo() {
		return dsEmpresaTratamentoArquivo;
	}
	
	/**
	 * Set: dsEmpresaTratamentoArquivo.
	 *
	 * @param dsEmpresaTratamentoArquivo the ds empresa tratamento arquivo
	 */
	public void setDsEmpresaTratamentoArquivo(String dsEmpresaTratamentoArquivo) {
		this.dsEmpresaTratamentoArquivo = dsEmpresaTratamentoArquivo;
	}
	

}
