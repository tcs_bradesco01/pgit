/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.excluirliquidacaopagamento.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ExcluirLiquidacaoPagamentoRequest.
 * 
 * @version $Revision$ $Date$
 */
public class ExcluirLiquidacaoPagamentoRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _numeroOcorrencias
     */
    private int _numeroOcorrencias = 0;

    /**
     * keeps track of state for field: _numeroOcorrencias
     */
    private boolean _has_numeroOcorrencias;

    /**
     * Field _cdFormaLiquidacao
     */
    private int _cdFormaLiquidacao = 0;

    /**
     * keeps track of state for field: _cdFormaLiquidacao
     */
    private boolean _has_cdFormaLiquidacao;

    /**
     * Field _dtInicioVigencia
     */
    private java.lang.String _dtInicioVigencia;

    /**
     * Field _dtFinalVigencia
     */
    private java.lang.String _dtFinalVigencia;

    /**
     * Field _hrInclusaoRegistroHistorico
     */
    private java.lang.String _hrInclusaoRegistroHistorico;

    /**
     * Field _hrConsultasSaldoPagamento
     */
    private java.lang.String _hrConsultasSaldoPagamento;

    /**
     * Field _hrConsultaFolhaPgto
     */
    private java.lang.String _hrConsultaFolhaPgto;


      //----------------/
     //- Constructors -/
    //----------------/

    public ExcluirLiquidacaoPagamentoRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.excluirliquidacaopagamento.request.ExcluirLiquidacaoPagamentoRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdFormaLiquidacao
     * 
     */
    public void deleteCdFormaLiquidacao()
    {
        this._has_cdFormaLiquidacao= false;
    } //-- void deleteCdFormaLiquidacao() 

    /**
     * Method deleteNumeroOcorrencias
     * 
     */
    public void deleteNumeroOcorrencias()
    {
        this._has_numeroOcorrencias= false;
    } //-- void deleteNumeroOcorrencias() 

    /**
     * Returns the value of field 'cdFormaLiquidacao'.
     * 
     * @return int
     * @return the value of field 'cdFormaLiquidacao'.
     */
    public int getCdFormaLiquidacao()
    {
        return this._cdFormaLiquidacao;
    } //-- int getCdFormaLiquidacao() 

    /**
     * Returns the value of field 'dtFinalVigencia'.
     * 
     * @return String
     * @return the value of field 'dtFinalVigencia'.
     */
    public java.lang.String getDtFinalVigencia()
    {
        return this._dtFinalVigencia;
    } //-- java.lang.String getDtFinalVigencia() 

    /**
     * Returns the value of field 'dtInicioVigencia'.
     * 
     * @return String
     * @return the value of field 'dtInicioVigencia'.
     */
    public java.lang.String getDtInicioVigencia()
    {
        return this._dtInicioVigencia;
    } //-- java.lang.String getDtInicioVigencia() 

    /**
     * Returns the value of field 'hrConsultaFolhaPgto'.
     * 
     * @return String
     * @return the value of field 'hrConsultaFolhaPgto'.
     */
    public java.lang.String getHrConsultaFolhaPgto()
    {
        return this._hrConsultaFolhaPgto;
    } //-- java.lang.String getHrConsultaFolhaPgto() 

    /**
     * Returns the value of field 'hrConsultasSaldoPagamento'.
     * 
     * @return String
     * @return the value of field 'hrConsultasSaldoPagamento'.
     */
    public java.lang.String getHrConsultasSaldoPagamento()
    {
        return this._hrConsultasSaldoPagamento;
    } //-- java.lang.String getHrConsultasSaldoPagamento() 

    /**
     * Returns the value of field 'hrInclusaoRegistroHistorico'.
     * 
     * @return String
     * @return the value of field 'hrInclusaoRegistroHistorico'.
     */
    public java.lang.String getHrInclusaoRegistroHistorico()
    {
        return this._hrInclusaoRegistroHistorico;
    } //-- java.lang.String getHrInclusaoRegistroHistorico() 

    /**
     * Returns the value of field 'numeroOcorrencias'.
     * 
     * @return int
     * @return the value of field 'numeroOcorrencias'.
     */
    public int getNumeroOcorrencias()
    {
        return this._numeroOcorrencias;
    } //-- int getNumeroOcorrencias() 

    /**
     * Method hasCdFormaLiquidacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFormaLiquidacao()
    {
        return this._has_cdFormaLiquidacao;
    } //-- boolean hasCdFormaLiquidacao() 

    /**
     * Method hasNumeroOcorrencias
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNumeroOcorrencias()
    {
        return this._has_numeroOcorrencias;
    } //-- boolean hasNumeroOcorrencias() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdFormaLiquidacao'.
     * 
     * @param cdFormaLiquidacao the value of field
     * 'cdFormaLiquidacao'.
     */
    public void setCdFormaLiquidacao(int cdFormaLiquidacao)
    {
        this._cdFormaLiquidacao = cdFormaLiquidacao;
        this._has_cdFormaLiquidacao = true;
    } //-- void setCdFormaLiquidacao(int) 

    /**
     * Sets the value of field 'dtFinalVigencia'.
     * 
     * @param dtFinalVigencia the value of field 'dtFinalVigencia'.
     */
    public void setDtFinalVigencia(java.lang.String dtFinalVigencia)
    {
        this._dtFinalVigencia = dtFinalVigencia;
    } //-- void setDtFinalVigencia(java.lang.String) 

    /**
     * Sets the value of field 'dtInicioVigencia'.
     * 
     * @param dtInicioVigencia the value of field 'dtInicioVigencia'
     */
    public void setDtInicioVigencia(java.lang.String dtInicioVigencia)
    {
        this._dtInicioVigencia = dtInicioVigencia;
    } //-- void setDtInicioVigencia(java.lang.String) 

    /**
     * Sets the value of field 'hrConsultaFolhaPgto'.
     * 
     * @param hrConsultaFolhaPgto the value of field
     * 'hrConsultaFolhaPgto'.
     */
    public void setHrConsultaFolhaPgto(java.lang.String hrConsultaFolhaPgto)
    {
        this._hrConsultaFolhaPgto = hrConsultaFolhaPgto;
    } //-- void setHrConsultaFolhaPgto(java.lang.String) 

    /**
     * Sets the value of field 'hrConsultasSaldoPagamento'.
     * 
     * @param hrConsultasSaldoPagamento the value of field
     * 'hrConsultasSaldoPagamento'.
     */
    public void setHrConsultasSaldoPagamento(java.lang.String hrConsultasSaldoPagamento)
    {
        this._hrConsultasSaldoPagamento = hrConsultasSaldoPagamento;
    } //-- void setHrConsultasSaldoPagamento(java.lang.String) 

    /**
     * Sets the value of field 'hrInclusaoRegistroHistorico'.
     * 
     * @param hrInclusaoRegistroHistorico the value of field
     * 'hrInclusaoRegistroHistorico'.
     */
    public void setHrInclusaoRegistroHistorico(java.lang.String hrInclusaoRegistroHistorico)
    {
        this._hrInclusaoRegistroHistorico = hrInclusaoRegistroHistorico;
    } //-- void setHrInclusaoRegistroHistorico(java.lang.String) 

    /**
     * Sets the value of field 'numeroOcorrencias'.
     * 
     * @param numeroOcorrencias the value of field
     * 'numeroOcorrencias'.
     */
    public void setNumeroOcorrencias(int numeroOcorrencias)
    {
        this._numeroOcorrencias = numeroOcorrencias;
        this._has_numeroOcorrencias = true;
    } //-- void setNumeroOcorrencias(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ExcluirLiquidacaoPagamentoRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.excluirliquidacaopagamento.request.ExcluirLiquidacaoPagamentoRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.excluirliquidacaopagamento.request.ExcluirLiquidacaoPagamentoRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.excluirliquidacaopagamento.request.ExcluirLiquidacaoPagamentoRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.excluirliquidacaopagamento.request.ExcluirLiquidacaoPagamentoRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
