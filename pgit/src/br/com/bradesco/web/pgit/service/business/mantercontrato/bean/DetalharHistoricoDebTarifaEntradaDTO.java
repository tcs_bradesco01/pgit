package br.com.bradesco.web.pgit.service.business.mantercontrato.bean;

public class DetalharHistoricoDebTarifaEntradaDTO {
	
    private Integer nrOcorrencias;
    private Long cdPessoaJuridicaNegocio;
    private Integer cdTipoContratoNegocio;
    private Long nrSequenciaContratoNegocio;
    private Integer cdProdutoServicoOperacao;
    private Integer cdProdutoOperacaoRelacionado;
    private Integer cdOperacaoProdutoServico;
    private String hrInclusaoRegistroHist;
    
	public Integer getNrOcorrencias() {
		return nrOcorrencias;
	}
	public void setNrOcorrencias(Integer nrOcorrencias) {
		this.nrOcorrencias = nrOcorrencias;
	}
	public Long getCdPessoaJuridicaNegocio() {
		return cdPessoaJuridicaNegocio;
	}
	public void setCdPessoaJuridicaNegocio(Long cdPessoaJuridicaNegocio) {
		this.cdPessoaJuridicaNegocio = cdPessoaJuridicaNegocio;
	}
	public Integer getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}
	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}
	public Long getNrSequenciaContratoNegocio() {
		return nrSequenciaContratoNegocio;
	}
	public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}
	public Integer getCdProdutoServicoOperacao() {
		return cdProdutoServicoOperacao;
	}
	public void setCdProdutoServicoOperacao(Integer cdProdutoServicoOperacao) {
		this.cdProdutoServicoOperacao = cdProdutoServicoOperacao;
	}
	public Integer getCdProdutoOperacaoRelacionado() {
		return cdProdutoOperacaoRelacionado;
	}
	public void setCdProdutoOperacaoRelacionado(Integer cdProdutoOperacaoRelacionado) {
		this.cdProdutoOperacaoRelacionado = cdProdutoOperacaoRelacionado;
	}
	public Integer getCdOperacaoProdutoServico() {
		return cdOperacaoProdutoServico;
	}
	public void setCdOperacaoProdutoServico(Integer cdOperacaoProdutoServico) {
		this.cdOperacaoProdutoServico = cdOperacaoProdutoServico;
	}
	public String getHrInclusaoRegistroHist() {
		return hrInclusaoRegistroHist;
	}
	public void setHrInclusaoRegistroHist(String hrInclusaoRegistroHist) {
		this.hrInclusaoRegistroHist = hrInclusaoRegistroHist;
	}

}
