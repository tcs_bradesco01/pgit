/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listartipolote.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdTipoLoteLayout
     */
    private int _cdTipoLoteLayout = 0;

    /**
     * keeps track of state for field: _cdTipoLoteLayout
     */
    private boolean _has_cdTipoLoteLayout;

    /**
     * Field _dsTipoLoteLayout
     */
    private java.lang.String _dsTipoLoteLayout;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listartipolote.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdTipoLoteLayout
     * 
     */
    public void deleteCdTipoLoteLayout()
    {
        this._has_cdTipoLoteLayout= false;
    } //-- void deleteCdTipoLoteLayout() 

    /**
     * Returns the value of field 'cdTipoLoteLayout'.
     * 
     * @return int
     * @return the value of field 'cdTipoLoteLayout'.
     */
    public int getCdTipoLoteLayout()
    {
        return this._cdTipoLoteLayout;
    } //-- int getCdTipoLoteLayout() 

    /**
     * Returns the value of field 'dsTipoLoteLayout'.
     * 
     * @return String
     * @return the value of field 'dsTipoLoteLayout'.
     */
    public java.lang.String getDsTipoLoteLayout()
    {
        return this._dsTipoLoteLayout;
    } //-- java.lang.String getDsTipoLoteLayout() 

    /**
     * Method hasCdTipoLoteLayout
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoLoteLayout()
    {
        return this._has_cdTipoLoteLayout;
    } //-- boolean hasCdTipoLoteLayout() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdTipoLoteLayout'.
     * 
     * @param cdTipoLoteLayout the value of field 'cdTipoLoteLayout'
     */
    public void setCdTipoLoteLayout(int cdTipoLoteLayout)
    {
        this._cdTipoLoteLayout = cdTipoLoteLayout;
        this._has_cdTipoLoteLayout = true;
    } //-- void setCdTipoLoteLayout(int) 

    /**
     * Sets the value of field 'dsTipoLoteLayout'.
     * 
     * @param dsTipoLoteLayout the value of field 'dsTipoLoteLayout'
     */
    public void setDsTipoLoteLayout(java.lang.String dsTipoLoteLayout)
    {
        this._dsTipoLoteLayout = dsTipoLoteLayout;
    } //-- void setDsTipoLoteLayout(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listartipolote.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listartipolote.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listartipolote.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listartipolote.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
