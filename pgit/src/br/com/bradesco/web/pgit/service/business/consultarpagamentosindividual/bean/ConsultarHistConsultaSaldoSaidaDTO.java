/*
 * Nome: br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean;

import java.math.BigDecimal;

/**
 * Nome: ConsultarHistConsultaSaldoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarHistConsultaSaldoSaidaDTO {
    
    /** Atributo codMensagem. */
    private String codMensagem;
    
    /** Atributo mensagem. */
    private String mensagem;
    
    /** Atributo dsBancoDebito. */
    private String dsBancoDebito;
    
    /** Atributo dsAgenciaDebito. */
    private String dsAgenciaDebito;
    
    /** Atributo dsTipoConta. */
    private String dsTipoConta;
    
    /** Atributo cdSituacaoContrato. */
    private Integer cdSituacaoContrato;
    
    /** Atributo dsSituacaoContrato. */
    private String dsSituacaoContrato;
    
    /** Atributo dsContrato. */
    private String dsContrato;
    
    /** Atributo dtVencimento. */
    private String dtVencimento;
    
    /** Atributo numeroConsultas. */
    private Integer numeroConsultas;
    
    /** Atributo hrConsultasSaldoPagamento. */
    private String hrConsultasSaldoPagamento;
    
    /** Atributo cdSituacao. */
    private Integer cdSituacao;
    
    /** Atributo cdValor. */
    private BigDecimal cdValor;
    
    /** Atributo cdSituacaoPagamentoCliente. */
    private Integer cdSituacaoPagamentoCliente;
    
    /** Atributo dsSituacaoPagamentoCliente. */
    private String dsSituacaoPagamentoCliente;
    
    /** Atributo cdMotivoPagamentoCliente. */
    private Integer cdMotivoPagamentoCliente;
    
    /** Atributo dsMotivoPagamentoCliente. */
    private String dsMotivoPagamentoCliente;
    
    /** Atributo cdBancoCredito. */
    private Integer cdBancoCredito;
    
    /** Atributo dsBancoCredito. */
    private String dsBancoCredito;
    
    /** Atributo cdAgenciaBancariaCredito. */
    private Integer cdAgenciaBancariaCredito;
    
    /** Atributo cdDigitoAgenciaCredito. */
    private Integer cdDigitoAgenciaCredito;
    
    /** Atributo dsAgenciaCredito. */
    private String dsAgenciaCredito;
    
    /** Atributo cdContaBancariaCredito. */
    private Long cdContaBancariaCredito;
    
    /** Atributo cdDigitoContaCredito. */
    private String cdDigitoContaCredito;
    
    /** Atributo cdTipoContaCredito. */
    private String cdTipoContaCredito;
    
    /** Atributo cdTipoInscricaoFavorecido. */
    private String cdTipoInscricaoFavorecido;
    
    /** Atributo inscricaoFavorecido. */
    private Long inscricaoFavorecido;
    
    /** Atributo sinal. */
    private String sinal;
 
   
    /** Atributo cdBancoCreditoFavorecido. */
    private Integer cdBancoCreditoFavorecido;
	
	/** Atributo dsBancoCreditoFavorecido. */
	private String dsBancoCreditoFavorecido;
	
	/** Atributo cdAgenciaBancariaCreditoFavorecido. */
	private Integer cdAgenciaBancariaCreditoFavorecido;
	
	/** Atributo cdDigitoAgenciaCreditoFavorecido. */
	private Integer cdDigitoAgenciaCreditoFavorecido;
	
	/** Atributo dsAgenciaCreditoFavorecido. */
	private String dsAgenciaCreditoFavorecido;	
	
	/** Atributo cdContaBancariaCreditoFavorecido. */
	private Long cdContaBancariaCreditoFavorecido ;
	
	/** Atributo cdDigitoContaCreditoFavorecido. */
	private String cdDigitoContaCreditoFavorecido;
	
	/** Atributo cdTipoContaCreditoFavorecido. */
	private String cdTipoContaCreditoFavorecido;
	
	/** Atributo bancoFavorecidoFormatado. */
	private String bancoFavorecidoFormatado;
	
	/** Atributo agenciaFavorecidoFormatada. */
	private String agenciaFavorecidoFormatada;
	
	/** Atributo contaFavorecidoFormatada. */
	private String contaFavorecidoFormatada;
	
	/** Atributo dataHoraFormatada. */
	private String dataHoraFormatada;

	/**
	 * Get: cdSituacao.
	 *
	 * @return cdSituacao
	 */
	public Integer getCdSituacao() {
		return cdSituacao;
	}

	/**
	 * Set: cdSituacao.
	 *
	 * @param cdSituacao the cd situacao
	 */
	public void setCdSituacao(Integer cdSituacao) {
		this.cdSituacao = cdSituacao;
	}

	/**
	 * Get: cdSituacaoContrato.
	 *
	 * @return cdSituacaoContrato
	 */
	public Integer getCdSituacaoContrato() {
		return cdSituacaoContrato;
	}

	/**
	 * Set: cdSituacaoContrato.
	 *
	 * @param cdSituacaoContrato the cd situacao contrato
	 */
	public void setCdSituacaoContrato(Integer cdSituacaoContrato) {
		this.cdSituacaoContrato = cdSituacaoContrato;
	}

	/**
	 * Get: cdValor.
	 *
	 * @return cdValor
	 */
	public BigDecimal getCdValor() {
		return cdValor;
	}

	/**
	 * Set: cdValor.
	 *
	 * @param cdValor the cd valor
	 */
	public void setCdValor(BigDecimal cdValor) {
		this.cdValor = cdValor;
	}

	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}

	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}

	/**
	 * Get: dataHoraFormatada.
	 *
	 * @return dataHoraFormatada
	 */
	public String getDataHoraFormatada() {
		return dataHoraFormatada;
	}

	/**
	 * Set: dataHoraFormatada.
	 *
	 * @param dataHoraFormatada the data hora formatada
	 */
	public void setDataHoraFormatada(String dataHoraFormatada) {
		this.dataHoraFormatada = dataHoraFormatada;
	}

	/**
	 * Get: dsAgenciaDebito.
	 *
	 * @return dsAgenciaDebito
	 */
	public String getDsAgenciaDebito() {
		return dsAgenciaDebito;
	}

	/**
	 * Set: dsAgenciaDebito.
	 *
	 * @param dsAgenciaDebito the ds agencia debito
	 */
	public void setDsAgenciaDebito(String dsAgenciaDebito) {
		this.dsAgenciaDebito = dsAgenciaDebito;
	}

	/**
	 * Get: dsBancoDebito.
	 *
	 * @return dsBancoDebito
	 */
	public String getDsBancoDebito() {
		return dsBancoDebito;
	}

	/**
	 * Set: dsBancoDebito.
	 *
	 * @param dsBancoDebito the ds banco debito
	 */
	public void setDsBancoDebito(String dsBancoDebito) {
		this.dsBancoDebito = dsBancoDebito;
	}

	/**
	 * Get: dsContrato.
	 *
	 * @return dsContrato
	 */
	public String getDsContrato() {
		return dsContrato;
	}

	/**
	 * Set: dsContrato.
	 *
	 * @param dsContrato the ds contrato
	 */
	public void setDsContrato(String dsContrato) {
		this.dsContrato = dsContrato;
	}

	/**
	 * Get: dsSituacaoContrato.
	 *
	 * @return dsSituacaoContrato
	 */
	public String getDsSituacaoContrato() {
		return dsSituacaoContrato;
	}

	/**
	 * Set: dsSituacaoContrato.
	 *
	 * @param dsSituacaoContrato the ds situacao contrato
	 */
	public void setDsSituacaoContrato(String dsSituacaoContrato) {
		this.dsSituacaoContrato = dsSituacaoContrato;
	}

	/**
	 * Get: dsTipoConta.
	 *
	 * @return dsTipoConta
	 */
	public String getDsTipoConta() {
		return dsTipoConta;
	}

	/**
	 * Set: dsTipoConta.
	 *
	 * @param dsTipoConta the ds tipo conta
	 */
	public void setDsTipoConta(String dsTipoConta) {
		this.dsTipoConta = dsTipoConta;
	}

	/**
	 * Get: dtVencimento.
	 *
	 * @return dtVencimento
	 */
	public String getDtVencimento() {
		return dtVencimento;
	}

	/**
	 * Set: dtVencimento.
	 *
	 * @param dtVencimento the dt vencimento
	 */
	public void setDtVencimento(String dtVencimento) {
		this.dtVencimento = dtVencimento;
	}

	/**
	 * Get: hrConsultasSaldoPagamento.
	 *
	 * @return hrConsultasSaldoPagamento
	 */
	public String getHrConsultasSaldoPagamento() {
		return hrConsultasSaldoPagamento;
	}

	/**
	 * Set: hrConsultasSaldoPagamento.
	 *
	 * @param hrConsultasSaldoPagamento the hr consultas saldo pagamento
	 */
	public void setHrConsultasSaldoPagamento(String hrConsultasSaldoPagamento) {
		this.hrConsultasSaldoPagamento = hrConsultasSaldoPagamento;
	}

	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}

	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	/**
	 * Get: numeroConsultas.
	 *
	 * @return numeroConsultas
	 */
	public Integer getNumeroConsultas() {
		return numeroConsultas;
	}

	/**
	 * Set: numeroConsultas.
	 *
	 * @param numeroConsultas the numero consultas
	 */
	public void setNumeroConsultas(Integer numeroConsultas) {
		this.numeroConsultas = numeroConsultas;
	}

	/**
	 * Get: cdAgenciaBancariaCredito.
	 *
	 * @return cdAgenciaBancariaCredito
	 */
	public Integer getCdAgenciaBancariaCredito() {
		return cdAgenciaBancariaCredito;
	}

	/**
	 * Set: cdAgenciaBancariaCredito.
	 *
	 * @param cdAgenciaBancariaCredito the cd agencia bancaria credito
	 */
	public void setCdAgenciaBancariaCredito(Integer cdAgenciaBancariaCredito) {
		this.cdAgenciaBancariaCredito = cdAgenciaBancariaCredito;
	}

	/**
	 * Get: cdBancoCredito.
	 *
	 * @return cdBancoCredito
	 */
	public Integer getCdBancoCredito() {
		return cdBancoCredito;
	}

	/**
	 * Set: cdBancoCredito.
	 *
	 * @param cdBancoCredito the cd banco credito
	 */
	public void setCdBancoCredito(Integer cdBancoCredito) {
		this.cdBancoCredito = cdBancoCredito;
	}

	/**
	 * Get: cdContaBancariaCredito.
	 *
	 * @return cdContaBancariaCredito
	 */
	public Long getCdContaBancariaCredito() {
		return cdContaBancariaCredito;
	}

	/**
	 * Set: cdContaBancariaCredito.
	 *
	 * @param cdContaBancariaCredito the cd conta bancaria credito
	 */
	public void setCdContaBancariaCredito(Long cdContaBancariaCredito) {
		this.cdContaBancariaCredito = cdContaBancariaCredito;
	}

	/**
	 * Get: cdDigitoAgenciaCredito.
	 *
	 * @return cdDigitoAgenciaCredito
	 */
	public Integer getCdDigitoAgenciaCredito() {
		return cdDigitoAgenciaCredito;
	}

	/**
	 * Set: cdDigitoAgenciaCredito.
	 *
	 * @param cdDigitoAgenciaCredito the cd digito agencia credito
	 */
	public void setCdDigitoAgenciaCredito(Integer cdDigitoAgenciaCredito) {
		this.cdDigitoAgenciaCredito = cdDigitoAgenciaCredito;
	}

	/**
	 * Get: cdDigitoContaCredito.
	 *
	 * @return cdDigitoContaCredito
	 */
	public String getCdDigitoContaCredito() {
		return cdDigitoContaCredito;
	}

	/**
	 * Set: cdDigitoContaCredito.
	 *
	 * @param cdDigitoContaCredito the cd digito conta credito
	 */
	public void setCdDigitoContaCredito(String cdDigitoContaCredito) {
		this.cdDigitoContaCredito = cdDigitoContaCredito;
	}

	/**
	 * Get: cdMotivoPagamentoCliente.
	 *
	 * @return cdMotivoPagamentoCliente
	 */
	public Integer getCdMotivoPagamentoCliente() {
		return cdMotivoPagamentoCliente;
	}

	/**
	 * Set: cdMotivoPagamentoCliente.
	 *
	 * @param cdMotivoPagamentoCliente the cd motivo pagamento cliente
	 */
	public void setCdMotivoPagamentoCliente(Integer cdMotivoPagamentoCliente) {
		this.cdMotivoPagamentoCliente = cdMotivoPagamentoCliente;
	}

	/**
	 * Get: cdSituacaoPagamentoCliente.
	 *
	 * @return cdSituacaoPagamentoCliente
	 */
	public Integer getCdSituacaoPagamentoCliente() {
		return cdSituacaoPagamentoCliente;
	}

	/**
	 * Set: cdSituacaoPagamentoCliente.
	 *
	 * @param cdSituacaoPagamentoCliente the cd situacao pagamento cliente
	 */
	public void setCdSituacaoPagamentoCliente(Integer cdSituacaoPagamentoCliente) {
		this.cdSituacaoPagamentoCliente = cdSituacaoPagamentoCliente;
	}

	/**
	 * Get: cdTipoContaCredito.
	 *
	 * @return cdTipoContaCredito
	 */
	public String getCdTipoContaCredito() {
		return cdTipoContaCredito;
	}

	/**
	 * Set: cdTipoContaCredito.
	 *
	 * @param cdTipoContaCredito the cd tipo conta credito
	 */
	public void setCdTipoContaCredito(String cdTipoContaCredito) {
		this.cdTipoContaCredito = cdTipoContaCredito;
	}

	/**
	 * Get: cdTipoInscricaoFavorecido.
	 *
	 * @return cdTipoInscricaoFavorecido
	 */
	public String getCdTipoInscricaoFavorecido() {
		return cdTipoInscricaoFavorecido;
	}

	/**
	 * Set: cdTipoInscricaoFavorecido.
	 *
	 * @param cdTipoInscricaoFavorecido the cd tipo inscricao favorecido
	 */
	public void setCdTipoInscricaoFavorecido(String cdTipoInscricaoFavorecido) {
		this.cdTipoInscricaoFavorecido = cdTipoInscricaoFavorecido;
	}

	/**
	 * Get: dsAgenciaCredito.
	 *
	 * @return dsAgenciaCredito
	 */
	public String getDsAgenciaCredito() {
		return dsAgenciaCredito;
	}

	/**
	 * Set: dsAgenciaCredito.
	 *
	 * @param dsAgenciaCredito the ds agencia credito
	 */
	public void setDsAgenciaCredito(String dsAgenciaCredito) {
		this.dsAgenciaCredito = dsAgenciaCredito;
	}

	/**
	 * Get: dsBancoCredito.
	 *
	 * @return dsBancoCredito
	 */
	public String getDsBancoCredito() {
		return dsBancoCredito;
	}

	/**
	 * Set: dsBancoCredito.
	 *
	 * @param dsBancoCredito the ds banco credito
	 */
	public void setDsBancoCredito(String dsBancoCredito) {
		this.dsBancoCredito = dsBancoCredito;
	}

	/**
	 * Get: dsMotivoPagamentoCliente.
	 *
	 * @return dsMotivoPagamentoCliente
	 */
	public String getDsMotivoPagamentoCliente() {
		return dsMotivoPagamentoCliente;
	}

	/**
	 * Set: dsMotivoPagamentoCliente.
	 *
	 * @param dsMotivoPagamentoCliente the ds motivo pagamento cliente
	 */
	public void setDsMotivoPagamentoCliente(String dsMotivoPagamentoCliente) {
		this.dsMotivoPagamentoCliente = dsMotivoPagamentoCliente;
	}

	/**
	 * Get: dsSituacaoPagamentoCliente.
	 *
	 * @return dsSituacaoPagamentoCliente
	 */
	public String getDsSituacaoPagamentoCliente() {
		return dsSituacaoPagamentoCliente;
	}

	/**
	 * Set: dsSituacaoPagamentoCliente.
	 *
	 * @param dsSituacaoPagamentoCliente the ds situacao pagamento cliente
	 */
	public void setDsSituacaoPagamentoCliente(String dsSituacaoPagamentoCliente) {
		this.dsSituacaoPagamentoCliente = dsSituacaoPagamentoCliente;
	}

	/**
	 * Get: inscricaoFavorecido.
	 *
	 * @return inscricaoFavorecido
	 */
	public Long getInscricaoFavorecido() {
		return inscricaoFavorecido;
	}

	/**
	 * Set: inscricaoFavorecido.
	 *
	 * @param inscricaoFavorecido the inscricao favorecido
	 */
	public void setInscricaoFavorecido(Long inscricaoFavorecido) {
		this.inscricaoFavorecido = inscricaoFavorecido;
	}

	/**
	 * Get: agenciaFavorecidoFormatada.
	 *
	 * @return agenciaFavorecidoFormatada
	 */
	public String getAgenciaFavorecidoFormatada() {
		return agenciaFavorecidoFormatada;
	}

	/**
	 * Set: agenciaFavorecidoFormatada.
	 *
	 * @param agenciaFavorecidoFormatada the agencia favorecido formatada
	 */
	public void setAgenciaFavorecidoFormatada(String agenciaFavorecidoFormatada) {
		this.agenciaFavorecidoFormatada = agenciaFavorecidoFormatada;
	}

	/**
	 * Get: bancoFavorecidoFormatado.
	 *
	 * @return bancoFavorecidoFormatado
	 */
	public String getBancoFavorecidoFormatado() {
		return bancoFavorecidoFormatado;
	}

	/**
	 * Set: bancoFavorecidoFormatado.
	 *
	 * @param bancoFavorecidoFormatado the banco favorecido formatado
	 */
	public void setBancoFavorecidoFormatado(String bancoFavorecidoFormatado) {
		this.bancoFavorecidoFormatado = bancoFavorecidoFormatado;
	}

	/**
	 * Get: cdAgenciaBancariaCreditoFavorecido.
	 *
	 * @return cdAgenciaBancariaCreditoFavorecido
	 */
	public Integer getCdAgenciaBancariaCreditoFavorecido() {
		return cdAgenciaBancariaCreditoFavorecido;
	}

	/**
	 * Set: cdAgenciaBancariaCreditoFavorecido.
	 *
	 * @param cdAgenciaBancariaCreditoFavorecido the cd agencia bancaria credito favorecido
	 */
	public void setCdAgenciaBancariaCreditoFavorecido(
			Integer cdAgenciaBancariaCreditoFavorecido) {
		this.cdAgenciaBancariaCreditoFavorecido = cdAgenciaBancariaCreditoFavorecido;
	}

	/**
	 * Get: cdBancoCreditoFavorecido.
	 *
	 * @return cdBancoCreditoFavorecido
	 */
	public Integer getCdBancoCreditoFavorecido() {
		return cdBancoCreditoFavorecido;
	}

	/**
	 * Set: cdBancoCreditoFavorecido.
	 *
	 * @param cdBancoCreditoFavorecido the cd banco credito favorecido
	 */
	public void setCdBancoCreditoFavorecido(Integer cdBancoCreditoFavorecido) {
		this.cdBancoCreditoFavorecido = cdBancoCreditoFavorecido;
	}

	/**
	 * Get: cdContaBancariaCreditoFavorecido.
	 *
	 * @return cdContaBancariaCreditoFavorecido
	 */
	public Long getCdContaBancariaCreditoFavorecido() {
		return cdContaBancariaCreditoFavorecido;
	}

	/**
	 * Set: cdContaBancariaCreditoFavorecido.
	 *
	 * @param cdContaBancariaCreditoFavorecido the cd conta bancaria credito favorecido
	 */
	public void setCdContaBancariaCreditoFavorecido(
			Long cdContaBancariaCreditoFavorecido) {
		this.cdContaBancariaCreditoFavorecido = cdContaBancariaCreditoFavorecido;
	}

	/**
	 * Get: cdDigitoAgenciaCreditoFavorecido.
	 *
	 * @return cdDigitoAgenciaCreditoFavorecido
	 */
	public Integer getCdDigitoAgenciaCreditoFavorecido() {
		return cdDigitoAgenciaCreditoFavorecido;
	}

	/**
	 * Set: cdDigitoAgenciaCreditoFavorecido.
	 *
	 * @param cdDigitoAgenciaCreditoFavorecido the cd digito agencia credito favorecido
	 */
	public void setCdDigitoAgenciaCreditoFavorecido(
			Integer cdDigitoAgenciaCreditoFavorecido) {
		this.cdDigitoAgenciaCreditoFavorecido = cdDigitoAgenciaCreditoFavorecido;
	}

	/**
	 * Get: cdDigitoContaCreditoFavorecido.
	 *
	 * @return cdDigitoContaCreditoFavorecido
	 */
	public String getCdDigitoContaCreditoFavorecido() {
		return cdDigitoContaCreditoFavorecido;
	}

	/**
	 * Set: cdDigitoContaCreditoFavorecido.
	 *
	 * @param cdDigitoContaCreditoFavorecido the cd digito conta credito favorecido
	 */
	public void setCdDigitoContaCreditoFavorecido(
			String cdDigitoContaCreditoFavorecido) {
		this.cdDigitoContaCreditoFavorecido = cdDigitoContaCreditoFavorecido;
	}

	/**
	 * Get: cdTipoContaCreditoFavorecido.
	 *
	 * @return cdTipoContaCreditoFavorecido
	 */
	public String getCdTipoContaCreditoFavorecido() {
		return cdTipoContaCreditoFavorecido;
	}

	/**
	 * Set: cdTipoContaCreditoFavorecido.
	 *
	 * @param cdTipoContaCreditoFavorecido the cd tipo conta credito favorecido
	 */
	public void setCdTipoContaCreditoFavorecido(String cdTipoContaCreditoFavorecido) {
		this.cdTipoContaCreditoFavorecido = cdTipoContaCreditoFavorecido;
	}

	/**
	 * Get: contaFavorecidoFormatada.
	 *
	 * @return contaFavorecidoFormatada
	 */
	public String getContaFavorecidoFormatada() {
		return contaFavorecidoFormatada;
	}

	/**
	 * Set: contaFavorecidoFormatada.
	 *
	 * @param contaFavorecidoFormatada the conta favorecido formatada
	 */
	public void setContaFavorecidoFormatada(String contaFavorecidoFormatada) {
		this.contaFavorecidoFormatada = contaFavorecidoFormatada;
	}

	/**
	 * Get: dsAgenciaCreditoFavorecido.
	 *
	 * @return dsAgenciaCreditoFavorecido
	 */
	public String getDsAgenciaCreditoFavorecido() {
		return dsAgenciaCreditoFavorecido;
	}

	/**
	 * Set: dsAgenciaCreditoFavorecido.
	 *
	 * @param dsAgenciaCreditoFavorecido the ds agencia credito favorecido
	 */
	public void setDsAgenciaCreditoFavorecido(String dsAgenciaCreditoFavorecido) {
		this.dsAgenciaCreditoFavorecido = dsAgenciaCreditoFavorecido;
	}

	/**
	 * Get: dsBancoCreditoFavorecido.
	 *
	 * @return dsBancoCreditoFavorecido
	 */
	public String getDsBancoCreditoFavorecido() {
		return dsBancoCreditoFavorecido;
	}

	/**
	 * Set: dsBancoCreditoFavorecido.
	 *
	 * @param dsBancoCreditoFavorecido the ds banco credito favorecido
	 */
	public void setDsBancoCreditoFavorecido(String dsBancoCreditoFavorecido) {
		this.dsBancoCreditoFavorecido = dsBancoCreditoFavorecido;
	}

	/**
	 * Get: sinal.
	 *
	 * @return sinal
	 */
	public String getSinal() {
		return sinal;
	}

	/**
	 * Set: sinal.
	 *
	 * @param sinal the sinal
	 */
	public void setSinal(String sinal) {
		this.sinal = sinal;
	}
}
