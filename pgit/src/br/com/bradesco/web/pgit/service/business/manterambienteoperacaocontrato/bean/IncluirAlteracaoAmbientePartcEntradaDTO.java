/*
 * Nome: br.com.bradesco.web.pgit.service.business.manterambienteoperacaocontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.manterambienteoperacaocontrato.bean;

/**
 * Nome: IncluirAlteracaoAmbientePartcEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class IncluirAlteracaoAmbientePartcEntradaDTO {
	
	/** Atributo cdpessoaJuridicaContrato. */
	private Long cdpessoaJuridicaContrato;
	
	/** Atributo cdTipoContratoNegocio. */
	private Integer cdTipoContratoNegocio;
	
	/** Atributo nrSequenciaContratoNegocio. */
	private Long nrSequenciaContratoNegocio;
	
	/** Atributo cdTipoParticipacaoPessoa. */
	private Integer cdTipoParticipacaoPessoa;
	
	/** Atributo cdPessoa. */
	private Long cdPessoa;
	
	/** Atributo cdProdutoServicoOperacao. */
	private Integer cdProdutoServicoOperacao;
	
	/** Atributo cdProdutoOperacaoRelacionado. */
	private Integer cdProdutoOperacaoRelacionado;
	
	/** Atributo cdRelacionamentoProduto. */
	private Integer cdRelacionamentoProduto;
	
	/** Atributo cdFormaMudanca. */
	private Integer cdFormaMudanca;
	
	/** Atributo dtMudancaAmbiente. */
	private String dtMudancaAmbiente;
	
	/** Atributo cdIndicadorArmazenamentoInformacao. */
	private Integer cdIndicadorArmazenamentoInformacao;
	
	/** Atributo cdAmbienteAtivoSolicitacao. */
	private String cdAmbienteAtivoSolicitacao;

	/**
	 * Get: cdpessoaJuridicaContrato.
	 *
	 * @return cdpessoaJuridicaContrato
	 */
	public Long getCdpessoaJuridicaContrato() {
		return cdpessoaJuridicaContrato;
	}

	/**
	 * Set: cdpessoaJuridicaContrato.
	 *
	 * @param cdpessoaJuridicaContrato the cdpessoa juridica contrato
	 */
	public void setCdpessoaJuridicaContrato(Long cdpessoaJuridicaContrato) {
		this.cdpessoaJuridicaContrato = cdpessoaJuridicaContrato;
	}

	/**
	 * Get: cdTipoContratoNegocio.
	 *
	 * @return cdTipoContratoNegocio
	 */
	public Integer getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}

	/**
	 * Set: cdTipoContratoNegocio.
	 *
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}

	/**
	 * Get: nrSequenciaContratoNegocio.
	 *
	 * @return nrSequenciaContratoNegocio
	 */
	public Long getNrSequenciaContratoNegocio() {
		return nrSequenciaContratoNegocio;
	}

	/**
	 * Set: nrSequenciaContratoNegocio.
	 *
	 * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
	 */
	public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}

	/**
	 * Get: cdTipoParticipacaoPessoa.
	 *
	 * @return cdTipoParticipacaoPessoa
	 */
	public Integer getCdTipoParticipacaoPessoa() {
		return cdTipoParticipacaoPessoa;
	}

	/**
	 * Set: cdTipoParticipacaoPessoa.
	 *
	 * @param cdTipoParticipacaoPessoa the cd tipo participacao pessoa
	 */
	public void setCdTipoParticipacaoPessoa(Integer cdTipoParticipacaoPessoa) {
		this.cdTipoParticipacaoPessoa = cdTipoParticipacaoPessoa;
	}

	/**
	 * Get: cdPessoa.
	 *
	 * @return cdPessoa
	 */
	public Long getCdPessoa() {
		return cdPessoa;
	}

	/**
	 * Set: cdPessoa.
	 *
	 * @param cdPessoa the cd pessoa
	 */
	public void setCdPessoa(Long cdPessoa) {
		this.cdPessoa = cdPessoa;
	}

	/**
	 * Get: cdProdutoServicoOperacao.
	 *
	 * @return cdProdutoServicoOperacao
	 */
	public Integer getCdProdutoServicoOperacao() {
		return cdProdutoServicoOperacao;
	}

	/**
	 * Set: cdProdutoServicoOperacao.
	 *
	 * @param cdProdutoServicoOperacao the cd produto servico operacao
	 */
	public void setCdProdutoServicoOperacao(Integer cdProdutoServicoOperacao) {
		this.cdProdutoServicoOperacao = cdProdutoServicoOperacao;
	}

	/**
	 * Get: cdProdutoOperacaoRelacionado.
	 *
	 * @return cdProdutoOperacaoRelacionado
	 */
	public Integer getCdProdutoOperacaoRelacionado() {
		return cdProdutoOperacaoRelacionado;
	}

	/**
	 * Set: cdProdutoOperacaoRelacionado.
	 *
	 * @param cdProdutoOperacaoRelacionado the cd produto operacao relacionado
	 */
	public void setCdProdutoOperacaoRelacionado(
			Integer cdProdutoOperacaoRelacionado) {
		this.cdProdutoOperacaoRelacionado = cdProdutoOperacaoRelacionado;
	}

	/**
	 * Get: cdRelacionamentoProduto.
	 *
	 * @return cdRelacionamentoProduto
	 */
	public Integer getCdRelacionamentoProduto() {
		return cdRelacionamentoProduto;
	}

	/**
	 * Set: cdRelacionamentoProduto.
	 *
	 * @param cdRelacionamentoProduto the cd relacionamento produto
	 */
	public void setCdRelacionamentoProduto(Integer cdRelacionamentoProduto) {
		this.cdRelacionamentoProduto = cdRelacionamentoProduto;
	}

	/**
	 * Get: cdFormaMudanca.
	 *
	 * @return cdFormaMudanca
	 */
	public Integer getCdFormaMudanca() {
		return cdFormaMudanca;
	}

	/**
	 * Set: cdFormaMudanca.
	 *
	 * @param cdFormaMudanca the cd forma mudanca
	 */
	public void setCdFormaMudanca(Integer cdFormaMudanca) {
		this.cdFormaMudanca = cdFormaMudanca;
	}

	/**
	 * Get: dtMudancaAmbiente.
	 *
	 * @return dtMudancaAmbiente
	 */
	public String getDtMudancaAmbiente() {
		return dtMudancaAmbiente;
	}

	/**
	 * Set: dtMudancaAmbiente.
	 *
	 * @param dtMudancaAmbiente the dt mudanca ambiente
	 */
	public void setDtMudancaAmbiente(String dtMudancaAmbiente) {
		this.dtMudancaAmbiente = dtMudancaAmbiente;
	}

	/**
	 * Get: cdIndicadorArmazenamentoInformacao.
	 *
	 * @return cdIndicadorArmazenamentoInformacao
	 */
	public Integer getCdIndicadorArmazenamentoInformacao() {
		return cdIndicadorArmazenamentoInformacao;
	}

	/**
	 * Set: cdIndicadorArmazenamentoInformacao.
	 *
	 * @param cdIndicadorArmazenamentoInformacao the cd indicador armazenamento informacao
	 */
	public void setCdIndicadorArmazenamentoInformacao(
			Integer cdIndicadorArmazenamentoInformacao) {
		this.cdIndicadorArmazenamentoInformacao = cdIndicadorArmazenamentoInformacao;
	}

	/**
	 * Get: cdAmbienteAtivoSolicitacao.
	 *
	 * @return cdAmbienteAtivoSolicitacao
	 */
	public String getCdAmbienteAtivoSolicitacao() {
		return cdAmbienteAtivoSolicitacao;
	}

	/**
	 * Set: cdAmbienteAtivoSolicitacao.
	 *
	 * @param cdAmbienteAtivoSolicitacao the cd ambiente ativo solicitacao
	 */
	public void setCdAmbienteAtivoSolicitacao(String cdAmbienteAtivoSolicitacao) {
		this.cdAmbienteAtivoSolicitacao = cdAmbienteAtivoSolicitacao;
	}
	
}
