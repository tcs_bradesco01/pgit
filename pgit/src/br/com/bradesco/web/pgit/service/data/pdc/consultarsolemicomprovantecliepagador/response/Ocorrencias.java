/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultarsolemicomprovantecliepagador.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdTipoSolicitacao
     */
    private int _cdTipoSolicitacao = 0;

    /**
     * keeps track of state for field: _cdTipoSolicitacao
     */
    private boolean _has_cdTipoSolicitacao;

    /**
     * Field _nrSolicitacao
     */
    private int _nrSolicitacao = 0;

    /**
     * keeps track of state for field: _nrSolicitacao
     */
    private boolean _has_nrSolicitacao;

    /**
     * Field _dtHoraSolicitacao
     */
    private java.lang.String _dtHoraSolicitacao;

    /**
     * Field _dtHoraAtendimento
     */
    private java.lang.String _dtHoraAtendimento;

    /**
     * Field _cdSituacaoSolicitacao
     */
    private int _cdSituacaoSolicitacao = 0;

    /**
     * keeps track of state for field: _cdSituacaoSolicitacao
     */
    private boolean _has_cdSituacaoSolicitacao;

    /**
     * Field _dsSituacaoSolicitacao
     */
    private java.lang.String _dsSituacaoSolicitacao;

    /**
     * Field _cdMotivoSituacao
     */
    private int _cdMotivoSituacao = 0;

    /**
     * keeps track of state for field: _cdMotivoSituacao
     */
    private boolean _has_cdMotivoSituacao;

    /**
     * Field _dsMotivoSituacao
     */
    private java.lang.String _dsMotivoSituacao;

    /**
     * Field _cdTipoCanal
     */
    private int _cdTipoCanal = 0;

    /**
     * keeps track of state for field: _cdTipoCanal
     */
    private boolean _has_cdTipoCanal;

    /**
     * Field _dsTipoCanal
     */
    private java.lang.String _dsTipoCanal;

    /**
     * Field _cdPessoaJuridica
     */
    private long _cdPessoaJuridica = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridica
     */
    private boolean _has_cdPessoaJuridica;

    /**
     * Field _cdTipoContrato
     */
    private int _cdTipoContrato = 0;

    /**
     * keeps track of state for field: _cdTipoContrato
     */
    private boolean _has_cdTipoContrato;

    /**
     * Field _nrSequenciaContrato
     */
    private long _nrSequenciaContrato = 0;

    /**
     * keeps track of state for field: _nrSequenciaContrato
     */
    private boolean _has_nrSequenciaContrato;

    /**
     * Field _cdUsuarioInclusao
     */
    private java.lang.String _cdUsuarioInclusao;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarsolemicomprovantecliepagador.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdMotivoSituacao
     * 
     */
    public void deleteCdMotivoSituacao()
    {
        this._has_cdMotivoSituacao= false;
    } //-- void deleteCdMotivoSituacao() 

    /**
     * Method deleteCdPessoaJuridica
     * 
     */
    public void deleteCdPessoaJuridica()
    {
        this._has_cdPessoaJuridica= false;
    } //-- void deleteCdPessoaJuridica() 

    /**
     * Method deleteCdSituacaoSolicitacao
     * 
     */
    public void deleteCdSituacaoSolicitacao()
    {
        this._has_cdSituacaoSolicitacao= false;
    } //-- void deleteCdSituacaoSolicitacao() 

    /**
     * Method deleteCdTipoCanal
     * 
     */
    public void deleteCdTipoCanal()
    {
        this._has_cdTipoCanal= false;
    } //-- void deleteCdTipoCanal() 

    /**
     * Method deleteCdTipoContrato
     * 
     */
    public void deleteCdTipoContrato()
    {
        this._has_cdTipoContrato= false;
    } //-- void deleteCdTipoContrato() 

    /**
     * Method deleteCdTipoSolicitacao
     * 
     */
    public void deleteCdTipoSolicitacao()
    {
        this._has_cdTipoSolicitacao= false;
    } //-- void deleteCdTipoSolicitacao() 

    /**
     * Method deleteNrSequenciaContrato
     * 
     */
    public void deleteNrSequenciaContrato()
    {
        this._has_nrSequenciaContrato= false;
    } //-- void deleteNrSequenciaContrato() 

    /**
     * Method deleteNrSolicitacao
     * 
     */
    public void deleteNrSolicitacao()
    {
        this._has_nrSolicitacao= false;
    } //-- void deleteNrSolicitacao() 

    /**
     * Returns the value of field 'cdMotivoSituacao'.
     * 
     * @return int
     * @return the value of field 'cdMotivoSituacao'.
     */
    public int getCdMotivoSituacao()
    {
        return this._cdMotivoSituacao;
    } //-- int getCdMotivoSituacao() 

    /**
     * Returns the value of field 'cdPessoaJuridica'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridica'.
     */
    public long getCdPessoaJuridica()
    {
        return this._cdPessoaJuridica;
    } //-- long getCdPessoaJuridica() 

    /**
     * Returns the value of field 'cdSituacaoSolicitacao'.
     * 
     * @return int
     * @return the value of field 'cdSituacaoSolicitacao'.
     */
    public int getCdSituacaoSolicitacao()
    {
        return this._cdSituacaoSolicitacao;
    } //-- int getCdSituacaoSolicitacao() 

    /**
     * Returns the value of field 'cdTipoCanal'.
     * 
     * @return int
     * @return the value of field 'cdTipoCanal'.
     */
    public int getCdTipoCanal()
    {
        return this._cdTipoCanal;
    } //-- int getCdTipoCanal() 

    /**
     * Returns the value of field 'cdTipoContrato'.
     * 
     * @return int
     * @return the value of field 'cdTipoContrato'.
     */
    public int getCdTipoContrato()
    {
        return this._cdTipoContrato;
    } //-- int getCdTipoContrato() 

    /**
     * Returns the value of field 'cdTipoSolicitacao'.
     * 
     * @return int
     * @return the value of field 'cdTipoSolicitacao'.
     */
    public int getCdTipoSolicitacao()
    {
        return this._cdTipoSolicitacao;
    } //-- int getCdTipoSolicitacao() 

    /**
     * Returns the value of field 'cdUsuarioInclusao'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioInclusao'.
     */
    public java.lang.String getCdUsuarioInclusao()
    {
        return this._cdUsuarioInclusao;
    } //-- java.lang.String getCdUsuarioInclusao() 

    /**
     * Returns the value of field 'dsMotivoSituacao'.
     * 
     * @return String
     * @return the value of field 'dsMotivoSituacao'.
     */
    public java.lang.String getDsMotivoSituacao()
    {
        return this._dsMotivoSituacao;
    } //-- java.lang.String getDsMotivoSituacao() 

    /**
     * Returns the value of field 'dsSituacaoSolicitacao'.
     * 
     * @return String
     * @return the value of field 'dsSituacaoSolicitacao'.
     */
    public java.lang.String getDsSituacaoSolicitacao()
    {
        return this._dsSituacaoSolicitacao;
    } //-- java.lang.String getDsSituacaoSolicitacao() 

    /**
     * Returns the value of field 'dsTipoCanal'.
     * 
     * @return String
     * @return the value of field 'dsTipoCanal'.
     */
    public java.lang.String getDsTipoCanal()
    {
        return this._dsTipoCanal;
    } //-- java.lang.String getDsTipoCanal() 

    /**
     * Returns the value of field 'dtHoraAtendimento'.
     * 
     * @return String
     * @return the value of field 'dtHoraAtendimento'.
     */
    public java.lang.String getDtHoraAtendimento()
    {
        return this._dtHoraAtendimento;
    } //-- java.lang.String getDtHoraAtendimento() 

    /**
     * Returns the value of field 'dtHoraSolicitacao'.
     * 
     * @return String
     * @return the value of field 'dtHoraSolicitacao'.
     */
    public java.lang.String getDtHoraSolicitacao()
    {
        return this._dtHoraSolicitacao;
    } //-- java.lang.String getDtHoraSolicitacao() 

    /**
     * Returns the value of field 'nrSequenciaContrato'.
     * 
     * @return long
     * @return the value of field 'nrSequenciaContrato'.
     */
    public long getNrSequenciaContrato()
    {
        return this._nrSequenciaContrato;
    } //-- long getNrSequenciaContrato() 

    /**
     * Returns the value of field 'nrSolicitacao'.
     * 
     * @return int
     * @return the value of field 'nrSolicitacao'.
     */
    public int getNrSolicitacao()
    {
        return this._nrSolicitacao;
    } //-- int getNrSolicitacao() 

    /**
     * Method hasCdMotivoSituacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMotivoSituacao()
    {
        return this._has_cdMotivoSituacao;
    } //-- boolean hasCdMotivoSituacao() 

    /**
     * Method hasCdPessoaJuridica
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridica()
    {
        return this._has_cdPessoaJuridica;
    } //-- boolean hasCdPessoaJuridica() 

    /**
     * Method hasCdSituacaoSolicitacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSituacaoSolicitacao()
    {
        return this._has_cdSituacaoSolicitacao;
    } //-- boolean hasCdSituacaoSolicitacao() 

    /**
     * Method hasCdTipoCanal
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoCanal()
    {
        return this._has_cdTipoCanal;
    } //-- boolean hasCdTipoCanal() 

    /**
     * Method hasCdTipoContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContrato()
    {
        return this._has_cdTipoContrato;
    } //-- boolean hasCdTipoContrato() 

    /**
     * Method hasCdTipoSolicitacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoSolicitacao()
    {
        return this._has_cdTipoSolicitacao;
    } //-- boolean hasCdTipoSolicitacao() 

    /**
     * Method hasNrSequenciaContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaContrato()
    {
        return this._has_nrSequenciaContrato;
    } //-- boolean hasNrSequenciaContrato() 

    /**
     * Method hasNrSolicitacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSolicitacao()
    {
        return this._has_nrSolicitacao;
    } //-- boolean hasNrSolicitacao() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdMotivoSituacao'.
     * 
     * @param cdMotivoSituacao the value of field 'cdMotivoSituacao'
     */
    public void setCdMotivoSituacao(int cdMotivoSituacao)
    {
        this._cdMotivoSituacao = cdMotivoSituacao;
        this._has_cdMotivoSituacao = true;
    } //-- void setCdMotivoSituacao(int) 

    /**
     * Sets the value of field 'cdPessoaJuridica'.
     * 
     * @param cdPessoaJuridica the value of field 'cdPessoaJuridica'
     */
    public void setCdPessoaJuridica(long cdPessoaJuridica)
    {
        this._cdPessoaJuridica = cdPessoaJuridica;
        this._has_cdPessoaJuridica = true;
    } //-- void setCdPessoaJuridica(long) 

    /**
     * Sets the value of field 'cdSituacaoSolicitacao'.
     * 
     * @param cdSituacaoSolicitacao the value of field
     * 'cdSituacaoSolicitacao'.
     */
    public void setCdSituacaoSolicitacao(int cdSituacaoSolicitacao)
    {
        this._cdSituacaoSolicitacao = cdSituacaoSolicitacao;
        this._has_cdSituacaoSolicitacao = true;
    } //-- void setCdSituacaoSolicitacao(int) 

    /**
     * Sets the value of field 'cdTipoCanal'.
     * 
     * @param cdTipoCanal the value of field 'cdTipoCanal'.
     */
    public void setCdTipoCanal(int cdTipoCanal)
    {
        this._cdTipoCanal = cdTipoCanal;
        this._has_cdTipoCanal = true;
    } //-- void setCdTipoCanal(int) 

    /**
     * Sets the value of field 'cdTipoContrato'.
     * 
     * @param cdTipoContrato the value of field 'cdTipoContrato'.
     */
    public void setCdTipoContrato(int cdTipoContrato)
    {
        this._cdTipoContrato = cdTipoContrato;
        this._has_cdTipoContrato = true;
    } //-- void setCdTipoContrato(int) 

    /**
     * Sets the value of field 'cdTipoSolicitacao'.
     * 
     * @param cdTipoSolicitacao the value of field
     * 'cdTipoSolicitacao'.
     */
    public void setCdTipoSolicitacao(int cdTipoSolicitacao)
    {
        this._cdTipoSolicitacao = cdTipoSolicitacao;
        this._has_cdTipoSolicitacao = true;
    } //-- void setCdTipoSolicitacao(int) 

    /**
     * Sets the value of field 'cdUsuarioInclusao'.
     * 
     * @param cdUsuarioInclusao the value of field
     * 'cdUsuarioInclusao'.
     */
    public void setCdUsuarioInclusao(java.lang.String cdUsuarioInclusao)
    {
        this._cdUsuarioInclusao = cdUsuarioInclusao;
    } //-- void setCdUsuarioInclusao(java.lang.String) 

    /**
     * Sets the value of field 'dsMotivoSituacao'.
     * 
     * @param dsMotivoSituacao the value of field 'dsMotivoSituacao'
     */
    public void setDsMotivoSituacao(java.lang.String dsMotivoSituacao)
    {
        this._dsMotivoSituacao = dsMotivoSituacao;
    } //-- void setDsMotivoSituacao(java.lang.String) 

    /**
     * Sets the value of field 'dsSituacaoSolicitacao'.
     * 
     * @param dsSituacaoSolicitacao the value of field
     * 'dsSituacaoSolicitacao'.
     */
    public void setDsSituacaoSolicitacao(java.lang.String dsSituacaoSolicitacao)
    {
        this._dsSituacaoSolicitacao = dsSituacaoSolicitacao;
    } //-- void setDsSituacaoSolicitacao(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoCanal'.
     * 
     * @param dsTipoCanal the value of field 'dsTipoCanal'.
     */
    public void setDsTipoCanal(java.lang.String dsTipoCanal)
    {
        this._dsTipoCanal = dsTipoCanal;
    } //-- void setDsTipoCanal(java.lang.String) 

    /**
     * Sets the value of field 'dtHoraAtendimento'.
     * 
     * @param dtHoraAtendimento the value of field
     * 'dtHoraAtendimento'.
     */
    public void setDtHoraAtendimento(java.lang.String dtHoraAtendimento)
    {
        this._dtHoraAtendimento = dtHoraAtendimento;
    } //-- void setDtHoraAtendimento(java.lang.String) 

    /**
     * Sets the value of field 'dtHoraSolicitacao'.
     * 
     * @param dtHoraSolicitacao the value of field
     * 'dtHoraSolicitacao'.
     */
    public void setDtHoraSolicitacao(java.lang.String dtHoraSolicitacao)
    {
        this._dtHoraSolicitacao = dtHoraSolicitacao;
    } //-- void setDtHoraSolicitacao(java.lang.String) 

    /**
     * Sets the value of field 'nrSequenciaContrato'.
     * 
     * @param nrSequenciaContrato the value of field
     * 'nrSequenciaContrato'.
     */
    public void setNrSequenciaContrato(long nrSequenciaContrato)
    {
        this._nrSequenciaContrato = nrSequenciaContrato;
        this._has_nrSequenciaContrato = true;
    } //-- void setNrSequenciaContrato(long) 

    /**
     * Sets the value of field 'nrSolicitacao'.
     * 
     * @param nrSolicitacao the value of field 'nrSolicitacao'.
     */
    public void setNrSolicitacao(int nrSolicitacao)
    {
        this._nrSolicitacao = nrSolicitacao;
        this._has_nrSolicitacao = true;
    } //-- void setNrSolicitacao(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultarsolemicomprovantecliepagador.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultarsolemicomprovantecliepagador.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultarsolemicomprovantecliepagador.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarsolemicomprovantecliepagador.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
