/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantercontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantercontrato.bean;

/**
 * Nome: ConsultarConManParticipantesSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarConManParticipantesSaidaDTO {
	
	/** Atributo codMensagem. */
	private String codMensagem;
	
	/** Atributo mensagem. */
	private String mensagem;
	
	/** Atributo cdTipoParticipacao. */
	private int cdTipoParticipacao;
	
	/** Atributo dsTipoParticipacao. */
	private String dsTipoParticipacao;
	
	/** Atributo cdCpfCnpj. */
	private long cdCpfCnpj;
	
	/** Atributo cdFilialCpfCnpj. */
	private int cdFilialCpfCnpj;
	
	/** Atributo cdControleCpfCnpj. */
	private int cdControleCpfCnpj;
	
	/** Atributo nome. */
	private String nome;
	
	/** Atributo cdGrupoEconomico. */
	private int cdGrupoEconomico;
	
	/** Atributo dsGrupoEconomico. */
	private String dsGrupoEconomico;
	
	/** Atributo cdAtividadeEconomica. */
	private int cdAtividadeEconomica;
	
	/** Atributo dsAtividadeEconomica. */
	private String dsAtividadeEconomica;
	
	/** Atributo cdSegCliente. */
	private int cdSegCliente;
	
	/** Atributo dsSegCliente. */
	private String dsSegCliente;
	
	/** Atributo cdSsgtoCliente. */
	private int cdSsgtoCliente;
	
	/** Atributo dsSsgtoCliente. */
	private String dsSsgtoCliente;
	
	/** Atributo cdIndicadorTipoManutencao. */
	private int cdIndicadorTipoManutencao;
	
	/** Atributo dsIndicadorTipoManutencao. */
	private String dsIndicadorTipoManutencao;
	
	/** Atributo hrInclusaoRegistro. */
	private String hrInclusaoRegistro;
	
	/** Atributo cdUsuarioInclusao. */
	private String cdUsuarioInclusao;
	
	/** Atributo cdUsuarioInclusaoExterno. */
	private String cdUsuarioInclusaoExterno;
	
	/** Atributo cdUsuarioManutencao. */
	private String cdUsuarioManutencao;
	
	/** Atributo cdUsuarioManutencaoExterno. */
	private String cdUsuarioManutencaoExterno;
	
	/** Atributo cdCanalManutencao. */
	private int cdCanalManutencao;
	
	/** Atributo cdCanalInclusao. */
	private int cdCanalInclusao;
	
	/** Atributo nrOperacaoFluxoInclusao. */
	private String nrOperacaoFluxoInclusao;
	
	/** Atributo hrManutencaoRegistro. */
	private String hrManutencaoRegistro;
	
	/** Atributo nrOperacaoFluxoManutencao. */
	private String nrOperacaoFluxoManutencao;
	
	/** Atributo dsCanalInclusao. */
	private String dsCanalInclusao;
	
	/** Atributo dsCanalManutencao. */
	private String dsCanalManutencao;
	
    /** Atributo cdMotivoSituacaoParticipante. */
    private Integer cdMotivoSituacaoParticipante;
    
    /** Atributo dsMotivoSituacaoParticipante. */
    private String dsMotivoSituacaoParticipante;
    
    /** Atributo cdClassificacaoAreaParticipante. */
    private Integer cdClassificacaoAreaParticipante;
    
    /** Atributo dsClassificacaoAreaParticipante. */
    private String dsClassificacaoAreaParticipante;	
	
	/** Atributo cpfFormatado. */
	private String cpfFormatado;
	
	/** Atributo dsSituacao. */
	private String dsSituacao;
	
	/** Atributo cdSituacao. */
	private int cdSituacao;
	
	
	
	/**
	 * Get: cdCanalInclusao.
	 *
	 * @return cdCanalInclusao
	 */
	public int getCdCanalInclusao() {
		return cdCanalInclusao;
	}
	
	/**
	 * Set: cdCanalInclusao.
	 *
	 * @param cdCanalInclusao the cd canal inclusao
	 */
	public void setCdCanalInclusao(int cdCanalInclusao) {
		this.cdCanalInclusao = cdCanalInclusao;
	}
	
	/**
	 * Get: cdUsuarioInclusaoExterno.
	 *
	 * @return cdUsuarioInclusaoExterno
	 */
	public String getCdUsuarioInclusaoExterno() {
		return cdUsuarioInclusaoExterno;
	}
	
	/**
	 * Set: cdUsuarioInclusaoExterno.
	 *
	 * @param cdUsuarioInclusaoExterno the cd usuario inclusao externo
	 */
	public void setCdUsuarioInclusaoExterno(String cdUsuarioInclusaoExterno) {
		this.cdUsuarioInclusaoExterno = cdUsuarioInclusaoExterno;
	}
	
	/**
	 * Get: cdUsuarioManutencao.
	 *
	 * @return cdUsuarioManutencao
	 */
	public String getCdUsuarioManutencao() {
		return cdUsuarioManutencao;
	}
	
	/**
	 * Set: cdUsuarioManutencao.
	 *
	 * @param cdUsuarioManutencao the cd usuario manutencao
	 */
	public void setCdUsuarioManutencao(String cdUsuarioManutencao) {
		this.cdUsuarioManutencao = cdUsuarioManutencao;
	}
	
	/**
	 * Get: hrManutencaoRegistro.
	 *
	 * @return hrManutencaoRegistro
	 */
	public String getHrManutencaoRegistro() {
		return hrManutencaoRegistro;
	}
	
	/**
	 * Set: hrManutencaoRegistro.
	 *
	 * @param hrManutencaoRegistro the hr manutencao registro
	 */
	public void setHrManutencaoRegistro(String hrManutencaoRegistro) {
		this.hrManutencaoRegistro = hrManutencaoRegistro;
	}
	
	/**
	 * Get: nrOperacaoFluxoInclusao.
	 *
	 * @return nrOperacaoFluxoInclusao
	 */
	public String getNrOperacaoFluxoInclusao() {
		return nrOperacaoFluxoInclusao;
	}
	
	/**
	 * Set: nrOperacaoFluxoInclusao.
	 *
	 * @param nrOperacaoFluxoInclusao the nr operacao fluxo inclusao
	 */
	public void setNrOperacaoFluxoInclusao(String nrOperacaoFluxoInclusao) {
		this.nrOperacaoFluxoInclusao = nrOperacaoFluxoInclusao;
	}
	
	/**
	 * Get: cpfFormatado.
	 *
	 * @return cpfFormatado
	 */
	public String getCpfFormatado() {
		return cpfFormatado;
	}
	
	/**
	 * Set: cpfFormatado.
	 *
	 * @param cpfFormatado the cpf formatado
	 */
	public void setCpfFormatado(String cpfFormatado) {
		this.cpfFormatado = cpfFormatado;
	}
	
	/**
	 * Get: cdAtividadeEconomica.
	 *
	 * @return cdAtividadeEconomica
	 */
	public int getCdAtividadeEconomica() {
		return cdAtividadeEconomica;
	}
	
	/**
	 * Set: cdAtividadeEconomica.
	 *
	 * @param cdAtividadeEconomica the cd atividade economica
	 */
	public void setCdAtividadeEconomica(int cdAtividadeEconomica) {
		this.cdAtividadeEconomica = cdAtividadeEconomica;
	}
	
	/**
	 * Get: cdCanalManutencao.
	 *
	 * @return cdCanalManutencao
	 */
	public int getCdCanalManutencao() {
		return cdCanalManutencao;
	}
	
	/**
	 * Set: cdCanalManutencao.
	 *
	 * @param cdCanalManutencao the cd canal manutencao
	 */
	public void setCdCanalManutencao(int cdCanalManutencao) {
		this.cdCanalManutencao = cdCanalManutencao;
	}
	
	/**
	 * Get: cdControleCpfCnpj.
	 *
	 * @return cdControleCpfCnpj
	 */
	public int getCdControleCpfCnpj() {
		return cdControleCpfCnpj;
	}
	
	/**
	 * Set: cdControleCpfCnpj.
	 *
	 * @param cdControleCpfCnpj the cd controle cpf cnpj
	 */
	public void setCdControleCpfCnpj(int cdControleCpfCnpj) {
		this.cdControleCpfCnpj = cdControleCpfCnpj;
	}
	
	/**
	 * Get: cdCpfCnpj.
	 *
	 * @return cdCpfCnpj
	 */
	public long getCdCpfCnpj() {
		return cdCpfCnpj;
	}
	
	/**
	 * Set: cdCpfCnpj.
	 *
	 * @param cdCpfCnpj the cd cpf cnpj
	 */
	public void setCdCpfCnpj(long cdCpfCnpj) {
		this.cdCpfCnpj = cdCpfCnpj;
	}
	
	/**
	 * Get: cdFilialCpfCnpj.
	 *
	 * @return cdFilialCpfCnpj
	 */
	public int getCdFilialCpfCnpj() {
		return cdFilialCpfCnpj;
	}
	
	/**
	 * Set: cdFilialCpfCnpj.
	 *
	 * @param cdFilialCpfCnpj the cd filial cpf cnpj
	 */
	public void setCdFilialCpfCnpj(int cdFilialCpfCnpj) {
		this.cdFilialCpfCnpj = cdFilialCpfCnpj;
	}
	
	/**
	 * Get: cdGrupoEconomico.
	 *
	 * @return cdGrupoEconomico
	 */
	public int getCdGrupoEconomico() {
		return cdGrupoEconomico;
	}
	
	/**
	 * Set: cdGrupoEconomico.
	 *
	 * @param cdGrupoEconomico the cd grupo economico
	 */
	public void setCdGrupoEconomico(int cdGrupoEconomico) {
		this.cdGrupoEconomico = cdGrupoEconomico;
	}
	
	/**
	 * Get: cdIndicadorTipoManutencao.
	 *
	 * @return cdIndicadorTipoManutencao
	 */
	public int getCdIndicadorTipoManutencao() {
		return cdIndicadorTipoManutencao;
	}
	
	/**
	 * Set: cdIndicadorTipoManutencao.
	 *
	 * @param cdIndicadorTipoManutencao the cd indicador tipo manutencao
	 */
	public void setCdIndicadorTipoManutencao(int cdIndicadorTipoManutencao) {
		this.cdIndicadorTipoManutencao = cdIndicadorTipoManutencao;
	}
	
	/**
	 * Get: cdSegCliente.
	 *
	 * @return cdSegCliente
	 */
	public int getCdSegCliente() {
		return cdSegCliente;
	}
	
	/**
	 * Set: cdSegCliente.
	 *
	 * @param cdSegCliente the cd seg cliente
	 */
	public void setCdSegCliente(int cdSegCliente) {
		this.cdSegCliente = cdSegCliente;
	}
	
	/**
	 * Get: cdSsgtoCliente.
	 *
	 * @return cdSsgtoCliente
	 */
	public int getCdSsgtoCliente() {
		return cdSsgtoCliente;
	}
	
	/**
	 * Set: cdSsgtoCliente.
	 *
	 * @param cdSsgtoCliente the cd ssgto cliente
	 */
	public void setCdSsgtoCliente(int cdSsgtoCliente) {
		this.cdSsgtoCliente = cdSsgtoCliente;
	}
	
	/**
	 * Get: cdTipoParticipacao.
	 *
	 * @return cdTipoParticipacao
	 */
	public int getCdTipoParticipacao() {
		return cdTipoParticipacao;
	}
	
	/**
	 * Set: cdTipoParticipacao.
	 *
	 * @param cdTipoParticipacao the cd tipo participacao
	 */
	public void setCdTipoParticipacao(int cdTipoParticipacao) {
		this.cdTipoParticipacao = cdTipoParticipacao;
	}
	
	/**
	 * Get: cdUsuarioInclusao.
	 *
	 * @return cdUsuarioInclusao
	 */
	public String getCdUsuarioInclusao() {
		return cdUsuarioInclusao;
	}
	
	/**
	 * Set: cdUsuarioInclusao.
	 *
	 * @param cdUsuarioInclusao the cd usuario inclusao
	 */
	public void setCdUsuarioInclusao(String cdUsuarioInclusao) {
		this.cdUsuarioInclusao = cdUsuarioInclusao;
	}
	
	/**
	 * Get: cdUsuarioManutencaoExterno.
	 *
	 * @return cdUsuarioManutencaoExterno
	 */
	public String getCdUsuarioManutencaoExterno() {
		return cdUsuarioManutencaoExterno;
	}
	
	/**
	 * Set: cdUsuarioManutencaoExterno.
	 *
	 * @param cdUsuarioManutencaoExterno the cd usuario manutencao externo
	 */
	public void setCdUsuarioManutencaoExterno(String cdUsuarioManutencaoExterno) {
		this.cdUsuarioManutencaoExterno = cdUsuarioManutencaoExterno;
	}
	
	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}
	
	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}
	
	/**
	 * Get: dsAtividadeEconomica.
	 *
	 * @return dsAtividadeEconomica
	 */
	public String getDsAtividadeEconomica() {
		return dsAtividadeEconomica;
	}
	
	/**
	 * Set: dsAtividadeEconomica.
	 *
	 * @param dsAtividadeEconomica the ds atividade economica
	 */
	public void setDsAtividadeEconomica(String dsAtividadeEconomica) {
		this.dsAtividadeEconomica = dsAtividadeEconomica;
	}
	
	/**
	 * Get: dsCanalInclusao.
	 *
	 * @return dsCanalInclusao
	 */
	public String getDsCanalInclusao() {
		return dsCanalInclusao;
	}
	
	/**
	 * Set: dsCanalInclusao.
	 *
	 * @param dsCanalInclusao the ds canal inclusao
	 */
	public void setDsCanalInclusao(String dsCanalInclusao) {
		this.dsCanalInclusao = dsCanalInclusao;
	}
	
	/**
	 * Get: dsCanalManutencao.
	 *
	 * @return dsCanalManutencao
	 */
	public String getDsCanalManutencao() {
		return dsCanalManutencao;
	}
	
	/**
	 * Set: dsCanalManutencao.
	 *
	 * @param dsCanalManutencao the ds canal manutencao
	 */
	public void setDsCanalManutencao(String dsCanalManutencao) {
		this.dsCanalManutencao = dsCanalManutencao;
	}
	
	/**
	 * Get: dsGrupoEconomico.
	 *
	 * @return dsGrupoEconomico
	 */
	public String getDsGrupoEconomico() {
		return dsGrupoEconomico;
	}
	
	/**
	 * Set: dsGrupoEconomico.
	 *
	 * @param dsGrupoEconomico the ds grupo economico
	 */
	public void setDsGrupoEconomico(String dsGrupoEconomico) {
		this.dsGrupoEconomico = dsGrupoEconomico;
	}
	
	/**
	 * Get: dsIndicadorTipoManutencao.
	 *
	 * @return dsIndicadorTipoManutencao
	 */
	public String getDsIndicadorTipoManutencao() {
		return dsIndicadorTipoManutencao;
	}
	
	/**
	 * Set: dsIndicadorTipoManutencao.
	 *
	 * @param dsIndicadorTipoManutencao the ds indicador tipo manutencao
	 */
	public void setDsIndicadorTipoManutencao(String dsIndicadorTipoManutencao) {
		this.dsIndicadorTipoManutencao = dsIndicadorTipoManutencao;
	}
	
	/**
	 * Get: dsSegCliente.
	 *
	 * @return dsSegCliente
	 */
	public String getDsSegCliente() {
		return dsSegCliente;
	}
	
	/**
	 * Set: dsSegCliente.
	 *
	 * @param dsSegCliente the ds seg cliente
	 */
	public void setDsSegCliente(String dsSegCliente) {
		this.dsSegCliente = dsSegCliente;
	}
	
	/**
	 * Get: dsSsgtoCliente.
	 *
	 * @return dsSsgtoCliente
	 */
	public String getDsSsgtoCliente() {
		return dsSsgtoCliente;
	}
	
	/**
	 * Set: dsSsgtoCliente.
	 *
	 * @param dsSsgtoCliente the ds ssgto cliente
	 */
	public void setDsSsgtoCliente(String dsSsgtoCliente) {
		this.dsSsgtoCliente = dsSsgtoCliente;
	}
	
	/**
	 * Get: dsTipoParticipacao.
	 *
	 * @return dsTipoParticipacao
	 */
	public String getDsTipoParticipacao() {
		return dsTipoParticipacao;
	}
	
	/**
	 * Set: dsTipoParticipacao.
	 *
	 * @param dsTipoParticipacao the ds tipo participacao
	 */
	public void setDsTipoParticipacao(String dsTipoParticipacao) {
		this.dsTipoParticipacao = dsTipoParticipacao;
	}
	
	/**
	 * Get: hrInclusaoRegistro.
	 *
	 * @return hrInclusaoRegistro
	 */
	public String getHrInclusaoRegistro() {
		return hrInclusaoRegistro;
	}
	
	/**
	 * Set: hrInclusaoRegistro.
	 *
	 * @param hrInclusaoRegistro the hr inclusao registro
	 */
	public void setHrInclusaoRegistro(String hrInclusaoRegistro) {
		this.hrInclusaoRegistro = hrInclusaoRegistro;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	/**
	 * Get: nome.
	 *
	 * @return nome
	 */
	public String getNome() {
		return nome;
	}
	
	/**
	 * Set: nome.
	 *
	 * @param nome the nome
	 */
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	/**
	 * Get: nrOperacaoFluxoManutencao.
	 *
	 * @return nrOperacaoFluxoManutencao
	 */
	public String getNrOperacaoFluxoManutencao() {
		return nrOperacaoFluxoManutencao;
	}
	
	/**
	 * Set: nrOperacaoFluxoManutencao.
	 *
	 * @param nrOperacaoFluxoManutencao the nr operacao fluxo manutencao
	 */
	public void setNrOperacaoFluxoManutencao(String nrOperacaoFluxoManutencao) {
		this.nrOperacaoFluxoManutencao = nrOperacaoFluxoManutencao;
	}
	
	/**
	 * Get: cdSituacao.
	 *
	 * @return cdSituacao
	 */
	public int getCdSituacao() {
		return cdSituacao;
	}
	
	/**
	 * Set: cdSituacao.
	 *
	 * @param cdSituacao the cd situacao
	 */
	public void setCdSituacao(int cdSituacao) {
		this.cdSituacao = cdSituacao;
	}
	
	/**
	 * Get: dsSituacao.
	 *
	 * @return dsSituacao
	 */
	public String getDsSituacao() {
		return dsSituacao;
	}
	
	/**
	 * Set: dsSituacao.
	 *
	 * @param dsSituacao the ds situacao
	 */
	public void setDsSituacao(String dsSituacao) {
		this.dsSituacao = dsSituacao;
	}
	
	/**
	 * Get: cdClassificacaoAreaParticipante.
	 *
	 * @return cdClassificacaoAreaParticipante
	 */
	public Integer getCdClassificacaoAreaParticipante() {
		return cdClassificacaoAreaParticipante;
	}
	
	/**
	 * Set: cdClassificacaoAreaParticipante.
	 *
	 * @param cdClassificacaoAreaParticipante the cd classificacao area participante
	 */
	public void setCdClassificacaoAreaParticipante(
			Integer cdClassificacaoAreaParticipante) {
		this.cdClassificacaoAreaParticipante = cdClassificacaoAreaParticipante;
	}
	
	/**
	 * Get: cdMotivoSituacaoParticipante.
	 *
	 * @return cdMotivoSituacaoParticipante
	 */
	public Integer getCdMotivoSituacaoParticipante() {
		return cdMotivoSituacaoParticipante;
	}
	
	/**
	 * Set: cdMotivoSituacaoParticipante.
	 *
	 * @param cdMotivoSituacaoParticipante the cd motivo situacao participante
	 */
	public void setCdMotivoSituacaoParticipante(Integer cdMotivoSituacaoParticipante) {
		this.cdMotivoSituacaoParticipante = cdMotivoSituacaoParticipante;
	}
	
	/**
	 * Get: dsClassificacaoAreaParticipante.
	 *
	 * @return dsClassificacaoAreaParticipante
	 */
	public String getDsClassificacaoAreaParticipante() {
		return dsClassificacaoAreaParticipante;
	}
	
	/**
	 * Set: dsClassificacaoAreaParticipante.
	 *
	 * @param dsClassificacaoAreaParticipante the ds classificacao area participante
	 */
	public void setDsClassificacaoAreaParticipante(
			String dsClassificacaoAreaParticipante) {
		this.dsClassificacaoAreaParticipante = dsClassificacaoAreaParticipante;
	}
	
	/**
	 * Get: dsMotivoSituacaoParticipante.
	 *
	 * @return dsMotivoSituacaoParticipante
	 */
	public String getDsMotivoSituacaoParticipante() {
		return dsMotivoSituacaoParticipante;
	}
	
	/**
	 * Set: dsMotivoSituacaoParticipante.
	 *
	 * @param dsMotivoSituacaoParticipante the ds motivo situacao participante
	 */
	public void setDsMotivoSituacaoParticipante(String dsMotivoSituacaoParticipante) {
		this.dsMotivoSituacaoParticipante = dsMotivoSituacaoParticipante;
	}
	
	
}