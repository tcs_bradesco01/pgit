/**
 * Nome: br.com.bradesco.web.pgit.service.business.consultaautorizante
 * Compilador: JDK 1.5
 * Prop�sito: INSERIR O PROP�SITO DAS CLASSES DO PACOTE
 * Data da cria��o: <dd/MM/yyyy>
 * Par�metros de compila��o: -d
 */
package br.com.bradesco.web.pgit.service.business.consultaautorizante;

import br.com.bradesco.web.pgit.service.business.consultaautorizante.bean.ConsultarAutorizantesNetEmpresaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultaautorizante.bean.ConsultarAutorizantesNetEmpresaSaidaDTO;

/**
 * Nome: IConsultaAutorizanteService
 * <p>
 * Prop�sito: Interface do adaptador ConsultaAutorizante
 * </p>
 * 
 * @author CPM Braxis / TI Melhorias - Arquitetura
 * @version 1.0
 */
public interface IConsultaAutorizanteService {
    
    /**
     * Consultar autorizantes net empresa.
     *
     * @param entrada the entrada
     * @return the consultar autorizantes net empresa saida dto
     */
    public ConsultarAutorizantesNetEmpresaSaidaDTO consultarAutorizantesNetEmpresa(ConsultarAutorizantesNetEmpresaEntradaDTO entradaDTO);
}