/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listarultimadatapagto.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdPessoaJuridicaContrato
     */
    private long _cdPessoaJuridicaContrato = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridicaContrato
     */
    private boolean _has_cdPessoaJuridicaContrato;

    /**
     * Field _cdTipoContratoNegocio
     */
    private int _cdTipoContratoNegocio = 0;

    /**
     * keeps track of state for field: _cdTipoContratoNegocio
     */
    private boolean _has_cdTipoContratoNegocio;

    /**
     * Field _nrSequenciaContratoNegocio
     */
    private long _nrSequenciaContratoNegocio = 0;

    /**
     * keeps track of state for field: _nrSequenciaContratoNegocio
     */
    private boolean _has_nrSequenciaContratoNegocio;

    /**
     * Field _cdCpfCnpj
     */
    private java.lang.String _cdCpfCnpj;

    /**
     * Field _dsRazaoSocial
     */
    private java.lang.String _dsRazaoSocial;

    /**
     * Field _dtUltimoPagamento
     */
    private java.lang.String _dtUltimoPagamento;

    /**
     * Field _hrUltimoPagamento
     */
    private java.lang.String _hrUltimoPagamento;

    /**
     * Field _cdProdutoOperacao
     */
    private int _cdProdutoOperacao = 0;

    /**
     * keeps track of state for field: _cdProdutoOperacao
     */
    private boolean _has_cdProdutoOperacao;

    /**
     * Field _dsProdutoOperacao
     */
    private java.lang.String _dsProdutoOperacao;

    /**
     * Field _cdProdutoRelacionado
     */
    private int _cdProdutoRelacionado = 0;

    /**
     * keeps track of state for field: _cdProdutoRelacionado
     */
    private boolean _has_cdProdutoRelacionado;

    /**
     * Field _dsProdutoRelacionado
     */
    private java.lang.String _dsProdutoRelacionado;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarultimadatapagto.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdPessoaJuridicaContrato
     * 
     */
    public void deleteCdPessoaJuridicaContrato()
    {
        this._has_cdPessoaJuridicaContrato= false;
    } //-- void deleteCdPessoaJuridicaContrato() 

    /**
     * Method deleteCdProdutoOperacao
     * 
     */
    public void deleteCdProdutoOperacao()
    {
        this._has_cdProdutoOperacao= false;
    } //-- void deleteCdProdutoOperacao() 

    /**
     * Method deleteCdProdutoRelacionado
     * 
     */
    public void deleteCdProdutoRelacionado()
    {
        this._has_cdProdutoRelacionado= false;
    } //-- void deleteCdProdutoRelacionado() 

    /**
     * Method deleteCdTipoContratoNegocio
     * 
     */
    public void deleteCdTipoContratoNegocio()
    {
        this._has_cdTipoContratoNegocio= false;
    } //-- void deleteCdTipoContratoNegocio() 

    /**
     * Method deleteNrSequenciaContratoNegocio
     * 
     */
    public void deleteNrSequenciaContratoNegocio()
    {
        this._has_nrSequenciaContratoNegocio= false;
    } //-- void deleteNrSequenciaContratoNegocio() 

    /**
     * Returns the value of field 'cdCpfCnpj'.
     * 
     * @return String
     * @return the value of field 'cdCpfCnpj'.
     */
    public java.lang.String getCdCpfCnpj()
    {
        return this._cdCpfCnpj;
    } //-- java.lang.String getCdCpfCnpj() 

    /**
     * Returns the value of field 'cdPessoaJuridicaContrato'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridicaContrato'.
     */
    public long getCdPessoaJuridicaContrato()
    {
        return this._cdPessoaJuridicaContrato;
    } //-- long getCdPessoaJuridicaContrato() 

    /**
     * Returns the value of field 'cdProdutoOperacao'.
     * 
     * @return int
     * @return the value of field 'cdProdutoOperacao'.
     */
    public int getCdProdutoOperacao()
    {
        return this._cdProdutoOperacao;
    } //-- int getCdProdutoOperacao() 

    /**
     * Returns the value of field 'cdProdutoRelacionado'.
     * 
     * @return int
     * @return the value of field 'cdProdutoRelacionado'.
     */
    public int getCdProdutoRelacionado()
    {
        return this._cdProdutoRelacionado;
    } //-- int getCdProdutoRelacionado() 

    /**
     * Returns the value of field 'cdTipoContratoNegocio'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoNegocio'.
     */
    public int getCdTipoContratoNegocio()
    {
        return this._cdTipoContratoNegocio;
    } //-- int getCdTipoContratoNegocio() 

    /**
     * Returns the value of field 'dsProdutoOperacao'.
     * 
     * @return String
     * @return the value of field 'dsProdutoOperacao'.
     */
    public java.lang.String getDsProdutoOperacao()
    {
        return this._dsProdutoOperacao;
    } //-- java.lang.String getDsProdutoOperacao() 

    /**
     * Returns the value of field 'dsProdutoRelacionado'.
     * 
     * @return String
     * @return the value of field 'dsProdutoRelacionado'.
     */
    public java.lang.String getDsProdutoRelacionado()
    {
        return this._dsProdutoRelacionado;
    } //-- java.lang.String getDsProdutoRelacionado() 

    /**
     * Returns the value of field 'dsRazaoSocial'.
     * 
     * @return String
     * @return the value of field 'dsRazaoSocial'.
     */
    public java.lang.String getDsRazaoSocial()
    {
        return this._dsRazaoSocial;
    } //-- java.lang.String getDsRazaoSocial() 

    /**
     * Returns the value of field 'dtUltimoPagamento'.
     * 
     * @return String
     * @return the value of field 'dtUltimoPagamento'.
     */
    public java.lang.String getDtUltimoPagamento()
    {
        return this._dtUltimoPagamento;
    } //-- java.lang.String getDtUltimoPagamento() 

    /**
     * Returns the value of field 'hrUltimoPagamento'.
     * 
     * @return String
     * @return the value of field 'hrUltimoPagamento'.
     */
    public java.lang.String getHrUltimoPagamento()
    {
        return this._hrUltimoPagamento;
    } //-- java.lang.String getHrUltimoPagamento() 

    /**
     * Returns the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @return long
     * @return the value of field 'nrSequenciaContratoNegocio'.
     */
    public long getNrSequenciaContratoNegocio()
    {
        return this._nrSequenciaContratoNegocio;
    } //-- long getNrSequenciaContratoNegocio() 

    /**
     * Method hasCdPessoaJuridicaContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridicaContrato()
    {
        return this._has_cdPessoaJuridicaContrato;
    } //-- boolean hasCdPessoaJuridicaContrato() 

    /**
     * Method hasCdProdutoOperacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdutoOperacao()
    {
        return this._has_cdProdutoOperacao;
    } //-- boolean hasCdProdutoOperacao() 

    /**
     * Method hasCdProdutoRelacionado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdutoRelacionado()
    {
        return this._has_cdProdutoRelacionado;
    } //-- boolean hasCdProdutoRelacionado() 

    /**
     * Method hasCdTipoContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoNegocio()
    {
        return this._has_cdTipoContratoNegocio;
    } //-- boolean hasCdTipoContratoNegocio() 

    /**
     * Method hasNrSequenciaContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaContratoNegocio()
    {
        return this._has_nrSequenciaContratoNegocio;
    } //-- boolean hasNrSequenciaContratoNegocio() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdCpfCnpj'.
     * 
     * @param cdCpfCnpj the value of field 'cdCpfCnpj'.
     */
    public void setCdCpfCnpj(java.lang.String cdCpfCnpj)
    {
        this._cdCpfCnpj = cdCpfCnpj;
    } //-- void setCdCpfCnpj(java.lang.String) 

    /**
     * Sets the value of field 'cdPessoaJuridicaContrato'.
     * 
     * @param cdPessoaJuridicaContrato the value of field
     * 'cdPessoaJuridicaContrato'.
     */
    public void setCdPessoaJuridicaContrato(long cdPessoaJuridicaContrato)
    {
        this._cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
        this._has_cdPessoaJuridicaContrato = true;
    } //-- void setCdPessoaJuridicaContrato(long) 

    /**
     * Sets the value of field 'cdProdutoOperacao'.
     * 
     * @param cdProdutoOperacao the value of field
     * 'cdProdutoOperacao'.
     */
    public void setCdProdutoOperacao(int cdProdutoOperacao)
    {
        this._cdProdutoOperacao = cdProdutoOperacao;
        this._has_cdProdutoOperacao = true;
    } //-- void setCdProdutoOperacao(int) 

    /**
     * Sets the value of field 'cdProdutoRelacionado'.
     * 
     * @param cdProdutoRelacionado the value of field
     * 'cdProdutoRelacionado'.
     */
    public void setCdProdutoRelacionado(int cdProdutoRelacionado)
    {
        this._cdProdutoRelacionado = cdProdutoRelacionado;
        this._has_cdProdutoRelacionado = true;
    } //-- void setCdProdutoRelacionado(int) 

    /**
     * Sets the value of field 'cdTipoContratoNegocio'.
     * 
     * @param cdTipoContratoNegocio the value of field
     * 'cdTipoContratoNegocio'.
     */
    public void setCdTipoContratoNegocio(int cdTipoContratoNegocio)
    {
        this._cdTipoContratoNegocio = cdTipoContratoNegocio;
        this._has_cdTipoContratoNegocio = true;
    } //-- void setCdTipoContratoNegocio(int) 

    /**
     * Sets the value of field 'dsProdutoOperacao'.
     * 
     * @param dsProdutoOperacao the value of field
     * 'dsProdutoOperacao'.
     */
    public void setDsProdutoOperacao(java.lang.String dsProdutoOperacao)
    {
        this._dsProdutoOperacao = dsProdutoOperacao;
    } //-- void setDsProdutoOperacao(java.lang.String) 

    /**
     * Sets the value of field 'dsProdutoRelacionado'.
     * 
     * @param dsProdutoRelacionado the value of field
     * 'dsProdutoRelacionado'.
     */
    public void setDsProdutoRelacionado(java.lang.String dsProdutoRelacionado)
    {
        this._dsProdutoRelacionado = dsProdutoRelacionado;
    } //-- void setDsProdutoRelacionado(java.lang.String) 

    /**
     * Sets the value of field 'dsRazaoSocial'.
     * 
     * @param dsRazaoSocial the value of field 'dsRazaoSocial'.
     */
    public void setDsRazaoSocial(java.lang.String dsRazaoSocial)
    {
        this._dsRazaoSocial = dsRazaoSocial;
    } //-- void setDsRazaoSocial(java.lang.String) 

    /**
     * Sets the value of field 'dtUltimoPagamento'.
     * 
     * @param dtUltimoPagamento the value of field
     * 'dtUltimoPagamento'.
     */
    public void setDtUltimoPagamento(java.lang.String dtUltimoPagamento)
    {
        this._dtUltimoPagamento = dtUltimoPagamento;
    } //-- void setDtUltimoPagamento(java.lang.String) 

    /**
     * Sets the value of field 'hrUltimoPagamento'.
     * 
     * @param hrUltimoPagamento the value of field
     * 'hrUltimoPagamento'.
     */
    public void setHrUltimoPagamento(java.lang.String hrUltimoPagamento)
    {
        this._hrUltimoPagamento = hrUltimoPagamento;
    } //-- void setHrUltimoPagamento(java.lang.String) 

    /**
     * Sets the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @param nrSequenciaContratoNegocio the value of field
     * 'nrSequenciaContratoNegocio'.
     */
    public void setNrSequenciaContratoNegocio(long nrSequenciaContratoNegocio)
    {
        this._nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
        this._has_nrSequenciaContratoNegocio = true;
    } //-- void setNrSequenciaContratoNegocio(long) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listarultimadatapagto.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listarultimadatapagto.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listarultimadatapagto.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarultimadatapagto.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
