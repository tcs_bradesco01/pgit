/*
 * Nome: br.com.bradesco.web.pgit.service.business.manterambienteoperacaocontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.manterambienteoperacaocontrato.bean;

/**
 * Nome: ConsultarSolicitacaoMudancaAmbienteEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarSolicitacaoMudancaAmbienteEntradaDTO {
	
	/** Atributo cdPessoaJuridica. */
	private long cdPessoaJuridica;
	
	/** Atributo cdTipoContratoNegocio. */
	private int cdTipoContratoNegocio;
	
	/** Atributo nrSequenciaContratoNegocio. */
	private long nrSequenciaContratoNegocio;
	
	/** Atributo dtInicioSolicitacao. */
	private String dtInicioSolicitacao;
	
	/** Atributo dtFimSolicitacao. */
	private String dtFimSolicitacao;
	
	/**
	 * Get: cdPessoaJuridica.
	 *
	 * @return cdPessoaJuridica
	 */
	public long getCdPessoaJuridica() {
		return cdPessoaJuridica;
	}
	
	/**
	 * Set: cdPessoaJuridica.
	 *
	 * @param cdPessoaJuridica the cd pessoa juridica
	 */
	public void setCdPessoaJuridica(long cdPessoaJuridica) {
		this.cdPessoaJuridica = cdPessoaJuridica;
	}
	
	/**
	 * Get: cdTipoContratoNegocio.
	 *
	 * @return cdTipoContratoNegocio
	 */
	public int getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}
	
	/**
	 * Set: cdTipoContratoNegocio.
	 *
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(int cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}
	
	/**
	 * Get: dtFimSolicitacao.
	 *
	 * @return dtFimSolicitacao
	 */
	public String getDtFimSolicitacao() {
		return dtFimSolicitacao;
	}
	
	/**
	 * Set: dtFimSolicitacao.
	 *
	 * @param dtFimSolicitacao the dt fim solicitacao
	 */
	public void setDtFimSolicitacao(String dtFimSolicitacao) {
		this.dtFimSolicitacao = dtFimSolicitacao;
	}
	
	/**
	 * Get: dtInicioSolicitacao.
	 *
	 * @return dtInicioSolicitacao
	 */
	public String getDtInicioSolicitacao() {
		return dtInicioSolicitacao;
	}
	
	/**
	 * Set: dtInicioSolicitacao.
	 *
	 * @param dtInicioSolicitacao the dt inicio solicitacao
	 */
	public void setDtInicioSolicitacao(String dtInicioSolicitacao) {
		this.dtInicioSolicitacao = dtInicioSolicitacao;
	}
	
	/**
	 * Get: nrSequenciaContratoNegocio.
	 *
	 * @return nrSequenciaContratoNegocio
	 */
	public long getNrSequenciaContratoNegocio() {
		return nrSequenciaContratoNegocio;
	}
	
	/**
	 * Set: nrSequenciaContratoNegocio.
	 *
	 * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
	 */
	public void setNrSequenciaContratoNegocio(long nrSequenciaContratoNegocio) {
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}
	
	

}
