/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.incluirhorariolimite.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class IncluirHorarioLimiteRequest.
 * 
 * @version $Revision$ $Date$
 */
public class IncluirHorarioLimiteRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdModalidade
     */
    private int _cdModalidade = 0;

    /**
     * keeps track of state for field: _cdModalidade
     */
    private boolean _has_cdModalidade;

    /**
     * Field _dsModalidade
     */
    private java.lang.String _dsModalidade;

    /**
     * Field _hrLimite
     */
    private java.lang.String _hrLimite;

    /**
     * Field _hrLimiteTransm
     */
    private java.lang.String _hrLimiteTransm;


      //----------------/
     //- Constructors -/
    //----------------/

    public IncluirHorarioLimiteRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.incluirhorariolimite.request.IncluirHorarioLimiteRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdModalidade
     * 
     */
    public void deleteCdModalidade()
    {
        this._has_cdModalidade= false;
    } //-- void deleteCdModalidade() 

    /**
     * Returns the value of field 'cdModalidade'.
     * 
     * @return int
     * @return the value of field 'cdModalidade'.
     */
    public int getCdModalidade()
    {
        return this._cdModalidade;
    } //-- int getCdModalidade() 

    /**
     * Returns the value of field 'dsModalidade'.
     * 
     * @return String
     * @return the value of field 'dsModalidade'.
     */
    public java.lang.String getDsModalidade()
    {
        return this._dsModalidade;
    } //-- java.lang.String getDsModalidade() 

    /**
     * Returns the value of field 'hrLimite'.
     * 
     * @return String
     * @return the value of field 'hrLimite'.
     */
    public java.lang.String getHrLimite()
    {
        return this._hrLimite;
    } //-- java.lang.String getHrLimite() 

    /**
     * Returns the value of field 'hrLimiteTransm'.
     * 
     * @return String
     * @return the value of field 'hrLimiteTransm'.
     */
    public java.lang.String getHrLimiteTransm()
    {
        return this._hrLimiteTransm;
    } //-- java.lang.String getHrLimiteTransm() 

    /**
     * Method hasCdModalidade
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdModalidade()
    {
        return this._has_cdModalidade;
    } //-- boolean hasCdModalidade() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdModalidade'.
     * 
     * @param cdModalidade the value of field 'cdModalidade'.
     */
    public void setCdModalidade(int cdModalidade)
    {
        this._cdModalidade = cdModalidade;
        this._has_cdModalidade = true;
    } //-- void setCdModalidade(int) 

    /**
     * Sets the value of field 'dsModalidade'.
     * 
     * @param dsModalidade the value of field 'dsModalidade'.
     */
    public void setDsModalidade(java.lang.String dsModalidade)
    {
        this._dsModalidade = dsModalidade;
    } //-- void setDsModalidade(java.lang.String) 

    /**
     * Sets the value of field 'hrLimite'.
     * 
     * @param hrLimite the value of field 'hrLimite'.
     */
    public void setHrLimite(java.lang.String hrLimite)
    {
        this._hrLimite = hrLimite;
    } //-- void setHrLimite(java.lang.String) 

    /**
     * Sets the value of field 'hrLimiteTransm'.
     * 
     * @param hrLimiteTransm the value of field 'hrLimiteTransm'.
     */
    public void setHrLimiteTransm(java.lang.String hrLimiteTransm)
    {
        this._hrLimiteTransm = hrLimiteTransm;
    } //-- void setHrLimiteTransm(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return IncluirHorarioLimiteRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.incluirhorariolimite.request.IncluirHorarioLimiteRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.incluirhorariolimite.request.IncluirHorarioLimiteRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.incluirhorariolimite.request.IncluirHorarioLimiteRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.incluirhorariolimite.request.IncluirHorarioLimiteRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
