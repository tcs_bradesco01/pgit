package br.com.bradesco.web.pgit.service.business.contacomplementar.bean;

public class IncluirContaComplementarEntradaDTO {
	
	private Long cdpessoaJuridicaContrato;
	private Integer cdTipoContratoNegocio;
	private Long nrSequenciaContratoNegocio;
	private Integer cdContaComplementar;
	private Long cdCorpoCnpjCpf;
	private Integer cdFilialCnpjCpf;
	private Integer cdControleCnpjCpf;
	private Long cdpessoaJuridicaContratoConta;
	private Integer cdTipoContratoConta;
	private Long nrSequenciaContratoConta;
	
	public Long getCdpessoaJuridicaContrato() {
		return cdpessoaJuridicaContrato;
	}
	public void setCdpessoaJuridicaContrato(Long cdpessoaJuridicaContrato) {
		this.cdpessoaJuridicaContrato = cdpessoaJuridicaContrato;
	}
	public Integer getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}
	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}
	public Long getNrSequenciaContratoNegocio() {
		return nrSequenciaContratoNegocio;
	}
	public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}
	public Integer getCdContaComplementar() {
		return cdContaComplementar;
	}
	public void setCdContaComplementar(Integer cdContaComplementar) {
		this.cdContaComplementar = cdContaComplementar;
	}
	public Long getCdCorpoCnpjCpf() {
		return cdCorpoCnpjCpf;
	}
	public void setCdCorpoCnpjCpf(Long cdCorpoCnpjCpf) {
		this.cdCorpoCnpjCpf = cdCorpoCnpjCpf;
	}
	public Integer getCdFilialCnpjCpf() {
		return cdFilialCnpjCpf;
	}
	public void setCdFilialCnpjCpf(Integer cdFilialCnpjCpf) {
		this.cdFilialCnpjCpf = cdFilialCnpjCpf;
	}
	public Integer getCdControleCnpjCpf() {
		return cdControleCnpjCpf;
	}
	public void setCdControleCnpjCpf(Integer cdControleCnpjCpf) {
		this.cdControleCnpjCpf = cdControleCnpjCpf;
	}
	public Long getCdpessoaJuridicaContratoConta() {
		return cdpessoaJuridicaContratoConta;
	}
	public void setCdpessoaJuridicaContratoConta(Long cdpessoaJuridicaContratoConta) {
		this.cdpessoaJuridicaContratoConta = cdpessoaJuridicaContratoConta;
	}
	public Integer getCdTipoContratoConta() {
		return cdTipoContratoConta;
	}
	public void setCdTipoContratoConta(Integer cdTipoContratoConta) {
		this.cdTipoContratoConta = cdTipoContratoConta;
	}
	public Long getNrSequenciaContratoConta() {
		return nrSequenciaContratoConta;
	}
	public void setNrSequenciaContratoConta(Long nrSequenciaContratoConta) {
		this.nrSequenciaContratoConta = nrSequenciaContratoConta;
	}
}
