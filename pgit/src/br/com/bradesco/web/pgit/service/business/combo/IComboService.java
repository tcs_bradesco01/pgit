/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/interfaz.ftl,v $
 * $Id: interfaz.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: interfaz.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.combo;

import java.util.List;

import br.com.bradesco.web.pgit.service.business.combo.bean.*;
import br.com.bradesco.web.pgit.service.business.combo.impl.bean.ListarModalidadesEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.impl.bean.ListarModalidadesListaDebitoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterfavorecido.bean.ConsultarMotivoBloqueioContaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterfavorecido.bean.ListarMotivoBloqueioFavorecidoSaidaDTO;

// TODO: Auto-generated Javadoc
/**
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Interface do adaptador: Combo
 * </p>.
 *
 * @comment CODIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public interface IComboService {

	/**
	 * Listar tipo unidade organizacional.
	 *
	 * @param tipoUnidadeOrganizacionalEntradaDTO the tipo unidade organizacional entrada dto
	 * @return the list< tipo unidade organizacional dt o>
	 */
	List<TipoUnidadeOrganizacionalDTO> listarTipoUnidadeOrganizacional(
					TipoUnidadeOrganizacionalDTO tipoUnidadeOrganizacionalEntradaDTO);

	/**
	 * Listar centro custo.
	 *
	 * @param centroCustoEntradaDTO the centro custo entrada dto
	 * @return the list< centro custo saida dt o>
	 */
	List<CentroCustoSaidaDTO> listarCentroCusto(CentroCustoEntradaDTO centroCustoEntradaDTO);

	/**
	 * Listar tipo processo.
	 *
	 * @param tipoProcessoEntradaDTO the tipo processo entrada dto
	 * @return the list< tipo processo saida dt o>
	 */
	List<TipoProcessoSaidaDTO> listarTipoProcesso(TipoProcessoEntradaDTO tipoProcessoEntradaDTO);

	/**
	 * Listar tipo lote.
	 *
	 * @param tipoLoteEntradaDTO the tipo lote entrada dto
	 * @return the list< tipo lote saida dt o>
	 */
	List<TipoLoteSaidaDTO> listarTipoLote(TipoLoteEntradaDTO tipoLoteEntradaDTO);

	/**
	 * Listar tipo layout arquivo.
	 *
	 * @param tipoLayoutArquivoEntradaDTO the tipo layout arquivo entrada dto
	 * @return Tipo Layout Arquivo Saida DTO
	 */
	TipoLayoutArquivoSaidaDTO listarTipoLayoutArquivo(TipoLayoutArquivoEntradaDTO tipoLayoutArquivoEntradaDTO);

	/**
	 * Listar tipo lote cem.
	 *
	 * @param tipoLoteEntradaDTO the tipo lote entrada dto
	 * @return the list< tipo lote saida dt o>
	 */
	List<TipoLoteSaidaDTO> listarTipoLoteCem(TipoLoteEntradaDTO tipoLoteEntradaDTO);

	/**
	 * Listar tipo layout arquivo cem.
	 *
	 * @param tipoLayoutArquivoEntradaDTO the tipo layout arquivo entrada dto
	 * @return the list< tipo layout arquivo saida dt o>
	 */
	List<TipoLayoutArquivoSaidaDTO> listarTipoLayoutArquivoCem(TipoLayoutArquivoEntradaDTO tipoLayoutArquivoEntradaDTO);

	/**
	 * Listar periodicidade.
	 *
	 * @param periodicidadeEntradaDTO the periodicidade entrada dto
	 * @return the list< periodicidade saida dt o>
	 */
	List<PeriodicidadeSaidaDTO> listarPeriodicidade(PeriodicidadeEntradaDTO periodicidadeEntradaDTO);

	/**
	 * Consultar lista periodicidade.
	 *
	 * @param periodicidadeEntradaDTO the periodicidade entrada dto
	 * @return the list< periodicidade saida dt o>
	 */
	List<PeriodicidadeSaidaDTO> consultarListaPeriodicidade(PeriodicidadeEntradaDTO periodicidadeEntradaDTO);

	/**
	 * Listar empresas gestoras.
	 *
	 * @return the list< empresa gestora saida dt o>
	 */
	List<EmpresaGestoraSaidaDTO> listarEmpresasGestoras();

	/**
	 * Listar tipo contrato.
	 *
	 * @return the list< tipo contrato saida dt o>
	 */
	List<TipoContratoSaidaDTO> listarTipoContrato();

	/**
	 * Listar empresa conglomerado.
	 *
	 * @param empresaConglomeradoEntradaDTO the empresa conglomerado entrada dto
	 * @return the list< empresa conglomerado saida dt o>
	 */
	List<EmpresaConglomeradoSaidaDTO> listarEmpresaConglomerado(
					EmpresaConglomeradoEntradaDTO empresaConglomeradoEntradaDTO);

	/**
	 * Listar moeda economica.
	 *
	 * @param moedaEconomicaEntradaDTO the moeda economica entrada dto
	 * @return the list< moeda economica saida dt o>
	 */
	List<MoedaEconomicaSaidaDTO> listarMoedaEconomica(MoedaEconomicaEntradaDTO moedaEconomicaEntradaDTO);

	/**
	 * Listar tipo favorecido.
	 *
	 * @return the list< tipo favorecido saida dt o>
	 */
	List<TipoFavorecidoSaidaDTO> listarTipoFavorecido();

	/**
	 * Listar tipo inscricao.
	 *
	 * @return the list< inscricao favorecidos saida dt o>
	 */
	List<InscricaoFavorecidosSaidaDTO> listarTipoInscricao();

	/**
	 * Listar forma liquidacao.
	 *
	 * @return the list< listar forma liquidacao saida dt o>
	 */
	List<ListarFormaLiquidacaoSaidaDTO> listarFormaLiquidacao();

	/**
	 * Listar modalidades.
	 *
	 * @param entradaDTO the entrada dto
	 * @return the list< listar modalidade saida dt o>
	 */
	List<ListarModalidadeSaidaDTO> listarModalidades(ListarModalidadesEntradaDTO entradaDTO);

	/**
	 * Listar motivo bloqueio favorecido.
	 *
	 * @return the list< listar motivo bloqueio favorecido saida dt o>
	 */
	List<ListarMotivoBloqueioFavorecidoSaidaDTO> listarMotivoBloqueioFavorecido();

	/**
	 * Pesquisa segmento.
	 *
	 * @return the list< empresa segmento saida dt o>
	 */
	List<EmpresaSegmentoSaidaDTO> pesquisaSegmento();

	/**
	 * Pesquisa gerencia.
	 *
	 * @param codPessoaJuridica the cod pessoa juridica
	 * @param empresaGerencia the empresa gerencia
	 * @param tipoPesquisa the tipo pesquisa
	 * @return the list< listar dependencia diretoria saida dt o>
	 */
	List<ListarDependenciaDiretoriaSaidaDTO> pesquisaGerencia(Long codPessoaJuridica, Long empresaGerencia,
					Integer tipoPesquisa);

	/**
	 * Pesquisa diretoria.
	 *
	 * @param empresaConglomerado the empresa conglomerado
	 * @return the list< listar dependencia empresa saida dt o>
	 */
	List<ListarDependenciaEmpresaSaidaDTO> pesquisaDiretoria(Long empresaConglomerado);

	/**
	 * Listar tipo servicos.
	 *
	 * @param natureza the natureza
	 * @return the list< listar servicos saida dt o>
	 */
	List<ListarServicosSaidaDTO> listarTipoServicos(int natureza);

	/**
	 * Listar tipo modalidades.
	 *
	 * @param entrada the entrada
	 * @return the list< listar modalidade saida dt o>
	 */
	List<ListarModalidadeSaidaDTO> listarTipoModalidades(ListarModalidadesEntradaDTO entrada);

	/**
	 * Listar tipo arquivo retorno.
	 *
	 * @return the list< listar tipo arquivo retorno saida dt o>
	 */
	List<ListarTipoArquivoRetornoSaidaDTO> listarTipoArquivoRetorno();

	/**
	 * Listar tipo arquivo retorno.
	 *
	 * @param entradaDTO the entrada dto
	 * @return the list< listar tipo arquivo retorno saida dt o>
	 */
	List<ListarTipoArquivoRetornoSaidaDTO> listarTipoArquivoRetorno(ListarTipoArquivoRetornoEntradaDTO entradaDTO);

	/**
	 * Listar aplicativo formatacao arquivo.
	 *
	 * @param entrada the entrada
	 * @return the list< listar aplicativo formatacao arquivo saida dt o>
	 */
	List<ListarAplicativoFormatacaoArquivoSaidaDTO> listarAplicativoFormatacaoArquivo(ListarAplicativoFormatacaoArquivoEntradaDTO entrada);

	/**
	 * Listar tipo acao.
	 *
	 * @return the list< tipo acao saida dt o>
	 */
	List<TipoAcaoSaidaDTO> listarTipoAcao();

	/**
	 * Listar operacoes servicos.
	 *
	 * @param entradaDTO the entrada dto
	 * @return the list< listar operacoes servico saida dt o>
	 */
	List<ListarOperacoesServicoSaidaDTO> listarOperacoesServicos(ListarOperacoesServicosEntradaDTO entradaDTO);

	/**
	 * Listar finalidade endereco.
	 *
	 * @return the list< listar finalidade endereco saida dt o>
	 */
	List<ListarFinalidadeEnderecoSaidaDTO> listarFinalidadeEndereco();

	/**
	 * Listar tipo conta.
	 *
	 * @return the list< listar tipo conta saida dt o>
	 */
	List<ListarTipoContaSaidaDTO> listarTipoConta();

	/**
	 * Listar participantes.
	 *
	 * @param listarParticipantesEntradaDTO the listar participantes entrada dto
	 * @return the list< listar participantes saida dt o>
	 */
	List<ListarParticipantesSaidaDTO> listarParticipantes(ListarParticipantesEntradaDTO listarParticipantesEntradaDTO);

	/**
	 * Listar motivo bloqueio conta favorecido.
	 *
	 * @return the list< consultar motivo bloqueio conta saida dt o>
	 */
	List<ConsultarMotivoBloqueioContaSaidaDTO> listarMotivoBloqueioContaFavorecido();

	/**
	 * Listar idioma.
	 *
	 * @return the list< idioma saida dt o>
	 */
	List<IdiomaSaidaDTO> listarIdioma();

	/**
	 * Listar recurso canal msg.
	 *
	 * @return the list< listar recurso canal msg saida dt o>
	 */
	List<ListarRecursoCanalMsgSaidaDTO> listarRecursoCanalMsg();

	/**
	 * Listar situacao solicitacao.
	 *
	 * @return the list< listar situacao solicitacao saida dt o>
	 */
	List<ListarSituacaoSolicitacaoSaidaDTO> listarSituacaoSolicitacao();

	/**
	 * Listar forma lanc cnab.
	 *
	 * @return the list< listar forma lanc cnab saida dt o>
	 */
	List<ListarFormaLancCnabSaidaDTO> listarFormaLancCnab();

	/**
	 * Listar servico cnab.
	 *
	 * @return the list< listar servico cnab saida dt o>
	 */
	List<ListarServicoCnabSaidaDTO> listarServicoCnab();

	/**
	 * Listar situacao contrato.
	 *
	 * @return the list< listar situacao contrato saida dt o>
	 */
	List<ListarSituacaoContratoSaidaDTO> listarSituacaoContrato();

	/**
	 * Listar situacao pend contrato.
	 *
	 * @return the list< listar situacao pend contrato saida dt o>
	 */
	List<ListarSituacaoPendContratoSaidaDTO> listarSituacaoPendContrato();

	/**
	 * Listar tipo pendencia.
	 *
	 * @return the list< listar tipo pendencia saida dt o>
	 */
	List<ListarTipoPendenciaSaidaDTO> listarTipoPendencia();

	/**
	 * Listar motivo situacao contrato.
	 *
	 * @param entradaDTO the entrada dto
	 * @return the list< listar motivo situacao contrato saida dt o>
	 */
	List<ListarMotivoSituacaoContratoSaidaDTO> listarMotivoSituacaoContrato(
					ListarMotivoSituacaoContratoEntradaDTO entradaDTO);

	/**
	 * Listar tipo mensagem.
	 *
	 * @param entrada the entrada
	 * @return the list< listar tipo mensagem saida dt o>
	 */
	List<ListarTipoMensagemSaidaDTO> listarTipoMensagem(ListarTipoMensagemEntradaDTO entrada);

	/**
	 * Listar municipio.
	 *
	 * @param entrada the entrada
	 * @return the list< listar municipio saida dt o>
	 */
	List<ListarMunicipioSaidaDTO> listarMunicipio(ListarMunicipioEntradaDTO entrada);

	/**
	 * Listar unidade federativa.
	 *
	 * @return the list< listar unidade federativa saida dt o>
	 */
	List<ListarUnidadeFederativaSaidaDTO> listarUnidadeFederativa();

	/**
	 * Listar finalidade conta.
	 *
	 * @return the list< listar finalidade conta saida dt o>
	 */
	List<ListarFinalidadeContaSaidaDTO> listarFinalidadeConta();

	/**
	 * Listar motivo situacao pend contrato.
	 *
	 * @param motivoSituacaoPendenciaContratoEntradaDTO the motivo situacao pendencia contrato entrada dto
	 * @return the list< motivo situacao pendencia contrato saida dt o>
	 */
	List<MotivoSituacaoPendenciaContratoSaidaDTO> listarMotivoSituacaoPendContrato(
					MotivoSituacaoPendenciaContratoEntradaDTO motivoSituacaoPendenciaContratoEntradaDTO);

	/**
	 * Listar servico composto.
	 *
	 * @return the list< lista servico composto saida dt o>
	 */
	List<ListaServicoCompostoSaidaDTO> listarServicoComposto();

	/**
	 * Listar tipo servicos relacionados.
	 *
	 * @return the list< listar servicos saida dt o>
	 */
	List<ListarServicosSaidaDTO> listarTipoServicosRelacionados();

	/**
	 * Listar servicos emissao.
	 *
	 * @param entrada the entrada
	 * @return the list< listar servicos emissao saida dt o>
	 */
	List<ListarServicosEmissaoSaidaDTO> listarServicosEmissao(ListarServicosEmissaoEntradaDTO entrada);

	/**
	 * Listar modalidades realcionados.
	 *
	 * @param listarModalidadesEntradaDTO the listar modalidades entrada dto
	 * @return the list< listar modalidade saida dt o>
	 */
	List<ListarModalidadeSaidaDTO> listarModalidadesRealcionados(ListarModalidadesEntradaDTO listarModalidadesEntradaDTO);
	
	/**
	 * Listar servico relacionado pgit.
	 *
	 * @param entrada the entrada
	 * @return the list< listar servico relacionado pgit saida dt o>
	 */
	ListarServicoRelacionadoPgitSaidaDTO listarServicoRelacionadoPgit(
			ListarServicoRelacionadoPgitEntradaDTO entrada);
	
	/**
	 * Listar servico relacionado pgit cdps.
	 *
	 * @param entrada the entrada
	 * @return the list< listar servico relacionado pgit cdps saida dt o>
	 */
	ListarServicoRelacionadoPgitCdpsSaidaDTO listarServicoRelacionadoPgitCdps(
			ListarServicoRelacionadoPgitCdpsEntradaDTO entrada);

	/**
	 * Consultar mensagem.
	 *
	 * @param entradaDTO the entrada dto
	 * @return the consulta mensagem saida dto
	 */
	ConsultaMensagemSaidaDTO consultarMensagem(ConsultaMensagemEntradaDTO entradaDTO);

	/**
	 * Listar indice economico.
	 *
	 * @param listarIndiceEconomicoEntradaDTO the listar indice economico entrada dto
	 * @return the list< listar indice economico saida dt o>
	 */
	List<ListarIndiceEconomicoSaidaDTO> listarIndiceEconomico(
					ListarIndiceEconomicoEntradaDTO listarIndiceEconomicoEntradaDTO);

	/**
	 * Listar tipo vinculo.
	 *
	 * @return the list< listar tipo vinculo saida dt o>
	 */
	List<ListarTipoVinculoSaidaDTO> listarTipoVinculo();

	/**
	 * Listar situacao vinc contrato.
	 *
	 * @return the list< listar situacao vinc contrato saida dt o>
	 */
	List<ListarSituacaoVincContratoSaidaDTO> listarSituacaoVincContrato();

	/**
	 * Listar motivo situacao serv contrato.
	 *
	 * @param entrada the entrada
	 * @return the list< listar motivo situacao serv contrato saida dt o>
	 */
	List<ListarMotivoSituacaoServContratoSaidaDTO> listarMotivoSituacaoServContrato(
					ListarMotivoSituacaoServContratoEntradaDTO entrada);

	/**
	 * Listar motivo situacao vinc contrato.
	 *
	 * @param entradaDTO the entrada dto
	 * @return the list< listar motivo situacao vinc contrato saida dt o>
	 */
	List<ListarMotivoSituacaoVincContratoSaidaDTO> listarMotivoSituacaoVincContrato(
					ListarMotivoSituacaoVincContratoEntradaDTO entradaDTO);

	/**
	 * Listar motivo situacao aditivo contrato.
	 *
	 * @param entrada the entrada
	 * @return the list< listar motivo situacao aditivo cont saida dt o>
	 */
	List<ListarMotivoSituacaoAditivoContSaidaDTO> listarMotivoSituacaoAditivoContrato(
					ListarMotivoSituacaoAditivoContEntradaDTO entrada);

	/**
	 * Listar combo forma liquidacao.
	 *
	 * @return the list< listar forma liquidacao saida dt o>
	 */
	List<ListarFormaLiquidacaoSaidaDTO> listarComboFormaLiquidacao();

	/**
	 * Listar meio transmissao.
	 *
	 * @param entrada the entrada
	 * @return the list< listar meio transmissao saida dt o>
	 */
	List<ListarMeioTransmissaoSaidaDTO> listarMeioTransmissao(ListarMeioTransmissaoEntradaDTO entrada);

	/**
	 * Listar modalidades parametros.
	 *
	 * @param listarModalidadesEntradaDTO the listar modalidades entrada dto
	 * @return the list< listar modalidade saida dt o>
	 */
	List<ListarModalidadeSaidaDTO> listarModalidadesParametros(ListarModalidadesEntradaDTO listarModalidadesEntradaDTO);

	/**
	 * Listar situacao aditivo contrato.
	 *
	 * @return the list< listar situacao aditivo contrato saida dt o>
	 */
	List<ListarSituacaoAditivoContratoSaidaDTO> listarSituacaoAditivoContrato();

	/**
	 * Listar situacao sol consistencia previa.
	 *
	 * @return the list< listar situacao sol consistencia previa saida dt o>
	 */
	List<ListarSituacaoSolConsistenciaPreviaSaidaDTO> listarSituacaoSolConsistenciaPrevia();

	/**
	 * Listar orgao pagador desc orgao.
	 *
	 * @param entrada the entrada
	 * @return the list< listar orgao pagador saida dt o>
	 */
	List<ListarOrgaoPagadorSaidaDTO> listarOrgaoPagadorDescOrgao(ListarOrgaoPagadorEntradaDTO entrada);

	/**
	 * Listar tipo servico.
	 *
	 * @param listarServicosEntradaDTO the listar servicos entrada dto
	 * @return the list< listar servicos saida dt o>
	 */
	List<ListarServicosSaidaDTO> listarTipoServico(ListarServicosEntradaDTO listarServicosEntradaDTO);

	/**
	 * Listar modalidades entrada.
	 *
	 * @param entradaDTO the entrada dto
	 * @return the list< listar modalidade saida dt o>
	 */
	List<ListarModalidadeSaidaDTO> listarModalidadesEntrada(ListarModalidadesEntradaDTO entradaDTO);
	
	/**
	 * Listar modalidades lista debito entrada.
	 *
	 * @param entradaDTO the entrada dto
	 * @return the list< listar modalidade lista debito saida dt o>
	 */
	List<ListarModalidadeListaDebitoSaidaDTO> listarModalidadesListaDebitoEntrada(ListarModalidadesListaDebitoEntradaDTO entradaDTO);

	/**
	 * Listar inscricao favorecido.
	 *
	 * @param entrada the entrada
	 * @return the list< inscricao favorecidos saida dt o>
	 */
	List<InscricaoFavorecidosSaidaDTO> listarInscricaoFavorecido(InscricaoFavorecidosEntradaDTO entrada);

	/**
	 * Consultar situacao pagamento.
	 *
	 * @param entrada the entrada
	 * @return the list< consultar situacao pagamento saida dt o>
	 */
	List<ConsultarSituacaoPagamentoSaidaDTO> consultarSituacaoPagamento(ConsultarSituacaoPagamentoEntradaDTO entrada);

	/**
	 * Consultar motivo situacao pagamento.
	 *
	 * @param entrada the entrada
	 * @return the list< consultar motivo situacao pagamento saida dt o>
	 */
	List<ConsultarMotivoSituacaoPagamentoSaidaDTO> consultarMotivoSituacaoPagamento(
					ConsultarMotivoSituacaoPagamentoEntradaDTO entrada);

	/**
	 * Listar orgao pagador.
	 *
	 * @param entrada the entrada
	 * @return the list< listar orgao pagador saida dt o>
	 */
	List<ListarOrgaoPagadorSaidaDTO> listarOrgaoPagador(ListarOrgaoPagadorEntradaDTO entrada);

	/**
	 * Consultar situacao solicitacao estorno.
	 *
	 * @param entrada the entrada
	 * @return the list< consultar situacao solicitacao estorno saida dt o>
	 */
	List<ConsultarSituacaoSolicitacaoEstornoSaidaDTO> consultarSituacaoSolicitacaoEstorno(
					ConsultarSituacaoSolicitacaoEstornoEntradaDTO entrada);

	/**
	 * Listar tipo contrato marq.
	 *
	 * @return the list< listar tipo contrato marq saida dt o>
	 */
	List<ListarTipoContratoMarqSaidaDTO> listarTipoContratoMarq();

	/**
	 * Listar pessoa juridica contrato marq.
	 *
	 * @return the list< listar pessoa juridica contrato marq saida dt o>
	 */
	List<ListarPessoaJuridicaContratoMarqSaidaDTO> listarPessoaJuridicaContratoMarq();

	/**
	 * Consultar tipo conta.
	 *
	 * @param entrada the entrada
	 * @return the list< consultar tipo conta saida dt o>
	 */
	List<ConsultarTipoContaSaidaDTO> consultarTipoConta(ConsultarTipoContaEntradaDTO entrada);

	/**
	 * Consultar modalidade pgit.
	 *
	 * @param entradaDTO the entrada dto
	 * @return the list< consultar modalidade pgit saida dt o>
	 */
	List<ConsultarModalidadePGITSaidaDTO> consultarModalidadePGIT(ConsultarModalidadePGITEntradaDTO entradaDTO);

	/**
	 * Listar tipo sistema.
	 *
	 * @return the list< tipo sistema saida dt o>
	 */
	List<TipoSistemaSaidaDTO> listarTipoSistema();

	/**
	 * Listar situacao retorno.
	 *
	 * @return the list< listar situacao retorno saida dt o>
	 */
	List<ListarSituacaoRetornoSaidaDTO> listarSituacaoRetorno();

	/**
	 * Listar tipo layout.
	 *
	 * @return the list< listar tipo layout saida dt o>
	 */
	List<ListarTipoLayoutSaidaDTO> listarTipoLayout();

	/**
	 * Listar contas vinculadas contrato.
	 *
	 * @param entradaDTO the entrada dto
	 * @return the list< listar contas vinculadas contrato saida dt o>
	 */
	List<ListarContasVinculadasContratoSaidaDTO> listarContasVinculadasContrato(
					ListarContasVinculadasContratoEntradaDTO entradaDTO);

	/**
	 * Listar situacao remessa.
	 *
	 * @return the list< listar situacao remessa saida dt o>
	 */
	List<ListarSituacaoRemessaSaidaDTO> listarSituacaoRemessa();

	/**
	 * Listar resultado processamento remessa.
	 *
	 * @return the list< listar resultado processamento remessa saida dt o>
	 */
	List<ListarResultadoProcessamentoRemessaSaidaDTO> listarResultadoProcessamentoRemessa();

	/**
	 * Consultar servico.
	 *
	 * @param entrada the entrada
	 * @return the list< consultar servico saida dt o>
	 */
	List<ConsultarServicoSaidaDTO> consultarServico(ConsultarServicoEntradaDTO entrada);

	/**
	 * Consultar modalidade.
	 *
	 * @param entradaDTO the entrada dto
	 * @return the list< consultar modalidade saida dt o>
	 */
	List<ConsultarModalidadeSaidaDTO> consultarModalidade(ConsultarModalidadeEntradaDTO entradaDTO);

	/**
	 * Consultar modalidade pagto.
	 *
	 * @param cdProdutoServicoOperacao the cd produto servico operacao
	 * @return the list< consultar modalidade pagto saida dt o>
	 */
	List<ConsultarModalidadePagtoSaidaDTO> consultarModalidadePagto(Integer cdProdutoServicoOperacao);

	/**
	 * Consultar servico pagto.
	 *
	 * @return the list< consultar servico pagto saida dt o>
	 */
	List<ConsultarServicoPagtoSaidaDTO> consultarServicoPagto();

	/**
	 * Consultar lista valores discretos.
	 *
	 * @param entradaDTO the entrada dto
	 * @return the list< consultar lista valores discretos saida dt o>
	 */
	List<ConsultarListaValoresDiscretosSaidaDTO> consultarListaValoresDiscretos(
					ConsultarListaValoresDiscretosEntradaDTO entradaDTO);
	
	List<ConsultarListaValoresDiscretosSaidaDTO> consultarConfiguracaoServicoModalidade(
			ConsultarListaValoresDiscretosEntradaDTO entradaDTO);

	/**
	 * Listar motivo bloqueio desb.
	 *
	 * @param entradaDTO the entrada dto
	 * @return the list< listar motivo bloqueio desb saida dt o>
	 */
	List<ListarMotivoBloqueioDesbSaidaDTO> listarMotivoBloqueioDesb(ListarMotivoBloqueioDesbEntradaDTO entradaDTO);

	/**
	 * Consultar motivo situacao estorno saida.
	 *
	 * @param entradaDTO the entrada dto
	 * @return the list< consultar motivo situacao estorno saida dt o>
	 */
	List<ConsultarMotivoSituacaoEstornoSaidaDTO> consultarMotivoSituacaoEstornoSaida(
					ConsultarMotivoSituacaoEstornoEntradaDTO entradaDTO);

	/**
	 * Listar ass lote layout arquivo.
	 *
	 * @param entradaDTO the entrada dto
	 * @return the list< listar ass lote layout arquivo saida dt o>
	 */
	List<ListarAssLoteLayoutArquivoSaidaDTO> listarAssLoteLayoutArquivo(ListarAssLoteLayoutArquivoEntradaDTO entradaDTO);

	/**
	 * Consultar modalidade servico.
	 *
	 * @return the list< consultar modalidade servico saida dt o>
	 */
	List<ConsultarModalidadeServicoSaidaDTO> consultarModalidadeServico();

	/**
	 * Listar liquidacao pagamento.
	 *
	 * @param entrada the entrada
	 * @return the list< listar liquidacao pagamento saida dt o>
	 */
	List<ListarLiquidacaoPagamentoSaidaDTO> listarLiquidacaoPagamento(ListarLiquidacaoPagamentoEntradaDTO entrada);

	/**
	 * Consultar motivo situacao pag pendente.
	 *
	 * @param entrada the entrada
	 * @return the list< consultar motivo situacao pag pendente saida dt o>
	 */
	List<ConsultarMotivoSituacaoPagPendenteSaidaDTO> consultarMotivoSituacaoPagPendente(
					ConsultarMotivoSituacaoPagPendenteEntradaDTO entrada);

	/**
	 * Listar classe ramo atvdd.
	 *
	 * @return the list< listar classe ramo atvdd saida dt o>
	 */
	List<ListarClasseRamoAtvddSaidaDTO> listarClasseRamoAtvdd();

	/**
	 * Listar sramo atvdd econc.
	 *
	 * @param entrada the entrada
	 * @return the list< listar sramo atvdd econc saida dt o>
	 */
	List<ListarSramoAtvddEconcSaidaDTO> listarSramoAtvddEconc(ListarSramoAtvddEconcEntradaDTO entrada);

	/**
	 * Listar tipo relac conta.
	 *
	 * @param entrada the entrada
	 * @return the list< listar tipo relac conta saida dt o>
	 */
	List<ListarTipoRelacContaSaidaDTO> listarTipoRelacConta(ListarTipoRelacContaEntradaDTO entrada);

	/**
	 * Listar operacao servico pagto integrado.
	 *
	 * @param entrada the entrada
	 * @return the list< listar operacao servico pagto integrado saida dt o>
	 */
	List<ListarOperacaoServicoPagtoIntegradoSaidaDTO> listarOperacaoServicoPagtoIntegrado(
					ListarOperacaoServicoPagtoIntegradoEntradaDTO entrada);

	/**
	 * Consultar empresa transmissao arquivo van.
	 *
	 * @param entradaDTO the entrada dto
	 * @return the list< listar empresa transmissao arquivo van saida dt o>
	 */
	List<ListarEmpresaTransmissaoArquivoVanSaidaDTO> consultarEmpresaTransmissaoArquivoVan(
					ListarEmpresaTransmissaoArquivoVanEntradaDTO entradaDTO);
	
	/**
	 * Listar aplictv formt tipo layout arqv.
	 *
	 * @param tipoLayoutArquivoEntradaDTO the tipo layout arquivo entrada dto
	 * @return the list< tipo layout arquivo saida dt o>
	 */
	List<TipoLayoutArquivoSaidaDTO> listarAplictvFormtTipoLayoutArqv(TipoLayoutArquivoEntradaDTO tipoLayoutArquivoEntradaDTO);
	
	/**
	 * Listar tipo servico contrato.
	 *
	 * @param entrada the entrada
	 * @return the list< lista tipo servico contrato saida dt o>
	 */
	List<ListaTipoServicoContratoSaidaDTO>listarTipoServicoContrato(ListaTipoServicoContratoEntradaDTO entrada);

	/**
	 * Listar servico modalidade emissao aviso.
	 *
	 * @param entrada the entrada
	 * @return the list< listar servico modalidade emissao aviso saida dt o>
	 */
	List<ListarServicoModalidadeEmissaoAvisoSaidaDTO>listarServicoModalidadeEmissaoAviso(ListarServicoModalidadeEmissaoAvisoEntradaDTO entrada);
	
	/**
	 * Listar modalidade contrato.
	 *
	 * @param entrada the entrada
	 * @return the list< listar modalidade contrato ocorrencia saida dt o>
	 */
	List<ListarModalidadeContratoOcorrenciaSaidaDTO>listarModalidadeContrato(ListarModalidadeContratoEntradaDTO entrada);

	/**
	 * Listar meio transmissao pagamento.
	 *
	 * @param entrada the entrada
	 * @return the listar meio transmissao pagamento saida dto
	 */
	ListarMeioTransmissaoPagamentoSaidaDTO listarMeioTransmissaoPagamento(ListarMeioTransmissaoPagamentoEntradaDTO entrada);
	
	/**
	 * Consultar lista servicos pgto.
	 *
	 * @param cdpessoaJuridicaContrato the cdpessoa juridica contrato
	 * @param cdTipoContrato the cd tipo contrato
	 * @param nrSequenciaContrato the nr sequencia contrato
	 * @return the list< consultar servico pagto saida dt o>
	 */
	List<ConsultarServicoPagtoSaidaDTO> consultarListaServicosPgto(Long cdpessoaJuridicaContrato, Integer cdTipoContrato, Long nrSequenciaContrato);
	
	/**
	 * Consultar lista modalidades.
	 *
	 * @param entradaDTO the entrada dto
	 * @return the list< listar modalidade saida dt o>
	 */
	List<ListarModalidadeSaidaDTO> consultarListaModalidades(ListarModalidadesEntradaDTO entradaDTO);
	
	/**
	 * Consultar modalidade estorno.
	 *
	 * @param entradaDTO the entrada dto
	 * @return the list< listar modalidades estorno saida dt o>
	 */
	List<ListarModalidadesEstornoSaidaDTO> consultarModalidadeEstorno(ListarModalidadesEstornoEntradaDTO entradaDTO);
	
	/**
	 * Listar Numero Versao Layout.
	 *
	 * @param entrada the entrada
	 * @return the Listar Numero Versao Layout Saida DTO
	 */
	ListarNumeroVersaoLayoutSaidaDTO listarNumeroVersaoLayout(ListarNumeroVersaoLayoutEntradaDTO entrada);

	/**
	 * Listar tipo processamento layout arquivo.
	 *
	 * @param entrada the entrada
	 * @return the listar tipo processamento layout arquivo saida dto
	 */
	ListarTipoProcessamentoLayoutArquivoSaidaDTO listarTipoProcessamentoLayoutArquivo 
		(ListarTipoProcessamentoLayoutArquivoEntradaDTO entrada);
	
	/**
	 * Listar parametros.
	 *
	 * @param entrada the entrada
	 * @return the listar parametros saida dto
	 */
	ListarParametrosSaidaDTO listarParametros (ListarParametrosEntradaDTO entrada);
	
	/**
	 * Listar layouts contrato.
	 *
	 * @param entrada the entrada
	 * @return the listar layouts contrato saida dto
	 */
	ListarLayoutsContratoSaidaDTO listarLayoutsContrato (ListarLayoutsContratoEntradaDTO entrada);
	
	/**
	 * Consultar servicos catalogo.
	 *
	 * @param entrada the entrada
	 * @return the consultar servicos catalogo saida dto
	 */
	ConsultarServicosCatalogoSaidaDTO consultarServicosCatalogo(ConsultarServicosCatalogoEntradaDTO entrada);
}