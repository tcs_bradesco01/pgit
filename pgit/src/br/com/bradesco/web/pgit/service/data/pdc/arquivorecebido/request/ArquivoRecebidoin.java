/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.arquivorecebido.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;

/**
 * Class ArquivoRecebidoin.
 * 
 * @version $Revision$ $Date$
 */
public class ArquivoRecebidoin implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _maxOcorrencia
     */
    private int _maxOcorrencia = 00;

    /**
     * keeps track of state for field: _maxOcorrencia
     */
    private boolean _has_maxOcorrencia;

    /**
     * Field _cpf
     */
    private java.lang.String _cpf = "00000000000";

    /**
     * Field _dataDe
     */
    private java.lang.String _dataDe;

    /**
     * Field _dataAte
     */
    private java.lang.String _dataAte;

    /**
     * Field _sequencia
     */
    private int _sequencia;

    /**
     * keeps track of state for field: _sequencia
     */
    private boolean _has_sequencia;

    /**
     * Field _produto
     */
    private java.lang.String _produto;

    /**
     * Field _perfil
     */
    private java.lang.String _perfil;

    /**
     * Field _processamento
     */
    private java.lang.String _processamento;

    /**
     * Field _resultadoProcessamento
     */
    private java.lang.String _resultadoProcessamento;


      //----------------/
     //- Constructors -/
    //----------------/

    public ArquivoRecebidoin() 
     {
        super();
        setCpf("00000000000");
    } //-- br.com.bradesco.web.pgit.service.data.pdc.arquivorecebido.request.ArquivoRecebidoin()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteMaxOcorrencia
     * 
     */
    public void deleteMaxOcorrencia()
    {
        this._has_maxOcorrencia= false;
    } //-- void deleteMaxOcorrencia() 

    /**
     * Method deleteSequencia
     * 
     */
    public void deleteSequencia()
    {
        this._has_sequencia= false;
    } //-- void deleteSequencia() 

    /**
     * Returns the value of field 'cpf'.
     * 
     * @return String
     * @return the value of field 'cpf'.
     */
    public java.lang.String getCpf()
    {
        return this._cpf;
    } //-- java.lang.String getCpf() 

    /**
     * Returns the value of field 'dataAte'.
     * 
     * @return String
     * @return the value of field 'dataAte'.
     */
    public java.lang.String getDataAte()
    {
        return this._dataAte;
    } //-- java.lang.String getDataAte() 

    /**
     * Returns the value of field 'dataDe'.
     * 
     * @return String
     * @return the value of field 'dataDe'.
     */
    public java.lang.String getDataDe()
    {
        return this._dataDe;
    } //-- java.lang.String getDataDe() 

    /**
     * Returns the value of field 'maxOcorrencia'.
     * 
     * @return int
     * @return the value of field 'maxOcorrencia'.
     */
    public int getMaxOcorrencia()
    {
        return this._maxOcorrencia;
    } //-- int getMaxOcorrencia() 

    /**
     * Returns the value of field 'perfil'.
     * 
     * @return String
     * @return the value of field 'perfil'.
     */
    public java.lang.String getPerfil()
    {
        return this._perfil;
    } //-- java.lang.String getPerfil() 

    /**
     * Returns the value of field 'processamento'.
     * 
     * @return String
     * @return the value of field 'processamento'.
     */
    public java.lang.String getProcessamento()
    {
        return this._processamento;
    } //-- java.lang.String getProcessamento() 

    /**
     * Returns the value of field 'produto'.
     * 
     * @return String
     * @return the value of field 'produto'.
     */
    public java.lang.String getProduto()
    {
        return this._produto;
    } //-- java.lang.String getProduto() 

    /**
     * Returns the value of field 'resultadoProcessamento'.
     * 
     * @return String
     * @return the value of field 'resultadoProcessamento'.
     */
    public java.lang.String getResultadoProcessamento()
    {
        return this._resultadoProcessamento;
    } //-- java.lang.String getResultadoProcessamento() 

    /**
     * Returns the value of field 'sequencia'.
     * 
     * @return int
     * @return the value of field 'sequencia'.
     */
    public int getSequencia()
    {
        return this._sequencia;
    } //-- int getSequencia() 

    /**
     * Method hasMaxOcorrencia
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasMaxOcorrencia()
    {
        return this._has_maxOcorrencia;
    } //-- boolean hasMaxOcorrencia() 

    /**
     * Method hasSequencia
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasSequencia()
    {
        return this._has_sequencia;
    } //-- boolean hasSequencia() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cpf'.
     * 
     * @param cpf the value of field 'cpf'.
     */
    public void setCpf(java.lang.String cpf)
    {
        this._cpf = cpf;
    } //-- void setCpf(java.lang.String) 

    /**
     * Sets the value of field 'dataAte'.
     * 
     * @param dataAte the value of field 'dataAte'.
     */
    public void setDataAte(java.lang.String dataAte)
    {
        this._dataAte = dataAte;
    } //-- void setDataAte(java.lang.String) 

    /**
     * Sets the value of field 'dataDe'.
     * 
     * @param dataDe the value of field 'dataDe'.
     */
    public void setDataDe(java.lang.String dataDe)
    {
        this._dataDe = dataDe;
    } //-- void setDataDe(java.lang.String) 

    /**
     * Sets the value of field 'maxOcorrencia'.
     * 
     * @param maxOcorrencia the value of field 'maxOcorrencia'.
     */
    public void setMaxOcorrencia(int maxOcorrencia)
    {
        this._maxOcorrencia = maxOcorrencia;
        this._has_maxOcorrencia = true;
    } //-- void setMaxOcorrencia(int) 

    /**
     * Sets the value of field 'perfil'.
     * 
     * @param perfil the value of field 'perfil'.
     */
    public void setPerfil(java.lang.String perfil)
    {
        this._perfil = perfil;
    } //-- void setPerfil(java.lang.String) 

    /**
     * Sets the value of field 'processamento'.
     * 
     * @param processamento the value of field 'processamento'.
     */
    public void setProcessamento(java.lang.String processamento)
    {
        this._processamento = processamento;
    } //-- void setProcessamento(java.lang.String) 

    /**
     * Sets the value of field 'produto'.
     * 
     * @param produto the value of field 'produto'.
     */
    public void setProduto(java.lang.String produto)
    {
        this._produto = produto;
    } //-- void setProduto(java.lang.String) 

    /**
     * Sets the value of field 'resultadoProcessamento'.
     * 
     * @param resultadoProcessamento the value of field
     * 'resultadoProcessamento'.
     */
    public void setResultadoProcessamento(java.lang.String resultadoProcessamento)
    {
        this._resultadoProcessamento = resultadoProcessamento;
    } //-- void setResultadoProcessamento(java.lang.String) 

    /**
     * Sets the value of field 'sequencia'.
     * 
     * @param sequencia the value of field 'sequencia'.
     */
    public void setSequencia(int sequencia)
    {
        this._sequencia = sequencia;
        this._has_sequencia = true;
    } //-- void setSequencia(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ArquivoRecebidoin
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.arquivorecebido.request.ArquivoRecebidoin unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.arquivorecebido.request.ArquivoRecebidoin) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.arquivorecebido.request.ArquivoRecebidoin.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.arquivorecebido.request.ArquivoRecebidoin unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
