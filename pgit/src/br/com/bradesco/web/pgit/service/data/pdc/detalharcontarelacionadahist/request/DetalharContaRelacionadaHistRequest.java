/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.detalharcontarelacionadahist.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class DetalharContaRelacionadaHistRequest.
 * 
 * @version $Revision$ $Date$
 */
public class DetalharContaRelacionadaHistRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _hrInclusaoRegistro
     */
    private java.lang.String _hrInclusaoRegistro;

    /**
     * Field _cdPessoaJuridicaNegocio
     */
    private long _cdPessoaJuridicaNegocio = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridicaNegocio
     */
    private boolean _has_cdPessoaJuridicaNegocio;

    /**
     * Field _cdTipoContratoNegocio
     */
    private int _cdTipoContratoNegocio = 0;

    /**
     * keeps track of state for field: _cdTipoContratoNegocio
     */
    private boolean _has_cdTipoContratoNegocio;

    /**
     * Field _nrSequenciaContratoNegocio
     */
    private long _nrSequenciaContratoNegocio = 0;

    /**
     * keeps track of state for field: _nrSequenciaContratoNegocio
     */
    private boolean _has_nrSequenciaContratoNegocio;

    /**
     * Field _cdPessoVinc
     */
    private long _cdPessoVinc = 0;

    /**
     * keeps track of state for field: _cdPessoVinc
     */
    private boolean _has_cdPessoVinc;

    /**
     * Field _cdTipoContratoVinc
     */
    private int _cdTipoContratoVinc = 0;

    /**
     * keeps track of state for field: _cdTipoContratoVinc
     */
    private boolean _has_cdTipoContratoVinc;

    /**
     * Field _nrSeqContratoVinc
     */
    private long _nrSeqContratoVinc = 0;

    /**
     * keeps track of state for field: _nrSeqContratoVinc
     */
    private boolean _has_nrSeqContratoVinc;

    /**
     * Field _cdTipoVincContrato
     */
    private int _cdTipoVincContrato = 0;

    /**
     * keeps track of state for field: _cdTipoVincContrato
     */
    private boolean _has_cdTipoVincContrato;

    /**
     * Field _cdTipoRelacionamentoConta
     */
    private int _cdTipoRelacionamentoConta = 0;

    /**
     * keeps track of state for field: _cdTipoRelacionamentoConta
     */
    private boolean _has_cdTipoRelacionamentoConta;

    /**
     * Field _cdBancoRelacionamento
     */
    private int _cdBancoRelacionamento = 0;

    /**
     * keeps track of state for field: _cdBancoRelacionamento
     */
    private boolean _has_cdBancoRelacionamento;

    /**
     * Field _cdAgenciaRelacionamento
     */
    private int _cdAgenciaRelacionamento = 0;

    /**
     * keeps track of state for field: _cdAgenciaRelacionamento
     */
    private boolean _has_cdAgenciaRelacionamento;

    /**
     * Field _cdContaRelacionamento
     */
    private long _cdContaRelacionamento = 0;

    /**
     * keeps track of state for field: _cdContaRelacionamento
     */
    private boolean _has_cdContaRelacionamento;

    /**
     * Field _digitoContaRelacionamento
     */
    private java.lang.String _digitoContaRelacionamento;

    /**
     * Field _cdTipoConta
     */
    private int _cdTipoConta = 0;

    /**
     * keeps track of state for field: _cdTipoConta
     */
    private boolean _has_cdTipoConta;

    /**
     * Field _cdCorpoCpfCnpjParticipante
     */
    private long _cdCorpoCpfCnpjParticipante = 0;

    /**
     * keeps track of state for field: _cdCorpoCpfCnpjParticipante
     */
    private boolean _has_cdCorpoCpfCnpjParticipante;

    /**
     * Field _cdFilialCpfCnpjParticipante
     */
    private int _cdFilialCpfCnpjParticipante = 0;

    /**
     * keeps track of state for field: _cdFilialCpfCnpjParticipante
     */
    private boolean _has_cdFilialCpfCnpjParticipante;

    /**
     * Field _cdDigitoCpfCnpjParticipante
     */
    private int _cdDigitoCpfCnpjParticipante = 0;

    /**
     * keeps track of state for field: _cdDigitoCpfCnpjParticipante
     */
    private boolean _has_cdDigitoCpfCnpjParticipante;

    /**
     * Field _cdPssoa
     */
    private long _cdPssoa = 0;

    /**
     * keeps track of state for field: _cdPssoa
     */
    private boolean _has_cdPssoa;


      //----------------/
     //- Constructors -/
    //----------------/

    public DetalharContaRelacionadaHistRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharcontarelacionadahist.request.DetalharContaRelacionadaHistRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdAgenciaRelacionamento
     * 
     */
    public void deleteCdAgenciaRelacionamento()
    {
        this._has_cdAgenciaRelacionamento= false;
    } //-- void deleteCdAgenciaRelacionamento() 

    /**
     * Method deleteCdBancoRelacionamento
     * 
     */
    public void deleteCdBancoRelacionamento()
    {
        this._has_cdBancoRelacionamento= false;
    } //-- void deleteCdBancoRelacionamento() 

    /**
     * Method deleteCdContaRelacionamento
     * 
     */
    public void deleteCdContaRelacionamento()
    {
        this._has_cdContaRelacionamento= false;
    } //-- void deleteCdContaRelacionamento() 

    /**
     * Method deleteCdCorpoCpfCnpjParticipante
     * 
     */
    public void deleteCdCorpoCpfCnpjParticipante()
    {
        this._has_cdCorpoCpfCnpjParticipante= false;
    } //-- void deleteCdCorpoCpfCnpjParticipante() 

    /**
     * Method deleteCdDigitoCpfCnpjParticipante
     * 
     */
    public void deleteCdDigitoCpfCnpjParticipante()
    {
        this._has_cdDigitoCpfCnpjParticipante= false;
    } //-- void deleteCdDigitoCpfCnpjParticipante() 

    /**
     * Method deleteCdFilialCpfCnpjParticipante
     * 
     */
    public void deleteCdFilialCpfCnpjParticipante()
    {
        this._has_cdFilialCpfCnpjParticipante= false;
    } //-- void deleteCdFilialCpfCnpjParticipante() 

    /**
     * Method deleteCdPessoVinc
     * 
     */
    public void deleteCdPessoVinc()
    {
        this._has_cdPessoVinc= false;
    } //-- void deleteCdPessoVinc() 

    /**
     * Method deleteCdPessoaJuridicaNegocio
     * 
     */
    public void deleteCdPessoaJuridicaNegocio()
    {
        this._has_cdPessoaJuridicaNegocio= false;
    } //-- void deleteCdPessoaJuridicaNegocio() 

    /**
     * Method deleteCdPssoa
     * 
     */
    public void deleteCdPssoa()
    {
        this._has_cdPssoa= false;
    } //-- void deleteCdPssoa() 

    /**
     * Method deleteCdTipoConta
     * 
     */
    public void deleteCdTipoConta()
    {
        this._has_cdTipoConta= false;
    } //-- void deleteCdTipoConta() 

    /**
     * Method deleteCdTipoContratoNegocio
     * 
     */
    public void deleteCdTipoContratoNegocio()
    {
        this._has_cdTipoContratoNegocio= false;
    } //-- void deleteCdTipoContratoNegocio() 

    /**
     * Method deleteCdTipoContratoVinc
     * 
     */
    public void deleteCdTipoContratoVinc()
    {
        this._has_cdTipoContratoVinc= false;
    } //-- void deleteCdTipoContratoVinc() 

    /**
     * Method deleteCdTipoRelacionamentoConta
     * 
     */
    public void deleteCdTipoRelacionamentoConta()
    {
        this._has_cdTipoRelacionamentoConta= false;
    } //-- void deleteCdTipoRelacionamentoConta() 

    /**
     * Method deleteCdTipoVincContrato
     * 
     */
    public void deleteCdTipoVincContrato()
    {
        this._has_cdTipoVincContrato= false;
    } //-- void deleteCdTipoVincContrato() 

    /**
     * Method deleteNrSeqContratoVinc
     * 
     */
    public void deleteNrSeqContratoVinc()
    {
        this._has_nrSeqContratoVinc= false;
    } //-- void deleteNrSeqContratoVinc() 

    /**
     * Method deleteNrSequenciaContratoNegocio
     * 
     */
    public void deleteNrSequenciaContratoNegocio()
    {
        this._has_nrSequenciaContratoNegocio= false;
    } //-- void deleteNrSequenciaContratoNegocio() 

    /**
     * Returns the value of field 'cdAgenciaRelacionamento'.
     * 
     * @return int
     * @return the value of field 'cdAgenciaRelacionamento'.
     */
    public int getCdAgenciaRelacionamento()
    {
        return this._cdAgenciaRelacionamento;
    } //-- int getCdAgenciaRelacionamento() 

    /**
     * Returns the value of field 'cdBancoRelacionamento'.
     * 
     * @return int
     * @return the value of field 'cdBancoRelacionamento'.
     */
    public int getCdBancoRelacionamento()
    {
        return this._cdBancoRelacionamento;
    } //-- int getCdBancoRelacionamento() 

    /**
     * Returns the value of field 'cdContaRelacionamento'.
     * 
     * @return long
     * @return the value of field 'cdContaRelacionamento'.
     */
    public long getCdContaRelacionamento()
    {
        return this._cdContaRelacionamento;
    } //-- long getCdContaRelacionamento() 

    /**
     * Returns the value of field 'cdCorpoCpfCnpjParticipante'.
     * 
     * @return long
     * @return the value of field 'cdCorpoCpfCnpjParticipante'.
     */
    public long getCdCorpoCpfCnpjParticipante()
    {
        return this._cdCorpoCpfCnpjParticipante;
    } //-- long getCdCorpoCpfCnpjParticipante() 

    /**
     * Returns the value of field 'cdDigitoCpfCnpjParticipante'.
     * 
     * @return int
     * @return the value of field 'cdDigitoCpfCnpjParticipante'.
     */
    public int getCdDigitoCpfCnpjParticipante()
    {
        return this._cdDigitoCpfCnpjParticipante;
    } //-- int getCdDigitoCpfCnpjParticipante() 

    /**
     * Returns the value of field 'cdFilialCpfCnpjParticipante'.
     * 
     * @return int
     * @return the value of field 'cdFilialCpfCnpjParticipante'.
     */
    public int getCdFilialCpfCnpjParticipante()
    {
        return this._cdFilialCpfCnpjParticipante;
    } //-- int getCdFilialCpfCnpjParticipante() 

    /**
     * Returns the value of field 'cdPessoVinc'.
     * 
     * @return long
     * @return the value of field 'cdPessoVinc'.
     */
    public long getCdPessoVinc()
    {
        return this._cdPessoVinc;
    } //-- long getCdPessoVinc() 

    /**
     * Returns the value of field 'cdPessoaJuridicaNegocio'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridicaNegocio'.
     */
    public long getCdPessoaJuridicaNegocio()
    {
        return this._cdPessoaJuridicaNegocio;
    } //-- long getCdPessoaJuridicaNegocio() 

    /**
     * Returns the value of field 'cdPssoa'.
     * 
     * @return long
     * @return the value of field 'cdPssoa'.
     */
    public long getCdPssoa()
    {
        return this._cdPssoa;
    } //-- long getCdPssoa() 

    /**
     * Returns the value of field 'cdTipoConta'.
     * 
     * @return int
     * @return the value of field 'cdTipoConta'.
     */
    public int getCdTipoConta()
    {
        return this._cdTipoConta;
    } //-- int getCdTipoConta() 

    /**
     * Returns the value of field 'cdTipoContratoNegocio'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoNegocio'.
     */
    public int getCdTipoContratoNegocio()
    {
        return this._cdTipoContratoNegocio;
    } //-- int getCdTipoContratoNegocio() 

    /**
     * Returns the value of field 'cdTipoContratoVinc'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoVinc'.
     */
    public int getCdTipoContratoVinc()
    {
        return this._cdTipoContratoVinc;
    } //-- int getCdTipoContratoVinc() 

    /**
     * Returns the value of field 'cdTipoRelacionamentoConta'.
     * 
     * @return int
     * @return the value of field 'cdTipoRelacionamentoConta'.
     */
    public int getCdTipoRelacionamentoConta()
    {
        return this._cdTipoRelacionamentoConta;
    } //-- int getCdTipoRelacionamentoConta() 

    /**
     * Returns the value of field 'cdTipoVincContrato'.
     * 
     * @return int
     * @return the value of field 'cdTipoVincContrato'.
     */
    public int getCdTipoVincContrato()
    {
        return this._cdTipoVincContrato;
    } //-- int getCdTipoVincContrato() 

    /**
     * Returns the value of field 'digitoContaRelacionamento'.
     * 
     * @return String
     * @return the value of field 'digitoContaRelacionamento'.
     */
    public java.lang.String getDigitoContaRelacionamento()
    {
        return this._digitoContaRelacionamento;
    } //-- java.lang.String getDigitoContaRelacionamento() 

    /**
     * Returns the value of field 'hrInclusaoRegistro'.
     * 
     * @return String
     * @return the value of field 'hrInclusaoRegistro'.
     */
    public java.lang.String getHrInclusaoRegistro()
    {
        return this._hrInclusaoRegistro;
    } //-- java.lang.String getHrInclusaoRegistro() 

    /**
     * Returns the value of field 'nrSeqContratoVinc'.
     * 
     * @return long
     * @return the value of field 'nrSeqContratoVinc'.
     */
    public long getNrSeqContratoVinc()
    {
        return this._nrSeqContratoVinc;
    } //-- long getNrSeqContratoVinc() 

    /**
     * Returns the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @return long
     * @return the value of field 'nrSequenciaContratoNegocio'.
     */
    public long getNrSequenciaContratoNegocio()
    {
        return this._nrSequenciaContratoNegocio;
    } //-- long getNrSequenciaContratoNegocio() 

    /**
     * Method hasCdAgenciaRelacionamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgenciaRelacionamento()
    {
        return this._has_cdAgenciaRelacionamento;
    } //-- boolean hasCdAgenciaRelacionamento() 

    /**
     * Method hasCdBancoRelacionamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdBancoRelacionamento()
    {
        return this._has_cdBancoRelacionamento;
    } //-- boolean hasCdBancoRelacionamento() 

    /**
     * Method hasCdContaRelacionamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdContaRelacionamento()
    {
        return this._has_cdContaRelacionamento;
    } //-- boolean hasCdContaRelacionamento() 

    /**
     * Method hasCdCorpoCpfCnpjParticipante
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCorpoCpfCnpjParticipante()
    {
        return this._has_cdCorpoCpfCnpjParticipante;
    } //-- boolean hasCdCorpoCpfCnpjParticipante() 

    /**
     * Method hasCdDigitoCpfCnpjParticipante
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdDigitoCpfCnpjParticipante()
    {
        return this._has_cdDigitoCpfCnpjParticipante;
    } //-- boolean hasCdDigitoCpfCnpjParticipante() 

    /**
     * Method hasCdFilialCpfCnpjParticipante
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFilialCpfCnpjParticipante()
    {
        return this._has_cdFilialCpfCnpjParticipante;
    } //-- boolean hasCdFilialCpfCnpjParticipante() 

    /**
     * Method hasCdPessoVinc
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoVinc()
    {
        return this._has_cdPessoVinc;
    } //-- boolean hasCdPessoVinc() 

    /**
     * Method hasCdPessoaJuridicaNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridicaNegocio()
    {
        return this._has_cdPessoaJuridicaNegocio;
    } //-- boolean hasCdPessoaJuridicaNegocio() 

    /**
     * Method hasCdPssoa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPssoa()
    {
        return this._has_cdPssoa;
    } //-- boolean hasCdPssoa() 

    /**
     * Method hasCdTipoConta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoConta()
    {
        return this._has_cdTipoConta;
    } //-- boolean hasCdTipoConta() 

    /**
     * Method hasCdTipoContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoNegocio()
    {
        return this._has_cdTipoContratoNegocio;
    } //-- boolean hasCdTipoContratoNegocio() 

    /**
     * Method hasCdTipoContratoVinc
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoVinc()
    {
        return this._has_cdTipoContratoVinc;
    } //-- boolean hasCdTipoContratoVinc() 

    /**
     * Method hasCdTipoRelacionamentoConta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoRelacionamentoConta()
    {
        return this._has_cdTipoRelacionamentoConta;
    } //-- boolean hasCdTipoRelacionamentoConta() 

    /**
     * Method hasCdTipoVincContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoVincContrato()
    {
        return this._has_cdTipoVincContrato;
    } //-- boolean hasCdTipoVincContrato() 

    /**
     * Method hasNrSeqContratoVinc
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSeqContratoVinc()
    {
        return this._has_nrSeqContratoVinc;
    } //-- boolean hasNrSeqContratoVinc() 

    /**
     * Method hasNrSequenciaContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaContratoNegocio()
    {
        return this._has_nrSequenciaContratoNegocio;
    } //-- boolean hasNrSequenciaContratoNegocio() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdAgenciaRelacionamento'.
     * 
     * @param cdAgenciaRelacionamento the value of field
     * 'cdAgenciaRelacionamento'.
     */
    public void setCdAgenciaRelacionamento(int cdAgenciaRelacionamento)
    {
        this._cdAgenciaRelacionamento = cdAgenciaRelacionamento;
        this._has_cdAgenciaRelacionamento = true;
    } //-- void setCdAgenciaRelacionamento(int) 

    /**
     * Sets the value of field 'cdBancoRelacionamento'.
     * 
     * @param cdBancoRelacionamento the value of field
     * 'cdBancoRelacionamento'.
     */
    public void setCdBancoRelacionamento(int cdBancoRelacionamento)
    {
        this._cdBancoRelacionamento = cdBancoRelacionamento;
        this._has_cdBancoRelacionamento = true;
    } //-- void setCdBancoRelacionamento(int) 

    /**
     * Sets the value of field 'cdContaRelacionamento'.
     * 
     * @param cdContaRelacionamento the value of field
     * 'cdContaRelacionamento'.
     */
    public void setCdContaRelacionamento(long cdContaRelacionamento)
    {
        this._cdContaRelacionamento = cdContaRelacionamento;
        this._has_cdContaRelacionamento = true;
    } //-- void setCdContaRelacionamento(long) 

    /**
     * Sets the value of field 'cdCorpoCpfCnpjParticipante'.
     * 
     * @param cdCorpoCpfCnpjParticipante the value of field
     * 'cdCorpoCpfCnpjParticipante'.
     */
    public void setCdCorpoCpfCnpjParticipante(long cdCorpoCpfCnpjParticipante)
    {
        this._cdCorpoCpfCnpjParticipante = cdCorpoCpfCnpjParticipante;
        this._has_cdCorpoCpfCnpjParticipante = true;
    } //-- void setCdCorpoCpfCnpjParticipante(long) 

    /**
     * Sets the value of field 'cdDigitoCpfCnpjParticipante'.
     * 
     * @param cdDigitoCpfCnpjParticipante the value of field
     * 'cdDigitoCpfCnpjParticipante'.
     */
    public void setCdDigitoCpfCnpjParticipante(int cdDigitoCpfCnpjParticipante)
    {
        this._cdDigitoCpfCnpjParticipante = cdDigitoCpfCnpjParticipante;
        this._has_cdDigitoCpfCnpjParticipante = true;
    } //-- void setCdDigitoCpfCnpjParticipante(int) 

    /**
     * Sets the value of field 'cdFilialCpfCnpjParticipante'.
     * 
     * @param cdFilialCpfCnpjParticipante the value of field
     * 'cdFilialCpfCnpjParticipante'.
     */
    public void setCdFilialCpfCnpjParticipante(int cdFilialCpfCnpjParticipante)
    {
        this._cdFilialCpfCnpjParticipante = cdFilialCpfCnpjParticipante;
        this._has_cdFilialCpfCnpjParticipante = true;
    } //-- void setCdFilialCpfCnpjParticipante(int) 

    /**
     * Sets the value of field 'cdPessoVinc'.
     * 
     * @param cdPessoVinc the value of field 'cdPessoVinc'.
     */
    public void setCdPessoVinc(long cdPessoVinc)
    {
        this._cdPessoVinc = cdPessoVinc;
        this._has_cdPessoVinc = true;
    } //-- void setCdPessoVinc(long) 

    /**
     * Sets the value of field 'cdPessoaJuridicaNegocio'.
     * 
     * @param cdPessoaJuridicaNegocio the value of field
     * 'cdPessoaJuridicaNegocio'.
     */
    public void setCdPessoaJuridicaNegocio(long cdPessoaJuridicaNegocio)
    {
        this._cdPessoaJuridicaNegocio = cdPessoaJuridicaNegocio;
        this._has_cdPessoaJuridicaNegocio = true;
    } //-- void setCdPessoaJuridicaNegocio(long) 

    /**
     * Sets the value of field 'cdPssoa'.
     * 
     * @param cdPssoa the value of field 'cdPssoa'.
     */
    public void setCdPssoa(long cdPssoa)
    {
        this._cdPssoa = cdPssoa;
        this._has_cdPssoa = true;
    } //-- void setCdPssoa(long) 

    /**
     * Sets the value of field 'cdTipoConta'.
     * 
     * @param cdTipoConta the value of field 'cdTipoConta'.
     */
    public void setCdTipoConta(int cdTipoConta)
    {
        this._cdTipoConta = cdTipoConta;
        this._has_cdTipoConta = true;
    } //-- void setCdTipoConta(int) 

    /**
     * Sets the value of field 'cdTipoContratoNegocio'.
     * 
     * @param cdTipoContratoNegocio the value of field
     * 'cdTipoContratoNegocio'.
     */
    public void setCdTipoContratoNegocio(int cdTipoContratoNegocio)
    {
        this._cdTipoContratoNegocio = cdTipoContratoNegocio;
        this._has_cdTipoContratoNegocio = true;
    } //-- void setCdTipoContratoNegocio(int) 

    /**
     * Sets the value of field 'cdTipoContratoVinc'.
     * 
     * @param cdTipoContratoVinc the value of field
     * 'cdTipoContratoVinc'.
     */
    public void setCdTipoContratoVinc(int cdTipoContratoVinc)
    {
        this._cdTipoContratoVinc = cdTipoContratoVinc;
        this._has_cdTipoContratoVinc = true;
    } //-- void setCdTipoContratoVinc(int) 

    /**
     * Sets the value of field 'cdTipoRelacionamentoConta'.
     * 
     * @param cdTipoRelacionamentoConta the value of field
     * 'cdTipoRelacionamentoConta'.
     */
    public void setCdTipoRelacionamentoConta(int cdTipoRelacionamentoConta)
    {
        this._cdTipoRelacionamentoConta = cdTipoRelacionamentoConta;
        this._has_cdTipoRelacionamentoConta = true;
    } //-- void setCdTipoRelacionamentoConta(int) 

    /**
     * Sets the value of field 'cdTipoVincContrato'.
     * 
     * @param cdTipoVincContrato the value of field
     * 'cdTipoVincContrato'.
     */
    public void setCdTipoVincContrato(int cdTipoVincContrato)
    {
        this._cdTipoVincContrato = cdTipoVincContrato;
        this._has_cdTipoVincContrato = true;
    } //-- void setCdTipoVincContrato(int) 

    /**
     * Sets the value of field 'digitoContaRelacionamento'.
     * 
     * @param digitoContaRelacionamento the value of field
     * 'digitoContaRelacionamento'.
     */
    public void setDigitoContaRelacionamento(java.lang.String digitoContaRelacionamento)
    {
        this._digitoContaRelacionamento = digitoContaRelacionamento;
    } //-- void setDigitoContaRelacionamento(java.lang.String) 

    /**
     * Sets the value of field 'hrInclusaoRegistro'.
     * 
     * @param hrInclusaoRegistro the value of field
     * 'hrInclusaoRegistro'.
     */
    public void setHrInclusaoRegistro(java.lang.String hrInclusaoRegistro)
    {
        this._hrInclusaoRegistro = hrInclusaoRegistro;
    } //-- void setHrInclusaoRegistro(java.lang.String) 

    /**
     * Sets the value of field 'nrSeqContratoVinc'.
     * 
     * @param nrSeqContratoVinc the value of field
     * 'nrSeqContratoVinc'.
     */
    public void setNrSeqContratoVinc(long nrSeqContratoVinc)
    {
        this._nrSeqContratoVinc = nrSeqContratoVinc;
        this._has_nrSeqContratoVinc = true;
    } //-- void setNrSeqContratoVinc(long) 

    /**
     * Sets the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @param nrSequenciaContratoNegocio the value of field
     * 'nrSequenciaContratoNegocio'.
     */
    public void setNrSequenciaContratoNegocio(long nrSequenciaContratoNegocio)
    {
        this._nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
        this._has_nrSequenciaContratoNegocio = true;
    } //-- void setNrSequenciaContratoNegocio(long) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return DetalharContaRelacionadaHistRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.detalharcontarelacionadahist.request.DetalharContaRelacionadaHistRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.detalharcontarelacionadahist.request.DetalharContaRelacionadaHistRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.detalharcontarelacionadahist.request.DetalharContaRelacionadaHistRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharcontarelacionadahist.request.DetalharContaRelacionadaHistRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
