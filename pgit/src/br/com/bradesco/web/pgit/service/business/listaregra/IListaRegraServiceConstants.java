/*
 * =========================================================================
 * 
 * Client:       Bradesco (BR)
 * Project:      Arquitetura Bradesco Canal Internet
 * Development:  GFT Iberia (http://www.gft.com)
 * -------------------------------------------------------------------------
 * Revision - Last:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PilotoIntranet/JavaSource/br/com/bradesco/web/pgit/service/business/listaregra/IListaRegraServiceConstants.java,v $
 * $Id: IListaRegraServiceConstants.java,v 1.1 2009/05/22 18:35:57 corporate\marcio.alves Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revision - History:
 * $Log: IListaRegraServiceConstants.java,v $
 * Revision 1.1  2009/05/22 18:35:57  corporate\marcio.alves
 * Adicionando o servico listaregra para teste do pdc
 *
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.listaregra;


/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Interface de constantes do adaptador: ListaRegra
 * </p>
 * 
 * @comment C�DIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public interface IListaRegraServiceConstants {

}

