/*
 * Nome: br.com.bradesco.web.pgit.service.business.manterfavorecido.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.manterfavorecido.bean;

/**
 * Nome: ConsultarListaHistoricoContaFavorecidoEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarListaHistoricoContaFavorecidoEntradaDTO {

    /** Atributo cdPessoaJuridicaContrato. */
    private Long cdPessoaJuridicaContrato;

    /** Atributo cdTipoContratoNegocio. */
    private Integer cdTipoContratoNegocio;

    /** Atributo nrSequenciaContratoNegocio. */
    private Long nrSequenciaContratoNegocio;

    /** Atributo cdFavorecidoClientePagador. */
    private Long cdFavorecidoClientePagador;

    /** Atributo nrOcorrenciaContaFavorecido. */
    private Integer nrOcorrenciaContaFavorecido;

    /** Atributo cdBanco. */
    private Integer cdBanco;

    /** Atributo cdAgencia. */
    private Integer cdAgencia;

    /** Atributo cdDigitoAgencia. */
    private Integer cdDigitoAgencia;

    /** Atributo cdConta. */
    private Long cdConta;

    /** Atributo cdDigitoConta. */
    private String cdDigitoConta;

    /** Atributo cdTipoConta. */
    private Integer cdTipoConta;

    /** Atributo dataInicio. */
    private String dataInicio;

    /** Atributo dataFim. */
    private String dataFim;

    /** Atributo cdPesquisaLista. */
    private Integer cdPesquisaLista;

    /**
     * Consultar lista historico conta favorecido entrada dto.
     */
    public ConsultarListaHistoricoContaFavorecidoEntradaDTO() {
	super();
    }

    /**
     * Get: cdFavorecidoClientePagador.
     *
     * @return cdFavorecidoClientePagador
     */
    public Long getCdFavorecidoClientePagador() {
	return cdFavorecidoClientePagador;
    }

    /**
     * Set: cdFavorecidoClientePagador.
     *
     * @param cdFavorecidoClientePagador the cd favorecido cliente pagador
     */
    public void setCdFavorecidoClientePagador(Long cdFavorecidoClientePagador) {
	this.cdFavorecidoClientePagador = cdFavorecidoClientePagador;
    }

    /**
     * Get: cdPessoaJuridicaContrato.
     *
     * @return cdPessoaJuridicaContrato
     */
    public Long getCdPessoaJuridicaContrato() {
	return cdPessoaJuridicaContrato;
    }

    /**
     * Set: cdPessoaJuridicaContrato.
     *
     * @param cdPessoaJuridicaContrato the cd pessoa juridica contrato
     */
    public void setCdPessoaJuridicaContrato(Long cdPessoaJuridicaContrato) {
	this.cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
    }

    /**
     * Get: cdTipoContratoNegocio.
     *
     * @return cdTipoContratoNegocio
     */
    public Integer getCdTipoContratoNegocio() {
	return cdTipoContratoNegocio;
    }

    /**
     * Set: cdTipoContratoNegocio.
     *
     * @param cdTipoContratoNegocio the cd tipo contrato negocio
     */
    public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
	this.cdTipoContratoNegocio = cdTipoContratoNegocio;
    }

    /**
     * Get: nrOcorrenciaContaFavorecido.
     *
     * @return nrOcorrenciaContaFavorecido
     */
    public Integer getNrOcorrenciaContaFavorecido() {
	return nrOcorrenciaContaFavorecido;
    }

    /**
     * Set: nrOcorrenciaContaFavorecido.
     *
     * @param nrOcorrenciaContaFavorecido the nr ocorrencia conta favorecido
     */
    public void setNrOcorrenciaContaFavorecido(Integer nrOcorrenciaContaFavorecido) {
	this.nrOcorrenciaContaFavorecido = nrOcorrenciaContaFavorecido;
    }

    /**
     * Get: nrSequenciaContratoNegocio.
     *
     * @return nrSequenciaContratoNegocio
     */
    public Long getNrSequenciaContratoNegocio() {
	return nrSequenciaContratoNegocio;
    }

    /**
     * Set: nrSequenciaContratoNegocio.
     *
     * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
     */
    public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
	this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
    }

    /**
     * Get: cdAgencia.
     *
     * @return cdAgencia
     */
    public Integer getCdAgencia() {
	return cdAgencia;
    }

    /**
     * Set: cdAgencia.
     *
     * @param cdAgencia the cd agencia
     */
    public void setCdAgencia(Integer cdAgencia) {
	this.cdAgencia = cdAgencia;
    }

    /**
     * Get: cdBanco.
     *
     * @return cdBanco
     */
    public Integer getCdBanco() {
	return cdBanco;
    }

    /**
     * Set: cdBanco.
     *
     * @param cdBanco the cd banco
     */
    public void setCdBanco(Integer cdBanco) {
	this.cdBanco = cdBanco;
    }

    /**
     * Get: cdConta.
     *
     * @return cdConta
     */
    public Long getCdConta() {
	return cdConta;
    }

    /**
     * Set: cdConta.
     *
     * @param cdConta the cd conta
     */
    public void setCdConta(Long cdConta) {
	this.cdConta = cdConta;
    }

    /**
     * Get: cdDigitoAgencia.
     *
     * @return cdDigitoAgencia
     */
    public Integer getCdDigitoAgencia() {
	return cdDigitoAgencia;
    }

    /**
     * Set: cdDigitoAgencia.
     *
     * @param cdDigitoAgencia the cd digito agencia
     */
    public void setCdDigitoAgencia(Integer cdDigitoAgencia) {
	this.cdDigitoAgencia = cdDigitoAgencia;
    }

    /**
     * Get: cdDigitoConta.
     *
     * @return cdDigitoConta
     */
    public String getCdDigitoConta() {
	return cdDigitoConta;
    }

    /**
     * Set: cdDigitoConta.
     *
     * @param cdDigitoConta the cd digito conta
     */
    public void setCdDigitoConta(String cdDigitoConta) {
	this.cdDigitoConta = cdDigitoConta;
    }

    /**
     * Get: cdPesquisaLista.
     *
     * @return cdPesquisaLista
     */
    public Integer getCdPesquisaLista() {
	return cdPesquisaLista;
    }

    /**
     * Set: cdPesquisaLista.
     *
     * @param cdPesquisaLista the cd pesquisa lista
     */
    public void setCdPesquisaLista(Integer cdPesquisaLista) {
	this.cdPesquisaLista = cdPesquisaLista;
    }

    /**
     * Get: cdTipoConta.
     *
     * @return cdTipoConta
     */
    public Integer getCdTipoConta() {
	return cdTipoConta;
    }

    /**
     * Set: cdTipoConta.
     *
     * @param cdTipoConta the cd tipo conta
     */
    public void setCdTipoConta(Integer cdTipoConta) {
	this.cdTipoConta = cdTipoConta;
    }

    /**
     * Get: dataFim.
     *
     * @return dataFim
     */
    public String getDataFim() {
	return dataFim;
    }

    /**
     * Set: dataFim.
     *
     * @param dataFim the data fim
     */
    public void setDataFim(String dataFim) {
	this.dataFim = dataFim;
    }

    /**
     * Get: dataInicio.
     *
     * @return dataInicio
     */
    public String getDataInicio() {
	return dataInicio;
    }

    /**
     * Set: dataInicio.
     *
     * @param dataInicio the data inicio
     */
    public void setDataInicio(String dataInicio) {
	this.dataInicio = dataInicio;
    }

}
