/*
 * Nome: br.com.bradesco.web.pgit.service.business.contrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 19/02/2016
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.contrato.bean;

/**
 * Nome: ImprimirContratoOcorrencias5SaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ImprimirContratoOcorrencias5SaidaDTO {

    /** The ds produto salario auto atendimento. */
    private String dsProdutoSalarioAutoAtendimento = null;

    /** The ds servico salario auto atendimento. */
    private String dsServicoSalarioAutoAtendimento = null;

    /**
     * Instancia um novo ImprimirContratoOcorrencias5SaidaDTO.
     * 
     * @see
     */
    public ImprimirContratoOcorrencias5SaidaDTO() {
        super();
    }

    /**
     * Sets the ds produto salario auto atendimento.
     *
     * @param dsProdutoSalarioAutoAtendimento the ds produto salario auto atendimento
     */
    public void setDsProdutoSalarioAutoAtendimento(String dsProdutoSalarioAutoAtendimento) {
        this.dsProdutoSalarioAutoAtendimento = dsProdutoSalarioAutoAtendimento;
    }

    /**
     * Gets the ds produto salario auto atendimento.
     *
     * @return the ds produto salario auto atendimento
     */
    public String getDsProdutoSalarioAutoAtendimento() {
        return this.dsProdutoSalarioAutoAtendimento;
    }

    /**
     * Sets the ds servico salario auto atendimento.
     *
     * @param dsServicoSalarioAutoAtendimento the ds servico salario auto atendimento
     */
    public void setDsServicoSalarioAutoAtendimento(String dsServicoSalarioAutoAtendimento) {
        this.dsServicoSalarioAutoAtendimento = dsServicoSalarioAutoAtendimento;
    }

    /**
     * Gets the ds servico salario auto atendimento.
     *
     * @return the ds servico salario auto atendimento
     */
    public String getDsServicoSalarioAutoAtendimento() {
        return this.dsServicoSalarioAutoAtendimento;
    }
}