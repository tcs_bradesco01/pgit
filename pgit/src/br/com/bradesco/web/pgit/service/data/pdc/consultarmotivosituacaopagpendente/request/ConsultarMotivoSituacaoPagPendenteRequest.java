/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultarmotivosituacaopagpendente.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ConsultarMotivoSituacaoPagPendenteRequest.
 * 
 * @version $Revision$ $Date$
 */
public class ConsultarMotivoSituacaoPagPendenteRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _nrOcorrencias
     */
    private int _nrOcorrencias = 0;

    /**
     * keeps track of state for field: _nrOcorrencias
     */
    private boolean _has_nrOcorrencias;

    /**
     * Field _cdSituacaoPagamento
     */
    private int _cdSituacaoPagamento = 0;

    /**
     * keeps track of state for field: _cdSituacaoPagamento
     */
    private boolean _has_cdSituacaoPagamento;


      //----------------/
     //- Constructors -/
    //----------------/

    public ConsultarMotivoSituacaoPagPendenteRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarmotivosituacaopagpendente.request.ConsultarMotivoSituacaoPagPendenteRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdSituacaoPagamento
     * 
     */
    public void deleteCdSituacaoPagamento()
    {
        this._has_cdSituacaoPagamento= false;
    } //-- void deleteCdSituacaoPagamento() 

    /**
     * Method deleteNrOcorrencias
     * 
     */
    public void deleteNrOcorrencias()
    {
        this._has_nrOcorrencias= false;
    } //-- void deleteNrOcorrencias() 

    /**
     * Returns the value of field 'cdSituacaoPagamento'.
     * 
     * @return int
     * @return the value of field 'cdSituacaoPagamento'.
     */
    public int getCdSituacaoPagamento()
    {
        return this._cdSituacaoPagamento;
    } //-- int getCdSituacaoPagamento() 

    /**
     * Returns the value of field 'nrOcorrencias'.
     * 
     * @return int
     * @return the value of field 'nrOcorrencias'.
     */
    public int getNrOcorrencias()
    {
        return this._nrOcorrencias;
    } //-- int getNrOcorrencias() 

    /**
     * Method hasCdSituacaoPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSituacaoPagamento()
    {
        return this._has_cdSituacaoPagamento;
    } //-- boolean hasCdSituacaoPagamento() 

    /**
     * Method hasNrOcorrencias
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrOcorrencias()
    {
        return this._has_nrOcorrencias;
    } //-- boolean hasNrOcorrencias() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdSituacaoPagamento'.
     * 
     * @param cdSituacaoPagamento the value of field
     * 'cdSituacaoPagamento'.
     */
    public void setCdSituacaoPagamento(int cdSituacaoPagamento)
    {
        this._cdSituacaoPagamento = cdSituacaoPagamento;
        this._has_cdSituacaoPagamento = true;
    } //-- void setCdSituacaoPagamento(int) 

    /**
     * Sets the value of field 'nrOcorrencias'.
     * 
     * @param nrOcorrencias the value of field 'nrOcorrencias'.
     */
    public void setNrOcorrencias(int nrOcorrencias)
    {
        this._nrOcorrencias = nrOcorrencias;
        this._has_nrOcorrencias = true;
    } //-- void setNrOcorrencias(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ConsultarMotivoSituacaoPagPendenteRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultarmotivosituacaopagpendente.request.ConsultarMotivoSituacaoPagPendenteRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultarmotivosituacaopagpendente.request.ConsultarMotivoSituacaoPagPendenteRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultarmotivosituacaopagpendente.request.ConsultarMotivoSituacaoPagPendenteRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarmotivosituacaopagpendente.request.ConsultarMotivoSituacaoPagPendenteRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
