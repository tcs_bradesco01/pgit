/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/implementation.ftl,v $
 * $Id: implementation.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: implementation.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */
 
package br.com.bradesco.web.pgit.service.business.duparqretorno.impl;

import java.util.ArrayList;
import java.util.List;

import br.com.bradesco.web.pgit.service.business.duplarqretorno.IDuplArqRetornoService;
import br.com.bradesco.web.pgit.service.business.duplarqretorno.IDuplArqRetornoServiceConstants;
import br.com.bradesco.web.pgit.service.business.duplarqretorno.bean.AlterarDuplicacaoArqRetornoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.duplarqretorno.bean.AlterarDuplicacaoArqRetornoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.duplarqretorno.bean.DetalharDuplicacaoArqRetornoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.duplarqretorno.bean.DetalharDuplicacaoArqRetornoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.duplarqretorno.bean.DuplicacaoArquivoRetornoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.duplarqretorno.bean.DuplicacaoArquivoRetornoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.duplarqretorno.bean.ExcluirDuplicacaoArqRetornoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.duplarqretorno.bean.ExcluirDuplicacaoArqRetornoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.duplarqretorno.bean.IncluirDuplicacaoArqRetornoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.duplarqretorno.bean.IncluirDuplicacaoArqRetornoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaContratosPessoasEntradaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaContratosPessoasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.impl.FiltroIdentificaoServiceImpl;
import br.com.bradesco.web.pgit.service.data.pdc.FactoryAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.alterarduplicacaoarqretorno.request.AlterarDuplicacaoArqRetornoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.alterarduplicacaoarqretorno.response.AlterarDuplicacaoArqRetornoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.detalharduplicacaoarqretorno.request.DetalharDuplicacaoArqRetornoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.detalharduplicacaoarqretorno.response.DetalharDuplicacaoArqRetornoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.excluirduplicacaoarqretorno.request.ExcluirDuplicacaoArqRetornoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.excluirduplicacaoarqretorno.response.ExcluirDuplicacaoArqRetornoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.incluirduplicacaoarqretorno.request.IncluirDuplicacaoArqRetornoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.incluirduplicacaoarqretorno.response.IncluirDuplicacaoArqRetornoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarduplicacaoarqretorno.request.ListarDuplicacaoArqRetornoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarduplicacaoarqretorno.response.ListarDuplicacaoArqRetornoResponse;
import br.com.bradesco.web.pgit.utils.CpfCnpjUtils;
import br.com.bradesco.web.pgit.utils.PgitUtil;


/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Implementa��o do adaptador: DuplicacaoArquivoRetorno
 * </p>
 * 
 * @comment C�DIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public class DuplArqRetornoServiceImpl implements IDuplArqRetornoService {

	/** Atributo factoryAdapter. */
	private FactoryAdapter factoryAdapter;
	
	/** Atributo filtroIdentificaoServiceImpl. */
	private FiltroIdentificaoServiceImpl filtroIdentificaoServiceImpl; 
	
    /**
     * Get: filtroIdentificaoServiceImpl.
     *
     * @return filtroIdentificaoServiceImpl
     */
    public FiltroIdentificaoServiceImpl getFiltroIdentificaoServiceImpl() {
		return filtroIdentificaoServiceImpl;
	}

	/**
	 * Set: filtroIdentificaoServiceImpl.
	 *
	 * @param filtroIdentificaoServiceImpl the filtro identificao service impl
	 */
	public void setFiltroIdentificaoServiceImpl(
			FiltroIdentificaoServiceImpl filtroIdentificaoServiceImpl) {
		this.filtroIdentificaoServiceImpl = filtroIdentificaoServiceImpl;
	}

	/**
     * Construtor.
     */
    public DuplArqRetornoServiceImpl() {
        // TODO: Implementa��o
    }
    
    /**
     * M�todo de exemplo.
     *
     * @see br.com.bradesco.web.pgit.service.business.duplarqretorno.IDuplicacaoArquivoRetorno#sampleDuplicacaoArquivoRetorno()
     */
    public void sampleDuplicacaoArquivoRetorno() {
        // TODO: Implementa�ao
    }

	/**
	 * Get: factoryAdapter.
	 *
	 * @return factoryAdapter
	 */
	public FactoryAdapter getFactoryAdapter() {
		return factoryAdapter;
	}

	/**
	 * Set: factoryAdapter.
	 *
	 * @param factoryAdapter the factory adapter
	 */
	public void setFactoryAdapter(FactoryAdapter factoryAdapter) {
		this.factoryAdapter = factoryAdapter;
	}
	
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.duplarqretorno.IDuplArqRetornoService#listarDuplicacaoArqRetorno(br.com.bradesco.web.pgit.service.business.duplarqretorno.bean.DuplicacaoArquivoRetornoEntradaDTO)
	 */
	public List<DuplicacaoArquivoRetornoSaidaDTO> listarDuplicacaoArqRetorno(DuplicacaoArquivoRetornoEntradaDTO duplicacaoArquivoRetornoEntradaDTO) {
		
		List<DuplicacaoArquivoRetornoSaidaDTO> listaRetorno = new ArrayList<DuplicacaoArquivoRetornoSaidaDTO>();		
		ListarDuplicacaoArqRetornoRequest request = new ListarDuplicacaoArqRetornoRequest();
		ListarDuplicacaoArqRetornoResponse response = new ListarDuplicacaoArqRetornoResponse();
		
		request.setCdPessoaJuridContrato(PgitUtil.verificaLongNulo(duplicacaoArquivoRetornoEntradaDTO.getCdPessoaJuridContrato()));
		request.setCdTipoArquivoRetorno(PgitUtil.verificaIntegerNulo(duplicacaoArquivoRetornoEntradaDTO.getCdTipoArquivoRetorno()));
		request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(duplicacaoArquivoRetornoEntradaDTO.getCdTipoContratoNegocio()));
		request.setCdTipoLayoutArquivo(PgitUtil.verificaIntegerNulo(duplicacaoArquivoRetornoEntradaDTO.getCdTipoLayoutArquivo()));
		request.setNrSeqContratoNegocio(PgitUtil.verificaLongNulo(duplicacaoArquivoRetornoEntradaDTO.getNrSeqContratoNegocio()));
		request.setQtConsultas(IDuplArqRetornoServiceConstants.QTDE_CONSULTAS_LISTAR);

		response = getFactoryAdapter().getListarDuplicacaoArqRetornoPDCAdapter().invokeProcess(request);

		DuplicacaoArquivoRetornoSaidaDTO saidaDTO;
		
		for (int i=0; i<response.getOcorrenciasCount();i++){
			saidaDTO = new DuplicacaoArquivoRetornoSaidaDTO();
			saidaDTO.setCodMensagem(response.getCodMensagem());
			saidaDTO.setMensagem(response.getMensagem());
			saidaDTO.setCdIndicadorDuplicidadeRetorno(response.getOcorrencias(i).getCdIndicadorDuplicidadeRetorno());
			saidaDTO.setCdInstrucaoEnvioRetorno(response.getOcorrencias(i).getCdInstrucaoEnvioRetorno());
			saidaDTO.setCdTipoArquivoRetorno(response.getOcorrencias(i).getCdTipoArquivoRetorno());
			saidaDTO.setCdPessoaJuridContrato(response.getOcorrencias(i).getCdPessoaJuridContrato());
			saidaDTO.setCdTipoContratoNegocio(response.getOcorrencias(i).getCdTipoContratoNegocio());
			saidaDTO.setCdTipoLayoutArquivo(response.getOcorrencias(i).getCdTipoLayoutArquivo());
			saidaDTO.setDsTipoArquivoRetorno(response.getOcorrencias(i).getDsTipoArquivoRetorno());
			saidaDTO.setDsTipoLayoutArquivo(response.getOcorrencias(i).getDsTipoLayoutArquivo());
			saidaDTO.setNrSeqContratoNegocio(response.getOcorrencias(i).getNrSeqContratoNegocio());
			saidaDTO.setCdPessoaJuridVinc(response.getOcorrencias(i).getCdPessoaJuridVinc());
			saidaDTO.setCdTipoContratoVinc(response.getOcorrencias(i).getCdTipoContratoVinc());
			saidaDTO.setNrSeqContratoVinc(response.getOcorrencias(i).getNrSeqContratoVinc());
			
			ConsultarListaContratosPessoasEntradaDTO consultarListaContratosPessoasEntradaDTO = new ConsultarListaContratosPessoasEntradaDTO();
			consultarListaContratosPessoasEntradaDTO.setCdPessoaJuridicaContrato(Long.valueOf(response.getOcorrencias(i).getCdPessoaJuridVinc()));
			consultarListaContratosPessoasEntradaDTO.setCdTipoContratoNegocio(response.getOcorrencias(i).getCdTipoContratoVinc());
			consultarListaContratosPessoasEntradaDTO.setNrSeqContratoNegocio(Long.valueOf(response.getOcorrencias(i).getNrSeqContratoVinc()));
			
			consultarListaContratosPessoasEntradaDTO.setCdClub(PgitUtil.verificaLongNulo(duplicacaoArquivoRetornoEntradaDTO.getCdClub()));
			 
			ConsultarListaContratosPessoasSaidaDTO consultarListaContratosPessoasSaidaDTO = filtroIdentificaoServiceImpl.detalharContratoOrigem(consultarListaContratosPessoasEntradaDTO);
			saidaDTO.setDsPessoaJuridVinc(consultarListaContratosPessoasSaidaDTO.getDsPessoaJuridicaContrato());
			saidaDTO.setDsTipoContratoVinc(consultarListaContratosPessoasSaidaDTO.getDsTipoContratoNegocio());
			saidaDTO.setNrSeqContratoVinc(consultarListaContratosPessoasSaidaDTO.getNrSeqContratoNegocio());
			
			listaRetorno.add(saidaDTO);
			
		}
		
		return listaRetorno;
	}
    
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.duplarqretorno.IDuplArqRetornoService#detalharDuplicacaoArqRetorno(br.com.bradesco.web.pgit.service.business.duplarqretorno.bean.DetalharDuplicacaoArqRetornoEntradaDTO)
	 */
	public DetalharDuplicacaoArqRetornoSaidaDTO detalharDuplicacaoArqRetorno(DetalharDuplicacaoArqRetornoEntradaDTO detalharDuplicacaoArqRetornoEntradaDTO) {
		
		DetalharDuplicacaoArqRetornoSaidaDTO saida = new DetalharDuplicacaoArqRetornoSaidaDTO();
		
		DetalharDuplicacaoArqRetornoRequest request = new DetalharDuplicacaoArqRetornoRequest();
		DetalharDuplicacaoArqRetornoResponse response = new DetalharDuplicacaoArqRetornoResponse();
		
		request.setCdPessoaJuridVinc(PgitUtil.verificaLongNulo(detalharDuplicacaoArqRetornoEntradaDTO.getCdPessoaJuridVinc()));
		request.setCdTipoContratoVinc(PgitUtil.verificaIntegerNulo(detalharDuplicacaoArqRetornoEntradaDTO.getCdTipoContratoVinc()));
		request.setNrSeqContratoVinc(PgitUtil.verificaLongNulo(detalharDuplicacaoArqRetornoEntradaDTO.getNrSeqContratoVinc()));
		request.setCdPessoaJuridContrato(PgitUtil.verificaLongNulo(detalharDuplicacaoArqRetornoEntradaDTO.getCdPessoaJuridContrato()));
		request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(detalharDuplicacaoArqRetornoEntradaDTO.getCdTipoContratoNegocio()));
		request.setNrSeqContratoNegocio(PgitUtil.verificaLongNulo(detalharDuplicacaoArqRetornoEntradaDTO.getNrSeqContratoNegocio()));
		request.setCdTipoLayoutArquivo(PgitUtil.verificaIntegerNulo(detalharDuplicacaoArqRetornoEntradaDTO.getCdTipoLayoutArquivo()));
		request.setCdTipoArquivoRetorno(PgitUtil.verificaIntegerNulo(detalharDuplicacaoArqRetornoEntradaDTO.getCdTipoArquivoRetorno()));
		request.setCdTipoParticipacaoPessoa(PgitUtil.verificaIntegerNulo(detalharDuplicacaoArqRetornoEntradaDTO.getCdTipoParticipacaoPessoa()));
		request.setCdPessoa(PgitUtil.verificaLongNulo(detalharDuplicacaoArqRetornoEntradaDTO.getCdPessoa()));
		request.setCdInstrucaoEnvioRetorno(PgitUtil.verificaIntegerNulo(detalharDuplicacaoArqRetornoEntradaDTO.getCdInstrucaoEnvioRetorno()));
		request.setCdIndicadorDuplicidadeRetorno(PgitUtil.verificaIntegerNulo(detalharDuplicacaoArqRetornoEntradaDTO.getCdIndicadorDuplicidadeRetorno()));
		request.setCdProdutoServicoOper(PgitUtil.verificaIntegerNulo(detalharDuplicacaoArqRetornoEntradaDTO.getCdProdutoServicoOper()));
		
		response = getFactoryAdapter().getDetalharDuplicacaoArqRetornoPDCAdapter().invokeProcess(request);
	
		saida = new DetalharDuplicacaoArqRetornoSaidaDTO();
		saida.setCodMensagem(response.getCodMensagem());
		saida.setMensagem(response.getMensagem());
		
		saida.setCodMensagem(response.getCodMensagem());
		saida.setMensagem(response.getMensagem());
		saida.setCdPessoaJuridVinc(response.getCdPessoaJuridVinc());
		saida.setCdTipoContratoVinc(response.getCdTipoContratoVinc());
		saida.setNrSeqContratoVinc(response.getNrSeqContratoVinc());
		saida.setCdPessoaJuridContrato(response.getCdPessoaJuridContrato());
		saida.setCdTipoContratoNegocio(response.getCdTipoContratoNegocio());
		saida.setNrSeqContratoNegocio(response.getNrSeqContratoNegocio());
		saida.setCdTipoLayoutArquivo(response.getCdTipoLayoutArquivo());
		saida.setDsTipoLayoutArquivo(response.getDsTipoLayoutArquivo());
		saida.setCdTipoArquivoRetorno(response.getCdTipoArquivoRetorno());
		saida.setDsTipoArquivoRetorno(response.getDsTipoArquivoRetorno());
		saida.setCdTipoParticipacaoPessoa(response.getCdTipoParticipacaoPessoa());
		saida.setCdPessoa(response.getCdPessoa());
		saida.setDsPessoa(response.getDsPessoa());
		saida.setCdInstrucaoEnvioRetorno(response.getCdInstrucaoEnvioRetorno());
		saida.setCdIndicadorDuplicidadeRetorno(response.getCdIndicadorDuplicidadeRetorno());
		saida.setCdCanalInclusao(response.getCdCanalInclusao());
		saida.setDsCanalInclusao(response.getDsCanalInclusao());
		saida.setCdAutenSegrcInclusao(response.getCdAutenSegrcInclusao());
		saida.setNmOperFluxoInclusao(response.getNmOperFluxoInclusao());
		saida.setHrInclusaoRegistro(response.getHrInclusaoRegistro());
		saida.setCdCanalManutencao(response.getCdCanalManutencao());
		saida.setDsCanalManutencao(response.getDsCanalManutencao());
		saida.setCdAutenSegrcManutencao(response.getCdAutenSegrcManutencao());
		saida.setNmOperFluxoManutencao(response.getNmOperFluxoManutencao());
		saida.setHrManutencaoRegistro(response.getHrManutencaoRegistro());
		saida.setCpfCnpjParticipante(CpfCnpjUtils.formatCpfCnpjCompleto(response.getCdCnpjParticipante(), response.getCdFilialParticipante(),response.getCdControlePArticipante()));
	
		return saida;
		
	}
	
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.duplarqretorno.IDuplArqRetornoService#excluirDuplicacaoArqRetorno(br.com.bradesco.web.pgit.service.business.duplarqretorno.bean.ExcluirDuplicacaoArqRetornoEntradaDTO)
	 */
	public ExcluirDuplicacaoArqRetornoSaidaDTO excluirDuplicacaoArqRetorno(ExcluirDuplicacaoArqRetornoEntradaDTO excluirDuplicacaoArqRetornoEntradaDTO) {
		ExcluirDuplicacaoArqRetornoSaidaDTO excluirDuplicacaoArqRetornoSaidaDTO = new ExcluirDuplicacaoArqRetornoSaidaDTO();
		ExcluirDuplicacaoArqRetornoRequest request = new ExcluirDuplicacaoArqRetornoRequest();
		ExcluirDuplicacaoArqRetornoResponse response = new ExcluirDuplicacaoArqRetornoResponse();
		
		request.setCdPessoaJuridVinc(PgitUtil.verificaLongNulo(excluirDuplicacaoArqRetornoEntradaDTO.getCdPessoaJuridVinc()));
		request.setCdTipoContratoVinc(PgitUtil.verificaIntegerNulo(excluirDuplicacaoArqRetornoEntradaDTO.getCdTipoContratoVinc()));
		request.setNrSeqContratoVinc(PgitUtil.verificaLongNulo(excluirDuplicacaoArqRetornoEntradaDTO.getNrSeqContratoVinc()));		
		request.setCdPessoaJuridContrato(PgitUtil.verificaLongNulo(excluirDuplicacaoArqRetornoEntradaDTO.getCdPessoaJuridContrato()));
		request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(excluirDuplicacaoArqRetornoEntradaDTO.getCdTipoContratoNegocio()));
		request.setNrSeqContratoNegocio(PgitUtil.verificaLongNulo(excluirDuplicacaoArqRetornoEntradaDTO.getNrSeqContratoNegocio()));
		request.setCdTipoLayoutArquivo(PgitUtil.verificaIntegerNulo(excluirDuplicacaoArqRetornoEntradaDTO.getCdTipoLayoutArquivo()));
		request.setCdTipoArquivoRetorno(PgitUtil.verificaIntegerNulo(excluirDuplicacaoArqRetornoEntradaDTO.getCdTipoArquivoRetorno()));
		request.setCdTipoParticipacaoPessoa(PgitUtil.verificaIntegerNulo(excluirDuplicacaoArqRetornoEntradaDTO.getCdTipoParticipacaoPessoa()));
		request.setCdPessoa(PgitUtil.verificaLongNulo(excluirDuplicacaoArqRetornoEntradaDTO.getCdPessoa()));
		request.setCdInstrucaoEnvioRetorno(PgitUtil.verificaIntegerNulo(excluirDuplicacaoArqRetornoEntradaDTO.getCdInstrucaoEnvioRetorno()));
		request.setCdIndicadorDuplicidadeRetorno(PgitUtil.verificaIntegerNulo(excluirDuplicacaoArqRetornoEntradaDTO.getCdIndicadorDuplicidadeRetorno()));
		request.setCdProdutoServicoOper(PgitUtil.verificaIntegerNulo(excluirDuplicacaoArqRetornoEntradaDTO.getCdProdutoServicoOper()));
		
		response = getFactoryAdapter().getExcluirDuplicacaoArqRetornoPDCAdapter().invokeProcess(request);
	
		excluirDuplicacaoArqRetornoSaidaDTO.setCodMensagem(response.getCodMensagem());
		excluirDuplicacaoArqRetornoSaidaDTO.setMensagem(response.getMensagem());
	
		return excluirDuplicacaoArqRetornoSaidaDTO;
	}


	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.duplarqretorno.IDuplArqRetornoService#alterarDuplicacaoArqRetorno(br.com.bradesco.web.pgit.service.business.duplarqretorno.bean.AlterarDuplicacaoArqRetornoEntradaDTO)
	 */
	public AlterarDuplicacaoArqRetornoSaidaDTO alterarDuplicacaoArqRetorno(AlterarDuplicacaoArqRetornoEntradaDTO alterarDuplicacaoArqRetornoEntradaDTO) {
		
		AlterarDuplicacaoArqRetornoRequest request = new AlterarDuplicacaoArqRetornoRequest();
		AlterarDuplicacaoArqRetornoResponse response = new AlterarDuplicacaoArqRetornoResponse();
		AlterarDuplicacaoArqRetornoSaidaDTO alterarDuplicacaoArqRetornoSaidaDTO = new AlterarDuplicacaoArqRetornoSaidaDTO();
		
		request.setCdPessoaJuridVinc(PgitUtil.verificaLongNulo(alterarDuplicacaoArqRetornoEntradaDTO.getCdPessoaJuridVinc()));
		request.setCdTipoContratoVinc(PgitUtil.verificaIntegerNulo(alterarDuplicacaoArqRetornoEntradaDTO.getCdTipoContratoVinc()));
		request.setNrSeqContratoVinc(PgitUtil.verificaLongNulo(alterarDuplicacaoArqRetornoEntradaDTO.getNrSeqContratoVinc()));
		request.setCdPessoaJuridContrato(PgitUtil.verificaLongNulo(alterarDuplicacaoArqRetornoEntradaDTO.getCdPessoaJuridContrato()));
		request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(alterarDuplicacaoArqRetornoEntradaDTO.getCdTipoContratoNegocio()));
		request.setNrSeqContratoNegocio(PgitUtil.verificaLongNulo(alterarDuplicacaoArqRetornoEntradaDTO.getNrSeqContratoNegocio()));
		request.setCdTipoLayoutArquivo(PgitUtil.verificaIntegerNulo(alterarDuplicacaoArqRetornoEntradaDTO.getCdTipoLayoutArquivo()));
		request.setCdTipoArquivoRetorno(PgitUtil.verificaIntegerNulo(alterarDuplicacaoArqRetornoEntradaDTO.getCdTipoArquivoRetorno()));
		request.setCdTipoParticipacaoPessoa(PgitUtil.verificaIntegerNulo(alterarDuplicacaoArqRetornoEntradaDTO.getCdTipoParticipacaoPessoa()));
		request.setCdPessoa(PgitUtil.verificaLongNulo(alterarDuplicacaoArqRetornoEntradaDTO.getCdPessoa()));
		request.setCdInstrucaoEnvioRetorno(PgitUtil.verificaIntegerNulo(alterarDuplicacaoArqRetornoEntradaDTO.getCdInstrucaoEnvioRetorno()));
		request.setCdIndicadorDuplicidadeRetorno(PgitUtil.verificaIntegerNulo(alterarDuplicacaoArqRetornoEntradaDTO.getCdIndicadorDuplicidadeRetorno()));
		request.setCdProdutoServicoOper(PgitUtil.verificaIntegerNulo(alterarDuplicacaoArqRetornoEntradaDTO.getCdProdutoServicoOper()));
		request.setCdTipoContratoAnt(PgitUtil.verificaIntegerNulo(alterarDuplicacaoArqRetornoEntradaDTO.getCdTipoContratoAnt()));
		request.setCdPessoaJuridAnt(PgitUtil.verificaLongNulo(alterarDuplicacaoArqRetornoEntradaDTO.getCdPessoaJuridAnt()));
		request.setNrSeqContratoAnt(PgitUtil.verificaLongNulo(alterarDuplicacaoArqRetornoEntradaDTO.getNrSeqContratoAnt()));
		response = getFactoryAdapter().getAlterarDuplicacaoArqRetornoPDCAdapter().invokeProcess(request);
					
		alterarDuplicacaoArqRetornoSaidaDTO.setCodMensagem(response.getCodMensagem());
		alterarDuplicacaoArqRetornoSaidaDTO.setMensagem(response.getMensagem());
				
		return alterarDuplicacaoArqRetornoSaidaDTO;
		
	}
	
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.duplarqretorno.IDuplArqRetornoService#incluirDuplicacaoArqRetorno(br.com.bradesco.web.pgit.service.business.duplarqretorno.bean.IncluirDuplicacaoArqRetornoEntradaDTO)
	 */
	public IncluirDuplicacaoArqRetornoSaidaDTO incluirDuplicacaoArqRetorno(IncluirDuplicacaoArqRetornoEntradaDTO incluirDuplicacaoArqRetornoEntradaDTO) {
		
		IncluirDuplicacaoArqRetornoSaidaDTO incluirDuplicacaoArqRetornoSaidaDTO = new IncluirDuplicacaoArqRetornoSaidaDTO();
		IncluirDuplicacaoArqRetornoRequest request = new IncluirDuplicacaoArqRetornoRequest();
		IncluirDuplicacaoArqRetornoResponse response = new IncluirDuplicacaoArqRetornoResponse();
		
		request.setCdPessoaJuridVinc(PgitUtil.verificaLongNulo(incluirDuplicacaoArqRetornoEntradaDTO.getCdPessoaJuridVinc()));
		request.setCdTipoContratoVinc(PgitUtil.verificaIntegerNulo(incluirDuplicacaoArqRetornoEntradaDTO.getCdTipoContratoVinc()));
		request.setNrSeqContratoVinc(PgitUtil.verificaLongNulo(incluirDuplicacaoArqRetornoEntradaDTO.getNrSeqContratoVinc()));	
		request.setCdPessoaJuridContrato(PgitUtil.verificaLongNulo(incluirDuplicacaoArqRetornoEntradaDTO.getCdPessoaJuridContrato()));
		request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(incluirDuplicacaoArqRetornoEntradaDTO.getCdTipoContratoNegocio()));
		request.setNrSeqContratoNegocio(PgitUtil.verificaLongNulo(incluirDuplicacaoArqRetornoEntradaDTO.getNrSeqContratoNegocio()));
		request.setCdTipoLayoutArquivo(PgitUtil.verificaIntegerNulo(incluirDuplicacaoArqRetornoEntradaDTO.getCdTipoLayoutArquivo()));
		request.setCdTipoArquivoRetorno(PgitUtil.verificaIntegerNulo(incluirDuplicacaoArqRetornoEntradaDTO.getCdTipoArquivoRetorno()));
		request.setCdTipoParticipacaoPessoa(PgitUtil.verificaIntegerNulo(incluirDuplicacaoArqRetornoEntradaDTO.getCdTipoParticipacaoPessoa()));
		request.setCdPessoa(PgitUtil.verificaLongNulo(incluirDuplicacaoArqRetornoEntradaDTO.getCdPessoa()));
		request.setCdInstrucaoEnvioRetorno(PgitUtil.verificaIntegerNulo(incluirDuplicacaoArqRetornoEntradaDTO.getCdInstrucaoEnvioRetorno()));
		request.setCdIndicadorDuplicidadeRetorno(PgitUtil.verificaIntegerNulo(incluirDuplicacaoArqRetornoEntradaDTO.getCdIndicadorDuplicidadeRetorno()));
		request.setCdProdutoServicoOper(PgitUtil.verificaIntegerNulo(incluirDuplicacaoArqRetornoEntradaDTO.getCdProdutoServicoOper()));
		
		response = getFactoryAdapter().getIncluirDuplicacaoArqRetornoPDCAdapter().invokeProcess(request);
		
		incluirDuplicacaoArqRetornoSaidaDTO.setCodMensagem(response.getCodMensagem());
		incluirDuplicacaoArqRetornoSaidaDTO.setMensagem(response.getMensagem());
	
		return incluirDuplicacaoArqRetornoSaidaDTO;
	}


}

