/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.imprimiraditivocontrato.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.util.Enumeration;
import java.util.Vector;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ImprimirAditivoContratoResponse.
 * 
 * @version $Revision$ $Date$
 */
public class ImprimirAditivoContratoResponse implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _codMensagem
     */
    private java.lang.String _codMensagem;

    /**
     * Field _mensagem
     */
    private java.lang.String _mensagem;

    /**
     * Field _nrAditivo
     */
    private long _nrAditivo = 0;

    /**
     * keeps track of state for field: _nrAditivo
     */
    private boolean _has_nrAditivo;

    /**
     * Field _dtAditivo
     */
    private java.lang.String _dtAditivo;

    /**
     * Field _cpfCnpjCliente
     */
    private java.lang.String _cpfCnpjCliente;

    /**
     * Field _nomeCliente
     */
    private java.lang.String _nomeCliente;

    /**
     * Field _cdPessoaEmpresa
     */
    private long _cdPessoaEmpresa = 0;

    /**
     * keeps track of state for field: _cdPessoaEmpresa
     */
    private boolean _has_cdPessoaEmpresa;

    /**
     * Field _cdCpfCnpjEmpresa
     */
    private java.lang.String _cdCpfCnpjEmpresa;

    /**
     * Field _nmEmpresa
     */
    private java.lang.String _nmEmpresa;

    /**
     * Field _cdAgenciaGestorContrato
     */
    private int _cdAgenciaGestorContrato = 0;

    /**
     * keeps track of state for field: _cdAgenciaGestorContrato
     */
    private boolean _has_cdAgenciaGestorContrato;

    /**
     * Field _digitoAgenciaGestor
     */
    private int _digitoAgenciaGestor = 0;

    /**
     * keeps track of state for field: _digitoAgenciaGestor
     */
    private boolean _has_digitoAgenciaGestor;

    /**
     * Field _dsAgenciaGestor
     */
    private java.lang.String _dsAgenciaGestor;

    /**
     * Field _cdContaAgenciaGestor
     */
    private long _cdContaAgenciaGestor = 0;

    /**
     * keeps track of state for field: _cdContaAgenciaGestor
     */
    private boolean _has_cdContaAgenciaGestor;

    /**
     * Field _digitoContaGestor
     */
    private java.lang.String _digitoContaGestor;

    /**
     * Field _cdCepAgencia
     */
    private int _cdCepAgencia = 0;

    /**
     * keeps track of state for field: _cdCepAgencia
     */
    private boolean _has_cdCepAgencia;

    /**
     * Field _cdComplementoCepAgencia
     */
    private int _cdComplementoCepAgencia = 0;

    /**
     * keeps track of state for field: _cdComplementoCepAgencia
     */
    private boolean _has_cdComplementoCepAgencia;

    /**
     * Field _logradouroAgencia
     */
    private java.lang.String _logradouroAgencia;

    /**
     * Field _nrLogradouroAgencia
     */
    private java.lang.String _nrLogradouroAgencia;

    /**
     * Field _complementoAgencia
     */
    private java.lang.String _complementoAgencia;

    /**
     * Field _bairroAgencia
     */
    private java.lang.String _bairroAgencia;

    /**
     * Field _cidadeAgencia
     */
    private java.lang.String _cidadeAgencia;

    /**
     * Field _siglaUfAgencia
     */
    private java.lang.String _siglaUfAgencia;

    /**
     * Field _cdCep
     */
    private int _cdCep = 0;

    /**
     * keeps track of state for field: _cdCep
     */
    private boolean _has_cdCep;

    /**
     * Field _cdCepComplemento
     */
    private int _cdCepComplemento = 0;

    /**
     * keeps track of state for field: _cdCepComplemento
     */
    private boolean _has_cdCepComplemento;

    /**
     * Field _dsLogradouroPessoa
     */
    private java.lang.String _dsLogradouroPessoa;

    /**
     * Field _dsLogradouroNumero
     */
    private java.lang.String _dsLogradouroNumero;

    /**
     * Field _dsComplementoEndereco
     */
    private java.lang.String _dsComplementoEndereco;

    /**
     * Field _dsBairroEndereco
     */
    private java.lang.String _dsBairroEndereco;

    /**
     * Field _dsCidadeEndereco
     */
    private java.lang.String _dsCidadeEndereco;

    /**
     * Field _cdSiglaUf
     */
    private java.lang.String _cdSiglaUf;

    /**
     * Field _nrAnexo
     */
    private int _nrAnexo = 0;

    /**
     * keeps track of state for field: _nrAnexo
     */
    private boolean _has_nrAnexo;

    /**
     * Field _ocorrenciasServicoList
     */
    private java.util.Vector _ocorrenciasServicoList;

    /**
     * Field _impressaoParticipante
     */
    private java.lang.String _impressaoParticipante;

    /**
     * Field _qtdeParticipante
     */
    private int _qtdeParticipante = 0;

    /**
     * keeps track of state for field: _qtdeParticipante
     */
    private boolean _has_qtdeParticipante;

    /**
     * Field _ocorrenciasParticipanteList
     */
    private java.util.Vector _ocorrenciasParticipanteList;

    /**
     * Field _impressaoConta
     */
    private java.lang.String _impressaoConta;

    /**
     * Field _qtdeConta
     */
    private int _qtdeConta = 0;

    /**
     * keeps track of state for field: _qtdeConta
     */
    private boolean _has_qtdeConta;

    /**
     * Field _ocorrenciasContaList
     */
    private java.util.Vector _ocorrenciasContaList;

    /**
     * Field _impressaoSeervico
     */
    private java.lang.String _impressaoSeervico;

    /**
     * Field _qtdeServico
     */
    private int _qtdeServico = 0;

    /**
     * keeps track of state for field: _qtdeServico
     */
    private boolean _has_qtdeServico;

    /**
     * Field _ocorrenciasServico2List
     */
    private java.util.Vector _ocorrenciasServico2List;


      //----------------/
     //- Constructors -/
    //----------------/

    public ImprimirAditivoContratoResponse() 
     {
        super();
        _ocorrenciasServicoList = new Vector();
        _ocorrenciasParticipanteList = new Vector();
        _ocorrenciasContaList = new Vector();
        _ocorrenciasServico2List = new Vector();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimiraditivocontrato.response.ImprimirAditivoContratoResponse()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method addOcorrenciasConta
     * 
     * 
     * 
     * @param vOcorrenciasConta
     */
    public void addOcorrenciasConta(br.com.bradesco.web.pgit.service.data.pdc.imprimiraditivocontrato.response.OcorrenciasConta vOcorrenciasConta)
        throws java.lang.IndexOutOfBoundsException
    {
        _ocorrenciasContaList.addElement(vOcorrenciasConta);
    } //-- void addOcorrenciasConta(br.com.bradesco.web.pgit.service.data.pdc.imprimiraditivocontrato.response.OcorrenciasConta) 

    /**
     * Method addOcorrenciasConta
     * 
     * 
     * 
     * @param index
     * @param vOcorrenciasConta
     */
    public void addOcorrenciasConta(int index, br.com.bradesco.web.pgit.service.data.pdc.imprimiraditivocontrato.response.OcorrenciasConta vOcorrenciasConta)
        throws java.lang.IndexOutOfBoundsException
    {
        _ocorrenciasContaList.insertElementAt(vOcorrenciasConta, index);
    } //-- void addOcorrenciasConta(int, br.com.bradesco.web.pgit.service.data.pdc.imprimiraditivocontrato.response.OcorrenciasConta) 

    /**
     * Method addOcorrenciasParticipante
     * 
     * 
     * 
     * @param vOcorrenciasParticipante
     */
    public void addOcorrenciasParticipante(br.com.bradesco.web.pgit.service.data.pdc.imprimiraditivocontrato.response.OcorrenciasParticipante vOcorrenciasParticipante)
        throws java.lang.IndexOutOfBoundsException
    {
        _ocorrenciasParticipanteList.addElement(vOcorrenciasParticipante);
    } //-- void addOcorrenciasParticipante(br.com.bradesco.web.pgit.service.data.pdc.imprimiraditivocontrato.response.OcorrenciasParticipante) 

    /**
     * Method addOcorrenciasParticipante
     * 
     * 
     * 
     * @param index
     * @param vOcorrenciasParticipante
     */
    public void addOcorrenciasParticipante(int index, br.com.bradesco.web.pgit.service.data.pdc.imprimiraditivocontrato.response.OcorrenciasParticipante vOcorrenciasParticipante)
        throws java.lang.IndexOutOfBoundsException
    {
        _ocorrenciasParticipanteList.insertElementAt(vOcorrenciasParticipante, index);
    } //-- void addOcorrenciasParticipante(int, br.com.bradesco.web.pgit.service.data.pdc.imprimiraditivocontrato.response.OcorrenciasParticipante) 

    /**
     * Method addOcorrenciasServico
     * 
     * 
     * 
     * @param vOcorrenciasServico
     */
    public void addOcorrenciasServico(br.com.bradesco.web.pgit.service.data.pdc.imprimiraditivocontrato.response.OcorrenciasServico vOcorrenciasServico)
        throws java.lang.IndexOutOfBoundsException
    {
        _ocorrenciasServicoList.addElement(vOcorrenciasServico);
    } //-- void addOcorrenciasServico(br.com.bradesco.web.pgit.service.data.pdc.imprimiraditivocontrato.response.OcorrenciasServico) 

    /**
     * Method addOcorrenciasServico
     * 
     * 
     * 
     * @param index
     * @param vOcorrenciasServico
     */
    public void addOcorrenciasServico(int index, br.com.bradesco.web.pgit.service.data.pdc.imprimiraditivocontrato.response.OcorrenciasServico vOcorrenciasServico)
        throws java.lang.IndexOutOfBoundsException
    {
        _ocorrenciasServicoList.insertElementAt(vOcorrenciasServico, index);
    } //-- void addOcorrenciasServico(int, br.com.bradesco.web.pgit.service.data.pdc.imprimiraditivocontrato.response.OcorrenciasServico) 

    /**
     * Method addOcorrenciasServico2
     * 
     * 
     * 
     * @param vOcorrenciasServico2
     */
    public void addOcorrenciasServico2(br.com.bradesco.web.pgit.service.data.pdc.imprimiraditivocontrato.response.OcorrenciasServico2 vOcorrenciasServico2)
        throws java.lang.IndexOutOfBoundsException
    {
        _ocorrenciasServico2List.addElement(vOcorrenciasServico2);
    } //-- void addOcorrenciasServico2(br.com.bradesco.web.pgit.service.data.pdc.imprimiraditivocontrato.response.OcorrenciasServico2) 

    /**
     * Method addOcorrenciasServico2
     * 
     * 
     * 
     * @param index
     * @param vOcorrenciasServico2
     */
    public void addOcorrenciasServico2(int index, br.com.bradesco.web.pgit.service.data.pdc.imprimiraditivocontrato.response.OcorrenciasServico2 vOcorrenciasServico2)
        throws java.lang.IndexOutOfBoundsException
    {
        _ocorrenciasServico2List.insertElementAt(vOcorrenciasServico2, index);
    } //-- void addOcorrenciasServico2(int, br.com.bradesco.web.pgit.service.data.pdc.imprimiraditivocontrato.response.OcorrenciasServico2) 

    /**
     * Method deleteCdAgenciaGestorContrato
     * 
     */
    public void deleteCdAgenciaGestorContrato()
    {
        this._has_cdAgenciaGestorContrato= false;
    } //-- void deleteCdAgenciaGestorContrato() 

    /**
     * Method deleteCdCep
     * 
     */
    public void deleteCdCep()
    {
        this._has_cdCep= false;
    } //-- void deleteCdCep() 

    /**
     * Method deleteCdCepAgencia
     * 
     */
    public void deleteCdCepAgencia()
    {
        this._has_cdCepAgencia= false;
    } //-- void deleteCdCepAgencia() 

    /**
     * Method deleteCdCepComplemento
     * 
     */
    public void deleteCdCepComplemento()
    {
        this._has_cdCepComplemento= false;
    } //-- void deleteCdCepComplemento() 

    /**
     * Method deleteCdComplementoCepAgencia
     * 
     */
    public void deleteCdComplementoCepAgencia()
    {
        this._has_cdComplementoCepAgencia= false;
    } //-- void deleteCdComplementoCepAgencia() 

    /**
     * Method deleteCdContaAgenciaGestor
     * 
     */
    public void deleteCdContaAgenciaGestor()
    {
        this._has_cdContaAgenciaGestor= false;
    } //-- void deleteCdContaAgenciaGestor() 

    /**
     * Method deleteCdPessoaEmpresa
     * 
     */
    public void deleteCdPessoaEmpresa()
    {
        this._has_cdPessoaEmpresa= false;
    } //-- void deleteCdPessoaEmpresa() 

    /**
     * Method deleteDigitoAgenciaGestor
     * 
     */
    public void deleteDigitoAgenciaGestor()
    {
        this._has_digitoAgenciaGestor= false;
    } //-- void deleteDigitoAgenciaGestor() 

    /**
     * Method deleteNrAditivo
     * 
     */
    public void deleteNrAditivo()
    {
        this._has_nrAditivo= false;
    } //-- void deleteNrAditivo() 

    /**
     * Method deleteNrAnexo
     * 
     */
    public void deleteNrAnexo()
    {
        this._has_nrAnexo= false;
    } //-- void deleteNrAnexo() 

    /**
     * Method deleteQtdeConta
     * 
     */
    public void deleteQtdeConta()
    {
        this._has_qtdeConta= false;
    } //-- void deleteQtdeConta() 

    /**
     * Method deleteQtdeParticipante
     * 
     */
    public void deleteQtdeParticipante()
    {
        this._has_qtdeParticipante= false;
    } //-- void deleteQtdeParticipante() 

    /**
     * Method deleteQtdeServico
     * 
     */
    public void deleteQtdeServico()
    {
        this._has_qtdeServico= false;
    } //-- void deleteQtdeServico() 

    /**
     * Method enumerateOcorrenciasConta
     * 
     * 
     * 
     * @return Enumeration
     */
    public java.util.Enumeration enumerateOcorrenciasConta()
    {
        return _ocorrenciasContaList.elements();
    } //-- java.util.Enumeration enumerateOcorrenciasConta() 

    /**
     * Method enumerateOcorrenciasParticipante
     * 
     * 
     * 
     * @return Enumeration
     */
    public java.util.Enumeration enumerateOcorrenciasParticipante()
    {
        return _ocorrenciasParticipanteList.elements();
    } //-- java.util.Enumeration enumerateOcorrenciasParticipante() 

    /**
     * Method enumerateOcorrenciasServico
     * 
     * 
     * 
     * @return Enumeration
     */
    public java.util.Enumeration enumerateOcorrenciasServico()
    {
        return _ocorrenciasServicoList.elements();
    } //-- java.util.Enumeration enumerateOcorrenciasServico() 

    /**
     * Method enumerateOcorrenciasServico2
     * 
     * 
     * 
     * @return Enumeration
     */
    public java.util.Enumeration enumerateOcorrenciasServico2()
    {
        return _ocorrenciasServico2List.elements();
    } //-- java.util.Enumeration enumerateOcorrenciasServico2() 

    /**
     * Returns the value of field 'bairroAgencia'.
     * 
     * @return String
     * @return the value of field 'bairroAgencia'.
     */
    public java.lang.String getBairroAgencia()
    {
        return this._bairroAgencia;
    } //-- java.lang.String getBairroAgencia() 

    /**
     * Returns the value of field 'cdAgenciaGestorContrato'.
     * 
     * @return int
     * @return the value of field 'cdAgenciaGestorContrato'.
     */
    public int getCdAgenciaGestorContrato()
    {
        return this._cdAgenciaGestorContrato;
    } //-- int getCdAgenciaGestorContrato() 

    /**
     * Returns the value of field 'cdCep'.
     * 
     * @return int
     * @return the value of field 'cdCep'.
     */
    public int getCdCep()
    {
        return this._cdCep;
    } //-- int getCdCep() 

    /**
     * Returns the value of field 'cdCepAgencia'.
     * 
     * @return int
     * @return the value of field 'cdCepAgencia'.
     */
    public int getCdCepAgencia()
    {
        return this._cdCepAgencia;
    } //-- int getCdCepAgencia() 

    /**
     * Returns the value of field 'cdCepComplemento'.
     * 
     * @return int
     * @return the value of field 'cdCepComplemento'.
     */
    public int getCdCepComplemento()
    {
        return this._cdCepComplemento;
    } //-- int getCdCepComplemento() 

    /**
     * Returns the value of field 'cdComplementoCepAgencia'.
     * 
     * @return int
     * @return the value of field 'cdComplementoCepAgencia'.
     */
    public int getCdComplementoCepAgencia()
    {
        return this._cdComplementoCepAgencia;
    } //-- int getCdComplementoCepAgencia() 

    /**
     * Returns the value of field 'cdContaAgenciaGestor'.
     * 
     * @return long
     * @return the value of field 'cdContaAgenciaGestor'.
     */
    public long getCdContaAgenciaGestor()
    {
        return this._cdContaAgenciaGestor;
    } //-- long getCdContaAgenciaGestor() 

    /**
     * Returns the value of field 'cdCpfCnpjEmpresa'.
     * 
     * @return String
     * @return the value of field 'cdCpfCnpjEmpresa'.
     */
    public java.lang.String getCdCpfCnpjEmpresa()
    {
        return this._cdCpfCnpjEmpresa;
    } //-- java.lang.String getCdCpfCnpjEmpresa() 

    /**
     * Returns the value of field 'cdPessoaEmpresa'.
     * 
     * @return long
     * @return the value of field 'cdPessoaEmpresa'.
     */
    public long getCdPessoaEmpresa()
    {
        return this._cdPessoaEmpresa;
    } //-- long getCdPessoaEmpresa() 

    /**
     * Returns the value of field 'cdSiglaUf'.
     * 
     * @return String
     * @return the value of field 'cdSiglaUf'.
     */
    public java.lang.String getCdSiglaUf()
    {
        return this._cdSiglaUf;
    } //-- java.lang.String getCdSiglaUf() 

    /**
     * Returns the value of field 'cidadeAgencia'.
     * 
     * @return String
     * @return the value of field 'cidadeAgencia'.
     */
    public java.lang.String getCidadeAgencia()
    {
        return this._cidadeAgencia;
    } //-- java.lang.String getCidadeAgencia() 

    /**
     * Returns the value of field 'codMensagem'.
     * 
     * @return String
     * @return the value of field 'codMensagem'.
     */
    public java.lang.String getCodMensagem()
    {
        return this._codMensagem;
    } //-- java.lang.String getCodMensagem() 

    /**
     * Returns the value of field 'complementoAgencia'.
     * 
     * @return String
     * @return the value of field 'complementoAgencia'.
     */
    public java.lang.String getComplementoAgencia()
    {
        return this._complementoAgencia;
    } //-- java.lang.String getComplementoAgencia() 

    /**
     * Returns the value of field 'cpfCnpjCliente'.
     * 
     * @return String
     * @return the value of field 'cpfCnpjCliente'.
     */
    public java.lang.String getCpfCnpjCliente()
    {
        return this._cpfCnpjCliente;
    } //-- java.lang.String getCpfCnpjCliente() 

    /**
     * Returns the value of field 'digitoAgenciaGestor'.
     * 
     * @return int
     * @return the value of field 'digitoAgenciaGestor'.
     */
    public int getDigitoAgenciaGestor()
    {
        return this._digitoAgenciaGestor;
    } //-- int getDigitoAgenciaGestor() 

    /**
     * Returns the value of field 'digitoContaGestor'.
     * 
     * @return String
     * @return the value of field 'digitoContaGestor'.
     */
    public java.lang.String getDigitoContaGestor()
    {
        return this._digitoContaGestor;
    } //-- java.lang.String getDigitoContaGestor() 

    /**
     * Returns the value of field 'dsAgenciaGestor'.
     * 
     * @return String
     * @return the value of field 'dsAgenciaGestor'.
     */
    public java.lang.String getDsAgenciaGestor()
    {
        return this._dsAgenciaGestor;
    } //-- java.lang.String getDsAgenciaGestor() 

    /**
     * Returns the value of field 'dsBairroEndereco'.
     * 
     * @return String
     * @return the value of field 'dsBairroEndereco'.
     */
    public java.lang.String getDsBairroEndereco()
    {
        return this._dsBairroEndereco;
    } //-- java.lang.String getDsBairroEndereco() 

    /**
     * Returns the value of field 'dsCidadeEndereco'.
     * 
     * @return String
     * @return the value of field 'dsCidadeEndereco'.
     */
    public java.lang.String getDsCidadeEndereco()
    {
        return this._dsCidadeEndereco;
    } //-- java.lang.String getDsCidadeEndereco() 

    /**
     * Returns the value of field 'dsComplementoEndereco'.
     * 
     * @return String
     * @return the value of field 'dsComplementoEndereco'.
     */
    public java.lang.String getDsComplementoEndereco()
    {
        return this._dsComplementoEndereco;
    } //-- java.lang.String getDsComplementoEndereco() 

    /**
     * Returns the value of field 'dsLogradouroNumero'.
     * 
     * @return String
     * @return the value of field 'dsLogradouroNumero'.
     */
    public java.lang.String getDsLogradouroNumero()
    {
        return this._dsLogradouroNumero;
    } //-- java.lang.String getDsLogradouroNumero() 

    /**
     * Returns the value of field 'dsLogradouroPessoa'.
     * 
     * @return String
     * @return the value of field 'dsLogradouroPessoa'.
     */
    public java.lang.String getDsLogradouroPessoa()
    {
        return this._dsLogradouroPessoa;
    } //-- java.lang.String getDsLogradouroPessoa() 

    /**
     * Returns the value of field 'dtAditivo'.
     * 
     * @return String
     * @return the value of field 'dtAditivo'.
     */
    public java.lang.String getDtAditivo()
    {
        return this._dtAditivo;
    } //-- java.lang.String getDtAditivo() 

    /**
     * Returns the value of field 'impressaoConta'.
     * 
     * @return String
     * @return the value of field 'impressaoConta'.
     */
    public java.lang.String getImpressaoConta()
    {
        return this._impressaoConta;
    } //-- java.lang.String getImpressaoConta() 

    /**
     * Returns the value of field 'impressaoParticipante'.
     * 
     * @return String
     * @return the value of field 'impressaoParticipante'.
     */
    public java.lang.String getImpressaoParticipante()
    {
        return this._impressaoParticipante;
    } //-- java.lang.String getImpressaoParticipante() 

    /**
     * Returns the value of field 'impressaoSeervico'.
     * 
     * @return String
     * @return the value of field 'impressaoSeervico'.
     */
    public java.lang.String getImpressaoSeervico()
    {
        return this._impressaoSeervico;
    } //-- java.lang.String getImpressaoSeervico() 

    /**
     * Returns the value of field 'logradouroAgencia'.
     * 
     * @return String
     * @return the value of field 'logradouroAgencia'.
     */
    public java.lang.String getLogradouroAgencia()
    {
        return this._logradouroAgencia;
    } //-- java.lang.String getLogradouroAgencia() 

    /**
     * Returns the value of field 'mensagem'.
     * 
     * @return String
     * @return the value of field 'mensagem'.
     */
    public java.lang.String getMensagem()
    {
        return this._mensagem;
    } //-- java.lang.String getMensagem() 

    /**
     * Returns the value of field 'nmEmpresa'.
     * 
     * @return String
     * @return the value of field 'nmEmpresa'.
     */
    public java.lang.String getNmEmpresa()
    {
        return this._nmEmpresa;
    } //-- java.lang.String getNmEmpresa() 

    /**
     * Returns the value of field 'nomeCliente'.
     * 
     * @return String
     * @return the value of field 'nomeCliente'.
     */
    public java.lang.String getNomeCliente()
    {
        return this._nomeCliente;
    } //-- java.lang.String getNomeCliente() 

    /**
     * Returns the value of field 'nrAditivo'.
     * 
     * @return long
     * @return the value of field 'nrAditivo'.
     */
    public long getNrAditivo()
    {
        return this._nrAditivo;
    } //-- long getNrAditivo() 

    /**
     * Returns the value of field 'nrAnexo'.
     * 
     * @return int
     * @return the value of field 'nrAnexo'.
     */
    public int getNrAnexo()
    {
        return this._nrAnexo;
    } //-- int getNrAnexo() 

    /**
     * Returns the value of field 'nrLogradouroAgencia'.
     * 
     * @return String
     * @return the value of field 'nrLogradouroAgencia'.
     */
    public java.lang.String getNrLogradouroAgencia()
    {
        return this._nrLogradouroAgencia;
    } //-- java.lang.String getNrLogradouroAgencia() 

    /**
     * Method getOcorrenciasConta
     * 
     * 
     * 
     * @param index
     * @return OcorrenciasConta
     */
    public br.com.bradesco.web.pgit.service.data.pdc.imprimiraditivocontrato.response.OcorrenciasConta getOcorrenciasConta(int index)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index >= _ocorrenciasContaList.size())) {
            throw new IndexOutOfBoundsException("getOcorrenciasConta: Index value '"+index+"' not in range [0.."+(_ocorrenciasContaList.size() - 1) + "]");
        }
        
        return (br.com.bradesco.web.pgit.service.data.pdc.imprimiraditivocontrato.response.OcorrenciasConta) _ocorrenciasContaList.elementAt(index);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimiraditivocontrato.response.OcorrenciasConta getOcorrenciasConta(int) 

    /**
     * Method getOcorrenciasConta
     * 
     * 
     * 
     * @return OcorrenciasConta
     */
    public br.com.bradesco.web.pgit.service.data.pdc.imprimiraditivocontrato.response.OcorrenciasConta[] getOcorrenciasConta()
    {
        int size = _ocorrenciasContaList.size();
        br.com.bradesco.web.pgit.service.data.pdc.imprimiraditivocontrato.response.OcorrenciasConta[] mArray = new br.com.bradesco.web.pgit.service.data.pdc.imprimiraditivocontrato.response.OcorrenciasConta[size];
        for (int index = 0; index < size; index++) {
            mArray[index] = (br.com.bradesco.web.pgit.service.data.pdc.imprimiraditivocontrato.response.OcorrenciasConta) _ocorrenciasContaList.elementAt(index);
        }
        return mArray;
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimiraditivocontrato.response.OcorrenciasConta[] getOcorrenciasConta() 

    /**
     * Method getOcorrenciasContaCount
     * 
     * 
     * 
     * @return int
     */
    public int getOcorrenciasContaCount()
    {
        return _ocorrenciasContaList.size();
    } //-- int getOcorrenciasContaCount() 

    /**
     * Method getOcorrenciasParticipante
     * 
     * 
     * 
     * @param index
     * @return OcorrenciasParticipante
     */
    public br.com.bradesco.web.pgit.service.data.pdc.imprimiraditivocontrato.response.OcorrenciasParticipante getOcorrenciasParticipante(int index)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index >= _ocorrenciasParticipanteList.size())) {
            throw new IndexOutOfBoundsException("getOcorrenciasParticipante: Index value '"+index+"' not in range [0.."+(_ocorrenciasParticipanteList.size() - 1) + "]");
        }
        
        return (br.com.bradesco.web.pgit.service.data.pdc.imprimiraditivocontrato.response.OcorrenciasParticipante) _ocorrenciasParticipanteList.elementAt(index);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimiraditivocontrato.response.OcorrenciasParticipante getOcorrenciasParticipante(int) 

    /**
     * Method getOcorrenciasParticipante
     * 
     * 
     * 
     * @return OcorrenciasParticipante
     */
    public br.com.bradesco.web.pgit.service.data.pdc.imprimiraditivocontrato.response.OcorrenciasParticipante[] getOcorrenciasParticipante()
    {
        int size = _ocorrenciasParticipanteList.size();
        br.com.bradesco.web.pgit.service.data.pdc.imprimiraditivocontrato.response.OcorrenciasParticipante[] mArray = new br.com.bradesco.web.pgit.service.data.pdc.imprimiraditivocontrato.response.OcorrenciasParticipante[size];
        for (int index = 0; index < size; index++) {
            mArray[index] = (br.com.bradesco.web.pgit.service.data.pdc.imprimiraditivocontrato.response.OcorrenciasParticipante) _ocorrenciasParticipanteList.elementAt(index);
        }
        return mArray;
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimiraditivocontrato.response.OcorrenciasParticipante[] getOcorrenciasParticipante() 

    /**
     * Method getOcorrenciasParticipanteCount
     * 
     * 
     * 
     * @return int
     */
    public int getOcorrenciasParticipanteCount()
    {
        return _ocorrenciasParticipanteList.size();
    } //-- int getOcorrenciasParticipanteCount() 

    /**
     * Method getOcorrenciasServico
     * 
     * 
     * 
     * @param index
     * @return OcorrenciasServico
     */
    public br.com.bradesco.web.pgit.service.data.pdc.imprimiraditivocontrato.response.OcorrenciasServico getOcorrenciasServico(int index)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index >= _ocorrenciasServicoList.size())) {
            throw new IndexOutOfBoundsException("getOcorrenciasServico: Index value '"+index+"' not in range [0.."+(_ocorrenciasServicoList.size() - 1) + "]");
        }
        
        return (br.com.bradesco.web.pgit.service.data.pdc.imprimiraditivocontrato.response.OcorrenciasServico) _ocorrenciasServicoList.elementAt(index);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimiraditivocontrato.response.OcorrenciasServico getOcorrenciasServico(int) 

    /**
     * Method getOcorrenciasServico
     * 
     * 
     * 
     * @return OcorrenciasServico
     */
    public br.com.bradesco.web.pgit.service.data.pdc.imprimiraditivocontrato.response.OcorrenciasServico[] getOcorrenciasServico()
    {
        int size = _ocorrenciasServicoList.size();
        br.com.bradesco.web.pgit.service.data.pdc.imprimiraditivocontrato.response.OcorrenciasServico[] mArray = new br.com.bradesco.web.pgit.service.data.pdc.imprimiraditivocontrato.response.OcorrenciasServico[size];
        for (int index = 0; index < size; index++) {
            mArray[index] = (br.com.bradesco.web.pgit.service.data.pdc.imprimiraditivocontrato.response.OcorrenciasServico) _ocorrenciasServicoList.elementAt(index);
        }
        return mArray;
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimiraditivocontrato.response.OcorrenciasServico[] getOcorrenciasServico() 

    /**
     * Method getOcorrenciasServico2
     * 
     * 
     * 
     * @param index
     * @return OcorrenciasServico2
     */
    public br.com.bradesco.web.pgit.service.data.pdc.imprimiraditivocontrato.response.OcorrenciasServico2 getOcorrenciasServico2(int index)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index >= _ocorrenciasServico2List.size())) {
            throw new IndexOutOfBoundsException("getOcorrenciasServico2: Index value '"+index+"' not in range [0.."+(_ocorrenciasServico2List.size() - 1) + "]");
        }
        
        return (br.com.bradesco.web.pgit.service.data.pdc.imprimiraditivocontrato.response.OcorrenciasServico2) _ocorrenciasServico2List.elementAt(index);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimiraditivocontrato.response.OcorrenciasServico2 getOcorrenciasServico2(int) 

    /**
     * Method getOcorrenciasServico2
     * 
     * 
     * 
     * @return OcorrenciasServico2
     */
    public br.com.bradesco.web.pgit.service.data.pdc.imprimiraditivocontrato.response.OcorrenciasServico2[] getOcorrenciasServico2()
    {
        int size = _ocorrenciasServico2List.size();
        br.com.bradesco.web.pgit.service.data.pdc.imprimiraditivocontrato.response.OcorrenciasServico2[] mArray = new br.com.bradesco.web.pgit.service.data.pdc.imprimiraditivocontrato.response.OcorrenciasServico2[size];
        for (int index = 0; index < size; index++) {
            mArray[index] = (br.com.bradesco.web.pgit.service.data.pdc.imprimiraditivocontrato.response.OcorrenciasServico2) _ocorrenciasServico2List.elementAt(index);
        }
        return mArray;
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimiraditivocontrato.response.OcorrenciasServico2[] getOcorrenciasServico2() 

    /**
     * Method getOcorrenciasServico2Count
     * 
     * 
     * 
     * @return int
     */
    public int getOcorrenciasServico2Count()
    {
        return _ocorrenciasServico2List.size();
    } //-- int getOcorrenciasServico2Count() 

    /**
     * Method getOcorrenciasServicoCount
     * 
     * 
     * 
     * @return int
     */
    public int getOcorrenciasServicoCount()
    {
        return _ocorrenciasServicoList.size();
    } //-- int getOcorrenciasServicoCount() 

    /**
     * Returns the value of field 'qtdeConta'.
     * 
     * @return int
     * @return the value of field 'qtdeConta'.
     */
    public int getQtdeConta()
    {
        return this._qtdeConta;
    } //-- int getQtdeConta() 

    /**
     * Returns the value of field 'qtdeParticipante'.
     * 
     * @return int
     * @return the value of field 'qtdeParticipante'.
     */
    public int getQtdeParticipante()
    {
        return this._qtdeParticipante;
    } //-- int getQtdeParticipante() 

    /**
     * Returns the value of field 'qtdeServico'.
     * 
     * @return int
     * @return the value of field 'qtdeServico'.
     */
    public int getQtdeServico()
    {
        return this._qtdeServico;
    } //-- int getQtdeServico() 

    /**
     * Returns the value of field 'siglaUfAgencia'.
     * 
     * @return String
     * @return the value of field 'siglaUfAgencia'.
     */
    public java.lang.String getSiglaUfAgencia()
    {
        return this._siglaUfAgencia;
    } //-- java.lang.String getSiglaUfAgencia() 

    /**
     * Method hasCdAgenciaGestorContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgenciaGestorContrato()
    {
        return this._has_cdAgenciaGestorContrato;
    } //-- boolean hasCdAgenciaGestorContrato() 

    /**
     * Method hasCdCep
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCep()
    {
        return this._has_cdCep;
    } //-- boolean hasCdCep() 

    /**
     * Method hasCdCepAgencia
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCepAgencia()
    {
        return this._has_cdCepAgencia;
    } //-- boolean hasCdCepAgencia() 

    /**
     * Method hasCdCepComplemento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCepComplemento()
    {
        return this._has_cdCepComplemento;
    } //-- boolean hasCdCepComplemento() 

    /**
     * Method hasCdComplementoCepAgencia
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdComplementoCepAgencia()
    {
        return this._has_cdComplementoCepAgencia;
    } //-- boolean hasCdComplementoCepAgencia() 

    /**
     * Method hasCdContaAgenciaGestor
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdContaAgenciaGestor()
    {
        return this._has_cdContaAgenciaGestor;
    } //-- boolean hasCdContaAgenciaGestor() 

    /**
     * Method hasCdPessoaEmpresa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaEmpresa()
    {
        return this._has_cdPessoaEmpresa;
    } //-- boolean hasCdPessoaEmpresa() 

    /**
     * Method hasDigitoAgenciaGestor
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasDigitoAgenciaGestor()
    {
        return this._has_digitoAgenciaGestor;
    } //-- boolean hasDigitoAgenciaGestor() 

    /**
     * Method hasNrAditivo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrAditivo()
    {
        return this._has_nrAditivo;
    } //-- boolean hasNrAditivo() 

    /**
     * Method hasNrAnexo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrAnexo()
    {
        return this._has_nrAnexo;
    } //-- boolean hasNrAnexo() 

    /**
     * Method hasQtdeConta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtdeConta()
    {
        return this._has_qtdeConta;
    } //-- boolean hasQtdeConta() 

    /**
     * Method hasQtdeParticipante
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtdeParticipante()
    {
        return this._has_qtdeParticipante;
    } //-- boolean hasQtdeParticipante() 

    /**
     * Method hasQtdeServico
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtdeServico()
    {
        return this._has_qtdeServico;
    } //-- boolean hasQtdeServico() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Method removeAllOcorrenciasConta
     * 
     */
    public void removeAllOcorrenciasConta()
    {
        _ocorrenciasContaList.removeAllElements();
    } //-- void removeAllOcorrenciasConta() 

    /**
     * Method removeAllOcorrenciasParticipante
     * 
     */
    public void removeAllOcorrenciasParticipante()
    {
        _ocorrenciasParticipanteList.removeAllElements();
    } //-- void removeAllOcorrenciasParticipante() 

    /**
     * Method removeAllOcorrenciasServico
     * 
     */
    public void removeAllOcorrenciasServico()
    {
        _ocorrenciasServicoList.removeAllElements();
    } //-- void removeAllOcorrenciasServico() 

    /**
     * Method removeAllOcorrenciasServico2
     * 
     */
    public void removeAllOcorrenciasServico2()
    {
        _ocorrenciasServico2List.removeAllElements();
    } //-- void removeAllOcorrenciasServico2() 

    /**
     * Method removeOcorrenciasConta
     * 
     * 
     * 
     * @param index
     * @return OcorrenciasConta
     */
    public br.com.bradesco.web.pgit.service.data.pdc.imprimiraditivocontrato.response.OcorrenciasConta removeOcorrenciasConta(int index)
    {
        java.lang.Object obj = _ocorrenciasContaList.elementAt(index);
        _ocorrenciasContaList.removeElementAt(index);
        return (br.com.bradesco.web.pgit.service.data.pdc.imprimiraditivocontrato.response.OcorrenciasConta) obj;
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimiraditivocontrato.response.OcorrenciasConta removeOcorrenciasConta(int) 

    /**
     * Method removeOcorrenciasParticipante
     * 
     * 
     * 
     * @param index
     * @return OcorrenciasParticipante
     */
    public br.com.bradesco.web.pgit.service.data.pdc.imprimiraditivocontrato.response.OcorrenciasParticipante removeOcorrenciasParticipante(int index)
    {
        java.lang.Object obj = _ocorrenciasParticipanteList.elementAt(index);
        _ocorrenciasParticipanteList.removeElementAt(index);
        return (br.com.bradesco.web.pgit.service.data.pdc.imprimiraditivocontrato.response.OcorrenciasParticipante) obj;
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimiraditivocontrato.response.OcorrenciasParticipante removeOcorrenciasParticipante(int) 

    /**
     * Method removeOcorrenciasServico
     * 
     * 
     * 
     * @param index
     * @return OcorrenciasServico
     */
    public br.com.bradesco.web.pgit.service.data.pdc.imprimiraditivocontrato.response.OcorrenciasServico removeOcorrenciasServico(int index)
    {
        java.lang.Object obj = _ocorrenciasServicoList.elementAt(index);
        _ocorrenciasServicoList.removeElementAt(index);
        return (br.com.bradesco.web.pgit.service.data.pdc.imprimiraditivocontrato.response.OcorrenciasServico) obj;
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimiraditivocontrato.response.OcorrenciasServico removeOcorrenciasServico(int) 

    /**
     * Method removeOcorrenciasServico2
     * 
     * 
     * 
     * @param index
     * @return OcorrenciasServico2
     */
    public br.com.bradesco.web.pgit.service.data.pdc.imprimiraditivocontrato.response.OcorrenciasServico2 removeOcorrenciasServico2(int index)
    {
        java.lang.Object obj = _ocorrenciasServico2List.elementAt(index);
        _ocorrenciasServico2List.removeElementAt(index);
        return (br.com.bradesco.web.pgit.service.data.pdc.imprimiraditivocontrato.response.OcorrenciasServico2) obj;
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimiraditivocontrato.response.OcorrenciasServico2 removeOcorrenciasServico2(int) 

    /**
     * Sets the value of field 'bairroAgencia'.
     * 
     * @param bairroAgencia the value of field 'bairroAgencia'.
     */
    public void setBairroAgencia(java.lang.String bairroAgencia)
    {
        this._bairroAgencia = bairroAgencia;
    } //-- void setBairroAgencia(java.lang.String) 

    /**
     * Sets the value of field 'cdAgenciaGestorContrato'.
     * 
     * @param cdAgenciaGestorContrato the value of field
     * 'cdAgenciaGestorContrato'.
     */
    public void setCdAgenciaGestorContrato(int cdAgenciaGestorContrato)
    {
        this._cdAgenciaGestorContrato = cdAgenciaGestorContrato;
        this._has_cdAgenciaGestorContrato = true;
    } //-- void setCdAgenciaGestorContrato(int) 

    /**
     * Sets the value of field 'cdCep'.
     * 
     * @param cdCep the value of field 'cdCep'.
     */
    public void setCdCep(int cdCep)
    {
        this._cdCep = cdCep;
        this._has_cdCep = true;
    } //-- void setCdCep(int) 

    /**
     * Sets the value of field 'cdCepAgencia'.
     * 
     * @param cdCepAgencia the value of field 'cdCepAgencia'.
     */
    public void setCdCepAgencia(int cdCepAgencia)
    {
        this._cdCepAgencia = cdCepAgencia;
        this._has_cdCepAgencia = true;
    } //-- void setCdCepAgencia(int) 

    /**
     * Sets the value of field 'cdCepComplemento'.
     * 
     * @param cdCepComplemento the value of field 'cdCepComplemento'
     */
    public void setCdCepComplemento(int cdCepComplemento)
    {
        this._cdCepComplemento = cdCepComplemento;
        this._has_cdCepComplemento = true;
    } //-- void setCdCepComplemento(int) 

    /**
     * Sets the value of field 'cdComplementoCepAgencia'.
     * 
     * @param cdComplementoCepAgencia the value of field
     * 'cdComplementoCepAgencia'.
     */
    public void setCdComplementoCepAgencia(int cdComplementoCepAgencia)
    {
        this._cdComplementoCepAgencia = cdComplementoCepAgencia;
        this._has_cdComplementoCepAgencia = true;
    } //-- void setCdComplementoCepAgencia(int) 

    /**
     * Sets the value of field 'cdContaAgenciaGestor'.
     * 
     * @param cdContaAgenciaGestor the value of field
     * 'cdContaAgenciaGestor'.
     */
    public void setCdContaAgenciaGestor(long cdContaAgenciaGestor)
    {
        this._cdContaAgenciaGestor = cdContaAgenciaGestor;
        this._has_cdContaAgenciaGestor = true;
    } //-- void setCdContaAgenciaGestor(long) 

    /**
     * Sets the value of field 'cdCpfCnpjEmpresa'.
     * 
     * @param cdCpfCnpjEmpresa the value of field 'cdCpfCnpjEmpresa'
     */
    public void setCdCpfCnpjEmpresa(java.lang.String cdCpfCnpjEmpresa)
    {
        this._cdCpfCnpjEmpresa = cdCpfCnpjEmpresa;
    } //-- void setCdCpfCnpjEmpresa(java.lang.String) 

    /**
     * Sets the value of field 'cdPessoaEmpresa'.
     * 
     * @param cdPessoaEmpresa the value of field 'cdPessoaEmpresa'.
     */
    public void setCdPessoaEmpresa(long cdPessoaEmpresa)
    {
        this._cdPessoaEmpresa = cdPessoaEmpresa;
        this._has_cdPessoaEmpresa = true;
    } //-- void setCdPessoaEmpresa(long) 

    /**
     * Sets the value of field 'cdSiglaUf'.
     * 
     * @param cdSiglaUf the value of field 'cdSiglaUf'.
     */
    public void setCdSiglaUf(java.lang.String cdSiglaUf)
    {
        this._cdSiglaUf = cdSiglaUf;
    } //-- void setCdSiglaUf(java.lang.String) 

    /**
     * Sets the value of field 'cidadeAgencia'.
     * 
     * @param cidadeAgencia the value of field 'cidadeAgencia'.
     */
    public void setCidadeAgencia(java.lang.String cidadeAgencia)
    {
        this._cidadeAgencia = cidadeAgencia;
    } //-- void setCidadeAgencia(java.lang.String) 

    /**
     * Sets the value of field 'codMensagem'.
     * 
     * @param codMensagem the value of field 'codMensagem'.
     */
    public void setCodMensagem(java.lang.String codMensagem)
    {
        this._codMensagem = codMensagem;
    } //-- void setCodMensagem(java.lang.String) 

    /**
     * Sets the value of field 'complementoAgencia'.
     * 
     * @param complementoAgencia the value of field
     * 'complementoAgencia'.
     */
    public void setComplementoAgencia(java.lang.String complementoAgencia)
    {
        this._complementoAgencia = complementoAgencia;
    } //-- void setComplementoAgencia(java.lang.String) 

    /**
     * Sets the value of field 'cpfCnpjCliente'.
     * 
     * @param cpfCnpjCliente the value of field 'cpfCnpjCliente'.
     */
    public void setCpfCnpjCliente(java.lang.String cpfCnpjCliente)
    {
        this._cpfCnpjCliente = cpfCnpjCliente;
    } //-- void setCpfCnpjCliente(java.lang.String) 

    /**
     * Sets the value of field 'digitoAgenciaGestor'.
     * 
     * @param digitoAgenciaGestor the value of field
     * 'digitoAgenciaGestor'.
     */
    public void setDigitoAgenciaGestor(int digitoAgenciaGestor)
    {
        this._digitoAgenciaGestor = digitoAgenciaGestor;
        this._has_digitoAgenciaGestor = true;
    } //-- void setDigitoAgenciaGestor(int) 

    /**
     * Sets the value of field 'digitoContaGestor'.
     * 
     * @param digitoContaGestor the value of field
     * 'digitoContaGestor'.
     */
    public void setDigitoContaGestor(java.lang.String digitoContaGestor)
    {
        this._digitoContaGestor = digitoContaGestor;
    } //-- void setDigitoContaGestor(java.lang.String) 

    /**
     * Sets the value of field 'dsAgenciaGestor'.
     * 
     * @param dsAgenciaGestor the value of field 'dsAgenciaGestor'.
     */
    public void setDsAgenciaGestor(java.lang.String dsAgenciaGestor)
    {
        this._dsAgenciaGestor = dsAgenciaGestor;
    } //-- void setDsAgenciaGestor(java.lang.String) 

    /**
     * Sets the value of field 'dsBairroEndereco'.
     * 
     * @param dsBairroEndereco the value of field 'dsBairroEndereco'
     */
    public void setDsBairroEndereco(java.lang.String dsBairroEndereco)
    {
        this._dsBairroEndereco = dsBairroEndereco;
    } //-- void setDsBairroEndereco(java.lang.String) 

    /**
     * Sets the value of field 'dsCidadeEndereco'.
     * 
     * @param dsCidadeEndereco the value of field 'dsCidadeEndereco'
     */
    public void setDsCidadeEndereco(java.lang.String dsCidadeEndereco)
    {
        this._dsCidadeEndereco = dsCidadeEndereco;
    } //-- void setDsCidadeEndereco(java.lang.String) 

    /**
     * Sets the value of field 'dsComplementoEndereco'.
     * 
     * @param dsComplementoEndereco the value of field
     * 'dsComplementoEndereco'.
     */
    public void setDsComplementoEndereco(java.lang.String dsComplementoEndereco)
    {
        this._dsComplementoEndereco = dsComplementoEndereco;
    } //-- void setDsComplementoEndereco(java.lang.String) 

    /**
     * Sets the value of field 'dsLogradouroNumero'.
     * 
     * @param dsLogradouroNumero the value of field
     * 'dsLogradouroNumero'.
     */
    public void setDsLogradouroNumero(java.lang.String dsLogradouroNumero)
    {
        this._dsLogradouroNumero = dsLogradouroNumero;
    } //-- void setDsLogradouroNumero(java.lang.String) 

    /**
     * Sets the value of field 'dsLogradouroPessoa'.
     * 
     * @param dsLogradouroPessoa the value of field
     * 'dsLogradouroPessoa'.
     */
    public void setDsLogradouroPessoa(java.lang.String dsLogradouroPessoa)
    {
        this._dsLogradouroPessoa = dsLogradouroPessoa;
    } //-- void setDsLogradouroPessoa(java.lang.String) 

    /**
     * Sets the value of field 'dtAditivo'.
     * 
     * @param dtAditivo the value of field 'dtAditivo'.
     */
    public void setDtAditivo(java.lang.String dtAditivo)
    {
        this._dtAditivo = dtAditivo;
    } //-- void setDtAditivo(java.lang.String) 

    /**
     * Sets the value of field 'impressaoConta'.
     * 
     * @param impressaoConta the value of field 'impressaoConta'.
     */
    public void setImpressaoConta(java.lang.String impressaoConta)
    {
        this._impressaoConta = impressaoConta;
    } //-- void setImpressaoConta(java.lang.String) 

    /**
     * Sets the value of field 'impressaoParticipante'.
     * 
     * @param impressaoParticipante the value of field
     * 'impressaoParticipante'.
     */
    public void setImpressaoParticipante(java.lang.String impressaoParticipante)
    {
        this._impressaoParticipante = impressaoParticipante;
    } //-- void setImpressaoParticipante(java.lang.String) 

    /**
     * Sets the value of field 'impressaoSeervico'.
     * 
     * @param impressaoSeervico the value of field
     * 'impressaoSeervico'.
     */
    public void setImpressaoSeervico(java.lang.String impressaoSeervico)
    {
        this._impressaoSeervico = impressaoSeervico;
    } //-- void setImpressaoSeervico(java.lang.String) 

    /**
     * Sets the value of field 'logradouroAgencia'.
     * 
     * @param logradouroAgencia the value of field
     * 'logradouroAgencia'.
     */
    public void setLogradouroAgencia(java.lang.String logradouroAgencia)
    {
        this._logradouroAgencia = logradouroAgencia;
    } //-- void setLogradouroAgencia(java.lang.String) 

    /**
     * Sets the value of field 'mensagem'.
     * 
     * @param mensagem the value of field 'mensagem'.
     */
    public void setMensagem(java.lang.String mensagem)
    {
        this._mensagem = mensagem;
    } //-- void setMensagem(java.lang.String) 

    /**
     * Sets the value of field 'nmEmpresa'.
     * 
     * @param nmEmpresa the value of field 'nmEmpresa'.
     */
    public void setNmEmpresa(java.lang.String nmEmpresa)
    {
        this._nmEmpresa = nmEmpresa;
    } //-- void setNmEmpresa(java.lang.String) 

    /**
     * Sets the value of field 'nomeCliente'.
     * 
     * @param nomeCliente the value of field 'nomeCliente'.
     */
    public void setNomeCliente(java.lang.String nomeCliente)
    {
        this._nomeCliente = nomeCliente;
    } //-- void setNomeCliente(java.lang.String) 

    /**
     * Sets the value of field 'nrAditivo'.
     * 
     * @param nrAditivo the value of field 'nrAditivo'.
     */
    public void setNrAditivo(long nrAditivo)
    {
        this._nrAditivo = nrAditivo;
        this._has_nrAditivo = true;
    } //-- void setNrAditivo(long) 

    /**
     * Sets the value of field 'nrAnexo'.
     * 
     * @param nrAnexo the value of field 'nrAnexo'.
     */
    public void setNrAnexo(int nrAnexo)
    {
        this._nrAnexo = nrAnexo;
        this._has_nrAnexo = true;
    } //-- void setNrAnexo(int) 

    /**
     * Sets the value of field 'nrLogradouroAgencia'.
     * 
     * @param nrLogradouroAgencia the value of field
     * 'nrLogradouroAgencia'.
     */
    public void setNrLogradouroAgencia(java.lang.String nrLogradouroAgencia)
    {
        this._nrLogradouroAgencia = nrLogradouroAgencia;
    } //-- void setNrLogradouroAgencia(java.lang.String) 

    /**
     * Method setOcorrenciasConta
     * 
     * 
     * 
     * @param index
     * @param vOcorrenciasConta
     */
    public void setOcorrenciasConta(int index, br.com.bradesco.web.pgit.service.data.pdc.imprimiraditivocontrato.response.OcorrenciasConta vOcorrenciasConta)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index >= _ocorrenciasContaList.size())) {
            throw new IndexOutOfBoundsException("setOcorrenciasConta: Index value '"+index+"' not in range [0.." + (_ocorrenciasContaList.size() - 1) + "]");
        }
        _ocorrenciasContaList.setElementAt(vOcorrenciasConta, index);
    } //-- void setOcorrenciasConta(int, br.com.bradesco.web.pgit.service.data.pdc.imprimiraditivocontrato.response.OcorrenciasConta) 

    /**
     * Method setOcorrenciasConta
     * 
     * 
     * 
     * @param ocorrenciasContaArray
     */
    public void setOcorrenciasConta(br.com.bradesco.web.pgit.service.data.pdc.imprimiraditivocontrato.response.OcorrenciasConta[] ocorrenciasContaArray)
    {
        //-- copy array
        _ocorrenciasContaList.removeAllElements();
        for (int i = 0; i < ocorrenciasContaArray.length; i++) {
            _ocorrenciasContaList.addElement(ocorrenciasContaArray[i]);
        }
    } //-- void setOcorrenciasConta(br.com.bradesco.web.pgit.service.data.pdc.imprimiraditivocontrato.response.OcorrenciasConta) 

    /**
     * Method setOcorrenciasParticipante
     * 
     * 
     * 
     * @param index
     * @param vOcorrenciasParticipante
     */
    public void setOcorrenciasParticipante(int index, br.com.bradesco.web.pgit.service.data.pdc.imprimiraditivocontrato.response.OcorrenciasParticipante vOcorrenciasParticipante)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index >= _ocorrenciasParticipanteList.size())) {
            throw new IndexOutOfBoundsException("setOcorrenciasParticipante: Index value '"+index+"' not in range [0.." + (_ocorrenciasParticipanteList.size() - 1) + "]");
        }
        _ocorrenciasParticipanteList.setElementAt(vOcorrenciasParticipante, index);
    } //-- void setOcorrenciasParticipante(int, br.com.bradesco.web.pgit.service.data.pdc.imprimiraditivocontrato.response.OcorrenciasParticipante) 

    /**
     * Method setOcorrenciasParticipante
     * 
     * 
     * 
     * @param ocorrenciasParticipanteArray
     */
    public void setOcorrenciasParticipante(br.com.bradesco.web.pgit.service.data.pdc.imprimiraditivocontrato.response.OcorrenciasParticipante[] ocorrenciasParticipanteArray)
    {
        //-- copy array
        _ocorrenciasParticipanteList.removeAllElements();
        for (int i = 0; i < ocorrenciasParticipanteArray.length; i++) {
            _ocorrenciasParticipanteList.addElement(ocorrenciasParticipanteArray[i]);
        }
    } //-- void setOcorrenciasParticipante(br.com.bradesco.web.pgit.service.data.pdc.imprimiraditivocontrato.response.OcorrenciasParticipante) 

    /**
     * Method setOcorrenciasServico
     * 
     * 
     * 
     * @param index
     * @param vOcorrenciasServico
     */
    public void setOcorrenciasServico(int index, br.com.bradesco.web.pgit.service.data.pdc.imprimiraditivocontrato.response.OcorrenciasServico vOcorrenciasServico)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index >= _ocorrenciasServicoList.size())) {
            throw new IndexOutOfBoundsException("setOcorrenciasServico: Index value '"+index+"' not in range [0.." + (_ocorrenciasServicoList.size() - 1) + "]");
        }
        _ocorrenciasServicoList.setElementAt(vOcorrenciasServico, index);
    } //-- void setOcorrenciasServico(int, br.com.bradesco.web.pgit.service.data.pdc.imprimiraditivocontrato.response.OcorrenciasServico) 

    /**
     * Method setOcorrenciasServico
     * 
     * 
     * 
     * @param ocorrenciasServicoArray
     */
    public void setOcorrenciasServico(br.com.bradesco.web.pgit.service.data.pdc.imprimiraditivocontrato.response.OcorrenciasServico[] ocorrenciasServicoArray)
    {
        //-- copy array
        _ocorrenciasServicoList.removeAllElements();
        for (int i = 0; i < ocorrenciasServicoArray.length; i++) {
            _ocorrenciasServicoList.addElement(ocorrenciasServicoArray[i]);
        }
    } //-- void setOcorrenciasServico(br.com.bradesco.web.pgit.service.data.pdc.imprimiraditivocontrato.response.OcorrenciasServico) 

    /**
     * Method setOcorrenciasServico2
     * 
     * 
     * 
     * @param index
     * @param vOcorrenciasServico2
     */
    public void setOcorrenciasServico2(int index, br.com.bradesco.web.pgit.service.data.pdc.imprimiraditivocontrato.response.OcorrenciasServico2 vOcorrenciasServico2)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index >= _ocorrenciasServico2List.size())) {
            throw new IndexOutOfBoundsException("setOcorrenciasServico2: Index value '"+index+"' not in range [0.." + (_ocorrenciasServico2List.size() - 1) + "]");
        }
        _ocorrenciasServico2List.setElementAt(vOcorrenciasServico2, index);
    } //-- void setOcorrenciasServico2(int, br.com.bradesco.web.pgit.service.data.pdc.imprimiraditivocontrato.response.OcorrenciasServico2) 

    /**
     * Method setOcorrenciasServico2
     * 
     * 
     * 
     * @param ocorrenciasServico2Array
     */
    public void setOcorrenciasServico2(br.com.bradesco.web.pgit.service.data.pdc.imprimiraditivocontrato.response.OcorrenciasServico2[] ocorrenciasServico2Array)
    {
        //-- copy array
        _ocorrenciasServico2List.removeAllElements();
        for (int i = 0; i < ocorrenciasServico2Array.length; i++) {
            _ocorrenciasServico2List.addElement(ocorrenciasServico2Array[i]);
        }
    } //-- void setOcorrenciasServico2(br.com.bradesco.web.pgit.service.data.pdc.imprimiraditivocontrato.response.OcorrenciasServico2) 

    /**
     * Sets the value of field 'qtdeConta'.
     * 
     * @param qtdeConta the value of field 'qtdeConta'.
     */
    public void setQtdeConta(int qtdeConta)
    {
        this._qtdeConta = qtdeConta;
        this._has_qtdeConta = true;
    } //-- void setQtdeConta(int) 

    /**
     * Sets the value of field 'qtdeParticipante'.
     * 
     * @param qtdeParticipante the value of field 'qtdeParticipante'
     */
    public void setQtdeParticipante(int qtdeParticipante)
    {
        this._qtdeParticipante = qtdeParticipante;
        this._has_qtdeParticipante = true;
    } //-- void setQtdeParticipante(int) 

    /**
     * Sets the value of field 'qtdeServico'.
     * 
     * @param qtdeServico the value of field 'qtdeServico'.
     */
    public void setQtdeServico(int qtdeServico)
    {
        this._qtdeServico = qtdeServico;
        this._has_qtdeServico = true;
    } //-- void setQtdeServico(int) 

    /**
     * Sets the value of field 'siglaUfAgencia'.
     * 
     * @param siglaUfAgencia the value of field 'siglaUfAgencia'.
     */
    public void setSiglaUfAgencia(java.lang.String siglaUfAgencia)
    {
        this._siglaUfAgencia = siglaUfAgencia;
    } //-- void setSiglaUfAgencia(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ImprimirAditivoContratoResponse
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.imprimiraditivocontrato.response.ImprimirAditivoContratoResponse unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.imprimiraditivocontrato.response.ImprimirAditivoContratoResponse) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.imprimiraditivocontrato.response.ImprimirAditivoContratoResponse.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimiraditivocontrato.response.ImprimirAditivoContratoResponse unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
