/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantercontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantercontrato.bean;

import java.math.BigDecimal;


/**
 * Nome: DetalharTipoServModContratoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class DetalharTipoServModContratoSaidaDTO {
	
	/** Atributo cdMensagem. */
	private String cdMensagem;
	
	/** Atributo mensagem. */
	private String mensagem;

	private String dsIndicador;
	
	private int dsOrigemIndicador;
	
	/** ************************************** TIPO DE SERVI�O ******************************************. */
	private int cdUtilizaPreAutorizacaoPagamentos;
	
	/** Atributo dsUtilizaPreAutorizacaoPagamentos. */
	private String dsUtilizaPreAutorizacaoPagamentos;
	
	/** Atributo cdTipoControleFloating. */
	private int cdTipoControleFloating;
	
	/** Atributo dsTipoControleFloating. */
	private String dsTipoControleFloating;
	/** Atributo emiteCartaoAntecipadoContaSalario. */
	private int emiteCartaoAntecipadoContaSalario;
	
	/** Atributo dsEmiteCartaoAntecipadoContaSalario. */
	private String dsEmiteCartaoAntecipadoContaSalario;
	
	/** Atributo cdTipoCartao. */
	private int cdTipoCartao;
	
	/** Atributo dsTipoCartao. */
	private String dsTipoCartao;
	
	/** Atributo qtdeLimiteSolicitacaoCartaoContaSalario. */
	private int qtdeLimiteSolicitacaoCartaoContaSalario;
	
	/** Atributo dtLimiteEnquadramentoConvenioContaSalario. */
	private String dtLimiteEnquadramentoConvenioContaSalario;
	
	/** The cd utilizacao rastreamento debito veiculo. */
	private int  cdUtilizacaoRastreamentoDebitoVeiculo;
	
	/** Atributo dsUtilizacaoRastreamentoDebitoVeiculo. */
	private String dsUtilizacaoRastreamentoDebitoVeiculo;
	
	/** Atributo cdPeriodicidadeRastreamentoDebitoVeiculo. */
	private int cdPeriodicidadeRastreamentoDebitoVeiculo;
	
	/** Atributo dsPeriodicidadeRastreamentoDebitoVeiculo. */
	private String dsPeriodicidadeRastreamentoDebitoVeiculo;
	
	/** Atributo cdAgendamentoDebitoVeiculoRastreamento. */
	private int cdAgendamentoDebitoVeiculoRastreamento;
	
	/** Atributo dsAgendamentoDebitoVeiculoRastreamento. */
	private String dsAgendamentoDebitoVeiculoRastreamento;
	
	/** Atributo cdPeriodicidadeCobrancaTarifa. */
	private int cdPeriodicidadeCobrancaTarifa;
	
	/** Atributo dsPeriodicidadeCobrancaTarifa. */
	private String dsPeriodicidadeCobrancaTarifa;
	
	/** Atributo diaFechamentoApuracaoTarifaMensal. */
	private int diaFechamentoApuracaoTarifaMensal;
	
	/** Atributo qtdeDiasCobrancaTarifaAposApuracao. */
	private int qtdeDiasCobrancaTarifaAposApuracao;
	
	/** Atributo cdTipoReajusteTarifa. */
	private int cdTipoReajusteTarifa;
	
	/** Atributo dsTipoReajusteTarifa. */
	private String dsTipoReajusteTarifa;
	
	/** Atributo qtdeMesesReajusteAutomaticoTarifa. */
	private int qtdeMesesReajusteAutomaticoTarifa;
	
	/** Atributo cdIndiceEconomicoReajuste. */
	private java.math.BigDecimal cdIndiceEconomicoReajuste;
	
	/** Atributo cdTipoAlimentacaoPagamentos. */
	private int cdTipoAlimentacaoPagamentos;
	
	/** Atributo cdTipoAutorizacaoPagamento. */
	private int cdTipoAutorizacaoPagamento;
	
	/** Atributo dsTipoAutorizacaoPagamento. */
	private String dsTipoAutorizacaoPagamento;
	
	/** Atributo cdInformaAgenciaContaCredito. */
	private int cdInformaAgenciaContaCredito;
	
	/** Atributo dsInformaAgenciaContaCredito. */
	private String dsInformaAgenciaContaCredito;
	
	/** Atributo cdTipoIdentificacaoBeneficio. */
	private int cdTipoIdentificacaoBeneficio;
	
	/** Atributo dsTipoIdentificacaoBeneficio. */
	private String dsTipoIdentificacaoBeneficio;
	
	/** Atributo cdUtilizaCadastroOrgaosPagadores. */
	private int cdUtilizaCadastroOrgaosPagadores;
	
	/** Atributo dsUtilizaCadastroOrgaosPagadores. */
	private String dsUtilizaCadastroOrgaosPagadores;
	
	/** Atributo cdMantemCadastroProcuradores. */
	private int cdMantemCadastroProcuradores;
	
	/** Atributo dsMantemCadastroProcuradores. */
	private String dsMantemCadastroProcuradores;
	
	/** Atributo cdPeriodicidadeManutencaoCadastroProcurador. */
	private int cdPeriodicidadeManutencaoCadastroProcurador;
	
	/** Atributo dsPeriodicidadeManutencaoCadastroProcurador. */
	private String dsPeriodicidadeManutencaoCadastroProcurador;
	
	/** Atributo cdFormaManutencaoCadastroProcurador. */
	private int cdFormaManutencaoCadastroProcurador;
	
	/** Atributo dsFormaManutencaoCadastroProcurador. */
	private String dsFormaManutencaoCadastroProcurador;
	
	/** Atributo cdPossuiExpiracaoCredito. */
	private int cdPossuiExpiracaoCredito;
	
	/** Atributo dsPossuiExpiracaoCredito. */
	private String dsPossuiExpiracaoCredito;
	
	/** Atributo cdFormaControleExpiracaoCredito. */
	private int cdFormaControleExpiracaoCredito;
	
	/** Atributo dsFormaControleExpiracaoCredito. */
	private String dsFormaControleExpiracaoCredito;
	
	/** Atributo qtdeDiasExpiracaoCredito. */
	private int qtdeDiasExpiracaoCredito;
	
	/** Atributo cdTratamentoFeriadoFimVigencia. */
	private int cdTratamentoFeriadoFimVigencia;
	
	/** Atributo dsTratamentoFeriadoFimVigencia. */
	private String dsTratamentoFeriadoFimVigencia;
	
	/** Atributo cdTipoPrestacaoContaCreditoPago. */
	private int cdTipoPrestacaoContaCreditoPago;
	
	/** Atributo dsTipoPrestacaoContaCreditoPago. */
	private String dsTipoPrestacaoContaCreditoPago;
	
	/** Atributo cdFormaManutencaoCadastro. */
	private int cdFormaManutencaoCadastro;
	
	/** Atributo dsFormaManutencaoCadastro. */
	private String dsFormaManutencaoCadastro;
	
	/** Atributo qtdeDiasInativacaoFavorecido. */
	private int qtdeDiasInativacaoFavorecido;
	
	/** Atributo cdConsistenciaCpfCnpjFavorecido. */
	private int cdConsistenciaCpfCnpjFavorecido;
	
	/** Atributo dsConsistenciaCpfCnpjFavorecido. */
	private String dsConsistenciaCpfCnpjFavorecido;
	/** Atributo cdPeriodicidade. */
	private int cdPeriodicidade;
	
	/** Atributo dsPeriodicidade. */
	private String dsPeriodicidade;
	
	/** Atributo cdDestino. */
	private int cdDestino;
	
	/** Atributo cdPermiteCorrespondenciaAberta. */
	private int cdPermiteCorrespondenciaAberta;
	
	/** Atributo cdDemonstraInformacoesAreaReservada. */
	private int cdDemonstraInformacoesAreaReservada;
	
	/** Atributo cdAgruparCorrespondencia. */
	private int cdAgruparCorrespondencia;
	
	/** Atributo qtdeVias. */
	private int qtdeVias;
	
	/** Atributo cdFloatServicoContrato. */
	private Integer cdFloatServicoContrato;
	
	/** Atributo dsFloatServicoContrato. */
    private String  dsFloatServicoContrato;
	/** Atributo cdTipoRejeicao. */
	private int cdTipoRejeicao;
	
	/** Atributo cdTipoEmissao. */
	private int cdTipoEmissao;
	
	/** Atributo cdMidiaDisponivelCorrentista. */
	private int cdMidiaDisponivelCorrentista;
	
	/** Atributo cdMidiaDisponivelNaoCorrentista. */
	private int cdMidiaDisponivelNaoCorrentista;
	
	/** Atributo cdTipoEnvioCorrespondencia. */
	private int cdTipoEnvioCorrespondencia;
	
	/** Atributo cdFrasesPreCadastratadas. */
	private int cdFrasesPreCadastratadas;
	
	/** Atributo cdCobrarTarifasFuncionarios. */
	private int cdCobrarTarifasFuncionarios;
	
	/** Atributo qtdeMesesEmissao. */
	private int qtdeMesesEmissao;
	
	/** Atributo qtdeLinhas. */
	private int qtdeLinhas;
	
	/** Atributo qtdeViasEmissaoComprovantes. */
	private int qtdeViasEmissaoComprovantes;
	
	/** Atributo qtdeViasPagas. */
	private int qtdeViasPagas;
	
	/** Atributo cdFormularioParaImpressao. */
	private int cdFormularioParaImpressao;
	
	/** Atributo cdPeriodicidadeCobrancaTarifaEmissaoComprovantes. */
	private int cdPeriodicidadeCobrancaTarifaEmissaoComprovantes;
	
	/** Atributo dsPeriodicidadeCobrancaTarifaEmissaoComprovantes. */
	private String dsPeriodicidadeCobrancaTarifaEmissaoComprovantes;
	
	/** Atributo cdFechamentoApuracao. */
	private int cdFechamentoApuracao;
	
	/** Atributo qtdeDiasCobrancaAposApuracao. */
	private int qtdeDiasCobrancaAposApuracao;
	
	/** Atributo cdTipoReajuste. */
	private int cdTipoReajuste;
	/** Atributo qtdeMesesReajusteAutomatico. */
	private int qtdeMesesReajusteAutomatico;
	
	/** Atributo cdIndiceReajuste. */
	private int cdIndiceReajuste;
	
	/** Atributo percentualReajuste. */
	private BigDecimal percentualReajuste;
	
	/** Atributo porcentagemBonificacaoTarifa. */
	private BigDecimal porcentagemBonificacaoTarifa;
	
	/** Atributo cdTipoIdentificacaoFuncionario. */
	private int cdTipoIdentificacaoFuncionario;
	
	/** Atributo dsTipoIdentificacaoFuncionario. */
	private String dsTipoIdentificacaoFuncionario;
	
	/** Atributo cdTipoConsistenciaIdentificador. */
	private int cdTipoConsistenciaIdentificador;
	
	/** Atributo dsTipoConsistenciaIdentificador. */
	private String dsTipoConsistenciaIdentificador;
	
	/** Atributo cdCondicaoEnquadramento. */
	private int cdCondicaoEnquadramento;
	
	/** Atributo dsCondicaoEnquadramento. */
	private String dsCondicaoEnquadramento;
	
	/** Atributo cdCriterioPrincipalEnquadramento. */
	private int cdCriterioPrincipalEnquadramento;
	
	/** Atributo dsCriterioPrincipalEnquadramento. */
	private String dsCriterioPrincipalEnquadramento;
	
	/** Atributo cdCriterioCompostoEnquadramento. */
	private int cdCriterioCompostoEnquadramento;
	
	/** Atributo dsCriterioCompostoEnquadramento. */
	private String dsCriterioCompostoEnquadramento;
	
	/** Atributo qtdeEtapasRecadastramento. */
	private int qtdeEtapasRecadastramento;
	
	/** Atributo qtdeFasesPorEtapa. */
	private int qtdeFasesPorEtapa;
	
	/** Atributo qtdeMesesPrazoPorFase. */
	private int qtdeMesesPrazoPorFase;
	
	/** Atributo qtdeMesesPrazoPorEtapa. */
	private int qtdeMesesPrazoPorEtapa;
	
	/** Atributo cdPermiteEnvioRemessaManutencao. */
	private int cdPermiteEnvioRemessaManutencao;
	
	/** Atributo cdPeriodicidadeEnvioRemessa. */
	private int cdPeriodicidadeEnvioRemessa;
	
	/** Atributo dsPeriodicidadeEnvioRemessa. */
	private String dsPeriodicidadeEnvioRemessa;
	
	/** Atributo cdBaseDadosUtilizada. */
	private int cdBaseDadosUtilizada;
	
	/** Atributo cdTipoCargaBaseDadosUtilizada. */
	private int cdTipoCargaBaseDadosUtilizada;
	
	/** Atributo cdPermiteAnteciparRecadastramento. */
	private int cdPermiteAnteciparRecadastramento;
	
	/** Atributo dtLimiteInicioVinculoCargaBase. */
	private String dtLimiteInicioVinculoCargaBase;
	
	/** Atributo dtInicioRecadastramento. */
	private String dtInicioRecadastramento;
	
	/** Atributo dtFimRecadastramento. */
	private String dtFimRecadastramento;
	
	/** Atributo cdPermiteAcertosDados. */
	private int cdPermiteAcertosDados;
	
	/** Atributo dtInicioPeriodoAcertoDados. */
	private String dtInicioPeriodoAcertoDados;
	
	/** Atributo dtFimPeriodoAcertoDados. */
	private String dtFimPeriodoAcertoDados;
	
	/** Atributo cdEmiteMensagemRecadastramentoMidiaOnLine. */
	private int cdEmiteMensagemRecadastramentoMidiaOnLine;
	
	/** Atributo cdMidiaMensagemRecastramentoOnLine. */
	private int cdMidiaMensagemRecastramentoOnLine;
	/** Atributo cdPeriodicidadeReajusteTarifa. */
	private int cdPeriodicidadeReajusteTarifa;
	
	/** Atributo dsPeriodicidadeReajusteTarifa. */
	private String dsPeriodicidadeReajusteTarifa;
	/** Atributo cdIndiceEconomicoReajusteTarifa. */
	private int cdIndiceEconomicoReajusteTarifa;
	
	/** Atributo dsIndiceEconomicoReajusteTarifa. */
	private String dsIndiceEconomicoReajusteTarifa;
	
	/** Atributo percentualIndiceEconomicoReajusteTarifa. */
	private BigDecimal percentualIndiceEconomicoReajusteTarifa;
	
	/** Atributo percentualBonificacaoTarifaPadrao. */
	private BigDecimal percentualBonificacaoTarifaPadrao;
	
	
	/** ************************************** MODALIDADE ******************************************. */
	private int cdPeriocidadeEnvio;
	
	/** Atributo dsPeriocidadeEnvio. */
	private String dsPeriocidadeEnvio;
	
	/** Atributo cdEnderecoEnvio. */
	private int cdEnderecoEnvio;
	
	/** Atributo dsEnderecoEnvio. */
	private String dsEnderecoEnvio;
	
	/** Atributo qntDiasAntecipacao. */
	private int qntDiasAntecipacao;
	/** Atributo dsDestino. */
	private String dsDestino;
	
	/** Atributo cdPermitirAgrupamento. */
	private int cdPermitirAgrupamento;
	
	/** Atributo dsPermitirAgrupamento. */
	private String dsPermitirAgrupamento;
	
	/** Atributo cdTipoComprovacao. */
	private int cdTipoComprovacao;
	
	/** Atributo dsTipoComprovacao. */
	private String dsTipoComprovacao;
	
	/** Atributo qntdMesesPeriComprovacao. */
	private int qntdMesesPeriComprovacao;
	
	/** Atributo codUtilizarMsgPersOnline. */
	private int codUtilizarMsgPersOnline;
	
	/** Atributo dsUtilizarMsgPersOnline. */
	private String dsUtilizarMsgPersOnline;
	/** Atributo qntdDiasAvisoVenc. */
	private int qntdDiasAvisoVenc;
	
	/** Atributo qntdDiasAntecedeInicio. */
	private int qntdDiasAntecedeInicio;
	
	/** Atributo cdEnderecoUtilizadoEnvio. */
	private int cdEnderecoUtilizadoEnvio;
	
	/** Atributo dsEnderecoUtilizadoEnvio. */
	private String dsEnderecoUtilizadoEnvio;
	
	
	/** Atributo cdTratamentoFeriado. */
	private int cdTratamentoFeriado;
	
	/** Atributo dsTratamentoFeriado. */
	private String dsTratamentoFeriado;
	
	/** Atributo cdTratamentoValorDivergente. */
	private int cdTratamentoValorDivergente;
	
	/** Atributo dsTratamentoValorDivergente. */
	private String dsTratamentoValorDivergente;
	
	/** Atributo valorMaxPagFavNaoCadastrado. */
	private BigDecimal valorMaxPagFavNaoCadastrado;
	
	/** Atributo cdTipoConsultaSaldo. */
	private int cdTipoConsultaSaldo;
	
	/** Atributo dsTipoConsultaSaldo. */
	private String dsTipoConsultaSaldo;
	/** Atributo cdControlePagFavorecido. */
	private int cdControlePagFavorecido;
	
	/** Atributo dsControlePagFavorecido. */
	private String dsControlePagFavorecido;
	
	/** Atributo cdTipoConsistenciaCpfCnpjProp. */
	private int cdTipoConsistenciaCpfCnpjProp;
	
	/** Atributo dsTipoConsistenciaCpfCnpjProp. */
	private String dsTipoConsistenciaCpfCnpjProp;
	
	/** Atributo cdTipoEfetivacao. */
	private int cdTipoEfetivacao;
	
	/** Atributo dsTipoEfetivacao. */
	private String dsTipoEfetivacao;
	/** Atributo cdPrioridadeCredito. */
	private int cdPrioridadeCredito;
	
	/** Atributo dsPrioridadeCredito. */
	private String dsPrioridadeCredito;
	/** Atributo qntdMaxInconsistenciaLote. */
	private int qntdMaxInconsistenciaLote;
	/** Atributo qntdDiasRepConsulta. */
	private int qntdDiasRepConsulta;
	/** Atributo porcentInconsistenciaLote. */
	private int porcentInconsistenciaLote;
	
	/** Atributo permiteContingenciaPag. */
	private int permiteContingenciaPag;
	
	/** Atributo cdPermiteConsultaFavorecido. */
	private int cdPermiteConsultaFavorecido;
	
	/** Atributo dsPermiteConsultaFavorecido. */
	private String dsPermiteConsultaFavorecido;
	/** Atributo cdTipoRejeicaoAgendamento. */
	private int cdTipoRejeicaoAgendamento;
	
	/** Atributo dsTipoRejeicaoAgendamento. */
	private String dsTipoRejeicaoAgendamento;
	
	/** Atributo cdPrioridadeDebito. */
	private int cdPrioridadeDebito;
	
	/** Atributo dsPrioridadeDebito. */
	private String dsPrioridadeDebito;
	
	/** Atributo valorMaximoPagamentoFavorecidoNaoCadastrado. */
	private BigDecimal valorMaximoPagamentoFavorecidoNaoCadastrado;	
	
	/** Atributo valorLimiteDiario. */
	private BigDecimal valorLimiteDiario;
	
	/** Atributo valorLimiteIndividual. */
	private BigDecimal valorLimiteIndividual;
	
	/** Atributo quantDiasRepique. */
	private int quantDiasRepique;
	
	/** Atributo cdTipoProcessamento. */
	private int cdTipoProcessamento;
	
	/** Atributo dsTipoProcessamento. */
	private String dsTipoProcessamento;
	
	/** Atributo cdTipoRejeicaoEfetivacao. */
	private int cdTipoRejeicaoEfetivacao;
	
	/** Atributo dsTipoRejeicaoEfetivacao. */
	private String dsTipoRejeicaoEfetivacao;
	
	/** Atributo cdControlePagamentoFavorecido. */
	private int cdControlePagamentoFavorecido;
	
	/** Atributo dsControlePagamentoFavorecido. */
	private String dsControlePagamentoFavorecido;
	
	/** Atributo qtdeMaxInconsistenciaPorLote. */
	private int qtdeMaxInconsistenciaPorLote;
	/** Atributo cdTipoRastreamentoTitulos. */
	private int cdTipoRastreamentoTitulos;
	
	/** Atributo dsTipoRastreamentoTitulos. */
	private String dsTipoRastreamentoTitulos;
	
	/** Atributo rdoRastrearNotasFiscais. */
	private int rdoRastrearNotasFiscais;
	
	/** Atributo rdoAgendarTitRastProp. */
	private int rdoAgendarTitRastProp;
	
	/** Atributo rdoBloquearEmissaoPap. */
	private int rdoBloquearEmissaoPap;
	
	/** Atributo dataInicioRastreamento. */
	private String dataInicioRastreamento;
	
	/** Atributo rdoRastrearTitTerceiros. */
	private int rdoRastrearTitTerceiros;
	
	/** Atributo rdoCapturarTitDtaRegistro. */
	private int rdoCapturarTitDtaRegistro;
	
	/** Atributo rdoRastrearTitRastreadoFilial. */
	private int rdoRastrearTitRastreadoFilial;
	
	/** Atributo dataInicioBloqPapeleta. */
	private String dataInicioBloqPapeleta;
	
	/** Atributo dataRegistroTitulo. */
	private String dataRegistroTitulo;
	/** Atributo rdoUtilizaCadFavControlePag. */
	private int rdoUtilizaCadFavControlePag;
	
	/** Atributo cdTratamentoFeriadosDtaPag. */
	private int cdTratamentoFeriadosDtaPag;
	
	/** Atributo dsTratamentoFeriadosDtaPag. */
	private String dsTratamentoFeriadosDtaPag;
	/** Atributo rdoPermiteFavConsultarPag. */
	private int rdoPermiteFavConsultarPag;
	/** Atributo rdoGerarLanctoFuturoDeb. */
	private int rdoGerarLanctoFuturoDeb;
	
	/** Atributo rdoPermitePagarVencido. */
	private int rdoPermitePagarVencido;
	
	/** Atributo rdoPermitePagarMenor. */
	private int rdoPermitePagarMenor;
	
	/** Atributo qntdLimiteDiasPagVencido. */
	private int qntdLimiteDiasPagVencido;

	/** Atributo rdoPermiteDebitoOnline. */
	private int rdoPermiteDebitoOnline;
	
	/** Atributo rdoGerarLanctoFuturoCred. */
	private int rdoGerarLanctoFuturoCred;
	/** Atributo rdoGerarLanctoProgramado. */
	private int rdoGerarLanctoProgramado;
	/** Atributo cdPermiteEstornoPagamento. */
	private int cdPermiteEstornoPagamento;
	
	/** Atributo dsPermiteEstornoPagamento. */
	private String dsPermiteEstornoPagamento;
	
	/** Atributo cdEfetuaConsistencia. */
	private int cdEfetuaConsistencia;
	
	/** Atributo dsEfetuaConsistencia. */
	private String dsEfetuaConsistencia;
	
	/** Atributo cdTipoTratamentoContasTransferidas. */
	private int cdTipoTratamentoContasTransferidas;
	
	/** Atributo dsTipoTratamentoContasTransferidas. */
	private String dsTipoTratamentoContasTransferidas;
	
	/** Atributo qntdMaxRegInconsistentes. */
	private int qntdMaxRegInconsistentes;
	
	/** Atributo percentualMaxRegInconsistenciaLote. */
	private int percentualMaxRegInconsistenciaLote;
	
	
	/** Atributo rdoOcorrenciaDebito. */
	private int rdoOcorrenciaDebito;
	
	/** Atributo qntdDiasExpiracao. */
	private int qntdDiasExpiracao;
	
	/** Atributo rdoPermiteOutrosTiposIncricaoFav. */
	private int rdoPermiteOutrosTiposIncricaoFav;
	/** Atributo cdOcorrenciaDebito. */
	private int cdOcorrenciaDebito;
	
	/** Atributo dsOcorrenciaDebito. */
	private String dsOcorrenciaDebito;
	
	/** Atributo dsLocalEmissao. */
	private String dsLocalEmissao;
	
	/** Atributo perInconsistenciaLote. */
	private int perInconsistenciaLote;
	
	/** Atributo cdPermiteContingenciaPag. */
	private int cdPermiteContingenciaPag;
	
	/** Atributo dsPermiteContingenciaPag. */
	private String dsPermiteContingenciaPag;
	
	
	/** ***************************************** TRILHA DE AUDITORIA ******************************************. */
	
	private String usuarioInclusao;
	
	/** Atributo usuarioManutencao. */
	private String usuarioManutencao;
	
	/** Atributo horaManutencao. */
	private String horaManutencao;
	
	/** Atributo horaInclusao. */
	private String horaInclusao;
	//private int cdCanalInclusao;
	/** Atributo cdCanalManutencao. */
	private int cdCanalManutencao;
	//private String dsCanalInclusao;
	/** Atributo dsCanalManutencao. */
	private String dsCanalManutencao;
	
	/** Atributo complementoInclusao. */
	private String complementoInclusao;
	
	/** Atributo complementoManutencao. */
	private String complementoManutencao;
	
	/** ***************************************** NOVAS VARI�VEIS ******************************************. */
	
	
	private Integer cdAcaoNaoVida;
    
    /** Atributo dsAcaoNaoVida. */
    private String dsAcaoNaoVida;
    
    /** Atributo cdAcertoDadoRecadastro. */
    private Integer cdAcertoDadoRecadastro;
    
    /** Atributo dsAcertoDadoRecadastro. */
    private String dsAcertoDadoRecadastro;
    
    /** Atributo cdAgendaDebitoVeiculo. */
    private Integer cdAgendaDebitoVeiculo;
    
    /** Atributo dsAgendaDebitoVeiculo. */
    private String dsAgendaDebitoVeiculo;
    
    /** Atributo cdAgendaPagamentoVencido. */
    private Integer cdAgendaPagamentoVencido;
    
    /** Atributo dsAgendaPagamentoVencido. */
    private String dsAgendaPagamentoVencido;
    
    /** Atributo cdAgendaValorMenor. */
    private Integer cdAgendaValorMenor;
    
    /** Atributo dsAgendaValorMenor. */
    private String dsAgendaValorMenor;
    
    /** Atributo cdAgrupamentoAviso. */
    private Integer cdAgrupamentoAviso;
    
    /** Atributo dsAgrupamentoAviso. */
    private String dsAgrupamentoAviso;
    
    /** Atributo cdAgrupamentoComprovado. */
    private Integer cdAgrupamentoComprovado;
    
    /** Atributo dsAgrupamentoComprovado. */
    private String dsAgrupamentoComprovado;
    
    /** Atributo cdAgrupamentoFormularioRecadastro. */
    private Integer cdAgrupamentoFormularioRecadastro;
    
    /** Atributo dsAgrupamentoFormularioRecadastro. */
    private String dsAgrupamentoFormularioRecadastro;
    
    /** Atributo cdAntecRecadastroBeneficio. */
    private Integer cdAntecRecadastroBeneficio;
    
    /** Atributo dsAntecRecadastroBeneficio. */
    private String dsAntecRecadastroBeneficio;
    
    /** Atributo cdAreaReservada. */
    private Integer cdAreaReservada;
    
    /** Atributo dsAreaReservada. */
    private String dsAreaReservada;
    
    /** Atributo cdBaseRecadastroBeneficio. */
    private Integer cdBaseRecadastroBeneficio;
    
    /** Atributo dsBaseRecadastroBeneficio. */
    private String dsBaseRecadastroBeneficio;
    
    /** Atributo cdBloqueioEmissaoPplta. */
    private Integer cdBloqueioEmissaoPplta;
    
    /** Atributo dsBloqueioEmissaoPplta. */
    private String dsBloqueioEmissaoPplta;
    
    /** Atributo cdCapituloTituloRegistro. */
    private Integer cdCapituloTituloRegistro;
    
    /** Atributo dsCapituloTituloRegistro. */
    private String dsCapituloTituloRegistro;
    
    /** Atributo cdCobrancaTarifa. */
    private Integer cdCobrancaTarifa;
    
    /** Atributo dsCobrancaTarifa. */
    private String dsCobrancaTarifa;
    
    /** Atributo cdConsDebitoVeiculo. */
    private Integer cdConsDebitoVeiculo;
    
    /** Atributo dsConsDebitoVeiculo. */
    private String dsConsDebitoVeiculo;
    
    /** Atributo cdConsEndereco. */
    private Integer cdConsEndereco;
    
    /** Atributo dsConsEndereco. */
    private String dsConsEndereco;
    
    /** Atributo cdConsSaldoPagamento. */
    private Integer cdConsSaldoPagamento;
    
    /** Atributo dsConsSaldoPagamento. */
    private String dsConsSaldoPagamento;
    
    /** Atributo cdContagemConsSaldo. */
    private Integer cdContagemConsSaldo;
    
    /** Atributo dsContagemConsSaldo. */
    private String dsContagemConsSaldo;
    
    /** Atributo cdCreditoNaoUtilizado. */
    private Integer cdCreditoNaoUtilizado;
    
    /** Atributo dsCreditoNaoUtilizado. */
    private String dsCreditoNaoUtilizado;
    
    /** Atributo cdCriterioEnquaBeneficio. */
    private Integer cdCriterioEnquaBeneficio;
    
    /** Atributo dsCriterioEnquaBeneficio. */
    private String dsCriterioEnquaBeneficio;
    
    /** Atributo cdCriterioEnquaRecadastro. */
    private Integer cdCriterioEnquaRecadastro;
    
    /** Atributo dsCriterioEnquaRecadastro. */
    private String dsCriterioEnquaRecadastro;
    
    /** Atributo cdCriterioRastreabilidadeTitulo. */
    private Integer cdCriterioRastreabilidadeTitulo;
    
    /** Atributo dsCriterioRastreabilidadeTitulo. */
    private String dsCriterioRastreabilidadeTitulo;
    
    /** Atributo cdCtciaEspecieBeneficio. */
    private Integer cdCtciaEspecieBeneficio;
    
    /** Atributo dsCtciaEspecieBeneficio. */
    private String dsCtciaEspecieBeneficio;
    
    /** Atributo cdCtciaIdentificacaoBeneficio. */
    private Integer cdCtciaIdentificacaoBeneficio;
    
    /** Atributo dsCtciaIdentificacaoBeneficio. */
    private String dsCtciaIdentificacaoBeneficio;
    
    /** Atributo cdCtciaInscricaoFavorecido. */
    private Integer cdCtciaInscricaoFavorecido;
    
    /** Atributo dsCtciaInscricaoFavorecido. */
    private String dsCtciaInscricaoFavorecido;
    
    /** Atributo cdCtciaProprietarioVeiculo. */
    private Integer cdCtciaProprietarioVeiculo;
    
    /** Atributo dsCtciaProprietarioVeiculo. */
    private String dsCtciaProprietarioVeiculo;
    
    /** Atributo cdDispzContaCredito. */
    private Integer cdDispzContaCredito;
    
    /** Atributo dsDispzContaCredito. */
    private String dsDispzContaCredito;
    
    /** Atributo cdDispzDiversarCrrtt. */
    private Integer cdDispzDiversarCrrtt;
    
    /** Atributo dsDispzDiversarCrrtt. */
    private String dsDispzDiversarCrrtt;
    
    /** Atributo cdDispzDiversasNao. */
    private Integer cdDispzDiversasNao;
    
    /** Atributo dsDispzDiversasNao. */
    private String dsDispzDiversasNao;
    
    /** Atributo cdDispzSalarioCrrtt. */
    private Integer cdDispzSalarioCrrtt;
    
    /** Atributo dsDispzSalarioCrrtt. */
    private String dsDispzSalarioCrrtt;
    
    /** Atributo cdDispzSalarioNao. */
    private Integer cdDispzSalarioNao;
    
    /** Atributo dsDispzSalarioNao. */
    private String dsDispzSalarioNao;
    
    /** Atributo cdDestinoAviso. */
    private Integer cdDestinoAviso;
    
    /** Atributo dsDestinoAviso. */
    private String dsDestinoAviso;
    
    /** Atributo cdDestinoComprovante. */
    private Integer cdDestinoComprovante;
    
    /** Atributo dsDestinoComprovante. */
    private String dsDestinoComprovante;
    
    /** Atributo cdDestinoFormularioRecadastro. */
    private Integer cdDestinoFormularioRecadastro;
    
    /** Atributo dsDestinoFormularioRecadastro. */
    private String dsDestinoFormularioRecadastro;
    
    /** Atributo cdEnvelopeAberto. */
    private Integer cdEnvelopeAberto;
    
    /** Atributo dsEnvelopeAberto. */
    private String dsEnvelopeAberto;
    
    /** Atributo cdFavorecidoConsPagamento. */
    private Integer cdFavorecidoConsPagamento;
    
    /** Atributo dsFavorecidoConsPagamento. */
    private String dsFavorecidoConsPagamento;
    
    /** Atributo cdFormaAutorizacaoPagamento. */
    private Integer cdFormaAutorizacaoPagamento;
    
    /** Atributo dsFormaAutorizacaoPagamento. */
    private String dsFormaAutorizacaoPagamento;
    
    /** Atributo cdFormaEnvioPagamento. */
    private Integer cdFormaEnvioPagamento;
    
    /** Atributo dsFormaEnvioPagamento. */
    private String dsFormaEnvioPagamento;
    
    /** Atributo cdFormaExpiracaoCredito. */
    private Integer cdFormaExpiracaoCredito;
    
    /** Atributo dsFormaExpiracaoCredito. */
    private String dsFormaExpiracaoCredito;
    
    /** Atributo cdFormaManutencao. */
    private Integer cdFormaManutencao;
    
    /** Atributo dsFormaManutencao. */
    private String dsFormaManutencao;
    
    /** Atributo cdFrasePreCadastro. */
    private Integer cdFrasePreCadastro;
    
    /** Atributo dsFrasePreCadastro. */
    private String dsFrasePreCadastro;
    
    /** Atributo cdIndicadorAgendaTitulo. */
    private Integer cdIndicadorAgendaTitulo;
    
    /** Atributo dsIndicadorAgendaTitulo. */
    private String dsIndicadorAgendaTitulo;
    
    /** Atributo cdIndicadorAutorizacaoCliente. */
    private Integer cdIndicadorAutorizacaoCliente;
    
    /** Atributo dsIndicadorAutorizacaoCliente. */
    private String dsIndicadorAutorizacaoCliente;
    
    /** Atributo cdIndicadorAutorizacaoComplemento. */
    private Integer cdIndicadorAutorizacaoComplemento;
    
    /** Atributo dsIndicadorAutorizacaoComplemento. */
    private String dsIndicadorAutorizacaoComplemento;
    
    /** Atributo cdIndicadorCadastroOrg. */
    private Integer cdIndicadorCadastroOrg;
    
    /** Atributo dsIndicadorCadastroOrg. */
    private String dsIndicadorCadastroOrg;
    
    /** Atributo cdIndicadorCadastroProcd. */
    private Integer cdIndicadorCadastroProcd;
    
    /** Atributo dsIndicadorCadastroProcd. */
    private String dsIndicadorCadastroProcd;
    
    /** Atributo cdIndicadorCataoSalario. */
    private Integer cdIndicadorCataoSalario;
    
    /** Atributo dsIndicadorCataoSalario. */
    private String dsIndicadorCataoSalario;
    
    /** Atributo cdIndicadorEconomicoReajuste. */
    private Integer cdIndicadorEconomicoReajuste;
    
    /** Atributo dsIndicadorEconomicoReajuste. */
    private String dsIndicadorEconomicoReajuste;
    
    /** Atributo cdIndicadorExpiracaoCredito. */
    private Integer cdIndicadorExpiracaoCredito;
    
    /** Atributo dsIndicadorExpiracaoCredito. */
    private String dsIndicadorExpiracaoCredito;
    
    /** Atributo cdIndicadorLancamentoPagamento. */
    private Integer cdIndicadorLancamentoPagamento;
    
    /** Atributo dsIndicadorLancamentoPagamento. */
    private String dsIndicadorLancamentoPagamento;
    
    /** Atributo cdIndicadorMensagemPerso. */
    private Integer cdIndicadorMensagemPerso;
    
    /** Atributo dsIndicadorMensagemPerso. */
    private String dsIndicadorMensagemPerso;
    
    /** Atributo cdLancamentoFuturoCredito. */
    private Integer cdLancamentoFuturoCredito;
    
    /** Atributo dsLancamentoFuturoCredito. */
    private String dsLancamentoFuturoCredito;
    
    /** Atributo cdLancamentoFuturoDebito. */
    private Integer cdLancamentoFuturoDebito;
    
    /** Atributo dsLancamentoFuturoDebito. */
    private String dsLancamentoFuturoDebito;
    
    /** Atributo cdLiberacaoLoteProcesso. */
    private Integer cdLiberacaoLoteProcesso;
    
    /** Atributo dsLiberacaoLoteProcesso. */
    private String dsLiberacaoLoteProcesso;
    
    /** Atributo cdManutencaoBaseRecadastro. */
    private Integer cdManutencaoBaseRecadastro;
    
    /** Atributo dsManutencaoBaseRecadastro. */
    private String dsManutencaoBaseRecadastro;
    
    /** Atributo cdMidiaDisponivel. */
    private Integer cdMidiaDisponivel;
    
    /** Atributo dsMidiaDisponivel. */
    private String dsMidiaDisponivel;
    
    /** Atributo cdMidiaMensagemRecadastro. */
    private Integer cdMidiaMensagemRecadastro;
    
    /** Atributo dsMidiaMensagemRecadastro. */
    private String dsMidiaMensagemRecadastro;
    
    /** Atributo cdMomentoAvisoRacadastro. */
    private Integer cdMomentoAvisoRacadastro;
    
    /** Atributo dsMomentoAvisoRacadastro. */
    private String dsMomentoAvisoRacadastro;
    
    /** Atributo cdMomentoCreditoEfetivacao. */
    private Integer cdMomentoCreditoEfetivacao;
    
    /** Atributo dsMomentoCreditoEfetivacao. */
    private String dsMomentoCreditoEfetivacao;
    
    /** Atributo cdMomentoDebitoPagamento. */
    private Integer cdMomentoDebitoPagamento;
    
    /** Atributo dsMomentoDebitoPagamento. */
    private String dsMomentoDebitoPagamento;
    
    /** Atributo cdMomentoFormularioRecadastro. */
    private Integer cdMomentoFormularioRecadastro;
    
    /** Atributo dsMomentoFormularioRecadastro. */
    private String dsMomentoFormularioRecadastro;
    
    /** Atributo cdMomentoProcessamentoPagamento. */
    private Integer cdMomentoProcessamentoPagamento;
    
    /** Atributo dsMomentoProcessamentoPagamento. */
    private String dsMomentoProcessamentoPagamento;
    
    /** Atributo cdMensagemRecadastroMidia. */
    private Integer cdMensagemRecadastroMidia;
    
    /** Atributo dsMensagemRecadastroMidia. */
    private String dsMensagemRecadastroMidia;
    
    /** Atributo cdPermissaoDebitoOnline. */
    private Integer cdPermissaoDebitoOnline;
    
    /** Atributo dsPermissaoDebitoOnline. */
    private String dsPermissaoDebitoOnline;
    
    /** Atributo cdNaturezaOperacaoPagamento. */
    private Integer cdNaturezaOperacaoPagamento;
    
    /** Atributo dsNaturezaOperacaoPagamento. */
    private String dsNaturezaOperacaoPagamento;
    
    /** Atributo cdPeriodicidadeAviso. */
    private Integer cdPeriodicidadeAviso;
    
    /** Atributo dsPeriodicidadeAviso. */
    private String dsPeriodicidadeAviso;
    
    /** Atributo cdPerdcCobrancaTarifa. */
    private Integer cdPerdcCobrancaTarifa;
    
    /** Atributo dsPerdcCobrancaTarifa. */
    private String dsPerdcCobrancaTarifa;
    
    /** Atributo cdPerdcComprovante. */
    private Integer cdPerdcComprovante;
    
    /** Atributo dsPerdcComprovante. */
    private String dsPerdcComprovante;
    
    /** Atributo cdPerdcConsultaVeiculo. */
    private Integer cdPerdcConsultaVeiculo;
    
    /** Atributo dsPerdcConsultaVeiculo. */
    private String dsPerdcConsultaVeiculo;
    
    /** Atributo cdPerdcEnvioRemessa. */
    private Integer cdPerdcEnvioRemessa;
    
    /** Atributo dsPerdcEnvioRemessa. */
    private String dsPerdcEnvioRemessa;
    
    /** Atributo cdPerdcManutencaoProcd. */
    private Integer cdPerdcManutencaoProcd;
    
    /** Atributo dsPerdcManutencaoProcd. */
    private String dsPerdcManutencaoProcd;
    
    /** Atributo cdPagamentoNaoUtilizado. */
    private Integer cdPagamentoNaoUtilizado;
    
    /** Atributo dsPagamentoNaoUtilizado. */
    private String dsPagamentoNaoUtilizado;
    
    /** Atributo cdPrincipalEnquaRecadastro. */
    private Integer cdPrincipalEnquaRecadastro;
    
    /** Atributo dsPrincipalEnquaRecadastro. */
    private String dsPrincipalEnquaRecadastro;
    
    /** Atributo cdPrioridadeEfetivacaoPagamento. */
    private Integer cdPrioridadeEfetivacaoPagamento;
    
    /** Atributo dsPrioridadeEfetivacaoPagamento. */
    private String dsPrioridadeEfetivacaoPagamento;
    
    /** Atributo cdRejeicaoAgendaLote. */
    private Integer cdRejeicaoAgendaLote;
    
    /** Atributo dsRejeicaoAgendaLote. */
    private String dsRejeicaoAgendaLote;
    
    /** Atributo cdRejeicaoEfetivacaoLote. */
    private Integer cdRejeicaoEfetivacaoLote;
    
    /** Atributo dsRejeicaoEfetivacaoLote. */
    private String dsRejeicaoEfetivacaoLote;
    
    /** Atributo cdRejeicaoLote. */
    private Integer cdRejeicaoLote;
    
    /** Atributo dsRejeicaoLote. */
    private String dsRejeicaoLote;
    
    /** Atributo cdRastreabilidadeNotaFiscal. */
    private Integer cdRastreabilidadeNotaFiscal;
    
    /** Atributo dsRastreabilidadeNotaFiscal. */
    private String dsRastreabilidadeNotaFiscal;
    
    /** Atributo cdRastreabilidadeTituloTerceiro. */
    private Integer cdRastreabilidadeTituloTerceiro;
    
    /** Atributo dsRastreabilidadeTituloTerceiro. */
    private String dsRastreabilidadeTituloTerceiro;
    
    /** Atributo cdTipoCargaRecadastro. */
    private Integer cdTipoCargaRecadastro;
    
    /** Atributo dsTipoCargaRecadastro. */
    private String dsTipoCargaRecadastro;
    
    /** Atributo cdTipoCataoSalario. */
    private Integer cdTipoCataoSalario;
    
    /** Atributo dsTipoCataoSalario. */
    private String dsTipoCataoSalario;
    
    /** Atributo cdTipoDataFloat. */
    private Integer cdTipoDataFloat;
    
    /** Atributo dsTipoDataFloat. */
    private String dsTipoDataFloat;
    
    /** Atributo cdTipoDivergenciaVeiculo. */
    private Integer cdTipoDivergenciaVeiculo;
    
    /** Atributo dsTipoDivergenciaVeiculo. */
    private String dsTipoDivergenciaVeiculo;
    
    /** Atributo cdTipoIdBeneficio. */
    private Integer cdTipoIdBeneficio;
    
    /** Atributo dsTipoIdBeneficio. */
    private String dsTipoIdBeneficio;
    
    /** Atributo cdTipoLayoutArquivo. */
    private Integer cdTipoLayoutArquivo;
    
    /** Atributo dsTipoLayoutArquivo. */
    private String dsTipoLayoutArquivo;
   /* private Integer cdTipoReajusteTarifa;
    private String dsTipoReajusteTarifa;*/
    /** Atributo cdContratoContaTransferencia. */
   private Integer cdContratoContaTransferencia;
    
    /** Atributo dsContratoContaTransferencia. */
    private String dsContratoContaTransferencia;
    
    /** Atributo cdUtilizacaoFavorecidoControle. */
    private Integer cdUtilizacaoFavorecidoControle;
    
    /** Atributo dsUtilizacaoFavorecidoControle. */
    private String dsUtilizacaoFavorecidoControle;
    
    /** Atributo dtEnquaContaSalario. */
    private String dtEnquaContaSalario;
    
    /** Atributo dtInicioBloqueioPplta. */
    private String dtInicioBloqueioPplta;
    
    /** Atributo dtInicioRastreabilidadeTitulo. */
    private String dtInicioRastreabilidadeTitulo;
    
    /** Atributo dtFimAcertoRecadastro. */
    private String dtFimAcertoRecadastro;
    
    /** Atributo dtFimRecadastroBeneficio. */
    private String dtFimRecadastroBeneficio;
    
    /** Atributo dtinicioAcertoRecadastro. */
    private String dtinicioAcertoRecadastro;
    
    /** Atributo dtInicioRecadastroBeneficio. */
    private String dtInicioRecadastroBeneficio;
    
    /** Atributo dtLimiteVinculoCarga. */
    private String dtLimiteVinculoCarga;
    
    /** Atributo nrFechamentoApuracaoTarifa. */
    private Integer nrFechamentoApuracaoTarifa;
    
    /** Atributo cdPercentualIndicadorReajusteTarifa. */
    private BigDecimal cdPercentualIndicadorReajusteTarifa;
    
    /** Atributo cdPercentualMaximoInconLote. */
    private Integer cdPercentualMaximoInconLote;
    
    /** Atributo cdPercentualReducaoTarifaCatalogo. */
    private BigDecimal cdPercentualReducaoTarifaCatalogo;
    
    /** Atributo qtAntecedencia. */
    private Integer qtAntecedencia;
    
    /** Atributo qtAnteriorVencimentoComprovado. */
    private Integer qtAnteriorVencimentoComprovado;
    
    /** Atributo qtDiaCobrancaTarifa. */
    private Integer qtDiaCobrancaTarifa;
    
    /** Atributo qtDiaExpiracao. */
    private Integer qtDiaExpiracao;
    
    /** Atributo cdDiaFloatPagamento. */
    private Integer cdDiaFloatPagamento;
    
    /** Atributo qtDiaInatividadeFavorecido. */
    private Integer qtDiaInatividadeFavorecido;
    
    /** Atributo qtDiaRepiqConsulta. */
    private Integer qtDiaRepiqConsulta;
    
    /** Atributo qtEtapasRecadastroBeneficio. */
    private Integer qtEtapasRecadastroBeneficio;
    
    /** Atributo qtFaseRecadastroBeneficio. */
    private Integer qtFaseRecadastroBeneficio;
    
    /** Atributo qtLimiteLinha. */
    private Integer qtLimiteLinha;
    
    /** Atributo qtLimiteSolicitacaoCatao. */
    private Integer qtLimiteSolicitacaoCatao;
    
    /** Atributo qtMaximaInconLote. */
    private Integer qtMaximaInconLote;
    
    /** Atributo qtMaximaTituloVencido. */
    private Integer qtMaximaTituloVencido;
    
    /** Atributo qtMesComprovante. */
    private Integer qtMesComprovante;
    
    /** Atributo qtMesEtapaRecadastro. */
    private Integer qtMesEtapaRecadastro;
    
    /** Atributo qtMesFaseRecadastro. */
    private Integer qtMesFaseRecadastro;
    
    /** Atributo qtMesReajusteTarifa. */
    private Integer qtMesReajusteTarifa;
    
    /** Atributo qtViaAviso. */
    private Integer qtViaAviso;
    
    /** Atributo qtViaCobranca. */
    private Integer qtViaCobranca;
    
    /** Atributo qtViaComprovante. */
    private Integer qtViaComprovante;
    
    /** Atributo vlFavorecidoNaoCadastro. */
    private BigDecimal vlFavorecidoNaoCadastro;
    
    /** Atributo vlLimiteDiaPagamento. */
    private BigDecimal vlLimiteDiaPagamento;
    
    /** Atributo vlLimiteIndividualPagamento. */
    private BigDecimal vlLimiteIndividualPagamento;
    
    /** Atributo cdIndicadorEmissaoAviso. */
    private Integer cdIndicadorEmissaoAviso;
    
    /** Atributo dsIndicadorEmissaoAviso. */
    private String dsIndicadorEmissaoAviso;
    
    /** Atributo cdIndicadorRetornoInternet. */
    private Integer cdIndicadorRetornoInternet;
    
    /** Atributo dsIndicadorRetornoInternet. */
    private String dsIndicadorRetornoInternet;
    
    /** Atributo cdIndicadorRetornoSeparado. */
    private Integer cdIndicadorRetornoSeparado;
    
    /** Atributo dsCodigoIndicadorRetornoSeparado. */
    private String dsCodigoIndicadorRetornoSeparado;
    
    /** Atributo cdIndicadorListaDebito. */
    private Integer cdIndicadorListaDebito;
    
    /** Atributo dsIndicadorListaDebito. */
    private String dsIndicadorListaDebito;
    
    /** Atributo cdTipoFormacaoLista. */
    private Integer cdTipoFormacaoLista;
    
    /** Atributo dsTipoFormacaoLista. */
    private String dsTipoFormacaoLista;
    
    /** Atributo cdTipoConsistenciaLista. */
    private Integer cdTipoConsistenciaLista;
    
    /** Atributo dsTipoConsistenciaLista. */
    private String dsTipoConsistenciaLista;
    
    /** Atributo cdTipoConsultaComprovante. */
    private Integer cdTipoConsultaComprovante;
    
    /** Atributo dsTipoConsolidacaoComprovante. */
    private String dsTipoConsolidacaoComprovante;
    
    /** Atributo cdValidacaoNomeFavorecido. */
    private Integer cdValidacaoNomeFavorecido;
    
    /** Atributo dsValidacaoNomeFavorecido. */
    private String dsValidacaoNomeFavorecido;
    
    /** Atributo cdTipoContaFavorecido. */
    private Integer cdTipoContaFavorecido;
    
    /** Atributo dsTipoConsistenciaFavorecido. */
    private String dsTipoConsistenciaFavorecido;
    
    /** Atributo cdIndLancamentoPersonalizado. */
    private Integer cdIndLancamentoPersonalizado;
    
    /** Atributo dsIndLancamentoPersonalizado. */
    private String dsIndLancamentoPersonalizado;
    
    /** Atributo cdAgendaRastreabilidadeFilial. */
    private Integer cdAgendaRastreabilidadeFilial;
    
    /** Atributo dsAgendamentoRastFilial. */
    private String dsAgendamentoRastFilial;
    
    /** Atributo cdIndicadorAdesaoSacador. */
    private Integer cdIndicadorAdesaoSacador;
    
    /** Atributo dsIndicadorAdesaoSacador. */
    private String dsIndicadorAdesaoSacador;
    
    /** Atributo cdMeioPagamentoCredito. */
    private Integer cdMeioPagamentoCredito;
    
    /** Atributo dsMeioPagamentoCredito. */
    private String dsMeioPagamentoCredito;
    
    /** Atributo cdTipoIsncricaoFavorecido. */
    private Integer cdTipoIsncricaoFavorecido;
    
    /** Atributo dsTipoIscricaoFavorecido. */
    private String dsTipoIscricaoFavorecido;
    
    /** Atributo cdIndicadorBancoPostal. */
    private Integer cdIndicadorBancoPostal;
    
    /** Atributo dsIndicadorBancoPostal. */
    private String dsIndicadorBancoPostal;
    
    /** Atributo cdFormularioContratoCliente. */
    private Integer cdFormularioContratoCliente;
    
    /** Atributo dsCodigoFormularioContratoCliente. */
    private String dsCodigoFormularioContratoCliente;
    
    /** Atributo cdUsuarioInclusao. */
    private String cdUsuarioInclusao;
    
    /** Atributo cdUsuarioExternoInclusao. */
    private String cdUsuarioExternoInclusao;
    
    /** Atributo hrManutencaoRegistroInclusao. */
    private String hrManutencaoRegistroInclusao;
    
    /** Atributo cdCanalInclusao. */
    private Integer cdCanalInclusao;
    
    /** Atributo dsCanalInclusao. */
    private String dsCanalInclusao;
    
    /** Atributo nmOperacaoFluxoInclusao. */
    private String nmOperacaoFluxoInclusao;
    
    /** Atributo cdUsuarioAlteracao. */
    private String cdUsuarioAlteracao;
    
    /** Atributo cdUsuarioExternoAlteracao. */
    private String cdUsuarioExternoAlteracao;
    
    /** Atributo hrManutencaoRegistroAlteracao. */
    private String hrManutencaoRegistroAlteracao;
    
    /** Atributo cdCanalAlteracao. */
    private Integer cdCanalAlteracao;
    
    /** Atributo dsCanalAlteracao. */
    private String dsCanalAlteracao;
    
    /** Atributo nmOperacaoFluxoAlteracao. */
    private String nmOperacaoFluxoAlteracao;
    
    /** Atributo dsAreaResrd. */
    private String dsAreaResrd;
    
    /** Atributo cdConsultaSaldoValorSuperior. */
    private Integer cdConsultaSaldoValorSuperior;
    
    /** Atributo dsConsultaSaldoSuperior. */
    private String dsConsultaSaldoSuperior;
    
    /** Atributo cdIndicadorFeriadoLocal. */
    private Integer cdIndicadorFeriadoLocal;
    
    /** Atributo dsCodigoIndFeriadoLocal. */
    private String dsCodigoIndFeriadoLocal;
    
    /** Atributo cdIndicadorSegundaLinha. */
    private Integer cdIndicadorSegundaLinha;
    
    /** Atributo dsIndicadorSegundaLinha. */
    private String dsIndicadorSegundaLinha;
    
    /** Atributo cdExigeAutFilial. */
	private Integer cdExigeAutFilial;

	/** Atributo cdPreenchimentoLancamentoPersonalizado. */
	private Integer cdPreenchimentoLancamentoPersonalizado;

	/** Atributo dsPreenchimentoLancamentoPersonalizado. */
	private String dsPreenchimentoLancamentoPersonalizado;

	/** Atributo cdTituloDdaRetorno. */
	private Integer cdTituloDdaRetorno;
	
	/** Atributo dsTituloDdaRetorno. */
	private String dsTituloDdaRetorno;

	/** Atributo cdIndicadorAgendaGrade. */
	private Integer cdIndicadorAgendaGrade;
	
	/** Atributo qtDiaUtilPgto. */
	private Integer qtDiaUtilPgto;
	
	/** Atributo dsIndicadorAgendaGrade. */
	private String dsIndicadorAgendaGrade;
	
	/** The cd indicador utiliza mora. */
	private Integer cdIndicadorUtilizaMora;
	
	/** The ds indicador utiliza mora. */
	private String dsIndicadorUtilizaMora;

	/** Atributo vlPercentualDiferencaTolerada. */
	private BigDecimal vlPercentualDiferencaTolerada = null;
	
    /** Atributo cdConsistenciaCpfCnpjBenefAvalNpc. */
    private Integer cdConsistenciaCpfCnpjBenefAvalNpc;
    
    /** Atributo dsConsistenciaCpfCnpjBenefAvalNpc. */
    private String dsConsistenciaCpfCnpjBenefAvalNpc;	
    
    /** Atributo cdIndicadorTipoRetornoInternet. */
    private Integer cdIndicadorTipoRetornoInternet;
    
    /** Atributo dsCodIndicadorTipoRetornoInternet. */
    private String dsCodIndicadorTipoRetornoInternet;
	
	/**
	 * Get: cdContratoContaTransferencia.
	 *
	 * @return cdContratoContaTransferencia
	 */
    
    
	public Integer getCdContratoContaTransferencia() {
		return cdContratoContaTransferencia;
	}
	
	public Integer getCdConsistenciaCpfCnpjBenefAvalNpc() {
		return cdConsistenciaCpfCnpjBenefAvalNpc;
	}

	public void setCdConsistenciaCpfCnpjBenefAvalNpc(
			Integer cdConsistenciaCpfCnpjBenefAvalNpc) {
		this.cdConsistenciaCpfCnpjBenefAvalNpc = cdConsistenciaCpfCnpjBenefAvalNpc;
	}

	public String getDsConsistenciaCpfCnpjBenefAvalNpc() {
		return dsConsistenciaCpfCnpjBenefAvalNpc;
	}

	public void setDsConsistenciaCpfCnpjBenefAvalNpc(
			String dsConsistenciaCpfCnpjBenefAvalNpc) {
		this.dsConsistenciaCpfCnpjBenefAvalNpc = dsConsistenciaCpfCnpjBenefAvalNpc;
	}

	/**
	 * Set: cdContratoContaTransferencia.
	 *
	 * @param cdContratoContaTransferencia the cd contrato conta transferencia
	 */
	public void setCdContratoContaTransferencia(Integer cdContratoContaTransferencia) {
		this.cdContratoContaTransferencia = cdContratoContaTransferencia;
	}
	
	/**
	 * Get: cdIndicadorLancamentoPagamento.
	 *
	 * @return cdIndicadorLancamentoPagamento
	 */
	public Integer getCdIndicadorLancamentoPagamento() {
		return cdIndicadorLancamentoPagamento;
	}
	
	/**
	 * Set: cdIndicadorLancamentoPagamento.
	 *
	 * @param cdIndicadorLancamentoPagamento the cd indicador lancamento pagamento
	 */
	public void setCdIndicadorLancamentoPagamento(
			Integer cdIndicadorLancamentoPagamento) {
		this.cdIndicadorLancamentoPagamento = cdIndicadorLancamentoPagamento;
	}
	
	/**
	 * Get: cdLiberacaoLoteProcesso.
	 *
	 * @return cdLiberacaoLoteProcesso
	 */
	public Integer getCdLiberacaoLoteProcesso() {
		return cdLiberacaoLoteProcesso;
	}
	
	/**
	 * Set: cdLiberacaoLoteProcesso.
	 *
	 * @param cdLiberacaoLoteProcesso the cd liberacao lote processo
	 */
	public void setCdLiberacaoLoteProcesso(Integer cdLiberacaoLoteProcesso) {
		this.cdLiberacaoLoteProcesso = cdLiberacaoLoteProcesso;
	}
	
	/**
	 * Get: cdPagamentoNaoUtilizado.
	 *
	 * @return cdPagamentoNaoUtilizado
	 */
	public Integer getCdPagamentoNaoUtilizado() {
		return cdPagamentoNaoUtilizado;
	}
	
	/**
	 * Set: cdPagamentoNaoUtilizado.
	 *
	 * @param cdPagamentoNaoUtilizado the cd pagamento nao utilizado
	 */
	public void setCdPagamentoNaoUtilizado(Integer cdPagamentoNaoUtilizado) {
		this.cdPagamentoNaoUtilizado = cdPagamentoNaoUtilizado;
	}
	
	/**
	 * Get: cdValidacaoNomeFavorecido.
	 *
	 * @return cdValidacaoNomeFavorecido
	 */
	public Integer getCdValidacaoNomeFavorecido() {
		return cdValidacaoNomeFavorecido;
	}
	
	/**
	 * Set: cdValidacaoNomeFavorecido.
	 *
	 * @param cdValidacaoNomeFavorecido the cd validacao nome favorecido
	 */
	public void setCdValidacaoNomeFavorecido(Integer cdValidacaoNomeFavorecido) {
		this.cdValidacaoNomeFavorecido = cdValidacaoNomeFavorecido;
	}
	
	/**
	 * Get: vlLimiteDiaPagamento.
	 *
	 * @return vlLimiteDiaPagamento
	 */
	public BigDecimal getVlLimiteDiaPagamento() {
		return vlLimiteDiaPagamento;
	}
	
	/**
	 * Set: vlLimiteDiaPagamento.
	 *
	 * @param vlLimiteDiaPagamento the vl limite dia pagamento
	 */
	public void setVlLimiteDiaPagamento(BigDecimal vlLimiteDiaPagamento) {
		this.vlLimiteDiaPagamento = vlLimiteDiaPagamento;
	}
	
	/**
	 * Get: vlLimiteIndividualPagamento.
	 *
	 * @return vlLimiteIndividualPagamento
	 */
	public BigDecimal getVlLimiteIndividualPagamento() {
		return vlLimiteIndividualPagamento;
	}
	
	/**
	 * Set: vlLimiteIndividualPagamento.
	 *
	 * @param vlLimiteIndividualPagamento the vl limite individual pagamento
	 */
	public void setVlLimiteIndividualPagamento(
			BigDecimal vlLimiteIndividualPagamento) {
		this.vlLimiteIndividualPagamento = vlLimiteIndividualPagamento;
	}
	
	/**
	 * Get: cdPrioridadeEfetivacaoPagamento.
	 *
	 * @return cdPrioridadeEfetivacaoPagamento
	 */
	public Integer getCdPrioridadeEfetivacaoPagamento() {
		return cdPrioridadeEfetivacaoPagamento;
	}
	
	/**
	 * Set: cdPrioridadeEfetivacaoPagamento.
	 *
	 * @param cdPrioridadeEfetivacaoPagamento the cd prioridade efetivacao pagamento
	 */
	public void setCdPrioridadeEfetivacaoPagamento(
			Integer cdPrioridadeEfetivacaoPagamento) {
		this.cdPrioridadeEfetivacaoPagamento = cdPrioridadeEfetivacaoPagamento;
	}
	
	/**
	 * Get: qtMaximaInconLote.
	 *
	 * @return qtMaximaInconLote
	 */
	public Integer getQtMaximaInconLote() {
		return qtMaximaInconLote;
	}
	
	/**
	 * Set: qtMaximaInconLote.
	 *
	 * @param qtMaximaInconLote the qt maxima incon lote
	 */
	public void setQtMaximaInconLote(Integer qtMaximaInconLote) {
		this.qtMaximaInconLote = qtMaximaInconLote;
	}
	
	/**
	 * Get: cdCanalInclusao.
	 *
	 * @return cdCanalInclusao
	 */
	public int getCdCanalInclusao() {
		return cdCanalInclusao;
	}
	
	/**
	 * Set: cdCanalInclusao.
	 *
	 * @param cdCanalInclusao the cd canal inclusao
	 */
	public void setCdCanalInclusao(int cdCanalInclusao) {
		this.cdCanalInclusao = cdCanalInclusao;
	}
	
	/**
	 * Get: cdCanalManutencao.
	 *
	 * @return cdCanalManutencao
	 */
	public int getCdCanalManutencao() {
		return cdCanalManutencao;
	}
	
	/**
	 * Set: cdCanalManutencao.
	 *
	 * @param cdCanalManutencao the cd canal manutencao
	 */
	public void setCdCanalManutencao(int cdCanalManutencao) {
		this.cdCanalManutencao = cdCanalManutencao;
	}
	
	/**
	 * Get: cdFormaAutorizacaoPagamento.
	 *
	 * @return cdFormaAutorizacaoPagamento
	 */
	public int getCdFormaAutorizacaoPagamento() {
		return cdFormaAutorizacaoPagamento;
	}
	
	/**
	 * Set: cdFormaAutorizacaoPagamento.
	 *
	 * @param cdFormaAutorizacaoPagamento the cd forma autorizacao pagamento
	 */
	public void setCdFormaAutorizacaoPagamento(int cdFormaAutorizacaoPagamento) {
		this.cdFormaAutorizacaoPagamento = cdFormaAutorizacaoPagamento;
	}
	
	/**
	 * Get: cdFormaEnvioPagamento.
	 *
	 * @return cdFormaEnvioPagamento
	 */
	public int getCdFormaEnvioPagamento() {
		return cdFormaEnvioPagamento;
	}
	
	/**
	 * Set: cdFormaEnvioPagamento.
	 *
	 * @param cdFormaEnvioPagamento the cd forma envio pagamento
	 */
	public void setCdFormaEnvioPagamento(int cdFormaEnvioPagamento) {
		this.cdFormaEnvioPagamento = cdFormaEnvioPagamento;
	}
	
	/**
	 * Get: cdTipoControleFloating.
	 *
	 * @return cdTipoControleFloating
	 */
	public int getCdTipoControleFloating() {
		return cdTipoControleFloating;
	}
	
	/**
	 * Set: cdTipoControleFloating.
	 *
	 * @param cdTipoControleFloating the cd tipo controle floating
	 */
	public void setCdTipoControleFloating(int cdTipoControleFloating) {
		this.cdTipoControleFloating = cdTipoControleFloating;
	}
	
	/**
	 * Get: cdTipoProcessamento.
	 *
	 * @return cdTipoProcessamento
	 */
	public int getCdTipoProcessamento() {
		return cdTipoProcessamento;
	}
	
	/**
	 * Set: cdTipoProcessamento.
	 *
	 * @param cdTipoProcessamento the cd tipo processamento
	 */
	public void setCdTipoProcessamento(int cdTipoProcessamento) {
		this.cdTipoProcessamento = cdTipoProcessamento;
	}
	
	/**
	 * Get: cdTipoRejeicaoAgendamento.
	 *
	 * @return cdTipoRejeicaoAgendamento
	 */
	public int getCdTipoRejeicaoAgendamento() {
		return cdTipoRejeicaoAgendamento;
	}
	
	/**
	 * Set: cdTipoRejeicaoAgendamento.
	 *
	 * @param cdTipoRejeicaoAgendamento the cd tipo rejeicao agendamento
	 */
	public void setCdTipoRejeicaoAgendamento(int cdTipoRejeicaoAgendamento) {
		this.cdTipoRejeicaoAgendamento = cdTipoRejeicaoAgendamento;
	}
	
	/**
	 * Get: cdTipoRejeicaoEfetivacao.
	 *
	 * @return cdTipoRejeicaoEfetivacao
	 */
	public int getCdTipoRejeicaoEfetivacao() {
		return cdTipoRejeicaoEfetivacao;
	}
	
	/**
	 * Set: cdTipoRejeicaoEfetivacao.
	 *
	 * @param cdTipoRejeicaoEfetivacao the cd tipo rejeicao efetivacao
	 */
	public void setCdTipoRejeicaoEfetivacao(int cdTipoRejeicaoEfetivacao) {
		this.cdTipoRejeicaoEfetivacao = cdTipoRejeicaoEfetivacao;
	}
	
	/**
	 * Get: cdUtilizaPreAutorizacaoPagamentos.
	 *
	 * @return cdUtilizaPreAutorizacaoPagamentos
	 */
	public int getCdUtilizaPreAutorizacaoPagamentos() {
		return cdUtilizaPreAutorizacaoPagamentos;
	}
	
	/**
	 * Set: cdUtilizaPreAutorizacaoPagamentos.
	 *
	 * @param cdUtilizaPreAutorizacaoPagamentos the cd utiliza pre autorizacao pagamentos
	 */
	public void setCdUtilizaPreAutorizacaoPagamentos(
			int cdUtilizaPreAutorizacaoPagamentos) {
		this.cdUtilizaPreAutorizacaoPagamentos = cdUtilizaPreAutorizacaoPagamentos;
	}
	
	/**
	 * Get: complementoInclusao.
	 *
	 * @return complementoInclusao
	 */
	public String getComplementoInclusao() {
		return complementoInclusao;
	}
	
	/**
	 * Set: complementoInclusao.
	 *
	 * @param complementoInclusao the complemento inclusao
	 */
	public void setComplementoInclusao(String complementoInclusao) {
		this.complementoInclusao = complementoInclusao;
	}
	
	/**
	 * Get: complementoManutencao.
	 *
	 * @return complementoManutencao
	 */
	public String getComplementoManutencao() {
		return complementoManutencao;
	}
	
	/**
	 * Set: complementoManutencao.
	 *
	 * @param complementoManutencao the complemento manutencao
	 */
	public void setComplementoManutencao(String complementoManutencao) {
		this.complementoManutencao = complementoManutencao;
	}
	
	/**
	 * Get: dsCanalInclusao.
	 *
	 * @return dsCanalInclusao
	 */
	public String getDsCanalInclusao() {
		return dsCanalInclusao;
	}
	
	/**
	 * Set: dsCanalInclusao.
	 *
	 * @param dsCanalInclusao the ds canal inclusao
	 */
	public void setDsCanalInclusao(String dsCanalInclusao) {
		this.dsCanalInclusao = dsCanalInclusao;
	}
	
	/**
	 * Get: dsCanalManutencao.
	 *
	 * @return dsCanalManutencao
	 */
	public String getDsCanalManutencao() {
		return dsCanalManutencao;
	}
	
	/**
	 * Set: dsCanalManutencao.
	 *
	 * @param dsCanalManutencao the ds canal manutencao
	 */
	public void setDsCanalManutencao(String dsCanalManutencao) {
		this.dsCanalManutencao = dsCanalManutencao;
	}
	
	/**
	 * Get: dsFormaAutorizacaoPagamento.
	 *
	 * @return dsFormaAutorizacaoPagamento
	 */
	public String getDsFormaAutorizacaoPagamento() {
		return dsFormaAutorizacaoPagamento;
	}
	
	/**
	 * Set: dsFormaAutorizacaoPagamento.
	 *
	 * @param dsFormaAutorizacaoPagamento the ds forma autorizacao pagamento
	 */
	public void setDsFormaAutorizacaoPagamento(String dsFormaAutorizacaoPagamento) {
		this.dsFormaAutorizacaoPagamento = dsFormaAutorizacaoPagamento;
	}
	
	/**
	 * Get: dsFormaEnvioPagamento.
	 *
	 * @return dsFormaEnvioPagamento
	 */
	public String getDsFormaEnvioPagamento() {
		return dsFormaEnvioPagamento;
	}
	
	/**
	 * Set: dsFormaEnvioPagamento.
	 *
	 * @param dsFormaEnvioPagamento the ds forma envio pagamento
	 */
	public void setDsFormaEnvioPagamento(String dsFormaEnvioPagamento) {
		this.dsFormaEnvioPagamento = dsFormaEnvioPagamento;
	}
	
	/**
	 * Get: dsPrioridadeDebito.
	 *
	 * @return dsPrioridadeDebito
	 */
	public String getDsPrioridadeDebito() {
		return dsPrioridadeDebito;
	}
	
	/**
	 * Set: dsPrioridadeDebito.
	 *
	 * @param dsPrioridadeDebito the ds prioridade debito
	 */
	public void setDsPrioridadeDebito(String dsPrioridadeDebito) {
		this.dsPrioridadeDebito = dsPrioridadeDebito;
	}
	
	/**
	 * Get: dsTipoControleFloating.
	 *
	 * @return dsTipoControleFloating
	 */
	public String getDsTipoControleFloating() {
		return dsTipoControleFloating;
	}
	
	/**
	 * Set: dsTipoControleFloating.
	 *
	 * @param dsTipoControleFloating the ds tipo controle floating
	 */
	public void setDsTipoControleFloating(String dsTipoControleFloating) {
		this.dsTipoControleFloating = dsTipoControleFloating;
	}
	
	/**
	 * Get: dsTipoProcessamento.
	 *
	 * @return dsTipoProcessamento
	 */
	public String getDsTipoProcessamento() {
		return dsTipoProcessamento;
	}
	
	/**
	 * Set: dsTipoProcessamento.
	 *
	 * @param dsTipoProcessamento the ds tipo processamento
	 */
	public void setDsTipoProcessamento(String dsTipoProcessamento) {
		this.dsTipoProcessamento = dsTipoProcessamento;
	}
	
	/**
	 * Get: dsTipoRejeicaoAgendamento.
	 *
	 * @return dsTipoRejeicaoAgendamento
	 */
	public String getDsTipoRejeicaoAgendamento() {
		return dsTipoRejeicaoAgendamento;
	}
	
	/**
	 * Set: dsTipoRejeicaoAgendamento.
	 *
	 * @param dsTipoRejeicaoAgendamento the ds tipo rejeicao agendamento
	 */
	public void setDsTipoRejeicaoAgendamento(String dsTipoRejeicaoAgendamento) {
		this.dsTipoRejeicaoAgendamento = dsTipoRejeicaoAgendamento;
	}
	
	/**
	 * Get: dsTipoRejeicaoEfetivacao.
	 *
	 * @return dsTipoRejeicaoEfetivacao
	 */
	public String getDsTipoRejeicaoEfetivacao() {
		return dsTipoRejeicaoEfetivacao;
	}
	
	/**
	 * Set: dsTipoRejeicaoEfetivacao.
	 *
	 * @param dsTipoRejeicaoEfetivacao the ds tipo rejeicao efetivacao
	 */
	public void setDsTipoRejeicaoEfetivacao(String dsTipoRejeicaoEfetivacao) {
		this.dsTipoRejeicaoEfetivacao = dsTipoRejeicaoEfetivacao;
	}
	
	/**
	 * Get: dsUtilizaPreAutorizacaoPagamentos.
	 *
	 * @return dsUtilizaPreAutorizacaoPagamentos
	 */
	public String getDsUtilizaPreAutorizacaoPagamentos() {
		return dsUtilizaPreAutorizacaoPagamentos;
	}
	
	/**
	 * Set: dsUtilizaPreAutorizacaoPagamentos.
	 *
	 * @param dsUtilizaPreAutorizacaoPagamentos the ds utiliza pre autorizacao pagamentos
	 */
	public void setDsUtilizaPreAutorizacaoPagamentos(
			String dsUtilizaPreAutorizacaoPagamentos) {
		this.dsUtilizaPreAutorizacaoPagamentos = dsUtilizaPreAutorizacaoPagamentos;
	}
	
	/**
	 * Get: horaInclusao.
	 *
	 * @return horaInclusao
	 */
	public String getHoraInclusao() {
		return horaInclusao;
	}
	
	/**
	 * Set: horaInclusao.
	 *
	 * @param horaInclusao the hora inclusao
	 */
	public void setHoraInclusao(String horaInclusao) {
		this.horaInclusao = horaInclusao;
	}
	
	/**
	 * Get: horaManutencao.
	 *
	 * @return horaManutencao
	 */
	public String getHoraManutencao() {
		return horaManutencao;
	}
	
	/**
	 * Set: horaManutencao.
	 *
	 * @param horaManutencao the hora manutencao
	 */
	public void setHoraManutencao(String horaManutencao) {
		this.horaManutencao = horaManutencao;
	}
	
	/**
	 * Get: quantDiasRepique.
	 *
	 * @return quantDiasRepique
	 */
	public int getQuantDiasRepique() {
		return quantDiasRepique;
	}
	
	/**
	 * Set: quantDiasRepique.
	 *
	 * @param quantDiasRepique the quant dias repique
	 */
	public void setQuantDiasRepique(int quantDiasRepique) {
		this.quantDiasRepique = quantDiasRepique;
	}
	
	/**
	 * Get: usuarioInclusao.
	 *
	 * @return usuarioInclusao
	 */
	public String getUsuarioInclusao() {
		return usuarioInclusao;
	}
	
	/**
	 * Set: usuarioInclusao.
	 *
	 * @param usuarioInclusao the usuario inclusao
	 */
	public void setUsuarioInclusao(String usuarioInclusao) {
		this.usuarioInclusao = usuarioInclusao;
	}
	
	/**
	 * Get: usuarioManutencao.
	 *
	 * @return usuarioManutencao
	 */
	public String getUsuarioManutencao() {
		return usuarioManutencao;
	}
	
	/**
	 * Set: usuarioManutencao.
	 *
	 * @param usuarioManutencao the usuario manutencao
	 */
	public void setUsuarioManutencao(String usuarioManutencao) {
		this.usuarioManutencao = usuarioManutencao;
	}
	
	
	/**
	 * Get: cdMensagem.
	 *
	 * @return cdMensagem
	 */
	public String getCdMensagem() {
		return cdMensagem;
	}
	
	/**
	 * Set: cdMensagem.
	 *
	 * @param cdMensagem the cd mensagem
	 */
	public void setCdMensagem(String cdMensagem) {
		this.cdMensagem = cdMensagem;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	/**
	 * Get: cdTipoCartao.
	 *
	 * @return cdTipoCartao
	 */
	public int getCdTipoCartao() {
		return cdTipoCartao;
	}
	
	/**
	 * Set: cdTipoCartao.
	 *
	 * @param cdTipoCartao the cd tipo cartao
	 */
	public void setCdTipoCartao(int cdTipoCartao) {
		this.cdTipoCartao = cdTipoCartao;
	}
	
	/**
	 * Get: dtLimiteEnquadramentoConvenioContaSalario.
	 *
	 * @return dtLimiteEnquadramentoConvenioContaSalario
	 */
	public String getDtLimiteEnquadramentoConvenioContaSalario() {
		return dtLimiteEnquadramentoConvenioContaSalario;
	}
	
	/**
	 * Set: dtLimiteEnquadramentoConvenioContaSalario.
	 *
	 * @param dtLimiteEnquadramentoConvenioContaSalario the dt limite enquadramento convenio conta salario
	 */
	public void setDtLimiteEnquadramentoConvenioContaSalario(
			String dtLimiteEnquadramentoConvenioContaSalario) {
		this.dtLimiteEnquadramentoConvenioContaSalario = dtLimiteEnquadramentoConvenioContaSalario;
	}
	
	/**
	 * Get: emiteCartaoAntecipadoContaSalario.
	 *
	 * @return emiteCartaoAntecipadoContaSalario
	 */
	public int getEmiteCartaoAntecipadoContaSalario() {
		return emiteCartaoAntecipadoContaSalario;
	}
	
	/**
	 * Set: emiteCartaoAntecipadoContaSalario.
	 *
	 * @param emiteCartaoAntecipadoContaSalario the emite cartao antecipado conta salario
	 */
	public void setEmiteCartaoAntecipadoContaSalario(
			int emiteCartaoAntecipadoContaSalario) {
		this.emiteCartaoAntecipadoContaSalario = emiteCartaoAntecipadoContaSalario;
	}
	
	/**
	 * Get: qtdeLimiteSolicitacaoCartaoContaSalario.
	 *
	 * @return qtdeLimiteSolicitacaoCartaoContaSalario
	 */
	public int getQtdeLimiteSolicitacaoCartaoContaSalario() {
		return qtdeLimiteSolicitacaoCartaoContaSalario;
	}
	
	/**
	 * Set: qtdeLimiteSolicitacaoCartaoContaSalario.
	 *
	 * @param qtdeLimiteSolicitacaoCartaoContaSalario the qtde limite solicitacao cartao conta salario
	 */
	public void setQtdeLimiteSolicitacaoCartaoContaSalario(
			int qtdeLimiteSolicitacaoCartaoContaSalario) {
		this.qtdeLimiteSolicitacaoCartaoContaSalario = qtdeLimiteSolicitacaoCartaoContaSalario;
	}
	
	/**
	 * Get: dsEmiteCartaoAntecipadoContaSalario.
	 *
	 * @return dsEmiteCartaoAntecipadoContaSalario
	 */
	public String getDsEmiteCartaoAntecipadoContaSalario() {
		return dsEmiteCartaoAntecipadoContaSalario;
	}
	
	/**
	 * Set: dsEmiteCartaoAntecipadoContaSalario.
	 *
	 * @param dsEmiteCartaoAntecipadoContaSalario the ds emite cartao antecipado conta salario
	 */
	public void setDsEmiteCartaoAntecipadoContaSalario(
			String dsEmiteCartaoAntecipadoContaSalario) {
		this.dsEmiteCartaoAntecipadoContaSalario = dsEmiteCartaoAntecipadoContaSalario;
	}
	
	/**
	 * Get: dsTipoCartao.
	 *
	 * @return dsTipoCartao
	 */
	public String getDsTipoCartao() {
		return dsTipoCartao;
	}
	
	/**
	 * Set: dsTipoCartao.
	 *
	 * @param dsTipoCartao the ds tipo cartao
	 */
	public void setDsTipoCartao(String dsTipoCartao) {
		this.dsTipoCartao = dsTipoCartao;
	}
	
	/**
	 * Get: cdAgendamentoDebitoVeiculoRastreamento.
	 *
	 * @return cdAgendamentoDebitoVeiculoRastreamento
	 */
	public int getCdAgendamentoDebitoVeiculoRastreamento() {
		return cdAgendamentoDebitoVeiculoRastreamento;
	}
	
	/**
	 * Set: cdAgendamentoDebitoVeiculoRastreamento.
	 *
	 * @param cdAgendamentoDebitoVeiculoRastreamento the cd agendamento debito veiculo rastreamento
	 */
	public void setCdAgendamentoDebitoVeiculoRastreamento(
			int cdAgendamentoDebitoVeiculoRastreamento) {
		this.cdAgendamentoDebitoVeiculoRastreamento = cdAgendamentoDebitoVeiculoRastreamento;
	}
	
	/**
	 * Get: cdIndiceEconomicoReajuste.
	 *
	 * @return cdIndiceEconomicoReajuste
	 */
	public java.math.BigDecimal getCdIndiceEconomicoReajuste() {
		return cdIndiceEconomicoReajuste;
	}
	
	/**
	 * Set: cdIndiceEconomicoReajuste.
	 *
	 * @param cdIndiceEconomicoReajuste the cd indice economico reajuste
	 */
	public void setCdIndiceEconomicoReajuste(java.math.BigDecimal cdIndiceEconomicoReajuste) {
		this.cdIndiceEconomicoReajuste = cdIndiceEconomicoReajuste;
	}
	
	/**
	 * Get: cdPeriodicidadeCobrancaTarifa.
	 *
	 * @return cdPeriodicidadeCobrancaTarifa
	 */
	public int getCdPeriodicidadeCobrancaTarifa() {
		return cdPeriodicidadeCobrancaTarifa;
	}
	
	/**
	 * Set: cdPeriodicidadeCobrancaTarifa.
	 *
	 * @param cdPeriodicidadeCobrancaTarifa the cd periodicidade cobranca tarifa
	 */
	public void setCdPeriodicidadeCobrancaTarifa(int cdPeriodicidadeCobrancaTarifa) {
		this.cdPeriodicidadeCobrancaTarifa = cdPeriodicidadeCobrancaTarifa;
	}
	
	/**
	 * Get: cdPeriodicidadeRastreamentoDebitoVeiculo.
	 *
	 * @return cdPeriodicidadeRastreamentoDebitoVeiculo
	 */
	public int getCdPeriodicidadeRastreamentoDebitoVeiculo() {
		return cdPeriodicidadeRastreamentoDebitoVeiculo;
	}
	
	/**
	 * Set: cdPeriodicidadeRastreamentoDebitoVeiculo.
	 *
	 * @param cdPeriodicidadeRastreamentoDebitoVeiculo the cd periodicidade rastreamento debito veiculo
	 */
	public void setCdPeriodicidadeRastreamentoDebitoVeiculo(
			int cdPeriodicidadeRastreamentoDebitoVeiculo) {
		this.cdPeriodicidadeRastreamentoDebitoVeiculo = cdPeriodicidadeRastreamentoDebitoVeiculo;
	}
	
	/**
	 * Get: cdTipoAlimentacaoPagamentos.
	 *
	 * @return cdTipoAlimentacaoPagamentos
	 */
	public int getCdTipoAlimentacaoPagamentos() {
		return cdTipoAlimentacaoPagamentos;
	}
	
	/**
	 * Set: cdTipoAlimentacaoPagamentos.
	 *
	 * @param cdTipoAlimentacaoPagamentosS the cd tipo alimentacao pagamentos
	 */
	public void setCdTipoAlimentacaoPagamentos(int cdTipoAlimentacaoPagamentosS) {
		this.cdTipoAlimentacaoPagamentos = cdTipoAlimentacaoPagamentosS;
	}
	
	/**
	 * Get: cdTipoAutorizacaoPagamento.
	 *
	 * @return cdTipoAutorizacaoPagamento
	 */
	public int getCdTipoAutorizacaoPagamento() {
		return cdTipoAutorizacaoPagamento;
	}
	
	/**
	 * Set: cdTipoAutorizacaoPagamento.
	 *
	 * @param cdTipoAutorizacaoPagamento the cd tipo autorizacao pagamento
	 */
	public void setCdTipoAutorizacaoPagamento(int cdTipoAutorizacaoPagamento) {
		this.cdTipoAutorizacaoPagamento = cdTipoAutorizacaoPagamento;
	}
	
	/**
	 * Get: cdUtilizacaoRastreamentoDebitoVeiculo.
	 *
	 * @return cdUtilizacaoRastreamentoDebitoVeiculo
	 */
	public int getCdUtilizacaoRastreamentoDebitoVeiculo() {
		return cdUtilizacaoRastreamentoDebitoVeiculo;
	}
	
	/**
	 * Set: cdUtilizacaoRastreamentoDebitoVeiculo.
	 *
	 * @param cdUtilizacaoRastreamentoDebitoVeiculo the cd utilizacao rastreamento debito veiculo
	 */
	public void setCdUtilizacaoRastreamentoDebitoVeiculo(
			int cdUtilizacaoRastreamentoDebitoVeiculo) {
		this.cdUtilizacaoRastreamentoDebitoVeiculo = cdUtilizacaoRastreamentoDebitoVeiculo;
	}
	
	/**
	 * Get: dsAgendamentoDebitoVeiculoRastreamento.
	 *
	 * @return dsAgendamentoDebitoVeiculoRastreamento
	 */
	public String getDsAgendamentoDebitoVeiculoRastreamento() {
		return dsAgendamentoDebitoVeiculoRastreamento;
	}
	
	/**
	 * Set: dsAgendamentoDebitoVeiculoRastreamento.
	 *
	 * @param dsAgendamentoDebitoVeiculoRastreamento the ds agendamento debito veiculo rastreamento
	 */
	public void setDsAgendamentoDebitoVeiculoRastreamento(
			String dsAgendamentoDebitoVeiculoRastreamento) {
		this.dsAgendamentoDebitoVeiculoRastreamento = dsAgendamentoDebitoVeiculoRastreamento;
	}
	
	/**
	 * Get: dsPeriodicidadeCobrancaTarifa.
	 *
	 * @return dsPeriodicidadeCobrancaTarifa
	 */
	public String getDsPeriodicidadeCobrancaTarifa() {
		return dsPeriodicidadeCobrancaTarifa;
	}
	
	/**
	 * Set: dsPeriodicidadeCobrancaTarifa.
	 *
	 * @param dsPeriodicidadeCobrancaTarifa the ds periodicidade cobranca tarifa
	 */
	public void setDsPeriodicidadeCobrancaTarifa(
			String dsPeriodicidadeCobrancaTarifa) {
		this.dsPeriodicidadeCobrancaTarifa = dsPeriodicidadeCobrancaTarifa;
	}
	
	/**
	 * Get: dsPeriodicidadeRastreamentoDebitoVeiculo.
	 *
	 * @return dsPeriodicidadeRastreamentoDebitoVeiculo
	 */
	public String getDsPeriodicidadeRastreamentoDebitoVeiculo() {
		return dsPeriodicidadeRastreamentoDebitoVeiculo;
	}
	
	/**
	 * Set: dsPeriodicidadeRastreamentoDebitoVeiculo.
	 *
	 * @param dsPeriodicidadeRastreamentoDebitoVeiculo the ds periodicidade rastreamento debito veiculo
	 */
	public void setDsPeriodicidadeRastreamentoDebitoVeiculo(
			String dsPeriodicidadeRastreamentoDebitoVeiculo) {
		this.dsPeriodicidadeRastreamentoDebitoVeiculo = dsPeriodicidadeRastreamentoDebitoVeiculo;
	}
	
	/**
	 * Get: dsTipoAutorizacaoPagamento.
	 *
	 * @return dsTipoAutorizacaoPagamento
	 */
	public String getDsTipoAutorizacaoPagamento() {
		return dsTipoAutorizacaoPagamento;
	}
	
	/**
	 * Set: dsTipoAutorizacaoPagamento.
	 *
	 * @param dsTipoAutorizacaoPagamento the ds tipo autorizacao pagamento
	 */
	public void setDsTipoAutorizacaoPagamento(String dsTipoAutorizacaoPagamento) {
		this.dsTipoAutorizacaoPagamento = dsTipoAutorizacaoPagamento;
	}
	
	/**
	 * Get: dsUtilizacaoRastreamentoDebitoVeiculo.
	 *
	 * @return dsUtilizacaoRastreamentoDebitoVeiculo
	 */
	public String getDsUtilizacaoRastreamentoDebitoVeiculo() {
		return dsUtilizacaoRastreamentoDebitoVeiculo;
	}
	
	/**
	 * Set: dsUtilizacaoRastreamentoDebitoVeiculo.
	 *
	 * @param dsUtilizacaoRastreamentoDebitoVeiculo the ds utilizacao rastreamento debito veiculo
	 */
	public void setDsUtilizacaoRastreamentoDebitoVeiculo(
			String dsUtilizacaoRastreamentoDebitoVeiculo) {
		this.dsUtilizacaoRastreamentoDebitoVeiculo = dsUtilizacaoRastreamentoDebitoVeiculo;
	}
	
	/**
	 * Get: diaFechamentoApuracaoTarifaMensal.
	 *
	 * @return diaFechamentoApuracaoTarifaMensal
	 */
	public int getDiaFechamentoApuracaoTarifaMensal() {
		return diaFechamentoApuracaoTarifaMensal;
	}
	
	/**
	 * Set: diaFechamentoApuracaoTarifaMensal.
	 *
	 * @param dtFechamentoApuracaoTarifaMensal the dia fechamento apuracao tarifa mensal
	 */
	public void setDiaFechamentoApuracaoTarifaMensal(
			int dtFechamentoApuracaoTarifaMensal) {
		this.diaFechamentoApuracaoTarifaMensal = dtFechamentoApuracaoTarifaMensal;
	}
	
	/**
	 * Get: qtdeDiasCobrancaTarifaAposApuracao.
	 *
	 * @return qtdeDiasCobrancaTarifaAposApuracao
	 */
	public int getQtdeDiasCobrancaTarifaAposApuracao() {
		return qtdeDiasCobrancaTarifaAposApuracao;
	}
	
	/**
	 * Set: qtdeDiasCobrancaTarifaAposApuracao.
	 *
	 * @param qtdeDiasCobrancaTarifaAposApuracao the qtde dias cobranca tarifa apos apuracao
	 */
	public void setQtdeDiasCobrancaTarifaAposApuracao(
			int qtdeDiasCobrancaTarifaAposApuracao) {
		this.qtdeDiasCobrancaTarifaAposApuracao = qtdeDiasCobrancaTarifaAposApuracao;
	}
	
	/**
	 * Get: qtdeMesesReajusteAutomaticoTarifa.
	 *
	 * @return qtdeMesesReajusteAutomaticoTarifa
	 */
	public int getQtdeMesesReajusteAutomaticoTarifa() {
		return qtdeMesesReajusteAutomaticoTarifa;
	}
	
	/**
	 * Set: qtdeMesesReajusteAutomaticoTarifa.
	 *
	 * @param qtdeMesesReajusteAutomaticoTarifa the qtde meses reajuste automatico tarifa
	 */
	public void setQtdeMesesReajusteAutomaticoTarifa(
			int qtdeMesesReajusteAutomaticoTarifa) {
		this.qtdeMesesReajusteAutomaticoTarifa = qtdeMesesReajusteAutomaticoTarifa;
	}
	
	/**
	 * Get: cdTipoReajusteTarifa.
	 *
	 * @return cdTipoReajusteTarifa
	 */
	public int getCdTipoReajusteTarifa() {
		return cdTipoReajusteTarifa;
	}
	
	/**
	 * Set: cdTipoReajusteTarifa.
	 *
	 * @param cdTipoReajusteTarifa the cd tipo reajuste tarifa
	 */
	public void setCdTipoReajusteTarifa(int cdTipoReajusteTarifa) {
		this.cdTipoReajusteTarifa = cdTipoReajusteTarifa;
	}
	
	/**
	 * Get: dsTipoReajusteTarifa.
	 *
	 * @return dsTipoReajusteTarifa
	 */
	public String getDsTipoReajusteTarifa() {
		return dsTipoReajusteTarifa;
	}
	
	/**
	 * Set: dsTipoReajusteTarifa.
	 *
	 * @param dsTipoReajusteTarifa the ds tipo reajuste tarifa
	 */
	public void setDsTipoReajusteTarifa(String dsTipoReajusteTarifa) {
		this.dsTipoReajusteTarifa = dsTipoReajusteTarifa;
	}
	
	/**
	 * Get: cdFormaControleExpiracaoCredito.
	 *
	 * @return cdFormaControleExpiracaoCredito
	 */
	public int getCdFormaControleExpiracaoCredito() {
		return cdFormaControleExpiracaoCredito;
	}
	
	/**
	 * Set: cdFormaControleExpiracaoCredito.
	 *
	 * @param cdFormaControleExpiracaoCredito the cd forma controle expiracao credito
	 */
	public void setCdFormaControleExpiracaoCredito(
			int cdFormaControleExpiracaoCredito) {
		this.cdFormaControleExpiracaoCredito = cdFormaControleExpiracaoCredito;
	}
	
	/**
	 * Get: cdFormaManutencaoCadastroProcurador.
	 *
	 * @return cdFormaManutencaoCadastroProcurador
	 */
	public int getCdFormaManutencaoCadastroProcurador() {
		return cdFormaManutencaoCadastroProcurador;
	}
	
	/**
	 * Set: cdFormaManutencaoCadastroProcurador.
	 *
	 * @param cdFormaManutencaoCadastroProcurador the cd forma manutencao cadastro procurador
	 */
	public void setCdFormaManutencaoCadastroProcurador(
			int cdFormaManutencaoCadastroProcurador) {
		this.cdFormaManutencaoCadastroProcurador = cdFormaManutencaoCadastroProcurador;
	}
	
	/**
	 * Get: cdInformaAgenciaContaCredito.
	 *
	 * @return cdInformaAgenciaContaCredito
	 */
	public int getCdInformaAgenciaContaCredito() {
		return cdInformaAgenciaContaCredito;
	}
	
	/**
	 * Set: cdInformaAgenciaContaCredito.
	 *
	 * @param cdInformaAgenciaContaCredito the cd informa agencia conta credito
	 */
	public void setCdInformaAgenciaContaCredito(int cdInformaAgenciaContaCredito) {
		this.cdInformaAgenciaContaCredito = cdInformaAgenciaContaCredito;
	}
	
	/**
	 * Get: cdMantemCadastroProcuradores.
	 *
	 * @return cdMantemCadastroProcuradores
	 */
	public int getCdMantemCadastroProcuradores() {
		return cdMantemCadastroProcuradores;
	}
	
	/**
	 * Set: cdMantemCadastroProcuradores.
	 *
	 * @param cdMantemCadastroProcuradores the cd mantem cadastro procuradores
	 */
	public void setCdMantemCadastroProcuradores(int cdMantemCadastroProcuradores) {
		this.cdMantemCadastroProcuradores = cdMantemCadastroProcuradores;
	}
	
	/**
	 * Get: cdPeriodicidadeManutencaoCadastroProcurador.
	 *
	 * @return cdPeriodicidadeManutencaoCadastroProcurador
	 */
	public int getCdPeriodicidadeManutencaoCadastroProcurador() {
		return cdPeriodicidadeManutencaoCadastroProcurador;
	}
	
	/**
	 * Set: cdPeriodicidadeManutencaoCadastroProcurador.
	 *
	 * @param cdPeriodicidadeManutencaoCadastroProcurador the cd periodicidade manutencao cadastro procurador
	 */
	public void setCdPeriodicidadeManutencaoCadastroProcurador(
			int cdPeriodicidadeManutencaoCadastroProcurador) {
		this.cdPeriodicidadeManutencaoCadastroProcurador = cdPeriodicidadeManutencaoCadastroProcurador;
	}
	
	/**
	 * Get: cdPossuiExpiracaoCredito.
	 *
	 * @return cdPossuiExpiracaoCredito
	 */
	public int getCdPossuiExpiracaoCredito() {
		return cdPossuiExpiracaoCredito;
	}
	
	/**
	 * Set: cdPossuiExpiracaoCredito.
	 *
	 * @param cdPossuiExpiracaoCredito the cd possui expiracao credito
	 */
	public void setCdPossuiExpiracaoCredito(int cdPossuiExpiracaoCredito) {
		this.cdPossuiExpiracaoCredito = cdPossuiExpiracaoCredito;
	}
	
	/**
	 * Get: cdTipoIdentificacaoBeneficio.
	 *
	 * @return cdTipoIdentificacaoBeneficio
	 */
	public int getCdTipoIdentificacaoBeneficio() {
		return cdTipoIdentificacaoBeneficio;
	}
	
	/**
	 * Set: cdTipoIdentificacaoBeneficio.
	 *
	 * @param cdTipoIdentificacaoBeneficio the cd tipo identificacao beneficio
	 */
	public void setCdTipoIdentificacaoBeneficio(int cdTipoIdentificacaoBeneficio) {
		this.cdTipoIdentificacaoBeneficio = cdTipoIdentificacaoBeneficio;
	}
	
	/**
	 * Get: cdTipoPrestacaoContaCreditoPago.
	 *
	 * @return cdTipoPrestacaoContaCreditoPago
	 */
	public int getCdTipoPrestacaoContaCreditoPago() {
		return cdTipoPrestacaoContaCreditoPago;
	}
	
	/**
	 * Set: cdTipoPrestacaoContaCreditoPago.
	 *
	 * @param cdTipoPrestacaoContaCreditoPago the cd tipo prestacao conta credito pago
	 */
	public void setCdTipoPrestacaoContaCreditoPago(
			int cdTipoPrestacaoContaCreditoPago) {
		this.cdTipoPrestacaoContaCreditoPago = cdTipoPrestacaoContaCreditoPago;
	}
	
	/**
	 * Get: cdTratamentoFeriadoFimVigencia.
	 *
	 * @return cdTratamentoFeriadoFimVigencia
	 */
	public int getCdTratamentoFeriadoFimVigencia() {
		return cdTratamentoFeriadoFimVigencia;
	}
	
	/**
	 * Set: cdTratamentoFeriadoFimVigencia.
	 *
	 * @param cdTratamentoFeriadoFimVigencia the cd tratamento feriado fim vigencia
	 */
	public void setCdTratamentoFeriadoFimVigencia(int cdTratamentoFeriadoFimVigencia) {
		this.cdTratamentoFeriadoFimVigencia = cdTratamentoFeriadoFimVigencia;
	}
	
	/**
	 * Get: cdUtilizaCadastroOrgaosPagadores.
	 *
	 * @return cdUtilizaCadastroOrgaosPagadores
	 */
	public int getCdUtilizaCadastroOrgaosPagadores() {
		return cdUtilizaCadastroOrgaosPagadores;
	}
	
	/**
	 * Set: cdUtilizaCadastroOrgaosPagadores.
	 *
	 * @param cdUtilizaCadastroOrgaosPagadores the cd utiliza cadastro orgaos pagadores
	 */
	public void setCdUtilizaCadastroOrgaosPagadores(
			int cdUtilizaCadastroOrgaosPagadores) {
		this.cdUtilizaCadastroOrgaosPagadores = cdUtilizaCadastroOrgaosPagadores;
	}
	
	/**
	 * Get: dsFormaControleExpiracaoCredito.
	 *
	 * @return dsFormaControleExpiracaoCredito
	 */
	public String getDsFormaControleExpiracaoCredito() {
		return dsFormaControleExpiracaoCredito;
	}
	
	/**
	 * Set: dsFormaControleExpiracaoCredito.
	 *
	 * @param dsFormaControleExpiracaoCredito the ds forma controle expiracao credito
	 */
	public void setDsFormaControleExpiracaoCredito(
			String dsFormaControleExpiracaoCredito) {
		this.dsFormaControleExpiracaoCredito = dsFormaControleExpiracaoCredito;
	}
	
	/**
	 * Get: dsFormaManutencaoCadastroProcurador.
	 *
	 * @return dsFormaManutencaoCadastroProcurador
	 */
	public String getDsFormaManutencaoCadastroProcurador() {
		return dsFormaManutencaoCadastroProcurador;
	}
	
	/**
	 * Set: dsFormaManutencaoCadastroProcurador.
	 *
	 * @param dsFormaManutencaoCadastroProcurador the ds forma manutencao cadastro procurador
	 */
	public void setDsFormaManutencaoCadastroProcurador(
			String dsFormaManutencaoCadastroProcurador) {
		this.dsFormaManutencaoCadastroProcurador = dsFormaManutencaoCadastroProcurador;
	}
	
	/**
	 * Get: dsInformaAgenciaContaCredito.
	 *
	 * @return dsInformaAgenciaContaCredito
	 */
	public String getDsInformaAgenciaContaCredito() {
		return dsInformaAgenciaContaCredito;
	}
	
	/**
	 * Set: dsInformaAgenciaContaCredito.
	 *
	 * @param dsInformaAgenciaContaCredito the ds informa agencia conta credito
	 */
	public void setDsInformaAgenciaContaCredito(String dsInformaAgenciaContaCredito) {
		this.dsInformaAgenciaContaCredito = dsInformaAgenciaContaCredito;
	}
	
	/**
	 * Get: dsMantemCadastroProcuradores.
	 *
	 * @return dsMantemCadastroProcuradores
	 */
	public String getDsMantemCadastroProcuradores() {
		return dsMantemCadastroProcuradores;
	}
	
	/**
	 * Set: dsMantemCadastroProcuradores.
	 *
	 * @param dsMantemCadastroProcuradores the ds mantem cadastro procuradores
	 */
	public void setDsMantemCadastroProcuradores(String dsMantemCadastroProcuradores) {
		this.dsMantemCadastroProcuradores = dsMantemCadastroProcuradores;
	}
	
	/**
	 * Get: dsPeriodicidadeManutencaoCadastroProcurador.
	 *
	 * @return dsPeriodicidadeManutencaoCadastroProcurador
	 */
	public String getDsPeriodicidadeManutencaoCadastroProcurador() {
		return dsPeriodicidadeManutencaoCadastroProcurador;
	}
	
	/**
	 * Set: dsPeriodicidadeManutencaoCadastroProcurador.
	 *
	 * @param dsPeriodicidadeManutencaoCadastroProcurador the ds periodicidade manutencao cadastro procurador
	 */
	public void setDsPeriodicidadeManutencaoCadastroProcurador(
			String dsPeriodicidadeManutencaoCadastroProcurador) {
		this.dsPeriodicidadeManutencaoCadastroProcurador = dsPeriodicidadeManutencaoCadastroProcurador;
	}
	
	/**
	 * Get: dsPossuiExpiracaoCredito.
	 *
	 * @return dsPossuiExpiracaoCredito
	 */
	public String getDsPossuiExpiracaoCredito() {
		return dsPossuiExpiracaoCredito;
	}
	
	/**
	 * Set: dsPossuiExpiracaoCredito.
	 *
	 * @param dsPossuiExpiracaoCredito the ds possui expiracao credito
	 */
	public void setDsPossuiExpiracaoCredito(String dsPossuiExpiracaoCredito) {
		this.dsPossuiExpiracaoCredito = dsPossuiExpiracaoCredito;
	}
	
	/**
	 * Get: dsTipoIdentificacaoBeneficio.
	 *
	 * @return dsTipoIdentificacaoBeneficio
	 */
	public String getDsTipoIdentificacaoBeneficio() {
		return dsTipoIdentificacaoBeneficio;
	}
	
	/**
	 * Set: dsTipoIdentificacaoBeneficio.
	 *
	 * @param dsTipoIdentificacaoBeneficio the ds tipo identificacao beneficio
	 */
	public void setDsTipoIdentificacaoBeneficio(String dsTipoIdentificacaoBeneficio) {
		this.dsTipoIdentificacaoBeneficio = dsTipoIdentificacaoBeneficio;
	}
	
	/**
	 * Get: dsTipoPrestacaoContaCreditoPago.
	 *
	 * @return dsTipoPrestacaoContaCreditoPago
	 */
	public String getDsTipoPrestacaoContaCreditoPago() {
		return dsTipoPrestacaoContaCreditoPago;
	}
	
	/**
	 * Set: dsTipoPrestacaoContaCreditoPago.
	 *
	 * @param dsTipoPrestacaoContaCreditoPago the ds tipo prestacao conta credito pago
	 */
	public void setDsTipoPrestacaoContaCreditoPago(
			String dsTipoPrestacaoContaCreditoPago) {
		this.dsTipoPrestacaoContaCreditoPago = dsTipoPrestacaoContaCreditoPago;
	}
	
	/**
	 * Get: dsTratamentoFeriadoFimVigencia.
	 *
	 * @return dsTratamentoFeriadoFimVigencia
	 */
	public String getDsTratamentoFeriadoFimVigencia() {
		return dsTratamentoFeriadoFimVigencia;
	}
	
	/**
	 * Set: dsTratamentoFeriadoFimVigencia.
	 *
	 * @param dsTratamentoFeriadoFimVigencia the ds tratamento feriado fim vigencia
	 */
	public void setDsTratamentoFeriadoFimVigencia(
			String dsTratamentoFeriadoFimVigencia) {
		this.dsTratamentoFeriadoFimVigencia = dsTratamentoFeriadoFimVigencia;
	}
	
	/**
	 * Get: dsUtilizaCadastroOrgaosPagadores.
	 *
	 * @return dsUtilizaCadastroOrgaosPagadores
	 */
	public String getDsUtilizaCadastroOrgaosPagadores() {
		return dsUtilizaCadastroOrgaosPagadores;
	}
	
	/**
	 * Set: dsUtilizaCadastroOrgaosPagadores.
	 *
	 * @param dsUtilizaCadastroOrgaosPagadores the ds utiliza cadastro orgaos pagadores
	 */
	public void setDsUtilizaCadastroOrgaosPagadores(
			String dsUtilizaCadastroOrgaosPagadores) {
		this.dsUtilizaCadastroOrgaosPagadores = dsUtilizaCadastroOrgaosPagadores;
	}
	
	/**
	 * Get: qtdeDiasExpiracaoCredito.
	 *
	 * @return qtdeDiasExpiracaoCredito
	 */
	public int getQtdeDiasExpiracaoCredito() {
		return qtdeDiasExpiracaoCredito;
	}
	
	/**
	 * Set: qtdeDiasExpiracaoCredito.
	 *
	 * @param qtdeDiasExpiracaoCredito the qtde dias expiracao credito
	 */
	public void setQtdeDiasExpiracaoCredito(int qtdeDiasExpiracaoCredito) {
		this.qtdeDiasExpiracaoCredito = qtdeDiasExpiracaoCredito;
	}
	
	/**
	 * Get: cdConsistenciaCpfCnpjFavorecido.
	 *
	 * @return cdConsistenciaCpfCnpjFavorecido
	 */
	public int getCdConsistenciaCpfCnpjFavorecido() {
		return cdConsistenciaCpfCnpjFavorecido;
	}
	
	/**
	 * Set: cdConsistenciaCpfCnpjFavorecido.
	 *
	 * @param cdConsistenciaCpfCnpjFavorecido the cd consistencia cpf cnpj favorecido
	 */
	public void setCdConsistenciaCpfCnpjFavorecido(
			int cdConsistenciaCpfCnpjFavorecido) {
		this.cdConsistenciaCpfCnpjFavorecido = cdConsistenciaCpfCnpjFavorecido;
	}
	
	/**
	 * Get: cdFormaManutencaoCadastro.
	 *
	 * @return cdFormaManutencaoCadastro
	 */
	public int getCdFormaManutencaoCadastro() {
		return cdFormaManutencaoCadastro;
	}
	
	/**
	 * Set: cdFormaManutencaoCadastro.
	 *
	 * @param cdFormaManutencaoCadastro the cd forma manutencao cadastro
	 */
	public void setCdFormaManutencaoCadastro(int cdFormaManutencaoCadastro) {
		this.cdFormaManutencaoCadastro = cdFormaManutencaoCadastro;
	}
	
	/**
	 * Get: dsConsistenciaCpfCnpjFavorecido.
	 *
	 * @return dsConsistenciaCpfCnpjFavorecido
	 */
	public String getDsConsistenciaCpfCnpjFavorecido() {
		return dsConsistenciaCpfCnpjFavorecido;
	}
	
	/**
	 * Set: dsConsistenciaCpfCnpjFavorecido.
	 *
	 * @param dsConsistenciaCpfCnpjFavorecido the ds consistencia cpf cnpj favorecido
	 */
	public void setDsConsistenciaCpfCnpjFavorecido(
			String dsConsistenciaCpfCnpjFavorecido) {
		this.dsConsistenciaCpfCnpjFavorecido = dsConsistenciaCpfCnpjFavorecido;
	}
	
	/**
	 * Get: dsFormaManutencaoCadastro.
	 *
	 * @return dsFormaManutencaoCadastro
	 */
	public String getDsFormaManutencaoCadastro() {
		return dsFormaManutencaoCadastro;
	}
	
	/**
	 * Set: dsFormaManutencaoCadastro.
	 *
	 * @param dsFormaManutencaoCadastro the ds forma manutencao cadastro
	 */
	public void setDsFormaManutencaoCadastro(String dsFormaManutencaoCadastro) {
		this.dsFormaManutencaoCadastro = dsFormaManutencaoCadastro;
	}
	
	/**
	 * Get: qtdeDiasInativacaoFavorecido.
	 *
	 * @return qtdeDiasInativacaoFavorecido
	 */
	public int getQtdeDiasInativacaoFavorecido() {
		return qtdeDiasInativacaoFavorecido;
	}
	
	/**
	 * Set: qtdeDiasInativacaoFavorecido.
	 *
	 * @param qtdeDiasInativacaoFavorecido the qtde dias inativacao favorecido
	 */
	public void setQtdeDiasInativacaoFavorecido(int qtdeDiasInativacaoFavorecido) {
		this.qtdeDiasInativacaoFavorecido = qtdeDiasInativacaoFavorecido;
	}
	
	/**
	 * Get: cdAgruparCorrespondencia.
	 *
	 * @return cdAgruparCorrespondencia
	 */
	public int getCdAgruparCorrespondencia() {
		return cdAgruparCorrespondencia;
	}
	
	/**
	 * Set: cdAgruparCorrespondencia.
	 *
	 * @param cdAgruparCorrespondencia the cd agrupar correspondencia
	 */
	public void setCdAgruparCorrespondencia(int cdAgruparCorrespondencia) {
		this.cdAgruparCorrespondencia = cdAgruparCorrespondencia;
	}
	
	/**
	 * Get: cdDemonstraInformacoesAreaReservada.
	 *
	 * @return cdDemonstraInformacoesAreaReservada
	 */
	public int getCdDemonstraInformacoesAreaReservada() {
		return cdDemonstraInformacoesAreaReservada;
	}
	
	/**
	 * Set: cdDemonstraInformacoesAreaReservada.
	 *
	 * @param cdDemonstraInformacoesAreaReservada the cd demonstra informacoes area reservada
	 */
	public void setCdDemonstraInformacoesAreaReservada(
			int cdDemonstraInformacoesAreaReservada) {
		this.cdDemonstraInformacoesAreaReservada = cdDemonstraInformacoesAreaReservada;
	}
	
	/**
	 * Get: cdDestino.
	 *
	 * @return cdDestino
	 */
	public int getCdDestino() {
		return cdDestino;
	}
	
	/**
	 * Set: cdDestino.
	 *
	 * @param cdDestino the cd destino
	 */
	public void setCdDestino(int cdDestino) {
		this.cdDestino = cdDestino;
	}
	
	/**
	 * Get: cdPeriodicidade.
	 *
	 * @return cdPeriodicidade
	 */
	public int getCdPeriodicidade() {
		return cdPeriodicidade;
	}
	
	/**
	 * Set: cdPeriodicidade.
	 *
	 * @param cdPeriodicidade the cd periodicidade
	 */
	public void setCdPeriodicidade(int cdPeriodicidade) {
		this.cdPeriodicidade = cdPeriodicidade;
	}
	
	/**
	 * Get: cdPermiteCorrespondenciaAberta.
	 *
	 * @return cdPermiteCorrespondenciaAberta
	 */
	public int getCdPermiteCorrespondenciaAberta() {
		return cdPermiteCorrespondenciaAberta;
	}
	
	/**
	 * Set: cdPermiteCorrespondenciaAberta.
	 *
	 * @param cdPermiteCorrespondenciaAberta the cd permite correspondencia aberta
	 */
	public void setCdPermiteCorrespondenciaAberta(int cdPermiteCorrespondenciaAberta) {
		this.cdPermiteCorrespondenciaAberta = cdPermiteCorrespondenciaAberta;
	}
	
	/**
	 * Get: qtdeVias.
	 *
	 * @return qtdeVias
	 */
	public int getQtdeVias() {
		return qtdeVias;
	}
	
	/**
	 * Set: qtdeVias.
	 *
	 * @param qtdeVias the qtde vias
	 */
	public void setQtdeVias(int qtdeVias) {
		this.qtdeVias = qtdeVias;
	}
	
	/**
	 * Get: cdCobrarTarifasFuncionarios.
	 *
	 * @return cdCobrarTarifasFuncionarios
	 */
	public int getCdCobrarTarifasFuncionarios() {
		return cdCobrarTarifasFuncionarios;
	}
	
	/**
	 * Set: cdCobrarTarifasFuncionarios.
	 *
	 * @param cdCobrarTarifasFuncionarios the cd cobrar tarifas funcionarios
	 */
	public void setCdCobrarTarifasFuncionarios(int cdCobrarTarifasFuncionarios) {
		this.cdCobrarTarifasFuncionarios = cdCobrarTarifasFuncionarios;
	}
	
	/**
	 * Get: cdFechamentoApuracao.
	 *
	 * @return cdFechamentoApuracao
	 */
	public int getCdFechamentoApuracao() {
		return cdFechamentoApuracao;
	}
	
	/**
	 * Set: cdFechamentoApuracao.
	 *
	 * @param cdFechamentoApuracao the cd fechamento apuracao
	 */
	public void setCdFechamentoApuracao(int cdFechamentoApuracao) {
		this.cdFechamentoApuracao = cdFechamentoApuracao;
	}
	
	/**
	 * Get: cdFormularioParaImpressao.
	 *
	 * @return cdFormularioParaImpressao
	 */
	public int getCdFormularioParaImpressao() {
		return cdFormularioParaImpressao;
	}
	
	/**
	 * Set: cdFormularioParaImpressao.
	 *
	 * @param cdFormularioParaImpressao the cd formulario para impressao
	 */
	public void setCdFormularioParaImpressao(int cdFormularioParaImpressao) {
		this.cdFormularioParaImpressao = cdFormularioParaImpressao;
	}
	
	/**
	 * Get: cdFrasesPreCadastratadas.
	 *
	 * @return cdFrasesPreCadastratadas
	 */
	public int getCdFrasesPreCadastratadas() {
		return cdFrasesPreCadastratadas;
	}
	
	/**
	 * Set: cdFrasesPreCadastratadas.
	 *
	 * @param cdFrasesPreCadastratadas the cd frases pre cadastratadas
	 */
	public void setCdFrasesPreCadastratadas(int cdFrasesPreCadastratadas) {
		this.cdFrasesPreCadastratadas = cdFrasesPreCadastratadas;
	}
	
	/**
	 * Get: cdIndiceReajuste.
	 *
	 * @return cdIndiceReajuste
	 */
	public int getCdIndiceReajuste() {
		return cdIndiceReajuste;
	}
	
	/**
	 * Set: cdIndiceReajuste.
	 *
	 * @param cdIndiceReajuste the cd indice reajuste
	 */
	public void setCdIndiceReajuste(int cdIndiceReajuste) {
		this.cdIndiceReajuste = cdIndiceReajuste;
	}
	
	/**
	 * Get: cdMidiaDisponivelCorrentista.
	 *
	 * @return cdMidiaDisponivelCorrentista
	 */
	public int getCdMidiaDisponivelCorrentista() {
		return cdMidiaDisponivelCorrentista;
	}
	
	/**
	 * Set: cdMidiaDisponivelCorrentista.
	 *
	 * @param cdMidiaDisponivelCorrentista the cd midia disponivel correntista
	 */
	public void setCdMidiaDisponivelCorrentista(int cdMidiaDisponivelCorrentista) {
		this.cdMidiaDisponivelCorrentista = cdMidiaDisponivelCorrentista;
	}
	
	/**
	 * Get: cdMidiaDisponivelNaoCorrentista.
	 *
	 * @return cdMidiaDisponivelNaoCorrentista
	 */
	public int getCdMidiaDisponivelNaoCorrentista() {
		return cdMidiaDisponivelNaoCorrentista;
	}
	
	/**
	 * Set: cdMidiaDisponivelNaoCorrentista.
	 *
	 * @param cdMidiaDisponivelNaoCorrentista the cd midia disponivel nao correntista
	 */
	public void setCdMidiaDisponivelNaoCorrentista(
			int cdMidiaDisponivelNaoCorrentista) {
		this.cdMidiaDisponivelNaoCorrentista = cdMidiaDisponivelNaoCorrentista;
	}
	
	/**
	 * Get: cdPeriodicidadeCobrancaTarifaEmissaoComprovantes.
	 *
	 * @return cdPeriodicidadeCobrancaTarifaEmissaoComprovantes
	 */
	public int getCdPeriodicidadeCobrancaTarifaEmissaoComprovantes() {
		return cdPeriodicidadeCobrancaTarifaEmissaoComprovantes;
	}
	
	/**
	 * Set: cdPeriodicidadeCobrancaTarifaEmissaoComprovantes.
	 *
	 * @param cdPeriodicidadeCobrancaTarifaEmissaoComprovantes the cd periodicidade cobranca tarifa emissao comprovantes
	 */
	public void setCdPeriodicidadeCobrancaTarifaEmissaoComprovantes(
			int cdPeriodicidadeCobrancaTarifaEmissaoComprovantes) {
		this.cdPeriodicidadeCobrancaTarifaEmissaoComprovantes = cdPeriodicidadeCobrancaTarifaEmissaoComprovantes;
	}
	
	/**
	 * Get: cdTipoEmissao.
	 *
	 * @return cdTipoEmissao
	 */
	public int getCdTipoEmissao() {
		return cdTipoEmissao;
	}
	
	/**
	 * Set: cdTipoEmissao.
	 *
	 * @param cdTipoEmissao the cd tipo emissao
	 */
	public void setCdTipoEmissao(int cdTipoEmissao) {
		this.cdTipoEmissao = cdTipoEmissao;
	}
	
	/**
	 * Get: cdTipoEnvioCorrespondencia.
	 *
	 * @return cdTipoEnvioCorrespondencia
	 */
	public int getCdTipoEnvioCorrespondencia() {
		return cdTipoEnvioCorrespondencia;
	}
	
	/**
	 * Set: cdTipoEnvioCorrespondencia.
	 *
	 * @param cdTipoEnvioCorrespondencia the cd tipo envio correspondencia
	 */
	public void setCdTipoEnvioCorrespondencia(int cdTipoEnvioCorrespondencia) {
		this.cdTipoEnvioCorrespondencia = cdTipoEnvioCorrespondencia;
	}
	
	/**
	 * Get: cdTipoReajuste.
	 *
	 * @return cdTipoReajuste
	 */
	public int getCdTipoReajuste() {
		return cdTipoReajuste;
	}
	
	/**
	 * Set: cdTipoReajuste.
	 *
	 * @param cdTipoReajuste the cd tipo reajuste
	 */
	public void setCdTipoReajuste(int cdTipoReajuste) {
		this.cdTipoReajuste = cdTipoReajuste;
	}
	
	/**
	 * Get: cdTipoRejeicao.
	 *
	 * @return cdTipoRejeicao
	 */
	public int getCdTipoRejeicao() {
		return cdTipoRejeicao;
	}
	
	/**
	 * Set: cdTipoRejeicao.
	 *
	 * @param cdTipoRejeicao the cd tipo rejeicao
	 */
	public void setCdTipoRejeicao(int cdTipoRejeicao) {
		this.cdTipoRejeicao = cdTipoRejeicao;
	}
	
	/**
	 * Get: dsPeriodicidadeCobrancaTarifaEmissaoComprovantes.
	 *
	 * @return dsPeriodicidadeCobrancaTarifaEmissaoComprovantes
	 */
	public String getDsPeriodicidadeCobrancaTarifaEmissaoComprovantes() {
		return dsPeriodicidadeCobrancaTarifaEmissaoComprovantes;
	}
	
	/**
	 * Set: dsPeriodicidadeCobrancaTarifaEmissaoComprovantes.
	 *
	 * @param dsPeriodicidadeCobrancaTarifaEmissaoComprovantes the ds periodicidade cobranca tarifa emissao comprovantes
	 */
	public void setDsPeriodicidadeCobrancaTarifaEmissaoComprovantes(
			String dsPeriodicidadeCobrancaTarifaEmissaoComprovantes) {
		this.dsPeriodicidadeCobrancaTarifaEmissaoComprovantes = dsPeriodicidadeCobrancaTarifaEmissaoComprovantes;
	}
	
	/**
	 * Get: percentualReajuste.
	 *
	 * @return percentualReajuste
	 */
	public BigDecimal getPercentualReajuste() {
		return percentualReajuste;
	}
	
	/**
	 * Set: percentualReajuste.
	 *
	 * @param percentualReajuste the percentual reajuste
	 */
	public void setPercentualReajuste(BigDecimal percentualReajuste) {
		this.percentualReajuste = percentualReajuste;
	}

	/**
	 * Get: porcentagemBonificacaoTarifa.
	 *
	 * @return porcentagemBonificacaoTarifa
	 */
	public BigDecimal getPorcentagemBonificacaoTarifa() {
		return porcentagemBonificacaoTarifa;
	}
	
	/**
	 * Set: porcentagemBonificacaoTarifa.
	 *
	 * @param porcentagemBonificacaoTarifa the porcentagem bonificacao tarifa
	 */
	public void setPorcentagemBonificacaoTarifa(
			BigDecimal porcentagemBonificacaoTarifa) {
		this.porcentagemBonificacaoTarifa = porcentagemBonificacaoTarifa;
	}
	
	/**
	 * Get: qtdeDiasCobrancaAposApuracao.
	 *
	 * @return qtdeDiasCobrancaAposApuracao
	 */
	public int getQtdeDiasCobrancaAposApuracao() {
		return qtdeDiasCobrancaAposApuracao;
	}
	
	/**
	 * Set: qtdeDiasCobrancaAposApuracao.
	 *
	 * @param qtdeDiasCobrancaAposApuracao the qtde dias cobranca apos apuracao
	 */
	public void setQtdeDiasCobrancaAposApuracao(int qtdeDiasCobrancaAposApuracao) {
		this.qtdeDiasCobrancaAposApuracao = qtdeDiasCobrancaAposApuracao;
	}
	
	/**
	 * Get: qtdeLinhas.
	 *
	 * @return qtdeLinhas
	 */
	public int getQtdeLinhas() {
		return qtdeLinhas;
	}
	
	/**
	 * Set: qtdeLinhas.
	 *
	 * @param qtdeLinhas the qtde linhas
	 */
	public void setQtdeLinhas(int qtdeLinhas) {
		this.qtdeLinhas = qtdeLinhas;
	}
	
	/**
	 * Get: qtdeMesesEmissao.
	 *
	 * @return qtdeMesesEmissao
	 */
	public int getQtdeMesesEmissao() {
		return qtdeMesesEmissao;
	}
	
	/**
	 * Set: qtdeMesesEmissao.
	 *
	 * @param qtdeMesesEmissao the qtde meses emissao
	 */
	public void setQtdeMesesEmissao(int qtdeMesesEmissao) {
		this.qtdeMesesEmissao = qtdeMesesEmissao;
	}
	
	/**
	 * Get: qtdeMesesReajusteAutomatico.
	 *
	 * @return qtdeMesesReajusteAutomatico
	 */
	public int getQtdeMesesReajusteAutomatico() {
		return qtdeMesesReajusteAutomatico;
	}
	
	/**
	 * Set: qtdeMesesReajusteAutomatico.
	 *
	 * @param qtdeMesesReajusteAutomatico the qtde meses reajuste automatico
	 */
	public void setQtdeMesesReajusteAutomatico(int qtdeMesesReajusteAutomatico) {
		this.qtdeMesesReajusteAutomatico = qtdeMesesReajusteAutomatico;
	}
	
	/**
	 * Get: qtdeViasEmissaoComprovantes.
	 *
	 * @return qtdeViasEmissaoComprovantes
	 */
	public int getQtdeViasEmissaoComprovantes() {
		return qtdeViasEmissaoComprovantes;
	}
	
	/**
	 * Set: qtdeViasEmissaoComprovantes.
	 *
	 * @param qtdeViasEmissaoComprovantes the qtde vias emissao comprovantes
	 */
	public void setQtdeViasEmissaoComprovantes(int qtdeViasEmissaoComprovantes) {
		this.qtdeViasEmissaoComprovantes = qtdeViasEmissaoComprovantes;
	}
	
	/**
	 * Get: qtdeViasPagas.
	 *
	 * @return qtdeViasPagas
	 */
	public int getQtdeViasPagas() {
		return qtdeViasPagas;
	}
	
	/**
	 * Set: qtdeViasPagas.
	 *
	 * @param qtdeViasPagas the qtde vias pagas
	 */
	public void setQtdeViasPagas(int qtdeViasPagas) {
		this.qtdeViasPagas = qtdeViasPagas;
	}
	
	/**
	 * Get: dsPeriodicidade.
	 *
	 * @return dsPeriodicidade
	 */
	public String getDsPeriodicidade() {
		return dsPeriodicidade;
	}
	
	/**
	 * Set: dsPeriodicidade.
	 *
	 * @param dsPeriodicidade the ds periodicidade
	 */
	public void setDsPeriodicidade(String dsPeriodicidade) {
		this.dsPeriodicidade = dsPeriodicidade;
	}
	
	/**
	 * Get: cdBaseDadosUtilizada.
	 *
	 * @return cdBaseDadosUtilizada
	 */
	public int getCdBaseDadosUtilizada() {
		return cdBaseDadosUtilizada;
	}
	
	/**
	 * Set: cdBaseDadosUtilizada.
	 *
	 * @param cdBaseDadosUtilizada the cd base dados utilizada
	 */
	public void setCdBaseDadosUtilizada(int cdBaseDadosUtilizada) {
		this.cdBaseDadosUtilizada = cdBaseDadosUtilizada;
	}
	
	/**
	 * Get: cdCondicaoEnquadramento.
	 *
	 * @return cdCondicaoEnquadramento
	 */
	public int getCdCondicaoEnquadramento() {
		return cdCondicaoEnquadramento;
	}
	
	/**
	 * Set: cdCondicaoEnquadramento.
	 *
	 * @param cdCondicaoEnquadramento the cd condicao enquadramento
	 */
	public void setCdCondicaoEnquadramento(int cdCondicaoEnquadramento) {
		this.cdCondicaoEnquadramento = cdCondicaoEnquadramento;
	}
	
	/**
	 * Get: cdCriterioCompostoEnquadramento.
	 *
	 * @return cdCriterioCompostoEnquadramento
	 */
	public int getCdCriterioCompostoEnquadramento() {
		return cdCriterioCompostoEnquadramento;
	}
	
	/**
	 * Set: cdCriterioCompostoEnquadramento.
	 *
	 * @param cdCriterioCompostoEnquadramento the cd criterio composto enquadramento
	 */
	public void setCdCriterioCompostoEnquadramento(
			int cdCriterioCompostoEnquadramento) {
		this.cdCriterioCompostoEnquadramento = cdCriterioCompostoEnquadramento;
	}
	
	/**
	 * Get: cdCriterioPrincipalEnquadramento.
	 *
	 * @return cdCriterioPrincipalEnquadramento
	 */
	public int getCdCriterioPrincipalEnquadramento() {
		return cdCriterioPrincipalEnquadramento;
	}
	
	/**
	 * Set: cdCriterioPrincipalEnquadramento.
	 *
	 * @param cdCriterioPrincipalEnquadramento the cd criterio principal enquadramento
	 */
	public void setCdCriterioPrincipalEnquadramento(
			int cdCriterioPrincipalEnquadramento) {
		this.cdCriterioPrincipalEnquadramento = cdCriterioPrincipalEnquadramento;
	}
	
	/**
	 * Get: cdEmiteMensagemRecadastramentoMidiaOnLine.
	 *
	 * @return cdEmiteMensagemRecadastramentoMidiaOnLine
	 */
	public int getCdEmiteMensagemRecadastramentoMidiaOnLine() {
		return cdEmiteMensagemRecadastramentoMidiaOnLine;
	}
	
	/**
	 * Set: cdEmiteMensagemRecadastramentoMidiaOnLine.
	 *
	 * @param cdEmiteMensagemRecadastramentoMidiaOnLine the cd emite mensagem recadastramento midia on line
	 */
	public void setCdEmiteMensagemRecadastramentoMidiaOnLine(
			int cdEmiteMensagemRecadastramentoMidiaOnLine) {
		this.cdEmiteMensagemRecadastramentoMidiaOnLine = cdEmiteMensagemRecadastramentoMidiaOnLine;
	}
	
	/**
	 * Get: cdIndiceEconomicoReajusteTarifa.
	 *
	 * @return cdIndiceEconomicoReajusteTarifa
	 */
	public int getCdIndiceEconomicoReajusteTarifa() {
		return cdIndiceEconomicoReajusteTarifa;
	}
	
	/**
	 * Set: cdIndiceEconomicoReajusteTarifa.
	 *
	 * @param cdIndiceEconomicoReajusteTarifa the cd indice economico reajuste tarifa
	 */
	public void setCdIndiceEconomicoReajusteTarifa(
			int cdIndiceEconomicoReajusteTarifa) {
		this.cdIndiceEconomicoReajusteTarifa = cdIndiceEconomicoReajusteTarifa;
	}
	
	/**
	 * Get: cdMidiaMensagemRecastramentoOnLine.
	 *
	 * @return cdMidiaMensagemRecastramentoOnLine
	 */
	public int getCdMidiaMensagemRecastramentoOnLine() {
		return cdMidiaMensagemRecastramentoOnLine;
	}
	
	/**
	 * Set: cdMidiaMensagemRecastramentoOnLine.
	 *
	 * @param cdMidiaMensagemRecastramentoOnLine the cd midia mensagem recastramento on line
	 */
	public void setCdMidiaMensagemRecastramentoOnLine(
			int cdMidiaMensagemRecastramentoOnLine) {
		this.cdMidiaMensagemRecastramentoOnLine = cdMidiaMensagemRecastramentoOnLine;
	}
	
	/**
	 * Get: cdPeriodicidadeEnvioRemessa.
	 *
	 * @return cdPeriodicidadeEnvioRemessa
	 */
	public int getCdPeriodicidadeEnvioRemessa() {
		return cdPeriodicidadeEnvioRemessa;
	}
	
	/**
	 * Set: cdPeriodicidadeEnvioRemessa.
	 *
	 * @param cdPeriodicidadeEnvioRemessa the cd periodicidade envio remessa
	 */
	public void setCdPeriodicidadeEnvioRemessa(int cdPeriodicidadeEnvioRemessa) {
		this.cdPeriodicidadeEnvioRemessa = cdPeriodicidadeEnvioRemessa;
	}
	
	/**
	 * Get: cdPeriodicidadeReajusteTarifa.
	 *
	 * @return cdPeriodicidadeReajusteTarifa
	 */
	public int getCdPeriodicidadeReajusteTarifa() {
		return cdPeriodicidadeReajusteTarifa;
	}
	
	/**
	 * Set: cdPeriodicidadeReajusteTarifa.
	 *
	 * @param cdPeriodicidadeReajusteTarifa the cd periodicidade reajuste tarifa
	 */
	public void setCdPeriodicidadeReajusteTarifa(int cdPeriodicidadeReajusteTarifa) {
		this.cdPeriodicidadeReajusteTarifa = cdPeriodicidadeReajusteTarifa;
	}
	
	/**
	 * Get: cdPermiteAcertosDados.
	 *
	 * @return cdPermiteAcertosDados
	 */
	public int getCdPermiteAcertosDados() {
		return cdPermiteAcertosDados;
	}
	
	/**
	 * Set: cdPermiteAcertosDados.
	 *
	 * @param cdPermiteAcertosDados the cd permite acertos dados
	 */
	public void setCdPermiteAcertosDados(int cdPermiteAcertosDados) {
		this.cdPermiteAcertosDados = cdPermiteAcertosDados;
	}
	
	/**
	 * Get: cdPermiteAnteciparRecadastramento.
	 *
	 * @return cdPermiteAnteciparRecadastramento
	 */
	public int getCdPermiteAnteciparRecadastramento() {
		return cdPermiteAnteciparRecadastramento;
	}
	
	/**
	 * Set: cdPermiteAnteciparRecadastramento.
	 *
	 * @param cdPermiteAnteciparRecadastramento the cd permite antecipar recadastramento
	 */
	public void setCdPermiteAnteciparRecadastramento(
			int cdPermiteAnteciparRecadastramento) {
		this.cdPermiteAnteciparRecadastramento = cdPermiteAnteciparRecadastramento;
	}
	
	/**
	 * Get: cdPermiteEnvioRemessaManutencao.
	 *
	 * @return cdPermiteEnvioRemessaManutencao
	 */
	public int getCdPermiteEnvioRemessaManutencao() {
		return cdPermiteEnvioRemessaManutencao;
	}
	
	/**
	 * Set: cdPermiteEnvioRemessaManutencao.
	 *
	 * @param cdPermiteEnvioRemessaManutencao the cd permite envio remessa manutencao
	 */
	public void setCdPermiteEnvioRemessaManutencao(
			int cdPermiteEnvioRemessaManutencao) {
		this.cdPermiteEnvioRemessaManutencao = cdPermiteEnvioRemessaManutencao;
	}
	
	/**
	 * Get: cdTipoCargaBaseDadosUtilizada.
	 *
	 * @return cdTipoCargaBaseDadosUtilizada
	 */
	public int getCdTipoCargaBaseDadosUtilizada() {
		return cdTipoCargaBaseDadosUtilizada;
	}
	
	/**
	 * Set: cdTipoCargaBaseDadosUtilizada.
	 *
	 * @param cdTipoCargaBaseDadosUtilizada the cd tipo carga base dados utilizada
	 */
	public void setCdTipoCargaBaseDadosUtilizada(int cdTipoCargaBaseDadosUtilizada) {
		this.cdTipoCargaBaseDadosUtilizada = cdTipoCargaBaseDadosUtilizada;
	}
	
	/**
	 * Get: cdTipoConsistenciaIdentificador.
	 *
	 * @return cdTipoConsistenciaIdentificador
	 */
	public int getCdTipoConsistenciaIdentificador() {
		return cdTipoConsistenciaIdentificador;
	}
	
	/**
	 * Set: cdTipoConsistenciaIdentificador.
	 *
	 * @param cdTipoConsistenciaIdentificador the cd tipo consistencia identificador
	 */
	public void setCdTipoConsistenciaIdentificador(
			int cdTipoConsistenciaIdentificador) {
		this.cdTipoConsistenciaIdentificador = cdTipoConsistenciaIdentificador;
	}
	
	/**
	 * Get: cdTipoIdentificacaoFuncionario.
	 *
	 * @return cdTipoIdentificacaoFuncionario
	 */
	public int getCdTipoIdentificacaoFuncionario() {
		return cdTipoIdentificacaoFuncionario;
	}
	
	/**
	 * Set: cdTipoIdentificacaoFuncionario.
	 *
	 * @param cdTipoIdentificacaoFuncionario the cd tipo identificacao funcionario
	 */
	public void setCdTipoIdentificacaoFuncionario(int cdTipoIdentificacaoFuncionario) {
		this.cdTipoIdentificacaoFuncionario = cdTipoIdentificacaoFuncionario;
	}
	
	/**
	 * Get: dsCondicaoEnquadramento.
	 *
	 * @return dsCondicaoEnquadramento
	 */
	public String getDsCondicaoEnquadramento() {
		return dsCondicaoEnquadramento;
	}
	
	/**
	 * Set: dsCondicaoEnquadramento.
	 *
	 * @param dsCondicaoEnquadramento the ds condicao enquadramento
	 */
	public void setDsCondicaoEnquadramento(String dsCondicaoEnquadramento) {
		this.dsCondicaoEnquadramento = dsCondicaoEnquadramento;
	}
	
	/**
	 * Get: dsCriterioCompostoEnquadramento.
	 *
	 * @return dsCriterioCompostoEnquadramento
	 */
	public String getDsCriterioCompostoEnquadramento() {
		return dsCriterioCompostoEnquadramento;
	}
	
	/**
	 * Set: dsCriterioCompostoEnquadramento.
	 *
	 * @param dsCriterioCompostoEnquadramento the ds criterio composto enquadramento
	 */
	public void setDsCriterioCompostoEnquadramento(
			String dsCriterioCompostoEnquadramento) {
		this.dsCriterioCompostoEnquadramento = dsCriterioCompostoEnquadramento;
	}
	
	/**
	 * Get: dsCriterioPrincipalEnquadramento.
	 *
	 * @return dsCriterioPrincipalEnquadramento
	 */
	public String getDsCriterioPrincipalEnquadramento() {
		return dsCriterioPrincipalEnquadramento;
	}
	
	/**
	 * Set: dsCriterioPrincipalEnquadramento.
	 *
	 * @param dsCriterioPrincipalEnquadramento the ds criterio principal enquadramento
	 */
	public void setDsCriterioPrincipalEnquadramento(
			String dsCriterioPrincipalEnquadramento) {
		this.dsCriterioPrincipalEnquadramento = dsCriterioPrincipalEnquadramento;
	}
	
	/**
	 * Get: dsPeriodicidadeEnvioRemessa.
	 *
	 * @return dsPeriodicidadeEnvioRemessa
	 */
	public String getDsPeriodicidadeEnvioRemessa() {
		return dsPeriodicidadeEnvioRemessa;
	}
	
	/**
	 * Set: dsPeriodicidadeEnvioRemessa.
	 *
	 * @param dsPeriodicidadeEnvioRemessa the ds periodicidade envio remessa
	 */
	public void setDsPeriodicidadeEnvioRemessa(String dsPeriodicidadeEnvioRemessa) {
		this.dsPeriodicidadeEnvioRemessa = dsPeriodicidadeEnvioRemessa;
	}
	
	/**
	 * Get: dsPeriodicidadeReajusteTarifa.
	 *
	 * @return dsPeriodicidadeReajusteTarifa
	 */
	public String getDsPeriodicidadeReajusteTarifa() {
		return dsPeriodicidadeReajusteTarifa;
	}
	
	/**
	 * Set: dsPeriodicidadeReajusteTarifa.
	 *
	 * @param dsPeriodicidadeReajusteTarifa the ds periodicidade reajuste tarifa
	 */
	public void setDsPeriodicidadeReajusteTarifa(
			String dsPeriodicidadeReajusteTarifa) {
		this.dsPeriodicidadeReajusteTarifa = dsPeriodicidadeReajusteTarifa;
	}
	
	/**
	 * Get: dsTipoConsistenciaIdentificador.
	 *
	 * @return dsTipoConsistenciaIdentificador
	 */
	public String getDsTipoConsistenciaIdentificador() {
		return dsTipoConsistenciaIdentificador;
	}
	
	/**
	 * Set: dsTipoConsistenciaIdentificador.
	 *
	 * @param dsTipoConsistenciaIdentificador the ds tipo consistencia identificador
	 */
	public void setDsTipoConsistenciaIdentificador(
			String dsTipoConsistenciaIdentificador) {
		this.dsTipoConsistenciaIdentificador = dsTipoConsistenciaIdentificador;
	}
	
	/**
	 * Get: dsTipoIdentificacaoFuncionario.
	 *
	 * @return dsTipoIdentificacaoFuncionario
	 */
	public String getDsTipoIdentificacaoFuncionario() {
		return dsTipoIdentificacaoFuncionario;
	}
	
	/**
	 * Set: dsTipoIdentificacaoFuncionario.
	 *
	 * @param dsTipoIdentificacaoFuncionario the ds tipo identificacao funcionario
	 */
	public void setDsTipoIdentificacaoFuncionario(
			String dsTipoIdentificacaoFuncionario) {
		this.dsTipoIdentificacaoFuncionario = dsTipoIdentificacaoFuncionario;
	}

	/**
	 * Get: dtFimPeriodoAcertoDados.
	 *
	 * @return dtFimPeriodoAcertoDados
	 */
	public String getDtFimPeriodoAcertoDados() {
		return dtFimPeriodoAcertoDados;
	}
	
	/**
	 * Set: dtFimPeriodoAcertoDados.
	 *
	 * @param dtFimPeriodoAcertoDados the dt fim periodo acerto dados
	 */
	public void setDtFimPeriodoAcertoDados(String dtFimPeriodoAcertoDados) {
		this.dtFimPeriodoAcertoDados = dtFimPeriodoAcertoDados;
	}
	
	/**
	 * Get: dtFimRecadastramento.
	 *
	 * @return dtFimRecadastramento
	 */
	public String getDtFimRecadastramento() {
		return dtFimRecadastramento;
	}
	
	/**
	 * Set: dtFimRecadastramento.
	 *
	 * @param dtFimRecadastramento the dt fim recadastramento
	 */
	public void setDtFimRecadastramento(String dtFimRecadastramento) {
		this.dtFimRecadastramento = dtFimRecadastramento;
	}
	
	/**
	 * Get: dtInicioPeriodoAcertoDados.
	 *
	 * @return dtInicioPeriodoAcertoDados
	 */
	public String getDtInicioPeriodoAcertoDados() {
		return dtInicioPeriodoAcertoDados;
	}
	
	/**
	 * Set: dtInicioPeriodoAcertoDados.
	 *
	 * @param dtInicioPeriodoAcertoDados the dt inicio periodo acerto dados
	 */
	public void setDtInicioPeriodoAcertoDados(String dtInicioPeriodoAcertoDados) {
		this.dtInicioPeriodoAcertoDados = dtInicioPeriodoAcertoDados;
	}
	
	/**
	 * Get: dtInicioRecadastramento.
	 *
	 * @return dtInicioRecadastramento
	 */
	public String getDtInicioRecadastramento() {
		return dtInicioRecadastramento;
	}
	
	/**
	 * Set: dtInicioRecadastramento.
	 *
	 * @param dtInicioRecadastramento the dt inicio recadastramento
	 */
	public void setDtInicioRecadastramento(String dtInicioRecadastramento) {
		this.dtInicioRecadastramento = dtInicioRecadastramento;
	}
	
	/**
	 * Get: dtLimiteInicioVinculoCargaBase.
	 *
	 * @return dtLimiteInicioVinculoCargaBase
	 */
	public String getDtLimiteInicioVinculoCargaBase() {
		return dtLimiteInicioVinculoCargaBase;
	}
	
	/**
	 * Set: dtLimiteInicioVinculoCargaBase.
	 *
	 * @param dtLimiteInicioVinculoCargaBase the dt limite inicio vinculo carga base
	 */
	public void setDtLimiteInicioVinculoCargaBase(
			String dtLimiteInicioVinculoCargaBase) {
		this.dtLimiteInicioVinculoCargaBase = dtLimiteInicioVinculoCargaBase;
	}
	
	/**
	 * Get: qtdeEtapasRecadastramento.
	 *
	 * @return qtdeEtapasRecadastramento
	 */
	public int getQtdeEtapasRecadastramento() {
		return qtdeEtapasRecadastramento;
	}
	
	/**
	 * Set: qtdeEtapasRecadastramento.
	 *
	 * @param qtdeEtapasRecadastramento the qtde etapas recadastramento
	 */
	public void setQtdeEtapasRecadastramento(int qtdeEtapasRecadastramento) {
		this.qtdeEtapasRecadastramento = qtdeEtapasRecadastramento;
	}
	
	/**
	 * Get: qtdeFasesPorEtapa.
	 *
	 * @return qtdeFasesPorEtapa
	 */
	public int getQtdeFasesPorEtapa() {
		return qtdeFasesPorEtapa;
	}
	
	/**
	 * Set: qtdeFasesPorEtapa.
	 *
	 * @param qtdeFasesPorEtapa the qtde fases por etapa
	 */
	public void setQtdeFasesPorEtapa(int qtdeFasesPorEtapa) {
		this.qtdeFasesPorEtapa = qtdeFasesPorEtapa;
	}
	
	/**
	 * Get: qtdeMesesPrazoPorEtapa.
	 *
	 * @return qtdeMesesPrazoPorEtapa
	 */
	public int getQtdeMesesPrazoPorEtapa() {
		return qtdeMesesPrazoPorEtapa;
	}
	
	/**
	 * Set: qtdeMesesPrazoPorEtapa.
	 *
	 * @param qtdeMesesPrazoPorEtapa the qtde meses prazo por etapa
	 */
	public void setQtdeMesesPrazoPorEtapa(int qtdeMesesPrazoPorEtapa) {
		this.qtdeMesesPrazoPorEtapa = qtdeMesesPrazoPorEtapa;
	}
	
	/**
	 * Get: qtdeMesesPrazoPorFase.
	 *
	 * @return qtdeMesesPrazoPorFase
	 */
	public int getQtdeMesesPrazoPorFase() {
		return qtdeMesesPrazoPorFase;
	}
	
	/**
	 * Set: qtdeMesesPrazoPorFase.
	 *
	 * @param qtdeMesesPrazoPorFase the qtde meses prazo por fase
	 */
	public void setQtdeMesesPrazoPorFase(int qtdeMesesPrazoPorFase) {
		this.qtdeMesesPrazoPorFase = qtdeMesesPrazoPorFase;
	}
	
	/**
	 * Get: percentualBonificacaoTarifaPadrao.
	 *
	 * @return percentualBonificacaoTarifaPadrao
	 */
	public BigDecimal getPercentualBonificacaoTarifaPadrao() {
		return percentualBonificacaoTarifaPadrao;
	}
	
	/**
	 * Set: percentualBonificacaoTarifaPadrao.
	 *
	 * @param percentualBonificacaoTarifaPadrao the percentual bonificacao tarifa padrao
	 */
	public void setPercentualBonificacaoTarifaPadrao(
			BigDecimal percentualBonificacaoTarifaPadrao) {
		this.percentualBonificacaoTarifaPadrao = percentualBonificacaoTarifaPadrao;
	}
	
	/**
	 * Get: percentualIndiceEconomicoReajusteTarifa.
	 *
	 * @return percentualIndiceEconomicoReajusteTarifa
	 */
	public BigDecimal getPercentualIndiceEconomicoReajusteTarifa() {
		return percentualIndiceEconomicoReajusteTarifa;
	}
	
	/**
	 * Set: percentualIndiceEconomicoReajusteTarifa.
	 *
	 * @param percentualIndiceEconomicoReajusteTarifa the percentual indice economico reajuste tarifa
	 */
	public void setPercentualIndiceEconomicoReajusteTarifa(
			BigDecimal percentualIndiceEconomicoReajusteTarifa) {
		this.percentualIndiceEconomicoReajusteTarifa = percentualIndiceEconomicoReajusteTarifa;
	}
	
	/**
	 * Get: cdControlePagFavorecido.
	 *
	 * @return cdControlePagFavorecido
	 */
	public int getCdControlePagFavorecido() {
		return cdControlePagFavorecido;
	}
	
	/**
	 * Set: cdControlePagFavorecido.
	 *
	 * @param cdControlePagFavorecido the cd controle pag favorecido
	 */
	public void setCdControlePagFavorecido(int cdControlePagFavorecido) {
		this.cdControlePagFavorecido = cdControlePagFavorecido;
	}
	
	/**
	 * Get: cdEnderecoEnvio.
	 *
	 * @return cdEnderecoEnvio
	 */
	public int getCdEnderecoEnvio() {
		return cdEnderecoEnvio;
	}
	
	/**
	 * Set: cdEnderecoEnvio.
	 *
	 * @param cdEnderecoEnvio the cd endereco envio
	 */
	public void setCdEnderecoEnvio(int cdEnderecoEnvio) {
		this.cdEnderecoEnvio = cdEnderecoEnvio;
	}
	
	/**
	 * Get: cdEnderecoUtilizadoEnvio.
	 *
	 * @return cdEnderecoUtilizadoEnvio
	 */
	public int getCdEnderecoUtilizadoEnvio() {
		return cdEnderecoUtilizadoEnvio;
	}
	
	/**
	 * Set: cdEnderecoUtilizadoEnvio.
	 *
	 * @param cdEnderecoUtilizadoEnvio the cd endereco utilizado envio
	 */
	public void setCdEnderecoUtilizadoEnvio(int cdEnderecoUtilizadoEnvio) {
		this.cdEnderecoUtilizadoEnvio = cdEnderecoUtilizadoEnvio;
	}
	
	/**
	 * Get: cdPeriocidadeEnvio.
	 *
	 * @return cdPeriocidadeEnvio
	 */
	public int getCdPeriocidadeEnvio() {
		return cdPeriocidadeEnvio;
	}
	
	/**
	 * Set: cdPeriocidadeEnvio.
	 *
	 * @param cdPeriocidadeEnvio the cd periocidade envio
	 */
	public void setCdPeriocidadeEnvio(int cdPeriocidadeEnvio) {
		this.cdPeriocidadeEnvio = cdPeriocidadeEnvio;
	}
	
	/**
	 * Get: cdPermitirAgrupamento.
	 *
	 * @return cdPermitirAgrupamento
	 */
	public int getCdPermitirAgrupamento() {
		return cdPermitirAgrupamento;
	}
	
	/**
	 * Set: cdPermitirAgrupamento.
	 *
	 * @param cdPermitirAgrupamento the cd permitir agrupamento
	 */
	public void setCdPermitirAgrupamento(int cdPermitirAgrupamento) {
		this.cdPermitirAgrupamento = cdPermitirAgrupamento;
	}
	
	/**
	 * Get: cdPrioridadeCredito.
	 *
	 * @return cdPrioridadeCredito
	 */
	public int getCdPrioridadeCredito() {
		return cdPrioridadeCredito;
	}
	
	/**
	 * Set: cdPrioridadeCredito.
	 *
	 * @param cdPrioridadeCredito the cd prioridade credito
	 */
	public void setCdPrioridadeCredito(int cdPrioridadeCredito) {
		this.cdPrioridadeCredito = cdPrioridadeCredito;
	}
	
	/**
	 * Get: cdTipoComprovacao.
	 *
	 * @return cdTipoComprovacao
	 */
	public int getCdTipoComprovacao() {
		return cdTipoComprovacao;
	}
	
	/**
	 * Set: cdTipoComprovacao.
	 *
	 * @param cdTipoComprovacao the cd tipo comprovacao
	 */
	public void setCdTipoComprovacao(int cdTipoComprovacao) {
		this.cdTipoComprovacao = cdTipoComprovacao;
	}
	
	/**
	 * Get: cdTipoConsistenciaCpfCnpjProp.
	 *
	 * @return cdTipoConsistenciaCpfCnpjProp
	 */
	public int getCdTipoConsistenciaCpfCnpjProp() {
		return cdTipoConsistenciaCpfCnpjProp;
	}
	
	/**
	 * Set: cdTipoConsistenciaCpfCnpjProp.
	 *
	 * @param cdTipoConsistenciaCpfCnpjProp the cd tipo consistencia cpf cnpj prop
	 */
	public void setCdTipoConsistenciaCpfCnpjProp(int cdTipoConsistenciaCpfCnpjProp) {
		this.cdTipoConsistenciaCpfCnpjProp = cdTipoConsistenciaCpfCnpjProp;
	}
	
	/**
	 * Get: cdTipoConsultaSaldo.
	 *
	 * @return cdTipoConsultaSaldo
	 */
	public int getCdTipoConsultaSaldo() {
		return cdTipoConsultaSaldo;
	}
	
	/**
	 * Set: cdTipoConsultaSaldo.
	 *
	 * @param cdTipoConsultaSaldo the cd tipo consulta saldo
	 */
	public void setCdTipoConsultaSaldo(int cdTipoConsultaSaldo) {
		this.cdTipoConsultaSaldo = cdTipoConsultaSaldo;
	}
	
	/**
	 * Get: cdTipoEfetivacao.
	 *
	 * @return cdTipoEfetivacao
	 */
	public int getCdTipoEfetivacao() {
		return cdTipoEfetivacao;
	}
	
	/**
	 * Set: cdTipoEfetivacao.
	 *
	 * @param cdTipoEfetivacao the cd tipo efetivacao
	 */
	public void setCdTipoEfetivacao(int cdTipoEfetivacao) {
		this.cdTipoEfetivacao = cdTipoEfetivacao;
	}
	
	/**
	 * Get: cdTratamentoFeriado.
	 *
	 * @return cdTratamentoFeriado
	 */
	public int getCdTratamentoFeriado() {
		return cdTratamentoFeriado;
	}
	
	/**
	 * Set: cdTratamentoFeriado.
	 *
	 * @param cdTratamentoFeriado the cd tratamento feriado
	 */
	public void setCdTratamentoFeriado(int cdTratamentoFeriado) {
		this.cdTratamentoFeriado = cdTratamentoFeriado;
	}
	
	/**
	 * Get: cdTratamentoValorDivergente.
	 *
	 * @return cdTratamentoValorDivergente
	 */
	public int getCdTratamentoValorDivergente() {
		return cdTratamentoValorDivergente;
	}
	
	/**
	 * Set: cdTratamentoValorDivergente.
	 *
	 * @param cdTratamentoValorDivergente the cd tratamento valor divergente
	 */
	public void setCdTratamentoValorDivergente(int cdTratamentoValorDivergente) {
		this.cdTratamentoValorDivergente = cdTratamentoValorDivergente;
	}
	
	/**
	 * Get: codUtilizarMsgPersOnline.
	 *
	 * @return codUtilizarMsgPersOnline
	 */
	public int getCodUtilizarMsgPersOnline() {
		return codUtilizarMsgPersOnline;
	}
	
	/**
	 * Set: codUtilizarMsgPersOnline.
	 *
	 * @param codUtilizarMsgPersOnline the cod utilizar msg pers online
	 */
	public void setCodUtilizarMsgPersOnline(int codUtilizarMsgPersOnline) {
		this.codUtilizarMsgPersOnline = codUtilizarMsgPersOnline;
	}
	
	/**
	 * Get: dsControlePagFavorecido.
	 *
	 * @return dsControlePagFavorecido
	 */
	public String getDsControlePagFavorecido() {
		return dsControlePagFavorecido;
	}
	
	/**
	 * Set: dsControlePagFavorecido.
	 *
	 * @param dsControlePagFavorecido the ds controle pag favorecido
	 */
	public void setDsControlePagFavorecido(String dsControlePagFavorecido) {
		this.dsControlePagFavorecido = dsControlePagFavorecido;
	}
	
	/**
	 * Get: dsDestino.
	 *
	 * @return dsDestino
	 */
	public String getDsDestino() {
		return dsDestino;
	}
	
	/**
	 * Set: dsDestino.
	 *
	 * @param dsDestino the ds destino
	 */
	public void setDsDestino(String dsDestino) {
		this.dsDestino = dsDestino;
	}
	
	/**
	 * Get: dsEnderecoEnvio.
	 *
	 * @return dsEnderecoEnvio
	 */
	public String getDsEnderecoEnvio() {
		return dsEnderecoEnvio;
	}
	
	/**
	 * Set: dsEnderecoEnvio.
	 *
	 * @param dsEnderecoEnvio the ds endereco envio
	 */
	public void setDsEnderecoEnvio(String dsEnderecoEnvio) {
		this.dsEnderecoEnvio = dsEnderecoEnvio;
	}
	
	/**
	 * Get: dsEnderecoUtilizadoEnvio.
	 *
	 * @return dsEnderecoUtilizadoEnvio
	 */
	public String getDsEnderecoUtilizadoEnvio() {
		return dsEnderecoUtilizadoEnvio;
	}
	
	/**
	 * Set: dsEnderecoUtilizadoEnvio.
	 *
	 * @param dsEnderecoUtilizadoEnvio the ds endereco utilizado envio
	 */
	public void setDsEnderecoUtilizadoEnvio(String dsEnderecoUtilizadoEnvio) {
		this.dsEnderecoUtilizadoEnvio = dsEnderecoUtilizadoEnvio;
	}
	
	/**
	 * Get: dsIndiceEconomicoReajusteTarifa.
	 *
	 * @return dsIndiceEconomicoReajusteTarifa
	 */
	public String getDsIndiceEconomicoReajusteTarifa() {
		return dsIndiceEconomicoReajusteTarifa;
	}
	
	/**
	 * Set: dsIndiceEconomicoReajusteTarifa.
	 *
	 * @param dsIndiceEconomicoReajusteTarifa the ds indice economico reajuste tarifa
	 */
	public void setDsIndiceEconomicoReajusteTarifa(
			String dsIndiceEconomicoReajusteTarifa) {
		this.dsIndiceEconomicoReajusteTarifa = dsIndiceEconomicoReajusteTarifa;
	}
	
	/**
	 * Get: dsPeriocidadeEnvio.
	 *
	 * @return dsPeriocidadeEnvio
	 */
	public String getDsPeriocidadeEnvio() {
		return dsPeriocidadeEnvio;
	}
	
	/**
	 * Set: dsPeriocidadeEnvio.
	 *
	 * @param dsPeriocidadeEnvio the ds periocidade envio
	 */
	public void setDsPeriocidadeEnvio(String dsPeriocidadeEnvio) {
		this.dsPeriocidadeEnvio = dsPeriocidadeEnvio;
	}
	
	/**
	 * Get: dsPermitirAgrupamento.
	 *
	 * @return dsPermitirAgrupamento
	 */
	public String getDsPermitirAgrupamento() {
		return dsPermitirAgrupamento;
	}
	
	/**
	 * Set: dsPermitirAgrupamento.
	 *
	 * @param dsPermitirAgrupamento the ds permitir agrupamento
	 */
	public void setDsPermitirAgrupamento(String dsPermitirAgrupamento) {
		this.dsPermitirAgrupamento = dsPermitirAgrupamento;
	}
	
	/**
	 * Get: dsPrioridadeCredito.
	 *
	 * @return dsPrioridadeCredito
	 */
	public String getDsPrioridadeCredito() {
		return dsPrioridadeCredito;
	}
	
	/**
	 * Set: dsPrioridadeCredito.
	 *
	 * @param dsPrioridadeCredito the ds prioridade credito
	 */
	public void setDsPrioridadeCredito(String dsPrioridadeCredito) {
		this.dsPrioridadeCredito = dsPrioridadeCredito;
	}
	
	/**
	 * Get: dsTipoComprovacao.
	 *
	 * @return dsTipoComprovacao
	 */
	public String getDsTipoComprovacao() {
		return dsTipoComprovacao;
	}
	
	/**
	 * Set: dsTipoComprovacao.
	 *
	 * @param dsTipoComprovacao the ds tipo comprovacao
	 */
	public void setDsTipoComprovacao(String dsTipoComprovacao) {
		this.dsTipoComprovacao = dsTipoComprovacao;
	}
	
	/**
	 * Get: dsTipoConsistenciaCpfCnpjProp.
	 *
	 * @return dsTipoConsistenciaCpfCnpjProp
	 */
	public String getDsTipoConsistenciaCpfCnpjProp() {
		return dsTipoConsistenciaCpfCnpjProp;
	}
	
	/**
	 * Set: dsTipoConsistenciaCpfCnpjProp.
	 *
	 * @param dsTipoConsistenciaCpfCnpjProp the ds tipo consistencia cpf cnpj prop
	 */
	public void setDsTipoConsistenciaCpfCnpjProp(
			String dsTipoConsistenciaCpfCnpjProp) {
		this.dsTipoConsistenciaCpfCnpjProp = dsTipoConsistenciaCpfCnpjProp;
	}
	
	/**
	 * Get: dsTipoConsultaSaldo.
	 *
	 * @return dsTipoConsultaSaldo
	 */
	public String getDsTipoConsultaSaldo() {
		return dsTipoConsultaSaldo;
	}
	
	/**
	 * Set: dsTipoConsultaSaldo.
	 *
	 * @param dsTipoConsultaSaldo the ds tipo consulta saldo
	 */
	public void setDsTipoConsultaSaldo(String dsTipoConsultaSaldo) {
		this.dsTipoConsultaSaldo = dsTipoConsultaSaldo;
	}
	
	/**
	 * Get: dsTipoEfetivacao.
	 *
	 * @return dsTipoEfetivacao
	 */
	public String getDsTipoEfetivacao() {
		return dsTipoEfetivacao;
	}
	
	/**
	 * Set: dsTipoEfetivacao.
	 *
	 * @param dsTipoEfetivacao the ds tipo efetivacao
	 */
	public void setDsTipoEfetivacao(String dsTipoEfetivacao) {
		this.dsTipoEfetivacao = dsTipoEfetivacao;
	}
	
	/**
	 * Get: dsTratamentoFeriado.
	 *
	 * @return dsTratamentoFeriado
	 */
	public String getDsTratamentoFeriado() {
		return dsTratamentoFeriado;
	}
	
	/**
	 * Set: dsTratamentoFeriado.
	 *
	 * @param dsTratamentoFeriado the ds tratamento feriado
	 */
	public void setDsTratamentoFeriado(String dsTratamentoFeriado) {
		this.dsTratamentoFeriado = dsTratamentoFeriado;
	}
	
	/**
	 * Get: dsTratamentoValorDivergente.
	 *
	 * @return dsTratamentoValorDivergente
	 */
	public String getDsTratamentoValorDivergente() {
		return dsTratamentoValorDivergente;
	}
	
	/**
	 * Set: dsTratamentoValorDivergente.
	 *
	 * @param dsTratamentoValorDivergente the ds tratamento valor divergente
	 */
	public void setDsTratamentoValorDivergente(String dsTratamentoValorDivergente) {
		this.dsTratamentoValorDivergente = dsTratamentoValorDivergente;
	}
	
	/**
	 * Get: dsUtilizarMsgPersOnline.
	 *
	 * @return dsUtilizarMsgPersOnline
	 */
	public String getDsUtilizarMsgPersOnline() {
		return dsUtilizarMsgPersOnline;
	}
	
	/**
	 * Set: dsUtilizarMsgPersOnline.
	 *
	 * @param dsUtilizarMsgPersOnline the ds utilizar msg pers online
	 */
	public void setDsUtilizarMsgPersOnline(String dsUtilizarMsgPersOnline) {
		this.dsUtilizarMsgPersOnline = dsUtilizarMsgPersOnline;
	}
	
	/**
	 * Get: permiteContingenciaPag.
	 *
	 * @return permiteContingenciaPag
	 */
	public int getPermiteContingenciaPag() {
		return permiteContingenciaPag;
	}
	
	/**
	 * Set: permiteContingenciaPag.
	 *
	 * @param permiteContingenciaPag the permite contingencia pag
	 */
	public void setPermiteContingenciaPag(int permiteContingenciaPag) {
		this.permiteContingenciaPag = permiteContingenciaPag;
	}
	
	/**
	 * Get: porcentInconsistenciaLote.
	 *
	 * @return porcentInconsistenciaLote
	 */
	public int getPorcentInconsistenciaLote() {
		return porcentInconsistenciaLote;
	}
	
	/**
	 * Set: porcentInconsistenciaLote.
	 *
	 * @param porcentInconsistenciaLote the porcent inconsistencia lote
	 */
	public void setPorcentInconsistenciaLote(int porcentInconsistenciaLote) {
		this.porcentInconsistenciaLote = porcentInconsistenciaLote;
	}
	
	/**
	 * Get: qntdDiasAntecedeInicio.
	 *
	 * @return qntdDiasAntecedeInicio
	 */
	public int getQntdDiasAntecedeInicio() {
		return qntdDiasAntecedeInicio;
	}
	
	/**
	 * Set: qntdDiasAntecedeInicio.
	 *
	 * @param qntdDiasAntecedeInicio the qntd dias antecede inicio
	 */
	public void setQntdDiasAntecedeInicio(int qntdDiasAntecedeInicio) {
		this.qntdDiasAntecedeInicio = qntdDiasAntecedeInicio;
	}
	
	/**
	 * Get: qntdDiasAvisoVenc.
	 *
	 * @return qntdDiasAvisoVenc
	 */
	public int getQntdDiasAvisoVenc() {
		return qntdDiasAvisoVenc;
	}
	
	/**
	 * Set: qntdDiasAvisoVenc.
	 *
	 * @param qntdDiasAvisoVenc the qntd dias aviso venc
	 */
	public void setQntdDiasAvisoVenc(int qntdDiasAvisoVenc) {
		this.qntdDiasAvisoVenc = qntdDiasAvisoVenc;
	}
	
	/**
	 * Get: qntdDiasRepConsulta.
	 *
	 * @return qntdDiasRepConsulta
	 */
	public int getQntdDiasRepConsulta() {
		return qntdDiasRepConsulta;
	}
	
	/**
	 * Set: qntdDiasRepConsulta.
	 *
	 * @param qntdDiasRepConsulta the qntd dias rep consulta
	 */
	public void setQntdDiasRepConsulta(int qntdDiasRepConsulta) {
		this.qntdDiasRepConsulta = qntdDiasRepConsulta;
	}
	
	/**
	 * Get: qntDiasAntecipacao.
	 *
	 * @return qntDiasAntecipacao
	 */
	public int getQntDiasAntecipacao() {
		return qntDiasAntecipacao;
	}
	
	/**
	 * Set: qntDiasAntecipacao.
	 *
	 * @param qntDiasAntecipacao the qnt dias antecipacao
	 */
	public void setQntDiasAntecipacao(int qntDiasAntecipacao) {
		this.qntDiasAntecipacao = qntDiasAntecipacao;
	}
	
	/**
	 * Get: qntdMaxInconsistenciaLote.
	 *
	 * @return qntdMaxInconsistenciaLote
	 */
	public int getQntdMaxInconsistenciaLote() {
		return qntdMaxInconsistenciaLote;
	}
	
	/**
	 * Set: qntdMaxInconsistenciaLote.
	 *
	 * @param qntdMaxInconsistenciaLote the qntd max inconsistencia lote
	 */
	public void setQntdMaxInconsistenciaLote(int qntdMaxInconsistenciaLote) {
		this.qntdMaxInconsistenciaLote = qntdMaxInconsistenciaLote;
	}
	
	/**
	 * Get: qntdMesesPeriComprovacao.
	 *
	 * @return qntdMesesPeriComprovacao
	 */
	public int getQntdMesesPeriComprovacao() {
		return qntdMesesPeriComprovacao;
	}
	
	/**
	 * Set: qntdMesesPeriComprovacao.
	 *
	 * @param qntdMesesPeriComprovacao the qntd meses peri comprovacao
	 */
	public void setQntdMesesPeriComprovacao(int qntdMesesPeriComprovacao) {
		this.qntdMesesPeriComprovacao = qntdMesesPeriComprovacao;
	}
	
	/**
	 * Get: cdControlePagamentoFavorecido.
	 *
	 * @return cdControlePagamentoFavorecido
	 */
	public int getCdControlePagamentoFavorecido() {
		return cdControlePagamentoFavorecido;
	}
	
	/**
	 * Set: cdControlePagamentoFavorecido.
	 *
	 * @param cdControlePagamentoFavorecido the cd controle pagamento favorecido
	 */
	public void setCdControlePagamentoFavorecido(int cdControlePagamentoFavorecido) {
		this.cdControlePagamentoFavorecido = cdControlePagamentoFavorecido;
	}
	
	/**
	 * Get: cdPermiteConsultaFavorecido.
	 *
	 * @return cdPermiteConsultaFavorecido
	 */
	public int getCdPermiteConsultaFavorecido() {
		return cdPermiteConsultaFavorecido;
	}
	
	/**
	 * Set: cdPermiteConsultaFavorecido.
	 *
	 * @param cdPermiteConsultaFavorecido the cd permite consulta favorecido
	 */
	public void setCdPermiteConsultaFavorecido(int cdPermiteConsultaFavorecido) {
		this.cdPermiteConsultaFavorecido = cdPermiteConsultaFavorecido;
	}
	
	/**
	 * Get: dsControlePagamentoFavorecido.
	 *
	 * @return dsControlePagamentoFavorecido
	 */
	public String getDsControlePagamentoFavorecido() {
		return dsControlePagamentoFavorecido;
	}
	
	/**
	 * Set: dsControlePagamentoFavorecido.
	 *
	 * @param dsControlePagamentoFavorecido the ds controle pagamento favorecido
	 */
	public void setDsControlePagamentoFavorecido(
			String dsControlePagamentoFavorecido) {
		this.dsControlePagamentoFavorecido = dsControlePagamentoFavorecido;
	}
	
	/**
	 * Get: dsPermiteConsultaFavorecido.
	 *
	 * @return dsPermiteConsultaFavorecido
	 */
	public String getDsPermiteConsultaFavorecido() {
		return dsPermiteConsultaFavorecido;
	}
	
	/**
	 * Set: dsPermiteConsultaFavorecido.
	 *
	 * @param dsPermiteConsultaFavorecido the ds permite consulta favorecido
	 */
	public void setDsPermiteConsultaFavorecido(String dsPermiteConsultaFavorecido) {
		this.dsPermiteConsultaFavorecido = dsPermiteConsultaFavorecido;
	}
	
	/**
	 * Get: cdEfetuaConsistencia.
	 *
	 * @return cdEfetuaConsistencia
	 */
	public int getCdEfetuaConsistencia() {
		return cdEfetuaConsistencia;
	}
	
	/**
	 * Set: cdEfetuaConsistencia.
	 *
	 * @param cdEfetuaConsistencia the cd efetua consistencia
	 */
	public void setCdEfetuaConsistencia(int cdEfetuaConsistencia) {
		this.cdEfetuaConsistencia = cdEfetuaConsistencia;
	}
	
	/**
	 * Get: cdPermiteContingenciaPag.
	 *
	 * @return cdPermiteContingenciaPag
	 */
	public int getCdPermiteContingenciaPag() {
		return cdPermiteContingenciaPag;
	}
	
	/**
	 * Set: cdPermiteContingenciaPag.
	 *
	 * @param cdPermiteContingenciaPag the cd permite contingencia pag
	 */
	public void setCdPermiteContingenciaPag(int cdPermiteContingenciaPag) {
		this.cdPermiteContingenciaPag = cdPermiteContingenciaPag;
	}
	
	/**
	 * Get: cdPermiteEstornoPagamento.
	 *
	 * @return cdPermiteEstornoPagamento
	 */
	public int getCdPermiteEstornoPagamento() {
		return cdPermiteEstornoPagamento;
	}
	
	/**
	 * Set: cdPermiteEstornoPagamento.
	 *
	 * @param cdPermiteEstornoPagamento the cd permite estorno pagamento
	 */
	public void setCdPermiteEstornoPagamento(int cdPermiteEstornoPagamento) {
		this.cdPermiteEstornoPagamento = cdPermiteEstornoPagamento;
	}
	
	/**
	 * Get: cdTipoRastreamentoTitulos.
	 *
	 * @return cdTipoRastreamentoTitulos
	 */
	public int getCdTipoRastreamentoTitulos() {
		return cdTipoRastreamentoTitulos;
	}
	
	/**
	 * Set: cdTipoRastreamentoTitulos.
	 *
	 * @param cdTipoRastreamentoTitulos the cd tipo rastreamento titulos
	 */
	public void setCdTipoRastreamentoTitulos(int cdTipoRastreamentoTitulos) {
		this.cdTipoRastreamentoTitulos = cdTipoRastreamentoTitulos;
	}
	
	/**
	 * Get: cdTipoTratamentoContasTransferidas.
	 *
	 * @return cdTipoTratamentoContasTransferidas
	 */
	public int getCdTipoTratamentoContasTransferidas() {
		return cdTipoTratamentoContasTransferidas;
	}
	
	/**
	 * Set: cdTipoTratamentoContasTransferidas.
	 *
	 * @param cdTipoTratamentoContasTransferidas the cd tipo tratamento contas transferidas
	 */
	public void setCdTipoTratamentoContasTransferidas(
			int cdTipoTratamentoContasTransferidas) {
		this.cdTipoTratamentoContasTransferidas = cdTipoTratamentoContasTransferidas;
	}
	
	/**
	 * Get: cdTratamentoFeriadosDtaPag.
	 *
	 * @return cdTratamentoFeriadosDtaPag
	 */
	public int getCdTratamentoFeriadosDtaPag() {
		return cdTratamentoFeriadosDtaPag;
	}
	
	/**
	 * Set: cdTratamentoFeriadosDtaPag.
	 *
	 * @param cdTratamentoFeriadosDtaPag the cd tratamento feriados dta pag
	 */
	public void setCdTratamentoFeriadosDtaPag(int cdTratamentoFeriadosDtaPag) {
		this.cdTratamentoFeriadosDtaPag = cdTratamentoFeriadosDtaPag;
	}
	
	/**
	 * Get: dataInicioBloqPapeleta.
	 *
	 * @return dataInicioBloqPapeleta
	 */
	public String getDataInicioBloqPapeleta() {
		return dataInicioBloqPapeleta;
	}
	
	/**
	 * Set: dataInicioBloqPapeleta.
	 *
	 * @param dataInicioBloqPapeleta the data inicio bloq papeleta
	 */
	public void setDataInicioBloqPapeleta(String dataInicioBloqPapeleta) {
		this.dataInicioBloqPapeleta = dataInicioBloqPapeleta;
	}
	
	/**
	 * Get: dataInicioRastreamento.
	 *
	 * @return dataInicioRastreamento
	 */
	public String getDataInicioRastreamento() {
		return dataInicioRastreamento;
	}
	
	/**
	 * Set: dataInicioRastreamento.
	 *
	 * @param dataInicioRastreamento the data inicio rastreamento
	 */
	public void setDataInicioRastreamento(String dataInicioRastreamento) {
		this.dataInicioRastreamento = dataInicioRastreamento;
	}
	
	/**
	 * Get: dataRegistroTitulo.
	 *
	 * @return dataRegistroTitulo
	 */
	public String getDataRegistroTitulo() {
		return dataRegistroTitulo;
	}
	
	/**
	 * Set: dataRegistroTitulo.
	 *
	 * @param dataRegistroTitulo the data registro titulo
	 */
	public void setDataRegistroTitulo(String dataRegistroTitulo) {
		this.dataRegistroTitulo = dataRegistroTitulo;
	}
	
	/**
	 * Get: dsEfetuaConsistencia.
	 *
	 * @return dsEfetuaConsistencia
	 */
	public String getDsEfetuaConsistencia() {
		return dsEfetuaConsistencia;
	}
	
	/**
	 * Set: dsEfetuaConsistencia.
	 *
	 * @param dsEfetuaConsistencia the ds efetua consistencia
	 */
	public void setDsEfetuaConsistencia(String dsEfetuaConsistencia) {
		this.dsEfetuaConsistencia = dsEfetuaConsistencia;
	}
	
	/**
	 * Get: dsPermiteContingenciaPag.
	 *
	 * @return dsPermiteContingenciaPag
	 */
	public String getDsPermiteContingenciaPag() {
		return dsPermiteContingenciaPag;
	}
	
	/**
	 * Set: dsPermiteContingenciaPag.
	 *
	 * @param dsPermiteContingenciaPag the ds permite contingencia pag
	 */
	public void setDsPermiteContingenciaPag(String dsPermiteContingenciaPag) {
		this.dsPermiteContingenciaPag = dsPermiteContingenciaPag;
	}
	
	/**
	 * Get: dsPermiteEstornoPagamento.
	 *
	 * @return dsPermiteEstornoPagamento
	 */
	public String getDsPermiteEstornoPagamento() {
		return dsPermiteEstornoPagamento;
	}
	
	/**
	 * Set: dsPermiteEstornoPagamento.
	 *
	 * @param dsPermiteEstornoPagamento the ds permite estorno pagamento
	 */
	public void setDsPermiteEstornoPagamento(String dsPermiteEstornoPagamento) {
		this.dsPermiteEstornoPagamento = dsPermiteEstornoPagamento;
	}
	
	/**
	 * Get: dsTipoRastreamentoTitulos.
	 *
	 * @return dsTipoRastreamentoTitulos
	 */
	public String getDsTipoRastreamentoTitulos() {
		return dsTipoRastreamentoTitulos;
	}
	
	/**
	 * Set: dsTipoRastreamentoTitulos.
	 *
	 * @param dsTipoRastreamentoTitulos the ds tipo rastreamento titulos
	 */
	public void setDsTipoRastreamentoTitulos(String dsTipoRastreamentoTitulos) {
		this.dsTipoRastreamentoTitulos = dsTipoRastreamentoTitulos;
	}
	
	/**
	 * Get: dsTipoTratamentoContasTransferidas.
	 *
	 * @return dsTipoTratamentoContasTransferidas
	 */
	public String getDsTipoTratamentoContasTransferidas() {
		return dsTipoTratamentoContasTransferidas;
	}
	
	/**
	 * Set: dsTipoTratamentoContasTransferidas.
	 *
	 * @param dsTipoTratamentoContasTransferidas the ds tipo tratamento contas transferidas
	 */
	public void setDsTipoTratamentoContasTransferidas(
			String dsTipoTratamentoContasTransferidas) {
		this.dsTipoTratamentoContasTransferidas = dsTipoTratamentoContasTransferidas;
	}
	
	/**
	 * Get: dsTratamentoFeriadosDtaPag.
	 *
	 * @return dsTratamentoFeriadosDtaPag
	 */
	public String getDsTratamentoFeriadosDtaPag() {
		return dsTratamentoFeriadosDtaPag;
	}
	
	/**
	 * Set: dsTratamentoFeriadosDtaPag.
	 *
	 * @param dsTratamentoFeriadosDtaPag the ds tratamento feriados dta pag
	 */
	public void setDsTratamentoFeriadosDtaPag(String dsTratamentoFeriadosDtaPag) {
		this.dsTratamentoFeriadosDtaPag = dsTratamentoFeriadosDtaPag;
	}
	
	/**
	 * Get: perInconsistenciaLote.
	 *
	 * @return perInconsistenciaLote
	 */
	public int getPerInconsistenciaLote() {
		return perInconsistenciaLote;
	}
	
	/**
	 * Set: perInconsistenciaLote.
	 *
	 * @param perInconsistenciaLote the per inconsistencia lote
	 */
	public void setPerInconsistenciaLote(int perInconsistenciaLote) {
		this.perInconsistenciaLote = perInconsistenciaLote;
	}
	
	/**
	 * Get: qntdDiasExpiracao.
	 *
	 * @return qntdDiasExpiracao
	 */
	public int getQntdDiasExpiracao() {
		return qntdDiasExpiracao;
	}
	
	/**
	 * Set: qntdDiasExpiracao.
	 *
	 * @param qntdDiasExpiracao the qntd dias expiracao
	 */
	public void setQntdDiasExpiracao(int qntdDiasExpiracao) {
		this.qntdDiasExpiracao = qntdDiasExpiracao;
	}
	
	/**
	 * Get: qntdLimiteDiasPagVencido.
	 *
	 * @return qntdLimiteDiasPagVencido
	 */
	public int getQntdLimiteDiasPagVencido() {
		return qntdLimiteDiasPagVencido;
	}
	
	/**
	 * Set: qntdLimiteDiasPagVencido.
	 *
	 * @param qntdLimiteDiasPagVencido the qntd limite dias pag vencido
	 */
	public void setQntdLimiteDiasPagVencido(int qntdLimiteDiasPagVencido) {
		this.qntdLimiteDiasPagVencido = qntdLimiteDiasPagVencido;
	}
	
	/**
	 * Get: qntdMaxRegInconsistentes.
	 *
	 * @return qntdMaxRegInconsistentes
	 */
	public int getQntdMaxRegInconsistentes() {
		return qntdMaxRegInconsistentes;
	}
	
	/**
	 * Set: qntdMaxRegInconsistentes.
	 *
	 * @param qntdMaxRegInconsistentes the qntd max reg inconsistentes
	 */
	public void setQntdMaxRegInconsistentes(int qntdMaxRegInconsistentes) {
		this.qntdMaxRegInconsistentes = qntdMaxRegInconsistentes;
	}
	
	/**
	 * Get: rdoAgendarTitRastProp.
	 *
	 * @return rdoAgendarTitRastProp
	 */
	public int getRdoAgendarTitRastProp() {
		return rdoAgendarTitRastProp;
	}
	
	/**
	 * Set: rdoAgendarTitRastProp.
	 *
	 * @param rdoAgendarTitRastProp the rdo agendar tit rast prop
	 */
	public void setRdoAgendarTitRastProp(int rdoAgendarTitRastProp) {
		this.rdoAgendarTitRastProp = rdoAgendarTitRastProp;
	}
	
	/**
	 * Get: rdoBloquearEmissaoPap.
	 *
	 * @return rdoBloquearEmissaoPap
	 */
	public int getRdoBloquearEmissaoPap() {
		return rdoBloquearEmissaoPap;
	}
	
	/**
	 * Set: rdoBloquearEmissaoPap.
	 *
	 * @param rdoBloquearEmissaoPap the rdo bloquear emissao pap
	 */
	public void setRdoBloquearEmissaoPap(int rdoBloquearEmissaoPap) {
		this.rdoBloquearEmissaoPap = rdoBloquearEmissaoPap;
	}
	
	/**
	 * Get: rdoCapturarTitDtaRegistro.
	 *
	 * @return rdoCapturarTitDtaRegistro
	 */
	public int getRdoCapturarTitDtaRegistro() {
		return rdoCapturarTitDtaRegistro;
	}
	
	/**
	 * Set: rdoCapturarTitDtaRegistro.
	 *
	 * @param rdoCapturarTitDtaRegistro the rdo capturar tit dta registro
	 */
	public void setRdoCapturarTitDtaRegistro(int rdoCapturarTitDtaRegistro) {
		this.rdoCapturarTitDtaRegistro = rdoCapturarTitDtaRegistro;
	}
	
	/**
	 * Get: rdoGerarLanctoFuturoCred.
	 *
	 * @return rdoGerarLanctoFuturoCred
	 */
	public int getRdoGerarLanctoFuturoCred() {
		return rdoGerarLanctoFuturoCred;
	}
	
	/**
	 * Set: rdoGerarLanctoFuturoCred.
	 *
	 * @param rdoGerarLanctoFuturoCred the rdo gerar lancto futuro cred
	 */
	public void setRdoGerarLanctoFuturoCred(int rdoGerarLanctoFuturoCred) {
		this.rdoGerarLanctoFuturoCred = rdoGerarLanctoFuturoCred;
	}
	
	/**
	 * Get: rdoGerarLanctoFuturoDeb.
	 *
	 * @return rdoGerarLanctoFuturoDeb
	 */
	public int getRdoGerarLanctoFuturoDeb() {
		return rdoGerarLanctoFuturoDeb;
	}
	
	/**
	 * Set: rdoGerarLanctoFuturoDeb.
	 *
	 * @param rdoGerarLanctoFuturoDeb the rdo gerar lancto futuro deb
	 */
	public void setRdoGerarLanctoFuturoDeb(int rdoGerarLanctoFuturoDeb) {
		this.rdoGerarLanctoFuturoDeb = rdoGerarLanctoFuturoDeb;
	}
	
	/**
	 * Get: rdoGerarLanctoProgramado.
	 *
	 * @return rdoGerarLanctoProgramado
	 */
	public int getRdoGerarLanctoProgramado() {
		return rdoGerarLanctoProgramado;
	}
	
	/**
	 * Set: rdoGerarLanctoProgramado.
	 *
	 * @param rdoGerarLanctoProgramado the rdo gerar lancto programado
	 */
	public void setRdoGerarLanctoProgramado(int rdoGerarLanctoProgramado) {
		this.rdoGerarLanctoProgramado = rdoGerarLanctoProgramado;
	}
	
	/**
	 * Get: rdoOcorrenciaDebito.
	 *
	 * @return rdoOcorrenciaDebito
	 */
	public int getRdoOcorrenciaDebito() {
		return rdoOcorrenciaDebito;
	}
	
	/**
	 * Set: rdoOcorrenciaDebito.
	 *
	 * @param rdoOcorrenciaDebito the rdo ocorrencia debito
	 */
	public void setRdoOcorrenciaDebito(int rdoOcorrenciaDebito) {
		this.rdoOcorrenciaDebito = rdoOcorrenciaDebito;
	}
	
	/**
	 * Get: rdoPermiteDebitoOnline.
	 *
	 * @return rdoPermiteDebitoOnline
	 */
	public int getRdoPermiteDebitoOnline() {
		return rdoPermiteDebitoOnline;
	}
	
	/**
	 * Set: rdoPermiteDebitoOnline.
	 *
	 * @param rdoPermiteDebitoOnline the rdo permite debito online
	 */
	public void setRdoPermiteDebitoOnline(int rdoPermiteDebitoOnline) {
		this.rdoPermiteDebitoOnline = rdoPermiteDebitoOnline;
	}
	
	/**
	 * Get: rdoPermiteFavConsultarPag.
	 *
	 * @return rdoPermiteFavConsultarPag
	 */
	public int getRdoPermiteFavConsultarPag() {
		return rdoPermiteFavConsultarPag;
	}
	
	/**
	 * Set: rdoPermiteFavConsultarPag.
	 *
	 * @param rdoPermiteFavConsultarPag the rdo permite fav consultar pag
	 */
	public void setRdoPermiteFavConsultarPag(int rdoPermiteFavConsultarPag) {
		this.rdoPermiteFavConsultarPag = rdoPermiteFavConsultarPag;
	}
	
	/**
	 * Get: rdoPermiteOutrosTiposIncricaoFav.
	 *
	 * @return rdoPermiteOutrosTiposIncricaoFav
	 */
	public int getRdoPermiteOutrosTiposIncricaoFav() {
		return rdoPermiteOutrosTiposIncricaoFav;
	}
	
	/**
	 * Set: rdoPermiteOutrosTiposIncricaoFav.
	 *
	 * @param rdoPermiteOutrosTiposIncricaoFav the rdo permite outros tipos incricao fav
	 */
	public void setRdoPermiteOutrosTiposIncricaoFav(
			int rdoPermiteOutrosTiposIncricaoFav) {
		this.rdoPermiteOutrosTiposIncricaoFav = rdoPermiteOutrosTiposIncricaoFav;
	}
	
	/**
	 * Get: rdoPermitePagarMenor.
	 *
	 * @return rdoPermitePagarMenor
	 */
	public int getRdoPermitePagarMenor() {
		return rdoPermitePagarMenor;
	}
	
	/**
	 * Set: rdoPermitePagarMenor.
	 *
	 * @param rdoPermitePagarMenor the rdo permite pagar menor
	 */
	public void setRdoPermitePagarMenor(int rdoPermitePagarMenor) {
		this.rdoPermitePagarMenor = rdoPermitePagarMenor;
	}
	
	/**
	 * Get: rdoPermitePagarVencido.
	 *
	 * @return rdoPermitePagarVencido
	 */
	public int getRdoPermitePagarVencido() {
		return rdoPermitePagarVencido;
	}
	
	/**
	 * Set: rdoPermitePagarVencido.
	 *
	 * @param rdoPermitePagarVencido the rdo permite pagar vencido
	 */
	public void setRdoPermitePagarVencido(int rdoPermitePagarVencido) {
		this.rdoPermitePagarVencido = rdoPermitePagarVencido;
	}
	
	/**
	 * Get: rdoRastrearTitRastreadoFilial.
	 *
	 * @return rdoRastrearTitRastreadoFilial
	 */
	public int getRdoRastrearTitRastreadoFilial() {
		return rdoRastrearTitRastreadoFilial;
	}
	
	/**
	 * Set: rdoRastrearTitRastreadoFilial.
	 *
	 * @param rdoRastrearTitRastreadoFilial the rdo rastrear tit rastreado filial
	 */
	public void setRdoRastrearTitRastreadoFilial(int rdoRastrearTitRastreadoFilial) {
		this.rdoRastrearTitRastreadoFilial = rdoRastrearTitRastreadoFilial;
	}
	
	/**
	 * Get: rdoRastrearTitTerceiros.
	 *
	 * @return rdoRastrearTitTerceiros
	 */
	public int getRdoRastrearTitTerceiros() {
		return rdoRastrearTitTerceiros;
	}
	
	/**
	 * Set: rdoRastrearTitTerceiros.
	 *
	 * @param rdoRastrearTitTerceiros the rdo rastrear tit terceiros
	 */
	public void setRdoRastrearTitTerceiros(int rdoRastrearTitTerceiros) {
		this.rdoRastrearTitTerceiros = rdoRastrearTitTerceiros;
	}
	
	/**
	 * Get: rdoRastrearNotasFiscais.
	 *
	 * @return rdoRastrearNotasFiscais
	 */
	public int getRdoRastrearNotasFiscais() {
		return rdoRastrearNotasFiscais;
	}
	
	/**
	 * Set: rdoRastrearNotasFiscais.
	 *
	 * @param rdoRastreasNotasFiscais the rdo rastrear notas fiscais
	 */
	public void setRdoRastrearNotasFiscais(int rdoRastreasNotasFiscais) {
		this.rdoRastrearNotasFiscais = rdoRastreasNotasFiscais;
	}
	
	/**
	 * Get: rdoUtilizaCadFavControlePag.
	 *
	 * @return rdoUtilizaCadFavControlePag
	 */
	public int getRdoUtilizaCadFavControlePag() {
		return rdoUtilizaCadFavControlePag;
	}
	
	/**
	 * Set: rdoUtilizaCadFavControlePag.
	 *
	 * @param rdoUtilizaCadFavControlePag the rdo utiliza cad fav controle pag
	 */
	public void setRdoUtilizaCadFavControlePag(int rdoUtilizaCadFavControlePag) {
		this.rdoUtilizaCadFavControlePag = rdoUtilizaCadFavControlePag;
	}
	
	/**
	 * Get: valorLimiteDiario.
	 *
	 * @return valorLimiteDiario
	 */
	public BigDecimal getValorLimiteDiario() {
		return valorLimiteDiario;
	}
	
	/**
	 * Set: valorLimiteDiario.
	 *
	 * @param valorLimiteDiario the valor limite diario
	 */
	public void setValorLimiteDiario(BigDecimal valorLimiteDiario) {
		this.valorLimiteDiario = valorLimiteDiario;
	}
	
	/**
	 * Get: valorLimiteIndividual.
	 *
	 * @return valorLimiteIndividual
	 */
	public BigDecimal getValorLimiteIndividual() {
		return valorLimiteIndividual;
	}
	
	/**
	 * Set: valorLimiteIndividual.
	 *
	 * @param valorLimiteIndividual the valor limite individual
	 */
	public void setValorLimiteIndividual(BigDecimal valorLimiteIndividual) {
		this.valorLimiteIndividual = valorLimiteIndividual;
	}
	
	/**
	 * Get: valorMaximoPagamentoFavorecidoNaoCadastrado.
	 *
	 * @return valorMaximoPagamentoFavorecidoNaoCadastrado
	 */
	public BigDecimal getValorMaximoPagamentoFavorecidoNaoCadastrado() {
		return valorMaximoPagamentoFavorecidoNaoCadastrado;
	}
	
	/**
	 * Set: valorMaximoPagamentoFavorecidoNaoCadastrado.
	 *
	 * @param valorMaximoPagamentoFavorecidoNaoCadastrado the valor maximo pagamento favorecido nao cadastrado
	 */
	public void setValorMaximoPagamentoFavorecidoNaoCadastrado(
			BigDecimal valorMaximoPagamentoFavorecidoNaoCadastrado) {
		this.valorMaximoPagamentoFavorecidoNaoCadastrado = valorMaximoPagamentoFavorecidoNaoCadastrado;
	}
	
	/**
	 * Get: cdPrioridadeDebito.
	 *
	 * @return cdPrioridadeDebito
	 */
	public int getCdPrioridadeDebito() {
		return cdPrioridadeDebito;
	}
	
	/**
	 * Set: cdPrioridadeDebito.
	 *
	 * @param cdPrioridadeDebito the cd prioridade debito
	 */
	public void setCdPrioridadeDebito(int cdPrioridadeDebito) {
		this.cdPrioridadeDebito = cdPrioridadeDebito;
	}
	
	/**
	 * Get: valorMaxPagFavNaoCadastrado.
	 *
	 * @return valorMaxPagFavNaoCadastrado
	 */
	public BigDecimal getValorMaxPagFavNaoCadastrado() {
		return valorMaxPagFavNaoCadastrado;
	}
	
	/**
	 * Set: valorMaxPagFavNaoCadastrado.
	 *
	 * @param valorMaxPagFavNaoCadastrado the valor max pag fav nao cadastrado
	 */
	public void setValorMaxPagFavNaoCadastrado(
			BigDecimal valorMaxPagFavNaoCadastrado) {
		this.valorMaxPagFavNaoCadastrado = valorMaxPagFavNaoCadastrado;
	}
	
	/**
	 * Get: cdOcorrenciaDebito.
	 *
	 * @return cdOcorrenciaDebito
	 */
	public int getCdOcorrenciaDebito() {
		return cdOcorrenciaDebito;
	}
	
	/**
	 * Set: cdOcorrenciaDebito.
	 *
	 * @param cdOcorrenciaDebito the cd ocorrencia debito
	 */
	public void setCdOcorrenciaDebito(int cdOcorrenciaDebito) {
		this.cdOcorrenciaDebito = cdOcorrenciaDebito;
	}
	
	/**
	 * Get: dsOcorrenciaDebito.
	 *
	 * @return dsOcorrenciaDebito
	 */
	public String getDsOcorrenciaDebito() {
		return dsOcorrenciaDebito;
	}
	
	/**
	 * Set: dsOcorrenciaDebito.
	 *
	 * @param dsOcorrenciaDebito the ds ocorrencia debito
	 */
	public void setDsOcorrenciaDebito(String dsOcorrenciaDebito) {
		this.dsOcorrenciaDebito = dsOcorrenciaDebito;
	}
	
	/**
	 * Get: percentualMaxRegInconsistenciaLote.
	 *
	 * @return percentualMaxRegInconsistenciaLote
	 */
	public int getPercentualMaxRegInconsistenciaLote() {
		return percentualMaxRegInconsistenciaLote;
	}
	
	/**
	 * Set: percentualMaxRegInconsistenciaLote.
	 *
	 * @param percentualMaxRegInconsistenciaLote the percentual max reg inconsistencia lote
	 */
	public void setPercentualMaxRegInconsistenciaLote(
			int percentualMaxRegInconsistenciaLote) {
		this.percentualMaxRegInconsistenciaLote = percentualMaxRegInconsistenciaLote;
	}
	
	/**
	 * Get: qtdeMaxInconsistenciaPorLote.
	 *
	 * @return qtdeMaxInconsistenciaPorLote
	 */
	public int getQtdeMaxInconsistenciaPorLote() {
		return qtdeMaxInconsistenciaPorLote;
	}
	
	/**
	 * Set: qtdeMaxInconsistenciaPorLote.
	 *
	 * @param qtdeMaxInconsistenciaPorLote the qtde max inconsistencia por lote
	 */
	public void setQtdeMaxInconsistenciaPorLote(int qtdeMaxInconsistenciaPorLote) {
		this.qtdeMaxInconsistenciaPorLote = qtdeMaxInconsistenciaPorLote;
	}
	
	/**
	 * Get: cdDiaFloatPagamento.
	 *
	 * @return cdDiaFloatPagamento
	 */
	public Integer getCdDiaFloatPagamento() {
		return cdDiaFloatPagamento;
	}
	
	/**
	 * Set: cdDiaFloatPagamento.
	 *
	 * @param cdDiaFloatPagamento the cd dia float pagamento
	 */
	public void setCdDiaFloatPagamento(Integer cdDiaFloatPagamento) {
		this.cdDiaFloatPagamento = cdDiaFloatPagamento;
	}
	
	/**
	 * Get: cdMomentoProcessamentoPagamento.
	 *
	 * @return cdMomentoProcessamentoPagamento
	 */
	public Integer getCdMomentoProcessamentoPagamento() {
		return cdMomentoProcessamentoPagamento;
	}
	
	/**
	 * Set: cdMomentoProcessamentoPagamento.
	 *
	 * @param cdMomentoProcessamentoPagamento the cd momento processamento pagamento
	 */
	public void setCdMomentoProcessamentoPagamento(
			Integer cdMomentoProcessamentoPagamento) {
		this.cdMomentoProcessamentoPagamento = cdMomentoProcessamentoPagamento;
	}
	
	/**
	 * Get: cdRejeicaoAgendaLote.
	 *
	 * @return cdRejeicaoAgendaLote
	 */
	public Integer getCdRejeicaoAgendaLote() {
		return cdRejeicaoAgendaLote;
	}
	
	/**
	 * Set: cdRejeicaoAgendaLote.
	 *
	 * @param cdRejeicaoAgendaLote the cd rejeicao agenda lote
	 */
	public void setCdRejeicaoAgendaLote(Integer cdRejeicaoAgendaLote) {
		this.cdRejeicaoAgendaLote = cdRejeicaoAgendaLote;
	}
	
	/**
	 * Get: cdRejeicaoEfetivacaoLote.
	 *
	 * @return cdRejeicaoEfetivacaoLote
	 */
	public Integer getCdRejeicaoEfetivacaoLote() {
		return cdRejeicaoEfetivacaoLote;
	}
	
	/**
	 * Set: cdRejeicaoEfetivacaoLote.
	 *
	 * @param cdRejeicaoEfetivacaoLote the cd rejeicao efetivacao lote
	 */
	public void setCdRejeicaoEfetivacaoLote(Integer cdRejeicaoEfetivacaoLote) {
		this.cdRejeicaoEfetivacaoLote = cdRejeicaoEfetivacaoLote;
	}
	
	/**
	 * Get: cdPercentualMaximoInconLote.
	 *
	 * @return cdPercentualMaximoInconLote
	 */
	public Integer getCdPercentualMaximoInconLote() {
		return cdPercentualMaximoInconLote;
	}
	
	/**
	 * Set: cdPercentualMaximoInconLote.
	 *
	 * @param cdPercentualMaximoInconLote the cd percentual maximo incon lote
	 */
	public void setCdPercentualMaximoInconLote(Integer cdPercentualMaximoInconLote) {
		this.cdPercentualMaximoInconLote = cdPercentualMaximoInconLote;
	}
	
	/**
	 * Get: cdConsSaldoPagamento.
	 *
	 * @return cdConsSaldoPagamento
	 */
	public Integer getCdConsSaldoPagamento() {
		return cdConsSaldoPagamento;
	}
	
	/**
	 * Set: cdConsSaldoPagamento.
	 *
	 * @param cdConsSaldoPagamento the cd cons saldo pagamento
	 */
	public void setCdConsSaldoPagamento(Integer cdConsSaldoPagamento) {
		this.cdConsSaldoPagamento = cdConsSaldoPagamento;
	}
	
	/**
	 * Get: cdCtciaEspecieBeneficio.
	 *
	 * @return cdCtciaEspecieBeneficio
	 */
	public Integer getCdCtciaEspecieBeneficio() {
		return cdCtciaEspecieBeneficio;
	}
	
	/**
	 * Set: cdCtciaEspecieBeneficio.
	 *
	 * @param cdCtciaEspecieBeneficio the cd ctcia especie beneficio
	 */
	public void setCdCtciaEspecieBeneficio(Integer cdCtciaEspecieBeneficio) {
		this.cdCtciaEspecieBeneficio = cdCtciaEspecieBeneficio;
	}
	
	/**
	 * Get: cdCtciaInscricaoFavorecido.
	 *
	 * @return cdCtciaInscricaoFavorecido
	 */
	public Integer getCdCtciaInscricaoFavorecido() {
		return cdCtciaInscricaoFavorecido;
	}
	
	/**
	 * Set: cdCtciaInscricaoFavorecido.
	 *
	 * @param cdCtciaInscricaoFavorecido the cd ctcia inscricao favorecido
	 */
	public void setCdCtciaInscricaoFavorecido(Integer cdCtciaInscricaoFavorecido) {
		this.cdCtciaInscricaoFavorecido = cdCtciaInscricaoFavorecido;
	}
	
	/**
	 * Get: cdFavorecidoConsPagamento.
	 *
	 * @return cdFavorecidoConsPagamento
	 */
	public Integer getCdFavorecidoConsPagamento() {
		return cdFavorecidoConsPagamento;
	}
	
	/**
	 * Set: cdFavorecidoConsPagamento.
	 *
	 * @param cdFavorecidoConsPagamento the cd favorecido cons pagamento
	 */
	public void setCdFavorecidoConsPagamento(Integer cdFavorecidoConsPagamento) {
		this.cdFavorecidoConsPagamento = cdFavorecidoConsPagamento;
	}
	
	/**
	 * Get: cdLancamentoFuturoCredito.
	 *
	 * @return cdLancamentoFuturoCredito
	 */
	public Integer getCdLancamentoFuturoCredito() {
		return cdLancamentoFuturoCredito;
	}
	
	/**
	 * Set: cdLancamentoFuturoCredito.
	 *
	 * @param cdLancamentoFuturoCredito the cd lancamento futuro credito
	 */
	public void setCdLancamentoFuturoCredito(Integer cdLancamentoFuturoCredito) {
		this.cdLancamentoFuturoCredito = cdLancamentoFuturoCredito;
	}
	
	/**
	 * Get: cdLancamentoFuturoDebito.
	 *
	 * @return cdLancamentoFuturoDebito
	 */
	public Integer getCdLancamentoFuturoDebito() {
		return cdLancamentoFuturoDebito;
	}
	
	/**
	 * Set: cdLancamentoFuturoDebito.
	 *
	 * @param cdLancamentoFuturoDebito the cd lancamento futuro debito
	 */
	public void setCdLancamentoFuturoDebito(Integer cdLancamentoFuturoDebito) {
		this.cdLancamentoFuturoDebito = cdLancamentoFuturoDebito;
	}
	
	/**
	 * Get: cdUtilizacaoFavorecidoControle.
	 *
	 * @return cdUtilizacaoFavorecidoControle
	 */
	public Integer getCdUtilizacaoFavorecidoControle() {
		return cdUtilizacaoFavorecidoControle;
	}
	
	/**
	 * Set: cdUtilizacaoFavorecidoControle.
	 *
	 * @param cdUtilizacaoFavorecidoControle the cd utilizacao favorecido controle
	 */
	public void setCdUtilizacaoFavorecidoControle(
			Integer cdUtilizacaoFavorecidoControle) {
		this.cdUtilizacaoFavorecidoControle = cdUtilizacaoFavorecidoControle;
	}
	
	/**
	 * Get: qtDiaRepiqConsulta.
	 *
	 * @return qtDiaRepiqConsulta
	 */
	public Integer getQtDiaRepiqConsulta() {
		return qtDiaRepiqConsulta;
	}
	
	/**
	 * Set: qtDiaRepiqConsulta.
	 *
	 * @param qtDiaRepiqConsulta the qt dia repiq consulta
	 */
	public void setQtDiaRepiqConsulta(Integer qtDiaRepiqConsulta) {
		this.qtDiaRepiqConsulta = qtDiaRepiqConsulta;
	}
	
	/**
	 * Get: vlFavorecidoNaoCadastro.
	 *
	 * @return vlFavorecidoNaoCadastro
	 */
	public BigDecimal getVlFavorecidoNaoCadastro() {
		return vlFavorecidoNaoCadastro;
	}
	
	/**
	 * Set: vlFavorecidoNaoCadastro.
	 *
	 * @param vlFavorecidoNaoCadastro the vl favorecido nao cadastro
	 */
	public void setVlFavorecidoNaoCadastro(BigDecimal vlFavorecidoNaoCadastro) {
		this.vlFavorecidoNaoCadastro = vlFavorecidoNaoCadastro;
	}
	
	/**
	 * Get: cdAcaoNaoVida.
	 *
	 * @return cdAcaoNaoVida
	 */
	public Integer getCdAcaoNaoVida() {
		return cdAcaoNaoVida;
	}
	
	/**
	 * Set: cdAcaoNaoVida.
	 *
	 * @param cdAcaoNaoVida the cd acao nao vida
	 */
	public void setCdAcaoNaoVida(Integer cdAcaoNaoVida) {
		this.cdAcaoNaoVida = cdAcaoNaoVida;
	}
	
	/**
	 * Get: cdAcertoDadoRecadastro.
	 *
	 * @return cdAcertoDadoRecadastro
	 */
	public Integer getCdAcertoDadoRecadastro() {
		return cdAcertoDadoRecadastro;
	}
	
	/**
	 * Set: cdAcertoDadoRecadastro.
	 *
	 * @param cdAcertoDadoRecadastro the cd acerto dado recadastro
	 */
	public void setCdAcertoDadoRecadastro(Integer cdAcertoDadoRecadastro) {
		this.cdAcertoDadoRecadastro = cdAcertoDadoRecadastro;
	}
	
	/**
	 * Get: cdAgendaDebitoVeiculo.
	 *
	 * @return cdAgendaDebitoVeiculo
	 */
	public Integer getCdAgendaDebitoVeiculo() {
		return cdAgendaDebitoVeiculo;
	}
	
	/**
	 * Set: cdAgendaDebitoVeiculo.
	 *
	 * @param cdAgendaDebitoVeiculo the cd agenda debito veiculo
	 */
	public void setCdAgendaDebitoVeiculo(Integer cdAgendaDebitoVeiculo) {
		this.cdAgendaDebitoVeiculo = cdAgendaDebitoVeiculo;
	}
	
	/**
	 * Get: cdAgendaPagamentoVencido.
	 *
	 * @return cdAgendaPagamentoVencido
	 */
	public Integer getCdAgendaPagamentoVencido() {
		return cdAgendaPagamentoVencido;
	}
	
	/**
	 * Set: cdAgendaPagamentoVencido.
	 *
	 * @param cdAgendaPagamentoVencido the cd agenda pagamento vencido
	 */
	public void setCdAgendaPagamentoVencido(Integer cdAgendaPagamentoVencido) {
		this.cdAgendaPagamentoVencido = cdAgendaPagamentoVencido;
	}
	
	/**
	 * Get: cdAgendaRastreabilidadeFilial.
	 *
	 * @return cdAgendaRastreabilidadeFilial
	 */
	public Integer getCdAgendaRastreabilidadeFilial() {
		return cdAgendaRastreabilidadeFilial;
	}
	
	/**
	 * Set: cdAgendaRastreabilidadeFilial.
	 *
	 * @param cdAgendaRastreabilidadeFilial the cd agenda rastreabilidade filial
	 */
	public void setCdAgendaRastreabilidadeFilial(
			Integer cdAgendaRastreabilidadeFilial) {
		this.cdAgendaRastreabilidadeFilial = cdAgendaRastreabilidadeFilial;
	}
	
	/**
	 * Get: cdAgendaValorMenor.
	 *
	 * @return cdAgendaValorMenor
	 */
	public Integer getCdAgendaValorMenor() {
		return cdAgendaValorMenor;
	}
	
	/**
	 * Set: cdAgendaValorMenor.
	 *
	 * @param cdAgendaValorMenor the cd agenda valor menor
	 */
	public void setCdAgendaValorMenor(Integer cdAgendaValorMenor) {
		this.cdAgendaValorMenor = cdAgendaValorMenor;
	}
	
	/**
	 * Get: cdAgrupamentoAviso.
	 *
	 * @return cdAgrupamentoAviso
	 */
	public Integer getCdAgrupamentoAviso() {
		return cdAgrupamentoAviso;
	}
	
	/**
	 * Set: cdAgrupamentoAviso.
	 *
	 * @param cdAgrupamentoAviso the cd agrupamento aviso
	 */
	public void setCdAgrupamentoAviso(Integer cdAgrupamentoAviso) {
		this.cdAgrupamentoAviso = cdAgrupamentoAviso;
	}
	
	/**
	 * Get: cdAgrupamentoComprovado.
	 *
	 * @return cdAgrupamentoComprovado
	 */
	public Integer getCdAgrupamentoComprovado() {
		return cdAgrupamentoComprovado;
	}
	
	/**
	 * Set: cdAgrupamentoComprovado.
	 *
	 * @param cdAgrupamentoComprovado the cd agrupamento comprovado
	 */
	public void setCdAgrupamentoComprovado(Integer cdAgrupamentoComprovado) {
		this.cdAgrupamentoComprovado = cdAgrupamentoComprovado;
	}
	
	/**
	 * Get: cdAgrupamentoFormularioRecadastro.
	 *
	 * @return cdAgrupamentoFormularioRecadastro
	 */
	public Integer getCdAgrupamentoFormularioRecadastro() {
		return cdAgrupamentoFormularioRecadastro;
	}
	
	/**
	 * Set: cdAgrupamentoFormularioRecadastro.
	 *
	 * @param cdAgrupamentoFormularioRecadastro the cd agrupamento formulario recadastro
	 */
	public void setCdAgrupamentoFormularioRecadastro(
			Integer cdAgrupamentoFormularioRecadastro) {
		this.cdAgrupamentoFormularioRecadastro = cdAgrupamentoFormularioRecadastro;
	}
	
	/**
	 * Get: cdAntecRecadastroBeneficio.
	 *
	 * @return cdAntecRecadastroBeneficio
	 */
	public Integer getCdAntecRecadastroBeneficio() {
		return cdAntecRecadastroBeneficio;
	}
	
	/**
	 * Set: cdAntecRecadastroBeneficio.
	 *
	 * @param cdAntecRecadastroBeneficio the cd antec recadastro beneficio
	 */
	public void setCdAntecRecadastroBeneficio(Integer cdAntecRecadastroBeneficio) {
		this.cdAntecRecadastroBeneficio = cdAntecRecadastroBeneficio;
	}
	
	/**
	 * Get: cdAreaReservada.
	 *
	 * @return cdAreaReservada
	 */
	public Integer getCdAreaReservada() {
		return cdAreaReservada;
	}
	
	/**
	 * Set: cdAreaReservada.
	 *
	 * @param cdAreaReservada the cd area reservada
	 */
	public void setCdAreaReservada(Integer cdAreaReservada) {
		this.cdAreaReservada = cdAreaReservada;
	}
	
	/**
	 * Get: cdBaseRecadastroBeneficio.
	 *
	 * @return cdBaseRecadastroBeneficio
	 */
	public Integer getCdBaseRecadastroBeneficio() {
		return cdBaseRecadastroBeneficio;
	}
	
	/**
	 * Set: cdBaseRecadastroBeneficio.
	 *
	 * @param cdBaseRecadastroBeneficio the cd base recadastro beneficio
	 */
	public void setCdBaseRecadastroBeneficio(Integer cdBaseRecadastroBeneficio) {
		this.cdBaseRecadastroBeneficio = cdBaseRecadastroBeneficio;
	}
	
	/**
	 * Get: cdBloqueioEmissaoPplta.
	 *
	 * @return cdBloqueioEmissaoPplta
	 */
	public Integer getCdBloqueioEmissaoPplta() {
		return cdBloqueioEmissaoPplta;
	}
	
	/**
	 * Set: cdBloqueioEmissaoPplta.
	 *
	 * @param cdBloqueioEmissaoPplta the cd bloqueio emissao pplta
	 */
	public void setCdBloqueioEmissaoPplta(Integer cdBloqueioEmissaoPplta) {
		this.cdBloqueioEmissaoPplta = cdBloqueioEmissaoPplta;
	}
	
	/**
	 * Get: cdCanalAlteracao.
	 *
	 * @return cdCanalAlteracao
	 */
	public Integer getCdCanalAlteracao() {
		return cdCanalAlteracao;
	}
	
	/**
	 * Set: cdCanalAlteracao.
	 *
	 * @param cdCanalAlteracao the cd canal alteracao
	 */
	public void setCdCanalAlteracao(Integer cdCanalAlteracao) {
		this.cdCanalAlteracao = cdCanalAlteracao;
	}
	
	/**
	 * Get: cdCapituloTituloRegistro.
	 *
	 * @return cdCapituloTituloRegistro
	 */
	public Integer getCdCapituloTituloRegistro() {
		return cdCapituloTituloRegistro;
	}
	
	/**
	 * Set: cdCapituloTituloRegistro.
	 *
	 * @param cdCapituloTituloRegistro the cd capitulo titulo registro
	 */
	public void setCdCapituloTituloRegistro(Integer cdCapituloTituloRegistro) {
		this.cdCapituloTituloRegistro = cdCapituloTituloRegistro;
	}
	
	/**
	 * Get: cdCobrancaTarifa.
	 *
	 * @return cdCobrancaTarifa
	 */
	public Integer getCdCobrancaTarifa() {
		return cdCobrancaTarifa;
	}
	
	/**
	 * Set: cdCobrancaTarifa.
	 *
	 * @param cdCobrancaTarifa the cd cobranca tarifa
	 */
	public void setCdCobrancaTarifa(Integer cdCobrancaTarifa) {
		this.cdCobrancaTarifa = cdCobrancaTarifa;
	}
	
	/**
	 * Get: cdConsDebitoVeiculo.
	 *
	 * @return cdConsDebitoVeiculo
	 */
	public Integer getCdConsDebitoVeiculo() {
		return cdConsDebitoVeiculo;
	}
	
	/**
	 * Set: cdConsDebitoVeiculo.
	 *
	 * @param cdConsDebitoVeiculo the cd cons debito veiculo
	 */
	public void setCdConsDebitoVeiculo(Integer cdConsDebitoVeiculo) {
		this.cdConsDebitoVeiculo = cdConsDebitoVeiculo;
	}
	
	/**
	 * Get: cdConsEndereco.
	 *
	 * @return cdConsEndereco
	 */
	public Integer getCdConsEndereco() {
		return cdConsEndereco;
	}
	
	/**
	 * Set: cdConsEndereco.
	 *
	 * @param cdConsEndereco the cd cons endereco
	 */
	public void setCdConsEndereco(Integer cdConsEndereco) {
		this.cdConsEndereco = cdConsEndereco;
	}
	
	/**
	 * Get: cdContagemConsSaldo.
	 *
	 * @return cdContagemConsSaldo
	 */
	public Integer getCdContagemConsSaldo() {
		return cdContagemConsSaldo;
	}
	
	/**
	 * Set: cdContagemConsSaldo.
	 *
	 * @param cdContagemConsSaldo the cd contagem cons saldo
	 */
	public void setCdContagemConsSaldo(Integer cdContagemConsSaldo) {
		this.cdContagemConsSaldo = cdContagemConsSaldo;
	}
	
	/**
	 * Get: cdCreditoNaoUtilizado.
	 *
	 * @return cdCreditoNaoUtilizado
	 */
	public Integer getCdCreditoNaoUtilizado() {
		return cdCreditoNaoUtilizado;
	}
	
	/**
	 * Set: cdCreditoNaoUtilizado.
	 *
	 * @param cdCreditoNaoUtilizado the cd credito nao utilizado
	 */
	public void setCdCreditoNaoUtilizado(Integer cdCreditoNaoUtilizado) {
		this.cdCreditoNaoUtilizado = cdCreditoNaoUtilizado;
	}
	
	/**
	 * Get: cdCriterioEnquaBeneficio.
	 *
	 * @return cdCriterioEnquaBeneficio
	 */
	public Integer getCdCriterioEnquaBeneficio() {
		return cdCriterioEnquaBeneficio;
	}
	
	/**
	 * Set: cdCriterioEnquaBeneficio.
	 *
	 * @param cdCriterioEnquaBeneficio the cd criterio enqua beneficio
	 */
	public void setCdCriterioEnquaBeneficio(Integer cdCriterioEnquaBeneficio) {
		this.cdCriterioEnquaBeneficio = cdCriterioEnquaBeneficio;
	}
	
	/**
	 * Get: cdCriterioEnquaRecadastro.
	 *
	 * @return cdCriterioEnquaRecadastro
	 */
	public Integer getCdCriterioEnquaRecadastro() {
		return cdCriterioEnquaRecadastro;
	}
	
	/**
	 * Set: cdCriterioEnquaRecadastro.
	 *
	 * @param cdCriterioEnquaRecadastro the cd criterio enqua recadastro
	 */
	public void setCdCriterioEnquaRecadastro(Integer cdCriterioEnquaRecadastro) {
		this.cdCriterioEnquaRecadastro = cdCriterioEnquaRecadastro;
	}
	
	/**
	 * Get: cdCriterioRastreabilidadeTitulo.
	 *
	 * @return cdCriterioRastreabilidadeTitulo
	 */
	public Integer getCdCriterioRastreabilidadeTitulo() {
		return cdCriterioRastreabilidadeTitulo;
	}
	
	/**
	 * Set: cdCriterioRastreabilidadeTitulo.
	 *
	 * @param cdCriterioRastreabilidadeTitulo the cd criterio rastreabilidade titulo
	 */
	public void setCdCriterioRastreabilidadeTitulo(
			Integer cdCriterioRastreabilidadeTitulo) {
		this.cdCriterioRastreabilidadeTitulo = cdCriterioRastreabilidadeTitulo;
	}
	
	/**
	 * Get: cdCtciaIdentificacaoBeneficio.
	 *
	 * @return cdCtciaIdentificacaoBeneficio
	 */
	public Integer getCdCtciaIdentificacaoBeneficio() {
		return cdCtciaIdentificacaoBeneficio;
	}
	
	/**
	 * Set: cdCtciaIdentificacaoBeneficio.
	 *
	 * @param cdCtciaIdentificacaoBeneficio the cd ctcia identificacao beneficio
	 */
	public void setCdCtciaIdentificacaoBeneficio(
			Integer cdCtciaIdentificacaoBeneficio) {
		this.cdCtciaIdentificacaoBeneficio = cdCtciaIdentificacaoBeneficio;
	}
	
	/**
	 * Get: cdCtciaProprietarioVeiculo.
	 *
	 * @return cdCtciaProprietarioVeiculo
	 */
	public Integer getCdCtciaProprietarioVeiculo() {
		return cdCtciaProprietarioVeiculo;
	}
	
	/**
	 * Set: cdCtciaProprietarioVeiculo.
	 *
	 * @param cdCtciaProprietarioVeiculo the cd ctcia proprietario veiculo
	 */
	public void setCdCtciaProprietarioVeiculo(Integer cdCtciaProprietarioVeiculo) {
		this.cdCtciaProprietarioVeiculo = cdCtciaProprietarioVeiculo;
	}
	
	/**
	 * Get: cdDestinoAviso.
	 *
	 * @return cdDestinoAviso
	 */
	public Integer getCdDestinoAviso() {
		return cdDestinoAviso;
	}
	
	/**
	 * Set: cdDestinoAviso.
	 *
	 * @param cdDestinoAviso the cd destino aviso
	 */
	public void setCdDestinoAviso(Integer cdDestinoAviso) {
		this.cdDestinoAviso = cdDestinoAviso;
	}
	
	/**
	 * Get: cdDestinoComprovante.
	 *
	 * @return cdDestinoComprovante
	 */
	public Integer getCdDestinoComprovante() {
		return cdDestinoComprovante;
	}
	
	/**
	 * Set: cdDestinoComprovante.
	 *
	 * @param cdDestinoComprovante the cd destino comprovante
	 */
	public void setCdDestinoComprovante(Integer cdDestinoComprovante) {
		this.cdDestinoComprovante = cdDestinoComprovante;
	}
	
	/**
	 * Get: cdDestinoFormularioRecadastro.
	 *
	 * @return cdDestinoFormularioRecadastro
	 */
	public Integer getCdDestinoFormularioRecadastro() {
		return cdDestinoFormularioRecadastro;
	}
	
	/**
	 * Set: cdDestinoFormularioRecadastro.
	 *
	 * @param cdDestinoFormularioRecadastro the cd destino formulario recadastro
	 */
	public void setCdDestinoFormularioRecadastro(
			Integer cdDestinoFormularioRecadastro) {
		this.cdDestinoFormularioRecadastro = cdDestinoFormularioRecadastro;
	}
	
	/**
	 * Get: cdDispzContaCredito.
	 *
	 * @return cdDispzContaCredito
	 */
	public Integer getCdDispzContaCredito() {
		return cdDispzContaCredito;
	}
	
	/**
	 * Set: cdDispzContaCredito.
	 *
	 * @param cdDispzContaCredito the cd dispz conta credito
	 */
	public void setCdDispzContaCredito(Integer cdDispzContaCredito) {
		this.cdDispzContaCredito = cdDispzContaCredito;
	}
	
	/**
	 * Get: cdDispzDiversarCrrtt.
	 *
	 * @return cdDispzDiversarCrrtt
	 */
	public Integer getCdDispzDiversarCrrtt() {
		return cdDispzDiversarCrrtt;
	}
	
	/**
	 * Set: cdDispzDiversarCrrtt.
	 *
	 * @param cdDispzDiversarCrrtt the cd dispz diversar crrtt
	 */
	public void setCdDispzDiversarCrrtt(Integer cdDispzDiversarCrrtt) {
		this.cdDispzDiversarCrrtt = cdDispzDiversarCrrtt;
	}
	
	/**
	 * Get: cdDispzDiversasNao.
	 *
	 * @return cdDispzDiversasNao
	 */
	public Integer getCdDispzDiversasNao() {
		return cdDispzDiversasNao;
	}
	
	/**
	 * Set: cdDispzDiversasNao.
	 *
	 * @param cdDispzDiversasNao the cd dispz diversas nao
	 */
	public void setCdDispzDiversasNao(Integer cdDispzDiversasNao) {
		this.cdDispzDiversasNao = cdDispzDiversasNao;
	}
	
	/**
	 * Get: cdDispzSalarioCrrtt.
	 *
	 * @return cdDispzSalarioCrrtt
	 */
	public Integer getCdDispzSalarioCrrtt() {
		return cdDispzSalarioCrrtt;
	}
	
	/**
	 * Set: cdDispzSalarioCrrtt.
	 *
	 * @param cdDispzSalarioCrrtt the cd dispz salario crrtt
	 */
	public void setCdDispzSalarioCrrtt(Integer cdDispzSalarioCrrtt) {
		this.cdDispzSalarioCrrtt = cdDispzSalarioCrrtt;
	}
	
	/**
	 * Get: cdDispzSalarioNao.
	 *
	 * @return cdDispzSalarioNao
	 */
	public Integer getCdDispzSalarioNao() {
		return cdDispzSalarioNao;
	}
	
	/**
	 * Set: cdDispzSalarioNao.
	 *
	 * @param cdDispzSalarioNao the cd dispz salario nao
	 */
	public void setCdDispzSalarioNao(Integer cdDispzSalarioNao) {
		this.cdDispzSalarioNao = cdDispzSalarioNao;
	}
	
	/**
	 * Get: cdEnvelopeAberto.
	 *
	 * @return cdEnvelopeAberto
	 */
	public Integer getCdEnvelopeAberto() {
		return cdEnvelopeAberto;
	}
	
	/**
	 * Set: cdEnvelopeAberto.
	 *
	 * @param cdEnvelopeAberto the cd envelope aberto
	 */
	public void setCdEnvelopeAberto(Integer cdEnvelopeAberto) {
		this.cdEnvelopeAberto = cdEnvelopeAberto;
	}
	
	/**
	 * Get: cdFormaExpiracaoCredito.
	 *
	 * @return cdFormaExpiracaoCredito
	 */
	public Integer getCdFormaExpiracaoCredito() {
		return cdFormaExpiracaoCredito;
	}
	
	/**
	 * Set: cdFormaExpiracaoCredito.
	 *
	 * @param cdFormaExpiracaoCredito the cd forma expiracao credito
	 */
	public void setCdFormaExpiracaoCredito(Integer cdFormaExpiracaoCredito) {
		this.cdFormaExpiracaoCredito = cdFormaExpiracaoCredito;
	}
	
	/**
	 * Get: cdFormaManutencao.
	 *
	 * @return cdFormaManutencao
	 */
	public Integer getCdFormaManutencao() {
		return cdFormaManutencao;
	}
	
	/**
	 * Set: cdFormaManutencao.
	 *
	 * @param cdFormaManutencao the cd forma manutencao
	 */
	public void setCdFormaManutencao(Integer cdFormaManutencao) {
		this.cdFormaManutencao = cdFormaManutencao;
	}
	
	/**
	 * Get: cdFormularioContratoCliente.
	 *
	 * @return cdFormularioContratoCliente
	 */
	public Integer getCdFormularioContratoCliente() {
		return cdFormularioContratoCliente;
	}
	
	/**
	 * Set: cdFormularioContratoCliente.
	 *
	 * @param cdFormularioContratoCliente the cd formulario contrato cliente
	 */
	public void setCdFormularioContratoCliente(Integer cdFormularioContratoCliente) {
		this.cdFormularioContratoCliente = cdFormularioContratoCliente;
	}
	
	/**
	 * Get: cdFrasePreCadastro.
	 *
	 * @return cdFrasePreCadastro
	 */
	public Integer getCdFrasePreCadastro() {
		return cdFrasePreCadastro;
	}
	
	/**
	 * Set: cdFrasePreCadastro.
	 *
	 * @param cdFrasePreCadastro the cd frase pre cadastro
	 */
	public void setCdFrasePreCadastro(Integer cdFrasePreCadastro) {
		this.cdFrasePreCadastro = cdFrasePreCadastro;
	}
	
	/**
	 * Get: cdIndicadorAdesaoSacador.
	 *
	 * @return cdIndicadorAdesaoSacador
	 */
	public Integer getCdIndicadorAdesaoSacador() {
		return cdIndicadorAdesaoSacador;
	}
	
	/**
	 * Set: cdIndicadorAdesaoSacador.
	 *
	 * @param cdIndicadorAdesaoSacador the cd indicador adesao sacador
	 */
	public void setCdIndicadorAdesaoSacador(Integer cdIndicadorAdesaoSacador) {
		this.cdIndicadorAdesaoSacador = cdIndicadorAdesaoSacador;
	}
	
	/**
	 * Get: cdIndicadorAgendaTitulo.
	 *
	 * @return cdIndicadorAgendaTitulo
	 */
	public Integer getCdIndicadorAgendaTitulo() {
		return cdIndicadorAgendaTitulo;
	}
	
	/**
	 * Set: cdIndicadorAgendaTitulo.
	 *
	 * @param cdIndicadorAgendaTitulo the cd indicador agenda titulo
	 */
	public void setCdIndicadorAgendaTitulo(Integer cdIndicadorAgendaTitulo) {
		this.cdIndicadorAgendaTitulo = cdIndicadorAgendaTitulo;
	}
	
	/**
	 * Get: cdIndicadorAutorizacaoCliente.
	 *
	 * @return cdIndicadorAutorizacaoCliente
	 */
	public Integer getCdIndicadorAutorizacaoCliente() {
		return cdIndicadorAutorizacaoCliente;
	}
	
	/**
	 * Set: cdIndicadorAutorizacaoCliente.
	 *
	 * @param cdIndicadorAutorizacaoCliente the cd indicador autorizacao cliente
	 */
	public void setCdIndicadorAutorizacaoCliente(
			Integer cdIndicadorAutorizacaoCliente) {
		this.cdIndicadorAutorizacaoCliente = cdIndicadorAutorizacaoCliente;
	}
	
	/**
	 * Get: cdIndicadorAutorizacaoComplemento.
	 *
	 * @return cdIndicadorAutorizacaoComplemento
	 */
	public Integer getCdIndicadorAutorizacaoComplemento() {
		return cdIndicadorAutorizacaoComplemento;
	}
	
	/**
	 * Set: cdIndicadorAutorizacaoComplemento.
	 *
	 * @param cdIndicadorAutorizacaoComplemento the cd indicador autorizacao complemento
	 */
	public void setCdIndicadorAutorizacaoComplemento(
			Integer cdIndicadorAutorizacaoComplemento) {
		this.cdIndicadorAutorizacaoComplemento = cdIndicadorAutorizacaoComplemento;
	}
	
	/**
	 * Get: cdIndicadorBancoPostal.
	 *
	 * @return cdIndicadorBancoPostal
	 */
	public Integer getCdIndicadorBancoPostal() {
		return cdIndicadorBancoPostal;
	}
	
	/**
	 * Set: cdIndicadorBancoPostal.
	 *
	 * @param cdIndicadorBancoPostal the cd indicador banco postal
	 */
	public void setCdIndicadorBancoPostal(Integer cdIndicadorBancoPostal) {
		this.cdIndicadorBancoPostal = cdIndicadorBancoPostal;
	}
	
	/**
	 * Get: cdIndicadorCadastroOrg.
	 *
	 * @return cdIndicadorCadastroOrg
	 */
	public Integer getCdIndicadorCadastroOrg() {
		return cdIndicadorCadastroOrg;
	}
	
	/**
	 * Set: cdIndicadorCadastroOrg.
	 *
	 * @param cdIndicadorCadastroOrg the cd indicador cadastro org
	 */
	public void setCdIndicadorCadastroOrg(Integer cdIndicadorCadastroOrg) {
		this.cdIndicadorCadastroOrg = cdIndicadorCadastroOrg;
	}
	
	/**
	 * Get: cdIndicadorCadastroProcd.
	 *
	 * @return cdIndicadorCadastroProcd
	 */
	public Integer getCdIndicadorCadastroProcd() {
		return cdIndicadorCadastroProcd;
	}
	
	/**
	 * Set: cdIndicadorCadastroProcd.
	 *
	 * @param cdIndicadorCadastroProcd the cd indicador cadastro procd
	 */
	public void setCdIndicadorCadastroProcd(Integer cdIndicadorCadastroProcd) {
		this.cdIndicadorCadastroProcd = cdIndicadorCadastroProcd;
	}
	
	/**
	 * Get: cdIndicadorCataoSalario.
	 *
	 * @return cdIndicadorCataoSalario
	 */
	public Integer getCdIndicadorCataoSalario() {
		return cdIndicadorCataoSalario;
	}
	
	/**
	 * Set: cdIndicadorCataoSalario.
	 *
	 * @param cdIndicadorCataoSalario the cd indicador catao salario
	 */
	public void setCdIndicadorCataoSalario(Integer cdIndicadorCataoSalario) {
		this.cdIndicadorCataoSalario = cdIndicadorCataoSalario;
	}
	
	/**
	 * Get: cdIndicadorEconomicoReajuste.
	 *
	 * @return cdIndicadorEconomicoReajuste
	 */
	public Integer getCdIndicadorEconomicoReajuste() {
		return cdIndicadorEconomicoReajuste;
	}
	
	/**
	 * Set: cdIndicadorEconomicoReajuste.
	 *
	 * @param cdIndicadorEconomicoReajuste the cd indicador economico reajuste
	 */
	public void setCdIndicadorEconomicoReajuste(Integer cdIndicadorEconomicoReajuste) {
		this.cdIndicadorEconomicoReajuste = cdIndicadorEconomicoReajuste;
	}
	
	/**
	 * Get: cdIndicadorEmissaoAviso.
	 *
	 * @return cdIndicadorEmissaoAviso
	 */
	public Integer getCdIndicadorEmissaoAviso() {
		return cdIndicadorEmissaoAviso;
	}
	
	/**
	 * Set: cdIndicadorEmissaoAviso.
	 *
	 * @param cdIndicadorEmissaoAviso the cd indicador emissao aviso
	 */
	public void setCdIndicadorEmissaoAviso(Integer cdIndicadorEmissaoAviso) {
		this.cdIndicadorEmissaoAviso = cdIndicadorEmissaoAviso;
	}
	
	/**
	 * Get: cdIndicadorExpiracaoCredito.
	 *
	 * @return cdIndicadorExpiracaoCredito
	 */
	public Integer getCdIndicadorExpiracaoCredito() {
		return cdIndicadorExpiracaoCredito;
	}
	
	/**
	 * Set: cdIndicadorExpiracaoCredito.
	 *
	 * @param cdIndicadorExpiracaoCredito the cd indicador expiracao credito
	 */
	public void setCdIndicadorExpiracaoCredito(Integer cdIndicadorExpiracaoCredito) {
		this.cdIndicadorExpiracaoCredito = cdIndicadorExpiracaoCredito;
	}
	
	/**
	 * Get: cdIndicadorListaDebito.
	 *
	 * @return cdIndicadorListaDebito
	 */
	public Integer getCdIndicadorListaDebito() {
		return cdIndicadorListaDebito;
	}
	
	/**
	 * Set: cdIndicadorListaDebito.
	 *
	 * @param cdIndicadorListaDebito the cd indicador lista debito
	 */
	public void setCdIndicadorListaDebito(Integer cdIndicadorListaDebito) {
		this.cdIndicadorListaDebito = cdIndicadorListaDebito;
	}
	
	/**
	 * Get: cdIndicadorMensagemPerso.
	 *
	 * @return cdIndicadorMensagemPerso
	 */
	public Integer getCdIndicadorMensagemPerso() {
		return cdIndicadorMensagemPerso;
	}
	
	/**
	 * Set: cdIndicadorMensagemPerso.
	 *
	 * @param cdIndicadorMensagemPerso the cd indicador mensagem perso
	 */
	public void setCdIndicadorMensagemPerso(Integer cdIndicadorMensagemPerso) {
		this.cdIndicadorMensagemPerso = cdIndicadorMensagemPerso;
	}
	
	/**
	 * Get: cdIndicadorRetornoInternet.
	 *
	 * @return cdIndicadorRetornoInternet
	 */
	public Integer getCdIndicadorRetornoInternet() {
		return cdIndicadorRetornoInternet;
	}
	
	/**
	 * Set: cdIndicadorRetornoInternet.
	 *
	 * @param cdIndicadorRetornoInternet the cd indicador retorno internet
	 */
	public void setCdIndicadorRetornoInternet(Integer cdIndicadorRetornoInternet) {
		this.cdIndicadorRetornoInternet = cdIndicadorRetornoInternet;
	}
	
	/**
	 * Get: cdIndLancamentoPersonalizado.
	 *
	 * @return cdIndLancamentoPersonalizado
	 */
	public Integer getCdIndLancamentoPersonalizado() {
		return cdIndLancamentoPersonalizado;
	}
	
	/**
	 * Set: cdIndLancamentoPersonalizado.
	 *
	 * @param cdIndLancamentoPersonalizado the cd ind lancamento personalizado
	 */
	public void setCdIndLancamentoPersonalizado(Integer cdIndLancamentoPersonalizado) {
		this.cdIndLancamentoPersonalizado = cdIndLancamentoPersonalizado;
	}
	
	/**
	 * Get: cdManutencaoBaseRecadastro.
	 *
	 * @return cdManutencaoBaseRecadastro
	 */
	public Integer getCdManutencaoBaseRecadastro() {
		return cdManutencaoBaseRecadastro;
	}
	
	/**
	 * Set: cdManutencaoBaseRecadastro.
	 *
	 * @param cdManutencaoBaseRecadastro the cd manutencao base recadastro
	 */
	public void setCdManutencaoBaseRecadastro(Integer cdManutencaoBaseRecadastro) {
		this.cdManutencaoBaseRecadastro = cdManutencaoBaseRecadastro;
	}
	
	/**
	 * Get: cdMeioPagamentoCredito.
	 *
	 * @return cdMeioPagamentoCredito
	 */
	public Integer getCdMeioPagamentoCredito() {
		return cdMeioPagamentoCredito;
	}
	
	/**
	 * Set: cdMeioPagamentoCredito.
	 *
	 * @param cdMeioPagamentoCredito the cd meio pagamento credito
	 */
	public void setCdMeioPagamentoCredito(Integer cdMeioPagamentoCredito) {
		this.cdMeioPagamentoCredito = cdMeioPagamentoCredito;
	}
	
	/**
	 * Get: cdMensagemRecadastroMidia.
	 *
	 * @return cdMensagemRecadastroMidia
	 */
	public Integer getCdMensagemRecadastroMidia() {
		return cdMensagemRecadastroMidia;
	}
	
	/**
	 * Set: cdMensagemRecadastroMidia.
	 *
	 * @param cdMensagemRecadastroMidia the cd mensagem recadastro midia
	 */
	public void setCdMensagemRecadastroMidia(Integer cdMensagemRecadastroMidia) {
		this.cdMensagemRecadastroMidia = cdMensagemRecadastroMidia;
	}
	
	/**
	 * Get: cdMidiaDisponivel.
	 *
	 * @return cdMidiaDisponivel
	 */
	public Integer getCdMidiaDisponivel() {
		return cdMidiaDisponivel;
	}
	
	/**
	 * Set: cdMidiaDisponivel.
	 *
	 * @param cdMidiaDisponivel the cd midia disponivel
	 */
	public void setCdMidiaDisponivel(Integer cdMidiaDisponivel) {
		this.cdMidiaDisponivel = cdMidiaDisponivel;
	}
	
	/**
	 * Get: cdMidiaMensagemRecadastro.
	 *
	 * @return cdMidiaMensagemRecadastro
	 */
	public Integer getCdMidiaMensagemRecadastro() {
		return cdMidiaMensagemRecadastro;
	}
	
	/**
	 * Set: cdMidiaMensagemRecadastro.
	 *
	 * @param cdMidiaMensagemRecadastro the cd midia mensagem recadastro
	 */
	public void setCdMidiaMensagemRecadastro(Integer cdMidiaMensagemRecadastro) {
		this.cdMidiaMensagemRecadastro = cdMidiaMensagemRecadastro;
	}
	
	/**
	 * Get: cdMomentoAvisoRacadastro.
	 *
	 * @return cdMomentoAvisoRacadastro
	 */
	public Integer getCdMomentoAvisoRacadastro() {
		return cdMomentoAvisoRacadastro;
	}
	
	/**
	 * Set: cdMomentoAvisoRacadastro.
	 *
	 * @param cdMomentoAvisoRacadastro the cd momento aviso racadastro
	 */
	public void setCdMomentoAvisoRacadastro(Integer cdMomentoAvisoRacadastro) {
		this.cdMomentoAvisoRacadastro = cdMomentoAvisoRacadastro;
	}
	
	/**
	 * Get: cdMomentoCreditoEfetivacao.
	 *
	 * @return cdMomentoCreditoEfetivacao
	 */
	public Integer getCdMomentoCreditoEfetivacao() {
		return cdMomentoCreditoEfetivacao;
	}
	
	/**
	 * Set: cdMomentoCreditoEfetivacao.
	 *
	 * @param cdMomentoCreditoEfetivacao the cd momento credito efetivacao
	 */
	public void setCdMomentoCreditoEfetivacao(Integer cdMomentoCreditoEfetivacao) {
		this.cdMomentoCreditoEfetivacao = cdMomentoCreditoEfetivacao;
	}
	
	/**
	 * Get: cdMomentoDebitoPagamento.
	 *
	 * @return cdMomentoDebitoPagamento
	 */
	public Integer getCdMomentoDebitoPagamento() {
		return cdMomentoDebitoPagamento;
	}
	
	/**
	 * Set: cdMomentoDebitoPagamento.
	 *
	 * @param cdMomentoDebitoPagamento the cd momento debito pagamento
	 */
	public void setCdMomentoDebitoPagamento(Integer cdMomentoDebitoPagamento) {
		this.cdMomentoDebitoPagamento = cdMomentoDebitoPagamento;
	}
	
	/**
	 * Get: cdMomentoFormularioRecadastro.
	 *
	 * @return cdMomentoFormularioRecadastro
	 */
	public Integer getCdMomentoFormularioRecadastro() {
		return cdMomentoFormularioRecadastro;
	}
	
	/**
	 * Set: cdMomentoFormularioRecadastro.
	 *
	 * @param cdMomentoFormularioRecadastro the cd momento formulario recadastro
	 */
	public void setCdMomentoFormularioRecadastro(
			Integer cdMomentoFormularioRecadastro) {
		this.cdMomentoFormularioRecadastro = cdMomentoFormularioRecadastro;
	}
	
	/**
	 * Get: cdNaturezaOperacaoPagamento.
	 *
	 * @return cdNaturezaOperacaoPagamento
	 */
	public Integer getCdNaturezaOperacaoPagamento() {
		return cdNaturezaOperacaoPagamento;
	}
	
	/**
	 * Set: cdNaturezaOperacaoPagamento.
	 *
	 * @param cdNaturezaOperacaoPagamento the cd natureza operacao pagamento
	 */
	public void setCdNaturezaOperacaoPagamento(Integer cdNaturezaOperacaoPagamento) {
		this.cdNaturezaOperacaoPagamento = cdNaturezaOperacaoPagamento;
	}
	
	/**
	 * Get: cdPercentualIndicadorReajusteTarifa.
	 *
	 * @return cdPercentualIndicadorReajusteTarifa
	 */
	public BigDecimal getCdPercentualIndicadorReajusteTarifa() {
		return cdPercentualIndicadorReajusteTarifa;
	}
	
	/**
	 * Set: cdPercentualIndicadorReajusteTarifa.
	 *
	 * @param cdPercentualIndicadorReajusteTarifa the cd percentual indicador reajuste tarifa
	 */
	public void setCdPercentualIndicadorReajusteTarifa(
			BigDecimal cdPercentualIndicadorReajusteTarifa) {
		this.cdPercentualIndicadorReajusteTarifa = cdPercentualIndicadorReajusteTarifa;
	}
	
	/**
	 * Get: cdPercentualReducaoTarifaCatalogo.
	 *
	 * @return cdPercentualReducaoTarifaCatalogo
	 */
	public BigDecimal getCdPercentualReducaoTarifaCatalogo() {
		return cdPercentualReducaoTarifaCatalogo;
	}
	
	/**
	 * Set: cdPercentualReducaoTarifaCatalogo.
	 *
	 * @param cdPercentualReducaoTarifaCatalogo the cd percentual reducao tarifa catalogo
	 */
	public void setCdPercentualReducaoTarifaCatalogo(
			BigDecimal cdPercentualReducaoTarifaCatalogo) {
		this.cdPercentualReducaoTarifaCatalogo = cdPercentualReducaoTarifaCatalogo;
	}
	
	/**
	 * Get: cdPerdcCobrancaTarifa.
	 *
	 * @return cdPerdcCobrancaTarifa
	 */
	public Integer getCdPerdcCobrancaTarifa() {
		return cdPerdcCobrancaTarifa;
	}
	
	/**
	 * Set: cdPerdcCobrancaTarifa.
	 *
	 * @param cdPerdcCobrancaTarifa the cd perdc cobranca tarifa
	 */
	public void setCdPerdcCobrancaTarifa(Integer cdPerdcCobrancaTarifa) {
		this.cdPerdcCobrancaTarifa = cdPerdcCobrancaTarifa;
	}
	
	/**
	 * Get: cdPerdcComprovante.
	 *
	 * @return cdPerdcComprovante
	 */
	public Integer getCdPerdcComprovante() {
		return cdPerdcComprovante;
	}
	
	/**
	 * Set: cdPerdcComprovante.
	 *
	 * @param cdPerdcComprovante the cd perdc comprovante
	 */
	public void setCdPerdcComprovante(Integer cdPerdcComprovante) {
		this.cdPerdcComprovante = cdPerdcComprovante;
	}
	
	/**
	 * Get: cdPerdcConsultaVeiculo.
	 *
	 * @return cdPerdcConsultaVeiculo
	 */
	public Integer getCdPerdcConsultaVeiculo() {
		return cdPerdcConsultaVeiculo;
	}
	
	/**
	 * Set: cdPerdcConsultaVeiculo.
	 *
	 * @param cdPerdcConsultaVeiculo the cd perdc consulta veiculo
	 */
	public void setCdPerdcConsultaVeiculo(Integer cdPerdcConsultaVeiculo) {
		this.cdPerdcConsultaVeiculo = cdPerdcConsultaVeiculo;
	}
	
	/**
	 * Get: cdPerdcEnvioRemessa.
	 *
	 * @return cdPerdcEnvioRemessa
	 */
	public Integer getCdPerdcEnvioRemessa() {
		return cdPerdcEnvioRemessa;
	}
	
	/**
	 * Set: cdPerdcEnvioRemessa.
	 *
	 * @param cdPerdcEnvioRemessa the cd perdc envio remessa
	 */
	public void setCdPerdcEnvioRemessa(Integer cdPerdcEnvioRemessa) {
		this.cdPerdcEnvioRemessa = cdPerdcEnvioRemessa;
	}
	
	/**
	 * Get: cdPerdcManutencaoProcd.
	 *
	 * @return cdPerdcManutencaoProcd
	 */
	public Integer getCdPerdcManutencaoProcd() {
		return cdPerdcManutencaoProcd;
	}
	
	/**
	 * Set: cdPerdcManutencaoProcd.
	 *
	 * @param cdPerdcManutencaoProcd the cd perdc manutencao procd
	 */
	public void setCdPerdcManutencaoProcd(Integer cdPerdcManutencaoProcd) {
		this.cdPerdcManutencaoProcd = cdPerdcManutencaoProcd;
	}
	
	/**
	 * Get: cdPeriodicidadeAviso.
	 *
	 * @return cdPeriodicidadeAviso
	 */
	public Integer getCdPeriodicidadeAviso() {
		return cdPeriodicidadeAviso;
	}
	
	/**
	 * Set: cdPeriodicidadeAviso.
	 *
	 * @param cdPeriodicidadeAviso the cd periodicidade aviso
	 */
	public void setCdPeriodicidadeAviso(Integer cdPeriodicidadeAviso) {
		this.cdPeriodicidadeAviso = cdPeriodicidadeAviso;
	}
	
	/**
	 * Get: cdPermissaoDebitoOnline.
	 *
	 * @return cdPermissaoDebitoOnline
	 */
	public Integer getCdPermissaoDebitoOnline() {
		return cdPermissaoDebitoOnline;
	}
	
	/**
	 * Set: cdPermissaoDebitoOnline.
	 *
	 * @param cdPermissaoDebitoOnline the cd permissao debito online
	 */
	public void setCdPermissaoDebitoOnline(Integer cdPermissaoDebitoOnline) {
		this.cdPermissaoDebitoOnline = cdPermissaoDebitoOnline;
	}
	
	/**
	 * Get: cdPrincipalEnquaRecadastro.
	 *
	 * @return cdPrincipalEnquaRecadastro
	 */
	public Integer getCdPrincipalEnquaRecadastro() {
		return cdPrincipalEnquaRecadastro;
	}
	
	/**
	 * Set: cdPrincipalEnquaRecadastro.
	 *
	 * @param cdPrincipalEnquaRecadastro the cd principal enqua recadastro
	 */
	public void setCdPrincipalEnquaRecadastro(Integer cdPrincipalEnquaRecadastro) {
		this.cdPrincipalEnquaRecadastro = cdPrincipalEnquaRecadastro;
	}
	
	/**
	 * Get: cdRastreabilidadeNotaFiscal.
	 *
	 * @return cdRastreabilidadeNotaFiscal
	 */
	public Integer getCdRastreabilidadeNotaFiscal() {
		return cdRastreabilidadeNotaFiscal;
	}
	
	/**
	 * Set: cdRastreabilidadeNotaFiscal.
	 *
	 * @param cdRastreabilidadeNotaFiscal the cd rastreabilidade nota fiscal
	 */
	public void setCdRastreabilidadeNotaFiscal(Integer cdRastreabilidadeNotaFiscal) {
		this.cdRastreabilidadeNotaFiscal = cdRastreabilidadeNotaFiscal;
	}
	
	/**
	 * Get: cdRastreabilidadeTituloTerceiro.
	 *
	 * @return cdRastreabilidadeTituloTerceiro
	 */
	public Integer getCdRastreabilidadeTituloTerceiro() {
		return cdRastreabilidadeTituloTerceiro;
	}
	
	/**
	 * Set: cdRastreabilidadeTituloTerceiro.
	 *
	 * @param cdRastreabilidadeTituloTerceiro the cd rastreabilidade titulo terceiro
	 */
	public void setCdRastreabilidadeTituloTerceiro(
			Integer cdRastreabilidadeTituloTerceiro) {
		this.cdRastreabilidadeTituloTerceiro = cdRastreabilidadeTituloTerceiro;
	}
	
	/**
	 * Get: cdRejeicaoLote.
	 *
	 * @return cdRejeicaoLote
	 */
	public Integer getCdRejeicaoLote() {
		return cdRejeicaoLote;
	}
	
	/**
	 * Set: cdRejeicaoLote.
	 *
	 * @param cdRejeicaoLote the cd rejeicao lote
	 */
	public void setCdRejeicaoLote(Integer cdRejeicaoLote) {
		this.cdRejeicaoLote = cdRejeicaoLote;
	}
	
	/**
	 * Get: cdTipoCargaRecadastro.
	 *
	 * @return cdTipoCargaRecadastro
	 */
	public Integer getCdTipoCargaRecadastro() {
		return cdTipoCargaRecadastro;
	}
	
	/**
	 * Set: cdTipoCargaRecadastro.
	 *
	 * @param cdTipoCargaRecadastro the cd tipo carga recadastro
	 */
	public void setCdTipoCargaRecadastro(Integer cdTipoCargaRecadastro) {
		this.cdTipoCargaRecadastro = cdTipoCargaRecadastro;
	}
	
	/**
	 * Get: cdTipoCataoSalario.
	 *
	 * @return cdTipoCataoSalario
	 */
	public Integer getCdTipoCataoSalario() {
		return cdTipoCataoSalario;
	}
	
	/**
	 * Set: cdTipoCataoSalario.
	 *
	 * @param cdTipoCataoSalario the cd tipo catao salario
	 */
	public void setCdTipoCataoSalario(Integer cdTipoCataoSalario) {
		this.cdTipoCataoSalario = cdTipoCataoSalario;
	}
	
	/**
	 * Get: cdTipoConsistenciaLista.
	 *
	 * @return cdTipoConsistenciaLista
	 */
	public Integer getCdTipoConsistenciaLista() {
		return cdTipoConsistenciaLista;
	}
	
	/**
	 * Set: cdTipoConsistenciaLista.
	 *
	 * @param cdTipoConsistenciaLista the cd tipo consistencia lista
	 */
	public void setCdTipoConsistenciaLista(Integer cdTipoConsistenciaLista) {
		this.cdTipoConsistenciaLista = cdTipoConsistenciaLista;
	}
	
	/**
	 * Get: cdTipoConsultaComprovante.
	 *
	 * @return cdTipoConsultaComprovante
	 */
	public Integer getCdTipoConsultaComprovante() {
		return cdTipoConsultaComprovante;
	}
	
	/**
	 * Set: cdTipoConsultaComprovante.
	 *
	 * @param cdTipoConsultaComprovante the cd tipo consulta comprovante
	 */
	public void setCdTipoConsultaComprovante(Integer cdTipoConsultaComprovante) {
		this.cdTipoConsultaComprovante = cdTipoConsultaComprovante;
	}
	
	/**
	 * Get: cdTipoContaFavorecido.
	 *
	 * @return cdTipoContaFavorecido
	 */
	public Integer getCdTipoContaFavorecido() {
		return cdTipoContaFavorecido;
	}
	
	/**
	 * Set: cdTipoContaFavorecido.
	 *
	 * @param cdTipoContaFavorecido the cd tipo conta favorecido
	 */
	public void setCdTipoContaFavorecido(Integer cdTipoContaFavorecido) {
		this.cdTipoContaFavorecido = cdTipoContaFavorecido;
	}
	
	/**
	 * Get: cdTipoDataFloat.
	 *
	 * @return cdTipoDataFloat
	 */
	public Integer getCdTipoDataFloat() {
		return cdTipoDataFloat;
	}
	
	/**
	 * Set: cdTipoDataFloat.
	 *
	 * @param cdTipoDataFloat the cd tipo data float
	 */
	public void setCdTipoDataFloat(Integer cdTipoDataFloat) {
		this.cdTipoDataFloat = cdTipoDataFloat;
	}
	
	/**
	 * Get: cdTipoDivergenciaVeiculo.
	 *
	 * @return cdTipoDivergenciaVeiculo
	 */
	public Integer getCdTipoDivergenciaVeiculo() {
		return cdTipoDivergenciaVeiculo;
	}
	
	/**
	 * Set: cdTipoDivergenciaVeiculo.
	 *
	 * @param cdTipoDivergenciaVeiculo the cd tipo divergencia veiculo
	 */
	public void setCdTipoDivergenciaVeiculo(Integer cdTipoDivergenciaVeiculo) {
		this.cdTipoDivergenciaVeiculo = cdTipoDivergenciaVeiculo;
	}
	
	/**
	 * Get: cdTipoFormacaoLista.
	 *
	 * @return cdTipoFormacaoLista
	 */
	public Integer getCdTipoFormacaoLista() {
		return cdTipoFormacaoLista;
	}
	
	/**
	 * Set: cdTipoFormacaoLista.
	 *
	 * @param cdTipoFormacaoLista the cd tipo formacao lista
	 */
	public void setCdTipoFormacaoLista(Integer cdTipoFormacaoLista) {
		this.cdTipoFormacaoLista = cdTipoFormacaoLista;
	}
	
	/**
	 * Get: cdTipoIdBeneficio.
	 *
	 * @return cdTipoIdBeneficio
	 */
	public Integer getCdTipoIdBeneficio() {
		return cdTipoIdBeneficio;
	}
	
	/**
	 * Set: cdTipoIdBeneficio.
	 *
	 * @param cdTipoIdBeneficio the cd tipo id beneficio
	 */
	public void setCdTipoIdBeneficio(Integer cdTipoIdBeneficio) {
		this.cdTipoIdBeneficio = cdTipoIdBeneficio;
	}
	
	/**
	 * Get: cdTipoIsncricaoFavorecido.
	 *
	 * @return cdTipoIsncricaoFavorecido
	 */
	public Integer getCdTipoIsncricaoFavorecido() {
		return cdTipoIsncricaoFavorecido;
	}
	
	/**
	 * Set: cdTipoIsncricaoFavorecido.
	 *
	 * @param cdTipoIsncricaoFavorecido the cd tipo isncricao favorecido
	 */
	public void setCdTipoIsncricaoFavorecido(Integer cdTipoIsncricaoFavorecido) {
		this.cdTipoIsncricaoFavorecido = cdTipoIsncricaoFavorecido;
	}
	
	/**
	 * Get: cdTipoLayoutArquivo.
	 *
	 * @return cdTipoLayoutArquivo
	 */
	public Integer getCdTipoLayoutArquivo() {
		return cdTipoLayoutArquivo;
	}
	
	/**
	 * Set: cdTipoLayoutArquivo.
	 *
	 * @param cdTipoLayoutArquivo the cd tipo layout arquivo
	 */
	public void setCdTipoLayoutArquivo(Integer cdTipoLayoutArquivo) {
		this.cdTipoLayoutArquivo = cdTipoLayoutArquivo;
	}
	
	/**
	 * Get: cdUsuarioAlteracao.
	 *
	 * @return cdUsuarioAlteracao
	 */
	public String getCdUsuarioAlteracao() {
		return cdUsuarioAlteracao;
	}
	
	/**
	 * Set: cdUsuarioAlteracao.
	 *
	 * @param cdUsuarioAlteracao the cd usuario alteracao
	 */
	public void setCdUsuarioAlteracao(String cdUsuarioAlteracao) {
		this.cdUsuarioAlteracao = cdUsuarioAlteracao;
	}
	
	/**
	 * Get: cdUsuarioExternoAlteracao.
	 *
	 * @return cdUsuarioExternoAlteracao
	 */
	public String getCdUsuarioExternoAlteracao() {
		return cdUsuarioExternoAlteracao;
	}
	
	/**
	 * Set: cdUsuarioExternoAlteracao.
	 *
	 * @param cdUsuarioExternoAlteracao the cd usuario externo alteracao
	 */
	public void setCdUsuarioExternoAlteracao(String cdUsuarioExternoAlteracao) {
		this.cdUsuarioExternoAlteracao = cdUsuarioExternoAlteracao;
	}
	
	/**
	 * Get: cdUsuarioExternoInclusao.
	 *
	 * @return cdUsuarioExternoInclusao
	 */
	public String getCdUsuarioExternoInclusao() {
		return cdUsuarioExternoInclusao;
	}
	
	/**
	 * Set: cdUsuarioExternoInclusao.
	 *
	 * @param cdUsuarioExternoInclusao the cd usuario externo inclusao
	 */
	public void setCdUsuarioExternoInclusao(String cdUsuarioExternoInclusao) {
		this.cdUsuarioExternoInclusao = cdUsuarioExternoInclusao;
	}
	
	/**
	 * Get: cdUsuarioInclusao.
	 *
	 * @return cdUsuarioInclusao
	 */
	public String getCdUsuarioInclusao() {
		return cdUsuarioInclusao;
	}
	
	/**
	 * Set: cdUsuarioInclusao.
	 *
	 * @param cdUsuarioInclusao the cd usuario inclusao
	 */
	public void setCdUsuarioInclusao(String cdUsuarioInclusao) {
		this.cdUsuarioInclusao = cdUsuarioInclusao;
	}
	
	/**
	 * Get: dsAcaoNaoVida.
	 *
	 * @return dsAcaoNaoVida
	 */
	public String getDsAcaoNaoVida() {
		return dsAcaoNaoVida;
	}
	
	/**
	 * Set: dsAcaoNaoVida.
	 *
	 * @param dsAcaoNaoVida the ds acao nao vida
	 */
	public void setDsAcaoNaoVida(String dsAcaoNaoVida) {
		this.dsAcaoNaoVida = dsAcaoNaoVida;
	}
	
	/**
	 * Get: dsAcertoDadoRecadastro.
	 *
	 * @return dsAcertoDadoRecadastro
	 */
	public String getDsAcertoDadoRecadastro() {
		return dsAcertoDadoRecadastro;
	}
	
	/**
	 * Set: dsAcertoDadoRecadastro.
	 *
	 * @param dsAcertoDadoRecadastro the ds acerto dado recadastro
	 */
	public void setDsAcertoDadoRecadastro(String dsAcertoDadoRecadastro) {
		this.dsAcertoDadoRecadastro = dsAcertoDadoRecadastro;
	}
	
	/**
	 * Get: dsAgendaDebitoVeiculo.
	 *
	 * @return dsAgendaDebitoVeiculo
	 */
	public String getDsAgendaDebitoVeiculo() {
		return dsAgendaDebitoVeiculo;
	}
	
	/**
	 * Set: dsAgendaDebitoVeiculo.
	 *
	 * @param dsAgendaDebitoVeiculo the ds agenda debito veiculo
	 */
	public void setDsAgendaDebitoVeiculo(String dsAgendaDebitoVeiculo) {
		this.dsAgendaDebitoVeiculo = dsAgendaDebitoVeiculo;
	}
	
	/**
	 * Get: dsAgendamentoRastFilial.
	 *
	 * @return dsAgendamentoRastFilial
	 */
	public String getDsAgendamentoRastFilial() {
		return dsAgendamentoRastFilial;
	}
	
	/**
	 * Set: dsAgendamentoRastFilial.
	 *
	 * @param dsAgendamentoRastFilial the ds agendamento rast filial
	 */
	public void setDsAgendamentoRastFilial(String dsAgendamentoRastFilial) {
		this.dsAgendamentoRastFilial = dsAgendamentoRastFilial;
	}
	
	/**
	 * Get: dsAgendaPagamentoVencido.
	 *
	 * @return dsAgendaPagamentoVencido
	 */
	public String getDsAgendaPagamentoVencido() {
		return dsAgendaPagamentoVencido;
	}
	
	/**
	 * Set: dsAgendaPagamentoVencido.
	 *
	 * @param dsAgendaPagamentoVencido the ds agenda pagamento vencido
	 */
	public void setDsAgendaPagamentoVencido(String dsAgendaPagamentoVencido) {
		this.dsAgendaPagamentoVencido = dsAgendaPagamentoVencido;
	}
	
	/**
	 * Get: dsAgendaValorMenor.
	 *
	 * @return dsAgendaValorMenor
	 */
	public String getDsAgendaValorMenor() {
		return dsAgendaValorMenor;
	}
	
	/**
	 * Set: dsAgendaValorMenor.
	 *
	 * @param dsAgendaValorMenor the ds agenda valor menor
	 */
	public void setDsAgendaValorMenor(String dsAgendaValorMenor) {
		this.dsAgendaValorMenor = dsAgendaValorMenor;
	}
	
	/**
	 * Get: dsAgrupamentoAviso.
	 *
	 * @return dsAgrupamentoAviso
	 */
	public String getDsAgrupamentoAviso() {
		return dsAgrupamentoAviso;
	}
	
	/**
	 * Set: dsAgrupamentoAviso.
	 *
	 * @param dsAgrupamentoAviso the ds agrupamento aviso
	 */
	public void setDsAgrupamentoAviso(String dsAgrupamentoAviso) {
		this.dsAgrupamentoAviso = dsAgrupamentoAviso;
	}
	
	/**
	 * Get: dsAgrupamentoComprovado.
	 *
	 * @return dsAgrupamentoComprovado
	 */
	public String getDsAgrupamentoComprovado() {
		return dsAgrupamentoComprovado;
	}
	
	/**
	 * Set: dsAgrupamentoComprovado.
	 *
	 * @param dsAgrupamentoComprovado the ds agrupamento comprovado
	 */
	public void setDsAgrupamentoComprovado(String dsAgrupamentoComprovado) {
		this.dsAgrupamentoComprovado = dsAgrupamentoComprovado;
	}
	
	/**
	 * Get: dsAgrupamentoFormularioRecadastro.
	 *
	 * @return dsAgrupamentoFormularioRecadastro
	 */
	public String getDsAgrupamentoFormularioRecadastro() {
		return dsAgrupamentoFormularioRecadastro;
	}
	
	/**
	 * Set: dsAgrupamentoFormularioRecadastro.
	 *
	 * @param dsAgrupamentoFormularioRecadastro the ds agrupamento formulario recadastro
	 */
	public void setDsAgrupamentoFormularioRecadastro(
			String dsAgrupamentoFormularioRecadastro) {
		this.dsAgrupamentoFormularioRecadastro = dsAgrupamentoFormularioRecadastro;
	}
	
	/**
	 * Get: dsAntecRecadastroBeneficio.
	 *
	 * @return dsAntecRecadastroBeneficio
	 */
	public String getDsAntecRecadastroBeneficio() {
		return dsAntecRecadastroBeneficio;
	}
	
	/**
	 * Set: dsAntecRecadastroBeneficio.
	 *
	 * @param dsAntecRecadastroBeneficio the ds antec recadastro beneficio
	 */
	public void setDsAntecRecadastroBeneficio(String dsAntecRecadastroBeneficio) {
		this.dsAntecRecadastroBeneficio = dsAntecRecadastroBeneficio;
	}
	
	/**
	 * Get: dsAreaReservada.
	 *
	 * @return dsAreaReservada
	 */
	public String getDsAreaReservada() {
		return dsAreaReservada;
	}
	
	/**
	 * Set: dsAreaReservada.
	 *
	 * @param dsAreaReservada the ds area reservada
	 */
	public void setDsAreaReservada(String dsAreaReservada) {
		this.dsAreaReservada = dsAreaReservada;
	}
	
	/**
	 * Get: dsAreaResrd.
	 *
	 * @return dsAreaResrd
	 */
	public String getDsAreaResrd() {
		return dsAreaResrd;
	}
	
	/**
	 * Set: dsAreaResrd.
	 *
	 * @param dsAreaResrd the ds area resrd
	 */
	public void setDsAreaResrd(String dsAreaResrd) {
		this.dsAreaResrd = dsAreaResrd;
	}
	
	/**
	 * Get: dsBaseRecadastroBeneficio.
	 *
	 * @return dsBaseRecadastroBeneficio
	 */
	public String getDsBaseRecadastroBeneficio() {
		return dsBaseRecadastroBeneficio;
	}
	
	/**
	 * Set: dsBaseRecadastroBeneficio.
	 *
	 * @param dsBaseRecadastroBeneficio the ds base recadastro beneficio
	 */
	public void setDsBaseRecadastroBeneficio(String dsBaseRecadastroBeneficio) {
		this.dsBaseRecadastroBeneficio = dsBaseRecadastroBeneficio;
	}
	
	/**
	 * Get: dsBloqueioEmissaoPplta.
	 *
	 * @return dsBloqueioEmissaoPplta
	 */
	public String getDsBloqueioEmissaoPplta() {
		return dsBloqueioEmissaoPplta;
	}
	
	/**
	 * Set: dsBloqueioEmissaoPplta.
	 *
	 * @param dsBloqueioEmissaoPplta the ds bloqueio emissao pplta
	 */
	public void setDsBloqueioEmissaoPplta(String dsBloqueioEmissaoPplta) {
		this.dsBloqueioEmissaoPplta = dsBloqueioEmissaoPplta;
	}
	
	/**
	 * Get: dsCanalAlteracao.
	 *
	 * @return dsCanalAlteracao
	 */
	public String getDsCanalAlteracao() {
		return dsCanalAlteracao;
	}
	
	/**
	 * Set: dsCanalAlteracao.
	 *
	 * @param dsCanalAlteracao the ds canal alteracao
	 */
	public void setDsCanalAlteracao(String dsCanalAlteracao) {
		this.dsCanalAlteracao = dsCanalAlteracao;
	}
	
	/**
	 * Get: dsCapituloTituloRegistro.
	 *
	 * @return dsCapituloTituloRegistro
	 */
	public String getDsCapituloTituloRegistro() {
		return dsCapituloTituloRegistro;
	}
	
	/**
	 * Set: dsCapituloTituloRegistro.
	 *
	 * @param dsCapituloTituloRegistro the ds capitulo titulo registro
	 */
	public void setDsCapituloTituloRegistro(String dsCapituloTituloRegistro) {
		this.dsCapituloTituloRegistro = dsCapituloTituloRegistro;
	}
	
	/**
	 * Get: dsCobrancaTarifa.
	 *
	 * @return dsCobrancaTarifa
	 */
	public String getDsCobrancaTarifa() {
		return dsCobrancaTarifa;
	}
	
	/**
	 * Set: dsCobrancaTarifa.
	 *
	 * @param dsCobrancaTarifa the ds cobranca tarifa
	 */
	public void setDsCobrancaTarifa(String dsCobrancaTarifa) {
		this.dsCobrancaTarifa = dsCobrancaTarifa;
	}
	
	/**
	 * Get: dsCodigoFormularioContratoCliente.
	 *
	 * @return dsCodigoFormularioContratoCliente
	 */
	public String getDsCodigoFormularioContratoCliente() {
		return dsCodigoFormularioContratoCliente;
	}
	
	/**
	 * Set: dsCodigoFormularioContratoCliente.
	 *
	 * @param dsCodigoFormularioContratoCliente the ds codigo formulario contrato cliente
	 */
	public void setDsCodigoFormularioContratoCliente(
			String dsCodigoFormularioContratoCliente) {
		this.dsCodigoFormularioContratoCliente = dsCodigoFormularioContratoCliente;
	}
	
	/**
	 * Get: dsConsDebitoVeiculo.
	 *
	 * @return dsConsDebitoVeiculo
	 */
	public String getDsConsDebitoVeiculo() {
		return dsConsDebitoVeiculo;
	}
	
	/**
	 * Set: dsConsDebitoVeiculo.
	 *
	 * @param dsConsDebitoVeiculo the ds cons debito veiculo
	 */
	public void setDsConsDebitoVeiculo(String dsConsDebitoVeiculo) {
		this.dsConsDebitoVeiculo = dsConsDebitoVeiculo;
	}
	
	/**
	 * Get: dsConsEndereco.
	 *
	 * @return dsConsEndereco
	 */
	public String getDsConsEndereco() {
		return dsConsEndereco;
	}
	
	/**
	 * Set: dsConsEndereco.
	 *
	 * @param dsConsEndereco the ds cons endereco
	 */
	public void setDsConsEndereco(String dsConsEndereco) {
		this.dsConsEndereco = dsConsEndereco;
	}
	
	/**
	 * Get: dsConsSaldoPagamento.
	 *
	 * @return dsConsSaldoPagamento
	 */
	public String getDsConsSaldoPagamento() {
		return dsConsSaldoPagamento;
	}
	
	/**
	 * Set: dsConsSaldoPagamento.
	 *
	 * @param dsConsSaldoPagamento the ds cons saldo pagamento
	 */
	public void setDsConsSaldoPagamento(String dsConsSaldoPagamento) {
		this.dsConsSaldoPagamento = dsConsSaldoPagamento;
	}
	
	/**
	 * Get: dsContagemConsSaldo.
	 *
	 * @return dsContagemConsSaldo
	 */
	public String getDsContagemConsSaldo() {
		return dsContagemConsSaldo;
	}
	
	/**
	 * Set: dsContagemConsSaldo.
	 *
	 * @param dsContagemConsSaldo the ds contagem cons saldo
	 */
	public void setDsContagemConsSaldo(String dsContagemConsSaldo) {
		this.dsContagemConsSaldo = dsContagemConsSaldo;
	}
	
	/**
	 * Get: dsContratoContaTransferencia.
	 *
	 * @return dsContratoContaTransferencia
	 */
	public String getDsContratoContaTransferencia() {
		return dsContratoContaTransferencia;
	}
	
	/**
	 * Set: dsContratoContaTransferencia.
	 *
	 * @param dsContratoContaTransferencia the ds contrato conta transferencia
	 */
	public void setDsContratoContaTransferencia(String dsContratoContaTransferencia) {
		this.dsContratoContaTransferencia = dsContratoContaTransferencia;
	}
	
	/**
	 * Get: dsCreditoNaoUtilizado.
	 *
	 * @return dsCreditoNaoUtilizado
	 */
	public String getDsCreditoNaoUtilizado() {
		return dsCreditoNaoUtilizado;
	}
	
	/**
	 * Set: dsCreditoNaoUtilizado.
	 *
	 * @param dsCreditoNaoUtilizado the ds credito nao utilizado
	 */
	public void setDsCreditoNaoUtilizado(String dsCreditoNaoUtilizado) {
		this.dsCreditoNaoUtilizado = dsCreditoNaoUtilizado;
	}
	
	/**
	 * Get: dsCriterioEnquaBeneficio.
	 *
	 * @return dsCriterioEnquaBeneficio
	 */
	public String getDsCriterioEnquaBeneficio() {
		return dsCriterioEnquaBeneficio;
	}
	
	/**
	 * Set: dsCriterioEnquaBeneficio.
	 *
	 * @param dsCriterioEnquaBeneficio the ds criterio enqua beneficio
	 */
	public void setDsCriterioEnquaBeneficio(String dsCriterioEnquaBeneficio) {
		this.dsCriterioEnquaBeneficio = dsCriterioEnquaBeneficio;
	}
	
	/**
	 * Get: dsCriterioEnquaRecadastro.
	 *
	 * @return dsCriterioEnquaRecadastro
	 */
	public String getDsCriterioEnquaRecadastro() {
		return dsCriterioEnquaRecadastro;
	}
	
	/**
	 * Set: dsCriterioEnquaRecadastro.
	 *
	 * @param dsCriterioEnquaRecadastro the ds criterio enqua recadastro
	 */
	public void setDsCriterioEnquaRecadastro(String dsCriterioEnquaRecadastro) {
		this.dsCriterioEnquaRecadastro = dsCriterioEnquaRecadastro;
	}
	
	/**
	 * Get: dsCriterioRastreabilidadeTitulo.
	 *
	 * @return dsCriterioRastreabilidadeTitulo
	 */
	public String getDsCriterioRastreabilidadeTitulo() {
		return dsCriterioRastreabilidadeTitulo;
	}
	
	/**
	 * Set: dsCriterioRastreabilidadeTitulo.
	 *
	 * @param dsCriterioRastreabilidadeTitulo the ds criterio rastreabilidade titulo
	 */
	public void setDsCriterioRastreabilidadeTitulo(
			String dsCriterioRastreabilidadeTitulo) {
		this.dsCriterioRastreabilidadeTitulo = dsCriterioRastreabilidadeTitulo;
	}
	
	/**
	 * Get: dsCtciaEspecieBeneficio.
	 *
	 * @return dsCtciaEspecieBeneficio
	 */
	public String getDsCtciaEspecieBeneficio() {
		return dsCtciaEspecieBeneficio;
	}
	
	/**
	 * Set: dsCtciaEspecieBeneficio.
	 *
	 * @param dsCtciaEspecieBeneficio the ds ctcia especie beneficio
	 */
	public void setDsCtciaEspecieBeneficio(String dsCtciaEspecieBeneficio) {
		this.dsCtciaEspecieBeneficio = dsCtciaEspecieBeneficio;
	}
	
	/**
	 * Get: dsCtciaIdentificacaoBeneficio.
	 *
	 * @return dsCtciaIdentificacaoBeneficio
	 */
	public String getDsCtciaIdentificacaoBeneficio() {
		return dsCtciaIdentificacaoBeneficio;
	}
	
	/**
	 * Set: dsCtciaIdentificacaoBeneficio.
	 *
	 * @param dsCtciaIdentificacaoBeneficio the ds ctcia identificacao beneficio
	 */
	public void setDsCtciaIdentificacaoBeneficio(
			String dsCtciaIdentificacaoBeneficio) {
		this.dsCtciaIdentificacaoBeneficio = dsCtciaIdentificacaoBeneficio;
	}
	
	/**
	 * Get: dsCtciaInscricaoFavorecido.
	 *
	 * @return dsCtciaInscricaoFavorecido
	 */
	public String getDsCtciaInscricaoFavorecido() {
		return dsCtciaInscricaoFavorecido;
	}
	
	/**
	 * Set: dsCtciaInscricaoFavorecido.
	 *
	 * @param dsCtciaInscricaoFavorecido the ds ctcia inscricao favorecido
	 */
	public void setDsCtciaInscricaoFavorecido(String dsCtciaInscricaoFavorecido) {
		this.dsCtciaInscricaoFavorecido = dsCtciaInscricaoFavorecido;
	}
	
	/**
	 * Get: dsCtciaProprietarioVeiculo.
	 *
	 * @return dsCtciaProprietarioVeiculo
	 */
	public String getDsCtciaProprietarioVeiculo() {
		return dsCtciaProprietarioVeiculo;
	}
	
	/**
	 * Set: dsCtciaProprietarioVeiculo.
	 *
	 * @param dsCtciaProprietarioVeiculo the ds ctcia proprietario veiculo
	 */
	public void setDsCtciaProprietarioVeiculo(String dsCtciaProprietarioVeiculo) {
		this.dsCtciaProprietarioVeiculo = dsCtciaProprietarioVeiculo;
	}
	
	/**
	 * Get: dsDestinoAviso.
	 *
	 * @return dsDestinoAviso
	 */
	public String getDsDestinoAviso() {
		return dsDestinoAviso;
	}
	
	/**
	 * Set: dsDestinoAviso.
	 *
	 * @param dsDestinoAviso the ds destino aviso
	 */
	public void setDsDestinoAviso(String dsDestinoAviso) {
		this.dsDestinoAviso = dsDestinoAviso;
	}
	
	/**
	 * Get: dsDestinoComprovante.
	 *
	 * @return dsDestinoComprovante
	 */
	public String getDsDestinoComprovante() {
		return dsDestinoComprovante;
	}
	
	/**
	 * Set: dsDestinoComprovante.
	 *
	 * @param dsDestinoComprovante the ds destino comprovante
	 */
	public void setDsDestinoComprovante(String dsDestinoComprovante) {
		this.dsDestinoComprovante = dsDestinoComprovante;
	}
	
	/**
	 * Get: dsDestinoFormularioRecadastro.
	 *
	 * @return dsDestinoFormularioRecadastro
	 */
	public String getDsDestinoFormularioRecadastro() {
		return dsDestinoFormularioRecadastro;
	}
	
	/**
	 * Set: dsDestinoFormularioRecadastro.
	 *
	 * @param dsDestinoFormularioRecadastro the ds destino formulario recadastro
	 */
	public void setDsDestinoFormularioRecadastro(
			String dsDestinoFormularioRecadastro) {
		this.dsDestinoFormularioRecadastro = dsDestinoFormularioRecadastro;
	}
	
	/**
	 * Get: dsDispzContaCredito.
	 *
	 * @return dsDispzContaCredito
	 */
	public String getDsDispzContaCredito() {
		return dsDispzContaCredito;
	}
	
	/**
	 * Set: dsDispzContaCredito.
	 *
	 * @param dsDispzContaCredito the ds dispz conta credito
	 */
	public void setDsDispzContaCredito(String dsDispzContaCredito) {
		this.dsDispzContaCredito = dsDispzContaCredito;
	}
	
	/**
	 * Get: dsDispzDiversarCrrtt.
	 *
	 * @return dsDispzDiversarCrrtt
	 */
	public String getDsDispzDiversarCrrtt() {
		return dsDispzDiversarCrrtt;
	}
	
	/**
	 * Set: dsDispzDiversarCrrtt.
	 *
	 * @param dsDispzDiversarCrrtt the ds dispz diversar crrtt
	 */
	public void setDsDispzDiversarCrrtt(String dsDispzDiversarCrrtt) {
		this.dsDispzDiversarCrrtt = dsDispzDiversarCrrtt;
	}
	
	/**
	 * Get: dsDispzDiversasNao.
	 *
	 * @return dsDispzDiversasNao
	 */
	public String getDsDispzDiversasNao() {
		return dsDispzDiversasNao;
	}
	
	/**
	 * Set: dsDispzDiversasNao.
	 *
	 * @param dsDispzDiversasNao the ds dispz diversas nao
	 */
	public void setDsDispzDiversasNao(String dsDispzDiversasNao) {
		this.dsDispzDiversasNao = dsDispzDiversasNao;
	}
	
	/**
	 * Get: dsDispzSalarioCrrtt.
	 *
	 * @return dsDispzSalarioCrrtt
	 */
	public String getDsDispzSalarioCrrtt() {
		return dsDispzSalarioCrrtt;
	}
	
	/**
	 * Set: dsDispzSalarioCrrtt.
	 *
	 * @param dsDispzSalarioCrrtt the ds dispz salario crrtt
	 */
	public void setDsDispzSalarioCrrtt(String dsDispzSalarioCrrtt) {
		this.dsDispzSalarioCrrtt = dsDispzSalarioCrrtt;
	}
	
	/**
	 * Get: dsDispzSalarioNao.
	 *
	 * @return dsDispzSalarioNao
	 */
	public String getDsDispzSalarioNao() {
		return dsDispzSalarioNao;
	}
	
	/**
	 * Set: dsDispzSalarioNao.
	 *
	 * @param dsDispzSalarioNao the ds dispz salario nao
	 */
	public void setDsDispzSalarioNao(String dsDispzSalarioNao) {
		this.dsDispzSalarioNao = dsDispzSalarioNao;
	}
	
	/**
	 * Get: dsEnvelopeAberto.
	 *
	 * @return dsEnvelopeAberto
	 */
	public String getDsEnvelopeAberto() {
		return dsEnvelopeAberto;
	}
	
	/**
	 * Set: dsEnvelopeAberto.
	 *
	 * @param dsEnvelopeAberto the ds envelope aberto
	 */
	public void setDsEnvelopeAberto(String dsEnvelopeAberto) {
		this.dsEnvelopeAberto = dsEnvelopeAberto;
	}
	
	/**
	 * Get: dsFavorecidoConsPagamento.
	 *
	 * @return dsFavorecidoConsPagamento
	 */
	public String getDsFavorecidoConsPagamento() {
		return dsFavorecidoConsPagamento;
	}
	
	/**
	 * Set: dsFavorecidoConsPagamento.
	 *
	 * @param dsFavorecidoConsPagamento the ds favorecido cons pagamento
	 */
	public void setDsFavorecidoConsPagamento(String dsFavorecidoConsPagamento) {
		this.dsFavorecidoConsPagamento = dsFavorecidoConsPagamento;
	}
	
	/**
	 * Get: dsFormaExpiracaoCredito.
	 *
	 * @return dsFormaExpiracaoCredito
	 */
	public String getDsFormaExpiracaoCredito() {
		return dsFormaExpiracaoCredito;
	}
	
	/**
	 * Set: dsFormaExpiracaoCredito.
	 *
	 * @param dsFormaExpiracaoCredito the ds forma expiracao credito
	 */
	public void setDsFormaExpiracaoCredito(String dsFormaExpiracaoCredito) {
		this.dsFormaExpiracaoCredito = dsFormaExpiracaoCredito;
	}
	
	/**
	 * Get: dsFormaManutencao.
	 *
	 * @return dsFormaManutencao
	 */
	public String getDsFormaManutencao() {
		return dsFormaManutencao;
	}
	
	/**
	 * Set: dsFormaManutencao.
	 *
	 * @param dsFormaManutencao the ds forma manutencao
	 */
	public void setDsFormaManutencao(String dsFormaManutencao) {
		this.dsFormaManutencao = dsFormaManutencao;
	}
	
	/**
	 * Get: dsFrasePreCadastro.
	 *
	 * @return dsFrasePreCadastro
	 */
	public String getDsFrasePreCadastro() {
		return dsFrasePreCadastro;
	}
	
	/**
	 * Set: dsFrasePreCadastro.
	 *
	 * @param dsFrasePreCadastro the ds frase pre cadastro
	 */
	public void setDsFrasePreCadastro(String dsFrasePreCadastro) {
		this.dsFrasePreCadastro = dsFrasePreCadastro;
	}
	
	/**
	 * Get: dsIndicadorAdesaoSacador.
	 *
	 * @return dsIndicadorAdesaoSacador
	 */
	public String getDsIndicadorAdesaoSacador() {
		return dsIndicadorAdesaoSacador;
	}
	
	/**
	 * Set: dsIndicadorAdesaoSacador.
	 *
	 * @param dsIndicadorAdesaoSacador the ds indicador adesao sacador
	 */
	public void setDsIndicadorAdesaoSacador(String dsIndicadorAdesaoSacador) {
		this.dsIndicadorAdesaoSacador = dsIndicadorAdesaoSacador;
	}
	
	/**
	 * Get: dsIndicadorAgendaTitulo.
	 *
	 * @return dsIndicadorAgendaTitulo
	 */
	public String getDsIndicadorAgendaTitulo() {
		return dsIndicadorAgendaTitulo;
	}
	
	/**
	 * Set: dsIndicadorAgendaTitulo.
	 *
	 * @param dsIndicadorAgendaTitulo the ds indicador agenda titulo
	 */
	public void setDsIndicadorAgendaTitulo(String dsIndicadorAgendaTitulo) {
		this.dsIndicadorAgendaTitulo = dsIndicadorAgendaTitulo;
	}
	
	/**
	 * Get: dsIndicadorAutorizacaoCliente.
	 *
	 * @return dsIndicadorAutorizacaoCliente
	 */
	public String getDsIndicadorAutorizacaoCliente() {
		return dsIndicadorAutorizacaoCliente;
	}
	
	/**
	 * Set: dsIndicadorAutorizacaoCliente.
	 *
	 * @param dsIndicadorAutorizacaoCliente the ds indicador autorizacao cliente
	 */
	public void setDsIndicadorAutorizacaoCliente(
			String dsIndicadorAutorizacaoCliente) {
		this.dsIndicadorAutorizacaoCliente = dsIndicadorAutorizacaoCliente;
	}
	
	/**
	 * Get: dsIndicadorAutorizacaoComplemento.
	 *
	 * @return dsIndicadorAutorizacaoComplemento
	 */
	public String getDsIndicadorAutorizacaoComplemento() {
		return dsIndicadorAutorizacaoComplemento;
	}
	
	/**
	 * Set: dsIndicadorAutorizacaoComplemento.
	 *
	 * @param dsIndicadorAutorizacaoComplemento the ds indicador autorizacao complemento
	 */
	public void setDsIndicadorAutorizacaoComplemento(
			String dsIndicadorAutorizacaoComplemento) {
		this.dsIndicadorAutorizacaoComplemento = dsIndicadorAutorizacaoComplemento;
	}
	
	/**
	 * Get: dsIndicadorBancoPostal.
	 *
	 * @return dsIndicadorBancoPostal
	 */
	public String getDsIndicadorBancoPostal() {
		return dsIndicadorBancoPostal;
	}
	
	/**
	 * Set: dsIndicadorBancoPostal.
	 *
	 * @param dsIndicadorBancoPostal the ds indicador banco postal
	 */
	public void setDsIndicadorBancoPostal(String dsIndicadorBancoPostal) {
		this.dsIndicadorBancoPostal = dsIndicadorBancoPostal;
	}
	
	/**
	 * Get: dsIndicadorCadastroOrg.
	 *
	 * @return dsIndicadorCadastroOrg
	 */
	public String getDsIndicadorCadastroOrg() {
		return dsIndicadorCadastroOrg;
	}
	
	/**
	 * Set: dsIndicadorCadastroOrg.
	 *
	 * @param dsIndicadorCadastroOrg the ds indicador cadastro org
	 */
	public void setDsIndicadorCadastroOrg(String dsIndicadorCadastroOrg) {
		this.dsIndicadorCadastroOrg = dsIndicadorCadastroOrg;
	}
	
	/**
	 * Get: dsIndicadorCadastroProcd.
	 *
	 * @return dsIndicadorCadastroProcd
	 */
	public String getDsIndicadorCadastroProcd() {
		return dsIndicadorCadastroProcd;
	}
	
	/**
	 * Set: dsIndicadorCadastroProcd.
	 *
	 * @param dsIndicadorCadastroProcd the ds indicador cadastro procd
	 */
	public void setDsIndicadorCadastroProcd(String dsIndicadorCadastroProcd) {
		this.dsIndicadorCadastroProcd = dsIndicadorCadastroProcd;
	}
	
	/**
	 * Get: dsIndicadorCataoSalario.
	 *
	 * @return dsIndicadorCataoSalario
	 */
	public String getDsIndicadorCataoSalario() {
		return dsIndicadorCataoSalario;
	}
	
	/**
	 * Set: dsIndicadorCataoSalario.
	 *
	 * @param dsIndicadorCataoSalario the ds indicador catao salario
	 */
	public void setDsIndicadorCataoSalario(String dsIndicadorCataoSalario) {
		this.dsIndicadorCataoSalario = dsIndicadorCataoSalario;
	}
	
	/**
	 * Get: dsIndicadorEconomicoReajuste.
	 *
	 * @return dsIndicadorEconomicoReajuste
	 */
	public String getDsIndicadorEconomicoReajuste() {
		return dsIndicadorEconomicoReajuste;
	}
	
	/**
	 * Set: dsIndicadorEconomicoReajuste.
	 *
	 * @param dsIndicadorEconomicoReajuste the ds indicador economico reajuste
	 */
	public void setDsIndicadorEconomicoReajuste(String dsIndicadorEconomicoReajuste) {
		this.dsIndicadorEconomicoReajuste = dsIndicadorEconomicoReajuste;
	}
	
	/**
	 * Get: dsIndicadorEmissaoAviso.
	 *
	 * @return dsIndicadorEmissaoAviso
	 */
	public String getDsIndicadorEmissaoAviso() {
		return dsIndicadorEmissaoAviso;
	}
	
	/**
	 * Set: dsIndicadorEmissaoAviso.
	 *
	 * @param dsIndicadorEmissaoAviso the ds indicador emissao aviso
	 */
	public void setDsIndicadorEmissaoAviso(String dsIndicadorEmissaoAviso) {
		this.dsIndicadorEmissaoAviso = dsIndicadorEmissaoAviso;
	}
	
	/**
	 * Get: dsIndicadorExpiracaoCredito.
	 *
	 * @return dsIndicadorExpiracaoCredito
	 */
	public String getDsIndicadorExpiracaoCredito() {
		return dsIndicadorExpiracaoCredito;
	}
	
	/**
	 * Set: dsIndicadorExpiracaoCredito.
	 *
	 * @param dsIndicadorExpiracaoCredito the ds indicador expiracao credito
	 */
	public void setDsIndicadorExpiracaoCredito(String dsIndicadorExpiracaoCredito) {
		this.dsIndicadorExpiracaoCredito = dsIndicadorExpiracaoCredito;
	}
	
	/**
	 * Get: dsIndicadorLancamentoPagamento.
	 *
	 * @return dsIndicadorLancamentoPagamento
	 */
	public String getDsIndicadorLancamentoPagamento() {
		return dsIndicadorLancamentoPagamento;
	}
	
	/**
	 * Set: dsIndicadorLancamentoPagamento.
	 *
	 * @param dsIndicadorLancamentoPagamento the ds indicador lancamento pagamento
	 */
	public void setDsIndicadorLancamentoPagamento(
			String dsIndicadorLancamentoPagamento) {
		this.dsIndicadorLancamentoPagamento = dsIndicadorLancamentoPagamento;
	}
	
	/**
	 * Get: dsIndicadorListaDebito.
	 *
	 * @return dsIndicadorListaDebito
	 */
	public String getDsIndicadorListaDebito() {
		return dsIndicadorListaDebito;
	}
	
	/**
	 * Set: dsIndicadorListaDebito.
	 *
	 * @param dsIndicadorListaDebito the ds indicador lista debito
	 */
	public void setDsIndicadorListaDebito(String dsIndicadorListaDebito) {
		this.dsIndicadorListaDebito = dsIndicadorListaDebito;
	}
	
	/**
	 * Get: dsIndicadorMensagemPerso.
	 *
	 * @return dsIndicadorMensagemPerso
	 */
	public String getDsIndicadorMensagemPerso() {
		return dsIndicadorMensagemPerso;
	}
	
	/**
	 * Set: dsIndicadorMensagemPerso.
	 *
	 * @param dsIndicadorMensagemPerso the ds indicador mensagem perso
	 */
	public void setDsIndicadorMensagemPerso(String dsIndicadorMensagemPerso) {
		this.dsIndicadorMensagemPerso = dsIndicadorMensagemPerso;
	}
	
	/**
	 * Get: dsIndicadorRetornoInternet.
	 *
	 * @return dsIndicadorRetornoInternet
	 */
	public String getDsIndicadorRetornoInternet() {
		return dsIndicadorRetornoInternet;
	}
	
	/**
	 * Set: dsIndicadorRetornoInternet.
	 *
	 * @param dsIndicadorRetornoInternet the ds indicador retorno internet
	 */
	public void setDsIndicadorRetornoInternet(String dsIndicadorRetornoInternet) {
		this.dsIndicadorRetornoInternet = dsIndicadorRetornoInternet;
	}
	
	/**
	 * Get: dsIndLancamentoPersonalizado.
	 *
	 * @return dsIndLancamentoPersonalizado
	 */
	public String getDsIndLancamentoPersonalizado() {
		return dsIndLancamentoPersonalizado;
	}
	
	/**
	 * Set: dsIndLancamentoPersonalizado.
	 *
	 * @param dsIndLancamentoPersonalizado the ds ind lancamento personalizado
	 */
	public void setDsIndLancamentoPersonalizado(String dsIndLancamentoPersonalizado) {
		this.dsIndLancamentoPersonalizado = dsIndLancamentoPersonalizado;
	}
	
	/**
	 * Get: dsLancamentoFuturoCredito.
	 *
	 * @return dsLancamentoFuturoCredito
	 */
	public String getDsLancamentoFuturoCredito() {
		return dsLancamentoFuturoCredito;
	}
	
	/**
	 * Set: dsLancamentoFuturoCredito.
	 *
	 * @param dsLancamentoFuturoCredito the ds lancamento futuro credito
	 */
	public void setDsLancamentoFuturoCredito(String dsLancamentoFuturoCredito) {
		this.dsLancamentoFuturoCredito = dsLancamentoFuturoCredito;
	}
	
	/**
	 * Get: dsLancamentoFuturoDebito.
	 *
	 * @return dsLancamentoFuturoDebito
	 */
	public String getDsLancamentoFuturoDebito() {
		return dsLancamentoFuturoDebito;
	}
	
	/**
	 * Set: dsLancamentoFuturoDebito.
	 *
	 * @param dsLancamentoFuturoDebito the ds lancamento futuro debito
	 */
	public void setDsLancamentoFuturoDebito(String dsLancamentoFuturoDebito) {
		this.dsLancamentoFuturoDebito = dsLancamentoFuturoDebito;
	}
	
	/**
	 * Get: dsLiberacaoLoteProcesso.
	 *
	 * @return dsLiberacaoLoteProcesso
	 */
	public String getDsLiberacaoLoteProcesso() {
		return dsLiberacaoLoteProcesso;
	}
	
	/**
	 * Set: dsLiberacaoLoteProcesso.
	 *
	 * @param dsLiberacaoLoteProcesso the ds liberacao lote processo
	 */
	public void setDsLiberacaoLoteProcesso(String dsLiberacaoLoteProcesso) {
		this.dsLiberacaoLoteProcesso = dsLiberacaoLoteProcesso;
	}
	
	/**
	 * Get: dsManutencaoBaseRecadastro.
	 *
	 * @return dsManutencaoBaseRecadastro
	 */
	public String getDsManutencaoBaseRecadastro() {
		return dsManutencaoBaseRecadastro;
	}
	
	/**
	 * Set: dsManutencaoBaseRecadastro.
	 *
	 * @param dsManutencaoBaseRecadastro the ds manutencao base recadastro
	 */
	public void setDsManutencaoBaseRecadastro(String dsManutencaoBaseRecadastro) {
		this.dsManutencaoBaseRecadastro = dsManutencaoBaseRecadastro;
	}
	
	/**
	 * Get: dsMeioPagamentoCredito.
	 *
	 * @return dsMeioPagamentoCredito
	 */
	public String getDsMeioPagamentoCredito() {
		return dsMeioPagamentoCredito;
	}
	
	/**
	 * Set: dsMeioPagamentoCredito.
	 *
	 * @param dsMeioPagamentoCredito the ds meio pagamento credito
	 */
	public void setDsMeioPagamentoCredito(String dsMeioPagamentoCredito) {
		this.dsMeioPagamentoCredito = dsMeioPagamentoCredito;
	}
	
	/**
	 * Get: dsMensagemRecadastroMidia.
	 *
	 * @return dsMensagemRecadastroMidia
	 */
	public String getDsMensagemRecadastroMidia() {
		return dsMensagemRecadastroMidia;
	}
	
	/**
	 * Set: dsMensagemRecadastroMidia.
	 *
	 * @param dsMensagemRecadastroMidia the ds mensagem recadastro midia
	 */
	public void setDsMensagemRecadastroMidia(String dsMensagemRecadastroMidia) {
		this.dsMensagemRecadastroMidia = dsMensagemRecadastroMidia;
	}
	
	/**
	 * Get: dsMidiaDisponivel.
	 *
	 * @return dsMidiaDisponivel
	 */
	public String getDsMidiaDisponivel() {
		return dsMidiaDisponivel;
	}
	
	/**
	 * Set: dsMidiaDisponivel.
	 *
	 * @param dsMidiaDisponivel the ds midia disponivel
	 */
	public void setDsMidiaDisponivel(String dsMidiaDisponivel) {
		this.dsMidiaDisponivel = dsMidiaDisponivel;
	}
	
	/**
	 * Get: dsMidiaMensagemRecadastro.
	 *
	 * @return dsMidiaMensagemRecadastro
	 */
	public String getDsMidiaMensagemRecadastro() {
		return dsMidiaMensagemRecadastro;
	}
	
	/**
	 * Set: dsMidiaMensagemRecadastro.
	 *
	 * @param dsMidiaMensagemRecadastro the ds midia mensagem recadastro
	 */
	public void setDsMidiaMensagemRecadastro(String dsMidiaMensagemRecadastro) {
		this.dsMidiaMensagemRecadastro = dsMidiaMensagemRecadastro;
	}
	
	/**
	 * Get: dsMomentoAvisoRacadastro.
	 *
	 * @return dsMomentoAvisoRacadastro
	 */
	public String getDsMomentoAvisoRacadastro() {
		return dsMomentoAvisoRacadastro;
	}
	
	/**
	 * Set: dsMomentoAvisoRacadastro.
	 *
	 * @param dsMomentoAvisoRacadastro the ds momento aviso racadastro
	 */
	public void setDsMomentoAvisoRacadastro(String dsMomentoAvisoRacadastro) {
		this.dsMomentoAvisoRacadastro = dsMomentoAvisoRacadastro;
	}
	
	/**
	 * Get: dsMomentoCreditoEfetivacao.
	 *
	 * @return dsMomentoCreditoEfetivacao
	 */
	public String getDsMomentoCreditoEfetivacao() {
		return dsMomentoCreditoEfetivacao;
	}
	
	/**
	 * Set: dsMomentoCreditoEfetivacao.
	 *
	 * @param dsMomentoCreditoEfetivacao the ds momento credito efetivacao
	 */
	public void setDsMomentoCreditoEfetivacao(String dsMomentoCreditoEfetivacao) {
		this.dsMomentoCreditoEfetivacao = dsMomentoCreditoEfetivacao;
	}
	
	/**
	 * Get: dsMomentoDebitoPagamento.
	 *
	 * @return dsMomentoDebitoPagamento
	 */
	public String getDsMomentoDebitoPagamento() {
		return dsMomentoDebitoPagamento;
	}
	
	/**
	 * Set: dsMomentoDebitoPagamento.
	 *
	 * @param dsMomentoDebitoPagamento the ds momento debito pagamento
	 */
	public void setDsMomentoDebitoPagamento(String dsMomentoDebitoPagamento) {
		this.dsMomentoDebitoPagamento = dsMomentoDebitoPagamento;
	}
	
	/**
	 * Get: dsMomentoFormularioRecadastro.
	 *
	 * @return dsMomentoFormularioRecadastro
	 */
	public String getDsMomentoFormularioRecadastro() {
		return dsMomentoFormularioRecadastro;
	}
	
	/**
	 * Set: dsMomentoFormularioRecadastro.
	 *
	 * @param dsMomentoFormularioRecadastro the ds momento formulario recadastro
	 */
	public void setDsMomentoFormularioRecadastro(
			String dsMomentoFormularioRecadastro) {
		this.dsMomentoFormularioRecadastro = dsMomentoFormularioRecadastro;
	}
	
	/**
	 * Get: dsMomentoProcessamentoPagamento.
	 *
	 * @return dsMomentoProcessamentoPagamento
	 */
	public String getDsMomentoProcessamentoPagamento() {
		return dsMomentoProcessamentoPagamento;
	}
	
	/**
	 * Set: dsMomentoProcessamentoPagamento.
	 *
	 * @param dsMomentoProcessamentoPagamento the ds momento processamento pagamento
	 */
	public void setDsMomentoProcessamentoPagamento(
			String dsMomentoProcessamentoPagamento) {
		this.dsMomentoProcessamentoPagamento = dsMomentoProcessamentoPagamento;
	}
	
	/**
	 * Get: dsNaturezaOperacaoPagamento.
	 *
	 * @return dsNaturezaOperacaoPagamento
	 */
	public String getDsNaturezaOperacaoPagamento() {
		return dsNaturezaOperacaoPagamento;
	}
	
	/**
	 * Set: dsNaturezaOperacaoPagamento.
	 *
	 * @param dsNaturezaOperacaoPagamento the ds natureza operacao pagamento
	 */
	public void setDsNaturezaOperacaoPagamento(String dsNaturezaOperacaoPagamento) {
		this.dsNaturezaOperacaoPagamento = dsNaturezaOperacaoPagamento;
	}
	
	/**
	 * Get: dsPagamentoNaoUtilizado.
	 *
	 * @return dsPagamentoNaoUtilizado
	 */
	public String getDsPagamentoNaoUtilizado() {
		return dsPagamentoNaoUtilizado;
	}
	
	/**
	 * Set: dsPagamentoNaoUtilizado.
	 *
	 * @param dsPagamentoNaoUtilizado the ds pagamento nao utilizado
	 */
	public void setDsPagamentoNaoUtilizado(String dsPagamentoNaoUtilizado) {
		this.dsPagamentoNaoUtilizado = dsPagamentoNaoUtilizado;
	}
	
	/**
	 * Get: dsPerdcCobrancaTarifa.
	 *
	 * @return dsPerdcCobrancaTarifa
	 */
	public String getDsPerdcCobrancaTarifa() {
		return dsPerdcCobrancaTarifa;
	}
	
	/**
	 * Set: dsPerdcCobrancaTarifa.
	 *
	 * @param dsPerdcCobrancaTarifa the ds perdc cobranca tarifa
	 */
	public void setDsPerdcCobrancaTarifa(String dsPerdcCobrancaTarifa) {
		this.dsPerdcCobrancaTarifa = dsPerdcCobrancaTarifa;
	}
	
	/**
	 * Get: dsPerdcComprovante.
	 *
	 * @return dsPerdcComprovante
	 */
	public String getDsPerdcComprovante() {
		return dsPerdcComprovante;
	}
	
	/**
	 * Set: dsPerdcComprovante.
	 *
	 * @param dsPerdcComprovante the ds perdc comprovante
	 */
	public void setDsPerdcComprovante(String dsPerdcComprovante) {
		this.dsPerdcComprovante = dsPerdcComprovante;
	}
	
	/**
	 * Get: dsPerdcConsultaVeiculo.
	 *
	 * @return dsPerdcConsultaVeiculo
	 */
	public String getDsPerdcConsultaVeiculo() {
		return dsPerdcConsultaVeiculo;
	}
	
	/**
	 * Set: dsPerdcConsultaVeiculo.
	 *
	 * @param dsPerdcConsultaVeiculo the ds perdc consulta veiculo
	 */
	public void setDsPerdcConsultaVeiculo(String dsPerdcConsultaVeiculo) {
		this.dsPerdcConsultaVeiculo = dsPerdcConsultaVeiculo;
	}
	
	/**
	 * Get: dsPerdcEnvioRemessa.
	 *
	 * @return dsPerdcEnvioRemessa
	 */
	public String getDsPerdcEnvioRemessa() {
		return dsPerdcEnvioRemessa;
	}
	
	/**
	 * Set: dsPerdcEnvioRemessa.
	 *
	 * @param dsPerdcEnvioRemessa the ds perdc envio remessa
	 */
	public void setDsPerdcEnvioRemessa(String dsPerdcEnvioRemessa) {
		this.dsPerdcEnvioRemessa = dsPerdcEnvioRemessa;
	}
	
	/**
	 * Get: dsPerdcManutencaoProcd.
	 *
	 * @return dsPerdcManutencaoProcd
	 */
	public String getDsPerdcManutencaoProcd() {
		return dsPerdcManutencaoProcd;
	}
	
	/**
	 * Set: dsPerdcManutencaoProcd.
	 *
	 * @param dsPerdcManutencaoProcd the ds perdc manutencao procd
	 */
	public void setDsPerdcManutencaoProcd(String dsPerdcManutencaoProcd) {
		this.dsPerdcManutencaoProcd = dsPerdcManutencaoProcd;
	}
	
	/**
	 * Get: dsPeriodicidadeAviso.
	 *
	 * @return dsPeriodicidadeAviso
	 */
	public String getDsPeriodicidadeAviso() {
		return dsPeriodicidadeAviso;
	}
	
	/**
	 * Set: dsPeriodicidadeAviso.
	 *
	 * @param dsPeriodicidadeAviso the ds periodicidade aviso
	 */
	public void setDsPeriodicidadeAviso(String dsPeriodicidadeAviso) {
		this.dsPeriodicidadeAviso = dsPeriodicidadeAviso;
	}
	
	/**
	 * Get: dsPermissaoDebitoOnline.
	 *
	 * @return dsPermissaoDebitoOnline
	 */
	public String getDsPermissaoDebitoOnline() {
		return dsPermissaoDebitoOnline;
	}
	
	/**
	 * Set: dsPermissaoDebitoOnline.
	 *
	 * @param dsPermissaoDebitoOnline the ds permissao debito online
	 */
	public void setDsPermissaoDebitoOnline(String dsPermissaoDebitoOnline) {
		this.dsPermissaoDebitoOnline = dsPermissaoDebitoOnline;
	}
	
	/**
	 * Get: dsPrincipalEnquaRecadastro.
	 *
	 * @return dsPrincipalEnquaRecadastro
	 */
	public String getDsPrincipalEnquaRecadastro() {
		return dsPrincipalEnquaRecadastro;
	}
	
	/**
	 * Set: dsPrincipalEnquaRecadastro.
	 *
	 * @param dsPrincipalEnquaRecadastro the ds principal enqua recadastro
	 */
	public void setDsPrincipalEnquaRecadastro(String dsPrincipalEnquaRecadastro) {
		this.dsPrincipalEnquaRecadastro = dsPrincipalEnquaRecadastro;
	}
	
	/**
	 * Get: dsPrioridadeEfetivacaoPagamento.
	 *
	 * @return dsPrioridadeEfetivacaoPagamento
	 */
	public String getDsPrioridadeEfetivacaoPagamento() {
		return dsPrioridadeEfetivacaoPagamento;
	}
	
	/**
	 * Set: dsPrioridadeEfetivacaoPagamento.
	 *
	 * @param dsPrioridadeEfetivacaoPagamento the ds prioridade efetivacao pagamento
	 */
	public void setDsPrioridadeEfetivacaoPagamento(
			String dsPrioridadeEfetivacaoPagamento) {
		this.dsPrioridadeEfetivacaoPagamento = dsPrioridadeEfetivacaoPagamento;
	}
	
	/**
	 * Get: dsRastreabilidadeNotaFiscal.
	 *
	 * @return dsRastreabilidadeNotaFiscal
	 */
	public String getDsRastreabilidadeNotaFiscal() {
		return dsRastreabilidadeNotaFiscal;
	}
	
	/**
	 * Set: dsRastreabilidadeNotaFiscal.
	 *
	 * @param dsRastreabilidadeNotaFiscal the ds rastreabilidade nota fiscal
	 */
	public void setDsRastreabilidadeNotaFiscal(String dsRastreabilidadeNotaFiscal) {
		this.dsRastreabilidadeNotaFiscal = dsRastreabilidadeNotaFiscal;
	}
	
	/**
	 * Get: dsRastreabilidadeTituloTerceiro.
	 *
	 * @return dsRastreabilidadeTituloTerceiro
	 */
	public String getDsRastreabilidadeTituloTerceiro() {
		return dsRastreabilidadeTituloTerceiro;
	}
	
	/**
	 * Set: dsRastreabilidadeTituloTerceiro.
	 *
	 * @param dsRastreabilidadeTituloTerceiro the ds rastreabilidade titulo terceiro
	 */
	public void setDsRastreabilidadeTituloTerceiro(
			String dsRastreabilidadeTituloTerceiro) {
		this.dsRastreabilidadeTituloTerceiro = dsRastreabilidadeTituloTerceiro;
	}
	
	/**
	 * Get: dsRejeicaoAgendaLote.
	 *
	 * @return dsRejeicaoAgendaLote
	 */
	public String getDsRejeicaoAgendaLote() {
		return dsRejeicaoAgendaLote;
	}
	
	/**
	 * Set: dsRejeicaoAgendaLote.
	 *
	 * @param dsRejeicaoAgendaLote the ds rejeicao agenda lote
	 */
	public void setDsRejeicaoAgendaLote(String dsRejeicaoAgendaLote) {
		this.dsRejeicaoAgendaLote = dsRejeicaoAgendaLote;
	}
	
	/**
	 * Get: dsRejeicaoEfetivacaoLote.
	 *
	 * @return dsRejeicaoEfetivacaoLote
	 */
	public String getDsRejeicaoEfetivacaoLote() {
		return dsRejeicaoEfetivacaoLote;
	}
	
	/**
	 * Set: dsRejeicaoEfetivacaoLote.
	 *
	 * @param dsRejeicaoEfetivacaoLote the ds rejeicao efetivacao lote
	 */
	public void setDsRejeicaoEfetivacaoLote(String dsRejeicaoEfetivacaoLote) {
		this.dsRejeicaoEfetivacaoLote = dsRejeicaoEfetivacaoLote;
	}
	
	/**
	 * Get: dsRejeicaoLote.
	 *
	 * @return dsRejeicaoLote
	 */
	public String getDsRejeicaoLote() {
		return dsRejeicaoLote;
	}
	
	/**
	 * Set: dsRejeicaoLote.
	 *
	 * @param dsRejeicaoLote the ds rejeicao lote
	 */
	public void setDsRejeicaoLote(String dsRejeicaoLote) {
		this.dsRejeicaoLote = dsRejeicaoLote;
	}
	
	/**
	 * Get: dsTipoCargaRecadastro.
	 *
	 * @return dsTipoCargaRecadastro
	 */
	public String getDsTipoCargaRecadastro() {
		return dsTipoCargaRecadastro;
	}
	
	/**
	 * Set: dsTipoCargaRecadastro.
	 *
	 * @param dsTipoCargaRecadastro the ds tipo carga recadastro
	 */
	public void setDsTipoCargaRecadastro(String dsTipoCargaRecadastro) {
		this.dsTipoCargaRecadastro = dsTipoCargaRecadastro;
	}
	
	/**
	 * Get: dsTipoCataoSalario.
	 *
	 * @return dsTipoCataoSalario
	 */
	public String getDsTipoCataoSalario() {
		return dsTipoCataoSalario;
	}
	
	/**
	 * Set: dsTipoCataoSalario.
	 *
	 * @param dsTipoCataoSalario the ds tipo catao salario
	 */
	public void setDsTipoCataoSalario(String dsTipoCataoSalario) {
		this.dsTipoCataoSalario = dsTipoCataoSalario;
	}
	
	/**
	 * Get: dsTipoConsistenciaFavorecido.
	 *
	 * @return dsTipoConsistenciaFavorecido
	 */
	public String getDsTipoConsistenciaFavorecido() {
		return dsTipoConsistenciaFavorecido;
	}
	
	/**
	 * Set: dsTipoConsistenciaFavorecido.
	 *
	 * @param dsTipoConsistenciaFavorecido the ds tipo consistencia favorecido
	 */
	public void setDsTipoConsistenciaFavorecido(String dsTipoConsistenciaFavorecido) {
		this.dsTipoConsistenciaFavorecido = dsTipoConsistenciaFavorecido;
	}
	
	/**
	 * Get: dsTipoConsistenciaLista.
	 *
	 * @return dsTipoConsistenciaLista
	 */
	public String getDsTipoConsistenciaLista() {
		return dsTipoConsistenciaLista;
	}
	
	/**
	 * Set: dsTipoConsistenciaLista.
	 *
	 * @param dsTipoConsistenciaLista the ds tipo consistencia lista
	 */
	public void setDsTipoConsistenciaLista(String dsTipoConsistenciaLista) {
		this.dsTipoConsistenciaLista = dsTipoConsistenciaLista;
	}
	
	/**
	 * Get: dsTipoConsolidacaoComprovante.
	 *
	 * @return dsTipoConsolidacaoComprovante
	 */
	public String getDsTipoConsolidacaoComprovante() {
		return dsTipoConsolidacaoComprovante;
	}
	
	/**
	 * Set: dsTipoConsolidacaoComprovante.
	 *
	 * @param dsTipoConsolidacaoComprovante the ds tipo consolidacao comprovante
	 */
	public void setDsTipoConsolidacaoComprovante(
			String dsTipoConsolidacaoComprovante) {
		this.dsTipoConsolidacaoComprovante = dsTipoConsolidacaoComprovante;
	}
	
	/**
	 * Get: dsTipoDataFloat.
	 *
	 * @return dsTipoDataFloat
	 */
	public String getDsTipoDataFloat() {
		return dsTipoDataFloat;
	}
	
	/**
	 * Set: dsTipoDataFloat.
	 *
	 * @param dsTipoDataFloat the ds tipo data float
	 */
	public void setDsTipoDataFloat(String dsTipoDataFloat) {
		this.dsTipoDataFloat = dsTipoDataFloat;
	}
	
	/**
	 * Get: dsTipoDivergenciaVeiculo.
	 *
	 * @return dsTipoDivergenciaVeiculo
	 */
	public String getDsTipoDivergenciaVeiculo() {
		return dsTipoDivergenciaVeiculo;
	}
	
	/**
	 * Set: dsTipoDivergenciaVeiculo.
	 *
	 * @param dsTipoDivergenciaVeiculo the ds tipo divergencia veiculo
	 */
	public void setDsTipoDivergenciaVeiculo(String dsTipoDivergenciaVeiculo) {
		this.dsTipoDivergenciaVeiculo = dsTipoDivergenciaVeiculo;
	}
	
	/**
	 * Get: dsTipoFormacaoLista.
	 *
	 * @return dsTipoFormacaoLista
	 */
	public String getDsTipoFormacaoLista() {
		return dsTipoFormacaoLista;
	}
	
	/**
	 * Set: dsTipoFormacaoLista.
	 *
	 * @param dsTipoFormacaoLista the ds tipo formacao lista
	 */
	public void setDsTipoFormacaoLista(String dsTipoFormacaoLista) {
		this.dsTipoFormacaoLista = dsTipoFormacaoLista;
	}
	
	/**
	 * Get: dsTipoIdBeneficio.
	 *
	 * @return dsTipoIdBeneficio
	 */
	public String getDsTipoIdBeneficio() {
		return dsTipoIdBeneficio;
	}
	
	/**
	 * Set: dsTipoIdBeneficio.
	 *
	 * @param dsTipoIdBeneficio the ds tipo id beneficio
	 */
	public void setDsTipoIdBeneficio(String dsTipoIdBeneficio) {
		this.dsTipoIdBeneficio = dsTipoIdBeneficio;
	}
	
	/**
	 * Get: dsTipoIscricaoFavorecido.
	 *
	 * @return dsTipoIscricaoFavorecido
	 */
	public String getDsTipoIscricaoFavorecido() {
		return dsTipoIscricaoFavorecido;
	}
	
	/**
	 * Set: dsTipoIscricaoFavorecido.
	 *
	 * @param dsTipoIscricaoFavorecido the ds tipo iscricao favorecido
	 */
	public void setDsTipoIscricaoFavorecido(String dsTipoIscricaoFavorecido) {
		this.dsTipoIscricaoFavorecido = dsTipoIscricaoFavorecido;
	}
	
	/**
	 * Get: dsTipoLayoutArquivo.
	 *
	 * @return dsTipoLayoutArquivo
	 */
	public String getDsTipoLayoutArquivo() {
		return dsTipoLayoutArquivo;
	}
	
	/**
	 * Set: dsTipoLayoutArquivo.
	 *
	 * @param dsTipoLayoutArquivo the ds tipo layout arquivo
	 */
	public void setDsTipoLayoutArquivo(String dsTipoLayoutArquivo) {
		this.dsTipoLayoutArquivo = dsTipoLayoutArquivo;
	}
	
	/**
	 * Get: dsUtilizacaoFavorecidoControle.
	 *
	 * @return dsUtilizacaoFavorecidoControle
	 */
	public String getDsUtilizacaoFavorecidoControle() {
		return dsUtilizacaoFavorecidoControle;
	}
	
	/**
	 * Set: dsUtilizacaoFavorecidoControle.
	 *
	 * @param dsUtilizacaoFavorecidoControle the ds utilizacao favorecido controle
	 */
	public void setDsUtilizacaoFavorecidoControle(
			String dsUtilizacaoFavorecidoControle) {
		this.dsUtilizacaoFavorecidoControle = dsUtilizacaoFavorecidoControle;
	}
	
	/**
	 * Get: dsValidacaoNomeFavorecido.
	 *
	 * @return dsValidacaoNomeFavorecido
	 */
	public String getDsValidacaoNomeFavorecido() {
		return dsValidacaoNomeFavorecido;
	}
	
	/**
	 * Set: dsValidacaoNomeFavorecido.
	 *
	 * @param dsValidacaoNomeFavorecido the ds validacao nome favorecido
	 */
	public void setDsValidacaoNomeFavorecido(String dsValidacaoNomeFavorecido) {
		this.dsValidacaoNomeFavorecido = dsValidacaoNomeFavorecido;
	}
	
	/**
	 * Get: dtEnquaContaSalario.
	 *
	 * @return dtEnquaContaSalario
	 */
	public String getDtEnquaContaSalario() {
		return dtEnquaContaSalario;
	}
	
	/**
	 * Set: dtEnquaContaSalario.
	 *
	 * @param dtEnquaContaSalario the dt enqua conta salario
	 */
	public void setDtEnquaContaSalario(String dtEnquaContaSalario) {
		this.dtEnquaContaSalario = dtEnquaContaSalario;
	}
	
	/**
	 * Get: dtFimAcertoRecadastro.
	 *
	 * @return dtFimAcertoRecadastro
	 */
	public String getDtFimAcertoRecadastro() {
		return dtFimAcertoRecadastro;
	}
	
	/**
	 * Set: dtFimAcertoRecadastro.
	 *
	 * @param dtFimAcertoRecadastro the dt fim acerto recadastro
	 */
	public void setDtFimAcertoRecadastro(String dtFimAcertoRecadastro) {
		this.dtFimAcertoRecadastro = dtFimAcertoRecadastro;
	}
	
	/**
	 * Get: dtFimRecadastroBeneficio.
	 *
	 * @return dtFimRecadastroBeneficio
	 */
	public String getDtFimRecadastroBeneficio() {
		return dtFimRecadastroBeneficio;
	}
	
	/**
	 * Set: dtFimRecadastroBeneficio.
	 *
	 * @param dtFimRecadastroBeneficio the dt fim recadastro beneficio
	 */
	public void setDtFimRecadastroBeneficio(String dtFimRecadastroBeneficio) {
		this.dtFimRecadastroBeneficio = dtFimRecadastroBeneficio;
	}
	
	/**
	 * Get: dtinicioAcertoRecadastro.
	 *
	 * @return dtinicioAcertoRecadastro
	 */
	public String getDtinicioAcertoRecadastro() {
		return dtinicioAcertoRecadastro;
	}
	
	/**
	 * Set: dtinicioAcertoRecadastro.
	 *
	 * @param dtinicioAcertoRecadastro the dtinicio acerto recadastro
	 */
	public void setDtinicioAcertoRecadastro(String dtinicioAcertoRecadastro) {
		this.dtinicioAcertoRecadastro = dtinicioAcertoRecadastro;
	}
	
	/**
	 * Get: dtInicioBloqueioPplta.
	 *
	 * @return dtInicioBloqueioPplta
	 */
	public String getDtInicioBloqueioPplta() {
		return dtInicioBloqueioPplta;
	}
	
	/**
	 * Set: dtInicioBloqueioPplta.
	 *
	 * @param dtInicioBloqueioPplta the dt inicio bloqueio pplta
	 */
	public void setDtInicioBloqueioPplta(String dtInicioBloqueioPplta) {
		this.dtInicioBloqueioPplta = dtInicioBloqueioPplta;
	}
	
	/**
	 * Get: dtInicioRastreabilidadeTitulo.
	 *
	 * @return dtInicioRastreabilidadeTitulo
	 */
	public String getDtInicioRastreabilidadeTitulo() {
		return dtInicioRastreabilidadeTitulo;
	}
	
	/**
	 * Set: dtInicioRastreabilidadeTitulo.
	 *
	 * @param dtInicioRastreabilidadeTitulo the dt inicio rastreabilidade titulo
	 */
	public void setDtInicioRastreabilidadeTitulo(
			String dtInicioRastreabilidadeTitulo) {
		this.dtInicioRastreabilidadeTitulo = dtInicioRastreabilidadeTitulo;
	}
	
	/**
	 * Get: dtInicioRecadastroBeneficio.
	 *
	 * @return dtInicioRecadastroBeneficio
	 */
	public String getDtInicioRecadastroBeneficio() {
		return dtInicioRecadastroBeneficio;
	}
	
	/**
	 * Set: dtInicioRecadastroBeneficio.
	 *
	 * @param dtInicioRecadastroBeneficio the dt inicio recadastro beneficio
	 */
	public void setDtInicioRecadastroBeneficio(String dtInicioRecadastroBeneficio) {
		this.dtInicioRecadastroBeneficio = dtInicioRecadastroBeneficio;
	}
	
	/**
	 * Get: dtLimiteVinculoCarga.
	 *
	 * @return dtLimiteVinculoCarga
	 */
	public String getDtLimiteVinculoCarga() {
		return dtLimiteVinculoCarga;
	}
	
	/**
	 * Set: dtLimiteVinculoCarga.
	 *
	 * @param dtLimiteVinculoCarga the dt limite vinculo carga
	 */
	public void setDtLimiteVinculoCarga(String dtLimiteVinculoCarga) {
		this.dtLimiteVinculoCarga = dtLimiteVinculoCarga;
	}
	
	/**
	 * Get: hrManutencaoRegistroAlteracao.
	 *
	 * @return hrManutencaoRegistroAlteracao
	 */
	public String getHrManutencaoRegistroAlteracao() {
		return hrManutencaoRegistroAlteracao;
	}
	
	/**
	 * Set: hrManutencaoRegistroAlteracao.
	 *
	 * @param hrManutencaoRegistroAlteracao the hr manutencao registro alteracao
	 */
	public void setHrManutencaoRegistroAlteracao(
			String hrManutencaoRegistroAlteracao) {
		this.hrManutencaoRegistroAlteracao = hrManutencaoRegistroAlteracao;
	}
	
	/**
	 * Get: hrManutencaoRegistroInclusao.
	 *
	 * @return hrManutencaoRegistroInclusao
	 */
	public String getHrManutencaoRegistroInclusao() {
		return hrManutencaoRegistroInclusao;
	}
	
	/**
	 * Set: hrManutencaoRegistroInclusao.
	 *
	 * @param hrManutencaoRegistroInclusao the hr manutencao registro inclusao
	 */
	public void setHrManutencaoRegistroInclusao(String hrManutencaoRegistroInclusao) {
		this.hrManutencaoRegistroInclusao = hrManutencaoRegistroInclusao;
	}
	
	/**
	 * Get: nmOperacaoFluxoAlteracao.
	 *
	 * @return nmOperacaoFluxoAlteracao
	 */
	public String getNmOperacaoFluxoAlteracao() {
		return nmOperacaoFluxoAlteracao;
	}
	
	/**
	 * Set: nmOperacaoFluxoAlteracao.
	 *
	 * @param nmOperacaoFluxoAlteracao the nm operacao fluxo alteracao
	 */
	public void setNmOperacaoFluxoAlteracao(String nmOperacaoFluxoAlteracao) {
		this.nmOperacaoFluxoAlteracao = nmOperacaoFluxoAlteracao;
	}
	
	/**
	 * Get: nmOperacaoFluxoInclusao.
	 *
	 * @return nmOperacaoFluxoInclusao
	 */
	public String getNmOperacaoFluxoInclusao() {
		return nmOperacaoFluxoInclusao;
	}
	
	/**
	 * Set: nmOperacaoFluxoInclusao.
	 *
	 * @param nmOperacaoFluxoInclusao the nm operacao fluxo inclusao
	 */
	public void setNmOperacaoFluxoInclusao(String nmOperacaoFluxoInclusao) {
		this.nmOperacaoFluxoInclusao = nmOperacaoFluxoInclusao;
	}
	
	/**
	 * Get: nrFechamentoApuracaoTarifa.
	 *
	 * @return nrFechamentoApuracaoTarifa
	 */
	public Integer getNrFechamentoApuracaoTarifa() {
		return nrFechamentoApuracaoTarifa;
	}
	
	/**
	 * Set: nrFechamentoApuracaoTarifa.
	 *
	 * @param nrFechamentoApuracaoTarifa the nr fechamento apuracao tarifa
	 */
	public void setNrFechamentoApuracaoTarifa(Integer nrFechamentoApuracaoTarifa) {
		this.nrFechamentoApuracaoTarifa = nrFechamentoApuracaoTarifa;
	}
	
	/**
	 * Get: qtAntecedencia.
	 *
	 * @return qtAntecedencia
	 */
	public Integer getQtAntecedencia() {
		return qtAntecedencia;
	}
	
	/**
	 * Set: qtAntecedencia.
	 *
	 * @param qtAntecedencia the qt antecedencia
	 */
	public void setQtAntecedencia(Integer qtAntecedencia) {
		this.qtAntecedencia = qtAntecedencia;
	}
	
	/**
	 * Get: qtAnteriorVencimentoComprovado.
	 *
	 * @return qtAnteriorVencimentoComprovado
	 */
	public Integer getQtAnteriorVencimentoComprovado() {
		return qtAnteriorVencimentoComprovado;
	}
	
	/**
	 * Set: qtAnteriorVencimentoComprovado.
	 *
	 * @param qtAnteriorVencimentoComprovado the qt anterior vencimento comprovado
	 */
	public void setQtAnteriorVencimentoComprovado(
			Integer qtAnteriorVencimentoComprovado) {
		this.qtAnteriorVencimentoComprovado = qtAnteriorVencimentoComprovado;
	}
	
	/**
	 * Get: qtDiaCobrancaTarifa.
	 *
	 * @return qtDiaCobrancaTarifa
	 */
	public Integer getQtDiaCobrancaTarifa() {
		return qtDiaCobrancaTarifa;
	}
	
	/**
	 * Set: qtDiaCobrancaTarifa.
	 *
	 * @param qtDiaCobrancaTarifa the qt dia cobranca tarifa
	 */
	public void setQtDiaCobrancaTarifa(Integer qtDiaCobrancaTarifa) {
		this.qtDiaCobrancaTarifa = qtDiaCobrancaTarifa;
	}
	
	/**
	 * Get: qtDiaExpiracao.
	 *
	 * @return qtDiaExpiracao
	 */
	public Integer getQtDiaExpiracao() {
		return qtDiaExpiracao;
	}
	
	/**
	 * Set: qtDiaExpiracao.
	 *
	 * @param qtDiaExpiracao the qt dia expiracao
	 */
	public void setQtDiaExpiracao(Integer qtDiaExpiracao) {
		this.qtDiaExpiracao = qtDiaExpiracao;
	}
	
	/**
	 * Get: qtDiaInatividadeFavorecido.
	 *
	 * @return qtDiaInatividadeFavorecido
	 */
	public Integer getQtDiaInatividadeFavorecido() {
		return qtDiaInatividadeFavorecido;
	}
	
	/**
	 * Set: qtDiaInatividadeFavorecido.
	 *
	 * @param qtDiaInatividadeFavorecido the qt dia inatividade favorecido
	 */
	public void setQtDiaInatividadeFavorecido(Integer qtDiaInatividadeFavorecido) {
		this.qtDiaInatividadeFavorecido = qtDiaInatividadeFavorecido;
	}
	
	/**
	 * Get: qtEtapasRecadastroBeneficio.
	 *
	 * @return qtEtapasRecadastroBeneficio
	 */
	public Integer getQtEtapasRecadastroBeneficio() {
		return qtEtapasRecadastroBeneficio;
	}
	
	/**
	 * Set: qtEtapasRecadastroBeneficio.
	 *
	 * @param qtEtapasRecadastroBeneficio the qt etapas recadastro beneficio
	 */
	public void setQtEtapasRecadastroBeneficio(Integer qtEtapasRecadastroBeneficio) {
		this.qtEtapasRecadastroBeneficio = qtEtapasRecadastroBeneficio;
	}
	
	/**
	 * Get: qtFaseRecadastroBeneficio.
	 *
	 * @return qtFaseRecadastroBeneficio
	 */
	public Integer getQtFaseRecadastroBeneficio() {
		return qtFaseRecadastroBeneficio;
	}
	
	/**
	 * Set: qtFaseRecadastroBeneficio.
	 *
	 * @param qtFaseRecadastroBeneficio the qt fase recadastro beneficio
	 */
	public void setQtFaseRecadastroBeneficio(Integer qtFaseRecadastroBeneficio) {
		this.qtFaseRecadastroBeneficio = qtFaseRecadastroBeneficio;
	}
	
	/**
	 * Get: qtLimiteLinha.
	 *
	 * @return qtLimiteLinha
	 */
	public Integer getQtLimiteLinha() {
		return qtLimiteLinha;
	}
	
	/**
	 * Set: qtLimiteLinha.
	 *
	 * @param qtLimiteLinha the qt limite linha
	 */
	public void setQtLimiteLinha(Integer qtLimiteLinha) {
		this.qtLimiteLinha = qtLimiteLinha;
	}
	
	/**
	 * Get: qtLimiteSolicitacaoCatao.
	 *
	 * @return qtLimiteSolicitacaoCatao
	 */
	public Integer getQtLimiteSolicitacaoCatao() {
		return qtLimiteSolicitacaoCatao;
	}
	
	/**
	 * Set: qtLimiteSolicitacaoCatao.
	 *
	 * @param qtLimiteSolicitacaoCatao the qt limite solicitacao catao
	 */
	public void setQtLimiteSolicitacaoCatao(Integer qtLimiteSolicitacaoCatao) {
		this.qtLimiteSolicitacaoCatao = qtLimiteSolicitacaoCatao;
	}
	
	/**
	 * Get: qtMaximaTituloVencido.
	 *
	 * @return qtMaximaTituloVencido
	 */
	public Integer getQtMaximaTituloVencido() {
		return qtMaximaTituloVencido;
	}
	
	/**
	 * Set: qtMaximaTituloVencido.
	 *
	 * @param qtMaximaTituloVencido the qt maxima titulo vencido
	 */
	public void setQtMaximaTituloVencido(Integer qtMaximaTituloVencido) {
		this.qtMaximaTituloVencido = qtMaximaTituloVencido;
	}
	
	/**
	 * Get: qtMesComprovante.
	 *
	 * @return qtMesComprovante
	 */
	public Integer getQtMesComprovante() {
		return qtMesComprovante;
	}
	
	/**
	 * Set: qtMesComprovante.
	 *
	 * @param qtMesComprovante the qt mes comprovante
	 */
	public void setQtMesComprovante(Integer qtMesComprovante) {
		this.qtMesComprovante = qtMesComprovante;
	}
	
	/**
	 * Get: qtMesEtapaRecadastro.
	 *
	 * @return qtMesEtapaRecadastro
	 */
	public Integer getQtMesEtapaRecadastro() {
		return qtMesEtapaRecadastro;
	}
	
	/**
	 * Set: qtMesEtapaRecadastro.
	 *
	 * @param qtMesEtapaRecadastro the qt mes etapa recadastro
	 */
	public void setQtMesEtapaRecadastro(Integer qtMesEtapaRecadastro) {
		this.qtMesEtapaRecadastro = qtMesEtapaRecadastro;
	}
	
	/**
	 * Get: qtMesFaseRecadastro.
	 *
	 * @return qtMesFaseRecadastro
	 */
	public Integer getQtMesFaseRecadastro() {
		return qtMesFaseRecadastro;
	}
	
	/**
	 * Set: qtMesFaseRecadastro.
	 *
	 * @param qtMesFaseRecadastro the qt mes fase recadastro
	 */
	public void setQtMesFaseRecadastro(Integer qtMesFaseRecadastro) {
		this.qtMesFaseRecadastro = qtMesFaseRecadastro;
	}
	
	/**
	 * Get: qtMesReajusteTarifa.
	 *
	 * @return qtMesReajusteTarifa
	 */
	public Integer getQtMesReajusteTarifa() {
		return qtMesReajusteTarifa;
	}
	
	/**
	 * Set: qtMesReajusteTarifa.
	 *
	 * @param qtMesReajusteTarifa the qt mes reajuste tarifa
	 */
	public void setQtMesReajusteTarifa(Integer qtMesReajusteTarifa) {
		this.qtMesReajusteTarifa = qtMesReajusteTarifa;
	}
	
	/**
	 * Get: qtViaAviso.
	 *
	 * @return qtViaAviso
	 */
	public Integer getQtViaAviso() {
		return qtViaAviso;
	}
	
	/**
	 * Set: qtViaAviso.
	 *
	 * @param qtViaAviso the qt via aviso
	 */
	public void setQtViaAviso(Integer qtViaAviso) {
		this.qtViaAviso = qtViaAviso;
	}
	
	/**
	 * Get: qtViaCobranca.
	 *
	 * @return qtViaCobranca
	 */
	public Integer getQtViaCobranca() {
		return qtViaCobranca;
	}
	
	/**
	 * Set: qtViaCobranca.
	 *
	 * @param qtViaCobranca the qt via cobranca
	 */
	public void setQtViaCobranca(Integer qtViaCobranca) {
		this.qtViaCobranca = qtViaCobranca;
	}
	
	/**
	 * Get: qtViaComprovante.
	 *
	 * @return qtViaComprovante
	 */
	public Integer getQtViaComprovante() {
		return qtViaComprovante;
	}
	
	/**
	 * Set: qtViaComprovante.
	 *
	 * @param qtViaComprovante the qt via comprovante
	 */
	public void setQtViaComprovante(Integer qtViaComprovante) {
		this.qtViaComprovante = qtViaComprovante;
	}
	
	/**
	 * Set: cdCanalInclusao.
	 *
	 * @param cdCanalInclusao the cd canal inclusao
	 */
	public void setCdCanalInclusao(Integer cdCanalInclusao) {
		this.cdCanalInclusao = cdCanalInclusao;
	}
	
	/**
	 * Set: cdFormaAutorizacaoPagamento.
	 *
	 * @param cdFormaAutorizacaoPagamento the cd forma autorizacao pagamento
	 */
	public void setCdFormaAutorizacaoPagamento(Integer cdFormaAutorizacaoPagamento) {
		this.cdFormaAutorizacaoPagamento = cdFormaAutorizacaoPagamento;
	}
	
	/**
	 * Set: cdFormaEnvioPagamento.
	 *
	 * @param cdFormaEnvioPagamento the cd forma envio pagamento
	 */
	public void setCdFormaEnvioPagamento(Integer cdFormaEnvioPagamento) {
		this.cdFormaEnvioPagamento = cdFormaEnvioPagamento;
	}
	
	/**
	 * Get: cdIndicadorRetornoSeparado.
	 *
	 * @return cdIndicadorRetornoSeparado
	 */
	public Integer getCdIndicadorRetornoSeparado() {
		return cdIndicadorRetornoSeparado;
	}
	
	/**
	 * Set: cdIndicadorRetornoSeparado.
	 *
	 * @param cdIndicadorRetornoSeparado the cd indicador retorno separado
	 */
	public void setCdIndicadorRetornoSeparado(Integer cdIndicadorRetornoSeparado) {
		this.cdIndicadorRetornoSeparado = cdIndicadorRetornoSeparado;
	}
	
	/**
	 * Get: dsCodigoIndicadorRetornoSeparado.
	 *
	 * @return dsCodigoIndicadorRetornoSeparado
	 */
	public String getDsCodigoIndicadorRetornoSeparado() {
		return dsCodigoIndicadorRetornoSeparado;
	}
	
	/**
	 * Set: dsCodigoIndicadorRetornoSeparado.
	 *
	 * @param dsCodigoIndicadorRetornoSeparado the ds codigo indicador retorno separado
	 */
	public void setDsCodigoIndicadorRetornoSeparado(
			String dsCodigoIndicadorRetornoSeparado) {
		this.dsCodigoIndicadorRetornoSeparado = dsCodigoIndicadorRetornoSeparado;
	}
	
	/**
	 * Get: dsLocalEmissao.
	 *
	 * @return dsLocalEmissao
	 */
	public String getDsLocalEmissao() {
		return dsLocalEmissao;
	}
	
	/**
	 * Set: dsLocalEmissao.
	 *
	 * @param dsLocalEmissao the ds local emissao
	 */
	public void setDsLocalEmissao(String dsLocalEmissao) {
		this.dsLocalEmissao = dsLocalEmissao;
	}
	
	/**
	 * Get: cdConsultaSaldoValorSuperior.
	 *
	 * @return cdConsultaSaldoValorSuperior
	 */
	public Integer getCdConsultaSaldoValorSuperior() {
		return cdConsultaSaldoValorSuperior;
	}
	
	/**
	 * Set: cdConsultaSaldoValorSuperior.
	 *
	 * @param cdConsultaSaldoValorSuperior the cd consulta saldo valor superior
	 */
	public void setCdConsultaSaldoValorSuperior(Integer cdConsultaSaldoValorSuperior) {
		this.cdConsultaSaldoValorSuperior = cdConsultaSaldoValorSuperior;
	}
	
	/**
	 * Get: dsConsultaSaldoSuperior.
	 *
	 * @return dsConsultaSaldoSuperior
	 */
	public String getDsConsultaSaldoSuperior() {
		return dsConsultaSaldoSuperior;
	}
	
	/**
	 * Set: dsConsultaSaldoSuperior.
	 *
	 * @param dsConsultaSaldoSuperior the ds consulta saldo superior
	 */
	public void setDsConsultaSaldoSuperior(String dsConsultaSaldoSuperior) {
		this.dsConsultaSaldoSuperior = dsConsultaSaldoSuperior;
	}
	
	/**
	 * Get: cdIndicadorFeriadoLocal.
	 *
	 * @return cdIndicadorFeriadoLocal
	 */
	public Integer getCdIndicadorFeriadoLocal() {
		return cdIndicadorFeriadoLocal;
	}
	
	/**
	 * Set: cdIndicadorFeriadoLocal.
	 *
	 * @param cdIndicadorFeriadoLocal the cd indicador feriado local
	 */
	public void setCdIndicadorFeriadoLocal(Integer cdIndicadorFeriadoLocal) {
		this.cdIndicadorFeriadoLocal = cdIndicadorFeriadoLocal;
	}
	
	/**
	 * Get: dsCodigoIndFeriadoLocal.
	 *
	 * @return dsCodigoIndFeriadoLocal
	 */
	public String getDsCodigoIndFeriadoLocal() {
		return dsCodigoIndFeriadoLocal;
	}
	
	/**
	 * Set: dsCodigoIndFeriadoLocal.
	 *
	 * @param dsCodigoIndFeriadoLocal the ds codigo ind feriado local
	 */
	public void setDsCodigoIndFeriadoLocal(String dsCodigoIndFeriadoLocal) {
		this.dsCodigoIndFeriadoLocal = dsCodigoIndFeriadoLocal;
	}
	
	/**
	 * Get: cdIndicadorSegundaLinha.
	 *
	 * @return cdIndicadorSegundaLinha
	 */
	public Integer getCdIndicadorSegundaLinha() {
		return cdIndicadorSegundaLinha;
	}
	
	/**
	 * Set: cdIndicadorSegundaLinha.
	 *
	 * @param cdIndicadorSegundaLinha the cd indicador segunda linha
	 */
	public void setCdIndicadorSegundaLinha(Integer cdIndicadorSegundaLinha) {
		this.cdIndicadorSegundaLinha = cdIndicadorSegundaLinha;
	}
	
	/**
	 * Get: dsIndicadorSegundaLinha.
	 *
	 * @return dsIndicadorSegundaLinha
	 */
	public String getDsIndicadorSegundaLinha() {
		return dsIndicadorSegundaLinha;
	}
	
	/**
	 * Set: dsIndicadorSegundaLinha.
	 *
	 * @param dsIndicadorSegundaLinha the ds indicador segunda linha
	 */
	public void setDsIndicadorSegundaLinha(String dsIndicadorSegundaLinha) {
		this.dsIndicadorSegundaLinha = dsIndicadorSegundaLinha;
	}

	/**
	 * Gets the cd float servico contrato.
	 *
	 * @return the cd float servico contrato
	 */
	public Integer getCdFloatServicoContrato() {
		return cdFloatServicoContrato;
	}

	/**
	 * Sets the cd float servico contrato.
	 *
	 * @param cdFloatServicoContrato the new cd float servico contrato
	 */
	public void setCdFloatServicoContrato(Integer cdFloatServicoContrato) {
		this.cdFloatServicoContrato = cdFloatServicoContrato;
	}

	/**
	 * Gets the ds float servico contrato.
	 *
	 * @return the ds float servico contrato
	 */
	public String getDsFloatServicoContrato() {
		return dsFloatServicoContrato;
	}

	/**
	 * Sets the ds float servico contrato.
	 *
	 * @param dsFloatServicoContrato the new ds float servico contrato
	 */
	public void setDsFloatServicoContrato(String dsFloatServicoContrato) {
		this.dsFloatServicoContrato = dsFloatServicoContrato;
	}

	/**
	 * Sets the cd exige aut filial.
	 *
	 * @param cdExigeAutFilial the cdExigeAutFilial to set
	 */
	public void setCdExigeAutFilial(Integer cdExigeAutFilial) {
		this.cdExigeAutFilial = cdExigeAutFilial;
	}

	/**
	 * Gets the cd exige aut filial.
	 *
	 * @return the cdExigeAutFilial
	 */
	public Integer getCdExigeAutFilial() {
		return cdExigeAutFilial;
	}

    /**
     * Nome: getCdPreenchimentoLancamentoPersonalizado.
     *
     * @return cdPreenchimentoLancamentoPersonalizado
     */
    public Integer getCdPreenchimentoLancamentoPersonalizado() {
        return cdPreenchimentoLancamentoPersonalizado;
    }

    /**
     * Nome: setCdPreenchimentoLancamentoPersonalizado.
     *
     * @param cdPreenchimentoLancamentoPersonalizado the new cd preenchimento lancamento personalizado
     */
    public void setCdPreenchimentoLancamentoPersonalizado(Integer cdPreenchimentoLancamentoPersonalizado) {
        this.cdPreenchimentoLancamentoPersonalizado = cdPreenchimentoLancamentoPersonalizado;
    }

    /**
     * Nome: getDsPreenchimentoLancamentoPersonalizado.
     *
     * @return dsPreenchimentoLancamentoPersonalizado
     */
    public String getDsPreenchimentoLancamentoPersonalizado() {
        return dsPreenchimentoLancamentoPersonalizado;
    }

    /**
     * Nome: setDsPreenchimentoLancamentoPersonalizado.
     *
     * @param dsPreenchimentoLancamentoPersonalizado the new ds preenchimento lancamento personalizado
     */
    public void setDsPreenchimentoLancamentoPersonalizado(String dsPreenchimentoLancamentoPersonalizado) {
        this.dsPreenchimentoLancamentoPersonalizado = dsPreenchimentoLancamentoPersonalizado;
    }

    /**
     * Nome: getCdTituloDdaRetorno.
     *
     * @return cdTituloDdaRetorno
     */
    public Integer getCdTituloDdaRetorno() {
        return cdTituloDdaRetorno;
    }

    /**
     * Nome: setCdTituloDdaRetorno.
     *
     * @param cdTituloDdaRetorno the new cd titulo dda retorno
     */
    public void setCdTituloDdaRetorno(Integer cdTituloDdaRetorno) {
        this.cdTituloDdaRetorno = cdTituloDdaRetorno;
    }

    /**
     * Nome: getDsTituloDdaRetorno.
     *
     * @return dsTituloDdaRetorno
     */
    public String getDsTituloDdaRetorno() {
        return dsTituloDdaRetorno;
    }

    /**
     * Nome: setDsTituloDdaRetorno.
     *
     * @param dsTituloDdaRetorno the new ds titulo dda retorno
     */
    public void setDsTituloDdaRetorno(String dsTituloDdaRetorno) {
        this.dsTituloDdaRetorno = dsTituloDdaRetorno;
    }

    /**
     * Nome: getCdIndicadorAgendaGrade.
     *
     * @return cdIndicadorAgendaGrade
     */
    public Integer getCdIndicadorAgendaGrade() {
        return cdIndicadorAgendaGrade;
    }

    /**
     * Nome: setCdIndicadorAgendaGrade.
     *
     * @param cdIndicadorAgendaGrade the new cd indicador agenda grade
     */
    public void setCdIndicadorAgendaGrade(Integer cdIndicadorAgendaGrade) {
        this.cdIndicadorAgendaGrade = cdIndicadorAgendaGrade;
    }

    /**
     * Nome: getDsIndicadorAgendaGrade.
     *
     * @return dsIndicadorAgendaGrade
     */
    public String getDsIndicadorAgendaGrade() {
        return dsIndicadorAgendaGrade;
    }

    /**
     * Nome: setDsIndicadorAgendaGrade.
     *
     * @param dsIndicadorAgendaGrade the new ds indicador agenda grade
     */
    public void setDsIndicadorAgendaGrade(String dsIndicadorAgendaGrade) {
        this.dsIndicadorAgendaGrade = dsIndicadorAgendaGrade;
    }

	/**
	 * Gets the qt dia util pgto.
	 *
	 * @return the qt dia util pgto
	 */
	public Integer getQtDiaUtilPgto() {
		return qtDiaUtilPgto;
	}

	/**
	 * Sets the qt dia util pgto.
	 *
	 * @param qtDiaUtilPgto the new qt dia util pgto
	 */
	public void setQtDiaUtilPgto(Integer qtDiaUtilPgto) {
		this.qtDiaUtilPgto = qtDiaUtilPgto;
	}

	/**
     * Get: cdIndicadorUtilizaMora.
     * 
     * @return cdIndicadorUtilizaMora
     */
	public Integer getCdIndicadorUtilizaMora() {
		return cdIndicadorUtilizaMora;
	}

	/**
     * Set: cdIndicadorUtilizaMora.
     * 
     * @param cdIndicadorUtilizaMora
     *            the cd indicador utiliza mora
     */
	public void setCdIndicadorUtilizaMora(Integer cdIndicadorUtilizaMora) {
		this.cdIndicadorUtilizaMora = cdIndicadorUtilizaMora;
	}

	/**
     * Get: dsIndicadorUtilizaMora.
     * 
     * @return dsIndicadorUtilizaMora
     */
	public String getDsIndicadorUtilizaMora() {
		return dsIndicadorUtilizaMora;
	}

	/**
     * Set: dsIndicadorUtilizaMora.
     * 
     * @param dsIndicadorUtilizaMora
     *            the ds indicador utiliza mora
     */
	public void setDsIndicadorUtilizaMora(String dsIndicadorUtilizaMora) {
		this.dsIndicadorUtilizaMora = dsIndicadorUtilizaMora;
	}

    /**
     * Nome: getVlPercentualDiferencaTolerada
     *
     * @return vlPercentualDiferencaTolerada
     */
    public BigDecimal getVlPercentualDiferencaTolerada() {
        return vlPercentualDiferencaTolerada;
    }

    /**
     * Nome: setVlPercentualDiferencaTolerada
     *
     * @param vlPercentualDiferencaTolerada
     */
    public void setVlPercentualDiferencaTolerada(BigDecimal vlPercentualDiferencaTolerada) {
        this.vlPercentualDiferencaTolerada = vlPercentualDiferencaTolerada;
    }

	public void setDsIndicador(String dsIndicador) {
		this.dsIndicador = dsIndicador;
	}

	public String getDsIndicador() {
		return dsIndicador;
	}

	public void setDsOrigemIndicador(int dsOrigemIndicador) {
		this.dsOrigemIndicador = dsOrigemIndicador;
	}

	public int getDsOrigemIndicador() {
		return dsOrigemIndicador;
	}

	public Integer getCdIndicadorTipoRetornoInternet() {
		return cdIndicadorTipoRetornoInternet;
	}

	public void setCdIndicadorTipoRetornoInternet(
			Integer cdIndicadorTipoRetornoInternet) {
		this.cdIndicadorTipoRetornoInternet = cdIndicadorTipoRetornoInternet;
	}

	public String getDsCodIndicadorTipoRetornoInternet() {
		return dsCodIndicadorTipoRetornoInternet;
	}

	public void setDsCodIndicadorTipoRetornoInternet(
			String dsCodIndicadorTipoRetornoInternet) {
		this.dsCodIndicadorTipoRetornoInternet = dsCodIndicadorTipoRetornoInternet;
	}
	
}