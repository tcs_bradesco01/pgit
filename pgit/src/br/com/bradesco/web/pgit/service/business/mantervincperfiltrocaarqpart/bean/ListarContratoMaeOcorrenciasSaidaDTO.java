package br.com.bradesco.web.pgit.service.business.mantervincperfiltrocaarqpart.bean;

import br.com.bradesco.web.pgit.utils.CpfCnpjUtils;


/**
 * Arquivo criado em 30/12/15.
 */
public class ListarContratoMaeOcorrenciasSaidaDTO {

    private Integer cdTipoParticipacaoPessoa;

    private Long cdPessoa;

    private Long cdCpfCnpj;

    private Integer cdFilialCnpj;

    private Integer cdControleCnpj;

    private String dsNomeRazao;

    private Long cdPerfilTrocaArquivo;

    private Integer cdTipoLayoutArquivo;

    private String dsTipoLayoutArquivo;
    
    public String getCpfCnpjFormatado(){
    	return CpfCnpjUtils.formatarCpfCnpj(cdCpfCnpj, cdFilialCnpj, cdControleCnpj);
    }

    public void setCdTipoParticipacaoPessoa(Integer cdTipoParticipacaoPessoa) {
        this.cdTipoParticipacaoPessoa = cdTipoParticipacaoPessoa;
    }

    public Integer getCdTipoParticipacaoPessoa() {
        return this.cdTipoParticipacaoPessoa;
    }

    public void setCdPessoa(Long cdPessoa) {
        this.cdPessoa = cdPessoa;
    }

    public Long getCdPessoa() {
        return this.cdPessoa;
    }

    public void setCdCpfCnpj(Long cdCpfCnpj) {
        this.cdCpfCnpj = cdCpfCnpj;
    }

    public Long getCdCpfCnpj() {
        return this.cdCpfCnpj;
    }

    public void setCdFilialCnpj(Integer cdFilialCnpj) {
        this.cdFilialCnpj = cdFilialCnpj;
    }

    public Integer getCdFilialCnpj() {
        return this.cdFilialCnpj;
    }

    public void setCdControleCnpj(Integer cdControleCnpj) {
        this.cdControleCnpj = cdControleCnpj;
    }

    public Integer getCdControleCnpj() {
        return this.cdControleCnpj;
    }

    public void setDsNomeRazao(String dsNomeRazao) {
        this.dsNomeRazao = dsNomeRazao;
    }

    public String getDsNomeRazao() {
        return this.dsNomeRazao;
    }

    public void setCdPerfilTrocaArquivo(Long cdPerfilTrocaArquivo) {
        this.cdPerfilTrocaArquivo = cdPerfilTrocaArquivo;
    }

    public Long getCdPerfilTrocaArquivo() {
        return this.cdPerfilTrocaArquivo;
    }

    public void setCdTipoLayoutArquivo(Integer cdTipoLayoutArquivo) {
        this.cdTipoLayoutArquivo = cdTipoLayoutArquivo;
    }

    public Integer getCdTipoLayoutArquivo() {
        return this.cdTipoLayoutArquivo;
    }

    public void setDsTipoLayoutArquivo(String dsTipoLayoutArquivo) {
        this.dsTipoLayoutArquivo = dsTipoLayoutArquivo;
    }

    public String getDsTipoLayoutArquivo() {
        return this.dsTipoLayoutArquivo;
    }
}