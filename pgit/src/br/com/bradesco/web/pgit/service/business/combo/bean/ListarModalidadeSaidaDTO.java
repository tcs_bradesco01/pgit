/*
 * Nome: br.com.bradesco.web.pgit.service.business.combo.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.combo.bean;

/**
 * Nome: ListarModalidadeSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarModalidadeSaidaDTO {

	/** Atributo cdModalidade. */
	private int cdModalidade;
	
	/** Atributo dsModalidade. */
	private String dsModalidade;
	
	/** Atributo cdRelacionamentoProduto. */
	private int cdRelacionamentoProduto;
	
	/** Atributo check. */
	private boolean check;
	
	/**
	 * Listar modalidade saida dto.
	 */
	public ListarModalidadeSaidaDTO(){
		this.check = false;
	}
	
	/**
	 * Is check.
	 *
	 * @return true, if is check
	 */
	public boolean isCheck() {
		return check;
	}

	/**
	 * Set: check.
	 *
	 * @param check the check
	 */
	public void setCheck(boolean check) {
		this.check = check;
	}

	/**
	 * Listar modalidade saida dto.
	 *
	 * @param cdModalidade the cd modalidade
	 * @param dsModalidade the ds modalidade
	 * @param cdRelacionamentoPRoduto the cd relacionamento p roduto
	 */
	public ListarModalidadeSaidaDTO(int cdModalidade, String dsModalidade, int cdRelacionamentoPRoduto){
		
		this.cdModalidade = cdModalidade;
		this.dsModalidade = dsModalidade;
		this.cdRelacionamentoProduto = cdRelacionamentoPRoduto;
	}

	/**
	 * Get: cdModalidade.
	 *
	 * @return cdModalidade
	 */
	public int getCdModalidade() {
		return cdModalidade;
	}

	/**
	 * Set: cdModalidade.
	 *
	 * @param cdModalidade the cd modalidade
	 */
	public void setCdModalidade(int cdModalidade) {
		this.cdModalidade = cdModalidade;
	}

	/**
	 * Get: dsModalidade.
	 *
	 * @return dsModalidade
	 */
	public String getDsModalidade() {
		return dsModalidade;
	}

	/**
	 * Set: dsModalidade.
	 *
	 * @param dsModalidade the ds modalidade
	 */
	public void setDsModalidade(String dsModalidade) {
		this.dsModalidade = dsModalidade;
	}

	/**
	 * Get: cdRelacionamentoProduto.
	 *
	 * @return cdRelacionamentoProduto
	 */
	public int getCdRelacionamentoProduto() {
		return cdRelacionamentoProduto;
	}

	/**
	 * Set: cdRelacionamentoProduto.
	 *
	 * @param cdRelacionamentoProduto the cd relacionamento produto
	 */
	public void setCdRelacionamentoProduto(int cdRelacionamentoProduto) {
		this.cdRelacionamentoProduto = cdRelacionamentoProduto;
	}
	
	
}
