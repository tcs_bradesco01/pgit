/*
 * Nome: br.com.bradesco.web.pgit.service.business.anteciparpostergarpagtosagecon.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.anteciparpostergarpagtosagecon.bean;

import java.util.List;

/**
 * Nome: DetalharAntPostergarPagtosSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class DetalharAntPostergarPagtosSaidaDTO {
	
	/** Atributo codMensagem. */
	private String codMensagem;
	
	/** Atributo mensagem. */
	private String mensagem;
	
	/** Atributo numeroConsultas. */
	private Integer numeroConsultas;
    
    /** Atributo dsBancoDebito. */
    private String dsBancoDebito;
    
    /** Atributo dsAgenciaDebito. */
    private String dsAgenciaDebito;
    
    /** Atributo dsTipoContaDebito. */
    private String dsTipoContaDebito;
    
    /** Atributo dsContrato. */
    private String dsContrato;
	
	/** Atributo dsSituacaoContrato. */
	private String dsSituacaoContrato;
	
	/** Atributo nomeCliente. */
	private String nomeCliente;
    
    /** Atributo listaDetalharAntPostergarPagtos. */
    private List<OcorrenciasDetalharAntPostergarPagtosSaidaDTO> listaDetalharAntPostergarPagtos;
    
	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}
	
	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}
	
	/**
	 * Get: dsAgenciaDebito.
	 *
	 * @return dsAgenciaDebito
	 */
	public String getDsAgenciaDebito() {
		return dsAgenciaDebito;
	}
	
	/**
	 * Set: dsAgenciaDebito.
	 *
	 * @param dsAgenciaDebito the ds agencia debito
	 */
	public void setDsAgenciaDebito(String dsAgenciaDebito) {
		this.dsAgenciaDebito = dsAgenciaDebito;
	}
	
	/**
	 * Get: dsBancoDebito.
	 *
	 * @return dsBancoDebito
	 */
	public String getDsBancoDebito() {
		return dsBancoDebito;
	}
	
	/**
	 * Set: dsBancoDebito.
	 *
	 * @param dsBancoDebito the ds banco debito
	 */
	public void setDsBancoDebito(String dsBancoDebito) {
		this.dsBancoDebito = dsBancoDebito;
	}
	
	/**
	 * Get: dsContrato.
	 *
	 * @return dsContrato
	 */
	public String getDsContrato() {
		return dsContrato;
	}
	
	/**
	 * Set: dsContrato.
	 *
	 * @param dsContrato the ds contrato
	 */
	public void setDsContrato(String dsContrato) {
		this.dsContrato = dsContrato;
	}
	
	/**
	 * Get: dsSituacaoContrato.
	 *
	 * @return dsSituacaoContrato
	 */
	public String getDsSituacaoContrato() {
		return dsSituacaoContrato;
	}
	
	/**
	 * Set: dsSituacaoContrato.
	 *
	 * @param dsSituacaoContrato the ds situacao contrato
	 */
	public void setDsSituacaoContrato(String dsSituacaoContrato) {
		this.dsSituacaoContrato = dsSituacaoContrato;
	}
	
	/**
	 * Get: dsTipoContaDebito.
	 *
	 * @return dsTipoContaDebito
	 */
	public String getDsTipoContaDebito() {
		return dsTipoContaDebito;
	}
	
	/**
	 * Set: dsTipoContaDebito.
	 *
	 * @param dsTipoContaDebito the ds tipo conta debito
	 */
	public void setDsTipoContaDebito(String dsTipoContaDebito) {
		this.dsTipoContaDebito = dsTipoContaDebito;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	/**
	 * Get: numeroConsultas.
	 *
	 * @return numeroConsultas
	 */
	public Integer getNumeroConsultas() {
		return numeroConsultas;
	}
	
	/**
	 * Set: numeroConsultas.
	 *
	 * @param numeroConsultas the numero consultas
	 */
	public void setNumeroConsultas(Integer numeroConsultas) {
		this.numeroConsultas = numeroConsultas;
	}
	
	/**
	 * Get: listaDetalharAntPostergarPagtos.
	 *
	 * @return listaDetalharAntPostergarPagtos
	 */
	public List<OcorrenciasDetalharAntPostergarPagtosSaidaDTO> getListaDetalharAntPostergarPagtos() {
		return listaDetalharAntPostergarPagtos;
	}
	
	/**
	 * Get: nomeCliente.
	 *
	 * @return nomeCliente
	 */
	public String getNomeCliente() {
		return nomeCliente;
	}
	
	/**
	 * Set: nomeCliente.
	 *
	 * @param nomeCliente the nome cliente
	 */
	public void setNomeCliente(String nomeCliente) {
		this.nomeCliente = nomeCliente;
	}
	
	/**
	 * Set: listaDetalharAntPostergarPagtos.
	 *
	 * @param listaDetalharAntPostergarPagtos the lista detalhar ant postergar pagtos
	 */
	public void setListaDetalharAntPostergarPagtos(
			List<OcorrenciasDetalharAntPostergarPagtosSaidaDTO> listaDetalharAntPostergarPagtos) {
		this.listaDetalharAntPostergarPagtos = listaDetalharAntPostergarPagtos;
	}
}