/*
 * Nome: br.com.bradesco.web.pgit.service.business.liberarpagtosconsolidadospend.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.liberarpagtosconsolidadospend.bean;

import java.math.BigDecimal;

/**
 * Nome: ConsultarListaPagamentoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarListaPagamentoSaidaDTO {
    
    /** Atributo codMensagem. */
    private String codMensagem;
    
    /** Atributo mensagem. */
    private String mensagem;
    
    /** Atributo cdPessoaJuridicaContrato. */
    private Long cdPessoaJuridicaContrato;
    
    /** Atributo cdTipoContratoNegocio. */
    private Integer cdTipoContratoNegocio;
    
    /** Atributo nrSequenciaContratoNegocio. */
    private Long nrSequenciaContratoNegocio;
    
    /** Atributo cdTipoCanal. */
    private Integer cdTipoCanal;
    
    /** Atributo nrPagamento. */
    private String nrPagamento;
    
    /** Atributo vlrPagamento. */
    private BigDecimal vlrPagamento;
    
    /** Atributo cdCorpoCpfCnpjFavorecido. */
    private Long cdCorpoCpfCnpjFavorecido;
    
    /** Atributo cdFilialCpfCnpjFavorecido. */
    private Integer cdFilialCpfCnpjFavorecido;
    
    /** Atributo cdControleCpfCnpjFavorecido. */
    private Integer cdControleCpfCnpjFavorecido;
    
    /** Atributo cdFavorecido. */
    private Long cdFavorecido;
    
    /** Atributo dsBeneficiario. */
    private String dsBeneficiario;
    
    /** Atributo cdBancoFavorecido. */
    private Integer cdBancoFavorecido;
    
    /** Atributo dsBanco. */
    private String dsBanco;
    
    /** Atributo cdAgenciaFavorecido. */
    private Integer cdAgenciaFavorecido;
    
    /** Atributo cdDigitoAgencia. */
    private Integer cdDigitoAgencia;
    
    /** Atributo dsAgencia. */
    private String dsAgencia;
    
    /** Atributo cdContaFavorecido. */
    private Long cdContaFavorecido;
    
    /** Atributo cdDigitoContaFavorecido. */
    private String cdDigitoContaFavorecido;
    
    /** Atributo cdTipoTela. */
    private Integer cdTipoTela;
    
    /** Atributo check. */
    private boolean check;   
    
    /** Atributo cpfFormatado. */
    private String cpfFormatado;
    
    /** Atributo favorecidoBeneficiario. */
    private String favorecidoBeneficiario;    
    
    /** Atributo bancoAgenciaContaFormatada. */
    private String bancoAgenciaContaFormatada;   
    
    /** Atributo bancoFormatado. */
    private String bancoFormatado;
    
    /** Atributo agenciaFormatada. */
    private String agenciaFormatada;
    
    /** Atributo contaFormatada. */
    private String contaFormatada;
    
    /** Atributo tipoServico. */
    private String tipoServico;
    
    /** Atributo modalidade. */
    private String modalidade;
    
    
    
    /**
     * Get: bancoAgenciaContaFormatada.
     *
     * @return bancoAgenciaContaFormatada
     */
    public String getBancoAgenciaContaFormatada() {
		return bancoAgenciaContaFormatada;
	}
	
	/**
	 * Set: bancoAgenciaContaFormatada.
	 *
	 * @param bancoAgenciaContaFormatada the banco agencia conta formatada
	 */
	public void setBancoAgenciaContaFormatada(String bancoAgenciaContaFormatada) {
		this.bancoAgenciaContaFormatada = bancoAgenciaContaFormatada;
	}
	
	/**
	 * Get: contaFormatada.
	 *
	 * @return contaFormatada
	 */
	public String getContaFormatada() {
		return contaFormatada;
	}
	
	/**
	 * Set: contaFormatada.
	 *
	 * @param contaFormatada the conta formatada
	 */
	public void setContaFormatada(String contaFormatada) {
		this.contaFormatada = contaFormatada;
	}
	
	/**
	 * Get: agenciaFormatada.
	 *
	 * @return agenciaFormatada
	 */
	public String getAgenciaFormatada() {
		return agenciaFormatada;
	}
	
	/**
	 * Set: agenciaFormatada.
	 *
	 * @param agenciaFormatada the agencia formatada
	 */
	public void setAgenciaFormatada(String agenciaFormatada) {
		this.agenciaFormatada = agenciaFormatada;
	}
	
	/**
	 * Get: bancoFormatado.
	 *
	 * @return bancoFormatado
	 */
	public String getBancoFormatado() {
		return bancoFormatado;
	}
	
	/**
	 * Set: bancoFormatado.
	 *
	 * @param bancoFormatado the banco formatado
	 */
	public void setBancoFormatado(String bancoFormatado) {
		this.bancoFormatado = bancoFormatado;
	}
	
	/**
	 * Get: favorecidoBeneficiario.
	 *
	 * @return favorecidoBeneficiario
	 */
	public String getFavorecidoBeneficiario() {
		return favorecidoBeneficiario;
	}
	
	/**
	 * Set: favorecidoBeneficiario.
	 *
	 * @param favorecidoBeneficiario the favorecido beneficiario
	 */
	public void setFavorecidoBeneficiario(String favorecidoBeneficiario) {
		this.favorecidoBeneficiario = favorecidoBeneficiario;
	}
	
	/**
	 * Get: cpfFormatado.
	 *
	 * @return cpfFormatado
	 */
	public String getCpfFormatado() {
		return cpfFormatado;
	}
	
	/**
	 * Set: cpfFormatado.
	 *
	 * @param cpfFormatado the cpf formatado
	 */
	public void setCpfFormatado(String cpfFormatado) {
		this.cpfFormatado = cpfFormatado;
	}
	
	/**
	 * Is check.
	 *
	 * @return true, if is check
	 */
	public boolean isCheck() {
		return check;
	}
	
	/**
	 * Set: check.
	 *
	 * @param check the check
	 */
	public void setCheck(boolean check) {
		this.check = check;
	}
	
	/**
	 * Get: cdAgenciaFavorecido.
	 *
	 * @return cdAgenciaFavorecido
	 */
	public Integer getCdAgenciaFavorecido() {
		return cdAgenciaFavorecido;
	}
	
	/**
	 * Set: cdAgenciaFavorecido.
	 *
	 * @param cdAgenciaFavorecido the cd agencia favorecido
	 */
	public void setCdAgenciaFavorecido(Integer cdAgenciaFavorecido) {
		this.cdAgenciaFavorecido = cdAgenciaFavorecido;
	}
	
	/**
	 * Get: cdBancoFavorecido.
	 *
	 * @return cdBancoFavorecido
	 */
	public Integer getCdBancoFavorecido() {
		return cdBancoFavorecido;
	}
	
	/**
	 * Set: cdBancoFavorecido.
	 *
	 * @param cdBancoFavorecido the cd banco favorecido
	 */
	public void setCdBancoFavorecido(Integer cdBancoFavorecido) {
		this.cdBancoFavorecido = cdBancoFavorecido;
	}
	
	/**
	 * Get: cdContaFavorecido.
	 *
	 * @return cdContaFavorecido
	 */
	public Long getCdContaFavorecido() {
		return cdContaFavorecido;
	}
	
	/**
	 * Set: cdContaFavorecido.
	 *
	 * @param cdContaFavorecido the cd conta favorecido
	 */
	public void setCdContaFavorecido(Long cdContaFavorecido) {
		this.cdContaFavorecido = cdContaFavorecido;
	}
	
	/**
	 * Get: cdControleCpfCnpjFavorecido.
	 *
	 * @return cdControleCpfCnpjFavorecido
	 */
	public Integer getCdControleCpfCnpjFavorecido() {
		return cdControleCpfCnpjFavorecido;
	}
	
	/**
	 * Set: cdControleCpfCnpjFavorecido.
	 *
	 * @param cdControleCpfCnpjFavorecido the cd controle cpf cnpj favorecido
	 */
	public void setCdControleCpfCnpjFavorecido(Integer cdControleCpfCnpjFavorecido) {
		this.cdControleCpfCnpjFavorecido = cdControleCpfCnpjFavorecido;
	}
	
	/**
	 * Get: cdCorpoCpfCnpjFavorecido.
	 *
	 * @return cdCorpoCpfCnpjFavorecido
	 */
	public Long getCdCorpoCpfCnpjFavorecido() {
		return cdCorpoCpfCnpjFavorecido;
	}
	
	/**
	 * Set: cdCorpoCpfCnpjFavorecido.
	 *
	 * @param cdCorpoCpfCnpjFavorecido the cd corpo cpf cnpj favorecido
	 */
	public void setCdCorpoCpfCnpjFavorecido(Long cdCorpoCpfCnpjFavorecido) {
		this.cdCorpoCpfCnpjFavorecido = cdCorpoCpfCnpjFavorecido;
	}
	
	/**
	 * Get: cdDigitoAgencia.
	 *
	 * @return cdDigitoAgencia
	 */
	public Integer getCdDigitoAgencia() {
		return cdDigitoAgencia;
	}
	
	/**
	 * Set: cdDigitoAgencia.
	 *
	 * @param cdDigitoAgencia the cd digito agencia
	 */
	public void setCdDigitoAgencia(Integer cdDigitoAgencia) {
		this.cdDigitoAgencia = cdDigitoAgencia;
	}
	
	/**
	 * Get: cdDigitoContaFavorecido.
	 *
	 * @return cdDigitoContaFavorecido
	 */
	public String getCdDigitoContaFavorecido() {
		return cdDigitoContaFavorecido;
	}
	
	/**
	 * Set: cdDigitoContaFavorecido.
	 *
	 * @param cdDigitoContaFavorecido the cd digito conta favorecido
	 */
	public void setCdDigitoContaFavorecido(String cdDigitoContaFavorecido) {
		this.cdDigitoContaFavorecido = cdDigitoContaFavorecido;
	}
	
	/**
	 * Get: cdFavorecido.
	 *
	 * @return cdFavorecido
	 */
	public Long getCdFavorecido() {
		return cdFavorecido;
	}
	
	/**
	 * Set: cdFavorecido.
	 *
	 * @param cdFavorecido the cd favorecido
	 */
	public void setCdFavorecido(Long cdFavorecido) {
		this.cdFavorecido = cdFavorecido;
	}
	
	/**
	 * Get: cdFilialCpfCnpjFavorecido.
	 *
	 * @return cdFilialCpfCnpjFavorecido
	 */
	public Integer getCdFilialCpfCnpjFavorecido() {
		return cdFilialCpfCnpjFavorecido;
	}
	
	/**
	 * Set: cdFilialCpfCnpjFavorecido.
	 *
	 * @param cdFilialCpfCnpjFavorecido the cd filial cpf cnpj favorecido
	 */
	public void setCdFilialCpfCnpjFavorecido(Integer cdFilialCpfCnpjFavorecido) {
		this.cdFilialCpfCnpjFavorecido = cdFilialCpfCnpjFavorecido;
	}
	
	/**
	 * Get: cdPessoaJuridicaContrato.
	 *
	 * @return cdPessoaJuridicaContrato
	 */
	public Long getCdPessoaJuridicaContrato() {
		return cdPessoaJuridicaContrato;
	}
	
	/**
	 * Set: cdPessoaJuridicaContrato.
	 *
	 * @param cdPessoaJuridicaContrato the cd pessoa juridica contrato
	 */
	public void setCdPessoaJuridicaContrato(Long cdPessoaJuridicaContrato) {
		this.cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
	}
	
	/**
	 * Get: cdTipoCanal.
	 *
	 * @return cdTipoCanal
	 */
	public Integer getCdTipoCanal() {
		return cdTipoCanal;
	}
	
	/**
	 * Set: cdTipoCanal.
	 *
	 * @param cdTipoCanal the cd tipo canal
	 */
	public void setCdTipoCanal(Integer cdTipoCanal) {
		this.cdTipoCanal = cdTipoCanal;
	}
	
	/**
	 * Get: cdTipoContratoNegocio.
	 *
	 * @return cdTipoContratoNegocio
	 */
	public Integer getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}
	
	/**
	 * Set: cdTipoContratoNegocio.
	 *
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}
	
	/**
	 * Get: cdTipoTela.
	 *
	 * @return cdTipoTela
	 */
	public Integer getCdTipoTela() {
		return cdTipoTela;
	}
	
	/**
	 * Set: cdTipoTela.
	 *
	 * @param cdTipoTela the cd tipo tela
	 */
	public void setCdTipoTela(Integer cdTipoTela) {
		this.cdTipoTela = cdTipoTela;
	}
	
	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}
	
	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}
	
	/**
	 * Get: dsAgencia.
	 *
	 * @return dsAgencia
	 */
	public String getDsAgencia() {
		return dsAgencia;
	}
	
	/**
	 * Set: dsAgencia.
	 *
	 * @param dsAgencia the ds agencia
	 */
	public void setDsAgencia(String dsAgencia) {
		this.dsAgencia = dsAgencia;
	}
	
	/**
	 * Get: dsBanco.
	 *
	 * @return dsBanco
	 */
	public String getDsBanco() {
		return dsBanco;
	}
	
	/**
	 * Set: dsBanco.
	 *
	 * @param dsBanco the ds banco
	 */
	public void setDsBanco(String dsBanco) {
		this.dsBanco = dsBanco;
	}
	
	/**
	 * Get: dsBeneficiario.
	 *
	 * @return dsBeneficiario
	 */
	public String getDsBeneficiario() {
		return dsBeneficiario;
	}
	
	/**
	 * Set: dsBeneficiario.
	 *
	 * @param dsBeneficiario the ds beneficiario
	 */
	public void setDsBeneficiario(String dsBeneficiario) {
		this.dsBeneficiario = dsBeneficiario;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	/**
	 * Get: nrPagamento.
	 *
	 * @return nrPagamento
	 */
	public String getNrPagamento() {
		return nrPagamento;
	}
	
	/**
	 * Set: nrPagamento.
	 *
	 * @param nrPagamento the nr pagamento
	 */
	public void setNrPagamento(String nrPagamento) {
		this.nrPagamento = nrPagamento;
	}
	
	/**
	 * Get: nrSequenciaContratoNegocio.
	 *
	 * @return nrSequenciaContratoNegocio
	 */
	public Long getNrSequenciaContratoNegocio() {
		return nrSequenciaContratoNegocio;
	}
	
	/**
	 * Set: nrSequenciaContratoNegocio.
	 *
	 * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
	 */
	public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}
	
	/**
	 * Get: vlrPagamento.
	 *
	 * @return vlrPagamento
	 */
	public BigDecimal getVlrPagamento() {
		return vlrPagamento;
	}
	
	/**
	 * Set: vlrPagamento.
	 *
	 * @param vlrPagamento the vlr pagamento
	 */
	public void setVlrPagamento(BigDecimal vlrPagamento) {
		this.vlrPagamento = vlrPagamento;
	}
	
	/**
	 * Get: modalidade.
	 *
	 * @return modalidade
	 */
	public String getModalidade() {
		return modalidade;
	}
	
	/**
	 * Set: modalidade.
	 *
	 * @param modalidade the modalidade
	 */
	public void setModalidade(String modalidade) {
		this.modalidade = modalidade;
	}
	
	/**
	 * Get: tipoServico.
	 *
	 * @return tipoServico
	 */
	public String getTipoServico() {
		return tipoServico;
	}
	
	/**
	 * Set: tipoServico.
	 *
	 * @param tipoServico the tipo servico
	 */
	public void setTipoServico(String tipoServico) {
		this.tipoServico = tipoServico;
	}
	
	
    
}
