/*
 * Nome: br.com.bradesco.web.pgit.service.business.combo.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.combo.bean;

/**
 * Nome: ConsultarModalidadePagtoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarModalidadePagtoSaidaDTO{
    
    /** Atributo codMensagem. */
    private String codMensagem;
    
    /** Atributo mensagem. */
    private String mensagem;
    
    /** Atributo cdProdutoOperacaoRelacionado. */
    private Integer cdProdutoOperacaoRelacionado;
    
    /** Atributo dsProdutoOperacaoRelacionado. */
    private String dsProdutoOperacaoRelacionado;
    
    /** Atributo cdRelacionamentoProduto. */
    private Integer cdRelacionamentoProduto;
    
	/**
	 * Consultar modalidade pagto saida dto.
	 *
	 * @param cdProdutoOperacaoRelacionado the cd produto operacao relacionado
	 * @param dsProdutoOperacaoRelacionado the ds produto operacao relacionado
	 */
	public ConsultarModalidadePagtoSaidaDTO(Integer cdProdutoOperacaoRelacionado, String dsProdutoOperacaoRelacionado) {
		super();
		this.cdProdutoOperacaoRelacionado = cdProdutoOperacaoRelacionado;
		this.dsProdutoOperacaoRelacionado = dsProdutoOperacaoRelacionado;
	}
	
	/**
	 * Get: cdProdutoOperacaoRelacionado.
	 *
	 * @return cdProdutoOperacaoRelacionado
	 */
	public Integer getCdProdutoOperacaoRelacionado() {
		return cdProdutoOperacaoRelacionado;
	}
	
	/**
	 * Set: cdProdutoOperacaoRelacionado.
	 *
	 * @param cdProdutoOperacaoRelacionado the cd produto operacao relacionado
	 */
	public void setCdProdutoOperacaoRelacionado(Integer cdProdutoOperacaoRelacionado) {
		this.cdProdutoOperacaoRelacionado = cdProdutoOperacaoRelacionado;
	}
	
	/**
	 * Get: cdRelacionamentoProduto.
	 *
	 * @return cdRelacionamentoProduto
	 */
	public Integer getCdRelacionamentoProduto() {
		return cdRelacionamentoProduto;
	}
	
	/**
	 * Set: cdRelacionamentoProduto.
	 *
	 * @param cdRelacionamentoProduto the cd relacionamento produto
	 */
	public void setCdRelacionamentoProduto(Integer cdRelacionamentoProduto) {
		this.cdRelacionamentoProduto = cdRelacionamentoProduto;
	}
	
	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}
	
	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}
	
	/**
	 * Get: dsProdutoOperacaoRelacionado.
	 *
	 * @return dsProdutoOperacaoRelacionado
	 */
	public String getDsProdutoOperacaoRelacionado() {
		return dsProdutoOperacaoRelacionado;
	}
	
	/**
	 * Set: dsProdutoOperacaoRelacionado.
	 *
	 * @param dsProdutoOperacaoRelacionado the ds produto operacao relacionado
	 */
	public void setDsProdutoOperacaoRelacionado(String dsProdutoOperacaoRelacionado) {
		this.dsProdutoOperacaoRelacionado = dsProdutoOperacaoRelacionado;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
    
}
