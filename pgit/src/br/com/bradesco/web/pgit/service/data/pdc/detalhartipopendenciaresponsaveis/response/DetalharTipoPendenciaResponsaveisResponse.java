/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.detalhartipopendenciaresponsaveis.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class DetalharTipoPendenciaResponsaveisResponse.
 * 
 * @version $Revision$ $Date$
 */
public class DetalharTipoPendenciaResponsaveisResponse implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _codMensagem
     */
    private java.lang.String _codMensagem;

    /**
     * Field _mensagem
     */
    private java.lang.String _mensagem;

    /**
     * Field _cdPendenciaPagamentoIntegrado
     */
    private long _cdPendenciaPagamentoIntegrado = 0;

    /**
     * keeps track of state for field: _cdPendenciaPagamentoIntegrad
     */
    private boolean _has_cdPendenciaPagamentoIntegrado;

    /**
     * Field _cdPessoaJuridica
     */
    private long _cdPessoaJuridica = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridica
     */
    private boolean _has_cdPessoaJuridica;

    /**
     * Field _dsCodigoPessoaJuridica
     */
    private java.lang.String _dsCodigoPessoaJuridica;

    /**
     * Field _cdUnidadeOrganizacional
     */
    private int _cdUnidadeOrganizacional = 0;

    /**
     * keeps track of state for field: _cdUnidadeOrganizacional
     */
    private boolean _has_cdUnidadeOrganizacional;

    /**
     * Field _dsNumeroSequenciaUnidadeOrganizacional
     */
    private java.lang.String _dsNumeroSequenciaUnidadeOrganizacional;

    /**
     * Field _cdTipoUnidadeOrganizacional
     */
    private int _cdTipoUnidadeOrganizacional = 0;

    /**
     * keeps track of state for field: _cdTipoUnidadeOrganizacional
     */
    private boolean _has_cdTipoUnidadeOrganizacional;

    /**
     * Field _dsTipoUnidadeOrganizacional
     */
    private java.lang.String _dsTipoUnidadeOrganizacional;

    /**
     * Field _dsPendenciaPagamentoIntegrado
     */
    private java.lang.String _dsPendenciaPagamentoIntegrado;

    /**
     * Field _dsResumoPendenciaPagamento
     */
    private java.lang.String _dsResumoPendenciaPagamento;

    /**
     * Field _cdTipoBaixaPendencia
     */
    private int _cdTipoBaixaPendencia = 0;

    /**
     * keeps track of state for field: _cdTipoBaixaPendencia
     */
    private boolean _has_cdTipoBaixaPendencia;

    /**
     * Field _cdIndicadorResponsavelPagamento
     */
    private int _cdIndicadorResponsavelPagamento = 0;

    /**
     * keeps track of state for field:
     * _cdIndicadorResponsavelPagamento
     */
    private boolean _has_cdIndicadorResponsavelPagamento;

    /**
     * Field _dsEmailRespPgto
     */
    private java.lang.String _dsEmailRespPgto;

    /**
     * Field _cdCanalInclusao
     */
    private int _cdCanalInclusao = 0;

    /**
     * keeps track of state for field: _cdCanalInclusao
     */
    private boolean _has_cdCanalInclusao;

    /**
     * Field _dsCanalInclusao
     */
    private java.lang.String _dsCanalInclusao;

    /**
     * Field _cdAutenticacaoSegurancaInclusao
     */
    private java.lang.String _cdAutenticacaoSegurancaInclusao;

    /**
     * Field _nrOperacaoFluxoInclusao
     */
    private java.lang.String _nrOperacaoFluxoInclusao;

    /**
     * Field _hrInclusaoRegistro
     */
    private java.lang.String _hrInclusaoRegistro;

    /**
     * Field _cdCanalManutencao
     */
    private int _cdCanalManutencao = 0;

    /**
     * keeps track of state for field: _cdCanalManutencao
     */
    private boolean _has_cdCanalManutencao;

    /**
     * Field _dsCanalManutencao
     */
    private java.lang.String _dsCanalManutencao;

    /**
     * Field _cdAutenticacaoSegurancaManutencao
     */
    private java.lang.String _cdAutenticacaoSegurancaManutencao;

    /**
     * Field _nrOperacaoFluxoManutencao
     */
    private java.lang.String _nrOperacaoFluxoManutencao;

    /**
     * Field _hrManutencaoRegistroManutencao
     */
    private java.lang.String _hrManutencaoRegistroManutencao;


      //----------------/
     //- Constructors -/
    //----------------/

    public DetalharTipoPendenciaResponsaveisResponse() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalhartipopendenciaresponsaveis.response.DetalharTipoPendenciaResponsaveisResponse()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdCanalInclusao
     * 
     */
    public void deleteCdCanalInclusao()
    {
        this._has_cdCanalInclusao= false;
    } //-- void deleteCdCanalInclusao() 

    /**
     * Method deleteCdCanalManutencao
     * 
     */
    public void deleteCdCanalManutencao()
    {
        this._has_cdCanalManutencao= false;
    } //-- void deleteCdCanalManutencao() 

    /**
     * Method deleteCdIndicadorResponsavelPagamento
     * 
     */
    public void deleteCdIndicadorResponsavelPagamento()
    {
        this._has_cdIndicadorResponsavelPagamento= false;
    } //-- void deleteCdIndicadorResponsavelPagamento() 

    /**
     * Method deleteCdPendenciaPagamentoIntegrado
     * 
     */
    public void deleteCdPendenciaPagamentoIntegrado()
    {
        this._has_cdPendenciaPagamentoIntegrado= false;
    } //-- void deleteCdPendenciaPagamentoIntegrado() 

    /**
     * Method deleteCdPessoaJuridica
     * 
     */
    public void deleteCdPessoaJuridica()
    {
        this._has_cdPessoaJuridica= false;
    } //-- void deleteCdPessoaJuridica() 

    /**
     * Method deleteCdTipoBaixaPendencia
     * 
     */
    public void deleteCdTipoBaixaPendencia()
    {
        this._has_cdTipoBaixaPendencia= false;
    } //-- void deleteCdTipoBaixaPendencia() 

    /**
     * Method deleteCdTipoUnidadeOrganizacional
     * 
     */
    public void deleteCdTipoUnidadeOrganizacional()
    {
        this._has_cdTipoUnidadeOrganizacional= false;
    } //-- void deleteCdTipoUnidadeOrganizacional() 

    /**
     * Method deleteCdUnidadeOrganizacional
     * 
     */
    public void deleteCdUnidadeOrganizacional()
    {
        this._has_cdUnidadeOrganizacional= false;
    } //-- void deleteCdUnidadeOrganizacional() 

    /**
     * Returns the value of field
     * 'cdAutenticacaoSegurancaInclusao'.
     * 
     * @return String
     * @return the value of field 'cdAutenticacaoSegurancaInclusao'.
     */
    public java.lang.String getCdAutenticacaoSegurancaInclusao()
    {
        return this._cdAutenticacaoSegurancaInclusao;
    } //-- java.lang.String getCdAutenticacaoSegurancaInclusao() 

    /**
     * Returns the value of field
     * 'cdAutenticacaoSegurancaManutencao'.
     * 
     * @return String
     * @return the value of field
     * 'cdAutenticacaoSegurancaManutencao'.
     */
    public java.lang.String getCdAutenticacaoSegurancaManutencao()
    {
        return this._cdAutenticacaoSegurancaManutencao;
    } //-- java.lang.String getCdAutenticacaoSegurancaManutencao() 

    /**
     * Returns the value of field 'cdCanalInclusao'.
     * 
     * @return int
     * @return the value of field 'cdCanalInclusao'.
     */
    public int getCdCanalInclusao()
    {
        return this._cdCanalInclusao;
    } //-- int getCdCanalInclusao() 

    /**
     * Returns the value of field 'cdCanalManutencao'.
     * 
     * @return int
     * @return the value of field 'cdCanalManutencao'.
     */
    public int getCdCanalManutencao()
    {
        return this._cdCanalManutencao;
    } //-- int getCdCanalManutencao() 

    /**
     * Returns the value of field
     * 'cdIndicadorResponsavelPagamento'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorResponsavelPagamento'.
     */
    public int getCdIndicadorResponsavelPagamento()
    {
        return this._cdIndicadorResponsavelPagamento;
    } //-- int getCdIndicadorResponsavelPagamento() 

    /**
     * Returns the value of field 'cdPendenciaPagamentoIntegrado'.
     * 
     * @return long
     * @return the value of field 'cdPendenciaPagamentoIntegrado'.
     */
    public long getCdPendenciaPagamentoIntegrado()
    {
        return this._cdPendenciaPagamentoIntegrado;
    } //-- long getCdPendenciaPagamentoIntegrado() 

    /**
     * Returns the value of field 'cdPessoaJuridica'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridica'.
     */
    public long getCdPessoaJuridica()
    {
        return this._cdPessoaJuridica;
    } //-- long getCdPessoaJuridica() 

    /**
     * Returns the value of field 'cdTipoBaixaPendencia'.
     * 
     * @return int
     * @return the value of field 'cdTipoBaixaPendencia'.
     */
    public int getCdTipoBaixaPendencia()
    {
        return this._cdTipoBaixaPendencia;
    } //-- int getCdTipoBaixaPendencia() 

    /**
     * Returns the value of field 'cdTipoUnidadeOrganizacional'.
     * 
     * @return int
     * @return the value of field 'cdTipoUnidadeOrganizacional'.
     */
    public int getCdTipoUnidadeOrganizacional()
    {
        return this._cdTipoUnidadeOrganizacional;
    } //-- int getCdTipoUnidadeOrganizacional() 

    /**
     * Returns the value of field 'cdUnidadeOrganizacional'.
     * 
     * @return int
     * @return the value of field 'cdUnidadeOrganizacional'.
     */
    public int getCdUnidadeOrganizacional()
    {
        return this._cdUnidadeOrganizacional;
    } //-- int getCdUnidadeOrganizacional() 

    /**
     * Returns the value of field 'codMensagem'.
     * 
     * @return String
     * @return the value of field 'codMensagem'.
     */
    public java.lang.String getCodMensagem()
    {
        return this._codMensagem;
    } //-- java.lang.String getCodMensagem() 

    /**
     * Returns the value of field 'dsCanalInclusao'.
     * 
     * @return String
     * @return the value of field 'dsCanalInclusao'.
     */
    public java.lang.String getDsCanalInclusao()
    {
        return this._dsCanalInclusao;
    } //-- java.lang.String getDsCanalInclusao() 

    /**
     * Returns the value of field 'dsCanalManutencao'.
     * 
     * @return String
     * @return the value of field 'dsCanalManutencao'.
     */
    public java.lang.String getDsCanalManutencao()
    {
        return this._dsCanalManutencao;
    } //-- java.lang.String getDsCanalManutencao() 

    /**
     * Returns the value of field 'dsCodigoPessoaJuridica'.
     * 
     * @return String
     * @return the value of field 'dsCodigoPessoaJuridica'.
     */
    public java.lang.String getDsCodigoPessoaJuridica()
    {
        return this._dsCodigoPessoaJuridica;
    } //-- java.lang.String getDsCodigoPessoaJuridica() 

    /**
     * Returns the value of field 'dsEmailRespPgto'.
     * 
     * @return String
     * @return the value of field 'dsEmailRespPgto'.
     */
    public java.lang.String getDsEmailRespPgto()
    {
        return this._dsEmailRespPgto;
    } //-- java.lang.String getDsEmailRespPgto() 

    /**
     * Returns the value of field
     * 'dsNumeroSequenciaUnidadeOrganizacional'.
     * 
     * @return String
     * @return the value of field
     * 'dsNumeroSequenciaUnidadeOrganizacional'.
     */
    public java.lang.String getDsNumeroSequenciaUnidadeOrganizacional()
    {
        return this._dsNumeroSequenciaUnidadeOrganizacional;
    } //-- java.lang.String getDsNumeroSequenciaUnidadeOrganizacional() 

    /**
     * Returns the value of field 'dsPendenciaPagamentoIntegrado'.
     * 
     * @return String
     * @return the value of field 'dsPendenciaPagamentoIntegrado'.
     */
    public java.lang.String getDsPendenciaPagamentoIntegrado()
    {
        return this._dsPendenciaPagamentoIntegrado;
    } //-- java.lang.String getDsPendenciaPagamentoIntegrado() 

    /**
     * Returns the value of field 'dsResumoPendenciaPagamento'.
     * 
     * @return String
     * @return the value of field 'dsResumoPendenciaPagamento'.
     */
    public java.lang.String getDsResumoPendenciaPagamento()
    {
        return this._dsResumoPendenciaPagamento;
    } //-- java.lang.String getDsResumoPendenciaPagamento() 

    /**
     * Returns the value of field 'dsTipoUnidadeOrganizacional'.
     * 
     * @return String
     * @return the value of field 'dsTipoUnidadeOrganizacional'.
     */
    public java.lang.String getDsTipoUnidadeOrganizacional()
    {
        return this._dsTipoUnidadeOrganizacional;
    } //-- java.lang.String getDsTipoUnidadeOrganizacional() 

    /**
     * Returns the value of field 'hrInclusaoRegistro'.
     * 
     * @return String
     * @return the value of field 'hrInclusaoRegistro'.
     */
    public java.lang.String getHrInclusaoRegistro()
    {
        return this._hrInclusaoRegistro;
    } //-- java.lang.String getHrInclusaoRegistro() 

    /**
     * Returns the value of field 'hrManutencaoRegistroManutencao'.
     * 
     * @return String
     * @return the value of field 'hrManutencaoRegistroManutencao'.
     */
    public java.lang.String getHrManutencaoRegistroManutencao()
    {
        return this._hrManutencaoRegistroManutencao;
    } //-- java.lang.String getHrManutencaoRegistroManutencao() 

    /**
     * Returns the value of field 'mensagem'.
     * 
     * @return String
     * @return the value of field 'mensagem'.
     */
    public java.lang.String getMensagem()
    {
        return this._mensagem;
    } //-- java.lang.String getMensagem() 

    /**
     * Returns the value of field 'nrOperacaoFluxoInclusao'.
     * 
     * @return String
     * @return the value of field 'nrOperacaoFluxoInclusao'.
     */
    public java.lang.String getNrOperacaoFluxoInclusao()
    {
        return this._nrOperacaoFluxoInclusao;
    } //-- java.lang.String getNrOperacaoFluxoInclusao() 

    /**
     * Returns the value of field 'nrOperacaoFluxoManutencao'.
     * 
     * @return String
     * @return the value of field 'nrOperacaoFluxoManutencao'.
     */
    public java.lang.String getNrOperacaoFluxoManutencao()
    {
        return this._nrOperacaoFluxoManutencao;
    } //-- java.lang.String getNrOperacaoFluxoManutencao() 

    /**
     * Method hasCdCanalInclusao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCanalInclusao()
    {
        return this._has_cdCanalInclusao;
    } //-- boolean hasCdCanalInclusao() 

    /**
     * Method hasCdCanalManutencao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCanalManutencao()
    {
        return this._has_cdCanalManutencao;
    } //-- boolean hasCdCanalManutencao() 

    /**
     * Method hasCdIndicadorResponsavelPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorResponsavelPagamento()
    {
        return this._has_cdIndicadorResponsavelPagamento;
    } //-- boolean hasCdIndicadorResponsavelPagamento() 

    /**
     * Method hasCdPendenciaPagamentoIntegrado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPendenciaPagamentoIntegrado()
    {
        return this._has_cdPendenciaPagamentoIntegrado;
    } //-- boolean hasCdPendenciaPagamentoIntegrado() 

    /**
     * Method hasCdPessoaJuridica
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridica()
    {
        return this._has_cdPessoaJuridica;
    } //-- boolean hasCdPessoaJuridica() 

    /**
     * Method hasCdTipoBaixaPendencia
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoBaixaPendencia()
    {
        return this._has_cdTipoBaixaPendencia;
    } //-- boolean hasCdTipoBaixaPendencia() 

    /**
     * Method hasCdTipoUnidadeOrganizacional
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoUnidadeOrganizacional()
    {
        return this._has_cdTipoUnidadeOrganizacional;
    } //-- boolean hasCdTipoUnidadeOrganizacional() 

    /**
     * Method hasCdUnidadeOrganizacional
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdUnidadeOrganizacional()
    {
        return this._has_cdUnidadeOrganizacional;
    } //-- boolean hasCdUnidadeOrganizacional() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdAutenticacaoSegurancaInclusao'.
     * 
     * @param cdAutenticacaoSegurancaInclusao the value of field
     * 'cdAutenticacaoSegurancaInclusao'.
     */
    public void setCdAutenticacaoSegurancaInclusao(java.lang.String cdAutenticacaoSegurancaInclusao)
    {
        this._cdAutenticacaoSegurancaInclusao = cdAutenticacaoSegurancaInclusao;
    } //-- void setCdAutenticacaoSegurancaInclusao(java.lang.String) 

    /**
     * Sets the value of field 'cdAutenticacaoSegurancaManutencao'.
     * 
     * @param cdAutenticacaoSegurancaManutencao the value of field
     * 'cdAutenticacaoSegurancaManutencao'.
     */
    public void setCdAutenticacaoSegurancaManutencao(java.lang.String cdAutenticacaoSegurancaManutencao)
    {
        this._cdAutenticacaoSegurancaManutencao = cdAutenticacaoSegurancaManutencao;
    } //-- void setCdAutenticacaoSegurancaManutencao(java.lang.String) 

    /**
     * Sets the value of field 'cdCanalInclusao'.
     * 
     * @param cdCanalInclusao the value of field 'cdCanalInclusao'.
     */
    public void setCdCanalInclusao(int cdCanalInclusao)
    {
        this._cdCanalInclusao = cdCanalInclusao;
        this._has_cdCanalInclusao = true;
    } //-- void setCdCanalInclusao(int) 

    /**
     * Sets the value of field 'cdCanalManutencao'.
     * 
     * @param cdCanalManutencao the value of field
     * 'cdCanalManutencao'.
     */
    public void setCdCanalManutencao(int cdCanalManutencao)
    {
        this._cdCanalManutencao = cdCanalManutencao;
        this._has_cdCanalManutencao = true;
    } //-- void setCdCanalManutencao(int) 

    /**
     * Sets the value of field 'cdIndicadorResponsavelPagamento'.
     * 
     * @param cdIndicadorResponsavelPagamento the value of field
     * 'cdIndicadorResponsavelPagamento'.
     */
    public void setCdIndicadorResponsavelPagamento(int cdIndicadorResponsavelPagamento)
    {
        this._cdIndicadorResponsavelPagamento = cdIndicadorResponsavelPagamento;
        this._has_cdIndicadorResponsavelPagamento = true;
    } //-- void setCdIndicadorResponsavelPagamento(int) 

    /**
     * Sets the value of field 'cdPendenciaPagamentoIntegrado'.
     * 
     * @param cdPendenciaPagamentoIntegrado the value of field
     * 'cdPendenciaPagamentoIntegrado'.
     */
    public void setCdPendenciaPagamentoIntegrado(long cdPendenciaPagamentoIntegrado)
    {
        this._cdPendenciaPagamentoIntegrado = cdPendenciaPagamentoIntegrado;
        this._has_cdPendenciaPagamentoIntegrado = true;
    } //-- void setCdPendenciaPagamentoIntegrado(long) 

    /**
     * Sets the value of field 'cdPessoaJuridica'.
     * 
     * @param cdPessoaJuridica the value of field 'cdPessoaJuridica'
     */
    public void setCdPessoaJuridica(long cdPessoaJuridica)
    {
        this._cdPessoaJuridica = cdPessoaJuridica;
        this._has_cdPessoaJuridica = true;
    } //-- void setCdPessoaJuridica(long) 

    /**
     * Sets the value of field 'cdTipoBaixaPendencia'.
     * 
     * @param cdTipoBaixaPendencia the value of field
     * 'cdTipoBaixaPendencia'.
     */
    public void setCdTipoBaixaPendencia(int cdTipoBaixaPendencia)
    {
        this._cdTipoBaixaPendencia = cdTipoBaixaPendencia;
        this._has_cdTipoBaixaPendencia = true;
    } //-- void setCdTipoBaixaPendencia(int) 

    /**
     * Sets the value of field 'cdTipoUnidadeOrganizacional'.
     * 
     * @param cdTipoUnidadeOrganizacional the value of field
     * 'cdTipoUnidadeOrganizacional'.
     */
    public void setCdTipoUnidadeOrganizacional(int cdTipoUnidadeOrganizacional)
    {
        this._cdTipoUnidadeOrganizacional = cdTipoUnidadeOrganizacional;
        this._has_cdTipoUnidadeOrganizacional = true;
    } //-- void setCdTipoUnidadeOrganizacional(int) 

    /**
     * Sets the value of field 'cdUnidadeOrganizacional'.
     * 
     * @param cdUnidadeOrganizacional the value of field
     * 'cdUnidadeOrganizacional'.
     */
    public void setCdUnidadeOrganizacional(int cdUnidadeOrganizacional)
    {
        this._cdUnidadeOrganizacional = cdUnidadeOrganizacional;
        this._has_cdUnidadeOrganizacional = true;
    } //-- void setCdUnidadeOrganizacional(int) 

    /**
     * Sets the value of field 'codMensagem'.
     * 
     * @param codMensagem the value of field 'codMensagem'.
     */
    public void setCodMensagem(java.lang.String codMensagem)
    {
        this._codMensagem = codMensagem;
    } //-- void setCodMensagem(java.lang.String) 

    /**
     * Sets the value of field 'dsCanalInclusao'.
     * 
     * @param dsCanalInclusao the value of field 'dsCanalInclusao'.
     */
    public void setDsCanalInclusao(java.lang.String dsCanalInclusao)
    {
        this._dsCanalInclusao = dsCanalInclusao;
    } //-- void setDsCanalInclusao(java.lang.String) 

    /**
     * Sets the value of field 'dsCanalManutencao'.
     * 
     * @param dsCanalManutencao the value of field
     * 'dsCanalManutencao'.
     */
    public void setDsCanalManutencao(java.lang.String dsCanalManutencao)
    {
        this._dsCanalManutencao = dsCanalManutencao;
    } //-- void setDsCanalManutencao(java.lang.String) 

    /**
     * Sets the value of field 'dsCodigoPessoaJuridica'.
     * 
     * @param dsCodigoPessoaJuridica the value of field
     * 'dsCodigoPessoaJuridica'.
     */
    public void setDsCodigoPessoaJuridica(java.lang.String dsCodigoPessoaJuridica)
    {
        this._dsCodigoPessoaJuridica = dsCodigoPessoaJuridica;
    } //-- void setDsCodigoPessoaJuridica(java.lang.String) 

    /**
     * Sets the value of field 'dsEmailRespPgto'.
     * 
     * @param dsEmailRespPgto the value of field 'dsEmailRespPgto'.
     */
    public void setDsEmailRespPgto(java.lang.String dsEmailRespPgto)
    {
        this._dsEmailRespPgto = dsEmailRespPgto;
    } //-- void setDsEmailRespPgto(java.lang.String) 

    /**
     * Sets the value of field
     * 'dsNumeroSequenciaUnidadeOrganizacional'.
     * 
     * @param dsNumeroSequenciaUnidadeOrganizacional the value of
     * field 'dsNumeroSequenciaUnidadeOrganizacional'.
     */
    public void setDsNumeroSequenciaUnidadeOrganizacional(java.lang.String dsNumeroSequenciaUnidadeOrganizacional)
    {
        this._dsNumeroSequenciaUnidadeOrganizacional = dsNumeroSequenciaUnidadeOrganizacional;
    } //-- void setDsNumeroSequenciaUnidadeOrganizacional(java.lang.String) 

    /**
     * Sets the value of field 'dsPendenciaPagamentoIntegrado'.
     * 
     * @param dsPendenciaPagamentoIntegrado the value of field
     * 'dsPendenciaPagamentoIntegrado'.
     */
    public void setDsPendenciaPagamentoIntegrado(java.lang.String dsPendenciaPagamentoIntegrado)
    {
        this._dsPendenciaPagamentoIntegrado = dsPendenciaPagamentoIntegrado;
    } //-- void setDsPendenciaPagamentoIntegrado(java.lang.String) 

    /**
     * Sets the value of field 'dsResumoPendenciaPagamento'.
     * 
     * @param dsResumoPendenciaPagamento the value of field
     * 'dsResumoPendenciaPagamento'.
     */
    public void setDsResumoPendenciaPagamento(java.lang.String dsResumoPendenciaPagamento)
    {
        this._dsResumoPendenciaPagamento = dsResumoPendenciaPagamento;
    } //-- void setDsResumoPendenciaPagamento(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoUnidadeOrganizacional'.
     * 
     * @param dsTipoUnidadeOrganizacional the value of field
     * 'dsTipoUnidadeOrganizacional'.
     */
    public void setDsTipoUnidadeOrganizacional(java.lang.String dsTipoUnidadeOrganizacional)
    {
        this._dsTipoUnidadeOrganizacional = dsTipoUnidadeOrganizacional;
    } //-- void setDsTipoUnidadeOrganizacional(java.lang.String) 

    /**
     * Sets the value of field 'hrInclusaoRegistro'.
     * 
     * @param hrInclusaoRegistro the value of field
     * 'hrInclusaoRegistro'.
     */
    public void setHrInclusaoRegistro(java.lang.String hrInclusaoRegistro)
    {
        this._hrInclusaoRegistro = hrInclusaoRegistro;
    } //-- void setHrInclusaoRegistro(java.lang.String) 

    /**
     * Sets the value of field 'hrManutencaoRegistroManutencao'.
     * 
     * @param hrManutencaoRegistroManutencao the value of field
     * 'hrManutencaoRegistroManutencao'.
     */
    public void setHrManutencaoRegistroManutencao(java.lang.String hrManutencaoRegistroManutencao)
    {
        this._hrManutencaoRegistroManutencao = hrManutencaoRegistroManutencao;
    } //-- void setHrManutencaoRegistroManutencao(java.lang.String) 

    /**
     * Sets the value of field 'mensagem'.
     * 
     * @param mensagem the value of field 'mensagem'.
     */
    public void setMensagem(java.lang.String mensagem)
    {
        this._mensagem = mensagem;
    } //-- void setMensagem(java.lang.String) 

    /**
     * Sets the value of field 'nrOperacaoFluxoInclusao'.
     * 
     * @param nrOperacaoFluxoInclusao the value of field
     * 'nrOperacaoFluxoInclusao'.
     */
    public void setNrOperacaoFluxoInclusao(java.lang.String nrOperacaoFluxoInclusao)
    {
        this._nrOperacaoFluxoInclusao = nrOperacaoFluxoInclusao;
    } //-- void setNrOperacaoFluxoInclusao(java.lang.String) 

    /**
     * Sets the value of field 'nrOperacaoFluxoManutencao'.
     * 
     * @param nrOperacaoFluxoManutencao the value of field
     * 'nrOperacaoFluxoManutencao'.
     */
    public void setNrOperacaoFluxoManutencao(java.lang.String nrOperacaoFluxoManutencao)
    {
        this._nrOperacaoFluxoManutencao = nrOperacaoFluxoManutencao;
    } //-- void setNrOperacaoFluxoManutencao(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return DetalharTipoPendenciaResponsaveisResponse
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.detalhartipopendenciaresponsaveis.response.DetalharTipoPendenciaResponsaveisResponse unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.detalhartipopendenciaresponsaveis.response.DetalharTipoPendenciaResponsaveisResponse) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.detalhartipopendenciaresponsaveis.response.DetalharTipoPendenciaResponsaveisResponse.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalhartipopendenciaresponsaveis.response.DetalharTipoPendenciaResponsaveisResponse unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
