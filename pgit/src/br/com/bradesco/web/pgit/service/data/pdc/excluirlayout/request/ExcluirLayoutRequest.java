/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.excluirlayout.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ExcluirLayoutRequest.
 * 
 * @version $Revision$ $Date$
 */
public class ExcluirLayoutRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdPessoaJuridicaNegocio
     */
    private long _cdPessoaJuridicaNegocio = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridicaNegocio
     */
    private boolean _has_cdPessoaJuridicaNegocio;

    /**
     * Field _cdTipoContratoNegocio
     */
    private int _cdTipoContratoNegocio = 0;

    /**
     * keeps track of state for field: _cdTipoContratoNegocio
     */
    private boolean _has_cdTipoContratoNegocio;

    /**
     * Field _nrSequenciaContratoNegocio
     */
    private long _nrSequenciaContratoNegocio = 0;

    /**
     * keeps track of state for field: _nrSequenciaContratoNegocio
     */
    private boolean _has_nrSequenciaContratoNegocio;

    /**
     * Field _cdTipoLayoutArquivo
     */
    private int _cdTipoLayoutArquivo = 0;

    /**
     * keeps track of state for field: _cdTipoLayoutArquivo
     */
    private boolean _has_cdTipoLayoutArquivo;

    /**
     * Field _numeroVersaoLayoutArquivo
     */
    private int _numeroVersaoLayoutArquivo = 0;

    /**
     * keeps track of state for field: _numeroVersaoLayoutArquivo
     */
    private boolean _has_numeroVersaoLayoutArquivo;


      //----------------/
     //- Constructors -/
    //----------------/

    public ExcluirLayoutRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.excluirlayout.request.ExcluirLayoutRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdPessoaJuridicaNegocio
     * 
     */
    public void deleteCdPessoaJuridicaNegocio()
    {
        this._has_cdPessoaJuridicaNegocio= false;
    } //-- void deleteCdPessoaJuridicaNegocio() 

    /**
     * Method deleteCdTipoContratoNegocio
     * 
     */
    public void deleteCdTipoContratoNegocio()
    {
        this._has_cdTipoContratoNegocio= false;
    } //-- void deleteCdTipoContratoNegocio() 

    /**
     * Method deleteCdTipoLayoutArquivo
     * 
     */
    public void deleteCdTipoLayoutArquivo()
    {
        this._has_cdTipoLayoutArquivo= false;
    } //-- void deleteCdTipoLayoutArquivo() 

    /**
     * Method deleteNrSequenciaContratoNegocio
     * 
     */
    public void deleteNrSequenciaContratoNegocio()
    {
        this._has_nrSequenciaContratoNegocio= false;
    } //-- void deleteNrSequenciaContratoNegocio() 

    /**
     * Method deleteNumeroVersaoLayoutArquivo
     * 
     */
    public void deleteNumeroVersaoLayoutArquivo()
    {
        this._has_numeroVersaoLayoutArquivo= false;
    } //-- void deleteNumeroVersaoLayoutArquivo() 

    /**
     * Returns the value of field 'cdPessoaJuridicaNegocio'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridicaNegocio'.
     */
    public long getCdPessoaJuridicaNegocio()
    {
        return this._cdPessoaJuridicaNegocio;
    } //-- long getCdPessoaJuridicaNegocio() 

    /**
     * Returns the value of field 'cdTipoContratoNegocio'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoNegocio'.
     */
    public int getCdTipoContratoNegocio()
    {
        return this._cdTipoContratoNegocio;
    } //-- int getCdTipoContratoNegocio() 

    /**
     * Returns the value of field 'cdTipoLayoutArquivo'.
     * 
     * @return int
     * @return the value of field 'cdTipoLayoutArquivo'.
     */
    public int getCdTipoLayoutArquivo()
    {
        return this._cdTipoLayoutArquivo;
    } //-- int getCdTipoLayoutArquivo() 

    /**
     * Returns the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @return long
     * @return the value of field 'nrSequenciaContratoNegocio'.
     */
    public long getNrSequenciaContratoNegocio()
    {
        return this._nrSequenciaContratoNegocio;
    } //-- long getNrSequenciaContratoNegocio() 

    /**
     * Returns the value of field 'numeroVersaoLayoutArquivo'.
     * 
     * @return int
     * @return the value of field 'numeroVersaoLayoutArquivo'.
     */
    public int getNumeroVersaoLayoutArquivo()
    {
        return this._numeroVersaoLayoutArquivo;
    } //-- int getNumeroVersaoLayoutArquivo() 

    /**
     * Method hasCdPessoaJuridicaNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridicaNegocio()
    {
        return this._has_cdPessoaJuridicaNegocio;
    } //-- boolean hasCdPessoaJuridicaNegocio() 

    /**
     * Method hasCdTipoContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoNegocio()
    {
        return this._has_cdTipoContratoNegocio;
    } //-- boolean hasCdTipoContratoNegocio() 

    /**
     * Method hasCdTipoLayoutArquivo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoLayoutArquivo()
    {
        return this._has_cdTipoLayoutArquivo;
    } //-- boolean hasCdTipoLayoutArquivo() 

    /**
     * Method hasNrSequenciaContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaContratoNegocio()
    {
        return this._has_nrSequenciaContratoNegocio;
    } //-- boolean hasNrSequenciaContratoNegocio() 

    /**
     * Method hasNumeroVersaoLayoutArquivo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNumeroVersaoLayoutArquivo()
    {
        return this._has_numeroVersaoLayoutArquivo;
    } //-- boolean hasNumeroVersaoLayoutArquivo() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdPessoaJuridicaNegocio'.
     * 
     * @param cdPessoaJuridicaNegocio the value of field
     * 'cdPessoaJuridicaNegocio'.
     */
    public void setCdPessoaJuridicaNegocio(long cdPessoaJuridicaNegocio)
    {
        this._cdPessoaJuridicaNegocio = cdPessoaJuridicaNegocio;
        this._has_cdPessoaJuridicaNegocio = true;
    } //-- void setCdPessoaJuridicaNegocio(long) 

    /**
     * Sets the value of field 'cdTipoContratoNegocio'.
     * 
     * @param cdTipoContratoNegocio the value of field
     * 'cdTipoContratoNegocio'.
     */
    public void setCdTipoContratoNegocio(int cdTipoContratoNegocio)
    {
        this._cdTipoContratoNegocio = cdTipoContratoNegocio;
        this._has_cdTipoContratoNegocio = true;
    } //-- void setCdTipoContratoNegocio(int) 

    /**
     * Sets the value of field 'cdTipoLayoutArquivo'.
     * 
     * @param cdTipoLayoutArquivo the value of field
     * 'cdTipoLayoutArquivo'.
     */
    public void setCdTipoLayoutArquivo(int cdTipoLayoutArquivo)
    {
        this._cdTipoLayoutArquivo = cdTipoLayoutArquivo;
        this._has_cdTipoLayoutArquivo = true;
    } //-- void setCdTipoLayoutArquivo(int) 

    /**
     * Sets the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @param nrSequenciaContratoNegocio the value of field
     * 'nrSequenciaContratoNegocio'.
     */
    public void setNrSequenciaContratoNegocio(long nrSequenciaContratoNegocio)
    {
        this._nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
        this._has_nrSequenciaContratoNegocio = true;
    } //-- void setNrSequenciaContratoNegocio(long) 

    /**
     * Sets the value of field 'numeroVersaoLayoutArquivo'.
     * 
     * @param numeroVersaoLayoutArquivo the value of field
     * 'numeroVersaoLayoutArquivo'.
     */
    public void setNumeroVersaoLayoutArquivo(int numeroVersaoLayoutArquivo)
    {
        this._numeroVersaoLayoutArquivo = numeroVersaoLayoutArquivo;
        this._has_numeroVersaoLayoutArquivo = true;
    } //-- void setNumeroVersaoLayoutArquivo(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ExcluirLayoutRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.excluirlayout.request.ExcluirLayoutRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.excluirlayout.request.ExcluirLayoutRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.excluirlayout.request.ExcluirLayoutRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.excluirlayout.request.ExcluirLayoutRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
