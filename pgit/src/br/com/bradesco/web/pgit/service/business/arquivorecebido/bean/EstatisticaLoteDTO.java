/*
 * Nome: br.com.bradesco.web.pgit.service.business.arquivorecebido.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.arquivorecebido.bean;

/**
 * Nome: EstatisticaLoteDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class EstatisticaLoteDTO {
	
	/** Atributo instrucaoMovimento. */
	private String instrucaoMovimento;
	
	/** Atributo regConsistenteQtde. */
	private String regConsistenteQtde;
	
	/** Atributo regConsistenteValor. */
	private String regConsistenteValor;
	
	/** Atributo regInconsistenteQtde. */
	private String regInconsistenteQtde;
	
	/** Atributo regInconsistenteValor. */
	private String regInconsistenteValor;
	
	/** Atributo regTotalGeralQtde. */
	private String regTotalGeralQtde;
	
	/** Atributo regTotalGeralValor. */
	private String regTotalGeralValor;
	
	/** Atributo totalGeralQtde. */
	private long totalGeralQtde;
	
	/** Atributo totalGeralValor. */
	private double totalGeralValor;
	
	/** Atributo sistemaLegado. */
	private String sistemaLegado;
	
	/** Atributo cpfCnpj. */
	private String cpfCnpj;
	
	/** Atributo codigoFilialCnpj. */
	private String codigoFilialCnpj;
	
	/** Atributo codigoControleCpfCnpj. */
	private String codigoControleCpfCnpj;
	
	/** Atributo perfil. */
	private String perfil;
	
	/** Atributo numeroArquivoRemessa. */
	private String numeroArquivoRemessa;
	
	/** Atributo numeroLoteRemessa. */
	private String numeroLoteRemessa;
	
	/** Atributo numeroSequenciaRemessa. */
	private String numeroSequenciaRemessa;
	
	/** Atributo tipoPesquisa. */
	private String tipoPesquisa;
	
	/** Atributo recurso. */
	private String recurso;
	
	/** Atributo filler. */
	private String filler;
	
	/** Atributo qtdeInconsistentes. */
	private String qtdeInconsistentes;
	
	/** Atributo qtdeConsistentes. */
	private String qtdeConsistentes;
	
	/** Atributo somaValorInconsistentes. */
	private String somaValorInconsistentes;
	
	/** Atributo somaValorConsistentes. */
	private String somaValorConsistentes;
	
	/** Atributo dataInclusaoRemessa. */
	private String dataInclusaoRemessa;
	
	/** Atributo qtdeInconsitentesLong. */
	private long qtdeInconsitentesLong;
	
	/** Atributo qtdeConsitentesLong. */
	private long qtdeConsitentesLong;
	
	/** Atributo somaValorInconsistentesDouble. */
	private double somaValorInconsistentesDouble;
	
	/** Atributo somaValorConsistentesDouble. */
	private double somaValorConsistentesDouble;
	
	
	/**
	 * Get: codigoControleCpfCnpj.
	 *
	 * @return codigoControleCpfCnpj
	 */
	public String getCodigoControleCpfCnpj() {
		return codigoControleCpfCnpj;
	}
	
	/**
	 * Set: codigoControleCpfCnpj.
	 *
	 * @param codigoControleCpfCnpj the codigo controle cpf cnpj
	 */
	public void setCodigoControleCpfCnpj(String codigoControleCpfCnpj) {
		this.codigoControleCpfCnpj = codigoControleCpfCnpj;
	}
	
	/**
	 * Get: codigoFilialCnpj.
	 *
	 * @return codigoFilialCnpj
	 */
	public String getCodigoFilialCnpj() {
		return codigoFilialCnpj;
	}
	
	/**
	 * Set: codigoFilialCnpj.
	 *
	 * @param codigoFilialCnpj the codigo filial cnpj
	 */
	public void setCodigoFilialCnpj(String codigoFilialCnpj) {
		this.codigoFilialCnpj = codigoFilialCnpj;
	}
	
	/**
	 * Get: cpfCnpj.
	 *
	 * @return cpfCnpj
	 */
	public String getCpfCnpj() {
		return cpfCnpj;
	}
	
	/**
	 * Set: cpfCnpj.
	 *
	 * @param cpfCnpj the cpf cnpj
	 */
	public void setCpfCnpj(String cpfCnpj) {
		this.cpfCnpj = cpfCnpj;
	}
	
	/**
	 * Get: filler.
	 *
	 * @return filler
	 */
	public String getFiller() {
		return filler;
	}
	
	/**
	 * Set: filler.
	 *
	 * @param filler the filler
	 */
	public void setFiller(String filler) {
		this.filler = filler;
	}
	
	/**
	 * Get: numeroArquivoRemessa.
	 *
	 * @return numeroArquivoRemessa
	 */
	public String getNumeroArquivoRemessa() {
		return numeroArquivoRemessa;
	}
	
	/**
	 * Set: numeroArquivoRemessa.
	 *
	 * @param numeroArquivoRemessa the numero arquivo remessa
	 */
	public void setNumeroArquivoRemessa(String numeroArquivoRemessa) {
		this.numeroArquivoRemessa = numeroArquivoRemessa;
	}
	
	/**
	 * Get: numeroLoteRemessa.
	 *
	 * @return numeroLoteRemessa
	 */
	public String getNumeroLoteRemessa() {
		return numeroLoteRemessa;
	}
	
	/**
	 * Set: numeroLoteRemessa.
	 *
	 * @param numeroLoteRemessa the numero lote remessa
	 */
	public void setNumeroLoteRemessa(String numeroLoteRemessa) {
		this.numeroLoteRemessa = numeroLoteRemessa;
	}
	
	/**
	 * Get: numeroSequenciaRemessa.
	 *
	 * @return numeroSequenciaRemessa
	 */
	public String getNumeroSequenciaRemessa() {
		return numeroSequenciaRemessa;
	}
	
	/**
	 * Set: numeroSequenciaRemessa.
	 *
	 * @param numeroSequenciaRemessa the numero sequencia remessa
	 */
	public void setNumeroSequenciaRemessa(String numeroSequenciaRemessa) {
		this.numeroSequenciaRemessa = numeroSequenciaRemessa;
	}
	
	/**
	 * Get: perfil.
	 *
	 * @return perfil
	 */
	public String getPerfil() {
		return perfil;
	}
	
	/**
	 * Set: perfil.
	 *
	 * @param perfil the perfil
	 */
	public void setPerfil(String perfil) {
		this.perfil = perfil;
	}
	
	/**
	 * Get: recurso.
	 *
	 * @return recurso
	 */
	public String getRecurso() {
		return recurso;
	}
	
	/**
	 * Set: recurso.
	 *
	 * @param recurso the recurso
	 */
	public void setRecurso(String recurso) {
		this.recurso = recurso;
	}
	
	/**
	 * Get: sistemaLegado.
	 *
	 * @return sistemaLegado
	 */
	public String getSistemaLegado() {
		return sistemaLegado;
	}
	
	/**
	 * Set: sistemaLegado.
	 *
	 * @param sistemaLegado the sistema legado
	 */
	public void setSistemaLegado(String sistemaLegado) {
		this.sistemaLegado = sistemaLegado;
	}
	
	/**
	 * Get: tipoPesquisa.
	 *
	 * @return tipoPesquisa
	 */
	public String getTipoPesquisa() {
		return tipoPesquisa;
	}
	
	/**
	 * Set: tipoPesquisa.
	 *
	 * @param tipoPesquisa the tipo pesquisa
	 */
	public void setTipoPesquisa(String tipoPesquisa) {
		this.tipoPesquisa = tipoPesquisa;
	}
	
	/**
	 * Get: instrucaoMovimento.
	 *
	 * @return instrucaoMovimento
	 */
	public String getInstrucaoMovimento() {
		return instrucaoMovimento;
	}
	
	/**
	 * Set: instrucaoMovimento.
	 *
	 * @param instrucaoMovimento the instrucao movimento
	 */
	public void setInstrucaoMovimento(String instrucaoMovimento) {
		this.instrucaoMovimento = instrucaoMovimento;
	}
	
	/**
	 * Get: totalGeralQtde.
	 *
	 * @return totalGeralQtde
	 */
	public long getTotalGeralQtde() {
		return totalGeralQtde;
	}
	
	/**
	 * Set: totalGeralQtde.
	 *
	 * @param totalGeralQtde the total geral qtde
	 */
	public void setTotalGeralQtde(long totalGeralQtde) {
		this.totalGeralQtde = totalGeralQtde;
	}
	
	/**
	 * Get: totalGeralValor.
	 *
	 * @return totalGeralValor
	 */
	public double getTotalGeralValor() {
		return totalGeralValor;
	}
	
	/**
	 * Set: totalGeralValor.
	 *
	 * @param totalGeralValor the total geral valor
	 */
	public void setTotalGeralValor(double totalGeralValor) {
		this.totalGeralValor = totalGeralValor;
	}
	
	/**
	 * Get: qtdeConsistentes.
	 *
	 * @return qtdeConsistentes
	 */
	public String getQtdeConsistentes() {
		return qtdeConsistentes;
	}
	
	/**
	 * Set: qtdeConsistentes.
	 *
	 * @param qtdeConsistentes the qtde consistentes
	 */
	public void setQtdeConsistentes(String qtdeConsistentes) {
		this.qtdeConsistentes = qtdeConsistentes;
	}
	
	/**
	 * Get: qtdeInconsistentes.
	 *
	 * @return qtdeInconsistentes
	 */
	public String getQtdeInconsistentes() {
		return qtdeInconsistentes;
	}
	
	/**
	 * Set: qtdeInconsistentes.
	 *
	 * @param qtdeInconsistentes the qtde inconsistentes
	 */
	public void setQtdeInconsistentes(String qtdeInconsistentes) {
		this.qtdeInconsistentes = qtdeInconsistentes;
	}
	
	/**
	 * Get: somaValorConsistentes.
	 *
	 * @return somaValorConsistentes
	 */
	public String getSomaValorConsistentes() {
		return somaValorConsistentes;
	}
	
	/**
	 * Set: somaValorConsistentes.
	 *
	 * @param somaValorConsistentes the soma valor consistentes
	 */
	public void setSomaValorConsistentes(String somaValorConsistentes) {
		this.somaValorConsistentes = somaValorConsistentes;
	}
	
	/**
	 * Get: somaValorInconsistentes.
	 *
	 * @return somaValorInconsistentes
	 */
	public String getSomaValorInconsistentes() {
		return somaValorInconsistentes;
	}
	
	/**
	 * Set: somaValorInconsistentes.
	 *
	 * @param somaValorInconsistentes the soma valor inconsistentes
	 */
	public void setSomaValorInconsistentes(String somaValorInconsistentes) {
		this.somaValorInconsistentes = somaValorInconsistentes;
	}
	
	/**
	 * Get: qtdeConsitentesLong.
	 *
	 * @return qtdeConsitentesLong
	 */
	public long getQtdeConsitentesLong() {
		return qtdeConsitentesLong;
	}
	
	/**
	 * Set: qtdeConsitentesLong.
	 *
	 * @param qtdeConsitentesLong the qtde consitentes long
	 */
	public void setQtdeConsitentesLong(long qtdeConsitentesLong) {
		this.qtdeConsitentesLong = qtdeConsitentesLong;
	}
	
	/**
	 * Get: qtdeInconsitentesLong.
	 *
	 * @return qtdeInconsitentesLong
	 */
	public long getQtdeInconsitentesLong() {
		return qtdeInconsitentesLong;
	}
	
	/**
	 * Set: qtdeInconsitentesLong.
	 *
	 * @param qtdeInconsitentesLong the qtde inconsitentes long
	 */
	public void setQtdeInconsitentesLong(long qtdeInconsitentesLong) {
		this.qtdeInconsitentesLong = qtdeInconsitentesLong;
	}
	
	/**
	 * Get: somaValorConsistentesDouble.
	 *
	 * @return somaValorConsistentesDouble
	 */
	public double getSomaValorConsistentesDouble() {
		return somaValorConsistentesDouble;
	}
	
	/**
	 * Set: somaValorConsistentesDouble.
	 *
	 * @param somaValorConsistentesDouble the soma valor consistentes double
	 */
	public void setSomaValorConsistentesDouble(double somaValorConsistentesDouble) {
		this.somaValorConsistentesDouble = somaValorConsistentesDouble;
	}
	
	/**
	 * Get: somaValorInconsistentesDouble.
	 *
	 * @return somaValorInconsistentesDouble
	 */
	public double getSomaValorInconsistentesDouble() {
		return somaValorInconsistentesDouble;
	}
	
	/**
	 * Set: somaValorInconsistentesDouble.
	 *
	 * @param somaValorInconsistentesDouble the soma valor inconsistentes double
	 */
	public void setSomaValorInconsistentesDouble(
			double somaValorInconsistentesDouble) {
		this.somaValorInconsistentesDouble = somaValorInconsistentesDouble;
	}
	
	/**
	 * Get: dataInclusaoRemessa.
	 *
	 * @return dataInclusaoRemessa
	 */
	public String getDataInclusaoRemessa() {
		return dataInclusaoRemessa;
	}
	
	/**
	 * Set: dataInclusaoRemessa.
	 *
	 * @param dataInclusaoRemessa the data inclusao remessa
	 */
	public void setDataInclusaoRemessa(String dataInclusaoRemessa) {
		this.dataInclusaoRemessa = dataInclusaoRemessa;
	}
	
	/**
	 * Get: regTotalGeralQtde.
	 *
	 * @return regTotalGeralQtde
	 */
	public String getRegTotalGeralQtde() {
		return regTotalGeralQtde;
	}
	
	/**
	 * Set: regTotalGeralQtde.
	 *
	 * @param regTotalGeralQtde the reg total geral qtde
	 */
	public void setRegTotalGeralQtde(String regTotalGeralQtde) {
		this.regTotalGeralQtde = regTotalGeralQtde;
	}
	
	/**
	 * Get: regTotalGeralValor.
	 *
	 * @return regTotalGeralValor
	 */
	public String getRegTotalGeralValor() {
		return regTotalGeralValor;
	}
	
	/**
	 * Set: regTotalGeralValor.
	 *
	 * @param regTotalGeralValor the reg total geral valor
	 */
	public void setRegTotalGeralValor(String regTotalGeralValor) {
		this.regTotalGeralValor = regTotalGeralValor;
	}
	
	/**
	 * Get: regConsistenteQtde.
	 *
	 * @return regConsistenteQtde
	 */
	public String getRegConsistenteQtde() {
		return regConsistenteQtde;
	}
	
	/**
	 * Set: regConsistenteQtde.
	 *
	 * @param regConsistenteQtde the reg consistente qtde
	 */
	public void setRegConsistenteQtde(String regConsistenteQtde) {
		this.regConsistenteQtde = regConsistenteQtde;
	}
	
	/**
	 * Get: regConsistenteValor.
	 *
	 * @return regConsistenteValor
	 */
	public String getRegConsistenteValor() {
		return regConsistenteValor;
	}
	
	/**
	 * Set: regConsistenteValor.
	 *
	 * @param regConsistenteValor the reg consistente valor
	 */
	public void setRegConsistenteValor(String regConsistenteValor) {
		this.regConsistenteValor = regConsistenteValor;
	}
	
	/**
	 * Get: regInconsistenteQtde.
	 *
	 * @return regInconsistenteQtde
	 */
	public String getRegInconsistenteQtde() {
		return regInconsistenteQtde;
	}
	
	/**
	 * Set: regInconsistenteQtde.
	 *
	 * @param regInconsistenteQtde the reg inconsistente qtde
	 */
	public void setRegInconsistenteQtde(String regInconsistenteQtde) {
		this.regInconsistenteQtde = regInconsistenteQtde;
	}
	
	/**
	 * Get: regInconsistenteValor.
	 *
	 * @return regInconsistenteValor
	 */
	public String getRegInconsistenteValor() {
		return regInconsistenteValor;
	}
	
	/**
	 * Set: regInconsistenteValor.
	 *
	 * @param regInconsistenteValor the reg inconsistente valor
	 */
	public void setRegInconsistenteValor(String regInconsistenteValor) {
		this.regInconsistenteValor = regInconsistenteValor;
	}
	

}
