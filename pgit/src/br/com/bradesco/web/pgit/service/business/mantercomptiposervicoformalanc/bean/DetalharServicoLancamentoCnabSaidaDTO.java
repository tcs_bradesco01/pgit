/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantercomptiposervicoformalanc.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantercomptiposervicoformalanc.bean;

/**
 * Nome: DetalharServicoLancamentoCnabSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class DetalharServicoLancamentoCnabSaidaDTO{
	
	/** Atributo codMensagem. */
	private String codMensagem;
	
	/** Atributo mensagem. */
	private String mensagem;
	
	/** Atributo cdFormaLancamentoCnab. */
	private Integer cdFormaLancamentoCnab;
	
	/** Atributo cdTipoServicoCnab. */
	private Integer cdTipoServicoCnab;
	
	/** Atributo cdFormaLiquidacao. */
	private Integer cdFormaLiquidacao;
	
	/** Atributo cdProdutoServicoOperacao. */
	private Integer cdProdutoServicoOperacao;
	
	/** Atributo cdProdutoOperacaoRelacionado. */
	private Integer cdProdutoOperacaoRelacionado;
	
	/** Atributo cdUsuarioInclusao. */
	private String cdUsuarioInclusao;
	
	/** Atributo hrInclusaoRegistro. */
	private String hrInclusaoRegistro;
	
	/** Atributo cdOperacaoCanalInclusao. */
	private String cdOperacaoCanalInclusao;
	
	/** Atributo cdTipoCanalInclusao. */
	private Integer cdTipoCanalInclusao;
	
	/** Atributo cdUsuarioManutencao. */
	private String cdUsuarioManutencao;
	
	/** Atributo hrManutencaoRegistro. */
	private String hrManutencaoRegistro;
	
	/** Atributo cdOperacaoCanalManutencao. */
	private String cdOperacaoCanalManutencao;
	
	/** Atributo cdTipoCanalManutencao. */
	private Integer cdTipoCanalManutencao;	
       
        
        /** Atributo dsFormaLancamentoCnab. */
        private String  dsFormaLancamentoCnab;
        
        /** Atributo dsTipoServicoCnab. */
        private String  dsTipoServicoCnab;
        
        /** Atributo dsFormaLiquidacao. */
        private String  dsFormaLiquidacao;
        
        /** Atributo dsProdutoServicoOperacao. */
        private String  dsProdutoServicoOperacao;        
        
        /** Atributo dsProdutoServicoRelacionado. */
        private String  dsProdutoServicoRelacionado;
                
        /** Atributo cdRelacionamentoProduto. */
        private Integer cdRelacionamentoProduto;                
        
        /** Atributo dsCanalInclusao. */
        private String dsCanalInclusao;
        
        /** Atributo dsCanalManutencao. */
        private String dsCanalManutencao;
        
	/**
	 * Get: dsCanalInclusao.
	 *
	 * @return dsCanalInclusao
	 */
	public String getDsCanalInclusao() {
	    return dsCanalInclusao;
	}

	/**
	 * Set: dsCanalInclusao.
	 *
	 * @param dsCanalInclusao the ds canal inclusao
	 */
	public void setDsCanalInclusao(String dsCanalInclusao) {
	    this.dsCanalInclusao = dsCanalInclusao;
	}

	/**
	 * Get: dsCanalManutencao.
	 *
	 * @return dsCanalManutencao
	 */
	public String getDsCanalManutencao() {
	    return dsCanalManutencao;
	}

	/**
	 * Set: dsCanalManutencao.
	 *
	 * @param dsCanalManutencao the ds canal manutencao
	 */
	public void setDsCanalManutencao(String dsCanalManutencao) {
	    this.dsCanalManutencao = dsCanalManutencao;
	}

	/**
	 * Get: dsFormaLancamentoCnab.
	 *
	 * @return dsFormaLancamentoCnab
	 */
	public String getDsFormaLancamentoCnab() {
	    return dsFormaLancamentoCnab;
	}

	/**
	 * Set: dsFormaLancamentoCnab.
	 *
	 * @param dsFormaLancamentoCnab the ds forma lancamento cnab
	 */
	public void setDsFormaLancamentoCnab(String dsFormaLancamentoCnab) {
	    this.dsFormaLancamentoCnab = dsFormaLancamentoCnab;
	}

	/**
	 * Get: dsFormaLiquidacao.
	 *
	 * @return dsFormaLiquidacao
	 */
	public String getDsFormaLiquidacao() {
	    return dsFormaLiquidacao;
	}

	/**
	 * Set: dsFormaLiquidacao.
	 *
	 * @param dsFormaLiquidacao the ds forma liquidacao
	 */
	public void setDsFormaLiquidacao(String dsFormaLiquidacao) {
	    this.dsFormaLiquidacao = dsFormaLiquidacao;
	}

	/**
	 * Get: dsProdutoServicoOperacao.
	 *
	 * @return dsProdutoServicoOperacao
	 */
	public String getDsProdutoServicoOperacao() {
	    return dsProdutoServicoOperacao;
	}

	/**
	 * Set: dsProdutoServicoOperacao.
	 *
	 * @param dsProdutoServicoOperacao the ds produto servico operacao
	 */
	public void setDsProdutoServicoOperacao(String dsProdutoServicoOperacao) {
	    this.dsProdutoServicoOperacao = dsProdutoServicoOperacao;
	}

	/**
	 * Get: dsProdutoServicoRelacionado.
	 *
	 * @return dsProdutoServicoRelacionado
	 */
	public String getDsProdutoServicoRelacionado() {
	    return dsProdutoServicoRelacionado;
	}

	/**
	 * Set: dsProdutoServicoRelacionado.
	 *
	 * @param dsProdutoServicoRelacionado the ds produto servico relacionado
	 */
	public void setDsProdutoServicoRelacionado(String dsProdutoServicoRelacionado) {
	    this.dsProdutoServicoRelacionado = dsProdutoServicoRelacionado;
	}

	/**
	 * Get: dsTipoServicoCnab.
	 *
	 * @return dsTipoServicoCnab
	 */
	public String getDsTipoServicoCnab() {
	    return dsTipoServicoCnab;
	}

	/**
	 * Set: dsTipoServicoCnab.
	 *
	 * @param dsTipoServicoCnab the ds tipo servico cnab
	 */
	public void setDsTipoServicoCnab(String dsTipoServicoCnab) {
	    this.dsTipoServicoCnab = dsTipoServicoCnab;
	}

	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem(){
		return codMensagem;
	}

	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem){
		this.codMensagem = codMensagem;
	}

	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem(){
		return mensagem;
	}

	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem){
		this.mensagem = mensagem;
	}

	/**
	 * Get: cdFormaLancamentoCnab.
	 *
	 * @return cdFormaLancamentoCnab
	 */
	public Integer getCdFormaLancamentoCnab(){
		return cdFormaLancamentoCnab;
	}

	/**
	 * Set: cdFormaLancamentoCnab.
	 *
	 * @param cdFormaLancamentoCnab the cd forma lancamento cnab
	 */
	public void setCdFormaLancamentoCnab(Integer cdFormaLancamentoCnab){
		this.cdFormaLancamentoCnab = cdFormaLancamentoCnab;
	}

	/**
	 * Get: cdTipoServicoCnab.
	 *
	 * @return cdTipoServicoCnab
	 */
	public Integer getCdTipoServicoCnab(){
		return cdTipoServicoCnab;
	}

	/**
	 * Set: cdTipoServicoCnab.
	 *
	 * @param cdTipoServicoCnab the cd tipo servico cnab
	 */
	public void setCdTipoServicoCnab(Integer cdTipoServicoCnab){
		this.cdTipoServicoCnab = cdTipoServicoCnab;
	}

	/**
	 * Get: cdFormaLiquidacao.
	 *
	 * @return cdFormaLiquidacao
	 */
	public Integer getCdFormaLiquidacao(){
		return cdFormaLiquidacao;
	}

	/**
	 * Set: cdFormaLiquidacao.
	 *
	 * @param cdFormaLiquidacao the cd forma liquidacao
	 */
	public void setCdFormaLiquidacao(Integer cdFormaLiquidacao){
		this.cdFormaLiquidacao = cdFormaLiquidacao;
	}

	/**
	 * Get: cdProdutoServicoOperacao.
	 *
	 * @return cdProdutoServicoOperacao
	 */
	public Integer getCdProdutoServicoOperacao(){
		return cdProdutoServicoOperacao;
	}

	/**
	 * Set: cdProdutoServicoOperacao.
	 *
	 * @param cdProdutoServicoOperacao the cd produto servico operacao
	 */
	public void setCdProdutoServicoOperacao(Integer cdProdutoServicoOperacao){
		this.cdProdutoServicoOperacao = cdProdutoServicoOperacao;
	}

	/**
	 * Get: cdProdutoOperacaoRelacionado.
	 *
	 * @return cdProdutoOperacaoRelacionado
	 */
	public Integer getCdProdutoOperacaoRelacionado(){
		return cdProdutoOperacaoRelacionado;
	}

	/**
	 * Set: cdProdutoOperacaoRelacionado.
	 *
	 * @param cdProdutoOperacaoRelacionado the cd produto operacao relacionado
	 */
	public void setCdProdutoOperacaoRelacionado(Integer cdProdutoOperacaoRelacionado){
		this.cdProdutoOperacaoRelacionado = cdProdutoOperacaoRelacionado;
	}

	/**
	 * Get: cdUsuarioInclusao.
	 *
	 * @return cdUsuarioInclusao
	 */
	public String getCdUsuarioInclusao(){
		return cdUsuarioInclusao;
	}

	/**
	 * Set: cdUsuarioInclusao.
	 *
	 * @param cdUsuarioInclusao the cd usuario inclusao
	 */
	public void setCdUsuarioInclusao(String cdUsuarioInclusao){
		this.cdUsuarioInclusao = cdUsuarioInclusao;
	}

	/**
	 * Get: hrInclusaoRegistro.
	 *
	 * @return hrInclusaoRegistro
	 */
	public String getHrInclusaoRegistro(){
		return hrInclusaoRegistro;
	}

	/**
	 * Set: hrInclusaoRegistro.
	 *
	 * @param hrInclusaoRegistro the hr inclusao registro
	 */
	public void setHrInclusaoRegistro(String hrInclusaoRegistro){
		this.hrInclusaoRegistro = hrInclusaoRegistro;
	}

	/**
	 * Get: cdOperacaoCanalInclusao.
	 *
	 * @return cdOperacaoCanalInclusao
	 */
	public String getCdOperacaoCanalInclusao(){
		return cdOperacaoCanalInclusao;
	}

	/**
	 * Set: cdOperacaoCanalInclusao.
	 *
	 * @param cdOperacaoCanalInclusao the cd operacao canal inclusao
	 */
	public void setCdOperacaoCanalInclusao(String cdOperacaoCanalInclusao){
		this.cdOperacaoCanalInclusao = cdOperacaoCanalInclusao;
	}

	/**
	 * Get: cdTipoCanalInclusao.
	 *
	 * @return cdTipoCanalInclusao
	 */
	public Integer getCdTipoCanalInclusao(){
		return cdTipoCanalInclusao;
	}

	/**
	 * Set: cdTipoCanalInclusao.
	 *
	 * @param cdTipoCanalInclusao the cd tipo canal inclusao
	 */
	public void setCdTipoCanalInclusao(Integer cdTipoCanalInclusao){
		this.cdTipoCanalInclusao = cdTipoCanalInclusao;
	}

	/**
	 * Get: cdUsuarioManutencao.
	 *
	 * @return cdUsuarioManutencao
	 */
	public String getCdUsuarioManutencao(){
		return cdUsuarioManutencao;
	}

	/**
	 * Set: cdUsuarioManutencao.
	 *
	 * @param cdUsuarioManutencao the cd usuario manutencao
	 */
	public void setCdUsuarioManutencao(String cdUsuarioManutencao){
		this.cdUsuarioManutencao = cdUsuarioManutencao;
	}

	/**
	 * Get: hrManutencaoRegistro.
	 *
	 * @return hrManutencaoRegistro
	 */
	public String getHrManutencaoRegistro(){
		return hrManutencaoRegistro;
	}

	/**
	 * Set: hrManutencaoRegistro.
	 *
	 * @param hrManutencaoRegistro the hr manutencao registro
	 */
	public void setHrManutencaoRegistro(String hrManutencaoRegistro){
		this.hrManutencaoRegistro = hrManutencaoRegistro;
	}

	/**
	 * Get: cdOperacaoCanalManutencao.
	 *
	 * @return cdOperacaoCanalManutencao
	 */
	public String getCdOperacaoCanalManutencao(){
		return cdOperacaoCanalManutencao;
	}

	/**
	 * Set: cdOperacaoCanalManutencao.
	 *
	 * @param cdOperacaoCanalManutencao the cd operacao canal manutencao
	 */
	public void setCdOperacaoCanalManutencao(String cdOperacaoCanalManutencao){
		this.cdOperacaoCanalManutencao = cdOperacaoCanalManutencao;
	}

	/**
	 * Get: cdTipoCanalManutencao.
	 *
	 * @return cdTipoCanalManutencao
	 */
	public Integer getCdTipoCanalManutencao(){
		return cdTipoCanalManutencao;
	}

	/**
	 * Set: cdTipoCanalManutencao.
	 *
	 * @param cdTipoCanalManutencao the cd tipo canal manutencao
	 */
	public void setCdTipoCanalManutencao(Integer cdTipoCanalManutencao){
		this.cdTipoCanalManutencao = cdTipoCanalManutencao;
	}

	/**
	 * Get: cdRelacionamentoProduto.
	 *
	 * @return cdRelacionamentoProduto
	 */
	public Integer getCdRelacionamentoProduto() {
	    return cdRelacionamentoProduto;
	}

	/**
	 * Set: cdRelacionamentoProduto.
	 *
	 * @param cdRelacionamentoProduto the cd relacionamento produto
	 */
	public void setCdRelacionamentoProduto(Integer cdRelacionamentoProduto) {
	    this.cdRelacionamentoProduto = cdRelacionamentoProduto;
	}
}