/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.detalharpagtosconsautorizados.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.math.BigDecimal;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _nrPagamento
     */
    private java.lang.String _nrPagamento;

    /**
     * Field _vlPagamento
     */
    private java.math.BigDecimal _vlPagamento = new java.math.BigDecimal("0");

    /**
     * Field _cdCnpjCpfFavorecido
     */
    private long _cdCnpjCpfFavorecido = 0;

    /**
     * keeps track of state for field: _cdCnpjCpfFavorecido
     */
    private boolean _has_cdCnpjCpfFavorecido;

    /**
     * Field _cdFilialCnpjCpfFavorecido
     */
    private int _cdFilialCnpjCpfFavorecido = 0;

    /**
     * keeps track of state for field: _cdFilialCnpjCpfFavorecido
     */
    private boolean _has_cdFilialCnpjCpfFavorecido;

    /**
     * Field _cdControleCnpjCpfFavorecido
     */
    private int _cdControleCnpjCpfFavorecido = 0;

    /**
     * keeps track of state for field: _cdControleCnpjCpfFavorecido
     */
    private boolean _has_cdControleCnpjCpfFavorecido;

    /**
     * Field _cdFavorecido
     */
    private long _cdFavorecido = 0;

    /**
     * keeps track of state for field: _cdFavorecido
     */
    private boolean _has_cdFavorecido;

    /**
     * Field _dsBeneficio
     */
    private java.lang.String _dsBeneficio;

    /**
     * Field _cdBanco
     */
    private int _cdBanco = 0;

    /**
     * keeps track of state for field: _cdBanco
     */
    private boolean _has_cdBanco;

    /**
     * Field _cdAgencia
     */
    private int _cdAgencia = 0;

    /**
     * keeps track of state for field: _cdAgencia
     */
    private boolean _has_cdAgencia;

    /**
     * Field _cdDigitoAgencia
     */
    private java.lang.String _cdDigitoAgencia;

    /**
     * Field _cdConta
     */
    private long _cdConta = 0;

    /**
     * keeps track of state for field: _cdConta
     */
    private boolean _has_cdConta;

    /**
     * Field _cdDigitoConta
     */
    private java.lang.String _cdDigitoConta;

    /**
     * Field _cdTipoCanal
     */
    private int _cdTipoCanal = 0;

    /**
     * keeps track of state for field: _cdTipoCanal
     */
    private boolean _has_cdTipoCanal;

    /**
     * Field _cdTipoTela
     */
    private int _cdTipoTela = 0;

    /**
     * keeps track of state for field: _cdTipoTela
     */
    private boolean _has_cdTipoTela;

    /**
     * Field _dsEfetivacaoPagamento
     */
    private java.lang.String _dsEfetivacaoPagamento;

    /**
     * Field _dsIndicadorAutorizacao
     */
    private java.lang.String _dsIndicadorAutorizacao;

    /**
     * Field _cdLoteInterno
     */
    private long _cdLoteInterno = 0;

    /**
     * keeps track of state for field: _cdLoteInterno
     */
    private boolean _has_cdLoteInterno;

    /**
     * Field _dsTipoLayout
     */
    private java.lang.String _dsTipoLayout;

    /**
     * Field _dsIndicadorPagamento
     */
    private java.lang.String _dsIndicadorPagamento;

    /**
     * Field _cdIspbPagtoDestino
     */
    private java.lang.String _cdIspbPagtoDestino;

    /**
     * Field _contaPagtoDestino
     */
    private java.lang.String _contaPagtoDestino = "0";


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
        setVlPagamento(new java.math.BigDecimal("0"));
        setContaPagtoDestino("0");
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharpagtosconsautorizados.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdAgencia
     * 
     */
    public void deleteCdAgencia()
    {
        this._has_cdAgencia= false;
    } //-- void deleteCdAgencia() 

    /**
     * Method deleteCdBanco
     * 
     */
    public void deleteCdBanco()
    {
        this._has_cdBanco= false;
    } //-- void deleteCdBanco() 

    /**
     * Method deleteCdCnpjCpfFavorecido
     * 
     */
    public void deleteCdCnpjCpfFavorecido()
    {
        this._has_cdCnpjCpfFavorecido= false;
    } //-- void deleteCdCnpjCpfFavorecido() 

    /**
     * Method deleteCdConta
     * 
     */
    public void deleteCdConta()
    {
        this._has_cdConta= false;
    } //-- void deleteCdConta() 

    /**
     * Method deleteCdControleCnpjCpfFavorecido
     * 
     */
    public void deleteCdControleCnpjCpfFavorecido()
    {
        this._has_cdControleCnpjCpfFavorecido= false;
    } //-- void deleteCdControleCnpjCpfFavorecido() 

    /**
     * Method deleteCdFavorecido
     * 
     */
    public void deleteCdFavorecido()
    {
        this._has_cdFavorecido= false;
    } //-- void deleteCdFavorecido() 

    /**
     * Method deleteCdFilialCnpjCpfFavorecido
     * 
     */
    public void deleteCdFilialCnpjCpfFavorecido()
    {
        this._has_cdFilialCnpjCpfFavorecido= false;
    } //-- void deleteCdFilialCnpjCpfFavorecido() 

    /**
     * Method deleteCdLoteInterno
     * 
     */
    public void deleteCdLoteInterno()
    {
        this._has_cdLoteInterno= false;
    } //-- void deleteCdLoteInterno() 

    /**
     * Method deleteCdTipoCanal
     * 
     */
    public void deleteCdTipoCanal()
    {
        this._has_cdTipoCanal= false;
    } //-- void deleteCdTipoCanal() 

    /**
     * Method deleteCdTipoTela
     * 
     */
    public void deleteCdTipoTela()
    {
        this._has_cdTipoTela= false;
    } //-- void deleteCdTipoTela() 

    /**
     * Returns the value of field 'cdAgencia'.
     * 
     * @return int
     * @return the value of field 'cdAgencia'.
     */
    public int getCdAgencia()
    {
        return this._cdAgencia;
    } //-- int getCdAgencia() 

    /**
     * Returns the value of field 'cdBanco'.
     * 
     * @return int
     * @return the value of field 'cdBanco'.
     */
    public int getCdBanco()
    {
        return this._cdBanco;
    } //-- int getCdBanco() 

    /**
     * Returns the value of field 'cdCnpjCpfFavorecido'.
     * 
     * @return long
     * @return the value of field 'cdCnpjCpfFavorecido'.
     */
    public long getCdCnpjCpfFavorecido()
    {
        return this._cdCnpjCpfFavorecido;
    } //-- long getCdCnpjCpfFavorecido() 

    /**
     * Returns the value of field 'cdConta'.
     * 
     * @return long
     * @return the value of field 'cdConta'.
     */
    public long getCdConta()
    {
        return this._cdConta;
    } //-- long getCdConta() 

    /**
     * Returns the value of field 'cdControleCnpjCpfFavorecido'.
     * 
     * @return int
     * @return the value of field 'cdControleCnpjCpfFavorecido'.
     */
    public int getCdControleCnpjCpfFavorecido()
    {
        return this._cdControleCnpjCpfFavorecido;
    } //-- int getCdControleCnpjCpfFavorecido() 

    /**
     * Returns the value of field 'cdDigitoAgencia'.
     * 
     * @return String
     * @return the value of field 'cdDigitoAgencia'.
     */
    public java.lang.String getCdDigitoAgencia()
    {
        return this._cdDigitoAgencia;
    } //-- java.lang.String getCdDigitoAgencia() 

    /**
     * Returns the value of field 'cdDigitoConta'.
     * 
     * @return String
     * @return the value of field 'cdDigitoConta'.
     */
    public java.lang.String getCdDigitoConta()
    {
        return this._cdDigitoConta;
    } //-- java.lang.String getCdDigitoConta() 

    /**
     * Returns the value of field 'cdFavorecido'.
     * 
     * @return long
     * @return the value of field 'cdFavorecido'.
     */
    public long getCdFavorecido()
    {
        return this._cdFavorecido;
    } //-- long getCdFavorecido() 

    /**
     * Returns the value of field 'cdFilialCnpjCpfFavorecido'.
     * 
     * @return int
     * @return the value of field 'cdFilialCnpjCpfFavorecido'.
     */
    public int getCdFilialCnpjCpfFavorecido()
    {
        return this._cdFilialCnpjCpfFavorecido;
    } //-- int getCdFilialCnpjCpfFavorecido() 

    /**
     * Returns the value of field 'cdIspbPagtoDestino'.
     * 
     * @return String
     * @return the value of field 'cdIspbPagtoDestino'.
     */
    public java.lang.String getCdIspbPagtoDestino()
    {
        return this._cdIspbPagtoDestino;
    } //-- java.lang.String getCdIspbPagtoDestino() 

    /**
     * Returns the value of field 'cdLoteInterno'.
     * 
     * @return long
     * @return the value of field 'cdLoteInterno'.
     */
    public long getCdLoteInterno()
    {
        return this._cdLoteInterno;
    } //-- long getCdLoteInterno() 

    /**
     * Returns the value of field 'cdTipoCanal'.
     * 
     * @return int
     * @return the value of field 'cdTipoCanal'.
     */
    public int getCdTipoCanal()
    {
        return this._cdTipoCanal;
    } //-- int getCdTipoCanal() 

    /**
     * Returns the value of field 'cdTipoTela'.
     * 
     * @return int
     * @return the value of field 'cdTipoTela'.
     */
    public int getCdTipoTela()
    {
        return this._cdTipoTela;
    } //-- int getCdTipoTela() 

    /**
     * Returns the value of field 'contaPagtoDestino'.
     * 
     * @return String
     * @return the value of field 'contaPagtoDestino'.
     */
    public java.lang.String getContaPagtoDestino()
    {
        return this._contaPagtoDestino;
    } //-- java.lang.String getContaPagtoDestino() 

    /**
     * Returns the value of field 'dsBeneficio'.
     * 
     * @return String
     * @return the value of field 'dsBeneficio'.
     */
    public java.lang.String getDsBeneficio()
    {
        return this._dsBeneficio;
    } //-- java.lang.String getDsBeneficio() 

    /**
     * Returns the value of field 'dsEfetivacaoPagamento'.
     * 
     * @return String
     * @return the value of field 'dsEfetivacaoPagamento'.
     */
    public java.lang.String getDsEfetivacaoPagamento()
    {
        return this._dsEfetivacaoPagamento;
    } //-- java.lang.String getDsEfetivacaoPagamento() 

    /**
     * Returns the value of field 'dsIndicadorAutorizacao'.
     * 
     * @return String
     * @return the value of field 'dsIndicadorAutorizacao'.
     */
    public java.lang.String getDsIndicadorAutorizacao()
    {
        return this._dsIndicadorAutorizacao;
    } //-- java.lang.String getDsIndicadorAutorizacao() 

    /**
     * Returns the value of field 'dsIndicadorPagamento'.
     * 
     * @return String
     * @return the value of field 'dsIndicadorPagamento'.
     */
    public java.lang.String getDsIndicadorPagamento()
    {
        return this._dsIndicadorPagamento;
    } //-- java.lang.String getDsIndicadorPagamento() 

    /**
     * Returns the value of field 'dsTipoLayout'.
     * 
     * @return String
     * @return the value of field 'dsTipoLayout'.
     */
    public java.lang.String getDsTipoLayout()
    {
        return this._dsTipoLayout;
    } //-- java.lang.String getDsTipoLayout() 

    /**
     * Returns the value of field 'nrPagamento'.
     * 
     * @return String
     * @return the value of field 'nrPagamento'.
     */
    public java.lang.String getNrPagamento()
    {
        return this._nrPagamento;
    } //-- java.lang.String getNrPagamento() 

    /**
     * Returns the value of field 'vlPagamento'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlPagamento'.
     */
    public java.math.BigDecimal getVlPagamento()
    {
        return this._vlPagamento;
    } //-- java.math.BigDecimal getVlPagamento() 

    /**
     * Method hasCdAgencia
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgencia()
    {
        return this._has_cdAgencia;
    } //-- boolean hasCdAgencia() 

    /**
     * Method hasCdBanco
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdBanco()
    {
        return this._has_cdBanco;
    } //-- boolean hasCdBanco() 

    /**
     * Method hasCdCnpjCpfFavorecido
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCnpjCpfFavorecido()
    {
        return this._has_cdCnpjCpfFavorecido;
    } //-- boolean hasCdCnpjCpfFavorecido() 

    /**
     * Method hasCdConta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdConta()
    {
        return this._has_cdConta;
    } //-- boolean hasCdConta() 

    /**
     * Method hasCdControleCnpjCpfFavorecido
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdControleCnpjCpfFavorecido()
    {
        return this._has_cdControleCnpjCpfFavorecido;
    } //-- boolean hasCdControleCnpjCpfFavorecido() 

    /**
     * Method hasCdFavorecido
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFavorecido()
    {
        return this._has_cdFavorecido;
    } //-- boolean hasCdFavorecido() 

    /**
     * Method hasCdFilialCnpjCpfFavorecido
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFilialCnpjCpfFavorecido()
    {
        return this._has_cdFilialCnpjCpfFavorecido;
    } //-- boolean hasCdFilialCnpjCpfFavorecido() 

    /**
     * Method hasCdLoteInterno
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdLoteInterno()
    {
        return this._has_cdLoteInterno;
    } //-- boolean hasCdLoteInterno() 

    /**
     * Method hasCdTipoCanal
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoCanal()
    {
        return this._has_cdTipoCanal;
    } //-- boolean hasCdTipoCanal() 

    /**
     * Method hasCdTipoTela
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoTela()
    {
        return this._has_cdTipoTela;
    } //-- boolean hasCdTipoTela() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdAgencia'.
     * 
     * @param cdAgencia the value of field 'cdAgencia'.
     */
    public void setCdAgencia(int cdAgencia)
    {
        this._cdAgencia = cdAgencia;
        this._has_cdAgencia = true;
    } //-- void setCdAgencia(int) 

    /**
     * Sets the value of field 'cdBanco'.
     * 
     * @param cdBanco the value of field 'cdBanco'.
     */
    public void setCdBanco(int cdBanco)
    {
        this._cdBanco = cdBanco;
        this._has_cdBanco = true;
    } //-- void setCdBanco(int) 

    /**
     * Sets the value of field 'cdCnpjCpfFavorecido'.
     * 
     * @param cdCnpjCpfFavorecido the value of field
     * 'cdCnpjCpfFavorecido'.
     */
    public void setCdCnpjCpfFavorecido(long cdCnpjCpfFavorecido)
    {
        this._cdCnpjCpfFavorecido = cdCnpjCpfFavorecido;
        this._has_cdCnpjCpfFavorecido = true;
    } //-- void setCdCnpjCpfFavorecido(long) 

    /**
     * Sets the value of field 'cdConta'.
     * 
     * @param cdConta the value of field 'cdConta'.
     */
    public void setCdConta(long cdConta)
    {
        this._cdConta = cdConta;
        this._has_cdConta = true;
    } //-- void setCdConta(long) 

    /**
     * Sets the value of field 'cdControleCnpjCpfFavorecido'.
     * 
     * @param cdControleCnpjCpfFavorecido the value of field
     * 'cdControleCnpjCpfFavorecido'.
     */
    public void setCdControleCnpjCpfFavorecido(int cdControleCnpjCpfFavorecido)
    {
        this._cdControleCnpjCpfFavorecido = cdControleCnpjCpfFavorecido;
        this._has_cdControleCnpjCpfFavorecido = true;
    } //-- void setCdControleCnpjCpfFavorecido(int) 

    /**
     * Sets the value of field 'cdDigitoAgencia'.
     * 
     * @param cdDigitoAgencia the value of field 'cdDigitoAgencia'.
     */
    public void setCdDigitoAgencia(java.lang.String cdDigitoAgencia)
    {
        this._cdDigitoAgencia = cdDigitoAgencia;
    } //-- void setCdDigitoAgencia(java.lang.String) 

    /**
     * Sets the value of field 'cdDigitoConta'.
     * 
     * @param cdDigitoConta the value of field 'cdDigitoConta'.
     */
    public void setCdDigitoConta(java.lang.String cdDigitoConta)
    {
        this._cdDigitoConta = cdDigitoConta;
    } //-- void setCdDigitoConta(java.lang.String) 

    /**
     * Sets the value of field 'cdFavorecido'.
     * 
     * @param cdFavorecido the value of field 'cdFavorecido'.
     */
    public void setCdFavorecido(long cdFavorecido)
    {
        this._cdFavorecido = cdFavorecido;
        this._has_cdFavorecido = true;
    } //-- void setCdFavorecido(long) 

    /**
     * Sets the value of field 'cdFilialCnpjCpfFavorecido'.
     * 
     * @param cdFilialCnpjCpfFavorecido the value of field
     * 'cdFilialCnpjCpfFavorecido'.
     */
    public void setCdFilialCnpjCpfFavorecido(int cdFilialCnpjCpfFavorecido)
    {
        this._cdFilialCnpjCpfFavorecido = cdFilialCnpjCpfFavorecido;
        this._has_cdFilialCnpjCpfFavorecido = true;
    } //-- void setCdFilialCnpjCpfFavorecido(int) 

    /**
     * Sets the value of field 'cdIspbPagtoDestino'.
     * 
     * @param cdIspbPagtoDestino the value of field
     * 'cdIspbPagtoDestino'.
     */
    public void setCdIspbPagtoDestino(java.lang.String cdIspbPagtoDestino)
    {
        this._cdIspbPagtoDestino = cdIspbPagtoDestino;
    } //-- void setCdIspbPagtoDestino(java.lang.String) 

    /**
     * Sets the value of field 'cdLoteInterno'.
     * 
     * @param cdLoteInterno the value of field 'cdLoteInterno'.
     */
    public void setCdLoteInterno(long cdLoteInterno)
    {
        this._cdLoteInterno = cdLoteInterno;
        this._has_cdLoteInterno = true;
    } //-- void setCdLoteInterno(long) 

    /**
     * Sets the value of field 'cdTipoCanal'.
     * 
     * @param cdTipoCanal the value of field 'cdTipoCanal'.
     */
    public void setCdTipoCanal(int cdTipoCanal)
    {
        this._cdTipoCanal = cdTipoCanal;
        this._has_cdTipoCanal = true;
    } //-- void setCdTipoCanal(int) 

    /**
     * Sets the value of field 'cdTipoTela'.
     * 
     * @param cdTipoTela the value of field 'cdTipoTela'.
     */
    public void setCdTipoTela(int cdTipoTela)
    {
        this._cdTipoTela = cdTipoTela;
        this._has_cdTipoTela = true;
    } //-- void setCdTipoTela(int) 

    /**
     * Sets the value of field 'contaPagtoDestino'.
     * 
     * @param contaPagtoDestino the value of field
     * 'contaPagtoDestino'.
     */
    public void setContaPagtoDestino(java.lang.String contaPagtoDestino)
    {
        this._contaPagtoDestino = contaPagtoDestino;
    } //-- void setContaPagtoDestino(java.lang.String) 

    /**
     * Sets the value of field 'dsBeneficio'.
     * 
     * @param dsBeneficio the value of field 'dsBeneficio'.
     */
    public void setDsBeneficio(java.lang.String dsBeneficio)
    {
        this._dsBeneficio = dsBeneficio;
    } //-- void setDsBeneficio(java.lang.String) 

    /**
     * Sets the value of field 'dsEfetivacaoPagamento'.
     * 
     * @param dsEfetivacaoPagamento the value of field
     * 'dsEfetivacaoPagamento'.
     */
    public void setDsEfetivacaoPagamento(java.lang.String dsEfetivacaoPagamento)
    {
        this._dsEfetivacaoPagamento = dsEfetivacaoPagamento;
    } //-- void setDsEfetivacaoPagamento(java.lang.String) 

    /**
     * Sets the value of field 'dsIndicadorAutorizacao'.
     * 
     * @param dsIndicadorAutorizacao the value of field
     * 'dsIndicadorAutorizacao'.
     */
    public void setDsIndicadorAutorizacao(java.lang.String dsIndicadorAutorizacao)
    {
        this._dsIndicadorAutorizacao = dsIndicadorAutorizacao;
    } //-- void setDsIndicadorAutorizacao(java.lang.String) 

    /**
     * Sets the value of field 'dsIndicadorPagamento'.
     * 
     * @param dsIndicadorPagamento the value of field
     * 'dsIndicadorPagamento'.
     */
    public void setDsIndicadorPagamento(java.lang.String dsIndicadorPagamento)
    {
        this._dsIndicadorPagamento = dsIndicadorPagamento;
    } //-- void setDsIndicadorPagamento(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoLayout'.
     * 
     * @param dsTipoLayout the value of field 'dsTipoLayout'.
     */
    public void setDsTipoLayout(java.lang.String dsTipoLayout)
    {
        this._dsTipoLayout = dsTipoLayout;
    } //-- void setDsTipoLayout(java.lang.String) 

    /**
     * Sets the value of field 'nrPagamento'.
     * 
     * @param nrPagamento the value of field 'nrPagamento'.
     */
    public void setNrPagamento(java.lang.String nrPagamento)
    {
        this._nrPagamento = nrPagamento;
    } //-- void setNrPagamento(java.lang.String) 

    /**
     * Sets the value of field 'vlPagamento'.
     * 
     * @param vlPagamento the value of field 'vlPagamento'.
     */
    public void setVlPagamento(java.math.BigDecimal vlPagamento)
    {
        this._vlPagamento = vlPagamento;
    } //-- void setVlPagamento(java.math.BigDecimal) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.detalharpagtosconsautorizados.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.detalharpagtosconsautorizados.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.detalharpagtosconsautorizados.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharpagtosconsautorizados.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
