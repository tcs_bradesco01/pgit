package br.com.bradesco.web.pgit.service.business.combo.bean;


// TODO: Auto-generated Javadoc
/**
 * Arquivo criado em 22/02/16.
 */
public class ListarLayoutsContratoOcorrenciasSaidaDTO {

    /** The cd tipo layout arquivo. */
    private Integer cdTipoLayoutArquivo;

    /** The ds tipo layout arquivo. */
    private String dsTipoLayoutArquivo;

    /**
     * Set cd tipo layout arquivo.
     *
     * @param cdTipoLayoutArquivo the cd tipo layout arquivo
     */
    public void setCdTipoLayoutArquivo(Integer cdTipoLayoutArquivo) {
        this.cdTipoLayoutArquivo = cdTipoLayoutArquivo;
    }

    /**
     * Get cd tipo layout arquivo.
     *
     * @return the cd tipo layout arquivo
     */
    public Integer getCdTipoLayoutArquivo() {
        return this.cdTipoLayoutArquivo;
    }

    /**
     * Set ds tipo layout arquivo.
     *
     * @param dsTipoLayoutArquivo the ds tipo layout arquivo
     */
    public void setDsTipoLayoutArquivo(String dsTipoLayoutArquivo) {
        this.dsTipoLayoutArquivo = dsTipoLayoutArquivo;
    }

    /**
     * Get ds tipo layout arquivo.
     *
     * @return the ds tipo layout arquivo
     */
    public String getDsTipoLayoutArquivo() {
        return this.dsTipoLayoutArquivo;
    }
}