/*
 * Nome: br.com.bradesco.web.pgit.service.business.concontratosmigrados.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.concontratosmigrados.bean;

/**
 * Nome: ConContratoMigradoEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConContratoMigradoEntradaDTO {
	
	/** Atributo nrOcorrencias. */
	private Integer nrOcorrencias;
    
    /** Atributo centroCustoOrigem. */
    private Integer centroCustoOrigem;
    
    /** Atributo dtInicio. */
    private String dtInicio;
    
    /** Atributo dtFim. */
    private String dtFim;
    
    /** Atributo indicadorPesquisa. */
    private Integer indicadorPesquisa;
    
    /** Atributo cdPessoaJuridicaContrato. */
    private Long cdPessoaJuridicaContrato;
    
    /** Atributo cdTipoContratoNegocio. */
    private Integer cdTipoContratoNegocio;
    
    /** Atributo nrSequenciaContratoNegocio. */
    private Long nrSequenciaContratoNegocio;
    
    /** Atributo cdCorpoCpfCnpj. */
    private Long cdCorpoCpfCnpj;
    
    /** Atributo filiaCnpj. */
    private Integer filiaCnpj;
    
    /** Atributo digitoCnpj. */
    private Integer digitoCnpj;
    
    /** Atributo cdPerfil. */
    private Long cdPerfil;
    
    /** Atributo cdAgencia. */
    private Integer cdAgencia;
    
    /** Atributo cdConta. */
    private Integer cdConta;
    
    /** Atributo cdSituacaoContrato. */
    private Integer cdSituacaoContrato;
    
    
	/**
	 * Get: cdAgencia.
	 *
	 * @return cdAgencia
	 */
	public Integer getCdAgencia() {
		return cdAgencia;
	}
	
	/**
	 * Set: cdAgencia.
	 *
	 * @param cdAgencia the cd agencia
	 */
	public void setCdAgencia(Integer cdAgencia) {
		this.cdAgencia = cdAgencia;
	}
	
	/**
	 * Get: cdConta.
	 *
	 * @return cdConta
	 */
	public Integer getCdConta() {
		return cdConta;
	}
	
	/**
	 * Get: cdSituacaoContrato.
	 *
	 * @return cdSituacaoContrato
	 */
	public Integer getCdSituacaoContrato() {
		return cdSituacaoContrato;
	}
	
	/**
	 * Set: cdSituacaoContrato.
	 *
	 * @param cdSituacaoContrato the cd situacao contrato
	 */
	public void setCdSituacaoContrato(Integer cdSituacaoContrato) {
		this.cdSituacaoContrato = cdSituacaoContrato;
	}
	
	/**
	 * Set: cdConta.
	 *
	 * @param cdConta the cd conta
	 */
	public void setCdConta(Integer cdConta) {
		this.cdConta = cdConta;
	}
	
	/**
	 * Get: cdCorpoCpfCnpj.
	 *
	 * @return cdCorpoCpfCnpj
	 */
	public Long getCdCorpoCpfCnpj() {
		return cdCorpoCpfCnpj;
	}
	
	/**
	 * Set: cdCorpoCpfCnpj.
	 *
	 * @param cdCorpoCpfCnpj the cd corpo cpf cnpj
	 */
	public void setCdCorpoCpfCnpj(Long cdCorpoCpfCnpj) {
		this.cdCorpoCpfCnpj = cdCorpoCpfCnpj;
	}
	
	/**
	 * Get: cdPerfil.
	 *
	 * @return cdPerfil
	 */
	public Long getCdPerfil() {
		return cdPerfil;
	}
	
	/**
	 * Set: cdPerfil.
	 *
	 * @param cdPerfil the cd perfil
	 */
	public void setCdPerfil(Long cdPerfil) {
		this.cdPerfil = cdPerfil;
	}
	
	/**
	 * Get: cdPessoaJuridicaContrato.
	 *
	 * @return cdPessoaJuridicaContrato
	 */
	public Long getCdPessoaJuridicaContrato() {
		return cdPessoaJuridicaContrato;
	}
	
	/**
	 * Set: cdPessoaJuridicaContrato.
	 *
	 * @param cdPessoaJuridicaContrato the cd pessoa juridica contrato
	 */
	public void setCdPessoaJuridicaContrato(Long cdPessoaJuridicaContrato) {
		this.cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
	}
	
	/**
	 * Get: cdTipoContratoNegocio.
	 *
	 * @return cdTipoContratoNegocio
	 */
	public Integer getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}
	
	/**
	 * Set: cdTipoContratoNegocio.
	 *
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}
	
	/**
	 * Get: digitoCnpj.
	 *
	 * @return digitoCnpj
	 */
	public Integer getDigitoCnpj() {
		return digitoCnpj;
	}
	
	/**
	 * Set: digitoCnpj.
	 *
	 * @param digitoCnpj the digito cnpj
	 */
	public void setDigitoCnpj(Integer digitoCnpj) {
		this.digitoCnpj = digitoCnpj;
	}
	
	/**
	 * Get: dtFim.
	 *
	 * @return dtFim
	 */
	public String getDtFim() {
		return dtFim;
	}
	
	/**
	 * Set: dtFim.
	 *
	 * @param dtFim the dt fim
	 */
	public void setDtFim(String dtFim) {
		this.dtFim = dtFim;
	}
	
	/**
	 * Get: dtInicio.
	 *
	 * @return dtInicio
	 */
	public String getDtInicio() {
		return dtInicio;
	}
	
	/**
	 * Set: dtInicio.
	 *
	 * @param dtInicio the dt inicio
	 */
	public void setDtInicio(String dtInicio) {
		this.dtInicio = dtInicio;
	}
	
	/**
	 * Get: filiaCnpj.
	 *
	 * @return filiaCnpj
	 */
	public Integer getFiliaCnpj() {
		return filiaCnpj;
	}
	
	/**
	 * Set: filiaCnpj.
	 *
	 * @param filiaCnpj the filia cnpj
	 */
	public void setFiliaCnpj(Integer filiaCnpj) {
		this.filiaCnpj = filiaCnpj;
	}
	
	/**
	 * Get: indicadorPesquisa.
	 *
	 * @return indicadorPesquisa
	 */
	public Integer getIndicadorPesquisa() {
		return indicadorPesquisa;
	}
	
	/**
	 * Set: indicadorPesquisa.
	 *
	 * @param indicadorPesquisa the indicador pesquisa
	 */
	public void setIndicadorPesquisa(Integer indicadorPesquisa) {
		this.indicadorPesquisa = indicadorPesquisa;
	}
	
	/**
	 * Get: nrOcorrencias.
	 *
	 * @return nrOcorrencias
	 */
	public Integer getNrOcorrencias() {
		return nrOcorrencias;
	}
	
	/**
	 * Set: nrOcorrencias.
	 *
	 * @param nrOcorrencias the nr ocorrencias
	 */
	public void setNrOcorrencias(Integer nrOcorrencias) {
		this.nrOcorrencias = nrOcorrencias;
	}
	
	/**
	 * Get: nrSequenciaContratoNegocio.
	 *
	 * @return nrSequenciaContratoNegocio
	 */
	public Long getNrSequenciaContratoNegocio() {
		return nrSequenciaContratoNegocio;
	}
	
	/**
	 * Set: nrSequenciaContratoNegocio.
	 *
	 * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
	 */
	public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}
	
	/**
	 * Get: centroCustoOrigem.
	 *
	 * @return centroCustoOrigem
	 */
	public Integer getCentroCustoOrigem() {
		return centroCustoOrigem;
	}
	
	/**
	 * Set: centroCustoOrigem.
	 *
	 * @param centroCustoOrigem the centro custo origem
	 */
	public void setCentroCustoOrigem(Integer centroCustoOrigem) {
		this.centroCustoOrigem = centroCustoOrigem;
	}

}
