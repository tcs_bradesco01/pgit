/*
 * =========================================================================
 * 
 * Client:       Bradesco (BR)
 * Project:      Arquitetura Bradesco Canal Internet
 * Development:  GFT Iberia (http://www.gft.com)
 * -------------------------------------------------------------------------
 * Revision - Last:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/constants.ftl,v $
 * $Id: constants.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revision - History:
 * $Log: constants.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.solemissaomovclientepag;


/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Interface de constantes do adaptador: SolEmissaoMovClientePag
 * </p>
 * 
 * @comment C�DIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public interface ISolEmissaoMovClientePagServiceConstants {
	
	/** Atributo NUMERO_OCORRENCIAS_CONSULTAR. */
	int NUMERO_OCORRENCIAS_CONSULTAR = 100;
	
	/** Atributo NUMERO_OCORRENCIAS_DETALHAR. */
	int NUMERO_OCORRENCIAS_DETALHAR = 70;
	
	/** Atributo NUMERO_REGISTRO_EXCLUIR. */
	int NUMERO_REGISTRO_EXCLUIR = 0;
	
	/** Atributo NUMERO_TIPO_CONTRATO_EXCLUIR. */
	int NUMERO_TIPO_CONTRATO_EXCLUIR = 0;
	
	/** Atributo NUMERO_REGISTRO_INCLUIR. */
	int NUMERO_REGISTRO_INCLUIR = 0;
}

