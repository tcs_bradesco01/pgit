/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantercontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantercontrato.bean;

import br.com.bradesco.web.pgit.utils.CpfCnpjUtils;

/**
 * Nome: ListarParticipantesSaidaDTO
 * <p>
 * Prop�sito:
 * </p>
 * .
 * 
 * @author : todo!
 * @version :
 */
public class ListarParticipantesSaidaDTO {

    /** Atributo codMensagem. */
    private String codMensagem;

    /** Atributo mensagem. */
    private String mensagem;

    /** Atributo cdPessoa. */
    private long cdPessoa;

    /** Atributo cdCpfCnpj. */
    private Long cdCpfCnpj;

    /** Atributo cdFilialCnpj. */
    private Integer cdFilialCnpj;

    /** Atributo cdControleCnpj. */
    private Integer cdControleCnpj;

    /** Atributo nomeRazao. */
    private String nomeRazao;

    /** Atributo cdGrupoEconomico. */
    private long cdGrupoEconomico;

    /** Atributo dsGrupoEconomico. */
    private String dsGrupoEconomico;

    /** Atributo cdAtividadeEconomica. */
    private int cdAtividadeEconomica;

    /** Atributo dsAtividadeEconomica. */
    private String dsAtividadeEconomica;

    /** Atributo cdSegmentoEconomico. */
    private int cdSegmentoEconomico;

    /** Atributo dsSegmentoEconomico. */
    private String dsSegmentoEconomico;

    /** Atributo cdSubSegmento. */
    private int cdSubSegmento;

    /** Atributo dsSubSegmento. */
    private String dsSubSegmento;

    /** Atributo cdTipoParticipacao. */
    private int cdTipoParticipacao;

    /** Atributo dsTipoParticipacao. */
    private String dsTipoParticipacao;

    /** Atributo cdSituacaoParticipacao. */
    private int cdSituacaoParticipacao;

    /** Atributo dsSituacaoParticipacao. */
    private String dsSituacaoParticipacao;

    /** Atributo cdUsuarioInclusao. */
    private String cdUsuarioInclusao;

    /** Atributo cdUsuarioExternoInclusao. */
    private String cdUsuarioExternoInclusao;

    /** Atributo horaManutencaoRegInclusao. */
    private String horaManutencaoRegInclusao;

    /** Atributo cdCanalInclusao. */
    private int cdCanalInclusao;

    /** Atributo dsCanalInclusao. */
    private String dsCanalInclusao;

    /** Atributo nomeOperacaoFluxoInclusao. */
    private String nomeOperacaoFluxoInclusao;

    /** Atributo cdUsuarioAlteracao. */
    private String cdUsuarioAlteracao;

    /** Atributo cdUsuarioExternoAlteracao. */
    private String cdUsuarioExternoAlteracao;

    /** Atributo horaManutencaoResAlteracao. */
    private String horaManutencaoResAlteracao;

    /** Atributo cdCanalAlteracao. */
    private int cdCanalAlteracao;

    /** Atributo dsCanalAlteracao. */
    private String dsCanalAlteracao;

    /** Atributo nomeOperacaoFluxoAlteracao. */
    private String nomeOperacaoFluxoAlteracao;

    /** Atributo cnpjOuCpfFormatado. */
    private String cnpjOuCpfFormatado;

    /** Atributo tipoParticipacaoFormatador. */
    private String tipoParticipacaoFormatador;

    /** Atributo hrManutencaoRegistroInclusao. */
    private String hrManutencaoRegistroInclusao;

    /** Atributo nmOperacaoFluxoInclusao. */
    private String nmOperacaoFluxoInclusao;

    /** Atributo hrManutencaoRegistroAlteracao. */
    private String hrManutencaoRegistroAlteracao;

    /** Atributo nmOperacaoFluxoAlteracao. */
    private String nmOperacaoFluxoAlteracao;

    /** Atributo cdMotivoParticipacao. */
    private Integer cdMotivoParticipacao;

    /** Atributo dsMotivoParticipacao. */
    private String dsMotivoParticipacao;

    /** Atributo cdClasificacaoParticipante. */
    private Integer cdClasificacaoParticipante;

    /** Atributo dsClasificacaoParticipante. */
    private String dsClasificacaoParticipante;

    /** Atributo nmRazao. */
    private String nmRazao;

    /**
     * Get: cdClasificacaoParticipante.
     * 
     * @return cdClasificacaoParticipante
     */
    public Integer getCdClasificacaoParticipante() {
        return cdClasificacaoParticipante;
    }

    /**
     * Set: cdClasificacaoParticipante.
     * 
     * @param cdClasificacaoParticipante
     *            the cd clasificacao participante
     */
    public void setCdClasificacaoParticipante(Integer cdClasificacaoParticipante) {
        this.cdClasificacaoParticipante = cdClasificacaoParticipante;
    }

    /**
     * Get: cdMotivoParticipacao.
     * 
     * @return cdMotivoParticipacao
     */
    public Integer getCdMotivoParticipacao() {
        return cdMotivoParticipacao;
    }

    /**
     * Set: cdMotivoParticipacao.
     * 
     * @param cdMotivoParticipacao
     *            the cd motivo participacao
     */
    public void setCdMotivoParticipacao(Integer cdMotivoParticipacao) {
        this.cdMotivoParticipacao = cdMotivoParticipacao;
    }

    /**
     * Get: dsClasificacaoParticipante.
     * 
     * @return dsClasificacaoParticipante
     */
    public String getDsClasificacaoParticipante() {
        return dsClasificacaoParticipante;
    }

    /**
     * Set: dsClasificacaoParticipante.
     * 
     * @param dsClasificacaoParticipante
     *            the ds clasificacao participante
     */
    public void setDsClasificacaoParticipante(String dsClasificacaoParticipante) {
        this.dsClasificacaoParticipante = dsClasificacaoParticipante;
    }

    /**
     * Get: dsMotivoParticipacao.
     * 
     * @return dsMotivoParticipacao
     */
    public String getDsMotivoParticipacao() {
        return dsMotivoParticipacao;
    }

    /**
     * Set: dsMotivoParticipacao.
     * 
     * @param dsMotivoParticipacao
     *            the ds motivo participacao
     */
    public void setDsMotivoParticipacao(String dsMotivoParticipacao) {
        this.dsMotivoParticipacao = dsMotivoParticipacao;
    }

    /**
     * Get: hrManutencaoRegistroAlteracao.
     * 
     * @return hrManutencaoRegistroAlteracao
     */
    public String getHrManutencaoRegistroAlteracao() {
        return hrManutencaoRegistroAlteracao;
    }

    /**
     * Set: hrManutencaoRegistroAlteracao.
     * 
     * @param hrManutencaoRegistroAlteracao
     *            the hr manutencao registro alteracao
     */
    public void setHrManutencaoRegistroAlteracao(String hrManutencaoRegistroAlteracao) {
        this.hrManutencaoRegistroAlteracao = hrManutencaoRegistroAlteracao;
    }

    /**
     * Get: hrManutencaoRegistroInclusao.
     * 
     * @return hrManutencaoRegistroInclusao
     */
    public String getHrManutencaoRegistroInclusao() {
        return hrManutencaoRegistroInclusao;
    }

    /**
     * Set: hrManutencaoRegistroInclusao.
     * 
     * @param hrManutencaoRegistroInclusao
     *            the hr manutencao registro inclusao
     */
    public void setHrManutencaoRegistroInclusao(String hrManutencaoRegistroInclusao) {
        this.hrManutencaoRegistroInclusao = hrManutencaoRegistroInclusao;
    }

    /**
     * Get: nmOperacaoFluxoAlteracao.
     * 
     * @return nmOperacaoFluxoAlteracao
     */
    public String getNmOperacaoFluxoAlteracao() {
        return nmOperacaoFluxoAlteracao;
    }

    /**
     * Set: nmOperacaoFluxoAlteracao.
     * 
     * @param nmOperacaoFluxoAlteracao
     *            the nm operacao fluxo alteracao
     */
    public void setNmOperacaoFluxoAlteracao(String nmOperacaoFluxoAlteracao) {
        this.nmOperacaoFluxoAlteracao = nmOperacaoFluxoAlteracao;
    }

    /**
     * Get: nmOperacaoFluxoInclusao.
     * 
     * @return nmOperacaoFluxoInclusao
     */
    public String getNmOperacaoFluxoInclusao() {
        return nmOperacaoFluxoInclusao;
    }

    /**
     * Set: nmOperacaoFluxoInclusao.
     * 
     * @param nmOperacaoFluxoInclusao
     *            the nm operacao fluxo inclusao
     */
    public void setNmOperacaoFluxoInclusao(String nmOperacaoFluxoInclusao) {
        this.nmOperacaoFluxoInclusao = nmOperacaoFluxoInclusao;
    }

    /**
     * Get: nmRazao.
     * 
     * @return nmRazao
     */
    public String getNmRazao() {
        return nmRazao;
    }

    /**
     * Set: nmRazao.
     * 
     * @param nmRazao
     *            the nm razao
     */
    public void setNmRazao(String nmRazao) {
        this.nmRazao = nmRazao;
    }

    /**
     * Get: tipoParticipacaoFormatador.
     * 
     * @return tipoParticipacaoFormatador
     */
    public String getTipoParticipacaoFormatador() {
        return tipoParticipacaoFormatador;
    }

    /**
     * Set: tipoParticipacaoFormatador.
     * 
     * @param tipoParticipacaoFormatador
     *            the tipo participacao formatador
     */
    public void setTipoParticipacaoFormatador(String tipoParticipacaoFormatador) {
        this.tipoParticipacaoFormatador = tipoParticipacaoFormatador;
    }

    /**
     * Get: cnpjOuCpfFormatado.
     * 
     * @return cnpjOuCpfFormatado
     */
    public String getCnpjOuCpfFormatado() {
        return CpfCnpjUtils.formatarCpfCnpj(cdCpfCnpj, cdFilialCnpj, cdControleCnpj);
    }

    /**
     * Set: cnpjOuCpfFormatado.
     * 
     * @param cnpjOuCpfFormatado
     *            the cnpj ou cpf formatado
     */
    public void setCnpjOuCpfFormatado(String cnpjOuCpfFormatado) {
        this.cnpjOuCpfFormatado = cnpjOuCpfFormatado;
    }

    /**
     * Get: codMensagem.
     * 
     * @return codMensagem
     */
    public String getCodMensagem() {
        return codMensagem;
    }

    /**
     * Set: codMensagem.
     * 
     * @param codMensagem
     *            the cod mensagem
     */
    public void setCodMensagem(String codMensagem) {
        this.codMensagem = codMensagem;
    }

    /**
     * Get: mensagem.
     * 
     * @return mensagem
     */
    public String getMensagem() {
        return mensagem;
    }

    /**
     * Set: mensagem.
     * 
     * @param mensagem
     *            the mensagem
     */
    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }

    /**
     * Get: cdAtividadeEconomica.
     * 
     * @return cdAtividadeEconomica
     */
    public int getCdAtividadeEconomica() {
        return cdAtividadeEconomica;
    }

    /**
     * Set: cdAtividadeEconomica.
     * 
     * @param cdAtividadeEconomica
     *            the cd atividade economica
     */
    public void setCdAtividadeEconomica(int cdAtividadeEconomica) {
        this.cdAtividadeEconomica = cdAtividadeEconomica;
    }

    /**
     * Get: cdCanalAlteracao.
     * 
     * @return cdCanalAlteracao
     */
    public int getCdCanalAlteracao() {
        return cdCanalAlteracao;
    }

    /**
     * Set: cdCanalAlteracao.
     * 
     * @param cdCanalAlteracao
     *            the cd canal alteracao
     */
    public void setCdCanalAlteracao(int cdCanalAlteracao) {
        this.cdCanalAlteracao = cdCanalAlteracao;
    }

    /**
     * Get: cdCanalInclusao.
     * 
     * @return cdCanalInclusao
     */
    public int getCdCanalInclusao() {
        return cdCanalInclusao;
    }

    /**
     * Set: cdCanalInclusao.
     * 
     * @param cdCanalInclusao
     *            the cd canal inclusao
     */
    public void setCdCanalInclusao(int cdCanalInclusao) {
        this.cdCanalInclusao = cdCanalInclusao;
    }

    /**
     * Get: cdControleCnpj.
     * 
     * @return cdControleCnpj
     */
    public Integer getCdControleCnpj() {
        return cdControleCnpj;
    }

    /**
     * Set: cdControleCnpj.
     * 
     * @param cdControleCnpj
     *            the cd controle cnpj
     */
    public void setCdControleCnpj(Integer cdControleCnpj) {
        this.cdControleCnpj = cdControleCnpj;
    }

    /**
     * Get: cdCpfCnpj.
     * 
     * @return cdCpfCnpj
     */
    public Long getCdCpfCnpj() {
        return cdCpfCnpj;
    }

    /**
     * Set: cdCpfCnpj.
     * 
     * @param cdCpfCnpj
     *            the cd cpf cnpj
     */
    public void setCdCpfCnpj(Long cdCpfCnpj) {
        this.cdCpfCnpj = cdCpfCnpj;
    }

    /**
     * Get: cdFilialCnpj.
     * 
     * @return cdFilialCnpj
     */
    public Integer getCdFilialCnpj() {
        return cdFilialCnpj;
    }

    /**
     * Set: cdFilialCnpj.
     * 
     * @param cdFilialCnpj
     *            the cd filial cnpj
     */
    public void setCdFilialCnpj(Integer cdFilialCnpj) {
        this.cdFilialCnpj = cdFilialCnpj;
    }

    /**
     * Get: cdGrupoEconomico.
     * 
     * @return cdGrupoEconomico
     */
    public long getCdGrupoEconomico() {
        return cdGrupoEconomico;
    }

    /**
     * Set: cdGrupoEconomico.
     * 
     * @param cdGrupoEconomico
     *            the cd grupo economico
     */
    public void setCdGrupoEconomico(long cdGrupoEconomico) {
        this.cdGrupoEconomico = cdGrupoEconomico;
    }

    /**
     * Get: cdPessoa.
     * 
     * @return cdPessoa
     */
    public long getCdPessoa() {
        return cdPessoa;
    }

    /**
     * Set: cdPessoa.
     * 
     * @param cdPessoa
     *            the cd pessoa
     */
    public void setCdPessoa(long cdPessoa) {
        this.cdPessoa = cdPessoa;
    }

    /**
     * Get: cdSegmentoEconomico.
     * 
     * @return cdSegmentoEconomico
     */
    public int getCdSegmentoEconomico() {
        return cdSegmentoEconomico;
    }

    /**
     * Set: cdSegmentoEconomico.
     * 
     * @param cdSegmentoEconomico
     *            the cd segmento economico
     */
    public void setCdSegmentoEconomico(int cdSegmentoEconomico) {
        this.cdSegmentoEconomico = cdSegmentoEconomico;
    }

    /**
     * Get: cdSituacaoParticipacao.
     * 
     * @return cdSituacaoParticipacao
     */
    public int getCdSituacaoParticipacao() {
        return cdSituacaoParticipacao;
    }

    /**
     * Set: cdSituacaoParticipacao.
     * 
     * @param cdSituacaoParticipacao
     *            the cd situacao participacao
     */
    public void setCdSituacaoParticipacao(int cdSituacaoParticipacao) {
        this.cdSituacaoParticipacao = cdSituacaoParticipacao;
    }

    /**
     * Get: cdSubSegmento.
     * 
     * @return cdSubSegmento
     */
    public int getCdSubSegmento() {
        return cdSubSegmento;
    }

    /**
     * Set: cdSubSegmento.
     * 
     * @param cdSubSegmento
     *            the cd sub segmento
     */
    public void setCdSubSegmento(int cdSubSegmento) {
        this.cdSubSegmento = cdSubSegmento;
    }

    /**
     * Get: cdTipoParticipacao.
     * 
     * @return cdTipoParticipacao
     */
    public int getCdTipoParticipacao() {
        return cdTipoParticipacao;
    }

    /**
     * Set: cdTipoParticipacao.
     * 
     * @param cdTipoParticipacao
     *            the cd tipo participacao
     */
    public void setCdTipoParticipacao(int cdTipoParticipacao) {
        this.cdTipoParticipacao = cdTipoParticipacao;
    }

    /**
     * Get: cdUsuarioAlteracao.
     * 
     * @return cdUsuarioAlteracao
     */
    public String getCdUsuarioAlteracao() {
        return cdUsuarioAlteracao;
    }

    /**
     * Set: cdUsuarioAlteracao.
     * 
     * @param cdUsuarioAlteracao
     *            the cd usuario alteracao
     */
    public void setCdUsuarioAlteracao(String cdUsuarioAlteracao) {
        this.cdUsuarioAlteracao = cdUsuarioAlteracao;
    }

    /**
     * Get: cdUsuarioExternoAlteracao.
     * 
     * @return cdUsuarioExternoAlteracao
     */
    public String getCdUsuarioExternoAlteracao() {
        return cdUsuarioExternoAlteracao;
    }

    /**
     * Set: cdUsuarioExternoAlteracao.
     * 
     * @param cdUsuarioExternoAlteracao
     *            the cd usuario externo alteracao
     */
    public void setCdUsuarioExternoAlteracao(String cdUsuarioExternoAlteracao) {
        this.cdUsuarioExternoAlteracao = cdUsuarioExternoAlteracao;
    }

    /**
     * Get: cdUsuarioExternoInclusao.
     * 
     * @return cdUsuarioExternoInclusao
     */
    public String getCdUsuarioExternoInclusao() {
        return cdUsuarioExternoInclusao;
    }

    /**
     * Set: cdUsuarioExternoInclusao.
     * 
     * @param cdUsuarioExternoInclusao
     *            the cd usuario externo inclusao
     */
    public void setCdUsuarioExternoInclusao(String cdUsuarioExternoInclusao) {
        this.cdUsuarioExternoInclusao = cdUsuarioExternoInclusao;
    }

    /**
     * Get: cdUsuarioInclusao.
     * 
     * @return cdUsuarioInclusao
     */
    public String getCdUsuarioInclusao() {
        return cdUsuarioInclusao;
    }

    /**
     * Set: cdUsuarioInclusao.
     * 
     * @param cdUsuarioInclusao
     *            the cd usuario inclusao
     */
    public void setCdUsuarioInclusao(String cdUsuarioInclusao) {
        this.cdUsuarioInclusao = cdUsuarioInclusao;
    }

    /**
     * Get: dsAtividadeEconomica.
     * 
     * @return dsAtividadeEconomica
     */
    public String getDsAtividadeEconomica() {
        return dsAtividadeEconomica;
    }

    /**
     * Set: dsAtividadeEconomica.
     * 
     * @param dsAtividadeEconomica
     *            the ds atividade economica
     */
    public void setDsAtividadeEconomica(String dsAtividadeEconomica) {
        this.dsAtividadeEconomica = dsAtividadeEconomica;
    }

    /**
     * Get: dsCanalAlteracao.
     * 
     * @return dsCanalAlteracao
     */
    public String getDsCanalAlteracao() {
        return dsCanalAlteracao;
    }

    /**
     * Set: dsCanalAlteracao.
     * 
     * @param dsCanalAlteracao
     *            the ds canal alteracao
     */
    public void setDsCanalAlteracao(String dsCanalAlteracao) {
        this.dsCanalAlteracao = dsCanalAlteracao;
    }

    /**
     * Get: dsCanalInclusao.
     * 
     * @return dsCanalInclusao
     */
    public String getDsCanalInclusao() {
        return dsCanalInclusao;
    }

    /**
     * Set: dsCanalInclusao.
     * 
     * @param dsCanalInclusao
     *            the ds canal inclusao
     */
    public void setDsCanalInclusao(String dsCanalInclusao) {
        this.dsCanalInclusao = dsCanalInclusao;
    }

    /**
     * Get: dsGrupoEconomico.
     * 
     * @return dsGrupoEconomico
     */
    public String getDsGrupoEconomico() {
        return dsGrupoEconomico;
    }

    /**
     * Set: dsGrupoEconomico.
     * 
     * @param dsGrupoEconomico
     *            the ds grupo economico
     */
    public void setDsGrupoEconomico(String dsGrupoEconomico) {
        this.dsGrupoEconomico = dsGrupoEconomico;
    }

    /**
     * Get: dsSegmentoEconomico.
     * 
     * @return dsSegmentoEconomico
     */
    public String getDsSegmentoEconomico() {
        return dsSegmentoEconomico;
    }

    /**
     * Set: dsSegmentoEconomico.
     * 
     * @param dsSegmentoEconomico
     *            the ds segmento economico
     */
    public void setDsSegmentoEconomico(String dsSegmentoEconomico) {
        this.dsSegmentoEconomico = dsSegmentoEconomico;
    }

    /**
     * Get: dsSituacaoParticipacao.
     * 
     * @return dsSituacaoParticipacao
     */
    public String getDsSituacaoParticipacao() {
        return dsSituacaoParticipacao;
    }

    /**
     * Set: dsSituacaoParticipacao.
     * 
     * @param dsSituacaoParticipacao
     *            the ds situacao participacao
     */
    public void setDsSituacaoParticipacao(String dsSituacaoParticipacao) {
        this.dsSituacaoParticipacao = dsSituacaoParticipacao;
    }

    /**
     * Get: dsSubSegmento.
     * 
     * @return dsSubSegmento
     */
    public String getDsSubSegmento() {
        return dsSubSegmento;
    }

    /**
     * Set: dsSubSegmento.
     * 
     * @param dsSubSegmento
     *            the ds sub segmento
     */
    public void setDsSubSegmento(String dsSubSegmento) {
        this.dsSubSegmento = dsSubSegmento;
    }

    /**
     * Get: dsTipoParticipacao.
     * 
     * @return dsTipoParticipacao
     */
    public String getDsTipoParticipacao() {
        return dsTipoParticipacao;
    }

    /**
     * Set: dsTipoParticipacao.
     * 
     * @param dsTipoParticipacao
     *            the ds tipo participacao
     */
    public void setDsTipoParticipacao(String dsTipoParticipacao) {
        this.dsTipoParticipacao = dsTipoParticipacao;
    }

    /**
     * Get: horaManutencaoRegInclusao.
     * 
     * @return horaManutencaoRegInclusao
     */
    public String getHoraManutencaoRegInclusao() {
        return horaManutencaoRegInclusao;
    }

    /**
     * Set: horaManutencaoRegInclusao.
     * 
     * @param horaManutencaoRegInclusao
     *            the hora manutencao reg inclusao
     */
    public void setHoraManutencaoRegInclusao(String horaManutencaoRegInclusao) {
        this.horaManutencaoRegInclusao = horaManutencaoRegInclusao;
    }

    /**
     * Get: horaManutencaoResAlteracao.
     * 
     * @return horaManutencaoResAlteracao
     */
    public String getHoraManutencaoResAlteracao() {
        return horaManutencaoResAlteracao;
    }

    /**
     * Set: horaManutencaoResAlteracao.
     * 
     * @param horaManutencaoResAlteracao
     *            the hora manutencao res alteracao
     */
    public void setHoraManutencaoResAlteracao(String horaManutencaoResAlteracao) {
        this.horaManutencaoResAlteracao = horaManutencaoResAlteracao;
    }

    /**
     * Get: nomeOperacaoFluxoAlteracao.
     * 
     * @return nomeOperacaoFluxoAlteracao
     */
    public String getNomeOperacaoFluxoAlteracao() {
        return nomeOperacaoFluxoAlteracao;
    }

    /**
     * Set: nomeOperacaoFluxoAlteracao.
     * 
     * @param nomeOperacaoFluxoAlteracao
     *            the nome operacao fluxo alteracao
     */
    public void setNomeOperacaoFluxoAlteracao(String nomeOperacaoFluxoAlteracao) {
        this.nomeOperacaoFluxoAlteracao = nomeOperacaoFluxoAlteracao;
    }

    /**
     * Get: nomeOperacaoFluxoInclusao.
     * 
     * @return nomeOperacaoFluxoInclusao
     */
    public String getNomeOperacaoFluxoInclusao() {
        return nomeOperacaoFluxoInclusao;
    }

    /**
     * Set: nomeOperacaoFluxoInclusao.
     * 
     * @param nomeOperacaoFluxoInclusao
     *            the nome operacao fluxo inclusao
     */
    public void setNomeOperacaoFluxoInclusao(String nomeOperacaoFluxoInclusao) {
        this.nomeOperacaoFluxoInclusao = nomeOperacaoFluxoInclusao;
    }

    /**
     * Get: nomeRazao.
     * 
     * @return nomeRazao
     */
    public String getNomeRazao() {
        return nomeRazao;
    }

    /**
     * Set: nomeRazao.
     * 
     * @param nomeRazao
     *            the nome razao
     */
    public void setNomeRazao(String nomeRazao) {
        this.nomeRazao = nomeRazao;
    }

    /**
     * Get: cdDsTipoParticipacao.
     * 
     * @return cdDsTipoParticipacao
     */
    public String getCdDsTipoParticipacao() {
        if (getCdTipoParticipacao() == 0 && getDsTipoParticipacao() == null) {
            return "";
        }

        StringBuilder codDesc = new StringBuilder();
        codDesc.append(getCdTipoParticipacao());
        codDesc.append(" - ");
        if (getDsTipoParticipacao() != null) {
            codDesc.append(getDsTipoParticipacao());
        }
        return codDesc.toString();
    }
}
