/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ListarConvenioContaSalarioRequest.
 * 
 * @version $Revision$ $Date$
 */
public class ListarConvenioContaSalarioRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdPessoaJuridicaProposta
     */
    private long _cdPessoaJuridicaProposta = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridicaProposta
     */
    private boolean _has_cdPessoaJuridicaProposta;

    /**
     * Field _cdTipoContratoProposta
     */
    private int _cdTipoContratoProposta = 0;

    /**
     * keeps track of state for field: _cdTipoContratoProposta
     */
    private boolean _has_cdTipoContratoProposta;

    /**
     * Field _nrSequenciaContratoNegocio
     */
    private long _nrSequenciaContratoNegocio = 0;

    /**
     * keeps track of state for field: _nrSequenciaContratoNegocio
     */
    private boolean _has_nrSequenciaContratoNegocio;


      //----------------/
     //- Constructors -/
    //----------------/

    public ListarConvenioContaSalarioRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.request.ListarConvenioContaSalarioRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdPessoaJuridicaProposta
     * 
     */
    public void deleteCdPessoaJuridicaProposta()
    {
        this._has_cdPessoaJuridicaProposta= false;
    } //-- void deleteCdPessoaJuridicaProposta() 

    /**
     * Method deleteCdTipoContratoProposta
     * 
     */
    public void deleteCdTipoContratoProposta()
    {
        this._has_cdTipoContratoProposta= false;
    } //-- void deleteCdTipoContratoProposta() 

    /**
     * Method deleteNrSequenciaContratoNegocio
     * 
     */
    public void deleteNrSequenciaContratoNegocio()
    {
        this._has_nrSequenciaContratoNegocio= false;
    } //-- void deleteNrSequenciaContratoNegocio() 

    /**
     * Returns the value of field 'cdPessoaJuridicaProposta'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridicaProposta'.
     */
    public long getCdPessoaJuridicaProposta()
    {
        return this._cdPessoaJuridicaProposta;
    } //-- long getCdPessoaJuridicaProposta() 

    /**
     * Returns the value of field 'cdTipoContratoProposta'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoProposta'.
     */
    public int getCdTipoContratoProposta()
    {
        return this._cdTipoContratoProposta;
    } //-- int getCdTipoContratoProposta() 

    /**
     * Returns the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @return long
     * @return the value of field 'nrSequenciaContratoNegocio'.
     */
    public long getNrSequenciaContratoNegocio()
    {
        return this._nrSequenciaContratoNegocio;
    } //-- long getNrSequenciaContratoNegocio() 

    /**
     * Method hasCdPessoaJuridicaProposta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridicaProposta()
    {
        return this._has_cdPessoaJuridicaProposta;
    } //-- boolean hasCdPessoaJuridicaProposta() 

    /**
     * Method hasCdTipoContratoProposta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoProposta()
    {
        return this._has_cdTipoContratoProposta;
    } //-- boolean hasCdTipoContratoProposta() 

    /**
     * Method hasNrSequenciaContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaContratoNegocio()
    {
        return this._has_nrSequenciaContratoNegocio;
    } //-- boolean hasNrSequenciaContratoNegocio() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdPessoaJuridicaProposta'.
     * 
     * @param cdPessoaJuridicaProposta the value of field
     * 'cdPessoaJuridicaProposta'.
     */
    public void setCdPessoaJuridicaProposta(long cdPessoaJuridicaProposta)
    {
        this._cdPessoaJuridicaProposta = cdPessoaJuridicaProposta;
        this._has_cdPessoaJuridicaProposta = true;
    } //-- void setCdPessoaJuridicaProposta(long) 

    /**
     * Sets the value of field 'cdTipoContratoProposta'.
     * 
     * @param cdTipoContratoProposta the value of field
     * 'cdTipoContratoProposta'.
     */
    public void setCdTipoContratoProposta(int cdTipoContratoProposta)
    {
        this._cdTipoContratoProposta = cdTipoContratoProposta;
        this._has_cdTipoContratoProposta = true;
    } //-- void setCdTipoContratoProposta(int) 

    /**
     * Sets the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @param nrSequenciaContratoNegocio the value of field
     * 'nrSequenciaContratoNegocio'.
     */
    public void setNrSequenciaContratoNegocio(long nrSequenciaContratoNegocio)
    {
        this._nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
        this._has_nrSequenciaContratoNegocio = true;
    } //-- void setNrSequenciaContratoNegocio(long) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ListarConvenioContaSalarioRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.request.ListarConvenioContaSalarioRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.request.ListarConvenioContaSalarioRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.request.ListarConvenioContaSalarioRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.request.ListarConvenioContaSalarioRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
