/*
 * Nome: br.com.bradesco.web.pgit.service.business.manterconultremessarecebida.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.manterconultremessarecebida.bean;

/**
 * Nome: AlterarUltimoNumeroRemessaEntradaDTO
 * <p>
 * Prop�sito:
 * </p>
 * .
 * 
 * @author : todo!
 * @version :
 */
public class AlterarUltimoNumeroRemessaEntradaDTO {

	private Long cdPessoaJuridicaContrato;

	private Integer cdTipoContratoNegocio;

	private Long nrSequenciaContratoNegocio;

	private Integer cdTipoLayoutArquivo;

	private Long cdPerfilTrocaArquivo;

	private Integer cdTipoLoteLayout;

	private Long nrUltimoArquivo;

	private Long nrMaximoArquivo;

	private Long cdClub;

	public Long getCdPessoaJuridicaContrato() {
		return cdPessoaJuridicaContrato;
	}

	public void setCdPessoaJuridicaContrato(Long cdPessoaJuridicaContrato) {
		this.cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
	}

	public Integer getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}

	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}

	public Long getNrSequenciaContratoNegocio() {
		return nrSequenciaContratoNegocio;
	}

	public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}

	public Integer getCdTipoLayoutArquivo() {
		return cdTipoLayoutArquivo;
	}

	public void setCdTipoLayoutArquivo(Integer cdTipoLayoutArquivo) {
		this.cdTipoLayoutArquivo = cdTipoLayoutArquivo;
	}

	public Long getCdPerfilTrocaArquivo() {
		return cdPerfilTrocaArquivo;
	}

	public void setCdPerfilTrocaArquivo(Long cdPerfilTrocaArquivo) {
		this.cdPerfilTrocaArquivo = cdPerfilTrocaArquivo;
	}

	public Integer getCdTipoLoteLayout() {
		return cdTipoLoteLayout;
	}

	public void setCdTipoLoteLayout(Integer cdTipoLoteLayout) {
		this.cdTipoLoteLayout = cdTipoLoteLayout;
	}

	public Long getNrUltimoArquivo() {
		return nrUltimoArquivo;
	}

	public void setNrUltimoArquivo(Long nrUltimoArquivo) {
		this.nrUltimoArquivo = nrUltimoArquivo;
	}

	public Long getNrMaximoArquivo() {
		return nrMaximoArquivo;
	}

	public void setNrMaximoArquivo(Long nrMaximoArquivo) {
		this.nrMaximoArquivo = nrMaximoArquivo;
	}

	public Long getCdClub() {
		return cdClub;
	}

	public void setCdClub(Long cdClub) {
		this.cdClub = cdClub;
	}

}
