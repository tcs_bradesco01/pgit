/*
 * Nome: br.com.bradesco.web.pgit.service.business.consultas.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.consultas.bean;

/**
 * Nome: ConsultarEmailSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarEmailSaidaDTO{
	
	/** Atributo codMensagem. */
	private String codMensagem;
	
	/** Atributo mensagem. */
	private String mensagem;
	
	/** Atributo numeroLinhas. */
	private Integer numeroLinhas;
	
	/** Atributo cdCpfCnpjRecebedor. */
	private Long cdCpfCnpjRecebedor;
	
	/** Atributo cdFilialCnpjRecebedor. */
	private Integer cdFilialCnpjRecebedor;
	
	/** Atributo cdControleCpfRecebedor. */
	private Integer cdControleCpfRecebedor;
	
	/** Atributo dsNomeRazao. */
	private String dsNomeRazao;
	
	/** Atributo cdEnderecoEletronico. */
	private String cdEnderecoEletronico;

	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem(){
		return codMensagem;
	}

	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem){
		this.codMensagem = codMensagem;
	}

	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem(){
		return mensagem;
	}

	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem){
		this.mensagem = mensagem;
	}

	/**
	 * Get: numeroLinhas.
	 *
	 * @return numeroLinhas
	 */
	public Integer getNumeroLinhas(){
		return numeroLinhas;
	}

	/**
	 * Set: numeroLinhas.
	 *
	 * @param numeroLinhas the numero linhas
	 */
	public void setNumeroLinhas(Integer numeroLinhas){
		this.numeroLinhas = numeroLinhas;
	}

	/**
	 * Get: cdCpfCnpjRecebedor.
	 *
	 * @return cdCpfCnpjRecebedor
	 */
	public Long getCdCpfCnpjRecebedor(){
		return cdCpfCnpjRecebedor;
	}

	/**
	 * Set: cdCpfCnpjRecebedor.
	 *
	 * @param cdCpfCnpjRecebedor the cd cpf cnpj recebedor
	 */
	public void setCdCpfCnpjRecebedor(Long cdCpfCnpjRecebedor){
		this.cdCpfCnpjRecebedor = cdCpfCnpjRecebedor;
	}

	/**
	 * Get: cdFilialCnpjRecebedor.
	 *
	 * @return cdFilialCnpjRecebedor
	 */
	public Integer getCdFilialCnpjRecebedor(){
		return cdFilialCnpjRecebedor;
	}

	/**
	 * Set: cdFilialCnpjRecebedor.
	 *
	 * @param cdFilialCnpjRecebedor the cd filial cnpj recebedor
	 */
	public void setCdFilialCnpjRecebedor(Integer cdFilialCnpjRecebedor){
		this.cdFilialCnpjRecebedor = cdFilialCnpjRecebedor;
	}

	/**
	 * Get: cdControleCpfRecebedor.
	 *
	 * @return cdControleCpfRecebedor
	 */
	public Integer getCdControleCpfRecebedor(){
		return cdControleCpfRecebedor;
	}

	/**
	 * Set: cdControleCpfRecebedor.
	 *
	 * @param cdControleCpfRecebedor the cd controle cpf recebedor
	 */
	public void setCdControleCpfRecebedor(Integer cdControleCpfRecebedor){
		this.cdControleCpfRecebedor = cdControleCpfRecebedor;
	}

	/**
	 * Get: dsNomeRazao.
	 *
	 * @return dsNomeRazao
	 */
	public String getDsNomeRazao(){
		return dsNomeRazao;
	}

	/**
	 * Set: dsNomeRazao.
	 *
	 * @param dsNomeRazao the ds nome razao
	 */
	public void setDsNomeRazao(String dsNomeRazao){
		this.dsNomeRazao = dsNomeRazao;
	}

	/**
	 * Get: cdEnderecoEletronico.
	 *
	 * @return cdEnderecoEletronico
	 */
	public String getCdEnderecoEletronico(){
		return cdEnderecoEletronico;
	}

	/**
	 * Set: cdEnderecoEletronico.
	 *
	 * @param cdEnderecoEletronico the cd endereco eletronico
	 */
	public void setCdEnderecoEletronico(String cdEnderecoEletronico){
		this.cdEnderecoEletronico = cdEnderecoEletronico;
	}
}