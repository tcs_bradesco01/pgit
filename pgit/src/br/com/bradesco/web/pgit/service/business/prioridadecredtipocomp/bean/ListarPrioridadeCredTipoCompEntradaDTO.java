/*
 * Nome: br.com.bradesco.web.pgit.service.business.prioridadecredtipocomp.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.prioridadecredtipocomp.bean;

import java.util.Date;

/**
 * Nome: ListarPrioridadeCredTipoCompEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarPrioridadeCredTipoCompEntradaDTO {
	
	/** Atributo tipoCompromisso. */
	private int tipoCompromisso;
	
	/** Atributo prioridade. */
	private int prioridade;
	
	/** Atributo cdCustoPrioridadeProduto. */
	private String cdCustoPrioridadeProduto;
	
	/** Atributo dtInicioVig. */
	private Date dtInicioVig;
	
	/**
	 * Get: prioridade.
	 *
	 * @return prioridade
	 */
	public int getPrioridade() {
		return prioridade;
	}
	
	/**
	 * Set: prioridade.
	 *
	 * @param prioridade the prioridade
	 */
	public void setPrioridade(int prioridade) {
		this.prioridade = prioridade;
	}
	
	/**
	 * Get: tipoCompromisso.
	 *
	 * @return tipoCompromisso
	 */
	public int getTipoCompromisso() {
		return tipoCompromisso;
	}
	
	/**
	 * Set: tipoCompromisso.
	 *
	 * @param tipoCompromisso the tipo compromisso
	 */
	public void setTipoCompromisso(int tipoCompromisso) {
		this.tipoCompromisso = tipoCompromisso;
	}
	
	/**
	 * Get: cdCustoPrioridadeProduto.
	 *
	 * @return cdCustoPrioridadeProduto
	 */
	public String getCdCustoPrioridadeProduto() {
		return cdCustoPrioridadeProduto;
	}
	
	/**
	 * Set: cdCustoPrioridadeProduto.
	 *
	 * @param cdCustoPrioridadeProduto the cd custo prioridade produto
	 */
	public void setCdCustoPrioridadeProduto(String cdCustoPrioridadeProduto) {
		this.cdCustoPrioridadeProduto = cdCustoPrioridadeProduto;
	}
	
	/**
	 * Get: dtInicioVig.
	 *
	 * @return dtInicioVig
	 */
	public Date getDtInicioVig() {
		return dtInicioVig;
	}
	
	/**
	 * Set: dtInicioVig.
	 *
	 * @param dtInicioVig the dt inicio vig
	 */
	public void setDtInicioVig(Date dtInicioVig) {
		this.dtInicioVig = dtInicioVig;
	}

}
