/*
 * Nome: br.com.bradesco.web.pgit.service.business.assoclinhatipomsg.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.assoclinhatipomsg.bean;

/**
 * Nome: ListarLinhaMsgComprovanteSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarLinhaMsgComprovanteSaidaDTO {
	
	/** Atributo codMensagem. */
	private String codMensagem;
	
	/** Atributo mensagem. */
	private String mensagem;
	
	/** Atributo cdTipoMensagem. */
	private int cdTipoMensagem;
	
	/** Atributo dsTipoMensagem. */
	private String dsTipoMensagem;
	
	/** Atributo prefixo. */
	private String prefixo;
	
	/** Atributo codigoLinhaMsgComprovante. */
	private int codigoLinhaMsgComprovante;
	
	/** Atributo dsMensagemComprovante. */
	private String dsMensagemComprovante;
	
	/** Atributo cdRecurso. */
	private int cdRecurso;
	
	/** Atributo dsRecurso. */
	private String dsRecurso;
	
	/** Atributo cdIdioma. */
	private int cdIdioma;
	
	/** Atributo dsIdioma. */
	private String dsIdioma;
	
	/** Atributo ordemLinha. */
	private long ordemLinha;
	
	/**
	 * Get: cdIdioma.
	 *
	 * @return cdIdioma
	 */
	public int getCdIdioma() {
		return cdIdioma;
	}
	
	/**
	 * Set: cdIdioma.
	 *
	 * @param cdIdioma the cd idioma
	 */
	public void setCdIdioma(int cdIdioma) {
		this.cdIdioma = cdIdioma;
	}
	
	/**
	 * Get: cdRecurso.
	 *
	 * @return cdRecurso
	 */
	public int getCdRecurso() {
		return cdRecurso;
	}
	
	/**
	 * Set: cdRecurso.
	 *
	 * @param cdRecurso the cd recurso
	 */
	public void setCdRecurso(int cdRecurso) {
		this.cdRecurso = cdRecurso;
	}
	
	/**
	 * Get: cdTipoMensagem.
	 *
	 * @return cdTipoMensagem
	 */
	public int getCdTipoMensagem() {
		return cdTipoMensagem;
	}
	
	/**
	 * Set: cdTipoMensagem.
	 *
	 * @param cdTipoMensagem the cd tipo mensagem
	 */
	public void setCdTipoMensagem(int cdTipoMensagem) {
		this.cdTipoMensagem = cdTipoMensagem;
	}
	
	/**
	 * Get: codigoLinhaMsgComprovante.
	 *
	 * @return codigoLinhaMsgComprovante
	 */
	public int getCodigoLinhaMsgComprovante() {
		return codigoLinhaMsgComprovante;
	}
	
	/**
	 * Set: codigoLinhaMsgComprovante.
	 *
	 * @param codigoLinhaMsgComprovante the codigo linha msg comprovante
	 */
	public void setCodigoLinhaMsgComprovante(int codigoLinhaMsgComprovante) {
		this.codigoLinhaMsgComprovante = codigoLinhaMsgComprovante;
	}
	
	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}
	
	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}
	
	/**
	 * Get: dsIdioma.
	 *
	 * @return dsIdioma
	 */
	public String getDsIdioma() {
		return dsIdioma;
	}
	
	/**
	 * Set: dsIdioma.
	 *
	 * @param dsIdioma the ds idioma
	 */
	public void setDsIdioma(String dsIdioma) {
		this.dsIdioma = dsIdioma;
	}
	
	/**
	 * Get: dsMensagemComprovante.
	 *
	 * @return dsMensagemComprovante
	 */
	public String getDsMensagemComprovante() {
		return dsMensagemComprovante;
	}
	
	/**
	 * Set: dsMensagemComprovante.
	 *
	 * @param dsMensagemComprovante the ds mensagem comprovante
	 */
	public void setDsMensagemComprovante(String dsMensagemComprovante) {
		this.dsMensagemComprovante = dsMensagemComprovante;
	}
	
	/**
	 * Get: dsRecurso.
	 *
	 * @return dsRecurso
	 */
	public String getDsRecurso() {
		return dsRecurso;
	}
	
	/**
	 * Set: dsRecurso.
	 *
	 * @param dsRecurso the ds recurso
	 */
	public void setDsRecurso(String dsRecurso) {
		this.dsRecurso = dsRecurso;
	}
	
	/**
	 * Get: dsTipoMensagem.
	 *
	 * @return dsTipoMensagem
	 */
	public String getDsTipoMensagem() {
		return dsTipoMensagem;
	}
	
	/**
	 * Set: dsTipoMensagem.
	 *
	 * @param dsTipoMensagem the ds tipo mensagem
	 */
	public void setDsTipoMensagem(String dsTipoMensagem) {
		this.dsTipoMensagem = dsTipoMensagem;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	/**
	 * Get: ordemLinha.
	 *
	 * @return ordemLinha
	 */
	public long getOrdemLinha() {
		return ordemLinha;
	}
	
	/**
	 * Set: ordemLinha.
	 *
	 * @param ordemLinha the ordem linha
	 */
	public void setOrdemLinha(long ordemLinha) {
		this.ordemLinha = ordemLinha;
	}
	
	/**
	 * Get: prefixo.
	 *
	 * @return prefixo
	 */
	public String getPrefixo() {
		return prefixo;
	}
	
	/**
	 * Set: prefixo.
	 *
	 * @param prefixo the prefixo
	 */
	public void setPrefixo(String prefixo) {
		this.prefixo = prefixo;
	}
	
	


}
