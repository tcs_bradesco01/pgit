/**
 * Nome: br.com.bradesco.web.pgit.service.business.solrelempresasacomp
 * Compilador: JDK 1.5
 * Prop�sito: INSERIR O PROP�SITO DAS CLASSES DO PACOTE
 * Data da cria��o: <dd/MM/yyyy>
 * Par�metros de compila��o: -d
 */
package br.com.bradesco.web.pgit.service.business.solrelempresasacomp;

/**
 * Nome: ISolrelempresasacompServiceConstants
 * <p>
 * Prop�sito: Interface de constantes do adaptador Solrelempresasacomp
 * </p>
 * 
 * @author CPM Braxis / TI Melhorias - Arquitetura
 * @version 1.0
 */
public interface ISolrelempresasacompServiceConstants {

	/** Atributo MAX_OCORRENCIAS_50. */
	int MAX_OCORRENCIAS_50 = 50;

	/** Atributo PENDENTE. */
	String PENDENTE = "Pendente";

	/** Atributo ATENDIDO. */
	String ATENDIDO = "Atendido";

}