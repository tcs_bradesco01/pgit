/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantercontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantercontrato.bean;

import java.math.BigDecimal;

/**
 * Nome: ConsultarLayoutArquivoContratoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>
 * .
 * 
 * @author : todo!
 * @version :
 */
public class ConsultarLayoutArquivoContratoSaidaDTO {

	/** Atributo codMensagem. */
	private String codMensagem;

	/** Atributo mensagem. */
	private String mensagem;

	/** Atributo cdSituacaoLayoutContrato. */
	private Integer cdSituacaoLayoutContrato;

	/** Atributo dsSituacaoLayoutContrato. */
	private String dsSituacaoLayoutContrato;

	/** Atributo cdRespostaCustoEmpr. */
	private Integer cdRespostaCustoEmpr;

	/** Atributo dsRespostaCustoEmpr. */
	private String dsRespostaCustoEmpr;

	/** Atributo cdPercentualOrgnzTransmissao. */
	private BigDecimal cdPercentualOrgnzTransmissao;

	/** Atributo cdMensagemLinhaExtrato. */
	private Integer cdMensagemLinhaExtrato;

	/** Atributo nrVersaoLayoutArquivo. */
	private Integer nrVersaoLayoutArquivo;

	/** Atributo cdTipoControlePagamento. */
	private Integer cdTipoControlePagamento;

	/** Atributo dsTipoControlePagamento. */
	private String dsTipoControlePagamento;

	/** Atributo cdUsuarioInclusao. */
	private String cdUsuarioInclusao;

	/** Atributo cdUsuarioInclusaoExterno. */
	private String cdUsuarioInclusaoExterno;

	/** Atributo hrInclusaoRegistro. */
	private String hrInclusaoRegistro;

	/** Atributo cdOperacaoCanalInclusao. */
	private String cdOperacaoCanalInclusao;

	/** Atributo cdTipoCanalInclusao. */
	private Integer cdTipoCanalInclusao;

	/** Atributo dsTipoCanalInclusao. */
	private String dsTipoCanalInclusao;

	/** Atributo cdUsuarioManutencao. */
	private String cdUsuarioManutencao;

	/** Atributo cdUsuarioManutencaoExterno. */
	private String cdUsuarioManutencaoExterno;

	/** Atributo hrManutencaoRegistro. */
	private String hrManutencaoRegistro;

	/** Atributo cdOperacaoCanalManutencao. */
	private String cdOperacaoCanalManutencao;

	/** Atributo cdTipoCanalManutencao. */
	private Integer cdTipoCanalManutencao;

	/** Atributo dsTipoCanalManutencao. */
	private String dsTipoCanalManutencao;

	public String getCodMensagem() {
		return codMensagem;
	}

	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}

	public String getMensagem() {
		return mensagem;
	}

	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	public Integer getCdSituacaoLayoutContrato() {
		return cdSituacaoLayoutContrato;
	}

	public void setCdSituacaoLayoutContrato(Integer cdSituacaoLayoutContrato) {
		this.cdSituacaoLayoutContrato = cdSituacaoLayoutContrato;
	}

	public String getDsSituacaoLayoutContrato() {
		return dsSituacaoLayoutContrato;
	}

	public void setDsSituacaoLayoutContrato(String dsSituacaoLayoutContrato) {
		this.dsSituacaoLayoutContrato = dsSituacaoLayoutContrato;
	}

	public Integer getCdRespostaCustoEmpr() {
		return cdRespostaCustoEmpr;
	}

	public void setCdRespostaCustoEmpr(Integer cdRespostaCustoEmpr) {
		this.cdRespostaCustoEmpr = cdRespostaCustoEmpr;
	}

	public String getDsRespostaCustoEmpr() {
		return dsRespostaCustoEmpr;
	}

	public void setDsRespostaCustoEmpr(String dsRespostaCustoEmpr) {
		this.dsRespostaCustoEmpr = dsRespostaCustoEmpr;
	}

	public BigDecimal getCdPercentualOrgnzTransmissao() {
		return cdPercentualOrgnzTransmissao;
	}

	public void setCdPercentualOrgnzTransmissao(
			BigDecimal cdPercentualOrgnzTransmissao) {
		this.cdPercentualOrgnzTransmissao = cdPercentualOrgnzTransmissao;
	}

	public Integer getCdMensagemLinhaExtrato() {
		return cdMensagemLinhaExtrato;
	}

	public void setCdMensagemLinhaExtrato(Integer cdMensagemLinhaExtrato) {
		this.cdMensagemLinhaExtrato = cdMensagemLinhaExtrato;
	}

	public Integer getNrVersaoLayoutArquivo() {
		return nrVersaoLayoutArquivo;
	}

	public void setNrVersaoLayoutArquivo(Integer nrVersaoLayoutArquivo) {
		this.nrVersaoLayoutArquivo = nrVersaoLayoutArquivo;
	}

	public Integer getCdTipoControlePagamento() {
		return cdTipoControlePagamento;
	}

	public void setCdTipoControlePagamento(Integer cdTipoControlePagamento) {
		this.cdTipoControlePagamento = cdTipoControlePagamento;
	}

	public String getDsTipoControlePagamento() {
		return dsTipoControlePagamento;
	}

	public void setDsTipoControlePagamento(String dsTipoControlePagamento) {
		this.dsTipoControlePagamento = dsTipoControlePagamento;
	}

	public String getCdUsuarioInclusao() {
		return cdUsuarioInclusao;
	}

	public void setCdUsuarioInclusao(String cdUsuarioInclusao) {
		this.cdUsuarioInclusao = cdUsuarioInclusao;
	}

	public String getCdUsuarioInclusaoExterno() {
		return cdUsuarioInclusaoExterno;
	}

	public void setCdUsuarioInclusaoExterno(String cdUsuarioInclusaoExterno) {
		this.cdUsuarioInclusaoExterno = cdUsuarioInclusaoExterno;
	}

	public String getHrInclusaoRegistro() {
		return hrInclusaoRegistro;
	}

	public void setHrInclusaoRegistro(String hrInclusaoRegistro) {
		this.hrInclusaoRegistro = hrInclusaoRegistro;
	}

	public String getCdOperacaoCanalInclusao() {
		return cdOperacaoCanalInclusao;
	}

	public void setCdOperacaoCanalInclusao(String cdOperacaoCanalInclusao) {
		this.cdOperacaoCanalInclusao = cdOperacaoCanalInclusao;
	}

	public Integer getCdTipoCanalInclusao() {
		return cdTipoCanalInclusao;
	}

	public void setCdTipoCanalInclusao(Integer cdTipoCanalInclusao) {
		this.cdTipoCanalInclusao = cdTipoCanalInclusao;
	}

	public String getDsTipoCanalInclusao() {
		return dsTipoCanalInclusao;
	}

	public void setDsTipoCanalInclusao(String dsTipoCanalInclusao) {
		this.dsTipoCanalInclusao = dsTipoCanalInclusao;
	}

	public String getCdUsuarioManutencao() {
		return cdUsuarioManutencao;
	}

	public void setCdUsuarioManutencao(String cdUsuarioManutencao) {
		this.cdUsuarioManutencao = cdUsuarioManutencao;
	}

	public String getCdUsuarioManutencaoExterno() {
		return cdUsuarioManutencaoExterno;
	}

	public void setCdUsuarioManutencaoExterno(String cdUsuarioManutencaoExterno) {
		this.cdUsuarioManutencaoExterno = cdUsuarioManutencaoExterno;
	}

	public String getHrManutencaoRegistro() {
		return hrManutencaoRegistro;
	}

	public void setHrManutencaoRegistro(String hrManutencaoRegistro) {
		this.hrManutencaoRegistro = hrManutencaoRegistro;
	}

	public String getCdOperacaoCanalManutencao() {
		return cdOperacaoCanalManutencao;
	}

	public void setCdOperacaoCanalManutencao(String cdOperacaoCanalManutencao) {
		this.cdOperacaoCanalManutencao = cdOperacaoCanalManutencao;
	}

	public String getDsTipoCanalManutencao() {
		return dsTipoCanalManutencao;
	}

	public void setDsTipoCanalManutencao(String dsTipoCanalManutencao) {
		this.dsTipoCanalManutencao = dsTipoCanalManutencao;
	}

	public Integer getCdTipoCanalManutencao() {
		return cdTipoCanalManutencao;
	}

	public void setCdTipoCanalManutencao(Integer cdTipoCanalManutencao) {
		this.cdTipoCanalManutencao = cdTipoCanalManutencao;
	}	
	
}
