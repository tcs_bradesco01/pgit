/*
 * Nome: br.com.bradesco.web.pgit.service.business.imprimir.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.imprimir.bean;

/**
 * Nome: OcorrenciaImprimirComprovanteSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class OcorrenciaImprimirComprovanteSaidaDTO{
	
	/** Atributo centroCusto. */
	private String centroCusto;
	
	/** Atributo protocolo. */
	private Long protocolo;

	/**
	 * Get: centroCusto.
	 *
	 * @return centroCusto
	 */
	public String getCentroCusto(){
		return centroCusto;
	}

	/**
	 * Set: centroCusto.
	 *
	 * @param centroCusto the centro custo
	 */
	public void setCentroCusto(String centroCusto){
		this.centroCusto = centroCusto;
	}

	/**
	 * Get: protocolo.
	 *
	 * @return protocolo
	 */
	public Long getProtocolo(){
		return protocolo;
	}

	/**
	 * Set: protocolo.
	 *
	 * @param protocolo the protocolo
	 */
	public void setProtocolo(Long protocolo){
		this.protocolo = protocolo;
	}
}