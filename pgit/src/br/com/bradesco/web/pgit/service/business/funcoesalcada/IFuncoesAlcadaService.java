/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/interfaz.ftl,v $
 * $Id: interfaz.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: interfaz.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.funcoesalcada;

import java.util.List;

import br.com.bradesco.web.pgit.service.business.funcoesalcada.bean.AlterarFuncoesAlcadaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.funcoesalcada.bean.AlterarFuncoesAlcadaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.funcoesalcada.bean.DetalharFuncoesAlcadaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.funcoesalcada.bean.DetalharFuncoesAlcadaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.funcoesalcada.bean.ExcluirFuncoesAlcadaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.funcoesalcada.bean.ExcluirFuncoesAlcadaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.funcoesalcada.bean.IncluirFuncoesAlcadaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.funcoesalcada.bean.IncluirFuncoesAlcadaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.funcoesalcada.bean.ListarFuncoesAlcadaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.funcoesalcada.bean.ListarFuncoesAlcadaSaidaDTO;

/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Interface do adaptador: ManterFuncoesAlcada
 * </p>
 * 
 * @comment CODIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public interface IFuncoesAlcadaService {

    /**
     * M�todo de exemplo.
     */
    void sampleManterFuncoesAlcada();
    
    /**
     * Listar funcoes alcadas.
     *
     * @param listarFuncoesAlcadaEntradaDTO the listar funcoes alcada entrada dto
     * @return the list< listar funcoes alcada saida dt o>
     */
    List<ListarFuncoesAlcadaSaidaDTO> listarFuncoesAlcadas(ListarFuncoesAlcadaEntradaDTO listarFuncoesAlcadaEntradaDTO);
    
    /**
     * Incluir funcoes alcada.
     *
     * @param incluirFuncoesAlcadaEntradaDTO the incluir funcoes alcada entrada dto
     * @return the incluir funcoes alcada saida dto
     */
    IncluirFuncoesAlcadaSaidaDTO incluirFuncoesAlcada(IncluirFuncoesAlcadaEntradaDTO incluirFuncoesAlcadaEntradaDTO);
    
    /**
     * Detalhar funcoes alcadas.
     *
     * @param detalharFuncoesAlcadaEntradaDTO the detalhar funcoes alcada entrada dto
     * @return the detalhar funcoes alcada saida dto
     */
    DetalharFuncoesAlcadaSaidaDTO detalharFuncoesAlcadas(DetalharFuncoesAlcadaEntradaDTO detalharFuncoesAlcadaEntradaDTO);
    
    /**
     * Excluir funcoes alcada.
     *
     * @param excluirFuncoesAlcadaEntradaDTO the excluir funcoes alcada entrada dto
     * @return the excluir funcoes alcada saida dto
     */
    ExcluirFuncoesAlcadaSaidaDTO excluirFuncoesAlcada(ExcluirFuncoesAlcadaEntradaDTO excluirFuncoesAlcadaEntradaDTO);
    
    /**
     * Alterar funcoes alcada.
     *
     * @param alterarFuncoesAlcadaEntradaDTO the alterar funcoes alcada entrada dto
     * @return the alterar funcoes alcada saida dto
     */
    AlterarFuncoesAlcadaSaidaDTO alterarFuncoesAlcada(AlterarFuncoesAlcadaEntradaDTO alterarFuncoesAlcadaEntradaDTO);

}

