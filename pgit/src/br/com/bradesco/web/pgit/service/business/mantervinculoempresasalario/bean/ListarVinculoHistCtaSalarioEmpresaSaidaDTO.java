/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantervinculoempresasalario.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantervinculoempresasalario.bean;

import br.com.bradesco.web.pgit.service.data.pdc.listarvinculohistctasalarioempresa.response.Ocorrencias;

/**
 * Nome: ListarVinculoHistCtaSalarioEmpresaSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarVinculoHistCtaSalarioEmpresaSaidaDTO {
	
	 /** Atributo cdPessoaJuridVinc. */
 	private Long cdPessoaJuridVinc;
     
     /** Atributo cdTipoContratoVinc. */
     private Integer cdTipoContratoVinc;
     
     /** Atributo nrSeqContratoVinc. */
     private Long nrSeqContratoVinc;
     
     /** Atributo hrInclusaoRegistro. */
     private String hrInclusaoRegistro;
     
     /** Atributo dtManutencao. */
     private String dtManutencao;
     
     /** Atributo hrManutencao. */
     private String hrManutencao;
     
     /** Atributo cdUsuario. */
     private String cdUsuario;
     
     /** Atributo cdBanco. */
     private Integer cdBanco;
     
     /** Atributo dsBanco. */
     private String dsBanco;
     
     /** Atributo cdAgenciaBancaria. */
     private Integer cdAgenciaBancaria;
     
     /** Atributo cdDigitoAgenciaBancaria. */
     private Integer cdDigitoAgenciaBancaria;
     
     /** Atributo dsAgenciaBancaria. */
     private String dsAgenciaBancaria;
     
     /** Atributo cdConta. */
     private Long cdConta;
     
     /** Atributo cdDigitoConta. */
     private String cdDigitoConta;
     
     /** Atributo tpManutencao. */
     private String tpManutencao;
     
	/**
	 * Listar vinculo hist cta salario empresa saida dto.
	 */
	public ListarVinculoHistCtaSalarioEmpresaSaidaDTO() {
		super();
	}
	
	/**
	 * Listar vinculo hist cta salario empresa saida dto.
	 *
	 * @param ocorrencia the ocorrencia
	 */
	public ListarVinculoHistCtaSalarioEmpresaSaidaDTO(Ocorrencias ocorrencia) {
		this.cdPessoaJuridVinc = ocorrencia.getCdPessoaJuridVinc();
		this.cdTipoContratoVinc = ocorrencia.getCdTipoContratoVinc();
		this.nrSeqContratoVinc = ocorrencia.getNrSeqContratoVinc();
		this.hrInclusaoRegistro = ocorrencia.getHrInclusaoRegistro();
		this.dtManutencao = ocorrencia.getDtManutencao();
		this.hrManutencao = ocorrencia.getHrManutencao();
		this.cdBanco = ocorrencia.getCdBanco();
		this.dsBanco = ocorrencia.getDsBanco();
		this.cdAgenciaBancaria = ocorrencia.getCdAgenciaBancaria();
		this.cdDigitoAgenciaBancaria = ocorrencia.getCdDigitoAgenciaBancaria();
		this.dsAgenciaBancaria = ocorrencia.getDsAgenciaBancaria();
		this.cdConta = ocorrencia.getCdConta();
		this.cdDigitoConta = ocorrencia.getCdDigitoConta();
		this.tpManutencao = ocorrencia.getTpManutencao();
		this.cdUsuario = ocorrencia.getCdUsuario();
	}

	/**
	 * Get: dataHoraManutencao.
	 *
	 * @return dataHoraManutencao
	 */
	public String getDataHoraManutencao() {
		return dtManutencao + " " + hrManutencao;
	}
	
	/**
	 * Get: banco.
	 *
	 * @return banco
	 */
	public String getBanco(){
		return dsBanco == null ? String.valueOf(cdBanco) : cdBanco + " " + dsBanco;
	}
	
	/**
	 * Get: agenciaDigito.
	 *
	 * @return agenciaDigito
	 */
	public String getAgenciaDigito(){
		return cdAgenciaBancaria + "-" + cdDigitoAgenciaBancaria + " - " + dsAgenciaBancaria;
	}
	
	/**
	 * Get: contaDigitoSalario.
	 *
	 * @return contaDigitoSalario
	 */
	public String getContaDigitoSalario() {
		return cdConta + "-" + cdDigitoConta;
	}
	
	/**
	 * Get: cdDigitoAgenciaBancaria.
	 *
	 * @return cdDigitoAgenciaBancaria
	 */
	public Integer getCdDigitoAgenciaBancaria() {
		return cdDigitoAgenciaBancaria;
	}

	/**
	 * Set: cdDigitoAgenciaBancaria.
	 *
	 * @param cdDigitoAgenciaBancaria the cd digito agencia bancaria
	 */
	public void setCdDigitoAgenciaBancaria(Integer cdDigitoAgenciaBancaria) {
		this.cdDigitoAgenciaBancaria = cdDigitoAgenciaBancaria;
	}

	/**
	 * Get: dsAgenciaBancaria.
	 *
	 * @return dsAgenciaBancaria
	 */
	public String getDsAgenciaBancaria() {
		return dsAgenciaBancaria;
	}

	/**
	 * Set: dsAgenciaBancaria.
	 *
	 * @param dsAgenciaBancaria the ds agencia bancaria
	 */
	public void setDsAgenciaBancaria(String dsAgenciaBancaria) {
		this.dsAgenciaBancaria = dsAgenciaBancaria;
	}

	/**
	 * Get: dsBanco.
	 *
	 * @return dsBanco
	 */
	public String getDsBanco() {
		return dsBanco;
	}

	/**
	 * Set: dsBanco.
	 *
	 * @param dsBanco the ds banco
	 */
	public void setDsBanco(String dsBanco) {
		this.dsBanco = dsBanco;
	}
	
	/**
	 * Get: cdBanco.
	 *
	 * @return cdBanco
	 */
	public Integer getCdBanco() {
		return cdBanco;
	}
	
	/**
	 * Set: cdBanco.
	 *
	 * @param cdBanco the cd banco
	 */
	public void setCdBanco(Integer cdBanco) {
		this.cdBanco = cdBanco;
	}
	
	/**
	 * Get: cdConta.
	 *
	 * @return cdConta
	 */
	public Long getCdConta() {
		return cdConta;
	}
	
	/**
	 * Set: cdConta.
	 *
	 * @param cdConta the cd conta
	 */
	public void setCdConta(Long cdConta) {
		this.cdConta = cdConta;
	}
	
	/**
	 * Get: cdDigitoConta.
	 *
	 * @return cdDigitoConta
	 */
	public String getCdDigitoConta() {
		return cdDigitoConta;
	}
	
	/**
	 * Set: cdDigitoConta.
	 *
	 * @param cdDigitoConta the cd digito conta
	 */
	public void setCdDigitoConta(String cdDigitoConta) {
		this.cdDigitoConta = cdDigitoConta;
	}
	
	/**
	 * Get: cdPessoaJuridVinc.
	 *
	 * @return cdPessoaJuridVinc
	 */
	public Long getCdPessoaJuridVinc() {
		return cdPessoaJuridVinc;
	}
	
	/**
	 * Set: cdPessoaJuridVinc.
	 *
	 * @param cdPessoaJuridVinc the cd pessoa jurid vinc
	 */
	public void setCdPessoaJuridVinc(Long cdPessoaJuridVinc) {
		this.cdPessoaJuridVinc = cdPessoaJuridVinc;
	}
	
	/**
	 * Get: cdTipoContratoVinc.
	 *
	 * @return cdTipoContratoVinc
	 */
	public Integer getCdTipoContratoVinc() {
		return cdTipoContratoVinc;
	}
	
	/**
	 * Set: cdTipoContratoVinc.
	 *
	 * @param cdTipoContratoVinc the cd tipo contrato vinc
	 */
	public void setCdTipoContratoVinc(Integer cdTipoContratoVinc) {
		this.cdTipoContratoVinc = cdTipoContratoVinc;
	}
	
	/**
	 * Get: nrSeqContratoVinc.
	 *
	 * @return nrSeqContratoVinc
	 */
	public Long getNrSeqContratoVinc() {
		return nrSeqContratoVinc;
	}
	
	/**
	 * Set: nrSeqContratoVinc.
	 *
	 * @param nrSeqContratoVinc the nr seq contrato vinc
	 */
	public void setNrSeqContratoVinc(Long nrSeqContratoVinc) {
		this.nrSeqContratoVinc = nrSeqContratoVinc;
	}

	/**
	 * Get: cdAgenciaBancaria.
	 *
	 * @return cdAgenciaBancaria
	 */
	public Integer getCdAgenciaBancaria() {
		return cdAgenciaBancaria;
	}

	/**
	 * Set: cdAgenciaBancaria.
	 *
	 * @param cdAgenciaBancaria the cd agencia bancaria
	 */
	public void setCdAgenciaBancaria(Integer cdAgenciaBancaria) {
		this.cdAgenciaBancaria = cdAgenciaBancaria;
	}

	/**
	 * Get: cdUsuario.
	 *
	 * @return cdUsuario
	 */
	public String getCdUsuario() {
		return cdUsuario;
	}

	/**
	 * Set: cdUsuario.
	 *
	 * @param cdUsuario the cd usuario
	 */
	public void setCdUsuario(String cdUsuario) {
		this.cdUsuario = cdUsuario;
	}

	/**
	 * Get: dtManutencao.
	 *
	 * @return dtManutencao
	 */
	public String getDtManutencao() {
		return dtManutencao;
	}

	/**
	 * Set: dtManutencao.
	 *
	 * @param dtManutencao the dt manutencao
	 */
	public void setDtManutencao(String dtManutencao) {
		this.dtManutencao = dtManutencao;
	}

	/**
	 * Get: hrInclusaoRegistro.
	 *
	 * @return hrInclusaoRegistro
	 */
	public String getHrInclusaoRegistro() {
		return hrInclusaoRegistro;
	}

	/**
	 * Set: hrInclusaoRegistro.
	 *
	 * @param hrInclusaoRegistro the hr inclusao registro
	 */
	public void setHrInclusaoRegistro(String hrInclusaoRegistro) {
		this.hrInclusaoRegistro = hrInclusaoRegistro;
	}

	/**
	 * Get: hrManutencao.
	 *
	 * @return hrManutencao
	 */
	public String getHrManutencao() {
		return hrManutencao;
	}

	/**
	 * Set: hrManutencao.
	 *
	 * @param hrManutencao the hr manutencao
	 */
	public void setHrManutencao(String hrManutencao) {
		this.hrManutencao = hrManutencao;
	}

	/**
	 * Get: tpManutencao.
	 *
	 * @return tpManutencao
	 */
	public String getTpManutencao() {
	    return tpManutencao;
	}

	/**
	 * Set: tpManutencao.
	 *
	 * @param tpManutencao the tp manutencao
	 */
	public void setTpManutencao(String tpManutencao) {
	    this.tpManutencao = tpManutencao;
	}

}
