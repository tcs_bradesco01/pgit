/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id: ListaRegraResponse.java,v 1.4 2009/05/28 12:54:07 cpm.com.br\heslei.silva Exp $
 */

package br.com.bradesco.web.pgit.service.data.pdc.listaregra.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.util.Vector;

import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;

/**
 * Class ListaRegraResponse.
 * 
 * @version $Revision: 1.4 $ $Date: 2009/05/28 12:54:07 $
 */
public class ListaRegraResponse implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _codMensagem
     */
    private java.lang.String _codMensagem;

    /**
     * Field _mensagem
     */
    private java.lang.String _mensagem;

    /**
     * Field _permissao
     */
    private java.lang.String _permissao;

    /**
     * Field _qtdeOcorrencia
     */
    private int _qtdeOcorrencia = 0;

    /**
     * keeps track of state for field: _qtdeOcorrencia
     */
    private boolean _has_qtdeOcorrencia;

    /**
     * Field _ocorrenciaList
     */
    private java.util.Vector _ocorrenciaList;


      //----------------/
     //- Constructors -/
    //----------------/

    public ListaRegraResponse() 
     {
        super();
        _ocorrenciaList = new Vector();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listaregra.response.ListaRegraResponse()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method addOcorrencia
     * 
     * 
     * 
     * @param vOcorrencia
     */
    public void addOcorrencia(br.com.bradesco.web.pgit.service.data.pdc.listaregra.response.Ocorrencia vOcorrencia)
        throws java.lang.IndexOutOfBoundsException
    {
        _ocorrenciaList.addElement(vOcorrencia);
    } //-- void addOcorrencia(br.com.bradesco.web.pgit.service.data.pdc.listaregra.response.Ocorrencia) 

    /**
     * Method addOcorrencia
     * 
     * 
     * 
     * @param index
     * @param vOcorrencia
     */
    public void addOcorrencia(int index, br.com.bradesco.web.pgit.service.data.pdc.listaregra.response.Ocorrencia vOcorrencia)
        throws java.lang.IndexOutOfBoundsException
    {
        _ocorrenciaList.insertElementAt(vOcorrencia, index);
    } //-- void addOcorrencia(int, br.com.bradesco.web.pgit.service.data.pdc.listaregra.response.Ocorrencia) 

    /**
     * Method deleteQtdeOcorrencia
     * 
     */
    public void deleteQtdeOcorrencia()
    {
        this._has_qtdeOcorrencia= false;
    } //-- void deleteQtdeOcorrencia() 

    /**
     * Method enumerateOcorrencia
     * 
     * 
     * 
     * @return Enumeration
     */
    public java.util.Enumeration enumerateOcorrencia()
    {
        return _ocorrenciaList.elements();
    } //-- java.util.Enumeration enumerateOcorrencia() 

    /**
     * Returns the value of field 'codMensagem'.
     * 
     * @return String
     * @return the value of field 'codMensagem'.
     */
    public java.lang.String getCodMensagem()
    {
        return this._codMensagem;
    } //-- java.lang.String getCodMensagem() 

    /**
     * Returns the value of field 'mensagem'.
     * 
     * @return String
     * @return the value of field 'mensagem'.
     */
    public java.lang.String getMensagem()
    {
        return this._mensagem;
    } //-- java.lang.String getMensagem() 

    /**
     * Method getOcorrencia
     * 
     * 
     * 
     * @param index
     * @return Ocorrencia
     */
    public br.com.bradesco.web.pgit.service.data.pdc.listaregra.response.Ocorrencia getOcorrencia(int index)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index >= _ocorrenciaList.size())) {
            throw new IndexOutOfBoundsException("getOcorrencia: Index value '"+index+"' not in range [0.."+(_ocorrenciaList.size() - 1) + "]");
        }
        
        return (br.com.bradesco.web.pgit.service.data.pdc.listaregra.response.Ocorrencia) _ocorrenciaList.elementAt(index);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listaregra.response.Ocorrencia getOcorrencia(int) 

    /**
     * Method getOcorrencia
     * 
     * 
     * 
     * @return Ocorrencia
     */
    public br.com.bradesco.web.pgit.service.data.pdc.listaregra.response.Ocorrencia[] getOcorrencia()
    {
        int size = _ocorrenciaList.size();
        br.com.bradesco.web.pgit.service.data.pdc.listaregra.response.Ocorrencia[] mArray = new br.com.bradesco.web.pgit.service.data.pdc.listaregra.response.Ocorrencia[size];
        for (int index = 0; index < size; index++) {
            mArray[index] = (br.com.bradesco.web.pgit.service.data.pdc.listaregra.response.Ocorrencia) _ocorrenciaList.elementAt(index);
        }
        return mArray;
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listaregra.response.Ocorrencia[] getOcorrencia() 

    /**
     * Method getOcorrenciaCount
     * 
     * 
     * 
     * @return int
     */
    public int getOcorrenciaCount()
    {
        return _ocorrenciaList.size();
    } //-- int getOcorrenciaCount() 

    /**
     * Returns the value of field 'permissao'.
     * 
     * @return String
     * @return the value of field 'permissao'.
     */
    public java.lang.String getPermissao()
    {
        return this._permissao;
    } //-- java.lang.String getPermissao() 

    /**
     * Returns the value of field 'qtdeOcorrencia'.
     * 
     * @return int
     * @return the value of field 'qtdeOcorrencia'.
     */
    public int getQtdeOcorrencia()
    {
        return this._qtdeOcorrencia;
    } //-- int getQtdeOcorrencia() 

    /**
     * Method hasQtdeOcorrencia
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtdeOcorrencia()
    {
        return this._has_qtdeOcorrencia;
    } //-- boolean hasQtdeOcorrencia() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Method removeAllOcorrencia
     * 
     */
    public void removeAllOcorrencia()
    {
        _ocorrenciaList.removeAllElements();
    } //-- void removeAllOcorrencia() 

    /**
     * Method removeOcorrencia
     * 
     * 
     * 
     * @param index
     * @return Ocorrencia
     */
    public br.com.bradesco.web.pgit.service.data.pdc.listaregra.response.Ocorrencia removeOcorrencia(int index)
    {
        java.lang.Object obj = _ocorrenciaList.elementAt(index);
        _ocorrenciaList.removeElementAt(index);
        return (br.com.bradesco.web.pgit.service.data.pdc.listaregra.response.Ocorrencia) obj;
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listaregra.response.Ocorrencia removeOcorrencia(int) 

    /**
     * Sets the value of field 'codMensagem'.
     * 
     * @param codMensagem the value of field 'codMensagem'.
     */
    public void setCodMensagem(java.lang.String codMensagem)
    {
        this._codMensagem = codMensagem;
    } //-- void setCodMensagem(java.lang.String) 

    /**
     * Sets the value of field 'mensagem'.
     * 
     * @param mensagem the value of field 'mensagem'.
     */
    public void setMensagem(java.lang.String mensagem)
    {
        this._mensagem = mensagem;
    } //-- void setMensagem(java.lang.String) 

    /**
     * Method setOcorrencia
     * 
     * 
     * 
     * @param index
     * @param vOcorrencia
     */
    public void setOcorrencia(int index, br.com.bradesco.web.pgit.service.data.pdc.listaregra.response.Ocorrencia vOcorrencia)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index >= _ocorrenciaList.size())) {
            throw new IndexOutOfBoundsException("setOcorrencia: Index value '"+index+"' not in range [0.." + (_ocorrenciaList.size() - 1) + "]");
        }
        _ocorrenciaList.setElementAt(vOcorrencia, index);
    } //-- void setOcorrencia(int, br.com.bradesco.web.pgit.service.data.pdc.listaregra.response.Ocorrencia) 

    /**
     * Method setOcorrencia
     * 
     * 
     * 
     * @param ocorrenciaArray
     */
    public void setOcorrencia(br.com.bradesco.web.pgit.service.data.pdc.listaregra.response.Ocorrencia[] ocorrenciaArray)
    {
        //-- copy array
        _ocorrenciaList.removeAllElements();
        for (int i = 0; i < ocorrenciaArray.length; i++) {
            _ocorrenciaList.addElement(ocorrenciaArray[i]);
        }
    } //-- void setOcorrencia(br.com.bradesco.web.pgit.service.data.pdc.listaregra.response.Ocorrencia) 

    /**
     * Sets the value of field 'permissao'.
     * 
     * @param permissao the value of field 'permissao'.
     */
    public void setPermissao(java.lang.String permissao)
    {
        this._permissao = permissao;
    } //-- void setPermissao(java.lang.String) 

    /**
     * Sets the value of field 'qtdeOcorrencia'.
     * 
     * @param qtdeOcorrencia the value of field 'qtdeOcorrencia'.
     */
    public void setQtdeOcorrencia(int qtdeOcorrencia)
    {
        this._qtdeOcorrencia = qtdeOcorrencia;
        this._has_qtdeOcorrencia = true;
    } //-- void setQtdeOcorrencia(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ListaRegraResponse
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listaregra.response.ListaRegraResponse unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listaregra.response.ListaRegraResponse) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listaregra.response.ListaRegraResponse.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listaregra.response.ListaRegraResponse unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
