package br.com.bradesco.web.pgit.service.business.mantervinculacaoconveniocontasalario.bean;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.ListarConvenioContaSalarioResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.Ocorrencias2;

public class OcorrenciasDTO2 implements Serializable{
	
	private static final long serialVersionUID = -3531072396433291002L;
	
	private Integer cdFaixaRenovQuest; 
	private String dsFaixaRenovQuest; 
	private Long qtdFuncCasaQuest;
	private Long qtdFuncDeslgQuest; 
	private BigDecimal ptcRenovFaixaQuest;
	
	public OcorrenciasDTO2() {
		super();
	}

	public OcorrenciasDTO2(Integer cdFaixaRenovQuest, String dsFaixaRenovQuest,
			Long qtdFuncCasaQuest, Long qtdFuncDeslgQuest,
			BigDecimal ptcRenovFaixaQuest) {
		super();
		this.cdFaixaRenovQuest = cdFaixaRenovQuest;
		this.dsFaixaRenovQuest = dsFaixaRenovQuest;
		this.qtdFuncCasaQuest = qtdFuncCasaQuest;
		this.qtdFuncDeslgQuest = qtdFuncDeslgQuest;
		this.ptcRenovFaixaQuest = ptcRenovFaixaQuest;
	}
	
	public List<OcorrenciasDTO2> listarOcorrenciasDTO2(ListarConvenioContaSalarioResponse response) {
		
		List<OcorrenciasDTO2> listaOcorrenciasDTO2 = new ArrayList<OcorrenciasDTO2>();
		OcorrenciasDTO2 ocorrenciasDTO2 = new OcorrenciasDTO2();
		
		for(Ocorrencias2 retorno : response.getOcorrencias2()){
			ocorrenciasDTO2.setCdFaixaRenovQuest(retorno.getCdFaixaRenovQuest());
			ocorrenciasDTO2.setDsFaixaRenovQuest(retorno.getDsFaixaRenovQuest());
			ocorrenciasDTO2.setQtdFuncCasaQuest(retorno.getQtdFuncCasaQuest());
			ocorrenciasDTO2.setQtdFuncDeslgQuest(retorno.getQtdFuncDeslgQuest());
			ocorrenciasDTO2.setPtcRenovFaixaQuest(retorno.getPtcRenovFaixaQuest());
			listaOcorrenciasDTO2.add(ocorrenciasDTO2);
		}
		return listaOcorrenciasDTO2;
	}

	public Integer getCdFaixaRenovQuest() {
		return cdFaixaRenovQuest;
	}

	public void setCdFaixaRenovQuest(Integer cdFaixaRenovQuest) {
		this.cdFaixaRenovQuest = cdFaixaRenovQuest;
	}

	public String getDsFaixaRenovQuest() {
		return dsFaixaRenovQuest;
	}

	public void setDsFaixaRenovQuest(String dsFaixaRenovQuest) {
		this.dsFaixaRenovQuest = dsFaixaRenovQuest;
	}

	public Long getQtdFuncCasaQuest() {
		return qtdFuncCasaQuest;
	}

	public void setQtdFuncCasaQuest(Long qtdFuncCasaQuest) {
		this.qtdFuncCasaQuest = qtdFuncCasaQuest;
	}

	public Long getQtdFuncDeslgQuest() {
		return qtdFuncDeslgQuest;
	}

	public void setQtdFuncDeslgQuest(Long qtdFuncDeslgQuest) {
		this.qtdFuncDeslgQuest = qtdFuncDeslgQuest;
	}

	public BigDecimal getPtcRenovFaixaQuest() {
		return ptcRenovFaixaQuest;
	}

	public void setPtcRenovFaixaQuest(BigDecimal ptcRenovFaixaQuest) {
		this.ptcRenovFaixaQuest = ptcRenovFaixaQuest;
	}

}
