/*
 * Nome: br.com.bradesco.web.pgit.service.business.prioridadecredtipocomp.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.prioridadecredtipocomp.bean;

/**
 * Nome: IncluirPrioridadeCredTipoCompEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class IncluirPrioridadeCredTipoCompEntradaDTO {
	
	
	/** Atributo tipoCompromisso. */
	private int tipoCompromisso;
	
	/** Atributo prioridade. */
	private int prioridade;
	
	/** Atributo dataInicioVigencia. */
	private String dataInicioVigencia;
	
	/** Atributo dataFimVigencia. */
	private String dataFimVigencia;
	
	/** Atributo centroCusto. */
	private String centroCusto;
	
	
	/**
	 * Get: dataInicioVigencia.
	 *
	 * @return dataInicioVigencia
	 */
	public String getDataInicioVigencia() {
		return dataInicioVigencia;
	}
	
	/**
	 * Set: dataInicioVigencia.
	 *
	 * @param dataInicioVigencia the data inicio vigencia
	 */
	public void setDataInicioVigencia(String dataInicioVigencia) {
		this.dataInicioVigencia = dataInicioVigencia;
	}

	/**
	 * Get: prioridade.
	 *
	 * @return prioridade
	 */
	public int getPrioridade() {
		return prioridade;
	}
	
	/**
	 * Set: prioridade.
	 *
	 * @param prioridade the prioridade
	 */
	public void setPrioridade(int prioridade) {
		this.prioridade = prioridade;
	}
	
	/**
	 * Get: tipoCompromisso.
	 *
	 * @return tipoCompromisso
	 */
	public int getTipoCompromisso() {
		return tipoCompromisso;
	}
	
	/**
	 * Set: tipoCompromisso.
	 *
	 * @param tipoCompromisso the tipo compromisso
	 */
	public void setTipoCompromisso(int tipoCompromisso) {
		this.tipoCompromisso = tipoCompromisso;
	}
	
	/**
	 * Get: dataFimVigencia.
	 *
	 * @return dataFimVigencia
	 */
	public String getDataFimVigencia() {
		return dataFimVigencia;
	}
	
	/**
	 * Set: dataFimVigencia.
	 *
	 * @param dataFimVigencia the data fim vigencia
	 */
	public void setDataFimVigencia(String dataFimVigencia) {
		this.dataFimVigencia = dataFimVigencia;
	}
	
	/**
	 * Get: centroCusto.
	 *
	 * @return centroCusto
	 */
	public String getCentroCusto() {
		return centroCusto;
	}
	
	/**
	 * Set: centroCusto.
	 *
	 * @param centroCusto the centro custo
	 */
	public void setCentroCusto(String centroCusto) {
		this.centroCusto = centroCusto;
	}
	
	
	
	

}
