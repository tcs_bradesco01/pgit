/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.detalharhistconsultasaldod.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.math.BigDecimal;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class DetalharHistConsultaSaldoResponse.
 * 
 * @version $Revision$ $Date$
 */
public class DetalharHistConsultaSaldoResponse implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _codMensagem
     */
    private java.lang.String _codMensagem;

    /**
     * Field _mensagem
     */
    private java.lang.String _mensagem;

    /**
     * Field _dsBanco
     */
    private java.lang.String _dsBanco;

    /**
     * Field _dsAgencia
     */
    private java.lang.String _dsAgencia;

    /**
     * Field _dsTipoConta
     */
    private java.lang.String _dsTipoConta;

    /**
     * Field _nrArquivoRemessaPagamento
     */
    private long _nrArquivoRemessaPagamento = 0;

    /**
     * keeps track of state for field: _nrArquivoRemessaPagamento
     */
    private boolean _has_nrArquivoRemessaPagamento;

    /**
     * Field _cdTipoCanal
     */
    private int _cdTipoCanal = 0;

    /**
     * keeps track of state for field: _cdTipoCanal
     */
    private boolean _has_cdTipoCanal;

    /**
     * Field _cdPessoaLoteRemessa
     */
    private long _cdPessoaLoteRemessa = 0;

    /**
     * keeps track of state for field: _cdPessoaLoteRemessa
     */
    private boolean _has_cdPessoaLoteRemessa;

    /**
     * Field _cdListaDebitoPagamento
     */
    private long _cdListaDebitoPagamento = 0;

    /**
     * keeps track of state for field: _cdListaDebitoPagamento
     */
    private boolean _has_cdListaDebitoPagamento;

    /**
     * Field _cdCanal
     */
    private int _cdCanal = 0;

    /**
     * keeps track of state for field: _cdCanal
     */
    private boolean _has_cdCanal;

    /**
     * Field _dsCanal
     */
    private java.lang.String _dsCanal;

    /**
     * Field _dtCreditoPagamento
     */
    private java.lang.String _dtCreditoPagamento;

    /**
     * Field _dtVencimento
     */
    private java.lang.String _dtVencimento;

    /**
     * Field _vlPagamento
     */
    private java.math.BigDecimal _vlPagamento = new java.math.BigDecimal("0");

    /**
     * Field _vlSaldoAnteriorLancamento
     */
    private java.math.BigDecimal _vlSaldoAnteriorLancamento = new java.math.BigDecimal("0");

    /**
     * Field _vlSaldoOperacional
     */
    private java.math.BigDecimal _vlSaldoOperacional = new java.math.BigDecimal("0");

    /**
     * Field _vlSaldoSemReserva
     */
    private java.math.BigDecimal _vlSaldoSemReserva = new java.math.BigDecimal("0");

    /**
     * Field _vlSaldoComReserva
     */
    private java.math.BigDecimal _vlSaldoComReserva = new java.math.BigDecimal("0");

    /**
     * Field _vlSaldoVinculadoJudicial
     */
    private java.math.BigDecimal _vlSaldoVinculadoJudicial = new java.math.BigDecimal("0");

    /**
     * Field _vlSaldoVinculadoAdministrativo
     */
    private java.math.BigDecimal _vlSaldoVinculadoAdministrativo = new java.math.BigDecimal("0");

    /**
     * Field _vlSaldoVinculadoSeguranca
     */
    private java.math.BigDecimal _vlSaldoVinculadoSeguranca = new java.math.BigDecimal("0");

    /**
     * Field _vlSaldoVinculadoAdministrativoLp
     */
    private java.math.BigDecimal _vlSaldoVinculadoAdministrativoLp = new java.math.BigDecimal("0");

    /**
     * Field _vlSaldoAgregadoFundo
     */
    private java.math.BigDecimal _vlSaldoAgregadoFundo = new java.math.BigDecimal("0");

    /**
     * Field _vlSaldoAgregadoCdb
     */
    private java.math.BigDecimal _vlSaldoAgregadoCdb = new java.math.BigDecimal("0");

    /**
     * Field _vlSaldoAgregadoPoupanca
     */
    private java.math.BigDecimal _vlSaldoAgregadoPoupanca = new java.math.BigDecimal("0");

    /**
     * Field _vlSaldoCreditoRotativo
     */
    private java.math.BigDecimal _vlSaldoCreditoRotativo = new java.math.BigDecimal("0");

    /**
     * Field _dtInclusao
     */
    private java.lang.String _dtInclusao;

    /**
     * Field _hrInclusao
     */
    private java.lang.String _hrInclusao;

    /**
     * Field _cdUsuarioInclusao
     */
    private java.lang.String _cdUsuarioInclusao;

    /**
     * Field _cdTipoCanalInclusao
     */
    private int _cdTipoCanalInclusao = 0;

    /**
     * keeps track of state for field: _cdTipoCanalInclusao
     */
    private boolean _has_cdTipoCanalInclusao;

    /**
     * Field _dsTipoCanalInclusao
     */
    private java.lang.String _dsTipoCanalInclusao;

    /**
     * Field _cdFluxoInclusao
     */
    private java.lang.String _cdFluxoInclusao;

    /**
     * Field _dtManutencao
     */
    private java.lang.String _dtManutencao;

    /**
     * Field _hrManutencao
     */
    private java.lang.String _hrManutencao;

    /**
     * Field _cdUsuarioManutencao
     */
    private java.lang.String _cdUsuarioManutencao;

    /**
     * Field _cdTipoCanalManutencao
     */
    private int _cdTipoCanalManutencao = 0;

    /**
     * keeps track of state for field: _cdTipoCanalManutencao
     */
    private boolean _has_cdTipoCanalManutencao;

    /**
     * Field _dsTipoCanalManutencao
     */
    private java.lang.String _dsTipoCanalManutencao;

    /**
     * Field _cdFluxoManutencao
     */
    private java.lang.String _cdFluxoManutencao;


      //----------------/
     //- Constructors -/
    //----------------/

    public DetalharHistConsultaSaldoResponse() 
     {
        super();
        setVlPagamento(new java.math.BigDecimal("0"));
        setVlSaldoAnteriorLancamento(new java.math.BigDecimal("0"));
        setVlSaldoOperacional(new java.math.BigDecimal("0"));
        setVlSaldoSemReserva(new java.math.BigDecimal("0"));
        setVlSaldoComReserva(new java.math.BigDecimal("0"));
        setVlSaldoVinculadoJudicial(new java.math.BigDecimal("0"));
        setVlSaldoVinculadoAdministrativo(new java.math.BigDecimal("0"));
        setVlSaldoVinculadoSeguranca(new java.math.BigDecimal("0"));
        setVlSaldoVinculadoAdministrativoLp(new java.math.BigDecimal("0"));
        setVlSaldoAgregadoFundo(new java.math.BigDecimal("0"));
        setVlSaldoAgregadoCdb(new java.math.BigDecimal("0"));
        setVlSaldoAgregadoPoupanca(new java.math.BigDecimal("0"));
        setVlSaldoCreditoRotativo(new java.math.BigDecimal("0"));
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharhistconsultasaldod.response.DetalharHistConsultaSaldoResponse()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdCanal
     * 
     */
    public void deleteCdCanal()
    {
        this._has_cdCanal= false;
    } //-- void deleteCdCanal() 

    /**
     * Method deleteCdListaDebitoPagamento
     * 
     */
    public void deleteCdListaDebitoPagamento()
    {
        this._has_cdListaDebitoPagamento= false;
    } //-- void deleteCdListaDebitoPagamento() 

    /**
     * Method deleteCdPessoaLoteRemessa
     * 
     */
    public void deleteCdPessoaLoteRemessa()
    {
        this._has_cdPessoaLoteRemessa= false;
    } //-- void deleteCdPessoaLoteRemessa() 

    /**
     * Method deleteCdTipoCanal
     * 
     */
    public void deleteCdTipoCanal()
    {
        this._has_cdTipoCanal= false;
    } //-- void deleteCdTipoCanal() 

    /**
     * Method deleteCdTipoCanalInclusao
     * 
     */
    public void deleteCdTipoCanalInclusao()
    {
        this._has_cdTipoCanalInclusao= false;
    } //-- void deleteCdTipoCanalInclusao() 

    /**
     * Method deleteCdTipoCanalManutencao
     * 
     */
    public void deleteCdTipoCanalManutencao()
    {
        this._has_cdTipoCanalManutencao= false;
    } //-- void deleteCdTipoCanalManutencao() 

    /**
     * Method deleteNrArquivoRemessaPagamento
     * 
     */
    public void deleteNrArquivoRemessaPagamento()
    {
        this._has_nrArquivoRemessaPagamento= false;
    } //-- void deleteNrArquivoRemessaPagamento() 

    /**
     * Returns the value of field 'cdCanal'.
     * 
     * @return int
     * @return the value of field 'cdCanal'.
     */
    public int getCdCanal()
    {
        return this._cdCanal;
    } //-- int getCdCanal() 

    /**
     * Returns the value of field 'cdFluxoInclusao'.
     * 
     * @return String
     * @return the value of field 'cdFluxoInclusao'.
     */
    public java.lang.String getCdFluxoInclusao()
    {
        return this._cdFluxoInclusao;
    } //-- java.lang.String getCdFluxoInclusao() 

    /**
     * Returns the value of field 'cdFluxoManutencao'.
     * 
     * @return String
     * @return the value of field 'cdFluxoManutencao'.
     */
    public java.lang.String getCdFluxoManutencao()
    {
        return this._cdFluxoManutencao;
    } //-- java.lang.String getCdFluxoManutencao() 

    /**
     * Returns the value of field 'cdListaDebitoPagamento'.
     * 
     * @return long
     * @return the value of field 'cdListaDebitoPagamento'.
     */
    public long getCdListaDebitoPagamento()
    {
        return this._cdListaDebitoPagamento;
    } //-- long getCdListaDebitoPagamento() 

    /**
     * Returns the value of field 'cdPessoaLoteRemessa'.
     * 
     * @return long
     * @return the value of field 'cdPessoaLoteRemessa'.
     */
    public long getCdPessoaLoteRemessa()
    {
        return this._cdPessoaLoteRemessa;
    } //-- long getCdPessoaLoteRemessa() 

    /**
     * Returns the value of field 'cdTipoCanal'.
     * 
     * @return int
     * @return the value of field 'cdTipoCanal'.
     */
    public int getCdTipoCanal()
    {
        return this._cdTipoCanal;
    } //-- int getCdTipoCanal() 

    /**
     * Returns the value of field 'cdTipoCanalInclusao'.
     * 
     * @return int
     * @return the value of field 'cdTipoCanalInclusao'.
     */
    public int getCdTipoCanalInclusao()
    {
        return this._cdTipoCanalInclusao;
    } //-- int getCdTipoCanalInclusao() 

    /**
     * Returns the value of field 'cdTipoCanalManutencao'.
     * 
     * @return int
     * @return the value of field 'cdTipoCanalManutencao'.
     */
    public int getCdTipoCanalManutencao()
    {
        return this._cdTipoCanalManutencao;
    } //-- int getCdTipoCanalManutencao() 

    /**
     * Returns the value of field 'cdUsuarioInclusao'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioInclusao'.
     */
    public java.lang.String getCdUsuarioInclusao()
    {
        return this._cdUsuarioInclusao;
    } //-- java.lang.String getCdUsuarioInclusao() 

    /**
     * Returns the value of field 'cdUsuarioManutencao'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioManutencao'.
     */
    public java.lang.String getCdUsuarioManutencao()
    {
        return this._cdUsuarioManutencao;
    } //-- java.lang.String getCdUsuarioManutencao() 

    /**
     * Returns the value of field 'codMensagem'.
     * 
     * @return String
     * @return the value of field 'codMensagem'.
     */
    public java.lang.String getCodMensagem()
    {
        return this._codMensagem;
    } //-- java.lang.String getCodMensagem() 

    /**
     * Returns the value of field 'dsAgencia'.
     * 
     * @return String
     * @return the value of field 'dsAgencia'.
     */
    public java.lang.String getDsAgencia()
    {
        return this._dsAgencia;
    } //-- java.lang.String getDsAgencia() 

    /**
     * Returns the value of field 'dsBanco'.
     * 
     * @return String
     * @return the value of field 'dsBanco'.
     */
    public java.lang.String getDsBanco()
    {
        return this._dsBanco;
    } //-- java.lang.String getDsBanco() 

    /**
     * Returns the value of field 'dsCanal'.
     * 
     * @return String
     * @return the value of field 'dsCanal'.
     */
    public java.lang.String getDsCanal()
    {
        return this._dsCanal;
    } //-- java.lang.String getDsCanal() 

    /**
     * Returns the value of field 'dsTipoCanalInclusao'.
     * 
     * @return String
     * @return the value of field 'dsTipoCanalInclusao'.
     */
    public java.lang.String getDsTipoCanalInclusao()
    {
        return this._dsTipoCanalInclusao;
    } //-- java.lang.String getDsTipoCanalInclusao() 

    /**
     * Returns the value of field 'dsTipoCanalManutencao'.
     * 
     * @return String
     * @return the value of field 'dsTipoCanalManutencao'.
     */
    public java.lang.String getDsTipoCanalManutencao()
    {
        return this._dsTipoCanalManutencao;
    } //-- java.lang.String getDsTipoCanalManutencao() 

    /**
     * Returns the value of field 'dsTipoConta'.
     * 
     * @return String
     * @return the value of field 'dsTipoConta'.
     */
    public java.lang.String getDsTipoConta()
    {
        return this._dsTipoConta;
    } //-- java.lang.String getDsTipoConta() 

    /**
     * Returns the value of field 'dtCreditoPagamento'.
     * 
     * @return String
     * @return the value of field 'dtCreditoPagamento'.
     */
    public java.lang.String getDtCreditoPagamento()
    {
        return this._dtCreditoPagamento;
    } //-- java.lang.String getDtCreditoPagamento() 

    /**
     * Returns the value of field 'dtInclusao'.
     * 
     * @return String
     * @return the value of field 'dtInclusao'.
     */
    public java.lang.String getDtInclusao()
    {
        return this._dtInclusao;
    } //-- java.lang.String getDtInclusao() 

    /**
     * Returns the value of field 'dtManutencao'.
     * 
     * @return String
     * @return the value of field 'dtManutencao'.
     */
    public java.lang.String getDtManutencao()
    {
        return this._dtManutencao;
    } //-- java.lang.String getDtManutencao() 

    /**
     * Returns the value of field 'dtVencimento'.
     * 
     * @return String
     * @return the value of field 'dtVencimento'.
     */
    public java.lang.String getDtVencimento()
    {
        return this._dtVencimento;
    } //-- java.lang.String getDtVencimento() 

    /**
     * Returns the value of field 'hrInclusao'.
     * 
     * @return String
     * @return the value of field 'hrInclusao'.
     */
    public java.lang.String getHrInclusao()
    {
        return this._hrInclusao;
    } //-- java.lang.String getHrInclusao() 

    /**
     * Returns the value of field 'hrManutencao'.
     * 
     * @return String
     * @return the value of field 'hrManutencao'.
     */
    public java.lang.String getHrManutencao()
    {
        return this._hrManutencao;
    } //-- java.lang.String getHrManutencao() 

    /**
     * Returns the value of field 'mensagem'.
     * 
     * @return String
     * @return the value of field 'mensagem'.
     */
    public java.lang.String getMensagem()
    {
        return this._mensagem;
    } //-- java.lang.String getMensagem() 

    /**
     * Returns the value of field 'nrArquivoRemessaPagamento'.
     * 
     * @return long
     * @return the value of field 'nrArquivoRemessaPagamento'.
     */
    public long getNrArquivoRemessaPagamento()
    {
        return this._nrArquivoRemessaPagamento;
    } //-- long getNrArquivoRemessaPagamento() 

    /**
     * Returns the value of field 'vlPagamento'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlPagamento'.
     */
    public java.math.BigDecimal getVlPagamento()
    {
        return this._vlPagamento;
    } //-- java.math.BigDecimal getVlPagamento() 

    /**
     * Returns the value of field 'vlSaldoAgregadoCdb'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlSaldoAgregadoCdb'.
     */
    public java.math.BigDecimal getVlSaldoAgregadoCdb()
    {
        return this._vlSaldoAgregadoCdb;
    } //-- java.math.BigDecimal getVlSaldoAgregadoCdb() 

    /**
     * Returns the value of field 'vlSaldoAgregadoFundo'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlSaldoAgregadoFundo'.
     */
    public java.math.BigDecimal getVlSaldoAgregadoFundo()
    {
        return this._vlSaldoAgregadoFundo;
    } //-- java.math.BigDecimal getVlSaldoAgregadoFundo() 

    /**
     * Returns the value of field 'vlSaldoAgregadoPoupanca'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlSaldoAgregadoPoupanca'.
     */
    public java.math.BigDecimal getVlSaldoAgregadoPoupanca()
    {
        return this._vlSaldoAgregadoPoupanca;
    } //-- java.math.BigDecimal getVlSaldoAgregadoPoupanca() 

    /**
     * Returns the value of field 'vlSaldoAnteriorLancamento'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlSaldoAnteriorLancamento'.
     */
    public java.math.BigDecimal getVlSaldoAnteriorLancamento()
    {
        return this._vlSaldoAnteriorLancamento;
    } //-- java.math.BigDecimal getVlSaldoAnteriorLancamento() 

    /**
     * Returns the value of field 'vlSaldoComReserva'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlSaldoComReserva'.
     */
    public java.math.BigDecimal getVlSaldoComReserva()
    {
        return this._vlSaldoComReserva;
    } //-- java.math.BigDecimal getVlSaldoComReserva() 

    /**
     * Returns the value of field 'vlSaldoCreditoRotativo'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlSaldoCreditoRotativo'.
     */
    public java.math.BigDecimal getVlSaldoCreditoRotativo()
    {
        return this._vlSaldoCreditoRotativo;
    } //-- java.math.BigDecimal getVlSaldoCreditoRotativo() 

    /**
     * Returns the value of field 'vlSaldoOperacional'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlSaldoOperacional'.
     */
    public java.math.BigDecimal getVlSaldoOperacional()
    {
        return this._vlSaldoOperacional;
    } //-- java.math.BigDecimal getVlSaldoOperacional() 

    /**
     * Returns the value of field 'vlSaldoSemReserva'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlSaldoSemReserva'.
     */
    public java.math.BigDecimal getVlSaldoSemReserva()
    {
        return this._vlSaldoSemReserva;
    } //-- java.math.BigDecimal getVlSaldoSemReserva() 

    /**
     * Returns the value of field 'vlSaldoVinculadoAdministrativo'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlSaldoVinculadoAdministrativo'.
     */
    public java.math.BigDecimal getVlSaldoVinculadoAdministrativo()
    {
        return this._vlSaldoVinculadoAdministrativo;
    } //-- java.math.BigDecimal getVlSaldoVinculadoAdministrativo() 

    /**
     * Returns the value of field
     * 'vlSaldoVinculadoAdministrativoLp'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlSaldoVinculadoAdministrativoLp'
     */
    public java.math.BigDecimal getVlSaldoVinculadoAdministrativoLp()
    {
        return this._vlSaldoVinculadoAdministrativoLp;
    } //-- java.math.BigDecimal getVlSaldoVinculadoAdministrativoLp() 

    /**
     * Returns the value of field 'vlSaldoVinculadoJudicial'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlSaldoVinculadoJudicial'.
     */
    public java.math.BigDecimal getVlSaldoVinculadoJudicial()
    {
        return this._vlSaldoVinculadoJudicial;
    } //-- java.math.BigDecimal getVlSaldoVinculadoJudicial() 

    /**
     * Returns the value of field 'vlSaldoVinculadoSeguranca'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlSaldoVinculadoSeguranca'.
     */
    public java.math.BigDecimal getVlSaldoVinculadoSeguranca()
    {
        return this._vlSaldoVinculadoSeguranca;
    } //-- java.math.BigDecimal getVlSaldoVinculadoSeguranca() 

    /**
     * Method hasCdCanal
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCanal()
    {
        return this._has_cdCanal;
    } //-- boolean hasCdCanal() 

    /**
     * Method hasCdListaDebitoPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdListaDebitoPagamento()
    {
        return this._has_cdListaDebitoPagamento;
    } //-- boolean hasCdListaDebitoPagamento() 

    /**
     * Method hasCdPessoaLoteRemessa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaLoteRemessa()
    {
        return this._has_cdPessoaLoteRemessa;
    } //-- boolean hasCdPessoaLoteRemessa() 

    /**
     * Method hasCdTipoCanal
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoCanal()
    {
        return this._has_cdTipoCanal;
    } //-- boolean hasCdTipoCanal() 

    /**
     * Method hasCdTipoCanalInclusao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoCanalInclusao()
    {
        return this._has_cdTipoCanalInclusao;
    } //-- boolean hasCdTipoCanalInclusao() 

    /**
     * Method hasCdTipoCanalManutencao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoCanalManutencao()
    {
        return this._has_cdTipoCanalManutencao;
    } //-- boolean hasCdTipoCanalManutencao() 

    /**
     * Method hasNrArquivoRemessaPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrArquivoRemessaPagamento()
    {
        return this._has_nrArquivoRemessaPagamento;
    } //-- boolean hasNrArquivoRemessaPagamento() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdCanal'.
     * 
     * @param cdCanal the value of field 'cdCanal'.
     */
    public void setCdCanal(int cdCanal)
    {
        this._cdCanal = cdCanal;
        this._has_cdCanal = true;
    } //-- void setCdCanal(int) 

    /**
     * Sets the value of field 'cdFluxoInclusao'.
     * 
     * @param cdFluxoInclusao the value of field 'cdFluxoInclusao'.
     */
    public void setCdFluxoInclusao(java.lang.String cdFluxoInclusao)
    {
        this._cdFluxoInclusao = cdFluxoInclusao;
    } //-- void setCdFluxoInclusao(java.lang.String) 

    /**
     * Sets the value of field 'cdFluxoManutencao'.
     * 
     * @param cdFluxoManutencao the value of field
     * 'cdFluxoManutencao'.
     */
    public void setCdFluxoManutencao(java.lang.String cdFluxoManutencao)
    {
        this._cdFluxoManutencao = cdFluxoManutencao;
    } //-- void setCdFluxoManutencao(java.lang.String) 

    /**
     * Sets the value of field 'cdListaDebitoPagamento'.
     * 
     * @param cdListaDebitoPagamento the value of field
     * 'cdListaDebitoPagamento'.
     */
    public void setCdListaDebitoPagamento(long cdListaDebitoPagamento)
    {
        this._cdListaDebitoPagamento = cdListaDebitoPagamento;
        this._has_cdListaDebitoPagamento = true;
    } //-- void setCdListaDebitoPagamento(long) 

    /**
     * Sets the value of field 'cdPessoaLoteRemessa'.
     * 
     * @param cdPessoaLoteRemessa the value of field
     * 'cdPessoaLoteRemessa'.
     */
    public void setCdPessoaLoteRemessa(long cdPessoaLoteRemessa)
    {
        this._cdPessoaLoteRemessa = cdPessoaLoteRemessa;
        this._has_cdPessoaLoteRemessa = true;
    } //-- void setCdPessoaLoteRemessa(long) 

    /**
     * Sets the value of field 'cdTipoCanal'.
     * 
     * @param cdTipoCanal the value of field 'cdTipoCanal'.
     */
    public void setCdTipoCanal(int cdTipoCanal)
    {
        this._cdTipoCanal = cdTipoCanal;
        this._has_cdTipoCanal = true;
    } //-- void setCdTipoCanal(int) 

    /**
     * Sets the value of field 'cdTipoCanalInclusao'.
     * 
     * @param cdTipoCanalInclusao the value of field
     * 'cdTipoCanalInclusao'.
     */
    public void setCdTipoCanalInclusao(int cdTipoCanalInclusao)
    {
        this._cdTipoCanalInclusao = cdTipoCanalInclusao;
        this._has_cdTipoCanalInclusao = true;
    } //-- void setCdTipoCanalInclusao(int) 

    /**
     * Sets the value of field 'cdTipoCanalManutencao'.
     * 
     * @param cdTipoCanalManutencao the value of field
     * 'cdTipoCanalManutencao'.
     */
    public void setCdTipoCanalManutencao(int cdTipoCanalManutencao)
    {
        this._cdTipoCanalManutencao = cdTipoCanalManutencao;
        this._has_cdTipoCanalManutencao = true;
    } //-- void setCdTipoCanalManutencao(int) 

    /**
     * Sets the value of field 'cdUsuarioInclusao'.
     * 
     * @param cdUsuarioInclusao the value of field
     * 'cdUsuarioInclusao'.
     */
    public void setCdUsuarioInclusao(java.lang.String cdUsuarioInclusao)
    {
        this._cdUsuarioInclusao = cdUsuarioInclusao;
    } //-- void setCdUsuarioInclusao(java.lang.String) 

    /**
     * Sets the value of field 'cdUsuarioManutencao'.
     * 
     * @param cdUsuarioManutencao the value of field
     * 'cdUsuarioManutencao'.
     */
    public void setCdUsuarioManutencao(java.lang.String cdUsuarioManutencao)
    {
        this._cdUsuarioManutencao = cdUsuarioManutencao;
    } //-- void setCdUsuarioManutencao(java.lang.String) 

    /**
     * Sets the value of field 'codMensagem'.
     * 
     * @param codMensagem the value of field 'codMensagem'.
     */
    public void setCodMensagem(java.lang.String codMensagem)
    {
        this._codMensagem = codMensagem;
    } //-- void setCodMensagem(java.lang.String) 

    /**
     * Sets the value of field 'dsAgencia'.
     * 
     * @param dsAgencia the value of field 'dsAgencia'.
     */
    public void setDsAgencia(java.lang.String dsAgencia)
    {
        this._dsAgencia = dsAgencia;
    } //-- void setDsAgencia(java.lang.String) 

    /**
     * Sets the value of field 'dsBanco'.
     * 
     * @param dsBanco the value of field 'dsBanco'.
     */
    public void setDsBanco(java.lang.String dsBanco)
    {
        this._dsBanco = dsBanco;
    } //-- void setDsBanco(java.lang.String) 

    /**
     * Sets the value of field 'dsCanal'.
     * 
     * @param dsCanal the value of field 'dsCanal'.
     */
    public void setDsCanal(java.lang.String dsCanal)
    {
        this._dsCanal = dsCanal;
    } //-- void setDsCanal(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoCanalInclusao'.
     * 
     * @param dsTipoCanalInclusao the value of field
     * 'dsTipoCanalInclusao'.
     */
    public void setDsTipoCanalInclusao(java.lang.String dsTipoCanalInclusao)
    {
        this._dsTipoCanalInclusao = dsTipoCanalInclusao;
    } //-- void setDsTipoCanalInclusao(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoCanalManutencao'.
     * 
     * @param dsTipoCanalManutencao the value of field
     * 'dsTipoCanalManutencao'.
     */
    public void setDsTipoCanalManutencao(java.lang.String dsTipoCanalManutencao)
    {
        this._dsTipoCanalManutencao = dsTipoCanalManutencao;
    } //-- void setDsTipoCanalManutencao(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoConta'.
     * 
     * @param dsTipoConta the value of field 'dsTipoConta'.
     */
    public void setDsTipoConta(java.lang.String dsTipoConta)
    {
        this._dsTipoConta = dsTipoConta;
    } //-- void setDsTipoConta(java.lang.String) 

    /**
     * Sets the value of field 'dtCreditoPagamento'.
     * 
     * @param dtCreditoPagamento the value of field
     * 'dtCreditoPagamento'.
     */
    public void setDtCreditoPagamento(java.lang.String dtCreditoPagamento)
    {
        this._dtCreditoPagamento = dtCreditoPagamento;
    } //-- void setDtCreditoPagamento(java.lang.String) 

    /**
     * Sets the value of field 'dtInclusao'.
     * 
     * @param dtInclusao the value of field 'dtInclusao'.
     */
    public void setDtInclusao(java.lang.String dtInclusao)
    {
        this._dtInclusao = dtInclusao;
    } //-- void setDtInclusao(java.lang.String) 

    /**
     * Sets the value of field 'dtManutencao'.
     * 
     * @param dtManutencao the value of field 'dtManutencao'.
     */
    public void setDtManutencao(java.lang.String dtManutencao)
    {
        this._dtManutencao = dtManutencao;
    } //-- void setDtManutencao(java.lang.String) 

    /**
     * Sets the value of field 'dtVencimento'.
     * 
     * @param dtVencimento the value of field 'dtVencimento'.
     */
    public void setDtVencimento(java.lang.String dtVencimento)
    {
        this._dtVencimento = dtVencimento;
    } //-- void setDtVencimento(java.lang.String) 

    /**
     * Sets the value of field 'hrInclusao'.
     * 
     * @param hrInclusao the value of field 'hrInclusao'.
     */
    public void setHrInclusao(java.lang.String hrInclusao)
    {
        this._hrInclusao = hrInclusao;
    } //-- void setHrInclusao(java.lang.String) 

    /**
     * Sets the value of field 'hrManutencao'.
     * 
     * @param hrManutencao the value of field 'hrManutencao'.
     */
    public void setHrManutencao(java.lang.String hrManutencao)
    {
        this._hrManutencao = hrManutencao;
    } //-- void setHrManutencao(java.lang.String) 

    /**
     * Sets the value of field 'mensagem'.
     * 
     * @param mensagem the value of field 'mensagem'.
     */
    public void setMensagem(java.lang.String mensagem)
    {
        this._mensagem = mensagem;
    } //-- void setMensagem(java.lang.String) 

    /**
     * Sets the value of field 'nrArquivoRemessaPagamento'.
     * 
     * @param nrArquivoRemessaPagamento the value of field
     * 'nrArquivoRemessaPagamento'.
     */
    public void setNrArquivoRemessaPagamento(long nrArquivoRemessaPagamento)
    {
        this._nrArquivoRemessaPagamento = nrArquivoRemessaPagamento;
        this._has_nrArquivoRemessaPagamento = true;
    } //-- void setNrArquivoRemessaPagamento(long) 

    /**
     * Sets the value of field 'vlPagamento'.
     * 
     * @param vlPagamento the value of field 'vlPagamento'.
     */
    public void setVlPagamento(java.math.BigDecimal vlPagamento)
    {
        this._vlPagamento = vlPagamento;
    } //-- void setVlPagamento(java.math.BigDecimal) 

    /**
     * Sets the value of field 'vlSaldoAgregadoCdb'.
     * 
     * @param vlSaldoAgregadoCdb the value of field
     * 'vlSaldoAgregadoCdb'.
     */
    public void setVlSaldoAgregadoCdb(java.math.BigDecimal vlSaldoAgregadoCdb)
    {
        this._vlSaldoAgregadoCdb = vlSaldoAgregadoCdb;
    } //-- void setVlSaldoAgregadoCdb(java.math.BigDecimal) 

    /**
     * Sets the value of field 'vlSaldoAgregadoFundo'.
     * 
     * @param vlSaldoAgregadoFundo the value of field
     * 'vlSaldoAgregadoFundo'.
     */
    public void setVlSaldoAgregadoFundo(java.math.BigDecimal vlSaldoAgregadoFundo)
    {
        this._vlSaldoAgregadoFundo = vlSaldoAgregadoFundo;
    } //-- void setVlSaldoAgregadoFundo(java.math.BigDecimal) 

    /**
     * Sets the value of field 'vlSaldoAgregadoPoupanca'.
     * 
     * @param vlSaldoAgregadoPoupanca the value of field
     * 'vlSaldoAgregadoPoupanca'.
     */
    public void setVlSaldoAgregadoPoupanca(java.math.BigDecimal vlSaldoAgregadoPoupanca)
    {
        this._vlSaldoAgregadoPoupanca = vlSaldoAgregadoPoupanca;
    } //-- void setVlSaldoAgregadoPoupanca(java.math.BigDecimal) 

    /**
     * Sets the value of field 'vlSaldoAnteriorLancamento'.
     * 
     * @param vlSaldoAnteriorLancamento the value of field
     * 'vlSaldoAnteriorLancamento'.
     */
    public void setVlSaldoAnteriorLancamento(java.math.BigDecimal vlSaldoAnteriorLancamento)
    {
        this._vlSaldoAnteriorLancamento = vlSaldoAnteriorLancamento;
    } //-- void setVlSaldoAnteriorLancamento(java.math.BigDecimal) 

    /**
     * Sets the value of field 'vlSaldoComReserva'.
     * 
     * @param vlSaldoComReserva the value of field
     * 'vlSaldoComReserva'.
     */
    public void setVlSaldoComReserva(java.math.BigDecimal vlSaldoComReserva)
    {
        this._vlSaldoComReserva = vlSaldoComReserva;
    } //-- void setVlSaldoComReserva(java.math.BigDecimal) 

    /**
     * Sets the value of field 'vlSaldoCreditoRotativo'.
     * 
     * @param vlSaldoCreditoRotativo the value of field
     * 'vlSaldoCreditoRotativo'.
     */
    public void setVlSaldoCreditoRotativo(java.math.BigDecimal vlSaldoCreditoRotativo)
    {
        this._vlSaldoCreditoRotativo = vlSaldoCreditoRotativo;
    } //-- void setVlSaldoCreditoRotativo(java.math.BigDecimal) 

    /**
     * Sets the value of field 'vlSaldoOperacional'.
     * 
     * @param vlSaldoOperacional the value of field
     * 'vlSaldoOperacional'.
     */
    public void setVlSaldoOperacional(java.math.BigDecimal vlSaldoOperacional)
    {
        this._vlSaldoOperacional = vlSaldoOperacional;
    } //-- void setVlSaldoOperacional(java.math.BigDecimal) 

    /**
     * Sets the value of field 'vlSaldoSemReserva'.
     * 
     * @param vlSaldoSemReserva the value of field
     * 'vlSaldoSemReserva'.
     */
    public void setVlSaldoSemReserva(java.math.BigDecimal vlSaldoSemReserva)
    {
        this._vlSaldoSemReserva = vlSaldoSemReserva;
    } //-- void setVlSaldoSemReserva(java.math.BigDecimal) 

    /**
     * Sets the value of field 'vlSaldoVinculadoAdministrativo'.
     * 
     * @param vlSaldoVinculadoAdministrativo the value of field
     * 'vlSaldoVinculadoAdministrativo'.
     */
    public void setVlSaldoVinculadoAdministrativo(java.math.BigDecimal vlSaldoVinculadoAdministrativo)
    {
        this._vlSaldoVinculadoAdministrativo = vlSaldoVinculadoAdministrativo;
    } //-- void setVlSaldoVinculadoAdministrativo(java.math.BigDecimal) 

    /**
     * Sets the value of field 'vlSaldoVinculadoAdministrativoLp'.
     * 
     * @param vlSaldoVinculadoAdministrativoLp the value of field
     * 'vlSaldoVinculadoAdministrativoLp'.
     */
    public void setVlSaldoVinculadoAdministrativoLp(java.math.BigDecimal vlSaldoVinculadoAdministrativoLp)
    {
        this._vlSaldoVinculadoAdministrativoLp = vlSaldoVinculadoAdministrativoLp;
    } //-- void setVlSaldoVinculadoAdministrativoLp(java.math.BigDecimal) 

    /**
     * Sets the value of field 'vlSaldoVinculadoJudicial'.
     * 
     * @param vlSaldoVinculadoJudicial the value of field
     * 'vlSaldoVinculadoJudicial'.
     */
    public void setVlSaldoVinculadoJudicial(java.math.BigDecimal vlSaldoVinculadoJudicial)
    {
        this._vlSaldoVinculadoJudicial = vlSaldoVinculadoJudicial;
    } //-- void setVlSaldoVinculadoJudicial(java.math.BigDecimal) 

    /**
     * Sets the value of field 'vlSaldoVinculadoSeguranca'.
     * 
     * @param vlSaldoVinculadoSeguranca the value of field
     * 'vlSaldoVinculadoSeguranca'.
     */
    public void setVlSaldoVinculadoSeguranca(java.math.BigDecimal vlSaldoVinculadoSeguranca)
    {
        this._vlSaldoVinculadoSeguranca = vlSaldoVinculadoSeguranca;
    } //-- void setVlSaldoVinculadoSeguranca(java.math.BigDecimal) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return DetalharHistConsultaSaldoResponse
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.detalharhistconsultasaldod.response.DetalharHistConsultaSaldoResponse unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.detalharhistconsultasaldod.response.DetalharHistConsultaSaldoResponse) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.detalharhistconsultasaldod.response.DetalharHistConsultaSaldoResponse.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharhistconsultasaldod.response.DetalharHistConsultaSaldoResponse unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
