/*
 * Nome: br.com.bradesco.web.pgit.service.business.tipocomprovante.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.tipocomprovante.bean;

/**
 * Nome: ConsultarDescricaoMensagemEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarDescricaoMensagemEntradaDTO {
	
	/** Atributo cdSistema. */
	private String cdSistema;
	
	/** Atributo nrEventoMensagemNegocio. */
	private Integer nrEventoMensagemNegocio;
	
	/** Atributo cdRecursoGeradorMensagem. */
	private Integer cdRecursoGeradorMensagem;
	
	/** Atributo cdIdiomaTextoMensagem. */
	private Integer cdIdiomaTextoMensagem;
	
	/** Atributo ordemLinhaMensagem. */
	private Long ordemLinhaMensagem;

	/**
	 * Get: cdIdiomaTextoMensagem.
	 *
	 * @return cdIdiomaTextoMensagem
	 */
	public Integer getCdIdiomaTextoMensagem() {
		return cdIdiomaTextoMensagem;
	}
	
	/**
	 * Set: cdIdiomaTextoMensagem.
	 *
	 * @param cdIdiomaTextoMensagem the cd idioma texto mensagem
	 */
	public void setCdIdiomaTextoMensagem(Integer cdIdiomaTextoMensagem) {
		this.cdIdiomaTextoMensagem = cdIdiomaTextoMensagem;
	}
	
	/**
	 * Get: cdRecursoGeradorMensagem.
	 *
	 * @return cdRecursoGeradorMensagem
	 */
	public Integer getCdRecursoGeradorMensagem() {
		return cdRecursoGeradorMensagem;
	}
	
	/**
	 * Set: cdRecursoGeradorMensagem.
	 *
	 * @param cdRecursoGeradorMensagem the cd recurso gerador mensagem
	 */
	public void setCdRecursoGeradorMensagem(Integer cdRecursoGeradorMensagem) {
		this.cdRecursoGeradorMensagem = cdRecursoGeradorMensagem;
	}
	
	/**
	 * Get: cdSistema.
	 *
	 * @return cdSistema
	 */
	public String getCdSistema() {
		return cdSistema;
	}
	
	/**
	 * Set: cdSistema.
	 *
	 * @param cdSistema the cd sistema
	 */
	public void setCdSistema(String cdSistema) {
		this.cdSistema = cdSistema;
	}
	
	/**
	 * Get: nrEventoMensagemNegocio.
	 *
	 * @return nrEventoMensagemNegocio
	 */
	public Integer getNrEventoMensagemNegocio() {
		return nrEventoMensagemNegocio;
	}
	
	/**
	 * Set: nrEventoMensagemNegocio.
	 *
	 * @param nrEventoMensagemNegocio the nr evento mensagem negocio
	 */
	public void setNrEventoMensagemNegocio(Integer nrEventoMensagemNegocio) {
		this.nrEventoMensagemNegocio = nrEventoMensagemNegocio;
	}
	
	/**
	 * Get: ordemLinhaMensagem.
	 *
	 * @return ordemLinhaMensagem
	 */
	public Long getOrdemLinhaMensagem() {
		return ordemLinhaMensagem;
	}
	
	/**
	 * Set: ordemLinhaMensagem.
	 *
	 * @param ordemLinhaMensagem the ordem linha mensagem
	 */
	public void setOrdemLinhaMensagem(Long ordemLinhaMensagem) {
		this.ordemLinhaMensagem = ordemLinhaMensagem;
	}
}