/*
 * Nome: br.com.bradesco.web.pgit.service.business.formaliqpagtoint.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.formaliqpagtoint.bean;

/**
 * Nome: AlterarLiquidacaoPagamentoEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class AlterarLiquidacaoPagamentoEntradaDTO {

	/** Atributo cdFormaLiquidacao. */
	private Integer cdFormaLiquidacao;
	
	/** Atributo cdSistema. */
	private String cdSistema;
	
	/** Atributo cdPrioridadeDebForma. */
	private Integer cdPrioridadeDebForma;
	
	/** Atributo cdControleHoraLimite. */
	private Integer cdControleHoraLimite;
	
	/** Atributo qtMinutoMargemSeguranca. */
	private Integer qtMinutoMargemSeguranca;
	
	/** Atributo hrLimiteProcessamento. */
	private String hrLimiteProcessamento;
	
	/** Atributo hrConsultasSaldoPagamento. */
	private String hrConsultasSaldoPagamento;
	
	/** Atributo hrConsultaFolhaPgto. */
	private String hrConsultaFolhaPgto;
	
	/** Atributo qtMinutosValorSuperior. */
	private Integer qtMinutosValorSuperior;
	
	/** Atributo hrLimiteValorSuperior. */
	private String hrLimiteValorSuperior;

	/** Atributo qtdTempoAgendamentoCobranca. */
    private Integer qtdTempoAgendamentoCobranca = null;

    /** Atributo qtdTempoEfetivacaoCiclicaCobranca. */
    private Integer qtdTempoEfetivacaoCiclicaCobranca = null;

    /** Atributo qtdTempoEfetivacaoDiariaCobranca. */
    private Integer qtdTempoEfetivacaoDiariaCobranca = null;
	
    /** Atributo qtdLimiteProcessamentoExcedido. */
    private Integer qtdLimiteProcessamentoExcedido = null;
    
    /**
	 * Alterar liquidacao pagamento entrada dto.
	 */
	public AlterarLiquidacaoPagamentoEntradaDTO() {
		super();
	}

	/**
	 * Get: cdControleHoraLimite.
	 *
	 * @return cdControleHoraLimite
	 */
	public Integer getCdControleHoraLimite() {
		return cdControleHoraLimite;
	}

	/**
	 * Set: cdControleHoraLimite.
	 *
	 * @param cdControleHoraLimite the cd controle hora limite
	 */
	public void setCdControleHoraLimite(Integer cdControleHoraLimite) {
		this.cdControleHoraLimite = cdControleHoraLimite;
	}

	/**
	 * Get: cdFormaLiquidacao.
	 *
	 * @return cdFormaLiquidacao
	 */
	public Integer getCdFormaLiquidacao() {
		return cdFormaLiquidacao;
	}

	/**
	 * Set: cdFormaLiquidacao.
	 *
	 * @param cdFormaLiquidacao the cd forma liquidacao
	 */
	public void setCdFormaLiquidacao(Integer cdFormaLiquidacao) {
		this.cdFormaLiquidacao = cdFormaLiquidacao;
	}

	/**
	 * Get: cdPrioridadeDebForma.
	 *
	 * @return cdPrioridadeDebForma
	 */
	public Integer getCdPrioridadeDebForma() {
		return cdPrioridadeDebForma;
	}

	/**
	 * Set: cdPrioridadeDebForma.
	 *
	 * @param cdPrioridadeDebForma the cd prioridade deb forma
	 */
	public void setCdPrioridadeDebForma(Integer cdPrioridadeDebForma) {
		this.cdPrioridadeDebForma = cdPrioridadeDebForma;
	}

	/**
	 * Get: cdSistema.
	 *
	 * @return cdSistema
	 */
	public String getCdSistema() {
		return cdSistema;
	}

	/**
	 * Set: cdSistema.
	 *
	 * @param cdSistema the cd sistema
	 */
	public void setCdSistema(String cdSistema) {
		this.cdSistema = cdSistema;
	}

	/**
	 * Get: hrLimiteProcessamento.
	 *
	 * @return hrLimiteProcessamento
	 */
	public String getHrLimiteProcessamento() {
		return hrLimiteProcessamento;
	}

	/**
	 * Set: hrLimiteProcessamento.
	 *
	 * @param hrLimiteProcessamento the hr limite processamento
	 */
	public void setHrLimiteProcessamento(String hrLimiteProcessamento) {
		this.hrLimiteProcessamento = hrLimiteProcessamento;
	}

	/**
	 * Get: qtMinutoMargemSeguranca.
	 *
	 * @return qtMinutoMargemSeguranca
	 */
	public Integer getQtMinutoMargemSeguranca() {
		return qtMinutoMargemSeguranca;
	}

	/**
	 * Set: qtMinutoMargemSeguranca.
	 *
	 * @param qtMinutoMargemSeguranca the qt minuto margem seguranca
	 */
	public void setQtMinutoMargemSeguranca(Integer qtMinutoMargemSeguranca) {
		this.qtMinutoMargemSeguranca = qtMinutoMargemSeguranca;
	}

	/**
	 * Get: hrConsultasSaldoPagamento.
	 *
	 * @return hrConsultasSaldoPagamento
	 */
	public String getHrConsultasSaldoPagamento() {
		return hrConsultasSaldoPagamento;
	}

	/**
	 * Set: hrConsultasSaldoPagamento.
	 *
	 * @param hrConsultasSaldoPagamento the hr consultas saldo pagamento
	 */
	public void setHrConsultasSaldoPagamento(String hrConsultasSaldoPagamento) {
		this.hrConsultasSaldoPagamento = hrConsultasSaldoPagamento;
	}

	/**
	 * Get: hrConsultaFolhaPgto.
	 *
	 * @return hrConsultaFolhaPgto
	 */
	public String getHrConsultaFolhaPgto() {
		return hrConsultaFolhaPgto;
	}

	/**
	 * Set: hrConsultaFolhaPgto.
	 *
	 * @param hrConsultaFolhaPgto the hr consulta folha pgto
	 */
	public void setHrConsultaFolhaPgto(String hrConsultaFolhaPgto) {
		this.hrConsultaFolhaPgto = hrConsultaFolhaPgto;
	}

	/**
	 * Get: qtMinutosValorSuperior.
	 *
	 * @return qtMinutosValorSuperior
	 */
	public Integer getQtMinutosValorSuperior() {
		return qtMinutosValorSuperior;
	}

	/**
	 * Set: qtMinutosValorSuperior.
	 *
	 * @param qtMinutosValorSuperior the qt minutos valor superior
	 */
	public void setQtMinutosValorSuperior(Integer qtMinutosValorSuperior) {
		this.qtMinutosValorSuperior = qtMinutosValorSuperior;
	}

	/**
	 * Get: hrLimiteValorSuperior.
	 *
	 * @return hrLimiteValorSuperior
	 */
	public String getHrLimiteValorSuperior() {
		return hrLimiteValorSuperior;
	}

	/**
	 * Set: hrLimiteValorSuperior.
	 *
	 * @param hrLimiteValorSuperior the hr limite valor superior
	 */
	public void setHrLimiteValorSuperior(String hrLimiteValorSuperior) {
		this.hrLimiteValorSuperior = hrLimiteValorSuperior;
	}

    /**
     * Nome: getQtdTempoAgendamentoCobranca
     *
     * @return qtdTempoAgendamentoCobranca
     */
    public Integer getQtdTempoAgendamentoCobranca() {
        return qtdTempoAgendamentoCobranca;
    }

    /**
     * Nome: setQtdTempoAgendamentoCobranca
     *
     * @param qtdTempoAgendamentoCobranca
     */
    public void setQtdTempoAgendamentoCobranca(Integer qtdTempoAgendamentoCobranca) {
        this.qtdTempoAgendamentoCobranca = qtdTempoAgendamentoCobranca;
    }

    /**
     * Nome: getQtdTempoEfetivacaoCiclicaCobranca
     *
     * @return qtdTempoEfetivacaoCiclicaCobranca
     */
    public Integer getQtdTempoEfetivacaoCiclicaCobranca() {
        return qtdTempoEfetivacaoCiclicaCobranca;
    }

    /**
     * Nome: setQtdTempoEfetivacaoCiclicaCobranca
     *
     * @param qtdTempoEfetivacaoCiclicaCobranca
     */
    public void setQtdTempoEfetivacaoCiclicaCobranca(Integer qtdTempoEfetivacaoCiclicaCobranca) {
        this.qtdTempoEfetivacaoCiclicaCobranca = qtdTempoEfetivacaoCiclicaCobranca;
    }

    /**
     * Nome: getQtdTempoEfetivacaoDiariaCobranca
     *
     * @return qtdTempoEfetivacaoDiariaCobranca
     */
    public Integer getQtdTempoEfetivacaoDiariaCobranca() {
        return qtdTempoEfetivacaoDiariaCobranca;
    }

    /**
     * Nome: setQtdTempoEfetivacaoDiariaCobranca
     *
     * @param qtdTempoEfetivacaoDiariaCobranca
     */
    public void setQtdTempoEfetivacaoDiariaCobranca(Integer qtdTempoEfetivacaoDiariaCobranca) {
        this.qtdTempoEfetivacaoDiariaCobranca = qtdTempoEfetivacaoDiariaCobranca;
    }

	public Integer getQtdLimiteProcessamentoExcedido() {
		return qtdLimiteProcessamentoExcedido;
	}

	public void setQtdLimiteProcessamentoExcedido(
			Integer qtdLimiteProcessamentoExcedido) {
		this.qtdLimiteProcessamentoExcedido = qtdLimiteProcessamentoExcedido;
	}
    
    
}
