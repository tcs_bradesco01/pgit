/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdIndicadorModalidadeFornecedor
     */
    private int _cdIndicadorModalidadeFornecedor = 0;

    /**
     * keeps track of state for field:
     * _cdIndicadorModalidadeFornecedor
     */
    private boolean _has_cdIndicadorModalidadeFornecedor;

    /**
     * Field _cdIndicadorFornecedorContrato
     */
    private java.lang.String _cdIndicadorFornecedorContrato;

    /**
     * Field _cdProdutoRelacionadoFornecedor
     */
    private int _cdProdutoRelacionadoFornecedor = 0;

    /**
     * keeps track of state for field:
     * _cdProdutoRelacionadoFornecedor
     */
    private boolean _has_cdProdutoRelacionadoFornecedor;

    /**
     * Field _dsProdutoRelacionadoFornecedor
     */
    private java.lang.String _dsProdutoRelacionadoFornecedor;

    /**
     * Field _cdMomentoProcessamentoFornecedor
     */
    private int _cdMomentoProcessamentoFornecedor = 0;

    /**
     * keeps track of state for field:
     * _cdMomentoProcessamentoFornecedor
     */
    private boolean _has_cdMomentoProcessamentoFornecedor;

    /**
     * Field _dsMomentoProcessamentoFornecedor
     */
    private java.lang.String _dsMomentoProcessamentoFornecedor;

    /**
     * Field _cdPagamentoUtilFornecedor
     */
    private int _cdPagamentoUtilFornecedor = 0;

    /**
     * keeps track of state for field: _cdPagamentoUtilFornecedor
     */
    private boolean _has_cdPagamentoUtilFornecedor;

    /**
     * Field _dsPagamentoUtilFornecedor
     */
    private java.lang.String _dsPagamentoUtilFornecedor;

    /**
     * Field _nrQuantidadeDiasFloatFornecedor
     */
    private int _nrQuantidadeDiasFloatFornecedor = 0;

    /**
     * keeps track of state for field:
     * _nrQuantidadeDiasFloatFornecedor
     */
    private boolean _has_nrQuantidadeDiasFloatFornecedor;

    /**
     * Field _cdIndicadorFeriadoFornecedor
     */
    private int _cdIndicadorFeriadoFornecedor = 0;

    /**
     * keeps track of state for field: _cdIndicadorFeriadoFornecedor
     */
    private boolean _has_cdIndicadorFeriadoFornecedor;

    /**
     * Field _dsIndicadorFeriadoFornecedor
     */
    private java.lang.String _dsIndicadorFeriadoFornecedor;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdIndicadorFeriadoFornecedor
     * 
     */
    public void deleteCdIndicadorFeriadoFornecedor()
    {
        this._has_cdIndicadorFeriadoFornecedor= false;
    } //-- void deleteCdIndicadorFeriadoFornecedor() 

    /**
     * Method deleteCdIndicadorModalidadeFornecedor
     * 
     */
    public void deleteCdIndicadorModalidadeFornecedor()
    {
        this._has_cdIndicadorModalidadeFornecedor= false;
    } //-- void deleteCdIndicadorModalidadeFornecedor() 

    /**
     * Method deleteCdMomentoProcessamentoFornecedor
     * 
     */
    public void deleteCdMomentoProcessamentoFornecedor()
    {
        this._has_cdMomentoProcessamentoFornecedor= false;
    } //-- void deleteCdMomentoProcessamentoFornecedor() 

    /**
     * Method deleteCdPagamentoUtilFornecedor
     * 
     */
    public void deleteCdPagamentoUtilFornecedor()
    {
        this._has_cdPagamentoUtilFornecedor= false;
    } //-- void deleteCdPagamentoUtilFornecedor() 

    /**
     * Method deleteCdProdutoRelacionadoFornecedor
     * 
     */
    public void deleteCdProdutoRelacionadoFornecedor()
    {
        this._has_cdProdutoRelacionadoFornecedor= false;
    } //-- void deleteCdProdutoRelacionadoFornecedor() 

    /**
     * Method deleteNrQuantidadeDiasFloatFornecedor
     * 
     */
    public void deleteNrQuantidadeDiasFloatFornecedor()
    {
        this._has_nrQuantidadeDiasFloatFornecedor= false;
    } //-- void deleteNrQuantidadeDiasFloatFornecedor() 

    /**
     * Returns the value of field 'cdIndicadorFeriadoFornecedor'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorFeriadoFornecedor'.
     */
    public int getCdIndicadorFeriadoFornecedor()
    {
        return this._cdIndicadorFeriadoFornecedor;
    } //-- int getCdIndicadorFeriadoFornecedor() 

    /**
     * Returns the value of field 'cdIndicadorFornecedorContrato'.
     * 
     * @return String
     * @return the value of field 'cdIndicadorFornecedorContrato'.
     */
    public java.lang.String getCdIndicadorFornecedorContrato()
    {
        return this._cdIndicadorFornecedorContrato;
    } //-- java.lang.String getCdIndicadorFornecedorContrato() 

    /**
     * Returns the value of field
     * 'cdIndicadorModalidadeFornecedor'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorModalidadeFornecedor'.
     */
    public int getCdIndicadorModalidadeFornecedor()
    {
        return this._cdIndicadorModalidadeFornecedor;
    } //-- int getCdIndicadorModalidadeFornecedor() 

    /**
     * Returns the value of field
     * 'cdMomentoProcessamentoFornecedor'.
     * 
     * @return int
     * @return the value of field 'cdMomentoProcessamentoFornecedor'
     */
    public int getCdMomentoProcessamentoFornecedor()
    {
        return this._cdMomentoProcessamentoFornecedor;
    } //-- int getCdMomentoProcessamentoFornecedor() 

    /**
     * Returns the value of field 'cdPagamentoUtilFornecedor'.
     * 
     * @return int
     * @return the value of field 'cdPagamentoUtilFornecedor'.
     */
    public int getCdPagamentoUtilFornecedor()
    {
        return this._cdPagamentoUtilFornecedor;
    } //-- int getCdPagamentoUtilFornecedor() 

    /**
     * Returns the value of field 'cdProdutoRelacionadoFornecedor'.
     * 
     * @return int
     * @return the value of field 'cdProdutoRelacionadoFornecedor'.
     */
    public int getCdProdutoRelacionadoFornecedor()
    {
        return this._cdProdutoRelacionadoFornecedor;
    } //-- int getCdProdutoRelacionadoFornecedor() 

    /**
     * Returns the value of field 'dsIndicadorFeriadoFornecedor'.
     * 
     * @return String
     * @return the value of field 'dsIndicadorFeriadoFornecedor'.
     */
    public java.lang.String getDsIndicadorFeriadoFornecedor()
    {
        return this._dsIndicadorFeriadoFornecedor;
    } //-- java.lang.String getDsIndicadorFeriadoFornecedor() 

    /**
     * Returns the value of field
     * 'dsMomentoProcessamentoFornecedor'.
     * 
     * @return String
     * @return the value of field 'dsMomentoProcessamentoFornecedor'
     */
    public java.lang.String getDsMomentoProcessamentoFornecedor()
    {
        return this._dsMomentoProcessamentoFornecedor;
    } //-- java.lang.String getDsMomentoProcessamentoFornecedor() 

    /**
     * Returns the value of field 'dsPagamentoUtilFornecedor'.
     * 
     * @return String
     * @return the value of field 'dsPagamentoUtilFornecedor'.
     */
    public java.lang.String getDsPagamentoUtilFornecedor()
    {
        return this._dsPagamentoUtilFornecedor;
    } //-- java.lang.String getDsPagamentoUtilFornecedor() 

    /**
     * Returns the value of field 'dsProdutoRelacionadoFornecedor'.
     * 
     * @return String
     * @return the value of field 'dsProdutoRelacionadoFornecedor'.
     */
    public java.lang.String getDsProdutoRelacionadoFornecedor()
    {
        return this._dsProdutoRelacionadoFornecedor;
    } //-- java.lang.String getDsProdutoRelacionadoFornecedor() 

    /**
     * Returns the value of field
     * 'nrQuantidadeDiasFloatFornecedor'.
     * 
     * @return int
     * @return the value of field 'nrQuantidadeDiasFloatFornecedor'.
     */
    public int getNrQuantidadeDiasFloatFornecedor()
    {
        return this._nrQuantidadeDiasFloatFornecedor;
    } //-- int getNrQuantidadeDiasFloatFornecedor() 

    /**
     * Method hasCdIndicadorFeriadoFornecedor
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorFeriadoFornecedor()
    {
        return this._has_cdIndicadorFeriadoFornecedor;
    } //-- boolean hasCdIndicadorFeriadoFornecedor() 

    /**
     * Method hasCdIndicadorModalidadeFornecedor
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorModalidadeFornecedor()
    {
        return this._has_cdIndicadorModalidadeFornecedor;
    } //-- boolean hasCdIndicadorModalidadeFornecedor() 

    /**
     * Method hasCdMomentoProcessamentoFornecedor
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMomentoProcessamentoFornecedor()
    {
        return this._has_cdMomentoProcessamentoFornecedor;
    } //-- boolean hasCdMomentoProcessamentoFornecedor() 

    /**
     * Method hasCdPagamentoUtilFornecedor
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPagamentoUtilFornecedor()
    {
        return this._has_cdPagamentoUtilFornecedor;
    } //-- boolean hasCdPagamentoUtilFornecedor() 

    /**
     * Method hasCdProdutoRelacionadoFornecedor
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdutoRelacionadoFornecedor()
    {
        return this._has_cdProdutoRelacionadoFornecedor;
    } //-- boolean hasCdProdutoRelacionadoFornecedor() 

    /**
     * Method hasNrQuantidadeDiasFloatFornecedor
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrQuantidadeDiasFloatFornecedor()
    {
        return this._has_nrQuantidadeDiasFloatFornecedor;
    } //-- boolean hasNrQuantidadeDiasFloatFornecedor() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdIndicadorFeriadoFornecedor'.
     * 
     * @param cdIndicadorFeriadoFornecedor the value of field
     * 'cdIndicadorFeriadoFornecedor'.
     */
    public void setCdIndicadorFeriadoFornecedor(int cdIndicadorFeriadoFornecedor)
    {
        this._cdIndicadorFeriadoFornecedor = cdIndicadorFeriadoFornecedor;
        this._has_cdIndicadorFeriadoFornecedor = true;
    } //-- void setCdIndicadorFeriadoFornecedor(int) 

    /**
     * Sets the value of field 'cdIndicadorFornecedorContrato'.
     * 
     * @param cdIndicadorFornecedorContrato the value of field
     * 'cdIndicadorFornecedorContrato'.
     */
    public void setCdIndicadorFornecedorContrato(java.lang.String cdIndicadorFornecedorContrato)
    {
        this._cdIndicadorFornecedorContrato = cdIndicadorFornecedorContrato;
    } //-- void setCdIndicadorFornecedorContrato(java.lang.String) 

    /**
     * Sets the value of field 'cdIndicadorModalidadeFornecedor'.
     * 
     * @param cdIndicadorModalidadeFornecedor the value of field
     * 'cdIndicadorModalidadeFornecedor'.
     */
    public void setCdIndicadorModalidadeFornecedor(int cdIndicadorModalidadeFornecedor)
    {
        this._cdIndicadorModalidadeFornecedor = cdIndicadorModalidadeFornecedor;
        this._has_cdIndicadorModalidadeFornecedor = true;
    } //-- void setCdIndicadorModalidadeFornecedor(int) 

    /**
     * Sets the value of field 'cdMomentoProcessamentoFornecedor'.
     * 
     * @param cdMomentoProcessamentoFornecedor the value of field
     * 'cdMomentoProcessamentoFornecedor'.
     */
    public void setCdMomentoProcessamentoFornecedor(int cdMomentoProcessamentoFornecedor)
    {
        this._cdMomentoProcessamentoFornecedor = cdMomentoProcessamentoFornecedor;
        this._has_cdMomentoProcessamentoFornecedor = true;
    } //-- void setCdMomentoProcessamentoFornecedor(int) 

    /**
     * Sets the value of field 'cdPagamentoUtilFornecedor'.
     * 
     * @param cdPagamentoUtilFornecedor the value of field
     * 'cdPagamentoUtilFornecedor'.
     */
    public void setCdPagamentoUtilFornecedor(int cdPagamentoUtilFornecedor)
    {
        this._cdPagamentoUtilFornecedor = cdPagamentoUtilFornecedor;
        this._has_cdPagamentoUtilFornecedor = true;
    } //-- void setCdPagamentoUtilFornecedor(int) 

    /**
     * Sets the value of field 'cdProdutoRelacionadoFornecedor'.
     * 
     * @param cdProdutoRelacionadoFornecedor the value of field
     * 'cdProdutoRelacionadoFornecedor'.
     */
    public void setCdProdutoRelacionadoFornecedor(int cdProdutoRelacionadoFornecedor)
    {
        this._cdProdutoRelacionadoFornecedor = cdProdutoRelacionadoFornecedor;
        this._has_cdProdutoRelacionadoFornecedor = true;
    } //-- void setCdProdutoRelacionadoFornecedor(int) 

    /**
     * Sets the value of field 'dsIndicadorFeriadoFornecedor'.
     * 
     * @param dsIndicadorFeriadoFornecedor the value of field
     * 'dsIndicadorFeriadoFornecedor'.
     */
    public void setDsIndicadorFeriadoFornecedor(java.lang.String dsIndicadorFeriadoFornecedor)
    {
        this._dsIndicadorFeriadoFornecedor = dsIndicadorFeriadoFornecedor;
    } //-- void setDsIndicadorFeriadoFornecedor(java.lang.String) 

    /**
     * Sets the value of field 'dsMomentoProcessamentoFornecedor'.
     * 
     * @param dsMomentoProcessamentoFornecedor the value of field
     * 'dsMomentoProcessamentoFornecedor'.
     */
    public void setDsMomentoProcessamentoFornecedor(java.lang.String dsMomentoProcessamentoFornecedor)
    {
        this._dsMomentoProcessamentoFornecedor = dsMomentoProcessamentoFornecedor;
    } //-- void setDsMomentoProcessamentoFornecedor(java.lang.String) 

    /**
     * Sets the value of field 'dsPagamentoUtilFornecedor'.
     * 
     * @param dsPagamentoUtilFornecedor the value of field
     * 'dsPagamentoUtilFornecedor'.
     */
    public void setDsPagamentoUtilFornecedor(java.lang.String dsPagamentoUtilFornecedor)
    {
        this._dsPagamentoUtilFornecedor = dsPagamentoUtilFornecedor;
    } //-- void setDsPagamentoUtilFornecedor(java.lang.String) 

    /**
     * Sets the value of field 'dsProdutoRelacionadoFornecedor'.
     * 
     * @param dsProdutoRelacionadoFornecedor the value of field
     * 'dsProdutoRelacionadoFornecedor'.
     */
    public void setDsProdutoRelacionadoFornecedor(java.lang.String dsProdutoRelacionadoFornecedor)
    {
        this._dsProdutoRelacionadoFornecedor = dsProdutoRelacionadoFornecedor;
    } //-- void setDsProdutoRelacionadoFornecedor(java.lang.String) 

    /**
     * Sets the value of field 'nrQuantidadeDiasFloatFornecedor'.
     * 
     * @param nrQuantidadeDiasFloatFornecedor the value of field
     * 'nrQuantidadeDiasFloatFornecedor'.
     */
    public void setNrQuantidadeDiasFloatFornecedor(int nrQuantidadeDiasFloatFornecedor)
    {
        this._nrQuantidadeDiasFloatFornecedor = nrQuantidadeDiasFloatFornecedor;
        this._has_nrQuantidadeDiasFloatFornecedor = true;
    } //-- void setNrQuantidadeDiasFloatFornecedor(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
