/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/interfaz.ftl,v $
 * $Id: interfaz.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: interfaz.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.mantercadastroterceiroparticipante;

import br.com.bradesco.web.pgit.service.business.mantercadastroterceiroparticipante.bean.AlterarTercParticiContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadastroterceiroparticipante.bean.AlterarTercParticiContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadastroterceiroparticipante.bean.ConsultarTercPartContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadastroterceiroparticipante.bean.ConsultarTercPartContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadastroterceiroparticipante.bean.ExcluirTerceiroPartContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadastroterceiroparticipante.bean.ExcluirTerceiroPartContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadastroterceiroparticipante.bean.IncluirTerceiroPartContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadastroterceiroparticipante.bean.IncluirTerceiroPartContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadastroterceiroparticipante.bean.ListarTercParticipanteContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadastroterceiroparticipante.bean.ListarTercParticipanteContratoSaidaDTO;

/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Interface do adaptador: ManterCadastroTerceiroParticipante
 * </p>
 * 
 * @comment CODIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public interface IManterCadastroTerceiroParticipanteService {

    /**
     * Listar terc participante contrato.
     *
     * @param entrada the entrada
     * @return the listar terc participante contrato saida dto
     */
    ListarTercParticipanteContratoSaidaDTO listarTercParticipanteContrato (ListarTercParticipanteContratoEntradaDTO entrada);
    
    /**
     * Incluir terceiro part contrato.
     *
     * @param entrada the entrada
     * @return the incluir terceiro part contrato saida dto
     */
    IncluirTerceiroPartContratoSaidaDTO incluirTerceiroPartContrato(IncluirTerceiroPartContratoEntradaDTO entrada);
    
    /**
     * Excluir terceiro part contrato.
     *
     * @param entrada the entrada
     * @return the excluir terceiro part contrato saida dto
     */
    ExcluirTerceiroPartContratoSaidaDTO excluirTerceiroPartContrato(ExcluirTerceiroPartContratoEntradaDTO entrada);
    
    /**
     * Consultar terc part contrato.
     *
     * @param entrada the entrada
     * @return the consultar terc part contrato saida dto
     */
    ConsultarTercPartContratoSaidaDTO consultarTercPartContrato(ConsultarTercPartContratoEntradaDTO entrada);
    
    /**
     * Alterar terc partici contrato.
     *
     * @param entrada the entrada
     * @return the alterar terc partici contrato saida dto
     */
    AlterarTercParticiContratoSaidaDTO alterarTercParticiContrato(AlterarTercParticiContratoEntradaDTO entrada);

}

