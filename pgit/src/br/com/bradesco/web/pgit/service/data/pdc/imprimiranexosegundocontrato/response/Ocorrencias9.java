/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias9.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias9 implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _dsCsdTipoComprovante
     */
    private java.lang.String _dsCsdTipoComprovante;

    /**
     * Field _dsCsdTipoRejeicaoLote
     */
    private java.lang.String _dsCsdTipoRejeicaoLote;

    /**
     * Field _dsCsdMeioDisponibilizacaoCorrentista
     */
    private java.lang.String _dsCsdMeioDisponibilizacaoCorrentista;

    /**
     * Field _dsCsdMediaDisponibilidadeCorrentista
     */
    private java.lang.String _dsCsdMediaDisponibilidadeCorrentista;

    /**
     * Field _dsCsdMediaDisponibilidadeNumeroCorrentistas
     */
    private java.lang.String _dsCsdMediaDisponibilidadeNumeroCorrentistas;

    /**
     * Field _dsCsdDestino
     */
    private java.lang.String _dsCsdDestino;

    /**
     * Field _qtCsdMesEmissao
     */
    private int _qtCsdMesEmissao = 0;

    /**
     * keeps track of state for field: _qtCsdMesEmissao
     */
    private boolean _has_qtCsdMesEmissao;

    /**
     * Field _dsCsdCadastroConservadorEndereco
     */
    private java.lang.String _dsCsdCadastroConservadorEndereco;

    /**
     * Field _cdCsdQuantidadeViasEmitir
     */
    private int _cdCsdQuantidadeViasEmitir = 0;

    /**
     * keeps track of state for field: _cdCsdQuantidadeViasEmitir
     */
    private boolean _has_cdCsdQuantidadeViasEmitir;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias9() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias9()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdCsdQuantidadeViasEmitir
     * 
     */
    public void deleteCdCsdQuantidadeViasEmitir()
    {
        this._has_cdCsdQuantidadeViasEmitir= false;
    } //-- void deleteCdCsdQuantidadeViasEmitir() 

    /**
     * Method deleteQtCsdMesEmissao
     * 
     */
    public void deleteQtCsdMesEmissao()
    {
        this._has_qtCsdMesEmissao= false;
    } //-- void deleteQtCsdMesEmissao() 

    /**
     * Returns the value of field 'cdCsdQuantidadeViasEmitir'.
     * 
     * @return int
     * @return the value of field 'cdCsdQuantidadeViasEmitir'.
     */
    public int getCdCsdQuantidadeViasEmitir()
    {
        return this._cdCsdQuantidadeViasEmitir;
    } //-- int getCdCsdQuantidadeViasEmitir() 

    /**
     * Returns the value of field
     * 'dsCsdCadastroConservadorEndereco'.
     * 
     * @return String
     * @return the value of field 'dsCsdCadastroConservadorEndereco'
     */
    public java.lang.String getDsCsdCadastroConservadorEndereco()
    {
        return this._dsCsdCadastroConservadorEndereco;
    } //-- java.lang.String getDsCsdCadastroConservadorEndereco() 

    /**
     * Returns the value of field 'dsCsdDestino'.
     * 
     * @return String
     * @return the value of field 'dsCsdDestino'.
     */
    public java.lang.String getDsCsdDestino()
    {
        return this._dsCsdDestino;
    } //-- java.lang.String getDsCsdDestino() 

    /**
     * Returns the value of field
     * 'dsCsdMediaDisponibilidadeCorrentista'.
     * 
     * @return String
     * @return the value of field
     * 'dsCsdMediaDisponibilidadeCorrentista'.
     */
    public java.lang.String getDsCsdMediaDisponibilidadeCorrentista()
    {
        return this._dsCsdMediaDisponibilidadeCorrentista;
    } //-- java.lang.String getDsCsdMediaDisponibilidadeCorrentista() 

    /**
     * Returns the value of field
     * 'dsCsdMediaDisponibilidadeNumeroCorrentistas'.
     * 
     * @return String
     * @return the value of field
     * 'dsCsdMediaDisponibilidadeNumeroCorrentistas'.
     */
    public java.lang.String getDsCsdMediaDisponibilidadeNumeroCorrentistas()
    {
        return this._dsCsdMediaDisponibilidadeNumeroCorrentistas;
    } //-- java.lang.String getDsCsdMediaDisponibilidadeNumeroCorrentistas() 

    /**
     * Returns the value of field
     * 'dsCsdMeioDisponibilizacaoCorrentista'.
     * 
     * @return String
     * @return the value of field
     * 'dsCsdMeioDisponibilizacaoCorrentista'.
     */
    public java.lang.String getDsCsdMeioDisponibilizacaoCorrentista()
    {
        return this._dsCsdMeioDisponibilizacaoCorrentista;
    } //-- java.lang.String getDsCsdMeioDisponibilizacaoCorrentista() 

    /**
     * Returns the value of field 'dsCsdTipoComprovante'.
     * 
     * @return String
     * @return the value of field 'dsCsdTipoComprovante'.
     */
    public java.lang.String getDsCsdTipoComprovante()
    {
        return this._dsCsdTipoComprovante;
    } //-- java.lang.String getDsCsdTipoComprovante() 

    /**
     * Returns the value of field 'dsCsdTipoRejeicaoLote'.
     * 
     * @return String
     * @return the value of field 'dsCsdTipoRejeicaoLote'.
     */
    public java.lang.String getDsCsdTipoRejeicaoLote()
    {
        return this._dsCsdTipoRejeicaoLote;
    } //-- java.lang.String getDsCsdTipoRejeicaoLote() 

    /**
     * Returns the value of field 'qtCsdMesEmissao'.
     * 
     * @return int
     * @return the value of field 'qtCsdMesEmissao'.
     */
    public int getQtCsdMesEmissao()
    {
        return this._qtCsdMesEmissao;
    } //-- int getQtCsdMesEmissao() 

    /**
     * Method hasCdCsdQuantidadeViasEmitir
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCsdQuantidadeViasEmitir()
    {
        return this._has_cdCsdQuantidadeViasEmitir;
    } //-- boolean hasCdCsdQuantidadeViasEmitir() 

    /**
     * Method hasQtCsdMesEmissao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtCsdMesEmissao()
    {
        return this._has_qtCsdMesEmissao;
    } //-- boolean hasQtCsdMesEmissao() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdCsdQuantidadeViasEmitir'.
     * 
     * @param cdCsdQuantidadeViasEmitir the value of field
     * 'cdCsdQuantidadeViasEmitir'.
     */
    public void setCdCsdQuantidadeViasEmitir(int cdCsdQuantidadeViasEmitir)
    {
        this._cdCsdQuantidadeViasEmitir = cdCsdQuantidadeViasEmitir;
        this._has_cdCsdQuantidadeViasEmitir = true;
    } //-- void setCdCsdQuantidadeViasEmitir(int) 

    /**
     * Sets the value of field 'dsCsdCadastroConservadorEndereco'.
     * 
     * @param dsCsdCadastroConservadorEndereco the value of field
     * 'dsCsdCadastroConservadorEndereco'.
     */
    public void setDsCsdCadastroConservadorEndereco(java.lang.String dsCsdCadastroConservadorEndereco)
    {
        this._dsCsdCadastroConservadorEndereco = dsCsdCadastroConservadorEndereco;
    } //-- void setDsCsdCadastroConservadorEndereco(java.lang.String) 

    /**
     * Sets the value of field 'dsCsdDestino'.
     * 
     * @param dsCsdDestino the value of field 'dsCsdDestino'.
     */
    public void setDsCsdDestino(java.lang.String dsCsdDestino)
    {
        this._dsCsdDestino = dsCsdDestino;
    } //-- void setDsCsdDestino(java.lang.String) 

    /**
     * Sets the value of field
     * 'dsCsdMediaDisponibilidadeCorrentista'.
     * 
     * @param dsCsdMediaDisponibilidadeCorrentista the value of
     * field 'dsCsdMediaDisponibilidadeCorrentista'.
     */
    public void setDsCsdMediaDisponibilidadeCorrentista(java.lang.String dsCsdMediaDisponibilidadeCorrentista)
    {
        this._dsCsdMediaDisponibilidadeCorrentista = dsCsdMediaDisponibilidadeCorrentista;
    } //-- void setDsCsdMediaDisponibilidadeCorrentista(java.lang.String) 

    /**
     * Sets the value of field
     * 'dsCsdMediaDisponibilidadeNumeroCorrentistas'.
     * 
     * @param dsCsdMediaDisponibilidadeNumeroCorrentistas the value
     * of field 'dsCsdMediaDisponibilidadeNumeroCorrentistas'.
     */
    public void setDsCsdMediaDisponibilidadeNumeroCorrentistas(java.lang.String dsCsdMediaDisponibilidadeNumeroCorrentistas)
    {
        this._dsCsdMediaDisponibilidadeNumeroCorrentistas = dsCsdMediaDisponibilidadeNumeroCorrentistas;
    } //-- void setDsCsdMediaDisponibilidadeNumeroCorrentistas(java.lang.String) 

    /**
     * Sets the value of field
     * 'dsCsdMeioDisponibilizacaoCorrentista'.
     * 
     * @param dsCsdMeioDisponibilizacaoCorrentista the value of
     * field 'dsCsdMeioDisponibilizacaoCorrentista'.
     */
    public void setDsCsdMeioDisponibilizacaoCorrentista(java.lang.String dsCsdMeioDisponibilizacaoCorrentista)
    {
        this._dsCsdMeioDisponibilizacaoCorrentista = dsCsdMeioDisponibilizacaoCorrentista;
    } //-- void setDsCsdMeioDisponibilizacaoCorrentista(java.lang.String) 

    /**
     * Sets the value of field 'dsCsdTipoComprovante'.
     * 
     * @param dsCsdTipoComprovante the value of field
     * 'dsCsdTipoComprovante'.
     */
    public void setDsCsdTipoComprovante(java.lang.String dsCsdTipoComprovante)
    {
        this._dsCsdTipoComprovante = dsCsdTipoComprovante;
    } //-- void setDsCsdTipoComprovante(java.lang.String) 

    /**
     * Sets the value of field 'dsCsdTipoRejeicaoLote'.
     * 
     * @param dsCsdTipoRejeicaoLote the value of field
     * 'dsCsdTipoRejeicaoLote'.
     */
    public void setDsCsdTipoRejeicaoLote(java.lang.String dsCsdTipoRejeicaoLote)
    {
        this._dsCsdTipoRejeicaoLote = dsCsdTipoRejeicaoLote;
    } //-- void setDsCsdTipoRejeicaoLote(java.lang.String) 

    /**
     * Sets the value of field 'qtCsdMesEmissao'.
     * 
     * @param qtCsdMesEmissao the value of field 'qtCsdMesEmissao'.
     */
    public void setQtCsdMesEmissao(int qtCsdMesEmissao)
    {
        this._qtCsdMesEmissao = qtCsdMesEmissao;
        this._has_qtCsdMesEmissao = true;
    } //-- void setQtCsdMesEmissao(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias9
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias9 unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias9) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias9.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias9 unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
