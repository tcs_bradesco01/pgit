/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultarpagtosindantpostergacao.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.math.BigDecimal;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ConsultarPagtosIndAntPostergacaoRequest.
 * 
 * @version $Revision$ $Date$
 */
public class ConsultarPagtosIndAntPostergacaoRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdTipoPesquisa
     */
    private int _cdTipoPesquisa = 0;

    /**
     * keeps track of state for field: _cdTipoPesquisa
     */
    private boolean _has_cdTipoPesquisa;

    /**
     * Field _cdAgendadosPagosNaoPagos
     */
    private int _cdAgendadosPagosNaoPagos = 0;

    /**
     * keeps track of state for field: _cdAgendadosPagosNaoPagos
     */
    private boolean _has_cdAgendadosPagosNaoPagos;

    /**
     * Field _nrOcorrencias
     */
    private int _nrOcorrencias = 0;

    /**
     * keeps track of state for field: _nrOcorrencias
     */
    private boolean _has_nrOcorrencias;

    /**
     * Field _cdPessoaJuridicaContrato
     */
    private long _cdPessoaJuridicaContrato = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridicaContrato
     */
    private boolean _has_cdPessoaJuridicaContrato;

    /**
     * Field _cdTipoContratoNegocio
     */
    private int _cdTipoContratoNegocio = 0;

    /**
     * keeps track of state for field: _cdTipoContratoNegocio
     */
    private boolean _has_cdTipoContratoNegocio;

    /**
     * Field _nrSequenciaContratoNegocio
     */
    private long _nrSequenciaContratoNegocio = 0;

    /**
     * keeps track of state for field: _nrSequenciaContratoNegocio
     */
    private boolean _has_nrSequenciaContratoNegocio;

    /**
     * Field _cdBanco
     */
    private int _cdBanco = 0;

    /**
     * keeps track of state for field: _cdBanco
     */
    private boolean _has_cdBanco;

    /**
     * Field _cdAgencia
     */
    private int _cdAgencia = 0;

    /**
     * keeps track of state for field: _cdAgencia
     */
    private boolean _has_cdAgencia;

    /**
     * Field _cdConta
     */
    private long _cdConta = 0;

    /**
     * keeps track of state for field: _cdConta
     */
    private boolean _has_cdConta;

    /**
     * Field _cdDigitoConta
     */
    private java.lang.String _cdDigitoConta;

    /**
     * Field _dtCreditoPagamentoInicio
     */
    private java.lang.String _dtCreditoPagamentoInicio;

    /**
     * Field _dtCreditoPagamentoFim
     */
    private java.lang.String _dtCreditoPagamentoFim;

    /**
     * Field _nrArquivoRemssaPagamento
     */
    private long _nrArquivoRemssaPagamento = 0;

    /**
     * keeps track of state for field: _nrArquivoRemssaPagamento
     */
    private boolean _has_nrArquivoRemssaPagamento;

    /**
     * Field _cdParticipante
     */
    private long _cdParticipante = 0;

    /**
     * keeps track of state for field: _cdParticipante
     */
    private boolean _has_cdParticipante;

    /**
     * Field _cdCpfCnpjParticipante
     */
    private long _cdCpfCnpjParticipante = 0;

    /**
     * keeps track of state for field: _cdCpfCnpjParticipante
     */
    private boolean _has_cdCpfCnpjParticipante;

    /**
     * Field _cdFilialCnpjParticipante
     */
    private int _cdFilialCnpjParticipante = 0;

    /**
     * keeps track of state for field: _cdFilialCnpjParticipante
     */
    private boolean _has_cdFilialCnpjParticipante;

    /**
     * Field _cdControleCnpjParticipante
     */
    private int _cdControleCnpjParticipante = 0;

    /**
     * keeps track of state for field: _cdControleCnpjParticipante
     */
    private boolean _has_cdControleCnpjParticipante;

    /**
     * Field _cdProdutoServicoOperacao
     */
    private int _cdProdutoServicoOperacao = 0;

    /**
     * keeps track of state for field: _cdProdutoServicoOperacao
     */
    private boolean _has_cdProdutoServicoOperacao;

    /**
     * Field _cdProdutoServicoRelacionado
     */
    private int _cdProdutoServicoRelacionado = 0;

    /**
     * keeps track of state for field: _cdProdutoServicoRelacionado
     */
    private boolean _has_cdProdutoServicoRelacionado;

    /**
     * Field _cdControlePagamentoDe
     */
    private java.lang.String _cdControlePagamentoDe;

    /**
     * Field _cdControlePagamentoAte
     */
    private java.lang.String _cdControlePagamentoAte;

    /**
     * Field _vlPagamentoDe
     */
    private java.math.BigDecimal _vlPagamentoDe = new java.math.BigDecimal("0");

    /**
     * Field _vlPagamentoAte
     */
    private java.math.BigDecimal _vlPagamentoAte = new java.math.BigDecimal("0");

    /**
     * Field _cdSituacaoOperacaoPagamento
     */
    private int _cdSituacaoOperacaoPagamento = 0;

    /**
     * keeps track of state for field: _cdSituacaoOperacaoPagamento
     */
    private boolean _has_cdSituacaoOperacaoPagamento;

    /**
     * Field _cdMotivoSituacaoPagamento
     */
    private int _cdMotivoSituacaoPagamento = 0;

    /**
     * keeps track of state for field: _cdMotivoSituacaoPagamento
     */
    private boolean _has_cdMotivoSituacaoPagamento;

    /**
     * Field _cdBarraDocumento
     */
    private java.lang.String _cdBarraDocumento;

    /**
     * Field _cdFavorecidoClientePagador
     */
    private long _cdFavorecidoClientePagador = 0;

    /**
     * keeps track of state for field: _cdFavorecidoClientePagador
     */
    private boolean _has_cdFavorecidoClientePagador;

    /**
     * Field _cdInscricaoFavorecido
     */
    private long _cdInscricaoFavorecido = 0;

    /**
     * keeps track of state for field: _cdInscricaoFavorecido
     */
    private boolean _has_cdInscricaoFavorecido;

    /**
     * Field _cdIdentificacaoInscricaoFavorecido
     */
    private int _cdIdentificacaoInscricaoFavorecido = 0;

    /**
     * keeps track of state for field:
     * _cdIdentificacaoInscricaoFavorecido
     */
    private boolean _has_cdIdentificacaoInscricaoFavorecido;

    /**
     * Field _cdBancoBeneficiario
     */
    private int _cdBancoBeneficiario = 0;

    /**
     * keeps track of state for field: _cdBancoBeneficiario
     */
    private boolean _has_cdBancoBeneficiario;

    /**
     * Field _cdAgenciaBeneficiario
     */
    private int _cdAgenciaBeneficiario = 0;

    /**
     * keeps track of state for field: _cdAgenciaBeneficiario
     */
    private boolean _has_cdAgenciaBeneficiario;

    /**
     * Field _cdContaBeneficiario
     */
    private long _cdContaBeneficiario = 0;

    /**
     * keeps track of state for field: _cdContaBeneficiario
     */
    private boolean _has_cdContaBeneficiario;

    /**
     * Field _cdDigitoContaBeneficiario
     */
    private java.lang.String _cdDigitoContaBeneficiario;

    /**
     * Field _cdEmpresaContrato
     */
    private long _cdEmpresaContrato = 0;

    /**
     * keeps track of state for field: _cdEmpresaContrato
     */
    private boolean _has_cdEmpresaContrato;

    /**
     * Field _cdTipoConta
     */
    private int _cdTipoConta = 0;

    /**
     * keeps track of state for field: _cdTipoConta
     */
    private boolean _has_cdTipoConta;

    /**
     * Field _nrContrato
     */
    private long _nrContrato = 0;

    /**
     * keeps track of state for field: _nrContrato
     */
    private boolean _has_nrContrato;

    /**
     * Field _nrUnidadeOrganizacional
     */
    private int _nrUnidadeOrganizacional = 0;

    /**
     * keeps track of state for field: _nrUnidadeOrganizacional
     */
    private boolean _has_nrUnidadeOrganizacional;

    /**
     * Field _cdBeneficio
     */
    private long _cdBeneficio = 0;

    /**
     * keeps track of state for field: _cdBeneficio
     */
    private boolean _has_cdBeneficio;

    /**
     * Field _cdEspecieBeneficioInss
     */
    private int _cdEspecieBeneficioInss = 0;

    /**
     * keeps track of state for field: _cdEspecieBeneficioInss
     */
    private boolean _has_cdEspecieBeneficioInss;

    /**
     * Field _cdPessoaContratoDebito
     */
    private long _cdPessoaContratoDebito = 0;

    /**
     * keeps track of state for field: _cdPessoaContratoDebito
     */
    private boolean _has_cdPessoaContratoDebito;

    /**
     * Field _cdTipoContratoDebito
     */
    private int _cdTipoContratoDebito = 0;

    /**
     * keeps track of state for field: _cdTipoContratoDebito
     */
    private boolean _has_cdTipoContratoDebito;

    /**
     * Field _nrSequenciaContratoDebito
     */
    private long _nrSequenciaContratoDebito = 0;

    /**
     * keeps track of state for field: _nrSequenciaContratoDebito
     */
    private boolean _has_nrSequenciaContratoDebito;

    /**
     * Field _cdPessoaContratoCredt
     */
    private long _cdPessoaContratoCredt = 0;

    /**
     * keeps track of state for field: _cdPessoaContratoCredt
     */
    private boolean _has_cdPessoaContratoCredt;

    /**
     * Field _cdTipoContratoCredt
     */
    private int _cdTipoContratoCredt = 0;

    /**
     * keeps track of state for field: _cdTipoContratoCredt
     */
    private boolean _has_cdTipoContratoCredt;

    /**
     * Field _nrSequenciaContratoCredt
     */
    private long _nrSequenciaContratoCredt = 0;

    /**
     * keeps track of state for field: _nrSequenciaContratoCredt
     */
    private boolean _has_nrSequenciaContratoCredt;

    /**
     * Field _cdIndiceSimulaPagamento
     */
    private int _cdIndiceSimulaPagamento = 0;

    /**
     * keeps track of state for field: _cdIndiceSimulaPagamento
     */
    private boolean _has_cdIndiceSimulaPagamento;

    /**
     * Field _cdOrigemRecebidoEfetivacao
     */
    private int _cdOrigemRecebidoEfetivacao = 0;

    /**
     * keeps track of state for field: _cdOrigemRecebidoEfetivacao
     */
    private boolean _has_cdOrigemRecebidoEfetivacao;

    /**
     * Field _cdClassificacao
     */
    private int _cdClassificacao = 0;

    /**
     * keeps track of state for field: _cdClassificacao
     */
    private boolean _has_cdClassificacao;

    /**
     * Field _cdTituloPgtoRastreado
     */
    private int _cdTituloPgtoRastreado = 0;

    /**
     * keeps track of state for field: _cdTituloPgtoRastreado
     */
    private boolean _has_cdTituloPgtoRastreado;

    /**
     * Field _cdTipoAntecipacao
     */
    private int _cdTipoAntecipacao = 0;

    /**
     * keeps track of state for field: _cdTipoAntecipacao
     */
    private boolean _has_cdTipoAntecipacao;


      //----------------/
     //- Constructors -/
    //----------------/

    public ConsultarPagtosIndAntPostergacaoRequest() 
     {
        super();
        setVlPagamentoDe(new java.math.BigDecimal("0"));
        setVlPagamentoAte(new java.math.BigDecimal("0"));
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarpagtosindantpostergacao.request.ConsultarPagtosIndAntPostergacaoRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdAgencia
     * 
     */
    public void deleteCdAgencia()
    {
        this._has_cdAgencia= false;
    } //-- void deleteCdAgencia() 

    /**
     * Method deleteCdAgenciaBeneficiario
     * 
     */
    public void deleteCdAgenciaBeneficiario()
    {
        this._has_cdAgenciaBeneficiario= false;
    } //-- void deleteCdAgenciaBeneficiario() 

    /**
     * Method deleteCdAgendadosPagosNaoPagos
     * 
     */
    public void deleteCdAgendadosPagosNaoPagos()
    {
        this._has_cdAgendadosPagosNaoPagos= false;
    } //-- void deleteCdAgendadosPagosNaoPagos() 

    /**
     * Method deleteCdBanco
     * 
     */
    public void deleteCdBanco()
    {
        this._has_cdBanco= false;
    } //-- void deleteCdBanco() 

    /**
     * Method deleteCdBancoBeneficiario
     * 
     */
    public void deleteCdBancoBeneficiario()
    {
        this._has_cdBancoBeneficiario= false;
    } //-- void deleteCdBancoBeneficiario() 

    /**
     * Method deleteCdBeneficio
     * 
     */
    public void deleteCdBeneficio()
    {
        this._has_cdBeneficio= false;
    } //-- void deleteCdBeneficio() 

    /**
     * Method deleteCdClassificacao
     * 
     */
    public void deleteCdClassificacao()
    {
        this._has_cdClassificacao= false;
    } //-- void deleteCdClassificacao() 

    /**
     * Method deleteCdConta
     * 
     */
    public void deleteCdConta()
    {
        this._has_cdConta= false;
    } //-- void deleteCdConta() 

    /**
     * Method deleteCdContaBeneficiario
     * 
     */
    public void deleteCdContaBeneficiario()
    {
        this._has_cdContaBeneficiario= false;
    } //-- void deleteCdContaBeneficiario() 

    /**
     * Method deleteCdControleCnpjParticipante
     * 
     */
    public void deleteCdControleCnpjParticipante()
    {
        this._has_cdControleCnpjParticipante= false;
    } //-- void deleteCdControleCnpjParticipante() 

    /**
     * Method deleteCdCpfCnpjParticipante
     * 
     */
    public void deleteCdCpfCnpjParticipante()
    {
        this._has_cdCpfCnpjParticipante= false;
    } //-- void deleteCdCpfCnpjParticipante() 

    /**
     * Method deleteCdEmpresaContrato
     * 
     */
    public void deleteCdEmpresaContrato()
    {
        this._has_cdEmpresaContrato= false;
    } //-- void deleteCdEmpresaContrato() 

    /**
     * Method deleteCdEspecieBeneficioInss
     * 
     */
    public void deleteCdEspecieBeneficioInss()
    {
        this._has_cdEspecieBeneficioInss= false;
    } //-- void deleteCdEspecieBeneficioInss() 

    /**
     * Method deleteCdFavorecidoClientePagador
     * 
     */
    public void deleteCdFavorecidoClientePagador()
    {
        this._has_cdFavorecidoClientePagador= false;
    } //-- void deleteCdFavorecidoClientePagador() 

    /**
     * Method deleteCdFilialCnpjParticipante
     * 
     */
    public void deleteCdFilialCnpjParticipante()
    {
        this._has_cdFilialCnpjParticipante= false;
    } //-- void deleteCdFilialCnpjParticipante() 

    /**
     * Method deleteCdIdentificacaoInscricaoFavorecido
     * 
     */
    public void deleteCdIdentificacaoInscricaoFavorecido()
    {
        this._has_cdIdentificacaoInscricaoFavorecido= false;
    } //-- void deleteCdIdentificacaoInscricaoFavorecido() 

    /**
     * Method deleteCdIndiceSimulaPagamento
     * 
     */
    public void deleteCdIndiceSimulaPagamento()
    {
        this._has_cdIndiceSimulaPagamento= false;
    } //-- void deleteCdIndiceSimulaPagamento() 

    /**
     * Method deleteCdInscricaoFavorecido
     * 
     */
    public void deleteCdInscricaoFavorecido()
    {
        this._has_cdInscricaoFavorecido= false;
    } //-- void deleteCdInscricaoFavorecido() 

    /**
     * Method deleteCdMotivoSituacaoPagamento
     * 
     */
    public void deleteCdMotivoSituacaoPagamento()
    {
        this._has_cdMotivoSituacaoPagamento= false;
    } //-- void deleteCdMotivoSituacaoPagamento() 

    /**
     * Method deleteCdOrigemRecebidoEfetivacao
     * 
     */
    public void deleteCdOrigemRecebidoEfetivacao()
    {
        this._has_cdOrigemRecebidoEfetivacao= false;
    } //-- void deleteCdOrigemRecebidoEfetivacao() 

    /**
     * Method deleteCdParticipante
     * 
     */
    public void deleteCdParticipante()
    {
        this._has_cdParticipante= false;
    } //-- void deleteCdParticipante() 

    /**
     * Method deleteCdPessoaContratoCredt
     * 
     */
    public void deleteCdPessoaContratoCredt()
    {
        this._has_cdPessoaContratoCredt= false;
    } //-- void deleteCdPessoaContratoCredt() 

    /**
     * Method deleteCdPessoaContratoDebito
     * 
     */
    public void deleteCdPessoaContratoDebito()
    {
        this._has_cdPessoaContratoDebito= false;
    } //-- void deleteCdPessoaContratoDebito() 

    /**
     * Method deleteCdPessoaJuridicaContrato
     * 
     */
    public void deleteCdPessoaJuridicaContrato()
    {
        this._has_cdPessoaJuridicaContrato= false;
    } //-- void deleteCdPessoaJuridicaContrato() 

    /**
     * Method deleteCdProdutoServicoOperacao
     * 
     */
    public void deleteCdProdutoServicoOperacao()
    {
        this._has_cdProdutoServicoOperacao= false;
    } //-- void deleteCdProdutoServicoOperacao() 

    /**
     * Method deleteCdProdutoServicoRelacionado
     * 
     */
    public void deleteCdProdutoServicoRelacionado()
    {
        this._has_cdProdutoServicoRelacionado= false;
    } //-- void deleteCdProdutoServicoRelacionado() 

    /**
     * Method deleteCdSituacaoOperacaoPagamento
     * 
     */
    public void deleteCdSituacaoOperacaoPagamento()
    {
        this._has_cdSituacaoOperacaoPagamento= false;
    } //-- void deleteCdSituacaoOperacaoPagamento() 

    /**
     * Method deleteCdTipoAntecipacao
     * 
     */
    public void deleteCdTipoAntecipacao()
    {
        this._has_cdTipoAntecipacao= false;
    } //-- void deleteCdTipoAntecipacao() 

    /**
     * Method deleteCdTipoConta
     * 
     */
    public void deleteCdTipoConta()
    {
        this._has_cdTipoConta= false;
    } //-- void deleteCdTipoConta() 

    /**
     * Method deleteCdTipoContratoCredt
     * 
     */
    public void deleteCdTipoContratoCredt()
    {
        this._has_cdTipoContratoCredt= false;
    } //-- void deleteCdTipoContratoCredt() 

    /**
     * Method deleteCdTipoContratoDebito
     * 
     */
    public void deleteCdTipoContratoDebito()
    {
        this._has_cdTipoContratoDebito= false;
    } //-- void deleteCdTipoContratoDebito() 

    /**
     * Method deleteCdTipoContratoNegocio
     * 
     */
    public void deleteCdTipoContratoNegocio()
    {
        this._has_cdTipoContratoNegocio= false;
    } //-- void deleteCdTipoContratoNegocio() 

    /**
     * Method deleteCdTipoPesquisa
     * 
     */
    public void deleteCdTipoPesquisa()
    {
        this._has_cdTipoPesquisa= false;
    } //-- void deleteCdTipoPesquisa() 

    /**
     * Method deleteCdTituloPgtoRastreado
     * 
     */
    public void deleteCdTituloPgtoRastreado()
    {
        this._has_cdTituloPgtoRastreado= false;
    } //-- void deleteCdTituloPgtoRastreado() 

    /**
     * Method deleteNrArquivoRemssaPagamento
     * 
     */
    public void deleteNrArquivoRemssaPagamento()
    {
        this._has_nrArquivoRemssaPagamento= false;
    } //-- void deleteNrArquivoRemssaPagamento() 

    /**
     * Method deleteNrContrato
     * 
     */
    public void deleteNrContrato()
    {
        this._has_nrContrato= false;
    } //-- void deleteNrContrato() 

    /**
     * Method deleteNrOcorrencias
     * 
     */
    public void deleteNrOcorrencias()
    {
        this._has_nrOcorrencias= false;
    } //-- void deleteNrOcorrencias() 

    /**
     * Method deleteNrSequenciaContratoCredt
     * 
     */
    public void deleteNrSequenciaContratoCredt()
    {
        this._has_nrSequenciaContratoCredt= false;
    } //-- void deleteNrSequenciaContratoCredt() 

    /**
     * Method deleteNrSequenciaContratoDebito
     * 
     */
    public void deleteNrSequenciaContratoDebito()
    {
        this._has_nrSequenciaContratoDebito= false;
    } //-- void deleteNrSequenciaContratoDebito() 

    /**
     * Method deleteNrSequenciaContratoNegocio
     * 
     */
    public void deleteNrSequenciaContratoNegocio()
    {
        this._has_nrSequenciaContratoNegocio= false;
    } //-- void deleteNrSequenciaContratoNegocio() 

    /**
     * Method deleteNrUnidadeOrganizacional
     * 
     */
    public void deleteNrUnidadeOrganizacional()
    {
        this._has_nrUnidadeOrganizacional= false;
    } //-- void deleteNrUnidadeOrganizacional() 

    /**
     * Returns the value of field 'cdAgencia'.
     * 
     * @return int
     * @return the value of field 'cdAgencia'.
     */
    public int getCdAgencia()
    {
        return this._cdAgencia;
    } //-- int getCdAgencia() 

    /**
     * Returns the value of field 'cdAgenciaBeneficiario'.
     * 
     * @return int
     * @return the value of field 'cdAgenciaBeneficiario'.
     */
    public int getCdAgenciaBeneficiario()
    {
        return this._cdAgenciaBeneficiario;
    } //-- int getCdAgenciaBeneficiario() 

    /**
     * Returns the value of field 'cdAgendadosPagosNaoPagos'.
     * 
     * @return int
     * @return the value of field 'cdAgendadosPagosNaoPagos'.
     */
    public int getCdAgendadosPagosNaoPagos()
    {
        return this._cdAgendadosPagosNaoPagos;
    } //-- int getCdAgendadosPagosNaoPagos() 

    /**
     * Returns the value of field 'cdBanco'.
     * 
     * @return int
     * @return the value of field 'cdBanco'.
     */
    public int getCdBanco()
    {
        return this._cdBanco;
    } //-- int getCdBanco() 

    /**
     * Returns the value of field 'cdBancoBeneficiario'.
     * 
     * @return int
     * @return the value of field 'cdBancoBeneficiario'.
     */
    public int getCdBancoBeneficiario()
    {
        return this._cdBancoBeneficiario;
    } //-- int getCdBancoBeneficiario() 

    /**
     * Returns the value of field 'cdBarraDocumento'.
     * 
     * @return String
     * @return the value of field 'cdBarraDocumento'.
     */
    public java.lang.String getCdBarraDocumento()
    {
        return this._cdBarraDocumento;
    } //-- java.lang.String getCdBarraDocumento() 

    /**
     * Returns the value of field 'cdBeneficio'.
     * 
     * @return long
     * @return the value of field 'cdBeneficio'.
     */
    public long getCdBeneficio()
    {
        return this._cdBeneficio;
    } //-- long getCdBeneficio() 

    /**
     * Returns the value of field 'cdClassificacao'.
     * 
     * @return int
     * @return the value of field 'cdClassificacao'.
     */
    public int getCdClassificacao()
    {
        return this._cdClassificacao;
    } //-- int getCdClassificacao() 

    /**
     * Returns the value of field 'cdConta'.
     * 
     * @return long
     * @return the value of field 'cdConta'.
     */
    public long getCdConta()
    {
        return this._cdConta;
    } //-- long getCdConta() 

    /**
     * Returns the value of field 'cdContaBeneficiario'.
     * 
     * @return long
     * @return the value of field 'cdContaBeneficiario'.
     */
    public long getCdContaBeneficiario()
    {
        return this._cdContaBeneficiario;
    } //-- long getCdContaBeneficiario() 

    /**
     * Returns the value of field 'cdControleCnpjParticipante'.
     * 
     * @return int
     * @return the value of field 'cdControleCnpjParticipante'.
     */
    public int getCdControleCnpjParticipante()
    {
        return this._cdControleCnpjParticipante;
    } //-- int getCdControleCnpjParticipante() 

    /**
     * Returns the value of field 'cdControlePagamentoAte'.
     * 
     * @return String
     * @return the value of field 'cdControlePagamentoAte'.
     */
    public java.lang.String getCdControlePagamentoAte()
    {
        return this._cdControlePagamentoAte;
    } //-- java.lang.String getCdControlePagamentoAte() 

    /**
     * Returns the value of field 'cdControlePagamentoDe'.
     * 
     * @return String
     * @return the value of field 'cdControlePagamentoDe'.
     */
    public java.lang.String getCdControlePagamentoDe()
    {
        return this._cdControlePagamentoDe;
    } //-- java.lang.String getCdControlePagamentoDe() 

    /**
     * Returns the value of field 'cdCpfCnpjParticipante'.
     * 
     * @return long
     * @return the value of field 'cdCpfCnpjParticipante'.
     */
    public long getCdCpfCnpjParticipante()
    {
        return this._cdCpfCnpjParticipante;
    } //-- long getCdCpfCnpjParticipante() 

    /**
     * Returns the value of field 'cdDigitoConta'.
     * 
     * @return String
     * @return the value of field 'cdDigitoConta'.
     */
    public java.lang.String getCdDigitoConta()
    {
        return this._cdDigitoConta;
    } //-- java.lang.String getCdDigitoConta() 

    /**
     * Returns the value of field 'cdDigitoContaBeneficiario'.
     * 
     * @return String
     * @return the value of field 'cdDigitoContaBeneficiario'.
     */
    public java.lang.String getCdDigitoContaBeneficiario()
    {
        return this._cdDigitoContaBeneficiario;
    } //-- java.lang.String getCdDigitoContaBeneficiario() 

    /**
     * Returns the value of field 'cdEmpresaContrato'.
     * 
     * @return long
     * @return the value of field 'cdEmpresaContrato'.
     */
    public long getCdEmpresaContrato()
    {
        return this._cdEmpresaContrato;
    } //-- long getCdEmpresaContrato() 

    /**
     * Returns the value of field 'cdEspecieBeneficioInss'.
     * 
     * @return int
     * @return the value of field 'cdEspecieBeneficioInss'.
     */
    public int getCdEspecieBeneficioInss()
    {
        return this._cdEspecieBeneficioInss;
    } //-- int getCdEspecieBeneficioInss() 

    /**
     * Returns the value of field 'cdFavorecidoClientePagador'.
     * 
     * @return long
     * @return the value of field 'cdFavorecidoClientePagador'.
     */
    public long getCdFavorecidoClientePagador()
    {
        return this._cdFavorecidoClientePagador;
    } //-- long getCdFavorecidoClientePagador() 

    /**
     * Returns the value of field 'cdFilialCnpjParticipante'.
     * 
     * @return int
     * @return the value of field 'cdFilialCnpjParticipante'.
     */
    public int getCdFilialCnpjParticipante()
    {
        return this._cdFilialCnpjParticipante;
    } //-- int getCdFilialCnpjParticipante() 

    /**
     * Returns the value of field
     * 'cdIdentificacaoInscricaoFavorecido'.
     * 
     * @return int
     * @return the value of field
     * 'cdIdentificacaoInscricaoFavorecido'.
     */
    public int getCdIdentificacaoInscricaoFavorecido()
    {
        return this._cdIdentificacaoInscricaoFavorecido;
    } //-- int getCdIdentificacaoInscricaoFavorecido() 

    /**
     * Returns the value of field 'cdIndiceSimulaPagamento'.
     * 
     * @return int
     * @return the value of field 'cdIndiceSimulaPagamento'.
     */
    public int getCdIndiceSimulaPagamento()
    {
        return this._cdIndiceSimulaPagamento;
    } //-- int getCdIndiceSimulaPagamento() 

    /**
     * Returns the value of field 'cdInscricaoFavorecido'.
     * 
     * @return long
     * @return the value of field 'cdInscricaoFavorecido'.
     */
    public long getCdInscricaoFavorecido()
    {
        return this._cdInscricaoFavorecido;
    } //-- long getCdInscricaoFavorecido() 

    /**
     * Returns the value of field 'cdMotivoSituacaoPagamento'.
     * 
     * @return int
     * @return the value of field 'cdMotivoSituacaoPagamento'.
     */
    public int getCdMotivoSituacaoPagamento()
    {
        return this._cdMotivoSituacaoPagamento;
    } //-- int getCdMotivoSituacaoPagamento() 

    /**
     * Returns the value of field 'cdOrigemRecebidoEfetivacao'.
     * 
     * @return int
     * @return the value of field 'cdOrigemRecebidoEfetivacao'.
     */
    public int getCdOrigemRecebidoEfetivacao()
    {
        return this._cdOrigemRecebidoEfetivacao;
    } //-- int getCdOrigemRecebidoEfetivacao() 

    /**
     * Returns the value of field 'cdParticipante'.
     * 
     * @return long
     * @return the value of field 'cdParticipante'.
     */
    public long getCdParticipante()
    {
        return this._cdParticipante;
    } //-- long getCdParticipante() 

    /**
     * Returns the value of field 'cdPessoaContratoCredt'.
     * 
     * @return long
     * @return the value of field 'cdPessoaContratoCredt'.
     */
    public long getCdPessoaContratoCredt()
    {
        return this._cdPessoaContratoCredt;
    } //-- long getCdPessoaContratoCredt() 

    /**
     * Returns the value of field 'cdPessoaContratoDebito'.
     * 
     * @return long
     * @return the value of field 'cdPessoaContratoDebito'.
     */
    public long getCdPessoaContratoDebito()
    {
        return this._cdPessoaContratoDebito;
    } //-- long getCdPessoaContratoDebito() 

    /**
     * Returns the value of field 'cdPessoaJuridicaContrato'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridicaContrato'.
     */
    public long getCdPessoaJuridicaContrato()
    {
        return this._cdPessoaJuridicaContrato;
    } //-- long getCdPessoaJuridicaContrato() 

    /**
     * Returns the value of field 'cdProdutoServicoOperacao'.
     * 
     * @return int
     * @return the value of field 'cdProdutoServicoOperacao'.
     */
    public int getCdProdutoServicoOperacao()
    {
        return this._cdProdutoServicoOperacao;
    } //-- int getCdProdutoServicoOperacao() 

    /**
     * Returns the value of field 'cdProdutoServicoRelacionado'.
     * 
     * @return int
     * @return the value of field 'cdProdutoServicoRelacionado'.
     */
    public int getCdProdutoServicoRelacionado()
    {
        return this._cdProdutoServicoRelacionado;
    } //-- int getCdProdutoServicoRelacionado() 

    /**
     * Returns the value of field 'cdSituacaoOperacaoPagamento'.
     * 
     * @return int
     * @return the value of field 'cdSituacaoOperacaoPagamento'.
     */
    public int getCdSituacaoOperacaoPagamento()
    {
        return this._cdSituacaoOperacaoPagamento;
    } //-- int getCdSituacaoOperacaoPagamento() 

    /**
     * Returns the value of field 'cdTipoAntecipacao'.
     * 
     * @return int
     * @return the value of field 'cdTipoAntecipacao'.
     */
    public int getCdTipoAntecipacao()
    {
        return this._cdTipoAntecipacao;
    } //-- int getCdTipoAntecipacao() 

    /**
     * Returns the value of field 'cdTipoConta'.
     * 
     * @return int
     * @return the value of field 'cdTipoConta'.
     */
    public int getCdTipoConta()
    {
        return this._cdTipoConta;
    } //-- int getCdTipoConta() 

    /**
     * Returns the value of field 'cdTipoContratoCredt'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoCredt'.
     */
    public int getCdTipoContratoCredt()
    {
        return this._cdTipoContratoCredt;
    } //-- int getCdTipoContratoCredt() 

    /**
     * Returns the value of field 'cdTipoContratoDebito'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoDebito'.
     */
    public int getCdTipoContratoDebito()
    {
        return this._cdTipoContratoDebito;
    } //-- int getCdTipoContratoDebito() 

    /**
     * Returns the value of field 'cdTipoContratoNegocio'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoNegocio'.
     */
    public int getCdTipoContratoNegocio()
    {
        return this._cdTipoContratoNegocio;
    } //-- int getCdTipoContratoNegocio() 

    /**
     * Returns the value of field 'cdTipoPesquisa'.
     * 
     * @return int
     * @return the value of field 'cdTipoPesquisa'.
     */
    public int getCdTipoPesquisa()
    {
        return this._cdTipoPesquisa;
    } //-- int getCdTipoPesquisa() 

    /**
     * Returns the value of field 'cdTituloPgtoRastreado'.
     * 
     * @return int
     * @return the value of field 'cdTituloPgtoRastreado'.
     */
    public int getCdTituloPgtoRastreado()
    {
        return this._cdTituloPgtoRastreado;
    } //-- int getCdTituloPgtoRastreado() 

    /**
     * Returns the value of field 'dtCreditoPagamentoFim'.
     * 
     * @return String
     * @return the value of field 'dtCreditoPagamentoFim'.
     */
    public java.lang.String getDtCreditoPagamentoFim()
    {
        return this._dtCreditoPagamentoFim;
    } //-- java.lang.String getDtCreditoPagamentoFim() 

    /**
     * Returns the value of field 'dtCreditoPagamentoInicio'.
     * 
     * @return String
     * @return the value of field 'dtCreditoPagamentoInicio'.
     */
    public java.lang.String getDtCreditoPagamentoInicio()
    {
        return this._dtCreditoPagamentoInicio;
    } //-- java.lang.String getDtCreditoPagamentoInicio() 

    /**
     * Returns the value of field 'nrArquivoRemssaPagamento'.
     * 
     * @return long
     * @return the value of field 'nrArquivoRemssaPagamento'.
     */
    public long getNrArquivoRemssaPagamento()
    {
        return this._nrArquivoRemssaPagamento;
    } //-- long getNrArquivoRemssaPagamento() 

    /**
     * Returns the value of field 'nrContrato'.
     * 
     * @return long
     * @return the value of field 'nrContrato'.
     */
    public long getNrContrato()
    {
        return this._nrContrato;
    } //-- long getNrContrato() 

    /**
     * Returns the value of field 'nrOcorrencias'.
     * 
     * @return int
     * @return the value of field 'nrOcorrencias'.
     */
    public int getNrOcorrencias()
    {
        return this._nrOcorrencias;
    } //-- int getNrOcorrencias() 

    /**
     * Returns the value of field 'nrSequenciaContratoCredt'.
     * 
     * @return long
     * @return the value of field 'nrSequenciaContratoCredt'.
     */
    public long getNrSequenciaContratoCredt()
    {
        return this._nrSequenciaContratoCredt;
    } //-- long getNrSequenciaContratoCredt() 

    /**
     * Returns the value of field 'nrSequenciaContratoDebito'.
     * 
     * @return long
     * @return the value of field 'nrSequenciaContratoDebito'.
     */
    public long getNrSequenciaContratoDebito()
    {
        return this._nrSequenciaContratoDebito;
    } //-- long getNrSequenciaContratoDebito() 

    /**
     * Returns the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @return long
     * @return the value of field 'nrSequenciaContratoNegocio'.
     */
    public long getNrSequenciaContratoNegocio()
    {
        return this._nrSequenciaContratoNegocio;
    } //-- long getNrSequenciaContratoNegocio() 

    /**
     * Returns the value of field 'nrUnidadeOrganizacional'.
     * 
     * @return int
     * @return the value of field 'nrUnidadeOrganizacional'.
     */
    public int getNrUnidadeOrganizacional()
    {
        return this._nrUnidadeOrganizacional;
    } //-- int getNrUnidadeOrganizacional() 

    /**
     * Returns the value of field 'vlPagamentoAte'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlPagamentoAte'.
     */
    public java.math.BigDecimal getVlPagamentoAte()
    {
        return this._vlPagamentoAte;
    } //-- java.math.BigDecimal getVlPagamentoAte() 

    /**
     * Returns the value of field 'vlPagamentoDe'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlPagamentoDe'.
     */
    public java.math.BigDecimal getVlPagamentoDe()
    {
        return this._vlPagamentoDe;
    } //-- java.math.BigDecimal getVlPagamentoDe() 

    /**
     * Method hasCdAgencia
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgencia()
    {
        return this._has_cdAgencia;
    } //-- boolean hasCdAgencia() 

    /**
     * Method hasCdAgenciaBeneficiario
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgenciaBeneficiario()
    {
        return this._has_cdAgenciaBeneficiario;
    } //-- boolean hasCdAgenciaBeneficiario() 

    /**
     * Method hasCdAgendadosPagosNaoPagos
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgendadosPagosNaoPagos()
    {
        return this._has_cdAgendadosPagosNaoPagos;
    } //-- boolean hasCdAgendadosPagosNaoPagos() 

    /**
     * Method hasCdBanco
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdBanco()
    {
        return this._has_cdBanco;
    } //-- boolean hasCdBanco() 

    /**
     * Method hasCdBancoBeneficiario
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdBancoBeneficiario()
    {
        return this._has_cdBancoBeneficiario;
    } //-- boolean hasCdBancoBeneficiario() 

    /**
     * Method hasCdBeneficio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdBeneficio()
    {
        return this._has_cdBeneficio;
    } //-- boolean hasCdBeneficio() 

    /**
     * Method hasCdClassificacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdClassificacao()
    {
        return this._has_cdClassificacao;
    } //-- boolean hasCdClassificacao() 

    /**
     * Method hasCdConta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdConta()
    {
        return this._has_cdConta;
    } //-- boolean hasCdConta() 

    /**
     * Method hasCdContaBeneficiario
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdContaBeneficiario()
    {
        return this._has_cdContaBeneficiario;
    } //-- boolean hasCdContaBeneficiario() 

    /**
     * Method hasCdControleCnpjParticipante
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdControleCnpjParticipante()
    {
        return this._has_cdControleCnpjParticipante;
    } //-- boolean hasCdControleCnpjParticipante() 

    /**
     * Method hasCdCpfCnpjParticipante
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCpfCnpjParticipante()
    {
        return this._has_cdCpfCnpjParticipante;
    } //-- boolean hasCdCpfCnpjParticipante() 

    /**
     * Method hasCdEmpresaContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdEmpresaContrato()
    {
        return this._has_cdEmpresaContrato;
    } //-- boolean hasCdEmpresaContrato() 

    /**
     * Method hasCdEspecieBeneficioInss
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdEspecieBeneficioInss()
    {
        return this._has_cdEspecieBeneficioInss;
    } //-- boolean hasCdEspecieBeneficioInss() 

    /**
     * Method hasCdFavorecidoClientePagador
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFavorecidoClientePagador()
    {
        return this._has_cdFavorecidoClientePagador;
    } //-- boolean hasCdFavorecidoClientePagador() 

    /**
     * Method hasCdFilialCnpjParticipante
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFilialCnpjParticipante()
    {
        return this._has_cdFilialCnpjParticipante;
    } //-- boolean hasCdFilialCnpjParticipante() 

    /**
     * Method hasCdIdentificacaoInscricaoFavorecido
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIdentificacaoInscricaoFavorecido()
    {
        return this._has_cdIdentificacaoInscricaoFavorecido;
    } //-- boolean hasCdIdentificacaoInscricaoFavorecido() 

    /**
     * Method hasCdIndiceSimulaPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndiceSimulaPagamento()
    {
        return this._has_cdIndiceSimulaPagamento;
    } //-- boolean hasCdIndiceSimulaPagamento() 

    /**
     * Method hasCdInscricaoFavorecido
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdInscricaoFavorecido()
    {
        return this._has_cdInscricaoFavorecido;
    } //-- boolean hasCdInscricaoFavorecido() 

    /**
     * Method hasCdMotivoSituacaoPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMotivoSituacaoPagamento()
    {
        return this._has_cdMotivoSituacaoPagamento;
    } //-- boolean hasCdMotivoSituacaoPagamento() 

    /**
     * Method hasCdOrigemRecebidoEfetivacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdOrigemRecebidoEfetivacao()
    {
        return this._has_cdOrigemRecebidoEfetivacao;
    } //-- boolean hasCdOrigemRecebidoEfetivacao() 

    /**
     * Method hasCdParticipante
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdParticipante()
    {
        return this._has_cdParticipante;
    } //-- boolean hasCdParticipante() 

    /**
     * Method hasCdPessoaContratoCredt
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaContratoCredt()
    {
        return this._has_cdPessoaContratoCredt;
    } //-- boolean hasCdPessoaContratoCredt() 

    /**
     * Method hasCdPessoaContratoDebito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaContratoDebito()
    {
        return this._has_cdPessoaContratoDebito;
    } //-- boolean hasCdPessoaContratoDebito() 

    /**
     * Method hasCdPessoaJuridicaContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridicaContrato()
    {
        return this._has_cdPessoaJuridicaContrato;
    } //-- boolean hasCdPessoaJuridicaContrato() 

    /**
     * Method hasCdProdutoServicoOperacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdutoServicoOperacao()
    {
        return this._has_cdProdutoServicoOperacao;
    } //-- boolean hasCdProdutoServicoOperacao() 

    /**
     * Method hasCdProdutoServicoRelacionado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdutoServicoRelacionado()
    {
        return this._has_cdProdutoServicoRelacionado;
    } //-- boolean hasCdProdutoServicoRelacionado() 

    /**
     * Method hasCdSituacaoOperacaoPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSituacaoOperacaoPagamento()
    {
        return this._has_cdSituacaoOperacaoPagamento;
    } //-- boolean hasCdSituacaoOperacaoPagamento() 

    /**
     * Method hasCdTipoAntecipacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoAntecipacao()
    {
        return this._has_cdTipoAntecipacao;
    } //-- boolean hasCdTipoAntecipacao() 

    /**
     * Method hasCdTipoConta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoConta()
    {
        return this._has_cdTipoConta;
    } //-- boolean hasCdTipoConta() 

    /**
     * Method hasCdTipoContratoCredt
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoCredt()
    {
        return this._has_cdTipoContratoCredt;
    } //-- boolean hasCdTipoContratoCredt() 

    /**
     * Method hasCdTipoContratoDebito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoDebito()
    {
        return this._has_cdTipoContratoDebito;
    } //-- boolean hasCdTipoContratoDebito() 

    /**
     * Method hasCdTipoContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoNegocio()
    {
        return this._has_cdTipoContratoNegocio;
    } //-- boolean hasCdTipoContratoNegocio() 

    /**
     * Method hasCdTipoPesquisa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoPesquisa()
    {
        return this._has_cdTipoPesquisa;
    } //-- boolean hasCdTipoPesquisa() 

    /**
     * Method hasCdTituloPgtoRastreado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTituloPgtoRastreado()
    {
        return this._has_cdTituloPgtoRastreado;
    } //-- boolean hasCdTituloPgtoRastreado() 

    /**
     * Method hasNrArquivoRemssaPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrArquivoRemssaPagamento()
    {
        return this._has_nrArquivoRemssaPagamento;
    } //-- boolean hasNrArquivoRemssaPagamento() 

    /**
     * Method hasNrContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrContrato()
    {
        return this._has_nrContrato;
    } //-- boolean hasNrContrato() 

    /**
     * Method hasNrOcorrencias
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrOcorrencias()
    {
        return this._has_nrOcorrencias;
    } //-- boolean hasNrOcorrencias() 

    /**
     * Method hasNrSequenciaContratoCredt
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaContratoCredt()
    {
        return this._has_nrSequenciaContratoCredt;
    } //-- boolean hasNrSequenciaContratoCredt() 

    /**
     * Method hasNrSequenciaContratoDebito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaContratoDebito()
    {
        return this._has_nrSequenciaContratoDebito;
    } //-- boolean hasNrSequenciaContratoDebito() 

    /**
     * Method hasNrSequenciaContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaContratoNegocio()
    {
        return this._has_nrSequenciaContratoNegocio;
    } //-- boolean hasNrSequenciaContratoNegocio() 

    /**
     * Method hasNrUnidadeOrganizacional
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrUnidadeOrganizacional()
    {
        return this._has_nrUnidadeOrganizacional;
    } //-- boolean hasNrUnidadeOrganizacional() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdAgencia'.
     * 
     * @param cdAgencia the value of field 'cdAgencia'.
     */
    public void setCdAgencia(int cdAgencia)
    {
        this._cdAgencia = cdAgencia;
        this._has_cdAgencia = true;
    } //-- void setCdAgencia(int) 

    /**
     * Sets the value of field 'cdAgenciaBeneficiario'.
     * 
     * @param cdAgenciaBeneficiario the value of field
     * 'cdAgenciaBeneficiario'.
     */
    public void setCdAgenciaBeneficiario(int cdAgenciaBeneficiario)
    {
        this._cdAgenciaBeneficiario = cdAgenciaBeneficiario;
        this._has_cdAgenciaBeneficiario = true;
    } //-- void setCdAgenciaBeneficiario(int) 

    /**
     * Sets the value of field 'cdAgendadosPagosNaoPagos'.
     * 
     * @param cdAgendadosPagosNaoPagos the value of field
     * 'cdAgendadosPagosNaoPagos'.
     */
    public void setCdAgendadosPagosNaoPagos(int cdAgendadosPagosNaoPagos)
    {
        this._cdAgendadosPagosNaoPagos = cdAgendadosPagosNaoPagos;
        this._has_cdAgendadosPagosNaoPagos = true;
    } //-- void setCdAgendadosPagosNaoPagos(int) 

    /**
     * Sets the value of field 'cdBanco'.
     * 
     * @param cdBanco the value of field 'cdBanco'.
     */
    public void setCdBanco(int cdBanco)
    {
        this._cdBanco = cdBanco;
        this._has_cdBanco = true;
    } //-- void setCdBanco(int) 

    /**
     * Sets the value of field 'cdBancoBeneficiario'.
     * 
     * @param cdBancoBeneficiario the value of field
     * 'cdBancoBeneficiario'.
     */
    public void setCdBancoBeneficiario(int cdBancoBeneficiario)
    {
        this._cdBancoBeneficiario = cdBancoBeneficiario;
        this._has_cdBancoBeneficiario = true;
    } //-- void setCdBancoBeneficiario(int) 

    /**
     * Sets the value of field 'cdBarraDocumento'.
     * 
     * @param cdBarraDocumento the value of field 'cdBarraDocumento'
     */
    public void setCdBarraDocumento(java.lang.String cdBarraDocumento)
    {
        this._cdBarraDocumento = cdBarraDocumento;
    } //-- void setCdBarraDocumento(java.lang.String) 

    /**
     * Sets the value of field 'cdBeneficio'.
     * 
     * @param cdBeneficio the value of field 'cdBeneficio'.
     */
    public void setCdBeneficio(long cdBeneficio)
    {
        this._cdBeneficio = cdBeneficio;
        this._has_cdBeneficio = true;
    } //-- void setCdBeneficio(long) 

    /**
     * Sets the value of field 'cdClassificacao'.
     * 
     * @param cdClassificacao the value of field 'cdClassificacao'.
     */
    public void setCdClassificacao(int cdClassificacao)
    {
        this._cdClassificacao = cdClassificacao;
        this._has_cdClassificacao = true;
    } //-- void setCdClassificacao(int) 

    /**
     * Sets the value of field 'cdConta'.
     * 
     * @param cdConta the value of field 'cdConta'.
     */
    public void setCdConta(long cdConta)
    {
        this._cdConta = cdConta;
        this._has_cdConta = true;
    } //-- void setCdConta(long) 

    /**
     * Sets the value of field 'cdContaBeneficiario'.
     * 
     * @param cdContaBeneficiario the value of field
     * 'cdContaBeneficiario'.
     */
    public void setCdContaBeneficiario(long cdContaBeneficiario)
    {
        this._cdContaBeneficiario = cdContaBeneficiario;
        this._has_cdContaBeneficiario = true;
    } //-- void setCdContaBeneficiario(long) 

    /**
     * Sets the value of field 'cdControleCnpjParticipante'.
     * 
     * @param cdControleCnpjParticipante the value of field
     * 'cdControleCnpjParticipante'.
     */
    public void setCdControleCnpjParticipante(int cdControleCnpjParticipante)
    {
        this._cdControleCnpjParticipante = cdControleCnpjParticipante;
        this._has_cdControleCnpjParticipante = true;
    } //-- void setCdControleCnpjParticipante(int) 

    /**
     * Sets the value of field 'cdControlePagamentoAte'.
     * 
     * @param cdControlePagamentoAte the value of field
     * 'cdControlePagamentoAte'.
     */
    public void setCdControlePagamentoAte(java.lang.String cdControlePagamentoAte)
    {
        this._cdControlePagamentoAte = cdControlePagamentoAte;
    } //-- void setCdControlePagamentoAte(java.lang.String) 

    /**
     * Sets the value of field 'cdControlePagamentoDe'.
     * 
     * @param cdControlePagamentoDe the value of field
     * 'cdControlePagamentoDe'.
     */
    public void setCdControlePagamentoDe(java.lang.String cdControlePagamentoDe)
    {
        this._cdControlePagamentoDe = cdControlePagamentoDe;
    } //-- void setCdControlePagamentoDe(java.lang.String) 

    /**
     * Sets the value of field 'cdCpfCnpjParticipante'.
     * 
     * @param cdCpfCnpjParticipante the value of field
     * 'cdCpfCnpjParticipante'.
     */
    public void setCdCpfCnpjParticipante(long cdCpfCnpjParticipante)
    {
        this._cdCpfCnpjParticipante = cdCpfCnpjParticipante;
        this._has_cdCpfCnpjParticipante = true;
    } //-- void setCdCpfCnpjParticipante(long) 

    /**
     * Sets the value of field 'cdDigitoConta'.
     * 
     * @param cdDigitoConta the value of field 'cdDigitoConta'.
     */
    public void setCdDigitoConta(java.lang.String cdDigitoConta)
    {
        this._cdDigitoConta = cdDigitoConta;
    } //-- void setCdDigitoConta(java.lang.String) 

    /**
     * Sets the value of field 'cdDigitoContaBeneficiario'.
     * 
     * @param cdDigitoContaBeneficiario the value of field
     * 'cdDigitoContaBeneficiario'.
     */
    public void setCdDigitoContaBeneficiario(java.lang.String cdDigitoContaBeneficiario)
    {
        this._cdDigitoContaBeneficiario = cdDigitoContaBeneficiario;
    } //-- void setCdDigitoContaBeneficiario(java.lang.String) 

    /**
     * Sets the value of field 'cdEmpresaContrato'.
     * 
     * @param cdEmpresaContrato the value of field
     * 'cdEmpresaContrato'.
     */
    public void setCdEmpresaContrato(long cdEmpresaContrato)
    {
        this._cdEmpresaContrato = cdEmpresaContrato;
        this._has_cdEmpresaContrato = true;
    } //-- void setCdEmpresaContrato(long) 

    /**
     * Sets the value of field 'cdEspecieBeneficioInss'.
     * 
     * @param cdEspecieBeneficioInss the value of field
     * 'cdEspecieBeneficioInss'.
     */
    public void setCdEspecieBeneficioInss(int cdEspecieBeneficioInss)
    {
        this._cdEspecieBeneficioInss = cdEspecieBeneficioInss;
        this._has_cdEspecieBeneficioInss = true;
    } //-- void setCdEspecieBeneficioInss(int) 

    /**
     * Sets the value of field 'cdFavorecidoClientePagador'.
     * 
     * @param cdFavorecidoClientePagador the value of field
     * 'cdFavorecidoClientePagador'.
     */
    public void setCdFavorecidoClientePagador(long cdFavorecidoClientePagador)
    {
        this._cdFavorecidoClientePagador = cdFavorecidoClientePagador;
        this._has_cdFavorecidoClientePagador = true;
    } //-- void setCdFavorecidoClientePagador(long) 

    /**
     * Sets the value of field 'cdFilialCnpjParticipante'.
     * 
     * @param cdFilialCnpjParticipante the value of field
     * 'cdFilialCnpjParticipante'.
     */
    public void setCdFilialCnpjParticipante(int cdFilialCnpjParticipante)
    {
        this._cdFilialCnpjParticipante = cdFilialCnpjParticipante;
        this._has_cdFilialCnpjParticipante = true;
    } //-- void setCdFilialCnpjParticipante(int) 

    /**
     * Sets the value of field
     * 'cdIdentificacaoInscricaoFavorecido'.
     * 
     * @param cdIdentificacaoInscricaoFavorecido the value of field
     * 'cdIdentificacaoInscricaoFavorecido'.
     */
    public void setCdIdentificacaoInscricaoFavorecido(int cdIdentificacaoInscricaoFavorecido)
    {
        this._cdIdentificacaoInscricaoFavorecido = cdIdentificacaoInscricaoFavorecido;
        this._has_cdIdentificacaoInscricaoFavorecido = true;
    } //-- void setCdIdentificacaoInscricaoFavorecido(int) 

    /**
     * Sets the value of field 'cdIndiceSimulaPagamento'.
     * 
     * @param cdIndiceSimulaPagamento the value of field
     * 'cdIndiceSimulaPagamento'.
     */
    public void setCdIndiceSimulaPagamento(int cdIndiceSimulaPagamento)
    {
        this._cdIndiceSimulaPagamento = cdIndiceSimulaPagamento;
        this._has_cdIndiceSimulaPagamento = true;
    } //-- void setCdIndiceSimulaPagamento(int) 

    /**
     * Sets the value of field 'cdInscricaoFavorecido'.
     * 
     * @param cdInscricaoFavorecido the value of field
     * 'cdInscricaoFavorecido'.
     */
    public void setCdInscricaoFavorecido(long cdInscricaoFavorecido)
    {
        this._cdInscricaoFavorecido = cdInscricaoFavorecido;
        this._has_cdInscricaoFavorecido = true;
    } //-- void setCdInscricaoFavorecido(long) 

    /**
     * Sets the value of field 'cdMotivoSituacaoPagamento'.
     * 
     * @param cdMotivoSituacaoPagamento the value of field
     * 'cdMotivoSituacaoPagamento'.
     */
    public void setCdMotivoSituacaoPagamento(int cdMotivoSituacaoPagamento)
    {
        this._cdMotivoSituacaoPagamento = cdMotivoSituacaoPagamento;
        this._has_cdMotivoSituacaoPagamento = true;
    } //-- void setCdMotivoSituacaoPagamento(int) 

    /**
     * Sets the value of field 'cdOrigemRecebidoEfetivacao'.
     * 
     * @param cdOrigemRecebidoEfetivacao the value of field
     * 'cdOrigemRecebidoEfetivacao'.
     */
    public void setCdOrigemRecebidoEfetivacao(int cdOrigemRecebidoEfetivacao)
    {
        this._cdOrigemRecebidoEfetivacao = cdOrigemRecebidoEfetivacao;
        this._has_cdOrigemRecebidoEfetivacao = true;
    } //-- void setCdOrigemRecebidoEfetivacao(int) 

    /**
     * Sets the value of field 'cdParticipante'.
     * 
     * @param cdParticipante the value of field 'cdParticipante'.
     */
    public void setCdParticipante(long cdParticipante)
    {
        this._cdParticipante = cdParticipante;
        this._has_cdParticipante = true;
    } //-- void setCdParticipante(long) 

    /**
     * Sets the value of field 'cdPessoaContratoCredt'.
     * 
     * @param cdPessoaContratoCredt the value of field
     * 'cdPessoaContratoCredt'.
     */
    public void setCdPessoaContratoCredt(long cdPessoaContratoCredt)
    {
        this._cdPessoaContratoCredt = cdPessoaContratoCredt;
        this._has_cdPessoaContratoCredt = true;
    } //-- void setCdPessoaContratoCredt(long) 

    /**
     * Sets the value of field 'cdPessoaContratoDebito'.
     * 
     * @param cdPessoaContratoDebito the value of field
     * 'cdPessoaContratoDebito'.
     */
    public void setCdPessoaContratoDebito(long cdPessoaContratoDebito)
    {
        this._cdPessoaContratoDebito = cdPessoaContratoDebito;
        this._has_cdPessoaContratoDebito = true;
    } //-- void setCdPessoaContratoDebito(long) 

    /**
     * Sets the value of field 'cdPessoaJuridicaContrato'.
     * 
     * @param cdPessoaJuridicaContrato the value of field
     * 'cdPessoaJuridicaContrato'.
     */
    public void setCdPessoaJuridicaContrato(long cdPessoaJuridicaContrato)
    {
        this._cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
        this._has_cdPessoaJuridicaContrato = true;
    } //-- void setCdPessoaJuridicaContrato(long) 

    /**
     * Sets the value of field 'cdProdutoServicoOperacao'.
     * 
     * @param cdProdutoServicoOperacao the value of field
     * 'cdProdutoServicoOperacao'.
     */
    public void setCdProdutoServicoOperacao(int cdProdutoServicoOperacao)
    {
        this._cdProdutoServicoOperacao = cdProdutoServicoOperacao;
        this._has_cdProdutoServicoOperacao = true;
    } //-- void setCdProdutoServicoOperacao(int) 

    /**
     * Sets the value of field 'cdProdutoServicoRelacionado'.
     * 
     * @param cdProdutoServicoRelacionado the value of field
     * 'cdProdutoServicoRelacionado'.
     */
    public void setCdProdutoServicoRelacionado(int cdProdutoServicoRelacionado)
    {
        this._cdProdutoServicoRelacionado = cdProdutoServicoRelacionado;
        this._has_cdProdutoServicoRelacionado = true;
    } //-- void setCdProdutoServicoRelacionado(int) 

    /**
     * Sets the value of field 'cdSituacaoOperacaoPagamento'.
     * 
     * @param cdSituacaoOperacaoPagamento the value of field
     * 'cdSituacaoOperacaoPagamento'.
     */
    public void setCdSituacaoOperacaoPagamento(int cdSituacaoOperacaoPagamento)
    {
        this._cdSituacaoOperacaoPagamento = cdSituacaoOperacaoPagamento;
        this._has_cdSituacaoOperacaoPagamento = true;
    } //-- void setCdSituacaoOperacaoPagamento(int) 

    /**
     * Sets the value of field 'cdTipoAntecipacao'.
     * 
     * @param cdTipoAntecipacao the value of field
     * 'cdTipoAntecipacao'.
     */
    public void setCdTipoAntecipacao(int cdTipoAntecipacao)
    {
        this._cdTipoAntecipacao = cdTipoAntecipacao;
        this._has_cdTipoAntecipacao = true;
    } //-- void setCdTipoAntecipacao(int) 

    /**
     * Sets the value of field 'cdTipoConta'.
     * 
     * @param cdTipoConta the value of field 'cdTipoConta'.
     */
    public void setCdTipoConta(int cdTipoConta)
    {
        this._cdTipoConta = cdTipoConta;
        this._has_cdTipoConta = true;
    } //-- void setCdTipoConta(int) 

    /**
     * Sets the value of field 'cdTipoContratoCredt'.
     * 
     * @param cdTipoContratoCredt the value of field
     * 'cdTipoContratoCredt'.
     */
    public void setCdTipoContratoCredt(int cdTipoContratoCredt)
    {
        this._cdTipoContratoCredt = cdTipoContratoCredt;
        this._has_cdTipoContratoCredt = true;
    } //-- void setCdTipoContratoCredt(int) 

    /**
     * Sets the value of field 'cdTipoContratoDebito'.
     * 
     * @param cdTipoContratoDebito the value of field
     * 'cdTipoContratoDebito'.
     */
    public void setCdTipoContratoDebito(int cdTipoContratoDebito)
    {
        this._cdTipoContratoDebito = cdTipoContratoDebito;
        this._has_cdTipoContratoDebito = true;
    } //-- void setCdTipoContratoDebito(int) 

    /**
     * Sets the value of field 'cdTipoContratoNegocio'.
     * 
     * @param cdTipoContratoNegocio the value of field
     * 'cdTipoContratoNegocio'.
     */
    public void setCdTipoContratoNegocio(int cdTipoContratoNegocio)
    {
        this._cdTipoContratoNegocio = cdTipoContratoNegocio;
        this._has_cdTipoContratoNegocio = true;
    } //-- void setCdTipoContratoNegocio(int) 

    /**
     * Sets the value of field 'cdTipoPesquisa'.
     * 
     * @param cdTipoPesquisa the value of field 'cdTipoPesquisa'.
     */
    public void setCdTipoPesquisa(int cdTipoPesquisa)
    {
        this._cdTipoPesquisa = cdTipoPesquisa;
        this._has_cdTipoPesquisa = true;
    } //-- void setCdTipoPesquisa(int) 

    /**
     * Sets the value of field 'cdTituloPgtoRastreado'.
     * 
     * @param cdTituloPgtoRastreado the value of field
     * 'cdTituloPgtoRastreado'.
     */
    public void setCdTituloPgtoRastreado(int cdTituloPgtoRastreado)
    {
        this._cdTituloPgtoRastreado = cdTituloPgtoRastreado;
        this._has_cdTituloPgtoRastreado = true;
    } //-- void setCdTituloPgtoRastreado(int) 

    /**
     * Sets the value of field 'dtCreditoPagamentoFim'.
     * 
     * @param dtCreditoPagamentoFim the value of field
     * 'dtCreditoPagamentoFim'.
     */
    public void setDtCreditoPagamentoFim(java.lang.String dtCreditoPagamentoFim)
    {
        this._dtCreditoPagamentoFim = dtCreditoPagamentoFim;
    } //-- void setDtCreditoPagamentoFim(java.lang.String) 

    /**
     * Sets the value of field 'dtCreditoPagamentoInicio'.
     * 
     * @param dtCreditoPagamentoInicio the value of field
     * 'dtCreditoPagamentoInicio'.
     */
    public void setDtCreditoPagamentoInicio(java.lang.String dtCreditoPagamentoInicio)
    {
        this._dtCreditoPagamentoInicio = dtCreditoPagamentoInicio;
    } //-- void setDtCreditoPagamentoInicio(java.lang.String) 

    /**
     * Sets the value of field 'nrArquivoRemssaPagamento'.
     * 
     * @param nrArquivoRemssaPagamento the value of field
     * 'nrArquivoRemssaPagamento'.
     */
    public void setNrArquivoRemssaPagamento(long nrArquivoRemssaPagamento)
    {
        this._nrArquivoRemssaPagamento = nrArquivoRemssaPagamento;
        this._has_nrArquivoRemssaPagamento = true;
    } //-- void setNrArquivoRemssaPagamento(long) 

    /**
     * Sets the value of field 'nrContrato'.
     * 
     * @param nrContrato the value of field 'nrContrato'.
     */
    public void setNrContrato(long nrContrato)
    {
        this._nrContrato = nrContrato;
        this._has_nrContrato = true;
    } //-- void setNrContrato(long) 

    /**
     * Sets the value of field 'nrOcorrencias'.
     * 
     * @param nrOcorrencias the value of field 'nrOcorrencias'.
     */
    public void setNrOcorrencias(int nrOcorrencias)
    {
        this._nrOcorrencias = nrOcorrencias;
        this._has_nrOcorrencias = true;
    } //-- void setNrOcorrencias(int) 

    /**
     * Sets the value of field 'nrSequenciaContratoCredt'.
     * 
     * @param nrSequenciaContratoCredt the value of field
     * 'nrSequenciaContratoCredt'.
     */
    public void setNrSequenciaContratoCredt(long nrSequenciaContratoCredt)
    {
        this._nrSequenciaContratoCredt = nrSequenciaContratoCredt;
        this._has_nrSequenciaContratoCredt = true;
    } //-- void setNrSequenciaContratoCredt(long) 

    /**
     * Sets the value of field 'nrSequenciaContratoDebito'.
     * 
     * @param nrSequenciaContratoDebito the value of field
     * 'nrSequenciaContratoDebito'.
     */
    public void setNrSequenciaContratoDebito(long nrSequenciaContratoDebito)
    {
        this._nrSequenciaContratoDebito = nrSequenciaContratoDebito;
        this._has_nrSequenciaContratoDebito = true;
    } //-- void setNrSequenciaContratoDebito(long) 

    /**
     * Sets the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @param nrSequenciaContratoNegocio the value of field
     * 'nrSequenciaContratoNegocio'.
     */
    public void setNrSequenciaContratoNegocio(long nrSequenciaContratoNegocio)
    {
        this._nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
        this._has_nrSequenciaContratoNegocio = true;
    } //-- void setNrSequenciaContratoNegocio(long) 

    /**
     * Sets the value of field 'nrUnidadeOrganizacional'.
     * 
     * @param nrUnidadeOrganizacional the value of field
     * 'nrUnidadeOrganizacional'.
     */
    public void setNrUnidadeOrganizacional(int nrUnidadeOrganizacional)
    {
        this._nrUnidadeOrganizacional = nrUnidadeOrganizacional;
        this._has_nrUnidadeOrganizacional = true;
    } //-- void setNrUnidadeOrganizacional(int) 

    /**
     * Sets the value of field 'vlPagamentoAte'.
     * 
     * @param vlPagamentoAte the value of field 'vlPagamentoAte'.
     */
    public void setVlPagamentoAte(java.math.BigDecimal vlPagamentoAte)
    {
        this._vlPagamentoAte = vlPagamentoAte;
    } //-- void setVlPagamentoAte(java.math.BigDecimal) 

    /**
     * Sets the value of field 'vlPagamentoDe'.
     * 
     * @param vlPagamentoDe the value of field 'vlPagamentoDe'.
     */
    public void setVlPagamentoDe(java.math.BigDecimal vlPagamentoDe)
    {
        this._vlPagamentoDe = vlPagamentoDe;
    } //-- void setVlPagamentoDe(java.math.BigDecimal) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ConsultarPagtosIndAntPostergacaoRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultarpagtosindantpostergacao.request.ConsultarPagtosIndAntPostergacaoRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultarpagtosindantpostergacao.request.ConsultarPagtosIndAntPostergacaoRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultarpagtosindantpostergacao.request.ConsultarPagtosIndAntPostergacaoRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarpagtosindantpostergacao.request.ConsultarPagtosIndAntPostergacaoRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
