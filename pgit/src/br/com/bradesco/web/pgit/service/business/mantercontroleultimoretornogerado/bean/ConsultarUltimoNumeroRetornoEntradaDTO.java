/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantercontroleultimoretornogerado.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantercontroleultimoretornogerado.bean;

/**
 * Nome: ConsultarUltimoNumeroRetornoEntradaDTO
 * <p>
 * Prop�sito:
 * </p>
 * .
 * 
 * @author : todo!
 * @version :
 */
public class ConsultarUltimoNumeroRetornoEntradaDTO {

	private Integer cdTipoLayoutArquivo;

	private Long cdPerfilTrocaArquivo;

	private Integer cdTipoLoteLayout;

	private Long cdClub;

	public Integer getCdTipoLayoutArquivo() {
		return cdTipoLayoutArquivo;
	}

	public void setCdTipoLayoutArquivo(Integer cdTipoLayoutArquivo) {
		this.cdTipoLayoutArquivo = cdTipoLayoutArquivo;
	}

	public Long getCdPerfilTrocaArquivo() {
		return cdPerfilTrocaArquivo;
	}

	public void setCdPerfilTrocaArquivo(Long cdPerfilTrocaArquivo) {
		this.cdPerfilTrocaArquivo = cdPerfilTrocaArquivo;
	}

	public Integer getCdTipoLoteLayout() {
		return cdTipoLoteLayout;
	}

	public void setCdTipoLoteLayout(Integer cdTipoLoteLayout) {
		this.cdTipoLoteLayout = cdTipoLoteLayout;
	}

	public Long getCdClub() {
		return cdClub;
	}

	public void setCdClub(Long cdClub) {
		this.cdClub = cdClub;
	}

}
