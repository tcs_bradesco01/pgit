/*
 * Nome: br.com.bradesco.web.pgit.service.business.cadproccontrole.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.cadproccontrole.bean;

import java.io.Serializable;
import java.util.Date;

/**
 * Nome: EstatisticaProcessamentoEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class EstatisticaProcessamentoEntradaDTO implements Serializable {

	/** Atributo serialVersionUID. */
	private static final long serialVersionUID = 5854027952729062854L;

	/** Atributo cdSistema. */
	private String cdSistema;

    /** Atributo cdTipoProcessoSistema. */
    private Integer cdTipoProcessoSistema;

    /** Atributo dtInicioEstatisticaProcessoInicio. */
    private Date dtInicioEstatisticaProcessoInicio;

    /** Atributo hrInicioEstatisticaProcessoInicio. */
    private String hrInicioEstatisticaProcessoInicio;
    
    /** Atributo hrInicioEstatisticaProcessoFim. */
    private String hrInicioEstatisticaProcessoFim;

    /** Atributo dtInicioEstatisticaProcessoFim. */
    private Date dtInicioEstatisticaProcessoFim;

    /** Atributo cdSituacaoEstatisticaSistema. */
    private Integer cdSituacaoEstatisticaSistema;

	/**
	 * Get: cdSistema.
	 *
	 * @return cdSistema
	 */
	public String getCdSistema() {
		return cdSistema;
	}

	/**
	 * Set: cdSistema.
	 *
	 * @param cdSistema the cd sistema
	 */
	public void setCdSistema(String cdSistema) {
		this.cdSistema = cdSistema;
	}

	/**
	 * Get: cdSituacaoEstatisticaSistema.
	 *
	 * @return cdSituacaoEstatisticaSistema
	 */
	public Integer getCdSituacaoEstatisticaSistema() {
		return cdSituacaoEstatisticaSistema;
	}

	/**
	 * Set: cdSituacaoEstatisticaSistema.
	 *
	 * @param cdSituacaoEstatisticaSistema the cd situacao estatistica sistema
	 */
	public void setCdSituacaoEstatisticaSistema(Integer cdSituacaoEstatisticaSistema) {
		this.cdSituacaoEstatisticaSistema = cdSituacaoEstatisticaSistema;
	}

	/**
	 * Get: cdTipoProcessoSistema.
	 *
	 * @return cdTipoProcessoSistema
	 */
	public Integer getCdTipoProcessoSistema() {
		return cdTipoProcessoSistema;
	}

	/**
	 * Set: cdTipoProcessoSistema.
	 *
	 * @param cdTipoProcessoSistema the cd tipo processo sistema
	 */
	public void setCdTipoProcessoSistema(Integer cdTipoProcessoSistema) {
		this.cdTipoProcessoSistema = cdTipoProcessoSistema;
	}

	/**
	 * Get: dtInicioEstatisticaProcessoFim.
	 *
	 * @return dtInicioEstatisticaProcessoFim
	 */
	public Date getDtInicioEstatisticaProcessoFim() {
		return dtInicioEstatisticaProcessoFim;
	}

	/**
	 * Set: dtInicioEstatisticaProcessoFim.
	 *
	 * @param dtInicioEstatisticaProcessoFim the dt inicio estatistica processo fim
	 */
	public void setDtInicioEstatisticaProcessoFim(
			Date dtInicioEstatisticaProcessoFim) {
		this.dtInicioEstatisticaProcessoFim = dtInicioEstatisticaProcessoFim;
	}

	/**
	 * Get: dtInicioEstatisticaProcessoInicio.
	 *
	 * @return dtInicioEstatisticaProcessoInicio
	 */
	public Date getDtInicioEstatisticaProcessoInicio() {
		return dtInicioEstatisticaProcessoInicio;
	}

	/**
	 * Set: dtInicioEstatisticaProcessoInicio.
	 *
	 * @param dtInicioEstatisticaProcessoInicio the dt inicio estatistica processo inicio
	 */
	public void setDtInicioEstatisticaProcessoInicio(
			Date dtInicioEstatisticaProcessoInicio) {
		this.dtInicioEstatisticaProcessoInicio = dtInicioEstatisticaProcessoInicio;
	}

	/**
	 * Get: hrInicioEstatisticaProcessoInicio.
	 *
	 * @return hrInicioEstatisticaProcessoInicio
	 */
	public String getHrInicioEstatisticaProcessoInicio() {
		return hrInicioEstatisticaProcessoInicio;
	}

	/**
	 * Set: hrInicioEstatisticaProcessoInicio.
	 *
	 * @param hrInicioEstatisticaProcessoInicio the hr inicio estatistica processo inicio
	 */
	public void setHrInicioEstatisticaProcessoInicio(
			String hrInicioEstatisticaProcessoInicio) {
		this.hrInicioEstatisticaProcessoInicio = hrInicioEstatisticaProcessoInicio;
	}

	/**
	 * Get: hrInicioEstatisticaProcessoFim.
	 *
	 * @return hrInicioEstatisticaProcessoFim
	 */
	public String getHrInicioEstatisticaProcessoFim() {
		return hrInicioEstatisticaProcessoFim;
	}

	/**
	 * Set: hrInicioEstatisticaProcessoFim.
	 *
	 * @param hrInicioEstatisticaProcessoFim the hr inicio estatistica processo fim
	 */
	public void setHrInicioEstatisticaProcessoFim(
			String hrInicioEstatisticaProcessoFim) {
		this.hrInicioEstatisticaProcessoFim = hrInicioEstatisticaProcessoFim;
	}

}
