/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/implementation.ftl,v $
 * $Id: implementation.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: implementation.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */
 
package br.com.bradesco.web.pgit.service.business.assoclayoutarqprodserv.impl;

import java.util.ArrayList;
import java.util.List;

import br.com.bradesco.web.pgit.service.business.assoclayoutarqprodserv.IAssocLayoutArqProdServService;
import br.com.bradesco.web.pgit.service.business.assoclayoutarqprodserv.IAssocLayoutArqProdServServiceConstants;
import br.com.bradesco.web.pgit.service.business.assoclayoutarqprodserv.bean.AlterarArquivoServicoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.assoclayoutarqprodserv.bean.AlterarArquivoServicoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.assoclayoutarqprodserv.bean.DetalharAssLayoutArqProdServicoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.assoclayoutarqprodserv.bean.DetalharAssLayoutArqProdServicoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.assoclayoutarqprodserv.bean.ExcluirAssLayoutArqProdServicoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.assoclayoutarqprodserv.bean.ExcluirAssLayoutArqProdServicoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.assoclayoutarqprodserv.bean.IncluirAssLayoutArqProdServicoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.assoclayoutarqprodserv.bean.IncluirAssLayoutArqProdServicoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.assoclayoutarqprodserv.bean.ListarAssLayoutArqProdServicoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.assoclayoutarqprodserv.bean.ListarAssLayoutArqProdServicoSaidaDTO;
import br.com.bradesco.web.pgit.service.data.pdc.FactoryAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.alterararquivoservico.request.AlterarArquivoServicoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.alterararquivoservico.response.AlterarArquivoServicoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.detalharlayoutarqservico.request.DetalharLayoutArqServicoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.detalharlayoutarqservico.response.DetalharLayoutArqServicoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.excluirlayoutarqservico.request.ExcluirLayoutArqServicoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.excluirlayoutarqservico.response.ExcluirLayoutArqServicoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.incluirlayoutarqservico.request.IncluirLayoutArqServicoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.incluirlayoutarqservico.response.IncluirLayoutArqServicoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarlayoutarqservico.request.ListarLayoutArqServicoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarlayoutarqservico.response.ListarLayoutArqServicoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarlayoutarqservico.response.Ocorrencias;
import br.com.bradesco.web.pgit.utils.PgitUtil;


/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Implementa��o do adaptador: AssocLayoutArqProdServ
 * </p>
 * 
 * @comment C�DIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public class AssocLayoutArqProdServServiceImpl implements IAssocLayoutArqProdServService {

	/** Atributo factoryAdapter. */
	private FactoryAdapter factoryAdapter;
	
    /**
     * Get: factoryAdapter.
     *
     * @return factoryAdapter
     */
    public FactoryAdapter getFactoryAdapter() {
		return factoryAdapter;
	}

	/**
	 * Set: factoryAdapter.
	 *
	 * @param factoryAdapter the factory adapter
	 */
	public void setFactoryAdapter(FactoryAdapter factoryAdapter) {
		this.factoryAdapter = factoryAdapter;
	}

	/**
     * Construtor.
     */
    public AssocLayoutArqProdServServiceImpl() {
        // TODO: Implementa��o
    }

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.assoclayoutarqprodserv.IAssocLayoutArqProdServService#listarAssocicaoLayoutArqProdutoServico(br.com.bradesco.web.pgit.service.business.assoclayoutarqprodserv.bean.ListarAssLayoutArqProdServicoEntradaDTO)
	 */
	public List<ListarAssLayoutArqProdServicoSaidaDTO> listarAssocicaoLayoutArqProdutoServico(ListarAssLayoutArqProdServicoEntradaDTO entrada) {
		List<ListarAssLayoutArqProdServicoSaidaDTO> listaRetorno = new ArrayList<ListarAssLayoutArqProdServicoSaidaDTO>();		
		ListarLayoutArqServicoRequest request = new ListarLayoutArqServicoRequest();
		ListarLayoutArqServicoResponse response = new ListarLayoutArqServicoResponse();

		request.setCdProdutoServicoOperacao(PgitUtil.verificaIntegerNulo(entrada.getTipoServico()));
		request.setCdTipoLayoutArquivo(PgitUtil.verificaIntegerNulo(entrada.getTipoLayoutArquivo()));
		request.setCdIdentificadorLayoutNegocio(PgitUtil.verificaIntegerNulo(entrada.getCdIdentificadorLayoutNegocio()));
		request.setQtConsultas(IAssocLayoutArqProdServServiceConstants.NUMERO_OCORRENCIAS_LISTA);

		response = getFactoryAdapter().getListarLayoutArqServicoPDCAdapter().invokeProcess(request);

		ListarAssLayoutArqProdServicoSaidaDTO saidaDTO;
		for (int i=0; i < response.getOcorrenciasCount();i++){
			Ocorrencias occurs = response.getOcorrencias(i);

			saidaDTO = new ListarAssLayoutArqProdServicoSaidaDTO();
			saidaDTO.setCodMensagem(response.getCodMensagem());
			saidaDTO.setMensagem(response.getMensagem());
			saidaDTO.setCdTipoLayoutArquivo(occurs.getCdTipoLayoutArquivo());
			saidaDTO.setDsTipoLayoutArquivo(occurs.getDsTipoLayoutArquivo());
			saidaDTO.setCdTipoServico(occurs.getCdProdutoServicoOperacao());
			saidaDTO.setDsTipoServico(occurs.getDsProsutoServicoOperacao());
			saidaDTO.setCdIdentificadorLayoutNegocio(occurs.getCdIdentificadorLayoutNegocio());
			saidaDTO.setDsIdentificadorLayoutNegocio(occurs.getDsIdentificadorLayoutNegocio());

			listaRetorno.add(saidaDTO);
		}

		return listaRetorno;
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.assoclayoutarqprodserv.IAssocLayoutArqProdServService#incluirAssocicaoLayoutArqProdutoServico(br.com.bradesco.web.pgit.service.business.assoclayoutarqprodserv.bean.IncluirAssLayoutArqProdServicoEntradaDTO)
	 */
	public IncluirAssLayoutArqProdServicoSaidaDTO incluirAssocicaoLayoutArqProdutoServico(IncluirAssLayoutArqProdServicoEntradaDTO entrada) {
		IncluirAssLayoutArqProdServicoSaidaDTO saidaDTO = new IncluirAssLayoutArqProdServicoSaidaDTO();
		IncluirLayoutArqServicoRequest request = new IncluirLayoutArqServicoRequest();
		IncluirLayoutArqServicoResponse response = new IncluirLayoutArqServicoResponse();

		request.setCdProdutoServicoOperacao(PgitUtil.verificaIntegerNulo(entrada.getTipoServico()));
		request.setCdTipoLayoutArquivo(PgitUtil.verificaIntegerNulo(entrada.getTipoLayoutArquivo()));
		request.setCdIdentificadorLayoutNegocio(PgitUtil.verificaIntegerNulo(entrada.getCdIdentificadorLayoutNegocio()));
		request.setCdIndicadorLayoutDefault(PgitUtil.verificaIntegerNulo(entrada.getCdIndicadorLayoutDefault()));
		

		response = getFactoryAdapter().getIncluirLayoutArqServicoPDCAdapter().invokeProcess(request);

		saidaDTO.setCodMensagem(response.getCodMensagem());
		saidaDTO.setMensagem(response.getMensagem());

		return saidaDTO; 
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.assoclayoutarqprodserv.IAssocLayoutArqProdServService#excluirAssocicaoLayoutArqProdutoServico(br.com.bradesco.web.pgit.service.business.assoclayoutarqprodserv.bean.ExcluirAssLayoutArqProdServicoEntradaDTO)
	 */
	public ExcluirAssLayoutArqProdServicoSaidaDTO excluirAssocicaoLayoutArqProdutoServico(ExcluirAssLayoutArqProdServicoEntradaDTO excluirAssLayoutArqProdServicoEntradaDTO) {
		ExcluirAssLayoutArqProdServicoSaidaDTO saidaDTO = new ExcluirAssLayoutArqProdServicoSaidaDTO();
		ExcluirLayoutArqServicoRequest request = new ExcluirLayoutArqServicoRequest();
		ExcluirLayoutArqServicoResponse response = new ExcluirLayoutArqServicoResponse();

		request.setCdProdutoServicoOperacao(excluirAssLayoutArqProdServicoEntradaDTO.getTipoServico());
		request.setCdTipoLayoutArquivo(excluirAssLayoutArqProdServicoEntradaDTO.getTipoLayoutArquivo());

		response = getFactoryAdapter().getExcluirLayoutArqServicoPDCAdapter().invokeProcess(request);

		saidaDTO.setCodMensagem(response.getCodMensagem());
		saidaDTO.setMensagem(response.getMensagem());

		return saidaDTO; 
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.assoclayoutarqprodserv.IAssocLayoutArqProdServService#detalharAssocicaoLayoutArqProdutoServico(br.com.bradesco.web.pgit.service.business.assoclayoutarqprodserv.bean.DetalharAssLayoutArqProdServicoEntradaDTO)
	 */
	public DetalharAssLayoutArqProdServicoSaidaDTO detalharAssocicaoLayoutArqProdutoServico(DetalharAssLayoutArqProdServicoEntradaDTO detalharAssLayoutArqProdServicoEntradaDTO) {
		DetalharAssLayoutArqProdServicoSaidaDTO saidaDTO = new DetalharAssLayoutArqProdServicoSaidaDTO();
		DetalharLayoutArqServicoRequest request = new DetalharLayoutArqServicoRequest();
		DetalharLayoutArqServicoResponse response = new DetalharLayoutArqServicoResponse();

		request.setCdProdutoServicoOperacao(detalharAssLayoutArqProdServicoEntradaDTO.getTipoServico());
		request.setCdTipoLayoutArquivo(detalharAssLayoutArqProdServicoEntradaDTO.getTipoLayoutArquivo());

		response = getFactoryAdapter().getDetalharLayoutArqServicoPDCAdapter().invokeProcess(request);

		saidaDTO = new DetalharAssLayoutArqProdServicoSaidaDTO();
		saidaDTO.setCodMensagem(response.getCodMensagem());
		saidaDTO.setMensagem(response.getMensagem());
		saidaDTO.setCdTipoLayoutArquivo(response.getCdTipoLayoutArquivo());
		saidaDTO.setDsTipoLayoutArquivo(response.getDsTipoLayoutArquivo());
		saidaDTO.setCdIndicadorLayoutDefault(response.getCdIndicadorLayoutDefault());
		saidaDTO.setCdTipoServico(response.getCdProdutoServicoOperacao());
		saidaDTO.setDsTipoServico(response.getDsProdutoServicoOperacao());
		saidaDTO.setCdCanalInclusao(response.getCdCanalInclusao());
		saidaDTO.setDsCanalInclusao(response.getDsCanalInclusao());
		saidaDTO.setUsuarioInclusao(response.getCdAutenticacaoSegregacaoInclusao());
		saidaDTO.setComplementoInclusao(response.getNmOperacaoFluxoInclusao());
		saidaDTO.setDataHoraInclusao(response.getHrInclusaoRegistro());
		saidaDTO.setCdCanalManutencao(response.getCdCanalManutencao());
		saidaDTO.setDsCanalManutencao(response.getDsCanalManutencao());
		saidaDTO.setUsuarioManutencao(response.getCdAutenticacaoSegregacaoManutencao());
		saidaDTO.setComplementoManutencao(response.getNmOperacaoFluxoManutencao());
		saidaDTO.setDataHoraManutencao(response.getHrManutencaoRegistro());

		saidaDTO.setCdIdentificadorLayoutNegocio(response.getCdIdentificadorLayoutNegocio());
		saidaDTO.setDsIdentificadorLayoutNegocio(response.getDsIdentificadorLayoutNegocio());

		return saidaDTO;
	}

	public AlterarArquivoServicoSaidaDTO alterarArquivoServico(
			AlterarArquivoServicoEntradaDTO entrada) {
		
		AlterarArquivoServicoRequest request = new AlterarArquivoServicoRequest();

		request.setCdTipoLayoutArquivo(PgitUtil.verificaIntegerNulo(entrada.getCdTipoLayoutArquivo()));
		request.setCdProdutoServicoOperacao(PgitUtil.verificaIntegerNulo(entrada.getCdProdutoServicoOperacao()));
		request.setCdIdentificadorLayoutNegocio(PgitUtil.verificaIntegerNulo(entrada.getCdIdentificadorLayoutNegocio()));
		request.setCdIndicadorLayoutDefault(PgitUtil.verificaIntegerNulo(entrada.getCdIndicadorLayoutDefault()));
		
		AlterarArquivoServicoResponse response = getFactoryAdapter().getAlterarArquivoServicoPDCAdapter().invokeProcess(request);
		AlterarArquivoServicoSaidaDTO saidaDTO = new AlterarArquivoServicoSaidaDTO();
		
		saidaDTO.setCodMensagem(response.getCodMensagem());
		saidaDTO.setMensagem(response.getMensagem());
		
		return saidaDTO;
	}
}