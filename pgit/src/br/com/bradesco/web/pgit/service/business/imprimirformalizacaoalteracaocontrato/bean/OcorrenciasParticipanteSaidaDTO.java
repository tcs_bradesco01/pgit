/*
 * Nome: br.com.bradesco.web.pgit.service.business.imprimirformalizacaoalteracaocontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.imprimirformalizacaoalteracaocontrato.bean;

import br.com.bradesco.web.pgit.service.data.pdc.imprimiraditivocontrato.response.OcorrenciasParticipante;

/**
 * Nome: OcorrenciasParticipanteSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class OcorrenciasParticipanteSaidaDTO {
    
    /** Atributo cdAcaoParticipante. */
    private Integer cdAcaoParticipante;
    
    /** Atributo cdCpfCnpjParticipante. */
    private String cdCpfCnpjParticipante;
    
    /** Atributo nmParticipante. */
    private String nmParticipante;
    
	/**
	 * Ocorrencias participante saida dto.
	 */
	public OcorrenciasParticipanteSaidaDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * Ocorrencias participante saida dto.
	 *
	 * @param ocorrenciasParticipante the ocorrencias participante
	 */
	public OcorrenciasParticipanteSaidaDTO(OcorrenciasParticipante ocorrenciasParticipante) {
		super();
		this.cdAcaoParticipante = ocorrenciasParticipante.getCdAcaoParticipante();
		this.cdCpfCnpjParticipante = ocorrenciasParticipante.getCdCpfCnpjParticipante();
		this.nmParticipante = ocorrenciasParticipante.getNmParticipante();
	}

	/**
	 * Get: cdAcaoParticipante.
	 *
	 * @return cdAcaoParticipante
	 */
	public Integer getCdAcaoParticipante() {
		return cdAcaoParticipante;
	}

	/**
	 * Set: cdAcaoParticipante.
	 *
	 * @param cdAcaoParticipante the cd acao participante
	 */
	public void setCdAcaoParticipante(Integer cdAcaoParticipante) {
		this.cdAcaoParticipante = cdAcaoParticipante;
	}

	/**
	 * Get: cdCpfCnpjParticipante.
	 *
	 * @return cdCpfCnpjParticipante
	 */
	public String getCdCpfCnpjParticipante() {
		return cdCpfCnpjParticipante;
	}

	/**
	 * Set: cdCpfCnpjParticipante.
	 *
	 * @param cdCpfCnpjParticipante the cd cpf cnpj participante
	 */
	public void setCdCpfCnpjParticipante(String cdCpfCnpjParticipante) {
		this.cdCpfCnpjParticipante = cdCpfCnpjParticipante;
	}

	/**
	 * Get: nmParticipante.
	 *
	 * @return nmParticipante
	 */
	public String getNmParticipante() {
		return nmParticipante;
	}

	/**
	 * Set: nmParticipante.
	 *
	 * @param nmParticipante the nm participante
	 */
	public void setNmParticipante(String nmParticipante) {
		this.nmParticipante = nmParticipante;
	}
}
