/*
 * Nome: br.com.bradesco.web.pgit.service.business.simulacaotarifasnegociacao.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.simulacaotarifasnegociacao.bean;

import java.math.BigDecimal;

/**
 * Nome: EfetuarValidacaoConfigAtribModalidadeEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class EfetuarValidacaoConfigAtribModalidadeEntradaDTO {

    private Integer cdServico;

    private Integer cdModalidade;

    private Integer cdIndicadorModalidade;

    private Integer cdTipoSaldo;

    private Integer cdTipoProcessamento;

    private Integer cdTratamentoFeriadoDataPagto;

    private Integer cdPermissaoFavorecidoConsultaPagto;

    private Integer cdTipoRejeicaoAgendamento;

    private Integer cdTipoRejeicaoEfetivacao;

    private Integer qtdeMaxRegInconsistenteLote;

    private Integer percentualMaxRegInconsistenteLote;

    private Integer cdPrioridadeDebito;

    private Integer cdGeracaoLancamentoFuturoDebito;

    private Integer cdUtilizacaoFavorecidoControlePagto;

    private BigDecimal vlrMaximoPagtoFavorecidoNaoCadastrado;

    private BigDecimal vlrLimiteIndividual;

    private BigDecimal vlrLimiteDiario;

    private Integer qtdeDiaRepique;

    private Integer qtdeDiaFloating;

    private Integer cdTipoInscricaoFavorecidoAceita;

    private Integer cdOcorrenciaDiaExpiracao;

    private Integer qtdeDiaExpiracao;

    private Integer cdGeracaoLancamentoFuturoCredito;

    private Integer cdGeracaoLancamentoProgramado;

    private Integer cdTratoContaTransf;

    private Integer cdConsistenciaCpfCnpj;

    private Integer cdTipoConsistenciaCnpjFavorecido;

    private Long cdTipoContaCredito;

    private Integer cdEfetuaConsistenciaEspBeneficiario;

    private Integer cdUtilizacaoCodigoLancamentoFixo;

    private Integer cdLancamentoFixo;

    private Integer cdPermissaoPagtoVencido;

    private Integer qtdeLimiteDiarioPagtoVencido;

    private Integer cdPermissaoPagtoMenor;

    private Integer cdTipoConsultaSaldo;

    private Integer cdTratamentoFeriado;

    private Integer cdPermissaoContingencia;

    private Integer cdTratamentoValorDivergente;

    private Integer cdTipoConsistenciaCnpjProposta;

    private Integer cdTipoComprovacaoVida;

    private Integer qtdeMesesComprovacaoVida;

    private Integer cdIndicadorEmissaoComprovanteVida;

    private Integer qtdeDiaAvisoComprovanteVidaInicial;

    private Integer qtdeDiaAvisoComprovanteVidaVencimento;

    private Integer cdUtilizacaoMensagemPersonalizada;

    private Integer cdDestino;

    private Integer cdCadastroEnderecoUtilizado;

    private Integer cdutilizacaoFormularioPersonalizado;

    private Long cdFormulario;

    private Integer cdCriterioRstrbTitulo;

    private Integer cdRastreabTituloTerc;

    private Integer cdRastreabNotaFiscal;

    private Integer cdCaptuTituloRegistro;

    private Integer cdIndicadorAgendaTitulo;

    private Integer cdAgendaRastreabilidadeFilial;

    private Integer cdBloqueioEmissaoPplta;

    private String dtInicioRastreabTitulo;

    private String dtInicioBloqueioPplta;

    private String dtInicioRegTitulo;

    private Integer cdMomentoAvisoRecadastramento;

    private Integer qtdeAntecipacaoInicioRecadastramento;

    private Integer cdAgruparCorrespondencia;

    private Integer cdAdesaoSacado;

    private Integer cdValidacaoNomeFavorecido;

    private Integer cdPermissaoDebitoOnline;

    private Integer cdLiberacaoLoteProcs;

    private Integer cdTipoSaldoValorSuperior;
    

	/**
	 * Nome: getCdServico
	 *
	 * @exception
	 * @throws
	 * @return cdServico
	 */
	public Integer getCdServico() {
		return cdServico;
	}

	/**
	 * Nome: setCdServico
	 *
	 * @exception
	 * @throws
	 * @param cdServico
	 */
	public void setCdServico(Integer cdServico) {
		this.cdServico = cdServico;
	}

	/**
	 * Nome: getCdModalidade
	 *
	 * @exception
	 * @throws
	 * @return cdModalidade
	 */
	public Integer getCdModalidade() {
		return cdModalidade;
	}

	/**
	 * Nome: setCdModalidade
	 *
	 * @exception
	 * @throws
	 * @param cdModalidade
	 */
	public void setCdModalidade(Integer cdModalidade) {
		this.cdModalidade = cdModalidade;
	}

	/**
	 * Nome: getCdIndicadorModalidade
	 *
	 * @exception
	 * @throws
	 * @return cdIndicadorModalidade
	 */
	public Integer getCdIndicadorModalidade() {
		return cdIndicadorModalidade;
	}

	/**
	 * Nome: setCdIndicadorModalidade
	 *
	 * @exception
	 * @throws
	 * @param cdIndicadorModalidade
	 */
	public void setCdIndicadorModalidade(Integer cdIndicadorModalidade) {
		this.cdIndicadorModalidade = cdIndicadorModalidade;
	}

	/**
	 * Nome: getCdTipoSaldo
	 *
	 * @exception
	 * @throws
	 * @return cdTipoSaldo
	 */
	public Integer getCdTipoSaldo() {
		return cdTipoSaldo;
	}

	/**
	 * Nome: setCdTipoSaldo
	 *
	 * @exception
	 * @throws
	 * @param cdTipoSaldo
	 */
	public void setCdTipoSaldo(Integer cdTipoSaldo) {
		this.cdTipoSaldo = cdTipoSaldo;
	}

	/**
	 * Nome: getCdTipoProcessamento
	 *
	 * @exception
	 * @throws
	 * @return cdTipoProcessamento
	 */
	public Integer getCdTipoProcessamento() {
		return cdTipoProcessamento;
	}

	/**
	 * Nome: setCdTipoProcessamento
	 *
	 * @exception
	 * @throws
	 * @param cdTipoProcessamento
	 */
	public void setCdTipoProcessamento(Integer cdTipoProcessamento) {
		this.cdTipoProcessamento = cdTipoProcessamento;
	}

	/**
	 * Nome: getCdTratamentoFeriadoDataPagto
	 *
	 * @exception
	 * @throws
	 * @return cdTratamentoFeriadoDataPagto
	 */
	public Integer getCdTratamentoFeriadoDataPagto() {
		return cdTratamentoFeriadoDataPagto;
	}

	/**
	 * Nome: setCdTratamentoFeriadoDataPagto
	 *
	 * @exception
	 * @throws
	 * @param cdTratamentoFeriadoDataPagto
	 */
	public void setCdTratamentoFeriadoDataPagto(Integer cdTratamentoFeriadoDataPagto) {
		this.cdTratamentoFeriadoDataPagto = cdTratamentoFeriadoDataPagto;
	}

	/**
	 * Nome: getCdPermissaoFavorecidoConsultaPagto
	 *
	 * @exception
	 * @throws
	 * @return cdPermissaoFavorecidoConsultaPagto
	 */
	public Integer getCdPermissaoFavorecidoConsultaPagto() {
		return cdPermissaoFavorecidoConsultaPagto;
	}

	/**
	 * Nome: setCdPermissaoFavorecidoConsultaPagto
	 *
	 * @exception
	 * @throws
	 * @param cdPermissaoFavorecidoConsultaPagto
	 */
	public void setCdPermissaoFavorecidoConsultaPagto(
			Integer cdPermissaoFavorecidoConsultaPagto) {
		this.cdPermissaoFavorecidoConsultaPagto = cdPermissaoFavorecidoConsultaPagto;
	}

	/**
	 * Nome: getCdTipoRejeicaoAgendamento
	 *
	 * @exception
	 * @throws
	 * @return cdTipoRejeicaoAgendamento
	 */
	public Integer getCdTipoRejeicaoAgendamento() {
		return cdTipoRejeicaoAgendamento;
	}

	/**
	 * Nome: setCdTipoRejeicaoAgendamento
	 *
	 * @exception
	 * @throws
	 * @param cdTipoRejeicaoAgendamento
	 */
	public void setCdTipoRejeicaoAgendamento(Integer cdTipoRejeicaoAgendamento) {
		this.cdTipoRejeicaoAgendamento = cdTipoRejeicaoAgendamento;
	}

	/**
	 * Nome: getCdTipoRejeicaoEfetivacao
	 *
	 * @exception
	 * @throws
	 * @return cdTipoRejeicaoEfetivacao
	 */
	public Integer getCdTipoRejeicaoEfetivacao() {
		return cdTipoRejeicaoEfetivacao;
	}

	/**
	 * Nome: setCdTipoRejeicaoEfetivacao
	 *
	 * @exception
	 * @throws
	 * @param cdTipoRejeicaoEfetivacao
	 */
	public void setCdTipoRejeicaoEfetivacao(Integer cdTipoRejeicaoEfetivacao) {
		this.cdTipoRejeicaoEfetivacao = cdTipoRejeicaoEfetivacao;
	}

	/**
	 * Nome: getQtdeMaxRegInconsistenteLote
	 *
	 * @exception
	 * @throws
	 * @return qtdeMaxRegInconsistenteLote
	 */
	public Integer getQtdeMaxRegInconsistenteLote() {
		return qtdeMaxRegInconsistenteLote;
	}

	/**
	 * Nome: setQtdeMaxRegInconsistenteLote
	 *
	 * @exception
	 * @throws
	 * @param qtdeMaxRegInconsistenteLote
	 */
	public void setQtdeMaxRegInconsistenteLote(Integer qtdeMaxRegInconsistenteLote) {
		this.qtdeMaxRegInconsistenteLote = qtdeMaxRegInconsistenteLote;
	}

	/**
	 * Nome: getPercentualMaxRegInconsistenteLote
	 *
	 * @exception
	 * @throws
	 * @return percentualMaxRegInconsistenteLote
	 */
	public Integer getPercentualMaxRegInconsistenteLote() {
		return percentualMaxRegInconsistenteLote;
	}

	/**
	 * Nome: setPercentualMaxRegInconsistenteLote
	 *
	 * @exception
	 * @throws
	 * @param percentualMaxRegInconsistenteLote
	 */
	public void setPercentualMaxRegInconsistenteLote(
			Integer percentualMaxRegInconsistenteLote) {
		this.percentualMaxRegInconsistenteLote = percentualMaxRegInconsistenteLote;
	}

	/**
	 * Nome: getCdPrioridadeDebito
	 *
	 * @exception
	 * @throws
	 * @return cdPrioridadeDebito
	 */
	public Integer getCdPrioridadeDebito() {
		return cdPrioridadeDebito;
	}

	/**
	 * Nome: setCdPrioridadeDebito
	 *
	 * @exception
	 * @throws
	 * @param cdPrioridadeDebito
	 */
	public void setCdPrioridadeDebito(Integer cdPrioridadeDebito) {
		this.cdPrioridadeDebito = cdPrioridadeDebito;
	}

	/**
	 * Nome: getCdGeracaoLancamentoFuturoDebito
	 *
	 * @exception
	 * @throws
	 * @return cdGeracaoLancamentoFuturoDebito
	 */
	public Integer getCdGeracaoLancamentoFuturoDebito() {
		return cdGeracaoLancamentoFuturoDebito;
	}

	/**
	 * Nome: setCdGeracaoLancamentoFuturoDebito
	 *
	 * @exception
	 * @throws
	 * @param cdGeracaoLancamentoFuturoDebito
	 */
	public void setCdGeracaoLancamentoFuturoDebito(
			Integer cdGeracaoLancamentoFuturoDebito) {
		this.cdGeracaoLancamentoFuturoDebito = cdGeracaoLancamentoFuturoDebito;
	}

	/**
	 * Nome: getCdUtilizacaoFavorecidoControlePagto
	 *
	 * @exception
	 * @throws
	 * @return cdUtilizacaoFavorecidoControlePagto
	 */
	public Integer getCdUtilizacaoFavorecidoControlePagto() {
		return cdUtilizacaoFavorecidoControlePagto;
	}

	/**
	 * Nome: setCdUtilizacaoFavorecidoControlePagto
	 *
	 * @exception
	 * @throws
	 * @param cdUtilizacaoFavorecidoControlePagto
	 */
	public void setCdUtilizacaoFavorecidoControlePagto(
			Integer cdUtilizacaoFavorecidoControlePagto) {
		this.cdUtilizacaoFavorecidoControlePagto = cdUtilizacaoFavorecidoControlePagto;
	}

	/**
	 * Nome: getVlrMaximoPagtoFavorecidoNaoCadastrado
	 *
	 * @exception
	 * @throws
	 * @return vlrMaximoPagtoFavorecidoNaoCadastrado
	 */
	public BigDecimal getVlrMaximoPagtoFavorecidoNaoCadastrado() {
		return vlrMaximoPagtoFavorecidoNaoCadastrado;
	}

	/**
	 * Nome: setVlrMaximoPagtoFavorecidoNaoCadastrado
	 *
	 * @exception
	 * @throws
	 * @param vlrMaximoPagtoFavorecidoNaoCadastrado
	 */
	public void setVlrMaximoPagtoFavorecidoNaoCadastrado(
			BigDecimal vlrMaximoPagtoFavorecidoNaoCadastrado) {
		this.vlrMaximoPagtoFavorecidoNaoCadastrado = vlrMaximoPagtoFavorecidoNaoCadastrado;
	}

	/**
	 * Nome: getVlrLimiteIndividual
	 *
	 * @exception
	 * @throws
	 * @return vlrLimiteIndividual
	 */
	public BigDecimal getVlrLimiteIndividual() {
		return vlrLimiteIndividual;
	}

	/**
	 * Nome: setVlrLimiteIndividual
	 *
	 * @exception
	 * @throws
	 * @param vlrLimiteIndividual
	 */
	public void setVlrLimiteIndividual(BigDecimal vlrLimiteIndividual) {
		this.vlrLimiteIndividual = vlrLimiteIndividual;
	}

	/**
	 * Nome: getVlrLimiteDiario
	 *
	 * @exception
	 * @throws
	 * @return vlrLimiteDiario
	 */
	public BigDecimal getVlrLimiteDiario() {
		return vlrLimiteDiario;
	}

	/**
	 * Nome: setVlrLimiteDiario
	 *
	 * @exception
	 * @throws
	 * @param vlrLimiteDiario
	 */
	public void setVlrLimiteDiario(BigDecimal vlrLimiteDiario) {
		this.vlrLimiteDiario = vlrLimiteDiario;
	}

	/**
	 * Nome: getQtdeDiaRepique
	 *
	 * @exception
	 * @throws
	 * @return qtdeDiaRepique
	 */
	public Integer getQtdeDiaRepique() {
		return qtdeDiaRepique;
	}

	/**
	 * Nome: setQtdeDiaRepique
	 *
	 * @exception
	 * @throws
	 * @param qtdeDiaRepique
	 */
	public void setQtdeDiaRepique(Integer qtdeDiaRepique) {
		this.qtdeDiaRepique = qtdeDiaRepique;
	}

	/**
	 * Nome: getQtdeDiaFloating
	 *
	 * @exception
	 * @throws
	 * @return qtdeDiaFloating
	 */
	public Integer getQtdeDiaFloating() {
		return qtdeDiaFloating;
	}

	/**
	 * Nome: setQtdeDiaFloating
	 *
	 * @exception
	 * @throws
	 * @param qtdeDiaFloating
	 */
	public void setQtdeDiaFloating(Integer qtdeDiaFloating) {
		this.qtdeDiaFloating = qtdeDiaFloating;
	}

	/**
	 * Nome: getCdTipoInscricaoFavorecidoAceita
	 *
	 * @exception
	 * @throws
	 * @return cdTipoInscricaoFavorecidoAceita
	 */
	public Integer getCdTipoInscricaoFavorecidoAceita() {
		return cdTipoInscricaoFavorecidoAceita;
	}

	/**
	 * Nome: setCdTipoInscricaoFavorecidoAceita
	 *
	 * @exception
	 * @throws
	 * @param cdTipoInscricaoFavorecidoAceita
	 */
	public void setCdTipoInscricaoFavorecidoAceita(
			Integer cdTipoInscricaoFavorecidoAceita) {
		this.cdTipoInscricaoFavorecidoAceita = cdTipoInscricaoFavorecidoAceita;
	}

	/**
	 * Nome: getCdOcorrenciaDiaExpiracao
	 *
	 * @exception
	 * @throws
	 * @return cdOcorrenciaDiaExpiracao
	 */
	public Integer getCdOcorrenciaDiaExpiracao() {
		return cdOcorrenciaDiaExpiracao;
	}

	/**
	 * Nome: setCdOcorrenciaDiaExpiracao
	 *
	 * @exception
	 * @throws
	 * @param cdOcorrenciaDiaExpiracao
	 */
	public void setCdOcorrenciaDiaExpiracao(Integer cdOcorrenciaDiaExpiracao) {
		this.cdOcorrenciaDiaExpiracao = cdOcorrenciaDiaExpiracao;
	}

	/**
	 * Nome: getQtdeDiaExpiracao
	 *
	 * @exception
	 * @throws
	 * @return qtdeDiaExpiracao
	 */
	public Integer getQtdeDiaExpiracao() {
		return qtdeDiaExpiracao;
	}

	/**
	 * Nome: setQtdeDiaExpiracao
	 *
	 * @exception
	 * @throws
	 * @param qtdeDiaExpiracao
	 */
	public void setQtdeDiaExpiracao(Integer qtdeDiaExpiracao) {
		this.qtdeDiaExpiracao = qtdeDiaExpiracao;
	}

	/**
	 * Nome: getCdGeracaoLancamentoFuturoCredito
	 *
	 * @exception
	 * @throws
	 * @return cdGeracaoLancamentoFuturoCredito
	 */
	public Integer getCdGeracaoLancamentoFuturoCredito() {
		return cdGeracaoLancamentoFuturoCredito;
	}

	/**
	 * Nome: setCdGeracaoLancamentoFuturoCredito
	 *
	 * @exception
	 * @throws
	 * @param cdGeracaoLancamentoFuturoCredito
	 */
	public void setCdGeracaoLancamentoFuturoCredito(
			Integer cdGeracaoLancamentoFuturoCredito) {
		this.cdGeracaoLancamentoFuturoCredito = cdGeracaoLancamentoFuturoCredito;
	}

	/**
	 * Nome: getCdGeracaoLancamentoProgramado
	 *
	 * @exception
	 * @throws
	 * @return cdGeracaoLancamentoProgramado
	 */
	public Integer getCdGeracaoLancamentoProgramado() {
		return cdGeracaoLancamentoProgramado;
	}

	/**
	 * Nome: setCdGeracaoLancamentoProgramado
	 *
	 * @exception
	 * @throws
	 * @param cdGeracaoLancamentoProgramado
	 */
	public void setCdGeracaoLancamentoProgramado(
			Integer cdGeracaoLancamentoProgramado) {
		this.cdGeracaoLancamentoProgramado = cdGeracaoLancamentoProgramado;
	}

	/**
	 * Nome: getCdTratoContaTransf
	 *
	 * @exception
	 * @throws
	 * @return cdTratoContaTransf
	 */
	public Integer getCdTratoContaTransf() {
		return cdTratoContaTransf;
	}

	/**
	 * Nome: setCdTratoContaTransf
	 *
	 * @exception
	 * @throws
	 * @param cdTratoContaTransf
	 */
	public void setCdTratoContaTransf(Integer cdTratoContaTransf) {
		this.cdTratoContaTransf = cdTratoContaTransf;
	}

	/**
	 * Nome: getCdConsistenciaCpfCnpj
	 *
	 * @exception
	 * @throws
	 * @return cdConsistenciaCpfCnpj
	 */
	public Integer getCdConsistenciaCpfCnpj() {
		return cdConsistenciaCpfCnpj;
	}

	/**
	 * Nome: setCdConsistenciaCpfCnpj
	 *
	 * @exception
	 * @throws
	 * @param cdConsistenciaCpfCnpj
	 */
	public void setCdConsistenciaCpfCnpj(Integer cdConsistenciaCpfCnpj) {
		this.cdConsistenciaCpfCnpj = cdConsistenciaCpfCnpj;
	}

	/**
	 * Nome: getCdTipoConsistenciaCnpjFavorecido
	 *
	 * @exception
	 * @throws
	 * @return cdTipoConsistenciaCnpjFavorecido
	 */
	public Integer getCdTipoConsistenciaCnpjFavorecido() {
		return cdTipoConsistenciaCnpjFavorecido;
	}

	/**
	 * Nome: setCdTipoConsistenciaCnpjFavorecido
	 *
	 * @exception
	 * @throws
	 * @param cdTipoConsistenciaCnpjFavorecido
	 */
	public void setCdTipoConsistenciaCnpjFavorecido(
			Integer cdTipoConsistenciaCnpjFavorecido) {
		this.cdTipoConsistenciaCnpjFavorecido = cdTipoConsistenciaCnpjFavorecido;
	}

	/**
	 * Nome: getCdTipoContaCredito
	 *
	 * @exception
	 * @throws
	 * @return cdTipoContaCredito
	 */
	public Long getCdTipoContaCredito() {
		return cdTipoContaCredito;
	}

	/**
	 * Nome: setCdTipoContaCredito
	 *
	 * @exception
	 * @throws
	 * @param cdTipoContaCredito
	 */
	public void setCdTipoContaCredito(Long cdTipoContaCredito) {
		this.cdTipoContaCredito = cdTipoContaCredito;
	}

	/**
	 * Nome: getCdEfetuaConsistenciaEspBeneficiario
	 *
	 * @exception
	 * @throws
	 * @return cdEfetuaConsistenciaEspBeneficiario
	 */
	public Integer getCdEfetuaConsistenciaEspBeneficiario() {
		return cdEfetuaConsistenciaEspBeneficiario;
	}

	/**
	 * Nome: setCdEfetuaConsistenciaEspBeneficiario
	 *
	 * @exception
	 * @throws
	 * @param cdEfetuaConsistenciaEspBeneficiario
	 */
	public void setCdEfetuaConsistenciaEspBeneficiario(
			Integer cdEfetuaConsistenciaEspBeneficiario) {
		this.cdEfetuaConsistenciaEspBeneficiario = cdEfetuaConsistenciaEspBeneficiario;
	}

	/**
	 * Nome: getCdUtilizacaoCodigoLancamentoFixo
	 *
	 * @exception
	 * @throws
	 * @return cdUtilizacaoCodigoLancamentoFixo
	 */
	public Integer getCdUtilizacaoCodigoLancamentoFixo() {
		return cdUtilizacaoCodigoLancamentoFixo;
	}

	/**
	 * Nome: setCdUtilizacaoCodigoLancamentoFixo
	 *
	 * @exception
	 * @throws
	 * @param cdUtilizacaoCodigoLancamentoFixo
	 */
	public void setCdUtilizacaoCodigoLancamentoFixo(
			Integer cdUtilizacaoCodigoLancamentoFixo) {
		this.cdUtilizacaoCodigoLancamentoFixo = cdUtilizacaoCodigoLancamentoFixo;
	}

	/**
	 * Nome: getCdLancamentoFixo
	 *
	 * @exception
	 * @throws
	 * @return cdLancamentoFixo
	 */
	public Integer getCdLancamentoFixo() {
		return cdLancamentoFixo;
	}

	/**
	 * Nome: setCdLancamentoFixo
	 *
	 * @exception
	 * @throws
	 * @param cdLancamentoFixo
	 */
	public void setCdLancamentoFixo(Integer cdLancamentoFixo) {
		this.cdLancamentoFixo = cdLancamentoFixo;
	}

	/**
	 * Nome: getCdPermissaoPagtoVencido
	 *
	 * @exception
	 * @throws
	 * @return cdPermissaoPagtoVencido
	 */
	public Integer getCdPermissaoPagtoVencido() {
		return cdPermissaoPagtoVencido;
	}

	/**
	 * Nome: setCdPermissaoPagtoVencido
	 *
	 * @exception
	 * @throws
	 * @param cdPermissaoPagtoVencido
	 */
	public void setCdPermissaoPagtoVencido(Integer cdPermissaoPagtoVencido) {
		this.cdPermissaoPagtoVencido = cdPermissaoPagtoVencido;
	}

	/**
	 * Nome: getQtdeLimiteDiarioPagtoVencido
	 *
	 * @exception
	 * @throws
	 * @return qtdeLimiteDiarioPagtoVencido
	 */
	public Integer getQtdeLimiteDiarioPagtoVencido() {
		return qtdeLimiteDiarioPagtoVencido;
	}

	/**
	 * Nome: setQtdeLimiteDiarioPagtoVencido
	 *
	 * @exception
	 * @throws
	 * @param qtdeLimiteDiarioPagtoVencido
	 */
	public void setQtdeLimiteDiarioPagtoVencido(Integer qtdeLimiteDiarioPagtoVencido) {
		this.qtdeLimiteDiarioPagtoVencido = qtdeLimiteDiarioPagtoVencido;
	}

	/**
	 * Nome: getCdPermissaoPagtoMenor
	 *
	 * @exception
	 * @throws
	 * @return cdPermissaoPagtoMenor
	 */
	public Integer getCdPermissaoPagtoMenor() {
		return cdPermissaoPagtoMenor;
	}

	/**
	 * Nome: setCdPermissaoPagtoMenor
	 *
	 * @exception
	 * @throws
	 * @param cdPermissaoPagtoMenor
	 */
	public void setCdPermissaoPagtoMenor(Integer cdPermissaoPagtoMenor) {
		this.cdPermissaoPagtoMenor = cdPermissaoPagtoMenor;
	}

	/**
	 * Nome: getCdTipoConsultaSaldo
	 *
	 * @exception
	 * @throws
	 * @return cdTipoConsultaSaldo
	 */
	public Integer getCdTipoConsultaSaldo() {
		return cdTipoConsultaSaldo;
	}

	/**
	 * Nome: setCdTipoConsultaSaldo
	 *
	 * @exception
	 * @throws
	 * @param cdTipoConsultaSaldo
	 */
	public void setCdTipoConsultaSaldo(Integer cdTipoConsultaSaldo) {
		this.cdTipoConsultaSaldo = cdTipoConsultaSaldo;
	}

	/**
	 * Nome: getCdTratamentoFeriado
	 *
	 * @exception
	 * @throws
	 * @return cdTratamentoFeriado
	 */
	public Integer getCdTratamentoFeriado() {
		return cdTratamentoFeriado;
	}

	/**
	 * Nome: setCdTratamentoFeriado
	 *
	 * @exception
	 * @throws
	 * @param cdTratamentoFeriado
	 */
	public void setCdTratamentoFeriado(Integer cdTratamentoFeriado) {
		this.cdTratamentoFeriado = cdTratamentoFeriado;
	}

	/**
	 * Nome: getCdPermissaoContingencia
	 *
	 * @exception
	 * @throws
	 * @return cdPermissaoContingencia
	 */
	public Integer getCdPermissaoContingencia() {
		return cdPermissaoContingencia;
	}

	/**
	 * Nome: setCdPermissaoContingencia
	 *
	 * @exception
	 * @throws
	 * @param cdPermissaoContingencia
	 */
	public void setCdPermissaoContingencia(Integer cdPermissaoContingencia) {
		this.cdPermissaoContingencia = cdPermissaoContingencia;
	}

	/**
	 * Nome: getCdTratamentoValorDivergente
	 *
	 * @exception
	 * @throws
	 * @return cdTratamentoValorDivergente
	 */
	public Integer getCdTratamentoValorDivergente() {
		return cdTratamentoValorDivergente;
	}

	/**
	 * Nome: setCdTratamentoValorDivergente
	 *
	 * @exception
	 * @throws
	 * @param cdTratamentoValorDivergente
	 */
	public void setCdTratamentoValorDivergente(Integer cdTratamentoValorDivergente) {
		this.cdTratamentoValorDivergente = cdTratamentoValorDivergente;
	}

	/**
	 * Nome: getCdTipoConsistenciaCnpjProposta
	 *
	 * @exception
	 * @throws
	 * @return cdTipoConsistenciaCnpjProposta
	 */
	public Integer getCdTipoConsistenciaCnpjProposta() {
		return cdTipoConsistenciaCnpjProposta;
	}

	/**
	 * Nome: setCdTipoConsistenciaCnpjProposta
	 *
	 * @exception
	 * @throws
	 * @param cdTipoConsistenciaCnpjProposta
	 */
	public void setCdTipoConsistenciaCnpjProposta(
			Integer cdTipoConsistenciaCnpjProposta) {
		this.cdTipoConsistenciaCnpjProposta = cdTipoConsistenciaCnpjProposta;
	}

	/**
	 * Nome: getCdTipoComprovacaoVida
	 *
	 * @exception
	 * @throws
	 * @return cdTipoComprovacaoVida
	 */
	public Integer getCdTipoComprovacaoVida() {
		return cdTipoComprovacaoVida;
	}

	/**
	 * Nome: setCdTipoComprovacaoVida
	 *
	 * @exception
	 * @throws
	 * @param cdTipoComprovacaoVida
	 */
	public void setCdTipoComprovacaoVida(Integer cdTipoComprovacaoVida) {
		this.cdTipoComprovacaoVida = cdTipoComprovacaoVida;
	}

	/**
	 * Nome: getQtdeMesesComprovacaoVida
	 *
	 * @exception
	 * @throws
	 * @return qtdeMesesComprovacaoVida
	 */
	public Integer getQtdeMesesComprovacaoVida() {
		return qtdeMesesComprovacaoVida;
	}

	/**
	 * Nome: setQtdeMesesComprovacaoVida
	 *
	 * @exception
	 * @throws
	 * @param qtdeMesesComprovacaoVida
	 */
	public void setQtdeMesesComprovacaoVida(Integer qtdeMesesComprovacaoVida) {
		this.qtdeMesesComprovacaoVida = qtdeMesesComprovacaoVida;
	}

	/**
	 * Nome: getCdIndicadorEmissaoComprovanteVida
	 *
	 * @exception
	 * @throws
	 * @return cdIndicadorEmissaoComprovanteVida
	 */
	public Integer getCdIndicadorEmissaoComprovanteVida() {
		return cdIndicadorEmissaoComprovanteVida;
	}

	/**
	 * Nome: setCdIndicadorEmissaoComprovanteVida
	 *
	 * @exception
	 * @throws
	 * @param cdIndicadorEmissaoComprovanteVida
	 */
	public void setCdIndicadorEmissaoComprovanteVida(
			Integer cdIndicadorEmissaoComprovanteVida) {
		this.cdIndicadorEmissaoComprovanteVida = cdIndicadorEmissaoComprovanteVida;
	}

	/**
	 * Nome: getQtdeDiaAvisoComprovanteVidaInicial
	 *
	 * @exception
	 * @throws
	 * @return qtdeDiaAvisoComprovanteVidaInicial
	 */
	public Integer getQtdeDiaAvisoComprovanteVidaInicial() {
		return qtdeDiaAvisoComprovanteVidaInicial;
	}

	/**
	 * Nome: setQtdeDiaAvisoComprovanteVidaInicial
	 *
	 * @exception
	 * @throws
	 * @param qtdeDiaAvisoComprovanteVidaInicial
	 */
	public void setQtdeDiaAvisoComprovanteVidaInicial(
			Integer qtdeDiaAvisoComprovanteVidaInicial) {
		this.qtdeDiaAvisoComprovanteVidaInicial = qtdeDiaAvisoComprovanteVidaInicial;
	}

	/**
	 * Nome: getQtdeDiaAvisoComprovanteVidaVencimento
	 *
	 * @exception
	 * @throws
	 * @return qtdeDiaAvisoComprovanteVidaVencimento
	 */
	public Integer getQtdeDiaAvisoComprovanteVidaVencimento() {
		return qtdeDiaAvisoComprovanteVidaVencimento;
	}

	/**
	 * Nome: setQtdeDiaAvisoComprovanteVidaVencimento
	 *
	 * @exception
	 * @throws
	 * @param qtdeDiaAvisoComprovanteVidaVencimento
	 */
	public void setQtdeDiaAvisoComprovanteVidaVencimento(
			Integer qtdeDiaAvisoComprovanteVidaVencimento) {
		this.qtdeDiaAvisoComprovanteVidaVencimento = qtdeDiaAvisoComprovanteVidaVencimento;
	}

	/**
	 * Nome: getCdUtilizacaoMensagemPersonalizada
	 *
	 * @exception
	 * @throws
	 * @return cdUtilizacaoMensagemPersonalizada
	 */
	public Integer getCdUtilizacaoMensagemPersonalizada() {
		return cdUtilizacaoMensagemPersonalizada;
	}

	/**
	 * Nome: setCdUtilizacaoMensagemPersonalizada
	 *
	 * @exception
	 * @throws
	 * @param cdUtilizacaoMensagemPersonalizada
	 */
	public void setCdUtilizacaoMensagemPersonalizada(
			Integer cdUtilizacaoMensagemPersonalizada) {
		this.cdUtilizacaoMensagemPersonalizada = cdUtilizacaoMensagemPersonalizada;
	}

	/**
	 * Nome: getCdDestino
	 *
	 * @exception
	 * @throws
	 * @return cdDestino
	 */
	public Integer getCdDestino() {
		return cdDestino;
	}

	/**
	 * Nome: setCdDestino
	 *
	 * @exception
	 * @throws
	 * @param cdDestino
	 */
	public void setCdDestino(Integer cdDestino) {
		this.cdDestino = cdDestino;
	}

	/**
	 * Nome: getCdCadastroEnderecoUtilizado
	 *
	 * @exception
	 * @throws
	 * @return cdCadastroEnderecoUtilizado
	 */
	public Integer getCdCadastroEnderecoUtilizado() {
		return cdCadastroEnderecoUtilizado;
	}

	/**
	 * Nome: setCdCadastroEnderecoUtilizado
	 *
	 * @exception
	 * @throws
	 * @param cdCadastroEnderecoUtilizado
	 */
	public void setCdCadastroEnderecoUtilizado(Integer cdCadastroEnderecoUtilizado) {
		this.cdCadastroEnderecoUtilizado = cdCadastroEnderecoUtilizado;
	}

	/**
	 * Nome: getCdutilizacaoFormularioPersonalizado
	 *
	 * @exception
	 * @throws
	 * @return cdutilizacaoFormularioPersonalizado
	 */
	public Integer getCdutilizacaoFormularioPersonalizado() {
		return cdutilizacaoFormularioPersonalizado;
	}

	/**
	 * Nome: setCdutilizacaoFormularioPersonalizado
	 *
	 * @exception
	 * @throws
	 * @param cdutilizacaoFormularioPersonalizado
	 */
	public void setCdutilizacaoFormularioPersonalizado(
			Integer cdutilizacaoFormularioPersonalizado) {
		this.cdutilizacaoFormularioPersonalizado = cdutilizacaoFormularioPersonalizado;
	}

	/**
	 * Nome: getCdFormulario
	 *
	 * @exception
	 * @throws
	 * @return cdFormulario
	 */
	public Long getCdFormulario() {
		return cdFormulario;
	}

	/**
	 * Nome: setCdFormulario
	 *
	 * @exception
	 * @throws
	 * @param cdFormulario
	 */
	public void setCdFormulario(Long cdFormulario) {
		this.cdFormulario = cdFormulario;
	}

	/**
	 * Nome: getCdCriterioRstrbTitulo
	 *
	 * @exception
	 * @throws
	 * @return cdCriterioRstrbTitulo
	 */
	public Integer getCdCriterioRstrbTitulo() {
		return cdCriterioRstrbTitulo;
	}

	/**
	 * Nome: setCdCriterioRstrbTitulo
	 *
	 * @exception
	 * @throws
	 * @param cdCriterioRstrbTitulo
	 */
	public void setCdCriterioRstrbTitulo(Integer cdCriterioRstrbTitulo) {
		this.cdCriterioRstrbTitulo = cdCriterioRstrbTitulo;
	}

	/**
	 * Nome: getCdRastreabTituloTerc
	 *
	 * @exception
	 * @throws
	 * @return cdRastreabTituloTerc
	 */
	public Integer getCdRastreabTituloTerc() {
		return cdRastreabTituloTerc;
	}

	/**
	 * Nome: setCdRastreabTituloTerc
	 *
	 * @exception
	 * @throws
	 * @param cdRastreabTituloTerc
	 */
	public void setCdRastreabTituloTerc(Integer cdRastreabTituloTerc) {
		this.cdRastreabTituloTerc = cdRastreabTituloTerc;
	}

	/**
	 * Nome: getCdRastreabNotaFiscal
	 *
	 * @exception
	 * @throws
	 * @return cdRastreabNotaFiscal
	 */
	public Integer getCdRastreabNotaFiscal() {
		return cdRastreabNotaFiscal;
	}

	/**
	 * Nome: setCdRastreabNotaFiscal
	 *
	 * @exception
	 * @throws
	 * @param cdRastreabNotaFiscal
	 */
	public void setCdRastreabNotaFiscal(Integer cdRastreabNotaFiscal) {
		this.cdRastreabNotaFiscal = cdRastreabNotaFiscal;
	}

	/**
	 * Nome: getCdCaptuTituloRegistro
	 *
	 * @exception
	 * @throws
	 * @return cdCaptuTituloRegistro
	 */
	public Integer getCdCaptuTituloRegistro() {
		return cdCaptuTituloRegistro;
	}

	/**
	 * Nome: setCdCaptuTituloRegistro
	 *
	 * @exception
	 * @throws
	 * @param cdCaptuTituloRegistro
	 */
	public void setCdCaptuTituloRegistro(Integer cdCaptuTituloRegistro) {
		this.cdCaptuTituloRegistro = cdCaptuTituloRegistro;
	}

	/**
	 * Nome: getCdIndicadorAgendaTitulo
	 *
	 * @exception
	 * @throws
	 * @return cdIndicadorAgendaTitulo
	 */
	public Integer getCdIndicadorAgendaTitulo() {
		return cdIndicadorAgendaTitulo;
	}

	/**
	 * Nome: setCdIndicadorAgendaTitulo
	 *
	 * @exception
	 * @throws
	 * @param cdIndicadorAgendaTitulo
	 */
	public void setCdIndicadorAgendaTitulo(Integer cdIndicadorAgendaTitulo) {
		this.cdIndicadorAgendaTitulo = cdIndicadorAgendaTitulo;
	}

	/**
	 * Nome: getCdAgendaRastreabilidadeFilial
	 *
	 * @exception
	 * @throws
	 * @return cdAgendaRastreabilidadeFilial
	 */
	public Integer getCdAgendaRastreabilidadeFilial() {
		return cdAgendaRastreabilidadeFilial;
	}

	/**
	 * Nome: setCdAgendaRastreabilidadeFilial
	 *
	 * @exception
	 * @throws
	 * @param cdAgendaRastreabilidadeFilial
	 */
	public void setCdAgendaRastreabilidadeFilial(
			Integer cdAgendaRastreabilidadeFilial) {
		this.cdAgendaRastreabilidadeFilial = cdAgendaRastreabilidadeFilial;
	}

	/**
	 * Nome: getCdBloqueioEmissaoPplta
	 *
	 * @exception
	 * @throws
	 * @return cdBloqueioEmissaoPplta
	 */
	public Integer getCdBloqueioEmissaoPplta() {
		return cdBloqueioEmissaoPplta;
	}

	/**
	 * Nome: setCdBloqueioEmissaoPplta
	 *
	 * @exception
	 * @throws
	 * @param cdBloqueioEmissaoPplta
	 */
	public void setCdBloqueioEmissaoPplta(Integer cdBloqueioEmissaoPplta) {
		this.cdBloqueioEmissaoPplta = cdBloqueioEmissaoPplta;
	}

	/**
	 * Nome: getDtInicioRastreabTitulo
	 *
	 * @exception
	 * @throws
	 * @return dtInicioRastreabTitulo
	 */
	public String getDtInicioRastreabTitulo() {
		return dtInicioRastreabTitulo;
	}

	/**
	 * Nome: setDtInicioRastreabTitulo
	 *
	 * @exception
	 * @throws
	 * @param dtInicioRastreabTitulo
	 */
	public void setDtInicioRastreabTitulo(String dtInicioRastreabTitulo) {
		this.dtInicioRastreabTitulo = dtInicioRastreabTitulo;
	}

	/**
	 * Nome: getDtInicioBloqueioPplta
	 *
	 * @exception
	 * @throws
	 * @return dtInicioBloqueioPplta
	 */
	public String getDtInicioBloqueioPplta() {
		return dtInicioBloqueioPplta;
	}

	/**
	 * Nome: setDtInicioBloqueioPplta
	 *
	 * @exception
	 * @throws
	 * @param dtInicioBloqueioPplta
	 */
	public void setDtInicioBloqueioPplta(String dtInicioBloqueioPplta) {
		this.dtInicioBloqueioPplta = dtInicioBloqueioPplta;
	}

	/**
	 * Nome: getDtInicioRegTitulo
	 *
	 * @exception
	 * @throws
	 * @return dtInicioRegTitulo
	 */
	public String getDtInicioRegTitulo() {
		return dtInicioRegTitulo;
	}

	/**
	 * Nome: setDtInicioRegTitulo
	 *
	 * @exception
	 * @throws
	 * @param dtInicioRegTitulo
	 */
	public void setDtInicioRegTitulo(String dtInicioRegTitulo) {
		this.dtInicioRegTitulo = dtInicioRegTitulo;
	}

	/**
	 * Nome: getCdMomentoAvisoRecadastramento
	 *
	 * @exception
	 * @throws
	 * @return cdMomentoAvisoRecadastramento
	 */
	public Integer getCdMomentoAvisoRecadastramento() {
		return cdMomentoAvisoRecadastramento;
	}

	/**
	 * Nome: setCdMomentoAvisoRecadastramento
	 *
	 * @exception
	 * @throws
	 * @param cdMomentoAvisoRecadastramento
	 */
	public void setCdMomentoAvisoRecadastramento(
			Integer cdMomentoAvisoRecadastramento) {
		this.cdMomentoAvisoRecadastramento = cdMomentoAvisoRecadastramento;
	}

	/**
	 * Nome: getQtdeAntecipacaoInicioRecadastramento
	 *
	 * @exception
	 * @throws
	 * @return qtdeAntecipacaoInicioRecadastramento
	 */
	public Integer getQtdeAntecipacaoInicioRecadastramento() {
		return qtdeAntecipacaoInicioRecadastramento;
	}

	/**
	 * Nome: setQtdeAntecipacaoInicioRecadastramento
	 *
	 * @exception
	 * @throws
	 * @param qtdeAntecipacaoInicioRecadastramento
	 */
	public void setQtdeAntecipacaoInicioRecadastramento(
			Integer qtdeAntecipacaoInicioRecadastramento) {
		this.qtdeAntecipacaoInicioRecadastramento = qtdeAntecipacaoInicioRecadastramento;
	}

	/**
	 * Nome: getCdAgruparCorrespondencia
	 *
	 * @exception
	 * @throws
	 * @return cdAgruparCorrespondencia
	 */
	public Integer getCdAgruparCorrespondencia() {
		return cdAgruparCorrespondencia;
	}

	/**
	 * Nome: setCdAgruparCorrespondencia
	 *
	 * @exception
	 * @throws
	 * @param cdAgruparCorrespondencia
	 */
	public void setCdAgruparCorrespondencia(Integer cdAgruparCorrespondencia) {
		this.cdAgruparCorrespondencia = cdAgruparCorrespondencia;
	}

	/**
	 * Nome: getCdAdesaoSacado
	 *
	 * @exception
	 * @throws
	 * @return cdAdesaoSacado
	 */
	public Integer getCdAdesaoSacado() {
		return cdAdesaoSacado;
	}

	/**
	 * Nome: setCdAdesaoSacado
	 *
	 * @exception
	 * @throws
	 * @param cdAdesaoSacado
	 */
	public void setCdAdesaoSacado(Integer cdAdesaoSacado) {
		this.cdAdesaoSacado = cdAdesaoSacado;
	}

	/**
	 * Nome: getCdValidacaoNomeFavorecido
	 *
	 * @exception
	 * @throws
	 * @return cdValidacaoNomeFavorecido
	 */
	public Integer getCdValidacaoNomeFavorecido() {
		return cdValidacaoNomeFavorecido;
	}

	/**
	 * Nome: setCdValidacaoNomeFavorecido
	 *
	 * @exception
	 * @throws
	 * @param cdValidacaoNomeFavorecido
	 */
	public void setCdValidacaoNomeFavorecido(Integer cdValidacaoNomeFavorecido) {
		this.cdValidacaoNomeFavorecido = cdValidacaoNomeFavorecido;
	}

	/**
	 * Nome: getCdPermissaoDebitoOnline
	 *
	 * @exception
	 * @throws
	 * @return cdPermissaoDebitoOnline
	 */
	public Integer getCdPermissaoDebitoOnline() {
		return cdPermissaoDebitoOnline;
	}

	/**
	 * Nome: setCdPermissaoDebitoOnline
	 *
	 * @exception
	 * @throws
	 * @param cdPermissaoDebitoOnline
	 */
	public void setCdPermissaoDebitoOnline(Integer cdPermissaoDebitoOnline) {
		this.cdPermissaoDebitoOnline = cdPermissaoDebitoOnline;
	}

	/**
	 * Nome: getCdLiberacaoLoteProcs
	 *
	 * @exception
	 * @throws
	 * @return cdLiberacaoLoteProcs
	 */
	public Integer getCdLiberacaoLoteProcs() {
		return cdLiberacaoLoteProcs;
	}

	/**
	 * Nome: setCdLiberacaoLoteProcs
	 *
	 * @exception
	 * @throws
	 * @param cdLiberacaoLoteProcs
	 */
	public void setCdLiberacaoLoteProcs(Integer cdLiberacaoLoteProcs) {
		this.cdLiberacaoLoteProcs = cdLiberacaoLoteProcs;
	}

	/**
	 * Nome: getCdTipoSaldoValorSuperior
	 *
	 * @exception
	 * @throws
	 * @return cdTipoSaldoValorSuperior
	 */
	public Integer getCdTipoSaldoValorSuperior() {
		return cdTipoSaldoValorSuperior;
	}

	/**
	 * Nome: setCdTipoSaldoValorSuperior
	 *
	 * @exception
	 * @throws
	 * @param cdTipoSaldoValorSuperior
	 */
	public void setCdTipoSaldoValorSuperior(Integer cdTipoSaldoValorSuperior) {
		this.cdTipoSaldoValorSuperior = cdTipoSaldoValorSuperior;
	}

}