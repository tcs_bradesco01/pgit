/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listarliquidacaopagamento.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ListarLiquidacaoPagamentoRequest.
 * 
 * @version $Revision$ $Date$
 */
public class ListarLiquidacaoPagamentoRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _qtConsultas
     */
    private int _qtConsultas = 0;

    /**
     * keeps track of state for field: _qtConsultas
     */
    private boolean _has_qtConsultas;

    /**
     * Field _cdFormaLiquidacao
     */
    private int _cdFormaLiquidacao = 0;

    /**
     * keeps track of state for field: _cdFormaLiquidacao
     */
    private boolean _has_cdFormaLiquidacao;

    /**
     * Field _dtInicioVigencia
     */
    private java.lang.String _dtInicioVigencia;

    /**
     * Field _dtFinalVigencia
     */
    private java.lang.String _dtFinalVigencia;

    /**
     * Field _hrInclusaoRegistroHist
     */
    private java.lang.String _hrInclusaoRegistroHist;

    /**
     * Field _hrConsultasSaldoPagamento
     */
    private java.lang.String _hrConsultasSaldoPagamento;

    /**
     * Field _hrConsultaFolhaPgto
     */
    private java.lang.String _hrConsultaFolhaPgto;


      //----------------/
     //- Constructors -/
    //----------------/

    public ListarLiquidacaoPagamentoRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarliquidacaopagamento.request.ListarLiquidacaoPagamentoRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdFormaLiquidacao
     * 
     */
    public void deleteCdFormaLiquidacao()
    {
        this._has_cdFormaLiquidacao= false;
    } //-- void deleteCdFormaLiquidacao() 

    /**
     * Method deleteQtConsultas
     * 
     */
    public void deleteQtConsultas()
    {
        this._has_qtConsultas= false;
    } //-- void deleteQtConsultas() 

    /**
     * Returns the value of field 'cdFormaLiquidacao'.
     * 
     * @return int
     * @return the value of field 'cdFormaLiquidacao'.
     */
    public int getCdFormaLiquidacao()
    {
        return this._cdFormaLiquidacao;
    } //-- int getCdFormaLiquidacao() 

    /**
     * Returns the value of field 'dtFinalVigencia'.
     * 
     * @return String
     * @return the value of field 'dtFinalVigencia'.
     */
    public java.lang.String getDtFinalVigencia()
    {
        return this._dtFinalVigencia;
    } //-- java.lang.String getDtFinalVigencia() 

    /**
     * Returns the value of field 'dtInicioVigencia'.
     * 
     * @return String
     * @return the value of field 'dtInicioVigencia'.
     */
    public java.lang.String getDtInicioVigencia()
    {
        return this._dtInicioVigencia;
    } //-- java.lang.String getDtInicioVigencia() 

    /**
     * Returns the value of field 'hrConsultaFolhaPgto'.
     * 
     * @return String
     * @return the value of field 'hrConsultaFolhaPgto'.
     */
    public java.lang.String getHrConsultaFolhaPgto()
    {
        return this._hrConsultaFolhaPgto;
    } //-- java.lang.String getHrConsultaFolhaPgto() 

    /**
     * Returns the value of field 'hrConsultasSaldoPagamento'.
     * 
     * @return String
     * @return the value of field 'hrConsultasSaldoPagamento'.
     */
    public java.lang.String getHrConsultasSaldoPagamento()
    {
        return this._hrConsultasSaldoPagamento;
    } //-- java.lang.String getHrConsultasSaldoPagamento() 

    /**
     * Returns the value of field 'hrInclusaoRegistroHist'.
     * 
     * @return String
     * @return the value of field 'hrInclusaoRegistroHist'.
     */
    public java.lang.String getHrInclusaoRegistroHist()
    {
        return this._hrInclusaoRegistroHist;
    } //-- java.lang.String getHrInclusaoRegistroHist() 

    /**
     * Returns the value of field 'qtConsultas'.
     * 
     * @return int
     * @return the value of field 'qtConsultas'.
     */
    public int getQtConsultas()
    {
        return this._qtConsultas;
    } //-- int getQtConsultas() 

    /**
     * Method hasCdFormaLiquidacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFormaLiquidacao()
    {
        return this._has_cdFormaLiquidacao;
    } //-- boolean hasCdFormaLiquidacao() 

    /**
     * Method hasQtConsultas
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtConsultas()
    {
        return this._has_qtConsultas;
    } //-- boolean hasQtConsultas() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdFormaLiquidacao'.
     * 
     * @param cdFormaLiquidacao the value of field
     * 'cdFormaLiquidacao'.
     */
    public void setCdFormaLiquidacao(int cdFormaLiquidacao)
    {
        this._cdFormaLiquidacao = cdFormaLiquidacao;
        this._has_cdFormaLiquidacao = true;
    } //-- void setCdFormaLiquidacao(int) 

    /**
     * Sets the value of field 'dtFinalVigencia'.
     * 
     * @param dtFinalVigencia the value of field 'dtFinalVigencia'.
     */
    public void setDtFinalVigencia(java.lang.String dtFinalVigencia)
    {
        this._dtFinalVigencia = dtFinalVigencia;
    } //-- void setDtFinalVigencia(java.lang.String) 

    /**
     * Sets the value of field 'dtInicioVigencia'.
     * 
     * @param dtInicioVigencia the value of field 'dtInicioVigencia'
     */
    public void setDtInicioVigencia(java.lang.String dtInicioVigencia)
    {
        this._dtInicioVigencia = dtInicioVigencia;
    } //-- void setDtInicioVigencia(java.lang.String) 

    /**
     * Sets the value of field 'hrConsultaFolhaPgto'.
     * 
     * @param hrConsultaFolhaPgto the value of field
     * 'hrConsultaFolhaPgto'.
     */
    public void setHrConsultaFolhaPgto(java.lang.String hrConsultaFolhaPgto)
    {
        this._hrConsultaFolhaPgto = hrConsultaFolhaPgto;
    } //-- void setHrConsultaFolhaPgto(java.lang.String) 

    /**
     * Sets the value of field 'hrConsultasSaldoPagamento'.
     * 
     * @param hrConsultasSaldoPagamento the value of field
     * 'hrConsultasSaldoPagamento'.
     */
    public void setHrConsultasSaldoPagamento(java.lang.String hrConsultasSaldoPagamento)
    {
        this._hrConsultasSaldoPagamento = hrConsultasSaldoPagamento;
    } //-- void setHrConsultasSaldoPagamento(java.lang.String) 

    /**
     * Sets the value of field 'hrInclusaoRegistroHist'.
     * 
     * @param hrInclusaoRegistroHist the value of field
     * 'hrInclusaoRegistroHist'.
     */
    public void setHrInclusaoRegistroHist(java.lang.String hrInclusaoRegistroHist)
    {
        this._hrInclusaoRegistroHist = hrInclusaoRegistroHist;
    } //-- void setHrInclusaoRegistroHist(java.lang.String) 

    /**
     * Sets the value of field 'qtConsultas'.
     * 
     * @param qtConsultas the value of field 'qtConsultas'.
     */
    public void setQtConsultas(int qtConsultas)
    {
        this._qtConsultas = qtConsultas;
        this._has_qtConsultas = true;
    } //-- void setQtConsultas(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ListarLiquidacaoPagamentoRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listarliquidacaopagamento.request.ListarLiquidacaoPagamentoRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listarliquidacaopagamento.request.ListarLiquidacaoPagamentoRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listarliquidacaopagamento.request.ListarLiquidacaoPagamentoRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarliquidacaopagamento.request.ListarLiquidacaoPagamentoRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
