/*
 * Nome: br.com.bradesco.web.pgit.service.business.imprimiranexoprimeirocontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.imprimiranexoprimeirocontrato.bean;

/**
 * Nome: OcorrenciasLayout
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class OcorrenciasLayout {
	
	/** Atributo dsServicoLayout. */
	private String dsServicoLayout;
	
	/** Atributo cdLayout. */
	private String cdLayout;
	
	/** Atributo cdPerfilTrocaArq. */
	private Long cdPerfilTrocaArq;
	
	/** Atributo dsAplicativoTransmissao. */
	private String dsAplicativoTransmissao;
	
	/** Atributo dsMeioTransmissao. */
	private String dsMeioTransmissao;
	
	/**
	 * Get: dsServicoLayout.
	 *
	 * @return dsServicoLayout
	 */
	public String getDsServicoLayout() {
		return dsServicoLayout;
	}
	
	/**
	 * Set: dsServicoLayout.
	 *
	 * @param dsServicoLayout the ds servico layout
	 */
	public void setDsServicoLayout(String dsServicoLayout) {
		this.dsServicoLayout = dsServicoLayout;
	}
	
	/**
	 * Get: cdLayout.
	 *
	 * @return cdLayout
	 */
	public String getCdLayout() {
		return cdLayout;
	}
	
	/**
	 * Set: cdLayout.
	 *
	 * @param cdLayout the cd layout
	 */
	public void setCdLayout(String cdLayout) {
		this.cdLayout = cdLayout;
	}
	
	/**
	 * Get: cdPerfilTrocaArq.
	 *
	 * @return cdPerfilTrocaArq
	 */
	public Long getCdPerfilTrocaArq() {
		return cdPerfilTrocaArq;
	}
	
	/**
	 * Set: cdPerfilTrocaArq.
	 *
	 * @param cdPerfilTrocaArq the cd perfil troca arq
	 */
	public void setCdPerfilTrocaArq(Long cdPerfilTrocaArq) {
		this.cdPerfilTrocaArq = cdPerfilTrocaArq;
	}
	
	/**
	 * Get: dsAplicativoTransmissao.
	 *
	 * @return dsAplicativoTransmissao
	 */
	public String getDsAplicativoTransmissao() {
		return dsAplicativoTransmissao;
	}
	
	/**
	 * Set: dsAplicativoTransmissao.
	 *
	 * @param dsAplicativoTransmissao the ds aplicativo transmissao
	 */
	public void setDsAplicativoTransmissao(String dsAplicativoTransmissao) {
		this.dsAplicativoTransmissao = dsAplicativoTransmissao;
	}
	
	/**
	 * Get: dsMeioTransmissao.
	 *
	 * @return dsMeioTransmissao
	 */
	public String getDsMeioTransmissao() {
		return dsMeioTransmissao;
	}
	
	/**
	 * Set: dsMeioTransmissao.
	 *
	 * @param dsMeioTransmissao the ds meio transmissao
	 */
	public void setDsMeioTransmissao(String dsMeioTransmissao) {
		this.dsMeioTransmissao = dsMeioTransmissao;
	}	
}
