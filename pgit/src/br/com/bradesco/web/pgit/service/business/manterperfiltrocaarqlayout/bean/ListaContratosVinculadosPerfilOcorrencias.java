/*
 * Nome: br.com.bradesco.web.pgit.service.business.manterperfiltrocaarqlayout.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.manterperfiltrocaarqlayout.bean;

import java.util.Date;

import br.com.bradesco.web.pgit.utils.CpfCnpjUtils;

/**
 * Nome: ListaContratosVinculadosPerfilOcorrencias
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListaContratosVinculadosPerfilOcorrencias {
	
	/** Atributo cdClubRepresentante. */
	private Long cdClubRepresentante;
    
    /** Atributo cdCorpoCnpjRepresentante. */
    private Long cdCorpoCnpjRepresentante;
    
    /** Atributo cdFilialCnpjRepresentante. */
    private Integer cdFilialCnpjRepresentante;
    
    /** Atributo cdControleCnpjRepresentante. */
    private Integer cdControleCnpjRepresentante;
    
    /** Atributo nmRazaoRepresentante. */
    private String nmRazaoRepresentante;
    
    /** Atributo cdPessoaJuridica. */
    private Long cdPessoaJuridica;
    
    /** Atributo dsPessoaJuridica. */
    private String dsPessoaJuridica;
    
    /** Atributo cdTipoContrato. */
    private Integer cdTipoContrato;
    
    /** Atributo dsTipoContrato. */
    private String dsTipoContrato;
    
    /** Atributo nrSequenciaContrato. */
    private Long nrSequenciaContrato;
    
    /** Atributo dsContrato. */
    private String dsContrato;
    
    /** Atributo cdAgenciaOperadora. */
    private Integer cdAgenciaOperadora;
    
    /** Atributo cdDigitoAgenciaOperadora. */
    private String cdDigitoAgenciaOperadora;
    
    /** Atributo dsAgenciaOperadora. */
    private String dsAgenciaOperadora;
    
    /** Atributo cdFuncionarioBradesco. */
    private Long cdFuncionarioBradesco;
    
    /** Atributo dsFuncionarioBradesco. */
    private String dsFuncionarioBradesco;
    
    /** Atributo cdSituacaoContrato. */
    private Integer cdSituacaoContrato;
    
    /** Atributo dsSituacaoContrato. */
    private String dsSituacaoContrato;
    
    /** Atributo cdMotivoSituacao. */
    private Integer cdMotivoSituacao;
    
    /** Atributo dsMotivoSituacao. */
    private String dsMotivoSituacao;
    
    /** Atributo cdAditivo. */
    private String cdAditivo;
    
    /** Atributo cdTipoParticipacao. */
    private String cdTipoParticipacao;
    
    /** Atributo cdSegmentoCliente. */
    private Integer cdSegmentoCliente;
    
    /** Atributo dsSegmentoCliente. */
    private String dsSegmentoCliente;
    
    /** Atributo cdSubSegmentoCliente. */
    private Integer cdSubSegmentoCliente;
    
    /** Atributo dsSubSegmentoCliente. */
    private String dsSubSegmentoCliente;
    
    /** Atributo cdAtividadeEconomica. */
    private Integer cdAtividadeEconomica;
    
    /** Atributo dsAtividadeEconomica. */
    private String dsAtividadeEconomica;
    
    /** Atributo cdGrupoEconomico. */
    private Long cdGrupoEconomico;
    
    /** Atributo dsGrupoEconomico. */
    private String dsGrupoEconomico;
    
    /** Atributo dtAbertura. */
    private Date dtAbertura;
    
    /** Atributo dtEncerramento. */
    private Date dtEncerramento;
    
    /** Atributo dtInclusao. */
    private Date dtInclusao;
    
	/**
	 * Get: cdClubRepresentante.
	 *
	 * @return cdClubRepresentante
	 */
	public Long getCdClubRepresentante() {
		return cdClubRepresentante;
	}
	
	/**
	 * Set: cdClubRepresentante.
	 *
	 * @param cdClubRepresentante the cd club representante
	 */
	public void setCdClubRepresentante(Long cdClubRepresentante) {
		this.cdClubRepresentante = cdClubRepresentante;
	}
	
	/**
	 * Get: cdCorpoCnpjRepresentante.
	 *
	 * @return cdCorpoCnpjRepresentante
	 */
	public Long getCdCorpoCnpjRepresentante() {
		return cdCorpoCnpjRepresentante;
	}
	
	/**
	 * Set: cdCorpoCnpjRepresentante.
	 *
	 * @param cdCorpoCnpjRepresentante the cd corpo cnpj representante
	 */
	public void setCdCorpoCnpjRepresentante(Long cdCorpoCnpjRepresentante) {
		this.cdCorpoCnpjRepresentante = cdCorpoCnpjRepresentante;
	}
	
	/**
	 * Get: cdFilialCnpjRepresentante.
	 *
	 * @return cdFilialCnpjRepresentante
	 */
	public Integer getCdFilialCnpjRepresentante() {
		return cdFilialCnpjRepresentante;
	}
	
	/**
	 * Set: cdFilialCnpjRepresentante.
	 *
	 * @param cdFilialCnpjRepresentante the cd filial cnpj representante
	 */
	public void setCdFilialCnpjRepresentante(Integer cdFilialCnpjRepresentante) {
		this.cdFilialCnpjRepresentante = cdFilialCnpjRepresentante;
	}
	
	/**
	 * Get: cdControleCnpjRepresentante.
	 *
	 * @return cdControleCnpjRepresentante
	 */
	public Integer getCdControleCnpjRepresentante() {
		return cdControleCnpjRepresentante;
	}
	
	/**
	 * Set: cdControleCnpjRepresentante.
	 *
	 * @param cdControleCnpjRepresentante the cd controle cnpj representante
	 */
	public void setCdControleCnpjRepresentante(Integer cdControleCnpjRepresentante) {
		this.cdControleCnpjRepresentante = cdControleCnpjRepresentante;
	}
	
	/**
	 * Get: nmRazaoRepresentante.
	 *
	 * @return nmRazaoRepresentante
	 */
	public String getNmRazaoRepresentante() {
		return nmRazaoRepresentante;
	}
	
	/**
	 * Set: nmRazaoRepresentante.
	 *
	 * @param nmRazaoRepresentante the nm razao representante
	 */
	public void setNmRazaoRepresentante(String nmRazaoRepresentante) {
		this.nmRazaoRepresentante = nmRazaoRepresentante;
	}
	
	/**
	 * Get: cdPessoaJuridica.
	 *
	 * @return cdPessoaJuridica
	 */
	public Long getCdPessoaJuridica() {
		return cdPessoaJuridica;
	}
	
	/**
	 * Set: cdPessoaJuridica.
	 *
	 * @param cdPessoaJuridica the cd pessoa juridica
	 */
	public void setCdPessoaJuridica(Long cdPessoaJuridica) {
		this.cdPessoaJuridica = cdPessoaJuridica;
	}
	
	/**
	 * Get: dsPessoaJuridica.
	 *
	 * @return dsPessoaJuridica
	 */
	public String getDsPessoaJuridica() {
		return dsPessoaJuridica;
	}
	
	/**
	 * Set: dsPessoaJuridica.
	 *
	 * @param dsPessoaJuridica the ds pessoa juridica
	 */
	public void setDsPessoaJuridica(String dsPessoaJuridica) {
		this.dsPessoaJuridica = dsPessoaJuridica;
	}
	
	/**
	 * Get: cdTipoContrato.
	 *
	 * @return cdTipoContrato
	 */
	public Integer getCdTipoContrato() {
		return cdTipoContrato;
	}
	
	/**
	 * Set: cdTipoContrato.
	 *
	 * @param cdTipoContrato the cd tipo contrato
	 */
	public void setCdTipoContrato(Integer cdTipoContrato) {
		this.cdTipoContrato = cdTipoContrato;
	}
	
	/**
	 * Get: dsTipoContrato.
	 *
	 * @return dsTipoContrato
	 */
	public String getDsTipoContrato() {
		return dsTipoContrato;
	}
	
	/**
	 * Set: dsTipoContrato.
	 *
	 * @param dsTipoContrato the ds tipo contrato
	 */
	public void setDsTipoContrato(String dsTipoContrato) {
		this.dsTipoContrato = dsTipoContrato;
	}
	
	/**
	 * Get: nrSequenciaContrato.
	 *
	 * @return nrSequenciaContrato
	 */
	public Long getNrSequenciaContrato() {
		return nrSequenciaContrato;
	}
	
	/**
	 * Set: nrSequenciaContrato.
	 *
	 * @param nrSequenciaContrato the nr sequencia contrato
	 */
	public void setNrSequenciaContrato(Long nrSequenciaContrato) {
		this.nrSequenciaContrato = nrSequenciaContrato;
	}
	
	/**
	 * Get: dsContrato.
	 *
	 * @return dsContrato
	 */
	public String getDsContrato() {
		return dsContrato;
	}
	
	/**
	 * Set: dsContrato.
	 *
	 * @param dsContrato the ds contrato
	 */
	public void setDsContrato(String dsContrato) {
		this.dsContrato = dsContrato;
	}
	
	/**
	 * Get: cdAgenciaOperadora.
	 *
	 * @return cdAgenciaOperadora
	 */
	public Integer getCdAgenciaOperadora() {
		return cdAgenciaOperadora;
	}
	
	/**
	 * Set: cdAgenciaOperadora.
	 *
	 * @param cdAgenciaOperadora the cd agencia operadora
	 */
	public void setCdAgenciaOperadora(Integer cdAgenciaOperadora) {
		this.cdAgenciaOperadora = cdAgenciaOperadora;
	}
	
	/**
	 * Get: cdDigitoAgenciaOperadora.
	 *
	 * @return cdDigitoAgenciaOperadora
	 */
	public String getCdDigitoAgenciaOperadora() {
		return cdDigitoAgenciaOperadora;
	}
	
	/**
	 * Set: cdDigitoAgenciaOperadora.
	 *
	 * @param cdDigitoAgenciaOperadora the cd digito agencia operadora
	 */
	public void setCdDigitoAgenciaOperadora(String cdDigitoAgenciaOperadora) {
		this.cdDigitoAgenciaOperadora = cdDigitoAgenciaOperadora;
	}
	
	/**
	 * Get: dsAgenciaOperadora.
	 *
	 * @return dsAgenciaOperadora
	 */
	public String getDsAgenciaOperadora() {
		return dsAgenciaOperadora;
	}
	
	/**
	 * Set: dsAgenciaOperadora.
	 *
	 * @param dsAgenciaOperadora the ds agencia operadora
	 */
	public void setDsAgenciaOperadora(String dsAgenciaOperadora) {
		this.dsAgenciaOperadora = dsAgenciaOperadora;
	}
	
	/**
	 * Get: cdFuncionarioBradesco.
	 *
	 * @return cdFuncionarioBradesco
	 */
	public Long getCdFuncionarioBradesco() {
		return cdFuncionarioBradesco;
	}
	
	/**
	 * Set: cdFuncionarioBradesco.
	 *
	 * @param cdFuncionarioBradesco the cd funcionario bradesco
	 */
	public void setCdFuncionarioBradesco(Long cdFuncionarioBradesco) {
		this.cdFuncionarioBradesco = cdFuncionarioBradesco;
	}
	
	/**
	 * Get: dsFuncionarioBradesco.
	 *
	 * @return dsFuncionarioBradesco
	 */
	public String getDsFuncionarioBradesco() {
		return dsFuncionarioBradesco;
	}
	
	/**
	 * Set: dsFuncionarioBradesco.
	 *
	 * @param dsFuncionarioBradesco the ds funcionario bradesco
	 */
	public void setDsFuncionarioBradesco(String dsFuncionarioBradesco) {
		this.dsFuncionarioBradesco = dsFuncionarioBradesco;
	}
	
	/**
	 * Get: cdSituacaoContrato.
	 *
	 * @return cdSituacaoContrato
	 */
	public Integer getCdSituacaoContrato() {
		return cdSituacaoContrato;
	}
	
	/**
	 * Set: cdSituacaoContrato.
	 *
	 * @param cdSituacaoContrato the cd situacao contrato
	 */
	public void setCdSituacaoContrato(Integer cdSituacaoContrato) {
		this.cdSituacaoContrato = cdSituacaoContrato;
	}
	
	/**
	 * Get: dsSituacaoContrato.
	 *
	 * @return dsSituacaoContrato
	 */
	public String getDsSituacaoContrato() {
		return dsSituacaoContrato;
	}
	
	/**
	 * Set: dsSituacaoContrato.
	 *
	 * @param dsSituacaoContrato the ds situacao contrato
	 */
	public void setDsSituacaoContrato(String dsSituacaoContrato) {
		this.dsSituacaoContrato = dsSituacaoContrato;
	}
	
	/**
	 * Get: cdMotivoSituacao.
	 *
	 * @return cdMotivoSituacao
	 */
	public Integer getCdMotivoSituacao() {
		return cdMotivoSituacao;
	}
	
	/**
	 * Set: cdMotivoSituacao.
	 *
	 * @param cdMotivoSituacao the cd motivo situacao
	 */
	public void setCdMotivoSituacao(Integer cdMotivoSituacao) {
		this.cdMotivoSituacao = cdMotivoSituacao;
	}
	
	/**
	 * Get: dsMotivoSituacao.
	 *
	 * @return dsMotivoSituacao
	 */
	public String getDsMotivoSituacao() {
		return dsMotivoSituacao;
	}
	
	/**
	 * Set: dsMotivoSituacao.
	 *
	 * @param dsMotivoSituacao the ds motivo situacao
	 */
	public void setDsMotivoSituacao(String dsMotivoSituacao) {
		this.dsMotivoSituacao = dsMotivoSituacao;
	}
	
	/**
	 * Get: cdAditivo.
	 *
	 * @return cdAditivo
	 */
	public String getCdAditivo() {
		return cdAditivo;
	}
	
	/**
	 * Set: cdAditivo.
	 *
	 * @param cdAditivo the cd aditivo
	 */
	public void setCdAditivo(String cdAditivo) {
		this.cdAditivo = cdAditivo;
	}
	
	/**
	 * Get: cdTipoParticipacao.
	 *
	 * @return cdTipoParticipacao
	 */
	public String getCdTipoParticipacao() {
		return cdTipoParticipacao;
	}
	
	/**
	 * Set: cdTipoParticipacao.
	 *
	 * @param cdTipoParticipacao the cd tipo participacao
	 */
	public void setCdTipoParticipacao(String cdTipoParticipacao) {
		this.cdTipoParticipacao = cdTipoParticipacao;
	}
	
	/**
	 * Get: cdSegmentoCliente.
	 *
	 * @return cdSegmentoCliente
	 */
	public Integer getCdSegmentoCliente() {
		return cdSegmentoCliente;
	}
	
	/**
	 * Set: cdSegmentoCliente.
	 *
	 * @param cdSegmentoCliente the cd segmento cliente
	 */
	public void setCdSegmentoCliente(Integer cdSegmentoCliente) {
		this.cdSegmentoCliente = cdSegmentoCliente;
	}
	
	/**
	 * Get: dsSegmentoCliente.
	 *
	 * @return dsSegmentoCliente
	 */
	public String getDsSegmentoCliente() {
		return dsSegmentoCliente;
	}
	
	/**
	 * Set: dsSegmentoCliente.
	 *
	 * @param dsSegmentoCliente the ds segmento cliente
	 */
	public void setDsSegmentoCliente(String dsSegmentoCliente) {
		this.dsSegmentoCliente = dsSegmentoCliente;
	}
	
	/**
	 * Get: cdSubSegmentoCliente.
	 *
	 * @return cdSubSegmentoCliente
	 */
	public Integer getCdSubSegmentoCliente() {
		return cdSubSegmentoCliente;
	}
	
	/**
	 * Set: cdSubSegmentoCliente.
	 *
	 * @param cdSubSegmentoCliente the cd sub segmento cliente
	 */
	public void setCdSubSegmentoCliente(Integer cdSubSegmentoCliente) {
		this.cdSubSegmentoCliente = cdSubSegmentoCliente;
	}
	
	/**
	 * Get: dsSubSegmentoCliente.
	 *
	 * @return dsSubSegmentoCliente
	 */
	public String getDsSubSegmentoCliente() {
		return dsSubSegmentoCliente;
	}
	
	/**
	 * Set: dsSubSegmentoCliente.
	 *
	 * @param dsSubSegmentoCliente the ds sub segmento cliente
	 */
	public void setDsSubSegmentoCliente(String dsSubSegmentoCliente) {
		this.dsSubSegmentoCliente = dsSubSegmentoCliente;
	}
	
	/**
	 * Get: cdAtividadeEconomica.
	 *
	 * @return cdAtividadeEconomica
	 */
	public Integer getCdAtividadeEconomica() {
		return cdAtividadeEconomica;
	}
	
	/**
	 * Set: cdAtividadeEconomica.
	 *
	 * @param cdAtividadeEconomica the cd atividade economica
	 */
	public void setCdAtividadeEconomica(Integer cdAtividadeEconomica) {
		this.cdAtividadeEconomica = cdAtividadeEconomica;
	}
	
	/**
	 * Get: dsAtividadeEconomica.
	 *
	 * @return dsAtividadeEconomica
	 */
	public String getDsAtividadeEconomica() {
		return dsAtividadeEconomica;
	}
	
	/**
	 * Set: dsAtividadeEconomica.
	 *
	 * @param dsAtividadeEconomica the ds atividade economica
	 */
	public void setDsAtividadeEconomica(String dsAtividadeEconomica) {
		this.dsAtividadeEconomica = dsAtividadeEconomica;
	}
	
	/**
	 * Get: cdGrupoEconomico.
	 *
	 * @return cdGrupoEconomico
	 */
	public Long getCdGrupoEconomico() {
		return cdGrupoEconomico;
	}
	
	/**
	 * Set: cdGrupoEconomico.
	 *
	 * @param cdGrupoEconomico the cd grupo economico
	 */
	public void setCdGrupoEconomico(Long cdGrupoEconomico) {
		this.cdGrupoEconomico = cdGrupoEconomico;
	}
	
	/**
	 * Get: dsGrupoEconomico.
	 *
	 * @return dsGrupoEconomico
	 */
	public String getDsGrupoEconomico() {
		return dsGrupoEconomico;
	}
	
	/**
	 * Set: dsGrupoEconomico.
	 *
	 * @param dsGrupoEconomico the ds grupo economico
	 */
	public void setDsGrupoEconomico(String dsGrupoEconomico) {
		this.dsGrupoEconomico = dsGrupoEconomico;
	}
	
	/**
	 * Get: dtAbertura.
	 *
	 * @return dtAbertura
	 */
	public Date getDtAbertura() {
		return dtAbertura;
	}
	
	/**
	 * Set: dtAbertura.
	 *
	 * @param dtAbertura the dt abertura
	 */
	public void setDtAbertura(Date dtAbertura) {
		this.dtAbertura = dtAbertura;
	}
	
	/**
	 * Get: dtEncerramento.
	 *
	 * @return dtEncerramento
	 */
	public Date getDtEncerramento() {
		return dtEncerramento;
	}
	
	/**
	 * Set: dtEncerramento.
	 *
	 * @param dtEncerramento the dt encerramento
	 */
	public void setDtEncerramento(Date dtEncerramento) {
		this.dtEncerramento = dtEncerramento;
	}
	
	/**
	 * Get: dtInclusao.
	 *
	 * @return dtInclusao
	 */
	public Date getDtInclusao() {
		return dtInclusao;
	}
	
	/**
	 * Set: dtInclusao.
	 *
	 * @param dtInclusao the dt inclusao
	 */
	public void setDtInclusao(Date dtInclusao) {
		this.dtInclusao = dtInclusao;
	}
    
    /**
     * Get: cpfCnpjFormatado.
     *
     * @return cpfCnpjFormatado
     */
    public String getCpfCnpjFormatado(){
    	return CpfCnpjUtils.formatarCpfCnpj(cdCorpoCnpjRepresentante, cdFilialCnpjRepresentante, cdControleCnpjRepresentante);
    }
}
