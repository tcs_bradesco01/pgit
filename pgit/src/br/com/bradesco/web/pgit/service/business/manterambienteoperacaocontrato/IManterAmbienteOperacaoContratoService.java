/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/interfaz.ftl,v $
 * $Id: interfaz.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: interfaz.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.manterambienteoperacaocontrato;

import java.util.List;

import br.com.bradesco.web.pgit.service.business.manterambienteoperacaocontrato.bean.ConsultarContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterambienteoperacaocontrato.bean.ConsultarContratoOcorrenciasDTO;
import br.com.bradesco.web.pgit.service.business.manterambienteoperacaocontrato.bean.ConsultarSolicitacaoAmbienteEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterambienteoperacaocontrato.bean.ConsultarSolicitacaoAmbientePartcEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterambienteoperacaocontrato.bean.ConsultarSolicitacaoAmbientePartcSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterambienteoperacaocontrato.bean.ConsultarSolicitacaoAmbienteSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterambienteoperacaocontrato.bean.ConsultarSolicitacaoMudancaAmbPartcEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterambienteoperacaocontrato.bean.ConsultarSolicitacaoMudancaAmbPartcOcorrenciasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterambienteoperacaocontrato.bean.ConsultarSolicitacaoMudancaAmbienteEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterambienteoperacaocontrato.bean.ConsultarSolicitacaoMudancaAmbienteSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterambienteoperacaocontrato.bean.DetalharAmbienteEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterambienteoperacaocontrato.bean.DetalharAmbienteSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterambienteoperacaocontrato.bean.ExcluirAlteracaoAmbienteEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterambienteoperacaocontrato.bean.ExcluirAlteracaoAmbientePartcEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterambienteoperacaocontrato.bean.ExcluirAlteracaoAmbientePartcSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterambienteoperacaocontrato.bean.ExcluirAlteracaoAmbienteSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterambienteoperacaocontrato.bean.IncluirAlteracaoAmbienteEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterambienteoperacaocontrato.bean.IncluirAlteracaoAmbientePartcEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterambienteoperacaocontrato.bean.IncluirAlteracaoAmbientePartcSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterambienteoperacaocontrato.bean.IncluirAlteracaoAmbienteSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterambienteoperacaocontrato.bean.ListarAmbienteEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterambienteoperacaocontrato.bean.ListarAmbienteOcorrenciaSaidaDTO;

/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Interface do adaptador: ManterAmbienteOperacaoContrato
 * </p>
 * 
 * @comment CODIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public interface IManterAmbienteOperacaoContratoService {

	/**
	 * Consultar solicitacao ambiente.
	 * 
	 * @param consultarSolicitacaoAmbienteEntradaDTO
	 *            the consultar solicitacao ambiente entrada dto
	 * @return the consultar solicitacao ambiente saida dto
	 */
	ConsultarSolicitacaoAmbienteSaidaDTO consultarSolicitacaoAmbiente(
			ConsultarSolicitacaoAmbienteEntradaDTO consultarSolicitacaoAmbienteEntradaDTO);

	/**
	 * Consultar solicitacao mudanca ambiente.
	 * 
	 * @param consultarSolicitacaoMudancaAmbienteEntradaDTO
	 *            the consultar solicitacao mudanca ambiente entrada dto
	 * @return the list< consultar solicitacao mudanca ambiente saida dt o>
	 */
	List<ConsultarSolicitacaoMudancaAmbienteSaidaDTO> consultarSolicitacaoMudancaAmbiente(
			ConsultarSolicitacaoMudancaAmbienteEntradaDTO consultarSolicitacaoMudancaAmbienteEntradaDTO);

	/**
	 * Incluir alteracao ambiente.
	 * 
	 * @param incluirAlteracaoAmbienteEntradaDTO
	 *            the incluir alteracao ambiente entrada dto
	 * @return the incluir alteracao ambiente saida dto
	 */
	IncluirAlteracaoAmbienteSaidaDTO incluirAlteracaoAmbiente(
			IncluirAlteracaoAmbienteEntradaDTO incluirAlteracaoAmbienteEntradaDTO);

	/**
	 * Excluir alteracao ambiente.
	 * 
	 * @param excluirAlteracaoAmbienteEntradaDTO
	 *            the excluir alteracao ambiente entrada dto
	 * @return the excluir alteracao ambiente saida dto
	 */
	ExcluirAlteracaoAmbienteSaidaDTO excluirAlteracaoAmbiente(
			ExcluirAlteracaoAmbienteEntradaDTO excluirAlteracaoAmbienteEntradaDTO);

	/**
	 * Consultar solicitacao mudanca amb partc.
	 * 
	 * @param entradaDTO
	 *            the entrada dto
	 * @return the list< consultar solicitacao mudanca amb partc ocorrencias
	 *         saida dt o>
	 */
	List<ConsultarSolicitacaoMudancaAmbPartcOcorrenciasSaidaDTO> consultarSolicitacaoMudancaAmbPartc(
			ConsultarSolicitacaoMudancaAmbPartcEntradaDTO entradaDTO);

	/**
	 * Listar ambiente.
	 * 
	 * @param entradaDTO
	 *            the entrada dto
	 * @return the list< listar ambiente ocorrencia saida dt o>
	 */
	List<ListarAmbienteOcorrenciaSaidaDTO> listarAmbiente(
			ListarAmbienteEntradaDTO entradaDTO);

	/**
	 * Incluir alteracao ambiente partc.
	 * 
	 * @param incluirAlteracaoAmbienteEntradaDTO
	 *            the incluir alteracao ambiente entrada dto
	 * @return the incluir alteracao ambiente partc saida dto
	 */
	IncluirAlteracaoAmbientePartcSaidaDTO incluirAlteracaoAmbientePartc(
			IncluirAlteracaoAmbientePartcEntradaDTO incluirAlteracaoAmbienteEntradaDTO);

	/**
	 * Consultar solicitacao ambiente partc.
	 * 
	 * @param consultarSolicitacaoAmbientePartcEntradaDTO
	 *            the consultar solicitacao ambiente partc entrada dto
	 * @return the consultar solicitacao ambiente partc saida dto
	 */
	ConsultarSolicitacaoAmbientePartcSaidaDTO consultarSolicitacaoAmbientePartc(
			ConsultarSolicitacaoAmbientePartcEntradaDTO consultarSolicitacaoAmbientePartcEntradaDTO);

	/**
	 * Excluir alteracao ambiente partc.
	 * 
	 * @param entrada
	 *            the entrada
	 * @return the excluir alteracao ambiente partc saida dto
	 */
	ExcluirAlteracaoAmbientePartcSaidaDTO excluirAlteracaoAmbientePartc(
			ExcluirAlteracaoAmbientePartcEntradaDTO entrada);

	/**
	 * Detalhar ambiente.
	 * 
	 * @param entrada
	 *            the entrada
	 * @return the detalhar ambiente saida dto
	 */
	DetalharAmbienteSaidaDTO detalharAmbiente(DetalharAmbienteEntradaDTO entrada);

	public List<ConsultarContratoOcorrenciasDTO> consultarContratos(
			ConsultarContratoEntradaDTO entrada);
}
