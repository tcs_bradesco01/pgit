/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.solicitarrelatoriocontratos.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class SolicitarRelatorioContratosRequest.
 * 
 * @version $Revision$ $Date$
 */
public class SolicitarRelatorioContratosRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdSolicitacaoPagamentoIntegrado
     */
    private int _cdSolicitacaoPagamentoIntegrado = 0;

    /**
     * keeps track of state for field:
     * _cdSolicitacaoPagamentoIntegrado
     */
    private boolean _has_cdSolicitacaoPagamentoIntegrado;

    /**
     * Field _cdProdutoServicoOper
     */
    private int _cdProdutoServicoOper = 0;

    /**
     * keeps track of state for field: _cdProdutoServicoOper
     */
    private boolean _has_cdProdutoServicoOper;

    /**
     * Field _cdProdutoOperRelacionado
     */
    private int _cdProdutoOperRelacionado = 0;

    /**
     * keeps track of state for field: _cdProdutoOperRelacionado
     */
    private boolean _has_cdProdutoOperRelacionado;

    /**
     * Field _cdRlctoProduto
     */
    private int _cdRlctoProduto = 0;

    /**
     * keeps track of state for field: _cdRlctoProduto
     */
    private boolean _has_cdRlctoProduto;

    /**
     * Field _cdPessoaJuridDir
     */
    private long _cdPessoaJuridDir = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridDir
     */
    private boolean _has_cdPessoaJuridDir;

    /**
     * Field _nrSequenciaUnidadeDir
     */
    private int _nrSequenciaUnidadeDir = 0;

    /**
     * keeps track of state for field: _nrSequenciaUnidadeDir
     */
    private boolean _has_nrSequenciaUnidadeDir;

    /**
     * Field _cdPessoaJuridGerc
     */
    private long _cdPessoaJuridGerc = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridGerc
     */
    private boolean _has_cdPessoaJuridGerc;

    /**
     * Field _nrSequenciaUnidadeGer
     */
    private int _nrSequenciaUnidadeGer = 0;

    /**
     * keeps track of state for field: _nrSequenciaUnidadeGer
     */
    private boolean _has_nrSequenciaUnidadeGer;

    /**
     * Field _cdPessoaJuridNegocio
     */
    private long _cdPessoaJuridNegocio = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridNegocio
     */
    private boolean _has_cdPessoaJuridNegocio;

    /**
     * Field _nrSequenciaUnidadeOper
     */
    private int _nrSequenciaUnidadeOper = 0;

    /**
     * keeps track of state for field: _nrSequenciaUnidadeOper
     */
    private boolean _has_nrSequenciaUnidadeOper;

    /**
     * Field _cdTipoRelatorioPagamento
     */
    private int _cdTipoRelatorioPagamento = 0;

    /**
     * keeps track of state for field: _cdTipoRelatorioPagamento
     */
    private boolean _has_cdTipoRelatorioPagamento;

    /**
     * Field _nrTipoClasfPessoa
     */
    private int _nrTipoClasfPessoa = 0;

    /**
     * keeps track of state for field: _nrTipoClasfPessoa
     */
    private boolean _has_nrTipoClasfPessoa;

    /**
     * Field _cdGrpEconomicoCliente
     */
    private long _cdGrpEconomicoCliente = 0;

    /**
     * keeps track of state for field: _cdGrpEconomicoCliente
     */
    private boolean _has_cdGrpEconomicoCliente;

    /**
     * Field _cdClassAtividadeEconomica
     */
    private java.lang.String _cdClassAtividadeEconomica;

    /**
     * Field _cdRamoAtividadeEconomica
     */
    private int _cdRamoAtividadeEconomica = 0;

    /**
     * keeps track of state for field: _cdRamoAtividadeEconomica
     */
    private boolean _has_cdRamoAtividadeEconomica;

    /**
     * Field _cdSubRamoAtividadeEconomica
     */
    private int _cdSubRamoAtividadeEconomica = 0;

    /**
     * keeps track of state for field: _cdSubRamoAtividadeEconomica
     */
    private boolean _has_cdSubRamoAtividadeEconomica;

    /**
     * Field _cdAtividadeEconomica
     */
    private int _cdAtividadeEconomica = 0;

    /**
     * keeps track of state for field: _cdAtividadeEconomica
     */
    private boolean _has_cdAtividadeEconomica;


      //----------------/
     //- Constructors -/
    //----------------/

    public SolicitarRelatorioContratosRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.solicitarrelatoriocontratos.request.SolicitarRelatorioContratosRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdAtividadeEconomica
     * 
     */
    public void deleteCdAtividadeEconomica()
    {
        this._has_cdAtividadeEconomica= false;
    } //-- void deleteCdAtividadeEconomica() 

    /**
     * Method deleteCdGrpEconomicoCliente
     * 
     */
    public void deleteCdGrpEconomicoCliente()
    {
        this._has_cdGrpEconomicoCliente= false;
    } //-- void deleteCdGrpEconomicoCliente() 

    /**
     * Method deleteCdPessoaJuridDir
     * 
     */
    public void deleteCdPessoaJuridDir()
    {
        this._has_cdPessoaJuridDir= false;
    } //-- void deleteCdPessoaJuridDir() 

    /**
     * Method deleteCdPessoaJuridGerc
     * 
     */
    public void deleteCdPessoaJuridGerc()
    {
        this._has_cdPessoaJuridGerc= false;
    } //-- void deleteCdPessoaJuridGerc() 

    /**
     * Method deleteCdPessoaJuridNegocio
     * 
     */
    public void deleteCdPessoaJuridNegocio()
    {
        this._has_cdPessoaJuridNegocio= false;
    } //-- void deleteCdPessoaJuridNegocio() 

    /**
     * Method deleteCdProdutoOperRelacionado
     * 
     */
    public void deleteCdProdutoOperRelacionado()
    {
        this._has_cdProdutoOperRelacionado= false;
    } //-- void deleteCdProdutoOperRelacionado() 

    /**
     * Method deleteCdProdutoServicoOper
     * 
     */
    public void deleteCdProdutoServicoOper()
    {
        this._has_cdProdutoServicoOper= false;
    } //-- void deleteCdProdutoServicoOper() 

    /**
     * Method deleteCdRamoAtividadeEconomica
     * 
     */
    public void deleteCdRamoAtividadeEconomica()
    {
        this._has_cdRamoAtividadeEconomica= false;
    } //-- void deleteCdRamoAtividadeEconomica() 

    /**
     * Method deleteCdRlctoProduto
     * 
     */
    public void deleteCdRlctoProduto()
    {
        this._has_cdRlctoProduto= false;
    } //-- void deleteCdRlctoProduto() 

    /**
     * Method deleteCdSolicitacaoPagamentoIntegrado
     * 
     */
    public void deleteCdSolicitacaoPagamentoIntegrado()
    {
        this._has_cdSolicitacaoPagamentoIntegrado= false;
    } //-- void deleteCdSolicitacaoPagamentoIntegrado() 

    /**
     * Method deleteCdSubRamoAtividadeEconomica
     * 
     */
    public void deleteCdSubRamoAtividadeEconomica()
    {
        this._has_cdSubRamoAtividadeEconomica= false;
    } //-- void deleteCdSubRamoAtividadeEconomica() 

    /**
     * Method deleteCdTipoRelatorioPagamento
     * 
     */
    public void deleteCdTipoRelatorioPagamento()
    {
        this._has_cdTipoRelatorioPagamento= false;
    } //-- void deleteCdTipoRelatorioPagamento() 

    /**
     * Method deleteNrSequenciaUnidadeDir
     * 
     */
    public void deleteNrSequenciaUnidadeDir()
    {
        this._has_nrSequenciaUnidadeDir= false;
    } //-- void deleteNrSequenciaUnidadeDir() 

    /**
     * Method deleteNrSequenciaUnidadeGer
     * 
     */
    public void deleteNrSequenciaUnidadeGer()
    {
        this._has_nrSequenciaUnidadeGer= false;
    } //-- void deleteNrSequenciaUnidadeGer() 

    /**
     * Method deleteNrSequenciaUnidadeOper
     * 
     */
    public void deleteNrSequenciaUnidadeOper()
    {
        this._has_nrSequenciaUnidadeOper= false;
    } //-- void deleteNrSequenciaUnidadeOper() 

    /**
     * Method deleteNrTipoClasfPessoa
     * 
     */
    public void deleteNrTipoClasfPessoa()
    {
        this._has_nrTipoClasfPessoa= false;
    } //-- void deleteNrTipoClasfPessoa() 

    /**
     * Returns the value of field 'cdAtividadeEconomica'.
     * 
     * @return int
     * @return the value of field 'cdAtividadeEconomica'.
     */
    public int getCdAtividadeEconomica()
    {
        return this._cdAtividadeEconomica;
    } //-- int getCdAtividadeEconomica() 

    /**
     * Returns the value of field 'cdClassAtividadeEconomica'.
     * 
     * @return String
     * @return the value of field 'cdClassAtividadeEconomica'.
     */
    public java.lang.String getCdClassAtividadeEconomica()
    {
        return this._cdClassAtividadeEconomica;
    } //-- java.lang.String getCdClassAtividadeEconomica() 

    /**
     * Returns the value of field 'cdGrpEconomicoCliente'.
     * 
     * @return long
     * @return the value of field 'cdGrpEconomicoCliente'.
     */
    public long getCdGrpEconomicoCliente()
    {
        return this._cdGrpEconomicoCliente;
    } //-- long getCdGrpEconomicoCliente() 

    /**
     * Returns the value of field 'cdPessoaJuridDir'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridDir'.
     */
    public long getCdPessoaJuridDir()
    {
        return this._cdPessoaJuridDir;
    } //-- long getCdPessoaJuridDir() 

    /**
     * Returns the value of field 'cdPessoaJuridGerc'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridGerc'.
     */
    public long getCdPessoaJuridGerc()
    {
        return this._cdPessoaJuridGerc;
    } //-- long getCdPessoaJuridGerc() 

    /**
     * Returns the value of field 'cdPessoaJuridNegocio'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridNegocio'.
     */
    public long getCdPessoaJuridNegocio()
    {
        return this._cdPessoaJuridNegocio;
    } //-- long getCdPessoaJuridNegocio() 

    /**
     * Returns the value of field 'cdProdutoOperRelacionado'.
     * 
     * @return int
     * @return the value of field 'cdProdutoOperRelacionado'.
     */
    public int getCdProdutoOperRelacionado()
    {
        return this._cdProdutoOperRelacionado;
    } //-- int getCdProdutoOperRelacionado() 

    /**
     * Returns the value of field 'cdProdutoServicoOper'.
     * 
     * @return int
     * @return the value of field 'cdProdutoServicoOper'.
     */
    public int getCdProdutoServicoOper()
    {
        return this._cdProdutoServicoOper;
    } //-- int getCdProdutoServicoOper() 

    /**
     * Returns the value of field 'cdRamoAtividadeEconomica'.
     * 
     * @return int
     * @return the value of field 'cdRamoAtividadeEconomica'.
     */
    public int getCdRamoAtividadeEconomica()
    {
        return this._cdRamoAtividadeEconomica;
    } //-- int getCdRamoAtividadeEconomica() 

    /**
     * Returns the value of field 'cdRlctoProduto'.
     * 
     * @return int
     * @return the value of field 'cdRlctoProduto'.
     */
    public int getCdRlctoProduto()
    {
        return this._cdRlctoProduto;
    } //-- int getCdRlctoProduto() 

    /**
     * Returns the value of field
     * 'cdSolicitacaoPagamentoIntegrado'.
     * 
     * @return int
     * @return the value of field 'cdSolicitacaoPagamentoIntegrado'.
     */
    public int getCdSolicitacaoPagamentoIntegrado()
    {
        return this._cdSolicitacaoPagamentoIntegrado;
    } //-- int getCdSolicitacaoPagamentoIntegrado() 

    /**
     * Returns the value of field 'cdSubRamoAtividadeEconomica'.
     * 
     * @return int
     * @return the value of field 'cdSubRamoAtividadeEconomica'.
     */
    public int getCdSubRamoAtividadeEconomica()
    {
        return this._cdSubRamoAtividadeEconomica;
    } //-- int getCdSubRamoAtividadeEconomica() 

    /**
     * Returns the value of field 'cdTipoRelatorioPagamento'.
     * 
     * @return int
     * @return the value of field 'cdTipoRelatorioPagamento'.
     */
    public int getCdTipoRelatorioPagamento()
    {
        return this._cdTipoRelatorioPagamento;
    } //-- int getCdTipoRelatorioPagamento() 

    /**
     * Returns the value of field 'nrSequenciaUnidadeDir'.
     * 
     * @return int
     * @return the value of field 'nrSequenciaUnidadeDir'.
     */
    public int getNrSequenciaUnidadeDir()
    {
        return this._nrSequenciaUnidadeDir;
    } //-- int getNrSequenciaUnidadeDir() 

    /**
     * Returns the value of field 'nrSequenciaUnidadeGer'.
     * 
     * @return int
     * @return the value of field 'nrSequenciaUnidadeGer'.
     */
    public int getNrSequenciaUnidadeGer()
    {
        return this._nrSequenciaUnidadeGer;
    } //-- int getNrSequenciaUnidadeGer() 

    /**
     * Returns the value of field 'nrSequenciaUnidadeOper'.
     * 
     * @return int
     * @return the value of field 'nrSequenciaUnidadeOper'.
     */
    public int getNrSequenciaUnidadeOper()
    {
        return this._nrSequenciaUnidadeOper;
    } //-- int getNrSequenciaUnidadeOper() 

    /**
     * Returns the value of field 'nrTipoClasfPessoa'.
     * 
     * @return int
     * @return the value of field 'nrTipoClasfPessoa'.
     */
    public int getNrTipoClasfPessoa()
    {
        return this._nrTipoClasfPessoa;
    } //-- int getNrTipoClasfPessoa() 

    /**
     * Method hasCdAtividadeEconomica
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAtividadeEconomica()
    {
        return this._has_cdAtividadeEconomica;
    } //-- boolean hasCdAtividadeEconomica() 

    /**
     * Method hasCdGrpEconomicoCliente
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdGrpEconomicoCliente()
    {
        return this._has_cdGrpEconomicoCliente;
    } //-- boolean hasCdGrpEconomicoCliente() 

    /**
     * Method hasCdPessoaJuridDir
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridDir()
    {
        return this._has_cdPessoaJuridDir;
    } //-- boolean hasCdPessoaJuridDir() 

    /**
     * Method hasCdPessoaJuridGerc
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridGerc()
    {
        return this._has_cdPessoaJuridGerc;
    } //-- boolean hasCdPessoaJuridGerc() 

    /**
     * Method hasCdPessoaJuridNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridNegocio()
    {
        return this._has_cdPessoaJuridNegocio;
    } //-- boolean hasCdPessoaJuridNegocio() 

    /**
     * Method hasCdProdutoOperRelacionado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdutoOperRelacionado()
    {
        return this._has_cdProdutoOperRelacionado;
    } //-- boolean hasCdProdutoOperRelacionado() 

    /**
     * Method hasCdProdutoServicoOper
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdutoServicoOper()
    {
        return this._has_cdProdutoServicoOper;
    } //-- boolean hasCdProdutoServicoOper() 

    /**
     * Method hasCdRamoAtividadeEconomica
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdRamoAtividadeEconomica()
    {
        return this._has_cdRamoAtividadeEconomica;
    } //-- boolean hasCdRamoAtividadeEconomica() 

    /**
     * Method hasCdRlctoProduto
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdRlctoProduto()
    {
        return this._has_cdRlctoProduto;
    } //-- boolean hasCdRlctoProduto() 

    /**
     * Method hasCdSolicitacaoPagamentoIntegrado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSolicitacaoPagamentoIntegrado()
    {
        return this._has_cdSolicitacaoPagamentoIntegrado;
    } //-- boolean hasCdSolicitacaoPagamentoIntegrado() 

    /**
     * Method hasCdSubRamoAtividadeEconomica
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSubRamoAtividadeEconomica()
    {
        return this._has_cdSubRamoAtividadeEconomica;
    } //-- boolean hasCdSubRamoAtividadeEconomica() 

    /**
     * Method hasCdTipoRelatorioPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoRelatorioPagamento()
    {
        return this._has_cdTipoRelatorioPagamento;
    } //-- boolean hasCdTipoRelatorioPagamento() 

    /**
     * Method hasNrSequenciaUnidadeDir
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaUnidadeDir()
    {
        return this._has_nrSequenciaUnidadeDir;
    } //-- boolean hasNrSequenciaUnidadeDir() 

    /**
     * Method hasNrSequenciaUnidadeGer
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaUnidadeGer()
    {
        return this._has_nrSequenciaUnidadeGer;
    } //-- boolean hasNrSequenciaUnidadeGer() 

    /**
     * Method hasNrSequenciaUnidadeOper
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaUnidadeOper()
    {
        return this._has_nrSequenciaUnidadeOper;
    } //-- boolean hasNrSequenciaUnidadeOper() 

    /**
     * Method hasNrTipoClasfPessoa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrTipoClasfPessoa()
    {
        return this._has_nrTipoClasfPessoa;
    } //-- boolean hasNrTipoClasfPessoa() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdAtividadeEconomica'.
     * 
     * @param cdAtividadeEconomica the value of field
     * 'cdAtividadeEconomica'.
     */
    public void setCdAtividadeEconomica(int cdAtividadeEconomica)
    {
        this._cdAtividadeEconomica = cdAtividadeEconomica;
        this._has_cdAtividadeEconomica = true;
    } //-- void setCdAtividadeEconomica(int) 

    /**
     * Sets the value of field 'cdClassAtividadeEconomica'.
     * 
     * @param cdClassAtividadeEconomica the value of field
     * 'cdClassAtividadeEconomica'.
     */
    public void setCdClassAtividadeEconomica(java.lang.String cdClassAtividadeEconomica)
    {
        this._cdClassAtividadeEconomica = cdClassAtividadeEconomica;
    } //-- void setCdClassAtividadeEconomica(java.lang.String) 

    /**
     * Sets the value of field 'cdGrpEconomicoCliente'.
     * 
     * @param cdGrpEconomicoCliente the value of field
     * 'cdGrpEconomicoCliente'.
     */
    public void setCdGrpEconomicoCliente(long cdGrpEconomicoCliente)
    {
        this._cdGrpEconomicoCliente = cdGrpEconomicoCliente;
        this._has_cdGrpEconomicoCliente = true;
    } //-- void setCdGrpEconomicoCliente(long) 

    /**
     * Sets the value of field 'cdPessoaJuridDir'.
     * 
     * @param cdPessoaJuridDir the value of field 'cdPessoaJuridDir'
     */
    public void setCdPessoaJuridDir(long cdPessoaJuridDir)
    {
        this._cdPessoaJuridDir = cdPessoaJuridDir;
        this._has_cdPessoaJuridDir = true;
    } //-- void setCdPessoaJuridDir(long) 

    /**
     * Sets the value of field 'cdPessoaJuridGerc'.
     * 
     * @param cdPessoaJuridGerc the value of field
     * 'cdPessoaJuridGerc'.
     */
    public void setCdPessoaJuridGerc(long cdPessoaJuridGerc)
    {
        this._cdPessoaJuridGerc = cdPessoaJuridGerc;
        this._has_cdPessoaJuridGerc = true;
    } //-- void setCdPessoaJuridGerc(long) 

    /**
     * Sets the value of field 'cdPessoaJuridNegocio'.
     * 
     * @param cdPessoaJuridNegocio the value of field
     * 'cdPessoaJuridNegocio'.
     */
    public void setCdPessoaJuridNegocio(long cdPessoaJuridNegocio)
    {
        this._cdPessoaJuridNegocio = cdPessoaJuridNegocio;
        this._has_cdPessoaJuridNegocio = true;
    } //-- void setCdPessoaJuridNegocio(long) 

    /**
     * Sets the value of field 'cdProdutoOperRelacionado'.
     * 
     * @param cdProdutoOperRelacionado the value of field
     * 'cdProdutoOperRelacionado'.
     */
    public void setCdProdutoOperRelacionado(int cdProdutoOperRelacionado)
    {
        this._cdProdutoOperRelacionado = cdProdutoOperRelacionado;
        this._has_cdProdutoOperRelacionado = true;
    } //-- void setCdProdutoOperRelacionado(int) 

    /**
     * Sets the value of field 'cdProdutoServicoOper'.
     * 
     * @param cdProdutoServicoOper the value of field
     * 'cdProdutoServicoOper'.
     */
    public void setCdProdutoServicoOper(int cdProdutoServicoOper)
    {
        this._cdProdutoServicoOper = cdProdutoServicoOper;
        this._has_cdProdutoServicoOper = true;
    } //-- void setCdProdutoServicoOper(int) 

    /**
     * Sets the value of field 'cdRamoAtividadeEconomica'.
     * 
     * @param cdRamoAtividadeEconomica the value of field
     * 'cdRamoAtividadeEconomica'.
     */
    public void setCdRamoAtividadeEconomica(int cdRamoAtividadeEconomica)
    {
        this._cdRamoAtividadeEconomica = cdRamoAtividadeEconomica;
        this._has_cdRamoAtividadeEconomica = true;
    } //-- void setCdRamoAtividadeEconomica(int) 

    /**
     * Sets the value of field 'cdRlctoProduto'.
     * 
     * @param cdRlctoProduto the value of field 'cdRlctoProduto'.
     */
    public void setCdRlctoProduto(int cdRlctoProduto)
    {
        this._cdRlctoProduto = cdRlctoProduto;
        this._has_cdRlctoProduto = true;
    } //-- void setCdRlctoProduto(int) 

    /**
     * Sets the value of field 'cdSolicitacaoPagamentoIntegrado'.
     * 
     * @param cdSolicitacaoPagamentoIntegrado the value of field
     * 'cdSolicitacaoPagamentoIntegrado'.
     */
    public void setCdSolicitacaoPagamentoIntegrado(int cdSolicitacaoPagamentoIntegrado)
    {
        this._cdSolicitacaoPagamentoIntegrado = cdSolicitacaoPagamentoIntegrado;
        this._has_cdSolicitacaoPagamentoIntegrado = true;
    } //-- void setCdSolicitacaoPagamentoIntegrado(int) 

    /**
     * Sets the value of field 'cdSubRamoAtividadeEconomica'.
     * 
     * @param cdSubRamoAtividadeEconomica the value of field
     * 'cdSubRamoAtividadeEconomica'.
     */
    public void setCdSubRamoAtividadeEconomica(int cdSubRamoAtividadeEconomica)
    {
        this._cdSubRamoAtividadeEconomica = cdSubRamoAtividadeEconomica;
        this._has_cdSubRamoAtividadeEconomica = true;
    } //-- void setCdSubRamoAtividadeEconomica(int) 

    /**
     * Sets the value of field 'cdTipoRelatorioPagamento'.
     * 
     * @param cdTipoRelatorioPagamento the value of field
     * 'cdTipoRelatorioPagamento'.
     */
    public void setCdTipoRelatorioPagamento(int cdTipoRelatorioPagamento)
    {
        this._cdTipoRelatorioPagamento = cdTipoRelatorioPagamento;
        this._has_cdTipoRelatorioPagamento = true;
    } //-- void setCdTipoRelatorioPagamento(int) 

    /**
     * Sets the value of field 'nrSequenciaUnidadeDir'.
     * 
     * @param nrSequenciaUnidadeDir the value of field
     * 'nrSequenciaUnidadeDir'.
     */
    public void setNrSequenciaUnidadeDir(int nrSequenciaUnidadeDir)
    {
        this._nrSequenciaUnidadeDir = nrSequenciaUnidadeDir;
        this._has_nrSequenciaUnidadeDir = true;
    } //-- void setNrSequenciaUnidadeDir(int) 

    /**
     * Sets the value of field 'nrSequenciaUnidadeGer'.
     * 
     * @param nrSequenciaUnidadeGer the value of field
     * 'nrSequenciaUnidadeGer'.
     */
    public void setNrSequenciaUnidadeGer(int nrSequenciaUnidadeGer)
    {
        this._nrSequenciaUnidadeGer = nrSequenciaUnidadeGer;
        this._has_nrSequenciaUnidadeGer = true;
    } //-- void setNrSequenciaUnidadeGer(int) 

    /**
     * Sets the value of field 'nrSequenciaUnidadeOper'.
     * 
     * @param nrSequenciaUnidadeOper the value of field
     * 'nrSequenciaUnidadeOper'.
     */
    public void setNrSequenciaUnidadeOper(int nrSequenciaUnidadeOper)
    {
        this._nrSequenciaUnidadeOper = nrSequenciaUnidadeOper;
        this._has_nrSequenciaUnidadeOper = true;
    } //-- void setNrSequenciaUnidadeOper(int) 

    /**
     * Sets the value of field 'nrTipoClasfPessoa'.
     * 
     * @param nrTipoClasfPessoa the value of field
     * 'nrTipoClasfPessoa'.
     */
    public void setNrTipoClasfPessoa(int nrTipoClasfPessoa)
    {
        this._nrTipoClasfPessoa = nrTipoClasfPessoa;
        this._has_nrTipoClasfPessoa = true;
    } //-- void setNrTipoClasfPessoa(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return SolicitarRelatorioContratosRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.solicitarrelatoriocontratos.request.SolicitarRelatorioContratosRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.solicitarrelatoriocontratos.request.SolicitarRelatorioContratosRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.solicitarrelatoriocontratos.request.SolicitarRelatorioContratosRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.solicitarrelatoriocontratos.request.SolicitarRelatorioContratosRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
