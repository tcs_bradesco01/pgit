/*
 * Nome: br.com.bradesco.web.pgit.service.business.manterperfiltrocaarqlayout.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.manterperfiltrocaarqlayout.bean;

import java.math.BigDecimal;

/**
 * Nome: AlterarPerfilTrocaArquivoEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class AlterarPerfilTrocaArquivoEntradaDTO {
    
    /** Atributo cdPerfilTrocaArquivo. */
    private Long cdPerfilTrocaArquivo;

    /** Atributo cdTipoLayoutArquivo. */
    private Integer cdTipoLayoutArquivo;

    /** Atributo cdSistemaOrigemArquivo. */
    private String cdSistemaOrigemArquivo;

    /** Atributo dsUserNameCliente. */
    private String dsUserNameCliente;

    /** Atributo dsNomeArquivoRemessa. */
    private String dsNomeArquivoRemessa;

    /** Atributo dsNameArquivoRetorno. */
    private String dsNameArquivoRetorno;

    /** Atributo cdRejeicaoAcltoRemessa. */
    private Integer cdRejeicaoAcltoRemessa;

    /** Atributo qtMaxIncotRemessa. */
    private Integer qtMaxIncotRemessa;

    /** Atributo percentualInconsistenciaRejeicaoRemessa. */
    private BigDecimal percentualInconsistenciaRejeicaoRemessa;

    /** Atributo cdNivelControleRemessa. */
    private Integer cdNivelControleRemessa;

    /** Atributo cdControleNumeroRemessa. */
    private Integer cdControleNumeroRemessa;

    /** Atributo cdPeriodicidadeContagemRemessa. */
    private Integer cdPeriodicidadeContagemRemessa;

    /** Atributo nrMaximoContagemRemessa. */
    private Long nrMaximoContagemRemessa;

    /** Atributo cdMeioPrincipalRemessa. */
    private Integer cdMeioPrincipalRemessa;

    /** Atributo cdMeioAlternativoRemessa. */
    private Integer cdMeioAlternativoRemessa;

    /** Atributo cdNivelControleRetorno. */
    private Integer cdNivelControleRetorno;

    /** Atributo cdControleNumeroRetorno. */
    private Integer cdControleNumeroRetorno;

    /** Atributo cdPerdcContagemRetorno. */
    private Integer cdPerdcContagemRetorno;

    /** Atributo nrMaximoContagemRetorno. */
    private Long nrMaximoContagemRetorno;

    /** Atributo cdMeioPrincipalRetorno. */
    private Integer cdMeioPrincipalRetorno;

    /** Atributo cdMeioAlternativoRetorno. */
    private Integer cdMeioAlternativoRetorno;

    /** Atributo cdSerieAplicacaoTransmissao. */
    private String cdSerieAplicacaoTransmissao;

    /** Atributo cdEmpresaResponsavelTransporte. */
    private Long cdEmpresaResponsavelTransporte;

    /** Atributo cdApliFormat. */
    private Long cdApliFormat;

    /** Atributo cdPessoaJuridicaParceiro. */
    private Long cdPessoaJuridicaParceiro;

    /** Atributo cdEmpresaResponsavel. */
    private Integer cdEmpresaResponsavel;

    /** Atributo cdCustoTrasmicao. */
    private BigDecimal cdCustoTrasmicao;
    
    /** Atributo cdPessoaJuridica. */
    private Long cdPessoaJuridica;
    
	/** Atributo cdUnidadeOrganizacional. */
	private Integer cdUnidadeOrganizacional;
	
	/** Atributo dsNomeContatoCliente. */
	private String dsNomeContatoCliente;
	
	/** Atributo cdAreaFone. */
	private Integer cdAreaFone;
	
	/** Atributo cdFoneContatoCliente. */
	private Long cdFoneContatoCliente;
	
	/** Atributo cdRamalContatoCliente. */
	private String cdRamalContatoCliente;
	
	/** Atributo dsEmailContatoCliente. */
	private String dsEmailContatoCliente;
	
	/** Atributo dsSolicitacaoPendente. */
	private String dsSolicitacaoPendente;
	
	/** Atributo cdAreaFonePend. */
	private Integer cdAreaFonePend;
	
	/** Atributo cdFoneSolicitacaoPend. */
	private Long cdFoneSolicitacaoPend;
	
	/** Atributo cdRamalSolctPend. */
	private String cdRamalSolctPend;
	
	/** Atributo dsEmailSolctPend. */
	private String dsEmailSolctPend;
	
	/** Atributo qtMesRegistroTrafg. */
	private Long qtMesRegistroTrafg;
	
	/** Atributo dsObsGeralPerfil. */
	private String dsObsGeralPerfil;
    
	/** Atributo cdIndicadorGeracaoSegmentoB. */
	private Integer cdIndicadorGeracaoSegmentoB;
	
	/** Atributo cdIndicadorGeracaoSegmentoZ. */
	private Integer cdIndicadorGeracaoSegmentoZ;

	/** Atributo cdIndicadorCpfLayout. */
	private Integer cdIndicadorCpfLayout;
	
	/** Atributo cdIndicadorAssociacaoLayout. */
	private Integer cdIndicadorAssociacaoLayout;
	
	/** Atributo cdIndicadorContaComplementar. */
	private Integer cdIndicadorContaComplementar;
	
	/** Atributo cdContaDebito. */
	private Integer cdContaDebito;
	
    /**
     * Get: cdCustoTrasmicao.
     *
     * @return cdCustoTrasmicao
     */
    public BigDecimal getCdCustoTrasmicao() {
	return cdCustoTrasmicao;
    }

    /**
     * Set: cdCustoTrasmicao.
     *
     * @param cdCustoTrasmicao the cd custo trasmicao
     */
    public void setCdCustoTrasmicao(BigDecimal cdCustoTrasmicao) {
	this.cdCustoTrasmicao = cdCustoTrasmicao;
    }

    /**
     * Get: cdEmpresaResponsavel.
     *
     * @return cdEmpresaResponsavel
     */
    public Integer getCdEmpresaResponsavel() {
	return cdEmpresaResponsavel;
    }

    /**
     * Set: cdEmpresaResponsavel.
     *
     * @param cdEmpresaResponsavel the cd empresa responsavel
     */
    public void setCdEmpresaResponsavel(Integer cdEmpresaResponsavel) {
	this.cdEmpresaResponsavel = cdEmpresaResponsavel;
    }

    /**
     * Get: cdPessoaJuridicaParceiro.
     *
     * @return cdPessoaJuridicaParceiro
     */
    public Long getCdPessoaJuridicaParceiro() {
	return cdPessoaJuridicaParceiro;
    }

    /**
     * Set: cdPessoaJuridicaParceiro.
     *
     * @param cdPessoaJuridicaParceiro the cd pessoa juridica parceiro
     */
    public void setCdPessoaJuridicaParceiro(Long cdPessoaJuridicaParceiro) {
	this.cdPessoaJuridicaParceiro = cdPessoaJuridicaParceiro;
    }

    /**
     * Get: cdPerfilTrocaArquivo.
     *
     * @return cdPerfilTrocaArquivo
     */
    public Long getCdPerfilTrocaArquivo() {
	return cdPerfilTrocaArquivo;
    }

    /**
     * Set: cdPerfilTrocaArquivo.
     *
     * @param cdPerfilTrocaArquivo the cd perfil troca arquivo
     */
    public void setCdPerfilTrocaArquivo(Long cdPerfilTrocaArquivo) {
	this.cdPerfilTrocaArquivo = cdPerfilTrocaArquivo;
    }

    /**
     * Get: cdTipoLayoutArquivo.
     *
     * @return cdTipoLayoutArquivo
     */
    public Integer getCdTipoLayoutArquivo() {
	return cdTipoLayoutArquivo;
    }

    /**
     * Set: cdTipoLayoutArquivo.
     *
     * @param cdTipoLayoutArquivo the cd tipo layout arquivo
     */
    public void setCdTipoLayoutArquivo(Integer cdTipoLayoutArquivo) {
	this.cdTipoLayoutArquivo = cdTipoLayoutArquivo;
    }

    /**
     * Get: cdSistemaOrigemArquivo.
     *
     * @return cdSistemaOrigemArquivo
     */
    public String getCdSistemaOrigemArquivo() {
	return cdSistemaOrigemArquivo;
    }

    /**
     * Set: cdSistemaOrigemArquivo.
     *
     * @param cdSistemaOrigemArquivo the cd sistema origem arquivo
     */
    public void setCdSistemaOrigemArquivo(String cdSistemaOrigemArquivo) {
	this.cdSistemaOrigemArquivo = cdSistemaOrigemArquivo;
    }

    /**
     * Get: dsUserNameCliente.
     *
     * @return dsUserNameCliente
     */
    public String getDsUserNameCliente() {
	return dsUserNameCliente;
    }

    /**
     * Set: dsUserNameCliente.
     *
     * @param dsUserNameCliente the ds user name cliente
     */
    public void setDsUserNameCliente(String dsUserNameCliente) {
	this.dsUserNameCliente = dsUserNameCliente;
    }

    /**
     * Get: dsNomeArquivoRemessa.
     *
     * @return dsNomeArquivoRemessa
     */
    public String getDsNomeArquivoRemessa() {
	return dsNomeArquivoRemessa;
    }

    /**
     * Set: dsNomeArquivoRemessa.
     *
     * @param dsNomeArquivoRemessa the ds nome arquivo remessa
     */
    public void setDsNomeArquivoRemessa(String dsNomeArquivoRemessa) {
	this.dsNomeArquivoRemessa = dsNomeArquivoRemessa;
    }

    /**
     * Get: dsNameArquivoRetorno.
     *
     * @return dsNameArquivoRetorno
     */
    public String getDsNameArquivoRetorno() {
	return dsNameArquivoRetorno;
    }

    /**
     * Set: dsNameArquivoRetorno.
     *
     * @param dsNameArquivoRetorno the ds name arquivo retorno
     */
    public void setDsNameArquivoRetorno(String dsNameArquivoRetorno) {
	this.dsNameArquivoRetorno = dsNameArquivoRetorno;
    }

    /**
     * Get: cdRejeicaoAcltoRemessa.
     *
     * @return cdRejeicaoAcltoRemessa
     */
    public Integer getCdRejeicaoAcltoRemessa() {
	return cdRejeicaoAcltoRemessa;
    }

    /**
     * Set: cdRejeicaoAcltoRemessa.
     *
     * @param cdRejeicaoAcltoRemessa the cd rejeicao aclto remessa
     */
    public void setCdRejeicaoAcltoRemessa(Integer cdRejeicaoAcltoRemessa) {
	this.cdRejeicaoAcltoRemessa = cdRejeicaoAcltoRemessa;
    }

    /**
     * Get: qtMaxIncotRemessa.
     *
     * @return qtMaxIncotRemessa
     */
    public Integer getQtMaxIncotRemessa() {
	return qtMaxIncotRemessa;
    }

    /**
     * Set: qtMaxIncotRemessa.
     *
     * @param qtMaxIncotRemessa the qt max incot remessa
     */
    public void setQtMaxIncotRemessa(Integer qtMaxIncotRemessa) {
	this.qtMaxIncotRemessa = qtMaxIncotRemessa;
    }

    /**
     * Get: percentualInconsistenciaRejeicaoRemessa.
     *
     * @return percentualInconsistenciaRejeicaoRemessa
     */
    public BigDecimal getPercentualInconsistenciaRejeicaoRemessa() {
	return percentualInconsistenciaRejeicaoRemessa;
    }

    /**
     * Set: percentualInconsistenciaRejeicaoRemessa.
     *
     * @param percentualInconsistenciaRejeicaoRemessa the percentual inconsistencia rejeicao remessa
     */
    public void setPercentualInconsistenciaRejeicaoRemessa(BigDecimal percentualInconsistenciaRejeicaoRemessa) {
	this.percentualInconsistenciaRejeicaoRemessa = percentualInconsistenciaRejeicaoRemessa;
    }

    /**
     * Get: cdNivelControleRemessa.
     *
     * @return cdNivelControleRemessa
     */
    public Integer getCdNivelControleRemessa() {
	return cdNivelControleRemessa;
    }

    /**
     * Set: cdNivelControleRemessa.
     *
     * @param cdNivelControleRemessa the cd nivel controle remessa
     */
    public void setCdNivelControleRemessa(Integer cdNivelControleRemessa) {
	this.cdNivelControleRemessa = cdNivelControleRemessa;
    }

    /**
     * Get: cdControleNumeroRemessa.
     *
     * @return cdControleNumeroRemessa
     */
    public Integer getCdControleNumeroRemessa() {
	return cdControleNumeroRemessa;
    }

    /**
     * Set: cdControleNumeroRemessa.
     *
     * @param cdControleNumeroRemessa the cd controle numero remessa
     */
    public void setCdControleNumeroRemessa(Integer cdControleNumeroRemessa) {
	this.cdControleNumeroRemessa = cdControleNumeroRemessa;
    }

    /**
     * Get: cdPeriodicidadeContagemRemessa.
     *
     * @return cdPeriodicidadeContagemRemessa
     */
    public Integer getCdPeriodicidadeContagemRemessa() {
	return cdPeriodicidadeContagemRemessa;
    }

    /**
     * Set: cdPeriodicidadeContagemRemessa.
     *
     * @param cdPeriodicidadeContagemRemessa the cd periodicidade contagem remessa
     */
    public void setCdPeriodicidadeContagemRemessa(Integer cdPeriodicidadeContagemRemessa) {
	this.cdPeriodicidadeContagemRemessa = cdPeriodicidadeContagemRemessa;
    }

    /**
     * Get: nrMaximoContagemRemessa.
     *
     * @return nrMaximoContagemRemessa
     */
    public Long getNrMaximoContagemRemessa() {
	return nrMaximoContagemRemessa;
    }

    /**
     * Set: nrMaximoContagemRemessa.
     *
     * @param nrMaximoContagemRemessa the nr maximo contagem remessa
     */
    public void setNrMaximoContagemRemessa(Long nrMaximoContagemRemessa) {
	this.nrMaximoContagemRemessa = nrMaximoContagemRemessa;
    }

    /**
     * Get: cdMeioPrincipalRemessa.
     *
     * @return cdMeioPrincipalRemessa
     */
    public Integer getCdMeioPrincipalRemessa() {
	return cdMeioPrincipalRemessa;
    }

    /**
     * Set: cdMeioPrincipalRemessa.
     *
     * @param cdMeioPrincipalRemessa the cd meio principal remessa
     */
    public void setCdMeioPrincipalRemessa(Integer cdMeioPrincipalRemessa) {
	this.cdMeioPrincipalRemessa = cdMeioPrincipalRemessa;
    }

    /**
     * Get: cdMeioAlternativoRemessa.
     *
     * @return cdMeioAlternativoRemessa
     */
    public Integer getCdMeioAlternativoRemessa() {
	return cdMeioAlternativoRemessa;
    }

    /**
     * Set: cdMeioAlternativoRemessa.
     *
     * @param cdMeioAlternativoRemessa the cd meio alternativo remessa
     */
    public void setCdMeioAlternativoRemessa(Integer cdMeioAlternativoRemessa) {
	this.cdMeioAlternativoRemessa = cdMeioAlternativoRemessa;
    }

    /**
     * Get: cdNivelControleRetorno.
     *
     * @return cdNivelControleRetorno
     */
    public Integer getCdNivelControleRetorno() {
	return cdNivelControleRetorno;
    }

    /**
     * Set: cdNivelControleRetorno.
     *
     * @param cdNivelControleRetorno the cd nivel controle retorno
     */
    public void setCdNivelControleRetorno(Integer cdNivelControleRetorno) {
	this.cdNivelControleRetorno = cdNivelControleRetorno;
    }

    /**
     * Get: cdControleNumeroRetorno.
     *
     * @return cdControleNumeroRetorno
     */
    public Integer getCdControleNumeroRetorno() {
	return cdControleNumeroRetorno;
    }

    /**
     * Set: cdControleNumeroRetorno.
     *
     * @param cdControleNumeroRetorno the cd controle numero retorno
     */
    public void setCdControleNumeroRetorno(Integer cdControleNumeroRetorno) {
	this.cdControleNumeroRetorno = cdControleNumeroRetorno;
    }

    /**
     * Get: cdPerdcContagemRetorno.
     *
     * @return cdPerdcContagemRetorno
     */
    public Integer getCdPerdcContagemRetorno() {
	return cdPerdcContagemRetorno;
    }

    /**
     * Set: cdPerdcContagemRetorno.
     *
     * @param cdPerdcContagemRetorno the cd perdc contagem retorno
     */
    public void setCdPerdcContagemRetorno(Integer cdPerdcContagemRetorno) {
	this.cdPerdcContagemRetorno = cdPerdcContagemRetorno;
    }

    /**
     * Get: nrMaximoContagemRetorno.
     *
     * @return nrMaximoContagemRetorno
     */
    public Long getNrMaximoContagemRetorno() {
	return nrMaximoContagemRetorno;
    }

    /**
     * Set: nrMaximoContagemRetorno.
     *
     * @param nrMaximoContagemRetorno the nr maximo contagem retorno
     */
    public void setNrMaximoContagemRetorno(Long nrMaximoContagemRetorno) {
	this.nrMaximoContagemRetorno = nrMaximoContagemRetorno;
    }

    /**
     * Get: cdMeioPrincipalRetorno.
     *
     * @return cdMeioPrincipalRetorno
     */
    public Integer getCdMeioPrincipalRetorno() {
	return cdMeioPrincipalRetorno;
    }

    /**
     * Set: cdMeioPrincipalRetorno.
     *
     * @param cdMeioPrincipalRetorno the cd meio principal retorno
     */
    public void setCdMeioPrincipalRetorno(Integer cdMeioPrincipalRetorno) {
	this.cdMeioPrincipalRetorno = cdMeioPrincipalRetorno;
    }

    /**
     * Get: cdMeioAlternativoRetorno.
     *
     * @return cdMeioAlternativoRetorno
     */
    public Integer getCdMeioAlternativoRetorno() {
	return cdMeioAlternativoRetorno;
    }

    /**
     * Set: cdMeioAlternativoRetorno.
     *
     * @param cdMeioAlternativoRetorno the cd meio alternativo retorno
     */
    public void setCdMeioAlternativoRetorno(Integer cdMeioAlternativoRetorno) {
	this.cdMeioAlternativoRetorno = cdMeioAlternativoRetorno;
    }

    /**
     * Get: cdSerieAplicacaoTransmissao.
     *
     * @return cdSerieAplicacaoTransmissao
     */
    public String getCdSerieAplicacaoTransmissao() {
	return cdSerieAplicacaoTransmissao;
    }

    /**
     * Set: cdSerieAplicacaoTransmissao.
     *
     * @param cdSerieAplicacaoTransmissao the cd serie aplicacao transmissao
     */
    public void setCdSerieAplicacaoTransmissao(String cdSerieAplicacaoTransmissao) {
	this.cdSerieAplicacaoTransmissao = cdSerieAplicacaoTransmissao;
    }

    /**
     * Get: cdApliFormat.
     *
     * @return cdApliFormat
     */
    public Long getCdApliFormat() {
	return cdApliFormat;
    }

    /**
     * Set: cdApliFormat.
     *
     * @param cdApliFormat the cd apli format
     */
    public void setCdApliFormat(Long cdApliFormat) {
	this.cdApliFormat = cdApliFormat;
    }

    /**
     * Get: cdEmpresaResponsavelTransporte.
     *
     * @return cdEmpresaResponsavelTransporte
     */
    public Long getCdEmpresaResponsavelTransporte() {
	return cdEmpresaResponsavelTransporte;
    }

    /**
     * Set: cdEmpresaResponsavelTransporte.
     *
     * @param cdEmpresaResponsavelTransporte the cd empresa responsavel transporte
     */
    public void setCdEmpresaResponsavelTransporte(Long cdEmpresaResponsavelTransporte) {
	this.cdEmpresaResponsavelTransporte = cdEmpresaResponsavelTransporte;
    }

	/**
	 * Get: cdPessoaJuridica.
	 *
	 * @return cdPessoaJuridica
	 */
	public Long getCdPessoaJuridica() {
		return cdPessoaJuridica;
	}

	/**
	 * Set: cdPessoaJuridica.
	 *
	 * @param cdPessoaJuridica the cd pessoa juridica
	 */
	public void setCdPessoaJuridica(Long cdPessoaJuridica) {
		this.cdPessoaJuridica = cdPessoaJuridica;
	}

	/**
	 * Get: cdUnidadeOrganizacional.
	 *
	 * @return cdUnidadeOrganizacional
	 */
	public Integer getCdUnidadeOrganizacional() {
		return cdUnidadeOrganizacional;
	}

	/**
	 * Set: cdUnidadeOrganizacional.
	 *
	 * @param cdUnidadeOrganizacional the cd unidade organizacional
	 */
	public void setCdUnidadeOrganizacional(Integer cdUnidadeOrganizacional) {
		this.cdUnidadeOrganizacional = cdUnidadeOrganizacional;
	}

	/**
	 * Get: dsNomeContatoCliente.
	 *
	 * @return dsNomeContatoCliente
	 */
	public String getDsNomeContatoCliente() {
		return dsNomeContatoCliente;
	}

	/**
	 * Set: dsNomeContatoCliente.
	 *
	 * @param dsNomeContatoCliente the ds nome contato cliente
	 */
	public void setDsNomeContatoCliente(String dsNomeContatoCliente) {
		this.dsNomeContatoCliente = dsNomeContatoCliente;
	}

	/**
	 * Get: cdAreaFone.
	 *
	 * @return cdAreaFone
	 */
	public Integer getCdAreaFone() {
		return cdAreaFone;
	}

	/**
	 * Set: cdAreaFone.
	 *
	 * @param cdAreaFone the cd area fone
	 */
	public void setCdAreaFone(Integer cdAreaFone) {
		this.cdAreaFone = cdAreaFone;
	}

	/**
	 * Get: cdFoneContatoCliente.
	 *
	 * @return cdFoneContatoCliente
	 */
	public Long getCdFoneContatoCliente() {
		return cdFoneContatoCliente;
	}

	/**
	 * Set: cdFoneContatoCliente.
	 *
	 * @param cdFoneContatoCliente the cd fone contato cliente
	 */
	public void setCdFoneContatoCliente(Long cdFoneContatoCliente) {
		this.cdFoneContatoCliente = cdFoneContatoCliente;
	}

	/**
	 * Get: cdRamalContatoCliente.
	 *
	 * @return cdRamalContatoCliente
	 */
	public String getCdRamalContatoCliente() {
		return cdRamalContatoCliente;
	}

	/**
	 * Set: cdRamalContatoCliente.
	 *
	 * @param cdRamalContatoCliente the cd ramal contato cliente
	 */
	public void setCdRamalContatoCliente(String cdRamalContatoCliente) {
		this.cdRamalContatoCliente = cdRamalContatoCliente;
	}

	/**
	 * Get: dsEmailContatoCliente.
	 *
	 * @return dsEmailContatoCliente
	 */
	public String getDsEmailContatoCliente() {
		return dsEmailContatoCliente;
	}

	/**
	 * Set: dsEmailContatoCliente.
	 *
	 * @param dsEmailContatoCliente the ds email contato cliente
	 */
	public void setDsEmailContatoCliente(String dsEmailContatoCliente) {
		this.dsEmailContatoCliente = dsEmailContatoCliente;
	}

	/**
	 * Get: dsSolicitacaoPendente.
	 *
	 * @return dsSolicitacaoPendente
	 */
	public String getDsSolicitacaoPendente() {
		return dsSolicitacaoPendente;
	}

	/**
	 * Set: dsSolicitacaoPendente.
	 *
	 * @param dsSolicitacaoPendente the ds solicitacao pendente
	 */
	public void setDsSolicitacaoPendente(String dsSolicitacaoPendente) {
		this.dsSolicitacaoPendente = dsSolicitacaoPendente;
	}

	/**
	 * Get: cdAreaFonePend.
	 *
	 * @return cdAreaFonePend
	 */
	public Integer getCdAreaFonePend() {
		return cdAreaFonePend;
	}

	/**
	 * Set: cdAreaFonePend.
	 *
	 * @param cdAreaFonePend the cd area fone pend
	 */
	public void setCdAreaFonePend(Integer cdAreaFonePend) {
		this.cdAreaFonePend = cdAreaFonePend;
	}

	/**
	 * Get: cdFoneSolicitacaoPend.
	 *
	 * @return cdFoneSolicitacaoPend
	 */
	public Long getCdFoneSolicitacaoPend() {
		return cdFoneSolicitacaoPend;
	}

	/**
	 * Set: cdFoneSolicitacaoPend.
	 *
	 * @param cdFoneSolicitacaoPend the cd fone solicitacao pend
	 */
	public void setCdFoneSolicitacaoPend(Long cdFoneSolicitacaoPend) {
		this.cdFoneSolicitacaoPend = cdFoneSolicitacaoPend;
	}

	/**
	 * Get: cdRamalSolctPend.
	 *
	 * @return cdRamalSolctPend
	 */
	public String getCdRamalSolctPend() {
		return cdRamalSolctPend;
	}

	/**
	 * Set: cdRamalSolctPend.
	 *
	 * @param cdRamalSolctPend the cd ramal solct pend
	 */
	public void setCdRamalSolctPend(String cdRamalSolctPend) {
		this.cdRamalSolctPend = cdRamalSolctPend;
	}

	/**
	 * Get: dsEmailSolctPend.
	 *
	 * @return dsEmailSolctPend
	 */
	public String getDsEmailSolctPend() {
		return dsEmailSolctPend;
	}

	/**
	 * Set: dsEmailSolctPend.
	 *
	 * @param dsEmailSolctPend the ds email solct pend
	 */
	public void setDsEmailSolctPend(String dsEmailSolctPend) {
		this.dsEmailSolctPend = dsEmailSolctPend;
	}

	/**
	 * Get: qtMesRegistroTrafg.
	 *
	 * @return qtMesRegistroTrafg
	 */
	public Long getQtMesRegistroTrafg() {
		return qtMesRegistroTrafg;
	}

	/**
	 * Set: qtMesRegistroTrafg.
	 *
	 * @param qtMesRegistroTrafg the qt mes registro trafg
	 */
	public void setQtMesRegistroTrafg(Long qtMesRegistroTrafg) {
		this.qtMesRegistroTrafg = qtMesRegistroTrafg;
	}

	/**
	 * Get: dsObsGeralPerfil.
	 *
	 * @return dsObsGeralPerfil
	 */
	public String getDsObsGeralPerfil() {
		return dsObsGeralPerfil;
	}

	/**
	 * Set: dsObsGeralPerfil.
	 *
	 * @param dsObsGeralPerfil the ds obs geral perfil
	 */
	public void setDsObsGeralPerfil(String dsObsGeralPerfil) {
		this.dsObsGeralPerfil = dsObsGeralPerfil;
	}

	/**
	 * @param cdIndicadorGeracaoSegmentoB the cdIndicadorGeracaoSegmentoB to set
	 */
	public void setCdIndicadorGeracaoSegmentoB(
			Integer cdIndicadorGeracaoSegmentoB) {
		this.cdIndicadorGeracaoSegmentoB = cdIndicadorGeracaoSegmentoB;
	}

	/**
	 * @return the cdIndicadorGeracaoSegmentoB
	 */
	public Integer getCdIndicadorGeracaoSegmentoB() {
		return cdIndicadorGeracaoSegmentoB;
	}

	/**
	 * @param cdIndicadorGeracaoSegmentoZ the cdIndicadorGeracaoSegmentoZ to set
	 */
	public void setCdIndicadorGeracaoSegmentoZ(
			Integer cdIndicadorGeracaoSegmentoZ) {
		this.cdIndicadorGeracaoSegmentoZ = cdIndicadorGeracaoSegmentoZ;
	}

	/**
	 * @return the cdIndicadorGeracaoSegmentoZ
	 */
	public Integer getCdIndicadorGeracaoSegmentoZ() {
		return cdIndicadorGeracaoSegmentoZ;
	}

	/**
	 * @return the cdIndicadorCpfLayout
	 */
	public Integer getCdIndicadorCpfLayout() {
		return cdIndicadorCpfLayout;
	}

	/**
	 * @param cdIndicadorCpfLayout the cdIndicadorCpfLayout to set
	 */
	public void setCdIndicadorCpfLayout(Integer cdIndicadorCpfLayout) {
		this.cdIndicadorCpfLayout = cdIndicadorCpfLayout;
	}

	/**
	 * @return the cdIndicadorAssociacaoLayout
	 */
	public Integer getCdIndicadorAssociacaoLayout() {
		return cdIndicadorAssociacaoLayout;
	}

	/**
	 * @param cdIndicadorAssociacaoLayout the cdIndicadorAssociacaoLayout to set
	 */
	public void setCdIndicadorAssociacaoLayout(Integer cdIndicadorAssociacaoLayout) {
		this.cdIndicadorAssociacaoLayout = cdIndicadorAssociacaoLayout;
	}

	/**
	 * @return the cdIndicadorContaComplementar
	 */
	public Integer getCdIndicadorContaComplementar() {
		return cdIndicadorContaComplementar;
	}

	/**
	 * @param cdIndicadorContaComplementar the cdIndicadorContaComplementar to set
	 */
	public void setCdIndicadorContaComplementar(Integer cdIndicadorContaComplementar) {
		this.cdIndicadorContaComplementar = cdIndicadorContaComplementar;
	}

	/**
	 * @return the cdContaDebito
	 */
	public Integer getCdContaDebito() {
		return cdContaDebito;
	}

	/**
	 * @param cdContaDebito the cdContaDebito to set
	 */
	public void setCdContaDebito(Integer cdContaDebito) {
		this.cdContaDebito = cdContaDebito;
	}
}