/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.alteraralcada.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;

/**
 * Class AlterarAlcadaRequest.
 * 
 * @version $Revision$ $Date$
 */
public class AlterarAlcadaRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdSistema
     */
    private java.lang.String _cdSistema;

    /**
     * Field _cdProgPagamentoIntegrado
     */
    private java.lang.String _cdProgPagamentoIntegrado;

    /**
     * Field _cdTipoAcaoPagamento
     */
    private int _cdTipoAcaoPagamento = 0;

    /**
     * keeps track of state for field: _cdTipoAcaoPagamento
     */
    private boolean _has_cdTipoAcaoPagamento;

    /**
     * Field _cdTipoAcessoAlcada
     */
    private int _cdTipoAcessoAlcada = 0;

    /**
     * keeps track of state for field: _cdTipoAcessoAlcada
     */
    private boolean _has_cdTipoAcessoAlcada;

    /**
     * Field _cdTipoRegraAlcada
     */
    private int _cdTipoRegraAlcada = 0;

    /**
     * keeps track of state for field: _cdTipoRegraAlcada
     */
    private boolean _has_cdTipoRegraAlcada;

    /**
     * Field _cdTipoTratoAlcada
     */
    private int _cdTipoTratoAlcada = 0;

    /**
     * keeps track of state for field: _cdTipoTratoAlcada
     */
    private boolean _has_cdTipoTratoAlcada;

    /**
     * Field _cdSistemaRetorno
     */
    private java.lang.String _cdSistemaRetorno;

    /**
     * Field _cdProgRetorPagamento
     */
    private java.lang.String _cdProgRetorPagamento;

    /**
     * Field _cdProdutoOperDeflt
     */
    private int _cdProdutoOperDeflt = 0;

    /**
     * keeps track of state for field: _cdProdutoOperDeflt
     */
    private boolean _has_cdProdutoOperDeflt;

    /**
     * Field _cdOperProdutoServico
     */
    private int _cdOperProdutoServico = 0;

    /**
     * keeps track of state for field: _cdOperProdutoServico
     */
    private boolean _has_cdOperProdutoServico;

    /**
     * Field _cdRegraAlcada
     */
    private int _cdRegraAlcada = 0;

    /**
     * keeps track of state for field: _cdRegraAlcada
     */
    private boolean _has_cdRegraAlcada;


      //----------------/
     //- Constructors -/
    //----------------/

    public AlterarAlcadaRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.alteraralcada.request.AlterarAlcadaRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdOperProdutoServico
     * 
     */
    public void deleteCdOperProdutoServico()
    {
        this._has_cdOperProdutoServico= false;
    } //-- void deleteCdOperProdutoServico() 

    /**
     * Method deleteCdProdutoOperDeflt
     * 
     */
    public void deleteCdProdutoOperDeflt()
    {
        this._has_cdProdutoOperDeflt= false;
    } //-- void deleteCdProdutoOperDeflt() 

    /**
     * Method deleteCdRegraAlcada
     * 
     */
    public void deleteCdRegraAlcada()
    {
        this._has_cdRegraAlcada= false;
    } //-- void deleteCdRegraAlcada() 

    /**
     * Method deleteCdTipoAcaoPagamento
     * 
     */
    public void deleteCdTipoAcaoPagamento()
    {
        this._has_cdTipoAcaoPagamento= false;
    } //-- void deleteCdTipoAcaoPagamento() 

    /**
     * Method deleteCdTipoAcessoAlcada
     * 
     */
    public void deleteCdTipoAcessoAlcada()
    {
        this._has_cdTipoAcessoAlcada= false;
    } //-- void deleteCdTipoAcessoAlcada() 

    /**
     * Method deleteCdTipoRegraAlcada
     * 
     */
    public void deleteCdTipoRegraAlcada()
    {
        this._has_cdTipoRegraAlcada= false;
    } //-- void deleteCdTipoRegraAlcada() 

    /**
     * Method deleteCdTipoTratoAlcada
     * 
     */
    public void deleteCdTipoTratoAlcada()
    {
        this._has_cdTipoTratoAlcada= false;
    } //-- void deleteCdTipoTratoAlcada() 

    /**
     * Returns the value of field 'cdOperProdutoServico'.
     * 
     * @return int
     * @return the value of field 'cdOperProdutoServico'.
     */
    public int getCdOperProdutoServico()
    {
        return this._cdOperProdutoServico;
    } //-- int getCdOperProdutoServico() 

    /**
     * Returns the value of field 'cdProdutoOperDeflt'.
     * 
     * @return int
     * @return the value of field 'cdProdutoOperDeflt'.
     */
    public int getCdProdutoOperDeflt()
    {
        return this._cdProdutoOperDeflt;
    } //-- int getCdProdutoOperDeflt() 

    /**
     * Returns the value of field 'cdProgPagamentoIntegrado'.
     * 
     * @return String
     * @return the value of field 'cdProgPagamentoIntegrado'.
     */
    public java.lang.String getCdProgPagamentoIntegrado()
    {
        return this._cdProgPagamentoIntegrado;
    } //-- java.lang.String getCdProgPagamentoIntegrado() 

    /**
     * Returns the value of field 'cdProgRetorPagamento'.
     * 
     * @return String
     * @return the value of field 'cdProgRetorPagamento'.
     */
    public java.lang.String getCdProgRetorPagamento()
    {
        return this._cdProgRetorPagamento;
    } //-- java.lang.String getCdProgRetorPagamento() 

    /**
     * Returns the value of field 'cdRegraAlcada'.
     * 
     * @return int
     * @return the value of field 'cdRegraAlcada'.
     */
    public int getCdRegraAlcada()
    {
        return this._cdRegraAlcada;
    } //-- int getCdRegraAlcada() 

    /**
     * Returns the value of field 'cdSistema'.
     * 
     * @return String
     * @return the value of field 'cdSistema'.
     */
    public java.lang.String getCdSistema()
    {
        return this._cdSistema;
    } //-- java.lang.String getCdSistema() 

    /**
     * Returns the value of field 'cdSistemaRetorno'.
     * 
     * @return String
     * @return the value of field 'cdSistemaRetorno'.
     */
    public java.lang.String getCdSistemaRetorno()
    {
        return this._cdSistemaRetorno;
    } //-- java.lang.String getCdSistemaRetorno() 

    /**
     * Returns the value of field 'cdTipoAcaoPagamento'.
     * 
     * @return int
     * @return the value of field 'cdTipoAcaoPagamento'.
     */
    public int getCdTipoAcaoPagamento()
    {
        return this._cdTipoAcaoPagamento;
    } //-- int getCdTipoAcaoPagamento() 

    /**
     * Returns the value of field 'cdTipoAcessoAlcada'.
     * 
     * @return int
     * @return the value of field 'cdTipoAcessoAlcada'.
     */
    public int getCdTipoAcessoAlcada()
    {
        return this._cdTipoAcessoAlcada;
    } //-- int getCdTipoAcessoAlcada() 

    /**
     * Returns the value of field 'cdTipoRegraAlcada'.
     * 
     * @return int
     * @return the value of field 'cdTipoRegraAlcada'.
     */
    public int getCdTipoRegraAlcada()
    {
        return this._cdTipoRegraAlcada;
    } //-- int getCdTipoRegraAlcada() 

    /**
     * Returns the value of field 'cdTipoTratoAlcada'.
     * 
     * @return int
     * @return the value of field 'cdTipoTratoAlcada'.
     */
    public int getCdTipoTratoAlcada()
    {
        return this._cdTipoTratoAlcada;
    } //-- int getCdTipoTratoAlcada() 

    /**
     * Method hasCdOperProdutoServico
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdOperProdutoServico()
    {
        return this._has_cdOperProdutoServico;
    } //-- boolean hasCdOperProdutoServico() 

    /**
     * Method hasCdProdutoOperDeflt
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdutoOperDeflt()
    {
        return this._has_cdProdutoOperDeflt;
    } //-- boolean hasCdProdutoOperDeflt() 

    /**
     * Method hasCdRegraAlcada
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdRegraAlcada()
    {
        return this._has_cdRegraAlcada;
    } //-- boolean hasCdRegraAlcada() 

    /**
     * Method hasCdTipoAcaoPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoAcaoPagamento()
    {
        return this._has_cdTipoAcaoPagamento;
    } //-- boolean hasCdTipoAcaoPagamento() 

    /**
     * Method hasCdTipoAcessoAlcada
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoAcessoAlcada()
    {
        return this._has_cdTipoAcessoAlcada;
    } //-- boolean hasCdTipoAcessoAlcada() 

    /**
     * Method hasCdTipoRegraAlcada
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoRegraAlcada()
    {
        return this._has_cdTipoRegraAlcada;
    } //-- boolean hasCdTipoRegraAlcada() 

    /**
     * Method hasCdTipoTratoAlcada
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoTratoAlcada()
    {
        return this._has_cdTipoTratoAlcada;
    } //-- boolean hasCdTipoTratoAlcada() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdOperProdutoServico'.
     * 
     * @param cdOperProdutoServico the value of field
     * 'cdOperProdutoServico'.
     */
    public void setCdOperProdutoServico(int cdOperProdutoServico)
    {
        this._cdOperProdutoServico = cdOperProdutoServico;
        this._has_cdOperProdutoServico = true;
    } //-- void setCdOperProdutoServico(int) 

    /**
     * Sets the value of field 'cdProdutoOperDeflt'.
     * 
     * @param cdProdutoOperDeflt the value of field
     * 'cdProdutoOperDeflt'.
     */
    public void setCdProdutoOperDeflt(int cdProdutoOperDeflt)
    {
        this._cdProdutoOperDeflt = cdProdutoOperDeflt;
        this._has_cdProdutoOperDeflt = true;
    } //-- void setCdProdutoOperDeflt(int) 

    /**
     * Sets the value of field 'cdProgPagamentoIntegrado'.
     * 
     * @param cdProgPagamentoIntegrado the value of field
     * 'cdProgPagamentoIntegrado'.
     */
    public void setCdProgPagamentoIntegrado(java.lang.String cdProgPagamentoIntegrado)
    {
        this._cdProgPagamentoIntegrado = cdProgPagamentoIntegrado;
    } //-- void setCdProgPagamentoIntegrado(java.lang.String) 

    /**
     * Sets the value of field 'cdProgRetorPagamento'.
     * 
     * @param cdProgRetorPagamento the value of field
     * 'cdProgRetorPagamento'.
     */
    public void setCdProgRetorPagamento(java.lang.String cdProgRetorPagamento)
    {
        this._cdProgRetorPagamento = cdProgRetorPagamento;
    } //-- void setCdProgRetorPagamento(java.lang.String) 

    /**
     * Sets the value of field 'cdRegraAlcada'.
     * 
     * @param cdRegraAlcada the value of field 'cdRegraAlcada'.
     */
    public void setCdRegraAlcada(int cdRegraAlcada)
    {
        this._cdRegraAlcada = cdRegraAlcada;
        this._has_cdRegraAlcada = true;
    } //-- void setCdRegraAlcada(int) 

    /**
     * Sets the value of field 'cdSistema'.
     * 
     * @param cdSistema the value of field 'cdSistema'.
     */
    public void setCdSistema(java.lang.String cdSistema)
    {
        this._cdSistema = cdSistema;
    } //-- void setCdSistema(java.lang.String) 

    /**
     * Sets the value of field 'cdSistemaRetorno'.
     * 
     * @param cdSistemaRetorno the value of field 'cdSistemaRetorno'
     */
    public void setCdSistemaRetorno(java.lang.String cdSistemaRetorno)
    {
        this._cdSistemaRetorno = cdSistemaRetorno;
    } //-- void setCdSistemaRetorno(java.lang.String) 

    /**
     * Sets the value of field 'cdTipoAcaoPagamento'.
     * 
     * @param cdTipoAcaoPagamento the value of field
     * 'cdTipoAcaoPagamento'.
     */
    public void setCdTipoAcaoPagamento(int cdTipoAcaoPagamento)
    {
        this._cdTipoAcaoPagamento = cdTipoAcaoPagamento;
        this._has_cdTipoAcaoPagamento = true;
    } //-- void setCdTipoAcaoPagamento(int) 

    /**
     * Sets the value of field 'cdTipoAcessoAlcada'.
     * 
     * @param cdTipoAcessoAlcada the value of field
     * 'cdTipoAcessoAlcada'.
     */
    public void setCdTipoAcessoAlcada(int cdTipoAcessoAlcada)
    {
        this._cdTipoAcessoAlcada = cdTipoAcessoAlcada;
        this._has_cdTipoAcessoAlcada = true;
    } //-- void setCdTipoAcessoAlcada(int) 

    /**
     * Sets the value of field 'cdTipoRegraAlcada'.
     * 
     * @param cdTipoRegraAlcada the value of field
     * 'cdTipoRegraAlcada'.
     */
    public void setCdTipoRegraAlcada(int cdTipoRegraAlcada)
    {
        this._cdTipoRegraAlcada = cdTipoRegraAlcada;
        this._has_cdTipoRegraAlcada = true;
    } //-- void setCdTipoRegraAlcada(int) 

    /**
     * Sets the value of field 'cdTipoTratoAlcada'.
     * 
     * @param cdTipoTratoAlcada the value of field
     * 'cdTipoTratoAlcada'.
     */
    public void setCdTipoTratoAlcada(int cdTipoTratoAlcada)
    {
        this._cdTipoTratoAlcada = cdTipoTratoAlcada;
        this._has_cdTipoTratoAlcada = true;
    } //-- void setCdTipoTratoAlcada(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return AlterarAlcadaRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.alteraralcada.request.AlterarAlcadaRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.alteraralcada.request.AlterarAlcadaRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.alteraralcada.request.AlterarAlcadaRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.alteraralcada.request.AlterarAlcadaRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
