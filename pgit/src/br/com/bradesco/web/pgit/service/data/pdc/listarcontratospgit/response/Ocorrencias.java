/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listarcontratospgit.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdClubRepresentante
     */
    private long _cdClubRepresentante = 0;

    /**
     * keeps track of state for field: _cdClubRepresentante
     */
    private boolean _has_cdClubRepresentante;

    /**
     * Field _cdCpfCnpjRepresentante
     */
    private long _cdCpfCnpjRepresentante = 0;

    /**
     * keeps track of state for field: _cdCpfCnpjRepresentante
     */
    private boolean _has_cdCpfCnpjRepresentante;

    /**
     * Field _cdFilialCpfCnpjRepresentante
     */
    private int _cdFilialCpfCnpjRepresentante = 0;

    /**
     * keeps track of state for field: _cdFilialCpfCnpjRepresentante
     */
    private boolean _has_cdFilialCpfCnpjRepresentante;

    /**
     * Field _cdControleCpfCnpjRepresentante
     */
    private int _cdControleCpfCnpjRepresentante = 0;

    /**
     * keeps track of state for field:
     * _cdControleCpfCnpjRepresentante
     */
    private boolean _has_cdControleCpfCnpjRepresentante;

    /**
     * Field _nmRazaoSocialRepresentante
     */
    private java.lang.String _nmRazaoSocialRepresentante;

    /**
     * Field _cdPessoaJuridica
     */
    private long _cdPessoaJuridica = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridica
     */
    private boolean _has_cdPessoaJuridica;

    /**
     * Field _dsPessoaJuridica
     */
    private java.lang.String _dsPessoaJuridica;

    /**
     * Field _cdTipoContrato
     */
    private int _cdTipoContrato = 0;

    /**
     * keeps track of state for field: _cdTipoContrato
     */
    private boolean _has_cdTipoContrato;

    /**
     * Field _dsTipoContrato
     */
    private java.lang.String _dsTipoContrato;

    /**
     * Field _nrSequenciaContrato
     */
    private long _nrSequenciaContrato = 0;

    /**
     * keeps track of state for field: _nrSequenciaContrato
     */
    private boolean _has_nrSequenciaContrato;

    /**
     * Field _dsContrato
     */
    private java.lang.String _dsContrato;

    /**
     * Field _cdAgenciaOperadora
     */
    private int _cdAgenciaOperadora = 0;

    /**
     * keeps track of state for field: _cdAgenciaOperadora
     */
    private boolean _has_cdAgenciaOperadora;

    /**
     * Field _cdDigitoAgenciaOperadora
     */
    private java.lang.String _cdDigitoAgenciaOperadora;

    /**
     * Field _dsAgenciaOperadora
     */
    private java.lang.String _dsAgenciaOperadora;

    /**
     * Field _cdFuncionarioBradesco
     */
    private long _cdFuncionarioBradesco = 0;

    /**
     * keeps track of state for field: _cdFuncionarioBradesco
     */
    private boolean _has_cdFuncionarioBradesco;

    /**
     * Field _nmFuncionarioBradesco
     */
    private java.lang.String _nmFuncionarioBradesco;

    /**
     * Field _cdSituacaoContrato
     */
    private int _cdSituacaoContrato = 0;

    /**
     * keeps track of state for field: _cdSituacaoContrato
     */
    private boolean _has_cdSituacaoContrato;

    /**
     * Field _dsSituacaoContrato
     */
    private java.lang.String _dsSituacaoContrato;

    /**
     * Field _cdMotivoSituacao
     */
    private int _cdMotivoSituacao = 0;

    /**
     * keeps track of state for field: _cdMotivoSituacao
     */
    private boolean _has_cdMotivoSituacao;

    /**
     * Field _dsMotivoSituacao
     */
    private java.lang.String _dsMotivoSituacao;

    /**
     * Field _cdAditivo
     */
    private java.lang.String _cdAditivo;

    /**
     * Field _cdTipoParticipacao
     */
    private java.lang.String _cdTipoParticipacao;

    /**
     * Field _cdSegmentoCliente
     */
    private int _cdSegmentoCliente = 0;

    /**
     * keeps track of state for field: _cdSegmentoCliente
     */
    private boolean _has_cdSegmentoCliente;

    /**
     * Field _dsSegmentoCliente
     */
    private java.lang.String _dsSegmentoCliente;

    /**
     * Field _cdSubSegmentoCliente
     */
    private int _cdSubSegmentoCliente = 0;

    /**
     * keeps track of state for field: _cdSubSegmentoCliente
     */
    private boolean _has_cdSubSegmentoCliente;

    /**
     * Field _dsSubSegmentoCliente
     */
    private java.lang.String _dsSubSegmentoCliente;

    /**
     * Field _cdAtividadeEconomica
     */
    private int _cdAtividadeEconomica = 0;

    /**
     * keeps track of state for field: _cdAtividadeEconomica
     */
    private boolean _has_cdAtividadeEconomica;

    /**
     * Field _dsAtividadeEconomica
     */
    private java.lang.String _dsAtividadeEconomica;

    /**
     * Field _cdGrupoEconomico
     */
    private long _cdGrupoEconomico = 0;

    /**
     * keeps track of state for field: _cdGrupoEconomico
     */
    private boolean _has_cdGrupoEconomico;

    /**
     * Field _dsGrupoEconomico
     */
    private java.lang.String _dsGrupoEconomico;

    /**
     * Field _dtAbertura
     */
    private java.lang.String _dtAbertura;

    /**
     * Field _dtEncerramento
     */
    private java.lang.String _dtEncerramento;

    /**
     * Field _dtInclusao
     */
    private java.lang.String _dtInclusao;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarcontratospgit.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdAgenciaOperadora
     * 
     */
    public void deleteCdAgenciaOperadora()
    {
        this._has_cdAgenciaOperadora= false;
    } //-- void deleteCdAgenciaOperadora() 

    /**
     * Method deleteCdAtividadeEconomica
     * 
     */
    public void deleteCdAtividadeEconomica()
    {
        this._has_cdAtividadeEconomica= false;
    } //-- void deleteCdAtividadeEconomica() 

    /**
     * Method deleteCdClubRepresentante
     * 
     */
    public void deleteCdClubRepresentante()
    {
        this._has_cdClubRepresentante= false;
    } //-- void deleteCdClubRepresentante() 

    /**
     * Method deleteCdControleCpfCnpjRepresentante
     * 
     */
    public void deleteCdControleCpfCnpjRepresentante()
    {
        this._has_cdControleCpfCnpjRepresentante= false;
    } //-- void deleteCdControleCpfCnpjRepresentante() 

    /**
     * Method deleteCdCpfCnpjRepresentante
     * 
     */
    public void deleteCdCpfCnpjRepresentante()
    {
        this._has_cdCpfCnpjRepresentante= false;
    } //-- void deleteCdCpfCnpjRepresentante() 

    /**
     * Method deleteCdFilialCpfCnpjRepresentante
     * 
     */
    public void deleteCdFilialCpfCnpjRepresentante()
    {
        this._has_cdFilialCpfCnpjRepresentante= false;
    } //-- void deleteCdFilialCpfCnpjRepresentante() 

    /**
     * Method deleteCdFuncionarioBradesco
     * 
     */
    public void deleteCdFuncionarioBradesco()
    {
        this._has_cdFuncionarioBradesco= false;
    } //-- void deleteCdFuncionarioBradesco() 

    /**
     * Method deleteCdGrupoEconomico
     * 
     */
    public void deleteCdGrupoEconomico()
    {
        this._has_cdGrupoEconomico= false;
    } //-- void deleteCdGrupoEconomico() 

    /**
     * Method deleteCdMotivoSituacao
     * 
     */
    public void deleteCdMotivoSituacao()
    {
        this._has_cdMotivoSituacao= false;
    } //-- void deleteCdMotivoSituacao() 

    /**
     * Method deleteCdPessoaJuridica
     * 
     */
    public void deleteCdPessoaJuridica()
    {
        this._has_cdPessoaJuridica= false;
    } //-- void deleteCdPessoaJuridica() 

    /**
     * Method deleteCdSegmentoCliente
     * 
     */
    public void deleteCdSegmentoCliente()
    {
        this._has_cdSegmentoCliente= false;
    } //-- void deleteCdSegmentoCliente() 

    /**
     * Method deleteCdSituacaoContrato
     * 
     */
    public void deleteCdSituacaoContrato()
    {
        this._has_cdSituacaoContrato= false;
    } //-- void deleteCdSituacaoContrato() 

    /**
     * Method deleteCdSubSegmentoCliente
     * 
     */
    public void deleteCdSubSegmentoCliente()
    {
        this._has_cdSubSegmentoCliente= false;
    } //-- void deleteCdSubSegmentoCliente() 

    /**
     * Method deleteCdTipoContrato
     * 
     */
    public void deleteCdTipoContrato()
    {
        this._has_cdTipoContrato= false;
    } //-- void deleteCdTipoContrato() 

    /**
     * Method deleteNrSequenciaContrato
     * 
     */
    public void deleteNrSequenciaContrato()
    {
        this._has_nrSequenciaContrato= false;
    } //-- void deleteNrSequenciaContrato() 

    /**
     * Returns the value of field 'cdAditivo'.
     * 
     * @return String
     * @return the value of field 'cdAditivo'.
     */
    public java.lang.String getCdAditivo()
    {
        return this._cdAditivo;
    } //-- java.lang.String getCdAditivo() 

    /**
     * Returns the value of field 'cdAgenciaOperadora'.
     * 
     * @return int
     * @return the value of field 'cdAgenciaOperadora'.
     */
    public int getCdAgenciaOperadora()
    {
        return this._cdAgenciaOperadora;
    } //-- int getCdAgenciaOperadora() 

    /**
     * Returns the value of field 'cdAtividadeEconomica'.
     * 
     * @return int
     * @return the value of field 'cdAtividadeEconomica'.
     */
    public int getCdAtividadeEconomica()
    {
        return this._cdAtividadeEconomica;
    } //-- int getCdAtividadeEconomica() 

    /**
     * Returns the value of field 'cdClubRepresentante'.
     * 
     * @return long
     * @return the value of field 'cdClubRepresentante'.
     */
    public long getCdClubRepresentante()
    {
        return this._cdClubRepresentante;
    } //-- long getCdClubRepresentante() 

    /**
     * Returns the value of field 'cdControleCpfCnpjRepresentante'.
     * 
     * @return int
     * @return the value of field 'cdControleCpfCnpjRepresentante'.
     */
    public int getCdControleCpfCnpjRepresentante()
    {
        return this._cdControleCpfCnpjRepresentante;
    } //-- int getCdControleCpfCnpjRepresentante() 

    /**
     * Returns the value of field 'cdCpfCnpjRepresentante'.
     * 
     * @return long
     * @return the value of field 'cdCpfCnpjRepresentante'.
     */
    public long getCdCpfCnpjRepresentante()
    {
        return this._cdCpfCnpjRepresentante;
    } //-- long getCdCpfCnpjRepresentante() 

    /**
     * Returns the value of field 'cdDigitoAgenciaOperadora'.
     * 
     * @return String
     * @return the value of field 'cdDigitoAgenciaOperadora'.
     */
    public java.lang.String getCdDigitoAgenciaOperadora()
    {
        return this._cdDigitoAgenciaOperadora;
    } //-- java.lang.String getCdDigitoAgenciaOperadora() 

    /**
     * Returns the value of field 'cdFilialCpfCnpjRepresentante'.
     * 
     * @return int
     * @return the value of field 'cdFilialCpfCnpjRepresentante'.
     */
    public int getCdFilialCpfCnpjRepresentante()
    {
        return this._cdFilialCpfCnpjRepresentante;
    } //-- int getCdFilialCpfCnpjRepresentante() 

    /**
     * Returns the value of field 'cdFuncionarioBradesco'.
     * 
     * @return long
     * @return the value of field 'cdFuncionarioBradesco'.
     */
    public long getCdFuncionarioBradesco()
    {
        return this._cdFuncionarioBradesco;
    } //-- long getCdFuncionarioBradesco() 

    /**
     * Returns the value of field 'cdGrupoEconomico'.
     * 
     * @return long
     * @return the value of field 'cdGrupoEconomico'.
     */
    public long getCdGrupoEconomico()
    {
        return this._cdGrupoEconomico;
    } //-- long getCdGrupoEconomico() 

    /**
     * Returns the value of field 'cdMotivoSituacao'.
     * 
     * @return int
     * @return the value of field 'cdMotivoSituacao'.
     */
    public int getCdMotivoSituacao()
    {
        return this._cdMotivoSituacao;
    } //-- int getCdMotivoSituacao() 

    /**
     * Returns the value of field 'cdPessoaJuridica'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridica'.
     */
    public long getCdPessoaJuridica()
    {
        return this._cdPessoaJuridica;
    } //-- long getCdPessoaJuridica() 

    /**
     * Returns the value of field 'cdSegmentoCliente'.
     * 
     * @return int
     * @return the value of field 'cdSegmentoCliente'.
     */
    public int getCdSegmentoCliente()
    {
        return this._cdSegmentoCliente;
    } //-- int getCdSegmentoCliente() 

    /**
     * Returns the value of field 'cdSituacaoContrato'.
     * 
     * @return int
     * @return the value of field 'cdSituacaoContrato'.
     */
    public int getCdSituacaoContrato()
    {
        return this._cdSituacaoContrato;
    } //-- int getCdSituacaoContrato() 

    /**
     * Returns the value of field 'cdSubSegmentoCliente'.
     * 
     * @return int
     * @return the value of field 'cdSubSegmentoCliente'.
     */
    public int getCdSubSegmentoCliente()
    {
        return this._cdSubSegmentoCliente;
    } //-- int getCdSubSegmentoCliente() 

    /**
     * Returns the value of field 'cdTipoContrato'.
     * 
     * @return int
     * @return the value of field 'cdTipoContrato'.
     */
    public int getCdTipoContrato()
    {
        return this._cdTipoContrato;
    } //-- int getCdTipoContrato() 

    /**
     * Returns the value of field 'cdTipoParticipacao'.
     * 
     * @return String
     * @return the value of field 'cdTipoParticipacao'.
     */
    public java.lang.String getCdTipoParticipacao()
    {
        return this._cdTipoParticipacao;
    } //-- java.lang.String getCdTipoParticipacao() 

    /**
     * Returns the value of field 'dsAgenciaOperadora'.
     * 
     * @return String
     * @return the value of field 'dsAgenciaOperadora'.
     */
    public java.lang.String getDsAgenciaOperadora()
    {
        return this._dsAgenciaOperadora;
    } //-- java.lang.String getDsAgenciaOperadora() 

    /**
     * Returns the value of field 'dsAtividadeEconomica'.
     * 
     * @return String
     * @return the value of field 'dsAtividadeEconomica'.
     */
    public java.lang.String getDsAtividadeEconomica()
    {
        return this._dsAtividadeEconomica;
    } //-- java.lang.String getDsAtividadeEconomica() 

    /**
     * Returns the value of field 'dsContrato'.
     * 
     * @return String
     * @return the value of field 'dsContrato'.
     */
    public java.lang.String getDsContrato()
    {
        return this._dsContrato;
    } //-- java.lang.String getDsContrato() 

    /**
     * Returns the value of field 'dsGrupoEconomico'.
     * 
     * @return String
     * @return the value of field 'dsGrupoEconomico'.
     */
    public java.lang.String getDsGrupoEconomico()
    {
        return this._dsGrupoEconomico;
    } //-- java.lang.String getDsGrupoEconomico() 

    /**
     * Returns the value of field 'dsMotivoSituacao'.
     * 
     * @return String
     * @return the value of field 'dsMotivoSituacao'.
     */
    public java.lang.String getDsMotivoSituacao()
    {
        return this._dsMotivoSituacao;
    } //-- java.lang.String getDsMotivoSituacao() 

    /**
     * Returns the value of field 'dsPessoaJuridica'.
     * 
     * @return String
     * @return the value of field 'dsPessoaJuridica'.
     */
    public java.lang.String getDsPessoaJuridica()
    {
        return this._dsPessoaJuridica;
    } //-- java.lang.String getDsPessoaJuridica() 

    /**
     * Returns the value of field 'dsSegmentoCliente'.
     * 
     * @return String
     * @return the value of field 'dsSegmentoCliente'.
     */
    public java.lang.String getDsSegmentoCliente()
    {
        return this._dsSegmentoCliente;
    } //-- java.lang.String getDsSegmentoCliente() 

    /**
     * Returns the value of field 'dsSituacaoContrato'.
     * 
     * @return String
     * @return the value of field 'dsSituacaoContrato'.
     */
    public java.lang.String getDsSituacaoContrato()
    {
        return this._dsSituacaoContrato;
    } //-- java.lang.String getDsSituacaoContrato() 

    /**
     * Returns the value of field 'dsSubSegmentoCliente'.
     * 
     * @return String
     * @return the value of field 'dsSubSegmentoCliente'.
     */
    public java.lang.String getDsSubSegmentoCliente()
    {
        return this._dsSubSegmentoCliente;
    } //-- java.lang.String getDsSubSegmentoCliente() 

    /**
     * Returns the value of field 'dsTipoContrato'.
     * 
     * @return String
     * @return the value of field 'dsTipoContrato'.
     */
    public java.lang.String getDsTipoContrato()
    {
        return this._dsTipoContrato;
    } //-- java.lang.String getDsTipoContrato() 

    /**
     * Returns the value of field 'dtAbertura'.
     * 
     * @return String
     * @return the value of field 'dtAbertura'.
     */
    public java.lang.String getDtAbertura()
    {
        return this._dtAbertura;
    } //-- java.lang.String getDtAbertura() 

    /**
     * Returns the value of field 'dtEncerramento'.
     * 
     * @return String
     * @return the value of field 'dtEncerramento'.
     */
    public java.lang.String getDtEncerramento()
    {
        return this._dtEncerramento;
    } //-- java.lang.String getDtEncerramento() 

    /**
     * Returns the value of field 'dtInclusao'.
     * 
     * @return String
     * @return the value of field 'dtInclusao'.
     */
    public java.lang.String getDtInclusao()
    {
        return this._dtInclusao;
    } //-- java.lang.String getDtInclusao() 

    /**
     * Returns the value of field 'nmFuncionarioBradesco'.
     * 
     * @return String
     * @return the value of field 'nmFuncionarioBradesco'.
     */
    public java.lang.String getNmFuncionarioBradesco()
    {
        return this._nmFuncionarioBradesco;
    } //-- java.lang.String getNmFuncionarioBradesco() 

    /**
     * Returns the value of field 'nmRazaoSocialRepresentante'.
     * 
     * @return String
     * @return the value of field 'nmRazaoSocialRepresentante'.
     */
    public java.lang.String getNmRazaoSocialRepresentante()
    {
        return this._nmRazaoSocialRepresentante;
    } //-- java.lang.String getNmRazaoSocialRepresentante() 

    /**
     * Returns the value of field 'nrSequenciaContrato'.
     * 
     * @return long
     * @return the value of field 'nrSequenciaContrato'.
     */
    public long getNrSequenciaContrato()
    {
        return this._nrSequenciaContrato;
    } //-- long getNrSequenciaContrato() 

    /**
     * Method hasCdAgenciaOperadora
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgenciaOperadora()
    {
        return this._has_cdAgenciaOperadora;
    } //-- boolean hasCdAgenciaOperadora() 

    /**
     * Method hasCdAtividadeEconomica
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAtividadeEconomica()
    {
        return this._has_cdAtividadeEconomica;
    } //-- boolean hasCdAtividadeEconomica() 

    /**
     * Method hasCdClubRepresentante
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdClubRepresentante()
    {
        return this._has_cdClubRepresentante;
    } //-- boolean hasCdClubRepresentante() 

    /**
     * Method hasCdControleCpfCnpjRepresentante
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdControleCpfCnpjRepresentante()
    {
        return this._has_cdControleCpfCnpjRepresentante;
    } //-- boolean hasCdControleCpfCnpjRepresentante() 

    /**
     * Method hasCdCpfCnpjRepresentante
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCpfCnpjRepresentante()
    {
        return this._has_cdCpfCnpjRepresentante;
    } //-- boolean hasCdCpfCnpjRepresentante() 

    /**
     * Method hasCdFilialCpfCnpjRepresentante
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFilialCpfCnpjRepresentante()
    {
        return this._has_cdFilialCpfCnpjRepresentante;
    } //-- boolean hasCdFilialCpfCnpjRepresentante() 

    /**
     * Method hasCdFuncionarioBradesco
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFuncionarioBradesco()
    {
        return this._has_cdFuncionarioBradesco;
    } //-- boolean hasCdFuncionarioBradesco() 

    /**
     * Method hasCdGrupoEconomico
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdGrupoEconomico()
    {
        return this._has_cdGrupoEconomico;
    } //-- boolean hasCdGrupoEconomico() 

    /**
     * Method hasCdMotivoSituacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMotivoSituacao()
    {
        return this._has_cdMotivoSituacao;
    } //-- boolean hasCdMotivoSituacao() 

    /**
     * Method hasCdPessoaJuridica
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridica()
    {
        return this._has_cdPessoaJuridica;
    } //-- boolean hasCdPessoaJuridica() 

    /**
     * Method hasCdSegmentoCliente
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSegmentoCliente()
    {
        return this._has_cdSegmentoCliente;
    } //-- boolean hasCdSegmentoCliente() 

    /**
     * Method hasCdSituacaoContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSituacaoContrato()
    {
        return this._has_cdSituacaoContrato;
    } //-- boolean hasCdSituacaoContrato() 

    /**
     * Method hasCdSubSegmentoCliente
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSubSegmentoCliente()
    {
        return this._has_cdSubSegmentoCliente;
    } //-- boolean hasCdSubSegmentoCliente() 

    /**
     * Method hasCdTipoContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContrato()
    {
        return this._has_cdTipoContrato;
    } //-- boolean hasCdTipoContrato() 

    /**
     * Method hasNrSequenciaContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaContrato()
    {
        return this._has_nrSequenciaContrato;
    } //-- boolean hasNrSequenciaContrato() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdAditivo'.
     * 
     * @param cdAditivo the value of field 'cdAditivo'.
     */
    public void setCdAditivo(java.lang.String cdAditivo)
    {
        this._cdAditivo = cdAditivo;
    } //-- void setCdAditivo(java.lang.String) 

    /**
     * Sets the value of field 'cdAgenciaOperadora'.
     * 
     * @param cdAgenciaOperadora the value of field
     * 'cdAgenciaOperadora'.
     */
    public void setCdAgenciaOperadora(int cdAgenciaOperadora)
    {
        this._cdAgenciaOperadora = cdAgenciaOperadora;
        this._has_cdAgenciaOperadora = true;
    } //-- void setCdAgenciaOperadora(int) 

    /**
     * Sets the value of field 'cdAtividadeEconomica'.
     * 
     * @param cdAtividadeEconomica the value of field
     * 'cdAtividadeEconomica'.
     */
    public void setCdAtividadeEconomica(int cdAtividadeEconomica)
    {
        this._cdAtividadeEconomica = cdAtividadeEconomica;
        this._has_cdAtividadeEconomica = true;
    } //-- void setCdAtividadeEconomica(int) 

    /**
     * Sets the value of field 'cdClubRepresentante'.
     * 
     * @param cdClubRepresentante the value of field
     * 'cdClubRepresentante'.
     */
    public void setCdClubRepresentante(long cdClubRepresentante)
    {
        this._cdClubRepresentante = cdClubRepresentante;
        this._has_cdClubRepresentante = true;
    } //-- void setCdClubRepresentante(long) 

    /**
     * Sets the value of field 'cdControleCpfCnpjRepresentante'.
     * 
     * @param cdControleCpfCnpjRepresentante the value of field
     * 'cdControleCpfCnpjRepresentante'.
     */
    public void setCdControleCpfCnpjRepresentante(int cdControleCpfCnpjRepresentante)
    {
        this._cdControleCpfCnpjRepresentante = cdControleCpfCnpjRepresentante;
        this._has_cdControleCpfCnpjRepresentante = true;
    } //-- void setCdControleCpfCnpjRepresentante(int) 

    /**
     * Sets the value of field 'cdCpfCnpjRepresentante'.
     * 
     * @param cdCpfCnpjRepresentante the value of field
     * 'cdCpfCnpjRepresentante'.
     */
    public void setCdCpfCnpjRepresentante(long cdCpfCnpjRepresentante)
    {
        this._cdCpfCnpjRepresentante = cdCpfCnpjRepresentante;
        this._has_cdCpfCnpjRepresentante = true;
    } //-- void setCdCpfCnpjRepresentante(long) 

    /**
     * Sets the value of field 'cdDigitoAgenciaOperadora'.
     * 
     * @param cdDigitoAgenciaOperadora the value of field
     * 'cdDigitoAgenciaOperadora'.
     */
    public void setCdDigitoAgenciaOperadora(java.lang.String cdDigitoAgenciaOperadora)
    {
        this._cdDigitoAgenciaOperadora = cdDigitoAgenciaOperadora;
    } //-- void setCdDigitoAgenciaOperadora(java.lang.String) 

    /**
     * Sets the value of field 'cdFilialCpfCnpjRepresentante'.
     * 
     * @param cdFilialCpfCnpjRepresentante the value of field
     * 'cdFilialCpfCnpjRepresentante'.
     */
    public void setCdFilialCpfCnpjRepresentante(int cdFilialCpfCnpjRepresentante)
    {
        this._cdFilialCpfCnpjRepresentante = cdFilialCpfCnpjRepresentante;
        this._has_cdFilialCpfCnpjRepresentante = true;
    } //-- void setCdFilialCpfCnpjRepresentante(int) 

    /**
     * Sets the value of field 'cdFuncionarioBradesco'.
     * 
     * @param cdFuncionarioBradesco the value of field
     * 'cdFuncionarioBradesco'.
     */
    public void setCdFuncionarioBradesco(long cdFuncionarioBradesco)
    {
        this._cdFuncionarioBradesco = cdFuncionarioBradesco;
        this._has_cdFuncionarioBradesco = true;
    } //-- void setCdFuncionarioBradesco(long) 

    /**
     * Sets the value of field 'cdGrupoEconomico'.
     * 
     * @param cdGrupoEconomico the value of field 'cdGrupoEconomico'
     */
    public void setCdGrupoEconomico(long cdGrupoEconomico)
    {
        this._cdGrupoEconomico = cdGrupoEconomico;
        this._has_cdGrupoEconomico = true;
    } //-- void setCdGrupoEconomico(long) 

    /**
     * Sets the value of field 'cdMotivoSituacao'.
     * 
     * @param cdMotivoSituacao the value of field 'cdMotivoSituacao'
     */
    public void setCdMotivoSituacao(int cdMotivoSituacao)
    {
        this._cdMotivoSituacao = cdMotivoSituacao;
        this._has_cdMotivoSituacao = true;
    } //-- void setCdMotivoSituacao(int) 

    /**
     * Sets the value of field 'cdPessoaJuridica'.
     * 
     * @param cdPessoaJuridica the value of field 'cdPessoaJuridica'
     */
    public void setCdPessoaJuridica(long cdPessoaJuridica)
    {
        this._cdPessoaJuridica = cdPessoaJuridica;
        this._has_cdPessoaJuridica = true;
    } //-- void setCdPessoaJuridica(long) 

    /**
     * Sets the value of field 'cdSegmentoCliente'.
     * 
     * @param cdSegmentoCliente the value of field
     * 'cdSegmentoCliente'.
     */
    public void setCdSegmentoCliente(int cdSegmentoCliente)
    {
        this._cdSegmentoCliente = cdSegmentoCliente;
        this._has_cdSegmentoCliente = true;
    } //-- void setCdSegmentoCliente(int) 

    /**
     * Sets the value of field 'cdSituacaoContrato'.
     * 
     * @param cdSituacaoContrato the value of field
     * 'cdSituacaoContrato'.
     */
    public void setCdSituacaoContrato(int cdSituacaoContrato)
    {
        this._cdSituacaoContrato = cdSituacaoContrato;
        this._has_cdSituacaoContrato = true;
    } //-- void setCdSituacaoContrato(int) 

    /**
     * Sets the value of field 'cdSubSegmentoCliente'.
     * 
     * @param cdSubSegmentoCliente the value of field
     * 'cdSubSegmentoCliente'.
     */
    public void setCdSubSegmentoCliente(int cdSubSegmentoCliente)
    {
        this._cdSubSegmentoCliente = cdSubSegmentoCliente;
        this._has_cdSubSegmentoCliente = true;
    } //-- void setCdSubSegmentoCliente(int) 

    /**
     * Sets the value of field 'cdTipoContrato'.
     * 
     * @param cdTipoContrato the value of field 'cdTipoContrato'.
     */
    public void setCdTipoContrato(int cdTipoContrato)
    {
        this._cdTipoContrato = cdTipoContrato;
        this._has_cdTipoContrato = true;
    } //-- void setCdTipoContrato(int) 

    /**
     * Sets the value of field 'cdTipoParticipacao'.
     * 
     * @param cdTipoParticipacao the value of field
     * 'cdTipoParticipacao'.
     */
    public void setCdTipoParticipacao(java.lang.String cdTipoParticipacao)
    {
        this._cdTipoParticipacao = cdTipoParticipacao;
    } //-- void setCdTipoParticipacao(java.lang.String) 

    /**
     * Sets the value of field 'dsAgenciaOperadora'.
     * 
     * @param dsAgenciaOperadora the value of field
     * 'dsAgenciaOperadora'.
     */
    public void setDsAgenciaOperadora(java.lang.String dsAgenciaOperadora)
    {
        this._dsAgenciaOperadora = dsAgenciaOperadora;
    } //-- void setDsAgenciaOperadora(java.lang.String) 

    /**
     * Sets the value of field 'dsAtividadeEconomica'.
     * 
     * @param dsAtividadeEconomica the value of field
     * 'dsAtividadeEconomica'.
     */
    public void setDsAtividadeEconomica(java.lang.String dsAtividadeEconomica)
    {
        this._dsAtividadeEconomica = dsAtividadeEconomica;
    } //-- void setDsAtividadeEconomica(java.lang.String) 

    /**
     * Sets the value of field 'dsContrato'.
     * 
     * @param dsContrato the value of field 'dsContrato'.
     */
    public void setDsContrato(java.lang.String dsContrato)
    {
        this._dsContrato = dsContrato;
    } //-- void setDsContrato(java.lang.String) 

    /**
     * Sets the value of field 'dsGrupoEconomico'.
     * 
     * @param dsGrupoEconomico the value of field 'dsGrupoEconomico'
     */
    public void setDsGrupoEconomico(java.lang.String dsGrupoEconomico)
    {
        this._dsGrupoEconomico = dsGrupoEconomico;
    } //-- void setDsGrupoEconomico(java.lang.String) 

    /**
     * Sets the value of field 'dsMotivoSituacao'.
     * 
     * @param dsMotivoSituacao the value of field 'dsMotivoSituacao'
     */
    public void setDsMotivoSituacao(java.lang.String dsMotivoSituacao)
    {
        this._dsMotivoSituacao = dsMotivoSituacao;
    } //-- void setDsMotivoSituacao(java.lang.String) 

    /**
     * Sets the value of field 'dsPessoaJuridica'.
     * 
     * @param dsPessoaJuridica the value of field 'dsPessoaJuridica'
     */
    public void setDsPessoaJuridica(java.lang.String dsPessoaJuridica)
    {
        this._dsPessoaJuridica = dsPessoaJuridica;
    } //-- void setDsPessoaJuridica(java.lang.String) 

    /**
     * Sets the value of field 'dsSegmentoCliente'.
     * 
     * @param dsSegmentoCliente the value of field
     * 'dsSegmentoCliente'.
     */
    public void setDsSegmentoCliente(java.lang.String dsSegmentoCliente)
    {
        this._dsSegmentoCliente = dsSegmentoCliente;
    } //-- void setDsSegmentoCliente(java.lang.String) 

    /**
     * Sets the value of field 'dsSituacaoContrato'.
     * 
     * @param dsSituacaoContrato the value of field
     * 'dsSituacaoContrato'.
     */
    public void setDsSituacaoContrato(java.lang.String dsSituacaoContrato)
    {
        this._dsSituacaoContrato = dsSituacaoContrato;
    } //-- void setDsSituacaoContrato(java.lang.String) 

    /**
     * Sets the value of field 'dsSubSegmentoCliente'.
     * 
     * @param dsSubSegmentoCliente the value of field
     * 'dsSubSegmentoCliente'.
     */
    public void setDsSubSegmentoCliente(java.lang.String dsSubSegmentoCliente)
    {
        this._dsSubSegmentoCliente = dsSubSegmentoCliente;
    } //-- void setDsSubSegmentoCliente(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoContrato'.
     * 
     * @param dsTipoContrato the value of field 'dsTipoContrato'.
     */
    public void setDsTipoContrato(java.lang.String dsTipoContrato)
    {
        this._dsTipoContrato = dsTipoContrato;
    } //-- void setDsTipoContrato(java.lang.String) 

    /**
     * Sets the value of field 'dtAbertura'.
     * 
     * @param dtAbertura the value of field 'dtAbertura'.
     */
    public void setDtAbertura(java.lang.String dtAbertura)
    {
        this._dtAbertura = dtAbertura;
    } //-- void setDtAbertura(java.lang.String) 

    /**
     * Sets the value of field 'dtEncerramento'.
     * 
     * @param dtEncerramento the value of field 'dtEncerramento'.
     */
    public void setDtEncerramento(java.lang.String dtEncerramento)
    {
        this._dtEncerramento = dtEncerramento;
    } //-- void setDtEncerramento(java.lang.String) 

    /**
     * Sets the value of field 'dtInclusao'.
     * 
     * @param dtInclusao the value of field 'dtInclusao'.
     */
    public void setDtInclusao(java.lang.String dtInclusao)
    {
        this._dtInclusao = dtInclusao;
    } //-- void setDtInclusao(java.lang.String) 

    /**
     * Sets the value of field 'nmFuncionarioBradesco'.
     * 
     * @param nmFuncionarioBradesco the value of field
     * 'nmFuncionarioBradesco'.
     */
    public void setNmFuncionarioBradesco(java.lang.String nmFuncionarioBradesco)
    {
        this._nmFuncionarioBradesco = nmFuncionarioBradesco;
    } //-- void setNmFuncionarioBradesco(java.lang.String) 

    /**
     * Sets the value of field 'nmRazaoSocialRepresentante'.
     * 
     * @param nmRazaoSocialRepresentante the value of field
     * 'nmRazaoSocialRepresentante'.
     */
    public void setNmRazaoSocialRepresentante(java.lang.String nmRazaoSocialRepresentante)
    {
        this._nmRazaoSocialRepresentante = nmRazaoSocialRepresentante;
    } //-- void setNmRazaoSocialRepresentante(java.lang.String) 

    /**
     * Sets the value of field 'nrSequenciaContrato'.
     * 
     * @param nrSequenciaContrato the value of field
     * 'nrSequenciaContrato'.
     */
    public void setNrSequenciaContrato(long nrSequenciaContrato)
    {
        this._nrSequenciaContrato = nrSequenciaContrato;
        this._has_nrSequenciaContrato = true;
    } //-- void setNrSequenciaContrato(long) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listarcontratospgit.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listarcontratospgit.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listarcontratospgit.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarcontratospgit.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
