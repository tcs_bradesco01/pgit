/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/implementation.ftl,v $
 * $Id: implementation.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: implementation.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.consultarpagamentosconsolidado.impl;

import static br.com.bradesco.web.pgit.service.business.consultarpagamentosconsolidado.IConsultarPagamentosConsolidadoServiceConstants.NUMERO_OCORRENCIAS_DETALHAR;
import static br.com.bradesco.web.pgit.utils.NumberUtils.format;
import static br.com.bradesco.web.pgit.utils.PgitUtil.formatAgencia;
import static br.com.bradesco.web.pgit.utils.PgitUtil.formatBanco;
import static br.com.bradesco.web.pgit.utils.PgitUtil.formatBancoAgenciaConta;
import static br.com.bradesco.web.pgit.utils.PgitUtil.formatConta;
import static br.com.bradesco.web.pgit.utils.PgitUtil.verificaIntegerNulo;
import static br.com.bradesco.web.pgit.utils.PgitUtil.verificaLongNulo;
import static br.com.bradesco.web.pgit.utils.PgitUtil.verificaStringNula;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import br.com.bradesco.web.pgit.service.business.consultarpagamentosconsolidado.IConsultarPagamentosConsolidadoService;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosconsolidado.IConsultarPagamentosConsolidadoServiceConstants;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosconsolidado.bean.ConsultarPagamentosConsolidadosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosconsolidado.bean.ConsultarPagamentosConsolidadosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosconsolidado.bean.DetalharPagamentosConsolidadosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosconsolidado.bean.DetalharPagamentosConsolidadosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosconsolidado.bean.OcorrenciasDetalharPagamentosConsolidados;
import br.com.bradesco.web.pgit.service.data.pdc.FactoryAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarimprimirpgtoconsolidado.request.ConsultarImprimirPgtoConsolidadoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarpagamentosconsolidados.request.ConsultarPagamentosConsolidadosRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarpagamentosconsolidados.response.ConsultarPagamentosConsolidadosResponse;
import br.com.bradesco.web.pgit.service.data.pdc.detalharpagamentosconsolidados.request.DetalharPagamentosConsolidadosRequest;
import br.com.bradesco.web.pgit.service.data.pdc.detalharpagamentosconsolidados.response.DetalharPagamentosConsolidadosResponse;
import br.com.bradesco.web.pgit.service.data.pdc.imprimirpagamentosconsolidados.request.ImprimirPagamentosConsolidadosRequest;
import br.com.bradesco.web.pgit.service.data.pdc.imprimirpagamentosconsolidados.response.ImprimirPagamentosConsolidadosResponse;
import br.com.bradesco.web.pgit.utils.CpfCnpjUtils;
import br.com.bradesco.web.pgit.utils.NumberUtils;
import br.com.bradesco.web.pgit.utils.PgitUtil;
import br.com.bradesco.web.pgit.view.converters.FormatarData;

/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Implementa��o do adaptador: ConsultarPagamentosConsolidado
 * </p>
 * 
 * @comment C�DIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public class ConsultarPagamentosConsolidadoServiceImpl implements
		IConsultarPagamentosConsolidadoService {

	/** Atributo nf. */
	private java.text.NumberFormat nf = java.text.NumberFormat
			.getInstance(new Locale("pt_br"));

	/** Atributo factoryAdapter. */
	private FactoryAdapter factoryAdapter;

	/**
	 * Get: factoryAdapter.
	 * 
	 * @return factoryAdapter
	 */
	public FactoryAdapter getFactoryAdapter() {
		return factoryAdapter;
	}

	/**
	 * Set: factoryAdapter.
	 * 
	 * @param factoryAdapter
	 *            the factory adapter
	 */
	public void setFactoryAdapter(FactoryAdapter factoryAdapter) {
		this.factoryAdapter = factoryAdapter;
	}

	/**
	 * Construtor.
	 */
	public ConsultarPagamentosConsolidadoServiceImpl() {
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.service.business.consultarpagamentosconsolidado.IConsultarPagamentosConsolidadoService#consultarPagamentosConsolidados(br.com.bradesco.web.pgit.service.business.consultarpagamentosconsolidado.bean.ConsultarPagamentosConsolidadosEntradaDTO)
	 */
	public List<ConsultarPagamentosConsolidadosSaidaDTO> consultarPagamentosConsolidados(
			ConsultarPagamentosConsolidadosEntradaDTO entradaDTO) {
		ConsultarPagamentosConsolidadosRequest request = new ConsultarPagamentosConsolidadosRequest();
		ConsultarPagamentosConsolidadosResponse response = new ConsultarPagamentosConsolidadosResponse();
		List<ConsultarPagamentosConsolidadosSaidaDTO> listaSaida = new ArrayList<ConsultarPagamentosConsolidadosSaidaDTO>();

		request.setCdTipoPesquisa(PgitUtil.verificaIntegerNulo(entradaDTO
				.getCdTipoPesquisa()));
		request.setCdAgendadosPagosNaoPagos(PgitUtil
				.verificaIntegerNulo(entradaDTO.getCdAgendadosPagosNaoPagos()));
		request
				.setNrOcorrencias(IConsultarPagamentosConsolidadoServiceConstants.NUMERO_OCORRENCIAS_CONSULTAR);
		request.setCdPessoaJuridicaContrato(PgitUtil
				.verificaLongNulo(entradaDTO.getCdPessoaJuridicaContrato()));
		request.setCdTipoContratoNegocio(PgitUtil
				.verificaIntegerNulo(entradaDTO.getCdTipoContratoNegocio()));
		request.setNrSequenciaContratoNegocio(PgitUtil
				.verificaLongNulo(entradaDTO.getNrSequenciaContratoNegocio()));
		request.setDtCreditoPagamentoInicio(PgitUtil
				.verificaStringNula(entradaDTO.getDtCreditoPagamentoInicio()));
		request.setDtCreditoPagamentoFim(PgitUtil.verificaStringNula(entradaDTO
				.getDtCreditoPagamentoFim()));
		request.setNrArquivoRemessaPagamento(PgitUtil
				.verificaLongNulo(entradaDTO.getNrArquivoRemessaPagamento()));
		request.setCdBanco(PgitUtil
				.verificaIntegerNulo(entradaDTO.getCdBanco()));
		request.setCdAgencia(PgitUtil.verificaIntegerNulo(entradaDTO
				.getCdAgencia()));
		request.setCdConta(PgitUtil.verificaLongNulo(entradaDTO.getCdConta()));
		request.setCdDigitoConta(PgitUtil.DIGITO_CONTA);
		request.setCdProdutoServicoOperacao(PgitUtil
				.verificaIntegerNulo(entradaDTO.getCdProdutoServicoOperacao()));
		request.setCdProdutoServicoRelacionado(PgitUtil
				.verificaIntegerNulo(entradaDTO
						.getCdProdutoServicoRelacionado()));
		request.setCdSituacaoOperacaoPagamento(PgitUtil
				.verificaIntegerNulo(entradaDTO
						.getCdSituacaoOperacaoPagamento()));
		request
				.setCdMotivoSituacaoPagamento(PgitUtil
						.verificaIntegerNulo(entradaDTO
								.getCdMotivoSituacaoPagamento()));
		request.setCdTituloPgtoRastreado(PgitUtil
				.verificaIntegerNulo(entradaDTO.getCdTituloPgtoRastreado()));

		request.setCdIndicadorAutorizacao(PgitUtil
				.verificaIntegerNulo(entradaDTO.getCdIndicadorAutorizacao()));
		request.setNrLoteInterno(PgitUtil.verificaLongNulo(entradaDTO
				.getNrLoteInterno()));
		request.setCdTipoLayout(PgitUtil.verificaIntegerNulo(entradaDTO
				.getCdTipoLayout()));

		response = getFactoryAdapter()
				.getConsultarPagamentosConsolidadosPDCAdapter().invokeProcess(
						request);

		ConsultarPagamentosConsolidadosSaidaDTO saida;

		for (int i = 0; i < response.getOcorrenciasCount(); i++) {
			saida = new ConsultarPagamentosConsolidadosSaidaDTO();
			saida.setCodMensagem(response.getCodMensagem());
			saida.setMensagem(response.getMensagem());

			saida.setCdPessoaJuridicaContrato(response.getOcorrencias(i)
					.getCdPessoaJuridicaContrato());
			saida.setDsRazaoSocial(response.getOcorrencias(i)
					.getDsRazaoSocial());
			saida.setCdTipoContratoNegocio(response.getOcorrencias(i)
					.getCdTipoContratoNegocio());
			saida.setNrContratoOrigem(response.getOcorrencias(i)
					.getNrContratoOrigem());
			saida.setNrCnpjCpf(response.getOcorrencias(i).getNrCnpjCpf());
			saida.setNrFilialCnpjCpf(response.getOcorrencias(i)
					.getNrFilialCnpjCpf());
			saida.setNrDigitoCnpjCpf(response.getOcorrencias(i)
					.getNrDigitoCnpjCpf());
			saida.setNrArquivoRemessaPagamento(response.getOcorrencias(i)
					.getNrArquivoRemessaPagamento());
			saida.setDtCreditoPagamento(FormatarData
					.formatarDataFromPdc(response.getOcorrencias(i)
							.getDtCreditoPagamento()));
			saida.setCdBanco(response.getOcorrencias(i).getCdBanco());
			saida.setCdAgencia(response.getOcorrencias(i).getCdAgencia());
			saida.setCdDigitoAgencia(response.getOcorrencias(i)
					.getCdDigitoAgencia());
			saida.setCdConta(response.getOcorrencias(i).getCdConta());
			saida.setCdDigitoConta(response.getOcorrencias(i)
					.getCdDigitoConta());

			saida.setContaDebitoFormatado(PgitUtil.formatBancoAgenciaConta(
					saida.getCdBanco(), saida.getCdAgencia(), saida
							.getCdDigitoAgencia(), saida.getCdConta(), saida
							.getCdDigitoConta(), true));

			saida.setCdProdutoServicoOperacao(response.getOcorrencias(i)
					.getCdProdutoServicoOperacao());
			saida.setDsResumoProdutoServico(response.getOcorrencias(i)
					.getDsResumoProdutoServico());
			saida.setCdProdutoServicoRelacionado(response.getOcorrencias(i)
					.getCdProdutoServicoRelacionado());
			saida.setDsOperacaoProdutoServico(response.getOcorrencias(i)
					.getDsOperacaoProdutoServico());
			saida.setCdSituacaoOperacaoPagamento(response.getOcorrencias(i)
					.getCdSituacaoOperacaoPagamento());
			saida.setDsSituacaoOperacaoPagamento(response.getOcorrencias(i)
					.getDsSituacaoOperacaoPagamento());

			saida.setQtPagamentoFormatado(nf.format(new java.math.BigDecimal(
					response.getOcorrencias(i).getQtPagamento())));
			saida.setQtPagamento(response.getOcorrencias(i).getQtPagamento());
			saida.setVlEfetivoPagamentoCliente(response.getOcorrencias(i)
					.getVlEfetivoPagamentoCliente());
			saida.setCdPessoaContratoDebito(response.getOcorrencias(i)
					.getCdPessoaContratoDebito());
			saida.setCdTipoContratoDebito(response.getOcorrencias(i)
					.getCdTipoContratoDebito());
			saida.setNrSequenciaContratoDebito(response.getOcorrencias(i)
					.getNrSequenciaContratoDebito());

			saida.setCnpjFormatado(CpfCnpjUtils.formatarCpfCnpj(response
					.getOcorrencias(i).getNrCnpjCpf(), response.getOcorrencias(
					i).getNrFilialCnpjCpf(), response.getOcorrencias(i)
					.getNrDigitoCnpjCpf()));

			listaSaida.add(saida);
		}

		return listaSaida;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.service.business.consultarpagamentosconsolidado.IConsultarPagamentosConsolidadoService#detalharPagamentosConsolidados(br.com.bradesco.web.pgit.service.business.consultarpagamentosconsolidado.bean.DetalharPagamentosConsolidadosEntradaDTO)
	 */
	public DetalharPagamentosConsolidadosSaidaDTO detalharPagamentosConsolidados(
			DetalharPagamentosConsolidadosEntradaDTO entrada) {
		DetalharPagamentosConsolidadosSaidaDTO saida = new DetalharPagamentosConsolidadosSaidaDTO();
		DetalharPagamentosConsolidadosRequest request = new DetalharPagamentosConsolidadosRequest();
		DetalharPagamentosConsolidadosResponse response = new DetalharPagamentosConsolidadosResponse();

		request
				.setNrOcorrencias(IConsultarPagamentosConsolidadoServiceConstants.NUMERO_OCORRENCIAS_DETALHAR);
		request.setCdPessoaJuridicaContrato(PgitUtil.verificaLongNulo(entrada
				.getCdPessoaJuridicaContrato()));
		request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(entrada
				.getCdTipoContratoNegocio()));
		request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(entrada
				.getNrSequenciaContratoNegocio()));
		request.setNrCnpjCpf(PgitUtil.verificaLongNulo(entrada.getNrCnpjCpf()));
		request.setNrFilialcnpjCpf(PgitUtil.verificaIntegerNulo(entrada
				.getNrFilialcnpjCpf()));
		request.setNrControleCnpjCpf(PgitUtil.verificaIntegerNulo(entrada
				.getNrControleCnpjCpf()));
		request.setNrArquivoRemessaPagamento(PgitUtil.verificaLongNulo(entrada
				.getNrArquivoRemessaPagamento()));
		request.setDtCreditoPagamento(PgitUtil.verificaStringNula(entrada
				.getDtCreditoPagamento()));
		request.setCdBanco(PgitUtil.verificaIntegerNulo(entrada.getCdBanco()));
		request.setCdAgencia(PgitUtil.verificaIntegerNulo(entrada
				.getCdAgencia()));
		request.setCdConta(PgitUtil.verificaLongNulo(entrada.getCdConta()));
		request.setCdDigitoConta(PgitUtil.verificaStringNula(entrada
				.getCdDigitoConta()));
		request.setCdProdutoServicoOperacao(PgitUtil
				.verificaIntegerNulo(entrada.getCdProdutoServicoOperacao()));
		request.setCdProdutoServicoRelacionado(PgitUtil
				.verificaIntegerNulo(entrada.getCdProdutoServicoRelacionado()));
		request.setCdSituacaoOperacaoPagamento(PgitUtil
				.verificaIntegerNulo(entrada.getCdSituacaoOperacaoPagamento()));
		request.setCdAgendadoPagaoNaoPago(PgitUtil.verificaIntegerNulo(entrada
				.getCdAgendadoPagaoNaoPago()));
		request.setCdPessoaContratoDebito(PgitUtil.verificaLongNulo(entrada
				.getCdPessoaContratoDebito()));
		request.setCdTipoContratoDebito(PgitUtil.verificaIntegerNulo(entrada
				.getCdTipoContratoDebito()));
		request.setNrSequenciaContratoDebito(PgitUtil.verificaLongNulo(entrada
				.getNrSequenciaContratoDebito()));
		request.setCdTituloPgtoRastreado(PgitUtil.verificaIntegerNulo(entrada
				.getCdTituloPgtoRastreado()));
		request.setCdIndicadorAutorizacaoPagador(PgitUtil.verificaIntegerNulo(entrada.getCdIndicadorAutorizacaoPagador()));
	    request.setNrLoteInternoPagamento(PgitUtil.verificaLongNulo(entrada.getNrLoteInternoPagamento()));

		response = getFactoryAdapter()
				.getDetalharPagamentosConsolidadosPDCAdapter().invokeProcess(
						request);

		saida.setCodMensagem(response.getCodMensagem());
		saida.setMensagem(response.getMensagem());
		saida.setDsBancoDebito(response.getDsBancoDebito());
		saida.setDsAgenciaDebito(response.getDsAgenciaDebito());
		saida.setDsTipoContaDebito(response.getDsTipoContaDebito());
		saida.setDsContrato(response.getDsContrato());
		saida.setDsSituacaoContrato(response.getDsSituacaoContrato());
		saida.setNomeCliente(response.getNmCliente());
		saida.setNumeroConsultas(response.getNumeroConsultas());

		List<OcorrenciasDetalharPagamentosConsolidados> listaOcorrecnias = new ArrayList<OcorrenciasDetalharPagamentosConsolidados>();
		for (int i = 0; i < response.getOcorrenciasCount(); i++) {
			OcorrenciasDetalharPagamentosConsolidados ocorrencias = new OcorrenciasDetalharPagamentosConsolidados();
			ocorrencias.setNrPagamento(response.getOcorrencias(i)
					.getNrPagamento());
			ocorrencias.setVlPagamento(response.getOcorrencias(i)
					.getVlPagamento());
			ocorrencias.setCdCnpjCpfFavorecido(response.getOcorrencias(i)
					.getCdCnpjCpfFavorecido());
			ocorrencias.setCdFilialCnpjCpfFavorecido(response.getOcorrencias(i)
					.getCdFilialCnpjCpfFavorecido());
			ocorrencias.setCdControleCnpjCpfFavorecido(response.getOcorrencias(
					i).getCdControleCnpjCpfFavorecido());
			ocorrencias.setCdFavorecido(response.getOcorrencias(i)
					.getCdFavorecido());
			ocorrencias.setDsBeneficio(response.getOcorrencias(i)
					.getDsBeneficio());
			ocorrencias.setCdBanco(response.getOcorrencias(i).getCdBanco());
			ocorrencias.setCdAgencia(response.getOcorrencias(i).getCdAgencia());
			ocorrencias.setCdDigitoAgencia(response.getOcorrencias(i)
					.getCdDigitoAgencia());
			ocorrencias.setCdConta(response.getOcorrencias(i).getCdConta());
			ocorrencias.setCdDigitoConta(response.getOcorrencias(i)
					.getCdDigitoConta());
			ocorrencias.setCdTipoCanal(response.getOcorrencias(i)
					.getCdTipoCanal());
			ocorrencias.setCdTipoTela(response.getOcorrencias(i)
					.getCdTipoTela());
			ocorrencias.setDsEfetivacaoPagamento(response.getOcorrencias(i)
					.getDsEfetivacaoPagamento());

			// Formatando campos
			ocorrencias.setFavorecidoBeneficiarioFormatado(response.getOcorrencias(i).getDsBeneficio());
			ocorrencias.setBancoFormatado(PgitUtil.formatBanco(ocorrencias
					.getCdBanco(), false));
			ocorrencias.setAgenciaFormatada(PgitUtil.formatAgencia(ocorrencias
					.getCdAgencia(), ocorrencias.getCdDigitoAgencia(), false));
			ocorrencias.setContaFormatada(PgitUtil.formatConta(ocorrencias
					.getCdConta(), ocorrencias.getCdDigitoConta(), false));
			
			String cdContaPagamentoDestino = response.getOcorrencias(i).getContaPagtoDestino();
			if(cdContaPagamentoDestino != null && !"".equals(cdContaPagamentoDestino) && !"0".equals(cdContaPagamentoDestino)){
				ocorrencias.setBancoAgenciaContaCreditoFormatado(PgitUtil.formatBancoIspbConta(response.getOcorrencias(i).getCdBanco(), 
						response.getOcorrencias(i).getCdIspbPagtoDestino(), cdContaPagamentoDestino, true));
			}else{
				ocorrencias.setBancoAgenciaContaCreditoFormatado(PgitUtil
						.formatBancoAgenciaConta(response.getOcorrencias(i)
								.getCdBanco(), response.getOcorrencias(i)
								.getCdAgencia(), response.getOcorrencias(i)
								.getCdDigitoAgencia(), response.getOcorrencias(i)
								.getCdConta(), response.getOcorrencias(i)
								.getCdDigitoConta(), true));
			}
			
			
			
			ocorrencias.setVlPagamentoFormatado(NumberUtils.format(ocorrencias
					.getVlPagamento(), "#,##0.00"));

			ocorrencias.setDsIndicadorAutorizacao(response.getOcorrencias(i)
					.getDsIndicadorAutorizacao());
			ocorrencias.setCdLoteInterno(response.getOcorrencias(i)
					.getCdLoteInterno());
			ocorrencias.setDsTipoLayout(response.getOcorrencias(i)
					.getDsTipoLayout());
			ocorrencias.setDsIndicadorPagamento(response.getOcorrencias(i).getDsIndicadorPagamento());
			ocorrencias.setCdIspbPagtoDestino(response.getOcorrencias(i).getCdIspbPagtoDestino());
			ocorrencias.setContaPagtoDestino(response.getOcorrencias(i).getContaPagtoDestino());
				
			
			listaOcorrecnias.add(ocorrencias);
		}
		saida.setListaOcorrecnias(listaOcorrecnias);

		return saida;
	}

    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.consultarpagamentosconsolidado.IConsultarPagamentosConsolidadoService#consultarImprimirPgtoConsolidado(br.com.bradesco.web.pgit.service.business.consultarpagamentosconsolidado.bean.DetalharPagamentosConsolidadosEntradaDTO)
     */
    public void consultarImprimirPgtoConsolidado(DetalharPagamentosConsolidadosEntradaDTO entrada) {
        
        ConsultarImprimirPgtoConsolidadoRequest request = new ConsultarImprimirPgtoConsolidadoRequest();
        
        request.setNrOcorrencias(NUMERO_OCORRENCIAS_DETALHAR);
        request.setCdPessoaJuridicaContrato(verificaLongNulo(entrada.getCdPessoaJuridicaContrato()));
        request.setCdTipoContratoNegocio(verificaIntegerNulo(entrada.getCdTipoContratoNegocio()));
        request.setNrSequenciaContratoNegocio(verificaLongNulo(entrada.getNrSequenciaContratoNegocio()));
        request.setNrCnpjCpf(verificaLongNulo(entrada.getNrCnpjCpf()));
        request.setNrFilialcnpjCpf(verificaIntegerNulo(entrada.getNrFilialcnpjCpf()));
        request.setNrControleCnpjCpf(verificaIntegerNulo(entrada.getNrControleCnpjCpf()));
        request.setNrArquivoRemessaPagamento(verificaLongNulo(entrada.getNrArquivoRemessaPagamento()));
        request.setDtCreditoPagamento(verificaStringNula(entrada.getDtCreditoPagamento()));
        request.setCdBanco(verificaIntegerNulo(entrada.getCdBanco()));
        request.setCdAgencia(verificaIntegerNulo(entrada.getCdAgencia()));
        request.setCdConta(verificaLongNulo(entrada.getCdConta()));
        request.setCdDigitoConta(verificaStringNula(entrada.getCdDigitoConta()));
        request.setCdProdutoServicoOperacao(verificaIntegerNulo(entrada.getCdProdutoServicoOperacao()));
        request.setCdProdutoServicoRelacionado(verificaIntegerNulo(entrada.getCdProdutoServicoRelacionado()));
        request.setCdSituacaoOperacaoPagamento(verificaIntegerNulo(entrada.getCdSituacaoOperacaoPagamento()));
        request.setCdAgendadoPagaoNaoPago(verificaIntegerNulo(entrada.getCdAgendadoPagaoNaoPago()));
        request.setCdPessoaContratoDebito(verificaLongNulo(entrada.getCdPessoaContratoDebito()));
        request.setCdTipoContratoDebito(verificaIntegerNulo(entrada.getCdTipoContratoDebito()));
        request.setNrSequenciaContratoDebito(verificaLongNulo(entrada.getNrSequenciaContratoDebito()));
        request.setCdTituloPgtoRastreado(verificaIntegerNulo(entrada.getCdTituloPgtoRastreado()));
        request.setCdIndicadorAutorizacaoPagador(verificaIntegerNulo(entrada.getCdIndicadorAutorizacaoPagador()));
        request.setNrLoteInternoPagamento(verificaLongNulo(entrada.getNrLoteInternoPagamento()));
        
        getFactoryAdapter().getConsultarImprimirPgtoConsolidadoPDCAdapter().invokeProcess(request);
        
    }

    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.consultarpagamentosconsolidado.IConsultarPagamentosConsolidadoService#imprimirPagamentosConsolidados(br.com.bradesco.web.pgit.service.business.consultarpagamentosconsolidado.bean.DetalharPagamentosConsolidadosEntradaDTO)
     */
    public DetalharPagamentosConsolidadosSaidaDTO imprimirPagamentosConsolidados(
        DetalharPagamentosConsolidadosEntradaDTO entrada) {
        
        DetalharPagamentosConsolidadosSaidaDTO saida = new DetalharPagamentosConsolidadosSaidaDTO();
        ImprimirPagamentosConsolidadosRequest request = new ImprimirPagamentosConsolidadosRequest();
        ImprimirPagamentosConsolidadosResponse response = null;
        
        request.setNrOcorrencias(NUMERO_OCORRENCIAS_DETALHAR);
        request.setCdPessoaJuridicaContrato(verificaLongNulo(entrada.getCdPessoaJuridicaContrato()));
        request.setCdTipoContratoNegocio(verificaIntegerNulo(entrada.getCdTipoContratoNegocio()));
        request.setNrSequenciaContratoNegocio(verificaLongNulo(entrada.getNrSequenciaContratoNegocio()));
        request.setNrCnpjCpf(verificaLongNulo(entrada.getNrCnpjCpf()));
        request.setNrFilialcnpjCpf(verificaIntegerNulo(entrada.getNrFilialcnpjCpf()));
        request.setNrControleCnpjCpf(verificaIntegerNulo(entrada.getNrControleCnpjCpf()));
        request.setNrArquivoRemessaPagamento(verificaLongNulo(entrada.getNrArquivoRemessaPagamento()));
        request.setDtCreditoPagamento(verificaStringNula(entrada.getDtCreditoPagamento()));
        request.setCdBanco(verificaIntegerNulo(entrada.getCdBanco()));
        request.setCdAgencia(verificaIntegerNulo(entrada.getCdAgencia()));
        request.setCdConta(verificaLongNulo(entrada.getCdConta()));
        request.setCdDigitoConta(verificaStringNula(entrada.getCdDigitoConta()));
        request.setCdProdutoServicoOperacao(verificaIntegerNulo(entrada.getCdProdutoServicoOperacao()));
        request.setCdProdutoServicoRelacionado(verificaIntegerNulo(entrada.getCdProdutoServicoRelacionado()));
        request.setCdSituacaoOperacaoPagamento(verificaIntegerNulo(entrada.getCdSituacaoOperacaoPagamento()));
        request.setCdAgendadoPagaoNaoPago(verificaIntegerNulo(entrada.getCdAgendadoPagaoNaoPago()));
        request.setCdPessoaContratoDebito(verificaLongNulo(entrada.getCdPessoaContratoDebito()));
        request.setCdTipoContratoDebito(verificaIntegerNulo(entrada.getCdTipoContratoDebito()));
        request.setNrSequenciaContratoDebito(verificaLongNulo(entrada.getNrSequenciaContratoDebito()));
        request.setCdTituloPgtoRastreado(verificaIntegerNulo(entrada.getCdTituloPgtoRastreado()));
        request.setCdIndicadorAutorizacaoPagador(verificaIntegerNulo(entrada.getCdIndicadorAutorizacaoPagador()));
        request.setNrLoteInternoPagamento(verificaLongNulo(entrada.getNrLoteInternoPagamento()));

        response = getFactoryAdapter().getImprimirPagamentosConsolidadosPDCAdapter().invokeProcess(request);
        
        saida.setCodMensagem(response.getCodMensagem());
        saida.setMensagem(response.getMensagem());
        saida.setDsBancoDebito(response.getDsBancoDebito());
        saida.setDsAgenciaDebito(response.getDsAgenciaDebito());
        saida.setDsTipoContaDebito(response.getDsTipoContaDebito());
        saida.setDsContrato(response.getDsContrato());
        saida.setDsSituacaoContrato(response.getDsSituacaoContrato());
        saida.setNomeCliente(response.getNmCliente());
        saida.setNumeroConsultas(response.getNumeroConsultas());

        List<OcorrenciasDetalharPagamentosConsolidados> listaOcorrecnias =
            new ArrayList<OcorrenciasDetalharPagamentosConsolidados>();
        for (int i = 0; i < response.getOcorrenciasCount(); i++) {
            OcorrenciasDetalharPagamentosConsolidados ocorrencias = new OcorrenciasDetalharPagamentosConsolidados();
            ocorrencias.setNrPagamento(response.getOcorrencias(i).getNrPagamento());
            ocorrencias.setVlPagamento(response.getOcorrencias(i).getVlPagamento());
            ocorrencias.setCdCnpjCpfFavorecido(response.getOcorrencias(i).getCdCnpjCpfFavorecido());
            ocorrencias.setCdFilialCnpjCpfFavorecido(response.getOcorrencias(i).getCdFilialCnpjCpfFavorecido());
            ocorrencias.setCdControleCnpjCpfFavorecido(response.getOcorrencias(i).getCdControleCnpjCpfFavorecido());
            ocorrencias.setCdFavorecido(response.getOcorrencias(i).getCdFavorecido());
            ocorrencias.setDsBeneficio(response.getOcorrencias(i).getDsBeneficio());
            ocorrencias.setCdBanco(response.getOcorrencias(i).getCdBanco());
            ocorrencias.setCdAgencia(response.getOcorrencias(i).getCdAgencia());
            ocorrencias.setCdDigitoAgencia(response.getOcorrencias(i).getCdDigitoAgencia());
            ocorrencias.setCdConta(response.getOcorrencias(i).getCdConta());
            ocorrencias.setCdDigitoConta(response.getOcorrencias(i).getCdDigitoConta());
            ocorrencias.setCdTipoCanal(response.getOcorrencias(i).getCdTipoCanal());
            ocorrencias.setCdTipoTela(response.getOcorrencias(i).getCdTipoTela());
            ocorrencias.setDsEfetivacaoPagamento(response.getOcorrencias(i).getDsEfetivacaoPagamento());

            // Formatando campos
            ocorrencias.setFavorecidoBeneficiarioFormatado(response.getOcorrencias(i).getDsBeneficio());
            ocorrencias.setBancoFormatado(formatBanco(ocorrencias.getCdBanco(), false));
            ocorrencias.setAgenciaFormatada(formatAgencia(ocorrencias.getCdAgencia(), ocorrencias.getCdDigitoAgencia(),
                false));
            ocorrencias.setContaFormatada(formatConta(ocorrencias.getCdConta(), ocorrencias.getCdDigitoConta(), false));
           
            ocorrencias.setVlPagamentoFormatado(format(ocorrencias.getVlPagamento(), "#,##0.00"));
            
            ocorrencias.setDsIndicadorAutorizacao(response.getOcorrencias(i).getDsIndicadorAutorizacao());
            ocorrencias.setCdLoteInterno(response.getOcorrencias(i).getCdLoteInterno());
            ocorrencias.setDsTipoLayout(response.getOcorrencias(i).getDsTipoLayout());
            ocorrencias.setDsIndicadorPagamento(response.getOcorrencias(i).getDsIndicadorPagamento());
            ocorrencias.setCdIspbPagtoDestino(response.getOcorrencias(i).getCdIspbPagtoDestino());
            ocorrencias.setContaPagtoDestino(response.getOcorrencias(i).getContaPagtoDestino());
            
            String cdContaPagamentoDestino = response.getOcorrencias(i).getContaPagtoDestino();
            if(cdContaPagamentoDestino != null && !"".equals(cdContaPagamentoDestino) && !"0".equals(cdContaPagamentoDestino)){
            	ocorrencias.setBancoAgenciaContaCreditoFormatado(PgitUtil.formatBancoIspbConta(
            			response.getOcorrencias(i).getCdBanco(), 
						response.getOcorrencias(i).getCdIspbPagtoDestino(), cdContaPagamentoDestino, true));
			}else{
				 ocorrencias.setBancoAgenciaContaCreditoFormatado(formatBancoAgenciaConta(response.getOcorrencias(i)
			                .getCdBanco(), response.getOcorrencias(i).getCdAgencia(), response.getOcorrencias(i)
			                .getCdDigitoAgencia(), response.getOcorrencias(i).getCdConta(), response.getOcorrencias(i)
			                .getCdDigitoConta(), true));	
			}
            

            listaOcorrecnias.add(ocorrencias);
        }
        saida.setListaOcorrecnias(listaOcorrecnias);

        return saida;
        
    }
}