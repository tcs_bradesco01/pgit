/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantercadastroterceiroparticipante.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantercadastroterceiroparticipante.bean;

/**
 * Nome: OcorrenciasListarTercParticipanteContratoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class OcorrenciasListarTercParticipanteContratoSaidaDTO{
	
	/** Atributo cdCpfCnpjTerceiro. */
	private Long cdCpfCnpjTerceiro;
	
	/** Atributo cdFilialCnpjTerceiro. */
	private Integer cdFilialCnpjTerceiro;
	
	/** Atributo cdControleCnpjTerceiro. */
	private Integer cdControleCnpjTerceiro;
	
	/** Atributo nrRastreaTerceiro. */
	private Integer nrRastreaTerceiro;
	
	/** Atributo dtInicioRastreaTerceiro. */
	private String dtInicioRastreaTerceiro;
	
	/** Atributo cpfCnpjFormatado. */
	private String cpfCnpjFormatado;
	//check da grid de consulta
	/** Atributo check. */
	private boolean check = false;

	/**
	 * Get: cdCpfCnpjTerceiro.
	 *
	 * @return cdCpfCnpjTerceiro
	 */
	public Long getCdCpfCnpjTerceiro(){
		return cdCpfCnpjTerceiro;
	}

	/**
	 * Set: cdCpfCnpjTerceiro.
	 *
	 * @param cdCpfCnpjTerceiro the cd cpf cnpj terceiro
	 */
	public void setCdCpfCnpjTerceiro(Long cdCpfCnpjTerceiro){
		this.cdCpfCnpjTerceiro = cdCpfCnpjTerceiro;
	}

	/**
	 * Get: cdFilialCnpjTerceiro.
	 *
	 * @return cdFilialCnpjTerceiro
	 */
	public Integer getCdFilialCnpjTerceiro(){
		return cdFilialCnpjTerceiro;
	}

	/**
	 * Set: cdFilialCnpjTerceiro.
	 *
	 * @param cdFilialCnpjTerceiro the cd filial cnpj terceiro
	 */
	public void setCdFilialCnpjTerceiro(Integer cdFilialCnpjTerceiro){
		this.cdFilialCnpjTerceiro = cdFilialCnpjTerceiro;
	}

	/**
	 * Get: cdControleCnpjTerceiro.
	 *
	 * @return cdControleCnpjTerceiro
	 */
	public Integer getCdControleCnpjTerceiro(){
		return cdControleCnpjTerceiro;
	}

	/**
	 * Set: cdControleCnpjTerceiro.
	 *
	 * @param cdControleCnpjTerceiro the cd controle cnpj terceiro
	 */
	public void setCdControleCnpjTerceiro(Integer cdControleCnpjTerceiro){
		this.cdControleCnpjTerceiro = cdControleCnpjTerceiro;
	}

	/**
	 * Get: nrRastreaTerceiro.
	 *
	 * @return nrRastreaTerceiro
	 */
	public Integer getNrRastreaTerceiro(){
		return nrRastreaTerceiro;
	}

	/**
	 * Set: nrRastreaTerceiro.
	 *
	 * @param nrRastreaTerceiro the nr rastrea terceiro
	 */
	public void setNrRastreaTerceiro(Integer nrRastreaTerceiro){
		this.nrRastreaTerceiro = nrRastreaTerceiro;
	}

	/**
	 * Get: dtInicioRastreaTerceiro.
	 *
	 * @return dtInicioRastreaTerceiro
	 */
	public String getDtInicioRastreaTerceiro(){
		return dtInicioRastreaTerceiro;
	}

	/**
	 * Is check.
	 *
	 * @return true, if is check
	 */
	public boolean isCheck() {
	    return check;
	}

	/**
	 * Set: check.
	 *
	 * @param check the check
	 */
	public void setCheck(boolean check) {
	    this.check = check;
	}

	/**
	 * Set: dtInicioRastreaTerceiro.
	 *
	 * @param dtInicioRastreaTerceiro the dt inicio rastrea terceiro
	 */
	public void setDtInicioRastreaTerceiro(String dtInicioRastreaTerceiro){
		this.dtInicioRastreaTerceiro = dtInicioRastreaTerceiro;
	}

	/**
	 * Get: cpfCnpjFormatado.
	 *
	 * @return cpfCnpjFormatado
	 */
	public String getCpfCnpjFormatado() {
	    return cpfCnpjFormatado;
	}

	/**
	 * Set: cpfCnpjFormatado.
	 *
	 * @param cpfCnpjFormatado the cpf cnpj formatado
	 */
	public void setCpfCnpjFormatado(String cpfCnpjFormatado) {
	    this.cpfCnpjFormatado = cpfCnpjFormatado;
	}
}