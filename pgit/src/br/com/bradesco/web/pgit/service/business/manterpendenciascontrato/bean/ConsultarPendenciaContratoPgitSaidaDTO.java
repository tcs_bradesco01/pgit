/*
 * Nome: br.com.bradesco.web.pgit.service.business.manterpendenciascontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.manterpendenciascontrato.bean;

import br.com.bradesco.web.pgit.utils.PgitUtil;

/**
 * Nome: ConsultarPendenciaContratoPgitSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarPendenciaContratoPgitSaidaDTO {
	
	/** Atributo codMensagem. */
	private String codMensagem;
	
	/** Atributo mensagem. */
	private String mensagem;
	
	/** Atributo dsPendenciaPagIntegrado. */
	private String dsPendenciaPagIntegrado;
	
	/** Atributo dsUnidadeOrg. */
	private String dsUnidadeOrg;
	
	/** Atributo dsTipoUnidadeOrg. */
	private String dsTipoUnidadeOrg;
	
	/** Atributo codPessoaUnidadeOrg. */
	private int codPessoaUnidadeOrg;
	
	/** Atributo codDigitoUnidadeOrganizacinal. */
	private String codDigitoUnidadeOrganizacinal;
	
	/** Atributo dsTipoBaixa. */
	private String dsTipoBaixa;
	
	/** Atributo dsSituacaoPendenciaContrato. */
	private String dsSituacaoPendenciaContrato;
	
	/** Atributo dataAtendimentoPendenciaContrato. */
	private String dataAtendimentoPendenciaContrato;
	
	/** Atributo horaBaixaPendenciaContrato. */
	private String horaBaixaPendenciaContrato;
	
	/** Atributo codUsuarioInclusao. */
	private String codUsuarioInclusao;
	
	/** Atributo codUsuarioInclusaoExterno. */
	private String codUsuarioInclusaoExterno;
	
	/** Atributo horaInclusaoRegistro. */
	private String horaInclusaoRegistro;
	
	/** Atributo codOperCanalInclusao. */
	private String codOperCanalInclusao;
	
	/** Atributo codTipoCanalInclusao. */
	private int codTipoCanalInclusao;
	
	/** Atributo dsCanalInclusao. */
	private String dsCanalInclusao;
	
	/** Atributo codUsuarioManutencao. */
	private String codUsuarioManutencao;
	
	/** Atributo codUsuarioManutencaoExterno. */
	private String codUsuarioManutencaoExterno;
	
	/** Atributo horarioManutencaoRegistro. */
	private String horarioManutencaoRegistro;
	
	/** Atributo codOperacaoCanalManutencao. */
	private String codOperacaoCanalManutencao;
	
	/** Atributo codTipoCanalManutencao. */
	private int codTipoCanalManutencao;
	
	/** Atributo dsCanalManutencao. */
	private String dsCanalManutencao;
	
	/** Atributo codMotivoSituacaoPendencia. */
	private String codMotivoSituacaoPendencia;
	
	

	/**
	 * Get: codDigitoUnidadeOrganizacinal.
	 *
	 * @return codDigitoUnidadeOrganizacinal
	 */
	public String getCodDigitoUnidadeOrganizacinal() {
		return codDigitoUnidadeOrganizacinal;
	}
	
	/**
	 * Set: codDigitoUnidadeOrganizacinal.
	 *
	 * @param codDigitoUnidadeOrganizacinal the cod digito unidade organizacinal
	 */
	public void setCodDigitoUnidadeOrganizacinal(
			String codDigitoUnidadeOrganizacinal) {
		this.codDigitoUnidadeOrganizacinal = codDigitoUnidadeOrganizacinal;
	}
	
	/**
	 * Get: codOperacaoCanalManutencao.
	 *
	 * @return codOperacaoCanalManutencao
	 */
	public String getCodOperacaoCanalManutencao() {
		return codOperacaoCanalManutencao;
	}
	
	/**
	 * Set: codOperacaoCanalManutencao.
	 *
	 * @param codOperacaoCanalManutencao the cod operacao canal manutencao
	 */
	public void setCodOperacaoCanalManutencao(String codOperacaoCanalManutencao) {
		this.codOperacaoCanalManutencao = codOperacaoCanalManutencao;
	}
	
	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}
	
	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	/**
	 * Get: codOperCanalInclusao.
	 *
	 * @return codOperCanalInclusao
	 */
	public String getCodOperCanalInclusao() {
		return codOperCanalInclusao;
	}
	
	/**
	 * Set: codOperCanalInclusao.
	 *
	 * @param codOperCanalInclusao the cod oper canal inclusao
	 */
	public void setCodOperCanalInclusao(String codOperCanalInclusao) {
		this.codOperCanalInclusao = codOperCanalInclusao;
	}
	
	/**
	 * Get: codPessoaUnidadeOrg.
	 *
	 * @return codPessoaUnidadeOrg
	 */
	public int getCodPessoaUnidadeOrg() {
		return codPessoaUnidadeOrg;
	}
	
	/**
	 * Set: codPessoaUnidadeOrg.
	 *
	 * @param codPessoaUnidadeOrg the cod pessoa unidade org
	 */
	public void setCodPessoaUnidadeOrg(int codPessoaUnidadeOrg) {
		this.codPessoaUnidadeOrg = codPessoaUnidadeOrg;
	}
	
	/**
	 * Get: codTipoCanalInclusao.
	 *
	 * @return codTipoCanalInclusao
	 */
	public int getCodTipoCanalInclusao() {
		return codTipoCanalInclusao;
	}
	
	/**
	 * Set: codTipoCanalInclusao.
	 *
	 * @param codTipoCanalInclusao the cod tipo canal inclusao
	 */
	public void setCodTipoCanalInclusao(int codTipoCanalInclusao) {
		this.codTipoCanalInclusao = codTipoCanalInclusao;
	}
	
	/**
	 * Get: codTipoCanalManutencao.
	 *
	 * @return codTipoCanalManutencao
	 */
	public int getCodTipoCanalManutencao() {
		return codTipoCanalManutencao;
	}
	
	/**
	 * Set: codTipoCanalManutencao.
	 *
	 * @param codTipoCanalManutencao the cod tipo canal manutencao
	 */
	public void setCodTipoCanalManutencao(int codTipoCanalManutencao) {
		this.codTipoCanalManutencao = codTipoCanalManutencao;
	}

	/**
	 * Get: codUsuarioInclusao.
	 *
	 * @return codUsuarioInclusao
	 */
	public String getCodUsuarioInclusao() {
		return codUsuarioInclusao;
	}
	
	/**
	 * Set: codUsuarioInclusao.
	 *
	 * @param codUsuarioInclusao the cod usuario inclusao
	 */
	public void setCodUsuarioInclusao(String codUsuarioInclusao) {
		this.codUsuarioInclusao = codUsuarioInclusao;
	}
	
	/**
	 * Get: codUsuarioInclusaoExterno.
	 *
	 * @return codUsuarioInclusaoExterno
	 */
	public String getCodUsuarioInclusaoExterno() {
		return codUsuarioInclusaoExterno;
	}
	
	/**
	 * Set: codUsuarioInclusaoExterno.
	 *
	 * @param codUsuarioInclusaoExterno the cod usuario inclusao externo
	 */
	public void setCodUsuarioInclusaoExterno(String codUsuarioInclusaoExterno) {
		this.codUsuarioInclusaoExterno = codUsuarioInclusaoExterno;
	}

	/**
	 * Get: codUsuarioManutencao.
	 *
	 * @return codUsuarioManutencao
	 */
	public String getCodUsuarioManutencao() {
		return codUsuarioManutencao;
	}
	
	/**
	 * Set: codUsuarioManutencao.
	 *
	 * @param codUsuarioManutencao the cod usuario manutencao
	 */
	public void setCodUsuarioManutencao(String codUsuarioManutencao) {
		this.codUsuarioManutencao = codUsuarioManutencao;
	}
	
	/**
	 * Get: codUsuarioManutencaoExterno.
	 *
	 * @return codUsuarioManutencaoExterno
	 */
	public String getCodUsuarioManutencaoExterno() {
		return codUsuarioManutencaoExterno;
	}
	
	/**
	 * Set: codUsuarioManutencaoExterno.
	 *
	 * @param codUsuarioManutencaoExterno the cod usuario manutencao externo
	 */
	public void setCodUsuarioManutencaoExterno(String codUsuarioManutencaoExterno) {
		this.codUsuarioManutencaoExterno = codUsuarioManutencaoExterno;
	}
	
	/**
	 * Get: dataAtendimentoPendenciaContrato.
	 *
	 * @return dataAtendimentoPendenciaContrato
	 */
	public String getDataAtendimentoPendenciaContrato() {
		return dataAtendimentoPendenciaContrato;
	}
	
	/**
	 * Set: dataAtendimentoPendenciaContrato.
	 *
	 * @param dataAtendimentoPendenciaContrato the data atendimento pendencia contrato
	 */
	public void setDataAtendimentoPendenciaContrato(
			String dataAtendimentoPendenciaContrato) {
		this.dataAtendimentoPendenciaContrato = dataAtendimentoPendenciaContrato;
	}
	
	/**
	 * Get: dsCanalInclusao.
	 *
	 * @return dsCanalInclusao
	 */
	public String getDsCanalInclusao() {
		return dsCanalInclusao;
	}
	
	/**
	 * Set: dsCanalInclusao.
	 *
	 * @param dsCanalInclusao the ds canal inclusao
	 */
	public void setDsCanalInclusao(String dsCanalInclusao) {
		this.dsCanalInclusao = dsCanalInclusao;
	}
	
	/**
	 * Get: dsCanalManutencao.
	 *
	 * @return dsCanalManutencao
	 */
	public String getDsCanalManutencao() {
		return dsCanalManutencao;
	}
	
	/**
	 * Set: dsCanalManutencao.
	 *
	 * @param dsCanalManutencao the ds canal manutencao
	 */
	public void setDsCanalManutencao(String dsCanalManutencao) {
		this.dsCanalManutencao = dsCanalManutencao;
	}
	
	/**
	 * Get: dsPendenciaPagIntegrado.
	 *
	 * @return dsPendenciaPagIntegrado
	 */
	public String getDsPendenciaPagIntegrado() {
		return dsPendenciaPagIntegrado;
	}
	
	/**
	 * Set: dsPendenciaPagIntegrado.
	 *
	 * @param dsPendenciaPagIntegrado the ds pendencia pag integrado
	 */
	public void setDsPendenciaPagIntegrado(String dsPendenciaPagIntegrado) {
		this.dsPendenciaPagIntegrado = dsPendenciaPagIntegrado;
	}
	
	/**
	 * Get: dsSituacaoPendenciaContrato.
	 *
	 * @return dsSituacaoPendenciaContrato
	 */
	public String getDsSituacaoPendenciaContrato() {
		return dsSituacaoPendenciaContrato;
	}
	
	/**
	 * Set: dsSituacaoPendenciaContrato.
	 *
	 * @param dsSituacaoPendenciaContrato the ds situacao pendencia contrato
	 */
	public void setDsSituacaoPendenciaContrato(String dsSituacaoPendenciaContrato) {
		this.dsSituacaoPendenciaContrato = dsSituacaoPendenciaContrato;
	}
	
	/**
	 * Get: dsTipoBaixa.
	 *
	 * @return dsTipoBaixa
	 */
	public String getDsTipoBaixa() {
		return dsTipoBaixa;
	}
	
	/**
	 * Set: dsTipoBaixa.
	 *
	 * @param dsTipoBaixa the ds tipo baixa
	 */
	public void setDsTipoBaixa(String dsTipoBaixa) {
		this.dsTipoBaixa = dsTipoBaixa;
	}
	
	/**
	 * Get: dsTipoUnidadeOrg.
	 *
	 * @return dsTipoUnidadeOrg
	 */
	public String getDsTipoUnidadeOrg() {
		return dsTipoUnidadeOrg;
	}
	
	/**
	 * Set: dsTipoUnidadeOrg.
	 *
	 * @param dsTipoUnidadeOrg the ds tipo unidade org
	 */
	public void setDsTipoUnidadeOrg(String dsTipoUnidadeOrg) {
		this.dsTipoUnidadeOrg = dsTipoUnidadeOrg;
	}
	
	/**
	 * Get: dsUnidadeOrg.
	 *
	 * @return dsUnidadeOrg
	 */
	public String getDsUnidadeOrg() {
		return dsUnidadeOrg;
	}
	
	/**
	 * Set: dsUnidadeOrg.
	 *
	 * @param dsUnidadeOrg the ds unidade org
	 */
	public void setDsUnidadeOrg(String dsUnidadeOrg) {
		this.dsUnidadeOrg = dsUnidadeOrg;
	}

	/**
	 * Get: horaBaixaPendenciaContrato.
	 *
	 * @return horaBaixaPendenciaContrato
	 */
	public String getHoraBaixaPendenciaContrato() {
		return horaBaixaPendenciaContrato;
	}
	
	/**
	 * Set: horaBaixaPendenciaContrato.
	 *
	 * @param horaBaixaPendenciaContrato the hora baixa pendencia contrato
	 */
	public void setHoraBaixaPendenciaContrato(String horaBaixaPendenciaContrato) {
		this.horaBaixaPendenciaContrato = horaBaixaPendenciaContrato;
	}
	
	/**
	 * Get: horaInclusaoRegistro.
	 *
	 * @return horaInclusaoRegistro
	 */
	public String getHoraInclusaoRegistro() {
		return horaInclusaoRegistro;
	}
	
	/**
	 * Set: horaInclusaoRegistro.
	 *
	 * @param horaInclusaoRegistro the hora inclusao registro
	 */
	public void setHoraInclusaoRegistro(String horaInclusaoRegistro) {
		this.horaInclusaoRegistro = horaInclusaoRegistro;
	}
	
	/**
	 * Get: horarioManutencaoRegistro.
	 *
	 * @return horarioManutencaoRegistro
	 */
	public String getHorarioManutencaoRegistro() {
		return horarioManutencaoRegistro;
	}
	
	/**
	 * Set: horarioManutencaoRegistro.
	 *
	 * @param horarioManutencaoRegistro the horario manutencao registro
	 */
	public void setHorarioManutencaoRegistro(String horarioManutencaoRegistro) {
		this.horarioManutencaoRegistro = horarioManutencaoRegistro;
	}
	
	/**
	 * Get: codMotivoSituacaoPendencia.
	 *
	 * @return codMotivoSituacaoPendencia
	 */
	public String getCodMotivoSituacaoPendencia() {
		return codMotivoSituacaoPendencia;
	}
	
	/**
	 * Set: codMotivoSituacaoPendencia.
	 *
	 * @param codMotivoSituacaoPendencia the cod motivo situacao pendencia
	 */
	public void setCodMotivoSituacaoPendencia(String codMotivoSituacaoPendencia) {
		this.codMotivoSituacaoPendencia = codMotivoSituacaoPendencia;
	}

	/**
	 * Get: complementoInclusaoFormatado.
	 *
	 * @return complementoInclusaoFormatado
	 */
	public String getComplementoInclusaoFormatado() {
		return codOperCanalInclusao;
	}

	/**
	 * Get: complementoManutencaoFormatado.
	 *
	 * @return complementoManutencaoFormatado
	 */
	public String getComplementoManutencaoFormatado() {
		return codOperacaoCanalManutencao;
	}
}