/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/implementation.ftl,v $
 * $Id: implementation.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: implementation.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */
 
package br.com.bradesco.web.pgit.service.business.desautorizarpagamentosindividuais.impl;

import java.util.ArrayList;
import java.util.List;

import br.com.bradesco.web.pgit.service.business.desautorizarpagamentosindividuais.IDesautorizarPagamentosIndividuaisService;
import br.com.bradesco.web.pgit.service.business.desautorizarpagamentosindividuais.IDesautorizarPagamentosIndividuaisServiceConstants;
import br.com.bradesco.web.pgit.service.business.desautorizarpagamentosindividuais.bean.ConsultarPagtosIndAutorizadosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.desautorizarpagamentosindividuais.bean.ConsultarPagtosIndAutorizadosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.desautorizarpagamentosindividuais.bean.DesautorizarPagtosIndividuaisEntradaDTO;
import br.com.bradesco.web.pgit.service.business.desautorizarpagamentosindividuais.bean.DesautorizarPagtosIndividuaisSaidaDTO;
import br.com.bradesco.web.pgit.service.data.pdc.FactoryAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarpagtosindautorizados.request.ConsultarPagtosIndAutorizadosRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarpagtosindautorizados.response.ConsultarPagtosIndAutorizadosResponse;
import br.com.bradesco.web.pgit.service.data.pdc.desautorizarpagtosindividuais.request.DesautorizarPagtosIndividuaisRequest;
import br.com.bradesco.web.pgit.service.data.pdc.desautorizarpagtosindividuais.response.DesautorizarPagtosIndividuaisResponse;
import br.com.bradesco.web.pgit.utils.CpfCnpjUtils;
import br.com.bradesco.web.pgit.utils.DateUtils;
import br.com.bradesco.web.pgit.utils.PgitUtil;


/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Implementa��o do adaptador: DesautorizarPagamentosIndividuais
 * </p>
 * 
 * @comment C�DIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public class DesautorizarPagamentosIndividuaisServiceImpl implements IDesautorizarPagamentosIndividuaisService {

	/** Atributo factoryAdapter. */
	private FactoryAdapter factoryAdapter;	
	
	/**
	 * Get: factoryAdapter.
	 *
	 * @return factoryAdapter
	 */
	public FactoryAdapter getFactoryAdapter() {
		return factoryAdapter;
	}

	/**
	 * Set: factoryAdapter.
	 *
	 * @param factoryAdapter the factory adapter
	 */
	public void setFactoryAdapter(FactoryAdapter factoryAdapter) {
		this.factoryAdapter = factoryAdapter;
	}
	
    /**
     * Construtor.
     */
    public DesautorizarPagamentosIndividuaisServiceImpl() {
    }
    
    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.desautorizarpagamentosindividuais.IDesautorizarPagamentosIndividuaisService#consultarPagtosIndAutorizados(br.com.bradesco.web.pgit.service.business.desautorizarpagamentosindividuais.bean.ConsultarPagtosIndAutorizadosEntradaDTO)
     */
    public List<ConsultarPagtosIndAutorizadosSaidaDTO> consultarPagtosIndAutorizados (ConsultarPagtosIndAutorizadosEntradaDTO entradaDTO){
    	List<ConsultarPagtosIndAutorizadosSaidaDTO> listaRetorno = new ArrayList<ConsultarPagtosIndAutorizadosSaidaDTO>();
    	ConsultarPagtosIndAutorizadosRequest request = new ConsultarPagtosIndAutorizadosRequest();
    	ConsultarPagtosIndAutorizadosResponse response = new ConsultarPagtosIndAutorizadosResponse();
    	
        request.setCdTipoPesquisa(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoPesquisa()));
        request.setCdAgendadosPagosNaoPagos(PgitUtil.verificaIntegerNulo(entradaDTO.getCdAgendadosPagosNaoPagos()));
        request.setNrOcorrencias(IDesautorizarPagamentosIndividuaisServiceConstants.NUMERO_OCORRENCIAS_CONSULTAR);
        request.setCdPessoaJuridicaContrato(PgitUtil.verificaLongNulo(entradaDTO.getCdPessoaJuridicaContrato()));
        request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoContratoNegocio()));
        request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(entradaDTO.getNrSequenciaContratoNegocio()));
        request.setCdBanco(PgitUtil.verificaIntegerNulo(entradaDTO.getCdBanco()));
        request.setCdAgencia(PgitUtil.verificaIntegerNulo(entradaDTO.getCdAgencia()));
        request.setCdConta(PgitUtil.verificaLongNulo(entradaDTO.getCdConta()));
        request.setCdDigitoConta(PgitUtil.DIGITO_CONTA);
        request.setDtCreditoPagamentoInicio(PgitUtil.verificaStringNula(entradaDTO.getDtCreditoPagamentoInicio()));
        request.setDtCreditoPagamentoFim(PgitUtil.verificaStringNula(entradaDTO.getDtCreditoPagamentoFim()));
        request.setNrArquivoRemssaPagamento(PgitUtil.verificaLongNulo(entradaDTO.getNrArquivoRemessaPagamento()));
        request.setCdParticipante(PgitUtil.verificaLongNulo(entradaDTO.getCdParticipante()));
        request.setCdProdutoServicoOperacao(PgitUtil.verificaIntegerNulo(entradaDTO.getCdProdutoServicoOperacao()));
        request.setCdProdutoServicoRelacionado(PgitUtil.verificaIntegerNulo(entradaDTO.getCdProdutoServicoRelacionado()));        
        request.setCdControlePagamentoDe(PgitUtil.verificaStringNula(entradaDTO.getCdControlePagamentoDe()));
        request.setCdControlePagamentoAte(PgitUtil.verificaStringNula(entradaDTO.getCdContolePagamentoAte()));        
        request.setVlPagamentoAte(PgitUtil.verificaBigDecimalNulo(entradaDTO.getVlPagamentoAte()));
        request.setVlPagamentoDe(PgitUtil.verificaBigDecimalNulo(entradaDTO.getVlPagamentoDe()));
        request.setCdSituacaoOperacaoPagamento(PgitUtil.verificaIntegerNulo(entradaDTO.getCdSituacaoOperacaoPagamento()));
        request.setCdMotivoSituacaoPagamento(PgitUtil.verificaIntegerNulo(entradaDTO.getCdMotivoSituacaoPagamento()));
        request.setCdBarraDocumento(PgitUtil.verificaStringNula(entradaDTO.getCdBarraDocumento()));
        request.setCdFavorecidoClientePagador(PgitUtil.verificaLongNulo(entradaDTO.getCdFavorecidoClientePagador()));
        request.setCdInscricaoFavorecido(PgitUtil.verificaLongNulo(entradaDTO.getCdInscricaoFavorecido()));
        request.setCdIdentificacaoInscricaoFavorecido(PgitUtil.verificaIntegerNulo(entradaDTO.getCdIdentificacaoInscricaoFavorecido()));
        request.setCdBancoBeneficiario(PgitUtil.verificaIntegerNulo(entradaDTO.getCdBancoBeneficiario()));
        request.setCdAgenciaBeneficiario(PgitUtil.verificaIntegerNulo(entradaDTO.getCdAgenciaBeneficiario()));
        request.setCdContaBeneficiario(PgitUtil.verificaLongNulo(entradaDTO.getCdContaBeneficiario()));
        request.setCdDigitoContaBeneficiario(PgitUtil.DIGITO_CONTA);
        request.setCdEmpresaContrato(PgitUtil.verificaLongNulo(entradaDTO.getCdEmpresaContrato()));
        request.setCdTipoConta(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoConta()));
        request.setNrContrato(PgitUtil.verificaLongNulo(entradaDTO.getNrContrato()));
        request.setNrUnidadeOrganizacional(PgitUtil.verificaIntegerNulo(entradaDTO.getNrUnidadeOrganizacional()));
        request.setCdBeneficio(PgitUtil.verificaLongNulo(entradaDTO.getCdBeneficio()));
        request.setCdEspecieBeneficioInss(PgitUtil.verificaIntegerNulo(entradaDTO.getCdEspecieBeneficioInss()));
        request.setCdClassificacao(PgitUtil.verificaIntegerNulo(entradaDTO.getCdClassificacao()));
        request.setCdCpfCnpjParticipante(PgitUtil.verificaLongNulo(entradaDTO.getCdCpfCnpjParticipante()));
        request.setCdFilialCnpjParticipante(PgitUtil.verificaIntegerNulo(entradaDTO.getCdFilialCnpjParticipante()));
        request.setCdControleCnpjParticipante(PgitUtil.verificaIntegerNulo(entradaDTO.getCdControleCnpjParticipante()));
        request.setCdPessoaContratoDebito(PgitUtil.verificaLongNulo(entradaDTO.getCdPessoaContratoDebito()));
        request.setCdTipoContratoDebito(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoContratoDebito()));
        request.setNrSequenciaContratoDebito(PgitUtil.verificaLongNulo(entradaDTO.getNrSequenciaContratoDebito()));
        request.setCdPessoaContratoCredt(PgitUtil.verificaLongNulo(entradaDTO.getCdPessoaContratoCredt()));
        request.setCdTipoContratoCredt(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoContratoCredt()));
        request.setNrSequenciaContratoCredt(PgitUtil.verificaLongNulo(entradaDTO.getNrSequenciaContratoCredt()));
        request.setCdIndiceSimulaPagamento(PgitUtil.verificaIntegerNulo(entradaDTO.getCdIndiceSimulaPagamento()));
        request.setCdOrigemRecebidoEfetivacao(PgitUtil.verificaIntegerNulo(entradaDTO.getCdOrigemRecebidoEfetivacao()));
        request.setCdTituloPgtoRastreado(0);
        request.setCdBancoSalarial(PgitUtil.verificaIntegerNulo(entradaDTO.getCdBancoSalarial()));
        request.setCdAgencaSalarial(PgitUtil.verificaIntegerNulo(entradaDTO.getCdAgencaSalarial()));
        request.setCdContaSalarial(PgitUtil.verificaLongNulo(entradaDTO.getCdContaSalarial()));
		if("".equals(PgitUtil.verificaStringNula(entradaDTO.getCdIspbPagtoDestino()))){
			request.setCdIspbPagtoDestino("");
		}else{
			request.setCdIspbPagtoDestino(PgitUtil.preencherZerosAEsquerda(entradaDTO.getCdIspbPagtoDestino(), 8));
		}
		request.setContaPagtoDestino(PgitUtil.preencherZerosAEsquerda(entradaDTO.getContaPagtoDestino(), 20));

        response = getFactoryAdapter().getConsultarPagtosIndAutorizadosPDCAdapter().invokeProcess(request);
        
        ConsultarPagtosIndAutorizadosSaidaDTO saidaDTO;
        for(int i = 0;i<response.getOcorrenciasCount();i++){
        	saidaDTO = new ConsultarPagtosIndAutorizadosSaidaDTO();
            saidaDTO.setCodMensagem(response.getCodMensagem());
            saidaDTO.setMensagem(response.getMensagem());
            saidaDTO.setNrCnpjCpf(response.getOcorrencias(i).getNrCnpjCpf());
            saidaDTO.setNrFilialCnpjCpf(response.getOcorrencias(i).getNrFilialCnpjCpf());
            saidaDTO.setNrDigitoCnpjCpf(response.getOcorrencias(i).getNrDigitoCnpjCpf());
            saidaDTO.setCdPessoaJuridicaContrato(response.getOcorrencias(i).getCdPessoaJuridicaContrato());
            saidaDTO.setDsRazaoSocial(response.getOcorrencias(i).getDsRazaoSocial());
            saidaDTO.setCdEmpresa(response.getOcorrencias(i).getCdEmpresa());
            saidaDTO.setDsEmpresa(response.getOcorrencias(i).getDsEmpresa());
            saidaDTO.setCdTipoContratoNegocio(response.getOcorrencias(i).getCdTipoContratoNegocio());
            saidaDTO.setNrContrato(response.getOcorrencias(i).getNrContrato());
            saidaDTO.setCdProdutoServicoOperacao(response.getOcorrencias(i).getCdProdutoServicoOperacao());
            saidaDTO.setDsResumoProdutoServico(response.getOcorrencias(i).getDsResumoProdutoServico());
            saidaDTO.setCdProdutoServicoRelacionado(response.getOcorrencias(i).getCdProdutoServicoRelacionado());
            saidaDTO.setDsOperacaoProdutoServico(response.getOcorrencias(i).getDsOperacaoProdutoServico());
            saidaDTO.setCdControlePagamento(response.getOcorrencias(i).getCdControlePagamento());
            saidaDTO.setDtCreditoPagamento(response.getOcorrencias(i).getDtCreditoPagamento());
            saidaDTO.setVlEfetivoPagamento(response.getOcorrencias(i).getVlEfetivoPagamento());
            saidaDTO.setCdInscricaoFavorecido(response.getOcorrencias(i).getCdInscricaoFavorecido() == 0L ? "" : String.valueOf(response.getOcorrencias(i).getCdInscricaoFavorecido()));
            saidaDTO.setDsFavorecido(response.getOcorrencias(i).getDsFavorecido());
            saidaDTO.setCdBancoDebito(response.getOcorrencias(i).getCdBancoDebito());
            saidaDTO.setCdAgenciaDebito(response.getOcorrencias(i).getCdAgenciaDebito());
            saidaDTO.setCdDigitoAgenciaDebito(response.getOcorrencias(i).getCdDigitoAgenciaDebito());
            saidaDTO.setCdContaDebito(response.getOcorrencias(i).getCdContaDebito());
            saidaDTO.setCdDigitoContaDebito(response.getOcorrencias(i).getCdDigitoContaDebito());
            saidaDTO.setCdBancoCredito(response.getOcorrencias(i).getCdBancoCredito());
            saidaDTO.setCdAgenciaCredito(response.getOcorrencias(i).getCdAgenciaCredito());
            saidaDTO.setCdDigitoAgenciaCredito(response.getOcorrencias(i).getCdDigitoAgenciaCredito());
            saidaDTO.setCdContaCredito(response.getOcorrencias(i).getCdContaCredito());
            saidaDTO.setCdDigitoContaCredito(response.getOcorrencias(i).getCdDigitoContaCredito());
            saidaDTO.setCdSituacaoOperacaoPagamento(response.getOcorrencias(i).getCdSituacaoOperacaoPagamento());
            saidaDTO.setDsSituacaoOperacaoPagamento(response.getOcorrencias(i).getDsSituacaoOperacaoPagamento());
            saidaDTO.setCdMotivoSituacaoPagamento(response.getOcorrencias(i).getCdMotivoSituacaoPagamento());
            saidaDTO.setDsMotivoSituacaoPagamento(response.getOcorrencias(i).getDsMotivoSituacaoPagamento());
            saidaDTO.setCdTipoCanal(response.getOcorrencias(i).getCdTipoCanal());
            saidaDTO.setCdTipoTela(response.getOcorrencias(i).getCdTipoTela());
            saidaDTO.setDsEfetivacaoPagamento(response.getOcorrencias(i).getDsEfetivacaoPagamento());
            
			saidaDTO.setCpfCnpjFormatado(CpfCnpjUtils.formatCpfCnpjCompleto(response.getOcorrencias(i).getNrCnpjCpf(), response.getOcorrencias(i).getNrFilialCnpjCpf(), response.getOcorrencias(i).getNrDigitoCnpjCpf()));
			saidaDTO.setFavorecidoBeneficiarioFormatado(response.getOcorrencias(i).getDsFavorecido());
			saidaDTO.setBancoAgenciaContaDebitoFormatado(PgitUtil.formatBancoAgenciaConta(response.getOcorrencias(i).getCdBancoDebito(), response.getOcorrencias(i).getCdAgenciaDebito(), response.getOcorrencias(i).getCdDigitoAgenciaDebito(), response.getOcorrencias(i).getCdContaDebito(), response.getOcorrencias(i).getCdDigitoContaDebito(), true));
	        saidaDTO.setBancoAgenciaContaCreditoFormatado(PgitUtil.formatBancoAgenciaConta(response.getOcorrencias(i).getCdBancoCredito(), response.getOcorrencias(i).getCdAgenciaCredito(), response.getOcorrencias(i).getCdDigitoAgenciaCredito(), response.getOcorrencias(i).getCdContaCredito(), response.getOcorrencias(i).getCdDigitoContaCredito(), true));
	        saidaDTO.setDataFormatada(DateUtils.formartarDataPDCparaPadraPtBr(saidaDTO.getDtCreditoPagamento(), "dd.MM.yyyy", "dd/MM/yyyy"));
	        
	        saidaDTO.setContaCreditoFormatada(PgitUtil.formatConta(response.getOcorrencias(i).getCdContaCredito(), response.getOcorrencias(i).getCdDigitoContaCredito(), false));
	        saidaDTO.setCdBancoCreditoFormatado(PgitUtil.formatBanco(response.getOcorrencias(i).getCdBancoCredito(), false));
	        saidaDTO.setAgenciaCreditoFormatada(PgitUtil.formatAgencia(response.getOcorrencias(i).getCdAgenciaCredito(), response.getOcorrencias(i).getCdDigitoAgenciaCredito(), false));
	        saidaDTO.setDsIndicadorPagamento(response.getOcorrencias(i).getDsIndicadorPagamento());
	        saidaDTO.setCdIspbPagtoDestino(response.getOcorrencias(i).getCdIspbPagtoDestino());
	        saidaDTO.setContaPagtoDestino(response.getOcorrencias(i).getContaPagtoDestino());
	        	
            listaRetorno.add(saidaDTO);
        }
    	
    	return listaRetorno;    	
    }
    
    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.desautorizarpagamentosindividuais.IDesautorizarPagamentosIndividuaisService#desautorizarPagtosIndividuais(br.com.bradesco.web.pgit.service.business.desautorizarpagamentosindividuais.bean.DesautorizarPagtosIndividuaisEntradaDTO)
     */
    public DesautorizarPagtosIndividuaisSaidaDTO desautorizarPagtosIndividuais (DesautorizarPagtosIndividuaisEntradaDTO entradaDTO){
    	DesautorizarPagtosIndividuaisSaidaDTO saidaRetorno = new DesautorizarPagtosIndividuaisSaidaDTO();
    	DesautorizarPagtosIndividuaisRequest request = new DesautorizarPagtosIndividuaisRequest();
    	DesautorizarPagtosIndividuaisResponse response = new DesautorizarPagtosIndividuaisResponse();
    	
    	request.setNumeroConsultas(entradaDTO.getLista().size());
    	ArrayList<br.com.bradesco.web.pgit.service.data.pdc.desautorizarpagtosindividuais.request.Ocorrencias> lista = new ArrayList<br.com.bradesco.web.pgit.service.data.pdc.desautorizarpagtosindividuais.request.Ocorrencias>();
    	br.com.bradesco.web.pgit.service.data.pdc.desautorizarpagtosindividuais.request.Ocorrencias ocorrencia;
    	
    	for(int i=0;i<entradaDTO.getLista().size();i++){
    		ocorrencia = new br.com.bradesco.web.pgit.service.data.pdc.desautorizarpagtosindividuais.request.Ocorrencias();
    		ocorrencia.setCdControlePagamento(PgitUtil.verificaStringNula(entradaDTO.getLista().get(i).getCdControlePagamento()));
    		ocorrencia.setCdOperacaoProdutoServico(PgitUtil.verificaIntegerNulo(entradaDTO.getLista().get(i).getCdOperacaoProdutoServico()));
    		ocorrencia.setCdPessoaJuridicaContrato(PgitUtil.verificaLongNulo(entradaDTO.getLista().get(i).getCdPessoaJuridicaContrato()));
    		ocorrencia.setCdProdutoServicoOperacao(PgitUtil.verificaIntegerNulo(entradaDTO.getLista().get(i).getCdProdutoServicoOperacao()));
    		ocorrencia.setCdTipoCanal(PgitUtil.verificaIntegerNulo(entradaDTO.getLista().get(i).getCdTipoCanal()));
    		ocorrencia.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(entradaDTO.getLista().get(i).getCdTipoContratoNegocio()));
    		ocorrencia.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(entradaDTO.getLista().get(i).getNrSequenciaContratoNegocio()));
    		
    		lista.add(ocorrencia);
    	}
    	
    	request.setOcorrencias((br.com.bradesco.web.pgit.service.data.pdc.desautorizarpagtosindividuais.request.Ocorrencias[]) lista.toArray(new br.com.bradesco.web.pgit.service.data.pdc.desautorizarpagtosindividuais.request.Ocorrencias[0]));
    	
    	response = getFactoryAdapter().getDesautorizarPagtosIndividuaisPDCAdapter().invokeProcess(request);
    	
		saidaRetorno.setCodMensagem(response.getCodMensagem());
		saidaRetorno.setMensagem(response.getMensagem());
    	
    	return saidaRetorno;
    }    
}

