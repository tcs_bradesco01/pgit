/*
 * Nome: br.com.bradesco.web.pgit.service.business.autorizarpagamentosconsolidados.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.autorizarpagamentosconsolidados.bean;

import java.util.ArrayList;
import java.util.List;


/**
 * Nome: DetalharPagtosConsPendAutorizacaoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class DetalharPagtosConsPendAutorizacaoSaidaDTO {
	
	/** Atributo codMensagem. */
	private String codMensagem;
	
	/** Atributo mensagem. */
	private String mensagem;
	
	/** Atributo dsBancoDebito. */
	private String dsBancoDebito;
	
	/** Atributo dsAgenciaDebito. */
	private String dsAgenciaDebito;
	
	/** Atributo dsTipoContaDebito. */
	private String dsTipoContaDebito;
	
	/** Atributo dsContrato. */
	private String dsContrato;
	
	/** Atributo dsSituacaoContrato. */
	private String dsSituacaoContrato;
	
	/** Atributo nmCliente. */
	private String nmCliente;
	
	/** Atributo listaDetPagtosConsPendAutorizacao. */
	private List<OcorrenciasDetPagConsPendAutSaidaDTO> listaDetPagtosConsPendAutorizacao = new ArrayList<OcorrenciasDetPagConsPendAutSaidaDTO>();
	
	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}
	
	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}
	
	/**
	 * Get: dsAgenciaDebito.
	 *
	 * @return dsAgenciaDebito
	 */
	public String getDsAgenciaDebito() {
		return dsAgenciaDebito;
	}
	
	/**
	 * Set: dsAgenciaDebito.
	 *
	 * @param dsAgenciaDebito the ds agencia debito
	 */
	public void setDsAgenciaDebito(String dsAgenciaDebito) {
		this.dsAgenciaDebito = dsAgenciaDebito;
	}
	
	/**
	 * Get: dsBancoDebito.
	 *
	 * @return dsBancoDebito
	 */
	public String getDsBancoDebito() {
		return dsBancoDebito;
	}
	
	/**
	 * Set: dsBancoDebito.
	 *
	 * @param dsBancoDebito the ds banco debito
	 */
	public void setDsBancoDebito(String dsBancoDebito) {
		this.dsBancoDebito = dsBancoDebito;
	}
	
	/**
	 * Get: dsContrato.
	 *
	 * @return dsContrato
	 */
	public String getDsContrato() {
		return dsContrato;
	}
	
	/**
	 * Set: dsContrato.
	 *
	 * @param dsContrato the ds contrato
	 */
	public void setDsContrato(String dsContrato) {
		this.dsContrato = dsContrato;
	}
	
	/**
	 * Get: dsSituacaoContrato.
	 *
	 * @return dsSituacaoContrato
	 */
	public String getDsSituacaoContrato() {
		return dsSituacaoContrato;
	}
	
	/**
	 * Set: dsSituacaoContrato.
	 *
	 * @param dsSituacaoContrato the ds situacao contrato
	 */
	public void setDsSituacaoContrato(String dsSituacaoContrato) {
		this.dsSituacaoContrato = dsSituacaoContrato;
	}
	
	/**
	 * Get: dsTipoContaDebito.
	 *
	 * @return dsTipoContaDebito
	 */
	public String getDsTipoContaDebito() {
		return dsTipoContaDebito;
	}
	
	/**
	 * Set: dsTipoContaDebito.
	 *
	 * @param dsTipoContaDebito the ds tipo conta debito
	 */
	public void setDsTipoContaDebito(String dsTipoContaDebito) {
		this.dsTipoContaDebito = dsTipoContaDebito;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	/**
	 * Get: listaDetPagtosConsPendAutorizacao.
	 *
	 * @return listaDetPagtosConsPendAutorizacao
	 */
	public List<OcorrenciasDetPagConsPendAutSaidaDTO> getListaDetPagtosConsPendAutorizacao() {
		return listaDetPagtosConsPendAutorizacao;
	}
	
	/**
	 * Set: listaDetPagtosConsPendAutorizacao.
	 *
	 * @param listaDetPagtosConsPendAutorizacao the lista det pagtos cons pend autorizacao
	 */
	public void setListaDetPagtosConsPendAutorizacao(
			List<OcorrenciasDetPagConsPendAutSaidaDTO> listaDetPagtosConsPendAutorizacao) {
		this.listaDetPagtosConsPendAutorizacao = listaDetPagtosConsPendAutorizacao;
	}
	
	/**
	 * Get: nmCliente.
	 *
	 * @return nmCliente
	 */
	public String getNmCliente() {
		return nmCliente;
	}
	
	/**
	 * Set: nmCliente.
	 *
	 * @param nmCliente the nm cliente
	 */
	public void setNmCliente(String nmCliente) {
		this.nmCliente = nmCliente;
	}
	
	
	
	
	
	
}
