/*
 * Nome: br.com.bradesco.web.pgit.service.business.combo.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.combo.bean;


/**
 * Nome: TipoUnidadeOrganizacionalDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class TipoUnidadeOrganizacionalDTO {
	
	/** Atributo codMensagem. */
	private String codMensagem;
	
	/** Atributo mensagem. */
	private String mensagem;
	
	/** Atributo cdTipoUnidade. */
	private Integer cdTipoUnidade;
	
	/** Atributo dsTipoUnidade. */
	private String dsTipoUnidade;
	
	/** Atributo cdSituacaoVinculacaoConta. */
	private int cdSituacaoVinculacaoConta;
	
	/**
	 * Tipo unidade organizacional dto.
	 */
	public TipoUnidadeOrganizacionalDTO() {
		super();
	}
	
	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}
	
	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	/**
	 * Get: cdSituacaoVinculacaoConta.
	 *
	 * @return cdSituacaoVinculacaoConta
	 */
	public int getCdSituacaoVinculacaoConta() {
		return cdSituacaoVinculacaoConta;
	}

	/**
	 * Set: cdSituacaoVinculacaoConta.
	 *
	 * @param cdSituacaoVinculacaoConta the cd situacao vinculacao conta
	 */
	public void setCdSituacaoVinculacaoConta(int cdSituacaoVinculacaoConta) {
		this.cdSituacaoVinculacaoConta = cdSituacaoVinculacaoConta;
	}

	/**
	 * Get: cdTipoUnidade.
	 *
	 * @return cdTipoUnidade
	 */
	public Integer getCdTipoUnidade() {
		return cdTipoUnidade;
	}

	/**
	 * Set: cdTipoUnidade.
	 *
	 * @param cdTipoUnidade the cd tipo unidade
	 */
	public void setCdTipoUnidade(Integer cdTipoUnidade) {
		this.cdTipoUnidade = cdTipoUnidade;
	}

	/**
	 * Get: dsTipoUnidade.
	 *
	 * @return dsTipoUnidade
	 */
	public String getDsTipoUnidade() {
		return dsTipoUnidade;
	}

	/**
	 * Set: dsTipoUnidade.
	 *
	 * @param dsTipoUnidade the ds tipo unidade
	 */
	public void setDsTipoUnidade(String dsTipoUnidade) {
		this.dsTipoUnidade = dsTipoUnidade;
	}

	/**
	 * (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof TipoUnidadeOrganizacionalDTO)) {
			return false;
		}

		TipoUnidadeOrganizacionalDTO other = (TipoUnidadeOrganizacionalDTO) obj;
		return this.getCdTipoUnidade().equals(other.getCdTipoUnidade());
	}

	/**
	 * (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {		 
		return getCdTipoUnidade().hashCode();
	}
}
