/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listardadosctaconvncontingencia.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.util.Enumeration;
import java.util.Vector;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ListarDadosCtaConvnContingenciaRequest.
 * 
 * @version $Revision$ $Date$
 */
public class ListarDadosCtaConvnContingenciaRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdCpfCnpjRepresentante
     */
    private long _cdCpfCnpjRepresentante = 0;

    /**
     * keeps track of state for field: _cdCpfCnpjRepresentante
     */
    private boolean _has_cdCpfCnpjRepresentante;

    /**
     * Field _cdFilialCnpjRepresentante
     */
    private int _cdFilialCnpjRepresentante = 0;

    /**
     * keeps track of state for field: _cdFilialCnpjRepresentante
     */
    private boolean _has_cdFilialCnpjRepresentante;

    /**
     * Field _cdControleCnpjRepresentante
     */
    private int _cdControleCnpjRepresentante = 0;

    /**
     * keeps track of state for field: _cdControleCnpjRepresentante
     */
    private boolean _has_cdControleCnpjRepresentante;

    /**
     * Field _nrOcorrencias
     */
    private int _nrOcorrencias = 0;

    /**
     * keeps track of state for field: _nrOcorrencias
     */
    private boolean _has_nrOcorrencias;

    /**
     * Field _ocorrenciasList
     */
    private java.util.Vector _ocorrenciasList;


      //----------------/
     //- Constructors -/
    //----------------/

    public ListarDadosCtaConvnContingenciaRequest() 
     {
        super();
        _ocorrenciasList = new Vector();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listardadosctaconvncontingencia.request.ListarDadosCtaConvnContingenciaRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method addOcorrencias
     * 
     * 
     * 
     * @param vOcorrencias
     */
    public void addOcorrencias(br.com.bradesco.web.pgit.service.data.pdc.listardadosctaconvncontingencia.request.Ocorrencias vOcorrencias)
        throws java.lang.IndexOutOfBoundsException
    {
        _ocorrenciasList.addElement(vOcorrencias);
    } //-- void addOcorrencias(br.com.bradesco.web.pgit.service.data.pdc.listardadosctaconvncontingencia.request.Ocorrencias) 

    /**
     * Method addOcorrencias
     * 
     * 
     * 
     * @param index
     * @param vOcorrencias
     */
    public void addOcorrencias(int index, br.com.bradesco.web.pgit.service.data.pdc.listardadosctaconvncontingencia.request.Ocorrencias vOcorrencias)
        throws java.lang.IndexOutOfBoundsException
    {
        _ocorrenciasList.insertElementAt(vOcorrencias, index);
    } //-- void addOcorrencias(int, br.com.bradesco.web.pgit.service.data.pdc.listardadosctaconvncontingencia.request.Ocorrencias) 

    /**
     * Method deleteCdControleCnpjRepresentante
     * 
     */
    public void deleteCdControleCnpjRepresentante()
    {
        this._has_cdControleCnpjRepresentante= false;
    } //-- void deleteCdControleCnpjRepresentante() 

    /**
     * Method deleteCdCpfCnpjRepresentante
     * 
     */
    public void deleteCdCpfCnpjRepresentante()
    {
        this._has_cdCpfCnpjRepresentante= false;
    } //-- void deleteCdCpfCnpjRepresentante() 

    /**
     * Method deleteCdFilialCnpjRepresentante
     * 
     */
    public void deleteCdFilialCnpjRepresentante()
    {
        this._has_cdFilialCnpjRepresentante= false;
    } //-- void deleteCdFilialCnpjRepresentante() 

    /**
     * Method deleteNrOcorrencias
     * 
     */
    public void deleteNrOcorrencias()
    {
        this._has_nrOcorrencias= false;
    } //-- void deleteNrOcorrencias() 

    /**
     * Method enumerateOcorrencias
     * 
     * 
     * 
     * @return Enumeration
     */
    public java.util.Enumeration enumerateOcorrencias()
    {
        return _ocorrenciasList.elements();
    } //-- java.util.Enumeration enumerateOcorrencias() 

    /**
     * Returns the value of field 'cdControleCnpjRepresentante'.
     * 
     * @return int
     * @return the value of field 'cdControleCnpjRepresentante'.
     */
    public int getCdControleCnpjRepresentante()
    {
        return this._cdControleCnpjRepresentante;
    } //-- int getCdControleCnpjRepresentante() 

    /**
     * Returns the value of field 'cdCpfCnpjRepresentante'.
     * 
     * @return long
     * @return the value of field 'cdCpfCnpjRepresentante'.
     */
    public long getCdCpfCnpjRepresentante()
    {
        return this._cdCpfCnpjRepresentante;
    } //-- long getCdCpfCnpjRepresentante() 

    /**
     * Returns the value of field 'cdFilialCnpjRepresentante'.
     * 
     * @return int
     * @return the value of field 'cdFilialCnpjRepresentante'.
     */
    public int getCdFilialCnpjRepresentante()
    {
        return this._cdFilialCnpjRepresentante;
    } //-- int getCdFilialCnpjRepresentante() 

    /**
     * Returns the value of field 'nrOcorrencias'.
     * 
     * @return int
     * @return the value of field 'nrOcorrencias'.
     */
    public int getNrOcorrencias()
    {
        return this._nrOcorrencias;
    } //-- int getNrOcorrencias() 

    /**
     * Method getOcorrencias
     * 
     * 
     * 
     * @param index
     * @return Ocorrencias
     */
    public br.com.bradesco.web.pgit.service.data.pdc.listardadosctaconvncontingencia.request.Ocorrencias getOcorrencias(int index)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index >= _ocorrenciasList.size())) {
            throw new IndexOutOfBoundsException("getOcorrencias: Index value '"+index+"' not in range [0.."+(_ocorrenciasList.size() - 1) + "]");
        }
        
        return (br.com.bradesco.web.pgit.service.data.pdc.listardadosctaconvncontingencia.request.Ocorrencias) _ocorrenciasList.elementAt(index);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listardadosctaconvncontingencia.request.Ocorrencias getOcorrencias(int) 

    /**
     * Method getOcorrencias
     * 
     * 
     * 
     * @return Ocorrencias
     */
    public br.com.bradesco.web.pgit.service.data.pdc.listardadosctaconvncontingencia.request.Ocorrencias[] getOcorrencias()
    {
        int size = _ocorrenciasList.size();
        br.com.bradesco.web.pgit.service.data.pdc.listardadosctaconvncontingencia.request.Ocorrencias[] mArray = new br.com.bradesco.web.pgit.service.data.pdc.listardadosctaconvncontingencia.request.Ocorrencias[size];
        for (int index = 0; index < size; index++) {
            mArray[index] = (br.com.bradesco.web.pgit.service.data.pdc.listardadosctaconvncontingencia.request.Ocorrencias) _ocorrenciasList.elementAt(index);
        }
        return mArray;
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listardadosctaconvncontingencia.request.Ocorrencias[] getOcorrencias() 

    /**
     * Method getOcorrenciasCount
     * 
     * 
     * 
     * @return int
     */
    public int getOcorrenciasCount()
    {
        return _ocorrenciasList.size();
    } //-- int getOcorrenciasCount() 

    /**
     * Method hasCdControleCnpjRepresentante
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdControleCnpjRepresentante()
    {
        return this._has_cdControleCnpjRepresentante;
    } //-- boolean hasCdControleCnpjRepresentante() 

    /**
     * Method hasCdCpfCnpjRepresentante
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCpfCnpjRepresentante()
    {
        return this._has_cdCpfCnpjRepresentante;
    } //-- boolean hasCdCpfCnpjRepresentante() 

    /**
     * Method hasCdFilialCnpjRepresentante
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFilialCnpjRepresentante()
    {
        return this._has_cdFilialCnpjRepresentante;
    } //-- boolean hasCdFilialCnpjRepresentante() 

    /**
     * Method hasNrOcorrencias
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrOcorrencias()
    {
        return this._has_nrOcorrencias;
    } //-- boolean hasNrOcorrencias() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Method removeAllOcorrencias
     * 
     */
    public void removeAllOcorrencias()
    {
        _ocorrenciasList.removeAllElements();
    } //-- void removeAllOcorrencias() 

    /**
     * Method removeOcorrencias
     * 
     * 
     * 
     * @param index
     * @return Ocorrencias
     */
    public br.com.bradesco.web.pgit.service.data.pdc.listardadosctaconvncontingencia.request.Ocorrencias removeOcorrencias(int index)
    {
        java.lang.Object obj = _ocorrenciasList.elementAt(index);
        _ocorrenciasList.removeElementAt(index);
        return (br.com.bradesco.web.pgit.service.data.pdc.listardadosctaconvncontingencia.request.Ocorrencias) obj;
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listardadosctaconvncontingencia.request.Ocorrencias removeOcorrencias(int) 

    /**
     * Sets the value of field 'cdControleCnpjRepresentante'.
     * 
     * @param cdControleCnpjRepresentante the value of field
     * 'cdControleCnpjRepresentante'.
     */
    public void setCdControleCnpjRepresentante(int cdControleCnpjRepresentante)
    {
        this._cdControleCnpjRepresentante = cdControleCnpjRepresentante;
        this._has_cdControleCnpjRepresentante = true;
    } //-- void setCdControleCnpjRepresentante(int) 

    /**
     * Sets the value of field 'cdCpfCnpjRepresentante'.
     * 
     * @param cdCpfCnpjRepresentante the value of field
     * 'cdCpfCnpjRepresentante'.
     */
    public void setCdCpfCnpjRepresentante(long cdCpfCnpjRepresentante)
    {
        this._cdCpfCnpjRepresentante = cdCpfCnpjRepresentante;
        this._has_cdCpfCnpjRepresentante = true;
    } //-- void setCdCpfCnpjRepresentante(long) 

    /**
     * Sets the value of field 'cdFilialCnpjRepresentante'.
     * 
     * @param cdFilialCnpjRepresentante the value of field
     * 'cdFilialCnpjRepresentante'.
     */
    public void setCdFilialCnpjRepresentante(int cdFilialCnpjRepresentante)
    {
        this._cdFilialCnpjRepresentante = cdFilialCnpjRepresentante;
        this._has_cdFilialCnpjRepresentante = true;
    } //-- void setCdFilialCnpjRepresentante(int) 

    /**
     * Sets the value of field 'nrOcorrencias'.
     * 
     * @param nrOcorrencias the value of field 'nrOcorrencias'.
     */
    public void setNrOcorrencias(int nrOcorrencias)
    {
        this._nrOcorrencias = nrOcorrencias;
        this._has_nrOcorrencias = true;
    } //-- void setNrOcorrencias(int) 

    /**
     * Method setOcorrencias
     * 
     * 
     * 
     * @param index
     * @param vOcorrencias
     */
    public void setOcorrencias(int index, br.com.bradesco.web.pgit.service.data.pdc.listardadosctaconvncontingencia.request.Ocorrencias vOcorrencias)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index >= _ocorrenciasList.size())) {
            throw new IndexOutOfBoundsException("setOcorrencias: Index value '"+index+"' not in range [0.." + (_ocorrenciasList.size() - 1) + "]");
        }
        _ocorrenciasList.setElementAt(vOcorrencias, index);
    } //-- void setOcorrencias(int, br.com.bradesco.web.pgit.service.data.pdc.listardadosctaconvncontingencia.request.Ocorrencias) 

    /**
     * Method setOcorrencias
     * 
     * 
     * 
     * @param ocorrenciasArray
     */
    public void setOcorrencias(br.com.bradesco.web.pgit.service.data.pdc.listardadosctaconvncontingencia.request.Ocorrencias[] ocorrenciasArray)
    {
        //-- copy array
        _ocorrenciasList.removeAllElements();
        for (int i = 0; i < ocorrenciasArray.length; i++) {
            _ocorrenciasList.addElement(ocorrenciasArray[i]);
        }
    } //-- void setOcorrencias(br.com.bradesco.web.pgit.service.data.pdc.listardadosctaconvncontingencia.request.Ocorrencias) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ListarDadosCtaConvnContingenciaRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listardadosctaconvncontingencia.request.ListarDadosCtaConvnContingenciaRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listardadosctaconvncontingencia.request.ListarDadosCtaConvnContingenciaRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listardadosctaconvncontingencia.request.ListarDadosCtaConvnContingenciaRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listardadosctaconvncontingencia.request.ListarDadosCtaConvnContingenciaRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
