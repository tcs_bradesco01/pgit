/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/interfaz.ftl,v $
 * $Id: interfaz.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: interfaz.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.histcomplementar;

import java.util.List;

import br.com.bradesco.web.pgit.service.business.histcomplementar.bean.AlterarMantHistoricoComplementarEntradaDTO;
import br.com.bradesco.web.pgit.service.business.histcomplementar.bean.AlterarMantHistoricoComplementarSaidaDTO;
import br.com.bradesco.web.pgit.service.business.histcomplementar.bean.DetalharHistMsgHistoricoComplementarEntradaDTO;
import br.com.bradesco.web.pgit.service.business.histcomplementar.bean.DetalharHistMsgHistoricoComplementarSaidaDTO;
import br.com.bradesco.web.pgit.service.business.histcomplementar.bean.DetalharMantHistoricoComplementarEntradaDTO;
import br.com.bradesco.web.pgit.service.business.histcomplementar.bean.DetalharMantHistoricoComplementarSaidaDTO;
import br.com.bradesco.web.pgit.service.business.histcomplementar.bean.ExcluirMantHistoricoComplementarEntradaDTO;
import br.com.bradesco.web.pgit.service.business.histcomplementar.bean.ExcluirMantHistoricoComplementarSaidaDTO;
import br.com.bradesco.web.pgit.service.business.histcomplementar.bean.IncluirMantHistoricoComplementarEntradaDTO;
import br.com.bradesco.web.pgit.service.business.histcomplementar.bean.IncluirMantHistoricoComplementarSaidaDTO;
import br.com.bradesco.web.pgit.service.business.histcomplementar.bean.ListarHistMsgHistoricoComplementarEntradaDTO;
import br.com.bradesco.web.pgit.service.business.histcomplementar.bean.ListarHistMsgHistoricoComplementarSaidaDTO;
import br.com.bradesco.web.pgit.service.business.histcomplementar.bean.ListarMantHistoricoComplementarEntradaDTO;
import br.com.bradesco.web.pgit.service.business.histcomplementar.bean.ListarMantHistoricoComplementarSaidaDTO;

/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Interface do adaptador: ManterHistoricoComplementar
 * </p>
 * 
 * @comment CODIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public interface IHistComplementarService {

    /**
     * M�todo de exemplo.
     */
    void sampleManterHistoricoComplementar();
    
    /**
     * Listar mant historico complementar.
     *
     * @param listarMantHistoricoComplementarEntradaDTO the listar mant historico complementar entrada dto
     * @return the list< listar mant historico complementar saida dt o>
     */
    List<ListarMantHistoricoComplementarSaidaDTO> listarMantHistoricoComplementar (ListarMantHistoricoComplementarEntradaDTO listarMantHistoricoComplementarEntradaDTO);
    
    /**
     * Listar hist msg historico complementar.
     *
     * @param listarHistMsgHistoricoComplementarEntradaDTO the listar hist msg historico complementar entrada dto
     * @return the list< listar hist msg historico complementar saida dt o>
     */
    List<ListarHistMsgHistoricoComplementarSaidaDTO> listarHistMsgHistoricoComplementar (ListarHistMsgHistoricoComplementarEntradaDTO listarHistMsgHistoricoComplementarEntradaDTO);
    
    /**
     * Incluir mant historico complementar.
     *
     * @param incluirMantHistoricoComplementarEntradaDTO the incluir mant historico complementar entrada dto
     * @return the incluir mant historico complementar saida dto
     */
    IncluirMantHistoricoComplementarSaidaDTO incluirMantHistoricoComplementar (IncluirMantHistoricoComplementarEntradaDTO incluirMantHistoricoComplementarEntradaDTO);
    
    /**
     * Alterar mant historico complementar.
     *
     * @param alterarMantHistoricoComplementarEntradaDTO the alterar mant historico complementar entrada dto
     * @return the alterar mant historico complementar saida dto
     */
    AlterarMantHistoricoComplementarSaidaDTO alterarMantHistoricoComplementar (AlterarMantHistoricoComplementarEntradaDTO alterarMantHistoricoComplementarEntradaDTO);
    
    /**
     * Excluir mant historico complementar.
     *
     * @param excluirMantHistoricoComplementarEntradaDTO the excluir mant historico complementar entrada dto
     * @return the excluir mant historico complementar saida dto
     */
    ExcluirMantHistoricoComplementarSaidaDTO excluirMantHistoricoComplementar (ExcluirMantHistoricoComplementarEntradaDTO excluirMantHistoricoComplementarEntradaDTO);
    
    /**
     * Detalhar mant historico complementar.
     *
     * @param detalharMantHistoricoComplementarEntradaDTO the detalhar mant historico complementar entrada dto
     * @return the detalhar mant historico complementar saida dto
     */
    DetalharMantHistoricoComplementarSaidaDTO detalharMantHistoricoComplementar (DetalharMantHistoricoComplementarEntradaDTO detalharMantHistoricoComplementarEntradaDTO);
    
    /**
     * Detalhar hist msg historico complementar.
     *
     * @param detalharHistMsgHistoricoComplementarEntradaDTO the detalhar hist msg historico complementar entrada dto
     * @return the detalhar hist msg historico complementar saida dto
     */
    DetalharHistMsgHistoricoComplementarSaidaDTO detalharHistMsgHistoricoComplementar (DetalharHistMsgHistoricoComplementarEntradaDTO detalharHistMsgHistoricoComplementarEntradaDTO);
}

