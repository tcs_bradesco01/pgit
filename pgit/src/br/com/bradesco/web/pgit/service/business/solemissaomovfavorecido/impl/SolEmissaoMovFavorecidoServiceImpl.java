/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/implementation.ftl,v $
 * $Id: implementation.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: implementation.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */
 
package br.com.bradesco.web.pgit.service.business.solemissaomovfavorecido.impl;

import java.util.ArrayList;
import java.util.List;

import br.com.bradesco.web.pgit.service.business.solemissaomovfavorecido.ISolEmissaoMovFavorecidoService;
import br.com.bradesco.web.pgit.service.business.solemissaomovfavorecido.ISolEmissaoMovFavorecidoServiceConstants;
import br.com.bradesco.web.pgit.service.business.solemissaomovfavorecido.bean.ConsultarSolEmiAviMovtoFavorecidoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.solemissaomovfavorecido.bean.ConsultarSolEmiAviMovtoFavorecidoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.solemissaomovfavorecido.bean.DetalharSolEmiAviMovtoFavorecidoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.solemissaomovfavorecido.bean.DetalharSolEmiAviMovtoFavorecidoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.solemissaomovfavorecido.bean.ExcluirSolEmiAviMovtoFavorecidoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.solemissaomovfavorecido.bean.ExcluirSolEmiAviMovtoFavorecidoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.solemissaomovfavorecido.bean.IncluirSolEmiAviMovtoFavorecidoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.solemissaomovfavorecido.bean.IncluirSolEmiAviMovtoFavorecidoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.solemissaomovfavorecido.bean.OcorrenciasDetalharSolEmiAviMovtoFavorecidoSaidaDTO;
import br.com.bradesco.web.pgit.service.data.pdc.FactoryAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarsolemiavimovtofavorecido.request.ConsultarSolEmiAviMovtoFavorecidoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarsolemiavimovtofavorecido.response.ConsultarSolEmiAviMovtoFavorecidoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.detalharsolemiavimovtofavorecido.request.DetalharSolEmiAviMovtoFavorecidoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.detalharsolemiavimovtofavorecido.response.DetalharSolEmiAviMovtoFavorecidoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.excluirsolemiavimovtofavorecido.request.ExcluirSolEmiAviMovtoFavorecidoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.excluirsolemiavimovtofavorecido.response.ExcluirSolEmiAviMovtoFavorecidoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.incluirsolemiavimovtofavorecido.request.IncluirSolEmiAviMovtoFavorecidoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.incluirsolemiavimovtofavorecido.response.IncluirSolEmiAviMovtoFavorecidoResponse;
import br.com.bradesco.web.pgit.utils.DateUtils;
import br.com.bradesco.web.pgit.utils.PgitUtil;
import br.com.bradesco.web.pgit.view.converters.FormatarData;


/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Implementa��o do adaptador: SolEmissaoMovFavorecido
 * </p>
 * 
 * @comment C�DIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public class SolEmissaoMovFavorecidoServiceImpl implements ISolEmissaoMovFavorecidoService {
	
	/** Atributo factoryAdapter. */
	private FactoryAdapter factoryAdapter;
	
    /**
     * Get: factoryAdapter.
     *
     * @return factoryAdapter
     */
    public FactoryAdapter getFactoryAdapter() {
		return factoryAdapter;
	}
	
	/**
	 * Set: factoryAdapter.
	 *
	 * @param factoryAdapter the factory adapter
	 */
	public void setFactoryAdapter(FactoryAdapter factoryAdapter) {
		this.factoryAdapter = factoryAdapter;
	}
    
    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.solemissaomovfavorecido.ISolEmissaoMovFavorecidoService#consultarSolEmiAviMovtoFavorecido(br.com.bradesco.web.pgit.service.business.solemissaomovfavorecido.bean.ConsultarSolEmiAviMovtoFavorecidoEntradaDTO)
     */
    public List<ConsultarSolEmiAviMovtoFavorecidoSaidaDTO> consultarSolEmiAviMovtoFavorecido(ConsultarSolEmiAviMovtoFavorecidoEntradaDTO entradaDTO){
    	List<ConsultarSolEmiAviMovtoFavorecidoSaidaDTO> listaSaida = new ArrayList<ConsultarSolEmiAviMovtoFavorecidoSaidaDTO>();
    	ConsultarSolEmiAviMovtoFavorecidoRequest request = new ConsultarSolEmiAviMovtoFavorecidoRequest();
    	ConsultarSolEmiAviMovtoFavorecidoResponse response = new ConsultarSolEmiAviMovtoFavorecidoResponse();
    	
    	request.setNrOcorrencias(ISolEmissaoMovFavorecidoServiceConstants.NUMERO_OCORRENCIAS_CONSULTAR);
    	request.setCdPessoaJuridicaContrato(PgitUtil.verificaLongNulo(entradaDTO.getCdPessoaJuridicaContrato()));
    	request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoContratoNegocio()));
    	request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(entradaDTO.getNrSequenciaContratoNegocio()));
    	request.setDtInicioSolicitacao(PgitUtil.verificaStringNula(entradaDTO.getDtInicioSolicitacao()));
    	request.setDtFimSolicitacao(PgitUtil.verificaStringNula(entradaDTO.getDtFimSolicitacao()));
    	request.setCdSituacaoSolicitacao(PgitUtil.verificaIntegerNulo(entradaDTO.getCdSituacaoSolicitacao()));
    	
    	response = getFactoryAdapter().getConsultarSolEmiAviMovtoFavorecidoPDCAdapter().invokeProcess(request);
    	for(br.com.bradesco.web.pgit.service.data.pdc.consultarsolemiavimovtofavorecido.response.Ocorrencias o : response.getOcorrencias()){
    		ConsultarSolEmiAviMovtoFavorecidoSaidaDTO saida = new ConsultarSolEmiAviMovtoFavorecidoSaidaDTO();
    		
    		saida.setCodMensagem(response.getCodMensagem());
    		saida.setMensagem(response.getMensagem());
    		
    		saida.setCdTipoSolicitacao(o.getCdTipoSolicitacao());
    		saida.setNrSolicitacao(o.getNrSolicitacao());
    		saida.setDtHoraSolicitacao(o.getDtHoraSolicitacao());
    		saida.setDtHoraAtendimento(o.getDtHoraAtendimento());
    		saida.setCdSituacaoSolicitacao(o.getCdSituacaoSolicitacao());
    		saida.setDsSituacaoSolicitacao(o.getDsSituacaoSolicitacao());
    		saida.setCdMotivoSituacao(o.getCdMotivoSituacao());
    		saida.setDsMotivoSituacao(o.getDsMotivoSituacao());
    		saida.setCdTipoCanal(o.getCdTipoCanal());
    		saida.setDsTipoCanal(o.getDsTipoCanal());
    		saida.setCdPessoaJuridica(o.getCdPessoaJuridica());
    		saida.setCdTipoContrato(o.getCdTipoContrato());
    		saida.setNrSequenciaContrato(o.getNrSequenciaContrato());
    		saida.setCdUsuarioInclusao(o.getCdUsuarioInclusao());
    		saida.setDtHoraSolicitacaoFormatada(FormatarData.formatarDataTrilha(o.getDtHoraSolicitacao()));
    		saida.setDsSituacaoFormatado(PgitUtil.concatenarCampos(o.getCdSituacaoSolicitacao(), o.getDsSituacaoSolicitacao()));
    		saida.setDsMotivoFormatado(PgitUtil.concatenarCampos(o.getCdMotivoSituacao(), o.getDsMotivoSituacao()));
    		saida.setDsContratoFormatado(PgitUtil.concatenarCampos(PgitUtil.concatenarCampos(o.getCdPessoaJuridica(), o.getCdTipoContrato()), o.getNrSequenciaContrato()));
    		
    		listaSaida.add(saida);
    	}
    	
    	return listaSaida;
    }
    
    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.solemissaomovfavorecido.ISolEmissaoMovFavorecidoService#detalharSolEmiAviMovtoFavorecido(br.com.bradesco.web.pgit.service.business.solemissaomovfavorecido.bean.DetalharSolEmiAviMovtoFavorecidoEntradaDTO)
     */
    public DetalharSolEmiAviMovtoFavorecidoSaidaDTO detalharSolEmiAviMovtoFavorecido(DetalharSolEmiAviMovtoFavorecidoEntradaDTO entradaDTO){
    	DetalharSolEmiAviMovtoFavorecidoSaidaDTO saidaDTO = new DetalharSolEmiAviMovtoFavorecidoSaidaDTO();
    	DetalharSolEmiAviMovtoFavorecidoRequest request = new DetalharSolEmiAviMovtoFavorecidoRequest();
    	DetalharSolEmiAviMovtoFavorecidoResponse response = new DetalharSolEmiAviMovtoFavorecidoResponse();
    	
    	request.setNrOcorrencias(ISolEmissaoMovFavorecidoServiceConstants.NUMERO_OCORRENCIAS_DETALHAR);
    	request.setCdPessoaJuridicaEmpr(PgitUtil.verificaLongNulo(entradaDTO.getCdPessoaJuridicaEmpr()));
    	request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoContratoNegocio()));
    	request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(entradaDTO.getNrSequenciaContratoNegocio()));
    	request.setCdTipoSolicitacaoPagamento(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoSolicitacaoPagamento()));
    	request.setCdSolicitacao(PgitUtil.verificaIntegerNulo(entradaDTO.getCdSolicitacao()));
    	
    	response = getFactoryAdapter().getDetalharSolEmiAviMovtoFavorecidoPDCAdapter().invokeProcess(request);
		
		saidaDTO.setCodMensagem(response.getCodMensagem());
		saidaDTO.setMensagem(response.getMensagem());
		saidaDTO.setCdSolicitacaoPagamentoIntegrado(response.getCdSolicitacaoPagamentoIntegrado());
	    saidaDTO.setNrSolicitacaoPagamentoIntegrado(response.getNrSolicitacaoPagamentoIntegrado());
	    saidaDTO.setCdSituacaoSolicitacao(response.getCdSituacaoSolicitacao());
        saidaDTO.setDsSituacaoSolicitacao(response.getDsSituacaoSolicitacao());
	    saidaDTO.setDtInicioPeriodoMovimentacao(DateUtils.formartarDataPDCparaPadraPtBr(response.getDtInicioPeriodoMovimentacao(), "dd.MM.yyyy", "dd/MM/yyyy"));
	    saidaDTO.setDtFimPeriodoMovimentacao(DateUtils.formartarDataPDCparaPadraPtBr(response.getDtFimPeriodoMovimentacao(), "dd.MM.yyyy", "dd/MM/yyyy"));
	    saidaDTO.setCdProdutoServicoOperacao(response.getCdProdutoServicoOperacao());
	    saidaDTO.setDsProdutoServicoOperacao(response.getDsProdutoServicoOperacao());
	    saidaDTO.setCdProdutoOperacaoRelacionado(response.getCdProdutoOperacaoRelacionado());
	    saidaDTO.setDsProdutoOperacaoRelacionado(response.getDsProdutoOperacaoRelacionado());
	    saidaDTO.setCdRecebedorCredito(response.getCdRecebedorCredito());
	    saidaDTO.setCdTipoInscricaoRecebedor(response.getCdTipoInscricaoRecebedor());
	    saidaDTO.setCdCpfCnpjRecebedor(response.getCdCpfCnpjRecebedor());
	    saidaDTO.setCdFilialCnpjRecebedor(response.getCdFilialCnpjRecebedor());
	    saidaDTO.setCdControleCpfRecebedor(response.getCdControleCpfRecebedor());
	    saidaDTO.setCdDestinoCorrespSolicitacao(response.getCdDestinoCorrespSolicitacao());
	    saidaDTO.setCdTipoPostagemSolicitacao(response.getCdTipoPostagemSolicitacao());
	    saidaDTO.setDsLogradouroPagador(response.getDsLogradouroPagador());
	    saidaDTO.setDsNumeroLogradouroPagador(response.getDsNumeroLogradouroPagador());
	    saidaDTO.setDsComplementoLogradouroPagador(response.getDsComplementoLogradouroPagador());
	    saidaDTO.setDsBairroClientePagador(response.getDsBairroClientePagador());
	    saidaDTO.setDsMunicipioClientePagador(response.getDsMunicipioClientePagador());
	    saidaDTO.setCdSiglaUfPagador(response.getCdSiglaUfPagador());
	    saidaDTO.setCdCepPagador(response.getCdCepPagador());
	    saidaDTO.setCdCepComplementoPagador(response.getCdCepComplementoPagador());
	    saidaDTO.setDsEmailClientePagador(response.getDsEmailClientePagador());
	    saidaDTO.setCdAgenciaOperadora(response.getCdAgenciaOperadora());
	    saidaDTO.setDsAgencia(response.getDsAgencia());
	    saidaDTO.setVlTarifa(response.getVlrTarifaAtual());
	    saidaDTO.setVlrTarifaAtual(response.getVlrTarifaAtual());
	    saidaDTO.setVlTarifaPadrao(response.getVlTarifaPadrao());
	    saidaDTO.setCdPercentualDescTarifa(response.getCdPercentualDescTarifa());
	    saidaDTO.setCdUsuarioInclusao(response.getCdUsuarioInclusao());
	    saidaDTO.setHrInclusaoRegistro(response.getHrInclusaoRegistro());
	    saidaDTO.setCdOperacaoCanalInclusao(response.getCdOperacaoCanalInclusao().equals("0") ? "" : response.getCdOperacaoCanalInclusao());
	    saidaDTO.setCdTipoCanalInclusao(response.getCdTipoCanalInclusao());
	    saidaDTO.setDsTipoCanalInclusao(response.getDsTipoCanalInclusao());
	    saidaDTO.setNrLinhas(response.getNrLinhas());
	    saidaDTO.setDsCepFormatado(PgitUtil.formatCep(response.getCdCepPagador(), response.getCdCepComplementoPagador()));
	    saidaDTO.setCdDepartamentoUnidade(response.getCdDepartamentoUnidade());
	    
	    List<OcorrenciasDetalharSolEmiAviMovtoFavorecidoSaidaDTO> listaMovimentacoes = new ArrayList<OcorrenciasDetalharSolEmiAviMovtoFavorecidoSaidaDTO>();
	    
    	for(br.com.bradesco.web.pgit.service.data.pdc.detalharsolemiavimovtofavorecido.response.Ocorrencias o : response.getOcorrencias()){
    		OcorrenciasDetalharSolEmiAviMovtoFavorecidoSaidaDTO movimentacao = new OcorrenciasDetalharSolEmiAviMovtoFavorecidoSaidaDTO();
    		movimentacao.setCdTipoCanal(o.getCdTipoCanal());
    		movimentacao.setCdControlePagamento(o.getCdControlePagamento());
    		movimentacao.setDtPagamento(DateUtils.formartarDataPDCparaPadraPtBr(o.getDtPagamento(), "dd.MM.yyyy", "dd/MM/yyyy"));
    		movimentacao.setVlPagamento(o.getVlPagamento());
    		movimentacao.setCdFavorecido(o.getCdFavorecido());
    		movimentacao.setDsFavorecido(o.getDsFavorecido());
    		movimentacao.setCdBancoDebito(o.getCdBancoDebito());
    		movimentacao.setCdAgenciaDebito(o.getCdAgenciaDebito());
    		movimentacao.setCdContaDebito(o.getCdContaDebito());
    	    movimentacao.setCdDigitoContaDebito(o.getCdDigitoContaDebito());    	   
    	    movimentacao.setCdProdutoServico(o.getCdProdutoServico());
    	    movimentacao.setCdSituacaoPagamento(o.getCdSituacaoPagamento());
    	    movimentacao.setDsSituacaoPagamento(o.getDsSituacaoPagamento());
    	    movimentacao.setDsCodigoProdutoServicoOperacao(o.getDsCodigoProdutoServicoOperacao());
    	    movimentacao.setCdProdutoRelacionado(o.getCdProdutoRelacionado());
    		movimentacao.setDsCodigoProdutoServicoRelacionado(o.getDsCodigoProdutoServicoRelacionado());
    	    
    		movimentacao.setDsFavorecidoFormatado(PgitUtil.concatenarCampos(o.getCdFavorecido(), o.getDsFavorecido()));
    		movimentacao.setDsContaDebitoFormatada(PgitUtil.formatBancoAgenciaConta(o.getCdBancoDebito(), o.getCdAgenciaDebito(), null, o.getCdContaDebito(), String.valueOf(PgitUtil.verificaIntegerNulo(o.getCdDigitoContaDebito())), true));
    		
    		listaMovimentacoes.add(movimentacao);
    	}
    	
    	saidaDTO.setListaMovimentacoes(listaMovimentacoes);
    	
    	return saidaDTO;
    }
    
    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.solemissaomovfavorecido.ISolEmissaoMovFavorecidoService#incluirSolEmiAviMovtoFavorecido(br.com.bradesco.web.pgit.service.business.solemissaomovfavorecido.bean.IncluirSolEmiAviMovtoFavorecidoEntradaDTO)
     */
    public IncluirSolEmiAviMovtoFavorecidoSaidaDTO incluirSolEmiAviMovtoFavorecido(IncluirSolEmiAviMovtoFavorecidoEntradaDTO entradaDTO){
    	IncluirSolEmiAviMovtoFavorecidoSaidaDTO saidaDTO = new IncluirSolEmiAviMovtoFavorecidoSaidaDTO();
    	IncluirSolEmiAviMovtoFavorecidoRequest request = new IncluirSolEmiAviMovtoFavorecidoRequest();
    	IncluirSolEmiAviMovtoFavorecidoResponse response = new IncluirSolEmiAviMovtoFavorecidoResponse();
    	
    	request.setCdPessoaJuridicaContrato(PgitUtil.verificaLongNulo(entradaDTO.getCdPessoaJuridicaContrato()));
        request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoContratoNegocio()));
        request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(entradaDTO.getNrSequenciaContratoNegocio()));
        request.setDtInicioPerMovimentacao(PgitUtil.verificaStringNula(entradaDTO.getDtInicioPerMovimentacao()));
        request.setDsFimPerMovimentacao(PgitUtil.verificaStringNula(entradaDTO.getDsFimPerMovimentacao()));
        request.setCdProdutoServicoOperacao(PgitUtil.verificaIntegerNulo(entradaDTO.getCdProdutoServicoOperacao()));
        request.setCdProdutoOperacaoRelacionado(PgitUtil.verificaIntegerNulo(entradaDTO.getCdProdutoOperacaoRelacionado()));
        request.setCdRecebedorCredito(PgitUtil.verificaLongNulo(entradaDTO.getCdRecebedorCredito()));
        request.setCdTipoInscricaoRecebedor(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoInscricaoRecebedor()));
        request.setCdCpfCnpjRecebedor(PgitUtil.verificaLongNulo(entradaDTO.getCdCpfCnpjRecebedor()));
        request.setCdFilialCnpjRecebedor(PgitUtil.verificaIntegerNulo(entradaDTO.getCdFilialCnpjRecebedor()));
        request.setCdControleCpfRecebedor(PgitUtil.verificaIntegerNulo(entradaDTO.getCdControleCpfRecebedor()));
        request.setCdDestinoCorrespondenciaSolic(PgitUtil.verificaIntegerNulo(entradaDTO.getCdDestinoCorrespondenciaSolic()));
        request.setCdTipoPostagemSolicitacao(PgitUtil.verificaStringNula(entradaDTO.getCdTipoPostagemSolicitacao()));
        request.setDsLogradouroPagador(PgitUtil.verificaStringNula(entradaDTO.getDsLogradouroPagador()));
        request.setDsNumeroLogradouroPagador(PgitUtil.verificaStringNula(entradaDTO.getDsNumeroLogradouroPagador()));
        request.setDsComplementoLogradouroPagador(PgitUtil.verificaStringNula(entradaDTO.getDsComplementoLogradouroPagador()));
        request.setDsBairroClientePagador(PgitUtil.verificaStringNula(entradaDTO.getDsBairroClientePagador()));
        request.setDsMunicipioClientePagador(PgitUtil.verificaStringNula(entradaDTO.getDsMunicipioClientePagador()));
        request.setCdSiglaUfPagador(PgitUtil.verificaStringNula(entradaDTO.getCdSiglaUfPagador()));
        request.setCdCepPagador(PgitUtil.verificaIntegerNulo(entradaDTO.getCdCepPagador()));
        request.setCdCepComplementoPagador(PgitUtil.verificaIntegerNulo(entradaDTO.getCdCepComplementoPagador()));
        request.setDsEmailClientePagador(PgitUtil.verificaStringNula(entradaDTO.getDsEmailClientePagador()));
        request.setCdPessoaJuridicaDepto(PgitUtil.verificaLongNulo(entradaDTO.getCdPessoaJuridicaDepto()));
        request.setNrSequenciaUnidadeDepto(PgitUtil.verificaIntegerNulo(entradaDTO.getNrSequenciaUnidadeDepto()));
        request.setCdDepartamentoUnidade(PgitUtil.verificaIntegerNulo(entradaDTO.getCdDepartamentoUnidade()));
        request.setVrTarifaPadraoSolic(PgitUtil.verificaBigDecimalNulo(entradaDTO.getVrTarifaPadraoSolic()));
        request.setCdPercentualTarifaSolic(PgitUtil.verificaBigDecimalNulo(entradaDTO.getCdPercentualTarifaSolic()));
        request.setVlTarifaNegocioSolic(PgitUtil.verificaBigDecimalNulo(entradaDTO.getVlTarifaNegocioSolic()));
        request.setNrRegistrosEntrada(ISolEmissaoMovFavorecidoServiceConstants.NUMERO_REGISTROS_INCLUIR);
        
        ArrayList<br.com.bradesco.web.pgit.service.data.pdc.incluirsolemiavimovtofavorecido.request.Ocorrencias> ocorrencias = new ArrayList<br.com.bradesco.web.pgit.service.data.pdc.incluirsolemiavimovtofavorecido.request.Ocorrencias>();

        br.com.bradesco.web.pgit.service.data.pdc.incluirsolemiavimovtofavorecido.request.Ocorrencias ocorrencia = new br.com.bradesco.web.pgit.service.data.pdc.incluirsolemiavimovtofavorecido.request.Ocorrencias();
   		ocorrencia.setCdTipoCanal(ISolEmissaoMovFavorecidoServiceConstants.TIPO_CANAL_INCLUIR);
   		ocorrencia.setCdControlePagamento(ISolEmissaoMovFavorecidoServiceConstants.CONTROLE_PAGAMENTO_INCLUIR);
   		ocorrencias.add(ocorrencia);
    	
    	request.setOcorrencias((br.com.bradesco.web.pgit.service.data.pdc.incluirsolemiavimovtofavorecido.request.Ocorrencias[]) ocorrencias.toArray(new br.com.bradesco.web.pgit.service.data.pdc.incluirsolemiavimovtofavorecido.request.Ocorrencias[0]));
    	
    	response = getFactoryAdapter().getIncluirSolEmiAviMovtoFavorecidoPDCAdapter().invokeProcess(request);
    	
    	saidaDTO.setCodMensagem(response.getCodMensagem());
	    saidaDTO.setMensagem(response.getMensagem());
	    
    	return saidaDTO;
    }
    
    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.solemissaomovfavorecido.ISolEmissaoMovFavorecidoService#excluirSolEmiAviMovtoFavorecido(br.com.bradesco.web.pgit.service.business.solemissaomovfavorecido.bean.ExcluirSolEmiAviMovtoFavorecidoEntradaDTO)
     */
    public ExcluirSolEmiAviMovtoFavorecidoSaidaDTO excluirSolEmiAviMovtoFavorecido(ExcluirSolEmiAviMovtoFavorecidoEntradaDTO entradaDTO){
    	ExcluirSolEmiAviMovtoFavorecidoSaidaDTO saidaDTO = new ExcluirSolEmiAviMovtoFavorecidoSaidaDTO();
    	ExcluirSolEmiAviMovtoFavorecidoRequest request = new ExcluirSolEmiAviMovtoFavorecidoRequest();
    	ExcluirSolEmiAviMovtoFavorecidoResponse response = new ExcluirSolEmiAviMovtoFavorecidoResponse();
    	
    	request.setCdSolicitacaoPagamentoIntegrado(PgitUtil.verificaIntegerNulo(entradaDTO.getCdSolicitacaoPagamentoIntegrado()));
    	request.setNrSolicitacaoPagamentoIntegrado(PgitUtil.verificaIntegerNulo(entradaDTO.getNrSolicitacaoPagamentoIntegrado()));
        request.setCdPessoaJuridicaContrato(PgitUtil.verificaLongNulo(entradaDTO.getCdPessoaJuridicaContrato()));
        request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoContratoNegocio()));
        request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(entradaDTO.getNrSequenciaContratoNegocio()));
        request.setNrRegistroEntrada(ISolEmissaoMovFavorecidoServiceConstants.CODIGO_TIPO_CONTRATO_EXCLUIR);
        
        ArrayList<br.com.bradesco.web.pgit.service.data.pdc.excluirsolemiavimovtofavorecido.request.Ocorrencias> ocorrencias = new ArrayList<br.com.bradesco.web.pgit.service.data.pdc.excluirsolemiavimovtofavorecido.request.Ocorrencias>();
        br.com.bradesco.web.pgit.service.data.pdc.excluirsolemiavimovtofavorecido.request.Ocorrencias ocorrencia = new br.com.bradesco.web.pgit.service.data.pdc.excluirsolemiavimovtofavorecido.request.Ocorrencias();
        ocorrencia.setCdControlePagamento("");
        ocorrencia.setCdTipoCanal(0);
        ocorrencias.add(ocorrencia);
        
    	request.setOcorrencias((br.com.bradesco.web.pgit.service.data.pdc.excluirsolemiavimovtofavorecido.request.Ocorrencias[]) ocorrencias.toArray(new br.com.bradesco.web.pgit.service.data.pdc.excluirsolemiavimovtofavorecido.request.Ocorrencias[0]));
    	
        response = getFactoryAdapter().getExcluirSolEmiAviMovtoFavorecidoPDCAdapter().invokeProcess(request);
        
    	saidaDTO.setCodMensagem(response.getCodMensagem());
	    saidaDTO.setMensagem(response.getMensagem());
	    
	    return saidaDTO;
    }
}