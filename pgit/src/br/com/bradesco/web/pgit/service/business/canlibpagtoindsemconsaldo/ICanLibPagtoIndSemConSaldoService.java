/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/interfaz.ftl,v $
 * $Id: interfaz.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: interfaz.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.canlibpagtoindsemconsaldo;

import java.util.List;

import br.com.bradesco.web.pgit.service.business.canlibpagtoindsemconsaldo.bean.CancelarLibPagtosIndSemConsultaSaldoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.canlibpagtoindsemconsaldo.bean.CancelarLibPagtosIndSemConsultaSaldoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.canlibpagtoindsemconsaldo.bean.ConsultarCanLibPagtosindSemConsSaldoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.canlibpagtoindsemconsaldo.bean.ConsultarCanLibPagtosindSemConsSaldoSaidaDTO;

/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Interface do adaptador: CanLibPagtoIndSemConSaldo
 * </p>
 * 
 * @comment CODIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public interface ICanLibPagtoIndSemConSaldoService {
	
	/**
	 * Consultar can lib pagtosind sem cons saldo.
	 *
	 * @param entradaDTO the entrada dto
	 * @return the list< consultar can lib pagtosind sem cons saldo saida dt o>
	 */
	List<ConsultarCanLibPagtosindSemConsSaldoSaidaDTO> consultarCanLibPagtosindSemConsSaldo(ConsultarCanLibPagtosindSemConsSaldoEntradaDTO entradaDTO);
	
	/**
	 * Cancelar lib pagtos ind sem consulta saldo.
	 *
	 * @param listaEntradaDTO the lista entrada dto
	 * @return the cancelar lib pagtos ind sem consulta saldo saida dto
	 */
	CancelarLibPagtosIndSemConsultaSaldoSaidaDTO cancelarLibPagtosIndSemConsultaSaldo(List<CancelarLibPagtosIndSemConsultaSaldoEntradaDTO> listaEntradaDTO);
}