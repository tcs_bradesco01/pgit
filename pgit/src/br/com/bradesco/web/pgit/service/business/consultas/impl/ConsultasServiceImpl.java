/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/implementation.ftl,v $
 * $Id: implementation.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: implementation.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */
 
package br.com.bradesco.web.pgit.service.business.consultas.impl;

import java.util.ArrayList;
import java.util.List;

import br.com.bradesco.web.pgit.service.business.consultas.IConsultasService;
import br.com.bradesco.web.pgit.service.business.consultas.IConsultasServiceConstants;
import br.com.bradesco.web.pgit.service.business.consultas.bean.ConsultarDescBancoAgenciaContaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultas.bean.ConsultarDescBancoAgenciaContaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultas.bean.ConsultarDescGrupoEconomicoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultas.bean.ConsultarDescGrupoEconomicoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultas.bean.ConsultarEmailEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultas.bean.ConsultarEmailSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultas.bean.ConsultarEnderecoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultas.bean.ConsultarEnderecoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultas.bean.ConsultarInformacoesFavorecidoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultas.bean.ConsultarInformacoesFavorecidoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultas.bean.DetalharDadosContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultas.bean.DetalharDadosContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultas.bean.ListarTipoRetiradaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultas.bean.ListarTipoRetiradaOcorrenciasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultas.bean.ListarTipoRetiradaSaidaDTO;
import br.com.bradesco.web.pgit.service.data.pdc.FactoryAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultardescbancoagenciaconta.request.ConsultarDescBancoAgenciaContaRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultardescbancoagenciaconta.response.ConsultarDescBancoAgenciaContaResponse;
import br.com.bradesco.web.pgit.service.data.pdc.consultardescricaogrupoeconomico.request.ConsultarDescricaoGrupoEconomicoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultardescricaogrupoeconomico.response.ConsultarDescricaoGrupoEconomicoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.consultaremail.request.ConsultarEmailRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultaremail.response.ConsultarEmailResponse;
import br.com.bradesco.web.pgit.service.data.pdc.consultarendereco.request.ConsultarEnderecoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarendereco.response.ConsultarEnderecoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.consultarinformacoesfavorecido.request.ConsultarInformacoesFavorecidoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarinformacoesfavorecido.response.ConsultarInformacoesFavorecidoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.detalhardadoscontrato.request.DetalharDadosContratoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.detalhardadoscontrato.response.DetalharDadosContratoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listartiporetirada.request.ListarTipoRetiradaRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listartiporetirada.response.ListarTipoRetiradaResponse;
import br.com.bradesco.web.pgit.utils.PgitUtil;


/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Implementa��o do adaptador: Consultas
 * </p>
 * 
 * @comment C�DIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public class ConsultasServiceImpl implements IConsultasService {
	
	/** Atributo factoryAdapter. */
	private FactoryAdapter factoryAdapter;

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.consultas.IConsultasService#consultarDescBancoAgenciaConta(br.com.bradesco.web.pgit.service.business.consultas.bean.ConsultarDescBancoAgenciaContaEntradaDTO)
	 */
	public ConsultarDescBancoAgenciaContaSaidaDTO consultarDescBancoAgenciaConta(ConsultarDescBancoAgenciaContaEntradaDTO consultarDescBancoAgenciaContaEntradaDTO) {
		
		ConsultarDescBancoAgenciaContaRequest request = new ConsultarDescBancoAgenciaContaRequest();
		request.setCdAgencia(PgitUtil.verificaIntegerNulo(consultarDescBancoAgenciaContaEntradaDTO.getCdAgencia()));
		request.setCdBanco(PgitUtil.verificaIntegerNulo(consultarDescBancoAgenciaContaEntradaDTO.getCdBanco()));
		request.setCdConta(PgitUtil.verificaLongNulo(consultarDescBancoAgenciaContaEntradaDTO.getCdConta()));
		request.setCdDigitoConta(PgitUtil.verificaStringNula(consultarDescBancoAgenciaContaEntradaDTO.getCdDigitoConta()));
		
		ConsultarDescBancoAgenciaContaResponse response = getFactoryAdapter().getConsultarDescBancoAgenciaContaPDCAdapter().invokeProcess(request);
		
		ConsultarDescBancoAgenciaContaSaidaDTO saidaDTO = new ConsultarDescBancoAgenciaContaSaidaDTO();
		saidaDTO.setCdAgencia(response.getCdAgencia());
		saidaDTO.setCdBanco(response.getCdBanco());
		saidaDTO.setCdConta(response.getCdConta());
		saidaDTO.setCdDigitoConta(PgitUtil.verificaBytesDigitoConta(response.getCdDigitoConta()));
		saidaDTO.setDsBanco(response.getDsBanco());
		saidaDTO.setDsAgencia(response.getDsAgencia());
		saidaDTO.setDsTipoConta(response.getDsConta());
		
		saidaDTO.setCdDigitoAgencia(response.getCdDigitoAgencia());
		
		return saidaDTO;
	}
	
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.consultas.IConsultasService#consultarDescGrupoEconomico(br.com.bradesco.web.pgit.service.business.consultas.bean.ConsultarDescGrupoEconomicoEntradaDTO)
	 */
	public ConsultarDescGrupoEconomicoSaidaDTO consultarDescGrupoEconomico(ConsultarDescGrupoEconomicoEntradaDTO entradaDTO) {
		
		ConsultarDescricaoGrupoEconomicoRequest request = new ConsultarDescricaoGrupoEconomicoRequest();
		request.setCdGrupoEconomico(PgitUtil.verificaLongNulo(entradaDTO.getCdGrupoEconomico()));
		
		ConsultarDescricaoGrupoEconomicoResponse response = getFactoryAdapter().getConsultarDescricaoGrupoEconomicoPDCAdapter().invokeProcess(request);
		
		ConsultarDescGrupoEconomicoSaidaDTO saidaDTO = new ConsultarDescGrupoEconomicoSaidaDTO();
		saidaDTO.setCdGrupoEconomico(response.getCdGrupoEconomico());
		saidaDTO.setDsGrupoEconomico(response.getDsGrupoEconomico());
				
		return saidaDTO;
	}
	
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.consultas.IConsultasService#consultarInformacoesFavorecido(br.com.bradesco.web.pgit.service.business.consultas.bean.ConsultarInformacoesFavorecidoEntradaDTO)
	 */
	public ConsultarInformacoesFavorecidoSaidaDTO consultarInformacoesFavorecido(ConsultarInformacoesFavorecidoEntradaDTO entradaDTO){
		
		ConsultarInformacoesFavorecidoRequest request = new ConsultarInformacoesFavorecidoRequest();
		request.setCdFavorecido(PgitUtil.verificaLongNulo(entradaDTO.getCdFavorecido()));
		request.setCdTipoIsncricaoFavorecido(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoIsncricaoFavorecido()));
		request.setNrInscricaoFavorecido(PgitUtil.verificaLongNulo(entradaDTO.getNrInscricaoFavorecido()));
		request.setCdPessoaJuridicaContrato(PgitUtil.verificaLongNulo(entradaDTO.getCdPessoaJuridicaContrato()));
		request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoContratoNegocio()));
		request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(entradaDTO.getNrSequenciaContratoNegocio()));
		
		ConsultarInformacoesFavorecidoResponse response = getFactoryAdapter().getConsultarInformacoesFavorecidoPDCAdapter().invokeProcess(request);
		
		ConsultarInformacoesFavorecidoSaidaDTO saidaDTO = new ConsultarInformacoesFavorecidoSaidaDTO();
		saidaDTO.setCodMensagem(response.getCodMensagem());
		saidaDTO.setMensagem(response.getMensagem());
		saidaDTO.setNmFavorecido(response.getNmFavorecido());
		saidaDTO.setCdFavorecido(response.getCdFavorecido());
		saidaDTO.setInscricaoFavorecido(response.getInscricaoFavorecido());
		saidaDTO.setDsTipoFavorecido(response.getDsTipoFavorecido());

		return saidaDTO;
	}

	
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.consultas.IConsultasService#detalharDadosContrato(br.com.bradesco.web.pgit.service.business.consultas.bean.DetalharDadosContratoEntradaDTO)
	 */
	public DetalharDadosContratoSaidaDTO detalharDadosContrato(DetalharDadosContratoEntradaDTO entradaDTO) {
		DetalharDadosContratoRequest request = new DetalharDadosContratoRequest();
		DetalharDadosContratoResponse response = new DetalharDadosContratoResponse();
		DetalharDadosContratoSaidaDTO saidaDTO = new DetalharDadosContratoSaidaDTO();
		
		request.setCdPessoaJuridicaContrato(PgitUtil.verificaLongNulo(entradaDTO.getCdPessoaJuridicaContrato()));
		request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoContratoNegocio()));
		request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(entradaDTO.getNrSequenciaContratoNegocio()));
		
		response = getFactoryAdapter().getDetalharDadosContratoPDCAdapter().invokeProcess(request);
		
		saidaDTO.setCdCnpjCpfTitular(response.getCdCnpjCpfTitular());
		saidaDTO.setCdParticipanteTitular(response.getCdParticipanteTitular());
		saidaDTO.setCdPessoaJuridicaContrato(response.getCdPessoaJuridicaContrato());
		saidaDTO.setCdSituacaoContratoNegocio(response.getCdSituacaoContratoNegocio());
		saidaDTO.setCdTipoContratoNegocio(response.getCdTipoContratoNegocio());		
		saidaDTO.setDsContrato(response.getDsContrato());
		saidaDTO.setDsParticipanteTitular(response.getDsParticipanteTitular());
		saidaDTO.setDsPessoaJuridicaContrato(response.getDsPessoaJuridicaContrato());
		saidaDTO.setDsSituacaoContratoNegocio(response.getDsSituacaoContratoNegocio());
		saidaDTO.setDsTipoContratoNegocio(response.getDsTipoContratoNegocio());		
		saidaDTO.setNrSequenciaContratoNegocio(response.getNrSequenciaContratoNegocio());
		

		return saidaDTO;
	}
	
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.consultas.IConsultasService#consultarEmail(br.com.bradesco.web.pgit.service.business.consultas.bean.ConsultarEmailEntradaDTO)
	 */
	public List<ConsultarEmailSaidaDTO> consultarEmail(ConsultarEmailEntradaDTO entrada){
		List<ConsultarEmailSaidaDTO> listaSaida = new ArrayList<ConsultarEmailSaidaDTO>();
		ConsultarEmailRequest request = new ConsultarEmailRequest();
		ConsultarEmailResponse response = new ConsultarEmailResponse();
		
		request.setNrOcorrencias(IConsultasServiceConstants.NUMERO_OCORRENCIAS_CONSULTAR_EMAIL);
		request.setCdTipoPessoa(PgitUtil.verificaStringNula(entrada.getCdTipoPessoa()));
		request.setCdClub(PgitUtil.verificaLongNulo(entrada.getCdClub()));
		request.setCdEmpresaContrato(PgitUtil.verificaLongNulo(entrada.getCdEmpresaContrato()));

		response = getFactoryAdapter().getConsultarEmailPDCAdapter().invokeProcess(request);

		for(int i=0;i<response.getOcorrenciasCount();i++){
			ConsultarEmailSaidaDTO saida = new ConsultarEmailSaidaDTO();
			saida.setCodMensagem(response.getCodMensagem());
			saida.setMensagem(response.getMensagem());
			saida.setNumeroLinhas(response.getNumeroLinhas());
			saida.setCdCpfCnpjRecebedor(response.getOcorrencias(i).getCdCpfCnpjRecebedor());
			saida.setCdFilialCnpjRecebedor(response.getOcorrencias(i).getCdFilialCnpjRecebedor());
			saida.setCdControleCpfRecebedor(response.getOcorrencias(i).getCdControleCpfRecebedor());
			saida.setDsNomeRazao(response.getOcorrencias(i).getDsNomeRazao());
			saida.setCdEnderecoEletronico(response.getOcorrencias(i).getCdEnderecoEletronico());
    		
			listaSaida.add(saida);
		}

		return listaSaida;
	}	
	
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.consultas.IConsultasService#consultarEndereco(br.com.bradesco.web.pgit.service.business.consultas.bean.ConsultarEnderecoEntradaDTO)
	 */
	public List<ConsultarEnderecoSaidaDTO> consultarEndereco(ConsultarEnderecoEntradaDTO entrada){
		List<ConsultarEnderecoSaidaDTO> listaSaida = new ArrayList<ConsultarEnderecoSaidaDTO>();
		ConsultarEnderecoRequest request = new ConsultarEnderecoRequest();
		ConsultarEnderecoResponse response = new ConsultarEnderecoResponse();

		request.setNrOcorrencias(IConsultasServiceConstants.NUMERO_OCORRENCIAS_CONSULTAR_ENDERECO);
		request.setCdTipoPessoa(PgitUtil.verificaStringNula(entrada.getCdTipoPessoa()));
		request.setCdClub(PgitUtil.verificaLongNulo(entrada.getCdClub()));
		request.setCdEmpresaContrato(PgitUtil.verificaLongNulo(entrada.getCdEmpresaContrato()));

		response = getFactoryAdapter().getConsultarEnderecoPDCAdapter().invokeProcess(request);

		for(int i=0;i<response.getOcorrenciasCount();i++){
			ConsultarEnderecoSaidaDTO saida = new ConsultarEnderecoSaidaDTO();
			saida.setCodMensagem(response.getCodMensagem());
			saida.setMensagem(response.getMensagem());
			saida.setNumeroLinhas(response.getNumeroLinhas());
			saida.setCdCpfCnpjRecebedor(response.getOcorrencias(i).getCdCpfCnpjRecebedor());
			saida.setCdFilialCnpjRecebedor(response.getOcorrencias(i).getCdFilialCnpjRecebedor());
			saida.setCdControleCpfRecebedor(response.getOcorrencias(i).getCdControleCpfRecebedor());
			saida.setDsNomeRazao(response.getOcorrencias(i).getDsNomeRazao());
			saida.setDsLogradouroPagador(response.getOcorrencias(i).getDsLogradouroPagador());
			saida.setDsNumeroLogradouroPagador(response.getOcorrencias(i).getDsNumeroLogradouroPagador());
			saida.setDsComplementoLogradouroPagador(response.getOcorrencias(i).getDsComplementoLogradouroPagador());
			saida.setDsBairroClientePagador(response.getOcorrencias(i).getDsBairroClientePagador());
			saida.setDsMunicipioClientePagador(response.getOcorrencias(i).getDsMunicipioClientePagador());
			saida.setCdSiglaUfPagador(response.getOcorrencias(i).getCdSiglaUfPagador());
			saida.setDsPais(response.getOcorrencias(i).getDsPais());
			saida.setCdCepPagador(response.getOcorrencias(i).getCdCepPagador());
			saida.setCdCepComplementoPagador(response.getOcorrencias(i).getCdCepComplementoPagador());
			
			listaSaida.add(saida);
		}

		return listaSaida;
	}	

	/**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.consultas.IConsultasService
     * #listarTipoRetirada(
     * br.com.bradesco.web.pgit.service.business.consultas.bean.ListarTipoRetiradaEntradaDTO)
     */
    public ListarTipoRetiradaSaidaDTO listarTipoRetirada(ListarTipoRetiradaEntradaDTO entrada) {
        ListarTipoRetiradaRequest request = new ListarTipoRetiradaRequest();
        request.setMaxOcorrencias(IConsultasServiceConstants.NUMERO_OCORRENCIAS_LISTAR_TIPO_RETIRADA);
        request.setCdpessoaJuridicaContrato(IConsultasServiceConstants.CODIGO_PESSOA_JURIDICA_BRAD);
        request.setCdTipoContratoNegocio(IConsultasServiceConstants.CODIGO_TIPO_CONTRATO_PGIT);
        request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(entrada.getNrSequenciaContratoNegocio()));
        request.setCdControlePagamento(PgitUtil.verificaStringNula(entrada.getCdControlePagamento()));
        request.setCdIndentificadorSeqPagamento(PgitUtil.verificaStringNula(entrada.getCdIndentificadorSeqPagamento()));
        request.setCdProdutoServicoOperacao(PgitUtil.verificaIntegerNulo(entrada.getCdProdutoServicoOperacao()));
        request.setCdProdutoOperacaoRelacionado(PgitUtil.verificaIntegerNulo(entrada.getCdProdutoOperacaoRelacionado()));
        request.setCdBancoPagador(PgitUtil.verificaIntegerNulo(entrada.getCdBancoPagador()));
        request.setCdAgenciaBancariaPagador(PgitUtil.verificaIntegerNulo(entrada.getCdAgenciaBancariaPagador()));
        request.setCdDigitoAgenciaPagador(PgitUtil.verificaStringNula(entrada.getCdDigitoAgenciaPagador()));
        request.setCdContaBancariaPagador(PgitUtil.verificaLongNulo(entrada.getCdContaBancariaPagador()));
        request.setCdDigitoContaPagador(PgitUtil.verificaStringNula(entrada.getCdDigitoContaPagador()));
        request.setCdBancoFavorecido(PgitUtil.verificaIntegerNulo(entrada.getCdBancoFavorecido()));
        request.setCdAgenciaFavorecido(PgitUtil.verificaIntegerNulo(entrada.getCdAgenciaFavorecido()));
        request.setCdDigitoAgenciaFavorecido(PgitUtil.verificaStringNula(entrada.getCdDigitoAgenciaFavorecido()));
        request.setCdContaFavorecido(PgitUtil.verificaLongNulo(entrada.getCdContaFavorecido()));
        request.setCdDigitoContaFavorecido(PgitUtil.verificaStringNula(entrada.getCdDigitoContaFavorecido()));
        request.setDtPagamentoDe(PgitUtil.verificaStringNula(entrada.getDtPagamentoDe()));
        request.setDtPagamentoAte(PgitUtil.verificaStringNula(entrada.getDtPagamentoDe()));

        ListarTipoRetiradaResponse response = factoryAdapter.getListarTipoRetiradaPDCAdapter().invokeProcess(request);
        
        ListarTipoRetiradaSaidaDTO saida = new ListarTipoRetiradaSaidaDTO();
        saida.setCodMensagem(response.getCodMensagem());
        saida.setMensagem(response.getMensagem());
        
        List<ListarTipoRetiradaOcorrenciasSaidaDTO> listaSaida = new ArrayList<ListarTipoRetiradaOcorrenciasSaidaDTO>();
        for (br.com.bradesco.web.pgit.service.data.pdc.listartiporetirada.response.Ocorrencias ocorr : response.getOcorrencias()) {
            ListarTipoRetiradaOcorrenciasSaidaDTO ocorrSaida = new ListarTipoRetiradaOcorrenciasSaidaDTO();
            ocorrSaida.setCdFormaPagamento(ocorr.getCdFormaPagamento());
            ocorrSaida.setDsFormaPagamento(ocorr.getDsFormaPagamento());
            ocorrSaida.setQtPagamento(ocorr.getQtPagamento());
            ocorrSaida.setVlPagamento(ocorr.getVlPagamento());

            listaSaida.add(ocorrSaida);
        }

        saida.setOcorrencias(listaSaida);

        return saida;
    }

	/**
	 * Get: factoryAdapter.
	 *
	 * @return factoryAdapter
	 */
	public FactoryAdapter getFactoryAdapter() {
		return factoryAdapter;
	}

	/**
	 * Set: factoryAdapter.
	 *
	 * @param factoryAdapter the factory adapter
	 */
	public void setFactoryAdapter(FactoryAdapter factoryAdapter) {
		this.factoryAdapter = factoryAdapter;
	}

}