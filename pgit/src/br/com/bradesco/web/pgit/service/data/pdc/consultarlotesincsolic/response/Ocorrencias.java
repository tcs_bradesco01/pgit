/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultarlotesincsolic.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.math.BigDecimal;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _nrLoteInterno
     */
    private long _nrLoteInterno = 0;

    /**
     * keeps track of state for field: _nrLoteInterno
     */
    private boolean _has_nrLoteInterno;

    /**
     * Field _cdTipoLayoutArquivo
     */
    private int _cdTipoLayoutArquivo = 0;

    /**
     * keeps track of state for field: _cdTipoLayoutArquivo
     */
    private boolean _has_cdTipoLayoutArquivo;

    /**
     * Field _dsTipoLayoutArquivo
     */
    private java.lang.String _dsTipoLayoutArquivo;

    /**
     * Field _cdProdutoServicoOperacao
     */
    private int _cdProdutoServicoOperacao = 0;

    /**
     * keeps track of state for field: _cdProdutoServicoOperacao
     */
    private boolean _has_cdProdutoServicoOperacao;

    /**
     * Field _dsProdutoServicoOperacao
     */
    private java.lang.String _dsProdutoServicoOperacao;

    /**
     * Field _qtTotalPagamentoLote
     */
    private long _qtTotalPagamentoLote = 0;

    /**
     * keeps track of state for field: _qtTotalPagamentoLote
     */
    private boolean _has_qtTotalPagamentoLote;

    /**
     * Field _vlTotalPagamentoLote
     */
    private java.math.BigDecimal _vlTotalPagamentoLote = new java.math.BigDecimal("0");

    /**
     * Field _cdIndicadorLoteApto
     */
    private int _cdIndicadorLoteApto = 0;

    /**
     * keeps track of state for field: _cdIndicadorLoteApto
     */
    private boolean _has_cdIndicadorLoteApto;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
        setVlTotalPagamentoLote(new java.math.BigDecimal("0"));
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarlotesincsolic.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdIndicadorLoteApto
     * 
     */
    public void deleteCdIndicadorLoteApto()
    {
        this._has_cdIndicadorLoteApto= false;
    } //-- void deleteCdIndicadorLoteApto() 

    /**
     * Method deleteCdProdutoServicoOperacao
     * 
     */
    public void deleteCdProdutoServicoOperacao()
    {
        this._has_cdProdutoServicoOperacao= false;
    } //-- void deleteCdProdutoServicoOperacao() 

    /**
     * Method deleteCdTipoLayoutArquivo
     * 
     */
    public void deleteCdTipoLayoutArquivo()
    {
        this._has_cdTipoLayoutArquivo= false;
    } //-- void deleteCdTipoLayoutArquivo() 

    /**
     * Method deleteNrLoteInterno
     * 
     */
    public void deleteNrLoteInterno()
    {
        this._has_nrLoteInterno= false;
    } //-- void deleteNrLoteInterno() 

    /**
     * Method deleteQtTotalPagamentoLote
     * 
     */
    public void deleteQtTotalPagamentoLote()
    {
        this._has_qtTotalPagamentoLote= false;
    } //-- void deleteQtTotalPagamentoLote() 

    /**
     * Returns the value of field 'cdIndicadorLoteApto'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorLoteApto'.
     */
    public int getCdIndicadorLoteApto()
    {
        return this._cdIndicadorLoteApto;
    } //-- int getCdIndicadorLoteApto() 

    /**
     * Returns the value of field 'cdProdutoServicoOperacao'.
     * 
     * @return int
     * @return the value of field 'cdProdutoServicoOperacao'.
     */
    public int getCdProdutoServicoOperacao()
    {
        return this._cdProdutoServicoOperacao;
    } //-- int getCdProdutoServicoOperacao() 

    /**
     * Returns the value of field 'cdTipoLayoutArquivo'.
     * 
     * @return int
     * @return the value of field 'cdTipoLayoutArquivo'.
     */
    public int getCdTipoLayoutArquivo()
    {
        return this._cdTipoLayoutArquivo;
    } //-- int getCdTipoLayoutArquivo() 

    /**
     * Returns the value of field 'dsProdutoServicoOperacao'.
     * 
     * @return String
     * @return the value of field 'dsProdutoServicoOperacao'.
     */
    public java.lang.String getDsProdutoServicoOperacao()
    {
        return this._dsProdutoServicoOperacao;
    } //-- java.lang.String getDsProdutoServicoOperacao() 

    /**
     * Returns the value of field 'dsTipoLayoutArquivo'.
     * 
     * @return String
     * @return the value of field 'dsTipoLayoutArquivo'.
     */
    public java.lang.String getDsTipoLayoutArquivo()
    {
        return this._dsTipoLayoutArquivo;
    } //-- java.lang.String getDsTipoLayoutArquivo() 

    /**
     * Returns the value of field 'nrLoteInterno'.
     * 
     * @return long
     * @return the value of field 'nrLoteInterno'.
     */
    public long getNrLoteInterno()
    {
        return this._nrLoteInterno;
    } //-- long getNrLoteInterno() 

    /**
     * Returns the value of field 'qtTotalPagamentoLote'.
     * 
     * @return long
     * @return the value of field 'qtTotalPagamentoLote'.
     */
    public long getQtTotalPagamentoLote()
    {
        return this._qtTotalPagamentoLote;
    } //-- long getQtTotalPagamentoLote() 

    /**
     * Returns the value of field 'vlTotalPagamentoLote'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlTotalPagamentoLote'.
     */
    public java.math.BigDecimal getVlTotalPagamentoLote()
    {
        return this._vlTotalPagamentoLote;
    } //-- java.math.BigDecimal getVlTotalPagamentoLote() 

    /**
     * Method hasCdIndicadorLoteApto
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorLoteApto()
    {
        return this._has_cdIndicadorLoteApto;
    } //-- boolean hasCdIndicadorLoteApto() 

    /**
     * Method hasCdProdutoServicoOperacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdutoServicoOperacao()
    {
        return this._has_cdProdutoServicoOperacao;
    } //-- boolean hasCdProdutoServicoOperacao() 

    /**
     * Method hasCdTipoLayoutArquivo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoLayoutArquivo()
    {
        return this._has_cdTipoLayoutArquivo;
    } //-- boolean hasCdTipoLayoutArquivo() 

    /**
     * Method hasNrLoteInterno
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrLoteInterno()
    {
        return this._has_nrLoteInterno;
    } //-- boolean hasNrLoteInterno() 

    /**
     * Method hasQtTotalPagamentoLote
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtTotalPagamentoLote()
    {
        return this._has_qtTotalPagamentoLote;
    } //-- boolean hasQtTotalPagamentoLote() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdIndicadorLoteApto'.
     * 
     * @param cdIndicadorLoteApto the value of field
     * 'cdIndicadorLoteApto'.
     */
    public void setCdIndicadorLoteApto(int cdIndicadorLoteApto)
    {
        this._cdIndicadorLoteApto = cdIndicadorLoteApto;
        this._has_cdIndicadorLoteApto = true;
    } //-- void setCdIndicadorLoteApto(int) 

    /**
     * Sets the value of field 'cdProdutoServicoOperacao'.
     * 
     * @param cdProdutoServicoOperacao the value of field
     * 'cdProdutoServicoOperacao'.
     */
    public void setCdProdutoServicoOperacao(int cdProdutoServicoOperacao)
    {
        this._cdProdutoServicoOperacao = cdProdutoServicoOperacao;
        this._has_cdProdutoServicoOperacao = true;
    } //-- void setCdProdutoServicoOperacao(int) 

    /**
     * Sets the value of field 'cdTipoLayoutArquivo'.
     * 
     * @param cdTipoLayoutArquivo the value of field
     * 'cdTipoLayoutArquivo'.
     */
    public void setCdTipoLayoutArquivo(int cdTipoLayoutArquivo)
    {
        this._cdTipoLayoutArquivo = cdTipoLayoutArquivo;
        this._has_cdTipoLayoutArquivo = true;
    } //-- void setCdTipoLayoutArquivo(int) 

    /**
     * Sets the value of field 'dsProdutoServicoOperacao'.
     * 
     * @param dsProdutoServicoOperacao the value of field
     * 'dsProdutoServicoOperacao'.
     */
    public void setDsProdutoServicoOperacao(java.lang.String dsProdutoServicoOperacao)
    {
        this._dsProdutoServicoOperacao = dsProdutoServicoOperacao;
    } //-- void setDsProdutoServicoOperacao(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoLayoutArquivo'.
     * 
     * @param dsTipoLayoutArquivo the value of field
     * 'dsTipoLayoutArquivo'.
     */
    public void setDsTipoLayoutArquivo(java.lang.String dsTipoLayoutArquivo)
    {
        this._dsTipoLayoutArquivo = dsTipoLayoutArquivo;
    } //-- void setDsTipoLayoutArquivo(java.lang.String) 

    /**
     * Sets the value of field 'nrLoteInterno'.
     * 
     * @param nrLoteInterno the value of field 'nrLoteInterno'.
     */
    public void setNrLoteInterno(long nrLoteInterno)
    {
        this._nrLoteInterno = nrLoteInterno;
        this._has_nrLoteInterno = true;
    } //-- void setNrLoteInterno(long) 

    /**
     * Sets the value of field 'qtTotalPagamentoLote'.
     * 
     * @param qtTotalPagamentoLote the value of field
     * 'qtTotalPagamentoLote'.
     */
    public void setQtTotalPagamentoLote(long qtTotalPagamentoLote)
    {
        this._qtTotalPagamentoLote = qtTotalPagamentoLote;
        this._has_qtTotalPagamentoLote = true;
    } //-- void setQtTotalPagamentoLote(long) 

    /**
     * Sets the value of field 'vlTotalPagamentoLote'.
     * 
     * @param vlTotalPagamentoLote the value of field
     * 'vlTotalPagamentoLote'.
     */
    public void setVlTotalPagamentoLote(java.math.BigDecimal vlTotalPagamentoLote)
    {
        this._vlTotalPagamentoLote = vlTotalPagamentoLote;
    } //-- void setVlTotalPagamentoLote(java.math.BigDecimal) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultarlotesincsolic.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultarlotesincsolic.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultarlotesincsolic.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarlotesincsolic.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
