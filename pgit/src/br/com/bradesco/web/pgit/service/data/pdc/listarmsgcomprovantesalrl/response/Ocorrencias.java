/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listarmsgcomprovantesalrl.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdControleMensagemSalarial
     */
    private int _cdControleMensagemSalarial = 0;

    /**
     * keeps track of state for field: _cdControleMensagemSalarial
     */
    private boolean _has_cdControleMensagemSalarial;

    /**
     * Field _cdTipoComprovanteSalarial
     */
    private int _cdTipoComprovanteSalarial = 0;

    /**
     * keeps track of state for field: _cdTipoComprovanteSalarial
     */
    private boolean _has_cdTipoComprovanteSalarial;

    /**
     * Field _dtTipoComprovanteSalarial
     */
    private java.lang.String _dtTipoComprovanteSalarial;

    /**
     * Field _cdTipoMensagemSalarial
     */
    private int _cdTipoMensagemSalarial = 0;

    /**
     * keeps track of state for field: _cdTipoMensagemSalarial
     */
    private boolean _has_cdTipoMensagemSalarial;

    /**
     * Field _dsControleMensagemSalarial
     */
    private java.lang.String _dsControleMensagemSalarial;

    /**
     * Field _cdRestMensagemSalarial
     */
    private int _cdRestMensagemSalarial = 0;

    /**
     * keeps track of state for field: _cdRestMensagemSalarial
     */
    private boolean _has_cdRestMensagemSalarial;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarmsgcomprovantesalrl.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdControleMensagemSalarial
     * 
     */
    public void deleteCdControleMensagemSalarial()
    {
        this._has_cdControleMensagemSalarial= false;
    } //-- void deleteCdControleMensagemSalarial() 

    /**
     * Method deleteCdRestMensagemSalarial
     * 
     */
    public void deleteCdRestMensagemSalarial()
    {
        this._has_cdRestMensagemSalarial= false;
    } //-- void deleteCdRestMensagemSalarial() 

    /**
     * Method deleteCdTipoComprovanteSalarial
     * 
     */
    public void deleteCdTipoComprovanteSalarial()
    {
        this._has_cdTipoComprovanteSalarial= false;
    } //-- void deleteCdTipoComprovanteSalarial() 

    /**
     * Method deleteCdTipoMensagemSalarial
     * 
     */
    public void deleteCdTipoMensagemSalarial()
    {
        this._has_cdTipoMensagemSalarial= false;
    } //-- void deleteCdTipoMensagemSalarial() 

    /**
     * Returns the value of field 'cdControleMensagemSalarial'.
     * 
     * @return int
     * @return the value of field 'cdControleMensagemSalarial'.
     */
    public int getCdControleMensagemSalarial()
    {
        return this._cdControleMensagemSalarial;
    } //-- int getCdControleMensagemSalarial() 

    /**
     * Returns the value of field 'cdRestMensagemSalarial'.
     * 
     * @return int
     * @return the value of field 'cdRestMensagemSalarial'.
     */
    public int getCdRestMensagemSalarial()
    {
        return this._cdRestMensagemSalarial;
    } //-- int getCdRestMensagemSalarial() 

    /**
     * Returns the value of field 'cdTipoComprovanteSalarial'.
     * 
     * @return int
     * @return the value of field 'cdTipoComprovanteSalarial'.
     */
    public int getCdTipoComprovanteSalarial()
    {
        return this._cdTipoComprovanteSalarial;
    } //-- int getCdTipoComprovanteSalarial() 

    /**
     * Returns the value of field 'cdTipoMensagemSalarial'.
     * 
     * @return int
     * @return the value of field 'cdTipoMensagemSalarial'.
     */
    public int getCdTipoMensagemSalarial()
    {
        return this._cdTipoMensagemSalarial;
    } //-- int getCdTipoMensagemSalarial() 

    /**
     * Returns the value of field 'dsControleMensagemSalarial'.
     * 
     * @return String
     * @return the value of field 'dsControleMensagemSalarial'.
     */
    public java.lang.String getDsControleMensagemSalarial()
    {
        return this._dsControleMensagemSalarial;
    } //-- java.lang.String getDsControleMensagemSalarial() 

    /**
     * Returns the value of field 'dtTipoComprovanteSalarial'.
     * 
     * @return String
     * @return the value of field 'dtTipoComprovanteSalarial'.
     */
    public java.lang.String getDtTipoComprovanteSalarial()
    {
        return this._dtTipoComprovanteSalarial;
    } //-- java.lang.String getDtTipoComprovanteSalarial() 

    /**
     * Method hasCdControleMensagemSalarial
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdControleMensagemSalarial()
    {
        return this._has_cdControleMensagemSalarial;
    } //-- boolean hasCdControleMensagemSalarial() 

    /**
     * Method hasCdRestMensagemSalarial
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdRestMensagemSalarial()
    {
        return this._has_cdRestMensagemSalarial;
    } //-- boolean hasCdRestMensagemSalarial() 

    /**
     * Method hasCdTipoComprovanteSalarial
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoComprovanteSalarial()
    {
        return this._has_cdTipoComprovanteSalarial;
    } //-- boolean hasCdTipoComprovanteSalarial() 

    /**
     * Method hasCdTipoMensagemSalarial
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoMensagemSalarial()
    {
        return this._has_cdTipoMensagemSalarial;
    } //-- boolean hasCdTipoMensagemSalarial() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdControleMensagemSalarial'.
     * 
     * @param cdControleMensagemSalarial the value of field
     * 'cdControleMensagemSalarial'.
     */
    public void setCdControleMensagemSalarial(int cdControleMensagemSalarial)
    {
        this._cdControleMensagemSalarial = cdControleMensagemSalarial;
        this._has_cdControleMensagemSalarial = true;
    } //-- void setCdControleMensagemSalarial(int) 

    /**
     * Sets the value of field 'cdRestMensagemSalarial'.
     * 
     * @param cdRestMensagemSalarial the value of field
     * 'cdRestMensagemSalarial'.
     */
    public void setCdRestMensagemSalarial(int cdRestMensagemSalarial)
    {
        this._cdRestMensagemSalarial = cdRestMensagemSalarial;
        this._has_cdRestMensagemSalarial = true;
    } //-- void setCdRestMensagemSalarial(int) 

    /**
     * Sets the value of field 'cdTipoComprovanteSalarial'.
     * 
     * @param cdTipoComprovanteSalarial the value of field
     * 'cdTipoComprovanteSalarial'.
     */
    public void setCdTipoComprovanteSalarial(int cdTipoComprovanteSalarial)
    {
        this._cdTipoComprovanteSalarial = cdTipoComprovanteSalarial;
        this._has_cdTipoComprovanteSalarial = true;
    } //-- void setCdTipoComprovanteSalarial(int) 

    /**
     * Sets the value of field 'cdTipoMensagemSalarial'.
     * 
     * @param cdTipoMensagemSalarial the value of field
     * 'cdTipoMensagemSalarial'.
     */
    public void setCdTipoMensagemSalarial(int cdTipoMensagemSalarial)
    {
        this._cdTipoMensagemSalarial = cdTipoMensagemSalarial;
        this._has_cdTipoMensagemSalarial = true;
    } //-- void setCdTipoMensagemSalarial(int) 

    /**
     * Sets the value of field 'dsControleMensagemSalarial'.
     * 
     * @param dsControleMensagemSalarial the value of field
     * 'dsControleMensagemSalarial'.
     */
    public void setDsControleMensagemSalarial(java.lang.String dsControleMensagemSalarial)
    {
        this._dsControleMensagemSalarial = dsControleMensagemSalarial;
    } //-- void setDsControleMensagemSalarial(java.lang.String) 

    /**
     * Sets the value of field 'dtTipoComprovanteSalarial'.
     * 
     * @param dtTipoComprovanteSalarial the value of field
     * 'dtTipoComprovanteSalarial'.
     */
    public void setDtTipoComprovanteSalarial(java.lang.String dtTipoComprovanteSalarial)
    {
        this._dtTipoComprovanteSalarial = dtTipoComprovanteSalarial;
    } //-- void setDtTipoComprovanteSalarial(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listarmsgcomprovantesalrl.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listarmsgcomprovantesalrl.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listarmsgcomprovantesalrl.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarmsgcomprovantesalrl.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
