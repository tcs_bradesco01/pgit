/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantercontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantercontrato.bean;


/**
 * Nome: ListarConManTarifaSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarConManTarifaSaidaDTO {
	
	/** Atributo codMensagem. */
	private String codMensagem;
	
	/** Atributo mensagem. */
	private String mensagem;
	
	/** Atributo codTipoServico. */
	private int codTipoServico;
	
	/** Atributo codModalidadeServico. */
	private int codModalidadeServico;
	
	/** Atributo codOperacaoModalidadeServico. */
	private int codOperacaoModalidadeServico;
	
	/** Atributo hrInclusaoRegistroHistorico. */
	private String hrInclusaoRegistroHistorico;
	
	/** Atributo dsServico. */
	private String dsServico;
	
	/** Atributo dsModalidade. */
	private String dsModalidade;
	
	/** Atributo dsOperacao. */
	private String dsOperacao;
	
	/** Atributo dtInicioVigencia. */
	private String dtInicioVigencia;	
	
	/** Atributo dtManutencao. */
	private String dtManutencao;
	
	/** Atributo hrManutencao. */
	private String hrManutencao;
	
	/** Atributo cdUsuario. */
	private String cdUsuario;
	
	/** Atributo dtManutencaoFormatada. */
	private String dtManutencaoFormatada;
	
	/** Atributo dsTipoManutencao. */
	private String dsTipoManutencao;

	/**
	 * Get: dtHrManutFormatada.
	 *
	 * @return dtHrManutFormatada
	 */
	public String getDtHrManutFormatada() {
		return dtManutencaoFormatada + " - " + hrManutencao;
	}
	
	/**
	 * Get: dsTipoManutencao.
	 *
	 * @return dsTipoManutencao
	 */
	public String getDsTipoManutencao() {
		return dsTipoManutencao;
	}
	
	/**
	 * Set: dsTipoManutencao.
	 *
	 * @param dsTipoManutencao the ds tipo manutencao
	 */
	public void setDsTipoManutencao(String dsTipoManutencao) {
		this.dsTipoManutencao = dsTipoManutencao;
	}
	
	/**
	 * Get: dtManutencaoFormatada.
	 *
	 * @return dtManutencaoFormatada
	 */
	public String getDtManutencaoFormatada() {
		return dtManutencaoFormatada;
	}
	
	/**
	 * Set: dtManutencaoFormatada.
	 *
	 * @param dtManutencaoFormatada the dt manutencao formatada
	 */
	public void setDtManutencaoFormatada(String dtManutencaoFormatada) {
		this.dtManutencaoFormatada = dtManutencaoFormatada;
	}
	
	/**
	 * Get: cdUsuario.
	 *
	 * @return cdUsuario
	 */
	public String getCdUsuario() {
		return cdUsuario;
	}
	
	/**
	 * Set: cdUsuario.
	 *
	 * @param cdUsuario the cd usuario
	 */
	public void setCdUsuario(String cdUsuario) {
		this.cdUsuario = cdUsuario;
	}
	
	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}
	
	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}
	
	/**
	 * Get: dsModalidade.
	 *
	 * @return dsModalidade
	 */
	public String getDsModalidade() {
		return dsModalidade;
	}
	
	/**
	 * Set: dsModalidade.
	 *
	 * @param dsModalidade the ds modalidade
	 */
	public void setDsModalidade(String dsModalidade) {
		this.dsModalidade = dsModalidade;
	}
	
	/**
	 * Get: dsOperacao.
	 *
	 * @return dsOperacao
	 */
	public String getDsOperacao() {
		return dsOperacao;
	}
	
	/**
	 * Set: dsOperacao.
	 *
	 * @param dsOperacao the ds operacao
	 */
	public void setDsOperacao(String dsOperacao) {
		this.dsOperacao = dsOperacao;
	}
	
	/**
	 * Get: dsServico.
	 *
	 * @return dsServico
	 */
	public String getDsServico() {
		return dsServico;
	}
	
	/**
	 * Set: dsServico.
	 *
	 * @param dsServico the ds servico
	 */
	public void setDsServico(String dsServico) {
		this.dsServico = dsServico;
	}
	
	/**
	 * Get: dtInicioVigencia.
	 *
	 * @return dtInicioVigencia
	 */
	public String getDtInicioVigencia() {
		return dtInicioVigencia;
	}
	
	/**
	 * Set: dtInicioVigencia.
	 *
	 * @param dtInicioVigencia the dt inicio vigencia
	 */
	public void setDtInicioVigencia(String dtInicioVigencia) {
		this.dtInicioVigencia = dtInicioVigencia;
	}
	
	/**
	 * Get: dtManutencao.
	 *
	 * @return dtManutencao
	 */
	public String getDtManutencao() {
		return dtManutencao;
	}
	
	/**
	 * Set: dtManutencao.
	 *
	 * @param dtManutencao the dt manutencao
	 */
	public void setDtManutencao(String dtManutencao) {
		this.dtManutencao = dtManutencao;
	}
	
	/**
	 * Get: hrInclusaoRegistroHistorico.
	 *
	 * @return hrInclusaoRegistroHistorico
	 */
	public String getHrInclusaoRegistroHistorico() {
		return hrInclusaoRegistroHistorico;
	}
	
	/**
	 * Set: hrInclusaoRegistroHistorico.
	 *
	 * @param hrInclusaoRegistroHistorico the hr inclusao registro historico
	 */
	public void setHrInclusaoRegistroHistorico(String hrInclusaoRegistroHistorico) {
		this.hrInclusaoRegistroHistorico = hrInclusaoRegistroHistorico;
	}
	
	/**
	 * Get: hrManutencao.
	 *
	 * @return hrManutencao
	 */
	public String getHrManutencao() {
		return hrManutencao;
	}
	
	/**
	 * Set: hrManutencao.
	 *
	 * @param hrManutencao the hr manutencao
	 */
	public void setHrManutencao(String hrManutencao) {
		this.hrManutencao = hrManutencao;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	/**
	 * Get: codModalidadeServico.
	 *
	 * @return codModalidadeServico
	 */
	public int getCodModalidadeServico() {
		return codModalidadeServico;
	}
	
	/**
	 * Set: codModalidadeServico.
	 *
	 * @param codModalidadeServico the cod modalidade servico
	 */
	public void setCodModalidadeServico(int codModalidadeServico) {
		this.codModalidadeServico = codModalidadeServico;
	}
	
	/**
	 * Get: codOperacaoModalidadeServico.
	 *
	 * @return codOperacaoModalidadeServico
	 */
	public int getCodOperacaoModalidadeServico() {
		return codOperacaoModalidadeServico;
	}
	
	/**
	 * Set: codOperacaoModalidadeServico.
	 *
	 * @param codOperacaoModalidadeServico the cod operacao modalidade servico
	 */
	public void setCodOperacaoModalidadeServico(int codOperacaoModalidadeServico) {
		this.codOperacaoModalidadeServico = codOperacaoModalidadeServico;
	}
	
	/**
	 * Get: codTipoServico.
	 *
	 * @return codTipoServico
	 */
	public int getCodTipoServico() {
		return codTipoServico;
	}
	
	/**
	 * Set: codTipoServico.
	 *
	 * @param codTipoServico the cod tipo servico
	 */
	public void setCodTipoServico(int codTipoServico) {
		this.codTipoServico = codTipoServico;
	}
	
	

}
