/*
 * Nome: br.com.bradesco.web.pgit.service.business.finalidadeendprodutoserv.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.finalidadeendprodutoserv.bean;

/**
 * Nome: FinalidadeEndProdutoServDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class FinalidadeEndProdutoServDTO {
	
	/** Atributo finalidadeEnd. */
	private String finalidadeEnd;
	
	/** Atributo tpoServico. */
	private String tpoServico;
	
	/** Atributo modalidadeServ. */
	private String modalidadeServ;
	
	
	/**
	 * Get: finalidadeEnd.
	 *
	 * @return finalidadeEnd
	 */
	public String getFinalidadeEnd() {
		return finalidadeEnd;
	}
	
	/**
	 * Set: finalidadeEnd.
	 *
	 * @param finalidadeEnd the finalidade end
	 */
	public void setFinalidadeEnd(String finalidadeEnd) {
		this.finalidadeEnd = finalidadeEnd;
	}
	
	/**
	 * Get: modalidadeServ.
	 *
	 * @return modalidadeServ
	 */
	public String getModalidadeServ() {
		return modalidadeServ;
	}
	
	/**
	 * Set: modalidadeServ.
	 *
	 * @param modalidadeServ the modalidade serv
	 */
	public void setModalidadeServ(String modalidadeServ) {
		this.modalidadeServ = modalidadeServ;
	}
	
	/**
	 * Get: tpoServico.
	 *
	 * @return tpoServico
	 */
	public String getTpoServico() {
		return tpoServico;
	}
	
	/**
	 * Set: tpoServico.
	 *
	 * @param tpoServico the tpo servico
	 */
	public void setTpoServico(String tpoServico) {
		this.tpoServico = tpoServico;
	}
}
