/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantervinculoempresasalario.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantervinculoempresasalario.bean;

/**
 * Nome: ExcluirVinculoCtaSalarioEmpresaEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ExcluirVinculoCtaSalarioEmpresaEntradaDTO {
	
	/** Atributo cdPessoa. */
	private Long cdPessoa;
    
    /** Atributo cdTipoPessoa. */
    private Integer cdTipoPessoa;
    
    /** Atributo nrSeqContrato. */
    private Long nrSeqContrato;
    
    /** Atributo cdPessoVinc. */
    private Long cdPessoVinc;
    
    /** Atributo cdTipoContratoVinc. */
    private Integer cdTipoContratoVinc;
    
    /** Atributo nrSeqContratoVinc. */
    private Long nrSeqContratoVinc;
    
    /** Atributo cdTipoVincContrato. */
    private Integer cdTipoVincContrato;

    
	/**
	 * Excluir vinculo cta salario empresa entrada dto.
	 */
	public ExcluirVinculoCtaSalarioEmpresaEntradaDTO() {
		super();
	}


	/**
	 * Get: cdPessoa.
	 *
	 * @return cdPessoa
	 */
	public Long getCdPessoa() {
		return cdPessoa;
	}


	/**
	 * Set: cdPessoa.
	 *
	 * @param cdPessoa the cd pessoa
	 */
	public void setCdPessoa(Long cdPessoa) {
		this.cdPessoa = cdPessoa;
	}


	/**
	 * Get: cdPessoVinc.
	 *
	 * @return cdPessoVinc
	 */
	public Long getCdPessoVinc() {
		return cdPessoVinc;
	}


	/**
	 * Set: cdPessoVinc.
	 *
	 * @param cdPessoVinc the cd pesso vinc
	 */
	public void setCdPessoVinc(Long cdPessoVinc) {
		this.cdPessoVinc = cdPessoVinc;
	}


	/**
	 * Get: cdTipoContratoVinc.
	 *
	 * @return cdTipoContratoVinc
	 */
	public Integer getCdTipoContratoVinc() {
		return cdTipoContratoVinc;
	}


	/**
	 * Set: cdTipoContratoVinc.
	 *
	 * @param cdTipoContratoVinc the cd tipo contrato vinc
	 */
	public void setCdTipoContratoVinc(Integer cdTipoContratoVinc) {
		this.cdTipoContratoVinc = cdTipoContratoVinc;
	}


	/**
	 * Get: cdTipoPessoa.
	 *
	 * @return cdTipoPessoa
	 */
	public Integer getCdTipoPessoa() {
		return cdTipoPessoa;
	}


	/**
	 * Set: cdTipoPessoa.
	 *
	 * @param cdTipoPessoa the cd tipo pessoa
	 */
	public void setCdTipoPessoa(Integer cdTipoPessoa) {
		this.cdTipoPessoa = cdTipoPessoa;
	}


	/**
	 * Get: cdTipoVincContrato.
	 *
	 * @return cdTipoVincContrato
	 */
	public Integer getCdTipoVincContrato() {
		return cdTipoVincContrato;
	}


	/**
	 * Set: cdTipoVincContrato.
	 *
	 * @param cdTipoVincContrato the cd tipo vinc contrato
	 */
	public void setCdTipoVincContrato(Integer cdTipoVincContrato) {
		this.cdTipoVincContrato = cdTipoVincContrato;
	}


	/**
	 * Get: nrSeqContrato.
	 *
	 * @return nrSeqContrato
	 */
	public Long getNrSeqContrato() {
		return nrSeqContrato;
	}


	/**
	 * Set: nrSeqContrato.
	 *
	 * @param nrSeqContrato the nr seq contrato
	 */
	public void setNrSeqContrato(Long nrSeqContrato) {
		this.nrSeqContrato = nrSeqContrato;
	}


	/**
	 * Get: nrSeqContratoVinc.
	 *
	 * @return nrSeqContratoVinc
	 */
	public Long getNrSeqContratoVinc() {
		return nrSeqContratoVinc;
	}


	/**
	 * Set: nrSeqContratoVinc.
	 *
	 * @param nrSeqContratoVinc the nr seq contrato vinc
	 */
	public void setNrSeqContratoVinc(Long nrSeqContratoVinc) {
		this.nrSeqContratoVinc = nrSeqContratoVinc;
	}
		
	

}
