/*
 * Nome: br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean;

/**
 * Nome: DetalharPagtoCreditoContaEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class DetalharPagtoCreditoContaEntradaDTO {	
	
	/** Atributo cdPessoaJuridicaContrato. */
	private long cdPessoaJuridicaContrato;
	
	/** Atributo cdTipoContratoNegocio. */
	private Integer cdTipoContratoNegocio;
	
	/** Atributo nrSequenciaContratoNegocio. */
	private long nrSequenciaContratoNegocio;
	
	/** Atributo dsEfetivacaoPagamento. */
	private String dsEfetivacaoPagamento;
	
	/** Atributo cdControlePagamento. */
	private String cdControlePagamento;
	
	/** Atributo cdBancoDebito. */
	private Integer cdBancoDebito;
	
	/** Atributo cdAgenciaDebito. */
	private Integer cdAgenciaDebito;
	
	/** Atributo cdContaDebito. */
	private long cdContaDebito;
	
	/** Atributo cdBancoCredito. */
	private Integer cdBancoCredito;
	
	/** Atributo cdAgenciaCredito. */
	private Integer cdAgenciaCredito;
	
	/** Atributo cdContaCredito. */
	private long cdContaCredito;
	
	/** Atributo cdAgendadosPagoNaoPago. */
	private Integer cdAgendadosPagoNaoPago;
	
	/** Atributo cdProdutoServicoRelacionado. */
	private Integer cdProdutoServicoRelacionado;
	
	/** Atributo dtHoraManutencao. */
	private String dtHoraManutencao;

	/**
	 * Get: cdAgenciaCredito.
	 *
	 * @return cdAgenciaCredito
	 */
	public Integer getCdAgenciaCredito() {
		return cdAgenciaCredito;
	}
	
	/**
	 * Set: cdAgenciaCredito.
	 *
	 * @param cdAgenciaCredito the cd agencia credito
	 */
	public void setCdAgenciaCredito(Integer cdAgenciaCredito) {
		this.cdAgenciaCredito = cdAgenciaCredito;
	}
	
	/**
	 * Get: cdAgenciaDebito.
	 *
	 * @return cdAgenciaDebito
	 */
	public Integer getCdAgenciaDebito() {
		return cdAgenciaDebito;
	}
	
	/**
	 * Set: cdAgenciaDebito.
	 *
	 * @param cdAgenciaDebito the cd agencia debito
	 */
	public void setCdAgenciaDebito(Integer cdAgenciaDebito) {
		this.cdAgenciaDebito = cdAgenciaDebito;
	}
	
	/**
	 * Get: cdAgendadosPagoNaoPago.
	 *
	 * @return cdAgendadosPagoNaoPago
	 */
	public Integer getCdAgendadosPagoNaoPago() {
		return cdAgendadosPagoNaoPago;
	}
	
	/**
	 * Set: cdAgendadosPagoNaoPago.
	 *
	 * @param cdAgendadosPagoNaoPago the cd agendados pago nao pago
	 */
	public void setCdAgendadosPagoNaoPago(Integer cdAgendadosPagoNaoPago) {
		this.cdAgendadosPagoNaoPago = cdAgendadosPagoNaoPago;
	}
	
	/**
	 * Get: cdBancoCredito.
	 *
	 * @return cdBancoCredito
	 */
	public Integer getCdBancoCredito() {
		return cdBancoCredito;
	}
	
	/**
	 * Set: cdBancoCredito.
	 *
	 * @param cdBancoCredito the cd banco credito
	 */
	public void setCdBancoCredito(Integer cdBancoCredito) {
		this.cdBancoCredito = cdBancoCredito;
	}
	
	/**
	 * Get: cdBancoDebito.
	 *
	 * @return cdBancoDebito
	 */
	public Integer getCdBancoDebito() {
		return cdBancoDebito;
	}
	
	/**
	 * Set: cdBancoDebito.
	 *
	 * @param cdBancoDebito the cd banco debito
	 */
	public void setCdBancoDebito(Integer cdBancoDebito) {
		this.cdBancoDebito = cdBancoDebito;
	}
	
	/**
	 * Get: cdContaCredito.
	 *
	 * @return cdContaCredito
	 */
	public long getCdContaCredito() {
		return cdContaCredito;
	}
	
	/**
	 * Set: cdContaCredito.
	 *
	 * @param cdContaCredito the cd conta credito
	 */
	public void setCdContaCredito(long cdContaCredito) {
		this.cdContaCredito = cdContaCredito;
	}
	
	/**
	 * Get: cdContaDebito.
	 *
	 * @return cdContaDebito
	 */
	public long getCdContaDebito() {
		return cdContaDebito;
	}
	
	/**
	 * Set: cdContaDebito.
	 *
	 * @param cdContaDebito the cd conta debito
	 */
	public void setCdContaDebito(long cdContaDebito) {
		this.cdContaDebito = cdContaDebito;
	}
	
	/**
	 * Get: cdControlePagamento.
	 *
	 * @return cdControlePagamento
	 */
	public String getCdControlePagamento() {
		return cdControlePagamento;
	}
	
	/**
	 * Set: cdControlePagamento.
	 *
	 * @param cdControlePagamento the cd controle pagamento
	 */
	public void setCdControlePagamento(String cdControlePagamento) {
		this.cdControlePagamento = cdControlePagamento;
	}
	
	/**
	 * Get: cdPessoaJuridicaContrato.
	 *
	 * @return cdPessoaJuridicaContrato
	 */
	public long getCdPessoaJuridicaContrato() {
		return cdPessoaJuridicaContrato;
	}
	
	/**
	 * Set: cdPessoaJuridicaContrato.
	 *
	 * @param cdPessoaJuridicaContrato the cd pessoa juridica contrato
	 */
	public void setCdPessoaJuridicaContrato(long cdPessoaJuridicaContrato) {
		this.cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
	}
	
	/**
	 * Get: cdProdutoServicoRelacionado.
	 *
	 * @return cdProdutoServicoRelacionado
	 */
	public Integer getCdProdutoServicoRelacionado() {
		return cdProdutoServicoRelacionado;
	}
	
	/**
	 * Set: cdProdutoServicoRelacionado.
	 *
	 * @param cdProdutoServicoRelacionado the cd produto servico relacionado
	 */
	public void setCdProdutoServicoRelacionado(Integer cdProdutoServicoRelacionado) {
		this.cdProdutoServicoRelacionado = cdProdutoServicoRelacionado;
	}
	
	/**
	 * Get: cdTipoContratoNegocio.
	 *
	 * @return cdTipoContratoNegocio
	 */
	public Integer getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}
	
	/**
	 * Set: cdTipoContratoNegocio.
	 *
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}
	
	/**
	 * Get: dtHoraManutencao.
	 *
	 * @return dtHoraManutencao
	 */
	public String getDtHoraManutencao() {
		return dtHoraManutencao;
	}
	
	/**
	 * Set: dtHoraManutencao.
	 *
	 * @param dtHoraManutencao the dt hora manutencao
	 */
	public void setDtHoraManutencao(String dtHoraManutencao) {
		this.dtHoraManutencao = dtHoraManutencao;
	}
	
	/**
	 * Get: nrSequenciaContratoNegocio.
	 *
	 * @return nrSequenciaContratoNegocio
	 */
	public long getNrSequenciaContratoNegocio() {
		return nrSequenciaContratoNegocio;
	}
	
	/**
	 * Set: nrSequenciaContratoNegocio.
	 *
	 * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
	 */
	public void setNrSequenciaContratoNegocio(long nrSequenciaContratoNegocio) {
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}
	
	/**
	 * Get: dsEfetivacaoPagamento.
	 *
	 * @return dsEfetivacaoPagamento
	 */
	public String getDsEfetivacaoPagamento() {
		return dsEfetivacaoPagamento;
	}
	
	/**
	 * Set: dsEfetivacaoPagamento.
	 *
	 * @param dsEfetivacaoPagamento the ds efetivacao pagamento
	 */
	public void setDsEfetivacaoPagamento(String dsEfetivacaoPagamento) {
		this.dsEfetivacaoPagamento = dsEfetivacaoPagamento;
	}
	
	
}
