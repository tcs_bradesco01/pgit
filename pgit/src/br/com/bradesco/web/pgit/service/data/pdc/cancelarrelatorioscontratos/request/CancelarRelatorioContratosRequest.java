/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.cancelarrelatorioscontratos.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;

/**
 * Class CancelarRelatorioContratosRequest.
 * 
 * @version $Revision$ $Date$
 */
public class CancelarRelatorioContratosRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdSolicitacaoPagamentoIntegrado
     */
    private int _cdSolicitacaoPagamentoIntegrado = 0;

    /**
     * keeps track of state for field:
     * _cdSolicitacaoPagamentoIntegrado
     */
    private boolean _has_cdSolicitacaoPagamentoIntegrado;

    /**
     * Field _nrSolicitacaoPagamentoIntegrado
     */
    private int _nrSolicitacaoPagamentoIntegrado = 0;

    /**
     * keeps track of state for field:
     * _nrSolicitacaoPagamentoIntegrado
     */
    private boolean _has_nrSolicitacaoPagamentoIntegrado;

    /**
     * Field _cdTipoRelatPagamento
     */
    private int _cdTipoRelatPagamento = 0;

    /**
     * keeps track of state for field: _cdTipoRelatPagamento
     */
    private boolean _has_cdTipoRelatPagamento;

    /**
     * Field _dtInicio
     */
    private java.lang.String _dtInicio;

    /**
     * Field _dtFim
     */
    private java.lang.String _dtFim;

    /**
     * Field _qtOcorrencias
     */
    private int _qtOcorrencias = 0;

    /**
     * keeps track of state for field: _qtOcorrencias
     */
    private boolean _has_qtOcorrencias;


      //----------------/
     //- Constructors -/
    //----------------/

    public CancelarRelatorioContratosRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.cancelarrelatorioscontratos.request.CancelarRelatorioContratosRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdSolicitacaoPagamentoIntegrado
     * 
     */
    public void deleteCdSolicitacaoPagamentoIntegrado()
    {
        this._has_cdSolicitacaoPagamentoIntegrado= false;
    } //-- void deleteCdSolicitacaoPagamentoIntegrado() 

    /**
     * Method deleteCdTipoRelatPagamento
     * 
     */
    public void deleteCdTipoRelatPagamento()
    {
        this._has_cdTipoRelatPagamento= false;
    } //-- void deleteCdTipoRelatPagamento() 

    /**
     * Method deleteNrSolicitacaoPagamentoIntegrado
     * 
     */
    public void deleteNrSolicitacaoPagamentoIntegrado()
    {
        this._has_nrSolicitacaoPagamentoIntegrado= false;
    } //-- void deleteNrSolicitacaoPagamentoIntegrado() 

    /**
     * Method deleteQtOcorrencias
     * 
     */
    public void deleteQtOcorrencias()
    {
        this._has_qtOcorrencias= false;
    } //-- void deleteQtOcorrencias() 

    /**
     * Returns the value of field
     * 'cdSolicitacaoPagamentoIntegrado'.
     * 
     * @return int
     * @return the value of field 'cdSolicitacaoPagamentoIntegrado'.
     */
    public int getCdSolicitacaoPagamentoIntegrado()
    {
        return this._cdSolicitacaoPagamentoIntegrado;
    } //-- int getCdSolicitacaoPagamentoIntegrado() 

    /**
     * Returns the value of field 'cdTipoRelatPagamento'.
     * 
     * @return int
     * @return the value of field 'cdTipoRelatPagamento'.
     */
    public int getCdTipoRelatPagamento()
    {
        return this._cdTipoRelatPagamento;
    } //-- int getCdTipoRelatPagamento() 

    /**
     * Returns the value of field 'dtFim'.
     * 
     * @return String
     * @return the value of field 'dtFim'.
     */
    public java.lang.String getDtFim()
    {
        return this._dtFim;
    } //-- java.lang.String getDtFim() 

    /**
     * Returns the value of field 'dtInicio'.
     * 
     * @return String
     * @return the value of field 'dtInicio'.
     */
    public java.lang.String getDtInicio()
    {
        return this._dtInicio;
    } //-- java.lang.String getDtInicio() 

    /**
     * Returns the value of field
     * 'nrSolicitacaoPagamentoIntegrado'.
     * 
     * @return int
     * @return the value of field 'nrSolicitacaoPagamentoIntegrado'.
     */
    public int getNrSolicitacaoPagamentoIntegrado()
    {
        return this._nrSolicitacaoPagamentoIntegrado;
    } //-- int getNrSolicitacaoPagamentoIntegrado() 

    /**
     * Returns the value of field 'qtOcorrencias'.
     * 
     * @return int
     * @return the value of field 'qtOcorrencias'.
     */
    public int getQtOcorrencias()
    {
        return this._qtOcorrencias;
    } //-- int getQtOcorrencias() 

    /**
     * Method hasCdSolicitacaoPagamentoIntegrado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSolicitacaoPagamentoIntegrado()
    {
        return this._has_cdSolicitacaoPagamentoIntegrado;
    } //-- boolean hasCdSolicitacaoPagamentoIntegrado() 

    /**
     * Method hasCdTipoRelatPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoRelatPagamento()
    {
        return this._has_cdTipoRelatPagamento;
    } //-- boolean hasCdTipoRelatPagamento() 

    /**
     * Method hasNrSolicitacaoPagamentoIntegrado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSolicitacaoPagamentoIntegrado()
    {
        return this._has_nrSolicitacaoPagamentoIntegrado;
    } //-- boolean hasNrSolicitacaoPagamentoIntegrado() 

    /**
     * Method hasQtOcorrencias
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtOcorrencias()
    {
        return this._has_qtOcorrencias;
    } //-- boolean hasQtOcorrencias() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdSolicitacaoPagamentoIntegrado'.
     * 
     * @param cdSolicitacaoPagamentoIntegrado the value of field
     * 'cdSolicitacaoPagamentoIntegrado'.
     */
    public void setCdSolicitacaoPagamentoIntegrado(int cdSolicitacaoPagamentoIntegrado)
    {
        this._cdSolicitacaoPagamentoIntegrado = cdSolicitacaoPagamentoIntegrado;
        this._has_cdSolicitacaoPagamentoIntegrado = true;
    } //-- void setCdSolicitacaoPagamentoIntegrado(int) 

    /**
     * Sets the value of field 'cdTipoRelatPagamento'.
     * 
     * @param cdTipoRelatPagamento the value of field
     * 'cdTipoRelatPagamento'.
     */
    public void setCdTipoRelatPagamento(int cdTipoRelatPagamento)
    {
        this._cdTipoRelatPagamento = cdTipoRelatPagamento;
        this._has_cdTipoRelatPagamento = true;
    } //-- void setCdTipoRelatPagamento(int) 

    /**
     * Sets the value of field 'dtFim'.
     * 
     * @param dtFim the value of field 'dtFim'.
     */
    public void setDtFim(java.lang.String dtFim)
    {
        this._dtFim = dtFim;
    } //-- void setDtFim(java.lang.String) 

    /**
     * Sets the value of field 'dtInicio'.
     * 
     * @param dtInicio the value of field 'dtInicio'.
     */
    public void setDtInicio(java.lang.String dtInicio)
    {
        this._dtInicio = dtInicio;
    } //-- void setDtInicio(java.lang.String) 

    /**
     * Sets the value of field 'nrSolicitacaoPagamentoIntegrado'.
     * 
     * @param nrSolicitacaoPagamentoIntegrado the value of field
     * 'nrSolicitacaoPagamentoIntegrado'.
     */
    public void setNrSolicitacaoPagamentoIntegrado(int nrSolicitacaoPagamentoIntegrado)
    {
        this._nrSolicitacaoPagamentoIntegrado = nrSolicitacaoPagamentoIntegrado;
        this._has_nrSolicitacaoPagamentoIntegrado = true;
    } //-- void setNrSolicitacaoPagamentoIntegrado(int) 

    /**
     * Sets the value of field 'qtOcorrencias'.
     * 
     * @param qtOcorrencias the value of field 'qtOcorrencias'.
     */
    public void setQtOcorrencias(int qtOcorrencias)
    {
        this._qtOcorrencias = qtOcorrencias;
        this._has_qtOcorrencias = true;
    } //-- void setQtOcorrencias(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return CancelarRelatorioContratosRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.cancelarrelatorioscontratos.request.CancelarRelatorioContratosRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.cancelarrelatorioscontratos.request.CancelarRelatorioContratosRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.cancelarrelatorioscontratos.request.CancelarRelatorioContratosRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.cancelarrelatorioscontratos.request.CancelarRelatorioContratosRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
