/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/implementation.ftl,v $
 * $Id: implementation.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: implementation.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */
 
package br.com.bradesco.web.pgit.service.business.histmanutorgaopag.impl;

import br.com.bradesco.web.pgit.service.business.histmanutorgaopag.IHistManutOrgaoPagService;
import br.com.bradesco.web.pgit.service.data.pdc.FactoryAdapter;


/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Implementa��o do adaptador: HistoricoManutencaoOrgaoPagador
 * </p>
 * 
 * @comment C�DIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public class HistManutOrgPagServiceImpl implements IHistManutOrgaoPagService {

	/** Atributo factoryAdapter. */
	private FactoryAdapter factoryAdapter;

	
	/**
     * Construtor.
     */
    public HistManutOrgPagServiceImpl() {
        // TODO: Implementa��o
    }
    
    
    
    /**
     * M�todo de exemplo.
     *
     * @see br.com.bradesco.web.pgit.service.business.histmanutorgaopag.IHistoricoManutencaoOrgaoPagador#sampleHistoricoManutencaoOrgaoPagador()
     */
    public void sampleHistoricoManutencaoOrgaoPagador() {
        // TODO: Implementa�ao
    }



	/**
	 * Get: factoryAdapter.
	 *
	 * @return factoryAdapter
	 */
	public FactoryAdapter getFactoryAdapter() {
		return factoryAdapter;
	}



	/**
	 * Set: factoryAdapter.
	 *
	 * @param factoryAdapter the factory adapter
	 */
	public void setFactoryAdapter(FactoryAdapter factoryAdapter) {
		this.factoryAdapter = factoryAdapter;
	}
    
}

