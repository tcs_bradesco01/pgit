/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/interfaz.ftl,v $
 * $Id: interfaz.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: interfaz.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.liberarpagtosindsemconsultasaldo;

import java.util.List;

import br.com.bradesco.web.pgit.service.business.liberarpagtosindsemconsultasaldo.bean.ConsultarLibPagtosIndSemConsSaldoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.liberarpagtosindsemconsultasaldo.bean.ConsultarLibPagtosIndSemConsSaldoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.liberarpagtosindsemconsultasaldo.bean.LiberarPagtosIndSemConsultaSaldoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.liberarpagtosindsemconsultasaldo.bean.LiberarPagtosIndSemConsultaSaldoSaidaDTO;

/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Interface do adaptador: LiberarPagtosIndSemConsultaSaldo
 * </p>
 * 
 * @comment CODIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public interface ILiberarPagtosIndSemConsultaSaldoService {

    
    /**
     * Consultar lib pagtos ind sem cons saldo.
     *
     * @param consultarLibPagtosIndSemConsSaldoEntradaDTO the consultar lib pagtos ind sem cons saldo entrada dto
     * @return the list< consultar lib pagtos ind sem cons saldo saida dt o>
     */
    List<ConsultarLibPagtosIndSemConsSaldoSaidaDTO> consultarLibPagtosIndSemConsSaldo(ConsultarLibPagtosIndSemConsSaldoEntradaDTO consultarLibPagtosIndSemConsSaldoEntradaDTO);
    
    /**
     * Liberar pagtos ind sem consulta saldo.
     *
     * @param listaEntradaDTO the lista entrada dto
     * @return the liberar pagtos ind sem consulta saldo saida dto
     */
    LiberarPagtosIndSemConsultaSaldoSaidaDTO liberarPagtosIndSemConsultaSaldo (List<LiberarPagtosIndSemConsultaSaldoEntradaDTO> listaEntradaDTO);

}

