/*
 * Nome: br.com.bradesco.web.pgit.service.business.combo.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.combo.bean;

import java.util.List;


/**
 * Nome: CentroCustoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarNumeroVersaoLayoutSaidaDTO {

	/** Atributo codMensagem. */
	private String codMensagem;
	
	/** Atributo mensagem. */
	private String mensagem;
    
    /** Atributo numeroLinhas. */
    private Integer numeroLinhas;
    
    /** Atributo ocorrencias. */
    private List<ListarNumeroVersaoLayoutOcorrSaidaDTO> ocorrencias;

	/**
	 * Nome: getCodMensagem
	 *
	 * @exception
	 * @throws
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}

	/**
	 * Nome: setCodMensagem
	 *
	 * @exception
	 * @throws
	 * @param codMensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}

	/**
	 * Nome: getMensagem
	 *
	 * @exception
	 * @throws
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}

	/**
	 * Nome: setMensagem
	 *
	 * @exception
	 * @throws
	 * @param mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	/**
	 * Nome: getNumeroLinhas
	 *
	 * @exception
	 * @throws
	 * @return numeroLinhas
	 */
	public Integer getNumeroLinhas() {
		return numeroLinhas;
	}

	/**
	 * Nome: setNumeroLinhas
	 *
	 * @exception
	 * @throws
	 * @param numeroLinhas
	 */
	public void setNumeroLinhas(Integer numeroLinhas) {
		this.numeroLinhas = numeroLinhas;
	}

	/**
	 * Nome: getOcorrencias
	 *
	 * @exception
	 * @throws
	 * @return ocorrencias
	 */
	public List<ListarNumeroVersaoLayoutOcorrSaidaDTO> getOcorrencias() {
		return ocorrencias;
	}

	/**
	 * Nome: setOcorrencias
	 *
	 * @exception
	 * @throws
	 * @param ocorrencias
	 */
	public void setOcorrencias(
			List<ListarNumeroVersaoLayoutOcorrSaidaDTO> ocorrencias) {
		this.ocorrencias = ocorrencias;
	}
    
}
