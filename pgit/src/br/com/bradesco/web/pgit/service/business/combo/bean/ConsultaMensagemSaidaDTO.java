/*
 * Nome: br.com.bradesco.web.pgit.service.business.combo.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.combo.bean;

/**
 * Nome: ConsultaMensagemSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultaMensagemSaidaDTO {
	
	/** Atributo codMensagem. */
	private String codMensagem;
	
	/** Atributo mensagem. */
	private String mensagem;
    
    /** Atributo nrEventoMensagemNegocio. */
    private Integer nrEventoMensagemNegocio;
	
	/** Atributo dsEventoMensagemNegocio. */
	private String dsEventoMensagemNegocio;
	
    /**
     * Consulta mensagem saida dto.
     */
    public ConsultaMensagemSaidaDTO() {
		super();
	}

	/**
	 * Consulta mensagem saida dto.
	 *
	 * @param nrEventoMensagemNegocio the nr evento mensagem negocio
	 * @param dsEventoMensagemNegocio the ds evento mensagem negocio
	 */
	public ConsultaMensagemSaidaDTO(Integer nrEventoMensagemNegocio, String dsEventoMensagemNegocio) {
		super();
		this.nrEventoMensagemNegocio = nrEventoMensagemNegocio;
		this.dsEventoMensagemNegocio = dsEventoMensagemNegocio;
	}

	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}

	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}

	/**
	 * Get: dsEventoMensagemNegocio.
	 *
	 * @return dsEventoMensagemNegocio
	 */
	public String getDsEventoMensagemNegocio() {
		return dsEventoMensagemNegocio;
	}

	/**
	 * Set: dsEventoMensagemNegocio.
	 *
	 * @param dsEventoMensagemNegocio the ds evento mensagem negocio
	 */
	public void setDsEventoMensagemNegocio(String dsEventoMensagemNegocio) {
		this.dsEventoMensagemNegocio = dsEventoMensagemNegocio;
	}

	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}

	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	/**
	 * Get: nrEventoMensagemNegocio.
	 *
	 * @return nrEventoMensagemNegocio
	 */
	public Integer getNrEventoMensagemNegocio() {
		return nrEventoMensagemNegocio;
	}

	/**
	 * Set: nrEventoMensagemNegocio.
	 *
	 * @param nrEventoMensagemNegocio the nr evento mensagem negocio
	 */
	public void setNrEventoMensagemNegocio(Integer nrEventoMensagemNegocio) {
		this.nrEventoMensagemNegocio = nrEventoMensagemNegocio;
	}

}
