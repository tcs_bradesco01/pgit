/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultarparticipantecontrato.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdCpfCnpjParticipante
     */
    private long _cdCpfCnpjParticipante = 0;

    /**
     * keeps track of state for field: _cdCpfCnpjParticipante
     */
    private boolean _has_cdCpfCnpjParticipante;

    /**
     * Field _cdFilialCnpjParticipante
     */
    private int _cdFilialCnpjParticipante = 0;

    /**
     * keeps track of state for field: _cdFilialCnpjParticipante
     */
    private boolean _has_cdFilialCnpjParticipante;

    /**
     * Field _cdControleCpfParticipante
     */
    private int _cdControleCpfParticipante = 0;

    /**
     * keeps track of state for field: _cdControleCpfParticipante
     */
    private boolean _has_cdControleCpfParticipante;

    /**
     * Field _cdParticipante
     */
    private long _cdParticipante = 0;

    /**
     * keeps track of state for field: _cdParticipante
     */
    private boolean _has_cdParticipante;

    /**
     * Field _dsParticipante
     */
    private java.lang.String _dsParticipante;

    /**
     * Field _dsTipoParticipacao
     */
    private java.lang.String _dsTipoParticipacao;

    /**
     * Field _dsSituacaoParticipacao
     */
    private java.lang.String _dsSituacaoParticipacao;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarparticipantecontrato.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdControleCpfParticipante
     * 
     */
    public void deleteCdControleCpfParticipante()
    {
        this._has_cdControleCpfParticipante= false;
    } //-- void deleteCdControleCpfParticipante() 

    /**
     * Method deleteCdCpfCnpjParticipante
     * 
     */
    public void deleteCdCpfCnpjParticipante()
    {
        this._has_cdCpfCnpjParticipante= false;
    } //-- void deleteCdCpfCnpjParticipante() 

    /**
     * Method deleteCdFilialCnpjParticipante
     * 
     */
    public void deleteCdFilialCnpjParticipante()
    {
        this._has_cdFilialCnpjParticipante= false;
    } //-- void deleteCdFilialCnpjParticipante() 

    /**
     * Method deleteCdParticipante
     * 
     */
    public void deleteCdParticipante()
    {
        this._has_cdParticipante= false;
    } //-- void deleteCdParticipante() 

    /**
     * Returns the value of field 'cdControleCpfParticipante'.
     * 
     * @return int
     * @return the value of field 'cdControleCpfParticipante'.
     */
    public int getCdControleCpfParticipante()
    {
        return this._cdControleCpfParticipante;
    } //-- int getCdControleCpfParticipante() 

    /**
     * Returns the value of field 'cdCpfCnpjParticipante'.
     * 
     * @return long
     * @return the value of field 'cdCpfCnpjParticipante'.
     */
    public long getCdCpfCnpjParticipante()
    {
        return this._cdCpfCnpjParticipante;
    } //-- long getCdCpfCnpjParticipante() 

    /**
     * Returns the value of field 'cdFilialCnpjParticipante'.
     * 
     * @return int
     * @return the value of field 'cdFilialCnpjParticipante'.
     */
    public int getCdFilialCnpjParticipante()
    {
        return this._cdFilialCnpjParticipante;
    } //-- int getCdFilialCnpjParticipante() 

    /**
     * Returns the value of field 'cdParticipante'.
     * 
     * @return long
     * @return the value of field 'cdParticipante'.
     */
    public long getCdParticipante()
    {
        return this._cdParticipante;
    } //-- long getCdParticipante() 

    /**
     * Returns the value of field 'dsParticipante'.
     * 
     * @return String
     * @return the value of field 'dsParticipante'.
     */
    public java.lang.String getDsParticipante()
    {
        return this._dsParticipante;
    } //-- java.lang.String getDsParticipante() 

    /**
     * Returns the value of field 'dsSituacaoParticipacao'.
     * 
     * @return String
     * @return the value of field 'dsSituacaoParticipacao'.
     */
    public java.lang.String getDsSituacaoParticipacao()
    {
        return this._dsSituacaoParticipacao;
    } //-- java.lang.String getDsSituacaoParticipacao() 

    /**
     * Returns the value of field 'dsTipoParticipacao'.
     * 
     * @return String
     * @return the value of field 'dsTipoParticipacao'.
     */
    public java.lang.String getDsTipoParticipacao()
    {
        return this._dsTipoParticipacao;
    } //-- java.lang.String getDsTipoParticipacao() 

    /**
     * Method hasCdControleCpfParticipante
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdControleCpfParticipante()
    {
        return this._has_cdControleCpfParticipante;
    } //-- boolean hasCdControleCpfParticipante() 

    /**
     * Method hasCdCpfCnpjParticipante
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCpfCnpjParticipante()
    {
        return this._has_cdCpfCnpjParticipante;
    } //-- boolean hasCdCpfCnpjParticipante() 

    /**
     * Method hasCdFilialCnpjParticipante
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFilialCnpjParticipante()
    {
        return this._has_cdFilialCnpjParticipante;
    } //-- boolean hasCdFilialCnpjParticipante() 

    /**
     * Method hasCdParticipante
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdParticipante()
    {
        return this._has_cdParticipante;
    } //-- boolean hasCdParticipante() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdControleCpfParticipante'.
     * 
     * @param cdControleCpfParticipante the value of field
     * 'cdControleCpfParticipante'.
     */
    public void setCdControleCpfParticipante(int cdControleCpfParticipante)
    {
        this._cdControleCpfParticipante = cdControleCpfParticipante;
        this._has_cdControleCpfParticipante = true;
    } //-- void setCdControleCpfParticipante(int) 

    /**
     * Sets the value of field 'cdCpfCnpjParticipante'.
     * 
     * @param cdCpfCnpjParticipante the value of field
     * 'cdCpfCnpjParticipante'.
     */
    public void setCdCpfCnpjParticipante(long cdCpfCnpjParticipante)
    {
        this._cdCpfCnpjParticipante = cdCpfCnpjParticipante;
        this._has_cdCpfCnpjParticipante = true;
    } //-- void setCdCpfCnpjParticipante(long) 

    /**
     * Sets the value of field 'cdFilialCnpjParticipante'.
     * 
     * @param cdFilialCnpjParticipante the value of field
     * 'cdFilialCnpjParticipante'.
     */
    public void setCdFilialCnpjParticipante(int cdFilialCnpjParticipante)
    {
        this._cdFilialCnpjParticipante = cdFilialCnpjParticipante;
        this._has_cdFilialCnpjParticipante = true;
    } //-- void setCdFilialCnpjParticipante(int) 

    /**
     * Sets the value of field 'cdParticipante'.
     * 
     * @param cdParticipante the value of field 'cdParticipante'.
     */
    public void setCdParticipante(long cdParticipante)
    {
        this._cdParticipante = cdParticipante;
        this._has_cdParticipante = true;
    } //-- void setCdParticipante(long) 

    /**
     * Sets the value of field 'dsParticipante'.
     * 
     * @param dsParticipante the value of field 'dsParticipante'.
     */
    public void setDsParticipante(java.lang.String dsParticipante)
    {
        this._dsParticipante = dsParticipante;
    } //-- void setDsParticipante(java.lang.String) 

    /**
     * Sets the value of field 'dsSituacaoParticipacao'.
     * 
     * @param dsSituacaoParticipacao the value of field
     * 'dsSituacaoParticipacao'.
     */
    public void setDsSituacaoParticipacao(java.lang.String dsSituacaoParticipacao)
    {
        this._dsSituacaoParticipacao = dsSituacaoParticipacao;
    } //-- void setDsSituacaoParticipacao(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoParticipacao'.
     * 
     * @param dsTipoParticipacao the value of field
     * 'dsTipoParticipacao'.
     */
    public void setDsTipoParticipacao(java.lang.String dsTipoParticipacao)
    {
        this._dsTipoParticipacao = dsTipoParticipacao;
    } //-- void setDsTipoParticipacao(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultarparticipantecontrato.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultarparticipantecontrato.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultarparticipantecontrato.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarparticipantecontrato.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
