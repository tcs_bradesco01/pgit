/*
 * =========================================================================
 * 
 * Client:       Bradesco (BR)
 * Project:      Arquitetura Bradesco Canal Internet
 * Development:  GFT Iberia (http://www.gft.com)
 * -------------------------------------------------------------------------
 * Revision - Last:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/constants.ftl,v $
 * $Id: constants.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revision - History:
 * $Log: constants.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.filtroagendamentoefetivacaoestorno;


/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Interface de constantes do adaptador: FiltroAgendamentoEfetivacaoEstorno
 * </p>
 * 
 * @comment C�DIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public interface IFiltroAgendamentoEfetivacaoEstornoServiceConstants {
	
	/** Atributo OCORRENCIA_LISTAR_CLIENTES_MARQ. */
	int OCORRENCIA_LISTAR_CLIENTES_MARQ = 50;
	
	/** Atributo OCORRENCIA_LISTAR_CONTRATO_CLIENTES_MARQ. */
	int OCORRENCIA_LISTAR_CONTRATO_CLIENTES_MARQ = 50;
	
	/** Atributo OCORRENCIA_LISTAR_CONTRATO_MARQ. */
	int OCORRENCIA_LISTAR_CONTRATO_MARQ = 50;	
	
	/** Atributo OCORRENCIA_LISTAR_PERFIL_TROCA_ARQUIVO. */
	int OCORRENCIA_LISTAR_PERFIL_TROCA_ARQUIVO = 100;
	
	/** Atributo OCORRENCIA_CONTRATO_CLIENTE_FILTRO. */
	int OCORRENCIA_CONTRATO_CLIENTE_FILTRO = 50;

}

