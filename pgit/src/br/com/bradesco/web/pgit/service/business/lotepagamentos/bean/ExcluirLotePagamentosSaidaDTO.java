package br.com.bradesco.web.pgit.service.business.lotepagamentos.bean;

public class ExcluirLotePagamentosSaidaDTO {

	private String codMensagem;
	private String mensagem;

	public String getCodMensagem() {
		return codMensagem;
	}

	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}

	public String getMensagem() {
		return mensagem;
	}

	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
}
