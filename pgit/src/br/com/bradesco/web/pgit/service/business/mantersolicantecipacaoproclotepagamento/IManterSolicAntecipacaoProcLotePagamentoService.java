/**
 * Nome: br.com.bradesco.web.pgit.service.business.mantersolicantecipacaoproclotepagamento
 * Compilador: JDK 1.5
 * Prop�sito: INSERIR O PROP�SITO DAS CLASSES DO PACOTE
 * Data da cria��o: <dd/MM/yyyy>
 * Par�metros de compila��o: -d
 */
package br.com.bradesco.web.pgit.service.business.mantersolicantecipacaoproclotepagamento;

import java.util.List;

import br.com.bradesco.web.pgit.service.business.lotepagamentos.bean.ConsultarLotePagamentosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.lotepagamentos.bean.ConsultarLotePagamentosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.lotepagamentos.bean.DetalharLotePagamentosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.lotepagamentos.bean.DetalharLotePagamentosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.lotepagamentos.bean.ExcluirLotePagamentosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.lotepagamentos.bean.ExcluirLotePagamentosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.lotepagamentos.bean.IncluirLotePagamentosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.lotepagamentos.bean.IncluirLotePagamentosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicantecipacaoproclotepagamento.bean.ConsultarLotesIncSolicEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicantecipacaoproclotepagamento.bean.ConsultarLotesIncSolicSaidaDTO;

/**
 * Nome: IManterSolicAntecipacaoProcLotePagamentoService
 * <p>
 * Prop�sito: Interface do adaptador ManterSolicAntecipacaoProcLotePagamento
 * </p>
 * 
 * @author CPM Braxis / TI Melhorias - Arquitetura
 * @version 1.0
 */
public interface IManterSolicAntecipacaoProcLotePagamentoService {

	public ConsultarLotePagamentosSaidaDTO consultarSolicAntProcLotePgtoAgda(
			ConsultarLotePagamentosEntradaDTO entrada);

	public DetalharLotePagamentosSaidaDTO detalharSolicAntProcLotePgtoAgda(
			DetalharLotePagamentosEntradaDTO entrada);

	public ExcluirLotePagamentosSaidaDTO excluirSolicAntProcLotePgtoAgda(
			ExcluirLotePagamentosEntradaDTO entrada);

	public IncluirLotePagamentosSaidaDTO incluirSolicAntProcAgda(
			IncluirLotePagamentosEntradaDTO entrada);
	
	public List<ConsultarLotesIncSolicSaidaDTO> consultarLotes(
			ConsultarLotesIncSolicEntradaDTO entrada);
}