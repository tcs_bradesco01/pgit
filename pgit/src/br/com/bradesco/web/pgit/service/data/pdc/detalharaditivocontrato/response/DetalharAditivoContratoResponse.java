/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.detalharaditivocontrato.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.util.Enumeration;
import java.util.Vector;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class DetalharAditivoContratoResponse.
 * 
 * @version $Revision$ $Date$
 */
public class DetalharAditivoContratoResponse implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _codMensagem
     */
    private java.lang.String _codMensagem;

    /**
     * Field _mensagem
     */
    private java.lang.String _mensagem;

    /**
     * Field _hrInclusaoRegistro
     */
    private java.lang.String _hrInclusaoRegistro;

    /**
     * Field _dsSituacaoAditivoContrato
     */
    private java.lang.String _dsSituacaoAditivoContrato;

    /**
     * Field _hrAssinaturaAditivoContrato
     */
    private java.lang.String _hrAssinaturaAditivoContrato;

    /**
     * Field _dsMotivoSituacaoAditivo
     */
    private java.lang.String _dsMotivoSituacaoAditivo;

    /**
     * Field _cdIndicadorContratoComl
     */
    private int _cdIndicadorContratoComl = 0;

    /**
     * keeps track of state for field: _cdIndicadorContratoComl
     */
    private boolean _has_cdIndicadorContratoComl;

    /**
     * Field _cdUsuarioInclusao
     */
    private java.lang.String _cdUsuarioInclusao;

    /**
     * Field _cdUsuarioInclusaoExterno
     */
    private java.lang.String _cdUsuarioInclusaoExterno;

    /**
     * Field _hrInclusaoRegistroTrilha
     */
    private java.lang.String _hrInclusaoRegistroTrilha;

    /**
     * Field _cdOperacaoCanalInclusao
     */
    private java.lang.String _cdOperacaoCanalInclusao;

    /**
     * Field _cdTipoCanalInclusaoTrilha
     */
    private int _cdTipoCanalInclusaoTrilha = 0;

    /**
     * keeps track of state for field: _cdTipoCanalInclusaoTrilha
     */
    private boolean _has_cdTipoCanalInclusaoTrilha;

    /**
     * Field _dsTipoCanalInclusaoTrilha
     */
    private java.lang.String _dsTipoCanalInclusaoTrilha;

    /**
     * Field _cdUsuarioManutencao
     */
    private java.lang.String _cdUsuarioManutencao;

    /**
     * Field _cdUsuarioManutencaoExterno
     */
    private java.lang.String _cdUsuarioManutencaoExterno;

    /**
     * Field _hrManutencaoRegistro
     */
    private java.lang.String _hrManutencaoRegistro;

    /**
     * Field _cdOperacaoCanalManutencao
     */
    private java.lang.String _cdOperacaoCanalManutencao;

    /**
     * Field _cdTipoCanalManutencao
     */
    private int _cdTipoCanalManutencao = 0;

    /**
     * keeps track of state for field: _cdTipoCanalManutencao
     */
    private boolean _has_cdTipoCanalManutencao;

    /**
     * Field _dsTipoCanalManutencao
     */
    private java.lang.String _dsTipoCanalManutencao;

    /**
     * Field _numeroOcorrenciasParticipante
     */
    private int _numeroOcorrenciasParticipante = 0;

    /**
     * keeps track of state for field: _numeroOcorrenciasParticipant
     */
    private boolean _has_numeroOcorrenciasParticipante;

    /**
     * Field _numeroOcorrenciasVinculacao
     */
    private int _numeroOcorrenciasVinculacao = 0;

    /**
     * keeps track of state for field: _numeroOcorrenciasVinculacao
     */
    private boolean _has_numeroOcorrenciasVinculacao;

    /**
     * Field _numeroOcorrenciasServicos
     */
    private int _numeroOcorrenciasServicos = 0;

    /**
     * keeps track of state for field: _numeroOcorrenciasServicos
     */
    private boolean _has_numeroOcorrenciasServicos;

    /**
     * Field _cdFormaAutorizacaoPagamento
     */
    private int _cdFormaAutorizacaoPagamento = 0;

    /**
     * keeps track of state for field: _cdFormaAutorizacaoPagamento
     */
    private boolean _has_cdFormaAutorizacaoPagamento;

    /**
     * Field _dsFormaAutorizacaoPagamento
     */
    private java.lang.String _dsFormaAutorizacaoPagamento;

    /**
     * Field _cdUnidadeOrganizacional
     */
    private int _cdUnidadeOrganizacional = 0;

    /**
     * keeps track of state for field: _cdUnidadeOrganizacional
     */
    private boolean _has_cdUnidadeOrganizacional;

    /**
     * Field _dsUnidadeOrganizacional
     */
    private java.lang.String _dsUnidadeOrganizacional;

    /**
     * Field _cdFuncionarioGerenteContrato
     */
    private long _cdFuncionarioGerenteContrato = 0;

    /**
     * keeps track of state for field: _cdFuncionarioGerenteContrato
     */
    private boolean _has_cdFuncionarioGerenteContrato;

    /**
     * Field _dsFuncionarioGerenteContrato
     */
    private java.lang.String _dsFuncionarioGerenteContrato;

    /**
     * Field _ocorrenciasList
     */
    private java.util.Vector _ocorrenciasList;

    /**
     * Field _ocorrencias1List
     */
    private java.util.Vector _ocorrencias1List;

    /**
     * Field _ocorrencias2List
     */
    private java.util.Vector _ocorrencias2List;


      //----------------/
     //- Constructors -/
    //----------------/

    public DetalharAditivoContratoResponse() 
     {
        super();
        _ocorrenciasList = new Vector();
        _ocorrencias1List = new Vector();
        _ocorrencias2List = new Vector();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharaditivocontrato.response.DetalharAditivoContratoResponse()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method addOcorrencias
     * 
     * 
     * 
     * @param vOcorrencias
     */
    public void addOcorrencias(br.com.bradesco.web.pgit.service.data.pdc.detalharaditivocontrato.response.Ocorrencias vOcorrencias)
        throws java.lang.IndexOutOfBoundsException
    {
        if (!(_ocorrenciasList.size() < 35)) {
            throw new IndexOutOfBoundsException("addOcorrencias has a maximum of 35");
        }
        _ocorrenciasList.addElement(vOcorrencias);
    } //-- void addOcorrencias(br.com.bradesco.web.pgit.service.data.pdc.detalharaditivocontrato.response.Ocorrencias) 

    /**
     * Method addOcorrencias
     * 
     * 
     * 
     * @param index
     * @param vOcorrencias
     */
    public void addOcorrencias(int index, br.com.bradesco.web.pgit.service.data.pdc.detalharaditivocontrato.response.Ocorrencias vOcorrencias)
        throws java.lang.IndexOutOfBoundsException
    {
        if (!(_ocorrenciasList.size() < 35)) {
            throw new IndexOutOfBoundsException("addOcorrencias has a maximum of 35");
        }
        _ocorrenciasList.insertElementAt(vOcorrencias, index);
    } //-- void addOcorrencias(int, br.com.bradesco.web.pgit.service.data.pdc.detalharaditivocontrato.response.Ocorrencias) 

    /**
     * Method addOcorrencias1
     * 
     * 
     * 
     * @param vOcorrencias1
     */
    public void addOcorrencias1(br.com.bradesco.web.pgit.service.data.pdc.detalharaditivocontrato.response.Ocorrencias1 vOcorrencias1)
        throws java.lang.IndexOutOfBoundsException
    {
        if (!(_ocorrencias1List.size() < 60)) {
            throw new IndexOutOfBoundsException("addOcorrencias1 has a maximum of 60");
        }
        _ocorrencias1List.addElement(vOcorrencias1);
    } //-- void addOcorrencias1(br.com.bradesco.web.pgit.service.data.pdc.detalharaditivocontrato.response.Ocorrencias1) 

    /**
     * Method addOcorrencias1
     * 
     * 
     * 
     * @param index
     * @param vOcorrencias1
     */
    public void addOcorrencias1(int index, br.com.bradesco.web.pgit.service.data.pdc.detalharaditivocontrato.response.Ocorrencias1 vOcorrencias1)
        throws java.lang.IndexOutOfBoundsException
    {
        if (!(_ocorrencias1List.size() < 60)) {
            throw new IndexOutOfBoundsException("addOcorrencias1 has a maximum of 60");
        }
        _ocorrencias1List.insertElementAt(vOcorrencias1, index);
    } //-- void addOcorrencias1(int, br.com.bradesco.web.pgit.service.data.pdc.detalharaditivocontrato.response.Ocorrencias1) 

    /**
     * Method addOcorrencias2
     * 
     * 
     * 
     * @param vOcorrencias2
     */
    public void addOcorrencias2(br.com.bradesco.web.pgit.service.data.pdc.detalharaditivocontrato.response.Ocorrencias2 vOcorrencias2)
        throws java.lang.IndexOutOfBoundsException
    {
        if (!(_ocorrencias2List.size() < 30)) {
            throw new IndexOutOfBoundsException("addOcorrencias2 has a maximum of 30");
        }
        _ocorrencias2List.addElement(vOcorrencias2);
    } //-- void addOcorrencias2(br.com.bradesco.web.pgit.service.data.pdc.detalharaditivocontrato.response.Ocorrencias2) 

    /**
     * Method addOcorrencias2
     * 
     * 
     * 
     * @param index
     * @param vOcorrencias2
     */
    public void addOcorrencias2(int index, br.com.bradesco.web.pgit.service.data.pdc.detalharaditivocontrato.response.Ocorrencias2 vOcorrencias2)
        throws java.lang.IndexOutOfBoundsException
    {
        if (!(_ocorrencias2List.size() < 30)) {
            throw new IndexOutOfBoundsException("addOcorrencias2 has a maximum of 30");
        }
        _ocorrencias2List.insertElementAt(vOcorrencias2, index);
    } //-- void addOcorrencias2(int, br.com.bradesco.web.pgit.service.data.pdc.detalharaditivocontrato.response.Ocorrencias2) 

    /**
     * Method deleteCdFormaAutorizacaoPagamento
     * 
     */
    public void deleteCdFormaAutorizacaoPagamento()
    {
        this._has_cdFormaAutorizacaoPagamento= false;
    } //-- void deleteCdFormaAutorizacaoPagamento() 

    /**
     * Method deleteCdFuncionarioGerenteContrato
     * 
     */
    public void deleteCdFuncionarioGerenteContrato()
    {
        this._has_cdFuncionarioGerenteContrato= false;
    } //-- void deleteCdFuncionarioGerenteContrato() 

    /**
     * Method deleteCdIndicadorContratoComl
     * 
     */
    public void deleteCdIndicadorContratoComl()
    {
        this._has_cdIndicadorContratoComl= false;
    } //-- void deleteCdIndicadorContratoComl() 

    /**
     * Method deleteCdTipoCanalInclusaoTrilha
     * 
     */
    public void deleteCdTipoCanalInclusaoTrilha()
    {
        this._has_cdTipoCanalInclusaoTrilha= false;
    } //-- void deleteCdTipoCanalInclusaoTrilha() 

    /**
     * Method deleteCdTipoCanalManutencao
     * 
     */
    public void deleteCdTipoCanalManutencao()
    {
        this._has_cdTipoCanalManutencao= false;
    } //-- void deleteCdTipoCanalManutencao() 

    /**
     * Method deleteCdUnidadeOrganizacional
     * 
     */
    public void deleteCdUnidadeOrganizacional()
    {
        this._has_cdUnidadeOrganizacional= false;
    } //-- void deleteCdUnidadeOrganizacional() 

    /**
     * Method deleteNumeroOcorrenciasParticipante
     * 
     */
    public void deleteNumeroOcorrenciasParticipante()
    {
        this._has_numeroOcorrenciasParticipante= false;
    } //-- void deleteNumeroOcorrenciasParticipante() 

    /**
     * Method deleteNumeroOcorrenciasServicos
     * 
     */
    public void deleteNumeroOcorrenciasServicos()
    {
        this._has_numeroOcorrenciasServicos= false;
    } //-- void deleteNumeroOcorrenciasServicos() 

    /**
     * Method deleteNumeroOcorrenciasVinculacao
     * 
     */
    public void deleteNumeroOcorrenciasVinculacao()
    {
        this._has_numeroOcorrenciasVinculacao= false;
    } //-- void deleteNumeroOcorrenciasVinculacao() 

    /**
     * Method enumerateOcorrencias
     * 
     * 
     * 
     * @return Enumeration
     */
    public java.util.Enumeration enumerateOcorrencias()
    {
        return _ocorrenciasList.elements();
    } //-- java.util.Enumeration enumerateOcorrencias() 

    /**
     * Method enumerateOcorrencias1
     * 
     * 
     * 
     * @return Enumeration
     */
    public java.util.Enumeration enumerateOcorrencias1()
    {
        return _ocorrencias1List.elements();
    } //-- java.util.Enumeration enumerateOcorrencias1() 

    /**
     * Method enumerateOcorrencias2
     * 
     * 
     * 
     * @return Enumeration
     */
    public java.util.Enumeration enumerateOcorrencias2()
    {
        return _ocorrencias2List.elements();
    } //-- java.util.Enumeration enumerateOcorrencias2() 

    /**
     * Returns the value of field 'cdFormaAutorizacaoPagamento'.
     * 
     * @return int
     * @return the value of field 'cdFormaAutorizacaoPagamento'.
     */
    public int getCdFormaAutorizacaoPagamento()
    {
        return this._cdFormaAutorizacaoPagamento;
    } //-- int getCdFormaAutorizacaoPagamento() 

    /**
     * Returns the value of field 'cdFuncionarioGerenteContrato'.
     * 
     * @return long
     * @return the value of field 'cdFuncionarioGerenteContrato'.
     */
    public long getCdFuncionarioGerenteContrato()
    {
        return this._cdFuncionarioGerenteContrato;
    } //-- long getCdFuncionarioGerenteContrato() 

    /**
     * Returns the value of field 'cdIndicadorContratoComl'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorContratoComl'.
     */
    public int getCdIndicadorContratoComl()
    {
        return this._cdIndicadorContratoComl;
    } //-- int getCdIndicadorContratoComl() 

    /**
     * Returns the value of field 'cdOperacaoCanalInclusao'.
     * 
     * @return String
     * @return the value of field 'cdOperacaoCanalInclusao'.
     */
    public java.lang.String getCdOperacaoCanalInclusao()
    {
        return this._cdOperacaoCanalInclusao;
    } //-- java.lang.String getCdOperacaoCanalInclusao() 

    /**
     * Returns the value of field 'cdOperacaoCanalManutencao'.
     * 
     * @return String
     * @return the value of field 'cdOperacaoCanalManutencao'.
     */
    public java.lang.String getCdOperacaoCanalManutencao()
    {
        return this._cdOperacaoCanalManutencao;
    } //-- java.lang.String getCdOperacaoCanalManutencao() 

    /**
     * Returns the value of field 'cdTipoCanalInclusaoTrilha'.
     * 
     * @return int
     * @return the value of field 'cdTipoCanalInclusaoTrilha'.
     */
    public int getCdTipoCanalInclusaoTrilha()
    {
        return this._cdTipoCanalInclusaoTrilha;
    } //-- int getCdTipoCanalInclusaoTrilha() 

    /**
     * Returns the value of field 'cdTipoCanalManutencao'.
     * 
     * @return int
     * @return the value of field 'cdTipoCanalManutencao'.
     */
    public int getCdTipoCanalManutencao()
    {
        return this._cdTipoCanalManutencao;
    } //-- int getCdTipoCanalManutencao() 

    /**
     * Returns the value of field 'cdUnidadeOrganizacional'.
     * 
     * @return int
     * @return the value of field 'cdUnidadeOrganizacional'.
     */
    public int getCdUnidadeOrganizacional()
    {
        return this._cdUnidadeOrganizacional;
    } //-- int getCdUnidadeOrganizacional() 

    /**
     * Returns the value of field 'cdUsuarioInclusao'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioInclusao'.
     */
    public java.lang.String getCdUsuarioInclusao()
    {
        return this._cdUsuarioInclusao;
    } //-- java.lang.String getCdUsuarioInclusao() 

    /**
     * Returns the value of field 'cdUsuarioInclusaoExterno'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioInclusaoExterno'.
     */
    public java.lang.String getCdUsuarioInclusaoExterno()
    {
        return this._cdUsuarioInclusaoExterno;
    } //-- java.lang.String getCdUsuarioInclusaoExterno() 

    /**
     * Returns the value of field 'cdUsuarioManutencao'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioManutencao'.
     */
    public java.lang.String getCdUsuarioManutencao()
    {
        return this._cdUsuarioManutencao;
    } //-- java.lang.String getCdUsuarioManutencao() 

    /**
     * Returns the value of field 'cdUsuarioManutencaoExterno'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioManutencaoExterno'.
     */
    public java.lang.String getCdUsuarioManutencaoExterno()
    {
        return this._cdUsuarioManutencaoExterno;
    } //-- java.lang.String getCdUsuarioManutencaoExterno() 

    /**
     * Returns the value of field 'codMensagem'.
     * 
     * @return String
     * @return the value of field 'codMensagem'.
     */
    public java.lang.String getCodMensagem()
    {
        return this._codMensagem;
    } //-- java.lang.String getCodMensagem() 

    /**
     * Returns the value of field 'dsFormaAutorizacaoPagamento'.
     * 
     * @return String
     * @return the value of field 'dsFormaAutorizacaoPagamento'.
     */
    public java.lang.String getDsFormaAutorizacaoPagamento()
    {
        return this._dsFormaAutorizacaoPagamento;
    } //-- java.lang.String getDsFormaAutorizacaoPagamento() 

    /**
     * Returns the value of field 'dsFuncionarioGerenteContrato'.
     * 
     * @return String
     * @return the value of field 'dsFuncionarioGerenteContrato'.
     */
    public java.lang.String getDsFuncionarioGerenteContrato()
    {
        return this._dsFuncionarioGerenteContrato;
    } //-- java.lang.String getDsFuncionarioGerenteContrato() 

    /**
     * Returns the value of field 'dsMotivoSituacaoAditivo'.
     * 
     * @return String
     * @return the value of field 'dsMotivoSituacaoAditivo'.
     */
    public java.lang.String getDsMotivoSituacaoAditivo()
    {
        return this._dsMotivoSituacaoAditivo;
    } //-- java.lang.String getDsMotivoSituacaoAditivo() 

    /**
     * Returns the value of field 'dsSituacaoAditivoContrato'.
     * 
     * @return String
     * @return the value of field 'dsSituacaoAditivoContrato'.
     */
    public java.lang.String getDsSituacaoAditivoContrato()
    {
        return this._dsSituacaoAditivoContrato;
    } //-- java.lang.String getDsSituacaoAditivoContrato() 

    /**
     * Returns the value of field 'dsTipoCanalInclusaoTrilha'.
     * 
     * @return String
     * @return the value of field 'dsTipoCanalInclusaoTrilha'.
     */
    public java.lang.String getDsTipoCanalInclusaoTrilha()
    {
        return this._dsTipoCanalInclusaoTrilha;
    } //-- java.lang.String getDsTipoCanalInclusaoTrilha() 

    /**
     * Returns the value of field 'dsTipoCanalManutencao'.
     * 
     * @return String
     * @return the value of field 'dsTipoCanalManutencao'.
     */
    public java.lang.String getDsTipoCanalManutencao()
    {
        return this._dsTipoCanalManutencao;
    } //-- java.lang.String getDsTipoCanalManutencao() 

    /**
     * Returns the value of field 'dsUnidadeOrganizacional'.
     * 
     * @return String
     * @return the value of field 'dsUnidadeOrganizacional'.
     */
    public java.lang.String getDsUnidadeOrganizacional()
    {
        return this._dsUnidadeOrganizacional;
    } //-- java.lang.String getDsUnidadeOrganizacional() 

    /**
     * Returns the value of field 'hrAssinaturaAditivoContrato'.
     * 
     * @return String
     * @return the value of field 'hrAssinaturaAditivoContrato'.
     */
    public java.lang.String getHrAssinaturaAditivoContrato()
    {
        return this._hrAssinaturaAditivoContrato;
    } //-- java.lang.String getHrAssinaturaAditivoContrato() 

    /**
     * Returns the value of field 'hrInclusaoRegistro'.
     * 
     * @return String
     * @return the value of field 'hrInclusaoRegistro'.
     */
    public java.lang.String getHrInclusaoRegistro()
    {
        return this._hrInclusaoRegistro;
    } //-- java.lang.String getHrInclusaoRegistro() 

    /**
     * Returns the value of field 'hrInclusaoRegistroTrilha'.
     * 
     * @return String
     * @return the value of field 'hrInclusaoRegistroTrilha'.
     */
    public java.lang.String getHrInclusaoRegistroTrilha()
    {
        return this._hrInclusaoRegistroTrilha;
    } //-- java.lang.String getHrInclusaoRegistroTrilha() 

    /**
     * Returns the value of field 'hrManutencaoRegistro'.
     * 
     * @return String
     * @return the value of field 'hrManutencaoRegistro'.
     */
    public java.lang.String getHrManutencaoRegistro()
    {
        return this._hrManutencaoRegistro;
    } //-- java.lang.String getHrManutencaoRegistro() 

    /**
     * Returns the value of field 'mensagem'.
     * 
     * @return String
     * @return the value of field 'mensagem'.
     */
    public java.lang.String getMensagem()
    {
        return this._mensagem;
    } //-- java.lang.String getMensagem() 

    /**
     * Returns the value of field 'numeroOcorrenciasParticipante'.
     * 
     * @return int
     * @return the value of field 'numeroOcorrenciasParticipante'.
     */
    public int getNumeroOcorrenciasParticipante()
    {
        return this._numeroOcorrenciasParticipante;
    } //-- int getNumeroOcorrenciasParticipante() 

    /**
     * Returns the value of field 'numeroOcorrenciasServicos'.
     * 
     * @return int
     * @return the value of field 'numeroOcorrenciasServicos'.
     */
    public int getNumeroOcorrenciasServicos()
    {
        return this._numeroOcorrenciasServicos;
    } //-- int getNumeroOcorrenciasServicos() 

    /**
     * Returns the value of field 'numeroOcorrenciasVinculacao'.
     * 
     * @return int
     * @return the value of field 'numeroOcorrenciasVinculacao'.
     */
    public int getNumeroOcorrenciasVinculacao()
    {
        return this._numeroOcorrenciasVinculacao;
    } //-- int getNumeroOcorrenciasVinculacao() 

    /**
     * Method getOcorrencias
     * 
     * 
     * 
     * @param index
     * @return Ocorrencias
     */
    public br.com.bradesco.web.pgit.service.data.pdc.detalharaditivocontrato.response.Ocorrencias getOcorrencias(int index)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index >= _ocorrenciasList.size())) {
            throw new IndexOutOfBoundsException("getOcorrencias: Index value '"+index+"' not in range [0.."+(_ocorrenciasList.size() - 1) + "]");
        }
        
        return (br.com.bradesco.web.pgit.service.data.pdc.detalharaditivocontrato.response.Ocorrencias) _ocorrenciasList.elementAt(index);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharaditivocontrato.response.Ocorrencias getOcorrencias(int) 

    /**
     * Method getOcorrencias
     * 
     * 
     * 
     * @return Ocorrencias
     */
    public br.com.bradesco.web.pgit.service.data.pdc.detalharaditivocontrato.response.Ocorrencias[] getOcorrencias()
    {
        int size = _ocorrenciasList.size();
        br.com.bradesco.web.pgit.service.data.pdc.detalharaditivocontrato.response.Ocorrencias[] mArray = new br.com.bradesco.web.pgit.service.data.pdc.detalharaditivocontrato.response.Ocorrencias[size];
        for (int index = 0; index < size; index++) {
            mArray[index] = (br.com.bradesco.web.pgit.service.data.pdc.detalharaditivocontrato.response.Ocorrencias) _ocorrenciasList.elementAt(index);
        }
        return mArray;
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharaditivocontrato.response.Ocorrencias[] getOcorrencias() 

    /**
     * Method getOcorrencias1
     * 
     * 
     * 
     * @param index
     * @return Ocorrencias1
     */
    public br.com.bradesco.web.pgit.service.data.pdc.detalharaditivocontrato.response.Ocorrencias1 getOcorrencias1(int index)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index >= _ocorrencias1List.size())) {
            throw new IndexOutOfBoundsException("getOcorrencias1: Index value '"+index+"' not in range [0.."+(_ocorrencias1List.size() - 1) + "]");
        }
        
        return (br.com.bradesco.web.pgit.service.data.pdc.detalharaditivocontrato.response.Ocorrencias1) _ocorrencias1List.elementAt(index);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharaditivocontrato.response.Ocorrencias1 getOcorrencias1(int) 

    /**
     * Method getOcorrencias1
     * 
     * 
     * 
     * @return Ocorrencias1
     */
    public br.com.bradesco.web.pgit.service.data.pdc.detalharaditivocontrato.response.Ocorrencias1[] getOcorrencias1()
    {
        int size = _ocorrencias1List.size();
        br.com.bradesco.web.pgit.service.data.pdc.detalharaditivocontrato.response.Ocorrencias1[] mArray = new br.com.bradesco.web.pgit.service.data.pdc.detalharaditivocontrato.response.Ocorrencias1[size];
        for (int index = 0; index < size; index++) {
            mArray[index] = (br.com.bradesco.web.pgit.service.data.pdc.detalharaditivocontrato.response.Ocorrencias1) _ocorrencias1List.elementAt(index);
        }
        return mArray;
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharaditivocontrato.response.Ocorrencias1[] getOcorrencias1() 

    /**
     * Method getOcorrencias1Count
     * 
     * 
     * 
     * @return int
     */
    public int getOcorrencias1Count()
    {
        return _ocorrencias1List.size();
    } //-- int getOcorrencias1Count() 

    /**
     * Method getOcorrencias2
     * 
     * 
     * 
     * @param index
     * @return Ocorrencias2
     */
    public br.com.bradesco.web.pgit.service.data.pdc.detalharaditivocontrato.response.Ocorrencias2 getOcorrencias2(int index)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index >= _ocorrencias2List.size())) {
            throw new IndexOutOfBoundsException("getOcorrencias2: Index value '"+index+"' not in range [0.."+(_ocorrencias2List.size() - 1) + "]");
        }
        
        return (br.com.bradesco.web.pgit.service.data.pdc.detalharaditivocontrato.response.Ocorrencias2) _ocorrencias2List.elementAt(index);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharaditivocontrato.response.Ocorrencias2 getOcorrencias2(int) 

    /**
     * Method getOcorrencias2
     * 
     * 
     * 
     * @return Ocorrencias2
     */
    public br.com.bradesco.web.pgit.service.data.pdc.detalharaditivocontrato.response.Ocorrencias2[] getOcorrencias2()
    {
        int size = _ocorrencias2List.size();
        br.com.bradesco.web.pgit.service.data.pdc.detalharaditivocontrato.response.Ocorrencias2[] mArray = new br.com.bradesco.web.pgit.service.data.pdc.detalharaditivocontrato.response.Ocorrencias2[size];
        for (int index = 0; index < size; index++) {
            mArray[index] = (br.com.bradesco.web.pgit.service.data.pdc.detalharaditivocontrato.response.Ocorrencias2) _ocorrencias2List.elementAt(index);
        }
        return mArray;
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharaditivocontrato.response.Ocorrencias2[] getOcorrencias2() 

    /**
     * Method getOcorrencias2Count
     * 
     * 
     * 
     * @return int
     */
    public int getOcorrencias2Count()
    {
        return _ocorrencias2List.size();
    } //-- int getOcorrencias2Count() 

    /**
     * Method getOcorrenciasCount
     * 
     * 
     * 
     * @return int
     */
    public int getOcorrenciasCount()
    {
        return _ocorrenciasList.size();
    } //-- int getOcorrenciasCount() 

    /**
     * Method hasCdFormaAutorizacaoPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFormaAutorizacaoPagamento()
    {
        return this._has_cdFormaAutorizacaoPagamento;
    } //-- boolean hasCdFormaAutorizacaoPagamento() 

    /**
     * Method hasCdFuncionarioGerenteContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFuncionarioGerenteContrato()
    {
        return this._has_cdFuncionarioGerenteContrato;
    } //-- boolean hasCdFuncionarioGerenteContrato() 

    /**
     * Method hasCdIndicadorContratoComl
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorContratoComl()
    {
        return this._has_cdIndicadorContratoComl;
    } //-- boolean hasCdIndicadorContratoComl() 

    /**
     * Method hasCdTipoCanalInclusaoTrilha
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoCanalInclusaoTrilha()
    {
        return this._has_cdTipoCanalInclusaoTrilha;
    } //-- boolean hasCdTipoCanalInclusaoTrilha() 

    /**
     * Method hasCdTipoCanalManutencao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoCanalManutencao()
    {
        return this._has_cdTipoCanalManutencao;
    } //-- boolean hasCdTipoCanalManutencao() 

    /**
     * Method hasCdUnidadeOrganizacional
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdUnidadeOrganizacional()
    {
        return this._has_cdUnidadeOrganizacional;
    } //-- boolean hasCdUnidadeOrganizacional() 

    /**
     * Method hasNumeroOcorrenciasParticipante
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNumeroOcorrenciasParticipante()
    {
        return this._has_numeroOcorrenciasParticipante;
    } //-- boolean hasNumeroOcorrenciasParticipante() 

    /**
     * Method hasNumeroOcorrenciasServicos
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNumeroOcorrenciasServicos()
    {
        return this._has_numeroOcorrenciasServicos;
    } //-- boolean hasNumeroOcorrenciasServicos() 

    /**
     * Method hasNumeroOcorrenciasVinculacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNumeroOcorrenciasVinculacao()
    {
        return this._has_numeroOcorrenciasVinculacao;
    } //-- boolean hasNumeroOcorrenciasVinculacao() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Method removeAllOcorrencias
     * 
     */
    public void removeAllOcorrencias()
    {
        _ocorrenciasList.removeAllElements();
    } //-- void removeAllOcorrencias() 

    /**
     * Method removeAllOcorrencias1
     * 
     */
    public void removeAllOcorrencias1()
    {
        _ocorrencias1List.removeAllElements();
    } //-- void removeAllOcorrencias1() 

    /**
     * Method removeAllOcorrencias2
     * 
     */
    public void removeAllOcorrencias2()
    {
        _ocorrencias2List.removeAllElements();
    } //-- void removeAllOcorrencias2() 

    /**
     * Method removeOcorrencias
     * 
     * 
     * 
     * @param index
     * @return Ocorrencias
     */
    public br.com.bradesco.web.pgit.service.data.pdc.detalharaditivocontrato.response.Ocorrencias removeOcorrencias(int index)
    {
        java.lang.Object obj = _ocorrenciasList.elementAt(index);
        _ocorrenciasList.removeElementAt(index);
        return (br.com.bradesco.web.pgit.service.data.pdc.detalharaditivocontrato.response.Ocorrencias) obj;
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharaditivocontrato.response.Ocorrencias removeOcorrencias(int) 

    /**
     * Method removeOcorrencias1
     * 
     * 
     * 
     * @param index
     * @return Ocorrencias1
     */
    public br.com.bradesco.web.pgit.service.data.pdc.detalharaditivocontrato.response.Ocorrencias1 removeOcorrencias1(int index)
    {
        java.lang.Object obj = _ocorrencias1List.elementAt(index);
        _ocorrencias1List.removeElementAt(index);
        return (br.com.bradesco.web.pgit.service.data.pdc.detalharaditivocontrato.response.Ocorrencias1) obj;
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharaditivocontrato.response.Ocorrencias1 removeOcorrencias1(int) 

    /**
     * Method removeOcorrencias2
     * 
     * 
     * 
     * @param index
     * @return Ocorrencias2
     */
    public br.com.bradesco.web.pgit.service.data.pdc.detalharaditivocontrato.response.Ocorrencias2 removeOcorrencias2(int index)
    {
        java.lang.Object obj = _ocorrencias2List.elementAt(index);
        _ocorrencias2List.removeElementAt(index);
        return (br.com.bradesco.web.pgit.service.data.pdc.detalharaditivocontrato.response.Ocorrencias2) obj;
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharaditivocontrato.response.Ocorrencias2 removeOcorrencias2(int) 

    /**
     * Sets the value of field 'cdFormaAutorizacaoPagamento'.
     * 
     * @param cdFormaAutorizacaoPagamento the value of field
     * 'cdFormaAutorizacaoPagamento'.
     */
    public void setCdFormaAutorizacaoPagamento(int cdFormaAutorizacaoPagamento)
    {
        this._cdFormaAutorizacaoPagamento = cdFormaAutorizacaoPagamento;
        this._has_cdFormaAutorizacaoPagamento = true;
    } //-- void setCdFormaAutorizacaoPagamento(int) 

    /**
     * Sets the value of field 'cdFuncionarioGerenteContrato'.
     * 
     * @param cdFuncionarioGerenteContrato the value of field
     * 'cdFuncionarioGerenteContrato'.
     */
    public void setCdFuncionarioGerenteContrato(long cdFuncionarioGerenteContrato)
    {
        this._cdFuncionarioGerenteContrato = cdFuncionarioGerenteContrato;
        this._has_cdFuncionarioGerenteContrato = true;
    } //-- void setCdFuncionarioGerenteContrato(long) 

    /**
     * Sets the value of field 'cdIndicadorContratoComl'.
     * 
     * @param cdIndicadorContratoComl the value of field
     * 'cdIndicadorContratoComl'.
     */
    public void setCdIndicadorContratoComl(int cdIndicadorContratoComl)
    {
        this._cdIndicadorContratoComl = cdIndicadorContratoComl;
        this._has_cdIndicadorContratoComl = true;
    } //-- void setCdIndicadorContratoComl(int) 

    /**
     * Sets the value of field 'cdOperacaoCanalInclusao'.
     * 
     * @param cdOperacaoCanalInclusao the value of field
     * 'cdOperacaoCanalInclusao'.
     */
    public void setCdOperacaoCanalInclusao(java.lang.String cdOperacaoCanalInclusao)
    {
        this._cdOperacaoCanalInclusao = cdOperacaoCanalInclusao;
    } //-- void setCdOperacaoCanalInclusao(java.lang.String) 

    /**
     * Sets the value of field 'cdOperacaoCanalManutencao'.
     * 
     * @param cdOperacaoCanalManutencao the value of field
     * 'cdOperacaoCanalManutencao'.
     */
    public void setCdOperacaoCanalManutencao(java.lang.String cdOperacaoCanalManutencao)
    {
        this._cdOperacaoCanalManutencao = cdOperacaoCanalManutencao;
    } //-- void setCdOperacaoCanalManutencao(java.lang.String) 

    /**
     * Sets the value of field 'cdTipoCanalInclusaoTrilha'.
     * 
     * @param cdTipoCanalInclusaoTrilha the value of field
     * 'cdTipoCanalInclusaoTrilha'.
     */
    public void setCdTipoCanalInclusaoTrilha(int cdTipoCanalInclusaoTrilha)
    {
        this._cdTipoCanalInclusaoTrilha = cdTipoCanalInclusaoTrilha;
        this._has_cdTipoCanalInclusaoTrilha = true;
    } //-- void setCdTipoCanalInclusaoTrilha(int) 

    /**
     * Sets the value of field 'cdTipoCanalManutencao'.
     * 
     * @param cdTipoCanalManutencao the value of field
     * 'cdTipoCanalManutencao'.
     */
    public void setCdTipoCanalManutencao(int cdTipoCanalManutencao)
    {
        this._cdTipoCanalManutencao = cdTipoCanalManutencao;
        this._has_cdTipoCanalManutencao = true;
    } //-- void setCdTipoCanalManutencao(int) 

    /**
     * Sets the value of field 'cdUnidadeOrganizacional'.
     * 
     * @param cdUnidadeOrganizacional the value of field
     * 'cdUnidadeOrganizacional'.
     */
    public void setCdUnidadeOrganizacional(int cdUnidadeOrganizacional)
    {
        this._cdUnidadeOrganizacional = cdUnidadeOrganizacional;
        this._has_cdUnidadeOrganizacional = true;
    } //-- void setCdUnidadeOrganizacional(int) 

    /**
     * Sets the value of field 'cdUsuarioInclusao'.
     * 
     * @param cdUsuarioInclusao the value of field
     * 'cdUsuarioInclusao'.
     */
    public void setCdUsuarioInclusao(java.lang.String cdUsuarioInclusao)
    {
        this._cdUsuarioInclusao = cdUsuarioInclusao;
    } //-- void setCdUsuarioInclusao(java.lang.String) 

    /**
     * Sets the value of field 'cdUsuarioInclusaoExterno'.
     * 
     * @param cdUsuarioInclusaoExterno the value of field
     * 'cdUsuarioInclusaoExterno'.
     */
    public void setCdUsuarioInclusaoExterno(java.lang.String cdUsuarioInclusaoExterno)
    {
        this._cdUsuarioInclusaoExterno = cdUsuarioInclusaoExterno;
    } //-- void setCdUsuarioInclusaoExterno(java.lang.String) 

    /**
     * Sets the value of field 'cdUsuarioManutencao'.
     * 
     * @param cdUsuarioManutencao the value of field
     * 'cdUsuarioManutencao'.
     */
    public void setCdUsuarioManutencao(java.lang.String cdUsuarioManutencao)
    {
        this._cdUsuarioManutencao = cdUsuarioManutencao;
    } //-- void setCdUsuarioManutencao(java.lang.String) 

    /**
     * Sets the value of field 'cdUsuarioManutencaoExterno'.
     * 
     * @param cdUsuarioManutencaoExterno the value of field
     * 'cdUsuarioManutencaoExterno'.
     */
    public void setCdUsuarioManutencaoExterno(java.lang.String cdUsuarioManutencaoExterno)
    {
        this._cdUsuarioManutencaoExterno = cdUsuarioManutencaoExterno;
    } //-- void setCdUsuarioManutencaoExterno(java.lang.String) 

    /**
     * Sets the value of field 'codMensagem'.
     * 
     * @param codMensagem the value of field 'codMensagem'.
     */
    public void setCodMensagem(java.lang.String codMensagem)
    {
        this._codMensagem = codMensagem;
    } //-- void setCodMensagem(java.lang.String) 

    /**
     * Sets the value of field 'dsFormaAutorizacaoPagamento'.
     * 
     * @param dsFormaAutorizacaoPagamento the value of field
     * 'dsFormaAutorizacaoPagamento'.
     */
    public void setDsFormaAutorizacaoPagamento(java.lang.String dsFormaAutorizacaoPagamento)
    {
        this._dsFormaAutorizacaoPagamento = dsFormaAutorizacaoPagamento;
    } //-- void setDsFormaAutorizacaoPagamento(java.lang.String) 

    /**
     * Sets the value of field 'dsFuncionarioGerenteContrato'.
     * 
     * @param dsFuncionarioGerenteContrato the value of field
     * 'dsFuncionarioGerenteContrato'.
     */
    public void setDsFuncionarioGerenteContrato(java.lang.String dsFuncionarioGerenteContrato)
    {
        this._dsFuncionarioGerenteContrato = dsFuncionarioGerenteContrato;
    } //-- void setDsFuncionarioGerenteContrato(java.lang.String) 

    /**
     * Sets the value of field 'dsMotivoSituacaoAditivo'.
     * 
     * @param dsMotivoSituacaoAditivo the value of field
     * 'dsMotivoSituacaoAditivo'.
     */
    public void setDsMotivoSituacaoAditivo(java.lang.String dsMotivoSituacaoAditivo)
    {
        this._dsMotivoSituacaoAditivo = dsMotivoSituacaoAditivo;
    } //-- void setDsMotivoSituacaoAditivo(java.lang.String) 

    /**
     * Sets the value of field 'dsSituacaoAditivoContrato'.
     * 
     * @param dsSituacaoAditivoContrato the value of field
     * 'dsSituacaoAditivoContrato'.
     */
    public void setDsSituacaoAditivoContrato(java.lang.String dsSituacaoAditivoContrato)
    {
        this._dsSituacaoAditivoContrato = dsSituacaoAditivoContrato;
    } //-- void setDsSituacaoAditivoContrato(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoCanalInclusaoTrilha'.
     * 
     * @param dsTipoCanalInclusaoTrilha the value of field
     * 'dsTipoCanalInclusaoTrilha'.
     */
    public void setDsTipoCanalInclusaoTrilha(java.lang.String dsTipoCanalInclusaoTrilha)
    {
        this._dsTipoCanalInclusaoTrilha = dsTipoCanalInclusaoTrilha;
    } //-- void setDsTipoCanalInclusaoTrilha(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoCanalManutencao'.
     * 
     * @param dsTipoCanalManutencao the value of field
     * 'dsTipoCanalManutencao'.
     */
    public void setDsTipoCanalManutencao(java.lang.String dsTipoCanalManutencao)
    {
        this._dsTipoCanalManutencao = dsTipoCanalManutencao;
    } //-- void setDsTipoCanalManutencao(java.lang.String) 

    /**
     * Sets the value of field 'dsUnidadeOrganizacional'.
     * 
     * @param dsUnidadeOrganizacional the value of field
     * 'dsUnidadeOrganizacional'.
     */
    public void setDsUnidadeOrganizacional(java.lang.String dsUnidadeOrganizacional)
    {
        this._dsUnidadeOrganizacional = dsUnidadeOrganizacional;
    } //-- void setDsUnidadeOrganizacional(java.lang.String) 

    /**
     * Sets the value of field 'hrAssinaturaAditivoContrato'.
     * 
     * @param hrAssinaturaAditivoContrato the value of field
     * 'hrAssinaturaAditivoContrato'.
     */
    public void setHrAssinaturaAditivoContrato(java.lang.String hrAssinaturaAditivoContrato)
    {
        this._hrAssinaturaAditivoContrato = hrAssinaturaAditivoContrato;
    } //-- void setHrAssinaturaAditivoContrato(java.lang.String) 

    /**
     * Sets the value of field 'hrInclusaoRegistro'.
     * 
     * @param hrInclusaoRegistro the value of field
     * 'hrInclusaoRegistro'.
     */
    public void setHrInclusaoRegistro(java.lang.String hrInclusaoRegistro)
    {
        this._hrInclusaoRegistro = hrInclusaoRegistro;
    } //-- void setHrInclusaoRegistro(java.lang.String) 

    /**
     * Sets the value of field 'hrInclusaoRegistroTrilha'.
     * 
     * @param hrInclusaoRegistroTrilha the value of field
     * 'hrInclusaoRegistroTrilha'.
     */
    public void setHrInclusaoRegistroTrilha(java.lang.String hrInclusaoRegistroTrilha)
    {
        this._hrInclusaoRegistroTrilha = hrInclusaoRegistroTrilha;
    } //-- void setHrInclusaoRegistroTrilha(java.lang.String) 

    /**
     * Sets the value of field 'hrManutencaoRegistro'.
     * 
     * @param hrManutencaoRegistro the value of field
     * 'hrManutencaoRegistro'.
     */
    public void setHrManutencaoRegistro(java.lang.String hrManutencaoRegistro)
    {
        this._hrManutencaoRegistro = hrManutencaoRegistro;
    } //-- void setHrManutencaoRegistro(java.lang.String) 

    /**
     * Sets the value of field 'mensagem'.
     * 
     * @param mensagem the value of field 'mensagem'.
     */
    public void setMensagem(java.lang.String mensagem)
    {
        this._mensagem = mensagem;
    } //-- void setMensagem(java.lang.String) 

    /**
     * Sets the value of field 'numeroOcorrenciasParticipante'.
     * 
     * @param numeroOcorrenciasParticipante the value of field
     * 'numeroOcorrenciasParticipante'.
     */
    public void setNumeroOcorrenciasParticipante(int numeroOcorrenciasParticipante)
    {
        this._numeroOcorrenciasParticipante = numeroOcorrenciasParticipante;
        this._has_numeroOcorrenciasParticipante = true;
    } //-- void setNumeroOcorrenciasParticipante(int) 

    /**
     * Sets the value of field 'numeroOcorrenciasServicos'.
     * 
     * @param numeroOcorrenciasServicos the value of field
     * 'numeroOcorrenciasServicos'.
     */
    public void setNumeroOcorrenciasServicos(int numeroOcorrenciasServicos)
    {
        this._numeroOcorrenciasServicos = numeroOcorrenciasServicos;
        this._has_numeroOcorrenciasServicos = true;
    } //-- void setNumeroOcorrenciasServicos(int) 

    /**
     * Sets the value of field 'numeroOcorrenciasVinculacao'.
     * 
     * @param numeroOcorrenciasVinculacao the value of field
     * 'numeroOcorrenciasVinculacao'.
     */
    public void setNumeroOcorrenciasVinculacao(int numeroOcorrenciasVinculacao)
    {
        this._numeroOcorrenciasVinculacao = numeroOcorrenciasVinculacao;
        this._has_numeroOcorrenciasVinculacao = true;
    } //-- void setNumeroOcorrenciasVinculacao(int) 

    /**
     * Method setOcorrencias
     * 
     * 
     * 
     * @param index
     * @param vOcorrencias
     */
    public void setOcorrencias(int index, br.com.bradesco.web.pgit.service.data.pdc.detalharaditivocontrato.response.Ocorrencias vOcorrencias)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index >= _ocorrenciasList.size())) {
            throw new IndexOutOfBoundsException("setOcorrencias: Index value '"+index+"' not in range [0.." + (_ocorrenciasList.size() - 1) + "]");
        }
        if (!(index < 35)) {
            throw new IndexOutOfBoundsException("setOcorrencias has a maximum of 35");
        }
        _ocorrenciasList.setElementAt(vOcorrencias, index);
    } //-- void setOcorrencias(int, br.com.bradesco.web.pgit.service.data.pdc.detalharaditivocontrato.response.Ocorrencias) 

    /**
     * Method setOcorrencias
     * 
     * 
     * 
     * @param ocorrenciasArray
     */
    public void setOcorrencias(br.com.bradesco.web.pgit.service.data.pdc.detalharaditivocontrato.response.Ocorrencias[] ocorrenciasArray)
    {
        //-- copy array
        _ocorrenciasList.removeAllElements();
        for (int i = 0; i < ocorrenciasArray.length; i++) {
            _ocorrenciasList.addElement(ocorrenciasArray[i]);
        }
    } //-- void setOcorrencias(br.com.bradesco.web.pgit.service.data.pdc.detalharaditivocontrato.response.Ocorrencias) 

    /**
     * Method setOcorrencias1
     * 
     * 
     * 
     * @param index
     * @param vOcorrencias1
     */
    public void setOcorrencias1(int index, br.com.bradesco.web.pgit.service.data.pdc.detalharaditivocontrato.response.Ocorrencias1 vOcorrencias1)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index >= _ocorrencias1List.size())) {
            throw new IndexOutOfBoundsException("setOcorrencias1: Index value '"+index+"' not in range [0.." + (_ocorrencias1List.size() - 1) + "]");
        }
        if (!(index < 60)) {
            throw new IndexOutOfBoundsException("setOcorrencias1 has a maximum of 60");
        }
        _ocorrencias1List.setElementAt(vOcorrencias1, index);
    } //-- void setOcorrencias1(int, br.com.bradesco.web.pgit.service.data.pdc.detalharaditivocontrato.response.Ocorrencias1) 

    /**
     * Method setOcorrencias1
     * 
     * 
     * 
     * @param ocorrencias1Array
     */
    public void setOcorrencias1(br.com.bradesco.web.pgit.service.data.pdc.detalharaditivocontrato.response.Ocorrencias1[] ocorrencias1Array)
    {
        //-- copy array
        _ocorrencias1List.removeAllElements();
        for (int i = 0; i < ocorrencias1Array.length; i++) {
            _ocorrencias1List.addElement(ocorrencias1Array[i]);
        }
    } //-- void setOcorrencias1(br.com.bradesco.web.pgit.service.data.pdc.detalharaditivocontrato.response.Ocorrencias1) 

    /**
     * Method setOcorrencias2
     * 
     * 
     * 
     * @param index
     * @param vOcorrencias2
     */
    public void setOcorrencias2(int index, br.com.bradesco.web.pgit.service.data.pdc.detalharaditivocontrato.response.Ocorrencias2 vOcorrencias2)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index >= _ocorrencias2List.size())) {
            throw new IndexOutOfBoundsException("setOcorrencias2: Index value '"+index+"' not in range [0.." + (_ocorrencias2List.size() - 1) + "]");
        }
        if (!(index < 30)) {
            throw new IndexOutOfBoundsException("setOcorrencias2 has a maximum of 30");
        }
        _ocorrencias2List.setElementAt(vOcorrencias2, index);
    } //-- void setOcorrencias2(int, br.com.bradesco.web.pgit.service.data.pdc.detalharaditivocontrato.response.Ocorrencias2) 

    /**
     * Method setOcorrencias2
     * 
     * 
     * 
     * @param ocorrencias2Array
     */
    public void setOcorrencias2(br.com.bradesco.web.pgit.service.data.pdc.detalharaditivocontrato.response.Ocorrencias2[] ocorrencias2Array)
    {
        //-- copy array
        _ocorrencias2List.removeAllElements();
        for (int i = 0; i < ocorrencias2Array.length; i++) {
            _ocorrencias2List.addElement(ocorrencias2Array[i]);
        }
    } //-- void setOcorrencias2(br.com.bradesco.web.pgit.service.data.pdc.detalharaditivocontrato.response.Ocorrencias2) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return DetalharAditivoContratoResponse
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.detalharaditivocontrato.response.DetalharAditivoContratoResponse unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.detalharaditivocontrato.response.DetalharAditivoContratoResponse) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.detalharaditivocontrato.response.DetalharAditivoContratoResponse.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharaditivocontrato.response.DetalharAditivoContratoResponse unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
