/*
 * Nome: br.com.bradesco.web.pgit.service.business.combo.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.combo.bean;

/**
 * Nome: ListarTipoPendenciaSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarTipoPendenciaSaidaDTO {
	
	/** Atributo codPendenciaPagamentoIntegrado. */
	private long codPendenciaPagamentoIntegrado;
    
    /** Atributo dsPendenciaPagamentoIntegrado. */
    private String dsPendenciaPagamentoIntegrado;
    
	/**
	 * Listar tipo pendencia saida dto.
	 */
	ListarTipoPendenciaSaidaDTO(){
		super();
	}
	
	
	 /**
 	 * Listar tipo pendencia saida dto.
 	 *
 	 * @param codPendenciaPagamentoIntegrado the cod pendencia pagamento integrado
 	 * @param dsPendenciaPagamentoIntegrado the ds pendencia pagamento integrado
 	 */
 	public ListarTipoPendenciaSaidaDTO(long  codPendenciaPagamentoIntegrado, String dsPendenciaPagamentoIntegrado) {
			super();
			this.codPendenciaPagamentoIntegrado = codPendenciaPagamentoIntegrado;
			this.dsPendenciaPagamentoIntegrado = dsPendenciaPagamentoIntegrado;
		}
    
    

	/**
	 * Get: codPendenciaPagamentoIntegrado.
	 *
	 * @return codPendenciaPagamentoIntegrado
	 */
	public long getCodPendenciaPagamentoIntegrado() {
		return codPendenciaPagamentoIntegrado;
	}


	/**
	 * Set: codPendenciaPagamentoIntegrado.
	 *
	 * @param codPendenciaPagamentoIntegrado the cod pendencia pagamento integrado
	 */
	public void setCodPendenciaPagamentoIntegrado(
			long codPendenciaPagamentoIntegrado) {
		this.codPendenciaPagamentoIntegrado = codPendenciaPagamentoIntegrado;
	}


	/**
	 * Get: dsPendenciaPagamentoIntegrado.
	 *
	 * @return dsPendenciaPagamentoIntegrado
	 */
	public String getDsPendenciaPagamentoIntegrado() {
		return dsPendenciaPagamentoIntegrado;
	}
	
	/**
	 * Set: dsPendenciaPagamentoIntegrado.
	 *
	 * @param dsPendenciaPagamentoIntegrado the ds pendencia pagamento integrado
	 */
	public void setDsPendenciaPagamentoIntegrado(
			String dsPendenciaPagamentoIntegrado) {
		this.dsPendenciaPagamentoIntegrado = dsPendenciaPagamentoIntegrado;
	}
    
    



    
    
    
    
}
