/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/implementation.ftl,v $
 * $Id: implementation.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: implementation.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */
 
package br.com.bradesco.web.pgit.service.business.orgaopagador.impl;

import java.util.ArrayList;
import java.util.List;

import br.com.bradesco.web.pgit.service.business.orgaopagador.IOrgaoPagServiceConstants;
import br.com.bradesco.web.pgit.service.business.orgaopagador.IOrgaoPagadorService;
import br.com.bradesco.web.pgit.service.business.orgaopagador.bean.HistoricoOrgaoPagadorEntradaDTO;
import br.com.bradesco.web.pgit.service.business.orgaopagador.bean.HistoricoOrgaoPagadorSaidaDTO;
import br.com.bradesco.web.pgit.service.business.orgaopagador.bean.OrgaoPagadorDetalheSaidaDTO;
import br.com.bradesco.web.pgit.service.business.orgaopagador.bean.OrgaoPagadorEntradaDTO;
import br.com.bradesco.web.pgit.service.business.orgaopagador.bean.OrgaoPagadorSaidaDTO;
import br.com.bradesco.web.pgit.service.data.pdc.FactoryAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.alterarorgaopagador.request.AlterarOrgaoPagadorRequest;
import br.com.bradesco.web.pgit.service.data.pdc.alterarorgaopagador.response.AlterarOrgaoPagadorResponse;
import br.com.bradesco.web.pgit.service.data.pdc.bloquearorgaopagador.request.BloquearOrgaoPagadorRequest;
import br.com.bradesco.web.pgit.service.data.pdc.bloquearorgaopagador.response.BloquearOrgaoPagadorResponse;
import br.com.bradesco.web.pgit.service.data.pdc.detalharhistoricoorgaopagador.request.DetalharHistoricoOrgaoPagadorRequest;
import br.com.bradesco.web.pgit.service.data.pdc.detalharhistoricoorgaopagador.response.DetalharHistoricoOrgaoPagadorResponse;
import br.com.bradesco.web.pgit.service.data.pdc.detalharorgaopagador.request.DetalharOrgaoPagadorRequest;
import br.com.bradesco.web.pgit.service.data.pdc.detalharorgaopagador.response.DetalharOrgaoPagadorResponse;
import br.com.bradesco.web.pgit.service.data.pdc.excluirorgaopagador.request.ExcluirOrgaoPagadorRequest;
import br.com.bradesco.web.pgit.service.data.pdc.excluirorgaopagador.response.ExcluirOrgaoPagadorResponse;
import br.com.bradesco.web.pgit.service.data.pdc.incluirorgaopagador.request.IncluirOrgaoPagadorRequest;
import br.com.bradesco.web.pgit.service.data.pdc.incluirorgaopagador.response.IncluirOrgaoPagadorResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarhistoricoorgaopagador.request.ListarHistoricoOrgaoPagadorRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarhistoricoorgaopagador.response.ListarHistoricoOrgaoPagadorResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarorgaopagador.request.ListarOrgaoPagadorRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarorgaopagador.response.ListarOrgaoPagadorResponse;
import br.com.bradesco.web.pgit.view.converters.FormatarData;


/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Implementa��o do adaptador: ManterOrgaoPagador
 * </p>
 * 
 * @comment C�DIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public class OrgaoPagadorServiceImpl implements IOrgaoPagadorService {

	/** Atributo factoryAdapter. */
	private FactoryAdapter factoryAdapter;
	
    /**
     * Construtor.
     */
    public OrgaoPagadorServiceImpl() {
    }

	/**
	 * Get: factoryAdapter.
	 *
	 * @return factoryAdapter
	 */
	public FactoryAdapter getFactoryAdapter() {
		return factoryAdapter;
	}

	/**
	 * Set: factoryAdapter.
	 *
	 * @param factoryAdapter the factory adapter
	 */
	public void setFactoryAdapter(FactoryAdapter factoryAdapter) {
		this.factoryAdapter = factoryAdapter;
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.orgaopagador.IOrgaoPagadorService#listarOrgaoPagador(br.com.bradesco.web.pgit.service.business.orgaopagador.bean.OrgaoPagadorEntradaDTO)
	 */
	public List<OrgaoPagadorSaidaDTO> listarOrgaoPagador(OrgaoPagadorEntradaDTO orgaoPagadorEntradaDTO) {
		ListarOrgaoPagadorRequest request = new ListarOrgaoPagadorRequest();
		request.setCdOrgaoPagadorOrganizacao(orgaoPagadorEntradaDTO.getCdOrgaoPagador() != null ? orgaoPagadorEntradaDTO.getCdOrgaoPagador() : 0);
		request.setCdPessoaJuridica(orgaoPagadorEntradaDTO.getCdPessoaJuridica() != null ? orgaoPagadorEntradaDTO.getCdPessoaJuridica() : 0);
		request.setCdSituacaoOrgaoPagador(orgaoPagadorEntradaDTO.getCdSituacao() != null ? orgaoPagadorEntradaDTO.getCdSituacao() : 0);
		request.setCdTarifaOrgaoPagador(orgaoPagadorEntradaDTO.getCdTarifOrgaoPagador() != null ? orgaoPagadorEntradaDTO.getCdTarifOrgaoPagador() : 0);
		request.setCdTipoUnidadeOrganizacao(orgaoPagadorEntradaDTO.getCdTipoUnidadeOrganizacional() != null ? orgaoPagadorEntradaDTO.getCdTipoUnidadeOrganizacional() : 0);
		request.setNrSequenciaUnidadeOrganizacao(orgaoPagadorEntradaDTO.getNrSeqUnidadeOrganizacional() != null ? orgaoPagadorEntradaDTO.getNrSeqUnidadeOrganizacional() : 0);
		request.setMaximoOcorrencias(IOrgaoPagServiceConstants.NUMERO_OCORRENCIAS_LISTAR_ORGAO_PAGADOR);

		ListarOrgaoPagadorResponse response = getFactoryAdapter().getListarOrgaoPagadorPDCAdapter().invokeProcess(request);
		
		if (response == null) {
			return null;
		}

		List<OrgaoPagadorSaidaDTO> listaOrgaoPagador = new ArrayList<OrgaoPagadorSaidaDTO>();
		for (br.com.bradesco.web.pgit.service.data.pdc.listarorgaopagador.response.Ocorrencias saida : response.getOcorrencias()) {
			OrgaoPagadorSaidaDTO orgaoPagador = new OrgaoPagadorSaidaDTO();
			orgaoPagador.setCdOrgaoPagador(saida.getCdOrgaoPagadorOrganizacao());
			orgaoPagador.setCdPessoaJuridica(saida.getCdPessoaJuridica());
			orgaoPagador.setDsPessoaJuridica(saida.getDsPessoaJuridica());
			orgaoPagador.setCdSituacao(saida.getCdSituacaoOrganizacaoPagador());
			orgaoPagador.setCdTipoUnidadeOrganizacional(saida.getCdTipoUnidadeOrganizacao());
			orgaoPagador.setDsNrSeqUnidadeOrganizacional(saida.getDsNumeroSequenciaUnidadeOrganizacao());
			orgaoPagador.setDsTipoUnidadeOrganizacional(saida.getDsTipoUnidadeOrganizacao());
			orgaoPagador.setNrSeqUnidadeOrganizacional(saida.getNrSequenciaUnidadeOrganizacao());
			listaOrgaoPagador.add(orgaoPagador);
		}
		return listaOrgaoPagador;
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.orgaopagador.IOrgaoPagadorService#alterarOrgaoPagador(br.com.bradesco.web.pgit.service.business.orgaopagador.bean.OrgaoPagadorEntradaDTO)
	 */
	public OrgaoPagadorSaidaDTO alterarOrgaoPagador(OrgaoPagadorEntradaDTO orgaoPagadorEntradaDTO) {
		AlterarOrgaoPagadorRequest request = new AlterarOrgaoPagadorRequest();
		request.setCdOrgaoPagador(orgaoPagadorEntradaDTO.getCdOrgaoPagador() != null ? orgaoPagadorEntradaDTO.getCdOrgaoPagador() : 0);
		request.setCdPessoaJuridica(orgaoPagadorEntradaDTO.getCdPessoaJuridica() != null ? orgaoPagadorEntradaDTO.getCdPessoaJuridica() : 0);
		request.setCdTarifOrgaoPagador(orgaoPagadorEntradaDTO.getCdTarifOrgaoPagador() != null ? orgaoPagadorEntradaDTO.getCdTarifOrgaoPagador() : 0);
		request.setCdTipoUnidadeOrganizacional(orgaoPagadorEntradaDTO.getCdTipoUnidadeOrganizacional() != null ? orgaoPagadorEntradaDTO.getCdTipoUnidadeOrganizacional() : 0);
		request.setNrSeqUnidadeOrganizacional(orgaoPagadorEntradaDTO.getNrSeqUnidadeOrganizacional() != null ? orgaoPagadorEntradaDTO.getNrSeqUnidadeOrganizacional() : 0);

		AlterarOrgaoPagadorResponse response = getFactoryAdapter().getAlterarOrgaoPagadorPDCAdapter().invokeProcess(request);

		if (response == null) {
			return null;
		}

		OrgaoPagadorSaidaDTO saidaDTO = new OrgaoPagadorSaidaDTO();
		saidaDTO.setCodMensagem(response.getCodMensagem());
		saidaDTO.setMensagem(response.getMensagem());
		return saidaDTO;
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.orgaopagador.IOrgaoPagadorService#excluirOrgaoPagador(br.com.bradesco.web.pgit.service.business.orgaopagador.bean.OrgaoPagadorEntradaDTO)
	 */
	public OrgaoPagadorSaidaDTO excluirOrgaoPagador(OrgaoPagadorEntradaDTO orgaoPagadorEntradaDTO) {
		ExcluirOrgaoPagadorRequest request = new ExcluirOrgaoPagadorRequest();
		request.setCdOrgaoPagador(orgaoPagadorEntradaDTO.getCdOrgaoPagador() != null ? orgaoPagadorEntradaDTO.getCdOrgaoPagador() : 0);
		request.setHrInclusaoRegistro(orgaoPagadorEntradaDTO.getHrInclusaoRegistro() != null ? orgaoPagadorEntradaDTO.getHrInclusaoRegistro() : "");

		ExcluirOrgaoPagadorResponse response = getFactoryAdapter().getExcluirOrgaoPagadorPDCAdapter().invokeProcess(request);

		if (response == null) {
			return null;
		}

		OrgaoPagadorSaidaDTO saidaDTO = new OrgaoPagadorSaidaDTO();
		saidaDTO.setCodMensagem(response.getCodMensagem());
		saidaDTO.setMensagem(response.getMensagem());
		return saidaDTO;
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.orgaopagador.IOrgaoPagadorService#incluirOrgaoPagador(br.com.bradesco.web.pgit.service.business.orgaopagador.bean.OrgaoPagadorEntradaDTO)
	 */
	public OrgaoPagadorSaidaDTO incluirOrgaoPagador(OrgaoPagadorEntradaDTO orgaoPagadorEntradaDTO) {
		IncluirOrgaoPagadorRequest request = new IncluirOrgaoPagadorRequest();
		request.setCdOrgaoPagador(orgaoPagadorEntradaDTO.getCdOrgaoPagador() != null ? orgaoPagadorEntradaDTO.getCdOrgaoPagador() : 0);
		request.setCdPessoaJuridica(orgaoPagadorEntradaDTO.getCdPessoaJuridica() != null ? orgaoPagadorEntradaDTO.getCdPessoaJuridica() : 0);
		request.setCdTarifOrgaoPagador(orgaoPagadorEntradaDTO.getCdTarifOrgaoPagador() != null ? orgaoPagadorEntradaDTO.getCdTarifOrgaoPagador() : 0);
		request.setCdTipoUnidadeOrganizacional(orgaoPagadorEntradaDTO.getCdTipoUnidadeOrganizacional() != null ? orgaoPagadorEntradaDTO.getCdTipoUnidadeOrganizacional() : 0);
		request.setNrSeqUnidadeOrganizacional(orgaoPagadorEntradaDTO.getNrSeqUnidadeOrganizacional() != null ? orgaoPagadorEntradaDTO.getNrSeqUnidadeOrganizacional() : 0);

		IncluirOrgaoPagadorResponse response = getFactoryAdapter().getIncluirOrgaoPagadorPDCAdapter().invokeProcess(request);

		if (response == null) {
			return null;
		}

		OrgaoPagadorSaidaDTO saidaDTO = new OrgaoPagadorSaidaDTO();
		saidaDTO.setCodMensagem(response.getCodMensagem());
		saidaDTO.setMensagem(response.getMensagem());
		return saidaDTO;
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.orgaopagador.IOrgaoPagadorService#detalharOrgaoPagador(br.com.bradesco.web.pgit.service.business.orgaopagador.bean.OrgaoPagadorEntradaDTO)
	 */
	public OrgaoPagadorDetalheSaidaDTO detalharOrgaoPagador(OrgaoPagadorEntradaDTO orgaoPagadorEntradaDTO) {
		DetalharOrgaoPagadorRequest request = new DetalharOrgaoPagadorRequest();
		request.setCdOrgaoPagador(orgaoPagadorEntradaDTO.getCdOrgaoPagador() != null ? orgaoPagadorEntradaDTO.getCdOrgaoPagador() : 0);
		request.setHrInclusaoRegistroHist(orgaoPagadorEntradaDTO.getHrInclusaoRegistro() != null ? orgaoPagadorEntradaDTO.getHrInclusaoRegistro() : "");

		DetalharOrgaoPagadorResponse response = getFactoryAdapter().getDetalharOrgaoPagadorPDCAdapter().invokeProcess(request);

		if (response == null) {
			return null;
		}

		OrgaoPagadorDetalheSaidaDTO orgaoPagadorSaidaDTO = new OrgaoPagadorDetalheSaidaDTO();
		for (br.com.bradesco.web.pgit.service.data.pdc.detalharorgaopagador.response.Ocorrencias saida : response.getOcorrencias()) {
			orgaoPagadorSaidaDTO.setCdOrgaoPagador(saida.getCdOrgaoPagador());
			orgaoPagadorSaidaDTO.setCdTipoUnidade(saida.getCdTipoUnidade());
			orgaoPagadorSaidaDTO.setDsTipoUnidade(saida.getDsTipoUnidade());
			orgaoPagadorSaidaDTO.setCdPessoa(saida.getCdPessoa());
			orgaoPagadorSaidaDTO.setDsPessoa(saida.getDsPessoa());
			orgaoPagadorSaidaDTO.setNrSeqUnidadeOrganizacional(saida.getNrSeqUnidadeOrganizacional());
			orgaoPagadorSaidaDTO.setDsSeqUnidadeOrganizacional(saida.getDsSeqUnidadeOrganizacional());
			orgaoPagadorSaidaDTO.setCdSituacaoOrgaoPagador(saida.getCdSituacaoOrgaoPagador());
			orgaoPagadorSaidaDTO.setCdTarifOrgaoPagador(saida.getCdTarifOrgaoPagador());
			orgaoPagadorSaidaDTO.setCdCanalInclusao(saida.getCdCanalInclusao());
			orgaoPagadorSaidaDTO.setDsCanalInclusao(saida.getDsCanalInclusao());
			orgaoPagadorSaidaDTO.setCdAutenSegrcInclusao(saida.getCdAutenSegrcInclusao());
			orgaoPagadorSaidaDTO.setNmOperFluxoInclusao(saida.getNmOperFluxoInclusao());
			orgaoPagadorSaidaDTO.setHrInclusaoRegistro(saida.getHrInclusaoRegistro());
			orgaoPagadorSaidaDTO.setCdCanalManutencao(saida.getCdCanalManutencao());
			orgaoPagadorSaidaDTO.setDsCanalManutencao(saida.getDsCanalManutencao());
			orgaoPagadorSaidaDTO.setCdAutenSegrcManutencao(saida.getCdAutenSegrcManutencao());
			orgaoPagadorSaidaDTO.setNmOperFluxoManutencao(saida.getNmOperFluxoManutencao());
			orgaoPagadorSaidaDTO.setHrManutencaoRegistro(saida.getHrManutencaoRegistro());
			orgaoPagadorSaidaDTO.setDsMotivoBloqueio(saida.getDsMotivoBloqueio());
			return orgaoPagadorSaidaDTO;
		}

		return orgaoPagadorSaidaDTO;
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.orgaopagador.IOrgaoPagadorService#bloquearOrgaoPagador(br.com.bradesco.web.pgit.service.business.orgaopagador.bean.OrgaoPagadorEntradaDTO)
	 */
	public OrgaoPagadorSaidaDTO bloquearOrgaoPagador(OrgaoPagadorEntradaDTO orgaoPagadorEntradaDTO) {
		BloquearOrgaoPagadorRequest request = new BloquearOrgaoPagadorRequest();
		request.setCdOrgaoPagador(orgaoPagadorEntradaDTO.getCdOrgaoPagador() != null ? orgaoPagadorEntradaDTO.getCdOrgaoPagador() : 0);
		request.setCdSituacao(orgaoPagadorEntradaDTO.getCdSituacao() != null ? orgaoPagadorEntradaDTO.getCdSituacao() : 0);
		request.setDsMotivoBloqueio(orgaoPagadorEntradaDTO.getDsMotivoBloqueio() != null ? orgaoPagadorEntradaDTO.getDsMotivoBloqueio() : "");

		BloquearOrgaoPagadorResponse response = getFactoryAdapter().getBloquearOrgaoPagadorPDCAdapter().invokeProcess(request);

		if (response == null) {
			return null;
		}

		OrgaoPagadorSaidaDTO saidaDTO = new OrgaoPagadorSaidaDTO();
		saidaDTO.setCodMensagem(response.getCodMensagem());
		saidaDTO.setMensagem(response.getMensagem());
		return saidaDTO;
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.orgaopagador.IOrgaoPagadorService#detalharHistoricoOrgaoPagador(br.com.bradesco.web.pgit.service.business.orgaopagador.bean.HistoricoOrgaoPagadorEntradaDTO)
	 */
	public List<HistoricoOrgaoPagadorSaidaDTO> detalharHistoricoOrgaoPagador(HistoricoOrgaoPagadorEntradaDTO historicoOrgaoPagadorEntradaDTO) {
		DetalharHistoricoOrgaoPagadorRequest request = new DetalharHistoricoOrgaoPagadorRequest();
		request.setCdOrgaoPagador(historicoOrgaoPagadorEntradaDTO.getCdOrgaoPagador() != null ? historicoOrgaoPagadorEntradaDTO.getCdOrgaoPagador() : 0);
		request.setHrInclusaoRegistro(historicoOrgaoPagadorEntradaDTO.getHrInclusaoRegistro() != null ? historicoOrgaoPagadorEntradaDTO.getHrInclusaoRegistro() : "");

		DetalharHistoricoOrgaoPagadorResponse response = getFactoryAdapter().getDetalharHistoricoOrgaoPagadorPDCAdapter().invokeProcess(request);
		
		if (response == null) {
			return null;
		}

		List<HistoricoOrgaoPagadorSaidaDTO> listaOrgaoPagador = new ArrayList<HistoricoOrgaoPagadorSaidaDTO>();
		for (br.com.bradesco.web.pgit.service.data.pdc.detalharhistoricoorgaopagador.response.Ocorrencias saida : response.getOcorrencias()) {
			HistoricoOrgaoPagadorSaidaDTO orgaoPagador = new HistoricoOrgaoPagadorSaidaDTO();
			orgaoPagador.setCdOrgaoPagador(saida.getCdOrgaoPagador());
			orgaoPagador.setCdTipoUnidadeOrganizacional(saida.getCdTipoUnidade());
			orgaoPagador.setDsTipoUnidadeOrganizacional(saida.getDsTipoUnidade());
			orgaoPagador.setCdPessoaJuridica(saida.getCdPessoa());
			orgaoPagador.setDsPessoJuridica(saida.getDsPessoa());
			orgaoPagador.setNrSeqUnidadeOrganizacional(saida.getNrSeqUnidade());
			orgaoPagador.setDsSeqUnidadeOrganizacional(saida.getDsSeqUnidade());
			orgaoPagador.setDsSituacao(saida.getCdSituacaoOrgaoPagador());
			orgaoPagador.setCdTarifOrgaoPagador(saida.getCdTarifOrgaoPagador());
			orgaoPagador.setCdCanalInclusao(saida.getCdCanalInclusao() != 0 ? saida.getCdCanalInclusao() : null);
			orgaoPagador.setDsCanalInclusao(saida.getDsCanalInclusao());
			orgaoPagador.setCdAutenSegrcInclusao(saida.getCdAutenSegrcInclusao());
			orgaoPagador.setNmOperFluxoInclusao(!"0".equals(saida.getNmOperFluxoInclusao()) ? saida.getNmOperFluxoInclusao() : "");
			orgaoPagador.setHrInclusaoRegistro(saida.getHrInclusaoRegistro());
			orgaoPagador.setCdCanalManutencao(saida.getCdCanalManutencao() != 0 ? saida.getCdCanalManutencao() : null);
			orgaoPagador.setDsCanalManutencao(saida.getDsCanalManutencao());
			orgaoPagador.setCdAutenSegrcManutencao(saida.getCdAutenSegrcManutencao());
			orgaoPagador.setNmOperFluxoManutencao(!"0".equals(saida.getNmOperFluxoManutencao()) ? saida.getNmOperFluxoManutencao() : "");
			orgaoPagador.setHrManutencaoRegistro(saida.getHrManutencaoRegistro());
			orgaoPagador.setDsMotivoBloqueio(saida.getDsMotivoBloqueio());

			listaOrgaoPagador.add(orgaoPagador);
		}
		return listaOrgaoPagador;
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.orgaopagador.IOrgaoPagadorService#listarHistoricoOrgaoPagador(br.com.bradesco.web.pgit.service.business.orgaopagador.bean.HistoricoOrgaoPagadorEntradaDTO)
	 */
	public List<HistoricoOrgaoPagadorSaidaDTO> listarHistoricoOrgaoPagador(HistoricoOrgaoPagadorEntradaDTO historicoOrgaoPagadorEntradaDTO) {
		ListarHistoricoOrgaoPagadorRequest request = new ListarHistoricoOrgaoPagadorRequest();
		request.setCdOrgaoPagador(historicoOrgaoPagadorEntradaDTO.getCdOrgaoPagador() != null ? historicoOrgaoPagadorEntradaDTO.getCdOrgaoPagador() : 0);
		request.setDtDe(historicoOrgaoPagadorEntradaDTO.getDtDe() != null ? FormatarData.formataDiaMesAnoToPdc(historicoOrgaoPagadorEntradaDTO.getDtDe()) : "");
		request.setDtAte(historicoOrgaoPagadorEntradaDTO.getDtAte() != null ? FormatarData.formataDiaMesAnoToPdc(historicoOrgaoPagadorEntradaDTO.getDtAte()) : "");
		request.setQtConsultas(IOrgaoPagServiceConstants.NUMERO_OCORRENCIAS_LISTAR_HIST_ORGAO_PAGADOR);

		ListarHistoricoOrgaoPagadorResponse response = getFactoryAdapter().getListarHistoricoOrgaoPagadorPDCAdapter().invokeProcess(request);
		
		if (response == null) {
			return null;
		}

		List<HistoricoOrgaoPagadorSaidaDTO> listaOrgaoPagador = new ArrayList<HistoricoOrgaoPagadorSaidaDTO>();
		for (br.com.bradesco.web.pgit.service.data.pdc.listarhistoricoorgaopagador.response.Ocorrencias saida : response.getOcorrencias()) {
			HistoricoOrgaoPagadorSaidaDTO orgaoPagador = new HistoricoOrgaoPagadorSaidaDTO();
			orgaoPagador.setCdOrgaoPagador(saida.getCdOrgaoPagador());
			orgaoPagador.setHorario(saida.getHorario());
			orgaoPagador.setCdTipoUnidadeOrganizacional(saida.getCdTipoUnidadeOrganizacional());
			orgaoPagador.setDsTipoUnidadeOrganizacional(saida.getDsTipoUnidadeOrganizacional());
			orgaoPagador.setCdPessoaJuridica(saida.getCdPessoaJuridica());
			orgaoPagador.setDsPessoJuridica(saida.getDsPessoJuridica());
			orgaoPagador.setNrSeqUnidadeOrganizacional(saida.getNrSeqUnidadeOrganizacional());
			orgaoPagador.setDsSeqUnidadeOrganizacional(saida.getDsSeqUnidadeOrganizacional());
			orgaoPagador.setDsSituacao(saida.getDsSituacao());
			orgaoPagador.setDsUsuarioResponsavel(saida.getDsUsuarioResponsavel());
			orgaoPagador.setCdIndicadorTipoManutencao(saida.getCdIndicadorTipoManutencao());
			listaOrgaoPagador.add(orgaoPagador);
		}
		return listaOrgaoPagador;
	}
}

