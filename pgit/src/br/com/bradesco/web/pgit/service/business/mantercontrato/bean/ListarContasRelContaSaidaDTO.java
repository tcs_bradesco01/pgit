/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantercontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantercontrato.bean;

/**
 * Nome: ListarContasRelContaSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarContasRelContaSaidaDTO {
	
	/** Atributo cdPessoaJuridicaRelacionada. */
	private Long cdPessoaJuridicaRelacionada;
	
	/** Atributo cdTipoContratoRelacionada. */
	private Integer cdTipoContratoRelacionada;
	
	/** Atributo nrSequenciaContratoRelacionada. */
	private Long nrSequenciaContratoRelacionada;
	
	/** Atributo cdTipoRelacionamento. */
	private Integer cdTipoRelacionamento;
	
	/** Atributo dsTipoRelacionamento. */
	private String dsTipoRelacionamento;
	
	/** Atributo cdPessoa. */
	private Long cdPessoa;
	
	/** Atributo dsPessoa. */
	private String dsPessoa;
	
	/** Atributo cdCpfCnpj. */
	private String cdCpfCnpj;
	
	/** Atributo cdBanco. */
	private Integer cdBanco;
	
	/** Atributo dsBanco. */
	private String dsBanco;
	
	/** Atributo cdAgenciaContabil. */
	private Integer cdAgenciaContabil;
	
	/** Atributo dsAgenciaContabil. */
	private String dsAgenciaContabil;
	
	/** Atributo cdConta. */
	private Long cdConta;
	
	/** Atributo cdDigitoConta. */
	private String cdDigitoConta;
	
	/** Atributo cdTipoConta. */
	private Integer cdTipoConta;
	
	/** Atributo dsTipoConta. */
	private String dsTipoConta;
	
	/** Atributo codMensagem. */
	private String codMensagem;
	
	/** Atributo mensagem. */
	private String mensagem;
	
	/** Atributo bancoFormatado. */
	private String bancoFormatado;
	
	/** Atributo agenciaFormatada. */
	private String agenciaFormatada;
	
	/** Atributo contaFormatada. */
	private String contaFormatada;
	
	/**
	 * Get: agenciaFormatada.
	 *
	 * @return agenciaFormatada
	 */
	public String getAgenciaFormatada() {
	    return agenciaFormatada;
	}
	
	/**
	 * Set: agenciaFormatada.
	 *
	 * @param agenciaFormatada the agencia formatada
	 */
	public void setAgenciaFormatada(String agenciaFormatada) {
	    this.agenciaFormatada = agenciaFormatada;
	}
	
	/**
	 * Get: bancoFormatado.
	 *
	 * @return bancoFormatado
	 */
	public String getBancoFormatado() {
	    return bancoFormatado;
	}
	
	/**
	 * Set: bancoFormatado.
	 *
	 * @param bancoFormatado the banco formatado
	 */
	public void setBancoFormatado(String bancoFormatado) {
	    this.bancoFormatado = bancoFormatado;
	}
	
	/**
	 * Get: contaFormatada.
	 *
	 * @return contaFormatada
	 */
	public String getContaFormatada() {
	    return contaFormatada;
	}
	
	/**
	 * Set: contaFormatada.
	 *
	 * @param contaFormatada the conta formatada
	 */
	public void setContaFormatada(String contaFormatada) {
	    this.contaFormatada = contaFormatada;
	}
	
	/**
	 * Get: cdAgenciaContabil.
	 *
	 * @return cdAgenciaContabil
	 */
	public Integer getCdAgenciaContabil() {
		return cdAgenciaContabil;
	}
	
	/**
	 * Set: cdAgenciaContabil.
	 *
	 * @param cdAgenciaContabil the cd agencia contabil
	 */
	public void setCdAgenciaContabil(Integer cdAgenciaContabil) {
		this.cdAgenciaContabil = cdAgenciaContabil;
	}
	
	/**
	 * Get: cdBanco.
	 *
	 * @return cdBanco
	 */
	public Integer getCdBanco() {
		return cdBanco;
	}
	
	/**
	 * Set: cdBanco.
	 *
	 * @param cdBanco the cd banco
	 */
	public void setCdBanco(Integer cdBanco) {
		this.cdBanco = cdBanco;
	}
	
	/**
	 * Get: cdConta.
	 *
	 * @return cdConta
	 */
	public Long getCdConta() {
		return cdConta;
	}
	
	/**
	 * Set: cdConta.
	 *
	 * @param cdConta the cd conta
	 */
	public void setCdConta(Long cdConta) {
		this.cdConta = cdConta;
	}
	
	/**
	 * Get: cdCpfCnpj.
	 *
	 * @return cdCpfCnpj
	 */
	public String getCdCpfCnpj() {
		return cdCpfCnpj;
	}
	
	/**
	 * Set: cdCpfCnpj.
	 *
	 * @param cdCpfCnpj the cd cpf cnpj
	 */
	public void setCdCpfCnpj(String cdCpfCnpj) {
		this.cdCpfCnpj = cdCpfCnpj;
	}
	
	/**
	 * Get: cdDigitoConta.
	 *
	 * @return cdDigitoConta
	 */
	public String getCdDigitoConta() {
		return cdDigitoConta;
	}
	
	/**
	 * Set: cdDigitoConta.
	 *
	 * @param cdDigitoConta the cd digito conta
	 */
	public void setCdDigitoConta(String cdDigitoConta) {
		this.cdDigitoConta = cdDigitoConta;
	}
	
	/**
	 * Get: cdPessoa.
	 *
	 * @return cdPessoa
	 */
	public Long getCdPessoa() {
		return cdPessoa;
	}
	
	/**
	 * Set: cdPessoa.
	 *
	 * @param cdPessoa the cd pessoa
	 */
	public void setCdPessoa(Long cdPessoa) {
		this.cdPessoa = cdPessoa;
	}
	
	/**
	 * Get: cdPessoaJuridicaRelacionada.
	 *
	 * @return cdPessoaJuridicaRelacionada
	 */
	public Long getCdPessoaJuridicaRelacionada() {
		return cdPessoaJuridicaRelacionada;
	}
	
	/**
	 * Set: cdPessoaJuridicaRelacionada.
	 *
	 * @param cdPessoaJuridicaRelacionada the cd pessoa juridica relacionada
	 */
	public void setCdPessoaJuridicaRelacionada(Long cdPessoaJuridicaRelacionada) {
		this.cdPessoaJuridicaRelacionada = cdPessoaJuridicaRelacionada;
	}
	
	/**
	 * Get: cdTipoConta.
	 *
	 * @return cdTipoConta
	 */
	public Integer getCdTipoConta() {
		return cdTipoConta;
	}
	
	/**
	 * Set: cdTipoConta.
	 *
	 * @param cdTipoConta the cd tipo conta
	 */
	public void setCdTipoConta(Integer cdTipoConta) {
		this.cdTipoConta = cdTipoConta;
	}
	
	/**
	 * Get: cdTipoContratoRelacionada.
	 *
	 * @return cdTipoContratoRelacionada
	 */
	public Integer getCdTipoContratoRelacionada() {
		return cdTipoContratoRelacionada;
	}
	
	/**
	 * Set: cdTipoContratoRelacionada.
	 *
	 * @param cdTipoContratoRelacionada the cd tipo contrato relacionada
	 */
	public void setCdTipoContratoRelacionada(Integer cdTipoContratoRelacionada) {
		this.cdTipoContratoRelacionada = cdTipoContratoRelacionada;
	}
	
	/**
	 * Get: cdTipoRelacionamento.
	 *
	 * @return cdTipoRelacionamento
	 */
	public Integer getCdTipoRelacionamento() {
		return cdTipoRelacionamento;
	}
	
	/**
	 * Set: cdTipoRelacionamento.
	 *
	 * @param cdTipoRelacionamento the cd tipo relacionamento
	 */
	public void setCdTipoRelacionamento(Integer cdTipoRelacionamento) {
		this.cdTipoRelacionamento = cdTipoRelacionamento;
	}
	
	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}
	
	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}
	
	/**
	 * Get: dsAgenciaContabil.
	 *
	 * @return dsAgenciaContabil
	 */
	public String getDsAgenciaContabil() {
		return dsAgenciaContabil;
	}
	
	/**
	 * Set: dsAgenciaContabil.
	 *
	 * @param dsAgenciaContabil the ds agencia contabil
	 */
	public void setDsAgenciaContabil(String dsAgenciaContabil) {
		this.dsAgenciaContabil = dsAgenciaContabil;
	}
	
	/**
	 * Get: dsBanco.
	 *
	 * @return dsBanco
	 */
	public String getDsBanco() {
		return dsBanco;
	}
	
	/**
	 * Set: dsBanco.
	 *
	 * @param dsBanco the ds banco
	 */
	public void setDsBanco(String dsBanco) {
		this.dsBanco = dsBanco;
	}
	
	/**
	 * Get: dsPessoa.
	 *
	 * @return dsPessoa
	 */
	public String getDsPessoa() {
		return dsPessoa;
	}
	
	/**
	 * Set: dsPessoa.
	 *
	 * @param dsPessoa the ds pessoa
	 */
	public void setDsPessoa(String dsPessoa) {
		this.dsPessoa = dsPessoa;
	}
	
	/**
	 * Get: dsTipoConta.
	 *
	 * @return dsTipoConta
	 */
	public String getDsTipoConta() {
		return dsTipoConta;
	}
	
	/**
	 * Set: dsTipoConta.
	 *
	 * @param dsTipoConta the ds tipo conta
	 */
	public void setDsTipoConta(String dsTipoConta) {
		this.dsTipoConta = dsTipoConta;
	}
	
	/**
	 * Get: dsTipoRelacionamento.
	 *
	 * @return dsTipoRelacionamento
	 */
	public String getDsTipoRelacionamento() {
		return dsTipoRelacionamento;
	}
	
	/**
	 * Set: dsTipoRelacionamento.
	 *
	 * @param dsTipoRelacionamento the ds tipo relacionamento
	 */
	public void setDsTipoRelacionamento(String dsTipoRelacionamento) {
		this.dsTipoRelacionamento = dsTipoRelacionamento;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	/**
	 * Get: nrSequenciaContratoRelacionada.
	 *
	 * @return nrSequenciaContratoRelacionada
	 */
	public Long getNrSequenciaContratoRelacionada() {
		return nrSequenciaContratoRelacionada;
	}
	
	/**
	 * Set: nrSequenciaContratoRelacionada.
	 *
	 * @param nrSequenciaContratoRelacionada the nr sequencia contrato relacionada
	 */
	public void setNrSequenciaContratoRelacionada(
			Long nrSequenciaContratoRelacionada) {
		this.nrSequenciaContratoRelacionada = nrSequenciaContratoRelacionada;
	}
	
	
	
}
