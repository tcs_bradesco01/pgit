/*
 * Nome: br.com.bradesco.web.pgit.service.business.assolotelayoutarquivo.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.assolotelayoutarquivo.bean;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.apache.commons.lang.StringUtils;

import br.com.bradesco.web.aq.application.error.BradescoViewException.BradescoViewExceptionActionType;
import br.com.bradesco.web.aq.application.log.ILogManager;
import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterFunctionalException;
import br.com.bradesco.web.aq.application.util.faces.BradescoFacesUtils;
import br.com.bradesco.web.pgit.service.business.assolotelayoutarquivo.IAssoLoteLayoutArquivoService;
import br.com.bradesco.web.pgit.service.business.assolotelayoutarquivo.IAssoLoteLayoutArquivoServiceConstants;
import br.com.bradesco.web.pgit.service.business.combo.IComboService;
import br.com.bradesco.web.pgit.service.business.combo.bean.TipoLayoutArquivoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.TipoLayoutArquivoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.TipoLoteEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.TipoLoteSaidaDTO;

/**
 * Nome: AssociacaoLoteLayoutArquivoBean
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class AssociacaoLoteLayoutArquivoBean {
	
	/** Atributo filtroTipoLote. */
	private Integer filtroTipoLote;
	
	/** Atributo filtroTipoLayoutArquivo. */
	private Integer filtroTipoLayoutArquivo;
	
	/** Atributo filtroIncluirTipoLote. */
	private Integer filtroIncluirTipoLote;
	
	/** Atributo filtroIncluirTipoLayoutArquivo. */
	private Integer filtroIncluirTipoLayoutArquivo;
	
	/** Atributo tipoLote. */
	private String tipoLote;
	
	/** Atributo tipoLayoutArquivo. */
	private String tipoLayoutArquivo;
	
	/** Atributo dataHoraManutencao. */
	private String dataHoraManutencao;
	
	/** Atributo usuarioManutencao. */
	private String usuarioManutencao;
	
	/** Atributo usuarioInclusao. */
	private String usuarioInclusao;
	
	/** Atributo dataHoraInclusao. */
	private String dataHoraInclusao;
	
	/** Atributo tipoCanalInclusao. */
	private String tipoCanalInclusao;
	
	/** Atributo complementoInclusao. */
	private String complementoInclusao;
	
	/** Atributo tipoCanalManutencao. */
	private String tipoCanalManutencao;
	
	/** Atributo complementoManutencao. */
	private String complementoManutencao;
	//private AssLoteLayoutArquivoSaidaDTO itemSelecionadoLista;
	/** Atributo itemSelecionadoLista. */
	private Integer itemSelecionadoLista;
	
	/** Atributo assLoteLayoutArquivoEntradaDTO. */
	private AssLoteLayoutArquivoEntradaDTO assLoteLayoutArquivoEntradaDTO = new AssLoteLayoutArquivoEntradaDTO();
	
	/** Atributo tipoLoteDesc. */
	private String tipoLoteDesc;
	
	/** Atributo tipoLayoutArquivoDesc. */
	private String tipoLayoutArquivoDesc;
	
	/** Atributo logger. */
	private ILogManager logger;
	
	/** Atributo listaControle. */
	private List<SelectItem> listaControle = new ArrayList<SelectItem>();
	
	/** Atributo listaGrid. */
	private List<AssLoteLayoutArquivoSaidaDTO> listaGrid;
	
	/** Atributo listaTipoLote. */
	private List<SelectItem> listaTipoLote = new ArrayList<SelectItem>();
	
	/** Atributo listaTipoLayoutArquivo. */
	private List<SelectItem> listaTipoLayoutArquivo =  new ArrayList<SelectItem>();
	
	/** Atributo mostraBotoes. */
	private boolean mostraBotoes;
	
	/** Atributo comboService. */
	private IComboService comboService;
	
	/** Atributo associacaoLoteLayoutArquivoService. */
	private IAssoLoteLayoutArquivoService associacaoLoteLayoutArquivoService;
	
	/** Atributo listaTipoLoteHash. */
	private Map<Integer, String> listaTipoLoteHash = new HashMap<Integer, String>();
	
	/** Atributo listaTipoLayoutArquivoHash. */
	private Map<Integer, String> listaTipoLayoutArquivoHash = new HashMap<Integer, String>();
	
	/** Atributo hiddenConfirma. */
	private boolean hiddenConfirma;
	
	/** Atributo btoAcionado. */
	private Boolean btoAcionado;
	
	/** Atributo saidaTipoLayoutArquivoSaidaDTO. */
	private TipoLayoutArquivoSaidaDTO saidaTipoLayoutArquivoSaidaDTO;
	
	
	/**
	 * Iniciar tela.
	 *
	 * @param e the e
	 */
	public void iniciarTela(ActionEvent e){
		
		setFiltroTipoLayoutArquivo(null);
		setFiltroTipoLote(null);	
		setListaGrid(null);
		setItemSelecionadoLista(null);
		preencheTipoLayoutArquivo();
		preencheTipoLote();
		setBtoAcionado(false);
	}
	
	/**
	 * Pesquisar.
	 *
	 * @param e the e
	 * @return the string
	 */
	public String pesquisar(ActionEvent e){
		return "";
	}
	
	/**
	 * Pesquisa paginar.
	 *
	 * @return the list< ass lote layout arquivo saida dt o>
	 */
	@SuppressWarnings("unchecked")
	public List<AssLoteLayoutArquivoSaidaDTO> pesquisaPaginar() {
		try{
			return getAssociacaoLoteLayoutArquivoService().listarAssociacaoLoteLayoutArquivo(assLoteLayoutArquivoEntradaDTO);
		}catch (PdcAdapterFunctionalException e){
			return new ArrayList<AssLoteLayoutArquivoSaidaDTO>();
		}
	}
	
	/**
	 * Is mostra botoes.
	 *
	 * @return true, if is mostra botoes
	 */
	public boolean isMostraBotoes() {
		return mostraBotoes;
	}

	/**
	 * Set: mostraBotoes.
	 *
	 * @param mostraBotoes the mostra botoes
	 */
	public void setMostraBotoes(boolean mostraBotoes) {
		this.mostraBotoes = mostraBotoes;
	}

	/**
	 * Get: filtroIncluirTipoLayoutArquivo.
	 *
	 * @return filtroIncluirTipoLayoutArquivo
	 */
	public Integer getFiltroIncluirTipoLayoutArquivo() {
		return filtroIncluirTipoLayoutArquivo;
	}
	
	/**
	 * Set: filtroIncluirTipoLayoutArquivo.
	 *
	 * @param filtroIncluirTipoLayoutArquivo the filtro incluir tipo layout arquivo
	 */
	public void setFiltroIncluirTipoLayoutArquivo(
			Integer filtroIncluirTipoLayoutArquivo) {
		this.filtroIncluirTipoLayoutArquivo = filtroIncluirTipoLayoutArquivo;
	}
	
	/**
	 * Get: filtroIncluirTipoLote.
	 *
	 * @return filtroIncluirTipoLote
	 */
	public Integer getFiltroIncluirTipoLote() {
		return filtroIncluirTipoLote;
	}
	
	/**
	 * Set: filtroIncluirTipoLote.
	 *
	 * @param filtroIncluirTipoLote the filtro incluir tipo lote
	 */
	public void setFiltroIncluirTipoLote(Integer filtroIncluirTipoLote) {
		this.filtroIncluirTipoLote = filtroIncluirTipoLote;
	}
	
	/**
	 * Get: filtroTipoLayoutArquivo.
	 *
	 * @return filtroTipoLayoutArquivo
	 */
	public Integer getFiltroTipoLayoutArquivo() {
		return filtroTipoLayoutArquivo;
	}
	
	/**
	 * Set: filtroTipoLayoutArquivo.
	 *
	 * @param filtroTipoLayoutArquivo the filtro tipo layout arquivo
	 */
	public void setFiltroTipoLayoutArquivo(Integer filtroTipoLayoutArquivo) {
		this.filtroTipoLayoutArquivo = filtroTipoLayoutArquivo;
	}
	
	/**
	 * Get: filtroTipoLote.
	 *
	 * @return filtroTipoLote
	 */
	public Integer getFiltroTipoLote() {
		return filtroTipoLote;
	}
	
	/**
	 * Set: filtroTipoLote.
	 *
	 * @param filtroTipoLote the filtro tipo lote
	 */
	public void setFiltroTipoLote(Integer filtroTipoLote) {
		this.filtroTipoLote = filtroTipoLote;
	}	
	
	/**
	 * Get: tipoLayoutArquivo.
	 *
	 * @return tipoLayoutArquivo
	 */
	public String getTipoLayoutArquivo() {
		return tipoLayoutArquivo;
	}
	
	/**
	 * Set: tipoLayoutArquivo.
	 *
	 * @param tipoLayoutArquivo the tipo layout arquivo
	 */
	public void setTipoLayoutArquivo(String tipoLayoutArquivo) {
		this.tipoLayoutArquivo = tipoLayoutArquivo;
	}
	
	/**
	 * Get: tipoLote.
	 *
	 * @return tipoLote
	 */
	public String getTipoLote() {
		return tipoLote;
	}
	
	/**
	 * Set: tipoLote.
	 *
	 * @param tipoLote the tipo lote
	 */
	public void setTipoLote(String tipoLote) {
		this.tipoLote = tipoLote;
	}
	
	/**
	 * Get: dataHoraManutencao.
	 *
	 * @return dataHoraManutencao
	 */
	public String getDataHoraManutencao() {
		return dataHoraManutencao;
	}
	
	/**
	 * Set: dataHoraManutencao.
	 *
	 * @param dataHora the data hora manutencao
	 */
	public void setDataHoraManutencao(String dataHora) {
		this.dataHoraManutencao = dataHora;
	}
	
	/**
	 * Get: usuarioManutencao.
	 *
	 * @return usuarioManutencao
	 */
	public String getUsuarioManutencao() {
		return usuarioManutencao;
	}
	
	/**
	 * Set: usuarioManutencao.
	 *
	 * @param usuario the usuario manutencao
	 */
	public void setUsuarioManutencao(String usuario) {
		this.usuarioManutencao = usuario;
	}
	
	/**
	 * Limpar campos.
	 */
	public void limparCampos(){
		this.setFiltroTipoLayoutArquivo(null);
		this.setFiltroTipoLote(null);
		setListaGrid(null);
		setBtoAcionado(false);
		setItemSelecionadoLista(null);
	}
	
	/**
	 * Consultar.
	 *
	 * @return the string
	 */
	public String consultar(){		
		this.carregarLista();		
				
		return "";
	}
	
	/**
	 * Carregar lista.
	 */
	private void carregarLista() {
		try{
			listaGrid =new ArrayList<AssLoteLayoutArquivoSaidaDTO>();
			assLoteLayoutArquivoEntradaDTO.setCdTipoLayoutArquivo(getFiltroTipoLayoutArquivo());
			assLoteLayoutArquivoEntradaDTO.setCdTipoLoteLayout(getFiltroTipoLote());
			assLoteLayoutArquivoEntradaDTO.setQtConsultas(IAssoLoteLayoutArquivoServiceConstants.QTDE_CONSULTAS);
			
			setListaGrid(getAssociacaoLoteLayoutArquivoService().listarAssociacaoLoteLayoutArquivo(assLoteLayoutArquivoEntradaDTO));
			setBtoAcionado(true);

			listaControle = new ArrayList<SelectItem>();
			
			for(int i = 0; i < listaGrid.size(); i++){
				listaControle.add(new SelectItem(i,""));
			}
			
			setItemSelecionadoLista(null);
		}catch (PdcAdapterFunctionalException p) {
				BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
				setListaGrid(null);
				setItemSelecionadoLista(null);

		}
		
	}
	
	/**
	 * Carregar lista sucesso.
	 */
	@SuppressWarnings("unused")
	private void carregarListaSucesso() {
		try {
			listaGrid =new ArrayList<AssLoteLayoutArquivoSaidaDTO>();
			
			assLoteLayoutArquivoEntradaDTO.setCdTipoLayoutArquivo(getFiltroTipoLayoutArquivo());
			assLoteLayoutArquivoEntradaDTO.setCdTipoLoteLayout(getFiltroTipoLote());
			assLoteLayoutArquivoEntradaDTO.setQtConsultas(IAssoLoteLayoutArquivoServiceConstants.QTDE_CONSULTAS);
			
			setListaGrid(getAssociacaoLoteLayoutArquivoService().listarAssociacaoLoteLayoutArquivo(assLoteLayoutArquivoEntradaDTO));
			
			listaControle = new ArrayList<SelectItem>();
			
			for(int i = 0; i < listaGrid.size(); i++){
				listaControle.add(new SelectItem(i,""));
			}
			
			
			setItemSelecionadoLista(null);
			
		}catch(PdcAdapterFunctionalException e){
			if(!StringUtils.right(e.getCode(), 8).equals("PGIT0003")){
				this.setListaGrid(null);
				throw e;
			}
		}
			
	}
	
	/**
	 * Voltar pesquisar.
	 *
	 * @return the string
	 */
	public String voltarPesquisar(){
		setItemSelecionadoLista(null);
		return "VOLTAR_PESQUISAR";
	}
	
	/**
	 * Confirmar excluir.
	 *
	 * @return the string
	 */
	public String confirmarExcluir(){
		AssLoteLayoutArquivoEntradaDTO  entradaDTO = new AssLoteLayoutArquivoEntradaDTO();
		entradaDTO.setCdTipoLayoutArquivo(Integer.parseInt(getTipoLayoutArquivo()));
		entradaDTO.setCdTipoLoteLayout(Integer.parseInt(getTipoLote()));
		entradaDTO.setQtConsultas(1);
		
		AssLoteLayoutArquivoSaidaDTO assLoteLayoutArquivoSaidaDTO = getAssociacaoLoteLayoutArquivoService().excluirAssociacaoLoteLayoutArquivo(entradaDTO);
		BradescoFacesUtils.addInfoModalMessage("(" + assLoteLayoutArquivoSaidaDTO.getCodMensagem() + ") " + assLoteLayoutArquivoSaidaDTO.getMensagem() ,  "conAssociacaoLoteLayoutArquivo", BradescoViewExceptionActionType.ACTION, false);
			
		carregarListaSucesso();
		return "";
	}
	
	/**
	 * Avancar incluir.
	 *
	 * @return the string
	 */
	public String avancarIncluir(){
		if (getFiltroIncluirTipoLote() == 0 || getFiltroIncluirTipoLote() == null){
			BradescoFacesUtils.addInfoModalMessage( "Informe o Tipo Lote", false);
			return null;
		}else if(getFiltroIncluirTipoLayoutArquivo() == 0 || getFiltroIncluirTipoLayoutArquivo() == null){
			BradescoFacesUtils.addInfoModalMessage( "Tipo Layout de Arquivo", false);
			return null;
		}
		
		setTipoLoteDesc((String)getListaTipoLoteHash().get(getFiltroIncluirTipoLote()));
		setTipoLayoutArquivoDesc((String)getListaTipoLayoutArquivoHash().get(getFiltroIncluirTipoLayoutArquivo()));
		return "AVANCAR_CONFIRMAR_INCLUIR";
	}
	
	/**
	 * Confirmar incluir.
	 *
	 * @return the string
	 */
	public String confirmarIncluir() {
		AssLoteLayoutArquivoEntradaDTO  entradaDTO = new AssLoteLayoutArquivoEntradaDTO();
		entradaDTO.setCdTipoLayoutArquivo(getFiltroIncluirTipoLayoutArquivo());
		entradaDTO.setCdTipoLoteLayout(getFiltroIncluirTipoLote());				
		
		AssLoteLayoutArquivoSaidaDTO assLoteLayoutArquivoSaidaDTO = getAssociacaoLoteLayoutArquivoService().incluirAssociacaoLoteLayoutArquivo(entradaDTO);
		BradescoFacesUtils.addInfoModalMessage("(" + assLoteLayoutArquivoSaidaDTO.getCodMensagem() + ") " + assLoteLayoutArquivoSaidaDTO.getMensagem() , "conAssociacaoLoteLayoutArquivo", BradescoViewExceptionActionType.ACTION, false);
		carregarListaSucesso();
		return "";
	}
	
	/**
	 * Voltar incluir.
	 *
	 * @return the string
	 */
	public String voltarIncluir(){
		return "VOLTAR_INCLUIR";
	}
	
	/**
	 * Avancar detalhar.
	 *
	 * @return the string
	 */
	public String avancarDetalhar(){
		
		preencheDados();
		
		return "AVANCAR_DETALHAR";
	}
	
	/**
	 * Incluir.
	 *
	 * @return the string
	 */
	public String incluir(){
		this.setFiltroIncluirTipoLayoutArquivo(null);
		this.setFiltroIncluirTipoLote(null);
		
		return "AVANCAR_INCLUIR";
	}
	
	/**
	 * Avancar excluir.
	 *
	 * @return the string
	 */
	public String avancarExcluir(){
		
		preencheDados();
		
		return "AVANCAR_EXCLUIR";
	}
	
	/**
	 * Preenche dados.
	 */
	private void preencheDados() {
		
		AssLoteLayoutArquivoEntradaDTO entradaDTO =  new AssLoteLayoutArquivoEntradaDTO();
		
		AssLoteLayoutArquivoSaidaDTO assLoteLayoutArquivoSaidaDTO = getListaGrid().get(getItemSelecionadoLista());
		entradaDTO.setCdTipoLayoutArquivo(assLoteLayoutArquivoSaidaDTO.getCdTipoLayoutArquivo());
		entradaDTO.setCdTipoLoteLayout(assLoteLayoutArquivoSaidaDTO.getCdTipoLoteLayout());
		
		assLoteLayoutArquivoSaidaDTO = getAssociacaoLoteLayoutArquivoService().detalharAssociacaoLoteLayoutArquivo(entradaDTO);

		setTipoLayoutArquivoDesc(assLoteLayoutArquivoSaidaDTO.getDsTipoLayoutArquivo());
		setTipoLoteDesc(assLoteLayoutArquivoSaidaDTO.getDsTipoLoteLayout());
		
		setTipoLayoutArquivo(String.valueOf(assLoteLayoutArquivoSaidaDTO.getCdTipoLayoutArquivo()));
		setTipoLote(String.valueOf(assLoteLayoutArquivoSaidaDTO.getCdTipoLoteLayout()));
		
		setDataHoraInclusao(assLoteLayoutArquivoSaidaDTO.getHrInclusaoRegistro());
		setUsuarioInclusao(assLoteLayoutArquivoSaidaDTO.getCdAutenticacaoSegurancaInclusao());
		setComplementoInclusao(assLoteLayoutArquivoSaidaDTO.getNrOperacaoFluxoInclusao().equals("0")?"":assLoteLayoutArquivoSaidaDTO.getNrOperacaoFluxoInclusao());
		setTipoCanalInclusao(assLoteLayoutArquivoSaidaDTO.getCdCanalInclusao() + " - " + assLoteLayoutArquivoSaidaDTO.getDsCanalInclusao());
		
		setDataHoraManutencao(assLoteLayoutArquivoSaidaDTO.getHrManutencaoRegistroManutencao());
		setUsuarioManutencao(assLoteLayoutArquivoSaidaDTO.getCdAutenticacaoSegurancaManutencao());
		setComplementoManutencao(assLoteLayoutArquivoSaidaDTO.getNrOperacaoFluxoManutencao());
		setTipoCanalManutencao(String.valueOf(assLoteLayoutArquivoSaidaDTO.getCdCanalManutencao()).equals("0")?"": assLoteLayoutArquivoSaidaDTO.getCdCanalManutencao() + " - " + assLoteLayoutArquivoSaidaDTO.getDsCanalManutencao());
	}

	/**
	 * Pesquisar.
	 *
	 * @return the string
	 */
	public String pesquisar() {
		return "";
	}

	/**
	 * Get: listaControle.
	 *
	 * @return listaControle
	 */
	public List<SelectItem> getListaControle() {
		return listaControle;
	}

	/**
	 * Set: listaControle.
	 *
	 * @param listaControle the lista controle
	 */
	public void setListaControle(List<SelectItem> listaControle) {
		this.listaControle = listaControle;
	}

	/**
	 * Get: listaGrid.
	 *
	 * @return listaGrid
	 */
	public List<AssLoteLayoutArquivoSaidaDTO> getListaGrid() {
		return listaGrid;
	}

	/**
	 * Set: listaGrid.
	 *
	 * @param listaGrid the lista grid
	 */
	public void setListaGrid(List<AssLoteLayoutArquivoSaidaDTO> listaGrid) {
		this.listaGrid = listaGrid;
	}

	/**
	 * Get: logger.
	 *
	 * @return logger
	 */
	public ILogManager getLogger() {
		return logger;
	}

	/**
	 * Set: logger.
	 *
	 * @param logger the logger
	 */
	public void setLogger(ILogManager logger) {
		this.logger = logger;
	}

	/**
	 * Get: complementoInclusao.
	 *
	 * @return complementoInclusao
	 */
	public String getComplementoInclusao() {
		return complementoInclusao;
	}

	/**
	 * Set: complementoInclusao.
	 *
	 * @param complementoInclusao the complemento inclusao
	 */
	public void setComplementoInclusao(String complementoInclusao) {
		this.complementoInclusao = complementoInclusao;
	}

	/**
	 * Get: complementoManutencao.
	 *
	 * @return complementoManutencao
	 */
	public String getComplementoManutencao() {
		return complementoManutencao;
	}

	/**
	 * Set: complementoManutencao.
	 *
	 * @param complementoManutencao the complemento manutencao
	 */
	public void setComplementoManutencao(String complementoManutencao) {
		this.complementoManutencao = complementoManutencao;
	}

	/**
	 * Get: dataHoraInclusao.
	 *
	 * @return dataHoraInclusao
	 */
	public String getDataHoraInclusao() {
		return dataHoraInclusao;
	}

	/**
	 * Set: dataHoraInclusao.
	 *
	 * @param dataHoraInclusao the data hora inclusao
	 */
	public void setDataHoraInclusao(String dataHoraInclusao) {
		this.dataHoraInclusao = dataHoraInclusao;
	}

	/**
	 * Get: tipoCanalInclusao.
	 *
	 * @return tipoCanalInclusao
	 */
	public String getTipoCanalInclusao() {
		return tipoCanalInclusao;
	}

	/**
	 * Set: tipoCanalInclusao.
	 *
	 * @param tipoCanalInclusao the tipo canal inclusao
	 */
	public void setTipoCanalInclusao(String tipoCanalInclusao) {
		this.tipoCanalInclusao = tipoCanalInclusao;
	}

	/**
	 * Get: tipoCanalManutencao.
	 *
	 * @return tipoCanalManutencao
	 */
	public String getTipoCanalManutencao() {
		return tipoCanalManutencao;
	}

	/**
	 * Set: tipoCanalManutencao.
	 *
	 * @param tipoCanalManutencao the tipo canal manutencao
	 */
	public void setTipoCanalManutencao(String tipoCanalManutencao) {
		this.tipoCanalManutencao = tipoCanalManutencao;
	}

	/**
	 * Get: usuarioInclusao.
	 *
	 * @return usuarioInclusao
	 */
	public String getUsuarioInclusao() {
		return usuarioInclusao;
	}

	/**
	 * Set: usuarioInclusao.
	 *
	 * @param usuarioInclusao the usuario inclusao
	 */
	public void setUsuarioInclusao(String usuarioInclusao) {
		this.usuarioInclusao = usuarioInclusao;
	}

	/**
	 * Get: comboService.
	 *
	 * @return comboService
	 */
	public IComboService getComboService() {
		return comboService;
	}

	/**
	 * Set: comboService.
	 *
	 * @param comboService the combo service
	 */
	public void setComboService(IComboService comboService) {
		this.comboService = comboService;
	}

	/**
	 * Get: associacaoLoteLayoutArquivoService.
	 *
	 * @return associacaoLoteLayoutArquivoService
	 */
	public IAssoLoteLayoutArquivoService getAssociacaoLoteLayoutArquivoService() {
		return associacaoLoteLayoutArquivoService;
	}

	/**
	 * Set: associacaoLoteLayoutArquivoService.
	 *
	 * @param associacaoLoteLayoutArquivoService the associacao lote layout arquivo service
	 */
	public void setAssociacaoLoteLayoutArquivoService(
			IAssoLoteLayoutArquivoService associacaoLoteLayoutArquivoService) {
		this.associacaoLoteLayoutArquivoService = associacaoLoteLayoutArquivoService;
	}
	
	/**
	 * Preenche tipo lote.
	 */
	@SuppressWarnings("unchecked")
	private void preencheTipoLote(){
		try{
			listaTipoLote = new ArrayList<SelectItem>();
			List<TipoLoteSaidaDTO> listaLote = new ArrayList<TipoLoteSaidaDTO>();
			TipoLoteEntradaDTO tipoLoteEntradaDTO = new TipoLoteEntradaDTO();		
			tipoLoteEntradaDTO.setCdSituacaoVinculacaoConta(0);
		
			listaLote = comboService.listarTipoLote(tipoLoteEntradaDTO);
			listaTipoLoteHash.clear();
			for(TipoLoteSaidaDTO combo : listaLote){
				listaTipoLoteHash.put(combo.getCdTipoLoteLayout(), combo.getDsTipoLoteLayout());
				listaTipoLote.add(new SelectItem(combo.getCdTipoLoteLayout(),combo.getDsTipoLoteLayout()));
			}
		}catch(PdcAdapterFunctionalException e){
			listaTipoLote = new ArrayList<SelectItem>();
		}
	}

	/**
	 * Get: listaTipoLote.
	 *
	 * @return listaTipoLote
	 */
	@SuppressWarnings("unchecked")
	public List<SelectItem> getListaTipoLote() {
		
		return listaTipoLote;
		
	}

	/**
	 * Set: listaTipoLote.
	 *
	 * @param listaTipoLote the lista tipo lote
	 */
	public void setListaTipoLote(List<SelectItem> listaTipoLote) {
		this.listaTipoLote = listaTipoLote;
	}

	/**
	 * Get: tipoLayoutArquivoDesc.
	 *
	 * @return tipoLayoutArquivoDesc
	 */
	public String getTipoLayoutArquivoDesc() {
		return tipoLayoutArquivoDesc;
	}

	/**
	 * Set: tipoLayoutArquivoDesc.
	 *
	 * @param tipoLayoutArquivoDesc the tipo layout arquivo desc
	 */
	public void setTipoLayoutArquivoDesc(String tipoLayoutArquivoDesc) {
		this.tipoLayoutArquivoDesc = tipoLayoutArquivoDesc;
	}

	/**
	 * Get: tipoLoteDesc.
	 *
	 * @return tipoLoteDesc
	 */
	public String getTipoLoteDesc() {
		return tipoLoteDesc;
	}

	/**
	 * Set: tipoLoteDesc.
	 *
	 * @param tipoLoteDesc the tipo lote desc
	 */
	public void setTipoLoteDesc(String tipoLoteDesc) {
		this.tipoLoteDesc = tipoLoteDesc;
	}
	
	/**
	 * Preenche tipo layout arquivo.
	 */
	@SuppressWarnings("unchecked")
	private void preencheTipoLayoutArquivo(){
		try{
			listaTipoLayoutArquivo = new ArrayList<SelectItem>();
			List<TipoLayoutArquivoSaidaDTO> listaTipoLayout = new ArrayList<TipoLayoutArquivoSaidaDTO>();
			TipoLayoutArquivoEntradaDTO tipoLoteEntradaDTO = new TipoLayoutArquivoEntradaDTO();		
			tipoLoteEntradaDTO.setCdSituacaoVinculacaoConta(0);
		
			saidaTipoLayoutArquivoSaidaDTO = comboService.listarTipoLayoutArquivo(tipoLoteEntradaDTO);
			listaTipoLayout = saidaTipoLayoutArquivoSaidaDTO.getOcorrencias();
			listaTipoLayoutArquivoHash.clear();
			for(TipoLayoutArquivoSaidaDTO combo : listaTipoLayout){
				listaTipoLayoutArquivoHash.put(combo.getCdTipoLayoutArquivo(), combo.getDsTipoLayoutArquivo());
				listaTipoLayoutArquivo.add(new SelectItem(combo.getCdTipoLayoutArquivo(),combo.getDsTipoLayoutArquivo()));
			}
		}catch(PdcAdapterFunctionalException e){
			listaTipoLayoutArquivo = new ArrayList<SelectItem>();
		}
	}

	/**
	 * Get: listaTipoLayoutArquivo.
	 *
	 * @return listaTipoLayoutArquivo
	 */
	@SuppressWarnings("unchecked")
	public List<SelectItem> getListaTipoLayoutArquivo() {
				
		return listaTipoLayoutArquivo;
	}

	/**
	 * Set: listaTipoLayoutArquivo.
	 *
	 * @param listaTipoLayoutArquivo the lista tipo layout arquivo
	 */
	public void setListaTipoLayoutArquivo(List<SelectItem> listaTipoLayoutArquivo) {
		this.listaTipoLayoutArquivo = listaTipoLayoutArquivo;
	}

	/**
	 * Get: hiddenConfirma.
	 *
	 * @return hiddenConfirma
	 */
	public boolean getHiddenConfirma() {
		return hiddenConfirma;
	}

	/**
	 * Set: hiddenConfirma.
	 *
	 * @param hiddenConfirma the hidden confirma
	 */
	public void setHiddenConfirma(boolean hiddenConfirma) {
		this.hiddenConfirma = hiddenConfirma;
	}

	/**
	 * Get: assLoteLayoutArquivoEntradaDTO.
	 *
	 * @return assLoteLayoutArquivoEntradaDTO
	 */
	public AssLoteLayoutArquivoEntradaDTO getAssLoteLayoutArquivoEntradaDTO() {
		return assLoteLayoutArquivoEntradaDTO;
	}

	/**
	 * Set: assLoteLayoutArquivoEntradaDTO.
	 *
	 * @param assLoteLayoutArquivoEntradaDTO the ass lote layout arquivo entrada dto
	 */
	public void setAssLoteLayoutArquivoEntradaDTO(
			AssLoteLayoutArquivoEntradaDTO assLoteLayoutArquivoEntradaDTO) {
		this.assLoteLayoutArquivoEntradaDTO = assLoteLayoutArquivoEntradaDTO;
	}

	/**
	 * Get: itemSelecionadoLista.
	 *
	 * @return itemSelecionadoLista
	 */
	public Integer getItemSelecionadoLista() {
		return itemSelecionadoLista;
	}

	/**
	 * Set: itemSelecionadoLista.
	 *
	 * @param itemSelecionadoLista the item selecionado lista
	 */
	public void setItemSelecionadoLista(Integer itemSelecionadoLista) {
		this.itemSelecionadoLista = itemSelecionadoLista;
	}

	/**
	 * Get: btoAcionado.
	 *
	 * @return btoAcionado
	 */
	public Boolean getBtoAcionado() {
		return btoAcionado;
	}

	/**
	 * Set: btoAcionado.
	 *
	 * @param btoAcionado the bto acionado
	 */
	public void setBtoAcionado(Boolean btoAcionado) {
		this.btoAcionado = btoAcionado;
	}

	/**
	 * Get: listaTipoLayoutArquivoHash.
	 *
	 * @return listaTipoLayoutArquivoHash
	 */
	public Map<Integer, String> getListaTipoLayoutArquivoHash() {
		return listaTipoLayoutArquivoHash;
	}

	/**
	 * Set lista tipo layout arquivo hash.
	 *
	 * @param listaTipoLayoutArquivoHash the lista tipo layout arquivo hash
	 */
	public void setListaTipoLayoutArquivoHash(
			Map<Integer, String> listaTipoLayoutArquivoHash) {
		this.listaTipoLayoutArquivoHash = listaTipoLayoutArquivoHash;
	}

	/**
	 * Get: listaTipoLoteHash.
	 *
	 * @return listaTipoLoteHash
	 */
	public Map<Integer, String> getListaTipoLoteHash() {
		return listaTipoLoteHash;
	}

	/**
	 * Set lista tipo lote hash.
	 *
	 * @param listaTipoLoteHash the lista tipo lote hash
	 */
	public void setListaTipoLoteHash(Map<Integer, String> listaTipoLoteHash) {
		this.listaTipoLoteHash = listaTipoLoteHash;
	}

	/**
	 * Nome: setSaidaTipoLayoutArquivoSaidaDTO
	 *
	 * @exception
	 * @throws
	 * @param saidaTipoLayoutArquivoSaidaDTO
	 */
	public void setSaidaTipoLayoutArquivoSaidaDTO(
			TipoLayoutArquivoSaidaDTO saidaTipoLayoutArquivoSaidaDTO) {
		this.saidaTipoLayoutArquivoSaidaDTO = saidaTipoLayoutArquivoSaidaDTO;
	}

	/**
	 * Nome: getSaidaTipoLayoutArquivoSaidaDTO
	 *
	 * @exception
	 * @throws
	 * @return saidaTipoLayoutArquivoSaidaDTO
	 */
	public TipoLayoutArquivoSaidaDTO getSaidaTipoLayoutArquivoSaidaDTO() {
		return saidaTipoLayoutArquivoSaidaDTO;
	}


	
}
