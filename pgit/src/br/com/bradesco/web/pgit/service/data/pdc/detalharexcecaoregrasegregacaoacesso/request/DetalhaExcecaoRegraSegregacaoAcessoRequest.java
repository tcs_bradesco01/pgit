/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.detalharexcecaoregrasegregacaoacesso.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class DetalhaExcecaoRegraSegregacaoAcessoRequest.
 * 
 * @version $Revision$ $Date$
 */
public class DetalhaExcecaoRegraSegregacaoAcessoRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdRegraAcessoDado
     */
    private int _cdRegraAcessoDado = 0;

    /**
     * keeps track of state for field: _cdRegraAcessoDado
     */
    private boolean _has_cdRegraAcessoDado;

    /**
     * Field _cdExcecaoAcessoDado
     */
    private int _cdExcecaoAcessoDado = 0;

    /**
     * keeps track of state for field: _cdExcecaoAcessoDado
     */
    private boolean _has_cdExcecaoAcessoDado;


      //----------------/
     //- Constructors -/
    //----------------/

    public DetalhaExcecaoRegraSegregacaoAcessoRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharexcecaoregrasegregacaoacesso.request.DetalhaExcecaoRegraSegregacaoAcessoRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdExcecaoAcessoDado
     * 
     */
    public void deleteCdExcecaoAcessoDado()
    {
        this._has_cdExcecaoAcessoDado= false;
    } //-- void deleteCdExcecaoAcessoDado() 

    /**
     * Method deleteCdRegraAcessoDado
     * 
     */
    public void deleteCdRegraAcessoDado()
    {
        this._has_cdRegraAcessoDado= false;
    } //-- void deleteCdRegraAcessoDado() 

    /**
     * Returns the value of field 'cdExcecaoAcessoDado'.
     * 
     * @return int
     * @return the value of field 'cdExcecaoAcessoDado'.
     */
    public int getCdExcecaoAcessoDado()
    {
        return this._cdExcecaoAcessoDado;
    } //-- int getCdExcecaoAcessoDado() 

    /**
     * Returns the value of field 'cdRegraAcessoDado'.
     * 
     * @return int
     * @return the value of field 'cdRegraAcessoDado'.
     */
    public int getCdRegraAcessoDado()
    {
        return this._cdRegraAcessoDado;
    } //-- int getCdRegraAcessoDado() 

    /**
     * Method hasCdExcecaoAcessoDado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdExcecaoAcessoDado()
    {
        return this._has_cdExcecaoAcessoDado;
    } //-- boolean hasCdExcecaoAcessoDado() 

    /**
     * Method hasCdRegraAcessoDado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdRegraAcessoDado()
    {
        return this._has_cdRegraAcessoDado;
    } //-- boolean hasCdRegraAcessoDado() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdExcecaoAcessoDado'.
     * 
     * @param cdExcecaoAcessoDado the value of field
     * 'cdExcecaoAcessoDado'.
     */
    public void setCdExcecaoAcessoDado(int cdExcecaoAcessoDado)
    {
        this._cdExcecaoAcessoDado = cdExcecaoAcessoDado;
        this._has_cdExcecaoAcessoDado = true;
    } //-- void setCdExcecaoAcessoDado(int) 

    /**
     * Sets the value of field 'cdRegraAcessoDado'.
     * 
     * @param cdRegraAcessoDado the value of field
     * 'cdRegraAcessoDado'.
     */
    public void setCdRegraAcessoDado(int cdRegraAcessoDado)
    {
        this._cdRegraAcessoDado = cdRegraAcessoDado;
        this._has_cdRegraAcessoDado = true;
    } //-- void setCdRegraAcessoDado(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return DetalhaExcecaoRegraSegregacaoAcessoRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.detalharexcecaoregrasegregacaoacesso.request.DetalhaExcecaoRegraSegregacaoAcessoRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.detalharexcecaoregrasegregacaoacesso.request.DetalhaExcecaoRegraSegregacaoAcessoRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.detalharexcecaoregrasegregacaoacesso.request.DetalhaExcecaoRegraSegregacaoAcessoRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharexcecaoregrasegregacaoacesso.request.DetalhaExcecaoRegraSegregacaoAcessoRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
