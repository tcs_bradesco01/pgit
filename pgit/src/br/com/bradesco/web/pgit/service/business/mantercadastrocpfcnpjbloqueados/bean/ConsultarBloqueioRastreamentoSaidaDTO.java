/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantercadastrocpfcnpjbloqueados.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantercadastrocpfcnpjbloqueados.bean;

import br.com.bradesco.web.pgit.utils.CpfCnpjUtils;

/**
 * Nome: ConsultarBloqueioRastreamentoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarBloqueioRastreamentoSaidaDTO {
	 
 	/** Atributo nmControleRastreabilidadeTitulo. */
 	private Integer nmControleRastreabilidadeTitulo;
	 
 	/** Atributo cdCpfCnpjBloqueio. */
 	private Long cdCpfCnpjBloqueio;
	 
 	/** Atributo cdFilialCnpjBloqueio. */
 	private Integer cdFilialCnpjBloqueio;
	 
 	/** Atributo cdControleCnpjCpf. */
 	private Integer cdControleCnpjCpf;
	 
 	/** Atributo dsPessoa. */
 	private String dsPessoa;
	 
 	/** Atributo cnpjCpfFormatado. */
 	private String cnpjCpfFormatado;
	 
 	/** Atributo check. */
 	private boolean check;

	/**
	 * Get: cpfCnpjFormatado.
	 *
	 * @return cpfCnpjFormatado
	 */
	public String getCpfCnpjFormatado() {
		return CpfCnpjUtils.formatarCpfCnpj(cdCpfCnpjBloqueio, cdFilialCnpjBloqueio, cdControleCnpjCpf);
	}
	
	/**
	 * Get: nmControleRastreabilidadeTitulo.
	 *
	 * @return nmControleRastreabilidadeTitulo
	 */
	public Integer getNmControleRastreabilidadeTitulo() {
		return nmControleRastreabilidadeTitulo;
	}
	
	/**
	 * Set: nmControleRastreabilidadeTitulo.
	 *
	 * @param nmControleRastreabilidadeTitulo the nm controle rastreabilidade titulo
	 */
	public void setNmControleRastreabilidadeTitulo(
			Integer nmControleRastreabilidadeTitulo) {
		this.nmControleRastreabilidadeTitulo = nmControleRastreabilidadeTitulo;
	}
	
	/**
	 * Get: cdCpfCnpjBloqueio.
	 *
	 * @return cdCpfCnpjBloqueio
	 */
	public Long getCdCpfCnpjBloqueio() {
		return cdCpfCnpjBloqueio;
	}
	
	/**
	 * Set: cdCpfCnpjBloqueio.
	 *
	 * @param cdCpfCnpjBloqueio the cd cpf cnpj bloqueio
	 */
	public void setCdCpfCnpjBloqueio(Long cdCpfCnpjBloqueio) {
		this.cdCpfCnpjBloqueio = cdCpfCnpjBloqueio;
	}
	
	/**
	 * Get: cdFilialCnpjBloqueio.
	 *
	 * @return cdFilialCnpjBloqueio
	 */
	public Integer getCdFilialCnpjBloqueio() {
		return cdFilialCnpjBloqueio;
	}
	
	/**
	 * Set: cdFilialCnpjBloqueio.
	 *
	 * @param cdFilialCnpjBloqueio the cd filial cnpj bloqueio
	 */
	public void setCdFilialCnpjBloqueio(Integer cdFilialCnpjBloqueio) {
		this.cdFilialCnpjBloqueio = cdFilialCnpjBloqueio;
	}
	
	/**
	 * Get: cdControleCnpjCpf.
	 *
	 * @return cdControleCnpjCpf
	 */
	public Integer getCdControleCnpjCpf() {
		return cdControleCnpjCpf;
	}
	
	/**
	 * Set: cdControleCnpjCpf.
	 *
	 * @param cdControleCnpjCpf the cd controle cnpj cpf
	 */
	public void setCdControleCnpjCpf(Integer cdControleCnpjCpf) {
		this.cdControleCnpjCpf = cdControleCnpjCpf;
	}
	
	/**
	 * Get: dsPessoa.
	 *
	 * @return dsPessoa
	 */
	public String getDsPessoa() {
		return dsPessoa;
	}
	
	/**
	 * Set: dsPessoa.
	 *
	 * @param dsPessoa the ds pessoa
	 */
	public void setDsPessoa(String dsPessoa) {
		this.dsPessoa = dsPessoa;
	}
	
	/**
	 * Get: cnpjCpfFormatado.
	 *
	 * @return cnpjCpfFormatado
	 */
	public String getCnpjCpfFormatado() {
		return cnpjCpfFormatado;
	}
	
	/**
	 * Set: cnpjCpfFormatado.
	 *
	 * @param cnpjCpfFormatado the cnpj cpf formatado
	 */
	public void setCnpjCpfFormatado(String cnpjCpfFormatado) {
		this.cnpjCpfFormatado = cnpjCpfFormatado;
	}
	
	/**
	 * Is check.
	 *
	 * @return true, if is check
	 */
	public boolean isCheck() {
		return check;
	}
	
	/**
	 * Set: check.
	 *
	 * @param check the check
	 */
	public void setCheck(boolean check) {
		this.check = check;
	}
	
}
