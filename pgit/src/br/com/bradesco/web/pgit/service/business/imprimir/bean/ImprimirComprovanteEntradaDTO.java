/*
 * Nome: br.com.bradesco.web.pgit.service.business.imprimir.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.imprimir.bean;

/**
 * Nome: ImprimirComprovanteEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ImprimirComprovanteEntradaDTO{
	
	/** Atributo cdPessoaJuridicaContrato. */
	private Long cdPessoaJuridicaContrato;
	
	/** Atributo cdTipoContratoNegocio. */
	private Integer cdTipoContratoNegocio;
	
	/** Atributo nrSequenciaContratoNegocio. */
	private Long nrSequenciaContratoNegocio;
	
	/** Atributo cdControlePagamento. */
	private String cdControlePagamento;
	
	/** Atributo dtEfetivacaoPagamento. */
	private String dtEfetivacaoPagamento;
	
	/** Atributo cdTipoVia. */
	private Integer cdTipoVia;

	/**
	 * Get: cdPessoaJuridicaContrato.
	 *
	 * @return cdPessoaJuridicaContrato
	 */
	public Long getCdPessoaJuridicaContrato(){
		return cdPessoaJuridicaContrato;
	}

	/**
	 * Set: cdPessoaJuridicaContrato.
	 *
	 * @param cdPessoaJuridicaContrato the cd pessoa juridica contrato
	 */
	public void setCdPessoaJuridicaContrato(Long cdPessoaJuridicaContrato){
		this.cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
	}

	/**
	 * Get: cdTipoContratoNegocio.
	 *
	 * @return cdTipoContratoNegocio
	 */
	public Integer getCdTipoContratoNegocio(){
		return cdTipoContratoNegocio;
	}

	/**
	 * Set: cdTipoContratoNegocio.
	 *
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio){
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}

	/**
	 * Get: nrSequenciaContratoNegocio.
	 *
	 * @return nrSequenciaContratoNegocio
	 */
	public Long getNrSequenciaContratoNegocio(){
		return nrSequenciaContratoNegocio;
	}

	/**
	 * Set: nrSequenciaContratoNegocio.
	 *
	 * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
	 */
	public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio){
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}

	/**
	 * Get: cdControlePagamento.
	 *
	 * @return cdControlePagamento
	 */
	public String getCdControlePagamento(){
		return cdControlePagamento;
	}

	/**
	 * Set: cdControlePagamento.
	 *
	 * @param cdControlePagamento the cd controle pagamento
	 */
	public void setCdControlePagamento(String cdControlePagamento){
		this.cdControlePagamento = cdControlePagamento;
	}

	/**
	 * Get: dtEfetivacaoPagamento.
	 *
	 * @return dtEfetivacaoPagamento
	 */
	public String getDtEfetivacaoPagamento(){
		return dtEfetivacaoPagamento;
	}

	/**
	 * Set: dtEfetivacaoPagamento.
	 *
	 * @param dtEfetivacaoPagamento the dt efetivacao pagamento
	 */
	public void setDtEfetivacaoPagamento(String dtEfetivacaoPagamento){
		this.dtEfetivacaoPagamento = dtEfetivacaoPagamento;
	}

    /**
     * Nome: setCdTipoVia.
     *
     * @param cdTipoVia the cd tipo via
     */
    public void setCdTipoVia(Integer cdTipoVia) {
        this.cdTipoVia = cdTipoVia;
    }

    /**
     * Nome: getCdTipoVia.
     *
     * @return cdTipoVia
     */
    public Integer getCdTipoVia() {
        return cdTipoVia;
    }
}