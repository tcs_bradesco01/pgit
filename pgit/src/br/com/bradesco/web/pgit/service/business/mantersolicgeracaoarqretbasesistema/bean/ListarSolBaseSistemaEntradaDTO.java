/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantersolicgeracaoarqretbasesistema.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantersolicgeracaoarqretbasesistema.bean;

/**
 * Nome: ListarSolBaseSistemaEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarSolBaseSistemaEntradaDTO {
	
	    /** Atributo cdPessoaJuridicaContrato. */
    	private Long cdPessoaJuridicaContrato;
	    
    	/** Atributo cdTipoContratoNegocio. */
    	private Integer cdTipoContratoNegocio;
	    
    	/** Atributo nrSequenciaContratoNegocio. */
    	private Long nrSequenciaContratoNegocio;
	    
    	/** Atributo dtSolicitacaoInicio. */
    	private String dtSolicitacaoInicio;
	    
    	/** Atributo dtSolicitacaoFim. */
    	private String dtSolicitacaoFim;
	    
    	/** Atributo nrSolicitacaoPagamento. */
    	private Integer nrSolicitacaoPagamento;
	    
    	/** Atributo cdSituacaoSolicitacao. */
    	private Integer cdSituacaoSolicitacao;
	    
    	/** Atributo cdorigemSolicitacao. */
    	private Integer cdorigemSolicitacao;
	    
		/**
		 * Get: cdorigemSolicitacao.
		 *
		 * @return cdorigemSolicitacao
		 */
		public Integer getCdorigemSolicitacao() {
			return cdorigemSolicitacao;
		}
		
		/**
		 * Set: cdorigemSolicitacao.
		 *
		 * @param cdorigemSolicitacao the cdorigem solicitacao
		 */
		public void setCdorigemSolicitacao(Integer cdorigemSolicitacao) {
			this.cdorigemSolicitacao = cdorigemSolicitacao;
		}
		
		/**
		 * Get: cdPessoaJuridicaContrato.
		 *
		 * @return cdPessoaJuridicaContrato
		 */
		public Long getCdPessoaJuridicaContrato() {
			return cdPessoaJuridicaContrato;
		}
		
		/**
		 * Set: cdPessoaJuridicaContrato.
		 *
		 * @param cdPessoaJuridicaContrato the cd pessoa juridica contrato
		 */
		public void setCdPessoaJuridicaContrato(Long cdPessoaJuridicaContrato) {
			this.cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
		}
		
		/**
		 * Get: cdSituacaoSolicitacao.
		 *
		 * @return cdSituacaoSolicitacao
		 */
		public Integer getCdSituacaoSolicitacao() {
			return cdSituacaoSolicitacao;
		}
		
		/**
		 * Set: cdSituacaoSolicitacao.
		 *
		 * @param cdSituacaoSolicitacao the cd situacao solicitacao
		 */
		public void setCdSituacaoSolicitacao(Integer cdSituacaoSolicitacao) {
			this.cdSituacaoSolicitacao = cdSituacaoSolicitacao;
		}
		
		/**
		 * Get: cdTipoContratoNegocio.
		 *
		 * @return cdTipoContratoNegocio
		 */
		public Integer getCdTipoContratoNegocio() {
			return cdTipoContratoNegocio;
		}
		
		/**
		 * Set: cdTipoContratoNegocio.
		 *
		 * @param cdTipoContratoNegocio the cd tipo contrato negocio
		 */
		public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
			this.cdTipoContratoNegocio = cdTipoContratoNegocio;
		}
		
		/**
		 * Get: dtSolicitacaoFim.
		 *
		 * @return dtSolicitacaoFim
		 */
		public String getDtSolicitacaoFim() {
			return dtSolicitacaoFim;
		}
		
		/**
		 * Set: dtSolicitacaoFim.
		 *
		 * @param dtSolicitacaoFim the dt solicitacao fim
		 */
		public void setDtSolicitacaoFim(String dtSolicitacaoFim) {
			this.dtSolicitacaoFim = dtSolicitacaoFim;
		}
		
		/**
		 * Get: dtSolicitacaoInicio.
		 *
		 * @return dtSolicitacaoInicio
		 */
		public String getDtSolicitacaoInicio() {
			return dtSolicitacaoInicio;
		}
		
		/**
		 * Set: dtSolicitacaoInicio.
		 *
		 * @param dtSolicitacaoInicio the dt solicitacao inicio
		 */
		public void setDtSolicitacaoInicio(String dtSolicitacaoInicio) {
			this.dtSolicitacaoInicio = dtSolicitacaoInicio;
		}
		
		/**
		 * Get: nrSequenciaContratoNegocio.
		 *
		 * @return nrSequenciaContratoNegocio
		 */
		public Long getNrSequenciaContratoNegocio() {
			return nrSequenciaContratoNegocio;
		}
		
		/**
		 * Set: nrSequenciaContratoNegocio.
		 *
		 * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
		 */
		public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
			this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
		}
		
		/**
		 * Get: nrSolicitacaoPagamento.
		 *
		 * @return nrSolicitacaoPagamento
		 */
		public Integer getNrSolicitacaoPagamento() {
			return nrSolicitacaoPagamento;
		}
		
		/**
		 * Set: nrSolicitacaoPagamento.
		 *
		 * @param nrSolicitacaoPagamento the nr solicitacao pagamento
		 */
		public void setNrSolicitacaoPagamento(Integer nrSolicitacaoPagamento) {
			this.nrSolicitacaoPagamento = nrSolicitacaoPagamento;
		}
	    
	    
	    
}



