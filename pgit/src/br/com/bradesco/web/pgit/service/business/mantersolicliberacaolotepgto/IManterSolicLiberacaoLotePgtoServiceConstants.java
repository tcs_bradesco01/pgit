/**
 * Nome: br.com.bradesco.web.pgit.service.business.mantersolicliberacaolotepgto
 * Compilador: JDK 1.5
 * Prop�sito: INSERIR O PROP�SITO DAS CLASSES DO PACOTE
 * Data da cria��o: <dd/MM/yyyy>
 * Par�metros de compila��o: -d
 */
package br.com.bradesco.web.pgit.service.business.mantersolicliberacaolotepgto;

/**
 * Nome: IManterSolicLiberacaoLotePgtoServiceConstants
 * <p>
 * Prop�sito: Interface de constantes do adaptador ManterSolicLiberacaoLotePgto
 * </p>
 * 
 * @author CPM Braxis / TI Melhorias - Arquitetura
 * @version 1.0
 */
public interface IManterSolicLiberacaoLotePgtoServiceConstants {

	static final int QTDE_MAXIMA_OCORRENCIAS = 40;
}