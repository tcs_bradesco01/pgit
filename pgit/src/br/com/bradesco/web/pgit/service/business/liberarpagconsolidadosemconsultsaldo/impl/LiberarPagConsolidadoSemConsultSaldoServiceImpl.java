/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/implementation.ftl,v $
 * $Id: implementation.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: implementation.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.liberarpagconsolidadosemconsultsaldo.impl;

import java.util.ArrayList;
import java.util.List;

import br.com.bradesco.web.pgit.service.business.liberarpagconsolidadosemconsultsaldo.ILiberarPagConsolidadoSemConsultSaldoService;
import br.com.bradesco.web.pgit.service.business.liberarpagconsolidadosemconsultsaldo.ILiberarPagConsolidadoSemConsultSaldoServiceConstants;
import br.com.bradesco.web.pgit.service.business.liberarpagconsolidadosemconsultsaldo.bean.ConsultarLibPagtosConsSemConsultaSaldoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.liberarpagconsolidadosemconsultsaldo.bean.ConsultarLibPagtosConsSemConsultaSaldoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.liberarpagconsolidadosemconsultsaldo.bean.DetalharLibPagtosConsSemConsultaSaldoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.liberarpagconsolidadosemconsultsaldo.bean.DetalharLibPagtosConsSemConsultaSaldoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.liberarpagconsolidadosemconsultsaldo.bean.LiberarIntegralEntradaDTO;
import br.com.bradesco.web.pgit.service.business.liberarpagconsolidadosemconsultsaldo.bean.LiberarIntegralSaidaDTO;
import br.com.bradesco.web.pgit.service.business.liberarpagconsolidadosemconsultsaldo.bean.LiberarParcialEntradaDTO;
import br.com.bradesco.web.pgit.service.business.liberarpagconsolidadosemconsultsaldo.bean.LiberarParcialSaidaDTO;
import br.com.bradesco.web.pgit.service.business.liberarpagconsolidadosemconsultsaldo.bean.OcorrenciasDetLibPagConsSemConsSalSaidaDTO;
import br.com.bradesco.web.pgit.service.data.pdc.FactoryAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarlibpagtosconssemconsultasaldo.request.ConsultarLibPagtosConsSemConsultaSaldoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarlibpagtosconssemconsultasaldo.response.ConsultarLibPagtosConsSemConsultaSaldoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.detalharlibpagtosconssemconsultasaldo.request.DetalharLibPagtosConsSemConsultaSaldoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.detalharlibpagtosconssemconsultasaldo.response.DetalharLibPagtosConsSemConsultaSaldoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.liberarintegral.request.LiberarIntegralRequest;
import br.com.bradesco.web.pgit.service.data.pdc.liberarintegral.response.LiberarIntegralResponse;
import br.com.bradesco.web.pgit.service.data.pdc.liberarparcial.request.LiberarParcialRequest;
import br.com.bradesco.web.pgit.service.data.pdc.liberarparcial.response.LiberarParcialResponse;
import br.com.bradesco.web.pgit.utils.CpfCnpjUtils;
import br.com.bradesco.web.pgit.utils.PgitUtil;

/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Implementa��o do adaptador: LiberarPagConsolidadoSemConsultSaldo
 * </p>
 * 
 * @comment C�DIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public class LiberarPagConsolidadoSemConsultSaldoServiceImpl implements ILiberarPagConsolidadoSemConsultSaldoService {

    /** Atributo factoryAdapter. */
    private FactoryAdapter factoryAdapter;

    /**
     * Get: factoryAdapter.
     *
     * @return factoryAdapter
     */
    public FactoryAdapter getFactoryAdapter() {
	return factoryAdapter;
    }

    /**
     * Set: factoryAdapter.
     *
     * @param factoryAdapter the factory adapter
     */
    public void setFactoryAdapter(FactoryAdapter factoryAdapter) {
	this.factoryAdapter = factoryAdapter;
    }

    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.liberarpagconsolidadosemconsultsaldo.ILiberarPagConsolidadoSemConsultSaldoService#consultarLibPagtosConsSemConsultaSaldo(br.com.bradesco.web.pgit.service.business.liberarpagconsolidadosemconsultsaldo.bean.ConsultarLibPagtosConsSemConsultaSaldoEntradaDTO)
     */
    public List<ConsultarLibPagtosConsSemConsultaSaldoSaidaDTO> consultarLibPagtosConsSemConsultaSaldo(
	    ConsultarLibPagtosConsSemConsultaSaldoEntradaDTO consultarLibPagtosConsSemConsultaSaldoEntradaDTO) {
	List<ConsultarLibPagtosConsSemConsultaSaldoSaidaDTO> listaRetorno = new ArrayList<ConsultarLibPagtosConsSemConsultaSaldoSaidaDTO>();
	ConsultarLibPagtosConsSemConsultaSaldoRequest request = new ConsultarLibPagtosConsSemConsultaSaldoRequest();
	ConsultarLibPagtosConsSemConsultaSaldoResponse response = new ConsultarLibPagtosConsSemConsultaSaldoResponse();

	request.setCdAgencia(PgitUtil.verificaIntegerNulo(consultarLibPagtosConsSemConsultaSaldoEntradaDTO.getCdAgencia()));
	request.setCdAgendadosPagosNaoPagos(PgitUtil.verificaIntegerNulo(consultarLibPagtosConsSemConsultaSaldoEntradaDTO
		.getCdAgendadosPagosNaoPagos()));
	request.setCdBanco(PgitUtil.verificaIntegerNulo(consultarLibPagtosConsSemConsultaSaldoEntradaDTO.getCdBanco()));
	request.setCdConta(PgitUtil.verificaLongNulo(consultarLibPagtosConsSemConsultaSaldoEntradaDTO.getCdConta()));
	request.setCdDigitoConta(PgitUtil.DIGITO_CONTA);
	request.setCdMotivoSituacaoPagamento(PgitUtil.verificaIntegerNulo(consultarLibPagtosConsSemConsultaSaldoEntradaDTO
		.getCdMotivoSituacaoPagamento()));
	request
		.setCdPessoaJuridicaContrato(PgitUtil
			.verificaLongNulo(consultarLibPagtosConsSemConsultaSaldoEntradaDTO.getCdPessoaJuridicaContrato()));
	request.setCdProdutoServicoOperacao(PgitUtil.verificaIntegerNulo(consultarLibPagtosConsSemConsultaSaldoEntradaDTO
		.getCdProdutoServicoOperacao()));
	request.setCdProdutoServicoRelacionado(PgitUtil.verificaIntegerNulo(consultarLibPagtosConsSemConsultaSaldoEntradaDTO
		.getCdProdutoServicoRelacionado()));
	request.setCdSituacaoOperacaoPagamento(PgitUtil.verificaIntegerNulo(consultarLibPagtosConsSemConsultaSaldoEntradaDTO
		.getCdSituacaoOperacaoPagamento()));
	request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(consultarLibPagtosConsSemConsultaSaldoEntradaDTO.getCdTipoContratoNegocio()));
	request.setCdTipoPesquisa(PgitUtil.verificaIntegerNulo(consultarLibPagtosConsSemConsultaSaldoEntradaDTO.getCdTipoPesquisa()));
	request.setDtCreditoPagamentoFim(PgitUtil.verificaStringNula(consultarLibPagtosConsSemConsultaSaldoEntradaDTO.getDtCreditoPagamentoFim()));
	request.setDtCreditoPagamentoInicio(PgitUtil.verificaStringNula(consultarLibPagtosConsSemConsultaSaldoEntradaDTO
		.getDtCreditoPagamentoInicio()));
	request.setNrArquivoRemessaPagamento(PgitUtil.verificaLongNulo(consultarLibPagtosConsSemConsultaSaldoEntradaDTO
		.getNrArquivoRemessaPagamento()));
	request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(consultarLibPagtosConsSemConsultaSaldoEntradaDTO
		.getNrSequenciaContratoNegocio()));
	request.setNrOcorrencias(PgitUtil.verificaIntegerNulo(ILiberarPagConsolidadoSemConsultSaldoServiceConstants.NUMERO_OCORRENCIAS_CONSULTAR));	
	request.setCdTituloPgtoRastreado(0);
	

	response = getFactoryAdapter().getConsultarLibPagtosConsSemConsultaSaldoPDCAdapter().invokeProcess(request);
	ConsultarLibPagtosConsSemConsultaSaldoSaidaDTO saidaDTO;

	for (int i = 0; i < response.getOcorrenciasCount(); i++) {
	    saidaDTO = new ConsultarLibPagtosConsSemConsultaSaldoSaidaDTO();

	    saidaDTO.setCdMensagem(response.getCodMensagem());
	    saidaDTO.setMensagem(response.getMensagem());

	    saidaDTO.setCdPessoaJuridicaContrato(response.getOcorrencias(i).getCdPessoaJuridicaContrato());
	    saidaDTO.setDsRazaoSocial(response.getOcorrencias(i).getDsRazaoSocial());
	    saidaDTO.setCdAgencia(response.getOcorrencias(i).getCdAgencia());
	    saidaDTO.setCdBanco(response.getOcorrencias(i).getCdBanco());
	    saidaDTO.setCdConta(response.getOcorrencias(i).getCdConta());
	    saidaDTO.setCdDigitoAgencia(response.getOcorrencias(i).getCdDigitoAgencia());
	    saidaDTO.setCdDigitoConta(response.getOcorrencias(i).getCdDigitoConta());
	    saidaDTO.setCdProdutoServicoOperacao(response.getOcorrencias(i).getCdProdutoServicoOperacao());
	    saidaDTO.setCdProdutoServicoRelacionado(response.getOcorrencias(i).getCdProdutoServicoRelacionado());
	    saidaDTO.setCdSituacaoOperacaoPagamento(response.getOcorrencias(i).getCdSituacaoOperacaoPagamento());
	    saidaDTO.setCdTipoContratoNegocio(response.getOcorrencias(i).getCdTipoContratoNegocio());
	    saidaDTO.setDsOperacaoProdutoServico(response.getOcorrencias(i).getDsOperacaoProdutoServico());
	    saidaDTO.setDsResumoProdutoServico(response.getOcorrencias(i).getDsResumoProdutoServico());
	    saidaDTO.setDsSituacaoOperacaoPagamento(response.getOcorrencias(i).getDsSituacaoOperacaoPagamento());
	    saidaDTO.setDtCreditoPagamento(response.getOcorrencias(i).getDtCreditoPagamento());
	    saidaDTO.setNrArquivoRemessaPagamento(response.getOcorrencias(i).getNrArquivoRemessaPagamento());
	    saidaDTO.setNrCnpjCpf(response.getOcorrencias(i).getNrCnpjCpf());
	    saidaDTO.setNrContratoOrigem(response.getOcorrencias(i).getNrContratoOrigem());
	    saidaDTO.setNrDigitoCnpjCpf(response.getOcorrencias(i).getNrDigitoCnpjCpf());
	    saidaDTO.setNrFilialCnpjCpf(response.getOcorrencias(i).getNrFilialCnpjCpf());
	    saidaDTO.setQtPagamento(response.getOcorrencias(i).getQtPagamento());
	    saidaDTO.setVlEfetivoPagamentoCliente(response.getOcorrencias(i).getVlEfetivoPagamentoCliente());
	    saidaDTO.setCdPessoaContratoDebito(response.getOcorrencias(i).getCdPessoaContratoDebito());
	    saidaDTO.setCdTipoContratoDebito(response.getOcorrencias(i).getCdTipoContratoDebito());
	    saidaDTO.setNrSequenciaContratoDebito(response.getOcorrencias(i).getNrSequenciaContratoDebito());

	    saidaDTO.setCnpjCpfFormatado(CpfCnpjUtils.formatCpfCnpjCompleto(response.getOcorrencias(i).getNrCnpjCpf(), response.getOcorrencias(i)
		    .getNrFilialCnpjCpf(), response.getOcorrencias(i).getNrDigitoCnpjCpf()));
	    saidaDTO.setBancoAgenciaContaDebitoFormatado(PgitUtil.formatBancoAgenciaConta(response.getOcorrencias(i).getCdBanco(), response
		    .getOcorrencias(i).getCdAgencia(), response.getOcorrencias(i).getCdDigitoAgencia(), response.getOcorrencias(i).getCdConta(),
		    response.getOcorrencias(i).getCdDigitoConta(), true));

	    listaRetorno.add(saidaDTO);
	}

	return listaRetorno;
    }

    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.liberarpagconsolidadosemconsultsaldo.ILiberarPagConsolidadoSemConsultSaldoService#liberarParcial(br.com.bradesco.web.pgit.service.business.liberarpagconsolidadosemconsultsaldo.bean.LiberarParcialEntradaDTO)
     */
    public LiberarParcialSaidaDTO liberarParcial(LiberarParcialEntradaDTO liberarParcialEntradaDTO) {
	LiberarParcialSaidaDTO saidaDTO = new LiberarParcialSaidaDTO();
	LiberarParcialRequest request = new LiberarParcialRequest();
	LiberarParcialResponse response = new LiberarParcialResponse();

	request.setNumeroConsultas(liberarParcialEntradaDTO.getListaLiberarParcial().size());
	ArrayList<br.com.bradesco.web.pgit.service.data.pdc.liberarparcial.request.Ocorrencias> lista = new ArrayList<br.com.bradesco.web.pgit.service.data.pdc.liberarparcial.request.Ocorrencias>();
	br.com.bradesco.web.pgit.service.data.pdc.liberarparcial.request.Ocorrencias ocorrencia;

	// V�rias Ocorr�ncias para Libera��o Parcial
	for (int i = 0; i < liberarParcialEntradaDTO.getListaLiberarParcial().size(); i++) {
	    ocorrencia = new br.com.bradesco.web.pgit.service.data.pdc.liberarparcial.request.Ocorrencias();

	    ocorrencia.setCdControlePagamento(PgitUtil.verificaStringNula(liberarParcialEntradaDTO.getListaLiberarParcial().get(i)
		    .getCdControlePagamento()));
	    ocorrencia.setCdOperacaoProdutoServico(PgitUtil.verificaIntegerNulo(liberarParcialEntradaDTO.getListaLiberarParcial().get(i)
		    .getCdOperacaoProdutoServico()));
	    ocorrencia.setCdPessoaJuridicaContrato(PgitUtil.verificaLongNulo(liberarParcialEntradaDTO.getListaLiberarParcial().get(i)
		    .getCdPessoaJuridicaContrato()));
	    ocorrencia.setCdProdutoServicoOperacao(PgitUtil.verificaIntegerNulo(liberarParcialEntradaDTO.getListaLiberarParcial().get(i)
		    .getCdProdutoServicoOperacao()));
	    ocorrencia.setCdTipoCanal(PgitUtil.verificaIntegerNulo(liberarParcialEntradaDTO.getListaLiberarParcial().get(i).getCdTipoCanal()));
	    ocorrencia.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(liberarParcialEntradaDTO.getListaLiberarParcial().get(i)
		    .getCdTipoContratoNegocio()));
	    ocorrencia.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(liberarParcialEntradaDTO.getListaLiberarParcial().get(i)
		    .getNrSequenciaContratoNegocio()));

	    lista.add(ocorrencia);

	}

	request.setOcorrencias((br.com.bradesco.web.pgit.service.data.pdc.liberarparcial.request.Ocorrencias[]) lista
		.toArray(new br.com.bradesco.web.pgit.service.data.pdc.liberarparcial.request.Ocorrencias[0]));

	response = getFactoryAdapter().getLiberarParcialPDCAdapter().invokeProcess(request);

	saidaDTO.setCdMensagem(response.getCodMensagem());
	saidaDTO.setMensagem(response.getMensagem());

	return saidaDTO;
    }

    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.liberarpagconsolidadosemconsultsaldo.ILiberarPagConsolidadoSemConsultSaldoService#liberarIntegral(br.com.bradesco.web.pgit.service.business.liberarpagconsolidadosemconsultsaldo.bean.LiberarIntegralEntradaDTO)
     */
    public LiberarIntegralSaidaDTO liberarIntegral(LiberarIntegralEntradaDTO liberarIntegralEntradaDTO) {
		LiberarIntegralSaidaDTO saidaDTO = new LiberarIntegralSaidaDTO();
		LiberarIntegralRequest request = new LiberarIntegralRequest();
		LiberarIntegralResponse response = new LiberarIntegralResponse();
	
		request.setCdPessoaJuridicaContrato(PgitUtil.verificaLongNulo(liberarIntegralEntradaDTO.getCdPessoaJuridicaContrato()));
		request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(liberarIntegralEntradaDTO.getCdTipoContratoNegocio()));
		request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(liberarIntegralEntradaDTO.getNrSequenciaContratoNegocio()));
		request.setCdProdutoServicoOperacao(PgitUtil.verificaIntegerNulo(liberarIntegralEntradaDTO.getCdProdutoServicoOperacao()));
		request.setCdProdutoServicoRelacionado(PgitUtil.verificaIntegerNulo(liberarIntegralEntradaDTO.getCdProdutoServicoRelacionado()));
		request.setCdCnpjCpf(PgitUtil.verificaLongNulo(liberarIntegralEntradaDTO.getCdCnpjCpf()));
		request.setCdFilialCnpjCpf(PgitUtil.verificaIntegerNulo(liberarIntegralEntradaDTO.getCdFilialCnpjCpf()));
		request.setCdControleCnpjCpf(PgitUtil.verificaIntegerNulo(liberarIntegralEntradaDTO.getCdControleCnpjCpf()));
		request.setNrArquivoRemessa(PgitUtil.verificaLongNulo(liberarIntegralEntradaDTO.getNrArquivoRemessa()));
		request.setHrInclusaoRemessaSistema(PgitUtil.verificaStringNula(liberarIntegralEntradaDTO.getHrInclusaoRemessaSistema()));
		request.setDtAgendamentoPagamento(PgitUtil.verificaStringNula(liberarIntegralEntradaDTO.getDtAgendamentoPagamento()));
		request.setCdBancoDebito(PgitUtil.verificaIntegerNulo(liberarIntegralEntradaDTO.getCdBancoDebito()));
		request.setCdAgenciaDebito(PgitUtil.verificaIntegerNulo(liberarIntegralEntradaDTO.getCdAgenciaDebito()));
		request.setCdContaDebito(PgitUtil.verificaLongNulo(liberarIntegralEntradaDTO.getCdContaDebito()));
		request.setCdSituacao(PgitUtil.verificaIntegerNulo(liberarIntegralEntradaDTO.getCdSituacao()));
	
		response = getFactoryAdapter().getLiberarIntegralPDCAdapter().invokeProcess(request);
		
		while ("PGIT0009".equals(response.getCodMensagem())) {
			response = getFactoryAdapter().getLiberarIntegralPDCAdapter().invokeProcess(request);
		}

		saidaDTO.setCdMensagem(response.getCodMensagem());
		saidaDTO.setMensagem(response.getMensagem());
	
		return saidaDTO;
    }

    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.liberarpagconsolidadosemconsultsaldo.ILiberarPagConsolidadoSemConsultSaldoService#detalharLibPagtosConsSemConsultaSaldo(br.com.bradesco.web.pgit.service.business.liberarpagconsolidadosemconsultsaldo.bean.DetalharLibPagtosConsSemConsultaSaldoEntradaDTO)
     */
    public DetalharLibPagtosConsSemConsultaSaldoSaidaDTO detalharLibPagtosConsSemConsultaSaldo(
	    DetalharLibPagtosConsSemConsultaSaldoEntradaDTO entradaDTO) {
	DetalharLibPagtosConsSemConsultaSaldoRequest request = new DetalharLibPagtosConsSemConsultaSaldoRequest();
	DetalharLibPagtosConsSemConsultaSaldoResponse response = new DetalharLibPagtosConsSemConsultaSaldoResponse();

	request.setCdAgencia(PgitUtil.verificaIntegerNulo(entradaDTO.getCdAgencia()));
	request.setCdBanco(PgitUtil.verificaIntegerNulo(entradaDTO.getCdBanco()));
	request.setCdConta(PgitUtil.verificaLongNulo(entradaDTO.getCdConta()));
	request.setCdDigitoConta(PgitUtil.complementaDigitoConta(entradaDTO.getCdDigitoConta()));
	request.setCdPessoaJuridicaContrato(PgitUtil.verificaLongNulo(entradaDTO.getCdPessoaJuridicaContrato()));
	request.setCdProdutoServicoOperacao(PgitUtil.verificaIntegerNulo(entradaDTO
		.getCdProdutoServicoOperacao()));
	request.setCdProdutoServicoRelacionado(PgitUtil.verificaIntegerNulo(entradaDTO
		.getCdProdutoServicoRelacionado()));
	request.setCdSituacaoOperacaoPagamento(PgitUtil.verificaIntegerNulo(entradaDTO
		.getCdSituacaoOperacaoPagamento()));
	request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoContratoNegocio()));
	request.setDtCreditoPagamento(PgitUtil.verificaStringNula(entradaDTO.getDtCreditoPagamento()));
	request.setNrArquivoRemessaPagamento(PgitUtil
		.verificaLongNulo(entradaDTO.getNrArquivoRemessaPagamento()));
	request.setNrCnpjCpf(PgitUtil.verificaLongNulo(entradaDTO.getNrCnpjCpf()));
	request.setNrControleCnpjCpf(PgitUtil.verificaIntegerNulo(entradaDTO.getNrControleCnpjCpf()));
	request.setNrOcorrencias(ILiberarPagConsolidadoSemConsultSaldoServiceConstants.NUMERO_OCORRENCIAS_DETALHAR);
	request.setNrFilialcnpjCpf(PgitUtil.verificaIntegerNulo(entradaDTO.getNrFilialCnpjCpf()));
	request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(entradaDTO
		.getNrSequenciaContratoNegocio()));
	request.setCdAgendadoPagaoNaoPago(PgitUtil.verificaIntegerNulo(entradaDTO.getCdAgendadoPagaoNaoPago()));
	request.setCdPessoaContratoDebito(PgitUtil.verificaLongNulo(entradaDTO.getCdPessoaContratoDebito()));
	request.setCdTipoContratoDebito(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoContratoDebito()));
	request.setNrSequenciaContratoDebito(PgitUtil
		.verificaLongNulo(entradaDTO.getNrSequenciaContratoDebito()));
	request.setCdTituloPgtoRastreado(0);
	request.setCdIndicadorAutorizacaoPagador(PgitUtil.verificaIntegerNulo(entradaDTO.getCdIndicadorAutorizacaoPagador()));
	request.setNrLoteInternoPagamento(PgitUtil.verificaLongNulo(entradaDTO.getNrLoteInternoPagamento()));

	response = getFactoryAdapter().getDetalharLibPagtosConsSemConsultaSaldoPDCAdapter().invokeProcess(request);
	DetalharLibPagtosConsSemConsultaSaldoSaidaDTO saidaDTO = new DetalharLibPagtosConsSemConsultaSaldoSaidaDTO();

	saidaDTO.setCdMensagem(response.getCodMensagem());
	saidaDTO.setMensagem(response.getMensagem());
	saidaDTO.setDsBancoDebito(response.getDsBancoDebito());
	saidaDTO.setDsAgenciaDebito(response.getDsAgenciaDebito());
	saidaDTO.setDsTipoContaDebito(response.getDsTipoContaDebito());
	saidaDTO.setDsContrato(response.getDsContrato());
	saidaDTO.setDsSituacaoContrato(response.getDsSituacaoContrato());
	saidaDTO.setNmCliente(response.getNmCliente());

	List<OcorrenciasDetLibPagConsSemConsSalSaidaDTO> listaDetalharLibPagtosConsSemConsultaSaldo = new ArrayList<OcorrenciasDetLibPagConsSemConsSalSaidaDTO>();
	OcorrenciasDetLibPagConsSemConsSalSaidaDTO ocorrencia;

	for (int i = 0; i < response.getOcorrenciasCount(); i++) {
	    ocorrencia = new OcorrenciasDetLibPagConsSemConsSalSaidaDTO();
	    ocorrencia.setNrPagamento(response.getOcorrencias(i).getNrPagamento());
	    ocorrencia.setNrPagamento(response.getOcorrencias(i).getNrPagamento());
	    ocorrencia.setVlPagamento(response.getOcorrencias(i).getVlPagamento());
	    ocorrencia.setCdCnpjCpfFavorecido(response.getOcorrencias(i).getCdCnpjCpfFavorecido());
	    ocorrencia.setCdFilialCnpjCpfFavorecido(response.getOcorrencias(i).getCdFilialCnpjCpfFavorecido());
	    ocorrencia.setCdControleCnpjCpfFavorecido(response.getOcorrencias(i).getCdControleCnpjCpfFavorecido());
	    ocorrencia.setCdFavorecido(response.getOcorrencias(i).getCdFavorecido() == 0L ? "" : String.valueOf(response.getOcorrencias(i)
		    .getCdFavorecido()));
	    ocorrencia.setDsBeneficio(response.getOcorrencias(i).getDsBeneficio());
	    ocorrencia.setCdBanco(response.getOcorrencias(i).getCdBanco());
	    ocorrencia.setCdAgencia(response.getOcorrencias(i).getCdAgencia());
	    ocorrencia.setCdDigitoAgencia(response.getOcorrencias(i).getCdDigitoAgencia());
	    ocorrencia.setCdConta(response.getOcorrencias(i).getCdConta());
	    ocorrencia.setCdDigitoConta(response.getOcorrencias(i).getCdDigitoConta());
	    ocorrencia.setCdTipoCanal(response.getOcorrencias(i).getCdTipoCanal());
	    ocorrencia.setCdTipoTela(response.getOcorrencias(i).getCdTipoTela());

	    ocorrencia.setBancoAgenciaContaCreditoFormatado(PgitUtil.formatBancoAgenciaConta(response.getOcorrencias(i).getCdBanco(), response
		    .getOcorrencias(i).getCdAgencia(), response.getOcorrencias(i).getCdDigitoAgencia(), response.getOcorrencias(i).getCdConta(),
		    response.getOcorrencias(i).getCdDigitoConta(), true));
	    ocorrencia.setBancoFormatado(PgitUtil.formatBanco(ocorrencia.getCdBanco(), false));
	    ocorrencia.setAgenciaFormatada(PgitUtil.formatAgencia(ocorrencia.getCdAgencia(), ocorrencia.getCdDigitoAgencia(), false));
	    ocorrencia.setContaFormatada(PgitUtil.formatConta(response.getOcorrencias(i).getCdConta(), response.getOcorrencias(i).getCdDigitoConta(),
		    false));
	    ocorrencia.setDsEfetivacaoPagamento(response.getOcorrencias(i).getDsEfetivacaoPagamento());

	    ocorrencia.setFavorecidoBeneficiario(response.getOcorrencias(i).getDsBeneficio());
	    ocorrencia.setCdIspbPagtoDestino(response.getOcorrencias(i).getCdIspbPagtoDestino());
	    ocorrencia.setContaPagtoDestino(response.getOcorrencias(i).getContaPagtoDestino());		
	    
	    listaDetalharLibPagtosConsSemConsultaSaldo.add(ocorrencia);
	}
	saidaDTO.setListaDetalharLibPagtosConsSemConsultaSaldo(listaDetalharLibPagtosConsSemConsultaSaldo);

	return saidaDTO;
    }

}
