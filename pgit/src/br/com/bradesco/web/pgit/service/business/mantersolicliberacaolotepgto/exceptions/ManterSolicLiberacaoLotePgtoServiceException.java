/**
 * Nome: br.com.bradesco.web.pgit.service.business.mantersolicliberacaolotepgto.exceptions
 * Compilador: JDK 1.5
 * Prop�sito: INSERIR O PROP�SITO DAS CLASSES DO PACOTE
 * Data da cria��o: <dd/MM/yyyy>
 * Par�metros de compila��o: -d
 */
package br.com.bradesco.web.pgit.service.business.mantersolicliberacaolotepgto.exceptions;

import br.com.bradesco.web.aq.application.error.BradescoApplicationException;

/**
 * Nome: ManterSolicLiberacaoLotePgtoServiceException
 * <p>
 * Prop�sito: Classe de exce��o do adaptador ManterSolicLiberacaoLotePgto
 * </p>
 * 
 * @author CPM Braxis / TI Melhorias - Arquitetura
 * @version 1.0
 * @see BradescoApplicationException
 */
public class ManterSolicLiberacaoLotePgtoServiceException extends BradescoApplicationException  {

}