package br.com.bradesco.web.pgit.service.business.vincmsglanctopecontratada.bean;

public class DetalharVincMsgLancOperEntradaDTO {

	/** Atributo cdpessoaJuridicaContrato. */
	private Long cdPessoaJuridica;

	/** Atributo cdpessoaJuridicaContrato. */
	private Integer cdTipoContratoNegocio;

	/** Atributo cdpessoaJuridicaContrato. */
	private Long nrSequenciaContratoNegocio;

	/** Atributo cdpessoaJuridicaContrato. */
	private Integer cdProdutoServicoOperacao;

	/** Atributo cdpessoaJuridicaContrato. */
	private Integer cdprodutoOperacaoRelacionado;

	/** Atributo cdpessoaJuridicaContrato. */
	private Integer cdRelacionamentoProdutoProduto;

	/** Atributo cdpessoaJuridicaContrato. */
	private Integer cdMensagemLinhaExtrato;

	/** Atributo cdpessoaJuridicaContrato. */
	private Integer qtOcorrencia;

	public Long getCdPessoaJuridica() {
		return cdPessoaJuridica;
	}

	public void setCdPessoaJuridica(Long cdPessoaJuridica) {
		this.cdPessoaJuridica = cdPessoaJuridica;
	}

	public Integer getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}

	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}

	public Long getNrSequenciaContratoNegocio() {
		return nrSequenciaContratoNegocio;
	}

	public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}

	public Integer getCdProdutoServicoOperacao() {
		return cdProdutoServicoOperacao;
	}

	public void setCdProdutoServicoOperacao(Integer cdProdutoServicoOperacao) {
		this.cdProdutoServicoOperacao = cdProdutoServicoOperacao;
	}

	public Integer getCdprodutoOperacaoRelacionado() {
		return cdprodutoOperacaoRelacionado;
	}

	public void setCdprodutoOperacaoRelacionado(
			Integer cdprodutoOperacaoRelacionado) {
		this.cdprodutoOperacaoRelacionado = cdprodutoOperacaoRelacionado;
	}

	public Integer getCdRelacionamentoProdutoProduto() {
		return cdRelacionamentoProdutoProduto;
	}

	public void setCdRelacionamentoProdutoProduto(
			Integer cdRelacionamentoProdutoProduto) {
		this.cdRelacionamentoProdutoProduto = cdRelacionamentoProdutoProduto;
	}

	public Integer getCdMensagemLinhaExtrato() {
		return cdMensagemLinhaExtrato;
	}

	public void setCdMensagemLinhaExtrato(Integer cdMensagemLinhaExtrato) {
		this.cdMensagemLinhaExtrato = cdMensagemLinhaExtrato;
	}

	public Integer getQtOcorrencia() {
		return qtOcorrencia;
	}

	public void setQtOcorrencia(Integer qtOcorrencia) {
		this.qtOcorrencia = qtOcorrencia;
	}

}
