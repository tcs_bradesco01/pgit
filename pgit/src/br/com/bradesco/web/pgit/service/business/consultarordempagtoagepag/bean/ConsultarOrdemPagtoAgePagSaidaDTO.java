/*
 * Nome: br.com.bradesco.web.pgit.service.business.consultarordempagtoagepag.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.consultarordempagtoagepag.bean;

import java.math.BigDecimal;

/**
 * Nome: ConsultarOrdemPagtoAgePagSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarOrdemPagtoAgePagSaidaDTO {

    /** Atributo corpoCpfPagador. */
    private Long corpoCpfPagador;

    /** Atributo filialCpfPagador. */
    private Integer filialCpfPagador;

    /** Atributo controleCpfPagador. */
    private Integer controleCpfPagador;

    /** Atributo dsClientePagador. */
    private String dsClientePagador;

    /** Atributo cdAgenciaPagadora. */
    private Integer cdAgenciaPagadora;

    /** Atributo cdInscricaoFavorecido. */
    private Long cdInscricaoFavorecido;

    /** Atributo cdIdentificacaoInscricaoFavorecido. */
    private Integer cdIdentificacaoInscricaoFavorecido;

    /** Atributo dsIdentificacaoInscricaoFavorecido. */
    private String dsIdentificacaoInscricaoFavorecido;

    /** Atributo dsAbrevFavorecido. */
    private String dsAbrevFavorecido;

    /** Atributo dtCreditoPagamento. */
    private String dtCreditoPagamento;

    /** Atributo cdControlePagamento. */
    private String cdControlePagamento;

    /** Atributo vlPagamentoClientePagador. */
    private BigDecimal vlPagamentoClientePagador;

    /** Atributo dtDisponivelAte. */
    private String dtDisponivelAte;

    /** Atributo cdSituacaoOperacao. */
    private Integer cdSituacaoOperacao;

    /** Atributo cdMotivoSituacaoPagamento. */
    private String cdMotivoSituacaoPagamento;

    /** Atributo cdPessoaJuridicaContrato. */
    private Long cdPessoaJuridicaContrato;

    /** Atributo cdTipoContratoNegocio. */
    private Integer cdTipoContratoNegocio;

    /** Atributo nrSequenciaContratoNegocio. */
    private Long nrSequenciaContratoNegocio;

    /** Atributo cdBancoDebito. */
    private Integer cdBancoDebito;

    /** Atributo cdAgenciaDebito. */
    private Integer cdAgenciaDebito;

    /** Atributo digitoAgenciaDebito. */
    private String digitoAgenciaDebito;

    /** Atributo contaDebito. */
    private Long contaDebito;

    /** Atributo digitoContaDebito. */
    private String digitoContaDebito;

    /** Atributo cdBancoCredito. */
    private Integer cdBancoCredito;

    /** Atributo cdAgenciaCredito. */
    private Integer cdAgenciaCredito;

    /** Atributo digitoAgenciaCredito. */
    private String digitoAgenciaCredito;

    /** Atributo cdContaCredito. */
    private Long cdContaCredito;

    /** Atributo digitoContaCredito. */
    private String digitoContaCredito;

    /** Atributo dsTipoContaCredito. */
    private String dsTipoContaCredito;

    /** Atributo cdServido. */
    private Integer cdServido;

    /** Atributo cdModalidade. */
    private Integer cdModalidade;

    /** Atributo dsServico. */
    private String dsServico;

    /** Atributo dtCreditoPagamentoFormatada. */
    private String dtCreditoPagamentoFormatada;

    /** Atributo dtDisponivelAteFormatada. */
    private String dtDisponivelAteFormatada;

    /** Atributo clientePagadorFormatado. */
    private String clientePagadorFormatado;

    /** Atributo cpfCnpjFormatado. */
    private String cpfCnpjFormatado;

    /** Atributo bancoCreditoFormatado. */
    private String bancoCreditoFormatado;

    /** Atributo agenciaCreditoFormatada. */
    private String agenciaCreditoFormatada;

    /** Atributo contaCreditoFormatada. */
    private String contaCreditoFormatada;

    /** Atributo cdTipoRetornoPagamento. */
    private Integer cdTipoRetornoPagamento;
    
    /** Atributo descTipoRetornoPagamento. */
    private String descTipoRetornoPagamento = null;

    /** Atributo cdFavorecido. */
    private Long cdFavorecido;

    /** Atributo inscricaoFavorecidoFormatado. */
    private String inscricaoFavorecidoFormatado;

    /** Atributo dsEfetivacaoPagamento. */
    private String dsEfetivacaoPagamento;

    /** Atributo cdAgenciaVencimento. */
    private Integer cdAgenciaVencimento;

    /**
     * Get: cdAgenciaVencimento.
     *
     * @return cdAgenciaVencimento
     */
    public Integer getCdAgenciaVencimento() {
	return cdAgenciaVencimento;
    }

    /**
     * Set: cdAgenciaVencimento.
     *
     * @param cdAgenciaVencimento the cd agencia vencimento
     */
    public void setCdAgenciaVencimento(Integer cdAgenciaVencimento) {
	this.cdAgenciaVencimento = cdAgenciaVencimento;
    }

    /**
     * Get: dsEfetivacaoPagamento.
     *
     * @return dsEfetivacaoPagamento
     */
    public String getDsEfetivacaoPagamento() {
	return dsEfetivacaoPagamento;
    }

    /**
     * Set: dsEfetivacaoPagamento.
     *
     * @param dsEfetivacaoPagamento the ds efetivacao pagamento
     */
    public void setDsEfetivacaoPagamento(String dsEfetivacaoPagamento) {
	this.dsEfetivacaoPagamento = dsEfetivacaoPagamento;
    }

    /**
     * Get: inscricaoFavorecidoFormatado.
     *
     * @return inscricaoFavorecidoFormatado
     */
    public String getInscricaoFavorecidoFormatado() {
	return inscricaoFavorecidoFormatado;
    }

    /**
     * Set: inscricaoFavorecidoFormatado.
     *
     * @param inscricaoFavorecidoFormatado the inscricao favorecido formatado
     */
    public void setInscricaoFavorecidoFormatado(String inscricaoFavorecidoFormatado) {
	this.inscricaoFavorecidoFormatado = inscricaoFavorecidoFormatado;
    }

    /**
     * Get: cdFavorecido.
     *
     * @return cdFavorecido
     */
    public Long getCdFavorecido() {
	return cdFavorecido;
    }

    /**
     * Set: cdFavorecido.
     *
     * @param cdFavorecido the cd favorecido
     */
    public void setCdFavorecido(Long cdFavorecido) {
	this.cdFavorecido = cdFavorecido;
    }

    /**
     * Get: cdTipoRetornoPagamento.
     *
     * @return cdTipoRetornoPagamento
     */
    public Integer getCdTipoRetornoPagamento() {
	return cdTipoRetornoPagamento;
    }

    /**
     * Set: cdTipoRetornoPagamento.
     *
     * @param cdTipoRetornoPagamento the cd tipo retorno pagamento
     */
    public void setCdTipoRetornoPagamento(Integer cdTipoRetornoPagamento) {
	this.cdTipoRetornoPagamento = cdTipoRetornoPagamento;
    }

    /**
     * Get: cdAgenciaCredito.
     *
     * @return cdAgenciaCredito
     */
    public Integer getCdAgenciaCredito() {
	return cdAgenciaCredito;
    }

    /**
     * Set: cdAgenciaCredito.
     *
     * @param cdAgenciaCredito the cd agencia credito
     */
    public void setCdAgenciaCredito(Integer cdAgenciaCredito) {
	this.cdAgenciaCredito = cdAgenciaCredito;
    }

    /**
     * Get: cdAgenciaDebito.
     *
     * @return cdAgenciaDebito
     */
    public Integer getCdAgenciaDebito() {
	return cdAgenciaDebito;
    }

    /**
     * Set: cdAgenciaDebito.
     *
     * @param cdAgenciaDebito the cd agencia debito
     */
    public void setCdAgenciaDebito(Integer cdAgenciaDebito) {
	this.cdAgenciaDebito = cdAgenciaDebito;
    }

    /**
     * Get: cdAgenciaPagadora.
     *
     * @return cdAgenciaPagadora
     */
    public Integer getCdAgenciaPagadora() {
	return cdAgenciaPagadora;
    }

    /**
     * Set: cdAgenciaPagadora.
     *
     * @param cdAgenciaPagadora the cd agencia pagadora
     */
    public void setCdAgenciaPagadora(Integer cdAgenciaPagadora) {
	this.cdAgenciaPagadora = cdAgenciaPagadora;
    }

    /**
     * Get: cdBancoCredito.
     *
     * @return cdBancoCredito
     */
    public Integer getCdBancoCredito() {
	return cdBancoCredito;
    }

    /**
     * Set: cdBancoCredito.
     *
     * @param cdBancoCredito the cd banco credito
     */
    public void setCdBancoCredito(Integer cdBancoCredito) {
	this.cdBancoCredito = cdBancoCredito;
    }

    /**
     * Get: cdBancoDebito.
     *
     * @return cdBancoDebito
     */
    public Integer getCdBancoDebito() {
	return cdBancoDebito;
    }

    /**
     * Set: cdBancoDebito.
     *
     * @param cdBancoDebito the cd banco debito
     */
    public void setCdBancoDebito(Integer cdBancoDebito) {
	this.cdBancoDebito = cdBancoDebito;
    }

    /**
     * Get: cdContaCredito.
     *
     * @return cdContaCredito
     */
    public Long getCdContaCredito() {
	return cdContaCredito;
    }

    /**
     * Set: cdContaCredito.
     *
     * @param cdContaCredito the cd conta credito
     */
    public void setCdContaCredito(Long cdContaCredito) {
	this.cdContaCredito = cdContaCredito;
    }

    /**
     * Get: cdControlePagamento.
     *
     * @return cdControlePagamento
     */
    public String getCdControlePagamento() {
	return cdControlePagamento;
    }

    /**
     * Set: cdControlePagamento.
     *
     * @param cdControlePagamento the cd controle pagamento
     */
    public void setCdControlePagamento(String cdControlePagamento) {
	this.cdControlePagamento = cdControlePagamento;
    }

    /**
     * Get: cdIdentificacaoInscricaoFavorecido.
     *
     * @return cdIdentificacaoInscricaoFavorecido
     */
    public Integer getCdIdentificacaoInscricaoFavorecido() {
	return cdIdentificacaoInscricaoFavorecido;
    }

    /**
     * Set: cdIdentificacaoInscricaoFavorecido.
     *
     * @param cdIdentificacaoInscricaoFavorecido the cd identificacao inscricao favorecido
     */
    public void setCdIdentificacaoInscricaoFavorecido(Integer cdIdentificacaoInscricaoFavorecido) {
	this.cdIdentificacaoInscricaoFavorecido = cdIdentificacaoInscricaoFavorecido;
    }

    /**
     * Get: cdInscricaoFavorecido.
     *
     * @return cdInscricaoFavorecido
     */
    public Long getCdInscricaoFavorecido() {
	return cdInscricaoFavorecido;
    }

    /**
     * Set: cdInscricaoFavorecido.
     *
     * @param cdInscricaoFavorecido the cd inscricao favorecido
     */
    public void setCdInscricaoFavorecido(Long cdInscricaoFavorecido) {
	this.cdInscricaoFavorecido = cdInscricaoFavorecido;
    }

    /**
     * Get: cdModalidade.
     *
     * @return cdModalidade
     */
    public Integer getCdModalidade() {
	return cdModalidade;
    }

    /**
     * Set: cdModalidade.
     *
     * @param cdModalidade the cd modalidade
     */
    public void setCdModalidade(Integer cdModalidade) {
	this.cdModalidade = cdModalidade;
    }

    /**
     * Get: cdMotivoSituacaoPagamento.
     *
     * @return cdMotivoSituacaoPagamento
     */
    public String getCdMotivoSituacaoPagamento() {
	return cdMotivoSituacaoPagamento;
    }

    /**
     * Set: cdMotivoSituacaoPagamento.
     *
     * @param cdMotivoSituacaoPagamento the cd motivo situacao pagamento
     */
    public void setCdMotivoSituacaoPagamento(String cdMotivoSituacaoPagamento) {
	this.cdMotivoSituacaoPagamento = cdMotivoSituacaoPagamento;
    }

    /**
     * Get: cdPessoaJuridicaContrato.
     *
     * @return cdPessoaJuridicaContrato
     */
    public Long getCdPessoaJuridicaContrato() {
	return cdPessoaJuridicaContrato;
    }

    /**
     * Set: cdPessoaJuridicaContrato.
     *
     * @param cdPessoaJuridicaContrato the cd pessoa juridica contrato
     */
    public void setCdPessoaJuridicaContrato(Long cdPessoaJuridicaContrato) {
	this.cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
    }

    /**
     * Get: cdServido.
     *
     * @return cdServido
     */
    public Integer getCdServido() {
	return cdServido;
    }

    /**
     * Set: cdServido.
     *
     * @param cdServido the cd servido
     */
    public void setCdServido(Integer cdServido) {
	this.cdServido = cdServido;
    }

    /**
     * Get: cdSituacaoOperacao.
     *
     * @return cdSituacaoOperacao
     */
    public Integer getCdSituacaoOperacao() {
	return cdSituacaoOperacao;
    }

    /**
     * Set: cdSituacaoOperacao.
     *
     * @param cdSituacaoOperacao the cd situacao operacao
     */
    public void setCdSituacaoOperacao(Integer cdSituacaoOperacao) {
	this.cdSituacaoOperacao = cdSituacaoOperacao;
    }

    /**
     * Get: cdTipoContratoNegocio.
     *
     * @return cdTipoContratoNegocio
     */
    public Integer getCdTipoContratoNegocio() {
	return cdTipoContratoNegocio;
    }

    /**
     * Set: cdTipoContratoNegocio.
     *
     * @param cdTipoContratoNegocio the cd tipo contrato negocio
     */
    public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
	this.cdTipoContratoNegocio = cdTipoContratoNegocio;
    }

    /**
     * Get: contaDebito.
     *
     * @return contaDebito
     */
    public Long getContaDebito() {
	return contaDebito;
    }

    /**
     * Set: contaDebito.
     *
     * @param contaDebito the conta debito
     */
    public void setContaDebito(Long contaDebito) {
	this.contaDebito = contaDebito;
    }

    /**
     * Get: controleCpfPagador.
     *
     * @return controleCpfPagador
     */
    public Integer getControleCpfPagador() {
	return controleCpfPagador;
    }

    /**
     * Set: controleCpfPagador.
     *
     * @param controleCpfPagador the controle cpf pagador
     */
    public void setControleCpfPagador(Integer controleCpfPagador) {
	this.controleCpfPagador = controleCpfPagador;
    }

    /**
     * Get: corpoCpfPagador.
     *
     * @return corpoCpfPagador
     */
    public Long getCorpoCpfPagador() {
	return corpoCpfPagador;
    }

    /**
     * Set: corpoCpfPagador.
     *
     * @param corpoCpfPagador the corpo cpf pagador
     */
    public void setCorpoCpfPagador(Long corpoCpfPagador) {
	this.corpoCpfPagador = corpoCpfPagador;
    }

    /**
     * Get: digitoAgenciaCredito.
     *
     * @return digitoAgenciaCredito
     */
    public String getDigitoAgenciaCredito() {
	return digitoAgenciaCredito;
    }

    /**
     * Set: digitoAgenciaCredito.
     *
     * @param digitoAgenciaCredito the digito agencia credito
     */
    public void setDigitoAgenciaCredito(String digitoAgenciaCredito) {
	this.digitoAgenciaCredito = digitoAgenciaCredito;
    }

    /**
     * Get: digitoAgenciaDebito.
     *
     * @return digitoAgenciaDebito
     */
    public String getDigitoAgenciaDebito() {
	return digitoAgenciaDebito;
    }

    /**
     * Set: digitoAgenciaDebito.
     *
     * @param digitoAgenciaDebito the digito agencia debito
     */
    public void setDigitoAgenciaDebito(String digitoAgenciaDebito) {
	this.digitoAgenciaDebito = digitoAgenciaDebito;
    }

    /**
     * Get: digitoContaCredito.
     *
     * @return digitoContaCredito
     */
    public String getDigitoContaCredito() {
	return digitoContaCredito;
    }

    /**
     * Set: digitoContaCredito.
     *
     * @param digitoContaCredito the digito conta credito
     */
    public void setDigitoContaCredito(String digitoContaCredito) {
	this.digitoContaCredito = digitoContaCredito;
    }

    /**
     * Get: digitoContaDebito.
     *
     * @return digitoContaDebito
     */
    public String getDigitoContaDebito() {
	return digitoContaDebito;
    }

    /**
     * Set: digitoContaDebito.
     *
     * @param digitoContaDebito the digito conta debito
     */
    public void setDigitoContaDebito(String digitoContaDebito) {
	this.digitoContaDebito = digitoContaDebito;
    }

    /**
     * Get: dsAbrevFavorecido.
     *
     * @return dsAbrevFavorecido
     */
    public String getDsAbrevFavorecido() {
	return dsAbrevFavorecido;
    }

    /**
     * Set: dsAbrevFavorecido.
     *
     * @param dsAbrevFavorecido the ds abrev favorecido
     */
    public void setDsAbrevFavorecido(String dsAbrevFavorecido) {
	this.dsAbrevFavorecido = dsAbrevFavorecido;
    }

    /**
     * Get: dsClientePagador.
     *
     * @return dsClientePagador
     */
    public String getDsClientePagador() {
	return dsClientePagador;
    }

    /**
     * Set: dsClientePagador.
     *
     * @param dsClientePagador the ds cliente pagador
     */
    public void setDsClientePagador(String dsClientePagador) {
	this.dsClientePagador = dsClientePagador;
    }

    /**
     * Get: dsIdentificacaoInscricaoFavorecido.
     *
     * @return dsIdentificacaoInscricaoFavorecido
     */
    public String getDsIdentificacaoInscricaoFavorecido() {
	return dsIdentificacaoInscricaoFavorecido;
    }

    /**
     * Set: dsIdentificacaoInscricaoFavorecido.
     *
     * @param dsIdentificacaoInscricaoFavorecido the ds identificacao inscricao favorecido
     */
    public void setDsIdentificacaoInscricaoFavorecido(String dsIdentificacaoInscricaoFavorecido) {
	this.dsIdentificacaoInscricaoFavorecido = dsIdentificacaoInscricaoFavorecido;
    }

    /**
     * Get: dsServico.
     *
     * @return dsServico
     */
    public String getDsServico() {
	return dsServico;
    }

    /**
     * Set: dsServico.
     *
     * @param dsServico the ds servico
     */
    public void setDsServico(String dsServico) {
	this.dsServico = dsServico;
    }

    /**
     * Get: dsTipoContaCredito.
     *
     * @return dsTipoContaCredito
     */
    public String getDsTipoContaCredito() {
	return dsTipoContaCredito;
    }

    /**
     * Set: dsTipoContaCredito.
     *
     * @param dsTipoContaCredito the ds tipo conta credito
     */
    public void setDsTipoContaCredito(String dsTipoContaCredito) {
	this.dsTipoContaCredito = dsTipoContaCredito;
    }

    /**
     * Get: dtCreditoPagamento.
     *
     * @return dtCreditoPagamento
     */
    public String getDtCreditoPagamento() {
	return dtCreditoPagamento;
    }

    /**
     * Set: dtCreditoPagamento.
     *
     * @param dtCreditoPagamento the dt credito pagamento
     */
    public void setDtCreditoPagamento(String dtCreditoPagamento) {
	this.dtCreditoPagamento = dtCreditoPagamento;
    }

    /**
     * Get: dtDisponivelAte.
     *
     * @return dtDisponivelAte
     */
    public String getDtDisponivelAte() {
	return dtDisponivelAte;
    }

    /**
     * Set: dtDisponivelAte.
     *
     * @param dtDisponivelAte the dt disponivel ate
     */
    public void setDtDisponivelAte(String dtDisponivelAte) {
	this.dtDisponivelAte = dtDisponivelAte;
    }

    /**
     * Get: filialCpfPagador.
     *
     * @return filialCpfPagador
     */
    public Integer getFilialCpfPagador() {
	return filialCpfPagador;
    }

    /**
     * Set: filialCpfPagador.
     *
     * @param filialCpfPagador the filial cpf pagador
     */
    public void setFilialCpfPagador(Integer filialCpfPagador) {
	this.filialCpfPagador = filialCpfPagador;
    }

    /**
     * Get: nrSequenciaContratoNegocio.
     *
     * @return nrSequenciaContratoNegocio
     */
    public Long getNrSequenciaContratoNegocio() {
	return nrSequenciaContratoNegocio;
    }

    /**
     * Set: nrSequenciaContratoNegocio.
     *
     * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
     */
    public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
	this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
    }

    /**
     * Get: vlPagamentoClientePagador.
     *
     * @return vlPagamentoClientePagador
     */
    public BigDecimal getVlPagamentoClientePagador() {
	return vlPagamentoClientePagador;
    }

    /**
     * Set: vlPagamentoClientePagador.
     *
     * @param vlPagamentoClientePagador the vl pagamento cliente pagador
     */
    public void setVlPagamentoClientePagador(BigDecimal vlPagamentoClientePagador) {
	this.vlPagamentoClientePagador = vlPagamentoClientePagador;
    }

    /**
     * Get: clientePagadorFormatado.
     *
     * @return clientePagadorFormatado
     */
    public String getClientePagadorFormatado() {
	return clientePagadorFormatado;
    }

    /**
     * Set: clientePagadorFormatado.
     *
     * @param clientePagadorFormatado the cliente pagador formatado
     */
    public void setClientePagadorFormatado(String clientePagadorFormatado) {
	this.clientePagadorFormatado = clientePagadorFormatado;
    }

    /**
     * Get: cpfCnpjFormatado.
     *
     * @return cpfCnpjFormatado
     */
    public String getCpfCnpjFormatado() {
	return cpfCnpjFormatado;
    }

    /**
     * Set: cpfCnpjFormatado.
     *
     * @param cpfCnpjFormatado the cpf cnpj formatado
     */
    public void setCpfCnpjFormatado(String cpfCnpjFormatado) {
	this.cpfCnpjFormatado = cpfCnpjFormatado;
    }

    /**
     * Get: agenciaCreditoFormatada.
     *
     * @return agenciaCreditoFormatada
     */
    public String getAgenciaCreditoFormatada() {
	return agenciaCreditoFormatada;
    }

    /**
     * Set: agenciaCreditoFormatada.
     *
     * @param agenciaCreditoFormatada the agencia credito formatada
     */
    public void setAgenciaCreditoFormatada(String agenciaCreditoFormatada) {
	this.agenciaCreditoFormatada = agenciaCreditoFormatada;
    }

    /**
     * Get: bancoCreditoFormatado.
     *
     * @return bancoCreditoFormatado
     */
    public String getBancoCreditoFormatado() {
	return bancoCreditoFormatado;
    }

    /**
     * Set: bancoCreditoFormatado.
     *
     * @param bancoCreditoFormatado the banco credito formatado
     */
    public void setBancoCreditoFormatado(String bancoCreditoFormatado) {
	this.bancoCreditoFormatado = bancoCreditoFormatado;
    }

    /**
     * Get: contaCreditoFormatada.
     *
     * @return contaCreditoFormatada
     */
    public String getContaCreditoFormatada() {
	return contaCreditoFormatada;
    }

    /**
     * Set: contaCreditoFormatada.
     *
     * @param contaCreditoFormatada the conta credito formatada
     */
    public void setContaCreditoFormatada(String contaCreditoFormatada) {
	this.contaCreditoFormatada = contaCreditoFormatada;
    }

    /**
     * Get: dtCreditoPagamentoFormatada.
     *
     * @return dtCreditoPagamentoFormatada
     */
    public String getDtCreditoPagamentoFormatada() {
	return dtCreditoPagamentoFormatada;
    }

    /**
     * Set: dtCreditoPagamentoFormatada.
     *
     * @param dtCreditoPagamentoFormatada the dt credito pagamento formatada
     */
    public void setDtCreditoPagamentoFormatada(String dtCreditoPagamentoFormatada) {
	this.dtCreditoPagamentoFormatada = dtCreditoPagamentoFormatada;
    }

    /**
     * Get: dtDisponivelAteFormatada.
     *
     * @return dtDisponivelAteFormatada
     */
    public String getDtDisponivelAteFormatada() {
	return dtDisponivelAteFormatada;
    }

    /**
     * Set: dtDisponivelAteFormatada.
     *
     * @param dtDisponivelAteFormatada the dt disponivel ate formatada
     */
    public void setDtDisponivelAteFormatada(String dtDisponivelAteFormatada) {
	this.dtDisponivelAteFormatada = dtDisponivelAteFormatada;
    }

    /**
     * Nome: getDescTipoRetornoPagamento
     *
     * @return descTipoRetornoPagamento
     */
    public String getDescTipoRetornoPagamento() {
        return descTipoRetornoPagamento;
    }

    /**
     * Nome: setDescTipoRetornoPagamento
     *
     * @param descTipoRetornoPagamento
     */
    public void setDescTipoRetornoPagamento(String descTipoRetornoPagamento) {
        this.descTipoRetornoPagamento = descTipoRetornoPagamento;
    }

}