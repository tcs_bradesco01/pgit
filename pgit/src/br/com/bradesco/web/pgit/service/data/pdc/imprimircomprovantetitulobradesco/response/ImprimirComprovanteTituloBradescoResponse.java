/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.imprimircomprovantetitulobradesco.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.util.Enumeration;
import java.util.Vector;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ImprimirComprovanteTituloBradescoResponse.
 * 
 * @version $Revision$ $Date$
 */
public class ImprimirComprovanteTituloBradescoResponse implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _codMensagem
     */
    private java.lang.String _codMensagem;

    /**
     * Field _mensagem
     */
    private java.lang.String _mensagem;

    /**
     * Field _quantidadeOcorrencia
     */
    private int _quantidadeOcorrencia = 0;

    /**
     * keeps track of state for field: _quantidadeOcorrencia
     */
    private boolean _has_quantidadeOcorrencia;

    /**
     * Field _ocorrenciaList
     */
    private java.util.Vector _ocorrenciaList;


      //----------------/
     //- Constructors -/
    //----------------/

    public ImprimirComprovanteTituloBradescoResponse() 
     {
        super();
        _ocorrenciaList = new Vector();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimircomprovantetitulobradesco.response.ImprimirComprovanteTituloBradescoResponse()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method addOcorrencia
     * 
     * 
     * 
     * @param vOcorrencia
     */
    public void addOcorrencia(br.com.bradesco.web.pgit.service.data.pdc.imprimircomprovantetitulobradesco.response.Ocorrencia vOcorrencia)
        throws java.lang.IndexOutOfBoundsException
    {
        _ocorrenciaList.addElement(vOcorrencia);
    } //-- void addOcorrencia(br.com.bradesco.web.pgit.service.data.pdc.imprimircomprovantetitulobradesco.response.Ocorrencia) 

    /**
     * Method addOcorrencia
     * 
     * 
     * 
     * @param index
     * @param vOcorrencia
     */
    public void addOcorrencia(int index, br.com.bradesco.web.pgit.service.data.pdc.imprimircomprovantetitulobradesco.response.Ocorrencia vOcorrencia)
        throws java.lang.IndexOutOfBoundsException
    {
        _ocorrenciaList.insertElementAt(vOcorrencia, index);
    } //-- void addOcorrencia(int, br.com.bradesco.web.pgit.service.data.pdc.imprimircomprovantetitulobradesco.response.Ocorrencia) 

    /**
     * Method deleteQuantidadeOcorrencia
     * 
     */
    public void deleteQuantidadeOcorrencia()
    {
        this._has_quantidadeOcorrencia= false;
    } //-- void deleteQuantidadeOcorrencia() 

    /**
     * Method enumerateOcorrencia
     * 
     * 
     * 
     * @return Enumeration
     */
    public java.util.Enumeration enumerateOcorrencia()
    {
        return _ocorrenciaList.elements();
    } //-- java.util.Enumeration enumerateOcorrencia() 

    /**
     * Returns the value of field 'codMensagem'.
     * 
     * @return String
     * @return the value of field 'codMensagem'.
     */
    public java.lang.String getCodMensagem()
    {
        return this._codMensagem;
    } //-- java.lang.String getCodMensagem() 

    /**
     * Returns the value of field 'mensagem'.
     * 
     * @return String
     * @return the value of field 'mensagem'.
     */
    public java.lang.String getMensagem()
    {
        return this._mensagem;
    } //-- java.lang.String getMensagem() 

    /**
     * Method getOcorrencia
     * 
     * 
     * 
     * @param index
     * @return Ocorrencia
     */
    public br.com.bradesco.web.pgit.service.data.pdc.imprimircomprovantetitulobradesco.response.Ocorrencia getOcorrencia(int index)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index >= _ocorrenciaList.size())) {
            throw new IndexOutOfBoundsException("getOcorrencia: Index value '"+index+"' not in range [0.."+(_ocorrenciaList.size() - 1) + "]");
        }
        
        return (br.com.bradesco.web.pgit.service.data.pdc.imprimircomprovantetitulobradesco.response.Ocorrencia) _ocorrenciaList.elementAt(index);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimircomprovantetitulobradesco.response.Ocorrencia getOcorrencia(int) 

    /**
     * Method getOcorrencia
     * 
     * 
     * 
     * @return Ocorrencia
     */
    public br.com.bradesco.web.pgit.service.data.pdc.imprimircomprovantetitulobradesco.response.Ocorrencia[] getOcorrencia()
    {
        int size = _ocorrenciaList.size();
        br.com.bradesco.web.pgit.service.data.pdc.imprimircomprovantetitulobradesco.response.Ocorrencia[] mArray = new br.com.bradesco.web.pgit.service.data.pdc.imprimircomprovantetitulobradesco.response.Ocorrencia[size];
        for (int index = 0; index < size; index++) {
            mArray[index] = (br.com.bradesco.web.pgit.service.data.pdc.imprimircomprovantetitulobradesco.response.Ocorrencia) _ocorrenciaList.elementAt(index);
        }
        return mArray;
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimircomprovantetitulobradesco.response.Ocorrencia[] getOcorrencia() 

    /**
     * Method getOcorrenciaCount
     * 
     * 
     * 
     * @return int
     */
    public int getOcorrenciaCount()
    {
        return _ocorrenciaList.size();
    } //-- int getOcorrenciaCount() 

    /**
     * Returns the value of field 'quantidadeOcorrencia'.
     * 
     * @return int
     * @return the value of field 'quantidadeOcorrencia'.
     */
    public int getQuantidadeOcorrencia()
    {
        return this._quantidadeOcorrencia;
    } //-- int getQuantidadeOcorrencia() 

    /**
     * Method hasQuantidadeOcorrencia
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQuantidadeOcorrencia()
    {
        return this._has_quantidadeOcorrencia;
    } //-- boolean hasQuantidadeOcorrencia() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Method removeAllOcorrencia
     * 
     */
    public void removeAllOcorrencia()
    {
        _ocorrenciaList.removeAllElements();
    } //-- void removeAllOcorrencia() 

    /**
     * Method removeOcorrencia
     * 
     * 
     * 
     * @param index
     * @return Ocorrencia
     */
    public br.com.bradesco.web.pgit.service.data.pdc.imprimircomprovantetitulobradesco.response.Ocorrencia removeOcorrencia(int index)
    {
        java.lang.Object obj = _ocorrenciaList.elementAt(index);
        _ocorrenciaList.removeElementAt(index);
        return (br.com.bradesco.web.pgit.service.data.pdc.imprimircomprovantetitulobradesco.response.Ocorrencia) obj;
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimircomprovantetitulobradesco.response.Ocorrencia removeOcorrencia(int) 

    /**
     * Sets the value of field 'codMensagem'.
     * 
     * @param codMensagem the value of field 'codMensagem'.
     */
    public void setCodMensagem(java.lang.String codMensagem)
    {
        this._codMensagem = codMensagem;
    } //-- void setCodMensagem(java.lang.String) 

    /**
     * Sets the value of field 'mensagem'.
     * 
     * @param mensagem the value of field 'mensagem'.
     */
    public void setMensagem(java.lang.String mensagem)
    {
        this._mensagem = mensagem;
    } //-- void setMensagem(java.lang.String) 

    /**
     * Method setOcorrencia
     * 
     * 
     * 
     * @param index
     * @param vOcorrencia
     */
    public void setOcorrencia(int index, br.com.bradesco.web.pgit.service.data.pdc.imprimircomprovantetitulobradesco.response.Ocorrencia vOcorrencia)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index >= _ocorrenciaList.size())) {
            throw new IndexOutOfBoundsException("setOcorrencia: Index value '"+index+"' not in range [0.." + (_ocorrenciaList.size() - 1) + "]");
        }
        _ocorrenciaList.setElementAt(vOcorrencia, index);
    } //-- void setOcorrencia(int, br.com.bradesco.web.pgit.service.data.pdc.imprimircomprovantetitulobradesco.response.Ocorrencia) 

    /**
     * Method setOcorrencia
     * 
     * 
     * 
     * @param ocorrenciaArray
     */
    public void setOcorrencia(br.com.bradesco.web.pgit.service.data.pdc.imprimircomprovantetitulobradesco.response.Ocorrencia[] ocorrenciaArray)
    {
        //-- copy array
        _ocorrenciaList.removeAllElements();
        for (int i = 0; i < ocorrenciaArray.length; i++) {
            _ocorrenciaList.addElement(ocorrenciaArray[i]);
        }
    } //-- void setOcorrencia(br.com.bradesco.web.pgit.service.data.pdc.imprimircomprovantetitulobradesco.response.Ocorrencia) 

    /**
     * Sets the value of field 'quantidadeOcorrencia'.
     * 
     * @param quantidadeOcorrencia the value of field
     * 'quantidadeOcorrencia'.
     */
    public void setQuantidadeOcorrencia(int quantidadeOcorrencia)
    {
        this._quantidadeOcorrencia = quantidadeOcorrencia;
        this._has_quantidadeOcorrencia = true;
    } //-- void setQuantidadeOcorrencia(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ImprimirComprovanteTituloBradescoResponse
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.imprimircomprovantetitulobradesco.response.ImprimirComprovanteTituloBradescoResponse unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.imprimircomprovantetitulobradesco.response.ImprimirComprovanteTituloBradescoResponse) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.imprimircomprovantetitulobradesco.response.ImprimirComprovanteTituloBradescoResponse.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimircomprovantetitulobradesco.response.ImprimirComprovanteTituloBradescoResponse unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
