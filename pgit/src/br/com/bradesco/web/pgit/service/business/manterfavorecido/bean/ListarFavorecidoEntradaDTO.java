/*
 * Nome: br.com.bradesco.web.pgit.service.business.manterfavorecido.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.manterfavorecido.bean;

/**
 * Nome: ListarFavorecidoEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarFavorecidoEntradaDTO {
	
    /** Atributo cdPessoaJuridicaContrato. */
    private Long cdPessoaJuridicaContrato;
    
    /** Atributo cdTipoContratoNegocio. */
    private Integer cdTipoContratoNegocio;
    
    /** Atributo nrSequencialContratoNegocio. */
    private Long nrSequencialContratoNegocio;
    
    /** Atributo nmFavorecido. */
    private String nmFavorecido;
    
    /** Atributo cdFavorecidoClientePagador. */
    private Long cdFavorecidoClientePagador;
    
    /** Atributo cdTipoFavorecido. */
    private Integer cdTipoFavorecido;
    
    /** Atributo cdInscricaoFavorecidoCliente. */
    private Long cdInscricaoFavorecidoCliente;
    
    /** Atributo cdTipoInscricaoFavorecido. */
    private Integer cdTipoInscricaoFavorecido;
    
    /** Atributo cdSituacaoFavorecidoCliente. */
    private Integer cdSituacaoFavorecidoCliente;
    
    /** Atributo cdInscricaoFavorecidoFormatado. */
    private String cdInscricaoFavorecidoFormatado;
    
	/**
	 * Listar favorecido entrada dto.
	 */
	public ListarFavorecidoEntradaDTO() {
	}

	/**
	 * Get: cdFavorecidoClientePagador.
	 *
	 * @return cdFavorecidoClientePagador
	 */
	public Long getCdFavorecidoClientePagador() {
		return cdFavorecidoClientePagador;
	}
	
	/**
	 * Set: cdFavorecidoClientePagador.
	 *
	 * @param cdFavorecidoClientePagador the cd favorecido cliente pagador
	 */
	public void setCdFavorecidoClientePagador(Long cdFavorecidoClientePagador) {
		this.cdFavorecidoClientePagador = cdFavorecidoClientePagador;
	}
	
	/**
	 * Get: cdInscricaoFavorecidoCliente.
	 *
	 * @return cdInscricaoFavorecidoCliente
	 */
	public Long getCdInscricaoFavorecidoCliente() {
		return cdInscricaoFavorecidoCliente;
	}
	
	/**
	 * Set: cdInscricaoFavorecidoCliente.
	 *
	 * @param cdInscricaoFavorecidoCliente the cd inscricao favorecido cliente
	 */
	public void setCdInscricaoFavorecidoCliente(Long cdInscricaoFavorecidoCliente) {
		this.cdInscricaoFavorecidoCliente = cdInscricaoFavorecidoCliente;
	}
	
	/**
	 * Get: cdPessoaJuridicaContrato.
	 *
	 * @return cdPessoaJuridicaContrato
	 */
	public Long getCdPessoaJuridicaContrato() {
		return cdPessoaJuridicaContrato;
	}
	
	/**
	 * Set: cdPessoaJuridicaContrato.
	 *
	 * @param cdPessoaJuridicaContrato the cd pessoa juridica contrato
	 */
	public void setCdPessoaJuridicaContrato(Long cdPessoaJuridicaContrato) {
		this.cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
	}
	
	/**
	 * Get: cdSituacaoFavorecidoCliente.
	 *
	 * @return cdSituacaoFavorecidoCliente
	 */
	public Integer getCdSituacaoFavorecidoCliente() {
		return cdSituacaoFavorecidoCliente;
	}
	
	/**
	 * Set: cdSituacaoFavorecidoCliente.
	 *
	 * @param cdSituacaoFavorecidoCliente the cd situacao favorecido cliente
	 */
	public void setCdSituacaoFavorecidoCliente(Integer cdSituacaoFavorecidoCliente) {
		this.cdSituacaoFavorecidoCliente = cdSituacaoFavorecidoCliente;
	}
	
	/**
	 * Get: cdTipoContratoNegocio.
	 *
	 * @return cdTipoContratoNegocio
	 */
	public Integer getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}
	
	/**
	 * Set: cdTipoContratoNegocio.
	 *
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}
	
	/**
	 * Get: cdTipoFavorecido.
	 *
	 * @return cdTipoFavorecido
	 */
	public Integer getCdTipoFavorecido() {
		return cdTipoFavorecido;
	}
	
	/**
	 * Set: cdTipoFavorecido.
	 *
	 * @param cdTipoFavorecido the cd tipo favorecido
	 */
	public void setCdTipoFavorecido(Integer cdTipoFavorecido) {
		this.cdTipoFavorecido = cdTipoFavorecido;
	}
	
	/**
	 * Get: cdTipoInscricaoFavorecido.
	 *
	 * @return cdTipoInscricaoFavorecido
	 */
	public Integer getCdTipoInscricaoFavorecido() {
		return cdTipoInscricaoFavorecido;
	}
	
	/**
	 * Set: cdTipoInscricaoFavorecido.
	 *
	 * @param cdTipoInscricaoFavorecido the cd tipo inscricao favorecido
	 */
	public void setCdTipoInscricaoFavorecido(Integer cdTipoInscricaoFavorecido) {
		this.cdTipoInscricaoFavorecido = cdTipoInscricaoFavorecido;
	}
	
	/**
	 * Get: nmFavorecido.
	 *
	 * @return nmFavorecido
	 */
	public String getNmFavorecido() {
		return nmFavorecido;
	}
	
	/**
	 * Set: nmFavorecido.
	 *
	 * @param nmFavorecido the nm favorecido
	 */
	public void setNmFavorecido(String nmFavorecido) {
		this.nmFavorecido = nmFavorecido;
	}
	
	/**
	 * Get: nrSequencialContratoNegocio.
	 *
	 * @return nrSequencialContratoNegocio
	 */
	public Long getNrSequencialContratoNegocio() {
		return nrSequencialContratoNegocio;
	}
	
	/**
	 * Set: nrSequencialContratoNegocio.
	 *
	 * @param nrSequencialContratoNegocio the nr sequencial contrato negocio
	 */
	public void setNrSequencialContratoNegocio(Long nrSequencialContratoNegocio) {
		this.nrSequencialContratoNegocio = nrSequencialContratoNegocio;
	}

	/**
	 * Get: cdInscricaoFavorecidoFormatado.
	 *
	 * @return cdInscricaoFavorecidoFormatado
	 */
	public String getCdInscricaoFavorecidoFormatado() {
	    return cdInscricaoFavorecidoFormatado;
	}

	/**
	 * Set: cdInscricaoFavorecidoFormatado.
	 *
	 * @param cdInscricaoFavorecidoFormatado the cd inscricao favorecido formatado
	 */
	public void setCdInscricaoFavorecidoFormatado(String cdInscricaoFavorecidoFormatado) {
	    this.cdInscricaoFavorecidoFormatado = cdInscricaoFavorecidoFormatado;
	}
	
	
}
