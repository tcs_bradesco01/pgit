/*
 * Nome: br.com.bradesco.web.pgit.service.business.manterfavorecido.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.manterfavorecido.bean;

/**
 * Nome: ConsultarHistoricoFavorecidoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarHistoricoFavorecidoSaidaDTO {
	
	 /** Atributo hrInclusaoRegistro. */
 	private String hrInclusaoRegistro;
     
     /** Atributo dtData. */
     private String dtData;
     
     /** Atributo hrHora. */
     private String hrHora;
     
     /** Atributo dsUsuario. */
     private String dsUsuario;
     
          
	/**
	 * Consultar historico favorecido saida dto.
	 */
	public ConsultarHistoricoFavorecidoSaidaDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * Get: dsUsuario.
	 *
	 * @return dsUsuario
	 */
	public String getDsUsuario() {
		return dsUsuario;
	}
	
	/**
	 * Set: dsUsuario.
	 *
	 * @param dsUsuario the ds usuario
	 */
	public void setDsUsuario(String dsUsuario) {
		this.dsUsuario = dsUsuario;
	}
	
	/**
	 * Get: dtData.
	 *
	 * @return dtData
	 */
	public String getDtData() {
		return dtData;
	}
	
	/**
	 * Set: dtData.
	 *
	 * @param dtData the dt data
	 */
	public void setDtData(String dtData) {
		this.dtData = dtData;
	}
	
	/**
	 * Get: hrHora.
	 *
	 * @return hrHora
	 */
	public String getHrHora() {
		return hrHora;
	}
	
	/**
	 * Set: hrHora.
	 *
	 * @param hrHora the hr hora
	 */
	public void setHrHora(String hrHora) {
		this.hrHora = hrHora;
	}
	
	/**
	 * Get: hrInclusaoRegistro.
	 *
	 * @return hrInclusaoRegistro
	 */
	public String getHrInclusaoRegistro() {
		return hrInclusaoRegistro;
	}
	
	/**
	 * Set: hrInclusaoRegistro.
	 *
	 * @param hrInclusaoRegistro the hr inclusao registro
	 */
	public void setHrInclusaoRegistro(String hrInclusaoRegistro) {
		this.hrInclusaoRegistro = hrInclusaoRegistro;
	}

}
