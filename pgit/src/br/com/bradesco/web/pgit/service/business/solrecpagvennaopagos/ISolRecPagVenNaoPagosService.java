/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/interfaz.ftl,v $
 * $Id: interfaz.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: interfaz.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.solrecpagvennaopagos;

import java.util.List;

import br.com.bradesco.web.pgit.service.business.solrecpagvennaopagos.bean.ConsultarPagotsVencNaoPagosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.solrecpagvennaopagos.bean.ConsultarPagotsVencNaoPagosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.solrecpagvennaopagos.bean.SolicitarRecPagtosVencidosNaoPagosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.solrecpagvennaopagos.bean.SolicitarRecPagtosVencidosNaoPagosSaidaDTO;

/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Interface do adaptador: SolRecPagVenNaoPagos
 * </p>
 * 
 * @comment CODIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public interface ISolRecPagVenNaoPagosService {

	
	/**
	 * Consultar pagots venc nao pagos.
	 *
	 * @param entradaDTO the entrada dto
	 * @return the list< consultar pagots venc nao pagos saida dt o>
	 */
	List<ConsultarPagotsVencNaoPagosSaidaDTO> consultarPagotsVencNaoPagos(ConsultarPagotsVencNaoPagosEntradaDTO entradaDTO);
	
	/**
	 * Solicitar rec pagtos vencidos nao pagos.
	 *
	 * @param entradaDTO the entrada dto
	 * @return the solicitar rec pagtos vencidos nao pagos saida dto
	 */
	SolicitarRecPagtosVencidosNaoPagosSaidaDTO solicitarRecPagtosVencidosNaoPagos(SolicitarRecPagtosVencidosNaoPagosEntradaDTO entradaDTO);
}

