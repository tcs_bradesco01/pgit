/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultarlistatiporetornolayout.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdTipoArquivoRetorno
     */
    private int _cdTipoArquivoRetorno = 0;

    /**
     * keeps track of state for field: _cdTipoArquivoRetorno
     */
    private boolean _has_cdTipoArquivoRetorno;

    /**
     * Field _dsTipoArquivoRetorno
     */
    private java.lang.String _dsTipoArquivoRetorno;

    /**
     * Field _dtInclusaoRegistro
     */
    private java.lang.String _dtInclusaoRegistro;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarlistatiporetornolayout.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdTipoArquivoRetorno
     * 
     */
    public void deleteCdTipoArquivoRetorno()
    {
        this._has_cdTipoArquivoRetorno= false;
    } //-- void deleteCdTipoArquivoRetorno() 

    /**
     * Returns the value of field 'cdTipoArquivoRetorno'.
     * 
     * @return int
     * @return the value of field 'cdTipoArquivoRetorno'.
     */
    public int getCdTipoArquivoRetorno()
    {
        return this._cdTipoArquivoRetorno;
    } //-- int getCdTipoArquivoRetorno() 

    /**
     * Returns the value of field 'dsTipoArquivoRetorno'.
     * 
     * @return String
     * @return the value of field 'dsTipoArquivoRetorno'.
     */
    public java.lang.String getDsTipoArquivoRetorno()
    {
        return this._dsTipoArquivoRetorno;
    } //-- java.lang.String getDsTipoArquivoRetorno() 

    /**
     * Returns the value of field 'dtInclusaoRegistro'.
     * 
     * @return String
     * @return the value of field 'dtInclusaoRegistro'.
     */
    public java.lang.String getDtInclusaoRegistro()
    {
        return this._dtInclusaoRegistro;
    } //-- java.lang.String getDtInclusaoRegistro() 

    /**
     * Method hasCdTipoArquivoRetorno
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoArquivoRetorno()
    {
        return this._has_cdTipoArquivoRetorno;
    } //-- boolean hasCdTipoArquivoRetorno() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdTipoArquivoRetorno'.
     * 
     * @param cdTipoArquivoRetorno the value of field
     * 'cdTipoArquivoRetorno'.
     */
    public void setCdTipoArquivoRetorno(int cdTipoArquivoRetorno)
    {
        this._cdTipoArquivoRetorno = cdTipoArquivoRetorno;
        this._has_cdTipoArquivoRetorno = true;
    } //-- void setCdTipoArquivoRetorno(int) 

    /**
     * Sets the value of field 'dsTipoArquivoRetorno'.
     * 
     * @param dsTipoArquivoRetorno the value of field
     * 'dsTipoArquivoRetorno'.
     */
    public void setDsTipoArquivoRetorno(java.lang.String dsTipoArquivoRetorno)
    {
        this._dsTipoArquivoRetorno = dsTipoArquivoRetorno;
    } //-- void setDsTipoArquivoRetorno(java.lang.String) 

    /**
     * Sets the value of field 'dtInclusaoRegistro'.
     * 
     * @param dtInclusaoRegistro the value of field
     * 'dtInclusaoRegistro'.
     */
    public void setDtInclusaoRegistro(java.lang.String dtInclusaoRegistro)
    {
        this._dtInclusaoRegistro = dtInclusaoRegistro;
    } //-- void setDtInclusaoRegistro(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultarlistatiporetornolayout.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultarlistatiporetornolayout.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultarlistatiporetornolayout.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarlistatiporetornolayout.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
