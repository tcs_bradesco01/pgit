/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/interfaz.ftl,v $
 * $Id: interfaz.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: interfaz.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.ativarcontratopendenteassinatura;

import br.com.bradesco.web.pgit.service.business.ativarcontratopendenteassinatura.bean.AtivarContratoPendAssinaturaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.ativarcontratopendenteassinatura.bean.AtivarContratoPendAssinaturaSaidaDTO;

/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Interface do adaptador: AtivarContratoPendenteAssinatura
 * </p>
 * 
 * @comment CODIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public interface IAtivarContratoPendenteAssinaturaService {
	
	/**
	 * Ativar contrato pend assinatura.
	 *
	 * @param ativarContratoPendAssinaturaEntradaDTO the ativar contrato pend assinatura entrada dto
	 * @return the ativar contrato pend assinatura saida dto
	 */
	AtivarContratoPendAssinaturaSaidaDTO ativarContratoPendAssinatura(AtivarContratoPendAssinaturaEntradaDTO ativarContratoPendAssinaturaEntradaDTO);

}

