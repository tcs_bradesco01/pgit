/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.detalharpiramide.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias6.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias6 implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdCrenAtendimentoQuest
     */
    private int _cdCrenAtendimentoQuest = 0;

    /**
     * keeps track of state for field: _cdCrenAtendimentoQuest
     */
    private boolean _has_cdCrenAtendimentoQuest;

    /**
     * Field _dsCrenAtendimentoQuest
     */
    private java.lang.String _dsCrenAtendimentoQuest;

    /**
     * Field _qtdCrenQuest
     */
    private long _qtdCrenQuest = 0;

    /**
     * keeps track of state for field: _qtdCrenQuest
     */
    private boolean _has_qtdCrenQuest;

    /**
     * Field _qtdFuncCrenQuest
     */
    private long _qtdFuncCrenQuest = 0;

    /**
     * keeps track of state for field: _qtdFuncCrenQuest
     */
    private boolean _has_qtdFuncCrenQuest;

    /**
     * Field _cdUnidadeMedAgQuest
     */
    private int _cdUnidadeMedAgQuest = 0;

    /**
     * keeps track of state for field: _cdUnidadeMedAgQuest
     */
    private boolean _has_cdUnidadeMedAgQuest;

    /**
     * Field _cdDistcCrenQuest
     */
    private int _cdDistcCrenQuest = 0;

    /**
     * keeps track of state for field: _cdDistcCrenQuest
     */
    private boolean _has_cdDistcCrenQuest;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias6() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharpiramide.response.Ocorrencias6()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdCrenAtendimentoQuest
     * 
     */
    public void deleteCdCrenAtendimentoQuest()
    {
        this._has_cdCrenAtendimentoQuest= false;
    } //-- void deleteCdCrenAtendimentoQuest() 

    /**
     * Method deleteCdDistcCrenQuest
     * 
     */
    public void deleteCdDistcCrenQuest()
    {
        this._has_cdDistcCrenQuest= false;
    } //-- void deleteCdDistcCrenQuest() 

    /**
     * Method deleteCdUnidadeMedAgQuest
     * 
     */
    public void deleteCdUnidadeMedAgQuest()
    {
        this._has_cdUnidadeMedAgQuest= false;
    } //-- void deleteCdUnidadeMedAgQuest() 

    /**
     * Method deleteQtdCrenQuest
     * 
     */
    public void deleteQtdCrenQuest()
    {
        this._has_qtdCrenQuest= false;
    } //-- void deleteQtdCrenQuest() 

    /**
     * Method deleteQtdFuncCrenQuest
     * 
     */
    public void deleteQtdFuncCrenQuest()
    {
        this._has_qtdFuncCrenQuest= false;
    } //-- void deleteQtdFuncCrenQuest() 

    /**
     * Returns the value of field 'cdCrenAtendimentoQuest'.
     * 
     * @return int
     * @return the value of field 'cdCrenAtendimentoQuest'.
     */
    public int getCdCrenAtendimentoQuest()
    {
        return this._cdCrenAtendimentoQuest;
    } //-- int getCdCrenAtendimentoQuest() 

    /**
     * Returns the value of field 'cdDistcCrenQuest'.
     * 
     * @return int
     * @return the value of field 'cdDistcCrenQuest'.
     */
    public int getCdDistcCrenQuest()
    {
        return this._cdDistcCrenQuest;
    } //-- int getCdDistcCrenQuest() 

    /**
     * Returns the value of field 'cdUnidadeMedAgQuest'.
     * 
     * @return int
     * @return the value of field 'cdUnidadeMedAgQuest'.
     */
    public int getCdUnidadeMedAgQuest()
    {
        return this._cdUnidadeMedAgQuest;
    } //-- int getCdUnidadeMedAgQuest() 

    /**
     * Returns the value of field 'dsCrenAtendimentoQuest'.
     * 
     * @return String
     * @return the value of field 'dsCrenAtendimentoQuest'.
     */
    public java.lang.String getDsCrenAtendimentoQuest()
    {
        return this._dsCrenAtendimentoQuest;
    } //-- java.lang.String getDsCrenAtendimentoQuest() 

    /**
     * Returns the value of field 'qtdCrenQuest'.
     * 
     * @return long
     * @return the value of field 'qtdCrenQuest'.
     */
    public long getQtdCrenQuest()
    {
        return this._qtdCrenQuest;
    } //-- long getQtdCrenQuest() 

    /**
     * Returns the value of field 'qtdFuncCrenQuest'.
     * 
     * @return long
     * @return the value of field 'qtdFuncCrenQuest'.
     */
    public long getQtdFuncCrenQuest()
    {
        return this._qtdFuncCrenQuest;
    } //-- long getQtdFuncCrenQuest() 

    /**
     * Method hasCdCrenAtendimentoQuest
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCrenAtendimentoQuest()
    {
        return this._has_cdCrenAtendimentoQuest;
    } //-- boolean hasCdCrenAtendimentoQuest() 

    /**
     * Method hasCdDistcCrenQuest
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdDistcCrenQuest()
    {
        return this._has_cdDistcCrenQuest;
    } //-- boolean hasCdDistcCrenQuest() 

    /**
     * Method hasCdUnidadeMedAgQuest
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdUnidadeMedAgQuest()
    {
        return this._has_cdUnidadeMedAgQuest;
    } //-- boolean hasCdUnidadeMedAgQuest() 

    /**
     * Method hasQtdCrenQuest
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtdCrenQuest()
    {
        return this._has_qtdCrenQuest;
    } //-- boolean hasQtdCrenQuest() 

    /**
     * Method hasQtdFuncCrenQuest
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtdFuncCrenQuest()
    {
        return this._has_qtdFuncCrenQuest;
    } //-- boolean hasQtdFuncCrenQuest() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdCrenAtendimentoQuest'.
     * 
     * @param cdCrenAtendimentoQuest the value of field
     * 'cdCrenAtendimentoQuest'.
     */
    public void setCdCrenAtendimentoQuest(int cdCrenAtendimentoQuest)
    {
        this._cdCrenAtendimentoQuest = cdCrenAtendimentoQuest;
        this._has_cdCrenAtendimentoQuest = true;
    } //-- void setCdCrenAtendimentoQuest(int) 

    /**
     * Sets the value of field 'cdDistcCrenQuest'.
     * 
     * @param cdDistcCrenQuest the value of field 'cdDistcCrenQuest'
     */
    public void setCdDistcCrenQuest(int cdDistcCrenQuest)
    {
        this._cdDistcCrenQuest = cdDistcCrenQuest;
        this._has_cdDistcCrenQuest = true;
    } //-- void setCdDistcCrenQuest(int) 

    /**
     * Sets the value of field 'cdUnidadeMedAgQuest'.
     * 
     * @param cdUnidadeMedAgQuest the value of field
     * 'cdUnidadeMedAgQuest'.
     */
    public void setCdUnidadeMedAgQuest(int cdUnidadeMedAgQuest)
    {
        this._cdUnidadeMedAgQuest = cdUnidadeMedAgQuest;
        this._has_cdUnidadeMedAgQuest = true;
    } //-- void setCdUnidadeMedAgQuest(int) 

    /**
     * Sets the value of field 'dsCrenAtendimentoQuest'.
     * 
     * @param dsCrenAtendimentoQuest the value of field
     * 'dsCrenAtendimentoQuest'.
     */
    public void setDsCrenAtendimentoQuest(java.lang.String dsCrenAtendimentoQuest)
    {
        this._dsCrenAtendimentoQuest = dsCrenAtendimentoQuest;
    } //-- void setDsCrenAtendimentoQuest(java.lang.String) 

    /**
     * Sets the value of field 'qtdCrenQuest'.
     * 
     * @param qtdCrenQuest the value of field 'qtdCrenQuest'.
     */
    public void setQtdCrenQuest(long qtdCrenQuest)
    {
        this._qtdCrenQuest = qtdCrenQuest;
        this._has_qtdCrenQuest = true;
    } //-- void setQtdCrenQuest(long) 

    /**
     * Sets the value of field 'qtdFuncCrenQuest'.
     * 
     * @param qtdFuncCrenQuest the value of field 'qtdFuncCrenQuest'
     */
    public void setQtdFuncCrenQuest(long qtdFuncCrenQuest)
    {
        this._qtdFuncCrenQuest = qtdFuncCrenQuest;
        this._has_qtdFuncCrenQuest = true;
    } //-- void setQtdFuncCrenQuest(long) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias6
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.detalharpiramide.response.Ocorrencias6 unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.detalharpiramide.response.Ocorrencias6) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.detalharpiramide.response.Ocorrencias6.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharpiramide.response.Ocorrencias6 unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
