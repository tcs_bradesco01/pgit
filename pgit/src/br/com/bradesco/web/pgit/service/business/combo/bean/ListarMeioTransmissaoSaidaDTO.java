/*
 * Nome: br.com.bradesco.web.pgit.service.business.combo.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.combo.bean;

/**
 * Nome: ListarMeioTransmissaoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarMeioTransmissaoSaidaDTO {
    
    /** Atributo codMensagem. */
    private String codMensagem;
    
    /** Atributo mensagem. */
    private String mensagem;
    
    /** Atributo cdTipoMeioTransmissao. */
    private Integer cdTipoMeioTransmissao;
    
    /** Atributo dsTipoMeioTransmissao. */
    private String dsTipoMeioTransmissao;
    
	/**
	 * Listar meio transmissao saida dto.
	 *
	 * @param cdTipoMeioTransmissao the cd tipo meio transmissao
	 * @param dsTipoMeioTransmissao the ds tipo meio transmissao
	 */
	public ListarMeioTransmissaoSaidaDTO(Integer cdTipoMeioTransmissao, String dsTipoMeioTransmissao) {
		super();
		this.cdTipoMeioTransmissao = cdTipoMeioTransmissao;
		this.dsTipoMeioTransmissao = dsTipoMeioTransmissao;
	}
	
	/**
	 * Get: cdTipoMeioTransmissao.
	 *
	 * @return cdTipoMeioTransmissao
	 */
	public Integer getCdTipoMeioTransmissao() {
		return cdTipoMeioTransmissao;
	}
	
	/**
	 * Set: cdTipoMeioTransmissao.
	 *
	 * @param cdTipoMeioTransmissao the cd tipo meio transmissao
	 */
	public void setCdTipoMeioTransmissao(Integer cdTipoMeioTransmissao) {
		this.cdTipoMeioTransmissao = cdTipoMeioTransmissao;
	}
	
	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}
	
	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}
	
	/**
	 * Get: dsTipoMeioTransmissao.
	 *
	 * @return dsTipoMeioTransmissao
	 */
	public String getDsTipoMeioTransmissao() {
		return dsTipoMeioTransmissao;
	}
	
	/**
	 * Set: dsTipoMeioTransmissao.
	 *
	 * @param dsTipoMeioTransmissao the ds tipo meio transmissao
	 */
	public void setDsTipoMeioTransmissao(String dsTipoMeioTransmissao) {
		this.dsTipoMeioTransmissao = dsTipoMeioTransmissao;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
    
    
}
