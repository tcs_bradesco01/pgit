package br.com.bradesco.web.pgit.service.business.assretlayarqproser.bean;

public class ConsultarAssVersaoLayoutLoteServicoEntradaDTO {
	
    private Integer cdTipoLayoutArquivo;

    private Integer nrVersaoLayoutArquivo;

    private Integer cdTipoLoteLayout;

    private Integer nrVersaoLoteLayout;

    private Integer cdTipoServicoCnab;

	public Integer getCdTipoLayoutArquivo() {
		return cdTipoLayoutArquivo;
	}

	public void setCdTipoLayoutArquivo(Integer cdTipoLayoutArquivo) {
		this.cdTipoLayoutArquivo = cdTipoLayoutArquivo;
	}

	public Integer getNrVersaoLayoutArquivo() {
		return nrVersaoLayoutArquivo;
	}

	public void setNrVersaoLayoutArquivo(Integer nrVersaoLayoutArquivo) {
		this.nrVersaoLayoutArquivo = nrVersaoLayoutArquivo;
	}

	public Integer getCdTipoLoteLayout() {
		return cdTipoLoteLayout;
	}

	public void setCdTipoLoteLayout(Integer cdTipoLoteLayout) {
		this.cdTipoLoteLayout = cdTipoLoteLayout;
	}

	public Integer getNrVersaoLoteLayout() {
		return nrVersaoLoteLayout;
	}

	public void setNrVersaoLoteLayout(Integer nrVersaoLoteLayout) {
		this.nrVersaoLoteLayout = nrVersaoLoteLayout;
	}

	public Integer getCdTipoServicoCnab() {
		return cdTipoServicoCnab;
	}

	public void setCdTipoServicoCnab(Integer cdTipoServicoCnab) {
		this.cdTipoServicoCnab = cdTipoServicoCnab;
	}

}
