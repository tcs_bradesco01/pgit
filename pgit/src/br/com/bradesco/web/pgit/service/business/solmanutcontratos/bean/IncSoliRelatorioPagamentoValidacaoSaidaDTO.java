/*
 * Nome: br.com.bradesco.web.pgit.service.business.solmanutcontratos.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.solmanutcontratos.bean;

/**
 * Nome: IncSoliRelatorioPagamentoValidacaoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class IncSoliRelatorioPagamentoValidacaoSaidaDTO {
	
	 /** Atributo codMensagem. */
 	private String codMensagem;
     
     /** Atributo mensagem. */
     private String mensagem;
     
     /** Atributo cdPessoaJuridicaDebito. */
     private Long cdPessoaJuridicaDebito;
     
     /** Atributo cdTipoContratoDebito. */
     private Integer cdTipoContratoDebito;
     
     /** Atributo nrSequenciaContratoDebito. */
     private Long nrSequenciaContratoDebito;
     
     /** Atributo cdAgenciaDebito. */
     private Integer cdAgenciaDebito;
     
     /** Atributo dsNomeAgenciaDebito. */
     private String dsNomeAgenciaDebito;
     
     /** Atributo cdContaDebito. */
     private Long cdContaDebito;
     
     /** Atributo cdDigitoContaDebito. */
     private String cdDigitoContaDebito;
     
     /** Atributo contaDigito. */
     private String contaDigito;
     
     /** Atributo codAgenciaDesc. */
     private String codAgenciaDesc;
     
	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}
	
	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	/**
	 * Get: cdPessoaJuridicaDebito.
	 *
	 * @return cdPessoaJuridicaDebito
	 */
	public Long getCdPessoaJuridicaDebito() {
		return cdPessoaJuridicaDebito;
	}
	
	/**
	 * Set: cdPessoaJuridicaDebito.
	 *
	 * @param cdPessoaJuridicaDebito the cd pessoa juridica debito
	 */
	public void setCdPessoaJuridicaDebito(Long cdPessoaJuridicaDebito) {
		this.cdPessoaJuridicaDebito = cdPessoaJuridicaDebito;
	}
	
	/**
	 * Get: cdTipoContratoDebito.
	 *
	 * @return cdTipoContratoDebito
	 */
	public Integer getCdTipoContratoDebito() {
		return cdTipoContratoDebito;
	}
	
	/**
	 * Set: cdTipoContratoDebito.
	 *
	 * @param cdTipoContratoDebito the cd tipo contrato debito
	 */
	public void setCdTipoContratoDebito(Integer cdTipoContratoDebito) {
		this.cdTipoContratoDebito = cdTipoContratoDebito;
	}
	
	/**
	 * Get: nrSequenciaContratoDebito.
	 *
	 * @return nrSequenciaContratoDebito
	 */
	public Long getNrSequenciaContratoDebito() {
		return nrSequenciaContratoDebito;
	}
	
	/**
	 * Set: nrSequenciaContratoDebito.
	 *
	 * @param nrSequenciaContratoDebito the nr sequencia contrato debito
	 */
	public void setNrSequenciaContratoDebito(Long nrSequenciaContratoDebito) {
		this.nrSequenciaContratoDebito = nrSequenciaContratoDebito;
	}
	
	/**
	 * Get: cdAgenciaDebito.
	 *
	 * @return cdAgenciaDebito
	 */
	public Integer getCdAgenciaDebito() {
		return cdAgenciaDebito;
	}
	
	/**
	 * Set: cdAgenciaDebito.
	 *
	 * @param cdAgenciaDebito the cd agencia debito
	 */
	public void setCdAgenciaDebito(Integer cdAgenciaDebito) {
		this.cdAgenciaDebito = cdAgenciaDebito;
	}
	
	/**
	 * Get: dsNomeAgenciaDebito.
	 *
	 * @return dsNomeAgenciaDebito
	 */
	public String getDsNomeAgenciaDebito() {
		return dsNomeAgenciaDebito;
	}
	
	/**
	 * Set: dsNomeAgenciaDebito.
	 *
	 * @param dsNomeAgenciaDebito the ds nome agencia debito
	 */
	public void setDsNomeAgenciaDebito(String dsNomeAgenciaDebito) {
		this.dsNomeAgenciaDebito = dsNomeAgenciaDebito;
	}
	
	/**
	 * Get: cdContaDebito.
	 *
	 * @return cdContaDebito
	 */
	public Long getCdContaDebito() {
		return cdContaDebito;
	}
	
	/**
	 * Set: cdContaDebito.
	 *
	 * @param cdContaDebito the cd conta debito
	 */
	public void setCdContaDebito(Long cdContaDebito) {
		this.cdContaDebito = cdContaDebito;
	}
	
	/**
	 * Get: cdDigitoContaDebito.
	 *
	 * @return cdDigitoContaDebito
	 */
	public String getCdDigitoContaDebito() {
		return cdDigitoContaDebito;
	}
	
	/**
	 * Set: cdDigitoContaDebito.
	 *
	 * @param cdDigitoContaDebito the cd digito conta debito
	 */
	public void setCdDigitoContaDebito(String cdDigitoContaDebito) {
		this.cdDigitoContaDebito = cdDigitoContaDebito;
	}
	
	/**
	 * Get: contaDigito.
	 *
	 * @return contaDigito
	 */
	public String getContaDigito() {
		return contaDigito;
	}
	
	/**
	 * Set: contaDigito.
	 *
	 * @param contaDigito the conta digito
	 */
	public void setContaDigito(String contaDigito) {
		this.contaDigito = contaDigito;
	}
	
	/**
	 * Get: codAgenciaDesc.
	 *
	 * @return codAgenciaDesc
	 */
	public String getCodAgenciaDesc() {
		return codAgenciaDesc;
	}
	
	/**
	 * Set: codAgenciaDesc.
	 *
	 * @param codAgenciaDesc the cod agencia desc
	 */
	public void setCodAgenciaDesc(String codAgenciaDesc) {
		this.codAgenciaDesc = codAgenciaDesc;
	}

}
