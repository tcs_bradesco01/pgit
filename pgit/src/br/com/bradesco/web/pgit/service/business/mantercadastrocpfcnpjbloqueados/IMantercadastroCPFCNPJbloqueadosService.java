/**
 * Nome: br.com.bradesco.web.pgit.service.business.mantercadastrocpfcnpjbloqueados
 * Compilador: JDK 1.5
 * Prop�sito: INSERIR O PROP�SITO DAS CLASSES DO PACOTE
 * Data da cria��o: <dd/MM/yyyy>
 * Par�metros de compila��o: -d
 */
package br.com.bradesco.web.pgit.service.business.mantercadastrocpfcnpjbloqueados;

import java.util.List;

import br.com.bradesco.web.pgit.service.business.geral.MensagemSaida;
import br.com.bradesco.web.pgit.service.business.mantercadastrocpfcnpjbloqueados.bean.ConsultarBloqueioRastreamentoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadastrocpfcnpjbloqueados.bean.ConsultarBloqueioRastreamentoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadastrocpfcnpjbloqueados.bean.DetalharBloqueioRastreamentoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadastrocpfcnpjbloqueados.bean.DetalharBloqueioRastreamentoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadastrocpfcnpjbloqueados.bean.DetalharCnpjFicticioEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadastrocpfcnpjbloqueados.bean.DetalharCnpjFicticioSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadastrocpfcnpjbloqueados.bean.ExcluirBloqueioRastreamentoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadastrocpfcnpjbloqueados.bean.ExcluirBloqueioRastreamentoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadastrocpfcnpjbloqueados.bean.ExcluirCnpjFicticioEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadastrocpfcnpjbloqueados.bean.IncluirBloqueioRastreamentoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadastrocpfcnpjbloqueados.bean.IncluirBloqueioRastreamentoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadastrocpfcnpjbloqueados.bean.IncluirCnpjFicticioEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadastrocpfcnpjbloqueados.bean.ListarCnpjFicticioEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadastrocpfcnpjbloqueados.bean.ListarCnpjFicticioSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadastrocpfcnpjbloqueados.bean.ValidarCnpjFicticioEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadastrocpfcnpjbloqueados.bean.ValidarCnpjFicticioSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadastrocpfcnpjbloqueados.bean.ValidarIncluirBloqueioRastreamentoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadastrocpfcnpjbloqueados.bean.ValidarIncluirBloqueioRastreamentoSaidaDTO;

/**
 * Nome: IMantercadastroCPFCNPJbloqueadosService
 * <p>
 * Prop�sito: Interface do adaptador MantercadastroCPFCNPJbloqueados
 * </p>
 * 
 * @author CPM Braxis / TI Melhorias - Arquitetura
 * @version 1.0
 */
public interface IMantercadastroCPFCNPJbloqueadosService {

    /**
     * Pesquisar cpfcnpj bloqueados.
     *
     * @param entrada the entrada
     * @return the list< consultar bloqueio rastreamento saida dt o>
     */
    List<ConsultarBloqueioRastreamentoSaidaDTO> pesquisarCPFCNPJBloqueados(ConsultarBloqueioRastreamentoEntradaDTO entrada);
    
    /**
     * Incluir bloqueio rastreamento.
     *
     * @param entrada the entrada
     * @return the incluir bloqueio rastreamento saida dto
     */
    IncluirBloqueioRastreamentoSaidaDTO incluirBloqueioRastreamento(IncluirBloqueioRastreamentoEntradaDTO entrada);
    
    /**
     * Validar bloqueio rastreamento.
     *
     * @param entrada the entrada
     * @return the validar incluir bloqueio rastreamento saida dto
     */
    ValidarIncluirBloqueioRastreamentoSaidaDTO validarBloqueioRastreamento(ValidarIncluirBloqueioRastreamentoEntradaDTO entrada);
    
    /**
     * Excluir bloqueio rastreamento.
     *
     * @param entrada the entrada
     * @return the excluir bloqueio rastreamento saida dto
     */
    ExcluirBloqueioRastreamentoSaidaDTO excluirBloqueioRastreamento(ExcluirBloqueioRastreamentoEntradaDTO entrada);
    
    /**
     * Detalhar bloqueio rastreamento.
     *
     * @param entrada the entrada
     * @return the detalhar bloqueio rastreamento saida dto
     */
    DetalharBloqueioRastreamentoSaidaDTO detalharBloqueioRastreamento(DetalharBloqueioRastreamentoEntradaDTO entrada);

    /**
     * Listar cnpj ficticio.
     *
     * @param entrada the entrada
     * @return the listar cnpj ficticio saida dto
     */
    ListarCnpjFicticioSaidaDTO listarCnpjFicticio(ListarCnpjFicticioEntradaDTO entrada);  
    
    /**
     * Detalhar cnpj ficticio.
     *
     * @param entrada the entrada
     * @return the detalhar cnpj ficticio saida dto
     */
    DetalharCnpjFicticioSaidaDTO detalharCnpjFicticio(DetalharCnpjFicticioEntradaDTO entrada); 
    
    /**
     * Excluir cnpj ficticio.
     *
     * @param entrada the entrada
     * @return the mensagem saida
     */
    MensagemSaida excluirCnpjFicticio(ExcluirCnpjFicticioEntradaDTO entrada);
    
    /**
     * Validar cnpj ficticio.
     *
     * @param entrada the entrada
     * @return the validar cnpj ficticio saida dto
     */
    ValidarCnpjFicticioSaidaDTO validarCnpjFicticio(ValidarCnpjFicticioEntradaDTO entrada);  
    
    /**
     * Incluir cnpj ficticio.
     *
     * @param entrada the entrada
     * @return the mensagem saida
     */
    MensagemSaida IncluirCnpjFicticio(IncluirCnpjFicticioEntradaDTO entrada);

}