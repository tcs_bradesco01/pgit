/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantercontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantercontrato.bean;

/**
 * Nome: ListarConManParticipanteEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarConManParticipanteEntradaDTO {

	
	/** Atributo cdPessoaJuridicaNegocio. */
	private long cdPessoaJuridicaNegocio;
	
	/** Atributo cdTipoContratoNegocio. */
	private int cdTipoContratoNegocio;
	
	/** Atributo nrSequenciaContratoNegocio. */
	private int nrSequenciaContratoNegocio;
	
	/** Atributo cdPessoa. */
	private long cdPessoa;
	
	/** Atributo cdTipoParticipacaoPessoa. */
	private int cdTipoParticipacaoPessoa;
	
	/** Atributo dtInicio. */
	private String dtInicio;
	
	/** Atributo dtFim. */
	private String dtFim;
	
	/**
	 * Get: cdPessoa.
	 *
	 * @return cdPessoa
	 */
	public long getCdPessoa() {
		return cdPessoa;
	}
	
	/**
	 * Set: cdPessoa.
	 *
	 * @param cdPessoa the cd pessoa
	 */
	public void setCdPessoa(long cdPessoa) {
		this.cdPessoa = cdPessoa;
	}
	
	/**
	 * Get: cdPessoaJuridicaNegocio.
	 *
	 * @return cdPessoaJuridicaNegocio
	 */
	public long getCdPessoaJuridicaNegocio() {
		return cdPessoaJuridicaNegocio;
	}
	
	/**
	 * Set: cdPessoaJuridicaNegocio.
	 *
	 * @param cdPessoaJuridicaNegocio the cd pessoa juridica negocio
	 */
	public void setCdPessoaJuridicaNegocio(long cdPessoaJuridicaNegocio) {
		this.cdPessoaJuridicaNegocio = cdPessoaJuridicaNegocio;
	}
	
	/**
	 * Get: cdTipoContratoNegocio.
	 *
	 * @return cdTipoContratoNegocio
	 */
	public int getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}
	
	/**
	 * Set: cdTipoContratoNegocio.
	 *
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(int cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}
	
	/**
	 * Get: cdTipoParticipacaoPessoa.
	 *
	 * @return cdTipoParticipacaoPessoa
	 */
	public int getCdTipoParticipacaoPessoa() {
		return cdTipoParticipacaoPessoa;
	}
	
	/**
	 * Set: cdTipoParticipacaoPessoa.
	 *
	 * @param cdTipoParticipacaoPessoa the cd tipo participacao pessoa
	 */
	public void setCdTipoParticipacaoPessoa(int cdTipoParticipacaoPessoa) {
		this.cdTipoParticipacaoPessoa = cdTipoParticipacaoPessoa;
	}
	
	/**
	 * Get: dtFim.
	 *
	 * @return dtFim
	 */
	public String getDtFim() {
		return dtFim;
	}
	
	/**
	 * Set: dtFim.
	 *
	 * @param dtFim the dt fim
	 */
	public void setDtFim(String dtFim) {
		this.dtFim = dtFim;
	}
	
	/**
	 * Get: dtInicio.
	 *
	 * @return dtInicio
	 */
	public String getDtInicio() {
		return dtInicio;
	}
	
	/**
	 * Set: dtInicio.
	 *
	 * @param dtInicio the dt inicio
	 */
	public void setDtInicio(String dtInicio) {
		this.dtInicio = dtInicio;
	}
	
	/**
	 * Get: nrSequenciaContratoNegocio.
	 *
	 * @return nrSequenciaContratoNegocio
	 */
	public int getNrSequenciaContratoNegocio() {
		return nrSequenciaContratoNegocio;
	}
	
	/**
	 * Set: nrSequenciaContratoNegocio.
	 *
	 * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
	 */
	public void setNrSequenciaContratoNegocio(int nrSequenciaContratoNegocio) {
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}
	
	
	
}
