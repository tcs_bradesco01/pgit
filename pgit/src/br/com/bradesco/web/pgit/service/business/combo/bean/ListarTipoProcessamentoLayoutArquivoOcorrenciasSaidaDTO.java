package br.com.bradesco.web.pgit.service.business.combo.bean;


/**
 * Arquivo criado em 06/11/15.
 */
public class ListarTipoProcessamentoLayoutArquivoOcorrenciasSaidaDTO {

    private Integer cdCombo;

    private String dsCombo;

    public void setCdCombo(Integer cdCombo) {
        this.cdCombo = cdCombo;
    }

    public Integer getCdCombo() {
        return this.cdCombo;
    }

    public void setDsCombo(String dsCombo) {
        this.dsCombo = dsCombo;
    }

    public String getDsCombo() {
        return this.dsCombo;
    }
}