/*
 * Nome: br.com.bradesco.web.pgit.service.business.lanctopersonalizado.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.lanctopersonalizado.bean;

/**
 * Nome: DetalharHistoricoLanctoPersonalizadoEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class DetalharHistoricoLanctoPersonalizadoEntradaDTO {
	
	/** Atributo codLancamentoPersonalizado. */
	private Integer codLancamentoPersonalizado;
	
	/** Atributo dataInicial. */
	private String dataInicial;
	
	/** Atributo dataFinal. */
	private String dataFinal;
	
	/** Atributo dataManutencao. */
	private String dataManutencao;
	
	/**
	 * Get: codLancamentoPersonalizado.
	 *
	 * @return codLancamentoPersonalizado
	 */
	public Integer getCodLancamentoPersonalizado() {
		return codLancamentoPersonalizado;
	}
	
	/**
	 * Set: codLancamentoPersonalizado.
	 *
	 * @param codLancamentoPersonalizado the cod lancamento personalizado
	 */
	public void setCodLancamentoPersonalizado(Integer codLancamentoPersonalizado) {
		this.codLancamentoPersonalizado = codLancamentoPersonalizado;
	}
	
	/**
	 * Get: dataFinal.
	 *
	 * @return dataFinal
	 */
	public String getDataFinal() {
		return dataFinal;
	}
	
	/**
	 * Set: dataFinal.
	 *
	 * @param dataFinal the data final
	 */
	public void setDataFinal(String dataFinal) {
		this.dataFinal = dataFinal;
	}
	
	/**
	 * Get: dataInicial.
	 *
	 * @return dataInicial
	 */
	public String getDataInicial() {
		return dataInicial;
	}
	
	/**
	 * Set: dataInicial.
	 *
	 * @param dataInicial the data inicial
	 */
	public void setDataInicial(String dataInicial) {
		this.dataInicial = dataInicial;
	}
	
	/**
	 * Get: dataManutencao.
	 *
	 * @return dataManutencao
	 */
	public String getDataManutencao() {
		return dataManutencao;
	}
	
	/**
	 * Set: dataManutencao.
	 *
	 * @param dataManutencao the data manutencao
	 */
	public void setDataManutencao(String dataManutencao) {
		this.dataManutencao = dataManutencao;
	}

}
