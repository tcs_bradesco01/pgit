/*
 * Nome: br.com.bradesco.web.pgit.service.business.arquivoretorno.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.arquivoretorno.bean;

/**
 * Nome: LoteRetornoDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class LoteRetornoDTO {

	/** Atributo seqLote. */
	private String seqLote;
	
	/** Atributo bcoAgeCtaDebito. */
	private String bcoAgeCtaDebito;
	
	/** Atributo modalidadePagto. */
	private String modalidadePagto;
	
	/** Atributo qtdeRegistros. */
	private String qtdeRegistros;
	
	/** Atributo valorRegistros. */
	private String valorRegistros;
	
	/** Atributo sequencia. */
	private int sequencia;
	
	/** Atributo codigoSistemaLegado. */
	private String codigoSistemaLegado;
	
	/** Atributo codigoCpfCnpj. */
	private String codigoCpfCnpj;
	
	/** Atributo codigoFilialCpfCnpj. */
	private String codigoFilialCpfCnpj;
	
	/** Atributo controleCpfCnpj. */
	private String controleCpfCnpj;
	
	/** Atributo codigoPerfil. */
	private String codigoPerfil;
	
	/** Atributo numeroArquivoRemessaPagamento. */
	private String numeroArquivoRemessaPagamento;
	
	/** Atributo numeroLoteArquivoRetorno. */
	private String numeroLoteArquivoRetorno;
	
	/** Atributo numeroLoteRetornoLong. */
	private Long numeroLoteRetornoLong;
	
	/** Atributo numeroSequenciaRegistroRemessa. */
	private String numeroSequenciaRegistroRemessa;
	
	/** Atributo horaInclusao. */
	private String horaInclusao;
	
	/** Atributo numeroArquivoRetorno. */
	private Long numeroArquivoRetorno;
	
	/** Atributo valorInc. */
	private Double valorInc;
	
	/** Atributo valorCon. */
	private Double valorCon;
	
	/** Atributo valorTotal. */
	private Double valorTotal;
	
	/** Atributo quantidadeInc. */
	private Long quantidadeInc;
	
	/** Atributo quantidadeCon. */
	private Long quantidadeCon;
	
	/** Atributo quantidadeTotal. */
	private Long quantidadeTotal;
	
	/**
	 * Get: bcoAgeCtaDebito.
	 *
	 * @return bcoAgeCtaDebito
	 */
	public String getBcoAgeCtaDebito() {
		return bcoAgeCtaDebito;
	}
	
	/**
	 * Set: bcoAgeCtaDebito.
	 *
	 * @param bcoAgeCtaDebito the bco age cta debito
	 */
	public void setBcoAgeCtaDebito(String bcoAgeCtaDebito) {
		this.bcoAgeCtaDebito = bcoAgeCtaDebito;
	}
	
	/**
	 * Get: modalidadePagto.
	 *
	 * @return modalidadePagto
	 */
	public String getModalidadePagto() {
		return modalidadePagto;
	}
	
	/**
	 * Set: modalidadePagto.
	 *
	 * @param modalidadePagto the modalidade pagto
	 */
	public void setModalidadePagto(String modalidadePagto) {
		this.modalidadePagto = modalidadePagto;
	}
	
	/**
	 * Get: qtdeRegistros.
	 *
	 * @return qtdeRegistros
	 */
	public String getQtdeRegistros() {
		return qtdeRegistros;
	}
	
	/**
	 * Set: qtdeRegistros.
	 *
	 * @param qtdeRegistros the qtde registros
	 */
	public void setQtdeRegistros(String qtdeRegistros) {
		this.qtdeRegistros = qtdeRegistros;
	}
	
	/**
	 * Get: seqLote.
	 *
	 * @return seqLote
	 */
	public String getSeqLote() {
		return seqLote;
	}
	
	/**
	 * Set: seqLote.
	 *
	 * @param seqLote the seq lote
	 */
	public void setSeqLote(String seqLote) {
		this.seqLote = seqLote;
	}
	
	/**
	 * Get: valorRegistros.
	 *
	 * @return valorRegistros
	 */
	public String getValorRegistros() {
		return valorRegistros;
	}
	
	/**
	 * Set: valorRegistros.
	 *
	 * @param valorRegistros the valor registros
	 */
	public void setValorRegistros(String valorRegistros) {
		this.valorRegistros = valorRegistros;
	}
	
	/**
	 * Get: sequencia.
	 *
	 * @return sequencia
	 */
	public int getSequencia() {
		return sequencia;
	}
	
	/**
	 * Set: sequencia.
	 *
	 * @param sequencia the sequencia
	 */
	public void setSequencia(int sequencia) {
		this.sequencia = sequencia;
	}
	
	/**
	 * Get: codigoCpfCnpj.
	 *
	 * @return codigoCpfCnpj
	 */
	public String getCodigoCpfCnpj() {
		return codigoCpfCnpj;
	}
	
	/**
	 * Set: codigoCpfCnpj.
	 *
	 * @param codigoCpfCnpj the codigo cpf cnpj
	 */
	public void setCodigoCpfCnpj(String codigoCpfCnpj) {
		this.codigoCpfCnpj = codigoCpfCnpj;
	}
	
	/**
	 * Get: codigoFilialCpfCnpj.
	 *
	 * @return codigoFilialCpfCnpj
	 */
	public String getCodigoFilialCpfCnpj() {
		return codigoFilialCpfCnpj;
	}
	
	/**
	 * Set: codigoFilialCpfCnpj.
	 *
	 * @param codigoFilialCpfCnpj the codigo filial cpf cnpj
	 */
	public void setCodigoFilialCpfCnpj(String codigoFilialCpfCnpj) {
		this.codigoFilialCpfCnpj = codigoFilialCpfCnpj;
	}
	
	/**
	 * Get: codigoPerfil.
	 *
	 * @return codigoPerfil
	 */
	public String getCodigoPerfil() {
		return codigoPerfil;
	}
	
	/**
	 * Set: codigoPerfil.
	 *
	 * @param codigoPerfil the codigo perfil
	 */
	public void setCodigoPerfil(String codigoPerfil) {
		this.codigoPerfil = codigoPerfil;
	}
	
	/**
	 * Get: codigoSistemaLegado.
	 *
	 * @return codigoSistemaLegado
	 */
	public String getCodigoSistemaLegado() {
		return codigoSistemaLegado;
	}
	
	/**
	 * Set: codigoSistemaLegado.
	 *
	 * @param codigoSistemaLegado the codigo sistema legado
	 */
	public void setCodigoSistemaLegado(String codigoSistemaLegado) {
		this.codigoSistemaLegado = codigoSistemaLegado;
	}
	
	/**
	 * Get: controleCpfCnpj.
	 *
	 * @return controleCpfCnpj
	 */
	public String getControleCpfCnpj() {
		return controleCpfCnpj;
	}
	
	/**
	 * Set: controleCpfCnpj.
	 *
	 * @param controleCpfCnpj the controle cpf cnpj
	 */
	public void setControleCpfCnpj(String controleCpfCnpj) {
		this.controleCpfCnpj = controleCpfCnpj;
	}
	
	/**
	 * Get: numeroArquivoRemessaPagamento.
	 *
	 * @return numeroArquivoRemessaPagamento
	 */
	public String getNumeroArquivoRemessaPagamento() {
		return numeroArquivoRemessaPagamento;
	}
	
	/**
	 * Set: numeroArquivoRemessaPagamento.
	 *
	 * @param numeroArquivoRemessaPagamento the numero arquivo remessa pagamento
	 */
	public void setNumeroArquivoRemessaPagamento(
			String numeroArquivoRemessaPagamento) {
		this.numeroArquivoRemessaPagamento = numeroArquivoRemessaPagamento;
	}
	
	/**
	 * Get: numeroLoteArquivoRetorno.
	 *
	 * @return numeroLoteArquivoRetorno
	 */
	public String getNumeroLoteArquivoRetorno() {
		return numeroLoteArquivoRetorno;
	}
	
	/**
	 * Set: numeroLoteArquivoRetorno.
	 *
	 * @param numeroLoteArquivoRetorno the numero lote arquivo retorno
	 */
	public void setNumeroLoteArquivoRetorno(String numeroLoteArquivoRetorno) {
		this.numeroLoteArquivoRetorno = numeroLoteArquivoRetorno;
	}
	
	/**
	 * Get: numeroSequenciaRegistroRemessa.
	 *
	 * @return numeroSequenciaRegistroRemessa
	 */
	public String getNumeroSequenciaRegistroRemessa() {
		return numeroSequenciaRegistroRemessa;
	}
	
	/**
	 * Set: numeroSequenciaRegistroRemessa.
	 *
	 * @param numeroSequenciaRegistroRemessa the numero sequencia registro remessa
	 */
	public void setNumeroSequenciaRegistroRemessa(
			String numeroSequenciaRegistroRemessa) {
		this.numeroSequenciaRegistroRemessa = numeroSequenciaRegistroRemessa;
	}
	
	/**
	 * Get: horaInclusao.
	 *
	 * @return horaInclusao
	 */
	public String getHoraInclusao() {
		return horaInclusao;
	}
	
	/**
	 * Set: horaInclusao.
	 *
	 * @param horaInclusao the hora inclusao
	 */
	public void setHoraInclusao(String horaInclusao) {
		this.horaInclusao = horaInclusao;
	}
	
	/**
	 * Get: numeroArquivoRetorno.
	 *
	 * @return numeroArquivoRetorno
	 */
	public Long getNumeroArquivoRetorno() {
		return numeroArquivoRetorno;
	}
	
	/**
	 * Set: numeroArquivoRetorno.
	 *
	 * @param numeroArquivoRetorno the numero arquivo retorno
	 */
	public void setNumeroArquivoRetorno(Long numeroArquivoRetorno) {
		this.numeroArquivoRetorno = numeroArquivoRetorno;
	}
	
	/**
	 * Get: numeroLoteRetornoLong.
	 *
	 * @return numeroLoteRetornoLong
	 */
	public Long getNumeroLoteRetornoLong() {
		return numeroLoteRetornoLong;
	}
	
	/**
	 * Set: numeroLoteRetornoLong.
	 *
	 * @param numeroLoteArquivoRetornoLong the numero lote retorno long
	 */
	public void setNumeroLoteRetornoLong(Long numeroLoteArquivoRetornoLong) {
		this.numeroLoteRetornoLong = numeroLoteArquivoRetornoLong;
	}
	
	/**
	 * Get: quantidadeCon.
	 *
	 * @return quantidadeCon
	 */
	public Long getQuantidadeCon() {
		return quantidadeCon;
	}
	
	/**
	 * Set: quantidadeCon.
	 *
	 * @param quantidadeCon the quantidade con
	 */
	public void setQuantidadeCon(Long quantidadeCon) {
		this.quantidadeCon = quantidadeCon;
	}
	
	/**
	 * Get: quantidadeInc.
	 *
	 * @return quantidadeInc
	 */
	public Long getQuantidadeInc() {
		return quantidadeInc;
	}
	
	/**
	 * Set: quantidadeInc.
	 *
	 * @param quantidadeInc the quantidade inc
	 */
	public void setQuantidadeInc(Long quantidadeInc) {
		this.quantidadeInc = quantidadeInc;
	}
	
	/**
	 * Get: quantidadeTotal.
	 *
	 * @return quantidadeTotal
	 */
	public Long getQuantidadeTotal() {
		return quantidadeTotal;
	}
	
	/**
	 * Set: quantidadeTotal.
	 *
	 * @param quantidadeTotal the quantidade total
	 */
	public void setQuantidadeTotal(Long quantidadeTotal) {
		this.quantidadeTotal = quantidadeTotal;
	}
	
	/**
	 * Get: valorCon.
	 *
	 * @return valorCon
	 */
	public Double getValorCon() {
		return valorCon;
	}
	
	/**
	 * Set: valorCon.
	 *
	 * @param valorCon the valor con
	 */
	public void setValorCon(Double valorCon) {
		this.valorCon = valorCon;
	}
	
	/**
	 * Get: valorInc.
	 *
	 * @return valorInc
	 */
	public Double getValorInc() {
		return valorInc;
	}
	
	/**
	 * Set: valorInc.
	 *
	 * @param valorInc the valor inc
	 */
	public void setValorInc(Double valorInc) {
		this.valorInc = valorInc;
	}
	
	/**
	 * Get: valorTotal.
	 *
	 * @return valorTotal
	 */
	public Double getValorTotal() {
		return valorTotal;
	}
	
	/**
	 * Set: valorTotal.
	 *
	 * @param valorTotal the valor total
	 */
	public void setValorTotal(Double valorTotal) {
		this.valorTotal = valorTotal;
	}	
	
}
