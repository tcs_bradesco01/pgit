/*
 * Nome: br.com.bradesco.web.pgit.service.business.orgaopagador.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.orgaopagador.bean;

import java.io.Serializable;

/**
 * Nome: OrgaoPagadorEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class OrgaoPagadorEntradaDTO implements Serializable {

	/** Atributo serialVersionUID. */
	private static final long serialVersionUID = 2925739197199345132L;

	/** Atributo cdOrgaoPagador. */
	private Integer cdOrgaoPagador;

    /** Atributo cdTipoUnidadeOrganizacional. */
    private Integer cdTipoUnidadeOrganizacional;

    /** Atributo cdPessoaJuridica. */
    private Long cdPessoaJuridica;

    /** Atributo cdPessoaJuridicaBradesco. */
    private Long cdPessoaJuridicaBradesco;

    /** Atributo cdPessoaJuridicaParceira. */
    private Long cdPessoaJuridicaParceira;

    /** Atributo nrSeqUnidadeOrganizacional. */
    private Integer nrSeqUnidadeOrganizacional;
    
    /** Atributo nrSeqUnidadeOrganizacionalBradesco. */
    private Integer nrSeqUnidadeOrganizacionalBradesco;
    
    /** Atributo nrSeqUnidadeOrganizacionalParceira. */
    private Integer nrSeqUnidadeOrganizacionalParceira;

    /** Atributo cdSituacao. */
    private Integer cdSituacao;

    /** Atributo cdTarifOrgaoPagador. */
    private Integer cdTarifOrgaoPagador;

    /** Atributo qtConsultas. */
    private Integer qtConsultas;

    /** Atributo hrInclusaoRegistro. */
    private String hrInclusaoRegistro;

    /** Atributo dsMotivoBloqueio. */
    private String dsMotivoBloqueio;

    /** Atributo dsPessoaJuridica. */
    private String dsPessoaJuridica;

    /** Atributo dsTipoUnidadeOrganizacional. */
    private String dsTipoUnidadeOrganizacional;

	/**
	 * Get: cdOrgaoPagador.
	 *
	 * @return cdOrgaoPagador
	 */
	public Integer getCdOrgaoPagador() {
		return cdOrgaoPagador;
	}

	/**
	 * Set: cdOrgaoPagador.
	 *
	 * @param cdOrgaoPagador the cd orgao pagador
	 */
	public void setCdOrgaoPagador(Integer cdOrgaoPagador) {
		this.cdOrgaoPagador = cdOrgaoPagador;
	}

	/**
	 * Get: cdPessoaJuridica.
	 *
	 * @return cdPessoaJuridica
	 */
	public Long getCdPessoaJuridica() {
		return cdPessoaJuridica;
	}

	/**
	 * Set: cdPessoaJuridica.
	 *
	 * @param cdPessoaJuridica the cd pessoa juridica
	 */
	public void setCdPessoaJuridica(Long cdPessoaJuridica) {
		this.cdPessoaJuridica = cdPessoaJuridica;
	}

	/**
	 * Get: cdSituacao.
	 *
	 * @return cdSituacao
	 */
	public Integer getCdSituacao() {
		return cdSituacao;
	}

	/**
	 * Set: cdSituacao.
	 *
	 * @param cdSituacao the cd situacao
	 */
	public void setCdSituacao(Integer cdSituacao) {
		this.cdSituacao = cdSituacao;
	}

	/**
	 * Get: cdTarifOrgaoPagador.
	 *
	 * @return cdTarifOrgaoPagador
	 */
	public Integer getCdTarifOrgaoPagador() {
		return cdTarifOrgaoPagador;
	}

	/**
	 * Set: cdTarifOrgaoPagador.
	 *
	 * @param cdTarifOrgaoPagador the cd tarif orgao pagador
	 */
	public void setCdTarifOrgaoPagador(Integer cdTarifOrgaoPagador) {
		this.cdTarifOrgaoPagador = cdTarifOrgaoPagador;
	}

	/**
	 * Get: cdTipoUnidadeOrganizacional.
	 *
	 * @return cdTipoUnidadeOrganizacional
	 */
	public Integer getCdTipoUnidadeOrganizacional() {
		return cdTipoUnidadeOrganizacional;
	}

	/**
	 * Set: cdTipoUnidadeOrganizacional.
	 *
	 * @param cdTipoUnidadeOrganizacional the cd tipo unidade organizacional
	 */
	public void setCdTipoUnidadeOrganizacional(Integer cdTipoUnidadeOrganizacional) {
		this.cdTipoUnidadeOrganizacional = cdTipoUnidadeOrganizacional;
	}

	/**
	 * Get: nrSeqUnidadeOrganizacional.
	 *
	 * @return nrSeqUnidadeOrganizacional
	 */
	public Integer getNrSeqUnidadeOrganizacional() {
		return nrSeqUnidadeOrganizacional;
	}

	/**
	 * Set: nrSeqUnidadeOrganizacional.
	 *
	 * @param nrSeqUnidadeOrganizacional the nr seq unidade organizacional
	 */
	public void setNrSeqUnidadeOrganizacional(Integer nrSeqUnidadeOrganizacional) {
		this.nrSeqUnidadeOrganizacional = nrSeqUnidadeOrganizacional;
	}

	/**
	 * Get: qtConsultas.
	 *
	 * @return qtConsultas
	 */
	public Integer getQtConsultas() {
		return qtConsultas;
	}

	/**
	 * Set: qtConsultas.
	 *
	 * @param qtConsultas the qt consultas
	 */
	public void setQtConsultas(Integer qtConsultas) {
		this.qtConsultas = qtConsultas;
	}

	/**
	 * Get: hrInclusaoRegistro.
	 *
	 * @return hrInclusaoRegistro
	 */
	public String getHrInclusaoRegistro() {
		return hrInclusaoRegistro;
	}

	/**
	 * Set: hrInclusaoRegistro.
	 *
	 * @param hrInclusaoRegistro the hr inclusao registro
	 */
	public void setHrInclusaoRegistro(String hrInclusaoRegistro) {
		this.hrInclusaoRegistro = hrInclusaoRegistro;
	}

	/**
	 * Get: dsMotivoBloqueio.
	 *
	 * @return dsMotivoBloqueio
	 */
	public String getDsMotivoBloqueio() {
		return dsMotivoBloqueio;
	}

	/**
	 * Set: dsMotivoBloqueio.
	 *
	 * @param dsMotivoBloqueio the ds motivo bloqueio
	 */
	public void setDsMotivoBloqueio(String dsMotivoBloqueio) {
		this.dsMotivoBloqueio = dsMotivoBloqueio;
	}

	/**
	 * Get: cdPessoaJuridicaBradesco.
	 *
	 * @return cdPessoaJuridicaBradesco
	 */
	public Long getCdPessoaJuridicaBradesco() {
		return cdPessoaJuridicaBradesco;
	}

	/**
	 * Set: cdPessoaJuridicaBradesco.
	 *
	 * @param cdPessoaJuridicaBradesco the cd pessoa juridica bradesco
	 */
	public void setCdPessoaJuridicaBradesco(Long cdPessoaJuridicaBradesco) {
		this.cdPessoaJuridicaBradesco = cdPessoaJuridicaBradesco;
	}

	/**
	 * Get: cdPessoaJuridicaParceira.
	 *
	 * @return cdPessoaJuridicaParceira
	 */
	public Long getCdPessoaJuridicaParceira() {
		return cdPessoaJuridicaParceira;
	}

	/**
	 * Set: cdPessoaJuridicaParceira.
	 *
	 * @param cdPessoaJuridicaParceira the cd pessoa juridica parceira
	 */
	public void setCdPessoaJuridicaParceira(Long cdPessoaJuridicaParceira) {
		this.cdPessoaJuridicaParceira = cdPessoaJuridicaParceira;
	}

	/**
	 * Get: nrSeqUnidadeOrganizacionalBradesco.
	 *
	 * @return nrSeqUnidadeOrganizacionalBradesco
	 */
	public Integer getNrSeqUnidadeOrganizacionalBradesco() {
		return nrSeqUnidadeOrganizacionalBradesco;
	}

	/**
	 * Set: nrSeqUnidadeOrganizacionalBradesco.
	 *
	 * @param nrSeqUnidadeOrganizacionalBradesco the nr seq unidade organizacional bradesco
	 */
	public void setNrSeqUnidadeOrganizacionalBradesco(
			Integer nrSeqUnidadeOrganizacionalBradesco) {
		this.nrSeqUnidadeOrganizacionalBradesco = nrSeqUnidadeOrganizacionalBradesco;
	}

	/**
	 * Get: nrSeqUnidadeOrganizacionalParceira.
	 *
	 * @return nrSeqUnidadeOrganizacionalParceira
	 */
	public Integer getNrSeqUnidadeOrganizacionalParceira() {
		return nrSeqUnidadeOrganizacionalParceira;
	}

	/**
	 * Set: nrSeqUnidadeOrganizacionalParceira.
	 *
	 * @param nrSeqUnidadeOrganizacionalParceira the nr seq unidade organizacional parceira
	 */
	public void setNrSeqUnidadeOrganizacionalParceira(
			Integer nrSeqUnidadeOrganizacionalParceira) {
		this.nrSeqUnidadeOrganizacionalParceira = nrSeqUnidadeOrganizacionalParceira;
	}

	/**
	 * Get: dsTipoUnidadeOrganizacional.
	 *
	 * @return dsTipoUnidadeOrganizacional
	 */
	public String getDsTipoUnidadeOrganizacional() {
		return dsTipoUnidadeOrganizacional;
	}

	/**
	 * Set: dsTipoUnidadeOrganizacional.
	 *
	 * @param dsTipoUnidadeOrganizacional the ds tipo unidade organizacional
	 */
	public void setDsTipoUnidadeOrganizacional(String dsTipoUnidadeOrganizacional) {
		this.dsTipoUnidadeOrganizacional = dsTipoUnidadeOrganizacional;
	}

	/**
	 * Get: dsPessoaJuridica.
	 *
	 * @return dsPessoaJuridica
	 */
	public String getDsPessoaJuridica() {
		return dsPessoaJuridica;
	}

	/**
	 * Set: dsPessoaJuridica.
	 *
	 * @param dsPessoaJuridica the ds pessoa juridica
	 */
	public void setDsPessoaJuridica(String dsPessoaJuridica) {
		this.dsPessoaJuridica = dsPessoaJuridica;
	}

}
