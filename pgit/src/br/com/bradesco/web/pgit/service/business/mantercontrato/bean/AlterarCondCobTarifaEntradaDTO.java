/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantercontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantercontrato.bean;

/**
 * Nome: AlterarCondCobTarifaEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class AlterarCondCobTarifaEntradaDTO {
	
	/** Atributo cdPessoaContrato. */
	private long cdPessoaContrato;
	
	/** Atributo cdTipoContrato. */
	private int cdTipoContrato;
	
	/** Atributo nrSequenciaContrato. */
	private long nrSequenciaContrato;
	
	/** Atributo cdTipoServico. */
	private int cdTipoServico;
	
	/** Atributo cdModalidade. */
	private int cdModalidade;
	
	/** Atributo cdPeriodicidadeCobrancaTarifa. */
	private int cdPeriodicidadeCobrancaTarifa;
	
	/** Atributo quantidadeDiasCobrancaTarifa. */
	private int quantidadeDiasCobrancaTarifa;
	
	/** Atributo nrFechamentoApuracaoTarifa. */
	private int nrFechamentoApuracaoTarifa;
	
	
	/**
	 * Get: cdPeriodicidadeCobrancaTarifa.
	 *
	 * @return cdPeriodicidadeCobrancaTarifa
	 */
	public int getCdPeriodicidadeCobrancaTarifa() {
		return cdPeriodicidadeCobrancaTarifa;
	}
	
	/**
	 * Set: cdPeriodicidadeCobrancaTarifa.
	 *
	 * @param cdPeriodicidadeCobrancaTarifa the cd periodicidade cobranca tarifa
	 */
	public void setCdPeriodicidadeCobrancaTarifa(int cdPeriodicidadeCobrancaTarifa) {
		this.cdPeriodicidadeCobrancaTarifa = cdPeriodicidadeCobrancaTarifa;
	}
	
	/**
	 * Get: cdPessoaContrato.
	 *
	 * @return cdPessoaContrato
	 */
	public long getCdPessoaContrato() {
		return cdPessoaContrato;
	}
	
	/**
	 * Set: cdPessoaContrato.
	 *
	 * @param cdPessoaContrato the cd pessoa contrato
	 */
	public void setCdPessoaContrato(long cdPessoaContrato) {
		this.cdPessoaContrato = cdPessoaContrato;
	}
	
	/**
	 * Get: cdTipoContrato.
	 *
	 * @return cdTipoContrato
	 */
	public int getCdTipoContrato() {
		return cdTipoContrato;
	}
	
	/**
	 * Set: cdTipoContrato.
	 *
	 * @param cdTipoContrato the cd tipo contrato
	 */
	public void setCdTipoContrato(int cdTipoContrato) {
		this.cdTipoContrato = cdTipoContrato;
	}
	
	/**
	 * Get: nrFechamentoApuracaoTarifa.
	 *
	 * @return nrFechamentoApuracaoTarifa
	 */
	public int getNrFechamentoApuracaoTarifa() {
		return nrFechamentoApuracaoTarifa;
	}
	
	/**
	 * Set: nrFechamentoApuracaoTarifa.
	 *
	 * @param nrFechamentoApuracaoTarifa the nr fechamento apuracao tarifa
	 */
	public void setNrFechamentoApuracaoTarifa(int nrFechamentoApuracaoTarifa) {
		this.nrFechamentoApuracaoTarifa = nrFechamentoApuracaoTarifa;
	}
	
	/**
	 * Get: nrSequenciaContrato.
	 *
	 * @return nrSequenciaContrato
	 */
	public long getNrSequenciaContrato() {
		return nrSequenciaContrato;
	}
	
	/**
	 * Set: nrSequenciaContrato.
	 *
	 * @param nrSequenciaContrato the nr sequencia contrato
	 */
	public void setNrSequenciaContrato(long nrSequenciaContrato) {
		this.nrSequenciaContrato = nrSequenciaContrato;
	}
	
	/**
	 * Get: quantidadeDiasCobrancaTarifa.
	 *
	 * @return quantidadeDiasCobrancaTarifa
	 */
	public int getQuantidadeDiasCobrancaTarifa() {
		return quantidadeDiasCobrancaTarifa;
	}
	
	/**
	 * Set: quantidadeDiasCobrancaTarifa.
	 *
	 * @param quantidadeDiasCobrancaTarifa the quantidade dias cobranca tarifa
	 */
	public void setQuantidadeDiasCobrancaTarifa(int quantidadeDiasCobrancaTarifa) {
		this.quantidadeDiasCobrancaTarifa = quantidadeDiasCobrancaTarifa;
	}
	
	/**
	 * Get: cdModalidade.
	 *
	 * @return cdModalidade
	 */
	public int getCdModalidade() {
		return cdModalidade;
	}
	
	/**
	 * Set: cdModalidade.
	 *
	 * @param cdModalidade the cd modalidade
	 */
	public void setCdModalidade(int cdModalidade) {
		this.cdModalidade = cdModalidade;
	}
	
	/**
	 * Get: cdTipoServico.
	 *
	 * @return cdTipoServico
	 */
	public int getCdTipoServico() {
		return cdTipoServico;
	}
	
	/**
	 * Set: cdTipoServico.
	 *
	 * @param cdTipoServico the cd tipo servico
	 */
	public void setCdTipoServico(int cdTipoServico) {
		this.cdTipoServico = cdTipoServico;
	}
	
	
	
	

}
