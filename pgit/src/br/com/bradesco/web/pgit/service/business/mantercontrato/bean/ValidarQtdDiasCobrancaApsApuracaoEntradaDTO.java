/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantercontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantercontrato.bean;

// TODO: Auto-generated Javadoc
/**
 * Nome: ValidarQtdDiasCobrancaApsApuracaoEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ValidarQtdDiasCobrancaApsApuracaoEntradaDTO {
	
	/** Atributo quantidadeDiasCobrancaTarifa. */
	private Integer quantidadeDiasCobrancaTarifa;

	/** The Cd periodicidade cobranca tarifa. */
	private Integer CdPeriodicidadeCobrancaTarifa; 
	
	/**
	 * Get: quantidadeDiasCobrancaTarifa.
	 *
	 * @return quantidadeDiasCobrancaTarifa
	 */
	public Integer getQuantidadeDiasCobrancaTarifa() {
		return quantidadeDiasCobrancaTarifa;
	}

	/**
	 * Set: quantidadeDiasCobrancaTarifa.
	 *
	 * @param quantidadeDiasCobrancaTarifa the quantidade dias cobranca tarifa
	 */
	public void setQuantidadeDiasCobrancaTarifa(Integer quantidadeDiasCobrancaTarifa) {
		this.quantidadeDiasCobrancaTarifa = quantidadeDiasCobrancaTarifa;
	}

	/**
	 * Gets the cd periodicidade cobranca tarifa.
	 *
	 * @return the cd periodicidade cobranca tarifa
	 */
	public Integer getCdPeriodicidadeCobrancaTarifa() {
		return CdPeriodicidadeCobrancaTarifa;
	}

	/**
	 * Sets the cd periodicidade cobranca tarifa.
	 *
	 * @param cdPeriodicidadeCobrancaTarifa the new cd periodicidade cobranca tarifa
	 */
	public void setCdPeriodicidadeCobrancaTarifa(
			Integer cdPeriodicidadeCobrancaTarifa) {
		CdPeriodicidadeCobrancaTarifa = cdPeriodicidadeCobrancaTarifa;
	}
}
