package br.com.bradesco.web.pgit.service.business.combo.bean;


// TODO: Auto-generated Javadoc
/**
 * Arquivo criado em 22/01/16.
 */
public class ListarParametrosOcorrenciasSaidaDTO {

    /** The cd parametro. */
    private Integer cdParametro = null;

    /** The ds parametro. */
    private String dsParametro = null;

    /** The cd indicador utiliza descricao. */
    private Integer cdIndicadorUtilizaDescricao = null;

    /**
     * Sets the cd parametro.
     *
     * @param cdParametro the cd parametro
     */
    public void setCdParametro(Integer cdParametro) {
        this.cdParametro = cdParametro;
    }

    /**
     * Gets the cd parametro.
     *
     * @return the cd parametro
     */
    public Integer getCdParametro() {
        return this.cdParametro;
    }

    /**
     * Sets the ds parametro.
     *
     * @param dsParametro the ds parametro
     */
    public void setDsParametro(String dsParametro) {
        this.dsParametro = dsParametro;
    }

    /**
     * Gets the ds parametro.
     *
     * @return the ds parametro
     */
    public String getDsParametro() {
        return this.dsParametro;
    }

    /**
     * Sets the cd indicador utiliza descricao.
     *
     * @param cdIndicadorUtilizaDescricao the cd indicador utiliza descricao
     */
    public void setCdIndicadorUtilizaDescricao(Integer cdIndicadorUtilizaDescricao) {
        this.cdIndicadorUtilizaDescricao = cdIndicadorUtilizaDescricao;
    }

    /**
     * Gets the cd indicador utiliza descricao.
     *
     * @return the cd indicador utiliza descricao
     */
    public Integer getCdIndicadorUtilizaDescricao() {
        return this.cdIndicadorUtilizaDescricao;
    }
}