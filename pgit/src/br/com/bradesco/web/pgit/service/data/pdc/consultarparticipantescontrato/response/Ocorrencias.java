/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultarparticipantescontrato.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdPessoaJuridicaContrato
     */
    private long _cdPessoaJuridicaContrato = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridicaContrato
     */
    private boolean _has_cdPessoaJuridicaContrato;

    /**
     * Field _cdCorpoCpfCnpj
     */
    private long _cdCorpoCpfCnpj = 0;

    /**
     * keeps track of state for field: _cdCorpoCpfCnpj
     */
    private boolean _has_cdCorpoCpfCnpj;

    /**
     * Field _cdFilialCnpj
     */
    private int _cdFilialCnpj = 0;

    /**
     * keeps track of state for field: _cdFilialCnpj
     */
    private boolean _has_cdFilialCnpj;

    /**
     * Field _cdControleCpfCnpj
     */
    private int _cdControleCpfCnpj = 0;

    /**
     * keeps track of state for field: _cdControleCpfCnpj
     */
    private boolean _has_cdControleCpfCnpj;

    /**
     * Field _dsNomeRazaoSocial
     */
    private java.lang.String _dsNomeRazaoSocial;

    /**
     * Field _cdGrupoEconomico
     */
    private long _cdGrupoEconomico = 0;

    /**
     * keeps track of state for field: _cdGrupoEconomico
     */
    private boolean _has_cdGrupoEconomico;

    /**
     * Field _dsGrupoEconomico
     */
    private java.lang.String _dsGrupoEconomico;

    /**
     * Field _cdAtividadeEconomica
     */
    private int _cdAtividadeEconomica = 0;

    /**
     * keeps track of state for field: _cdAtividadeEconomica
     */
    private boolean _has_cdAtividadeEconomica;

    /**
     * Field _dsAtividadeEconomica
     */
    private java.lang.String _dsAtividadeEconomica;

    /**
     * Field _cdSegmentoEconomico
     */
    private int _cdSegmentoEconomico = 0;

    /**
     * keeps track of state for field: _cdSegmentoEconomico
     */
    private boolean _has_cdSegmentoEconomico;

    /**
     * Field _dsSegmentoEconomico
     */
    private java.lang.String _dsSegmentoEconomico;

    /**
     * Field _cdSubSegmento
     */
    private int _cdSubSegmento = 0;

    /**
     * keeps track of state for field: _cdSubSegmento
     */
    private boolean _has_cdSubSegmento;

    /**
     * Field _dsSubSegmentoCliente
     */
    private java.lang.String _dsSubSegmentoCliente;

    /**
     * Field _cdTipoParticipante
     */
    private int _cdTipoParticipante = 0;

    /**
     * keeps track of state for field: _cdTipoParticipante
     */
    private boolean _has_cdTipoParticipante;

    /**
     * Field _dsTipoParticipante
     */
    private java.lang.String _dsTipoParticipante;

    /**
     * Field _cdSituacaoParticipante
     */
    private int _cdSituacaoParticipante = 0;

    /**
     * keeps track of state for field: _cdSituacaoParticipante
     */
    private boolean _has_cdSituacaoParticipante;

    /**
     * Field _dsSituacaoParticipante
     */
    private java.lang.String _dsSituacaoParticipante;

    /**
     * Field _cdMotivoParticipante
     */
    private int _cdMotivoParticipante = 0;

    /**
     * keeps track of state for field: _cdMotivoParticipante
     */
    private boolean _has_cdMotivoParticipante;

    /**
     * Field _dsMotivoParticipante
     */
    private java.lang.String _dsMotivoParticipante;

    /**
     * Field _cdClassificacaoParticipacao
     */
    private int _cdClassificacaoParticipacao = 0;

    /**
     * keeps track of state for field: _cdClassificacaoParticipacao
     */
    private boolean _has_cdClassificacaoParticipacao;

    /**
     * Field _dsClassificacaoParticipacao
     */
    private java.lang.String _dsClassificacaoParticipacao;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarparticipantescontrato.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdAtividadeEconomica
     * 
     */
    public void deleteCdAtividadeEconomica()
    {
        this._has_cdAtividadeEconomica= false;
    } //-- void deleteCdAtividadeEconomica() 

    /**
     * Method deleteCdClassificacaoParticipacao
     * 
     */
    public void deleteCdClassificacaoParticipacao()
    {
        this._has_cdClassificacaoParticipacao= false;
    } //-- void deleteCdClassificacaoParticipacao() 

    /**
     * Method deleteCdControleCpfCnpj
     * 
     */
    public void deleteCdControleCpfCnpj()
    {
        this._has_cdControleCpfCnpj= false;
    } //-- void deleteCdControleCpfCnpj() 

    /**
     * Method deleteCdCorpoCpfCnpj
     * 
     */
    public void deleteCdCorpoCpfCnpj()
    {
        this._has_cdCorpoCpfCnpj= false;
    } //-- void deleteCdCorpoCpfCnpj() 

    /**
     * Method deleteCdFilialCnpj
     * 
     */
    public void deleteCdFilialCnpj()
    {
        this._has_cdFilialCnpj= false;
    } //-- void deleteCdFilialCnpj() 

    /**
     * Method deleteCdGrupoEconomico
     * 
     */
    public void deleteCdGrupoEconomico()
    {
        this._has_cdGrupoEconomico= false;
    } //-- void deleteCdGrupoEconomico() 

    /**
     * Method deleteCdMotivoParticipante
     * 
     */
    public void deleteCdMotivoParticipante()
    {
        this._has_cdMotivoParticipante= false;
    } //-- void deleteCdMotivoParticipante() 

    /**
     * Method deleteCdPessoaJuridicaContrato
     * 
     */
    public void deleteCdPessoaJuridicaContrato()
    {
        this._has_cdPessoaJuridicaContrato= false;
    } //-- void deleteCdPessoaJuridicaContrato() 

    /**
     * Method deleteCdSegmentoEconomico
     * 
     */
    public void deleteCdSegmentoEconomico()
    {
        this._has_cdSegmentoEconomico= false;
    } //-- void deleteCdSegmentoEconomico() 

    /**
     * Method deleteCdSituacaoParticipante
     * 
     */
    public void deleteCdSituacaoParticipante()
    {
        this._has_cdSituacaoParticipante= false;
    } //-- void deleteCdSituacaoParticipante() 

    /**
     * Method deleteCdSubSegmento
     * 
     */
    public void deleteCdSubSegmento()
    {
        this._has_cdSubSegmento= false;
    } //-- void deleteCdSubSegmento() 

    /**
     * Method deleteCdTipoParticipante
     * 
     */
    public void deleteCdTipoParticipante()
    {
        this._has_cdTipoParticipante= false;
    } //-- void deleteCdTipoParticipante() 

    /**
     * Returns the value of field 'cdAtividadeEconomica'.
     * 
     * @return int
     * @return the value of field 'cdAtividadeEconomica'.
     */
    public int getCdAtividadeEconomica()
    {
        return this._cdAtividadeEconomica;
    } //-- int getCdAtividadeEconomica() 

    /**
     * Returns the value of field 'cdClassificacaoParticipacao'.
     * 
     * @return int
     * @return the value of field 'cdClassificacaoParticipacao'.
     */
    public int getCdClassificacaoParticipacao()
    {
        return this._cdClassificacaoParticipacao;
    } //-- int getCdClassificacaoParticipacao() 

    /**
     * Returns the value of field 'cdControleCpfCnpj'.
     * 
     * @return int
     * @return the value of field 'cdControleCpfCnpj'.
     */
    public int getCdControleCpfCnpj()
    {
        return this._cdControleCpfCnpj;
    } //-- int getCdControleCpfCnpj() 

    /**
     * Returns the value of field 'cdCorpoCpfCnpj'.
     * 
     * @return long
     * @return the value of field 'cdCorpoCpfCnpj'.
     */
    public long getCdCorpoCpfCnpj()
    {
        return this._cdCorpoCpfCnpj;
    } //-- long getCdCorpoCpfCnpj() 

    /**
     * Returns the value of field 'cdFilialCnpj'.
     * 
     * @return int
     * @return the value of field 'cdFilialCnpj'.
     */
    public int getCdFilialCnpj()
    {
        return this._cdFilialCnpj;
    } //-- int getCdFilialCnpj() 

    /**
     * Returns the value of field 'cdGrupoEconomico'.
     * 
     * @return long
     * @return the value of field 'cdGrupoEconomico'.
     */
    public long getCdGrupoEconomico()
    {
        return this._cdGrupoEconomico;
    } //-- long getCdGrupoEconomico() 

    /**
     * Returns the value of field 'cdMotivoParticipante'.
     * 
     * @return int
     * @return the value of field 'cdMotivoParticipante'.
     */
    public int getCdMotivoParticipante()
    {
        return this._cdMotivoParticipante;
    } //-- int getCdMotivoParticipante() 

    /**
     * Returns the value of field 'cdPessoaJuridicaContrato'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridicaContrato'.
     */
    public long getCdPessoaJuridicaContrato()
    {
        return this._cdPessoaJuridicaContrato;
    } //-- long getCdPessoaJuridicaContrato() 

    /**
     * Returns the value of field 'cdSegmentoEconomico'.
     * 
     * @return int
     * @return the value of field 'cdSegmentoEconomico'.
     */
    public int getCdSegmentoEconomico()
    {
        return this._cdSegmentoEconomico;
    } //-- int getCdSegmentoEconomico() 

    /**
     * Returns the value of field 'cdSituacaoParticipante'.
     * 
     * @return int
     * @return the value of field 'cdSituacaoParticipante'.
     */
    public int getCdSituacaoParticipante()
    {
        return this._cdSituacaoParticipante;
    } //-- int getCdSituacaoParticipante() 

    /**
     * Returns the value of field 'cdSubSegmento'.
     * 
     * @return int
     * @return the value of field 'cdSubSegmento'.
     */
    public int getCdSubSegmento()
    {
        return this._cdSubSegmento;
    } //-- int getCdSubSegmento() 

    /**
     * Returns the value of field 'cdTipoParticipante'.
     * 
     * @return int
     * @return the value of field 'cdTipoParticipante'.
     */
    public int getCdTipoParticipante()
    {
        return this._cdTipoParticipante;
    } //-- int getCdTipoParticipante() 

    /**
     * Returns the value of field 'dsAtividadeEconomica'.
     * 
     * @return String
     * @return the value of field 'dsAtividadeEconomica'.
     */
    public java.lang.String getDsAtividadeEconomica()
    {
        return this._dsAtividadeEconomica;
    } //-- java.lang.String getDsAtividadeEconomica() 

    /**
     * Returns the value of field 'dsClassificacaoParticipacao'.
     * 
     * @return String
     * @return the value of field 'dsClassificacaoParticipacao'.
     */
    public java.lang.String getDsClassificacaoParticipacao()
    {
        return this._dsClassificacaoParticipacao;
    } //-- java.lang.String getDsClassificacaoParticipacao() 

    /**
     * Returns the value of field 'dsGrupoEconomico'.
     * 
     * @return String
     * @return the value of field 'dsGrupoEconomico'.
     */
    public java.lang.String getDsGrupoEconomico()
    {
        return this._dsGrupoEconomico;
    } //-- java.lang.String getDsGrupoEconomico() 

    /**
     * Returns the value of field 'dsMotivoParticipante'.
     * 
     * @return String
     * @return the value of field 'dsMotivoParticipante'.
     */
    public java.lang.String getDsMotivoParticipante()
    {
        return this._dsMotivoParticipante;
    } //-- java.lang.String getDsMotivoParticipante() 

    /**
     * Returns the value of field 'dsNomeRazaoSocial'.
     * 
     * @return String
     * @return the value of field 'dsNomeRazaoSocial'.
     */
    public java.lang.String getDsNomeRazaoSocial()
    {
        return this._dsNomeRazaoSocial;
    } //-- java.lang.String getDsNomeRazaoSocial() 

    /**
     * Returns the value of field 'dsSegmentoEconomico'.
     * 
     * @return String
     * @return the value of field 'dsSegmentoEconomico'.
     */
    public java.lang.String getDsSegmentoEconomico()
    {
        return this._dsSegmentoEconomico;
    } //-- java.lang.String getDsSegmentoEconomico() 

    /**
     * Returns the value of field 'dsSituacaoParticipante'.
     * 
     * @return String
     * @return the value of field 'dsSituacaoParticipante'.
     */
    public java.lang.String getDsSituacaoParticipante()
    {
        return this._dsSituacaoParticipante;
    } //-- java.lang.String getDsSituacaoParticipante() 

    /**
     * Returns the value of field 'dsSubSegmentoCliente'.
     * 
     * @return String
     * @return the value of field 'dsSubSegmentoCliente'.
     */
    public java.lang.String getDsSubSegmentoCliente()
    {
        return this._dsSubSegmentoCliente;
    } //-- java.lang.String getDsSubSegmentoCliente() 

    /**
     * Returns the value of field 'dsTipoParticipante'.
     * 
     * @return String
     * @return the value of field 'dsTipoParticipante'.
     */
    public java.lang.String getDsTipoParticipante()
    {
        return this._dsTipoParticipante;
    } //-- java.lang.String getDsTipoParticipante() 

    /**
     * Method hasCdAtividadeEconomica
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAtividadeEconomica()
    {
        return this._has_cdAtividadeEconomica;
    } //-- boolean hasCdAtividadeEconomica() 

    /**
     * Method hasCdClassificacaoParticipacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdClassificacaoParticipacao()
    {
        return this._has_cdClassificacaoParticipacao;
    } //-- boolean hasCdClassificacaoParticipacao() 

    /**
     * Method hasCdControleCpfCnpj
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdControleCpfCnpj()
    {
        return this._has_cdControleCpfCnpj;
    } //-- boolean hasCdControleCpfCnpj() 

    /**
     * Method hasCdCorpoCpfCnpj
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCorpoCpfCnpj()
    {
        return this._has_cdCorpoCpfCnpj;
    } //-- boolean hasCdCorpoCpfCnpj() 

    /**
     * Method hasCdFilialCnpj
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFilialCnpj()
    {
        return this._has_cdFilialCnpj;
    } //-- boolean hasCdFilialCnpj() 

    /**
     * Method hasCdGrupoEconomico
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdGrupoEconomico()
    {
        return this._has_cdGrupoEconomico;
    } //-- boolean hasCdGrupoEconomico() 

    /**
     * Method hasCdMotivoParticipante
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMotivoParticipante()
    {
        return this._has_cdMotivoParticipante;
    } //-- boolean hasCdMotivoParticipante() 

    /**
     * Method hasCdPessoaJuridicaContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridicaContrato()
    {
        return this._has_cdPessoaJuridicaContrato;
    } //-- boolean hasCdPessoaJuridicaContrato() 

    /**
     * Method hasCdSegmentoEconomico
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSegmentoEconomico()
    {
        return this._has_cdSegmentoEconomico;
    } //-- boolean hasCdSegmentoEconomico() 

    /**
     * Method hasCdSituacaoParticipante
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSituacaoParticipante()
    {
        return this._has_cdSituacaoParticipante;
    } //-- boolean hasCdSituacaoParticipante() 

    /**
     * Method hasCdSubSegmento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSubSegmento()
    {
        return this._has_cdSubSegmento;
    } //-- boolean hasCdSubSegmento() 

    /**
     * Method hasCdTipoParticipante
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoParticipante()
    {
        return this._has_cdTipoParticipante;
    } //-- boolean hasCdTipoParticipante() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdAtividadeEconomica'.
     * 
     * @param cdAtividadeEconomica the value of field
     * 'cdAtividadeEconomica'.
     */
    public void setCdAtividadeEconomica(int cdAtividadeEconomica)
    {
        this._cdAtividadeEconomica = cdAtividadeEconomica;
        this._has_cdAtividadeEconomica = true;
    } //-- void setCdAtividadeEconomica(int) 

    /**
     * Sets the value of field 'cdClassificacaoParticipacao'.
     * 
     * @param cdClassificacaoParticipacao the value of field
     * 'cdClassificacaoParticipacao'.
     */
    public void setCdClassificacaoParticipacao(int cdClassificacaoParticipacao)
    {
        this._cdClassificacaoParticipacao = cdClassificacaoParticipacao;
        this._has_cdClassificacaoParticipacao = true;
    } //-- void setCdClassificacaoParticipacao(int) 

    /**
     * Sets the value of field 'cdControleCpfCnpj'.
     * 
     * @param cdControleCpfCnpj the value of field
     * 'cdControleCpfCnpj'.
     */
    public void setCdControleCpfCnpj(int cdControleCpfCnpj)
    {
        this._cdControleCpfCnpj = cdControleCpfCnpj;
        this._has_cdControleCpfCnpj = true;
    } //-- void setCdControleCpfCnpj(int) 

    /**
     * Sets the value of field 'cdCorpoCpfCnpj'.
     * 
     * @param cdCorpoCpfCnpj the value of field 'cdCorpoCpfCnpj'.
     */
    public void setCdCorpoCpfCnpj(long cdCorpoCpfCnpj)
    {
        this._cdCorpoCpfCnpj = cdCorpoCpfCnpj;
        this._has_cdCorpoCpfCnpj = true;
    } //-- void setCdCorpoCpfCnpj(long) 

    /**
     * Sets the value of field 'cdFilialCnpj'.
     * 
     * @param cdFilialCnpj the value of field 'cdFilialCnpj'.
     */
    public void setCdFilialCnpj(int cdFilialCnpj)
    {
        this._cdFilialCnpj = cdFilialCnpj;
        this._has_cdFilialCnpj = true;
    } //-- void setCdFilialCnpj(int) 

    /**
     * Sets the value of field 'cdGrupoEconomico'.
     * 
     * @param cdGrupoEconomico the value of field 'cdGrupoEconomico'
     */
    public void setCdGrupoEconomico(long cdGrupoEconomico)
    {
        this._cdGrupoEconomico = cdGrupoEconomico;
        this._has_cdGrupoEconomico = true;
    } //-- void setCdGrupoEconomico(long) 

    /**
     * Sets the value of field 'cdMotivoParticipante'.
     * 
     * @param cdMotivoParticipante the value of field
     * 'cdMotivoParticipante'.
     */
    public void setCdMotivoParticipante(int cdMotivoParticipante)
    {
        this._cdMotivoParticipante = cdMotivoParticipante;
        this._has_cdMotivoParticipante = true;
    } //-- void setCdMotivoParticipante(int) 

    /**
     * Sets the value of field 'cdPessoaJuridicaContrato'.
     * 
     * @param cdPessoaJuridicaContrato the value of field
     * 'cdPessoaJuridicaContrato'.
     */
    public void setCdPessoaJuridicaContrato(long cdPessoaJuridicaContrato)
    {
        this._cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
        this._has_cdPessoaJuridicaContrato = true;
    } //-- void setCdPessoaJuridicaContrato(long) 

    /**
     * Sets the value of field 'cdSegmentoEconomico'.
     * 
     * @param cdSegmentoEconomico the value of field
     * 'cdSegmentoEconomico'.
     */
    public void setCdSegmentoEconomico(int cdSegmentoEconomico)
    {
        this._cdSegmentoEconomico = cdSegmentoEconomico;
        this._has_cdSegmentoEconomico = true;
    } //-- void setCdSegmentoEconomico(int) 

    /**
     * Sets the value of field 'cdSituacaoParticipante'.
     * 
     * @param cdSituacaoParticipante the value of field
     * 'cdSituacaoParticipante'.
     */
    public void setCdSituacaoParticipante(int cdSituacaoParticipante)
    {
        this._cdSituacaoParticipante = cdSituacaoParticipante;
        this._has_cdSituacaoParticipante = true;
    } //-- void setCdSituacaoParticipante(int) 

    /**
     * Sets the value of field 'cdSubSegmento'.
     * 
     * @param cdSubSegmento the value of field 'cdSubSegmento'.
     */
    public void setCdSubSegmento(int cdSubSegmento)
    {
        this._cdSubSegmento = cdSubSegmento;
        this._has_cdSubSegmento = true;
    } //-- void setCdSubSegmento(int) 

    /**
     * Sets the value of field 'cdTipoParticipante'.
     * 
     * @param cdTipoParticipante the value of field
     * 'cdTipoParticipante'.
     */
    public void setCdTipoParticipante(int cdTipoParticipante)
    {
        this._cdTipoParticipante = cdTipoParticipante;
        this._has_cdTipoParticipante = true;
    } //-- void setCdTipoParticipante(int) 

    /**
     * Sets the value of field 'dsAtividadeEconomica'.
     * 
     * @param dsAtividadeEconomica the value of field
     * 'dsAtividadeEconomica'.
     */
    public void setDsAtividadeEconomica(java.lang.String dsAtividadeEconomica)
    {
        this._dsAtividadeEconomica = dsAtividadeEconomica;
    } //-- void setDsAtividadeEconomica(java.lang.String) 

    /**
     * Sets the value of field 'dsClassificacaoParticipacao'.
     * 
     * @param dsClassificacaoParticipacao the value of field
     * 'dsClassificacaoParticipacao'.
     */
    public void setDsClassificacaoParticipacao(java.lang.String dsClassificacaoParticipacao)
    {
        this._dsClassificacaoParticipacao = dsClassificacaoParticipacao;
    } //-- void setDsClassificacaoParticipacao(java.lang.String) 

    /**
     * Sets the value of field 'dsGrupoEconomico'.
     * 
     * @param dsGrupoEconomico the value of field 'dsGrupoEconomico'
     */
    public void setDsGrupoEconomico(java.lang.String dsGrupoEconomico)
    {
        this._dsGrupoEconomico = dsGrupoEconomico;
    } //-- void setDsGrupoEconomico(java.lang.String) 

    /**
     * Sets the value of field 'dsMotivoParticipante'.
     * 
     * @param dsMotivoParticipante the value of field
     * 'dsMotivoParticipante'.
     */
    public void setDsMotivoParticipante(java.lang.String dsMotivoParticipante)
    {
        this._dsMotivoParticipante = dsMotivoParticipante;
    } //-- void setDsMotivoParticipante(java.lang.String) 

    /**
     * Sets the value of field 'dsNomeRazaoSocial'.
     * 
     * @param dsNomeRazaoSocial the value of field
     * 'dsNomeRazaoSocial'.
     */
    public void setDsNomeRazaoSocial(java.lang.String dsNomeRazaoSocial)
    {
        this._dsNomeRazaoSocial = dsNomeRazaoSocial;
    } //-- void setDsNomeRazaoSocial(java.lang.String) 

    /**
     * Sets the value of field 'dsSegmentoEconomico'.
     * 
     * @param dsSegmentoEconomico the value of field
     * 'dsSegmentoEconomico'.
     */
    public void setDsSegmentoEconomico(java.lang.String dsSegmentoEconomico)
    {
        this._dsSegmentoEconomico = dsSegmentoEconomico;
    } //-- void setDsSegmentoEconomico(java.lang.String) 

    /**
     * Sets the value of field 'dsSituacaoParticipante'.
     * 
     * @param dsSituacaoParticipante the value of field
     * 'dsSituacaoParticipante'.
     */
    public void setDsSituacaoParticipante(java.lang.String dsSituacaoParticipante)
    {
        this._dsSituacaoParticipante = dsSituacaoParticipante;
    } //-- void setDsSituacaoParticipante(java.lang.String) 

    /**
     * Sets the value of field 'dsSubSegmentoCliente'.
     * 
     * @param dsSubSegmentoCliente the value of field
     * 'dsSubSegmentoCliente'.
     */
    public void setDsSubSegmentoCliente(java.lang.String dsSubSegmentoCliente)
    {
        this._dsSubSegmentoCliente = dsSubSegmentoCliente;
    } //-- void setDsSubSegmentoCliente(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoParticipante'.
     * 
     * @param dsTipoParticipante the value of field
     * 'dsTipoParticipante'.
     */
    public void setDsTipoParticipante(java.lang.String dsTipoParticipante)
    {
        this._dsTipoParticipante = dsTipoParticipante;
    } //-- void setDsTipoParticipante(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultarparticipantescontrato.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultarparticipantescontrato.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultarparticipantescontrato.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarparticipantescontrato.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
