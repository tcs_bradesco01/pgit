/*
 * Nome: br.com.bradesco.web.pgit.service.business.mansolrecuperacaopagconsulta.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mansolrecuperacaopagconsulta.bean;


/**
 * Nome: ConsultarSolRecuperacaoPagtosBackupSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarSolRecuperacaoPagtosBackupSaidaDTO {

	/** Atributo cdSolicitacaoPagamento. */
	private Integer cdSolicitacaoPagamento;
	   
   	/** Atributo nrSolicitacaoPagamento. */
   	private Integer nrSolicitacaoPagamento;
	   
   	/** Atributo cdCpfCnpjParticipante. */
   	private String cdCpfCnpjParticipante;
	   
   	/** Atributo dsRazaoSocial. */
   	private String dsRazaoSocial;
	   
   	/** Atributo cdPessoaJuridicaContrato. */
   	private Long cdPessoaJuridicaContrato;
	   
   	/** Atributo dsEmpresa. */
   	private String dsEmpresa;
	   
   	/** Atributo cdTipoContratoNegocio. */
   	private Integer cdTipoContratoNegocio;
	   
   	/** Atributo nrSequenciaContratoNegocio. */
   	private Long nrSequenciaContratoNegocio;
	   
   	/** Atributo dsDataSolicitacao. */
   	private String dsDataSolicitacao;
	   
   	/** Atributo dsHoraSolicitacao. */
   	private String dsHoraSolicitacao;
	  
	   /** Atributo cdProdutoServicoOperacao. */
   	private Integer cdProdutoServicoOperacao;
	   
   	/** Atributo dsResumoProdutoServico. */
   	private String dsResumoProdutoServico;
	   
   	/** Atributo cdProdutoServicoRelacionado. */
   	private Integer cdProdutoServicoRelacionado;
	   
   	/** Atributo dsResumoServicoRelacionado. */
   	private String dsResumoServicoRelacionado;
	   
   	/** Atributo cdSituacaoSolicitacaoPagamento. */
   	private Integer cdSituacaoSolicitacaoPagamento;
	   
   	/** Atributo dsSituacaoSolicitacao. */
   	private String dsSituacaoSolicitacao;
	   
   	/** Atributo cdMotivoSolicitacao. */
   	private Integer cdMotivoSolicitacao;
	   
   	/** Atributo dsMotivoSolicitacao. */
   	private String dsMotivoSolicitacao;
	   
   	/** Atributo dtRecuperacao. */
   	private String dtRecuperacao;
	   
	/**
	 * Get: cdCpfCnpjParticipante.
	 *
	 * @return cdCpfCnpjParticipante
	 */
	public String getCdCpfCnpjParticipante() {
		return cdCpfCnpjParticipante;
	}
	
	/**
	 * Set: cdCpfCnpjParticipante.
	 *
	 * @param cdCpfCnpjParticipante the cd cpf cnpj participante
	 */
	public void setCdCpfCnpjParticipante(String cdCpfCnpjParticipante) {
		this.cdCpfCnpjParticipante = cdCpfCnpjParticipante;
	}
	
	/**
	 * Get: cdMotivoSolicitacao.
	 *
	 * @return cdMotivoSolicitacao
	 */
	public Integer getCdMotivoSolicitacao() {
		return cdMotivoSolicitacao;
	}
	
	/**
	 * Set: cdMotivoSolicitacao.
	 *
	 * @param cdMotivoSolicitacao the cd motivo solicitacao
	 */
	public void setCdMotivoSolicitacao(Integer cdMotivoSolicitacao) {
		this.cdMotivoSolicitacao = cdMotivoSolicitacao;
	}
	
	/**
	 * Get: cdPessoaJuridicaContrato.
	 *
	 * @return cdPessoaJuridicaContrato
	 */
	public Long getCdPessoaJuridicaContrato() {
		return cdPessoaJuridicaContrato;
	}
	
	/**
	 * Set: cdPessoaJuridicaContrato.
	 *
	 * @param cdPessoaJuridicaContrato the cd pessoa juridica contrato
	 */
	public void setCdPessoaJuridicaContrato(Long cdPessoaJuridicaContrato) {
		this.cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
	}
	
	/**
	 * Get: cdProdutoServicoOperacao.
	 *
	 * @return cdProdutoServicoOperacao
	 */
	public Integer getCdProdutoServicoOperacao() {
		return cdProdutoServicoOperacao;
	}
	
	/**
	 * Set: cdProdutoServicoOperacao.
	 *
	 * @param cdProdutoServicoOperacao the cd produto servico operacao
	 */
	public void setCdProdutoServicoOperacao(Integer cdProdutoServicoOperacao) {
		this.cdProdutoServicoOperacao = cdProdutoServicoOperacao;
	}
	
	/**
	 * Get: cdProdutoServicoRelacionado.
	 *
	 * @return cdProdutoServicoRelacionado
	 */
	public Integer getCdProdutoServicoRelacionado() {
		return cdProdutoServicoRelacionado;
	}
	
	/**
	 * Set: cdProdutoServicoRelacionado.
	 *
	 * @param cdProdutoServicoRelacionado the cd produto servico relacionado
	 */
	public void setCdProdutoServicoRelacionado(Integer cdProdutoServicoRelacionado) {
		this.cdProdutoServicoRelacionado = cdProdutoServicoRelacionado;
	}
	
	/**
	 * Get: cdSituacaoSolicitacaoPagamento.
	 *
	 * @return cdSituacaoSolicitacaoPagamento
	 */
	public Integer getCdSituacaoSolicitacaoPagamento() {
		return cdSituacaoSolicitacaoPagamento;
	}
	
	/**
	 * Set: cdSituacaoSolicitacaoPagamento.
	 *
	 * @param cdSituacaoSolicitacaoPagamento the cd situacao solicitacao pagamento
	 */
	public void setCdSituacaoSolicitacaoPagamento(
			Integer cdSituacaoSolicitacaoPagamento) {
		this.cdSituacaoSolicitacaoPagamento = cdSituacaoSolicitacaoPagamento;
	}
	
	/**
	 * Get: cdSolicitacaoPagamento.
	 *
	 * @return cdSolicitacaoPagamento
	 */
	public Integer getCdSolicitacaoPagamento() {
		return cdSolicitacaoPagamento;
	}
	
	/**
	 * Set: cdSolicitacaoPagamento.
	 *
	 * @param cdSolicitacaoPagamento the cd solicitacao pagamento
	 */
	public void setCdSolicitacaoPagamento(Integer cdSolicitacaoPagamento) {
		this.cdSolicitacaoPagamento = cdSolicitacaoPagamento;
	}
	
	/**
	 * Get: cdTipoContratoNegocio.
	 *
	 * @return cdTipoContratoNegocio
	 */
	public Integer getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}
	
	/**
	 * Set: cdTipoContratoNegocio.
	 *
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}
	
	/**
	 * Get: dsDataSolicitacao.
	 *
	 * @return dsDataSolicitacao
	 */
	public String getDsDataSolicitacao() {
		return dsDataSolicitacao;
	}
	
	/**
	 * Set: dsDataSolicitacao.
	 *
	 * @param dsDataSolicitacao the ds data solicitacao
	 */
	public void setDsDataSolicitacao(String dsDataSolicitacao) {
		this.dsDataSolicitacao = dsDataSolicitacao;
	}
	
	/**
	 * Get: dsEmpresa.
	 *
	 * @return dsEmpresa
	 */
	public String getDsEmpresa() {
		return dsEmpresa;
	}
	
	/**
	 * Set: dsEmpresa.
	 *
	 * @param dsEmpresa the ds empresa
	 */
	public void setDsEmpresa(String dsEmpresa) {
		this.dsEmpresa = dsEmpresa;
	}
	
	/**
	 * Get: dsMotivoSolicitacao.
	 *
	 * @return dsMotivoSolicitacao
	 */
	public String getDsMotivoSolicitacao() {
		return dsMotivoSolicitacao;
	}
	
	/**
	 * Set: dsMotivoSolicitacao.
	 *
	 * @param dsMotivoSolicitacao the ds motivo solicitacao
	 */
	public void setDsMotivoSolicitacao(String dsMotivoSolicitacao) {
		this.dsMotivoSolicitacao = dsMotivoSolicitacao;
	}
	
	/**
	 * Get: dsRazaoSocial.
	 *
	 * @return dsRazaoSocial
	 */
	public String getDsRazaoSocial() {
		return dsRazaoSocial;
	}
	
	/**
	 * Set: dsRazaoSocial.
	 *
	 * @param dsRazaoSocial the ds razao social
	 */
	public void setDsRazaoSocial(String dsRazaoSocial) {
		this.dsRazaoSocial = dsRazaoSocial;
	}
	
	/**
	 * Get: dsResumoProdutoServico.
	 *
	 * @return dsResumoProdutoServico
	 */
	public String getDsResumoProdutoServico() {
		return dsResumoProdutoServico;
	}
	
	/**
	 * Set: dsResumoProdutoServico.
	 *
	 * @param dsResumoProdutoServico the ds resumo produto servico
	 */
	public void setDsResumoProdutoServico(String dsResumoProdutoServico) {
		this.dsResumoProdutoServico = dsResumoProdutoServico;
	}
	
	/**
	 * Get: dsResumoServicoRelacionado.
	 *
	 * @return dsResumoServicoRelacionado
	 */
	public String getDsResumoServicoRelacionado() {
		return dsResumoServicoRelacionado;
	}
	
	/**
	 * Set: dsResumoServicoRelacionado.
	 *
	 * @param dsResumoServicoRelacionado the ds resumo servico relacionado
	 */
	public void setDsResumoServicoRelacionado(String dsResumoServicoRelacionado) {
		this.dsResumoServicoRelacionado = dsResumoServicoRelacionado;
	}
	
	/**
	 * Get: dsSituacaoSolicitacao.
	 *
	 * @return dsSituacaoSolicitacao
	 */
	public String getDsSituacaoSolicitacao() {
		return dsSituacaoSolicitacao;
	}
	
	/**
	 * Set: dsSituacaoSolicitacao.
	 *
	 * @param dsSituacaoSolicitacao the ds situacao solicitacao
	 */
	public void setDsSituacaoSolicitacao(String dsSituacaoSolicitacao) {
		this.dsSituacaoSolicitacao = dsSituacaoSolicitacao;
	}
	
	/**
	 * Get: nrSequenciaContratoNegocio.
	 *
	 * @return nrSequenciaContratoNegocio
	 */
	public Long getNrSequenciaContratoNegocio() {
		return nrSequenciaContratoNegocio;
	}
	
	/**
	 * Set: nrSequenciaContratoNegocio.
	 *
	 * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
	 */
	public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}
	
	/**
	 * Get: nrSolicitacaoPagamento.
	 *
	 * @return nrSolicitacaoPagamento
	 */
	public Integer getNrSolicitacaoPagamento() {
		return nrSolicitacaoPagamento;
	}
	
	/**
	 * Set: nrSolicitacaoPagamento.
	 *
	 * @param nrSolicitacaoPagamento the nr solicitacao pagamento
	 */
	public void setNrSolicitacaoPagamento(Integer nrSolicitacaoPagamento) {
		this.nrSolicitacaoPagamento = nrSolicitacaoPagamento;
	}
	
	/**
	 * Get: dtRecuperacao.
	 *
	 * @return dtRecuperacao
	 */
	public String getDtRecuperacao() {
		return dtRecuperacao;
	}
	
	/**
	 * Set: dtRecuperacao.
	 *
	 * @param dtRecuperacao the dt recuperacao
	 */
	public void setDtRecuperacao(String dtRecuperacao) {
		this.dtRecuperacao = dtRecuperacao;
	}
	 
 	/**
 	 * Get: dsHoraSolicitacao.
 	 *
 	 * @return dsHoraSolicitacao
 	 */
 	public String getDsHoraSolicitacao() {
		return dsHoraSolicitacao;
	}
	
	/**
	 * Set: dsHoraSolicitacao.
	 *
	 * @param dsHoraSolicitacao the ds hora solicitacao
	 */
	public void setDsHoraSolicitacao(String dsHoraSolicitacao) {
		this.dsHoraSolicitacao = dsHoraSolicitacao;
	}
	    
}
