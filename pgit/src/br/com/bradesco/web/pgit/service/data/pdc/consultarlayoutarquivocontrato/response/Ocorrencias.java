/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultarlayoutarquivocontrato.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.math.BigDecimal;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdMeioPrincipalRemessa
     */
    private int _cdMeioPrincipalRemessa = 0;

    /**
     * keeps track of state for field: _cdMeioPrincipalRemessa
     */
    private boolean _has_cdMeioPrincipalRemessa;

    /**
     * Field _dsMeioPrincipalRemessa
     */
    private java.lang.String _dsMeioPrincipalRemessa;

    /**
     * Field _cdMeioAlternaticoRemessa
     */
    private int _cdMeioAlternaticoRemessa = 0;

    /**
     * keeps track of state for field: _cdMeioAlternaticoRemessa
     */
    private boolean _has_cdMeioAlternaticoRemessa;

    /**
     * Field _dsMeioAlternaticoRemessa
     */
    private java.lang.String _dsMeioAlternaticoRemessa;

    /**
     * Field _cdMeioPrincipalRetorno
     */
    private int _cdMeioPrincipalRetorno = 0;

    /**
     * keeps track of state for field: _cdMeioPrincipalRetorno
     */
    private boolean _has_cdMeioPrincipalRetorno;

    /**
     * Field _dsMeioPrincipalRetorno
     */
    private java.lang.String _dsMeioPrincipalRetorno;

    /**
     * Field _cdMeioAlternativoRetorno
     */
    private int _cdMeioAlternativoRetorno = 0;

    /**
     * keeps track of state for field: _cdMeioAlternativoRetorno
     */
    private boolean _has_cdMeioAlternativoRetorno;

    /**
     * Field _dsMeioAlternativoRetorno
     */
    private java.lang.String _dsMeioAlternativoRetorno;

    /**
     * Field _cdPerdcContagemRemessa
     */
    private int _cdPerdcContagemRemessa = 0;

    /**
     * keeps track of state for field: _cdPerdcContagemRemessa
     */
    private boolean _has_cdPerdcContagemRemessa;

    /**
     * Field _dsPerdcContagemRemessa
     */
    private java.lang.String _dsPerdcContagemRemessa;

    /**
     * Field _cdAplicacaoTransmissaoArquivo
     */
    private int _cdAplicacaoTransmissaoArquivo = 0;

    /**
     * keeps track of state for field: _cdAplicacaoTransmissaoArquiv
     */
    private boolean _has_cdAplicacaoTransmissaoArquivo;

    /**
     * Field _dsAplicacaoTransmissaoArquivo
     */
    private java.lang.String _dsAplicacaoTransmissaoArquivo;

    /**
     * Field _cdSistema
     */
    private java.lang.String _cdSistema;

    /**
     * Field _dsEmpresaTratoArquivo
     */
    private java.lang.String _dsEmpresaTratoArquivo;

    /**
     * Field _dsNomeArquivoRemessa
     */
    private java.lang.String _dsNomeArquivoRemessa;

    /**
     * Field _dsNomeArquivoRetorno
     */
    private java.lang.String _dsNomeArquivoRetorno;

    /**
     * Field _dsNomeCliente
     */
    private java.lang.String _dsNomeCliente;

    /**
     * Field _cdNivelControleRemessa
     */
    private int _cdNivelControleRemessa = 0;

    /**
     * keeps track of state for field: _cdNivelControleRemessa
     */
    private boolean _has_cdNivelControleRemessa;

    /**
     * Field _dsNivelControleRemessa
     */
    private java.lang.String _dsNivelControleRemessa;

    /**
     * Field _cdRejeicaoAcolhimentoRemessa
     */
    private int _cdRejeicaoAcolhimentoRemessa = 0;

    /**
     * keeps track of state for field: _cdRejeicaoAcolhimentoRemessa
     */
    private boolean _has_cdRejeicaoAcolhimentoRemessa;

    /**
     * Field _dsRejeicaoAcolhimentoRemessa
     */
    private java.lang.String _dsRejeicaoAcolhimentoRemessa;

    /**
     * Field _cdPercentualRejeicaoRemessa
     */
    private java.math.BigDecimal _cdPercentualRejeicaoRemessa = new java.math.BigDecimal("0");

    /**
     * Field _nrMaximoContagemRemessa
     */
    private java.lang.String _nrMaximoContagemRemessa;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
        setCdPercentualRejeicaoRemessa(new java.math.BigDecimal("0"));
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarlayoutarquivocontrato.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdAplicacaoTransmissaoArquivo
     * 
     */
    public void deleteCdAplicacaoTransmissaoArquivo()
    {
        this._has_cdAplicacaoTransmissaoArquivo= false;
    } //-- void deleteCdAplicacaoTransmissaoArquivo() 

    /**
     * Method deleteCdMeioAlternaticoRemessa
     * 
     */
    public void deleteCdMeioAlternaticoRemessa()
    {
        this._has_cdMeioAlternaticoRemessa= false;
    } //-- void deleteCdMeioAlternaticoRemessa() 

    /**
     * Method deleteCdMeioAlternativoRetorno
     * 
     */
    public void deleteCdMeioAlternativoRetorno()
    {
        this._has_cdMeioAlternativoRetorno= false;
    } //-- void deleteCdMeioAlternativoRetorno() 

    /**
     * Method deleteCdMeioPrincipalRemessa
     * 
     */
    public void deleteCdMeioPrincipalRemessa()
    {
        this._has_cdMeioPrincipalRemessa= false;
    } //-- void deleteCdMeioPrincipalRemessa() 

    /**
     * Method deleteCdMeioPrincipalRetorno
     * 
     */
    public void deleteCdMeioPrincipalRetorno()
    {
        this._has_cdMeioPrincipalRetorno= false;
    } //-- void deleteCdMeioPrincipalRetorno() 

    /**
     * Method deleteCdNivelControleRemessa
     * 
     */
    public void deleteCdNivelControleRemessa()
    {
        this._has_cdNivelControleRemessa= false;
    } //-- void deleteCdNivelControleRemessa() 

    /**
     * Method deleteCdPerdcContagemRemessa
     * 
     */
    public void deleteCdPerdcContagemRemessa()
    {
        this._has_cdPerdcContagemRemessa= false;
    } //-- void deleteCdPerdcContagemRemessa() 

    /**
     * Method deleteCdRejeicaoAcolhimentoRemessa
     * 
     */
    public void deleteCdRejeicaoAcolhimentoRemessa()
    {
        this._has_cdRejeicaoAcolhimentoRemessa= false;
    } //-- void deleteCdRejeicaoAcolhimentoRemessa() 

    /**
     * Returns the value of field 'cdAplicacaoTransmissaoArquivo'.
     * 
     * @return int
     * @return the value of field 'cdAplicacaoTransmissaoArquivo'.
     */
    public int getCdAplicacaoTransmissaoArquivo()
    {
        return this._cdAplicacaoTransmissaoArquivo;
    } //-- int getCdAplicacaoTransmissaoArquivo() 

    /**
     * Returns the value of field 'cdMeioAlternaticoRemessa'.
     * 
     * @return int
     * @return the value of field 'cdMeioAlternaticoRemessa'.
     */
    public int getCdMeioAlternaticoRemessa()
    {
        return this._cdMeioAlternaticoRemessa;
    } //-- int getCdMeioAlternaticoRemessa() 

    /**
     * Returns the value of field 'cdMeioAlternativoRetorno'.
     * 
     * @return int
     * @return the value of field 'cdMeioAlternativoRetorno'.
     */
    public int getCdMeioAlternativoRetorno()
    {
        return this._cdMeioAlternativoRetorno;
    } //-- int getCdMeioAlternativoRetorno() 

    /**
     * Returns the value of field 'cdMeioPrincipalRemessa'.
     * 
     * @return int
     * @return the value of field 'cdMeioPrincipalRemessa'.
     */
    public int getCdMeioPrincipalRemessa()
    {
        return this._cdMeioPrincipalRemessa;
    } //-- int getCdMeioPrincipalRemessa() 

    /**
     * Returns the value of field 'cdMeioPrincipalRetorno'.
     * 
     * @return int
     * @return the value of field 'cdMeioPrincipalRetorno'.
     */
    public int getCdMeioPrincipalRetorno()
    {
        return this._cdMeioPrincipalRetorno;
    } //-- int getCdMeioPrincipalRetorno() 

    /**
     * Returns the value of field 'cdNivelControleRemessa'.
     * 
     * @return int
     * @return the value of field 'cdNivelControleRemessa'.
     */
    public int getCdNivelControleRemessa()
    {
        return this._cdNivelControleRemessa;
    } //-- int getCdNivelControleRemessa() 

    /**
     * Returns the value of field 'cdPercentualRejeicaoRemessa'.
     * 
     * @return BigDecimal
     * @return the value of field 'cdPercentualRejeicaoRemessa'.
     */
    public java.math.BigDecimal getCdPercentualRejeicaoRemessa()
    {
        return this._cdPercentualRejeicaoRemessa;
    } //-- java.math.BigDecimal getCdPercentualRejeicaoRemessa() 

    /**
     * Returns the value of field 'cdPerdcContagemRemessa'.
     * 
     * @return int
     * @return the value of field 'cdPerdcContagemRemessa'.
     */
    public int getCdPerdcContagemRemessa()
    {
        return this._cdPerdcContagemRemessa;
    } //-- int getCdPerdcContagemRemessa() 

    /**
     * Returns the value of field 'cdRejeicaoAcolhimentoRemessa'.
     * 
     * @return int
     * @return the value of field 'cdRejeicaoAcolhimentoRemessa'.
     */
    public int getCdRejeicaoAcolhimentoRemessa()
    {
        return this._cdRejeicaoAcolhimentoRemessa;
    } //-- int getCdRejeicaoAcolhimentoRemessa() 

    /**
     * Returns the value of field 'cdSistema'.
     * 
     * @return String
     * @return the value of field 'cdSistema'.
     */
    public java.lang.String getCdSistema()
    {
        return this._cdSistema;
    } //-- java.lang.String getCdSistema() 

    /**
     * Returns the value of field 'dsAplicacaoTransmissaoArquivo'.
     * 
     * @return String
     * @return the value of field 'dsAplicacaoTransmissaoArquivo'.
     */
    public java.lang.String getDsAplicacaoTransmissaoArquivo()
    {
        return this._dsAplicacaoTransmissaoArquivo;
    } //-- java.lang.String getDsAplicacaoTransmissaoArquivo() 

    /**
     * Returns the value of field 'dsEmpresaTratoArquivo'.
     * 
     * @return String
     * @return the value of field 'dsEmpresaTratoArquivo'.
     */
    public java.lang.String getDsEmpresaTratoArquivo()
    {
        return this._dsEmpresaTratoArquivo;
    } //-- java.lang.String getDsEmpresaTratoArquivo() 

    /**
     * Returns the value of field 'dsMeioAlternaticoRemessa'.
     * 
     * @return String
     * @return the value of field 'dsMeioAlternaticoRemessa'.
     */
    public java.lang.String getDsMeioAlternaticoRemessa()
    {
        return this._dsMeioAlternaticoRemessa;
    } //-- java.lang.String getDsMeioAlternaticoRemessa() 

    /**
     * Returns the value of field 'dsMeioAlternativoRetorno'.
     * 
     * @return String
     * @return the value of field 'dsMeioAlternativoRetorno'.
     */
    public java.lang.String getDsMeioAlternativoRetorno()
    {
        return this._dsMeioAlternativoRetorno;
    } //-- java.lang.String getDsMeioAlternativoRetorno() 

    /**
     * Returns the value of field 'dsMeioPrincipalRemessa'.
     * 
     * @return String
     * @return the value of field 'dsMeioPrincipalRemessa'.
     */
    public java.lang.String getDsMeioPrincipalRemessa()
    {
        return this._dsMeioPrincipalRemessa;
    } //-- java.lang.String getDsMeioPrincipalRemessa() 

    /**
     * Returns the value of field 'dsMeioPrincipalRetorno'.
     * 
     * @return String
     * @return the value of field 'dsMeioPrincipalRetorno'.
     */
    public java.lang.String getDsMeioPrincipalRetorno()
    {
        return this._dsMeioPrincipalRetorno;
    } //-- java.lang.String getDsMeioPrincipalRetorno() 

    /**
     * Returns the value of field 'dsNivelControleRemessa'.
     * 
     * @return String
     * @return the value of field 'dsNivelControleRemessa'.
     */
    public java.lang.String getDsNivelControleRemessa()
    {
        return this._dsNivelControleRemessa;
    } //-- java.lang.String getDsNivelControleRemessa() 

    /**
     * Returns the value of field 'dsNomeArquivoRemessa'.
     * 
     * @return String
     * @return the value of field 'dsNomeArquivoRemessa'.
     */
    public java.lang.String getDsNomeArquivoRemessa()
    {
        return this._dsNomeArquivoRemessa;
    } //-- java.lang.String getDsNomeArquivoRemessa() 

    /**
     * Returns the value of field 'dsNomeArquivoRetorno'.
     * 
     * @return String
     * @return the value of field 'dsNomeArquivoRetorno'.
     */
    public java.lang.String getDsNomeArquivoRetorno()
    {
        return this._dsNomeArquivoRetorno;
    } //-- java.lang.String getDsNomeArquivoRetorno() 

    /**
     * Returns the value of field 'dsNomeCliente'.
     * 
     * @return String
     * @return the value of field 'dsNomeCliente'.
     */
    public java.lang.String getDsNomeCliente()
    {
        return this._dsNomeCliente;
    } //-- java.lang.String getDsNomeCliente() 

    /**
     * Returns the value of field 'dsPerdcContagemRemessa'.
     * 
     * @return String
     * @return the value of field 'dsPerdcContagemRemessa'.
     */
    public java.lang.String getDsPerdcContagemRemessa()
    {
        return this._dsPerdcContagemRemessa;
    } //-- java.lang.String getDsPerdcContagemRemessa() 

    /**
     * Returns the value of field 'dsRejeicaoAcolhimentoRemessa'.
     * 
     * @return String
     * @return the value of field 'dsRejeicaoAcolhimentoRemessa'.
     */
    public java.lang.String getDsRejeicaoAcolhimentoRemessa()
    {
        return this._dsRejeicaoAcolhimentoRemessa;
    } //-- java.lang.String getDsRejeicaoAcolhimentoRemessa() 

    /**
     * Returns the value of field 'nrMaximoContagemRemessa'.
     * 
     * @return String
     * @return the value of field 'nrMaximoContagemRemessa'.
     */
    public java.lang.String getNrMaximoContagemRemessa()
    {
        return this._nrMaximoContagemRemessa;
    } //-- java.lang.String getNrMaximoContagemRemessa() 

    /**
     * Method hasCdAplicacaoTransmissaoArquivo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAplicacaoTransmissaoArquivo()
    {
        return this._has_cdAplicacaoTransmissaoArquivo;
    } //-- boolean hasCdAplicacaoTransmissaoArquivo() 

    /**
     * Method hasCdMeioAlternaticoRemessa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMeioAlternaticoRemessa()
    {
        return this._has_cdMeioAlternaticoRemessa;
    } //-- boolean hasCdMeioAlternaticoRemessa() 

    /**
     * Method hasCdMeioAlternativoRetorno
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMeioAlternativoRetorno()
    {
        return this._has_cdMeioAlternativoRetorno;
    } //-- boolean hasCdMeioAlternativoRetorno() 

    /**
     * Method hasCdMeioPrincipalRemessa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMeioPrincipalRemessa()
    {
        return this._has_cdMeioPrincipalRemessa;
    } //-- boolean hasCdMeioPrincipalRemessa() 

    /**
     * Method hasCdMeioPrincipalRetorno
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMeioPrincipalRetorno()
    {
        return this._has_cdMeioPrincipalRetorno;
    } //-- boolean hasCdMeioPrincipalRetorno() 

    /**
     * Method hasCdNivelControleRemessa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdNivelControleRemessa()
    {
        return this._has_cdNivelControleRemessa;
    } //-- boolean hasCdNivelControleRemessa() 

    /**
     * Method hasCdPerdcContagemRemessa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPerdcContagemRemessa()
    {
        return this._has_cdPerdcContagemRemessa;
    } //-- boolean hasCdPerdcContagemRemessa() 

    /**
     * Method hasCdRejeicaoAcolhimentoRemessa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdRejeicaoAcolhimentoRemessa()
    {
        return this._has_cdRejeicaoAcolhimentoRemessa;
    } //-- boolean hasCdRejeicaoAcolhimentoRemessa() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdAplicacaoTransmissaoArquivo'.
     * 
     * @param cdAplicacaoTransmissaoArquivo the value of field
     * 'cdAplicacaoTransmissaoArquivo'.
     */
    public void setCdAplicacaoTransmissaoArquivo(int cdAplicacaoTransmissaoArquivo)
    {
        this._cdAplicacaoTransmissaoArquivo = cdAplicacaoTransmissaoArquivo;
        this._has_cdAplicacaoTransmissaoArquivo = true;
    } //-- void setCdAplicacaoTransmissaoArquivo(int) 

    /**
     * Sets the value of field 'cdMeioAlternaticoRemessa'.
     * 
     * @param cdMeioAlternaticoRemessa the value of field
     * 'cdMeioAlternaticoRemessa'.
     */
    public void setCdMeioAlternaticoRemessa(int cdMeioAlternaticoRemessa)
    {
        this._cdMeioAlternaticoRemessa = cdMeioAlternaticoRemessa;
        this._has_cdMeioAlternaticoRemessa = true;
    } //-- void setCdMeioAlternaticoRemessa(int) 

    /**
     * Sets the value of field 'cdMeioAlternativoRetorno'.
     * 
     * @param cdMeioAlternativoRetorno the value of field
     * 'cdMeioAlternativoRetorno'.
     */
    public void setCdMeioAlternativoRetorno(int cdMeioAlternativoRetorno)
    {
        this._cdMeioAlternativoRetorno = cdMeioAlternativoRetorno;
        this._has_cdMeioAlternativoRetorno = true;
    } //-- void setCdMeioAlternativoRetorno(int) 

    /**
     * Sets the value of field 'cdMeioPrincipalRemessa'.
     * 
     * @param cdMeioPrincipalRemessa the value of field
     * 'cdMeioPrincipalRemessa'.
     */
    public void setCdMeioPrincipalRemessa(int cdMeioPrincipalRemessa)
    {
        this._cdMeioPrincipalRemessa = cdMeioPrincipalRemessa;
        this._has_cdMeioPrincipalRemessa = true;
    } //-- void setCdMeioPrincipalRemessa(int) 

    /**
     * Sets the value of field 'cdMeioPrincipalRetorno'.
     * 
     * @param cdMeioPrincipalRetorno the value of field
     * 'cdMeioPrincipalRetorno'.
     */
    public void setCdMeioPrincipalRetorno(int cdMeioPrincipalRetorno)
    {
        this._cdMeioPrincipalRetorno = cdMeioPrincipalRetorno;
        this._has_cdMeioPrincipalRetorno = true;
    } //-- void setCdMeioPrincipalRetorno(int) 

    /**
     * Sets the value of field 'cdNivelControleRemessa'.
     * 
     * @param cdNivelControleRemessa the value of field
     * 'cdNivelControleRemessa'.
     */
    public void setCdNivelControleRemessa(int cdNivelControleRemessa)
    {
        this._cdNivelControleRemessa = cdNivelControleRemessa;
        this._has_cdNivelControleRemessa = true;
    } //-- void setCdNivelControleRemessa(int) 

    /**
     * Sets the value of field 'cdPercentualRejeicaoRemessa'.
     * 
     * @param cdPercentualRejeicaoRemessa the value of field
     * 'cdPercentualRejeicaoRemessa'.
     */
    public void setCdPercentualRejeicaoRemessa(java.math.BigDecimal cdPercentualRejeicaoRemessa)
    {
        this._cdPercentualRejeicaoRemessa = cdPercentualRejeicaoRemessa;
    } //-- void setCdPercentualRejeicaoRemessa(java.math.BigDecimal) 

    /**
     * Sets the value of field 'cdPerdcContagemRemessa'.
     * 
     * @param cdPerdcContagemRemessa the value of field
     * 'cdPerdcContagemRemessa'.
     */
    public void setCdPerdcContagemRemessa(int cdPerdcContagemRemessa)
    {
        this._cdPerdcContagemRemessa = cdPerdcContagemRemessa;
        this._has_cdPerdcContagemRemessa = true;
    } //-- void setCdPerdcContagemRemessa(int) 

    /**
     * Sets the value of field 'cdRejeicaoAcolhimentoRemessa'.
     * 
     * @param cdRejeicaoAcolhimentoRemessa the value of field
     * 'cdRejeicaoAcolhimentoRemessa'.
     */
    public void setCdRejeicaoAcolhimentoRemessa(int cdRejeicaoAcolhimentoRemessa)
    {
        this._cdRejeicaoAcolhimentoRemessa = cdRejeicaoAcolhimentoRemessa;
        this._has_cdRejeicaoAcolhimentoRemessa = true;
    } //-- void setCdRejeicaoAcolhimentoRemessa(int) 

    /**
     * Sets the value of field 'cdSistema'.
     * 
     * @param cdSistema the value of field 'cdSistema'.
     */
    public void setCdSistema(java.lang.String cdSistema)
    {
        this._cdSistema = cdSistema;
    } //-- void setCdSistema(java.lang.String) 

    /**
     * Sets the value of field 'dsAplicacaoTransmissaoArquivo'.
     * 
     * @param dsAplicacaoTransmissaoArquivo the value of field
     * 'dsAplicacaoTransmissaoArquivo'.
     */
    public void setDsAplicacaoTransmissaoArquivo(java.lang.String dsAplicacaoTransmissaoArquivo)
    {
        this._dsAplicacaoTransmissaoArquivo = dsAplicacaoTransmissaoArquivo;
    } //-- void setDsAplicacaoTransmissaoArquivo(java.lang.String) 

    /**
     * Sets the value of field 'dsEmpresaTratoArquivo'.
     * 
     * @param dsEmpresaTratoArquivo the value of field
     * 'dsEmpresaTratoArquivo'.
     */
    public void setDsEmpresaTratoArquivo(java.lang.String dsEmpresaTratoArquivo)
    {
        this._dsEmpresaTratoArquivo = dsEmpresaTratoArquivo;
    } //-- void setDsEmpresaTratoArquivo(java.lang.String) 

    /**
     * Sets the value of field 'dsMeioAlternaticoRemessa'.
     * 
     * @param dsMeioAlternaticoRemessa the value of field
     * 'dsMeioAlternaticoRemessa'.
     */
    public void setDsMeioAlternaticoRemessa(java.lang.String dsMeioAlternaticoRemessa)
    {
        this._dsMeioAlternaticoRemessa = dsMeioAlternaticoRemessa;
    } //-- void setDsMeioAlternaticoRemessa(java.lang.String) 

    /**
     * Sets the value of field 'dsMeioAlternativoRetorno'.
     * 
     * @param dsMeioAlternativoRetorno the value of field
     * 'dsMeioAlternativoRetorno'.
     */
    public void setDsMeioAlternativoRetorno(java.lang.String dsMeioAlternativoRetorno)
    {
        this._dsMeioAlternativoRetorno = dsMeioAlternativoRetorno;
    } //-- void setDsMeioAlternativoRetorno(java.lang.String) 

    /**
     * Sets the value of field 'dsMeioPrincipalRemessa'.
     * 
     * @param dsMeioPrincipalRemessa the value of field
     * 'dsMeioPrincipalRemessa'.
     */
    public void setDsMeioPrincipalRemessa(java.lang.String dsMeioPrincipalRemessa)
    {
        this._dsMeioPrincipalRemessa = dsMeioPrincipalRemessa;
    } //-- void setDsMeioPrincipalRemessa(java.lang.String) 

    /**
     * Sets the value of field 'dsMeioPrincipalRetorno'.
     * 
     * @param dsMeioPrincipalRetorno the value of field
     * 'dsMeioPrincipalRetorno'.
     */
    public void setDsMeioPrincipalRetorno(java.lang.String dsMeioPrincipalRetorno)
    {
        this._dsMeioPrincipalRetorno = dsMeioPrincipalRetorno;
    } //-- void setDsMeioPrincipalRetorno(java.lang.String) 

    /**
     * Sets the value of field 'dsNivelControleRemessa'.
     * 
     * @param dsNivelControleRemessa the value of field
     * 'dsNivelControleRemessa'.
     */
    public void setDsNivelControleRemessa(java.lang.String dsNivelControleRemessa)
    {
        this._dsNivelControleRemessa = dsNivelControleRemessa;
    } //-- void setDsNivelControleRemessa(java.lang.String) 

    /**
     * Sets the value of field 'dsNomeArquivoRemessa'.
     * 
     * @param dsNomeArquivoRemessa the value of field
     * 'dsNomeArquivoRemessa'.
     */
    public void setDsNomeArquivoRemessa(java.lang.String dsNomeArquivoRemessa)
    {
        this._dsNomeArquivoRemessa = dsNomeArquivoRemessa;
    } //-- void setDsNomeArquivoRemessa(java.lang.String) 

    /**
     * Sets the value of field 'dsNomeArquivoRetorno'.
     * 
     * @param dsNomeArquivoRetorno the value of field
     * 'dsNomeArquivoRetorno'.
     */
    public void setDsNomeArquivoRetorno(java.lang.String dsNomeArquivoRetorno)
    {
        this._dsNomeArquivoRetorno = dsNomeArquivoRetorno;
    } //-- void setDsNomeArquivoRetorno(java.lang.String) 

    /**
     * Sets the value of field 'dsNomeCliente'.
     * 
     * @param dsNomeCliente the value of field 'dsNomeCliente'.
     */
    public void setDsNomeCliente(java.lang.String dsNomeCliente)
    {
        this._dsNomeCliente = dsNomeCliente;
    } //-- void setDsNomeCliente(java.lang.String) 

    /**
     * Sets the value of field 'dsPerdcContagemRemessa'.
     * 
     * @param dsPerdcContagemRemessa the value of field
     * 'dsPerdcContagemRemessa'.
     */
    public void setDsPerdcContagemRemessa(java.lang.String dsPerdcContagemRemessa)
    {
        this._dsPerdcContagemRemessa = dsPerdcContagemRemessa;
    } //-- void setDsPerdcContagemRemessa(java.lang.String) 

    /**
     * Sets the value of field 'dsRejeicaoAcolhimentoRemessa'.
     * 
     * @param dsRejeicaoAcolhimentoRemessa the value of field
     * 'dsRejeicaoAcolhimentoRemessa'.
     */
    public void setDsRejeicaoAcolhimentoRemessa(java.lang.String dsRejeicaoAcolhimentoRemessa)
    {
        this._dsRejeicaoAcolhimentoRemessa = dsRejeicaoAcolhimentoRemessa;
    } //-- void setDsRejeicaoAcolhimentoRemessa(java.lang.String) 

    /**
     * Sets the value of field 'nrMaximoContagemRemessa'.
     * 
     * @param nrMaximoContagemRemessa the value of field
     * 'nrMaximoContagemRemessa'.
     */
    public void setNrMaximoContagemRemessa(java.lang.String nrMaximoContagemRemessa)
    {
        this._nrMaximoContagemRemessa = nrMaximoContagemRemessa;
    } //-- void setNrMaximoContagemRemessa(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultarlayoutarquivocontrato.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultarlayoutarquivocontrato.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultarlayoutarquivocontrato.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarlayoutarquivocontrato.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
