/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/interfaz.ftl,v $
 * $Id: interfaz.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: interfaz.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.assoclayoutarqprodserv;

import java.util.List;

import br.com.bradesco.web.pgit.service.business.assoclayoutarqprodserv.bean.AlterarArquivoServicoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.assoclayoutarqprodserv.bean.AlterarArquivoServicoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.assoclayoutarqprodserv.bean.DetalharAssLayoutArqProdServicoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.assoclayoutarqprodserv.bean.DetalharAssLayoutArqProdServicoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.assoclayoutarqprodserv.bean.ExcluirAssLayoutArqProdServicoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.assoclayoutarqprodserv.bean.ExcluirAssLayoutArqProdServicoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.assoclayoutarqprodserv.bean.IncluirAssLayoutArqProdServicoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.assoclayoutarqprodserv.bean.IncluirAssLayoutArqProdServicoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.assoclayoutarqprodserv.bean.ListarAssLayoutArqProdServicoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.assoclayoutarqprodserv.bean.ListarAssLayoutArqProdServicoSaidaDTO;

/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Interface do adaptador: AssocLayoutArqProdServ
 * </p>
 * 
 * @comment CODIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public interface IAssocLayoutArqProdServService {

    
	/**
	 * Listar associcao layout arq produto servico.
	 *
	 * @param listarAssLayoutArqProdServicoEntradaDTO the listar ass layout arq prod servico entrada dto
	 * @return the list< listar ass layout arq prod servico saida dt o>
	 */
	List<ListarAssLayoutArqProdServicoSaidaDTO> listarAssocicaoLayoutArqProdutoServico(ListarAssLayoutArqProdServicoEntradaDTO listarAssLayoutArqProdServicoEntradaDTO);
	
	/**
	 * Incluir associcao layout arq produto servico.
	 *
	 * @param incluirAssLayoutArqProdServicoEntradaDTO the incluir ass layout arq prod servico entrada dto
	 * @return the incluir ass layout arq prod servico saida dto
	 */
	IncluirAssLayoutArqProdServicoSaidaDTO incluirAssocicaoLayoutArqProdutoServico(IncluirAssLayoutArqProdServicoEntradaDTO incluirAssLayoutArqProdServicoEntradaDTO);
	
	/**
	 * Excluir associcao layout arq produto servico.
	 *
	 * @param excluirAssLayoutArqProdServicoEntradaDTO the excluir ass layout arq prod servico entrada dto
	 * @return the excluir ass layout arq prod servico saida dto
	 */
	ExcluirAssLayoutArqProdServicoSaidaDTO excluirAssocicaoLayoutArqProdutoServico(ExcluirAssLayoutArqProdServicoEntradaDTO excluirAssLayoutArqProdServicoEntradaDTO);
	
	/**
	 * Detalhar associcao layout arq produto servico.
	 *
	 * @param detalharAssLayoutArqProdServicoEntradaDTO the detalhar ass layout arq prod servico entrada dto
	 * @return the detalhar ass layout arq prod servico saida dto
	 */
	DetalharAssLayoutArqProdServicoSaidaDTO detalharAssocicaoLayoutArqProdutoServico(DetalharAssLayoutArqProdServicoEntradaDTO detalharAssLayoutArqProdServicoEntradaDTO);

	/**
	 * Alterar associcao layout arq produto servico.
	 *
	 * @param AlterarArquivoServicoEntradaDTO the alterar ass layout arq prod servico entrada dto
	 * @return the alterar ass layout arq prod servico saida dto
	 */
	AlterarArquivoServicoSaidaDTO alterarArquivoServico(AlterarArquivoServicoEntradaDTO entrada);
}

