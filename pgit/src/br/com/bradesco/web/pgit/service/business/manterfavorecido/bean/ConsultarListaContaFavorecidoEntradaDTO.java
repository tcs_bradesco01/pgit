/*
 * Nome: br.com.bradesco.web.pgit.service.business.manterfavorecido.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.manterfavorecido.bean;

/**
 * Nome: ConsultarListaContaFavorecidoEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarListaContaFavorecidoEntradaDTO {

	/** Atributo cdPessoaJuridContrato. */
	private Long  cdPessoaJuridContrato;
	
	/** Atributo cdTipoContratoNegocio. */
	private Integer  cdTipoContratoNegocio;
	
	/** Atributo nrSeqContratoNegocio. */
	private Long  nrSeqContratoNegocio;
	
	/** Atributo cdFavorecidoClientePagador. */
	private Long  cdFavorecidoClientePagador;
	
	/**
	 * Consultar lista conta favorecido entrada dto.
	 */
	public ConsultarListaContaFavorecidoEntradaDTO() {
		super();
	}

	/**
	 * Consultar lista conta favorecido entrada dto.
	 *
	 * @param cdPessoaJuridContrato the cd pessoa jurid contrato
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 * @param nrSeqContratoNegocio the nr seq contrato negocio
	 * @param cdFavorecidoClientePagador the cd favorecido cliente pagador
	 */
	public ConsultarListaContaFavorecidoEntradaDTO(Long cdPessoaJuridContrato, Integer cdTipoContratoNegocio, Long nrSeqContratoNegocio, Long cdFavorecidoClientePagador) {
		super();
		this.cdPessoaJuridContrato = cdPessoaJuridContrato;
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
		this.nrSeqContratoNegocio = nrSeqContratoNegocio;
		this.cdFavorecidoClientePagador = cdFavorecidoClientePagador;
	}

	/**
	 * Get: cdFavorecidoClientePagador.
	 *
	 * @return cdFavorecidoClientePagador
	 */
	public Long getCdFavorecidoClientePagador() {
		return cdFavorecidoClientePagador;
	}

	/**
	 * Set: cdFavorecidoClientePagador.
	 *
	 * @param cdFavorecidoClientePagador the cd favorecido cliente pagador
	 */
	public void setCdFavorecidoClientePagador(Long cdFavorecidoClientePagador) {
		this.cdFavorecidoClientePagador = cdFavorecidoClientePagador;
	}

	/**
	 * Get: cdPessoaJuridContrato.
	 *
	 * @return cdPessoaJuridContrato
	 */
	public Long getCdPessoaJuridContrato() {
		return cdPessoaJuridContrato;
	}

	/**
	 * Set: cdPessoaJuridContrato.
	 *
	 * @param cdPessoaJuridContrato the cd pessoa jurid contrato
	 */
	public void setCdPessoaJuridContrato(Long cdPessoaJuridContrato) {
		this.cdPessoaJuridContrato = cdPessoaJuridContrato;
	}

	/**
	 * Get: cdTipoContratoNegocio.
	 *
	 * @return cdTipoContratoNegocio
	 */
	public Integer getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}

	/**
	 * Set: cdTipoContratoNegocio.
	 *
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}

	/**
	 * Get: nrSeqContratoNegocio.
	 *
	 * @return nrSeqContratoNegocio
	 */
	public Long getNrSeqContratoNegocio() {
		return nrSeqContratoNegocio;
	}

	/**
	 * Set: nrSeqContratoNegocio.
	 *
	 * @param nrSeqContratoNegocio the nr seq contrato negocio
	 */
	public void setNrSeqContratoNegocio(Long nrSeqContratoNegocio) {
		this.nrSeqContratoNegocio = nrSeqContratoNegocio;
	}
}
