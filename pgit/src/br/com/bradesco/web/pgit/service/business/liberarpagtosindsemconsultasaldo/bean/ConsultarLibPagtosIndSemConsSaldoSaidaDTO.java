/*
 * Nome: br.com.bradesco.web.pgit.service.business.liberarpagtosindsemconsultasaldo.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.liberarpagtosindsemconsultasaldo.bean;

import java.math.BigDecimal;

/**
 * Nome: ConsultarLibPagtosIndSemConsSaldoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarLibPagtosIndSemConsSaldoSaidaDTO {

    /** Atributo codMensagem. */
    private String codMensagem;

    /** Atributo mensagem. */
    private String mensagem;

    /** Atributo numeroConsultas. */
    private Integer numeroConsultas;

    /** Atributo nrCnpjCpf. */
    private Long nrCnpjCpf;

    /** Atributo nrFilialCnpjCpf. */
    private Integer nrFilialCnpjCpf;

    /** Atributo nrDigitoCnpjCpf. */
    private Integer nrDigitoCnpjCpf;

    /** Atributo cdPessoaJuridicaContrato. */
    private Long cdPessoaJuridicaContrato;

    /** Atributo dsRazaoSocial. */
    private String dsRazaoSocial;

    /** Atributo cdEmpresa. */
    private Long cdEmpresa;

    /** Atributo dsEmpresa. */
    private String dsEmpresa;

    /** Atributo cdTipoContratoNegocio. */
    private Integer cdTipoContratoNegocio;

    /** Atributo nrContrato. */
    private Long nrContrato;

    /** Atributo cdProdutoServicoOperacao. */
    private Integer cdProdutoServicoOperacao;

    /** Atributo dsResumoProdutoServico. */
    private String dsResumoProdutoServico;

    /** Atributo cdProdutoServicoRelacionado. */
    private Integer cdProdutoServicoRelacionado;

    /** Atributo dsOperacaoProdutoServico. */
    private String dsOperacaoProdutoServico;

    /** Atributo cdControlePagamento. */
    private String cdControlePagamento;

    /** Atributo dtCreditoPagamento. */
    private String dtCreditoPagamento;

    /** Atributo vlEfetivoPagamento. */
    private BigDecimal vlEfetivoPagamento;

    /** Atributo cdInscricaoFavorecido. */
    private String cdInscricaoFavorecido;

    /** Atributo dsFavorecido. */
    private String dsFavorecido;

    /** Atributo cdBancoDebito. */
    private Integer cdBancoDebito;

    /** Atributo cdAgenciaDebito. */
    private Integer cdAgenciaDebito;

    /** Atributo cdDigitoAgenciaDebito. */
    private Integer cdDigitoAgenciaDebito;

    /** Atributo cdContaDebito. */
    private Long cdContaDebito;

    /** Atributo cdDigitoContaDebito. */
    private String cdDigitoContaDebito;

    /** Atributo cdBancoCredito. */
    private Integer cdBancoCredito;

    /** Atributo cdAgenciaCredito. */
    private Integer cdAgenciaCredito;

    /** Atributo cdDigitoAgenciaCredito. */
    private Integer cdDigitoAgenciaCredito;

    /** Atributo cdContaCredito. */
    private Long cdContaCredito;

    /** Atributo cdDigitoContaCredito. */
    private String cdDigitoContaCredito;

    /** Atributo cdSituacaoOperacaoPagamento. */
    private Integer cdSituacaoOperacaoPagamento;

    /** Atributo dsSituacaoOperacaoPagamento. */
    private String dsSituacaoOperacaoPagamento;

    /** Atributo cdMotivoSituacaoPagamento. */
    private Integer cdMotivoSituacaoPagamento;

    /** Atributo dsMotivoSituacaoPagamento. */
    private String dsMotivoSituacaoPagamento;

    /** Atributo cdTipoCanal. */
    private Integer cdTipoCanal;

    /** Atributo cdTipoTela. */
    private Integer cdTipoTela;

    /** Atributo dsEfetivacaoPagamento. */
    private String dsEfetivacaoPagamento;

    /** Atributo check. */
    private boolean check;

    // vari�veis relat�rio
    /** Atributo cpfFormatado. */
    private String cpfFormatado;

    /** Atributo favBeneficiarioFormatado. */
    private String favBeneficiarioFormatado;

    /** Atributo contaDebitoFormatado. */
    private String contaDebitoFormatado;

    /** Atributo contaCreditoFormatado. */
    private String contaCreditoFormatado;

    /** Atributo dtFormatada. */
    private String dtFormatada;

    // vari�veis detalhes
    /** Atributo cpfCnpjFormatado. */
    private String cpfCnpjFormatado;

    /** Atributo bancoAgenciaContaDebitoFormatado. */
    private String bancoAgenciaContaDebitoFormatado;

    /** Atributo bancoAgenciaContaCreditoFormatado. */
    private String bancoAgenciaContaCreditoFormatado;

    /** Atributo bancoCreditoFormatado. */
    private String bancoCreditoFormatado;

    /** Atributo agenciaCreditoFormatada. */
    private String agenciaCreditoFormatada;

    /** Atributo contaCreditoFormatada. */
    private String contaCreditoFormatada;

    /** Atributo dsIndicadorPagamento. */
    private String dsIndicadorPagamento;
    
	private String cdIspbPagtoDestino;
	
	private String contaPagtoDestino;

    /**
     * Get: dtFormatada.
     *
     * @return dtFormatada
     */
    public String getDtFormatada() {
	return dtFormatada;
    }

    /**
     * Set: dtFormatada.
     *
     * @param dtFormatada the dt formatada
     */
    public void setDtFormatada(String dtFormatada) {
	this.dtFormatada = dtFormatada;
    }

    /**
     * Get: favBeneficiarioFormatado.
     *
     * @return favBeneficiarioFormatado
     */
    public String getFavBeneficiarioFormatado() {
	return favBeneficiarioFormatado;
    }

    /**
     * Set: favBeneficiarioFormatado.
     *
     * @param favBeneficiarioFormatado the fav beneficiario formatado
     */
    public void setFavBeneficiarioFormatado(String favBeneficiarioFormatado) {
	this.favBeneficiarioFormatado = favBeneficiarioFormatado;
    }

    /**
     * Get: cpfCnpjFormatado.
     *
     * @return cpfCnpjFormatado
     */
    public String getCpfCnpjFormatado() {
	return cpfCnpjFormatado;
    }

    /**
     * Set: cpfCnpjFormatado.
     *
     * @param cpfCnpjFormatado the cpf cnpj formatado
     */
    public void setCpfCnpjFormatado(String cpfCnpjFormatado) {
	this.cpfCnpjFormatado = cpfCnpjFormatado;
    }

    /**
     * Get: cdAgenciaCredito.
     *
     * @return cdAgenciaCredito
     */
    public Integer getCdAgenciaCredito() {
	return cdAgenciaCredito;
    }

    /**
     * Set: cdAgenciaCredito.
     *
     * @param cdAgenciaCredito the cd agencia credito
     */
    public void setCdAgenciaCredito(Integer cdAgenciaCredito) {
	this.cdAgenciaCredito = cdAgenciaCredito;
    }

    /**
     * Get: cdAgenciaDebito.
     *
     * @return cdAgenciaDebito
     */
    public Integer getCdAgenciaDebito() {
	return cdAgenciaDebito;
    }

    /**
     * Set: cdAgenciaDebito.
     *
     * @param cdAgenciaDebito the cd agencia debito
     */
    public void setCdAgenciaDebito(Integer cdAgenciaDebito) {
	this.cdAgenciaDebito = cdAgenciaDebito;
    }

    /**
     * Get: cdBancoCredito.
     *
     * @return cdBancoCredito
     */
    public Integer getCdBancoCredito() {
	return cdBancoCredito;
    }

    /**
     * Set: cdBancoCredito.
     *
     * @param cdBancoCredito the cd banco credito
     */
    public void setCdBancoCredito(Integer cdBancoCredito) {
	this.cdBancoCredito = cdBancoCredito;
    }

    /**
     * Get: cdBancoDebito.
     *
     * @return cdBancoDebito
     */
    public Integer getCdBancoDebito() {
	return cdBancoDebito;
    }

    /**
     * Set: cdBancoDebito.
     *
     * @param cdBancoDebito the cd banco debito
     */
    public void setCdBancoDebito(Integer cdBancoDebito) {
	this.cdBancoDebito = cdBancoDebito;
    }

    /**
     * Get: cdContaCredito.
     *
     * @return cdContaCredito
     */
    public Long getCdContaCredito() {
	return cdContaCredito;
    }

    /**
     * Set: cdContaCredito.
     *
     * @param cdContaCredito the cd conta credito
     */
    public void setCdContaCredito(Long cdContaCredito) {
	this.cdContaCredito = cdContaCredito;
    }

    /**
     * Get: cdContaDebito.
     *
     * @return cdContaDebito
     */
    public Long getCdContaDebito() {
	return cdContaDebito;
    }

    /**
     * Set: cdContaDebito.
     *
     * @param cdContaDebito the cd conta debito
     */
    public void setCdContaDebito(Long cdContaDebito) {
	this.cdContaDebito = cdContaDebito;
    }

    /**
     * Get: cdControlePagamento.
     *
     * @return cdControlePagamento
     */
    public String getCdControlePagamento() {
	return cdControlePagamento;
    }

    /**
     * Set: cdControlePagamento.
     *
     * @param cdControlePagamento the cd controle pagamento
     */
    public void setCdControlePagamento(String cdControlePagamento) {
	this.cdControlePagamento = cdControlePagamento;
    }

    /**
     * Get: cdDigitoAgenciaCredito.
     *
     * @return cdDigitoAgenciaCredito
     */
    public Integer getCdDigitoAgenciaCredito() {
	return cdDigitoAgenciaCredito;
    }

    /**
     * Set: cdDigitoAgenciaCredito.
     *
     * @param cdDigitoAgenciaCredito the cd digito agencia credito
     */
    public void setCdDigitoAgenciaCredito(Integer cdDigitoAgenciaCredito) {
	this.cdDigitoAgenciaCredito = cdDigitoAgenciaCredito;
    }

    /**
     * Get: cdDigitoAgenciaDebito.
     *
     * @return cdDigitoAgenciaDebito
     */
    public Integer getCdDigitoAgenciaDebito() {
	return cdDigitoAgenciaDebito;
    }

    /**
     * Set: cdDigitoAgenciaDebito.
     *
     * @param cdDigitoAgenciaDebito the cd digito agencia debito
     */
    public void setCdDigitoAgenciaDebito(Integer cdDigitoAgenciaDebito) {
	this.cdDigitoAgenciaDebito = cdDigitoAgenciaDebito;
    }

    /**
     * Get: cdDigitoContaCredito.
     *
     * @return cdDigitoContaCredito
     */
    public String getCdDigitoContaCredito() {
	return cdDigitoContaCredito;
    }

    /**
     * Set: cdDigitoContaCredito.
     *
     * @param cdDigitoContaCredito the cd digito conta credito
     */
    public void setCdDigitoContaCredito(String cdDigitoContaCredito) {
	this.cdDigitoContaCredito = cdDigitoContaCredito;
    }

    /**
     * Get: cdDigitoContaDebito.
     *
     * @return cdDigitoContaDebito
     */
    public String getCdDigitoContaDebito() {
	return cdDigitoContaDebito;
    }

    /**
     * Set: cdDigitoContaDebito.
     *
     * @param cdDigitoContaDebito the cd digito conta debito
     */
    public void setCdDigitoContaDebito(String cdDigitoContaDebito) {
	this.cdDigitoContaDebito = cdDigitoContaDebito;
    }

    /**
     * Get: cdEmpresa.
     *
     * @return cdEmpresa
     */
    public Long getCdEmpresa() {
	return cdEmpresa;
    }

    /**
     * Set: cdEmpresa.
     *
     * @param cdEmpresa the cd empresa
     */
    public void setCdEmpresa(Long cdEmpresa) {
	this.cdEmpresa = cdEmpresa;
    }

    /**
     * Get: cdInscricaoFavorecido.
     *
     * @return cdInscricaoFavorecido
     */
    public String getCdInscricaoFavorecido() {
	return cdInscricaoFavorecido;
    }

    /**
     * Set: cdInscricaoFavorecido.
     *
     * @param cdInscricaoFavorecido the cd inscricao favorecido
     */
    public void setCdInscricaoFavorecido(String cdInscricaoFavorecido) {
	this.cdInscricaoFavorecido = cdInscricaoFavorecido;
    }

    /**
     * Get: cdMotivoSituacaoPagamento.
     *
     * @return cdMotivoSituacaoPagamento
     */
    public Integer getCdMotivoSituacaoPagamento() {
	return cdMotivoSituacaoPagamento;
    }

    /**
     * Set: cdMotivoSituacaoPagamento.
     *
     * @param cdMotivoSituacaoPagamento the cd motivo situacao pagamento
     */
    public void setCdMotivoSituacaoPagamento(Integer cdMotivoSituacaoPagamento) {
	this.cdMotivoSituacaoPagamento = cdMotivoSituacaoPagamento;
    }

    /**
     * Get: cdPessoaJuridicaContrato.
     *
     * @return cdPessoaJuridicaContrato
     */
    public Long getCdPessoaJuridicaContrato() {
	return cdPessoaJuridicaContrato;
    }

    /**
     * Set: cdPessoaJuridicaContrato.
     *
     * @param cdPessoaJuridicaContrato the cd pessoa juridica contrato
     */
    public void setCdPessoaJuridicaContrato(Long cdPessoaJuridicaContrato) {
	this.cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
    }

    /**
     * Get: cdProdutoServicoOperacao.
     *
     * @return cdProdutoServicoOperacao
     */
    public Integer getCdProdutoServicoOperacao() {
	return cdProdutoServicoOperacao;
    }

    /**
     * Set: cdProdutoServicoOperacao.
     *
     * @param cdProdutoServicoOperacao the cd produto servico operacao
     */
    public void setCdProdutoServicoOperacao(Integer cdProdutoServicoOperacao) {
	this.cdProdutoServicoOperacao = cdProdutoServicoOperacao;
    }

    /**
     * Get: cdProdutoServicoRelacionado.
     *
     * @return cdProdutoServicoRelacionado
     */
    public Integer getCdProdutoServicoRelacionado() {
	return cdProdutoServicoRelacionado;
    }

    /**
     * Set: cdProdutoServicoRelacionado.
     *
     * @param cdProdutoServicoRelacionado the cd produto servico relacionado
     */
    public void setCdProdutoServicoRelacionado(Integer cdProdutoServicoRelacionado) {
	this.cdProdutoServicoRelacionado = cdProdutoServicoRelacionado;
    }

    /**
     * Get: cdSituacaoOperacaoPagamento.
     *
     * @return cdSituacaoOperacaoPagamento
     */
    public Integer getCdSituacaoOperacaoPagamento() {
	return cdSituacaoOperacaoPagamento;
    }

    /**
     * Set: cdSituacaoOperacaoPagamento.
     *
     * @param cdSituacaoOperacaoPagamento the cd situacao operacao pagamento
     */
    public void setCdSituacaoOperacaoPagamento(Integer cdSituacaoOperacaoPagamento) {
	this.cdSituacaoOperacaoPagamento = cdSituacaoOperacaoPagamento;
    }

    /**
     * Get: cdTipoCanal.
     *
     * @return cdTipoCanal
     */
    public Integer getCdTipoCanal() {
	return cdTipoCanal;
    }

    /**
     * Set: cdTipoCanal.
     *
     * @param cdTipoCanal the cd tipo canal
     */
    public void setCdTipoCanal(Integer cdTipoCanal) {
	this.cdTipoCanal = cdTipoCanal;
    }

    /**
     * Get: cdTipoContratoNegocio.
     *
     * @return cdTipoContratoNegocio
     */
    public Integer getCdTipoContratoNegocio() {
	return cdTipoContratoNegocio;
    }

    /**
     * Set: cdTipoContratoNegocio.
     *
     * @param cdTipoContratoNegocio the cd tipo contrato negocio
     */
    public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
	this.cdTipoContratoNegocio = cdTipoContratoNegocio;
    }

    /**
     * Get: cdTipoTela.
     *
     * @return cdTipoTela
     */
    public Integer getCdTipoTela() {
	return cdTipoTela;
    }

    /**
     * Set: cdTipoTela.
     *
     * @param cdTipoTela the cd tipo tela
     */
    public void setCdTipoTela(Integer cdTipoTela) {
	this.cdTipoTela = cdTipoTela;
    }

    /**
     * Get: codMensagem.
     *
     * @return codMensagem
     */
    public String getCodMensagem() {
	return codMensagem;
    }

    /**
     * Set: codMensagem.
     *
     * @param codMensagem the cod mensagem
     */
    public void setCodMensagem(String codMensagem) {
	this.codMensagem = codMensagem;
    }

    /**
     * Get: dsEmpresa.
     *
     * @return dsEmpresa
     */
    public String getDsEmpresa() {
	return dsEmpresa;
    }

    /**
     * Set: dsEmpresa.
     *
     * @param dsEmpresa the ds empresa
     */
    public void setDsEmpresa(String dsEmpresa) {
	this.dsEmpresa = dsEmpresa;
    }

    /**
     * Get: dsFavorecido.
     *
     * @return dsFavorecido
     */
    public String getDsFavorecido() {
	return dsFavorecido;
    }

    /**
     * Set: dsFavorecido.
     *
     * @param dsFavorecido the ds favorecido
     */
    public void setDsFavorecido(String dsFavorecido) {
	this.dsFavorecido = dsFavorecido;
    }

    /**
     * Get: dsMotivoSituacaoPagamento.
     *
     * @return dsMotivoSituacaoPagamento
     */
    public String getDsMotivoSituacaoPagamento() {
	return dsMotivoSituacaoPagamento;
    }

    /**
     * Set: dsMotivoSituacaoPagamento.
     *
     * @param dsMotivoSituacaoPagamento the ds motivo situacao pagamento
     */
    public void setDsMotivoSituacaoPagamento(String dsMotivoSituacaoPagamento) {
	this.dsMotivoSituacaoPagamento = dsMotivoSituacaoPagamento;
    }

    /**
     * Get: dsOperacaoProdutoServico.
     *
     * @return dsOperacaoProdutoServico
     */
    public String getDsOperacaoProdutoServico() {
	return dsOperacaoProdutoServico;
    }

    /**
     * Set: dsOperacaoProdutoServico.
     *
     * @param dsOperacaoProdutoServico the ds operacao produto servico
     */
    public void setDsOperacaoProdutoServico(String dsOperacaoProdutoServico) {
	this.dsOperacaoProdutoServico = dsOperacaoProdutoServico;
    }

    /**
     * Get: dsRazaoSocial.
     *
     * @return dsRazaoSocial
     */
    public String getDsRazaoSocial() {
	return dsRazaoSocial;
    }

    /**
     * Set: dsRazaoSocial.
     *
     * @param dsRazaoSocial the ds razao social
     */
    public void setDsRazaoSocial(String dsRazaoSocial) {
	this.dsRazaoSocial = dsRazaoSocial;
    }

    /**
     * Get: dsResumoProdutoServico.
     *
     * @return dsResumoProdutoServico
     */
    public String getDsResumoProdutoServico() {
	return dsResumoProdutoServico;
    }

    /**
     * Set: dsResumoProdutoServico.
     *
     * @param dsResumoProdutoServico the ds resumo produto servico
     */
    public void setDsResumoProdutoServico(String dsResumoProdutoServico) {
	this.dsResumoProdutoServico = dsResumoProdutoServico;
    }

    /**
     * Get: dsSituacaoOperacaoPagamento.
     *
     * @return dsSituacaoOperacaoPagamento
     */
    public String getDsSituacaoOperacaoPagamento() {
	return dsSituacaoOperacaoPagamento;
    }

    /**
     * Set: dsSituacaoOperacaoPagamento.
     *
     * @param dsSituacaoOperacaoPagamento the ds situacao operacao pagamento
     */
    public void setDsSituacaoOperacaoPagamento(String dsSituacaoOperacaoPagamento) {
	this.dsSituacaoOperacaoPagamento = dsSituacaoOperacaoPagamento;
    }

    /**
     * Get: dtCreditoPagamento.
     *
     * @return dtCreditoPagamento
     */
    public String getDtCreditoPagamento() {
	return dtCreditoPagamento;
    }

    /**
     * Set: dtCreditoPagamento.
     *
     * @param dtCreditoPagamento the dt credito pagamento
     */
    public void setDtCreditoPagamento(String dtCreditoPagamento) {
	this.dtCreditoPagamento = dtCreditoPagamento;
    }

    /**
     * Get: mensagem.
     *
     * @return mensagem
     */
    public String getMensagem() {
	return mensagem;
    }

    /**
     * Set: mensagem.
     *
     * @param mensagem the mensagem
     */
    public void setMensagem(String mensagem) {
	this.mensagem = mensagem;
    }

    /**
     * Get: nrCnpjCpf.
     *
     * @return nrCnpjCpf
     */
    public Long getNrCnpjCpf() {
	return nrCnpjCpf;
    }

    /**
     * Set: nrCnpjCpf.
     *
     * @param nrCnpjCpf the nr cnpj cpf
     */
    public void setNrCnpjCpf(Long nrCnpjCpf) {
	this.nrCnpjCpf = nrCnpjCpf;
    }

    /**
     * Get: nrContrato.
     *
     * @return nrContrato
     */
    public Long getNrContrato() {
	return nrContrato;
    }

    /**
     * Set: nrContrato.
     *
     * @param nrContrato the nr contrato
     */
    public void setNrContrato(Long nrContrato) {
	this.nrContrato = nrContrato;
    }

    /**
     * Get: nrDigitoCnpjCpf.
     *
     * @return nrDigitoCnpjCpf
     */
    public Integer getNrDigitoCnpjCpf() {
	return nrDigitoCnpjCpf;
    }

    /**
     * Set: nrDigitoCnpjCpf.
     *
     * @param nrDigitoCnpjCpf the nr digito cnpj cpf
     */
    public void setNrDigitoCnpjCpf(Integer nrDigitoCnpjCpf) {
	this.nrDigitoCnpjCpf = nrDigitoCnpjCpf;
    }

    /**
     * Get: nrFilialCnpjCpf.
     *
     * @return nrFilialCnpjCpf
     */
    public Integer getNrFilialCnpjCpf() {
	return nrFilialCnpjCpf;
    }

    /**
     * Set: nrFilialCnpjCpf.
     *
     * @param nrFilialCnpjCpf the nr filial cnpj cpf
     */
    public void setNrFilialCnpjCpf(Integer nrFilialCnpjCpf) {
	this.nrFilialCnpjCpf = nrFilialCnpjCpf;
    }

    /**
     * Get: numeroConsultas.
     *
     * @return numeroConsultas
     */
    public Integer getNumeroConsultas() {
	return numeroConsultas;
    }

    /**
     * Set: numeroConsultas.
     *
     * @param numeroConsultas the numero consultas
     */
    public void setNumeroConsultas(Integer numeroConsultas) {
	this.numeroConsultas = numeroConsultas;
    }

    /**
     * Get: vlEfetivoPagamento.
     *
     * @return vlEfetivoPagamento
     */
    public BigDecimal getVlEfetivoPagamento() {
	return vlEfetivoPagamento;
    }

    /**
     * Set: vlEfetivoPagamento.
     *
     * @param vlEfetivoPagamento the vl efetivo pagamento
     */
    public void setVlEfetivoPagamento(BigDecimal vlEfetivoPagamento) {
	this.vlEfetivoPagamento = vlEfetivoPagamento;
    }

    /**
     * Is check.
     *
     * @return true, if is check
     */
    public boolean isCheck() {
	return check;
    }

    /**
     * Set: check.
     *
     * @param check the check
     */
    public void setCheck(boolean check) {
	this.check = check;
    }

    /**
     * Get: agenciaCreditoFormatada.
     *
     * @return agenciaCreditoFormatada
     */
    public String getAgenciaCreditoFormatada() {
	return agenciaCreditoFormatada;
    }

    /**
     * Set: agenciaCreditoFormatada.
     *
     * @param agenciaCreditoFormatada the agencia credito formatada
     */
    public void setAgenciaCreditoFormatada(String agenciaCreditoFormatada) {
	this.agenciaCreditoFormatada = agenciaCreditoFormatada;
    }

    /**
     * Get: bancoAgenciaContaDebitoFormatado.
     *
     * @return bancoAgenciaContaDebitoFormatado
     */
    public String getBancoAgenciaContaDebitoFormatado() {
	return bancoAgenciaContaDebitoFormatado;
    }

    /**
     * Set: bancoAgenciaContaDebitoFormatado.
     *
     * @param bancoAgenciaContaDebitoFormatado the banco agencia conta debito formatado
     */
    public void setBancoAgenciaContaDebitoFormatado(String bancoAgenciaContaDebitoFormatado) {
	this.bancoAgenciaContaDebitoFormatado = bancoAgenciaContaDebitoFormatado;
    }

    /**
     * Get: bancoCreditoFormatado.
     *
     * @return bancoCreditoFormatado
     */
    public String getBancoCreditoFormatado() {
	return bancoCreditoFormatado;
    }

    /**
     * Set: bancoCreditoFormatado.
     *
     * @param bancoCreditoFormatado the banco credito formatado
     */
    public void setBancoCreditoFormatado(String bancoCreditoFormatado) {
	this.bancoCreditoFormatado = bancoCreditoFormatado;
    }

    /**
     * Get: contaCreditoFormatada.
     *
     * @return contaCreditoFormatada
     */
    public String getContaCreditoFormatada() {
	return contaCreditoFormatada;
    }

    /**
     * Set: contaCreditoFormatada.
     *
     * @param contaCreditoFormatada the conta credito formatada
     */
    public void setContaCreditoFormatada(String contaCreditoFormatada) {
	this.contaCreditoFormatada = contaCreditoFormatada;
    }

    /**
     * Get: bancoAgenciaContaCreditoFormatado.
     *
     * @return bancoAgenciaContaCreditoFormatado
     */
    public String getBancoAgenciaContaCreditoFormatado() {
	return bancoAgenciaContaCreditoFormatado;
    }

    /**
     * Set: bancoAgenciaContaCreditoFormatado.
     *
     * @param bancoAgenciaContaCreditoFormatado the banco agencia conta credito formatado
     */
    public void setBancoAgenciaContaCreditoFormatado(String bancoAgenciaContaCreditoFormatado) {
	this.bancoAgenciaContaCreditoFormatado = bancoAgenciaContaCreditoFormatado;
    }

    /**
     * Get: contaCreditoFormatado.
     *
     * @return contaCreditoFormatado
     */
    public String getContaCreditoFormatado() {
	return contaCreditoFormatado;
    }

    /**
     * Set: contaCreditoFormatado.
     *
     * @param contaCreditoFormatado the conta credito formatado
     */
    public void setContaCreditoFormatado(String contaCreditoFormatado) {
	this.contaCreditoFormatado = contaCreditoFormatado;
    }

    /**
     * Get: contaDebitoFormatado.
     *
     * @return contaDebitoFormatado
     */
    public String getContaDebitoFormatado() {
	return contaDebitoFormatado;
    }

    /**
     * Set: contaDebitoFormatado.
     *
     * @param contaDebitoFormatado the conta debito formatado
     */
    public void setContaDebitoFormatado(String contaDebitoFormatado) {
	this.contaDebitoFormatado = contaDebitoFormatado;
    }

    /**
     * Get: cpfFormatado.
     *
     * @return cpfFormatado
     */
    public String getCpfFormatado() {
	return cpfFormatado;
    }

    /**
     * Set: cpfFormatado.
     *
     * @param cpfFormatado the cpf formatado
     */
    public void setCpfFormatado(String cpfFormatado) {
	this.cpfFormatado = cpfFormatado;
    }

    /**
     * Get: dsEfetivacaoPagamento.
     *
     * @return dsEfetivacaoPagamento
     */
    public String getDsEfetivacaoPagamento() {
	return dsEfetivacaoPagamento;
    }

    /**
     * Set: dsEfetivacaoPagamento.
     *
     * @param dsEfetivacaoPagamento the ds efetivacao pagamento
     */
    public void setDsEfetivacaoPagamento(String dsEfetivacaoPagamento) {
	this.dsEfetivacaoPagamento = dsEfetivacaoPagamento;
    }

    /**
     * Get: dsIndicadorPagamento.
     *
     * @return dsIndicadorPagamento
     */
    public String getDsIndicadorPagamento() {
        return dsIndicadorPagamento;
    }

    /**
     * Set: dsIndicadorPagamento.
     *
     * @param dsIndicadorPagamento the ds indicador pagamento
     */
    public void setDsIndicadorPagamento(String dsIndicadorPagamento) {
        this.dsIndicadorPagamento = dsIndicadorPagamento;
    }

	public String getCdIspbPagtoDestino() {
		return cdIspbPagtoDestino;
	}

	public void setCdIspbPagtoDestino(String cdIspbPagtoDestino) {
		this.cdIspbPagtoDestino = cdIspbPagtoDestino;
	}

	public String getContaPagtoDestino() {
		return contaPagtoDestino;
	}

	public void setContaPagtoDestino(String contaPagtoDestino) {
		this.contaPagtoDestino = contaPagtoDestino;
	}
}