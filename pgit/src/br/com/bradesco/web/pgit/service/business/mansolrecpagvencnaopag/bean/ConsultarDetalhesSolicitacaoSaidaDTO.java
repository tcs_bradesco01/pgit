/*
 * Nome: br.com.bradesco.web.pgit.service.business.mansolrecpagvencnaopag.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mansolrecpagvencnaopag.bean;
import java.math.BigDecimal;

import br.com.bradesco.web.pgit.utils.PgitUtil;

/**
 * Nome: ConsultarDetalhesSolicitacaoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarDetalhesSolicitacaoSaidaDTO{
	
	/** Atributo codMensagem. */
	private String codMensagem;
	
	/** Atributo mensagem. */
	private String mensagem;
	
	/** Atributo cdBancoEstorno. */
	private Integer cdBancoEstorno;
	
	/** Atributo dsBancoEstorno. */
	private String dsBancoEstorno;
	
	/** Atributo cdAgenciaEstorno. */
	private Integer cdAgenciaEstorno;
	
	/** Atributo cdDigitoAgenciaEstorno. */
	private Integer cdDigitoAgenciaEstorno;
	
	/** Atributo dsAgenciaEstorno. */
	private String dsAgenciaEstorno;
	
	/** Atributo cdContaEstorno. */
	private Long cdContaEstorno;
	
	/** Atributo cdDigitoContaEstorno. */
	private String cdDigitoContaEstorno;
	
	/** Atributo dsTipoConta. */
	private String dsTipoConta;
	
	/** Atributo situacaoSolicitacao. */
	private String situacaoSolicitacao;
	
	/** Atributo cdMotivoSolicitacao. */
	private String cdMotivoSolicitacao;
	
	/** Atributo dsObservacao. */
	private String dsObservacao;
	
	/** Atributo dsDataSolicitacao. */
	private String dsDataSolicitacao;
	
	/** Atributo dsHoraSolicitacao. */
	private String dsHoraSolicitacao;
	
	/** Atributo dtAutorizacao. */
	private String dtAutorizacao;
	
	/** Atributo hrAutorizacao. */
	private String hrAutorizacao;
	
	/** Atributo cdUsuarioAutorizacao. */
	private String cdUsuarioAutorizacao;
	
	/** Atributo dtPrevistaEstorno. */
	private String dtPrevistaEstorno;
	
	/** Atributo cdValorPrevistoEstorno. */
	private BigDecimal cdValorPrevistoEstorno;
	
	/** Atributo cdFormaEstrnPagamento. */
    private Integer cdFormaEstrnPagamento;
    
    /** Atributo dsFormaEstrnPagamento. */
    private String dsFormaEstrnPagamento;

    /** Atributo dtInclusao. */
    private String dtInclusao = null;

    /** Atributo hrInclusao. */
    private String hrInclusao = null;

    /** Atributo cdUsuarioInclusao. */
    private String cdUsuarioInclusao = null;
    
    /** Atributo cdTipoCanalInclusao. */
    private Integer cdTipoCanalInclusao = null;

    /** Atributo dsTipoCanalInclusao. */
    private String dsTipoCanalInclusao = null;

    /** Atributo cdOperacaoFluxoInclusao. */
    private String cdOperacaoFluxoInclusao = null;

    /** Atributo dtManutencao. */
    private String dtManutencao = null;

    /** Atributo hrManutencao. */
    private String hrManutencao = null;

    /** Atributo cdUsuarioManutencao. */
    private String cdUsuarioManutencao = null;

    /** Atributo cdCanalManutencao. */
    private Integer cdCanalManutencao = null;

    /** Atributo dsTipoCanalManutencao. */
    private String dsTipoCanalManutencao = null;

    /** Atributo cdOperacaoFluxoManutencao. */
    private String cdOperacaoFluxoManutencao = null;

	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem(){
		return codMensagem;
	}

	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem){
		this.codMensagem = codMensagem;
	}

	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem(){
		return mensagem;
	}

	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem){
		this.mensagem = mensagem;
	}

	/**
	 * Get: cdBancoEstorno.
	 *
	 * @return cdBancoEstorno
	 */
	public Integer getCdBancoEstorno(){
		return cdBancoEstorno;
	}

	/**
	 * Set: cdBancoEstorno.
	 *
	 * @param cdBancoEstorno the cd banco estorno
	 */
	public void setCdBancoEstorno(Integer cdBancoEstorno){
		this.cdBancoEstorno = cdBancoEstorno;
	}

	/**
	 * Get: dsBancoEstorno.
	 *
	 * @return dsBancoEstorno
	 */
	public String getDsBancoEstorno(){
		return dsBancoEstorno;
	}

	/**
	 * Set: dsBancoEstorno.
	 *
	 * @param dsBancoEstorno the ds banco estorno
	 */
	public void setDsBancoEstorno(String dsBancoEstorno){
		this.dsBancoEstorno = dsBancoEstorno;
	}

	/**
	 * Get: cdAgenciaEstorno.
	 *
	 * @return cdAgenciaEstorno
	 */
	public Integer getCdAgenciaEstorno(){
		return cdAgenciaEstorno;
	}

	/**
	 * Set: cdAgenciaEstorno.
	 *
	 * @param cdAgenciaEstorno the cd agencia estorno
	 */
	public void setCdAgenciaEstorno(Integer cdAgenciaEstorno){
		this.cdAgenciaEstorno = cdAgenciaEstorno;
	}

	/**
	 * Get: cdDigitoAgenciaEstorno.
	 *
	 * @return cdDigitoAgenciaEstorno
	 */
	public Integer getCdDigitoAgenciaEstorno(){
		return cdDigitoAgenciaEstorno;
	}

	/**
	 * Set: cdDigitoAgenciaEstorno.
	 *
	 * @param cdDigitoAgenciaEstorno the cd digito agencia estorno
	 */
	public void setCdDigitoAgenciaEstorno(Integer cdDigitoAgenciaEstorno){
		this.cdDigitoAgenciaEstorno = cdDigitoAgenciaEstorno;
	}

	/**
	 * Get: dsAgenciaEstorno.
	 *
	 * @return dsAgenciaEstorno
	 */
	public String getDsAgenciaEstorno(){
		return dsAgenciaEstorno;
	}

	/**
	 * Set: dsAgenciaEstorno.
	 *
	 * @param dsAgenciaEstorno the ds agencia estorno
	 */
	public void setDsAgenciaEstorno(String dsAgenciaEstorno){
		this.dsAgenciaEstorno = dsAgenciaEstorno;
	}

	/**
	 * Get: cdContaEstorno.
	 *
	 * @return cdContaEstorno
	 */
	public Long getCdContaEstorno(){
		return cdContaEstorno;
	}

	/**
	 * Set: cdContaEstorno.
	 *
	 * @param cdContaEstorno the cd conta estorno
	 */
	public void setCdContaEstorno(Long cdContaEstorno){
		this.cdContaEstorno = cdContaEstorno;
	}

	/**
	 * Get: cdDigitoContaEstorno.
	 *
	 * @return cdDigitoContaEstorno
	 */
	public String getCdDigitoContaEstorno(){
		return cdDigitoContaEstorno;
	}

	/**
	 * Set: cdDigitoContaEstorno.
	 *
	 * @param cdDigitoContaEstorno the cd digito conta estorno
	 */
	public void setCdDigitoContaEstorno(String cdDigitoContaEstorno){
		this.cdDigitoContaEstorno = cdDigitoContaEstorno;
	}

	/**
	 * Get: dsTipoConta.
	 *
	 * @return dsTipoConta
	 */
	public String getDsTipoConta(){
		return dsTipoConta;
	}

	/**
	 * Set: dsTipoConta.
	 *
	 * @param dsTipoConta the ds tipo conta
	 */
	public void setDsTipoConta(String dsTipoConta){
		this.dsTipoConta = dsTipoConta;
	}

	/**
	 * Get: situacaoSolicitacao.
	 *
	 * @return situacaoSolicitacao
	 */
	public String getSituacaoSolicitacao(){
		return situacaoSolicitacao;
	}

	/**
	 * Set: situacaoSolicitacao.
	 *
	 * @param situacaoSolicitacao the situacao solicitacao
	 */
	public void setSituacaoSolicitacao(String situacaoSolicitacao){
		this.situacaoSolicitacao = situacaoSolicitacao;
	}

	/**
	 * Get: cdMotivoSolicitacao.
	 *
	 * @return cdMotivoSolicitacao
	 */
	public String getCdMotivoSolicitacao(){
		return cdMotivoSolicitacao;
	}

	/**
	 * Set: cdMotivoSolicitacao.
	 *
	 * @param cdMotivoSolicitacao the cd motivo solicitacao
	 */
	public void setCdMotivoSolicitacao(String cdMotivoSolicitacao){
		this.cdMotivoSolicitacao = cdMotivoSolicitacao;
	}

	/**
	 * Get: dsObservacao.
	 *
	 * @return dsObservacao
	 */
	public String getDsObservacao(){
		return dsObservacao;
	}

	/**
	 * Set: dsObservacao.
	 *
	 * @param dsObservacao the ds observacao
	 */
	public void setDsObservacao(String dsObservacao){
		this.dsObservacao = dsObservacao;
	}

	/**
	 * Get: dsDataSolicitacao.
	 *
	 * @return dsDataSolicitacao
	 */
	public String getDsDataSolicitacao(){
		return dsDataSolicitacao;
	}

	/**
	 * Set: dsDataSolicitacao.
	 *
	 * @param dsDataSolicitacao the ds data solicitacao
	 */
	public void setDsDataSolicitacao(String dsDataSolicitacao){
		this.dsDataSolicitacao = dsDataSolicitacao;
	}

	/**
	 * Get: dsHoraSolicitacao.
	 *
	 * @return dsHoraSolicitacao
	 */
	public String getDsHoraSolicitacao(){
		return dsHoraSolicitacao;
	}

	/**
	 * Set: dsHoraSolicitacao.
	 *
	 * @param dsHoraSolicitacao the ds hora solicitacao
	 */
	public void setDsHoraSolicitacao(String dsHoraSolicitacao){
		this.dsHoraSolicitacao = dsHoraSolicitacao;
	}

	/**
	 * Get: dtAutorizacao.
	 *
	 * @return dtAutorizacao
	 */
	public String getDtAutorizacao(){
		return dtAutorizacao;
	}

	/**
	 * Set: dtAutorizacao.
	 *
	 * @param dtAutorizacao the dt autorizacao
	 */
	public void setDtAutorizacao(String dtAutorizacao){
		this.dtAutorizacao = dtAutorizacao;
	}

	/**
	 * Get: hrAutorizacao.
	 *
	 * @return hrAutorizacao
	 */
	public String getHrAutorizacao(){
		return hrAutorizacao;
	}

	/**
	 * Set: hrAutorizacao.
	 *
	 * @param hrAutorizacao the hr autorizacao
	 */
	public void setHrAutorizacao(String hrAutorizacao){
		this.hrAutorizacao = hrAutorizacao;
	}

	/**
	 * Get: cdUsuarioAutorizacao.
	 *
	 * @return cdUsuarioAutorizacao
	 */
	public String getCdUsuarioAutorizacao(){
		return cdUsuarioAutorizacao;
	}

	/**
	 * Set: cdUsuarioAutorizacao.
	 *
	 * @param cdUsuarioAutorizacao the cd usuario autorizacao
	 */
	public void setCdUsuarioAutorizacao(String cdUsuarioAutorizacao){
		this.cdUsuarioAutorizacao = cdUsuarioAutorizacao;
	}

	/**
	 * Get: dtPrevistaEstorno.
	 *
	 * @return dtPrevistaEstorno
	 */
	public String getDtPrevistaEstorno(){
		return dtPrevistaEstorno;
	}

	/**
	 * Set: dtPrevistaEstorno.
	 *
	 * @param dtPrevistaEstorno the dt prevista estorno
	 */
	public void setDtPrevistaEstorno(String dtPrevistaEstorno){
		this.dtPrevistaEstorno = dtPrevistaEstorno;
	}

	/**
	 * Get: cdValorPrevistoEstorno.
	 *
	 * @return cdValorPrevistoEstorno
	 */
	public BigDecimal getCdValorPrevistoEstorno(){
		return cdValorPrevistoEstorno;
	}

	/**
	 * Set: cdValorPrevistoEstorno.
	 *
	 * @param cdValorPrevistoEstorno the cd valor previsto estorno
	 */
	public void setCdValorPrevistoEstorno(BigDecimal cdValorPrevistoEstorno){
		this.cdValorPrevistoEstorno = cdValorPrevistoEstorno;
	}

	/**
     * Get: cdFormaEstrnPagamento.
     * 
     * @return cdFormaEstrnPagamento
     */
	public Integer getCdFormaEstrnPagamento() {
		return cdFormaEstrnPagamento;
	}

	/**
     * Set: cdFormaEstrnPagamento.
     * 
     * @param cdFormaEstrnPagamento
     *            the cd forma estrn pagamento
     */
	public void setCdFormaEstrnPagamento(Integer cdFormaEstrnPagamento) {
		this.cdFormaEstrnPagamento = cdFormaEstrnPagamento;
	}

	/**
     * Get: dsFormaEstrnPagamento.
     * 
     * @return dsFormaEstrnPagamento
     */
	public String getDsFormaEstrnPagamento() {
		return dsFormaEstrnPagamento;
	}

	/**
     * Set: dsFormaEstrnPagamento.
     * 
     * @param dsFormaEstrnPagamento
     *            the ds forma estrn pagamento
     */
	public void setDsFormaEstrnPagamento(String dsFormaEstrnPagamento) {
		this.dsFormaEstrnPagamento = dsFormaEstrnPagamento;
	}

    /**
     * Nome: getDtInclusao
     *
     * @return dtInclusao
     */
    public String getDtInclusao() {
        return dtInclusao;
    }

    /**
     * Nome: setDtInclusao
     *
     * @param dtInclusao
     */
    public void setDtInclusao(String dtInclusao) {
        this.dtInclusao = dtInclusao;
    }

    /**
     * Nome: getHrInclusao
     *
     * @return hrInclusao
     */
    public String getHrInclusao() {
        return hrInclusao;
    }

    /**
     * Nome: setHrInclusao
     *
     * @param hrInclusao
     */
    public void setHrInclusao(String hrInclusao) {
        this.hrInclusao = hrInclusao;
    }

    /**
     * Nome: getCdUsuarioInclusao
     *
     * @return cdUsuarioInclusao
     */
    public String getCdUsuarioInclusao() {
        return cdUsuarioInclusao;
    }

    /**
     * Nome: setCdUsuarioInclusao
     *
     * @param cdUsuarioInclusao
     */
    public void setCdUsuarioInclusao(String cdUsuarioInclusao) {
        this.cdUsuarioInclusao = cdUsuarioInclusao;
    }

    /**
     * Nome: getCdTipoCanalInclusao
     *
     * @return cdTipoCanalInclusao
     */
    public Integer getCdTipoCanalInclusao() {
        return cdTipoCanalInclusao;
    }

    /**
     * Nome: setCdTipoCanalInclusao
     *
     * @param cdTipoCanalInclusao
     */
    public void setCdTipoCanalInclusao(Integer cdTipoCanalInclusao) {
        this.cdTipoCanalInclusao = cdTipoCanalInclusao;
    }

    /**
     * Nome: getDsTipoCanalInclusao
     *
     * @return dsTipoCanalInclusao
     */
    public String getDsTipoCanalInclusao() {
        return dsTipoCanalInclusao;
    }

    /**
     * Nome: setDsTipoCanalInclusao
     *
     * @param dsTipoCanalInclusao
     */
    public void setDsTipoCanalInclusao(String dsTipoCanalInclusao) {
        this.dsTipoCanalInclusao = dsTipoCanalInclusao;
    }

    /**
     * Nome: getCdOperacaoFluxoInclusao
     *
     * @return cdOperacaoFluxoInclusao
     */
    public String getCdOperacaoFluxoInclusao() {
        return cdOperacaoFluxoInclusao;
    }

    /**
     * Nome: setCdOperacaoFluxoInclusao
     *
     * @param cdOperacaoFluxoInclusao
     */
    public void setCdOperacaoFluxoInclusao(String cdOperacaoFluxoInclusao) {
        this.cdOperacaoFluxoInclusao = cdOperacaoFluxoInclusao;
    }

    /**
     * Nome: getDtManutencao
     *
     * @return dtManutencao
     */
    public String getDtManutencao() {
        return dtManutencao;
    }

    /**
     * Nome: setDtManutencao
     *
     * @param dtManutencao
     */
    public void setDtManutencao(String dtManutencao) {
        this.dtManutencao = dtManutencao;
    }

    /**
     * Nome: getHrManutencao
     *
     * @return hrManutencao
     */
    public String getHrManutencao() {
        return hrManutencao;
    }

    /**
     * Nome: setHrManutencao
     *
     * @param hrManutencao
     */
    public void setHrManutencao(String hrManutencao) {
        this.hrManutencao = hrManutencao;
    }

    /**
     * Nome: getCdUsuarioManutencao
     *
     * @return cdUsuarioManutencao
     */
    public String getCdUsuarioManutencao() {
        return cdUsuarioManutencao;
    }

    /**
     * Nome: setCdUsuarioManutencao
     *
     * @param cdUsuarioManutencao
     */
    public void setCdUsuarioManutencao(String cdUsuarioManutencao) {
        this.cdUsuarioManutencao = cdUsuarioManutencao;
    }

    /**
     * Nome: getCdCanalManutencao
     *
     * @return cdCanalManutencao
     */
    public Integer getCdCanalManutencao() {
        return cdCanalManutencao;
    }

    /**
     * Nome: setCdCanalManutencao
     *
     * @param cdCanalManutencao
     */
    public void setCdCanalManutencao(Integer cdCanalManutencao) {
        this.cdCanalManutencao = cdCanalManutencao;
    }

    /**
     * Nome: getDsTipoCanalManutencao
     *
     * @return dsTipoCanalManutencao
     */
    public String getDsTipoCanalManutencao() {
        return dsTipoCanalManutencao;
    }

    /**
     * Nome: setDsTipoCanalManutencao
     *
     * @param dsTipoCanalManutencao
     */
    public void setDsTipoCanalManutencao(String dsTipoCanalManutencao) {
        this.dsTipoCanalManutencao = dsTipoCanalManutencao;
    }

    /**
     * Nome: getCdOperacaoFluxoManutencao
     *
     * @return cdOperacaoFluxoManutencao
     */
    public String getCdOperacaoFluxoManutencao() {
        return cdOperacaoFluxoManutencao;
    }

    /**
     * Nome: setCdOperacaoFluxoManutencao
     *
     * @param cdOperacaoFluxoManutencao
     */
    public void setCdOperacaoFluxoManutencao(String cdOperacaoFluxoManutencao) {
        this.cdOperacaoFluxoManutencao = cdOperacaoFluxoManutencao;
    }

    /**
     * Get: dataHoraInclusao.
     * 
     * @return dataHoraInclusao
     */
    public String getDataHoraInclusao() {
        return PgitUtil.concatenarCampos(dtInclusao, hrInclusao);
    }

    /**
     * Get: tipoCanalInclusaoCompleto.
     * 
     * @return tipoCanalInclusaoCompleto
     */
    public String getTipoCanalInclusaoCompleto() {
        return PgitUtil.concatenarCampos(cdTipoCanalInclusao, dsTipoCanalInclusao);
    }
}