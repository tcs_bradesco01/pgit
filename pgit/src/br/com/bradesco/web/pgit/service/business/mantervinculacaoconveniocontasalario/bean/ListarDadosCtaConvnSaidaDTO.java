package br.com.bradesco.web.pgit.service.business.mantervinculacaoconveniocontasalario.bean;

import java.util.List;

import br.com.bradesco.web.pgit.service.data.pdc.listardadosctaconvn.response.ListarDadosCtaConvnResponse;


/**
 * Nome: ListarDadosCtaConvnSaidaDTO
 * <p>
 * Propůsito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarDadosCtaConvnSaidaDTO {
	
	/** Atributo codMensagem. */
	private String codMensagem;
	
	/** Atributo mensagem. */
	private String mensagem;
	
	/** Atributo numeroLinhas. */
	private int numeroLinhas; 
	
	/** Atributo ocorrencias. */
	List<ListarDadosCtaConvnListaOcorrenciasDTO> ocorrencias;
	
	
	public ListarDadosCtaConvnSaidaDTO(){}
	
	public ListarDadosCtaConvnSaidaDTO(ListarDadosCtaConvnResponse response, List<ListarDadosCtaConvnListaOcorrenciasDTO> ocorrencias) {
		super();
		this.codMensagem = response.getCodMensagem();
		this.mensagem = response.getMensagem();
		this.numeroLinhas = response.getNumeroLinhas();
		this.ocorrencias = ocorrencias;
	}

	/**
	 * @return the codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}

	/**
	 * @param codMensagem the codMensagem to set
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}

	/**
	 * @return the mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}

	/**
	 * @param mensagem the mensagem to set
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	/**
	 * @return the numeroLinhas
	 */
	public int getNumeroLinhas() {
		return numeroLinhas;
	}

	/**
	 * @param numeroLinhas the numeroLinhas to set
	 */
	public void setNumeroLinhas(int numeroLinhas) {
		this.numeroLinhas = numeroLinhas;
	}

	/**
	 * @return the ocorrencias
	 */
	public List<ListarDadosCtaConvnListaOcorrenciasDTO> getOcorrencias() {
		return ocorrencias;
	}

	/**
	 * @param ocorrencias the ocorrencias to set
	 */
	public void setOcorrencias(
			List<ListarDadosCtaConvnListaOcorrenciasDTO> ocorrencias) {
		this.ocorrencias = ocorrencias;
	}

	
}