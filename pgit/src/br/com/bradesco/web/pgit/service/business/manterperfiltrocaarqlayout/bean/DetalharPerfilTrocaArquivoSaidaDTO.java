/*
 * Nome: br.com.bradesco.web.pgit.service.business.manterperfiltrocaarqlayout.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.manterperfiltrocaarqlayout.bean;
import java.math.BigDecimal;

import br.com.bradesco.web.pgit.utils.CpfCnpjUtils;
import br.com.bradesco.web.pgit.utils.NumberUtils;
import br.com.bradesco.web.pgit.utils.PgitUtil;
import br.com.bradesco.web.pgit.view.utils.PgitFacesUtils;

// TODO: Auto-generated Javadoc
/**
 * Nome: DetalharPerfilTrocaArquivoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class DetalharPerfilTrocaArquivoSaidaDTO{
	
	/** Atributo codMensagem. */
	private String codMensagem;
	
	/** Atributo mensagem. */
	private String mensagem;
	
	/** Atributo cdPerfilTrocaArq. */
	private Long cdPerfilTrocaArq;
	
	/** Atributo cdTipoLayoutArquivo. */
	private Integer cdTipoLayoutArquivo;
	
	/** Atributo dsCodTipoLayout. */
	private String dsCodTipoLayout;
	
	/** Atributo dsAplicFormat. */
	private String dsAplicFormat;
	
	/** Atributo cdSistemaOrigemArquivo. */
	private String cdSistemaOrigemArquivo;
	
	/** Atributo dsSistemaOrigemArquivo. */
	private String dsSistemaOrigemArquivo;
	
	/** Atributo dsEmpresa. */
	private String dsEmpresa;
	
	/** Atributo dsNomeCliente. */
	private String dsNomeCliente;
	
	/** Atributo dsNomeArquivoRemessa. */
	private String dsNomeArquivoRemessa;
	
	/** Atributo dsNomeArquivoRetorno. */
	private String dsNomeArquivoRetorno;
	
	/** Atributo cdRejeicaoAcltoRemessa. */
	private Integer cdRejeicaoAcltoRemessa;
	
	/** Atributo dsRejeicaoAcltoRemessa. */
	private String dsRejeicaoAcltoRemessa;
	
	/** Atributo qtMaxIncotRemessa. */
	private Integer qtMaxIncotRemessa;
	
	/** Atributo percentualIncotRejeiRemessa. */
	private BigDecimal percentualIncotRejeiRemessa;
	
	/** Atributo cdNivelControleRemessa. */
	private Integer cdNivelControleRemessa;
	
	/** Atributo dsNivelControleRemessa. */
	private String dsNivelControleRemessa;
	
	/** Atributo cdControleNumeroRemessa. */
	private Integer cdControleNumeroRemessa;
	
	/** Atributo dsControleNumeroRemessa. */
	private String dsControleNumeroRemessa;
	
	/** Atributo cdPeriodicidadeContagemRemessa. */
	private Integer cdPeriodicidadeContagemRemessa;
	
	/** Atributo dsPeriodicidadeContagemRemessa. */
	private String dsPeriodicidadeContagemRemessa;
	
	/** Atributo nrMaxContagemRemessa. */
	private Long nrMaxContagemRemessa;
	
	/** Atributo cdMeioPrincipalRemessa. */
	private Integer cdMeioPrincipalRemessa;
	
	/** Atributo dsCodMeioPrincipalRemessa. */
	private String dsCodMeioPrincipalRemessa;
	
	/** Atributo cdMeioAlternRemessa. */
	private Integer cdMeioAlternRemessa;
	
	/** Atributo dsCodMeioAlternRemessa. */
	private String dsCodMeioAlternRemessa;
	
	/** Atributo cdNivelControleRetorno. */
	private Integer cdNivelControleRetorno;
	
	/** Atributo dsNivelControleRetorno. */
	private String dsNivelControleRetorno;
	
	/** Atributo cdControleNumeroRetorno. */
	private Integer cdControleNumeroRetorno;
	
	/** Atributo dsControleNumeroRetorno. */
	private String dsControleNumeroRetorno;
	
	/** Atributo cdPeriodicodadeContagemRetorno. */
	private Integer cdPeriodicodadeContagemRetorno;
	
	/** Atributo dsPeriodicodadeContagemRetorno. */
	private String dsPeriodicodadeContagemRetorno;
	
	/** Atributo nrMaxContagemRetorno. */
	private Long nrMaxContagemRetorno;
	
	/** Atributo cdMeioPrincipalRetorno. */
	private Integer cdMeioPrincipalRetorno;
	
	/** Atributo cdCodMeioPrincipalRetorno. */
	private String cdCodMeioPrincipalRetorno;
	
	/** Atributo cdMeioAltrnRetorno. */
	private Integer cdMeioAltrnRetorno;
	
	/** Atributo dsMeioAltrnRetorno. */
	private String dsMeioAltrnRetorno;
	
	/** Atributo cdSerieAplicTranmicao. */
	private String cdSerieAplicTranmicao;
	
	/** Atributo cdOperacaoCanalInclusao. */
	private String cdOperacaoCanalInclusao;
	
	/** Atributo cdTipoCanalInclusao. */
	private Integer cdTipoCanalInclusao;
	
	/** Atributo cdUsuarioInclusao. */
	private String cdUsuarioInclusao;
	
	/** Atributo cdUsuarioInclusaoExterno. */
	private String cdUsuarioInclusaoExterno;
	
	/** Atributo hrInclusaoRegistro. */
	private String hrInclusaoRegistro;
	
	/** Atributo cdOperacaoCanalManutencao. */
	private String cdOperacaoCanalManutencao;
	
	/** Atributo cdTipoCanalManutencao. */
	private Integer cdTipoCanalManutencao;
	
	/** Atributo cdUsuarioManutencao. */
	private String cdUsuarioManutencao;
	
	/** Atributo cdUsuarioManutencaoexterno. */
	private String cdUsuarioManutencaoexterno;
	
	/** Atributo hrManutencaoRegistro. */
	private String hrManutencaoRegistro;
    
    /** Atributo dsCanalInclusao. */
    private String dsCanalInclusao;
    
    /** Atributo dsCanalManutencao. */
    private String dsCanalManutencao;	
    
    /** Atributo qtMaxIncotRemessaFormatada. */
    private String qtMaxIncotRemessaFormatada;
    
    /** Atributo canalInclusaoFormatado. */
    private String canalInclusaoFormatado;
    
    /** Atributo canalManutencaoFormatado. */
    private String canalManutencaoFormatado;    
    
    /** Atributo cdResponsavelCustoEmpresa. */
    private Integer cdResponsavelCustoEmpresa;
    
    /** Atributo dsResponsavelCustoEmpresa. */
    private String dsResponsavelCustoEmpresa;
    
    /** Atributo cdPessoaJuridicaParceiro. */
    private Long cdPessoaJuridicaParceiro;
    
    /** Atributo pcCustoOrganizacaoTransmissao. */
    private BigDecimal pcCustoOrganizacaoTransmissao;
    
    /** Atributo cdAplicacaoTransmPagamento. */
    private Long cdAplicacaoTransmPagamento;
    
    /** Atributo dsUtilizacaoEmpresaVan. */
    private String dsUtilizacaoEmpresaVan;    
    
    /** Atributo cdPessoaJuridica. */
    private Long cdPessoaJuridica;
    
    /** Atributo cdUnidadeOrganizacional. */
    private Integer cdUnidadeOrganizacional;
    
    /** Atributo dsNomeContatoCliente. */
    private String dsNomeContatoCliente;
    
    /** Atributo cdAreaFoneCliente. */
    private Integer cdAreaFoneCliente;
    
    /** Atributo cdFoneContatoCliente. */
    private String cdFoneContatoCliente;
    
    /** Atributo cdRamalContatoCliente. */
    private String cdRamalContatoCliente;
    
    /** Atributo dsEmailContatoCliente. */
    private String dsEmailContatoCliente;
    
    /** Atributo dsSolicitacaoPendente. */
    private String dsSolicitacaoPendente;
    
    /** Atributo cdAreaFonePend. */
    private Integer cdAreaFonePend;
    
    /** Atributo cdFoneSolicitacaoPend. */
    private String cdFoneSolicitacaoPend;
    
    /** Atributo cdRamalSolctPend. */
    private String cdRamalSolctPend;
    
    /** Atributo dsEmailSolctPend. */
    private String dsEmailSolctPend;
    
    /** Atributo qtMesRegistroTrafg. */
    private String qtMesRegistroTrafg;
    
    /** Atributo dsObsGeralPerfil. */
    private String dsObsGeralPerfil;
    
    /** Atributo clienteFormatado. */
    private String clienteFormatado;
    
    /** Atributo cdIndicadorLayoutProprio. */
    private Integer cdIndicadorLayoutProprio;
    
	/** Atributo cdIndicadorGeracaoSegmentoB. */
	private Integer cdIndicadorGeracaoSegmentoB;
	
    /** Atributo dsSegmentoB. */
    private String dsSegmentoB;	
	
	/** Atributo cdIndicadorGeracaoSegmentoZ. */
	private Integer cdIndicadorGeracaoSegmentoZ;
    
    /** Atributo dsSegmentoZ. */
    private String dsSegmentoZ;	

    /** Atributo cdIndicadorCpfLayout. */
    private Integer cdIndicadorCpfLayout;
    
    /** Atributo dsIndicadorCpfLayout. */
    private String dsIndicadorCpfLayout;
    
    /** Atributo cdIndicadorAssociacaoLayout. */
    private Integer cdIndicadorAssociacaoLayout;
    
    /** Atributo dsIndicadorAssociacaoLayout. */
    private String dsIndicadorAssociacaoLayout;
    
    /** Atributo cdIndicadorContaComplementar. */
    private Integer cdIndicadorContaComplementar;
    
    /** Atributo dsIndicadorContaComplementar. */
    private String dsIndicadorContaComplementar;
    
    /** The ds indicador consiste conta debito. */
    private String dsIndicadorConsisteContaDebito;
    
    private Integer cdIndicadorConsisteContaDebito;
    
    private Integer cdTipoLayoutMae;
    private Integer cdTipoContratoMae;
    private Integer cdTipoParticipanteMae;
    private Integer cdFilialCnpj;
    private Integer cdControleCpfCnpj;
    
    private String dsNomeRazaoSocial;
    private String dsTipoLayoutArquivo;
    
    private Long cdPerfilTrocaMae;
    private Long cdPessoaJuridicaMae;
    private Long nrSequencialContratoMae;
    private Long cdCpssoaMae;
    private Long cdCpfCnpj;
    
    public String getCpfCnpjFormatado(){
    	return CpfCnpjUtils.formatarCpfCnpj(getCdCpfCnpj(), getCdFilialCnpj(), getCdControleCpfCnpj());
    }
       
	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}
	
	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	/**
	 * Get: cdPerfilTrocaArq.
	 *
	 * @return cdPerfilTrocaArq
	 */
	public Long getCdPerfilTrocaArq() {
		return cdPerfilTrocaArq;
	}
	
	/**
	 * Set: cdPerfilTrocaArq.
	 *
	 * @param cdPerfilTrocaArq the cd perfil troca arq
	 */
	public void setCdPerfilTrocaArq(Long cdPerfilTrocaArq) {
		this.cdPerfilTrocaArq = cdPerfilTrocaArq;
	}
	
	/**
	 * Get: cdTipoLayoutArquivo.
	 *
	 * @return cdTipoLayoutArquivo
	 */
	public Integer getCdTipoLayoutArquivo() {
		return cdTipoLayoutArquivo;
	}
	
	/**
	 * Set: cdTipoLayoutArquivo.
	 *
	 * @param cdTipoLayoutArquivo the cd tipo layout arquivo
	 */
	public void setCdTipoLayoutArquivo(Integer cdTipoLayoutArquivo) {
		this.cdTipoLayoutArquivo = cdTipoLayoutArquivo;
	}
	
	/**
	 * Get: dsCodTipoLayout.
	 *
	 * @return dsCodTipoLayout
	 */
	public String getDsCodTipoLayout() {
		return dsCodTipoLayout;
	}
	
	/**
	 * Set: dsCodTipoLayout.
	 *
	 * @param dsCodTipoLayout the ds cod tipo layout
	 */
	public void setDsCodTipoLayout(String dsCodTipoLayout) {
		this.dsCodTipoLayout = dsCodTipoLayout;
	}
	
	/**
	 * Get: dsAplicFormat.
	 *
	 * @return dsAplicFormat
	 */
	public String getDsAplicFormat() {
		return dsAplicFormat;
	}
	
	/**
	 * Set: dsAplicFormat.
	 *
	 * @param dsAplicFormat the ds aplic format
	 */
	public void setDsAplicFormat(String dsAplicFormat) {
		this.dsAplicFormat = dsAplicFormat;
	}
	
	/**
	 * Get: cdSistemaOrigemArquivo.
	 *
	 * @return cdSistemaOrigemArquivo
	 */
	public String getCdSistemaOrigemArquivo() {
		return cdSistemaOrigemArquivo;
	}
	
	/**
	 * Set: cdSistemaOrigemArquivo.
	 *
	 * @param cdSistemaOrigemArquivo the cd sistema origem arquivo
	 */
	public void setCdSistemaOrigemArquivo(String cdSistemaOrigemArquivo) {
		this.cdSistemaOrigemArquivo = cdSistemaOrigemArquivo;
	}
	
	/**
	 * Get: dsSistemaOrigemArquivo.
	 *
	 * @return dsSistemaOrigemArquivo
	 */
	public String getDsSistemaOrigemArquivo() {
		return dsSistemaOrigemArquivo;
	}
	
	/**
	 * Set: dsSistemaOrigemArquivo.
	 *
	 * @param dsSistemaOrigemArquivo the ds sistema origem arquivo
	 */
	public void setDsSistemaOrigemArquivo(String dsSistemaOrigemArquivo) {
		this.dsSistemaOrigemArquivo = dsSistemaOrigemArquivo;
	}
	
	/**
	 * Get: dsEmpresa.
	 *
	 * @return dsEmpresa
	 */
	public String getDsEmpresa() {
		return dsEmpresa;
	}
	
	/**
	 * Set: dsEmpresa.
	 *
	 * @param dsEmpresa the ds empresa
	 */
	public void setDsEmpresa(String dsEmpresa) {
		this.dsEmpresa = dsEmpresa;
	}
	
	/**
	 * Get: dsNomeCliente.
	 *
	 * @return dsNomeCliente
	 */
	public String getDsNomeCliente() {
		return dsNomeCliente;
	}
	
	/**
	 * Set: dsNomeCliente.
	 *
	 * @param dsNomeCliente the ds nome cliente
	 */
	public void setDsNomeCliente(String dsNomeCliente) {
		this.dsNomeCliente = dsNomeCliente;
	}
	
	/**
	 * Get: dsNomeArquivoRemessa.
	 *
	 * @return dsNomeArquivoRemessa
	 */
	public String getDsNomeArquivoRemessa() {
		return dsNomeArquivoRemessa;
	}
	
	/**
	 * Set: dsNomeArquivoRemessa.
	 *
	 * @param dsNomeArquivoRemessa the ds nome arquivo remessa
	 */
	public void setDsNomeArquivoRemessa(String dsNomeArquivoRemessa) {
		this.dsNomeArquivoRemessa = dsNomeArquivoRemessa;
	}
	
	/**
	 * Get: dsNomeArquivoRetorno.
	 *
	 * @return dsNomeArquivoRetorno
	 */
	public String getDsNomeArquivoRetorno() {
		return dsNomeArquivoRetorno;
	}
	
	/**
	 * Set: dsNomeArquivoRetorno.
	 *
	 * @param dsNomeArquivoRetorno the ds nome arquivo retorno
	 */
	public void setDsNomeArquivoRetorno(String dsNomeArquivoRetorno) {
		this.dsNomeArquivoRetorno = dsNomeArquivoRetorno;
	}
	
	/**
	 * Get: cdRejeicaoAcltoRemessa.
	 *
	 * @return cdRejeicaoAcltoRemessa
	 */
	public Integer getCdRejeicaoAcltoRemessa() {
		return cdRejeicaoAcltoRemessa;
	}
	
	/**
	 * Set: cdRejeicaoAcltoRemessa.
	 *
	 * @param cdRejeicaoAcltoRemessa the cd rejeicao aclto remessa
	 */
	public void setCdRejeicaoAcltoRemessa(Integer cdRejeicaoAcltoRemessa) {
		this.cdRejeicaoAcltoRemessa = cdRejeicaoAcltoRemessa;
	}
	
	/**
	 * Get: dsRejeicaoAcltoRemessa.
	 *
	 * @return dsRejeicaoAcltoRemessa
	 */
	public String getDsRejeicaoAcltoRemessa() {
		return dsRejeicaoAcltoRemessa;
	}
	
	/**
	 * Set: dsRejeicaoAcltoRemessa.
	 *
	 * @param dsRejeicaoAcltoRemessa the ds rejeicao aclto remessa
	 */
	public void setDsRejeicaoAcltoRemessa(String dsRejeicaoAcltoRemessa) {
		this.dsRejeicaoAcltoRemessa = dsRejeicaoAcltoRemessa;
	}
	
	/**
	 * Get: qtMaxIncotRemessa.
	 *
	 * @return qtMaxIncotRemessa
	 */
	public Integer getQtMaxIncotRemessa() {
		return qtMaxIncotRemessa;
	}
	
	/**
	 * Set: qtMaxIncotRemessa.
	 *
	 * @param qtMaxIncotRemessa the qt max incot remessa
	 */
	public void setQtMaxIncotRemessa(Integer qtMaxIncotRemessa) {
		this.qtMaxIncotRemessa = qtMaxIncotRemessa;
	}
	
	/**
	 * Get: percentualIncotRejeiRemessa.
	 *
	 * @return percentualIncotRejeiRemessa
	 */
	public BigDecimal getPercentualIncotRejeiRemessa() {
		return percentualIncotRejeiRemessa;
	}
	
	/**
	 * Set: percentualIncotRejeiRemessa.
	 *
	 * @param percentualIncotRejeiRemessa the percentual incot rejei remessa
	 */
	public void setPercentualIncotRejeiRemessa(
			BigDecimal percentualIncotRejeiRemessa) {
		this.percentualIncotRejeiRemessa = percentualIncotRejeiRemessa;
	}
	
	/**
	 * Get: cdNivelControleRemessa.
	 *
	 * @return cdNivelControleRemessa
	 */
	public Integer getCdNivelControleRemessa() {
		return cdNivelControleRemessa;
	}
	
	/**
	 * Set: cdNivelControleRemessa.
	 *
	 * @param cdNivelControleRemessa the cd nivel controle remessa
	 */
	public void setCdNivelControleRemessa(Integer cdNivelControleRemessa) {
		this.cdNivelControleRemessa = cdNivelControleRemessa;
	}
	
	/**
	 * Get: dsNivelControleRemessa.
	 *
	 * @return dsNivelControleRemessa
	 */
	public String getDsNivelControleRemessa() {
		return dsNivelControleRemessa;
	}
	
	/**
	 * Set: dsNivelControleRemessa.
	 *
	 * @param dsNivelControleRemessa the ds nivel controle remessa
	 */
	public void setDsNivelControleRemessa(String dsNivelControleRemessa) {
		this.dsNivelControleRemessa = dsNivelControleRemessa;
	}
	
	/**
	 * Get: cdControleNumeroRemessa.
	 *
	 * @return cdControleNumeroRemessa
	 */
	public Integer getCdControleNumeroRemessa() {
		return cdControleNumeroRemessa;
	}
	
	/**
	 * Set: cdControleNumeroRemessa.
	 *
	 * @param cdControleNumeroRemessa the cd controle numero remessa
	 */
	public void setCdControleNumeroRemessa(Integer cdControleNumeroRemessa) {
		this.cdControleNumeroRemessa = cdControleNumeroRemessa;
	}
	
	/**
	 * Get: dsControleNumeroRemessa.
	 *
	 * @return dsControleNumeroRemessa
	 */
	public String getDsControleNumeroRemessa() {
		return dsControleNumeroRemessa;
	}
	
	/**
	 * Set: dsControleNumeroRemessa.
	 *
	 * @param dsControleNumeroRemessa the ds controle numero remessa
	 */
	public void setDsControleNumeroRemessa(String dsControleNumeroRemessa) {
		this.dsControleNumeroRemessa = dsControleNumeroRemessa;
	}
	
	/**
	 * Get: cdPeriodicidadeContagemRemessa.
	 *
	 * @return cdPeriodicidadeContagemRemessa
	 */
	public Integer getCdPeriodicidadeContagemRemessa() {
		return cdPeriodicidadeContagemRemessa;
	}
	
	/**
	 * Set: cdPeriodicidadeContagemRemessa.
	 *
	 * @param cdPeriodicidadeContagemRemessa the cd periodicidade contagem remessa
	 */
	public void setCdPeriodicidadeContagemRemessa(
			Integer cdPeriodicidadeContagemRemessa) {
		this.cdPeriodicidadeContagemRemessa = cdPeriodicidadeContagemRemessa;
	}
	
	/**
	 * Get: dsPeriodicidadeContagemRemessa.
	 *
	 * @return dsPeriodicidadeContagemRemessa
	 */
	public String getDsPeriodicidadeContagemRemessa() {
		return dsPeriodicidadeContagemRemessa;
	}
	
	/**
	 * Set: dsPeriodicidadeContagemRemessa.
	 *
	 * @param dsPeriodicidadeContagemRemessa the ds periodicidade contagem remessa
	 */
	public void setDsPeriodicidadeContagemRemessa(
			String dsPeriodicidadeContagemRemessa) {
		this.dsPeriodicidadeContagemRemessa = dsPeriodicidadeContagemRemessa;
	}
	
	/**
	 * Get: nrMaxContagemRemessa.
	 *
	 * @return nrMaxContagemRemessa
	 */
	public Long getNrMaxContagemRemessa() {
		return nrMaxContagemRemessa;
	}
	
	/**
	 * Set: nrMaxContagemRemessa.
	 *
	 * @param nrMaxContagemRemessa the nr max contagem remessa
	 */
	public void setNrMaxContagemRemessa(Long nrMaxContagemRemessa) {
		this.nrMaxContagemRemessa = nrMaxContagemRemessa;
	}
	
	/**
	 * Get: cdMeioPrincipalRemessa.
	 *
	 * @return cdMeioPrincipalRemessa
	 */
	public Integer getCdMeioPrincipalRemessa() {
		return cdMeioPrincipalRemessa;
	}
	
	/**
	 * Set: cdMeioPrincipalRemessa.
	 *
	 * @param cdMeioPrincipalRemessa the cd meio principal remessa
	 */
	public void setCdMeioPrincipalRemessa(Integer cdMeioPrincipalRemessa) {
		this.cdMeioPrincipalRemessa = cdMeioPrincipalRemessa;
	}
	
	/**
	 * Get: dsCodMeioPrincipalRemessa.
	 *
	 * @return dsCodMeioPrincipalRemessa
	 */
	public String getDsCodMeioPrincipalRemessa() {
		return dsCodMeioPrincipalRemessa;
	}
	
	/**
	 * Set: dsCodMeioPrincipalRemessa.
	 *
	 * @param dsCodMeioPrincipalRemessa the ds cod meio principal remessa
	 */
	public void setDsCodMeioPrincipalRemessa(String dsCodMeioPrincipalRemessa) {
		this.dsCodMeioPrincipalRemessa = dsCodMeioPrincipalRemessa;
	}
	
	/**
	 * Get: cdMeioAlternRemessa.
	 *
	 * @return cdMeioAlternRemessa
	 */
	public Integer getCdMeioAlternRemessa() {
		return cdMeioAlternRemessa;
	}
	
	/**
	 * Set: cdMeioAlternRemessa.
	 *
	 * @param cdMeioAlternRemessa the cd meio altern remessa
	 */
	public void setCdMeioAlternRemessa(Integer cdMeioAlternRemessa) {
		this.cdMeioAlternRemessa = cdMeioAlternRemessa;
	}
	
	/**
	 * Get: dsCodMeioAlternRemessa.
	 *
	 * @return dsCodMeioAlternRemessa
	 */
	public String getDsCodMeioAlternRemessa() {
		return dsCodMeioAlternRemessa;
	}
	
	/**
	 * Set: dsCodMeioAlternRemessa.
	 *
	 * @param dsCodMeioAlternRemessa the ds cod meio altern remessa
	 */
	public void setDsCodMeioAlternRemessa(String dsCodMeioAlternRemessa) {
		this.dsCodMeioAlternRemessa = dsCodMeioAlternRemessa;
	}
	
	/**
	 * Get: cdNivelControleRetorno.
	 *
	 * @return cdNivelControleRetorno
	 */
	public Integer getCdNivelControleRetorno() {
		return cdNivelControleRetorno;
	}
	
	/**
	 * Set: cdNivelControleRetorno.
	 *
	 * @param cdNivelControleRetorno the cd nivel controle retorno
	 */
	public void setCdNivelControleRetorno(Integer cdNivelControleRetorno) {
		this.cdNivelControleRetorno = cdNivelControleRetorno;
	}
	
	/**
	 * Get: dsNivelControleRetorno.
	 *
	 * @return dsNivelControleRetorno
	 */
	public String getDsNivelControleRetorno() {
		return dsNivelControleRetorno;
	}
	
	/**
	 * Set: dsNivelControleRetorno.
	 *
	 * @param dsNivelControleRetorno the ds nivel controle retorno
	 */
	public void setDsNivelControleRetorno(String dsNivelControleRetorno) {
		this.dsNivelControleRetorno = dsNivelControleRetorno;
	}
	
	/**
	 * Get: cdControleNumeroRetorno.
	 *
	 * @return cdControleNumeroRetorno
	 */
	public Integer getCdControleNumeroRetorno() {
		return cdControleNumeroRetorno;
	}
	
	/**
	 * Set: cdControleNumeroRetorno.
	 *
	 * @param cdControleNumeroRetorno the cd controle numero retorno
	 */
	public void setCdControleNumeroRetorno(Integer cdControleNumeroRetorno) {
		this.cdControleNumeroRetorno = cdControleNumeroRetorno;
	}
	
	/**
	 * Get: dsControleNumeroRetorno.
	 *
	 * @return dsControleNumeroRetorno
	 */
	public String getDsControleNumeroRetorno() {
		return dsControleNumeroRetorno;
	}
	
	/**
	 * Set: dsControleNumeroRetorno.
	 *
	 * @param dsControleNumeroRetorno the ds controle numero retorno
	 */
	public void setDsControleNumeroRetorno(String dsControleNumeroRetorno) {
		this.dsControleNumeroRetorno = dsControleNumeroRetorno;
	}
	
	/**
	 * Get: cdPeriodicodadeContagemRetorno.
	 *
	 * @return cdPeriodicodadeContagemRetorno
	 */
	public Integer getCdPeriodicodadeContagemRetorno() {
		return cdPeriodicodadeContagemRetorno;
	}
	
	/**
	 * Set: cdPeriodicodadeContagemRetorno.
	 *
	 * @param cdPeriodicodadeContagemRetorno the cd periodicodade contagem retorno
	 */
	public void setCdPeriodicodadeContagemRetorno(
			Integer cdPeriodicodadeContagemRetorno) {
		this.cdPeriodicodadeContagemRetorno = cdPeriodicodadeContagemRetorno;
	}
	
	/**
	 * Get: dsPeriodicodadeContagemRetorno.
	 *
	 * @return dsPeriodicodadeContagemRetorno
	 */
	public String getDsPeriodicodadeContagemRetorno() {
		return dsPeriodicodadeContagemRetorno;
	}
	
	/**
	 * Set: dsPeriodicodadeContagemRetorno.
	 *
	 * @param dsPeriodicodadeContagemRetorno the ds periodicodade contagem retorno
	 */
	public void setDsPeriodicodadeContagemRetorno(
			String dsPeriodicodadeContagemRetorno) {
		this.dsPeriodicodadeContagemRetorno = dsPeriodicodadeContagemRetorno;
	}
	
	/**
	 * Get: nrMaxContagemRetorno.
	 *
	 * @return nrMaxContagemRetorno
	 */
	public Long getNrMaxContagemRetorno() {
		return nrMaxContagemRetorno;
	}
	
	/**
	 * Set: nrMaxContagemRetorno.
	 *
	 * @param nrMaxContagemRetorno the nr max contagem retorno
	 */
	public void setNrMaxContagemRetorno(Long nrMaxContagemRetorno) {
		this.nrMaxContagemRetorno = nrMaxContagemRetorno;
	}
	
	/**
	 * Get: cdMeioPrincipalRetorno.
	 *
	 * @return cdMeioPrincipalRetorno
	 */
	public Integer getCdMeioPrincipalRetorno() {
		return cdMeioPrincipalRetorno;
	}
	
	/**
	 * Set: cdMeioPrincipalRetorno.
	 *
	 * @param cdMeioPrincipalRetorno the cd meio principal retorno
	 */
	public void setCdMeioPrincipalRetorno(Integer cdMeioPrincipalRetorno) {
		this.cdMeioPrincipalRetorno = cdMeioPrincipalRetorno;
	}
	
	/**
	 * Get: cdCodMeioPrincipalRetorno.
	 *
	 * @return cdCodMeioPrincipalRetorno
	 */
	public String getCdCodMeioPrincipalRetorno() {
		return cdCodMeioPrincipalRetorno;
	}
	
	/**
	 * Set: cdCodMeioPrincipalRetorno.
	 *
	 * @param cdCodMeioPrincipalRetorno the cd cod meio principal retorno
	 */
	public void setCdCodMeioPrincipalRetorno(String cdCodMeioPrincipalRetorno) {
		this.cdCodMeioPrincipalRetorno = cdCodMeioPrincipalRetorno;
	}
	
	/**
	 * Get: cdMeioAltrnRetorno.
	 *
	 * @return cdMeioAltrnRetorno
	 */
	public Integer getCdMeioAltrnRetorno() {
		return cdMeioAltrnRetorno;
	}
	
	/**
	 * Set: cdMeioAltrnRetorno.
	 *
	 * @param cdMeioAltrnRetorno the cd meio altrn retorno
	 */
	public void setCdMeioAltrnRetorno(Integer cdMeioAltrnRetorno) {
		this.cdMeioAltrnRetorno = cdMeioAltrnRetorno;
	}
	
	/**
	 * Get: dsMeioAltrnRetorno.
	 *
	 * @return dsMeioAltrnRetorno
	 */
	public String getDsMeioAltrnRetorno() {
		return dsMeioAltrnRetorno;
	}
	
	/**
	 * Set: dsMeioAltrnRetorno.
	 *
	 * @param dsMeioAltrnRetorno the ds meio altrn retorno
	 */
	public void setDsMeioAltrnRetorno(String dsMeioAltrnRetorno) {
		this.dsMeioAltrnRetorno = dsMeioAltrnRetorno;
	}
	
	/**
	 * Get: cdSerieAplicTranmicao.
	 *
	 * @return cdSerieAplicTranmicao
	 */
	public String getCdSerieAplicTranmicao() {
		return cdSerieAplicTranmicao;
	}
	
	/**
	 * Set: cdSerieAplicTranmicao.
	 *
	 * @param cdSerieAplicTranmicao the cd serie aplic tranmicao
	 */
	public void setCdSerieAplicTranmicao(String cdSerieAplicTranmicao) {
		this.cdSerieAplicTranmicao = cdSerieAplicTranmicao;
	}
	
	/**
	 * Get: cdOperacaoCanalInclusao.
	 *
	 * @return cdOperacaoCanalInclusao
	 */
	public String getCdOperacaoCanalInclusao() {
		return cdOperacaoCanalInclusao;
	}
	
	/**
	 * Set: cdOperacaoCanalInclusao.
	 *
	 * @param cdOperacaoCanalInclusao the cd operacao canal inclusao
	 */
	public void setCdOperacaoCanalInclusao(String cdOperacaoCanalInclusao) {
		this.cdOperacaoCanalInclusao = cdOperacaoCanalInclusao;
	}
	
	/**
	 * Get: cdTipoCanalInclusao.
	 *
	 * @return cdTipoCanalInclusao
	 */
	public Integer getCdTipoCanalInclusao() {
		return cdTipoCanalInclusao;
	}
	
	/**
	 * Set: cdTipoCanalInclusao.
	 *
	 * @param cdTipoCanalInclusao the cd tipo canal inclusao
	 */
	public void setCdTipoCanalInclusao(Integer cdTipoCanalInclusao) {
		this.cdTipoCanalInclusao = cdTipoCanalInclusao;
	}
	
	/**
	 * Get: cdUsuarioInclusao.
	 *
	 * @return cdUsuarioInclusao
	 */
	public String getCdUsuarioInclusao() {
		return cdUsuarioInclusao;
	}
	
	/**
	 * Set: cdUsuarioInclusao.
	 *
	 * @param cdUsuarioInclusao the cd usuario inclusao
	 */
	public void setCdUsuarioInclusao(String cdUsuarioInclusao) {
		this.cdUsuarioInclusao = cdUsuarioInclusao;
	}
	
	/**
	 * Get: cdUsuarioInclusaoExterno.
	 *
	 * @return cdUsuarioInclusaoExterno
	 */
	public String getCdUsuarioInclusaoExterno() {
		return cdUsuarioInclusaoExterno;
	}
	
	/**
	 * Set: cdUsuarioInclusaoExterno.
	 *
	 * @param cdUsuarioInclusaoExterno the cd usuario inclusao externo
	 */
	public void setCdUsuarioInclusaoExterno(String cdUsuarioInclusaoExterno) {
		this.cdUsuarioInclusaoExterno = cdUsuarioInclusaoExterno;
	}
	
	/**
	 * Get: hrInclusaoRegistro.
	 *
	 * @return hrInclusaoRegistro
	 */
	public String getHrInclusaoRegistro() {
		return hrInclusaoRegistro;
	}
	
	/**
	 * Set: hrInclusaoRegistro.
	 *
	 * @param hrInclusaoRegistro the hr inclusao registro
	 */
	public void setHrInclusaoRegistro(String hrInclusaoRegistro) {
		this.hrInclusaoRegistro = hrInclusaoRegistro;
	}
	
	/**
	 * Get: cdOperacaoCanalManutencao.
	 *
	 * @return cdOperacaoCanalManutencao
	 */
	public String getCdOperacaoCanalManutencao() {
		return cdOperacaoCanalManutencao;
	}
	
	/**
	 * Set: cdOperacaoCanalManutencao.
	 *
	 * @param cdOperacaoCanalManutencao the cd operacao canal manutencao
	 */
	public void setCdOperacaoCanalManutencao(String cdOperacaoCanalManutencao) {
		this.cdOperacaoCanalManutencao = cdOperacaoCanalManutencao;
	}
	
	/**
	 * Get: cdTipoCanalManutencao.
	 *
	 * @return cdTipoCanalManutencao
	 */
	public Integer getCdTipoCanalManutencao() {
		return cdTipoCanalManutencao;
	}
	
	/**
	 * Set: cdTipoCanalManutencao.
	 *
	 * @param cdTipoCanalManutencao the cd tipo canal manutencao
	 */
	public void setCdTipoCanalManutencao(Integer cdTipoCanalManutencao) {
		this.cdTipoCanalManutencao = cdTipoCanalManutencao;
	}
	
	/**
	 * Get: cdUsuarioManutencao.
	 *
	 * @return cdUsuarioManutencao
	 */
	public String getCdUsuarioManutencao() {
		return cdUsuarioManutencao;
	}
	
	/**
	 * Set: cdUsuarioManutencao.
	 *
	 * @param cdUsuarioManutencao the cd usuario manutencao
	 */
	public void setCdUsuarioManutencao(String cdUsuarioManutencao) {
		this.cdUsuarioManutencao = cdUsuarioManutencao;
	}
	
	/**
	 * Get: cdUsuarioManutencaoexterno.
	 *
	 * @return cdUsuarioManutencaoexterno
	 */
	public String getCdUsuarioManutencaoexterno() {
		return cdUsuarioManutencaoexterno;
	}
	
	/**
	 * Set: cdUsuarioManutencaoexterno.
	 *
	 * @param cdUsuarioManutencaoexterno the cd usuario manutencaoexterno
	 */
	public void setCdUsuarioManutencaoexterno(String cdUsuarioManutencaoexterno) {
		this.cdUsuarioManutencaoexterno = cdUsuarioManutencaoexterno;
	}
	
	/**
	 * Get: hrManutencaoRegistro.
	 *
	 * @return hrManutencaoRegistro
	 */
	public String getHrManutencaoRegistro() {
		return hrManutencaoRegistro;
	}
	
	/**
	 * Set: hrManutencaoRegistro.
	 *
	 * @param hrManutencaoRegistro the hr manutencao registro
	 */
	public void setHrManutencaoRegistro(String hrManutencaoRegistro) {
		this.hrManutencaoRegistro = hrManutencaoRegistro;
	}
	
	/**
	 * Get: dsCanalInclusao.
	 *
	 * @return dsCanalInclusao
	 */
	public String getDsCanalInclusao() {
		return dsCanalInclusao;
	}
	
	/**
	 * Set: dsCanalInclusao.
	 *
	 * @param dsCanalInclusao the ds canal inclusao
	 */
	public void setDsCanalInclusao(String dsCanalInclusao) {
		this.dsCanalInclusao = dsCanalInclusao;
	}
	
	/**
	 * Get: dsCanalManutencao.
	 *
	 * @return dsCanalManutencao
	 */
	public String getDsCanalManutencao() {
		return dsCanalManutencao;
	}
	
	/**
	 * Set: dsCanalManutencao.
	 *
	 * @param dsCanalManutencao the ds canal manutencao
	 */
	public void setDsCanalManutencao(String dsCanalManutencao) {
		this.dsCanalManutencao = dsCanalManutencao;
	}
	
	/**
	 * Get: qtMaxIncotRemessaFormatada.
	 *
	 * @return qtMaxIncotRemessaFormatada
	 */
	public String getQtMaxIncotRemessaFormatada() {
		return qtMaxIncotRemessaFormatada;
	}
	
	/**
	 * Set: qtMaxIncotRemessaFormatada.
	 *
	 * @param qtMaxIncotRemessaFormatada the qt max incot remessa formatada
	 */
	public void setQtMaxIncotRemessaFormatada(String qtMaxIncotRemessaFormatada) {
		this.qtMaxIncotRemessaFormatada = qtMaxIncotRemessaFormatada;
	}
	
	/**
	 * Get: canalInclusaoFormatado.
	 *
	 * @return canalInclusaoFormatado
	 */
	public String getCanalInclusaoFormatado() {
		return canalInclusaoFormatado;
	}
	
	/**
	 * Set: canalInclusaoFormatado.
	 *
	 * @param canalInclusaoFormatado the canal inclusao formatado
	 */
	public void setCanalInclusaoFormatado(String canalInclusaoFormatado) {
		this.canalInclusaoFormatado = canalInclusaoFormatado;
	}
	
	/**
	 * Get: canalManutencaoFormatado.
	 *
	 * @return canalManutencaoFormatado
	 */
	public String getCanalManutencaoFormatado() {
		return canalManutencaoFormatado;
	}
	
	/**
	 * Set: canalManutencaoFormatado.
	 *
	 * @param canalManutencaoFormatado the canal manutencao formatado
	 */
	public void setCanalManutencaoFormatado(String canalManutencaoFormatado) {
		this.canalManutencaoFormatado = canalManutencaoFormatado;
	}
	
	/**
	 * Get: cdResponsavelCustoEmpresa.
	 *
	 * @return cdResponsavelCustoEmpresa
	 */
	public Integer getCdResponsavelCustoEmpresa() {
		return cdResponsavelCustoEmpresa;
	}
	
	/**
	 * Set: cdResponsavelCustoEmpresa.
	 *
	 * @param cdResponsavelCustoEmpresa the cd responsavel custo empresa
	 */
	public void setCdResponsavelCustoEmpresa(Integer cdResponsavelCustoEmpresa) {
		this.cdResponsavelCustoEmpresa = cdResponsavelCustoEmpresa;
	}
	
	/**
	 * Get: dsResponsavelCustoEmpresa.
	 *
	 * @return dsResponsavelCustoEmpresa
	 */
	public String getDsResponsavelCustoEmpresa() {
		return dsResponsavelCustoEmpresa;
	}
	
	/**
	 * Set: dsResponsavelCustoEmpresa.
	 *
	 * @param dsResponsavelCustoEmpresa the ds responsavel custo empresa
	 */
	public void setDsResponsavelCustoEmpresa(String dsResponsavelCustoEmpresa) {
		this.dsResponsavelCustoEmpresa = dsResponsavelCustoEmpresa;
	}
	
	/**
	 * Get: cdPessoaJuridicaParceiro.
	 *
	 * @return cdPessoaJuridicaParceiro
	 */
	public Long getCdPessoaJuridicaParceiro() {
		return cdPessoaJuridicaParceiro;
	}
	
	/**
	 * Set: cdPessoaJuridicaParceiro.
	 *
	 * @param cdPessoaJuridicaParceiro the cd pessoa juridica parceiro
	 */
	public void setCdPessoaJuridicaParceiro(Long cdPessoaJuridicaParceiro) {
		this.cdPessoaJuridicaParceiro = cdPessoaJuridicaParceiro;
	}
	
	/**
	 * Get: pcCustoOrganizacaoTransmissao.
	 *
	 * @return pcCustoOrganizacaoTransmissao
	 */
	public BigDecimal getPcCustoOrganizacaoTransmissao() {
		return pcCustoOrganizacaoTransmissao;
	}
	
	/**
	 * Set: pcCustoOrganizacaoTransmissao.
	 *
	 * @param pcCustoOrganizacaoTransmissao the pc custo organizacao transmissao
	 */
	public void setPcCustoOrganizacaoTransmissao(
			BigDecimal pcCustoOrganizacaoTransmissao) {
		this.pcCustoOrganizacaoTransmissao = pcCustoOrganizacaoTransmissao;
	}
	
	/**
	 * Get: dsUtilizacaoEmpresaVan.
	 *
	 * @return dsUtilizacaoEmpresaVan
	 */
	public String getDsUtilizacaoEmpresaVan() {
		return dsUtilizacaoEmpresaVan;
	}
	
	/**
	 * Set: dsUtilizacaoEmpresaVan.
	 *
	 * @param dsUtilizacaoEmpresaVan the ds utilizacao empresa van
	 */
	public void setDsUtilizacaoEmpresaVan(String dsUtilizacaoEmpresaVan) {
		this.dsUtilizacaoEmpresaVan = dsUtilizacaoEmpresaVan;
	}
	
	/**
	 * Get: cdPessoaJuridica.
	 *
	 * @return cdPessoaJuridica
	 */
	public Long getCdPessoaJuridica() {
		return cdPessoaJuridica;
	}
	
	/**
	 * Set: cdPessoaJuridica.
	 *
	 * @param cdPessoaJuridica the cd pessoa juridica
	 */
	public void setCdPessoaJuridica(Long cdPessoaJuridica) {
		this.cdPessoaJuridica = cdPessoaJuridica;
	}
	
	/**
	 * Get: cdUnidadeOrganizacional.
	 *
	 * @return cdUnidadeOrganizacional
	 */
	public Integer getCdUnidadeOrganizacional() {
		return cdUnidadeOrganizacional;
	}
	
	/**
	 * Set: cdUnidadeOrganizacional.
	 *
	 * @param cdUnidadeOrganizacional the cd unidade organizacional
	 */
	public void setCdUnidadeOrganizacional(Integer cdUnidadeOrganizacional) {
		this.cdUnidadeOrganizacional = cdUnidadeOrganizacional;
	}
	
	/**
	 * Get: dsNomeContatoCliente.
	 *
	 * @return dsNomeContatoCliente
	 */
	public String getDsNomeContatoCliente() {
		return dsNomeContatoCliente;
	}
	
	/**
	 * Set: dsNomeContatoCliente.
	 *
	 * @param dsNomeContatoCliente the ds nome contato cliente
	 */
	public void setDsNomeContatoCliente(String dsNomeContatoCliente) {
		this.dsNomeContatoCliente = dsNomeContatoCliente;
	}
	
	/**
	 * Get: cdAreaFoneCliente.
	 *
	 * @return cdAreaFoneCliente
	 */
	public Integer getCdAreaFoneCliente() {
		return cdAreaFoneCliente;
	}
	
	/**
	 * Set: cdAreaFoneCliente.
	 *
	 * @param cdAreaFoneCliente the cd area fone cliente
	 */
	public void setCdAreaFoneCliente(Integer cdAreaFoneCliente) {
		this.cdAreaFoneCliente = cdAreaFoneCliente;
	}
	
	/**
	 * Get: cdFoneContatoCliente.
	 *
	 * @return cdFoneContatoCliente
	 */
	public String getCdFoneContatoCliente() {
		return cdFoneContatoCliente;
	}
	
	/**
	 * Set: cdFoneContatoCliente.
	 *
	 * @param cdFoneContatoCliente the cd fone contato cliente
	 */
	public void setCdFoneContatoCliente(String cdFoneContatoCliente) {
		this.cdFoneContatoCliente = cdFoneContatoCliente;
	}
	
	/**
	 * Get: cdRamalContatoCliente.
	 *
	 * @return cdRamalContatoCliente
	 */
	public String getCdRamalContatoCliente() {
		return cdRamalContatoCliente;
	}
	
	/**
	 * Set: cdRamalContatoCliente.
	 *
	 * @param cdRamalContatoCliente the cd ramal contato cliente
	 */
	public void setCdRamalContatoCliente(String cdRamalContatoCliente) {
		this.cdRamalContatoCliente = cdRamalContatoCliente;
	}
	
	/**
	 * Get: dsEmailContatoCliente.
	 *
	 * @return dsEmailContatoCliente
	 */
	public String getDsEmailContatoCliente() {
		return dsEmailContatoCliente;
	}
	
	/**
	 * Set: dsEmailContatoCliente.
	 *
	 * @param dsEmailContatoCliente the ds email contato cliente
	 */
	public void setDsEmailContatoCliente(String dsEmailContatoCliente) {
		this.dsEmailContatoCliente = dsEmailContatoCliente;
	}
	
	/**
	 * Get: dsSolicitacaoPendente.
	 *
	 * @return dsSolicitacaoPendente
	 */
	public String getDsSolicitacaoPendente() {
		return dsSolicitacaoPendente;
	}
	
	/**
	 * Set: dsSolicitacaoPendente.
	 *
	 * @param dsSolicitacaoPendente the ds solicitacao pendente
	 */
	public void setDsSolicitacaoPendente(String dsSolicitacaoPendente) {
		this.dsSolicitacaoPendente = dsSolicitacaoPendente;
	}
	
	/**
	 * Get: cdAreaFonePend.
	 *
	 * @return cdAreaFonePend
	 */
	public Integer getCdAreaFonePend() {
		return cdAreaFonePend;
	}
	
	/**
	 * Set: cdAreaFonePend.
	 *
	 * @param cdAreaFonePend the cd area fone pend
	 */
	public void setCdAreaFonePend(Integer cdAreaFonePend) {
		this.cdAreaFonePend = cdAreaFonePend;
	}
	
	/**
	 * Get: cdFoneSolicitacaoPend.
	 *
	 * @return cdFoneSolicitacaoPend
	 */
	public String getCdFoneSolicitacaoPend() {
		return cdFoneSolicitacaoPend;
	}
	
	/**
	 * Set: cdFoneSolicitacaoPend.
	 *
	 * @param cdFoneSolicitacaoPend the cd fone solicitacao pend
	 */
	public void setCdFoneSolicitacaoPend(String cdFoneSolicitacaoPend) {
		this.cdFoneSolicitacaoPend = cdFoneSolicitacaoPend;
	}
	
	/**
	 * Get: cdRamalSolctPend.
	 *
	 * @return cdRamalSolctPend
	 */
	public String getCdRamalSolctPend() {
		return cdRamalSolctPend;
	}
	
	/**
	 * Set: cdRamalSolctPend.
	 *
	 * @param cdRamalSolctPend the cd ramal solct pend
	 */
	public void setCdRamalSolctPend(String cdRamalSolctPend) {
		this.cdRamalSolctPend = cdRamalSolctPend;
	}
	
	/**
	 * Get: dsEmailSolctPend.
	 *
	 * @return dsEmailSolctPend
	 */
	public String getDsEmailSolctPend() {
		return dsEmailSolctPend;
	}
	
	/**
	 * Set: dsEmailSolctPend.
	 *
	 * @param dsEmailSolctPend the ds email solct pend
	 */
	public void setDsEmailSolctPend(String dsEmailSolctPend) {
		this.dsEmailSolctPend = dsEmailSolctPend;
	}
	
	/**
	 * Get: qtMesRegistroTrafg.
	 *
	 * @return qtMesRegistroTrafg
	 */
	public String getQtMesRegistroTrafg() {
		return qtMesRegistroTrafg;
	}
	
	/**
	 * Set: qtMesRegistroTrafg.
	 *
	 * @param qtMesRegistroTrafg the qt mes registro trafg
	 */
	public void setQtMesRegistroTrafg(String qtMesRegistroTrafg) {
		this.qtMesRegistroTrafg = qtMesRegistroTrafg;
	}
	
	/**
	 * Get: dsObsGeralPerfil.
	 *
	 * @return dsObsGeralPerfil
	 */
	public String getDsObsGeralPerfil() {
		return dsObsGeralPerfil;
	}
	
	/**
	 * Set: dsObsGeralPerfil.
	 *
	 * @param dsObsGeralPerfil the ds obs geral perfil
	 */
	public void setDsObsGeralPerfil(String dsObsGeralPerfil) {
		this.dsObsGeralPerfil = dsObsGeralPerfil;
	}
	
	/**
	 * Set: cdAplicacaoTransmPagamento.
	 *
	 * @param cdAplicacaoTransmPagamento the cd aplicacao transm pagamento
	 */
	public void setCdAplicacaoTransmPagamento(Long cdAplicacaoTransmPagamento) {
		this.cdAplicacaoTransmPagamento = cdAplicacaoTransmPagamento;
	}
	
	/**
	 * Get: cdAplicacaoTransmPagamento.
	 *
	 * @return cdAplicacaoTransmPagamento
	 */
	public Long getCdAplicacaoTransmPagamento() {
		return cdAplicacaoTransmPagamento;
	}

	/**
	 * Get: qtMesRegistroTrafgFormatado.
	 *
	 * @return qtMesRegistroTrafgFormatado
	 */
	public String getQtMesRegistroTrafgFormatado() {
		if (qtMesRegistroTrafg != null) {
			return qtMesRegistroTrafg.replace(",", ".");
		}

		return "";
	}

	/**
	 * Get: telefoneClienteFormatado.
	 *
	 * @return telefoneClienteFormatado
	 */
	public String getTelefoneClienteFormatado() {
		return NumberUtils.formatTelefone9Digitos(cdAreaFoneCliente, NumberUtils.convertTelefoneSemDdd(cdFoneContatoCliente));
	}

	/**
	 * Get: telefoneSolicitanteFormatado.
	 *
	 * @return telefoneSolicitanteFormatado
	 */
	public String getTelefoneSolicitanteFormatado() {
		return NumberUtils.formatTelefone9Digitos(cdAreaFonePend, NumberUtils.convertTelefoneSemDdd(cdFoneSolicitacaoPend));
	}

	/**
	 * Get: clienteFormatado.
	 *
	 * @return clienteFormatado
	 */
	public String getClienteFormatado() {
		return clienteFormatado;
	}
	
	/**
	 * Set: clienteFormatado.
	 *
	 * @param clienteFormatado the cliente formatado
	 */
	public void setClienteFormatado(String clienteFormatado) {
		this.clienteFormatado = clienteFormatado;
	}

	/**
	 * Get: bancoFormatado.
	 *
	 * @return bancoFormatado
	 */
	public String getBancoFormatado() {
		if (pcCustoOrganizacaoTransmissao != null && pcCustoOrganizacaoTransmissao.compareTo(BigDecimal.ZERO) == 0) {
			return "";
		}

		return PgitUtil.formatNumberPercent(pcCustoOrganizacaoTransmissao);
	}

	/**
	 * Sets the cd indicador layout proprio.
	 *
	 * @param cdIndicadorLayoutProprio the cdIndicadorLayoutProprio to set
	 */
	public void setCdIndicadorLayoutProprio(Integer cdIndicadorLayoutProprio) {
		this.cdIndicadorLayoutProprio = cdIndicadorLayoutProprio;
	}

	/**
	 * Gets the cd indicador layout proprio.
	 *
	 * @return the cdIndicadorLayoutProprio
	 */
	public Integer getCdIndicadorLayoutProprio() {
		return cdIndicadorLayoutProprio;
	}

	/**
	 * Sets the cd indicador geracao segmento b.
	 *
	 * @param cdIndicadorGeracaoSegmentoB the cdIndicadorGeracaoSegmentoB to set
	 */
	public void setCdIndicadorGeracaoSegmentoB(
			Integer cdIndicadorGeracaoSegmentoB) {
		this.cdIndicadorGeracaoSegmentoB = cdIndicadorGeracaoSegmentoB;
	}

	/**
	 * Gets the cd indicador geracao segmento b.
	 *
	 * @return the cdIndicadorGeracaoSegmentoB
	 */
	public Integer getCdIndicadorGeracaoSegmentoB() {
		return cdIndicadorGeracaoSegmentoB;
	}

	/**
	 * Sets the cd indicador geracao segmento z.
	 *
	 * @param cdIndicadorGeracaoSegmentoZ the cdIndicadorGeracaoSegmentoZ to set
	 */
	public void setCdIndicadorGeracaoSegmentoZ(
			Integer cdIndicadorGeracaoSegmentoZ) {
		this.cdIndicadorGeracaoSegmentoZ = cdIndicadorGeracaoSegmentoZ;
	}

	/**
	 * Gets the cd indicador geracao segmento z.
	 *
	 * @return the cdIndicadorGeracaoSegmentoZ
	 */
	public Integer getCdIndicadorGeracaoSegmentoZ() {
		return cdIndicadorGeracaoSegmentoZ;
	}

	/**
	 * Sets the ds segmento b.
	 *
	 * @param dsSegmentoB the dsSegmentoB to set
	 */
	public void setDsSegmentoB(String dsSegmentoB) {
		this.dsSegmentoB = dsSegmentoB;
	}

	/**
	 * Gets the ds segmento b.
	 *
	 * @return the dsSegmentoB
	 */
	public String getDsSegmentoB() {
		return dsSegmentoB;
	}

	/**
	 * Sets the ds segmento z.
	 *
	 * @param dsSegmentoZ the dsSegmentoZ to set
	 */
	public void setDsSegmentoZ(String dsSegmentoZ) {
		this.dsSegmentoZ = dsSegmentoZ;
	}

	/**
	 * Gets the ds segmento z.
	 *
	 * @return the dsSegmentoZ
	 */
	public String getDsSegmentoZ() {
		return dsSegmentoZ;
	}

	/**
	 * Gets the cd indicador cpf layout.
	 *
	 * @return the cdIndicadorCpfLayout
	 */
	public Integer getCdIndicadorCpfLayout() {
		return cdIndicadorCpfLayout;
	}

	/**
	 * Sets the cd indicador cpf layout.
	 *
	 * @param cdIndicadorCpfLayout the cdIndicadorCpfLayout to set
	 */
	public void setCdIndicadorCpfLayout(Integer cdIndicadorCpfLayout) {
		this.cdIndicadorCpfLayout = cdIndicadorCpfLayout;
	}

	/**
	 * Gets the ds indicador cpf layout.
	 *
	 * @return the dsIndicadorCpfLayout
	 */
	public String getDsIndicadorCpfLayout() {
		return dsIndicadorCpfLayout;
	}

	/**
	 * Sets the ds indicador cpf layout.
	 *
	 * @param dsIndicadorCpfLayout the dsIndicadorCpfLayout to set
	 */
	public void setDsIndicadorCpfLayout(String dsIndicadorCpfLayout) {
		this.dsIndicadorCpfLayout = dsIndicadorCpfLayout;
	}

	/**
	 * Gets the cd indicador associacao layout.
	 *
	 * @return the cdIndicadorAssociacaoLayout
	 */
	public Integer getCdIndicadorAssociacaoLayout() {
		return cdIndicadorAssociacaoLayout;
	}

	/**
	 * Sets the cd indicador associacao layout.
	 *
	 * @param cdIndicadorAssociacaoLayout the cdIndicadorAssociacaoLayout to set
	 */
	public void setCdIndicadorAssociacaoLayout(Integer cdIndicadorAssociacaoLayout) {
		this.cdIndicadorAssociacaoLayout = cdIndicadorAssociacaoLayout;
	}

	/**
	 * Gets the ds indicador associacao layout.
	 *
	 * @return the dsIndicadorAssociacaoLayout
	 */
	public String getDsIndicadorAssociacaoLayout() {
		return dsIndicadorAssociacaoLayout;
	}

	/**
	 * Sets the ds indicador associacao layout.
	 *
	 * @param dsIndicadorAssociacaoLayout the dsIndicadorAssociacaoLayout to set
	 */
	public void setDsIndicadorAssociacaoLayout(String dsIndicadorAssociacaoLayout) {
		this.dsIndicadorAssociacaoLayout = dsIndicadorAssociacaoLayout;
	}

	/**
	 * Gets the cd indicador conta complementar.
	 *
	 * @return the cdIndicadorContaComplementar
	 */
	public Integer getCdIndicadorContaComplementar() {
		return cdIndicadorContaComplementar;
	}

	/**
	 * Sets the cd indicador conta complementar.
	 *
	 * @param cdIndicadorContaComplementar the cdIndicadorContaComplementar to set
	 */
	public void setCdIndicadorContaComplementar(Integer cdIndicadorContaComplementar) {
		this.cdIndicadorContaComplementar = cdIndicadorContaComplementar;
	}

	/**
	 * Gets the ds indicador conta complementar.
	 *
	 * @return the dsIndicadorContaComplementar
	 */
	public String getDsIndicadorContaComplementar() {
		return dsIndicadorContaComplementar;
	}

	/**
	 * Sets the ds indicador conta complementar.
	 *
	 * @param dsIndicadorContaComplementar the dsIndicadorContaComplementar to set
	 */
	public void setDsIndicadorContaComplementar(String dsIndicadorContaComplementar) {
		this.dsIndicadorContaComplementar = dsIndicadorContaComplementar;
	}

	/**
	 * Sets the ds indicador consiste conta debito.
	 *
	 * @param dsIndicadorConsisteContaDebito the dsIndicadorConsisteContaDebito to set
	 */
	public void setDsIndicadorConsisteContaDebito(
			String dsIndicadorConsisteContaDebito) {
		this.dsIndicadorConsisteContaDebito = dsIndicadorConsisteContaDebito;
	}

	/**
	 * Gets the ds indicador consiste conta debito.
	 *
	 * @return the dsIndicadorConsisteContaDebito
	 */
	public String getDsIndicadorConsisteContaDebito() {
		return dsIndicadorConsisteContaDebito;
	}

	/**
	 * @return the cdIndicadorConsisteContaDebito
	 */
	public Integer getCdIndicadorConsisteContaDebito() {
		return cdIndicadorConsisteContaDebito;
	}

	/**
	 * @param cdIndicadorConsisteContaDebito the cdIndicadorConsisteContaDebito to set
	 */
	public void setCdIndicadorConsisteContaDebito(
			Integer cdIndicadorConsisteContaDebito) {
		this.cdIndicadorConsisteContaDebito = cdIndicadorConsisteContaDebito;
	}

	public Integer getCdTipoLayoutMae() {
		return cdTipoLayoutMae;
	}

	public void setCdTipoLayoutMae(Integer cdTipoLayoutMae) {
		this.cdTipoLayoutMae = cdTipoLayoutMae;
	}

	public Integer getCdTipoContratoMae() {
		return cdTipoContratoMae;
	}

	public void setCdTipoContratoMae(Integer cdTipoContratoMae) {
		this.cdTipoContratoMae = cdTipoContratoMae;
	}

	public Integer getCdTipoParticipanteMae() {
		return cdTipoParticipanteMae;
	}

	public void setCdTipoParticipanteMae(Integer cdTipoParticipanteMae) {
		this.cdTipoParticipanteMae = cdTipoParticipanteMae;
	}

	public Integer getCdFilialCnpj() {
		return cdFilialCnpj;
	}

	public void setCdFilialCnpj(Integer cdFilialCnpj) {
		this.cdFilialCnpj = cdFilialCnpj;
	}

	public Integer getCdControleCpfCnpj() {
		return cdControleCpfCnpj;
	}

	public void setCdControleCpfCnpj(Integer cdControleCpfCnpj) {
		this.cdControleCpfCnpj = cdControleCpfCnpj;
	}

	public String getDsNomeRazaoSocial() {
		return dsNomeRazaoSocial;
	}

	public void setDsNomeRazaoSocial(String dsNomeRazaoSocial) {
		this.dsNomeRazaoSocial = dsNomeRazaoSocial;
	}

	public String getDsTipoLayoutArquivo() {
		return dsTipoLayoutArquivo;
	}

	public void setDsTipoLayoutArquivo(String dsTipoLayoutArquivo) {
		this.dsTipoLayoutArquivo = dsTipoLayoutArquivo;
	}

	public Long getCdPerfilTrocaMae() {
		return cdPerfilTrocaMae;
	}

	public void setCdPerfilTrocaMae(Long cdPerfilTrocaMae) {
		this.cdPerfilTrocaMae = cdPerfilTrocaMae;
	}

	public Long getCdPessoaJuridicaMae() {
		return cdPessoaJuridicaMae;
	}

	public void setCdPessoaJuridicaMae(Long cdPessoaJuridicaMae) {
		this.cdPessoaJuridicaMae = cdPessoaJuridicaMae;
	}

	public Long getNrSequencialContratoMae() {
		return nrSequencialContratoMae;
	}

	public void setNrSequencialContratoMae(Long nrSequencialContratoMae) {
		this.nrSequencialContratoMae = nrSequencialContratoMae;
	}

	public Long getCdCpssoaMae() {
		return cdCpssoaMae;
	}

	public void setCdCpssoaMae(Long cdCpssoaMae) {
		this.cdCpssoaMae = cdCpssoaMae;
	}

	public Long getCdCpfCnpj() {
		return cdCpfCnpj;
	}

	public void setCdCpfCnpj(Long cdCpfCnpj) {
		this.cdCpfCnpj = cdCpfCnpj;
	}
}