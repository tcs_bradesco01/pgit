/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias7.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias7 implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _dsAvisoTipoAviso
     */
    private java.lang.String _dsAvisoTipoAviso;

    /**
     * Field _dsAvisoPeriodicidadeEmissao
     */
    private java.lang.String _dsAvisoPeriodicidadeEmissao;

    /**
     * Field _dsAvisoDestino
     */
    private java.lang.String _dsAvisoDestino;

    /**
     * Field _dsAvisoPermissaoCorrespondenciaAbertura
     */
    private java.lang.String _dsAvisoPermissaoCorrespondenciaAbertura;

    /**
     * Field _dsAvisoDemontracaoInformacaoReservada
     */
    private java.lang.String _dsAvisoDemontracaoInformacaoReservada;

    /**
     * Field _cdAvisoQuantidadeViasEmitir
     */
    private int _cdAvisoQuantidadeViasEmitir = 0;

    /**
     * keeps track of state for field: _cdAvisoQuantidadeViasEmitir
     */
    private boolean _has_cdAvisoQuantidadeViasEmitir;

    /**
     * Field _dsAvisoAgrupamentoCorrespondencia
     */
    private java.lang.String _dsAvisoAgrupamentoCorrespondencia;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias7() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias7()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdAvisoQuantidadeViasEmitir
     * 
     */
    public void deleteCdAvisoQuantidadeViasEmitir()
    {
        this._has_cdAvisoQuantidadeViasEmitir= false;
    } //-- void deleteCdAvisoQuantidadeViasEmitir() 

    /**
     * Returns the value of field 'cdAvisoQuantidadeViasEmitir'.
     * 
     * @return int
     * @return the value of field 'cdAvisoQuantidadeViasEmitir'.
     */
    public int getCdAvisoQuantidadeViasEmitir()
    {
        return this._cdAvisoQuantidadeViasEmitir;
    } //-- int getCdAvisoQuantidadeViasEmitir() 

    /**
     * Returns the value of field
     * 'dsAvisoAgrupamentoCorrespondencia'.
     * 
     * @return String
     * @return the value of field
     * 'dsAvisoAgrupamentoCorrespondencia'.
     */
    public java.lang.String getDsAvisoAgrupamentoCorrespondencia()
    {
        return this._dsAvisoAgrupamentoCorrespondencia;
    } //-- java.lang.String getDsAvisoAgrupamentoCorrespondencia() 

    /**
     * Returns the value of field
     * 'dsAvisoDemontracaoInformacaoReservada'.
     * 
     * @return String
     * @return the value of field
     * 'dsAvisoDemontracaoInformacaoReservada'.
     */
    public java.lang.String getDsAvisoDemontracaoInformacaoReservada()
    {
        return this._dsAvisoDemontracaoInformacaoReservada;
    } //-- java.lang.String getDsAvisoDemontracaoInformacaoReservada() 

    /**
     * Returns the value of field 'dsAvisoDestino'.
     * 
     * @return String
     * @return the value of field 'dsAvisoDestino'.
     */
    public java.lang.String getDsAvisoDestino()
    {
        return this._dsAvisoDestino;
    } //-- java.lang.String getDsAvisoDestino() 

    /**
     * Returns the value of field 'dsAvisoPeriodicidadeEmissao'.
     * 
     * @return String
     * @return the value of field 'dsAvisoPeriodicidadeEmissao'.
     */
    public java.lang.String getDsAvisoPeriodicidadeEmissao()
    {
        return this._dsAvisoPeriodicidadeEmissao;
    } //-- java.lang.String getDsAvisoPeriodicidadeEmissao() 

    /**
     * Returns the value of field
     * 'dsAvisoPermissaoCorrespondenciaAbertura'.
     * 
     * @return String
     * @return the value of field
     * 'dsAvisoPermissaoCorrespondenciaAbertura'.
     */
    public java.lang.String getDsAvisoPermissaoCorrespondenciaAbertura()
    {
        return this._dsAvisoPermissaoCorrespondenciaAbertura;
    } //-- java.lang.String getDsAvisoPermissaoCorrespondenciaAbertura() 

    /**
     * Returns the value of field 'dsAvisoTipoAviso'.
     * 
     * @return String
     * @return the value of field 'dsAvisoTipoAviso'.
     */
    public java.lang.String getDsAvisoTipoAviso()
    {
        return this._dsAvisoTipoAviso;
    } //-- java.lang.String getDsAvisoTipoAviso() 

    /**
     * Method hasCdAvisoQuantidadeViasEmitir
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAvisoQuantidadeViasEmitir()
    {
        return this._has_cdAvisoQuantidadeViasEmitir;
    } //-- boolean hasCdAvisoQuantidadeViasEmitir() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdAvisoQuantidadeViasEmitir'.
     * 
     * @param cdAvisoQuantidadeViasEmitir the value of field
     * 'cdAvisoQuantidadeViasEmitir'.
     */
    public void setCdAvisoQuantidadeViasEmitir(int cdAvisoQuantidadeViasEmitir)
    {
        this._cdAvisoQuantidadeViasEmitir = cdAvisoQuantidadeViasEmitir;
        this._has_cdAvisoQuantidadeViasEmitir = true;
    } //-- void setCdAvisoQuantidadeViasEmitir(int) 

    /**
     * Sets the value of field 'dsAvisoAgrupamentoCorrespondencia'.
     * 
     * @param dsAvisoAgrupamentoCorrespondencia the value of field
     * 'dsAvisoAgrupamentoCorrespondencia'.
     */
    public void setDsAvisoAgrupamentoCorrespondencia(java.lang.String dsAvisoAgrupamentoCorrespondencia)
    {
        this._dsAvisoAgrupamentoCorrespondencia = dsAvisoAgrupamentoCorrespondencia;
    } //-- void setDsAvisoAgrupamentoCorrespondencia(java.lang.String) 

    /**
     * Sets the value of field
     * 'dsAvisoDemontracaoInformacaoReservada'.
     * 
     * @param dsAvisoDemontracaoInformacaoReservada the value of
     * field 'dsAvisoDemontracaoInformacaoReservada'.
     */
    public void setDsAvisoDemontracaoInformacaoReservada(java.lang.String dsAvisoDemontracaoInformacaoReservada)
    {
        this._dsAvisoDemontracaoInformacaoReservada = dsAvisoDemontracaoInformacaoReservada;
    } //-- void setDsAvisoDemontracaoInformacaoReservada(java.lang.String) 

    /**
     * Sets the value of field 'dsAvisoDestino'.
     * 
     * @param dsAvisoDestino the value of field 'dsAvisoDestino'.
     */
    public void setDsAvisoDestino(java.lang.String dsAvisoDestino)
    {
        this._dsAvisoDestino = dsAvisoDestino;
    } //-- void setDsAvisoDestino(java.lang.String) 

    /**
     * Sets the value of field 'dsAvisoPeriodicidadeEmissao'.
     * 
     * @param dsAvisoPeriodicidadeEmissao the value of field
     * 'dsAvisoPeriodicidadeEmissao'.
     */
    public void setDsAvisoPeriodicidadeEmissao(java.lang.String dsAvisoPeriodicidadeEmissao)
    {
        this._dsAvisoPeriodicidadeEmissao = dsAvisoPeriodicidadeEmissao;
    } //-- void setDsAvisoPeriodicidadeEmissao(java.lang.String) 

    /**
     * Sets the value of field
     * 'dsAvisoPermissaoCorrespondenciaAbertura'.
     * 
     * @param dsAvisoPermissaoCorrespondenciaAbertura the value of
     * field 'dsAvisoPermissaoCorrespondenciaAbertura'.
     */
    public void setDsAvisoPermissaoCorrespondenciaAbertura(java.lang.String dsAvisoPermissaoCorrespondenciaAbertura)
    {
        this._dsAvisoPermissaoCorrespondenciaAbertura = dsAvisoPermissaoCorrespondenciaAbertura;
    } //-- void setDsAvisoPermissaoCorrespondenciaAbertura(java.lang.String) 

    /**
     * Sets the value of field 'dsAvisoTipoAviso'.
     * 
     * @param dsAvisoTipoAviso the value of field 'dsAvisoTipoAviso'
     */
    public void setDsAvisoTipoAviso(java.lang.String dsAvisoTipoAviso)
    {
        this._dsAvisoTipoAviso = dsAvisoTipoAviso;
    } //-- void setDsAvisoTipoAviso(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias7
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias7 unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias7) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias7.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias7 unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
