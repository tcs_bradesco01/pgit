/*
 * Nome: br.com.bradesco.web.pgit.service.business.manterformalizacaoalteracaocontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.manterformalizacaoalteracaocontrato.bean;

/**
 * Nome: ServicosDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ServicosDTO {
	
	/** Atributo dsProdutoServicoOperacao. */
	private String dsProdutoServicoOperacao;
    
    /** Atributo dsProdutoServicoRelacionado. */
    private String dsProdutoServicoRelacionado;
    
    /** Atributo dsAcaoManutencaoServico. */
    private String dsAcaoManutencaoServico;
	
	/**
	 * Get: dsProdutoServicoOperacao.
	 *
	 * @return dsProdutoServicoOperacao
	 */
	public String getDsProdutoServicoOperacao() {
		return dsProdutoServicoOperacao;
	}
	
	/**
	 * Set: dsProdutoServicoOperacao.
	 *
	 * @param dsProdutoServicoOperacao the ds produto servico operacao
	 */
	public void setDsProdutoServicoOperacao(String dsProdutoServicoOperacao) {
		this.dsProdutoServicoOperacao = dsProdutoServicoOperacao;
	}
	
	/**
	 * Get: dsProdutoServicoRelacionado.
	 *
	 * @return dsProdutoServicoRelacionado
	 */
	public String getDsProdutoServicoRelacionado() {
		return dsProdutoServicoRelacionado;
	}
	
	/**
	 * Set: dsProdutoServicoRelacionado.
	 *
	 * @param dsProdutoServicoRelacionado the ds produto servico relacionado
	 */
	public void setDsProdutoServicoRelacionado(String dsProdutoServicoRelacionado) {
		this.dsProdutoServicoRelacionado = dsProdutoServicoRelacionado;
	}
	
	/**
	 * Get: dsAcaoManutencaoServico.
	 *
	 * @return dsAcaoManutencaoServico
	 */
	public String getDsAcaoManutencaoServico() {
		return dsAcaoManutencaoServico;
	}
	
	/**
	 * Set: dsAcaoManutencaoServico.
	 *
	 * @param dsAcaoManutencaoServico the ds acao manutencao servico
	 */
	public void setDsAcaoManutencaoServico(String dsAcaoManutencaoServico) {
		this.dsAcaoManutencaoServico = dsAcaoManutencaoServico;
	}
	

}
