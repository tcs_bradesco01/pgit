/*
 * Nome: br.com.bradesco.web.pgit.service.business.arquivoretorno.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.arquivoretorno.bean;

/**
 * Nome: EstatisticaRetornoDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class EstatisticaRetornoDTO {
	//Dados do Request
	/** Atributo maximoOcorrencia. */
	private String maximoOcorrencia;
	
	/** Atributo sistemaLegado. */
	private String sistemaLegado;
	
	/** Atributo cpfCnpj. */
	private String cpfCnpj;
	
	/** Atributo codigoCpfCnpj. */
	private String codigoCpfCnpj;
	
	/** Atributo codigoFilialCnpj. */
	private String codigoFilialCnpj;
	
	/** Atributo controleCnpj. */
	private String controleCnpj;
	
	/** Atributo codigoPerfil. */
	private String codigoPerfil;
	
	/** Atributo numeroArquivoRetorno. */
	private String numeroArquivoRetorno;
	
	/** Atributo numeroLoteRetorno. */
	private String numeroLoteRetorno;
	
	//Dados do Response
	/** Atributo numeroSequenciaRetorno. */
	private String numeroSequenciaRetorno;
	
	//Linha selecionada da grid de Inconsistentes
	/** Atributo linhaSelecionada. */
	private int linhaSelecionada;
	
	/**
	 * Get: codigoFilialCnpj.
	 *
	 * @return codigoFilialCnpj
	 */
	public String getCodigoFilialCnpj() {
		return codigoFilialCnpj;
	}
	
	/**
	 * Set: codigoFilialCnpj.
	 *
	 * @param codigoFilialCnpj the codigo filial cnpj
	 */
	public void setCodigoFilialCnpj(String codigoFilialCnpj) {
		this.codigoFilialCnpj = codigoFilialCnpj;
	}
	
	/**
	 * Get: codigoPerfil.
	 *
	 * @return codigoPerfil
	 */
	public String getCodigoPerfil() {
		return codigoPerfil;
	}
	
	/**
	 * Set: codigoPerfil.
	 *
	 * @param codigoPerfil the codigo perfil
	 */
	public void setCodigoPerfil(String codigoPerfil) {
		this.codigoPerfil = codigoPerfil;
	}
	
	/**
	 * Get: controleCnpj.
	 *
	 * @return controleCnpj
	 */
	public String getControleCnpj() {
		return controleCnpj;
	}
	
	/**
	 * Set: controleCnpj.
	 *
	 * @param controleCnpj the controle cnpj
	 */
	public void setControleCnpj(String controleCnpj) {
		this.controleCnpj = controleCnpj;
	}
	
	/**
	 * Get: cpfCnpj.
	 *
	 * @return cpfCnpj
	 */
	public String getCpfCnpj() {
		return cpfCnpj;
	}
	
	/**
	 * Set: cpfCnpj.
	 *
	 * @param cpfCnpj the cpf cnpj
	 */
	public void setCpfCnpj(String cpfCnpj) {
		this.cpfCnpj = cpfCnpj;
	}
	
	/**
	 * Get: maximoOcorrencia.
	 *
	 * @return maximoOcorrencia
	 */
	public String getMaximoOcorrencia() {
		return maximoOcorrencia;
	}
	
	/**
	 * Set: maximoOcorrencia.
	 *
	 * @param maximoOcorrencia the maximo ocorrencia
	 */
	public void setMaximoOcorrencia(String maximoOcorrencia) {
		this.maximoOcorrencia = maximoOcorrencia;
	}
	
	/**
	 * Get: numeroArquivoRetorno.
	 *
	 * @return numeroArquivoRetorno
	 */
	public String getNumeroArquivoRetorno() {
		return numeroArquivoRetorno;
	}
	
	/**
	 * Set: numeroArquivoRetorno.
	 *
	 * @param numeroArquivoRetorno the numero arquivo retorno
	 */
	public void setNumeroArquivoRetorno(String numeroArquivoRetorno) {
		this.numeroArquivoRetorno = numeroArquivoRetorno;
	}
	
	/**
	 * Get: numeroLoteRetorno.
	 *
	 * @return numeroLoteRetorno
	 */
	public String getNumeroLoteRetorno() {
		return numeroLoteRetorno;
	}
	
	/**
	 * Set: numeroLoteRetorno.
	 *
	 * @param numeroLoteRetorno the numero lote retorno
	 */
	public void setNumeroLoteRetorno(String numeroLoteRetorno) {
		this.numeroLoteRetorno = numeroLoteRetorno;
	}
	
	/**
	 * Get: numeroSequenciaRetorno.
	 *
	 * @return numeroSequenciaRetorno
	 */
	public String getNumeroSequenciaRetorno() {
		return numeroSequenciaRetorno;
	}
	
	/**
	 * Set: numeroSequenciaRetorno.
	 *
	 * @param numeroSequenciaRetorno the numero sequencia retorno
	 */
	public void setNumeroSequenciaRetorno(String numeroSequenciaRetorno) {
		this.numeroSequenciaRetorno = numeroSequenciaRetorno;
	}
	
	/**
	 * Get: sistemaLegado.
	 *
	 * @return sistemaLegado
	 */
	public String getSistemaLegado() {
		return sistemaLegado;
	}
	
	/**
	 * Set: sistemaLegado.
	 *
	 * @param sistemaLegado the sistema legado
	 */
	public void setSistemaLegado(String sistemaLegado) {
		this.sistemaLegado = sistemaLegado;
	}
	
	/**
	 * Get: linhaSelecionada.
	 *
	 * @return linhaSelecionada
	 */
	public int getLinhaSelecionada() {
		return linhaSelecionada;
	}
	
	/**
	 * Set: linhaSelecionada.
	 *
	 * @param linhaSelecionada the linha selecionada
	 */
	public void setLinhaSelecionada(int linhaSelecionada) {
		this.linhaSelecionada = linhaSelecionada;
	}
	
	/**
	 * Get: codigoCpfCnpj.
	 *
	 * @return codigoCpfCnpj
	 */
	public String getCodigoCpfCnpj() {
		return codigoCpfCnpj;
	}
	
	/**
	 * Set: codigoCpfCnpj.
	 *
	 * @param codigoCpfCnpj the codigo cpf cnpj
	 */
	public void setCodigoCpfCnpj(String codigoCpfCnpj) {
		this.codigoCpfCnpj = codigoCpfCnpj;
	}
	
}