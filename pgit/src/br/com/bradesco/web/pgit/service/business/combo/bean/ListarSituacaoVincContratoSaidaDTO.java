/*
 * Nome: br.com.bradesco.web.pgit.service.business.combo.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.combo.bean;

/**
 * Nome: ListarSituacaoVincContratoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarSituacaoVincContratoSaidaDTO {
	
	/** Atributo cdSituacaoVinculacaoContrato. */
	private int cdSituacaoVinculacaoContrato;
	
	/** Atributo dsSituacaoVinculacaoCOntrato. */
	private String dsSituacaoVinculacaoCOntrato;
	
	/**
	 * Listar situacao vinc contrato saida dto.
	 */
	public ListarSituacaoVincContratoSaidaDTO(){
		super();
	}
	
	/**
	 * Listar situacao vinc contrato saida dto.
	 *
	 * @param cdSituacaoVinculacaoContrato the cd situacao vinculacao contrato
	 * @param dsSituacaoVinculacaoCOntrato the ds situacao vinculacao c ontrato
	 */
	public ListarSituacaoVincContratoSaidaDTO(int cdSituacaoVinculacaoContrato,String dsSituacaoVinculacaoCOntrato ){
		super();
		this.cdSituacaoVinculacaoContrato = cdSituacaoVinculacaoContrato;
		this.dsSituacaoVinculacaoCOntrato = dsSituacaoVinculacaoCOntrato;
	}

	/**
	 * Get: cdSituacaoVinculacaoContrato.
	 *
	 * @return cdSituacaoVinculacaoContrato
	 */
	public int getCdSituacaoVinculacaoContrato() {
		return cdSituacaoVinculacaoContrato;
	}

	/**
	 * Set: cdSituacaoVinculacaoContrato.
	 *
	 * @param cdSituacaoVinculacaoContrato the cd situacao vinculacao contrato
	 */
	public void setCdSituacaoVinculacaoContrato(int cdSituacaoVinculacaoContrato) {
		this.cdSituacaoVinculacaoContrato = cdSituacaoVinculacaoContrato;
	}

	/**
	 * Get: dsSituacaoVinculacaoCOntrato.
	 *
	 * @return dsSituacaoVinculacaoCOntrato
	 */
	public String getDsSituacaoVinculacaoCOntrato() {
		return dsSituacaoVinculacaoCOntrato;
	}

	/**
	 * Set: dsSituacaoVinculacaoCOntrato.
	 *
	 * @param dsSituacaoVinculacaoCOntrato the ds situacao vinculacao c ontrato
	 */
	public void setDsSituacaoVinculacaoCOntrato(String dsSituacaoVinculacaoCOntrato) {
		this.dsSituacaoVinculacaoCOntrato = dsSituacaoVinculacaoCOntrato;
	}
	
	
}
