/*
 * Nome: br.com.bradesco.web.pgit.service.business.manterformalizacaoalteracaocontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.manterformalizacaoalteracaocontrato.bean;

/**
 * Nome: ConsultarManterFormAltContratoEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarManterFormAltContratoEntradaDTO {
	
	/** Atributo cdAcesso. */
	private int cdAcesso;
	
	/** Atributo cdPessoaJuridicaContrato. */
	private Long cdPessoaJuridicaContrato;
	
	/** Atributo cdTipoContratoNegocio. */
	private int cdTipoContratoNegocio;
	
	/** Atributo nrSequenciaContratoNegocio. */
	private Long nrSequenciaContratoNegocio;
	
	/** Atributo nrAditivoContratoNegocio. */
	private Long nrAditivoContratoNegocio;
	
	/** Atributo cdSituacaoAditivoContrato. */
	private int cdSituacaoAditivoContrato;
	
	/** Atributo dtInicioInclusaoContrato. */
	private String dtInicioInclusaoContrato;
	
	/** Atributo dtFimInclusaoContrato. */
	private String dtFimInclusaoContrato;
	
	
	/**
	 * Get: cdAcesso.
	 *
	 * @return cdAcesso
	 */
	public int getCdAcesso() {
		return cdAcesso;
	}
	
	/**
	 * Set: cdAcesso.
	 *
	 * @param cdAcesso the cd acesso
	 */
	public void setCdAcesso(int cdAcesso) {
		this.cdAcesso = cdAcesso;
	}
	
	/**
	 * Get: cdPessoaJuridicaContrato.
	 *
	 * @return cdPessoaJuridicaContrato
	 */
	public Long getCdPessoaJuridicaContrato() {
		return cdPessoaJuridicaContrato;
	}
	
	/**
	 * Set: cdPessoaJuridicaContrato.
	 *
	 * @param cdPessoaJuridicaContrato the cd pessoa juridica contrato
	 */
	public void setCdPessoaJuridicaContrato(Long cdPessoaJuridicaContrato) {
		this.cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
	}
	
	/**
	 * Get: cdSituacaoAditivoContrato.
	 *
	 * @return cdSituacaoAditivoContrato
	 */
	public int getCdSituacaoAditivoContrato() {
		return cdSituacaoAditivoContrato;
	}
	
	/**
	 * Set: cdSituacaoAditivoContrato.
	 *
	 * @param cdSituacaoAditivoContrato the cd situacao aditivo contrato
	 */
	public void setCdSituacaoAditivoContrato(int cdSituacaoAditivoContrato) {
		this.cdSituacaoAditivoContrato = cdSituacaoAditivoContrato;
	}
	
	/**
	 * Get: cdTipoContratoNegocio.
	 *
	 * @return cdTipoContratoNegocio
	 */
	public int getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}
	
	/**
	 * Set: cdTipoContratoNegocio.
	 *
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(int cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}
	
	/**
	 * Get: dtFimInclusaoContrato.
	 *
	 * @return dtFimInclusaoContrato
	 */
	public String getDtFimInclusaoContrato() {
		return dtFimInclusaoContrato;
	}
	
	/**
	 * Set: dtFimInclusaoContrato.
	 *
	 * @param dtFimInclusaoContrato the dt fim inclusao contrato
	 */
	public void setDtFimInclusaoContrato(String dtFimInclusaoContrato) {
		this.dtFimInclusaoContrato = dtFimInclusaoContrato;
	}
	
	/**
	 * Get: dtInicioInclusaoContrato.
	 *
	 * @return dtInicioInclusaoContrato
	 */
	public String getDtInicioInclusaoContrato() {
		return dtInicioInclusaoContrato;
	}
	
	/**
	 * Set: dtInicioInclusaoContrato.
	 *
	 * @param dtInicioInclusaoContrato the dt inicio inclusao contrato
	 */
	public void setDtInicioInclusaoContrato(String dtInicioInclusaoContrato) {
		this.dtInicioInclusaoContrato = dtInicioInclusaoContrato;
	}
	
	/**
	 * Get: nrAditivoContratoNegocio.
	 *
	 * @return nrAditivoContratoNegocio
	 */
	public Long getNrAditivoContratoNegocio() {
		return nrAditivoContratoNegocio;
	}
	
	/**
	 * Set: nrAditivoContratoNegocio.
	 *
	 * @param nrAditivoContratoNegocio the nr aditivo contrato negocio
	 */
	public void setNrAditivoContratoNegocio(Long nrAditivoContratoNegocio) {
		this.nrAditivoContratoNegocio = nrAditivoContratoNegocio;
	}
	
	/**
	 * Get: nrSequenciaContratoNegocio.
	 *
	 * @return nrSequenciaContratoNegocio
	 */
	public Long getNrSequenciaContratoNegocio() {
		return nrSequenciaContratoNegocio;
	}
	
	/**
	 * Set: nrSequenciaContratoNegocio.
	 *
	 * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
	 */
	public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}
	

}
