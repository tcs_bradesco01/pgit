/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantercomptiposervicoformalanc.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantercomptiposervicoformalanc.bean;

/**
 * Nome: ConsultarServicoLancamentoCnabSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarServicoLancamentoCnabSaidaDTO{
	
	/** Atributo codMensagem. */
	private String codMensagem;
	
	/** Atributo mensagem. */
	private String mensagem;
	
	/** Atributo numeroLinhas. */
	private Integer numeroLinhas;
	
	/** Atributo cdFormaLancamentoCnab. */
	private Integer cdFormaLancamentoCnab;
	
	/** Atributo cdTipoServicoCnab. */
	private Integer cdTipoServicoCnab;
	
	/** Atributo cdFormaLiquidacao. */
	private Integer cdFormaLiquidacao;
	
	/** Atributo cdProdutoServicoOperacao. */
	private Integer cdProdutoServicoOperacao;
	
	/** Atributo cdProdutoOperacaoRelacionado. */
	private Integer cdProdutoOperacaoRelacionado;
	
        /** Atributo dsFormaLancamentoCnab. */
        private String  dsFormaLancamentoCnab;
        
        /** Atributo dsTipoServicoCnab. */
        private String  dsTipoServicoCnab;
        
        /** Atributo dsFormaLiquidacao. */
        private String  dsFormaLiquidacao;
        
        /** Atributo dsProdutoServicoOperacao. */
        private String  dsProdutoServicoOperacao;        
        
        /** Atributo dsProdutoServicoRelacionado. */
        private String  dsProdutoServicoRelacionado;

	/**
	 * Get: dsFormaLancamentoCnab.
	 *
	 * @return dsFormaLancamentoCnab
	 */
	public String getDsFormaLancamentoCnab() {
	    return dsFormaLancamentoCnab;
	}

	/**
	 * Set: dsFormaLancamentoCnab.
	 *
	 * @param dsFormaLancamentoCnab the ds forma lancamento cnab
	 */
	public void setDsFormaLancamentoCnab(String dsFormaLancamentoCnab) {
	    this.dsFormaLancamentoCnab = dsFormaLancamentoCnab;
	}

	/**
	 * Get: dsFormaLiquidacao.
	 *
	 * @return dsFormaLiquidacao
	 */
	public String getDsFormaLiquidacao() {
	    return dsFormaLiquidacao;
	}

	/**
	 * Set: dsFormaLiquidacao.
	 *
	 * @param dsFormaLiquidacao the ds forma liquidacao
	 */
	public void setDsFormaLiquidacao(String dsFormaLiquidacao) {
	    this.dsFormaLiquidacao = dsFormaLiquidacao;
	}

	/**
	 * Get: dsProdutoServicoOperacao.
	 *
	 * @return dsProdutoServicoOperacao
	 */
	public String getDsProdutoServicoOperacao() {
	    return dsProdutoServicoOperacao;
	}

	/**
	 * Set: dsProdutoServicoOperacao.
	 *
	 * @param dsProdutoServicoOperacao the ds produto servico operacao
	 */
	public void setDsProdutoServicoOperacao(String dsProdutoServicoOperacao) {
	    this.dsProdutoServicoOperacao = dsProdutoServicoOperacao;
	}

	/**
	 * Get: dsProdutoServicoRelacionado.
	 *
	 * @return dsProdutoServicoRelacionado
	 */
	public String getDsProdutoServicoRelacionado() {
	    return dsProdutoServicoRelacionado;
	}

	/**
	 * Set: dsProdutoServicoRelacionado.
	 *
	 * @param dsProdutoServicoRelacionado the ds produto servico relacionado
	 */
	public void setDsProdutoServicoRelacionado(String dsProdutoServicoRelacionado) {
	    this.dsProdutoServicoRelacionado = dsProdutoServicoRelacionado;
	}

	/**
	 * Get: dsTipoServicoCnab.
	 *
	 * @return dsTipoServicoCnab
	 */
	public String getDsTipoServicoCnab() {
	    return dsTipoServicoCnab;
	}

	/**
	 * Set: dsTipoServicoCnab.
	 *
	 * @param dsTipoServicoCnab the ds tipo servico cnab
	 */
	public void setDsTipoServicoCnab(String dsTipoServicoCnab) {
	    this.dsTipoServicoCnab = dsTipoServicoCnab;
	}

	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem(){
		return codMensagem;
	}

	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem){
		this.codMensagem = codMensagem;
	}

	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem(){
		return mensagem;
	}

	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem){
		this.mensagem = mensagem;
	}

	/**
	 * Get: numeroLinhas.
	 *
	 * @return numeroLinhas
	 */
	public Integer getNumeroLinhas(){
		return numeroLinhas;
	}

	/**
	 * Set: numeroLinhas.
	 *
	 * @param numeroLinhas the numero linhas
	 */
	public void setNumeroLinhas(Integer numeroLinhas){
		this.numeroLinhas = numeroLinhas;
	}

	/**
	 * Get: cdFormaLancamentoCnab.
	 *
	 * @return cdFormaLancamentoCnab
	 */
	public Integer getCdFormaLancamentoCnab(){
		return cdFormaLancamentoCnab;
	}

	/**
	 * Set: cdFormaLancamentoCnab.
	 *
	 * @param cdFormaLancamentoCnab the cd forma lancamento cnab
	 */
	public void setCdFormaLancamentoCnab(Integer cdFormaLancamentoCnab){
		this.cdFormaLancamentoCnab = cdFormaLancamentoCnab;
	}

	/**
	 * Get: cdTipoServicoCnab.
	 *
	 * @return cdTipoServicoCnab
	 */
	public Integer getCdTipoServicoCnab(){
		return cdTipoServicoCnab;
	}

	/**
	 * Set: cdTipoServicoCnab.
	 *
	 * @param cdTipoServicoCnab the cd tipo servico cnab
	 */
	public void setCdTipoServicoCnab(Integer cdTipoServicoCnab){
		this.cdTipoServicoCnab = cdTipoServicoCnab;
	}

	/**
	 * Get: cdFormaLiquidacao.
	 *
	 * @return cdFormaLiquidacao
	 */
	public Integer getCdFormaLiquidacao(){
		return cdFormaLiquidacao;
	}

	/**
	 * Set: cdFormaLiquidacao.
	 *
	 * @param cdFormaLiquidacao the cd forma liquidacao
	 */
	public void setCdFormaLiquidacao(Integer cdFormaLiquidacao){
		this.cdFormaLiquidacao = cdFormaLiquidacao;
	}

	/**
	 * Get: cdProdutoServicoOperacao.
	 *
	 * @return cdProdutoServicoOperacao
	 */
	public Integer getCdProdutoServicoOperacao(){
		return cdProdutoServicoOperacao;
	}

	/**
	 * Set: cdProdutoServicoOperacao.
	 *
	 * @param cdProdutoServicoOperacao the cd produto servico operacao
	 */
	public void setCdProdutoServicoOperacao(Integer cdProdutoServicoOperacao){
		this.cdProdutoServicoOperacao = cdProdutoServicoOperacao;
	}

	/**
	 * Get: cdProdutoOperacaoRelacionado.
	 *
	 * @return cdProdutoOperacaoRelacionado
	 */
	public Integer getCdProdutoOperacaoRelacionado(){
		return cdProdutoOperacaoRelacionado;
	}

	/**
	 * Set: cdProdutoOperacaoRelacionado.
	 *
	 * @param cdProdutoOperacaoRelacionado the cd produto operacao relacionado
	 */
	public void setCdProdutoOperacaoRelacionado(Integer cdProdutoOperacaoRelacionado){
		this.cdProdutoOperacaoRelacionado = cdProdutoOperacaoRelacionado;
	}
}