package br.com.bradesco.web.pgit.service.business.mantercontrato.bean;

import java.util.List;

// TODO: Auto-generated Javadoc
/**
 * Arquivo criado em 03/05/17.
 */
public class ListarContasExclusaoSaidaDTO {

    /** The cod mensagem. */
    private String codMensagem;

    /** The mensagem. */
    private String mensagem;

    /** The numero consultas. */
    private Integer numeroConsultas;

    /** The ocorrencias. */
    private List<ListarContasExclusaoOcorrenciasSaidaDTO> ocorrencias;

    /**
     * Set cod mensagem.
     *
     * @param codMensagem the cod mensagem
     */
    public void setCodMensagem(String codMensagem) {
        this.codMensagem = codMensagem;
    }

    /**
     * Get cod mensagem.
     *
     * @return the cod mensagem
     */
    public String getCodMensagem() {
        return this.codMensagem;
    }

    /**
     * Set mensagem.
     *
     * @param mensagem the mensagem
     */
    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }

    /**
     * Get mensagem.
     *
     * @return the mensagem
     */
    public String getMensagem() {
        return this.mensagem;
    }

    /**
     * Set numero consultas.
     *
     * @param numeroConsultas the numero consultas
     */
    public void setNumeroConsultas(Integer numeroConsultas) {
        this.numeroConsultas = numeroConsultas;
    }

    /**
     * Get numero consultas.
     *
     * @return the numero consultas
     */
    public Integer getNumeroConsultas() {
        return this.numeroConsultas;
    }

    /**
     * Set ocorrencias.
     *
     * @param ocorrencias the ocorrencias
     */
    public void setOcorrencias(List<ListarContasExclusaoOcorrenciasSaidaDTO> ocorrencias) {
        this.ocorrencias = ocorrencias;
    }

    /**
     * Get ocorrencias.
     *
     * @return the ocorrencias
     */
    public List<ListarContasExclusaoOcorrenciasSaidaDTO> getOcorrencias() {
        return this.ocorrencias;
    }
}