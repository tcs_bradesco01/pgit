/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultardadoscliente.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.math.BigDecimal;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ConsultarDadosClienteResponse.
 * 
 * @version $Revision$ $Date$
 */
public class ConsultarDadosClienteResponse implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _codMensagem
     */
    private java.lang.String _codMensagem;

    /**
     * Field _mensagem
     */
    private java.lang.String _mensagem;

    /**
     * Field _cdTIpoServDoc
     */
    private int _cdTIpoServDoc = 0;

    /**
     * keeps track of state for field: _cdTIpoServDoc
     */
    private boolean _has_cdTIpoServDoc;

    /**
     * Field _cdTipoModalidadeDoc
     */
    private int _cdTipoModalidadeDoc = 0;

    /**
     * keeps track of state for field: _cdTipoModalidadeDoc
     */
    private boolean _has_cdTipoModalidadeDoc;

    /**
     * Field _qtDoc
     */
    private long _qtDoc = 0;

    /**
     * keeps track of state for field: _qtDoc
     */
    private boolean _has_qtDoc;

    /**
     * Field _vlTotalDoc
     */
    private java.math.BigDecimal _vlTotalDoc = new java.math.BigDecimal("0");

    /**
     * Field _cdTipoServTed
     */
    private int _cdTipoServTed = 0;

    /**
     * keeps track of state for field: _cdTipoServTed
     */
    private boolean _has_cdTipoServTed;

    /**
     * Field _cdTipoModTed
     */
    private int _cdTipoModTed = 0;

    /**
     * keeps track of state for field: _cdTipoModTed
     */
    private boolean _has_cdTipoModTed;

    /**
     * Field _qtTEd
     */
    private long _qtTEd = 0;

    /**
     * keeps track of state for field: _qtTEd
     */
    private boolean _has_qtTEd;

    /**
     * Field _vlTotalTed
     */
    private java.math.BigDecimal _vlTotalTed = new java.math.BigDecimal("0");

    /**
     * Field _cdTipoSevOp
     */
    private int _cdTipoSevOp = 0;

    /**
     * keeps track of state for field: _cdTipoSevOp
     */
    private boolean _has_cdTipoSevOp;

    /**
     * Field _cdTIpoModOp
     */
    private int _cdTIpoModOp = 0;

    /**
     * keeps track of state for field: _cdTIpoModOp
     */
    private boolean _has_cdTIpoModOp;

    /**
     * Field _qtOp
     */
    private long _qtOp = 0;

    /**
     * keeps track of state for field: _qtOp
     */
    private boolean _has_qtOp;

    /**
     * Field _vlTotalOp
     */
    private java.math.BigDecimal _vlTotalOp = new java.math.BigDecimal("0");

    /**
     * Field _cdTipoServCodConta
     */
    private int _cdTipoServCodConta = 0;

    /**
     * keeps track of state for field: _cdTipoServCodConta
     */
    private boolean _has_cdTipoServCodConta;

    /**
     * Field _cdTipoModCodConta
     */
    private int _cdTipoModCodConta = 0;

    /**
     * keeps track of state for field: _cdTipoModCodConta
     */
    private boolean _has_cdTipoModCodConta;

    /**
     * Field _qtCodConta
     */
    private long _qtCodConta = 0;

    /**
     * keeps track of state for field: _qtCodConta
     */
    private boolean _has_qtCodConta;

    /**
     * Field _vlTotalCodConta
     */
    private java.math.BigDecimal _vlTotalCodConta = new java.math.BigDecimal("0");

    /**
     * Field _cdTipoServDebConta
     */
    private int _cdTipoServDebConta = 0;

    /**
     * keeps track of state for field: _cdTipoServDebConta
     */
    private boolean _has_cdTipoServDebConta;

    /**
     * Field _cdTipoModDebConta
     */
    private int _cdTipoModDebConta = 0;

    /**
     * keeps track of state for field: _cdTipoModDebConta
     */
    private boolean _has_cdTipoModDebConta;

    /**
     * Field _qtDebConta
     */
    private long _qtDebConta = 0;

    /**
     * keeps track of state for field: _qtDebConta
     */
    private boolean _has_qtDebConta;

    /**
     * Field _vlTotalDebConta
     */
    private java.math.BigDecimal _vlTotalDebConta = new java.math.BigDecimal("0");

    /**
     * Field _cdTipoServTitBd
     */
    private int _cdTipoServTitBd = 0;

    /**
     * keeps track of state for field: _cdTipoServTitBd
     */
    private boolean _has_cdTipoServTitBd;

    /**
     * Field _cdTipoModTitBd
     */
    private int _cdTipoModTitBd = 0;

    /**
     * keeps track of state for field: _cdTipoModTitBd
     */
    private boolean _has_cdTipoModTitBd;

    /**
     * Field _qtTitBd
     */
    private long _qtTitBd = 0;

    /**
     * keeps track of state for field: _qtTitBd
     */
    private boolean _has_qtTitBd;

    /**
     * Field _vlTotalTitBd
     */
    private java.math.BigDecimal _vlTotalTitBd = new java.math.BigDecimal("0");

    /**
     * Field _cdTipoServTitOutros
     */
    private int _cdTipoServTitOutros = 0;

    /**
     * keeps track of state for field: _cdTipoServTitOutros
     */
    private boolean _has_cdTipoServTitOutros;

    /**
     * Field _cdTipoMOdTitOutros
     */
    private int _cdTipoMOdTitOutros = 0;

    /**
     * keeps track of state for field: _cdTipoMOdTitOutros
     */
    private boolean _has_cdTipoMOdTitOutros;

    /**
     * Field _qtTitOutros
     */
    private long _qtTitOutros = 0;

    /**
     * keeps track of state for field: _qtTitOutros
     */
    private boolean _has_qtTitOutros;

    /**
     * Field _vlTotalTitOutros
     */
    private java.math.BigDecimal _vlTotalTitOutros = new java.math.BigDecimal("0");

    /**
     * Field _cdTIpoServDepIden
     */
    private int _cdTIpoServDepIden = 0;

    /**
     * keeps track of state for field: _cdTIpoServDepIden
     */
    private boolean _has_cdTIpoServDepIden;

    /**
     * Field _cdModDepIden
     */
    private int _cdModDepIden = 0;

    /**
     * keeps track of state for field: _cdModDepIden
     */
    private boolean _has_cdModDepIden;

    /**
     * Field _qtDepIden
     */
    private long _qtDepIden = 0;

    /**
     * keeps track of state for field: _qtDepIden
     */
    private boolean _has_qtDepIden;

    /**
     * Field _vlTotalDepIden
     */
    private java.math.BigDecimal _vlTotalDepIden = new java.math.BigDecimal("0");

    /**
     * Field _cdTIpoServOrdCerd
     */
    private int _cdTIpoServOrdCerd = 0;

    /**
     * keeps track of state for field: _cdTIpoServOrdCerd
     */
    private boolean _has_cdTIpoServOrdCerd;

    /**
     * Field _cdTipoModOrdCred
     */
    private int _cdTipoModOrdCred = 0;

    /**
     * keeps track of state for field: _cdTipoModOrdCred
     */
    private boolean _has_cdTipoModOrdCred;

    /**
     * Field _qtOrdCred
     */
    private long _qtOrdCred = 0;

    /**
     * keeps track of state for field: _qtOrdCred
     */
    private boolean _has_qtOrdCred;

    /**
     * Field _vlTotalOrdCred
     */
    private java.math.BigDecimal _vlTotalOrdCred = new java.math.BigDecimal("0");

    /**
     * Field _qtParfor
     */
    private long _qtParfor = 0;

    /**
     * keeps track of state for field: _qtParfor
     */
    private boolean _has_qtParfor;

    /**
     * Field _clTotalPagfor
     */
    private java.math.BigDecimal _clTotalPagfor = new java.math.BigDecimal("0");

    /**
     * Field _cdTipoServicoSalDoc
     */
    private int _cdTipoServicoSalDoc = 0;

    /**
     * keeps track of state for field: _cdTipoServicoSalDoc
     */
    private boolean _has_cdTipoServicoSalDoc;

    /**
     * Field _cdTipoModalidadeSalDoc
     */
    private int _cdTipoModalidadeSalDoc = 0;

    /**
     * keeps track of state for field: _cdTipoModalidadeSalDoc
     */
    private boolean _has_cdTipoModalidadeSalDoc;

    /**
     * Field _cdQuantidadeSal
     */
    private long _cdQuantidadeSal = 0;

    /**
     * keeps track of state for field: _cdQuantidadeSal
     */
    private boolean _has_cdQuantidadeSal;

    /**
     * Field _vlTotalSalDoc
     */
    private java.math.BigDecimal _vlTotalSalDoc = new java.math.BigDecimal("0");

    /**
     * Field _cdTipoServicoSalTed
     */
    private int _cdTipoServicoSalTed = 0;

    /**
     * keeps track of state for field: _cdTipoServicoSalTed
     */
    private boolean _has_cdTipoServicoSalTed;

    /**
     * Field _cdTipoModalidadeSalTed
     */
    private int _cdTipoModalidadeSalTed = 0;

    /**
     * keeps track of state for field: _cdTipoModalidadeSalTed
     */
    private boolean _has_cdTipoModalidadeSalTed;

    /**
     * Field _cdQuantidadeSalTed
     */
    private long _cdQuantidadeSalTed = 0;

    /**
     * keeps track of state for field: _cdQuantidadeSalTed
     */
    private boolean _has_cdQuantidadeSalTed;

    /**
     * Field _vlTotalSalTed
     */
    private java.math.BigDecimal _vlTotalSalTed = new java.math.BigDecimal("0");

    /**
     * Field _cdTipoSevicoSalOp
     */
    private int _cdTipoSevicoSalOp = 0;

    /**
     * keeps track of state for field: _cdTipoSevicoSalOp
     */
    private boolean _has_cdTipoSevicoSalOp;

    /**
     * Field _cdTipoModalidadeSalOp
     */
    private int _cdTipoModalidadeSalOp = 0;

    /**
     * keeps track of state for field: _cdTipoModalidadeSalOp
     */
    private boolean _has_cdTipoModalidadeSalOp;

    /**
     * Field _qtSalOp
     */
    private long _qtSalOp = 0;

    /**
     * keeps track of state for field: _qtSalOp
     */
    private boolean _has_qtSalOp;

    /**
     * Field _vlTotalSalOp
     */
    private java.math.BigDecimal _vlTotalSalOp = new java.math.BigDecimal("0");

    /**
     * Field _cdTIpoServicoSalConta
     */
    private int _cdTIpoServicoSalConta = 0;

    /**
     * keeps track of state for field: _cdTIpoServicoSalConta
     */
    private boolean _has_cdTIpoServicoSalConta;

    /**
     * Field _cdTipoMadalidadeSalConta
     */
    private int _cdTipoMadalidadeSalConta = 0;

    /**
     * keeps track of state for field: _cdTipoMadalidadeSalConta
     */
    private boolean _has_cdTipoMadalidadeSalConta;

    /**
     * Field _qtSalConta
     */
    private long _qtSalConta = 0;

    /**
     * keeps track of state for field: _qtSalConta
     */
    private boolean _has_qtSalConta;

    /**
     * Field _vlTotalSalConta
     */
    private java.math.BigDecimal _vlTotalSalConta = new java.math.BigDecimal("0");

    /**
     * Field _qtPagSal
     */
    private long _qtPagSal = 0;

    /**
     * keeps track of state for field: _qtPagSal
     */
    private boolean _has_qtPagSal;

    /**
     * Field _vlTotalPagSal
     */
    private java.math.BigDecimal _vlTotalPagSal = new java.math.BigDecimal("0");

    /**
     * Field _cdTipoServDarf
     */
    private int _cdTipoServDarf = 0;

    /**
     * keeps track of state for field: _cdTipoServDarf
     */
    private boolean _has_cdTipoServDarf;

    /**
     * Field _cdTipoModDarf
     */
    private int _cdTipoModDarf = 0;

    /**
     * keeps track of state for field: _cdTipoModDarf
     */
    private boolean _has_cdTipoModDarf;

    /**
     * Field _qtDarf
     */
    private long _qtDarf = 0;

    /**
     * keeps track of state for field: _qtDarf
     */
    private boolean _has_qtDarf;

    /**
     * Field _vlTotalDarf
     */
    private java.math.BigDecimal _vlTotalDarf = new java.math.BigDecimal("0");

    /**
     * Field _cdTipoServicoGps
     */
    private int _cdTipoServicoGps = 0;

    /**
     * keeps track of state for field: _cdTipoServicoGps
     */
    private boolean _has_cdTipoServicoGps;

    /**
     * Field _cdTipoModalidadeGps
     */
    private int _cdTipoModalidadeGps = 0;

    /**
     * keeps track of state for field: _cdTipoModalidadeGps
     */
    private boolean _has_cdTipoModalidadeGps;

    /**
     * Field _qtGps
     */
    private long _qtGps = 0;

    /**
     * keeps track of state for field: _qtGps
     */
    private boolean _has_qtGps;

    /**
     * Field _vlTotalGps
     */
    private java.math.BigDecimal _vlTotalGps = new java.math.BigDecimal("0");

    /**
     * Field _cdTipoServicoGare
     */
    private int _cdTipoServicoGare = 0;

    /**
     * keeps track of state for field: _cdTipoServicoGare
     */
    private boolean _has_cdTipoServicoGare;

    /**
     * Field _cdTipoModalidadeGare
     */
    private int _cdTipoModalidadeGare = 0;

    /**
     * keeps track of state for field: _cdTipoModalidadeGare
     */
    private boolean _has_cdTipoModalidadeGare;

    /**
     * Field _qtGare
     */
    private long _qtGare = 0;

    /**
     * keeps track of state for field: _qtGare
     */
    private boolean _has_qtGare;

    /**
     * Field _vlTotalGare
     */
    private java.math.BigDecimal _vlTotalGare = new java.math.BigDecimal("0");

    /**
     * Field _cdTipoServicoDebVei
     */
    private int _cdTipoServicoDebVei = 0;

    /**
     * keeps track of state for field: _cdTipoServicoDebVei
     */
    private boolean _has_cdTipoServicoDebVei;

    /**
     * Field _cdTipoModalidadeDebVei
     */
    private int _cdTipoModalidadeDebVei = 0;

    /**
     * keeps track of state for field: _cdTipoModalidadeDebVei
     */
    private boolean _has_cdTipoModalidadeDebVei;

    /**
     * Field _qtDebVei
     */
    private long _qtDebVei = 0;

    /**
     * keeps track of state for field: _qtDebVei
     */
    private boolean _has_qtDebVei;

    /**
     * Field _vlTotalDebVei
     */
    private java.math.BigDecimal _vlTotalDebVei = new java.math.BigDecimal("0");

    /**
     * Field _cdTipoServicoCodBarra
     */
    private int _cdTipoServicoCodBarra = 0;

    /**
     * keeps track of state for field: _cdTipoServicoCodBarra
     */
    private boolean _has_cdTipoServicoCodBarra;

    /**
     * Field _cdTipoModalidadeVS
     */
    private int _cdTipoModalidadeVS = 0;

    /**
     * keeps track of state for field: _cdTipoModalidadeVS
     */
    private boolean _has_cdTipoModalidadeVS;

    /**
     * Field _qtCodBarra
     */
    private long _qtCodBarra = 0;

    /**
     * keeps track of state for field: _qtCodBarra
     */
    private boolean _has_qtCodBarra;

    /**
     * Field _vlTotalCodBarra
     */
    private java.math.BigDecimal _vlTotalCodBarra = new java.math.BigDecimal("0");

    /**
     * Field _qtPagBen
     */
    private long _qtPagBen = 0;

    /**
     * keeps track of state for field: _qtPagBen
     */
    private boolean _has_qtPagBen;

    /**
     * Field _vlTotalPagBen
     */
    private java.math.BigDecimal _vlTotalPagBen = new java.math.BigDecimal("0");


      //----------------/
     //- Constructors -/
    //----------------/

    public ConsultarDadosClienteResponse() 
     {
        super();
        setVlTotalDoc(new java.math.BigDecimal("0"));
        setVlTotalTed(new java.math.BigDecimal("0"));
        setVlTotalOp(new java.math.BigDecimal("0"));
        setVlTotalCodConta(new java.math.BigDecimal("0"));
        setVlTotalDebConta(new java.math.BigDecimal("0"));
        setVlTotalTitBd(new java.math.BigDecimal("0"));
        setVlTotalTitOutros(new java.math.BigDecimal("0"));
        setVlTotalDepIden(new java.math.BigDecimal("0"));
        setVlTotalOrdCred(new java.math.BigDecimal("0"));
        setClTotalPagfor(new java.math.BigDecimal("0"));
        setVlTotalSalDoc(new java.math.BigDecimal("0"));
        setVlTotalSalTed(new java.math.BigDecimal("0"));
        setVlTotalSalOp(new java.math.BigDecimal("0"));
        setVlTotalSalConta(new java.math.BigDecimal("0"));
        setVlTotalPagSal(new java.math.BigDecimal("0"));
        setVlTotalDarf(new java.math.BigDecimal("0"));
        setVlTotalGps(new java.math.BigDecimal("0"));
        setVlTotalGare(new java.math.BigDecimal("0"));
        setVlTotalDebVei(new java.math.BigDecimal("0"));
        setVlTotalCodBarra(new java.math.BigDecimal("0"));
        setVlTotalPagBen(new java.math.BigDecimal("0"));
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultardadoscliente.response.ConsultarDadosClienteResponse()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdModDepIden
     * 
     */
    public void deleteCdModDepIden()
    {
        this._has_cdModDepIden= false;
    } //-- void deleteCdModDepIden() 

    /**
     * Method deleteCdQuantidadeSal
     * 
     */
    public void deleteCdQuantidadeSal()
    {
        this._has_cdQuantidadeSal= false;
    } //-- void deleteCdQuantidadeSal() 

    /**
     * Method deleteCdQuantidadeSalTed
     * 
     */
    public void deleteCdQuantidadeSalTed()
    {
        this._has_cdQuantidadeSalTed= false;
    } //-- void deleteCdQuantidadeSalTed() 

    /**
     * Method deleteCdTIpoModOp
     * 
     */
    public void deleteCdTIpoModOp()
    {
        this._has_cdTIpoModOp= false;
    } //-- void deleteCdTIpoModOp() 

    /**
     * Method deleteCdTIpoServDepIden
     * 
     */
    public void deleteCdTIpoServDepIden()
    {
        this._has_cdTIpoServDepIden= false;
    } //-- void deleteCdTIpoServDepIden() 

    /**
     * Method deleteCdTIpoServDoc
     * 
     */
    public void deleteCdTIpoServDoc()
    {
        this._has_cdTIpoServDoc= false;
    } //-- void deleteCdTIpoServDoc() 

    /**
     * Method deleteCdTIpoServOrdCerd
     * 
     */
    public void deleteCdTIpoServOrdCerd()
    {
        this._has_cdTIpoServOrdCerd= false;
    } //-- void deleteCdTIpoServOrdCerd() 

    /**
     * Method deleteCdTIpoServicoSalConta
     * 
     */
    public void deleteCdTIpoServicoSalConta()
    {
        this._has_cdTIpoServicoSalConta= false;
    } //-- void deleteCdTIpoServicoSalConta() 

    /**
     * Method deleteCdTipoMOdTitOutros
     * 
     */
    public void deleteCdTipoMOdTitOutros()
    {
        this._has_cdTipoMOdTitOutros= false;
    } //-- void deleteCdTipoMOdTitOutros() 

    /**
     * Method deleteCdTipoMadalidadeSalConta
     * 
     */
    public void deleteCdTipoMadalidadeSalConta()
    {
        this._has_cdTipoMadalidadeSalConta= false;
    } //-- void deleteCdTipoMadalidadeSalConta() 

    /**
     * Method deleteCdTipoModCodConta
     * 
     */
    public void deleteCdTipoModCodConta()
    {
        this._has_cdTipoModCodConta= false;
    } //-- void deleteCdTipoModCodConta() 

    /**
     * Method deleteCdTipoModDarf
     * 
     */
    public void deleteCdTipoModDarf()
    {
        this._has_cdTipoModDarf= false;
    } //-- void deleteCdTipoModDarf() 

    /**
     * Method deleteCdTipoModDebConta
     * 
     */
    public void deleteCdTipoModDebConta()
    {
        this._has_cdTipoModDebConta= false;
    } //-- void deleteCdTipoModDebConta() 

    /**
     * Method deleteCdTipoModOrdCred
     * 
     */
    public void deleteCdTipoModOrdCred()
    {
        this._has_cdTipoModOrdCred= false;
    } //-- void deleteCdTipoModOrdCred() 

    /**
     * Method deleteCdTipoModTed
     * 
     */
    public void deleteCdTipoModTed()
    {
        this._has_cdTipoModTed= false;
    } //-- void deleteCdTipoModTed() 

    /**
     * Method deleteCdTipoModTitBd
     * 
     */
    public void deleteCdTipoModTitBd()
    {
        this._has_cdTipoModTitBd= false;
    } //-- void deleteCdTipoModTitBd() 

    /**
     * Method deleteCdTipoModalidadeDebVei
     * 
     */
    public void deleteCdTipoModalidadeDebVei()
    {
        this._has_cdTipoModalidadeDebVei= false;
    } //-- void deleteCdTipoModalidadeDebVei() 

    /**
     * Method deleteCdTipoModalidadeDoc
     * 
     */
    public void deleteCdTipoModalidadeDoc()
    {
        this._has_cdTipoModalidadeDoc= false;
    } //-- void deleteCdTipoModalidadeDoc() 

    /**
     * Method deleteCdTipoModalidadeGare
     * 
     */
    public void deleteCdTipoModalidadeGare()
    {
        this._has_cdTipoModalidadeGare= false;
    } //-- void deleteCdTipoModalidadeGare() 

    /**
     * Method deleteCdTipoModalidadeGps
     * 
     */
    public void deleteCdTipoModalidadeGps()
    {
        this._has_cdTipoModalidadeGps= false;
    } //-- void deleteCdTipoModalidadeGps() 

    /**
     * Method deleteCdTipoModalidadeSalDoc
     * 
     */
    public void deleteCdTipoModalidadeSalDoc()
    {
        this._has_cdTipoModalidadeSalDoc= false;
    } //-- void deleteCdTipoModalidadeSalDoc() 

    /**
     * Method deleteCdTipoModalidadeSalOp
     * 
     */
    public void deleteCdTipoModalidadeSalOp()
    {
        this._has_cdTipoModalidadeSalOp= false;
    } //-- void deleteCdTipoModalidadeSalOp() 

    /**
     * Method deleteCdTipoModalidadeSalTed
     * 
     */
    public void deleteCdTipoModalidadeSalTed()
    {
        this._has_cdTipoModalidadeSalTed= false;
    } //-- void deleteCdTipoModalidadeSalTed() 

    /**
     * Method deleteCdTipoModalidadeVS
     * 
     */
    public void deleteCdTipoModalidadeVS()
    {
        this._has_cdTipoModalidadeVS= false;
    } //-- void deleteCdTipoModalidadeVS() 

    /**
     * Method deleteCdTipoServCodConta
     * 
     */
    public void deleteCdTipoServCodConta()
    {
        this._has_cdTipoServCodConta= false;
    } //-- void deleteCdTipoServCodConta() 

    /**
     * Method deleteCdTipoServDarf
     * 
     */
    public void deleteCdTipoServDarf()
    {
        this._has_cdTipoServDarf= false;
    } //-- void deleteCdTipoServDarf() 

    /**
     * Method deleteCdTipoServDebConta
     * 
     */
    public void deleteCdTipoServDebConta()
    {
        this._has_cdTipoServDebConta= false;
    } //-- void deleteCdTipoServDebConta() 

    /**
     * Method deleteCdTipoServTed
     * 
     */
    public void deleteCdTipoServTed()
    {
        this._has_cdTipoServTed= false;
    } //-- void deleteCdTipoServTed() 

    /**
     * Method deleteCdTipoServTitBd
     * 
     */
    public void deleteCdTipoServTitBd()
    {
        this._has_cdTipoServTitBd= false;
    } //-- void deleteCdTipoServTitBd() 

    /**
     * Method deleteCdTipoServTitOutros
     * 
     */
    public void deleteCdTipoServTitOutros()
    {
        this._has_cdTipoServTitOutros= false;
    } //-- void deleteCdTipoServTitOutros() 

    /**
     * Method deleteCdTipoServicoCodBarra
     * 
     */
    public void deleteCdTipoServicoCodBarra()
    {
        this._has_cdTipoServicoCodBarra= false;
    } //-- void deleteCdTipoServicoCodBarra() 

    /**
     * Method deleteCdTipoServicoDebVei
     * 
     */
    public void deleteCdTipoServicoDebVei()
    {
        this._has_cdTipoServicoDebVei= false;
    } //-- void deleteCdTipoServicoDebVei() 

    /**
     * Method deleteCdTipoServicoGare
     * 
     */
    public void deleteCdTipoServicoGare()
    {
        this._has_cdTipoServicoGare= false;
    } //-- void deleteCdTipoServicoGare() 

    /**
     * Method deleteCdTipoServicoGps
     * 
     */
    public void deleteCdTipoServicoGps()
    {
        this._has_cdTipoServicoGps= false;
    } //-- void deleteCdTipoServicoGps() 

    /**
     * Method deleteCdTipoServicoSalDoc
     * 
     */
    public void deleteCdTipoServicoSalDoc()
    {
        this._has_cdTipoServicoSalDoc= false;
    } //-- void deleteCdTipoServicoSalDoc() 

    /**
     * Method deleteCdTipoServicoSalTed
     * 
     */
    public void deleteCdTipoServicoSalTed()
    {
        this._has_cdTipoServicoSalTed= false;
    } //-- void deleteCdTipoServicoSalTed() 

    /**
     * Method deleteCdTipoSevOp
     * 
     */
    public void deleteCdTipoSevOp()
    {
        this._has_cdTipoSevOp= false;
    } //-- void deleteCdTipoSevOp() 

    /**
     * Method deleteCdTipoSevicoSalOp
     * 
     */
    public void deleteCdTipoSevicoSalOp()
    {
        this._has_cdTipoSevicoSalOp= false;
    } //-- void deleteCdTipoSevicoSalOp() 

    /**
     * Method deleteQtCodBarra
     * 
     */
    public void deleteQtCodBarra()
    {
        this._has_qtCodBarra= false;
    } //-- void deleteQtCodBarra() 

    /**
     * Method deleteQtCodConta
     * 
     */
    public void deleteQtCodConta()
    {
        this._has_qtCodConta= false;
    } //-- void deleteQtCodConta() 

    /**
     * Method deleteQtDarf
     * 
     */
    public void deleteQtDarf()
    {
        this._has_qtDarf= false;
    } //-- void deleteQtDarf() 

    /**
     * Method deleteQtDebConta
     * 
     */
    public void deleteQtDebConta()
    {
        this._has_qtDebConta= false;
    } //-- void deleteQtDebConta() 

    /**
     * Method deleteQtDebVei
     * 
     */
    public void deleteQtDebVei()
    {
        this._has_qtDebVei= false;
    } //-- void deleteQtDebVei() 

    /**
     * Method deleteQtDepIden
     * 
     */
    public void deleteQtDepIden()
    {
        this._has_qtDepIden= false;
    } //-- void deleteQtDepIden() 

    /**
     * Method deleteQtDoc
     * 
     */
    public void deleteQtDoc()
    {
        this._has_qtDoc= false;
    } //-- void deleteQtDoc() 

    /**
     * Method deleteQtGare
     * 
     */
    public void deleteQtGare()
    {
        this._has_qtGare= false;
    } //-- void deleteQtGare() 

    /**
     * Method deleteQtGps
     * 
     */
    public void deleteQtGps()
    {
        this._has_qtGps= false;
    } //-- void deleteQtGps() 

    /**
     * Method deleteQtOp
     * 
     */
    public void deleteQtOp()
    {
        this._has_qtOp= false;
    } //-- void deleteQtOp() 

    /**
     * Method deleteQtOrdCred
     * 
     */
    public void deleteQtOrdCred()
    {
        this._has_qtOrdCred= false;
    } //-- void deleteQtOrdCred() 

    /**
     * Method deleteQtPagBen
     * 
     */
    public void deleteQtPagBen()
    {
        this._has_qtPagBen= false;
    } //-- void deleteQtPagBen() 

    /**
     * Method deleteQtPagSal
     * 
     */
    public void deleteQtPagSal()
    {
        this._has_qtPagSal= false;
    } //-- void deleteQtPagSal() 

    /**
     * Method deleteQtParfor
     * 
     */
    public void deleteQtParfor()
    {
        this._has_qtParfor= false;
    } //-- void deleteQtParfor() 

    /**
     * Method deleteQtSalConta
     * 
     */
    public void deleteQtSalConta()
    {
        this._has_qtSalConta= false;
    } //-- void deleteQtSalConta() 

    /**
     * Method deleteQtSalOp
     * 
     */
    public void deleteQtSalOp()
    {
        this._has_qtSalOp= false;
    } //-- void deleteQtSalOp() 

    /**
     * Method deleteQtTEd
     * 
     */
    public void deleteQtTEd()
    {
        this._has_qtTEd= false;
    } //-- void deleteQtTEd() 

    /**
     * Method deleteQtTitBd
     * 
     */
    public void deleteQtTitBd()
    {
        this._has_qtTitBd= false;
    } //-- void deleteQtTitBd() 

    /**
     * Method deleteQtTitOutros
     * 
     */
    public void deleteQtTitOutros()
    {
        this._has_qtTitOutros= false;
    } //-- void deleteQtTitOutros() 

    /**
     * Returns the value of field 'cdModDepIden'.
     * 
     * @return int
     * @return the value of field 'cdModDepIden'.
     */
    public int getCdModDepIden()
    {
        return this._cdModDepIden;
    } //-- int getCdModDepIden() 

    /**
     * Returns the value of field 'cdQuantidadeSal'.
     * 
     * @return long
     * @return the value of field 'cdQuantidadeSal'.
     */
    public long getCdQuantidadeSal()
    {
        return this._cdQuantidadeSal;
    } //-- long getCdQuantidadeSal() 

    /**
     * Returns the value of field 'cdQuantidadeSalTed'.
     * 
     * @return long
     * @return the value of field 'cdQuantidadeSalTed'.
     */
    public long getCdQuantidadeSalTed()
    {
        return this._cdQuantidadeSalTed;
    } //-- long getCdQuantidadeSalTed() 

    /**
     * Returns the value of field 'cdTIpoModOp'.
     * 
     * @return int
     * @return the value of field 'cdTIpoModOp'.
     */
    public int getCdTIpoModOp()
    {
        return this._cdTIpoModOp;
    } //-- int getCdTIpoModOp() 

    /**
     * Returns the value of field 'cdTIpoServDepIden'.
     * 
     * @return int
     * @return the value of field 'cdTIpoServDepIden'.
     */
    public int getCdTIpoServDepIden()
    {
        return this._cdTIpoServDepIden;
    } //-- int getCdTIpoServDepIden() 

    /**
     * Returns the value of field 'cdTIpoServDoc'.
     * 
     * @return int
     * @return the value of field 'cdTIpoServDoc'.
     */
    public int getCdTIpoServDoc()
    {
        return this._cdTIpoServDoc;
    } //-- int getCdTIpoServDoc() 

    /**
     * Returns the value of field 'cdTIpoServOrdCerd'.
     * 
     * @return int
     * @return the value of field 'cdTIpoServOrdCerd'.
     */
    public int getCdTIpoServOrdCerd()
    {
        return this._cdTIpoServOrdCerd;
    } //-- int getCdTIpoServOrdCerd() 

    /**
     * Returns the value of field 'cdTIpoServicoSalConta'.
     * 
     * @return int
     * @return the value of field 'cdTIpoServicoSalConta'.
     */
    public int getCdTIpoServicoSalConta()
    {
        return this._cdTIpoServicoSalConta;
    } //-- int getCdTIpoServicoSalConta() 

    /**
     * Returns the value of field 'cdTipoMOdTitOutros'.
     * 
     * @return int
     * @return the value of field 'cdTipoMOdTitOutros'.
     */
    public int getCdTipoMOdTitOutros()
    {
        return this._cdTipoMOdTitOutros;
    } //-- int getCdTipoMOdTitOutros() 

    /**
     * Returns the value of field 'cdTipoMadalidadeSalConta'.
     * 
     * @return int
     * @return the value of field 'cdTipoMadalidadeSalConta'.
     */
    public int getCdTipoMadalidadeSalConta()
    {
        return this._cdTipoMadalidadeSalConta;
    } //-- int getCdTipoMadalidadeSalConta() 

    /**
     * Returns the value of field 'cdTipoModCodConta'.
     * 
     * @return int
     * @return the value of field 'cdTipoModCodConta'.
     */
    public int getCdTipoModCodConta()
    {
        return this._cdTipoModCodConta;
    } //-- int getCdTipoModCodConta() 

    /**
     * Returns the value of field 'cdTipoModDarf'.
     * 
     * @return int
     * @return the value of field 'cdTipoModDarf'.
     */
    public int getCdTipoModDarf()
    {
        return this._cdTipoModDarf;
    } //-- int getCdTipoModDarf() 

    /**
     * Returns the value of field 'cdTipoModDebConta'.
     * 
     * @return int
     * @return the value of field 'cdTipoModDebConta'.
     */
    public int getCdTipoModDebConta()
    {
        return this._cdTipoModDebConta;
    } //-- int getCdTipoModDebConta() 

    /**
     * Returns the value of field 'cdTipoModOrdCred'.
     * 
     * @return int
     * @return the value of field 'cdTipoModOrdCred'.
     */
    public int getCdTipoModOrdCred()
    {
        return this._cdTipoModOrdCred;
    } //-- int getCdTipoModOrdCred() 

    /**
     * Returns the value of field 'cdTipoModTed'.
     * 
     * @return int
     * @return the value of field 'cdTipoModTed'.
     */
    public int getCdTipoModTed()
    {
        return this._cdTipoModTed;
    } //-- int getCdTipoModTed() 

    /**
     * Returns the value of field 'cdTipoModTitBd'.
     * 
     * @return int
     * @return the value of field 'cdTipoModTitBd'.
     */
    public int getCdTipoModTitBd()
    {
        return this._cdTipoModTitBd;
    } //-- int getCdTipoModTitBd() 

    /**
     * Returns the value of field 'cdTipoModalidadeDebVei'.
     * 
     * @return int
     * @return the value of field 'cdTipoModalidadeDebVei'.
     */
    public int getCdTipoModalidadeDebVei()
    {
        return this._cdTipoModalidadeDebVei;
    } //-- int getCdTipoModalidadeDebVei() 

    /**
     * Returns the value of field 'cdTipoModalidadeDoc'.
     * 
     * @return int
     * @return the value of field 'cdTipoModalidadeDoc'.
     */
    public int getCdTipoModalidadeDoc()
    {
        return this._cdTipoModalidadeDoc;
    } //-- int getCdTipoModalidadeDoc() 

    /**
     * Returns the value of field 'cdTipoModalidadeGare'.
     * 
     * @return int
     * @return the value of field 'cdTipoModalidadeGare'.
     */
    public int getCdTipoModalidadeGare()
    {
        return this._cdTipoModalidadeGare;
    } //-- int getCdTipoModalidadeGare() 

    /**
     * Returns the value of field 'cdTipoModalidadeGps'.
     * 
     * @return int
     * @return the value of field 'cdTipoModalidadeGps'.
     */
    public int getCdTipoModalidadeGps()
    {
        return this._cdTipoModalidadeGps;
    } //-- int getCdTipoModalidadeGps() 

    /**
     * Returns the value of field 'cdTipoModalidadeSalDoc'.
     * 
     * @return int
     * @return the value of field 'cdTipoModalidadeSalDoc'.
     */
    public int getCdTipoModalidadeSalDoc()
    {
        return this._cdTipoModalidadeSalDoc;
    } //-- int getCdTipoModalidadeSalDoc() 

    /**
     * Returns the value of field 'cdTipoModalidadeSalOp'.
     * 
     * @return int
     * @return the value of field 'cdTipoModalidadeSalOp'.
     */
    public int getCdTipoModalidadeSalOp()
    {
        return this._cdTipoModalidadeSalOp;
    } //-- int getCdTipoModalidadeSalOp() 

    /**
     * Returns the value of field 'cdTipoModalidadeSalTed'.
     * 
     * @return int
     * @return the value of field 'cdTipoModalidadeSalTed'.
     */
    public int getCdTipoModalidadeSalTed()
    {
        return this._cdTipoModalidadeSalTed;
    } //-- int getCdTipoModalidadeSalTed() 

    /**
     * Returns the value of field 'cdTipoModalidadeVS'.
     * 
     * @return int
     * @return the value of field 'cdTipoModalidadeVS'.
     */
    public int getCdTipoModalidadeVS()
    {
        return this._cdTipoModalidadeVS;
    } //-- int getCdTipoModalidadeVS() 

    /**
     * Returns the value of field 'cdTipoServCodConta'.
     * 
     * @return int
     * @return the value of field 'cdTipoServCodConta'.
     */
    public int getCdTipoServCodConta()
    {
        return this._cdTipoServCodConta;
    } //-- int getCdTipoServCodConta() 

    /**
     * Returns the value of field 'cdTipoServDarf'.
     * 
     * @return int
     * @return the value of field 'cdTipoServDarf'.
     */
    public int getCdTipoServDarf()
    {
        return this._cdTipoServDarf;
    } //-- int getCdTipoServDarf() 

    /**
     * Returns the value of field 'cdTipoServDebConta'.
     * 
     * @return int
     * @return the value of field 'cdTipoServDebConta'.
     */
    public int getCdTipoServDebConta()
    {
        return this._cdTipoServDebConta;
    } //-- int getCdTipoServDebConta() 

    /**
     * Returns the value of field 'cdTipoServTed'.
     * 
     * @return int
     * @return the value of field 'cdTipoServTed'.
     */
    public int getCdTipoServTed()
    {
        return this._cdTipoServTed;
    } //-- int getCdTipoServTed() 

    /**
     * Returns the value of field 'cdTipoServTitBd'.
     * 
     * @return int
     * @return the value of field 'cdTipoServTitBd'.
     */
    public int getCdTipoServTitBd()
    {
        return this._cdTipoServTitBd;
    } //-- int getCdTipoServTitBd() 

    /**
     * Returns the value of field 'cdTipoServTitOutros'.
     * 
     * @return int
     * @return the value of field 'cdTipoServTitOutros'.
     */
    public int getCdTipoServTitOutros()
    {
        return this._cdTipoServTitOutros;
    } //-- int getCdTipoServTitOutros() 

    /**
     * Returns the value of field 'cdTipoServicoCodBarra'.
     * 
     * @return int
     * @return the value of field 'cdTipoServicoCodBarra'.
     */
    public int getCdTipoServicoCodBarra()
    {
        return this._cdTipoServicoCodBarra;
    } //-- int getCdTipoServicoCodBarra() 

    /**
     * Returns the value of field 'cdTipoServicoDebVei'.
     * 
     * @return int
     * @return the value of field 'cdTipoServicoDebVei'.
     */
    public int getCdTipoServicoDebVei()
    {
        return this._cdTipoServicoDebVei;
    } //-- int getCdTipoServicoDebVei() 

    /**
     * Returns the value of field 'cdTipoServicoGare'.
     * 
     * @return int
     * @return the value of field 'cdTipoServicoGare'.
     */
    public int getCdTipoServicoGare()
    {
        return this._cdTipoServicoGare;
    } //-- int getCdTipoServicoGare() 

    /**
     * Returns the value of field 'cdTipoServicoGps'.
     * 
     * @return int
     * @return the value of field 'cdTipoServicoGps'.
     */
    public int getCdTipoServicoGps()
    {
        return this._cdTipoServicoGps;
    } //-- int getCdTipoServicoGps() 

    /**
     * Returns the value of field 'cdTipoServicoSalDoc'.
     * 
     * @return int
     * @return the value of field 'cdTipoServicoSalDoc'.
     */
    public int getCdTipoServicoSalDoc()
    {
        return this._cdTipoServicoSalDoc;
    } //-- int getCdTipoServicoSalDoc() 

    /**
     * Returns the value of field 'cdTipoServicoSalTed'.
     * 
     * @return int
     * @return the value of field 'cdTipoServicoSalTed'.
     */
    public int getCdTipoServicoSalTed()
    {
        return this._cdTipoServicoSalTed;
    } //-- int getCdTipoServicoSalTed() 

    /**
     * Returns the value of field 'cdTipoSevOp'.
     * 
     * @return int
     * @return the value of field 'cdTipoSevOp'.
     */
    public int getCdTipoSevOp()
    {
        return this._cdTipoSevOp;
    } //-- int getCdTipoSevOp() 

    /**
     * Returns the value of field 'cdTipoSevicoSalOp'.
     * 
     * @return int
     * @return the value of field 'cdTipoSevicoSalOp'.
     */
    public int getCdTipoSevicoSalOp()
    {
        return this._cdTipoSevicoSalOp;
    } //-- int getCdTipoSevicoSalOp() 

    /**
     * Returns the value of field 'clTotalPagfor'.
     * 
     * @return BigDecimal
     * @return the value of field 'clTotalPagfor'.
     */
    public java.math.BigDecimal getClTotalPagfor()
    {
        return this._clTotalPagfor;
    } //-- java.math.BigDecimal getClTotalPagfor() 

    /**
     * Returns the value of field 'codMensagem'.
     * 
     * @return String
     * @return the value of field 'codMensagem'.
     */
    public java.lang.String getCodMensagem()
    {
        return this._codMensagem;
    } //-- java.lang.String getCodMensagem() 

    /**
     * Returns the value of field 'mensagem'.
     * 
     * @return String
     * @return the value of field 'mensagem'.
     */
    public java.lang.String getMensagem()
    {
        return this._mensagem;
    } //-- java.lang.String getMensagem() 

    /**
     * Returns the value of field 'qtCodBarra'.
     * 
     * @return long
     * @return the value of field 'qtCodBarra'.
     */
    public long getQtCodBarra()
    {
        return this._qtCodBarra;
    } //-- long getQtCodBarra() 

    /**
     * Returns the value of field 'qtCodConta'.
     * 
     * @return long
     * @return the value of field 'qtCodConta'.
     */
    public long getQtCodConta()
    {
        return this._qtCodConta;
    } //-- long getQtCodConta() 

    /**
     * Returns the value of field 'qtDarf'.
     * 
     * @return long
     * @return the value of field 'qtDarf'.
     */
    public long getQtDarf()
    {
        return this._qtDarf;
    } //-- long getQtDarf() 

    /**
     * Returns the value of field 'qtDebConta'.
     * 
     * @return long
     * @return the value of field 'qtDebConta'.
     */
    public long getQtDebConta()
    {
        return this._qtDebConta;
    } //-- long getQtDebConta() 

    /**
     * Returns the value of field 'qtDebVei'.
     * 
     * @return long
     * @return the value of field 'qtDebVei'.
     */
    public long getQtDebVei()
    {
        return this._qtDebVei;
    } //-- long getQtDebVei() 

    /**
     * Returns the value of field 'qtDepIden'.
     * 
     * @return long
     * @return the value of field 'qtDepIden'.
     */
    public long getQtDepIden()
    {
        return this._qtDepIden;
    } //-- long getQtDepIden() 

    /**
     * Returns the value of field 'qtDoc'.
     * 
     * @return long
     * @return the value of field 'qtDoc'.
     */
    public long getQtDoc()
    {
        return this._qtDoc;
    } //-- long getQtDoc() 

    /**
     * Returns the value of field 'qtGare'.
     * 
     * @return long
     * @return the value of field 'qtGare'.
     */
    public long getQtGare()
    {
        return this._qtGare;
    } //-- long getQtGare() 

    /**
     * Returns the value of field 'qtGps'.
     * 
     * @return long
     * @return the value of field 'qtGps'.
     */
    public long getQtGps()
    {
        return this._qtGps;
    } //-- long getQtGps() 

    /**
     * Returns the value of field 'qtOp'.
     * 
     * @return long
     * @return the value of field 'qtOp'.
     */
    public long getQtOp()
    {
        return this._qtOp;
    } //-- long getQtOp() 

    /**
     * Returns the value of field 'qtOrdCred'.
     * 
     * @return long
     * @return the value of field 'qtOrdCred'.
     */
    public long getQtOrdCred()
    {
        return this._qtOrdCred;
    } //-- long getQtOrdCred() 

    /**
     * Returns the value of field 'qtPagBen'.
     * 
     * @return long
     * @return the value of field 'qtPagBen'.
     */
    public long getQtPagBen()
    {
        return this._qtPagBen;
    } //-- long getQtPagBen() 

    /**
     * Returns the value of field 'qtPagSal'.
     * 
     * @return long
     * @return the value of field 'qtPagSal'.
     */
    public long getQtPagSal()
    {
        return this._qtPagSal;
    } //-- long getQtPagSal() 

    /**
     * Returns the value of field 'qtParfor'.
     * 
     * @return long
     * @return the value of field 'qtParfor'.
     */
    public long getQtParfor()
    {
        return this._qtParfor;
    } //-- long getQtParfor() 

    /**
     * Returns the value of field 'qtSalConta'.
     * 
     * @return long
     * @return the value of field 'qtSalConta'.
     */
    public long getQtSalConta()
    {
        return this._qtSalConta;
    } //-- long getQtSalConta() 

    /**
     * Returns the value of field 'qtSalOp'.
     * 
     * @return long
     * @return the value of field 'qtSalOp'.
     */
    public long getQtSalOp()
    {
        return this._qtSalOp;
    } //-- long getQtSalOp() 

    /**
     * Returns the value of field 'qtTEd'.
     * 
     * @return long
     * @return the value of field 'qtTEd'.
     */
    public long getQtTEd()
    {
        return this._qtTEd;
    } //-- long getQtTEd() 

    /**
     * Returns the value of field 'qtTitBd'.
     * 
     * @return long
     * @return the value of field 'qtTitBd'.
     */
    public long getQtTitBd()
    {
        return this._qtTitBd;
    } //-- long getQtTitBd() 

    /**
     * Returns the value of field 'qtTitOutros'.
     * 
     * @return long
     * @return the value of field 'qtTitOutros'.
     */
    public long getQtTitOutros()
    {
        return this._qtTitOutros;
    } //-- long getQtTitOutros() 

    /**
     * Returns the value of field 'vlTotalCodBarra'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlTotalCodBarra'.
     */
    public java.math.BigDecimal getVlTotalCodBarra()
    {
        return this._vlTotalCodBarra;
    } //-- java.math.BigDecimal getVlTotalCodBarra() 

    /**
     * Returns the value of field 'vlTotalCodConta'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlTotalCodConta'.
     */
    public java.math.BigDecimal getVlTotalCodConta()
    {
        return this._vlTotalCodConta;
    } //-- java.math.BigDecimal getVlTotalCodConta() 

    /**
     * Returns the value of field 'vlTotalDarf'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlTotalDarf'.
     */
    public java.math.BigDecimal getVlTotalDarf()
    {
        return this._vlTotalDarf;
    } //-- java.math.BigDecimal getVlTotalDarf() 

    /**
     * Returns the value of field 'vlTotalDebConta'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlTotalDebConta'.
     */
    public java.math.BigDecimal getVlTotalDebConta()
    {
        return this._vlTotalDebConta;
    } //-- java.math.BigDecimal getVlTotalDebConta() 

    /**
     * Returns the value of field 'vlTotalDebVei'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlTotalDebVei'.
     */
    public java.math.BigDecimal getVlTotalDebVei()
    {
        return this._vlTotalDebVei;
    } //-- java.math.BigDecimal getVlTotalDebVei() 

    /**
     * Returns the value of field 'vlTotalDepIden'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlTotalDepIden'.
     */
    public java.math.BigDecimal getVlTotalDepIden()
    {
        return this._vlTotalDepIden;
    } //-- java.math.BigDecimal getVlTotalDepIden() 

    /**
     * Returns the value of field 'vlTotalDoc'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlTotalDoc'.
     */
    public java.math.BigDecimal getVlTotalDoc()
    {
        return this._vlTotalDoc;
    } //-- java.math.BigDecimal getVlTotalDoc() 

    /**
     * Returns the value of field 'vlTotalGare'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlTotalGare'.
     */
    public java.math.BigDecimal getVlTotalGare()
    {
        return this._vlTotalGare;
    } //-- java.math.BigDecimal getVlTotalGare() 

    /**
     * Returns the value of field 'vlTotalGps'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlTotalGps'.
     */
    public java.math.BigDecimal getVlTotalGps()
    {
        return this._vlTotalGps;
    } //-- java.math.BigDecimal getVlTotalGps() 

    /**
     * Returns the value of field 'vlTotalOp'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlTotalOp'.
     */
    public java.math.BigDecimal getVlTotalOp()
    {
        return this._vlTotalOp;
    } //-- java.math.BigDecimal getVlTotalOp() 

    /**
     * Returns the value of field 'vlTotalOrdCred'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlTotalOrdCred'.
     */
    public java.math.BigDecimal getVlTotalOrdCred()
    {
        return this._vlTotalOrdCred;
    } //-- java.math.BigDecimal getVlTotalOrdCred() 

    /**
     * Returns the value of field 'vlTotalPagBen'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlTotalPagBen'.
     */
    public java.math.BigDecimal getVlTotalPagBen()
    {
        return this._vlTotalPagBen;
    } //-- java.math.BigDecimal getVlTotalPagBen() 

    /**
     * Returns the value of field 'vlTotalPagSal'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlTotalPagSal'.
     */
    public java.math.BigDecimal getVlTotalPagSal()
    {
        return this._vlTotalPagSal;
    } //-- java.math.BigDecimal getVlTotalPagSal() 

    /**
     * Returns the value of field 'vlTotalSalConta'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlTotalSalConta'.
     */
    public java.math.BigDecimal getVlTotalSalConta()
    {
        return this._vlTotalSalConta;
    } //-- java.math.BigDecimal getVlTotalSalConta() 

    /**
     * Returns the value of field 'vlTotalSalDoc'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlTotalSalDoc'.
     */
    public java.math.BigDecimal getVlTotalSalDoc()
    {
        return this._vlTotalSalDoc;
    } //-- java.math.BigDecimal getVlTotalSalDoc() 

    /**
     * Returns the value of field 'vlTotalSalOp'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlTotalSalOp'.
     */
    public java.math.BigDecimal getVlTotalSalOp()
    {
        return this._vlTotalSalOp;
    } //-- java.math.BigDecimal getVlTotalSalOp() 

    /**
     * Returns the value of field 'vlTotalSalTed'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlTotalSalTed'.
     */
    public java.math.BigDecimal getVlTotalSalTed()
    {
        return this._vlTotalSalTed;
    } //-- java.math.BigDecimal getVlTotalSalTed() 

    /**
     * Returns the value of field 'vlTotalTed'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlTotalTed'.
     */
    public java.math.BigDecimal getVlTotalTed()
    {
        return this._vlTotalTed;
    } //-- java.math.BigDecimal getVlTotalTed() 

    /**
     * Returns the value of field 'vlTotalTitBd'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlTotalTitBd'.
     */
    public java.math.BigDecimal getVlTotalTitBd()
    {
        return this._vlTotalTitBd;
    } //-- java.math.BigDecimal getVlTotalTitBd() 

    /**
     * Returns the value of field 'vlTotalTitOutros'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlTotalTitOutros'.
     */
    public java.math.BigDecimal getVlTotalTitOutros()
    {
        return this._vlTotalTitOutros;
    } //-- java.math.BigDecimal getVlTotalTitOutros() 

    /**
     * Method hasCdModDepIden
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdModDepIden()
    {
        return this._has_cdModDepIden;
    } //-- boolean hasCdModDepIden() 

    /**
     * Method hasCdQuantidadeSal
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdQuantidadeSal()
    {
        return this._has_cdQuantidadeSal;
    } //-- boolean hasCdQuantidadeSal() 

    /**
     * Method hasCdQuantidadeSalTed
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdQuantidadeSalTed()
    {
        return this._has_cdQuantidadeSalTed;
    } //-- boolean hasCdQuantidadeSalTed() 

    /**
     * Method hasCdTIpoModOp
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTIpoModOp()
    {
        return this._has_cdTIpoModOp;
    } //-- boolean hasCdTIpoModOp() 

    /**
     * Method hasCdTIpoServDepIden
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTIpoServDepIden()
    {
        return this._has_cdTIpoServDepIden;
    } //-- boolean hasCdTIpoServDepIden() 

    /**
     * Method hasCdTIpoServDoc
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTIpoServDoc()
    {
        return this._has_cdTIpoServDoc;
    } //-- boolean hasCdTIpoServDoc() 

    /**
     * Method hasCdTIpoServOrdCerd
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTIpoServOrdCerd()
    {
        return this._has_cdTIpoServOrdCerd;
    } //-- boolean hasCdTIpoServOrdCerd() 

    /**
     * Method hasCdTIpoServicoSalConta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTIpoServicoSalConta()
    {
        return this._has_cdTIpoServicoSalConta;
    } //-- boolean hasCdTIpoServicoSalConta() 

    /**
     * Method hasCdTipoMOdTitOutros
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoMOdTitOutros()
    {
        return this._has_cdTipoMOdTitOutros;
    } //-- boolean hasCdTipoMOdTitOutros() 

    /**
     * Method hasCdTipoMadalidadeSalConta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoMadalidadeSalConta()
    {
        return this._has_cdTipoMadalidadeSalConta;
    } //-- boolean hasCdTipoMadalidadeSalConta() 

    /**
     * Method hasCdTipoModCodConta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoModCodConta()
    {
        return this._has_cdTipoModCodConta;
    } //-- boolean hasCdTipoModCodConta() 

    /**
     * Method hasCdTipoModDarf
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoModDarf()
    {
        return this._has_cdTipoModDarf;
    } //-- boolean hasCdTipoModDarf() 

    /**
     * Method hasCdTipoModDebConta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoModDebConta()
    {
        return this._has_cdTipoModDebConta;
    } //-- boolean hasCdTipoModDebConta() 

    /**
     * Method hasCdTipoModOrdCred
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoModOrdCred()
    {
        return this._has_cdTipoModOrdCred;
    } //-- boolean hasCdTipoModOrdCred() 

    /**
     * Method hasCdTipoModTed
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoModTed()
    {
        return this._has_cdTipoModTed;
    } //-- boolean hasCdTipoModTed() 

    /**
     * Method hasCdTipoModTitBd
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoModTitBd()
    {
        return this._has_cdTipoModTitBd;
    } //-- boolean hasCdTipoModTitBd() 

    /**
     * Method hasCdTipoModalidadeDebVei
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoModalidadeDebVei()
    {
        return this._has_cdTipoModalidadeDebVei;
    } //-- boolean hasCdTipoModalidadeDebVei() 

    /**
     * Method hasCdTipoModalidadeDoc
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoModalidadeDoc()
    {
        return this._has_cdTipoModalidadeDoc;
    } //-- boolean hasCdTipoModalidadeDoc() 

    /**
     * Method hasCdTipoModalidadeGare
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoModalidadeGare()
    {
        return this._has_cdTipoModalidadeGare;
    } //-- boolean hasCdTipoModalidadeGare() 

    /**
     * Method hasCdTipoModalidadeGps
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoModalidadeGps()
    {
        return this._has_cdTipoModalidadeGps;
    } //-- boolean hasCdTipoModalidadeGps() 

    /**
     * Method hasCdTipoModalidadeSalDoc
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoModalidadeSalDoc()
    {
        return this._has_cdTipoModalidadeSalDoc;
    } //-- boolean hasCdTipoModalidadeSalDoc() 

    /**
     * Method hasCdTipoModalidadeSalOp
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoModalidadeSalOp()
    {
        return this._has_cdTipoModalidadeSalOp;
    } //-- boolean hasCdTipoModalidadeSalOp() 

    /**
     * Method hasCdTipoModalidadeSalTed
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoModalidadeSalTed()
    {
        return this._has_cdTipoModalidadeSalTed;
    } //-- boolean hasCdTipoModalidadeSalTed() 

    /**
     * Method hasCdTipoModalidadeVS
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoModalidadeVS()
    {
        return this._has_cdTipoModalidadeVS;
    } //-- boolean hasCdTipoModalidadeVS() 

    /**
     * Method hasCdTipoServCodConta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoServCodConta()
    {
        return this._has_cdTipoServCodConta;
    } //-- boolean hasCdTipoServCodConta() 

    /**
     * Method hasCdTipoServDarf
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoServDarf()
    {
        return this._has_cdTipoServDarf;
    } //-- boolean hasCdTipoServDarf() 

    /**
     * Method hasCdTipoServDebConta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoServDebConta()
    {
        return this._has_cdTipoServDebConta;
    } //-- boolean hasCdTipoServDebConta() 

    /**
     * Method hasCdTipoServTed
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoServTed()
    {
        return this._has_cdTipoServTed;
    } //-- boolean hasCdTipoServTed() 

    /**
     * Method hasCdTipoServTitBd
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoServTitBd()
    {
        return this._has_cdTipoServTitBd;
    } //-- boolean hasCdTipoServTitBd() 

    /**
     * Method hasCdTipoServTitOutros
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoServTitOutros()
    {
        return this._has_cdTipoServTitOutros;
    } //-- boolean hasCdTipoServTitOutros() 

    /**
     * Method hasCdTipoServicoCodBarra
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoServicoCodBarra()
    {
        return this._has_cdTipoServicoCodBarra;
    } //-- boolean hasCdTipoServicoCodBarra() 

    /**
     * Method hasCdTipoServicoDebVei
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoServicoDebVei()
    {
        return this._has_cdTipoServicoDebVei;
    } //-- boolean hasCdTipoServicoDebVei() 

    /**
     * Method hasCdTipoServicoGare
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoServicoGare()
    {
        return this._has_cdTipoServicoGare;
    } //-- boolean hasCdTipoServicoGare() 

    /**
     * Method hasCdTipoServicoGps
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoServicoGps()
    {
        return this._has_cdTipoServicoGps;
    } //-- boolean hasCdTipoServicoGps() 

    /**
     * Method hasCdTipoServicoSalDoc
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoServicoSalDoc()
    {
        return this._has_cdTipoServicoSalDoc;
    } //-- boolean hasCdTipoServicoSalDoc() 

    /**
     * Method hasCdTipoServicoSalTed
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoServicoSalTed()
    {
        return this._has_cdTipoServicoSalTed;
    } //-- boolean hasCdTipoServicoSalTed() 

    /**
     * Method hasCdTipoSevOp
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoSevOp()
    {
        return this._has_cdTipoSevOp;
    } //-- boolean hasCdTipoSevOp() 

    /**
     * Method hasCdTipoSevicoSalOp
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoSevicoSalOp()
    {
        return this._has_cdTipoSevicoSalOp;
    } //-- boolean hasCdTipoSevicoSalOp() 

    /**
     * Method hasQtCodBarra
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtCodBarra()
    {
        return this._has_qtCodBarra;
    } //-- boolean hasQtCodBarra() 

    /**
     * Method hasQtCodConta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtCodConta()
    {
        return this._has_qtCodConta;
    } //-- boolean hasQtCodConta() 

    /**
     * Method hasQtDarf
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtDarf()
    {
        return this._has_qtDarf;
    } //-- boolean hasQtDarf() 

    /**
     * Method hasQtDebConta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtDebConta()
    {
        return this._has_qtDebConta;
    } //-- boolean hasQtDebConta() 

    /**
     * Method hasQtDebVei
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtDebVei()
    {
        return this._has_qtDebVei;
    } //-- boolean hasQtDebVei() 

    /**
     * Method hasQtDepIden
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtDepIden()
    {
        return this._has_qtDepIden;
    } //-- boolean hasQtDepIden() 

    /**
     * Method hasQtDoc
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtDoc()
    {
        return this._has_qtDoc;
    } //-- boolean hasQtDoc() 

    /**
     * Method hasQtGare
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtGare()
    {
        return this._has_qtGare;
    } //-- boolean hasQtGare() 

    /**
     * Method hasQtGps
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtGps()
    {
        return this._has_qtGps;
    } //-- boolean hasQtGps() 

    /**
     * Method hasQtOp
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtOp()
    {
        return this._has_qtOp;
    } //-- boolean hasQtOp() 

    /**
     * Method hasQtOrdCred
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtOrdCred()
    {
        return this._has_qtOrdCred;
    } //-- boolean hasQtOrdCred() 

    /**
     * Method hasQtPagBen
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtPagBen()
    {
        return this._has_qtPagBen;
    } //-- boolean hasQtPagBen() 

    /**
     * Method hasQtPagSal
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtPagSal()
    {
        return this._has_qtPagSal;
    } //-- boolean hasQtPagSal() 

    /**
     * Method hasQtParfor
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtParfor()
    {
        return this._has_qtParfor;
    } //-- boolean hasQtParfor() 

    /**
     * Method hasQtSalConta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtSalConta()
    {
        return this._has_qtSalConta;
    } //-- boolean hasQtSalConta() 

    /**
     * Method hasQtSalOp
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtSalOp()
    {
        return this._has_qtSalOp;
    } //-- boolean hasQtSalOp() 

    /**
     * Method hasQtTEd
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtTEd()
    {
        return this._has_qtTEd;
    } //-- boolean hasQtTEd() 

    /**
     * Method hasQtTitBd
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtTitBd()
    {
        return this._has_qtTitBd;
    } //-- boolean hasQtTitBd() 

    /**
     * Method hasQtTitOutros
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtTitOutros()
    {
        return this._has_qtTitOutros;
    } //-- boolean hasQtTitOutros() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdModDepIden'.
     * 
     * @param cdModDepIden the value of field 'cdModDepIden'.
     */
    public void setCdModDepIden(int cdModDepIden)
    {
        this._cdModDepIden = cdModDepIden;
        this._has_cdModDepIden = true;
    } //-- void setCdModDepIden(int) 

    /**
     * Sets the value of field 'cdQuantidadeSal'.
     * 
     * @param cdQuantidadeSal the value of field 'cdQuantidadeSal'.
     */
    public void setCdQuantidadeSal(long cdQuantidadeSal)
    {
        this._cdQuantidadeSal = cdQuantidadeSal;
        this._has_cdQuantidadeSal = true;
    } //-- void setCdQuantidadeSal(long) 

    /**
     * Sets the value of field 'cdQuantidadeSalTed'.
     * 
     * @param cdQuantidadeSalTed the value of field
     * 'cdQuantidadeSalTed'.
     */
    public void setCdQuantidadeSalTed(long cdQuantidadeSalTed)
    {
        this._cdQuantidadeSalTed = cdQuantidadeSalTed;
        this._has_cdQuantidadeSalTed = true;
    } //-- void setCdQuantidadeSalTed(long) 

    /**
     * Sets the value of field 'cdTIpoModOp'.
     * 
     * @param cdTIpoModOp the value of field 'cdTIpoModOp'.
     */
    public void setCdTIpoModOp(int cdTIpoModOp)
    {
        this._cdTIpoModOp = cdTIpoModOp;
        this._has_cdTIpoModOp = true;
    } //-- void setCdTIpoModOp(int) 

    /**
     * Sets the value of field 'cdTIpoServDepIden'.
     * 
     * @param cdTIpoServDepIden the value of field
     * 'cdTIpoServDepIden'.
     */
    public void setCdTIpoServDepIden(int cdTIpoServDepIden)
    {
        this._cdTIpoServDepIden = cdTIpoServDepIden;
        this._has_cdTIpoServDepIden = true;
    } //-- void setCdTIpoServDepIden(int) 

    /**
     * Sets the value of field 'cdTIpoServDoc'.
     * 
     * @param cdTIpoServDoc the value of field 'cdTIpoServDoc'.
     */
    public void setCdTIpoServDoc(int cdTIpoServDoc)
    {
        this._cdTIpoServDoc = cdTIpoServDoc;
        this._has_cdTIpoServDoc = true;
    } //-- void setCdTIpoServDoc(int) 

    /**
     * Sets the value of field 'cdTIpoServOrdCerd'.
     * 
     * @param cdTIpoServOrdCerd the value of field
     * 'cdTIpoServOrdCerd'.
     */
    public void setCdTIpoServOrdCerd(int cdTIpoServOrdCerd)
    {
        this._cdTIpoServOrdCerd = cdTIpoServOrdCerd;
        this._has_cdTIpoServOrdCerd = true;
    } //-- void setCdTIpoServOrdCerd(int) 

    /**
     * Sets the value of field 'cdTIpoServicoSalConta'.
     * 
     * @param cdTIpoServicoSalConta the value of field
     * 'cdTIpoServicoSalConta'.
     */
    public void setCdTIpoServicoSalConta(int cdTIpoServicoSalConta)
    {
        this._cdTIpoServicoSalConta = cdTIpoServicoSalConta;
        this._has_cdTIpoServicoSalConta = true;
    } //-- void setCdTIpoServicoSalConta(int) 

    /**
     * Sets the value of field 'cdTipoMOdTitOutros'.
     * 
     * @param cdTipoMOdTitOutros the value of field
     * 'cdTipoMOdTitOutros'.
     */
    public void setCdTipoMOdTitOutros(int cdTipoMOdTitOutros)
    {
        this._cdTipoMOdTitOutros = cdTipoMOdTitOutros;
        this._has_cdTipoMOdTitOutros = true;
    } //-- void setCdTipoMOdTitOutros(int) 

    /**
     * Sets the value of field 'cdTipoMadalidadeSalConta'.
     * 
     * @param cdTipoMadalidadeSalConta the value of field
     * 'cdTipoMadalidadeSalConta'.
     */
    public void setCdTipoMadalidadeSalConta(int cdTipoMadalidadeSalConta)
    {
        this._cdTipoMadalidadeSalConta = cdTipoMadalidadeSalConta;
        this._has_cdTipoMadalidadeSalConta = true;
    } //-- void setCdTipoMadalidadeSalConta(int) 

    /**
     * Sets the value of field 'cdTipoModCodConta'.
     * 
     * @param cdTipoModCodConta the value of field
     * 'cdTipoModCodConta'.
     */
    public void setCdTipoModCodConta(int cdTipoModCodConta)
    {
        this._cdTipoModCodConta = cdTipoModCodConta;
        this._has_cdTipoModCodConta = true;
    } //-- void setCdTipoModCodConta(int) 

    /**
     * Sets the value of field 'cdTipoModDarf'.
     * 
     * @param cdTipoModDarf the value of field 'cdTipoModDarf'.
     */
    public void setCdTipoModDarf(int cdTipoModDarf)
    {
        this._cdTipoModDarf = cdTipoModDarf;
        this._has_cdTipoModDarf = true;
    } //-- void setCdTipoModDarf(int) 

    /**
     * Sets the value of field 'cdTipoModDebConta'.
     * 
     * @param cdTipoModDebConta the value of field
     * 'cdTipoModDebConta'.
     */
    public void setCdTipoModDebConta(int cdTipoModDebConta)
    {
        this._cdTipoModDebConta = cdTipoModDebConta;
        this._has_cdTipoModDebConta = true;
    } //-- void setCdTipoModDebConta(int) 

    /**
     * Sets the value of field 'cdTipoModOrdCred'.
     * 
     * @param cdTipoModOrdCred the value of field 'cdTipoModOrdCred'
     */
    public void setCdTipoModOrdCred(int cdTipoModOrdCred)
    {
        this._cdTipoModOrdCred = cdTipoModOrdCred;
        this._has_cdTipoModOrdCred = true;
    } //-- void setCdTipoModOrdCred(int) 

    /**
     * Sets the value of field 'cdTipoModTed'.
     * 
     * @param cdTipoModTed the value of field 'cdTipoModTed'.
     */
    public void setCdTipoModTed(int cdTipoModTed)
    {
        this._cdTipoModTed = cdTipoModTed;
        this._has_cdTipoModTed = true;
    } //-- void setCdTipoModTed(int) 

    /**
     * Sets the value of field 'cdTipoModTitBd'.
     * 
     * @param cdTipoModTitBd the value of field 'cdTipoModTitBd'.
     */
    public void setCdTipoModTitBd(int cdTipoModTitBd)
    {
        this._cdTipoModTitBd = cdTipoModTitBd;
        this._has_cdTipoModTitBd = true;
    } //-- void setCdTipoModTitBd(int) 

    /**
     * Sets the value of field 'cdTipoModalidadeDebVei'.
     * 
     * @param cdTipoModalidadeDebVei the value of field
     * 'cdTipoModalidadeDebVei'.
     */
    public void setCdTipoModalidadeDebVei(int cdTipoModalidadeDebVei)
    {
        this._cdTipoModalidadeDebVei = cdTipoModalidadeDebVei;
        this._has_cdTipoModalidadeDebVei = true;
    } //-- void setCdTipoModalidadeDebVei(int) 

    /**
     * Sets the value of field 'cdTipoModalidadeDoc'.
     * 
     * @param cdTipoModalidadeDoc the value of field
     * 'cdTipoModalidadeDoc'.
     */
    public void setCdTipoModalidadeDoc(int cdTipoModalidadeDoc)
    {
        this._cdTipoModalidadeDoc = cdTipoModalidadeDoc;
        this._has_cdTipoModalidadeDoc = true;
    } //-- void setCdTipoModalidadeDoc(int) 

    /**
     * Sets the value of field 'cdTipoModalidadeGare'.
     * 
     * @param cdTipoModalidadeGare the value of field
     * 'cdTipoModalidadeGare'.
     */
    public void setCdTipoModalidadeGare(int cdTipoModalidadeGare)
    {
        this._cdTipoModalidadeGare = cdTipoModalidadeGare;
        this._has_cdTipoModalidadeGare = true;
    } //-- void setCdTipoModalidadeGare(int) 

    /**
     * Sets the value of field 'cdTipoModalidadeGps'.
     * 
     * @param cdTipoModalidadeGps the value of field
     * 'cdTipoModalidadeGps'.
     */
    public void setCdTipoModalidadeGps(int cdTipoModalidadeGps)
    {
        this._cdTipoModalidadeGps = cdTipoModalidadeGps;
        this._has_cdTipoModalidadeGps = true;
    } //-- void setCdTipoModalidadeGps(int) 

    /**
     * Sets the value of field 'cdTipoModalidadeSalDoc'.
     * 
     * @param cdTipoModalidadeSalDoc the value of field
     * 'cdTipoModalidadeSalDoc'.
     */
    public void setCdTipoModalidadeSalDoc(int cdTipoModalidadeSalDoc)
    {
        this._cdTipoModalidadeSalDoc = cdTipoModalidadeSalDoc;
        this._has_cdTipoModalidadeSalDoc = true;
    } //-- void setCdTipoModalidadeSalDoc(int) 

    /**
     * Sets the value of field 'cdTipoModalidadeSalOp'.
     * 
     * @param cdTipoModalidadeSalOp the value of field
     * 'cdTipoModalidadeSalOp'.
     */
    public void setCdTipoModalidadeSalOp(int cdTipoModalidadeSalOp)
    {
        this._cdTipoModalidadeSalOp = cdTipoModalidadeSalOp;
        this._has_cdTipoModalidadeSalOp = true;
    } //-- void setCdTipoModalidadeSalOp(int) 

    /**
     * Sets the value of field 'cdTipoModalidadeSalTed'.
     * 
     * @param cdTipoModalidadeSalTed the value of field
     * 'cdTipoModalidadeSalTed'.
     */
    public void setCdTipoModalidadeSalTed(int cdTipoModalidadeSalTed)
    {
        this._cdTipoModalidadeSalTed = cdTipoModalidadeSalTed;
        this._has_cdTipoModalidadeSalTed = true;
    } //-- void setCdTipoModalidadeSalTed(int) 

    /**
     * Sets the value of field 'cdTipoModalidadeVS'.
     * 
     * @param cdTipoModalidadeVS the value of field
     * 'cdTipoModalidadeVS'.
     */
    public void setCdTipoModalidadeVS(int cdTipoModalidadeVS)
    {
        this._cdTipoModalidadeVS = cdTipoModalidadeVS;
        this._has_cdTipoModalidadeVS = true;
    } //-- void setCdTipoModalidadeVS(int) 

    /**
     * Sets the value of field 'cdTipoServCodConta'.
     * 
     * @param cdTipoServCodConta the value of field
     * 'cdTipoServCodConta'.
     */
    public void setCdTipoServCodConta(int cdTipoServCodConta)
    {
        this._cdTipoServCodConta = cdTipoServCodConta;
        this._has_cdTipoServCodConta = true;
    } //-- void setCdTipoServCodConta(int) 

    /**
     * Sets the value of field 'cdTipoServDarf'.
     * 
     * @param cdTipoServDarf the value of field 'cdTipoServDarf'.
     */
    public void setCdTipoServDarf(int cdTipoServDarf)
    {
        this._cdTipoServDarf = cdTipoServDarf;
        this._has_cdTipoServDarf = true;
    } //-- void setCdTipoServDarf(int) 

    /**
     * Sets the value of field 'cdTipoServDebConta'.
     * 
     * @param cdTipoServDebConta the value of field
     * 'cdTipoServDebConta'.
     */
    public void setCdTipoServDebConta(int cdTipoServDebConta)
    {
        this._cdTipoServDebConta = cdTipoServDebConta;
        this._has_cdTipoServDebConta = true;
    } //-- void setCdTipoServDebConta(int) 

    /**
     * Sets the value of field 'cdTipoServTed'.
     * 
     * @param cdTipoServTed the value of field 'cdTipoServTed'.
     */
    public void setCdTipoServTed(int cdTipoServTed)
    {
        this._cdTipoServTed = cdTipoServTed;
        this._has_cdTipoServTed = true;
    } //-- void setCdTipoServTed(int) 

    /**
     * Sets the value of field 'cdTipoServTitBd'.
     * 
     * @param cdTipoServTitBd the value of field 'cdTipoServTitBd'.
     */
    public void setCdTipoServTitBd(int cdTipoServTitBd)
    {
        this._cdTipoServTitBd = cdTipoServTitBd;
        this._has_cdTipoServTitBd = true;
    } //-- void setCdTipoServTitBd(int) 

    /**
     * Sets the value of field 'cdTipoServTitOutros'.
     * 
     * @param cdTipoServTitOutros the value of field
     * 'cdTipoServTitOutros'.
     */
    public void setCdTipoServTitOutros(int cdTipoServTitOutros)
    {
        this._cdTipoServTitOutros = cdTipoServTitOutros;
        this._has_cdTipoServTitOutros = true;
    } //-- void setCdTipoServTitOutros(int) 

    /**
     * Sets the value of field 'cdTipoServicoCodBarra'.
     * 
     * @param cdTipoServicoCodBarra the value of field
     * 'cdTipoServicoCodBarra'.
     */
    public void setCdTipoServicoCodBarra(int cdTipoServicoCodBarra)
    {
        this._cdTipoServicoCodBarra = cdTipoServicoCodBarra;
        this._has_cdTipoServicoCodBarra = true;
    } //-- void setCdTipoServicoCodBarra(int) 

    /**
     * Sets the value of field 'cdTipoServicoDebVei'.
     * 
     * @param cdTipoServicoDebVei the value of field
     * 'cdTipoServicoDebVei'.
     */
    public void setCdTipoServicoDebVei(int cdTipoServicoDebVei)
    {
        this._cdTipoServicoDebVei = cdTipoServicoDebVei;
        this._has_cdTipoServicoDebVei = true;
    } //-- void setCdTipoServicoDebVei(int) 

    /**
     * Sets the value of field 'cdTipoServicoGare'.
     * 
     * @param cdTipoServicoGare the value of field
     * 'cdTipoServicoGare'.
     */
    public void setCdTipoServicoGare(int cdTipoServicoGare)
    {
        this._cdTipoServicoGare = cdTipoServicoGare;
        this._has_cdTipoServicoGare = true;
    } //-- void setCdTipoServicoGare(int) 

    /**
     * Sets the value of field 'cdTipoServicoGps'.
     * 
     * @param cdTipoServicoGps the value of field 'cdTipoServicoGps'
     */
    public void setCdTipoServicoGps(int cdTipoServicoGps)
    {
        this._cdTipoServicoGps = cdTipoServicoGps;
        this._has_cdTipoServicoGps = true;
    } //-- void setCdTipoServicoGps(int) 

    /**
     * Sets the value of field 'cdTipoServicoSalDoc'.
     * 
     * @param cdTipoServicoSalDoc the value of field
     * 'cdTipoServicoSalDoc'.
     */
    public void setCdTipoServicoSalDoc(int cdTipoServicoSalDoc)
    {
        this._cdTipoServicoSalDoc = cdTipoServicoSalDoc;
        this._has_cdTipoServicoSalDoc = true;
    } //-- void setCdTipoServicoSalDoc(int) 

    /**
     * Sets the value of field 'cdTipoServicoSalTed'.
     * 
     * @param cdTipoServicoSalTed the value of field
     * 'cdTipoServicoSalTed'.
     */
    public void setCdTipoServicoSalTed(int cdTipoServicoSalTed)
    {
        this._cdTipoServicoSalTed = cdTipoServicoSalTed;
        this._has_cdTipoServicoSalTed = true;
    } //-- void setCdTipoServicoSalTed(int) 

    /**
     * Sets the value of field 'cdTipoSevOp'.
     * 
     * @param cdTipoSevOp the value of field 'cdTipoSevOp'.
     */
    public void setCdTipoSevOp(int cdTipoSevOp)
    {
        this._cdTipoSevOp = cdTipoSevOp;
        this._has_cdTipoSevOp = true;
    } //-- void setCdTipoSevOp(int) 

    /**
     * Sets the value of field 'cdTipoSevicoSalOp'.
     * 
     * @param cdTipoSevicoSalOp the value of field
     * 'cdTipoSevicoSalOp'.
     */
    public void setCdTipoSevicoSalOp(int cdTipoSevicoSalOp)
    {
        this._cdTipoSevicoSalOp = cdTipoSevicoSalOp;
        this._has_cdTipoSevicoSalOp = true;
    } //-- void setCdTipoSevicoSalOp(int) 

    /**
     * Sets the value of field 'clTotalPagfor'.
     * 
     * @param clTotalPagfor the value of field 'clTotalPagfor'.
     */
    public void setClTotalPagfor(java.math.BigDecimal clTotalPagfor)
    {
        this._clTotalPagfor = clTotalPagfor;
    } //-- void setClTotalPagfor(java.math.BigDecimal) 

    /**
     * Sets the value of field 'codMensagem'.
     * 
     * @param codMensagem the value of field 'codMensagem'.
     */
    public void setCodMensagem(java.lang.String codMensagem)
    {
        this._codMensagem = codMensagem;
    } //-- void setCodMensagem(java.lang.String) 

    /**
     * Sets the value of field 'mensagem'.
     * 
     * @param mensagem the value of field 'mensagem'.
     */
    public void setMensagem(java.lang.String mensagem)
    {
        this._mensagem = mensagem;
    } //-- void setMensagem(java.lang.String) 

    /**
     * Sets the value of field 'qtCodBarra'.
     * 
     * @param qtCodBarra the value of field 'qtCodBarra'.
     */
    public void setQtCodBarra(long qtCodBarra)
    {
        this._qtCodBarra = qtCodBarra;
        this._has_qtCodBarra = true;
    } //-- void setQtCodBarra(long) 

    /**
     * Sets the value of field 'qtCodConta'.
     * 
     * @param qtCodConta the value of field 'qtCodConta'.
     */
    public void setQtCodConta(long qtCodConta)
    {
        this._qtCodConta = qtCodConta;
        this._has_qtCodConta = true;
    } //-- void setQtCodConta(long) 

    /**
     * Sets the value of field 'qtDarf'.
     * 
     * @param qtDarf the value of field 'qtDarf'.
     */
    public void setQtDarf(long qtDarf)
    {
        this._qtDarf = qtDarf;
        this._has_qtDarf = true;
    } //-- void setQtDarf(long) 

    /**
     * Sets the value of field 'qtDebConta'.
     * 
     * @param qtDebConta the value of field 'qtDebConta'.
     */
    public void setQtDebConta(long qtDebConta)
    {
        this._qtDebConta = qtDebConta;
        this._has_qtDebConta = true;
    } //-- void setQtDebConta(long) 

    /**
     * Sets the value of field 'qtDebVei'.
     * 
     * @param qtDebVei the value of field 'qtDebVei'.
     */
    public void setQtDebVei(long qtDebVei)
    {
        this._qtDebVei = qtDebVei;
        this._has_qtDebVei = true;
    } //-- void setQtDebVei(long) 

    /**
     * Sets the value of field 'qtDepIden'.
     * 
     * @param qtDepIden the value of field 'qtDepIden'.
     */
    public void setQtDepIden(long qtDepIden)
    {
        this._qtDepIden = qtDepIden;
        this._has_qtDepIden = true;
    } //-- void setQtDepIden(long) 

    /**
     * Sets the value of field 'qtDoc'.
     * 
     * @param qtDoc the value of field 'qtDoc'.
     */
    public void setQtDoc(long qtDoc)
    {
        this._qtDoc = qtDoc;
        this._has_qtDoc = true;
    } //-- void setQtDoc(long) 

    /**
     * Sets the value of field 'qtGare'.
     * 
     * @param qtGare the value of field 'qtGare'.
     */
    public void setQtGare(long qtGare)
    {
        this._qtGare = qtGare;
        this._has_qtGare = true;
    } //-- void setQtGare(long) 

    /**
     * Sets the value of field 'qtGps'.
     * 
     * @param qtGps the value of field 'qtGps'.
     */
    public void setQtGps(long qtGps)
    {
        this._qtGps = qtGps;
        this._has_qtGps = true;
    } //-- void setQtGps(long) 

    /**
     * Sets the value of field 'qtOp'.
     * 
     * @param qtOp the value of field 'qtOp'.
     */
    public void setQtOp(long qtOp)
    {
        this._qtOp = qtOp;
        this._has_qtOp = true;
    } //-- void setQtOp(long) 

    /**
     * Sets the value of field 'qtOrdCred'.
     * 
     * @param qtOrdCred the value of field 'qtOrdCred'.
     */
    public void setQtOrdCred(long qtOrdCred)
    {
        this._qtOrdCred = qtOrdCred;
        this._has_qtOrdCred = true;
    } //-- void setQtOrdCred(long) 

    /**
     * Sets the value of field 'qtPagBen'.
     * 
     * @param qtPagBen the value of field 'qtPagBen'.
     */
    public void setQtPagBen(long qtPagBen)
    {
        this._qtPagBen = qtPagBen;
        this._has_qtPagBen = true;
    } //-- void setQtPagBen(long) 

    /**
     * Sets the value of field 'qtPagSal'.
     * 
     * @param qtPagSal the value of field 'qtPagSal'.
     */
    public void setQtPagSal(long qtPagSal)
    {
        this._qtPagSal = qtPagSal;
        this._has_qtPagSal = true;
    } //-- void setQtPagSal(long) 

    /**
     * Sets the value of field 'qtParfor'.
     * 
     * @param qtParfor the value of field 'qtParfor'.
     */
    public void setQtParfor(long qtParfor)
    {
        this._qtParfor = qtParfor;
        this._has_qtParfor = true;
    } //-- void setQtParfor(long) 

    /**
     * Sets the value of field 'qtSalConta'.
     * 
     * @param qtSalConta the value of field 'qtSalConta'.
     */
    public void setQtSalConta(long qtSalConta)
    {
        this._qtSalConta = qtSalConta;
        this._has_qtSalConta = true;
    } //-- void setQtSalConta(long) 

    /**
     * Sets the value of field 'qtSalOp'.
     * 
     * @param qtSalOp the value of field 'qtSalOp'.
     */
    public void setQtSalOp(long qtSalOp)
    {
        this._qtSalOp = qtSalOp;
        this._has_qtSalOp = true;
    } //-- void setQtSalOp(long) 

    /**
     * Sets the value of field 'qtTEd'.
     * 
     * @param qtTEd the value of field 'qtTEd'.
     */
    public void setQtTEd(long qtTEd)
    {
        this._qtTEd = qtTEd;
        this._has_qtTEd = true;
    } //-- void setQtTEd(long) 

    /**
     * Sets the value of field 'qtTitBd'.
     * 
     * @param qtTitBd the value of field 'qtTitBd'.
     */
    public void setQtTitBd(long qtTitBd)
    {
        this._qtTitBd = qtTitBd;
        this._has_qtTitBd = true;
    } //-- void setQtTitBd(long) 

    /**
     * Sets the value of field 'qtTitOutros'.
     * 
     * @param qtTitOutros the value of field 'qtTitOutros'.
     */
    public void setQtTitOutros(long qtTitOutros)
    {
        this._qtTitOutros = qtTitOutros;
        this._has_qtTitOutros = true;
    } //-- void setQtTitOutros(long) 

    /**
     * Sets the value of field 'vlTotalCodBarra'.
     * 
     * @param vlTotalCodBarra the value of field 'vlTotalCodBarra'.
     */
    public void setVlTotalCodBarra(java.math.BigDecimal vlTotalCodBarra)
    {
        this._vlTotalCodBarra = vlTotalCodBarra;
    } //-- void setVlTotalCodBarra(java.math.BigDecimal) 

    /**
     * Sets the value of field 'vlTotalCodConta'.
     * 
     * @param vlTotalCodConta the value of field 'vlTotalCodConta'.
     */
    public void setVlTotalCodConta(java.math.BigDecimal vlTotalCodConta)
    {
        this._vlTotalCodConta = vlTotalCodConta;
    } //-- void setVlTotalCodConta(java.math.BigDecimal) 

    /**
     * Sets the value of field 'vlTotalDarf'.
     * 
     * @param vlTotalDarf the value of field 'vlTotalDarf'.
     */
    public void setVlTotalDarf(java.math.BigDecimal vlTotalDarf)
    {
        this._vlTotalDarf = vlTotalDarf;
    } //-- void setVlTotalDarf(java.math.BigDecimal) 

    /**
     * Sets the value of field 'vlTotalDebConta'.
     * 
     * @param vlTotalDebConta the value of field 'vlTotalDebConta'.
     */
    public void setVlTotalDebConta(java.math.BigDecimal vlTotalDebConta)
    {
        this._vlTotalDebConta = vlTotalDebConta;
    } //-- void setVlTotalDebConta(java.math.BigDecimal) 

    /**
     * Sets the value of field 'vlTotalDebVei'.
     * 
     * @param vlTotalDebVei the value of field 'vlTotalDebVei'.
     */
    public void setVlTotalDebVei(java.math.BigDecimal vlTotalDebVei)
    {
        this._vlTotalDebVei = vlTotalDebVei;
    } //-- void setVlTotalDebVei(java.math.BigDecimal) 

    /**
     * Sets the value of field 'vlTotalDepIden'.
     * 
     * @param vlTotalDepIden the value of field 'vlTotalDepIden'.
     */
    public void setVlTotalDepIden(java.math.BigDecimal vlTotalDepIden)
    {
        this._vlTotalDepIden = vlTotalDepIden;
    } //-- void setVlTotalDepIden(java.math.BigDecimal) 

    /**
     * Sets the value of field 'vlTotalDoc'.
     * 
     * @param vlTotalDoc the value of field 'vlTotalDoc'.
     */
    public void setVlTotalDoc(java.math.BigDecimal vlTotalDoc)
    {
        this._vlTotalDoc = vlTotalDoc;
    } //-- void setVlTotalDoc(java.math.BigDecimal) 

    /**
     * Sets the value of field 'vlTotalGare'.
     * 
     * @param vlTotalGare the value of field 'vlTotalGare'.
     */
    public void setVlTotalGare(java.math.BigDecimal vlTotalGare)
    {
        this._vlTotalGare = vlTotalGare;
    } //-- void setVlTotalGare(java.math.BigDecimal) 

    /**
     * Sets the value of field 'vlTotalGps'.
     * 
     * @param vlTotalGps the value of field 'vlTotalGps'.
     */
    public void setVlTotalGps(java.math.BigDecimal vlTotalGps)
    {
        this._vlTotalGps = vlTotalGps;
    } //-- void setVlTotalGps(java.math.BigDecimal) 

    /**
     * Sets the value of field 'vlTotalOp'.
     * 
     * @param vlTotalOp the value of field 'vlTotalOp'.
     */
    public void setVlTotalOp(java.math.BigDecimal vlTotalOp)
    {
        this._vlTotalOp = vlTotalOp;
    } //-- void setVlTotalOp(java.math.BigDecimal) 

    /**
     * Sets the value of field 'vlTotalOrdCred'.
     * 
     * @param vlTotalOrdCred the value of field 'vlTotalOrdCred'.
     */
    public void setVlTotalOrdCred(java.math.BigDecimal vlTotalOrdCred)
    {
        this._vlTotalOrdCred = vlTotalOrdCred;
    } //-- void setVlTotalOrdCred(java.math.BigDecimal) 

    /**
     * Sets the value of field 'vlTotalPagBen'.
     * 
     * @param vlTotalPagBen the value of field 'vlTotalPagBen'.
     */
    public void setVlTotalPagBen(java.math.BigDecimal vlTotalPagBen)
    {
        this._vlTotalPagBen = vlTotalPagBen;
    } //-- void setVlTotalPagBen(java.math.BigDecimal) 

    /**
     * Sets the value of field 'vlTotalPagSal'.
     * 
     * @param vlTotalPagSal the value of field 'vlTotalPagSal'.
     */
    public void setVlTotalPagSal(java.math.BigDecimal vlTotalPagSal)
    {
        this._vlTotalPagSal = vlTotalPagSal;
    } //-- void setVlTotalPagSal(java.math.BigDecimal) 

    /**
     * Sets the value of field 'vlTotalSalConta'.
     * 
     * @param vlTotalSalConta the value of field 'vlTotalSalConta'.
     */
    public void setVlTotalSalConta(java.math.BigDecimal vlTotalSalConta)
    {
        this._vlTotalSalConta = vlTotalSalConta;
    } //-- void setVlTotalSalConta(java.math.BigDecimal) 

    /**
     * Sets the value of field 'vlTotalSalDoc'.
     * 
     * @param vlTotalSalDoc the value of field 'vlTotalSalDoc'.
     */
    public void setVlTotalSalDoc(java.math.BigDecimal vlTotalSalDoc)
    {
        this._vlTotalSalDoc = vlTotalSalDoc;
    } //-- void setVlTotalSalDoc(java.math.BigDecimal) 

    /**
     * Sets the value of field 'vlTotalSalOp'.
     * 
     * @param vlTotalSalOp the value of field 'vlTotalSalOp'.
     */
    public void setVlTotalSalOp(java.math.BigDecimal vlTotalSalOp)
    {
        this._vlTotalSalOp = vlTotalSalOp;
    } //-- void setVlTotalSalOp(java.math.BigDecimal) 

    /**
     * Sets the value of field 'vlTotalSalTed'.
     * 
     * @param vlTotalSalTed the value of field 'vlTotalSalTed'.
     */
    public void setVlTotalSalTed(java.math.BigDecimal vlTotalSalTed)
    {
        this._vlTotalSalTed = vlTotalSalTed;
    } //-- void setVlTotalSalTed(java.math.BigDecimal) 

    /**
     * Sets the value of field 'vlTotalTed'.
     * 
     * @param vlTotalTed the value of field 'vlTotalTed'.
     */
    public void setVlTotalTed(java.math.BigDecimal vlTotalTed)
    {
        this._vlTotalTed = vlTotalTed;
    } //-- void setVlTotalTed(java.math.BigDecimal) 

    /**
     * Sets the value of field 'vlTotalTitBd'.
     * 
     * @param vlTotalTitBd the value of field 'vlTotalTitBd'.
     */
    public void setVlTotalTitBd(java.math.BigDecimal vlTotalTitBd)
    {
        this._vlTotalTitBd = vlTotalTitBd;
    } //-- void setVlTotalTitBd(java.math.BigDecimal) 

    /**
     * Sets the value of field 'vlTotalTitOutros'.
     * 
     * @param vlTotalTitOutros the value of field 'vlTotalTitOutros'
     */
    public void setVlTotalTitOutros(java.math.BigDecimal vlTotalTitOutros)
    {
        this._vlTotalTitOutros = vlTotalTitOutros;
    } //-- void setVlTotalTitOutros(java.math.BigDecimal) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ConsultarDadosClienteResponse
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultardadoscliente.response.ConsultarDadosClienteResponse unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultardadoscliente.response.ConsultarDadosClienteResponse) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultardadoscliente.response.ConsultarDadosClienteResponse.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultardadoscliente.response.ConsultarDadosClienteResponse unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
