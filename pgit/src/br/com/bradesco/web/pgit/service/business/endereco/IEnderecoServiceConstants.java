/*
 * Nome: br.com.bradesco.web.pgit.service.business.endereco
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.endereco;

/**
 * Nome: IEnderecoServiceConstants
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public interface IEnderecoServiceConstants {

	/** Atributo CD_ESPECIE_ENDERECO. */
	Integer CD_ESPECIE_ENDERECO = 1;

	/** Atributo CD_ESPECIE_EMAIL. */
	Integer CD_ESPECIE_EMAIL = 2;

	/** Atributo CD_USO_ENDERECO_FIXO. */
	Integer CD_USO_ENDERECO_FIXO = 1;

	/** Atributo CD_USO_ENDERECO_TRANSACIONAL. */
	Integer CD_USO_ENDERECO_TRANSACIONAL = 2;
}
