/*
 * Nome: br.com.bradesco.web.pgit.service.business.tipocomprovante.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.tipocomprovante.bean;

/**
 * Nome: AlterarTipoComprovanteEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class AlterarTipoComprovanteEntradaDTO {

	/** Atributo cdTipoComprovante. */
	private int cdTipoComprovante;
	
	/** Atributo dsTipoComprovante. */
	private String dsTipoComprovante;
	
	/** Atributo msgPadraoOrganizacional. */
	private int msgPadraoOrganizacional;
	
	/**
	 * Get: cdTipoComprovante.
	 *
	 * @return cdTipoComprovante
	 */
	public int getCdTipoComprovante() {
		return cdTipoComprovante;
	}
	
	/**
	 * Set: cdTipoComprovante.
	 *
	 * @param cdTipoComprovante the cd tipo comprovante
	 */
	public void setCdTipoComprovante(int cdTipoComprovante) {
		this.cdTipoComprovante = cdTipoComprovante;
	}
	
	/**
	 * Get: dsTipoComprovante.
	 *
	 * @return dsTipoComprovante
	 */
	public String getDsTipoComprovante() {
		return dsTipoComprovante;
	}
	
	/**
	 * Set: dsTipoComprovante.
	 *
	 * @param dsTipoComprovante the ds tipo comprovante
	 */
	public void setDsTipoComprovante(String dsTipoComprovante) {
		this.dsTipoComprovante = dsTipoComprovante;
	}
	
	/**
	 * Get: msgPadraoOrganizacional.
	 *
	 * @return msgPadraoOrganizacional
	 */
	public int getMsgPadraoOrganizacional() {
		return msgPadraoOrganizacional;
	}
	
	/**
	 * Set: msgPadraoOrganizacional.
	 *
	 * @param msgPadraoOrganizacional the msg padrao organizacional
	 */
	public void setMsgPadraoOrganizacional(int msgPadraoOrganizacional) {
		this.msgPadraoOrganizacional = msgPadraoOrganizacional;
	}
	
	
}
