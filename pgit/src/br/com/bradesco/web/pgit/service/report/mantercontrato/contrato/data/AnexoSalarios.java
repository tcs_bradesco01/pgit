/*
 * Nome: br.com.bradesco.web.pgit.service.report.contrato.data
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 05/02/2016
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.report.mantercontrato.contrato.data;

import java.util.List;

/**
 * Nome: AnexoSalarios
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class AnexoSalarios {

    /** Atributo aplicacaoFloatingSim. */
    private String aplicacaoFloatingSim = null;

    /** Atributo aplicacaoFloatingNao. */
    private String aplicacaoFloatingNao = null;
    
    /** Atributo tipoAplicacao. */
    private String tipoAplicacao = null;
    
    /** The cd transferencia conta salarial. */
    private String cdTransferenciaContaSalarial = null;

    /** Atributo modalidadeCreditoConta. */
    private ModalidadeCondicao modalidadeCreditoConta = null;
    
    /** Atributo modalidadeTed. */
    private ModalidadeCondicao modalidadeTed = null;
    
    /** Atributo modalidadeDoc. */
    private ModalidadeCondicao modalidadeDoc = null;

    /** Atributo modalidadeOrdemPagamento. */
    private ModalidadeCondicao modalidadeOrdemPagamento = null;

    /** Atributo isentoTarifas. */
    private List<ProdutoServico> isentoTarifas = null;
    
    /** Atributo guicheCaixa. */
    private List<ProdutoServico> guicheCaixa = null;

    /** Atributo autoAtendimento. */
    private List<ProdutoServico> autoAtendimento = null;

    /** Atributo foneFacil. */
    private List<ProdutoServico> foneFacil = null;

    /** Atributo internetBanking. */
    private List<ProdutoServico> internetBanking = null;
    
    /**
     * Instancia um novo AnexoSalarios
     * 
     * @see
     */
    public AnexoSalarios() {
        super();
    }

    /**
     * Nome: getAplicacaoFloatingSim.
     *
     * @return aplicacaoFloatingSim
     */
    public String getAplicacaoFloatingSim() {
        return aplicacaoFloatingSim;
    }

    /**
     * Nome: setAplicacaoFloatingSim.
     *
     * @param aplicacaoFloatingSim the aplicacao floating sim
     */
    public void setAplicacaoFloatingSim(String aplicacaoFloatingSim) {
        this.aplicacaoFloatingSim = aplicacaoFloatingSim;
    }

    /**
     * Nome: getAplicacaoFloatingNao.
     *
     * @return aplicacaoFloatingNao
     */
    public String getAplicacaoFloatingNao() {
        return aplicacaoFloatingNao;
    }

    /**
     * Nome: setAplicacaoFloatingNao.
     *
     * @param aplicacaoFloatingNao the aplicacao floating nao
     */
    public void setAplicacaoFloatingNao(String aplicacaoFloatingNao) {
        this.aplicacaoFloatingNao = aplicacaoFloatingNao;
    }

    /**
     * Nome: getModalidadeCreditoConta.
     *
     * @return modalidadeCreditoConta
     */
    public ModalidadeCondicao getModalidadeCreditoConta() {
        return modalidadeCreditoConta;
    }

    /**
     * Nome: setModalidadeCreditoConta.
     *
     * @param modalidadeCreditoConta the modalidade credito conta
     */
    public void setModalidadeCreditoConta(ModalidadeCondicao modalidadeCreditoConta) {
        this.modalidadeCreditoConta = modalidadeCreditoConta;
    }

    /**
     * Nome: getModalidadeTed.
     *
     * @return modalidadeTed
     */
    public ModalidadeCondicao getModalidadeTed() {
        return modalidadeTed;
    }

    /**
     * Nome: setModalidadeTed.
     *
     * @param modalidadeTed the modalidade ted
     */
    public void setModalidadeTed(ModalidadeCondicao modalidadeTed) {
        this.modalidadeTed = modalidadeTed;
    }

    /**
     * Nome: getModalidadeDoc.
     *
     * @return modalidadeDoc
     */
    public ModalidadeCondicao getModalidadeDoc() {
        return modalidadeDoc;
    }

    /**
     * Nome: setModalidadeDoc.
     *
     * @param modalidadeDoc the modalidade doc
     */
    public void setModalidadeDoc(ModalidadeCondicao modalidadeDoc) {
        this.modalidadeDoc = modalidadeDoc;
    }

    /**
     * Nome: getModalidadeOrdemPagamento.
     *
     * @return modalidadeOrdemPagamento
     */
    public ModalidadeCondicao getModalidadeOrdemPagamento() {
        return modalidadeOrdemPagamento;
    }

    /**
     * Nome: setModalidadeOrdemPagamento.
     *
     * @param modalidadeOrdemPagamento the modalidade ordem pagamento
     */
    public void setModalidadeOrdemPagamento(ModalidadeCondicao modalidadeOrdemPagamento) {
        this.modalidadeOrdemPagamento = modalidadeOrdemPagamento;
    }

    /**
     * Nome: getIsentoTarifas.
     *
     * @return isentoTarifas
     */
    public List<ProdutoServico> getIsentoTarifas() {
        return isentoTarifas;
    }

    /**
     * Nome: setIsentoTarifas.
     *
     * @param isentoTarifas the isento tarifas
     */
    public void setIsentoTarifas(List<ProdutoServico> isentoTarifas) {
        this.isentoTarifas = isentoTarifas;
    }

    /**
     * Nome: getGuicheCaixa.
     *
     * @return guicheCaixa
     */
    public List<ProdutoServico> getGuicheCaixa() {
        return guicheCaixa;
    }

    /**
     * Nome: setGuicheCaixa.
     *
     * @param guicheCaixa the guiche caixa
     */
    public void setGuicheCaixa(List<ProdutoServico> guicheCaixa) {
        this.guicheCaixa = guicheCaixa;
    }

    /**
     * Nome: getAutoAtendimento.
     *
     * @return autoAtendimento
     */
    public List<ProdutoServico> getAutoAtendimento() {
        return autoAtendimento;
    }

    /**
     * Nome: setAutoAtendimento.
     *
     * @param autoAtendimento the auto atendimento
     */
    public void setAutoAtendimento(List<ProdutoServico> autoAtendimento) {
        this.autoAtendimento = autoAtendimento;
    }

    /**
     * Nome: getFoneFacil.
     *
     * @return foneFacil
     */
    public List<ProdutoServico> getFoneFacil() {
        return foneFacil;
    }

    /**
     * Nome: setFoneFacil.
     *
     * @param foneFacil the fone facil
     */
    public void setFoneFacil(List<ProdutoServico> foneFacil) {
        this.foneFacil = foneFacil;
    }

    /**
     * Nome: getInternetBanking.
     *
     * @return internetBanking
     */
    public List<ProdutoServico> getInternetBanking() {
        return internetBanking;
    }

    /**
     * Nome: setInternetBanking.
     *
     * @param internetBanking the internet banking
     */
    public void setInternetBanking(List<ProdutoServico> internetBanking) {
        this.internetBanking = internetBanking;
    }

	/**
	 * Sets the tipo aplicacao.
	 *
	 * @param tipoAplicacao the tipoAplicacao to set
	 */
	public void setTipoAplicacao(String tipoAplicacao) {
		this.tipoAplicacao = tipoAplicacao;
	}

	/**
	 * Gets the tipo aplicacao.
	 *
	 * @return the tipoAplicacao
	 */
	public String getTipoAplicacao() {
		return tipoAplicacao;
	}

	/**
	 * Gets the cd transferencia conta salarial.
	 *
	 * @return the cd transferencia conta salarial
	 */
	public String getCdTransferenciaContaSalarial() {
		return cdTransferenciaContaSalarial;
	}

	/**
	 * Sets the cd transferencia conta salarial.
	 *
	 * @param cdTransferenciaContaSalarial the new cd transferencia conta salarial
	 */
	public void setCdTransferenciaContaSalarial(String cdTransferenciaContaSalarial) {
		this.cdTransferenciaContaSalarial = cdTransferenciaContaSalarial;
	}

}
