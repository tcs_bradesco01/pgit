/*
 * Nome: br.com.bradesco.web.pgit.service.business.manterambienteoperacaocontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.manterambienteoperacaocontrato.bean;

/**
 * Nome: DetalharAmbienteSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class DetalharAmbienteSaidaDTO {
	
	/** Atributo codMensagem. */
	private String codMensagem;
	
	/** Atributo mensagem. */
	private String mensagem;
	
	/** Atributo cdUsuarioInclusao. */
	private String cdUsuarioInclusao;
	
	/** Atributo cdCanalInclusao. */
	private Integer cdCanalInclusao;
	
	/** Atributo dsCanalInclusao. */
	private String dsCanalInclusao;
	
	/** Atributo nmOperacaoFluxoInclusao. */
	private String nmOperacaoFluxoInclusao;
	
	/** Atributo hrManutencaoRegistroManuntencao. */
	private String hrManutencaoRegistroManuntencao;
	
	/** Atributo hrManutencaRegistroInclusao. */
	private String hrManutencaRegistroInclusao;
	
	/** Atributo cdUsuarioManutencao. */
	private String cdUsuarioManutencao;
	
	/** Atributo cdCanalManutencao. */
	private Integer cdCanalManutencao;
	
	/** Atributo dsCanalManutencao. */
	private String dsCanalManutencao;
	
	/** Atributo nmOperacaoFluxoManutencao. */
	private String nmOperacaoFluxoManutencao;

	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}

	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}

	/**
	 * Get: cdUsuarioInclusao.
	 *
	 * @return cdUsuarioInclusao
	 */
	public String getCdUsuarioInclusao() {
		return cdUsuarioInclusao;
	}

	/**
	 * Get: cdCanalInclusao.
	 *
	 * @return cdCanalInclusao
	 */
	public Integer getCdCanalInclusao() {
		return cdCanalInclusao;
	}

	/**
	 * Get: dsCanalInclusao.
	 *
	 * @return dsCanalInclusao
	 */
	public String getDsCanalInclusao() {
		return dsCanalInclusao;
	}

	/**
	 * Get: nmOperacaoFluxoInclusao.
	 *
	 * @return nmOperacaoFluxoInclusao
	 */
	public String getNmOperacaoFluxoInclusao() {
		return nmOperacaoFluxoInclusao;
	}

	/**
	 * Get: hrManutencaoRegistroManuntencao.
	 *
	 * @return hrManutencaoRegistroManuntencao
	 */
	public String getHrManutencaoRegistroManuntencao() {
		return hrManutencaoRegistroManuntencao;
	}

	/**
	 * Get: cdUsuarioManutencao.
	 *
	 * @return cdUsuarioManutencao
	 */
	public String getCdUsuarioManutencao() {
		return cdUsuarioManutencao;
	}

	/**
	 * Get: cdCanalManutencao.
	 *
	 * @return cdCanalManutencao
	 */
	public Integer getCdCanalManutencao() {
		return cdCanalManutencao;
	}

	/**
	 * Get: dsCanalManutencao.
	 *
	 * @return dsCanalManutencao
	 */
	public String getDsCanalManutencao() {
		return dsCanalManutencao;
	}

	/**
	 * Get: nmOperacaoFluxoManutencao.
	 *
	 * @return nmOperacaoFluxoManutencao
	 */
	public String getNmOperacaoFluxoManutencao() {
		return nmOperacaoFluxoManutencao;
	}

	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}

	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	/**
	 * Set: cdUsuarioInclusao.
	 *
	 * @param cdUsuarioInclusao the cd usuario inclusao
	 */
	public void setCdUsuarioInclusao(String cdUsuarioInclusao) {
		this.cdUsuarioInclusao = cdUsuarioInclusao;
	}

	/**
	 * Set: cdCanalInclusao.
	 *
	 * @param cdCanalInclusao the cd canal inclusao
	 */
	public void setCdCanalInclusao(Integer cdCanalInclusao) {
		this.cdCanalInclusao = cdCanalInclusao;
	}

	/**
	 * Set: dsCanalInclusao.
	 *
	 * @param dsCanalInclusao the ds canal inclusao
	 */
	public void setDsCanalInclusao(String dsCanalInclusao) {
		this.dsCanalInclusao = dsCanalInclusao;
	}

	/**
	 * Set: nmOperacaoFluxoInclusao.
	 *
	 * @param nmOperacaoFluxoInclusao the nm operacao fluxo inclusao
	 */
	public void setNmOperacaoFluxoInclusao(String nmOperacaoFluxoInclusao) {
		this.nmOperacaoFluxoInclusao = nmOperacaoFluxoInclusao;
	}

	/**
	 * Set: hrManutencaoRegistroManuntencao.
	 *
	 * @param hrManutencaoRegistroManuntencao the hr manutencao registro manuntencao
	 */
	public void setHrManutencaoRegistroManuntencao(
			String hrManutencaoRegistroManuntencao) {
		this.hrManutencaoRegistroManuntencao = hrManutencaoRegistroManuntencao;
	}

	/**
	 * Set: cdUsuarioManutencao.
	 *
	 * @param cdUsuarioManutencao the cd usuario manutencao
	 */
	public void setCdUsuarioManutencao(String cdUsuarioManutencao) {
		this.cdUsuarioManutencao = cdUsuarioManutencao;
	}

	/**
	 * Set: cdCanalManutencao.
	 *
	 * @param cdCanalManutencao the cd canal manutencao
	 */
	public void setCdCanalManutencao(Integer cdCanalManutencao) {
		this.cdCanalManutencao = cdCanalManutencao;
	}

	/**
	 * Set: dsCanalManutencao.
	 *
	 * @param dsCanalManutencao the ds canal manutencao
	 */
	public void setDsCanalManutencao(String dsCanalManutencao) {
		this.dsCanalManutencao = dsCanalManutencao;
	}

	/**
	 * Set: nmOperacaoFluxoManutencao.
	 *
	 * @param nmOperacaoFluxoManutencao the nm operacao fluxo manutencao
	 */
	public void setNmOperacaoFluxoManutencao(String nmOperacaoFluxoManutencao) {
		this.nmOperacaoFluxoManutencao = nmOperacaoFluxoManutencao;
	}

	/**
	 * Get: hrManutencaRegistroInclusao.
	 *
	 * @return hrManutencaRegistroInclusao
	 */
	public String getHrManutencaRegistroInclusao() {
		return hrManutencaRegistroInclusao;
	}

	/**
	 * Set: hrManutencaRegistroInclusao.
	 *
	 * @param hrManutencaRegistroInclusao the hr manutenca registro inclusao
	 */
	public void setHrManutencaRegistroInclusao(
			String hrManutencaRegistroInclusao) {
		this.hrManutencaRegistroInclusao = hrManutencaRegistroInclusao;
	}

}
