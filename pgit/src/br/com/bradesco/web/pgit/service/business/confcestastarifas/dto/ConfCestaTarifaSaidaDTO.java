package br.com.bradesco.web.pgit.service.business.confcestastarifas.dto;

import java.io.Serializable;
import java.util.List;

public class ConfCestaTarifaSaidaDTO implements Serializable{

	private static final long serialVersionUID = 8929777896289339422L;
	
    private Integer numLinhas;
	private List<OcorrenciasSaidaDTO> listaOcorrenciasSaidaDTO;

	public Integer getNumLinhas() {
		return numLinhas;
	}

	public void setNumLinhas(Integer numLinhas) {
		this.numLinhas = numLinhas;
	}

	public List<OcorrenciasSaidaDTO> getListaOcorrenciasSaidaDTO() {
		return listaOcorrenciasSaidaDTO;
	}

	public void setListaOcorrenciasSaidaDTO(
			List<OcorrenciasSaidaDTO> listaOcorrenciasSaidaDTO) {
		this.listaOcorrenciasSaidaDTO = listaOcorrenciasSaidaDTO;
	}

}
