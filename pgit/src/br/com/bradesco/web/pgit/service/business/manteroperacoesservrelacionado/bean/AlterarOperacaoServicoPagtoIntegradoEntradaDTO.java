/*
 * Nome: br.com.bradesco.web.pgit.service.business.manteroperacoesservrelacionado.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 13/03/2017
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.manteroperacoesservrelacionado.bean;

import java.math.BigDecimal;

/**
 * Nome: AlterarOperacaoServicoPagtoIntegradoEntradaDTO
 * <p>
 * Prop�sito:
 * </p>
 * 
 * @author : todo!
 * 
 * @version :
 */
public class AlterarOperacaoServicoPagtoIntegradoEntradaDTO {

    /**
     * Atributo Integer
     */
    private Integer cdProdutoServicoOperacional = null;

    /**
     * Atributo Integer
     */
    private Integer cdProdutoOperacionalRelacionado = null;

    /**
     * Atributo Integer
     */
    private Integer cdOperacaoProdutoServico = null;

    /**
     * Atributo Integer
     */
    private Integer cdOperacaoServicoPagamentoIntegrado = null;

    /**
     * Atributo Integer
     */
    private Integer cdNaturezaOperacaoPagamento = null;

    /**
     * Atributo Integer
     */
    private Integer cdTipoAlcadaTarifa = null;

    /**
     * Atributo BigDecimal
     */
    private BigDecimal vrAlcadaTarifaAgencia = null;

    /**
     * Atributo BigDecimal
     */
    private BigDecimal pcAlcadaTarifaAgencia = null;

    /**
     * Nome: getCdProdutoServicoOperacional
     * 
     * @return cdProdutoServicoOperacional
     */
    public Integer getCdProdutoServicoOperacional() {
        return cdProdutoServicoOperacional;
    }

    /**
     * Nome: setCdProdutoServicoOperacional
     * 
     * @param cdProdutoServicoOperacional
     */
    public void setCdProdutoServicoOperacional(Integer cdProdutoServicoOperacional) {
        this.cdProdutoServicoOperacional = cdProdutoServicoOperacional;
    }

    /**
     * Nome: getCdProdutoOperacionalRelacionado
     * 
     * @return cdProdutoOperacionalRelacionado
     */
    public Integer getCdProdutoOperacionalRelacionado() {
        return cdProdutoOperacionalRelacionado;
    }

    /**
     * Nome: setCdProdutoOperacionalRelacionado
     * 
     * @param cdProdutoOperacionalRelacionado
     */
    public void setCdProdutoOperacionalRelacionado(Integer cdProdutoOperacionalRelacionado) {
        this.cdProdutoOperacionalRelacionado = cdProdutoOperacionalRelacionado;
    }

    /**
     * Nome: getCdOperacaoProdutoServico
     * 
     * @return cdOperacaoProdutoServico
     */
    public Integer getCdOperacaoProdutoServico() {
        return cdOperacaoProdutoServico;
    }

    /**
     * Nome: setCdOperacaoProdutoServico
     * 
     * @param cdOperacaoProdutoServico
     */
    public void setCdOperacaoProdutoServico(Integer cdOperacaoProdutoServico) {
        this.cdOperacaoProdutoServico = cdOperacaoProdutoServico;
    }

    /**
     * Nome: getCdOperacaoServicoPagamentoIntegrado
     * 
     * @return cdOperacaoServicoPagamentoIntegrado
     */
    public Integer getCdOperacaoServicoPagamentoIntegrado() {
        return cdOperacaoServicoPagamentoIntegrado;
    }

    /**
     * Nome: setCdOperacaoServicoPagamentoIntegrado
     * 
     * @param cdOperacaoServicoPagamentoIntegrado
     */
    public void setCdOperacaoServicoPagamentoIntegrado(Integer cdOperacaoServicoPagamentoIntegrado) {
        this.cdOperacaoServicoPagamentoIntegrado = cdOperacaoServicoPagamentoIntegrado;
    }

    /**
     * Nome: getCdNaturezaOperacaoPagamento
     * 
     * @return cdNaturezaOperacaoPagamento
     */
    public Integer getCdNaturezaOperacaoPagamento() {
        return cdNaturezaOperacaoPagamento;
    }

    /**
     * Nome: setCdNaturezaOperacaoPagamento
     * 
     * @param cdNaturezaOperacaoPagamento
     */
    public void setCdNaturezaOperacaoPagamento(Integer cdNaturezaOperacaoPagamento) {
        this.cdNaturezaOperacaoPagamento = cdNaturezaOperacaoPagamento;
    }

    /**
     * Nome: getCdTipoAlcadaTarifa
     * 
     * @return cdTipoAlcadaTarifa
     */
    public Integer getCdTipoAlcadaTarifa() {
        return cdTipoAlcadaTarifa;
    }

    /**
     * Nome: setCdTipoAlcadaTarifa
     * 
     * @param cdTipoAlcadaTarifa
     */
    public void setCdTipoAlcadaTarifa(Integer cdTipoAlcadaTarifa) {
        this.cdTipoAlcadaTarifa = cdTipoAlcadaTarifa;
    }

    /**
     * Nome: getVrAlcadaTarifaAgencia
     * 
     * @return vrAlcadaTarifaAgencia
     */
    public BigDecimal getVrAlcadaTarifaAgencia() {
        return vrAlcadaTarifaAgencia;
    }

    /**
     * Nome: setVrAlcadaTarifaAgencia
     * 
     * @param vrAlcadaTarifaAgencia
     */
    public void setVrAlcadaTarifaAgencia(BigDecimal vrAlcadaTarifaAgencia) {
        this.vrAlcadaTarifaAgencia = vrAlcadaTarifaAgencia;
    }

    /**
     * Nome: getPcAlcadaTarifaAgencia
     * 
     * @return pcAlcadaTarifaAgencia
     */
    public BigDecimal getPcAlcadaTarifaAgencia() {
        return pcAlcadaTarifaAgencia;
    }

    /**
     * Nome: setPcAlcadaTarifaAgencia
     * 
     * @param pcAlcadaTarifaAgencia
     */
    public void setPcAlcadaTarifaAgencia(BigDecimal pcAlcadaTarifaAgencia) {
        this.pcAlcadaTarifaAgencia = pcAlcadaTarifaAgencia;
    }
}
