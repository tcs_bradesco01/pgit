/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listarparametros.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ListarParametrosRequest.
 * 
 * @version $Revision$ $Date$
 */
public class ListarParametrosRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _nrMaximoOcorrencias
     */
    private int _nrMaximoOcorrencias = 0;

    /**
     * keeps track of state for field: _nrMaximoOcorrencias
     */
    private boolean _has_nrMaximoOcorrencias;


      //----------------/
     //- Constructors -/
    //----------------/

    public ListarParametrosRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarparametros.request.ListarParametrosRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteNrMaximoOcorrencias
     * 
     */
    public void deleteNrMaximoOcorrencias()
    {
        this._has_nrMaximoOcorrencias= false;
    } //-- void deleteNrMaximoOcorrencias() 

    /**
     * Returns the value of field 'nrMaximoOcorrencias'.
     * 
     * @return int
     * @return the value of field 'nrMaximoOcorrencias'.
     */
    public int getNrMaximoOcorrencias()
    {
        return this._nrMaximoOcorrencias;
    } //-- int getNrMaximoOcorrencias() 

    /**
     * Method hasNrMaximoOcorrencias
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrMaximoOcorrencias()
    {
        return this._has_nrMaximoOcorrencias;
    } //-- boolean hasNrMaximoOcorrencias() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'nrMaximoOcorrencias'.
     * 
     * @param nrMaximoOcorrencias the value of field
     * 'nrMaximoOcorrencias'.
     */
    public void setNrMaximoOcorrencias(int nrMaximoOcorrencias)
    {
        this._nrMaximoOcorrencias = nrMaximoOcorrencias;
        this._has_nrMaximoOcorrencias = true;
    } //-- void setNrMaximoOcorrencias(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ListarParametrosRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listarparametros.request.ListarParametrosRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listarparametros.request.ListarParametrosRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listarparametros.request.ListarParametrosRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarparametros.request.ListarParametrosRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
