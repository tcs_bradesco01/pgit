/*
 * Nome: br.com.bradesco.web.pgit.service.business.consultas.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 19/10/2016
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.consultas.bean;

import java.util.ArrayList;
import java.util.List;

/**
 * Nome: ListarTipoRetiradaSaidaDTO
 * <p>
 * Prop�sito:
 * </p>
 * .
 * 
 * @author : todo!
 * @version :
 */
public class ListarTipoRetiradaSaidaDTO {

    /** Atributo codMensagem. */
    private String codMensagem = null;

    /** Atributo mensagem. */
    private String mensagem = null;

    /** Atributo numeroConsulta. */
    private Integer numeroConsulta = null;

    /** Atributo ocorrencias. */
    private List<ListarTipoRetiradaOcorrenciasSaidaDTO> ocorrencias = new ArrayList<ListarTipoRetiradaOcorrenciasSaidaDTO>();

    /**
     * Set: codMensagem.
     * 
     * @param codMensagem
     *            the cod mensagem
     */
    public void setCodMensagem(String codMensagem) {
        this.codMensagem = codMensagem;
    }

    /**
     * Get: codMensagem.
     * 
     * @return codMensagem
     */
    public String getCodMensagem() {
        return this.codMensagem;
    }

    /**
     * Set: mensagem.
     * 
     * @param mensagem
     *            the mensagem
     */
    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }

    /**
     * Get: mensagem.
     * 
     * @return mensagem
     */
    public String getMensagem() {
        return this.mensagem;
    }

    /**
     * Set: numeroConsulta.
     * 
     * @param numeroConsulta
     *            the numero consulta
     */
    public void setNumeroConsulta(Integer numeroConsulta) {
        this.numeroConsulta = numeroConsulta;
    }

    /**
     * Get: numeroConsulta.
     * 
     * @return numeroConsulta
     */
    public Integer getNumeroConsulta() {
        return this.numeroConsulta;
    }

    /**
     * Set: ocorrencias.
     * 
     * @param ocorrencias
     *            the ocorrencias
     */
    public void setOcorrencias(List<ListarTipoRetiradaOcorrenciasSaidaDTO> ocorrencias) {
        this.ocorrencias = ocorrencias;
    }

    /**
     * Get: ocorrencias.
     * 
     * @return ocorrencias
     */
    public List<ListarTipoRetiradaOcorrenciasSaidaDTO> getOcorrencias() {
        return this.ocorrencias;
    }
}