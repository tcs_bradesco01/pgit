/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/interfaz.ftl,v $
 * $Id: interfaz.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: interfaz.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.mantervinculoempresasalario;

import java.util.List;

import br.com.bradesco.web.pgit.service.business.mantercadcontadestino.bean.ConsultarBancoAgenciaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadcontadestino.bean.ConsultarBancoAgenciaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantervinculoempresasalario.bean.ConsultarDetHistCtaSalarioEmpresaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantervinculoempresasalario.bean.ConsultarDetHistCtaSalarioEmpresaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantervinculoempresasalario.bean.ConsultarDetVinculoCtaSalarioEmpresaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantervinculoempresasalario.bean.ConsultarDetVinculoCtaSalarioEmpresaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantervinculoempresasalario.bean.ExcluirVinculoCtaSalarioEmpresaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantervinculoempresasalario.bean.ExcluirVinculoCtaSalarioEmpresaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantervinculoempresasalario.bean.IncluirVinculoEmprPagContSalEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantervinculoempresasalario.bean.IncluirVinculoEmprPagContSalSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantervinculoempresasalario.bean.ListarVinculoCtaSalarioEmpresaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantervinculoempresasalario.bean.ListarVinculoCtaSalarioEmpresaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantervinculoempresasalario.bean.ListarVinculoHistCtaSalarioEmpresaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantervinculoempresasalario.bean.ListarVinculoHistCtaSalarioEmpresaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantervinculoempresasalario.bean.SubstituirVinculoCtaSalarioEmpresaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantervinculoempresasalario.bean.SubstituirVinculoCtaSalarioEmpresaSaidaDTO;

/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Interface do adaptador: Mantervinculoempresasalario
 * </p>
 * 
 * @comment CODIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public interface IManterVinculoEmpresaSalarioService {

	 /**
 	 * Pesquisa manter vinculo empresa salario.
 	 *
 	 * @param entrada the entrada
 	 * @return the list< listar vinculo cta salario empresa saida dt o>
 	 */
 	List<ListarVinculoCtaSalarioEmpresaSaidaDTO> pesquisaManterVinculoEmpresaSalario(ListarVinculoCtaSalarioEmpresaEntradaDTO entrada);
	
	 /**
 	 * Detalhe conta salario emp.
 	 *
 	 * @param entrada the entrada
 	 * @return the consultar det vinculo cta salario empresa saida dto
 	 */
 	ConsultarDetVinculoCtaSalarioEmpresaSaidaDTO detalheContaSalarioEmp(ConsultarDetVinculoCtaSalarioEmpresaEntradaDTO entrada);
	
	 /**
 	 * Incluir vinculo empr pag cont sal.
 	 *
 	 * @param entrada the entrada
 	 * @return the incluir vinculo empr pag cont sal saida dto
 	 */
 	IncluirVinculoEmprPagContSalSaidaDTO incluirVinculoEmprPagContSal(IncluirVinculoEmprPagContSalEntradaDTO entrada);
	 
	 /**
 	 * Substituir manter vinc cta salario empr.
 	 *
 	 * @param entrada the entrada
 	 * @return the substituir vinculo cta salario empresa saida dto
 	 */
 	SubstituirVinculoCtaSalarioEmpresaSaidaDTO substituirManterVincCtaSalarioEmpr(SubstituirVinculoCtaSalarioEmpresaEntradaDTO entrada);
	
	 /**
 	 * Excluir manter vinc cta salario emp.
 	 *
 	 * @param entrada the entrada
 	 * @return the excluir vinculo cta salario empresa saida dto
 	 */
 	ExcluirVinculoCtaSalarioEmpresaSaidaDTO excluirManterVincCtaSalarioEmp (ExcluirVinculoCtaSalarioEmpresaEntradaDTO entrada);
	
	 /**
 	 * Pesquisar historico vinc cta salario emp.
 	 *
 	 * @param entradaListarHistorico the entrada listar historico
 	 * @return the list< listar vinculo hist cta salario empresa saida dt o>
 	 */
 	List<ListarVinculoHistCtaSalarioEmpresaSaidaDTO> pesquisarHistoricoVincCtaSalarioEmp(ListarVinculoHistCtaSalarioEmpresaEntradaDTO entradaListarHistorico);
	
	 /**
 	 * Detalhar historico vinc cta salario emp.
 	 *
 	 * @param entradaDetalheHistorico the entrada detalhe historico
 	 * @return the consultar det hist cta salario empresa saida dto
 	 */
 	ConsultarDetHistCtaSalarioEmpresaSaidaDTO detalharHistoricoVincCtaSalarioEmp(ConsultarDetHistCtaSalarioEmpresaEntradaDTO entradaDetalheHistorico);
	
	 /**
 	 * Consultar descricao banco agencia.
 	 *
 	 * @param entradaConsultarBancoAgencia the entrada consultar banco agencia
 	 * @return the consultar banco agencia saida dto
 	 */
 	ConsultarBancoAgenciaSaidaDTO consultarDescricaoBancoAgencia(ConsultarBancoAgenciaEntradaDTO entradaConsultarBancoAgencia);
}


