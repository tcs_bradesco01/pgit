/*
 * Nome: br.com.bradesco.web.pgit.service.business.endereco.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.endereco.bean;


/**
 * Nome: ExcluirEnderecoParticipanteEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ExcluirEnderecoParticipanteEntradaDTO {

    /** Atributo cdpessoaJuridicaContrato. */
    private Long cdpessoaJuridicaContrato;

    /** Atributo cdTipoContratoNegocio. */
    private Integer cdTipoContratoNegocio;

    /** Atributo nrSequenciaContratoNegocio. */
    private Long nrSequenciaContratoNegocio;

    /** Atributo cdTipoParticipacaoPessoa. */
    private Integer cdTipoParticipacaoPessoa;

    /** Atributo cdPessoa. */
    private Long cdPessoa;

    /** Atributo cdFinalidadeEnderecoContrato. */
    private Integer cdFinalidadeEnderecoContrato;

    /**
     * Set: cdpessoaJuridicaContrato.
     *
     * @param cdpessoaJuridicaContrato the cdpessoa juridica contrato
     */
    public void setCdpessoaJuridicaContrato(Long cdpessoaJuridicaContrato) {
        this.cdpessoaJuridicaContrato = cdpessoaJuridicaContrato;
    }

    /**
     * Get: cdpessoaJuridicaContrato.
     *
     * @return cdpessoaJuridicaContrato
     */
    public Long getCdpessoaJuridicaContrato() {
        return this.cdpessoaJuridicaContrato;
    }

    /**
     * Set: cdTipoContratoNegocio.
     *
     * @param cdTipoContratoNegocio the cd tipo contrato negocio
     */
    public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
        this.cdTipoContratoNegocio = cdTipoContratoNegocio;
    }

    /**
     * Get: cdTipoContratoNegocio.
     *
     * @return cdTipoContratoNegocio
     */
    public Integer getCdTipoContratoNegocio() {
        return this.cdTipoContratoNegocio;
    }

    /**
     * Set: nrSequenciaContratoNegocio.
     *
     * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
     */
    public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
        this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
    }

    /**
     * Get: nrSequenciaContratoNegocio.
     *
     * @return nrSequenciaContratoNegocio
     */
    public Long getNrSequenciaContratoNegocio() {
        return this.nrSequenciaContratoNegocio;
    }

    /**
     * Set: cdTipoParticipacaoPessoa.
     *
     * @param cdTipoParticipacaoPessoa the cd tipo participacao pessoa
     */
    public void setCdTipoParticipacaoPessoa(Integer cdTipoParticipacaoPessoa) {
        this.cdTipoParticipacaoPessoa = cdTipoParticipacaoPessoa;
    }

    /**
     * Get: cdTipoParticipacaoPessoa.
     *
     * @return cdTipoParticipacaoPessoa
     */
    public Integer getCdTipoParticipacaoPessoa() {
        return this.cdTipoParticipacaoPessoa;
    }

    /**
     * Set: cdPessoa.
     *
     * @param cdPessoa the cd pessoa
     */
    public void setCdPessoa(Long cdPessoa) {
        this.cdPessoa = cdPessoa;
    }

    /**
     * Get: cdPessoa.
     *
     * @return cdPessoa
     */
    public Long getCdPessoa() {
        return this.cdPessoa;
    }

    /**
     * Set: cdFinalidadeEnderecoContrato.
     *
     * @param cdFinalidadeEnderecoContrato the cd finalidade endereco contrato
     */
    public void setCdFinalidadeEnderecoContrato(Integer cdFinalidadeEnderecoContrato) {
        this.cdFinalidadeEnderecoContrato = cdFinalidadeEnderecoContrato;
    }

    /**
     * Get: cdFinalidadeEnderecoContrato.
     *
     * @return cdFinalidadeEnderecoContrato
     */
    public Integer getCdFinalidadeEnderecoContrato() {
        return this.cdFinalidadeEnderecoContrato;
    }
}