/*
 * Nome: br.com.bradesco.web.pgit.service.business.msgcompsalarial.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.msgcompsalarial.bean;

/**
 * Nome: AlterarMsgComprovanteSalrlEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class AlterarMsgComprovanteSalrlEntradaDTO {
	
	/** Atributo codigoTipoMensagem. */
	private Integer codigoTipoMensagem;
	
	/** Atributo tipoComprovante. */
	private Integer tipoComprovante;
	
	/** Atributo nivelMensagem. */
	private Integer nivelMensagem;
	
	/** Atributo descricao. */
	private String descricao;
	
	/** Atributo restricao. */
	private Integer restricao;
	
	/**
	 * Get: codigoTipoMensagem.
	 *
	 * @return codigoTipoMensagem
	 */
	public Integer getCodigoTipoMensagem() {
		return codigoTipoMensagem;
	}
	
	/**
	 * Set: codigoTipoMensagem.
	 *
	 * @param codigoTipoMensagem the codigo tipo mensagem
	 */
	public void setCodigoTipoMensagem(Integer codigoTipoMensagem) {
		this.codigoTipoMensagem = codigoTipoMensagem;
	}
	
	/**
	 * Get: descricao.
	 *
	 * @return descricao
	 */
	public String getDescricao() {
		return descricao;
	}
	
	/**
	 * Set: descricao.
	 *
	 * @param descricao the descricao
	 */
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	/**
	 * Get: nivelMensagem.
	 *
	 * @return nivelMensagem
	 */
	public Integer getNivelMensagem() {
		return nivelMensagem;
	}
	
	/**
	 * Set: nivelMensagem.
	 *
	 * @param nivelMensagem the nivel mensagem
	 */
	public void setNivelMensagem(Integer nivelMensagem) {
		this.nivelMensagem = nivelMensagem;
	}
	
	/**
	 * Get: restricao.
	 *
	 * @return restricao
	 */
	public Integer getRestricao() {
		return restricao;
	}
	
	/**
	 * Set: restricao.
	 *
	 * @param restricao the restricao
	 */
	public void setRestricao(Integer restricao) {
		this.restricao = restricao;
	}
	
	/**
	 * Get: tipoComprovante.
	 *
	 * @return tipoComprovante
	 */
	public Integer getTipoComprovante() {
		return tipoComprovante;
	}
	
	/**
	 * Set: tipoComprovante.
	 *
	 * @param tipoComprovante the tipo comprovante
	 */
	public void setTipoComprovante(Integer tipoComprovante) {
		this.tipoComprovante = tipoComprovante;
	}

}
