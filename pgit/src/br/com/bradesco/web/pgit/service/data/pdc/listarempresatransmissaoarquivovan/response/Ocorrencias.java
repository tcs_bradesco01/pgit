/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listarempresatransmissaoarquivovan.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdTipoParticipacao
     */
    private long _cdTipoParticipacao = 0;

    /**
     * keeps track of state for field: _cdTipoParticipacao
     */
    private boolean _has_cdTipoParticipacao;

    /**
     * Field _dsTipoParticipacao
     */
    private java.lang.String _dsTipoParticipacao;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarempresatransmissaoarquivovan.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdTipoParticipacao
     * 
     */
    public void deleteCdTipoParticipacao()
    {
        this._has_cdTipoParticipacao= false;
    } //-- void deleteCdTipoParticipacao() 

    /**
     * Returns the value of field 'cdTipoParticipacao'.
     * 
     * @return long
     * @return the value of field 'cdTipoParticipacao'.
     */
    public long getCdTipoParticipacao()
    {
        return this._cdTipoParticipacao;
    } //-- long getCdTipoParticipacao() 

    /**
     * Returns the value of field 'dsTipoParticipacao'.
     * 
     * @return String
     * @return the value of field 'dsTipoParticipacao'.
     */
    public java.lang.String getDsTipoParticipacao()
    {
        return this._dsTipoParticipacao;
    } //-- java.lang.String getDsTipoParticipacao() 

    /**
     * Method hasCdTipoParticipacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoParticipacao()
    {
        return this._has_cdTipoParticipacao;
    } //-- boolean hasCdTipoParticipacao() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdTipoParticipacao'.
     * 
     * @param cdTipoParticipacao the value of field
     * 'cdTipoParticipacao'.
     */
    public void setCdTipoParticipacao(long cdTipoParticipacao)
    {
        this._cdTipoParticipacao = cdTipoParticipacao;
        this._has_cdTipoParticipacao = true;
    } //-- void setCdTipoParticipacao(long) 

    /**
     * Sets the value of field 'dsTipoParticipacao'.
     * 
     * @param dsTipoParticipacao the value of field
     * 'dsTipoParticipacao'.
     */
    public void setDsTipoParticipacao(java.lang.String dsTipoParticipacao)
    {
        this._dsTipoParticipacao = dsTipoParticipacao;
    } //-- void setDsTipoParticipacao(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listarempresatransmissaoarquivovan.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listarempresatransmissaoarquivovan.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listarempresatransmissaoarquivovan.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarempresatransmissaoarquivovan.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
