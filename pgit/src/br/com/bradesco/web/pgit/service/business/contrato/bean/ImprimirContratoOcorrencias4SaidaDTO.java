/*
 * Nome: br.com.bradesco.web.pgit.service.business.contrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 19/02/2016
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.contrato.bean;

/**
 * Nome: ImprimirContratoOcorrencias4SaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ImprimirContratoOcorrencias4SaidaDTO {

    /** The ds produto salario guiche caixa. */
    private String dsProdutoSalarioGuicheCaixa = null;

    /** The ds servico salario guiche caixa. */
    private String dsServicoSalarioGuicheCaixa = null;

    /**
     * Instancia um novo ImprimirContratoOcorrencias4SaidaDTO.
     * 
     * @see
     */
    public ImprimirContratoOcorrencias4SaidaDTO() {
        super();
    }

    /**
     * Sets the ds produto salario guiche caixa.
     *
     * @param dsProdutoSalarioGuicheCaixa the ds produto salario guiche caixa
     */
    public void setDsProdutoSalarioGuicheCaixa(String dsProdutoSalarioGuicheCaixa) {
        this.dsProdutoSalarioGuicheCaixa = dsProdutoSalarioGuicheCaixa;
    }

    /**
     * Gets the ds produto salario guiche caixa.
     *
     * @return the ds produto salario guiche caixa
     */
    public String getDsProdutoSalarioGuicheCaixa() {
        return this.dsProdutoSalarioGuicheCaixa;
    }

    /**
     * Sets the ds servico salario guiche caixa.
     *
     * @param dsServicoSalarioGuicheCaixa the ds servico salario guiche caixa
     */
    public void setDsServicoSalarioGuicheCaixa(String dsServicoSalarioGuicheCaixa) {
        this.dsServicoSalarioGuicheCaixa = dsServicoSalarioGuicheCaixa;
    }

    /**
     * Gets the ds servico salario guiche caixa.
     *
     * @return the ds servico salario guiche caixa
     */
    public String getDsServicoSalarioGuicheCaixa() {
        return this.dsServicoSalarioGuicheCaixa;
    }
}