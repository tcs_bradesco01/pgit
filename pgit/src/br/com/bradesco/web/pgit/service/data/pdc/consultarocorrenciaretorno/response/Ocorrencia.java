/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultarocorrenciaretorno.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencia.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencia implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _textoMensagem
     */
    private java.lang.String _textoMensagem;

    /**
     * Field _conteudoEnviado
     */
    private java.lang.String _conteudoEnviado;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencia() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarocorrenciaretorno.response.Ocorrencia()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Returns the value of field 'conteudoEnviado'.
     * 
     * @return String
     * @return the value of field 'conteudoEnviado'.
     */
    public java.lang.String getConteudoEnviado()
    {
        return this._conteudoEnviado;
    } //-- java.lang.String getConteudoEnviado() 

    /**
     * Returns the value of field 'textoMensagem'.
     * 
     * @return String
     * @return the value of field 'textoMensagem'.
     */
    public java.lang.String getTextoMensagem()
    {
        return this._textoMensagem;
    } //-- java.lang.String getTextoMensagem() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'conteudoEnviado'.
     * 
     * @param conteudoEnviado the value of field 'conteudoEnviado'.
     */
    public void setConteudoEnviado(java.lang.String conteudoEnviado)
    {
        this._conteudoEnviado = conteudoEnviado;
    } //-- void setConteudoEnviado(java.lang.String) 

    /**
     * Sets the value of field 'textoMensagem'.
     * 
     * @param textoMensagem the value of field 'textoMensagem'.
     */
    public void setTextoMensagem(java.lang.String textoMensagem)
    {
        this._textoMensagem = textoMensagem;
    } //-- void setTextoMensagem(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencia
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultarocorrenciaretorno.response.Ocorrencia unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultarocorrenciaretorno.response.Ocorrencia) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultarocorrenciaretorno.response.Ocorrencia.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarocorrenciaretorno.response.Ocorrencia unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
