/*
 * Nome: br.com.bradesco.web.pgit.service.business.emissaoautcomp.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.emissaoautcomp.bean;

/**
 * Nome: EmissaoAutCompDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class EmissaoAutCompDTO {

	/** Atributo servico. */
	private String servico;

	/** Atributo operacao. */
	private String operacao;

	/** Atributo checkGrid. */
	private boolean checkGrid;

	/**
	 * Get: operacao.
	 *
	 * @return operacao
	 */
	public String getOperacao() {
		return operacao;
	}

	/**
	 * Set: operacao.
	 *
	 * @param operacao the operacao
	 */
	public void setOperacao(String operacao) {
		this.operacao = operacao;
	}

	/**
	 * Get: servico.
	 *
	 * @return servico
	 */
	public String getServico() {
		return servico;
	}

	/**
	 * Set: servico.
	 *
	 * @param servico the servico
	 */
	public void setServico(String servico) {
		this.servico = servico;
	}

	/**
	 * Is check grid.
	 *
	 * @return true, if is check grid
	 */
	public boolean isCheckGrid() {
		return checkGrid;
	}

	/**
	 * Set: checkGrid.
	 *
	 * @param checkGrid the check grid
	 */
	public void setCheckGrid(boolean checkGrid) {
		this.checkGrid = checkGrid;
	}

}
