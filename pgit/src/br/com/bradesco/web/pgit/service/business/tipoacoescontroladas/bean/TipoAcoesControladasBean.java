/*
 * Nome: br.com.bradesco.web.pgit.service.business.tipoacoescontroladas.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.tipoacoescontroladas.bean;

import java.util.ArrayList;
import java.util.List;

import javax.faces.model.SelectItem;

import br.com.bradesco.web.aq.application.error.BradescoViewException;
import br.com.bradesco.web.pgit.service.business.tipoacoescontroladas.ITipoAcoesControladasService;


/**
 * Nome: TipoAcoesControladasBean
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class TipoAcoesControladasBean {
	
	/** Atributo codListaRadio. */
	private int codListaRadio;
	
	/** Atributo codigo. */
	private  String codigo;
	
	/** Atributo numero. */
	private  String numero;
	
	/** Atributo descricao. */
	private  String descricao;
	
	/** Atributo listaGridAcoesControladas. */
	private List<TipoAcoesControladasDTO> listaGridAcoesControladas;
	
	/** Atributo listaControleRadioAcoes. */
	private  List<SelectItem> listaControleRadioAcoes;
	
	/** Atributo mostraBotoes. */
	private boolean mostraBotoes;	
	
	/** Atributo manterTipoAcoesControladasServiceImpl. */
	private ITipoAcoesControladasService manterTipoAcoesControladasServiceImpl;
	
	
	/**
	 * Get: manterTipoAcoesControladasServiceImpl.
	 *
	 * @return manterTipoAcoesControladasServiceImpl
	 */
	public ITipoAcoesControladasService getManterTipoAcoesControladasServiceImpl() {
		return manterTipoAcoesControladasServiceImpl;
	}
	
	/**
	 * Set: manterTipoAcoesControladasServiceImpl.
	 *
	 * @param manterTipoAcoesControladasServiceImpl the manter tipo acoes controladas service impl
	 */
	public void setManterTipoAcoesControladasServiceImpl(
			ITipoAcoesControladasService manterTipoAcoesControladasServiceImpl) {
		this.manterTipoAcoesControladasServiceImpl = manterTipoAcoesControladasServiceImpl;
	}
	
	/**
	 * Get: codigo.
	 *
	 * @return codigo
	 */
	public String getCodigo() {
		return codigo;
	}
	
	/**
	 * Set: codigo.
	 *
	 * @param codigo the codigo
	 */
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	
	/**
	 * Get: codListaRadio.
	 *
	 * @return codListaRadio
	 */
	public int getCodListaRadio() {
		return codListaRadio;
	}
	
	/**
	 * Set: codListaRadio.
	 *
	 * @param codListaRadio the cod lista radio
	 */
	public void setCodListaRadio(int codListaRadio) {
		this.codListaRadio = codListaRadio;
	}
	
	/**
	 * Get: descricao.
	 *
	 * @return descricao
	 */
	public String getDescricao() {
		return descricao;
	}
	
	/**
	 * Set: descricao.
	 *
	 * @param descricao the descricao
	 */
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	/**
	 * Get: listaControleRadioAcoes.
	 *
	 * @return listaControleRadioAcoes
	 */
	public List<SelectItem> getListaControleRadioAcoes() {
		return listaControleRadioAcoes;
	}
	
	/**
	 * Set: listaControleRadioAcoes.
	 *
	 * @param listaControleRadioAcoes the lista controle radio acoes
	 */
	public void setListaControleRadioAcoes(List<SelectItem> listaControleRadioAcoes) {
		this.listaControleRadioAcoes = listaControleRadioAcoes;
	}
	
	/**
	 * Get: listaGridAcoesControladas.
	 *
	 * @return listaGridAcoesControladas
	 */
	public List<TipoAcoesControladasDTO> getListaGridAcoesControladas() {
		return listaGridAcoesControladas;
	}
	
	/**
	 * Set: listaGridAcoesControladas.
	 *
	 * @param listaGridAcoesControladas the lista grid acoes controladas
	 */
	public void setListaGridAcoesControladas(
			List<TipoAcoesControladasDTO> listaGridAcoesControladas) {
		this.listaGridAcoesControladas = listaGridAcoesControladas;
	}
	
	/**
	 * Is mostra botoes.
	 *
	 * @return true, if is mostra botoes
	 */
	public boolean isMostraBotoes() {
		return mostraBotoes;
	}
	
	/**
	 * Set: mostraBotoes.
	 *
	 * @param mostraBotoes the mostra botoes
	 */
	public void setMostraBotoes(boolean mostraBotoes) {
		this.mostraBotoes = mostraBotoes;
	}
	
	/**
	 * Get: numero.
	 *
	 * @return numero
	 */
	public String getNumero() {
		return numero;
	}
	
	/**
	 * Set: numero.
	 *
	 * @param numero the numero
	 */
	public void setNumero(String numero) {
		this.numero = numero;
	}
	
	/**
	 * Pesquisar.
	 *
	 * @return the string
	 */
	public String pesquisar(){
		return "OK";
	}
	
	/**
	 * Limpar.
	 *
	 * @return the string
	 */
	public String limpar(){
		return "OK";
	}
	
	/**
	 * Incluir.
	 *
	 * @return the string
	 */
	public String incluir(){
		setNumero(null);
		setDescricao(null);
		return "INCLUIR";
	}
	
	/**
	 * Limpar incluir.
	 *
	 * @return the string
	 */
	public String limparIncluir(){
		setNumero(null);
		setDescricao(null);
		return "LIMPAR_INCLUIR";
	}
	
	/**
	 * Confirmar incluir.
	 *
	 * @return the string
	 */
	public String confirmarIncluir(){
		return "CONFIRMAR_INCLUIR";
	}
	
	/**
	 * Excluir.
	 *
	 * @return the string
	 */
	public String excluir(){
		TipoAcoesControladasDTO controladasDTO =  getListaGridAcoesControladas().get(getCodListaRadio());
		setNumero(controladasDTO.getCodigo());
		setDescricao(controladasDTO.getDescricao());		
		return "EXCLUIR";
	}
	
	/**
	 * Confirmar excluir.
	 *
	 * @return the string
	 */
	public String confirmarExcluir(){
		return "CONFIRMAR_EXCLUIR";
	}
	
	/**
	 * Voltar pesquisa.
	 *
	 * @return the string
	 */
	public String voltarPesquisa(){
		return "VOLTAR_PESQUISA";
	}
	
	/**
	 * Limpar dados.
	 *
	 * @return the string
	 */
	public String limparDados(){
		setCodigo(null);		
		return "OK";
	}
	
	/**
	 * Carrega lista.
	 *
	 * @return the string
	 */
	public String carregaLista(){
		
		/*Teste*/
		TipoAcoesControladasDTO controladasDTO = new TipoAcoesControladasDTO();
		TipoAcoesControladasDTO controladasDTO2 = new TipoAcoesControladasDTO();
		List<TipoAcoesControladasDTO> listTeste = new ArrayList<TipoAcoesControladasDTO>();
		
		controladasDTO.setCodigo("0001");
		controladasDTO.setDescricao("Descricao 1");
		controladasDTO.setLinhaSelecionada(0);
		
		controladasDTO2.setCodigo("0002");
		controladasDTO2.setDescricao("Descricao 2");
		controladasDTO2.setLinhaSelecionada(1);
		
		listTeste.add(controladasDTO);
		listTeste.add(controladasDTO2);
		/*Teste*/	
		List<SelectItem> listaControleAcoes = new ArrayList<SelectItem>();
		
		setListaGridAcoesControladas(listTeste);
		
		try{
		//	setListaGridRegras(manterRegraSegregacaoAcessoDadosServiceImpl.listaRegras(acessoDadosDTO));
			
			for (int i = 0 ; i< listTeste.size();i++){
				listaControleAcoes.add(new SelectItem(i,""));
			}
			setListaControleRadioAcoes(listaControleAcoes);
		} catch(Exception e) {
			throw new BradescoViewException(e.getMessage(), e, null);
		}
		
		if (this.getListaGridAcoesControladas().size() > 5){
			this.setMostraBotoes(true);
		}
		else{
			this.setMostraBotoes(false);
		}
		
		return "PESQUISA";
	}	
}
