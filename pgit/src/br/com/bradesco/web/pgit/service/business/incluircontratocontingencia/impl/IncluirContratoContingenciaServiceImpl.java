/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/implementation.ftl,v $
 * $Id: implementation.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: implementation.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.incluircontratocontingencia.impl;

import java.util.ArrayList;
import java.util.List;

import br.com.bradesco.web.pgit.service.business.incluircontratocontingencia.IIncluirContratoContingenciaService;
import br.com.bradesco.web.pgit.service.business.incluircontratocontingencia.IIncluirContratoContingenciaServiceConstants;
import br.com.bradesco.web.pgit.service.business.incluircontratocontingencia.bean.ConsultarListaContasInclusaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.incluircontratocontingencia.bean.ConsultarListaContasInclusaoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.incluircontratocontingencia.bean.ConsultarServRelacServperacionalEntradaDTO;
import br.com.bradesco.web.pgit.service.business.incluircontratocontingencia.bean.ConsultarServRelacServperacionalSaidaDTO;
import br.com.bradesco.web.pgit.service.business.incluircontratocontingencia.bean.IncluirContratoPgitEntradaDTO;
import br.com.bradesco.web.pgit.service.business.incluircontratocontingencia.bean.IncluirContratoPgitSaidaDTO;
import br.com.bradesco.web.pgit.service.business.incluircontratocontingencia.bean.ListarDadosCtaConvnContingenciaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.incluircontratocontingencia.bean.ListarDadosCtaConvnContingenciaOcorrenciasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.incluircontratocontingencia.bean.ListarDadosCtaConvnContingenciaSaidaDTO;
import br.com.bradesco.web.pgit.service.data.pdc.FactoryAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarservrelacservperacional.request.ConsultarServRelacServperacionalRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarservrelacservperacional.response.ConsultarServRelacServperacionalResponse;
import br.com.bradesco.web.pgit.service.data.pdc.incluircontratopgit.request.IncluirContratoPgitRequest;
import br.com.bradesco.web.pgit.service.data.pdc.incluircontratopgit.response.IncluirContratoPgitResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarcontaspossiveisinclusao.request.ListarContasPossiveisInclusaoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarcontaspossiveisinclusao.response.ListarContasPossiveisInclusaoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listardadosctaconvncontingencia.request.ListarDadosCtaConvnContingenciaRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listardadosctaconvncontingencia.request.Ocorrencias;
import br.com.bradesco.web.pgit.service.data.pdc.listardadosctaconvncontingencia.response.ListarDadosCtaConvnContingenciaResponse;
import br.com.bradesco.web.pgit.utils.PgitUtil;

/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Implementa��o do adaptador: IncluirContratoContingencia
 * </p>
 * 
 * @comment C�DIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public class IncluirContratoContingenciaServiceImpl implements IIncluirContratoContingenciaService {

	/** Atributo factoryAdapter. */
	private FactoryAdapter factoryAdapter;

	/**
	 * Get: factoryAdapter.
	 *
	 * @return factoryAdapter
	 */
	public FactoryAdapter getFactoryAdapter() {
		return factoryAdapter;
	}

	/**
	 * Set: factoryAdapter.
	 *
	 * @param factoryAdapter the factory adapter
	 */
	public void setFactoryAdapter(FactoryAdapter factoryAdapter) {
		this.factoryAdapter = factoryAdapter;
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.incluircontratocontingencia.IIncluirContratoContingenciaService#consultarServRelacServperacional(br.com.bradesco.web.pgit.service.business.incluircontratocontingencia.bean.ConsultarServRelacServperacionalEntradaDTO)
	 */
	public List<ConsultarServRelacServperacionalSaidaDTO> consultarServRelacServperacional(ConsultarServRelacServperacionalEntradaDTO entradaDTO) {
		List<ConsultarServRelacServperacionalSaidaDTO> listaRetorno = new ArrayList<ConsultarServRelacServperacionalSaidaDTO>();
		ConsultarServRelacServperacionalRequest request = new ConsultarServRelacServperacionalRequest();
		ConsultarServRelacServperacionalResponse response = new ConsultarServRelacServperacionalResponse();
	
		request.setCdprodutoServicoOperacao(PgitUtil.verificaIntegerNulo(entradaDTO.getCdprodutoServicoOperacao()));
		request.setCdRelacionamentoProdutoProduto(PgitUtil.verificaIntegerNulo(entradaDTO.getCdRelacionamentoProdutoProduto()));
		request.setCdProdutoOperacaoRelacionado(PgitUtil.verificaIntegerNulo(entradaDTO.getCdProdutoOperacaoRelacionado()));
		request.setNumeroOcorrencias(IIncluirContratoContingenciaServiceConstants.NUMERO_OCORRENCIAS_CONSULTA);
	
		response = getFactoryAdapter().getConsultarServRelacServperacionalPDCAdapter().invokeProcess(request);
	
		ConsultarServRelacServperacionalSaidaDTO saidaDTO;
		for (int i = 0; i < response.getOcorrenciasCount(); i++) {
		    saidaDTO = new ConsultarServRelacServperacionalSaidaDTO();
		    saidaDTO.setCodMensagem(response.getCodMensagem());
		    saidaDTO.setMensagem(response.getMensagem());
		    saidaDTO.setCdProdutoOperacaoRelacionado(response.getOcorrencias(i).getCdProdutoOperacaoRelacionado());
		    saidaDTO.setCdProdutoServicoRelacionado(response.getOcorrencias(i).getCdprodutoServicoRelacionado());
		    saidaDTO.setCdRelacionamentoProdutoProduto(response.getOcorrencias(i).getCdRelacionamentoProdutoProduto());
		    saidaDTO.setCdTipoProdutoServico(response.getOcorrencias(i).getCdTipoProdutoServico());
		    saidaDTO.setDsRelacionamentoProdutoProduto(response.getOcorrencias(i).getDsRelacionamentoProdutoProduto());
		    saidaDTO.setDsTipoProdutoServico(response.getOcorrencias(i).getDsTipoProdutoServico());
		    saidaDTO.setNumeroLinhas(response.getNumeroLinhas());
	
		    listaRetorno.add(saidaDTO);
		}
	
		return listaRetorno;
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.incluircontratocontingencia.IIncluirContratoContingenciaService#incluirContratoPgit(br.com.bradesco.web.pgit.service.business.incluircontratocontingencia.bean.IncluirContratoPgitEntradaDTO)
	 */
	public IncluirContratoPgitSaidaDTO incluirContratoPgit(IncluirContratoPgitEntradaDTO entradaDTO) {

		IncluirContratoPgitSaidaDTO saidaDTO = new IncluirContratoPgitSaidaDTO();
		IncluirContratoPgitRequest request = new IncluirContratoPgitRequest();
		IncluirContratoPgitResponse response = new IncluirContratoPgitResponse();
	
		request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(entradaDTO.getNrSequenciaContratoNegocio()));
		request.setCdAcaoRelacionamento(PgitUtil.verificaLongNulo(entradaDTO.getCdAcaoRelacionamento()));
	
		request.setCdAgenciaContrato(PgitUtil.verificaIntegerNulo(entradaDTO.getCdAgenciaContrato()));
		request.setCdContratoNegocioPagamento(PgitUtil.verificaStringNula(entradaDTO.getCdContratoNegocioPagamento()));
		request.setCdFuncionalidadeGerContrato(PgitUtil.verificaLongNulo(entradaDTO.getCdFuncionalidadeGerContrato()));
		request.setCdIdioma(PgitUtil.verificaIntegerNulo(entradaDTO.getCdIdioma()));
		request.setCdIndicadorEconomicoMoeda(PgitUtil.verificaIntegerNulo(entradaDTO.getCdIndicadorEconomicoMoeda()));
		request.setCdPessoa(PgitUtil.verificaLongNulo(entradaDTO.getCdPessoa()));
		request.setCdPessoaJuridica(PgitUtil.verificaLongNulo(entradaDTO.getCdPessoaJuridica()));
		request.setCdPessoaJuridicaNegocio(PgitUtil.verificaLongNulo(entradaDTO.getCdPessoaJuridicaNegocio()));
		request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoContratoNegocio()));
		request.setCdTipoParticipacaoPessoa(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoParticipacaoPessoa()));
		request.setCdTipoResponsavelOrganizacao(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoResponsavelOrganizacao()));
		request.setDsContratoNegocio(PgitUtil.verificaStringNula(entradaDTO.getDsContratoNegocio()));
		request.setFaseContrato(PgitUtil.verificaIntegerNulo(entradaDTO.getFaseContrato()));
		request.setQtServico(PgitUtil.verificaIntegerNulo(entradaDTO.getQtServico()));
		request.setQtContas(PgitUtil.verificaIntegerNulo(entradaDTO.getQtContas()));
		request.setCdSetor(entradaDTO.getCdSetor());
	
		ArrayList<br.com.bradesco.web.pgit.service.data.pdc.incluircontratopgit.request.Ocorrencias1> listaConta = new ArrayList<br.com.bradesco.web.pgit.service.data.pdc.incluircontratopgit.request.Ocorrencias1>();
		// V�rias Ocorr�ncias para Inclus�o
		for (int i = 0; i < entradaDTO.getOcorrenciasConta().size(); i++) {
		    br.com.bradesco.web.pgit.service.data.pdc.incluircontratopgit.request.Ocorrencias1 ocorrencia1 = new br.com.bradesco.web.pgit.service.data.pdc.incluircontratopgit.request.Ocorrencias1();
		    ocorrencia1.setCdBanco(PgitUtil.verificaIntegerNulo(entradaDTO.getOcorrenciasConta().get(i).getCdBanco()));
		    ocorrencia1.setCdAgencia(PgitUtil.verificaIntegerNulo(entradaDTO.getOcorrenciasConta().get(i).getCdAgencia()));
		    ocorrencia1.setCdDigitoAgencia(PgitUtil.verificaIntegerNulo(entradaDTO.getOcorrenciasConta().get(i).getCdDigitoAgencia()));
		    ocorrencia1.setCdConta(PgitUtil.verificaLongNulo(entradaDTO.getOcorrenciasConta().get(i).getCdConta()));
		    ocorrencia1.setCdDigitoConta(PgitUtil.verificaStringNula(entradaDTO.getOcorrenciasConta().get(i).getCdDigitoConta()));
		    ocorrencia1.setCdTipoConta(PgitUtil.verificaIntegerNulo(entradaDTO.getOcorrenciasConta().get(i).getCdTipoConta()));
		    listaConta.add(ocorrencia1);
		}
		request.setOcorrencias1((br.com.bradesco.web.pgit.service.data.pdc.incluircontratopgit.request.Ocorrencias1[]) listaConta
			.toArray(new br.com.bradesco.web.pgit.service.data.pdc.incluircontratopgit.request.Ocorrencias1[0]));
	
		ArrayList<br.com.bradesco.web.pgit.service.data.pdc.incluircontratopgit.request.Ocorrencias> lista = new ArrayList<br.com.bradesco.web.pgit.service.data.pdc.incluircontratopgit.request.Ocorrencias>();
		// V�rias Ocorr�ncias para Inclus�o
		for (int i = 0; i < entradaDTO.getOcorrencias().size(); i++) {
		    br.com.bradesco.web.pgit.service.data.pdc.incluircontratopgit.request.Ocorrencias ocorrencia = new br.com.bradesco.web.pgit.service.data.pdc.incluircontratopgit.request.Ocorrencias();
		    ocorrencia.setCdProduto(PgitUtil.verificaIntegerNulo(entradaDTO.getOcorrencias().get(i).getCdProduto()));
		    lista.add(ocorrencia);
		}
		request.setOcorrencias((br.com.bradesco.web.pgit.service.data.pdc.incluircontratopgit.request.Ocorrencias[]) lista
			.toArray(new br.com.bradesco.web.pgit.service.data.pdc.incluircontratopgit.request.Ocorrencias[0]));
		
		//convenio
		request.setCdCpfCnpjConve(PgitUtil.verificaLongNulo(entradaDTO.getCdCpfCnpjConve()));

		request.setCdFilialCnpjConve(PgitUtil.verificaIntegerNulo(entradaDTO.getCdFilialCnpjConve()));

		request.setCdControleCnpjConve(PgitUtil.verificaIntegerNulo(entradaDTO.getCdControleCnpjConve()));

		String dsConvenioCta = entradaDTO.getDsConveCtaSalrl();
		if (dsConvenioCta != null && dsConvenioCta.length() > 40) {
		    dsConvenioCta = dsConvenioCta.substring(0, 40);
		}
		request.setDsConveCtaSalrl(PgitUtil.verificaStringNula(dsConvenioCta));

		request.setCdBcoEmprConvn(PgitUtil.verificaIntegerNulo(entradaDTO.getCdBcoEmprConvn()));

		request.setCdAgEmprConvn(PgitUtil.verificaIntegerNulo(entradaDTO.getCdAgEmprConvn()));

		request.setCdCtaEmpresaConvn(PgitUtil.verificaLongNulo(entradaDTO.getCdCtaEmpresaConvn()));

		request.setCdDigEmpresaConvn(PgitUtil.verificaStringNula(entradaDTO.getCdDigEmpresaConvn()));

		request.setCdConveCtaSalarial(PgitUtil.verificaLongNulo(entradaDTO.getCdConveCtaSalarial()));
		
		request.setCdIndicadorConvnNovo(PgitUtil.verificaIntegerNulo(entradaDTO.getCdIndicadorConvnNovo()));
	
		response = getFactoryAdapter().getIncluirContratoPgitPDCAdapter().invokeProcess(request);
	
		/*
	         * Regra conforme e-mail do Rodrigo Fernando Mafioletti Staejak - 01/02/2011 O controle ser� feito pelo c�digo
	         * de mensagem da seguinte forma. Enquanto o c�digo de mensagem for igual PGIT0009 voc� deve somar um no campo
	         * �Fase� e startar o Fluxo �PGICIAKI� novamente Caso c�digo diferente de PGIT0009, encerrar chamada recursiva.
	         */
		while (response.getCodMensagem() != null && response.getCodMensagem().equals("PGIT0009")) {
		    request.setFaseContrato(response.getCdFaseSaida());
		    request.setNrSequenciaContratoNegocio(response.getNrSequenciaContratoNegocio());
		    response = getFactoryAdapter().getIncluirContratoPgitPDCAdapter().invokeProcess(request);
		}

		saidaDTO.setCodMensagem(response.getCodMensagem());
		saidaDTO.setMensagem(response.getMensagem());

		return saidaDTO;
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.incluircontratocontingencia.IIncluirContratoContingenciaService#consultarListaContasInclusao(br.com.bradesco.web.pgit.service.business.incluircontratocontingencia.bean.ConsultarListaContasInclusaoEntradaDTO)
	 */
	public List<ConsultarListaContasInclusaoSaidaDTO> consultarListaContasInclusao(ConsultarListaContasInclusaoEntradaDTO entradaDTO) {
		ListarContasPossiveisInclusaoRequest request = new ListarContasPossiveisInclusaoRequest();
		ListarContasPossiveisInclusaoResponse response = new ListarContasPossiveisInclusaoResponse();

		request.setCdAgencia(PgitUtil.verificaIntegerNulo(entradaDTO.getCdAgencia()));
		request.setCdBanco(PgitUtil.verificaIntegerNulo(entradaDTO.getCdBanco()));
		request.setCdConta(PgitUtil.verificaLongNulo(entradaDTO.getCdConta()));
		request.setCdControleCpfCnpj(PgitUtil.verificaIntegerNulo(entradaDTO.getCdControleCpfCnpj()));
		request.setCdCorpoCpfCnpj(PgitUtil.verificaLongNulo(entradaDTO.getCdCpfCnpj()));
		request.setCdDigitoAgencia(PgitUtil.verificaIntegerNulo(entradaDTO.getCdDigitoAgencia()));
		request.setCdDigitoConta("".equals(PgitUtil.verificaStringNula(entradaDTO.getCdDigitoConta())) ? 0 : Integer.parseInt(PgitUtil.verificaStringNula(entradaDTO.getCdDigitoConta())));
		request.setCdFilialCpfCnpj(PgitUtil.verificaIntegerNulo(entradaDTO.getCdFilialCpfCnpj()));
		request.setCdPessoaJuridicaContrato(PgitUtil.verificaLongNulo(entradaDTO.getCdPessoaJuridicaContrato()));
		request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoContratoNegocio()));
		request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(entradaDTO.getNrSequenciaContratoNegocio()));
		request.setDsTipoConta(PgitUtil.verificaIntegerNulo(entradaDTO.getDsTipoConta()));
		request.setNumeroOcorrencias(IIncluirContratoContingenciaServiceConstants.NUMERO_OCORRENCIAS_CONSULTA_CONTA);
	
		response = getFactoryAdapter().getListarContasPossiveisInclusaoPDCAdapter().invokeProcess(request);
	
		List<ConsultarListaContasInclusaoSaidaDTO> listaRetorno = new ArrayList<ConsultarListaContasInclusaoSaidaDTO>();
		ConsultarListaContasInclusaoSaidaDTO saidaDTO;
		for (int i = 0; i < response.getOcorrenciasCount(); i++) {
		    saidaDTO = new ConsultarListaContasInclusaoSaidaDTO();
	
		    saidaDTO.setCdAgencia(response.getOcorrencias(i).getCdAgencia());
		    saidaDTO.setCdBanco(response.getOcorrencias(i).getCdBanco());
		    saidaDTO.setCdConta(response.getOcorrencias(i).getCdConta());
		    saidaDTO.setCdCpfCnpj(response.getOcorrencias(i).getCdCorpoCpfCnpj());
		    saidaDTO.setCdDigitoConta(response.getOcorrencias(i).getCdDigitoConta());
		    saidaDTO.setCdPessoaJuridicaVinculo(response.getOcorrencias(i).getCdPessoVinc());
		    saidaDTO.setCdTipoConta(response.getOcorrencias(i).getCdTipoConta());
		    saidaDTO.setCdTipoContratoVinculo(response.getOcorrencias(i).getCdTipoContratoVinc());
		    saidaDTO.setDsAgencia(response.getOcorrencias(i).getDsAgencia());
		    saidaDTO.setDsBanco(response.getOcorrencias(i).getDsBanco());
		    saidaDTO.setNmParticipante(response.getOcorrencias(i).getNmParticipante());
		    saidaDTO.setNrSequenciaContratoVinculo(response.getOcorrencias(i).getNrSeqContratoVinc());
		    saidaDTO.setCdDigitoAgencia(response.getOcorrencias(i).getCdDigitoAgencia());
		    saidaDTO.setDsTipoConta(response.getOcorrencias(i).getDsTipoConta());
		    saidaDTO.setCdPessoa(response.getOcorrencias(i).getCdPessoa());

		    // Formatacao
		    saidaDTO.setCpfCnpjFormatado(saidaDTO.getCdCpfCnpj());
		    /*
	                 * O Cpf e Cnpj j� esta vindo formatado do PDC. String aux; aux =
	                 * SiteUtil.formatNumber(saidaDTO.getCdCpfCnpj(), 15); if(!aux.equals("000000000000000")){
	                 * if(aux.substring(9, 13).equals("0000")){ saidaDTO.setCpfCnpjFormatado(CpfCnpjUtils.formatCpfCnpj(aux,
	                 * 1)); }else{ saidaDTO.setCpfCnpjFormatado(CpfCnpjUtils.formatCpfCnpj(aux, 2)); } }else{
	                 * saidaDTO.setCpfCnpjFormatado(""); }
	                 */
	
		    saidaDTO.setBancoFormatado(PgitUtil.concatenarCampos(saidaDTO.getCdBanco(), saidaDTO.getDsBanco()));
		    saidaDTO.setAgenciaFormatada(PgitUtil.concatenarCampos(saidaDTO.getCdAgencia(), saidaDTO.getDsAgencia()));
		    saidaDTO.setContaFormatada(PgitUtil.concatenarCampos(saidaDTO.getCdConta(), saidaDTO.getCdDigitoConta()));
	
		    listaRetorno.add(saidaDTO);
		}
		
		return listaRetorno;
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.incluircontratocontingencia.IIncluirContratoContingenciaService#listarDadosCtaConvnContingencia(br.com.bradesco.web.pgit.service.business.incluircontratocontingencia.bean.ListarDadosCtaConvnContingenciaEntradaDTO)
	 */
	public ListarDadosCtaConvnContingenciaSaidaDTO listarDadosCtaConvnContingencia(ListarDadosCtaConvnContingenciaEntradaDTO entradaDTO) {
		List<ListarDadosCtaConvnContingenciaOcorrenciasSaidaDTO> listaRetorno = new ArrayList<ListarDadosCtaConvnContingenciaOcorrenciasSaidaDTO>();
		ListarDadosCtaConvnContingenciaRequest request = new ListarDadosCtaConvnContingenciaRequest();
		ListarDadosCtaConvnContingenciaResponse response = new ListarDadosCtaConvnContingenciaResponse();

		request.setCdCpfCnpjRepresentante(PgitUtil.verificaLongNulo(entradaDTO.getCdCpfCnpjRepresentante()));
		request.setCdControleCnpjRepresentante(PgitUtil.verificaIntegerNulo(entradaDTO.getCdControleCnpjRepresentante()));
		request.setCdFilialCnpjRepresentante(PgitUtil.verificaIntegerNulo(entradaDTO.getCdFilialCnpjRepresentante()));
		request.setNrOcorrencias(entradaDTO.getOcorrencias().size());
		ListarDadosCtaConvnContingenciaSaidaDTO saida = new ListarDadosCtaConvnContingenciaSaidaDTO();

		for (int i = 0; i < entradaDTO.getOcorrencias().size(); i++) {
			Ocorrencias ocurr = new Ocorrencias();
			ocurr.setCdAgencia(entradaDTO.getOcorrencias().get(i).getCdAgencia());
			ocurr.setCdBanco(entradaDTO.getOcorrencias().get(i).getCdBanco());
			ocurr.setCdConta(entradaDTO.getOcorrencias().get(i).getCdConta());
			ocurr.setCdDigitoAgencia(entradaDTO.getOcorrencias().get(i).getCdDigitoAgencia());
			ocurr.setCdDigitoConta(entradaDTO.getOcorrencias().get(i).getCdDigitoConta());

			request.addOcorrencias(ocurr);
		}

		response = getFactoryAdapter().getListarDadosCtaConvnContingenciaPDCAdapter().invokeProcess(request);
	
		ListarDadosCtaConvnContingenciaOcorrenciasSaidaDTO ocorrenciaSaidaDTO;
		List<ListarDadosCtaConvnContingenciaOcorrenciasSaidaDTO> lista = new ArrayList<ListarDadosCtaConvnContingenciaOcorrenciasSaidaDTO>();
		saida.setCodMensagem(response.getCodMensagem());
		saida.setMensagem(response.getMensagem());
		
		for (int i = 0; i < response.getOcorrenciasCount(); i++) {
		    ocorrenciaSaidaDTO = new ListarDadosCtaConvnContingenciaOcorrenciasSaidaDTO();
		    ocorrenciaSaidaDTO.setCdAgeEmpresaConvn(response.getOcorrencias(i).getCdAgeEmpresaConvn());
		    ocorrenciaSaidaDTO.setCdBancoEmpresaConvn(response.getOcorrencias(i).getCdBancoEmpresaConvn());
		    ocorrenciaSaidaDTO.setCdConveCtaSalarial(response.getOcorrencias(i).getCdConveCtaSalarial());
		    ocorrenciaSaidaDTO.setCdConveNovo(response.getOcorrencias(i).getCdConveNovo());
		    ocorrenciaSaidaDTO.setCdCpfCnpjConve(response.getOcorrencias(i).getCdCpfCnpjConve());
		    ocorrenciaSaidaDTO.setCdCtaEmpresaConvn(response.getOcorrencias(i).getCdCtaEmpresaConvn());
		    ocorrenciaSaidaDTO.setCdCtrlCnpjEmp(response.getOcorrencias(i).getCdCtrlCnpjEmp());
		    ocorrenciaSaidaDTO.setCdDigEmpresaConvn(response.getOcorrencias(i).getCdDigEmpresaConvn());
		    ocorrenciaSaidaDTO.setCdDigitoAgencia(response.getOcorrencias(i).getCdDigitoAgencia());
		    ocorrenciaSaidaDTO.setCdFilialCnpjConve(response.getOcorrencias(i).getCdFilialCnpjConve());
		    ocorrenciaSaidaDTO.setDsConveCtaSalrl(response.getOcorrencias(i).getDsConveCtaSalrl());
    		ocorrenciaSaidaDTO.setDsSitConve(response.getOcorrencias(i).getDsSitConve());

    		lista.add(ocorrenciaSaidaDTO);
		}
		saida.setOcorrencias(lista);

		return saida;
	}
}