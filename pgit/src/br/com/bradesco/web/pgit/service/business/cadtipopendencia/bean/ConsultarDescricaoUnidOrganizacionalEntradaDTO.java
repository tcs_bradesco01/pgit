/*
 * Nome: br.com.bradesco.web.pgit.service.business.cadtipopendencia.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.cadtipopendencia.bean;


/**
 * Nome: ConsultarDescricaoUnidOrganizacionalEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarDescricaoUnidOrganizacionalEntradaDTO {
	
	/** Atributo cdPessoaJuridica. */
	private Long cdPessoaJuridica;
    
    /** Atributo nrSequenciaUnidadeOrganizacional. */
    private Integer nrSequenciaUnidadeOrganizacional;
    
    /** Atributo cdTipoUnidadeOrganizacional. */
    private Integer cdTipoUnidadeOrganizacional;
    
	/**
	 * Get: cdPessoaJuridica.
	 *
	 * @return cdPessoaJuridica
	 */
	public Long getCdPessoaJuridica() {
		return cdPessoaJuridica;
	}
	
	/**
	 * Set: cdPessoaJuridica.
	 *
	 * @param cdPessoaJuridica the cd pessoa juridica
	 */
	public void setCdPessoaJuridica(Long cdPessoaJuridica) {
		this.cdPessoaJuridica = cdPessoaJuridica;
	}
	
	/**
	 * Get: cdTipoUnidadeOrganizacional.
	 *
	 * @return cdTipoUnidadeOrganizacional
	 */
	public Integer getCdTipoUnidadeOrganizacional() {
		return cdTipoUnidadeOrganizacional;
	}
	
	/**
	 * Set: cdTipoUnidadeOrganizacional.
	 *
	 * @param cdTipoUnidadeOrganizacional the cd tipo unidade organizacional
	 */
	public void setCdTipoUnidadeOrganizacional(Integer cdTipoUnidadeOrganizacional) {
		this.cdTipoUnidadeOrganizacional = cdTipoUnidadeOrganizacional;
	}
	
	/**
	 * Get: nrSequenciaUnidadeOrganizacional.
	 *
	 * @return nrSequenciaUnidadeOrganizacional
	 */
	public Integer getNrSequenciaUnidadeOrganizacional() {
		return nrSequenciaUnidadeOrganizacional;
	}
	
	/**
	 * Set: nrSequenciaUnidadeOrganizacional.
	 *
	 * @param nrSequenciaUnidadeOrganizacional the nr sequencia unidade organizacional
	 */
	public void setNrSequenciaUnidadeOrganizacional(
			Integer nrSequenciaUnidadeOrganizacional) {
		this.nrSequenciaUnidadeOrganizacional = nrSequenciaUnidadeOrganizacional;
	}
	
	
}
