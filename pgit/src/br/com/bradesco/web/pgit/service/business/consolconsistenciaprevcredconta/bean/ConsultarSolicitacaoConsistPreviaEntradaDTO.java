/*
 * Nome: br.com.bradesco.web.pgit.service.business.consolconsistenciaprevcredconta.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.consolconsistenciaprevcredconta.bean;

/**
 * Nome: ConsultarSolicitacaoConsistPreviaEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarSolicitacaoConsistPreviaEntradaDTO {
    
    /** Atributo nrOcorrencias. */
    private Integer nrOcorrencias;
    
    /** Atributo cdPessoaJuridicaContrato. */
    private Long cdPessoaJuridicaContrato;
	
	/** Atributo cdTipoContratoNegocio. */
	private Integer cdTipoContratoNegocio;
    
    /** Atributo nrSequenciaContratoNegocio. */
    private Long nrSequenciaContratoNegocio;
    
    /** Atributo cdBancoDebito. */
    private Integer cdBancoDebito;
    
    /** Atributo cdAgenciaDebito. */
    private Integer cdAgenciaDebito;
    
    /** Atributo cdContaDebito. */
    private Long cdContaDebito;
    
    /** Atributo digitoContaDebito. */
    private String digitoContaDebito;
    
    /** Atributo dtCreditoPagamentoInicial. */
    private String dtCreditoPagamentoInicial;
    
    /** Atributo dtCreditoPagamentoFinal. */
    private String dtCreditoPagamentoFinal;
    
    /** Atributo cdSituacaoLotePagamento. */
    private Integer cdSituacaoLotePagamento;
    
	/**
	 * Get: cdAgenciaDebito.
	 *
	 * @return cdAgenciaDebito
	 */
	public Integer getCdAgenciaDebito() {
		return cdAgenciaDebito;
	}
	
	/**
	 * Set: cdAgenciaDebito.
	 *
	 * @param cdAgenciaDebito the cd agencia debito
	 */
	public void setCdAgenciaDebito(Integer cdAgenciaDebito) {
		this.cdAgenciaDebito = cdAgenciaDebito;
	}
	
	/**
	 * Get: cdBancoDebito.
	 *
	 * @return cdBancoDebito
	 */
	public Integer getCdBancoDebito() {
		return cdBancoDebito;
	}
	
	/**
	 * Set: cdBancoDebito.
	 *
	 * @param cdBancoDebito the cd banco debito
	 */
	public void setCdBancoDebito(Integer cdBancoDebito) {
		this.cdBancoDebito = cdBancoDebito;
	}
	
	/**
	 * Get: cdContaDebito.
	 *
	 * @return cdContaDebito
	 */
	public Long getCdContaDebito() {
		return cdContaDebito;
	}
	
	/**
	 * Set: cdContaDebito.
	 *
	 * @param cdContaDebito the cd conta debito
	 */
	public void setCdContaDebito(Long cdContaDebito) {
		this.cdContaDebito = cdContaDebito;
	}
	
	/**
	 * Get: cdPessoaJuridicaContrato.
	 *
	 * @return cdPessoaJuridicaContrato
	 */
	public Long getCdPessoaJuridicaContrato() {
		return cdPessoaJuridicaContrato;
	}
	
	/**
	 * Set: cdPessoaJuridicaContrato.
	 *
	 * @param cdPessoaJuridicaContrato the cd pessoa juridica contrato
	 */
	public void setCdPessoaJuridicaContrato(Long cdPessoaJuridicaContrato) {
		this.cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
	}
	
	/**
	 * Get: cdSituacaoLotePagamento.
	 *
	 * @return cdSituacaoLotePagamento
	 */
	public Integer getCdSituacaoLotePagamento() {
		return cdSituacaoLotePagamento;
	}
	
	/**
	 * Set: cdSituacaoLotePagamento.
	 *
	 * @param cdSituacaoLotePagamento the cd situacao lote pagamento
	 */
	public void setCdSituacaoLotePagamento(Integer cdSituacaoLotePagamento) {
		this.cdSituacaoLotePagamento = cdSituacaoLotePagamento;
	}
	
	/**
	 * Get: cdTipoContratoNegocio.
	 *
	 * @return cdTipoContratoNegocio
	 */
	public Integer getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}
	
	/**
	 * Set: cdTipoContratoNegocio.
	 *
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}
	
	
	/**
	 * Get: digitoContaDebito.
	 *
	 * @return digitoContaDebito
	 */
	public String getDigitoContaDebito() {
		return digitoContaDebito;
	}
	
	/**
	 * Set: digitoContaDebito.
	 *
	 * @param digitoContaDebito the digito conta debito
	 */
	public void setDigitoContaDebito(String digitoContaDebito) {
		this.digitoContaDebito = digitoContaDebito;
	}
	
	/**
	 * Get: dtCreditoPagamentoFinal.
	 *
	 * @return dtCreditoPagamentoFinal
	 */
	public String getDtCreditoPagamentoFinal() {
		return dtCreditoPagamentoFinal;
	}
	
	/**
	 * Set: dtCreditoPagamentoFinal.
	 *
	 * @param dtCreditoPagamentoFinal the dt credito pagamento final
	 */
	public void setDtCreditoPagamentoFinal(String dtCreditoPagamentoFinal) {
		this.dtCreditoPagamentoFinal = dtCreditoPagamentoFinal;
	}
	
	/**
	 * Get: dtCreditoPagamentoInicial.
	 *
	 * @return dtCreditoPagamentoInicial
	 */
	public String getDtCreditoPagamentoInicial() {
		return dtCreditoPagamentoInicial;
	}
	
	/**
	 * Set: dtCreditoPagamentoInicial.
	 *
	 * @param dtCreditoPagamentoInicial the dt credito pagamento inicial
	 */
	public void setDtCreditoPagamentoInicial(String dtCreditoPagamentoInicial) {
		this.dtCreditoPagamentoInicial = dtCreditoPagamentoInicial;
	}
	
	/**
	 * Get: nrOcorrencias.
	 *
	 * @return nrOcorrencias
	 */
	public Integer getNrOcorrencias() {
		return nrOcorrencias;
	}
	
	/**
	 * Set: nrOcorrencias.
	 *
	 * @param nrOcorrencias the nr ocorrencias
	 */
	public void setNrOcorrencias(Integer nrOcorrencias) {
		this.nrOcorrencias = nrOcorrencias;
	}
	
	/**
	 * Get: nrSequenciaContratoNegocio.
	 *
	 * @return nrSequenciaContratoNegocio
	 */
	public Long getNrSequenciaContratoNegocio() {
		return nrSequenciaContratoNegocio;
	}
	
	/**
	 * Set: nrSequenciaContratoNegocio.
	 *
	 * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
	 */
	public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}
}
