/*
 * Nome: br.com.bradesco.web.pgit.service.business.filtroidentificao.impl
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */

package br.com.bradesco.web.pgit.service.business.filtroidentificao.impl;

import java.util.ArrayList;
import java.util.List;

import br.com.bradesco.web.pgit.service.business.filtroidentificao.IFiltroIdentificaoService;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.IFiltroIdentificaoServiceConstants;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasEntradaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaContratosPessoasEntradaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaContratosPessoasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaFuncionarioBradescoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaFuncionarioBradescoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ListarClubesClientesSaidaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ListarContratosPgitEntradaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ListarContratosPgitSaidaDTO;
import br.com.bradesco.web.pgit.service.data.pdc.FactoryAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarlistaclientepessoas.
    request.ConsultarListaClientePessoasRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarlistaclientepessoas.
    response.ConsultarListaClientePessoasResponse;
import br.com.bradesco.web.pgit.service.data.pdc.consultarlistaclientepessoas.response.Ocorrencias;
import br.com.bradesco.web.pgit.service.data.pdc.consultarlistacontratospessoas.
    request.ConsultarListaContratosPessoasRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarlistacontratospessoas.
    response.ConsultarListaContratosPessoasResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarclubesclientes.request.ListarClubesClientesRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarclubesclientes.response.ListarClubesClientesResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarcontratospgit.request.ListarContratosPgitRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarcontratospgit.response.ListarContratosPgitResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarcontratosrenegociacao.
    request.ListarContratosRenegociacaoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarcontratosrenegociacao.
    response.ListarContratosRenegociacaoResponse;
import br.com.bradesco.web.pgit.utils.CpfCnpjUtils;
import br.com.bradesco.web.pgit.utils.PgitUtil;

/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Implementa��o do adaptador: Filtroidentificao
 * </p>
 * 
 * @comment C�DIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 * @see IFiltroIdentificaoService
 */
public class FiltroIdentificaoServiceImpl implements IFiltroIdentificaoService {

    /** Atributo factoryAdapter. */
    private FactoryAdapter factoryAdapter;

    /**
     * Filtro identificao service impl.
     */
    public FiltroIdentificaoServiceImpl() {

    }

    /**
     * (non-Javadoc)
     * 
     * @see br.com.bradesco.web.pgit.service.business.filtroidentificao.IFiltroIdentificaoService#pesquisarClientes(br.com.
     * bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasEntradaDTO)
     */
    public List<ConsultarListaClientePessoasSaidaDTO> pesquisarClientes(ConsultarListaClientePessoasEntradaDTO entrada) {

        ConsultarListaClientePessoasRequest request = new ConsultarListaClientePessoasRequest();

        request.setCdAgenciaBancaria(PgitUtil.verificaIntegerNulo(entrada.getCdAgenciaBancaria()));
        request.setCdBanco(PgitUtil.verificaIntegerNulo(entrada.getCdBanco()));
        request.setCdContaBancaria(PgitUtil.verificaLongNulo(entrada.getCdContaBancaria()));
        request.setCdDigitoConta(PgitUtil.verificaStringNula(entrada.getCdDigitoConta()));
        request.setCdTipoConta(PgitUtil.verificaIntegerNulo(entrada.getCdTipoConta()));
        request.setDsNomeRazao(PgitUtil.verificaStringNula(entrada.getDsNomeRazaoSocial() == null ? "" : 
        	entrada.getDsNomeRazaoSocial().toUpperCase()));
        if (entrada.isCnpjECpfDesabilitados()) {
            request.setCdControleCnpj(0);
            request.setCdCpfCnpj(0);
            request.setCdFilialCnpj(0);
        } else {
            if (entrada.isCnpjOuCpf()) {
                request.setCdControleCnpj(PgitUtil.verificaIntegerNulo(entrada.getCdControleCpf()));
                request.setCdCpfCnpj(PgitUtil.verificaLongNulo(entrada.getCdCpf()));
                request.setCdFilialCnpj(0);

            } else {
                request.setCdControleCnpj(PgitUtil.verificaIntegerNulo(entrada.getCdControleCnpj()));
                request.setCdCpfCnpj(PgitUtil.verificaLongNulo(entrada.getCdCpfCnpj()));
                request.setCdFilialCnpj(PgitUtil.verificaIntegerNulo(entrada.getCdFilialCnpj()));

            }
        }

        request.setDtNascimentoFundacao(PgitUtil.verificaStringNula(entrada.getDtNascimentoFundacao()));
        request.setNumeroOcorrencias(50);

        ConsultarListaClientePessoasResponse response =
            getFactoryAdapter().getConsultarListaClientePessoasPDCAdapter().invokeProcess(request);

        List<ConsultarListaClientePessoasSaidaDTO> list = new ArrayList<ConsultarListaClientePessoasSaidaDTO>();

        for (Ocorrencias saida : response.getOcorrencias()) {
            list.add(new ConsultarListaClientePessoasSaidaDTO(saida));
        }

        return list;
    }

    /**
     * (non-Javadoc)
     * 
     * @see br.com.bradesco.web.pgit.service.business.filtroidentificao.IFiltroIdentificaoService#listarClubesClientes(
     * br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasEntradaDTO)
     */
    public List<ListarClubesClientesSaidaDTO> listarClubesClientes(ConsultarListaClientePessoasEntradaDTO entrada) {
        ListarClubesClientesRequest request = new ListarClubesClientesRequest();

        request.setCdAgencia(entrada.getCdAgenciaBancaria() == null ? 0 : entrada.getCdAgenciaBancaria());
        request.setCdBanco(entrada.getCdBanco() == null ? 0 : entrada.getCdBanco());
        request.setCdConta(entrada.getCdContaBancaria() == null ? 0 : entrada.getCdContaBancaria());
        request.setCdDigitoConta(PgitUtil.verificaStringNula(entrada.getCdDigitoConta()));
        request.setCdTipoConta(entrada.getCdTipoConta() == null ? 0 : entrada.getCdTipoConta());
        request.setDsNomeRazao(entrada.getDsNomeRazaoSocial() == null ? "" : entrada.getDsNomeRazaoSocial()
            .toUpperCase());

        if (entrada.isCnpjECpfDesabilitados()) {
            request.setCdControleCpfCnpj(0);
            request.setCdCorpoCpfCnpj(0);
            request.setCdFilialCnpj(0);
        } else {
            if (entrada.isCnpjOuCpf()) {
                request.setCdControleCpfCnpj(entrada.getCdControleCpf() == null ? 0 : entrada.getCdControleCpf());
                request.setCdCorpoCpfCnpj(entrada.getCdCpf() == null ? 0 : entrada.getCdCpf());
                request.setCdFilialCnpj(0);

            } else {
                request.setCdControleCpfCnpj(entrada.getCdControleCnpj() == null ? 0 : entrada.getCdControleCnpj());
                request.setCdCorpoCpfCnpj(entrada.getCdCpfCnpj() == null ? 0 : entrada.getCdCpfCnpj());
                request.setCdFilialCnpj(entrada.getCdFilialCnpj() == null ? 0 : entrada.getCdFilialCnpj());

            }
        }

        request.setDtNascimentoFundacao(entrada.getDtNascimentoFundacao() == null ? "" : entrada
            .getDtNascimentoFundacao());
        request.setNrOcorrencias(50);

        ListarClubesClientesResponse response =
            getFactoryAdapter().getListarClubesClientesPDCAdapter().invokeProcess(request);

        List<ListarClubesClientesSaidaDTO> list = new ArrayList<ListarClubesClientesSaidaDTO>();

        for (br.com.bradesco.web.pgit.service.data.pdc.listarclubesclientes.response.Ocorrencias saida : response
            .getOcorrencias()) {
            list.add(new ListarClubesClientesSaidaDTO(saida));
        }

        return list;
    }

    /**
     * (non-Javadoc)
     * 
     * @see br.com.bradesco.web.pgit.service.business.filtroidentificao.IFiltroIdentificaoService#pesquisarContratos(
     * br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaContratosPessoasEntradaDTO)
     */
    public List<ConsultarListaContratosPessoasSaidaDTO> pesquisarContratos(
        ConsultarListaContratosPessoasEntradaDTO entrada) {

        ConsultarListaContratosPessoasRequest request = new ConsultarListaContratosPessoasRequest();

        request.setCdClub(entrada.getCdClub() == null ? 0 : entrada.getCdClub());
        request.setCdPessoaJuridicaContrato(entrada.getCdPessoaJuridicaContrato() == null ? 0 : entrada
            .getCdPessoaJuridicaContrato());
        request.setCdTipoContratoNegocio(entrada.getCdTipoContratoNegocio() == null ? 0 : entrada
            .getCdTipoContratoNegocio());
        request.setNrSeqContratoNegocio(entrada.getNrSeqContratoNegocio() == null ? 0L : entrada
            .getNrSeqContratoNegocio());
        request.setCdControleCpfPssoa(PgitUtil.verificaIntegerNulo(entrada.getCdControleCpfPssoa()));
        request.setCdCpfCnpjPssoa(PgitUtil.verificaLongNulo(entrada.getCdCpfCnpjPssoa()));
        request.setCdFilialCnpjPssoa(PgitUtil.verificaIntegerNulo(entrada.getCdFilialCnpjPssoa()));

        request.setNrOcorrencias(50);

        ConsultarListaContratosPessoasResponse response =
            getFactoryAdapter().getConsultarListaContratosPessoasPDCAdapter().invokeProcess(request);

        List<ConsultarListaContratosPessoasSaidaDTO> list = new ArrayList<ConsultarListaContratosPessoasSaidaDTO>();

        ConsultarListaContratosPessoasSaidaDTO registro;
        for (int i = 0; i < response.getOcorrenciasCount(); i++) {
            registro = new ConsultarListaContratosPessoasSaidaDTO();
            registro.setCdPessoaJuridicaContrato(response.getOcorrencias(i).getCdPessoaJuridicaContrato());
            registro.setCdSituacaoContratoNegocio(response.getOcorrencias(i).getCdSituacaoContratoNegocio());
            registro.setCdTipoContratoNegocio(response.getOcorrencias(i).getCdTipoContratoNegocio());
            registro.setCdTipoParticipacaoPessoa(response.getOcorrencias(i).getCdTipoParticipacaoPessoa());
            registro.setCodMensagem(response.getCodMensagem());
            registro.setDsContrato(response.getOcorrencias(i).getDsContrato());
            registro.setDsPessoaJuridicaContrato(response.getOcorrencias(i).getDsPessoaJuridicaContrato());
            registro.setDsSituacaoContratoNegocio(response.getOcorrencias(i).getDsSituacaoContratoNegocio());
            registro.setDsTipoContratoNegocio(response.getOcorrencias(i).getDsTipoContratoNegocio());
            registro.setDsTipoParticipacaoPessoa(response.getOcorrencias(i).getDsTipoParticipacaoPessoa());
            registro.setMensagem(response.getMensagem());
            registro.setNrSeqContratoNegocio(response.getOcorrencias(i).getNrSeqContratoNegocio());
            registro.setCdControleCpfCnpjParticipante(response.getOcorrencias(i).getCdControleCpfCnpjParticipante());
            registro.setCdCpfCnpjParticipante(response.getOcorrencias(i).getCdCpfCnpjParticipante());
            registro.setCdFilialCnpjParticipante(response.getOcorrencias(i).getCdFilialCnpjParticipante());
            registro.setCdPessoaParticipante(response.getOcorrencias(i).getCdPessoaParticipante());
            registro.setDsPessoaParticipante(response.getOcorrencias(i).getDsPessoaParticipante());

            registro.setCnpjOuCpfFormatado(CpfCnpjUtils.formatCpfCnpjCompleto(registro.getCdCpfCnpjParticipante(),
                registro.getCdFilialCnpjParticipante(), registro.getCdControleCpfCnpjParticipante()));

            list.add(registro);

        }

        return list;
    }

    /**
     * (non-Javadoc)
     * 
     * @see br.com.bradesco.web.pgit.service.business.filtroidentificao.IFiltroIdentificaoService#
     * detalharContratoOrigem(br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaContratosPessoasEntradaDTO)
     */
    public ConsultarListaContratosPessoasSaidaDTO detalharContratoOrigem(
        ConsultarListaContratosPessoasEntradaDTO entrada) {

        ConsultarListaContratosPessoasRequest request = new ConsultarListaContratosPessoasRequest();

        request.setCdClub(entrada.getCdClub() == null ? 0 : entrada.getCdClub());
        request.setCdPessoaJuridicaContrato(entrada.getCdPessoaJuridicaContrato() == null ? 0 : entrada
            .getCdPessoaJuridicaContrato());
        request.setCdTipoContratoNegocio(entrada.getCdTipoContratoNegocio() == null ? 0 : entrada
            .getCdTipoContratoNegocio());
        request.setNrSeqContratoNegocio(entrada.getNrSeqContratoNegocio() == null ? 0L : entrada
            .getNrSeqContratoNegocio());
        request.setNrOcorrencias(50);
        request.setCdControleCpfPssoa(PgitUtil.verificaIntegerNulo(entrada.getCdControleCpfPssoa()));
        request.setCdCpfCnpjPssoa(PgitUtil.verificaLongNulo(entrada.getCdCpfCnpjPssoa()));
        request.setCdFilialCnpjPssoa(PgitUtil.verificaIntegerNulo(entrada.getCdFilialCnpjPssoa()));
        ConsultarListaContratosPessoasResponse response =
            getFactoryAdapter().getConsultarListaContratosPessoasPDCAdapter().invokeProcess(request);

        ConsultarListaContratosPessoasSaidaDTO saidaDTO = new ConsultarListaContratosPessoasSaidaDTO();

        saidaDTO.setDsPessoaJuridicaContrato(response.getOcorrencias(0).getDsPessoaJuridicaContrato());
        saidaDTO.setDsTipoContratoNegocio(response.getOcorrencias(0).getDsTipoContratoNegocio());

        saidaDTO.setDsSituacaoContratoNegocio(response.getOcorrencias(0).getDsSituacaoContratoNegocio());
        saidaDTO.setNrSeqContratoNegocio(response.getOcorrencias(0).getNrSeqContratoNegocio());

        return saidaDTO;

    }

    /**
     * (non-Javadoc)
     * 
     * @see br.com.bradesco.web.pgit.service.business.filtroidentificao.IFiltroIdentificaoService#pesquisarFuncionarios
     * (br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaFuncionarioBradescoEntradaDTO)
     */
    public List<ConsultarListaFuncionarioBradescoSaidaDTO> pesquisarFuncionarios(
        ConsultarListaFuncionarioBradescoEntradaDTO entrada) {
        return null;
    }

    /**
     * Get: factoryAdapter.
     * 
     * @return factoryAdapter
     */
    public FactoryAdapter getFactoryAdapter() {
        return factoryAdapter;
    }

    /**
     * Set: factoryAdapter.
     * 
     * @param factoryAdapter
     *            the factory adapter
     */
    public void setFactoryAdapter(FactoryAdapter factoryAdapter) {
        this.factoryAdapter = factoryAdapter;
    }

    /**
     * (non-Javadoc)
     * 
     * @see br.com.bradesco.web.pgit.service.business.filtroidentificao.IFiltroIdentificaoService#
     * pesquisarManutencaoContrato(br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ListarContratosPgitEntradaDTO)
     */
    public List<ListarContratosPgitSaidaDTO> pesquisarManutencaoContrato(ListarContratosPgitEntradaDTO entrada) {

        ListarContratosPgitRequest request = new ListarContratosPgitRequest();

        request.setCdAgencia(entrada.getCdAgencia() == null ? 0 : entrada.getCdAgencia());
        request.setCdClubPessoa(entrada.getCdClubPessoa() == null ? 0 : entrada.getCdClubPessoa());
        request.setCdFuncionario(entrada.getCdFuncionario() == null ? 0 : entrada.getCdFuncionario());
        request.setCdPessoaJuridica(entrada.getCdPessoaJuridica() == null ? 0 : entrada.getCdPessoaJuridica());
        request.setCdSituacaoContrato(entrada.getCdSituacaoContrato() == null ? 0 : entrada.getCdSituacaoContrato());
        request.setCdTipoContratoNegocio(entrada.getCdTipoContratoNegocio() == null ? 0 : entrada
            .getCdTipoContratoNegocio());
        request.setNrSequenciaContrato(entrada.getNrSequenciaContrato() == null ? 0 : entrada.getNrSequenciaContrato());
        request.setNumeroOcorrencias(IFiltroIdentificaoServiceConstants.QTDE_CONSULTAS_LISTAR);
        request.setCdParametroPrograma(entrada.getParametroPrograma());
        request.setCdControleCpfPssoa(PgitUtil.verificaIntegerNulo(entrada.getCdControleCpfPssoa()));
        request.setCdCpfCnpjPssoa(PgitUtil.verificaLongNulo(entrada.getCdCpfCnpjPssoa()));
        request.setCdFilialCnpjPssoa(PgitUtil.verificaIntegerNulo(entrada.getCdFilialCnpjPssoa()));

        ListarContratosPgitResponse response =
            getFactoryAdapter().getListarContratosPgitPDCAdapter().invokeProcess(request);

        List<ListarContratosPgitSaidaDTO> list = new ArrayList<ListarContratosPgitSaidaDTO>();

        ListarContratosPgitSaidaDTO registro;

        for (int i = 0; i < response.getOcorrenciasCount(); i++) {
            registro = new ListarContratosPgitSaidaDTO();

            registro.setCdAditivo(response.getOcorrencias(i).getCdAditivo());
            registro.setCdClubRepresentante(response.getOcorrencias(i).getCdClubRepresentante());
            registro.setCdControleCpfCnpjRepresentante(response.getOcorrencias(i).getCdControleCpfCnpjRepresentante());
            registro.setCdCpfCnpjRepresentante(response.getOcorrencias(i).getCdCpfCnpjRepresentante());
            registro.setCdDigitoAgenciaOperadora(response.getOcorrencias(i).getCdDigitoAgenciaOperadora());
            registro.setCdAgenciaOperadora(response.getOcorrencias(i).getCdAgenciaOperadora());
            registro.setCdFilialCpfCnpjRepresentante(response.getOcorrencias(i).getCdFilialCpfCnpjRepresentante());
            registro.setCdGrupoEconomico(response.getOcorrencias(i).getCdGrupoEconomico());
            registro.setCdMotivoSituacao(response.getOcorrencias(i).getCdMotivoSituacao());
            registro.setCdPessoaJuridica(response.getOcorrencias(i).getCdPessoaJuridica());
            registro.setCdSegmentoCliente(response.getOcorrencias(i).getCdSegmentoCliente());
            registro.setCdSituacaoContrato(response.getOcorrencias(i).getCdSituacaoContrato());
            registro.setCdSubSegmentoCliente(response.getOcorrencias(i).getCdSubSegmentoCliente());
            registro.setCdTipoContrato(response.getOcorrencias(i).getCdTipoContrato());
            registro.setCdTipoParticipacao(response.getOcorrencias(i).getCdTipoParticipacao());
            registro.setDsAgenciaOperadora(response.getOcorrencias(i).getCdAgenciaOperadora() + " - "
                + response.getOcorrencias(i).getDsAgenciaOperadora());
            registro.setDsAtividadeEconomica(response.getOcorrencias(i).getDsAtividadeEconomica());
            registro.setDsContrato(response.getOcorrencias(i).getDsContrato());
            registro.setDsGrupoEconomico(response.getOcorrencias(i).getDsGrupoEconomico());
            registro.setDsMotivoSituacao(response.getOcorrencias(i).getDsMotivoSituacao());
            registro.setDsSegmentoCliente(response.getOcorrencias(i).getDsSegmentoCliente());
            registro.setDsSituacaoContrato(response.getOcorrencias(i).getDsSituacaoContrato());
            registro.setDsSubSegmentoCliente(response.getOcorrencias(i).getDsSubSegmentoCliente());
            registro.setDsTipoContrato(response.getOcorrencias(i).getDsTipoContrato());
            registro.setNmRazaoSocialRepresentante(response.getOcorrencias(i).getNmRazaoSocialRepresentante());
            registro.setNrSequenciaContrato(response.getOcorrencias(i).getNrSequenciaContrato());
            registro.setCnpjOuCpfFormatado(CpfCnpjUtils.formatCpfCnpjCompleto(response.getOcorrencias(i)
                .getCdCpfCnpjRepresentante(), response.getOcorrencias(i).getCdFilialCpfCnpjRepresentante(), response
                .getOcorrencias(i).getCdControleCpfCnpjRepresentante()));
            registro.setDsPessoaJuridica(response.getOcorrencias(i).getDsPessoaJuridica());
            registro.setCdAtividadeEconomica(response.getOcorrencias(i).getCdAtividadeEconomica());
            registro.setCodMensagem(response.getCodMensagem());
            registro.setMensagem(response.getMensagem());
            registro.setCdFuncionarioBradesco(response.getOcorrencias(i).getCdFuncionarioBradesco());
            registro.setNmFuncionarioBradesco(response.getOcorrencias(i).getNmFuncionarioBradesco());
            registro.setDtAbertura(response.getOcorrencias(i).getDtAbertura());
            registro.setDtEncerramento(response.getOcorrencias(i).getDtEncerramento());
            registro.setDtInclusao(response.getOcorrencias(i).getDtInclusao());

            list.add(registro);
        }

        return list;
    }

    /**
     * (non-Javadoc)
     * 
     * @see br.com.bradesco.web.pgit.service.business.filtroidentificao.IFiltroIdentificaoService#
     * listarContratosRenegociacao(br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ListarContratosPgitEntradaDTO)
     */
    public List<ListarContratosPgitSaidaDTO> listarContratosRenegociacao(ListarContratosPgitEntradaDTO entrada) {

        ListarContratosRenegociacaoRequest request = new ListarContratosRenegociacaoRequest();

        request.setCdClubPessoa(entrada.getCdClubPessoa() == null ? 0 : entrada.getCdClubPessoa());
        request.setCdPessoaJuridica(entrada.getCdPessoaJuridica() == null ? 0 : entrada.getCdPessoaJuridica());
        request.setNrSequenciaContrato(entrada.getNrSequenciaContrato() == null ? 0 : entrada.getNrSequenciaContrato());
        request.setCdControleCpfPssoa(PgitUtil.verificaIntegerNulo(entrada.getCdControleCpfPssoa()));
        request.setCdCpfCnpjPssoa(PgitUtil.verificaLongNulo(entrada.getCdCpfCnpjPssoa()));
        request.setCdFilialCnpjPssoa(PgitUtil.verificaIntegerNulo(entrada.getCdFilialCnpjPssoa()));
        request.setNrOcorrencias(IFiltroIdentificaoServiceConstants.QTDE_CONSULTAS_LISTAR);
        request.setCdTipoContrato(PgitUtil.verificaIntegerNulo(entrada.getCdTipoContratoNegocio()));

        ListarContratosRenegociacaoResponse response =
            getFactoryAdapter().getListarContratosRenegociacaoPDCAdapter().invokeProcess(request);

        List<ListarContratosPgitSaidaDTO> list = new ArrayList<ListarContratosPgitSaidaDTO>();

        ListarContratosPgitSaidaDTO registro;

        for (int i = 0; i < response.getOcorrenciasCount(); i++) {
            registro = new ListarContratosPgitSaidaDTO();

            registro.setCdAditivo(response.getOcorrencias(i).getCdAditivo());
            registro.setCdClubRepresentante(response.getOcorrencias(i).getCdClubRepresentante());
            registro.setCdControleCpfCnpjRepresentante(response.getOcorrencias(i).getCdControleCnpjRepresentante());
            registro.setCdCpfCnpjRepresentante(response.getOcorrencias(i).getCdCorpoCnpjRepresentante());
            registro.setCdDigitoAgenciaOperadora(response.getOcorrencias(i).getCdDigitoAgenciaOperadora());
            registro.setCdAgenciaOperadora(response.getOcorrencias(i).getCdAgenciaOperadora());
            registro.setCdFilialCpfCnpjRepresentante(response.getOcorrencias(i).getCdFilialCnpjRepresentante());
            registro.setCdGrupoEconomico(response.getOcorrencias(i).getCdGrupoEconomico());
            registro.setCdMotivoSituacao(response.getOcorrencias(i).getCdMotivoSituacao());
            registro.setCdPessoaJuridica(response.getOcorrencias(i).getCdPessoaJuridica());
            registro.setCdSegmentoCliente(response.getOcorrencias(i).getCdSegmentoCliente());
            registro.setCdSituacaoContrato(response.getOcorrencias(i).getCdSituacaoContrato());
            registro.setCdSubSegmentoCliente(response.getOcorrencias(i).getCdSubSegmentoCliente());
            registro.setCdTipoContrato(response.getOcorrencias(i).getCdTipoContrato());
            registro.setCdTipoParticipacao(response.getOcorrencias(i).getCdTipoParticipacao());
            registro.setDsAgenciaOperadora(response.getOcorrencias(i).getCdAgenciaOperadora() + " - "
                + response.getOcorrencias(i).getDsAgenciaOperadora());
            registro.setDsAtividadeEconomica(response.getOcorrencias(i).getDsAtividadeEconomica());
            registro.setDsContrato(response.getOcorrencias(i).getDsContrato());
            registro.setDsGrupoEconomico(response.getOcorrencias(i).getDsGrupoEconomico());
            registro.setDsMotivoSituacao(response.getOcorrencias(i).getDsMotivoSituacao());
            registro.setDsSegmentoCliente(response.getOcorrencias(i).getDsSegmentoCliente());
            registro.setDsSituacaoContrato(response.getOcorrencias(i).getDsSituacaoContrato());
            registro.setDsSubSegmentoCliente(response.getOcorrencias(i).getDsSubSegmentoCliente());
            registro.setDsTipoContrato(response.getOcorrencias(i).getDsTipoContrato());
            registro.setNmRazaoSocialRepresentante(response.getOcorrencias(i).getNmRazaoRepresentante());
            registro.setNrSequenciaContrato(response.getOcorrencias(i).getNrSequenciaContrato());
            registro.setCnpjOuCpfFormatado(CpfCnpjUtils.formatCpfCnpjCompleto(response.getOcorrencias(i)
                .getCdCorpoCnpjRepresentante(), response.getOcorrencias(i).getCdFilialCnpjRepresentante(), response
                .getOcorrencias(i).getCdControleCnpjRepresentante()));
            registro.setDsPessoaJuridica(response.getOcorrencias(i).getDsPessoaJuridica());
            registro.setCdAtividadeEconomica(response.getOcorrencias(i).getCdAtividadeEconomica());
            registro.setCodMensagem(response.getCodMensagem());
            registro.setMensagem(response.getMensagem());
            registro.setCdFuncionarioBradesco(response.getOcorrencias(i).getCdFuncionarioBradesco());
            registro.setNmFuncionarioBradesco(response.getOcorrencias(i).getDsFuncionarioBradesco());
            registro.setDtAbertura(response.getOcorrencias(i).getDtAbertura());
            registro.setDtEncerramento(response.getOcorrencias(i).getDtEncerramento());
            registro.setDtInclusao(response.getOcorrencias(i).getDtInclusao());

            list.add(registro);
        }

        return list;
    }

}
