/*
 * Nome: br.com.bradesco.web.pgit.service.business.simulacaotarifasnegociacao.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.simulacaotarifasnegociacao.bean;

import java.math.BigDecimal;

/**
 * Nome: DetalharSimulacaoTarifaNegociacaoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class DetalharSimulacaoTarifaNegociacaoSaidaDTO {
	
	/** Atributo codMensagem. */
	private String codMensagem;
    
    /** Atributo mensagem. */
    private String mensagem;
    
    /** Atributo vlTarifaTotalPad. */
    private BigDecimal vlTarifaTotalPad;
    
    /** Atributo vlTarifaTotalPro. */
    private BigDecimal vlTarifaTotalPro;
    
    /** Atributo vlTarifaDesconto. */
    private BigDecimal vlTarifaDesconto;
    
    /** Atributo vlFloat. */
    private BigDecimal vlFloat;
    
    /** Atributo percentualFlexibilidade. */
    private BigDecimal percentualFlexibilidade;
	
	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}
	
	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	/**
	 * Get: percentualFlexibilidade.
	 *
	 * @return percentualFlexibilidade
	 */
	public BigDecimal getPercentualFlexibilidade() {
		return percentualFlexibilidade;
	}
	
	/**
	 * Set: percentualFlexibilidade.
	 *
	 * @param percentualFlexibilidade the percentual flexibilidade
	 */
	public void setPercentualFlexibilidade(BigDecimal percentualFlexibilidade) {
		this.percentualFlexibilidade = percentualFlexibilidade;
	}
	
	/**
	 * Get: vlFloat.
	 *
	 * @return vlFloat
	 */
	public BigDecimal getVlFloat() {
		return vlFloat;
	}
	
	/**
	 * Set: vlFloat.
	 *
	 * @param vlFloat the vl float
	 */
	public void setVlFloat(BigDecimal vlFloat) {
		this.vlFloat = vlFloat;
	}
	
	/**
	 * Get: vlTarifaDesconto.
	 *
	 * @return vlTarifaDesconto
	 */
	public BigDecimal getVlTarifaDesconto() {
		return vlTarifaDesconto;
	}
	
	/**
	 * Set: vlTarifaDesconto.
	 *
	 * @param vlTarifaDesconto the vl tarifa desconto
	 */
	public void setVlTarifaDesconto(BigDecimal vlTarifaDesconto) {
		this.vlTarifaDesconto = vlTarifaDesconto;
	}
	
	/**
	 * Get: vlTarifaTotalPad.
	 *
	 * @return vlTarifaTotalPad
	 */
	public BigDecimal getVlTarifaTotalPad() {
		return vlTarifaTotalPad;
	}
	
	/**
	 * Set: vlTarifaTotalPad.
	 *
	 * @param vlTarifaTotalPad the vl tarifa total pad
	 */
	public void setVlTarifaTotalPad(BigDecimal vlTarifaTotalPad) {
		this.vlTarifaTotalPad = vlTarifaTotalPad;
	}
	
	/**
	 * Get: vlTarifaTotalPro.
	 *
	 * @return vlTarifaTotalPro
	 */
	public BigDecimal getVlTarifaTotalPro() {
		return vlTarifaTotalPro;
	}
	
	/**
	 * Set: vlTarifaTotalPro.
	 *
	 * @param vlTarifaTotalPro the vl tarifa total pro
	 */
	public void setVlTarifaTotalPro(BigDecimal vlTarifaTotalPro) {
		this.vlTarifaTotalPro = vlTarifaTotalPro;
	}
    
    

}
