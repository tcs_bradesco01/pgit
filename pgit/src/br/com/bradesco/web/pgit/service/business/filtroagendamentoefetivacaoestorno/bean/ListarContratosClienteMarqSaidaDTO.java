/*
 * Nome: br.com.bradesco.web.pgit.service.business.filtroagendamentoefetivacaoestorno.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.filtroagendamentoefetivacaoestorno.bean;

/**
 * Nome: ListarContratosClienteMarqSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarContratosClienteMarqSaidaDTO{
    
    /** Atributo codMensagem. */
    private String codMensagem;
    
    /** Atributo mensagem. */
    private String mensagem;
    
    /** Atributo numeroLinhas. */
    private Integer numeroLinhas;
    
    /** Atributo cdPessoaJuridicaContrato. */
    private Long cdPessoaJuridicaContrato;
    
    /** Atributo dsPessoaJuridicaContrato. */
    private String dsPessoaJuridicaContrato;
    
    /** Atributo cdTipoParticipacaoPessoa. */
    private Integer cdTipoParticipacaoPessoa;
    
    /** Atributo dsTipoParticipacaoPessoa. */
    private String dsTipoParticipacaoPessoa;
    
    /** Atributo cdTipoContratoNegocio. */
    private Integer cdTipoContratoNegocio;
    
    /** Atributo dsTipoContratoNegocio. */
    private String dsTipoContratoNegocio;
    
    /** Atributo nrSequenciaContratoNegocio. */
    private Long nrSequenciaContratoNegocio;
    
    /** Atributo cdSituacaoParticipanteContrato. */
    private Integer cdSituacaoParticipanteContrato;
    
    /** Atributo dsSituacaoParticipanteContrato. */
    private String dsSituacaoParticipanteContrato;
    
    /** Atributo dtInicioParticipacaoContrato. */
    private String dtInicioParticipacaoContrato;
    
    /** Atributo dtFimParticipacaoContrato. */
    private String dtFimParticipacaoContrato;
    
    /** Atributo dsContrato. */
    private String dsContrato;
    
    /** Atributo cdSituacaoContratoNegocio. */
    private Integer cdSituacaoContratoNegocio;
    
    /** Atributo dsSituacaoContratoNegocio. */
    private String dsSituacaoContratoNegocio;
    
	/**
	 * Get: cdPessoaJuridicaContrato.
	 *
	 * @return cdPessoaJuridicaContrato
	 */
	public Long getCdPessoaJuridicaContrato() {
		return cdPessoaJuridicaContrato;
	}
	
	/**
	 * Set: cdPessoaJuridicaContrato.
	 *
	 * @param cdPessoaJuridicaContrato the cd pessoa juridica contrato
	 */
	public void setCdPessoaJuridicaContrato(Long cdPessoaJuridicaContrato) {
		this.cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
	}
	
	/**
	 * Get: cdSituacaoContratoNegocio.
	 *
	 * @return cdSituacaoContratoNegocio
	 */
	public Integer getCdSituacaoContratoNegocio() {
		return cdSituacaoContratoNegocio;
	}
	
	/**
	 * Set: cdSituacaoContratoNegocio.
	 *
	 * @param cdSituacaoContratoNegocio the cd situacao contrato negocio
	 */
	public void setCdSituacaoContratoNegocio(Integer cdSituacaoContratoNegocio) {
		this.cdSituacaoContratoNegocio = cdSituacaoContratoNegocio;
	}
	
	/**
	 * Get: cdSituacaoParticipanteContrato.
	 *
	 * @return cdSituacaoParticipanteContrato
	 */
	public Integer getCdSituacaoParticipanteContrato() {
		return cdSituacaoParticipanteContrato;
	}
	
	/**
	 * Set: cdSituacaoParticipanteContrato.
	 *
	 * @param cdSituacaoParticipanteContrato the cd situacao participante contrato
	 */
	public void setCdSituacaoParticipanteContrato(
			Integer cdSituacaoParticipanteContrato) {
		this.cdSituacaoParticipanteContrato = cdSituacaoParticipanteContrato;
	}
	
	/**
	 * Get: cdTipoContratoNegocio.
	 *
	 * @return cdTipoContratoNegocio
	 */
	public Integer getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}
	
	/**
	 * Set: cdTipoContratoNegocio.
	 *
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}
	
	/**
	 * Get: cdTipoParticipacaoPessoa.
	 *
	 * @return cdTipoParticipacaoPessoa
	 */
	public Integer getCdTipoParticipacaoPessoa() {
		return cdTipoParticipacaoPessoa;
	}
	
	/**
	 * Set: cdTipoParticipacaoPessoa.
	 *
	 * @param cdTipoParticipacaoPessoa the cd tipo participacao pessoa
	 */
	public void setCdTipoParticipacaoPessoa(Integer cdTipoParticipacaoPessoa) {
		this.cdTipoParticipacaoPessoa = cdTipoParticipacaoPessoa;
	}
	
	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}
	
	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}
	
	/**
	 * Get: dsContrato.
	 *
	 * @return dsContrato
	 */
	public String getDsContrato() {
		return dsContrato;
	}
	
	/**
	 * Set: dsContrato.
	 *
	 * @param dsContrato the ds contrato
	 */
	public void setDsContrato(String dsContrato) {
		this.dsContrato = dsContrato;
	}
	
	/**
	 * Get: dsPessoaJuridicaContrato.
	 *
	 * @return dsPessoaJuridicaContrato
	 */
	public String getDsPessoaJuridicaContrato() {
		return dsPessoaJuridicaContrato;
	}
	
	/**
	 * Set: dsPessoaJuridicaContrato.
	 *
	 * @param dsPessoaJuridicaContrato the ds pessoa juridica contrato
	 */
	public void setDsPessoaJuridicaContrato(String dsPessoaJuridicaContrato) {
		this.dsPessoaJuridicaContrato = dsPessoaJuridicaContrato;
	}
	
	/**
	 * Get: dsSituacaoContratoNegocio.
	 *
	 * @return dsSituacaoContratoNegocio
	 */
	public String getDsSituacaoContratoNegocio() {
		return dsSituacaoContratoNegocio;
	}
	
	/**
	 * Set: dsSituacaoContratoNegocio.
	 *
	 * @param dsSituacaoContratoNegocio the ds situacao contrato negocio
	 */
	public void setDsSituacaoContratoNegocio(String dsSituacaoContratoNegocio) {
		this.dsSituacaoContratoNegocio = dsSituacaoContratoNegocio;
	}
	
	/**
	 * Get: dsSituacaoParticipanteContrato.
	 *
	 * @return dsSituacaoParticipanteContrato
	 */
	public String getDsSituacaoParticipanteContrato() {
		return dsSituacaoParticipanteContrato;
	}
	
	/**
	 * Set: dsSituacaoParticipanteContrato.
	 *
	 * @param dsSituacaoParticipanteContrato the ds situacao participante contrato
	 */
	public void setDsSituacaoParticipanteContrato(
			String dsSituacaoParticipanteContrato) {
		this.dsSituacaoParticipanteContrato = dsSituacaoParticipanteContrato;
	}
	
	/**
	 * Get: dsTipoContratoNegocio.
	 *
	 * @return dsTipoContratoNegocio
	 */
	public String getDsTipoContratoNegocio() {
		return dsTipoContratoNegocio;
	}
	
	/**
	 * Set: dsTipoContratoNegocio.
	 *
	 * @param dsTipoContratoNegocio the ds tipo contrato negocio
	 */
	public void setDsTipoContratoNegocio(String dsTipoContratoNegocio) {
		this.dsTipoContratoNegocio = dsTipoContratoNegocio;
	}
	
	/**
	 * Get: dsTipoParticipacaoPessoa.
	 *
	 * @return dsTipoParticipacaoPessoa
	 */
	public String getDsTipoParticipacaoPessoa() {
		return dsTipoParticipacaoPessoa;
	}
	
	/**
	 * Set: dsTipoParticipacaoPessoa.
	 *
	 * @param dsTipoParticipacaoPessoa the ds tipo participacao pessoa
	 */
	public void setDsTipoParticipacaoPessoa(String dsTipoParticipacaoPessoa) {
		this.dsTipoParticipacaoPessoa = dsTipoParticipacaoPessoa;
	}
	
	/**
	 * Get: dtFimParticipacaoContrato.
	 *
	 * @return dtFimParticipacaoContrato
	 */
	public String getDtFimParticipacaoContrato() {
		return dtFimParticipacaoContrato;
	}
	
	/**
	 * Set: dtFimParticipacaoContrato.
	 *
	 * @param dtFimParticipacaoContrato the dt fim participacao contrato
	 */
	public void setDtFimParticipacaoContrato(String dtFimParticipacaoContrato) {
		this.dtFimParticipacaoContrato = dtFimParticipacaoContrato;
	}
	
	/**
	 * Get: dtInicioParticipacaoContrato.
	 *
	 * @return dtInicioParticipacaoContrato
	 */
	public String getDtInicioParticipacaoContrato() {
		return dtInicioParticipacaoContrato;
	}
	
	/**
	 * Set: dtInicioParticipacaoContrato.
	 *
	 * @param dtInicioParticipacaoContrato the dt inicio participacao contrato
	 */
	public void setDtInicioParticipacaoContrato(String dtInicioParticipacaoContrato) {
		this.dtInicioParticipacaoContrato = dtInicioParticipacaoContrato;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	/**
	 * Get: nrSequenciaContratoNegocio.
	 *
	 * @return nrSequenciaContratoNegocio
	 */
	public Long getNrSequenciaContratoNegocio() {
		return nrSequenciaContratoNegocio;
	}
	
	/**
	 * Set: nrSequenciaContratoNegocio.
	 *
	 * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
	 */
	public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}
	
	/**
	 * Get: numeroLinhas.
	 *
	 * @return numeroLinhas
	 */
	public Integer getNumeroLinhas() {
		return numeroLinhas;
	}
	
	/**
	 * Set: numeroLinhas.
	 *
	 * @param numeroLinhas the numero linhas
	 */
	public void setNumeroLinhas(Integer numeroLinhas) {
		this.numeroLinhas = numeroLinhas;
	}
    
    
}
