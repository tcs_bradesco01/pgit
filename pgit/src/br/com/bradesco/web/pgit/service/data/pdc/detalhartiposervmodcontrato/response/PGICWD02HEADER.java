/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.detalhartiposervmodcontrato.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class PGICWD02HEADER.
 * 
 * @version $Revision$ $Date$
 */
public class PGICWD02HEADER implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _PGICWD02CODLAYOUT
     */
    private java.lang.String _PGICWD02CODLAYOUT = "PGICWD02";

    /**
     * Field _PGICWD02TAMLAYOUT
     */
    private int _PGICWD02TAMLAYOUT = 6373;

    /**
     * keeps track of state for field: _PGICWD02TAMLAYOUT
     */
    private boolean _has_PGICWD02TAMLAYOUT;


      //----------------/
     //- Constructors -/
    //----------------/

    public PGICWD02HEADER() 
     {
        super();
        setPGICWD02CODLAYOUT("PGICWD02");
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalhartiposervmodcontrato.response.PGICWD02HEADER()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deletePGICWD02TAMLAYOUT
     * 
     */
    public void deletePGICWD02TAMLAYOUT()
    {
        this._has_PGICWD02TAMLAYOUT= false;
    } //-- void deletePGICWD02TAMLAYOUT() 

    /**
     * Returns the value of field 'PGICWD02CODLAYOUT'.
     * 
     * @return String
     * @return the value of field 'PGICWD02CODLAYOUT'.
     */
    public java.lang.String getPGICWD02CODLAYOUT()
    {
        return this._PGICWD02CODLAYOUT;
    } //-- java.lang.String getPGICWD02CODLAYOUT() 

    /**
     * Returns the value of field 'PGICWD02TAMLAYOUT'.
     * 
     * @return int
     * @return the value of field 'PGICWD02TAMLAYOUT'.
     */
    public int getPGICWD02TAMLAYOUT()
    {
        return this._PGICWD02TAMLAYOUT;
    } //-- int getPGICWD02TAMLAYOUT() 

    /**
     * Method hasPGICWD02TAMLAYOUT
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasPGICWD02TAMLAYOUT()
    {
        return this._has_PGICWD02TAMLAYOUT;
    } //-- boolean hasPGICWD02TAMLAYOUT() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'PGICWD02CODLAYOUT'.
     * 
     * @param PGICWD02CODLAYOUT the value of field
     * 'PGICWD02CODLAYOUT'.
     */
    public void setPGICWD02CODLAYOUT(java.lang.String PGICWD02CODLAYOUT)
    {
        this._PGICWD02CODLAYOUT = PGICWD02CODLAYOUT;
    } //-- void setPGICWD02CODLAYOUT(java.lang.String) 

    /**
     * Sets the value of field 'PGICWD02TAMLAYOUT'.
     * 
     * @param PGICWD02TAMLAYOUT the value of field
     * 'PGICWD02TAMLAYOUT'.
     */
    public void setPGICWD02TAMLAYOUT(int PGICWD02TAMLAYOUT)
    {
        this._PGICWD02TAMLAYOUT = PGICWD02TAMLAYOUT;
        this._has_PGICWD02TAMLAYOUT = true;
    } //-- void setPGICWD02TAMLAYOUT(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return PGICWD02HEADER
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.detalhartiposervmodcontrato.response.PGICWD02HEADER unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.detalhartiposervmodcontrato.response.PGICWD02HEADER) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.detalhartiposervmodcontrato.response.PGICWD02HEADER.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalhartiposervmodcontrato.response.PGICWD02HEADER unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
