/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultarcondreajustetarifa.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.math.BigDecimal;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ConsultarCondReajusteTarifaResponse.
 * 
 * @version $Revision$ $Date$
 */
public class ConsultarCondReajusteTarifaResponse implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _codMensagem
     */
    private java.lang.String _codMensagem;

    /**
     * Field _mensagem
     */
    private java.lang.String _mensagem;

    /**
     * Field _cdPeriodicidadeCobrancaTarifa
     */
    private int _cdPeriodicidadeCobrancaTarifa = 0;

    /**
     * keeps track of state for field: _cdPeriodicidadeCobrancaTarif
     */
    private boolean _has_cdPeriodicidadeCobrancaTarifa;

    /**
     * Field _dsPeriodicidadeCobrancaTarifa
     */
    private java.lang.String _dsPeriodicidadeCobrancaTarifa;

    /**
     * Field _qtDiaCobrancaTarifa
     */
    private int _qtDiaCobrancaTarifa = 0;

    /**
     * keeps track of state for field: _qtDiaCobrancaTarifa
     */
    private boolean _has_qtDiaCobrancaTarifa;

    /**
     * Field _nrFechamentoApuracaoTarifa
     */
    private int _nrFechamentoApuracaoTarifa = 0;

    /**
     * keeps track of state for field: _nrFechamentoApuracaoTarifa
     */
    private boolean _has_nrFechamentoApuracaoTarifa;

    /**
     * Field _cdTipoReajusteTarifa
     */
    private int _cdTipoReajusteTarifa = 0;

    /**
     * keeps track of state for field: _cdTipoReajusteTarifa
     */
    private boolean _has_cdTipoReajusteTarifa;

    /**
     * Field _dsTipoReajusteTarifa
     */
    private java.lang.String _dsTipoReajusteTarifa;

    /**
     * Field _cdPeriodicidadeReajusteTarifa
     */
    private int _cdPeriodicidadeReajusteTarifa = 0;

    /**
     * keeps track of state for field: _cdPeriodicidadeReajusteTarif
     */
    private boolean _has_cdPeriodicidadeReajusteTarifa;

    /**
     * Field _qtMesReajusteTarifa
     */
    private int _qtMesReajusteTarifa = 0;

    /**
     * keeps track of state for field: _qtMesReajusteTarifa
     */
    private boolean _has_qtMesReajusteTarifa;

    /**
     * Field _cdIndicadorEconomicoReajuste
     */
    private int _cdIndicadorEconomicoReajuste = 0;

    /**
     * keeps track of state for field: _cdIndicadorEconomicoReajuste
     */
    private boolean _has_cdIndicadorEconomicoReajuste;

    /**
     * Field _cdPercentualReajusteTarifa
     */
    private java.math.BigDecimal _cdPercentualReajusteTarifa = new java.math.BigDecimal("0");

    /**
     * Field _cdPercentualTarifaCatalogo
     */
    private java.math.BigDecimal _cdPercentualTarifaCatalogo = new java.math.BigDecimal("0");

    /**
     * Field _cdIndicadorTipoReajusteBloqueio
     */
    private java.lang.String _cdIndicadorTipoReajusteBloqueio;


      //----------------/
     //- Constructors -/
    //----------------/

    public ConsultarCondReajusteTarifaResponse() 
     {
        super();
        setCdPercentualReajusteTarifa(new java.math.BigDecimal("0"));
        setCdPercentualTarifaCatalogo(new java.math.BigDecimal("0"));
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarcondreajustetarifa.response.ConsultarCondReajusteTarifaResponse()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdIndicadorEconomicoReajuste
     * 
     */
    public void deleteCdIndicadorEconomicoReajuste()
    {
        this._has_cdIndicadorEconomicoReajuste= false;
    } //-- void deleteCdIndicadorEconomicoReajuste() 

    /**
     * Method deleteCdPeriodicidadeCobrancaTarifa
     * 
     */
    public void deleteCdPeriodicidadeCobrancaTarifa()
    {
        this._has_cdPeriodicidadeCobrancaTarifa= false;
    } //-- void deleteCdPeriodicidadeCobrancaTarifa() 

    /**
     * Method deleteCdPeriodicidadeReajusteTarifa
     * 
     */
    public void deleteCdPeriodicidadeReajusteTarifa()
    {
        this._has_cdPeriodicidadeReajusteTarifa= false;
    } //-- void deleteCdPeriodicidadeReajusteTarifa() 

    /**
     * Method deleteCdTipoReajusteTarifa
     * 
     */
    public void deleteCdTipoReajusteTarifa()
    {
        this._has_cdTipoReajusteTarifa= false;
    } //-- void deleteCdTipoReajusteTarifa() 

    /**
     * Method deleteNrFechamentoApuracaoTarifa
     * 
     */
    public void deleteNrFechamentoApuracaoTarifa()
    {
        this._has_nrFechamentoApuracaoTarifa= false;
    } //-- void deleteNrFechamentoApuracaoTarifa() 

    /**
     * Method deleteQtDiaCobrancaTarifa
     * 
     */
    public void deleteQtDiaCobrancaTarifa()
    {
        this._has_qtDiaCobrancaTarifa= false;
    } //-- void deleteQtDiaCobrancaTarifa() 

    /**
     * Method deleteQtMesReajusteTarifa
     * 
     */
    public void deleteQtMesReajusteTarifa()
    {
        this._has_qtMesReajusteTarifa= false;
    } //-- void deleteQtMesReajusteTarifa() 

    /**
     * Returns the value of field 'cdIndicadorEconomicoReajuste'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorEconomicoReajuste'.
     */
    public int getCdIndicadorEconomicoReajuste()
    {
        return this._cdIndicadorEconomicoReajuste;
    } //-- int getCdIndicadorEconomicoReajuste() 

    /**
     * Returns the value of field
     * 'cdIndicadorTipoReajusteBloqueio'.
     * 
     * @return String
     * @return the value of field 'cdIndicadorTipoReajusteBloqueio'.
     */
    public java.lang.String getCdIndicadorTipoReajusteBloqueio()
    {
        return this._cdIndicadorTipoReajusteBloqueio;
    } //-- java.lang.String getCdIndicadorTipoReajusteBloqueio() 

    /**
     * Returns the value of field 'cdPercentualReajusteTarifa'.
     * 
     * @return BigDecimal
     * @return the value of field 'cdPercentualReajusteTarifa'.
     */
    public java.math.BigDecimal getCdPercentualReajusteTarifa()
    {
        return this._cdPercentualReajusteTarifa;
    } //-- java.math.BigDecimal getCdPercentualReajusteTarifa() 

    /**
     * Returns the value of field 'cdPercentualTarifaCatalogo'.
     * 
     * @return BigDecimal
     * @return the value of field 'cdPercentualTarifaCatalogo'.
     */
    public java.math.BigDecimal getCdPercentualTarifaCatalogo()
    {
        return this._cdPercentualTarifaCatalogo;
    } //-- java.math.BigDecimal getCdPercentualTarifaCatalogo() 

    /**
     * Returns the value of field 'cdPeriodicidadeCobrancaTarifa'.
     * 
     * @return int
     * @return the value of field 'cdPeriodicidadeCobrancaTarifa'.
     */
    public int getCdPeriodicidadeCobrancaTarifa()
    {
        return this._cdPeriodicidadeCobrancaTarifa;
    } //-- int getCdPeriodicidadeCobrancaTarifa() 

    /**
     * Returns the value of field 'cdPeriodicidadeReajusteTarifa'.
     * 
     * @return int
     * @return the value of field 'cdPeriodicidadeReajusteTarifa'.
     */
    public int getCdPeriodicidadeReajusteTarifa()
    {
        return this._cdPeriodicidadeReajusteTarifa;
    } //-- int getCdPeriodicidadeReajusteTarifa() 

    /**
     * Returns the value of field 'cdTipoReajusteTarifa'.
     * 
     * @return int
     * @return the value of field 'cdTipoReajusteTarifa'.
     */
    public int getCdTipoReajusteTarifa()
    {
        return this._cdTipoReajusteTarifa;
    } //-- int getCdTipoReajusteTarifa() 

    /**
     * Returns the value of field 'codMensagem'.
     * 
     * @return String
     * @return the value of field 'codMensagem'.
     */
    public java.lang.String getCodMensagem()
    {
        return this._codMensagem;
    } //-- java.lang.String getCodMensagem() 

    /**
     * Returns the value of field 'dsPeriodicidadeCobrancaTarifa'.
     * 
     * @return String
     * @return the value of field 'dsPeriodicidadeCobrancaTarifa'.
     */
    public java.lang.String getDsPeriodicidadeCobrancaTarifa()
    {
        return this._dsPeriodicidadeCobrancaTarifa;
    } //-- java.lang.String getDsPeriodicidadeCobrancaTarifa() 

    /**
     * Returns the value of field 'dsTipoReajusteTarifa'.
     * 
     * @return String
     * @return the value of field 'dsTipoReajusteTarifa'.
     */
    public java.lang.String getDsTipoReajusteTarifa()
    {
        return this._dsTipoReajusteTarifa;
    } //-- java.lang.String getDsTipoReajusteTarifa() 

    /**
     * Returns the value of field 'mensagem'.
     * 
     * @return String
     * @return the value of field 'mensagem'.
     */
    public java.lang.String getMensagem()
    {
        return this._mensagem;
    } //-- java.lang.String getMensagem() 

    /**
     * Returns the value of field 'nrFechamentoApuracaoTarifa'.
     * 
     * @return int
     * @return the value of field 'nrFechamentoApuracaoTarifa'.
     */
    public int getNrFechamentoApuracaoTarifa()
    {
        return this._nrFechamentoApuracaoTarifa;
    } //-- int getNrFechamentoApuracaoTarifa() 

    /**
     * Returns the value of field 'qtDiaCobrancaTarifa'.
     * 
     * @return int
     * @return the value of field 'qtDiaCobrancaTarifa'.
     */
    public int getQtDiaCobrancaTarifa()
    {
        return this._qtDiaCobrancaTarifa;
    } //-- int getQtDiaCobrancaTarifa() 

    /**
     * Returns the value of field 'qtMesReajusteTarifa'.
     * 
     * @return int
     * @return the value of field 'qtMesReajusteTarifa'.
     */
    public int getQtMesReajusteTarifa()
    {
        return this._qtMesReajusteTarifa;
    } //-- int getQtMesReajusteTarifa() 

    /**
     * Method hasCdIndicadorEconomicoReajuste
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorEconomicoReajuste()
    {
        return this._has_cdIndicadorEconomicoReajuste;
    } //-- boolean hasCdIndicadorEconomicoReajuste() 

    /**
     * Method hasCdPeriodicidadeCobrancaTarifa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPeriodicidadeCobrancaTarifa()
    {
        return this._has_cdPeriodicidadeCobrancaTarifa;
    } //-- boolean hasCdPeriodicidadeCobrancaTarifa() 

    /**
     * Method hasCdPeriodicidadeReajusteTarifa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPeriodicidadeReajusteTarifa()
    {
        return this._has_cdPeriodicidadeReajusteTarifa;
    } //-- boolean hasCdPeriodicidadeReajusteTarifa() 

    /**
     * Method hasCdTipoReajusteTarifa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoReajusteTarifa()
    {
        return this._has_cdTipoReajusteTarifa;
    } //-- boolean hasCdTipoReajusteTarifa() 

    /**
     * Method hasNrFechamentoApuracaoTarifa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrFechamentoApuracaoTarifa()
    {
        return this._has_nrFechamentoApuracaoTarifa;
    } //-- boolean hasNrFechamentoApuracaoTarifa() 

    /**
     * Method hasQtDiaCobrancaTarifa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtDiaCobrancaTarifa()
    {
        return this._has_qtDiaCobrancaTarifa;
    } //-- boolean hasQtDiaCobrancaTarifa() 

    /**
     * Method hasQtMesReajusteTarifa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtMesReajusteTarifa()
    {
        return this._has_qtMesReajusteTarifa;
    } //-- boolean hasQtMesReajusteTarifa() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdIndicadorEconomicoReajuste'.
     * 
     * @param cdIndicadorEconomicoReajuste the value of field
     * 'cdIndicadorEconomicoReajuste'.
     */
    public void setCdIndicadorEconomicoReajuste(int cdIndicadorEconomicoReajuste)
    {
        this._cdIndicadorEconomicoReajuste = cdIndicadorEconomicoReajuste;
        this._has_cdIndicadorEconomicoReajuste = true;
    } //-- void setCdIndicadorEconomicoReajuste(int) 

    /**
     * Sets the value of field 'cdIndicadorTipoReajusteBloqueio'.
     * 
     * @param cdIndicadorTipoReajusteBloqueio the value of field
     * 'cdIndicadorTipoReajusteBloqueio'.
     */
    public void setCdIndicadorTipoReajusteBloqueio(java.lang.String cdIndicadorTipoReajusteBloqueio)
    {
        this._cdIndicadorTipoReajusteBloqueio = cdIndicadorTipoReajusteBloqueio;
    } //-- void setCdIndicadorTipoReajusteBloqueio(java.lang.String) 

    /**
     * Sets the value of field 'cdPercentualReajusteTarifa'.
     * 
     * @param cdPercentualReajusteTarifa the value of field
     * 'cdPercentualReajusteTarifa'.
     */
    public void setCdPercentualReajusteTarifa(java.math.BigDecimal cdPercentualReajusteTarifa)
    {
        this._cdPercentualReajusteTarifa = cdPercentualReajusteTarifa;
    } //-- void setCdPercentualReajusteTarifa(java.math.BigDecimal) 

    /**
     * Sets the value of field 'cdPercentualTarifaCatalogo'.
     * 
     * @param cdPercentualTarifaCatalogo the value of field
     * 'cdPercentualTarifaCatalogo'.
     */
    public void setCdPercentualTarifaCatalogo(java.math.BigDecimal cdPercentualTarifaCatalogo)
    {
        this._cdPercentualTarifaCatalogo = cdPercentualTarifaCatalogo;
    } //-- void setCdPercentualTarifaCatalogo(java.math.BigDecimal) 

    /**
     * Sets the value of field 'cdPeriodicidadeCobrancaTarifa'.
     * 
     * @param cdPeriodicidadeCobrancaTarifa the value of field
     * 'cdPeriodicidadeCobrancaTarifa'.
     */
    public void setCdPeriodicidadeCobrancaTarifa(int cdPeriodicidadeCobrancaTarifa)
    {
        this._cdPeriodicidadeCobrancaTarifa = cdPeriodicidadeCobrancaTarifa;
        this._has_cdPeriodicidadeCobrancaTarifa = true;
    } //-- void setCdPeriodicidadeCobrancaTarifa(int) 

    /**
     * Sets the value of field 'cdPeriodicidadeReajusteTarifa'.
     * 
     * @param cdPeriodicidadeReajusteTarifa the value of field
     * 'cdPeriodicidadeReajusteTarifa'.
     */
    public void setCdPeriodicidadeReajusteTarifa(int cdPeriodicidadeReajusteTarifa)
    {
        this._cdPeriodicidadeReajusteTarifa = cdPeriodicidadeReajusteTarifa;
        this._has_cdPeriodicidadeReajusteTarifa = true;
    } //-- void setCdPeriodicidadeReajusteTarifa(int) 

    /**
     * Sets the value of field 'cdTipoReajusteTarifa'.
     * 
     * @param cdTipoReajusteTarifa the value of field
     * 'cdTipoReajusteTarifa'.
     */
    public void setCdTipoReajusteTarifa(int cdTipoReajusteTarifa)
    {
        this._cdTipoReajusteTarifa = cdTipoReajusteTarifa;
        this._has_cdTipoReajusteTarifa = true;
    } //-- void setCdTipoReajusteTarifa(int) 

    /**
     * Sets the value of field 'codMensagem'.
     * 
     * @param codMensagem the value of field 'codMensagem'.
     */
    public void setCodMensagem(java.lang.String codMensagem)
    {
        this._codMensagem = codMensagem;
    } //-- void setCodMensagem(java.lang.String) 

    /**
     * Sets the value of field 'dsPeriodicidadeCobrancaTarifa'.
     * 
     * @param dsPeriodicidadeCobrancaTarifa the value of field
     * 'dsPeriodicidadeCobrancaTarifa'.
     */
    public void setDsPeriodicidadeCobrancaTarifa(java.lang.String dsPeriodicidadeCobrancaTarifa)
    {
        this._dsPeriodicidadeCobrancaTarifa = dsPeriodicidadeCobrancaTarifa;
    } //-- void setDsPeriodicidadeCobrancaTarifa(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoReajusteTarifa'.
     * 
     * @param dsTipoReajusteTarifa the value of field
     * 'dsTipoReajusteTarifa'.
     */
    public void setDsTipoReajusteTarifa(java.lang.String dsTipoReajusteTarifa)
    {
        this._dsTipoReajusteTarifa = dsTipoReajusteTarifa;
    } //-- void setDsTipoReajusteTarifa(java.lang.String) 

    /**
     * Sets the value of field 'mensagem'.
     * 
     * @param mensagem the value of field 'mensagem'.
     */
    public void setMensagem(java.lang.String mensagem)
    {
        this._mensagem = mensagem;
    } //-- void setMensagem(java.lang.String) 

    /**
     * Sets the value of field 'nrFechamentoApuracaoTarifa'.
     * 
     * @param nrFechamentoApuracaoTarifa the value of field
     * 'nrFechamentoApuracaoTarifa'.
     */
    public void setNrFechamentoApuracaoTarifa(int nrFechamentoApuracaoTarifa)
    {
        this._nrFechamentoApuracaoTarifa = nrFechamentoApuracaoTarifa;
        this._has_nrFechamentoApuracaoTarifa = true;
    } //-- void setNrFechamentoApuracaoTarifa(int) 

    /**
     * Sets the value of field 'qtDiaCobrancaTarifa'.
     * 
     * @param qtDiaCobrancaTarifa the value of field
     * 'qtDiaCobrancaTarifa'.
     */
    public void setQtDiaCobrancaTarifa(int qtDiaCobrancaTarifa)
    {
        this._qtDiaCobrancaTarifa = qtDiaCobrancaTarifa;
        this._has_qtDiaCobrancaTarifa = true;
    } //-- void setQtDiaCobrancaTarifa(int) 

    /**
     * Sets the value of field 'qtMesReajusteTarifa'.
     * 
     * @param qtMesReajusteTarifa the value of field
     * 'qtMesReajusteTarifa'.
     */
    public void setQtMesReajusteTarifa(int qtMesReajusteTarifa)
    {
        this._qtMesReajusteTarifa = qtMesReajusteTarifa;
        this._has_qtMesReajusteTarifa = true;
    } //-- void setQtMesReajusteTarifa(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ConsultarCondReajusteTarifaResponse
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultarcondreajustetarifa.response.ConsultarCondReajusteTarifaResponse unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultarcondreajustetarifa.response.ConsultarCondReajusteTarifaResponse) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultarcondreajustetarifa.response.ConsultarCondReajusteTarifaResponse.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarcondreajustetarifa.response.ConsultarCondReajusteTarifaResponse unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
