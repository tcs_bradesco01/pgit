package br.com.bradesco.web.pgit.service.business.conposicaodiariapagtosagencia.bean;


/**
 * Arquivo criado em 13/10/15.
 */
public class ValidarAgenciaSaidaDTO {

    private String codMensagem;

    private String mensagem;

    private Integer cdTipoUnidadeOrganizacional;

    private Integer cdUnidadeOrganizacional;

    private String cdIndicadorBloq;

    public void setCodMensagem(String codMensagem) {
        this.codMensagem = codMensagem;
    }

    public String getCodMensagem() {
        return this.codMensagem;
    }

    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }

    public String getMensagem() {
        return this.mensagem;
    }

    public void setCdTipoUnidadeOrganizacional(Integer cdTipoUnidadeOrganizacional) {
        this.cdTipoUnidadeOrganizacional = cdTipoUnidadeOrganizacional;
    }

    public Integer getCdTipoUnidadeOrganizacional() {
        return this.cdTipoUnidadeOrganizacional;
    }

    public void setCdUnidadeOrganizacional(Integer cdUnidadeOrganizacional) {
        this.cdUnidadeOrganizacional = cdUnidadeOrganizacional;
    }

    public Integer getCdUnidadeOrganizacional() {
        return this.cdUnidadeOrganizacional;
    }

    public void setCdIndicadorBloq(String cdIndicadorBloq) {
        this.cdIndicadorBloq = cdIndicadorBloq;
    }

    public String getCdIndicadorBloq() {
        return this.cdIndicadorBloq;
    }
}