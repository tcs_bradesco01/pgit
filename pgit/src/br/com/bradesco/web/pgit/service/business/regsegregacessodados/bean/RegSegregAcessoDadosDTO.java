/*
 * Nome: br.com.bradesco.web.pgit.service.business.regsegregacessodados.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.regsegregacessodados.bean;

/**
 * Nome: RegSegregAcessoDadosDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class RegSegregAcessoDadosDTO {
	
	/** Atributo linhaSelecionada. */
	private int linhaSelecionada;
	
	/** Atributo tipoUnidadeOrganizacionalUsuario. */
	private String tipoUnidadeOrganizacionalUsuario;
	
	/** Atributo tipoUnidadeOrganizacionalProprietarioDados. */
	private String tipoUnidadeOrganizacionalProprietarioDados;
	
	/** Atributo tipoManutencao. */
	private String tipoManutencao;
	
	/** Atributo perfilGerenciadorSeguranca. */
	private String perfilGerenciadorSeguranca;
	
	/** Atributo tipoBaixa. */
	private String tipoBaixa;
	
	
	/**
	 * Get: linhaSelecionada.
	 *
	 * @return linhaSelecionada
	 */
	public int getLinhaSelecionada() {
		return linhaSelecionada;
	}
	
	/**
	 * Set: linhaSelecionada.
	 *
	 * @param linhaSelecionada the linha selecionada
	 */
	public void setLinhaSelecionada(int linhaSelecionada) {
		this.linhaSelecionada = linhaSelecionada;
	}
	
	/**
	 * Get: perfilGerenciadorSeguranca.
	 *
	 * @return perfilGerenciadorSeguranca
	 */
	public String getPerfilGerenciadorSeguranca() {
		return perfilGerenciadorSeguranca;
	}
	
	/**
	 * Set: perfilGerenciadorSeguranca.
	 *
	 * @param perfilGerenciadorSeguranca the perfil gerenciador seguranca
	 */
	public void setPerfilGerenciadorSeguranca(String perfilGerenciadorSeguranca) {
		this.perfilGerenciadorSeguranca = perfilGerenciadorSeguranca;
	}
	
	/**
	 * Get: tipoBaixa.
	 *
	 * @return tipoBaixa
	 */
	public String getTipoBaixa() {
		return tipoBaixa;
	}
	
	/**
	 * Set: tipoBaixa.
	 *
	 * @param tipoBaixa the tipo baixa
	 */
	public void setTipoBaixa(String tipoBaixa) {
		this.tipoBaixa = tipoBaixa;
	}
	
	/**
	 * Get: tipoManutencao.
	 *
	 * @return tipoManutencao
	 */
	public String getTipoManutencao() {
		return tipoManutencao;
	}
	
	/**
	 * Set: tipoManutencao.
	 *
	 * @param tipoManutencao the tipo manutencao
	 */
	public void setTipoManutencao(String tipoManutencao) {
		this.tipoManutencao = tipoManutencao;
	}
	
	/**
	 * Get: tipoUnidadeOrganizacionalProprietarioDados.
	 *
	 * @return tipoUnidadeOrganizacionalProprietarioDados
	 */
	public String getTipoUnidadeOrganizacionalProprietarioDados() {
		return tipoUnidadeOrganizacionalProprietarioDados;
	}
	
	/**
	 * Set: tipoUnidadeOrganizacionalProprietarioDados.
	 *
	 * @param tipoUnidadeOrganizacionalProprietarioDados the tipo unidade organizacional proprietario dados
	 */
	public void setTipoUnidadeOrganizacionalProprietarioDados(
			String tipoUnidadeOrganizacionalProprietarioDados) {
		this.tipoUnidadeOrganizacionalProprietarioDados = tipoUnidadeOrganizacionalProprietarioDados;
	}
	
	/**
	 * Get: tipoUnidadeOrganizacionalUsuario.
	 *
	 * @return tipoUnidadeOrganizacionalUsuario
	 */
	public String getTipoUnidadeOrganizacionalUsuario() {
		return tipoUnidadeOrganizacionalUsuario;
	}
	
	/**
	 * Set: tipoUnidadeOrganizacionalUsuario.
	 *
	 * @param tipoUnidadeOrganizacionalUsuario the tipo unidade organizacional usuario
	 */
	public void setTipoUnidadeOrganizacionalUsuario(
			String tipoUnidadeOrganizacionalUsuario) {
		this.tipoUnidadeOrganizacionalUsuario = tipoUnidadeOrganizacionalUsuario;
	}
	
	
	
}
