package br.com.bradesco.web.pgit.service.business.mantervinculacaoconveniocontasalario.bean;

public class ListarVincConvnCtaSalarioEntradaDTO {
	
	private int maxOcorrencias;	
	private Long codPessoaJuridica;
	private int codTipoContratoNegocio;
	private Long numSeqContratoNegocio;
	public int getMaxOcorrencias() {
		return maxOcorrencias;
	}
	public void setMaxOcorrencias(int maxOcorrencias) {
		this.maxOcorrencias = maxOcorrencias;
	}
	public Long getCodPessoaJuridica() {
		return codPessoaJuridica;
	}
	public void setCodPessoaJuridica(Long codPessoaJuridica) {
		this.codPessoaJuridica = codPessoaJuridica;
	}
	public int getCodTipoContratoNegocio() {
		return codTipoContratoNegocio;
	}
	public void setCodTipoContratoNegocio(int codTipoContratoNegocio) {
		this.codTipoContratoNegocio = codTipoContratoNegocio;
	}
	public Long getNumSeqContratoNegocio() {
		return numSeqContratoNegocio;
	}
	public void setNumSeqContratoNegocio(Long numSeqContratoNegocio) {
		this.numSeqContratoNegocio = numSeqContratoNegocio;
	}
	
	
	

}
