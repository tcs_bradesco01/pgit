/*
 * Nome: br.com.bradesco.web.pgit.service.business.cmpi0000.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.cmpi0000.bean;

/**
 * Nome: ListarClientesDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarClientesDTO {
	
	/** Atributo cpf. */
	private String cpf;
	
	/** Atributo nomeRazao. */
	private String nomeRazao;
	
	/** Atributo dataNascimentoFundacao. */
	private String dataNascimentoFundacao;
	
	/** Atributo banco. */
	private String banco;
	
	/** Atributo agencia. */
	private String agencia;
	
	/** Atributo conta. */
	private String conta;
	
	/** Atributo controleCpfCnpj. */
	private String controleCpfCnpj;
	
	/** Atributo totalRegistros. */
	private int totalRegistros;
	
	/** Atributo codigoCpfCnpjCliente. */
	private String codigoCpfCnpjCliente;
	
	/** Atributo codigoFilialCnpj. */
	private String codigoFilialCnpj;
	
	/** Atributo codigoBancoDebito. */
	private String codigoBancoDebito;
	
	/** Atributo agenciaBancaria. */
	private String agenciaBancaria;
	
	/** Atributo contaBancaria. */
	private String contaBancaria;
	
	/** Atributo cpfCnpjCliente. */
	private String cpfCnpjCliente; 
	
	/** Atributo filialCnpj. */
	private String filialCnpj; 
	
	/** Atributo bcoAgeConta. */
	private String bcoAgeConta;
	
	/**
	 * Get: agencia.
	 *
	 * @return agencia
	 */
	public String getAgencia() {
		return agencia;
	}
	
	/**
	 * Set: agencia.
	 *
	 * @param agencia the agencia
	 */
	public void setAgencia(String agencia) {
		this.agencia = agencia;
	}
	
	/**
	 * Get: agenciaBancaria.
	 *
	 * @return agenciaBancaria
	 */
	public String getAgenciaBancaria() {
		return agenciaBancaria;
	}
	
	/**
	 * Set: agenciaBancaria.
	 *
	 * @param agenciaBancaria the agencia bancaria
	 */
	public void setAgenciaBancaria(String agenciaBancaria) {
		this.agenciaBancaria = agenciaBancaria;
	}
	
	/**
	 * Get: banco.
	 *
	 * @return banco
	 */
	public String getBanco() {
		return banco;
	}
	
	/**
	 * Set: banco.
	 *
	 * @param banco the banco
	 */
	public void setBanco(String banco) {
		this.banco = banco;
	}
	
	/**
	 * Get: bcoAgeConta.
	 *
	 * @return bcoAgeConta
	 */
	public String getBcoAgeConta() {
		return bcoAgeConta;
	}
	
	/**
	 * Set: bcoAgeConta.
	 *
	 * @param bcoAgeConta the bco age conta
	 */
	public void setBcoAgeConta(String bcoAgeConta) {
		this.bcoAgeConta = bcoAgeConta;
	}
	
	/**
	 * Get: codigoBancoDebito.
	 *
	 * @return codigoBancoDebito
	 */
	public String getCodigoBancoDebito() {
		return codigoBancoDebito;
	}
	
	/**
	 * Set: codigoBancoDebito.
	 *
	 * @param codigoBancoDebito the codigo banco debito
	 */
	public void setCodigoBancoDebito(String codigoBancoDebito) {
		this.codigoBancoDebito = codigoBancoDebito;
	}
	
	/**
	 * Get: codigoCpfCnpjCliente.
	 *
	 * @return codigoCpfCnpjCliente
	 */
	public String getCodigoCpfCnpjCliente() {
		return codigoCpfCnpjCliente;
	}
	
	/**
	 * Set: codigoCpfCnpjCliente.
	 *
	 * @param codigoCpfCnpjCliente the codigo cpf cnpj cliente
	 */
	public void setCodigoCpfCnpjCliente(String codigoCpfCnpjCliente) {
		this.codigoCpfCnpjCliente = codigoCpfCnpjCliente;
	}
	
	/**
	 * Get: codigoFilialCnpj.
	 *
	 * @return codigoFilialCnpj
	 */
	public String getCodigoFilialCnpj() {
		return codigoFilialCnpj;
	}
	
	/**
	 * Set: codigoFilialCnpj.
	 *
	 * @param codigoFilialCnpj the codigo filial cnpj
	 */
	public void setCodigoFilialCnpj(String codigoFilialCnpj) {
		this.codigoFilialCnpj = codigoFilialCnpj;
	}
	
	/**
	 * Get: conta.
	 *
	 * @return conta
	 */
	public String getConta() {
		return conta;
	}
	
	/**
	 * Set: conta.
	 *
	 * @param conta the conta
	 */
	public void setConta(String conta) {
		this.conta = conta;
	}
	
	/**
	 * Get: contaBancaria.
	 *
	 * @return contaBancaria
	 */
	public String getContaBancaria() {
		return contaBancaria;
	}
	
	/**
	 * Set: contaBancaria.
	 *
	 * @param contaBancaria the conta bancaria
	 */
	public void setContaBancaria(String contaBancaria) {
		this.contaBancaria = contaBancaria;
	}
	
	/**
	 * Get: controleCpfCnpj.
	 *
	 * @return controleCpfCnpj
	 */
	public String getControleCpfCnpj() {
		return controleCpfCnpj;
	}
	
	/**
	 * Set: controleCpfCnpj.
	 *
	 * @param controleCpfCnpj the controle cpf cnpj
	 */
	public void setControleCpfCnpj(String controleCpfCnpj) {
		this.controleCpfCnpj = controleCpfCnpj;
	}
	
	/**
	 * Get: cpf.
	 *
	 * @return cpf
	 */
	public String getCpf() {
		return cpf;
	}
	
	/**
	 * Set: cpf.
	 *
	 * @param cpf the cpf
	 */
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	
	/**
	 * Get: cpfCnpjCliente.
	 *
	 * @return cpfCnpjCliente
	 */
	public String getCpfCnpjCliente() {
		return cpfCnpjCliente;
	}
	
	/**
	 * Set: cpfCnpjCliente.
	 *
	 * @param cpfCnpjCliente the cpf cnpj cliente
	 */
	public void setCpfCnpjCliente(String cpfCnpjCliente) {
		this.cpfCnpjCliente = cpfCnpjCliente;
	}
	
	/**
	 * Get: dataNascimentoFundacao.
	 *
	 * @return dataNascimentoFundacao
	 */
	public String getDataNascimentoFundacao() {
		return dataNascimentoFundacao;
	}
	
	/**
	 * Set: dataNascimentoFundacao.
	 *
	 * @param dataNascimentoFundacao the data nascimento fundacao
	 */
	public void setDataNascimentoFundacao(String dataNascimentoFundacao) {
		this.dataNascimentoFundacao = dataNascimentoFundacao;
	}

	/**
	 * Get: filialCnpj.
	 *
	 * @return filialCnpj
	 */
	public String getFilialCnpj() {
		return filialCnpj;
	}
	
	/**
	 * Set: filialCnpj.
	 *
	 * @param filialCnpj the filial cnpj
	 */
	public void setFilialCnpj(String filialCnpj) {
		this.filialCnpj = filialCnpj;
	}
	
	/**
	 * Get: nomeRazao.
	 *
	 * @return nomeRazao
	 */
	public String getNomeRazao() {
		return nomeRazao;
	}
	
	/**
	 * Set: nomeRazao.
	 *
	 * @param nomeRazao the nome razao
	 */
	public void setNomeRazao(String nomeRazao) {
		this.nomeRazao = nomeRazao;
	}
	
	/**
	 * Get: totalRegistros.
	 *
	 * @return totalRegistros
	 */
	public int getTotalRegistros() {
		return totalRegistros;
	}
	
	/**
	 * Set: totalRegistros.
	 *
	 * @param totalRegistros the total registros
	 */
	public void setTotalRegistros(int totalRegistros) {
		this.totalRegistros = totalRegistros;
	}

}
