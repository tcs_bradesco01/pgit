/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantervincperfiltrocaarqpart.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantervincperfiltrocaarqpart.bean;


/**
 * Nome: IncluirVincTrocaArqParticipanteEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class IncluirVincTrocaArqParticipanteEntradaDTO{
	
	/** Atributo cdTipoLayoutArquivo. */
	private Integer cdTipoLayoutArquivo;
	
	/** Atributo cdPerfilTrocaArquivo. */
	private Long cdPerfilTrocaArquivo;
	
	/** Atributo cdPessoaJuridicaContrato. */
	private Long cdPessoaJuridicaContrato;
	
	/** Atributo cdTipoContratoNegocio. */
	private Integer cdTipoContratoNegocio;
	
	/** Atributo nrSequenciaContratoNegocio. */
	private Long nrSequenciaContratoNegocio;
	
	/** Atributo cdTipoParticipacaoPessoa. */
	private Integer cdTipoParticipacaoPessoa;
	
	/** Atributo cdPessoa. */
	private Long cdPessoa;
	
	/** Atributo cdCorpoCpfCnpj. */
	private  Long cdCorpoCpfCnpj;
	
	/** Atributo nrFilialCnpj. */
	private Integer nrFilialCnpj;
	
	/** Atributo cdDigitoCpfCnpj. */
	private String cdDigitoCpfCnpj;
	
	/** Atributo cdTipoLayoutMae. */
	private Integer cdTipoLayoutMae;
	
	/** Atributo cdPerfilTrocaMae. */
	private Long cdPerfilTrocaMae;
	
	/** Atributo cdPessoaJuridicaMae. */
	private Long cdPessoaJuridicaMae;
	
	/** Atributo cdTipoContratoMae. */
	private Integer cdTipoContratoMae;
	
	/** Atributo nrSequencialContratoMae. */
	private Long nrSequencialContratoMae;
	
	/** Atributo cdTipoParticipanteMae. */
	private Integer cdTipoParticipanteMae;
	
	/** Atributo cdCpessoaMae. */
	private Long cdCpessoaMae;
	
	/** Atributo cdCpfCnpjMae. */
	private Long cdCpfCnpjMae;
	
	/** Atributo cdCnpjFilialMae. */
	private Integer cdCnpjFilialMae;
	
	/** Atributo cdDigitoCpfCnpjMae. */
	private String cdDigitoCpfCnpjMae;
	
	/**
	 * Get: cdTipoLayoutArquivo.
	 *
	 * @return cdTipoLayoutArquivo
	 */
	public Integer getCdTipoLayoutArquivo(){
		return cdTipoLayoutArquivo;
	}

	/**
	 * Set: cdTipoLayoutArquivo.
	 *
	 * @param cdTipoLayoutArquivo the cd tipo layout arquivo
	 */
	public void setCdTipoLayoutArquivo(Integer cdTipoLayoutArquivo){
		this.cdTipoLayoutArquivo = cdTipoLayoutArquivo;
	}

	/**
	 * Get: cdPerfilTrocaArquivo.
	 *
	 * @return cdPerfilTrocaArquivo
	 */
	public Long getCdPerfilTrocaArquivo(){
		return cdPerfilTrocaArquivo;
	}

	/**
	 * Set: cdPerfilTrocaArquivo.
	 *
	 * @param cdPerfilTrocaArquivo the cd perfil troca arquivo
	 */
	public void setCdPerfilTrocaArquivo(Long cdPerfilTrocaArquivo){
		this.cdPerfilTrocaArquivo = cdPerfilTrocaArquivo;
	}

	/**
	 * Get: cdPessoaJuridicaContrato.
	 *
	 * @return cdPessoaJuridicaContrato
	 */
	public Long getCdPessoaJuridicaContrato(){
		return cdPessoaJuridicaContrato;
	}

	/**
	 * Get: cdCorpoCpfCnpj.
	 *
	 * @return cdCorpoCpfCnpj
	 */
	public Long getCdCorpoCpfCnpj() {
	    return cdCorpoCpfCnpj;
	}

	/**
	 * Set: cdCorpoCpfCnpj.
	 *
	 * @param cdCorpoCpfCnpj the cd corpo cpf cnpj
	 */
	public void setCdCorpoCpfCnpj(Long cdCorpoCpfCnpj) {
	    this.cdCorpoCpfCnpj = cdCorpoCpfCnpj;
	}

	/**
	 * Get: cdDigitoCpfCnpj.
	 *
	 * @return cdDigitoCpfCnpj
	 */
	public String getCdDigitoCpfCnpj() {
	    return cdDigitoCpfCnpj;
	}

	/**
	 * Set: cdDigitoCpfCnpj.
	 *
	 * @param cdDigitoCpfCnpj the cd digito cpf cnpj
	 */
	public void setCdDigitoCpfCnpj(String cdDigitoCpfCnpj) {
	    this.cdDigitoCpfCnpj = cdDigitoCpfCnpj;
	}

	/**
	 * Get: nrFilialCnpj.
	 *
	 * @return nrFilialCnpj
	 */
	public Integer getNrFilialCnpj() {
	    return nrFilialCnpj;
	}

	/**
	 * Set: nrFilialCnpj.
	 *
	 * @param nrFilialCnpj the nr filial cnpj
	 */
	public void setNrFilialCnpj(Integer nrFilialCnpj) {
	    this.nrFilialCnpj = nrFilialCnpj;
	}

	/**
	 * Set: cdPessoaJuridicaContrato.
	 *
	 * @param cdPessoaJuridicaContrato the cd pessoa juridica contrato
	 */
	public void setCdPessoaJuridicaContrato(Long cdPessoaJuridicaContrato){
		this.cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
	}

	/**
	 * Get: cdTipoContratoNegocio.
	 *
	 * @return cdTipoContratoNegocio
	 */
	public Integer getCdTipoContratoNegocio(){
		return cdTipoContratoNegocio;
	}

	/**
	 * Set: cdTipoContratoNegocio.
	 *
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio){
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}

	/**
	 * Get: nrSequenciaContratoNegocio.
	 *
	 * @return nrSequenciaContratoNegocio
	 */
	public Long getNrSequenciaContratoNegocio(){
		return nrSequenciaContratoNegocio;
	}

	/**
	 * Set: nrSequenciaContratoNegocio.
	 *
	 * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
	 */
	public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio){
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}

	/**
	 * Get: cdTipoParticipacaoPessoa.
	 *
	 * @return cdTipoParticipacaoPessoa
	 */
	public Integer getCdTipoParticipacaoPessoa(){
		return cdTipoParticipacaoPessoa;
	}

	/**
	 * Set: cdTipoParticipacaoPessoa.
	 *
	 * @param cdTipoParticipacaoPessoa the cd tipo participacao pessoa
	 */
	public void setCdTipoParticipacaoPessoa(Integer cdTipoParticipacaoPessoa){
		this.cdTipoParticipacaoPessoa = cdTipoParticipacaoPessoa;
	}

	/**
	 * Get: cdPessoa.
	 *
	 * @return cdPessoa
	 */
	public Long getCdPessoa(){
		return cdPessoa;
	}

	/**
	 * Set: cdPessoa.
	 *
	 * @param cdPessoa the cd pessoa
	 */
	public void setCdPessoa(Long cdPessoa){
		this.cdPessoa = cdPessoa;
	}

	/**
	 * @return the cdTipoLayoutMae
	 */
	public Integer getCdTipoLayoutMae() {
		return cdTipoLayoutMae;
	}

	/**
	 * @param cdTipoLayoutMae the cdTipoLayoutMae to set
	 */
	public void setCdTipoLayoutMae(Integer cdTipoLayoutMae) {
		this.cdTipoLayoutMae = cdTipoLayoutMae;
	}

	/**
	 * @return the cdPerfilTrocaMae
	 */
	public Long getCdPerfilTrocaMae() {
		return cdPerfilTrocaMae;
	}

	/**
	 * @param cdPerfilTrocaMae the cdPerfilTrocaMae to set
	 */
	public void setCdPerfilTrocaMae(Long cdPerfilTrocaMae) {
		this.cdPerfilTrocaMae = cdPerfilTrocaMae;
	}

	/**
	 * @return the cdPessoaJuridicaMae
	 */
	public Long getCdPessoaJuridicaMae() {
		return cdPessoaJuridicaMae;
	}

	/**
	 * @param cdPessoaJuridicaMae the cdPessoaJuridicaMae to set
	 */
	public void setCdPessoaJuridicaMae(Long cdPessoaJuridicaMae) {
		this.cdPessoaJuridicaMae = cdPessoaJuridicaMae;
	}

	/**
	 * @return the cdTipoContratoMae
	 */
	public Integer getCdTipoContratoMae() {
		return cdTipoContratoMae;
	}

	/**
	 * @param cdTipoContratoMae the cdTipoContratoMae to set
	 */
	public void setCdTipoContratoMae(Integer cdTipoContratoMae) {
		this.cdTipoContratoMae = cdTipoContratoMae;
	}

	/**
	 * @return the nrSequencialContratoMae
	 */
	public Long getNrSequencialContratoMae() {
		return nrSequencialContratoMae;
	}

	/**
	 * @param nrSequencialContratoMae the nrSequencialContratoMae to set
	 */
	public void setNrSequencialContratoMae(Long nrSequencialContratoMae) {
		this.nrSequencialContratoMae = nrSequencialContratoMae;
	}

	/**
	 * @return the cdTipoParticipanteMae
	 */
	public Integer getCdTipoParticipanteMae() {
		return cdTipoParticipanteMae;
	}

	/**
	 * @param cdTipoParticipanteMae the cdTipoParticipanteMae to set
	 */
	public void setCdTipoParticipanteMae(Integer cdTipoParticipanteMae) {
		this.cdTipoParticipanteMae = cdTipoParticipanteMae;
	}

	/**
	 * @return the cdCpessoaMae
	 */
	public Long getCdCpessoaMae() {
		return cdCpessoaMae;
	}

	/**
	 * @param cdCpessoaMae the cdCpessoaMae to set
	 */
	public void setCdCpessoaMae(Long cdCpessoaMae) {
		this.cdCpessoaMae = cdCpessoaMae;
	}

	/**
	 * @return the cdCpfCnpjMae
	 */
	public Long getCdCpfCnpjMae() {
		return cdCpfCnpjMae;
	}

	/**
	 * @param cdCpfCnpjMae the cdCpfCnpjMae to set
	 */
	public void setCdCpfCnpjMae(Long cdCpfCnpjMae) {
		this.cdCpfCnpjMae = cdCpfCnpjMae;
	}

	/**
	 * @return the cdCnpjFilialMae
	 */
	public Integer getCdCnpjFilialMae() {
		return cdCnpjFilialMae;
	}

	/**
	 * @param cdCnpjFilialMae the cdCnpjFilialMae to set
	 */
	public void setCdCnpjFilialMae(Integer cdCnpjFilialMae) {
		this.cdCnpjFilialMae = cdCnpjFilialMae;
	}

	/**
	 * @return the cdDigitoCpfCnpjMae
	 */
	public String getCdDigitoCpfCnpjMae() {
		return cdDigitoCpfCnpjMae;
	}

	/**
	 * @param cdDigitoCpfCnpjMae the cdDigitoCpfCnpjMae to set
	 */
	public void setCdDigitoCpfCnpjMae(String cdDigitoCpfCnpjMae) {
		this.cdDigitoCpfCnpjMae = cdDigitoCpfCnpjMae;
	}

}