/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultardadoscliente.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ConsultarDadosClienteRequest.
 * 
 * @version $Revision$ $Date$
 */
public class ConsultarDadosClienteRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdCorpoCpfCnpj
     */
    private long _cdCorpoCpfCnpj = 0;

    /**
     * keeps track of state for field: _cdCorpoCpfCnpj
     */
    private boolean _has_cdCorpoCpfCnpj;

    /**
     * Field _cdFilialCpfCnpj
     */
    private int _cdFilialCpfCnpj = 0;

    /**
     * keeps track of state for field: _cdFilialCpfCnpj
     */
    private boolean _has_cdFilialCpfCnpj;

    /**
     * Field _cdContratoCpfCnpj
     */
    private int _cdContratoCpfCnpj = 0;

    /**
     * keeps track of state for field: _cdContratoCpfCnpj
     */
    private boolean _has_cdContratoCpfCnpj;

    /**
     * Field _dtSistema
     */
    private int _dtSistema = 0;

    /**
     * keeps track of state for field: _dtSistema
     */
    private boolean _has_dtSistema;


      //----------------/
     //- Constructors -/
    //----------------/

    public ConsultarDadosClienteRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultardadoscliente.request.ConsultarDadosClienteRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdContratoCpfCnpj
     * 
     */
    public void deleteCdContratoCpfCnpj()
    {
        this._has_cdContratoCpfCnpj= false;
    } //-- void deleteCdContratoCpfCnpj() 

    /**
     * Method deleteCdCorpoCpfCnpj
     * 
     */
    public void deleteCdCorpoCpfCnpj()
    {
        this._has_cdCorpoCpfCnpj= false;
    } //-- void deleteCdCorpoCpfCnpj() 

    /**
     * Method deleteCdFilialCpfCnpj
     * 
     */
    public void deleteCdFilialCpfCnpj()
    {
        this._has_cdFilialCpfCnpj= false;
    } //-- void deleteCdFilialCpfCnpj() 

    /**
     * Method deleteDtSistema
     * 
     */
    public void deleteDtSistema()
    {
        this._has_dtSistema= false;
    } //-- void deleteDtSistema() 

    /**
     * Returns the value of field 'cdContratoCpfCnpj'.
     * 
     * @return int
     * @return the value of field 'cdContratoCpfCnpj'.
     */
    public int getCdContratoCpfCnpj()
    {
        return this._cdContratoCpfCnpj;
    } //-- int getCdContratoCpfCnpj() 

    /**
     * Returns the value of field 'cdCorpoCpfCnpj'.
     * 
     * @return long
     * @return the value of field 'cdCorpoCpfCnpj'.
     */
    public long getCdCorpoCpfCnpj()
    {
        return this._cdCorpoCpfCnpj;
    } //-- long getCdCorpoCpfCnpj() 

    /**
     * Returns the value of field 'cdFilialCpfCnpj'.
     * 
     * @return int
     * @return the value of field 'cdFilialCpfCnpj'.
     */
    public int getCdFilialCpfCnpj()
    {
        return this._cdFilialCpfCnpj;
    } //-- int getCdFilialCpfCnpj() 

    /**
     * Returns the value of field 'dtSistema'.
     * 
     * @return int
     * @return the value of field 'dtSistema'.
     */
    public int getDtSistema()
    {
        return this._dtSistema;
    } //-- int getDtSistema() 

    /**
     * Method hasCdContratoCpfCnpj
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdContratoCpfCnpj()
    {
        return this._has_cdContratoCpfCnpj;
    } //-- boolean hasCdContratoCpfCnpj() 

    /**
     * Method hasCdCorpoCpfCnpj
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCorpoCpfCnpj()
    {
        return this._has_cdCorpoCpfCnpj;
    } //-- boolean hasCdCorpoCpfCnpj() 

    /**
     * Method hasCdFilialCpfCnpj
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFilialCpfCnpj()
    {
        return this._has_cdFilialCpfCnpj;
    } //-- boolean hasCdFilialCpfCnpj() 

    /**
     * Method hasDtSistema
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasDtSistema()
    {
        return this._has_dtSistema;
    } //-- boolean hasDtSistema() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdContratoCpfCnpj'.
     * 
     * @param cdContratoCpfCnpj the value of field
     * 'cdContratoCpfCnpj'.
     */
    public void setCdContratoCpfCnpj(int cdContratoCpfCnpj)
    {
        this._cdContratoCpfCnpj = cdContratoCpfCnpj;
        this._has_cdContratoCpfCnpj = true;
    } //-- void setCdContratoCpfCnpj(int) 

    /**
     * Sets the value of field 'cdCorpoCpfCnpj'.
     * 
     * @param cdCorpoCpfCnpj the value of field 'cdCorpoCpfCnpj'.
     */
    public void setCdCorpoCpfCnpj(long cdCorpoCpfCnpj)
    {
        this._cdCorpoCpfCnpj = cdCorpoCpfCnpj;
        this._has_cdCorpoCpfCnpj = true;
    } //-- void setCdCorpoCpfCnpj(long) 

    /**
     * Sets the value of field 'cdFilialCpfCnpj'.
     * 
     * @param cdFilialCpfCnpj the value of field 'cdFilialCpfCnpj'.
     */
    public void setCdFilialCpfCnpj(int cdFilialCpfCnpj)
    {
        this._cdFilialCpfCnpj = cdFilialCpfCnpj;
        this._has_cdFilialCpfCnpj = true;
    } //-- void setCdFilialCpfCnpj(int) 

    /**
     * Sets the value of field 'dtSistema'.
     * 
     * @param dtSistema the value of field 'dtSistema'.
     */
    public void setDtSistema(int dtSistema)
    {
        this._dtSistema = dtSistema;
        this._has_dtSistema = true;
    } //-- void setDtSistema(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ConsultarDadosClienteRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultardadoscliente.request.ConsultarDadosClienteRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultardadoscliente.request.ConsultarDadosClienteRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultardadoscliente.request.ConsultarDadosClienteRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultardadoscliente.request.ConsultarDadosClienteRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
