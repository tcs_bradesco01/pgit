/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/interfaz.ftl,v $
 * $Id: interfaz.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: interfaz.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.cadtipopendencia;

import java.util.List;

import br.com.bradesco.web.pgit.service.business.cadtipopendencia.bean.ConsultarDescricaoUnidOrganizacionalEntradaDTO;
import br.com.bradesco.web.pgit.service.business.cadtipopendencia.bean.ConsultarDescricaoUnidOrganizacionalSaidaDTO;
import br.com.bradesco.web.pgit.service.business.cadtipopendencia.bean.TipoPendenciaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.cadtipopendencia.bean.TipoPendenciaSaidaDTO;

/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Interface do adaptador: ManterCadastroTipoPendencia
 * </p>
 * 
 * @comment CODIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public interface ICadTipPendenciaService {


	/**
	 * Incluir tipo pendencia.
	 *
	 * @param tipoPendenciaEntradaDTO the tipo pendencia entrada dto
	 * @return the tipo pendencia saida dto
	 */
	TipoPendenciaSaidaDTO incluirTipoPendencia(TipoPendenciaEntradaDTO tipoPendenciaEntradaDTO);
	
	/**
	 * Alterar tipo pendencia.
	 *
	 * @param tipoPendenciaEntradaDTO the tipo pendencia entrada dto
	 * @return the tipo pendencia saida dto
	 */
	TipoPendenciaSaidaDTO alterarTipoPendencia(TipoPendenciaEntradaDTO tipoPendenciaEntradaDTO);
	
	/**
	 * Excluir tipo pendencia.
	 *
	 * @param tipoPendenciaEntradaDTO the tipo pendencia entrada dto
	 * @return the tipo pendencia saida dto
	 */
	TipoPendenciaSaidaDTO excluirTipoPendencia(TipoPendenciaEntradaDTO tipoPendenciaEntradaDTO);
	
	/**
	 * Listar tipo pendencia.
	 *
	 * @param tipoPendenciaEntradaDTO the tipo pendencia entrada dto
	 * @return the list< tipo pendencia saida dt o>
	 */
	List<TipoPendenciaSaidaDTO> listarTipoPendencia(TipoPendenciaEntradaDTO tipoPendenciaEntradaDTO);
	
	/**
	 * Detalhar tipo pendencia.
	 *
	 * @param tipoPendenciaEntradaDTO the tipo pendencia entrada dto
	 * @return the tipo pendencia saida dto
	 */
	TipoPendenciaSaidaDTO detalharTipoPendencia(TipoPendenciaEntradaDTO tipoPendenciaEntradaDTO);
	
	/**
	 * Descricao unidade organizacional.
	 *
	 * @param descricaoUnidOrgEntradaDTO the descricao unid org entrada dto
	 * @return the consultar descricao unid organizacional saida dto
	 */
	ConsultarDescricaoUnidOrganizacionalSaidaDTO descricaoUnidadeOrganizacional(ConsultarDescricaoUnidOrganizacionalEntradaDTO descricaoUnidOrgEntradaDTO);
	
}

