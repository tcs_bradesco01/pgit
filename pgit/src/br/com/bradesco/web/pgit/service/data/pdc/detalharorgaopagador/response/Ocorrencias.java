/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.detalharorgaopagador.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdOrgaoPagador
     */
    private int _cdOrgaoPagador = 0;

    /**
     * keeps track of state for field: _cdOrgaoPagador
     */
    private boolean _has_cdOrgaoPagador;

    /**
     * Field _cdTipoUnidade
     */
    private int _cdTipoUnidade = 0;

    /**
     * keeps track of state for field: _cdTipoUnidade
     */
    private boolean _has_cdTipoUnidade;

    /**
     * Field _dsTipoUnidade
     */
    private java.lang.String _dsTipoUnidade;

    /**
     * Field _cdPessoa
     */
    private long _cdPessoa = 0;

    /**
     * keeps track of state for field: _cdPessoa
     */
    private boolean _has_cdPessoa;

    /**
     * Field _dsPessoa
     */
    private java.lang.String _dsPessoa;

    /**
     * Field _nrSeqUnidadeOrganizacional
     */
    private int _nrSeqUnidadeOrganizacional = 0;

    /**
     * keeps track of state for field: _nrSeqUnidadeOrganizacional
     */
    private boolean _has_nrSeqUnidadeOrganizacional;

    /**
     * Field _dsSeqUnidadeOrganizacional
     */
    private java.lang.String _dsSeqUnidadeOrganizacional;

    /**
     * Field _cdSituacaoOrgaoPagador
     */
    private int _cdSituacaoOrgaoPagador = 0;

    /**
     * keeps track of state for field: _cdSituacaoOrgaoPagador
     */
    private boolean _has_cdSituacaoOrgaoPagador;

    /**
     * Field _cdTarifOrgaoPagador
     */
    private int _cdTarifOrgaoPagador = 0;

    /**
     * keeps track of state for field: _cdTarifOrgaoPagador
     */
    private boolean _has_cdTarifOrgaoPagador;

    /**
     * Field _cdCanalInclusao
     */
    private int _cdCanalInclusao = 0;

    /**
     * keeps track of state for field: _cdCanalInclusao
     */
    private boolean _has_cdCanalInclusao;

    /**
     * Field _dsCanalInclusao
     */
    private java.lang.String _dsCanalInclusao;

    /**
     * Field _cdAutenSegrcInclusao
     */
    private java.lang.String _cdAutenSegrcInclusao;

    /**
     * Field _nmOperFluxoInclusao
     */
    private java.lang.String _nmOperFluxoInclusao;

    /**
     * Field _hrInclusaoRegistro
     */
    private java.lang.String _hrInclusaoRegistro;

    /**
     * Field _cdCanalManutencao
     */
    private int _cdCanalManutencao = 0;

    /**
     * keeps track of state for field: _cdCanalManutencao
     */
    private boolean _has_cdCanalManutencao;

    /**
     * Field _dsCanalManutencao
     */
    private java.lang.String _dsCanalManutencao;

    /**
     * Field _cdAutenSegrcManutencao
     */
    private java.lang.String _cdAutenSegrcManutencao;

    /**
     * Field _nmOperFluxoManutencao
     */
    private java.lang.String _nmOperFluxoManutencao;

    /**
     * Field _hrManutencaoRegistro
     */
    private java.lang.String _hrManutencaoRegistro;

    /**
     * Field _dsMotivoBloqueio
     */
    private java.lang.String _dsMotivoBloqueio;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharorgaopagador.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdCanalInclusao
     * 
     */
    public void deleteCdCanalInclusao()
    {
        this._has_cdCanalInclusao= false;
    } //-- void deleteCdCanalInclusao() 

    /**
     * Method deleteCdCanalManutencao
     * 
     */
    public void deleteCdCanalManutencao()
    {
        this._has_cdCanalManutencao= false;
    } //-- void deleteCdCanalManutencao() 

    /**
     * Method deleteCdOrgaoPagador
     * 
     */
    public void deleteCdOrgaoPagador()
    {
        this._has_cdOrgaoPagador= false;
    } //-- void deleteCdOrgaoPagador() 

    /**
     * Method deleteCdPessoa
     * 
     */
    public void deleteCdPessoa()
    {
        this._has_cdPessoa= false;
    } //-- void deleteCdPessoa() 

    /**
     * Method deleteCdSituacaoOrgaoPagador
     * 
     */
    public void deleteCdSituacaoOrgaoPagador()
    {
        this._has_cdSituacaoOrgaoPagador= false;
    } //-- void deleteCdSituacaoOrgaoPagador() 

    /**
     * Method deleteCdTarifOrgaoPagador
     * 
     */
    public void deleteCdTarifOrgaoPagador()
    {
        this._has_cdTarifOrgaoPagador= false;
    } //-- void deleteCdTarifOrgaoPagador() 

    /**
     * Method deleteCdTipoUnidade
     * 
     */
    public void deleteCdTipoUnidade()
    {
        this._has_cdTipoUnidade= false;
    } //-- void deleteCdTipoUnidade() 

    /**
     * Method deleteNrSeqUnidadeOrganizacional
     * 
     */
    public void deleteNrSeqUnidadeOrganizacional()
    {
        this._has_nrSeqUnidadeOrganizacional= false;
    } //-- void deleteNrSeqUnidadeOrganizacional() 

    /**
     * Returns the value of field 'cdAutenSegrcInclusao'.
     * 
     * @return String
     * @return the value of field 'cdAutenSegrcInclusao'.
     */
    public java.lang.String getCdAutenSegrcInclusao()
    {
        return this._cdAutenSegrcInclusao;
    } //-- java.lang.String getCdAutenSegrcInclusao() 

    /**
     * Returns the value of field 'cdAutenSegrcManutencao'.
     * 
     * @return String
     * @return the value of field 'cdAutenSegrcManutencao'.
     */
    public java.lang.String getCdAutenSegrcManutencao()
    {
        return this._cdAutenSegrcManutencao;
    } //-- java.lang.String getCdAutenSegrcManutencao() 

    /**
     * Returns the value of field 'cdCanalInclusao'.
     * 
     * @return int
     * @return the value of field 'cdCanalInclusao'.
     */
    public int getCdCanalInclusao()
    {
        return this._cdCanalInclusao;
    } //-- int getCdCanalInclusao() 

    /**
     * Returns the value of field 'cdCanalManutencao'.
     * 
     * @return int
     * @return the value of field 'cdCanalManutencao'.
     */
    public int getCdCanalManutencao()
    {
        return this._cdCanalManutencao;
    } //-- int getCdCanalManutencao() 

    /**
     * Returns the value of field 'cdOrgaoPagador'.
     * 
     * @return int
     * @return the value of field 'cdOrgaoPagador'.
     */
    public int getCdOrgaoPagador()
    {
        return this._cdOrgaoPagador;
    } //-- int getCdOrgaoPagador() 

    /**
     * Returns the value of field 'cdPessoa'.
     * 
     * @return long
     * @return the value of field 'cdPessoa'.
     */
    public long getCdPessoa()
    {
        return this._cdPessoa;
    } //-- long getCdPessoa() 

    /**
     * Returns the value of field 'cdSituacaoOrgaoPagador'.
     * 
     * @return int
     * @return the value of field 'cdSituacaoOrgaoPagador'.
     */
    public int getCdSituacaoOrgaoPagador()
    {
        return this._cdSituacaoOrgaoPagador;
    } //-- int getCdSituacaoOrgaoPagador() 

    /**
     * Returns the value of field 'cdTarifOrgaoPagador'.
     * 
     * @return int
     * @return the value of field 'cdTarifOrgaoPagador'.
     */
    public int getCdTarifOrgaoPagador()
    {
        return this._cdTarifOrgaoPagador;
    } //-- int getCdTarifOrgaoPagador() 

    /**
     * Returns the value of field 'cdTipoUnidade'.
     * 
     * @return int
     * @return the value of field 'cdTipoUnidade'.
     */
    public int getCdTipoUnidade()
    {
        return this._cdTipoUnidade;
    } //-- int getCdTipoUnidade() 

    /**
     * Returns the value of field 'dsCanalInclusao'.
     * 
     * @return String
     * @return the value of field 'dsCanalInclusao'.
     */
    public java.lang.String getDsCanalInclusao()
    {
        return this._dsCanalInclusao;
    } //-- java.lang.String getDsCanalInclusao() 

    /**
     * Returns the value of field 'dsCanalManutencao'.
     * 
     * @return String
     * @return the value of field 'dsCanalManutencao'.
     */
    public java.lang.String getDsCanalManutencao()
    {
        return this._dsCanalManutencao;
    } //-- java.lang.String getDsCanalManutencao() 

    /**
     * Returns the value of field 'dsMotivoBloqueio'.
     * 
     * @return String
     * @return the value of field 'dsMotivoBloqueio'.
     */
    public java.lang.String getDsMotivoBloqueio()
    {
        return this._dsMotivoBloqueio;
    } //-- java.lang.String getDsMotivoBloqueio() 

    /**
     * Returns the value of field 'dsPessoa'.
     * 
     * @return String
     * @return the value of field 'dsPessoa'.
     */
    public java.lang.String getDsPessoa()
    {
        return this._dsPessoa;
    } //-- java.lang.String getDsPessoa() 

    /**
     * Returns the value of field 'dsSeqUnidadeOrganizacional'.
     * 
     * @return String
     * @return the value of field 'dsSeqUnidadeOrganizacional'.
     */
    public java.lang.String getDsSeqUnidadeOrganizacional()
    {
        return this._dsSeqUnidadeOrganizacional;
    } //-- java.lang.String getDsSeqUnidadeOrganizacional() 

    /**
     * Returns the value of field 'dsTipoUnidade'.
     * 
     * @return String
     * @return the value of field 'dsTipoUnidade'.
     */
    public java.lang.String getDsTipoUnidade()
    {
        return this._dsTipoUnidade;
    } //-- java.lang.String getDsTipoUnidade() 

    /**
     * Returns the value of field 'hrInclusaoRegistro'.
     * 
     * @return String
     * @return the value of field 'hrInclusaoRegistro'.
     */
    public java.lang.String getHrInclusaoRegistro()
    {
        return this._hrInclusaoRegistro;
    } //-- java.lang.String getHrInclusaoRegistro() 

    /**
     * Returns the value of field 'hrManutencaoRegistro'.
     * 
     * @return String
     * @return the value of field 'hrManutencaoRegistro'.
     */
    public java.lang.String getHrManutencaoRegistro()
    {
        return this._hrManutencaoRegistro;
    } //-- java.lang.String getHrManutencaoRegistro() 

    /**
     * Returns the value of field 'nmOperFluxoInclusao'.
     * 
     * @return String
     * @return the value of field 'nmOperFluxoInclusao'.
     */
    public java.lang.String getNmOperFluxoInclusao()
    {
        return this._nmOperFluxoInclusao;
    } //-- java.lang.String getNmOperFluxoInclusao() 

    /**
     * Returns the value of field 'nmOperFluxoManutencao'.
     * 
     * @return String
     * @return the value of field 'nmOperFluxoManutencao'.
     */
    public java.lang.String getNmOperFluxoManutencao()
    {
        return this._nmOperFluxoManutencao;
    } //-- java.lang.String getNmOperFluxoManutencao() 

    /**
     * Returns the value of field 'nrSeqUnidadeOrganizacional'.
     * 
     * @return int
     * @return the value of field 'nrSeqUnidadeOrganizacional'.
     */
    public int getNrSeqUnidadeOrganizacional()
    {
        return this._nrSeqUnidadeOrganizacional;
    } //-- int getNrSeqUnidadeOrganizacional() 

    /**
     * Method hasCdCanalInclusao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCanalInclusao()
    {
        return this._has_cdCanalInclusao;
    } //-- boolean hasCdCanalInclusao() 

    /**
     * Method hasCdCanalManutencao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCanalManutencao()
    {
        return this._has_cdCanalManutencao;
    } //-- boolean hasCdCanalManutencao() 

    /**
     * Method hasCdOrgaoPagador
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdOrgaoPagador()
    {
        return this._has_cdOrgaoPagador;
    } //-- boolean hasCdOrgaoPagador() 

    /**
     * Method hasCdPessoa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoa()
    {
        return this._has_cdPessoa;
    } //-- boolean hasCdPessoa() 

    /**
     * Method hasCdSituacaoOrgaoPagador
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSituacaoOrgaoPagador()
    {
        return this._has_cdSituacaoOrgaoPagador;
    } //-- boolean hasCdSituacaoOrgaoPagador() 

    /**
     * Method hasCdTarifOrgaoPagador
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTarifOrgaoPagador()
    {
        return this._has_cdTarifOrgaoPagador;
    } //-- boolean hasCdTarifOrgaoPagador() 

    /**
     * Method hasCdTipoUnidade
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoUnidade()
    {
        return this._has_cdTipoUnidade;
    } //-- boolean hasCdTipoUnidade() 

    /**
     * Method hasNrSeqUnidadeOrganizacional
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSeqUnidadeOrganizacional()
    {
        return this._has_nrSeqUnidadeOrganizacional;
    } //-- boolean hasNrSeqUnidadeOrganizacional() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdAutenSegrcInclusao'.
     * 
     * @param cdAutenSegrcInclusao the value of field
     * 'cdAutenSegrcInclusao'.
     */
    public void setCdAutenSegrcInclusao(java.lang.String cdAutenSegrcInclusao)
    {
        this._cdAutenSegrcInclusao = cdAutenSegrcInclusao;
    } //-- void setCdAutenSegrcInclusao(java.lang.String) 

    /**
     * Sets the value of field 'cdAutenSegrcManutencao'.
     * 
     * @param cdAutenSegrcManutencao the value of field
     * 'cdAutenSegrcManutencao'.
     */
    public void setCdAutenSegrcManutencao(java.lang.String cdAutenSegrcManutencao)
    {
        this._cdAutenSegrcManutencao = cdAutenSegrcManutencao;
    } //-- void setCdAutenSegrcManutencao(java.lang.String) 

    /**
     * Sets the value of field 'cdCanalInclusao'.
     * 
     * @param cdCanalInclusao the value of field 'cdCanalInclusao'.
     */
    public void setCdCanalInclusao(int cdCanalInclusao)
    {
        this._cdCanalInclusao = cdCanalInclusao;
        this._has_cdCanalInclusao = true;
    } //-- void setCdCanalInclusao(int) 

    /**
     * Sets the value of field 'cdCanalManutencao'.
     * 
     * @param cdCanalManutencao the value of field
     * 'cdCanalManutencao'.
     */
    public void setCdCanalManutencao(int cdCanalManutencao)
    {
        this._cdCanalManutencao = cdCanalManutencao;
        this._has_cdCanalManutencao = true;
    } //-- void setCdCanalManutencao(int) 

    /**
     * Sets the value of field 'cdOrgaoPagador'.
     * 
     * @param cdOrgaoPagador the value of field 'cdOrgaoPagador'.
     */
    public void setCdOrgaoPagador(int cdOrgaoPagador)
    {
        this._cdOrgaoPagador = cdOrgaoPagador;
        this._has_cdOrgaoPagador = true;
    } //-- void setCdOrgaoPagador(int) 

    /**
     * Sets the value of field 'cdPessoa'.
     * 
     * @param cdPessoa the value of field 'cdPessoa'.
     */
    public void setCdPessoa(long cdPessoa)
    {
        this._cdPessoa = cdPessoa;
        this._has_cdPessoa = true;
    } //-- void setCdPessoa(long) 

    /**
     * Sets the value of field 'cdSituacaoOrgaoPagador'.
     * 
     * @param cdSituacaoOrgaoPagador the value of field
     * 'cdSituacaoOrgaoPagador'.
     */
    public void setCdSituacaoOrgaoPagador(int cdSituacaoOrgaoPagador)
    {
        this._cdSituacaoOrgaoPagador = cdSituacaoOrgaoPagador;
        this._has_cdSituacaoOrgaoPagador = true;
    } //-- void setCdSituacaoOrgaoPagador(int) 

    /**
     * Sets the value of field 'cdTarifOrgaoPagador'.
     * 
     * @param cdTarifOrgaoPagador the value of field
     * 'cdTarifOrgaoPagador'.
     */
    public void setCdTarifOrgaoPagador(int cdTarifOrgaoPagador)
    {
        this._cdTarifOrgaoPagador = cdTarifOrgaoPagador;
        this._has_cdTarifOrgaoPagador = true;
    } //-- void setCdTarifOrgaoPagador(int) 

    /**
     * Sets the value of field 'cdTipoUnidade'.
     * 
     * @param cdTipoUnidade the value of field 'cdTipoUnidade'.
     */
    public void setCdTipoUnidade(int cdTipoUnidade)
    {
        this._cdTipoUnidade = cdTipoUnidade;
        this._has_cdTipoUnidade = true;
    } //-- void setCdTipoUnidade(int) 

    /**
     * Sets the value of field 'dsCanalInclusao'.
     * 
     * @param dsCanalInclusao the value of field 'dsCanalInclusao'.
     */
    public void setDsCanalInclusao(java.lang.String dsCanalInclusao)
    {
        this._dsCanalInclusao = dsCanalInclusao;
    } //-- void setDsCanalInclusao(java.lang.String) 

    /**
     * Sets the value of field 'dsCanalManutencao'.
     * 
     * @param dsCanalManutencao the value of field
     * 'dsCanalManutencao'.
     */
    public void setDsCanalManutencao(java.lang.String dsCanalManutencao)
    {
        this._dsCanalManutencao = dsCanalManutencao;
    } //-- void setDsCanalManutencao(java.lang.String) 

    /**
     * Sets the value of field 'dsMotivoBloqueio'.
     * 
     * @param dsMotivoBloqueio the value of field 'dsMotivoBloqueio'
     */
    public void setDsMotivoBloqueio(java.lang.String dsMotivoBloqueio)
    {
        this._dsMotivoBloqueio = dsMotivoBloqueio;
    } //-- void setDsMotivoBloqueio(java.lang.String) 

    /**
     * Sets the value of field 'dsPessoa'.
     * 
     * @param dsPessoa the value of field 'dsPessoa'.
     */
    public void setDsPessoa(java.lang.String dsPessoa)
    {
        this._dsPessoa = dsPessoa;
    } //-- void setDsPessoa(java.lang.String) 

    /**
     * Sets the value of field 'dsSeqUnidadeOrganizacional'.
     * 
     * @param dsSeqUnidadeOrganizacional the value of field
     * 'dsSeqUnidadeOrganizacional'.
     */
    public void setDsSeqUnidadeOrganizacional(java.lang.String dsSeqUnidadeOrganizacional)
    {
        this._dsSeqUnidadeOrganizacional = dsSeqUnidadeOrganizacional;
    } //-- void setDsSeqUnidadeOrganizacional(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoUnidade'.
     * 
     * @param dsTipoUnidade the value of field 'dsTipoUnidade'.
     */
    public void setDsTipoUnidade(java.lang.String dsTipoUnidade)
    {
        this._dsTipoUnidade = dsTipoUnidade;
    } //-- void setDsTipoUnidade(java.lang.String) 

    /**
     * Sets the value of field 'hrInclusaoRegistro'.
     * 
     * @param hrInclusaoRegistro the value of field
     * 'hrInclusaoRegistro'.
     */
    public void setHrInclusaoRegistro(java.lang.String hrInclusaoRegistro)
    {
        this._hrInclusaoRegistro = hrInclusaoRegistro;
    } //-- void setHrInclusaoRegistro(java.lang.String) 

    /**
     * Sets the value of field 'hrManutencaoRegistro'.
     * 
     * @param hrManutencaoRegistro the value of field
     * 'hrManutencaoRegistro'.
     */
    public void setHrManutencaoRegistro(java.lang.String hrManutencaoRegistro)
    {
        this._hrManutencaoRegistro = hrManutencaoRegistro;
    } //-- void setHrManutencaoRegistro(java.lang.String) 

    /**
     * Sets the value of field 'nmOperFluxoInclusao'.
     * 
     * @param nmOperFluxoInclusao the value of field
     * 'nmOperFluxoInclusao'.
     */
    public void setNmOperFluxoInclusao(java.lang.String nmOperFluxoInclusao)
    {
        this._nmOperFluxoInclusao = nmOperFluxoInclusao;
    } //-- void setNmOperFluxoInclusao(java.lang.String) 

    /**
     * Sets the value of field 'nmOperFluxoManutencao'.
     * 
     * @param nmOperFluxoManutencao the value of field
     * 'nmOperFluxoManutencao'.
     */
    public void setNmOperFluxoManutencao(java.lang.String nmOperFluxoManutencao)
    {
        this._nmOperFluxoManutencao = nmOperFluxoManutencao;
    } //-- void setNmOperFluxoManutencao(java.lang.String) 

    /**
     * Sets the value of field 'nrSeqUnidadeOrganizacional'.
     * 
     * @param nrSeqUnidadeOrganizacional the value of field
     * 'nrSeqUnidadeOrganizacional'.
     */
    public void setNrSeqUnidadeOrganizacional(int nrSeqUnidadeOrganizacional)
    {
        this._nrSeqUnidadeOrganizacional = nrSeqUnidadeOrganizacional;
        this._has_nrSeqUnidadeOrganizacional = true;
    } //-- void setNrSeqUnidadeOrganizacional(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.detalharorgaopagador.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.detalharorgaopagador.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.detalharorgaopagador.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharorgaopagador.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
