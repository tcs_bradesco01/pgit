/*
 * Nome: br.com.bradesco.web.pgit.service.business.combo.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.combo.bean;

/**
 * Nome: ListarOperacoesServicoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarOperacoesServicoSaidaDTO {
	
	/** Atributo cdOperacao. */
	private int cdOperacao;
	
	/** Atributo dsOperacao. */
	private String dsOperacao;
	
	/**
	 * Listar operacoes servico saida dto.
	 *
	 * @param cdOperacao the cd operacao
	 * @param dsOperacao the ds operacao
	 */
	public ListarOperacoesServicoSaidaDTO(Integer cdOperacao, String dsOperacao) {
		
		super();
		this.cdOperacao = cdOperacao;
		this.dsOperacao = dsOperacao;
	}
	
	/**
	 * Get: cdOperacao.
	 *
	 * @return cdOperacao
	 */
	public int getCdOperacao() {
		return cdOperacao;
	}
	
	/**
	 * Set: cdOperacao.
	 *
	 * @param cdOperacao the cd operacao
	 */
	public void setCdOperacao(int cdOperacao) {
		this.cdOperacao = cdOperacao;
	}
	
	/**
	 * Get: dsOperacao.
	 *
	 * @return dsOperacao
	 */
	public String getDsOperacao() {
		return dsOperacao;
	}
	
	/**
	 * Set: dsOperacao.
	 *
	 * @param dsOperacao the ds operacao
	 */
	public void setDsOperacao(String dsOperacao) {
		this.dsOperacao = dsOperacao;
	}

}
