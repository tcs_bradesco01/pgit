/*
 * Nome: br.com.bradesco.web.pgit.service.business.filtroidentificao.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.filtroidentificao.bean;

import br.com.bradesco.web.pgit.utils.CpfCnpjUtils;
import br.com.bradesco.web.pgit.utils.DateUtils;

/**
 * Nome: ListarClubesClientesSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarClubesClientesSaidaDTO {

	/** Atributo cdClub. */
	private Long cdClub;
	
	/** Atributo cdCpfCnpj. */
	private Long cdCpfCnpj;
	
	/** Atributo cdFilialCnpj. */
	private Integer cdFilialCnpj;
	
	/** Atributo cdControleCnpj. */
	private Integer cdControleCnpj;
	
	/** Atributo dsNomeRazao. */
	private String dsNomeRazao;
	
	/** Atributo cdSegmentoCliente. */
	private Integer cdSegmentoCliente;
	
	/** Atributo dsSegmentoCliente. */
	private String dsSegmentoCliente;
	
	/** Atributo cdSubSegmentoCliente. */
	private Integer cdSubSegmentoCliente;
	
	/** Atributo dsSubSegmentoCliente. */
	private String dsSubSegmentoCliente;
	
	/** Atributo cdAtividadeEconomica. */
	private Integer cdAtividadeEconomica;
	
	/** Atributo dsAtividadeEconomica. */
	private String dsAtividadeEconomica;
	
	/** Atributo cdGrupoEconomico. */
	private Long cdGrupoEconomico;
	
	/** Atributo dsGrupoEconomico. */
	private String dsGrupoEconomico;
	
	/** Atributo cnpjOuCpfFormatado. */
	private String cnpjOuCpfFormatado;
	
	/** Atributo dtNascimentoFundacao. */
	private String dtNascimentoFundacao = null;
	
	/** Atributo dtNascimentoFundacaoFormatado. */
	private String dtNascimentoFundacaoFormatado;
	
	/** Atributo check. */
	private boolean check;

	/**
	 * Listar clubes clientes saida dto.
	 */
	public ListarClubesClientesSaidaDTO() {
		super();
	}

	/**
	 * Listar clubes clientes saida dto.
	 *
	 * @param saida the saida
	 */
	public ListarClubesClientesSaidaDTO(br.com.bradesco.web.pgit.service.data.pdc.listarclubesclientes.response.Ocorrencias saida) {
		super();
		this.cdClub = saida.getCdClub();
		this.cdCpfCnpj = saida.getCdCorpoCpfCnpj();
		this.cdFilialCnpj = saida.getCdFilialCnpj();
		this.cdControleCnpj = saida.getCdControleCpfCnpj();
		this.dsNomeRazao = saida.getDsNomeRazao();
		this.cdSegmentoCliente = saida.getCdSegmento();
		this.dsSegmentoCliente = saida.getDsSegmento();
		this.cdSubSegmentoCliente = saida.getCdSubSegmento();
		this.dsSubSegmentoCliente = saida.getDsSubSegmento();
		this.cdAtividadeEconomica = saida.getCdAtividadeEconomica();
		this.dsAtividadeEconomica = saida.getDtAtividadeEconomica();
		this.cdGrupoEconomico = saida.getCdGrupoEconomico();
		this.dsGrupoEconomico = saida.getDsGrupoEconomico();
		this.cnpjOuCpfFormatado = CpfCnpjUtils.formatCpfCnpjCompleto(cdCpfCnpj, cdFilialCnpj, cdControleCnpj); // m�todo alterado para ficar no formato 000.000.000/0000-00 ou 000.000.000-00
		this.dtNascimentoFundacao = saida.getDtNascimentoFundacao();
		this.dtNascimentoFundacaoFormatado = DateUtils.formartarDataPDCparaPadraPtBr(dtNascimentoFundacao, "dd.MM.yyyy", "dd/MM/yyyy");
	}
	
	/**
	 * Get: dtNascimentoFundacao.
	 *
	 * @return dtNascimentoFundacao
	 */
	public String getDtNascimentoFundacao() {
		return dtNascimentoFundacao;
	}

	/**
	 * Set: dtNascimentoFundacao.
	 *
	 * @param dtNascimentoFundacao the dt nascimento fundacao
	 */
	public void setDtNascimentoFundacao(String dtNascimentoFundacao) {
		this.dtNascimentoFundacao = dtNascimentoFundacao;
	}

	/**
	 * Get: cdAtividadeEconomica.
	 *
	 * @return cdAtividadeEconomica
	 */
	public Integer getCdAtividadeEconomica() {
		return cdAtividadeEconomica;
	}

	/**
	 * Set: cdAtividadeEconomica.
	 *
	 * @param cdAtividadeEconomica the cd atividade economica
	 */
	public void setCdAtividadeEconomica(Integer cdAtividadeEconomica) {
		this.cdAtividadeEconomica = cdAtividadeEconomica;
	}

	/**
	 * Get: cdClub.
	 *
	 * @return cdClub
	 */
	public Long getCdClub() {
		return cdClub;
	}

	/**
	 * Set: cdClub.
	 *
	 * @param cdClub the cd club
	 */
	public void setCdClub(Long cdClub) {
		this.cdClub = cdClub;
	}

	/**
	 * Get: cdControleCnpj.
	 *
	 * @return cdControleCnpj
	 */
	public Integer getCdControleCnpj() {
		return cdControleCnpj;
	}

	/**
	 * Set: cdControleCnpj.
	 *
	 * @param cdControleCnpj the cd controle cnpj
	 */
	public void setCdControleCnpj(Integer cdControleCnpj) {
		this.cdControleCnpj = cdControleCnpj;
	}

	/**
	 * Get: cdCpfCnpj.
	 *
	 * @return cdCpfCnpj
	 */
	public Long getCdCpfCnpj() {
		return cdCpfCnpj;
	}

	/**
	 * Set: cdCpfCnpj.
	 *
	 * @param cdCpfCnpj the cd cpf cnpj
	 */
	public void setCdCpfCnpj(Long cdCpfCnpj) {
		this.cdCpfCnpj = cdCpfCnpj;
	}

	/**
	 * Get: cdFilialCnpj.
	 *
	 * @return cdFilialCnpj
	 */
	public Integer getCdFilialCnpj() {
		return cdFilialCnpj;
	}

	/**
	 * Set: cdFilialCnpj.
	 *
	 * @param cdFilialCnpj the cd filial cnpj
	 */
	public void setCdFilialCnpj(Integer cdFilialCnpj) {
		this.cdFilialCnpj = cdFilialCnpj;
	}

	/**
	 * Get: cdGrupoEconomico.
	 *
	 * @return cdGrupoEconomico
	 */
	public Long getCdGrupoEconomico() {
		return cdGrupoEconomico;
	}

	/**
	 * Set: cdGrupoEconomico.
	 *
	 * @param cdGrupoEconomico the cd grupo economico
	 */
	public void setCdGrupoEconomico(Long cdGrupoEconomico) {
		this.cdGrupoEconomico = cdGrupoEconomico;
	}

	/**
	 * Get: cdSegmentoCliente.
	 *
	 * @return cdSegmentoCliente
	 */
	public Integer getCdSegmentoCliente() {
		return cdSegmentoCliente;
	}

	/**
	 * Set: cdSegmentoCliente.
	 *
	 * @param cdSegmentoCliente the cd segmento cliente
	 */
	public void setCdSegmentoCliente(Integer cdSegmentoCliente) {
		this.cdSegmentoCliente = cdSegmentoCliente;
	}

	/**
	 * Get: cdSubSegmentoCliente.
	 *
	 * @return cdSubSegmentoCliente
	 */
	public Integer getCdSubSegmentoCliente() {
		return cdSubSegmentoCliente;
	}

	/**
	 * Set: cdSubSegmentoCliente.
	 *
	 * @param cdSubSegmentoCliente the cd sub segmento cliente
	 */
	public void setCdSubSegmentoCliente(Integer cdSubSegmentoCliente) {
		this.cdSubSegmentoCliente = cdSubSegmentoCliente;
	}

	/**
	 * Get: dsAtividadeEconomica.
	 *
	 * @return dsAtividadeEconomica
	 */
	public String getDsAtividadeEconomica() {
		return dsAtividadeEconomica;
	}

	/**
	 * Set: dsAtividadeEconomica.
	 *
	 * @param dsAtividadeEconomica the ds atividade economica
	 */
	public void setDsAtividadeEconomica(String dsAtividadeEconomica) {
		this.dsAtividadeEconomica = dsAtividadeEconomica;
	}

	/**
	 * Get: dsGrupoEconomico.
	 *
	 * @return dsGrupoEconomico
	 */
	public String getDsGrupoEconomico() {
		return dsGrupoEconomico;
	}

	/**
	 * Set: dsGrupoEconomico.
	 *
	 * @param dsGrupoEconomico the ds grupo economico
	 */
	public void setDsGrupoEconomico(String dsGrupoEconomico) {
		this.dsGrupoEconomico = dsGrupoEconomico;
	}

	/**
	 * Get: dsNomeRazao.
	 *
	 * @return dsNomeRazao
	 */
	public String getDsNomeRazao() {
		return dsNomeRazao;
	}

	/**
	 * Set: dsNomeRazao.
	 *
	 * @param dsNomeRazao the ds nome razao
	 */
	public void setDsNomeRazao(String dsNomeRazao) {
		this.dsNomeRazao = dsNomeRazao;
	}

	/**
	 * Get: dsSegmentoCliente.
	 *
	 * @return dsSegmentoCliente
	 */
	public String getDsSegmentoCliente() {
		return dsSegmentoCliente;
	}

	/**
	 * Set: dsSegmentoCliente.
	 *
	 * @param dsSegmentoCliente the ds segmento cliente
	 */
	public void setDsSegmentoCliente(String dsSegmentoCliente) {
		this.dsSegmentoCliente = dsSegmentoCliente;
	}

	/**
	 * Get: dsSubSegmentoCliente.
	 *
	 * @return dsSubSegmentoCliente
	 */
	public String getDsSubSegmentoCliente() {
		return dsSubSegmentoCliente;
	}

	/**
	 * Set: dsSubSegmentoCliente.
	 *
	 * @param dsSubSegmentoCliente the ds sub segmento cliente
	 */
	public void setDsSubSegmentoCliente(String dsSubSegmentoCliente) {
		this.dsSubSegmentoCliente = dsSubSegmentoCliente;
	}

	/**
	 * Get: cnpjOuCpfFormatado.
	 *
	 * @return cnpjOuCpfFormatado
	 */
	public String getCnpjOuCpfFormatado() {
		return CpfCnpjUtils.formatCpfCnpjCompleto(cdCpfCnpj, cdFilialCnpj, cdControleCnpj);
	}

	/**
	 * Set: cnpjOuCpfFormatado.
	 *
	 * @param cnpjOuCpfFormatado the cnpj ou cpf formatado
	 */
	public void setCnpjOuCpfFormatado(String cnpjOuCpfFormatado) {
		this.cnpjOuCpfFormatado = cnpjOuCpfFormatado;
	}

	/**
	 * Is check.
	 *
	 * @return true, if is check
	 */
	public boolean isCheck() {
		return check;
	}

	/**
	 * Set: check.
	 *
	 * @param check the check
	 */
	public void setCheck(boolean check) {
		this.check = check;
	}

	/**
	 * Get: dtNascimentoFundacaoFormatado.
	 *
	 * @return dtNascimentoFundacaoFormatado
	 */
	public String getDtNascimentoFundacaoFormatado() {
		return DateUtils.formartarDataPDCparaPadraPtBr(dtNascimentoFundacao, "dd.MM.yyyy", "dd/MM/yyyy");
	}

	/**
	 * Set: dtNascimentoFundacaoFormatado.
	 *
	 * @param dtNascimentoFundacaoFormatado the dt nascimento fundacao formatado
	 */
	public void setDtNascimentoFundacaoFormatado(
			String dtNascimentoFundacaoFormatado) {
		this.dtNascimentoFundacaoFormatado = dtNascimentoFundacaoFormatado;
	}
}