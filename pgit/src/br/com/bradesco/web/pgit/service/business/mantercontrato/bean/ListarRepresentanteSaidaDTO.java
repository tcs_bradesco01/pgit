package br.com.bradesco.web.pgit.service.business.mantercontrato.bean;

import java.util.ArrayList;
import java.util.List;

import br.com.bradesco.web.pgit.service.data.pdc.listarrepresentante.response.ListarRepresentanteResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarrepresentante.response.Ocorrencias;
import br.com.bradesco.web.pgit.utils.CpfCnpjUtils;

/**
 * Nome: ListarRepresentanteSaidaDTO
 * <p>
 * Propůsito:
 * </p>
 * .
 * 
 * @author : todo!
 * @version :
 */
public class ListarRepresentanteSaidaDTO {

	private String codMensagem;

	private String mensagem;

	private Integer nrLinhas;

	private List<ListarRepresentanteOcorrenciaDTO> ocorrencias;

	public ListarRepresentanteSaidaDTO() {
		this.ocorrencias = new ArrayList<ListarRepresentanteOcorrenciaDTO>();
	}

	public ListarRepresentanteSaidaDTO(ListarRepresentanteResponse response) {
		this.ocorrencias = new ArrayList<ListarRepresentanteOcorrenciaDTO>();
		this.codMensagem = response.getCodMensagem();
		this.mensagem = response.getMensagem();
		this.nrLinhas = response.getNrLinhas();
		
		for(Ocorrencias ocorr : response.getOcorrencias()) {
			ListarRepresentanteOcorrenciaDTO ocorrenciaDTO = new ListarRepresentanteOcorrenciaDTO(ocorr.getCdPessoa(), ocorr.getCdTipoParticipacaoPessoa(), 
					ocorr.getCdCPF(), ocorr.getCdFilialCnpj(), ocorr.getCdControleCnpj(), 
					ocorr.getDsNomeRazao(), "", null, null, null);
			this.ocorrencias.add(ocorrenciaDTO);
		}
	}
	/**
	 * @return the codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}

	/**
	 * @param codMensagem
	 *            the codMensagem to set
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}

	/**
	 * @return the mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}

	/**
	 * @param mensagem
	 *            the mensagem to set
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	/**
	 * @return the nrLinhas
	 */
	public Integer getNrLinhas() {
		return nrLinhas;
	}

	/**
	 * @param nrLinhas
	 *            the nrLinhas to set
	 */
	public void setNrLinhas(Integer nrLinhas) {
		this.nrLinhas = nrLinhas;
	}

	/**
	 * @return the ocorrencias
	 */
	public List<ListarRepresentanteOcorrenciaDTO> getOcorrencias() {
		return ocorrencias;
	}

	/**
	 * @param ocorrencias
	 *            the ocorrencias to set
	 */
	public void setOcorrencias(
			List<ListarRepresentanteOcorrenciaDTO> ocorrencias) {
		this.ocorrencias = ocorrencias;
	}

}