/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias5.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias5 implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _dsProdutoSalarioAutoAtendimento
     */
    private java.lang.String _dsProdutoSalarioAutoAtendimento;

    /**
     * Field _dsServicoSalarioAutoAtendimento
     */
    private java.lang.String _dsServicoSalarioAutoAtendimento;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias5() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias5()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Returns the value of field
     * 'dsProdutoSalarioAutoAtendimento'.
     * 
     * @return String
     * @return the value of field 'dsProdutoSalarioAutoAtendimento'.
     */
    public java.lang.String getDsProdutoSalarioAutoAtendimento()
    {
        return this._dsProdutoSalarioAutoAtendimento;
    } //-- java.lang.String getDsProdutoSalarioAutoAtendimento() 

    /**
     * Returns the value of field
     * 'dsServicoSalarioAutoAtendimento'.
     * 
     * @return String
     * @return the value of field 'dsServicoSalarioAutoAtendimento'.
     */
    public java.lang.String getDsServicoSalarioAutoAtendimento()
    {
        return this._dsServicoSalarioAutoAtendimento;
    } //-- java.lang.String getDsServicoSalarioAutoAtendimento() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'dsProdutoSalarioAutoAtendimento'.
     * 
     * @param dsProdutoSalarioAutoAtendimento the value of field
     * 'dsProdutoSalarioAutoAtendimento'.
     */
    public void setDsProdutoSalarioAutoAtendimento(java.lang.String dsProdutoSalarioAutoAtendimento)
    {
        this._dsProdutoSalarioAutoAtendimento = dsProdutoSalarioAutoAtendimento;
    } //-- void setDsProdutoSalarioAutoAtendimento(java.lang.String) 

    /**
     * Sets the value of field 'dsServicoSalarioAutoAtendimento'.
     * 
     * @param dsServicoSalarioAutoAtendimento the value of field
     * 'dsServicoSalarioAutoAtendimento'.
     */
    public void setDsServicoSalarioAutoAtendimento(java.lang.String dsServicoSalarioAutoAtendimento)
    {
        this._dsServicoSalarioAutoAtendimento = dsServicoSalarioAutoAtendimento;
    } //-- void setDsServicoSalarioAutoAtendimento(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias5
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias5 unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias5) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias5.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias5 unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
