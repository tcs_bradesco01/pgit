/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantercadastroacomp.impl
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantercadastroacomp.impl;

import java.util.ArrayList;

import br.com.bradesco.web.pgit.service.business.mantercadastroacomp.IManterCadastroEmpresaAcompService;
import br.com.bradesco.web.pgit.service.business.mantercadastroacomp.IManterCadastroEmpresaAcompServiceConstants;
import br.com.bradesco.web.pgit.service.business.mantercadastroacomp.bean.ConsultarEmpresaAcompanhadaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadastroacomp.bean.ConsultarEmpresaAcompanhadaListaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadastroacomp.bean.ConsultarEmpresaAcompanhadaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadastroacomp.bean.ManterEmpresaAcompanhadaEntradaDTO;
import br.com.bradesco.web.pgit.service.data.pdc.FactoryAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.alterarempresaacompanhada.request.AlterarEmpresaAcompanhadaRequest;
import br.com.bradesco.web.pgit.service.data.pdc.alterarempresaacompanhada.response.AlterarEmpresaAcompanhadaResponse;
import br.com.bradesco.web.pgit.service.data.pdc.consistirdadoslogicos.request.ConsistirDadosLogicosRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consistirdadoslogicos.response.ConsistirDadosLogicosResponse;
import br.com.bradesco.web.pgit.service.data.pdc.consultarempresaacompanhada.request.ConsultarEmpresaAcompanhadaRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarempresaacompanhada.response.ConsultarEmpresaAcompanhadaResponse;
import br.com.bradesco.web.pgit.service.data.pdc.excluirempresaacompanhada.request.ExcluirEmpresaAcompanhadaRequest;
import br.com.bradesco.web.pgit.service.data.pdc.excluirempresaacompanhada.response.ExcluirEmpresaAcompanhadaResponse;
import br.com.bradesco.web.pgit.service.data.pdc.incluirempresaacompanhada.request.IncluirEmpresaAcompanhadaRequest;
import br.com.bradesco.web.pgit.service.data.pdc.incluirempresaacompanhada.response.IncluirEmpresaAcompanhadaResponse;

/**
 * Nome: ManterCadastroEmpresaAcompServiceImpl
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ManterCadastroEmpresaAcompServiceImpl implements IManterCadastroEmpresaAcompService {

	/** Atributo factoryAdapter. */
	private FactoryAdapter factoryAdapter;

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.mantercadastroacomp.IManterCadastroEmpresaAcompService#consultarEmpresaAcompanhada(br.com.bradesco.web.pgit.service.business.mantercadastroacomp.bean.ConsultarEmpresaAcompanhadaEntradaDTO)
	 */
	public ConsultarEmpresaAcompanhadaSaidaDTO consultarEmpresaAcompanhada(ConsultarEmpresaAcompanhadaEntradaDTO entradaDTO) {

		ConsultarEmpresaAcompanhadaSaidaDTO saidaDTO = new ConsultarEmpresaAcompanhadaSaidaDTO();
		ConsultarEmpresaAcompanhadaRequest request = new ConsultarEmpresaAcompanhadaRequest();
		ConsultarEmpresaAcompanhadaResponse response = new ConsultarEmpresaAcompanhadaResponse();

		request.setMaxOcorrencias(IManterCadastroEmpresaAcompServiceConstants.QTDE_CONSULTAS_LISTAR);
		request.setCdIndicadorPesquisa(entradaDTO.getCdIndicadorPesquisa());
		request.setCdCnpjCpf(entradaDTO.getCdCnpjCpf());
		request.setCdFilialCnpjCpf(entradaDTO.getCdFilialCnpjCpf());
		request.setCdCtrlCnpjCPf(entradaDTO.getCdCtrlCnpjCPf());
		request.setDsRazaoSocial(entradaDTO.getDsRazaoSocial());
		request.setCdJuncaoAgencia(entradaDTO.getCdJuncaoAgencia());
		request.setCdContaCorrente(entradaDTO.getCdContaCorrente());
		request.setCdDigitoConta(entradaDTO.getCdDigitoConta());

		response = getFactoryAdapter().getConsultarEmpresaAcompanhadaPDCAdapter().invokeProcess(request);

		saidaDTO = new ConsultarEmpresaAcompanhadaSaidaDTO();

		saidaDTO.setCodMensagem(response.getCodMensagem());
		saidaDTO.setMensagem(response.getMensagem());
		saidaDTO.setNrOcorrencias(response.getNrOcorrencias());

		saidaDTO.setOcorrencias(new ArrayList<ConsultarEmpresaAcompanhadaListaDTO>());
		ConsultarEmpresaAcompanhadaListaDTO lista;
		for (int i = 0; i < response.getNrOcorrencias(); i++) {
			lista = new ConsultarEmpresaAcompanhadaListaDTO();
			
			lista.setCdCnpjCpf(response.getOcorrencias(i).getCdCnpjCpf());
			lista.setCdFilialCpfCnpj(response.getOcorrencias(i).getCdFilialCpfCnpj());
			lista.setCdCtrlCpfCnpjSaida(response.getOcorrencias(i).getCdCtrlCpfCnpjSaida());
			lista.setDsRazaoSocial(response.getOcorrencias(i).getDsRazaoSocial());
			lista.setCdAgenciaSaida(response.getOcorrencias(i).getCdAgenciaSaida());
			lista.setCdContaCorrenteSaida(response.getOcorrencias(i).getCdContaCorrenteSaida());
			lista.setCdDigitoConta(response.getOcorrencias(i).getCdDigitoConta());
			lista.setCdConveCtaSalarial(response.getOcorrencias(i).getCdConveCtaSalarial());
			lista.setCdIndicadorPrevidencia(response.getOcorrencias(i).getCdIndicadorPrevidencia());
			lista.setDtInclusao(response.getOcorrencias(i).getDtInclusao());
			lista.setHrInclusao(response.getOcorrencias(i).getHrInclusao());
			lista.setCdUsuarioInclusao(response.getOcorrencias(i).getCdUsuarioInclusao());
			lista.setCdUsuarioInclusaoExterno(response.getOcorrencias(i).getCdUsuarioInclusaoExterno());
			lista.setCdOperacaoCanalInclusao(response.getOcorrencias(i).getCdOperacaoCanalInclusao());
			lista.setDsTipoCanalInclusao(response.getOcorrencias(i).getDsTipoCanalInclusao());
			lista.setDtManutencao(response.getOcorrencias(i).getDtManutencao());
			lista.setHrManutencao(response.getOcorrencias(i).getHrManutencao());
			lista.setCdUsuarioManutencao(response.getOcorrencias(i).getCdUsuarioManutencao());
			lista.setCdUsuarioManutencaoexterno(response.getOcorrencias(i).getCdUsuarioManutencaoexterno());
			lista.setCdOperacaoCanalManutencao(response.getOcorrencias(i).getCdOperacaoCanalManutencao());
			lista.setDsTipoCanalManutencao(response.getOcorrencias(i).getDsTipoCanalManutencao());
			lista.setDsComplementoInclusao(response.getOcorrencias(i).getDsComplementoInclusao());
			lista.setDsComplementoManutencao(response.getOcorrencias(i).getDsComplementoManutencao());
			
			saidaDTO.getOcorrencias().add(lista);
		}

		return saidaDTO;

	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.mantercadastroacomp.IManterCadastroEmpresaAcompService#consistirDadosLogicos(br.com.bradesco.web.pgit.service.business.mantercadastroacomp.bean.ManterEmpresaAcompanhadaEntradaDTO)
	 */
	public String consistirDadosLogicos(ManterEmpresaAcompanhadaEntradaDTO entrada) {
		ConsistirDadosLogicosRequest request = new ConsistirDadosLogicosRequest();
		ConsistirDadosLogicosResponse response = new ConsistirDadosLogicosResponse();

		request.setCdCnpjCpf(entrada.getCdCnpjCpf());
		request.setCdFilialCnpjCpf(entrada.getCdFilialCnpjCpf());
		request.setCdCtrlCnpjCPf(entrada.getCdCtrlCnpjCPf());
		request.setDsRazaoSocial(entrada.getDsRazaoSocial());
		request.setCdJuncaoAgencia(entrada.getCdJuncaoAgencia());
		request.setCdContaCorrente(entrada.getCdContaCorrente());
		request.setCdDigitoConta(entrada.getCdDigitoConta());
		request.setCdConveCtaSalarial(entrada.getCdConveCtaSalarial());
		request.setCdIndicadorPrevidencia(entrada.getCdIndicadorPrevidencia());

		response = getFactoryAdapter().getConsistirDadosLogicosPDCAdapter().invokeProcess(request);

		return response.getCodMensagem().trim() + " - " + response.getMensagem().trim();
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.mantercadastroacomp.IManterCadastroEmpresaAcompService#incluirEmpresaAcompanhada(br.com.bradesco.web.pgit.service.business.mantercadastroacomp.bean.ManterEmpresaAcompanhadaEntradaDTO)
	 */
	public String incluirEmpresaAcompanhada(ManterEmpresaAcompanhadaEntradaDTO entradaDTO) {
		IncluirEmpresaAcompanhadaRequest request = new IncluirEmpresaAcompanhadaRequest();
		IncluirEmpresaAcompanhadaResponse response = new IncluirEmpresaAcompanhadaResponse();
		
		request.setCdCnpjCpf(entradaDTO.getCdCnpjCpf());
		request.setCdFilialCnpjCpf(entradaDTO.getCdFilialCnpjCpf());
		request.setCdCtrlCnpjCPf(entradaDTO.getCdCtrlCnpjCPf());
		request.setDsRazaoSocial(entradaDTO.getDsRazaoSocial());
		request.setCdJuncaoAgencia(entradaDTO.getCdJuncaoAgencia());
		request.setCdContaCorrente(entradaDTO.getCdContaCorrente());
		request.setCdDigitoConta(entradaDTO.getCdDigitoConta());
		request.setCdConveCtaSalarial(entradaDTO.getCdConveCtaSalarial());
		request.setCdIndicadorPrevidencia(entradaDTO.getCdIndicadorPrevidencia());
		
		response = getFactoryAdapter().getIncluirEmpresaAcompanhadaPDCAdapter().invokeProcess(request);

		return response.getCodMensagem().trim() + " - " + response.getMensagem().trim();
	}
	
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.mantercadastroacomp.IManterCadastroEmpresaAcompService#alterarEmpresaAcompanhada(br.com.bradesco.web.pgit.service.business.mantercadastroacomp.bean.ManterEmpresaAcompanhadaEntradaDTO)
	 */
	public String alterarEmpresaAcompanhada(ManterEmpresaAcompanhadaEntradaDTO entradaDTO) {
		AlterarEmpresaAcompanhadaRequest request = new AlterarEmpresaAcompanhadaRequest();
		AlterarEmpresaAcompanhadaResponse response = new AlterarEmpresaAcompanhadaResponse();
		
		request.setCdCnpjCpf(entradaDTO.getCdCnpjCpf());
		request.setCdFilialCnpjCpf(entradaDTO.getCdFilialCnpjCpf());
		request.setCdCtrlCnpjCPf(entradaDTO.getCdCtrlCnpjCPf());
		request.setDsRazaoSocial(entradaDTO.getDsRazaoSocial());
		request.setCdJuncaoAgencia(entradaDTO.getCdJuncaoAgencia());
		request.setCdContaCorrente(entradaDTO.getCdContaCorrente());
		request.setCdDigitoConta(entradaDTO.getCdDigitoConta());
		request.setCdConveCtaSalarial(entradaDTO.getCdConveCtaSalarial());
		request.setCdIndicadorPrevidencia(entradaDTO.getCdIndicadorPrevidencia());
		
		response = getFactoryAdapter().getAlterarEmpresaAcompanhadaPDCAdapter().invokeProcess(request);
		
		return response.getCodMensagem().trim() + " - " + response.getMensagem().trim();
	}
	
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.mantercadastroacomp.IManterCadastroEmpresaAcompService#excluirEmpresaAcompanhada(br.com.bradesco.web.pgit.service.business.mantercadastroacomp.bean.ManterEmpresaAcompanhadaEntradaDTO)
	 */
	public String excluirEmpresaAcompanhada(ManterEmpresaAcompanhadaEntradaDTO entradaDTO) {
		ExcluirEmpresaAcompanhadaRequest request = new ExcluirEmpresaAcompanhadaRequest();
		ExcluirEmpresaAcompanhadaResponse response = new ExcluirEmpresaAcompanhadaResponse();
		
		request.setCdCnpjCpf(entradaDTO.getCdCnpjCpf());
		request.setCdFilialCnpjCpf(entradaDTO.getCdFilialCnpjCpf());
		request.setCdCtrlCnpjCPf(entradaDTO.getCdCtrlCnpjCPf());
		
		response = getFactoryAdapter().getExcluirEmpresaAcompanhadaPDCAdapter().invokeProcess(request);
		
		return response.getCodMensagem().trim() + " - " + response.getMensagem().trim();
	}

	/**
	 * Get: factoryAdapter.
	 *
	 * @return factoryAdapter
	 */
	public FactoryAdapter getFactoryAdapter() {
		return factoryAdapter;
	}

	/**
	 * Set: factoryAdapter.
	 *
	 * @param factoryAdapter the factory adapter
	 */
	public void setFactoryAdapter(FactoryAdapter factoryAdapter) {
		this.factoryAdapter = factoryAdapter;
	}
}