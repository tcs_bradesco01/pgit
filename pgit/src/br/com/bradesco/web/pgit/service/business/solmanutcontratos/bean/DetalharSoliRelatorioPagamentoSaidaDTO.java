/*
 * Nome: br.com.bradesco.web.pgit.service.business.solmanutcontratos.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.solmanutcontratos.bean;

import br.com.bradesco.web.pgit.utils.CpfCnpjUtils;

/**
 * Nome: DetalharSoliRelatorioPagamentoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class DetalharSoliRelatorioPagamentoSaidaDTO {
	
	/** Atributo codMensagem. */
	private String codMensagem;
    
    /** Atributo mensagem. */
    private String mensagem;
    
    /** Atributo cdpessoaJuridicaContrato. */
    private Long cdpessoaJuridicaContrato;
    
    /** Atributo cdTipoContratoNegocio. */
    private Integer cdTipoContratoNegocio;
    
    /** Atributo nrSequenciaContratoNegocio. */
    private Long nrSequenciaContratoNegocio;
    
    /** Atributo cdTipoRelatorio. */
    private Integer cdTipoRelatorio;
    
    /** Atributo dsTipoRelatorio. */
    private String dsTipoRelatorio;
    
    /** Atributo cdProdutoServicoOperaca. */
    private Integer cdProdutoServicoOperaca;
    
    /** Atributo dsProdutoServicoOperacao. */
    private String dsProdutoServicoOperacao;
    
    /** Atributo cdProdutoOperacaoRelacionado. */
    private Integer cdProdutoOperacaoRelacionado;
    
    /** Atributo dsProdutoOperRelacionado. */
    private String dsProdutoOperRelacionado;
    
    /** Atributo cdRelacionamentoProduto. */
    private Integer cdRelacionamentoProduto;
    
    /** Atributo cdCpfCnpjParticipante. */
    private Long cdCpfCnpjParticipante;
    
    /** Atributo cdCflialCnpjParticipante. */
    private Integer cdCflialCnpjParticipante;
    
    /** Atributo cdCctrlCpfParticipante. */
    private Integer cdCctrlCpfParticipante;
    
    /** Atributo dsNomeParticipante. */
    private String dsNomeParticipante;
    
    /** Atributo cdAgenciaDebito. */
    private Integer cdAgenciaDebito;
    
    /** Atributo dsNomeAgenciaDebito. */
    private String dsNomeAgenciaDebito;
    
    /** Atributo cdContaDebito. */
    private Long cdContaDebito;
    
    /** Atributo cdDigitoContaDebito. */
    private String cdDigitoContaDebito;
    
    /** Atributo dtInicioPerMovimentacao. */
    private String dtInicioPerMovimentacao;
    
    /** Atributo dsFimPerMovimentacao. */
    private String dsFimPerMovimentacao;
    
    /** Atributo dtInclusao. */
    private String dtInclusao; 
    
    /** Atributo hrInclusao. */
    private String hrInclusao; 
    
    /** Atributo cdUsuarioInclusao. */
    private String cdUsuarioInclusao;
    
    /** Atributo cdUsuarioInclusaoExterno. */
    private String cdUsuarioInclusaoExterno;
    
    /** Atributo cdOperacaoCanalInclusao. */
    private String cdOperacaoCanalInclusao;
    
    /** Atributo cdTipoCanalInclusao. */
    private Integer cdTipoCanalInclusao;
    
    /** Atributo dsCanalInclusao. */
    private String dsCanalInclusao;
    
    /** Atributo cdUsuarioManutencao. */
    private String cdUsuarioManutencao;
    
    /** Atributo cdUsuarioManutencaoExterno. */
    private String cdUsuarioManutencaoExterno;
    
    /** Atributo dtManutencao. */
    private String dtManutencao;
    
    /** Atributo hrManutencao. */
    private String hrManutencao;
    
    /** Atributo cdOperacaoCanalManutencao. */
    private String cdOperacaoCanalManutencao;
    
    /** Atributo cdTipoCanalManutencao. */
    private Integer cdTipoCanalManutencao;
    
    /** Atributo dsCanalManutencao. */
    private String  dsCanalManutencao;
    
    /** Atributo contaDigito. */
    private String contaDigito;
    
    /** Atributo codAgenciaDesc. */
    private String codAgenciaDesc;
    
    /** Atributo dataHoraInclusao. */
    private String dataHoraInclusao;
    
    /** Atributo dataHoraManut. */
    private String dataHoraManut;
    
	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}
	
	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	/**
	 * Get: cdpessoaJuridicaContrato.
	 *
	 * @return cdpessoaJuridicaContrato
	 */
	public Long getCdpessoaJuridicaContrato() {
		return cdpessoaJuridicaContrato;
	}
	
	/**
	 * Set: cdpessoaJuridicaContrato.
	 *
	 * @param cdpessoaJuridicaContrato the cdpessoa juridica contrato
	 */
	public void setCdpessoaJuridicaContrato(Long cdpessoaJuridicaContrato) {
		this.cdpessoaJuridicaContrato = cdpessoaJuridicaContrato;
	}
	
	/**
	 * Get: cdTipoContratoNegocio.
	 *
	 * @return cdTipoContratoNegocio
	 */
	public Integer getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}
	
	/**
	 * Set: cdTipoContratoNegocio.
	 *
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}
	
	/**
	 * Get: nrSequenciaContratoNegocio.
	 *
	 * @return nrSequenciaContratoNegocio
	 */
	public Long getNrSequenciaContratoNegocio() {
		return nrSequenciaContratoNegocio;
	}
	
	/**
	 * Set: nrSequenciaContratoNegocio.
	 *
	 * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
	 */
	public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}
	
	/**
	 * Get: cdTipoRelatorio.
	 *
	 * @return cdTipoRelatorio
	 */
	public Integer getCdTipoRelatorio() {
		return cdTipoRelatorio;
	}
	
	/**
	 * Set: cdTipoRelatorio.
	 *
	 * @param cdTipoRelatorio the cd tipo relatorio
	 */
	public void setCdTipoRelatorio(Integer cdTipoRelatorio) {
		this.cdTipoRelatorio = cdTipoRelatorio;
	}
	
	/**
	 * Get: dsTipoRelatorio.
	 *
	 * @return dsTipoRelatorio
	 */
	public String getDsTipoRelatorio() {
		return dsTipoRelatorio;
	}
	
	/**
	 * Set: dsTipoRelatorio.
	 *
	 * @param dsTipoRelatorio the ds tipo relatorio
	 */
	public void setDsTipoRelatorio(String dsTipoRelatorio) {
		this.dsTipoRelatorio = dsTipoRelatorio;
	}
	
	/**
	 * Get: cdProdutoServicoOperaca.
	 *
	 * @return cdProdutoServicoOperaca
	 */
	public Integer getCdProdutoServicoOperaca() {
		return cdProdutoServicoOperaca;
	}
	
	/**
	 * Set: cdProdutoServicoOperaca.
	 *
	 * @param cdProdutoServicoOperaca the cd produto servico operaca
	 */
	public void setCdProdutoServicoOperaca(Integer cdProdutoServicoOperaca) {
		this.cdProdutoServicoOperaca = cdProdutoServicoOperaca;
	}
	
	/**
	 * Get: dsProdutoServicoOperacao.
	 *
	 * @return dsProdutoServicoOperacao
	 */
	public String getDsProdutoServicoOperacao() {
		return dsProdutoServicoOperacao;
	}
	
	/**
	 * Set: dsProdutoServicoOperacao.
	 *
	 * @param dsProdutoServicoOperacao the ds produto servico operacao
	 */
	public void setDsProdutoServicoOperacao(String dsProdutoServicoOperacao) {
		this.dsProdutoServicoOperacao = dsProdutoServicoOperacao;
	}
	
	/**
	 * Get: cdProdutoOperacaoRelacionado.
	 *
	 * @return cdProdutoOperacaoRelacionado
	 */
	public Integer getCdProdutoOperacaoRelacionado() {
		return cdProdutoOperacaoRelacionado;
	}
	
	/**
	 * Set: cdProdutoOperacaoRelacionado.
	 *
	 * @param cdProdutoOperacaoRelacionado the cd produto operacao relacionado
	 */
	public void setCdProdutoOperacaoRelacionado(Integer cdProdutoOperacaoRelacionado) {
		this.cdProdutoOperacaoRelacionado = cdProdutoOperacaoRelacionado;
	}
	
	/**
	 * Get: dsProdutoOperRelacionado.
	 *
	 * @return dsProdutoOperRelacionado
	 */
	public String getDsProdutoOperRelacionado() {
		return dsProdutoOperRelacionado;
	}
	
	/**
	 * Set: dsProdutoOperRelacionado.
	 *
	 * @param dsProdutoOperRelacionado the ds produto oper relacionado
	 */
	public void setDsProdutoOperRelacionado(String dsProdutoOperRelacionado) {
		this.dsProdutoOperRelacionado = dsProdutoOperRelacionado;
	}
	
	/**
	 * Get: cdRelacionamentoProduto.
	 *
	 * @return cdRelacionamentoProduto
	 */
	public Integer getCdRelacionamentoProduto() {
		return cdRelacionamentoProduto;
	}
	
	/**
	 * Set: cdRelacionamentoProduto.
	 *
	 * @param cdRelacionamentoProduto the cd relacionamento produto
	 */
	public void setCdRelacionamentoProduto(Integer cdRelacionamentoProduto) {
		this.cdRelacionamentoProduto = cdRelacionamentoProduto;
	}
	
	/**
	 * Get: cdCpfCnpjParticipante.
	 *
	 * @return cdCpfCnpjParticipante
	 */
	public Long getCdCpfCnpjParticipante() {
		return cdCpfCnpjParticipante;
	}
	
	/**
	 * Set: cdCpfCnpjParticipante.
	 *
	 * @param cdCpfCnpjParticipante the cd cpf cnpj participante
	 */
	public void setCdCpfCnpjParticipante(Long cdCpfCnpjParticipante) {
		this.cdCpfCnpjParticipante = cdCpfCnpjParticipante;
	}
	
	/**
	 * Get: cdCflialCnpjParticipante.
	 *
	 * @return cdCflialCnpjParticipante
	 */
	public Integer getCdCflialCnpjParticipante() {
		return cdCflialCnpjParticipante;
	}
	
	/**
	 * Set: cdCflialCnpjParticipante.
	 *
	 * @param cdCflialCnpjParticipante the cd cflial cnpj participante
	 */
	public void setCdCflialCnpjParticipante(Integer cdCflialCnpjParticipante) {
		this.cdCflialCnpjParticipante = cdCflialCnpjParticipante;
	}
	
	/**
	 * Get: cdCctrlCpfParticipante.
	 *
	 * @return cdCctrlCpfParticipante
	 */
	public Integer getCdCctrlCpfParticipante() {
		return cdCctrlCpfParticipante;
	}
	
	/**
	 * Set: cdCctrlCpfParticipante.
	 *
	 * @param cdCctrlCpfParticipante the cd cctrl cpf participante
	 */
	public void setCdCctrlCpfParticipante(Integer cdCctrlCpfParticipante) {
		this.cdCctrlCpfParticipante = cdCctrlCpfParticipante;
	}
	
	/**
	 * Get: dsNomeParticipante.
	 *
	 * @return dsNomeParticipante
	 */
	public String getDsNomeParticipante() {
		return dsNomeParticipante;
	}
	
	/**
	 * Set: dsNomeParticipante.
	 *
	 * @param dsNomeParticipante the ds nome participante
	 */
	public void setDsNomeParticipante(String dsNomeParticipante) {
		this.dsNomeParticipante = dsNomeParticipante;
	}
	
	/**
	 * Get: cdAgenciaDebito.
	 *
	 * @return cdAgenciaDebito
	 */
	public Integer getCdAgenciaDebito() {
		return cdAgenciaDebito;
	}
	
	/**
	 * Set: cdAgenciaDebito.
	 *
	 * @param cdAgenciaDebito the cd agencia debito
	 */
	public void setCdAgenciaDebito(Integer cdAgenciaDebito) {
		this.cdAgenciaDebito = cdAgenciaDebito;
	}
	
	/**
	 * Get: dsNomeAgenciaDebito.
	 *
	 * @return dsNomeAgenciaDebito
	 */
	public String getDsNomeAgenciaDebito() {
		return dsNomeAgenciaDebito;
	}
	
	/**
	 * Set: dsNomeAgenciaDebito.
	 *
	 * @param dsNomeAgenciaDebito the ds nome agencia debito
	 */
	public void setDsNomeAgenciaDebito(String dsNomeAgenciaDebito) {
		this.dsNomeAgenciaDebito = dsNomeAgenciaDebito;
	}
	
	/**
	 * Get: cdContaDebito.
	 *
	 * @return cdContaDebito
	 */
	public Long getCdContaDebito() {
		return cdContaDebito;
	}
	
	/**
	 * Set: cdContaDebito.
	 *
	 * @param cdContaDebito the cd conta debito
	 */
	public void setCdContaDebito(Long cdContaDebito) {
		this.cdContaDebito = cdContaDebito;
	}
	
	/**
	 * Get: cdDigitoContaDebito.
	 *
	 * @return cdDigitoContaDebito
	 */
	public String getCdDigitoContaDebito() {
		return cdDigitoContaDebito;
	}
	
	/**
	 * Set: cdDigitoContaDebito.
	 *
	 * @param cdDigitoContaDebito the cd digito conta debito
	 */
	public void setCdDigitoContaDebito(String cdDigitoContaDebito) {
		this.cdDigitoContaDebito = cdDigitoContaDebito;
	}
	
	/**
	 * Get: dtInicioPerMovimentacao.
	 *
	 * @return dtInicioPerMovimentacao
	 */
	public String getDtInicioPerMovimentacao() {
		return dtInicioPerMovimentacao;
	}
	
	/**
	 * Set: dtInicioPerMovimentacao.
	 *
	 * @param dtInicioPerMovimentacao the dt inicio per movimentacao
	 */
	public void setDtInicioPerMovimentacao(String dtInicioPerMovimentacao) {
		this.dtInicioPerMovimentacao = dtInicioPerMovimentacao;
	}
	
	/**
	 * Get: dsFimPerMovimentacao.
	 *
	 * @return dsFimPerMovimentacao
	 */
	public String getDsFimPerMovimentacao() {
		return dsFimPerMovimentacao;
	}
	
	/**
	 * Set: dsFimPerMovimentacao.
	 *
	 * @param dsFimPerMovimentacao the ds fim per movimentacao
	 */
	public void setDsFimPerMovimentacao(String dsFimPerMovimentacao) {
		this.dsFimPerMovimentacao = dsFimPerMovimentacao;
	}
	
	/**
	 * Get: dtInclusao.
	 *
	 * @return dtInclusao
	 */
	public String getDtInclusao() {
		return dtInclusao;
	}
	
	/**
	 * Set: dtInclusao.
	 *
	 * @param dtInclusao the dt inclusao
	 */
	public void setDtInclusao(String dtInclusao) {
		this.dtInclusao = dtInclusao;
	}
	
	/**
	 * Get: hrInclusao.
	 *
	 * @return hrInclusao
	 */
	public String getHrInclusao() {
		return hrInclusao;
	}
	
	/**
	 * Set: hrInclusao.
	 *
	 * @param hrInclusao the hr inclusao
	 */
	public void setHrInclusao(String hrInclusao) {
		this.hrInclusao = hrInclusao;
	}
	
	/**
	 * Get: cdUsuarioInclusao.
	 *
	 * @return cdUsuarioInclusao
	 */
	public String getCdUsuarioInclusao() {
		return cdUsuarioInclusao;
	}
	
	/**
	 * Set: cdUsuarioInclusao.
	 *
	 * @param cdUsuarioInclusao the cd usuario inclusao
	 */
	public void setCdUsuarioInclusao(String cdUsuarioInclusao) {
		this.cdUsuarioInclusao = cdUsuarioInclusao;
	}
	
	/**
	 * Get: cdUsuarioInclusaoExterno.
	 *
	 * @return cdUsuarioInclusaoExterno
	 */
	public String getCdUsuarioInclusaoExterno() {
		return cdUsuarioInclusaoExterno;
	}
	
	/**
	 * Set: cdUsuarioInclusaoExterno.
	 *
	 * @param cdUsuarioInclusaoExterno the cd usuario inclusao externo
	 */
	public void setCdUsuarioInclusaoExterno(String cdUsuarioInclusaoExterno) {
		this.cdUsuarioInclusaoExterno = cdUsuarioInclusaoExterno;
	}
	
	/**
	 * Get: cdOperacaoCanalInclusao.
	 *
	 * @return cdOperacaoCanalInclusao
	 */
	public String getCdOperacaoCanalInclusao() {
		return cdOperacaoCanalInclusao;
	}
	
	/**
	 * Set: cdOperacaoCanalInclusao.
	 *
	 * @param cdOperacaoCanalInclusao the cd operacao canal inclusao
	 */
	public void setCdOperacaoCanalInclusao(String cdOperacaoCanalInclusao) {
		this.cdOperacaoCanalInclusao = cdOperacaoCanalInclusao;
	}
	
	/**
	 * Get: cdTipoCanalInclusao.
	 *
	 * @return cdTipoCanalInclusao
	 */
	public Integer getCdTipoCanalInclusao() {
		return cdTipoCanalInclusao;
	}
	
	/**
	 * Set: cdTipoCanalInclusao.
	 *
	 * @param cdTipoCanalInclusao the cd tipo canal inclusao
	 */
	public void setCdTipoCanalInclusao(Integer cdTipoCanalInclusao) {
		this.cdTipoCanalInclusao = cdTipoCanalInclusao;
	}
	
	/**
	 * Get: dsCanalInclusao.
	 *
	 * @return dsCanalInclusao
	 */
	public String getDsCanalInclusao() {
		return dsCanalInclusao;
	}
	
	/**
	 * Set: dsCanalInclusao.
	 *
	 * @param dsCanalInclusao the ds canal inclusao
	 */
	public void setDsCanalInclusao(String dsCanalInclusao) {
		this.dsCanalInclusao = dsCanalInclusao;
	}
	
	/**
	 * Get: cdUsuarioManutencao.
	 *
	 * @return cdUsuarioManutencao
	 */
	public String getCdUsuarioManutencao() {
		return cdUsuarioManutencao;
	}
	
	/**
	 * Set: cdUsuarioManutencao.
	 *
	 * @param cdUsuarioManutencao the cd usuario manutencao
	 */
	public void setCdUsuarioManutencao(String cdUsuarioManutencao) {
		this.cdUsuarioManutencao = cdUsuarioManutencao;
	}
	
	/**
	 * Get: cdUsuarioManutencaoExterno.
	 *
	 * @return cdUsuarioManutencaoExterno
	 */
	public String getCdUsuarioManutencaoExterno() {
		return cdUsuarioManutencaoExterno;
	}
	
	/**
	 * Set: cdUsuarioManutencaoExterno.
	 *
	 * @param cdUsuarioManutencaoExterno the cd usuario manutencao externo
	 */
	public void setCdUsuarioManutencaoExterno(String cdUsuarioManutencaoExterno) {
		this.cdUsuarioManutencaoExterno = cdUsuarioManutencaoExterno;
	}
	
	/**
	 * Get: dtManutencao.
	 *
	 * @return dtManutencao
	 */
	public String getDtManutencao() {
		return dtManutencao;
	}
	
	/**
	 * Set: dtManutencao.
	 *
	 * @param dtManutencao the dt manutencao
	 */
	public void setDtManutencao(String dtManutencao) {
		this.dtManutencao = dtManutencao;
	}
	
	/**
	 * Get: hrManutencao.
	 *
	 * @return hrManutencao
	 */
	public String getHrManutencao() {
		return hrManutencao;
	}
	
	/**
	 * Set: hrManutencao.
	 *
	 * @param hrManutencao the hr manutencao
	 */
	public void setHrManutencao(String hrManutencao) {
		this.hrManutencao = hrManutencao;
	}
	
	/**
	 * Get: cdOperacaoCanalManutencao.
	 *
	 * @return cdOperacaoCanalManutencao
	 */
	public String getCdOperacaoCanalManutencao() {
		return cdOperacaoCanalManutencao;
	}
	
	/**
	 * Set: cdOperacaoCanalManutencao.
	 *
	 * @param cdOperacaoCanalManutencao the cd operacao canal manutencao
	 */
	public void setCdOperacaoCanalManutencao(String cdOperacaoCanalManutencao) {
		this.cdOperacaoCanalManutencao = cdOperacaoCanalManutencao;
	}
	
	/**
	 * Get: cdTipoCanalManutencao.
	 *
	 * @return cdTipoCanalManutencao
	 */
	public Integer getCdTipoCanalManutencao() {
		return cdTipoCanalManutencao;
	}
	
	/**
	 * Set: cdTipoCanalManutencao.
	 *
	 * @param cdTipoCanalManutencao the cd tipo canal manutencao
	 */
	public void setCdTipoCanalManutencao(Integer cdTipoCanalManutencao) {
		this.cdTipoCanalManutencao = cdTipoCanalManutencao;
	}
	
	/**
	 * Get: dsCanalManutencao.
	 *
	 * @return dsCanalManutencao
	 */
	public String getDsCanalManutencao() {
		return dsCanalManutencao;
	}
	
	/**
	 * Set: dsCanalManutencao.
	 *
	 * @param dsCanalManutencao the ds canal manutencao
	 */
	public void setDsCanalManutencao(String dsCanalManutencao) {
		this.dsCanalManutencao = dsCanalManutencao;
	}
	
	/**
	 * Get: contaDigito.
	 *
	 * @return contaDigito
	 */
	public String getContaDigito() {
		return contaDigito;
	}
	
	/**
	 * Set: contaDigito.
	 *
	 * @param contaDigito the conta digito
	 */
	public void setContaDigito(String contaDigito) {
		this.contaDigito = contaDigito;
	}
	
	/**
	 * Get: contratoCompleto.
	 *
	 * @return contratoCompleto
	 */
	public String getContratoCompleto() {
		StringBuilder  contratoCompleto = new StringBuilder();
		contratoCompleto.append(cdpessoaJuridicaContrato);
		contratoCompleto.append(cdTipoContratoNegocio);
		contratoCompleto.append(nrSequenciaContratoNegocio);
		return contratoCompleto.toString();
	}
	
	/**
	 * Get: cpfCnpjFormatado.
	 *
	 * @return cpfCnpjFormatado
	 */
	public String getCpfCnpjFormatado() {
		  if(cdCpfCnpjParticipante != null && cdCpfCnpjParticipante != 0 ){
				return CpfCnpjUtils.formatarCpfCnpj(cdCpfCnpjParticipante, cdCflialCnpjParticipante, cdCctrlCpfParticipante);
		  }else{
			  return null;
		  }
	}
	
	/**
	 * Get: codAgenciaDesc.
	 *
	 * @return codAgenciaDesc
	 */
	public String getCodAgenciaDesc() {
		return codAgenciaDesc;
	}
	
	/**
	 * Set: codAgenciaDesc.
	 *
	 * @param codAgenciaDesc the cod agencia desc
	 */
	public void setCodAgenciaDesc(String codAgenciaDesc) {
		this.codAgenciaDesc = codAgenciaDesc;
	}
	
	/**
	 * Get: dataHoraInclusao.
	 *
	 * @return dataHoraInclusao
	 */
	public String getDataHoraInclusao() {
		return dataHoraInclusao;
	}
	
	/**
	 * Set: dataHoraInclusao.
	 *
	 * @param dataHoraInclusao the data hora inclusao
	 */
	public void setDataHoraInclusao(String dataHoraInclusao) {
		this.dataHoraInclusao = dataHoraInclusao;
	}
	
	/**
	 * Get: dataHoraManut.
	 *
	 * @return dataHoraManut
	 */
	public String getDataHoraManut() {
		return dataHoraManut;
	}
	
	/**
	 * Set: dataHoraManut.
	 *
	 * @param dataHoraManut the data hora manut
	 */
	public void setDataHoraManut(String dataHoraManut) {
		this.dataHoraManut = dataHoraManut;
	}

}
