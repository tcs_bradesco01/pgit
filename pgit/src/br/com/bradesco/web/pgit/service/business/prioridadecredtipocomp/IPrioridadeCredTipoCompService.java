/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/interfaz.ftl,v $
 * $Id: interfaz.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: interfaz.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.prioridadecredtipocomp;

import java.util.List;

import br.com.bradesco.web.pgit.service.business.prioridadecredtipocomp.bean.AlterarPrioridadeCredTipoCompEntradaDTO;
import br.com.bradesco.web.pgit.service.business.prioridadecredtipocomp.bean.AlterarPrioridadeCredTipoCompSaidaDTO;
import br.com.bradesco.web.pgit.service.business.prioridadecredtipocomp.bean.DetalharPrioridadeCredTipoCompEntradaDTO;
import br.com.bradesco.web.pgit.service.business.prioridadecredtipocomp.bean.DetalharPrioridadeCredTipoCompSaidaDTO;
import br.com.bradesco.web.pgit.service.business.prioridadecredtipocomp.bean.ExcluirPrioridadeCredTipoCompEntradaDTO;
import br.com.bradesco.web.pgit.service.business.prioridadecredtipocomp.bean.ExcluirPrioridadeCredTipoCompSaidaDTO;
import br.com.bradesco.web.pgit.service.business.prioridadecredtipocomp.bean.IncluirPrioridadeCredTipoCompEntradaDTO;
import br.com.bradesco.web.pgit.service.business.prioridadecredtipocomp.bean.IncluirPrioridadeCredTipoCompSaidaDTO;
import br.com.bradesco.web.pgit.service.business.prioridadecredtipocomp.bean.ListarPrioridadeCredTipoCompEntradaDTO;
import br.com.bradesco.web.pgit.service.business.prioridadecredtipocomp.bean.ListarPrioridadeCredTipoCompSaidaDTO;



/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Interface do adaptador: PrioridadeCredTipoComp
 * </p>
 * 
 * @comment CODIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public interface IPrioridadeCredTipoCompService {
	
	
	/**
	 * Listar prioridade cred tipo comp.
	 *
	 * @param listarPrioridadeCredTipoCompEntradaDTO the listar prioridade cred tipo comp entrada dto
	 * @return the list< listar prioridade cred tipo comp saida dt o>
	 */
	List<ListarPrioridadeCredTipoCompSaidaDTO> listarPrioridadeCredTipoComp(ListarPrioridadeCredTipoCompEntradaDTO listarPrioridadeCredTipoCompEntradaDTO);
	
	/**
	 * Incluir prioridade cred tipo comp.
	 *
	 * @param incluirPrioridadeCredTipoCompEntradaDTO the incluir prioridade cred tipo comp entrada dto
	 * @return the incluir prioridade cred tipo comp saida dto
	 */
	IncluirPrioridadeCredTipoCompSaidaDTO incluirPrioridadeCredTipoComp(IncluirPrioridadeCredTipoCompEntradaDTO incluirPrioridadeCredTipoCompEntradaDTO);
	
	/**
	 * Excluir prioridade cred tipo comp.
	 *
	 * @param excluirPrioridadeCredTipoCompEntradaDTO the excluir prioridade cred tipo comp entrada dto
	 * @return the excluir prioridade cred tipo comp saida dto
	 */
	ExcluirPrioridadeCredTipoCompSaidaDTO excluirPrioridadeCredTipoComp(ExcluirPrioridadeCredTipoCompEntradaDTO excluirPrioridadeCredTipoCompEntradaDTO);
	
	/**
	 * Detalhar prioridade cred tipo comp.
	 *
	 * @param detalharPrioridadeCredTipoCompEntradaDTO the detalhar prioridade cred tipo comp entrada dto
	 * @return the detalhar prioridade cred tipo comp saida dto
	 */
	DetalharPrioridadeCredTipoCompSaidaDTO detalharPrioridadeCredTipoComp(DetalharPrioridadeCredTipoCompEntradaDTO detalharPrioridadeCredTipoCompEntradaDTO);
	
	/**
	 * Alterar prioridade cred tipo comp saida dto.
	 *
	 * @param alterarPrioridadeCredTipoCompEntradaDTO the alterar prioridade cred tipo comp entrada dto
	 * @return the alterar prioridade cred tipo comp saida dto
	 */
	AlterarPrioridadeCredTipoCompSaidaDTO alterarPrioridadeCredTipoCompSaidaDTO(AlterarPrioridadeCredTipoCompEntradaDTO alterarPrioridadeCredTipoCompEntradaDTO);
	
	

}

