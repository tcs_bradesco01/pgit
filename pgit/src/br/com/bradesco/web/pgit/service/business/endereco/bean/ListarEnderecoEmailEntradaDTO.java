/*
 * Nome: br.com.bradesco.web.pgit.service.business.endereco.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.endereco.bean;


/**
 * Nome: ListarEnderecoEmailEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarEnderecoEmailEntradaDTO {

    /** Atributo nrOcorrencias. */
    private Integer nrOcorrencias;

    /** Atributo cdCorpoCpfCnpj. */
    private Long cdCorpoCpfCnpj;

    /** Atributo cdCnpjContrato. */
    private Integer cdCnpjContrato;

    /** Atributo cdCpfCnpjDigito. */
    private Integer cdCpfCnpjDigito;

    /** Atributo cdTipoPessoa. */
    private String cdTipoPessoa;

    /** Atributo cdClub. */
    private Long cdClub;

    /** Atributo cdEmpresaContrato. */
    private Long cdEmpresaContrato;

    /** Atributo cdEspecieEndereco. */
    private Integer cdEspecieEndereco;

    /**
     * Set: nrOcorrencias.
     *
     * @param nrOcorrencias the nr ocorrencias
     */
    public void setNrOcorrencias(Integer nrOcorrencias) {
        this.nrOcorrencias = nrOcorrencias;
    }

    /**
     * Get: nrOcorrencias.
     *
     * @return nrOcorrencias
     */
    public Integer getNrOcorrencias() {
        return this.nrOcorrencias;
    }

    /**
     * Set: cdCorpoCpfCnpj.
     *
     * @param cdCorpoCpfCnpj the cd corpo cpf cnpj
     */
    public void setCdCorpoCpfCnpj(Long cdCorpoCpfCnpj) {
        this.cdCorpoCpfCnpj = cdCorpoCpfCnpj;
    }

    /**
     * Get: cdCorpoCpfCnpj.
     *
     * @return cdCorpoCpfCnpj
     */
    public Long getCdCorpoCpfCnpj() {
        return this.cdCorpoCpfCnpj;
    }

    /**
     * Set: cdCnpjContrato.
     *
     * @param cdCnpjContrato the cd cnpj contrato
     */
    public void setCdCnpjContrato(Integer cdCnpjContrato) {
        this.cdCnpjContrato = cdCnpjContrato;
    }

    /**
     * Get: cdCnpjContrato.
     *
     * @return cdCnpjContrato
     */
    public Integer getCdCnpjContrato() {
        return this.cdCnpjContrato;
    }

    /**
     * Set: cdCpfCnpjDigito.
     *
     * @param cdCpfCnpjDigito the cd cpf cnpj digito
     */
    public void setCdCpfCnpjDigito(Integer cdCpfCnpjDigito) {
        this.cdCpfCnpjDigito = cdCpfCnpjDigito;
    }

    /**
     * Get: cdCpfCnpjDigito.
     *
     * @return cdCpfCnpjDigito
     */
    public Integer getCdCpfCnpjDigito() {
        return this.cdCpfCnpjDigito;
    }

    /**
     * Set: cdTipoPessoa.
     *
     * @param cdTipoPessoa the cd tipo pessoa
     */
    public void setCdTipoPessoa(String cdTipoPessoa) {
        this.cdTipoPessoa = cdTipoPessoa;
    }

    /**
     * Get: cdTipoPessoa.
     *
     * @return cdTipoPessoa
     */
    public String getCdTipoPessoa() {
        return this.cdTipoPessoa;
    }

    /**
     * Set: cdClub.
     *
     * @param cdClub the cd club
     */
    public void setCdClub(Long cdClub) {
        this.cdClub = cdClub;
    }

    /**
     * Get: cdClub.
     *
     * @return cdClub
     */
    public Long getCdClub() {
        return this.cdClub;
    }

    /**
     * Set: cdEmpresaContrato.
     *
     * @param cdEmpresaContrato the cd empresa contrato
     */
    public void setCdEmpresaContrato(Long cdEmpresaContrato) {
        this.cdEmpresaContrato = cdEmpresaContrato;
    }

    /**
     * Get: cdEmpresaContrato.
     *
     * @return cdEmpresaContrato
     */
    public Long getCdEmpresaContrato() {
        return this.cdEmpresaContrato;
    }

    /**
     * Set: cdEspecieEndereco.
     *
     * @param cdEspecieEndereco the cd especie endereco
     */
    public void setCdEspecieEndereco(Integer cdEspecieEndereco) {
        this.cdEspecieEndereco = cdEspecieEndereco;
    }

    /**
     * Get: cdEspecieEndereco.
     *
     * @return cdEspecieEndereco
     */
    public Integer getCdEspecieEndereco() {
        return this.cdEspecieEndereco;
    }
}