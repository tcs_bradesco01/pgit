/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias2.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias2 implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdIndicadorModalidadeSalario
     */
    private int _cdIndicadorModalidadeSalario = 0;

    /**
     * keeps track of state for field: _cdIndicadorModalidadeSalario
     */
    private boolean _has_cdIndicadorModalidadeSalario;

    /**
     * Field _cdIndicadorSalarioContrato
     */
    private java.lang.String _cdIndicadorSalarioContrato;

    /**
     * Field _cdProdutoRelacionadoSalario
     */
    private int _cdProdutoRelacionadoSalario = 0;

    /**
     * keeps track of state for field: _cdProdutoRelacionadoSalario
     */
    private boolean _has_cdProdutoRelacionadoSalario;

    /**
     * Field _dsProdutoRelacionadoSalario
     */
    private java.lang.String _dsProdutoRelacionadoSalario;

    /**
     * Field _cdMomeProcessamentoSalario
     */
    private int _cdMomeProcessamentoSalario = 0;

    /**
     * keeps track of state for field: _cdMomeProcessamentoSalario
     */
    private boolean _has_cdMomeProcessamentoSalario;

    /**
     * Field _dsMomeProcessamentoSalario
     */
    private java.lang.String _dsMomeProcessamentoSalario;

    /**
     * Field _cdPagamentoUtilSalario
     */
    private int _cdPagamentoUtilSalario = 0;

    /**
     * keeps track of state for field: _cdPagamentoUtilSalario
     */
    private boolean _has_cdPagamentoUtilSalario;

    /**
     * Field _dsPagamentoUtilSalario
     */
    private java.lang.String _dsPagamentoUtilSalario;

    /**
     * Field _nrQuantidadeDiaFLoatSalario
     */
    private int _nrQuantidadeDiaFLoatSalario = 0;

    /**
     * keeps track of state for field: _nrQuantidadeDiaFLoatSalario
     */
    private boolean _has_nrQuantidadeDiaFLoatSalario;

    /**
     * Field _cdIndicadorFeriadoSalario
     */
    private int _cdIndicadorFeriadoSalario = 0;

    /**
     * keeps track of state for field: _cdIndicadorFeriadoSalario
     */
    private boolean _has_cdIndicadorFeriadoSalario;

    /**
     * Field _dsIndicadorFeriadoSalario
     */
    private java.lang.String _dsIndicadorFeriadoSalario;

    /**
     * Field _cdIndicadorRepiqueSalario
     */
    private java.lang.String _cdIndicadorRepiqueSalario;

    /**
     * Field _nrQuantidadeRepiqueSalario
     */
    private int _nrQuantidadeRepiqueSalario = 0;

    /**
     * keeps track of state for field: _nrQuantidadeRepiqueSalario
     */
    private boolean _has_nrQuantidadeRepiqueSalario;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias2() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias2()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdIndicadorFeriadoSalario
     * 
     */
    public void deleteCdIndicadorFeriadoSalario()
    {
        this._has_cdIndicadorFeriadoSalario= false;
    } //-- void deleteCdIndicadorFeriadoSalario() 

    /**
     * Method deleteCdIndicadorModalidadeSalario
     * 
     */
    public void deleteCdIndicadorModalidadeSalario()
    {
        this._has_cdIndicadorModalidadeSalario= false;
    } //-- void deleteCdIndicadorModalidadeSalario() 

    /**
     * Method deleteCdMomeProcessamentoSalario
     * 
     */
    public void deleteCdMomeProcessamentoSalario()
    {
        this._has_cdMomeProcessamentoSalario= false;
    } //-- void deleteCdMomeProcessamentoSalario() 

    /**
     * Method deleteCdPagamentoUtilSalario
     * 
     */
    public void deleteCdPagamentoUtilSalario()
    {
        this._has_cdPagamentoUtilSalario= false;
    } //-- void deleteCdPagamentoUtilSalario() 

    /**
     * Method deleteCdProdutoRelacionadoSalario
     * 
     */
    public void deleteCdProdutoRelacionadoSalario()
    {
        this._has_cdProdutoRelacionadoSalario= false;
    } //-- void deleteCdProdutoRelacionadoSalario() 

    /**
     * Method deleteNrQuantidadeDiaFLoatSalario
     * 
     */
    public void deleteNrQuantidadeDiaFLoatSalario()
    {
        this._has_nrQuantidadeDiaFLoatSalario= false;
    } //-- void deleteNrQuantidadeDiaFLoatSalario() 

    /**
     * Method deleteNrQuantidadeRepiqueSalario
     * 
     */
    public void deleteNrQuantidadeRepiqueSalario()
    {
        this._has_nrQuantidadeRepiqueSalario= false;
    } //-- void deleteNrQuantidadeRepiqueSalario() 

    /**
     * Returns the value of field 'cdIndicadorFeriadoSalario'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorFeriadoSalario'.
     */
    public int getCdIndicadorFeriadoSalario()
    {
        return this._cdIndicadorFeriadoSalario;
    } //-- int getCdIndicadorFeriadoSalario() 

    /**
     * Returns the value of field 'cdIndicadorModalidadeSalario'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorModalidadeSalario'.
     */
    public int getCdIndicadorModalidadeSalario()
    {
        return this._cdIndicadorModalidadeSalario;
    } //-- int getCdIndicadorModalidadeSalario() 

    /**
     * Returns the value of field 'cdIndicadorRepiqueSalario'.
     * 
     * @return String
     * @return the value of field 'cdIndicadorRepiqueSalario'.
     */
    public java.lang.String getCdIndicadorRepiqueSalario()
    {
        return this._cdIndicadorRepiqueSalario;
    } //-- java.lang.String getCdIndicadorRepiqueSalario() 

    /**
     * Returns the value of field 'cdIndicadorSalarioContrato'.
     * 
     * @return String
     * @return the value of field 'cdIndicadorSalarioContrato'.
     */
    public java.lang.String getCdIndicadorSalarioContrato()
    {
        return this._cdIndicadorSalarioContrato;
    } //-- java.lang.String getCdIndicadorSalarioContrato() 

    /**
     * Returns the value of field 'cdMomeProcessamentoSalario'.
     * 
     * @return int
     * @return the value of field 'cdMomeProcessamentoSalario'.
     */
    public int getCdMomeProcessamentoSalario()
    {
        return this._cdMomeProcessamentoSalario;
    } //-- int getCdMomeProcessamentoSalario() 

    /**
     * Returns the value of field 'cdPagamentoUtilSalario'.
     * 
     * @return int
     * @return the value of field 'cdPagamentoUtilSalario'.
     */
    public int getCdPagamentoUtilSalario()
    {
        return this._cdPagamentoUtilSalario;
    } //-- int getCdPagamentoUtilSalario() 

    /**
     * Returns the value of field 'cdProdutoRelacionadoSalario'.
     * 
     * @return int
     * @return the value of field 'cdProdutoRelacionadoSalario'.
     */
    public int getCdProdutoRelacionadoSalario()
    {
        return this._cdProdutoRelacionadoSalario;
    } //-- int getCdProdutoRelacionadoSalario() 

    /**
     * Returns the value of field 'dsIndicadorFeriadoSalario'.
     * 
     * @return String
     * @return the value of field 'dsIndicadorFeriadoSalario'.
     */
    public java.lang.String getDsIndicadorFeriadoSalario()
    {
        return this._dsIndicadorFeriadoSalario;
    } //-- java.lang.String getDsIndicadorFeriadoSalario() 

    /**
     * Returns the value of field 'dsMomeProcessamentoSalario'.
     * 
     * @return String
     * @return the value of field 'dsMomeProcessamentoSalario'.
     */
    public java.lang.String getDsMomeProcessamentoSalario()
    {
        return this._dsMomeProcessamentoSalario;
    } //-- java.lang.String getDsMomeProcessamentoSalario() 

    /**
     * Returns the value of field 'dsPagamentoUtilSalario'.
     * 
     * @return String
     * @return the value of field 'dsPagamentoUtilSalario'.
     */
    public java.lang.String getDsPagamentoUtilSalario()
    {
        return this._dsPagamentoUtilSalario;
    } //-- java.lang.String getDsPagamentoUtilSalario() 

    /**
     * Returns the value of field 'dsProdutoRelacionadoSalario'.
     * 
     * @return String
     * @return the value of field 'dsProdutoRelacionadoSalario'.
     */
    public java.lang.String getDsProdutoRelacionadoSalario()
    {
        return this._dsProdutoRelacionadoSalario;
    } //-- java.lang.String getDsProdutoRelacionadoSalario() 

    /**
     * Returns the value of field 'nrQuantidadeDiaFLoatSalario'.
     * 
     * @return int
     * @return the value of field 'nrQuantidadeDiaFLoatSalario'.
     */
    public int getNrQuantidadeDiaFLoatSalario()
    {
        return this._nrQuantidadeDiaFLoatSalario;
    } //-- int getNrQuantidadeDiaFLoatSalario() 

    /**
     * Returns the value of field 'nrQuantidadeRepiqueSalario'.
     * 
     * @return int
     * @return the value of field 'nrQuantidadeRepiqueSalario'.
     */
    public int getNrQuantidadeRepiqueSalario()
    {
        return this._nrQuantidadeRepiqueSalario;
    } //-- int getNrQuantidadeRepiqueSalario() 

    /**
     * Method hasCdIndicadorFeriadoSalario
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorFeriadoSalario()
    {
        return this._has_cdIndicadorFeriadoSalario;
    } //-- boolean hasCdIndicadorFeriadoSalario() 

    /**
     * Method hasCdIndicadorModalidadeSalario
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorModalidadeSalario()
    {
        return this._has_cdIndicadorModalidadeSalario;
    } //-- boolean hasCdIndicadorModalidadeSalario() 

    /**
     * Method hasCdMomeProcessamentoSalario
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMomeProcessamentoSalario()
    {
        return this._has_cdMomeProcessamentoSalario;
    } //-- boolean hasCdMomeProcessamentoSalario() 

    /**
     * Method hasCdPagamentoUtilSalario
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPagamentoUtilSalario()
    {
        return this._has_cdPagamentoUtilSalario;
    } //-- boolean hasCdPagamentoUtilSalario() 

    /**
     * Method hasCdProdutoRelacionadoSalario
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdutoRelacionadoSalario()
    {
        return this._has_cdProdutoRelacionadoSalario;
    } //-- boolean hasCdProdutoRelacionadoSalario() 

    /**
     * Method hasNrQuantidadeDiaFLoatSalario
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrQuantidadeDiaFLoatSalario()
    {
        return this._has_nrQuantidadeDiaFLoatSalario;
    } //-- boolean hasNrQuantidadeDiaFLoatSalario() 

    /**
     * Method hasNrQuantidadeRepiqueSalario
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrQuantidadeRepiqueSalario()
    {
        return this._has_nrQuantidadeRepiqueSalario;
    } //-- boolean hasNrQuantidadeRepiqueSalario() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdIndicadorFeriadoSalario'.
     * 
     * @param cdIndicadorFeriadoSalario the value of field
     * 'cdIndicadorFeriadoSalario'.
     */
    public void setCdIndicadorFeriadoSalario(int cdIndicadorFeriadoSalario)
    {
        this._cdIndicadorFeriadoSalario = cdIndicadorFeriadoSalario;
        this._has_cdIndicadorFeriadoSalario = true;
    } //-- void setCdIndicadorFeriadoSalario(int) 

    /**
     * Sets the value of field 'cdIndicadorModalidadeSalario'.
     * 
     * @param cdIndicadorModalidadeSalario the value of field
     * 'cdIndicadorModalidadeSalario'.
     */
    public void setCdIndicadorModalidadeSalario(int cdIndicadorModalidadeSalario)
    {
        this._cdIndicadorModalidadeSalario = cdIndicadorModalidadeSalario;
        this._has_cdIndicadorModalidadeSalario = true;
    } //-- void setCdIndicadorModalidadeSalario(int) 

    /**
     * Sets the value of field 'cdIndicadorRepiqueSalario'.
     * 
     * @param cdIndicadorRepiqueSalario the value of field
     * 'cdIndicadorRepiqueSalario'.
     */
    public void setCdIndicadorRepiqueSalario(java.lang.String cdIndicadorRepiqueSalario)
    {
        this._cdIndicadorRepiqueSalario = cdIndicadorRepiqueSalario;
    } //-- void setCdIndicadorRepiqueSalario(java.lang.String) 

    /**
     * Sets the value of field 'cdIndicadorSalarioContrato'.
     * 
     * @param cdIndicadorSalarioContrato the value of field
     * 'cdIndicadorSalarioContrato'.
     */
    public void setCdIndicadorSalarioContrato(java.lang.String cdIndicadorSalarioContrato)
    {
        this._cdIndicadorSalarioContrato = cdIndicadorSalarioContrato;
    } //-- void setCdIndicadorSalarioContrato(java.lang.String) 

    /**
     * Sets the value of field 'cdMomeProcessamentoSalario'.
     * 
     * @param cdMomeProcessamentoSalario the value of field
     * 'cdMomeProcessamentoSalario'.
     */
    public void setCdMomeProcessamentoSalario(int cdMomeProcessamentoSalario)
    {
        this._cdMomeProcessamentoSalario = cdMomeProcessamentoSalario;
        this._has_cdMomeProcessamentoSalario = true;
    } //-- void setCdMomeProcessamentoSalario(int) 

    /**
     * Sets the value of field 'cdPagamentoUtilSalario'.
     * 
     * @param cdPagamentoUtilSalario the value of field
     * 'cdPagamentoUtilSalario'.
     */
    public void setCdPagamentoUtilSalario(int cdPagamentoUtilSalario)
    {
        this._cdPagamentoUtilSalario = cdPagamentoUtilSalario;
        this._has_cdPagamentoUtilSalario = true;
    } //-- void setCdPagamentoUtilSalario(int) 

    /**
     * Sets the value of field 'cdProdutoRelacionadoSalario'.
     * 
     * @param cdProdutoRelacionadoSalario the value of field
     * 'cdProdutoRelacionadoSalario'.
     */
    public void setCdProdutoRelacionadoSalario(int cdProdutoRelacionadoSalario)
    {
        this._cdProdutoRelacionadoSalario = cdProdutoRelacionadoSalario;
        this._has_cdProdutoRelacionadoSalario = true;
    } //-- void setCdProdutoRelacionadoSalario(int) 

    /**
     * Sets the value of field 'dsIndicadorFeriadoSalario'.
     * 
     * @param dsIndicadorFeriadoSalario the value of field
     * 'dsIndicadorFeriadoSalario'.
     */
    public void setDsIndicadorFeriadoSalario(java.lang.String dsIndicadorFeriadoSalario)
    {
        this._dsIndicadorFeriadoSalario = dsIndicadorFeriadoSalario;
    } //-- void setDsIndicadorFeriadoSalario(java.lang.String) 

    /**
     * Sets the value of field 'dsMomeProcessamentoSalario'.
     * 
     * @param dsMomeProcessamentoSalario the value of field
     * 'dsMomeProcessamentoSalario'.
     */
    public void setDsMomeProcessamentoSalario(java.lang.String dsMomeProcessamentoSalario)
    {
        this._dsMomeProcessamentoSalario = dsMomeProcessamentoSalario;
    } //-- void setDsMomeProcessamentoSalario(java.lang.String) 

    /**
     * Sets the value of field 'dsPagamentoUtilSalario'.
     * 
     * @param dsPagamentoUtilSalario the value of field
     * 'dsPagamentoUtilSalario'.
     */
    public void setDsPagamentoUtilSalario(java.lang.String dsPagamentoUtilSalario)
    {
        this._dsPagamentoUtilSalario = dsPagamentoUtilSalario;
    } //-- void setDsPagamentoUtilSalario(java.lang.String) 

    /**
     * Sets the value of field 'dsProdutoRelacionadoSalario'.
     * 
     * @param dsProdutoRelacionadoSalario the value of field
     * 'dsProdutoRelacionadoSalario'.
     */
    public void setDsProdutoRelacionadoSalario(java.lang.String dsProdutoRelacionadoSalario)
    {
        this._dsProdutoRelacionadoSalario = dsProdutoRelacionadoSalario;
    } //-- void setDsProdutoRelacionadoSalario(java.lang.String) 

    /**
     * Sets the value of field 'nrQuantidadeDiaFLoatSalario'.
     * 
     * @param nrQuantidadeDiaFLoatSalario the value of field
     * 'nrQuantidadeDiaFLoatSalario'.
     */
    public void setNrQuantidadeDiaFLoatSalario(int nrQuantidadeDiaFLoatSalario)
    {
        this._nrQuantidadeDiaFLoatSalario = nrQuantidadeDiaFLoatSalario;
        this._has_nrQuantidadeDiaFLoatSalario = true;
    } //-- void setNrQuantidadeDiaFLoatSalario(int) 

    /**
     * Sets the value of field 'nrQuantidadeRepiqueSalario'.
     * 
     * @param nrQuantidadeRepiqueSalario the value of field
     * 'nrQuantidadeRepiqueSalario'.
     */
    public void setNrQuantidadeRepiqueSalario(int nrQuantidadeRepiqueSalario)
    {
        this._nrQuantidadeRepiqueSalario = nrQuantidadeRepiqueSalario;
        this._has_nrQuantidadeRepiqueSalario = true;
    } //-- void setNrQuantidadeRepiqueSalario(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias2
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias2 unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias2) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias2.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias2 unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
