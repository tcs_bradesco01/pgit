/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/implementation.ftl,v $
 * $Id: implementation.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: implementation.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */
 
package br.com.bradesco.web.pgit.service.business.liberarpagtosindividuaispend.impl;

import java.util.ArrayList;
import java.util.List;

import br.com.bradesco.web.pgit.service.business.liberarpagtosindividuaispend.ILiberarPagtosIndividuaisPendService;
import br.com.bradesco.web.pgit.service.business.liberarpagtosindividuaispend.ILiberarPagtosIndividuaisPendServiceConstants;
import br.com.bradesco.web.pgit.service.business.liberarpagtosindividuaispend.bean.ConsultarPagtoPendIndividualEntradaDTO;
import br.com.bradesco.web.pgit.service.business.liberarpagtosindividuaispend.bean.ConsultarPagtoPendIndividualSaidaDTO;
import br.com.bradesco.web.pgit.service.business.liberarpagtosindividuaispend.bean.LiberarPagtoIndividualPendenciaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.liberarpagtosindividuaispend.bean.LiberarPagtoIndividualPendenciaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.liberarpagtosindividuaispend.bean.LiberarPagtoPendIndividualEntradaDTO;
import br.com.bradesco.web.pgit.service.business.liberarpagtosindividuaispend.bean.LiberarPagtoPendIndividualSaidaDTO;
import br.com.bradesco.web.pgit.service.data.pdc.FactoryAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarpagtopendindividual.request.ConsultarPagtoPendIndividualRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarpagtopendindividual.response.ConsultarPagtoPendIndividualResponse;
import br.com.bradesco.web.pgit.service.data.pdc.liberarpagtoindividualpendencia.request.LiberarPagtoIndividualPendenciaRequest;
import br.com.bradesco.web.pgit.service.data.pdc.liberarpagtoindividualpendencia.response.LiberarPagtoIndividualPendenciaResponse;
import br.com.bradesco.web.pgit.service.data.pdc.liberarpagtopendindividual.request.LiberarPagtoPendIndividualRequest;
import br.com.bradesco.web.pgit.service.data.pdc.liberarpagtopendindividual.response.LiberarPagtoPendIndividualResponse;
import br.com.bradesco.web.pgit.utils.CpfCnpjUtils;
import br.com.bradesco.web.pgit.utils.DateUtils;
import br.com.bradesco.web.pgit.utils.PgitUtil;


/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Implementa��o do adaptador: LiberarPagtosIndividuaisPend
 * </p>
 * 
 * @comment C�DIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public class LiberarPagtosIndividuaisPendServiceImpl implements ILiberarPagtosIndividuaisPendService {
	
	/** Atributo factoryAdapter. */
	private FactoryAdapter factoryAdapter;
	

	/**
	 * Get: factoryAdapter.
	 *
	 * @return factoryAdapter
	 */
	public FactoryAdapter getFactoryAdapter() {
		return factoryAdapter;
	}

	/**
	 * Set: factoryAdapter.
	 *
	 * @param factoryAdapter the factory adapter
	 */
	public void setFactoryAdapter(FactoryAdapter factoryAdapter) {
		this.factoryAdapter = factoryAdapter;
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.liberarpagtosindividuaispend.ILiberarPagtosIndividuaisPendService#consultarPagtoPendIndividual(br.com.bradesco.web.pgit.service.business.liberarpagtosindividuaispend.bean.ConsultarPagtoPendIndividualEntradaDTO)
	 */
	public List<ConsultarPagtoPendIndividualSaidaDTO> consultarPagtoPendIndividual(ConsultarPagtoPendIndividualEntradaDTO entradaDTO) {
		ConsultarPagtoPendIndividualRequest request = new ConsultarPagtoPendIndividualRequest();
		ConsultarPagtoPendIndividualResponse response = new ConsultarPagtoPendIndividualResponse();
    	List<ConsultarPagtoPendIndividualSaidaDTO> listaSaida = new ArrayList<ConsultarPagtoPendIndividualSaidaDTO>();
    	
    	request.setCdTipoPesquisa(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoPesquisa()));
    	request.setCdAgendadosPagosNaoPagos(ILiberarPagtosIndividuaisPendServiceConstants.CODIGO_AGENDAMENTO_PAGO_NAO_PAGO);
    	request.setCdPessoaJuridicaContrato(PgitUtil.verificaLongNulo(entradaDTO.getCdPessoaJuridicaContrato()));
    	request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoContratoNegocio()));
    	request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(entradaDTO.getNrSequenciaContratoNegocio()));
    	request.setCdBanco(PgitUtil.verificaIntegerNulo(entradaDTO.getCdBanco()));
    	request.setCdAgencia(PgitUtil.verificaIntegerNulo(entradaDTO.getCdAgencia()));
    	request.setCdConta(PgitUtil.verificaLongNulo(entradaDTO.getCdConta()));
    	request.setCdDigitoConta(PgitUtil.DIGITO_CONTA);
    	request.setDtCreditoPagamentoInicio(PgitUtil.verificaStringNula(entradaDTO.getDtCreditoPagamentoInicio()));
    	request.setDtCreditoPagamentoFim(PgitUtil.verificaStringNula(entradaDTO.getDtCreditoPagamentoFim()));
    	request.setNrArquivoRemssaPagamento(PgitUtil.verificaLongNulo(entradaDTO.getNrArquivoRemssaPagamento()));
    	request.setCdParticipante(PgitUtil.verificaLongNulo(entradaDTO.getCdParticipante()));
    	request.setCdProdutoServicoOperacao(PgitUtil.verificaIntegerNulo(entradaDTO.getCdProdutoServicoOperacao()));
    	request.setCdProdutoServicoRelacionado(PgitUtil.verificaIntegerNulo(entradaDTO.getCdProdutoServicoRelacionado()));
    	request.setCdControlePagamentoDe(PgitUtil.verificaStringNula(entradaDTO.getCdControlePagamentoDe()));
    	request.setCdControlePagamentoAte(PgitUtil.verificaStringNula(entradaDTO.getCdControlePagamentoAte()));
    	request.setVlPagamentoDe(PgitUtil.verificaBigDecimalNulo(entradaDTO.getVlPagamentoDe()));
    	request.setVlPagamentoAte(PgitUtil.verificaBigDecimalNulo(entradaDTO.getVlPagamentoAte()));
    	request.setCdSituacaoOperacaoPagamento(ILiberarPagtosIndividuaisPendServiceConstants.CODIGO_SITUACAO_PAGAMENTO);
    	request.setCdMotivoSituacaoPagamento(PgitUtil.verificaIntegerNulo(entradaDTO.getCdMotivoSituacaoPagamento()));
    	request.setCdBarraDocumento(PgitUtil.verificaStringNula(entradaDTO.getCdBarraDocumento()));
    	request.setCdFavorecidoClientePagador(PgitUtil.verificaLongNulo(entradaDTO.getCdFavorecidoClientePagador()));
    	request.setCdInscricaoFavorecido(PgitUtil.verificaLongNulo(entradaDTO.getCdInscricaoFavorecido()));
    	request.setCdIdentificacaoInscricaoFavorecido(PgitUtil.verificaIntegerNulo(entradaDTO.getCdIdentificacaoInscricaoFavorecido()));
    	request.setCdBancoBeneficiario(PgitUtil.verificaIntegerNulo(entradaDTO.getCdBancoBeneficiario()));
    	request.setCdAgenciaBeneficiario(PgitUtil.verificaIntegerNulo(entradaDTO.getCdAgenciaBeneficiario()));
    	request.setCdContaBeneficiario(PgitUtil.verificaLongNulo(entradaDTO.getCdContaBeneficiario()));
    	request.setCdDigitoContaBeneficiario(PgitUtil.DIGITO_CONTA);
    	request.setNrUnidadeOrganizacional(PgitUtil.verificaIntegerNulo(entradaDTO.getNrUnidadeOrganizacional()));
    	request.setCdBeneficio(PgitUtil.verificaLongNulo(entradaDTO.getCdBeneficio()));
    	request.setCdEspecieBeneficioInss(PgitUtil.verificaIntegerNulo(entradaDTO.getCdEspecieBeneficioInss()));
    	request.setCdClassificacao(PgitUtil.verificaIntegerNulo(entradaDTO.getCdClassificacao()));
    	request.setCdEmpresaContrato(PgitUtil.verificaLongNulo(entradaDTO.getCdEmpresaContrato()));
    	request.setCdTipoConta(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoConta()));
    	request.setNrContrato(PgitUtil.verificaLongNulo(entradaDTO.getNrContrato()));
    	request.setNrOcorrencias(ILiberarPagtosIndividuaisPendServiceConstants.NUMERO_OCORRENCIAS_CONSULTAR);
    	request.setCdCpfCnpjParticipante(PgitUtil.verificaLongNulo(entradaDTO.getCdCpfCnpjParticipante()));
    	request.setCdFilialCnpjParticipante(PgitUtil.verificaIntegerNulo(entradaDTO.getCdFilialCnpjParticipante()));
    	request.setCdControleCnpjParticipante(PgitUtil.verificaIntegerNulo(entradaDTO.getCdControleCnpjParticipante()));
    	request.setCdPessoaContratoDebito(PgitUtil.verificaLongNulo(entradaDTO.getCdPessoaContratoDebito()));
    	request.setCdTipoContratoDebito(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoContratoDebito()));
    	request.setNrSequenciaContratoDebito(PgitUtil.verificaLongNulo(entradaDTO.getNrSequenciaContratoDebito()));
    	request.setCdPessoaContratoCredt(PgitUtil.verificaLongNulo(entradaDTO.getCdPessoaContratoCredt()));
    	request.setCdTipoContratoCredt(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoContratoCredt()));
    	request.setNrSequenciaContratoCredt(PgitUtil.verificaLongNulo(entradaDTO.getNrSequenciaContratoCredt()));
    	request.setCdIndiceSimulaPagamento(PgitUtil.verificaIntegerNulo(entradaDTO.getCdIndiceSimulaPagamento()));
    	request.setCdOrigemRecebidoEfetivacao(PgitUtil.verificaIntegerNulo(entradaDTO.getCdOrigemRecebidoEfetivacao()));      	
    	request.setCdTituloPgtoRastreado(0); 
    	request.setCdBancoSalarial(PgitUtil.verificaIntegerNulo(entradaDTO.getCdBancoSalarial()));
    	request.setCdAgencaSalarial(PgitUtil.verificaIntegerNulo(entradaDTO.getCdAgencaSalarial()));
    	request.setCdContaSalarial(PgitUtil.verificaLongNulo(entradaDTO.getCdContaSalarial()));
		if("".equals(PgitUtil.verificaStringNula(entradaDTO.getCdIspbPagtoDestino()))){
			request.setCdIspbPagtoDestino("");
		}else{
			request.setCdIspbPagtoDestino(PgitUtil.preencherZerosAEsquerda(entradaDTO.getCdIspbPagtoDestino(), 8));
		}
		request.setContaPagtoDestino(PgitUtil.preencherZerosAEsquerda(entradaDTO.getContaPagtoDestino(), 20));
 
    	response = getFactoryAdapter().getConsultarPagtoPendIndividualPDCAdapter().invokeProcess(request);
    	
   		for(br.com.bradesco.web.pgit.service.data.pdc.consultarpagtopendindividual.response.Ocorrencias o : response.getOcorrencias()){
    		ConsultarPagtoPendIndividualSaidaDTO saida = new ConsultarPagtoPendIndividualSaidaDTO();
    		saida.setNrCnpjCpf(o.getNrCnpjCpf());
    		saida.setNrFilialCnpjCpf(o.getNrFilialCnpjCpf());
    		saida.setNrDigitoCnpjCpf(o.getNrDigitoCnpjCpf());
    		saida.setCdPessoaJuridicaContrato(o.getCdPessoaJuridicaContrato());
    		saida.setDsRazaoSocial(o.getDsRazaoSocial());
    		saida.setCdEmpresa(o.getCdEmpresa());
    		saida.setDsEmpresa(o.getDsEmpresa());
    		saida.setCdTipoContratoNegocio(o.getCdTipoContratoNegocio());
    		saida.setNrContrato(o.getNrContrato());
    		saida.setCdProdutoServicoOperacao(o.getCdProdutoServicoOperacao());
    		saida.setDsResumoProdutoServico(o.getDsResumoProdutoServico());
    		saida.setCdProdutoServicoRelacionado(o.getCdProdutoServicoRelacionado());
    		saida.setDsOperacaoProdutoServico(o.getDsOperacaoProdutoServico());
    		saida.setCdControlePagamento(o.getCdControlePagamento());
    		saida.setDtCreditoPagamento(DateUtils.formartarDataPDCparaPadraPtBr(o.getDtCreditoPagamento(), "dd.MM.yyyy", "dd/MM/yyyy"));
    		saida.setVlEfetivoPagamento(o.getVlEfetivoPagamento());
    		saida.setCdInscricaoFavorecido(o.getCdInscricaoFavorecido() == 0L ? "" : String.valueOf(o.getCdInscricaoFavorecido()));
    		saida.setDsFavorecido(o.getDsFavorecido());
    		saida.setCdBancoDebito(o.getCdBancoDebito());
    		saida.setCdAgenciaDebito(o.getCdAgenciaDebito());
    		saida.setCdDigitoAgenciaDebito(o.getCdDigitoAgenciaDebito());
    		saida.setCdContaDebito(o.getCdContaDebito());
    		saida.setCdDigitoContaDebito(o.getCdDigitoContaDebito());
    		saida.setCdBancoCredito(o.getCdBancoCredito());
    		saida.setCdAgenciaCredito(o.getCdAgenciaCredito());
    		saida.setCdDigitoAgenciaCredito(o.getCdDigitoAgenciaCredito());
    		saida.setCdContaCredito(o.getCdContaCredito());
    		saida.setCdDigitoContaCredito(o.getCdDigitoContaCredito());
    		saida.setCdSituacaoOperacaoPagamento(o.getCdSituacaoOperacaoPagamento());
    		saida.setDsSituacaoOperacaoPagamento(o.getDsSituacaoOperacaoPagamento());
    		saida.setCdMotivoSituacaoPagamento(o.getCdMotivoSituacaoPagamento());
    		saida.setDsMotivoSituacaoPagamento(o.getDsMotivoSituacaoPagamento());
    		saida.setCdTipoCanal(o.getCdTipoCanal());
    		saida.setCdTipoTela(o.getCdTipoTela());
    		saida.setDsEfetivacaoPagamento(o.getDsEfetivacaoPagamento());
    		
    		saida.setBancoCreditoFormatado(PgitUtil.formatBanco(o.getCdBancoCredito(), false));
    		saida.setAgenciaCreditoFormatada(PgitUtil.formatAgencia(o.getCdAgenciaCredito(), o.getCdDigitoAgenciaCredito(), false));
    		saida.setContaCreditoFormatada(PgitUtil.formatConta(o.getCdContaCredito(), o.getCdDigitoContaCredito(), false));

    		saida.setCpfCnpjFormatado(CpfCnpjUtils.formatCpfCnpjCompleto(o.getNrCnpjCpf(), o.getNrFilialCnpjCpf(), o.getNrDigitoCnpjCpf()));
    		saida.setEmpresaFormatada(PgitUtil.concatenarCampos(o.getCdEmpresa(), o.getDsEmpresa()));
    		saida.setTipoServicoFormatado(PgitUtil.concatenarCampos(o.getCdProdutoServicoOperacao(), o.getDsResumoProdutoServico()));
    		saida.setModalidadeFormatada(PgitUtil.concatenarCampos(o.getCdProdutoServicoRelacionado(), o.getDsOperacaoProdutoServico()));
    		
    		//Formatar Conta Cr�dito/D�bito
	   	    saida.setBancoAgenciaContaCreditoFormatado(PgitUtil.formatBancoAgenciaConta(saida.getCdBancoCredito(), saida.getCdAgenciaCredito(), saida.getCdDigitoAgenciaCredito(), saida.getCdContaCredito(), saida.getCdDigitoContaCredito(), true));
	   	    saida.setBancoAgenciaContaDebitoFormatado(PgitUtil.formatBancoAgenciaConta(saida.getCdBancoDebito(), saida.getCdAgenciaDebito(), saida.getCdDigitoAgenciaDebito(), saida.getCdContaDebito(), saida.getCdDigitoContaDebito(), true));
	   	    saida.setDsIndicadorPagamento(o.getDsIndicadorPagamento());

	   	    saida.setCdIspbPagtoDestino(o.getCdIspbPagtoDestino());
	   	    saida.setContaPagtoDestino(o.getContaPagtoDestino());

	   	    
    		listaSaida.add(saida);
    	}
    	
    	return listaSaida;
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.liberarpagtosindividuaispend.ILiberarPagtosIndividuaisPendService#liberarPagtoIndividualPendencia(java.util.List)
	 */
	public LiberarPagtoIndividualPendenciaSaidaDTO liberarPagtoIndividualPendencia(List<LiberarPagtoIndividualPendenciaEntradaDTO> listaEntrada) {
		LiberarPagtoIndividualPendenciaSaidaDTO saidaDTO = new LiberarPagtoIndividualPendenciaSaidaDTO();
		LiberarPagtoIndividualPendenciaRequest request = new LiberarPagtoIndividualPendenciaRequest();
		LiberarPagtoIndividualPendenciaResponse response = new LiberarPagtoIndividualPendenciaResponse();
		
		request.setQtRegistros(listaEntrada.size());
		ArrayList<br.com.bradesco.web.pgit.service.data.pdc.liberarpagtoindividualpendencia.request.Ocorrencias> lista = new ArrayList<br.com.bradesco.web.pgit.service.data.pdc.liberarpagtoindividualpendencia.request.Ocorrencias>();
				
		//V�rias Ocorr�ncias para Libera��o
		for(LiberarPagtoIndividualPendenciaEntradaDTO o : listaEntrada){
			br.com.bradesco.web.pgit.service.data.pdc.liberarpagtoindividualpendencia.request.Ocorrencias ocorrencia = new br.com.bradesco.web.pgit.service.data.pdc.liberarpagtoindividualpendencia.request.Ocorrencias();
			
			ocorrencia.setCdPessoaJuridicaContrato(PgitUtil.verificaLongNulo(o.getCdPessoaJuridicaContrato()));
			ocorrencia.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(o.getCdTipoContratoNegocio()));
			ocorrencia.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(o.getNrSequenciaContratoNegocio()));
			ocorrencia.setCdTipoCanal(PgitUtil.verificaIntegerNulo(o.getCdTipoCanal()));
			ocorrencia.setCdControlePagamento(PgitUtil.verificaStringNula(o.getCdControlePagamento()));
			ocorrencia.setCdModalidade(PgitUtil.verificaIntegerNulo(o.getCdModalidade()));
			ocorrencia.setCdMotivoPendencia(PgitUtil.verificaIntegerNulo(o.getCdMotivoPendencia()));
	
			lista.add(ocorrencia);		
		}
				
		request.setOcorrencias((br.com.bradesco.web.pgit.service.data.pdc.liberarpagtoindividualpendencia.request.Ocorrencias[]) lista.toArray(new br.com.bradesco.web.pgit.service.data.pdc.liberarpagtoindividualpendencia.request.Ocorrencias[0]));		
		response = getFactoryAdapter().getLiberarPagtoIndividualPendenciaPDCAdapter().invokeProcess(request);
		
		saidaDTO.setCodMensagem(response.getCodMensagem());
		saidaDTO.setMensagem(response.getMensagem());
		
		return saidaDTO;
	}
	
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.liberarpagtosindividuaispend.ILiberarPagtosIndividuaisPendService#liberarPagtoPendIndividual(java.util.List)
	 */
	public LiberarPagtoPendIndividualSaidaDTO liberarPagtoPendIndividual(List<LiberarPagtoPendIndividualEntradaDTO> listaEntrada) {
		LiberarPagtoPendIndividualSaidaDTO saidaDTO = new LiberarPagtoPendIndividualSaidaDTO();
		LiberarPagtoPendIndividualRequest request = new LiberarPagtoPendIndividualRequest();
		LiberarPagtoPendIndividualResponse response = new LiberarPagtoPendIndividualResponse();
		
		request.setQtRegistros(listaEntrada.size());
		ArrayList<br.com.bradesco.web.pgit.service.data.pdc.liberarpagtopendindividual.request.Ocorrencias> lista = new ArrayList<br.com.bradesco.web.pgit.service.data.pdc.liberarpagtopendindividual.request.Ocorrencias>();
				
		//V�rias Ocorr�ncias para Libera��o
		for(LiberarPagtoPendIndividualEntradaDTO o : listaEntrada){
			br.com.bradesco.web.pgit.service.data.pdc.liberarpagtopendindividual.request.Ocorrencias ocorrencia = new br.com.bradesco.web.pgit.service.data.pdc.liberarpagtopendindividual.request.Ocorrencias();
			
			ocorrencia.setCdPessoaJuridicaContrato(PgitUtil.verificaLongNulo(o.getCdPessoaJuridicaContrato()));
			ocorrencia.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(o.getCdTipoContratoNegocio()));
			ocorrencia.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(o.getNrSequenciaContratoNegocio()));
			ocorrencia.setCdTipoCanal(PgitUtil.verificaIntegerNulo(o.getCdTipoCanal()));
			ocorrencia.setCdControlePagamento(PgitUtil.verificaStringNula(o.getCdControlePagamento()));
			ocorrencia.setCdModalidade(PgitUtil.verificaIntegerNulo(o.getCdModalidade()));
			
			lista.add(ocorrencia);		
		}
				
		request.setOcorrencias((br.com.bradesco.web.pgit.service.data.pdc.liberarpagtopendindividual.request.Ocorrencias[]) lista.toArray(new br.com.bradesco.web.pgit.service.data.pdc.liberarpagtopendindividual.request.Ocorrencias[0]));		
		response = getFactoryAdapter().getLiberarPagtoPendIndividualPDCAdapter().invokeProcess(request);
		
		saidaDTO.setCodMensagem(response.getCodMensagem());
		saidaDTO.setMensagem(response.getMensagem());
		
		return saidaDTO;
	}

	
	
}