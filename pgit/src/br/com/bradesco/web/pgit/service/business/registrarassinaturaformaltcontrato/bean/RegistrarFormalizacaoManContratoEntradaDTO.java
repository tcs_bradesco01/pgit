/*
 * Nome: br.com.bradesco.web.pgit.service.business.registrarassinaturaformaltcontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.registrarassinaturaformaltcontrato.bean;

/**
 * Nome: RegistrarFormalizacaoManContratoEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class RegistrarFormalizacaoManContratoEntradaDTO {
	
	/** Atributo cdPessoaJuridicaContrato. */
	private long cdPessoaJuridicaContrato;
	
	/** Atributo cdTipoContratoNegocio. */
	private int cdTipoContratoNegocio;
	
	/** Atributo nrSequenciaContratoNegocio. */
	private long nrSequenciaContratoNegocio;
	
	/** Atributo nrAditivoContratoNegocio. */
	private long nrAditivoContratoNegocio;
	
	/** Atributo cdContratoNegocioPagamento. */
	private String cdContratoNegocioPagamento;
	
	/** Atributo dtAssinaturaAditivo. */
	private String dtAssinaturaAditivo;
	
	/**
	 * Get: cdContratoNegocioPagamento.
	 *
	 * @return cdContratoNegocioPagamento
	 */
	public String getCdContratoNegocioPagamento() {
		return cdContratoNegocioPagamento;
	}
	
	/**
	 * Set: cdContratoNegocioPagamento.
	 *
	 * @param cdContratoNegocioPagamento the cd contrato negocio pagamento
	 */
	public void setCdContratoNegocioPagamento(String cdContratoNegocioPagamento) {
		this.cdContratoNegocioPagamento = cdContratoNegocioPagamento;
	}
	
	/**
	 * Get: cdPessoaJuridicaContrato.
	 *
	 * @return cdPessoaJuridicaContrato
	 */
	public long getCdPessoaJuridicaContrato() {
		return cdPessoaJuridicaContrato;
	}
	
	/**
	 * Set: cdPessoaJuridicaContrato.
	 *
	 * @param cdPessoaJuridicaContrato the cd pessoa juridica contrato
	 */
	public void setCdPessoaJuridicaContrato(long cdPessoaJuridicaContrato) {
		this.cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
	}
	
	/**
	 * Get: cdTipoContratoNegocio.
	 *
	 * @return cdTipoContratoNegocio
	 */
	public int getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}
	
	/**
	 * Set: cdTipoContratoNegocio.
	 *
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(int cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}
	
	/**
	 * Get: dtAssinaturaAditivo.
	 *
	 * @return dtAssinaturaAditivo
	 */
	public String getDtAssinaturaAditivo() {
		return dtAssinaturaAditivo;
	}
	
	/**
	 * Set: dtAssinaturaAditivo.
	 *
	 * @param dtAssinaturaAditivo the dt assinatura aditivo
	 */
	public void setDtAssinaturaAditivo(String dtAssinaturaAditivo) {
		this.dtAssinaturaAditivo = dtAssinaturaAditivo;
	}
	
	/**
	 * Get: nrAditivoContratoNegocio.
	 *
	 * @return nrAditivoContratoNegocio
	 */
	public long getNrAditivoContratoNegocio() {
		return nrAditivoContratoNegocio;
	}
	
	/**
	 * Set: nrAditivoContratoNegocio.
	 *
	 * @param nrAditivoContratoNegocio the nr aditivo contrato negocio
	 */
	public void setNrAditivoContratoNegocio(long nrAditivoContratoNegocio) {
		this.nrAditivoContratoNegocio = nrAditivoContratoNegocio;
	}
	
	/**
	 * Get: nrSequenciaContratoNegocio.
	 *
	 * @return nrSequenciaContratoNegocio
	 */
	public long getNrSequenciaContratoNegocio() {
		return nrSequenciaContratoNegocio;
	}
	
	/**
	 * Set: nrSequenciaContratoNegocio.
	 *
	 * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
	 */
	public void setNrSequenciaContratoNegocio(long nrSequenciaContratoNegocio) {
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}
	
	
}
