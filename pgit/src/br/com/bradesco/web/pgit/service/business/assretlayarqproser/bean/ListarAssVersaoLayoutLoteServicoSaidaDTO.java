package br.com.bradesco.web.pgit.service.business.assretlayarqproser.bean;

import java.util.List;

/**
 * Arquivo criado em 09/02/15.
 */
public class ListarAssVersaoLayoutLoteServicoSaidaDTO {

    private String codMensagem;

    private String mensagem;

    private Integer numeroLinhas;

    private List<ListarAssVersaoLayoutLoteServicoOcorrenciasSaidaDTO> ocorrencias;

    public void setCodMensagem(String codMensagem) {
        this.codMensagem = codMensagem;
    }

    public String getCodMensagem() {
        return this.codMensagem;
    }

    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }

    public String getMensagem() {
        return this.mensagem;
    }

    public void setNumeroLinhas(Integer numeroLinhas) {
        this.numeroLinhas = numeroLinhas;
    }

    public Integer getNumeroLinhas() {
        return this.numeroLinhas;
    }

    public void setOcorrencias(List<ListarAssVersaoLayoutLoteServicoOcorrenciasSaidaDTO> ocorrencias) {
        this.ocorrencias = ocorrencias;
    }

    public List<ListarAssVersaoLayoutLoteServicoOcorrenciasSaidaDTO> getOcorrencias() {
        return this.ocorrencias;
    }
}