/*
 * Nome: ${package_name}
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: ${date}
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantervinculacaoconveniocontasalario.bean;

import java.math.BigDecimal;

import br.com.bradesco.web.pgit.utils.NumberUtils;

/**
 * The Class DetalharPiramideOcorrencias1SaidaDTO.
 */
public class DetalharPiramideOcorrencias1SaidaDTO {

    /** The n seq faixa salarial. */
    private Integer nSeqFaixaSalarial;

    /** The vl inicial faixa salarial. */
    private BigDecimal vlInicialFaixaSalarial;

    /** The vl final faixa salarial. */
    private BigDecimal vlFinalFaixaSalarial;

    /** The qtd funcionario filial01. */
    private Long qtdFuncionarioFilial01;

    /** The qtd funcionario filial02. */
    private Long qtdFuncionarioFilial02;

    /** The qtd funcionario filial03. */
    private Long qtdFuncionarioFilial03;

    /** The qtd funcionario filial04. */
    private Long qtdFuncionarioFilial04;

    /** The qtd funcionario filial05. */
    private Long qtdFuncionarioFilial05;

    /** The qtd funcionario filial06. */
    private Long qtdFuncionarioFilial06;

    /** The qtd funcionario filial07. */
    private Long qtdFuncionarioFilial07;

    /** The qtd funcionario filial08. */
    private Long qtdFuncionarioFilial08;

    /** The qtd funcionario filial09. */
    private Long qtdFuncionarioFilial09;

    /** The qtd funcionario filial10. */
    private Long qtdFuncionarioFilial10;

    /**
     * Set n seq faixa salarial.
     * 
     * @param nSeqFaixaSalarial
     *            the n seq faixa salarial
     */
    public void setNSeqFaixaSalarial(Integer nSeqFaixaSalarial) {
        this.nSeqFaixaSalarial = nSeqFaixaSalarial;
    }

    /**
     * Get n seq faixa salarial.
     * 
     * @return the n seq faixa salarial
     */
    public Integer getNSeqFaixaSalarial() {
        return this.nSeqFaixaSalarial;
    }

    /**
     * Set vl inicial faixa salarial.
     * 
     * @param vlInicialFaixaSalarial
     *            the vl inicial faixa salarial
     */
    public void setVlInicialFaixaSalarial(BigDecimal vlInicialFaixaSalarial) {
        this.vlInicialFaixaSalarial = vlInicialFaixaSalarial;
    }

    /**
     * Get vl inicial faixa salarial.
     * 
     * @return the vl inicial faixa salarial
     */
    public BigDecimal getVlInicialFaixaSalarial() {
        return this.vlInicialFaixaSalarial;
    }

    /**
     * Set vl final faixa salarial.
     * 
     * @param vlFinalFaixaSalarial
     *            the vl final faixa salarial
     */
    public void setVlFinalFaixaSalarial(BigDecimal vlFinalFaixaSalarial) {
        this.vlFinalFaixaSalarial = vlFinalFaixaSalarial;
    }

    /**
     * Get vl final faixa salarial.
     * 
     * @return the vl final faixa salarial
     */
    public BigDecimal getVlFinalFaixaSalarial() {
        return this.vlFinalFaixaSalarial;
    }

    /**
     * Set qtd funcionario filial01.
     * 
     * @param qtdFuncionarioFilial01
     *            the qtd funcionario filial01
     */
    public void setQtdFuncionarioFilial01(Long qtdFuncionarioFilial01) {
        this.qtdFuncionarioFilial01 = qtdFuncionarioFilial01;
    }

    /**
     * Get qtd funcionario filial01.
     * 
     * @return the qtd funcionario filial01
     */
    public Long getQtdFuncionarioFilial01() {
        return this.qtdFuncionarioFilial01;
    }

    /**
     * Set qtd funcionario filial02.
     * 
     * @param qtdFuncionarioFilial02
     *            the qtd funcionario filial02
     */
    public void setQtdFuncionarioFilial02(Long qtdFuncionarioFilial02) {
        this.qtdFuncionarioFilial02 = qtdFuncionarioFilial02;
    }

    /**
     * Get qtd funcionario filial02.
     * 
     * @return the qtd funcionario filial02
     */
    public Long getQtdFuncionarioFilial02() {
        return this.qtdFuncionarioFilial02;
    }

    /**
     * Set qtd funcionario filial03.
     * 
     * @param qtdFuncionarioFilial03
     *            the qtd funcionario filial03
     */
    public void setQtdFuncionarioFilial03(Long qtdFuncionarioFilial03) {
        this.qtdFuncionarioFilial03 = qtdFuncionarioFilial03;
    }

    /**
     * Get qtd funcionario filial03.
     * 
     * @return the qtd funcionario filial03
     */
    public Long getQtdFuncionarioFilial03() {
        return this.qtdFuncionarioFilial03;
    }

    /**
     * Set qtd funcionario filial04.
     * 
     * @param qtdFuncionarioFilial04
     *            the qtd funcionario filial04
     */
    public void setQtdFuncionarioFilial04(Long qtdFuncionarioFilial04) {
        this.qtdFuncionarioFilial04 = qtdFuncionarioFilial04;
    }

    /**
     * Get qtd funcionario filial04.
     * 
     * @return the qtd funcionario filial04
     */
    public Long getQtdFuncionarioFilial04() {
        return this.qtdFuncionarioFilial04;
    }

    /**
     * Set qtd funcionario filial05.
     * 
     * @param qtdFuncionarioFilial05
     *            the qtd funcionario filial05
     */
    public void setQtdFuncionarioFilial05(Long qtdFuncionarioFilial05) {
        this.qtdFuncionarioFilial05 = qtdFuncionarioFilial05;
    }

    /**
     * Get qtd funcionario filial05.
     * 
     * @return the qtd funcionario filial05
     */
    public Long getQtdFuncionarioFilial05() {
        return this.qtdFuncionarioFilial05;
    }

    /**
     * Set qtd funcionario filial06.
     * 
     * @param qtdFuncionarioFilial06
     *            the qtd funcionario filial06
     */
    public void setQtdFuncionarioFilial06(Long qtdFuncionarioFilial06) {
        this.qtdFuncionarioFilial06 = qtdFuncionarioFilial06;
    }

    /**
     * Get qtd funcionario filial06.
     * 
     * @return the qtd funcionario filial06
     */
    public Long getQtdFuncionarioFilial06() {
        return this.qtdFuncionarioFilial06;
    }

    /**
     * Set qtd funcionario filial07.
     * 
     * @param qtdFuncionarioFilial07
     *            the qtd funcionario filial07
     */
    public void setQtdFuncionarioFilial07(Long qtdFuncionarioFilial07) {
        this.qtdFuncionarioFilial07 = qtdFuncionarioFilial07;
    }

    /**
     * Get qtd funcionario filial07.
     * 
     * @return the qtd funcionario filial07
     */
    public Long getQtdFuncionarioFilial07() {
        return this.qtdFuncionarioFilial07;
    }

    /**
     * Set qtd funcionario filial08.
     * 
     * @param qtdFuncionarioFilial08
     *            the qtd funcionario filial08
     */
    public void setQtdFuncionarioFilial08(Long qtdFuncionarioFilial08) {
        this.qtdFuncionarioFilial08 = qtdFuncionarioFilial08;
    }

    /**
     * Get qtd funcionario filial08.
     * 
     * @return the qtd funcionario filial08
     */
    public Long getQtdFuncionarioFilial08() {
        return this.qtdFuncionarioFilial08;
    }

    /**
     * Set qtd funcionario filial09.
     * 
     * @param qtdFuncionarioFilial09
     *            the qtd funcionario filial09
     */
    public void setQtdFuncionarioFilial09(Long qtdFuncionarioFilial09) {
        this.qtdFuncionarioFilial09 = qtdFuncionarioFilial09;
    }

    /**
     * Get qtd funcionario filial09.
     * 
     * @return the qtd funcionario filial09
     */
    public Long getQtdFuncionarioFilial09() {
        return this.qtdFuncionarioFilial09;
    }

    /**
     * Set qtd funcionario filial10.
     * 
     * @param qtdFuncionarioFilial10
     *            the qtd funcionario filial10
     */
    public void setQtdFuncionarioFilial10(Long qtdFuncionarioFilial10) {
        this.qtdFuncionarioFilial10 = qtdFuncionarioFilial10;
    }

    /**
     * Get qtd funcionario filial10.
     * 
     * @return the qtd funcionario filial10
     */
    public Long getQtdFuncionarioFilial10() {
        return this.qtdFuncionarioFilial10;
    }

    /**
     * Get: faixa.
     * 
     * @return faixa
     */
    public String getFaixa() {
        setVlInicialFaixaSalarial(vlInicialFaixaSalarial == null ? BigDecimal.ZERO : vlInicialFaixaSalarial);
        setVlFinalFaixaSalarial(vlFinalFaixaSalarial == null ? BigDecimal.ZERO : vlFinalFaixaSalarial);
        
        String valorInicial = NumberUtils.format(vlInicialFaixaSalarial);
        String valorFinal = NumberUtils.format(vlFinalFaixaSalarial);

        if (vlInicialFaixaSalarial == null || vlInicialFaixaSalarial.compareTo(BigDecimal.ZERO) == 0) {
            return String.format("At� R$ %s", valorFinal);
        }

        if (BigDecimal.ZERO.compareTo(vlFinalFaixaSalarial) == 0
                || new BigDecimal("999999999999999.99").compareTo(vlFinalFaixaSalarial) == 0) {
            return String.format("Acima de R$ %s", valorInicial);
        }

        return String.format("De R$ %s at� R$ %s", valorInicial, valorFinal);

    }
}