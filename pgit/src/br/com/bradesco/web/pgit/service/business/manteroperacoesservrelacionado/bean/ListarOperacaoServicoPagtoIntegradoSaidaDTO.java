/*
 * Nome: br.com.bradesco.web.pgit.service.business.manteroperacoesservrelacionado.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.manteroperacoesservrelacionado.bean;

/**
 * Nome: ListarOperacaoServicoPagtoIntegradoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarOperacaoServicoPagtoIntegradoSaidaDTO{
	
	/** Atributo codMensagem. */
	private String codMensagem;
	
	/** Atributo mensagem. */
	private String mensagem;
	
	/** Atributo numeroLinhas. */
	private Integer numeroLinhas;
	
	/** Atributo cdprodutoServicoOperacao. */
	private Integer cdprodutoServicoOperacao;
	
	/** Atributo cdProdutoOperacaoRelacionado. */
	private Integer cdProdutoOperacaoRelacionado;
	
	/** Atributo cdOperacaoProdutoServico. */
	private Integer cdOperacaoProdutoServico;
	
	/** Atributo cdOperacaoServicoIntegrado. */
	private String cdOperacaoServicoIntegrado;
	
	/** Atributo dsTipoServico. */
	private String dsTipoServico;
	
	/** Atributo dsModalidadeServico. */
	private String dsModalidadeServico;
	
	/** Atributo dsOperacaoCatalogo. */
	private String dsOperacaoCatalogo;
	
	/** Atributo dsNatureza. */
	private String dsNatureza;
	
	/** Atributo tipoServicoFormatado. */
	private String tipoServicoFormatado;
	
	/** Atributo modalidadeServicoFormatado. */
	private String modalidadeServicoFormatado;
	
	/** Atributo operacaoCatalogoFormatado. */
	private String operacaoCatalogoFormatado;

	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem(){
		return codMensagem;
	}

	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem){
		this.codMensagem = codMensagem;
	}

	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem(){
		return mensagem;
	}

	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem){
		this.mensagem = mensagem;
	}

	/**
	 * Get: numeroLinhas.
	 *
	 * @return numeroLinhas
	 */
	public Integer getNumeroLinhas(){
		return numeroLinhas;
	}

	/**
	 * Set: numeroLinhas.
	 *
	 * @param numeroLinhas the numero linhas
	 */
	public void setNumeroLinhas(Integer numeroLinhas){
		this.numeroLinhas = numeroLinhas;
	}

	/**
	 * Get: cdprodutoServicoOperacao.
	 *
	 * @return cdprodutoServicoOperacao
	 */
	public Integer getCdprodutoServicoOperacao(){
		return cdprodutoServicoOperacao;
	}

	/**
	 * Set: cdprodutoServicoOperacao.
	 *
	 * @param cdprodutoServicoOperacao the cdproduto servico operacao
	 */
	public void setCdprodutoServicoOperacao(Integer cdprodutoServicoOperacao){
		this.cdprodutoServicoOperacao = cdprodutoServicoOperacao;
	}

	/**
	 * Get: cdProdutoOperacaoRelacionado.
	 *
	 * @return cdProdutoOperacaoRelacionado
	 */
	public Integer getCdProdutoOperacaoRelacionado(){
		return cdProdutoOperacaoRelacionado;
	}

	/**
	 * Set: cdProdutoOperacaoRelacionado.
	 *
	 * @param cdProdutoOperacaoRelacionado the cd produto operacao relacionado
	 */
	public void setCdProdutoOperacaoRelacionado(Integer cdProdutoOperacaoRelacionado){
		this.cdProdutoOperacaoRelacionado = cdProdutoOperacaoRelacionado;
	}

	/**
	 * Get: cdOperacaoProdutoServico.
	 *
	 * @return cdOperacaoProdutoServico
	 */
	public Integer getCdOperacaoProdutoServico(){
		return cdOperacaoProdutoServico;
	}

	/**
	 * Set: cdOperacaoProdutoServico.
	 *
	 * @param cdOperacaoProdutoServico the cd operacao produto servico
	 */
	public void setCdOperacaoProdutoServico(Integer cdOperacaoProdutoServico){
		this.cdOperacaoProdutoServico = cdOperacaoProdutoServico;
	}

	/**
	 * Get: cdOperacaoServicoIntegrado.
	 *
	 * @return cdOperacaoServicoIntegrado
	 */
	public String getCdOperacaoServicoIntegrado(){
		return cdOperacaoServicoIntegrado;
	}

	/**
	 * Set: cdOperacaoServicoIntegrado.
	 *
	 * @param cdOperacaoServicoIntegrado the cd operacao servico integrado
	 */
	public void setCdOperacaoServicoIntegrado(String cdOperacaoServicoIntegrado){
		this.cdOperacaoServicoIntegrado = cdOperacaoServicoIntegrado;
	}

	/**
	 * Get: dsTipoServico.
	 *
	 * @return dsTipoServico
	 */
	public String getDsTipoServico(){
		return dsTipoServico;
	}

	/**
	 * Set: dsTipoServico.
	 *
	 * @param dsTipoServico the ds tipo servico
	 */
	public void setDsTipoServico(String dsTipoServico){
		this.dsTipoServico = dsTipoServico;
	}

	/**
	 * Get: dsModalidadeServico.
	 *
	 * @return dsModalidadeServico
	 */
	public String getDsModalidadeServico(){
		return dsModalidadeServico;
	}

	/**
	 * Set: dsModalidadeServico.
	 *
	 * @param dsModalidadeServico the ds modalidade servico
	 */
	public void setDsModalidadeServico(String dsModalidadeServico){
		this.dsModalidadeServico = dsModalidadeServico;
	}

	/**
	 * Get: dsOperacaoCatalogo.
	 *
	 * @return dsOperacaoCatalogo
	 */
	public String getDsOperacaoCatalogo(){
		return dsOperacaoCatalogo;
	}

	/**
	 * Set: dsOperacaoCatalogo.
	 *
	 * @param dsOperacaoCatalogo the ds operacao catalogo
	 */
	public void setDsOperacaoCatalogo(String dsOperacaoCatalogo){
		this.dsOperacaoCatalogo = dsOperacaoCatalogo;
	}

	/**
	 * Get: modalidadeServicoFormatado.
	 *
	 * @return modalidadeServicoFormatado
	 */
	public String getModalidadeServicoFormatado() {
		return modalidadeServicoFormatado;
	}

	/**
	 * Set: modalidadeServicoFormatado.
	 *
	 * @param modalidadeServicoFormatado the modalidade servico formatado
	 */
	public void setModalidadeServicoFormatado(String modalidadeServicoFormatado) {
		this.modalidadeServicoFormatado = modalidadeServicoFormatado;
	}

	/**
	 * Get: operacaoCatalogoFormatado.
	 *
	 * @return operacaoCatalogoFormatado
	 */
	public String getOperacaoCatalogoFormatado() {
		return operacaoCatalogoFormatado;
	}

	/**
	 * Set: operacaoCatalogoFormatado.
	 *
	 * @param operacaoCatalogoFormatado the operacao catalogo formatado
	 */
	public void setOperacaoCatalogoFormatado(String operacaoCatalogoFormatado) {
		this.operacaoCatalogoFormatado = operacaoCatalogoFormatado;
	}

	/**
	 * Get: tipoServicoFormatado.
	 *
	 * @return tipoServicoFormatado
	 */
	public String getTipoServicoFormatado() {
		return tipoServicoFormatado;
	}

	/**
	 * Set: tipoServicoFormatado.
	 *
	 * @param tipoServicoFormatado the tipo servico formatado
	 */
	public void setTipoServicoFormatado(String tipoServicoFormatado) {
		this.tipoServicoFormatado = tipoServicoFormatado;
	}

	/**
	 * Get: dsNatureza.
	 *
	 * @return dsNatureza
	 */
	public String getDsNatureza() {
	    return dsNatureza;
	}

	/**
	 * Set: dsNatureza.
	 *
	 * @param dsNatureza the ds natureza
	 */
	public void setDsNatureza(String dsNatureza) {
	    this.dsNatureza = dsNatureza;
	}
	
	
}