package br.com.bradesco.web.pgit.service.data.pdc.consultarsolemiavimovtocliepagador;

import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterException;
import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcConnectorException;
import br.com.bradesco.web.pgit.service.data.pdc.consultarsolemiavimovtocliepagador.request.ConsultarSolEmiAviMovtoCliePagadorRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarsolemiavimovtocliepagador.response.ConsultarSolEmiAviMovtoCliePagadorResponse;

/**
 * 
 * <p>
 * <b>T�tulo:</b> Arquitetura Bradesco Canal Internet.
 * </p>
 * <p>
 * <b>Descri��o:</b>
 * </p>
 * <p>
 * Interface que define os m�todos que devem ser implementados em cada adaptador.
 * </p>
 * 
 * @author GFT Iberia Solutions / Emagine <BR/> copyright Copyright (c) 2006
 *         <BR/> created 26-Jun-2006 <BR/>
 * @version 1.0
 * Esta classe foi automaticamente gerada com 
 * <a href="http://www.bradesco.com.br">Gerador de Adaptadores</a> 
 */
public interface IConsultarSolEmiAviMovtoCliePagadorPDCAdapter {
    
    /**
     * M�todo utilizado para invocar um processo PDC.
     * 
     * @param request Objeto com os par�metros necess�rios para invocar um processo no PDC.
     * @return Objeto do tipo ConsultarSolEmiAviMovtoCliePagadorResponse 
     		   com o resultado da execu��o do processo no PDC.
     * @throws PdcAdapterException Representa qualquer exce��o que se pode produzir no Adaptador.
     * @throws PdcConnectorException Representa qualquer exce��o que se pode produzir no Conector.
     */
    ConsultarSolEmiAviMovtoCliePagadorResponse invokeProcess(ConsultarSolEmiAviMovtoCliePagadorRequest request) 
    								throws PdcAdapterException, PdcConnectorException;
    
}
