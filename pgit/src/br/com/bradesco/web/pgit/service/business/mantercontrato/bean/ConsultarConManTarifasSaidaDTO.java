/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantercontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantercontrato.bean;

import java.math.BigDecimal;

/**
 * Nome: ConsultarConManTarifasSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarConManTarifasSaidaDTO {
	
	/** Atributo codMensagem. */
	private String codMensagem;
	
	/** Atributo mensagem. */
	private String mensagem;
	
	/** Atributo cdProdutoServicoOperacao. */
	private int cdProdutoServicoOperacao;
	
	/** Atributo cdProdutoServicoRelacionado. */
	private int cdProdutoServicoRelacionado;
	
	/** Atributo cdOperacaoProdutoServico. */
	private int cdOperacaoProdutoServico;
	
	/** Atributo hrInclusaoRegistroHistorico. */
	private String hrInclusaoRegistroHistorico;
	
	/** Atributo dtInicioVigenciaTarifa. */
	private String dtInicioVigenciaTarifa;
	
	/** Atributo dtFimVigenciaTarifa. */
	private String dtFimVigenciaTarifa;
	
	/** Atributo vlTarifaContrato. */
	private BigDecimal vlTarifaContrato;
	
	/** Atributo vlTarifaReferenciaMinima. */
	private BigDecimal vlTarifaReferenciaMinima;
	
	/** Atributo vlTarifaReferenciaMaxima. */
	private BigDecimal vlTarifaReferenciaMaxima;
	
	/** Atributo dsProdutoServicoOperacao. */
	private String dsProdutoServicoOperacao;
	
	/** Atributo dsProdutoServicoRelacionado. */
	private String dsProdutoServicoRelacionado;
	
	/** Atributo dsOperacaoProdutoServico. */
	private String dsOperacaoProdutoServico;
	
	/** Atributo cdIndicadorTipoManutencao. */
	private int cdIndicadorTipoManutencao;
	
	/** Atributo dsIndicadorTipoManutencao. */
	private String dsIndicadorTipoManutencao;
	
	/** Atributo hrInclusaoRegistro. */
	private String hrInclusaoRegistro;
	
	/** Atributo cdUsuarioInclusao. */
	private String cdUsuarioInclusao;
	
	/** Atributo cdUsuarioInclusaoExterno. */
	private String cdUsuarioInclusaoExterno;
	
	/** Atributo cdCanalInclusao. */
	private int cdCanalInclusao;
	
	/** Atributo nrOperacaoFluxoInclusao. */
	private String nrOperacaoFluxoInclusao;
	
	/** Atributo hrManutencaoResgistro. */
	private String hrManutencaoResgistro;
	
	/** Atributo cdUsuarioManutecao. */
	private String cdUsuarioManutecao;
	
	/** Atributo cdUsuarioManutencaoExterno. */
	private String cdUsuarioManutencaoExterno;
	
	/** Atributo cdCanalManutencao. */
	private int cdCanalManutencao;
	
	/** Atributo nrOperacaoFluxoManuntencao. */
	private String nrOperacaoFluxoManuntencao;
	
	/** Atributo dsCanalInclusao. */
	private String dsCanalInclusao;
	
	/** Atributo dsCanalManutencao. */
	private String dsCanalManutencao;
	
	/**
	 * Get: cdCanalInclusao.
	 *
	 * @return cdCanalInclusao
	 */
	public int getCdCanalInclusao() {
		return cdCanalInclusao;
	}
	
	/**
	 * Set: cdCanalInclusao.
	 *
	 * @param cdCanalInclusao the cd canal inclusao
	 */
	public void setCdCanalInclusao(int cdCanalInclusao) {
		this.cdCanalInclusao = cdCanalInclusao;
	}
	
	/**
	 * Get: cdCanalManutencao.
	 *
	 * @return cdCanalManutencao
	 */
	public int getCdCanalManutencao() {
		return cdCanalManutencao;
	}
	
	/**
	 * Set: cdCanalManutencao.
	 *
	 * @param cdCanalManutencao the cd canal manutencao
	 */
	public void setCdCanalManutencao(int cdCanalManutencao) {
		this.cdCanalManutencao = cdCanalManutencao;
	}
	
	/**
	 * Get: cdIndicadorTipoManutencao.
	 *
	 * @return cdIndicadorTipoManutencao
	 */
	public int getCdIndicadorTipoManutencao() {
		return cdIndicadorTipoManutencao;
	}
	
	/**
	 * Set: cdIndicadorTipoManutencao.
	 *
	 * @param cdIndicadorTipoManutencao the cd indicador tipo manutencao
	 */
	public void setCdIndicadorTipoManutencao(int cdIndicadorTipoManutencao) {
		this.cdIndicadorTipoManutencao = cdIndicadorTipoManutencao;
	}
	
	/**
	 * Get: cdOperacaoProdutoServico.
	 *
	 * @return cdOperacaoProdutoServico
	 */
	public int getCdOperacaoProdutoServico() {
		return cdOperacaoProdutoServico;
	}
	
	/**
	 * Set: cdOperacaoProdutoServico.
	 *
	 * @param cdOperacaoProdutoServico the cd operacao produto servico
	 */
	public void setCdOperacaoProdutoServico(int cdOperacaoProdutoServico) {
		this.cdOperacaoProdutoServico = cdOperacaoProdutoServico;
	}
	
	/**
	 * Get: cdProdutoServicoOperacao.
	 *
	 * @return cdProdutoServicoOperacao
	 */
	public int getCdProdutoServicoOperacao() {
		return cdProdutoServicoOperacao;
	}
	
	/**
	 * Set: cdProdutoServicoOperacao.
	 *
	 * @param cdProdutoServicoOperacao the cd produto servico operacao
	 */
	public void setCdProdutoServicoOperacao(int cdProdutoServicoOperacao) {
		this.cdProdutoServicoOperacao = cdProdutoServicoOperacao;
	}
	
	/**
	 * Get: cdProdutoServicoRelacionado.
	 *
	 * @return cdProdutoServicoRelacionado
	 */
	public int getCdProdutoServicoRelacionado() {
		return cdProdutoServicoRelacionado;
	}
	
	/**
	 * Set: cdProdutoServicoRelacionado.
	 *
	 * @param cdProdutoServicoRelacionado the cd produto servico relacionado
	 */
	public void setCdProdutoServicoRelacionado(int cdProdutoServicoRelacionado) {
		this.cdProdutoServicoRelacionado = cdProdutoServicoRelacionado;
	}
	
	/**
	 * Get: cdUsuarioInclusao.
	 *
	 * @return cdUsuarioInclusao
	 */
	public String getCdUsuarioInclusao() {
		return cdUsuarioInclusao;
	}
	
	/**
	 * Set: cdUsuarioInclusao.
	 *
	 * @param cdUsuarioInclusao the cd usuario inclusao
	 */
	public void setCdUsuarioInclusao(String cdUsuarioInclusao) {
		this.cdUsuarioInclusao = cdUsuarioInclusao;
	}
	
	/**
	 * Get: cdUsuarioInclusaoExterno.
	 *
	 * @return cdUsuarioInclusaoExterno
	 */
	public String getCdUsuarioInclusaoExterno() {
		return cdUsuarioInclusaoExterno;
	}
	
	/**
	 * Set: cdUsuarioInclusaoExterno.
	 *
	 * @param cdUsuarioInclusaoExterno the cd usuario inclusao externo
	 */
	public void setCdUsuarioInclusaoExterno(String cdUsuarioInclusaoExterno) {
		this.cdUsuarioInclusaoExterno = cdUsuarioInclusaoExterno;
	}
	
	/**
	 * Get: cdUsuarioManutecao.
	 *
	 * @return cdUsuarioManutecao
	 */
	public String getCdUsuarioManutecao() {
		return cdUsuarioManutecao;
	}
	
	/**
	 * Set: cdUsuarioManutecao.
	 *
	 * @param cdUsuarioManutecao the cd usuario manutecao
	 */
	public void setCdUsuarioManutecao(String cdUsuarioManutecao) {
		this.cdUsuarioManutecao = cdUsuarioManutecao;
	}
	
	/**
	 * Get: cdUsuarioManutencaoExterno.
	 *
	 * @return cdUsuarioManutencaoExterno
	 */
	public String getCdUsuarioManutencaoExterno() {
		return cdUsuarioManutencaoExterno;
	}
	
	/**
	 * Set: cdUsuarioManutencaoExterno.
	 *
	 * @param cdUsuarioManutencaoExterno the cd usuario manutencao externo
	 */
	public void setCdUsuarioManutencaoExterno(String cdUsuarioManutencaoExterno) {
		this.cdUsuarioManutencaoExterno = cdUsuarioManutencaoExterno;
	}
	
	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}
	
	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}
	
	/**
	 * Get: dsCanalInclusao.
	 *
	 * @return dsCanalInclusao
	 */
	public String getDsCanalInclusao() {
		return dsCanalInclusao;
	}
	
	/**
	 * Set: dsCanalInclusao.
	 *
	 * @param dsCanalInclusao the ds canal inclusao
	 */
	public void setDsCanalInclusao(String dsCanalInclusao) {
		this.dsCanalInclusao = dsCanalInclusao;
	}
	
	/**
	 * Get: dsCanalManutencao.
	 *
	 * @return dsCanalManutencao
	 */
	public String getDsCanalManutencao() {
		return dsCanalManutencao;
	}
	
	/**
	 * Set: dsCanalManutencao.
	 *
	 * @param dsCanalManutencao the ds canal manutencao
	 */
	public void setDsCanalManutencao(String dsCanalManutencao) {
		this.dsCanalManutencao = dsCanalManutencao;
	}
	
	/**
	 * Get: dsIndicadorTipoManutencao.
	 *
	 * @return dsIndicadorTipoManutencao
	 */
	public String getDsIndicadorTipoManutencao() {
		return dsIndicadorTipoManutencao;
	}
	
	/**
	 * Set: dsIndicadorTipoManutencao.
	 *
	 * @param dsIndicadorTipoManutencao the ds indicador tipo manutencao
	 */
	public void setDsIndicadorTipoManutencao(String dsIndicadorTipoManutencao) {
		this.dsIndicadorTipoManutencao = dsIndicadorTipoManutencao;
	}
	
	/**
	 * Get: dsOperacaoProdutoServico.
	 *
	 * @return dsOperacaoProdutoServico
	 */
	public String getDsOperacaoProdutoServico() {
		return dsOperacaoProdutoServico;
	}
	
	/**
	 * Set: dsOperacaoProdutoServico.
	 *
	 * @param dsOperacaoProdutoServico the ds operacao produto servico
	 */
	public void setDsOperacaoProdutoServico(String dsOperacaoProdutoServico) {
		this.dsOperacaoProdutoServico = dsOperacaoProdutoServico;
	}
	
	/**
	 * Get: dsProdutoServicoOperacao.
	 *
	 * @return dsProdutoServicoOperacao
	 */
	public String getDsProdutoServicoOperacao() {
		return dsProdutoServicoOperacao;
	}
	
	/**
	 * Set: dsProdutoServicoOperacao.
	 *
	 * @param dsProdutoServicoOperacao the ds produto servico operacao
	 */
	public void setDsProdutoServicoOperacao(String dsProdutoServicoOperacao) {
		this.dsProdutoServicoOperacao = dsProdutoServicoOperacao;
	}
	
	/**
	 * Get: dsProdutoServicoRelacionado.
	 *
	 * @return dsProdutoServicoRelacionado
	 */
	public String getDsProdutoServicoRelacionado() {
		return dsProdutoServicoRelacionado;
	}
	
	/**
	 * Set: dsProdutoServicoRelacionado.
	 *
	 * @param dsProdutoServicoRelacionado the ds produto servico relacionado
	 */
	public void setDsProdutoServicoRelacionado(String dsProdutoServicoRelacionado) {
		this.dsProdutoServicoRelacionado = dsProdutoServicoRelacionado;
	}

	/**
	 * Get: dtInicioVigenciaTarifa.
	 *
	 * @return dtInicioVigenciaTarifa
	 */
	public String getDtInicioVigenciaTarifa() {
		return dtInicioVigenciaTarifa;
	}
	
	/**
	 * Set: dtInicioVigenciaTarifa.
	 *
	 * @param dtInicioVigenciaTarifa the dt inicio vigencia tarifa
	 */
	public void setDtInicioVigenciaTarifa(String dtInicioVigenciaTarifa) {
		this.dtInicioVigenciaTarifa = dtInicioVigenciaTarifa;
	}

	/**
	 * Get: hrInclusaoRegistro.
	 *
	 * @return hrInclusaoRegistro
	 */
	public String getHrInclusaoRegistro() {
		return hrInclusaoRegistro;
	}
	
	/**
	 * Set: hrInclusaoRegistro.
	 *
	 * @param hrInclusaoRegistro the hr inclusao registro
	 */
	public void setHrInclusaoRegistro(String hrInclusaoRegistro) {
		this.hrInclusaoRegistro = hrInclusaoRegistro;
	}
	
	/**
	 * Get: hrManutencaoResgistro.
	 *
	 * @return hrManutencaoResgistro
	 */
	public String getHrManutencaoResgistro() {
		return hrManutencaoResgistro;
	}
	
	/**
	 * Set: hrManutencaoResgistro.
	 *
	 * @param hrManutencaoResgistro the hr manutencao resgistro
	 */
	public void setHrManutencaoResgistro(String hrManutencaoResgistro) {
		this.hrManutencaoResgistro = hrManutencaoResgistro;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	/**
	 * Get: nrOperacaoFluxoInclusao.
	 *
	 * @return nrOperacaoFluxoInclusao
	 */
	public String getNrOperacaoFluxoInclusao() {
		return nrOperacaoFluxoInclusao;
	}
	
	/**
	 * Set: nrOperacaoFluxoInclusao.
	 *
	 * @param nrOperacaoFluxoInclusao the nr operacao fluxo inclusao
	 */
	public void setNrOperacaoFluxoInclusao(String nrOperacaoFluxoInclusao) {
		this.nrOperacaoFluxoInclusao = nrOperacaoFluxoInclusao;
	}
	
	/**
	 * Get: nrOperacaoFluxoManuntencao.
	 *
	 * @return nrOperacaoFluxoManuntencao
	 */
	public String getNrOperacaoFluxoManuntencao() {
		return nrOperacaoFluxoManuntencao;
	}
	
	/**
	 * Set: nrOperacaoFluxoManuntencao.
	 *
	 * @param nrOperacaoFluxoManuntencao the nr operacao fluxo manuntencao
	 */
	public void setNrOperacaoFluxoManuntencao(String nrOperacaoFluxoManuntencao) {
		this.nrOperacaoFluxoManuntencao = nrOperacaoFluxoManuntencao;
	}

	/**
	 * Get: hrInclusaoRegistroHistorico.
	 *
	 * @return hrInclusaoRegistroHistorico
	 */
	public String getHrInclusaoRegistroHistorico() {
		return hrInclusaoRegistroHistorico;
	}
	
	/**
	 * Set: hrInclusaoRegistroHistorico.
	 *
	 * @param hrInclusaoRegistroHistorico the hr inclusao registro historico
	 */
	public void setHrInclusaoRegistroHistorico(String hrInclusaoRegistroHistorico) {
		this.hrInclusaoRegistroHistorico = hrInclusaoRegistroHistorico;
	}
	
	/**
	 * Get: dtFimVigenciaTarifa.
	 *
	 * @return dtFimVigenciaTarifa
	 */
	public String getDtFimVigenciaTarifa() {
		return dtFimVigenciaTarifa;
	}
	
	/**
	 * Set: dtFimVigenciaTarifa.
	 *
	 * @param dtFimVigenciaTarifa the dt fim vigencia tarifa
	 */
	public void setDtFimVigenciaTarifa(String dtFimVigenciaTarifa) {
		this.dtFimVigenciaTarifa = dtFimVigenciaTarifa;
	}
	
	/**
	 * Get: vlTarifaContrato.
	 *
	 * @return vlTarifaContrato
	 */
	public BigDecimal getVlTarifaContrato() {
		return vlTarifaContrato;
	}
	
	/**
	 * Set: vlTarifaContrato.
	 *
	 * @param vlTarifaContrato the vl tarifa contrato
	 */
	public void setVlTarifaContrato(BigDecimal vlTarifaContrato) {
		this.vlTarifaContrato = vlTarifaContrato;
	}
	
	/**
	 * Get: vlTarifaReferenciaMaxima.
	 *
	 * @return vlTarifaReferenciaMaxima
	 */
	public BigDecimal getVlTarifaReferenciaMaxima() {
		return vlTarifaReferenciaMaxima;
	}
	
	/**
	 * Set: vlTarifaReferenciaMaxima.
	 *
	 * @param vlTarifaReferenciaMaxima the vl tarifa referencia maxima
	 */
	public void setVlTarifaReferenciaMaxima(BigDecimal vlTarifaReferenciaMaxima) {
		this.vlTarifaReferenciaMaxima = vlTarifaReferenciaMaxima;
	}
	
	/**
	 * Get: vlTarifaReferenciaMinima.
	 *
	 * @return vlTarifaReferenciaMinima
	 */
	public BigDecimal getVlTarifaReferenciaMinima() {
		return vlTarifaReferenciaMinima;
	}
	
	/**
	 * Set: vlTarifaReferenciaMinima.
	 *
	 * @param vlTarifaReferenciaMinima the vl tarifa referencia minima
	 */
	public void setVlTarifaReferenciaMinima(BigDecimal vlTarifaReferenciaMinima) {
		this.vlTarifaReferenciaMinima = vlTarifaReferenciaMinima;
	}
	
	
}
