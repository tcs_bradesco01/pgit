/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/implementation.ftl,v $
 * $Id: implementation.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: implementation.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.solmanutcontratos.impl;

import static br.com.bradesco.web.pgit.utils.PgitUtil.verificaIntegerNulo;
import static br.com.bradesco.web.pgit.utils.PgitUtil.verificaLongNulo;
import static br.com.bradesco.web.pgit.utils.PgitUtil.verificaStringNula;
import static br.com.bradesco.web.pgit.view.converters.FormatarData.formatarDataTrilha;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ConsultarManutencaoMeioTransmissaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ConsultarManutencaoMeioTransmissaoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.solmanutcontratos.ISolManutContratosService;
import br.com.bradesco.web.pgit.service.business.solmanutcontratos.bean.CancelarManutencaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.solmanutcontratos.bean.CancelarManutencaoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.solmanutcontratos.bean.ConsultarManutencaoConfigContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.solmanutcontratos.bean.ConsultarManutencaoConfigContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.solmanutcontratos.bean.ConsultarManutencaoContaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.solmanutcontratos.bean.ConsultarManutencaoContaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.solmanutcontratos.bean.ConsultarManutencaoParticipanteEntradaDTO;
import br.com.bradesco.web.pgit.service.business.solmanutcontratos.bean.ConsultarManutencaoParticipanteSaidaDTO;
import br.com.bradesco.web.pgit.service.business.solmanutcontratos.bean.ConsultarManutencaoServicoContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.solmanutcontratos.bean.ConsultarManutencaoServicoContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.solmanutcontratos.bean.ListarManutencaoContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.solmanutcontratos.bean.ListarManutencaoContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.data.pdc.FactoryAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.cancelarmanutencaoparticipante.request.CancelarManutencaoParticipanteRequest;
import br.com.bradesco.web.pgit.service.data.pdc.cancelarmanutencaoparticipante.response.CancelarManutencaoParticipanteResponse;
import br.com.bradesco.web.pgit.service.data.pdc.consultardetmanutencaoparticipante.request.ConsultarDetManutencaoParticipanteRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultardetmanutencaoparticipante.response.ConsultarDetManutencaoParticipanteResponse;
import br.com.bradesco.web.pgit.service.data.pdc.consultarmanutencaoconfigcontrato.request.ConsultarManutencaoConfigContratoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarmanutencaoconfigcontrato.response.ConsultarManutencaoConfigContratoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.consultarmanutencaocontacontrato.request.ConsultarManutencaoContaContratoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarmanutencaocontacontrato.response.ConsultarManutencaoContaContratoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.consultarmanutencaocontrato.request.ConsultarManutencaoContratoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarmanutencaocontrato.response.ConsultarManutencaoContratoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.consultarmanutencaomeiotransmissao.request.ConsultarManutencaoMeioTransmissaoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarmanutencaomeiotransmissao.response.ConsultarManutencaoMeioTransmissaoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.consultarmanutencaoservicocontrato.request.ConsultarManutencaoServicoContratoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarmanutencaoservicocontrato.response.ConsultarManutencaoServicoContratoResponse;
import br.com.bradesco.web.pgit.utils.PgitUtil;
import br.com.bradesco.web.pgit.view.converters.FormatarData;


/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Implementa��o do adaptador: SolManutContratos
 * </p>
 * 
 * @comment C�DIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public class SolManutContratosServiceImpl implements ISolManutContratosService {

    /** Atributo factoryAdapter. */
    private FactoryAdapter factoryAdapter;

    /**
     * Get: factoryAdapter.
     *
     * @return factoryAdapter
     */
    public FactoryAdapter getFactoryAdapter() {
        return factoryAdapter;
    }

    /**
     * Set: factoryAdapter.
     *
     * @param factoryAdapter the factory adapter
     */
    public void setFactoryAdapter(FactoryAdapter factoryAdapter) {
        this.factoryAdapter = factoryAdapter;
    }

    /**
     * Construtor.
     */
    public SolManutContratosServiceImpl() {
        // TODO: Implementa��o
    }


    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.solmanutcontratos.ISolManutContratosService#listarManutencaoContrato(br.com.bradesco.web.pgit.service.business.solmanutcontratos.bean.ListarManutencaoContratoEntradaDTO)
     */
    public List<ListarManutencaoContratoSaidaDTO> listarManutencaoContrato(ListarManutencaoContratoEntradaDTO listarManutencaoContratoEntradaDTO) {

        List<ListarManutencaoContratoSaidaDTO> listaRetorno = new ArrayList<ListarManutencaoContratoSaidaDTO>();		
        ConsultarManutencaoContratoRequest request = new ConsultarManutencaoContratoRequest();
        ConsultarManutencaoContratoResponse response = new ConsultarManutencaoContratoResponse();

        request.setCdIndicadorTipoManutencao(listarManutencaoContratoEntradaDTO.getCdAcao());
        request.setCdPessoaJuridicaContrato(listarManutencaoContratoEntradaDTO.getCdPessoaJuridicaContrato());
        request.setCdTipoContratoNegocio(listarManutencaoContratoEntradaDTO.getCdTipoContratoNegocio());
        request.setCdTipoManutencaoContrato(listarManutencaoContratoEntradaDTO.getCdTipoManutencaoContrato());
        request.setDtinicio(FormatarData.formataDiaMesAnoToPdc(listarManutencaoContratoEntradaDTO.getDtInicio()));
        request.setDtFim(FormatarData.formataDiaMesAnoToPdc(listarManutencaoContratoEntradaDTO.getDtFim()));
        request.setNrSequenciaContratoNegocio(listarManutencaoContratoEntradaDTO.getNrSequenciaContratoNegocio());		
        request.setCdSituacaoManutencaoContrato(listarManutencaoContratoEntradaDTO.getCdSituacaoManutencaoContrato());
        request.setNumeroOcorrencias(30);

        response = getFactoryAdapter().getConsultarManutencaoContratoPDCAdapter().invokeProcess(request);

        SimpleDateFormat formato1 = new SimpleDateFormat("yyyy-MM-dd-HH.mm.ss");		
        SimpleDateFormat formato2 = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

        ListarManutencaoContratoSaidaDTO saidaDTO;		
        for (int i = 0; i < response.getNumeroLinhas(); i++) {
            saidaDTO = new ListarManutencaoContratoSaidaDTO();

            saidaDTO.setCdIndicadorTipoManutencao(response.getOcorrencias(i).getCdIndicadorTipoManutencao());
            saidaDTO.setCdManutencao(response.getOcorrencias(i).getCdManutencao());
            saidaDTO.setCdSituacaoManutencaoContrato(response.getOcorrencias(i).getCdSituacaoManutencaoContrato());
            saidaDTO.setCdTipoManutencaoContrato(response.getOcorrencias(i).getCdTipoManutencaoContrato());
            saidaDTO.setDsIndicadorTipoManutencao(response.getOcorrencias(i).getDsIndicadorTipoManutencao());
            saidaDTO.setDsManutencaoContrato(response.getOcorrencias(i).getDsManutencaoContrato());
            saidaDTO.setDsSituacaoManutencaoContrato(response.getOcorrencias(i).getDsSituacaoManutencaoContrato());
            saidaDTO.setDsTipoManutencaoContrato(response.getOcorrencias(i).getDsTipoManutencaoContrato());



            if (response.getOcorrencias(i).getHrManutencaoRegistro() != null && response.getOcorrencias(i).getHrManutencaoRegistro().length() >=19 ){

                String stringData = response.getOcorrencias(i).getHrManutencaoRegistro().substring(0, 19);

                try {
                    saidaDTO.setHrManutencaoRegistro(formato2.format(formato1.parse(stringData)));
                }  catch (ParseException e) {
                    saidaDTO.setHrManutencaoRegistro("");
                }
            }else{
                saidaDTO.setHrManutencaoRegistro("");
            }


            //saidaDTO.setHrManutencaoRegistro(response.getOcorrencias(i).getHrManutencaoRegistro());
            listaRetorno.add(saidaDTO);
        }



        return listaRetorno;
    }

    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.solmanutcontratos.ISolManutContratosService
     * #detalharServicoContrato(br.com.bradesco.web.pgit.service.business.solmanutcontratos.
     * bean.ConsultarManutencaoServicoContratoEntradaDTO)
     */
    public ConsultarManutencaoServicoContratoSaidaDTO detalharServicoContrato(
        ConsultarManutencaoServicoContratoEntradaDTO consultarManutencaoServicoContratoEntradaDTO) {

        ConsultarManutencaoServicoContratoSaidaDTO saidaDTO = new ConsultarManutencaoServicoContratoSaidaDTO();

        ConsultarManutencaoServicoContratoRequest request = new ConsultarManutencaoServicoContratoRequest();
        ConsultarManutencaoServicoContratoResponse response = new ConsultarManutencaoServicoContratoResponse();

        request.setCdPessoaJuridica(consultarManutencaoServicoContratoEntradaDTO.getCdPessoaJuridica());
        request.setCdTipoContratoNegocio(consultarManutencaoServicoContratoEntradaDTO.getCdTipoContratoNegocio());
        request.setCdTipoManutencaoContrato(consultarManutencaoServicoContratoEntradaDTO.getCdTipoManutencaoContrato());
        request.setNrManutencaoContratoNegocio(consultarManutencaoServicoContratoEntradaDTO.getNrManutencaoContratoNegocio());
        request.setNrSequenciaContratoNegocio(consultarManutencaoServicoContratoEntradaDTO.getNrSequenciaContratoNegocio());


        response = getFactoryAdapter().getConsultarManutencaoServicoContratoPDCAdapter().invokeProcess(request); 

        saidaDTO = new ConsultarManutencaoServicoContratoSaidaDTO();
        saidaDTO.setCodMensagem(response.getCodMensagem());
        saidaDTO.setMensagem(response.getMensagem());

        //saidaDTO.setDsAcao(response.getDsAcaoManutencaoContrato()); //Pdc foi regerado e esse campo foi excluido
        saidaDTO.setDsMotivo(verificaStringNula(response.getDsMotivoManutencaoContrato()));
        saidaDTO.setDsServico(verificaStringNula(response.getDsTipoServico()));
        saidaDTO.setDsModalidade(verificaStringNula(response.getDsModalidadeServico()));
        saidaDTO.setDsSituacao(verificaStringNula(response.getDsSituacaoManutencaoContrato()));

        saidaDTO.setCdCanalInclusao(verificaIntegerNulo(response.getCdCanalInclusao()));
        saidaDTO.setDsCanalInclusao(verificaStringNula(response.getDsCanalInclusao())); 
        saidaDTO.setUsuarioInclusao(verificaStringNula(response.getCdUsuarioInclusao()));
        saidaDTO.setComplementoInclusao(verificaStringNula(response.getNrOperacaoFluxoInclusao()));
        saidaDTO.setDataHoraInclusao(formatarDataTrilha(response.getHrInclusaoRegistro()));

        saidaDTO.setCdCanalManutencao(verificaIntegerNulo(response.getCdCanalManutencao()));
        saidaDTO.setDsCanalManutencao(verificaStringNula(response.getDsCanalManutencao())); 
        saidaDTO.setUsuarioManutencao(verificaStringNula(String.valueOf(response.getCdUsuarioManutencao())));
        saidaDTO.setComplementoManutencao(verificaStringNula(response.getNrOperacaoFluxoManutencao()));
        saidaDTO.setDataHoraManutencao(verificaStringNula(FormatarData.formatarDataTrilha(response.getHrManutencaoRegistro())));

        saidaDTO.setCdPessoaJuridicaContrato(verificaLongNulo(response.getCdPessoaJuridicaContrato()));
        saidaDTO.setCdTipoContratoNegocio(verificaIntegerNulo(response.getCdTipoContratoNegocio()));
        saidaDTO.setNrSequenciaContratoNegocio(verificaLongNulo(response.getNrSequenciaContratoNegocio()));
        saidaDTO.setCdTipoManutencaoContrato(verificaIntegerNulo(response.getCdTipoManutencaoContrato()));
        saidaDTO.setNrManutencaoContratoNegocio(verificaLongNulo(response.getNrManutencaoContratoNegocio()));
        saidaDTO.setCdSituacaoManutencaoContrato(verificaIntegerNulo(response.getCdSituacaoManutencaoContrato()));
        saidaDTO.setCdMotivoManutencaoContrato(verificaIntegerNulo(response.getCdMotivoManutencaoContrato()));
        saidaDTO.setNrAditivoContratoNegocio(verificaLongNulo(response.getNrAditivoContratoNegocio()));
        saidaDTO.setCdTipoServico(verificaIntegerNulo(response.getCdTipoServico()));
        saidaDTO.setCdModalidadeServico(verificaIntegerNulo(response.getCdModalidadeServico()));
        saidaDTO.setCdUsuarioManutencaoExterno(verificaStringNula(response.getCdUsuarioManutencaoExterno()));
        saidaDTO.setCdUsuarioInclusaoExterno(verificaStringNula(response.getCdUsuarioInclusaoExterno()));


        saidaDTO.setDsTipoDataFloat(response.getDsTipoDataFloat());
        saidaDTO.setDsFloarServicoContratado(response.getDsFloarServicoContratado());
        saidaDTO.setDsMomentoProcessamentoPagamento(response.getDsMomentoProcessamentoPagamento());
        saidaDTO.setDsPagamentoNaoUtil(response.getDsPagamentoNaoUtil());
        saidaDTO.setDsIndicadorFeriadoLocal(response.getDsIndicadorFeriadoLocal());
        saidaDTO.setCdFloatServicoContratado(response.getCdFloatServicoContratado());
        saidaDTO.setCdPagamentoNaoUtil(response.getCdPagamentoNaoUtil());
        saidaDTO.setCdMomentoProcessamentoPagamento(response.getCdMomentoProcessamentoPagamento());
        saidaDTO.setCdIndicadorFeriadoLocal(response.getCdIndicadorFeriadoLocal());
        saidaDTO.setCdTipoDataFloat(response.getCdTipoDataFloat());
        saidaDTO.setCdIndicadorDiaFloat(response.getCdIndicadorDiaFloat());
        saidaDTO.setCdIndicadorDiaRepique(response.getCdIndicadorDiaRepique());
        saidaDTO.setQtdeDiaFloatPagamento(response.getQtdeDiaFloatPagamento());
        saidaDTO.setQtdeDiaRepiqueCons(response.getQtdeDiaRepiqueCons());

        return saidaDTO;

    }

    public ConsultarManutencaoConfigContratoSaidaDTO consultarManutencaoConfigContrato(
        ConsultarManutencaoConfigContratoEntradaDTO entradaDto) {

        ConsultarManutencaoConfigContratoRequest request = new ConsultarManutencaoConfigContratoRequest();
        request.setCdPessoaJuridica(verificaLongNulo(entradaDto.getCdPessoaJuridica()));
        request.setCdTipoContratoNegocio(verificaIntegerNulo(entradaDto.getCdTipoContratoNegocio()));
        request.setCdTipoManutencaoContrato(verificaIntegerNulo(entradaDto.getCdTipoManutencaoContrato()));
        request.setNrManutencaoContratoNegocio(verificaLongNulo(entradaDto.getNrManutencaoContratoNegocio()));
        request.setNrSequenciaContratoNegocio(verificaLongNulo(entradaDto.getNrSequenciaContratoNegocio()));
    
        ConsultarManutencaoConfigContratoResponse response = 
            getFactoryAdapter().getConsultarManutencaoConfigContratoPDCAdapter().invokeProcess(request); 

        ConsultarManutencaoConfigContratoSaidaDTO saidaDTO = new ConsultarManutencaoConfigContratoSaidaDTO();
        saidaDTO.setCodMensagem(response.getCodMensagem());
        saidaDTO.setMensagem(response.getMensagem());
        saidaDTO.setCdFormaAutorizacaoPagamento(response.getCdFormaAutorizacaoPagamento());
        saidaDTO.setCdIndicadorTipoManutencao(response.getCdIndicadorTipoManutencao());
        saidaDTO.setCdMotivoSituacaoManutencao(response.getCdMotivoSituacaoManutencao());
        saidaDTO.setCdSituacaoManutencaoContrato(response.getCdSituacaoManutencaoContrato());
        saidaDTO.setCdTipoCanalInclusao(response.getCdTipoCanalInclusao());
        saidaDTO.setCdTipoCanalManutencao(response.getCdTipoCanalManutencao());
        saidaDTO.setCdTipoContratoNegocio(response.getCdTipoContratoNegocio());
        saidaDTO.setCdTipoManutencaoContrato(response.getCdTipoManutencaoContrato());
        saidaDTO.setCdTipoResponsavelOrganizacao(response.getCdTipoResponsavelOrganizacao());
        saidaDTO.setCdUnidadeOrganizacional(response.getCdUnidadeOrganizacional());
        saidaDTO.setNrSequenciaUnidadeOrganizacional(response.getNrSequenciaUnidadeOrganizacional());
        saidaDTO.setCdFuncionarioGerenteContrato(response.getCdFuncionarioGerenteContrato());
        saidaDTO.setCdPessoaJuridica(response.getCdPessoaJuridica());
        saidaDTO.setCdpessoaJuridicaContrato(response.getCdpessoaJuridicaContrato());
        saidaDTO.setNrAditivoContratoNegocio(response.getNrAditivoContratoNegocio());
        saidaDTO.setNrManutencaoContratoNegocio(response.getNrManutencaoContratoNegocio());
        saidaDTO.setNrSequenciaContratoNegocio(response.getNrSequenciaContratoNegocio());
        saidaDTO.setCdOperacaoCanalInclusao(response.getCdOperacaoCanalInclusao());
        saidaDTO.setCdOperacaoCanalManutencao(response.getCdOperacaoCanalManutencao());
        saidaDTO.setCdUsuarioInclusao(response.getCdUsuarioInclusao());
        saidaDTO.setCdUsuarioInclusaoExterno(response.getCdUsuarioInclusaoExterno());
        saidaDTO.setCdUsuarioManutencao(response.getCdUsuarioManutencao());
        saidaDTO.setCdUsuarioManutencaoExterno(response.getCdUsuarioManutencaoExterno());
        saidaDTO.setDsFormaAutorizacaoPagamento(response.getDsFormaAutorizacaoPagamento());
        saidaDTO.setDsFuncionarioGerenteContrato(response.getDsFuncionarioGerenteContrato());
        saidaDTO.setDsIndicadorTipoManutencao(response.getDsIndicadorTipoManutencao());
        saidaDTO.setDsMotivoSituacaoManutencao(response.getDsMotivoSituacaoManutencao());
        saidaDTO.setDsSituacaoManutencaoContrato(response.getDsSituacaoManutencaoContrato());
        saidaDTO.setDsTipoCanalInclusao(response.getDsTipoCanalInclusao());
        saidaDTO.setDsTipoCanalManutencao(response.getDsTipoCanalManutencao());
        saidaDTO.setDsUnidadeOrganizacional(response.getDsUnidadeOrganizacional());
        saidaDTO.setHrInclusaoRegistro(formatarDataTrilha(response.getHrInclusaoRegistro()));
        saidaDTO.setHrManutencaoRegistro(formatarDataTrilha(response.getHrManutencaoRegistro()));

        return saidaDTO;
    }
    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.solmanutcontratos.ISolManutContratosService#detalharParticipanteContrato(br.com.bradesco.web.pgit.service.business.solmanutcontratos.bean.ConsultarManutencaoParticipanteEntradaDTO)
     */
    public ConsultarManutencaoParticipanteSaidaDTO detalharParticipanteContrato(ConsultarManutencaoParticipanteEntradaDTO consultarManutencaoParticipanteEntradaDTO) {

        ConsultarManutencaoParticipanteSaidaDTO saidaDTO = new ConsultarManutencaoParticipanteSaidaDTO();

        ConsultarDetManutencaoParticipanteRequest request = new ConsultarDetManutencaoParticipanteRequest();
        ConsultarDetManutencaoParticipanteResponse response = new ConsultarDetManutencaoParticipanteResponse();

        request.setCdPessoaJuridicaContrato(consultarManutencaoParticipanteEntradaDTO.getCdPessoaJuridica());
        request.setCdTipoContratoNegocio(consultarManutencaoParticipanteEntradaDTO.getCdTipoContratoNegocio());
        request.setCdTipoManutencaoContrato(consultarManutencaoParticipanteEntradaDTO.getCdTipoManutencaoContrato());
        request.setNrManutencaoContratoNegocio(consultarManutencaoParticipanteEntradaDTO.getNrManutencaoContratoNegocio());
        request.setNrSequenciaContratoNegocio(consultarManutencaoParticipanteEntradaDTO.getNrSequenciaContratoNegocio());

        response = getFactoryAdapter().getConsultarDetManutencaoParticipantePDCAdapter().invokeProcess(request); 

        SimpleDateFormat formato1 = new SimpleDateFormat("dd.MM.yyyy");		
        SimpleDateFormat formato2 = new SimpleDateFormat("dd/MM/yyyy");

        saidaDTO = new ConsultarManutencaoParticipanteSaidaDTO();
        saidaDTO.setCodMensagem(response.getCodMensagem());
        saidaDTO.setMensagem(response.getMensagem());	
        saidaDTO.setCpfParticipante(response.getCdCpfCnpj());
        saidaDTO.setNomeParticipane(response.getNmParticipante());

        try {			
            saidaDTO.setDataNascimento((formato2.format(formato1.parse(response.getDtNascimento()))));
        }  catch (ParseException e) {
            saidaDTO.setDataNascimento("");
        }




        saidaDTO.setDsGrupoEconomicoParticipante(response.getDsGrupoEconomico());
        saidaDTO.setDsTipoParticipacaoPessoa(response.getDsTipoParticipacaoPessoa());
        saidaDTO.setCdSituacaoManutencaoContrato(response.getCdSituacaoManutencaoContrato());
        saidaDTO.setDsSituacaoManutencaoContrato(response.getDsSituacaoManutencaoContrato());
        saidaDTO.setCdMotivoManutencaoContrato(response.getCdMotivoManutencaoContrato());
        saidaDTO.setDsMotivoManutencaoContrato(response.getDsMotivoManutencaoContrato());		
        saidaDTO.setCdIndicadorTipoManutencao(response.getCdIndicadorTipoManutencao());
        saidaDTO.setDsIndicadorTipoManutencao(response.getDsIndicadorTipoManutencao());
        saidaDTO.setNrAditivoContratoNegocio(response.getNrAditivoContratoNegocio());


        saidaDTO.setUsuarioSolicitante(response.getCdUsuarioInclusao());
        saidaDTO.setDataHoraSolicitacao(response.getHrManutencaoRegistro());
        saidaDTO.setCdCanalInclusao(response.getCdTipoCanalInclusao());
        saidaDTO.setDsCanalInclusao(response.getDsCanalInclusao()); 
        saidaDTO.setUsuarioInclusao(response.getCdUsuarioInclusao());
        saidaDTO.setComplementoInclusao(response.getCdOpercacaoCanalInclusao());
        saidaDTO.setDataHoraInclusao(FormatarData.formatarDataTrilha(response.getHrInclusaoRegistro()));		
        saidaDTO.setCdCanalManutencao(response.getCdTipoCanalManutencao());
        saidaDTO.setDsCanalManutencao(response.getDsCanalManutencao()); 
        saidaDTO.setUsuarioManutencao(response.getCdUsuarioManutencao());
        saidaDTO.setComplementoManutencao(response.getCdOpercaoCanalManutencao());
        saidaDTO.setDataHoraManutencao(FormatarData.formatarDataTrilha(response.getHrManutencaoRegistro())); 

        return saidaDTO;

    }

    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.solmanutcontratos.ISolManutContratosService#detalharContaContrato(br.com.bradesco.web.pgit.service.business.solmanutcontratos.bean.ConsultarManutencaoContaEntradaDTO)
     */
    public ConsultarManutencaoContaSaidaDTO detalharContaContrato(ConsultarManutencaoContaEntradaDTO consultarManutencaoContaEntradaDTO) {

        ConsultarManutencaoContaSaidaDTO saidaDTO = new ConsultarManutencaoContaSaidaDTO();

        ConsultarManutencaoContaContratoRequest request = new ConsultarManutencaoContaContratoRequest();
        ConsultarManutencaoContaContratoResponse response = new ConsultarManutencaoContaContratoResponse();

        request.setCdPessoaJuridica(consultarManutencaoContaEntradaDTO.getCdPessoaJuridica());
        request.setCdTipoContratoNegocio(consultarManutencaoContaEntradaDTO.getCdTipoContratoNegocio());
        request.setCdTipoManutencaoContrato(consultarManutencaoContaEntradaDTO.getCdTipoManutencaoContrato());
        request.setNrManutencaoContratoNegocio(consultarManutencaoContaEntradaDTO.getNrManutencaoContratoNegocio());
        request.setNrSequenciaContratoNegocio(consultarManutencaoContaEntradaDTO.getNrSequenciaContratoNegocio());

        response = getFactoryAdapter().getConsultarManutencaoContaContratoPDCAdapter().invokeProcess(request); 

        saidaDTO = new ConsultarManutencaoContaSaidaDTO();
        saidaDTO.setCodMensagem(response.getCodMensagem());
        saidaDTO.setMensagem(response.getMensagem());
        saidaDTO.setCdPessoaJuridicaContrato(response.getCdPessoaJuridicaContrato());
        saidaDTO.setCdTipoContratoNegocio(response.getCdTipoContratoNegocio());
        saidaDTO.setNrSequenciaContratoNegocio(response.getNrSequenciaContratoNegocio());
        saidaDTO.setCdTipoManutencaoContrato(response.getCdTipoManutencaoContrato());
        saidaDTO.setNrManutencaoContratoNegocio(response.getNrManutencaoContratoNegocio());
        saidaDTO.setCdSituacaoManutencaoContrato(response.getCdSituacaoManutencaoContrato());
        saidaDTO.setDsSituacaoManutencaoContrato(response.getDsSituacaoManutencaoContrato());
        saidaDTO.setCdMotivoManutencaoContrato(response.getCdMotivoManutencaoContrato());
        saidaDTO.setDsMotivoManutencaoContrato(response.getDsMotivoManutencaoContrato());
        saidaDTO.setNrAditivoContratoNegocio(response.getNrAditivoContratoNegocio());
        saidaDTO.setCdIndicadorTipoManutencao(response.getCdIndicadorTipoManutencao());
        saidaDTO.setDsIndicadorTipoManutencao(response.getDsIndicadorTipoManutencao());
        saidaDTO.setDsTipoContaContrato(response.getDsTipoContaContrato());
        saidaDTO.setDtVinculo(response.getDtVinculo());
        saidaDTO.setCdSituacaoVinculo(response.getCdSituacaoVinculo());
        saidaDTO.setCdBanco(response.getCdBanco());
        saidaDTO.setDsBanco(response.getDsBanco());
        saidaDTO.setCdComercialAgenciaContabil(response.getCdComercialAgenciaContabil());
        saidaDTO.setDsComercialAgenciaContabil(response.getDsComercialAgenciaContabil());
        saidaDTO.setNrConta(response.getNrConta());
        saidaDTO.setCdDigitoConta(response.getCdDigitoConta());
        saidaDTO.setCdCpfCnpj(response.getCdCpfCnpj());
        saidaDTO.setNmPessoa(response.getNmPessoa());
        saidaDTO.setHrInclusaoRegistro(FormatarData.formatarDataTrilha(response.getHrInclusaoRegistro()));
        saidaDTO.setHrManutencaoRegistro(FormatarData.formatarDataTrilha(response.getHrManutencaoRegistro()));
        saidaDTO.setCdUsuarioInclusao(response.getCdUsuarioInclusao());
        saidaDTO.setCdUsuarioInclusaoExterno(response.getCdUsuarioInclusaoExterno());
        saidaDTO.setCdOperacaoCanalInclusao(response.getCdOperacaoCanalInclusao());
        saidaDTO.setCdTipoCanalInclusao(response.getCdTipoCanalInclusao());
        saidaDTO.setDsCanalInclusao(response.getDsCanalInclusao());
        saidaDTO.setCdusuarioManutencao(response.getCdusuarioManutencao());
        saidaDTO.setCdUsuarioManutencaoExterno(response.getCdUsuarioManutencaoExterno());
        saidaDTO.setCdOperacaoCanalManutencao(response.getCdOperacaoCanalManutencao());
        saidaDTO.setCdTipoCanalManutencao(response.getCdTipoCanalManutencao());
        saidaDTO.setDsCanalManutencao(response.getDsCanalManutencao());

        return saidaDTO;

    }

    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.solmanutcontratos.ISolManutContratosService#cancelarManutencao(br.com.bradesco.web.pgit.service.business.solmanutcontratos.bean.CancelarManutencaoEntradaDTO)
     */
    public CancelarManutencaoSaidaDTO cancelarManutencao(CancelarManutencaoEntradaDTO cancelarManutencaoEntradaDTO) {
        CancelarManutencaoSaidaDTO cancelarManutencaoSaidaDTO = new CancelarManutencaoSaidaDTO();
        CancelarManutencaoParticipanteRequest cancelarManutencaoParticipanteRequest = new CancelarManutencaoParticipanteRequest();
        CancelarManutencaoParticipanteResponse cancelarManutencaoParticipanteResponse = new CancelarManutencaoParticipanteResponse();

        cancelarManutencaoParticipanteRequest.setCdPessoaJuridicaContrato(cancelarManutencaoEntradaDTO.getCdPessoaJuridica());
        cancelarManutencaoParticipanteRequest.setCdTipoContratoNegocio(cancelarManutencaoEntradaDTO.getCdTipoContratoNegocio()) ;
        cancelarManutencaoParticipanteRequest.setCdTipoManutencaoContrato(cancelarManutencaoEntradaDTO.getCdTipoManutencaoContrato());
        cancelarManutencaoParticipanteRequest.setNrManutencaoContratoNegocio(cancelarManutencaoEntradaDTO.getNrManutencaoContratoNegocio());
        cancelarManutencaoParticipanteRequest.setNrSequenciaContratoNegocio(cancelarManutencaoEntradaDTO.getNrSequenciaContratoNegocio());

        cancelarManutencaoParticipanteResponse = getFactoryAdapter().getCancelarManutencaoParticipantePDCAdapter().invokeProcess(cancelarManutencaoParticipanteRequest);

        cancelarManutencaoSaidaDTO.setCodMensagem(cancelarManutencaoParticipanteResponse.getCodMensagem());
        cancelarManutencaoSaidaDTO.setMensagem(cancelarManutencaoParticipanteResponse.getMensagem());

        return cancelarManutencaoSaidaDTO;
    }

    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.solmanutcontratos.ISolManutContratosService#consultarManutencaoMeioTransmissao(br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ConsultarManutencaoMeioTransmissaoEntradaDTO)
     */
    public ConsultarManutencaoMeioTransmissaoSaidaDTO consultarManutencaoMeioTransmissao(
        ConsultarManutencaoMeioTransmissaoEntradaDTO entradaDTO) {
        ConsultarManutencaoMeioTransmissaoSaidaDTO saida =  new ConsultarManutencaoMeioTransmissaoSaidaDTO();
        ConsultarManutencaoMeioTransmissaoRequest request = new ConsultarManutencaoMeioTransmissaoRequest();
        ConsultarManutencaoMeioTransmissaoResponse response = new ConsultarManutencaoMeioTransmissaoResponse(); 

        request.setCdpessoaJuridicaContrato(PgitUtil.verificaLongNulo(entradaDTO.getCdpessoaJuridicaContrato()));
        request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoContratoNegocio()));
        request.setCdTipoManutencaoContrato(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoManutencaoContrato()));
        request.setNrManutencaoContratoNegocio(PgitUtil.verificaLongNulo(entradaDTO.getNrManutencaoContratoNegocio()));
        request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(entradaDTO.getNrSequenciaContratoNegocio()));

        response = getFactoryAdapter().getConsultarManutencaoMeioTransmissaoPDCAdapter().invokeProcess(request);

        saida.setCdAplicacaoTransmPagamento(response.getCdAplicacaoTransmPagamento());
        saida.setCdCodMeioPrincipalRetorno(response.getCdCodMeioPrincipalRetorno());
        saida.setCdIndicadorTipoManutencao(response.getCdIndicadorTipoManutencao());
        saida.setCdMeioAlternRemessa(response.getCdMeioAlternRemessa());
        saida.setCdMeioAltrnRetorno(response.getCdMeioAltrnRetorno());
        saida.setCdMeioPrincipalRemessa(response.getCdMeioPrincipalRemessa());
        saida.setCdMeioPrincipalRetorno(response.getCdMeioPrincipalRetorno());
        saida.setCdMotivoManutContr(response.getCdMotivoManutContr());
        saida.setCdOperacaoCanalInclusao(response.getCdOperacaoCanalInclusao());
        saida.setCdOperacaoCanalManutencao(response.getCdOperacaoCanalManutencao());
        saida.setCdPerfilTrocaArq(response.getCdPerfilTrocaArq());
        saida.setCdPessoaJuridica(response.getCdPessoaJuridica());
        saida.setCdPessoaJuridicaParceiro(response.getCdPessoaJuridicaParceiro());
        saida.setCdResponsavelCustoEmpresa(response.getCdResponsavelCustoEmpresa());
        saida.setCdSituacaoManutencaoContrato(response.getCdSituacaoManutencaoContrato());
        saida.setCdTipoCanalInclusao(response.getCdTipoCanalInclusao());
        saida.setCdTipoContratoNegocio(response.getCdTipoContratoNegocio());
        saida.setCdTipoLayoutArquivo(response.getCdTipoLayoutArquivo());
        saida.setCdTipoManutencaoContrato(response.getCdTipoManutencaoContrato());
        saida.setCdUnidadeOrganizacional(response.getCdUnidadeOrganizacional());
        saida.setCdUsuarioInclusao(response.getCdUsuarioInclusao());
        saida.setCdUsuarioInclusaoExterno(response.getCdUsuarioInclusaoExterno());
        saida.setCdUsuarioManutencao(response.getCdUsuarioManutencao());
        saida.setCdUsuarioManutencaoexterno(response.getCdUsuarioManutencaoexterno());
        saida.setCodMensagem(response.getCodMensagem());
        saida.setDsAplicFormat(response.getDsAplicFormat());
        saida.setDsCanalInclusao(response.getDsCanalInclusao());
        saida.setDsCanalManutencao(response.getDsCanalManutencao());
        saida.setDsCodMeioAlternRemessa(response.getDsCodMeioAlternRemessa());
        saida.setDsCodMeioPrincipalRemessa(response.getDsCodMeioPrincipalRemessa());
        saida.setDsCodTipoLayout(response.getDsCodTipoLayout());
        saida.setDsEmpresa(response.getDsEmpresa());
        saida.setDsIndicadorTipoManutencao(response.getDsIndicadorTipoManutencao());
        saida.setDsLayoutProprio(response.getDsLayoutProprio());
        saida.setDsMeioAltrnRetorno(response.getDsMeioAltrnRetorno());
        saida.setDsMotivoManuContr(response.getDsMotivoManuContr());
        saida.setDsResponsavelCustoEmpresa(response.getDsResponsavelCustoEmpresa());
        saida.setDsSituacaoManutContr(response.getDsSituacaoManutContr());
        saida.setDsUtilizacaoEmpresaVan(response.getDsUtilizacaoEmpresaVan());
        saida.setHrInclusaoRegistro(FormatarData.formatarDataTrilha(response.getHrInclusaoRegistro()));
        saida.setHrManutencaoRegistro(FormatarData.formatarDataTrilha(response.getHrManutencaoRegistro()));
        saida.setMensagem(response.getMensagem());
        saida.setNrAditivoContratoNegocio(response.getNrAditivoContratoNegocio());
        saida.setNrManutencaoContratoNegocio(response.getNrManutencaoContratoNegocio());
        saida.setNrSequenciaContratoNegocio(response.getNrSequenciaContratoNegocio());
        saida.setPcCustoOrganizacaoTransmissao(response.getPcCustoOrganizacaoTransmissao());
        saida.setQtMesRegistroTrafg(response.getQtMesRegistroTrafg());
        saida.setCdPessoaJuridicaContrato(response.getCdPessoaJuridicaContrato());
        saida.setCdTipoCanalManutencao(response.getCdTipoCanalManutencao());

        return saida;
    }



}

