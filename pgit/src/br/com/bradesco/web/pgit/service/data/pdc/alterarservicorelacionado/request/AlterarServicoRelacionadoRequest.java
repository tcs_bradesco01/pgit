/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.alterarservicorelacionado.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class AlterarServicoRelacionadoRequest.
 * 
 * @version $Revision$ $Date$
 */
public class AlterarServicoRelacionadoRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdProdutoServicoOperacao
     */
    private int _cdProdutoServicoOperacao = 0;

    /**
     * keeps track of state for field: _cdProdutoServicoOperacao
     */
    private boolean _has_cdProdutoServicoOperacao;

    /**
     * Field _cdProdutoOperacaoRelacionado
     */
    private int _cdProdutoOperacaoRelacionado = 0;

    /**
     * keeps track of state for field: _cdProdutoOperacaoRelacionado
     */
    private boolean _has_cdProdutoOperacaoRelacionado;

    /**
     * Field _cdRelacionamentoProduto
     */
    private int _cdRelacionamentoProduto = 0;

    /**
     * keeps track of state for field: _cdRelacionamentoProduto
     */
    private boolean _has_cdRelacionamentoProduto;

    /**
     * Field _cdServicoCompostoPagamento
     */
    private long _cdServicoCompostoPagamento = 0;

    /**
     * keeps track of state for field: _cdServicoCompostoPagamento
     */
    private boolean _has_cdServicoCompostoPagamento;

    /**
     * Field _cdNaturezaOperacaoPagamento
     */
    private int _cdNaturezaOperacaoPagamento = 0;

    /**
     * keeps track of state for field: _cdNaturezaOperacaoPagamento
     */
    private boolean _has_cdNaturezaOperacaoPagamento;

    /**
     * Field _nrOrdenacaoServicoRelacionado
     */
    private int _nrOrdenacaoServicoRelacionado = 0;

    /**
     * keeps track of state for field: _nrOrdenacaoServicoRelacionad
     */
    private boolean _has_nrOrdenacaoServicoRelacionado;

    /**
     * Field _cdIndicadorServicoNegociavel
     */
    private int _cdIndicadorServicoNegociavel = 0;

    /**
     * keeps track of state for field: _cdIndicadorServicoNegociavel
     */
    private boolean _has_cdIndicadorServicoNegociavel;

    /**
     * Field _cdIndicadorNegociavelServico
     */
    private int _cdIndicadorNegociavelServico = 0;

    /**
     * keeps track of state for field: _cdIndicadorNegociavelServico
     */
    private boolean _has_cdIndicadorNegociavelServico;

    /**
     * Field _cdIndicadorContigenciaServico
     */
    private int _cdIndicadorContigenciaServico = 0;

    /**
     * keeps track of state for field: _cdIndicadorContigenciaServic
     */
    private boolean _has_cdIndicadorContigenciaServico;

    /**
     * Field _cdIndicadorManutencaoServico
     */
    private int _cdIndicadorManutencaoServico = 0;

    /**
     * keeps track of state for field: _cdIndicadorManutencaoServico
     */
    private boolean _has_cdIndicadorManutencaoServico;


      //----------------/
     //- Constructors -/
    //----------------/

    public AlterarServicoRelacionadoRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.alterarservicorelacionado.request.AlterarServicoRelacionadoRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdIndicadorContigenciaServico
     * 
     */
    public void deleteCdIndicadorContigenciaServico()
    {
        this._has_cdIndicadorContigenciaServico= false;
    } //-- void deleteCdIndicadorContigenciaServico() 

    /**
     * Method deleteCdIndicadorManutencaoServico
     * 
     */
    public void deleteCdIndicadorManutencaoServico()
    {
        this._has_cdIndicadorManutencaoServico= false;
    } //-- void deleteCdIndicadorManutencaoServico() 

    /**
     * Method deleteCdIndicadorNegociavelServico
     * 
     */
    public void deleteCdIndicadorNegociavelServico()
    {
        this._has_cdIndicadorNegociavelServico= false;
    } //-- void deleteCdIndicadorNegociavelServico() 

    /**
     * Method deleteCdIndicadorServicoNegociavel
     * 
     */
    public void deleteCdIndicadorServicoNegociavel()
    {
        this._has_cdIndicadorServicoNegociavel= false;
    } //-- void deleteCdIndicadorServicoNegociavel() 

    /**
     * Method deleteCdNaturezaOperacaoPagamento
     * 
     */
    public void deleteCdNaturezaOperacaoPagamento()
    {
        this._has_cdNaturezaOperacaoPagamento= false;
    } //-- void deleteCdNaturezaOperacaoPagamento() 

    /**
     * Method deleteCdProdutoOperacaoRelacionado
     * 
     */
    public void deleteCdProdutoOperacaoRelacionado()
    {
        this._has_cdProdutoOperacaoRelacionado= false;
    } //-- void deleteCdProdutoOperacaoRelacionado() 

    /**
     * Method deleteCdProdutoServicoOperacao
     * 
     */
    public void deleteCdProdutoServicoOperacao()
    {
        this._has_cdProdutoServicoOperacao= false;
    } //-- void deleteCdProdutoServicoOperacao() 

    /**
     * Method deleteCdRelacionamentoProduto
     * 
     */
    public void deleteCdRelacionamentoProduto()
    {
        this._has_cdRelacionamentoProduto= false;
    } //-- void deleteCdRelacionamentoProduto() 

    /**
     * Method deleteCdServicoCompostoPagamento
     * 
     */
    public void deleteCdServicoCompostoPagamento()
    {
        this._has_cdServicoCompostoPagamento= false;
    } //-- void deleteCdServicoCompostoPagamento() 

    /**
     * Method deleteNrOrdenacaoServicoRelacionado
     * 
     */
    public void deleteNrOrdenacaoServicoRelacionado()
    {
        this._has_nrOrdenacaoServicoRelacionado= false;
    } //-- void deleteNrOrdenacaoServicoRelacionado() 

    /**
     * Returns the value of field 'cdIndicadorContigenciaServico'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorContigenciaServico'.
     */
    public int getCdIndicadorContigenciaServico()
    {
        return this._cdIndicadorContigenciaServico;
    } //-- int getCdIndicadorContigenciaServico() 

    /**
     * Returns the value of field 'cdIndicadorManutencaoServico'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorManutencaoServico'.
     */
    public int getCdIndicadorManutencaoServico()
    {
        return this._cdIndicadorManutencaoServico;
    } //-- int getCdIndicadorManutencaoServico() 

    /**
     * Returns the value of field 'cdIndicadorNegociavelServico'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorNegociavelServico'.
     */
    public int getCdIndicadorNegociavelServico()
    {
        return this._cdIndicadorNegociavelServico;
    } //-- int getCdIndicadorNegociavelServico() 

    /**
     * Returns the value of field 'cdIndicadorServicoNegociavel'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorServicoNegociavel'.
     */
    public int getCdIndicadorServicoNegociavel()
    {
        return this._cdIndicadorServicoNegociavel;
    } //-- int getCdIndicadorServicoNegociavel() 

    /**
     * Returns the value of field 'cdNaturezaOperacaoPagamento'.
     * 
     * @return int
     * @return the value of field 'cdNaturezaOperacaoPagamento'.
     */
    public int getCdNaturezaOperacaoPagamento()
    {
        return this._cdNaturezaOperacaoPagamento;
    } //-- int getCdNaturezaOperacaoPagamento() 

    /**
     * Returns the value of field 'cdProdutoOperacaoRelacionado'.
     * 
     * @return int
     * @return the value of field 'cdProdutoOperacaoRelacionado'.
     */
    public int getCdProdutoOperacaoRelacionado()
    {
        return this._cdProdutoOperacaoRelacionado;
    } //-- int getCdProdutoOperacaoRelacionado() 

    /**
     * Returns the value of field 'cdProdutoServicoOperacao'.
     * 
     * @return int
     * @return the value of field 'cdProdutoServicoOperacao'.
     */
    public int getCdProdutoServicoOperacao()
    {
        return this._cdProdutoServicoOperacao;
    } //-- int getCdProdutoServicoOperacao() 

    /**
     * Returns the value of field 'cdRelacionamentoProduto'.
     * 
     * @return int
     * @return the value of field 'cdRelacionamentoProduto'.
     */
    public int getCdRelacionamentoProduto()
    {
        return this._cdRelacionamentoProduto;
    } //-- int getCdRelacionamentoProduto() 

    /**
     * Returns the value of field 'cdServicoCompostoPagamento'.
     * 
     * @return long
     * @return the value of field 'cdServicoCompostoPagamento'.
     */
    public long getCdServicoCompostoPagamento()
    {
        return this._cdServicoCompostoPagamento;
    } //-- long getCdServicoCompostoPagamento() 

    /**
     * Returns the value of field 'nrOrdenacaoServicoRelacionado'.
     * 
     * @return int
     * @return the value of field 'nrOrdenacaoServicoRelacionado'.
     */
    public int getNrOrdenacaoServicoRelacionado()
    {
        return this._nrOrdenacaoServicoRelacionado;
    } //-- int getNrOrdenacaoServicoRelacionado() 

    /**
     * Method hasCdIndicadorContigenciaServico
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorContigenciaServico()
    {
        return this._has_cdIndicadorContigenciaServico;
    } //-- boolean hasCdIndicadorContigenciaServico() 

    /**
     * Method hasCdIndicadorManutencaoServico
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorManutencaoServico()
    {
        return this._has_cdIndicadorManutencaoServico;
    } //-- boolean hasCdIndicadorManutencaoServico() 

    /**
     * Method hasCdIndicadorNegociavelServico
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorNegociavelServico()
    {
        return this._has_cdIndicadorNegociavelServico;
    } //-- boolean hasCdIndicadorNegociavelServico() 

    /**
     * Method hasCdIndicadorServicoNegociavel
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorServicoNegociavel()
    {
        return this._has_cdIndicadorServicoNegociavel;
    } //-- boolean hasCdIndicadorServicoNegociavel() 

    /**
     * Method hasCdNaturezaOperacaoPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdNaturezaOperacaoPagamento()
    {
        return this._has_cdNaturezaOperacaoPagamento;
    } //-- boolean hasCdNaturezaOperacaoPagamento() 

    /**
     * Method hasCdProdutoOperacaoRelacionado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdutoOperacaoRelacionado()
    {
        return this._has_cdProdutoOperacaoRelacionado;
    } //-- boolean hasCdProdutoOperacaoRelacionado() 

    /**
     * Method hasCdProdutoServicoOperacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdutoServicoOperacao()
    {
        return this._has_cdProdutoServicoOperacao;
    } //-- boolean hasCdProdutoServicoOperacao() 

    /**
     * Method hasCdRelacionamentoProduto
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdRelacionamentoProduto()
    {
        return this._has_cdRelacionamentoProduto;
    } //-- boolean hasCdRelacionamentoProduto() 

    /**
     * Method hasCdServicoCompostoPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdServicoCompostoPagamento()
    {
        return this._has_cdServicoCompostoPagamento;
    } //-- boolean hasCdServicoCompostoPagamento() 

    /**
     * Method hasNrOrdenacaoServicoRelacionado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrOrdenacaoServicoRelacionado()
    {
        return this._has_nrOrdenacaoServicoRelacionado;
    } //-- boolean hasNrOrdenacaoServicoRelacionado() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdIndicadorContigenciaServico'.
     * 
     * @param cdIndicadorContigenciaServico the value of field
     * 'cdIndicadorContigenciaServico'.
     */
    public void setCdIndicadorContigenciaServico(int cdIndicadorContigenciaServico)
    {
        this._cdIndicadorContigenciaServico = cdIndicadorContigenciaServico;
        this._has_cdIndicadorContigenciaServico = true;
    } //-- void setCdIndicadorContigenciaServico(int) 

    /**
     * Sets the value of field 'cdIndicadorManutencaoServico'.
     * 
     * @param cdIndicadorManutencaoServico the value of field
     * 'cdIndicadorManutencaoServico'.
     */
    public void setCdIndicadorManutencaoServico(int cdIndicadorManutencaoServico)
    {
        this._cdIndicadorManutencaoServico = cdIndicadorManutencaoServico;
        this._has_cdIndicadorManutencaoServico = true;
    } //-- void setCdIndicadorManutencaoServico(int) 

    /**
     * Sets the value of field 'cdIndicadorNegociavelServico'.
     * 
     * @param cdIndicadorNegociavelServico the value of field
     * 'cdIndicadorNegociavelServico'.
     */
    public void setCdIndicadorNegociavelServico(int cdIndicadorNegociavelServico)
    {
        this._cdIndicadorNegociavelServico = cdIndicadorNegociavelServico;
        this._has_cdIndicadorNegociavelServico = true;
    } //-- void setCdIndicadorNegociavelServico(int) 

    /**
     * Sets the value of field 'cdIndicadorServicoNegociavel'.
     * 
     * @param cdIndicadorServicoNegociavel the value of field
     * 'cdIndicadorServicoNegociavel'.
     */
    public void setCdIndicadorServicoNegociavel(int cdIndicadorServicoNegociavel)
    {
        this._cdIndicadorServicoNegociavel = cdIndicadorServicoNegociavel;
        this._has_cdIndicadorServicoNegociavel = true;
    } //-- void setCdIndicadorServicoNegociavel(int) 

    /**
     * Sets the value of field 'cdNaturezaOperacaoPagamento'.
     * 
     * @param cdNaturezaOperacaoPagamento the value of field
     * 'cdNaturezaOperacaoPagamento'.
     */
    public void setCdNaturezaOperacaoPagamento(int cdNaturezaOperacaoPagamento)
    {
        this._cdNaturezaOperacaoPagamento = cdNaturezaOperacaoPagamento;
        this._has_cdNaturezaOperacaoPagamento = true;
    } //-- void setCdNaturezaOperacaoPagamento(int) 

    /**
     * Sets the value of field 'cdProdutoOperacaoRelacionado'.
     * 
     * @param cdProdutoOperacaoRelacionado the value of field
     * 'cdProdutoOperacaoRelacionado'.
     */
    public void setCdProdutoOperacaoRelacionado(int cdProdutoOperacaoRelacionado)
    {
        this._cdProdutoOperacaoRelacionado = cdProdutoOperacaoRelacionado;
        this._has_cdProdutoOperacaoRelacionado = true;
    } //-- void setCdProdutoOperacaoRelacionado(int) 

    /**
     * Sets the value of field 'cdProdutoServicoOperacao'.
     * 
     * @param cdProdutoServicoOperacao the value of field
     * 'cdProdutoServicoOperacao'.
     */
    public void setCdProdutoServicoOperacao(int cdProdutoServicoOperacao)
    {
        this._cdProdutoServicoOperacao = cdProdutoServicoOperacao;
        this._has_cdProdutoServicoOperacao = true;
    } //-- void setCdProdutoServicoOperacao(int) 

    /**
     * Sets the value of field 'cdRelacionamentoProduto'.
     * 
     * @param cdRelacionamentoProduto the value of field
     * 'cdRelacionamentoProduto'.
     */
    public void setCdRelacionamentoProduto(int cdRelacionamentoProduto)
    {
        this._cdRelacionamentoProduto = cdRelacionamentoProduto;
        this._has_cdRelacionamentoProduto = true;
    } //-- void setCdRelacionamentoProduto(int) 

    /**
     * Sets the value of field 'cdServicoCompostoPagamento'.
     * 
     * @param cdServicoCompostoPagamento the value of field
     * 'cdServicoCompostoPagamento'.
     */
    public void setCdServicoCompostoPagamento(long cdServicoCompostoPagamento)
    {
        this._cdServicoCompostoPagamento = cdServicoCompostoPagamento;
        this._has_cdServicoCompostoPagamento = true;
    } //-- void setCdServicoCompostoPagamento(long) 

    /**
     * Sets the value of field 'nrOrdenacaoServicoRelacionado'.
     * 
     * @param nrOrdenacaoServicoRelacionado the value of field
     * 'nrOrdenacaoServicoRelacionado'.
     */
    public void setNrOrdenacaoServicoRelacionado(int nrOrdenacaoServicoRelacionado)
    {
        this._nrOrdenacaoServicoRelacionado = nrOrdenacaoServicoRelacionado;
        this._has_nrOrdenacaoServicoRelacionado = true;
    } //-- void setNrOrdenacaoServicoRelacionado(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return AlterarServicoRelacionadoRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.alterarservicorelacionado.request.AlterarServicoRelacionadoRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.alterarservicorelacionado.request.AlterarServicoRelacionadoRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.alterarservicorelacionado.request.AlterarServicoRelacionadoRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.alterarservicorelacionado.request.AlterarServicoRelacionadoRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
