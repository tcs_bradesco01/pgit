package br.com.bradesco.web.pgit.service.business.combo.bean;


// TODO: Auto-generated Javadoc
/**
 * Arquivo criado em 11/12/15.
 */
public class ListarServicoRelacionadoPgitOcorrenciasSaidaDTO {

    /** The cd servico. */
    private Integer cdServico;

    /** The ds servico. */
    private String dsServico;

    
    public ListarServicoRelacionadoPgitOcorrenciasSaidaDTO(Integer cdServico,
			String dsServico) {
		super();
		this.cdServico = cdServico;
		this.dsServico = dsServico;
	}

	/**
     * Sets the cd servico.
     *
     * @param cdServico the cd servico
     */
    public void setCdServico(Integer cdServico) {
        this.cdServico = cdServico;
    }

    /**
     * Gets the cd servico.
     *
     * @return the cd servico
     */
    public Integer getCdServico() {
        return this.cdServico;
    }

    /**
     * Sets the ds servico.
     *
     * @param dsServico the ds servico
     */
    public void setDsServico(String dsServico) {
        this.dsServico = dsServico;
    }

    /**
     * Gets the ds servico.
     *
     * @return the ds servico
     */
    public String getDsServico() {
        return this.dsServico;
    }
}