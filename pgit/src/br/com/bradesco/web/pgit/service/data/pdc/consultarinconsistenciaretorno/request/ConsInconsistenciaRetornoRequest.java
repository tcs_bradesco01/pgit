/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultarinconsistenciaretorno.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ConsInconsistenciaRetornoRequest.
 * 
 * @version $Revision$ $Date$
 */
public class ConsInconsistenciaRetornoRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _numeroOcorrencia
     */
    private int _numeroOcorrencia = 0;

    /**
     * keeps track of state for field: _numeroOcorrencia
     */
    private boolean _has_numeroOcorrencia;

    /**
     * Field _codigoSistemaLegado
     */
    private java.lang.String _codigoSistemaLegado;

    /**
     * Field _cpfCnpj
     */
    private long _cpfCnpj = 0;

    /**
     * keeps track of state for field: _cpfCnpj
     */
    private boolean _has_cpfCnpj;

    /**
     * Field _filial
     */
    private int _filial = 0;

    /**
     * keeps track of state for field: _filial
     */
    private boolean _has_filial;

    /**
     * Field _codigoPerfilComunicacao
     */
    private long _codigoPerfilComunicacao = 0;

    /**
     * keeps track of state for field: _codigoPerfilComunicacao
     */
    private boolean _has_codigoPerfilComunicacao;

    /**
     * Field _numeroArquivoRetorno
     */
    private long _numeroArquivoRetorno = 0;

    /**
     * keeps track of state for field: _numeroArquivoRetorno
     */
    private boolean _has_numeroArquivoRetorno;

    /**
     * Field _horaInclusaoArquivoRetorno
     */
    private java.lang.String _horaInclusaoArquivoRetorno;

    /**
     * Field _numeroLoteRetorno
     */
    private long _numeroLoteRetorno = 0;

    /**
     * keeps track of state for field: _numeroLoteRetorno
     */
    private boolean _has_numeroLoteRetorno;

    /**
     * Field _numeroSequenciaRegistro
     */
    private long _numeroSequenciaRegistro = 0;

    /**
     * keeps track of state for field: _numeroSequenciaRegistro
     */
    private boolean _has_numeroSequenciaRegistro;


      //----------------/
     //- Constructors -/
    //----------------/

    public ConsInconsistenciaRetornoRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarinconsistenciaretorno.request.ConsInconsistenciaRetornoRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCodigoPerfilComunicacao
     * 
     */
    public void deleteCodigoPerfilComunicacao()
    {
        this._has_codigoPerfilComunicacao= false;
    } //-- void deleteCodigoPerfilComunicacao() 

    /**
     * Method deleteCpfCnpj
     * 
     */
    public void deleteCpfCnpj()
    {
        this._has_cpfCnpj= false;
    } //-- void deleteCpfCnpj() 

    /**
     * Method deleteFilial
     * 
     */
    public void deleteFilial()
    {
        this._has_filial= false;
    } //-- void deleteFilial() 

    /**
     * Method deleteNumeroArquivoRetorno
     * 
     */
    public void deleteNumeroArquivoRetorno()
    {
        this._has_numeroArquivoRetorno= false;
    } //-- void deleteNumeroArquivoRetorno() 

    /**
     * Method deleteNumeroLoteRetorno
     * 
     */
    public void deleteNumeroLoteRetorno()
    {
        this._has_numeroLoteRetorno= false;
    } //-- void deleteNumeroLoteRetorno() 

    /**
     * Method deleteNumeroOcorrencia
     * 
     */
    public void deleteNumeroOcorrencia()
    {
        this._has_numeroOcorrencia= false;
    } //-- void deleteNumeroOcorrencia() 

    /**
     * Method deleteNumeroSequenciaRegistro
     * 
     */
    public void deleteNumeroSequenciaRegistro()
    {
        this._has_numeroSequenciaRegistro= false;
    } //-- void deleteNumeroSequenciaRegistro() 

    /**
     * Returns the value of field 'codigoPerfilComunicacao'.
     * 
     * @return long
     * @return the value of field 'codigoPerfilComunicacao'.
     */
    public long getCodigoPerfilComunicacao()
    {
        return this._codigoPerfilComunicacao;
    } //-- long getCodigoPerfilComunicacao() 

    /**
     * Returns the value of field 'codigoSistemaLegado'.
     * 
     * @return String
     * @return the value of field 'codigoSistemaLegado'.
     */
    public java.lang.String getCodigoSistemaLegado()
    {
        return this._codigoSistemaLegado;
    } //-- java.lang.String getCodigoSistemaLegado() 

    /**
     * Returns the value of field 'cpfCnpj'.
     * 
     * @return long
     * @return the value of field 'cpfCnpj'.
     */
    public long getCpfCnpj()
    {
        return this._cpfCnpj;
    } //-- long getCpfCnpj() 

    /**
     * Returns the value of field 'filial'.
     * 
     * @return int
     * @return the value of field 'filial'.
     */
    public int getFilial()
    {
        return this._filial;
    } //-- int getFilial() 

    /**
     * Returns the value of field 'horaInclusaoArquivoRetorno'.
     * 
     * @return String
     * @return the value of field 'horaInclusaoArquivoRetorno'.
     */
    public java.lang.String getHoraInclusaoArquivoRetorno()
    {
        return this._horaInclusaoArquivoRetorno;
    } //-- java.lang.String getHoraInclusaoArquivoRetorno() 

    /**
     * Returns the value of field 'numeroArquivoRetorno'.
     * 
     * @return long
     * @return the value of field 'numeroArquivoRetorno'.
     */
    public long getNumeroArquivoRetorno()
    {
        return this._numeroArquivoRetorno;
    } //-- long getNumeroArquivoRetorno() 

    /**
     * Returns the value of field 'numeroLoteRetorno'.
     * 
     * @return long
     * @return the value of field 'numeroLoteRetorno'.
     */
    public long getNumeroLoteRetorno()
    {
        return this._numeroLoteRetorno;
    } //-- long getNumeroLoteRetorno() 

    /**
     * Returns the value of field 'numeroOcorrencia'.
     * 
     * @return int
     * @return the value of field 'numeroOcorrencia'.
     */
    public int getNumeroOcorrencia()
    {
        return this._numeroOcorrencia;
    } //-- int getNumeroOcorrencia() 

    /**
     * Returns the value of field 'numeroSequenciaRegistro'.
     * 
     * @return long
     * @return the value of field 'numeroSequenciaRegistro'.
     */
    public long getNumeroSequenciaRegistro()
    {
        return this._numeroSequenciaRegistro;
    } //-- long getNumeroSequenciaRegistro() 

    /**
     * Method hasCodigoPerfilComunicacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCodigoPerfilComunicacao()
    {
        return this._has_codigoPerfilComunicacao;
    } //-- boolean hasCodigoPerfilComunicacao() 

    /**
     * Method hasCpfCnpj
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCpfCnpj()
    {
        return this._has_cpfCnpj;
    } //-- boolean hasCpfCnpj() 

    /**
     * Method hasFilial
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasFilial()
    {
        return this._has_filial;
    } //-- boolean hasFilial() 

    /**
     * Method hasNumeroArquivoRetorno
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNumeroArquivoRetorno()
    {
        return this._has_numeroArquivoRetorno;
    } //-- boolean hasNumeroArquivoRetorno() 

    /**
     * Method hasNumeroLoteRetorno
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNumeroLoteRetorno()
    {
        return this._has_numeroLoteRetorno;
    } //-- boolean hasNumeroLoteRetorno() 

    /**
     * Method hasNumeroOcorrencia
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNumeroOcorrencia()
    {
        return this._has_numeroOcorrencia;
    } //-- boolean hasNumeroOcorrencia() 

    /**
     * Method hasNumeroSequenciaRegistro
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNumeroSequenciaRegistro()
    {
        return this._has_numeroSequenciaRegistro;
    } //-- boolean hasNumeroSequenciaRegistro() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'codigoPerfilComunicacao'.
     * 
     * @param codigoPerfilComunicacao the value of field
     * 'codigoPerfilComunicacao'.
     */
    public void setCodigoPerfilComunicacao(long codigoPerfilComunicacao)
    {
        this._codigoPerfilComunicacao = codigoPerfilComunicacao;
        this._has_codigoPerfilComunicacao = true;
    } //-- void setCodigoPerfilComunicacao(long) 

    /**
     * Sets the value of field 'codigoSistemaLegado'.
     * 
     * @param codigoSistemaLegado the value of field
     * 'codigoSistemaLegado'.
     */
    public void setCodigoSistemaLegado(java.lang.String codigoSistemaLegado)
    {
        this._codigoSistemaLegado = codigoSistemaLegado;
    } //-- void setCodigoSistemaLegado(java.lang.String) 

    /**
     * Sets the value of field 'cpfCnpj'.
     * 
     * @param cpfCnpj the value of field 'cpfCnpj'.
     */
    public void setCpfCnpj(long cpfCnpj)
    {
        this._cpfCnpj = cpfCnpj;
        this._has_cpfCnpj = true;
    } //-- void setCpfCnpj(long) 

    /**
     * Sets the value of field 'filial'.
     * 
     * @param filial the value of field 'filial'.
     */
    public void setFilial(int filial)
    {
        this._filial = filial;
        this._has_filial = true;
    } //-- void setFilial(int) 

    /**
     * Sets the value of field 'horaInclusaoArquivoRetorno'.
     * 
     * @param horaInclusaoArquivoRetorno the value of field
     * 'horaInclusaoArquivoRetorno'.
     */
    public void setHoraInclusaoArquivoRetorno(java.lang.String horaInclusaoArquivoRetorno)
    {
        this._horaInclusaoArquivoRetorno = horaInclusaoArquivoRetorno;
    } //-- void setHoraInclusaoArquivoRetorno(java.lang.String) 

    /**
     * Sets the value of field 'numeroArquivoRetorno'.
     * 
     * @param numeroArquivoRetorno the value of field
     * 'numeroArquivoRetorno'.
     */
    public void setNumeroArquivoRetorno(long numeroArquivoRetorno)
    {
        this._numeroArquivoRetorno = numeroArquivoRetorno;
        this._has_numeroArquivoRetorno = true;
    } //-- void setNumeroArquivoRetorno(long) 

    /**
     * Sets the value of field 'numeroLoteRetorno'.
     * 
     * @param numeroLoteRetorno the value of field
     * 'numeroLoteRetorno'.
     */
    public void setNumeroLoteRetorno(long numeroLoteRetorno)
    {
        this._numeroLoteRetorno = numeroLoteRetorno;
        this._has_numeroLoteRetorno = true;
    } //-- void setNumeroLoteRetorno(long) 

    /**
     * Sets the value of field 'numeroOcorrencia'.
     * 
     * @param numeroOcorrencia the value of field 'numeroOcorrencia'
     */
    public void setNumeroOcorrencia(int numeroOcorrencia)
    {
        this._numeroOcorrencia = numeroOcorrencia;
        this._has_numeroOcorrencia = true;
    } //-- void setNumeroOcorrencia(int) 

    /**
     * Sets the value of field 'numeroSequenciaRegistro'.
     * 
     * @param numeroSequenciaRegistro the value of field
     * 'numeroSequenciaRegistro'.
     */
    public void setNumeroSequenciaRegistro(long numeroSequenciaRegistro)
    {
        this._numeroSequenciaRegistro = numeroSequenciaRegistro;
        this._has_numeroSequenciaRegistro = true;
    } //-- void setNumeroSequenciaRegistro(long) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ConsInconsistenciaRetornoRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultarinconsistenciaretorno.request.ConsInconsistenciaRetornoRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultarinconsistenciaretorno.request.ConsInconsistenciaRetornoRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultarinconsistenciaretorno.request.ConsInconsistenciaRetornoRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarinconsistenciaretorno.request.ConsInconsistenciaRetornoRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
