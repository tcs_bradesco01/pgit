package br.com.bradesco.web.pgit.service.business.concontratosmigrados.bean;

import br.com.bradesco.web.pgit.utils.CpfCnpjUtils;
import br.com.bradesco.web.pgit.utils.PgitUtil;


// TODO: Auto-generated Javadoc
/**
 * Arquivo criado em 21/01/16.
 */
public class DetalharContratoMigradoHsbcSaidaDTO {

    /** The cod mensagem. */
    private String codMensagem = null;

    /** The mensagem. */
    private String mensagem = null;

    /** The cd digito conta bancaria. */
    private String cdDigitoContaBancaria = null;
    
    /** The cd conta bancaria. */
    private Long cdContaBancaria = null;
    
    /** The cd cpf cnpj pagador. */
    private Long cdCpfCnpjPagador = null;

    /** The cd filial cpf cnpj pagador. */
    private Integer cdFilialCpfCnpjPagador = null;

    /** The cd controle cpf cnpj pagador. */
    private Integer cdControleCpfCnpjPagador = null;

    /** The cd banco. */
    private Integer cdBanco = null;

    /** The cd agencia bancaria. */
    private Integer cdAgenciaBancaria = null;

    
    /**
     * Gets the cpf cnpj formatado.
     *
     * @return the cpf cnpj formatado
     */
    public String getCpfCnpjFormatado(){
    	return CpfCnpjUtils.formatCpfCnpjCompleto(getCdCpfCnpjPagador(), 
    			getCdFilialCpfCnpjPagador(), getCdControleCpfCnpjPagador());
    }
    
    /**
     * Gets the conta bancaria formatada.
     *
     * @return the conta bancaria formatada
     */
    public String getContaBancariaFormatada(){
    	return PgitUtil.formatConta(getCdContaBancaria(), getCdDigitoContaBancaria(), false);
    }

    /**
     * Sets the cod mensagem.
     *
     * @param codMensagem the cod mensagem
     */
    public void setCodMensagem(String codMensagem) {
        this.codMensagem = codMensagem;
    }

    /**
     * Gets the cod mensagem.
     *
     * @return the cod mensagem
     */
    public String getCodMensagem() {
        return this.codMensagem;
    }

    /**
     * Sets the mensagem.
     *
     * @param mensagem the mensagem
     */
    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }

    /**
     * Gets the mensagem.
     *
     * @return the mensagem
     */
    public String getMensagem() {
        return this.mensagem;
    }

    /**
     * Sets the cd cpf cnpj pagador.
     *
     * @param cdCpfCnpjPagador the cd cpf cnpj pagador
     */
    public void setCdCpfCnpjPagador(Long cdCpfCnpjPagador) {
        this.cdCpfCnpjPagador = cdCpfCnpjPagador;
    }

    /**
     * Gets the cd cpf cnpj pagador.
     *
     * @return the cd cpf cnpj pagador
     */
    public Long getCdCpfCnpjPagador() {
        return this.cdCpfCnpjPagador;
    }

    /**
     * Sets the cd filial cpf cnpj pagador.
     *
     * @param cdFilialCpfCnpjPagador the cd filial cpf cnpj pagador
     */
    public void setCdFilialCpfCnpjPagador(Integer cdFilialCpfCnpjPagador) {
        this.cdFilialCpfCnpjPagador = cdFilialCpfCnpjPagador;
    }

    /**
     * Gets the cd filial cpf cnpj pagador.
     *
     * @return the cd filial cpf cnpj pagador
     */
    public Integer getCdFilialCpfCnpjPagador() {
        return this.cdFilialCpfCnpjPagador;
    }

    /**
     * Sets the cd controle cpf cnpj pagador.
     *
     * @param cdControleCpfCnpjPagador the cd controle cpf cnpj pagador
     */
    public void setCdControleCpfCnpjPagador(Integer cdControleCpfCnpjPagador) {
        this.cdControleCpfCnpjPagador = cdControleCpfCnpjPagador;
    }

    /**
     * Gets the cd controle cpf cnpj pagador.
     *
     * @return the cd controle cpf cnpj pagador
     */
    public Integer getCdControleCpfCnpjPagador() {
        return this.cdControleCpfCnpjPagador;
    }

    /**
     * Sets the cd banco.
     *
     * @param cdBanco the cd banco
     */
    public void setCdBanco(Integer cdBanco) {
        this.cdBanco = cdBanco;
    }

    /**
     * Gets the cd banco.
     *
     * @return the cd banco
     */
    public Integer getCdBanco() {
        return this.cdBanco;
    }

    /**
     * Sets the cd agencia bancaria.
     *
     * @param cdAgenciaBancaria the cd agencia bancaria
     */
    public void setCdAgenciaBancaria(Integer cdAgenciaBancaria) {
        this.cdAgenciaBancaria = cdAgenciaBancaria;
    }

    /**
     * Gets the cd agencia bancaria.
     *
     * @return the cd agencia bancaria
     */
    public Integer getCdAgenciaBancaria() {
        return this.cdAgenciaBancaria;
    }

    /**
     * Sets the cd conta bancaria.
     *
     * @param cdContaBancaria the cd conta bancaria
     */
    public void setCdContaBancaria(Long cdContaBancaria) {
        this.cdContaBancaria = cdContaBancaria;
    }

    /**
     * Gets the cd conta bancaria.
     *
     * @return the cd conta bancaria
     */
    public Long getCdContaBancaria() {
        return this.cdContaBancaria;
    }

    /**
     * Sets the cd digito conta bancaria.
     *
     * @param cdDigitoContaBancaria the cd digito conta bancaria
     */
    public void setCdDigitoContaBancaria(String cdDigitoContaBancaria) {
        this.cdDigitoContaBancaria = cdDigitoContaBancaria;
    }

    /**
     * Gets the cd digito conta bancaria.
     *
     * @return the cd digito conta bancaria
     */
    public String getCdDigitoContaBancaria() {
        return this.cdDigitoContaBancaria;
    }
}