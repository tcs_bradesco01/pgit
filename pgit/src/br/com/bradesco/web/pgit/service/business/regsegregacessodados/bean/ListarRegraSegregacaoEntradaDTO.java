/*
 * Nome: br.com.bradesco.web.pgit.service.business.regsegregacessodados.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.regsegregacessodados.bean;

/**
 * Nome: ListarRegraSegregacaoEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarRegraSegregacaoEntradaDTO {
	
	/** Atributo codigoRegra. */
	private int codigoRegra;
	
	/** Atributo tipoUnidadeOrganizacionalUsuario. */
	private int tipoUnidadeOrganizacionalUsuario;
	
	/** Atributo tipoAcao. */
	private int tipoAcao;
	
	/** Atributo nivelPermissaoAcessoDados. */
	private int nivelPermissaoAcessoDados;
	
	/**
	 * Get: codigoRegra.
	 *
	 * @return codigoRegra
	 */
	public int getCodigoRegra() {
		return codigoRegra;
	}
	
	/**
	 * Set: codigoRegra.
	 *
	 * @param codigoRegra the codigo regra
	 */
	public void setCodigoRegra(int codigoRegra) {
		this.codigoRegra = codigoRegra;
	}
	
	/**
	 * Get: nivelPermissaoAcessoDados.
	 *
	 * @return nivelPermissaoAcessoDados
	 */
	public int getNivelPermissaoAcessoDados() {
		return nivelPermissaoAcessoDados;
	}
	
	/**
	 * Set: nivelPermissaoAcessoDados.
	 *
	 * @param nivelPermissaoAcessoDados the nivel permissao acesso dados
	 */
	public void setNivelPermissaoAcessoDados(int nivelPermissaoAcessoDados) {
		this.nivelPermissaoAcessoDados = nivelPermissaoAcessoDados;
	}
	
	/**
	 * Get: tipoAcao.
	 *
	 * @return tipoAcao
	 */
	public int getTipoAcao() {
		return tipoAcao;
	}
	
	/**
	 * Set: tipoAcao.
	 *
	 * @param tipoAcao the tipo acao
	 */
	public void setTipoAcao(int tipoAcao) {
		this.tipoAcao = tipoAcao;
	}
	
	/**
	 * Get: tipoUnidadeOrganizacionalUsuario.
	 *
	 * @return tipoUnidadeOrganizacionalUsuario
	 */
	public int getTipoUnidadeOrganizacionalUsuario() {
		return tipoUnidadeOrganizacionalUsuario;
	}
	
	/**
	 * Set: tipoUnidadeOrganizacionalUsuario.
	 *
	 * @param tipoUnidadeOrganizacionalUsuario the tipo unidade organizacional usuario
	 */
	public void setTipoUnidadeOrganizacionalUsuario(
			int tipoUnidadeOrganizacionalUsuario) {
		this.tipoUnidadeOrganizacionalUsuario = tipoUnidadeOrganizacionalUsuario;
	}
	
	
	

}
