/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/interfaz.ftl,v $
 * $Id: interfaz.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: interfaz.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.autorizarpagamentosconsolidados;

import java.util.List;

import br.com.bradesco.web.pgit.service.business.autorizarpagamentosconsolidados.bean.AutorizarIntegralEntradaDTO;
import br.com.bradesco.web.pgit.service.business.autorizarpagamentosconsolidados.bean.AutorizarIntegralSaidaDTO;
import br.com.bradesco.web.pgit.service.business.autorizarpagamentosconsolidados.bean.AutorizarParcialEntradaDTO;
import br.com.bradesco.web.pgit.service.business.autorizarpagamentosconsolidados.bean.AutorizarParcialSaidaDTO;
import br.com.bradesco.web.pgit.service.business.autorizarpagamentosconsolidados.bean.ConsultarPagtosConsPendAutorizacaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.autorizarpagamentosconsolidados.bean.ConsultarPagtosConsPendAutorizacaoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.autorizarpagamentosconsolidados.bean.DetalharPagtosConsPendAutorizacaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.autorizarpagamentosconsolidados.bean.DetalharPagtosConsPendAutorizacaoSaidaDTO;

/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Interface do adaptador: AutorizarPagamentosConsolidados
 * </p>
 * 
 * @comment CODIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public interface IAutorizarPagamentosConsolidadosService {
	
	/**
	 * Autorizar integral.
	 *
	 * @param entradaDTO the entrada dto
	 * @return the autorizar integral saida dto
	 */
	AutorizarIntegralSaidaDTO autorizarIntegral(AutorizarIntegralEntradaDTO entradaDTO);
	
	/**
	 * Autorizar parcial.
	 *
	 * @param listaEntradaDTO the lista entrada dto
	 * @return the autorizar parcial saida dto
	 */
	AutorizarParcialSaidaDTO autorizarParcial(List<AutorizarParcialEntradaDTO> listaEntradaDTO);
	
	/**
	 * Detalhar pagtos cons pend autorizacao.
	 *
	 * @param entradaDTO the entrada dto
	 * @return the detalhar pagtos cons pend autorizacao saida dto
	 */
	DetalharPagtosConsPendAutorizacaoSaidaDTO detalharPagtosConsPendAutorizacao(DetalharPagtosConsPendAutorizacaoEntradaDTO entradaDTO);
	
	/**
	 * Consultar pagtos cons pend autorizacao.
	 *
	 * @param entradaDTO the entrada dto
	 * @return the list< consultar pagtos cons pend autorizacao saida dt o>
	 */
	List<ConsultarPagtosConsPendAutorizacaoSaidaDTO> consultarPagtosConsPendAutorizacao(ConsultarPagtosConsPendAutorizacaoEntradaDTO entradaDTO);
}