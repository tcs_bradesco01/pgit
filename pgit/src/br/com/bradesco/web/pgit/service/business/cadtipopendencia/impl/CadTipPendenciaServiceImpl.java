/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/implementation.ftl,v $
 * $Id: implementation.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: implementation.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */
 
package br.com.bradesco.web.pgit.service.business.cadtipopendencia.impl;

import java.util.ArrayList;
import java.util.List;

import br.com.bradesco.web.pgit.service.business.cadtipopendencia.ICadTipPendenciaService;
import br.com.bradesco.web.pgit.service.business.cadtipopendencia.bean.ConsultarDescricaoUnidOrganizacionalEntradaDTO;
import br.com.bradesco.web.pgit.service.business.cadtipopendencia.bean.ConsultarDescricaoUnidOrganizacionalSaidaDTO;
import br.com.bradesco.web.pgit.service.business.cadtipopendencia.bean.TipoPendenciaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.cadtipopendencia.bean.TipoPendenciaSaidaDTO;
import br.com.bradesco.web.pgit.service.data.pdc.FactoryAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.alterartipopendenciaresponsaveis.request.AlterarTipoPendenciaResponsaveisRequest;
import br.com.bradesco.web.pgit.service.data.pdc.alterartipopendenciaresponsaveis.response.AlterarTipoPendenciaResponsaveisResponse;
import br.com.bradesco.web.pgit.service.data.pdc.consultardescricaounidadeorganizacional.request.ConsultarDescricaoUnidadeOrganizacionalRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultardescricaounidadeorganizacional.response.ConsultarDescricaoUnidadeOrganizacionalResponse;
import br.com.bradesco.web.pgit.service.data.pdc.detalhartipopendenciaresponsaveis.request.DetalharTipoPendenciaResponsaveisRequest;
import br.com.bradesco.web.pgit.service.data.pdc.detalhartipopendenciaresponsaveis.response.DetalharTipoPendenciaResponsaveisResponse;
import br.com.bradesco.web.pgit.service.data.pdc.excluirtipopendenciaresponsaveis.request.ExcluirTipoPendenciaResponsaveisRequest;
import br.com.bradesco.web.pgit.service.data.pdc.excluirtipopendenciaresponsaveis.response.ExcluirTipoPendenciaResponsaveisResponse;
import br.com.bradesco.web.pgit.service.data.pdc.incluirtipopendenciaresponsaveis.request.IncluirTipoPendenciaResponsaveisRequest;
import br.com.bradesco.web.pgit.service.data.pdc.incluirtipopendenciaresponsaveis.response.IncluirTipoPendenciaResponsaveisResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listartipopendenciaresponsaveis.request.ListarTipoPendenciaResponsaveisRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listartipopendenciaresponsaveis.response.ListarTipoPendenciaResponsaveisResponse;
import br.com.bradesco.web.pgit.utils.PgitUtil;


/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Implementa��o do adaptador: ManterCadastroTipoPendencia
 * </p>
 * 
 * @comment C�DIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public class CadTipPendenciaServiceImpl implements ICadTipPendenciaService {
	
	/** Atributo factoryAdapter. */
	private FactoryAdapter factoryAdapter;
	
	/**
	 * Get: factoryAdapter.
	 *
	 * @return factoryAdapter
	 */
	public FactoryAdapter getFactoryAdapter() {
		return factoryAdapter;
	}

	/**
	 * Set: factoryAdapter.
	 *
	 * @param factoryAdapter the factory adapter
	 */
	public void setFactoryAdapter(FactoryAdapter factoryAdapter) {
		this.factoryAdapter = factoryAdapter;
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.cadtipopendencia.ICadTipPendenciaService#alterarTipoPendencia(br.com.bradesco.web.pgit.service.business.cadtipopendencia.bean.TipoPendenciaEntradaDTO)
	 */
	public TipoPendenciaSaidaDTO alterarTipoPendencia(TipoPendenciaEntradaDTO tipoPendenciaEntradaDTO) {
		
		TipoPendenciaSaidaDTO tipoPendenciaSaidaDTO = new TipoPendenciaSaidaDTO();
		
		AlterarTipoPendenciaResponsaveisRequest alterarTipoPendenciaResponsaveisRequest = new AlterarTipoPendenciaResponsaveisRequest();
		AlterarTipoPendenciaResponsaveisResponse alterarTipoPendenciaResponsaveisResponse = new AlterarTipoPendenciaResponsaveisResponse();
		
		alterarTipoPendenciaResponsaveisRequest.setCdIndicadorResponsavelPagamento(tipoPendenciaEntradaDTO.getCdIndicadorResponsavelPagamento());		
		alterarTipoPendenciaResponsaveisRequest.setCdTipoPendenciaPagamentoIntegrado(tipoPendenciaEntradaDTO.getCdTipoPendenciaPagamentoIntegrado());		
		alterarTipoPendenciaResponsaveisRequest.setCdPessoaJuridica(tipoPendenciaEntradaDTO.getCdPessoaJuridica());
		alterarTipoPendenciaResponsaveisRequest.setCdTipoBaixaPendencia(tipoPendenciaEntradaDTO.getCdTipoBaixaPendencia() );
		alterarTipoPendenciaResponsaveisRequest.setCdTipoUnidadeOrganizacional(tipoPendenciaEntradaDTO.getCdTipoUnidadeOrganizacional());
		alterarTipoPendenciaResponsaveisRequest.setCdUnidadeOrganizacional(tipoPendenciaEntradaDTO.getCdUnidadeOrganizacional());
		alterarTipoPendenciaResponsaveisRequest.setDsPendenciaPagamentoIntegrado(tipoPendenciaEntradaDTO.getDsPendenciaPagamentoIntegrado());		
		alterarTipoPendenciaResponsaveisRequest.setRsResumoPendenciaPagamento(tipoPendenciaEntradaDTO.getRsResumoPendenciaPagamento());
		alterarTipoPendenciaResponsaveisRequest.setDsEmailRespPgto(tipoPendenciaEntradaDTO.getDsEmailRespPgto());
		
		alterarTipoPendenciaResponsaveisResponse = getFactoryAdapter().getAlterarTipoPendenciaResponsaveisPDCAdapter().invokeProcess(alterarTipoPendenciaResponsaveisRequest);
				
		tipoPendenciaSaidaDTO.setCodMensagem(alterarTipoPendenciaResponsaveisResponse.getCodMensagem());
		tipoPendenciaSaidaDTO.setMensagem(alterarTipoPendenciaResponsaveisResponse.getMensagem());
		
		/*
    		tipoPendenciaSaidaDTO.setCdPendenciaPagamentoIntegrado(alterarTipoPendenciaResponsaveisResponse.getCdTipoPendenciaPagamentoIntegrado());    		
    		tipoPendenciaSaidaDTO.setCdTipoUnidadeOrganizacional(alterarTipoPendenciaResponsaveisResponse.getCdTipoUnidadeOrganizacional());
    		tipoPendenciaSaidaDTO.setCdPessoaJuridica(alterarTipoPendenciaResponsaveisResponse.getCdPessoaJuridica());
    		tipoPendenciaSaidaDTO.setCdTipoUnidadeOrganizacional(alterarTipoPendenciaResponsaveisResponse.getCdUnidadeOrganizacional());
    		tipoPendenciaSaidaDTO.setDsPendenciaPagamentoIntegrado(alterarTipoPendenciaResponsaveisResponse.getDsPendenciaPagamentoIntegrado());
    		tipoPendenciaSaidaDTO.setDsResumoPendenciaPagamento(alterarTipoPendenciaResponsaveisResponse.getRsResumoPendenciaPagamento());
    		tipoPendenciaSaidaDTO.setCdTipoBaixaPendencia(alterarTipoPendenciaResponsaveisResponse.getCdTipoBaixaPendencia());
    		tipoPendenciaSaidaDTO.setCdIndicadorResponsavelPagamento(alterarTipoPendenciaResponsaveisResponse.getCdIndicadorResponsavelPagamento());
    		*/
		
		return tipoPendenciaSaidaDTO;	
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.cadtipopendencia.ICadTipPendenciaService#detalharTipoPendencia(br.com.bradesco.web.pgit.service.business.cadtipopendencia.bean.TipoPendenciaEntradaDTO)
	 */
	public TipoPendenciaSaidaDTO detalharTipoPendencia(TipoPendenciaEntradaDTO tipoPendenciaEntradaDTO) {
		DetalharTipoPendenciaResponsaveisRequest detalharTipoPendenciaResponsaveisRequest = new DetalharTipoPendenciaResponsaveisRequest();
		DetalharTipoPendenciaResponsaveisResponse detalharTipoPendenciaResponsaveisResponse = new DetalharTipoPendenciaResponsaveisResponse();
		
		detalharTipoPendenciaResponsaveisRequest.setCdPendenciaPagamentoIntegrado(tipoPendenciaEntradaDTO.getCdTipoPendenciaPagamentoIntegrado());		
		detalharTipoPendenciaResponsaveisRequest.setCdTipoUnidadeOrganizacional(tipoPendenciaEntradaDTO.getCdTipoUnidadeOrganizacional());
		
		detalharTipoPendenciaResponsaveisResponse = getFactoryAdapter().getDetalharTipoPendenciaResponsaveisPDCAdapter().invokeProcess(detalharTipoPendenciaResponsaveisRequest);

		TipoPendenciaSaidaDTO tipoPendenciaSaidaDTO = new TipoPendenciaSaidaDTO();
			
		tipoPendenciaSaidaDTO.setCodMensagem(detalharTipoPendenciaResponsaveisResponse.getCodMensagem());
		tipoPendenciaSaidaDTO.setMensagem(detalharTipoPendenciaResponsaveisResponse.getMensagem());
		tipoPendenciaSaidaDTO.setCdIndicadorResponsavelPagamento(detalharTipoPendenciaResponsaveisResponse.getCdIndicadorResponsavelPagamento());
		tipoPendenciaSaidaDTO.setCdPendenciaPagamentoIntegrado(detalharTipoPendenciaResponsaveisResponse.getCdPendenciaPagamentoIntegrado());
		tipoPendenciaSaidaDTO.setCdTipoBaixaPendencia(detalharTipoPendenciaResponsaveisResponse.getCdTipoBaixaPendencia());
		tipoPendenciaSaidaDTO.setCdTipoUnidadeOrganizacional(detalharTipoPendenciaResponsaveisResponse.getCdTipoUnidadeOrganizacional());
		tipoPendenciaSaidaDTO.setDsTipoUnidadeOrganizacional(detalharTipoPendenciaResponsaveisResponse.getDsTipoUnidadeOrganizacional());
		tipoPendenciaSaidaDTO.setDsPendenciaPagamentoIntegrado(detalharTipoPendenciaResponsaveisResponse.getDsPendenciaPagamentoIntegrado());
		
		tipoPendenciaSaidaDTO.setCdPessoaJuridica(detalharTipoPendenciaResponsaveisResponse.getCdPessoaJuridica());
		tipoPendenciaSaidaDTO.setDsCodigoPessoaJuridica(detalharTipoPendenciaResponsaveisResponse.getDsCodigoPessoaJuridica());
		tipoPendenciaSaidaDTO.setNrSequenciaUnidadeOrganizacional(detalharTipoPendenciaResponsaveisResponse.getCdUnidadeOrganizacional());
		tipoPendenciaSaidaDTO.setDsNumeroSequenciaUnidadeOrganizacional(detalharTipoPendenciaResponsaveisResponse.getDsNumeroSequenciaUnidadeOrganizacional());
		tipoPendenciaSaidaDTO.setDsResumoPendenciaPagamento(detalharTipoPendenciaResponsaveisResponse.getDsResumoPendenciaPagamento());
		tipoPendenciaSaidaDTO.setCdCanalInclusao(detalharTipoPendenciaResponsaveisResponse.getCdCanalInclusao());
		tipoPendenciaSaidaDTO.setDsCanalInclusao(detalharTipoPendenciaResponsaveisResponse.getDsCanalInclusao());
		tipoPendenciaSaidaDTO.setCdAutenticacaoSegurancaInclusao(detalharTipoPendenciaResponsaveisResponse.getCdAutenticacaoSegurancaInclusao());
		tipoPendenciaSaidaDTO.setHrInclusaoRegistro(detalharTipoPendenciaResponsaveisResponse.getHrInclusaoRegistro());
		tipoPendenciaSaidaDTO.setNrOperacaoFluxoInclusao(detalharTipoPendenciaResponsaveisResponse.getNrOperacaoFluxoInclusao());
		
		tipoPendenciaSaidaDTO.setDsEmailRespPgto(detalharTipoPendenciaResponsaveisResponse.getDsEmailRespPgto());
		tipoPendenciaSaidaDTO.setCdCanalManutencao(detalharTipoPendenciaResponsaveisResponse.getCdCanalManutencao());
		tipoPendenciaSaidaDTO.setDsCanalManutencao(detalharTipoPendenciaResponsaveisResponse.getDsCanalManutencao());
		tipoPendenciaSaidaDTO.setCdAutenticacaoSegurancaManutencao(detalharTipoPendenciaResponsaveisResponse.getCdAutenticacaoSegurancaManutencao());
		tipoPendenciaSaidaDTO.setHrManutencaoRegistroManutencao(detalharTipoPendenciaResponsaveisResponse.getHrManutencaoRegistroManutencao());
		tipoPendenciaSaidaDTO.setNrOperacaoFluxoManutencao(detalharTipoPendenciaResponsaveisResponse.getNrOperacaoFluxoManutencao().equals("0")? "" : detalharTipoPendenciaResponsaveisResponse.getNrOperacaoFluxoManutencao());
		
		return tipoPendenciaSaidaDTO;	

	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.cadtipopendencia.ICadTipPendenciaService#excluirTipoPendencia(br.com.bradesco.web.pgit.service.business.cadtipopendencia.bean.TipoPendenciaEntradaDTO)
	 */
	public TipoPendenciaSaidaDTO excluirTipoPendencia(TipoPendenciaEntradaDTO tipoPendenciaEntradaDTO) {
		
		TipoPendenciaSaidaDTO tipoPendenciaSaidaDTO = new TipoPendenciaSaidaDTO();
		ExcluirTipoPendenciaResponsaveisRequest excluirTipoPendenciaResponsaveisRequest = new ExcluirTipoPendenciaResponsaveisRequest();
		ExcluirTipoPendenciaResponsaveisResponse excluirTipoPendenciaResponsaveisResponse = new ExcluirTipoPendenciaResponsaveisResponse();
				
		excluirTipoPendenciaResponsaveisRequest.setCdPendenciaPagamentoIntegrado(tipoPendenciaEntradaDTO.getCdTipoPendenciaPagamentoIntegrado());		
		excluirTipoPendenciaResponsaveisRequest.setCdTipoUnidadeOrganizacional(tipoPendenciaEntradaDTO.getCdTipoUnidadeOrganizacional());
		
		excluirTipoPendenciaResponsaveisResponse = getFactoryAdapter().getExcluirTipoPendenciaResponsaveisPDCAdapter().invokeProcess(excluirTipoPendenciaResponsaveisRequest);
				
		tipoPendenciaSaidaDTO.setCodMensagem(excluirTipoPendenciaResponsaveisResponse.getCodMensagem());
		tipoPendenciaSaidaDTO.setMensagem(excluirTipoPendenciaResponsaveisResponse.getMensagem());
		
		return tipoPendenciaSaidaDTO;
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.cadtipopendencia.ICadTipPendenciaService#incluirTipoPendencia(br.com.bradesco.web.pgit.service.business.cadtipopendencia.bean.TipoPendenciaEntradaDTO)
	 */
	public TipoPendenciaSaidaDTO incluirTipoPendencia(TipoPendenciaEntradaDTO tipoPendenciaEntradaDTO) {
		TipoPendenciaSaidaDTO tipoPendenciaSaidaDTO = new TipoPendenciaSaidaDTO();
		IncluirTipoPendenciaResponsaveisRequest incluirTipoPendenciaResponsaveisRequest = new IncluirTipoPendenciaResponsaveisRequest();
		IncluirTipoPendenciaResponsaveisResponse incluirTipoPendenciaResponsaveisResponse = new IncluirTipoPendenciaResponsaveisResponse();
		
		incluirTipoPendenciaResponsaveisRequest.setCdIndicadorResponsavelPagamento(tipoPendenciaEntradaDTO.getCdIndicadorResponsavelPagamento());		
		incluirTipoPendenciaResponsaveisRequest.setCdPendenciaPagamentoIntegrado(tipoPendenciaEntradaDTO.getCdTipoPendenciaPagamentoIntegrado());		
		incluirTipoPendenciaResponsaveisRequest.setCdPessoaJuridica(tipoPendenciaEntradaDTO.getCdPessoaJuridica());
		incluirTipoPendenciaResponsaveisRequest.setCdTipoBaixaPendencia(tipoPendenciaEntradaDTO.getCdTipoBaixaPendencia() );
		incluirTipoPendenciaResponsaveisRequest.setCdTipoUnidadeOrganizacional(tipoPendenciaEntradaDTO.getCdTipoUnidadeOrganizacional());
		incluirTipoPendenciaResponsaveisRequest.setCdUnidadeOrganizacional(tipoPendenciaEntradaDTO.getCdUnidadeOrganizacional());
		incluirTipoPendenciaResponsaveisRequest.setDsPendenciaPagamentoIntegrado(tipoPendenciaEntradaDTO.getDsPendenciaPagamentoIntegrado());		
		incluirTipoPendenciaResponsaveisRequest.setRsResumoPendenciaPagamento(tipoPendenciaEntradaDTO.getRsResumoPendenciaPagamento());
		incluirTipoPendenciaResponsaveisRequest.setDsEmailRespPgto(tipoPendenciaEntradaDTO.getDsEmailRespPgto());
		
		incluirTipoPendenciaResponsaveisResponse = getFactoryAdapter().getIncluirTipoPendenciaResponsaveisPDCAdapter().invokeProcess(incluirTipoPendenciaResponsaveisRequest);
				
		tipoPendenciaSaidaDTO.setCodMensagem(incluirTipoPendenciaResponsaveisResponse.getCodMensagem());
		tipoPendenciaSaidaDTO.setMensagem(incluirTipoPendenciaResponsaveisResponse.getMensagem());
				
		return tipoPendenciaSaidaDTO;
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.cadtipopendencia.ICadTipPendenciaService#listarTipoPendencia(br.com.bradesco.web.pgit.service.business.cadtipopendencia.bean.TipoPendenciaEntradaDTO)
	 */
	public List<TipoPendenciaSaidaDTO> listarTipoPendencia(TipoPendenciaEntradaDTO tipoPendenciaEntradaDTO) {
		List<TipoPendenciaSaidaDTO> listaTipoPendencia = new ArrayList<TipoPendenciaSaidaDTO>();
		ListarTipoPendenciaResponsaveisRequest listarTipoPendenciaResponsaveisRequest = new ListarTipoPendenciaResponsaveisRequest();
		ListarTipoPendenciaResponsaveisResponse listarTipoPendenciaResponsaveisResponse = new ListarTipoPendenciaResponsaveisResponse();
		
		listarTipoPendenciaResponsaveisRequest.setCdPendenciaPagamentoIntegrado(PgitUtil.verificaLongNulo(tipoPendenciaEntradaDTO.getCdTipoPendenciaPagamentoIntegrado()));
		listarTipoPendenciaResponsaveisRequest.setCdTipoBaixaPendencia(PgitUtil.verificaIntegerNulo(tipoPendenciaEntradaDTO.getCdTipoBaixaPendencia()));
		listarTipoPendenciaResponsaveisRequest.setCdIndicadorRespostaPagamento(PgitUtil.verificaIntegerNulo(tipoPendenciaEntradaDTO.getCdIndicadorResponsavelPagamento()));
		listarTipoPendenciaResponsaveisRequest.setCdTipoUnidadeOrganizacional(PgitUtil.verificaIntegerNulo(tipoPendenciaEntradaDTO.getCdTipoUnidadeOrganizacional()));
		listarTipoPendenciaResponsaveisRequest.setCdPessoaJuridica(PgitUtil.verificaLongNulo(tipoPendenciaEntradaDTO.getCdPessoaJuridica()));
		listarTipoPendenciaResponsaveisRequest.setCdUnidadeOrganizacional(PgitUtil.verificaIntegerNulo(tipoPendenciaEntradaDTO.getCdUnidadeOrganizacional()));
		listarTipoPendenciaResponsaveisRequest.setQtConsultas(PgitUtil.verificaIntegerNulo(tipoPendenciaEntradaDTO.getNumeroConsultas()));
		
		listarTipoPendenciaResponsaveisResponse = getFactoryAdapter().getListarTipoPendenciaResponsaveisPDCAdapter().invokeProcess(listarTipoPendenciaResponsaveisRequest);

		TipoPendenciaSaidaDTO tipoPendenciaSaidaDTO;
		for (int i=0; i<listarTipoPendenciaResponsaveisResponse.getOcorrenciasCount();i++){
			tipoPendenciaSaidaDTO = new TipoPendenciaSaidaDTO();
			tipoPendenciaSaidaDTO.setCodMensagem(listarTipoPendenciaResponsaveisResponse.getCodMensagem());
			tipoPendenciaSaidaDTO.setMensagem(listarTipoPendenciaResponsaveisResponse.getMensagem());
			tipoPendenciaSaidaDTO.setCdIndicadorResponsavelPagamento(listarTipoPendenciaResponsaveisResponse.getOcorrencias(i).getCdIndicadorRespostaPagamento());
			tipoPendenciaSaidaDTO.setCdPendenciaPagamentoIntegrado(listarTipoPendenciaResponsaveisResponse.getOcorrencias(i).getCdPendenciaPagamentoIntegrado());
			tipoPendenciaSaidaDTO.setCdTipoBaixaPendencia(listarTipoPendenciaResponsaveisResponse.getOcorrencias(i).getCdTipoBaixaPendencia());
			tipoPendenciaSaidaDTO.setDsTipoUnidadeOrganizacional(listarTipoPendenciaResponsaveisResponse.getOcorrencias(i).getDsTipoUnidadeOrganizacional());
			tipoPendenciaSaidaDTO.setDsPendenciaPagamentoIntegrado(listarTipoPendenciaResponsaveisResponse.getOcorrencias(i).getDsPendenciaPagamentoIntegrado());
			tipoPendenciaSaidaDTO.setDsIndicadorResponsavelPagamento(listarTipoPendenciaResponsaveisResponse.getOcorrencias(i).getCdIndicadorRespostaPagamento()==1?"DIN�MICA":"EST�TICA");
			tipoPendenciaSaidaDTO.setDsTipoBaixaPendencia(listarTipoPendenciaResponsaveisResponse.getOcorrencias(i).getCdTipoBaixaPendencia()==1?"MANUAL":"AUTOM�TICA");
		
			listaTipoPendencia.add(tipoPendenciaSaidaDTO);
		}
	
		return listaTipoPendencia;	
	}
	
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.cadtipopendencia.ICadTipPendenciaService#descricaoUnidadeOrganizacional(br.com.bradesco.web.pgit.service.business.cadtipopendencia.bean.ConsultarDescricaoUnidOrganizacionalEntradaDTO)
	 */
	public ConsultarDescricaoUnidOrganizacionalSaidaDTO descricaoUnidadeOrganizacional(ConsultarDescricaoUnidOrganizacionalEntradaDTO descricaoUnidOrgEntradaDTO) {
		ConsultarDescricaoUnidadeOrganizacionalRequest detalharUnidOrgRequest = new ConsultarDescricaoUnidadeOrganizacionalRequest();
		ConsultarDescricaoUnidadeOrganizacionalResponse detalharUnidOrgResponse = new ConsultarDescricaoUnidadeOrganizacionalResponse();
		
		detalharUnidOrgRequest.setCdPessoaJuridica(descricaoUnidOrgEntradaDTO.getCdPessoaJuridica());		
		detalharUnidOrgRequest.setCdTipoUnidadeOrganizacional(descricaoUnidOrgEntradaDTO.getCdTipoUnidadeOrganizacional());
		detalharUnidOrgRequest.setNrSequenciaUnidadeOrganizacional(descricaoUnidOrgEntradaDTO.getNrSequenciaUnidadeOrganizacional());
		
		detalharUnidOrgResponse = getFactoryAdapter().getConsultarDescricaoUnidadeOrganizacionalPDCAdapter().invokeProcess(detalharUnidOrgRequest);

		ConsultarDescricaoUnidOrganizacionalSaidaDTO detalharUnidOrgSaidaDTO = new ConsultarDescricaoUnidOrganizacionalSaidaDTO();
			
		detalharUnidOrgSaidaDTO.setCodMensagem(detalharUnidOrgResponse.getCodMensagem());
		detalharUnidOrgSaidaDTO.setMensagem(detalharUnidOrgResponse.getMensagem());
		detalharUnidOrgSaidaDTO.setDsNumeroSeqUnidadeOrganizacional(detalharUnidOrgResponse.getDsNumeroSeqUnidadeOrganizacional());
		
		
		return detalharUnidOrgSaidaDTO;	

	}
    
}

