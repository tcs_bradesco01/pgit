/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultardetalhetipocomprovante.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ConsultarDetalheTipoComprovanteRequest.
 * 
 * @version $Revision$ $Date$
 */
public class ConsultarDetalheTipoComprovanteRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdTipoComprovanteSalarial
     */
    private int _cdTipoComprovanteSalarial = 0;

    /**
     * keeps track of state for field: _cdTipoComprovanteSalarial
     */
    private boolean _has_cdTipoComprovanteSalarial;

    /**
     * Field _qtConsultas
     */
    private int _qtConsultas = 0;

    /**
     * keeps track of state for field: _qtConsultas
     */
    private boolean _has_qtConsultas;


      //----------------/
     //- Constructors -/
    //----------------/

    public ConsultarDetalheTipoComprovanteRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultardetalhetipocomprovante.request.ConsultarDetalheTipoComprovanteRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdTipoComprovanteSalarial
     * 
     */
    public void deleteCdTipoComprovanteSalarial()
    {
        this._has_cdTipoComprovanteSalarial= false;
    } //-- void deleteCdTipoComprovanteSalarial() 

    /**
     * Method deleteQtConsultas
     * 
     */
    public void deleteQtConsultas()
    {
        this._has_qtConsultas= false;
    } //-- void deleteQtConsultas() 

    /**
     * Returns the value of field 'cdTipoComprovanteSalarial'.
     * 
     * @return int
     * @return the value of field 'cdTipoComprovanteSalarial'.
     */
    public int getCdTipoComprovanteSalarial()
    {
        return this._cdTipoComprovanteSalarial;
    } //-- int getCdTipoComprovanteSalarial() 

    /**
     * Returns the value of field 'qtConsultas'.
     * 
     * @return int
     * @return the value of field 'qtConsultas'.
     */
    public int getQtConsultas()
    {
        return this._qtConsultas;
    } //-- int getQtConsultas() 

    /**
     * Method hasCdTipoComprovanteSalarial
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoComprovanteSalarial()
    {
        return this._has_cdTipoComprovanteSalarial;
    } //-- boolean hasCdTipoComprovanteSalarial() 

    /**
     * Method hasQtConsultas
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtConsultas()
    {
        return this._has_qtConsultas;
    } //-- boolean hasQtConsultas() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdTipoComprovanteSalarial'.
     * 
     * @param cdTipoComprovanteSalarial the value of field
     * 'cdTipoComprovanteSalarial'.
     */
    public void setCdTipoComprovanteSalarial(int cdTipoComprovanteSalarial)
    {
        this._cdTipoComprovanteSalarial = cdTipoComprovanteSalarial;
        this._has_cdTipoComprovanteSalarial = true;
    } //-- void setCdTipoComprovanteSalarial(int) 

    /**
     * Sets the value of field 'qtConsultas'.
     * 
     * @param qtConsultas the value of field 'qtConsultas'.
     */
    public void setQtConsultas(int qtConsultas)
    {
        this._qtConsultas = qtConsultas;
        this._has_qtConsultas = true;
    } //-- void setQtConsultas(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ConsultarDetalheTipoComprovanteRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultardetalhetipocomprovante.request.ConsultarDetalheTipoComprovanteRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultardetalhetipocomprovante.request.ConsultarDetalheTipoComprovanteRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultardetalhetipocomprovante.request.ConsultarDetalheTipoComprovanteRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultardetalhetipocomprovante.request.ConsultarDetalheTipoComprovanteRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
