/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantersolicretransmissaoarquivosretorno.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantersolicretransmissaoarquivosretorno.bean;

/**
 * Nome: OcorrenciasIncluirSolBaseRetransmissaoEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class OcorrenciasIncluirSolBaseRetransmissaoEntradaDTO {
	
	/** Atributo cdPessoa. */
	private Long cdPessoa;
    
    /** Atributo cdTipoLayoutArquivo. */
    private Integer cdTipoLayoutArquivo;
    
    /** Atributo cdClienteTransferenciaArquivo. */
    private Long cdClienteTransferenciaArquivo;
    
    /** Atributo nrSequenciaArquivoRetorno. */
    private Long nrSequenciaArquivoRetorno;
    
    /** Atributo hrInclusaoArquivoRetorno. */
    private String hrInclusaoArquivoRetorno;
    
    /** Atributo dsArquivoRetorno. */
    private String dsArquivoRetorno;
    
	/**
	 * Get: cdClienteTransferenciaArquivo.
	 *
	 * @return cdClienteTransferenciaArquivo
	 */
	public Long getCdClienteTransferenciaArquivo() {
		return cdClienteTransferenciaArquivo;
	}
	
	/**
	 * Set: cdClienteTransferenciaArquivo.
	 *
	 * @param cdClienteTransferenciaArquivo the cd cliente transferencia arquivo
	 */
	public void setCdClienteTransferenciaArquivo(Long cdClienteTransferenciaArquivo) {
		this.cdClienteTransferenciaArquivo = cdClienteTransferenciaArquivo;
	}
	
	/**
	 * Get: cdPessoa.
	 *
	 * @return cdPessoa
	 */
	public Long getCdPessoa() {
		return cdPessoa;
	}
	
	/**
	 * Set: cdPessoa.
	 *
	 * @param cdPessoa the cd pessoa
	 */
	public void setCdPessoa(Long cdPessoa) {
		this.cdPessoa = cdPessoa;
	}
	
	/**
	 * Get: cdTipoLayoutArquivo.
	 *
	 * @return cdTipoLayoutArquivo
	 */
	public Integer getCdTipoLayoutArquivo() {
		return cdTipoLayoutArquivo;
	}
	
	/**
	 * Set: cdTipoLayoutArquivo.
	 *
	 * @param cdTipoLayoutArquivo the cd tipo layout arquivo
	 */
	public void setCdTipoLayoutArquivo(Integer cdTipoLayoutArquivo) {
		this.cdTipoLayoutArquivo = cdTipoLayoutArquivo;
	}
	
	/**
	 * Get: hrInclusaoArquivoRetorno.
	 *
	 * @return hrInclusaoArquivoRetorno
	 */
	public String getHrInclusaoArquivoRetorno() {
		return hrInclusaoArquivoRetorno;
	}
	
	/**
	 * Set: hrInclusaoArquivoRetorno.
	 *
	 * @param hrInclusaoArquivoRetorno the hr inclusao arquivo retorno
	 */
	public void setHrInclusaoArquivoRetorno(String hrInclusaoArquivoRetorno) {
		this.hrInclusaoArquivoRetorno = hrInclusaoArquivoRetorno;
	}
	
	/**
	 * Get: nrSequenciaArquivoRetorno.
	 *
	 * @return nrSequenciaArquivoRetorno
	 */
	public Long getNrSequenciaArquivoRetorno() {
		return nrSequenciaArquivoRetorno;
	}
	
	/**
	 * Set: nrSequenciaArquivoRetorno.
	 *
	 * @param nrSequenciaArquivoRetorno the nr sequencia arquivo retorno
	 */
	public void setNrSequenciaArquivoRetorno(Long nrSequenciaArquivoRetorno) {
		this.nrSequenciaArquivoRetorno = nrSequenciaArquivoRetorno;
	}
	
	/**
	 * Get: dsArquivoRetorno.
	 *
	 * @return dsArquivoRetorno
	 */
	public String getDsArquivoRetorno() {
		return dsArquivoRetorno;
	}
	
	/**
	 * Set: dsArquivoRetorno.
	 *
	 * @param dsArquivoRetorno the ds arquivo retorno
	 */
	public void setDsArquivoRetorno(String dsArquivoRetorno) {
		this.dsArquivoRetorno = dsArquivoRetorno;
	}
}