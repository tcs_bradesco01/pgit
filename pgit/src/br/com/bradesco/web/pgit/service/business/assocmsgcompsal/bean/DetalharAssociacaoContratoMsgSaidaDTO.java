/*
 * Nome: br.com.bradesco.web.pgit.service.business.assocmsgcompsal.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.assocmsgcompsal.bean;

/**
 * Nome: DetalharAssociacaoContratoMsgSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class DetalharAssociacaoContratoMsgSaidaDTO {
	
	/** Atributo codMensagem. */
	private String codMensagem;
	
	/** Atributo mensagem. */
	private String mensagem;
	
	/** Atributo codTipoMensagem. */
	private int codTipoMensagem;
	
	/** Atributo dsTipoMensagem. */
	private String dsTipoMensagem;
	
	/** Atributo contratoPssoaJurdContr. */
	private long contratoPssoaJurdContr;
	
	/** Atributo contratoTpoContrNegoc. */
	private int contratoTpoContrNegoc;
	
	/** Atributo contratoSeqContrNegoc. */
	private long contratoSeqContrNegoc;
	
	/** Atributo cdCanalInclusao. */
	private int cdCanalInclusao;
	
	/** Atributo dsCanalInclusao. */
	private String dsCanalInclusao;
	
	/** Atributo usuarioInclusao. */
	private String usuarioInclusao;
	
	/** Atributo complementoInclusao. */
	private String complementoInclusao;
	
	/** Atributo dataHoraInclusao. */
	private String dataHoraInclusao;
	
	/** Atributo cdCanalManutencao. */
	private int cdCanalManutencao;
	
	/** Atributo dsCanalManutencao. */
	private String dsCanalManutencao;
	
	/** Atributo usuarioManutencao. */
	private String usuarioManutencao;
	
	/** Atributo complementoManutencao. */
	private String complementoManutencao;
	
	/** Atributo dataHoraManutencao. */
	private String dataHoraManutencao;
	
	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}
	
	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}
	
	/**
	 * Get: cdCanalInclusao.
	 *
	 * @return cdCanalInclusao
	 */
	public int getCdCanalInclusao() {
		return cdCanalInclusao;
	}
	
	/**
	 * Set: cdCanalInclusao.
	 *
	 * @param codTipoCanalInclusao the cd canal inclusao
	 */
	public void setCdCanalInclusao(int codTipoCanalInclusao) {
		this.cdCanalInclusao = codTipoCanalInclusao;
	}
	
	/**
	 * Get: cdCanalManutencao.
	 *
	 * @return cdCanalManutencao
	 */
	public int getCdCanalManutencao() {
		return cdCanalManutencao;
	}
	
	/**
	 * Set: cdCanalManutencao.
	 *
	 * @param codTipoCanalManutencao the cd canal manutencao
	 */
	public void setCdCanalManutencao(int codTipoCanalManutencao) {
		this.cdCanalManutencao = codTipoCanalManutencao;
	}
	
	/**
	 * Get: codTipoMensagem.
	 *
	 * @return codTipoMensagem
	 */
	public int getCodTipoMensagem() {
		return codTipoMensagem;
	}
	
	/**
	 * Set: codTipoMensagem.
	 *
	 * @param codTipoMensagem the cod tipo mensagem
	 */
	public void setCodTipoMensagem(int codTipoMensagem) {
		this.codTipoMensagem = codTipoMensagem;
	}
	
	/**
	 * Get: complementoInclusao.
	 *
	 * @return complementoInclusao
	 */
	public String getComplementoInclusao() {
		return complementoInclusao;
	}
	
	/**
	 * Set: complementoInclusao.
	 *
	 * @param complementoInclusao the complemento inclusao
	 */
	public void setComplementoInclusao(String complementoInclusao) {
		this.complementoInclusao = complementoInclusao;
	}
	
	/**
	 * Get: complementoManutencao.
	 *
	 * @return complementoManutencao
	 */
	public String getComplementoManutencao() {
		return complementoManutencao;
	}
	
	/**
	 * Set: complementoManutencao.
	 *
	 * @param complementoManutencao the complemento manutencao
	 */
	public void setComplementoManutencao(String complementoManutencao) {
		this.complementoManutencao = complementoManutencao;
	}
	
	/**
	 * Get: dataHoraInclusao.
	 *
	 * @return dataHoraInclusao
	 */
	public String getDataHoraInclusao() {
		return dataHoraInclusao;
	}
	
	/**
	 * Set: dataHoraInclusao.
	 *
	 * @param dataHoraInclusao the data hora inclusao
	 */
	public void setDataHoraInclusao(String dataHoraInclusao) {
		this.dataHoraInclusao = dataHoraInclusao;
	}
	
	/**
	 * Get: dataHoraManutencao.
	 *
	 * @return dataHoraManutencao
	 */
	public String getDataHoraManutencao() {
		return dataHoraManutencao;
	}
	
	/**
	 * Set: dataHoraManutencao.
	 *
	 * @param dataHoraManutencao the data hora manutencao
	 */
	public void setDataHoraManutencao(String dataHoraManutencao) {
		this.dataHoraManutencao = dataHoraManutencao;
	}
	
	/**
	 * Get: dsCanalInclusao.
	 *
	 * @return dsCanalInclusao
	 */
	public String getDsCanalInclusao() {
		return dsCanalInclusao;
	}
	
	/**
	 * Set: dsCanalInclusao.
	 *
	 * @param dsTipoCanalInclusao the ds canal inclusao
	 */
	public void setDsCanalInclusao(String dsTipoCanalInclusao) {
		this.dsCanalInclusao = dsTipoCanalInclusao;
	}
	
	/**
	 * Get: dsCanalManutencao.
	 *
	 * @return dsCanalManutencao
	 */
	public String getDsCanalManutencao() {
		return dsCanalManutencao;
	}
	
	/**
	 * Set: dsCanalManutencao.
	 *
	 * @param dsTipoCanalManutencao the ds canal manutencao
	 */
	public void setDsCanalManutencao(String dsTipoCanalManutencao) {
		this.dsCanalManutencao = dsTipoCanalManutencao;
	}
	
	/**
	 * Get: dsTipoMensagem.
	 *
	 * @return dsTipoMensagem
	 */
	public String getDsTipoMensagem() {
		return dsTipoMensagem;
	}
	
	/**
	 * Set: dsTipoMensagem.
	 *
	 * @param dsTipoMensagem the ds tipo mensagem
	 */
	public void setDsTipoMensagem(String dsTipoMensagem) {
		this.dsTipoMensagem = dsTipoMensagem;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	/**
	 * Get: usuarioInclusao.
	 *
	 * @return usuarioInclusao
	 */
	public String getUsuarioInclusao() {
		return usuarioInclusao;
	}
	
	/**
	 * Set: usuarioInclusao.
	 *
	 * @param usuarioInclusao the usuario inclusao
	 */
	public void setUsuarioInclusao(String usuarioInclusao) {
		this.usuarioInclusao = usuarioInclusao;
	}
	
	/**
	 * Get: usuarioManutencao.
	 *
	 * @return usuarioManutencao
	 */
	public String getUsuarioManutencao() {
		return usuarioManutencao;
	}
	
	/**
	 * Set: usuarioManutencao.
	 *
	 * @param usuarioManutencao the usuario manutencao
	 */
	public void setUsuarioManutencao(String usuarioManutencao) {
		this.usuarioManutencao = usuarioManutencao;
	}

	/**
	 * Get: contratoPssoaJurdContr.
	 *
	 * @return contratoPssoaJurdContr
	 */
	public long getContratoPssoaJurdContr() {
		return contratoPssoaJurdContr;
	}

	/**
	 * Set: contratoPssoaJurdContr.
	 *
	 * @param contratoPssoaJurdContr the contrato pssoa jurd contr
	 */
	public void setContratoPssoaJurdContr(long contratoPssoaJurdContr) {
		this.contratoPssoaJurdContr = contratoPssoaJurdContr;
	}

	/**
	 * Get: contratoSeqContrNegoc.
	 *
	 * @return contratoSeqContrNegoc
	 */
	public long getContratoSeqContrNegoc() {
		return contratoSeqContrNegoc;
	}

	/**
	 * Set: contratoSeqContrNegoc.
	 *
	 * @param contratoSeqContrNegoc the contrato seq contr negoc
	 */
	public void setContratoSeqContrNegoc(long contratoSeqContrNegoc) {
		this.contratoSeqContrNegoc = contratoSeqContrNegoc;
	}

	/**
	 * Get: contratoTpoContrNegoc.
	 *
	 * @return contratoTpoContrNegoc
	 */
	public int getContratoTpoContrNegoc() {
		return contratoTpoContrNegoc;
	}

	/**
	 * Set: contratoTpoContrNegoc.
	 *
	 * @param contratoTpoContrNegoc the contrato tpo contr negoc
	 */
	public void setContratoTpoContrNegoc(int contratoTpoContrNegoc) {
		this.contratoTpoContrNegoc = contratoTpoContrNegoc;
	}
	
	
}






