/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantervinculoempresasalario.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantervinculoempresasalario.bean;

import br.com.bradesco.web.pgit.service.data.pdc.listarvinculoctasalarioempresa.response.Ocorrencias;


/**
 * Nome: ListarVinculoCtaSalarioEmpresaSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarVinculoCtaSalarioEmpresaSaidaDTO {
	
	/** Atributo cdPessoaJuridVinc. */
	private Long cdPessoaJuridVinc;
    
    /** Atributo cdTipoContratoVinc. */
    private Integer cdTipoContratoVinc;
    
    /** Atributo nrSeqContratoVinc. */
    private Long nrSeqContratoVinc;
    
    /** Atributo cdTipoVinculoContrato. */
    private Integer cdTipoVinculoContrato;
    
    /** Atributo cdBancoSalario. */
    private Integer cdBancoSalario;
    
    /** Atributo dsBancoSalario. */
    private String dsBancoSalario;
    
    /** Atributo cdAgenciaSalario. */
    private Integer cdAgenciaSalario;
    
    /** Atributo cdDigitoAgenciaSalario. */
    private Integer cdDigitoAgenciaSalario;
    
    /** Atributo dsAgencia. */
    private String dsAgencia;
    
    /** Atributo cdContaSalario. */
    private Long cdContaSalario;
    
    /** Atributo cdDigitoContaSalario. */
    private String cdDigitoContaSalario;
    
    /** Atributo nmTitular. */
    private String nmTitular;
    
    /** Atributo cdSituacaoConta. */
    private String cdSituacaoConta;
    
	/**
	 * Listar vinculo cta salario empresa saida dto.
	 */
	public ListarVinculoCtaSalarioEmpresaSaidaDTO() {
		super();
	}	
	
	/**
	 * Listar vinculo cta salario empresa saida dto.
	 *
	 * @param ocorrencia the ocorrencia
	 */
	public ListarVinculoCtaSalarioEmpresaSaidaDTO(Ocorrencias ocorrencia) {
		this.cdPessoaJuridVinc = ocorrencia.getCdPessoaJuridVinc();
		this.cdTipoContratoVinc = ocorrencia.getCdTipoContratoVinc();
		this.nrSeqContratoVinc = ocorrencia.getNrSeqContratoVinc();
		this.cdTipoVinculoContrato = ocorrencia.getCdTipoVinculoContrato();
		this.cdBancoSalario = ocorrencia.getCdBancoSalario();
		this.dsBancoSalario = ocorrencia.getDsBancoSalario();
		this.cdAgenciaSalario = ocorrencia.getCdAgenciaSalario();
		this.dsAgencia = ocorrencia.getDsAgencia();
		this.cdDigitoAgenciaSalario = ocorrencia.getCdDigitoAgenciaSalario();
		this.cdContaSalario = ocorrencia.getCdContaSalario();
		this.cdDigitoContaSalario = ocorrencia.getCdDigitoContaSalario();
		this.nmTitular = ocorrencia.getNmTitular();
		this.cdSituacaoConta = ocorrencia.getCdSituacaoConta();
	}
	
	/**
	 * Get: contaDigito.
	 *
	 * @return contaDigito
	 */
	public String getContaDigito(){
		return cdDigitoContaSalario.trim().equals("") ? String.valueOf(cdContaSalario) : cdContaSalario + "-" + cdDigitoContaSalario;
	}
	
	/**
	 * Get: agenciaSalario.
	 *
	 * @return agenciaSalario
	 */
	public String getAgenciaSalario(){
		if (cdDigitoAgenciaSalario != null)	{
			return dsAgencia.trim().equals("") ? cdAgenciaSalario + "-" + cdDigitoAgenciaSalario : cdAgenciaSalario + "-" + cdDigitoAgenciaSalario + " " + dsAgencia;
		}else {
			return dsAgencia.trim().equals("")  ? String.valueOf(cdAgenciaSalario) : cdAgenciaSalario + " " + dsAgencia;
		}
	}
	
	/**
	 * Get: bancoSalario.
	 *
	 * @return bancoSalario
	 */
	public String getBancoSalario() {
		return dsBancoSalario.trim().equals("")  ? String.valueOf(cdBancoSalario) : cdBancoSalario + " " + dsBancoSalario;
	}
	
	/**
	 * Get: cdDigitoAgenciaSalario.
	 *
	 * @return cdDigitoAgenciaSalario
	 */
	public Integer getCdDigitoAgenciaSalario() {
		return cdDigitoAgenciaSalario;
	}

	/**
	 * Set: cdDigitoAgenciaSalario.
	 *
	 * @param cdDigitoAgenciaSalario the cd digito agencia salario
	 */
	public void setCdDigitoAgenciaSalario(Integer cdDigitoAgenciaSalario) {
		this.cdDigitoAgenciaSalario = cdDigitoAgenciaSalario;
	}

	/**
	 * Get: dsBancoSalario.
	 *
	 * @return dsBancoSalario
	 */
	public String getDsBancoSalario() {
		return dsBancoSalario;
	}

	/**
	 * Set: dsBancoSalario.
	 *
	 * @param dsBancoSalario the ds banco salario
	 */
	public void setDsBancoSalario(String dsBancoSalario) {
		this.dsBancoSalario = dsBancoSalario;
	}

	/**
	 * Get: cdSituacaoConta.
	 *
	 * @return cdSituacaoConta
	 */
	public String getCdSituacaoConta() {
		return cdSituacaoConta;
	}

	/**
	 * Set: cdSituacaoConta.
	 *
	 * @param cdSituacaoConta the cd situacao conta
	 */
	public void setCdSituacaoConta(String cdSituacaoConta) {
		this.cdSituacaoConta = cdSituacaoConta;
	}

	/**
	 * Get: cdAgenciaSalario.
	 *
	 * @return cdAgenciaSalario
	 */
	public Integer getCdAgenciaSalario() {
		return cdAgenciaSalario;
	}

	/**
	 * Set: cdAgenciaSalario.
	 *
	 * @param cdAgenciaSalario the cd agencia salario
	 */
	public void setCdAgenciaSalario(Integer cdAgenciaSalario) {
		this.cdAgenciaSalario = cdAgenciaSalario;
	}

	/**
	 * Get: cdBancoSalario.
	 *
	 * @return cdBancoSalario
	 */
	public Integer getCdBancoSalario() {
		return cdBancoSalario;
	}

	/**
	 * Set: cdBancoSalario.
	 *
	 * @param cdBancoSalario the cd banco salario
	 */
	public void setCdBancoSalario(Integer cdBancoSalario) {
		this.cdBancoSalario = cdBancoSalario;
	}

	/**
	 * Get: cdContaSalario.
	 *
	 * @return cdContaSalario
	 */
	public Long getCdContaSalario() {
		return cdContaSalario;
	}

	/**
	 * Set: cdContaSalario.
	 *
	 * @param cdContaSalario the cd conta salario
	 */
	public void setCdContaSalario(Long cdContaSalario) {
		this.cdContaSalario = cdContaSalario;
	}

	/**
	 * Get: cdDigitoContaSalario.
	 *
	 * @return cdDigitoContaSalario
	 */
	public String getCdDigitoContaSalario() {
		return cdDigitoContaSalario;
	}

	/**
	 * Set: cdDigitoContaSalario.
	 *
	 * @param cdDigitoContaSalario the cd digito conta salario
	 */
	public void setCdDigitoContaSalario(String cdDigitoContaSalario) {
		this.cdDigitoContaSalario = cdDigitoContaSalario;
	}

	/**
	 * Get: cdPessoaJuridVinc.
	 *
	 * @return cdPessoaJuridVinc
	 */
	public Long getCdPessoaJuridVinc() {
		return cdPessoaJuridVinc;
	}

	/**
	 * Set: cdPessoaJuridVinc.
	 *
	 * @param cdPessoaJuridVinc the cd pessoa jurid vinc
	 */
	public void setCdPessoaJuridVinc(Long cdPessoaJuridVinc) {
		this.cdPessoaJuridVinc = cdPessoaJuridVinc;
	}

	/**
	 * Get: cdTipoContratoVinc.
	 *
	 * @return cdTipoContratoVinc
	 */
	public Integer getCdTipoContratoVinc() {
		return cdTipoContratoVinc;
	}

	/**
	 * Set: cdTipoContratoVinc.
	 *
	 * @param cdTipoContratoVinc the cd tipo contrato vinc
	 */
	public void setCdTipoContratoVinc(Integer cdTipoContratoVinc) {
		this.cdTipoContratoVinc = cdTipoContratoVinc;
	}

	/**
	 * Get: cdTipoVinculoContrato.
	 *
	 * @return cdTipoVinculoContrato
	 */
	public Integer getCdTipoVinculoContrato() {
		return cdTipoVinculoContrato;
	}

	/**
	 * Set: cdTipoVinculoContrato.
	 *
	 * @param cdTipoVinculoContrato the cd tipo vinculo contrato
	 */
	public void setCdTipoVinculoContrato(Integer cdTipoVinculoContrato) {
		this.cdTipoVinculoContrato = cdTipoVinculoContrato;
	}

	/**
	 * Get: dsAgencia.
	 *
	 * @return dsAgencia
	 */
	public String getDsAgencia() {
		return dsAgencia;
	}

	/**
	 * Set: dsAgencia.
	 *
	 * @param dsAgencia the ds agencia
	 */
	public void setDsAgencia(String dsAgencia) {
		this.dsAgencia = dsAgencia;
	}

	/**
	 * Get: nmTitular.
	 *
	 * @return nmTitular
	 */
	public String getNmTitular() {
		return nmTitular;
	}

	/**
	 * Set: nmTitular.
	 *
	 * @param nmTitular the nm titular
	 */
	public void setNmTitular(String nmTitular) {
		this.nmTitular = nmTitular;
	}

	/**
	 * Get: nrSeqContratoVinc.
	 *
	 * @return nrSeqContratoVinc
	 */
	public Long getNrSeqContratoVinc() {
		return nrSeqContratoVinc;
	}

	/**
	 * Set: nrSeqContratoVinc.
	 *
	 * @param nrSeqContratoVinc the nr seq contrato vinc
	 */
	public void setNrSeqContratoVinc(Long nrSeqContratoVinc) {
		this.nrSeqContratoVinc = nrSeqContratoVinc;
	}	
}
