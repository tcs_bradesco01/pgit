/**
 * Nome: br.com.bradesco.web.pgit.service.business.mantersolicantecipacaopostergarlotepagamento.exceptions
 * Compilador: JDK 1.5
 * Prop�sito: INSERIR O PROP�SITO DAS CLASSES DO PACOTE
 * Data da cria��o: <dd/MM/yyyy>
 * Par�metros de compila��o: -d
 */
package br.com.bradesco.web.pgit.service.business.mantersolicantecipacaopostergarlotepagamento.exceptions;

import br.com.bradesco.web.aq.application.error.BradescoApplicationException;

/**
 * Nome: ManterSolicAntecipacaoPostergarLotePagamentoServiceException
 * <p>
 * Prop�sito: Classe de exce��o do adaptador ManterSolicAntecipacaoPostergarLotePagamento
 * </p>
 * 
 * @author CPM Braxis / TI Melhorias - Arquitetura
 * @version 1.0
 * @see BradescoApplicationException
 */
public class ManterSolicAntecipacaoPostergarLotePagamentoServiceException extends BradescoApplicationException  {

}