/**
 * Nome: br.com.bradesco.web.pgit.service.business.imprimiranexoprimeirocontrato
 * Compilador: JDK 1.5
 * Prop�sito: INSERIR O PROP�SITO DAS CLASSES DO PACOTE
 * Data da cria��o: <dd/MM/yyyy>
 * Par�metros de compila��o: -d
 */
package br.com.bradesco.web.pgit.service.business.imprimiranexoprimeirocontrato;

import br.com.bradesco.web.pgit.service.business.imprimiranexoprimeirocontrato.bean.ImprimirAnexoPrimeiroContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.imprimiranexoprimeirocontrato.bean.ImprimirAnexoPrimeiroContratoSaidaDTO;

/**
 * Nome: IImprimirAnexoPrimeiroContratoService
 * <p>
 * Prop�sito: Interface do adaptador ImprimirAnexoPrimeiroContrato
 * </p>
 * 
 * @author CPM Braxis / TI Melhorias - Arquitetura
 * @version 1.0
 */
public interface IImprimirAnexoPrimeiroContratoService {

    /**
     * Imprimir anexo primeiro contrato.
     *
     * @param entrada the entrada
     * @return the imprimir anexo primeiro contrato saida dto
     */
    public ImprimirAnexoPrimeiroContratoSaidaDTO imprimirAnexoPrimeiroContrato(ImprimirAnexoPrimeiroContratoEntradaDTO entrada);
    
    public ImprimirAnexoPrimeiroContratoSaidaDTO imprimirAnexoPrimeiroContratoLoop(ImprimirAnexoPrimeiroContratoEntradaDTO entrada);
}