/*
 * Nome: br.com.bradesco.web.pgit.service.business.regsegregacessodados.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.regsegregacessodados.bean;

/**
 * Nome: ExcluirRegraSegregacaoEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ExcluirRegraSegregacaoEntradaDTO {
	
	/** Atributo cdRegraAcessoDado. */
	private int cdRegraAcessoDado;

	/**
	 * Get: cdRegraAcessoDado.
	 *
	 * @return cdRegraAcessoDado
	 */
	public int getCdRegraAcessoDado() {
		return cdRegraAcessoDado;
	}

	/**
	 * Set: cdRegraAcessoDado.
	 *
	 * @param cdRegraAcessoDado the cd regra acesso dado
	 */
	public void setCdRegraAcessoDado(int cdRegraAcessoDado) {
		this.cdRegraAcessoDado = cdRegraAcessoDado;
	}
	
	


}
