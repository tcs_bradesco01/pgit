/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/implementation.ftl,v $
 * $Id: implementation.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: implementation.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */
 
package br.com.bradesco.web.pgit.service.business.listarfuncbradesco.impl;

import br.com.bradesco.web.pgit.service.business.listarfuncbradesco.IListarFuncBradescoService;
import br.com.bradesco.web.pgit.service.business.listarfuncbradesco.bean.ListarFuncionarioSaidaDTO;
import br.com.bradesco.web.pgit.service.data.pdc.FactoryAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarlistafuncionariobradesco.request.ConsultarListaFuncionarioBradescoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarlistafuncionariobradesco.response.ConsultarListaFuncionarioBradescoResponse;


/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Implementa��o do adaptador: ListarFuncBradesco
 * </p>
 * 
 * @comment C�DIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public class ListarFuncBradescoServiceImpl implements IListarFuncBradescoService {

	/** Atributo factoryAdapter. */
	private FactoryAdapter factoryAdapter;
	
	/**
	 * Get: factoryAdapter.
	 *
	 * @return factoryAdapter
	 */
	public FactoryAdapter getFactoryAdapter() {
		return factoryAdapter;
	}

	/**
	 * Set: factoryAdapter.
	 *
	 * @param factoryAdapter the factory adapter
	 */
	public void setFactoryAdapter(FactoryAdapter factoryAdapter) {
		this.factoryAdapter = factoryAdapter;
	}	
	
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.listarfuncbradesco.IListarFuncBradescoService#listarFuncionario(java.lang.Long)
	 */
	public ListarFuncionarioSaidaDTO listarFuncionario(Long cdFuncionarioBradesco) {
		
		ConsultarListaFuncionarioBradescoRequest consultarListaFuncionarioBradescoRequest = new ConsultarListaFuncionarioBradescoRequest();
		ConsultarListaFuncionarioBradescoResponse consultarListaFuncionarioBradescoResponse = new ConsultarListaFuncionarioBradescoResponse();
		
		consultarListaFuncionarioBradescoRequest.setCdFuncionarioBradesco(cdFuncionarioBradesco);
		
		consultarListaFuncionarioBradescoResponse = getFactoryAdapter().getConsultarListaFuncionarioBradescoPDCAdapter().invokeProcess(consultarListaFuncionarioBradescoRequest);

		ListarFuncionarioSaidaDTO listarFuncionarioSaidaDTO = new ListarFuncionarioSaidaDTO();

		listarFuncionarioSaidaDTO.setCdFuncionario(consultarListaFuncionarioBradescoResponse.getCdCriptografadoFuncionarioBradesco());
		listarFuncionarioSaidaDTO.setDsFuncionario(consultarListaFuncionarioBradescoResponse.getDsCriptografadoFuncionarioBradesco());		
		listarFuncionarioSaidaDTO.setCdJuncao(consultarListaFuncionarioBradescoResponse.getCdJuncaoPertencente());

		return listarFuncionarioSaidaDTO;
	}	
	
    /**
     * Construtor.
     */
    public ListarFuncBradescoServiceImpl() {
        // TODO: Implementa��o
    }
    
    /**
     * M�todo de exemplo.
     *
     * @see br.com.bradesco.web.pgit.service.business.listarfuncbradesco.IListarFuncBradesco#sampleListarFuncBradesco()
     */
    public void sampleListarFuncBradesco() {
        // TODO: Implementa�ao
    }
    
}

