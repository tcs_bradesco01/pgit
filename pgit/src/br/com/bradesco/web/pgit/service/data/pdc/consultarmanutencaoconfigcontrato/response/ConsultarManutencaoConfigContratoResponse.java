/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultarmanutencaoconfigcontrato.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ConsultarManutencaoConfigContratoResponse.
 * 
 * @version $Revision$ $Date$
 */
public class ConsultarManutencaoConfigContratoResponse implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _codMensagem
     */
    private java.lang.String _codMensagem;

    /**
     * Field _mensagem
     */
    private java.lang.String _mensagem;

    /**
     * Field _cdpessoaJuridicaContrato
     */
    private long _cdpessoaJuridicaContrato = 0;

    /**
     * keeps track of state for field: _cdpessoaJuridicaContrato
     */
    private boolean _has_cdpessoaJuridicaContrato;

    /**
     * Field _cdTipoContratoNegocio
     */
    private int _cdTipoContratoNegocio = 0;

    /**
     * keeps track of state for field: _cdTipoContratoNegocio
     */
    private boolean _has_cdTipoContratoNegocio;

    /**
     * Field _nrSequenciaContratoNegocio
     */
    private long _nrSequenciaContratoNegocio = 0;

    /**
     * keeps track of state for field: _nrSequenciaContratoNegocio
     */
    private boolean _has_nrSequenciaContratoNegocio;

    /**
     * Field _cdTipoManutencaoContrato
     */
    private int _cdTipoManutencaoContrato = 0;

    /**
     * keeps track of state for field: _cdTipoManutencaoContrato
     */
    private boolean _has_cdTipoManutencaoContrato;

    /**
     * Field _nrManutencaoContratoNegocio
     */
    private long _nrManutencaoContratoNegocio = 0;

    /**
     * keeps track of state for field: _nrManutencaoContratoNegocio
     */
    private boolean _has_nrManutencaoContratoNegocio;

    /**
     * Field _cdSituacaoManutencaoContrato
     */
    private int _cdSituacaoManutencaoContrato = 0;

    /**
     * keeps track of state for field: _cdSituacaoManutencaoContrato
     */
    private boolean _has_cdSituacaoManutencaoContrato;

    /**
     * Field _dsSituacaoManutencaoContrato
     */
    private java.lang.String _dsSituacaoManutencaoContrato;

    /**
     * Field _cdMotivoSituacaoManutencao
     */
    private int _cdMotivoSituacaoManutencao = 0;

    /**
     * keeps track of state for field: _cdMotivoSituacaoManutencao
     */
    private boolean _has_cdMotivoSituacaoManutencao;

    /**
     * Field _dsMotivoSituacaoManutencao
     */
    private java.lang.String _dsMotivoSituacaoManutencao;

    /**
     * Field _nrAditivoContratoNegocio
     */
    private long _nrAditivoContratoNegocio = 0;

    /**
     * keeps track of state for field: _nrAditivoContratoNegocio
     */
    private boolean _has_nrAditivoContratoNegocio;

    /**
     * Field _cdIndicadorTipoManutencao
     */
    private int _cdIndicadorTipoManutencao = 0;

    /**
     * keeps track of state for field: _cdIndicadorTipoManutencao
     */
    private boolean _has_cdIndicadorTipoManutencao;

    /**
     * Field _dsIndicadorTipoManutencao
     */
    private java.lang.String _dsIndicadorTipoManutencao;

    /**
     * Field _cdFormaAutorizacaoPagamento
     */
    private int _cdFormaAutorizacaoPagamento = 0;

    /**
     * keeps track of state for field: _cdFormaAutorizacaoPagamento
     */
    private boolean _has_cdFormaAutorizacaoPagamento;

    /**
     * Field _dsFormaAutorizacaoPagamento
     */
    private java.lang.String _dsFormaAutorizacaoPagamento;

    /**
     * Field _cdPessoaJuridica
     */
    private long _cdPessoaJuridica = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridica
     */
    private boolean _has_cdPessoaJuridica;

    /**
     * Field _nrSequenciaUnidadeOrganizacional
     */
    private int _nrSequenciaUnidadeOrganizacional = 0;

    /**
     * keeps track of state for field:
     * _nrSequenciaUnidadeOrganizacional
     */
    private boolean _has_nrSequenciaUnidadeOrganizacional;

    /**
     * Field _cdTipoResponsavelOrganizacao
     */
    private int _cdTipoResponsavelOrganizacao = 0;

    /**
     * keeps track of state for field: _cdTipoResponsavelOrganizacao
     */
    private boolean _has_cdTipoResponsavelOrganizacao;

    /**
     * Field _cdUnidadeOrganizacional
     */
    private int _cdUnidadeOrganizacional = 0;

    /**
     * keeps track of state for field: _cdUnidadeOrganizacional
     */
    private boolean _has_cdUnidadeOrganizacional;

    /**
     * Field _dsUnidadeOrganizacional
     */
    private java.lang.String _dsUnidadeOrganizacional;

    /**
     * Field _cdFuncionarioGerenteContrato
     */
    private long _cdFuncionarioGerenteContrato = 0;

    /**
     * keeps track of state for field: _cdFuncionarioGerenteContrato
     */
    private boolean _has_cdFuncionarioGerenteContrato;

    /**
     * Field _dsFuncionarioGerenteContrato
     */
    private java.lang.String _dsFuncionarioGerenteContrato;

    /**
     * Field _cdUsuarioInclusao
     */
    private java.lang.String _cdUsuarioInclusao;

    /**
     * Field _cdUsuarioInclusaoExterno
     */
    private java.lang.String _cdUsuarioInclusaoExterno;

    /**
     * Field _hrInclusaoRegistro
     */
    private java.lang.String _hrInclusaoRegistro;

    /**
     * Field _cdOperacaoCanalInclusao
     */
    private java.lang.String _cdOperacaoCanalInclusao;

    /**
     * Field _cdTipoCanalInclusao
     */
    private int _cdTipoCanalInclusao = 0;

    /**
     * keeps track of state for field: _cdTipoCanalInclusao
     */
    private boolean _has_cdTipoCanalInclusao;

    /**
     * Field _dsTipoCanalInclusao
     */
    private java.lang.String _dsTipoCanalInclusao;

    /**
     * Field _cdUsuarioManutencao
     */
    private java.lang.String _cdUsuarioManutencao;

    /**
     * Field _cdUsuarioManutencaoExterno
     */
    private java.lang.String _cdUsuarioManutencaoExterno;

    /**
     * Field _hrManutencaoRegistro
     */
    private java.lang.String _hrManutencaoRegistro;

    /**
     * Field _cdOperacaoCanalManutencao
     */
    private java.lang.String _cdOperacaoCanalManutencao;

    /**
     * Field _cdTipoCanalManutencao
     */
    private int _cdTipoCanalManutencao = 0;

    /**
     * keeps track of state for field: _cdTipoCanalManutencao
     */
    private boolean _has_cdTipoCanalManutencao;

    /**
     * Field _dsTipoCanalManutencao
     */
    private java.lang.String _dsTipoCanalManutencao;


      //----------------/
     //- Constructors -/
    //----------------/

    public ConsultarManutencaoConfigContratoResponse() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarmanutencaoconfigcontrato.response.ConsultarManutencaoConfigContratoResponse()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdFormaAutorizacaoPagamento
     * 
     */
    public void deleteCdFormaAutorizacaoPagamento()
    {
        this._has_cdFormaAutorizacaoPagamento= false;
    } //-- void deleteCdFormaAutorizacaoPagamento() 

    /**
     * Method deleteCdFuncionarioGerenteContrato
     * 
     */
    public void deleteCdFuncionarioGerenteContrato()
    {
        this._has_cdFuncionarioGerenteContrato= false;
    } //-- void deleteCdFuncionarioGerenteContrato() 

    /**
     * Method deleteCdIndicadorTipoManutencao
     * 
     */
    public void deleteCdIndicadorTipoManutencao()
    {
        this._has_cdIndicadorTipoManutencao= false;
    } //-- void deleteCdIndicadorTipoManutencao() 

    /**
     * Method deleteCdMotivoSituacaoManutencao
     * 
     */
    public void deleteCdMotivoSituacaoManutencao()
    {
        this._has_cdMotivoSituacaoManutencao= false;
    } //-- void deleteCdMotivoSituacaoManutencao() 

    /**
     * Method deleteCdPessoaJuridica
     * 
     */
    public void deleteCdPessoaJuridica()
    {
        this._has_cdPessoaJuridica= false;
    } //-- void deleteCdPessoaJuridica() 

    /**
     * Method deleteCdSituacaoManutencaoContrato
     * 
     */
    public void deleteCdSituacaoManutencaoContrato()
    {
        this._has_cdSituacaoManutencaoContrato= false;
    } //-- void deleteCdSituacaoManutencaoContrato() 

    /**
     * Method deleteCdTipoCanalInclusao
     * 
     */
    public void deleteCdTipoCanalInclusao()
    {
        this._has_cdTipoCanalInclusao= false;
    } //-- void deleteCdTipoCanalInclusao() 

    /**
     * Method deleteCdTipoCanalManutencao
     * 
     */
    public void deleteCdTipoCanalManutencao()
    {
        this._has_cdTipoCanalManutencao= false;
    } //-- void deleteCdTipoCanalManutencao() 

    /**
     * Method deleteCdTipoContratoNegocio
     * 
     */
    public void deleteCdTipoContratoNegocio()
    {
        this._has_cdTipoContratoNegocio= false;
    } //-- void deleteCdTipoContratoNegocio() 

    /**
     * Method deleteCdTipoManutencaoContrato
     * 
     */
    public void deleteCdTipoManutencaoContrato()
    {
        this._has_cdTipoManutencaoContrato= false;
    } //-- void deleteCdTipoManutencaoContrato() 

    /**
     * Method deleteCdTipoResponsavelOrganizacao
     * 
     */
    public void deleteCdTipoResponsavelOrganizacao()
    {
        this._has_cdTipoResponsavelOrganizacao= false;
    } //-- void deleteCdTipoResponsavelOrganizacao() 

    /**
     * Method deleteCdUnidadeOrganizacional
     * 
     */
    public void deleteCdUnidadeOrganizacional()
    {
        this._has_cdUnidadeOrganizacional= false;
    } //-- void deleteCdUnidadeOrganizacional() 

    /**
     * Method deleteCdpessoaJuridicaContrato
     * 
     */
    public void deleteCdpessoaJuridicaContrato()
    {
        this._has_cdpessoaJuridicaContrato= false;
    } //-- void deleteCdpessoaJuridicaContrato() 

    /**
     * Method deleteNrAditivoContratoNegocio
     * 
     */
    public void deleteNrAditivoContratoNegocio()
    {
        this._has_nrAditivoContratoNegocio= false;
    } //-- void deleteNrAditivoContratoNegocio() 

    /**
     * Method deleteNrManutencaoContratoNegocio
     * 
     */
    public void deleteNrManutencaoContratoNegocio()
    {
        this._has_nrManutencaoContratoNegocio= false;
    } //-- void deleteNrManutencaoContratoNegocio() 

    /**
     * Method deleteNrSequenciaContratoNegocio
     * 
     */
    public void deleteNrSequenciaContratoNegocio()
    {
        this._has_nrSequenciaContratoNegocio= false;
    } //-- void deleteNrSequenciaContratoNegocio() 

    /**
     * Method deleteNrSequenciaUnidadeOrganizacional
     * 
     */
    public void deleteNrSequenciaUnidadeOrganizacional()
    {
        this._has_nrSequenciaUnidadeOrganizacional= false;
    } //-- void deleteNrSequenciaUnidadeOrganizacional() 

    /**
     * Returns the value of field 'cdFormaAutorizacaoPagamento'.
     * 
     * @return int
     * @return the value of field 'cdFormaAutorizacaoPagamento'.
     */
    public int getCdFormaAutorizacaoPagamento()
    {
        return this._cdFormaAutorizacaoPagamento;
    } //-- int getCdFormaAutorizacaoPagamento() 

    /**
     * Returns the value of field 'cdFuncionarioGerenteContrato'.
     * 
     * @return long
     * @return the value of field 'cdFuncionarioGerenteContrato'.
     */
    public long getCdFuncionarioGerenteContrato()
    {
        return this._cdFuncionarioGerenteContrato;
    } //-- long getCdFuncionarioGerenteContrato() 

    /**
     * Returns the value of field 'cdIndicadorTipoManutencao'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorTipoManutencao'.
     */
    public int getCdIndicadorTipoManutencao()
    {
        return this._cdIndicadorTipoManutencao;
    } //-- int getCdIndicadorTipoManutencao() 

    /**
     * Returns the value of field 'cdMotivoSituacaoManutencao'.
     * 
     * @return int
     * @return the value of field 'cdMotivoSituacaoManutencao'.
     */
    public int getCdMotivoSituacaoManutencao()
    {
        return this._cdMotivoSituacaoManutencao;
    } //-- int getCdMotivoSituacaoManutencao() 

    /**
     * Returns the value of field 'cdOperacaoCanalInclusao'.
     * 
     * @return String
     * @return the value of field 'cdOperacaoCanalInclusao'.
     */
    public java.lang.String getCdOperacaoCanalInclusao()
    {
        return this._cdOperacaoCanalInclusao;
    } //-- java.lang.String getCdOperacaoCanalInclusao() 

    /**
     * Returns the value of field 'cdOperacaoCanalManutencao'.
     * 
     * @return String
     * @return the value of field 'cdOperacaoCanalManutencao'.
     */
    public java.lang.String getCdOperacaoCanalManutencao()
    {
        return this._cdOperacaoCanalManutencao;
    } //-- java.lang.String getCdOperacaoCanalManutencao() 

    /**
     * Returns the value of field 'cdPessoaJuridica'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridica'.
     */
    public long getCdPessoaJuridica()
    {
        return this._cdPessoaJuridica;
    } //-- long getCdPessoaJuridica() 

    /**
     * Returns the value of field 'cdSituacaoManutencaoContrato'.
     * 
     * @return int
     * @return the value of field 'cdSituacaoManutencaoContrato'.
     */
    public int getCdSituacaoManutencaoContrato()
    {
        return this._cdSituacaoManutencaoContrato;
    } //-- int getCdSituacaoManutencaoContrato() 

    /**
     * Returns the value of field 'cdTipoCanalInclusao'.
     * 
     * @return int
     * @return the value of field 'cdTipoCanalInclusao'.
     */
    public int getCdTipoCanalInclusao()
    {
        return this._cdTipoCanalInclusao;
    } //-- int getCdTipoCanalInclusao() 

    /**
     * Returns the value of field 'cdTipoCanalManutencao'.
     * 
     * @return int
     * @return the value of field 'cdTipoCanalManutencao'.
     */
    public int getCdTipoCanalManutencao()
    {
        return this._cdTipoCanalManutencao;
    } //-- int getCdTipoCanalManutencao() 

    /**
     * Returns the value of field 'cdTipoContratoNegocio'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoNegocio'.
     */
    public int getCdTipoContratoNegocio()
    {
        return this._cdTipoContratoNegocio;
    } //-- int getCdTipoContratoNegocio() 

    /**
     * Returns the value of field 'cdTipoManutencaoContrato'.
     * 
     * @return int
     * @return the value of field 'cdTipoManutencaoContrato'.
     */
    public int getCdTipoManutencaoContrato()
    {
        return this._cdTipoManutencaoContrato;
    } //-- int getCdTipoManutencaoContrato() 

    /**
     * Returns the value of field 'cdTipoResponsavelOrganizacao'.
     * 
     * @return int
     * @return the value of field 'cdTipoResponsavelOrganizacao'.
     */
    public int getCdTipoResponsavelOrganizacao()
    {
        return this._cdTipoResponsavelOrganizacao;
    } //-- int getCdTipoResponsavelOrganizacao() 

    /**
     * Returns the value of field 'cdUnidadeOrganizacional'.
     * 
     * @return int
     * @return the value of field 'cdUnidadeOrganizacional'.
     */
    public int getCdUnidadeOrganizacional()
    {
        return this._cdUnidadeOrganizacional;
    } //-- int getCdUnidadeOrganizacional() 

    /**
     * Returns the value of field 'cdUsuarioInclusao'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioInclusao'.
     */
    public java.lang.String getCdUsuarioInclusao()
    {
        return this._cdUsuarioInclusao;
    } //-- java.lang.String getCdUsuarioInclusao() 

    /**
     * Returns the value of field 'cdUsuarioInclusaoExterno'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioInclusaoExterno'.
     */
    public java.lang.String getCdUsuarioInclusaoExterno()
    {
        return this._cdUsuarioInclusaoExterno;
    } //-- java.lang.String getCdUsuarioInclusaoExterno() 

    /**
     * Returns the value of field 'cdUsuarioManutencao'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioManutencao'.
     */
    public java.lang.String getCdUsuarioManutencao()
    {
        return this._cdUsuarioManutencao;
    } //-- java.lang.String getCdUsuarioManutencao() 

    /**
     * Returns the value of field 'cdUsuarioManutencaoExterno'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioManutencaoExterno'.
     */
    public java.lang.String getCdUsuarioManutencaoExterno()
    {
        return this._cdUsuarioManutencaoExterno;
    } //-- java.lang.String getCdUsuarioManutencaoExterno() 

    /**
     * Returns the value of field 'cdpessoaJuridicaContrato'.
     * 
     * @return long
     * @return the value of field 'cdpessoaJuridicaContrato'.
     */
    public long getCdpessoaJuridicaContrato()
    {
        return this._cdpessoaJuridicaContrato;
    } //-- long getCdpessoaJuridicaContrato() 

    /**
     * Returns the value of field 'codMensagem'.
     * 
     * @return String
     * @return the value of field 'codMensagem'.
     */
    public java.lang.String getCodMensagem()
    {
        return this._codMensagem;
    } //-- java.lang.String getCodMensagem() 

    /**
     * Returns the value of field 'dsFormaAutorizacaoPagamento'.
     * 
     * @return String
     * @return the value of field 'dsFormaAutorizacaoPagamento'.
     */
    public java.lang.String getDsFormaAutorizacaoPagamento()
    {
        return this._dsFormaAutorizacaoPagamento;
    } //-- java.lang.String getDsFormaAutorizacaoPagamento() 

    /**
     * Returns the value of field 'dsFuncionarioGerenteContrato'.
     * 
     * @return String
     * @return the value of field 'dsFuncionarioGerenteContrato'.
     */
    public java.lang.String getDsFuncionarioGerenteContrato()
    {
        return this._dsFuncionarioGerenteContrato;
    } //-- java.lang.String getDsFuncionarioGerenteContrato() 

    /**
     * Returns the value of field 'dsIndicadorTipoManutencao'.
     * 
     * @return String
     * @return the value of field 'dsIndicadorTipoManutencao'.
     */
    public java.lang.String getDsIndicadorTipoManutencao()
    {
        return this._dsIndicadorTipoManutencao;
    } //-- java.lang.String getDsIndicadorTipoManutencao() 

    /**
     * Returns the value of field 'dsMotivoSituacaoManutencao'.
     * 
     * @return String
     * @return the value of field 'dsMotivoSituacaoManutencao'.
     */
    public java.lang.String getDsMotivoSituacaoManutencao()
    {
        return this._dsMotivoSituacaoManutencao;
    } //-- java.lang.String getDsMotivoSituacaoManutencao() 

    /**
     * Returns the value of field 'dsSituacaoManutencaoContrato'.
     * 
     * @return String
     * @return the value of field 'dsSituacaoManutencaoContrato'.
     */
    public java.lang.String getDsSituacaoManutencaoContrato()
    {
        return this._dsSituacaoManutencaoContrato;
    } //-- java.lang.String getDsSituacaoManutencaoContrato() 

    /**
     * Returns the value of field 'dsTipoCanalInclusao'.
     * 
     * @return String
     * @return the value of field 'dsTipoCanalInclusao'.
     */
    public java.lang.String getDsTipoCanalInclusao()
    {
        return this._dsTipoCanalInclusao;
    } //-- java.lang.String getDsTipoCanalInclusao() 

    /**
     * Returns the value of field 'dsTipoCanalManutencao'.
     * 
     * @return String
     * @return the value of field 'dsTipoCanalManutencao'.
     */
    public java.lang.String getDsTipoCanalManutencao()
    {
        return this._dsTipoCanalManutencao;
    } //-- java.lang.String getDsTipoCanalManutencao() 

    /**
     * Returns the value of field 'dsUnidadeOrganizacional'.
     * 
     * @return String
     * @return the value of field 'dsUnidadeOrganizacional'.
     */
    public java.lang.String getDsUnidadeOrganizacional()
    {
        return this._dsUnidadeOrganizacional;
    } //-- java.lang.String getDsUnidadeOrganizacional() 

    /**
     * Returns the value of field 'hrInclusaoRegistro'.
     * 
     * @return String
     * @return the value of field 'hrInclusaoRegistro'.
     */
    public java.lang.String getHrInclusaoRegistro()
    {
        return this._hrInclusaoRegistro;
    } //-- java.lang.String getHrInclusaoRegistro() 

    /**
     * Returns the value of field 'hrManutencaoRegistro'.
     * 
     * @return String
     * @return the value of field 'hrManutencaoRegistro'.
     */
    public java.lang.String getHrManutencaoRegistro()
    {
        return this._hrManutencaoRegistro;
    } //-- java.lang.String getHrManutencaoRegistro() 

    /**
     * Returns the value of field 'mensagem'.
     * 
     * @return String
     * @return the value of field 'mensagem'.
     */
    public java.lang.String getMensagem()
    {
        return this._mensagem;
    } //-- java.lang.String getMensagem() 

    /**
     * Returns the value of field 'nrAditivoContratoNegocio'.
     * 
     * @return long
     * @return the value of field 'nrAditivoContratoNegocio'.
     */
    public long getNrAditivoContratoNegocio()
    {
        return this._nrAditivoContratoNegocio;
    } //-- long getNrAditivoContratoNegocio() 

    /**
     * Returns the value of field 'nrManutencaoContratoNegocio'.
     * 
     * @return long
     * @return the value of field 'nrManutencaoContratoNegocio'.
     */
    public long getNrManutencaoContratoNegocio()
    {
        return this._nrManutencaoContratoNegocio;
    } //-- long getNrManutencaoContratoNegocio() 

    /**
     * Returns the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @return long
     * @return the value of field 'nrSequenciaContratoNegocio'.
     */
    public long getNrSequenciaContratoNegocio()
    {
        return this._nrSequenciaContratoNegocio;
    } //-- long getNrSequenciaContratoNegocio() 

    /**
     * Returns the value of field
     * 'nrSequenciaUnidadeOrganizacional'.
     * 
     * @return int
     * @return the value of field 'nrSequenciaUnidadeOrganizacional'
     */
    public int getNrSequenciaUnidadeOrganizacional()
    {
        return this._nrSequenciaUnidadeOrganizacional;
    } //-- int getNrSequenciaUnidadeOrganizacional() 

    /**
     * Method hasCdFormaAutorizacaoPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFormaAutorizacaoPagamento()
    {
        return this._has_cdFormaAutorizacaoPagamento;
    } //-- boolean hasCdFormaAutorizacaoPagamento() 

    /**
     * Method hasCdFuncionarioGerenteContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFuncionarioGerenteContrato()
    {
        return this._has_cdFuncionarioGerenteContrato;
    } //-- boolean hasCdFuncionarioGerenteContrato() 

    /**
     * Method hasCdIndicadorTipoManutencao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorTipoManutencao()
    {
        return this._has_cdIndicadorTipoManutencao;
    } //-- boolean hasCdIndicadorTipoManutencao() 

    /**
     * Method hasCdMotivoSituacaoManutencao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMotivoSituacaoManutencao()
    {
        return this._has_cdMotivoSituacaoManutencao;
    } //-- boolean hasCdMotivoSituacaoManutencao() 

    /**
     * Method hasCdPessoaJuridica
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridica()
    {
        return this._has_cdPessoaJuridica;
    } //-- boolean hasCdPessoaJuridica() 

    /**
     * Method hasCdSituacaoManutencaoContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSituacaoManutencaoContrato()
    {
        return this._has_cdSituacaoManutencaoContrato;
    } //-- boolean hasCdSituacaoManutencaoContrato() 

    /**
     * Method hasCdTipoCanalInclusao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoCanalInclusao()
    {
        return this._has_cdTipoCanalInclusao;
    } //-- boolean hasCdTipoCanalInclusao() 

    /**
     * Method hasCdTipoCanalManutencao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoCanalManutencao()
    {
        return this._has_cdTipoCanalManutencao;
    } //-- boolean hasCdTipoCanalManutencao() 

    /**
     * Method hasCdTipoContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoNegocio()
    {
        return this._has_cdTipoContratoNegocio;
    } //-- boolean hasCdTipoContratoNegocio() 

    /**
     * Method hasCdTipoManutencaoContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoManutencaoContrato()
    {
        return this._has_cdTipoManutencaoContrato;
    } //-- boolean hasCdTipoManutencaoContrato() 

    /**
     * Method hasCdTipoResponsavelOrganizacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoResponsavelOrganizacao()
    {
        return this._has_cdTipoResponsavelOrganizacao;
    } //-- boolean hasCdTipoResponsavelOrganizacao() 

    /**
     * Method hasCdUnidadeOrganizacional
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdUnidadeOrganizacional()
    {
        return this._has_cdUnidadeOrganizacional;
    } //-- boolean hasCdUnidadeOrganizacional() 

    /**
     * Method hasCdpessoaJuridicaContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdpessoaJuridicaContrato()
    {
        return this._has_cdpessoaJuridicaContrato;
    } //-- boolean hasCdpessoaJuridicaContrato() 

    /**
     * Method hasNrAditivoContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrAditivoContratoNegocio()
    {
        return this._has_nrAditivoContratoNegocio;
    } //-- boolean hasNrAditivoContratoNegocio() 

    /**
     * Method hasNrManutencaoContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrManutencaoContratoNegocio()
    {
        return this._has_nrManutencaoContratoNegocio;
    } //-- boolean hasNrManutencaoContratoNegocio() 

    /**
     * Method hasNrSequenciaContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaContratoNegocio()
    {
        return this._has_nrSequenciaContratoNegocio;
    } //-- boolean hasNrSequenciaContratoNegocio() 

    /**
     * Method hasNrSequenciaUnidadeOrganizacional
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaUnidadeOrganizacional()
    {
        return this._has_nrSequenciaUnidadeOrganizacional;
    } //-- boolean hasNrSequenciaUnidadeOrganizacional() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdFormaAutorizacaoPagamento'.
     * 
     * @param cdFormaAutorizacaoPagamento the value of field
     * 'cdFormaAutorizacaoPagamento'.
     */
    public void setCdFormaAutorizacaoPagamento(int cdFormaAutorizacaoPagamento)
    {
        this._cdFormaAutorizacaoPagamento = cdFormaAutorizacaoPagamento;
        this._has_cdFormaAutorizacaoPagamento = true;
    } //-- void setCdFormaAutorizacaoPagamento(int) 

    /**
     * Sets the value of field 'cdFuncionarioGerenteContrato'.
     * 
     * @param cdFuncionarioGerenteContrato the value of field
     * 'cdFuncionarioGerenteContrato'.
     */
    public void setCdFuncionarioGerenteContrato(long cdFuncionarioGerenteContrato)
    {
        this._cdFuncionarioGerenteContrato = cdFuncionarioGerenteContrato;
        this._has_cdFuncionarioGerenteContrato = true;
    } //-- void setCdFuncionarioGerenteContrato(long) 

    /**
     * Sets the value of field 'cdIndicadorTipoManutencao'.
     * 
     * @param cdIndicadorTipoManutencao the value of field
     * 'cdIndicadorTipoManutencao'.
     */
    public void setCdIndicadorTipoManutencao(int cdIndicadorTipoManutencao)
    {
        this._cdIndicadorTipoManutencao = cdIndicadorTipoManutencao;
        this._has_cdIndicadorTipoManutencao = true;
    } //-- void setCdIndicadorTipoManutencao(int) 

    /**
     * Sets the value of field 'cdMotivoSituacaoManutencao'.
     * 
     * @param cdMotivoSituacaoManutencao the value of field
     * 'cdMotivoSituacaoManutencao'.
     */
    public void setCdMotivoSituacaoManutencao(int cdMotivoSituacaoManutencao)
    {
        this._cdMotivoSituacaoManutencao = cdMotivoSituacaoManutencao;
        this._has_cdMotivoSituacaoManutencao = true;
    } //-- void setCdMotivoSituacaoManutencao(int) 

    /**
     * Sets the value of field 'cdOperacaoCanalInclusao'.
     * 
     * @param cdOperacaoCanalInclusao the value of field
     * 'cdOperacaoCanalInclusao'.
     */
    public void setCdOperacaoCanalInclusao(java.lang.String cdOperacaoCanalInclusao)
    {
        this._cdOperacaoCanalInclusao = cdOperacaoCanalInclusao;
    } //-- void setCdOperacaoCanalInclusao(java.lang.String) 

    /**
     * Sets the value of field 'cdOperacaoCanalManutencao'.
     * 
     * @param cdOperacaoCanalManutencao the value of field
     * 'cdOperacaoCanalManutencao'.
     */
    public void setCdOperacaoCanalManutencao(java.lang.String cdOperacaoCanalManutencao)
    {
        this._cdOperacaoCanalManutencao = cdOperacaoCanalManutencao;
    } //-- void setCdOperacaoCanalManutencao(java.lang.String) 

    /**
     * Sets the value of field 'cdPessoaJuridica'.
     * 
     * @param cdPessoaJuridica the value of field 'cdPessoaJuridica'
     */
    public void setCdPessoaJuridica(long cdPessoaJuridica)
    {
        this._cdPessoaJuridica = cdPessoaJuridica;
        this._has_cdPessoaJuridica = true;
    } //-- void setCdPessoaJuridica(long) 

    /**
     * Sets the value of field 'cdSituacaoManutencaoContrato'.
     * 
     * @param cdSituacaoManutencaoContrato the value of field
     * 'cdSituacaoManutencaoContrato'.
     */
    public void setCdSituacaoManutencaoContrato(int cdSituacaoManutencaoContrato)
    {
        this._cdSituacaoManutencaoContrato = cdSituacaoManutencaoContrato;
        this._has_cdSituacaoManutencaoContrato = true;
    } //-- void setCdSituacaoManutencaoContrato(int) 

    /**
     * Sets the value of field 'cdTipoCanalInclusao'.
     * 
     * @param cdTipoCanalInclusao the value of field
     * 'cdTipoCanalInclusao'.
     */
    public void setCdTipoCanalInclusao(int cdTipoCanalInclusao)
    {
        this._cdTipoCanalInclusao = cdTipoCanalInclusao;
        this._has_cdTipoCanalInclusao = true;
    } //-- void setCdTipoCanalInclusao(int) 

    /**
     * Sets the value of field 'cdTipoCanalManutencao'.
     * 
     * @param cdTipoCanalManutencao the value of field
     * 'cdTipoCanalManutencao'.
     */
    public void setCdTipoCanalManutencao(int cdTipoCanalManutencao)
    {
        this._cdTipoCanalManutencao = cdTipoCanalManutencao;
        this._has_cdTipoCanalManutencao = true;
    } //-- void setCdTipoCanalManutencao(int) 

    /**
     * Sets the value of field 'cdTipoContratoNegocio'.
     * 
     * @param cdTipoContratoNegocio the value of field
     * 'cdTipoContratoNegocio'.
     */
    public void setCdTipoContratoNegocio(int cdTipoContratoNegocio)
    {
        this._cdTipoContratoNegocio = cdTipoContratoNegocio;
        this._has_cdTipoContratoNegocio = true;
    } //-- void setCdTipoContratoNegocio(int) 

    /**
     * Sets the value of field 'cdTipoManutencaoContrato'.
     * 
     * @param cdTipoManutencaoContrato the value of field
     * 'cdTipoManutencaoContrato'.
     */
    public void setCdTipoManutencaoContrato(int cdTipoManutencaoContrato)
    {
        this._cdTipoManutencaoContrato = cdTipoManutencaoContrato;
        this._has_cdTipoManutencaoContrato = true;
    } //-- void setCdTipoManutencaoContrato(int) 

    /**
     * Sets the value of field 'cdTipoResponsavelOrganizacao'.
     * 
     * @param cdTipoResponsavelOrganizacao the value of field
     * 'cdTipoResponsavelOrganizacao'.
     */
    public void setCdTipoResponsavelOrganizacao(int cdTipoResponsavelOrganizacao)
    {
        this._cdTipoResponsavelOrganizacao = cdTipoResponsavelOrganizacao;
        this._has_cdTipoResponsavelOrganizacao = true;
    } //-- void setCdTipoResponsavelOrganizacao(int) 

    /**
     * Sets the value of field 'cdUnidadeOrganizacional'.
     * 
     * @param cdUnidadeOrganizacional the value of field
     * 'cdUnidadeOrganizacional'.
     */
    public void setCdUnidadeOrganizacional(int cdUnidadeOrganizacional)
    {
        this._cdUnidadeOrganizacional = cdUnidadeOrganizacional;
        this._has_cdUnidadeOrganizacional = true;
    } //-- void setCdUnidadeOrganizacional(int) 

    /**
     * Sets the value of field 'cdUsuarioInclusao'.
     * 
     * @param cdUsuarioInclusao the value of field
     * 'cdUsuarioInclusao'.
     */
    public void setCdUsuarioInclusao(java.lang.String cdUsuarioInclusao)
    {
        this._cdUsuarioInclusao = cdUsuarioInclusao;
    } //-- void setCdUsuarioInclusao(java.lang.String) 

    /**
     * Sets the value of field 'cdUsuarioInclusaoExterno'.
     * 
     * @param cdUsuarioInclusaoExterno the value of field
     * 'cdUsuarioInclusaoExterno'.
     */
    public void setCdUsuarioInclusaoExterno(java.lang.String cdUsuarioInclusaoExterno)
    {
        this._cdUsuarioInclusaoExterno = cdUsuarioInclusaoExterno;
    } //-- void setCdUsuarioInclusaoExterno(java.lang.String) 

    /**
     * Sets the value of field 'cdUsuarioManutencao'.
     * 
     * @param cdUsuarioManutencao the value of field
     * 'cdUsuarioManutencao'.
     */
    public void setCdUsuarioManutencao(java.lang.String cdUsuarioManutencao)
    {
        this._cdUsuarioManutencao = cdUsuarioManutencao;
    } //-- void setCdUsuarioManutencao(java.lang.String) 

    /**
     * Sets the value of field 'cdUsuarioManutencaoExterno'.
     * 
     * @param cdUsuarioManutencaoExterno the value of field
     * 'cdUsuarioManutencaoExterno'.
     */
    public void setCdUsuarioManutencaoExterno(java.lang.String cdUsuarioManutencaoExterno)
    {
        this._cdUsuarioManutencaoExterno = cdUsuarioManutencaoExterno;
    } //-- void setCdUsuarioManutencaoExterno(java.lang.String) 

    /**
     * Sets the value of field 'cdpessoaJuridicaContrato'.
     * 
     * @param cdpessoaJuridicaContrato the value of field
     * 'cdpessoaJuridicaContrato'.
     */
    public void setCdpessoaJuridicaContrato(long cdpessoaJuridicaContrato)
    {
        this._cdpessoaJuridicaContrato = cdpessoaJuridicaContrato;
        this._has_cdpessoaJuridicaContrato = true;
    } //-- void setCdpessoaJuridicaContrato(long) 

    /**
     * Sets the value of field 'codMensagem'.
     * 
     * @param codMensagem the value of field 'codMensagem'.
     */
    public void setCodMensagem(java.lang.String codMensagem)
    {
        this._codMensagem = codMensagem;
    } //-- void setCodMensagem(java.lang.String) 

    /**
     * Sets the value of field 'dsFormaAutorizacaoPagamento'.
     * 
     * @param dsFormaAutorizacaoPagamento the value of field
     * 'dsFormaAutorizacaoPagamento'.
     */
    public void setDsFormaAutorizacaoPagamento(java.lang.String dsFormaAutorizacaoPagamento)
    {
        this._dsFormaAutorizacaoPagamento = dsFormaAutorizacaoPagamento;
    } //-- void setDsFormaAutorizacaoPagamento(java.lang.String) 

    /**
     * Sets the value of field 'dsFuncionarioGerenteContrato'.
     * 
     * @param dsFuncionarioGerenteContrato the value of field
     * 'dsFuncionarioGerenteContrato'.
     */
    public void setDsFuncionarioGerenteContrato(java.lang.String dsFuncionarioGerenteContrato)
    {
        this._dsFuncionarioGerenteContrato = dsFuncionarioGerenteContrato;
    } //-- void setDsFuncionarioGerenteContrato(java.lang.String) 

    /**
     * Sets the value of field 'dsIndicadorTipoManutencao'.
     * 
     * @param dsIndicadorTipoManutencao the value of field
     * 'dsIndicadorTipoManutencao'.
     */
    public void setDsIndicadorTipoManutencao(java.lang.String dsIndicadorTipoManutencao)
    {
        this._dsIndicadorTipoManutencao = dsIndicadorTipoManutencao;
    } //-- void setDsIndicadorTipoManutencao(java.lang.String) 

    /**
     * Sets the value of field 'dsMotivoSituacaoManutencao'.
     * 
     * @param dsMotivoSituacaoManutencao the value of field
     * 'dsMotivoSituacaoManutencao'.
     */
    public void setDsMotivoSituacaoManutencao(java.lang.String dsMotivoSituacaoManutencao)
    {
        this._dsMotivoSituacaoManutencao = dsMotivoSituacaoManutencao;
    } //-- void setDsMotivoSituacaoManutencao(java.lang.String) 

    /**
     * Sets the value of field 'dsSituacaoManutencaoContrato'.
     * 
     * @param dsSituacaoManutencaoContrato the value of field
     * 'dsSituacaoManutencaoContrato'.
     */
    public void setDsSituacaoManutencaoContrato(java.lang.String dsSituacaoManutencaoContrato)
    {
        this._dsSituacaoManutencaoContrato = dsSituacaoManutencaoContrato;
    } //-- void setDsSituacaoManutencaoContrato(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoCanalInclusao'.
     * 
     * @param dsTipoCanalInclusao the value of field
     * 'dsTipoCanalInclusao'.
     */
    public void setDsTipoCanalInclusao(java.lang.String dsTipoCanalInclusao)
    {
        this._dsTipoCanalInclusao = dsTipoCanalInclusao;
    } //-- void setDsTipoCanalInclusao(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoCanalManutencao'.
     * 
     * @param dsTipoCanalManutencao the value of field
     * 'dsTipoCanalManutencao'.
     */
    public void setDsTipoCanalManutencao(java.lang.String dsTipoCanalManutencao)
    {
        this._dsTipoCanalManutencao = dsTipoCanalManutencao;
    } //-- void setDsTipoCanalManutencao(java.lang.String) 

    /**
     * Sets the value of field 'dsUnidadeOrganizacional'.
     * 
     * @param dsUnidadeOrganizacional the value of field
     * 'dsUnidadeOrganizacional'.
     */
    public void setDsUnidadeOrganizacional(java.lang.String dsUnidadeOrganizacional)
    {
        this._dsUnidadeOrganizacional = dsUnidadeOrganizacional;
    } //-- void setDsUnidadeOrganizacional(java.lang.String) 

    /**
     * Sets the value of field 'hrInclusaoRegistro'.
     * 
     * @param hrInclusaoRegistro the value of field
     * 'hrInclusaoRegistro'.
     */
    public void setHrInclusaoRegistro(java.lang.String hrInclusaoRegistro)
    {
        this._hrInclusaoRegistro = hrInclusaoRegistro;
    } //-- void setHrInclusaoRegistro(java.lang.String) 

    /**
     * Sets the value of field 'hrManutencaoRegistro'.
     * 
     * @param hrManutencaoRegistro the value of field
     * 'hrManutencaoRegistro'.
     */
    public void setHrManutencaoRegistro(java.lang.String hrManutencaoRegistro)
    {
        this._hrManutencaoRegistro = hrManutencaoRegistro;
    } //-- void setHrManutencaoRegistro(java.lang.String) 

    /**
     * Sets the value of field 'mensagem'.
     * 
     * @param mensagem the value of field 'mensagem'.
     */
    public void setMensagem(java.lang.String mensagem)
    {
        this._mensagem = mensagem;
    } //-- void setMensagem(java.lang.String) 

    /**
     * Sets the value of field 'nrAditivoContratoNegocio'.
     * 
     * @param nrAditivoContratoNegocio the value of field
     * 'nrAditivoContratoNegocio'.
     */
    public void setNrAditivoContratoNegocio(long nrAditivoContratoNegocio)
    {
        this._nrAditivoContratoNegocio = nrAditivoContratoNegocio;
        this._has_nrAditivoContratoNegocio = true;
    } //-- void setNrAditivoContratoNegocio(long) 

    /**
     * Sets the value of field 'nrManutencaoContratoNegocio'.
     * 
     * @param nrManutencaoContratoNegocio the value of field
     * 'nrManutencaoContratoNegocio'.
     */
    public void setNrManutencaoContratoNegocio(long nrManutencaoContratoNegocio)
    {
        this._nrManutencaoContratoNegocio = nrManutencaoContratoNegocio;
        this._has_nrManutencaoContratoNegocio = true;
    } //-- void setNrManutencaoContratoNegocio(long) 

    /**
     * Sets the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @param nrSequenciaContratoNegocio the value of field
     * 'nrSequenciaContratoNegocio'.
     */
    public void setNrSequenciaContratoNegocio(long nrSequenciaContratoNegocio)
    {
        this._nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
        this._has_nrSequenciaContratoNegocio = true;
    } //-- void setNrSequenciaContratoNegocio(long) 

    /**
     * Sets the value of field 'nrSequenciaUnidadeOrganizacional'.
     * 
     * @param nrSequenciaUnidadeOrganizacional the value of field
     * 'nrSequenciaUnidadeOrganizacional'.
     */
    public void setNrSequenciaUnidadeOrganizacional(int nrSequenciaUnidadeOrganizacional)
    {
        this._nrSequenciaUnidadeOrganizacional = nrSequenciaUnidadeOrganizacional;
        this._has_nrSequenciaUnidadeOrganizacional = true;
    } //-- void setNrSequenciaUnidadeOrganizacional(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ConsultarManutencaoConfigContratoResponse
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultarmanutencaoconfigcontrato.response.ConsultarManutencaoConfigContratoResponse unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultarmanutencaoconfigcontrato.response.ConsultarManutencaoConfigContratoResponse) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultarmanutencaoconfigcontrato.response.ConsultarManutencaoConfigContratoResponse.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarmanutencaoconfigcontrato.response.ConsultarManutencaoConfigContratoResponse unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
