/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.incluirsolicrecuperacao.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdPessoa
     */
    private long _cdPessoa = 0;

    /**
     * keeps track of state for field: _cdPessoa
     */
    private boolean _has_cdPessoa;

    /**
     * Field _cdTipoLayoutArquivo
     */
    private int _cdTipoLayoutArquivo = 0;

    /**
     * keeps track of state for field: _cdTipoLayoutArquivo
     */
    private boolean _has_cdTipoLayoutArquivo;

    /**
     * Field _cdClienteTransfArq
     */
    private long _cdClienteTransfArq = 0;

    /**
     * keeps track of state for field: _cdClienteTransfArq
     */
    private boolean _has_cdClienteTransfArq;

    /**
     * Field _nrArquivoRemessa
     */
    private long _nrArquivoRemessa = 0;

    /**
     * keeps track of state for field: _nrArquivoRemessa
     */
    private boolean _has_nrArquivoRemessa;

    /**
     * Field _hrInclusaoRemessaSistema
     */
    private java.lang.String _hrInclusaoRemessaSistema;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.incluirsolicrecuperacao.request.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdClienteTransfArq
     * 
     */
    public void deleteCdClienteTransfArq()
    {
        this._has_cdClienteTransfArq= false;
    } //-- void deleteCdClienteTransfArq() 

    /**
     * Method deleteCdPessoa
     * 
     */
    public void deleteCdPessoa()
    {
        this._has_cdPessoa= false;
    } //-- void deleteCdPessoa() 

    /**
     * Method deleteCdTipoLayoutArquivo
     * 
     */
    public void deleteCdTipoLayoutArquivo()
    {
        this._has_cdTipoLayoutArquivo= false;
    } //-- void deleteCdTipoLayoutArquivo() 

    /**
     * Method deleteNrArquivoRemessa
     * 
     */
    public void deleteNrArquivoRemessa()
    {
        this._has_nrArquivoRemessa= false;
    } //-- void deleteNrArquivoRemessa() 

    /**
     * Returns the value of field 'cdClienteTransfArq'.
     * 
     * @return long
     * @return the value of field 'cdClienteTransfArq'.
     */
    public long getCdClienteTransfArq()
    {
        return this._cdClienteTransfArq;
    } //-- long getCdClienteTransfArq() 

    /**
     * Returns the value of field 'cdPessoa'.
     * 
     * @return long
     * @return the value of field 'cdPessoa'.
     */
    public long getCdPessoa()
    {
        return this._cdPessoa;
    } //-- long getCdPessoa() 

    /**
     * Returns the value of field 'cdTipoLayoutArquivo'.
     * 
     * @return int
     * @return the value of field 'cdTipoLayoutArquivo'.
     */
    public int getCdTipoLayoutArquivo()
    {
        return this._cdTipoLayoutArquivo;
    } //-- int getCdTipoLayoutArquivo() 

    /**
     * Returns the value of field 'hrInclusaoRemessaSistema'.
     * 
     * @return String
     * @return the value of field 'hrInclusaoRemessaSistema'.
     */
    public java.lang.String getHrInclusaoRemessaSistema()
    {
        return this._hrInclusaoRemessaSistema;
    } //-- java.lang.String getHrInclusaoRemessaSistema() 

    /**
     * Returns the value of field 'nrArquivoRemessa'.
     * 
     * @return long
     * @return the value of field 'nrArquivoRemessa'.
     */
    public long getNrArquivoRemessa()
    {
        return this._nrArquivoRemessa;
    } //-- long getNrArquivoRemessa() 

    /**
     * Method hasCdClienteTransfArq
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdClienteTransfArq()
    {
        return this._has_cdClienteTransfArq;
    } //-- boolean hasCdClienteTransfArq() 

    /**
     * Method hasCdPessoa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoa()
    {
        return this._has_cdPessoa;
    } //-- boolean hasCdPessoa() 

    /**
     * Method hasCdTipoLayoutArquivo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoLayoutArquivo()
    {
        return this._has_cdTipoLayoutArquivo;
    } //-- boolean hasCdTipoLayoutArquivo() 

    /**
     * Method hasNrArquivoRemessa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrArquivoRemessa()
    {
        return this._has_nrArquivoRemessa;
    } //-- boolean hasNrArquivoRemessa() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdClienteTransfArq'.
     * 
     * @param cdClienteTransfArq the value of field
     * 'cdClienteTransfArq'.
     */
    public void setCdClienteTransfArq(long cdClienteTransfArq)
    {
        this._cdClienteTransfArq = cdClienteTransfArq;
        this._has_cdClienteTransfArq = true;
    } //-- void setCdClienteTransfArq(long) 

    /**
     * Sets the value of field 'cdPessoa'.
     * 
     * @param cdPessoa the value of field 'cdPessoa'.
     */
    public void setCdPessoa(long cdPessoa)
    {
        this._cdPessoa = cdPessoa;
        this._has_cdPessoa = true;
    } //-- void setCdPessoa(long) 

    /**
     * Sets the value of field 'cdTipoLayoutArquivo'.
     * 
     * @param cdTipoLayoutArquivo the value of field
     * 'cdTipoLayoutArquivo'.
     */
    public void setCdTipoLayoutArquivo(int cdTipoLayoutArquivo)
    {
        this._cdTipoLayoutArquivo = cdTipoLayoutArquivo;
        this._has_cdTipoLayoutArquivo = true;
    } //-- void setCdTipoLayoutArquivo(int) 

    /**
     * Sets the value of field 'hrInclusaoRemessaSistema'.
     * 
     * @param hrInclusaoRemessaSistema the value of field
     * 'hrInclusaoRemessaSistema'.
     */
    public void setHrInclusaoRemessaSistema(java.lang.String hrInclusaoRemessaSistema)
    {
        this._hrInclusaoRemessaSistema = hrInclusaoRemessaSistema;
    } //-- void setHrInclusaoRemessaSistema(java.lang.String) 

    /**
     * Sets the value of field 'nrArquivoRemessa'.
     * 
     * @param nrArquivoRemessa the value of field 'nrArquivoRemessa'
     */
    public void setNrArquivoRemessa(long nrArquivoRemessa)
    {
        this._nrArquivoRemessa = nrArquivoRemessa;
        this._has_nrArquivoRemessa = true;
    } //-- void setNrArquivoRemessa(long) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.incluirsolicrecuperacao.request.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.incluirsolicrecuperacao.request.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.incluirsolicrecuperacao.request.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.incluirsolicrecuperacao.request.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
