/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.imprimircomprovantecodigobarra.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencia.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencia implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _centroCusto
     */
    private java.lang.String _centroCusto;

    /**
     * Field _protocolo
     */
    private long _protocolo = 0;

    /**
     * keeps track of state for field: _protocolo
     */
    private boolean _has_protocolo;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencia() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimircomprovantecodigobarra.response.Ocorrencia()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteProtocolo
     * 
     */
    public void deleteProtocolo()
    {
        this._has_protocolo= false;
    } //-- void deleteProtocolo() 

    /**
     * Returns the value of field 'centroCusto'.
     * 
     * @return String
     * @return the value of field 'centroCusto'.
     */
    public java.lang.String getCentroCusto()
    {
        return this._centroCusto;
    } //-- java.lang.String getCentroCusto() 

    /**
     * Returns the value of field 'protocolo'.
     * 
     * @return long
     * @return the value of field 'protocolo'.
     */
    public long getProtocolo()
    {
        return this._protocolo;
    } //-- long getProtocolo() 

    /**
     * Method hasProtocolo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasProtocolo()
    {
        return this._has_protocolo;
    } //-- boolean hasProtocolo() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'centroCusto'.
     * 
     * @param centroCusto the value of field 'centroCusto'.
     */
    public void setCentroCusto(java.lang.String centroCusto)
    {
        this._centroCusto = centroCusto;
    } //-- void setCentroCusto(java.lang.String) 

    /**
     * Sets the value of field 'protocolo'.
     * 
     * @param protocolo the value of field 'protocolo'.
     */
    public void setProtocolo(long protocolo)
    {
        this._protocolo = protocolo;
        this._has_protocolo = true;
    } //-- void setProtocolo(long) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencia
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.imprimircomprovantecodigobarra.response.Ocorrencia unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.imprimircomprovantecodigobarra.response.Ocorrencia) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.imprimircomprovantecodigobarra.response.Ocorrencia.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimircomprovantecodigobarra.response.Ocorrencia unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
